<s>
Kůže	kůže	k1gFnSc1	kůže
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
cutis	cutis	k1gInSc1	cutis
<g/>
,	,	kIx,	,
gr	gr	k?	gr
<g/>
.	.	kIx.	.
derma	derma	k1gFnSc1	derma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
zvířat	zvíře	k1gNnPc2	zvíře
kožich	kožich	k1gInSc1	kožich
<g/>
,	,	kIx,	,
kožešina	kožešina	k1gFnSc1	kožešina
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc4	orgán
pokrývající	pokrývající	k2eAgNnPc4d1	pokrývající
těla	tělo	k1gNnPc4	tělo
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
tak	tak	k6eAd1	tak
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
prostředí	prostředí	k1gNnSc4	prostředí
organismu	organismus	k1gInSc2	organismus
od	od	k7c2	od
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
savců	savec	k1gMnPc2	savec
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
ochlupením	ochlupení	k1gNnSc7	ochlupení
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
tzv.	tzv.	kA	tzv.
srstí	srst	k1gFnPc2	srst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
kůže	kůže	k1gFnSc2	kůže
u	u	k7c2	u
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1,6	[number]	k4	1,6
až	až	k9	až
1,8	[number]	k4	1,8
m2	m2	k4	m2
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
největší	veliký	k2eAgInSc1d3	veliký
orgán	orgán	k1gInSc1	orgán
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
kůže	kůže	k1gFnSc2	kůže
představuje	představovat	k5eAaImIp3nS	představovat
7	[number]	k4	7
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
krk	krk	k1gInSc4	krk
u	u	k7c2	u
běžného	běžný	k2eAgMnSc2d1	běžný
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
připadá	připadat	k5eAaImIp3nS	připadat
přibližně	přibližně	k6eAd1	přibližně
11	[number]	k4	11
%	%	kIx~	%
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
na	na	k7c4	na
trup	trup	k1gInSc4	trup
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
na	na	k7c4	na
horní	horní	k2eAgFnPc4d1	horní
končetiny	končetina	k1gFnPc4	končetina
23	[number]	k4	23
%	%	kIx~	%
a	a	k8xC	a
na	na	k7c4	na
dolní	dolní	k2eAgFnPc4d1	dolní
končetiny	končetina	k1gFnPc4	končetina
asi	asi	k9	asi
36	[number]	k4	36
%	%	kIx~	%
celého	celý	k2eAgInSc2d1	celý
povrchu	povrch	k1gInSc2	povrch
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
tloušťka	tloušťka	k1gFnSc1	tloušťka
<g/>
)	)	kIx)	)
lidské	lidský	k2eAgFnSc2d1	lidská
kůže	kůže	k1gFnSc2	kůže
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
0,4	[number]	k4	0,4
do	do	k7c2	do
4	[number]	k4	4
mm	mm	kA	mm
(	(	kIx(	(
<g/>
záda	záda	k1gNnPc4	záda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejtenčí	tenký	k2eAgFnSc1d3	nejtenčí
kůže	kůže	k1gFnSc1	kůže
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
očních	oční	k2eAgNnPc6d1	oční
víčkách	víčko	k1gNnPc6	víčko
<g/>
,	,	kIx,	,
uchu	ucho	k1gNnSc6	ucho
<g/>
,	,	kIx,	,
penisu	penis	k1gInSc6	penis
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
vlasové	vlasový	k2eAgFnSc6d1	vlasová
části	část	k1gFnSc6	část
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
plní	plnit	k5eAaImIp3nS	plnit
řadu	řada	k1gFnSc4	řada
rozmanitých	rozmanitý	k2eAgFnPc2d1	rozmanitá
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
ochranná	ochranný	k2eAgFnSc1d1	ochranná
funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bariéru	bariéra	k1gFnSc4	bariéra
mezi	mezi	k7c7	mezi
vnějším	vnější	k2eAgMnSc7d1	vnější
a	a	k8xC	a
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
chrání	chránit	k5eAaImIp3nS	chránit
tělo	tělo	k1gNnSc4	tělo
proti	proti	k7c3	proti
vniku	vnik	k1gInSc3	vnik
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
a	a	k8xC	a
před	před	k7c4	před
UV	UV	kA	UV
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
smyslové	smyslový	k2eAgFnSc2d1	smyslová
funkce	funkce	k1gFnSc2	funkce
<g/>
:	:	kIx,	:
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
řada	řada	k1gFnSc1	řada
receptorů	receptor	k1gInPc2	receptor
(	(	kIx(	(
<g/>
nervových	nervový	k2eAgNnPc2d1	nervové
zakončení	zakončení	k1gNnPc2	zakončení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
chlad	chlad	k1gInSc1	chlad
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
nebo	nebo	k8xC	nebo
poranění	poranění	k1gNnSc1	poranění
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
termoregulace	termoregulace	k1gFnSc1	termoregulace
<g/>
:	:	kIx,	:
kůže	kůže	k1gFnSc1	kůže
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
stálou	stálý	k2eAgFnSc4d1	stálá
teplotu	teplota	k1gFnSc4	teplota
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomocí	pomocí	k7c2	pomocí
kožních	kožní	k2eAgFnPc2d1	kožní
cév	céva	k1gFnPc2	céva
a	a	k8xC	a
potních	potní	k2eAgFnPc2d1	potní
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplém	teplý	k2eAgNnSc6d1	teplé
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
cévy	céva	k1gFnPc1	céva
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zvětšení	zvětšení	k1gNnSc3	zvětšení
průtoku	průtok	k1gInSc2	průtok
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
urychlení	urychlení	k1gNnSc3	urychlení
výdeje	výdej	k1gInSc2	výdej
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
tělesného	tělesný	k2eAgNnSc2d1	tělesné
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
k	k	k7c3	k
odpaření	odpaření	k1gNnSc3	odpaření
potu	pot	k1gInSc2	pot
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
kůže	kůže	k1gFnSc1	kůže
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
nechtěnému	chtěný	k2eNgNnSc3d1	nechtěné
odpařování	odpařování	k1gNnSc3	odpařování
tekutin	tekutina	k1gFnPc2	tekutina
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
skladovací	skladovací	k2eAgFnSc1d1	skladovací
funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
v	v	k7c6	v
podkožním	podkožní	k2eAgNnSc6d1	podkožní
vazivu	vazivo	k1gNnSc6	vazivo
se	se	k3xPyFc4	se
skladuje	skladovat	k5eAaImIp3nS	skladovat
tuk	tuk	k1gInSc1	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
funkce	funkce	k1gFnSc2	funkce
zásobní	zásobní	k2eAgFnSc2d1	zásobní
i	i	k8xC	i
funkci	funkce	k1gFnSc4	funkce
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
a	a	k8xC	a
izolační	izolační	k2eAgFnSc4d1	izolační
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
uskladněny	uskladnit	k5eAaPmNgInP	uskladnit
i	i	k9	i
vitaminy	vitamin	k1gInPc4	vitamin
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
.	.	kIx.	.
vylučovací	vylučovací	k2eAgFnSc1d1	vylučovací
funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
ledvin	ledvina	k1gFnPc2	ledvina
dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
orgánem	orgán	k1gInSc7	orgán
pro	pro	k7c4	pro
vylučování	vylučování	k1gNnSc4	vylučování
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
mazovými	mazový	k2eAgFnPc7d1	mazová
a	a	k8xC	a
potními	potní	k2eAgFnPc7d1	potní
žlázami	žláza	k1gFnPc7	žláza
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
sekrety	sekreta	k1gFnPc1	sekreta
(	(	kIx(	(
<g/>
pot	pot	k1gInSc1	pot
a	a	k8xC	a
maz	maz	k1gInSc1	maz
<g/>
)	)	kIx)	)
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Pot	pot	k1gInSc1	pot
svou	svůj	k3xOyFgFnSc4	svůj
kyselou	kyselá	k1gFnSc4	kyselá
reakcí	reakce	k1gFnPc2	reakce
omezuje	omezovat	k5eAaImIp3nS	omezovat
růst	růst	k1gInSc1	růst
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
slabé	slabý	k2eAgNnSc1d1	slabé
dezinfekční	dezinfekční	k2eAgInPc1d1	dezinfekční
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Vylučování	vylučování	k1gNnSc1	vylučování
potu	pot	k1gInSc2	pot
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc4d1	důležitý
prostředek	prostředek	k1gInSc4	prostředek
termoregulace	termoregulace	k1gFnSc2	termoregulace
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
resorpční	resorpční	k2eAgFnPc1d1	resorpční
funkce	funkce	k1gFnPc1	funkce
<g/>
:	:	kIx,	:
přes	přes	k7c4	přes
kůži	kůže	k1gFnSc4	kůže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
vpravit	vpravit	k5eAaPmF	vpravit
jen	jen	k9	jen
látky	látka	k1gFnPc4	látka
rozpuštěné	rozpuštěný	k2eAgFnPc4d1	rozpuštěná
v	v	k7c6	v
tukových	tukový	k2eAgInPc6d1	tukový
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
nebo	nebo	k8xC	nebo
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
lze	lze	k6eAd1	lze
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
vtírat	vtírat	k5eAaImF	vtírat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
různé	různý	k2eAgInPc1d1	různý
léky	lék	k1gInPc1	lék
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mastí	mast	k1gFnPc2	mast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
kůži	kůže	k1gFnSc4	kůže
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
absorbovat	absorbovat	k5eAaBmF	absorbovat
dýchací	dýchací	k2eAgInPc4d1	dýchací
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Zdravá	zdravý	k2eAgFnSc1d1	zdravá
kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
absorbovat	absorbovat	k5eAaBmF	absorbovat
jen	jen	k6eAd1	jen
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Poškozená	poškozený	k2eAgFnSc1d1	poškozená
kůže	kůže	k1gFnSc1	kůže
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
velké	velký	k2eAgFnPc4d1	velká
resorpční	resorpční	k2eAgFnPc4d1	resorpční
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
infekcí	infekce	k1gFnPc2	infekce
způsobených	způsobený	k2eAgFnPc2d1	způsobená
mikroorganismy	mikroorganismus	k1gInPc4	mikroorganismus
<g/>
.	.	kIx.	.
estetická	estetický	k2eAgFnSc1d1	estetická
funkce	funkce	k1gFnSc1	funkce
a	a	k8xC	a
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
:	:	kIx,	:
př	př	kA	př
<g/>
.	.	kIx.	.
červenání	červenání	k1gNnSc4	červenání
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uhodnout	uhodnout	k5eAaPmF	uhodnout
psychické	psychický	k2eAgNnSc4d1	psychické
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
jedince	jedinec	k1gMnSc2	jedinec
Kůže	kůže	k1gFnSc2	kůže
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
škáry	škára	k1gFnSc2	škára
a	a	k8xC	a
podkožního	podkožní	k2eAgNnSc2d1	podkožní
vaziva	vazivo	k1gNnSc2	vazivo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
pokožka	pokožka	k1gFnSc1	pokožka
(	(	kIx(	(
<g/>
živočichové	živočich	k1gMnPc1	živočich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokožka	pokožka	k1gFnSc1	pokožka
(	(	kIx(	(
<g/>
epidermis	epidermis	k1gFnSc1	epidermis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
mnoha	mnoho	k4c7	mnoho
vrstvami	vrstva	k1gFnPc7	vrstva
buněk	buňka	k1gFnPc2	buňka
dlaždicového	dlaždicový	k2eAgInSc2d1	dlaždicový
epitelu	epitel	k1gInSc2	epitel
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnPc1d1	horní
vrstvy	vrstva	k1gFnPc1	vrstva
kůže	kůže	k1gFnSc2	kůže
neustále	neustále	k6eAd1	neustále
rohovatí	rohovatět	k5eAaImIp3nP	rohovatět
<g/>
,	,	kIx,	,
odumírají	odumírat	k5eAaImIp3nP	odumírat
a	a	k8xC	a
odlupují	odlupovat	k5eAaImIp3nP	odlupovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
pokožky	pokožka	k1gFnSc2	pokožka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pozvolna	pozvolna	k6eAd1	pozvolna
degenerují	degenerovat	k5eAaBmIp3nP	degenerovat
<g/>
,	,	kIx,	,
naplňují	naplňovat	k5eAaImIp3nP	naplňovat
se	se	k3xPyFc4	se
keratinem	keratin	k1gInSc7	keratin
(	(	kIx(	(
<g/>
rohovinou	rohovina	k1gFnSc7	rohovina
<g/>
)	)	kIx)	)
a	a	k8xC	a
odumírají	odumírat	k5eAaImIp3nP	odumírat
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
pokožka	pokožka	k1gFnSc1	pokožka
se	se	k3xPyFc4	se
obmění	obměnit	k5eAaPmIp3nS	obměnit
asi	asi	k9	asi
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
se	se	k3xPyFc4	se
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
oloupe	oloupat	k5eAaPmIp3nS	oloupat
asi	asi	k9	asi
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
kg	kg	kA	kg
mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
buněk	buňka	k1gFnPc2	buňka
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
<g/>
Buňky	buňka	k1gFnPc1	buňka
ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
vrstvách	vrstva	k1gFnPc6	vrstva
kůže	kůže	k1gFnSc2	kůže
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
dělí	dělit	k5eAaImIp3nP	dělit
a	a	k8xC	a
vytlačují	vytlačovat	k5eAaImIp3nP	vytlačovat
starší	starý	k2eAgFnPc4d2	starší
buňky	buňka	k1gFnPc4	buňka
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
spodních	spodní	k2eAgFnPc2d1	spodní
vrstev	vrstva	k1gFnPc2	vrstva
pokožky	pokožka	k1gFnSc2	pokožka
je	být	k5eAaImIp3nS	být
také	také	k9	také
pigmentové	pigmentový	k2eAgNnSc1d1	pigmentové
barvivo	barvivo	k1gNnSc1	barvivo
melanin	melanin	k1gInSc1	melanin
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
tělo	tělo	k1gNnSc4	tělo
před	před	k7c7	před
škodlivými	škodlivý	k2eAgInPc7d1	škodlivý
účinky	účinek	k1gInPc7	účinek
UV-záření	UVáření	k1gNnSc2	UV-záření
<g/>
.	.	kIx.	.
</s>
<s>
Neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
kapiláry	kapilára	k1gFnPc4	kapilára
(	(	kIx(	(
<g/>
vlásečnice	vlásečnice	k1gFnPc4	vlásečnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
živin	živina	k1gFnPc2	živina
získává	získávat	k5eAaImIp3nS	získávat
ze	z	k7c2	z
škáry	škára	k1gFnSc2	škára
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
škára	škára	k1gFnSc1	škára
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
vrstvou	vrstva	k1gFnSc7	vrstva
kůže	kůže	k1gFnSc2	kůže
je	být	k5eAaImIp3nS	být
škára	škára	k1gFnSc1	škára
(	(	kIx(	(
<g/>
dermis	dermis	k1gFnSc1	dermis
<g/>
,	,	kIx,	,
corium	corium	k1gNnSc1	corium
<g/>
,	,	kIx,	,
cutis	cutis	k1gFnSc1	cutis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
pružná	pružný	k2eAgFnSc1d1	pružná
vazivová	vazivový	k2eAgFnSc1d1	vazivová
vrstva	vrstva	k1gFnSc1	vrstva
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
sítí	síť	k1gFnSc7	síť
kolagenových	kolagenův	k2eAgFnPc2d1	kolagenův
a	a	k8xC	a
elastických	elastický	k2eAgFnPc2d1	elastická
vláken	vlákna	k1gFnPc2	vlákna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
pokožky	pokožka	k1gFnSc2	pokožka
a	a	k8xC	a
škáry	škára	k1gFnSc2	škára
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
škárové	škárový	k2eAgFnPc1d1	Škárová
papily	papila	k1gFnPc1	papila
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
jsou	být	k5eAaImIp3nP	být
kapilární	kapilární	k2eAgFnPc4d1	kapilární
sítě	síť	k1gFnPc4	síť
a	a	k8xC	a
nervová	nervový	k2eAgNnPc1d1	nervové
zakončení	zakončení	k1gNnPc1	zakončení
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dosažení	dosažení	k1gNnSc2	dosažení
větší	veliký	k2eAgFnSc2d2	veliký
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
do	do	k7c2	do
pokožky	pokožka	k1gFnSc2	pokožka
pronikají	pronikat	k5eAaImIp3nP	pronikat
živiny	živina	k1gFnPc1	živina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
papily	papila	k1gFnPc1	papila
silně	silně	k6eAd1	silně
zvlněné	zvlněný	k2eAgFnPc1d1	zvlněná
a	a	k8xC	a
právě	právě	k6eAd1	právě
jim	on	k3xPp3gMnPc3	on
vděčí	vděčit	k5eAaImIp3nS	vděčit
člověk	člověk	k1gMnSc1	člověk
za	za	k7c7	za
otisky	otisk	k1gInPc7	otisk
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
daktyloskopie	daktyloskopie	k1gFnPc4	daktyloskopie
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
pružnosti	pružnost	k1gFnSc2	pružnost
škáry	škára	k1gFnSc2	škára
je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgInSc7d1	přirozený
projevem	projev	k1gInSc7	projev
stárnutí	stárnutí	k1gNnSc2	stárnutí
-	-	kIx~	-
kůže	kůže	k1gFnSc1	kůže
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
do	do	k7c2	do
záhybů	záhyb	k1gInPc2	záhyb
a	a	k8xC	a
vrásek	vráska	k1gFnPc2	vráska
<g/>
.	.	kIx.	.
</s>
<s>
Nervová	nervový	k2eAgNnPc1d1	nervové
tělíska	tělísko	k1gNnPc1	tělísko
<g/>
:	:	kIx,	:
Meissnerova	Meissnerův	k2eAgNnPc1d1	Meissnerův
tělíska	tělísko	k1gNnPc1	tělísko
(	(	kIx(	(
<g/>
hmatová	hmatový	k2eAgFnSc1d1	hmatová
<g/>
)	)	kIx)	)
-	-	kIx~	-
čidla	čidlo	k1gNnPc1	čidlo
dotyku	dotyk	k1gInSc2	dotyk
Krauseova	Krauseův	k2eAgNnPc1d1	Krauseův
tělíska	tělísko	k1gNnPc1	tělísko
-	-	kIx~	-
receptory	receptor	k1gInPc1	receptor
chladu	chlad	k1gInSc2	chlad
Ruffiniho	Ruffini	k1gMnSc2	Ruffini
tělíska	tělísko	k1gNnSc2	tělísko
-	-	kIx~	-
receptory	receptor	k1gInPc7	receptor
tepla	teplo	k1gNnSc2	teplo
Vater-Paciniho	Vater-Pacini	k1gMnSc2	Vater-Pacini
tělíska	tělísko	k1gNnSc2	tělísko
-	-	kIx~	-
čidla	čidlo	k1gNnSc2	čidlo
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
vibrací	vibrace	k1gFnPc2	vibrace
a	a	k8xC	a
tahu	tah	k1gInSc2	tah
<g/>
;	;	kIx,	;
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
jsou	být	k5eAaImIp3nP	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
podkožním	podkožní	k2eAgNnSc6d1	podkožní
vazivu	vazivo	k1gNnSc6	vazivo
Ve	v	k7c6	v
škáře	škára	k1gFnSc6	škára
však	však	k9	však
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
ještě	ještě	k9	ještě
další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mění	měnit	k5eAaImIp3nP	měnit
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
naší	náš	k3xOp1gFnSc2	náš
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tady	tady	k6eAd1	tady
kožní	kožní	k2eAgFnPc1d1	kožní
a	a	k8xC	a
mazové	mazový	k2eAgFnPc1d1	mazová
žlázy	žláza	k1gFnPc1	žláza
a	a	k8xC	a
také	také	k9	také
vlasové	vlasový	k2eAgFnPc4d1	vlasová
cibulky	cibulka	k1gFnPc4	cibulka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
podkožní	podkožní	k2eAgNnSc1d1	podkožní
vazivo	vazivo	k1gNnSc1	vazivo
<g/>
.	.	kIx.	.
</s>
<s>
Podkožní	podkožní	k2eAgNnSc1d1	podkožní
vazivo	vazivo	k1gNnSc1	vazivo
(	(	kIx(	(
<g/>
hypodermis	hypodermis	k1gFnSc1	hypodermis
<g/>
,	,	kIx,	,
tela	tel	k2eAgFnSc1d1	tela
subcutanea	subcutanea	k1gFnSc1	subcutanea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vrstva	vrstva	k1gFnSc1	vrstva
kůže	kůže	k1gFnSc2	kůže
pod	pod	k7c7	pod
škárou	škára	k1gFnSc7	škára
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
tukových	tukový	k2eAgFnPc2d1	tuková
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xC	jako
zásobárna	zásobárna	k1gFnSc1	zásobárna
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
rozpuštěny	rozpuštěn	k2eAgInPc1d1	rozpuštěn
vitamíny	vitamín	k1gInPc1	vitamín
A	A	kA	A
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
a	a	k8xC	a
K.	K.	kA	K.
V	v	k7c6	v
podkožním	podkožní	k2eAgNnSc6d1	podkožní
vazivu	vazivo	k1gNnSc6	vazivo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Vater-Paciniho	Vater-Pacini	k1gMnSc2	Vater-Pacini
tělíska	tělísko	k1gNnPc4	tělísko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
receptory	receptor	k1gInPc4	receptor
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
tahu	tah	k1gInSc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Funkcí	funkce	k1gFnSc7	funkce
podkožního	podkožní	k2eAgNnSc2d1	podkožní
vaziva	vazivo	k1gNnSc2	vazivo
je	být	k5eAaImIp3nS	být
izolovat	izolovat	k5eAaBmF	izolovat
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
svaly	sval	k1gInPc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Podkožní	podkožní	k2eAgFnSc1d1	podkožní
tuková	tukový	k2eAgFnSc1d1	tuková
vrstva	vrstva	k1gFnSc1	vrstva
určuje	určovat	k5eAaImIp3nS	určovat
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc4	hmotnost
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
bývá	bývat	k5eAaImIp3nS	bývat
tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Vater-Paciniho	Vater-Pacini	k1gMnSc4	Vater-Pacini
tělíska	tělísko	k1gNnSc2	tělísko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
receptory	receptor	k1gInPc4	receptor
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
tahu	tah	k1gInSc2	tah
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
kožní	kožní	k2eAgInPc4d1	kožní
deriváty	derivát	k1gInPc4	derivát
(	(	kIx(	(
<g/>
přídatné	přídatný	k2eAgInPc1d1	přídatný
kožní	kožní	k2eAgInPc1d1	kožní
orgány	orgán	k1gInPc1	orgán
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nP	patřit
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
chlupy	chlup	k1gInPc1	chlup
<g/>
,	,	kIx,	,
kožní	kožní	k2eAgFnPc4d1	kožní
žlázy	žláza	k1gFnPc4	žláza
nebo	nebo	k8xC	nebo
nehty	nehet	k1gInPc4	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Vlasy	vlas	k1gInPc1	vlas
a	a	k8xC	a
chlupy	chlup	k1gInPc1	chlup
Vlasy	vlas	k1gInPc1	vlas
a	a	k8xC	a
chlupy	chlup	k1gInPc1	chlup
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
vlasových	vlasový	k2eAgInPc2d1	vlasový
(	(	kIx(	(
<g/>
chlupových	chlupový	k2eAgInPc2d1	chlupový
<g/>
)	)	kIx)	)
váčků	váček	k1gInPc2	váček
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
váčku	váček	k1gInSc6	váček
je	být	k5eAaImIp3nS	být
vlasová	vlasový	k2eAgFnSc1d1	vlasová
cibulka	cibulka	k1gFnSc1	cibulka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
vlas	vlas	k1gInSc4	vlas
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Vlas	vlas	k1gInSc1	vlas
(	(	kIx(	(
<g/>
chlup	chlup	k1gInSc1	chlup
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
,	,	kIx,	,
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
kutikuly	kutikula	k1gFnSc2	kutikula
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
vlas	vlas	k1gInSc1	vlas
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
svalem	sval	k1gInSc7	sval
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
stahem	stah	k1gInSc7	stah
na	na	k7c4	na
chlad	chlad	k1gInSc4	chlad
nebo	nebo	k8xC	nebo
psychické	psychický	k2eAgNnSc4d1	psychické
podráždění	podráždění	k1gNnSc4	podráždění
a	a	k8xC	a
vztyčí	vztyčit	k5eAaPmIp3nS	vztyčit
volnou	volný	k2eAgFnSc4d1	volná
část	část	k1gFnSc4	část
vlasu	vlas	k1gInSc2	vlas
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
husí	husí	k2eAgFnSc1d1	husí
kůže	kůže	k1gFnSc1	kůže
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nehty	nehet	k1gInPc1	nehet
Potní	potní	k2eAgFnSc2d1	potní
žlázy	žláza	k1gFnSc2	žláza
V	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
jsou	být	k5eAaImIp3nP	být
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
rozloženy	rozložit	k5eAaPmNgInP	rozložit
–	–	k?	–
nejvíce	nejvíce	k6eAd1	nejvíce
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gInPc2	on
v	v	k7c6	v
podpaží	podpaží	k1gNnSc6	podpaží
<g/>
,	,	kIx,	,
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
na	na	k7c6	na
dlaních	dlaň	k1gFnPc6	dlaň
a	a	k8xC	a
ploskách	ploska	k1gFnPc6	ploska
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Pot	pot	k1gInSc1	pot
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
98,5	[number]	k4	98,5
<g/>
%	%	kIx~	%
až	až	k9	až
99	[number]	k4	99
<g/>
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
NaCl	NaCla	k1gFnPc2	NaCla
a	a	k8xC	a
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
organické	organický	k2eAgFnSc2d1	organická
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
močovinu	močovina	k1gFnSc4	močovina
<g/>
,	,	kIx,	,
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
Tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
z	z	k7c2	z
tkáňového	tkáňový	k2eAgInSc2d1	tkáňový
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
vyloučeného	vyloučený	k2eAgInSc2d1	vyloučený
potu	pot	k1gInSc2	pot
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
na	na	k7c6	na
tělesné	tělesný	k2eAgFnSc6d1	tělesná
námaze	námaha	k1gFnSc6	námaha
<g/>
.	.	kIx.	.
</s>
<s>
Kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
0,5	[number]	k4	0,5
<g/>
l	l	kA	l
do	do	k7c2	do
10	[number]	k4	10
<g/>
l	l	kA	l
a	a	k8xC	a
více	hodně	k6eAd2	hodně
za	za	k7c4	za
24	[number]	k4	24
<g/>
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
</s>
<s>
Mazové	mazový	k2eAgFnPc1d1	mazová
žlázy	žláza	k1gFnPc1	žláza
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vlasy	vlas	k1gInPc1	vlas
a	a	k8xC	a
chlupy	chlup	k1gInPc1	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
dlaních	dlaň	k1gFnPc6	dlaň
a	a	k8xC	a
ploskách	ploska	k1gFnPc6	ploska
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Výměšek	výměšek	k1gInSc1	výměšek
mazových	mazový	k2eAgFnPc2d1	mazová
žláz	žláza	k1gFnPc2	žláza
–	–	k?	–
kožní	kožní	k2eAgInSc1d1	kožní
maz	maz	k1gInSc1	maz
–	–	k?	–
dělá	dělat	k5eAaImIp3nS	dělat
pokožku	pokožka	k1gFnSc4	pokožka
vláčnou	vláčný	k2eAgFnSc4d1	vláčná
a	a	k8xC	a
hebkou	hebký	k2eAgFnSc4d1	hebká
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Chrání	chránit	k5eAaImIp3nP	chránit
také	také	k9	také
vlasy	vlas	k1gInPc1	vlas
a	a	k8xC	a
chlupy	chlup	k1gInPc1	chlup
před	před	k7c7	před
vysycháním	vysychání	k1gNnSc7	vysychání
a	a	k8xC	a
lámáním	lámání	k1gNnSc7	lámání
<g/>
.	.	kIx.	.
</s>
<s>
Apokrinní	Apokrinný	k2eAgMnPc1d1	Apokrinný
žlázy	žláza	k1gFnSc2	žláza
Apokrinní	Apokrinný	k2eAgMnPc1d1	Apokrinný
(	(	kIx(	(
<g/>
sexuální	sexuální	k2eAgFnPc1d1	sexuální
<g/>
,	,	kIx,	,
pachové	pachový	k2eAgFnPc1d1	pachová
<g/>
)	)	kIx)	)
žlázy	žláza	k1gFnPc1	žláza
začínají	začínat	k5eAaImIp3nP	začínat
pracovat	pracovat	k5eAaImF	pracovat
až	až	k9	až
v	v	k7c6	v
pubertě	puberta	k1gFnSc6	puberta
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
podpaží	podpaží	k1gNnSc6	podpaží
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
konečníku	konečník	k1gInSc2	konečník
a	a	k8xC	a
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Produkují	produkovat	k5eAaImIp3nP	produkovat
specificky	specificky	k6eAd1	specificky
zapáchající	zapáchající	k2eAgInPc1d1	zapáchající
výměšky	výměšek	k1gInPc1	výměšek
<g/>
.	.	kIx.	.
</s>
<s>
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
žláza	žláza	k1gFnSc1	žláza
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
žláza	žláza	k1gFnSc1	žláza
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
kožní	kožní	k2eAgFnSc1d1	kožní
žláza	žláza	k1gFnSc1	žláza
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
(	(	kIx(	(
<g/>
embryonálně	embryonálně	k6eAd1	embryonálně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
dívek	dívka	k1gFnPc2	dívka
se	se	k3xPyFc4	se
v	v	k7c6	v
pubertě	puberta	k1gFnSc6	puberta
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
ženských	ženský	k2eAgInPc2d1	ženský
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
hormonů	hormon	k1gInPc2	hormon
<g/>
.	.	kIx.	.
</s>
<s>
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
žláza	žláza	k1gFnSc1	žláza
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
15-20	[number]	k4	15-20
paprsčitě	paprsčitě	k6eAd1	paprsčitě
uspořádanými	uspořádaný	k2eAgInPc7d1	uspořádaný
žlázovými	žlázový	k2eAgInPc7d1	žlázový
laloky	lalok	k1gInPc7	lalok
<g/>
,	,	kIx,	,
obklopenými	obklopený	k2eAgInPc7d1	obklopený
tukovým	tukový	k2eAgNnSc7d1	tukové
vazivem	vazivo	k1gNnSc7	vazivo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lalůčků	lalůček	k1gInPc2	lalůček
vycházejí	vycházet	k5eAaImIp3nP	vycházet
úzké	úzký	k2eAgInPc1d1	úzký
mlékovody	mlékovod	k1gInPc1	mlékovod
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
a	a	k8xC	a
vyúsťují	vyúsťovat	k5eAaImIp3nP	vyúsťovat
na	na	k7c6	na
prsní	prsní	k2eAgFnSc6d1	prsní
bradavce	bradavka	k1gFnSc6	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Barvu	barva	k1gFnSc4	barva
kůže	kůže	k1gFnSc2	kůže
určují	určovat	k5eAaImIp3nP	určovat
její	její	k3xOp3gNnPc4	její
barviva	barvivo	k1gNnPc4	barvivo
–	–	k?	–
melanin	melanin	k1gInSc1	melanin
<g/>
,	,	kIx,	,
karoten	karoten	k1gInSc1	karoten
a	a	k8xC	a
hemoglobin	hemoglobin	k1gInSc1	hemoglobin
<g/>
.	.	kIx.	.
</s>
<s>
Melanin	melanin	k1gInSc1	melanin
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
melanocytech	melanocyt	k1gInPc6	melanocyt
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
červenou	červený	k2eAgFnSc4d1	červená
hnědou	hnědý	k2eAgFnSc4d1	hnědá
až	až	k8xS	až
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
tmavou	tmavý	k2eAgFnSc7d1	tmavá
barvou	barva	k1gFnSc7	barva
kůže	kůže	k1gFnSc2	kůže
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
melanocyty	melanocyt	k1gInPc1	melanocyt
více	hodně	k6eAd2	hodně
melaninu	melanin	k1gInSc2	melanin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tmavší	tmavý	k2eAgInSc1d2	tmavší
než	než	k8xS	než
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	s	k7c7	s
světlou	světlý	k2eAgFnSc7d1	světlá
barvou	barva	k1gFnSc7	barva
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Karoten	karoten	k1gInSc1	karoten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
získáváme	získávat	k5eAaImIp1nP	získávat
například	například	k6eAd1	například
z	z	k7c2	z
mrkve	mrkev	k1gFnSc2	mrkev
je	být	k5eAaImIp3nS	být
oranžové	oranžový	k2eAgNnSc1d1	oranžové
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
,	,	kIx,	,
ukládající	ukládající	k2eAgMnSc1d1	ukládající
se	se	k3xPyFc4	se
v	v	k7c6	v
povrchové	povrchový	k2eAgFnSc6d1	povrchová
vrstvě	vrstva	k1gFnSc6	vrstva
zvané	zvaný	k2eAgFnSc2d1	zvaná
epidermis	epidermis	k1gFnSc2	epidermis
<g/>
.	.	kIx.	.
</s>
<s>
Hemoglobin	hemoglobin	k1gInSc1	hemoglobin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kožních	kožní	k2eAgFnPc6d1	kožní
cévách	céva	k1gFnPc6	céva
<g/>
,	,	kIx,	,
dodává	dodávat	k5eAaImIp3nS	dodávat
růžový	růžový	k2eAgInSc1d1	růžový
nádech	nádech	k1gInSc1	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Choroby	choroba	k1gFnPc1	choroba
kůže	kůže	k1gFnSc2	kůže
<g/>
:	:	kIx,	:
Akné	akné	k1gFnSc1	akné
Ucpání	ucpání	k1gNnSc2	ucpání
vývodů	vývod	k1gInPc2	vývod
mazových	mazový	k2eAgFnPc2d1	mazová
žláz	žláza	k1gFnPc2	žláza
mazem	maz	k1gInSc7	maz
z	z	k7c2	z
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
mazových	mazový	k2eAgFnPc2d1	mazová
žláz	žláza	k1gFnPc2	žláza
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zánětlivé	zánětlivý	k2eAgInPc4d1	zánětlivý
vřídky	vřídek	k1gInPc4	vřídek
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Albinismus	albinismus	k1gInSc1	albinismus
Dědičná	dědičný	k2eAgFnSc1d1	dědičná
vada	vada	k1gFnSc1	vada
<g/>
,	,	kIx,	,
postižený	postižený	k2eAgMnSc1d1	postižený
má	mít	k5eAaImIp3nS	mít
nedostatek	nedostatek	k1gInSc1	nedostatek
barviva	barvivo	k1gNnSc2	barvivo
melaninu	melanin	k1gInSc2	melanin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dává	dávat	k5eAaImIp3nS	dávat
barvu	barva	k1gFnSc4	barva
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
vlasům	vlas	k1gInPc3	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Bradavice	bradavice	k1gFnSc1	bradavice
Bradavice	bradavice	k1gFnSc2	bradavice
je	být	k5eAaImIp3nS	být
nezhoubný	zhoubný	k2eNgInSc1d1	nezhoubný
výrůstek	výrůstek	k1gInSc1	výrůstek
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
nebo	nebo	k8xC	nebo
na	na	k7c4	na
sliznici	sliznice	k1gFnSc4	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Dermatitida	dermatitida	k1gFnSc1	dermatitida
Zánět	zánět	k1gInSc4	zánět
kůže	kůže	k1gFnSc2	kůže
způsobený	způsobený	k2eAgInSc4d1	způsobený
alergií	alergie	k1gFnSc7	alergie
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc7d1	jiná
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Ekzém	ekzém	k1gInSc1	ekzém
Zánět	zánět	k1gInSc1	zánět
kůže	kůže	k1gFnSc2	kůže
provázený	provázený	k2eAgInSc1d1	provázený
svěděním	svědění	k1gNnSc7	svědění
a	a	k8xC	a
puchýři	puchýř	k1gInPc7	puchýř
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
alergický	alergický	k2eAgMnSc1d1	alergický
nebo	nebo	k8xC	nebo
dědičný	dědičný	k2eAgMnSc1d1	dědičný
<g/>
.	.	kIx.	.
</s>
<s>
Kopřivka	kopřivka	k1gFnSc1	kopřivka
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
způsobená	způsobený	k2eAgFnSc1d1	způsobená
alergií	alergie	k1gFnSc7	alergie
na	na	k7c4	na
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
požití	požití	k1gNnSc6	požití
se	se	k3xPyFc4	se
na	na	k7c4	na
kůží	kůže	k1gFnPc2	kůže
objeví	objevit	k5eAaPmIp3nS	objevit
svědící	svědící	k2eAgFnPc4d1	svědící
zanícené	zanícený	k2eAgFnPc4d1	zanícená
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Lupénka	lupénka	k1gFnSc1	lupénka
Nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
kůže	kůže	k1gFnSc1	kůže
zanítí	zanítit	k5eAaPmIp3nS	zanítit
a	a	k8xC	a
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
stříbrnými	stříbrný	k2eAgFnPc7d1	stříbrná
šupinkami	šupinka	k1gFnPc7	šupinka
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc4	potíž
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
lupénka	lupénka	k1gFnSc1	lupénka
velkého	velký	k2eAgInSc2d1	velký
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Mateřské	mateřský	k2eAgNnSc4d1	mateřské
znaménko	znaménko	k1gNnSc4	znaménko
Pigmentová	pigmentový	k2eAgFnSc1d1	pigmentová
skvrna	skvrna	k1gFnSc1	skvrna
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
přítomná	přítomný	k2eAgFnSc1d1	přítomná
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
kolísá	kolísat	k5eAaImIp3nS	kolísat
od	od	k7c2	od
drobných	drobný	k2eAgNnPc2d1	drobné
znamének	znaménko	k1gNnPc2	znaménko
po	po	k7c4	po
velké	velký	k2eAgFnPc4d1	velká
skvrny	skvrna	k1gFnPc4	skvrna
barvy	barva	k1gFnSc2	barva
"	"	kIx"	"
<g/>
portského	portský	k2eAgNnSc2d1	portské
vína	víno	k1gNnSc2	víno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přehřátí	přehřátí	k1gNnPc4	přehřátí
Svědící	svědící	k2eAgFnSc1d1	svědící
červená	červená	k1gFnSc1	červená
kůže	kůže	k1gFnSc2	kůže
s	s	k7c7	s
drobnými	drobný	k2eAgFnPc7d1	drobná
červenými	červený	k2eAgFnPc7d1	červená
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
pocením	pocení	k1gNnSc7	pocení
<g/>
,	,	kIx,	,
způsobená	způsobený	k2eAgFnSc1d1	způsobená
horkem	horko	k1gNnSc7	horko
<g/>
.	.	kIx.	.
</s>
<s>
Rakovina	rakovina	k1gFnSc1	rakovina
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
melanom	melanom	k1gInSc1	melanom
<g/>
)	)	kIx)	)
Spálení	spálení	k1gNnPc2	spálení
sluncem	slunce	k1gNnSc7	slunce
Kožní	kožní	k2eAgInSc1d1	kožní
zánět	zánět	k1gInSc1	zánět
způsobený	způsobený	k2eAgInSc1d1	způsobený
nadměrně	nadměrně	k6eAd1	nadměrně
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
pobytem	pobyt	k1gInSc7	pobyt
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Opakované	opakovaný	k2eAgNnSc1d1	opakované
nadměrně	nadměrně	k6eAd1	nadměrně
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
slunění	slunění	k1gNnSc1	slunění
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Vitiligo	Vitiligo	k6eAd1	Vitiligo
Porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
častější	častý	k2eAgInSc1d2	častější
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
tmavé	tmavý	k2eAgFnSc2d1	tmavá
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
okrsky	okrsek	k1gInPc1	okrsek
kůže	kůže	k1gFnSc1	kůže
ztratí	ztratit	k5eAaPmIp3nP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
materiál	materiál	k1gInSc1	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpracovaná	zpracovaný	k2eAgFnSc1d1	zpracovaná
zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
i	i	k9	i
lidská	lidský	k2eAgFnSc1d1	lidská
<g/>
)	)	kIx)	)
kůže	kůže	k1gFnSc1	kůže
se	se	k3xPyFc4	se
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
využívala	využívat	k5eAaPmAgFnS	využívat
jako	jako	k8xC	jako
oděv	oděv	k1gInSc1	oděv
nebo	nebo	k8xC	nebo
surovina	surovina	k1gFnSc1	surovina
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
oděvních	oděvní	k2eAgInPc2d1	oděvní
doplňků	doplněk	k1gInPc2	doplněk
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1	specifické
využití	využití	k1gNnSc4	využití
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
bicích	bicí	k2eAgInPc2d1	bicí
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
civilizovaných	civilizovaný	k2eAgFnPc6d1	civilizovaná
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
jinými	jiný	k2eAgInPc7d1	jiný
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
