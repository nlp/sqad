<s>
Kůže	kůže	k1gFnSc1	kůže
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
cutis	cutis	k1gInSc1	cutis
<g/>
,	,	kIx,	,
gr	gr	k?	gr
<g/>
.	.	kIx.	.
derma	derma	k1gFnSc1	derma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
zvířat	zvíře	k1gNnPc2	zvíře
kožich	kožich	k1gInSc1	kožich
<g/>
,	,	kIx,	,
kožešina	kožešina	k1gFnSc1	kožešina
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc4	orgán
pokrývající	pokrývající	k2eAgNnPc4d1	pokrývající
těla	tělo	k1gNnPc4	tělo
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
