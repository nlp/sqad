<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
panovnickým	panovnický	k2eAgInSc7d1	panovnický
činem	čin	k1gInSc7	čin
knížete	kníže	k1gMnSc2	kníže
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
Statutu	statut	k1gInSc2	statut
Konráda	Konrád	k1gMnSc2	Konrád
Oty	Ota	k1gMnSc2	Ota
<g/>
,	,	kIx,	,
nejstaršího	starý	k2eAgInSc2d3	nejstarší
českého	český	k2eAgInSc2d1	český
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlášen	k2eAgInSc1d1	vyhlášen
roku	rok	k1gInSc2	rok
1189	[number]	k4	1189
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
velmožů	velmož	k1gMnPc2	velmož
v	v	k7c6	v
Sadské	sadský	k2eAgFnSc6d1	Sadská
u	u	k7c2	u
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
.	.	kIx.	.
</s>
