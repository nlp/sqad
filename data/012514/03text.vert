<p>
<s>
Tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
zelené	zelená	k1gFnSc2	zelená
mající	mající	k2eAgFnSc4d1	mající
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
500	[number]	k4	500
–	–	k?	–
520	[number]	k4	520
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
zelená	zelená	k1gFnSc1	zelená
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
brány	brána	k1gFnPc1	brána
jako	jako	k8xC	jako
odstíny	odstín	k1gInPc1	odstín
tyrkysové	tyrkysový	k2eAgInPc1d1	tyrkysový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
barvy	barva	k1gFnSc2	barva
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
tyrkysu	tyrkys	k1gInSc2	tyrkys
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
staré	starý	k2eAgFnPc4d1	stará
civilizace	civilizace	k1gFnPc4	civilizace
posvátný	posvátný	k2eAgInSc1d1	posvátný
<g/>
,	,	kIx,	,
přinášející	přinášející	k2eAgNnSc4d1	přinášející
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Nalezené	nalezený	k2eAgInPc1d1	nalezený
pohřební	pohřební	k2eAgInPc1d1	pohřební
předměty	předmět	k1gInPc1	předmět
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
Egypta	Egypt	k1gInSc2	Egypt
zdobené	zdobený	k2eAgInPc1d1	zdobený
tyrkysem	tyrkys	k1gInSc7	tyrkys
jsou	být	k5eAaImIp3nP	být
datovány	datovat	k5eAaImNgInP	datovat
do	do	k7c2	do
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
př.n.l.	př.n.l.	k?	př.n.l.
V	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
byl	být	k5eAaImAgInS	být
tyrkysový	tyrkysový	k2eAgInSc1d1	tyrkysový
talisman	talisman	k1gInSc1	talisman
nošen	nošen	k2eAgInSc1d1	nošen
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgMnS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
zdobení	zdobení	k1gNnPc2	zdobení
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
užita	užít	k5eAaPmNgFnS	užít
také	také	k9	také
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
výzdobou	výzdoba	k1gFnSc7	výzdoba
velkých	velký	k2eAgFnPc2d1	velká
mešit	mešita	k1gFnPc2	mešita
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Aztékové	Azték	k1gMnPc1	Azték
tyrkys	tyrkys	k1gInSc4	tyrkys
znali	znát	k5eAaImAgMnP	znát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Chalchihuitl	Chalchihuitl	k1gFnSc2	Chalchihuitl
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
ho	on	k3xPp3gMnSc4	on
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
předmětů	předmět	k1gInPc2	předmět
pro	pro	k7c4	pro
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Tyrkys	tyrkys	k1gInSc1	tyrkys
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tyrkysový	tyrkysový	k2eAgMnSc1d1	tyrkysový
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
