<s>
Tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
užita	užít	k5eAaPmNgFnS	užít
také	také	k9	také
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
i	i	k8xC	i
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
výzdobou	výzdoba	k1gFnSc7	výzdoba
velkých	velký	k2eAgFnPc2d1	velká
mešit	mešita	k1gFnPc2	mešita
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
