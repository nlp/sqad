<s>
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Rossija	Rossija	k1gFnSc1	Rossija
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
Ф	Ф	k?	Ф
<g/>
,	,	kIx,	,
Rossijskaja	Rossijskaja	k1gMnSc1	Rossijskaja
federacija	federacija	k1gMnSc1	federacija
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
17	[number]	k4	17
125	[number]	k4	125
191	[number]	k4	191
km2	km2	k4	km2
největší	veliký	k2eAgInSc4d3	veliký
stát	stát	k1gInSc4	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
severní	severní	k2eAgFnSc4d1	severní
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
počtem	počet	k1gInSc7	počet
146,1	[number]	k4	146,1
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
devátá	devátý	k4xOgFnSc1	devátý
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
země	země	k1gFnSc1	země
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sousedy	soused	k1gMnPc7	soused
Ruska	Rusko	k1gNnSc2	Rusko
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
,	,	kIx,	,
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Kaliningradské	kaliningradský	k2eAgFnSc2d1	Kaliningradská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
strategické	strategický	k2eAgFnSc2d1	strategická
západní	západní	k2eAgFnSc2d1	západní
exklávy	exkláva	k1gFnSc2	exkláva
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgInPc1d1	ruský
Kurilské	kurilský	k2eAgInPc1d1	kurilský
ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c4	na
dohled	dohled	k1gInSc4	dohled
od	od	k7c2	od
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
z	z	k7c2	z
ruské	ruský	k2eAgFnSc2d1	ruská
Čukotky	Čukotka	k1gFnSc2	Čukotka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
na	na	k7c4	na
Aljašku	Aljaška	k1gFnSc4	Aljaška
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
9	[number]	k4	9
časových	časový	k2eAgNnPc2d1	časové
pásem	pásmo	k1gNnPc2	pásmo
a	a	k8xC	a
do	do	k7c2	do
84	[number]	k4	84
samosprávných	samosprávný	k2eAgInPc2d1	samosprávný
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
22	[number]	k4	22
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
následnickým	následnický	k2eAgInSc7d1	následnický
státem	stát	k1gInSc7	stát
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
;	;	kIx,	;
převzala	převzít	k5eAaPmAgFnS	převzít
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vůdčím	vůdčí	k2eAgInSc7d1	vůdčí
členem	člen	k1gInSc7	člen
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
G	G	kA	G
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
OBSE	OBSE	kA	OBSE
<g/>
,	,	kIx,	,
OSKB	OSKB	kA	OSKB
<g/>
,	,	kIx,	,
SCO	SCO	kA	SCO
<g/>
,	,	kIx,	,
APEC	APEC	kA	APEC
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
2014	[number]	k4	2014
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
Rusko	Rusko	k1gNnSc1	Rusko
členem	člen	k1gInSc7	člen
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
také	také	k9	také
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
světových	světový	k2eAgFnPc2d1	světová
vojenských	vojenský	k2eAgFnPc2d1	vojenská
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
ekonomika	ekonomika	k1gFnSc1	ekonomika
od	od	k7c2	od
r.	r.	kA	r.
2016	[number]	k4	2016
opustila	opustit	k5eAaPmAgFnS	opustit
desítku	desítka	k1gFnSc4	desítka
největších	veliký	k2eAgFnPc2d3	veliký
světových	světový	k2eAgFnPc2d1	světová
ekonomik	ekonomika	k1gFnPc2	ekonomika
a	a	k8xC	a
podle	podle	k7c2	podle
nominální	nominální	k2eAgFnSc2d1	nominální
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
dvanáctá	dvanáctý	k4xOgFnSc1	dvanáctý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
objemu	objem	k1gInSc6	objem
celkového	celkový	k2eAgNnSc2d1	celkové
světového	světový	k2eAgNnSc2d1	světové
HDP	HDP	kA	HDP
a	a	k8xC	a
šestá	šestý	k4xOgFnSc1	šestý
v	v	k7c6	v
HDP	HDP	kA	HDP
dle	dle	k7c2	dle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
71	[number]	k4	71
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
ze	z	k7c2	z
187	[number]	k4	187
sledovaných	sledovaný	k2eAgFnPc2d1	sledovaná
zemí	zem	k1gFnPc2	zem
Mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
světa	svět	k1gInSc2	svět
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnPc4d3	veliký
zásoby	zásoba	k1gFnPc4	zásoba
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
–	–	k?	–
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
–	–	k?	–
z	z	k7c2	z
energetického	energetický	k2eAgNnSc2d1	energetické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
Rusko	Rusko	k1gNnSc1	Rusko
soběstačným	soběstačný	k2eAgInSc7d1	soběstačný
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
hospodářství	hospodářství	k1gNnSc1	hospodářství
prodělalo	prodělat	k5eAaPmAgNnS	prodělat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vleklou	vleklý	k2eAgFnSc4d1	vleklá
krizi	krize	k1gFnSc4	krize
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc4	období
silného	silný	k2eAgInSc2d1	silný
růstu	růst	k1gInSc2	růst
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2000	[number]	k4	2000
až	až	k9	až
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
světovou	světový	k2eAgFnSc7d1	světová
finanční	finanční	k2eAgFnSc7d1	finanční
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
období	období	k1gNnSc6	období
2008	[number]	k4	2008
až	až	k9	až
2009	[number]	k4	2009
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
ruská	ruský	k2eAgFnSc1d1	ruská
ekonomika	ekonomika	k1gFnSc1	ekonomika
pokles	pokles	k1gInSc4	pokles
způsobený	způsobený	k2eAgInSc1d1	způsobený
snížením	snížení	k1gNnSc7	snížení
cen	cena	k1gFnPc2	cena
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
sankcemi	sankce	k1gFnPc7	sankce
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
asi	asi	k9	asi
na	na	k7c6	na
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
světového	světový	k2eAgInSc2d1	světový
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
za	za	k7c4	za
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
pravoslavné	pravoslavný	k2eAgNnSc4d1	pravoslavné
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zemi	zem	k1gFnSc3	zem
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
období	období	k1gNnSc1	období
ateistického	ateistický	k2eAgInSc2d1	ateistický
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
jsou	být	k5eAaImIp3nP	být
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
judaismus	judaismus	k1gInSc1	judaismus
a	a	k8xC	a
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
ruské	ruský	k2eAgFnSc2d1	ruská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
literatura	literatura	k1gFnSc1	literatura
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
světového	světový	k2eAgInSc2d1	světový
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
současné	současný	k2eAgNnSc1d1	současné
Rusko	Rusko	k1gNnSc1	Rusko
mnohonárodnostním	mnohonárodnostní	k2eAgInSc7d1	mnohonárodnostní
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kromě	kromě	k7c2	kromě
úřední	úřední	k2eAgFnSc2d1	úřední
ruštiny	ruština	k1gFnSc2	ruština
uznává	uznávat	k5eAaImIp3nS	uznávat
také	také	k9	také
regionální	regionální	k2eAgInSc4d1	regionální
jazyky	jazyk	k1gInPc4	jazyk
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
tvoří	tvořit	k5eAaImIp3nP	tvořit
80	[number]	k4	80
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
menšinou	menšina	k1gFnSc7	menšina
jsou	být	k5eAaImIp3nP	být
Tataři	Tatar	k1gMnPc1	Tatar
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
mnohé	mnohý	k2eAgInPc1d1	mnohý
národy	národ	k1gInPc1	národ
ruského	ruský	k2eAgInSc2d1	ruský
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rus	Rus	k1gMnSc1	Rus
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
země	zem	k1gFnSc2	zem
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
formě	forma	k1gFnSc6	forma
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
poč	poč	k?	poč
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
ruštiny	ruština	k1gFnSc2	ruština
převzato	převzít	k5eAaPmNgNnS	převzít
ze	z	k7c2	z
středověké	středověký	k2eAgFnSc2d1	středověká
řečtiny	řečtina	k1gFnSc2	řečtina
–	–	k?	–
byzantské	byzantský	k2eAgInPc1d1	byzantský
prameny	pramen	k1gInPc1	pramen
nazývaly	nazývat	k5eAaImAgInP	nazývat
zemi	zem	k1gFnSc4	zem
východních	východní	k2eAgInPc2d1	východní
Slovanů	Slovan	k1gInPc2	Slovan
Ρ	Ρ	k?	Ρ
<g/>
,	,	kIx,	,
Rhóssía	Rhóssía	k1gFnSc1	Rhóssía
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc4	tento
řecký	řecký	k2eAgInSc4d1	řecký
výraz	výraz	k1gInSc4	výraz
sám	sám	k3xTgInSc4	sám
byl	být	k5eAaImAgInS	být
mladší	mladý	k2eAgFnSc7d2	mladší
variantou	varianta	k1gFnSc7	varianta
slova	slovo	k1gNnSc2	slovo
Ρ	Ρ	k?	Ρ
<g/>
,	,	kIx,	,
Rhós	Rhós	k1gInSc1	Rhós
neboli	neboli	k8xC	neboli
Rus	Rus	k1gMnSc1	Rus
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Rus	Rus	k1gMnSc1	Rus
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označujícího	označující	k2eAgInSc2d1	označující
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
prostor	prostora	k1gFnPc2	prostora
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
evropského	evropský	k2eAgNnSc2d1	Evropské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Etymologie	etymologie	k1gFnSc1	etymologie
názvu	název	k1gInSc2	název
Rus	Rus	k1gFnSc1	Rus
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejběžněji	běžně	k6eAd3	běžně
se	se	k3xPyFc4	se
přijímá	přijímat	k5eAaImIp3nS	přijímat
tzv.	tzv.	kA	tzv.
normanská	normanský	k2eAgFnSc1d1	normanská
teorie	teorie	k1gFnSc1	teorie
<g/>
:	:	kIx,	:
termín	termín	k1gInSc1	termín
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
veslaři	veslař	k1gMnPc1	veslař
<g/>
"	"	kIx"	"
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
kraje	kraj	k1gInSc2	kraj
Roden	Rodna	k1gFnPc2	Rodna
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zvaného	zvaný	k2eAgInSc2d1	zvaný
Roslagen	Roslagen	k1gInSc1	Roslagen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
bylo	být	k5eAaImAgNnS	být
pobřeží	pobřeží	k1gNnSc1	pobřeží
Svealandu	Svealand	k1gInSc2	Svealand
<g/>
,	,	kIx,	,
středního	střední	k2eAgNnSc2d1	střední
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Rodenu	Roden	k1gInSc2	Roden
se	se	k3xPyFc4	se
varjagové	varjag	k1gMnPc1	varjag
(	(	kIx(	(
<g/>
švédští	švédský	k2eAgMnPc1d1	švédský
vikingové	viking	k1gMnPc1	viking
čili	čili	k8xC	čili
Normani	Norman	k1gMnPc1	Norman
<g/>
)	)	kIx)	)
vydávali	vydávat	k5eAaPmAgMnP	vydávat
po	po	k7c6	po
mořích	moře	k1gNnPc6	moře
a	a	k8xC	a
řekách	řeka	k1gFnPc6	řeka
do	do	k7c2	do
východoevropských	východoevropský	k2eAgFnPc2d1	východoevropská
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
Černomoří	Černomoří	k1gNnPc2	Černomoří
<g/>
,	,	kIx,	,
Rus	Rus	k1gMnSc1	Rus
částečně	částečně	k6eAd1	částečně
kolonizovali	kolonizovat	k5eAaBmAgMnP	kolonizovat
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
posléze	posléze	k6eAd1	posléze
se	s	k7c7	s
Slovany	Slovan	k1gMnPc7	Slovan
splynula	splynout	k5eAaPmAgFnS	splynout
<g/>
.	.	kIx.	.
</s>
<s>
Germánské	germánský	k2eAgNnSc1d1	germánské
jméno	jméno	k1gNnSc1	jméno
tak	tak	k6eAd1	tak
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
slovanštiny	slovanština	k1gFnSc2	slovanština
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
etnonymum	etnonymum	k1gNnSc1	etnonymum
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
geonymum	geonymum	k1gNnSc1	geonymum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
odvozující	odvozující	k2eAgNnSc1d1	odvozující
jméno	jméno	k1gNnSc1	jméno
Ruska	Rusko	k1gNnSc2	Rusko
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
kraje	kraj	k1gInSc2	kraj
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
podporu	podpora	k1gFnSc4	podpora
jak	jak	k8xS	jak
ve	v	k7c6	v
zprávách	zpráva	k1gFnPc6	zpráva
kronik	kronika	k1gFnPc2	kronika
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
lingvistických	lingvistický	k2eAgFnPc6d1	lingvistická
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
:	:	kIx,	:
s	s	k7c7	s
ruštinou	ruština	k1gFnSc7	ruština
a	a	k8xC	a
švédštinou	švédština	k1gFnSc7	švédština
sousedící	sousedící	k2eAgInPc1d1	sousedící
ugrofinské	ugrofinský	k2eAgInPc1d1	ugrofinský
jazyky	jazyk	k1gInPc1	jazyk
totiž	totiž	k9	totiž
označují	označovat	k5eAaImIp3nP	označovat
Švédsko	Švédsko	k1gNnSc4	Švédsko
výrazy	výraz	k1gInPc4	výraz
takřka	takřka	k6eAd1	takřka
shodnými	shodný	k2eAgInPc7d1	shodný
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Rossija	Rossij	k1gInSc2	Rossij
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
<g />
.	.	kIx.	.
</s>
<s>
Ruotsi	Ruots	k1gMnSc3	Ruots
<g/>
,	,	kIx,	,
Ruoŧ	Ruoŧ	k1gMnSc3	Ruoŧ
<g/>
,	,	kIx,	,
Rootsi	Roots	k1gMnSc3	Roots
<g/>
,	,	kIx,	,
Roodsi	Roods	k1gMnSc3	Roods
<g/>
,	,	kIx,	,
Ročinma	Ročinma	k1gFnSc1	Ročinma
apod.	apod.	kA	apod.
Kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
normanské	normanský	k2eAgFnSc2d1	normanská
<g/>
"	"	kIx"	"
teorie	teorie	k1gFnSc2	teorie
ovšem	ovšem	k9	ovšem
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
dovozující	dovozující	k2eAgFnPc1d1	dovozující
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
Rus	Rus	k1gMnSc1	Rus
pochází	pocházet	k5eAaImIp3nS	pocházet
buď	buď	k8xC	buď
<g/>
:	:	kIx,	:
od	od	k7c2	od
základu	základ	k1gInSc2	základ
indoevropského	indoevropský	k2eAgNnSc2d1	indoevropské
slova	slovo	k1gNnSc2	slovo
ruksa	ruks	k1gMnSc2	ruks
či	či	k8xC	či
russa	russ	k1gMnSc2	russ
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
světlý	světlý	k2eAgInSc1d1	světlý
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
od	od	k7c2	od
hypotetického	hypotetický	k2eAgNnSc2d1	hypotetické
praslovanského	praslovanský	k2eAgNnSc2d1	praslovanské
slova	slovo	k1gNnSc2	slovo
р	р	k?	р
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
"	"	kIx"	"
–	–	k?	–
posvátné	posvátný	k2eAgNnSc4d1	posvátné
zvíře	zvíře	k1gNnSc4	zvíře
starých	starý	k2eAgInPc2d1	starý
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příbuzného	příbuzný	k2eAgNnSc2d1	příbuzné
s	s	k7c7	s
latinským	latinský	k2eAgNnSc7d1	latinské
ursus	ursus	k1gInSc1	ursus
a	a	k8xC	a
starořeckým	starořecký	k2eAgNnSc7d1	starořecké
α	α	k?	α
<g/>
,	,	kIx,	,
arktos	arktos	k1gInSc1	arktos
anebo	anebo	k8xC	anebo
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
řeky	řeka	k1gFnSc2	řeka
Ros	Rosa	k1gFnPc2	Rosa
(	(	kIx(	(
<g/>
pravý	pravý	k2eAgInSc1d1	pravý
přítok	přítok	k1gInSc1	přítok
Dněpru	Dněpr	k1gInSc2	Dněpr
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k9	ani
tyto	tento	k3xDgFnPc1	tento
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jiné	jiný	k2eAgInPc1d1	jiný
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
obecně	obecně	k6eAd1	obecně
lingvisty	lingvista	k1gMnPc4	lingvista
a	a	k8xC	a
historiky	historik	k1gMnPc4	historik
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většina	k1gFnSc7	většina
ani	ani	k8xC	ani
ruskými	ruský	k2eAgInPc7d1	ruský
<g/>
)	)	kIx)	)
uznávány	uznávat	k5eAaImNgInP	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
historie	historie	k1gFnSc2	historie
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
křižovatkou	křižovatka	k1gFnSc7	křižovatka
euroasijského	euroasijský	k2eAgInSc2d1	euroasijský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
mostem	most	k1gInSc7	most
mezi	mezi	k7c7	mezi
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Asií	Asie	k1gFnSc7	Asie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
se	se	k3xPyFc4	se
usídlili	usídlit	k5eAaPmAgMnP	usídlit
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
řeky	řeka	k1gFnSc2	řeka
Don	dona	k1gFnPc2	dona
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
20	[number]	k4	20
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vtiskli	vtisknout	k5eAaPmAgMnP	vtisknout
Rusku	Ruska	k1gFnSc4	Ruska
jeho	on	k3xPp3gInSc4	on
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
střídavě	střídavě	k6eAd1	střídavě
ovládány	ovládán	k2eAgInPc4d1	ovládán
různými	různý	k2eAgInPc7d1	různý
kmeny	kmen	k1gInPc7	kmen
(	(	kIx(	(
<g/>
Skythové	Skyth	k1gMnPc1	Skyth
<g/>
,	,	kIx,	,
Hunové	Hun	k1gMnPc1	Hun
<g/>
,	,	kIx,	,
Avaři	Avar	k1gMnPc1	Avar
<g/>
,	,	kIx,	,
Chazaři	Chazar	k1gMnPc1	Chazar
<g/>
,	,	kIx,	,
Volžští	volžský	k2eAgMnPc1d1	volžský
Bulhaři	Bulhar	k1gMnPc1	Bulhar
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
Východních	východní	k2eAgInPc2d1	východní
Slovanů	Slovan	k1gInPc2	Slovan
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
rozdělených	rozdělená	k1gFnPc2	rozdělená
na	na	k7c4	na
Rusy	Rus	k1gMnPc4	Rus
<g/>
,	,	kIx,	,
Bělorusy	Bělorus	k1gMnPc4	Bělorus
a	a	k8xC	a
Ukrajince	Ukrajinec	k1gMnPc4	Ukrajinec
<g/>
)	)	kIx)	)
vznikal	vznikat	k5eAaImAgInS	vznikat
mezi	mezi	k7c7	mezi
Baltským	baltský	k2eAgMnSc7d1	baltský
a	a	k8xC	a
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
vedly	vést	k5eAaImAgFnP	vést
obchodní	obchodní	k2eAgFnPc1d1	obchodní
stezky	stezka	k1gFnPc1	stezka
mezi	mezi	k7c7	mezi
Skandinávií	Skandinávie	k1gFnSc7	Skandinávie
a	a	k8xC	a
Byzantskou	byzantský	k2eAgFnSc7d1	byzantská
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
dějin	dějiny	k1gFnPc2	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
založení	založení	k1gNnSc1	založení
Novgorodu	Novgorod	k1gInSc2	Novgorod
roku	rok	k1gInSc2	rok
862	[number]	k4	862
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
882	[number]	k4	882
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
novgorodský	novgorodský	k2eAgMnSc1d1	novgorodský
kníže	kníže	k1gMnSc1	kníže
Oleg	Oleg	k1gMnSc1	Oleg
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
Kyjev	Kyjev	k1gInSc4	Kyjev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
předtím	předtím	k6eAd1	předtím
platil	platit	k5eAaImAgInS	platit
tribut	tribut	k1gInSc1	tribut
Chazarské	chazarský	k2eAgFnSc3d1	Chazarská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Oleg	Oleg	k1gMnSc1	Oleg
založil	založit	k5eAaPmAgMnS	založit
Kyjevskou	kyjevský	k2eAgFnSc4d1	Kyjevská
Rus	Rus	k1gFnSc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojený	k2eAgMnPc1d1	spojený
Slované	Slovan	k1gMnPc1	Slovan
poté	poté	k6eAd1	poté
dobyli	dobýt	k5eAaPmAgMnP	dobýt
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Chazarů	Chazar	k1gMnPc2	Chazar
a	a	k8xC	a
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
prosperujících	prosperující	k2eAgInPc2d1	prosperující
států	stát	k1gInPc2	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
988	[number]	k4	988
přijal	přijmout	k5eAaPmAgMnS	přijmout
kníže	kníže	k1gMnSc1	kníže
Vladimír	Vladimír	k1gMnSc1	Vladimír
I.	I.	kA	I.
křest	křest	k1gInSc1	křest
z	z	k7c2	z
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zařadilo	zařadit	k5eAaPmAgNnS	zařadit
Rus	Rus	k1gFnSc4	Rus
mezi	mezi	k7c4	mezi
východokřesťanské	východokřesťanský	k2eAgFnPc4d1	východokřesťanský
(	(	kIx(	(
<g/>
od	od	k7c2	od
rozkolu	rozkol	k1gInSc2	rozkol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1054	[number]	k4	1054
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
<g/>
)	)	kIx)	)
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1020	[number]	k4	1020
vydal	vydat	k5eAaPmAgMnS	vydat
kníže	kníže	k1gMnSc1	kníže
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Moudrý	moudrý	k2eAgInSc4d1	moudrý
první	první	k4xOgInSc4	první
zákoník	zákoník	k1gInSc4	zákoník
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Ruskou	ruský	k2eAgFnSc4d1	ruská
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Rus	Rus	k1gFnSc1	Rus
oslabena	oslabit	k5eAaPmNgFnS	oslabit
neustálými	neustálý	k2eAgInPc7d1	neustálý
nájezdy	nájezd	k1gInPc7	nájezd
kočovných	kočovný	k2eAgInPc2d1	kočovný
turkických	turkický	k2eAgInPc2d1	turkický
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
populace	populace	k1gFnSc1	populace
začala	začít	k5eAaPmAgFnS	začít
migrovat	migrovat	k5eAaImF	migrovat
na	na	k7c4	na
bezpečnější	bezpečný	k2eAgInSc4d2	bezpečnější
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
zalesněné	zalesněný	k2eAgFnSc2d1	zalesněná
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
rozpory	rozpor	k1gInPc7	rozpor
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
a	a	k8xC	a
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
invazí	invaze	k1gFnSc7	invaze
mongolských	mongolský	k2eAgFnPc2d1	mongolská
hord	horda	k1gFnPc2	horda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Batu-chana	Batuhan	k1gMnSc2	Batu-chan
porážku	porážka	k1gFnSc4	porážka
(	(	kIx(	(
<g/>
1237	[number]	k4	1237
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ovládnuta	ovládnout	k5eAaPmNgFnS	ovládnout
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
Hordou	horda	k1gFnSc7	horda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
muslimských	muslimský	k2eAgMnPc2d1	muslimský
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
Novgorod	Novgorod	k1gInSc1	Novgorod
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Pskovem	Pskovo	k1gNnSc7	Pskovo
udržel	udržet	k5eAaPmAgInS	udržet
poté	poté	k6eAd1	poté
jistou	jistý	k2eAgFnSc4d1	jistá
míru	míra	k1gFnSc4	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Expanzi	expanze	k1gFnSc4	expanze
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Švédů	Švéd	k1gMnPc2	Švéd
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
kníže	kníže	k1gMnSc1	kníže
Alexandr	Alexandr	k1gMnSc1	Alexandr
Něvský	něvský	k2eAgMnSc1d1	něvský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Něvě	Něva	k1gFnSc6	Něva
(	(	kIx(	(
<g/>
1240	[number]	k4	1240
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
zastavil	zastavit	k5eAaPmAgInS	zastavit
vpád	vpád	k1gInSc4	vpád
Řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
ledové	ledový	k2eAgFnSc6d1	ledová
bitvě	bitva	k1gFnSc6	bitva
roku	rok	k1gInSc2	rok
1242	[number]	k4	1242
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
Alexandra	Alexandr	k1gMnSc2	Alexandr
Něvského	něvský	k2eAgMnSc2d1	něvský
z	z	k7c2	z
novgorodské	novgorodský	k2eAgFnSc2d1	Novgorodská
rurikovské	rurikovský	k2eAgFnSc2d1	rurikovský
větve	větev	k1gFnSc2	větev
vládli	vládnout	k5eAaImAgMnP	vládnout
Vladimiru	Vladimira	k1gFnSc4	Vladimira
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
učinili	učinit	k5eAaPmAgMnP	učinit
Moskvu	Moskva	k1gFnSc4	Moskva
svým	svůj	k3xOyFgNnSc7	svůj
sídelním	sídelní	k2eAgNnSc7d1	sídelní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
velkoknížectví	velkoknížectví	k1gNnSc3	velkoknížectví
a	a	k8xC	a
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Horda	horda	k1gFnSc1	horda
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
nabývat	nabývat	k5eAaImF	nabývat
na	na	k7c6	na
významu	význam	k1gInSc6	význam
Moskevské	moskevský	k2eAgNnSc1d1	moskevské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
1147	[number]	k4	1147
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
sídlem	sídlo	k1gNnSc7	sídlo
hlavy	hlava	k1gFnSc2	hlava
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
ruské	ruský	k2eAgFnSc2d1	ruská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
však	však	k9	však
byla	být	k5eAaImAgFnS	být
nadále	nadále	k6eAd1	nadále
poddaná	poddaný	k2eAgFnSc1d1	poddaná
Zlaté	zlatý	k2eAgFnSc3d1	zlatá
hordě	horda	k1gFnSc3	horda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
musela	muset	k5eAaImAgFnS	muset
odvádět	odvádět	k5eAaImF	odvádět
daně	daň	k1gFnPc4	daň
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
těžké	těžký	k2eAgInPc1d1	těžký
časy	čas	k1gInPc1	čas
s	s	k7c7	s
častými	častý	k2eAgInPc7d1	častý
mongolsko-tatarskými	mongolskoatarský	k2eAgInPc7d1	mongolsko-tatarský
nájezdy	nájezd	k1gInPc7	nájezd
<g/>
,	,	kIx,	,
strádáním	strádání	k1gNnSc7	strádání
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc6	rozšíření
různých	různý	k2eAgFnPc2d1	různá
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
však	však	k9	však
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
Rusko	Rusko	k1gNnSc4	Rusko
tak	tak	k6eAd1	tak
silně	silně	k6eAd1	silně
jako	jako	k8xC	jako
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
populaci	populace	k1gFnSc4	populace
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
brzy	brzy	k6eAd1	brzy
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1380	[number]	k4	1380
moskevský	moskevský	k2eAgMnSc1d1	moskevský
velkokníže	velkokníže	k1gMnSc1	velkokníže
Dimitrij	Dimitrij	k1gFnSc2	Dimitrij
Donský	donský	k2eAgMnSc1d1	donský
porazil	porazit	k5eAaPmAgMnS	porazit
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
hordu	horda	k1gFnSc4	horda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Kulikově	Kulikův	k2eAgNnSc6d1	Kulikovo
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
postupně	postupně	k6eAd1	postupně
vstřebala	vstřebat	k5eAaPmAgFnS	vstřebat
okolní	okolní	k2eAgNnSc4d1	okolní
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Tveru	Tver	k1gInSc2	Tver
a	a	k8xC	a
Novgorodu	Novgorod	k1gInSc2	Novgorod
<g/>
,	,	kIx,	,
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
však	však	k9	však
odváděla	odvádět	k5eAaImAgFnS	odvádět
poplatky	poplatek	k1gInPc4	poplatek
Zlaté	zlatý	k2eAgFnSc3d1	zlatá
hordě	horda	k1gFnSc3	horda
<g/>
.	.	kIx.	.
</s>
<s>
Moskevský	moskevský	k2eAgInSc1d1	moskevský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
na	na	k7c6	na
Tatarech	tatar	k1gInPc6	tatar
plně	plně	k6eAd1	plně
nezávislým	závislý	k2eNgInSc7d1	nezávislý
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1480	[number]	k4	1480
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Ivana	Ivan	k1gMnSc2	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
území	území	k1gNnSc4	území
Rusi	Rus	k1gFnSc2	Rus
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgFnPc1	některý
západní	západní	k2eAgFnPc1d1	západní
oblasti	oblast	k1gFnPc1	oblast
včetně	včetně	k7c2	včetně
Kyjeva	Kyjev	k1gInSc2	Kyjev
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Polsko-litevského	polskoitevský	k2eAgInSc2d1	polsko-litevský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tataři	Tatar	k1gMnPc1	Tatar
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
dodnes	dodnes	k6eAd1	dodnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejpočetnější	početní	k2eAgFnSc4d3	nejpočetnější
menšinu	menšina	k1gFnSc4	menšina
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
koncentrovanou	koncentrovaný	k2eAgFnSc4d1	koncentrovaná
zejména	zejména	k9	zejména
v	v	k7c6	v
Tatarstánu	Tatarstán	k1gInSc6	Tatarstán
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
neteří	neteř	k1gFnSc7	neteř
posledního	poslední	k2eAgMnSc2d1	poslední
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
XI	XI	kA	XI
<g/>
,	,	kIx,	,
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
znak	znak	k1gInSc1	znak
byzantského	byzantský	k2eAgMnSc2d1	byzantský
dvouhlavého	dvouhlavý	k2eAgMnSc2d1	dvouhlavý
orla	orel	k1gMnSc2	orel
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
také	také	k9	také
jako	jako	k9	jako
ruský	ruský	k2eAgInSc1d1	ruský
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruské	ruský	k2eAgNnSc1d1	ruské
carství	carství	k1gNnSc1	carství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1547	[number]	k4	1547
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Ivan	Ivan	k1gMnSc1	Ivan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Vasiljevič	Vasiljevič	k1gInSc1	Vasiljevič
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Hrozný	hrozný	k2eAgInSc1d1	hrozný
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgInS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
carem	car	k1gMnSc7	car
a	a	k8xC	a
nastolil	nastolit	k5eAaPmAgInS	nastolit
tak	tak	k9	tak
tzv.	tzv.	kA	tzv.
samoděržaví	samoděržaví	k1gNnSc4	samoděržaví
<g/>
:	:	kIx,	:
centralistickou	centralistický	k2eAgFnSc4d1	centralistická
absolutní	absolutní	k2eAgFnSc4d1	absolutní
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
ruské	ruský	k2eAgNnSc1d1	ruské
teritorium	teritorium	k1gNnSc1	teritorium
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
zdvojnásobilo	zdvojnásobit	k5eAaPmAgNnS	zdvojnásobit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
transkontinentální	transkontinentální	k2eAgInSc4d1	transkontinentální
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
<g/>
,	,	kIx,	,
jediní	jediný	k2eAgMnPc1d1	jediný
zbývající	zbývající	k2eAgMnPc1d1	zbývající
zástupci	zástupce	k1gMnPc1	zástupce
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Hordy	horda	k1gFnSc2	horda
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
nájezdech	nájezd	k1gInPc6	nájezd
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
odváděli	odvádět	k5eAaImAgMnP	odvádět
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
obyvatele	obyvatel	k1gMnPc4	obyvatel
do	do	k7c2	do
osmanského	osmanský	k2eAgNnSc2d1	osmanské
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Zabraňovali	zabraňovat	k5eAaImAgMnP	zabraňovat
tak	tak	k6eAd1	tak
většímu	veliký	k2eAgNnSc3d2	veliký
osídlení	osídlení	k1gNnSc3	osídlení
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
také	také	k9	také
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
zdevastovali	zdevastovat	k5eAaPmAgMnP	zdevastovat
Rjazaň	Rjazaň	k1gFnSc4	Rjazaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
jen	jen	k9	jen
se	s	k7c7	s
slabým	slabý	k2eAgInSc7d1	slabý
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1571	[number]	k4	1571
Krymští	krymský	k2eAgMnPc1d1	krymský
Tataři	Tatar	k1gMnPc1	Tatar
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
osmanští	osmanský	k2eAgMnPc1d1	osmanský
spojenci	spojenec	k1gMnPc1	spojenec
(	(	kIx(	(
<g/>
80	[number]	k4	80
000	[number]	k4	000
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
,	,	kIx,	,
33	[number]	k4	33
000	[number]	k4	000
Turků	Turek	k1gMnPc2	Turek
a	a	k8xC	a
7	[number]	k4	7
000	[number]	k4	000
janičárů	janičár	k1gMnPc2	janičár
<g/>
)	)	kIx)	)
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
centrálního	centrální	k2eAgNnSc2d1	centrální
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
Ugrou	Ugra	k1gFnSc7	Ugra
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
křídlo	křídlo	k1gNnSc4	křídlo
ruského	ruský	k2eAgNnSc2d1	ruské
vojska	vojsko	k1gNnSc2	vojsko
se	s	k7c7	s
6	[number]	k4	6
000	[number]	k4	000
bojovníky	bojovník	k1gMnPc7	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
neměla	mít	k5eNaImAgFnS	mít
sílu	síla	k1gFnSc4	síla
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
invaze	invaze	k1gFnSc2	invaze
<g/>
,	,	kIx,	,
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Venkovské	venkovský	k2eAgNnSc1d1	venkovské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
také	také	k9	také
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
Krymských	krymský	k2eAgMnPc2d1	krymský
Tatarů	Tatar	k1gMnPc2	Tatar
zdevastovala	zdevastovat	k5eAaPmAgFnS	zdevastovat
města	město	k1gNnSc2	město
a	a	k8xC	a
vesnice	vesnice	k1gFnSc2	vesnice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
poté	poté	k6eAd1	poté
vypálila	vypálit	k5eAaPmAgFnS	vypálit
většinu	většina	k1gFnSc4	většina
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tatarské	tatarský	k2eAgFnSc6d1	tatarská
invazi	invaze	k1gFnSc6	invaze
padlo	padnout	k5eAaImAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
80	[number]	k4	80
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
150	[number]	k4	150
000	[number]	k4	000
Rusů	Rus	k1gMnPc2	Rus
bylo	být	k5eAaImAgNnS	být
odvlečeno	odvléct	k5eAaPmNgNnS	odvléct
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
krymský	krymský	k2eAgMnSc1d1	krymský
chán	chán	k1gMnSc1	chán
Devlet	Devlet	k1gInSc1	Devlet
Giraj	Giraj	k1gInSc1	Giraj
<g/>
,	,	kIx,	,
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
plánoval	plánovat	k5eAaImAgMnS	plánovat
plné	plný	k2eAgNnSc4d1	plné
dobytí	dobytí	k1gNnSc4	dobytí
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
poražena	porazit	k5eAaPmNgFnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Molodi	Molod	k1gMnPc1	Molod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
eliminovalo	eliminovat	k5eAaBmAgNnS	eliminovat
hrozbu	hrozba	k1gFnSc4	hrozba
krymsko-osmanské	krymskosmanský	k2eAgFnSc2d1	krymsko-osmanský
expanze	expanze	k1gFnSc2	expanze
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Nájezdy	nájezd	k1gInPc4	nájezd
Tatarů	Tatar	k1gMnPc2	Tatar
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zajímání	zajímání	k1gNnSc2	zajímání
otroků	otrok	k1gMnPc2	otrok
však	však	k9	však
neustaly	ustat	k5eNaPmAgInP	ustat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
byli	být	k5eAaImAgMnP	být
odvlékáni	odvlékat	k5eAaImNgMnP	odvlékat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
počtech	počet	k1gInPc6	počet
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgNnPc2d1	nové
opevnění	opevnění	k1gNnPc2	opevnění
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
jižním	jižní	k2eAgNnSc6d1	jižní
Rusku	Rusko	k1gNnSc6	Rusko
neustále	neustále	k6eAd1	neustále
zužovala	zužovat	k5eAaImAgFnS	zužovat
prostor	prostor	k1gInSc4	prostor
přístupný	přístupný	k2eAgInSc4d1	přístupný
pro	pro	k7c4	pro
nájezdy	nájezd	k1gInPc4	nájezd
Krymských	krymský	k2eAgMnPc2d1	krymský
Tatarů	Tatar	k1gMnPc2	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
Rozmach	rozmach	k1gInSc1	rozmach
ruského	ruský	k2eAgNnSc2d1	ruské
carství	carství	k1gNnSc2	carství
se	se	k3xPyFc4	se
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
dynastie	dynastie	k1gFnSc2	dynastie
Rurikovců	Rurikovec	k1gMnPc2	Rurikovec
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Polsko-litevská	polskoitevský	k2eAgFnSc1d1	polsko-litevská
unie	unie	k1gFnSc1	unie
obsadila	obsadit	k5eAaPmAgFnS	obsadit
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1612	[number]	k4	1612
však	však	k9	však
byli	být	k5eAaImAgMnP	být
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Litevci	Litevec	k1gMnPc1	Litevec
nuceni	nucen	k2eAgMnPc1d1	nucen
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
sbor	sbor	k1gInSc1	sbor
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Kuzmy	Kuzma	k1gFnSc2	Kuzma
Minina	Minin	k2eAgMnSc2d1	Minin
a	a	k8xC	a
knížete	kníže	k1gMnSc2	kníže
Dmitrije	Dmitrije	k1gMnSc2	Dmitrije
Požarského	Požarský	k2eAgMnSc2d1	Požarský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
zvolen	zvolit	k5eAaPmNgInS	zvolit
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
Michail	Michail	k1gMnSc1	Michail
I.	I.	kA	I.
Romanov	Romanov	k1gInSc1	Romanov
<g/>
.	.	kIx.	.
</s>
<s>
Carská	carský	k2eAgFnSc1d1	carská
dynastie	dynastie	k1gFnSc1	dynastie
Romanovců	Romanovec	k1gMnPc2	Romanovec
následně	následně	k6eAd1	následně
vládla	vládnout	k5eAaImAgFnS	vládnout
Rusku	Ruska	k1gFnSc4	Ruska
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Rusko	Rusko	k1gNnSc1	Rusko
kolonizovalo	kolonizovat	k5eAaBmAgNnS	kolonizovat
většinu	většina	k1gFnSc4	většina
Sibiře	Sibiř	k1gFnSc2	Sibiř
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
Rusové	Rus	k1gMnPc1	Rus
poprvé	poprvé	k6eAd1	poprvé
přešli	přejít	k5eAaPmAgMnP	přejít
Beringův	Beringův	k2eAgInSc4d1	Beringův
průliv	průliv	k1gInSc4	průliv
mezi	mezi	k7c7	mezi
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
se	se	k3xPyFc4	se
rolníci	rolník	k1gMnPc1	rolník
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
záporožským	záporožský	k2eAgInPc3d1	záporožský
kozákům	kozák	k1gInPc3	kozák
ve	v	k7c6	v
vzpouře	vzpoura	k1gFnSc6	vzpoura
proti	proti	k7c3	proti
polsko-litevskému	polskoitevský	k2eAgNnSc3d1	polsko-litevské
společenství	společenství	k1gNnSc3	společenství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1654	[number]	k4	1654
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
vůdce	vůdce	k1gMnSc1	vůdce
Bohdan	Bohdan	k1gMnSc1	Bohdan
Chmelnický	Chmelnický	k2eAgMnSc1d1	Chmelnický
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
ruskému	ruský	k2eAgMnSc3d1	ruský
carovi	car	k1gMnSc3	car
Alexeji	Alexej	k1gMnSc3	Alexej
I.	I.	kA	I.
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
oddělena	oddělit	k5eAaPmNgNnP	oddělit
od	od	k7c2	od
Polsko-litevské	polskoitevský	k2eAgFnSc2d1	polsko-litevská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
postavena	postaven	k2eAgFnSc1d1	postavena
pod	pod	k7c4	pod
jeho	jeho	k3xOp3gFnSc4	jeho
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
Alexej	Alexej	k1gMnSc1	Alexej
I.	I.	kA	I.
tuto	tento	k3xDgFnSc4	tento
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
rusko-polské	ruskoolský	k2eAgFnSc3d1	rusko-polská
válce	válka	k1gFnSc3	válka
(	(	kIx(	(
<g/>
1654	[number]	k4	1654
<g/>
–	–	k?	–
<g/>
1667	[number]	k4	1667
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
podél	podél	k7c2	podél
Dněpru	Dněpr	k1gInSc2	Dněpr
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
připadla	připadnout	k5eAaPmAgFnS	připadnout
Polsku	Polsko	k1gNnSc3	Polsko
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ruským	ruský	k2eAgMnSc7d1	ruský
carem	car	k1gMnSc7	car
Petr	Petr	k1gMnSc1	Petr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
svými	svůj	k3xOyFgFnPc7	svůj
odvážnými	odvážný	k2eAgFnPc7d1	odvážná
reformami	reforma	k1gFnPc7	reforma
a	a	k8xC	a
modernizací	modernizace	k1gFnSc7	modernizace
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
válce	válka	k1gFnSc6	válka
nad	nad	k7c7	nad
Švédskem	Švédsko	k1gNnSc7	Švédsko
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
<g/>
–	–	k?	–
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc4d1	zaveden
název	název	k1gInSc4	název
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
ze	z	k7c2	z
získaných	získaný	k2eAgNnPc2d1	získané
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Něvy	Něva	k1gFnSc2	Něva
<g/>
)	)	kIx)	)
založil	založit	k5eAaPmAgInS	založit
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1721	[number]	k4	1721
také	také	k9	také
přijal	přijmout	k5eAaPmAgMnS	přijmout
Petr	Petr	k1gMnSc1	Petr
titul	titul	k1gInSc4	titul
imperátora	imperátor	k1gMnSc2	imperátor
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Kateřiny	Kateřina	k1gFnSc2	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliké	veliký	k2eAgInPc4d1	veliký
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
přinesla	přinést	k5eAaPmAgFnS	přinést
osvícenské	osvícenský	k2eAgFnPc4d1	osvícenská
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
počátek	počátek	k1gInSc4	počátek
rozvoje	rozvoj	k1gInSc2	rozvoj
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
stavbu	stavba	k1gFnSc4	stavba
obrovských	obrovský	k2eAgInPc2d1	obrovský
paláců	palác	k1gInPc2	palác
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
územní	územní	k2eAgInPc1d1	územní
zisky	zisk	k1gInPc1	zisk
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
Krymu	Krym	k1gInSc6	Krym
v	v	k7c6	v
opakovaných	opakovaný	k2eAgFnPc6d1	opakovaná
válkách	válka	k1gFnPc6	válka
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
zisk	zisk	k1gInSc1	zisk
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
postupně	postupně	k6eAd1	postupně
rozdělovaného	rozdělovaný	k2eAgNnSc2d1	rozdělované
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
impérium	impérium	k1gNnSc1	impérium
se	se	k3xPyFc4	se
rozrostlo	rozrůst	k5eAaPmAgNnS	rozrůst
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
území	území	k1gNnSc4	území
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
až	až	k9	až
po	po	k7c4	po
Aljašku	Aljaška	k1gFnSc4	Aljaška
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gruzii	Gruzie	k1gFnSc4	Gruzie
(	(	kIx(	(
<g/>
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rusko-švédské	rusko-švédský	k2eAgFnSc2d1	rusko-švédská
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
k	k	k7c3	k
Rusku	Rusko	k1gNnSc6	Rusko
připojeno	připojen	k2eAgNnSc1d1	připojeno
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
<g/>
–	–	k?	–
<g/>
1806	[number]	k4	1806
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
první	první	k4xOgNnSc1	první
ruské	ruský	k2eAgNnSc1d1	ruské
obeplutí	obeplutí	k1gNnSc1	obeplutí
země	zem	k1gFnSc2	zem
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
ruská	ruský	k2eAgFnSc1d1	ruská
expedice	expedice	k1gFnSc1	expedice
objevila	objevit	k5eAaPmAgFnS	objevit
kontinent	kontinent	k1gInSc4	kontinent
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
země	zem	k1gFnSc2	zem
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gNnSc1	jeho
tažení	tažení	k1gNnSc1	tažení
zakončené	zakončený	k2eAgNnSc1d1	zakončené
okupací	okupace	k1gFnSc7	okupace
Moskvy	Moskva	k1gFnSc2	Moskva
skončilo	skončit	k5eAaPmAgNnS	skončit
debaklem	debakl	k1gInSc7	debakl
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
byl	být	k5eAaImAgInS	být
poražen	porazit	k5eAaPmNgInS	porazit
a	a	k8xC	a
Ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
přehnala	přehnat	k5eAaPmAgFnS	přehnat
přes	přes	k7c4	přes
Evropu	Evropa	k1gFnSc4	Evropa
až	až	k9	až
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
ruské	ruský	k2eAgFnSc2d1	ruská
delegace	delegace	k1gFnSc2	delegace
Vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
definoval	definovat	k5eAaBmAgInS	definovat
mapy	mapa	k1gFnPc4	mapa
v	v	k7c6	v
po-napoleonské	poapoleonský	k2eAgFnSc6d1	po-napoleonský
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
oslabila	oslabit	k5eAaPmAgFnS	oslabit
Krymská	krymský	k2eAgFnSc1d1	Krymská
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říší	říš	k1gFnSc7	říš
v	v	k7c6	v
letech	let	k1gInPc6	let
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
bojoval	bojovat	k5eAaImAgMnS	bojovat
mj.	mj.	kA	mj.
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářsky	hospodářsky	k6eAd1	hospodářsky
zaostalé	zaostalý	k2eAgNnSc1d1	zaostalé
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
postupně	postupně	k6eAd1	postupně
modernizovat	modernizovat	k5eAaBmF	modernizovat
(	(	kIx(	(
<g/>
roku	rok	k1gInSc3	rok
1861	[number]	k4	1861
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
nevolnictví	nevolnictví	k1gNnSc2	nevolnictví
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
držet	držet	k5eAaImF	držet
krok	krok	k1gInSc4	krok
se	s	k7c7	s
západními	západní	k2eAgFnPc7d1	západní
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
vzestup	vzestup	k1gInSc1	vzestup
byl	být	k5eAaImAgInS	být
vystřídán	vystřídat	k5eAaPmNgInS	vystřídat
krizí	krize	k1gFnSc7	krize
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
probíhala	probíhat	k5eAaImAgFnS	probíhat
Rusko-japonská	ruskoaponský	k2eAgFnSc1d1	rusko-japonská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
poraženo	porazit	k5eAaPmNgNnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
proti	proti	k7c3	proti
carství	carství	k1gNnSc3	carství
(	(	kIx(	(
<g/>
vyjádřená	vyjádřený	k2eAgNnPc1d1	vyjádřené
již	již	k6eAd1	již
povstáním	povstání	k1gNnSc7	povstání
Děkabristů	děkabrista	k1gMnPc2	děkabrista
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
či	či	k8xC	či
teroristickými	teroristický	k2eAgFnPc7d1	teroristická
akcemi	akce	k1gFnPc7	akce
hnutí	hnutí	k1gNnPc2	hnutí
Zemlja	Zemlj	k2eAgFnSc1d1	Zemlj
i	i	k8xC	i
Volja	Volj	k2eAgFnSc1d1	Volj
<g/>
)	)	kIx)	)
sílila	sílit	k5eAaImAgFnS	sílit
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
intelektuálů	intelektuál	k1gMnPc2	intelektuál
a	a	k8xC	a
příslušníků	příslušník	k1gMnPc2	příslušník
neruských	ruský	k2eNgInPc2d1	neruský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
tvrdé	tvrdá	k1gFnPc1	tvrdá
rusifikaci	rusifikace	k1gFnSc4	rusifikace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poražené	poražený	k2eAgFnSc6d1	poražená
první	první	k4xOgFnSc6	první
ruské	ruský	k2eAgFnSc6d1	ruská
revoluci	revoluce	k1gFnSc6	revoluce
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
zřízen	zřízen	k2eAgInSc1d1	zřízen
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
duma	duma	k1gFnSc1	duma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
absolutismus	absolutismus	k1gInSc1	absolutismus
však	však	k9	však
oslaben	oslaben	k2eAgMnSc1d1	oslaben
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
pak	pak	k6eAd1	pak
přinesla	přinést	k5eAaPmAgFnS	přinést
rozklad	rozklad	k1gInSc4	rozklad
státu	stát	k1gInSc2	stát
a	a	k8xC	a
nejprve	nejprve	k6eAd1	nejprve
únorovou	únorový	k2eAgFnSc4d1	únorová
a	a	k8xC	a
následně	následně	k6eAd1	následně
radikálnější	radikální	k2eAgFnSc4d2	radikálnější
Velkou	velký	k2eAgFnSc4d1	velká
říjnovou	říjnový	k2eAgFnSc4d1	říjnová
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
Rusko	Rusko	k1gNnSc4	Rusko
v	v	k7c4	v
socialistický	socialistický	k2eAgInSc4d1	socialistický
stát	stát	k1gInSc4	stát
a	a	k8xC	a
uvedla	uvést	k5eAaPmAgFnS	uvést
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
bolševiky	bolševik	k1gMnPc7	bolševik
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
někdejší	někdejší	k2eAgMnSc1d1	někdejší
car	car	k1gMnSc1	car
ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
civilním	civilní	k2eAgNnSc7d1	civilní
jménem	jméno	k1gNnSc7	jméno
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Romanov	Romanov	k1gInSc1	Romanov
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
Sovětské	sovětský	k2eAgInPc4d1	sovětský
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
ustavena	ustaven	k2eAgFnSc1d1	ustavena
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Leninem	Lenin	k1gMnSc7	Lenin
<g/>
,	,	kIx,	,
revolucionářem	revolucionář	k1gMnSc7	revolucionář
a	a	k8xC	a
marxistickým	marxistický	k2eAgMnSc7d1	marxistický
intelektuálem	intelektuál	k1gMnSc7	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Ruská	ruský	k2eAgFnSc1d1	ruská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
federativní	federativní	k2eAgFnSc1d1	federativní
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
RSFSR	RSFSR	kA	RSFSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíchž	jejíž	k3xOyRp3gInPc6	jejíž
postupem	postup	k1gInSc7	postup
doby	doba	k1gFnSc2	doba
pozměněných	pozměněný	k2eAgFnPc6d1	pozměněná
hranicích	hranice	k1gFnPc6	hranice
existuje	existovat	k5eAaImIp3nS	existovat
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
opět	opět	k6eAd1	opět
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Rusko	Rusko	k1gNnSc1	Rusko
separátní	separátní	k2eAgFnSc4d1	separátní
Brest-litevskou	Brestitevský	k2eAgFnSc4d1	Brest-litevský
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
ukončující	ukončující	k2eAgFnSc2d1	ukončující
první	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
ztráty	ztráta	k1gFnSc2	ztráta
většiny	většina	k1gFnSc2	většina
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Besarábie	Besarábie	k1gFnSc2	Besarábie
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
upadla	upadnout	k5eAaPmAgFnS	upadnout
do	do	k7c2	do
vleklé	vleklý	k2eAgFnSc2d1	vleklá
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
trvající	trvající	k2eAgInSc1d1	trvající
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
;	;	kIx,	;
proti	proti	k7c3	proti
komunistům	komunista	k1gMnPc3	komunista
povstalo	povstat	k5eAaPmAgNnS	povstat
mnoho	mnoho	k4c1	mnoho
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Bílá	bílý	k2eAgFnSc1d1	bílá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
Zelená	zelený	k2eAgFnSc1d1	zelená
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
nejednotní	jednotný	k2eNgMnPc1d1	nejednotný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nemohli	moct	k5eNaImAgMnP	moct
být	být	k5eAaImF	být
silným	silný	k2eAgMnSc7d1	silný
protivníkem	protivník	k1gMnSc7	protivník
proti	proti	k7c3	proti
bolševikům	bolševik	k1gMnPc3	bolševik
podporovaným	podporovaný	k2eAgNnSc7d1	podporované
Rakousko-Uherskem	Rakousko-Uhersko	k1gNnSc7	Rakousko-Uhersko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
přes	přes	k7c4	přes
intervenci	intervence	k1gFnSc4	intervence
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Českoslovenští	československý	k2eAgMnPc1d1	československý
legionáři	legionář	k1gMnPc1	legionář
dobyli	dobýt	k5eAaPmAgMnP	dobýt
všechna	všechen	k3xTgNnPc4	všechen
velká	velký	k2eAgNnPc4d1	velké
města	město	k1gNnPc4	město
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
cara	car	k1gMnSc2	car
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
již	již	k9	již
zachránit	zachránit	k5eAaPmF	zachránit
v	v	k7c6	v
Jekatěrinburgu	Jekatěrinburg	k1gInSc6	Jekatěrinburg
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
týden	týden	k1gInSc4	týden
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
legií	legie	k1gFnPc2	legie
do	do	k7c2	do
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
vyvražděna	vyvraždit	k5eAaPmNgFnS	vyvraždit
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
příkaz	příkaz	k1gInSc4	příkaz
rudých	rudý	k1gMnPc2	rudý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
odešla	odejít	k5eAaPmAgFnS	odejít
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
vědců	vědec	k1gMnPc2	vědec
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
Lenin	Lenin	k1gMnSc1	Lenin
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
novou	nový	k2eAgFnSc4d1	nová
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
konsolidovat	konsolidovat	k5eAaBmF	konsolidovat
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Leninovým	Leninův	k2eAgFnPc3d1	Leninova
zdravotním	zdravotní	k2eAgFnPc3d1	zdravotní
komplikacím	komplikace	k1gFnPc3	komplikace
byl	být	k5eAaImAgInS	být
nově	nově	k6eAd1	nově
zřízen	zřízen	k2eAgInSc1d1	zřízen
úřad	úřad	k1gInSc1	úřad
Generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSSS	KSSS	kA	KSSS
<g/>
;	;	kIx,	;
dosazen	dosadit	k5eAaPmNgMnS	dosadit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
byl	být	k5eAaImAgMnS	být
Josif	Josif	k1gMnSc1	Josif
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
úspěšně	úspěšně	k6eAd1	úspěšně
získat	získat	k5eAaPmF	získat
co	co	k3yInSc4	co
nejvíce	hodně	k6eAd3	hodně
moci	moct	k5eAaImF	moct
a	a	k8xC	a
odstranit	odstranit	k5eAaPmF	odstranit
ideové	ideový	k2eAgMnPc4d1	ideový
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
trockisty	trockista	k1gMnSc2	trockista
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1922	[number]	k4	1922
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
RSFSR	RSFSR	kA	RSFSR
<g/>
,	,	kIx,	,
Ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
Běloruské	běloruský	k2eAgFnPc4d1	Běloruská
SSR	SSR	kA	SSR
a	a	k8xC	a
Zakavkazské	zakavkazský	k2eAgFnSc2d1	zakavkazská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Leninově	Leninův	k2eAgFnSc6d1	Leninova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
moci	moct	k5eAaImF	moct
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Probíhala	probíhat	k5eAaImAgFnS	probíhat
násilná	násilný	k2eAgFnSc1d1	násilná
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc1	budování
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
elektrifikace	elektrifikace	k1gFnSc2	elektrifikace
země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
se	se	k3xPyFc4	se
gramotnost	gramotnost	k1gFnSc1	gramotnost
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
či	či	k8xC	či
dostupnost	dostupnost	k1gFnSc4	dostupnost
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
rozvoj	rozvoj	k1gInSc1	rozvoj
země	zem	k1gFnSc2	zem
probíhal	probíhat	k5eAaImAgInS	probíhat
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
likvidace	likvidace	k1gFnSc2	likvidace
odpůrců	odpůrce	k1gMnPc2	odpůrce
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
kolektivizace	kolektivizace	k1gFnSc2	kolektivizace
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
internováni	internovat	k5eAaBmNgMnP	internovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
gulagů	gulag	k1gInPc2	gulag
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
Dálný	dálný	k2eAgInSc4d1	dálný
Východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřely	zemřít	k5eAaPmAgFnP	zemřít
milióny	milión	k4xCgInPc4	milión
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obdobím	období	k1gNnSc7	období
další	další	k2eAgFnSc2d1	další
rusifikace	rusifikace	k1gFnSc2	rusifikace
neruských	ruský	k2eNgInPc2d1	neruský
národů	národ	k1gInPc2	národ
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
brutálních	brutální	k2eAgFnPc2d1	brutální
stalinských	stalinský	k2eAgFnPc2d1	stalinská
čistek	čistka	k1gFnPc2	čistka
NKVD	NKVD	kA	NKVD
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
hitlerovské	hitlerovský	k2eAgNnSc1d1	hitlerovské
Německo	Německo	k1gNnSc1	Německo
porušilo	porušit	k5eAaPmAgNnS	porušit
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
(	(	kIx(	(
<g/>
pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
<g/>
)	)	kIx)	)
a	a	k8xC	a
bez	bez	k7c2	bez
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
Velká	velký	k2eAgFnSc1d1	velká
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
většinu	většina	k1gFnSc4	většina
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
těžce	těžce	k6eAd1	těžce
poničena	poničen	k2eAgFnSc1d1	poničena
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
bojích	boj	k1gInPc6	boj
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
a	a	k8xC	a
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Kurska	Kursk	k1gInSc2	Kursk
začíná	začínat	k5eAaImIp3nS	začínat
obrat	obrat	k1gInSc1	obrat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
země	země	k1gFnSc1	země
od	od	k7c2	od
okupantů	okupant	k1gMnPc2	okupant
osvobozena	osvobozen	k2eAgFnSc1d1	osvobozena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
pak	pak	k6eAd1	pak
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
tzv.	tzv.	kA	tzv.
jaltská	jaltský	k2eAgFnSc1d1	Jaltská
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Stalinovi	Stalin	k1gMnSc3	Stalin
podařilo	podařit	k5eAaPmAgNnS	podařit
upevnit	upevnit	k5eAaPmF	upevnit
sovětský	sovětský	k2eAgInSc4d1	sovětský
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
získal	získat	k5eAaPmAgInS	získat
porážkou	porážka	k1gFnSc7	porážka
nacistů	nacista	k1gMnPc2	nacista
velkou	velký	k2eAgFnSc4d1	velká
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
etablování	etablování	k1gNnSc3	etablování
komunistických	komunistický	k2eAgFnPc2d1	komunistická
diktatur	diktatura	k1gFnPc2	diktatura
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
tzv.	tzv.	kA	tzv.
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
včetně	včetně	k7c2	včetně
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
je	být	k5eAaImIp3nS	být
SSSR	SSSR	kA	SSSR
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
tzv.	tzv.	kA	tzv.
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
zemřel	zemřít	k5eAaPmAgMnS	zemřít
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
generalissimus	generalissimus	k1gMnSc1	generalissimus
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
Nikita	Nikita	k1gMnSc1	Nikita
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
,	,	kIx,	,
v	v	k7c6	v
tajném	tajný	k2eAgInSc6d1	tajný
projevu	projev	k1gInSc6	projev
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
tzv.	tzv.	kA	tzv.
kult	kult	k1gInSc4	kult
osobnosti	osobnost	k1gFnSc2	osobnost
stalinského	stalinský	k2eAgNnSc2d1	stalinské
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc4d1	následující
desetiletí	desetiletí	k1gNnSc4	desetiletí
provází	provázet	k5eAaImIp3nS	provázet
určité	určitý	k2eAgNnSc4d1	určité
oslabení	oslabení	k1gNnSc4	oslabení
represí	represe	k1gFnPc2	represe
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
sovětský	sovětský	k2eAgInSc1d1	sovětský
kosmický	kosmický	k2eAgInSc1d1	kosmický
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
daroval	darovat	k5eAaPmAgMnS	darovat
Chruščov	Chruščov	k1gInSc4	Chruščov
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
poloostrov	poloostrov	k1gInSc1	poloostrov
Krym	Krym	k1gInSc1	Krym
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
300	[number]	k4	300
let	léto	k1gNnPc2	léto
od	od	k7c2	od
kozáckého	kozácký	k2eAgNnSc2d1	kozácké
shromáždění	shromáždění	k1gNnSc2	shromáždění
Perejaslavské	Perejaslavský	k2eAgFnSc2d1	Perejaslavský
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
připojení	připojení	k1gNnSc2	připojení
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
k	k	k7c3	k
Ruskému	ruský	k2eAgNnSc3d1	ruské
impériu	impérium	k1gNnSc3	impérium
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
vyvstaly	vyvstat	k5eAaPmAgInP	vyvstat
při	při	k7c6	při
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
převážně	převážně	k6eAd1	převážně
ruskojazyčném	ruskojazyčný	k2eAgInSc6d1	ruskojazyčný
Krymu	Krym	k1gInSc6	Krym
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
existovaly	existovat	k5eAaImAgFnP	existovat
separatistické	separatistický	k2eAgFnPc1d1	separatistická
tendence	tendence	k1gFnPc1	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
Chruščovova	Chruščovův	k2eAgMnSc2d1	Chruščovův
následníka	následník	k1gMnSc2	následník
Brežněva	Brežněv	k1gMnSc2	Brežněv
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obdobím	období	k1gNnSc7	období
tzv.	tzv.	kA	tzv.
ustrnutí	ustrnutí	k1gNnSc2	ustrnutí
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
již	již	k6eAd1	již
provázena	provázen	k2eAgFnSc1d1	provázena
represemi	represe	k1gFnPc7	represe
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bránila	bránit	k5eAaImAgFnS	bránit
se	se	k3xPyFc4	se
jakékoli	jakýkoli	k3yIgFnSc3	jakýkoli
modernizaci	modernizace	k1gFnSc3	modernizace
a	a	k8xC	a
způsobila	způsobit	k5eAaPmAgFnS	způsobit
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
propad	propad	k1gInSc4	propad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
začala	začít	k5eAaPmAgFnS	začít
sovětská	sovětský	k2eAgFnSc1d1	sovětská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Brežněvově	Brežněvův	k2eAgFnSc6d1	Brežněvova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
po	po	k7c6	po
přechodném	přechodný	k2eAgNnSc6d1	přechodné
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
krátkém	krátký	k2eAgNnSc6d1	krátké
působení	působení	k1gNnSc6	působení
Jurije	Jurije	k1gFnSc4	Jurije
Andropova	Andropův	k2eAgMnSc2d1	Andropův
a	a	k8xC	a
Konstantina	Konstantin	k1gMnSc2	Konstantin
Černěnka	Černěnka	k1gFnSc1	Černěnka
v	v	k7c6	v
čelných	čelný	k2eAgFnPc6d1	čelná
funkcích	funkce	k1gFnPc6	funkce
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
stává	stávat	k5eAaImIp3nS	stávat
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
KSSS	KSSS	kA	KSSS
Michail	Michail	k1gInSc1	Michail
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
období	období	k1gNnSc4	období
perestrojky	perestrojka	k1gFnSc2	perestrojka
a	a	k8xC	a
glasnosti	glasnost	k1gFnSc2	glasnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
reformování	reformování	k1gNnSc1	reformování
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
skutečným	skutečný	k2eAgInSc7d1	skutečný
důsledkem	důsledek	k1gInSc7	důsledek
jeho	jeho	k3xOp3gFnSc4	jeho
postupná	postupný	k2eAgFnSc1d1	postupná
dezintegrace	dezintegrace	k1gFnSc1	dezintegrace
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
reorganizován	reorganizovat	k5eAaBmNgInS	reorganizovat
ve	v	k7c6	v
volnější	volný	k2eAgFnSc6d2	volnější
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
většina	většina	k1gFnSc1	většina
voličů	volič	k1gInPc2	volič
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
funkce	funkce	k1gFnSc1	funkce
prezidenta	prezident	k1gMnSc2	prezident
RSFSR	RSFSR	kA	RSFSR
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Ruska	Rusko	k1gNnSc2	Rusko
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Boris	Boris	k1gMnSc1	Boris
Jelcin	Jelcin	k1gMnSc1	Jelcin
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
srpnový	srpnový	k2eAgInSc4d1	srpnový
puč	puč	k1gInSc4	puč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zrušení	zrušení	k1gNnSc1	zrušení
demokratických	demokratický	k2eAgFnPc2d1	demokratická
přeměn	přeměna	k1gFnPc2	přeměna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
potlačen	potlačit	k5eAaPmNgInS	potlačit
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
pak	pak	k6eAd1	pak
hlavy	hlava	k1gFnPc4	hlava
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
podepisují	podepisovat	k5eAaImIp3nP	podepisovat
Bělověžskou	Bělověžský	k2eAgFnSc4d1	Bělověžská
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
prohlášení	prohlášení	k1gNnSc4	prohlášení
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
stanovící	stanovící	k2eAgNnSc1d1	stanovící
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
SSSR	SSSR	kA	SSSR
jako	jako	k8xC	jako
subjekt	subjekt	k1gInSc1	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
přestal	přestat	k5eAaPmAgMnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
se	se	k3xPyFc4	se
SSSR	SSSR	kA	SSSR
oficiálně	oficiálně	k6eAd1	oficiálně
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
Ruskou	ruský	k2eAgFnSc4d1	ruská
federaci	federace	k1gFnSc4	federace
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
14	[number]	k4	14
postsovětských	postsovětský	k2eAgFnPc2d1	postsovětská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1993	[number]	k4	1993
propukla	propuknout	k5eAaPmAgFnS	propuknout
tzv.	tzv.	kA	tzv.
ústavní	ústavní	k2eAgFnSc2d1	ústavní
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
Jelcin	Jelcin	k1gMnSc1	Jelcin
podporoval	podporovat	k5eAaImAgInS	podporovat
kurz	kurz	k1gInSc4	kurz
radikální	radikální	k2eAgFnSc2d1	radikální
privatizace	privatizace	k1gFnSc2	privatizace
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Jelcin	Jelcin	k1gMnSc1	Jelcin
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
a	a	k8xC	a
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
referenda	referendum	k1gNnSc2	referendum
o	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
pravomocech	pravomoc	k1gFnPc6	pravomoc
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
ruský	ruský	k2eAgInSc1d1	ruský
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
protestovalo	protestovat	k5eAaBmAgNnS	protestovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
krvavě	krvavě	k6eAd1	krvavě
potlačeno	potlačen	k2eAgNnSc4d1	potlačeno
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Éra	éra	k1gFnSc1	éra
Borise	Boris	k1gMnSc2	Boris
Jelcina	Jelcin	k1gMnSc2	Jelcin
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
obdobím	období	k1gNnSc7	období
bolestného	bolestný	k2eAgInSc2d1	bolestný
přechodu	přechod	k1gInSc2	přechod
ke	k	k7c3	k
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
privatizace	privatizace	k1gFnSc2	privatizace
<g/>
,	,	kIx,	,
krachu	krach	k1gInSc2	krach
velkých	velký	k2eAgInPc2d1	velký
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
nárůstu	nárůst	k1gInSc3	nárůst
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
,	,	kIx,	,
ztráty	ztráta	k1gFnSc2	ztráta
super-velmocenského	superelmocenský	k2eAgNnSc2d1	super-velmocenský
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
pomalému	pomalý	k2eAgNnSc3d1	pomalé
a	a	k8xC	a
problematickému	problematický	k2eAgNnSc3d1	problematické
etablování	etablování	k1gNnSc3	etablování
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
nemá	mít	k5eNaImIp3nS	mít
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
probíhá	probíhat	k5eAaImIp3nS	probíhat
první	první	k4xOgInSc4	první
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
čečenská	čečenský	k2eAgFnSc1d1	čečenská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
svazu	svaz	k1gInSc2	svaz
s	s	k7c7	s
Lukašenkovým	Lukašenkový	k2eAgNnSc7d1	Lukašenkový
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Jelcin	Jelcin	k1gMnSc1	Jelcin
spustil	spustit	k5eAaPmAgMnS	spustit
vlnu	vlna	k1gFnSc4	vlna
privatizace	privatizace	k1gFnSc2	privatizace
podniků	podnik	k1gInPc2	podnik
včetně	včetně	k7c2	včetně
nalezišť	naleziště	k1gNnPc2	naleziště
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
krocích	krok	k1gInPc6	krok
<g/>
,	,	kIx,	,
prodeje	prodej	k1gFnPc1	prodej
majetku	majetek	k1gInSc2	majetek
pod	pod	k7c7	pod
cenou	cena	k1gFnSc7	cena
<g/>
,	,	kIx,	,
profitoval	profitovat	k5eAaBmAgMnS	profitovat
poměrně	poměrně	k6eAd1	poměrně
úzký	úzký	k2eAgInSc4d1	úzký
okruh	okruh	k1gInSc4	okruh
osob	osoba	k1gFnPc2	osoba
kolem	kolem	k7c2	kolem
Jelcina	Jelcin	k1gMnSc2	Jelcin
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
těžební	těžební	k2eAgFnSc2d1	těžební
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
okruhu	okruh	k1gInSc2	okruh
osob	osoba	k1gFnPc2	osoba
údajně	údajně	k6eAd1	údajně
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
tzv.	tzv.	kA	tzv.
oligarchové	oligarch	k1gMnPc1	oligarch
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
prezident	prezident	k1gMnSc1	prezident
Jelcin	Jelcin	k1gMnSc1	Jelcin
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
výkonem	výkon	k1gInSc7	výkon
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
pravomocí	pravomoc	k1gFnPc2	pravomoc
předsedu	předseda	k1gMnSc4	předseda
ruské	ruský	k2eAgFnSc2d1	ruská
vlády	vláda	k1gFnSc2	vláda
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Putina	putin	k2eAgMnSc2d1	putin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2000	[number]	k4	2000
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
druhým	druhý	k4xOgMnSc7	druhý
prezidentem	prezident	k1gMnSc7	prezident
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
;	;	kIx,	;
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
značné	značný	k2eAgFnPc4d1	značná
popularity	popularita	k1gFnPc4	popularita
a	a	k8xC	a
mandát	mandát	k1gInSc4	mandát
obhájil	obhájit	k5eAaPmAgMnS	obhájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
se	se	k3xPyFc4	se
vliv	vliv	k1gInSc4	vliv
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
sjednocené	sjednocený	k2eAgNnSc1d1	sjednocené
s	s	k7c7	s
exilovou	exilový	k2eAgFnSc7d1	exilová
větví	větev	k1gFnSc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
rozpočet	rozpočet	k1gInSc1	rozpočet
začal	začít	k5eAaPmAgInS	začít
dosahovat	dosahovat	k5eAaImF	dosahovat
přebytků	přebytek	k1gInPc2	přebytek
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
rozvoji	rozvoj	k1gInSc6	rozvoj
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
<g/>
,	,	kIx,	,
růstu	růst	k1gInSc2	růst
reálných	reálný	k2eAgInPc2d1	reálný
příjmů	příjem	k1gInPc2	příjem
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
Dmitrij	Dmitrij	k1gFnSc2	Dmitrij
Medveděv	Medveděv	k1gMnSc7	Medveděv
<g/>
,	,	kIx,	,
však	však	k9	však
země	země	k1gFnSc1	země
začala	začít	k5eAaPmAgFnS	začít
čelit	čelit	k5eAaImF	čelit
globální	globální	k2eAgFnSc3d1	globální
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
<g/>
;	;	kIx,	;
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
jakožto	jakožto	k8xS	jakožto
vývozce	vývozce	k1gMnSc1	vývozce
surovin	surovina	k1gFnPc2	surovina
těžce	těžce	k6eAd1	těžce
postiženo	postižen	k2eAgNnSc1d1	postiženo
snížením	snížení	k1gNnSc7	snížení
světové	světový	k2eAgFnSc2d1	světová
poptávky	poptávka	k1gFnSc2	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
Rusko	Rusko	k1gNnSc1	Rusko
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Osetii	Osetie	k1gFnSc6	Osetie
<g/>
.	.	kIx.	.
</s>
<s>
Putin	putin	k2eAgMnSc1d1	putin
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
2012	[number]	k4	2012
a	a	k8xC	a
Medveděv	Medveděv	k1gMnSc1	Medveděv
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
komentátory	komentátor	k1gMnPc4	komentátor
hodnoceny	hodnotit	k5eAaImNgFnP	hodnotit
přes	přes	k7c4	přes
menší	malý	k2eAgFnPc4d2	menší
výtky	výtka	k1gFnPc4	výtka
jako	jako	k8xS	jako
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
Rusko	Rusko	k1gNnSc1	Rusko
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
dění	dění	k1gNnSc2	dění
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
poloostrov	poloostrov	k1gInSc1	poloostrov
Krym	Krym	k1gInSc1	Krym
na	na	k7c6	na
základě	základ	k1gInSc6	základ
referenda	referendum	k1gNnSc2	referendum
ze	z	k7c2	z
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
jednostranně	jednostranně	k6eAd1	jednostranně
jako	jako	k8xC	jako
federální	federální	k2eAgInSc4d1	federální
subjekt	subjekt	k1gInSc4	subjekt
Republika	republika	k1gFnSc1	republika
Krym	Krym	k1gInSc1	Krym
a	a	k8xC	a
město	město	k1gNnSc4	město
Sevastopol	Sevastopol	k1gInSc1	Sevastopol
do	do	k7c2	do
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
leteckými	letecký	k2eAgInPc7d1	letecký
a	a	k8xC	a
raketovými	raketový	k2eAgInPc7d1	raketový
údery	úder	k1gInPc7	úder
do	do	k7c2	do
Syrské	syrský	k2eAgFnSc2d1	Syrská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
militantním	militantní	k2eAgFnPc3d1	militantní
skupinám	skupina	k1gFnPc3	skupina
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Fronty	fronta	k1gFnSc2	fronta
an-Nusrá	an-Nusrý	k2eAgFnSc1d1	an-Nusrý
(	(	kIx(	(
<g/>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
v	v	k7c6	v
Levantě	Levanta	k1gFnSc6	Levanta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Džaíš	Džaíš	k1gFnSc1	Džaíš
al-Fatah	al-Fataha	k1gFnPc2	al-Fataha
(	(	kIx(	(
<g/>
radikální	radikální	k2eAgMnPc1d1	radikální
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Putin	putin	k2eAgMnSc1d1	putin
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
Francois	Francois	k1gMnSc1	Francois
Hollande	Holland	k1gInSc5	Holland
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
vojenské	vojenský	k2eAgFnSc6d1	vojenská
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
společně	společně	k6eAd1	společně
kooperují	kooperovat	k5eAaImIp3nP	kooperovat
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
Islámský	islámský	k2eAgInSc4d1	islámský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
přišla	přijít	k5eAaPmAgFnS	přijít
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
výbuchu	výbuch	k1gInSc2	výbuch
ruského	ruský	k2eAgNnSc2d1	ruské
letadla	letadlo	k1gNnSc2	letadlo
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
<g/>
,	,	kIx,	,
postsovětského	postsovětský	k2eAgNnSc2d1	postsovětské
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
propukla	propuknout	k5eAaPmAgFnS	propuknout
hluboká	hluboký	k2eAgFnSc1d1	hluboká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
celá	celý	k2eAgFnSc1d1	celá
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
útlumu	útlum	k1gInSc6	útlum
<g/>
,	,	kIx,	,
odcházeli	odcházet	k5eAaImAgMnP	odcházet
do	do	k7c2	do
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
zločin	zločin	k1gInSc1	zločin
a	a	k8xC	a
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
kriminalita	kriminalita	k1gFnSc1	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
restrukturalizací	restrukturalizace	k1gFnSc7	restrukturalizace
hospodářství	hospodářství	k1gNnSc2	hospodářství
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vrstva	vrstva	k1gFnSc1	vrstva
extrémně	extrémně	k6eAd1	extrémně
bohatých	bohatý	k2eAgInPc2d1	bohatý
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
chudých	chudý	k2eAgMnPc2d1	chudý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Rusko	Rusko	k1gNnSc1	Rusko
státní	státní	k2eAgFnSc2d1	státní
bankrot	bankrot	k1gInSc4	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Putina	putin	k2eAgMnSc2d1	putin
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
<g/>
,	,	kIx,	,
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
se	se	k3xPyFc4	se
splacení	splacení	k1gNnSc2	splacení
ruských	ruský	k2eAgInPc2d1	ruský
dluhů	dluh	k1gInPc2	dluh
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
růstu	růst	k1gInSc2	růst
cen	cena	k1gFnPc2	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
je	být	k5eAaImIp3nS	být
Rusko	Rusko	k1gNnSc4	Rusko
významným	významný	k2eAgMnSc7d1	významný
exportérem	exportér	k1gMnSc7	exportér
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
kapitálové	kapitálový	k2eAgFnPc1d1	kapitálová
investice	investice	k1gFnPc1	investice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ale	ale	k8xC	ale
přibývají	přibývat	k5eAaImIp3nP	přibývat
nová	nový	k2eAgNnPc4d1	nové
rizika	riziko	k1gNnPc4	riziko
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
terorismus	terorismus	k1gInSc1	terorismus
nebo	nebo	k8xC	nebo
separatismus	separatismus	k1gInSc1	separatismus
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Závažný	závažný	k2eAgInSc4d1	závažný
problém	problém	k1gInSc4	problém
představovala	představovat	k5eAaImAgFnS	představovat
nízká	nízký	k2eAgFnSc1d1	nízká
porodnost	porodnost	k1gFnSc1	porodnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obzvláště	obzvláště	k6eAd1	obzvláště
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
ruské	ruský	k2eAgFnSc2d1	ruská
národnosti	národnost	k1gFnSc2	národnost
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
nejníže	nízce	k6eAd3	nízce
klesla	klesnout	k5eAaPmAgFnS	klesnout
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
mírně	mírně	k6eAd1	mírně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
netýká	týkat	k5eNaImIp3nS	týkat
některých	některý	k3yIgFnPc2	některý
neruských	ruský	k2eNgFnPc2d1	neruská
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgMnPc2	jenž
převládají	převládat	k5eAaImIp3nP	převládat
vyznavači	vyznavač	k1gMnPc7	vyznavač
islámu	islám	k1gInSc2	islám
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc1	jejich
porodnost	porodnost	k1gFnSc1	porodnost
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
vyšší	vysoký	k2eAgFnSc7d2	vyšší
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nepředstavují	představovat	k5eNaImIp3nP	představovat
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
negativní	negativní	k2eAgInPc4d1	negativní
trendy	trend	k1gInPc4	trend
v	v	k7c6	v
celonárodním	celonárodní	k2eAgNnSc6d1	celonárodní
měřítku	měřítko	k1gNnSc6	měřítko
zvrátit	zvrátit	k5eAaPmF	zvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
porodnostech	porodnost	k1gFnPc6	porodnost
se	se	k3xPyFc4	se
však	však	k9	však
projevuje	projevovat	k5eAaImIp3nS	projevovat
postupnou	postupný	k2eAgFnSc7d1	postupná
změnou	změna	k1gFnSc7	změna
poměru	poměr	k1gInSc2	poměr
obyvatel	obyvatel	k1gMnSc1	obyvatel
těchto	tento	k3xDgFnPc2	tento
národností	národnost	k1gFnPc2	národnost
a	a	k8xC	a
etnických	etnický	k2eAgMnPc2d1	etnický
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
zejména	zejména	k9	zejména
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zabránit	zabránit	k5eAaPmF	zabránit
separatistickým	separatistický	k2eAgFnPc3d1	separatistická
tendencím	tendence	k1gFnPc3	tendence
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severního	severní	k2eAgInSc2d1	severní
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
ruská	ruský	k2eAgFnSc1d1	ruská
armáda	armáda	k1gFnSc1	armáda
na	na	k7c4	na
Čečenskou	čečenský	k2eAgFnSc4d1	čečenská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
a	a	k8xC	a
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
však	však	k9	však
válka	válka	k1gFnSc1	válka
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
partyzánský	partyzánský	k2eAgInSc4d1	partyzánský
boj	boj	k1gInSc4	boj
a	a	k8xC	a
čečenští	čečenský	k2eAgMnPc1d1	čečenský
teroristé	terorista	k1gMnPc1	terorista
začali	začít	k5eAaPmAgMnP	začít
páchat	páchat	k5eAaImF	páchat
bombové	bombový	k2eAgInPc4d1	bombový
útoky	útok	k1gInPc4	útok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
teroristické	teroristický	k2eAgInPc4d1	teroristický
masakry	masakr	k1gInPc4	masakr
patří	patřit	k5eAaImIp3nS	patřit
teroristický	teroristický	k2eAgInSc1d1	teroristický
útok	útok	k1gInSc1	útok
na	na	k7c4	na
beslanskou	beslanský	k2eAgFnSc4d1	beslanská
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bombové	bombový	k2eAgInPc1d1	bombový
útoky	útok	k1gInPc1	útok
v	v	k7c6	v
Moskevském	moskevský	k2eAgNnSc6d1	moskevské
metru	metro	k1gNnSc6	metro
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
trolejbus	trolejbus	k1gInSc4	trolejbus
ve	v	k7c6	v
Volgogradu	Volgograd	k1gInSc2	Volgograd
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
2009	[number]	k4	2009
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snížily	snížit	k5eAaPmAgFnP	snížit
ceny	cena	k1gFnPc1	cena
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
kolem	kolem	k7c2	kolem
40	[number]	k4	40
USD	USD	kA	USD
za	za	k7c4	za
barel	barel	k1gInSc4	barel
(	(	kIx(	(
<g/>
počátek	počátek	k1gInSc4	počátek
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Analytikové	analytik	k1gMnPc1	analytik
přitom	přitom	k6eAd1	přitom
varovali	varovat	k5eAaImAgMnP	varovat
Rusko	Rusko	k1gNnSc4	Rusko
před	před	k7c7	před
vážnými	vážný	k2eAgInPc7d1	vážný
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
cena	cena	k1gFnSc1	cena
klesla	klesnout	k5eAaPmAgFnS	klesnout
pod	pod	k7c4	pod
50	[number]	k4	50
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Rusko	Rusko	k1gNnSc4	Rusko
to	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
významný	významný	k2eAgInSc4d1	významný
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
export	export	k1gInSc1	export
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
pilířů	pilíř	k1gInPc2	pilíř
současné	současný	k2eAgFnSc2d1	současná
ruské	ruský	k2eAgFnSc2d1	ruská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Akciové	akciový	k2eAgInPc1d1	akciový
trhy	trh	k1gInPc1	trh
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
zkolabovaly	zkolabovat	k5eAaPmAgInP	zkolabovat
<g/>
,	,	kIx,	,
příjmy	příjem	k1gInPc1	příjem
se	se	k3xPyFc4	se
snížily	snížit	k5eAaPmAgInP	snížit
a	a	k8xC	a
začala	začít	k5eAaPmAgNnP	začít
výrazně	výrazně	k6eAd1	výrazně
stoupat	stoupat	k5eAaImF	stoupat
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Putinismus	Putinismus	k1gInSc1	Putinismus
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
Anexe	anexe	k1gFnSc1	anexe
Krymu	Krym	k1gInSc2	Krym
Ruskou	Ruska	k1gFnSc7	Ruska
federací	federace	k1gFnSc7	federace
<g/>
,	,	kIx,	,
Ruská	ruský	k2eAgFnSc1d1	ruská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
intervence	intervence	k1gFnSc1	intervence
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
sankce	sankce	k1gFnSc1	sankce
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
na	na	k7c6	na
konci	konec	k1gInSc6	konec
desátých	desátá	k1gFnPc2	desátá
let	léto	k1gNnPc2	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
někdejší	někdejší	k2eAgFnSc4d1	někdejší
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc4d1	aktivní
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
návratu	návrat	k1gInSc6	návrat
image	image	k1gFnSc2	image
přední	přední	k2eAgFnSc2d1	přední
světové	světový	k2eAgFnSc2d1	světová
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Zapojilo	zapojit	k5eAaPmAgNnS	zapojit
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
do	do	k7c2	do
několika	několik	k4yIc2	několik
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jednou	jednou	k6eAd1	jednou
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
konfliktu	konflikt	k1gInSc2	konflikt
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
de	de	k?	de
facto	facto	k1gNnSc4	facto
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stav	stav	k1gInSc1	stav
separatistické	separatistický	k2eAgFnSc2d1	separatistická
Abcházie	Abcházie	k1gFnSc2	Abcházie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Osetie	Osetie	k1gFnSc2	Osetie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
které	který	k3yRgNnSc1	který
anektovalo	anektovat	k5eAaBmAgNnS	anektovat
ukrajinský	ukrajinský	k2eAgInSc4d1	ukrajinský
Krym	Krym	k1gInSc4	Krym
a	a	k8xC	a
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
separatistickou	separatistický	k2eAgFnSc4d1	separatistická
válku	válka	k1gFnSc4	válka
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
světových	světový	k2eAgMnPc2d1	světový
lídrů	lídr	k1gMnPc2	lídr
Rusko	Rusko	k1gNnSc1	Rusko
tvrdě	tvrdě	k6eAd1	tvrdě
kritizovalo	kritizovat	k5eAaImAgNnS	kritizovat
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
států	stát	k1gInPc2	stát
Západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
na	na	k7c4	na
ruské	ruský	k2eAgNnSc4d1	ruské
zboží	zboží	k1gNnSc4	zboží
uvalila	uvalit	k5eAaPmAgFnS	uvalit
embargo	embargo	k1gNnSc4	embargo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
cenou	cena	k1gFnSc7	cena
ropy	ropa	k1gFnSc2	ropa
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
ruskou	ruský	k2eAgFnSc4d1	ruská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
téměř	téměř	k6eAd1	téměř
okamžitý	okamžitý	k2eAgInSc1d1	okamžitý
propad	propad	k1gInSc1	propad
HDP	HDP	kA	HDP
asi	asi	k9	asi
o	o	k7c4	o
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
složité	složitý	k2eAgFnSc2d1	složitá
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podpořit	podpořit	k5eAaPmF	podpořit
režim	režim	k1gInSc4	režim
Bašára	Bašár	k1gMnSc4	Bašár
al-Asada	al-Asada	k1gFnSc1	al-Asada
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Ruská	ruský	k2eAgFnSc1d1	ruská
opozice	opozice	k1gFnSc1	opozice
<g/>
,	,	kIx,	,
LGBT	LGBT	kA	LGBT
práva	práv	k2eAgFnSc1d1	práva
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
ruské	ruský	k2eAgInPc1d1	ruský
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Dodržování	dodržování	k1gNnSc1	dodržování
občanských	občanský	k2eAgNnPc2d1	občanské
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
sledováno	sledován	k2eAgNnSc1d1	sledováno
jak	jak	k8xS	jak
občany	občan	k1gMnPc4	občan
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zahraničím	zahraničit	k5eAaPmIp1nS	zahraničit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
případné	případný	k2eAgNnSc1d1	případné
nedodržování	nedodržování	k1gNnSc1	nedodržování
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
potlačování	potlačování	k1gNnSc1	potlačování
je	být	k5eAaImIp3nS	být
soustavně	soustavně	k6eAd1	soustavně
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
vládami	vláda	k1gFnPc7	vláda
<g/>
,	,	kIx,	,
některými	některý	k3yIgMnPc7	některý
ruskými	ruský	k2eAgMnPc7d1	ruský
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
zahraničními	zahraniční	k2eAgNnPc7d1	zahraniční
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
i	i	k9	i
mimo	mimo	k7c4	mimo
Rusko	Rusko	k1gNnSc1	Rusko
působícími	působící	k2eAgFnPc7d1	působící
nevládními	vládní	k2eNgFnPc7d1	nevládní
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
cílícími	cílící	k2eAgFnPc7d1	cílící
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Amnesty	Amnest	k1gInPc4	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
Human	Human	k1gInSc4	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watcha	k1gFnPc2	Watcha
nebo	nebo	k8xC	nebo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
ruskému	ruský	k2eAgInSc3d1	ruský
systému	systém	k1gInSc3	systém
porušování	porušování	k1gNnSc2	porušování
občanských	občanský	k2eAgFnPc2d1	občanská
svobod	svoboda	k1gFnPc2	svoboda
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
práv	právo	k1gNnPc2	právo
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
financovaná	financovaný	k2eAgFnSc1d1	financovaná
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
označila	označit	k5eAaPmAgFnS	označit
Rusko	Rusko	k1gNnSc4	Rusko
za	za	k7c4	za
"	"	kIx"	"
<g/>
nesvobodný	svobodný	k2eNgInSc4d1	nesvobodný
stát	stát	k1gInSc4	stát
<g/>
"	"	kIx"	"
–	–	k?	–
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
jsou	být	k5eAaImIp3nP	být
volby	volba	k1gFnPc1	volba
pečlivě	pečlivě	k6eAd1	pečlivě
vykonstruované	vykonstruovaný	k2eAgFnPc1d1	vykonstruovaná
a	a	k8xC	a
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
chybí	chybit	k5eAaPmIp3nS	chybit
možnost	možnost	k1gFnSc4	možnost
skutečně	skutečně	k6eAd1	skutečně
svobodné	svobodný	k2eAgFnSc2d1	svobodná
diskuse	diskuse	k1gFnSc2	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgFnSc2d1	ruská
autority	autorita	k1gFnSc2	autorita
veškeré	veškerý	k3xTgFnSc2	veškerý
námitky	námitka	k1gFnSc2	námitka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
závěry	závěra	k1gFnPc4	závěra
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nevládní	vládní	k2eNgFnSc2d1	nevládní
organizace	organizace	k1gFnSc2	organizace
Democracy	Democraca	k1gFnPc1	Democraca
Ranking	Ranking	k1gInSc4	Ranking
se	se	k3xPyFc4	se
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
demokratických	demokratický	k2eAgFnPc2d1	demokratická
svobod	svoboda	k1gFnPc2	svoboda
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
bylo	být	k5eAaImAgNnS	být
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
115	[number]	k4	115
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
95	[number]	k4	95
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
je	být	k5eAaImIp3nS	být
zastupitelská	zastupitelský	k2eAgFnSc1d1	zastupitelská
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
poloprezidentská	poloprezidentský	k2eAgFnSc1d1	poloprezidentská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc4d1	dvoukomorový
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc7d1	horní
komorou	komora	k1gFnSc7	komora
a	a	k8xC	a
Státní	státní	k2eAgFnSc1d1	státní
duma	duma	k1gFnSc1	duma
je	být	k5eAaImIp3nS	být
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
dumě	duma	k1gFnSc6	duma
zastoupeny	zastoupen	k2eAgFnPc1d1	zastoupena
strany	strana	k1gFnPc1	strana
Jednotné	jednotný	k2eAgNnSc1d1	jednotné
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
54	[number]	k4	54
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
13	[number]	k4	13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liberální	liberální	k2eAgFnSc1d1	liberální
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
13	[number]	k4	13
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spravedlivé	spravedlivý	k2eAgNnSc1d1	spravedlivé
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
mimoparlametní	mimoparlametní	k2eAgFnSc1d1	mimoparlametní
opozice	opozice	k1gFnSc1	opozice
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
má	mít	k5eAaImIp3nS	mít
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
generální	generální	k2eAgInPc4d1	generální
konzuláty	konzulát	k1gInPc4	konzulát
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
a	a	k8xC	a
konzulát	konzulát	k1gInSc4	konzulát
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
ruské	ruský	k2eAgNnSc4d1	ruské
území	území	k1gNnSc4	území
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
čeští	český	k2eAgMnPc1d1	český
občané	občan	k1gMnPc1	občan
vízum	vízum	k1gNnSc1	vízum
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
víza	vízo	k1gNnPc1	vízo
buď	buď	k8xC	buď
turistická	turistický	k2eAgFnSc1d1	turistická
(	(	kIx(	(
<g/>
do	do	k7c2	do
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
služební	služební	k2eAgInSc4d1	služební
(	(	kIx(	(
<g/>
business	business	k1gInSc4	business
<g/>
,	,	kIx,	,
na	na	k7c4	na
30	[number]	k4	30
dní	den	k1gInPc2	den
až	až	k9	až
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vyřizováním	vyřizování	k1gNnSc7	vyřizování
víz	vízo	k1gNnPc2	vízo
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
různé	různý	k2eAgFnPc4d1	různá
agentury	agentura	k1gFnPc4	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
generální	generální	k2eAgInPc4d1	generální
konzuláty	konzulát	k1gInPc4	konzulát
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
Jekatěrinburgu	Jekatěrinburg	k1gInSc2	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
vyspělých	vyspělý	k2eAgInPc2d1	vyspělý
národů	národ	k1gInPc2	národ
G	G	kA	G
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
Rady	rada	k1gFnPc1	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
OBSE	OBSE	kA	OBSE
<g/>
,	,	kIx,	,
APEC	APEC	kA	APEC
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnPc1	skupina
BRICS	BRICS	kA	BRICS
a	a	k8xC	a
také	také	k9	také
regionálních	regionální	k2eAgFnPc2d1	regionální
organizací	organizace	k1gFnPc2	organizace
SNS	SNS	kA	SNS
<g/>
,	,	kIx,	,
EurAsEC	EurAsEC	k1gFnSc1	EurAsEC
<g/>
,	,	kIx,	,
OSKB	OSKB	kA	OSKB
a	a	k8xC	a
Šanghajské	šanghajský	k2eAgFnSc2d1	Šanghajská
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
událostí	událost	k1gFnPc2	událost
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Krymu	Krym	k1gInSc2	Krym
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
přijali	přijmout	k5eAaPmAgMnP	přijmout
lídři	lídr	k1gMnPc1	lídr
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
politické	politický	k2eAgFnSc2d1	politická
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
sankce	sankce	k1gFnSc2	sankce
vůči	vůči	k7c3	vůči
Ruské	ruský	k2eAgFnSc3d1	ruská
federaci	federace	k1gFnSc3	federace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
uznat	uznat	k5eAaPmF	uznat
Krymské	krymský	k2eAgNnSc4d1	krymské
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
devadesát	devadesát	k4xCc1	devadesát
procent	procento	k1gNnPc2	procento
hlasujících	hlasující	k1gMnPc2	hlasující
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
poloostrova	poloostrov	k1gInSc2	poloostrov
k	k	k7c3	k
Ruské	ruský	k2eAgFnSc3d1	ruská
federaci	federace	k1gFnSc3	federace
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
sankce	sankce	k1gFnPc1	sankce
zostřovány	zostřován	k2eAgFnPc1d1	zostřován
také	také	k9	také
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
údajné	údajný	k2eAgFnSc2d1	údajná
podpory	podpora	k1gFnSc2	podpora
povstalců	povstalec	k1gMnPc2	povstalec
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
řada	řada	k1gFnSc1	řada
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Kypru	Kypr	k1gInSc2	Kypr
<g/>
,	,	kIx,	,
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
,	,	kIx,	,
jdou	jít	k5eAaImIp3nP	jít
jen	jen	k9	jen
neochotně	ochotně	k6eNd1	ochotně
cestou	cesta	k1gFnSc7	cesta
sankcí	sankce	k1gFnSc7	sankce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
uvalení	uvalení	k1gNnSc4	uvalení
sankcí	sankce	k1gFnPc2	sankce
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
soustředit	soustředit	k5eAaPmF	soustředit
svou	svůj	k3xOyFgFnSc4	svůj
ekonomiku	ekonomika	k1gFnSc4	ekonomika
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
dodávkách	dodávka	k1gFnPc6	dodávka
plynu	plyn	k1gInSc2	plyn
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
užší	úzký	k2eAgFnSc3d2	užší
obchodní	obchodní	k2eAgFnSc3d1	obchodní
spolupráci	spolupráce	k1gFnSc3	spolupráce
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
znamenat	znamenat	k5eAaImF	znamenat
návštěva	návštěva	k1gFnSc1	návštěva
jejího	její	k3xOp3gMnSc2	její
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
samostatné	samostatný	k2eAgInPc4d1	samostatný
druhy	druh	k1gInPc4	druh
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
:	:	kIx,	:
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
a	a	k8xC	a
kosmická	kosmický	k2eAgFnSc1d1	kosmická
obrana	obrana	k1gFnSc1	obrana
<g/>
,	,	kIx,	,
raketová	raketový	k2eAgNnPc1d1	raketové
vojska	vojsko	k1gNnPc1	vojsko
strategického	strategický	k2eAgNnSc2d1	strategické
určení	určení	k1gNnSc2	určení
a	a	k8xC	a
vzdušná	vzdušný	k2eAgNnPc1d1	vzdušné
výsadková	výsadkový	k2eAgNnPc1d1	výsadkové
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
disponuje	disponovat	k5eAaBmIp3nS	disponovat
největší	veliký	k2eAgFnSc7d3	veliký
jadernou	jaderný	k2eAgFnSc7d1	jaderná
a	a	k8xC	a
tankovou	tankový	k2eAgFnSc7d1	tanková
silou	síla	k1gFnSc7	síla
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgNnSc2	svůj
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vojenského	vojenský	k2eAgNnSc2d1	vojenské
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
,	,	kIx,	,
jen	jen	k9	jen
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
dováží	dovážit	k5eAaPmIp3nS	dovážit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
dodavatelem	dodavatel	k1gMnSc7	dodavatel
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Severní	severní	k2eAgFnSc1d1	severní
(	(	kIx(	(
<g/>
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tichooceánské	tichooceánský	k2eAgNnSc4d1	tichooceánský
<g/>
,	,	kIx,	,
Černomořské	černomořský	k2eAgNnSc4d1	černomořské
<g/>
,	,	kIx,	,
Baltské	baltský	k2eAgNnSc4d1	Baltské
a	a	k8xC	a
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Námořní	námořní	k2eAgNnSc1d1	námořní
letectvo	letectvo	k1gNnSc1	letectvo
a	a	k8xC	a
Pobřežní	pobřežní	k2eAgNnSc1d1	pobřežní
vojsko	vojsko	k1gNnSc1	vojsko
(	(	kIx(	(
<g/>
námořní	námořní	k2eAgFnSc1d1	námořní
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
rakety	raketa	k1gFnPc1	raketa
a	a	k8xC	a
dělostřelecká	dělostřelecký	k2eAgNnPc1d1	dělostřelecké
vojska	vojsko	k1gNnPc1	vojsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
jednu	jeden	k4xCgFnSc4	jeden
letadlovou	letadlový	k2eAgFnSc4d1	letadlová
loď	loď	k1gFnSc4	loď
(	(	kIx(	(
<g/>
Admiral	Admiral	k1gFnSc1	Admiral
Kuzněcov	Kuzněcov	k1gInSc1	Kuzněcov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
křižníků	křižník	k1gInPc2	křižník
(	(	kIx(	(
<g/>
křižník	křižník	k1gInSc1	křižník
Pjotr	Pjotr	k1gInSc1	Pjotr
Velikij	Velikij	k1gMnSc2	Velikij
třídy	třída	k1gFnSc2	třída
Kirov	Kirovo	k1gNnPc2	Kirovo
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nukleární	nukleární	k2eAgInSc1d1	nukleární
pohon	pohon	k1gInSc1	pohon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
čtrnáct	čtrnáct	k4xCc4	čtrnáct
torpédoborců	torpédoborec	k1gInPc2	torpédoborec
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
fregat	fregata	k1gFnPc2	fregata
<g/>
,	,	kIx,	,
šedesát	šedesát	k4xCc4	šedesát
čtyři	čtyři	k4xCgNnPc4	čtyři
korvet	korveta	k1gFnPc2	korveta
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
nukleárních	nukleární	k2eAgFnPc2d1	nukleární
balistických	balistický	k2eAgFnPc2d1	balistická
ponorek	ponorka	k1gFnPc2	ponorka
a	a	k8xC	a
desítky	desítka	k1gFnPc4	desítka
útočných	útočný	k2eAgFnPc2d1	útočná
ponorek	ponorka	k1gFnPc2	ponorka
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
letectvo	letectvo	k1gNnSc1	letectvo
využívá	využívat	k5eAaImIp3nS	využívat
víceúčelové	víceúčelový	k2eAgInPc4d1	víceúčelový
stíhací	stíhací	k2eAgInPc4d1	stíhací
letouny	letoun	k1gInPc4	letoun
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
generace	generace	k1gFnSc2	generace
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc4	Su-
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
35	[number]	k4	35
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
MiG-	MiG-	k1gFnSc1	MiG-
<g/>
29	[number]	k4	29
a	a	k8xC	a
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bitevní	bitevní	k2eAgInPc4d1	bitevní
letouny	letoun	k1gInPc4	letoun
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gMnPc2	Su-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
stíhací-bombardovací	stíhacíombardovací	k2eAgInSc1d1	stíhací-bombardovací
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc1	Su-
<g/>
24	[number]	k4	24
a	a	k8xC	a
Suchoj	Suchoj	k1gInSc1	Suchoj
Su-	Su-	k1gFnSc2	Su-
<g/>
34	[number]	k4	34
<g/>
,	,	kIx,	,
bombardéry	bombardér	k1gInPc4	bombardér
Tupolev	Tupolev	k1gFnSc2	Tupolev
Tu-	Tu-	k1gFnSc7	Tu-
<g/>
22	[number]	k4	22
<g/>
M	M	kA	M
<g/>
,	,	kIx,	,
Tupolev	Tupolev	k1gFnSc1	Tupolev
Tu-	Tu-	k1gFnSc1	Tu-
<g/>
95	[number]	k4	95
a	a	k8xC	a
Tupolev	Tupolev	k1gMnSc4	Tupolev
Tu-	Tu-	k1gMnSc4	Tu-
<g/>
160	[number]	k4	160
<g/>
,	,	kIx,	,
stovky	stovka	k1gFnPc1	stovka
transportních	transportní	k2eAgInPc2d1	transportní
letounů	letoun	k1gInPc2	letoun
a	a	k8xC	a
bitevních	bitevní	k2eAgInPc2d1	bitevní
vrtulníků	vrtulník	k1gInPc2	vrtulník
(	(	kIx(	(
<g/>
Mil	míle	k1gFnPc2	míle
a	a	k8xC	a
Kamov	Kamovo	k1gNnPc2	Kamovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
česká	český	k2eAgNnPc4d1	české
letadla	letadlo	k1gNnPc4	letadlo
Let	let	k1gInSc1	let
L-410	L-410	k1gMnSc1	L-410
Turbolet	Turbolet	k1gInSc1	Turbolet
a	a	k8xC	a
Aero	aero	k1gNnSc1	aero
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
<g/>
.	.	kIx.	.
</s>
<s>
Stíhací	stíhací	k2eAgInSc1d1	stíhací
letoun	letoun	k1gInSc1	letoun
páté	pátá	k1gFnSc2	pátá
generace	generace	k1gFnSc2	generace
Suchoj	Suchoj	k1gInSc1	Suchoj
T-50	T-50	k1gFnSc2	T-50
měl	mít	k5eAaImAgInS	mít
první	první	k4xOgInSc4	první
let	let	k1gInSc4	let
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
a	a	k8xC	a
první	první	k4xOgFnPc4	první
dodávky	dodávka	k1gFnPc4	dodávka
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
plánují	plánovat	k5eAaImIp3nP	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
8	[number]	k4	8
federálních	federální	k2eAgInPc2d1	federální
okruhů	okruh	k1gInPc2	okruh
nebo	nebo	k8xC	nebo
na	na	k7c4	na
12	[number]	k4	12
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
rajónů	rajón	k1gInPc2	rajón
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
85	[number]	k4	85
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Federálními	federální	k2eAgInPc7d1	federální
okruhy	okruh	k1gInPc7	okruh
jsou	být	k5eAaImIp3nP	být
Centrální	centrální	k2eAgInSc1d1	centrální
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgInSc1d1	jižní
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
Severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
Dálněvýchodní	dálněvýchodní	k2eAgInSc1d1	dálněvýchodní
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
Sibiřský	sibiřský	k2eAgInSc1d1	sibiřský
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
Uralský	uralský	k2eAgInSc1d1	uralský
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
Povolžský	povolžský	k2eAgInSc1d1	povolžský
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
vyčleněn	vyčlenit	k5eAaPmNgInS	vyčlenit
z	z	k7c2	z
Jižního	jižní	k2eAgInSc2d1	jižní
federálního	federální	k2eAgInSc2d1	federální
okruhu	okruh	k1gInSc2	okruh
nový	nový	k2eAgInSc4d1	nový
Severokavkazský	severokavkazský	k2eAgInSc4d1	severokavkazský
federální	federální	k2eAgInSc4d1	federální
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
vzniknul	vzniknout	k5eAaPmAgInS	vzniknout
dočasně	dočasně	k6eAd1	dočasně
Krymský	krymský	k2eAgInSc1d1	krymský
federální	federální	k2eAgInSc1d1	federální
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
začleněn	začleněn	k2eAgMnSc1d1	začleněn
do	do	k7c2	do
Jižního	jižní	k2eAgInSc2d1	jižní
federálního	federální	k2eAgInSc2d1	federální
okruhu	okruh	k1gInSc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Federální	federální	k2eAgInPc1d1	federální
subjekty	subjekt	k1gInPc1	subjekt
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
22	[number]	k4	22
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
46	[number]	k4	46
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
9	[number]	k4	9
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
4	[number]	k4	4
autonomní	autonomní	k2eAgInPc1d1	autonomní
okruhy	okruh	k1gInPc1	okruh
<g/>
,	,	kIx,	,
1	[number]	k4	1
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
3	[number]	k4	3
federální	federální	k2eAgNnPc4d1	federální
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
v	v	k7c6	v
Ruské	ruský	k2eAgFnSc6d1	ruská
federaci	federace	k1gFnSc6	federace
sílí	sílet	k5eAaImIp3nS	sílet
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
slučování	slučování	k1gNnSc4	slučování
krajů	kraj	k1gInPc2	kraj
<g/>
,	,	kIx,	,
republik	republika	k1gFnPc2	republika
nebo	nebo	k8xC	nebo
autonomních	autonomní	k2eAgInPc2d1	autonomní
okruhů	okruh	k1gInPc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Putinovou	Putinový	k2eAgFnSc7d1	Putinový
centralizační	centralizační	k2eAgFnSc7d1	centralizační
politikou	politika	k1gFnSc7	politika
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
89	[number]	k4	89
subjektů	subjekt	k1gInPc2	subjekt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c4	na
dnešních	dnešní	k2eAgInPc2d1	dnešní
83	[number]	k4	83
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
jsou	být	k5eAaImIp3nP	být
nejlidnatějšími	lidnatý	k2eAgFnPc7d3	nejlidnatější
celky	celek	k1gInPc4	celek
Krasnodarský	krasnodarský	k2eAgInSc1d1	krasnodarský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
a	a	k8xC	a
Rostovská	Rostovský	k2eAgFnSc1d1	Rostovská
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
republiky	republika	k1gFnSc2	republika
Baškortostán	Baškortostán	k1gInSc1	Baškortostán
a	a	k8xC	a
Tatarstán	Tatarstán	k1gInSc1	Tatarstán
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
celkem	celek	k1gInSc7	celek
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
Sacha	Sacha	k1gFnSc1	Sacha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
km2	km2	k4	km2
<g/>
)	)	kIx)	)
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
třem	tři	k4xCgFnPc3	tři
čtvrtinám	čtvrtina	k1gFnPc3	čtvrtina
rozlohy	rozloha	k1gFnSc2	rozloha
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
však	však	k9	však
jen	jen	k9	jen
950	[number]	k4	950
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejmenšími	malý	k2eAgInPc7d3	nejmenší
celky	celek	k1gInPc7	celek
jsou	být	k5eAaImIp3nP	být
federální	federální	k2eAgNnPc4d1	federální
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
osídlené	osídlený	k2eAgFnSc2d1	osídlená
kavkazské	kavkazský	k2eAgFnSc2d1	kavkazská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
středoruské	středoruský	k2eAgFnSc2d1	středoruský
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
pobaltskými	pobaltský	k2eAgFnPc7d1	pobaltská
republikami	republika	k1gFnPc7	republika
a	a	k8xC	a
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
pak	pak	k6eAd1	pak
s	s	k7c7	s
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
rovinatý	rovinatý	k2eAgInSc1d1	rovinatý
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
pohoří	pohoří	k1gNnSc4	pohoří
Ural	Ural	k1gInSc1	Ural
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
evropským	evropský	k2eAgNnSc7d1	Evropské
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Sibiří	Sibiř	k1gFnSc7	Sibiř
a	a	k8xC	a
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Elbrusem	Elbrus	k1gMnSc7	Elbrus
(	(	kIx(	(
<g/>
5642	[number]	k4	5642
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
23	[number]	k4	23
míst	místo	k1gNnPc2	místo
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
40	[number]	k4	40
biosférických	biosférický	k2eAgFnPc2d1	biosférická
rezervací	rezervace	k1gFnPc2	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
41	[number]	k4	41
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
a	a	k8xC	a
101	[number]	k4	101
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
leží	ležet	k5eAaImIp3nS	ležet
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
nížin	nížina	k1gFnPc2	nížina
světa	svět	k1gInSc2	svět
Západosibiřská	západosibiřský	k2eAgFnSc1d1	Západosibiřská
rovina	rovina	k1gFnSc1	rovina
<g/>
,	,	kIx,	,
protékaná	protékaný	k2eAgFnSc1d1	protékaná
veletoky	veletok	k1gInPc4	veletok
Obu	Ob	k1gInSc2	Ob
s	s	k7c7	s
Irtyšem	Irtyš	k1gInSc7	Irtyš
a	a	k8xC	a
Jenisejem	Jenisej	k1gInSc7	Jenisej
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
je	být	k5eAaImIp3nS	být
Altaj	Altaj	k1gInSc1	Altaj
s	s	k7c7	s
četnými	četný	k2eAgInPc7d1	četný
ledovci	ledovec	k1gInPc7	ledovec
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
čtyř	čtyři	k4xCgInPc2	čtyři
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Altaje	Altaj	k1gInSc2	Altaj
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
Sajan	Sajana	k1gFnPc2	Sajana
a	a	k8xC	a
na	na	k7c6	na
mongolské	mongolský	k2eAgFnSc6d1	mongolská
hranici	hranice	k1gFnSc6	hranice
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
Východní	východní	k2eAgInSc4d1	východní
Sajan	Sajan	k1gInSc4	Sajan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
jezero	jezero	k1gNnSc4	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
1637	[number]	k4	1637
m	m	kA	m
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgFnSc4d3	veliký
zásobárnu	zásobárna	k1gFnSc4	zásobárna
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
ekosystém	ekosystém	k1gInSc1	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
Bajkalem	Bajkal	k1gInSc7	Bajkal
se	se	k3xPyFc4	se
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
střídají	střídat	k5eAaImIp3nP	střídat
náhorní	náhorní	k2eAgFnPc1d1	náhorní
plošiny	plošina	k1gFnPc1	plošina
(	(	kIx(	(
<g/>
Vitimská	Vitimský	k2eAgFnSc1d1	Vitimský
<g/>
,	,	kIx,	,
Aldanská	Aldanský	k2eAgFnSc1d1	Aldanský
<g/>
)	)	kIx)	)
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
horskými	horský	k2eAgInPc7d1	horský
hřbety	hřbet	k1gInPc7	hřbet
(	(	kIx(	(
<g/>
Jablonový	Jablonový	k2eAgInSc4d1	Jablonový
<g/>
,	,	kIx,	,
Stanový	stanový	k2eAgInSc4d1	stanový
<g/>
)	)	kIx)	)
až	až	k9	až
k	k	k7c3	k
Ochotskému	ochotský	k2eAgNnSc3d1	Ochotské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnější	východní	k2eAgNnSc1d3	nejvýchodnější
Čukotské	čukotský	k2eAgNnSc1d1	Čukotské
pohoří	pohoří	k1gNnSc1	pohoří
prostupuje	prostupovat	k5eAaImIp3nS	prostupovat
poloostrov	poloostrov	k1gInSc1	poloostrov
Čukotku	Čukotka	k1gFnSc4	Čukotka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
necelých	celý	k2eNgInPc2d1	necelý
100	[number]	k4	100
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Beringův	Beringův	k2eAgInSc1d1	Beringův
průliv	průliv	k1gInSc1	průliv
dělí	dělit	k5eAaImIp3nS	dělit
od	od	k7c2	od
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
Beringova	Beringův	k2eAgNnSc2d1	Beringovo
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
táhne	táhnout	k5eAaImIp3nS	táhnout
Korjacké	Korjacký	k2eAgNnSc4d1	Korjacký
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c6	na
Kamčatském	kamčatský	k2eAgInSc6d1	kamčatský
poloostrově	poloostrov	k1gInSc6	poloostrov
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
oddělené	oddělený	k2eAgNnSc1d1	oddělené
sníženinou	sníženina	k1gFnSc7	sníženina
řeky	řeka	k1gFnSc2	řeka
Kamčatky	Kamčatka	k1gFnSc2	Kamčatka
od	od	k7c2	od
pásma	pásmo	k1gNnSc2	pásmo
vysokých	vysoký	k2eAgFnPc2d1	vysoká
a	a	k8xC	a
činných	činný	k2eAgFnPc2d1	činná
sopek	sopka	k1gFnPc2	sopka
lemujících	lemující	k2eAgInPc2d1	lemující
tichomořské	tichomořský	k2eAgNnSc4d1	Tichomořské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Ruskou	ruský	k2eAgFnSc4d1	ruská
federaci	federace	k1gFnSc4	federace
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
moře	moře	k1gNnSc2	moře
tří	tři	k4xCgInPc2	tři
světových	světový	k2eAgInPc2d1	světový
oceánů	oceán	k1gInPc2	oceán
–	–	k?	–
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
<g/>
,	,	kIx,	,
Tichého	Tichého	k2eAgInSc2d1	Tichého
a	a	k8xC	a
Atlantického	atlantický	k2eAgInSc2d1	atlantický
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
mělká	mělký	k2eAgNnPc1d1	mělké
a	a	k8xC	a
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
je	on	k3xPp3gFnPc4	on
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
souvislá	souvislý	k2eAgFnSc1d1	souvislá
vrstva	vrstva	k1gFnSc1	vrstva
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
moří	mořit	k5eAaImIp3nS	mořit
Tichého	Tichého	k2eAgInSc2d1	Tichého
a	a	k8xC	a
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgFnSc2d1	odlišná
<g/>
:	:	kIx,	:
vody	voda	k1gFnSc2	voda
těchto	tento	k3xDgNnPc2	tento
moří	moře	k1gNnPc2	moře
zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
jen	jen	k9	jen
na	na	k7c4	na
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vůbec	vůbec	k9	vůbec
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
řekou	řeka	k1gFnSc7	řeka
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
Jenisej	Jenisej	k1gInSc1	Jenisej
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
Ob	Ob	k1gInSc1	Ob
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Irtyšem	Irtyš	k1gInSc7	Irtyš
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nP	tvořit
sedmou	sedmý	k4xOgFnSc4	sedmý
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
řeku	řeka	k1gFnSc4	řeka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
náleží	náležet	k5eAaImIp3nP	náležet
do	do	k7c2	do
úmoří	úmoří	k1gNnSc2	úmoří
Severního	severní	k2eAgInSc2d1	severní
ledového	ledový	k2eAgInSc2d1	ledový
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Dvinou	Dvina	k1gFnSc7	Dvina
<g/>
,	,	kIx,	,
Pečorou	Pečora	k1gFnSc7	Pečora
<g/>
,	,	kIx,	,
Lenou	Lena	k1gFnSc7	Lena
<g/>
,	,	kIx,	,
Janou	Jana	k1gFnSc7	Jana
<g/>
,	,	kIx,	,
Kolymou	Kolyma	k1gFnSc7	Kolyma
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
přítokem	přítok	k1gInSc7	přítok
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
Amur	Amur	k1gInSc1	Amur
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úmoří	úmoří	k1gNnSc3	úmoří
Atlantiku	Atlantik	k1gInSc2	Atlantik
patří	patřit	k5eAaImIp3nS	patřit
Don	Don	k1gMnSc1	Don
a	a	k8xC	a
Dněpr	Dněpr	k1gInSc1	Dněpr
(	(	kIx(	(
<g/>
na	na	k7c6	na
ruském	ruský	k2eAgNnSc6d1	ruské
území	území	k1gNnSc6	území
pouze	pouze	k6eAd1	pouze
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
řeka	řeka	k1gFnSc1	řeka
Ural	Ural	k1gInSc1	Ural
a	a	k8xC	a
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejvyužívanější	využívaný	k2eAgFnSc1d3	nejvyužívanější
Volha	Volha	k1gFnSc1	Volha
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
bezodtokého	bezodtoký	k2eAgNnSc2d1	bezodtoké
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgFnPc4d1	známá
řeky	řeka	k1gFnPc4	řeka
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
protékající	protékající	k2eAgNnSc1d1	protékající
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
Něva	Něva	k1gFnSc1	Něva
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
ústí	ústí	k1gNnSc6	ústí
do	do	k7c2	do
finského	finský	k2eAgInSc2d1	finský
zálivu	záliv	k1gInSc2	záliv
byl	být	k5eAaImAgInS	být
vystavěn	vystavěn	k2eAgInSc1d1	vystavěn
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
jezer	jezero	k1gNnPc2	jezero
rozmanitého	rozmanitý	k2eAgInSc2d1	rozmanitý
původu	původ	k1gInSc2	původ
a	a	k8xC	a
velikostí	velikost	k1gFnPc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
slané	slaný	k2eAgNnSc1d1	slané
(	(	kIx(	(
<g/>
brakické	brakický	k2eAgNnSc4d1	brakické
<g/>
)	)	kIx)	)
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sladkovodních	sladkovodní	k2eAgMnPc2d1	sladkovodní
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
jezero	jezero	k1gNnSc1	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
jezero	jezero	k1gNnSc1	jezero
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
leží	ležet	k5eAaImIp3nS	ležet
jezero	jezero	k1gNnSc1	jezero
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
<g/>
,	,	kIx,	,
Oněžské	oněžský	k2eAgFnPc1d1	oněžský
<g/>
,	,	kIx,	,
Čudské	čudský	k2eAgFnPc1d1	čudský
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zbudováno	zbudovat	k5eAaPmNgNnS	zbudovat
také	také	k9	také
množství	množství	k1gNnSc1	množství
přehradních	přehradní	k2eAgNnPc2d1	přehradní
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
<g/>
,	,	kIx,	,
Angaře	Angara	k1gFnSc6	Angara
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
řekách	řeka	k1gFnPc6	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Podnebí	podnebí	k1gNnSc2	podnebí
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
je	být	k5eAaImIp3nS	být
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
několik	několik	k4yIc4	několik
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
klimatických	klimatický	k2eAgFnPc2d1	klimatická
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
počasí	počasí	k1gNnSc4	počasí
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
a	a	k8xC	a
středním	střední	k2eAgNnSc6d1	střední
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
velké	velký	k2eAgNnSc1d1	velké
střídání	střídání	k1gNnSc1	střídání
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jaro	jaro	k1gNnSc1	jaro
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
studenější	studený	k2eAgFnSc1d2	studenější
než	než	k8xS	než
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
zimy	zima	k1gFnPc1	zima
podstatně	podstatně	k6eAd1	podstatně
chladnější	chladný	k2eAgFnPc1d2	chladnější
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
centrálním	centrální	k2eAgNnSc6d1	centrální
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
stepí	step	k1gFnPc2	step
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc1d1	nízká
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc4d1	vysoká
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
také	také	k9	také
málo	málo	k6eAd1	málo
prší	pršet	k5eAaImIp3nS	pršet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
černomořském	černomořský	k2eAgNnSc6d1	černomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
jsou	být	k5eAaImIp3nP	být
zimy	zima	k1gFnPc1	zima
mírné	mírný	k2eAgFnSc2d1	mírná
a	a	k8xC	a
léta	léto	k1gNnSc2	léto
příjemně	příjemně	k6eAd1	příjemně
teplá	teplý	k2eAgFnSc1d1	teplá
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
Sibiře	Sibiř	k1gFnSc2	Sibiř
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgFnPc1d1	nízká
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
krátká	krátký	k2eAgNnPc1d1	krátké
a	a	k8xC	a
četnými	četný	k2eAgFnPc7d1	četná
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Sibiř	Sibiř	k1gFnSc1	Sibiř
a	a	k8xC	a
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
věčné	věčný	k2eAgFnSc2d1	věčná
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
značné	značný	k2eAgInPc4d1	značný
problémy	problém	k1gInPc4	problém
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Podnebné	podnebný	k2eAgInPc1d1	podnebný
pásy	pás	k1gInPc1	pás
<g/>
:	:	kIx,	:
Polární	polární	k2eAgInSc1d1	polární
pás	pás	k1gInSc1	pás
(	(	kIx(	(
<g/>
nejsevernější	severní	k2eAgFnSc6d3	nejsevernější
oblasti	oblast	k1gFnSc6	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
subpolární	subpolární	k2eAgInSc1d1	subpolární
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
mírný	mírný	k2eAgInSc1d1	mírný
pás	pás	k1gInSc1	pás
a	a	k8xC	a
subtropický	subtropický	k2eAgInSc1d1	subtropický
pás	pás	k1gInSc1	pás
(	(	kIx(	(
<g/>
pobřeží	pobřeží	k1gNnSc1	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
světa	svět	k1gInSc2	svět
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
výrazně	výrazně	k6eAd1	výrazně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
zbytek	zbytek	k1gInSc4	zbytek
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Pětimilionový	pětimilionový	k2eAgInSc1d1	pětimilionový
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgFnSc1d1	někdejší
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
především	především	k9	především
centrem	centrum	k1gNnSc7	centrum
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgMnSc7	třetí
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
centrem	centr	k1gMnSc7	centr
ruského	ruský	k2eAgNnSc2d1	ruské
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
Jekatěrinburg	Jekatěrinburg	k1gInSc1	Jekatěrinburg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
leží	ležet	k5eAaImIp3nS	ležet
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
Nižnij	Nižnij	k1gFnSc2	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
,	,	kIx,	,
Kazaň	Kazaň	k1gFnSc1	Kazaň
<g/>
,	,	kIx,	,
Toljatti	Toljatti	k1gNnSc1	Toljatti
<g/>
,	,	kIx,	,
Samara	Samara	k1gFnSc1	Samara
<g/>
,	,	kIx,	,
Saratov	Saratov	k1gInSc1	Saratov
<g/>
,	,	kIx,	,
Volgograd	Volgograd	k1gInSc1	Volgograd
<g/>
,	,	kIx,	,
Astrachaň	Astrachaň	k1gFnSc1	Astrachaň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
a	a	k8xC	a
regionální	regionální	k2eAgNnPc1d1	regionální
centra	centrum	k1gNnPc1	centrum
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
Perm	perm	k1gInSc1	perm
<g/>
,	,	kIx,	,
Iževsk	Iževsk	k1gInSc1	Iževsk
<g/>
,	,	kIx,	,
Ufa	Ufa	k1gFnSc1	Ufa
<g/>
,	,	kIx,	,
Voroněž	Voroněž	k1gFnSc1	Voroněž
<g/>
,	,	kIx,	,
Jaroslavl	Jaroslavl	k1gFnSc1	Jaroslavl
<g/>
,	,	kIx,	,
Rostov	Rostov	k1gInSc1	Rostov
<g/>
,	,	kIx,	,
Krasnodar	Krasnodar	k1gInSc1	Krasnodar
<g/>
,	,	kIx,	,
v	v	k7c6	v
asijské	asijský	k2eAgFnSc6d1	asijská
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
Čeljabinsk	Čeljabinsk	k1gInSc4	Čeljabinsk
<g/>
,	,	kIx,	,
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
,	,	kIx,	,
Omsk	Omsk	k1gInSc1	Omsk
<g/>
,	,	kIx,	,
Barnaul	Barnaul	k1gInSc1	Barnaul
<g/>
,	,	kIx,	,
Krasnojarsk	Krasnojarsk	k1gInSc1	Krasnojarsk
<g/>
,	,	kIx,	,
Irkutsk	Irkutsk	k1gInSc1	Irkutsk
<g/>
,	,	kIx,	,
Jakutsk	Jakutsk	k1gInSc1	Jakutsk
<g/>
,	,	kIx,	,
Chabarovsk	Chabarovsk	k1gInSc1	Chabarovsk
či	či	k8xC	či
tichomořský	tichomořský	k2eAgInSc1d1	tichomořský
přístav	přístav	k1gInSc1	přístav
Vladivostok	Vladivostok	k1gInSc1	Vladivostok
<g/>
.	.	kIx.	.
</s>
<s>
Menšími	malý	k2eAgNnPc7d2	menší
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
strategickými	strategický	k2eAgInPc7d1	strategický
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
Novorossijsk	Novorossijsk	k1gInSc4	Novorossijsk
<g/>
,	,	kIx,	,
Kaliningrad	Kaliningrad	k1gInSc4	Kaliningrad
<g/>
,	,	kIx,	,
Archangelsk	Archangelsk	k1gInSc4	Archangelsk
a	a	k8xC	a
Murmansk	Murmansk	k1gInSc4	Murmansk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
za	za	k7c7	za
severním	severní	k2eAgInSc7d1	severní
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
severněji	severně	k6eAd2	severně
leží	ležet	k5eAaImIp3nS	ležet
Norilsk	Norilsk	k1gInSc1	Norilsk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
těžbě	těžba	k1gFnSc3	těžba
kovů	kov	k1gInPc2	kov
nechvalně	chvalně	k6eNd1	chvalně
proslulý	proslulý	k2eAgMnSc1d1	proslulý
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejznečištěnějších	znečištěný	k2eAgNnPc2d3	nejznečištěnější
měst	město	k1gNnPc2	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Historickými	historický	k2eAgFnPc7d1	historická
památkami	památka	k1gFnPc7	památka
kromě	kromě	k7c2	kromě
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
a	a	k8xC	a
povolžských	povolžský	k2eAgNnPc2d1	Povolžské
měst	město	k1gNnPc2	město
vyniká	vynikat	k5eAaImIp3nS	vynikat
Pskov	Pskov	k1gInSc1	Pskov
<g/>
,	,	kIx,	,
Novgorod	Novgorod	k1gInSc1	Novgorod
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kruh	kruh	k1gInSc1	kruh
Ruska	Rusko	k1gNnSc2	Rusko
–	–	k?	–
prstenec	prstenec	k1gInSc1	prstenec
historických	historický	k2eAgNnPc2d1	historické
měst	město	k1gNnPc2	město
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Rekreačním	rekreační	k2eAgNnSc7d1	rekreační
centrem	centrum	k1gNnSc7	centrum
je	být	k5eAaImIp3nS	být
Soči	Soči	k1gNnPc4	Soči
u	u	k7c2	u
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
oproti	oproti	k7c3	oproti
zbytku	zbytek	k1gInSc2	zbytek
Ruska	Rusko	k1gNnSc2	Rusko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
s	s	k7c7	s
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
písečnými	písečný	k2eAgFnPc7d1	písečná
plážemi	pláž	k1gFnPc7	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
ekonomika	ekonomika	k1gFnSc1	ekonomika
v	v	k7c6	v
r.	r.	kA	r.
2016	[number]	k4	2016
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c6	na
dvanáctou	dvanáctý	k4xOgFnSc7	dvanáctý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
stavu	stav	k1gInSc6	stav
jeho	jeho	k3xOp3gFnSc2	jeho
ekonomiky	ekonomika	k1gFnSc2	ekonomika
začíná	začínat	k5eAaImIp3nS	začínat
vážně	vážně	k6eAd1	vážně
projevovat	projevovat	k5eAaImF	projevovat
soubor	soubor	k1gInSc4	soubor
negativních	negativní	k2eAgInPc2d1	negativní
faktorů	faktor	k1gInPc2	faktor
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejhlavnější	hlavní	k2eAgMnPc1d3	nejhlavnější
jsou	být	k5eAaImIp3nP	být
pokles	pokles	k1gInSc4	pokles
cen	cena	k1gFnPc2	cena
klíčových	klíčový	k2eAgFnPc2d1	klíčová
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
sankce	sankce	k1gFnSc2	sankce
uvalené	uvalený	k2eAgFnSc2d1	uvalená
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
Ruskou	ruský	k2eAgFnSc4d1	ruská
anexi	anexe	k1gFnSc4	anexe
Krymu	Krym	k1gInSc2	Krym
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Financial	Financial	k1gMnSc1	Financial
Times	Times	k1gMnSc1	Times
ohodnotily	ohodnotit	k5eAaPmAgInP	ohodnotit
Rusko	Rusko	k1gNnSc4	Rusko
jako	jako	k9	jako
druhou	druhý	k4xOgFnSc4	druhý
finančně	finančně	k6eAd1	finančně
nejstabilnější	stabilní	k2eAgFnSc4d3	nejstabilnější
ekonomiku	ekonomika	k1gFnSc4	ekonomika
mezi	mezi	k7c7	mezi
G20	G20	k1gFnSc7	G20
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
nízkému	nízký	k2eAgNnSc3d1	nízké
dluhovému	dluhový	k2eAgNnSc3d1	dluhové
zatížení	zatížení	k1gNnSc3	zatížení
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
11	[number]	k4	11
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgFnSc2d1	nízká
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
významným	významný	k2eAgFnPc3d1	významná
rezervám	rezerva	k1gFnPc3	rezerva
a	a	k8xC	a
míry	mír	k1gInPc4	mír
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
růstu	růst	k1gInSc2	růst
<g/>
..	..	k?	..
<g />
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
však	však	k9	však
díky	díky	k7c3	díky
ekonomickým	ekonomický	k2eAgInPc3d1	ekonomický
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
rychlému	rychlý	k2eAgNnSc3d1	rychlé
snižování	snižování	k1gNnSc3	snižování
státních	státní	k2eAgFnPc2d1	státní
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc3d1	vysoká
inflaci	inflace	k1gFnSc3	inflace
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
negativním	negativní	k2eAgInPc3d1	negativní
jevům	jev	k1gInPc3	jev
situace	situace	k1gFnSc2	situace
výrazně	výrazně	k6eAd1	výrazně
změnila	změnit	k5eAaPmAgFnS	změnit
V	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
aktivitě	aktivita	k1gFnSc6	aktivita
má	mít	k5eAaImIp3nS	mít
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
metropole	metropol	k1gFnSc2	metropol
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
hrubý	hrubý	k2eAgInSc4d1	hrubý
domácí	domácí	k2eAgInSc4d1	domácí
produkt	produkt	k1gInSc4	produkt
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
si	se	k3xPyFc3	se
také	také	k9	také
ještě	ještě	k6eAd1	ještě
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
drží	držet	k5eAaImIp3nP	držet
další	další	k2eAgNnPc1d1	další
velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchod	severovýchod	k1gInSc1	severovýchod
–	–	k?	–
Sibiř	Sibiř	k1gFnSc1	Sibiř
a	a	k8xC	a
Dálný	dálný	k2eAgInSc1d1	dálný
východ	východ	k1gInSc1	východ
–	–	k?	–
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k6eAd1	především
jako	jako	k9	jako
surovinová	surovinový	k2eAgFnSc1d1	surovinová
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
komoditami	komodita	k1gFnPc7	komodita
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
a	a	k8xC	a
živočišné	živočišný	k2eAgFnSc2d1	živočišná
výroby	výroba	k1gFnSc2	výroba
jsou	být	k5eAaImIp3nP	být
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnPc1	pšenice
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
;	;	kIx,	;
vepřové	vepřové	k1gNnSc1	vepřové
<g/>
,	,	kIx,	,
drůbeží	drůbeží	k2eAgNnSc1d1	drůbeží
a	a	k8xC	a
skopové	skopový	k2eAgNnSc1d1	skopové
maso	maso	k1gNnSc1	maso
<g/>
;	;	kIx,	;
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
jedenáctým	jedenáctý	k4xOgNnSc7	jedenáctý
největším	veliký	k2eAgNnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc3d3	veliký
automobilové	automobilový	k2eAgFnSc3d1	automobilová
společnosti	společnost	k1gFnSc3	společnost
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
jsou	být	k5eAaImIp3nP	být
AvtoVAZ	AvtoVAZ	k1gFnSc1	AvtoVAZ
a	a	k8xC	a
GAZ	GAZ	kA	GAZ
<g/>
.	.	kIx.	.
</s>
<s>
Kamaz	Kamaz	k1gInSc1	Kamaz
je	být	k5eAaImIp3nS	být
předním	přední	k2eAgMnSc7d1	přední
výrobcem	výrobce	k1gMnSc7	výrobce
nákladních	nákladní	k2eAgInPc2d1	nákladní
vozů	vůz	k1gInPc2	vůz
a	a	k8xC	a
kamionů	kamion	k1gInPc2	kamion
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Marussia	Marussia	k1gFnSc1	Marussia
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
ruský	ruský	k2eAgMnSc1d1	ruský
výrobce	výrobce	k1gMnSc1	výrobce
supersportů	supersport	k1gInPc2	supersport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
výrazně	výrazně	k6eAd1	výrazně
zvedla	zvednout	k5eAaPmAgFnS	zvednout
příjmová	příjmový	k2eAgFnSc1d1	příjmová
nerovnost	nerovnost	k1gFnSc1	nerovnost
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
nerovnost	nerovnost	k1gFnSc1	nerovnost
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
Kavkazské	kavkazský	k2eAgInPc1d1	kavkazský
regiony	region	k1gInPc1	region
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
Giniho	Gini	k1gMnSc4	Gini
koeficient	koeficient	k1gInSc1	koeficient
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
změřen	změřit	k5eAaPmNgMnS	změřit
na	na	k7c4	na
40,11	[number]	k4	40,11
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
zažívalo	zažívat	k5eAaImAgNnS	zažívat
Rusko	Rusko	k1gNnSc1	Rusko
značný	značný	k2eAgInSc1d1	značný
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
jen	jen	k9	jen
globální	globální	k2eAgInSc1d1	globální
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
má	mít	k5eAaImIp3nS	mít
Rusko	Rusko	k1gNnSc4	Rusko
opět	opět	k6eAd1	opět
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
zapříčiněny	zapříčiněn	k2eAgInPc1d1	zapříčiněn
hlavně	hlavně	k6eAd1	hlavně
poklesem	pokles	k1gInSc7	pokles
cen	cena	k1gFnPc2	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
surovin	surovina	k1gFnPc2	surovina
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
trzích	trh	k1gInPc6	trh
a	a	k8xC	a
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
sankcemi	sankce	k1gFnPc7	sankce
<g/>
,	,	kIx,	,
uvalenými	uvalený	k2eAgFnPc7d1	uvalená
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
anexí	anexe	k1gFnSc7	anexe
Krymu	Krym	k1gInSc2	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
těženými	těžený	k2eAgFnPc7d1	těžená
surovinami	surovina	k1gFnPc7	surovina
jsou	být	k5eAaImIp3nP	být
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
a	a	k8xC	a
nikl	nikl	k1gInSc4	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnPc4d3	veliký
rezervy	rezerva	k1gFnPc4	rezerva
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnPc4	druhý
největší	veliký	k2eAgFnPc4d3	veliký
zásoby	zásoba	k1gFnPc4	zásoba
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
osmé	osmý	k4xOgFnSc2	osmý
největší	veliký	k2eAgFnSc2d3	veliký
zásoby	zásoba	k1gFnSc2	zásoba
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
mj.	mj.	kA	mj.
velké	velký	k2eAgFnPc4d1	velká
zásoby	zásoba	k1gFnPc4	zásoba
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
diamantů	diamant	k1gInPc2	diamant
a	a	k8xC	a
titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existují	existovat	k5eAaImIp3nP	existovat
velké	velký	k2eAgFnPc1d1	velká
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
-	-	kIx~	-
Rosněft	Rosněft	k1gMnSc1	Rosněft
<g/>
,	,	kIx,	,
Lukoil	Lukoil	k1gMnSc1	Lukoil
<g/>
,	,	kIx,	,
Sibněft	Sibněft	k1gMnSc1	Sibněft
<g/>
,	,	kIx,	,
Juganskněftěgaz	Juganskněftěgaz	k1gInSc1	Juganskněftěgaz
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
plynárenský	plynárenský	k2eAgInSc1d1	plynárenský
gigant	gigant	k1gInSc1	gigant
Gazprom	Gazprom	k1gInSc1	Gazprom
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
firmou	firma	k1gFnSc7	firma
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
těžbou	těžba	k1gFnSc7	těžba
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
výkony	výkon	k1gInPc1	výkon
těžby	těžba	k1gFnSc2	těžba
nafty	nafta	k1gFnSc2	nafta
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
tvoří	tvořit	k5eAaImIp3nP	tvořit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
HDP	HDP	kA	HDP
ruské	ruský	k2eAgFnSc2d1	ruská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
těžba	těžba	k1gFnSc1	těžba
všech	všecek	k3xTgFnPc2	všecek
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
tvořila	tvořit	k5eAaImAgFnS	tvořit
10,4	[number]	k4	10,4
%	%	kIx~	%
HDP	HDP	kA	HDP
Ruska	Ruska	k1gFnSc1	Ruska
<g/>
)	)	kIx)	)
a	a	k8xC	a
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
kryjí	krýt	k5eAaImIp3nP	krýt
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
výdaje	výdaj	k1gInSc2	výdaj
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
těžby	těžba	k1gFnSc2	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
vývozní	vývozní	k2eAgFnSc4d1	vývozní
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
ropných	ropný	k2eAgInPc2d1	ropný
produktů	produkt	k1gInPc2	produkt
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
příjmech	příjem	k1gInPc6	příjem
federálního	federální	k2eAgInSc2d1	federální
rozpočtu	rozpočet	k1gInSc2	rozpočet
ze	z	k7c2	z
46	[number]	k4	46
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Surovinové	surovinový	k2eAgInPc1d1	surovinový
příjmy	příjem	k1gInPc1	příjem
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředit	k5eAaPmNgInP	soustředit
na	na	k7c6	na
federální	federální	k2eAgFnSc6d1	federální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
váha	váha	k1gFnSc1	váha
v	v	k7c6	v
konsolidovaných	konsolidovaný	k2eAgInPc6d1	konsolidovaný
státních	státní	k2eAgInPc6d1	státní
příjmech	příjem	k1gInPc6	příjem
<g/>
,	,	kIx,	,
zahrnujících	zahrnující	k2eAgFnPc6d1	zahrnující
i	i	k8xC	i
rozpočty	rozpočet	k1gInPc4	rozpočet
regionů	region	k1gInPc2	region
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
zhruba	zhruba	k6eAd1	zhruba
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
dodává	dodávat	k5eAaImIp3nS	dodávat
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
spotřeby	spotřeba	k1gFnSc2	spotřeba
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tranzitních	tranzitní	k2eAgFnPc2d1	tranzitní
cest	cesta	k1gFnPc2	cesta
přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
(	(	kIx(	(
<g/>
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
,	,	kIx,	,
Družba	družba	k1gFnSc1	družba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
(	(	kIx(	(
<g/>
Jamal-Evropa	Jamal-Evropa	k1gFnSc1	Jamal-Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
přerušení	přerušení	k1gNnSc2	přerušení
dodávek	dodávka	k1gFnPc2	dodávka
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
přes	přes	k7c4	přes
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
nesplácela	splácet	k5eNaImAgFnS	splácet
dluhy	dluh	k1gInPc4	dluh
za	za	k7c4	za
tyto	tento	k3xDgFnPc4	tento
suroviny	surovina	k1gFnPc4	surovina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
dokončena	dokončit	k5eAaPmNgFnS	dokončit
první	první	k4xOgFnSc1	první
fáze	fáze	k1gFnSc1	fáze
tzv.	tzv.	kA	tzv.
Severního	severní	k2eAgInSc2d1	severní
plynovodu	plynovod	k1gInSc2	plynovod
Nord	Norda	k1gFnPc2	Norda
Stream	Stream	k1gInSc1	Stream
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
pod	pod	k7c7	pod
Baltickým	baltický	k2eAgNnSc7d1	Baltické
mořem	moře	k1gNnSc7	moře
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
s	s	k7c7	s
napojením	napojení	k1gNnSc7	napojení
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dokončen	dokončen	k2eAgInSc1d1	dokončen
plynovod	plynovod	k1gInSc1	plynovod
NEL	Nela	k1gFnPc2	Nela
navazující	navazující	k2eAgMnSc1d1	navazující
na	na	k7c4	na
plynovod	plynovod	k1gInSc4	plynovod
BBL	BBL	kA	BBL
<g/>
,	,	kIx,	,
přivádějící	přivádějící	k2eAgInSc1d1	přivádějící
plyn	plyn	k1gInSc1	plyn
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
také	také	k9	také
dokončen	dokončen	k2eAgInSc1d1	dokončen
ropovod	ropovod	k1gInSc1	ropovod
ESPO	ESPO	kA	ESPO
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgFnSc1d1	spojující
Sibiř	Sibiř	k1gFnSc1	Sibiř
s	s	k7c7	s
asijsko-tichomořskými	asijskoichomořský	k2eAgInPc7d1	asijsko-tichomořský
trhy	trh	k1gInPc7	trh
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
energetických	energetický	k2eAgFnPc2d1	energetická
potřeb	potřeba	k1gFnPc2	potřeba
států	stát	k1gInPc2	stát
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Evropy	Evropa	k1gFnSc2	Evropa
včetně	včetně	k7c2	včetně
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
donedávna	donedávna	k6eAd1	donedávna
plánován	plánovat	k5eAaImNgInS	plánovat
plynovod	plynovod	k1gInSc1	plynovod
South	Southa	k1gFnPc2	Southa
Stream	Stream	k1gInSc1	Stream
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gFnSc1	vedoucí
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
pod	pod	k7c7	pod
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
přes	přes	k7c4	přes
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
podepsali	podepsat	k5eAaPmAgMnP	podepsat
zástupci	zástupce	k1gMnPc1	zástupce
rakouské	rakouský	k2eAgFnSc2d1	rakouská
společnosti	společnost	k1gFnSc2	společnost
OMV	OMV	kA	OMV
a	a	k8xC	a
ruského	ruský	k2eAgInSc2d1	ruský
Gazpromu	Gazprom	k1gInSc2	Gazprom
tzv.	tzv.	kA	tzv.
Memorandum	memorandum	k1gNnSc4	memorandum
o	o	k7c6	o
porozumění	porozumění	k1gNnSc6	porozumění
(	(	kIx(	(
<g/>
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
Rakušané	Rakušan	k1gMnPc1	Rakušan
budou	být	k5eAaImBp3nP	být
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
dvou	dva	k4xCgNnPc2	dva
dalších	další	k2eAgNnPc2d1	další
potrubí	potrubí	k1gNnPc2	potrubí
pod	pod	k7c7	pod
Baltickým	baltický	k2eAgNnSc7d1	Baltické
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
uskupení	uskupení	k1gNnSc3	uskupení
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
stavbě	stavba	k1gFnSc6	stavba
podílet	podílet	k5eAaImF	podílet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
také	také	k9	také
německá	německý	k2eAgFnSc1d1	německá
společnost	společnost	k1gFnSc1	společnost
EON	EON	kA	EON
a	a	k8xC	a
nizozemsko-britská	nizozemskoritský	k2eAgFnSc1d1	nizozemsko-britský
společnost	společnost	k1gFnSc1	společnost
Royal	Royal	k1gMnSc1	Royal
Dutch	Dutch	k1gMnSc1	Dutch
Shell	Shell	k1gMnSc1	Shell
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
potrubí	potrubí	k1gNnSc1	potrubí
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
postaveno	postavit	k5eAaPmNgNnS	postavit
a	a	k8xC	a
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
bude	být	k5eAaImBp3nS	být
stavěno	stavěn	k2eAgNnSc1d1	stavěno
ještě	ještě	k9	ještě
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
<g/>
,	,	kIx,	,
potrubí	potrubí	k1gNnSc2	potrubí
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
projektu	projekt	k1gInSc2	projekt
přesáhnou	přesáhnout	k5eAaPmIp3nP	přesáhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
fázi	fáze	k1gFnSc4	fáze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
činily	činit	k5eAaImAgFnP	činit
7,4	[number]	k4	7,4
miliardy	miliarda	k4xCgFnPc4	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
nová	nový	k2eAgNnPc4d1	nové
potrubí	potrubí	k1gNnPc4	potrubí
mají	mít	k5eAaImIp3nP	mít
přepravovat	přepravovat	k5eAaImF	přepravovat
dalších	další	k2eAgFnPc2d1	další
55	[number]	k4	55
miliard	miliarda	k4xCgFnPc2	miliarda
m3	m3	k4	m3
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
ročně	ročně	k6eAd1	ročně
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
již	již	k9	již
žádný	žádný	k3yNgInSc4	žádný
plyn	plyn	k1gInSc4	plyn
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
Západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
přepravován	přepravován	k2eAgMnSc1d1	přepravován
přes	přes	k7c4	přes
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
tranzitní	tranzitní	k2eAgFnSc4d1	tranzitní
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgMnSc1	třetí
největší	veliký	k2eAgMnSc1d3	veliký
výrobce	výrobce	k1gMnSc1	výrobce
elektřiny	elektřina	k1gFnSc2	elektřina
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
první	první	k4xOgFnSc4	první
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
jadernou	jaderný	k2eAgFnSc4d1	jaderná
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
jaderné	jaderný	k2eAgFnPc1d1	jaderná
elektrárny	elektrárna	k1gFnPc1	elektrárna
jsou	být	k5eAaImIp3nP	být
řízeny	řídit	k5eAaImNgFnP	řídit
státní	státní	k2eAgFnSc7d1	státní
firmou	firma	k1gFnSc7	firma
Rosatom	Rosatom	k1gInSc1	Rosatom
<g/>
.	.	kIx.	.
</s>
<s>
Sektor	sektor	k1gInSc1	sektor
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
federálního	federální	k2eAgInSc2d1	federální
rozpočtu	rozpočet	k1gInSc2	rozpočet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vyčleněn	vyčleněn	k2eAgInSc1d1	vyčleněn
asi	asi	k9	asi
1	[number]	k4	1
bilion	bilion	k4xCgInSc4	bilion
rublů	rubl	k1gInPc2	rubl
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
kaskády	kaskáda	k1gFnPc1	kaskáda
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
řekách	řeka	k1gFnPc6	řeka
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Volha	Volha	k1gFnSc1	Volha
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
potenciál	potenciál	k1gInSc1	potenciál
východní	východní	k2eAgFnSc2d1	východní
Sibiře	Sibiř	k1gFnSc2	Sibiř
zatím	zatím	k6eAd1	zatím
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
většinou	většinou	k6eAd1	většinou
nevyužitý	využitý	k2eNgInSc1d1	nevyužitý
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
63	[number]	k4	63
<g/>
%	%	kIx~	%
z	z	k7c2	z
ruské	ruský	k2eAgFnSc2d1	ruská
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
generováno	generovat	k5eAaImNgNnS	generovat
tepelnými	tepelný	k2eAgFnPc7d1	tepelná
elektrárnami	elektrárna	k1gFnPc7	elektrárna
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
%	%	kIx~	%
vodními	vodní	k2eAgFnPc7d1	vodní
a	a	k8xC	a
16	[number]	k4	16
<g/>
%	%	kIx~	%
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Rusko	Rusko	k1gNnSc1	Rusko
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
951	[number]	k4	951
TWh	TWh	k1gMnPc2	TWh
a	a	k8xC	a
exportovalo	exportovat	k5eAaBmAgNnS	exportovat
23	[number]	k4	23
TWh	TWh	k1gMnPc2	TWh
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Žebříček	žebříček	k1gInSc1	žebříček
firem	firma	k1gFnPc2	firma
podle	podle	k7c2	podle
kapitalizace	kapitalizace	k1gFnSc2	kapitalizace
(	(	kIx(	(
<g/>
časopis	časopis	k1gInSc1	časopis
"	"	kIx"	"
<g/>
Expert	expert	k1gMnSc1	expert
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Rusko	Rusko	k1gNnSc1	Rusko
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
železniční	železniční	k2eAgFnSc4d1	železniční
síť	síť	k1gFnSc4	síť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
státem	stát	k1gInSc7	stát
vlastněné	vlastněný	k2eAgFnSc2d1	vlastněná
společnosti	společnost	k1gFnSc2	společnost
RŽD	RŽD	kA	RŽD
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnPc1	železnice
používají	používat	k5eAaImIp3nP	používat
oproti	oproti	k7c3	oproti
Česku	Česko	k1gNnSc3	Česko
širší	široký	k2eAgInSc4d2	širší
rozchod	rozchod	k1gInSc4	rozchod
(	(	kIx(	(
<g/>
1520	[number]	k4	1520
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Transsibiřská	transsibiřský	k2eAgFnSc1d1	Transsibiřská
magistrála	magistrála	k1gFnSc1	magistrála
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
do	do	k7c2	do
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
projíždí	projíždět	k5eAaImIp3nP	projíždět
rekordních	rekordní	k2eAgFnPc2d1	rekordní
sedm	sedm	k4xCc1	sedm
časových	časový	k2eAgNnPc2d1	časové
pásem	pásmo	k1gNnPc2	pásmo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
železniční	železniční	k2eAgFnSc7d1	železniční
tratí	trať	k1gFnSc7	trať
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1	vysokorychlostní
trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Petrohrad-Moskva-Nižnij	Petrohrad-Moskva-Nižnij	k1gFnPc2	Petrohrad-Moskva-Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
ruské	ruský	k2eAgInPc1d1	ruský
vysokorychlostní	vysokorychlostní	k2eAgInPc1d1	vysokorychlostní
vlaky	vlak	k1gInPc1	vlak
Sapsan	Sapsana	k1gFnPc2	Sapsana
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
druhem	druh	k1gInSc7	druh
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
bývají	bývat	k5eAaImIp3nP	bývat
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
tramvají	tramvaj	k1gFnPc2	tramvaj
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
české	český	k2eAgFnSc2d1	Česká
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
Tatra	Tatra	k1gFnSc1	Tatra
T	T	kA	T
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Tatra	Tatra	k1gFnSc1	Tatra
T6B5	T6B5	k1gMnSc1	T6B5
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linky	linka	k1gFnPc1	linka
metra	metro	k1gNnSc2	metro
využívá	využívat	k5eAaImIp3nS	využívat
sedm	sedm	k4xCc1	sedm
měst	město	k1gNnPc2	město
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Nižnij	Nižnij	k1gFnSc1	Nižnij
Novgorod	Novgorod	k1gInSc1	Novgorod
<g/>
,	,	kIx,	,
Novosibirsk	Novosibirsk	k1gInSc1	Novosibirsk
<g/>
,	,	kIx,	,
Samara	Samara	k1gFnSc1	Samara
<g/>
,	,	kIx,	,
Jekatěrinburg	Jekatěrinburg	k1gInSc1	Jekatěrinburg
a	a	k8xC	a
Kazaň	Kazaň	k1gFnSc1	Kazaň
<g/>
)	)	kIx)	)
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
systém	systém	k1gInSc1	systém
Metrotram	Metrotram	k1gInSc1	Metrotram
využívají	využívat	k5eAaPmIp3nP	využívat
další	další	k2eAgFnPc1d1	další
tři	tři	k4xCgFnPc1	tři
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
je	být	k5eAaImIp3nS	být
metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Čeljabinsku	Čeljabinsk	k1gInSc6	Čeljabinsk
<g/>
,	,	kIx,	,
Krasnojarsku	Krasnojarsek	k1gInSc6	Krasnojarsek
a	a	k8xC	a
Omsku	Omsk	k1gInSc6	Omsk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
metro	metro	k1gNnSc1	metro
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
mělo	mít	k5eAaImAgNnS	mít
Rusko	Rusko	k1gNnSc1	Rusko
755,000	[number]	k4	755,000
km	km	kA	km
zpevněných	zpevněný	k2eAgFnPc2d1	zpevněná
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nejdůležitější	důležitý	k2eAgMnPc4d3	nejdůležitější
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
Ruské	ruský	k2eAgFnSc2d1	ruská
federální	federální	k2eAgFnSc2d1	federální
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dálnice	dálnice	k1gFnPc1	dálnice
spojují	spojovat	k5eAaImIp3nP	spojovat
především	především	k9	především
Moskvu	Moskva	k1gFnSc4	Moskva
a	a	k8xC	a
okolní	okolní	k2eAgFnPc4d1	okolní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
M10	M10	k1gFnSc1	M10
spojuje	spojovat	k5eAaImIp3nS	spojovat
dvě	dva	k4xCgNnPc4	dva
největší	veliký	k2eAgNnPc4d3	veliký
města	město	k1gNnPc4	město
(	(	kIx(	(
<g/>
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
k	k	k7c3	k
finskému	finský	k2eAgNnSc3d1	finské
pohraničí	pohraničí	k1gNnSc3	pohraničí
<g/>
,	,	kIx,	,
M6	M6	k1gFnSc1	M6
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Volgograd	Volgograd	k1gInSc4	Volgograd
ke	k	k7c3	k
Kaspickému	kaspický	k2eAgNnSc3d1	Kaspické
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
M9	M9	k1gFnSc3	M9
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Trans-sibiřská	Transibiřský	k2eAgFnSc1d1	Trans-sibiřská
dálnice	dálnice	k1gFnSc1	dálnice
(	(	kIx(	(
<g/>
AH	ah	k0	ah
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
měří	měřit	k5eAaImIp3nS	měřit
přes	přes	k7c4	přes
11.000	[number]	k4	11.000
km	km	kA	km
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
na	na	k7c4	na
léta	léto	k1gNnPc4	léto
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2020	[number]	k4	2020
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
výstavbu	výstavba	k1gFnSc4	výstavba
120.000	[number]	k4	120.000
km	km	kA	km
nových	nový	k2eAgFnPc2d1	nová
silnic	silnice	k1gFnPc2	silnice
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
18.000	[number]	k4	18.000
km	km	kA	km
federálních	federální	k2eAgFnPc2d1	federální
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
vlastní	vlastní	k2eAgFnSc4d1	vlastní
jedinou	jediný	k2eAgFnSc4d1	jediná
flotilu	flotila	k1gFnSc4	flotila
atomových	atomový	k2eAgMnPc2d1	atomový
ledoborců	ledoborec	k1gMnPc2	ledoborec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
ruskými	ruský	k2eAgInPc7d1	ruský
námořními	námořní	k2eAgInPc7d1	námořní
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Rostov	Rostov	k1gInSc1	Rostov
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
u	u	k7c2	u
Azovského	azovský	k2eAgNnSc2d1	Azovské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Novorossijsk	Novorossijsk	k1gInSc1	Novorossijsk
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Černého	Černý	k1gMnSc2	Černý
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Astrachaň	Astrachaň	k1gFnSc4	Astrachaň
a	a	k8xC	a
Machačkala	Machačkala	k1gFnSc4	Machačkala
u	u	k7c2	u
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Kaliningrad	Kaliningrad	k1gInSc1	Kaliningrad
a	a	k8xC	a
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
u	u	k7c2	u
Baltu	Balt	k1gInSc2	Balt
<g/>
,	,	kIx,	,
Archangelsk	Archangelsk	k1gInSc1	Archangelsk
u	u	k7c2	u
Bílého	bílý	k2eAgNnSc2d1	bílé
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Murmansk	Murmansk	k1gInSc4	Murmansk
v	v	k7c6	v
Barentsově	barentsově	k6eAd1	barentsově
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
Petropavlovsk-Kamčatskij	Petropavlovsk-Kamčatskij	k1gFnSc3	Petropavlovsk-Kamčatskij
a	a	k8xC	a
Vladivostok	Vladivostok	k1gInSc4	Vladivostok
u	u	k7c2	u
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
výrobce	výrobce	k1gMnPc4	výrobce
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
patří	patřit	k5eAaImIp3nS	patřit
Suchoj	Suchoj	k1gInSc1	Suchoj
<g/>
,	,	kIx,	,
Tupolev	Tupolev	k1gFnSc1	Tupolev
a	a	k8xC	a
Iljušin	iljušin	k1gInSc1	iljušin
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
ruských	ruský	k2eAgMnPc2d1	ruský
leteckých	letecký	k2eAgMnPc2d1	letecký
výrobců	výrobce	k1gMnPc2	výrobce
spojena	spojit	k5eAaPmNgFnS	spojit
do	do	k7c2	do
holdingu	holding	k1gInSc2	holding
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
letecká	letecký	k2eAgFnSc1d1	letecká
korporace	korporace	k1gFnSc1	korporace
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
holding	holding	k1gInSc1	holding
Ruské	ruský	k2eAgInPc1d1	ruský
vrtulníky	vrtulník	k1gInPc1	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
pět	pět	k4xCc1	pět
typů	typ	k1gInPc2	typ
dopravních	dopravní	k2eAgNnPc2d1	dopravní
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc4	Rusko
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
několik	několik	k4yIc1	několik
vážných	vážný	k2eAgFnPc2d1	vážná
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
krizí	krize	k1gFnPc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
finanční	finanční	k2eAgFnSc1d1	finanční
krize	krize	k1gFnSc1	krize
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1998	[number]	k4	1998
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
širší	široký	k2eAgFnPc4d2	širší
příčiny	příčina	k1gFnPc4	příčina
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
až	až	k9	až
k	k	k7c3	k
východoasijské	východoasijský	k2eAgFnSc3d1	východoasijská
finanční	finanční	k2eAgFnSc3d1	finanční
krizi	krize	k1gFnSc3	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostřední	bezprostřední	k2eAgFnSc7d1	bezprostřední
příčinou	příčina	k1gFnSc7	příčina
byla	být	k5eAaImAgFnS	být
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
neschopnost	neschopnost	k1gFnSc1	neschopnost
Ruska	Ruska	k1gFnSc1	Ruska
splácet	splácet	k5eAaImF	splácet
svůj	svůj	k3xOyFgInSc4	svůj
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
měla	mít	k5eAaImAgFnS	mít
vážné	vážný	k2eAgInPc4d1	vážný
následky	následek	k1gInPc4	následek
i	i	k9	i
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
státech	stát	k1gInPc6	stát
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
finanční	finanční	k2eAgFnSc1d1	finanční
krize	krize	k1gFnSc1	krize
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
začal	začít	k5eAaPmAgInS	začít
ruský	ruský	k2eAgInSc1d1	ruský
rubl	rubl	k1gInSc1	rubl
oslabovat	oslabovat	k5eAaImF	oslabovat
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgFnPc3d1	ostatní
světovým	světový	k2eAgFnPc3d1	světová
měnám	měna	k1gFnPc3	měna
<g/>
,	,	kIx,	,
což	což	k9	což
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
valutovou	valutový	k2eAgFnSc4d1	valutová
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
rychlý	rychlý	k2eAgInSc1d1	rychlý
pokles	pokles	k1gInSc1	pokles
směnné	směnný	k2eAgFnSc2d1	směnná
hodnoty	hodnota	k1gFnSc2	hodnota
rublu	rubl	k1gInSc2	rubl
vůči	vůči	k7c3	vůči
americkému	americký	k2eAgInSc3d1	americký
dolaru	dolar	k1gInSc3	dolar
a	a	k8xC	a
euru	euro	k1gNnSc6	euro
o	o	k7c6	o
maximálních	maximální	k2eAgInPc6d1	maximální
50-60	[number]	k4	50-60
%	%	kIx~	%
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
následovalo	následovat	k5eAaImAgNnS	následovat
opět	opět	k6eAd1	opět
posílení	posílení	k1gNnSc1	posílení
rublu	rubl	k1gInSc2	rubl
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
prudce	prudko	k6eAd1	prudko
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
poptávkou	poptávka	k1gFnSc7	poptávka
občanů	občan	k1gMnPc2	občan
po	po	k7c6	po
dováženém	dovážený	k2eAgNnSc6d1	dovážené
zboží	zboží	k1gNnSc6	zboží
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hodnotou	hodnota	k1gFnSc7	hodnota
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
automobily	automobil	k1gInPc4	automobil
a	a	k8xC	a
televizory	televizor	k1gInPc4	televizor
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
důvody	důvod	k1gInPc7	důvod
byl	být	k5eAaImAgInS	být
odliv	odliv	k1gInSc1	odliv
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
kapitálu	kapitál	k1gInSc2	kapitál
kvůli	kvůli	k7c3	kvůli
ztrátě	ztráta	k1gFnSc3	ztráta
důvěry	důvěra	k1gFnSc2	důvěra
investorů	investor	k1gMnPc2	investor
v	v	k7c4	v
ruskou	ruský	k2eAgFnSc4d1	ruská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
poklesem	pokles	k1gInSc7	pokles
cen	cena	k1gFnPc2	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
surovin	surovina	k1gFnPc2	surovina
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
trzích	trh	k1gInPc6	trh
a	a	k8xC	a
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
sankcemi	sankce	k1gFnPc7	sankce
<g/>
,	,	kIx,	,
uvalenými	uvalený	k2eAgFnPc7d1	uvalená
na	na	k7c4	na
Rusko	Rusko	k1gNnSc4	Rusko
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
anexí	anexe	k1gFnSc7	anexe
Krymu	Krym	k1gInSc2	Krym
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
ruská	ruský	k2eAgFnSc1d1	ruská
krize	krize	k1gFnSc1	krize
způsobila	způsobit	k5eAaPmAgFnS	způsobit
téměř	téměř	k6eAd1	téměř
všem	všecek	k3xTgInPc3	všecek
státům	stát	k1gInPc3	stát
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Číny	Čína	k1gFnSc2	Čína
<g/>
)	)	kIx)	)
značné	značný	k2eAgInPc4d1	značný
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
poklesu	pokles	k1gInSc2	pokles
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
jejich	jejich	k3xOp3gFnPc2	jejich
měn	měna	k1gFnPc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
technika	technika	k1gFnSc1	technika
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
kvetly	kvést	k5eAaImAgInP	kvést
od	od	k7c2	od
osvícenství	osvícenství	k1gNnSc2	osvícenství
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
univerzit	univerzita	k1gFnPc2	univerzita
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
a	a	k8xC	a
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Ruské	ruský	k2eAgFnSc2d1	ruská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rozvoj	rozvoj	k1gInSc1	rozvoj
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
cara	car	k1gMnSc2	car
Petra	Petr	k1gMnSc2	Petr
Velikého	veliký	k2eAgMnSc2d1	veliký
a	a	k8xC	a
carevny	carevna	k1gFnPc4	carevna
Kateřiny	Kateřina	k1gFnSc2	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Veliké	veliký	k2eAgNnSc1d1	veliké
<g/>
,	,	kIx,	,
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
Rusko	Rusko	k1gNnSc4	Rusko
vlivům	vliv	k1gInPc3	vliv
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
matematická	matematický	k2eAgFnSc1d1	matematická
škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Ivanovskij	Ivanovskij	k1gFnPc6	Ivanovskij
objevil	objevit	k5eAaPmAgMnS	objevit
viry	vir	k1gInPc4	vir
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
periodickou	periodický	k2eAgFnSc4d1	periodická
tabulku	tabulka	k1gFnSc4	tabulka
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
Sergej	Sergej	k1gMnSc1	Sergej
Lebeděv	Lebeděv	k1gMnSc1	Lebeděv
syntetickou	syntetický	k2eAgFnSc4d1	syntetická
gumu	guma	k1gFnSc4	guma
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
vynálezci	vynálezce	k1gMnPc7	vynálezce
byli	být	k5eAaImAgMnP	být
Michail	Michail	k1gMnSc1	Michail
Britnev	Britnev	k1gFnSc1	Britnev
(	(	kIx(	(
<g/>
ledoborec	ledoborec	k1gInSc1	ledoborec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stěpan	Stěpan	k1gInSc1	Stěpan
Makarov	Makarov	k1gInSc1	Makarov
(	(	kIx(	(
<g/>
torpédová	torpédový	k2eAgFnSc1d1	torpédová
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
San	San	k1gMnSc1	San
Galli	Galle	k1gFnSc4	Galle
(	(	kIx(	(
<g/>
radiátor	radiátor	k1gInSc4	radiátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gleb	Gleb	k1gInSc1	Gleb
Kotelnikov	Kotelnikov	k1gInSc4	Kotelnikov
představil	představit	k5eAaPmAgInS	představit
padák	padák	k1gInSc1	padák
a	a	k8xC	a
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Čertovskij	Čertovskij	k1gFnSc4	Čertovskij
první	první	k4xOgInSc1	první
přetlakový	přetlakový	k2eAgInSc1d1	přetlakový
oblek	oblek	k1gInSc1	oblek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významnějším	významný	k2eAgInPc3d2	významnější
ruským	ruský	k2eAgInPc3d1	ruský
objevům	objev	k1gInPc3	objev
a	a	k8xC	a
vynálezům	vynález	k1gInPc3	vynález
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
elektrický	elektrický	k2eAgInSc1d1	elektrický
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
laser	laser	k1gInSc1	laser
<g/>
,	,	kIx,	,
maser	maser	k1gInSc1	maser
<g/>
,	,	kIx,	,
Lenzův	Lenzův	k2eAgInSc1d1	Lenzův
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
fotovoltaický	fotovoltaický	k2eAgInSc1d1	fotovoltaický
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
Čerenkovovo	Čerenkovův	k2eAgNnSc1d1	Čerenkovovo
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
3D	[number]	k4	3D
holografie	holografie	k1gFnSc1	holografie
<g/>
,	,	kIx,	,
elektronová	elektronový	k2eAgFnSc1d1	elektronová
paramagnetická	paramagnetický	k2eAgFnSc1d1	paramagnetická
rezonance	rezonance	k1gFnSc1	rezonance
<g/>
,	,	kIx,	,
heterotransistor	heterotransistor	k1gInSc1	heterotransistor
a	a	k8xC	a
tokamak	tokamak	k1gInSc1	tokamak
na	na	k7c4	na
termonukleární	termonukleární	k2eAgFnSc4d1	termonukleární
fúzi	fúze	k1gFnSc4	fúze
<g/>
.	.	kIx.	.
</s>
<s>
Proslulými	proslulý	k2eAgMnPc7d1	proslulý
leteckými	letecký	k2eAgMnPc7d1	letecký
konstruktéry	konstruktér	k1gMnPc7	konstruktér
byli	být	k5eAaImAgMnP	být
Andrej	Andrej	k1gMnSc1	Andrej
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tupolev	Tupolev	k1gMnSc1	Tupolev
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Iljušin	iljušin	k1gInSc1	iljušin
a	a	k8xC	a
Oleg	Oleg	k1gMnSc1	Oleg
Antonov	Antonov	k1gInSc1	Antonov
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
konstruktér	konstruktér	k1gMnSc1	konstruktér
pěchotních	pěchotní	k2eAgFnPc2d1	pěchotní
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Michail	Michail	k1gMnSc1	Michail
Kalašnikov	kalašnikov	k1gInSc4	kalašnikov
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
příspěvcích	příspěvek	k1gInPc6	příspěvek
ruských	ruský	k2eAgMnPc2d1	ruský
vědců	vědec	k1gMnPc2	vědec
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
kapitola	kapitola	k1gFnSc1	kapitola
níže	níže	k1gFnSc1	níže
<g/>
.	.	kIx.	.
</s>
<s>
Pilíři	pilíř	k1gInSc3	pilíř
ruské	ruský	k2eAgFnSc2d1	ruská
vědy	věda	k1gFnSc2	věda
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
Ivan	Ivan	k1gMnSc1	Ivan
Petrovič	Petrovič	k1gMnSc1	Petrovič
Pavlov	Pavlovo	k1gNnPc2	Pavlovo
a	a	k8xC	a
Michail	Michaila	k1gFnPc2	Michaila
Lomonosov	Lomonosovo	k1gNnPc2	Lomonosovo
<g/>
.	.	kIx.	.
</s>
<s>
Pavlov	Pavlov	k1gInSc1	Pavlov
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Pjotr	Pjotr	k1gMnSc1	Pjotr
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Kapica	Kapica	k1gMnSc1	Kapica
<g/>
,	,	kIx,	,
Andre	Andr	k1gInSc5	Andr
Geim	Geim	k1gMnSc1	Geim
<g/>
,	,	kIx,	,
Konstantin	Konstantin	k1gMnSc1	Konstantin
Novoselov	Novoselov	k1gInSc1	Novoselov
<g/>
,	,	kIx,	,
Alexej	Alexej	k1gMnSc1	Alexej
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
Abrikosov	Abrikosov	k1gInSc1	Abrikosov
<g/>
,	,	kIx,	,
Vitalij	Vitalij	k1gMnSc1	Vitalij
Lazarevič	Lazarevič	k1gMnSc1	Lazarevič
Ginzburg	Ginzburg	k1gMnSc1	Ginzburg
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gMnSc1	Ilja
Prigogine	Prigogin	k1gInSc5	Prigogin
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Gennadijevič	Gennadijevič	k1gMnSc1	Gennadijevič
Basov	Basov	k1gInSc1	Basov
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Prochorov	Prochorov	k1gInSc1	Prochorov
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
Čerenkov	Čerenkov	k1gInSc1	Čerenkov
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
Jevgeněvič	Jevgeněvič	k1gMnSc1	Jevgeněvič
Tamm	Tamm	k1gMnSc1	Tamm
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gMnSc1	Ilja
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Semjonov	Semjonovo	k1gNnPc2	Semjonovo
a	a	k8xC	a
Ilja	Ilja	k1gMnSc1	Ilja
Iljič	Iljič	k1gMnSc1	Iljič
Mečnikov	Mečnikov	k1gInSc4	Mečnikov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
významným	významný	k2eAgFnPc3d1	významná
osobnostem	osobnost	k1gFnPc3	osobnost
exaktních	exaktní	k2eAgFnPc2d1	exaktní
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
patří	patřit	k5eAaImIp3nS	patřit
Andrej	Andrej	k1gMnSc1	Andrej
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Kolmogorov	Kolmogorov	k1gInSc1	Kolmogorov
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Lobačevskij	Lobačevskij	k1gMnSc1	Lobačevskij
<g/>
,	,	kIx,	,
Grigorij	Grigorij	k1gMnSc1	Grigorij
Perelman	Perelman	k1gMnSc1	Perelman
<g/>
,	,	kIx,	,
Sofia	Sofia	k1gFnSc1	Sofia
Kovalevská	Kovalevský	k2eAgFnSc1d1	Kovalevská
<g/>
,	,	kIx,	,
Igor	Igor	k1gMnSc1	Igor
Kurčatov	Kurčatov	k1gInSc1	Kurčatov
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Oparin	Oparin	k1gInSc1	Oparin
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Markov	Markov	k1gInSc1	Markov
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Prokudin-Gorskij	Prokudin-Gorskij	k1gMnSc1	Prokudin-Gorskij
<g/>
,	,	kIx,	,
Pafnutij	Pafnutij	k1gMnSc1	Pafnutij
Lvovič	Lvovič	k1gMnSc1	Lvovič
Čebyšev	Čebyšev	k1gMnSc1	Čebyšev
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Fridman	Fridman	k1gMnSc1	Fridman
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Stěpanovič	Stěpanovič	k1gMnSc1	Stěpanovič
Popov	Popov	k1gInSc1	Popov
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Zvorykin	Zvorykin	k1gMnSc1	Zvorykin
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vavilov	Vavilov	k1gInSc1	Vavilov
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Vernadskij	Vernadskij	k1gMnSc1	Vernadskij
a	a	k8xC	a
Wladimir	Wladimir	k1gMnSc1	Wladimir
Köppen	Köppen	k2eAgMnSc1d1	Köppen
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
drastickému	drastický	k2eAgNnSc3d1	drastické
snížení	snížení	k1gNnSc3	snížení
státní	státní	k2eAgFnSc2d1	státní
podpory	podpora	k1gFnSc2	podpora
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
mozků	mozek	k1gInPc2	mozek
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
nového	nový	k2eAgInSc2d1	nový
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
boomu	boom	k1gInSc2	boom
situace	situace	k1gFnSc2	situace
ruské	ruský	k2eAgFnSc2d1	ruská
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
kampaň	kampaň	k1gFnSc4	kampaň
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
na	na	k7c4	na
modernizaci	modernizace	k1gFnSc4	modernizace
a	a	k8xC	a
inovaci	inovace	k1gFnSc4	inovace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
humanitních	humanitní	k2eAgFnPc2d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
literární	literární	k2eAgMnPc1d1	literární
vědci	vědec	k1gMnPc1	vědec
Michail	Michail	k1gMnSc1	Michail
Bachtin	Bachtin	k1gMnSc1	Bachtin
a	a	k8xC	a
Vissarion	Vissarion	k1gInSc1	Vissarion
Grigorjevič	Grigorjevič	k1gMnSc1	Grigorjevič
Bělinskij	Bělinskij	k1gMnSc1	Bělinskij
<g/>
,	,	kIx,	,
lingvisté	lingvista	k1gMnPc1	lingvista
Roman	Roman	k1gMnSc1	Roman
Jakobson	Jakobson	k1gMnSc1	Jakobson
a	a	k8xC	a
Vladimir	Vladimir	k1gMnSc1	Vladimir
Jakovlevič	Jakovlevič	k1gMnSc1	Jakovlevič
Propp	Propp	k1gMnSc1	Propp
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
moderní	moderní	k2eAgFnSc2d1	moderní
pedagogiky	pedagogika	k1gFnSc2	pedagogika
Anton	Anton	k1gMnSc1	Anton
Semjonovič	Semjonovič	k1gMnSc1	Semjonovič
Makarenko	Makarenka	k1gFnSc5	Makarenka
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
antropolog	antropolog	k1gMnSc1	antropolog
Andrej	Andrej	k1gMnSc1	Andrej
Korotajev	Korotajev	k1gMnSc1	Korotajev
<g/>
,	,	kIx,	,
teoretik	teoretik	k1gMnSc1	teoretik
kultury	kultura	k1gFnSc2	kultura
Anatolij	Anatolij	k1gMnSc1	Anatolij
Lunačarskij	Lunačarskij	k1gMnSc1	Lunačarskij
či	či	k8xC	či
historik	historik	k1gMnSc1	historik
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Karamzin	Karamzin	k1gMnSc1	Karamzin
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgMnPc7d3	nejznámější
ruskými	ruský	k2eAgMnPc7d1	ruský
filozofy	filozof	k1gMnPc7	filozof
jsou	být	k5eAaImIp3nP	být
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Berďajev	Berďajev	k1gMnSc1	Berďajev
či	či	k8xC	či
Vladimir	Vladimir	k1gMnSc1	Vladimir
Solovjov	Solovjov	k1gInSc1	Solovjov
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovými	klíčový	k2eAgMnPc7d1	klíčový
představiteli	představitel	k1gMnPc7	představitel
ruské	ruský	k2eAgFnSc2d1	ruská
psychologie	psychologie	k1gFnSc2	psychologie
a	a	k8xC	a
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
byli	být	k5eAaImAgMnP	být
Vladimir	Vladimir	k1gMnSc1	Vladimir
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Bechtěrev	Bechtěrev	k1gFnSc1	Bechtěrev
<g/>
,	,	kIx,	,
Lev	Lev	k1gMnSc1	Lev
Vygotskij	Vygotskij	k1gMnSc1	Vygotskij
a	a	k8xC	a
Alexandr	Alexandr	k1gMnSc1	Alexandr
Romanovič	Romanovič	k1gMnSc1	Romanovič
Lurija	Lurija	k1gMnSc1	Lurija
<g/>
.	.	kIx.	.
</s>
<s>
Leonid	Leonid	k1gInSc1	Leonid
Kantorovič	Kantorovič	k1gInSc1	Kantorovič
a	a	k8xC	a
Leonid	Leonid	k1gInSc1	Leonid
Hurwicz	Hurwicz	k1gMnSc1	Hurwicz
získali	získat	k5eAaPmAgMnP	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Sovětský	sovětský	k2eAgInSc4d1	sovětský
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
program	program	k1gInSc4	program
a	a	k8xC	a
Roskosmos	Roskosmos	k1gInSc4	Roskosmos
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
vědci	vědec	k1gMnPc1	vědec
a	a	k8xC	a
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
podstatně	podstatně	k6eAd1	podstatně
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
pronikání	pronikání	k1gNnSc3	pronikání
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
Ciolkovskij	Ciolkovskij	k1gMnSc1	Ciolkovskij
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
soudobé	soudobý	k2eAgFnSc2d1	soudobá
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
Sergeje	Sergej	k1gMnPc4	Sergej
Koroljova	Koroljův	k2eAgFnSc1d1	Koroljova
<g/>
,	,	kIx,	,
Valentina	Valentina	k1gFnSc1	Valentina
Gluška	Gluška	k1gFnSc1	Gluška
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
i	i	k9	i
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
programu	program	k1gInSc2	program
Sojuz	Sojuz	k1gInSc4	Sojuz
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
hranice	hranice	k1gFnPc4	hranice
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
vyslal	vyslat	k5eAaPmAgMnS	vyslat
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
první	první	k4xOgFnSc4	první
umělou	umělý	k2eAgFnSc4d1	umělá
družici	družice	k1gFnSc4	družice
Země	zem	k1gFnSc2	zem
Sputnik	sputnik	k1gInSc4	sputnik
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
vyslal	vyslat	k5eAaPmAgInS	vyslat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
živého	živý	k2eAgMnSc4d1	živý
tvora	tvor	k1gMnSc4	tvor
<g/>
,	,	kIx,	,
fenu	fena	k1gFnSc4	fena
Lajku	lajka	k1gFnSc4	lajka
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
triumfem	triumf	k1gInSc7	triumf
sovětského	sovětský	k2eAgInSc2d1	sovětský
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vyslání	vyslání	k1gNnSc1	vyslání
a	a	k8xC	a
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
návrat	návrat	k1gInSc1	návrat
majora	major	k1gMnSc2	major
Jurije	Jurije	k1gMnSc2	Jurije
Gagarina	Gagarin	k1gMnSc2	Gagarin
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc4	první
člověka	člověk	k1gMnSc4	člověk
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
Ruska	Ruska	k1gFnSc1	Ruska
Valentina	Valentina	k1gFnSc1	Valentina
Těreškovová	Těreškovová	k1gFnSc1	Těreškovová
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Luna	luna	k1gFnSc1	luna
9	[number]	k4	9
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
Veněra	Veněra	k1gFnSc1	Veněra
7	[number]	k4	7
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přistála	přistát	k5eAaPmAgFnS	přistát
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
planetě	planeta	k1gFnSc6	planeta
(	(	kIx(	(
<g/>
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mars	Mars	k1gInSc1	Mars
3	[number]	k4	3
pak	pak	k6eAd1	pak
rovněž	rovněž	k6eAd1	rovněž
první	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
také	také	k9	také
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
sestavil	sestavit	k5eAaPmAgInS	sestavit
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Země	zem	k1gFnSc2	zem
permanentně	permanentně	k6eAd1	permanentně
obydlenou	obydlený	k2eAgFnSc4d1	obydlená
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
stanici	stanice	k1gFnSc4	stanice
Mir	Mira	k1gFnPc2	Mira
<g/>
.	.	kIx.	.
</s>
<s>
Agentura	agentura	k1gFnSc1	agentura
Roskosmos	Roskosmos	k1gInSc1	Roskosmos
používá	používat	k5eAaImIp3nS	používat
pilotované	pilotovaný	k2eAgFnSc2d1	pilotovaná
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
lodě	loď	k1gFnSc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
vynáší	vynášet	k5eAaImIp3nS	vynášet
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
nosná	nosný	k2eAgFnSc1d1	nosná
raketa	raketa	k1gFnSc1	raketa
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
automatické	automatický	k2eAgFnSc2d1	automatická
nákladní	nákladní	k2eAgFnSc2d1	nákladní
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodě	loď	k1gFnSc2	loď
Progress	Progressa	k1gFnPc2	Progressa
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
raketoplánů	raketoplán	k1gInPc2	raketoplán
Buran	buran	k1gInSc1	buran
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rozpadu	rozpad	k1gInSc2	rozpad
SSSR	SSSR	kA	SSSR
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
amerického	americký	k2eAgInSc2d1	americký
programu	program	k1gInSc2	program
Space	Space	k1gFnSc2	Space
Shuttle	Shuttle	k1gFnSc2	Shuttle
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
ruské	ruský	k2eAgFnPc1d1	ruská
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
lodě	loď	k1gFnPc1	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jediným	jediný	k2eAgInSc7d1	jediný
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
kosmonauty	kosmonaut	k1gMnPc4	kosmonaut
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
astronauty	astronaut	k1gMnPc7	astronaut
<g/>
)	)	kIx)	)
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc3d1	vesmírná
stanici	stanice	k1gFnSc3	stanice
(	(	kIx(	(
<g/>
ISS	ISS	kA	ISS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
společného	společný	k2eAgInSc2d1	společný
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
těchto	tento	k3xDgFnPc2	tento
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
cest	cesta	k1gFnPc2	cesta
účastní	účastnit	k5eAaImIp3nP	účastnit
také	také	k9	také
astronauti	astronaut	k1gMnPc1	astronaut
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Rusko	Rusko	k1gNnSc4	Rusko
jejich	jejich	k3xOp3gNnSc4	jejich
zemím	zem	k1gFnPc3	zem
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
účastnická	účastnický	k2eAgNnPc4d1	účastnické
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
devátým	devátý	k4xOgInSc7	devátý
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
klesl	klesnout	k5eAaPmAgInS	klesnout
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
<g/>
;	;	kIx,	;
odhad	odhad	k1gInSc4	odhad
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2014	[number]	k4	2014
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
143	[number]	k4	143
657	[number]	k4	657
000	[number]	k4	000
obyvatelích	obyvatel	k1gMnPc6	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
800	[number]	k4	800
000	[number]	k4	000
více	hodně	k6eAd2	hodně
než	než	k8xS	než
při	při	k7c6	při
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
sčítání	sčítání	k1gNnSc6	sčítání
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
tedy	tedy	k9	tedy
opět	opět	k6eAd1	opět
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
mírně	mírně	k6eAd1	mírně
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
vlivem	vliv	k1gInSc7	vliv
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
poprvé	poprvé	k6eAd1	poprvé
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
imigrace	imigrace	k1gFnSc2	imigrace
a	a	k8xC	a
zvyšující	zvyšující	k2eAgFnSc1d1	zvyšující
se	se	k3xPyFc4	se
porodnosti	porodnost	k1gFnSc2	porodnost
<g/>
,	,	kIx,	,
demografického	demografický	k2eAgInSc2d1	demografický
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jen	jen	k9	jen
o	o	k7c4	o
20	[number]	k4	20
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Rozmístění	rozmístění	k1gNnSc1	rozmístění
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
nerovnoměrné	rovnoměrný	k2eNgNnSc1d1	nerovnoměrné
<g/>
:	:	kIx,	:
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
77	[number]	k4	77
%	%	kIx~	%
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přes	přes	k7c4	přes
110	[number]	k4	110
miliónů	milión	k4xCgInPc2	milión
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
obrovská	obrovský	k2eAgNnPc1d1	obrovské
území	území	k1gNnPc1	území
Sibiře	Sibiř	k1gFnSc2	Sibiř
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
liduprázdná	liduprázdný	k2eAgFnSc1d1	liduprázdná
–	–	k?	–
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
osídlení	osídlení	k1gNnSc2	osídlení
činí	činit	k5eAaImIp3nS	činit
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obrovské	obrovský	k2eAgFnSc3d1	obrovská
rozloze	rozloha	k1gFnSc3	rozloha
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgFnPc2d1	pouhá
8,4	[number]	k4	8,4
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
Čečensko	Čečensko	k1gNnSc1	Čečensko
<g/>
,	,	kIx,	,
Ingušsko	Ingušsko	k1gNnSc1	Ingušsko
<g/>
,	,	kIx,	,
Dagestán	Dagestán	k1gInSc1	Dagestán
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
vylidňují	vylidňovat	k5eAaImIp3nP	vylidňovat
oblasti	oblast	k1gFnPc1	oblast
evropského	evropský	k2eAgInSc2d1	evropský
západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
Pskovská	Pskovská	k1gFnSc1	Pskovská
<g/>
,	,	kIx,	,
Smolenská	Smolenský	k2eAgFnSc1d1	Smolenská
<g/>
,	,	kIx,	,
Novgorodská	novgorodský	k2eAgFnSc1d1	Novgorodská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
71	[number]	k4	71
let	léto	k1gNnPc2	léto
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
rozdílem	rozdíl	k1gInSc7	rozdíl
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
:	:	kIx,	:
65	[number]	k4	65
let	léto	k1gNnPc2	léto
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
77	[number]	k4	77
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ovládají	ovládat	k5eAaImIp3nP	ovládat
prakticky	prakticky	k6eAd1	prakticky
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
autonomní	autonomní	k2eAgInPc1d1	autonomní
celky	celek	k1gInPc1	celek
vedle	vedle	k7c2	vedle
ruštiny	ruština	k1gFnSc2	ruština
používají	používat	k5eAaImIp3nP	používat
místní	místní	k2eAgInPc1d1	místní
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
tatarština	tatarština	k1gFnSc1	tatarština
<g/>
,	,	kIx,	,
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
<g/>
,	,	kIx,	,
čuvaština	čuvaština	k1gFnSc1	čuvaština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
celkem	celkem	k6eAd1	celkem
tak	tak	k9	tak
Rusko	Rusko	k1gNnSc4	Rusko
oficiálně	oficiálně	k6eAd1	oficiálně
hovoří	hovořit	k5eAaImIp3nP	hovořit
31	[number]	k4	31
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
písmem	písmo	k1gNnSc7	písmo
je	být	k5eAaImIp3nS	být
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zapisovány	zapisován	k2eAgInPc4d1	zapisován
úřední	úřední	k2eAgInPc4d1	úřední
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
latinku	latinka	k1gFnSc4	latinka
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
geograficky	geograficky	k6eAd1	geograficky
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
jazyk	jazyk	k1gInSc1	jazyk
Eurasie	Eurasie	k1gFnSc2	Eurasie
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgInSc7	druhý
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
po	po	k7c6	po
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
oficiálních	oficiální	k2eAgInPc2d1	oficiální
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zastoupení	zastoupení	k1gNnSc1	zastoupení
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pravoslavné	pravoslavný	k2eAgNnSc4d1	pravoslavné
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
;	;	kIx,	;
dominantní	dominantní	k2eAgFnSc1d1	dominantní
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
Ruská	ruský	k2eAgFnSc1d1	ruská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
okolo	okolo	k7c2	okolo
poloviny	polovina	k1gFnSc2	polovina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Rusko	Rusko	k1gNnSc1	Rusko
sekulárním	sekulární	k2eAgInSc7d1	sekulární
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Ruská	ruský	k2eAgFnSc1d1	ruská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
uznávána	uznáván	k2eAgFnSc1d1	uznávána
i	i	k9	i
nevěřícími	nevěřící	k1gFnPc7	nevěřící
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
bylo	být	k5eAaImAgNnS	být
ruským	ruský	k2eAgNnSc7d1	ruské
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
od	od	k7c2	od
roku	rok	k1gInSc2	rok
988	[number]	k4	988
a	a	k8xC	a
ani	ani	k8xC	ani
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
protináboženského	protináboženský	k2eAgInSc2d1	protináboženský
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
církev	církev	k1gFnSc1	církev
krutě	krutě	k6eAd1	krutě
pronásledována	pronásledovat	k5eAaImNgFnS	pronásledovat
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
zcela	zcela	k6eAd1	zcela
potlačen	potlačen	k2eAgMnSc1d1	potlačen
a	a	k8xC	a
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
opět	opět	k6eAd1	opět
značně	značně	k6eAd1	značně
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
významným	významný	k2eAgInSc7d1	významný
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
sunnitský	sunnitský	k2eAgInSc1d1	sunnitský
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
zejména	zejména	k9	zejména
turkické	turkický	k2eAgInPc1d1	turkický
národy	národ	k1gInPc1	národ
a	a	k8xC	a
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
tvoří	tvořit	k5eAaImIp3nP	tvořit
muslimové	muslim	k1gMnPc1	muslim
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
14,5	[number]	k4	14,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
celkové	celkový	k2eAgFnSc2d1	celková
143	[number]	k4	143
miliónové	miliónový	k2eAgFnSc2d1	miliónová
ruské	ruský	k2eAgFnSc2d1	ruská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgMnSc1d1	duchovní
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
hovoří	hovořit	k5eAaImIp3nS	hovořit
až	až	k9	až
o	o	k7c6	o
20	[number]	k4	20
miliónech	milión	k4xCgInPc6	milión
přívrženců	přívrženec	k1gMnPc2	přívrženec
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Etničtí	etnický	k2eAgMnPc1d1	etnický
muslimové	muslim	k1gMnPc1	muslim
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
národy	národ	k1gInPc7	národ
tradičně	tradičně	k6eAd1	tradičně
muslimské	muslimský	k2eAgNnSc1d1	muslimské
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinu	většina	k1gFnSc4	většina
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
regionech	region	k1gInPc6	region
Ruské	ruský	k2eAgFnPc1d1	ruská
federace	federace	k1gFnPc1	federace
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
v	v	k7c6	v
Ingušsku	Ingušsko	k1gNnSc6	Ingušsko
(	(	kIx(	(
<g/>
98	[number]	k4	98
%	%	kIx~	%
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čečenské	čečenský	k2eAgFnSc6d1	čečenská
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
96	[number]	k4	96
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dagestánu	Dagestán	k1gInSc6	Dagestán
(	(	kIx(	(
<g/>
94	[number]	k4	94
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c4	v
Kabardino-Balkaria	Kabardino-Balkarium	k1gNnPc4	Kabardino-Balkarium
(	(	kIx(	(
<g/>
70	[number]	k4	70
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Karačajevo-Cherkessia	Karačajevo-Cherkessia	k1gFnSc1	Karačajevo-Cherkessia
(	(	kIx(	(
<g/>
54,6	[number]	k4	54,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c4	v
Baškortostán	Baškortostán	k1gInSc4	Baškortostán
(	(	kIx(	(
<g/>
54,5	[number]	k4	54,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
Tatarstán	Tatarstán	k1gInSc1	Tatarstán
(	(	kIx(	(
<g/>
54	[number]	k4	54
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
komunity	komunita	k1gFnPc4	komunita
římských	římský	k2eAgMnPc2d1	římský
a	a	k8xC	a
řeckých	řecký	k2eAgMnPc2d1	řecký
katolíků	katolík	k1gMnPc2	katolík
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protestantů	protestant	k1gMnPc2	protestant
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buddhistů	buddhista	k1gMnPc2	buddhista
(	(	kIx(	(
<g/>
Kalmycko	Kalmycko	k1gNnSc1	Kalmycko
<g/>
,	,	kIx,	,
Burjatsko	Burjatsko	k1gNnSc1	Burjatsko
a	a	k8xC	a
Tuva	Tuva	k1gFnSc1	Tuva
<g/>
;	;	kIx,	;
0,5	[number]	k4	0,5
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
židů	žid	k1gMnPc2	žid
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
národů	národ	k1gInPc2	národ
Sibiře	Sibiř	k1gFnSc2	Sibiř
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
původní	původní	k2eAgNnPc4d1	původní
animistická	animistický	k2eAgNnPc4d1	animistické
náboženství	náboženství	k1gNnPc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
také	také	k9	také
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
novopohanství	novopohanství	k1gNnSc2	novopohanství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
54	[number]	k4	54
%	%	kIx~	%
ruské	ruský	k2eAgFnSc2d1	ruská
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
dosažení	dosažení	k1gNnSc2	dosažení
vysokoškolské	vysokoškolský	k2eAgFnSc2d1	vysokoškolská
úrovně	úroveň	k1gFnSc2	úroveň
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
gramotnosti	gramotnost	k1gFnSc2	gramotnost
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
99,4	[number]	k4	99,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
a	a	k8xC	a
sekundární	sekundární	k2eAgNnSc1d1	sekundární
vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
jedenáctileté	jedenáctiletý	k2eAgNnSc1d1	jedenáctileté
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
nebo	nebo	k8xC	nebo
sedmém	sedmý	k4xOgInSc6	sedmý
roce	rok	k1gInSc6	rok
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
etapy	etapa	k1gFnPc4	etapa
<g/>
;	;	kIx,	;
základní	základní	k2eAgInPc4d1	základní
(	(	kIx(	(
<g/>
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgMnSc1d1	střední
sekundární	sekundární	k2eAgMnSc1d1	sekundární
(	(	kIx(	(
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
sekundární	sekundární	k2eAgMnSc1d1	sekundární
(	(	kIx(	(
<g/>
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
počet	počet	k1gInSc1	počet
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
technického	technický	k2eAgNnSc2d1	technické
i	i	k8xC	i
humanitního	humanitní	k2eAgNnSc2d1	humanitní
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
dělení	dělení	k1gNnSc2	dělení
studia	studio	k1gNnSc2	studio
na	na	k7c4	na
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
a	a	k8xC	a
magisterské	magisterský	k2eAgNnSc4d1	magisterské
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
řádně	řádně	k6eAd1	řádně
složil	složit	k5eAaPmAgMnS	složit
přijímací	přijímací	k2eAgFnPc4d1	přijímací
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vynikající	vynikající	k2eAgInPc4d1	vynikající
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vzdělání	vzdělání	k1gNnSc4	vzdělání
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
dostává	dostávat	k5eAaImIp3nS	dostávat
měsíční	měsíční	k2eAgNnSc1d1	měsíční
prospěchové	prospěchový	k2eAgNnSc1d1	prospěchové
stipendium	stipendium	k1gNnSc1	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c6	na
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
13	[number]	k4	13
%	%	kIx~	%
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
(	(	kIx(	(
<g/>
3,6	[number]	k4	3,6
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
zkouškový	zkouškový	k2eAgInSc4d1	zkouškový
měsíc	měsíc	k1gInSc4	měsíc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
studenti	student	k1gMnPc1	student
z	z	k7c2	z
devátého	devátý	k4xOgInSc2	devátý
a	a	k8xC	a
jedenáctého	jedenáctý	k4xOgInSc2	jedenáctý
ročníku	ročník	k1gInSc2	ročník
skládají	skládat	k5eAaImIp3nP	skládat
různé	různý	k2eAgFnPc4d1	různá
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Známkuje	známkovat	k5eAaImIp3nS	známkovat
se	se	k3xPyFc4	se
obráceně	obráceně	k6eAd1	obráceně
než	než	k8xS	než
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
jednička	jednička	k1gFnSc1	jednička
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
a	a	k8xC	a
pětka	pětka	k1gFnSc1	pětka
výborná	výborný	k2eAgFnSc1d1	výborná
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
a	a	k8xC	a
největší	veliký	k2eAgFnPc1d3	veliký
ruské	ruský	k2eAgFnPc1d1	ruská
univerzity	univerzita	k1gFnPc1	univerzita
jsou	být	k5eAaImIp3nP	být
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Petrohradská	petrohradský	k2eAgFnSc1d1	Petrohradská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
ruský	ruský	k2eAgInSc1d1	ruský
folklór	folklór	k1gInSc1	folklór
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
pohanském	pohanský	k2eAgNnSc6d1	pohanské
slovanském	slovanský	k2eAgNnSc6d1	slovanské
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c4	mnoho
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
dodržovány	dodržovat	k5eAaImNgFnP	dodržovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
Ruska	Rusko	k1gNnSc2	Rusko
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
mytí	mytí	k1gNnSc1	mytí
v	v	k7c4	v
zimní	zimní	k2eAgFnPc4d1	zimní
báně	báně	k1gFnPc4	báně
<g/>
,	,	kIx,	,
horké	horký	k2eAgFnSc3d1	horká
parní	parní	k2eAgFnSc3d1	parní
lázni	lázeň	k1gFnSc3	lázeň
podobné	podobný	k2eAgFnSc6d1	podobná
sauně	sauna	k1gFnSc6	sauna
<g/>
,	,	kIx,	,
před	před	k7c7	před
odjezdem	odjezd	k1gInSc7	odjezd
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
musí	muset	k5eAaImIp3nP	muset
všichni	všechen	k3xTgMnPc1	všechen
cestovatelé	cestovatel	k1gMnPc1	cestovatel
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
posedět	posedět	k5eAaPmF	posedět
v	v	k7c6	v
tichu	ticho	k1gNnSc6	ticho
<g/>
,	,	kIx,	,
zapískání	zapískání	k1gNnSc1	zapískání
v	v	k7c6	v
domě	dům	k1gInSc6	dům
přináší	přinášet	k5eAaImIp3nS	přinášet
velkou	velký	k2eAgFnSc4d1	velká
smůlu	smůla	k1gFnSc4	smůla
a	a	k8xC	a
matky	matka	k1gFnPc4	matka
obvykle	obvykle	k6eAd1	obvykle
neukazují	ukazovat	k5eNaImIp3nP	ukazovat
své	svůj	k3xOyFgNnSc4	svůj
dítě	dítě	k1gNnSc4	dítě
nikomu	nikdo	k3yNnSc3	nikdo
kromě	kromě	k7c2	kromě
otci	otec	k1gMnSc6	otec
a	a	k8xC	a
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
příbuzným	příbuzný	k1gMnPc3	příbuzný
čtyřicet	čtyřicet	k4xCc4	čtyřicet
dní	den	k1gInPc2	den
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
veškerou	veškerý	k3xTgFnSc4	veškerý
literaturu	literatura	k1gFnSc4	literatura
psanou	psaný	k2eAgFnSc7d1	psaná
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
jazyce	jazyk	k1gInSc6	jazyk
obyvateli	obyvatel	k1gMnPc7	obyvatel
Ruska	Rusko	k1gNnSc2	Rusko
nebo	nebo	k8xC	nebo
ruských	ruský	k2eAgMnPc2d1	ruský
emigrantů	emigrant	k1gMnPc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ruské	ruský	k2eAgFnSc2d1	ruská
literatury	literatura	k1gFnSc2	literatura
se	se	k3xPyFc4	se
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
i	i	k9	i
neruští	ruský	k2eNgMnPc1d1	neruský
spisovatelé	spisovatel	k1gMnPc1	spisovatel
z	z	k7c2	z
menších	malý	k2eAgInPc2d2	menší
národů	národ	k1gInPc2	národ
žijících	žijící	k2eAgInPc2d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
Ruska	Rusko	k1gNnSc2	Rusko
nebo	nebo	k8xC	nebo
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
do	do	k7c2	do
konce	konec	k1gInSc2	konec
období	období	k1gNnSc2	období
klasicismu	klasicismus	k1gInSc2	klasicismus
se	se	k3xPyFc4	se
o	o	k7c4	o
rozkvět	rozkvět	k1gInSc4	rozkvět
ruské	ruský	k2eAgFnSc2d1	ruská
literatury	literatura	k1gFnSc2	literatura
starali	starat	k5eAaImAgMnP	starat
hlavně	hlavně	k9	hlavně
ruští	ruský	k2eAgMnPc1d1	ruský
básníci	básník	k1gMnPc1	básník
a	a	k8xC	a
méně	málo	k6eAd2	málo
celosvětově	celosvětově	k6eAd1	celosvětově
známí	známý	k2eAgMnPc1d1	známý
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
ruská	ruský	k2eAgFnSc1d1	ruská
literatura	literatura	k1gFnSc1	literatura
nebývalý	bývalý	k2eNgInSc4d1	bývalý
rozmach	rozmach	k1gInSc4	rozmach
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
století	století	k1gNnSc2	století
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
klasikům	klasik	k1gMnPc3	klasik
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
bezpochyby	bezpochyby	k6eAd1	bezpochyby
patří	patřit	k5eAaImIp3nS	patřit
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
<g/>
,	,	kIx,	,
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
Anton	Anton	k1gMnSc1	Anton
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
Čechov	Čechov	k1gMnSc1	Čechov
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelé	spisovatel	k1gMnPc1	spisovatel
Josif	Josif	k1gMnSc1	Josif
Brodskij	Brodskij	k1gMnSc1	Brodskij
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Šolochov	Šolochov	k1gInSc1	Šolochov
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Solženicyn	Solženicyn	k1gMnSc1	Solženicyn
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
Bunin	Bunin	k1gMnSc1	Bunin
a	a	k8xC	a
Boris	Boris	k1gMnSc1	Boris
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Pasternak	Pasternak	k1gMnSc1	Pasternak
získali	získat	k5eAaPmAgMnP	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
národním	národní	k2eAgMnPc3d1	národní
klasikům	klasik	k1gMnPc3	klasik
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Vladimir	Vladimir	k1gMnSc1	Vladimir
Nabokov	Nabokov	k1gInSc1	Nabokov
<g/>
,	,	kIx,	,
Maxim	Maxim	k1gMnSc1	Maxim
Gorkij	Gorkij	k1gMnSc1	Gorkij
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Turgeněv	Turgeněv	k1gMnSc1	Turgeněv
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Andrejevna	Andrejevna	k1gFnSc1	Andrejevna
Achmatovová	Achmatovová	k1gFnSc1	Achmatovová
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Jurjevič	Jurjevič	k1gMnSc1	Jurjevič
Lermontov	Lermontov	k1gInSc1	Lermontov
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Bulgakov	Bulgakov	k1gInSc1	Bulgakov
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Jesenin	Jesenin	k2eAgMnSc1d1	Jesenin
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Majakovskij	Majakovskij	k1gMnSc1	Majakovskij
<g/>
,	,	kIx,	,
Marina	Marina	k1gFnSc1	Marina
Cvětajevová	Cvětajevová	k1gFnSc1	Cvětajevová
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Gončarov	Gončarovo	k1gNnPc2	Gončarovo
či	či	k8xC	či
bajkař	bajkař	k1gMnSc1	bajkař
Ivan	Ivan	k1gMnSc1	Ivan
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Krylov	Krylovo	k1gNnPc2	Krylovo
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
překročil	překročit	k5eAaPmAgInS	překročit
také	také	k9	také
věhlas	věhlas	k1gInSc1	věhlas
autorů	autor	k1gMnPc2	autor
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Alexandr	Alexandr	k1gMnSc1	Alexandr
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
Fjodor	Fjodor	k1gMnSc1	Fjodor
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Ťutčev	Ťutčev	k1gMnSc1	Ťutčev
<g/>
,	,	kIx,	,
Osip	Osip	k1gMnSc1	Osip
Mandelštam	Mandelštam	k1gInSc1	Mandelštam
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Kuprin	Kuprin	k1gInSc1	Kuprin
<g/>
,	,	kIx,	,
Vasilij	Vasilij	k1gMnSc1	Vasilij
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Žukovskij	Žukovskij	k1gMnSc1	Žukovskij
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Ostrovskij	Ostrovskij	k1gMnSc1	Ostrovskij
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
Někrasov	Někrasov	k1gInSc1	Někrasov
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Vozněsenskij	Vozněsenskij	k1gMnSc1	Vozněsenskij
či	či	k8xC	či
Andrej	Andrej	k1gMnSc1	Andrej
Bělyj	Bělyj	k1gMnSc1	Bělyj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
americká	americký	k2eAgFnSc1d1	americká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Ayn	Ayn	k1gFnSc1	Ayn
Randová	Randová	k1gFnSc1	Randová
<g/>
.	.	kIx.	.
</s>
<s>
Klasiky	klasika	k1gFnPc1	klasika
sci-fi	scii	k1gNnSc2	sci-fi
jsou	být	k5eAaImIp3nP	být
Alexej	Alexej	k1gMnSc1	Alexej
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
<g/>
,	,	kIx,	,
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Zamjatin	Zamjatina	k1gFnPc2	Zamjatina
či	či	k8xC	či
Arkadij	Arkadij	k1gMnSc1	Arkadij
a	a	k8xC	a
Boris	Boris	k1gMnSc1	Boris
Strugačtí	Strugacký	k1gMnPc1	Strugacký
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Ruský	ruský	k2eAgInSc1d1	ruský
balet	balet	k1gInSc1	balet
a	a	k8xC	a
Ruští	ruský	k2eAgMnPc1d1	ruský
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
pohanských	pohanský	k2eAgInPc2d1	pohanský
východních	východní	k2eAgInPc2d1	východní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
počátků	počátek	k1gInPc2	počátek
prošla	projít	k5eAaPmAgFnS	projít
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
historickým	historický	k2eAgInSc7d1	historický
vývojem	vývoj	k1gInSc7	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
duchovní	duchovní	k2eAgFnSc2d1	duchovní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
přejímání	přejímání	k1gNnSc6	přejímání
byzantské	byzantský	k2eAgFnSc2d1	byzantská
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
brzy	brzy	k6eAd1	brzy
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgInPc4	svůj
charakterisktické	charakterisktický	k2eAgInPc4d1	charakterisktický
ruské	ruský	k2eAgInPc4d1	ruský
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
hraje	hrát	k5eAaImIp3nS	hrát
nezastupitelnou	zastupitelný	k2eNgFnSc4d1	nezastupitelná
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
ruských	ruský	k2eAgFnPc6d1	ruská
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
znamennyj	znamennyj	k1gInSc1	znamennyj
raspěv	raspěv	k1gInSc4	raspěv
<g/>
,	,	kIx,	,
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
způsob	způsob	k1gInSc4	způsob
pravoslavného	pravoslavný	k2eAgInSc2d1	pravoslavný
církevního	církevní	k2eAgInSc2d1	církevní
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
archeologické	archeologický	k2eAgInPc4d1	archeologický
objevy	objev	k1gInPc4	objev
částí	část	k1gFnPc2	část
nejstarších	starý	k2eAgInPc2d3	nejstarší
strunných	strunný	k2eAgInPc2d1	strunný
nástrojů	nástroj	k1gInPc2	nástroj
v	v	k7c6	v
Novgorodu	Novgorod	k1gInSc6	Novgorod
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
ruská	ruský	k2eAgFnSc1d1	ruská
lidová	lidový	k2eAgFnSc1d1	lidová
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
světoznámými	světoznámý	k2eAgFnPc7d1	světoznámá
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Píseň	píseň	k1gFnSc1	píseň
burlaků	burlak	k1gInPc2	burlak
<g/>
,	,	kIx,	,
Kalinka	kalinka	k1gFnSc1	kalinka
<g/>
,	,	kIx,	,
Kaťuša	Kaťuša	k1gFnSc1	Kaťuša
<g/>
,	,	kIx,	,
Kozácká	kozácký	k2eAgFnSc1d1	kozácká
ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
,	,	kIx,	,
Dubinuška	Dubinuška	k1gFnSc1	Dubinuška
<g/>
,	,	kIx,	,
Poljuško	Poljuška	k1gFnSc5	Poljuška
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
Korobuška	Korobuška	k1gFnSc1	Korobuška
<g/>
,	,	kIx,	,
Oči	oko	k1gNnPc1	oko
čornyje	čornýt	k5eAaPmIp3nS	čornýt
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
umělé	umělý	k2eAgFnSc2d1	umělá
ruské	ruský	k2eAgFnSc2d1	ruská
hudby	hudba	k1gFnSc2	hudba
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
panování	panování	k1gNnSc2	panování
Petra	Petr	k1gMnSc2	Petr
Velikého	veliký	k2eAgInSc2d1	veliký
byly	být	k5eAaImAgFnP	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
západní	západní	k2eAgFnSc2d1	západní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
skladatelem	skladatel	k1gMnSc7	skladatel
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgMnS	být
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Bortňanskij	Bortňanskij	k1gMnSc1	Bortňanskij
-	-	kIx~	-
Ukrajinec	Ukrajinec	k1gMnSc1	Ukrajinec
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
tvorbě	tvorba	k1gFnSc6	tvorba
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
jak	jak	k8xC	jak
komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
a	a	k8xC	a
capellové	capellový	k2eAgInPc1d1	capellový
sborové	sborový	k2eAgInPc1d1	sborový
zpěvy	zpěv	k1gInPc1	zpěv
považované	považovaný	k2eAgInPc1d1	považovaný
za	za	k7c4	za
typické	typický	k2eAgNnSc4d1	typické
pro	pro	k7c4	pro
ruskou	ruský	k2eAgFnSc4d1	ruská
pravoslavnou	pravoslavný	k2eAgFnSc4d1	pravoslavná
duchovní	duchovní	k2eAgFnSc4d1	duchovní
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Inspirace	inspirace	k1gFnSc1	inspirace
ruskou	ruský	k2eAgFnSc7d1	ruská
lidovou	lidový	k2eAgFnSc7d1	lidová
hudbou	hudba	k1gFnSc7	hudba
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
operách	opera	k1gFnPc6	opera
a	a	k8xC	a
orchestrálních	orchestrální	k2eAgFnPc6d1	orchestrální
skladbách	skladba	k1gFnPc6	skladba
Michaila	Michail	k1gMnSc2	Michail
Ivanoviče	Ivanovič	k1gMnSc2	Ivanovič
Glinky	Glinka	k1gMnSc2	Glinka
a	a	k8xC	a
Alexandra	Alexandr	k1gMnSc2	Alexandr
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Dargomyšského	Dargomyšský	k2eAgMnSc2d1	Dargomyšský
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
předurčena	předurčit	k5eAaPmNgFnS	předurčit
cesta	cesta	k1gFnSc1	cesta
vývoje	vývoj	k1gInSc2	vývoj
ruské	ruský	k2eAgFnSc2d1	ruská
národní	národní	k2eAgFnSc2d1	národní
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
volné	volný	k2eAgNnSc1d1	volné
sdružení	sdružení	k1gNnSc1	sdružení
pěti	pět	k4xCc2	pět
mladých	mladý	k2eAgMnPc2d1	mladý
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Mocná	mocný	k2eAgFnSc1d1	mocná
hrstka	hrstka	k1gFnSc1	hrstka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
patřili	patřit	k5eAaImAgMnP	patřit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Borodin	Borodina	k1gFnPc2	Borodina
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
Kníže	kníže	k1gMnSc1	kníže
Igor	Igor	k1gMnSc1	Igor
se	s	k7c7	s
známými	známý	k2eAgInPc7d1	známý
Poloveckými	Polovecký	k2eAgInPc7d1	Polovecký
tanci	tanec	k1gInPc7	tanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
César	César	k1gMnSc1	César
Kjuj	Kjuj	k1gMnSc1	Kjuj
<g/>
,	,	kIx,	,
Milij	Milij	k1gMnSc1	Milij
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
Balakirev	Balakirev	k1gFnSc1	Balakirev
<g/>
,	,	kIx,	,
Modest	Modest	k1gMnSc1	Modest
Petrovič	Petrovič	k1gMnSc1	Petrovič
Musorgskij	Musorgskij	k1gMnSc1	Musorgskij
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnPc3	jeho
stěžejní	stěžejní	k2eAgNnPc4d1	stěžejní
díla	dílo	k1gNnPc4	dílo
jsou	být	k5eAaImIp3nP	být
opera	opera	k1gFnSc1	opera
Boris	Boris	k1gMnSc1	Boris
Godunov	Godunov	k1gInSc1	Godunov
a	a	k8xC	a
pásmo	pásmo	k1gNnSc1	pásmo
skladeb	skladba	k1gFnPc2	skladba
"	"	kIx"	"
<g/>
Kartinky	Kartinka	k1gFnPc4	Kartinka
<g/>
"	"	kIx"	"
Obrázky	obrázek	k1gInPc4	obrázek
z	z	k7c2	z
výstavy	výstava	k1gFnSc2	výstava
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Rimský-Korsakov	Rimský-Korsakov	k1gInSc4	Rimský-Korsakov
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
skladatelé	skladatel	k1gMnPc1	skladatel
si	se	k3xPyFc3	se
vytyčili	vytyčit	k5eAaPmAgMnP	vytyčit
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
použití	použití	k1gNnSc2	použití
prvků	prvek	k1gInPc2	prvek
osobité	osobitý	k2eAgFnSc2d1	osobitá
ruské	ruský	k2eAgFnSc2d1	ruská
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
symfoniích	symfonie	k1gFnPc6	symfonie
<g/>
,	,	kIx,	,
operách	opera	k1gFnPc6	opera
a	a	k8xC	a
komorní	komorní	k2eAgFnSc3d1	komorní
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
ruským	ruský	k2eAgMnSc7d1	ruský
skladatelem	skladatel	k1gMnSc7	skladatel
období	období	k1gNnSc2	období
romantismu	romantismus	k1gInSc2	romantismus
byl	být	k5eAaImAgMnS	být
Petr	Petr	k1gMnSc1	Petr
Iljič	Iljič	k1gMnSc1	Iljič
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
hudební	hudební	k2eAgNnSc1d1	hudební
dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sedm	sedm	k4xCc4	sedm
symfonií	symfonie	k1gFnPc2	symfonie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Patetické	patetický	k2eAgFnSc2d1	patetická
symfonie	symfonie	k1gFnSc2	symfonie
h-moll	holl	k1gMnSc1	h-moll
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
74	[number]	k4	74
a	a	k8xC	a
nečíslované	číslovaný	k2eNgFnSc2d1	nečíslovaná
Symfonie	symfonie	k1gFnSc2	symfonie
Manfred	Manfred	k1gMnSc1	Manfred
h	h	k?	h
moll	moll	k1gNnPc2	moll
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
58	[number]	k4	58
<g/>
)	)	kIx)	)
a	a	k8xC	a
proslulé	proslulý	k2eAgInPc1d1	proslulý
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
koncerty	koncert	k1gInPc1	koncert
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
(	(	kIx(	(
<g/>
D-dur	Dur	k1gMnSc1	D-dur
<g/>
)	)	kIx)	)
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
b-moll	boll	k1gInSc4	b-moll
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stálému	stálý	k2eAgInSc3d1	stálý
repertoáru	repertoár	k1gInSc2	repertoár
světových	světový	k2eAgNnPc2d1	světové
divadel	divadlo	k1gNnPc2	divadlo
patří	patřit	k5eAaImIp3nS	patřit
jeho	jeho	k3xOp3gFnSc2	jeho
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
a	a	k8xC	a
Piková	pikový	k2eAgFnSc1d1	Piková
dáma	dáma	k1gFnSc1	dáma
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnPc4	jeho
klasická	klasický	k2eAgNnPc4d1	klasické
baletní	baletní	k2eAgNnPc4d1	baletní
díla	dílo	k1gNnPc4	dílo
Labutí	labutí	k2eAgNnPc4d1	labutí
jezero	jezero	k1gNnSc4	jezero
<g/>
,	,	kIx,	,
Šípková	Šípková	k1gFnSc1	Šípková
Růženka	Růženka	k1gFnSc1	Růženka
a	a	k8xC	a
Louskáček	louskáček	k1gInSc1	louskáček
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
duchovní	duchovní	k2eAgFnSc1d1	duchovní
sborová	sborový	k2eAgFnSc1d1	sborová
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Romantickým	romantický	k2eAgMnSc7d1	romantický
skladatelem	skladatel	k1gMnSc7	skladatel
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Alexander	Alexandra	k1gFnPc2	Alexandra
Glazunov	Glazunovo	k1gNnPc2	Glazunovo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
velmi	velmi	k6eAd1	velmi
uznávána	uznáván	k2eAgFnSc1d1	uznávána
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
novějších	nový	k2eAgMnPc2d2	novější
ruských	ruský	k2eAgMnPc2d1	ruský
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Sergej	Sergej	k1gMnSc1	Sergej
Rachmaninov	Rachmaninov	k1gInSc1	Rachmaninov
(	(	kIx(	(
<g/>
význačné	význačný	k2eAgInPc1d1	význačný
koncerty	koncert	k1gInPc1	koncert
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alexandr	Alexandr	k1gMnSc1	Alexandr
Skrjabin	Skrjabina	k1gFnPc2	Skrjabina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
moderní	moderní	k2eAgMnPc4d1	moderní
autory	autor	k1gMnPc4	autor
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Prokofjev	Prokofjev	k1gMnSc1	Prokofjev
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Šostakovič	Šostakovič	k1gMnSc1	Šostakovič
<g/>
,	,	kIx,	,
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Kabalevskij	Kabalevskij	k1gMnSc1	Kabalevskij
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Schnittke	Schnittk	k1gFnSc2	Schnittk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
interprety	interpret	k1gMnPc7	interpret
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
violoncellista	violoncellista	k1gMnSc1	violoncellista
Mstislav	Mstislava	k1gFnPc2	Mstislava
Rostropovič	Rostropovič	k1gMnSc1	Rostropovič
či	či	k8xC	či
operní	operní	k2eAgMnPc1d1	operní
pěvci	pěvec	k1gMnPc1	pěvec
Fjodor	Fjodor	k1gMnSc1	Fjodor
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Šaljapin	Šaljapin	k1gMnSc1	Šaljapin
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Netrebko	Netrebka	k1gFnSc5	Netrebka
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
proslulé	proslulý	k2eAgFnSc2d1	proslulá
skupiny	skupina	k1gFnSc2	skupina
Ruský	ruský	k2eAgInSc1d1	ruský
balet	balet	k1gInSc1	balet
byl	být	k5eAaImAgMnS	být
Sergej	Sergej	k1gMnSc1	Sergej
Ďagilev	Ďagilev	k1gMnSc1	Ďagilev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
dvorním	dvorní	k2eAgMnSc7d1	dvorní
scénografem	scénograf	k1gMnSc7	scénograf
byl	být	k5eAaImAgMnS	být
Léon	Léon	k1gMnSc1	Léon
Bakst	Bakst	k1gMnSc1	Bakst
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Pavlovová	Pavlovová	k1gFnSc1	Pavlovová
a	a	k8xC	a
Maja	Maja	k1gFnSc1	Maja
Plisecká	Plisecká	k1gFnSc1	Plisecká
jsou	být	k5eAaImIp3nP	být
legendárními	legendární	k2eAgFnPc7d1	legendární
baletními	baletní	k2eAgFnPc7d1	baletní
tanečnicemi	tanečnice	k1gFnPc7	tanečnice
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc4d1	divadelní
pedagogiku	pedagogika	k1gFnSc4	pedagogika
zásadně	zásadně	k6eAd1	zásadně
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
Konstantin	Konstantin	k1gMnSc1	Konstantin
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Stanislavskij	Stanislavskij	k1gMnSc1	Stanislavskij
<g/>
.	.	kIx.	.
</s>
<s>
Představiteli	představitel	k1gMnSc3	představitel
ruského	ruský	k2eAgInSc2d1	ruský
popu	pop	k1gInSc2	pop
jsou	být	k5eAaImIp3nP	být
Valerij	Valerij	k1gFnSc1	Valerij
Leontijev	Leontijev	k1gFnSc1	Leontijev
<g/>
,	,	kIx,	,
Alla	Alla	k1gFnSc1	Alla
Pugačova	Pugačův	k2eAgFnSc1d1	Pugačova
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Viktor	Viktor	k1gMnSc1	Viktor
Coj	Coj	k1gMnSc1	Coj
<g/>
.	.	kIx.	.
rozvoj	rozvoj	k1gInSc1	rozvoj
popové	popový	k2eAgFnSc2d1	popová
a	a	k8xC	a
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
zažilo	zažít	k5eAaPmAgNnS	zažít
Rusko	Rusko	k1gNnSc1	Rusko
zejména	zejména	k9	zejména
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sovětské	sovětský	k2eAgFnSc2d1	sovětská
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnSc2	město
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Leningrad	Leningrad	k1gInSc1	Leningrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jekatěrinburg	Jekatěrinburg	k1gMnSc1	Jekatěrinburg
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Sverdlovsk	Sverdlovsk	k1gInSc1	Sverdlovsk
<g/>
)	)	kIx)	)
a	a	k8xC	a
Omsk	Omsk	k1gInSc4	Omsk
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavními	hlavní	k2eAgFnPc7d1	hlavní
středisky	středisko	k1gNnPc7	středisko
rockové	rockový	k2eAgFnPc1d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
například	například	k6eAd1	například
zpěváci	zpěvák	k1gMnPc1	zpěvák
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vysockij	Vysockij	k1gMnSc1	Vysockij
a	a	k8xC	a
Bulat	bulat	k5eAaImF	bulat
Okudžava	Okudžava	k1gFnSc1	Okudžava
<g/>
,	,	kIx,	,
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
např.	např.	kA	např.
Mašina	mašina	k1gFnSc1	mašina
Vremeni	Vremen	k2eAgMnPc1d1	Vremen
<g/>
,	,	kIx,	,
Slot	slot	k1gInSc1	slot
<g/>
,	,	kIx,	,
DDT	DDT	kA	DDT
<g/>
,	,	kIx,	,
Akvárium	akvárium	k1gNnSc1	akvárium
<g/>
,	,	kIx,	,
Alisa	Alisa	k1gFnSc1	Alisa
<g/>
,	,	kIx,	,
Kino	kino	k1gNnSc1	kino
<g/>
,	,	kIx,	,
Nautilus	Nautilus	k1gMnSc1	Nautilus
Pompilius	Pompilius	k1gMnSc1	Pompilius
<g/>
,	,	kIx,	,
Aria	Aria	k1gMnSc1	Aria
<g/>
,	,	kIx,	,
Graždanskaja	Graždanskaj	k2eAgFnSc1d1	Graždanskaja
oborona	oborona	k1gFnSc1	oborona
<g/>
,	,	kIx,	,
Splin	Splina	k1gFnPc2	Splina
a	a	k8xC	a
Korol	Korola	k1gFnPc2	Korola
i	i	k8xC	i
Šut	šuta	k1gFnPc2	šuta
<g/>
.	.	kIx.	.
</s>
<s>
Nejnověji	nově	k6eAd3	nově
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
známé	známý	k2eAgNnSc1d1	známé
popové	popový	k2eAgNnSc1d1	popové
duo	duo	k1gNnSc1	duo
Tatu	tata	k1gMnSc4	tata
a	a	k8xC	a
skupiny	skupina	k1gFnPc4	skupina
Nu	nu	k9	nu
Virgos	Virgos	k1gMnSc1	Virgos
a	a	k8xC	a
Vitas	Vitas	k1gMnSc1	Vitas
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
působily	působit	k5eAaImAgFnP	působit
také	také	k9	také
jazzové	jazzový	k2eAgFnPc1d1	jazzová
kapely	kapela	k1gFnPc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
ruské	ruský	k2eAgFnPc1d1	ruská
malby	malba	k1gFnPc1	malba
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
v	v	k7c6	v
ikonách	ikona	k1gFnPc6	ikona
a	a	k8xC	a
pestrých	pestrý	k2eAgFnPc6d1	pestrá
freskách	freska	k1gFnPc6	freska
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Císařská	císařský	k2eAgFnSc1d1	císařská
akademie	akademie	k1gFnSc1	akademie
umění	umění	k1gNnSc2	umění
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1757	[number]	k4	1757
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
umělecké	umělecký	k2eAgFnSc3d1	umělecká
společnosti	společnost	k1gFnSc3	společnost
Peredvižnici	peredvižnik	k1gMnPc1	peredvižnik
se	se	k3xPyFc4	se
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
zpopularizoval	zpopularizovat	k5eAaPmAgInS	zpopularizovat
realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dominoval	dominovat	k5eAaImAgInS	dominovat
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zlomová	zlomový	k2eAgFnSc1d1	zlomová
Ruská	ruský	k2eAgFnSc1d1	ruská
avantgarda	avantgarda	k1gFnSc1	avantgarda
převládala	převládat	k5eAaImAgFnS	převládat
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
konce	konec	k1gInSc2	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
musela	muset	k5eAaImAgFnS	muset
postupně	postupně	k6eAd1	postupně
většina	většina	k1gFnSc1	většina
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
sloužit	sloužit	k5eAaImF	sloužit
diktatuře	diktatura	k1gFnSc3	diktatura
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgNnP	být
díla	dílo	k1gNnPc1	dílo
velmi	velmi	k6eAd1	velmi
vlastenecká	vlastenecký	k2eAgNnPc1d1	vlastenecké
a	a	k8xC	a
propagandistická	propagandistický	k2eAgNnPc1d1	propagandistické
<g/>
,	,	kIx,	,
od	od	k7c2	od
omezení	omezení	k1gNnPc2	omezení
co	co	k9	co
umělci	umělec	k1gMnPc1	umělec
mohli	moct	k5eAaImAgMnP	moct
malovat	malovat	k5eAaImF	malovat
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
upouštět	upouštět	k5eAaImF	upouštět
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ermitáž	Ermitáž	k1gFnSc1	Ermitáž
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
spravuje	spravovat	k5eAaImIp3nS	spravovat
největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
obrazů	obraz	k1gInPc2	obraz
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějšími	slavný	k2eAgMnPc7d3	nejslavnější
ruskými	ruský	k2eAgMnPc7d1	ruský
malíři	malíř	k1gMnPc7	malíř
jsou	být	k5eAaImIp3nP	být
Vasilij	Vasilij	k1gMnSc1	Vasilij
Kandinskij	Kandinskij	k1gMnSc1	Kandinskij
<g/>
,	,	kIx,	,
Ilja	Ilja	k1gMnSc1	Ilja
Repin	Repin	k1gMnSc1	Repin
<g/>
,	,	kIx,	,
Kazimir	Kazimir	k1gMnSc1	Kazimir
Malevič	Malevič	k1gMnSc1	Malevič
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Ajvazovskij	Ajvazovskij	k1gMnSc1	Ajvazovskij
<g/>
,	,	kIx,	,
Andrej	Andrej	k1gMnSc1	Andrej
Rublev	Rublev	k1gMnSc1	Rublev
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Šiškin	Šiškin	k1gMnSc1	Šiškin
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Vasněcov	Vasněcov	k1gInSc1	Vasněcov
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Kramskoj	Kramskoj	k1gInSc4	Kramskoj
<g/>
,	,	kIx,	,
Vasilij	Vasilij	k1gFnSc4	Vasilij
Perov	Perovo	k1gNnPc2	Perovo
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Konstantinovič	Konstantinovič	k1gMnSc1	Konstantinovič
Rerich	Rericha	k1gFnPc2	Rericha
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Lisickij	Lisickij	k1gFnSc2	Lisickij
a	a	k8xC	a
Isaak	Isaak	k1gMnSc1	Isaak
Iljič	Iljič	k1gMnSc1	Iljič
Levitan	Levitan	k1gInSc4	Levitan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
christianizace	christianizace	k1gFnSc2	christianizace
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
Rusi	Rus	k1gFnSc2	Rus
byla	být	k5eAaImAgFnS	být
ruská	ruský	k2eAgFnSc1d1	ruská
architektura	architektura	k1gFnSc1	architektura
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
převážně	převážně	k6eAd1	převážně
byzantskou	byzantský	k2eAgFnSc7d1	byzantská
architekturou	architektura	k1gFnSc7	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
opevnění	opevnění	k1gNnSc2	opevnění
(	(	kIx(	(
<g/>
Kremlin	Kremlin	k1gInSc1	Kremlin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
hlavní	hlavní	k2eAgFnPc4d1	hlavní
kamenné	kamenný	k2eAgFnPc4d1	kamenná
budovy	budova	k1gFnPc4	budova
staré	starý	k2eAgFnPc4d1	stará
Rusy	Rus	k1gMnPc7	Rus
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
dómy	dóm	k1gInPc1	dóm
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
pozlacené	pozlacený	k2eAgFnPc1d1	pozlacená
nebo	nebo	k8xC	nebo
pestře	pestro	k6eAd1	pestro
malované	malovaný	k2eAgNnSc1d1	malované
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgMnSc1d1	italský
architekt	architekt	k1gMnSc1	architekt
Aristotele	Aristoteles	k1gMnSc5	Aristoteles
Fioravanti	Fioravant	k1gMnPc1	Fioravant
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
přinesli	přinést	k5eAaPmAgMnP	přinést
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
renesanci	renesance	k1gFnSc4	renesance
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
následujícím	následující	k2eAgNnSc6d1	následující
začala	začít	k5eAaPmAgFnS	začít
převládat	převládat	k5eAaImF	převládat
věžatá	věžatý	k2eAgFnSc1d1	věžatá
architektura	architektura	k1gFnSc1	architektura
reprezentující	reprezentující	k2eAgFnSc1d1	reprezentující
například	například	k6eAd1	například
Chrám	chrám	k1gInSc1	chrám
Vasila	Vasil	k1gMnSc2	Vasil
Blaženého	blažený	k2eAgMnSc2d1	blažený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
naryškinské	naryškinský	k2eAgNnSc1d1	naryškinský
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
také	také	k9	také
sibiřské	sibiřský	k2eAgNnSc4d1	sibiřské
baroko	baroko	k1gNnSc4	baroko
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dominuje	dominovat	k5eAaImIp3nS	dominovat
neo-byzantský	oyzantský	k2eNgInSc1d1	o-byzantský
či	či	k8xC	či
pseudoruský	pseudoruský	k2eAgInSc1d1	pseudoruský
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Art	Art	k1gMnSc1	Art
Nouveau	Nouveaa	k1gFnSc4	Nouveaa
<g/>
,	,	kIx,	,
konstruktivismus	konstruktivismus	k1gInSc4	konstruktivismus
a	a	k8xC	a
empír	empír	k1gInSc4	empír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
návrší	návrší	k1gNnSc6	návrší
zvaném	zvaný	k2eAgNnSc6d1	zvané
Mamajevova	Mamajevův	k2eAgFnSc1d1	Mamajevův
mohyla	mohyla	k1gFnSc1	mohyla
u	u	k7c2	u
Volgogradu	Volgograd	k1gInSc2	Volgograd
byla	být	k5eAaImAgFnS	být
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
největší	veliký	k2eAgFnSc1d3	veliký
socha	socha	k1gFnSc1	socha
ženy	žena	k1gFnSc2	žena
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
Matka	matka	k1gFnSc1	matka
vlast	vlast	k1gFnSc1	vlast
volá	volat	k5eAaImIp3nS	volat
<g/>
,	,	kIx,	,
zpodobněna	zpodobnit	k5eAaPmNgFnS	zpodobnit
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
antická	antický	k2eAgFnSc1d1	antická
bohyně	bohyně	k1gFnSc1	bohyně
vítězství	vítězství	k1gNnSc2	vítězství
Niké	Niké	k1gFnSc2	Niké
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
kinematografie	kinematografie	k1gFnSc1	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgInPc1d1	ruský
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
snímků	snímek	k1gInPc2	snímek
světové	světový	k2eAgFnSc2d1	světová
kinematografie	kinematografie	k1gFnSc2	kinematografie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
film	film	k1gInSc1	film
Křižník	křižník	k1gInSc4	křižník
Potěmkin	Potěmkin	k1gMnSc1	Potěmkin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
Sergej	Sergej	k1gMnSc1	Sergej
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Ejzenštejn	Ejzenštejn	k1gMnSc1	Ejzenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
stal	stát	k5eAaPmAgMnS	stát
Dziga	Dzig	k1gMnSc4	Dzig
Vertov	Vertov	k1gInSc4	Vertov
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
sovětské	sovětský	k2eAgNnSc4d1	sovětské
omezování	omezování	k1gNnSc4	omezování
kreativity	kreativita	k1gFnSc2	kreativita
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
natočit	natočit	k5eAaBmF	natočit
několik	několik	k4yIc4	několik
světově	světově	k6eAd1	světově
známých	známý	k2eAgInPc2d1	známý
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Jeřábi	jeřáb	k1gMnPc1	jeřáb
táhnou	táhnout	k5eAaImIp3nP	táhnout
<g/>
,	,	kIx,	,
Balada	balada	k1gFnSc1	balada
o	o	k7c6	o
vojákovi	voják	k1gMnSc6	voják
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
Eldara	Eldara	k1gFnSc1	Eldara
Rjazanova	Rjazanův	k2eAgFnSc1d1	Rjazanova
(	(	kIx(	(
<g/>
Ironie	ironie	k1gFnSc1	ironie
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
Služební	služební	k2eAgInSc1d1	služební
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
a	a	k8xC	a
Leonida	Leonida	k1gFnSc1	Leonida
Gajdaje	Gajdaje	k1gFnSc1	Gajdaje
<g/>
,	,	kIx,	,
oskarový	oskarový	k2eAgInSc1d1	oskarový
film	film	k1gInSc1	film
Sergeje	Sergej	k1gMnSc2	Sergej
Bondarčuka	Bondarčuk	k1gMnSc2	Bondarčuk
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Slavnými	slavný	k2eAgMnPc7d1	slavný
filmovými	filmový	k2eAgMnPc7d1	filmový
režiséry	režisér	k1gMnPc7	režisér
jsou	být	k5eAaImIp3nP	být
Andrej	Andrej	k1gMnSc1	Andrej
Tarkovskij	Tarkovskij	k1gMnSc1	Tarkovskij
a	a	k8xC	a
Nikita	Nikita	k1gMnSc1	Nikita
Michalkov	Michalkov	k1gInSc1	Michalkov
<g/>
.	.	kIx.	.
</s>
<s>
Sojuzmultfilm	Sojuzmultfilm	k6eAd1	Sojuzmultfilm
studio	studio	k1gNnSc1	studio
bylo	být	k5eAaImAgNnS	být
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
animace	animace	k1gFnSc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
postavičkám	postavička	k1gFnPc3	postavička
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
okolních	okolní	k2eAgFnPc6d1	okolní
zemích	zem	k1gFnPc6	zem
zejména	zejména	k9	zejména
Čeburaška	Čeburašek	k1gInSc2	Čeburašek
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
Medvídek	medvídek	k1gMnSc1	medvídek
Pú	Pú	k1gMnSc1	Pú
<g/>
,	,	kIx,	,
a	a	k8xC	a
vlk	vlk	k1gMnSc1	vlk
a	a	k8xC	a
zajíc	zajíc	k1gMnSc1	zajíc
z	z	k7c2	z
Jen	jen	k8xS	jen
počkej	počkat	k5eAaPmRp2nS	počkat
<g/>
,	,	kIx,	,
zajíci	zajíc	k1gMnSc5	zajíc
<g/>
!	!	kIx.	!
</s>
<s>
Pozdní	pozdní	k2eAgNnPc4d1	pozdní
osmdesátá	osmdesátý	k4xOgNnPc4	osmdesátý
a	a	k8xC	a
devadesátá	devadesátý	k4xOgNnPc4	devadesátý
léta	léto	k1gNnPc4	léto
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
filmu	film	k1gInSc6	film
a	a	k8xC	a
animaci	animace	k1gFnSc6	animace
obdobím	období	k1gNnSc7	období
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
několikrát	několikrát	k6eAd1	několikrát
oceněný	oceněný	k2eAgInSc4d1	oceněný
film	film	k1gInSc4	film
Svéráz	svéráz	k1gInSc1	svéráz
národního	národní	k2eAgInSc2d1	národní
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
produkce	produkce	k1gFnSc1	produkce
znovu	znovu	k6eAd1	znovu
vyplácet	vyplácet	k5eAaImF	vyplácet
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
úrovně	úroveň	k1gFnPc4	úroveň
než	než	k8xS	než
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
opravdovým	opravdový	k2eAgInSc7d1	opravdový
blockbusterem	blockbuster	k1gInSc7	blockbuster
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
film	film	k1gInSc1	film
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejúspěšnějšími	úspěšný	k2eAgInPc7d3	nejúspěšnější
ruskými	ruský	k2eAgInPc7d1	ruský
filmy	film	k1gInPc7	film
jsou	být	k5eAaImIp3nP	být
Ironie	ironie	k1gFnSc1	ironie
osudu	osud	k1gInSc2	osud
2	[number]	k4	2
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tajemný	tajemný	k2eAgInSc1d1	tajemný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stalingrad	Stalingrad	k1gInSc1	Stalingrad
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vij	vít	k5eAaImRp2nS	vít
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
ruská	ruský	k2eAgFnSc1d1	ruská
filmová	filmový	k2eAgFnSc1d1	filmová
cena	cena	k1gFnSc1	cena
Nika	nika	k1gFnSc1	nika
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
každoročně	každoročně	k6eAd1	každoročně
udělovaná	udělovaný	k2eAgFnSc1d1	udělovaná
Ruskou	ruský	k2eAgFnSc7d1	ruská
filmovou	filmový	k2eAgFnSc7d1	filmová
akademií	akademie	k1gFnSc7	akademie
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ruská	ruský	k2eAgFnSc1d1	ruská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
národní	národní	k2eAgNnSc1d1	národní
jídlo	jídlo	k1gNnSc1	jídlo
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
pelmeni	pelmen	k2eAgMnPc1d1	pelmen
(	(	kIx(	(
<g/>
taštičky	taštička	k1gFnPc1	taštička
z	z	k7c2	z
tenkého	tenký	k2eAgNnSc2d1	tenké
těsta	těsto	k1gNnSc2	těsto
plněné	plněný	k2eAgFnSc2d1	plněná
masem	maso	k1gNnSc7	maso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
polévka	polévka	k1gFnSc1	polévka
boršč	boršč	k1gInSc1	boršč
<g/>
,	,	kIx,	,
pirohy	piroh	k1gInPc1	piroh
<g/>
,	,	kIx,	,
grilovaný	grilovaný	k2eAgInSc1d1	grilovaný
šašlik	šašlik	k1gInSc1	šašlik
a	a	k8xC	a
ruský	ruský	k2eAgInSc1d1	ruský
černý	černý	k2eAgInSc1d1	černý
i	i	k8xC	i
červený	červený	k2eAgInSc1d1	červený
kaviár	kaviár	k1gInSc1	kaviár
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
také	také	k9	také
ruská	ruský	k2eAgFnSc1d1	ruská
vodka	vodka	k1gFnSc1	vodka
a	a	k8xC	a
ruský	ruský	k2eAgInSc1d1	ruský
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tradiční	tradiční	k2eAgNnSc4d1	tradiční
vaření	vaření	k1gNnSc4	vaření
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
čaj	čaj	k1gInSc4	čaj
se	se	k3xPyFc4	se
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
kuchyni	kuchyně	k1gFnSc6	kuchyně
užívá	užívat	k5eAaImIp3nS	užívat
tzv.	tzv.	kA	tzv.
samovar	samovar	k1gInSc1	samovar
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgInPc1d1	ruský
a	a	k8xC	a
sovětské	sovětský	k2eAgInPc1d1	sovětský
olympijské	olympijský	k2eAgInPc1d1	olympijský
týmy	tým	k1gInPc1	tým
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
čtyřmi	čtyři	k4xCgInPc7	čtyři
příčkami	příčka	k1gFnPc7	příčka
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc4	co
do	do	k7c2	do
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
medailí	medaile	k1gFnPc2	medaile
ze	z	k7c2	z
zimních	zimní	k2eAgFnPc2d1	zimní
a	a	k8xC	a
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
je	být	k5eAaImIp3nS	být
Rusko	Rusko	k1gNnSc1	Rusko
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
2014	[number]	k4	2014
byly	být	k5eAaImAgFnP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
právě	právě	k6eAd1	právě
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
a	a	k8xC	a
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
předstihlo	předstihnout	k5eAaPmAgNnS	předstihnout
Kanadu	Kanada	k1gFnSc4	Kanada
jako	jako	k8xS	jako
stát	stát	k5eAaImF	stát
s	s	k7c7	s
nejvíce	nejvíce	k6eAd1	nejvíce
zlatými	zlatý	k2eAgFnPc7d1	zlatá
medailemi	medaile	k1gFnPc7	medaile
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadnárodní	nadnárodní	k2eAgFnSc6d1	nadnárodní
evropské	evropský	k2eAgFnSc6d1	Evropská
lize	liga	k1gFnSc6	liga
KHL	KHL	kA	KHL
tvoří	tvořit	k5eAaImIp3nP	tvořit
velkou	velký	k2eAgFnSc4d1	velká
většinu	většina	k1gFnSc4	většina
ruští	ruský	k2eAgMnPc1d1	ruský
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
hráčů	hráč	k1gMnPc2	hráč
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
účastní	účastnit	k5eAaImIp3nS	účastnit
týmy	tým	k1gInPc4	tým
z	z	k7c2	z
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
,	,	kIx,	,
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
po	po	k7c6	po
NHL	NHL	kA	NHL
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
ligu	liga	k1gFnSc4	liga
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
sporty	sport	k1gInPc4	sport
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
také	také	k9	také
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
basketbal	basketbal	k1gInSc4	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgInPc1d1	ruský
kluby	klub	k1gInPc1	klub
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
Zenit	zenit	k1gInSc1	zenit
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
Pohár	pohár	k1gInSc4	pohár
UEFA	UEFA	kA	UEFA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Zenit	zenit	k1gInSc1	zenit
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
i	i	k9	i
Superpohár	superpohár	k1gInSc1	superpohár
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgMnS	porazit
vítěze	vítěz	k1gMnPc4	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc4	United
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
bude	být	k5eAaImBp3nS	být
hostit	hostit	k5eAaImF	hostit
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
je	být	k5eAaImIp3nS	být
další	další	k2eAgInSc4d1	další
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
ruský	ruský	k2eAgInSc4d1	ruský
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
bruslení	bruslení	k1gNnSc1	bruslení
v	v	k7c4	v
páru	pára	k1gFnSc4	pára
a	a	k8xC	a
tanec	tanec	k1gInSc4	tanec
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
sovětské	sovětský	k2eAgInPc1d1	sovětský
nebo	nebo	k8xC	nebo
ruské	ruský	k2eAgInPc1d1	ruský
páry	pár	k1gInPc1	pár
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Maria	Maria	k1gFnSc1	Maria
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
byla	být	k5eAaImAgFnS	být
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
i	i	k8xC	i
vítězkou	vítězka	k1gFnSc7	vítězka
všech	všecek	k3xTgInPc2	všecek
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
ATP	atp	kA	atp
Marat	Marat	k2eAgInSc4d1	Marat
Safin	Safin	k1gInSc4	Safin
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
zlaté	zlatý	k2eAgFnPc1d1	zlatá
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
olympiád	olympiáda	k1gFnPc2	olympiáda
má	mít	k5eAaImIp3nS	mít
tyčkařka	tyčkařka	k?	tyčkařka
Jelena	Jelena	k1gFnSc1	Jelena
Isinbajevová	Isinbajevová	k1gFnSc1	Isinbajevová
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
zlaté	zlatá	k1gFnPc4	zlatá
má	mít	k5eAaImIp3nS	mít
zápasník	zápasník	k1gMnSc1	zápasník
Alexandr	Alexandr	k1gMnSc1	Alexandr
Karelin	Karelin	k2eAgMnSc1d1	Karelin
<g/>
.	.	kIx.	.
</s>
<s>
Proslulá	proslulý	k2eAgFnSc1d1	proslulá
je	být	k5eAaImIp3nS	být
ruská	ruský	k2eAgFnSc1d1	ruská
šachová	šachový	k2eAgFnSc1d1	šachová
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
k	k	k7c3	k
legendám	legenda	k1gFnPc3	legenda
patří	patřit	k5eAaImIp3nP	patřit
Garri	Garr	k1gFnPc1	Garr
Kasparov	Kasparovo	k1gNnPc2	Kasparovo
<g/>
,	,	kIx,	,
Anatolij	Anatolij	k1gMnSc1	Anatolij
Karpov	Karpov	k1gInSc1	Karpov
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Kramnik	Kramnik	k1gMnSc1	Kramnik
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Alexandrovič	Alexandrovič	k1gMnSc1	Alexandrovič
Aljechin	Aljechin	k1gMnSc1	Aljechin
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Botvinnik	Botvinnik	k1gMnSc1	Botvinnik
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Tal	Tal	k1gMnSc1	Tal
nebo	nebo	k8xC	nebo
Boris	Boris	k1gMnSc1	Boris
Spasskij	Spasskij	k1gMnSc1	Spasskij
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
ruským	ruský	k2eAgMnSc7d1	ruský
fotbalistou	fotbalista	k1gMnSc7	fotbalista
a	a	k8xC	a
držitelem	držitel	k1gMnSc7	držitel
Zlatého	zlatý	k2eAgInSc2d1	zlatý
míče	míč	k1gInSc2	míč
byl	být	k5eAaImAgMnS	být
brankář	brankář	k1gMnSc1	brankář
Lev	Lev	k1gMnSc1	Lev
Jašin	Jašin	k1gMnSc1	Jašin
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
úspěchů	úspěch	k1gInPc2	úspěch
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Rusové	Rus	k1gMnPc1	Rus
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
stál	stát	k5eAaImAgMnS	stát
náročný	náročný	k2eAgMnSc1d1	náročný
trenér	trenér	k1gMnSc1	trenér
Viktor	Viktor	k1gMnSc1	Viktor
Tichonov	Tichonovo	k1gNnPc2	Tichonovo
<g/>
.	.	kIx.	.
</s>
<s>
Slavnou	slavný	k2eAgFnSc4d1	slavná
pětku	pětka	k1gFnSc4	pětka
snů	sen	k1gInPc2	sen
čili	čili	k8xC	čili
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
green	green	k1gInSc1	green
line	linout	k5eAaImIp3nS	linout
<g/>
"	"	kIx"	"
tvořili	tvořit	k5eAaImAgMnP	tvořit
Igor	Igor	k1gMnSc1	Igor
Larionov	Larionov	k1gInSc4	Larionov
<g/>
,	,	kIx,	,
Sergej	Sergej	k1gMnSc1	Sergej
Makarov	Makarov	k1gInSc1	Makarov
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gInSc1	Vladimir
Krutov	Krutov	k1gInSc1	Krutov
<g/>
,	,	kIx,	,
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Fetisov	Fetisov	k1gInSc1	Fetisov
a	a	k8xC	a
Alexej	Alexej	k1gMnSc1	Alexej
Kasatonov	Kasatonov	k1gInSc1	Kasatonov
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
brankářem	brankář	k1gMnSc7	brankář
byl	být	k5eAaImAgMnS	být
Vladislav	Vladislav	k1gMnSc1	Vladislav
Treťjak	Treťjak	k1gMnSc1	Treťjak
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
sovětské	sovětský	k2eAgFnSc2d1	sovětská
éry	éra	k1gFnSc2	éra
rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Rusko	Rusko	k1gNnSc1	Rusko
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
28.4	[number]	k4	28.4
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
devátý	devátý	k4xOgInSc1	devátý
nejnavštěvovanější	navštěvovaný	k2eAgInSc1d3	nejnavštěvovanější
stát	stát	k1gInSc1	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
sedmý	sedmý	k4xOgInSc4	sedmý
nejnavštěvovanější	navštěvovaný	k2eAgInSc4d3	nejnavštěvovanější
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanější	navštěvovaný	k2eAgFnPc1d3	nejnavštěvovanější
destinace	destinace	k1gFnPc1	destinace
jsou	být	k5eAaImIp3nP	být
současné	současný	k2eAgFnPc1d1	současná
a	a	k8xC	a
bývalé	bývalý	k2eAgNnSc1d1	bývalé
hlavní	hlavní	k2eAgNnSc1d1	hlavní
města	město	k1gNnPc1	město
země	zem	k1gFnSc2	zem
–	–	k?	–
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanější	navštěvovaný	k2eAgNnPc1d3	nejnavštěvovanější
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
Chrám	chrám	k1gInSc4	chrám
Vasila	Vasil	k1gMnSc4	Vasil
Blaženého	blažený	k2eAgMnSc4d1	blažený
<g/>
,	,	kIx,	,
Kreml	Kreml	k1gInSc1	Kreml
<g/>
,	,	kIx,	,
Rudé	rudý	k2eAgNnSc1d1	Rudé
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
Katedrála	katedrála	k1gFnSc1	katedrála
Krista	Kristus	k1gMnSc2	Kristus
Spasitele	spasitel	k1gMnSc2	spasitel
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
pravoslavný	pravoslavný	k2eAgInSc1d1	pravoslavný
kostel	kostel	k1gInSc1	kostel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Izáka	Izák	k1gMnSc2	Izák
<g/>
,	,	kIx,	,
Palácové	palácový	k2eAgNnSc1d1	palácové
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
Petěrgof	Petěrgof	k1gInSc1	Petěrgof
<g/>
,	,	kIx,	,
Treťjakovská	Treťjakovský	k2eAgFnSc1d1	Treťjakovská
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
Ermitáž	Ermitáž	k1gFnSc1	Ermitáž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
turistické	turistický	k2eAgFnPc4d1	turistická
trasy	trasa	k1gFnPc4	trasa
patří	patřit	k5eAaImIp3nS	patřit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kruh	kruh	k1gInSc1	kruh
<g/>
"	"	kIx"	"
historických	historický	k2eAgNnPc2d1	historické
měst	město	k1gNnPc2	město
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
plavby	plavba	k1gFnPc4	plavba
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
řekách	řeka	k1gFnPc6	řeka
např.	např.	kA	např.
Volze	Volha	k1gFnSc6	Volha
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
cesty	cesta	k1gFnPc4	cesta
přes	přes	k7c4	přes
slavnou	slavný	k2eAgFnSc4d1	slavná
Trans-sibiřskou	Transibiřský	k2eAgFnSc4d1	Trans-sibiřská
magistrálu	magistrála	k1gFnSc4	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Rekreačními	rekreační	k2eAgFnPc7d1	rekreační
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
subtropické	subtropický	k2eAgFnPc4d1	subtropická
pláže	pláž	k1gFnPc4	pláž
u	u	k7c2	u
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
lyžařská	lyžařský	k2eAgNnPc1d1	lyžařské
střediska	středisko	k1gNnPc1	středisko
v	v	k7c6	v
Předkavkazsku	Předkavkazsek	k1gInSc6	Předkavkazsek
(	(	kIx(	(
<g/>
Krasnaja	Krasnaj	k2eAgFnSc1d1	Krasnaja
Poljana	Poljana	k1gFnSc1	Poljana
<g/>
,	,	kIx,	,
Čeget	Čeget	k1gInSc1	Čeget
a	a	k8xC	a
Elbrus	Elbrus	k1gInSc1	Elbrus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
umělý	umělý	k2eAgInSc1d1	umělý
Ostrov	ostrov	k1gInSc1	ostrov
Federace	federace	k1gFnSc2	federace
s	s	k7c7	s
hotely	hotel	k1gInPc7	hotel
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
poblíž	poblíž	k7c2	poblíž
Soči	Soči	k1gNnSc2	Soči
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
tvar	tvar	k1gInSc4	tvar
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
turistů	turist	k1gMnPc2	turist
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
také	také	k9	také
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
suvenýry	suvenýr	k1gInPc1	suvenýr
jsou	být	k5eAaImIp3nP	být
Matrjoška	matrjoška	k1gFnSc1	matrjoška
<g/>
,	,	kIx,	,
Ušanka	ušanka	k1gFnSc1	ušanka
<g/>
,	,	kIx,	,
kožešinové	kožešinový	k2eAgNnSc1d1	kožešinové
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
,	,	kIx,	,
balalajka	balalajka	k1gFnSc1	balalajka
<g/>
,	,	kIx,	,
samovar	samovar	k1gInSc1	samovar
a	a	k8xC	a
kaviár	kaviár	k1gInSc1	kaviár
<g/>
.	.	kIx.	.
</s>
