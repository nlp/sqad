<s>
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
<g/>
,	,	kIx,	,
Rossija	Rossija	k1gFnSc1	Rossija
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Ruská	ruský	k2eAgFnSc1d1	ruská
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Р	Р	k?	Р
Ф	Ф	k?	Ф
<g/>
,	,	kIx,	,
Rossijskaja	Rossijskaja	k1gMnSc1	Rossijskaja
federacija	federacija	k1gMnSc1	federacija
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
17	[number]	k4	17
125	[number]	k4	125
191	[number]	k4	191
km2	km2	k4	km2
největší	veliký	k2eAgInSc4d3	veliký
stát	stát	k1gInSc4	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
