<s>
Praslovanština	praslovanština	k1gFnSc1	praslovanština
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
satemové	satemový	k2eAgFnSc3d1	satemový
skupině	skupina	k1gFnSc3	skupina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jazyky	jazyk	k1gInPc7	jazyk
indoíránskými	indoíránský	k2eAgInPc7d1	indoíránský
<g/>
,	,	kIx,	,
s	s	k7c7	s
arménštinou	arménština	k1gFnSc7	arménština
<g/>
,	,	kIx,	,
albánštinou	albánština	k1gFnSc7	albánština
a	a	k8xC	a
jazyky	jazyk	k1gInPc7	jazyk
baltskými	baltský	k2eAgInPc7d1	baltský
<g/>
.	.	kIx.	.
</s>
