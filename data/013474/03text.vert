<s>
Václav	Václav	k1gMnSc1
Kout	kout	k1gInSc4
</s>
<s>
Václav	Václav	k1gMnSc1
KoutOsobní	KoutOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
1951	#num#	k4
Klubové	klubový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
obránce	obránce	k1gMnSc1
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
Spartak	Spartak	k1gInSc1
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
<g/>
16	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Václav	Václav	k1gMnSc1
Kout	Kout	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1951	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
obránce	obránce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
Spartak	Spartak	k1gInSc4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
16	#num#	k4
ligových	ligový	k2eAgNnPc6d1
utkáních	utkání	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gól	gól	k1gInSc1
v	v	k7c6
lize	liga	k1gFnSc6
nedal	dát	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Spartak	Spartak	k1gInSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
CELKEM	celkem	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
16	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
–	–	k?
Libri	Libri	k1gNnSc1
1997	#num#	k4
</s>
<s>
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
Jenšík	Jenšík	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
českého	český	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
–	–	k?
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
2006	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
CSFOTBAL	CSFOTBAL	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
