<s>
Kosmologie	kosmologie	k1gFnSc1	kosmologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
κ	κ	k?	κ
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvětvím	odvětví	k1gNnSc7	odvětví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vesmírem	vesmír	k1gInSc7	vesmír
jako	jako	k8xC	jako
celkem	celek	k1gInSc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
studia	studio	k1gNnSc2	studio
kosmologie	kosmologie	k1gFnSc2	kosmologie
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc4	vznik
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
budoucí	budoucí	k2eAgInSc4d1	budoucí
osud	osud	k1gInSc4	osud
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
kosmologie	kosmologie	k1gFnSc2	kosmologie
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
kosmologie	kosmologie	k1gFnSc1	kosmologie
využívá	využívat	k5eAaPmIp3nS	využívat
poznatků	poznatek	k1gInPc2	poznatek
několika	několik	k4yIc2	několik
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
<g/>
,	,	kIx,	,
částicová	částicový	k2eAgFnSc1d1	částicová
a	a	k8xC	a
atomová	atomový	k2eAgFnSc1d1	atomová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
popisy	popis	k1gInPc1	popis
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
světě	svět	k1gInSc6	svět
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
proměn	proměna	k1gFnPc2	proměna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Babylónu	babylón	k1gInSc2	babylón
(	(	kIx(	(
<g/>
Enúma	Enúma	k1gFnSc1	Enúma
eliš	eliš	k5eAaPmIp2nS	eliš
<g/>
)	)	kIx)	)
a	a	k8xC	a
předjónského	předjónský	k2eAgNnSc2d1	předjónský
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
Hésiodova	Hésiodův	k2eAgNnPc1d1	Hésiodův
Theogonia	Theogonium	k1gNnPc1	Theogonium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Babylónské	babylónský	k2eAgInPc1d1	babylónský
mýty	mýtus	k1gInPc1	mýtus
měly	mít	k5eAaImAgInP	mít
pak	pak	k6eAd1	pak
mj.	mj.	kA	mj.
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
mytologické	mytologický	k2eAgFnPc4d1	mytologická
představy	představa	k1gFnPc4	představa
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
též	též	k9	též
na	na	k7c4	na
biblickou	biblický	k2eAgFnSc4d1	biblická
představu	představa	k1gFnSc4	představa
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
.	.	kIx.	.
</s>
<s>
Rané	raný	k2eAgFnPc1d1	raná
kosmologie	kosmologie	k1gFnPc1	kosmologie
měly	mít	k5eAaImAgFnP	mít
především	především	k9	především
mýtický	mýtický	k2eAgInSc4d1	mýtický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
předsókratičtí	předsókratický	k2eAgMnPc1d1	předsókratický
filozofové	filozof	k1gMnPc1	filozof
začali	začít	k5eAaPmAgMnP	začít
vznik	vznik	k1gInSc4	vznik
světa	svět	k1gInSc2	svět
zkoumat	zkoumat	k5eAaImF	zkoumat
kriticky	kriticky	k6eAd1	kriticky
(	(	kIx(	(
<g/>
Thalés	Thalés	k1gInSc1	Thalés
z	z	k7c2	z
Milétu	Milét	k1gInSc2	Milét
<g/>
,	,	kIx,	,
Anaximandros	Anaximandrosa	k1gFnPc2	Anaximandrosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
ubírá	ubírat	k5eAaImIp3nS	ubírat
i	i	k9	i
myšlení	myšlení	k1gNnSc1	myšlení
Démokritovo	Démokritův	k2eAgNnSc1d1	Démokritovo
a	a	k8xC	a
Anaxagorovo	Anaxagorův	k2eAgNnSc1d1	Anaxagorův
<g/>
.	.	kIx.	.
</s>
<s>
Důležitého	důležitý	k2eAgInSc2d1	důležitý
posunu	posun	k1gInSc2	posun
dějiny	dějiny	k1gFnPc1	dějiny
kosmologie	kosmologie	k1gFnSc1	kosmologie
nabraly	nabrat	k5eAaPmAgFnP	nabrat
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
doloženým	doložený	k2eAgInSc7d1	doložený
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nestavěl	stavět	k5eNaImAgInS	stavět
Zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
středu	střed	k1gInSc2	střed
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
který	který	k3yRgInSc1	který
jí	on	k3xPp3gFnSc3	on
dával	dávat	k5eAaImAgInS	dávat
kulový	kulový	k2eAgInSc1d1	kulový
tvar	tvar	k1gInSc1	tvar
–	–	k?	–
ten	ten	k3xDgInSc1	ten
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Filolaa	Filolaus	k1gMnSc2	Filolaus
<g/>
,	,	kIx,	,
žáka	žák	k1gMnSc2	žák
Pýthagorova	Pýthagorův	k2eAgInSc2d1	Pýthagorův
<g/>
,	,	kIx,	,
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Kosmologie	kosmologie	k1gFnSc1	kosmologie
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
modely	model	k1gInPc4	model
k	k	k7c3	k
vesmíru	vesmír	k1gInSc3	vesmír
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
popsat	popsat	k5eAaPmF	popsat
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
utváření	utváření	k1gNnSc3	utváření
větších	veliký	k2eAgInPc2d2	veliký
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
útvarů	útvar	k1gInPc2	útvar
(	(	kIx(	(
<g/>
galaxie	galaxie	k1gFnPc1	galaxie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
kupy	kupa	k1gFnPc1	kupa
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
modely	model	k1gInPc1	model
obvykle	obvykle	k6eAd1	obvykle
popisují	popisovat	k5eAaImIp3nP	popisovat
vesmír	vesmír	k1gInSc4	vesmír
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Kosmologové	kosmolog	k1gMnPc1	kosmolog
pak	pak	k6eAd1	pak
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
astronomy	astronom	k1gMnPc7	astronom
a	a	k8xC	a
astrofyziky	astrofyzik	k1gMnPc7	astrofyzik
hledají	hledat	k5eAaImIp3nP	hledat
experimentální	experimentální	k2eAgNnPc4d1	experimentální
potvrzení	potvrzení	k1gNnPc4	potvrzení
(	(	kIx(	(
<g/>
verifikaci	verifikace	k1gFnSc4	verifikace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
daného	daný	k2eAgInSc2d1	daný
kosmologického	kosmologický	k2eAgInSc2d1	kosmologický
modelu	model	k1gInSc2	model
na	na	k7c6	na
základě	základ	k1gInSc6	základ
porovnání	porovnání	k1gNnSc2	porovnání
předpovědí	předpověď	k1gFnPc2	předpověď
daného	daný	k2eAgInSc2d1	daný
modelu	model	k1gInSc2	model
s	s	k7c7	s
experimentálními	experimentální	k2eAgNnPc7d1	experimentální
daty	datum	k1gNnPc7	datum
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
současných	současný	k2eAgInPc2d1	současný
modelů	model	k1gInPc2	model
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
kosmologického	kosmologický	k2eAgInSc2d1	kosmologický
principu	princip	k1gInSc2	princip
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
z	z	k7c2	z
Koperníkova	Koperníkův	k2eAgInSc2d1	Koperníkův
principu	princip	k1gInSc2	princip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
hypotézy	hypotéza	k1gFnSc2	hypotéza
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
bodech	bod	k1gInPc6	bod
vesmíru	vesmír	k1gInSc2	vesmír
platí	platit	k5eAaImIp3nP	platit
stejné	stejný	k2eAgInPc1d1	stejný
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
také	také	k9	také
zavést	zavést	k5eAaPmF	zavést
předpoklad	předpoklad	k1gInSc4	předpoklad
o	o	k7c6	o
homogenitě	homogenita	k1gFnSc6	homogenita
a	a	k8xC	a
izotropii	izotropie	k1gFnSc6	izotropie
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
velkých	velký	k2eAgNnPc6d1	velké
měřítkách	měřítko	k1gNnPc6	měřítko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
třesk	třesk	k1gInSc1	třesk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vědecká	vědecký	k2eAgFnSc1d1	vědecká
mainstreamová	mainstreamový	k2eAgFnSc1d1	mainstreamová
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
a	a	k8xC	a
považuje	považovat	k5eAaImIp3nS	považovat
gravitaci	gravitace	k1gFnSc4	gravitace
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
sílu	síla	k1gFnSc4	síla
formující	formující	k2eAgInSc4d1	formující
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
alternativní	alternativní	k2eAgFnSc2d1	alternativní
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
plazmová	plazmový	k2eAgFnSc1d1	plazmová
kosmologie	kosmologie	k1gFnSc1	kosmologie
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
silnější	silný	k2eAgFnSc1d2	silnější
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
