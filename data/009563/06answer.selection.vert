<s>
Kaktusovité	kaktusovitý	k2eAgInPc4d1	kaktusovitý
(	(	kIx(	(
<g/>
Cactaceae	Cactacea	k1gInPc4	Cactacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
též	též	k6eAd1	též
jako	jako	k8xC	jako
kaktusy	kaktus	k1gInPc4	kaktus
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
hvozdíkotvaré	hvozdíkotvarý	k2eAgNnSc1d1	hvozdíkotvarý
<g/>
.	.	kIx.	.
</s>
