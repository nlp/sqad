<p>
<s>
Kaktusovité	kaktusovitý	k2eAgInPc4d1	kaktusovitý
(	(	kIx(	(
<g/>
Cactaceae	Cactacea	k1gInPc4	Cactacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
též	též	k6eAd1	též
jako	jako	k8xC	jako
kaktusy	kaktus	k1gInPc4	kaktus
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
hvozdíkotvaré	hvozdíkotvarý	k2eAgNnSc1d1	hvozdíkotvarý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vytrvalé	vytrvalý	k2eAgFnPc1d1	vytrvalá
rostliny	rostlina	k1gFnPc1	rostlina
se	s	k7c7	s
sukulentními	sukulentní	k2eAgInPc7d1	sukulentní
stonky	stonek	k1gInPc7	stonek
specifické	specifický	k2eAgFnSc2d1	specifická
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
nemá	mít	k5eNaImIp3nS	mít
zelené	zelený	k2eAgInPc4d1	zelený
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
přeměnily	přeměnit	k5eAaPmAgInP	přeměnit
v	v	k7c4	v
trny	trn	k1gInPc4	trn
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c4	mnoho
okvětních	okvětní	k2eAgInPc2d1	okvětní
lístků	lístek	k1gInPc2	lístek
i	i	k8xC	i
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
bobule	bobule	k1gFnSc1	bobule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
asi	asi	k9	asi
1900	[number]	k4	1900
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
127	[number]	k4	127
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jediného	jediný	k2eAgInSc2d1	jediný
druhu	druh	k1gInSc2	druh
ripsalisu	ripsalis	k1gInSc2	ripsalis
pocházejí	pocházet	k5eAaImIp3nP	pocházet
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
od	od	k7c2	od
Kanady	Kanada	k1gFnSc2	Kanada
až	až	k9	až
po	po	k7c6	po
Patagonii	Patagonie	k1gFnSc6	Patagonie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jz	jz	k?	jz
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
a	a	k8xC	a
sz.	sz.	k?	sz.
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
jihoamerických	jihoamerický	k2eAgFnPc6d1	jihoamerická
Andách	Anda	k1gFnPc6	Anda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
náležejí	náležet	k5eAaImIp3nP	náležet
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
známé	známý	k2eAgFnPc1d1	známá
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
pěstované	pěstovaný	k2eAgFnPc1d1	pěstovaná
v	v	k7c6	v
nepřeberném	přeberný	k2eNgNnSc6d1	nepřeberné
množství	množství	k1gNnSc6	množství
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Opuncie	opuncie	k1gFnSc1	opuncie
mexická	mexický	k2eAgFnSc1d1	mexická
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
pěstovány	pěstován	k2eAgFnPc4d1	pěstována
pro	pro	k7c4	pro
ovoce	ovoce	k1gNnPc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Lofofory	Lofofora	k1gFnPc1	Lofofora
<g/>
,	,	kIx,	,
Echinopsis	Echinopsis	k1gInSc1	Echinopsis
pachanoi	pachano	k1gFnSc2	pachano
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
halucinogeny	halucinogen	k1gInPc1	halucinogen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
jsou	být	k5eAaImIp3nP	být
vytrvalé	vytrvalý	k2eAgFnPc4d1	vytrvalá
sukulentní	sukulentní	k2eAgFnPc4d1	sukulentní
rostliny	rostlina	k1gFnPc4	rostlina
dosti	dosti	k6eAd1	dosti
rozmanitého	rozmanitý	k2eAgInSc2d1	rozmanitý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pozemních	pozemní	k2eAgMnPc2d1	pozemní
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
až	až	k6eAd1	až
sloupcovitý	sloupcovitý	k2eAgInSc1d1	sloupcovitý
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k9	i
zploštělý	zploštělý	k2eAgInSc4d1	zploštělý
a	a	k8xC	a
článkovaný	článkovaný	k2eAgInSc4d1	článkovaný
dužnatý	dužnatý	k2eAgInSc4d1	dužnatý
stonek	stonek	k1gInSc4	stonek
velmi	velmi	k6eAd1	velmi
specifické	specifický	k2eAgFnPc1d1	specifická
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hrbolkatý	hrbolkatý	k2eAgInSc4d1	hrbolkatý
<g/>
,	,	kIx,	,
žebernatý	žebernatý	k2eAgInSc4d1	žebernatý
až	až	k8xS	až
křídlatý	křídlatý	k2eAgInSc4d1	křídlatý
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
kaktusů	kaktus	k1gInPc2	kaktus
je	být	k5eAaImIp3nS	být
bezlistá	bezlistý	k2eAgFnSc1d1	bezlistá
a	a	k8xC	a
listy	lista	k1gFnPc1	lista
jsou	být	k5eAaImIp3nP	být
přeměněny	přeměnit	k5eAaPmNgFnP	přeměnit
v	v	k7c4	v
trny	trn	k1gInPc4	trn
<g/>
.	.	kIx.	.
</s>
<s>
Plně	plně	k6eAd1	plně
vyvinuté	vyvinutý	k2eAgInPc4d1	vyvinutý
listy	list	k1gInPc4	list
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
několika	několik	k4yIc2	několik
vývojově	vývojově	k6eAd1	vývojově
nejpůvodnějších	původní	k2eAgInPc2d3	nejpůvodnější
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
tyto	tento	k3xDgInPc1	tento
kaktusy	kaktus	k1gInPc1	kaktus
(	(	kIx(	(
<g/>
Pereskia	Pereskia	k1gFnSc1	Pereskia
lychnidiflora	lychnidiflora	k1gFnSc1	lychnidiflora
<g/>
,	,	kIx,	,
Quiabentia	Quiabentia	k1gFnSc1	Quiabentia
verticillata	verticille	k1gNnPc1	verticille
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
stromovitý	stromovitý	k2eAgInSc4d1	stromovitý
vzrůst	vzrůst	k1gInSc4	vzrůst
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
výšky	výška	k1gFnSc2	výška
až	až	k6eAd1	až
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgNnSc1d1	drobné
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
opadavé	opadavý	k2eAgInPc1d1	opadavý
listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
u	u	k7c2	u
podčeledi	podčeleď	k1gFnSc2	podčeleď
Opuntioideae	Opuntioidea	k1gFnSc2	Opuntioidea
<g/>
.	.	kIx.	.
</s>
<s>
Specifickým	specifický	k2eAgInSc7d1	specifický
orgánem	orgán	k1gInSc7	orgán
kaktusů	kaktus	k1gInPc2	kaktus
jsou	být	k5eAaImIp3nP	být
areoly	areola	k1gFnPc1	areola
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
zkrácené	zkrácený	k2eAgFnSc2d1	zkrácená
postranní	postranní	k2eAgFnSc2d1	postranní
větévky	větévka	k1gFnSc2	větévka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
růstové	růstový	k2eAgNnSc4d1	růstové
pletivo	pletivo	k1gNnSc4	pletivo
i	i	k8xC	i
cévní	cévní	k2eAgNnSc4d1	cévní
zásobení	zásobení	k1gNnSc4	zásobení
a	a	k8xC	a
nesou	nést	k5eAaImIp3nP	nést
trny	trn	k1gInPc1	trn
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
trichomů	trichom	k1gInPc2	trichom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nemnohých	mnohý	k2eNgInPc2d1	nemnohý
kaktusů	kaktus	k1gInPc2	kaktus
trny	trn	k1gInPc4	trn
v	v	k7c4	v
dospělosti	dospělost	k1gFnPc4	dospělost
chybějí	chybět	k5eAaImIp3nP	chybět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
lofofora	lofofora	k1gFnSc1	lofofora
<g/>
,	,	kIx,	,
Ariocarpus	Ariocarpus	k1gInSc1	Ariocarpus
<g/>
,	,	kIx,	,
Blossfeldia	Blossfeldium	k1gNnPc1	Blossfeldium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Epifytické	epifytický	k2eAgInPc1d1	epifytický
kaktusy	kaktus	k1gInPc1	kaktus
mají	mít	k5eAaImIp3nP	mít
buď	buď	k8xC	buď
tenké	tenký	k2eAgInPc1d1	tenký
stonky	stonek	k1gInPc1	stonek
s	s	k7c7	s
okrouhlým	okrouhlý	k2eAgInSc7d1	okrouhlý
průřezem	průřez	k1gInSc7	průřez
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rhipsalis	Rhipsalis	k1gFnSc2	Rhipsalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zploštělé	zploštělý	k2eAgInPc4d1	zploštělý
stonky	stonek	k1gInPc4	stonek
(	(	kIx(	(
<g/>
fylokládia	fylokládium	k1gNnSc2	fylokládium
<g/>
)	)	kIx)	)
připomínající	připomínající	k2eAgInPc1d1	připomínající
dužnaté	dužnatý	k2eAgInPc1d1	dužnatý
listy	list	k1gInPc1	list
(	(	kIx(	(
<g/>
Epiphyllum	Epiphyllum	k1gInSc1	Epiphyllum
<g/>
,	,	kIx,	,
Disocactus	Disocactus	k1gInSc1	Disocactus
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Květy	květ	k1gInPc1	květ
kaktusů	kaktus	k1gInPc2	kaktus
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pohledné	pohledný	k2eAgFnPc1d1	pohledná
a	a	k8xC	a
nápadné	nápadný	k2eAgFnPc1d1	nápadná
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
nebo	nebo	k8xC	nebo
řidčeji	řídce	k6eAd2	řídce
po	po	k7c6	po
několika	několik	k4yIc6	několik
na	na	k7c4	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
areol	areol	k1gInSc1	areol
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
vrcholové	vrcholový	k2eAgNnSc1d1	vrcholové
(	(	kIx(	(
<g/>
Pterocactus	Pterocactus	k1gInSc1	Pterocactus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
v	v	k7c6	v
latovitých	latovitý	k2eAgInPc6d1	latovitý
či	či	k8xC	či
vrcholíkovitých	vrcholíkovitý	k2eAgNnPc6d1	vrcholíkovitý
květenstvích	květenství	k1gNnPc6	květenství
(	(	kIx(	(
<g/>
Pereskia	Pereskia	k1gFnSc1	Pereskia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
a	a	k8xC	a
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
češuli	češule	k1gFnSc4	češule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
holá	holý	k2eAgFnSc1d1	holá
nebo	nebo	k8xC	nebo
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
listenovitými	listenovitý	k2eAgFnPc7d1	listenovitý
šupinami	šupina	k1gFnPc7	šupina
<g/>
,	,	kIx,	,
areolami	areola	k1gFnPc7	areola
<g/>
,	,	kIx,	,
trichomy	trichom	k1gInPc7	trichom
nebo	nebo	k8xC	nebo
i	i	k9	i
ostny	osten	k1gInPc4	osten
<g/>
.	.	kIx.	.
</s>
<s>
Okvětí	okvětí	k1gNnSc1	okvětí
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
lístků	lístek	k1gInPc2	lístek
postupně	postupně	k6eAd1	postupně
přecházejících	přecházející	k2eAgMnPc2d1	přecházející
od	od	k7c2	od
kališních	kališní	k2eAgInPc2d1	kališní
ke	k	k7c3	k
korunním	korunní	k2eAgFnPc3d1	korunní
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c4	na
řídké	řídký	k2eAgFnPc4d1	řídká
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
Pereskia	Pereskium	k1gNnSc2	Pereskium
<g/>
)	)	kIx)	)
spodní	spodní	k2eAgFnSc2d1	spodní
<g/>
,	,	kIx,	,
srostlý	srostlý	k2eAgMnSc1d1	srostlý
ze	z	k7c2	z
3	[number]	k4	3
až	až	k9	až
20	[number]	k4	20
plodolistů	plodolist	k1gInPc2	plodolist
a	a	k8xC	a
obsahující	obsahující	k2eAgFnSc4d1	obsahující
jedinou	jediný	k2eAgFnSc4d1	jediná
komůrku	komůrka	k1gFnSc4	komůrka
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
vajíčky	vajíčko	k1gNnPc7	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Čnělka	čnělka	k1gFnSc1	čnělka
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
bliznových	bliznův	k2eAgInPc2d1	bliznův
laloků	lalok	k1gInPc2	lalok
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
počtu	počet	k1gInSc2	počet
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
dužnatá	dužnatý	k2eAgFnSc1d1	dužnatá
nebo	nebo	k8xC	nebo
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
za	za	k7c2	za
zralosti	zralost	k1gFnSc2	zralost
nepukavá	pukavý	k2eNgFnSc1d1	nepukavá
nebo	nebo	k8xC	nebo
pukající	pukající	k2eAgFnSc1d1	pukající
bobule	bobule	k1gFnSc1	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
lysé	lysý	k2eAgFnPc1d1	Lysá
<g/>
,	,	kIx,	,
šupinaté	šupinatý	k2eAgFnPc1d1	šupinatá
<g/>
,	,	kIx,	,
chlupaté	chlupatý	k2eAgFnPc1d1	chlupatá
nebo	nebo	k8xC	nebo
ostnité	ostnitý	k2eAgFnPc1d1	ostnitá
<g/>
.	.	kIx.	.
</s>
<s>
Semen	semeno	k1gNnPc2	semeno
bývá	bývat	k5eAaImIp3nS	bývat
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
zástupců	zástupce	k1gMnPc2	zástupce
mohou	moct	k5eAaImIp3nP	moct
nést	nést	k5eAaImF	nést
míšek	míšek	k1gInSc4	míšek
nebo	nebo	k8xC	nebo
strofiolu	strofiola	k1gFnSc4	strofiola
<g/>
.	.	kIx.	.
</s>
<s>
Osemení	osemení	k1gNnSc1	osemení
je	být	k5eAaImIp3nS	být
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
skulpturováno	skulpturován	k2eAgNnSc1d1	skulpturován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rostliny	rostlina	k1gFnPc1	rostlina
podobné	podobný	k2eAgFnPc1d1	podobná
kaktusům	kaktus	k1gInPc3	kaktus
==	==	k?	==
</s>
</p>
<p>
<s>
Kaktusům	kaktus	k1gInPc3	kaktus
se	se	k3xPyFc4	se
poněkud	poněkud	k6eAd1	poněkud
podobají	podobat	k5eAaImIp3nP	podobat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
jiné	jiný	k2eAgFnPc1d1	jiná
sukulentní	sukulentní	k2eAgFnPc1d1	sukulentní
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
vesměs	vesměs	k6eAd1	vesměs
nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
vzhled	vzhled	k1gInSc1	vzhled
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
se	se	k3xPyFc4	se
obdobným	obdobný	k2eAgFnPc3d1	obdobná
ekologickým	ekologický	k2eAgFnPc3d1	ekologická
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Kaktusům	kaktus	k1gInPc3	kaktus
asi	asi	k9	asi
nejpodobnější	podobný	k2eAgInPc1d3	nejpodobnější
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
sukulentní	sukulentní	k2eAgInPc1d1	sukulentní
pryšce	pryšec	k1gInPc1	pryšec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
žebernatých	žebernatý	k2eAgInPc2d1	žebernatý
<g/>
,	,	kIx,	,
sukulentních	sukulentní	k2eAgInPc2d1	sukulentní
<g/>
,	,	kIx,	,
trnitých	trnitý	k2eAgInPc2d1	trnitý
stonků	stonek	k1gInPc2	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pryšce	pryšec	k1gInPc1	pryšec
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
vzrůst	vzrůst	k1gInSc4	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
rozeznat	rozeznat	k5eAaPmF	rozeznat
podle	podle	k7c2	podle
zcela	zcela	k6eAd1	zcela
odlišných	odlišný	k2eAgInPc2d1	odlišný
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
v	v	k7c6	v
nekvetoucím	kvetoucí	k2eNgInSc6d1	nekvetoucí
stavu	stav	k1gInSc6	stav
pak	pak	k9	pak
např.	např.	kA	např.
podle	podle	k7c2	podle
párových	párový	k2eAgInPc2d1	párový
trnů	trn	k1gInPc2	trn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
trnů	trn	k1gInPc2	trn
kaktusů	kaktus	k1gInPc2	kaktus
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
přeměnou	přeměna	k1gFnSc7	přeměna
párových	párový	k2eAgInPc2d1	párový
palistů	palist	k1gInPc2	palist
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
sukulentní	sukulentní	k2eAgInPc1d1	sukulentní
pryšce	pryšec	k1gInPc1	pryšec
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kaktusy	kaktus	k1gInPc1	kaktus
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
čeledí	čeleď	k1gFnPc2	čeleď
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
kaktusům	kaktus	k1gInPc3	kaktus
poněkud	poněkud	k6eAd1	poněkud
podobat	podobat	k5eAaImF	podobat
např.	např.	kA	např.
některé	některý	k3yIgFnPc4	některý
stapélie	stapélie	k1gFnPc4	stapélie
a	a	k8xC	a
jim	on	k3xPp3gMnPc3	on
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
sukulenty	sukulent	k1gInPc4	sukulent
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
toješťovité	toješťovitý	k2eAgFnSc2d1	toješťovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
kaktusovité	kaktusovitý	k2eAgNnSc1d1	kaktusovité
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dohromady	dohromady	k6eAd1	dohromady
asi	asi	k9	asi
1900	[number]	k4	1900
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
127	[number]	k4	127
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
rody	rod	k1gInPc1	rod
jsou	být	k5eAaImIp3nP	být
opuncie	opuncie	k1gFnPc4	opuncie
(	(	kIx(	(
<g/>
Opuntia	Opuntius	k1gMnSc2	Opuntius
<g/>
,	,	kIx,	,
205	[number]	k4	205
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mamilárie	mamilárie	k1gFnSc1	mamilárie
(	(	kIx(	(
<g/>
Mammillaria	Mammillarium	k1gNnPc4	Mammillarium
<g/>
,	,	kIx,	,
166	[number]	k4	166
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Echinopsis	Echinopsis	k1gFnSc1	Echinopsis
(	(	kIx(	(
<g/>
78	[number]	k4	78
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
Až	až	k9	až
na	na	k7c4	na
jedinou	jediný	k2eAgFnSc4d1	jediná
výjimku	výjimka	k1gFnSc4	výjimka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Galapágy	Galapágy	k1gFnPc1	Galapágy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdále	daleko	k6eAd3	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
(	(	kIx(	(
<g/>
až	až	k9	až
po	po	k7c4	po
56	[number]	k4	56
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
)	)	kIx)	)
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
opuncie	opuncie	k1gFnSc1	opuncie
Opuntia	Opuntia	k1gFnSc1	Opuntia
fragilis	fragilis	k1gFnSc1	fragilis
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
až	až	k9	až
po	po	k7c4	po
kanadské	kanadský	k2eAgFnPc4d1	kanadská
provincie	provincie	k1gFnPc4	provincie
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
a	a	k8xC	a
Alberta	Alberta	k1gFnSc1	Alberta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
druh	druh	k1gInSc1	druh
Opuntia	Opuntius	k1gMnSc2	Opuntius
humifusa	humifus	k1gMnSc2	humifus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
50	[number]	k4	50
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
v	v	k7c6	v
argentinské	argentinský	k2eAgFnSc6d1	Argentinská
Patagonii	Patagonie	k1gFnSc6	Patagonie
rody	rod	k1gInPc1	rod
Maihuenia	Maihuenium	k1gNnSc2	Maihuenium
<g/>
,	,	kIx,	,
Maihueniopsis	Maihueniopsis	k1gInSc1	Maihueniopsis
a	a	k8xC	a
Pterocactus	Pterocactus	k1gInSc1	Pterocactus
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
centrum	centrum	k1gNnSc1	centrum
druhové	druhový	k2eAgFnSc2d1	druhová
diverzity	diverzita	k1gFnSc2	diverzita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
oblasti	oblast	k1gFnSc6	oblast
jihu	jih	k1gInSc2	jih
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
sloupcovité	sloupcovitý	k2eAgInPc4d1	sloupcovitý
kaktusy	kaktus	k1gInPc4	kaktus
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
rody	rod	k1gInPc7	rod
Pachycereus	Pachycereus	k1gMnSc1	Pachycereus
<g/>
,	,	kIx,	,
Stenocereus	Stenocereus	k1gMnSc1	Stenocereus
<g/>
,	,	kIx,	,
Carnegiea	Carnegiea	k1gMnSc1	Carnegiea
<g/>
,	,	kIx,	,
Myrtillocactus	Myrtillocactus	k1gMnSc1	Myrtillocactus
<g/>
,	,	kIx,	,
Cephalocereus	Cephalocereus	k1gMnSc1	Cephalocereus
<g/>
,	,	kIx,	,
Neobuxbaumia	Neobuxbaumia	k1gFnSc1	Neobuxbaumia
a	a	k8xC	a
Polaskia	Polaskia	k1gFnSc1	Polaskia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nízkých	nízký	k2eAgInPc2d1	nízký
kaktusů	kaktus	k1gInPc2	kaktus
zde	zde	k6eAd1	zde
rostou	růst	k5eAaImIp3nP	růst
zejména	zejména	k9	zejména
mamilárie	mamilárie	k1gFnPc1	mamilárie
(	(	kIx(	(
<g/>
Mammillaria	Mammillarium	k1gNnPc1	Mammillarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Echinocereus	Echinocereus	k1gMnSc1	Echinocereus
a	a	k8xC	a
Coryphantha	Coryphantha	k1gMnSc1	Coryphantha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
druhotné	druhotný	k2eAgNnSc1d1	druhotné
centrum	centrum	k1gNnSc1	centrum
diverzity	diverzita	k1gFnSc2	diverzita
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
And	Anda	k1gFnPc2	Anda
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Převažují	převažovat	k5eAaImIp3nP	převažovat
zde	zde	k6eAd1	zde
kulovité	kulovitý	k2eAgInPc1d1	kulovitý
kaktusy	kaktus	k1gInPc1	kaktus
zejména	zejména	k9	zejména
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Gymnocalycium	Gymnocalycium	k1gNnSc1	Gymnocalycium
<g/>
,	,	kIx,	,
Echinopsis	Echinopsis	k1gInSc1	Echinopsis
<g/>
,	,	kIx,	,
Parodia	Parodium	k1gNnPc1	Parodium
a	a	k8xC	a
rebucie	rebucie	k1gFnPc1	rebucie
(	(	kIx(	(
<g/>
Rebutia	Rebutia	k1gFnSc1	Rebutia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc4d1	další
centra	centrum	k1gNnPc4	centrum
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Brazílii	Brazílie	k1gFnSc6	Brazílie
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Pereskia	Pereskium	k1gNnSc2	Pereskium
<g/>
,	,	kIx,	,
Tacinga	Tacinga	k1gFnSc1	Tacinga
<g/>
,	,	kIx,	,
Facheiroa	Facheiroa	k1gFnSc1	Facheiroa
<g/>
,	,	kIx,	,
Stephanocereus	Stephanocereus	k1gInSc1	Stephanocereus
<g/>
,	,	kIx,	,
Espostoopsis	Espostoopsis	k1gInSc1	Espostoopsis
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
středozápadní	středozápadní	k2eAgFnSc2d1	středozápadní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
,	,	kIx,	,
Uruguaye	Uruguay	k1gFnSc2	Uruguay
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Argentiny	Argentina	k1gFnSc2	Argentina
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Frailea	Fraileum	k1gNnSc2	Fraileum
<g/>
,	,	kIx,	,
Harrisia	Harrisium	k1gNnSc2	Harrisium
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Opuntia	Opuntium	k1gNnPc1	Opuntium
<g/>
,	,	kIx,	,
Cereus	Cereus	k1gInSc1	Cereus
<g/>
,	,	kIx,	,
Gymnocalycium	Gymnocalycium	k1gNnSc1	Gymnocalycium
<g/>
,	,	kIx,	,
Parodia	Parodium	k1gNnPc1	Parodium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
a	a	k8xC	a
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
Chile	Chile	k1gNnSc6	Chile
(	(	kIx(	(
<g/>
mnoho	mnoho	k4c4	mnoho
endemických	endemický	k2eAgInPc2d1	endemický
druhů	druh	k1gInPc2	druh
rodů	rod	k1gInPc2	rod
Copiapoa	Copiapo	k1gInSc2	Copiapo
a	a	k8xC	a
Eriosyce	Eriosyec	k1gInSc2	Eriosyec
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Karibských	karibský	k2eAgInPc6d1	karibský
ostrovech	ostrov	k1gInPc6	ostrov
(	(	kIx(	(
<g/>
endemické	endemický	k2eAgInPc1d1	endemický
rody	rod	k1gInPc1	rod
Consolea	Consoleum	k1gNnSc2	Consoleum
<g/>
,	,	kIx,	,
Leptocereus	Leptocereus	k1gMnSc1	Leptocereus
a	a	k8xC	a
Dendrocereus	Dendrocereus	k1gMnSc1	Dendrocereus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
monotypické	monotypický	k2eAgInPc1d1	monotypický
endemické	endemický	k2eAgInPc1d1	endemický
rody	rod	k1gInPc1	rod
(	(	kIx(	(
<g/>
Jasminocereus	Jasminocereus	k1gInSc1	Jasminocereus
a	a	k8xC	a
Brachycereus	Brachycereus	k1gInSc1	Brachycereus
<g/>
)	)	kIx)	)
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
Galapágách	Galapágy	k1gFnPc6	Galapágy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
druhem	druh	k1gInSc7	druh
kaktusu	kaktus	k1gInSc2	kaktus
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
mimo	mimo	k7c4	mimo
Ameriku	Amerika	k1gFnSc4	Amerika
je	být	k5eAaImIp3nS	být
Rhipsalis	Rhipsalis	k1gFnSc1	Rhipsalis
baccifera	baccifer	k1gMnSc2	baccifer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
značné	značný	k2eAgFnPc4d1	značná
části	část	k1gFnPc4	část
tropické	tropický	k2eAgFnSc2d1	tropická
Ameriky	Amerika	k1gFnSc2	Amerika
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
i	i	k9	i
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
ostrovech	ostrov	k1gInPc6	ostrov
západního	západní	k2eAgInSc2d1	západní
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
široké	široký	k2eAgFnSc6d1	široká
škále	škála	k1gFnSc6	škála
stanovišť	stanoviště	k1gNnPc2	stanoviště
od	od	k7c2	od
vyprahlých	vyprahlý	k2eAgFnPc2d1	vyprahlá
pouští	poušť	k1gFnPc2	poušť
až	až	k9	až
po	po	k7c4	po
tropické	tropický	k2eAgInPc4d1	tropický
deštné	deštný	k2eAgInPc4d1	deštný
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
soustředěn	soustředěn	k2eAgMnSc1d1	soustředěn
do	do	k7c2	do
suchých	suchý	k2eAgFnPc2d1	suchá
<g/>
,	,	kIx,	,
polopouštních	polopouštní	k2eAgFnPc2d1	polopouštní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihoamerických	jihoamerický	k2eAgFnPc6d1	jihoamerická
Andách	Anda	k1gFnPc6	Anda
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
do	do	k7c2	do
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
okolo	okolo	k7c2	okolo
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
stenoendemické	stenoendemický	k2eAgInPc1d1	stenoendemický
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
svým	svůj	k3xOyFgInSc7	svůj
výskytem	výskyt	k1gInSc7	výskyt
omezeny	omezit	k5eAaPmNgFnP	omezit
na	na	k7c4	na
izolované	izolovaný	k2eAgInPc4d1	izolovaný
výchozy	výchoz	k1gInPc4	výchoz
vápenců	vápenec	k1gInPc2	vápenec
<g/>
,	,	kIx,	,
sádrovců	sádrovec	k1gInPc2	sádrovec
nebo	nebo	k8xC	nebo
hadců	hadec	k1gInPc2	hadec
<g/>
.	.	kIx.	.
<g/>
Kaktusy	kaktus	k1gInPc1	kaktus
se	se	k3xPyFc4	se
rozličnými	rozličný	k2eAgFnPc7d1	rozličná
způsoby	způsob	k1gInPc7	způsob
přizpůsobily	přizpůsobit	k5eAaPmAgFnP	přizpůsobit
růstu	růst	k1gInSc2	růst
a	a	k8xC	a
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
přežití	přežití	k1gNnSc3	přežití
v	v	k7c6	v
suchém	suchý	k2eAgNnSc6d1	suché
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
zcela	zcela	k6eAd1	zcela
ztratila	ztratit	k5eAaPmAgFnS	ztratit
zelené	zelený	k2eAgInPc4d1	zelený
listy	list	k1gInPc4	list
a	a	k8xC	a
fotosyntetizuje	fotosyntetizovat	k5eAaBmIp3nS	fotosyntetizovat
stonkem	stonek	k1gInSc7	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Stonky	stonka	k1gFnPc1	stonka
jsou	být	k5eAaImIp3nP	být
sukulentní	sukulentní	k2eAgFnPc1d1	sukulentní
<g/>
,	,	kIx,	,
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
chráněné	chráněný	k2eAgNnSc1d1	chráněné
tlustou	tlustý	k2eAgFnSc7d1	tlustá
pokožkou	pokožka	k1gFnSc7	pokožka
se	s	k7c7	s
zanořenými	zanořený	k2eAgInPc7d1	zanořený
průduchy	průduch	k1gInPc7	průduch
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pletiva	pletivo	k1gNnPc1	pletivo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
shromažďována	shromažďován	k2eAgFnSc1d1	shromažďována
vláha	vláha	k1gFnSc1	vláha
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
chráněny	chránit	k5eAaImNgInP	chránit
ostny	osten	k1gInPc7	osten
nebo	nebo	k8xC	nebo
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
vlasovitými	vlasovitý	k2eAgInPc7d1	vlasovitý
chlupy	chlup	k1gInPc7	chlup
<g/>
.	.	kIx.	.
</s>
<s>
CAM	CAM	kA	CAM
metabolismus	metabolismus	k1gInSc1	metabolismus
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mít	mít	k5eAaImF	mít
průduchy	průduch	k1gInPc4	průduch
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
v	v	k7c6	v
denním	denní	k2eAgInSc6d1	denní
žáru	žár	k1gInSc6	žár
zavřené	zavřený	k2eAgFnPc1d1	zavřená
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
omezují	omezovat	k5eAaImIp3nP	omezovat
ztráty	ztráta	k1gFnSc2	ztráta
vody	voda	k1gFnSc2	voda
odparem	odpar	k1gInSc7	odpar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
průduchů	průduch	k1gInPc2	průduch
a	a	k8xC	a
výměně	výměna	k1gFnSc3	výměna
plynů	plyn	k1gInPc2	plyn
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Kořenový	kořenový	k2eAgInSc1d1	kořenový
systém	systém	k1gInSc1	systém
bývá	bývat	k5eAaImIp3nS	bývat
mělký	mělký	k2eAgInSc1d1	mělký
a	a	k8xC	a
rozprostřený	rozprostřený	k2eAgInSc1d1	rozprostřený
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
zástupců	zástupce	k1gMnPc2	zástupce
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
i	i	k8xC	i
hluboký	hluboký	k2eAgInSc1d1	hluboký
kůlovitý	kůlovitý	k2eAgInSc1d1	kůlovitý
hlavní	hlavní	k2eAgInSc1d1	hlavní
kořen	kořen	k1gInSc1	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologické	ekologický	k2eAgFnPc1d1	ekologická
interakce	interakce	k1gFnPc1	interakce
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
kaktusů	kaktus	k1gInPc2	kaktus
je	být	k5eAaImIp3nS	být
cizosprašná	cizosprašný	k2eAgFnSc1d1	cizosprašná
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
tedy	tedy	k9	tedy
schopny	schopen	k2eAgFnPc4d1	schopna
samoopylení	samoopylení	k1gNnPc4	samoopylení
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kaktusů	kaktus	k1gInPc2	kaktus
je	být	k5eAaImIp3nS	být
opylována	opylován	k2eAgFnSc1d1	opylována
včelami	včela	k1gFnPc7	včela
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
kaktusů	kaktus	k1gInPc2	kaktus
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c4	za
základní	základní	k2eAgInSc4d1	základní
a	a	k8xC	a
původní	původní	k2eAgInSc4d1	původní
způsob	způsob	k1gInSc4	způsob
opylování	opylování	k1gNnSc2	opylování
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
způsob	způsob	k1gInSc4	způsob
opylení	opylení	k1gNnSc2	opylení
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
rebucií	rebucie	k1gFnPc2	rebucie
má	mít	k5eAaImIp3nS	mít
květy	květ	k1gInPc4	květ
s	s	k7c7	s
úzkou	úzký	k2eAgFnSc7d1	úzká
květní	květní	k2eAgFnSc7d1	květní
trubkou	trubka	k1gFnSc7	trubka
a	a	k8xC	a
kolovitou	kolovitý	k2eAgFnSc7d1	kolovitá
korunou	koruna	k1gFnSc7	koruna
s	s	k7c7	s
odstávajícími	odstávající	k2eAgInPc7d1	odstávající
korunními	korunní	k2eAgInPc7d1	korunní
plátky	plátek	k1gInPc7	plátek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
opylovány	opylován	k2eAgMnPc4d1	opylován
motýly	motýl	k1gMnPc4	motýl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
kaktusů	kaktus	k1gInPc2	kaktus
se	se	k3xPyFc4	se
nezávisle	závisle	k6eNd1	závisle
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
též	též	k9	též
opylování	opylování	k1gNnSc1	opylování
kolibříky	kolibřík	k1gMnPc4	kolibřík
s	s	k7c7	s
charakteristickými	charakteristický	k2eAgInPc7d1	charakteristický
průvodními	průvodní	k2eAgInPc7d1	průvodní
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
nevonné	vonný	k2eNgFnPc1d1	nevonná
<g/>
,	,	kIx,	,
trubkovité	trubkovitý	k2eAgFnPc1d1	trubkovitá
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
dvoustranně	dvoustranně	k6eAd1	dvoustranně
souměrné	souměrný	k2eAgInPc4d1	souměrný
květy	květ	k1gInPc4	květ
vytvářející	vytvářející	k2eAgNnSc4d1	vytvářející
množství	množství	k1gNnSc4	množství
nektaru	nektar	k1gInSc2	nektar
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
Cleistocactus	Cleistocactus	k1gInSc1	Cleistocactus
<g/>
,	,	kIx,	,
Disocactus	Disocactus	k1gInSc1	Disocactus
<g/>
,	,	kIx,	,
Schlumbergera	Schlumbergera	k1gFnSc1	Schlumbergera
<g/>
,	,	kIx,	,
Pereskia	Pereskia	k1gFnSc1	Pereskia
stenantha	stenantha	k1gFnSc1	stenantha
<g/>
,	,	kIx,	,
Opuntia	Opuntia	k1gFnSc1	Opuntia
cochenillifera	cochenillifera	k1gFnSc1	cochenillifera
nebo	nebo	k8xC	nebo
mamilárie	mamilárie	k1gFnSc1	mamilárie
Mammillaria	Mammillarium	k1gNnSc2	Mammillarium
poselgeri	poselger	k1gFnSc2	poselger
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
podčeledi	podčeleď	k1gFnSc2	podčeleď
Cactoideae	Cactoidea	k1gFnSc2	Cactoidea
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
specializována	specializovat	k5eAaBmNgFnS	specializovat
na	na	k7c4	na
opylování	opylování	k1gNnSc4	opylování
lišaji	lišaj	k1gMnPc1	lišaj
(	(	kIx(	(
<g/>
Epiphyllum	Epiphyllum	k1gInSc1	Epiphyllum
phyllanthus	phyllanthus	k1gInSc1	phyllanthus
<g/>
,	,	kIx,	,
Selenicereus	Selenicereus	k1gInSc1	Selenicereus
wittii	wittie	k1gFnSc4	wittie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
netopýry	netopýr	k1gMnPc4	netopýr
(	(	kIx(	(
<g/>
sloupcovité	sloupcovitý	k2eAgInPc1d1	sloupcovitý
kaktusy	kaktus	k1gInPc1	kaktus
Carnegiea	Carnegieum	k1gNnSc2	Carnegieum
<g/>
,	,	kIx,	,
Pilosocereus	Pilosocereus	k1gMnSc1	Pilosocereus
<g/>
,	,	kIx,	,
epifytní	epifytní	k2eAgMnSc1d1	epifytní
Weberocereus	Weberocereus	k1gMnSc1	Weberocereus
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
květech	květ	k1gInPc6	květ
kaktusů	kaktus	k1gInPc2	kaktus
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
nacházeny	nacházen	k2eAgInPc1d1	nacházen
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
brouků	brouk	k1gMnPc2	brouk
a	a	k8xC	a
larvy	larva	k1gFnSc2	larva
řady	řada	k1gFnSc2	řada
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
<g/>
,	,	kIx,	,
k	k	k7c3	k
opylování	opylování	k1gNnSc3	opylování
však	však	k9	však
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
nepřispívají	přispívat	k5eNaImIp3nP	přispívat
<g/>
.	.	kIx.	.
<g/>
Dužnaté	dužnatý	k2eAgFnPc1d1	dužnatá
a	a	k8xC	a
sladce	sladko	k6eAd1	sladko
chutnající	chutnající	k2eAgInPc1d1	chutnající
plody	plod	k1gInPc1	plod
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
vyhledávány	vyhledáván	k2eAgFnPc1d1	vyhledávána
a	a	k8xC	a
šířeny	šířen	k2eAgFnPc1d1	šířena
zejména	zejména	k9	zejména
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
drobných	drobný	k2eAgInPc2d1	drobný
kulovitých	kulovitý	k2eAgInPc2d1	kulovitý
druhů	druh	k1gInPc2	druh
se	s	k7c7	s
suchými	suchý	k2eAgInPc7d1	suchý
plody	plod	k1gInPc7	plod
se	se	k3xPyFc4	se
o	o	k7c6	o
šíření	šíření	k1gNnSc6	šíření
semen	semeno	k1gNnPc2	semeno
starají	starat	k5eAaImIp3nP	starat
mravenci	mravenec	k1gMnPc1	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Cereus	Cereus	k1gInSc1	Cereus
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
kombinaci	kombinace	k1gFnSc4	kombinace
obou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
šíření	šíření	k1gNnPc2	šíření
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
o	o	k7c4	o
plody	plod	k1gInPc4	plod
nepostarají	postarat	k5eNaPmIp3nP	postarat
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
uschnou	uschnout	k5eAaPmIp3nP	uschnout
na	na	k7c6	na
rostlině	rostlina	k1gFnSc6	rostlina
a	a	k8xC	a
po	po	k7c6	po
puknutí	puknutí	k1gNnSc6	puknutí
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
vypadávají	vypadávat	k5eAaImIp3nP	vypadávat
drobná	drobný	k2eAgNnPc1d1	drobné
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
mravenci	mravenec	k1gMnPc1	mravenec
pro	pro	k7c4	pro
dužnaté	dužnatý	k2eAgNnSc4d1	dužnaté
poutko	poutko	k1gNnSc4	poutko
(	(	kIx(	(
<g/>
funiculus	funiculus	k1gInSc1	funiculus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
některých	některý	k3yIgFnPc2	některý
opuncií	opuncie	k1gFnPc2	opuncie
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
ostnité	ostnitý	k2eAgFnPc1d1	ostnitá
a	a	k8xC	a
přichytávají	přichytávat	k5eAaImIp3nP	přichytávat
se	se	k3xPyFc4	se
na	na	k7c4	na
srsti	srst	k1gFnPc4	srst
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
opuncií	opuncie	k1gFnPc2	opuncie
<g/>
,	,	kIx,	,
mamilárií	mamilárie	k1gFnSc7	mamilárie
aj.	aj.	kA	aj.
rodů	rod	k1gInPc2	rod
se	se	k3xPyFc4	se
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
šíří	šířit	k5eAaImIp3nS	šířit
vegetativně	vegetativně	k6eAd1	vegetativně
pomocí	pomocí	k7c2	pomocí
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
odlamujících	odlamující	k2eAgInPc2d1	odlamující
<g/>
,	,	kIx,	,
ostnitých	ostnitý	k2eAgInPc2d1	ostnitý
segmentů	segment	k1gInPc2	segment
stonku	stonek	k1gInSc2	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
Pereskia	Pereskia	k1gFnSc1	Pereskia
bahiensis	bahiensis	k1gFnSc1	bahiensis
<g/>
,	,	kIx,	,
Acanthocereus	Acanthocereus	k1gInSc1	Acanthocereus
brasiliensis	brasiliensis	k1gFnSc2	brasiliensis
<g/>
)	)	kIx)	)
po	po	k7c6	po
dozrání	dozrání	k1gNnSc6	dozrání
opadávají	opadávat	k5eAaImIp3nP	opadávat
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
puknou	puknout	k5eAaPmIp3nP	puknout
a	a	k8xC	a
vydávají	vydávat	k5eAaPmIp3nP	vydávat
sladkou	sladký	k2eAgFnSc4d1	sladká
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
je	on	k3xPp3gInPc4	on
různé	různý	k2eAgInPc4d1	různý
větší	veliký	k2eAgInPc4d2	veliký
druhy	druh	k1gInPc4	druh
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnSc2	semeno
Pterocactus	Pterocactus	k1gMnSc1	Pterocactus
a	a	k8xC	a
Espostoa	Espostoa	k1gMnSc1	Espostoa
blossfeldiorum	blossfeldiorum	k1gNnSc4	blossfeldiorum
a	a	k8xC	a
celé	celý	k2eAgInPc4d1	celý
<g/>
,	,	kIx,	,
nafouklé	nafouklý	k2eAgInPc4d1	nafouklý
plody	plod	k1gInPc4	plod
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
Neoporteria	Neoporterium	k1gNnSc2	Neoporterium
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Drobounká	drobounký	k2eAgNnPc1d1	drobounké
semena	semeno	k1gNnPc1	semeno
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
kulovitých	kulovitý	k2eAgInPc2d1	kulovitý
kaktusů	kaktus	k1gInPc2	kaktus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Parodia	Parodium	k1gNnSc2	Parodium
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
dokonce	dokonce	k9	dokonce
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
strofiola	strofiola	k1gFnSc1	strofiola
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
plovák	plovák	k1gInSc1	plovák
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Selenicereus	Selenicereus	k1gMnSc1	Selenicereus
wittii	wittie	k1gFnSc4	wittie
<g/>
,	,	kIx,	,
epifytní	epifytní	k2eAgInSc4d1	epifytní
druh	druh	k1gInSc4	druh
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
v	v	k7c6	v
periodicky	periodicky	k6eAd1	periodicky
zaplavovaných	zaplavovaný	k2eAgInPc6d1	zaplavovaný
lesích	les	k1gInPc6	les
Amazonie	Amazonie	k1gFnSc2	Amazonie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
</s>
</p>
<p>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
zpravidla	zpravidla	k6eAd1	zpravidla
tvoří	tvořit	k5eAaImIp3nP	tvořit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
uchycení	uchycení	k1gNnSc2	uchycení
semenáčků	semenáček	k1gInPc2	semenáček
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnSc2	semeno
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
různých	různý	k2eAgMnPc2d1	různý
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
netopýři	netopýr	k1gMnPc1	netopýr
<g/>
,	,	kIx,	,
hlodavci	hlodavec	k1gMnPc1	hlodavec
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
<g/>
,	,	kIx,	,
krabi	krab	k1gMnPc1	krab
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
šířena	šířit	k5eAaImNgNnP	šířit
také	také	k9	také
větrem	vítr	k1gInSc7	vítr
a	a	k8xC	a
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
uchycení	uchycení	k1gNnSc3	uchycení
semenáčků	semenáček	k1gInPc2	semenáček
hurikány	hurikán	k1gInPc1	hurikán
<g/>
.	.	kIx.	.
<g/>
Kaktusy	kaktus	k1gInPc1	kaktus
představují	představovat	k5eAaImIp3nP	představovat
v	v	k7c6	v
suchých	suchý	k2eAgFnPc6d1	suchá
oblastech	oblast	k1gFnPc6	oblast
významný	významný	k2eAgInSc4d1	významný
zdroj	zdroj	k1gInSc4	zdroj
potravy	potrava	k1gFnSc2	potrava
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
různých	různý	k2eAgMnPc2d1	různý
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jim	on	k3xPp3gMnPc3	on
i	i	k9	i
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dužnaté	dužnatý	k2eAgInPc1d1	dužnatý
plody	plod	k1gInPc1	plod
Carnegiea	Carnegieus	k1gMnSc2	Carnegieus
gigantea	giganteus	k1gMnSc2	giganteus
živí	živit	k5eAaImIp3nS	živit
přinejmenším	přinejmenším	k6eAd1	přinejmenším
20	[number]	k4	20
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
60	[number]	k4	60
různých	různý	k2eAgInPc2d1	různý
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tyraminové	tyraminový	k2eAgInPc4d1	tyraminový
alkaloidy	alkaloid	k1gInPc4	alkaloid
a	a	k8xC	a
fenethyl-aminy	fenethylmin	k1gInPc4	fenethyl-amin
<g/>
.	.	kIx.	.
</s>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
rodu	rod	k1gInSc2	rod
ježunka	ježunka	k1gFnSc1	ježunka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
halucinogenního	halucinogenní	k2eAgInSc2d1	halucinogenní
meskalinu	meskalin	k1gInSc2	meskalin
a	a	k8xC	a
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
zástupců	zástupce	k1gMnPc2	zástupce
Pachycereeae	Pachycereea	k1gInSc2	Pachycereea
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
též	též	k9	též
tetrahydroisochinolinové	tetrahydroisochinolinový	k2eAgInPc1d1	tetrahydroisochinolinový
alkaloidy	alkaloid	k1gInPc1	alkaloid
(	(	kIx(	(
<g/>
anhalidin	anhalidin	k1gInSc1	anhalidin
<g/>
,	,	kIx,	,
pellotin	pellotina	k1gFnPc2	pellotina
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
také	také	k9	také
obsah	obsah	k1gInSc1	obsah
triterpenů	triterpen	k1gInPc2	triterpen
<g/>
,	,	kIx,	,
glykosidů	glykosid	k1gInPc2	glykosid
a	a	k8xC	a
steroidních	steroidní	k2eAgFnPc2d1	steroidní
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Fenolické	fenolický	k2eAgFnPc1d1	fenolická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
jen	jen	k9	jen
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
a	a	k8xC	a
třísloviny	tříslovina	k1gFnSc2	tříslovina
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jasné	jasný	k2eAgNnSc1d1	jasné
zbarvení	zbarvení	k1gNnSc1	zbarvení
květů	květ	k1gInPc2	květ
i	i	k8xC	i
plodů	plod	k1gInPc2	plod
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
barviva	barvivo	k1gNnPc1	barvivo
betalainy	betalaina	k1gFnSc2	betalaina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
anthokyany	anthokyan	k1gInPc1	anthokyan
zcela	zcela	k6eAd1	zcela
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
klasických	klasický	k2eAgInPc6d1	klasický
taxonomických	taxonomický	k2eAgInPc6d1	taxonomický
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
Cronquist	Cronquist	k1gInSc1	Cronquist
<g/>
,	,	kIx,	,
Dahlgren	Dahlgrna	k1gFnPc2	Dahlgrna
<g/>
,	,	kIx,	,
Tachtadžjan	Tachtadžjana	k1gFnPc2	Tachtadžjana
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
čeleď	čeleď	k1gFnSc1	čeleď
Cactaceae	Cactacea	k1gFnSc2	Cactacea
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
Caryophyllales	Caryophyllalesa	k1gFnPc2	Caryophyllalesa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
analýz	analýza	k1gFnPc2	analýza
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
příbuznou	příbuzný	k2eAgFnSc7d1	příbuzná
skupinou	skupina	k1gFnSc7	skupina
čeleď	čeleď	k1gFnSc1	čeleď
Anacampserotaceae	Anacampserotaceae	k1gFnSc1	Anacampserotaceae
<g/>
,	,	kIx,	,
oddělená	oddělený	k2eAgFnSc1d1	oddělená
v	v	k7c6	v
systému	systém	k1gInSc6	systém
APG	APG	kA	APG
III	III	kA	III
od	od	k7c2	od
čeledi	čeleď	k1gFnSc2	čeleď
Portulacaceae	Portulacacea	k1gFnSc2	Portulacacea
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
taxonomii	taxonomie	k1gFnSc6	taxonomie
je	být	k5eAaImIp3nS	být
uznáváno	uznávat	k5eAaImNgNnS	uznávat
asi	asi	k9	asi
1900	[number]	k4	1900
druhů	druh	k1gInPc2	druh
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
dosud	dosud	k6eAd1	dosud
publikovaných	publikovaný	k2eAgInPc2d1	publikovaný
druhových	druhový	k2eAgInPc2d1	druhový
názvů	název	k1gInPc2	název
však	však	k9	však
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
12000	[number]	k4	12000
<g/>
.	.	kIx.	.
</s>
<s>
Enormní	enormní	k2eAgInSc1d1	enormní
počet	počet	k1gInSc1	počet
neplatných	platný	k2eNgInPc2d1	neplatný
názvů	název	k1gInPc2	název
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
pěstitelským	pěstitelský	k2eAgInSc7d1	pěstitelský
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
tyto	tento	k3xDgFnPc4	tento
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekordy	rekord	k1gInPc7	rekord
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
kaktusů	kaktus	k1gInPc2	kaktus
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
druhy	druh	k1gInPc4	druh
sloupcovitých	sloupcovitý	k2eAgInPc2d1	sloupcovitý
kaktusů	kaktus	k1gInPc2	kaktus
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
výšek	výška	k1gFnPc2	výška
až	až	k9	až
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druh	druh	k1gInSc4	druh
Carnegiea	Carnegie	k2eAgNnPc4d1	Carnegie
gigantea	giganteum	k1gNnPc4	giganteum
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Neobuxbaumia	Neobuxbaumia	k1gFnSc1	Neobuxbaumia
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
květy	květ	k1gInPc4	květ
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
Hylocereus	Hylocereus	k1gInSc4	Hylocereus
<g/>
.	.	kIx.	.
</s>
<s>
Dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc4	délka
až	až	k9	až
30	[number]	k4	30
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
obdobné	obdobný	k2eAgFnPc1d1	obdobná
šířky	šířka	k1gFnPc1	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgInSc7d3	nejmenší
druhem	druh	k1gInSc7	druh
kaktusu	kaktus	k1gInSc2	kaktus
je	být	k5eAaImIp3nS	být
Blossfeldia	Blossfeldium	k1gNnSc2	Blossfeldium
liliputana	liliputana	k1gFnSc1	liliputana
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
kaktus	kaktus	k1gInSc4	kaktus
původem	původ	k1gInSc7	původ
z	z	k7c2	z
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
And	Anda	k1gFnPc2	Anda
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnPc4	velikost
jen	jen	k9	jen
12	[number]	k4	12
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Nejodolnějšími	odolný	k2eAgInPc7d3	nejodolnější
kaktusy	kaktus	k1gInPc7	kaktus
vůči	vůči	k7c3	vůči
nízkým	nízký	k2eAgFnPc3d1	nízká
teplotám	teplota	k1gFnPc3	teplota
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc4	některý
opuncie	opuncie	k1gFnPc4	opuncie
<g/>
.	.	kIx.	.
</s>
<s>
Severoamerické	severoamerický	k2eAgInPc1d1	severoamerický
druhy	druh	k1gInPc1	druh
Opuntia	Opuntium	k1gNnSc2	Opuntium
fragilis	fragilis	k1gFnSc2	fragilis
a	a	k8xC	a
Opuntia	Opuntius	k1gMnSc2	Opuntius
humifusa	humifus	k1gMnSc2	humifus
snášejí	snášet	k5eAaImIp3nP	snášet
mráz	mráz	k1gInSc4	mráz
až	až	k9	až
do	do	k7c2	do
-35	-35	k4	-35
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
astrofytum	astrofytum	k1gNnSc1	astrofytum
(	(	kIx(	(
<g/>
Astrophytum	Astrophytum	k1gNnSc1	Astrophytum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hylocereus	hylocereus	k1gMnSc1	hylocereus
(	(	kIx(	(
<g/>
Hylocereus	Hylocereus	k1gMnSc1	Hylocereus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
isolatocereus	isolatocereus	k1gMnSc1	isolatocereus
(	(	kIx(	(
<g/>
Isolatocereus	Isolatocereus	k1gMnSc1	Isolatocereus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kleistokaktus	kleistokaktus	k1gMnSc1	kleistokaktus
(	(	kIx(	(
<g/>
Cleistocactus	Cleistocactus	k1gMnSc1	Cleistocactus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
opuncie	opuncie	k1gFnSc1	opuncie
(	(	kIx(	(
<g/>
Opuntia	Opuntia	k1gFnSc1	Opuntia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rebucie	rebucie	k1gFnSc1	rebucie
(	(	kIx(	(
<g/>
Rebutia	Rebutia	k1gFnSc1	Rebutia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ripsalis	ripsalis	k1gFnSc1	ripsalis
(	(	kIx(	(
<g/>
Rhipsalis	Rhipsalis	k1gFnSc1	Rhipsalis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
saguaro	saguara	k1gFnSc5	saguara
(	(	kIx(	(
<g/>
Carnegiea	Carnegieum	k1gNnSc2	Carnegieum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
selenicereus	selenicereus	k1gMnSc1	selenicereus
(	(	kIx(	(
<g/>
Selenicereus	Selenicereus	k1gMnSc1	Selenicereus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sklerokaktus	sklerokaktus	k1gMnSc1	sklerokaktus
(	(	kIx(	(
<g/>
Sclerocactus	Sclerocactus	k1gMnSc1	Sclerocactus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
stetsonie	stetsonie	k1gFnPc1	stetsonie
(	(	kIx(	(
<g/>
Stetsonia	Stetsonium	k1gNnPc1	Stetsonium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
telokaktus	telokaktus	k1gMnSc1	telokaktus
(	(	kIx(	(
<g/>
Thelocactus	Thelocactus	k1gMnSc1	Thelocactus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
trichocereus	trichocereus	k1gMnSc1	trichocereus
(	(	kIx(	(
<g/>
Trichocereus	Trichocereus	k1gMnSc1	Trichocereus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Opuncie	opuncie	k1gFnSc1	opuncie
mexická	mexický	k2eAgFnSc1d1	mexická
je	být	k5eAaImIp3nS	být
pěstována	pěstován	k2eAgFnSc1d1	pěstována
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
tropických	tropický	k2eAgFnPc6d1	tropická
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc6d1	subtropická
zemích	zem	k1gFnPc6	zem
pro	pro	k7c4	pro
jedlé	jedlý	k2eAgInPc4d1	jedlý
plody	plod	k1gInPc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místních	místní	k2eAgInPc6d1	místní
trzích	trh	k1gInPc6	trh
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
i	i	k8xC	i
plody	plod	k1gInPc4	plod
některých	některý	k3yIgInPc2	některý
jiných	jiný	k2eAgInPc2d1	jiný
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Hylocereus	Hylocereus	k1gInSc1	Hylocereus
<g/>
,	,	kIx,	,
Myrtillocactus	Myrtillocactus	k1gInSc1	Myrtillocactus
<g/>
,	,	kIx,	,
Echinocereus	Echinocereus	k1gInSc1	Echinocereus
a	a	k8xC	a
Pereskia	Pereskia	k1gFnSc1	Pereskia
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc4d1	mladý
stonky	stonek	k1gInPc4	stonek
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
opuncií	opuncie	k1gFnPc2	opuncie
se	se	k3xPyFc4	se
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
vařené	vařený	k2eAgInPc1d1	vařený
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k9	jako
krmivo	krmivo	k1gNnSc4	krmivo
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
se	se	k3xPyFc4	se
kandují	kandovat	k5eAaImIp3nP	kandovat
některé	některý	k3yIgInPc1	některý
vonné	vonný	k2eAgInPc1d1	vonný
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
Ferocactus	Ferocactus	k1gInSc1	Ferocactus
<g/>
.	.	kIx.	.
<g/>
Sloupcovité	sloupcovitý	k2eAgInPc1d1	sloupcovitý
kaktusy	kaktus	k1gInPc1	kaktus
se	se	k3xPyFc4	se
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
jako	jako	k9	jako
neprostupné	prostupný	k2eNgFnPc1d1	neprostupná
zábrany	zábrana	k1gFnPc1	zábrana
a	a	k8xC	a
ohrazení	ohrazení	k1gNnPc1	ohrazení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
bez	bez	k7c2	bez
stromů	strom	k1gInPc2	strom
slouží	sloužit	k5eAaImIp3nP	sloužit
stonky	stonka	k1gFnPc4	stonka
sloupcovitých	sloupcovitý	k2eAgInPc2d1	sloupcovitý
kaktusů	kaktus	k1gInPc2	kaktus
i	i	k9	i
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jako	jako	k9	jako
palivo	palivo	k1gNnSc1	palivo
<g/>
.	.	kIx.	.
<g/>
Aztékové	Azték	k1gMnPc1	Azték
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
mexické	mexický	k2eAgInPc1d1	mexický
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
získávaly	získávat	k5eAaImAgFnP	získávat
z	z	k7c2	z
červců	červec	k1gMnPc2	červec
nopálových	nopálový	k2eAgInPc2d1	nopálový
žijících	žijící	k2eAgInPc2d1	žijící
na	na	k7c6	na
opunciích	opuncie	k1gFnPc6	opuncie
červené	červený	k2eAgNnSc1d1	červené
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
kulturu	kultura	k1gFnSc4	kultura
později	pozdě	k6eAd2	pozdě
převzali	převzít	k5eAaPmAgMnP	převzít
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
a	a	k8xC	a
barvivo	barvivo	k1gNnSc1	barvivo
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k9	jako
karmín	karmín	k1gInSc4	karmín
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gNnSc4	jeho
využití	využití	k1gNnSc4	využití
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
vytlačila	vytlačit	k5eAaPmAgNnP	vytlačit
syntetická	syntetický	k2eAgNnPc1d1	syntetické
barviva	barvivo	k1gNnPc1	barvivo
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
získává	získávat	k5eAaImIp3nS	získávat
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
(	(	kIx(	(
<g/>
E	E	kA	E
120	[number]	k4	120
<g/>
)	)	kIx)	)
a	a	k8xC	a
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
kaktusy	kaktus	k1gInPc1	kaktus
mají	mít	k5eAaImIp3nP	mít
halucinogenní	halucinogenní	k2eAgInPc1d1	halucinogenní
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
náleží	náležet	k5eAaImIp3nS	náležet
mexický	mexický	k2eAgMnSc1d1	mexický
peyotl	peyotnout	k5eAaPmAgMnS	peyotnout
(	(	kIx(	(
<g/>
Lophophora	Lophophora	k1gFnSc1	Lophophora
williamsii	williamsie	k1gFnSc4	williamsie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
San	San	k1gMnPc1	San
Pedro	Pedro	k1gNnSc1	Pedro
(	(	kIx(	(
<g/>
Echinopsis	Echinopsis	k1gFnSc1	Echinopsis
pachanoi	pachano	k1gFnSc2	pachano
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
využití	využití	k1gNnSc1	využití
má	mít	k5eAaImIp3nS	mít
tisíciletou	tisíciletý	k2eAgFnSc4d1	tisíciletá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
<g/>
Kaktusy	kaktus	k1gInPc1	kaktus
mají	mít	k5eAaImIp3nP	mít
nemalý	malý	k2eNgInSc4d1	nemalý
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
okrasné	okrasný	k2eAgFnPc4d1	okrasná
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstovat	k5eAaImNgFnP	pěstovat
v	v	k7c6	v
nepřeberném	přeberný	k2eNgNnSc6d1	nepřeberné
množství	množství	k1gNnSc6	množství
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
kaktusů	kaktus	k1gInPc2	kaktus
==	==	k?	==
</s>
</p>
<p>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
pěstovaní	pěstovaný	k2eAgMnPc1d1	pěstovaný
kaktusů	kaktus	k1gInPc2	kaktus
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dodržení	dodržení	k1gNnSc1	dodržení
dvou	dva	k4xCgFnPc2	dva
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
respektování	respektování	k1gNnSc1	respektování
období	období	k1gNnSc2	období
vegetace	vegetace	k1gFnSc2	vegetace
a	a	k8xC	a
období	období	k1gNnSc2	období
zimního	zimní	k2eAgInSc2d1	zimní
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
zadruhé	zadruhé	k4xO	zadruhé
opatrnou	opatrný	k2eAgFnSc4d1	opatrná
zálivku	zálivka	k1gFnSc4	zálivka
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnPc1d1	následující
pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
obecná	obecný	k2eAgNnPc1d1	obecné
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
požadavky	požadavek	k1gInPc1	požadavek
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
druhy	druh	k1gInPc1	druh
pěstované	pěstovaný	k2eAgInPc1d1	pěstovaný
kaktusáři	kaktusář	k1gMnPc1	kaktusář
<g/>
.	.	kIx.	.
</s>
<s>
Odlišně	odlišně	k6eAd1	odlišně
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
epifytické	epifytický	k2eAgInPc1d1	epifytický
kaktusy	kaktus	k1gInPc1	kaktus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vegetační	vegetační	k2eAgNnSc4d1	vegetační
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Vegetační	vegetační	k2eAgNnSc1d1	vegetační
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
přímým	přímý	k2eAgInSc7d1	přímý
slunečním	sluneční	k2eAgInSc7d1	sluneční
svitem	svit	k1gInSc7	svit
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
obvykle	obvykle	k6eAd1	obvykle
nad	nad	k7c4	nad
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
oslunění	oslunění	k1gNnSc2	oslunění
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kratším	krátký	k2eAgNnSc6d2	kratší
oslunění	oslunění	k1gNnSc6	oslunění
se	se	k3xPyFc4	se
výběr	výběr	k1gInSc1	výběr
prosperujících	prosperující	k2eAgInPc2d1	prosperující
druhů	druh	k1gInPc2	druh
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
<g/>
,	,	kIx,	,
na	na	k7c6	na
temeni	temeno	k1gNnSc6	temeno
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
přirůstající	přirůstající	k2eAgInPc4d1	přirůstající
nové	nový	k2eAgInPc4d1	nový
areoly	areol	k1gInPc4	areol
s	s	k7c7	s
trny	trn	k1gInPc7	trn
a	a	k8xC	a
mladá	mladý	k2eAgFnSc1d1	mladá
světlejší	světlý	k2eAgFnSc1d2	světlejší
pokožka	pokožka	k1gFnSc1	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
díky	díky	k7c3	díky
zálivce	zálivka	k1gFnSc3	zálivka
napité	napitý	k2eAgFnSc3d1	napitá
<g/>
.	.	kIx.	.
</s>
<s>
Zálivka	zálivka	k1gFnSc1	zálivka
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
,	,	kIx,	,
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
noční	noční	k2eAgFnPc1d1	noční
teploty	teplota	k1gFnPc1	teplota
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
letní	letní	k2eAgInPc1d1	letní
období	období	k1gNnSc4	období
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
Rebutia	Rebutia	k1gFnSc1	Rebutia
<g/>
,	,	kIx,	,
Lobivia	Lobivia	k1gFnSc1	Lobivia
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
se	se	k3xPyFc4	se
již	již	k9	již
zálivka	zálivka	k1gFnSc1	zálivka
omezuje	omezovat	k5eAaImIp3nS	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vegetace	vegetace	k1gFnSc2	vegetace
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
2	[number]	k4	2
<g/>
×	×	k?	×
až	až	k9	až
4	[number]	k4	4
<g/>
×	×	k?	×
doplnit	doplnit	k5eAaPmF	doplnit
zálivku	zálivka	k1gFnSc4	zálivka
o	o	k7c4	o
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
zálivce	zálivka	k1gFnSc6	zálivka
musí	muset	k5eAaImIp3nS	muset
následovat	následovat	k5eAaImF	následovat
proschnutí	proschnutí	k1gNnSc4	proschnutí
celého	celý	k2eAgInSc2d1	celý
objemu	objem	k1gInSc2	objem
substrátu	substrát	k1gInSc2	substrát
<g/>
.	.	kIx.	.
</s>
<s>
Trvalá	trvalý	k2eAgFnSc1d1	trvalá
vlhkost	vlhkost	k1gFnSc1	vlhkost
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
žádoucí	žádoucí	k2eAgInSc4d1	žádoucí
pro	pro	k7c4	pro
nesukulentní	sukulentní	k2eNgFnPc4d1	sukulentní
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
hnilobě	hniloba	k1gFnSc3	hniloba
kořenů	kořen	k1gInPc2	kořen
a	a	k8xC	a
zániku	zánik	k1gInSc2	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
déletrvajících	déletrvající	k2eAgInPc2d1	déletrvající
dešťů	dešť	k1gInPc2	dešť
se	se	k3xPyFc4	se
zálivka	zálivka	k1gFnSc1	zálivka
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
<g/>
.	.	kIx.	.
</s>
<s>
Sukulenty	sukulent	k1gInPc1	sukulent
snadno	snadno	k6eAd1	snadno
vydrží	vydržet	k5eAaPmIp3nP	vydržet
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
bez	bez	k7c2	bez
zálivky	zálivka	k1gFnSc2	zálivka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jedna	jeden	k4xCgFnSc1	jeden
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
zálivka	zálivka	k1gFnSc1	zálivka
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Zálivku	zálivka	k1gFnSc4	zálivka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
doplnit	doplnit	k5eAaPmF	doplnit
o	o	k7c6	o
rosení	rosení	k1gNnSc6	rosení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přezimování	přezimování	k1gNnSc2	přezimování
===	===	k?	===
</s>
</p>
<p>
<s>
v	v	k7c6	v
období	období	k1gNnSc6	období
nedostatečného	dostatečný	k2eNgInSc2d1	nedostatečný
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
až	až	k8xS	až
březnu	březen	k1gInSc6	březen
jsou	být	k5eAaImIp3nP	být
kaktusy	kaktus	k1gInPc1	kaktus
v	v	k7c6	v
období	období	k1gNnSc6	období
klidu	klid	k1gInSc2	klid
odpovídajícímu	odpovídající	k2eAgInSc3d1	odpovídající
stagnaci	stagnace	k1gFnSc3	stagnace
v	v	k7c6	v
suchém	suchý	k2eAgNnSc6d1	suché
období	období	k1gNnSc6	období
na	na	k7c6	na
nalezištích	naleziště	k1gNnPc6	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
suchu	sucho	k1gNnSc6	sucho
a	a	k8xC	a
chladu	chlad	k1gInSc6	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Většině	většina	k1gFnSc3	většina
druhů	druh	k1gInPc2	druh
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
teplota	teplota	k1gFnSc1	teplota
8	[number]	k4	8
–	–	k?	–
12	[number]	k4	12
°	°	k?	°
<g/>
C.	C.	kA	C.
Světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Zásadně	zásadně	k6eAd1	zásadně
se	se	k3xPyFc4	se
nezalévají	zalévat	k5eNaImIp3nP	zalévat
<g/>
,	,	kIx,	,
vodu	voda	k1gFnSc4	voda
nevyužijí	využít	k5eNaPmIp3nP	využít
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
hnilobě	hniloba	k1gFnSc3	hniloba
kořenů	kořen	k1gInPc2	kořen
a	a	k8xC	a
zániku	zánik	k1gInSc2	zánik
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
vlhkost	vlhkost	k1gFnSc1	vlhkost
je	být	k5eAaImIp3nS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
sesycháním	sesychání	k1gNnSc7	sesychání
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
třetinu	třetina	k1gFnSc4	třetina
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přesazování	přesazování	k1gNnSc2	přesazování
===	===	k?	===
</s>
</p>
<p>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
se	se	k3xPyFc4	se
přesazují	přesazovat	k5eAaImIp3nP	přesazovat
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
doporučované	doporučovaný	k2eAgNnSc1d1	doporučované
každoroční	každoroční	k2eAgNnSc1d1	každoroční
přesazování	přesazování	k1gNnSc1	přesazování
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
a	a	k8xC	a
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
sbírek	sbírka	k1gFnPc2	sbírka
ani	ani	k8xC	ani
proveditelné	proveditelný	k2eAgNnSc1d1	proveditelné
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neexistovaly	existovat	k5eNaImAgInP	existovat
účinné	účinný	k2eAgInPc1d1	účinný
postřiky	postřik	k1gInPc1	postřik
proti	proti	k7c3	proti
kořenovým	kořenový	k2eAgMnPc3d1	kořenový
škůdcům	škůdce	k1gMnPc3	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
období	období	k1gNnSc1	období
na	na	k7c6	na
přesazování	přesazování	k1gNnSc6	přesazování
je	být	k5eAaImIp3nS	být
jaro	jaro	k1gNnSc4	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Substrát	substrát	k1gInSc1	substrát
má	mít	k5eAaImIp3nS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
jednu	jeden	k4xCgFnSc4	jeden
až	až	k9	až
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
hrubšího	hrubý	k2eAgInSc2d2	hrubší
propraného	propraný	k2eAgInSc2d1	propraný
písku	písek	k1gInSc2	písek
nebo	nebo	k8xC	nebo
antuky	antuka	k1gFnSc2	antuka
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc2	složení
zbývající	zbývající	k2eAgFnSc2d1	zbývající
zeminy	zemina	k1gFnSc2	zemina
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
podstatné	podstatný	k2eAgNnSc1d1	podstatné
<g/>
.	.	kIx.	.
</s>
<s>
Kaktusy	kaktus	k1gInPc1	kaktus
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
během	během	k7c2	během
suchého	suchý	k2eAgNnSc2d1	suché
období	období	k1gNnSc2	období
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
kořeny	kořen	k1gInPc1	kořen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
při	při	k7c6	při
přesazování	přesazování	k1gNnSc6	přesazování
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc1	smysl
zachovávat	zachovávat	k5eAaImF	zachovávat
kořenový	kořenový	k2eAgInSc4d1	kořenový
bal	bal	k1gInSc4	bal
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
starý	starý	k2eAgInSc1d1	starý
substrát	substrát	k1gInSc1	substrát
zcela	zcela	k6eAd1	zcela
odstraní	odstranit	k5eAaPmIp3nS	odstranit
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
kořenů	kořen	k1gInPc2	kořen
a	a	k8xC	a
hlavní	hlavní	k2eAgInPc1d1	hlavní
kořeny	kořen	k1gInPc1	kořen
se	se	k3xPyFc4	se
zkrátí	zkrátit	k5eAaPmIp3nP	zkrátit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
až	až	k8xS	až
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Ranky	ranka	k1gFnPc1	ranka
se	se	k3xPyFc4	se
nechají	nechat	k5eAaPmIp3nP	nechat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
zaschnout	zaschnout	k5eAaPmF	zaschnout
<g/>
.	.	kIx.	.
</s>
<s>
Sází	sázet	k5eAaImIp3nS	sázet
se	se	k3xPyFc4	se
do	do	k7c2	do
suchého	suchý	k2eAgInSc2d1	suchý
substrátu	substrát	k1gInSc2	substrát
a	a	k8xC	a
zalévá	zalévat	k5eAaImIp3nS	zalévat
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
týden	týden	k1gInSc4	týden
po	po	k7c6	po
zasazení	zasazení	k1gNnSc6	zasazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hydroponie	hydroponie	k1gFnSc2	hydroponie
===	===	k?	===
</s>
</p>
<p>
<s>
neboli	neboli	k8xC	neboli
štěrková	štěrkový	k2eAgFnSc1d1	štěrková
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
běžného	běžný	k2eAgInSc2d1	běžný
substrátu	substrát	k1gInSc2	substrát
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
zeminy	zemina	k1gFnSc2	zemina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
anorganický	anorganický	k2eAgInSc1d1	anorganický
inertní	inertní	k2eAgInSc1d1	inertní
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
písek	písek	k1gInSc1	písek
a	a	k8xC	a
antuka	antuka	k1gFnSc1	antuka
o	o	k7c6	o
zrnění	zrnění	k1gNnSc6	zrnění
2	[number]	k4	2
–	–	k?	–
6	[number]	k4	6
mm	mm	kA	mm
<g/>
,	,	kIx,	,
proprané	propraný	k2eAgFnSc2d1	propraná
a	a	k8xC	a
zbavené	zbavený	k2eAgFnSc2d1	zbavená
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
substrát	substrát	k1gInSc1	substrát
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
inertní	inertní	k2eAgNnSc1d1	inertní
<g/>
,	,	kIx,	,
nemění	měnit	k5eNaImIp3nS	měnit
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzdušný	vzdušný	k2eAgMnSc1d1	vzdušný
<g/>
,	,	kIx,	,
omezuje	omezovat	k5eAaImIp3nS	omezovat
vývin	vývin	k1gInSc1	vývin
škůdců	škůdce	k1gMnPc2	škůdce
a	a	k8xC	a
plísní	plíseň	k1gFnPc2	plíseň
a	a	k8xC	a
kaktusy	kaktus	k1gInPc4	kaktus
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
dobře	dobře	k6eAd1	dobře
koření	kořenit	k5eAaImIp3nS	kořenit
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
<g/>
,	,	kIx,	,
propláchnutím	propláchnutí	k1gNnPc3	propláchnutí
se	se	k3xPyFc4	se
vyplaví	vyplavit	k5eAaPmIp3nS	vyplavit
organické	organický	k2eAgInPc4d1	organický
zbytky	zbytek	k1gInPc4	zbytek
a	a	k8xC	a
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
propařením	propaření	k1gNnSc7	propaření
zničí	zničit	k5eAaPmIp3nP	zničit
škůdci	škůdce	k1gMnPc1	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přesazování	přesazování	k1gNnSc6	přesazování
pak	pak	k6eAd1	pak
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
shánět	shánět	k5eAaImF	shánět
novou	nový	k2eAgFnSc4d1	nová
zeminu	zemina	k1gFnSc4	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zálivky	zálivka	k1gFnSc2	zálivka
se	se	k3xPyFc4	se
asi	asi	k9	asi
4	[number]	k4	4
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
přidává	přidávat	k5eAaImIp3nS	přidávat
plné	plný	k2eAgNnSc4d1	plné
hnojivo	hnojivo	k1gNnSc4	hnojivo
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
stopových	stopový	k2eAgInPc2d1	stopový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volná	volný	k2eAgFnSc1d1	volná
kultura	kultura	k1gFnSc1	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
znamená	znamenat	k5eAaImIp3nS	znamenat
umístění	umístění	k1gNnSc4	umístění
během	během	k7c2	během
vegetace	vegetace	k1gFnSc2	vegetace
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k9	až
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
venkovního	venkovní	k2eAgNnSc2d1	venkovní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
do	do	k7c2	do
předokenních	předokenní	k2eAgInPc2d1	předokenní
truhlíků	truhlík	k1gInPc2	truhlík
<g/>
,	,	kIx,	,
na	na	k7c4	na
balkon	balkon	k1gInSc4	balkon
<g/>
,	,	kIx,	,
na	na	k7c4	na
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
místo	místo	k1gNnSc4	místo
třeba	třeba	k9	třeba
stůl	stůl	k1gInSc4	stůl
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Kaktusy	kaktus	k1gInPc4	kaktus
vystavené	vystavená	k1gFnSc2	vystavená
přímému	přímý	k2eAgNnSc3d1	přímé
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
záření	záření	k1gNnSc3	záření
<g/>
,	,	kIx,	,
střídání	střídání	k1gNnSc1	střídání
teplot	teplota	k1gFnPc2	teplota
během	během	k7c2	během
dne	den	k1gInSc2	den
a	a	k8xC	a
ranní	ranní	k2eAgFnSc6d1	ranní
rose	rosa	k1gFnSc6	rosa
mají	mít	k5eAaImIp3nP	mít
mohutné	mohutný	k2eAgInPc4d1	mohutný
trny	trn	k1gInPc4	trn
<g/>
,	,	kIx,	,
vybarvenou	vybarvený	k2eAgFnSc4d1	vybarvená
pokožku	pokožka	k1gFnSc4	pokožka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
otužilé	otužilý	k2eAgFnPc1d1	otužilá
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
kvetou	kvést	k5eAaImIp3nP	kvést
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
mexické	mexický	k2eAgInPc4d1	mexický
pouštní	pouštní	k2eAgInPc4d1	pouštní
druhy	druh	k1gInPc4	druh
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Aztekium	Aztekium	k1gNnSc1	Aztekium
<g/>
,	,	kIx,	,
Ariocarpus	Ariocarpus	k1gInSc1	Ariocarpus
<g/>
,	,	kIx,	,
Astrophytum	Astrophytum	k1gNnSc1	Astrophytum
<g/>
,	,	kIx,	,
Sclerocactus	Sclerocactus	k1gInSc1	Sclerocactus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
během	během	k7c2	během
vegetace	vegetace	k1gFnSc2	vegetace
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
===	===	k?	===
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
odnože	odnož	k1gFnPc4	odnož
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
lehce	lehko	k6eAd1	lehko
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Pevně	pevně	k6eAd1	pevně
přirostlé	přirostlý	k2eAgFnPc4d1	přirostlá
odnože	odnož	k1gFnPc4	odnož
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
spojení	spojení	k1gNnSc2	spojení
odříznout	odříznout	k5eAaPmF	odříznout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
vytržení	vytržení	k1gNnSc3	vytržení
cévního	cévní	k2eAgInSc2d1	cévní
svazku	svazek	k1gInSc2	svazek
z	z	k7c2	z
mateční	mateční	k2eAgFnSc2d1	mateční
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Ranku	ranka	k1gFnSc4	ranka
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nechat	nechat	k5eAaPmF	nechat
zaschnout	zaschnout	k5eAaPmF	zaschnout
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
přijít	přijít	k5eAaPmF	přijít
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zaschnutí	zaschnutí	k1gNnSc6	zaschnutí
se	se	k3xPyFc4	se
odnož	odnož	k1gInSc1	odnož
zasadí	zasadit	k5eAaPmIp3nS	zasadit
a	a	k8xC	a
substrát	substrát	k1gInSc1	substrát
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
zvlhčuje	zvlhčovat	k5eAaImIp3nS	zvlhčovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyčkat	vyčkat	k5eAaPmF	vyčkat
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
zárodků	zárodek	k1gInPc2	zárodek
kořenů	kořen	k1gInPc2	kořen
bez	bez	k7c2	bez
zasazení	zasazení	k1gNnSc2	zasazení
na	na	k7c6	na
suchu	sucho	k1gNnSc6	sucho
a	a	k8xC	a
sázet	sázet	k5eAaImF	sázet
až	až	k9	až
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
objevení	objevení	k1gNnSc6	objevení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
semeny	semeno	k1gNnPc7	semeno
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
především	především	k9	především
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
plísním	plíseň	k1gFnPc3	plíseň
a	a	k8xC	a
trpělivost	trpělivost	k1gFnSc4	trpělivost
<g/>
.	.	kIx.	.
</s>
<s>
Substrát	substrát	k1gInSc1	substrát
se	se	k3xPyFc4	se
sterilizuje	sterilizovat	k5eAaImIp3nS	sterilizovat
přepařením	přepaření	k1gNnSc7	přepaření
<g/>
,	,	kIx,	,
pěstební	pěstební	k2eAgFnPc1d1	pěstební
nádoby	nádoba	k1gFnPc1	nádoba
také	také	k9	také
nebo	nebo	k8xC	nebo
chemicky	chemicky	k6eAd1	chemicky
(	(	kIx(	(
<g/>
peroxidem	peroxid	k1gInSc7	peroxid
<g/>
,	,	kIx,	,
lihem	líh	k1gInSc7	líh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
se	se	k3xPyFc4	se
očistí	očistit	k5eAaPmIp3nP	očistit
od	od	k7c2	od
zbytků	zbytek	k1gInPc2	zbytek
pulpy	pulpa	k1gFnSc2	pulpa
a	a	k8xC	a
ošetřují	ošetřovat	k5eAaImIp3nP	ošetřovat
se	se	k3xPyFc4	se
fungicidy	fungicid	k1gInPc7	fungicid
<g/>
.	.	kIx.	.
</s>
<s>
Drobná	drobný	k2eAgNnPc1d1	drobné
semena	semeno	k1gNnPc1	semeno
se	se	k3xPyFc4	se
do	do	k7c2	do
substrátu	substrát	k1gInSc2	substrát
pouze	pouze	k6eAd1	pouze
zatlačují	zatlačovat	k5eAaImIp3nP	zatlačovat
<g/>
,	,	kIx,	,
nezasypávají	zasypávat	k5eNaImIp3nP	zasypávat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Klíčí	klíčit	k5eAaImIp3nS	klíčit
během	během	k7c2	během
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
rodů	rod	k1gInPc2	rod
==	==	k?	==
</s>
</p>
<p>
<s>
Acanthocereus	Acanthocereus	k1gMnSc1	Acanthocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Acharagma	Acharagma	k1gFnSc1	Acharagma
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ariocarpus	Ariocarpus	k1gMnSc1	Ariocarpus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Armatocereus	Armatocereus	k1gMnSc1	Armatocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Arrojadoa	Arrojadoa	k6eAd1	Arrojadoa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Arthrocereus	Arthrocereus	k1gMnSc1	Arthrocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Astrophytum	Astrophytum	k1gNnSc1	Astrophytum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Austrocactus	Austrocactus	k1gMnSc1	Austrocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Austrocylindropuntia	Austrocylindropuntia	k1gFnSc1	Austrocylindropuntia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Aztekium	Aztekium	k1gNnSc1	Aztekium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bergerocactus	Bergerocactus	k1gMnSc1	Bergerocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Blossfeldia	Blossfeldium	k1gNnPc1	Blossfeldium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Brachycereus	Brachycereus	k1gMnSc1	Brachycereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Brasilicereus	Brasilicereus	k1gMnSc1	Brasilicereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Brasiliopuntia	Brasiliopuntia	k1gFnSc1	Brasiliopuntia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Browningia	Browningia	k1gFnSc1	Browningia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Calymmanthium	Calymmanthium	k1gNnSc1	Calymmanthium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Carnegiea	Carnegiea	k6eAd1	Carnegiea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Castellanosia	Castellanosia	k1gFnSc1	Castellanosia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cephalocereus	Cephalocereus	k1gMnSc1	Cephalocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cereus	Cereus	k1gMnSc1	Cereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cintia	Cintia	k1gFnSc1	Cintia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cipocereus	Cipocereus	k1gMnSc1	Cipocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cleistocactus	Cleistocactus	k1gMnSc1	Cleistocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cleistocana	Cleistocana	k1gFnSc1	Cleistocana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Coleocephalocereus	Coleocephalocereus	k1gMnSc1	Coleocephalocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Consolea	Consolea	k6eAd1	Consolea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Copiapoa	Copiapoa	k6eAd1	Copiapoa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Corryocactus	Corryocactus	k1gMnSc1	Corryocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Corynopuntia	Corynopuntia	k1gFnSc1	Corynopuntia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Coryphantha	Coryphantha	k1gFnSc1	Coryphantha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cumulopuntia	Cumulopuntia	k1gFnSc1	Cumulopuntia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cylindropuntia	Cylindropuntia	k1gFnSc1	Cylindropuntia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dendrocereus	Dendrocereus	k1gMnSc1	Dendrocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Denmoza	Denmoza	k1gFnSc1	Denmoza
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Discocactus	Discocactus	k1gMnSc1	Discocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Disocactus	Disocactus	k1gMnSc1	Disocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Echinocactus	Echinocactus	k1gMnSc1	Echinocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Echinocereus	Echinocereus	k1gMnSc1	Echinocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Echinopsis	Echinopsis	k1gFnSc1	Echinopsis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Epiphyllum	Epiphyllum	k1gInSc1	Epiphyllum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Epithelantha	Epithelantha	k1gFnSc1	Epithelantha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Eriosyce	Eriosyko	k6eAd1	Eriosyko
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Escobaria	Escobarium	k1gNnPc1	Escobarium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Escontria	Escontrium	k1gNnPc1	Escontrium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Espostoa	Espostoa	k6eAd1	Espostoa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Espostocactus	Espostocactus	k1gMnSc1	Espostocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Espostoopsis	Espostoopsis	k1gFnSc1	Espostoopsis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Eulychnia	Eulychnium	k1gNnPc1	Eulychnium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Facheiroa	Facheiroa	k6eAd1	Facheiroa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ferocactus	Ferocactus	k1gMnSc1	Ferocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Frailea	Frailea	k6eAd1	Frailea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Geohintonia	Geohintonium	k1gNnPc1	Geohintonium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Grusonia	Grusonium	k1gNnPc1	Grusonium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Gymnocalycium	Gymnocalycium	k1gNnSc1	Gymnocalycium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Haageocereus	Haageocereus	k1gMnSc1	Haageocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Haagespostoa	Haagespostoa	k6eAd1	Haagespostoa
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Harrisia	Harrisia	k1gFnSc1	Harrisia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hatiora	Hatiora	k1gFnSc1	Hatiora
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hylocereus	Hylocereus	k1gMnSc1	Hylocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jasminocereus	Jasminocereus	k1gMnSc1	Jasminocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lasiocereus	Lasiocereus	k1gMnSc1	Lasiocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Leocereus	Leocereus	k1gMnSc1	Leocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lepismium	Lepismium	k1gNnSc1	Lepismium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Leptocereus	Leptocereus	k1gMnSc1	Leptocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Leuchtenbergia	Leuchtenbergia	k1gFnSc1	Leuchtenbergia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lophophora	Lophophora	k1gFnSc1	Lophophora
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Maihuenia	Maihuenium	k1gNnPc1	Maihuenium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Maihueniopsis	Maihueniopsis	k1gFnSc1	Maihueniopsis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Mammillaria	Mammillarium	k1gNnPc1	Mammillarium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Matucana	Matucana	k1gFnSc1	Matucana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Melocactus	Melocactus	k1gMnSc1	Melocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Micranthocereus	Micranthocereus	k1gMnSc1	Micranthocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Mila	Mila	k6eAd1	Mila
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Miqueliopuntia	Miqueliopuntia	k1gFnSc1	Miqueliopuntia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Myrtgerocactus	Myrtgerocactus	k1gMnSc1	Myrtgerocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Myrtillocactus	Myrtillocactus	k1gMnSc1	Myrtillocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Neobuxbaumia	Neobuxbaumia	k1gFnSc1	Neobuxbaumia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Neolloydia	Neolloydium	k1gNnPc1	Neolloydium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Neoraimondia	Neoraimondium	k1gNnPc1	Neoraimondium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Neowerdermannia	Neowerdermannium	k1gNnPc1	Neowerdermannium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Nopalea	Nopalea	k6eAd1	Nopalea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Obregonia	Obregonium	k1gNnPc1	Obregonium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Opuntia	Opuntia	k1gFnSc1	Opuntia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Oreocereus	Oreocereus	k1gMnSc1	Oreocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Oroya	Oroya	k6eAd1	Oroya
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ortegocactus	Ortegocactus	k1gMnSc1	Ortegocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pacherocactus	Pacherocactus	k1gMnSc1	Pacherocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pachycereus	Pachycereus	k1gMnSc1	Pachycereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Parodia	Parodium	k1gNnPc1	Parodium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pediocactus	Pediocactus	k1gMnSc1	Pediocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pelecyphora	Pelecyphora	k1gFnSc1	Pelecyphora
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Peniocereus	Peniocereus	k1gMnSc1	Peniocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pereskia	Pereskia	k1gFnSc1	Pereskia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pereskiopsis	Pereskiopsis	k1gFnSc1	Pereskiopsis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pfeiffera	Pfeiffera	k1gFnSc1	Pfeiffera
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pierrebraunia	Pierrebraunium	k1gNnPc1	Pierrebraunium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pilosocereus	Pilosocereus	k1gMnSc1	Pilosocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Polaskia	Polaskia	k1gFnSc1	Polaskia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Praecereus	Praecereus	k1gMnSc1	Praecereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pseudoacanthocereus	Pseudoacanthocereus	k1gMnSc1	Pseudoacanthocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pseudorhipsalis	Pseudorhipsalis	k1gFnSc1	Pseudorhipsalis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pterocactus	Pterocactus	k1gMnSc1	Pterocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pygmaeocereus	Pygmaeocereus	k1gMnSc1	Pygmaeocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Quiabentia	Quiabentia	k1gFnSc1	Quiabentia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Rauhocereus	Rauhocereus	k1gMnSc1	Rauhocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Rebutia	Rebutia	k1gFnSc1	Rebutia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Rhipsalis	Rhipsalis	k1gFnSc1	Rhipsalis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Samaipaticereus	Samaipaticereus	k1gMnSc1	Samaipaticereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Schlumbergera	Schlumbergera	k1gFnSc1	Schlumbergera
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Sclerocactus	Sclerocactus	k1gMnSc1	Sclerocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Selenicereus	Selenicereus	k1gMnSc1	Selenicereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stenocactus	Stenocactus	k1gMnSc1	Stenocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stenocereus	Stenocereus	k1gMnSc1	Stenocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stephanocereus	Stephanocereus	k1gMnSc1	Stephanocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stetsonia	Stetsonium	k1gNnPc1	Stetsonium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Strombocactus	Strombocactus	k1gMnSc1	Strombocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Strophocactus	Strophocactus	k1gMnSc1	Strophocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tacinga	Tacinga	k1gFnSc1	Tacinga
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tephrocactus	Tephrocactus	k1gMnSc1	Tephrocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Thelocactus	Thelocactus	k1gMnSc1	Thelocactus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tunilla	Tunilla	k6eAd1	Tunilla
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Turbinicarpus	Turbinicarpus	k1gMnSc1	Turbinicarpus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Uebelmannia	Uebelmannium	k1gNnPc1	Uebelmannium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Weberbauerocereus	Weberbauerocereus	k1gMnSc1	Weberbauerocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Weberocereus	Weberocereus	k1gMnSc1	Weberocereus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Yavia	Yavia	k1gFnSc1	Yavia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Yungasocereus	Yungasocereus	k1gMnSc1	Yungasocereus
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kaktus	kaktus	k1gInSc1	kaktus
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kaktusovité	kaktusovitý	k2eAgFnPc4d1	kaktusovitá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Cactaceae	Cactaceae	k1gInSc1	Cactaceae
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
kaktusů	kaktus	k1gInPc2	kaktus
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
o	o	k7c6	o
kaktusech	kaktus	k1gInPc6	kaktus
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Vesmír	vesmír	k1gInSc1	vesmír
</s>
</p>
