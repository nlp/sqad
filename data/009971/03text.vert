<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
(	(	kIx(	(
<g/>
700	[number]	k4	700
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
767	[number]	k4	767
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
757	[number]	k4	757
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
767	[number]	k4	767
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Jáhen	jáhen	k1gMnSc1	jáhen
Pavel	Pavel	k1gMnSc1	Pavel
byl	být	k5eAaImAgMnS	být
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
již	již	k9	již
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
Štěpána	Štěpán	k1gMnSc2	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
protikandidátem	protikandidát	k1gMnSc7	protikandidát
byl	být	k5eAaImAgMnS	být
arcijáhen	arcijáhen	k1gMnSc1	arcijáhen
Theofylaktos	Theofylaktos	k1gMnSc1	Theofylaktos
<g/>
,	,	kIx,	,
navrhovaný	navrhovaný	k2eAgInSc1d1	navrhovaný
probyzantskou	probyzantský	k2eAgFnSc7d1	probyzantský
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
obnovit	obnovit	k5eAaPmF	obnovit
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
papežským	papežský	k2eAgInSc7d1	papežský
stolcem	stolec	k1gInSc7	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Přednost	přednost	k1gFnSc4	přednost
nakonec	nakonec	k6eAd1	nakonec
dostal	dostat	k5eAaPmAgMnS	dostat
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
pontifikát	pontifikát	k1gInSc4	pontifikát
Štěpánovi	Štěpán	k1gMnSc3	Štěpán
nablízku	nablízku	k6eAd1	nablízku
<g/>
;	;	kIx,	;
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
po	po	k7c6	po
sobě	se	k3xPyFc3	se
v	v	k7c6	v
papežském	papežský	k2eAgInSc6d1	papežský
úřadě	úřad	k1gInSc6	úřad
měli	mít	k5eAaImAgMnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
kontinuitu	kontinuita	k1gFnSc4	kontinuita
jeho	jeho	k3xOp3gFnSc2	jeho
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
byl	být	k5eAaImAgMnS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
757	[number]	k4	757
jako	jako	k8xC	jako
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
předchůdce	předchůdce	k1gMnSc1	předchůdce
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
dobrých	dobrý	k2eAgInPc6d1	dobrý
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
franckým	francký	k2eAgMnSc7d1	francký
králem	král	k1gMnSc7	král
Pipinem	Pipin	k1gMnSc7	Pipin
Krátkým	Krátký	k1gMnSc7	Krátký
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
Pavlově	Pavlův	k2eAgNnSc6d1	Pavlovo
zvolení	zvolení	k1gNnSc6	zvolení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gInSc2	jeho
pontifikátu	pontifikát	k1gInSc2	pontifikát
spadají	spadat	k5eAaPmIp3nP	spadat
pokračující	pokračující	k2eAgFnPc4d1	pokračující
neshody	neshoda	k1gFnPc4	neshoda
s	s	k7c7	s
Langobardy	Langobard	k1gInPc7	Langobard
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
krále	král	k1gMnSc2	král
Desideria	desiderium	k1gNnSc2	desiderium
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Itálie	Itálie	k1gFnSc2	Itálie
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc4	jejich
následné	následný	k2eAgNnSc4d1	následné
urovnání	urovnání	k1gNnSc4	urovnání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konzolidaci	konzolidace	k1gFnSc4	konzolidace
církve	církev	k1gFnSc2	církev
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Pipinem	Pipin	k1gMnSc7	Pipin
proti	proti	k7c3	proti
Langobardům	Langobard	k1gMnPc3	Langobard
i	i	k8xC	i
Řekům	Řek	k1gMnPc3	Řek
<g/>
;	;	kIx,	;
Řím	Řím	k1gInSc1	Řím
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
pontifikátu	pontifikát	k1gInSc2	pontifikát
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Benevento	Benevento	k1gNnSc4	Benevento
a	a	k8xC	a
Toskánu	Toskána	k1gFnSc4	Toskána
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
Frankům	Frank	k1gMnPc3	Frank
bylo	být	k5eAaImAgNnS	být
převezení	převezení	k1gNnSc4	převezení
ostatků	ostatek	k1gInPc2	ostatek
svaté	svatý	k2eAgFnSc2d1	svatá
Petronily	Petronila	k1gFnSc2	Petronila
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
byl	být	k5eAaImAgInS	být
i	i	k9	i
kmotrem	kmotr	k1gMnSc7	kmotr
Pipinovy	Pipinův	k2eAgFnSc2d1	Pipinova
dcery	dcera	k1gFnSc2	dcera
Gisely	Gisela	k1gFnSc2	Gisela
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
pontifikátu	pontifikát	k1gInSc2	pontifikát
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
projevilo	projevit	k5eAaPmAgNnS	projevit
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
synodu	synod	k1gInSc2	synod
konaného	konaný	k2eAgInSc2d1	konaný
roku	rok	k1gInSc2	rok
754	[number]	k4	754
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
císaře	císař	k1gMnSc4	císař
Konstantina	Konstantin	k1gMnSc4	Konstantin
V.	V.	kA	V.
opět	opět	k6eAd1	opět
odsouzeno	odsouzen	k2eAgNnSc4d1	odsouzeno
uctívání	uctívání	k1gNnSc4	uctívání
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
obrazoborectví	obrazoborectví	k1gNnSc1	obrazoborectví
neboli	neboli	k8xC	neboli
ikonoklasmus	ikonoklasmus	k1gInSc1	ikonoklasmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
útěk	útěk	k1gInSc1	útěk
mnoha	mnoho	k4c2	mnoho
řeckých	řecký	k2eAgMnPc2d1	řecký
mnichů	mnich	k1gMnPc2	mnich
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
se	se	k3xPyFc4	se
Pavel	Pavel	k1gMnSc1	Pavel
I.	I.	kA	I.
zajímal	zajímat	k5eAaImAgMnS	zajímat
o	o	k7c4	o
výstavbu	výstavba	k1gFnSc4	výstavba
církevních	církevní	k2eAgFnPc2d1	církevní
budov	budova	k1gFnPc2	budova
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
především	především	k9	především
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
kláštera	klášter	k1gInSc2	klášter
San	San	k1gFnSc4	San
Silvestro	Silvestro	k1gNnSc1	Silvestro
in	in	k?	in
Capite	Capit	k1gInSc5	Capit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
767	[number]	k4	767
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
blahoslavené	blahoslavený	k2eAgFnSc2d1	blahoslavená
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nechal	nechat	k5eAaPmAgMnS	nechat
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pavel	Pavla	k1gFnPc2	Pavla
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Pavel	Pavel	k1gMnSc1	Pavel
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
