<s>
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1822	[number]	k4	1822
Dole	dole	k6eAd1	dole
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1895	[number]	k4	1895
Villeneuve-l	Villeneuveum	k1gNnPc2	Villeneuve-lum
<g/>
'	'	kIx"	'
<g/>
Etang	Etang	k1gInSc1	Etang
<g/>
,	,	kIx,	,
Marnes-la-Coquette	Marnesa-Coquett	k1gMnSc5	Marnes-la-Coquett
<g/>
,	,	kIx,	,
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
biolog	biolog	k1gMnSc1	biolog
<g/>
,	,	kIx,	,
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznačnějších	význačný	k2eAgMnPc2d3	nejvýznačnější
vědců	vědec	k1gMnPc2	vědec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
lékařských	lékařský	k2eAgFnPc2d1	lékařská
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
chemii	chemie	k1gFnSc4	chemie
na	na	k7c4	na
École	École	k1gInSc4	École
normale	normale	k6eAd1	normale
supérieure	supérieur	k1gMnSc5	supérieur
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
nových	nový	k2eAgInPc2d1	nový
vědeckých	vědecký	k2eAgInPc2d1	vědecký
oborů	obor	k1gInPc2	obor
stereochemie	stereochemie	k1gFnSc2	stereochemie
<g/>
,	,	kIx,	,
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
a	a	k8xC	a
imunologie	imunologie	k1gFnSc2	imunologie
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
vakcín	vakcína	k1gFnPc2	vakcína
proti	proti	k7c3	proti
sněti	sněť	k1gFnSc3	sněť
slezinné	slezinný	k2eAgFnSc3d1	slezinná
a	a	k8xC	a
vzteklině	vzteklina	k1gFnSc6	vzteklina
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
v	v	k7c6	v
Dole	dol	k1gInSc6	dol
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Francii	Francie	k1gFnSc6	Francie
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
chudého	chudý	k2eAgMnSc2d1	chudý
koželuha	koželuh	k1gMnSc2	koželuh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
představoval	představovat	k5eAaImAgMnS	představovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Louis	Louis	k1gMnSc1	Louis
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc3	jeho
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
synovy	synův	k2eAgFnPc1d1	synova
známky	známka	k1gFnPc1	známka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
píle	píle	k1gFnSc1	píle
a	a	k8xC	a
silná	silný	k2eAgFnSc1d1	silná
vůle	vůle	k1gFnSc1	vůle
otce	otka	k1gFnSc3	otka
přesvědčily	přesvědčit	k5eAaPmAgFnP	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
změnil	změnit	k5eAaPmAgInS	změnit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
promoval	promovat	k5eAaBmAgMnS	promovat
jako	jako	k9	jako
doktor	doktor	k1gMnSc1	doktor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
na	na	k7c4	na
katedru	katedra	k1gFnSc4	katedra
chemie	chemie	k1gFnSc2	chemie
na	na	k7c6	na
štrasburské	štrasburský	k2eAgFnSc6d1	Štrasburská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Laurentovou	Laurentův	k2eAgFnSc7d1	Laurentova
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
rektora	rektor	k1gMnSc2	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
manželského	manželský	k2eAgInSc2d1	manželský
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1849	[number]	k4	1849
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
dožily	dožít	k5eAaPmAgFnP	dožít
dospělosti	dospělost	k1gFnPc1	dospělost
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
zemřely	zemřít	k5eAaPmAgFnP	zemřít
na	na	k7c4	na
tyfus	tyfus	k1gInSc4	tyfus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
Pasteura	Pasteur	k1gMnSc4	Pasteur
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
léčby	léčba	k1gFnSc2	léčba
tyfu	tyf	k1gInSc2	tyf
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc4	titul
profesora	profesor	k1gMnSc2	profesor
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
objevem	objev	k1gInSc7	objev
disymetrie	disymetrie	k1gFnSc2	disymetrie
molekul	molekula	k1gFnPc2	molekula
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
chiralita	chiralita	k1gFnSc1	chiralita
<g/>
)	)	kIx)	)
kyseliny	kyselina	k1gFnSc2	kyselina
hroznové	hroznový	k2eAgFnPc1d1	Hroznová
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
založil	založit	k5eAaPmAgInS	založit
nový	nový	k2eAgInSc1d1	nový
vědecký	vědecký	k2eAgInSc1d1	vědecký
obor	obor	k1gInSc1	obor
–	–	k?	–
stereochemii	stereochemie	k1gFnSc4	stereochemie
<g/>
.	.	kIx.	.
</s>
<s>
Při	pře	k1gFnSc4	pře
zkoumání	zkoumání	k1gNnSc2	zkoumání
tohoto	tento	k3xDgInSc2	tento
fenoménu	fenomén	k1gInSc2	fenomén
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
a	a	k8xC	a
přítelem	přítel	k1gMnSc7	přítel
Biota	Biot	k1gInSc2	Biot
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc1	objev
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojené	spojený	k2eAgNnSc4d1	spojené
konzultoval	konzultovat	k5eAaImAgMnS	konzultovat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
výzkumy	výzkum	k1gInPc4	výzkum
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c4	v
oblasti	oblast	k1gFnPc4	oblast
mléčného	mléčný	k2eAgNnSc2d1	mléčné
<g/>
,	,	kIx,	,
octového	octový	k2eAgNnSc2d1	octové
a	a	k8xC	a
alkoholového	alkoholový	k2eAgNnSc2d1	alkoholové
kvašení	kvašení	k1gNnSc2	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvašení	kvašení	k1gNnSc1	kvašení
je	být	k5eAaImIp3nS	být
životní	životní	k2eAgInSc4d1	životní
projev	projev	k1gInSc4	projev
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
že	že	k8xS	že
různé	různý	k2eAgInPc1d1	různý
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
kvašení	kvašení	k1gNnSc2	kvašení
<g/>
,	,	kIx,	,
a	a	k8xC	a
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
metodu	metoda	k1gFnSc4	metoda
tepelné	tepelný	k2eAgFnSc2d1	tepelná
sterilace	sterilace	k1gFnSc2	sterilace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
nežádoucímu	žádoucí	k2eNgNnSc3d1	nežádoucí
kvašení	kvašení	k1gNnSc3	kvašení
potravin	potravina	k1gFnPc2	potravina
–	–	k?	–
tzv.	tzv.	kA	tzv.
pasterace	pasterace	k1gFnSc2	pasterace
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
hniloba	hniloba	k1gFnSc1	hniloba
a	a	k8xC	a
zánět	zánět	k1gInSc1	zánět
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
rovněž	rovněž	k6eAd1	rovněž
živými	živý	k2eAgInPc7d1	živý
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
(	(	kIx(	(
<g/>
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
Joseph	Joseph	k1gMnSc1	Joseph
Lister	Lister	k1gMnSc1	Lister
založil	založit	k5eAaPmAgMnS	založit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
práci	práce	k1gFnSc4	práce
teorii	teorie	k1gFnSc4	teorie
aseptické	aseptický	k2eAgFnSc2d1	aseptická
operace	operace	k1gFnSc2	operace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrátil	vyvrátit	k5eAaPmAgMnS	vyvrátit
též	též	k9	též
teorii	teorie	k1gFnSc4	teorie
abiogeneze	abiogeneze	k1gFnSc2	abiogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
následovalo	následovat	k5eAaImAgNnS	následovat
pověření	pověření	k1gNnSc1	pověření
profesora	profesor	k1gMnSc2	profesor
Pasteura	Pasteur	k1gMnSc2	Pasteur
vyšetřením	vyšetření	k1gNnSc7	vyšetření
tzv.	tzv.	kA	tzv.
bourcového	bourcový	k2eAgInSc2d1	bourcový
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kosil	kosit	k5eAaImAgInS	kosit
hedvábnický	hedvábnický	k2eAgInSc1d1	hedvábnický
průmysl	průmysl	k1gInSc1	průmysl
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
těžké	těžký	k2eAgFnSc3d1	těžká
mozkové	mozkový	k2eAgFnSc3d1	mozková
mrtvici	mrtvice	k1gFnSc3	mrtvice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bádání	bádání	k1gNnSc2	bádání
utrpěl	utrpět	k5eAaPmAgInS	utrpět
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
moru	mor	k1gInSc2	mor
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
a	a	k8xC	a
stanovil	stanovit	k5eAaPmAgMnS	stanovit
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgMnS	prosadit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
zásady	zásada	k1gFnSc2	zásada
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zamezit	zamezit	k5eAaPmF	zamezit
jeho	jeho	k3xOp3gNnSc4	jeho
šíření	šíření	k1gNnSc4	šíření
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
později	pozdě	k6eAd2	pozdě
převzaly	převzít	k5eAaPmAgInP	převzít
i	i	k9	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
věnoval	věnovat	k5eAaImAgMnS	věnovat
výzkumu	výzkum	k1gInSc2	výzkum
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
infekčních	infekční	k2eAgFnPc2d1	infekční
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
prevenci	prevence	k1gFnSc4	prevence
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
lékařem	lékař	k1gMnSc7	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
vakcínu	vakcína	k1gFnSc4	vakcína
proti	proti	k7c3	proti
nějaké	nějaký	k3yIgFnSc3	nějaký
chorobě	choroba	k1gFnSc3	choroba
z	z	k7c2	z
původce	původce	k1gMnSc2	původce
choroby	choroba	k1gFnSc2	choroba
samého	samý	k3xTgMnSc4	samý
a	a	k8xC	a
ustavil	ustavit	k5eAaPmAgMnS	ustavit
zásady	zásada	k1gFnPc4	zásada
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
postupovat	postupovat	k5eAaImF	postupovat
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
a	a	k8xC	a
prováděl	provádět	k5eAaImAgMnS	provádět
očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
anthraxu	anthrax	k1gInSc3	anthrax
<g/>
,	,	kIx,	,
slepičí	slepičit	k5eAaImIp3nS	slepičit
choleře	cholera	k1gFnSc3	cholera
a	a	k8xC	a
prasečímu	prasečí	k2eAgInSc3d1	prasečí
moru	mor	k1gInSc3	mor
<g/>
.	.	kIx.	.
</s>
<s>
Stanovil	stanovit	k5eAaPmAgMnS	stanovit
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgMnS	prosadit
nové	nový	k2eAgFnPc4d1	nová
a	a	k8xC	a
přísnější	přísný	k2eAgFnPc4d2	přísnější
normy	norma	k1gFnPc4	norma
pro	pro	k7c4	pro
zacházení	zacházení	k1gNnSc4	zacházení
s	s	k7c7	s
dobytkem	dobytek	k1gMnSc7	dobytek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c4	na
anthrax	anthrax	k1gInSc4	anthrax
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poprvé	poprvé	k6eAd1	poprvé
provedl	provést	k5eAaPmAgInS	provést
očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
vzteklině	vzteklina	k1gFnSc3	vzteklina
(	(	kIx(	(
<g/>
po	po	k7c6	po
předchozích	předchozí	k2eAgInPc6d1	předchozí
pokusech	pokus	k1gInPc6	pokus
na	na	k7c6	na
psech	pes	k1gMnPc6	pes
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
vlastní	vlastní	k2eAgFnSc6d1	vlastní
osobě	osoba	k1gFnSc6	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
ustavený	ustavený	k2eAgInSc4d1	ustavený
postup	postup	k1gInSc4	postup
výroby	výroba	k1gFnSc2	výroba
vakcíny	vakcína	k1gFnSc2	vakcína
vysoušením	vysoušení	k1gNnSc7	vysoušení
králičí	králičí	k2eAgFnSc2d1	králičí
míchy	mícha	k1gFnSc2	mícha
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
používal	používat	k5eAaImAgInS	používat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
Pasteurův	Pasteurův	k2eAgInSc1d1	Pasteurův
ústav	ústav	k1gInSc1	ústav
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dodnes	dodnes	k6eAd1	dodnes
představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
mikrobiologického	mikrobiologický	k2eAgInSc2d1	mikrobiologický
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
byl	být	k5eAaImAgInS	být
skalním	skalní	k2eAgMnSc7d1	skalní
a	a	k8xC	a
zbožným	zbožný	k2eAgMnSc7d1	zbožný
katolíkem	katolík	k1gMnSc7	katolík
a	a	k8xC	a
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
debat	debata	k1gFnPc2	debata
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
ateismu	ateismus	k1gInSc3	ateismus
šířícímu	šířící	k2eAgInSc3d1	šířící
se	se	k3xPyFc4	se
po	po	k7c6	po
francouzských	francouzský	k2eAgFnPc6d1	francouzská
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
prohlašujícímu	prohlašující	k2eAgInSc3d1	prohlašující
víru	vír	k1gInSc3	vír
v	v	k7c6	v
Boha	bůh	k1gMnSc2	bůh
za	za	k7c4	za
nevědeckou	vědecký	k2eNgFnSc4d1	nevědecká
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
citován	citován	k2eAgMnSc1d1	citován
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInPc7	jeho
výrok	výrok	k1gInSc4	výrok
<g/>
,	,	kIx,	,
stavící	stavící	k2eAgFnPc4d1	stavící
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
údajnému	údajný	k2eAgInSc3d1	údajný
rozporu	rozpor	k1gInSc3	rozpor
mezi	mezi	k7c7	mezi
vědou	věda	k1gFnSc7	věda
a	a	k8xC	a
vírou	víra	k1gFnSc7	víra
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Poněvadž	poněvadž	k8xS	poněvadž
jsem	být	k5eAaImIp1nS	být
hodně	hodně	k6eAd1	hodně
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
věřím	věřit	k5eAaImIp1nS	věřit
jako	jako	k9	jako
bretoňský	bretoňský	k2eAgInSc1d1	bretoňský
sedlák	sedlák	k1gInSc1	sedlák
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
byl	být	k5eAaImAgInS	být
studoval	studovat	k5eAaImAgInS	studovat
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
věřil	věřit	k5eAaImAgMnS	věřit
bych	by	kYmCp1nS	by
jako	jako	k9	jako
bretoňská	bretoňský	k2eAgFnSc1d1	bretoňská
selka	selka	k1gFnSc1	selka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
František	františek	k1gInSc1	františek
Gel	gel	k1gInSc1	gel
<g/>
:	:	kIx,	:
Přemožitel	přemožitel	k1gMnSc1	přemožitel
neviditelných	viditelný	k2eNgMnPc2d1	Neviditelný
dravců	dravec	k1gMnPc2	dravec
–	–	k?	–
životopis	životopis	k1gInSc1	životopis
Pasteura	Pasteur	k1gMnSc2	Pasteur
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgNnSc2	který
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc1d1	natočen
film	film	k1gInSc1	film
slovenské	slovenský	k2eAgFnSc2d1	slovenská
televize	televize	k1gFnSc2	televize
</s>
