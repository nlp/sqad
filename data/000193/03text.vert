<s>
Injekční	injekční	k2eAgInPc1d1	injekční
přípravky	přípravek	k1gInPc1	přípravek
jsou	být	k5eAaImIp3nP	být
sterilní	sterilní	k2eAgInPc1d1	sterilní
tekuté	tekutý	k2eAgInPc1d1	tekutý
přípravky	přípravek	k1gInPc1	přípravek
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
parenterálnímu	parenterální	k2eAgNnSc3d1	parenterální
podání	podání	k1gNnSc3	podání
injekční	injekční	k2eAgInSc4d1	injekční
jehlou	jehla	k1gFnSc7	jehla
na	na	k7c6	na
stříkačce	stříkačka	k1gFnSc6	stříkačka
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInSc7d1	jiný
vhodným	vhodný	k2eAgInSc7d1	vhodný
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
např.	např.	kA	např.
očkovací	očkovací	k2eAgFnSc7d1	očkovací
pistolí	pistol	k1gFnSc7	pistol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sterilní	sterilní	k2eAgFnPc1d1	sterilní
tuhé	tuhý	k2eAgFnPc1d1	tuhá
látky	látka	k1gFnPc1	látka
nebo	nebo	k8xC	nebo
výlisky	výlisek	k1gInPc1	výlisek
(	(	kIx(	(
<g/>
tablety	tableta	k1gFnPc1	tableta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
injekční	injekční	k2eAgInSc1d1	injekční
roztok	roztok	k1gInSc1	roztok
připraví	připravit	k5eAaPmIp3nS	připravit
přidáním	přidání	k1gNnSc7	přidání
předepsané	předepsaný	k2eAgFnSc2d1	předepsaná
tekutiny	tekutina	k1gFnSc2	tekutina
v	v	k7c4	v
čas	čas	k1gInSc4	čas
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
rozředěním	rozředění	k1gNnSc7	rozředění
<g/>
.	.	kIx.	.
</s>
<s>
Injekce	injekce	k1gFnSc1	injekce
jako	jako	k8xC	jako
způsob	způsob	k1gInSc1	způsob
aplikace	aplikace	k1gFnSc2	aplikace
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
aplikace	aplikace	k1gFnSc1	aplikace
léčiva	léčivo	k1gNnSc2	léčivo
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
řádově	řádově	k6eAd1	řádově
do	do	k7c2	do
30	[number]	k4	30
ml	ml	kA	ml
<g/>
,	,	kIx,	,
najednou	najednou	k6eAd1	najednou
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
časovém	časový	k2eAgInSc6d1	časový
intervalu	interval	k1gInSc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
roztoky	roztok	k1gInPc1	roztok
<g/>
,	,	kIx,	,
suspenze	suspenze	k1gFnPc1	suspenze
a	a	k8xC	a
emulze	emulze	k1gFnPc1	emulze
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
Williamem	William	k1gInSc7	William
Harveyem	Harveyem	k1gInSc1	Harveyem
(	(	kIx(	(
<g/>
1578	[number]	k4	1578
<g/>
-	-	kIx~	-
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1616	[number]	k4	1616
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
aplikací	aplikace	k1gFnSc7	aplikace
injekcí	injekce	k1gFnSc7	injekce
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
intravenózní	intravenózní	k2eAgFnSc1d1	intravenózní
injekce	injekce	k1gFnSc1	injekce
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1658	[number]	k4	1658
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
technicky	technicky	k6eAd1	technicky
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
první	první	k4xOgFnSc7	první
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
injekční	injekční	k2eAgFnSc7d1	injekční
stříkačkou	stříkačka	k1gFnSc7	stříkačka
zavedl	zavést	k5eAaPmAgInS	zavést
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chirurg	chirurg	k1gMnSc1	chirurg
Pravaz	Pravaz	k1gInSc4	Pravaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k2eAgMnSc1d1	skotský
lékař	lékař	k1gMnSc1	lékař
Wood	Wood	k1gMnSc1	Wood
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1853	[number]	k4	1853
první	první	k4xOgFnSc4	první
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
aplikaci	aplikace	k1gFnSc4	aplikace
jehlou	jehla	k1gFnSc7	jehla
do	do	k7c2	do
podkoží	podkoží	k1gNnSc2	podkoží
<g/>
.	.	kIx.	.
</s>
<s>
Injekční	injekční	k2eAgFnSc4d1	injekční
stříkačku	stříkačka	k1gFnSc4	stříkačka
používal	používat	k5eAaImAgMnS	používat
ruský	ruský	k2eAgMnSc1d1	ruský
lékař	lékař	k1gMnSc1	lékař
Lazarov	Lazarovo	k1gNnPc2	Lazarovo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
aplikace	aplikace	k1gFnSc2	aplikace
přispěli	přispět	k5eAaPmAgMnP	přispět
také	také	k9	také
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
Louis	Louis	k1gMnSc1	Louis
Pasteur	Pasteur	k1gMnSc1	Pasteur
a	a	k8xC	a
německý	německý	k2eAgMnSc1d1	německý
lékař	lékař	k1gMnSc1	lékař
Robert	Robert	k1gMnSc1	Robert
Koch	Koch	k1gMnSc1	Koch
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zdokonalili	zdokonalit	k5eAaPmAgMnP	zdokonalit
způsoby	způsob	k1gInPc4	způsob
sterilizace	sterilizace	k1gFnSc2	sterilizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
pařížský	pařížský	k2eAgMnSc1d1	pařížský
lékárník	lékárník	k1gMnSc1	lékárník
Limousin	Limousin	k1gMnSc1	Limousin
zavedl	zavést	k5eAaPmAgMnS	zavést
jako	jako	k9	jako
vhodný	vhodný	k2eAgInSc4d1	vhodný
obal	obal	k1gInSc4	obal
pro	pro	k7c4	pro
injekce	injekce	k1gFnPc4	injekce
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
ampuli	ampule	k1gFnSc4	ampule
<g/>
.	.	kIx.	.
</s>
<s>
Parenterální	parenterální	k2eAgFnPc1d1	parenterální
aplikace	aplikace	k1gFnPc1	aplikace
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejúčinnější	účinný	k2eAgInPc4d3	nejúčinnější
způsoby	způsob	k1gInPc4	způsob
podávání	podávání	k1gNnSc3	podávání
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Výhody	výhoda	k1gFnPc1	výhoda
použití	použití	k1gNnSc2	použití
<g/>
:	:	kIx,	:
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
aplikací	aplikace	k1gFnPc2	aplikace
dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
rychlého	rychlý	k2eAgInSc2d1	rychlý
účinku	účinek	k1gInSc2	účinek
(	(	kIx(	(
<g/>
křeče	křeč	k1gFnPc1	křeč
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
<g/>
,	,	kIx,	,
šok	šok	k1gInSc1	šok
<g/>
,	,	kIx,	,
zástava	zástava	k1gFnSc1	zástava
srdce	srdce	k1gNnSc2	srdce
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
<g/>
-li	i	k?	-li
aplikovat	aplikovat	k5eAaBmF	aplikovat
léčivo	léčivo	k1gNnSc4	léčivo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
biodegradace	biodegradace	k1gFnSc2	biodegradace
v	v	k7c6	v
trávicím	trávicí	k2eAgInSc6d1	trávicí
traktu	trakt	k1gInSc6	trakt
(	(	kIx(	(
<g/>
insulin	insulin	k1gInSc1	insulin
<g/>
)	)	kIx)	)
stavy	stav	k1gInPc1	stav
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
trávicího	trávicí	k2eAgNnSc2d1	trávicí
ústrojí	ústrojí	k1gNnSc2	ústrojí
Injekční	injekční	k2eAgFnSc2d1	injekční
přípravky	přípravka	k1gFnSc2	přípravka
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
disperzní	disperzní	k2eAgFnSc2d1	disperzní
soustavy	soustava	k1gFnSc2	soustava
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
na	na	k7c6	na
<g/>
:	:	kIx,	:
čiré	čirý	k2eAgFnSc6d1	čirá
zakalené	zakalený	k2eAgFnSc6d1	zakalená
práškovité	práškovitý	k2eAgFnSc6d1	práškovitá
lyofilizované	lyofilizovaný	k2eAgFnSc6d1	lyofilizovaná
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
pórovité	pórovitý	k2eAgFnSc2d1	pórovitá
hmoty	hmota	k1gFnSc2	hmota
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
mrazovou	mrazový	k2eAgFnSc7d1	mrazová
sublimací	sublimace	k1gFnSc7	sublimace
suspenzní	suspenzeň	k1gFnPc2	suspenzeň
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
velikost	velikost	k1gFnSc4	velikost
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
emulze	emulze	k1gFnSc1	emulze
forma	forma	k1gFnSc1	forma
emulze	emulze	k1gFnSc1	emulze
musí	muset	k5eAaImIp3nS	muset
vydržet	vydržet	k5eAaPmF	vydržet
alespoň	alespoň	k9	alespoň
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
po	po	k7c6	po
protřepání	protřepání	k1gNnSc6	protřepání
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
injekce	injekce	k1gFnSc2	injekce
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pro	pro	k7c4	pro
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
:	:	kIx,	:
sterilita	sterilita	k1gFnSc1	sterilita
injekce	injekce	k1gFnSc2	injekce
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
nejvýše	vysoce	k6eAd3	vysoce
předepsané	předepsaný	k2eAgNnSc4d1	předepsané
množství	množství	k1gNnSc4	množství
živých	živý	k2eAgMnPc2d1	živý
či	či	k8xC	či
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
apyrogenita	apyrogenita	k1gFnSc1	apyrogenita
injekce	injekce	k1gFnSc2	injekce
nesmí	smět	k5eNaImIp3nS	smět
obsahovat	obsahovat	k5eAaImF	obsahovat
pyrogenní	pyrogenní	k2eAgFnPc4d1	pyrogenní
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
metabolické	metabolický	k2eAgInPc1d1	metabolický
produkty	produkt	k1gInPc1	produkt
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
aplikaci	aplikace	k1gFnSc6	aplikace
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
horečku	horečka	k1gFnSc4	horečka
<g/>
.	.	kIx.	.
izotonické	izotonický	k2eAgInPc1d1	izotonický
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc4d1	stejný
osmotický	osmotický	k2eAgInSc4d1	osmotický
tlak	tlak	k1gInSc4	tlak
jako	jako	k8xS	jako
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
injekcí	injekce	k1gFnPc2	injekce
používají	používat	k5eAaImIp3nP	používat
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
organická	organický	k2eAgNnPc1d1	organické
rozpouštědla	rozpouštědlo	k1gNnPc1	rozpouštědlo
<g/>
:	:	kIx,	:
Voda	voda	k1gFnSc1	voda
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
injekční	injekční	k2eAgFnSc2d1	injekční
roztoků	roztok	k1gInPc2	roztok
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
injekční	injekční	k2eAgFnSc1d1	injekční
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
aqua	aqua	k1gFnSc1	aqua
pro	pro	k7c4	pro
injectione	injection	k1gInSc5	injection
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
redestilovaná	redestilovaný	k2eAgFnSc1d1	redestilovaný
nebo	nebo	k8xC	nebo
voda	voda	k1gFnSc1	voda
zbavená	zbavený	k2eAgFnSc1d1	zbavená
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
destilovaná	destilovaný	k2eAgFnSc1d1	destilovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pomocí	pomocí	k7c2	pomocí
reverzní	reverzní	k2eAgFnSc2d1	reverzní
osmózy	osmóza	k1gFnSc2	osmóza
<g/>
.	.	kIx.	.
</s>
<s>
Reverzní	reverzní	k2eAgFnSc1d1	reverzní
osmóza	osmóza	k1gFnSc1	osmóza
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
pronikají	pronikat	k5eAaImIp3nP	pronikat
přes	přes	k7c4	přes
polopropustnou	polopropustný	k2eAgFnSc4d1	polopropustná
membránou	membrána	k1gFnSc7	membrána
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
za	za	k7c4	za
využití	využití	k1gNnSc4	využití
osmotického	osmotický	k2eAgInSc2d1	osmotický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Hydrofobní	hydrofobní	k2eAgNnPc1d1	hydrofobní
rozpouštědla	rozpouštědlo	k1gNnPc1	rozpouštědlo
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jako	jako	k9	jako
rozpouštědla	rozpouštědlo	k1gNnPc1	rozpouštědlo
používají	používat	k5eAaImIp3nP	používat
rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
oleje	olej	k1gInPc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
slunečnicový	slunečnicový	k2eAgMnSc1d1	slunečnicový
nebo	nebo	k8xC	nebo
olivový	olivový	k2eAgMnSc1d1	olivový
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
rozpustnost	rozpustnost	k1gFnSc4	rozpustnost
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
také	také	k9	také
ethanol	ethanol	k1gInSc4	ethanol
40	[number]	k4	40
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
glycerol	glycerol	k1gInSc1	glycerol
<g/>
,	,	kIx,	,
butandiol	butandiol	k1gInSc1	butandiol
<g/>
,	,	kIx,	,
propylenglykol	propylenglykol	k1gInSc1	propylenglykol
a	a	k8xC	a
makrogoly	makrogol	k1gInPc1	makrogol
<g/>
.	.	kIx.	.
ethanol	ethanol	k1gInSc1	ethanol
40	[number]	k4	40
<g/>
%	%	kIx~	%
propylenglykol	propylenglykol	k1gInSc1	propylenglykol
60	[number]	k4	60
<g/>
%	%	kIx~	%
makrogoly	makrogola	k1gFnSc2	makrogola
do	do	k7c2	do
60	[number]	k4	60
<g/>
%	%	kIx~	%
glycerol	glycerol	k1gInSc1	glycerol
dimethylformamid	dimethylformamid	k1gInSc1	dimethylformamid
dimetylacetamid	dimetylacetamida	k1gFnPc2	dimetylacetamida
butandiol	butandiola	k1gFnPc2	butandiola
olivový	olivový	k2eAgInSc4d1	olivový
olej	olej	k1gInSc4	olej
a	a	k8xC	a
slunečnicový	slunečnicový	k2eAgInSc4d1	slunečnicový
olej	olej	k1gInSc4	olej
Úprava	úprava	k1gFnSc1	úprava
osmózy	osmóza	k1gFnSc2	osmóza
injekčního	injekční	k2eAgInSc2d1	injekční
roztoku	roztok	k1gInSc2	roztok
Osmotický	osmotický	k2eAgInSc1d1	osmotický
tlak	tlak	k1gInSc1	tlak
injekčního	injekční	k2eAgInSc2d1	injekční
roztoku	roztok	k1gInSc2	roztok
upravujeme	upravovat	k5eAaImIp1nP	upravovat
pomocí	pomocí	k7c2	pomocí
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Acidita	acidita	k1gFnSc1	acidita
injekcí	injekce	k1gFnPc2	injekce
Pokud	pokud	k8xS	pokud
injekční	injekční	k2eAgInSc1d1	injekční
roztok	roztok	k1gInSc1	roztok
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
stejné	stejný	k2eAgFnPc4d1	stejná
kyselosti	kyselost	k1gFnPc4	kyselost
jako	jako	k8xS	jako
má	můj	k3xOp1gFnSc1	můj
krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
ji	on	k3xPp3gFnSc4	on
nutno	nutno	k6eAd1	nutno
upravit	upravit	k5eAaPmF	upravit
pomocí	pomocí	k7c2	pomocí
vhodných	vhodný	k2eAgFnPc2d1	vhodná
přísad	přísada	k1gFnPc2	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgFnPc4	takový
přísady	přísada	k1gFnPc4	přísada
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
sůl	sůl	k1gFnSc1	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
citrónové	citrónový	k2eAgFnSc2d1	citrónová
sůl	sůl	k1gFnSc4	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
trihydrogenfosforečné	trihydrogenfosforečný	k2eAgFnSc2d1	trihydrogenfosforečná
Injekční	injekční	k2eAgFnSc2d1	injekční
přípravky	přípravka	k1gFnSc2	přípravka
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
pouze	pouze	k6eAd1	pouze
průmyslově	průmyslově	k6eAd1	průmyslově
<g/>
.	.	kIx.	.
</s>
<s>
Plnění	plnění	k1gNnSc1	plnění
ampulí	ampule	k1gFnPc2	ampule
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
sterilních	sterilní	k2eAgFnPc6d1	sterilní
místnostech	místnost	k1gFnPc6	místnost
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
speciálně	speciálně	k6eAd1	speciálně
upravených	upravený	k2eAgInPc6d1	upravený
oblecích	oblek	k1gInPc6	oblek
hygienickou	hygienický	k2eAgFnSc7d1	hygienická
smyčkou	smyčka	k1gFnSc7	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Hygienická	hygienický	k2eAgFnSc1d1	hygienická
smyčka	smyčka	k1gFnSc1	smyčka
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracovník	pracovník	k1gMnSc1	pracovník
projde	projít	k5eAaPmIp3nS	projít
předepsanou	předepsaný	k2eAgFnSc7d1	předepsaná
očistou	očista	k1gFnSc7	očista
a	a	k8xC	a
podrobí	podrobit	k5eAaPmIp3nS	podrobit
se	se	k3xPyFc4	se
desinfekci	desinfekce	k1gFnSc3	desinfekce
<g/>
.	.	kIx.	.
</s>
<s>
Sterilní	sterilní	k2eAgFnSc1d1	sterilní
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
čistá	čistý	k2eAgFnSc1d1	čistá
místnost	místnost	k1gFnSc1	místnost
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
klimatizací	klimatizace	k1gFnPc2	klimatizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
redukovat	redukovat	k5eAaBmF	redukovat
počet	počet	k1gInSc1	počet
částic	částice	k1gFnPc2	částice
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
teplotu	teplota	k1gFnSc4	teplota
místnosti	místnost	k1gFnSc2	místnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
panuje	panovat	k5eAaImIp3nS	panovat
přetlak	přetlak	k1gInSc1	přetlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zamezí	zamezit	k5eAaPmIp3nS	zamezit
průniku	průnik	k1gInSc3	průnik
částic	částice	k1gFnPc2	částice
vzduchu	vzduch	k1gInSc2	vzduch
do	do	k7c2	do
čistého	čistý	k2eAgInSc2d1	čistý
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
výroba	výroba	k1gFnSc1	výroba
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
stupňů	stupeň	k1gInPc2	stupeň
<g/>
:	:	kIx,	:
Příprava	příprava	k1gFnSc1	příprava
roztoku	roztok	k1gInSc2	roztok
s	s	k7c7	s
léčivem	léčivo	k1gNnSc7	léčivo
a	a	k8xC	a
sterilizace	sterilizace	k1gFnSc1	sterilizace
ampulí	ampule	k1gFnPc2	ampule
plnění	plnění	k1gNnSc2	plnění
sterilizace	sterilizace	k1gFnSc2	sterilizace
kontrola	kontrola	k1gFnSc1	kontrola
těsnosti	těsnost	k1gFnSc2	těsnost
ampule	ampule	k1gFnSc1	ampule
adjustace	adjustace	k1gFnSc1	adjustace
/	/	kIx~	/
balení	balení	k1gNnSc1	balení
<g/>
/	/	kIx~	/
Pro	pro	k7c4	pro
zkoušení	zkoušení	k1gNnSc4	zkoušení
injekčních	injekční	k2eAgInPc2d1	injekční
roztoků	roztok	k1gInPc2	roztok
provádíme	provádět	k5eAaImIp1nP	provádět
tyto	tento	k3xDgFnPc4	tento
speciální	speciální	k2eAgFnPc4d1	speciální
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
:	:	kIx,	:
objem	objem	k1gInSc1	objem
kapaliny	kapalina	k1gFnSc2	kapalina
v	v	k7c6	v
ampuli	ampule	k1gFnSc6	ampule
hmotnostní	hmotnostní	k2eAgFnSc1d1	hmotnostní
stejnoměrnost	stejnoměrnost	k1gFnSc1	stejnoměrnost
pyrogenitu	pyrogenit	k1gInSc2	pyrogenit
zkouška	zkouška	k1gFnSc1	zkouška
těsnosti	těsnost	k1gFnSc2	těsnost
ampulí	ampule	k1gFnPc2	ampule
ampule	ampule	k1gFnSc2	ampule
vložíme	vložit	k5eAaPmIp1nP	vložit
do	do	k7c2	do
vakuové	vakuový	k2eAgFnSc2d1	vakuová
skříně	skříň	k1gFnSc2	skříň
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
ponoříme	ponořit	k5eAaPmIp1nP	ponořit
do	do	k7c2	do
barevného	barevný	k2eAgInSc2d1	barevný
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
tlak	tlak	k1gInSc1	tlak
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ampule	ampule	k1gFnPc4	ampule
vyjmeme	vyjmout	k5eAaPmIp1nP	vyjmout
<g/>
,	,	kIx,	,
omyjeme	omýt	k5eAaPmIp1nP	omýt
a	a	k8xC	a
porovnáváme	porovnávat	k5eAaImIp1nP	porovnávat
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Injekční	injekční	k2eAgInPc1d1	injekční
roztoky	roztok	k1gInPc1	roztok
se	se	k3xPyFc4	se
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
<g/>
:	:	kIx,	:
intravenózně	intravenózně	k6eAd1	intravenózně
(	(	kIx(	(
<g/>
i.	i.	k?	i.
v.	v.	k?	v.
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
žíly	žíla	k1gFnSc2	žíla
<g/>
;	;	kIx,	;
intramuskulárně	intramuskulárně	k6eAd1	intramuskulárně
(	(	kIx(	(
<g/>
i.	i.	k?	i.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
svalu	sval	k1gInSc2	sval
<g/>
;	;	kIx,	;
subkutánně	subkutánně	k6eAd1	subkutánně
(	(	kIx(	(
<g/>
s.	s.	k?	s.
c.	c.	k?	c.
<g/>
)	)	kIx)	)
-	-	kIx~	-
pod	pod	k7c4	pod
kůži	kůže	k1gFnSc4	kůže
<g/>
;	;	kIx,	;
submukózně	submukózně	k6eAd1	submukózně
-	-	kIx~	-
pod	pod	k7c4	pod
sliznici	sliznice	k1gFnSc4	sliznice
<g/>
;	;	kIx,	;
intraartikulárně	intraartikulárně	k6eAd1	intraartikulárně
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
i.	i.	k?	i.
a.	a.	k?	a.
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
kloubů	kloub	k1gInPc2	kloub
<g/>
;	;	kIx,	;
periartikulárně	periartikulárně	k6eAd1	periartikulárně
(	(	kIx(	(
<g/>
p.	p.	k?	p.
a.	a.	k?	a.
<g/>
)	)	kIx)	)
-	-	kIx~	-
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
kloubů	kloub	k1gInPc2	kloub
<g/>
;	;	kIx,	;
intrathekálně	intrathekálně	k6eAd1	intrathekálně
-	-	kIx~	-
do	do	k7c2	do
páteřního	páteřní	k2eAgInSc2d1	páteřní
kanálu	kanál	k1gInSc2	kanál
<g/>
;	;	kIx,	;
epidurálně	epidurálně	k6eAd1	epidurálně
-	-	kIx~	-
do	do	k7c2	do
prostotu	prostota	k1gFnSc4	prostota
míšních	míšní	k2eAgInPc2d1	míšní
obalů	obal	k1gInPc2	obal
<g/>
;	;	kIx,	;
periorbitálně	periorbitálně	k6eAd1	periorbitálně
-	-	kIx~	-
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
očnice	očnice	k1gFnSc2	očnice
<g/>
;	;	kIx,	;
intraarterialní-	intraarterialní-	k?	intraarterialní-
<g/>
(	(	kIx(	(
<g/>
i.a	i.a	k?	i.a
<g/>
)	)	kIx)	)
do	do	k7c2	do
tepny	tepna	k1gFnSc2	tepna
intrakardiální-	intrakardiální-	k?	intrakardiální-
<g/>
(	(	kIx(	(
<g/>
i.c	i.c	k?	i.c
<g/>
)	)	kIx)	)
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Injekce	injekce	k1gFnSc2	injekce
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
injekce	injekce	k1gFnSc2	injekce
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
