<p>
<s>
Ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6	Bratislava
je	být	k5eAaImIp3nS
synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2
roku	rok	k1gInSc2	rok
1926	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
Heydukově	Heydukův	k2eAgFnSc6d1	Heydukova
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?
11	#num#	k4
v	v	k7c6
bratislavském	bratislavský	k2eAgNnSc6d1	Bratislavské
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
a	a	k8xC
je	být	k5eAaImIp3nS
dílem	dílo	k1gNnSc7	dílo
architekta	architekt	k1gMnSc2	architekt
Artura	Artur	k1gMnSc2	Artur
Szalatnaie	Szalatnaie	k1gFnSc2	Szalatnaie
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
Ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
synagoga	synagoga	k1gFnSc1	synagoga
patří	patřit	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
žilinskou	žilinský	k2eAgFnSc7d1	Žilinská
neologickou	neologický	k2eAgFnSc7d1	neologická
synagogou	synagoga	k1gFnSc7	synagoga
k	k	k7c3
významným	významný	k2eAgFnPc3d1	významná
slovenským	slovenský	k2eAgFnPc3d1	slovenská
sakrálním	sakrální	k2eAgFnPc3d1	sakrální
stavbám	stavba	k1gFnPc3	stavba
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojuje	spojovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
modernost	modernost	k1gFnSc1	modernost
s	s	k7c7
orientální	orientální	k2eAgFnSc7d1	orientální
tradicí	tradice	k1gFnSc7	tradice
–	–	k?
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
použity	použít	k5eAaPmNgInP
stylizované	stylizovaný	k2eAgInPc1d1	stylizovaný
ornamenty	ornament	k1gInPc1	ornament
<g/>
,	,	kIx,
společně	společně	k6eAd1
se	s	k7c7
železobetonovou	železobetonový	k2eAgFnSc7d1	železobetonová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Popis	popis	k1gInSc4	popis
==	==	k?
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
vznikl	vzniknout	k5eAaPmAgInS
na	na	k7c6
základě	základ	k1gInSc6	základ
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
zvítězil	zvítězit	k5eAaPmAgMnS
Artúr	Artúr	k1gMnSc1	Artúr
Szalatnai	Szalatna	k1gFnSc2	Szalatna
<g/>
,	,	kIx,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
architekt	architekt	k1gMnSc1	architekt
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhl	navrhnout	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
jako	jako	k8xS,k8xC
první	první	k4xOgNnSc1
velké	velký	k2eAgNnSc1d1	velké
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
přišel	přijít	k5eAaPmAgMnS
z	z	k7c2
Budapešti	Budapešť	k1gFnSc2	Budapešť
do	do	k7c2
Bratislavy	Bratislava	k1gFnSc2	Bratislava
jako	jako	k8xC,k8xS
čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
absolvent	absolvent	k1gMnSc1	absolvent
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6	doba
na	na	k7c6
nové	nový	k2eAgFnSc6d1	nová
Heydukově	Heydukův	k2eAgFnSc6d1	Heydukova
ulici	ulice	k1gFnSc6	ulice
ještě	ještě	k6eAd1
neexistovaly	existovat	k5eNaImAgFnP
okolní	okolní	k2eAgFnPc1d1	okolní
stavby	stavba	k1gFnPc1	stavba
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monumentální	monumentální	k2eAgFnSc4d1	monumentální
budovu	budova	k1gFnSc4	budova
prostého	prostý	k2eAgInSc2d1	prostý
klasicizujícího	klasicizující	k2eAgInSc2d1	klasicizující
charakteru	charakter	k1gInSc2	charakter
architekt	architekt	k1gMnSc1	architekt
umístil	umístit	k5eAaPmAgMnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
uličního	uliční	k2eAgNnSc2d1	uliční
průčelí	průčelí	k1gNnSc2	průčelí
orientoval	orientovat	k5eAaBmAgInS
boční	boční	k2eAgFnSc4d1	boční
stěnu	stěna	k1gFnSc4	stěna
hlavního	hlavní	k2eAgInSc2d1	hlavní
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC
vstup	vstup	k1gInSc4	vstup
do	do	k7c2
objektu	objekt	k1gInSc2	objekt
umístil	umístit	k5eAaPmAgInS
směrem	směr	k1gInSc7	směr
na	na	k7c4
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,
do	do	k7c2
průjezdu	průjezd	k1gInSc2	průjezd
z	z	k7c2
ulice	ulice	k1gFnSc2	ulice
na	na	k7c4
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
objektu	objekt	k1gInSc6	objekt
jsou	být	k5eAaImIp3nP
po	po	k7c6
stranách	strana	k1gFnPc6	strana
vstupního	vstupní	k2eAgInSc2d1	vstupní
vestibulu	vestibul	k1gInSc2	vestibul
dvě	dva	k4xCgNnPc1
schodiště	schodiště	k1gNnSc1	schodiště
na	na	k7c4
galerii	galerie	k1gFnSc4	galerie
pro	pro	k7c4
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
tvoří	tvořit	k5eAaImIp3nS
sedm	sedm	k4xCc4
mohutných	mohutný	k2eAgInPc2d1	mohutný
hranolových	hranolový	k2eAgInPc2d1	hranolový
pilířů	pilíř	k1gInPc2	pilíř
<g/>
,	,	kIx,
zakončených	zakončený	k2eAgFnPc2d1	zakončená
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
hlavicemi	hlavice	k1gFnPc7	hlavice
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nesou	nést	k5eAaImIp3nP
nevysoký	vysoký	k2eNgInSc4d1	nevysoký
architráv	architráv	k1gInSc4	architráv
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stěně	stěna	k1gFnSc6	stěna
za	za	k7c7
pilíři	pilíř	k1gInPc7	pilíř
jsou	být	k5eAaImIp3nP
okna	okno	k1gNnPc4	okno
s	s	k7c7
geometrizovanou	geometrizovaný	k2eAgFnSc7d1	geometrizovaná
treláží	treláž	k1gFnSc7	treláž
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objekt	objekt	k1gInSc1	objekt
má	mít	k5eAaImIp3nS
travertinové	travertinový	k2eAgInPc4d1	travertinový
architektonické	architektonický	k2eAgInPc4d1	architektonický
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
kontrastují	kontrastovat	k5eAaImIp3nP
se	s	k7c7
stěnami	stěna	k1gFnPc7	stěna
žluté	žlutý	k2eAgFnSc2d1	žlutá
a	a	k8xC
hnědočervené	hnědočervený	k2eAgFnSc2d1	hnědočervená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pilíři	pilíř	k1gInSc6	pilíř
vpravo	vpravo	k6eAd1
od	od	k7c2
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2
podjezdu	podjezd	k1gInSc2	podjezd
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgMnS
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC
rok	rok	k1gInSc1	rok
vzniku	vznik	k1gInSc2	vznik
stavby	stavba	k1gFnSc2	stavba
podle	podle	k7c2
židovského	židovský	k2eAgInSc2d1	židovský
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?
</s>
</p>
<p>
<s>
===	===	k?
Reference	reference	k1gFnPc1	reference
===	===	k?
</s>
</p>
<p>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2
článku	článek	k1gInSc2	článek
Ortodoxná	Ortodoxný	k2eAgFnSc1d1	Ortodoxný
synagóga	synagóga	k1gFnSc1	synagóga
v	v	k7c6
Bratislave	Bratislav	k1gMnSc5	Bratislav
na	na	k7c6
slovenské	slovenský	k2eAgFnSc3d1	slovenská
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
===	===	k?
Literatura	literatura	k1gFnSc1	literatura
===	===	k?
</s>
</p>
<p>
<s>
Dulla	Dulla	k1gMnSc1	Dulla
Matúš	Matúš	k1gMnSc1	Matúš
<g/>
,	,	kIx,
Majstri	Majstr	k1gMnPc1	Majstr
architektúry	architektúra	k1gFnSc2	architektúra
<g/>
,	,	kIx,
Perfect	Perfect	k1gMnSc1	Perfect
<g/>
,	,	kIx,
str	str	kA
<g/>
,	,	kIx,
56	#num#	k4
-	-	kIx~
57	#num#	k4
</s>
</p>
<p>
<s>
Barkány	Barkán	k1gInPc1	Barkán
Eugen	Eugna	k1gFnPc2	Eugna
<g/>
,	,	kIx,
Židovske	Židovske	k1gFnSc1	Židovske
náboženské	náboženský	k2eAgFnSc2d1	náboženská
obce	obec	k1gFnSc2	obec
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,
Vesna	Vesna	k1gFnSc1	Vesna
<g/>
,	,	kIx,
str	str	kA
<g/>
.48	.48	k4
-	-	kIx~
49	#num#	k4
</s>
</p>
<p>
<s>
===	===	k?
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?
</s>
</p>
<p>
<s>
http://www.fa.stuba.sk/docs/udta/exkurzia.pdf	http://www.fa.stuba.sk/docs/udta/exkurzia.pdf	k1gMnSc1	http://www.fa.stuba.sk/docs/udta/exkurzia.pdf
</s>
</p>
