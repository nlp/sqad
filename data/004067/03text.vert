<s>
Vitamíny	vitamín	k1gInPc1	vitamín
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
vitaminy	vitamin	k1gInPc1	vitamin
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nízkomolekulární	nízkomolekulární	k2eAgFnPc1d1	nízkomolekulární
látky	látka	k1gFnPc1	látka
nezbytné	zbytný	k2eNgFnPc1d1	zbytný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
mají	mít	k5eAaImIp3nP	mít
vitamíny	vitamín	k1gInPc4	vitamín
funkci	funkce	k1gFnSc4	funkce
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
biochemických	biochemický	k2eAgFnPc2d1	biochemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Podílejí	podílet	k5eAaImIp3nP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
metabolismu	metabolismus	k1gInSc6	metabolismus
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
13	[number]	k4	13
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
organismus	organismus	k1gInSc1	organismus
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc4	vitamín
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	on	k3xPp3gInPc4	on
musí	muset	k5eAaImIp3nP	muset
získávat	získávat	k5eAaImF	získávat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
avitaminóze	avitaminóza	k1gFnSc6	avitaminóza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevovat	objevovat	k5eAaImF	objevovat
poruchy	poruch	k1gInPc1	poruch
funkcí	funkce	k1gFnPc2	funkce
organismu	organismus	k1gInSc2	organismus
nebo	nebo	k8xC	nebo
i	i	k9	i
vážná	vážný	k2eAgNnPc4d1	vážné
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Přebytečných	přebytečný	k2eAgInPc2d1	přebytečný
vitamínů	vitamín	k1gInPc2	vitamín
(	(	kIx(	(
<g/>
hypervitaminóza	hypervitaminóza	k1gFnSc1	hypervitaminóza
<g/>
)	)	kIx)	)
rozpustných	rozpustný	k2eAgInPc2d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
organismus	organismus	k1gInSc1	organismus
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zbavit	zbavit	k5eAaPmF	zbavit
a	a	k8xC	a
pokud	pokud	k8xS	pokud
přestaneme	přestat	k5eAaPmIp1nP	přestat
vitamín	vitamín	k1gInSc4	vitamín
přijímat	přijímat	k5eAaImF	přijímat
<g/>
,	,	kIx,	,
organismus	organismus	k1gInSc1	organismus
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
nadbytečné	nadbytečný	k2eAgNnSc1d1	nadbytečné
množství	množství	k1gNnSc1	množství
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
pomocí	pomocí	k7c2	pomocí
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vitamínů	vitamín	k1gInPc2	vitamín
rozpustných	rozpustný	k2eAgInPc2d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nefunguje	fungovat	k5eNaImIp3nS	fungovat
–	–	k?	–
nejrizikovější	rizikový	k2eAgMnPc4d3	nejrizikovější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
vitamín	vitamín	k1gInSc1	vitamín
A	a	k8xC	a
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgInSc2	jenž
existují	existovat	k5eAaImIp3nP	existovat
případy	případ	k1gInPc4	případ
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
otrav	otrava	k1gFnPc2	otrava
nebo	nebo	k8xC	nebo
otrav	otrava	k1gFnPc2	otrava
s	s	k7c7	s
doživotními	doživotní	k2eAgInPc7d1	doživotní
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
<s>
Vitamíny	vitamín	k1gInPc1	vitamín
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgInPc1d1	nutný
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
mnohých	mnohý	k2eAgFnPc2d1	mnohá
tělesných	tělesný	k2eAgFnPc2d1	tělesná
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
posilovat	posilovat	k5eAaImF	posilovat
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
imunitní	imunitní	k2eAgFnPc4d1	imunitní
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Vitamin	vitamin	k1gInSc4	vitamin
B1	B1	k1gFnSc2	B1
objevil	objevit	k5eAaPmAgMnS	objevit
polský	polský	k2eAgMnSc1d1	polský
biochemik	biochemik	k1gMnSc1	biochemik
Kazimierz	Kazimierz	k1gMnSc1	Kazimierz
Funk	funk	k1gInSc4	funk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
v	v	k7c6	v
otrubách	otruba	k1gFnPc6	otruba
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Funk	funk	k1gInSc4	funk
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
název	název	k1gInSc1	název
vitamín	vitamín	k1gInSc1	vitamín
podle	podle	k7c2	podle
latinského	latinský	k2eAgMnSc2d1	latinský
vital	vitat	k5eAaImAgMnS	vitat
a	a	k8xC	a
amine	amin	k1gInSc5	amin
=	=	kIx~	=
"	"	kIx"	"
<g/>
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc1d1	důležitý
aminy	amin	k1gInPc1	amin
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
podle	podle	k7c2	podle
dnešních	dnešní	k2eAgInPc2d1	dnešní
poznatků	poznatek	k1gInPc2	poznatek
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
aminy	amin	k1gInPc4	amin
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
podobné	podobný	k2eAgFnPc4d1	podobná
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
vitaminy	vitamin	k1gInPc4	vitamin
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
...	...	k?	...
K	K	kA	K
a	a	k8xC	a
pseudovitamíny	pseudovitamína	k1gFnSc2	pseudovitamína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
A	a	k9	a
(	(	kIx(	(
<g/>
retinol	retinol	k1gInSc1	retinol
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
(	(	kIx(	(
<g/>
kalciferol	kalciferol	k1gInSc1	kalciferol
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
slunce	slunce	k1gNnSc1	slunce
Vitamín	vitamín	k1gInSc1	vitamín
E	E	kA	E
(	(	kIx(	(
<g/>
tokoferol	tokoferol	k1gInSc1	tokoferol
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
K	K	kA	K
(	(	kIx(	(
<g/>
fylochinon	fylochinon	k1gInSc1	fylochinon
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
(	(	kIx(	(
<g/>
kys	kys	k?	kys
<g/>
.	.	kIx.	.
askorbová	askorbový	k2eAgFnSc1d1	askorbová
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
300	[number]	k4	300
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
H	H	kA	H
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
biotin	biotin	k1gInSc1	biotin
<g/>
,	,	kIx,	,
koenzym	koenzym	k1gInSc1	koenzym
R	R	kA	R
či	či	k8xC	či
vitamín	vitamín	k1gInSc1	vitamín
B	B	kA	B
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B	B	kA	B
Vitamín	vitamín	k1gInSc1	vitamín
B1	B1	k1gFnSc1	B1
(	(	kIx(	(
<g/>
také	také	k9	také
thiamin	thiamin	k1gInSc1	thiamin
nebo	nebo	k8xC	nebo
aneurin	aneurin	k1gInSc1	aneurin
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B2	B2	k1gFnSc2	B2
(	(	kIx(	(
<g/>
riboflavin	riboflavin	k1gInSc1	riboflavin
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B3	B3	k1gFnSc2	B3
(	(	kIx(	(
<g/>
niacin	niacin	k1gInSc1	niacin
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B5	B5	k1gFnSc2	B5
(	(	kIx(	(
<g/>
kyselina	kyselina	k1gFnSc1	kyselina
pantothenová	pantothenová	k1gFnSc1	pantothenová
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B6	B6	k1gFnSc2	B6
(	(	kIx(	(
<g/>
pyridoxin	pyridoxin	k1gInSc1	pyridoxin
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B7	B7	k1gFnSc2	B7
(	(	kIx(	(
<g/>
biotin	biotin	k1gInSc1	biotin
,	,	kIx,	,
<g/>
vitamín	vitamín	k1gInSc1	vitamín
H	H	kA	H
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B9	B9	k1gFnSc2	B9
(	(	kIx(	(
<g/>
kyselina	kyselina	k1gFnSc1	kyselina
listová	listový	k2eAgFnSc1d1	listová
<g/>
)	)	kIx)	)
Vitamín	vitamín	k1gInSc1	vitamín
B12	B12	k1gFnSc2	B12
(	(	kIx(	(
<g/>
kobalamin	kobalamin	k1gInSc4	kobalamin
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
B-komplex	Bomplex	k1gInSc1	B-komplex
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
vitaminů	vitamin	k1gInPc2	vitamin
B	B	kA	B
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc1	význam
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
H	H	kA	H
a	a	k8xC	a
PP	PP	kA	PP
faktor	faktor	k1gInSc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
vitaminů	vitamin	k1gInPc2	vitamin
B	B	kA	B
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
látkové	látkový	k2eAgFnSc3d1	látková
výměně	výměna	k1gFnSc3	výměna
ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
a	a	k8xC	a
nervové	nervový	k2eAgFnSc3d1	nervová
tkáni	tkáň	k1gFnSc3	tkáň
a	a	k8xC	a
ke	k	k7c3	k
krvetvorbě	krvetvorba	k1gFnSc3	krvetvorba
<g/>
.	.	kIx.	.
</s>
<s>
Antivitaminy	Antivitamin	k2eAgFnPc1d1	Antivitamin
jsou	být	k5eAaImIp3nP	být
přirozené	přirozený	k2eAgFnPc1d1	přirozená
nebo	nebo	k8xC	nebo
syntetické	syntetický	k2eAgFnPc1d1	syntetická
látky	látka	k1gFnPc1	látka
narušující	narušující	k2eAgFnSc4d1	narušující
funkci	funkce	k1gFnSc4	funkce
vitaminů	vitamin	k1gInPc2	vitamin
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc4	jejich
vstřebávání	vstřebávání	k1gNnSc4	vstřebávání
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vitaminy	vitamin	k1gInPc1	vitamin
štěpí	štěpit	k5eAaImIp3nP	štěpit
na	na	k7c4	na
neúčinné	účinný	k2eNgFnPc4d1	neúčinná
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
nevyužitelné	využitelný	k2eNgInPc1d1	nevyužitelný
komplexy	komplex	k1gInPc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Antivitamín	Antivitamín	k1gInSc1	Antivitamín
Avitaminóza	avitaminóza	k1gFnSc1	avitaminóza
Hypovitaminóza	hypovitaminóza	k1gFnSc1	hypovitaminóza
Hypervitaminóza	hypervitaminóza	k1gFnSc1	hypervitaminóza
Provitamin	provitamin	k1gInSc1	provitamin
Vitageny	Vitagen	k1gInPc1	Vitagen
(	(	kIx(	(
<g/>
vitamín	vitamín	k1gInSc1	vitamín
F	F	kA	F
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vitamín	vitamín	k1gInSc1	vitamín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vitamín	vitamín	k1gInSc1	vitamín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
