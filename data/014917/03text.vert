<s>
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
(	(	kIx(
<g/>
Bratislava	Bratislava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
je	být	k5eAaImIp3nS
historická	historický	k2eAgFnSc1d1
budova	budova	k1gFnSc1
na	na	k7c6
Hlavním	hlavní	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejstarší	starý	k2eAgFnPc4d3
budovy	budova	k1gFnPc4
ve	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
víceré	vícerý	k4xRyIgFnPc4
budovy	budova	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
konkrétně	konkrétně	k6eAd1
Jakubův	Jakubův	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
Pawerův	Pawerův	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
Ungerův	Ungerův	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
Aponiho	Aponi	k1gMnSc4
palác	palác	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
postaveny	postavit	k5eAaPmNgInP
v	v	k7c6
různých	různý	k2eAgInPc6d1
uměleckých	umělecký	k2eAgInPc6d1
slozích	sloh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
část	část	k1gFnSc1
radnice	radnice	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
samostatný	samostatný	k2eAgInSc1d1
dům	dům	k1gInSc1
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
věží	věž	k1gFnSc7
na	na	k7c6
východním	východní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
náměstí	náměstí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
výstavby	výstavba	k1gFnSc2
</s>
<s>
Byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
v	v	k7c6
gotickém	gotický	k2eAgInSc6d1
stylu	styl	k1gInSc6
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k9
výsledek	výsledek	k1gInSc4
spojení	spojení	k1gNnSc2
vícerých	vícerý	k4xRyIgInPc2
domů	dům	k1gInPc2
(	(	kIx(
<g/>
Pawerův	Pawerův	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
,	,	kIx,
Ungerův	Ungerův	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
,	,	kIx,
Aponiho	Aponi	k1gMnSc4
palác	palác	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
vedle	vedle	k7c2
věže	věž	k1gFnSc2
dal	dát	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
rychtář	rychtář	k1gMnSc1
Jakub	Jakub	k1gMnSc1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
samotná	samotný	k2eAgFnSc1d1
věž	věž	k1gFnSc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
gotická	gotický	k2eAgNnPc1d1
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
postavena	postavit	k5eAaPmNgNnP
ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
prošla	projít	k5eAaPmAgFnS
mnohými	mnohý	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
a	a	k8xC
rozšířeními	rozšíření	k1gNnPc7
<g/>
,	,	kIx,
hlavně	hlavně	k9
rekonstrukcí	rekonstrukce	k1gFnPc2
v	v	k7c6
renesančním	renesanční	k2eAgInSc6d1
stylu	styl	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1599	#num#	k4
po	po	k7c4
poškození	poškození	k1gNnSc4
způsobeným	způsobený	k2eAgNnSc7d1
zemětřesením	zemětřesení	k1gNnSc7
<g/>
,	,	kIx,
barokní	barokní	k2eAgFnSc1d1
restylizace	restylizace	k1gFnSc1
věže	věž	k1gFnSc2
po	po	k7c6
požáru	požár	k1gInSc6
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
a	a	k8xC
novorenesanční	novorenesanční	k2eAgNnSc1d1
<g/>
,	,	kIx,
neogotické	ogotický	k2eNgNnSc1d1
křídlo	křídlo	k1gNnSc1
postavené	postavený	k2eAgNnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
přímo	přímo	k6eAd1
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
mezi	mezi	k7c7
Hlavním	hlavní	k2eAgNnSc7d1
náměstím	náměstí	k1gNnSc7
a	a	k8xC
Primaciálním	primaciální	k2eAgNnSc7d1
náměstím	náměstí	k1gNnSc7
<g/>
,	,	kIx,
vedle	vedle	k7c2
ní	on	k3xPp3gFnSc2
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
řecké	řecký	k2eAgNnSc1d1
a	a	k8xC
japonské	japonský	k2eAgNnSc1d1
velvyslanectví	velvyslanectví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radnici	radnice	k1gFnSc3
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
lehce	lehko	k6eAd1
poznat	poznat	k5eAaPmF
podle	podle	k7c2
barevné	barevný	k2eAgFnSc2d1
dlaždicové	dlaždicový	k2eAgFnSc2d1
střechy	střecha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Budova	budova	k1gFnSc1
byla	být	k5eAaImAgFnS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
jako	jako	k8xC,k8xS
městská	městský	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
do	do	k7c2
pozdního	pozdní	k2eAgMnSc2d1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
sloužila	sloužit	k5eAaImAgFnS
i	i	k9
jiným	jiný	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
,	,	kIx,
například	například	k6eAd1
jako	jako	k9
vězení	vězení	k1gNnSc1
<g/>
,	,	kIx,
mincovna	mincovna	k1gFnSc1
<g/>
,	,	kIx,
tržní	tržní	k2eAgNnSc1d1
místo	místo	k1gNnSc1
a	a	k8xC
objekt	objekt	k1gInSc1
pro	pro	k7c4
veřejné	veřejný	k2eAgFnPc4d1
oslavy	oslava	k1gFnPc4
a	a	k8xC
slavnosti	slavnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
využívána	využívat	k5eAaImNgFnS,k5eAaPmNgFnS
také	také	k9
jako	jako	k9
depozitář	depozitář	k1gInSc1
<g/>
,	,	kIx,
zbrojnice	zbrojnice	k1gFnSc1
a	a	k8xC
městský	městský	k2eAgInSc1d1
archiv	archiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
Městské	městský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
s	s	k7c7
expozicí	expozice	k1gFnSc7
o	o	k7c6
historii	historie	k1gFnSc6
města	město	k1gNnSc2
Bratislavy	Bratislava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
vystavované	vystavovaný	k2eAgInPc4d1
exponáty	exponát	k1gInPc4
patří	patřit	k5eAaImIp3nS
nástroje	nástroj	k1gInPc1
používané	používaný	k2eAgInPc1d1
k	k	k7c3
mučení	mučení	k1gNnSc3
<g/>
,	,	kIx,
staroměstské	staroměstský	k2eAgInPc1d1
žaláře	žalář	k1gInPc1
<g/>
,	,	kIx,
historické	historický	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
<g/>
,	,	kIx,
brnění	brnění	k1gNnPc1
<g/>
,	,	kIx,
miniatury	miniatura	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
atrakcí	atrakce	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
dělová	dělový	k2eAgFnSc1d1
koule	koule	k1gFnSc1
vestavěná	vestavěný	k2eAgFnSc1d1
do	do	k7c2
stěny	stěna	k1gFnSc2
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kouli	koule	k1gFnSc6
na	na	k7c4
budovu	budova	k1gFnSc4
vystřelili	vystřelit	k5eAaPmAgMnP
Napoleonovi	Napoleonův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Stará	starý	k2eAgFnSc1d1
radnice	radnice	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Skupinka	skupinka	k1gFnSc1
turistů	turist	k1gMnPc2
u	u	k7c2
Staré	Staré	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bratislava	Bratislava	k1gFnSc1
–	–	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Členění	členění	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Okresy	okres	k1gInPc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
I	i	k8xC
•	•	k?
Bratislava	Bratislava	k1gFnSc1
II	II	kA
•	•	k?
Bratislava	Bratislava	k1gFnSc1
III	III	kA
•	•	k?
Bratislava	Bratislava	k1gFnSc1
IV	IV	kA
•	•	k?
Bratislava	Bratislava	k1gFnSc1
V	v	k7c6
Městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
</s>
<s>
Čunovo	Čunovo	k1gNnSc1
•	•	k?
Devín	Devín	k1gMnSc1
•	•	k?
Devínska	Devínsko	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Dúbravka	Dúbravka	k1gFnSc1
•	•	k?
Jarovce	Jarovec	k1gInSc2
•	•	k?
Karlova	Karlův	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Lamač	lamač	k1gMnSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Mesto	Mesto	k1gNnSc1
•	•	k?
Petržalka	Petržalka	k1gFnSc1
•	•	k?
Podunajské	podunajský	k2eAgFnSc3d1
Biskupice	Biskupika	k1gFnSc3
•	•	k?
Rača	Rač	k1gInSc2
•	•	k?
Rusovce	Rusovec	k1gMnSc2
•	•	k?
Ružinov	Ružinov	k1gInSc1
•	•	k?
Staré	Staré	k2eAgNnSc4d1
Mesto	Mesto	k1gNnSc4
•	•	k?
Vajnory	Vajnora	k1gFnSc2
•	•	k?
Vrakuňa	Vrakuň	k1gInSc2
•	•	k?
Záhorská	záhorský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
</s>
<s>
Významné	významný	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Historické	historický	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
</s>
<s>
Bratislavský	bratislavský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
•	•	k?
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
•	•	k?
Stará	starat	k5eAaImIp3nS
radnice	radnice	k1gFnSc1
•	•	k?
Michalská	Michalský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
•	•	k?
Primaciální	primaciální	k2eAgInSc1d1
palác	palác	k1gInSc1
Mosty	most	k1gInPc1
</s>
<s>
Most	most	k1gInSc1
Apollo	Apollo	k1gMnSc1
•	•	k?
Most	most	k1gInSc1
Lafranconi	Lafrancoň	k1gFnSc3
•	•	k?
Most	most	k1gInSc1
Slovenského	slovenský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
•	•	k?
Prístavný	Prístavný	k2eAgInSc1d1
most	most	k1gInSc1
•	•	k?
Starý	starý	k2eAgInSc1d1
most	most	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bratislava	Bratislava	k1gFnSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
316593823	#num#	k4
</s>
