<s>
Historie	historie	k1gFnSc1	historie
anime	animat	k5eAaPmIp3nS	animat
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
japonští	japonský	k2eAgMnPc1d1	japonský
filmoví	filmový	k2eAgMnPc1d1	filmový
tvůrci	tvůrce	k1gMnPc1	tvůrce
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
animačními	animační	k2eAgFnPc7d1	animační
technikami	technika	k1gFnPc7	technika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
