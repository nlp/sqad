<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
Metro	metro	k1gNnSc1	metro
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
základ	základ	k1gInSc4	základ
sítě	síť	k1gFnSc2	síť
pražské	pražský	k2eAgFnSc2d1	Pražská
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
"	"	kIx"	"
metro	metro	k1gNnSc1	metro
sovětského	sovětský	k2eAgInSc2d1	sovětský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
584	[number]	k4	584
miliony	milion	k4xCgInPc1	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
tedy	tedy	k9	tedy
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
síť	síť	k1gFnSc4	síť
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jedinou	jediný	k2eAgFnSc4d1	jediná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
dráhách	dráha	k1gFnPc6	dráha
kategorizována	kategorizovat	k5eAaBmNgFnS	kategorizovat
jako	jako	k8xC	jako
speciální	speciální	k2eAgFnSc1d1	speciální
železniční	železniční	k2eAgFnSc1d1	železniční
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
dráhy	dráha	k1gFnSc2	dráha
i	i	k8xC	i
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a.	a.	k?	a.
s.	s.	k?	s.
První	první	k4xOgInSc4	první
úsek	úsek	k1gInSc4	úsek
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1974	[number]	k4	1974
(	(	kIx(	(
<g/>
úsek	úsek	k1gInSc1	úsek
Florenc	Florenc	k1gFnSc1	Florenc
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
<g/>
)	)	kIx)	)
–	–	k?	–
Kačerov	Kačerov	k1gInSc1	Kačerov
trasy	trasa	k1gFnSc2	trasa
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
poslední	poslední	k2eAgInSc1d1	poslední
úsek	úsek	k1gInSc1	úsek
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
prodloužení	prodloužení	k1gNnSc3	prodloužení
trasy	trasa	k1gFnSc2	trasa
A	a	k9	a
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
–	–	k?	–
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tři	tři	k4xCgFnPc1	tři
linky	linka	k1gFnPc1	linka
značené	značený	k2eAgFnPc1d1	značená
písmeny	písmeno	k1gNnPc7	písmeno
a	a	k8xC	a
barvami	barva	k1gFnPc7	barva
<g/>
:	:	kIx,	:
A	a	k9	a
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B	B	kA	B
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
a	a	k8xC	a
C	C	kA	C
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
nová	nový	k2eAgFnSc1d1	nová
linka	linka	k1gFnSc1	linka
D	D	kA	D
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
měří	měřit	k5eAaImIp3nS	měřit
jeho	jeho	k3xOp3gFnSc1	jeho
síť	síť	k1gFnSc1	síť
65,2	[number]	k4	65,2
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Soupravy	souprava	k1gFnPc1	souprava
metra	metro	k1gNnSc2	metro
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
61	[number]	k4	61
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
stanic	stanice	k1gFnPc2	stanice
jsou	být	k5eAaImIp3nP	být
přestupní	přestupní	k2eAgMnSc1d1	přestupní
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Můstek	můstek	k1gInSc1	můstek
a	a	k8xC	a
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linky	linka	k1gFnPc1	linka
podzemní	podzemní	k2eAgFnPc1d1	podzemní
dráhy	dráha	k1gFnPc1	dráha
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
čtyřech	čtyři	k4xCgNnPc6	čtyři
místech	místo	k1gNnPc6	místo
vedeny	vést	k5eAaImNgInP	vést
pod	pod	k7c7	pod
řekou	řeka	k1gFnSc7	řeka
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
C	C	kA	C
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
tubusu	tubus	k1gInSc6	tubus
Nuselského	nuselský	k2eAgInSc2d1	nuselský
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
linka	linka	k1gFnSc1	linka
A	a	k9	a
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
otevřeném	otevřený	k2eAgInSc6d1	otevřený
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
depa	depo	k1gNnSc2	depo
Hostivař	Hostivař	k1gFnSc1	Hostivař
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
linka	linka	k1gFnSc1	linka
B	B	kA	B
vede	vést	k5eAaImIp3nS	vést
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Hloubětín	Hloubětína	k1gFnPc2	Hloubětína
a	a	k8xC	a
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
a	a	k8xC	a
také	také	k9	také
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Hůrka	hůrka	k1gFnSc1	hůrka
a	a	k8xC	a
Lužiny	lužina	k1gFnSc2	lužina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
v	v	k7c6	v
tubusu	tubus	k1gInSc6	tubus
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
"	"	kIx"	"
metro	metro	k1gNnSc1	metro
sovětského	sovětský	k2eAgInSc2d1	sovětský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
budované	budovaný	k2eAgInPc4d1	budovaný
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
o	o	k7c4	o
dosažení	dosažení	k1gNnPc4	dosažení
co	co	k9	co
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
přepravní	přepravní	k2eAgFnPc4d1	přepravní
kapacity	kapacita	k1gFnPc4	kapacita
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
lehčími	lehký	k2eAgInPc7d2	lehčí
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
lehké	lehký	k2eAgNnSc1d1	lehké
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
podpovrchová	podpovrchový	k2eAgFnSc1d1	podpovrchová
tramvaj	tramvaj	k1gFnSc1	tramvaj
aj.	aj.	kA	aj.
I	i	k9	i
o	o	k7c4	o
realizaci	realizace	k1gFnSc4	realizace
takovýchto	takovýto	k3xDgInPc2	takovýto
druhů	druh	k1gInPc2	druh
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
hovořilo	hovořit	k5eAaImAgNnS	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
úseků	úsek	k1gInPc2	úsek
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc4	některý
úseky	úsek	k1gInPc4	úsek
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
sítě	síť	k1gFnSc2	síť
jsou	být	k5eAaImIp3nP	být
povrchové	povrchový	k2eAgFnPc1d1	povrchová
či	či	k8xC	či
nadzemní	nadzemní	k2eAgFnPc1d1	nadzemní
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
linky	linka	k1gFnPc1	linka
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
nekříží	křížit	k5eNaImIp3nS	křížit
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
;	;	kIx,	;
vlaky	vlak	k1gInPc1	vlak
mohou	moct	k5eAaImIp3nP	moct
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
přejíždět	přejíždět	k5eAaImF	přejíždět
pomocí	pomocí	k7c2	pomocí
manipulačních	manipulační	k2eAgFnPc2d1	manipulační
spojek	spojka	k1gFnPc2	spojka
<g/>
,	,	kIx,	,
cestující	cestující	k1gMnPc1	cestující
přecházejí	přecházet	k5eAaImIp3nP	přecházet
přestupními	přestupní	k2eAgFnPc7d1	přestupní
chodbami	chodba	k1gFnPc7	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
jednosměrné	jednosměrný	k2eAgInPc1d1	jednosměrný
tunely	tunel	k1gInPc1	tunel
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
většinou	většinou	k6eAd1	většinou
ražené	ražený	k2eAgFnPc1d1	ražená
a	a	k8xC	a
stanice	stanice	k1gFnPc1	stanice
trojlodní	trojlodní	k2eAgFnPc1d1	trojlodní
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnPc1d1	založená
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
cca	cca	kA	cca
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pražského	pražský	k2eAgInSc2d1	pražský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
je	on	k3xPp3gNnPc4	on
spojují	spojovat	k5eAaImIp3nP	spojovat
šikmo	šikmo	k6eAd1	šikmo
vyražené	vyražený	k2eAgInPc1d1	vyražený
eskalátorové	eskalátorový	k2eAgInPc1d1	eskalátorový
tunely	tunel	k1gInPc1	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
úsecích	úsek	k1gInPc6	úsek
na	na	k7c6	na
sídlištích	sídliště	k1gNnPc6	sídliště
a	a	k8xC	a
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
tratě	trať	k1gFnPc1	trať
metra	metro	k1gNnSc2	metro
umístěny	umístit	k5eAaPmNgInP	umístit
mělčeji	mělce	k6eAd2	mělce
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
hloubené	hloubený	k2eAgNnSc1d1	hloubené
<g/>
;	;	kIx,	;
tomu	ten	k3xDgNnSc3	ten
rovněž	rovněž	k9	rovněž
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
ráz	ráz	k1gInSc1	ráz
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
založené	založený	k2eAgNnSc1d1	založené
ve	v	k7c6	v
stavební	stavební	k2eAgFnSc6d1	stavební
jámě	jáma	k1gFnSc6	jáma
<g/>
,	,	kIx,	,
cca	cca	kA	cca
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
m	m	kA	m
hluboko	hluboko	k6eAd1	hluboko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	Nová	k1gFnSc1	Nová
sídliště	sídliště	k1gNnSc2	sídliště
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
stavěna	stavit	k5eAaImNgFnS	stavit
s	s	k7c7	s
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
prostorem	prostor	k1gInSc7	prostor
pro	pro	k7c4	pro
případné	případný	k2eAgNnSc4d1	případné
budování	budování	k1gNnSc4	budování
staveb	stavba	k1gFnPc2	stavba
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svojit	k5eAaImIp3nS	svojit
úlohu	úloha	k1gFnSc4	úloha
sehrálo	sehrát	k5eAaPmAgNnS	sehrát
také	také	k9	také
konstruování	konstruování	k1gNnSc1	konstruování
metra	metro	k1gNnSc2	metro
jako	jako	k8xC	jako
krytu	kryt	k1gInSc2	kryt
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pro	pro	k7c4	pro
případný	případný	k2eAgInSc4d1	případný
vojenský	vojenský	k2eAgInSc4d1	vojenský
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
;	;	kIx,	;
tunely	tunel	k1gInPc1	tunel
i	i	k8xC	i
stanice	stanice	k1gFnPc1	stanice
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
ochranného	ochranný	k2eAgInSc2d1	ochranný
systému	systém	k1gInSc2	systém
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
úseků	úsek	k1gInPc2	úsek
vedoucích	vedoucí	k2eAgInPc2d1	vedoucí
pod	pod	k7c7	pod
Vltavou	Vltava	k1gFnSc7	Vltava
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc4	tři
ražené	ražený	k2eAgInPc4d1	ražený
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
hloubený	hloubený	k2eAgMnSc1d1	hloubený
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tyto	tento	k3xDgFnPc4	tento
části	část	k1gFnPc4	část
celého	celý	k2eAgInSc2d1	celý
systému	systém	k1gInSc2	systém
bylo	být	k5eAaImAgNnS	být
nejnáročnější	náročný	k2eAgNnSc1d3	nejnáročnější
vybudovat	vybudovat	k5eAaPmF	vybudovat
<g/>
;	;	kIx,	;
nezbytností	nezbytnost	k1gFnSc7	nezbytnost
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jednak	jednak	k8xC	jednak
zpevňování	zpevňování	k1gNnSc1	zpevňování
nadloží	nadloží	k1gNnSc2	nadloží
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
využití	využití	k1gNnSc1	využití
nejmodernějších	moderní	k2eAgInPc2d3	nejmodernější
dosud	dosud	k6eAd1	dosud
neotestovaných	otestovaný	k2eNgFnPc2d1	neotestovaná
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
hloubeného	hloubený	k2eAgInSc2d1	hloubený
podchodu	podchod	k1gInSc2	podchod
linky	linka	k1gFnSc2	linka
C	C	kA	C
pod	pod	k7c7	pod
řekou	řeka	k1gFnSc7	řeka
u	u	k7c2	u
Tróji	Trója	k1gFnSc6	Trója
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
přepraví	přepravit	k5eAaPmIp3nS	přepravit
přes	přes	k7c4	přes
1,6	[number]	k4	1,6
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
radiální	radiální	k2eAgInSc1d1	radiální
systém	systém	k1gInSc1	systém
tří	tři	k4xCgFnPc2	tři
linek	linka	k1gFnPc2	linka
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
hlavních	hlavní	k2eAgNnPc2d1	hlavní
sídlišť	sídliště	k1gNnPc2	sídliště
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgNnSc1d1	jižní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Jihozápadní	jihozápadní	k2eAgNnSc1d1	jihozápadní
město	město	k1gNnSc1	město
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
a	a	k8xC	a
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
systém	systém	k1gInSc4	systém
tramvají	tramvaj	k1gFnPc2	tramvaj
i	i	k8xC	i
autobusů	autobus	k1gInPc2	autobus
<g/>
;	;	kIx,	;
obě	dva	k4xCgFnPc1	dva
sítě	síť	k1gFnPc1	síť
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
postupně	postupně	k6eAd1	postupně
upravovány	upravovat	k5eAaImNgFnP	upravovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
napaječi	napaječ	k1gInSc6	napaječ
<g/>
"	"	kIx"	"
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
největší	veliký	k2eAgInPc4d3	veliký
obraty	obrat	k1gInPc4	obrat
cestujících	cestující	k1gMnPc2	cestující
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
přestup	přestup	k1gInSc1	přestup
na	na	k7c4	na
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
zatíženými	zatížený	k2eAgFnPc7d1	zatížená
stanicemi	stanice	k1gFnPc7	stanice
jsou	být	k5eAaImIp3nP	být
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
(	(	kIx(	(
<g/>
obrat	obrat	k1gInSc1	obrat
118	[number]	k4	118
647	[number]	k4	647
cestujících	cestující	k1gMnPc2	cestující
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
(	(	kIx(	(
<g/>
117	[number]	k4	117
726	[number]	k4	726
<g/>
)	)	kIx)	)
a	a	k8xC	a
Anděl	Anděl	k1gMnSc1	Anděl
(	(	kIx(	(
<g/>
101	[number]	k4	101
451	[number]	k4	451
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejzatíženějšími	zatížený	k2eAgInPc7d3	nejzatíženější
úseky	úsek	k1gInPc7	úsek
jsou	být	k5eAaImIp3nP	být
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
-	-	kIx~	-
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
(	(	kIx(	(
<g/>
291	[number]	k4	291
689	[number]	k4	689
cestujících	cestující	k1gMnPc2	cestující
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
-	-	kIx~	-
Pražského	pražský	k2eAgNnSc2d1	Pražské
povstání	povstání	k1gNnSc2	povstání
(	(	kIx(	(
<g/>
280	[number]	k4	280
916	[number]	k4	916
<g/>
)	)	kIx)	)
a	a	k8xC	a
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgNnPc1d1	Pavlovo
-	-	kIx~	-
Muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
268	[number]	k4	268
0	[number]	k4	0
<g/>
78	[number]	k4	78
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
C.	C.	kA	C.
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
vytíženosti	vytíženost	k1gFnSc3	vytíženost
bývá	bývat	k5eAaImIp3nS	bývat
vypravováno	vypravován	k2eAgNnSc1d1	vypravováno
v	v	k7c6	v
ranní	ranní	k2eAgFnSc6d1	ranní
špičce	špička	k1gFnSc6	špička
445	[number]	k4	445
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
254	[number]	k4	254
a	a	k8xC	a
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
165	[number]	k4	165
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopravních	dopravní	k2eAgFnPc6d1	dopravní
špičkách	špička	k1gFnPc6	špička
byly	být	k5eAaImAgFnP	být
krátké	krátký	k2eAgInPc4d1	krátký
intervaly	interval	k1gInPc4	interval
(	(	kIx(	(
<g/>
180	[number]	k4	180
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
(	(	kIx(	(
<g/>
trojvozové	trojvozový	k2eAgFnSc2d1	trojvozový
soupravy	souprava	k1gFnSc2	souprava
vozů	vůz	k1gInPc2	vůz
Ečs	Ečs	k1gFnSc2	Ečs
<g/>
,	,	kIx,	,
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1975	[number]	k4	1975
čtyřvozové	čtyřvozový	k2eAgInPc1d1	čtyřvozový
<g/>
,	,	kIx,	,
od	od	k7c2	od
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
délce	délka	k1gFnSc6	délka
pěti	pět	k4xCc2	pět
vozů	vůz	k1gInPc2	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgNnSc1d1	moderní
zabezpečovací	zabezpečovací	k2eAgNnSc1d1	zabezpečovací
zařízení	zařízení	k1gNnSc1	zařízení
instalované	instalovaný	k2eAgNnSc1d1	instalované
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
další	další	k2eAgNnSc1d1	další
zkrácení	zkrácení	k1gNnSc1	zkrácení
intervalů	interval	k1gInPc2	interval
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
časový	časový	k2eAgInSc4d1	časový
interval	interval	k1gInSc4	interval
mezi	mezi	k7c4	mezi
odjezdy	odjezd	k1gInPc4	odjezd
souprav	souprava	k1gFnPc2	souprava
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
pracovního	pracovní	k2eAgInSc2d1	pracovní
dne	den	k1gInSc2	den
cca	cca	kA	cca
2-3	[number]	k4	2-3
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
špičku	špička	k1gFnSc4	špička
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
podíl	podíl	k1gInSc1	podíl
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
přepravě	přeprava	k1gFnSc6	přeprava
všech	všecek	k3xTgMnPc2	všecek
cestujících	cestující	k1gMnPc2	cestující
v	v	k7c4	v
pražské	pražský	k2eAgNnSc4d1	Pražské
MHD	MHD	kA	MHD
45,65	[number]	k4	45,65
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vcelku	vcelku	k6eAd1	vcelku
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
původní	původní	k2eAgFnSc3d1	původní
koncepci	koncepce	k1gFnSc3	koncepce
MHD	MHD	kA	MHD
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
poměry	poměra	k1gFnSc2	poměra
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
třemi	tři	k4xCgInPc7	tři
primárními	primární	k2eAgInPc7d1	primární
druhy	druh	k1gInPc7	druh
dopravy	doprava	k1gFnSc2	doprava
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
let	léto	k1gNnPc2	léto
90	[number]	k4	90
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
40	[number]	k4	40
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Zprovoznění	zprovoznění	k1gNnSc1	zprovoznění
metra	metro	k1gNnSc2	metro
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
ukončit	ukončit	k5eAaPmF	ukončit
provoz	provoz	k1gInSc4	provoz
starých	starý	k2eAgInPc2d1	starý
dvounápravových	dvounápravový	k2eAgInPc2d1	dvounápravový
motorových	motorový	k2eAgInPc2d1	motorový
vozů	vůz	k1gInPc2	vůz
s	s	k7c7	s
vlečnými	vlečný	k2eAgFnPc7d1	vlečná
<g/>
,	,	kIx,	,
i	i	k8xC	i
některých	některý	k3yIgFnPc2	některý
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
vítali	vítat	k5eAaImAgMnP	vítat
cestující	cestující	k1gMnPc1	cestující
s	s	k7c7	s
horší	zlý	k2eAgFnSc7d2	horší
pohyblivostí	pohyblivost	k1gFnSc7	pohyblivost
přepravující	přepravující	k2eAgFnSc7d1	přepravující
se	se	k3xPyFc4	se
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
nutnost	nutnost	k1gFnSc4	nutnost
překonávání	překonávání	k1gNnSc3	překonávání
výškových	výškový	k2eAgInPc2d1	výškový
rozdílů	rozdíl	k1gInPc2	rozdíl
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
vestibulu	vestibul	k1gInSc2	vestibul
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pro	pro	k7c4	pro
omezení	omezení	k1gNnSc4	omezení
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nutnost	nutnost	k1gFnSc4	nutnost
překonávat	překonávat	k5eAaImF	překonávat
delší	dlouhý	k2eAgFnPc4d2	delší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
prosazované	prosazovaný	k2eAgFnSc3d1	prosazovaná
ideji	idea	k1gFnSc3	idea
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kam	kam	k6eAd1	kam
jede	jet	k5eAaImIp3nS	jet
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
nepojede	jet	k5eNaImIp3nS	jet
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
např.	např.	kA	např.
zrušena	zrušit	k5eAaPmNgFnS	zrušit
i	i	k8xC	i
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Na	na	k7c4	na
Veselí	veselí	k1gNnSc4	veselí
-	-	kIx~	-
Budějovické	budějovický	k2eAgNnSc4d1	Budějovické
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ovšem	ovšem	k9	ovšem
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
odříznutí	odříznutí	k1gNnSc3	odříznutí
tratě	trať	k1gFnSc2	trať
z	z	k7c2	z
Budějovického	budějovický	k2eAgNnSc2d1	Budějovické
náměstí	náměstí	k1gNnSc2	náměstí
na	na	k7c6	na
Ryšánku	Ryšánek	k1gMnSc6	Ryšánek
od	od	k7c2	od
vozovny	vozovna	k1gFnSc2	vozovna
Pankrác	Pankrác	k1gFnSc4	Pankrác
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
předtím	předtím	k6eAd1	předtím
prošla	projít	k5eAaPmAgFnS	projít
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
prvním	první	k4xOgInSc7	první
návrhem	návrh	k1gInSc7	návrh
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byla	být	k5eAaImAgFnS	být
iniciativa	iniciativa	k1gFnSc1	iniciativa
pražského	pražský	k2eAgMnSc2d1	pražský
obchodníka	obchodník	k1gMnSc2	obchodník
Ladislava	Ladislav	k1gMnSc2	Ladislav
Rotta	Rott	k1gMnSc2	Rott
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
městské	městský	k2eAgFnSc3d1	městská
radě	rada	k1gFnSc3	rada
využít	využít	k5eAaPmF	využít
prací	práce	k1gFnSc7	práce
na	na	k7c4	na
kanalizaci	kanalizace	k1gFnSc4	kanalizace
a	a	k8xC	a
asanaci	asanace	k1gFnSc4	asanace
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zahájit	zahájit	k5eAaPmF	zahájit
stavby	stavba	k1gFnPc4	stavba
tunelů	tunel	k1gInPc2	tunel
na	na	k7c6	na
první	první	k4xOgFnSc6	první
případné	případný	k2eAgFnSc6d1	případná
lince	linka	k1gFnSc6	linka
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Karlín	Karlín	k1gInSc1	Karlín
–	–	k?	–
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Podolí	Podolí	k1gNnPc2	Podolí
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
u	u	k7c2	u
Křižovnického	Křižovnický	k2eAgInSc2d1	Křižovnický
pivovaru	pivovar	k1gInSc2	pivovar
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
linkou	linka	k1gFnSc7	linka
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
–	–	k?	–
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pražské	pražský	k2eAgFnSc2d1	Pražská
radnice	radnice	k1gFnSc2	radnice
však	však	k9	však
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
návrhy	návrh	k1gInPc7	návrh
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
metra	metro	k1gNnSc2	metro
či	či	k8xC	či
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
tramvaje	tramvaj	k1gFnSc2	tramvaj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
i	i	k9	i
v	v	k7c6	v
předválečném	předválečný	k2eAgNnSc6d1	předválečné
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
čtyř	čtyři	k4xCgFnPc2	čtyři
podzemních	podzemní	k2eAgFnPc2d1	podzemní
tratí	trať	k1gFnPc2	trať
předložili	předložit	k5eAaPmAgMnP	předložit
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	list	k1gInSc1	list
a	a	k8xC	a
Bohumil	Bohumil	k1gMnSc1	Bohumil
Belada	Belada	k1gFnSc1	Belada
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
rychlá	rychlý	k2eAgFnSc1d1	rychlá
dráha	dráha	k1gFnSc1	dráha
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
podzemních	podzemní	k2eAgFnPc2d1	podzemní
tratí	trať	k1gFnPc2	trať
byly	být	k5eAaImAgInP	být
vypracovány	vypracovat	k5eAaPmNgInP	vypracovat
do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
vypsané	vypsaný	k2eAgInPc1d1	vypsaný
Elektrickými	elektrický	k2eAgInPc7d1	elektrický
podniky	podnik	k1gInPc7	podnik
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
byla	být	k5eAaImAgFnS	být
fakticky	fakticky	k6eAd1	fakticky
zahájena	zahájen	k2eAgFnSc1d1	zahájena
stavba	stavba	k1gFnSc1	stavba
trasy	trasa	k1gFnSc2	trasa
A	a	k9	a
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
válečné	válečný	k2eAgFnSc3d1	válečná
situaci	situace	k1gFnSc3	situace
ukončena	ukončen	k2eAgFnSc1d1	ukončena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
plánovalo	plánovat	k5eAaImAgNnS	plánovat
obnovení	obnovení	k1gNnSc1	obnovení
projekčních	projekční	k2eAgFnPc2d1	projekční
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
úsek	úsek	k1gInSc1	úsek
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
ale	ale	k9	ale
přišel	přijít	k5eAaPmAgInS	přijít
příkaz	příkaz	k1gInSc1	příkaz
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
práce	práce	k1gFnPc4	práce
zmrazil	zmrazit	k5eAaPmAgMnS	zmrazit
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
není	být	k5eNaImIp3nS	být
země	země	k1gFnSc1	země
hospodářsky	hospodářsky	k6eAd1	hospodářsky
dost	dost	k6eAd1	dost
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Výhledový	výhledový	k2eAgInSc1d1	výhledový
plán	plán	k1gInSc1	plán
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
metropole	metropol	k1gFnSc2	metropol
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
z	z	k7c2	z
června	červen	k1gInSc2	červen
1949	[number]	k4	1949
již	již	k6eAd1	již
s	s	k7c7	s
podzemní	podzemní	k2eAgFnSc7d1	podzemní
dráhou	dráha	k1gFnSc7	dráha
nepočítal	počítat	k5eNaImAgMnS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
návrhy	návrh	k1gInPc1	návrh
přišly	přijít	k5eAaPmAgInP	přijít
až	až	k9	až
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začínala	začínat	k5eAaImAgFnS	začínat
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
rapidně	rapidně	k6eAd1	rapidně
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
tepny	tepna	k1gFnPc1	tepna
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc1	ulice
Na	na	k7c6	na
příkopě	příkop	k1gInSc6	příkop
a	a	k8xC	a
Václavské	václavský	k2eAgNnSc1d1	Václavské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
přeplněné	přeplněný	k2eAgFnPc1d1	přeplněná
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
doprava	doprava	k1gFnSc1	doprava
k	k	k7c3	k
nově	nově	k6eAd1	nově
budovanému	budovaný	k2eAgNnSc3d1	budované
Jižnímu	jižní	k2eAgNnSc3d1	jižní
Městu	město	k1gNnSc3	město
a	a	k8xC	a
přes	přes	k7c4	přes
Nuselské	nuselský	k2eAgNnSc4d1	Nuselské
údolí	údolí	k1gNnSc4	údolí
vůbec	vůbec	k9	vůbec
byla	být	k5eAaImAgFnS	být
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
;	;	kIx,	;
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Nusle	Nusle	k1gFnPc4	Nusle
pro	pro	k7c4	pro
náročné	náročný	k2eAgFnPc4d1	náročná
terénní	terénní	k2eAgFnPc4d1	terénní
podmínky	podmínka	k1gFnPc4	podmínka
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
vyhovující	vyhovující	k2eAgFnSc1d1	vyhovující
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vítězný	vítězný	k2eAgInSc4d1	vítězný
návrh	návrh	k1gInSc4	návrh
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
podpovrchová	podpovrchový	k2eAgFnSc1d1	podpovrchová
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
vést	vést	k5eAaImF	vést
z	z	k7c2	z
Hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
přes	přes	k7c4	přes
Nuselský	nuselský	k2eAgInSc4d1	nuselský
most	most	k1gInSc4	most
na	na	k7c4	na
Pankrác	Pankrác	k1gFnSc4	Pankrác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
<g/>
,	,	kIx,	,
zanedlouho	zanedlouho	k6eAd1	zanedlouho
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
průzkumu	průzkum	k1gInSc2	průzkum
sovětské	sovětský	k2eAgFnSc2d1	sovětská
komise	komise	k1gFnSc2	komise
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
metra	metro	k1gNnSc2	metro
odděleného	oddělený	k2eAgNnSc2d1	oddělené
od	od	k7c2	od
tramvaje	tramvaj	k1gFnSc2	tramvaj
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
efektivnější	efektivní	k2eAgMnSc1d2	efektivnější
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
rozestavěné	rozestavěný	k2eAgInPc1d1	rozestavěný
úseky	úsek	k1gInPc1	úsek
musely	muset	k5eAaImAgInP	muset
ještě	ještě	k6eAd1	ještě
přestavovat	přestavovat	k5eAaImF	přestavovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
přišlo	přijít	k5eAaPmAgNnS	přijít
politické	politický	k2eAgNnSc1d1	politické
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
použít	použít	k5eAaPmF	použít
místo	místo	k1gNnSc4	místo
vozů	vůz	k1gInPc2	vůz
domácí	domácí	k2eAgFnSc2d1	domácí
výroby	výroba	k1gFnSc2	výroba
sovětské	sovětský	k2eAgFnPc1d1	sovětská
<g/>
,	,	kIx,	,
zastaralé	zastaralý	k2eAgFnPc1d1	zastaralá
koncepce	koncepce	k1gFnPc1	koncepce
(	(	kIx(	(
<g/>
vycházející	vycházející	k2eAgFnPc1d1	vycházející
z	z	k7c2	z
typu	typ	k1gInSc2	typ
vyvinutého	vyvinutý	k2eAgInSc2d1	vyvinutý
na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
vložení	vložení	k1gNnSc1	vložení
posilujícího	posilující	k2eAgInSc2d1	posilující
roštu	rošt	k1gInSc2	rošt
do	do	k7c2	do
Nuselského	nuselský	k2eAgInSc2d1	nuselský
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nebyl	být	k5eNaImAgInS	být
na	na	k7c4	na
takové	takový	k3xDgNnSc4	takový
zatížení	zatížení	k1gNnSc4	zatížení
původně	původně	k6eAd1	původně
navržen	navrhnout	k5eAaPmNgInS	navrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Posilující	posilující	k2eAgInSc4d1	posilující
rošt	rošt	k1gInSc4	rošt
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
však	však	k9	však
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
i	i	k9	i
použití	použití	k1gNnSc1	použití
souprav	souprava	k1gFnPc2	souprava
československých	československý	k2eAgInPc2d1	československý
vozů	vůz	k1gInPc2	vůz
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tubus	tubus	k1gInSc1	tubus
Nuselského	nuselský	k2eAgInSc2d1	nuselský
mostu	most	k1gInSc2	most
byl	být	k5eAaImAgInS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
kloubových	kloubový	k2eAgFnPc2d1	kloubová
tramvají	tramvaj	k1gFnPc2	tramvaj
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
nápravovým	nápravový	k2eAgInSc7d1	nápravový
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
otevřením	otevření	k1gNnSc7	otevření
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
zpoždění	zpoždění	k1gNnPc4	zpoždění
způsobená	způsobený	k2eAgNnPc4d1	způsobené
nedostatkem	nedostatek	k1gInSc7	nedostatek
zkušeností	zkušenost	k1gFnPc2	zkušenost
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
stavbu	stavba	k1gFnSc4	stavba
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
ražbou	ražba	k1gFnSc7	ražba
linky	linka	k1gFnSc2	linka
A	a	k8xC	a
z	z	k7c2	z
Dejvic	Dejvice	k1gFnPc2	Dejvice
na	na	k7c4	na
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
prvního	první	k4xOgInSc2	první
úseku	úsek	k1gInSc2	úsek
trasy	trasa	k1gFnSc2	trasa
C	C	kA	C
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1974	[number]	k4	1974
v	v	k7c4	v
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
19	[number]	k4	19
minut	minuta	k1gFnPc2	minuta
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
politickou	politický	k2eAgFnSc7d1	politická
manifestací	manifestace	k1gFnSc7	manifestace
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
KSČ	KSČ	kA	KSČ
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
slavnostně	slavnostně	k6eAd1	slavnostně
přestřihl	přestřihnout	k5eAaPmAgMnS	přestřihnout
pásku	páska	k1gFnSc4	páska
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
byl	být	k5eAaImAgInS	být
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
první	první	k4xOgInSc1	první
úsek	úsek	k1gInSc1	úsek
linky	linka	k1gFnSc2	linka
A	A	kA	A
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Muzeum	muzeum	k1gNnSc1	muzeum
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc4	první
přestupní	přestupní	k2eAgInSc4d1	přestupní
bod	bod	k1gInSc4	bod
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
trasy	trasa	k1gFnPc1	trasa
prodlouženy	prodloužit	k5eAaPmNgFnP	prodloužit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
prvního	první	k4xOgInSc2	první
úseku	úsek	k1gInSc2	úsek
linky	linka	k1gFnSc2	linka
B.	B.	kA	B.
Budování	budování	k1gNnSc4	budování
úseku	úsek	k1gInSc2	úsek
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
)	)	kIx)	)
–	–	k?	–
Smíchovské	smíchovské	k1gNnSc1	smíchovské
nádraží	nádraží	k1gNnSc2	nádraží
trvalo	trvat	k5eAaImAgNnS	trvat
osm	osm	k4xCc1	osm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
byla	být	k5eAaImAgFnS	být
trasa	trasa	k1gFnSc1	trasa
B	B	kA	B
dána	dán	k2eAgFnSc1d1	dána
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
stavba	stavba	k1gFnSc1	stavba
dalších	další	k2eAgInPc2d1	další
navazujících	navazující	k2eAgInPc2d1	navazující
úseků	úsek	k1gInPc2	úsek
Dukelská	dukelský	k2eAgFnSc1d1	Dukelská
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Nové	Nové	k2eAgFnPc1d1	Nové
Butovice	Butovice	k1gFnPc1	Butovice
<g/>
)	)	kIx)	)
–	–	k?	–
Smíchovské	smíchovské	k1gNnSc1	smíchovské
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
připravovala	připravovat	k5eAaImAgFnS	připravovat
se	se	k3xPyFc4	se
stavba	stavba	k1gFnSc1	stavba
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
<g/>
–	–	k?	–
<g/>
Zápotockého	Zápotocký	k1gMnSc2	Zápotocký
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stavělo	stavět	k5eAaImAgNnS	stavět
se	se	k3xPyFc4	se
depo	depo	k1gNnSc1	depo
trasy	trasa	k1gFnSc2	trasa
A	a	k9	a
v	v	k7c6	v
Hostivaři	Hostivař	k1gFnSc6	Hostivař
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
byla	být	k5eAaImAgFnS	být
trasa	trasa	k1gFnSc1	trasa
C	C	kA	C
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
pod	pod	k7c7	pod
Vltavou	Vltava	k1gFnSc7	Vltava
do	do	k7c2	do
Holešovic	Holešovice	k1gFnPc2	Holešovice
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
a	a	k8xC	a
rychlá	rychlý	k2eAgFnSc1d1	rychlá
výstavba	výstavba	k1gFnSc1	výstavba
však	však	k9	však
patrně	patrně	k6eAd1	patrně
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
možnosti	možnost	k1gFnPc4	možnost
stagnující	stagnující	k2eAgFnSc2d1	stagnující
ekonomiky	ekonomika	k1gFnSc2	ekonomika
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
termíny	termín	k1gInPc1	termín
pro	pro	k7c4	pro
oficiální	oficiální	k2eAgNnSc4d1	oficiální
otevření	otevření	k1gNnSc4	otevření
mírně	mírně	k6eAd1	mírně
zpožďovat	zpožďovat	k5eAaImF	zpožďovat
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
byly	být	k5eAaImAgFnP	být
přesunuty	přesunut	k2eAgFnPc1d1	přesunuta
finance	finance	k1gFnPc1	finance
určené	určený	k2eAgFnPc1d1	určená
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
(	(	kIx(	(
<g/>
výstavba	výstavba	k1gFnSc1	výstavba
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
)	)	kIx)	)
a	a	k8xC	a
peníze	peníz	k1gInPc1	peníz
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
trasy	trasa	k1gFnSc2	trasa
B.	B.	kA	B.
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
bylo	být	k5eAaImAgNnS	být
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
politické	politický	k2eAgFnSc2d1	politická
situace	situace	k1gFnSc2	situace
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
stanovené	stanovený	k2eAgNnSc1d1	stanovené
tempo	tempo	k1gNnSc1	tempo
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
udržet	udržet	k5eAaPmF	udržet
–	–	k?	–
podzemní	podzemní	k2eAgFnSc1d1	podzemní
dráha	dráha	k1gFnSc1	dráha
ztratila	ztratit	k5eAaPmAgFnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
obtížnější	obtížný	k2eAgFnSc4d2	obtížnější
trať	trať	k1gFnSc4	trať
naplánovat	naplánovat	k5eAaBmF	naplánovat
než	než	k8xS	než
sehnat	sehnat	k5eAaPmF	sehnat
finance	finance	k1gFnPc4	finance
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
opačná	opačný	k2eAgFnSc1d1	opačná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výstavba	výstavba	k1gFnSc1	výstavba
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
financovat	financovat	k5eAaBmF	financovat
z	z	k7c2	z
federálních	federální	k2eAgInPc2d1	federální
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
financovat	financovat	k5eAaBmF	financovat
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
většina	většina	k1gFnSc1	většina
stanic	stanice	k1gFnPc2	stanice
s	s	k7c7	s
ideologickými	ideologický	k2eAgInPc7d1	ideologický
názvy	název	k1gInPc7	název
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
jména	jméno	k1gNnPc4	jméno
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
místním	místní	k2eAgFnPc3d1	místní
částem	část	k1gFnPc3	část
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
došlo	dojít	k5eAaPmAgNnS	dojít
například	například	k6eAd1	například
u	u	k7c2	u
stanic	stanice	k1gFnPc2	stanice
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
(	(	kIx(	(
<g/>
Gottwaldova	Gottwaldův	k2eAgFnSc1d1	Gottwaldova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Háje	háj	k1gInPc1	háj
(	(	kIx(	(
<g/>
Kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pankrác	Pankrác	k1gFnSc1	Pankrác
(	(	kIx(	(
<g/>
Mládežnická	mládežnický	k2eAgFnSc1d1	mládežnická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chodov	Chodov	k1gInSc1	Chodov
(	(	kIx(	(
<g/>
Budovatelů	budovatel	k1gMnPc2	budovatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Opatov	Opatov	k1gInSc1	Opatov
(	(	kIx(	(
<g/>
Družby	družba	k1gFnSc2	družba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc4	nádraží
Holešovice	Holešovice	k1gFnPc1	Holešovice
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Fučíkova	Fučíkův	k2eAgFnSc1d1	Fučíkova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Roztyly	Roztyly	k?	Roztyly
(	(	kIx(	(
<g/>
Primátora	primátor	k1gMnSc2	primátor
Vacka	Vacek	k1gMnSc2	Vacek
<g/>
)	)	kIx)	)
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
C.	C.	kA	C.
Stanice	stanice	k1gFnSc1	stanice
Florenc	Florenc	k1gFnSc1	Florenc
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Sokolovské	sokolovský	k2eAgFnPc1d1	Sokolovská
ulice	ulice	k1gFnPc1	ulice
ležely	ležet	k5eAaImAgFnP	ležet
čtyři	čtyři	k4xCgFnPc4	čtyři
nové	nový	k2eAgFnPc4d1	nová
stanice	stanice	k1gFnPc4	stanice
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
B.	B.	kA	B.
Na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
A	a	k9	a
byla	být	k5eAaImAgFnS	být
Leninova	Leninův	k2eAgFnSc1d1	Leninova
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Dejvickou	dejvický	k2eAgFnSc4d1	Dejvická
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
stanice	stanice	k1gFnPc1	stanice
byly	být	k5eAaImAgFnP	být
přejmenovány	přejmenovat	k5eAaPmNgFnP	přejmenovat
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
B	B	kA	B
<g/>
:	:	kIx,	:
Nové	Nové	k2eAgFnPc1d1	Nové
Butovice	Butovice	k1gFnPc1	Butovice
(	(	kIx(	(
<g/>
Dukelská	dukelský	k2eAgFnSc1d1	Dukelská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anděl	Anděl	k1gMnSc1	Anděl
(	(	kIx(	(
<g/>
Moskevská	moskevský	k2eAgFnSc1d1	Moskevská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jinonice	Jinonice	k1gFnPc1	Jinonice
(	(	kIx(	(
<g/>
Švermova	Švermův	k2eAgFnSc1d1	Švermova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
změněny	změnit	k5eAaPmNgInP	změnit
i	i	k9	i
názvy	název	k1gInPc1	název
stanic	stanice	k1gFnPc2	stanice
na	na	k7c6	na
rozestavěném	rozestavěný	k2eAgInSc6d1	rozestavěný
úseku	úsek	k1gInSc6	úsek
trasy	trasa	k1gFnSc2	trasa
B.	B.	kA	B.
Stanice	stanice	k1gFnSc1	stanice
Invalidovna	invalidovna	k1gFnSc1	invalidovna
měla	mít	k5eAaImAgFnS	mít
původně	původně	k6eAd1	původně
být	být	k5eAaImF	být
Hakenova	Hakenův	k2eAgFnSc1d1	Hakenova
a	a	k8xC	a
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
jmenovat	jmenovat	k5eAaBmF	jmenovat
Zápotockého	Zápotockého	k2eAgFnSc1d1	Zápotockého
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
úsek	úsek	k1gInSc1	úsek
linky	linka	k1gFnSc2	linka
B	B	kA	B
Florenc	Florenc	k1gFnSc1	Florenc
–	–	k?	–
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
tohoto	tento	k3xDgInSc2	tento
úseku	úsek	k1gInSc2	úsek
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
budovat	budovat	k5eAaImF	budovat
dále	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Hloubětín	Hloubětín	k1gInSc4	Hloubětín
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
linky	linka	k1gFnSc2	linka
B	B	kA	B
také	také	k9	také
na	na	k7c4	na
Zličín	Zličín	k1gInSc4	Zličín
(	(	kIx(	(
<g/>
otevřen	otevřít	k5eAaPmNgInS	otevřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
–	–	k?	–
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
dvou	dva	k4xCgFnPc2	dva
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
Hloubětín	Hloubětín	k1gInSc1	Hloubětín
a	a	k8xC	a
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
a	a	k8xC	a
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
dokončením	dokončení	k1gNnSc7	dokončení
je	být	k5eAaImIp3nS	být
soupravy	souprava	k1gFnSc2	souprava
projížděly	projíždět	k5eAaImAgInP	projíždět
bez	bez	k7c2	bez
zastavení	zastavení	k1gNnSc2	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
započala	započnout	k5eAaPmAgFnS	započnout
obměna	obměna	k1gFnSc1	obměna
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
jak	jak	k8xS	jak
rekonstrukcemi	rekonstrukce	k1gFnPc7	rekonstrukce
sovětských	sovětský	k2eAgInPc2d1	sovětský
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k9	také
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
nákupu	nákup	k1gInSc2	nákup
nových	nový	k2eAgFnPc2d1	nová
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Komplikace	komplikace	k1gFnPc4	komplikace
a	a	k8xC	a
zdražení	zdražení	k1gNnSc4	zdražení
přinesl	přinést	k5eAaPmAgInS	přinést
krach	krach	k1gInSc1	krach
firmy	firma	k1gFnSc2	firma
ČKD	ČKD	kA	ČKD
Dopravní	dopravní	k2eAgInPc1d1	dopravní
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gMnSc7	jeho
hlavním	hlavní	k2eAgMnSc7d1	hlavní
výrobcem	výrobce	k1gMnSc7	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
pásmového	pásmový	k2eAgInSc2d1	pásmový
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
B.	B.	kA	B.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pásmový	pásmový	k2eAgInSc1d1	pásmový
provoz	provoz	k1gInSc1	provoz
spočíval	spočívat	k5eAaImAgInS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
spoje	spoj	k1gFnPc1	spoj
metra	metro	k1gNnSc2	metro
nebyly	být	k5eNaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
až	až	k9	až
na	na	k7c4	na
konečné	konečný	k2eAgNnSc4d1	konečné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
blíže	blízce	k6eAd2	blízce
centru	centrum	k1gNnSc6	centrum
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
bývalé	bývalý	k2eAgFnSc6d1	bývalá
konečné	konečná	k1gFnSc6	konečná
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
až	až	k9	až
na	na	k7c4	na
Zličín	Zličín	k1gInSc4	Zličín
tak	tak	k8xS	tak
některé	některý	k3yIgFnPc1	některý
soupravy	souprava	k1gFnPc1	souprava
jezdily	jezdit	k5eAaImAgFnP	jezdit
jen	jen	k9	jen
na	na	k7c4	na
Smíchovské	smíchovský	k2eAgNnSc4d1	Smíchovské
nádraží	nádraží	k1gNnSc4	nádraží
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Nových	Nových	k2eAgFnPc2d1	Nových
Butovic	Butovice	k1gFnPc2	Butovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
města	město	k1gNnSc2	město
končily	končit	k5eAaImAgInP	končit
na	na	k7c4	na
Českomoravské	českomoravský	k2eAgNnSc4d1	Českomoravské
místo	místo	k1gNnSc4	místo
až	až	k9	až
na	na	k7c6	na
Černém	černý	k2eAgInSc6d1	černý
Mostě	most	k1gInSc6	most
<g/>
.	.	kIx.	.
</s>
<s>
Pásmový	pásmový	k2eAgInSc1d1	pásmový
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
jízdních	jízdní	k2eAgMnPc2d1	jízdní
řádů	řád	k1gInPc2	řád
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zaplavení	zaplavení	k1gNnSc3	zaplavení
metra	metro	k1gNnSc2	metro
povodní	povodeň	k1gFnPc2	povodeň
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
protipovodňové	protipovodňový	k2eAgFnSc2d1	protipovodňová
ochrany	ochrana	k1gFnSc2	ochrana
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
nebyl	být	k5eNaImAgInS	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
způsobem	způsob	k1gInSc7	způsob
udržován	udržovat	k5eAaImNgInS	udržovat
a	a	k8xC	a
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
novějších	nový	k2eAgInPc6d2	novější
úsecích	úsek	k1gInPc6	úsek
zcela	zcela	k6eAd1	zcela
chyběl	chybět	k5eAaImAgInS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
úseky	úsek	k1gInPc1	úsek
metra	metro	k1gNnSc2	metro
mimo	mimo	k7c4	mimo
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
časté	častý	k2eAgNnSc1d1	časté
dopravní	dopravní	k2eAgInPc1d1	dopravní
kolapsy	kolaps	k1gInPc1	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obnovou	obnova	k1gFnSc7	obnova
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
finanční	finanční	k2eAgInPc1d1	finanční
náklady	náklad	k1gInPc1	náklad
přesáhly	přesáhnout	k5eAaPmAgInP	přesáhnout
6,96	[number]	k4	6,96
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
pomohly	pomoct	k5eAaPmAgInP	pomoct
jak	jak	k6eAd1	jak
státní	státní	k2eAgInPc1d1	státní
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zrenovován	zrenovovat	k5eAaPmNgInS	zrenovovat
i	i	k8xC	i
ochranný	ochranný	k2eAgInSc1d1	ochranný
systém	systém	k1gInSc1	systém
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podobným	podobný	k2eAgFnPc3d1	podobná
událostem	událost	k1gFnPc3	událost
předešlo	předejít	k5eAaPmAgNnS	předejít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
prodloužení	prodloužení	k1gNnSc1	prodloužení
sítě	síť	k1gFnSc2	síť
přišlo	přijít	k5eAaPmAgNnS	přijít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
linka	linka	k1gFnSc1	linka
C	C	kA	C
podruhé	podruhé	k6eAd1	podruhé
překročila	překročit	k5eAaPmAgFnS	překročit
Vltavu	Vltava	k1gFnSc4	Vltava
a	a	k8xC	a
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
stanice	stanice	k1gFnSc2	stanice
Kobylisy	Kobylisy	k1gInPc4	Kobylisy
a	a	k8xC	a
Ládví	Ládví	k1gNnSc4	Ládví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
byl	být	k5eAaImAgInS	být
polovinou	polovina	k1gFnSc7	polovina
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
tratě	trať	k1gFnSc2	trať
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
Letňanech	Letňan	k1gMnPc6	Letňan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
stanice	stanice	k1gFnSc1	stanice
Depo	depo	k1gNnSc1	depo
Hostivař	Hostivař	k1gFnSc1	Hostivař
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A.	A.	kA	A.
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přestavbou	přestavba	k1gFnSc7	přestavba
části	část	k1gFnSc2	část
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
depa	depo	k1gNnSc2	depo
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přesunuty	přesunut	k2eAgFnPc1d1	přesunuta
konečné	konečná	k1gFnSc2	konečná
zastávky	zastávka	k1gFnSc2	zastávka
mnoha	mnoho	k4c2	mnoho
(	(	kIx(	(
<g/>
šesti	šest	k4xCc6	šest
<g/>
)	)	kIx)	)
městských	městský	k2eAgFnPc2d1	městská
a	a	k8xC	a
příměstských	příměstský	k2eAgFnPc2d1	příměstská
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dříve	dříve	k6eAd2	dříve
končily	končit	k5eAaImAgFnP	končit
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
ve	v	k7c6	v
špičkách	špička	k1gFnPc6	špička
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
jen	jen	k9	jen
každý	každý	k3xTgInSc1	každý
druhý	druhý	k4xOgInSc1	druhý
vlak	vlak	k1gInSc1	vlak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
její	její	k3xOp3gFnSc4	její
efektivitu	efektivita	k1gFnSc4	efektivita
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
navazující	navazující	k2eAgInSc1d1	navazující
úsek	úsek	k1gInSc1	úsek
z	z	k7c2	z
Ládví	Ládev	k1gFnPc2	Ládev
do	do	k7c2	do
Letňan	Letňany	k1gInPc2	Letňany
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
novými	nový	k2eAgFnPc7d1	nová
stanicemi	stanice	k1gFnPc7	stanice
–	–	k?	–
Střížkovem	Střížkov	k1gInSc7	Střížkov
<g/>
,	,	kIx,	,
Prosekem	Prosek	k1gInSc7	Prosek
a	a	k8xC	a
Letňany	Letňan	k1gMnPc7	Letňan
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kde	kde	k6eAd1	kde
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
autobusový	autobusový	k2eAgInSc1d1	autobusový
terminál	terminál	k1gInSc1	terminál
odbavující	odbavující	k2eAgFnSc2d1	odbavující
městské	městský	k2eAgFnSc2d1	městská
a	a	k8xC	a
příměstské	příměstský	k2eAgFnSc2d1	příměstská
linky	linka	k1gFnSc2	linka
a	a	k8xC	a
záchytné	záchytný	k2eAgNnSc4d1	záchytné
parkoviště	parkoviště	k1gNnSc4	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R.	R.	kA	R.
Po	po	k7c6	po
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
úsek	úsek	k1gInSc1	úsek
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
výstavba	výstavba	k1gFnSc1	výstavba
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
2	[number]	k4	2
fáze	fáze	k1gFnSc2	fáze
<g/>
:	:	kIx,	:
C1	C1	k1gFnPc2	C1
Nádraží	nádraží	k1gNnSc2	nádraží
Holešovice	Holešovice	k1gFnPc4	Holešovice
–	–	k?	–
Ládví	Ládví	k1gNnSc1	Ládví
a	a	k8xC	a
C2	C2	k1gFnSc1	C2
Ládví	Ládev	k1gFnPc2	Ládev
–	–	k?	–
Letňany	Letňan	k1gMnPc7	Letňan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obsloužil	obsloužit	k5eAaPmAgMnS	obsloužit
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
přebudovaného	přebudovaný	k2eAgInSc2d1	přebudovaný
západního	západní	k2eAgInSc2d1	západní
vestibulu	vestibul	k1gInSc2	vestibul
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Stodůlky	stodůlka	k1gFnSc2	stodůlka
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
prodloužení	prodloužení	k1gNnSc1	prodloužení
metra	metro	k1gNnSc2	metro
A	a	k9	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
západním	západní	k2eAgInSc6d1	západní
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
konečné	konečný	k2eAgFnSc2d1	konečná
stanice	stanice	k1gFnSc2	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
se	se	k3xPyFc4	se
linka	linka	k1gFnSc1	linka
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
4	[number]	k4	4
stanice	stanice	k1gFnSc2	stanice
<g/>
:	:	kIx,	:
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc1	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
,	,	kIx,	,
Petřiny	Petřiny	k1gFnPc1	Petřiny
a	a	k8xC	a
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
metra	metro	k1gNnSc2	metro
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
řešena	řešit	k5eAaImNgFnS	řešit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
splňovala	splňovat	k5eAaImAgFnS	splňovat
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
funkční	funkční	k2eAgInPc4d1	funkční
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
účelné	účelný	k2eAgNnSc4d1	účelné
vyřešení	vyřešení	k1gNnSc4	vyřešení
dopravní	dopravní	k2eAgFnSc2d1	dopravní
situace	situace	k1gFnSc2	situace
ve	v	k7c6	v
velkoměstě	velkoměsto	k1gNnSc6	velkoměsto
a	a	k8xC	a
vylepšení	vylepšení	k1gNnSc6	vylepšení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
estetiku	estetika	k1gFnSc4	estetika
úpravy	úprava	k1gFnSc2	úprava
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
vestibulů	vestibul	k1gInPc2	vestibul
<g/>
.	.	kIx.	.
</s>
<s>
Koncepce	koncepce	k1gFnPc1	koncepce
esteticky	esteticky	k6eAd1	esteticky
vyváženého	vyvážený	k2eAgNnSc2d1	vyvážené
prostředí	prostředí	k1gNnSc2	prostředí
přichází	přicházet	k5eAaImIp3nS	přicházet
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
z	z	k7c2	z
tamního	tamní	k2eAgNnSc2d1	tamní
moskevského	moskevský	k2eAgNnSc2d1	moskevské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vyhlášené	vyhlášený	k2eAgNnSc1d1	vyhlášené
svou	svůj	k3xOyFgFnSc4	svůj
úpravou	úprava	k1gFnSc7	úprava
staničních	staniční	k2eAgInPc2d1	staniční
interiérů	interiér	k1gInPc2	interiér
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Celková	celkový	k2eAgFnSc1d1	celková
kompozice	kompozice	k1gFnSc1	kompozice
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
řešena	řešit	k5eAaImNgFnS	řešit
zabudováním	zabudování	k1gNnSc7	zabudování
výstupů	výstup	k1gInPc2	výstup
metra	metr	k1gMnSc2	metr
do	do	k7c2	do
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
zastřešené	zastřešený	k2eAgInPc4d1	zastřešený
samostatné	samostatný	k2eAgInPc4d1	samostatný
výstupy	výstup	k1gInPc4	výstup
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
C.	C.	kA	C.
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
převýšení	převýšení	k1gNnSc1	převýšení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
m	m	kA	m
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
ke	k	k7c3	k
klasickým	klasický	k2eAgInPc3d1	klasický
schodům	schod	k1gInPc3	schod
přidány	přidat	k5eAaPmNgInP	přidat
i	i	k9	i
schody	schod	k1gInPc1	schod
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
eskalátory	eskalátor	k1gInPc1	eskalátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vstupy	vstup	k1gInPc4	vstup
do	do	k7c2	do
stanic	stanice	k1gFnPc2	stanice
dále	daleko	k6eAd2	daleko
navazují	navazovat	k5eAaImIp3nP	navazovat
vestibuly	vestibul	k1gInPc4	vestibul
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc4d1	umožňující
i	i	k8xC	i
drobný	drobný	k2eAgInSc4d1	drobný
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vestibuly	vestibul	k1gInPc1	vestibul
byly	být	k5eAaImAgInP	být
esteticky	esteticky	k6eAd1	esteticky
vyvážené	vyvážený	k2eAgInPc1d1	vyvážený
<g/>
.	.	kIx.	.
</s>
<s>
Plní	plnit	k5eAaImIp3nS	plnit
totiž	totiž	k9	totiž
nejenom	nejenom	k6eAd1	nejenom
funkci	funkce	k1gFnSc4	funkce
přístupovou	přístupový	k2eAgFnSc4d1	přístupová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
i	i	k9	i
jako	jako	k9	jako
podchody	podchod	k1gInPc1	podchod
(	(	kIx(	(
<g/>
Muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
<g/>
,	,	kIx,	,
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
představa	představa	k1gFnSc1	představa
esteticky	esteticky	k6eAd1	esteticky
vyváženého	vyvážený	k2eAgNnSc2d1	vyvážené
prostředí	prostředí	k1gNnSc2	prostředí
značně	značně	k6eAd1	značně
narušena	narušit	k5eAaPmNgFnS	narušit
rozdělením	rozdělení	k1gNnSc7	rozdělení
na	na	k7c4	na
část	část	k1gFnSc4	část
neplacenou	placený	k2eNgFnSc4d1	neplacená
s	s	k7c7	s
veřejnými	veřejný	k2eAgInPc7d1	veřejný
záchody	záchod	k1gInPc7	záchod
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc7	obchod
<g/>
,	,	kIx,	,
trafikami	trafika	k1gFnPc7	trafika
a	a	k8xC	a
část	část	k1gFnSc4	část
placenou	placený	k2eAgFnSc4d1	placená
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Původní	původní	k2eAgInPc1d1	původní
obklady	obklad	k1gInPc1	obklad
z	z	k7c2	z
kamenných	kamenný	k2eAgFnPc2d1	kamenná
desek	deska	k1gFnPc2	deska
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
Prahy	Praha	k1gFnSc2	Praha
tras	trasa	k1gFnPc2	trasa
C	C	kA	C
a	a	k8xC	a
A	A	kA	A
<g/>
)	)	kIx)	)
ničí	ničit	k5eAaImIp3nS	ničit
graffiti	graffiti	k1gNnSc4	graffiti
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
polepovány	polepovat	k5eAaImNgFnP	polepovat
reklamou	reklama	k1gFnSc7	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
došlo	dojít	k5eAaPmAgNnS	dojít
též	též	k6eAd1	též
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
charakteru	charakter	k1gInSc6	charakter
osvětlení	osvětlení	k1gNnSc2	osvětlení
a	a	k8xC	a
stropních	stropní	k2eAgInPc2d1	stropní
krytů	kryt	k1gInPc2	kryt
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
obměněn	obměněn	k2eAgInSc4d1	obměněn
systém	systém	k1gInSc4	systém
grafických	grafický	k2eAgFnPc2d1	grafická
navigačních	navigační	k2eAgFnPc2d1	navigační
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
hlavně	hlavně	k9	hlavně
ražené	ražený	k2eAgInPc4d1	ražený
tunely	tunel	k1gInPc4	tunel
a	a	k8xC	a
stanice	stanice	k1gFnPc4	stanice
(	(	kIx(	(
<g/>
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
třemi	tři	k4xCgNnPc7	tři
tubusy	tubus	k1gInPc1	tubus
oddělenými	oddělený	k2eAgInPc7d1	oddělený
pilířovými	pilířový	k2eAgInPc7d1	pilířový
sloupy	sloup	k1gInPc7	sloup
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hloubené	hloubený	k2eAgNnSc1d1	hloubené
(	(	kIx(	(
<g/>
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
zastřešeným	zastřešený	k2eAgNnSc7d1	zastřešené
nástupištěm	nástupiště	k1gNnSc7	nástupiště
bez	bez	k7c2	bez
opor	opora	k1gFnPc2	opora
nebo	nebo	k8xC	nebo
s	s	k7c7	s
oporami	opora	k1gFnPc7	opora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sestupy	sestup	k1gInPc1	sestup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
C	C	kA	C
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
<g/>
,	,	kIx,	,
eskalátory	eskalátor	k1gInPc1	eskalátor
(	(	kIx(	(
<g/>
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kombinací	kombinace	k1gFnSc7	kombinace
(	(	kIx(	(
<g/>
Nádraží	nádraží	k1gNnSc4	nádraží
Holešovice	Holešovice	k1gFnPc1	Holešovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
jsou	být	k5eAaImIp3nP	být
doplňovány	doplňován	k2eAgInPc1d1	doplňován
i	i	k8xC	i
výtahy	výtah	k1gInPc1	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Nástupiště	nástupiště	k1gNnPc1	nástupiště
bývají	bývat	k5eAaImIp3nP	bývat
ostrovní	ostrovní	k2eAgNnPc1d1	ostrovní
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
boční	boční	k2eAgInSc1d1	boční
(	(	kIx(	(
<g/>
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
Střížkov	Střížkov	k1gInSc1	Střížkov
<g/>
,	,	kIx,	,
Prosek	proséct	k5eAaPmDgInS	proséct
<g/>
,	,	kIx,	,
Rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obklady	obklad	k1gInPc1	obklad
stěn	stěna	k1gFnPc2	stěna
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
osazeny	osadit	k5eAaPmNgFnP	osadit
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
leštěnými	leštěný	k2eAgFnPc7d1	leštěná
deskami	deska	k1gFnPc7	deska
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
(	(	kIx(	(
<g/>
Florenc	Florenc	k1gFnSc1	Florenc
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
<g/>
,	,	kIx,	,
Budějovická	budějovický	k2eAgFnSc1d1	Budějovická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
etapách	etapa	k1gFnPc6	etapa
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přikročilo	přikročit	k5eAaPmAgNnS	přikročit
ke	k	k7c3	k
glazovaným	glazovaný	k2eAgFnPc3d1	glazovaná
tvárnicím	tvárnice	k1gFnPc3	tvárnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
jiné	jiný	k2eAgInPc1d1	jiný
moderní	moderní	k2eAgInPc1d1	moderní
materiály	materiál	k1gInPc1	materiál
(	(	kIx(	(
<g/>
Kobylisy	Kobylisy	k1gInPc1	Kobylisy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
architektem	architekt	k1gMnSc7	architekt
první	první	k4xOgFnSc2	první
etapy	etapa	k1gFnSc2	etapa
trasy	trasa	k1gFnSc2	trasa
C	C	kA	C
byl	být	k5eAaImAgMnS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Otruba	otruba	k1gFnSc1	otruba
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
C	C	kA	C
podílelo	podílet	k5eAaImAgNnS	podílet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
architektů	architekt	k1gMnPc2	architekt
a	a	k8xC	a
ateliérů	ateliér	k1gInPc2	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
linka	linka	k1gFnSc1	linka
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
trať	trať	k1gFnSc4	trať
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
úseku	úsek	k1gInSc6	úsek
není	být	k5eNaImIp3nS	být
vedeno	vést	k5eAaImNgNnS	vést
více	hodně	k6eAd2	hodně
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Přestup	přestup	k1gInSc1	přestup
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
stanicích	stanice	k1gFnPc6	stanice
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Florenc	Florenc	k1gFnSc1	Florenc
a	a	k8xC	a
Můstek	můstek	k1gInSc1	můstek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
vrcholy	vrchol	k1gInPc4	vrchol
"	"	kIx"	"
<g/>
přestupního	přestupní	k2eAgInSc2d1	přestupní
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Soupravy	souprava	k1gFnPc1	souprava
mohou	moct	k5eAaImIp3nP	moct
přejíždět	přejíždět	k5eAaImF	přejíždět
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
linkami	linka	k1gFnPc7	linka
–	–	k?	–
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
časech	čas	k1gInPc6	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
některé	některý	k3yIgFnPc1	některý
linky	linka	k1gFnPc1	linka
neměly	mít	k5eNaImAgFnP	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
depo	depo	k1gNnSc4	depo
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
spojky	spojka	k1gFnPc1	spojka
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
linkami	linka	k1gFnPc7	linka
A	A	kA	A
a	a	k8xC	a
C	C	kA	C
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
jednokolejné	jednokolejný	k2eAgFnPc1d1	jednokolejná
spojky	spojka	k1gFnPc1	spojka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
linkami	linka	k1gFnPc7	linka
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
<g/>
,	,	kIx,	,
využívaná	využívaný	k2eAgFnSc1d1	využívaná
obousměrně	obousměrně	k6eAd1	obousměrně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
linky	linka	k1gFnSc2	linka
A	a	k9	a
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
B	B	kA	B
není	být	k5eNaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
přímý	přímý	k2eAgInSc1d1	přímý
přejezd	přejezd	k1gInSc1	přejezd
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
A	a	k8xC	a
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
A	a	k9	a
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Motola	Motola	k1gFnSc1	Motola
do	do	k7c2	do
Hostivaře	Hostivař	k1gFnSc2	Hostivař
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
17	[number]	k4	17
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
17	[number]	k4	17
km	km	kA	km
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
ji	on	k3xPp3gFnSc4	on
běžnou	běžný	k2eAgFnSc4d1	běžná
rychlostí	rychlost	k1gFnSc7	rychlost
projede	projet	k5eAaPmIp3nS	projet
za	za	k7c4	za
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
trasy	trasa	k1gFnSc2	trasa
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
otevřena	otevřít	k5eAaPmNgFnS	otevřít
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
(	(	kIx(	(
<g/>
I.A	I.A	k1gMnPc2	I.A
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Leninova	Leninův	k2eAgNnSc2d1	Leninovo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
)	)	kIx)	)
a	a	k8xC	a
Náměstí	náměstí	k1gNnSc1	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
4,7	[number]	k4	4,7
km	km	kA	km
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
7	[number]	k4	7
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výstavba	výstavba	k1gFnSc1	výstavba
úseku	úsek	k1gInSc2	úsek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
2,6	[number]	k4	2,6
km	km	kA	km
a	a	k8xC	a
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
stanice	stanice	k1gFnSc1	stanice
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
Flora	Floro	k1gNnSc2	Floro
a	a	k8xC	a
Želivského	želivský	k2eAgNnSc2d1	Želivské
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1987	[number]	k4	1987
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
úsek	úsek	k1gInSc1	úsek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trasu	trasa	k1gFnSc4	trasa
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
o	o	k7c4	o
1,3	[number]	k4	1,3
km	km	kA	km
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Strašnická	strašnický	k2eAgFnSc1d1	Strašnická
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
pak	pak	k6eAd1	pak
úsek	úsek	k1gInSc1	úsek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
přidal	přidat	k5eAaPmAgMnS	přidat
dalších	další	k2eAgFnPc2d1	další
1,4	[number]	k4	1,4
km	km	kA	km
a	a	k8xC	a
stanici	stanice	k1gFnSc6	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
trasa	trasa	k1gFnSc1	trasa
ještě	ještě	k6eAd1	ještě
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
o	o	k7c6	o
stanici	stanice	k1gFnSc6	stanice
Depo	depo	k1gNnSc1	depo
Hostivař	Hostivař	k1gFnSc4	Hostivař
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
existující	existující	k2eAgFnSc2d1	existující
spojky	spojka	k1gFnSc2	spojka
do	do	k7c2	do
depa	depo	k1gNnSc2	depo
vedené	vedený	k2eAgFnSc2d1	vedená
částečně	částečně	k6eAd1	částečně
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Přepravní	přepravní	k2eAgFnSc1d1	přepravní
kapacita	kapacita	k1gFnSc1	kapacita
trasy	trasa	k1gFnSc2	trasa
A	a	k9	a
přepočtená	přepočtený	k2eAgFnSc1d1	přepočtená
na	na	k7c4	na
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
činí	činit	k5eAaImIp3nS	činit
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
cca	cca	kA	cca
20	[number]	k4	20
320	[number]	k4	320
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
v	v	k7c6	v
době	doba	k1gFnSc6	doba
snížených	snížený	k2eAgInPc2d1	snížený
přepravních	přepravní	k2eAgInPc2d1	přepravní
nároků	nárok	k1gInPc2	nárok
<g/>
,	,	kIx,	,
cca	cca	kA	cca
10	[number]	k4	10
500	[number]	k4	500
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
linka	linka	k1gFnSc1	linka
A	a	k9	a
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
úsekem	úsek	k1gInSc7	úsek
V.A	V.A	k1gFnSc2	V.A
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
přes	přes	k7c4	přes
stanice	stanice	k1gFnPc4	stanice
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc1	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
,	,	kIx,	,
Petřiny	Petřiny	k1gFnPc1	Petřiny
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
B	B	kA	B
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
B	B	kA	B
vede	vést	k5eAaImIp3nS	vést
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jihozápad-severovýchod	jihozápadeverovýchod	k1gInSc1	jihozápad-severovýchod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
žlutou	žlutý	k2eAgFnSc7d1	žlutá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
24	[number]	k4	24
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
25,7	[number]	k4	25,7
km	km	kA	km
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
ji	on	k3xPp3gFnSc4	on
projede	projet	k5eAaPmIp3nS	projet
za	za	k7c4	za
42	[number]	k4	42
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
prvního	první	k4xOgInSc2	první
úseku	úsek	k1gInSc2	úsek
I.B	I.B	k1gMnSc2	I.B
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
otevřen	otevřít	k5eAaPmNgInS	otevřít
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc1	sedm
stanic	stanice	k1gFnPc2	stanice
od	od	k7c2	od
Smíchovského	smíchovský	k2eAgNnSc2d1	Smíchovské
nádraží	nádraží	k1gNnSc2	nádraží
po	po	k7c6	po
Florenc	Florenc	k1gFnSc1	Florenc
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
otevření	otevření	k1gNnSc2	otevření
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Sokolovská	sokolovský	k2eAgNnPc4d1	Sokolovské
<g/>
)	)	kIx)	)
a	a	k8xC	a
délku	délka	k1gFnSc4	délka
4,9	[number]	k4	4,9
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
B	B	kA	B
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
4,9	[number]	k4	4,9
km	km	kA	km
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
novými	nový	k2eAgFnPc7d1	nová
stanicemi	stanice	k1gFnPc7	stanice
mezi	mezi	k7c7	mezi
Smíchovským	smíchovský	k2eAgNnSc7d1	Smíchovské
nádražím	nádraží	k1gNnSc7	nádraží
a	a	k8xC	a
Novými	nový	k2eAgFnPc7d1	nová
Butovicemi	Butovice	k1gFnPc7	Butovice
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Dukelskou	dukelský	k2eAgFnSc4d1	Dukelská
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1988	[number]	k4	1988
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
3	[number]	k4	3
stanice	stanice	k1gFnSc2	stanice
–	–	k?	–
Radlická	radlický	k2eAgFnSc1d1	Radlická
<g/>
,	,	kIx,	,
Jinonice	Jinonice	k1gFnPc1	Jinonice
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Švermova	Švermův	k2eAgFnSc1d1	Švermova
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nové	Nové	k2eAgFnPc1d1	Nové
Butovice	Butovice	k1gFnPc1	Butovice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
stanice	stanice	k1gFnPc4	stanice
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
využívají	využívat	k5eAaImIp3nP	využívat
<g/>
,	,	kIx,	,
Radlická	radlický	k2eAgFnSc1d1	Radlická
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejméně	málo	k6eAd3	málo
využívaná	využívaný	k2eAgFnSc1d1	využívaná
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
síti	síť	k1gFnSc6	síť
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
bylo	být	k5eAaImAgNnS	být
totiž	totiž	k9	totiž
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
stanice	stanice	k1gFnSc2	stanice
nové	nový	k2eAgNnSc4d1	nové
sídliště	sídliště	k1gNnSc4	sídliště
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
výstavba	výstavba	k1gFnSc1	výstavba
se	se	k3xPyFc4	se
však	však	k9	však
neuskutečnila	uskutečnit	k5eNaPmAgFnS	uskutečnit
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
tratě	trať	k1gFnSc2	trať
stála	stát	k5eAaImAgFnS	stát
mimo	mimo	k7c4	mimo
zájem	zájem	k1gInSc4	zájem
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neprocházela	procházet	k5eNaImAgFnS	procházet
hustě	hustě	k6eAd1	hustě
obydlenou	obydlený	k2eAgFnSc7d1	obydlená
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
úsek	úsek	k1gInSc1	úsek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
k	k	k7c3	k
trase	trasa	k1gFnSc3	trasa
přidal	přidat	k5eAaPmAgMnS	přidat
4,4	[number]	k4	4,4
km	km	kA	km
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
4	[number]	k4	4
nové	nový	k2eAgFnSc2d1	nová
stanice	stanice	k1gFnSc2	stanice
za	za	k7c7	za
Florencí	Florenc	k1gFnSc7	Florenc
po	po	k7c4	po
Českomoravskou	českomoravský	k2eAgFnSc4d1	Českomoravská
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
V.B	V.B	k1gFnSc2	V.B
mezi	mezi	k7c7	mezi
Novými	nový	k2eAgFnPc7d1	nová
Butovicemi	Butovice	k1gFnPc7	Butovice
a	a	k8xC	a
Zličínem	Zličín	k1gInSc7	Zličín
(	(	kIx(	(
<g/>
5	[number]	k4	5
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
5,1	[number]	k4	5,1
km	km	kA	km
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
B	B	kA	B
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
6,3	[number]	k4	6,3
km	km	kA	km
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
na	na	k7c4	na
Černý	černý	k2eAgInSc4d1	černý
Most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
však	však	k9	však
zůstávaly	zůstávat	k5eAaImAgFnP	zůstávat
nedostavěny	dostavěn	k2eNgFnPc1d1	nedostavěna
stanice	stanice	k1gFnPc1	stanice
Hloubětín	Hloubětína	k1gFnPc2	Hloubětína
a	a	k8xC	a
Kolbenova	Kolbenův	k2eAgNnSc2d1	Kolbenovo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
až	až	k9	až
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Přepravní	přepravní	k2eAgFnSc1d1	přepravní
kapacita	kapacita	k1gFnSc1	kapacita
trasy	trasa	k1gFnSc2	trasa
B	B	kA	B
přepočtená	přepočtený	k2eAgFnSc1d1	přepočtená
na	na	k7c4	na
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
činí	činit	k5eAaImIp3nS	činit
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
cca	cca	kA	cca
21	[number]	k4	21
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
cca	cca	kA	cca
10	[number]	k4	10
500	[number]	k4	500
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
C	C	kA	C
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
C	C	kA	C
vede	vést	k5eAaImIp3nS	vést
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sever	sever	k1gInSc4	sever
<g/>
–	–	k?	–
<g/>
jihovýchod	jihovýchod	k1gInSc1	jihovýchod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
20	[number]	k4	20
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
22,4	[number]	k4	22,4
km	km	kA	km
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
trasu	trasa	k1gFnSc4	trasa
projede	projet	k5eAaPmIp3nS	projet
za	za	k7c4	za
36	[number]	k4	36
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
úsek	úsek	k1gInSc1	úsek
trasy	trasa	k1gFnSc2	trasa
C	C	kA	C
i	i	k8xC	i
celého	celý	k2eAgNnSc2d1	celé
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
I.C	I.C	k1gFnSc2	I.C
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
budovat	budovat	k5eAaImF	budovat
7.1	[number]	k4	7.1
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
9.5	[number]	k4	9.5
<g/>
.	.	kIx.	.
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
6,6	[number]	k4	6,6
km	km	kA	km
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
9	[number]	k4	9
stanic	stanice	k1gFnPc2	stanice
mezi	mezi	k7c7	mezi
Florencí	Florenc	k1gFnSc7	Florenc
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Sokolovská	sokolovský	k2eAgFnSc1d1	Sokolovská
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kačerovem	Kačerov	k1gInSc7	Kačerov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
pak	pak	k6eAd1	pak
následovalo	následovat	k5eAaImAgNnS	následovat
zahájení	zahájení	k1gNnSc1	zahájení
navazujícího	navazující	k2eAgInSc2d1	navazující
úseku	úsek	k1gInSc2	úsek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
otevření	otevření	k1gNnSc6	otevření
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1980	[number]	k4	1980
přidal	přidat	k5eAaPmAgMnS	přidat
5,3	[number]	k4	5,3
km	km	kA	km
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
4	[number]	k4	4
stanice	stanice	k1gFnPc4	stanice
mezi	mezi	k7c7	mezi
Kačerovem	Kačerov	k1gInSc7	Kačerov
a	a	k8xC	a
Háji	háj	k1gInPc7	háj
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Kosmonautů	kosmonaut	k1gMnPc2	kosmonaut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1984	[number]	k4	1984
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
úsek	úsek	k1gInSc1	úsek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c7	za
Florencí	Florenc	k1gFnSc7	Florenc
přidal	přidat	k5eAaPmAgMnS	přidat
2,2	[number]	k4	2,2
km	km	kA	km
trati	trať	k1gFnSc2	trať
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
nové	nový	k2eAgFnPc1d1	nová
stanice	stanice	k1gFnPc1	stanice
(	(	kIx(	(
<g/>
Vltavská	vltavský	k2eAgFnSc1d1	Vltavská
a	a	k8xC	a
Nádraží	nádraží	k1gNnSc1	nádraží
Holešovice	Holešovice	k1gFnPc1	Holešovice
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
Fučíkova	Fučíkův	k2eAgFnSc1d1	Fučíkova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
4,0	[number]	k4	4,0
km	km	kA	km
byl	být	k5eAaImAgInS	být
zprovozněn	zprovozněn	k2eAgInSc4d1	zprovozněn
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
a	a	k8xC	a
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
stanice	stanice	k1gFnPc4	stanice
(	(	kIx(	(
<g/>
Kobylisy	Kobylisy	k1gInPc4	Kobylisy
a	a	k8xC	a
Ládví	Ládví	k1gNnSc4	Ládví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
4,6	[number]	k4	4,6
km	km	kA	km
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
stanicemi	stanice	k1gFnPc7	stanice
(	(	kIx(	(
<g/>
Střížkov	Střížkov	k1gInSc1	Střížkov
<g/>
,	,	kIx,	,
Prosek	proséct	k5eAaPmDgInS	proséct
a	a	k8xC	a
Letňany	Letňan	k1gMnPc4	Letňan
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Přepravní	přepravní	k2eAgFnSc1d1	přepravní
kapacita	kapacita	k1gFnSc1	kapacita
trasy	trasa	k1gFnSc2	trasa
C	C	kA	C
přepočtená	přepočtený	k2eAgFnSc1d1	přepočtená
na	na	k7c4	na
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
činí	činit	k5eAaImIp3nS	činit
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
cca	cca	kA	cca
26	[number]	k4	26
900	[number]	k4	900
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
cca	cca	kA	cca
12	[number]	k4	12
600	[number]	k4	600
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
tvoří	tvořit	k5eAaImIp3nS	tvořit
několik	několik	k4yIc1	několik
typů	typ	k1gInPc2	typ
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
typ	typ	k1gInSc1	typ
R1	R1	k1gFnSc2	R1
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
společností	společnost	k1gFnPc2	společnost
ČKD	ČKD	kA	ČKD
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
závod	závod	k1gInSc1	závod
Tatra	Tatra	k1gFnSc1	Tatra
Smíchov	Smíchov	k1gInSc4	Smíchov
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
kusy	kus	k1gInPc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
dodání	dodání	k1gNnSc6	dodání
vozů	vůz	k1gInPc2	vůz
Ečs	Ečs	k1gFnPc2	Ečs
sovětské	sovětský	k2eAgFnSc2d1	sovětská
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
nedostaly	dostat	k5eNaPmAgInP	dostat
se	se	k3xPyFc4	se
prototypy	prototyp	k1gInPc7	prototyp
nikdy	nikdy	k6eAd1	nikdy
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Smíchovský	smíchovský	k2eAgMnSc1d1	smíchovský
výrobce	výrobce	k1gMnSc1	výrobce
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
vytížení	vytížení	k1gNnSc4	vytížení
výrobních	výrobní	k2eAgFnPc2d1	výrobní
kapacit	kapacita	k1gFnPc2	kapacita
exportem	export	k1gInSc7	export
tramvají	tramvaj	k1gFnSc7	tramvaj
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
nestihl	stihnout	k5eNaPmAgMnS	stihnout
dodat	dodat	k5eAaPmF	dodat
včas	včas	k6eAd1	včas
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
počet	počet	k1gInSc4	počet
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
vůz	vůz	k1gInSc1	vůz
R1	R1	k1gFnSc2	R1
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
19	[number]	k4	19
cm	cm	kA	cm
širší	široký	k2eAgFnSc1d2	širší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
současné	současný	k2eAgInPc4d1	současný
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
nevešel	vejít	k5eNaPmAgMnS	vejít
<g/>
"	"	kIx"	"
do	do	k7c2	do
tunelu	tunel	k1gInSc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
R1	R1	k1gMnPc2	R1
byly	být	k5eAaImAgInP	být
odlehčené	odlehčený	k2eAgInPc1d1	odlehčený
díky	díky	k7c3	díky
použití	použití	k1gNnSc3	použití
laminátu	laminát	k1gInSc2	laminát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
politických	politický	k2eAgInPc2d1	politický
tlaků	tlak	k1gInPc2	tlak
bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xC	jako
další	další	k2eAgInSc1d1	další
důvod	důvod	k1gInSc1	důvod
dání	dání	k1gNnSc6	dání
přednosti	přednost	k1gFnSc2	přednost
sovětským	sovětský	k2eAgInSc7d1	sovětský
soupravám	souprava	k1gFnPc3	souprava
zdlouhavý	zdlouhavý	k2eAgInSc4d1	zdlouhavý
vývoj	vývoj	k1gInSc4	vývoj
souprav	souprava	k1gFnPc2	souprava
R1	R1	k1gFnSc2	R1
a	a	k8xC	a
obavy	obava	k1gFnSc2	obava
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
laminátové	laminátový	k2eAgInPc4d1	laminátový
prvky	prvek	k1gInPc4	prvek
ještě	ještě	k9	ještě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nevykazovaly	vykazovat	k5eNaImAgFnP	vykazovat
takové	takový	k3xDgFnPc4	takový
protipožární	protipožární	k2eAgFnPc4d1	protipožární
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
tunelech	tunel	k1gInPc6	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
prototypy	prototyp	k1gInPc1	prototyp
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
při	při	k7c6	při
záhadné	záhadný	k2eAgFnSc6d1	záhadná
nehodě	nehoda	k1gFnSc6	nehoda
v	v	k7c6	v
depu	depo	k1gNnSc6	depo
Kačerov	Kačerov	k1gInSc1	Kačerov
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
odstavené	odstavený	k2eAgFnSc2d1	odstavená
soupravy	souprava	k1gFnSc2	souprava
narazila	narazit	k5eAaPmAgFnS	narazit
souprava	souprava	k1gFnSc1	souprava
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
vládou	vláda	k1gFnSc7	vláda
byla	být	k5eAaImAgFnS	být
nehoda	nehoda	k1gFnSc1	nehoda
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
jako	jako	k8xC	jako
úplné	úplný	k2eAgNnSc1d1	úplné
selhání	selhání	k1gNnSc1	selhání
brzd	brzda	k1gFnPc2	brzda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
najde	najít	k5eAaPmIp3nS	najít
spousta	spousta	k1gFnSc1	spousta
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehoda	nehoda	k1gFnSc1	nehoda
byla	být	k5eAaImAgFnS	být
zinscenovaná	zinscenovaný	k2eAgFnSc1d1	zinscenovaná
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
konstrukcí	konstrukce	k1gFnSc7	konstrukce
se	se	k3xPyFc4	se
R1	R1	k1gMnSc1	R1
částečně	částečně	k6eAd1	částečně
podobal	podobat	k5eAaImAgMnS	podobat
dnešním	dnešní	k2eAgMnSc7d1	dnešní
vozům	vůz	k1gInPc3	vůz
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
právě	právě	k9	právě
u	u	k7c2	u
"	"	kIx"	"
<g/>
er-jedničky	erednička	k1gFnSc2	er-jednička
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
pracovníci	pracovník	k1gMnPc1	pracovník
ČKD	ČKD	kA	ČKD
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Dodány	dodán	k2eAgInPc1d1	dodán
byly	být	k5eAaImAgInP	být
tedy	tedy	k9	tedy
vozy	vůz	k1gInPc1	vůz
typu	typ	k1gInSc2	typ
Ečs	Ečs	k1gFnSc2	Ečs
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
v	v	k7c6	v
Mytiščinském	Mytiščinský	k2eAgInSc6d1	Mytiščinský
strojírenském	strojírenský	k2eAgInSc6d1	strojírenský
závodu	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
sovětské	sovětský	k2eAgInPc1d1	sovětský
vozy	vůz	k1gInPc1	vůz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
85	[number]	k4	85
ks	ks	kA	ks
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
jezdily	jezdit	k5eAaImAgFnP	jezdit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
C	C	kA	C
<g/>
;	;	kIx,	;
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
vyřazení	vyřazení	k1gNnSc6	vyřazení
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Třívozová	třívozový	k2eAgFnSc1d1	třívozová
souprava	souprava	k1gFnSc1	souprava
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
občas	občas	k6eAd1	občas
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
C	C	kA	C
jako	jako	k8xC	jako
muzejní	muzejní	k2eAgInPc1d1	muzejní
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vozů	vůz	k1gInPc2	vůz
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
také	také	k9	také
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
speciální	speciální	k2eAgFnSc6d1	speciální
podbetonované	podbetonovaný	k2eAgFnSc6d1	podbetonovaný
koleji	kolej	k1gFnSc6	kolej
i	i	k8xC	i
ve	v	k7c6	v
Vozovně	vozovna	k1gFnSc6	vozovna
Střešovice	Střešovice	k1gFnSc2	Střešovice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
typ	typ	k1gInSc1	typ
vlaků	vlak	k1gInPc2	vlak
tvoří	tvořit	k5eAaImIp3nS	tvořit
soupravy	souprava	k1gFnPc4	souprava
řady	řada	k1gFnSc2	řada
81-71	[number]	k4	81-71
též	též	k9	též
zkonstruované	zkonstruovaný	k2eAgInPc1d1	zkonstruovaný
v	v	k7c6	v
závodu	závod	k1gInSc6	závod
Mytišči	Mytišč	k1gInSc6	Mytišč
<g/>
.	.	kIx.	.
</s>
<s>
Skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
řídících	řídící	k2eAgInPc2d1	řídící
vozů	vůz	k1gInPc2	vůz
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
717.1	[number]	k4	717.1
a	a	k8xC	a
vložených	vložený	k2eAgInPc2d1	vložený
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
714.1	[number]	k4	714.1
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nástupci	nástupce	k1gMnPc1	nástupce
původních	původní	k2eAgFnPc2d1	původní
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
provedena	proveden	k2eAgNnPc1d1	provedeno
jistá	jistý	k2eAgNnPc1d1	jisté
vylepšení	vylepšení	k1gNnPc1	vylepšení
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nápadné	nápadný	k2eAgNnSc1d1	nápadné
například	například	k6eAd1	například
rozdělení	rozdělení	k1gNnSc4	rozdělení
vozů	vůz	k1gInPc2	vůz
právě	právě	k6eAd1	právě
na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
a	a	k8xC	a
vložené	vložený	k2eAgFnSc6d1	vložená
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
kapacita	kapacita	k1gFnSc1	kapacita
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
úpravy	úprava	k1gFnPc4	úprava
sovětský	sovětský	k2eAgMnSc1d1	sovětský
výrobce	výrobce	k1gMnSc1	výrobce
provedl	provést	k5eAaPmAgMnS	provést
jednak	jednak	k8xC	jednak
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
pražského	pražský	k2eAgInSc2d1	pražský
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
požadoval	požadovat	k5eAaImAgMnS	požadovat
například	například	k6eAd1	například
kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgInSc2	tento
od	od	k7c2	od
vlaků	vlak	k1gInPc2	vlak
i	i	k8xC	i
zvládnout	zvládnout	k5eAaPmF	zvládnout
vyšší	vysoký	k2eAgNnSc4d2	vyšší
stoupání	stoupání	k1gNnSc4	stoupání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vozy	vůz	k1gInPc1	vůz
mohly	moct	k5eAaImAgInP	moct
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
však	však	k9	však
aby	aby	kYmCp3nS	aby
splnil	splnit	k5eAaPmAgMnS	splnit
požadavky	požadavek	k1gInPc4	požadavek
provozovatelů	provozovatel	k1gMnPc2	provozovatel
domácích	domácí	k1gMnPc2	domácí
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
zase	zase	k9	zase
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
zvýšit	zvýšit	k5eAaPmF	zvýšit
obsaditelnost	obsaditelnost	k1gFnSc4	obsaditelnost
na	na	k7c6	na
vytížených	vytížený	k2eAgFnPc6d1	vytížená
linkách	linka	k1gFnPc6	linka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
soupravy	souprava	k1gFnSc2	souprava
typu	typ	k1gInSc2	typ
81-71	[number]	k4	81-71
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
a	a	k8xC	a
poslední	poslední	k2eAgFnSc2d1	poslední
nové	nový	k2eAgFnSc2d1	nová
pak	pak	k8xC	pak
výrobce	výrobce	k1gMnPc4	výrobce
dodal	dodat	k5eAaPmAgMnS	dodat
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
201	[number]	k4	201
čelních	čelní	k2eAgInPc2d1	čelní
a	a	k8xC	a
299	[number]	k4	299
vložených	vložený	k2eAgInPc2d1	vložený
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
soupravy	souprava	k1gFnPc1	souprava
dojezdily	dojezdit	k5eAaPmAgFnP	dojezdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Snahu	snaha	k1gFnSc4	snaha
řešit	řešit	k5eAaImF	řešit
zastaralost	zastaralost	k1gFnSc4	zastaralost
vozového	vozový	k2eAgInSc2d1	vozový
parku	park	k1gInSc2	park
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
modernizací	modernizace	k1gFnSc7	modernizace
některých	některý	k3yIgMnPc2	některý
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Zhoršující	zhoršující	k2eAgFnSc1d1	zhoršující
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
nutné	nutný	k2eAgInPc1d1	nutný
odstávky	odstávek	k1gInPc1	odstávek
původních	původní	k2eAgFnPc2d1	původní
souprav	souprava	k1gFnPc2	souprava
po	po	k7c6	po
ujetí	ujetí	k1gNnSc6	ujetí
maximálně	maximálně	k6eAd1	maximálně
1,44	[number]	k4	1,44
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nutila	nutit	k5eAaImAgFnS	nutit
již	již	k9	již
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výměnu	výměna	k1gFnSc4	výměna
veškeré	veškerý	k3xTgFnSc2	veškerý
elektroinstalace	elektroinstalace	k1gFnSc2	elektroinstalace
<g/>
,	,	kIx,	,
interiéru	interiér	k1gInSc2	interiér
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
provedla	provést	k5eAaPmAgFnS	provést
firma	firma	k1gFnSc1	firma
Škoda	škoda	k1gFnSc1	škoda
Transportation	Transportation	k1gInSc4	Transportation
a	a	k8xC	a
zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
tak	tak	k9	tak
souprava	souprava	k1gFnSc1	souprava
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
71	[number]	k4	71
<g/>
M.	M.	kA	M.
První	první	k4xOgFnSc1	první
souprava	souprava	k1gFnSc1	souprava
tohoto	tento	k3xDgNnSc2	tento
provedení	provedení	k1gNnSc2	provedení
vyjela	vyjet	k5eAaPmAgFnS	vyjet
do	do	k7c2	do
pražských	pražský	k2eAgInPc2d1	pražský
tunelů	tunel	k1gInPc2	tunel
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
s	s	k7c7	s
cestujícími	cestující	k1gFnPc7	cestující
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
dekádě	dekáda	k1gFnSc6	dekáda
pak	pak	k6eAd1	pak
následovaly	následovat	k5eAaImAgFnP	následovat
desítky	desítka	k1gFnPc1	desítka
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
linky	linka	k1gFnPc4	linka
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
životnosti	životnost	k1gFnSc2	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
vlaky	vlak	k1gInPc1	vlak
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
až	až	k9	až
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soupravy	souprava	k1gFnPc4	souprava
typu	typ	k1gInSc2	typ
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vyráběné	vyráběný	k2eAgInPc4d1	vyráběný
nejdříve	dříve	k6eAd3	dříve
konsorciem	konsorcium	k1gNnSc7	konsorcium
ČKD	ČKD	kA	ČKD
Dopravní	dopravní	k2eAgInSc1d1	dopravní
systémy-ADtranz-Siemens	systémy-ADtranz-Siemens	k1gInSc1	systémy-ADtranz-Siemens
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jen	jen	k9	jen
společností	společnost	k1gFnSc7	společnost
Siemens	siemens	k1gInSc1	siemens
AG	AG	kA	AG
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Zličíně	Zličín	k1gInSc6	Zličín
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
postupně	postupně	k6eAd1	postupně
nasazovány	nasazovat	k5eAaImNgFnP	nasazovat
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
C	C	kA	C
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
staly	stát	k5eAaPmAgFnP	stát
jedinými	jediný	k2eAgInPc7d1	jediný
provozovanými	provozovaný	k2eAgInPc7d1	provozovaný
typy	typ	k1gInPc7	typ
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
ještě	ještě	k6eAd1	ještě
mnoho	mnoho	k4c4	mnoho
pracovních	pracovní	k2eAgNnPc2d1	pracovní
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
již	již	k6eAd1	již
v	v	k7c6	v
časech	čas	k1gInPc6	čas
výstavby	výstavba	k1gFnSc2	výstavba
linky	linka	k1gFnSc2	linka
B	B	kA	B
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
10	[number]	k4	10
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
řady	řada	k1gFnSc2	řada
ČSD	ČSD	kA	ČSD
T	T	kA	T
212.1	[number]	k4	212.1
nevyhovovalo	vyhovovat	k5eNaImAgNnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
proto	proto	k8xC	proto
pořízeny	pořízen	k2eAgFnPc1d1	pořízena
nové	nový	k2eAgFnPc1d1	nová
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
již	již	k6eAd1	již
disponovaly	disponovat	k5eAaBmAgFnP	disponovat
úpravou	úprava	k1gFnSc7	úprava
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
s	s	k7c7	s
napájením	napájení	k1gNnSc7	napájení
přívodní	přívodní	k2eAgFnSc2d1	přívodní
kolejnicí	kolejnice	k1gFnSc7	kolejnice
(	(	kIx(	(
<g/>
řada	řada	k1gFnSc1	řada
797.8	[number]	k4	797.8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
pořizovány	pořizovat	k5eAaImNgFnP	pořizovat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
roku	rok	k1gInSc6	rok
2000	[number]	k4	2000
jich	on	k3xPp3gMnPc2	on
měl	mít	k5eAaImAgInS	mít
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
již	již	k6eAd1	již
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označený	k2eAgMnPc1d1	označený
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
interními	interní	k2eAgFnPc7d1	interní
čísly	číslo	k1gNnPc7	číslo
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
T1	T1	k1gFnSc2	T1
až	až	k9	až
T	T	kA	T
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
jedinými	jediný	k2eAgInPc7d1	jediný
pracovními	pracovní	k2eAgInPc7d1	pracovní
vozy	vůz	k1gInPc7	vůz
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
takové	takový	k3xDgNnSc1	takový
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
i	i	k9	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
motorové	motorový	k2eAgNnSc1d1	motorové
univerzální	univerzální	k2eAgNnSc1d1	univerzální
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
"	"	kIx"	"
WŽB	WŽB	kA	WŽB
10	[number]	k4	10
<g/>
-M	-M	k?	-M
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
provádění	provádění	k1gNnSc3	provádění
různých	různý	k2eAgFnPc2d1	různá
stavebních	stavební	k2eAgFnPc2d1	stavební
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
dovozu	dovoz	k1gInSc2	dovoz
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
svařovací	svařovací	k2eAgInSc4d1	svařovací
vůz	vůz	k1gInSc4	vůz
K	k	k7c3	k
355	[number]	k4	355
PT	PT	kA	PT
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
měřící	měřící	k2eAgInSc4d1	měřící
vůz	vůz	k1gInSc4	vůz
MATISA	MATISA	kA	MATISA
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
neboť	neboť	k8xC	neboť
tratě	trať	k1gFnPc1	trať
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
177	[number]	k4	177
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
drahách	draha	k1gFnPc6	draha
pravidelně	pravidelně	k6eAd1	pravidelně
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
<g/>
;	;	kIx,	;
metro	metro	k1gNnSc1	metro
samo	sám	k3xTgNnSc1	sám
má	mít	k5eAaImIp3nS	mít
vůbec	vůbec	k9	vůbec
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejpřísnějších	přísný	k2eAgFnPc2d3	nejpřísnější
norem	norma	k1gFnPc2	norma
ohledně	ohledně	k7c2	ohledně
provozních	provozní	k2eAgInPc2d1	provozní
parametrů	parametr	k1gInPc2	parametr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podbíjecí	podbíjecí	k2eAgInSc1d1	podbíjecí
vůz	vůz	k1gInSc1	vůz
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
podbíjení	podbíjení	k1gNnSc4	podbíjení
kolejí	kolej	k1gFnPc2	kolej
v	v	k7c6	v
úsecích	úsek	k1gInPc6	úsek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
štěrkové	štěrkový	k2eAgNnSc1d1	štěrkové
lože	lože	k1gNnSc1	lože
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cisternu	cisterna	k1gFnSc4	cisterna
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
vozy	vůz	k1gInPc1	vůz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
jezdí	jezdit	k5eAaImIp3nS	jezdit
od	od	k7c2	od
4-5	[number]	k4	4-5
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
až	až	k9	až
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
s	s	k7c7	s
pětivozovými	pětivozový	k2eAgFnPc7d1	pětivozový
soupravami	souprava	k1gFnPc7	souprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
také	také	k9	také
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
linky	linka	k1gFnPc4	linka
nasazovány	nasazován	k2eAgInPc1d1	nasazován
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
C	C	kA	C
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
provozní	provozní	k2eAgFnSc1d1	provozní
rychlost	rychlost	k1gFnSc1	rychlost
v	v	k7c6	v
tunelech	tunel	k1gInPc6	tunel
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgFnSc1d1	cestovní
rychlost	rychlost	k1gFnSc1	rychlost
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
včetně	včetně	k7c2	včetně
stání	stání	k1gNnSc2	stání
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
34,6	[number]	k4	34,6
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
V	v	k7c6	v
ranní	ranní	k2eAgFnSc6d1	ranní
špičce	špička	k1gFnSc6	špička
se	se	k3xPyFc4	se
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
89	[number]	k4	89
souprav	souprava	k1gFnPc2	souprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
50	[number]	k4	50
a	a	k8xC	a
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
33	[number]	k4	33
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopravních	dopravní	k2eAgFnPc6d1	dopravní
špičkách	špička	k1gFnPc6	špička
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgInPc1d1	krátký
intervaly	interval	k1gInPc1	interval
již	již	k6eAd1	již
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
zabezpečovací	zabezpečovací	k2eAgNnSc4d1	zabezpečovací
zařízení	zařízení	k1gNnSc4	zařízení
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
další	další	k2eAgNnSc1d1	další
zkrácení	zkrácení	k1gNnSc1	zkrácení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jízdních	jízdní	k2eAgInPc2d1	jízdní
řádů	řád	k1gInPc2	řád
platných	platný	k2eAgInPc2d1	platný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
činí	činit	k5eAaImIp3nS	činit
nejkratší	krátký	k2eAgInSc1d3	nejkratší
interval	interval	k1gInSc1	interval
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	a	k9	a
150	[number]	k4	150
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
B	B	kA	B
140	[number]	k4	140
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
C	C	kA	C
115	[number]	k4	115
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
zřizován	zřizován	k2eAgInSc4d1	zřizován
orientační	orientační	k2eAgInSc4d1	orientační
a	a	k8xC	a
informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
v	v	k7c6	v
přestupních	přestupní	k2eAgInPc6d1	přestupní
uzlech	uzel	k1gInPc6	uzel
mimo	mimo	k7c4	mimo
stanice	stanice	k1gFnPc4	stanice
metra	metro	k1gNnSc2	metro
dosud	dosud	k6eAd1	dosud
většinou	většinou	k6eAd1	většinou
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staničním	staniční	k2eAgInSc6d1	staniční
tunelu	tunel	k1gInSc6	tunel
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
umístěno	umístěn	k2eAgNnSc1d1	umístěno
výrazné	výrazný	k2eAgNnSc1d1	výrazné
liniové	liniový	k2eAgNnSc1d1	liniové
schéma	schéma	k1gNnSc1	schéma
příslušné	příslušný	k2eAgFnSc2d1	příslušná
linky	linka	k1gFnSc2	linka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vzhledu	vzhled	k1gInSc2	vzhled
přezdívané	přezdívaný	k2eAgMnPc4d1	přezdívaný
"	"	kIx"	"
<g/>
teploměr	teploměr	k1gInSc1	teploměr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
vyznačenými	vyznačený	k2eAgFnPc7d1	vyznačená
stanicemi	stanice	k1gFnPc7	stanice
a	a	k8xC	a
přestupy	přestup	k1gInPc7	přestup
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
linky	linka	k1gFnPc4	linka
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
staničním	staniční	k2eAgInSc6d1	staniční
tunelu	tunel	k1gInSc6	tunel
bývá	bývat	k5eAaImIp3nS	bývat
umístěno	umístit	k5eAaPmNgNnS	umístit
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
ose	osa	k1gFnSc3	osa
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
šipkami	šipka	k1gFnPc7	šipka
je	být	k5eAaImIp3nS	být
naznačeno	naznačen	k2eAgNnSc1d1	naznačeno
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
stanic	stanice	k1gFnPc2	stanice
lze	lze	k6eAd1	lze
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
koleje	kolej	k1gFnSc2	kolej
cestovat	cestovat	k5eAaImF	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
koleje	kolej	k1gFnSc2	kolej
nebo	nebo	k8xC	nebo
u	u	k7c2	u
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
bočnímu	boční	k2eAgMnSc3d1	boční
(	(	kIx(	(
<g/>
traťovému	traťový	k2eAgInSc3d1	traťový
<g/>
)	)	kIx)	)
staničnímu	staniční	k2eAgInSc3d1	staniční
tunelu	tunel	k1gInSc3	tunel
bývá	bývat	k5eAaImIp3nS	bývat
schéma	schéma	k1gNnSc1	schéma
v	v	k7c6	v
jednosměrné	jednosměrný	k2eAgFnSc6d1	jednosměrná
variantě	varianta	k1gFnSc6	varianta
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnPc1d1	umístěná
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
po	po	k7c6	po
zkušenostech	zkušenost	k1gFnPc6	zkušenost
s	s	k7c7	s
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
povodňovou	povodňový	k2eAgFnSc7d1	povodňová
dopravou	doprava	k1gFnSc7	doprava
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
informační	informační	k2eAgInPc1d1	informační
prvky	prvek	k1gInPc1	prvek
doplněny	doplněn	k2eAgInPc1d1	doplněn
o	o	k7c4	o
čísla	číslo	k1gNnPc4	číslo
kolejí	kolej	k1gFnPc2	kolej
(	(	kIx(	(
<g/>
kolej	kolej	k1gFnSc1	kolej
1	[number]	k4	1
<g/>
,	,	kIx,	,
kolej	kolej	k1gFnSc1	kolej
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
stěně	stěna	k1gFnSc6	stěna
traťového	traťový	k2eAgInSc2d1	traťový
tunelu	tunel	k1gInSc2	tunel
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
několika	několik	k4yIc6	několik
místech	místo	k1gNnPc6	místo
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
název	název	k1gInSc1	název
stanice	stanice	k1gFnSc2	stanice
kovovými	kovový	k2eAgInPc7d1	kovový
reliéfními	reliéfní	k2eAgInPc7d1	reliéfní
písmeny	písmeno	k1gNnPc7	písmeno
doplněným	doplněný	k2eAgInSc7d1	doplněný
čtvercovou	čtvercový	k2eAgFnSc7d1	čtvercová
deskou	deska	k1gFnSc7	deska
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
trasy	trasa	k1gFnSc2	trasa
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
pro	pro	k7c4	pro
trasu	trasa	k1gFnSc4	trasa
A	A	kA	A
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
pro	pro	k7c4	pro
trasu	trasa	k1gFnSc4	trasa
B	B	kA	B
a	a	k8xC	a
červená	červenat	k5eAaImIp3nS	červenat
pro	pro	k7c4	pro
trasu	trasa	k1gFnSc4	trasa
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
nástupištěm	nástupiště	k1gNnSc7	nástupiště
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
staničním	staniční	k2eAgInSc6d1	staniční
tunelu	tunel	k1gInSc6	tunel
bývá	bývat	k5eAaImIp3nS	bývat
název	název	k1gInSc1	název
stanice	stanice	k1gFnSc2	stanice
vyznačen	vyznačit	k5eAaPmNgInS	vyznačit
na	na	k7c6	na
tabulích	tabule	k1gFnPc6	tabule
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
samolepkách	samolepka	k1gFnPc6	samolepka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staničních	staniční	k2eAgInPc6d1	staniční
tunelech	tunel	k1gInPc6	tunel
i	i	k8xC	i
v	v	k7c6	v
halách	hala	k1gFnPc6	hala
(	(	kIx(	(
<g/>
vestibulech	vestibul	k1gInPc6	vestibul
<g/>
)	)	kIx)	)
a	a	k8xC	a
chodbách	chodba	k1gFnPc6	chodba
stanic	stanice	k1gFnPc2	stanice
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgMnPc4d1	umístěn
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
grafickém	grafický	k2eAgNnSc6d1	grafické
provedení	provedení	k1gNnSc6	provedení
navigační	navigační	k2eAgFnSc2d1	navigační
tabule	tabule	k1gFnSc2	tabule
k	k	k7c3	k
výstupům	výstup	k1gInPc3	výstup
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
k	k	k7c3	k
přestupům	přestup	k1gInPc3	přestup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
stanicích	stanice	k1gFnPc6	stanice
zřizován	zřizován	k2eAgInSc1d1	zřizován
obdobný	obdobný	k2eAgInSc1d1	obdobný
informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
i	i	k9	i
v	v	k7c6	v
přestupních	přestupní	k2eAgInPc6d1	přestupní
uzlech	uzel	k1gInPc6	uzel
a	a	k8xC	a
autobusových	autobusový	k2eAgInPc6d1	autobusový
terminálech	terminál	k1gInPc6	terminál
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
u	u	k7c2	u
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
Holešovice	Holešovice	k1gFnPc1	Holešovice
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgNnSc1d1	jižní
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupy	vstup	k1gInPc1	vstup
do	do	k7c2	do
stanic	stanice	k1gFnPc2	stanice
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
většinou	většinou	k6eAd1	většinou
světelnými	světelný	k2eAgInPc7d1	světelný
nebo	nebo	k8xC	nebo
osvětlenými	osvětlený	k2eAgInPc7d1	osvětlený
piktogramy	piktogram	k1gInPc7	piktogram
se	s	k7c7	s
stylizovaným	stylizovaný	k2eAgInSc7d1	stylizovaný
znakem	znak	k1gInSc7	znak
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
písmeno	písmeno	k1gNnSc1	písmeno
M	M	kA	M
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
třemi	tři	k4xCgFnPc7	tři
šipkami	šipka	k1gFnPc7	šipka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgNnSc1d1	grafické
provedení	provedení	k1gNnSc1	provedení
navigačních	navigační	k2eAgFnPc2d1	navigační
tabulí	tabule	k1gFnPc2	tabule
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
přibližně	přibližně	k6eAd1	přibližně
dvakrát	dvakrát	k6eAd1	dvakrát
výrazně	výrazně	k6eAd1	výrazně
změněno	změnit	k5eAaPmNgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
byly	být	k5eAaImAgInP	být
vestibuly	vestibul	k1gInPc1	vestibul
stanic	stanice	k1gFnPc2	stanice
vybavovány	vybavovat	k5eAaImNgInP	vybavovat
plánkem	plánek	k1gInSc7	plánek
okolí	okolí	k1gNnSc2	okolí
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
schématem	schéma	k1gNnSc7	schéma
sítě	síť	k1gFnSc2	síť
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
informační	informační	k2eAgInPc1d1	informační
prvky	prvek	k1gInPc1	prvek
byly	být	k5eAaImAgInP	být
nedostatečně	dostatečně	k6eNd1	dostatečně
udržovány	udržován	k2eAgInPc1d1	udržován
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
odstraněny	odstranit	k5eAaPmNgInP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
století	století	k1gNnSc2	století
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
stanice	stanice	k1gFnPc4	stanice
vybavovány	vybavován	k2eAgFnPc4d1	vybavována
informačními	informační	k2eAgInPc7d1	informační
panely	panel	k1gInPc7	panel
AWK	AWK	kA	AWK
<g/>
,	,	kIx,	,
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
dopravní	dopravní	k2eAgFnSc4d1	dopravní
mapu	mapa	k1gFnSc4	mapa
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
stálé	stálý	k2eAgInPc4d1	stálý
i	i	k8xC	i
příležitostné	příležitostný	k2eAgInPc4d1	příležitostný
informační	informační	k2eAgInPc4d1	informační
letáky	leták	k1gInPc4	leták
a	a	k8xC	a
staniční	staniční	k2eAgInSc4d1	staniční
jízdní	jízdní	k2eAgInSc4d1	jízdní
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Staniční	staniční	k2eAgInSc1d1	staniční
jízdní	jízdní	k2eAgInSc1d1	jízdní
řád	řád	k1gInSc1	řád
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
podrobný	podrobný	k2eAgInSc4d1	podrobný
rozpis	rozpis	k1gInSc4	rozpis
spojů	spoj	k1gInPc2	spoj
–	–	k?	–
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
pouze	pouze	k6eAd1	pouze
rozmezí	rozmezí	k1gNnSc4	rozmezí
intervalů	interval	k1gInPc2	interval
a	a	k8xC	a
údaj	údaj	k1gInSc4	údaj
o	o	k7c6	o
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
posledním	poslední	k2eAgInSc6d1	poslední
spoji	spoj	k1gInSc6	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
každých	každý	k3xTgFnPc2	každý
dveří	dveře	k1gFnPc2	dveře
umístěno	umístěn	k2eAgNnSc1d1	umístěno
schéma	schéma	k1gNnSc1	schéma
sítě	síť	k1gFnSc2	síť
metra	metro	k1gNnSc2	metro
–	–	k?	–
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
umisťováno	umisťován	k2eAgNnSc1d1	umisťován
mapové	mapový	k2eAgNnSc1d1	mapové
schéma	schéma	k1gNnSc1	schéma
vedle	vedle	k7c2	vedle
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
liniové	liniový	k2eAgNnSc4d1	liniové
schéma	schéma	k1gNnSc4	schéma
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
linek	linka	k1gFnPc2	linka
nad	nad	k7c7	nad
každými	každý	k3xTgFnPc7	každý
dveřmi	dveře	k1gFnPc7	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
přelomu	přelom	k1gInSc2	přelom
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
stanice	stanice	k1gFnPc4	stanice
vybavovány	vybavován	k2eAgFnPc4d1	vybavována
akustickými	akustický	k2eAgNnPc7d1	akustické
i	i	k8xC	i
mechanickými	mechanický	k2eAgNnPc7d1	mechanické
navigačními	navigační	k2eAgNnPc7d1	navigační
zařízeními	zařízení	k1gNnPc7	zařízení
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
(	(	kIx(	(
<g/>
houkátka	houkátka	k1gFnSc1	houkátka
<g/>
,	,	kIx,	,
vodicí	vodicí	k2eAgFnPc1d1	vodicí
drážky	drážka	k1gFnPc1	drážka
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nástupiště	nástupiště	k1gNnSc2	nástupiště
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
stanic	stanice	k1gFnPc2	stanice
jsou	být	k5eAaImIp3nP	být
umístěny	umístěn	k2eAgFnPc1d1	umístěna
hodiny	hodina	k1gFnPc1	hodina
s	s	k7c7	s
aktuálním	aktuální	k2eAgInSc7d1	aktuální
časem	čas	k1gInSc7	čas
v	v	k7c6	v
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
minutách	minuta	k1gFnPc6	minuta
a	a	k8xC	a
sekundách	sekunda	k1gFnPc6	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nástupiště	nástupiště	k1gNnSc2	nástupiště
hodiny	hodina	k1gFnSc2	hodina
navíc	navíc	k6eAd1	navíc
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
počet	počet	k1gInSc4	počet
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
sekund	sekunda	k1gFnPc2	sekunda
od	od	k7c2	od
odjezdu	odjezd	k1gInSc2	odjezd
posledního	poslední	k2eAgInSc2d1	poslední
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
frekventovaných	frekventovaný	k2eAgFnPc6d1	frekventovaná
stanicích	stanice	k1gFnPc6	stanice
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
traťového	traťový	k2eAgInSc2d1	traťový
tunelu	tunel	k1gInSc2	tunel
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
umístěna	umístit	k5eAaPmNgFnS	umístit
projekční	projekční	k2eAgMnPc1d1	projekční
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
promítáno	promítán	k2eAgNnSc1d1	promítáno
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
plátna	plátno	k1gNnSc2	plátno
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
konečné	konečný	k2eAgFnSc6d1	konečná
stanici	stanice	k1gFnSc6	stanice
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
vlaku	vlak	k1gInSc2	vlak
a	a	k8xC	a
o	o	k7c6	o
době	doba	k1gFnSc6	doba
zbývající	zbývající	k2eAgInSc4d1	zbývající
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
příjezdu	příjezd	k1gInSc2	příjezd
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
odjezdu	odjezd	k1gInSc2	odjezd
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
stanicích	stanice	k1gFnPc6	stanice
jsou	být	k5eAaImIp3nP	být
nad	nad	k7c7	nad
nástupištěm	nástupiště	k1gNnSc7	nástupiště
proměnné	proměnná	k1gFnSc2	proměnná
informační	informační	k2eAgInPc4d1	informační
panely	panel	k1gInPc4	panel
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
zobrazovány	zobrazován	k2eAgFnPc4d1	zobrazována
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
případném	případný	k2eAgNnSc6d1	případné
ukončení	ukončení	k1gNnSc6	ukončení
vlaku	vlak	k1gInSc2	vlak
před	před	k7c7	před
nejzazší	zadní	k2eAgFnSc7d3	nejzazší
konečnou	konečný	k2eAgFnSc7d1	konečná
stanicí	stanice	k1gFnSc7	stanice
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlak	vlak	k1gInSc1	vlak
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
i	i	k9	i
o	o	k7c4	o
časové	časový	k2eAgInPc4d1	časový
údaje	údaj	k1gInPc4	údaj
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
aktuální	aktuální	k2eAgInSc4d1	aktuální
čas	čas	k1gInSc4	čas
a	a	k8xC	a
čas	čas	k1gInSc4	čas
do	do	k7c2	do
příjezdu	příjezd	k1gInSc2	příjezd
další	další	k2eAgFnSc2d1	další
soupravy	souprava	k1gFnSc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
frekventovaných	frekventovaný	k2eAgFnPc6d1	frekventovaná
stanicích	stanice	k1gFnPc6	stanice
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
staničním	staniční	k2eAgInSc6d1	staniční
tunelu	tunel	k1gInSc6	tunel
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
umístěny	umístit	k5eAaPmNgInP	umístit
informační	informační	k2eAgInPc1d1	informační
stojany	stojan	k1gInPc1	stojan
umožňující	umožňující	k2eAgFnSc4d1	umožňující
hlasovou	hlasový	k2eAgFnSc4d1	hlasová
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
informačním	informační	k2eAgNnSc7d1	informační
střediskem	středisko	k1gNnSc7	středisko
a	a	k8xC	a
dotyková	dotykový	k2eAgFnSc1d1	dotyková
obrazovka	obrazovka	k1gFnSc1	obrazovka
počítače	počítač	k1gInSc2	počítač
umožňující	umožňující	k2eAgNnSc4d1	umožňující
vyhledání	vyhledání	k1gNnSc4	vyhledání
dopravních	dopravní	k2eAgFnPc2d1	dopravní
i	i	k8xC	i
turistických	turistický	k2eAgFnPc2d1	turistická
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
orientace	orientace	k1gFnSc2	orientace
cestujících	cestující	k1gMnPc2	cestující
funguje	fungovat	k5eAaImIp3nS	fungovat
ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
provozu	provoz	k1gInSc2	provoz
metra	metro	k1gNnSc2	metro
hlášení	hlášení	k1gNnSc2	hlášení
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
důležitých	důležitý	k2eAgFnPc2d1	důležitá
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příjezdu	příjezd	k1gInSc6	příjezd
vlaku	vlak	k1gInSc2	vlak
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
jsou	být	k5eAaImIp3nP	být
cestující	cestující	k1gMnPc1	cestující
informováni	informovat	k5eAaBmNgMnP	informovat
<g/>
,	,	kIx,	,
o	o	k7c4	o
jakou	jaký	k3yIgFnSc4	jaký
stanici	stanice	k1gFnSc4	stanice
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
a	a	k8xC	a
zdali	zdali	k8xS	zdali
je	být	k5eAaImIp3nS	být
přestupní	přestupní	k2eAgFnSc1d1	přestupní
nebo	nebo	k8xC	nebo
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
,	,	kIx,	,
hlásí	hlásit	k5eAaImIp3nS	hlásit
se	se	k3xPyFc4	se
také	také	k9	také
podstatné	podstatný	k2eAgFnPc1d1	podstatná
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
východů	východ	k1gInPc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
pásmového	pásmový	k2eAgInSc2d1	pásmový
provozu	provoz	k1gInSc2	provoz
či	či	k8xC	či
výluky	výluka	k1gFnSc2	výluka
je	být	k5eAaImIp3nS	být
ohlášena	ohlášen	k2eAgFnSc1d1	ohlášena
změna	změna	k1gFnSc1	změna
konečné	konečný	k2eAgFnSc2d1	konečná
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
u	u	k7c2	u
původních	původní	k2eAgInPc2d1	původní
vozů	vůz	k1gInPc2	vůz
zcela	zcela	k6eAd1	zcela
chyběla	chybět	k5eAaImAgFnS	chybět
optická	optický	k2eAgFnSc1d1	optická
i	i	k8xC	i
zvuková	zvukový	k2eAgFnSc1d1	zvuková
signalizace	signalizace	k1gFnSc1	signalizace
před	před	k7c7	před
uzavřením	uzavření	k1gNnSc7	uzavření
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
v	v	k7c6	v
povrchové	povrchový	k2eAgFnSc6d1	povrchová
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
cestující	cestující	k1gMnPc1	cestující
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
provozu	provoz	k1gInSc2	provoz
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
upozorňování	upozorňování	k1gNnSc4	upozorňování
slovním	slovní	k2eAgNnSc7d1	slovní
hlášením	hlášení	k1gNnSc7	hlášení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zpočátku	zpočátku	k6eAd1	zpočátku
znělo	znět	k5eAaImAgNnS	znět
Ukončete	ukončit	k5eAaPmRp2nP	ukončit
nástup	nástup	k1gInSc4	nástup
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
se	se	k3xPyFc4	se
zavírají	zavírat	k5eAaImIp3nP	zavírat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Ukončete	ukončit	k5eAaPmRp2nP	ukončit
výstup	výstup	k1gInSc4	výstup
a	a	k8xC	a
nástup	nástup	k1gInSc4	nástup
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
se	se	k3xPyFc4	se
zavírají	zavírat	k5eAaImIp3nP	zavírat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c6	na
Ukončete	ukončit	k5eAaPmRp2nP	ukončit
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
výstup	výstup	k1gInSc1	výstup
a	a	k8xC	a
nástup	nástup	k1gInSc1	nástup
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
se	se	k3xPyFc4	se
zavírají	zavírat	k5eAaImIp3nP	zavírat
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zavření	zavření	k1gNnSc6	zavření
dveří	dveře	k1gFnPc2	dveře
a	a	k8xC	a
rozjezdu	rozjezd	k1gInSc2	rozjezd
soupravy	souprava	k1gFnSc2	souprava
je	být	k5eAaImIp3nS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
název	název	k1gInSc1	název
příští	příští	k2eAgFnSc2d1	příští
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Hlášení	hlášení	k1gNnSc4	hlášení
stanic	stanice	k1gFnPc2	stanice
namluvila	namluvit	k5eAaPmAgFnS	namluvit
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
B	B	kA	B
Eva	Eva	k1gFnSc1	Eva
Jurinová	Jurinová	k1gFnSc1	Jurinová
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
FN	FN	kA	FN
Motol	Motol	k1gInSc1	Motol
a	a	k8xC	a
hlasatelka	hlasatelka	k1gFnSc1	hlasatelka
zpráv	zpráva	k1gFnPc2	zpráva
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	a	k9	a
Světlana	Světlana	k1gFnSc1	Světlana
Lavičková	lavičkový	k2eAgFnSc1d1	lavičková
(	(	kIx(	(
<g/>
moderátorka	moderátorka	k1gFnSc1	moderátorka
ČRo	ČRo	k1gFnSc1	ČRo
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lince	Linec	k1gInPc1	Linec
C	C	kA	C
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
hlas	hlas	k1gInSc4	hlas
hlasatel	hlasatel	k1gMnSc1	hlasatel
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
ČRo	ČRo	k1gFnSc2	ČRo
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
Tomáš	Tomáš	k1gMnSc1	Tomáš
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
stanicích	stanice	k1gFnPc6	stanice
s	s	k7c7	s
přestupem	přestup	k1gInSc7	přestup
na	na	k7c4	na
příměstské	příměstský	k2eAgInPc4d1	příměstský
vlaky	vlak	k1gInPc4	vlak
vyhlašováno	vyhlašován	k2eAgNnSc4d1	vyhlašováno
hlášení	hlášení	k1gNnSc4	hlášení
Přestup	přestup	k1gInSc4	přestup
na	na	k7c4	na
linky	linka	k1gFnPc4	linka
S	s	k7c7	s
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
vlakové	vlakový	k2eAgInPc1d1	vlakový
spoje	spoj	k1gInPc1	spoj
(	(	kIx(	(
<g/>
prvních	první	k4xOgInPc2	první
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c4	v
znění	znění	k1gNnSc4	znění
Přestup	přestup	k1gInSc4	přestup
na	na	k7c4	na
vlaky	vlak	k1gInPc4	vlak
linky	linka	k1gFnSc2	linka
S	s	k7c7	s
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
spoje	spoj	k1gInPc1	spoj
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konečných	konečný	k2eAgFnPc6d1	konečná
stanicích	stanice	k1gFnPc6	stanice
jsou	být	k5eAaImIp3nP	být
cestující	cestující	k1gMnPc1	cestující
upozorněni	upozornit	k5eAaPmNgMnP	upozornit
hlášením	hlášení	k1gNnSc7	hlášení
Konečná	Konečná	k1gFnSc1	Konečná
stanice	stanice	k1gFnPc1	stanice
<g/>
,	,	kIx,	,
prosíme	prosit	k5eAaImIp1nP	prosit
<g/>
,	,	kIx,	,
vystupte	vystoupit	k5eAaPmRp2nP	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Terminal	Terminat	k5eAaPmAgInS	Terminat
station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
please	please	k6eAd1	please
exit	exit	k1gInSc1	exit
the	the	k?	the
train	train	k1gInSc1	train
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
znělo	znět	k5eAaImAgNnS	znět
anglické	anglický	k2eAgNnSc1d1	anglické
hlášení	hlášení	k1gNnSc1	hlášení
Terminus	terminus	k1gInSc1	terminus
<g/>
,	,	kIx,	,
please	please	k6eAd1	please
leave	leavat	k5eAaPmIp3nS	leavat
the	the	k?	the
train	train	k1gInSc1	train
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
Prosek	proséct	k5eAaPmDgInS	proséct
<g/>
,	,	kIx,	,
Střížkov	Střížkov	k1gInSc1	Střížkov
<g/>
,	,	kIx,	,
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
,	,	kIx,	,
Rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
směr	směr	k1gInSc4	směr
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
a	a	k8xC	a
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
hlášení	hlášení	k1gNnSc4	hlášení
při	při	k7c6	při
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c6	o
Vystupujte	vystupovat	k5eAaImRp2nP	vystupovat
prosím	prosit	k5eAaImIp1nS	prosit
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
na	na	k7c6	na
Černém	černý	k2eAgInSc6d1	černý
Mostě	most	k1gInSc6	most
a	a	k8xC	a
NM	NM	kA	NM
je	být	k5eAaImIp3nS	být
hlášení	hlášení	k1gNnPc4	hlášení
o	o	k7c4	o
konečné	konečný	k2eAgNnSc4d1	konečné
ořezáno	ořezán	k2eAgNnSc4d1	ořezáno
o	o	k7c4	o
anglické	anglický	k2eAgNnSc4d1	anglické
znění	znění	k1gNnSc4	znění
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
Černý	černý	k2eAgInSc1d1	černý
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Vystupujte	vystupovat	k5eAaImRp2nP	vystupovat
prosím	prosit	k5eAaImIp1nS	prosit
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
<g/>
Terminal	Terminal	k1gFnSc1	Terminal
station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
please	please	k6eAd1	please
exit	exit	k1gInSc1	exit
the	the	k?	the
train	train	k1gInSc1	train
<g/>
;	;	kIx,	;
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
končící	končící	k2eAgInSc1d1	končící
vlak	vlak	k1gInSc1	vlak
přejede	přejet	k5eAaPmIp3nS	přejet
před	před	k7c7	před
stanicí	stanice	k1gFnSc7	stanice
do	do	k7c2	do
protisměru	protisměr	k1gInSc2	protisměr
<g/>
,	,	kIx,	,
zase	zase	k9	zase
chybí	chybit	k5eAaPmIp3nS	chybit
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
strany	strana	k1gFnSc2	strana
výstupu	výstup	k1gInSc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Skalka	skalka	k1gFnSc1	skalka
je	být	k5eAaImIp3nS	být
také	také	k9	také
těžké	těžký	k2eAgNnSc1d1	těžké
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
kolej	kolej	k1gFnSc4	kolej
vlak	vlak	k1gInSc1	vlak
pojede	pojet	k5eAaPmIp3nS	pojet
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zde	zde	k6eAd1	zde
končí	končit	k5eAaImIp3nS	končit
–	–	k?	–
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
,	,	kIx,	,
jede	jet	k5eAaImIp3nS	jet
na	na	k7c4	na
kolej	kolej	k1gFnSc4	kolej
2	[number]	k4	2
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
však	však	k9	však
jede	jet	k5eAaImIp3nS	jet
do	do	k7c2	do
depa	depo	k1gNnSc2	depo
<g/>
,	,	kIx,	,
přijede	přijet	k5eAaPmIp3nS	přijet
na	na	k7c4	na
kolej	kolej	k1gFnSc4	kolej
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vlak	vlak	k1gInSc1	vlak
jede	jet	k5eAaImIp3nS	jet
do	do	k7c2	do
depa	depo	k1gNnSc2	depo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
třech	tři	k4xCgFnPc2	tři
předcházejících	předcházející	k2eAgFnPc2d1	předcházející
stanic	stanice	k1gFnPc2	stanice
přidá	přidat	k5eAaPmIp3nS	přidat
hlášení	hlášení	k1gNnSc1	hlášení
např.	např.	kA	např.
Želivského	želivský	k2eAgInSc2d1	želivský
<g/>
.	.	kIx.	.
</s>
<s>
Upozorňujeme	upozorňovat	k5eAaImIp1nP	upozorňovat
cestující	cestující	k1gMnPc4	cestující
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
vlak	vlak	k1gInSc1	vlak
končí	končit	k5eAaImIp3nS	končit
jízdu	jízda	k1gFnSc4	jízda
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
koncové	koncový	k2eAgFnSc2d1	koncová
stanice	stanice	k1gFnSc2	stanice
se	se	k3xPyFc4	se
spustí	spustit	k5eAaPmIp3nP	spustit
trojjazyčné	trojjazyčný	k2eAgInPc1d1	trojjazyčný
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
EN	EN	kA	EN
<g/>
,	,	kIx,	,
DE	DE	k?	DE
<g/>
)	)	kIx)	)
staniční	staniční	k2eAgNnSc1d1	staniční
hlášení	hlášení	k1gNnSc1	hlášení
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Vlak	vlak	k1gInSc1	vlak
na	na	k7c6	na
první	první	k4xOgFnSc6	první
koleji	kolej	k1gFnSc6	kolej
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Depo	depo	k1gNnSc4	depo
Hostivař	Hostivař	k1gFnSc1	Hostivař
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Nenastupujte	nastupovat	k5eNaImRp2nP	nastupovat
prosím	prosit	k5eAaImIp1nS	prosit
do	do	k7c2	do
soupravy	souprava	k1gFnSc2	souprava
a	a	k8xC	a
nepřekračujte	překračovat	k5eNaImRp2nP	překračovat
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
pás	pás	k1gInSc4	pás
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
a	a	k8xC	a
rekonstruované	rekonstruovaný	k2eAgInPc1d1	rekonstruovaný
vozy	vůz	k1gInPc1	vůz
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
světelnou	světelný	k2eAgFnSc4d1	světelná
signalizaci	signalizace	k1gFnSc4	signalizace
LED	LED	kA	LED
upozorňující	upozorňující	k2eAgNnSc1d1	upozorňující
na	na	k7c4	na
zavírající	zavírající	k2eAgNnSc4d1	zavírající
se	se	k3xPyFc4	se
dveře	dveře	k1gFnPc1	dveře
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
světelný	světelný	k2eAgInSc4d1	světelný
informační	informační	k2eAgInSc4d1	informační
panel	panel	k1gInSc4	panel
s	s	k7c7	s
názvy	název	k1gInPc7	název
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlášení	hlášení	k1gNnSc2	hlášení
ve	v	k7c6	v
vlacích	vlak	k1gInPc6	vlak
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
staniční	staniční	k2eAgMnSc1d1	staniční
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
cestující	cestující	k1gMnPc4	cestující
jednak	jednak	k8xC	jednak
na	na	k7c4	na
základní	základní	k2eAgNnSc4d1	základní
bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
opatření	opatření	k1gNnSc4	opatření
(	(	kIx(	(
<g/>
zákaz	zákaz	k1gInSc1	zákaz
překračování	překračování	k1gNnSc2	překračování
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
pásu	pás	k1gInSc2	pás
na	na	k7c6	na
nástupišti	nástupiště	k1gNnSc6	nástupiště
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
zde	zde	k6eAd1	zde
přítomna	přítomen	k2eAgFnSc1d1	přítomna
souprava	souprava	k1gFnSc1	souprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
důležité	důležitý	k2eAgFnPc4d1	důležitá
události	událost	k1gFnPc4	událost
jako	jako	k8xS	jako
například	například	k6eAd1	například
dopravní	dopravní	k2eAgFnSc2d1	dopravní
změny	změna	k1gFnSc2	změna
(	(	kIx(	(
<g/>
zavedení	zavedení	k1gNnSc1	zavedení
speciálních	speciální	k2eAgFnPc2d1	speciální
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
výluky	výluka	k1gFnPc1	výluka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
či	či	k8xC	či
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
zastavení	zastavení	k1gNnSc3	zastavení
či	či	k8xC	či
spouštění	spouštění	k1gNnSc3	spouštění
eskalátorů	eskalátor	k1gInPc2	eskalátor
<g/>
,	,	kIx,	,
uzavření	uzavření	k1gNnSc1	uzavření
stanice	stanice	k1gFnSc2	stanice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
ohlašují	ohlašovat	k5eAaImIp3nP	ohlašovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
kulturních	kulturní	k2eAgFnPc6d1	kulturní
a	a	k8xC	a
sportovních	sportovní	k2eAgFnPc6d1	sportovní
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
jeho	on	k3xPp3gInSc2	on
odbavovacího	odbavovací	k2eAgInSc2d1	odbavovací
systému	systém	k1gInSc2	systém
dotkly	dotknout	k5eAaPmAgFnP	dotknout
dvě	dva	k4xCgFnPc1	dva
významnější	významný	k2eAgFnPc1d2	významnější
změny	změna	k1gFnPc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
stanice	stanice	k1gFnPc4	stanice
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
vstupech	vstup	k1gInPc6	vstup
i	i	k8xC	i
výstupech	výstup	k1gInPc6	výstup
vybaveny	vybaven	k2eAgInPc4d1	vybaven
mincovními	mincovní	k2eAgInPc7d1	mincovní
turnikety	turniket	k1gInPc7	turniket
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mechanickými	mechanický	k2eAgMnPc7d1	mechanický
výsuvnými	výsuvný	k2eAgFnPc7d1	výsuvná
rameny	rameno	k1gNnPc7	rameno
zabránily	zabránit	k5eAaPmAgInP	zabránit
v	v	k7c6	v
průchodu	průchod	k1gInSc6	průchod
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vstoupit	vstoupit	k5eAaPmF	vstoupit
bez	bez	k7c2	bez
zaplacení	zaplacení	k1gNnSc4	zaplacení
(	(	kIx(	(
<g/>
vhození	vhození	k1gNnSc4	vhození
jednokorunové	jednokorunový	k2eAgFnSc2d1	jednokorunová
mince	mince	k1gFnSc2	mince
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
projít	projít	k5eAaPmF	projít
nesprávným	správný	k2eNgInSc7d1	nesprávný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovějším	nový	k2eAgInSc7d3	nejnovější
úsekem	úsek	k1gInSc7	úsek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
turnikety	turniket	k1gInPc4	turniket
vybaven	vybaven	k2eAgInSc1d1	vybaven
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc4d1	otevřený
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdné	k1gNnSc1	jízdné
činilo	činit	k5eAaImAgNnS	činit
1	[number]	k4	1
Kčs	Kčs	kA	Kčs
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
tarif	tarif	k1gInSc1	tarif
v	v	k7c6	v
MHD	MHD	kA	MHD
byl	být	k5eAaImAgMnS	být
obecně	obecně	k6eAd1	obecně
nepřestupný	přestupný	k2eNgMnSc1d1	nepřestupný
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
trasami	trasa	k1gFnPc7	trasa
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
smělo	smět	k5eAaImAgNnS	smět
přestupovat	přestupovat	k5eAaImF	přestupovat
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
placení	placení	k1gNnSc2	placení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
vstupů	vstup	k1gInPc2	vstup
do	do	k7c2	do
metra	metro	k1gNnSc2	metro
zprovozněny	zprovozněn	k2eAgInPc4d1	zprovozněn
označovače	označovač	k1gInPc4	označovač
jízdenek	jízdenka	k1gFnPc2	jízdenka
značky	značka	k1gFnSc2	značka
Merona	Merona	k1gFnSc1	Merona
a	a	k8xC	a
odstraněny	odstraněn	k2eAgInPc1d1	odstraněn
turnikety	turniket	k1gInPc1	turniket
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
změna	změna	k1gFnSc1	změna
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
úspěšně	úspěšně	k6eAd1	úspěšně
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
otevřením	otevření	k1gNnSc7	otevření
úseku	úsek	k1gInSc2	úsek
metra	metro	k1gNnSc2	metro
I.B.	I.B.	k1gFnSc2	I.B.
Označovače	označovač	k1gInSc2	označovač
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
byly	být	k5eAaImAgFnP	být
elektronické	elektronický	k2eAgFnPc1d1	elektronická
razítkovací	razítkovací	k2eAgFnPc1d1	razítkovací
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
tramvajích	tramvaj	k1gFnPc6	tramvaj
a	a	k8xC	a
autobusech	autobus	k1gInPc6	autobus
byly	být	k5eAaImAgFnP	být
nadále	nadále	k6eAd1	nadále
pouze	pouze	k6eAd1	pouze
mechanické	mechanický	k2eAgInPc1d1	mechanický
děrovače	děrovač	k1gInPc1	děrovač
maďarské	maďarský	k2eAgFnSc2d1	maďarská
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdné	k1gNnSc1	jízdné
se	se	k3xPyFc4	se
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
desetiletí	desetiletí	k1gNnSc6	desetiletí
postupně	postupně	k6eAd1	postupně
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
<g/>
,	,	kIx,	,
tarif	tarif	k1gInSc1	tarif
MHD	MHD	kA	MHD
však	však	k9	však
zůstal	zůstat	k5eAaPmAgInS	zůstat
nepřestupní	přestupní	k2eNgInSc1d1	nepřestupní
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
přestupu	přestup	k1gInSc2	přestup
mezi	mezi	k7c7	mezi
trasami	trasa	k1gFnPc7	trasa
metra	metro	k1gNnSc2	metro
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
jízdenku	jízdenka	k1gFnSc4	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Pražské	pražský	k2eAgFnSc6d1	Pražská
integrované	integrovaný	k2eAgFnSc6d1	integrovaná
dopravě	doprava	k1gFnSc6	doprava
přestupní	přestupní	k2eAgInSc1d1	přestupní
a	a	k8xC	a
časový	časový	k2eAgInSc1d1	časový
tarif	tarif	k1gInSc1	tarif
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metru	metr	k1gInSc6	metr
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
výměnu	výměna	k1gFnSc4	výměna
všech	všecek	k3xTgInPc2	všecek
označovačů	označovač	k1gInPc2	označovač
jízdenek	jízdenka	k1gFnPc2	jízdenka
za	za	k7c4	za
značku	značka	k1gFnSc4	značka
Mikroelektronika	mikroelektronika	k1gFnSc1	mikroelektronika
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
konsorcia	konsorcium	k1gNnSc2	konsorcium
Mypol	Mypol	k1gInSc1	Mypol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nové	nový	k2eAgFnPc1d1	nová
jízdenky	jízdenka	k1gFnPc1	jízdenka
byly	být	k5eAaImAgFnP	být
širší	široký	k2eAgFnPc1d2	širší
a	a	k8xC	a
označovací	označovací	k2eAgInSc1d1	označovací
potisk	potisk	k1gInSc1	potisk
byl	být	k5eAaImAgInS	být
prováděn	provádět	k5eAaImNgInS	provádět
příčně	příčně	k6eAd1	příčně
při	při	k7c6	při
kratší	krátký	k2eAgFnSc6d2	kratší
straně	strana	k1gFnSc6	strana
jízdenky	jízdenka	k1gFnPc4	jízdenka
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
délce	délka	k1gFnSc6	délka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
turniketového	turniketový	k2eAgInSc2d1	turniketový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nP	by
však	však	k9	však
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
čtení	čtení	k1gNnSc6	čtení
čipových	čipový	k2eAgFnPc2d1	čipová
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnPc1	diskuse
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
úvahami	úvaha	k1gFnPc7	úvaha
o	o	k7c6	o
Univerzální	univerzální	k2eAgFnSc6d1	univerzální
kartě	karta	k1gFnSc6	karta
Pražana	Pražan	k1gMnSc4	Pražan
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zavedené	zavedený	k2eAgInPc4d1	zavedený
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Opencard	Opencard	k1gInSc1	Opencard
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Národní	národní	k2eAgFnSc3d1	národní
dopravní	dopravní	k2eAgFnSc3d1	dopravní
kartě	karta	k1gFnSc3	karta
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
kritiků	kritik	k1gMnPc2	kritik
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
nadměrný	nadměrný	k2eAgInSc4d1	nadměrný
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
soukromí	soukromí	k1gNnSc2	soukromí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kampaň	kampaň	k1gFnSc4	kampaň
sdružení	sdružení	k1gNnSc4	sdružení
Iuridicum	Iuridicum	k1gInSc1	Iuridicum
Remedium	remedium	k1gNnSc1	remedium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
systém	systém	k1gInSc1	systém
nejen	nejen	k6eAd1	nejen
kontroloval	kontrolovat	k5eAaImAgInS	kontrolovat
zaplacení	zaplacení	k1gNnSc4	zaplacení
jízdného	jízdné	k1gNnSc2	jízdné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
soustavně	soustavně	k6eAd1	soustavně
systematicky	systematicky	k6eAd1	systematicky
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
a	a	k8xC	a
kam	kam	k6eAd1	kam
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
lidé	člověk	k1gMnPc1	člověk
cestují	cestovat	k5eAaImIp3nP	cestovat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vyhodnocováno	vyhodnocovat	k5eAaImNgNnS	vyhodnocovat
nyní	nyní	k6eAd1	nyní
při	při	k7c6	při
občasných	občasný	k2eAgInPc6d1	občasný
přepravních	přepravní	k2eAgInPc6d1	přepravní
průzkumech	průzkum	k1gInPc6	průzkum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cestující	cestující	k1gMnPc1	cestující
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
dostanou	dostat	k5eAaPmIp3nP	dostat
kartičku	kartička	k1gFnSc4	kartička
a	a	k8xC	a
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
ji	on	k3xPp3gFnSc4	on
odevzdají	odevzdat	k5eAaPmIp3nP	odevzdat
<g/>
.	.	kIx.	.
</s>
<s>
Uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
i	i	k9	i
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
využití	využití	k1gNnSc2	využití
systému	systém	k1gInSc2	systém
k	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
vstupu	vstup	k1gInSc2	vstup
konkrétním	konkrétní	k2eAgFnPc3d1	konkrétní
nežádoucím	žádoucí	k2eNgFnPc3d1	nežádoucí
osobám	osoba	k1gFnPc3	osoba
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
turniketů	turniket	k1gInPc2	turniket
a	a	k8xC	a
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
odbavovacího	odbavovací	k2eAgInSc2d1	odbavovací
systému	systém	k1gInSc2	systém
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
vážně	vážně	k6eAd1	vážně
uvažovat	uvažovat	k5eAaImF	uvažovat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
Metroprojekt	Metroprojekt	k1gInSc1	Metroprojekt
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
zavedení	zavedení	k1gNnSc1	zavedení
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
instalaci	instalace	k1gFnSc4	instalace
asi	asi	k9	asi
800	[number]	k4	800
turniketů	turniket	k1gInPc2	turniket
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
úpravy	úprava	k1gFnPc4	úprava
kvůli	kvůli	k7c3	kvůli
propustnosti	propustnost	k1gFnSc3	propustnost
by	by	k9	by
byly	být	k5eAaImAgFnP	být
nutné	nutný	k2eAgFnPc1d1	nutná
jen	jen	k9	jen
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgInSc2d1	Pavlův
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2008	[number]	k4	2008
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
ne	ne	k9	ne
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
nebo	nebo	k8xC	nebo
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
snížením	snížení	k1gNnSc7	snížení
rozpočtu	rozpočet	k1gInSc2	rozpočet
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
ředitel	ředitel	k1gMnSc1	ředitel
DP	DP	kA	DP
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
turnikety	turniket	k1gInPc1	turniket
budou	být	k5eAaImBp3nP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Investiční	investiční	k2eAgInPc1d1	investiční
náklady	náklad	k1gInPc1	náklad
se	se	k3xPyFc4	se
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
až	až	k9	až
dvě	dva	k4xCgFnPc4	dva
miliardy	miliarda	k4xCgFnPc4	miliarda
Kč	Kč	kA	Kč
a	a	k8xC	a
návratnost	návratnost	k1gFnSc1	návratnost
investice	investice	k1gFnSc1	investice
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
proveditelnosti	proveditelnost	k1gFnSc2	proveditelnost
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
2008	[number]	k4	2008
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
pomocí	pomocí	k7c2	pomocí
turniketů	turniket	k1gInPc2	turniket
je	být	k5eAaImIp3nS	být
však	však	k9	však
obtížně	obtížně	k6eAd1	obtížně
slučitelné	slučitelný	k2eAgInPc1d1	slučitelný
například	například	k6eAd1	například
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
SMS	SMS	kA	SMS
jízdenek	jízdenka	k1gFnPc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
se	se	k3xPyFc4	se
primátor	primátor	k1gMnSc1	primátor
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
postavil	postavit	k5eAaPmAgMnS	postavit
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
turniketů	turniket	k1gInPc2	turniket
odmítavě	odmítavě	k6eAd1	odmítavě
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
hazard	hazard	k1gInSc4	hazard
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
přeceněn	přeceněn	k2eAgInSc1d1	přeceněn
podíl	podíl	k1gInSc1	podíl
černých	černý	k2eAgMnPc2d1	černý
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
silně	silně	k6eAd1	silně
zkresluje	zkreslovat	k5eAaImIp3nS	zkreslovat
odhad	odhad	k1gInSc1	odhad
návratnosti	návratnost	k1gFnSc2	návratnost
<g/>
.	.	kIx.	.
</s>
<s>
Skepticky	skepticky	k6eAd1	skepticky
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
rovněž	rovněž	k9	rovněž
jeho	jeho	k3xOp3gMnSc1	jeho
náměstek	náměstek	k1gMnSc1	náměstek
Karel	Karel	k1gMnSc1	Karel
Březina	Březina	k1gMnSc1	Březina
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
záměru	záměr	k1gInSc3	záměr
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
i	i	k9	i
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
pirátská	pirátský	k2eAgFnSc1d1	pirátská
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
depa	depo	k1gNnPc4	depo
<g/>
;	;	kIx,	;
nejstarší	starý	k2eAgNnSc4d3	nejstarší
Depo	depo	k1gNnSc4	depo
Kačerov	Kačerov	k1gInSc1	Kačerov
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgNnSc1d1	otevřené
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
a	a	k8xC	a
sloužící	sloužící	k2eAgMnSc1d1	sloužící
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
C	C	kA	C
<g/>
,	,	kIx,	,
Depo	depo	k1gNnSc1	depo
Hostivař	Hostivař	k1gFnSc1	Hostivař
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc1d1	umožňující
vypravování	vypravování	k1gNnSc1	vypravování
vozů	vůz	k1gInPc2	vůz
metra	metro	k1gNnSc2	metro
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
A	A	kA	A
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
také	také	k9	také
částečně	částečně	k6eAd1	částečně
i	i	k9	i
jako	jako	k9	jako
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
a	a	k8xC	a
zprovozněné	zprovozněný	k2eAgFnSc2d1	zprovozněná
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
dvěma	dva	k4xCgInPc3	dva
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
Depo	depo	k1gNnSc1	depo
Zličín	Zličína	k1gFnPc2	Zličína
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
B	B	kA	B
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zabezpečovací	zabezpečovací	k2eAgNnSc1d1	zabezpečovací
zařízení	zařízení	k1gNnSc1	zařízení
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečovací	zabezpečovací	k2eAgNnSc1d1	zabezpečovací
zařízení	zařízení	k1gNnSc1	zařízení
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
plynulost	plynulost	k1gFnSc4	plynulost
provozu	provoz	k1gInSc2	provoz
vlaků	vlak	k1gInPc2	vlak
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
nejkratší	krátký	k2eAgInSc1d3	nejkratší
následný	následný	k2eAgInSc1d1	následný
interval	interval	k1gInSc1	interval
mezi	mezi	k7c7	mezi
vlaky	vlak	k1gInPc7	vlak
90	[number]	k4	90
s	s	k7c7	s
při	pře	k1gFnSc3	pře
maximální	maximální	k2eAgFnSc2d1	maximální
dovolené	dovolená	k1gFnSc2	dovolená
rychlosti	rychlost	k1gFnSc2	rychlost
vlaků	vlak	k1gInPc2	vlak
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
stanice	stanice	k1gFnSc2	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
nejkratší	krátký	k2eAgInSc4d3	nejkratší
následný	následný	k2eAgInSc4d1	následný
interval	interval	k1gInSc4	interval
mezi	mezi	k7c7	mezi
vlaky	vlak	k1gInPc7	vlak
105	[number]	k4	105
s.	s.	k?	s.
Bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
provoz	provoz	k1gInSc4	provoz
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
kombinace	kombinace	k1gFnSc1	kombinace
několika	několik	k4yIc2	několik
druhů	druh	k1gInPc2	druh
zabezpečovacích	zabezpečovací	k2eAgNnPc2d1	zabezpečovací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
:	:	kIx,	:
Staniční	staniční	k2eAgFnSc1d1	staniční
<g/>
:	:	kIx,	:
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
využívá	využívat	k5eAaPmIp3nS	využívat
reléové	reléový	k2eAgNnSc1d1	reléové
staniční	staniční	k2eAgNnSc1d1	staniční
zabezpečovací	zabezpečovací	k2eAgNnSc1d1	zabezpečovací
zařízení	zařízení	k1gNnSc1	zařízení
typu	typ	k1gInSc2	typ
AŽD	AŽD	kA	AŽD
71	[number]	k4	71
speciálně	speciálně	k6eAd1	speciálně
upravené	upravený	k2eAgInPc1d1	upravený
pro	pro	k7c4	pro
podzemní	podzemní	k2eAgFnSc4d1	podzemní
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
jízdní	jízdní	k2eAgFnPc4d1	jízdní
cesty	cesta	k1gFnPc4	cesta
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
s	s	k7c7	s
kolejovým	kolejový	k2eAgNnSc7d1	kolejové
rozvětvením	rozvětvení	k1gNnSc7	rozvětvení
a	a	k8xC	a
v	v	k7c6	v
depech	depo	k1gNnPc6	depo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
Nádraží	nádraží	k1gNnSc4	nádraží
Holešovice	Holešovice	k1gFnPc4	Holešovice
<g/>
,	,	kIx,	,
Ládví	Ládví	k1gNnPc4	Ládví
a	a	k8xC	a
Letňany	Letňan	k1gMnPc4	Letňan
je	být	k5eAaImIp3nS	být
zapojeno	zapojen	k2eAgNnSc1d1	zapojeno
elektronické	elektronický	k2eAgNnSc1d1	elektronické
staniční	staniční	k2eAgNnSc1d1	staniční
zabezpečovací	zabezpečovací	k2eAgNnSc1d1	zabezpečovací
zařízení	zařízení	k1gNnSc1	zařízení
typu	typ	k1gInSc2	typ
ESA	eso	k1gNnSc2	eso
11	[number]	k4	11
M	M	kA	M
s	s	k7c7	s
úpravou	úprava	k1gFnSc7	úprava
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
jízdní	jízdní	k2eAgFnPc4d1	jízdní
cesty	cesta	k1gFnPc4	cesta
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
s	s	k7c7	s
kolejovým	kolejový	k2eAgNnSc7d1	kolejové
rozvětvením	rozvětvení	k1gNnSc7	rozvětvení
<g/>
.	.	kIx.	.
</s>
<s>
ESA	eso	k1gNnSc2	eso
11	[number]	k4	11
M	M	kA	M
je	být	k5eAaImIp3nS	být
ovládána	ovládán	k2eAgFnSc1d1	ovládána
z	z	k7c2	z
jednotného	jednotný	k2eAgNnSc2d1	jednotné
ovládacího	ovládací	k2eAgNnSc2d1	ovládací
pracoviště	pracoviště	k1gNnSc2	pracoviště
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
JOP	JOP	kA	JOP
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
využito	využít	k5eAaPmNgNnS	využít
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Florenc-B	Florenc-B	k1gFnSc2	Florenc-B
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ovládá	ovládat	k5eAaImIp3nS	ovládat
AŽD	AŽD	kA	AŽD
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
úseku	úsek	k1gInSc2	úsek
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
jsou	být	k5eAaImIp3nP	být
ESA	eso	k1gNnSc2	eso
11	[number]	k4	11
M	M	kA	M
a	a	k8xC	a
JOP	JOP	kA	JOP
použity	použít	k5eAaPmNgInP	použít
ve	v	k7c4	v
stanici	stanice	k1gFnSc4	stanice
Ládví	Ládví	k1gNnSc2	Ládví
<g/>
.	.	kIx.	.
</s>
<s>
Traťové	traťový	k2eAgNnSc1d1	traťové
<g/>
:	:	kIx,	:
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
reléové	reléový	k2eAgNnSc4d1	reléové
zařízení	zařízení	k1gNnSc4	zařízení
typu	typ	k1gInSc2	typ
AŽD	AŽD	kA	AŽD
71	[number]	k4	71
nebo	nebo	k8xC	nebo
ESA	eso	k1gNnSc2	eso
11	[number]	k4	11
M	M	kA	M
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
jízdy	jízda	k1gFnPc4	jízda
vlaků	vlak	k1gInPc2	vlak
v	v	k7c6	v
úsecích	úsek	k1gInPc6	úsek
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
s	s	k7c7	s
kolejovým	kolejový	k2eAgNnSc7d1	kolejové
rozvětvením	rozvětvení	k1gNnSc7	rozvětvení
<g/>
,	,	kIx,	,
traťových	traťový	k2eAgFnPc6d1	traťová
spojkách	spojka	k1gFnPc6	spojka
a	a	k8xC	a
spojovacích	spojovací	k2eAgFnPc6d1	spojovací
kolejích	kolej	k1gFnPc6	kolej
do	do	k7c2	do
dep	depo	k1gNnPc2	depo
<g/>
.	.	kIx.	.
</s>
<s>
Vlakové	vlakový	k2eAgNnSc1d1	vlakové
<g/>
:	:	kIx,	:
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
využívá	využívat	k5eAaImIp3nS	využívat
liniový	liniový	k2eAgInSc4d1	liniový
vlakový	vlakový	k2eAgInSc4d1	vlakový
zabezpečovač	zabezpečovač	k1gInSc4	zabezpečovač
ARS	ARS	kA	ARS
s	s	k7c7	s
kontrolou	kontrola	k1gFnSc7	kontrola
rychlosti	rychlost	k1gFnSc2	rychlost
jízdy	jízda	k1gFnSc2	jízda
vlaků	vlak	k1gInPc2	vlak
sovětské	sovětský	k2eAgFnSc2d1	sovětská
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
C	C	kA	C
je	být	k5eAaImIp3nS	být
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
vlakový	vlakový	k2eAgInSc1d1	vlakový
zabezpečovač	zabezpečovač	k1gInSc1	zabezpečovač
francouzské	francouzský	k2eAgFnSc2d1	francouzská
firmy	firma	k1gFnSc2	firma
MATRA	Matra	k1gFnSc1	Matra
typu	typ	k1gInSc2	typ
PA	Pa	kA	Pa
135	[number]	k4	135
firmy	firma	k1gFnSc2	firma
Siemens	siemens	k1gInSc1	siemens
AG	AG	kA	AG
s	s	k7c7	s
kontrolou	kontrola	k1gFnSc7	kontrola
rychlosti	rychlost	k1gFnSc2	rychlost
jízdy	jízda	k1gFnSc2	jízda
vlaků	vlak	k1gInPc2	vlak
<g/>
,	,	kIx,	,
kontrolou	kontrola	k1gFnSc7	kontrola
sledu	sled	k1gInSc2	sled
vlaků	vlak	k1gInPc2	vlak
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
automatické	automatický	k2eAgNnSc4d1	automatické
vedení	vedení	k1gNnSc4	vedení
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
A	a	k9	a
je	být	k5eAaImIp3nS	být
vlakový	vlakový	k2eAgInSc1d1	vlakový
zabezpečovač	zabezpečovač	k1gInSc1	zabezpečovač
LZA	LZA	kA	LZA
(	(	kIx(	(
<g/>
SOP-	SOP-	k1gMnSc1	SOP-
<g/>
2	[number]	k4	2
<g/>
P	P	kA	P
polské	polský	k2eAgFnSc2d1	polská
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
automatizační	automatizační	k2eAgNnPc1d1	automatizační
zařízení	zařízení	k1gNnPc1	zařízení
ACBM-3	ACBM-3	k1gMnSc2	ACBM-3
firmy	firma	k1gFnSc2	firma
AŽD	AŽD	kA	AŽD
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
automatické	automatický	k2eAgNnSc4d1	automatické
vedení	vedení	k1gNnSc4	vedení
vlaku	vlak	k1gInSc2	vlak
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
činnosti	činnost	k1gFnSc2	činnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgInPc4d1	spojený
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
i	i	k9	i
na	na	k7c4	na
trasu	trasa	k1gFnSc4	trasa
B.	B.	kA	B.
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Eskalátory	eskalátor	k1gInPc1	eskalátor
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Eskalátory	eskalátor	k1gInPc1	eskalátor
jsou	být	k5eAaImIp3nP	být
základním	základní	k2eAgNnSc7d1	základní
zařízením	zařízení	k1gNnSc7	zařízení
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
nezbytným	nezbytný	k2eAgNnSc7d1	nezbytný
pro	pro	k7c4	pro
fungování	fungování	k1gNnSc4	fungování
hlavně	hlavně	k9	hlavně
hluboce	hluboko	k6eAd1	hluboko
založených	založený	k2eAgFnPc2d1	založená
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podzemní	podzemní	k2eAgFnSc6d1	podzemní
dráze	dráha	k1gFnSc6	dráha
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těch	ten	k3xDgInPc2	ten
úseků	úsek	k1gInPc2	úsek
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
budovaných	budovaný	k2eAgInPc2d1	budovaný
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
jednak	jednak	k8xC	jednak
kratší	krátký	k2eAgInPc1d2	kratší
eskalátory	eskalátor	k1gInPc1	eskalátor
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
z	z	k7c2	z
vestibulů	vestibul	k1gInPc2	vestibul
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
)	)	kIx)	)
československé	československý	k2eAgFnSc2d1	Československá
výroby	výroba	k1gFnSc2	výroba
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Transporta	transporta	k1gFnSc1	transporta
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
delší	dlouhý	k2eAgInPc1d2	delší
<g/>
,	,	kIx,	,
hlubinné	hlubinný	k2eAgInPc1d1	hlubinný
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
ze	z	k7c2	z
staniční	staniční	k2eAgFnSc2d1	staniční
střední	střední	k2eAgFnSc2d1	střední
lodi	loď	k1gFnSc2	loď
do	do	k7c2	do
vestibulu	vestibul	k1gInSc2	vestibul
u	u	k7c2	u
hluboko	hluboko	k6eAd1	hluboko
založených	založený	k2eAgFnPc2d1	založená
ražených	ražený	k2eAgFnPc2d1	ražená
stanic	stanice	k1gFnPc2	stanice
na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
<g/>
)	)	kIx)	)
sovětské	sovětský	k2eAgFnSc2d1	sovětská
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
nejstarší	starý	k2eAgFnPc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
(	(	kIx(	(
<g/>
instalace	instalace	k1gFnSc2	instalace
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
postupně	postupně	k6eAd1	postupně
vyměňovány	vyměňován	k2eAgFnPc1d1	vyměňována
<g/>
,	,	kIx,	,
na	na	k7c6	na
nových	nový	k2eAgInPc6d1	nový
úsecích	úsek	k1gInPc6	úsek
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
schody	schod	k1gInPc1	schod
západoevropské	západoevropský	k2eAgFnSc2d1	západoevropská
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
eskalátory	eskalátor	k1gInPc1	eskalátor
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
(	(	kIx(	(
<g/>
87	[number]	k4	87
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Výtahy	výtah	k1gInPc1	výtah
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
stavět	stavět	k5eAaImF	stavět
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
tedy	tedy	k9	tedy
při	při	k7c6	při
otevření	otevření	k1gNnSc6	otevření
V.	V.	kA	V.
úseku	úsek	k1gInSc2	úsek
trasy	trasa	k1gFnSc2	trasa
B	B	kA	B
na	na	k7c4	na
Zličín	Zličín	k1gInSc4	Zličín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
běží	běžet	k5eAaImIp3nS	běžet
projekt	projekt	k1gInSc4	projekt
na	na	k7c4	na
doplnění	doplnění	k1gNnSc4	doplnění
výtahů	výtah	k1gInPc2	výtah
do	do	k7c2	do
starších	starý	k2eAgFnPc2d2	starší
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
metru	metr	k1gInSc6	metr
160	[number]	k4	160
výtahů	výtah	k1gInPc2	výtah
a	a	k8xC	a
plošin	plošina	k1gFnPc2	plošina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
březnu	březen	k1gInSc3	březen
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
bezbariérových	bezbariérový	k2eAgMnPc2d1	bezbariérový
43	[number]	k4	43
z	z	k7c2	z
61	[number]	k4	61
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
stanicí	stanice	k1gFnSc7	stanice
s	s	k7c7	s
nově	nově	k6eAd1	nově
otevřeným	otevřený	k2eAgInSc7d1	otevřený
výtahem	výtah	k1gInSc7	výtah
je	být	k5eAaImIp3nS	být
přestupní	přestupní	k2eAgFnSc1d1	přestupní
stanice	stanice	k1gFnSc1	stanice
Můstek	můstek	k1gInSc1	můstek
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
B.	B.	kA	B.
Další	další	k2eAgFnSc1d1	další
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
stanice	stanice	k1gFnSc1	stanice
Palmovka	Palmovka	k1gFnSc1	Palmovka
a	a	k8xC	a
Karlovo	Karlův	k2eAgNnSc1d1	Karlovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
schválené	schválený	k2eAgFnSc2d1	schválená
dopravní	dopravní	k2eAgFnSc2d1	dopravní
koncepce	koncepce	k1gFnSc2	koncepce
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
bezbariérové	bezbariérový	k2eAgFnPc1d1	bezbariérová
všechny	všechen	k3xTgFnPc1	všechen
stanice	stanice	k1gFnPc1	stanice
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2025	[number]	k4	2025
tzn.	tzn.	kA	tzn.
za	za	k7c4	za
9	[number]	k4	9
let	léto	k1gNnPc2	léto
přibude	přibýt	k5eAaPmIp3nS	přibýt
ještě	ještě	k9	ještě
18	[number]	k4	18
stanic	stanice	k1gFnPc2	stanice
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ochranný	ochranný	k2eAgInSc4d1	ochranný
systém	systém	k1gInSc4	systém
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnPc2	součást
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
–	–	k?	–
především	především	k9	především
těch	ten	k3xDgMnPc2	ten
budovaných	budovaný	k2eAgMnPc2d1	budovaný
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
prostory	prostora	k1gFnPc1	prostora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
kryt	kryt	k2eAgInSc1d1	kryt
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
války	válka	k1gFnSc2	válka
a	a	k8xC	a
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
slepé	slepý	k2eAgFnPc1d1	slepá
štoly	štola	k1gFnPc1	štola
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
provozní	provozní	k2eAgFnPc1d1	provozní
místnosti	místnost	k1gFnPc1	místnost
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
je	být	k5eAaImIp3nS	být
běžným	běžný	k2eAgMnPc3d1	běžný
cestujícím	cestující	k1gMnPc3	cestující
vstup	vstup	k1gInSc4	vstup
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
změnit	změnit	k5eAaPmF	změnit
v	v	k7c6	v
zásobovací	zásobovací	k2eAgFnSc6d1	zásobovací
<g/>
,	,	kIx,	,
bezpečnostní	bezpečnostní	k2eAgFnSc3d1	bezpečnostní
i	i	k9	i
zdravotnická	zdravotnický	k2eAgNnPc1d1	zdravotnické
centra	centrum	k1gNnPc1	centrum
a	a	k8xC	a
poskytovat	poskytovat	k5eAaImF	poskytovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
72	[number]	k4	72
hodin	hodina	k1gFnPc2	hodina
nezbytné	zbytný	k2eNgFnSc2d1	zbytný
věci	věc	k1gFnSc2	věc
evakuovanému	evakuovaný	k2eAgNnSc3d1	evakuované
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
navíc	navíc	k6eAd1	navíc
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
upraveny	upravit	k5eAaPmNgFnP	upravit
i	i	k9	i
části	část	k1gFnPc1	část
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
traťových	traťový	k2eAgInPc2d1	traťový
tunelů	tunel	k1gInPc2	tunel
<g/>
;	;	kIx,	;
okrajovými	okrajový	k2eAgInPc7d1	okrajový
úseky	úsek	k1gInPc7	úsek
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
spojení	spojení	k1gNnSc1	spojení
systému	systém	k1gInSc2	systém
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Řídící	řídící	k2eAgNnPc1d1	řídící
stanoviště	stanoviště	k1gNnPc1	stanoviště
včetně	včetně	k7c2	včetně
řídícího	řídící	k2eAgInSc2d1	řídící
počítače	počítač	k1gInSc2	počítač
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
Centrálního	centrální	k2eAgInSc2d1	centrální
dispečinku	dispečink	k1gInSc2	dispečink
DPP	DPP	kA	DPP
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tratí	trať	k1gFnPc2	trať
stavěných	stavěný	k2eAgFnPc2d1	stavěná
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
rozsah	rozsah	k1gInSc1	rozsah
OSM	osm	k4xCc1	osm
výrazně	výrazně	k6eAd1	výrazně
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
povodni	povodeň	k1gFnSc3	povodeň
(	(	kIx(	(
<g/>
což	což	k9	což
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
aktivován	aktivovat	k5eAaBmNgInS	aktivovat
<g/>
)	)	kIx)	)
kryty	kryt	k1gInPc1	kryt
nebyly	být	k5eNaImAgInP	být
projektovány	projektovat	k5eAaBmNgInP	projektovat
a	a	k8xC	a
jak	jak	k6eAd1	jak
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
zatopení	zatopení	k1gNnSc1	zatopení
metra	metro	k1gNnSc2	metro
při	při	k7c6	při
povodni	povodeň	k1gFnSc6	povodeň
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
nemohly	moct	k5eNaImAgFnP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
účinné	účinný	k2eAgInPc1d1	účinný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
radou	rada	k1gMnSc7	rada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
dimenzovat	dimenzovat	k5eAaBmF	dimenzovat
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
případy	případ	k1gInPc4	případ
teroristického	teroristický	k2eAgInSc2d1	teroristický
útoku	útok	k1gInSc2	útok
nebo	nebo	k8xC	nebo
živelní	živelní	k2eAgFnSc2d1	živelní
pohromy	pohroma	k1gFnSc2	pohroma
aj	aj	kA	aj
<g/>
,	,	kIx,	,
než	než	k8xS	než
na	na	k7c4	na
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
byl	být	k5eAaImAgInS	být
připravován	připravován	k2eAgInSc1d1	připravován
tzv.	tzv.	kA	tzv.
ochranný	ochranný	k2eAgInSc1d1	ochranný
systém	systém	k1gInSc1	systém
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
již	již	k9	již
na	na	k7c6	na
nových	nový	k2eAgFnPc6d1	nová
stavbách	stavba	k1gFnPc6	stavba
metra	metro	k1gNnSc2	metro
realizován	realizovat	k5eAaBmNgInS	realizovat
nebude	být	k5eNaImBp3nS	být
–	–	k?	–
posledním	poslední	k2eAgInSc7d1	poslední
úsekem	úsek	k1gInSc7	úsek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
významná	významný	k2eAgFnSc1d1	významná
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
u	u	k7c2	u
nově	nově	k6eAd1	nově
budovaných	budovaný	k2eAgFnPc2d1	budovaná
staveb	stavba	k1gFnPc2	stavba
projeví	projevit	k5eAaPmIp3nP	projevit
hlavně	hlavně	k9	hlavně
úsporami	úspora	k1gFnPc7	úspora
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
stovek	stovka	k1gFnPc2	stovka
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
;	;	kIx,	;
nové	nový	k2eAgInPc4d1	nový
systémy	systém	k1gInPc4	systém
bude	být	k5eAaImBp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
OSM	osm	k4xCc1	osm
možné	možný	k2eAgNnSc1d1	možné
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
kratší	krátký	k2eAgFnSc6d2	kratší
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
usnesení	usnesení	k1gNnSc2	usnesení
vlády	vláda	k1gFnSc2	vláda
č.	č.	k?	č.
823	[number]	k4	823
uvolněny	uvolněn	k2eAgInPc4d1	uvolněn
prostředky	prostředek	k1gInPc4	prostředek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
město	město	k1gNnSc1	město
žádalo	žádat	k5eAaImAgNnS	žádat
celkem	celkem	k6eAd1	celkem
162,5	[number]	k4	162,5
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
na	na	k7c4	na
provedení	provedení	k1gNnPc4	provedení
těchto	tento	k3xDgNnPc2	tento
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Reklama	reklama	k1gFnSc1	reklama
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
Reklama	reklama	k1gFnSc1	reklama
se	se	k3xPyFc4	se
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
začala	začít	k5eAaPmAgNnP	začít
objevovat	objevovat	k5eAaImF	objevovat
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
století	století	k1gNnSc2	století
jedenadvacátého	jedenadvacátý	k4xOgNnSc2	jedenadvacátý
<g/>
.	.	kIx.	.
</s>
<s>
Reklamní	reklamní	k2eAgInPc1d1	reklamní
panely	panel	k1gInPc1	panel
AWK	AWK	kA	AWK
obsluhované	obsluhovaný	k2eAgNnSc1d1	obsluhované
agenturou	agentura	k1gFnSc7	agentura
Rencar	Rencara	k1gFnPc2	Rencara
<g/>
,	,	kIx,	,
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
dopravce	dopravce	k1gMnSc1	dopravce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
staničních	staniční	k2eAgFnPc6d1	staniční
lodích	loď	k1gFnPc6	loď
<g/>
,	,	kIx,	,
přestupních	přestupní	k2eAgFnPc6d1	přestupní
i	i	k8xC	i
jiných	jiný	k2eAgFnPc6d1	jiná
chodbách	chodba	k1gFnPc6	chodba
<g/>
,	,	kIx,	,
vstupních	vstupní	k2eAgInPc6d1	vstupní
vestibulech	vestibul	k1gInPc6	vestibul
atd.	atd.	kA	atd.
Reklamy	reklama	k1gFnSc2	reklama
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
samolepicích	samolepicí	k2eAgFnPc2d1	samolepicí
nebo	nebo	k8xC	nebo
výměnných	výměnný	k2eAgFnPc2d1	výměnná
fólií	fólie	k1gFnPc2	fólie
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
podlahách	podlaha	k1gFnPc6	podlaha
a	a	k8xC	a
schodištích	schodiště	k1gNnPc6	schodiště
<g/>
,	,	kIx,	,
na	na	k7c6	na
bočních	boční	k2eAgFnPc6d1	boční
stěnách	stěna	k1gFnPc6	stěna
eskalátorových	eskalátorův	k2eAgInPc2d1	eskalátorův
tunelů	tunel	k1gInPc2	tunel
a	a	k8xC	a
přestupních	přestupní	k2eAgFnPc2d1	přestupní
chodeb	chodba	k1gFnPc2	chodba
<g/>
,	,	kIx,	,
v	v	k7c6	v
interiéru	interiér	k1gInSc2	interiér
i	i	k8xC	i
exteriéru	exteriér	k1gInSc2	exteriér
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
frekventovaných	frekventovaný	k2eAgFnPc6d1	frekventovaná
stanicích	stanice	k1gFnPc6	stanice
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
informační	informační	k2eAgInPc4d1	informační
projektory	projektor	k1gInPc4	projektor
promítající	promítající	k2eAgNnSc4d1	promítající
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
a	a	k8xC	a
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
příjezdu	příjezd	k1gInSc6	příjezd
příštího	příští	k2eAgInSc2d1	příští
vlaku	vlak	k1gInSc2	vlak
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
traťového	traťový	k2eAgInSc2d1	traťový
tunelu	tunel	k1gInSc2	tunel
naproti	naproti	k7c3	naproti
nástupišti	nástupiště	k1gNnSc3	nástupiště
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
byly	být	k5eAaImAgFnP	být
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
demontovány	demontován	k2eAgInPc1d1	demontován
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
billboardy	billboard	k1gInPc4	billboard
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
do	do	k7c2	do
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
znovu	znovu	k6eAd1	znovu
vrátily	vrátit	k5eAaPmAgFnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
postupně	postupně	k6eAd1	postupně
demontovány	demontován	k2eAgFnPc1d1	demontována
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Graffiti	graffiti	k1gNnSc2	graffiti
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
graffiti	graffiti	k1gNnSc1	graffiti
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
;	;	kIx,	;
souviselo	souviset	k5eAaImAgNnS	souviset
to	ten	k3xDgNnSc1	ten
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
společenských	společenský	k2eAgFnPc2d1	společenská
podmínek	podmínka	k1gFnPc2	podmínka
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
se	se	k3xPyFc4	se
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
graffiti	graffiti	k1gNnSc2	graffiti
začalo	začít	k5eAaPmAgNnS	začít
potýkat	potýkat	k5eAaImF	potýkat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
mramorové	mramorový	k2eAgInPc1d1	mramorový
obklady	obklad	k1gInPc1	obklad
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
plochy	plocha	k1gFnPc1	plocha
opatřovány	opatřován	k2eAgFnPc1d1	opatřována
speciálním	speciální	k2eAgInSc7d1	speciální
nátěrem	nátěr	k1gInSc7	nátěr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
usnadnit	usnadnit	k5eAaPmF	usnadnit
čištění	čištění	k1gNnSc4	čištění
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
i	i	k9	i
na	na	k7c6	na
soupravách	souprava	k1gFnPc6	souprava
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
různé	různý	k2eAgInPc4d1	různý
obrazce	obrazec	k1gInPc4	obrazec
a	a	k8xC	a
nápisy	nápis	k1gInPc4	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc4	vlak
typů	typ	k1gInPc2	typ
81-71M	[number]	k4	81-71M
a	a	k8xC	a
M1	M1	k1gMnSc1	M1
sice	sice	k8xC	sice
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
interiér	interiér	k1gInSc4	interiér
navržený	navržený	k2eAgInSc4d1	navržený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
snadněji	snadno	k6eAd2	snadno
omyvatelný	omyvatelný	k2eAgInSc1d1	omyvatelný
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
odolával	odolávat	k5eAaImAgMnS	odolávat
výtvorům	výtvor	k1gInPc3	výtvor
graffiti	graffiti	k1gNnSc2	graffiti
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
i	i	k9	i
vozy	vůz	k1gInPc1	vůz
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
terčem	terč	k1gInSc7	terč
sprejerů	sprejer	k1gMnPc2	sprejer
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
soupravách	souprava	k1gFnPc6	souprava
81-71	[number]	k4	81-71
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
Graffiti	graffiti	k1gNnSc1	graffiti
rozšířenější	rozšířený	k2eAgNnSc1d2	rozšířenější
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
graffiti	graffiti	k1gNnSc2	graffiti
je	být	k5eAaImIp3nS	být
také	také	k9	také
problematický	problematický	k2eAgInSc1d1	problematický
i	i	k8xC	i
vandalismus	vandalismus	k1gInSc1	vandalismus
<g/>
,	,	kIx,	,
demolování	demolování	k1gNnSc1	demolování
vybavení	vybavení	k1gNnSc2	vybavení
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
či	či	k8xC	či
znemožňování	znemožňování	k1gNnSc2	znemožňování
jejího	její	k3xOp3gNnSc2	její
plynulého	plynulý	k2eAgNnSc2d1	plynulé
fungování	fungování	k1gNnSc2	fungování
<g/>
;	;	kIx,	;
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
poškozených	poškozený	k2eAgFnPc2d1	poškozená
věcí	věc	k1gFnPc2	věc
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
desítek	desítka	k1gFnPc2	desítka
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
spojováno	spojovat	k5eAaImNgNnS	spojovat
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
příjezdy	příjezd	k1gInPc7	příjezd
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
sportovních	sportovní	k2eAgMnPc2d1	sportovní
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
způsobili	způsobit	k5eAaPmAgMnP	způsobit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
dočasně	dočasně	k6eAd1	dočasně
zastavili	zastavit	k5eAaPmAgMnP	zastavit
provoz	provoz	k1gInSc4	provoz
částí	část	k1gFnPc2	část
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
využívají	využívat	k5eAaImIp3nP	využívat
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vlaky	vlak	k1gInPc4	vlak
i	i	k8xC	i
stanice	stanice	k1gFnPc4	stanice
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
k	k	k7c3	k
trestné	trestný	k2eAgFnSc3d1	trestná
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
drobných	drobný	k2eAgFnPc2d1	drobná
kapesních	kapesní	k2eAgFnPc2d1	kapesní
krádeží	krádež	k1gFnPc2	krádež
až	až	k9	až
po	po	k7c4	po
přepadení	přepadení	k1gNnSc4	přepadení
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
vybavena	vybavit	k5eAaPmNgFnS	vybavit
kamerovým	kamerový	k2eAgInSc7d1	kamerový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikal	vznikat	k5eAaImAgInS	vznikat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
umístěním	umístění	k1gNnSc7	umístění
či	či	k8xC	či
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
s	s	k7c7	s
pořizováním	pořizování	k1gNnSc7	pořizování
záznamu	záznam	k1gInSc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnSc4d1	podzemní
dráhu	dráha	k1gFnSc4	dráha
pravidelně	pravidelně	k6eAd1	pravidelně
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
i	i	k9	i
dvojčlenné	dvojčlenný	k2eAgFnPc1d1	dvojčlenná
hlídky	hlídka	k1gFnPc1	hlídka
městské	městský	k2eAgFnSc2d1	městská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
také	také	k9	také
i	i	k9	i
zvažována	zvažován	k2eAgFnSc1d1	zvažována
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
moskevském	moskevský	k2eAgInSc6d1	moskevský
nebo	nebo	k8xC	nebo
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
metru	metro	k1gNnSc6	metro
–	–	k?	–
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
teroristickému	teroristický	k2eAgInSc3d1	teroristický
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgNnP	být
proto	proto	k8xC	proto
provedena	proveden	k2eAgNnPc1d1	provedeno
různá	různý	k2eAgNnPc1d1	různé
cvičení	cvičení	k1gNnPc1	cvičení
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
a	a	k8xC	a
ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
podzemní	podzemní	k2eAgFnPc1d1	podzemní
dráhy	dráha	k1gFnPc1	dráha
se	se	k3xPyFc4	se
také	také	k9	také
objevily	objevit	k5eAaPmAgInP	objevit
plakáty	plakát	k1gInPc1	plakát
vyzývající	vyzývající	k2eAgFnSc2d1	vyzývající
cestující	cestující	k1gFnSc2	cestující
k	k	k7c3	k
obezřetnosti	obezřetnost	k1gFnSc3	obezřetnost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nalezení	nalezení	k1gNnSc2	nalezení
podezřelého	podezřelý	k2eAgNnSc2d1	podezřelé
zavazadla	zavazadlo	k1gNnSc2	zavazadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
španělské	španělský	k2eAgInPc4d1	španělský
vlaky	vlak	k1gInPc4	vlak
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
většina	většina	k1gFnSc1	většina
odpadkových	odpadkový	k2eAgInPc2d1	odpadkový
košů	koš	k1gInPc2	koš
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
negativně	negativně	k6eAd1	negativně
projevilo	projevit	k5eAaPmAgNnS	projevit
na	na	k7c6	na
čistotě	čistota	k1gFnSc6	čistota
a	a	k8xC	a
pořádku	pořádek	k1gInSc6	pořádek
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
dočasně	dočasně	k6eAd1	dočasně
–	–	k?	–
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
instalovat	instalovat	k5eAaBmF	instalovat
nové	nový	k2eAgInPc1d1	nový
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
protiteroristické	protiteroristický	k2eAgInPc4d1	protiteroristický
<g/>
"	"	kIx"	"
koše	koš	k1gInPc4	koš
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
do	do	k7c2	do
některého	některý	k3yIgNnSc2	některý
byla	být	k5eAaImAgFnS	být
uložena	uložen	k2eAgFnSc1d1	uložena
výbušnina	výbušnina	k1gFnSc1	výbušnina
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
by	by	kYmCp3nP	by
k	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
koše	koš	k1gInSc2	koš
by	by	kYmCp3nS	by
výbuch	výbuch	k1gInSc4	výbuch
udržela	udržet	k5eAaPmAgFnS	udržet
uvnitř	uvnitř	k7c2	uvnitř
odpadové	odpadový	k2eAgFnSc2d1	odpadová
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
D	D	kA	D
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
linky	linka	k1gFnSc2	linka
D.	D.	kA	D.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
magistrát	magistrát	k1gInSc1	magistrát
schválil	schválit	k5eAaPmAgInS	schválit
stavbu	stavba	k1gFnSc4	stavba
této	tento	k3xDgFnSc2	tento
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgNnP	mít
být	být	k5eAaImF	být
stavěna	stavit	k5eAaImNgNnP	stavit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
úsecích	úsek	k1gInPc6	úsek
<g/>
:	:	kIx,	:
Úsek	úsek	k1gInSc1	úsek
I.D	I.D	k1gFnSc1	I.D
<g/>
1	[number]	k4	1
z	z	k7c2	z
Pankráce	Pankrác	k1gFnSc2	Pankrác
do	do	k7c2	do
Písnice	písnice	k1gFnSc2	písnice
se	s	k7c7	s
sedmi	sedm	k4xCc2	sedm
novými	nový	k2eAgFnPc7d1	nová
stanicemi	stanice	k1gFnPc7	stanice
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
stanice	stanice	k1gFnPc1	stanice
ponesou	nést	k5eAaImIp3nP	nést
názvy	název	k1gInPc4	název
<g/>
:	:	kIx,	:
Olbrachtova	Olbrachtův	k2eAgFnSc1d1	Olbrachtova
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc1	nádraží
Krč	Krč	k1gFnSc1	Krč
<g/>
,	,	kIx,	,
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Krč	Krč	k1gFnSc1	Krč
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Libuš	Libuš	k1gInSc1	Libuš
<g/>
,	,	kIx,	,
Písnice	písnice	k1gFnSc1	písnice
a	a	k8xC	a
Depo	depo	k1gNnSc1	depo
Písnice	písnice	k1gFnSc2	písnice
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
začít	začít	k5eAaPmF	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
I.D	I.D	k1gFnSc1	I.D
<g/>
2	[number]	k4	2
z	z	k7c2	z
Pankráce	Pankrác	k1gFnSc2	Pankrác
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
stanice	stanice	k1gFnSc1	stanice
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
ponese	ponést	k5eAaPmIp3nS	ponést
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Nám.	Nám.	k1gFnSc1	Nám.
Bratří	bratr	k1gMnPc2	bratr
Synků	Synek	k1gMnPc2	Synek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
radní	radní	k1gMnPc1	radní
města	město	k1gNnSc2	město
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
linka	linka	k1gFnSc1	linka
D	D	kA	D
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
vlaky	vlak	k1gInPc4	vlak
bez	bez	k7c2	bez
řidiče	řidič	k1gInSc2	řidič
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
úsek	úsek	k1gInSc4	úsek
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
hotový	hotový	k2eAgInSc1d1	hotový
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2022	[number]	k4	2022
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dostavbě	dostavba	k1gFnSc6	dostavba
těchto	tento	k3xDgInPc2	tento
úseků	úsek	k1gInPc2	úsek
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
linka	linka	k1gFnSc1	linka
D	D	kA	D
schopna	schopen	k2eAgFnSc1d1	schopna
částečně	částečně	k6eAd1	částečně
nahradit	nahradit	k5eAaPmF	nahradit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
Nuselský	nuselský	k2eAgInSc4d1	nuselský
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
vede	vést	k5eAaImIp3nS	vést
linka	linka	k1gFnSc1	linka
C.	C.	kA	C.
Ve	v	k7c6	v
výhledových	výhledový	k2eAgNnPc6d1	výhledové
studiích	studio	k1gNnPc6	studio
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
prodloužení	prodloužení	k1gNnSc1	prodloužení
z	z	k7c2	z
Náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
na	na	k7c4	na
Žižkov	Žižkov	k1gInSc4	Žižkov
a	a	k8xC	a
Vysočany	Vysočany	k1gInPc4	Vysočany
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přes	přes	k7c4	přes
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
přestupu	přestup	k1gInSc3	přestup
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
B.	B.	kA	B.
Ve	v	k7c6	v
výhledu	výhled	k1gInSc6	výhled
je	být	k5eAaImIp3nS	být
prodloužení	prodloužení	k1gNnSc1	prodloužení
z	z	k7c2	z
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Motol	Motol	k1gInSc1	Motol
až	až	k6eAd1	až
na	na	k7c4	na
Letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobněji	pravděpodobně	k6eAd2	pravděpodobně
bude	být	k5eAaImBp3nS	být
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Nádraží	nádraží	k1gNnSc2	nádraží
Veleslavín	Veleslavín	k1gMnSc1	Veleslavín
vedena	vést	k5eAaImNgFnS	vést
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
rychlodráha	rychlodráha	k1gFnSc1	rychlodráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
také	také	k9	také
prodloužení	prodloužení	k1gNnSc4	prodloužení
přes	přes	k7c4	přes
Řepy	řepa	k1gFnPc4	řepa
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Zličín	Zličín	k1gInSc1	Zličín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
setkala	setkat	k5eAaPmAgNnP	setkat
s	s	k7c7	s
linkou	linka	k1gFnSc7	linka
B.	B.	kA	B.
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
E	E	kA	E
(	(	kIx(	(
<g/>
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
další	další	k2eAgFnSc6d1	další
lince	linka	k1gFnSc6	linka
E	E	kA	E
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
koncepci	koncepce	k1gFnSc3	koncepce
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
jako	jako	k8xS	jako
okružní	okružní	k2eAgFnSc1d1	okružní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
opět	opět	k6eAd1	opět
nezávazně	závazně	k6eNd1	závazně
mluvilo	mluvit	k5eAaImAgNnS	mluvit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
návrhy	návrh	k1gInPc7	návrh
na	na	k7c4	na
pořádání	pořádání	k1gNnSc4	pořádání
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
přesná	přesný	k2eAgFnSc1d1	přesná
trasa	trasa	k1gFnSc1	trasa
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
zanesena	zanést	k5eAaPmNgFnS	zanést
v	v	k7c6	v
územním	územní	k2eAgInSc6d1	územní
plánu	plán	k1gInSc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Fojtík	Fojtík	k1gMnSc1	Fojtík
<g/>
:	:	kIx,	:
30	[number]	k4	30
let	léto	k1gNnPc2	léto
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-239-2704-3	[number]	k4	80-239-2704-3
Pavel	Pavel	k1gMnSc1	Pavel
Fojtík	Fojtík	k1gMnSc1	Fojtík
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Linert	Linert	k1gMnSc1	Linert
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-238-5702-9	[number]	k4	80-238-5702-9
Pavel	Pavel	k1gMnSc1	Pavel
Fojtík	Fojtík	k1gMnSc1	Fojtík
<g/>
,	,	kIx,	,
PhDr.	PhDr.	kA	PhDr.
Marie	Marie	k1gFnSc1	Marie
Jílková	Jílková	k1gFnSc1	Jílková
<g/>
,	,	kIx,	,
ing.	ing.	kA	ing.
František	František	k1gMnSc1	František
Prošek	Prošek	k1gMnSc1	Prošek
<g/>
:	:	kIx,	:
Sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-238-0890-7	[number]	k4	80-238-0890-7
Josef	Josef	k1gMnSc1	Josef
Škorpil	Škorpil	k1gMnSc1	Škorpil
<g/>
:	:	kIx,	:
Pražské	pražský	k2eAgNnSc1d1	Pražské
metro	metro	k1gNnSc1	metro
<g/>
:	:	kIx,	:
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
dimenze	dimenze	k1gFnSc1	dimenze
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
:	:	kIx,	:
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7038-195-7	[number]	k4	80-7038-195-7
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Pražská	pražský	k2eAgFnSc1d1	Pražská
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
doprava	doprava	k1gFnSc1	doprava
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
Video	video	k1gNnSc1	video
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
ČT	ČT	kA	ČT
–	–	k?	–
dobové	dobový	k2eAgInPc4d1	dobový
zpravodajské	zpravodajský	k2eAgInPc4d1	zpravodajský
materiály	materiál	k1gInPc4	materiál
Československých	československý	k2eAgInPc2d1	československý
filmových	filmový	k2eAgInPc2d1	filmový
týdeníků	týdeník	k1gInPc2	týdeník
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
a	a	k8xC	a
provozu	provoz	k1gInSc2	provoz
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
Podívejte	podívat	k5eAaImRp2nP	podívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
stavělo	stavět	k5eAaImAgNnS	stavět
metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgFnPc1d1	unikátní
fotografie	fotografia	k1gFnPc1	fotografia
staré	starý	k2eAgFnPc1d1	stará
40	[number]	k4	40
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Technet	Technet	k1gInSc1	Technet
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Leningradské	leningradský	k2eAgNnSc4d1	Leningradské
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
shnilé	shnilý	k2eAgFnPc4d1	shnilá
brambory	brambora	k1gFnPc4	brambora
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
archivních	archivní	k2eAgInPc2d1	archivní
zvukových	zvukový	k2eAgInPc2d1	zvukový
záznamů	záznam	k1gInPc2	záznam
z	z	k7c2	z
vysílání	vysílání	k1gNnSc2	vysílání
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
z	z	k7c2	z
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
stavělo	stavět	k5eAaImAgNnS	stavět
a	a	k8xC	a
uvádělo	uvádět	k5eAaImAgNnS	uvádět
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
