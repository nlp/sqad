<s>
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1743	[number]	k4	1743
v	v	k7c6	v
Shadwellu	Shadwell	k1gInSc6	Shadwell
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1826	[number]	k4	1826
v	v	k7c6	v
Charlottesville	Charlottesvilla	k1gFnSc6	Charlottesvilla
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
třetí	třetí	k4xOgMnSc1	třetí
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
autor	autor	k1gMnSc1	autor
amerického	americký	k2eAgNnSc2d1	americké
prohlášení	prohlášení	k1gNnSc2	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
americké	americký	k2eAgFnSc2d1	americká
revoluce	revoluce	k1gFnSc2	revoluce
byl	být	k5eAaImAgMnS	být
guvernérem	guvernér	k1gMnSc7	guvernér
Virginie	Virginie	k1gFnSc2	Virginie
(	(	kIx(	(
<g/>
1779	[number]	k4	1779
<g/>
-	-	kIx~	-
<g/>
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
americkým	americký	k2eAgMnSc7d1	americký
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
<g/>
-	-	kIx~	-
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
prezidenta	prezident	k1gMnSc2	prezident
George	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
(	(	kIx(	(
<g/>
1790	[number]	k4	1790
<g/>
-	-	kIx~	-
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
sympatie	sympatie	k1gFnSc1	sympatie
k	k	k7c3	k
Velké	velký	k2eAgFnSc3d1	velká
francouzské	francouzský	k2eAgFnSc3d1	francouzská
revoluci	revoluce	k1gFnSc3	revoluce
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
konfliktu	konflikt	k1gInSc3	konflikt
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Hamiltonem	Hamilton	k1gInSc7	Hamilton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
konfliktu	konflikt	k1gInSc3	konflikt
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Označoval	označovat	k5eAaImAgMnS	označovat
se	se	k3xPyFc4	se
za	za	k7c4	za
materialistu	materialista	k1gMnSc4	materialista
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
deista	deista	k1gMnSc1	deista
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
pravdivost	pravdivost	k1gFnSc4	pravdivost
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Tíhnul	Tíhnout	k5eAaPmAgInS	Tíhnout
k	k	k7c3	k
"	"	kIx"	"
<g/>
zdravému	zdravý	k2eAgInSc3d1	zdravý
rozumu	rozum	k1gInSc3	rozum
<g/>
"	"	kIx"	"
a	a	k8xC	a
přírodním	přírodní	k2eAgInPc3d1	přírodní
zákonům	zákon	k1gInPc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Stavil	stavit	k5eAaBmAgInS	stavit
se	se	k3xPyFc4	se
do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
bankám	banka	k1gFnPc3	banka
a	a	k8xC	a
dluhům	dluh	k1gInPc3	dluh
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
mj.	mj.	kA	mj.
brání	bránit	k5eAaImIp3nS	bránit
koloniální	koloniální	k2eAgInSc1d1	koloniální
nezávislosti	nezávislost	k1gFnSc3	nezávislost
na	na	k7c4	na
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
rovnost	rovnost	k1gFnSc4	rovnost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
mu	on	k3xPp3gMnSc3	on
sloužili	sloužit	k5eAaImAgMnP	sloužit
otroci	otrok	k1gMnPc1	otrok
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgMnS	bojovat
například	například	k6eAd1	například
i	i	k9	i
proti	proti	k7c3	proti
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
Richmondu	Richmond	k1gInSc6	Richmond
budovu	budova	k1gFnSc4	budova
pro	pro	k7c4	pro
zákonodárné	zákonodárný	k2eAgNnSc4d1	zákonodárné
shromaždění	shromaždění	k1gNnSc4	shromaždění
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
stavební	stavební	k2eAgFnSc7d1	stavební
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
rok	rok	k1gInSc1	rok
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
Virginskou	virginský	k2eAgFnSc4d1	virginská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
konci	konec	k1gInSc6	konec
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
cihlová	cihlový	k2eAgFnSc1d1	cihlová
rotunda	rotunda	k1gFnSc1	rotunda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohrál	prohrát	k5eAaPmAgMnS	prohrát
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
hlasy	hlas	k1gInPc4	hlas
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Adamsem	Adams	k1gMnSc7	Adams
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nespolupracoval	spolupracovat	k5eNaImAgMnS	spolupracovat
s	s	k7c7	s
Adamsem	Adams	k1gInSc7	Adams
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgMnS	postavit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gMnPc3	jeho
zákonům	zákon	k1gInPc3	zákon
omezujícím	omezující	k2eAgNnSc6d1	omezující
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
amerického	americký	k2eAgNnSc2d1	americké
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
a	a	k8xC	a
zákonům	zákon	k1gInPc3	zákon
omezujícím	omezující	k2eAgFnPc3d1	omezující
kritiku	kritika	k1gFnSc4	kritika
státních	státní	k2eAgMnPc2d1	státní
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
John	John	k1gMnSc1	John
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
nadšence	nadšenka	k1gFnSc6	nadšenka
do	do	k7c2	do
šifrování	šifrování	k1gNnSc2	šifrování
a	a	k8xC	a
tajných	tajný	k2eAgInPc2d1	tajný
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
kteroužto	kteroužto	k?	kteroužto
zálibu	zálib	k1gInSc6	zálib
sdílel	sdílet	k5eAaImAgMnS	sdílet
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Pattersonem	Patterson	k1gMnSc7	Patterson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
pozůstalosti	pozůstalost	k1gFnSc6	pozůstalost
se	se	k3xPyFc4	se
jako	jako	k9	jako
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
přátelského	přátelský	k2eAgNnSc2d1	přátelské
soutěžení	soutěžení	k1gNnSc2	soutěžení
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
muži	muž	k1gMnPc7	muž
nachází	nacházet	k5eAaImIp3nS	nacházet
zašifrovaný	zašifrovaný	k2eAgInSc1d1	zašifrovaný
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
Patterson	Patterson	k1gNnSc1	Patterson
bez	bez	k7c2	bez
klíče	klíč	k1gInSc2	klíč
poslal	poslat	k5eAaPmAgMnS	poslat
coby	coby	k?	coby
dárek	dárek	k1gInSc4	dárek
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jej	on	k3xPp3gNnSc4	on
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
někdy	někdy	k6eAd1	někdy
rozluštil	rozluštit	k5eAaPmAgMnS	rozluštit
<g/>
,	,	kIx,	,
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
nikoliv	nikoliv	k9	nikoliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
určitě	určitě	k6eAd1	určitě
považoval	považovat	k5eAaImAgMnS	považovat
šifru	šifra	k1gFnSc4	šifra
za	za	k7c4	za
natolik	natolik	k6eAd1	natolik
důmyslnou	důmyslný	k2eAgFnSc4d1	důmyslná
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
její	její	k3xOp3gNnSc4	její
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Šifra	šifra	k1gFnSc1	šifra
odolávala	odolávat	k5eAaImAgFnS	odolávat
přes	přes	k7c4	přes
dvě	dva	k4xCgFnPc4	dva
stě	sto	k4xCgFnPc1	sto
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
počítačů	počítač	k1gMnPc2	počítač
prolomil	prolomit	k5eAaPmAgMnS	prolomit
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
kryptologii	kryptologie	k1gFnSc4	kryptologie
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
Lawren	Lawrna	k1gFnPc2	Lawrna
Smithline	Smithlin	k1gInSc5	Smithlin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
zábavám	zábava	k1gFnPc3	zábava
patřilo	patřit	k5eAaImAgNnS	patřit
také	také	k9	také
chovatelství	chovatelství	k1gNnSc1	chovatelství
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gNnSc4	Jefferson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc2	galerie
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
