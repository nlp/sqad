<p>
<s>
Český	český	k2eAgInSc1d1	český
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
klub	klub	k1gInSc1	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
Mistrovství	mistrovství	k1gNnSc2	mistrovství
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
došel	dojít	k5eAaPmAgInS	dojít
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
pražské	pražský	k2eAgFnSc3d1	Pražská
Slavii	slavie	k1gFnSc3	slavie
vysoko	vysoko	k6eAd1	vysoko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc2	mistrovství
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
po	po	k7c6	po
vynechání	vynechání	k1gNnSc3	vynechání
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
opět	opět	k6eAd1	opět
došel	dojít	k5eAaPmAgInS	dojít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znovu	znovu	k6eAd1	znovu
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
pražské	pražský	k2eAgFnSc3d1	Pražská
Slavii	slavie	k1gFnSc3	slavie
tentokráte	tentokráte	k?	tentokráte
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
ligové	ligový	k2eAgFnSc2d1	ligová
účasti	účast	k1gFnSc2	účast
==	==	k?	==
</s>
</p>
<p>
<s>
Stručný	stručný	k2eAgInSc1d1	stručný
přehledZdroj	přehledZdroj	k1gInSc1	přehledZdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1909	[number]	k4	1909
<g/>
:	:	kIx,	:
Mistrovství	mistrovství	k1gNnSc1	mistrovství
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1912	[number]	k4	1912
<g/>
:	:	kIx,	:
Mistrovství	mistrovství	k1gNnSc1	mistrovství
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligová	ligový	k2eAgFnSc1d1	ligová
úroveň	úroveň	k1gFnSc1	úroveň
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
<g/>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
ročníkyZdroj	ročníkyZdroj	k1gInSc4	ročníkyZdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
ZČ	ZČ	kA	ZČ
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
GUT	GUT	kA	GUT
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
PACINA	PACINA	kA	PACINA
-	-	kIx~	-
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
Olympia	Olympia	k1gFnSc1	Olympia
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
