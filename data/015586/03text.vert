<s>
Muzeum	muzeum	k1gNnSc1
autíček	autíčko	k1gNnPc2
</s>
<s>
Muzeum	muzeum	k1gNnSc1
autíček	autíčko	k1gNnPc2
Údaje	údaj	k1gInSc2
o	o	k7c6
muzeu	muzeum	k1gNnSc6
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Příseka	Příseka	k1gFnSc1
18	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
společnost	společnost	k1gFnSc1
Abrex	Abrex	k1gInSc1
Založeno	založit	k5eAaPmNgNnS
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
Zaměření	zaměření	k1gNnSc1
</s>
<s>
modely	model	k1gInPc1
automobilů	automobil	k1gInPc2
a	a	k8xC
hračky	hračka	k1gFnPc1
Původní	původní	k2eAgFnPc1d1
účel	účel	k1gInSc4
budovy	budova	k1gFnSc2
</s>
<s>
zámek	zámek	k1gInSc1
Vyhledávané	vyhledávaný	k2eAgInPc1d1
exponáty	exponát	k1gInPc1
</s>
<s>
nákladní	nákladní	k2eAgNnSc1d1
auto	auto	k1gNnSc1
Stella	Stella	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
20,33	20,33	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
48,3	48,3	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Muzeum	muzeum	k1gNnSc1
autíček	autíčko	k1gNnPc2
je	být	k5eAaImIp3nS
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Přísece	Příseka	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
umístěno	umístit	k5eAaPmNgNnS
na	na	k7c6
zámku	zámek	k1gInSc6
v	v	k7c6
Přísece	Příseka	k1gFnSc6
<g/>
,	,	kIx,
čp.	čp.	k?
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
součástí	součást	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
kavárna	kavárna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zřizovatelem	zřizovatel	k1gMnSc7
muzea	muzeum	k1gNnSc2
je	být	k5eAaImIp3nS
Radek	Radek	k1gMnSc1
Bukovský	Bukovský	k1gMnSc1
<g/>
,	,	kIx,
jednatel	jednatel	k1gMnSc1
společnosti	společnost	k1gFnSc2
Abrex	Abrex	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Expozice	expozice	k1gFnSc1
</s>
<s>
Muzeum	muzeum	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2015	#num#	k4
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
je	být	k5eAaImIp3nS
ve	v	k7c6
čtyřech	čtyři	k4xCgNnPc6
patrech	patro	k1gNnPc6
zámku	zámek	k1gInSc2
vystaveno	vystaven	k2eAgNnSc1d1
přibližně	přibližně	k6eAd1
10	#num#	k4
000	#num#	k4
autíček	autíčko	k1gNnPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
starých	starý	k2eAgFnPc2d1
hraček	hračka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
sbírkách	sbírka	k1gFnPc6
jsou	být	k5eAaImIp3nP
modely	model	k1gInPc1
aut	auto	k1gNnPc2
vyrobené	vyrobený	k2eAgNnSc4d1
od	od	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
vystaveny	vystavit	k5eAaPmNgInP
jsou	být	k5eAaImIp3nP
modely	model	k1gInPc1
od	od	k7c2
výrobců	výrobce	k1gMnPc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Kovodružstvo	kovodružstvo	k1gNnSc4
Náchod	Náchod	k1gInSc1
(	(	kIx(
<g/>
KDN	KDN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Igra	Igr	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Směr	směr	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Husch	Husch	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Šerý	šerý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Zbrojovka	zbrojovka	k1gFnSc1
Vsetín	Vsetín	k1gInSc1
<g/>
,	,	kIx,
Koh-i-Noor	Koh-i-Noor	k1gInSc1
<g/>
,	,	kIx,
Ites	Ites	k1gInSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
vystaveny	vystavit	k5eAaPmNgInP
například	například	k6eAd1
modely	model	k1gInPc1
mechanické	mechanický	k2eAgFnSc2d1
tramvaje	tramvaj	k1gFnSc2
<g/>
,	,	kIx,
válce	válec	k1gInSc2
<g/>
,	,	kIx,
tanku	tank	k1gInSc2
a	a	k8xC
českých	český	k2eAgNnPc2d1
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystaveny	vystaven	k2eAgInPc1d1
je	být	k5eAaImIp3nS
také	také	k6eAd1
elektricky	elektricky	k6eAd1
ovládaný	ovládaný	k2eAgInSc4d1
tank	tank	k1gInSc4
<g/>
,	,	kIx,
francouzská	francouzský	k2eAgFnSc1d1
autodráha	autodráha	k1gFnSc1
či	či	k8xC
nákladní	nákladní	k2eAgNnSc1d1
auto	auto	k1gNnSc1
Stella	Stella	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vystaveny	vystaven	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
modely	model	k1gInPc1
vyrobené	vyrobený	k2eAgInPc1d1
v	v	k7c6
Německu	Německo	k1gNnSc6
či	či	k8xC
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
muzea	muzeum	k1gNnSc2
chtěl	chtít	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc1
zakladatel	zakladatel	k1gMnSc1
také	také	k9
instalovat	instalovat	k5eAaBmF
svou	svůj	k3xOyFgFnSc4
soukromou	soukromý	k2eAgFnSc4d1
sbírku	sbírka	k1gFnSc4
200	#num#	k4
kusů	kus	k1gInPc2
šlapacích	šlapací	k2eAgNnPc2d1
autíček	autíčko	k1gNnPc2
<g/>
,	,	kIx,
ta	ten	k3xDgNnPc1
se	se	k3xPyFc4
tam	tam	k6eAd1
ale	ale	k9
nevejde	vejít	k5eNaPmIp3nS
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
se	se	k3xPyFc4
majitel	majitel	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
prodá	prodat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Muzeum	muzeum	k1gNnSc1
autíček	autíčko	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
muzeu	muzeum	k1gNnSc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
muzea	muzeum	k1gNnSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
společnosti	společnost	k1gFnSc2
Abrex	Abrex	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
KOMÍNOVÁ	komínový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
autíček	autíčko	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2015-11-23	2015-11-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
BLAŽEK	Blažka	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Příseky	přísek	k1gInPc4
musí	muset	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
chlap	chlap	k1gMnSc1
<g/>
,	,	kIx,
otevřelo	otevřít	k5eAaPmAgNnS
se	se	k3xPyFc4
tu	tu	k6eAd1
jedinečné	jedinečný	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
autíček	autíčko	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-10-09	2015-10-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
autíček	autíčko	k1gNnPc2
v	v	k7c4
Přísece	Příseec	k1gInPc4
přivítá	přivítat	k5eAaPmIp3nS
brzy	brzy	k6eAd1
první	první	k4xOgMnPc4
návštěvníky	návštěvník	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-09-19	2015-09-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CÁPOVÁ	CÁPOVÁ	kA
<g/>
,	,	kIx,
Irena	Irena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čech	Čechy	k1gFnPc2
prodává	prodávat	k5eAaImIp3nS
sbírku	sbírka	k1gFnSc4
šlapacích	šlapací	k2eAgNnPc2d1
autíček	autíčko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačí	stačit	k5eAaBmIp3nS
vám	vy	k3xPp2nPc3
tři	tři	k4xCgFnPc4
miliony	milion	k4xCgInPc4
a	a	k8xC
garáž	garáž	k1gFnSc4
pro	pro	k7c4
200	#num#	k4
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes	forbes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-20	2020-02-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
