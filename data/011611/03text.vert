<p>
<s>
Srí	Srí	k?	Srí
Lanka	lanko	k1gNnPc4	lanko
nebo	nebo	k8xC	nebo
Šrí	Šrí	k1gFnSc4	Šrí
Lanka	lanko	k1gNnSc2	lanko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
úředním	úřední	k2eAgInSc7d1	úřední
názvem	název	k1gInSc7	název
Srílanská	srílanský	k2eAgFnSc1d1	Srílanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
nebo	nebo	k8xC	nebo
Šrílanská	Šrílanský	k2eAgFnSc1d1	Šrílanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgNnSc1d1	ostrovní
stát	stát	k5eAaImF	stát
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
poloostrova	poloostrov	k1gInSc2	poloostrov
Přední	přední	k2eAgFnSc2d1	přední
Indie	Indie	k1gFnSc2	Indie
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Cejlon	Cejlon	k1gInSc1	Cejlon
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Indie	Indie	k1gFnSc2	Indie
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc1	ostrov
oddělen	oddělit	k5eAaPmNgInS	oddělit
Palkovou	Palkův	k2eAgFnSc7d1	Palkův
úžinou	úžina	k1gFnSc7	úžina
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
řetěz	řetěz	k1gInSc1	řetěz
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
zvaný	zvaný	k2eAgInSc1d1	zvaný
Adamův	Adamův	k2eAgInSc1d1	Adamův
most	most	k1gInSc1	most
<g/>
;	;	kIx,	;
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Bengálský	bengálský	k2eAgInSc1d1	bengálský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Kolombo	Kolombo	k1gNnSc1	Kolombo
(	(	kIx(	(
<g/>
též	též	k9	též
Colombo	Colomba	k1gFnSc5	Colomba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Šrí	Šrí	k1gFnSc1	Šrí
Džajavardanapura	Džajavardanapura	k1gFnSc1	Džajavardanapura
Kotte	Kott	k1gInSc5	Kott
(	(	kIx(	(
<g/>
Sri	Sri	k1gMnSc4	Sri
Jayawardenapura	Jayawardenapur	k1gMnSc4	Jayawardenapur
nebo	nebo	k8xC	nebo
také	také	k9	také
jen	jen	k9	jen
Kotte	Kott	k1gMnSc5	Kott
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
předměstím	předměstí	k1gNnSc7	předměstí
Kolomba	Kolombo	k1gNnSc2	Kolombo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nS	patřit
Dehivala	Dehivala	k1gFnSc1	Dehivala
<g/>
,	,	kIx,	,
Moratuva	Moratuvo	k1gNnPc1	Moratuvo
<g/>
,	,	kIx,	,
Kandy	Kand	k1gInPc1	Kand
<g/>
,	,	kIx,	,
Jápané	Jápaný	k2eAgInPc1d1	Jápaný
a	a	k8xC	a
Trinkunámalé	Trinkunámalý	k2eAgInPc1d1	Trinkunámalý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
různými	různý	k2eAgNnPc7d1	různé
jmény	jméno	k1gNnPc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Starořečtí	starořecký	k2eAgMnPc1d1	starořecký
zeměpisci	zeměpisec	k1gMnPc1	zeměpisec
mu	on	k3xPp3gMnSc3	on
říkali	říkat	k5eAaImAgMnP	říkat
Taprobané	Taprobaný	k2eAgFnPc1d1	Taprobaný
<g/>
,	,	kIx,	,
Arabové	Arab	k1gMnPc1	Arab
ho	on	k3xPp3gMnSc4	on
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k8xS	jako
Serendib	Serendib	k1gInSc4	Serendib
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sem	sem	k6eAd1	sem
připluli	připlout	k5eAaPmAgMnP	připlout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1505	[number]	k4	1505
<g/>
,	,	kIx,	,
ho	on	k3xPp3gNnSc4	on
zanesli	zanést	k5eAaPmAgMnP	zanést
na	na	k7c4	na
mapu	mapa	k1gFnSc4	mapa
jako	jako	k8xC	jako
Ceilã	Ceilã	k1gFnSc4	Ceilã
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pozdější	pozdní	k2eAgNnSc1d2	pozdější
anglické	anglický	k2eAgNnSc1d1	anglické
Ceylon	Ceylon	k1gInSc4	Ceylon
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
Cejlon	Cejlon	k1gInSc4	Cejlon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
Cejlon	Cejlon	k1gInSc1	Cejlon
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
ze	z	k7c2	z
sanskrtského	sanskrtský	k2eAgMnSc2d1	sanskrtský
Sinhala	Sinhal	k1gMnSc2	Sinhal
(	(	kIx(	(
<g/>
i	i	k9	i
hlavní	hlavní	k2eAgFnSc1d1	hlavní
etnická	etnický	k2eAgFnSc1d1	etnická
skupina	skupina	k1gFnSc1	skupina
srílanských	srílanský	k2eAgMnPc2d1	srílanský
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
Sinhálci	Sinhálec	k1gMnPc1	Sinhálec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
sinha	sinha	k1gFnSc1	sinha
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
"	"	kIx"	"
<g/>
lev	lev	k1gMnSc1	lev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
název	název	k1gInSc4	název
lze	lze	k6eAd1	lze
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
lví	lví	k2eAgFnSc1d1	lví
krev	krev	k1gFnSc1	krev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
lvi	lev	k1gMnPc1	lev
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
nikdy	nikdy	k6eAd1	nikdy
nežili	žít	k5eNaImAgMnP	žít
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
lvího	lví	k2eAgMnSc4d1	lví
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
dědečka	dědeček	k1gMnSc2	dědeček
prvního	první	k4xOgNnSc2	první
krále	král	k1gMnSc4	král
Vidžáji	Vidžáj	k1gFnSc6	Vidžáj
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
dějepisec	dějepisec	k1gMnSc1	dějepisec
Ammianus	Ammianus	k1gMnSc1	Ammianus
Marcellinus	Marcellinus	k1gMnSc1	Marcellinus
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
obyvatelích	obyvatel	k1gMnPc6	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
jako	jako	k8xS	jako
o	o	k7c4	o
Serandives	Serandives	k1gInSc4	Serandives
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
-dives	ives	k1gInSc1	-dives
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
indického	indický	k2eAgMnSc2d1	indický
dwī	dwī	k?	dwī
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ostrov	ostrov	k1gInSc1	ostrov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
ostrov	ostrov	k1gInSc1	ostrov
Seranů	Seran	k1gInPc2	Seran
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Serané	Seran	k1gMnPc1	Seran
snad	snad	k9	snad
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
Sinhálci	Sinhálec	k1gMnPc1	Sinhálec
<g/>
.	.	kIx.	.
</s>
<s>
Obdobnou	obdobný	k2eAgFnSc4d1	obdobná
etymologii	etymologie	k1gFnSc4	etymologie
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc1	název
Sielen	Sielen	k1gInSc1	Sielen
Diva	diva	k1gFnSc1	diva
<g/>
,	,	kIx,	,
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
řeckým	řecký	k2eAgInSc7d1	řecký
mořeplavcem	mořeplavec	k1gMnSc7	mořeplavec
Kosmasem	Kosmas	k1gMnSc7	Kosmas
Indikopleustem	Indikopleust	k1gMnSc7	Indikopleust
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Sielen	Sielen	k1gInSc1	Sielen
je	být	k5eAaImIp3nS	být
bezprostředním	bezprostřední	k2eAgMnSc7d1	bezprostřední
předchůdcem	předchůdce	k1gMnSc7	předchůdce
portugalského	portugalský	k2eAgInSc2d1	portugalský
Ceilã	Ceilã	k1gFnPc7	Ceilã
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cejlon	Cejlon	k1gInSc1	Cejlon
byl	být	k5eAaImAgInS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
ostrova	ostrov	k1gInSc2	ostrov
i	i	k8xC	i
země	zem	k1gFnSc2	zem
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
stát	stát	k1gInSc1	stát
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
<g/>
,	,	kIx,	,
suverénní	suverénní	k2eAgFnSc1d1	suverénní
a	a	k8xC	a
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
republika	republika	k1gFnSc1	republika
Srí	Srí	k1gFnSc2	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
"	"	kIx"	"
<g/>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
Srí	Srí	k1gFnSc2	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
ze	z	k7c2	z
sanskrtského	sanskrtský	k2eAgNnSc2d1	sanskrtské
slova	slovo	k1gNnSc2	slovo
laṃ	laṃ	k?	laṃ
<g/>
,	,	kIx,	,
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
"	"	kIx"	"
<g/>
zářivá	zářivý	k2eAgFnSc1d1	zářivá
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
takto	takto	k6eAd1	takto
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
popisován	popisován	k2eAgInSc1d1	popisován
ve	v	k7c6	v
starých	starý	k2eAgInPc6d1	starý
indických	indický	k2eAgInPc6d1	indický
eposech	epos	k1gInPc6	epos
Mahábhárata	Mahábhárata	k1gFnSc1	Mahábhárata
a	a	k8xC	a
Rámájana	Rámájana	k1gFnSc1	Rámájana
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
přepisoval	přepisovat	k5eAaImAgInS	přepisovat
jako	jako	k9	jako
Srí	Srí	k1gFnSc4	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
výslovnost	výslovnost	k1gFnSc1	výslovnost
sinhálského	sinhálský	k2eAgInSc2d1	sinhálský
ශ	ශ	k?	ශ
<g/>
්	්	k?	්
<g/>
ර	ර	k?	ර
<g/>
ී	ී	k?	ී
ල	ල	k?	ල
<g/>
ං	ං	k?	ං
<g/>
ක	ක	k?	ක
<g/>
ා	ා	k?	ා
śrī	śrī	k?	śrī
laṃ	laṃ	k?	laṃ
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
je	být	k5eAaImIp3nS	být
někde	někde	k6eAd1	někde
mezi	mezi	k7c4	mezi
s	s	k7c7	s
a	a	k8xC	a
š.	š.	k?	š.
V	v	k7c6	v
tamilštině	tamilština	k1gFnSc6	tamilština
zní	znět	k5eAaImIp3nS	znět
název	název	k1gInSc4	název
இ	இ	k?	இ
<g/>
்	்	k?	்
<g/>
க	க	k?	க
<g/>
ை	ை	k?	ை
ilaṅ	ilaṅ	k?	ilaṅ
<g/>
,	,	kIx,	,
iˈ	iˈ	k?	iˈ
<g/>
.	.	kIx.	.
</s>
<s>
Sinhálský	sinhálský	k2eAgInSc1d1	sinhálský
název	název	k1gInSc1	název
ostrova	ostrov	k1gInSc2	ostrov
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
státu	stát	k1gInSc3	stát
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ල	ල	k?	ල
<g/>
ං	ං	k?	ං
<g/>
ක	ක	k?	ක
<g/>
ා	ා	k?	ා
<g/>
ව	ව	k?	ව
laṃ	laṃ	k?	laṃ
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
laŋ	laŋ	k?	laŋ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Srí	Srí	k?	Srí
Lanka	lanko	k1gNnPc1	lanko
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Cejlon	Cejlon	k1gInSc1	Cejlon
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
mezi	mezi	k7c7	mezi
Lakadivským	lakadivský	k2eAgNnSc7d1	lakadivský
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
Bengálským	bengálský	k2eAgInSc7d1	bengálský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
Indickým	indický	k2eAgInSc7d1	indický
subkontinentem	subkontinent	k1gInSc7	subkontinent
<g/>
,	,	kIx,	,
od	od	k7c2	od
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
6000	[number]	k4	6000
a	a	k8xC	a
3500	[number]	k4	3500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
oddělil	oddělit	k5eAaPmAgMnS	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
Srí	Srí	k1gMnSc7	Srí
Lankou	Lanka	k1gMnSc7	Lanka
a	a	k8xC	a
Indií	Indie	k1gFnSc7	Indie
64	[number]	k4	64
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Palkův	Palkův	k2eAgInSc1d1	Palkův
průliv	průliv	k1gInSc1	průliv
a	a	k8xC	a
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
kontinentem	kontinent	k1gInSc7	kontinent
připomíná	připomínat	k5eAaImIp3nS	připomínat
řetěz	řetěz	k1gInSc1	řetěz
korálových	korálový	k2eAgFnPc2d1	korálová
mělčin	mělčina	k1gFnPc2	mělčina
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Adamův	Adamův	k2eAgInSc1d1	Adamův
most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kapky	kapka	k1gFnSc2	kapka
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
445	[number]	k4	445
km	km	kA	km
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
225	[number]	k4	225
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
ostrovem	ostrov	k1gInSc7	ostrov
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Pidurutalagala	Pidurutalagala	k1gFnSc1	Pidurutalagala
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
2	[number]	k4	2
524	[number]	k4	524
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
65	[number]	k4	65
610	[number]	k4	610
km2	km2	k4	km2
by	by	kYmCp3nS	by
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
zabírala	zabírat	k5eAaImAgFnS	zabírat
83,2	[number]	k4	83,2
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
podobně	podobně	k6eAd1	podobně
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
jako	jako	k8xC	jako
Litva	Litva	k1gFnSc1	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
1	[number]	k4	1
340	[number]	k4	340
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Desítky	desítka	k1gFnPc1	desítka
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
kolem	kolem	k7c2	kolem
pobřeží	pobřeží	k1gNnSc2	pobřeží
mají	mít	k5eAaImIp3nP	mít
celkovou	celkový	k2eAgFnSc4d1	celková
rozlohu	rozloha	k1gFnSc4	rozloha
342	[number]	k4	342
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrazné	výrazný	k2eAgInPc1d1	výrazný
klimatické	klimatický	k2eAgInPc1d1	klimatický
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgInP	dát
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
nížinami	nížina	k1gFnPc7	nížina
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
do	do	k7c2	do
2524	[number]	k4	2524
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgNnSc2d1	vysoké
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvlhčích	vlhký	k2eAgFnPc6d3	nejvlhčí
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
3000	[number]	k4	3000
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nP	dařit
svěžím	svěží	k2eAgInPc3d1	svěží
deštným	deštný	k2eAgInPc3d1	deštný
pralesům	prales	k1gInPc3	prales
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
sušších	suchý	k2eAgFnPc6d2	sušší
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
rostou	růst	k5eAaImIp3nP	růst
odolnější	odolný	k2eAgInPc4d2	odolnější
monzunové	monzunový	k2eAgInPc4d1	monzunový
lesy	les	k1gInPc4	les
s	s	k7c7	s
cennými	cenný	k2eAgFnPc7d1	cenná
dřevinami	dřevina	k1gFnPc7	dřevina
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
ebenové	ebenový	k2eAgNnSc1d1	ebenové
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
atlasové	atlasový	k2eAgNnSc1d1	atlasové
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnPc1d1	velká
plochy	plocha	k1gFnPc1	plocha
lesů	les	k1gInPc2	les
však	však	k9	však
již	již	k6eAd1	již
byly	být	k5eAaImAgFnP	být
vykáceny	vykácen	k2eAgFnPc1d1	vykácena
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
původně	původně	k6eAd1	původně
velká	velký	k2eAgNnPc1d1	velké
stáda	stádo	k1gNnPc1	stádo
slonů	slon	k1gMnPc2	slon
nyní	nyní	k6eAd1	nyní
čítají	čítat	k5eAaImIp3nP	čítat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Přírodními	přírodní	k2eAgInPc7d1	přírodní
zdroji	zdroj	k1gInPc7	zdroj
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
diamanty	diamant	k1gInPc1	diamant
<g/>
,	,	kIx,	,
drahokamy	drahokam	k1gInPc1	drahokam
<g/>
,	,	kIx,	,
grafit	grafit	k1gInSc1	grafit
<g/>
,	,	kIx,	,
písky	písek	k1gInPc1	písek
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Maháveli	Mahável	k1gInPc7	Mahável
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
tropickými	tropický	k2eAgFnPc7d1	tropická
cyklónami	cyklóna	k1gFnPc7	cyklóna
a	a	k8xC	a
tornády	tornádo	k1gNnPc7	tornádo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Evropská	evropský	k2eAgFnSc1d1	Evropská
kolonizace	kolonizace	k1gFnSc1	kolonizace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1505	[number]	k4	1505
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
poprvé	poprvé	k6eAd1	poprvé
dopluli	doplout	k5eAaPmAgMnP	doplout
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
ostrov	ostrov	k1gInSc1	ostrov
však	však	k9	však
plně	plně	k6eAd1	plně
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
teprve	teprve	k6eAd1	teprve
Britové	Brit	k1gMnPc1	Brit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
se	se	k3xPyFc4	se
pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
části	část	k1gFnPc1	část
ostrova	ostrov	k1gInSc2	ostrov
staly	stát	k5eAaPmAgFnP	stát
britskou	britský	k2eAgFnSc7d1	britská
korunní	korunní	k2eAgFnSc7d1	korunní
kolonií	kolonie	k1gFnSc7	kolonie
Cejlonem	Cejlon	k1gInSc7	Cejlon
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
připojeno	připojit	k5eAaPmNgNnS	připojit
poslední	poslední	k2eAgNnSc1d1	poslední
sinhálské	sinhálský	k2eAgNnSc1d1	sinhálské
království	království	k1gNnSc1	království
Kandy	Kanda	k1gFnSc2	Kanda
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgMnSc1d1	rozkládající
se	se	k3xPyFc4	se
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Britů	Brit	k1gMnPc2	Brit
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
ekonomicky	ekonomicky	k6eAd1	ekonomicky
i	i	k9	i
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
ostrova	ostrov	k1gInSc2	ostrov
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
využívali	využívat	k5eAaPmAgMnP	využívat
Tamily	Tamil	k1gMnPc7	Tamil
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
nově	nově	k6eAd1	nově
přivezené	přivezený	k2eAgFnSc2d1	přivezená
příslušníky	příslušník	k1gMnPc7	příslušník
tamního	tamní	k2eAgNnSc2d1	tamní
tamilského	tamilský	k2eAgNnSc2d1	tamilské
etnika	etnikum	k1gNnSc2	etnikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
dostala	dostat	k5eAaPmAgFnS	dostat
"	"	kIx"	"
<g/>
tamilská	tamilský	k2eAgFnSc1d1	tamilská
otázka	otázka	k1gFnSc1	otázka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
rozpory	rozpora	k1gFnPc4	rozpora
mezi	mezi	k7c7	mezi
většinovými	většinový	k2eAgMnPc7d1	většinový
Sinhálci	Sinhálec	k1gMnPc7	Sinhálec
a	a	k8xC	a
menšinovými	menšinový	k2eAgMnPc7d1	menšinový
Tamily	Tamil	k1gMnPc7	Tamil
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
zbaveni	zbavit	k5eAaPmNgMnP	zbavit
cejlonského	cejlonský	k2eAgNnSc2d1	cejlonské
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
vystěhováni	vystěhovat	k5eAaPmNgMnP	vystěhovat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Diskriminace	diskriminace	k1gFnSc1	diskriminace
Tamilů	Tamil	k1gMnPc2	Tamil
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
We	We	k1gFnSc2	We
Tamils	Tamils	k1gInSc1	Tamils
Movement	Movement	k1gMnSc1	Movement
(	(	kIx(	(
<g/>
WTM	WTM	kA	WTM
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Hnutí	hnutí	k1gNnSc1	hnutí
My	my	k3xPp1nPc1	my
Tamilové	Tamil	k1gMnPc1	Tamil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvním	první	k4xOgInPc3	první
krvavým	krvavý	k2eAgInPc3d1	krvavý
střetům	střet	k1gInPc3	střet
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
lidské	lidský	k2eAgFnPc1d1	lidská
oběti	oběť	k1gFnPc1	oběť
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
premiéra	premiér	k1gMnSc2	premiér
Solomona	Solomon	k1gMnSc2	Solomon
Bandáranájakeho	Bandáranájake	k1gMnSc2	Bandáranájake
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
části	část	k1gFnSc2	část
tamilského	tamilský	k2eAgNnSc2d1	tamilské
hnutí	hnutí	k1gNnSc2	hnutí
radikalizovaly	radikalizovat	k5eAaBmAgFnP	radikalizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zastřešující	zastřešující	k2eAgFnSc2d1	zastřešující
organizace	organizace	k1gFnSc2	organizace
Tygři	tygr	k1gMnPc1	tygr
osvobození	osvobození	k1gNnSc2	osvobození
tamilského	tamilský	k2eAgInSc2d1	tamilský
Ílamu	Ílam	k1gInSc2	Ílam
(	(	kIx(	(
<g/>
LTTE	LTTE	kA	LTTE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
sinhálskou	sinhálský	k2eAgFnSc7d1	sinhálská
buddhistickou	buddhistický	k2eAgFnSc7d1	buddhistická
většinou	většina	k1gFnSc7	většina
a	a	k8xC	a
tamilskou	tamilský	k2eAgFnSc7d1	tamilská
hinduistickou	hinduistický	k2eAgFnSc7d1	hinduistická
menšinou	menšina	k1gFnSc7	menšina
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c4	v
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
trvala	trvalo	k1gNnSc2	trvalo
přes	přes	k7c4	přes
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
LTTE	LTTE	kA	LTTE
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
bojích	boj	k1gInPc6	boj
definitivně	definitivně	k6eAd1	definitivně
poražení	poražení	k1gNnSc4	poražení
srílanskou	srílanský	k2eAgFnSc7d1	Srílanská
armádou	armáda	k1gFnSc7	armáda
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
většinovými	většinový	k2eAgMnPc7d1	většinový
Sinhálci	Sinhálec	k1gMnPc7	Sinhálec
a	a	k8xC	a
menšinovými	menšinový	k2eAgMnPc7d1	menšinový
Tamily	Tamil	k1gMnPc7	Tamil
zůstal	zůstat	k5eAaPmAgMnS	zůstat
stále	stále	k6eAd1	stále
napjatý	napjatý	k2eAgMnSc1d1	napjatý
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
ostrov	ostrov	k1gInSc4	ostrov
tsunami	tsunami	k1gNnSc1	tsunami
způsobená	způsobený	k2eAgFnSc1d1	způsobená
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
poslední	poslední	k2eAgFnSc7d1	poslední
etapou	etapa	k1gFnSc7	etapa
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
tísnivé	tísnivý	k2eAgFnSc6d1	tísnivá
životní	životní	k2eAgFnSc6d1	životní
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
i	i	k9	i
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
občany	občan	k1gMnPc7	občan
ČR	ČR	kA	ČR
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
přes	přes	k7c4	přes
360	[number]	k4	360
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
humanitární	humanitární	k2eAgFnPc1d1	humanitární
organizace	organizace	k1gFnPc1	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
,	,	kIx,	,
ADRA	ADRA	kA	ADRA
či	či	k8xC	či
Lékaři	lékař	k1gMnPc1	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
pomoc	pomoc	k1gFnSc4	pomoc
navázaly	navázat	k5eAaPmAgFnP	navázat
další	další	k2eAgFnPc1d1	další
rozvojové	rozvojový	k2eAgFnPc1d1	rozvojová
aktivity	aktivita	k1gFnPc1	aktivita
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yRgInPc3	který
získala	získat	k5eAaPmAgFnS	získat
ČR	ČR	kA	ČR
dle	dle	k7c2	dle
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc1d1	dobré
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
==	==	k?	==
</s>
</p>
<p>
<s>
Srí	Srí	k?	Srí
Lanka	lanko	k1gNnSc2	lanko
je	být	k5eAaImIp3nS	být
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
republikou	republika	k1gFnSc7	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stojí	stát	k5eAaImIp3nS	stát
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
maximálně	maximálně	k6eAd1	maximálně
2	[number]	k4	2
šestiletá	šestiletý	k2eAgFnSc1d1	šestiletá
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
členů	člen	k1gMnPc2	člen
parlamentu	parlament	k1gInSc2	parlament
pak	pak	k6eAd1	pak
vybírá	vybírat	k5eAaImIp3nS	vybírat
premiéra	premiéra	k1gFnSc1	premiéra
a	a	k8xC	a
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
členy	člen	k1gMnPc7	člen
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
prezidenta	prezident	k1gMnSc4	prezident
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
nemůže	moct	k5eNaImIp3nS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
jednokomorového	jednokomorový	k2eAgInSc2d1	jednokomorový
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
voleno	volen	k2eAgNnSc1d1	voleno
225	[number]	k4	225
poslanců	poslanec	k1gMnPc2	poslanec
na	na	k7c4	na
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Šrí	Šrí	k1gFnSc1	Šrí
Džajavardanapura	Džajavardanapura	k1gFnSc1	Džajavardanapura
Kotte	Kott	k1gInSc5	Kott
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
decentralizace	decentralizace	k1gFnSc2	decentralizace
přeneseno	přenesen	k2eAgNnSc1d1	přeneseno
sídlo	sídlo	k1gNnSc1	sídlo
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
však	však	k9	však
dosud	dosud	k6eAd1	dosud
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Kolombu	Kolombo	k1gNnSc6	Kolombo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
budou	být	k5eAaImBp3nP	být
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
přenesena	přenést	k5eAaPmNgFnS	přenést
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
Kotte	Kott	k1gInSc5	Kott
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
Kolomba	Kolombo	k1gNnSc2	Kolombo
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
není	být	k5eNaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Kandy	Kanda	k1gFnPc4	Kanda
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Kendy	Kenda	k1gFnSc2	Kenda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgNnSc4d1	bývalé
sídlo	sídlo	k1gNnSc4	sídlo
Sinhálských	sinhálský	k2eAgMnPc2d1	sinhálský
králů	král	k1gMnPc2	král
ležící	ležící	k2eAgFnSc4d1	ležící
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Sjíždějí	sjíždět	k5eAaImIp3nP	sjíždět
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
buddhisté	buddhista	k1gMnPc1	buddhista
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
Chrámu	chrám	k1gInSc6	chrám
Zlatého	zlatý	k2eAgInSc2d1	zlatý
zubu	zub	k1gInSc2	zub
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
uložen	uložit	k5eAaPmNgInS	uložit
zub	zub	k1gInSc1	zub
Buddhy	Buddha	k1gMnPc4	Buddha
Gautamy	Gautam	k1gInPc7	Gautam
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
je	být	k5eAaImIp3nS	být
též	též	k9	též
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Galle	Galle	k1gFnSc1	Galle
(	(	kIx(	(
<g/>
Gálla	Gálla	k1gFnSc1	Gálla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejdůležitější	důležitý	k2eAgInSc4d3	nejdůležitější
pro	pro	k7c4	pro
holandské	holandský	k2eAgMnPc4d1	holandský
kolonizátory	kolonizátor	k1gMnPc4	kolonizátor
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
2000	[number]	k4	2000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
ležící	ležící	k2eAgFnSc1d1	ležící
Nuwara	Nuwara	k1gFnSc1	Nuwara
Eliya	Eliya	k1gFnSc1	Eliya
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
boháčů	boháč	k1gMnPc2	boháč
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
pěstování	pěstování	k1gNnSc2	pěstování
čaje	čaj	k1gInSc2	čaj
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
9	[number]	k4	9
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
sinhálsky	sinhálsky	k6eAd1	sinhálsky
ප	ප	k?	ප
<g/>
ා	ා	k?	ා
<g/>
ත	ත	k?	ත
<g/>
,	,	kIx,	,
paḷ	paḷ	k?	paḷ
<g/>
;	;	kIx,	;
tamilsky	tamilsky	k6eAd1	tamilsky
ம	ம	k?	ம
<g/>
ா	ா	k?	ா
<g/>
க	க	k?	க
<g/>
ா	ா	k?	ா
<g/>
ண	ண	k?	ண
<g/>
்	்	k?	்
<g/>
,	,	kIx,	,
mā	mā	k?	mā
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Národnostní	národnostní	k2eAgNnSc4d1	národnostní
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
Sinhálci	Sinhálec	k1gMnPc1	Sinhálec
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
73,8	[number]	k4	73,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
etnikum	etnikum	k1gNnSc4	etnikum
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tamilové	Tamil	k1gMnPc1	Tamil
(	(	kIx(	(
<g/>
13,9	[number]	k4	13,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náležící	náležící	k2eAgMnSc1d1	náležící
k	k	k7c3	k
drávidské	drávidský	k2eAgFnSc3d1	drávidská
skupině	skupina	k1gFnSc3	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Separatistické	separatistický	k2eAgFnPc1d1	separatistická
tendence	tendence	k1gFnPc1	tendence
<g/>
,	,	kIx,	,
reprezentované	reprezentovaný	k2eAgFnPc1d1	reprezentovaná
především	především	k9	především
ozbrojenou	ozbrojený	k2eAgFnSc7d1	ozbrojená
skupinou	skupina	k1gFnSc7	skupina
Tygři	tygr	k1gMnPc1	tygr
osvobození	osvobození	k1gNnSc2	osvobození
tamilského	tamilský	k2eAgInSc2d1	tamilský
Ílamu	Ílam	k1gInSc2	Ílam
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgInSc3d1	dlouhodobý
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Sinhálců	Sinhálec	k1gMnPc2	Sinhálec
(	(	kIx(	(
<g/>
buddhistů	buddhista	k1gMnPc2	buddhista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tamilů	Tamil	k1gMnPc2	Tamil
(	(	kIx(	(
<g/>
hinduistů	hinduista	k1gMnPc2	hinduista
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
Srí	Srí	k1gFnPc6	Srí
Lance	lance	k1gNnSc4	lance
velké	velký	k2eAgNnSc4d1	velké
procento	procento	k1gNnSc4	procento
muslimů	muslim	k1gMnPc2	muslim
(	(	kIx(	(
<g/>
7,2	[number]	k4	7,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
menšina	menšina	k1gFnSc1	menšina
Tamilů	Tamil	k1gMnPc2	Tamil
s	s	k7c7	s
indickými	indický	k2eAgInPc7d1	indický
kořeny	kořen	k1gInPc7	kořen
(	(	kIx(	(
<g/>
4,6	[number]	k4	4,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
malá	malý	k2eAgFnSc1d1	malá
menšina	menšina	k1gFnSc1	menšina
Malajců	Malajec	k1gMnPc2	Malajec
(	(	kIx(	(
<g/>
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
potomci	potomek	k1gMnPc1	potomek
Portugalců	Portugalec	k1gMnPc2	Portugalec
a	a	k8xC	a
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
tzv.	tzv.	kA	tzv.
Burgherové	Burgherová	k1gFnSc6	Burgherová
(	(	kIx(	(
<g/>
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Demografie	demografie	k1gFnSc2	demografie
===	===	k?	===
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
dožití	dožití	k1gNnSc1	dožití
muže	muž	k1gMnSc2	muž
<g/>
:	:	kIx,	:
69,33	[number]	k4	69,33
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
dožití	dožití	k1gNnSc1	dožití
ženy	žena	k1gFnSc2	žena
<g/>
:	:	kIx,	:
74,45	[number]	k4	74,45
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
<g/>
:	:	kIx,	:
90,2	[number]	k4	90,2
%	%	kIx~	%
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Srí	Srí	k1gMnSc1	Srí
Lanky	lanko	k1gNnPc7	lanko
jsou	být	k5eAaImIp3nP	být
thérávádoví	thérávádový	k2eAgMnPc1d1	thérávádový
buddhisté	buddhista	k1gMnPc1	buddhista
<g/>
,	,	kIx,	,
12,6	[number]	k4	12,6
%	%	kIx~	%
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
,	,	kIx,	,
9,7	[number]	k4	9,7
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
7,4	[number]	k4	7,4
%	%	kIx~	%
křesťané	křesťan	k1gMnPc1	křesťan
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
též	též	k9	též
anglikáni	anglikán	k1gMnPc1	anglikán
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Údaje	údaj	k1gInPc1	údaj
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
2011	[number]	k4	2011
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Srí	Srí	k?	Srí
Lanka	lanko	k1gNnPc4	lanko
je	být	k5eAaImIp3nS	být
především	především	k9	především
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
rozvinutým	rozvinutý	k2eAgNnSc7d1	rozvinuté
plantážním	plantážní	k2eAgNnSc7d1	plantážní
<g/>
,	,	kIx,	,
pěstováním	pěstování	k1gNnSc7	pěstování
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
,	,	kIx,	,
kokosové	kokosový	k2eAgFnSc2d1	kokosová
palmy	palma	k1gFnSc2	palma
<g/>
,	,	kIx,	,
kaučukovníku	kaučukovník	k1gInSc2	kaučukovník
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
manioku	maniok	k1gInSc2	maniok
<g/>
,	,	kIx,	,
batátů	batáty	k1gInPc2	batáty
<g/>
,	,	kIx,	,
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
také	také	k9	také
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
zpracovatelský	zpracovatelský	k2eAgInSc4d1	zpracovatelský
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
a	a	k8xC	a
textilní	textilní	k2eAgInSc4d1	textilní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Srí	Srí	k?	Srí
Lanka	lanko	k1gNnPc4	lanko
je	být	k5eAaImIp3nS	být
také	také	k9	také
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
turistickou	turistický	k2eAgFnSc7d1	turistická
destinací	destinace	k1gFnSc7	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Maledivách	Maledivy	k1gFnPc6	Maledivy
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
1	[number]	k4	1
447	[number]	k4	447
km	km	kA	km
železnic	železnice	k1gFnPc2	železnice
provozovaných	provozovaný	k2eAgFnPc2d1	provozovaná
národní	národní	k2eAgFnSc7d1	národní
společností	společnost	k1gFnSc7	společnost
Sri	Sri	k1gFnSc2	Sri
Lanka	lanko	k1gNnSc2	lanko
Railways	Railwaysa	k1gFnPc2	Railwaysa
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc7d1	národní
leteckou	letecký	k2eAgFnSc7d1	letecká
společností	společnost	k1gFnSc7	společnost
jsou	být	k5eAaImIp3nP	být
SriLankan	SriLankan	k1gMnSc1	SriLankan
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
Přístav	přístav	k1gInSc1	přístav
Mahindy	Mahinda	k1gFnSc2	Mahinda
Radžapaksy	Radžapaksa	k1gFnSc2	Radžapaksa
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Hambantota	Hambantota	k1gFnSc1	Hambantota
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přístav	přístav	k1gInSc1	přístav
má	mít	k5eAaImIp3nS	mít
využít	využít	k5eAaPmF	využít
výhodné	výhodný	k2eAgFnPc4d1	výhodná
polohy	poloha	k1gFnPc4	poloha
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
námořní	námořní	k2eAgFnSc6d1	námořní
trase	trasa	k1gFnSc6	trasa
mezi	mezi	k7c7	mezi
Suezským	suezský	k2eAgInSc7d1	suezský
průplavem	průplav	k1gInSc7	průplav
a	a	k8xC	a
Malackým	malacký	k2eAgInSc7d1	malacký
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
aspoň	aspoň	k9	aspoň
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
roka	rok	k1gInSc2	rok
navštíví	navštívit	k5eAaPmIp3nP	navštívit
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnPc1	hora
Šrí	Šrí	k1gFnSc2	Šrí
Pada	Pada	k1gFnSc1	Pada
(	(	kIx(	(
<g/>
Svatá	svatý	k2eAgFnSc1d1	svatá
stopa	stopa	k1gFnSc1	stopa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2	[number]	k4	2
224	[number]	k4	224
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Adam	Adam	k1gMnSc1	Adam
<g/>
'	'	kIx"	'
<g/>
ova	ova	k?	ova
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
otištěno	otištěn	k2eAgNnSc1d1	otištěno
ve	v	k7c6	v
skále	skála	k1gFnSc6	skála
velké	velký	k2eAgNnSc1d1	velké
chodidlo	chodidlo	k1gNnSc1	chodidlo
<g/>
:	:	kIx,	:
křesťané	křesťan	k1gMnPc1	křesťan
a	a	k8xC	a
muslimové	muslim	k1gMnPc1	muslim
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Adamovo	Adamův	k2eAgNnSc1d1	Adamovo
<g/>
,	,	kIx,	,
buddhisté	buddhista	k1gMnPc1	buddhista
Buddhovo	Buddhův	k2eAgNnSc1d1	Buddhovo
<g/>
,	,	kIx,	,
hinduisté	hinduista	k1gMnPc1	hinduista
Šivovo	Šivův	k2eAgNnSc1d1	Šivovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
měl	mít	k5eAaImAgInS	mít
každý	každý	k3xTgInSc1	každý
větší	veliký	k2eAgInSc1d2	veliký
náboženský	náboženský	k2eAgInSc1d1	náboženský
směr	směr	k1gInSc1	směr
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Rádžapakse	Rádžapakse	k1gFnSc2	Rádžapakse
je	být	k5eAaImIp3nS	být
však	však	k9	však
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
do	do	k7c2	do
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
pro	pro	k7c4	pro
náboženské	náboženský	k2eAgFnPc4d1	náboženská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
<g/>
:	:	kIx,	:
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Ženy	žena	k1gFnPc1	žena
mají	mít	k5eAaImIp3nP	mít
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
pít	pít	k5eAaImF	pít
alkohol	alkohol	k1gInSc4	alkohol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
kouřit	kouřit	k5eAaImF	kouřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
kávy	káva	k1gFnSc2	káva
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FILIPSKÝ	FILIPSKÝ	kA	FILIPSKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
,	,	kIx,	,
Malediv	Maledivy	k1gFnPc2	Maledivy
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
Šrí	Šrí	k1gFnSc1	Šrí
Lanky	lanko	k1gNnPc7	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
647	[number]	k4	647
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Srí	Srí	k1gFnSc2	Srí
Lanka	lanko	k1gNnSc2	lanko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Srí	Srí	k1gFnSc2	Srí
Lanka	lanko	k1gNnSc2	lanko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Srí	Srí	k1gFnSc2	Srí
Lanka	lanko	k1gNnSc2	lanko
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
</s>
</p>
<p>
<s>
Anurádhapura	Anurádhapura	k1gFnSc1	Anurádhapura
<g/>
:	:	kIx,	:
první	první	k4xOgMnSc1	první
hlavní	hlavní	k2eAgMnSc1d1	hlavní
město	město	k1gNnSc4	město
na	na	k7c6	na
Cejlonu	Cejlon	k1gInSc6	Cejlon
</s>
</p>
<p>
<s>
Humanitární	humanitární	k2eAgFnSc1d1	humanitární
krize	krize	k1gFnSc1	krize
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc1	lance
–	–	k?	–
stránky	stránka	k1gFnPc4	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Leonardo	Leonardo	k1gMnSc1	Leonardo
a	a	k8xC	a
Lékařů	lékař	k1gMnPc2	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
</s>
</p>
<p>
<s>
Cestopis	cestopis	k1gInSc1	cestopis
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
ze	z	k7c2	z
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
leden	leden	k1gInSc1	leden
<g/>
–	–	k?	–
<g/>
únor	únor	k1gInSc4	únor
2009	[number]	k4	2009
den	den	k1gInSc4	den
po	po	k7c6	po
dni	den	k1gInSc6	den
</s>
</p>
<p>
<s>
Cestopis	cestopis	k1gInSc1	cestopis
s	s	k7c7	s
3D	[number]	k4	3D
fotografiemi	fotografia	k1gFnPc7	fotografia
ze	z	k7c2	z
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Sri	Sri	k?	Sri
Lanka	lanko	k1gNnPc1	lanko
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sri	Sri	k?	Sri
Lanka	lanko	k1gNnPc1	lanko
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Sri	Sri	k1gFnSc2	Sri
Lanka	lanko	k1gNnSc2	lanko
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
South	South	k1gMnSc1	South
and	and	k?	and
Central	Central	k1gMnSc1	Central
Asian	Asian	k1gMnSc1	Asian
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Sri	Sri	k1gFnSc1	Sri
Lanka	lanko	k1gNnSc2	lanko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-04-06	[number]	k4	2011-04-06
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Sri	Sri	k1gFnSc1	Sri
Lanka	lanko	k1gNnSc2	lanko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-05	[number]	k4	2011-07-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-03-25	[number]	k4	2011-03-25
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ARASARATNAM	ARASARATNAM	kA	ARASARATNAM
<g/>
,	,	kIx,	,
Sinnappah	Sinnappah	k1gMnSc1	Sinnappah
<g/>
;	;	kIx,	;
PEIRIS	PEIRIS	kA	PEIRIS
<g/>
,	,	kIx,	,
Gerald	Gerald	k1gMnSc1	Gerald
Hubert	Hubert	k1gMnSc1	Hubert
<g/>
.	.	kIx.	.
</s>
<s>
Sri	Sri	k?	Sri
Lanka	lanko	k1gNnSc2	lanko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
jmen	jméno	k1gNnPc2	jméno
států	stát	k1gInPc2	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
územních	územní	k2eAgFnPc2d1	územní
částí	část	k1gFnPc2	část
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
úřad	úřad	k1gInSc1	úřad
zeměměřický	zeměměřický	k2eAgInSc1d1	zeměměřický
a	a	k8xC	a
katastrální	katastrální	k2eAgInSc1d1	katastrální
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
