<s>
Korelace	korelace	k1gFnSc1	korelace
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
procesy	proces	k1gInPc7	proces
nebo	nebo	k8xC	nebo
veličinami	veličina	k1gFnPc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
korelativně	korelativně	k6eAd1	korelativně
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
procesy	proces	k1gInPc7	proces
ukáže	ukázat	k5eAaPmIp3nS	ukázat
korelace	korelace	k1gFnSc1	korelace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
závisejí	záviset	k5eAaImIp3nP	záviset
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
ještě	ještě	k6eAd1	ještě
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
následkem	následkem	k7c2	následkem
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samotná	samotný	k2eAgFnSc1d1	samotná
korelace	korelace	k1gFnSc1	korelace
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
korelace	korelace	k1gFnSc1	korelace
neimplikuje	implikovat	k5eNaImIp3nS	implikovat
kauzalitu	kauzalita	k1gFnSc4	kauzalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitějším	určitý	k2eAgInSc6d2	určitější
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
korelace	korelace	k1gFnSc2	korelace
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
statistice	statistika	k1gFnSc6	statistika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znamená	znamenat	k5eAaImIp3nS	znamenat
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
lineární	lineární	k2eAgInSc1d1	lineární
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
či	či	k8xC	či
veličinami	veličina	k1gFnPc7	veličina
x	x	k?	x
a	a	k8xC	a
y.	y.	k?	y.
.	.	kIx.	.
</s>
<s>
Míru	Míra	k1gFnSc4	Míra
korelace	korelace	k1gFnSc2	korelace
pak	pak	k6eAd1	pak
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
korelační	korelační	k2eAgInSc1d1	korelační
koeficient	koeficient	k1gInSc1	koeficient
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
hodnot	hodnota	k1gFnPc2	hodnota
od	od	k7c2	od
-	-	kIx~	-
až	až	k9	až
po	po	k7c4	po
+1	+1	k4	+1
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
či	či	k8xC	či
veličinami	veličina	k1gFnPc7	veličina
x	x	k?	x
a	a	k8xC	a
y	y	k?	y
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
<g/>
)	)	kIx)	)
platí	platit	k5eAaImIp3nS	platit
y	y	k?	y
=	=	kIx~	=
kx	kx	k?	kx
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
záporný	záporný	k2eAgInSc1d1	záporný
(	(	kIx(	(
<g/>
y	y	k?	y
=	=	kIx~	=
-kx	x	k?	-kx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
korelačního	korelační	k2eAgInSc2d1	korelační
koeficientu	koeficient	k1gInSc2	koeficient
-	-	kIx~	-
značí	značit	k5eAaImIp3nS	značit
zcela	zcela	k6eAd1	zcela
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
závislost	závislost	k1gFnSc4	závislost
(	(	kIx(	(
<g/>
antikorelaci	antikorelace	k1gFnSc4	antikorelace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
zvětší	zvětšit	k5eAaPmIp3nP	zvětšit
hodnoty	hodnota	k1gFnPc1	hodnota
v	v	k7c6	v
první	první	k4xOgFnSc6	první
skupině	skupina	k1gFnSc6	skupina
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
zmenší	zmenšit	k5eAaPmIp3nP	zmenšit
hodnoty	hodnota	k1gFnPc1	hodnota
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
skupině	skupina	k1gFnSc6	skupina
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
uplynulým	uplynulý	k2eAgInSc7d1	uplynulý
a	a	k8xC	a
zbývajícím	zbývající	k2eAgInSc7d1	zbývající
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
korelačního	korelační	k2eAgInSc2d1	korelační
koeficientu	koeficient	k1gInSc2	koeficient
+1	+1	k4	+1
značí	značit	k5eAaImIp3nS	značit
zcela	zcela	k6eAd1	zcela
přímou	přímý	k2eAgFnSc4d1	přímá
závislost	závislost	k1gFnSc4	závislost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
rychlostí	rychlost	k1gFnSc7	rychlost
bicyklu	bicykl	k1gInSc2	bicykl
a	a	k8xC	a
frekvencí	frekvence	k1gFnPc2	frekvence
otáček	otáčka	k1gFnPc2	otáčka
kola	kolo	k1gNnSc2	kolo
bicyklu	bicykl	k1gInSc6	bicykl
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
korelační	korelační	k2eAgInSc1d1	korelační
koeficient	koeficient	k1gInSc1	koeficient
roven	roven	k2eAgInSc1d1	roven
0	[number]	k4	0
(	(	kIx(	(
<g/>
nekorelovanost	nekorelovanost	k1gFnSc1	nekorelovanost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
mezi	mezi	k7c7	mezi
znaky	znak	k1gInPc7	znak
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
statisticky	statisticky	k6eAd1	statisticky
zjistitelná	zjistitelný	k2eAgFnSc1d1	zjistitelná
lineární	lineární	k2eAgFnSc1d1	lineární
závislost	závislost	k1gFnSc1	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
při	při	k7c6	při
nulovém	nulový	k2eAgInSc6d1	nulový
korelačním	korelační	k2eAgInSc6d1	korelační
koeficientu	koeficient	k1gInSc6	koeficient
na	na	k7c6	na
sobě	se	k3xPyFc3	se
veličiny	veličina	k1gFnPc1	veličina
mohou	moct	k5eAaImIp3nP	moct
záviset	záviset	k5eAaImF	záviset
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
nelze	lze	k6eNd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
lineární	lineární	k2eAgFnSc7d1	lineární
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
přibližně	přibližně	k6eAd1	přibližně
<g/>
.	.	kIx.	.
</s>
<s>
Pearsonův	Pearsonův	k2eAgInSc1d1	Pearsonův
korelační	korelační	k2eAgInSc1d1	korelační
koeficient	koeficient	k1gInSc1	koeficient
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
druhé	druhý	k4xOgInPc1	druhý
momenty	moment	k1gInPc1	moment
náhodných	náhodný	k2eAgFnPc2d1	náhodná
veličin	veličina	k1gFnPc2	veličina
X	X	kA	X
a	a	k8xC	a
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
(	(	kIx(	(
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
E	E	kA	E
(	(	kIx(	(
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
konečné	konečná	k1gFnSc3	konečná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
kovarianci	kovarianec	k1gMnPc1	kovarianec
převedeme	převést	k5eAaPmIp1nP	převést
na	na	k7c4	na
bezrozměrné	bezrozměrný	k2eAgNnSc4d1	bezrozměrné
číslo	číslo	k1gNnSc4	číslo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
podělíme	podělit	k5eAaPmIp1nP	podělit
směrodatnými	směrodatný	k2eAgFnPc7d1	směrodatná
odchylkami	odchylka	k1gFnPc7	odchylka
obou	dva	k4xCgFnPc2	dva
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
X	X	kA	X
,	,	kIx,	,
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
o	o	k7c4	o
v	v	k7c6	v
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
,	,	kIx,	,
Y	Y	kA	Y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
(	(	kIx(	(
(	(	kIx(	(
X	X	kA	X
-	-	kIx~	-
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
(	(	kIx(	(
Y	Y	kA	Y
-	-	kIx~	-
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
cov	cov	k?	cov
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
Y	Y	kA	Y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
((	((	k?	((
<g/>
X-	X-	k1gMnSc3	X-
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Y-	Y-	k1gMnSc3	Y-
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
Y	Y	kA	Y
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
))	))	k?	))
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc1	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
Y	Y	kA	Y
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Protože	protože	k8xS	protože
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
E	E	kA	E
(	(	kIx(	(
X	X	kA	X
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
σ	σ	k?	σ
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
E	E	kA	E
(	(	kIx(	(
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
-	-	kIx~	-
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sigma	sigma	k1gNnSc3	sigma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-E	-E	k?	-E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
obdobně	obdobně	k6eAd1	obdobně
pro	pro	k7c4	pro
Y	Y	kA	Y
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc4d1	uvedený
vzorec	vzorec	k1gInSc4	vzorec
upravit	upravit	k5eAaPmF	upravit
do	do	k7c2	do
přehlednějšího	přehlední	k2eAgInSc2d2	přehlední
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
tvaru	tvar	k1gInSc2	tvar
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
:	:	kIx,	:
X	X	kA	X
,	,	kIx,	,
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
(	(	kIx(	(
X	X	kA	X
Y	Y	kA	Y
)	)	kIx)	)
-	-	kIx~	-
E	E	kA	E
(	(	kIx(	(
X	X	kA	X
)	)	kIx)	)
E	E	kA	E
(	(	kIx(	(
Y	Y	kA	Y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
(	(	kIx(	(
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
(	(	kIx(	(
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
Y	Y	kA	Y
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
XY	XY	kA	XY
<g/>
)	)	kIx)	)
<g/>
-E	-E	k?	-E
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gMnSc1	sqrt
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-E	-E	k?	-E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
~	~	kIx~	~
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-E	-E	k?	-E
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
}}}}}	}}}}}	k?	}}}}}
:	:	kIx,	:
Koeficient	koeficient	k1gInSc1	koeficient
korelace	korelace	k1gFnSc2	korelace
nabývá	nabývat	k5eAaImIp3nS	nabývat
hodnot	hodnota	k1gFnPc2	hodnota
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
-	-	kIx~	-
1	[number]	k4	1
,	,	kIx,	,
1	[number]	k4	1
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
-1,1	-1,1	k4	-1,1
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k1gNnSc2	rangle
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nezávislosti	nezávislost	k1gFnSc6	nezávislost
veličin	veličina	k1gFnPc2	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
koeficient	koeficient	k1gInSc1	koeficient
korelace	korelace	k1gFnSc2	korelace
roven	roven	k2eAgInSc1d1	roven
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Nulový	nulový	k2eAgInSc1d1	nulový
korelační	korelační	k2eAgInSc1d1	korelační
koeficient	koeficient	k1gInSc1	koeficient
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
veličiny	veličina	k1gFnPc4	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
}	}	kIx)	}
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
<s>
Nulový	nulový	k2eAgInSc1d1	nulový
korelační	korelační	k2eAgInSc1d1	korelační
koeficient	koeficient	k1gInSc1	koeficient
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
dvojice	dvojice	k1gFnSc1	dvojice
náhodných	náhodný	k2eAgFnPc2d1	náhodná
veličin	veličina	k1gFnPc2	veličina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Y	Y	kA	Y
=	=	kIx~	=
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Y	Y	kA	Y
<g/>
=	=	kIx~	=
<g/>
X	X	kA	X
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koeficient	koeficient	k1gInSc1	koeficient
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
odvodil	odvodit	k5eAaPmAgMnS	odvodit
anglický	anglický	k2eAgMnSc1d1	anglický
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
antropolog	antropolog	k1gMnSc1	antropolog
Sir	sir	k1gMnSc1	sir
Francis	Francis	k1gFnSc2	Francis
Galton	Galton	k1gInSc1	Galton
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
nicméně	nicméně	k8xC	nicméně
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
koeficienty	koeficient	k1gInPc4	koeficient
korelace	korelace	k1gFnSc2	korelace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Spearmanovo	Spearmanův	k2eAgNnSc4d1	Spearmanův
rhó	rhó	k?	rhó
či	či	k8xC	či
Kendallovo	Kendallův	k2eAgNnSc1d1	Kendallův
tau	tau	k1gNnSc1	tau
pro	pro	k7c4	pro
ordinální	ordinální	k2eAgNnPc4d1	ordinální
(	(	kIx(	(
<g/>
pořadová	pořadový	k2eAgNnPc4d1	pořadové
<g/>
)	)	kIx)	)
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
korelace	korelace	k1gFnSc1	korelace
(	(	kIx(	(
<g/>
zpracování	zpracování	k1gNnSc1	zpracování
signálu	signál	k1gInSc2	signál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
korelační	korelační	k2eAgFnSc4d1	korelační
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
spojité	spojitý	k2eAgInPc4d1	spojitý
signály	signál	k1gInPc4	signál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
⋆	⋆	k?	⋆
g	g	kA	g
)	)	kIx)	)
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
e	e	k0	e
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
∗	∗	k?	∗
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
τ	τ	k?	τ
)	)	kIx)	)
⋅	⋅	k?	⋅
g	g	kA	g
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
t	t	k?	t
+	+	kIx~	+
τ	τ	k?	τ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
star	star	k1gInSc1	star
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
stackrel	stackrel	k1gInSc1	stackrel
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
def	def	k?	def
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc1	tau
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
diskrétní	diskrétní	k2eAgInPc4d1	diskrétní
signály	signál	k1gInPc4	signál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
⋆	⋆	k?	⋆
g	g	kA	g
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
e	e	k0	e
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
-	-	kIx~	-
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
∗	∗	k?	∗
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
k	k	k7c3	k
+	+	kIx~	+
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
star	star	k1gInSc1	star
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
stackrel	stackrel	k1gInSc1	stackrel
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
def	def	k?	def
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
f_	f_	k?	f_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
g_	g_	k?	g_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
:	:	kIx,	:
U	u	k7c2	u
komplexních	komplexní	k2eAgInPc2d1	komplexní
signálů	signál	k1gInPc2	signál
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
∗	∗	k?	∗
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
*	*	kIx~	*
<g/>
}}	}}	k?	}}
představuje	představovat	k5eAaImIp3nS	představovat
komplexně	komplexně	k6eAd1	komplexně
sdružené	sdružený	k2eAgNnSc4d1	sdružené
číslo	číslo	k1gNnSc4	číslo
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
konvoluci	konvoluce	k1gFnSc3	konvoluce
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
časovém	časový	k2eAgNnSc6d1	časové
překlopení	překlopení	k1gNnSc6	překlopení
druhé	druhý	k4xOgFnSc2	druhý
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
g	g	kA	g
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
autokorelace	autokorelace	k1gFnSc1	autokorelace
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
korelace	korelace	k1gFnSc1	korelace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
⋆	⋆	k?	⋆
f	f	k?	f
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
star	star	k1gInSc1	star
f	f	k?	f
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tak	tak	k6eAd1	tak
určit	určit	k5eAaPmF	určit
tzv.	tzv.	kA	tzv.
soběpodobnost	soběpodobnost	k1gFnSc1	soběpodobnost
signálu	signál	k1gInSc2	signál
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
např.	např.	kA	např.
signál	signál	k1gInSc4	signál
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
periodách	perioda	k1gFnPc6	perioda
neopakuje	opakovat	k5eNaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1	charakteristika
náhodné	náhodný	k2eAgFnSc2d1	náhodná
veličiny	veličina	k1gFnSc2	veličina
Korelace	korelace	k1gFnSc2	korelace
neimplikuje	implikovat	k5eNaImIp3nS	implikovat
kauzalitu	kauzalita	k1gFnSc4	kauzalita
Spearmanův	Spearmanův	k2eAgInSc4d1	Spearmanův
koeficient	koeficient	k1gInSc4	koeficient
pořadové	pořadový	k2eAgFnSc2d1	pořadová
korelace	korelace	k1gFnSc2	korelace
</s>
