<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Adrienne	Adrienn	k1gInSc5	Adrienn
Švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Blekingu	Bleking	k1gInSc2	Bleking
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Adrienne	Adrienn	k1gInSc5	Adrienn
Josephine	Josephin	k1gInSc5	Josephin
Alice	Alice	k1gFnSc1	Alice
Bernadotte	Bernadott	k1gInSc5	Bernadott
<g/>
,	,	kIx,	,
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
princezny	princezna	k1gFnSc2	princezna
Madeleine	Madeleine	k1gFnSc2	Madeleine
Švédské	švédský	k2eAgFnSc2d1	švédská
a	a	k8xC	a
Christophera	Christophero	k1gNnSc2	Christophero
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neilla	Neillo	k1gNnPc4	Neillo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vnučkou	vnučka	k1gFnSc7	vnučka
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Gustava	Gustav	k1gMnSc2	Gustav
a	a	k8xC	a
královny	královna	k1gFnSc2	královna
Silvie	Silvia	k1gFnPc4	Silvia
Švédské	švédský	k2eAgFnPc4d1	švédská
<g/>
.	.	kIx.	.
</s>
<s>
Adrienne	Adriennout	k5eAaImIp3nS	Adriennout
je	on	k3xPp3gMnPc4	on
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
desátou	desátá	k1gFnSc7	desátá
následnicí	následnice	k1gFnSc7	následnice
švédského	švédský	k2eAgInSc2d1	švédský
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Narození	narození	k1gNnSc2	narození
==	==	k?	==
</s>
</p>
<p>
<s>
Princezna	princezna	k1gFnSc1	princezna
Adrienne	Adrienn	k1gInSc5	Adrienn
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Danderydu	Danderyd	k1gInSc6	Danderyd
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
narození	narození	k1gNnSc1	narození
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
jednadvaceti	jednadvacet	k4xCc7	jednadvacet
oslavnými	oslavný	k2eAgInPc7d1	oslavný
výstřely	výstřel	k1gInPc7	výstřel
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
a	a	k8xC	a
následně	následně	k6eAd1	následně
pak	pak	k6eAd1	pak
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
,	,	kIx,	,
Härnösandu	Härnösand	k1gInSc6	Härnösand
<g/>
,	,	kIx,	,
Karlskroně	Karlskron	k1gInSc6	Karlskron
a	a	k8xC	a
Bodenu	Boden	k1gInSc6	Boden
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgNnP	být
jejím	její	k3xOp3gMnSc7	její
dědečkem	dědeček	k1gMnSc7	dědeček
<g/>
,	,	kIx,	,
králem	král	k1gMnSc7	král
Karlem	Karel	k1gMnSc7	Karel
Gustavem	Gustav	k1gMnSc7	Gustav
<g/>
,	,	kIx,	,
přidělena	přidělen	k2eAgNnPc1d1	přiděleno
jména	jméno	k1gNnPc1	jméno
a	a	k8xC	a
tituly	titul	k1gInPc1	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Adrianne	Adriannout	k5eAaPmIp3nS	Adriannout
byla	být	k5eAaImAgFnS	být
pokřtěna	pokřtěn	k2eAgFnSc1d1	pokřtěna
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
Drottningholmského	Drottningholmský	k2eAgInSc2d1	Drottningholmský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
přesně	přesně	k6eAd1	přesně
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
po	po	k7c4	po
uzavření	uzavření	k1gNnSc4	uzavření
sňatku	sňatek	k1gInSc2	sňatek
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc1	čtyři
roky	rok	k1gInPc1	rok
od	od	k7c2	od
křtu	křest	k1gInSc2	křest
její	její	k3xOp3gFnSc2	její
starší	starý	k2eAgFnSc2d2	starší
sestry	sestra	k1gFnSc2	sestra
Leonore	Leonor	k1gMnSc5	Leonor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tituly	titul	k1gInPc1	titul
a	a	k8xC	a
styly	styl	k1gInPc1	styl
==	==	k?	==
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
<g/>
:	:	kIx,	:
Její	její	k3xOp3gFnSc1	její
královská	královský	k2eAgFnSc1d1	královská
výsost	výsost	k1gFnSc1	výsost
princezna	princezna	k1gFnSc1	princezna
Adrienne	Adrienn	k1gInSc5	Adrienn
Švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Blekingu	Bleking	k1gInSc2	Bleking
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Princess	Princessa	k1gFnPc2	Princessa
Adrienne	Adrienn	k1gInSc5	Adrienn
<g/>
,	,	kIx,	,
Duchess	Duchess	k1gInSc1	Duchess
of	of	k?	of
Blekinge	Bleking	k1gFnSc2	Bleking
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
