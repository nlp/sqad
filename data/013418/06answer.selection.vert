<s>
Princezna	princezna	k1gFnSc1	princezna
Adrienne	Adrienn	k1gInSc5	Adrienn
Švédská	švédský	k2eAgFnSc1d1	švédská
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
z	z	k7c2	z
Blekingu	Bleking	k1gInSc2	Bleking
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Adrienne	Adrienn	k1gInSc5	Adrienn
Josephine	Josephin	k1gInSc5	Josephin
Alice	Alice	k1gFnSc1	Alice
Bernadotte	Bernadott	k1gInSc5	Bernadott
<g/>
,	,	kIx,	,
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
dcera	dcera	k1gFnSc1	dcera
princezny	princezna	k1gFnSc2	princezna
Madeleine	Madeleine	k1gFnSc2	Madeleine
Švédské	švédský	k2eAgFnSc2d1	švédská
a	a	k8xC	a
Christophera	Christophero	k1gNnSc2	Christophero
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neilla	Neillo	k1gNnPc4	Neillo
<g/>
.	.	kIx.	.
</s>
