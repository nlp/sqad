<s>
Hry	hra	k1gFnPc1	hra
XXVII	XXVII	kA	XXVII
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
2000	[number]	k4	2000
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
australském	australský	k2eAgNnSc6d1	Australské
Sydney	Sydney	k1gNnSc6	Sydney
v	v	k7c6	v
září	září	k1gNnSc6	září
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
se	se	k3xPyFc4	se
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
konaly	konat	k5eAaImAgFnP	konat
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
44	[number]	k4	44
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
199	[number]	k4	199
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
chyběl	chybět	k5eAaImAgInS	chybět
pouze	pouze	k6eAd1	pouze
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
výpravy	výprava	k1gFnPc1	výprava
Eritreje	Eritrea	k1gFnSc2	Eritrea
<g/>
,	,	kIx,	,
Mikronésie	Mikronésie	k1gFnSc2	Mikronésie
a	a	k8xC	a
Palau	Palaus	k1gInSc2	Palaus
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
pod	pod	k7c7	pod
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
vlajkou	vlajka	k1gFnSc7	vlajka
závodili	závodit	k5eAaImAgMnP	závodit
také	také	k9	také
samostatně	samostatně	k6eAd1	samostatně
čtyři	čtyři	k4xCgMnPc1	čtyři
sportovci	sportovec	k1gMnPc1	sportovec
z	z	k7c2	z
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
o	o	k7c4	o
300	[number]	k4	300
sad	sada	k1gFnPc2	sada
medailí	medaile	k1gFnPc2	medaile
ve	v	k7c6	v
28	[number]	k4	28
sportovních	sportovní	k2eAgNnPc6d1	sportovní
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
126	[number]	k4	126
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
osm	osm	k4xCc4	osm
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
zlaté	zlatá	k1gFnPc1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
uspořádání	uspořádání	k1gNnSc6	uspořádání
her	hra	k1gFnPc2	hra
podalo	podat	k5eAaPmAgNnS	podat
oficiální	oficiální	k2eAgFnSc4d1	oficiální
kandidaturu	kandidatura	k1gFnSc4	kandidatura
šest	šest	k4xCc4	šest
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Brasília	Brasília	k1gFnSc1	Brasília
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Brazílie	Brazílie	k1gFnSc1	Brazílie
ji	on	k3xPp3gFnSc4	on
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
závěrečným	závěrečný	k2eAgNnSc7d1	závěrečné
hlasováním	hlasování	k1gNnSc7	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
tak	tak	k6eAd1	tak
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1993	[number]	k4	1993
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Carlu	Carl	k1gMnSc3	Carl
volil	volit	k5eAaImAgInS	volit
mezi	mezi	k7c7	mezi
pěti	pět	k4xCc7	pět
kandidáty	kandidát	k1gMnPc7	kandidát
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
rozdílem	rozdíl	k1gInSc7	rozdíl
dvou	dva	k4xCgInPc2	dva
hlasů	hlas	k1gInPc2	hlas
upřednostnil	upřednostnit	k5eAaPmAgInS	upřednostnit
Sydney	Sydney	k1gNnSc4	Sydney
před	před	k7c7	před
Pekingem	Peking	k1gInSc7	Peking
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ten	ten	k3xDgInSc4	ten
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
kolech	kolo	k1gNnPc6	kolo
dostával	dostávat	k5eAaImAgMnS	dostávat
největší	veliký	k2eAgFnSc4d3	veliký
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Člen	člen	k1gMnSc1	člen
MOV	MOV	kA	MOV
ze	z	k7c2	z
Svazijska	Svazijsko	k1gNnSc2	Svazijsko
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
nehlasoval	hlasovat	k5eNaImAgMnS	hlasovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
označili	označit	k5eAaPmAgMnP	označit
vítězství	vítězství	k1gNnSc4	vítězství
Sydney	Sydney	k1gNnSc2	Sydney
nad	nad	k7c7	nad
Pekingem	Peking	k1gInSc7	Peking
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
podpoře	podpora	k1gFnSc3	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
za	za	k7c4	za
překvapení	překvapení	k1gNnSc4	překvapení
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
neúspěch	neúspěch	k1gInSc4	neúspěch
čínské	čínský	k2eAgFnSc2d1	čínská
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Peking	Peking	k1gInSc1	Peking
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
vynahradil	vynahradit	k5eAaPmAgMnS	vynahradit
o	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
ziskem	zisk	k1gInSc7	zisk
pořadatelství	pořadatelství	k1gNnSc2	pořadatelství
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
chtěla	chtít	k5eAaImAgFnS	chtít
podat	podat	k5eAaPmF	podat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Melbourne	Melbourne	k1gNnSc3	Melbourne
na	na	k7c4	na
pořádání	pořádání	k1gNnPc4	pořádání
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
podpory	podpora	k1gFnSc2	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
vláda	vláda	k1gFnSc1	vláda
státu	stát	k1gInSc2	stát
Victoria	Victorium	k1gNnSc2	Victorium
kandidaturu	kandidatura	k1gFnSc4	kandidatura
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
neuspěly	uspět	k5eNaPmAgInP	uspět
pokusy	pokus	k1gInPc1	pokus
Brisbane	Brisban	k1gMnSc5	Brisban
ucházet	ucházet	k5eAaImF	ucházet
se	se	k3xPyFc4	se
o	o	k7c4	o
hry	hra	k1gFnPc4	hra
1992	[number]	k4	1992
a	a	k8xC	a
Melbourne	Melbourne	k1gNnSc4	Melbourne
o	o	k7c4	o
hry	hra	k1gFnPc4	hra
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
úspěch	úspěch	k1gInSc1	úspěch
kandidatury	kandidatura	k1gFnSc2	kandidatura
Sydney	Sydney	k1gNnSc2	Sydney
byl	být	k5eAaImAgInS	být
zpochybněn	zpochybnit	k5eAaPmNgInS	zpochybnit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
vyšetřovací	vyšetřovací	k2eAgFnSc1d1	vyšetřovací
komise	komise	k1gFnSc1	komise
sice	sice	k8xC	sice
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc4	některý
dárky	dárek	k1gInPc4	dárek
pro	pro	k7c4	pro
zástupce	zástupce	k1gMnPc4	zástupce
MOV	MOV	kA	MOV
byly	být	k5eAaImAgInP	být
dražší	drahý	k2eAgInPc1d2	dražší
<g/>
,	,	kIx,	,
než	než	k8xS	než
povolují	povolovat	k5eAaImIp3nP	povolovat
interní	interní	k2eAgNnPc4d1	interní
pravidla	pravidlo	k1gNnPc4	pravidlo
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
ke	k	k7c3	k
korupci	korupce	k1gFnSc3	korupce
nebo	nebo	k8xC	nebo
uplácení	uplácení	k1gNnSc1	uplácení
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Australský	australský	k2eAgInSc1d1	australský
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
oficiálně	oficiálně	k6eAd1	oficiálně
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sydney	Sydney	k1gNnSc4	Sydney
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
pořádání	pořádání	k1gNnSc4	pořádání
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
kandidátského	kandidátský	k2eAgInSc2d1	kandidátský
výboru	výbor	k1gInSc2	výbor
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
Nick	Nick	k1gMnSc1	Nick
Greiner	Greiner	k1gMnSc1	Greiner
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
státu	stát	k1gInSc2	stát
Nový	nový	k2eAgInSc4d1	nový
Jižní	jižní	k2eAgInSc4d1	jižní
Wales	Wales	k1gInSc4	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
kandidaturu	kandidatura	k1gFnSc4	kandidatura
spravovala	spravovat	k5eAaImAgFnS	spravovat
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
ručením	ručení	k1gNnSc7	ručení
omezeným	omezený	k2eAgNnSc7d1	omezené
Sydney	Sydney	k1gNnSc7	Sydney
Olympics	Olympicsa	k1gFnPc2	Olympicsa
Bid	Bid	k1gMnSc2	Bid
Limited	limited	k2eAgFnSc2d1	limited
(	(	kIx(	(
<g/>
SOBL	SOBL	kA	SOBL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
-	-	kIx~	-
tj.	tj.	kA	tj.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
volby	volba	k1gFnSc2	volba
pořadatele	pořadatel	k1gMnSc2	pořadatel
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
premiér	premiér	k1gMnSc1	premiér
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
John	John	k1gMnSc1	John
Fahey	Fahea	k1gFnSc2	Fahea
<g/>
,	,	kIx,	,
výkonným	výkonný	k2eAgMnSc7d1	výkonný
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
42	[number]	k4	42
<g/>
členný	členný	k2eAgInSc1d1	členný
tým	tým	k1gInSc1	tým
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
právník	právník	k1gMnSc1	právník
Rod	rod	k1gInSc4	rod
McGeoch	McGeoch	k1gInSc1	McGeoch
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
pro	pro	k7c4	pro
podání	podání	k1gNnSc4	podání
kandidatury	kandidatura	k1gFnSc2	kandidatura
byl	být	k5eAaImAgMnS	být
25,2	[number]	k4	25,2
milionu	milion	k4xCgInSc2	milion
australských	australský	k2eAgMnPc2d1	australský
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
kandidatury	kandidatura	k1gFnSc2	kandidatura
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
tvaru	tvar	k1gInSc2	tvar
budovy	budova	k1gFnSc2	budova
Opery	opera	k1gFnSc2	opera
v	v	k7c4	v
Sydney	Sydney	k1gNnSc4	Sydney
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
olympijských	olympijský	k2eAgInPc2d1	olympijský
kruhů	kruh	k1gInPc2	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přidělení	přidělení	k1gNnSc6	přidělení
pořadatelství	pořadatelství	k1gNnSc2	pořadatelství
přijal	přijmout	k5eAaPmAgInS	přijmout
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
parlament	parlament	k1gInSc1	parlament
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
Sydney	Sydney	k1gNnSc2	Sydney
Organising	Organising	k1gInSc4	Organising
Committee	Committee	k1gNnSc2	Committee
for	forum	k1gNnPc2	forum
the	the	k?	the
Olympic	Olympic	k1gMnSc1	Olympic
Games	Games	k1gMnSc1	Games
Act	Act	k1gMnSc1	Act
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
Sydneyském	sydneyský	k2eAgInSc6d1	sydneyský
organizačním	organizační	k2eAgInSc6d1	organizační
výboru	výbor	k1gInSc6	výbor
pro	pro	k7c4	pro
Olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
organizační	organizační	k2eAgInSc4d1	organizační
výbor	výbor	k1gInSc4	výbor
SOCOG	SOCOG	kA	SOCOG
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgInS	stanout
Gary	Gara	k1gFnSc2	Gara
Pemberton	Pemberton	k1gInSc1	Pemberton
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
vládě	vláda	k1gFnSc6	vláda
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
post	post	k1gInSc1	post
ministra	ministr	k1gMnSc2	ministr
pro	pro	k7c4	pro
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1996	[number]	k4	1996
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
pro	pro	k7c4	pro
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
Michael	Michael	k1gMnSc1	Michael
Knight	Knight	k1gMnSc1	Knight
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Pembertona	Pemberton	k1gMnSc4	Pemberton
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
SOCOG	SOCOG	kA	SOCOG
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
až	až	k9	až
do	do	k7c2	do
skončení	skončení	k1gNnSc2	skončení
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
speciální	speciální	k2eAgFnPc1d1	speciální
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
např.	např.	kA	např.
dopravu	doprava	k1gFnSc4	doprava
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
či	či	k8xC	či
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
Letní	letní	k2eAgFnPc4d1	letní
paralympijské	paralympijský	k2eAgFnPc4d1	paralympijská
hry	hra	k1gFnPc4	hra
2000	[number]	k4	2000
řídil	řídit	k5eAaImAgInS	řídit
SPOC	SPOC	kA	SPOC
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Johnem	John	k1gMnSc7	John
Grantem	grant	k1gInSc7	grant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
organizaci	organizace	k1gFnSc6	organizace
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
47	[number]	k4	47
tisíc	tisíc	k4xCgInPc2	tisíc
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
zájemců	zájemce	k1gMnPc2	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
úřadu	úřad	k1gInSc2	úřad
generálního	generální	k2eAgMnSc2d1	generální
auditora	auditor	k1gMnSc2	auditor
Nového	Nového	k2eAgInSc2d1	Nového
Jižního	jižní	k2eAgInSc2d1	jižní
Walesu	Wales	k1gInSc2	Wales
stály	stát	k5eAaImAgFnP	stát
olympijské	olympijský	k2eAgFnPc1d1	olympijská
a	a	k8xC	a
následné	následný	k2eAgFnPc1d1	následná
paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
celkem	celkem	k6eAd1	celkem
6,5	[number]	k4	6,5
miliardy	miliarda	k4xCgFnSc2	miliarda
australských	australský	k2eAgMnPc2d1	australský
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Organizační	organizační	k2eAgInSc1d1	organizační
výbor	výbor	k1gInSc1	výbor
her	hra	k1gFnPc2	hra
SOCOG	SOCOG	kA	SOCOG
a	a	k8xC	a
výbor	výbor	k1gInSc1	výbor
paralympijských	paralympijský	k2eAgFnPc2d1	paralympijská
her	hra	k1gFnPc2	hra
SPOC	SPOC	kA	SPOC
získaly	získat	k5eAaPmAgFnP	získat
ze	z	k7c2	z
sponzoringu	sponzoring	k1gInSc2	sponzoring
<g/>
,	,	kIx,	,
prodeje	prodej	k1gInSc2	prodej
vstupenek	vstupenka	k1gFnPc2	vstupenka
a	a	k8xC	a
televizních	televizní	k2eAgNnPc2d1	televizní
práv	právo	k1gNnPc2	právo
tři	tři	k4xCgFnPc4	tři
miliardy	miliarda	k4xCgFnPc4	miliarda
dolarů	dolar	k1gInPc2	dolar
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgInPc1d1	čistý
vládní	vládní	k2eAgInPc1d1	vládní
náklady	náklad	k1gInPc1	náklad
(	(	kIx(	(
<g/>
vlády	vláda	k1gFnSc2	vláda
státu	stát	k1gInSc2	stát
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
i	i	k8xC	i
vlády	vláda	k1gFnSc2	vláda
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
činily	činit	k5eAaImAgInP	činit
1,5	[number]	k4	1,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
s	s	k7c7	s
odečtením	odečtení	k1gNnSc7	odečtení
souvisejících	související	k2eAgInPc2d1	související
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sportoviště	sportoviště	k1gNnSc2	sportoviště
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
úkolem	úkol	k1gInSc7	úkol
pořadatelů	pořadatel	k1gMnPc2	pořadatel
bylo	být	k5eAaImAgNnS	být
vybudovat	vybudovat	k5eAaPmF	vybudovat
potřebná	potřebný	k2eAgNnPc4d1	potřebné
sportoviště	sportoviště	k1gNnPc4	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
postavit	postavit	k5eAaPmF	postavit
15	[number]	k4	15
zcela	zcela	k6eAd1	zcela
nových	nový	k2eAgNnPc2d1	nové
sportovišť	sportoviště	k1gNnPc2	sportoviště
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
byla	být	k5eAaImAgNnP	být
dokončena	dokončit	k5eAaPmNgNnP	dokončit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
dostavěny	dostavět	k5eAaPmNgInP	dostavět
pouze	pouze	k6eAd1	pouze
dějiště	dějiště	k1gNnSc4	dějiště
ženského	ženský	k2eAgInSc2d1	ženský
turnaje	turnaj	k1gInSc2	turnaj
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
pólu	pólo	k1gNnSc6	pólo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
programu	program	k1gInSc2	program
her	hra	k1gFnPc2	hra
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
dodatečně	dodatečně	k6eAd1	dodatečně
<g/>
,	,	kIx,	,
a	a	k8xC	a
beachvolejbalový	beachvolejbalový	k2eAgInSc1d1	beachvolejbalový
stadion	stadion	k1gInSc1	stadion
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
Bondi	Bond	k1gMnPc1	Bond
Beach	Beach	k1gInSc4	Beach
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
her	hra	k1gFnPc2	hra
opět	opět	k6eAd1	opět
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
olympijských	olympijský	k2eAgNnPc2d1	Olympijské
sportovišť	sportoviště	k1gNnPc2	sportoviště
činil	činit	k5eAaImAgInS	činit
3,3	[number]	k4	3,3
miliardy	miliarda	k4xCgFnSc2	miliarda
australských	australský	k2eAgMnPc2d1	australský
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
pětin	pětina	k1gFnPc2	pětina
byl	být	k5eAaImAgInS	být
hrazen	hradit	k5eAaImNgInS	hradit
z	z	k7c2	z
vládních	vládní	k2eAgInPc2d1	vládní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
soukromé	soukromý	k2eAgFnPc1d1	soukromá
společnosti	společnost	k1gFnPc1	společnost
financovaly	financovat	k5eAaBmAgFnP	financovat
hlavně	hlavně	k6eAd1	hlavně
výstavbu	výstavba	k1gFnSc4	výstavba
Olympijského	olympijský	k2eAgInSc2d1	olympijský
stadionu	stadion	k1gInSc2	stadion
Australia	Australium	k1gNnSc2	Australium
<g/>
,	,	kIx,	,
Sydney	Sydney	k1gNnSc2	Sydney
Superdomu	Superdom	k1gInSc2	Superdom
a	a	k8xC	a
olympijské	olympijský	k2eAgFnSc2d1	olympijská
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
stálých	stálý	k2eAgNnPc2d1	stálé
sportovišť	sportoviště	k1gNnPc2	sportoviště
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
jejich	jejich	k3xOp3gFnSc2	jejich
divácké	divácký	k2eAgFnSc2d1	divácká
kapacity	kapacita	k1gFnSc2	kapacita
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
her	hra	k1gFnPc2	hra
odebrána	odebrat	k5eAaPmNgFnS	odebrat
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
na	na	k7c6	na
třiceti	třicet	k4xCc6	třicet
sportovištích	sportoviště	k1gNnPc6	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Olympijském	olympijský	k2eAgInSc6d1	olympijský
parku	park	k1gInSc6	park
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
40	[number]	k4	40
ha	ha	kA	ha
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Homebush	Homebush	k1gMnSc1	Homebush
Bay	Bay	k1gMnSc1	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
komplexu	komplex	k1gInSc2	komplex
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
68,5	[number]	k4	68,5
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vesnice	vesnice	k1gFnSc1	vesnice
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
tiskové	tiskový	k2eAgNnSc1d1	tiskové
středisko	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
sportoviště	sportoviště	k1gNnPc1	sportoviště
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgNnP	rozkládat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
mužského	mužský	k2eAgMnSc2d1	mužský
a	a	k8xC	a
ženského	ženský	k2eAgInSc2d1	ženský
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
také	také	k9	také
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
<g/>
,	,	kIx,	,
Adelaide	Adelaid	k1gMnSc5	Adelaid
<g/>
,	,	kIx,	,
Brisbane	Brisban	k1gMnSc5	Brisban
a	a	k8xC	a
Canbeře	Canbera	k1gFnSc6	Canbera
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
částečně	částečně	k6eAd1	částečně
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
loga	logo	k1gNnSc2	logo
kandidatury	kandidatura	k1gFnSc2	kandidatura
<g/>
.	.	kIx.	.
</s>
<s>
Představovalo	představovat	k5eAaImAgNnS	představovat
sportovce	sportovec	k1gMnSc4	sportovec
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
označovaného	označovaný	k2eAgInSc2d1	označovaný
Millenium	Millenium	k1gNnSc1	Millenium
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
Muž	muž	k1gMnSc1	muž
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
logu	log	k1gInSc6	log
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
motivy	motiv	k1gInPc1	motiv
bumerangu	bumerang	k1gInSc2	bumerang
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
skal	skála	k1gFnPc2	skála
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
typické	typický	k2eAgFnSc2d1	typická
barevnosti	barevnost	k1gFnSc2	barevnost
australského	australský	k2eAgInSc2d1	australský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Silueta	silueta	k1gFnSc1	silueta
Opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
loga	logo	k1gNnSc2	logo
kandidatury	kandidatura	k1gFnSc2	kandidatura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
připomenuta	připomenout	k5eAaPmNgFnS	připomenout
v	v	k7c6	v
modrém	modrý	k2eAgInSc6d1	modrý
tvaru	tvar	k1gInSc6	tvar
stopy	stopa	k1gFnSc2	stopa
kouře	kouř	k1gInSc2	kouř
olympijské	olympijský	k2eAgFnPc4d1	olympijská
pochodně	pochodeň	k1gFnPc4	pochodeň
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
měly	mít	k5eAaImAgInP	mít
tři	tři	k4xCgInPc1	tři
maskoty	maskot	k1gInPc1	maskot
<g/>
:	:	kIx,	:
Olly	Olly	k1gInPc1	Olly
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Ollie	Ollie	k1gFnSc1	Ollie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ledňák	ledňák	k1gMnSc1	ledňák
<g/>
,	,	kIx,	,
Syd	Syd	k1gMnSc1	Syd
byl	být	k5eAaImAgMnS	být
ptakopysk	ptakopysk	k1gMnSc1	ptakopysk
a	a	k8xC	a
Millie	Millie	k1gFnSc1	Millie
(	(	kIx(	(
<g/>
jméno	jméno	k1gNnSc1	jméno
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
millenium	millenium	k1gNnSc1	millenium
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
)	)	kIx)	)
ježura	ježura	k1gFnSc1	ježura
<g/>
.	.	kIx.	.
</s>
<s>
Zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
tři	tři	k4xCgInPc4	tři
živly	živel	k1gInPc4	živel
(	(	kIx(	(
<g/>
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
Matta	Matt	k1gMnSc2	Matt
Hattona	Hatton	k1gMnSc2	Hatton
a	a	k8xC	a
Jozefa	Jozef	k1gMnSc2	Jozef
Szekerese	Szekerese	k1gFnSc2	Szekerese
i	i	k8xC	i
další	další	k2eAgFnPc4d1	další
předepsané	předepsaný	k2eAgFnPc4d1	předepsaná
charakterové	charakterový	k2eAgFnPc4d1	charakterová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Upomínkové	upomínkový	k2eAgInPc1d1	upomínkový
předměty	předmět	k1gInPc1	předmět
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
maskotů	maskot	k1gInPc2	maskot
byly	být	k5eAaImAgInP	být
nejlépe	dobře	k6eAd3	dobře
prodávaným	prodávaný	k2eAgNnSc7d1	prodávané
olympijským	olympijský	k2eAgNnSc7d1	Olympijské
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
jako	jako	k8xS	jako
plyšové	plyšový	k2eAgFnPc1d1	plyšová
hračky	hračka	k1gFnPc1	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Hry	hra	k1gFnPc1	hra
konaly	konat	k5eAaImAgFnP	konat
na	na	k7c6	na
Jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
start	start	k1gInSc1	start
odložen	odložit	k5eAaPmNgInS	odložit
na	na	k7c4	na
nezvykle	zvykle	k6eNd1	zvykle
pozdní	pozdní	k2eAgInSc4d1	pozdní
termín	termín	k1gInSc4	termín
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
začaly	začít	k5eAaPmAgFnP	začít
zápasy	zápas	k1gInPc4	zápas
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
kopané	kopaná	k1gFnSc6	kopaná
<g/>
.	.	kIx.	.
</s>
<s>
Soutěže	soutěž	k1gFnPc1	soutěž
probíhaly	probíhat	k5eAaImAgFnP	probíhat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
obvyklých	obvyklý	k2eAgInPc6d1	obvyklý
časech	čas	k1gInPc6	čas
s	s	k7c7	s
odpoledními	odpolední	k2eAgNnPc7d1	odpolední
a	a	k8xC	a
večerními	večerní	k2eAgNnPc7d1	večerní
finále	finále	k1gNnPc7	finále
v	v	k7c6	v
plavání	plavání	k1gNnSc6	plavání
<g/>
,	,	kIx,	,
atletice	atletika	k1gFnSc6	atletika
či	či	k8xC	či
gymnastice	gymnastika	k1gFnSc6	gymnastika
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
soutěží	soutěž	k1gFnPc2	soutěž
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
disciplín	disciplína	k1gFnPc2	disciplína
najdete	najít	k5eAaPmIp2nP	najít
pod	pod	k7c4	pod
odkazy	odkaz	k1gInPc4	odkaz
<g/>
:	:	kIx,	:
Oproti	oproti	k7c3	oproti
olympijským	olympijský	k2eAgFnPc3d1	olympijská
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
si	se	k3xPyFc3	se
sportovci	sportovec	k1gMnPc1	sportovec
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
o	o	k7c4	o
29	[number]	k4	29
medailových	medailový	k2eAgFnPc2d1	medailová
kolekcí	kolekce	k1gFnPc2	kolekce
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
neustálé	neustálý	k2eAgNnSc1d1	neustálé
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
počtu	počet	k1gInSc2	počet
ženských	ženský	k2eAgFnPc2d1	ženská
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
pěti	pět	k4xCc2	pět
olympijskými	olympijský	k2eAgFnPc7d1	olympijská
kruhy	kruh	k1gInPc1	kruh
představily	představit	k5eAaPmAgFnP	představit
vzpěračky	vzpěračka	k1gFnPc1	vzpěračka
či	či	k8xC	či
moderní	moderní	k2eAgFnPc1d1	moderní
pětibojařky	pětibojařka	k1gFnPc1	pětibojařka
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
počtu	počet	k1gInSc2	počet
disciplín	disciplína	k1gFnPc2	disciplína
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
ženy	žena	k1gFnPc1	žena
také	také	k9	také
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
a	a	k8xC	a
ve	v	k7c6	v
sportovní	sportovní	k2eAgFnSc6d1	sportovní
střelbě	střelba	k1gFnSc6	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
olympijské	olympijský	k2eAgFnSc2d1	olympijská
rodiny	rodina	k1gFnSc2	rodina
přibyly	přibýt	k5eAaPmAgInP	přibýt
také	také	k6eAd1	také
tři	tři	k4xCgInPc4	tři
nové	nový	k2eAgInPc4d1	nový
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
premiéry	premiéra	k1gFnPc4	premiéra
si	se	k3xPyFc3	se
užili	užít	k5eAaPmAgMnP	užít
triatlonisté	triatlonista	k1gMnPc1	triatlonista
<g/>
,	,	kIx,	,
taekwondisté	taekwondista	k1gMnPc1	taekwondista
a	a	k8xC	a
trampolinisté	trampolinista	k1gMnPc1	trampolinista
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgInSc1d1	úvodní
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
začal	začít	k5eAaPmAgInS	začít
na	na	k7c6	na
Olympijském	olympijský	k2eAgInSc6d1	olympijský
stadionu	stadion	k1gInSc6	stadion
Australia	Australium	k1gNnSc2	Australium
v	v	k7c4	v
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
před	před	k7c7	před
108	[number]	k4	108
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
začala	začít	k5eAaPmAgFnS	začít
vystoupením	vystoupení	k1gNnSc7	vystoupení
120	[number]	k4	120
honáků	honák	k1gMnPc2	honák
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
zvaných	zvaný	k2eAgFnPc2d1	zvaná
stockmen	stockmen	k1gInSc4	stockmen
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
představovala	představovat	k5eAaImAgFnS	představovat
historie	historie	k1gFnSc1	historie
i	i	k8xC	i
přírodní	přírodní	k2eAgNnSc1d1	přírodní
bohatství	bohatství	k1gNnSc1	bohatství
australského	australský	k2eAgInSc2d1	australský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
hluboký	hluboký	k2eAgInSc4d1	hluboký
oceán	oceán	k1gInSc4	oceán
<g/>
,	,	kIx,	,
poušť	poušť	k1gFnSc4	poušť
s	s	k7c7	s
představiteli	představitel	k1gMnPc7	představitel
původního	původní	k2eAgNnSc2d1	původní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Austrálie	Austrálie	k1gFnSc1	Austrálie
-	-	kIx~	-
Austrálců	Austrálec	k1gMnPc2	Austrálec
<g/>
,	,	kIx,	,
přehlídku	přehlídka	k1gFnSc4	přehlídka
místní	místní	k2eAgFnSc2d1	místní
flóry	flóra	k1gFnSc2	flóra
i	i	k8xC	i
osídlení	osídlení	k1gNnSc1	osídlení
evropskými	evropský	k2eAgMnPc7d1	evropský
přistěhovalci	přistěhovalec	k1gMnPc7	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
defilé	defilé	k1gNnSc1	defilé
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
výprav	výprava	k1gFnPc2	výprava
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
vystoupení	vystoupení	k1gNnSc4	vystoupení
dvoutisícičlenného	dvoutisícičlenný	k2eAgInSc2d1	dvoutisícičlenný
olympijského	olympijský	k2eAgInSc2d1	olympijský
bandu	band	k1gInSc2	band
<g/>
,	,	kIx,	,
hrajícího	hrající	k2eAgMnSc2d1	hrající
populární	populární	k2eAgInSc4d1	populární
píseň	píseň	k1gFnSc4	píseň
Waltzing	Waltzing	k1gInSc1	Waltzing
Matilda	Matildo	k1gNnSc2	Matildo
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
zástupci	zástupce	k1gMnPc1	zástupce
199	[number]	k4	199
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
čtyři	čtyři	k4xCgMnPc1	čtyři
sportovci	sportovec	k1gMnPc1	sportovec
z	z	k7c2	z
Východního	východní	k2eAgInSc2d1	východní
Timoru	Timor	k1gInSc2	Timor
pod	pod	k7c7	pod
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
nastupovali	nastupovat	k5eAaImAgMnP	nastupovat
společně	společně	k6eAd1	společně
sportovci	sportovec	k1gMnPc1	sportovec
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
pod	pod	k7c7	pod
jedinou	jediný	k2eAgFnSc7d1	jediná
vlajkou	vlajka	k1gFnSc7	vlajka
sjednocení	sjednocení	k1gNnSc2	sjednocení
-	-	kIx~	-
modrou	modrý	k2eAgFnSc7d1	modrá
siluetou	silueta	k1gFnSc7	silueta
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
na	na	k7c6	na
bílém	bílý	k2eAgNnSc6d1	bílé
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Sportovce	sportovec	k1gMnSc4	sportovec
KLDR	KLDR	kA	KLDR
odlišovaly	odlišovat	k5eAaImAgInP	odlišovat
pouze	pouze	k6eAd1	pouze
odznáčky	odznáček	k1gInPc1	odznáček
s	s	k7c7	s
podobiznou	podobizna	k1gFnSc7	podobizna
jejich	jejich	k3xOp3gMnSc2	jejich
komunistického	komunistický	k2eAgMnSc2d1	komunistický
vůdce	vůdce	k1gMnSc2	vůdce
Kim	Kim	k1gMnSc2	Kim
Čong-ila	Čongl	k1gMnSc2	Čong-il
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
deváté	devátý	k4xOgFnSc6	devátý
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkonošem	vlajkonoš	k1gMnSc7	vlajkonoš
české	český	k2eAgFnSc2d1	Česká
výpravy	výprava	k1gFnSc2	výprava
byl	být	k5eAaImAgMnS	být
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
z	z	k7c2	z
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
Martin	Martin	k1gMnSc1	Martin
Doktor	doktor	k1gMnSc1	doktor
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
ve	v	k7c4	v
22	[number]	k4	22
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
42	[number]	k4	42
minuty	minuta	k1gFnPc4	minuta
australského	australský	k2eAgInSc2d1	australský
letního	letní	k2eAgInSc2d1	letní
času	čas	k1gInSc2	čas
zahájil	zahájit	k5eAaPmAgMnS	zahájit
hry	hra	k1gFnPc4	hra
XXVII	XXVII	kA	XXVII
<g/>
.	.	kIx.	.
olympiády	olympiáda	k1gFnSc2	olympiáda
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
Austrálie	Austrálie	k1gFnSc2	Austrálie
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc1	William
Deane	Dean	k1gMnSc5	Dean
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
slibu	slib	k1gInSc6	slib
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
vysloveném	vyslovený	k2eAgInSc6d1	vyslovený
pozemní	pozemní	k2eAgMnSc1d1	pozemní
hokejistkou	hokejistka	k1gFnSc7	hokejistka
Rechelle	Rechelle	k1gFnSc2	Rechelle
Hawkesovou	Hawkesový	k2eAgFnSc4d1	Hawkesová
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
výslovně	výslovně	k6eAd1	výslovně
zmíněn	zmíněn	k2eAgInSc4d1	zmíněn
závazek	závazek	k1gInSc4	závazek
startovat	startovat	k5eAaBmF	startovat
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
dopingu	doping	k1gInSc2	doping
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
pochodní	pochodeň	k1gFnSc7	pochodeň
na	na	k7c4	na
stadion	stadion	k1gInSc4	stadion
přiběhl	přiběhnout	k5eAaPmAgMnS	přiběhnout
legendární	legendární	k2eAgMnSc1d1	legendární
australský	australský	k2eAgMnSc1d1	australský
atlet	atlet	k1gMnSc1	atlet
Herb	Herb	k1gMnSc1	Herb
Elliott	Elliott	k1gMnSc1	Elliott
a	a	k8xC	a
štafetu	štafeta	k1gFnSc4	štafeta
převzala	převzít	k5eAaPmAgFnS	převzít
sprinterka	sprinterka	k1gFnSc1	sprinterka
Betty	Betty	k1gFnSc1	Betty
Cuthbertová	Cuthbertová	k1gFnSc1	Cuthbertová
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
invalidním	invalidní	k2eAgInSc6d1	invalidní
vozíku	vozík	k1gInSc6	vozík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
členkou	členka	k1gFnSc7	členka
štafety	štafeta	k1gFnSc2	štafeta
byla	být	k5eAaImAgFnS	být
aboridžinská	aboridžinský	k2eAgFnSc1d1	aboridžinská
běžkyně	běžkyně	k1gFnSc1	běžkyně
Cathy	Catha	k1gFnSc2	Catha
Freemanová	Freemanová	k1gFnSc1	Freemanová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zapálila	zapálit	k5eAaPmAgFnS	zapálit
olympijský	olympijský	k2eAgInSc4d1	olympijský
oheň	oheň	k1gInSc4	oheň
na	na	k7c6	na
vodní	vodní	k2eAgFnSc6d1	vodní
hladině	hladina	k1gFnSc6	hladina
v	v	k7c6	v
bazénku	bazének	k1gInSc6	bazének
pod	pod	k7c7	pod
umělým	umělý	k2eAgInSc7d1	umělý
vodopádem	vodopád	k1gInSc7	vodopád
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
výtahem	výtah	k1gInSc7	výtah
oheň	oheň	k1gInSc4	oheň
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
pylon	pylon	k1gInSc4	pylon
nad	nad	k7c7	nad
stadionem	stadion	k1gInSc7	stadion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
programu	program	k1gInSc2	program
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
mj.	mj.	kA	mj.
zpěváci	zpěvák	k1gMnPc1	zpěvák
Olivia	Olivia	k1gFnSc1	Olivia
Newton-Johnová	Newton-Johnová	k1gFnSc1	Newton-Johnová
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Farnhamem	Farnham	k1gInSc7	Farnham
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
Dare	dar	k1gInSc5	dar
to	ten	k3xDgNnSc1	ten
Dream	Dream	k1gInSc1	Dream
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vanessa	Vanessa	k1gFnSc1	Vanessa
Amorosi	Amorose	k1gFnSc4	Amorose
(	(	kIx(	(
<g/>
píseň	píseň	k1gFnSc4	píseň
Heroes	Heroesa	k1gFnPc2	Heroesa
Live	Live	k1gFnSc1	Live
Forever	Forever	k1gInSc1	Forever
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tina	Tina	k1gFnSc1	Tina
Arena	Arena	k1gFnSc1	Arena
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Flame	Flam	k1gMnSc5	Flam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hvězdičkou	hvězdička	k1gFnSc7	hvězdička
programu	program	k1gInSc2	program
Nikki	Nikk	k1gFnSc2	Nikk
Websterová	Websterová	k1gFnSc1	Websterová
(	(	kIx(	(
<g/>
Under	Under	k1gMnSc1	Under
the	the	k?	the
Southern	Southern	k1gMnSc1	Southern
Skies	Skies	k1gMnSc1	Skies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
týmu	tým	k1gInSc2	tým
autorů	autor	k1gMnPc2	autor
kulturního	kulturní	k2eAgInSc2d1	kulturní
programu	program	k1gInSc2	program
zahájení	zahájení	k1gNnSc2	zahájení
byli	být	k5eAaImAgMnP	být
režiséři	režisér	k1gMnPc1	režisér
Ric	Ric	k1gMnSc1	Ric
Birch	Birch	k1gMnSc1	Birch
a	a	k8xC	a
David	David	k1gMnSc1	David
Atkins	Atkinsa	k1gFnPc2	Atkinsa
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
i	i	k9	i
vynechání	vynechání	k1gNnSc4	vynechání
symbolů	symbol	k1gInPc2	symbol
klokana	klokan	k1gMnSc2	klokan
či	či	k8xC	či
populárního	populární	k2eAgInSc2d1	populární
filmu	film	k1gInSc2	film
Krokodýl	krokodýl	k1gMnSc1	krokodýl
Dundee	Dundee	k1gFnSc1	Dundee
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgInPc4	jenž
byli	být	k5eAaImAgMnP	být
pořadatelé	pořadatel	k1gMnPc1	pořadatel
jako	jako	k9	jako
za	za	k7c4	za
prvoplánové	prvoplánový	k2eAgFnPc4d1	prvoplánová
kritizováni	kritizovat	k5eAaImNgMnP	kritizovat
při	při	k7c6	při
krátké	krátký	k2eAgFnSc6d1	krátká
ukázce	ukázka	k1gFnSc6	ukázka
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zakončení	zakončení	k1gNnSc6	zakončení
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
Juan	Juan	k1gMnSc1	Juan
Antonio	Antonio	k1gMnSc1	Antonio
Samaranch	Samaranch	k1gMnSc1	Samaranch
označil	označit	k5eAaPmAgMnS	označit
ceremoniál	ceremoniál	k1gInSc4	ceremoniál
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejkrásnější	krásný	k2eAgNnSc4d3	nejkrásnější
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
zahájení	zahájení	k1gNnSc4	zahájení
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
předsednictví	předsednictví	k1gNnSc2	předsednictví
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
zhasl	zhasnout	k5eAaPmAgInS	zhasnout
na	na	k7c6	na
Stadionu	stadion	k1gInSc6	stadion
Australia	Australium	k1gNnSc2	Australium
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2000	[number]	k4	2000
ve	v	k7c4	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
stadionu	stadion	k1gInSc2	stadion
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
sportovci	sportovec	k1gMnPc1	sportovec
s	s	k7c7	s
vlajkami	vlajka	k1gFnPc7	vlajka
199	[number]	k4	199
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nesl	nést	k5eAaImAgMnS	nést
Roman	Roman	k1gMnSc1	Roman
Šebrle	Šebrle	k1gFnSc2	Šebrle
<g/>
,	,	kIx,	,
stříbrný	stříbrný	k2eAgMnSc1d1	stříbrný
desetibojař	desetibojař	k1gMnSc1	desetibojař
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
;	;	kIx,	;
3500	[number]	k4	3500
sportovců	sportovec	k1gMnPc2	sportovec
ale	ale	k8xC	ale
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
nebylo	být	k5eNaImAgNnS	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
podle	podle	k7c2	podle
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vběhli	vběhnout	k5eAaPmAgMnP	vběhnout
z	z	k7c2	z
několika	několik	k4yIc2	několik
vchodů	vchod	k1gInPc2	vchod
na	na	k7c4	na
stadion	stadion	k1gNnSc4	stadion
promíchaní	promíchaný	k2eAgMnPc1d1	promíchaný
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
MOV	MOV	kA	MOV
Juan	Juan	k1gMnSc1	Juan
Antonio	Antonio	k1gMnSc1	Antonio
Samaranch	Samaranch	k1gMnSc1	Samaranch
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
skvělou	skvělý	k2eAgFnSc4d1	skvělá
atmosféru	atmosféra	k1gFnSc4	atmosféra
Her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
vítěze	vítěz	k1gMnPc4	vítěz
označil	označit	k5eAaPmAgMnS	označit
Sydney	Sydney	k1gNnSc4	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Diváky	divák	k1gMnPc4	divák
pozdravil	pozdravit	k5eAaPmAgInS	pozdravit
jejich	jejich	k3xOp3gInSc7	jejich
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
pokřikem	pokřik	k1gInSc7	pokřik
"	"	kIx"	"
<g/>
Aussie	Aussie	k1gFnSc1	Aussie
Aussie	Aussie	k1gFnSc1	Aussie
Aussie	Aussie	k1gFnSc1	Aussie
<g/>
,	,	kIx,	,
Oi	Oi	k1gFnSc1	Oi
Oi	Oi	k1gFnSc1	Oi
Oi	Oi	k1gFnSc1	Oi
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Bylo	být	k5eAaImAgNnS	být
představeno	představit	k5eAaPmNgNnS	představit
osm	osm	k4xCc1	osm
nově	nově	k6eAd1	nově
zvolených	zvolený	k2eAgMnPc2d1	zvolený
členů	člen	k1gInPc2	člen
MOV	MOV	kA	MOV
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
bylo	být	k5eAaImAgNnS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
včetně	včetně	k7c2	včetně
Jana	Jan	k1gMnSc2	Jan
Železného	Železný	k1gMnSc2	Železný
<g/>
.	.	kIx.	.
</s>
<s>
Starosta	Starosta	k1gMnSc1	Starosta
Atén	Atény	k1gFnPc2	Atény
Dimitris	Dimitris	k1gFnPc2	Dimitris
L.	L.	kA	L.
Avramopoulous	Avramopoulous	k1gMnSc1	Avramopoulous
převzal	převzít	k5eAaPmAgMnS	převzít
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
vlajku	vlajka	k1gFnSc4	vlajka
od	od	k7c2	od
starosty	starosta	k1gMnSc2	starosta
Sydney	Sydney	k1gNnSc2	Sydney
Franka	Frank	k1gMnSc2	Frank
Sartora	Sartor	k1gMnSc2	Sartor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velkoplošné	velkoplošný	k2eAgFnSc6d1	velkoplošná
projekci	projekce	k1gFnSc6	projekce
zazářil	zazářit	k5eAaPmAgMnS	zazářit
nápis	nápis	k1gInSc4	nápis
"	"	kIx"	"
<g/>
Vítejte	vítat	k5eAaImRp2nP	vítat
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
Atény	Atény	k1gFnPc1	Atény
2004	[number]	k4	2004
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
Yvonne	Yvonn	k1gInSc5	Yvonn
Kennyová	Kennyová	k1gFnSc1	Kennyová
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
hymnu	hymna	k1gFnSc4	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
zhasl	zhasnout	k5eAaPmAgInS	zhasnout
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nad	nad	k7c7	nad
stadionem	stadion	k1gInSc7	stadion
prolétala	prolétat	k5eAaPmAgFnS	prolétat
stíhačka	stíhačka	k1gFnSc1	stíhačka
F-111	F-111	k1gFnSc1	F-111
australského	australský	k2eAgNnSc2d1	Australské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oheň	oheň	k1gInSc4	oheň
zdánlivě	zdánlivě	k6eAd1	zdánlivě
odnesla	odnést	k5eAaPmAgFnS	odnést
z	z	k7c2	z
pylonu	pylon	k1gInSc2	pylon
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
program	program	k1gInSc1	program
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zahájení	zahájení	k1gNnSc2	zahájení
byl	být	k5eAaImAgInS	být
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xC	jako
velká	velký	k2eAgFnSc1d1	velká
party	party	k1gFnSc1	party
s	s	k7c7	s
účinkujícími	účinkující	k2eAgFnPc7d1	účinkující
hvězdami	hvězda	k1gFnPc7	hvězda
zábavního	zábavní	k2eAgInSc2d1	zábavní
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
před	před	k7c7	před
zhašením	zhašení	k1gNnSc7	zhašení
olympijského	olympijský	k2eAgInSc2d1	olympijský
ohně	oheň	k1gInSc2	oheň
skupina	skupina	k1gFnSc1	skupina
Savage	Savag	k1gFnSc2	Savag
Garden	Gardna	k1gFnPc2	Gardna
a	a	k8xC	a
třináctiletá	třináctiletý	k2eAgFnSc1d1	třináctiletá
hvězdička	hvězdička	k1gFnSc1	hvězdička
zahájení	zahájení	k1gNnSc2	zahájení
Her	hra	k1gFnPc2	hra
Nikki	Nikk	k1gFnSc2	Nikk
Websterová	Websterová	k1gFnSc1	Websterová
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
např.	např.	kA	např.
Midnight	Midnight	k1gMnSc1	Midnight
Oil	Oil	k1gMnSc1	Oil
<g/>
,	,	kIx,	,
Vanessa	Vanessa	k1gFnSc1	Vanessa
Amorosi	Amorose	k1gFnSc4	Amorose
<g/>
,	,	kIx,	,
INXS	INXS	kA	INXS
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Paul	Paul	k1gMnSc1	Paul
Young	Young	k1gMnSc1	Young
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tzv.	tzv.	kA	tzv.
parádě	paráda	k1gFnSc6	paráda
ikon	ikona	k1gFnPc2	ikona
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Kylie	Kylie	k1gFnSc1	Kylie
Minogue	Minogue	k1gFnSc1	Minogue
<g/>
,	,	kIx,	,
golfista	golfista	k1gMnSc1	golfista
Greg	Greg	k1gMnSc1	Greg
Norman	Norman	k1gMnSc1	Norman
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
Elle	Elle	k1gFnSc1	Elle
McPhersonová	McPhersonová	k1gFnSc1	McPhersonová
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Paul	Paul	k1gMnSc1	Paul
Hogan	Hogan	k1gMnSc1	Hogan
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
ve	v	k7c4	v
14	[number]	k4	14
kilometrů	kilometr	k1gInPc2	kilometr
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
přístavu	přístav	k1gInSc6	přístav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
promítán	promítat	k5eAaImNgMnS	promítat
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
diváků	divák	k1gMnPc2	divák
na	na	k7c6	na
velkoplošných	velkoplošný	k2eAgFnPc6d1	velkoplošná
obrazovkách	obrazovka	k1gFnPc6	obrazovka
<g/>
,	,	kIx,	,
grandiózním	grandiózní	k2eAgInSc7d1	grandiózní
ohňostrojem	ohňostroj	k1gInSc7	ohňostroj
nad	nad	k7c7	nad
mostem	most	k1gInSc7	most
Harbour	Harboura	k1gFnPc2	Harboura
Bridge	Bridge	k1gFnPc2	Bridge
<g/>
.	.	kIx.	.
</s>
<s>
Zakončení	zakončení	k1gNnSc1	zakončení
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
městě	město	k1gNnSc6	město
sledovalo	sledovat	k5eAaImAgNnS	sledovat
podle	podle	k7c2	podle
organizátorů	organizátor	k1gInPc2	organizátor
přes	přes	k7c4	přes
pět	pět	k4xCc4	pět
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Výběr	výběr	k1gInSc1	výběr
vrcholů	vrchol	k1gInPc2	vrchol
je	být	k5eAaImIp3nS	být
zkompilován	zkompilován	k2eAgInSc1d1	zkompilován
podle	podle	k7c2	podle
více	hodně	k6eAd2	hodně
nezávislých	závislý	k2eNgInPc2d1	nezávislý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Sprinterka	sprinterka	k1gFnSc1	sprinterka
a	a	k8xC	a
dálkařka	dálkařka	k1gFnSc1	dálkařka
Marion	Marion	k1gInSc1	Marion
Jonesová	Jonesový	k2eAgFnSc1d1	Jonesová
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
už	už	k6eAd1	už
před	před	k7c7	před
startem	start	k1gInSc7	start
Her	hra	k1gFnPc2	hra
útok	útok	k1gInSc4	útok
na	na	k7c4	na
pět	pět	k4xCc4	pět
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
doplněné	doplněný	k2eAgInPc4d1	doplněný
dvěma	dva	k4xCgFnPc7	dva
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
bězích	běh	k1gInPc6	běh
na	na	k7c4	na
100	[number]	k4	100
a	a	k8xC	a
200	[number]	k4	200
m	m	kA	m
a	a	k8xC	a
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dálce	dálka	k1gFnSc6	dálka
obsadila	obsadit	k5eAaPmAgFnS	obsadit
třetí	třetí	k4xOgFnSc1	třetí
místo	místo	k7c2	místo
sedm	sedm	k4xCc1	sedm
centimetrů	centimetr	k1gInPc2	centimetr
za	za	k7c7	za
vítěznou	vítězný	k2eAgFnSc7d1	vítězná
Heike	Heike	k1gFnSc7	Heike
Drechslerovou	Drechslerův	k2eAgFnSc7d1	Drechslerův
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Her	hra	k1gFnPc2	hra
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
výsledek	výsledek	k1gInSc1	výsledek
zpochybňován	zpochybňován	k2eAgInSc1d1	zpochybňován
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
byl	být	k5eAaImAgInS	být
usvědčen	usvědčen	k2eAgInSc1d1	usvědčen
z	z	k7c2	z
dopingu	doping	k1gInSc2	doping
její	její	k3xOp3gFnSc4	její
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
manžel	manžel	k1gMnSc1	manžel
koulař	koulař	k1gMnSc1	koulař
C.	C.	kA	C.
J.	J.	kA	J.
Hunter	Hunter	k1gMnSc1	Hunter
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
podezření	podezření	k1gNnSc4	podezření
přinesl	přinést	k5eAaPmAgInS	přinést
skandál	skandál	k1gInSc1	skandál
kolem	kolem	k7c2	kolem
laboratoře	laboratoř	k1gFnSc2	laboratoř
Balco	Balco	k1gNnSc1	Balco
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
nové	nový	k2eAgInPc4d1	nový
dopingové	dopingový	k2eAgInPc4d1	dopingový
prostředky	prostředek	k1gInPc4	prostředek
a	a	k8xC	a
Jonesová	Jonesový	k2eAgFnSc1d1	Jonesová
měla	mít	k5eAaImAgFnS	mít
patřit	patřit	k5eAaImF	patřit
mezi	mezi	k7c4	mezi
její	její	k3xOp3gMnPc4	její
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
na	na	k7c4	na
nové	nový	k2eAgInPc4d1	nový
důkazy	důkaz	k1gInPc4	důkaz
reagovala	reagovat	k5eAaBmAgFnS	reagovat
přiznáním	přiznání	k1gNnSc7	přiznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
užívala	užívat	k5eAaImAgFnS	užívat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
doping	doping	k1gInSc1	doping
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
Americkému	americký	k2eAgInSc3d1	americký
olympijskému	olympijský	k2eAgInSc3d1	olympijský
výboru	výbor	k1gInSc3	výbor
všech	všecek	k3xTgFnPc2	všecek
pět	pět	k4xCc4	pět
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
s	s	k7c7	s
revizí	revize	k1gFnSc7	revize
výsledků	výsledek	k1gInPc2	výsledek
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
šetření	šetření	k1gNnSc4	šetření
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
pouze	pouze	k6eAd1	pouze
formálně	formálně	k6eAd1	formálně
odebral	odebrat	k5eAaPmAgInS	odebrat
medaile	medaile	k1gFnPc4	medaile
Jonesové	Jonesový	k2eAgFnPc4d1	Jonesová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k6eAd1	ještě
je	být	k5eAaImIp3nS	být
dál	daleko	k6eAd2	daleko
nepřerozdělil	přerozdělit	k5eNaPmAgMnS	přerozdělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
dodatečně	dodatečně	k6eAd1	dodatečně
diskvalifikována	diskvalifikován	k2eAgFnSc1d1	diskvalifikována
štafeta	štafeta	k1gFnSc1	štafeta
USA	USA	kA	USA
(	(	kIx(	(
<g/>
4	[number]	k4	4
×	×	k?	×
400	[number]	k4	400
m	m	kA	m
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
člen	člen	k1gMnSc1	člen
štafety	štafeta	k1gFnSc2	štafeta
Antonio	Antonio	k1gMnSc1	Antonio
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
přiznal	přiznat	k5eAaPmAgMnS	přiznat
používání	používání	k1gNnSc2	používání
dopingu	doping	k1gInSc2	doping
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
střetnout	střetnout	k5eAaPmF	střetnout
vítězka	vítězka	k1gFnSc1	vítězka
předchozích	předchozí	k2eAgFnPc2d1	předchozí
dvou	dva	k4xCgFnPc2	dva
olympiád	olympiáda	k1gFnPc2	olympiáda
Marie-José	Marie-Josý	k2eAgFnPc1d1	Marie-Josý
Pérecová	Pérecová	k1gFnSc1	Pérecová
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
s	s	k7c7	s
domácí	domácí	k2eAgFnSc7d1	domácí
favoritkou	favoritka	k1gFnSc7	favoritka
Cathy	Catha	k1gFnSc2	Catha
Freemanovou	Freemanová	k1gFnSc4	Freemanová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
zapálení	zapálení	k1gNnSc6	zapálení
olympijského	olympijský	k2eAgInSc2d1	olympijský
ohně	oheň	k1gInSc2	oheň
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Perecová	Perecová	k1gFnSc1	Perecová
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
před	před	k7c7	před
svým	svůj	k3xOyFgInSc7	svůj
startem	start	k1gInSc7	start
Sydney	Sydney	k1gNnSc2	Sydney
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
když	když	k8xS	když
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
opakovaným	opakovaný	k2eAgMnPc3d1	opakovaný
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
největší	veliký	k2eAgFnSc2d3	veliký
soupeřky	soupeřka	k1gFnSc2	soupeřka
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Freemanová	Freemanová	k1gFnSc1	Freemanová
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
na	na	k7c6	na
vyprodaném	vyprodaný	k2eAgInSc6d1	vyprodaný
stadionu	stadion	k1gInSc6	stadion
Australia	Australium	k1gNnSc2	Australium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
disciplíně	disciplína	k1gFnSc6	disciplína
mužů	muž	k1gMnPc2	muž
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
obhájil	obhájit	k5eAaPmAgMnS	obhájit
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
vítězství	vítězství	k1gNnSc4	vítězství
Američan	Američan	k1gMnSc1	Američan
Michael	Michael	k1gMnSc1	Michael
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejším	rychlý	k2eAgMnSc7d3	nejrychlejší
mužem	muž	k1gMnSc7	muž
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
krajan	krajan	k1gMnSc1	krajan
Maurice	Maurika	k1gFnSc3	Maurika
Greene	Green	k1gMnSc5	Green
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trojskoku	trojskok	k1gInSc6	trojskok
se	se	k3xPyFc4	se
z	z	k7c2	z
vítězství	vítězství	k1gNnSc2	vítězství
radoval	radovat	k5eAaImAgMnS	radovat
poprvé	poprvé	k6eAd1	poprvé
světový	světový	k2eAgMnSc1d1	světový
rekordman	rekordman	k1gMnSc1	rekordman
Jonathan	Jonathan	k1gMnSc1	Jonathan
Edwards	Edwards	k1gInSc4	Edwards
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
dálku	dálka	k1gFnSc4	dálka
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
rovněž	rovněž	k9	rovněž
poprvé	poprvé	k6eAd1	poprvé
Iván	Iván	k1gMnSc1	Iván
Pedroso	Pedrosa	k1gFnSc5	Pedrosa
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
disciplíně	disciplína	k1gFnSc6	disciplína
získal	získat	k5eAaPmAgMnS	získat
oštěpař	oštěpař	k1gMnSc1	oštěpař
Jan	Jan	k1gMnSc1	Jan
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
na	na	k7c4	na
olympijský	olympijský	k2eAgInSc4d1	olympijský
rekord	rekord	k1gInSc4	rekord
Brita	Brit	k1gMnSc4	Brit
Steva	Steve	k1gMnSc4	Steve
Backleyho	Backley	k1gMnSc4	Backley
stejnou	stejný	k2eAgFnSc7d1	stejná
mincí	mince	k1gFnSc7	mince
<g/>
.	.	kIx.	.
</s>
<s>
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
Haile	Haile	k1gInSc4	Haile
Gebrselassie	Gebrselassie	k1gFnSc2	Gebrselassie
z	z	k7c2	z
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Senzací	senzace	k1gFnPc2	senzace
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vítězství	vítězství	k1gNnSc1	vítězství
Noaha	Noah	k1gMnSc2	Noah
Ngenyho	Ngeny	k1gMnSc2	Ngeny
z	z	k7c2	z
Keni	Keňa	k1gFnSc2	Keňa
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
před	před	k7c7	před
favorizovaným	favorizovaný	k2eAgNnSc7d1	favorizované
El	Ela	k1gFnPc2	Ela
Guerroujem	Guerrouj	k1gInSc7	Guerrouj
z	z	k7c2	z
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Světový	světový	k2eAgInSc1d1	světový
rekord	rekord	k1gInSc1	rekord
nezaručil	zaručit	k5eNaPmAgInS	zaručit
úspěch	úspěch	k1gInSc4	úspěch
ani	ani	k8xC	ani
Keňanovi	Keňan	k1gMnSc6	Keňan
Wilsonu	Wilson	k1gMnSc6	Wilson
Kipketerovi	Kipketer	k1gMnSc6	Kipketer
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
už	už	k6eAd1	už
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
Dánska	Dánsko	k1gNnSc2	Dánsko
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
800	[number]	k4	800
metrů	metr	k1gInPc2	metr
nečekaně	nečekaně	k6eAd1	nečekaně
Nilsi	Nils	k1gMnSc3	Nils
Schumannovi	Schumann	k1gMnSc3	Schumann
<g/>
.	.	kIx.	.
</s>
<s>
Osmé	osmý	k4xOgNnSc1	osmý
místo	místo	k1gNnSc1	místo
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
1500	[number]	k4	1500
metrů	metr	k1gInPc2	metr
navzdory	navzdory	k7c3	navzdory
zrakovému	zrakový	k2eAgNnSc3d1	zrakové
postižení	postižení	k1gNnSc3	postižení
Marla	Marla	k1gFnSc1	Marla
Runyanová	Runyanový	k2eAgFnSc1d1	Runyanový
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
několikanásobná	několikanásobný	k2eAgFnSc1d1	několikanásobná
vítězka	vítězka	k1gFnSc1	vítězka
paralympijských	paralympijský	k2eAgFnPc2d1	paralympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plaveckých	plavecký	k2eAgFnPc6d1	plavecká
soutěžích	soutěž	k1gFnPc6	soutěž
bylo	být	k5eAaImAgNnS	být
překonáno	překonat	k5eAaPmNgNnS	překonat
celkem	celkem	k6eAd1	celkem
15	[number]	k4	15
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hodnoty	hodnota	k1gFnSc2	hodnota
cenných	cenný	k2eAgInPc2d1	cenný
kovů	kov	k1gInPc2	kov
byl	být	k5eAaImAgInS	být
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
účastníkem	účastník	k1gMnSc7	účastník
Her	hra	k1gFnPc2	hra
Ian	Ian	k1gMnSc7	Ian
Thorpe	Thorp	k1gInSc5	Thorp
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
držitel	držitel	k1gMnSc1	držitel
tří	tři	k4xCgFnPc2	tři
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
jeho	jeho	k3xOp3gNnSc2	jeho
snažení	snažení	k1gNnSc2	snažení
byla	být	k5eAaImAgFnS	být
rekordní	rekordní	k2eAgFnSc1d1	rekordní
štafeta	štafeta	k1gFnSc1	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
volný	volný	k2eAgInSc4d1	volný
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
úseku	úsek	k1gInSc6	úsek
smazal	smazat	k5eAaPmAgMnS	smazat
manko	manko	k1gNnSc4	manko
na	na	k7c4	na
finišmana	finišman	k1gMnSc4	finišman
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Garryho	Garry	k1gMnSc2	Garry
Halla	Hall	k1gMnSc2	Hall
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
o	o	k7c4	o
0,2	[number]	k4	0,2
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Sedmnáctiletý	sedmnáctiletý	k2eAgInSc1d1	sedmnáctiletý
Thorpe	Thorp	k1gInSc5	Thorp
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
také	také	k9	také
ve	v	k7c6	v
dvojnásobné	dvojnásobný	k2eAgFnSc6d1	dvojnásobná
štafetě	štafeta	k1gFnSc6	štafeta
a	a	k8xC	a
v	v	k7c6	v
individuálním	individuální	k2eAgInSc6d1	individuální
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
volný	volný	k2eAgInSc4d1	volný
způsob	způsob	k1gInSc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
100	[number]	k4	100
a	a	k8xC	a
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
volný	volný	k2eAgInSc4d1	volný
způsob	způsob	k1gInSc4	způsob
pokaždé	pokaždé	k6eAd1	pokaždé
porazil	porazit	k5eAaPmAgMnS	porazit
Nizozemec	Nizozemec	k1gMnSc1	Nizozemec
Pieter	Pieter	k1gMnSc1	Pieter
van	vana	k1gFnPc2	vana
den	dna	k1gFnPc2	dna
Hoogenband	Hoogenband	k1gInSc1	Hoogenband
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tímto	tento	k3xDgInSc7	tento
doublem	double	k1gInSc7	double
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
napodobil	napodobit	k5eAaPmAgInS	napodobit
legendárního	legendární	k2eAgMnSc4d1	legendární
Marka	Marek	k1gMnSc4	Marek
Spitze	Spitze	k1gFnSc2	Spitze
<g/>
.	.	kIx.	.
</s>
<s>
Nemenší	malý	k2eNgFnSc7d2	Nemenší
hvězdou	hvězda	k1gFnSc7	hvězda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Eric	Eric	k1gInSc4	Eric
Moussambani	Moussamban	k1gMnPc1	Moussamban
z	z	k7c2	z
Rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
Guineje	Guinea	k1gFnSc2	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozplavbě	rozplavba	k1gFnSc6	rozplavba
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
volný	volný	k2eAgInSc1d1	volný
způsob	způsob	k1gInSc1	způsob
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
času	čas	k1gInSc2	čas
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
54,70	[number]	k4	54,70
minuty	minuta	k1gFnPc1	minuta
(	(	kIx(	(
<g/>
vítězný	vítězný	k2eAgInSc4d1	vítězný
van	van	k1gInSc4	van
den	den	k1gInSc4	den
Hoogenband	Hoogenbanda	k1gFnPc2	Hoogenbanda
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
48,30	[number]	k4	48,30
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vinou	vinou	k7c2	vinou
chybných	chybný	k2eAgInPc2d1	chybný
startů	start	k1gInPc2	start
soupeřů	soupeř	k1gMnPc2	soupeř
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
svou	svůj	k3xOyFgFnSc4	svůj
rozplavbu	rozplavba	k1gFnSc4	rozplavba
sám	sám	k3xTgInSc1	sám
povzbuzovaný	povzbuzovaný	k2eAgInSc1d1	povzbuzovaný
skvělým	skvělý	k2eAgNnSc7d1	skvělé
publikem	publikum	k1gNnSc7	publikum
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
závodním	závodní	k2eAgNnSc6d1	závodní
vystoupení	vystoupení	k1gNnSc6	vystoupení
musel	muset	k5eAaImAgInS	muset
hodinu	hodina	k1gFnSc4	hodina
odpovídat	odpovídat	k5eAaImF	odpovídat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc1	tři
zlata	zlato	k1gNnPc1	zlato
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
světových	světový	k2eAgInPc6d1	světový
rekordech	rekord	k1gInPc6	rekord
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
Nizozemka	Nizozemka	k1gFnSc1	Nizozemka
Inge	Inge	k1gFnSc1	Inge
de	de	k?	de
Bruijnová	Bruijnová	k1gFnSc1	Bruijnová
<g/>
.	.	kIx.	.
</s>
<s>
Alexej	Alexej	k1gMnSc1	Alexej
Němov	Němov	k1gInSc1	Němov
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
svůj	svůj	k3xOyFgInSc4	svůj
šestinásobný	šestinásobný	k2eAgInSc4d1	šestinásobný
medailový	medailový	k2eAgInSc4d1	medailový
zisk	zisk	k1gInSc4	zisk
z	z	k7c2	z
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
sportovcem	sportovec	k1gMnSc7	sportovec
Her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dopingový	dopingový	k2eAgInSc1d1	dopingový
případ	případ	k1gInSc1	případ
v	v	k7c6	v
olympijské	olympijský	k2eAgFnSc6d1	olympijská
historii	historie	k1gFnSc6	historie
gymnastiky	gymnastika	k1gFnSc2	gymnastika
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
jednotlivkyň	jednotlivkyně	k1gFnPc2	jednotlivkyně
<g/>
.	.	kIx.	.
</s>
<s>
Andreea	Andree	k2eAgFnSc1d1	Andree
Raducanová	Raducanová	k1gFnSc1	Raducanová
byla	být	k5eAaImAgFnS	být
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
užití	užití	k1gNnSc2	užití
nedovolené	dovolený	k2eNgFnSc2d1	nedovolená
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
v	v	k7c6	v
předepsaném	předepsaný	k2eAgInSc6d1	předepsaný
léku	lék	k1gInSc6	lék
proti	proti	k7c3	proti
nachlazení	nachlazení	k1gNnSc3	nachlazení
<g/>
.	.	kIx.	.
</s>
<s>
Potrestán	potrestán	k2eAgMnSc1d1	potrestán
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
šestnáctiletá	šestnáctiletý	k2eAgFnSc1d1	šestnáctiletá
gymnastka	gymnastka	k1gFnSc1	gymnastka
ale	ale	k9	ale
musela	muset	k5eAaImAgFnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k9	tak
připadla	připadnout	k5eAaPmAgFnS	připadnout
její	její	k3xOp3gFnSc3	její
krajance	krajanka	k1gFnSc3	krajanka
Simoně	Simona	k1gFnSc3	Simona
Amanarové	Amanarová	k1gFnSc3	Amanarová
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
zlato	zlato	k1gNnSc1	zlato
boxerské	boxerský	k2eAgNnSc1d1	boxerské
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
váze	váha	k1gFnSc6	váha
získal	získat	k5eAaPmAgMnS	získat
Kubánec	Kubánec	k1gMnSc1	Kubánec
Félix	Félix	k1gInSc4	Félix
Savón	Savón	k1gMnSc1	Savón
a	a	k8xC	a
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
se	se	k3xPyFc4	se
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
nejúspěšnějším	úspěšný	k2eAgInPc3d3	nejúspěšnější
boxerům	boxer	k1gInPc3	boxer
olympijské	olympijský	k2eAgFnSc2d1	olympijská
historie	historie	k1gFnSc2	historie
-	-	kIx~	-
Pappovi	Papp	k1gMnSc3	Papp
a	a	k8xC	a
Stevensonovi	Stevenson	k1gMnSc3	Stevenson
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
naopak	naopak	k6eAd1	naopak
skončila	skončit	k5eAaPmAgFnS	skončit
série	série	k1gFnSc1	série
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
ruského	ruský	k2eAgMnSc2d1	ruský
zápasníka	zápasník	k1gMnSc2	zápasník
Alexandra	Alexandr	k1gMnSc2	Alexandr
Karelina	Karelin	k2eAgMnSc2d1	Karelin
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
nejtěžší	těžký	k2eAgFnSc2d3	nejtěžší
kategorie	kategorie	k1gFnSc2	kategorie
trojnásobného	trojnásobný	k2eAgMnSc2d1	trojnásobný
olympijského	olympijský	k2eAgMnSc2d1	olympijský
vítěze	vítěz	k1gMnSc2	vítěz
porazil	porazit	k5eAaPmAgMnS	porazit
Američan	Američan	k1gMnSc1	Američan
Rulon	Rulon	k1gMnSc1	Rulon
Gardner	Gardner	k1gMnSc1	Gardner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nezískal	získat	k5eNaPmAgInS	získat
ani	ani	k8xC	ani
jednu	jeden	k4xCgFnSc4	jeden
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
zlato	zlato	k1gNnSc4	zlato
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
snažil	snažit	k5eAaImAgMnS	snažit
i	i	k9	i
turecký	turecký	k2eAgMnSc1d1	turecký
vzpěrač	vzpěrač	k1gMnSc1	vzpěrač
Naim	Naim	k1gMnSc1	Naim
Suleymanoglu	Suleymanogla	k1gMnSc4	Suleymanogla
<g/>
.	.	kIx.	.
</s>
<s>
Vzpěračské	vzpěračský	k2eAgFnPc1d1	vzpěračská
soutěže	soutěž	k1gFnPc1	soutěž
přinesly	přinést	k5eAaPmAgFnP	přinést
mnoho	mnoho	k6eAd1	mnoho
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
řadu	řada	k1gFnSc4	řada
dopingových	dopingový	k2eAgInPc2d1	dopingový
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
Bulhaři	Bulhar	k1gMnPc1	Bulhar
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gMnSc3	on
museli	muset	k5eAaImAgMnP	muset
vrátit	vrátit	k5eAaPmF	vrátit
své	svůj	k3xOyFgFnPc4	svůj
vybojované	vybojovaný	k2eAgFnPc4d1	vybojovaná
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Džudista	džudista	k1gMnSc1	džudista
David	David	k1gMnSc1	David
Douillet	Douillet	k1gInSc4	Douillet
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
po	po	k7c6	po
Hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
utrpěl	utrpět	k5eAaPmAgInS	utrpět
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
při	při	k7c6	při
nehodě	nehoda	k1gFnSc6	nehoda
na	na	k7c6	na
motocyklu	motocykl	k1gInSc6	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgInS	porazit
Japonce	Japonec	k1gMnPc4	Japonec
Šinoharu	Šinohara	k1gFnSc4	Šinohara
kontroverzním	kontroverzní	k2eAgNnSc7d1	kontroverzní
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
sudích	sudí	k1gFnPc2	sudí
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
nevydařeném	vydařený	k2eNgInSc6d1	nevydařený
chvatu	chvat	k1gInSc6	chvat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
senzaci	senzace	k1gFnSc3	senzace
se	se	k3xPyFc4	se
schylovalo	schylovat	k5eAaImAgNnS	schylovat
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
basketbalového	basketbalový	k2eAgInSc2d1	basketbalový
turnaje	turnaj	k1gInSc2	turnaj
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
mohla	moct	k5eAaImAgFnS	moct
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
připravit	připravit	k5eAaPmF	připravit
americký	americký	k2eAgInSc4d1	americký
Dream	Dream	k1gInSc4	Dream
Team	team	k1gInSc1	team
o	o	k7c4	o
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Šarū	Šarū	k1gMnSc1	Šarū
Jasikevičius	Jasikevičius	k1gMnSc1	Jasikevičius
se	s	k7c7	s
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
sirénou	siréna	k1gFnSc7	siréna
tříbodový	tříbodový	k2eAgInSc1d1	tříbodový
pokus	pokus	k1gInSc1	pokus
neproměnil	proměnit	k5eNaPmAgInS	proměnit
a	a	k8xC	a
USA	USA	kA	USA
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
85	[number]	k4	85
<g/>
:	:	kIx,	:
<g/>
83	[number]	k4	83
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Američani	Američan	k1gMnPc1	Američan
porazili	porazit	k5eAaPmAgMnP	porazit
Francii	Francie	k1gFnSc4	Francie
85	[number]	k4	85
<g/>
:	:	kIx,	:
<g/>
75	[number]	k4	75
<g/>
.	.	kIx.	.
</s>
<s>
Tenisový	tenisový	k2eAgInSc4d1	tenisový
turnaj	turnaj	k1gInSc4	turnaj
ozdobila	ozdobit	k5eAaPmAgFnS	ozdobit
účast	účast	k1gFnSc1	účast
sester	sestra	k1gFnPc2	sestra
Venus	Venus	k1gInSc4	Venus
a	a	k8xC	a
Sereny	Seren	k1gInPc4	Seren
Williamsových	Williamsová	k1gFnPc2	Williamsová
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
pouhých	pouhý	k2eAgFnPc2d1	pouhá
dvou	dva	k4xCgFnPc2	dva
her	hra	k1gFnPc2	hra
finále	finále	k1gNnSc2	finále
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
Venus	Venus	k1gInSc4	Venus
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
i	i	k9	i
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
soutěži	soutěž	k1gFnSc6	soutěž
mužů	muž	k1gMnPc2	muž
překvapili	překvapit	k5eAaPmAgMnP	překvapit
domácí	domácí	k2eAgMnPc4d1	domácí
favority	favorit	k1gMnPc4	favorit
Woodbridge	Woodbridg	k1gMnSc4	Woodbridg
a	a	k8xC	a
Woodforda	Woodford	k1gMnSc4	Woodford
<g/>
,	,	kIx,	,
přezdívané	přezdívaný	k2eAgInPc1d1	přezdívaný
"	"	kIx"	"
<g/>
Woodies	Woodies	k1gInSc1	Woodies
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
Mark	Mark	k1gMnSc1	Mark
Nestor	Nestor	k1gMnSc1	Nestor
a	a	k8xC	a
Sébastien	Sébastien	k1gInSc1	Sébastien
Lareau	Lareaus	k1gInSc2	Lareaus
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
v	v	k7c6	v
turnaji	turnaj	k1gInSc6	turnaj
fotbalistů	fotbalista	k1gMnPc2	fotbalista
znovu	znovu	k6eAd1	znovu
patří	patřit	k5eAaImIp3nS	patřit
Afričanům	Afričan	k1gMnPc3	Afričan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazili	porazit	k5eAaPmAgMnP	porazit
Kamerunci	Kamerunek	k1gMnPc1	Kamerunek
na	na	k7c4	na
penalty	penalty	k1gNnSc4	penalty
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Penalty	penalta	k1gFnPc1	penalta
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
i	i	k9	i
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
pozemním	pozemní	k2eAgInSc6d1	pozemní
hokeji	hokej	k1gInSc6	hokej
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
sice	sice	k8xC	sice
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
smazala	smazat	k5eAaPmAgFnS	smazat
dvoubrankové	dvoubrankový	k2eAgNnSc4d1	dvoubrankové
manko	manko	k1gNnSc4	manko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
rozstřelu	rozstřel	k1gInSc6	rozstřel
přece	přece	k8xC	přece
přenechala	přenechat	k5eAaPmAgFnS	přenechat
vítězství	vítězství	k1gNnSc4	vítězství
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
olympiády	olympiáda	k1gFnPc1	olympiáda
neporažení	neporažení	k1gNnSc2	neporažení
baseballisté	baseballista	k1gMnPc1	baseballista
Kuby	Kuba	k1gFnSc2	Kuba
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
USA	USA	kA	USA
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
112	[number]	k4	112
výhrách	výhra	k1gFnPc6	výhra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
ztratily	ztratit	k5eAaPmAgInP	ztratit
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
i	i	k8xC	i
reprezentantky	reprezentantka	k1gFnPc1	reprezentantka
USA	USA	kA	USA
v	v	k7c6	v
softballu	softball	k1gInSc6	softball
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navzdory	navzdory	k6eAd1	navzdory
celkem	celkem	k6eAd1	celkem
třem	tři	k4xCgInPc3	tři
porážkám	porážka	k1gFnPc3	porážka
vybojovaly	vybojovat	k5eAaPmAgFnP	vybojovat
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
veslař	veslař	k1gMnSc1	veslař
Steven	Stevna	k1gFnPc2	Stevna
Redgrave	Redgrav	k1gInSc5	Redgrav
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
své	svůj	k3xOyFgNnSc4	svůj
páté	pátý	k4xOgNnSc4	pátý
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
na	na	k7c6	na
páté	pátý	k4xOgFnSc6	pátý
olympiádě	olympiáda	k1gFnSc6	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemské	nizozemský	k2eAgFnSc3d1	nizozemská
cyklistce	cyklistka	k1gFnSc3	cyklistka
Leontien	Leontina	k1gFnPc2	Leontina
Zijlaardové	Zijlaard	k1gMnPc1	Zijlaard
stačila	stačit	k5eAaBmAgNnP	stačit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
zlaté	zlatý	k1gInPc4	zlatý
jen	jen	k9	jen
olympiáda	olympiáda	k1gFnSc1	olympiáda
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
Neslavně	slavně	k6eNd1	slavně
se	se	k3xPyFc4	se
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
Her	hra	k1gFnPc2	hra
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
zapsal	zapsat	k5eAaPmAgInS	zapsat
doping	doping	k1gInSc1	doping
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgInSc1d3	veliký
skandál	skandál	k1gInSc1	skandál
Jonesové	Jonesový	k2eAgFnSc2d1	Jonesová
propukl	propuknout	k5eAaPmAgInS	propuknout
až	až	k9	až
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zakončení	zakončení	k1gNnSc6	zakončení
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
případů	případ	k1gInPc2	případ
bulharských	bulharský	k2eAgMnPc2d1	bulharský
medailistů	medailista	k1gMnPc2	medailista
ze	z	k7c2	z
vzpírání	vzpírání	k1gNnSc2	vzpírání
byl	být	k5eAaImAgInS	být
doping	doping	k1gInSc1	doping
zjištěn	zjistit	k5eAaPmNgInS	zjistit
i	i	k8xC	i
u	u	k7c2	u
vzpěračů	vzpěrač	k1gMnPc2	vzpěrač
z	z	k7c2	z
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
také	také	k9	také
výsledky	výsledek	k1gInPc4	výsledek
v	v	k7c6	v
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
,	,	kIx,	,
veslování	veslování	k1gNnSc6	veslování
<g/>
,	,	kIx,	,
boxu	box	k1gInSc6	box
a	a	k8xC	a
atletice	atletika	k1gFnSc6	atletika
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
na	na	k7c4	na
antidopingová	antidopingový	k2eAgNnPc4d1	antidopingové
šetření	šetření	k1gNnPc4	šetření
přímo	přímo	k6eAd1	přímo
v	v	k7c4	v
Sydney	Sydney	k1gNnSc4	Sydney
Světová	světový	k2eAgFnSc1d1	světová
antidopingová	antidopingový	k2eAgFnSc1d1	antidopingová
agentura	agentura	k1gFnSc1	agentura
WADA	WADA	kA	WADA
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
testy	test	k1gInPc1	test
na	na	k7c4	na
hormon	hormon	k1gInSc4	hormon
EPO	EPO	kA	EPO
a	a	k8xC	a
krevní	krevní	k2eAgInPc1d1	krevní
dopingové	dopingový	k2eAgInPc1d1	dopingový
testy	test	k1gInPc1	test
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prestižním	prestižní	k2eAgInSc6d1	prestižní
souboji	souboj	k1gInSc6	souboj
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
předešlých	předešlý	k2eAgFnPc6d1	předešlá
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
těšily	těšit	k5eAaImAgFnP	těšit
z	z	k7c2	z
vítězství	vítězství	k1gNnSc2	vítězství
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
měli	mít	k5eAaImAgMnP	mít
atleti	atlet	k1gMnPc1	atlet
a	a	k8xC	a
plavci	plavec	k1gMnPc1	plavec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přispěli	přispět	k5eAaPmAgMnP	přispět
do	do	k7c2	do
amerického	americký	k2eAgInSc2d1	americký
zlatého	zlatý	k2eAgInSc2d1	zlatý
pokladu	poklad	k1gInSc2	poklad
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinou	polovina	k1gFnSc7	polovina
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
polepšilo	polepšit	k5eAaPmAgNnS	polepšit
o	o	k7c6	o
šest	šest	k4xCc4	šest
zlatých	zlatý	k1gInPc2	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
nárůst	nárůst	k1gInSc1	nárůst
medailí	medaile	k1gFnPc2	medaile
vykázali	vykázat	k5eAaPmAgMnP	vykázat
domácí	domácí	k2eAgMnPc1d1	domácí
Australané	Australan	k1gMnPc1	Australan
a	a	k8xC	a
také	také	k6eAd1	také
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
počet	počet	k1gInSc4	počet
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
téměř	téměř	k6eAd1	téměř
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
skok	skok	k1gInSc1	skok
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
podařil	podařit	k5eAaPmAgInS	podařit
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
Britové	Brit	k1gMnPc1	Brit
získali	získat	k5eAaPmAgMnP	získat
jedinou	jediný	k2eAgFnSc4d1	jediná
zlatou	zlatá	k1gFnSc4	zlatá
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
hned	hned	k6eAd1	hned
jedenáct	jedenáct	k4xCc1	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
Medailový	medailový	k2eAgInSc4d1	medailový
ústup	ústup	k1gInSc4	ústup
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
sbírka	sbírka	k1gFnSc1	sbírka
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgFnPc3d1	minulá
hrám	hra	k1gFnPc3	hra
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
zlata	zlato	k1gNnPc4	zlato
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
bronz	bronz	k1gInSc4	bronz
chudší	chudý	k2eAgFnSc1d2	chudší
<g/>
.	.	kIx.	.
</s>
<s>
Premiérově	premiérově	k6eAd1	premiérově
získali	získat	k5eAaPmAgMnP	získat
medaile	medaile	k1gFnPc4	medaile
zástupci	zástupce	k1gMnPc7	zástupce
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
dvě	dva	k4xCgFnPc4	dva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
Makedonie	Makedonie	k1gFnSc2	Makedonie
<g/>
,	,	kIx,	,
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
a	a	k8xC	a
Kyrgyzstánu	Kyrgyzstán	k1gInSc2	Kyrgyzstán
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
jsou	být	k5eAaImIp3nP	být
odečtené	odečtený	k2eAgFnPc4d1	odečtená
medaile	medaile	k1gFnPc4	medaile
Marion	Marion	k1gInSc1	Marion
Jonesové	Jonesový	k2eAgInPc1d1	Jonesový
a	a	k8xC	a
štafet	štafeta	k1gFnPc2	štafeta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
startovala	startovat	k5eAaBmAgFnS	startovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
MOV	MOV	kA	MOV
kvůli	kvůli	k7c3	kvůli
dopingu	doping	k1gInSc3	doping
oficiálně	oficiálně	k6eAd1	oficiálně
odebral	odebrat	k5eAaPmAgMnS	odebrat
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c4	o
přerozdělení	přerozdělení	k1gNnSc4	přerozdělení
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
individuálních	individuální	k2eAgFnPc2d1	individuální
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
,	,	kIx,	,
o	o	k7c6	o
medailích	medaile	k1gFnPc6	medaile
ze	z	k7c2	z
štafet	štafeta	k1gFnPc2	štafeta
odložil	odložit	k5eAaPmAgInS	odložit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
až	až	k6eAd1	až
po	po	k7c6	po
verdiktu	verdikt	k1gInSc6	verdikt
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sportovní	sportovní	k2eAgFnSc2d1	sportovní
arbitráže	arbitráž	k1gFnSc2	arbitráž
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
.	.	kIx.	.
</s>
<s>
Číně	Čína	k1gFnSc3	Čína
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
startu	start	k1gInSc3	start
gymnastky	gymnastka	k1gFnSc2	gymnastka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nesplňovala	splňovat	k5eNaImAgFnS	splňovat
věkový	věkový	k2eAgInSc4d1	věkový
limit	limit	k1gInSc4	limit
<g/>
,	,	kIx,	,
odečtena	odečten	k2eAgFnSc1d1	odečtena
bronzová	bronzový	k2eAgFnSc1d1	bronzová
medaile	medaile	k1gFnSc1	medaile
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
družstev	družstvo	k1gNnPc2	družstvo
ve	v	k7c6	v
sportovní	sportovní	k2eAgFnSc6d1	sportovní
gymnastice	gymnastika	k1gFnSc6	gymnastika
<g/>
,	,	kIx,	,
přiřazena	přiřazen	k2eAgFnSc1d1	přiřazena
je	být	k5eAaImIp3nS	být
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
celkem	celkem	k6eAd1	celkem
199	[number]	k4	199
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
tři	tři	k4xCgFnPc1	tři
země	zem	k1gFnPc1	zem
<g/>
:	:	kIx,	:
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
,	,	kIx,	,
Mikronésie	Mikronésie	k1gFnSc1	Mikronésie
a	a	k8xC	a
Palau	Palaa	k1gFnSc4	Palaa
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
udávají	udávat	k5eAaImIp3nP	udávat
počty	počet	k1gInPc1	počet
sportovců	sportovec	k1gMnPc2	sportovec
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
medailí	medaile	k1gFnPc2	medaile
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
ruský	ruský	k2eAgMnSc1d1	ruský
gymnasta	gymnasta	k1gMnSc1	gymnasta
Alexej	Alexej	k1gMnSc1	Alexej
Němov	Němov	k1gInSc1	Němov
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
šesti	šest	k4xCc2	šest
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Smělý	smělý	k2eAgInSc1d1	smělý
plán	plán	k1gInSc1	plán
Marion	Marion	k1gInSc1	Marion
Jonesové	Jonesový	k2eAgInPc1d1	Jonesový
na	na	k7c4	na
zisk	zisk	k1gInSc4	zisk
pěti	pět	k4xCc2	pět
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
sice	sice	k8xC	sice
nevyšel	vyjít	k5eNaPmAgInS	vyjít
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
zisk	zisk	k1gInSc1	zisk
tří	tři	k4xCgFnPc2	tři
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
bronzových	bronzový	k2eAgFnPc2d1	bronzová
zařadil	zařadit	k5eAaPmAgInS	zařadit
Jonesovou	Jonesový	k2eAgFnSc4d1	Jonesová
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
příčky	příčka	k1gFnPc4	příčka
medailového	medailový	k2eAgInSc2d1	medailový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
však	však	k9	však
před	před	k7c7	před
soudem	soud	k1gInSc7	soud
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
brala	brát	k5eAaImAgFnS	brát
nepovolené	povolený	k2eNgInPc4d1	nepovolený
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
a	a	k8xC	a
odevzdala	odevzdat	k5eAaPmAgFnS	odevzdat
své	svůj	k3xOyFgFnPc4	svůj
medaile	medaile	k1gFnPc4	medaile
Americkému	americký	k2eAgInSc3d1	americký
olympijskému	olympijský	k2eAgInSc3d1	olympijský
výboru	výbor	k1gInSc3	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
hodnoty	hodnota	k1gFnSc2	hodnota
medailí	medaile	k1gFnPc2	medaile
však	však	k9	však
byl	být	k5eAaImAgMnS	být
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
domácí	domácí	k2eAgMnSc1d1	domácí
plavec	plavec	k1gMnSc1	plavec
Ian	Ian	k1gMnSc1	Ian
Thorpe	Thorp	k1gInSc5	Thorp
<g/>
.	.	kIx.	.
</s>
<s>
Sedmnáctiletý	sedmnáctiletý	k2eAgMnSc1d1	sedmnáctiletý
mladík	mladík	k1gMnSc1	mladík
sice	sice	k8xC	sice
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
jediné	jediný	k2eAgNnSc4d1	jediné
individuální	individuální	k2eAgNnSc4d1	individuální
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
dvou	dva	k4xCgFnPc2	dva
vítězných	vítězný	k2eAgFnPc2d1	vítězná
australských	australský	k2eAgFnPc2d1	australská
štafet	štafeta	k1gFnPc2	štafeta
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc4	dva
stříbrné	stříbrná	k1gFnPc4	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plavání	plavání	k1gNnSc6	plavání
vůbec	vůbec	k9	vůbec
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
nejvíce	hodně	k6eAd3	hodně
vícenásobných	vícenásobný	k2eAgMnPc2d1	vícenásobný
medailistů	medailista	k1gMnPc2	medailista
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
medaile	medaile	k1gFnPc1	medaile
vybojovala	vybojovat	k5eAaPmAgNnP	vybojovat
27	[number]	k4	27
<g/>
letá	letý	k2eAgFnSc1d1	letá
Američanka	Američanka	k1gFnSc1	Američanka
Jenny	Jenna	k1gFnSc2	Jenna
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
díky	díky	k7c3	díky
třem	tři	k4xCgInPc3	tři
zlatým	zlatý	k1gInPc3	zlatý
ze	z	k7c2	z
štafet	štafeta	k1gFnPc2	štafeta
již	již	k6eAd1	již
atakovala	atakovat	k5eAaBmAgFnS	atakovat
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
sportovce	sportovec	k1gMnPc4	sportovec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česko	Česko	k1gNnSc4	Česko
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
