<s>
Pantheon	Pantheon	k1gInSc1	Pantheon
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
<g/>
Π	Π	k?	Π
<g/>
;	;	kIx,	;
pan	pan	k1gMnSc1	pan
-	-	kIx~	-
vše	všechen	k3xTgNnSc1	všechen
a	a	k8xC	a
theoi	theoi	k6eAd1	theoi
-	-	kIx~	-
bohové	bůh	k1gMnPc1	bůh
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starověký	starověký	k2eAgInSc4d1	starověký
kruhový	kruhový	k2eAgInSc4d1	kruhový
chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zasvěcený	zasvěcený	k2eAgMnSc1d1	zasvěcený
všem	všecek	k3xTgMnPc3	všecek
bohům	bůh	k1gMnPc3	bůh
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
609	[number]	k4	609
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc4	Maria
mučedníků	mučedník	k1gMnPc2	mučedník
(	(	kIx(	(
<g/>
Santa	Santa	k1gMnSc1	Santa
Maria	Mario	k1gMnSc2	Mario
dei	dei	k?	dei
Martiri	Martir	k1gFnSc2	Martir
<g/>
,	,	kIx,	,
také	také	k9	také
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
Rotonda	Rotonda	k1gFnSc1	Rotonda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgInPc3d3	nejvýznamnější
a	a	k8xC	a
nejzachovalejším	zachovalý	k2eAgInPc3d3	nejzachovalejší
antickým	antický	k2eAgInPc3d1	antický
chrámům	chrám	k1gInPc3	chrám
a	a	k8xC	a
stavbám	stavba	k1gFnPc3	stavba
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
Piazza	Piazz	k1gMnSc4	Piazz
della	dell	k1gMnSc4	dell
Rotonda	Rotond	k1gMnSc4	Rotond
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Pigna	Pign	k1gInSc2	Pign
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
Marcus	Marcus	k1gInSc4	Marcus
Vipsanius	Vipsanius	k1gInSc1	Vipsanius
Agrippa	Agrippa	k1gFnSc1	Agrippa
(	(	kIx(	(
<g/>
zeť	zeť	k1gMnSc1	zeť
a	a	k8xC	a
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
císaře	císař	k1gMnSc2	císař
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
)	)	kIx)	)
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
třetího	třetí	k4xOgInSc2	třetí
konzulátu	konzulát	k1gInSc2	konzulát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
29	[number]	k4	29
až	až	k9	až
19	[number]	k4	19
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
baziliky	bazilika	k1gFnSc2	bazilika
jako	jako	k8xC	jako
svoji	svůj	k3xOyFgFnSc4	svůj
soukromou	soukromý	k2eAgFnSc4d1	soukromá
svatyni	svatyně	k1gFnSc4	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
80	[number]	k4	80
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
-	-	kIx~	-
snad	snad	k9	snad
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
-	-	kIx~	-
obnoven	obnoven	k2eAgMnSc1d1	obnoven
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Domitiána	Domitián	k1gMnSc4	Domitián
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
kruhová	kruhový	k2eAgFnSc1d1	kruhová
podoba	podoba	k1gFnSc1	podoba
Pantheonu	Pantheon	k1gInSc2	Pantheon
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Hadriánovy	Hadriánův	k2eAgFnSc2d1	Hadriánova
(	(	kIx(	(
<g/>
†	†	k?	†
138	[number]	k4	138
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
123	[number]	k4	123
přestavět	přestavět	k5eAaPmF	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
přestavby	přestavba	k1gFnSc2	přestavba
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
Apollodóros	Apollodórosa	k1gFnPc2	Apollodórosa
z	z	k7c2	z
Damašku	Damašek	k1gInSc2	Damašek
(	(	kIx(	(
<g/>
†	†	k?	†
kolem	kolem	k7c2	kolem
130	[number]	k4	130
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kopule	kopule	k1gFnSc1	kopule
a	a	k8xC	a
strop	strop	k1gInSc1	strop
předsíně	předsíň	k1gFnSc2	předsíň
byly	být	k5eAaImAgInP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
pozlacenými	pozlacený	k2eAgFnPc7d1	pozlacená
střešními	střešní	k2eAgFnPc7d1	střešní
taškami	taška	k1gFnPc7	taška
s	s	k7c7	s
reliéfy	reliéf	k1gInPc7	reliéf
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
202	[number]	k4	202
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
opraven	opravna	k1gFnPc2	opravna
císaři	císař	k1gMnSc3	císař
Septimiem	Septimium	k1gNnSc7	Septimium
Severem	sever	k1gInSc7	sever
a	a	k8xC	a
Caracallou	Caracalla	k1gFnSc7	Caracalla
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
sotva	sotva	k6eAd1	sotva
čitelný	čitelný	k2eAgInSc1d1	čitelný
nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
architrávu	architráv	k1gInSc6	architráv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
chrám	chrám	k1gInSc1	chrám
pustnul	pustnout	k5eAaPmAgInS	pustnout
<g/>
,	,	kIx,	,
až	až	k9	až
jej	on	k3xPp3gMnSc4	on
východořímský	východořímský	k2eAgMnSc1d1	východořímský
císař	císař	k1gMnSc1	císař
Fokas	Fokas	k1gMnSc1	Fokas
věnoval	věnovat	k5eAaPmAgMnS	věnovat
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
609	[number]	k4	609
byl	být	k5eAaImAgInS	být
chrám	chrám	k1gInSc1	chrám
zasvěcen	zasvěcen	k2eAgInSc1d1	zasvěcen
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
a	a	k8xC	a
mučedníkům	mučedník	k1gMnPc3	mučedník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
prý	prý	k9	prý
z	z	k7c2	z
katakomb	katakomby	k1gFnPc2	katakomby
převezeny	převézt	k5eAaPmNgInP	převézt
a	a	k8xC	a
pohřbeny	pohřbít	k5eAaPmNgInP	pohřbít
pod	pod	k7c7	pod
podlahou	podlaha	k1gFnSc7	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
chrám	chrám	k1gInSc1	chrám
zachránil	zachránit	k5eAaPmAgInS	zachránit
před	před	k7c7	před
zničením	zničení	k1gNnSc7	zničení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
postihlo	postihnout	k5eAaPmAgNnS	postihnout
tolik	tolik	k4yIc1	tolik
jiných	jiný	k2eAgFnPc2d1	jiná
starověkých	starověký	k2eAgFnPc2d1	starověká
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
roku	rok	k1gInSc2	rok
663	[number]	k4	663
dal	dát	k5eAaPmAgMnS	dát
císař	císař	k1gMnSc1	císař
Konstans	Konstansa	k1gFnPc2	Konstansa
II	II	kA	II
<g/>
.	.	kIx.	.
sejmout	sejmout	k5eAaPmF	sejmout
bronzové	bronzový	k2eAgFnPc4d1	bronzová
tašky	taška	k1gFnPc4	taška
z	z	k7c2	z
kopule	kopule	k1gFnSc2	kopule
<g/>
,	,	kIx,	,
tašky	taška	k1gFnPc1	taška
z	z	k7c2	z
předsíně	předsíň	k1gFnSc2	předsíň
dal	dát	k5eAaPmAgMnS	dát
sejmout	sejmout	k5eAaPmF	sejmout
až	až	k9	až
papež	papež	k1gMnSc1	papež
Urban	Urban	k1gMnSc1	Urban
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
něho	on	k3xPp3gMnSc4	on
byly	být	k5eAaImAgInP	být
také	také	k9	také
doplněny	doplněn	k2eAgInPc1d1	doplněn
chybějící	chybějící	k2eAgInPc1d1	chybějící
sloupy	sloup	k1gInPc1	sloup
v	v	k7c6	v
portiku	portikus	k1gInSc6	portikus
a	a	k8xC	a
středověká	středověký	k2eAgFnSc1d1	středověká
zvonice	zvonice	k1gFnSc1	zvonice
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dvěma	dva	k4xCgFnPc7	dva
malými	malý	k2eAgFnPc7d1	malá
věžičkami	věžička	k1gFnPc7	věžička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mylně	mylně	k6eAd1	mylně
připisují	připisovat	k5eAaImIp3nP	připisovat
Berninimu	Berninima	k1gFnSc4	Berninima
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
sneseny	snést	k5eAaPmNgInP	snést
za	za	k7c4	za
velké	velký	k2eAgFnPc4d1	velká
opravy	oprava	k1gFnPc4	oprava
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
doplněno	doplněn	k2eAgNnSc1d1	doplněno
mramorové	mramorový	k2eAgNnSc1d1	mramorové
obložení	obložení	k1gNnSc1	obložení
a	a	k8xC	a
podlaha	podlaha	k1gFnSc1	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
tvoří	tvořit	k5eAaImIp3nS	tvořit
mohutný	mohutný	k2eAgInSc1d1	mohutný
portikus	portikus	k1gInSc1	portikus
s	s	k7c7	s
16	[number]	k4	16
žulovými	žulový	k2eAgInPc7d1	žulový
sloupy	sloup	k1gInPc7	sloup
s	s	k7c7	s
korintskými	korintský	k2eAgFnPc7d1	Korintská
hlavicemi	hlavice	k1gFnPc7	hlavice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
částí	část	k1gFnSc7	část
původní	původní	k2eAgFnSc2d1	původní
Agrippovy	Agrippův	k2eAgFnSc2d1	Agrippův
stavby	stavba	k1gFnSc2	stavba
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
tří	tři	k4xCgInPc2	tři
sloupů	sloup	k1gInPc2	sloup
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
replikou	replika	k1gFnSc7	replika
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sloupy	sloup	k1gInPc1	sloup
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
12	[number]	k4	12
m	m	kA	m
vysoké	vysoká	k1gFnSc2	vysoká
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
60	[number]	k4	60
t.	t.	k?	t.
Jsou	být	k5eAaImIp3nP	být
seřazeny	seřadit	k5eAaPmNgInP	seřadit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c4	v
první	první	k4xOgNnSc4	první
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zbývajících	zbývající	k2eAgNnPc6d1	zbývající
dvou	dva	k4xCgNnPc6	dva
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
4	[number]	k4	4
sloupech	sloup	k1gInPc6	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlysu	vlys	k1gInSc6	vlys
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
zvěčňující	zvěčňující	k2eAgInSc1d1	zvěčňující
zakladatele	zakladatel	k1gMnSc2	zakladatel
chrámu	chrám	k1gInSc2	chrám
<g/>
:	:	kIx,	:
M	M	kA	M
AGRIPPA	AGRIPPA	kA	AGRIPPA
L	L	kA	L
F	F	kA	F
COS	cos	kA	cos
TERTIVM	TERTIVM	kA	TERTIVM
FECIT	FECIT	kA	FECIT
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
M	M	kA	M
<g/>
(	(	kIx(	(
<g/>
arcus	arcus	k1gMnSc1	arcus
<g/>
)	)	kIx)	)
Agrippa	Agrippa	k1gFnSc1	Agrippa
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
ucii	uci	k1gMnSc3	uci
<g/>
)	)	kIx)	)
<g/>
F	F	kA	F
<g/>
(	(	kIx(	(
<g/>
ilius	ilius	k1gMnSc1	ilius
<g/>
)	)	kIx)	)
CO	co	k8xS	co
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
S	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
ul	ul	kA	ul
<g/>
)	)	kIx)	)
TERTIVM	TERTIVM	kA	TERTIVM
FECIT	FECIT	kA	FECIT
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Postavil	postavit	k5eAaPmAgMnS	postavit
Marcus	Marcus	k1gMnSc1	Marcus
Agrippa	Agrippa	k1gFnSc1	Agrippa
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Lucia	Lucia	k1gFnSc1	Lucia
<g/>
,	,	kIx,	,
potřetí	potřetí	k4xO	potřetí
zvolený	zvolený	k2eAgInSc1d1	zvolený
konzulem	konzul	k1gMnSc7	konzul
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
chrám	chrám	k1gInSc1	chrám
tvoří	tvořit	k5eAaImIp3nS	tvořit
velká	velký	k2eAgFnSc1d1	velká
kruhová	kruhový	k2eAgFnSc1d1	kruhová
aula	aula	k1gFnSc1	aula
o	o	k7c6	o
stejném	stejný	k2eAgInSc6d1	stejný
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
43,2	[number]	k4	43,2
m	m	kA	m
(	(	kIx(	(
<g/>
150	[number]	k4	150
římských	římský	k2eAgFnPc2d1	římská
stop	stopa	k1gFnPc2	stopa
<g/>
)	)	kIx)	)
zakrytá	zakrytý	k2eAgFnSc1d1	zakrytá
polokulovitou	polokulovitý	k2eAgFnSc7d1	polokulovitá
klenbou	klenba	k1gFnSc7	klenba
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
otvor	otvor	k1gInSc1	otvor
(	(	kIx(	(
<g/>
okulus	okulus	k1gInSc1	okulus
<g/>
)	)	kIx)	)
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
9	[number]	k4	9
m	m	kA	m
<g/>
,	,	kIx,	,
zdroj	zdroj	k1gInSc1	zdroj
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
kopule	kopule	k1gFnSc2	kopule
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
litý	litý	k2eAgInSc1d1	litý
pucolánový	pucolánový	k2eAgInSc1d1	pucolánový
beton	beton	k1gInSc1	beton
<g/>
.	.	kIx.	.
</s>
<s>
Kopule	kopule	k1gFnSc1	kopule
ovšem	ovšem	k9	ovšem
nemá	mít	k5eNaImIp3nS	mít
armatury	armatura	k1gFnPc4	armatura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zevnitř	zevnitř	k6eAd1	zevnitř
vylehčena	vylehčit	k5eAaPmNgFnS	vylehčit
pěti	pět	k4xCc7	pět
řadami	řada	k1gFnPc7	řada
kazet	kazeta	k1gFnPc2	kazeta
(	(	kIx(	(
<g/>
lichoběžníkovitých	lichoběžníkovitý	k2eAgNnPc2d1	lichoběžníkovitý
zahloubení	zahloubení	k1gNnPc2	zahloubení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
betonu	beton	k1gInSc6	beton
jsou	být	k5eAaImIp3nP	být
keramické	keramický	k2eAgFnPc1d1	keramická
nádoby	nádoba	k1gFnPc1	nádoba
a	a	k8xC	a
kusy	kus	k1gInPc1	kus
pemzy	pemza	k1gFnSc2	pemza
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
váží	vážit	k5eAaImIp3nS	vážit
asi	asi	k9	asi
4	[number]	k4	4
500	[number]	k4	500
t.	t.	k?	t.
Mimořádně	mimořádně	k6eAd1	mimořádně
silné	silný	k2eAgFnPc1d1	silná
obvodové	obvodový	k2eAgFnPc1d1	obvodová
zdi	zeď	k1gFnPc1	zeď
(	(	kIx(	(
<g/>
6,4	[number]	k4	6,4
m	m	kA	m
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
zdiva	zdivo	k1gNnSc2	zdivo
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
sedm	sedm	k4xCc1	sedm
výklenků	výklenek	k1gInPc2	výklenek
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgMnSc1d1	hlavní
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
s	s	k7c7	s
představenými	představený	k2eAgInPc7d1	představený
sloupy	sloup	k1gInPc7	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Mramorová	mramorový	k2eAgFnSc1d1	mramorová
podlaha	podlaha	k1gFnSc1	podlaha
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
vypuklá	vypuklý	k2eAgFnSc1d1	vypuklá
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
kvůli	kvůli	k7c3	kvůli
odtoku	odtok	k1gInSc3	odtok
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
optických	optický	k2eAgInPc2d1	optický
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
díky	díky	k7c3	díky
perspektivě	perspektiva	k1gFnSc3	perspektiva
vypadá	vypadat	k5eAaPmIp3nS	vypadat
prostor	prostor	k1gInSc1	prostor
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
mramorová	mramorový	k2eAgFnSc1d1	mramorová
výzdoba	výzdoba	k1gFnSc1	výzdoba
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
padesáti	padesát	k4xCc2	padesát
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
mramory	mramor	k1gInPc1	mramor
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
podlahy	podlaha	k1gFnSc2	podlaha
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
věrnou	věrný	k2eAgFnSc7d1	věrná
replikou	replika	k1gFnSc7	replika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
chrámě	chrám	k1gInSc6	chrám
pohřbívalo	pohřbívat	k5eAaImAgNnS	pohřbívat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
mj.	mj.	kA	mj.
Raffael	Raffael	k1gMnSc1	Raffael
Santi	Sanť	k1gFnSc2	Sanť
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
snoubenka	snoubenka	k1gFnSc1	snoubenka
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
Arcangelo	Arcangela	k1gFnSc5	Arcangela
Corelli	Corell	k1gMnPc5	Corell
<g/>
,	,	kIx,	,
architekti	architekt	k1gMnPc5	architekt
Baldassare	Baldassar	k1gMnSc5	Baldassar
Peruzzi	Peruzze	k1gFnSc3	Peruzze
a	a	k8xC	a
Jacopo	Jacopa	k1gFnSc5	Jacopa
Barozzi	Barozze	k1gFnSc4	Barozze
da	da	k?	da
Vignola	Vignola	k1gFnSc1	Vignola
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
také	také	k9	také
první	první	k4xOgMnPc1	první
italští	italský	k2eAgMnPc1d1	italský
králové	král	k1gMnPc1	král
Viktor	Viktor	k1gMnSc1	Viktor
Emanuel	Emanuel	k1gMnSc1	Emanuel
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Umberto	Umberta	k1gFnSc5	Umberta
I.	I.	kA	I.
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
chrám	chrám	k1gInSc1	chrám
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
katolický	katolický	k2eAgInSc4d1	katolický
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
místo	místo	k1gNnSc1	místo
svateb	svatba	k1gFnPc2	svatba
a	a	k8xC	a
také	také	k9	také
národní	národní	k2eAgNnSc4d1	národní
mauzoleum	mauzoleum	k1gNnSc4	mauzoleum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
se	se	k3xPyFc4	se
Pantheon	Pantheon	k1gInSc1	Pantheon
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc7d3	veliký
stavbou	stavba	k1gFnSc7	stavba
zaklenutou	zaklenutý	k2eAgFnSc7d1	zaklenutá
kopulí	kopule	k1gFnSc7	kopule
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Atreova	Atreův	k2eAgFnSc1d1	Atreova
pokladnice	pokladnice	k1gFnSc1	pokladnice
v	v	k7c6	v
Mykénách	Mykény	k1gFnPc6	Mykény
<g/>
.	.	kIx.	.
</s>
<s>
Pantheon	Pantheon	k1gInSc1	Pantheon
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
toto	tento	k3xDgNnSc4	tento
prvenství	prvenství	k1gNnSc4	prvenství
podržel	podržet	k5eAaPmAgInS	podržet
až	až	k6eAd1	až
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
osmiboká	osmiboký	k2eAgFnSc1d1	osmiboká
kopule	kopule	k1gFnSc1	kopule
katedrály	katedrála	k1gFnSc2	katedrála
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
del	del	k?	del
Fiore	Fior	k1gInSc5	Fior
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
jen	jen	k9	jen
nepatrně	patrně	k6eNd1	patrně
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
zděná	zděný	k2eAgFnSc1d1	zděná
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Pantheon	Pantheon	k1gInSc1	Pantheon
pak	pak	k6eAd1	pak
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
vzor	vzor	k1gInSc1	vzor
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
slavnostních	slavnostní	k2eAgFnPc2d1	slavnostní
veřejných	veřejný	k2eAgFnPc2d1	veřejná
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
znamená	znamenat	k5eAaImIp3nS	znamenat
pantheon	pantheon	k1gInSc1	pantheon
jakýsi	jakýsi	k3yIgInSc4	jakýsi
soubor	soubor	k1gInSc4	soubor
všech	všecek	k3xTgMnPc2	všecek
bohů	bůh	k1gMnPc2	bůh
uznávaných	uznávaný	k2eAgMnPc2d1	uznávaný
či	či	k8xC	či
uctívaných	uctívaný	k2eAgMnPc2d1	uctívaný
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
získalo	získat	k5eAaPmAgNnS	získat
i	i	k9	i
význam	význam	k1gInSc4	význam
místa	místo	k1gNnSc2	místo
odpočinku	odpočinek	k1gInSc2	odpočinek
velkých	velký	k2eAgFnPc2d1	velká
osobností	osobnost	k1gFnPc2	osobnost
určitého	určitý	k2eAgInSc2d1	určitý
národa	národ	k1gInSc2	národ
-	-	kIx~	-
mauzolea	mauzoleum	k1gNnSc2	mauzoleum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Panthéon	Panthéon	k1gMnSc1	Panthéon
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
