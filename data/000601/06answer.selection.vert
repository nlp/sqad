<s>
Pantheon	Pantheon	k1gInSc1	Pantheon
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
<g/>
Π	Π	k?	Π
<g/>
;	;	kIx,	;
pan	pan	k1gMnSc1	pan
-	-	kIx~	-
vše	všechen	k3xTgNnSc1	všechen
a	a	k8xC	a
theoi	theoi	k6eAd1	theoi
-	-	kIx~	-
bohové	bůh	k1gMnPc1	bůh
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starověký	starověký	k2eAgInSc4d1	starověký
kruhový	kruhový	k2eAgInSc4d1	kruhový
chrám	chrám	k1gInSc4	chrám
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
zasvěcený	zasvěcený	k2eAgMnSc1d1	zasvěcený
všem	všecek	k3xTgMnPc3	všecek
bohům	bůh	k1gMnPc3	bůh
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
609	[number]	k4	609
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc4	Maria
mučedníků	mučedník	k1gMnPc2	mučedník
(	(	kIx(	(
<g/>
Santa	Santa	k1gMnSc1	Santa
Maria	Mario	k1gMnSc2	Mario
dei	dei	k?	dei
Martiri	Martir	k1gFnSc2	Martir
<g/>
,	,	kIx,	,
také	také	k9	také
Santa	Santa	k1gFnSc1	Santa
Maria	Maria	k1gFnSc1	Maria
Rotonda	Rotonda	k1gFnSc1	Rotonda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
