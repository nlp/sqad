<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1943	[number]	k4	1943
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
80	[number]	k4	80
let	léto	k1gNnPc2	léto
deportován	deportován	k2eAgMnSc1d1	deportován
také	také	k9	také
Emil	Emil	k1gMnSc1	Emil
Kolben	Kolben	k2eAgMnSc1d1	Kolben
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Hanušem	Hanuš	k1gMnSc7	Hanuš
a	a	k8xC	a
vnukem	vnuk	k1gMnSc7	vnuk
Jindřichem	Jindřich	k1gMnSc7	Jindřich
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Terezín	Terezín	k1gInSc1	Terezín
<g/>
.	.	kIx.	.
</s>
