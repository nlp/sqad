<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
kapacita	kapacita	k1gFnSc1	kapacita
stadionu	stadion	k1gInSc2	stadion
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
635	[number]	k4	635
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
čistě	čistě	k6eAd1	čistě
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
stadionem	stadion	k1gInSc7	stadion
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
stadionem	stadion	k1gInSc7	stadion
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
jedenáctým	jedenáctý	k4xOgInSc7	jedenáctý
největším	veliký	k2eAgInSc7d3	veliký
stadionem	stadion	k1gInSc7	stadion
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
