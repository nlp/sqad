<s>
Judith	Judith	k1gInSc1
Butlerová	Butlerová	k1gFnSc1
</s>
<s>
Judith	Judith	k1gInSc1
Butlerová	Butlerová	k1gFnSc1
Judith	Judith	k1gMnSc1
Butlerová	Butlerová	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
Narození	narození	k1gNnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1956	#num#	k4
(	(	kIx(
<g/>
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Cleveland	Cleveland	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
sociolog	sociolog	k1gMnSc1
<g/>
,	,	kIx,
literární	literární	k2eAgMnSc1d1
kritik	kritik	k1gMnSc1
<g/>
,	,	kIx,
feminista	feminista	k1gMnSc1
<g/>
,	,	kIx,
novinář	novinář	k1gMnSc1
a	a	k8xC
teoretik	teoretik	k1gMnSc1
umění	umění	k1gNnSc2
Národnost	národnost	k1gFnSc1
</s>
<s>
Američané	Američan	k1gMnPc1
Témata	téma	k1gNnPc4
</s>
<s>
feminist	feminist	k1gInSc1
theory	theora	k1gFnSc2
<g/>
,	,	kIx,
queer	queer	k1gInSc4
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
literární	literární	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
a	a	k8xC
genderová	genderový	k2eAgNnPc1d1
studia	studio	k1gNnPc1
Významná	významný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Vulnerability	Vulnerabilita	k1gFnPc1
in	in	k?
ResistanceTorture	ResistanceTortur	k1gMnSc5
and	and	k?
the	the	k?
Ethics	Ethics	k1gInSc1
of	of	k?
Phiolosophy	Phiolosopha	k1gFnSc2
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Brudner	Brudner	k1gInSc1
Prize	Prize	k1gFnSc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
čestný	čestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
Univerzity	univerzita	k1gFnSc2
Bordeaux	Bordeaux	k1gNnSc2
Montaigne	Montaign	k1gInSc5
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
Cena	cena	k1gFnSc1
Theodora	Theodora	k1gFnSc1
W.	W.	kA
Adorna	Adorna	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
Lysenkova	Lysenkův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
honorary	honorara	k1gFnSc2
doctor	doctor	k1gInSc1
of	of	k?
the	the	k?
McGill	McGill	k1gInSc4
University	universita	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Vlivy	vliv	k1gInPc4
</s>
<s>
Michel	Michel	k1gMnSc1
Foucault	Foucault	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Judith	Judith	k1gInSc1
Butlerová	Butlerová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1956	#num#	k4
Cleveland	Clevelanda	k1gFnPc2
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
feministka	feministka	k1gFnSc1
<g/>
,	,	kIx,
filozofka	filozofka	k1gFnSc1
a	a	k8xC
teoretička	teoretička	k1gFnSc1
psychoanalýzy	psychoanalýza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
knihy	kniha	k1gFnSc2
také	také	k9
odstartovaly	odstartovat	k5eAaPmAgFnP
ve	v	k7c6
feministickém	feministický	k2eAgInSc6d1
diskurzu	diskurz	k1gInSc6
posun	posun	k1gInSc4
od	od	k7c2
politiky	politika	k1gFnSc2
k	k	k7c3
otázkám	otázka	k1gFnPc3
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
obrazu	obraz	k1gInSc2
a	a	k8xC
identity	identita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Clevelandu	Cleveland	k1gInSc6
do	do	k7c2
rodiny	rodina	k1gFnSc2
ruských	ruský	k2eAgMnPc2d1
a	a	k8xC
maďarských	maďarský	k2eAgMnPc2d1
Židů	Žid	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k6eAd1
jejích	její	k3xOp3gMnPc2
příbuzných	příbuzný	k1gMnPc2
z	z	k7c2
matčiny	matčin	k2eAgFnSc2d1
strany	strana	k1gFnSc2
zahynulo	zahynout	k5eAaPmAgNnS
během	během	k7c2
holokaustu	holokaust	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
starého	starý	k2eAgInSc2d1
feminismu	feminismus	k1gInSc2
</s>
<s>
Její	její	k3xOp3gFnPc1
dvě	dva	k4xCgFnPc1
nejznámější	známý	k2eAgFnPc1d3
knihy	kniha	k1gFnPc1
Gender	Gender	k1gMnSc1
Trouble	Trouble	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Bodies	Bodies	k1gMnSc1
That	That	k1gMnSc1
Matter	Matter	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
označovány	označovat	k5eAaImNgInP
za	za	k7c4
přelomové	přelomový	k2eAgInPc4d1
ve	v	k7c6
feministickém	feministický	k2eAgNnSc6d1
hnutí	hnutí	k1gNnSc6
a	a	k8xC
za	za	k7c4
znak	znak	k1gInSc4
přechodu	přechod	k1gInSc2
k	k	k7c3
postfeminismu	postfeminismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podkopala	podkopat	k5eAaPmAgFnS
v	v	k7c6
nich	on	k3xPp3gFnPc6
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
základy	základ	k1gInPc1
tradičního	tradiční	k2eAgInSc2d1
feminismu	feminismus	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
zpochybnila	zpochybnit	k5eAaPmAgFnS
samotnou	samotný	k2eAgFnSc4d1
kategorii	kategorie	k1gFnSc4
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
feminismus	feminismus	k1gInSc1
pracuje	pracovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
koncepce	koncepce	k1gFnPc4
Michela	Michel	k1gMnSc2
Foucaulta	Foucault	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
ní	on	k3xPp3gFnSc2
klasický	klasický	k2eAgInSc1d1
feminismus	feminismus	k1gInSc1
konstruuje	konstruovat	k5eAaImIp3nS
umělý	umělý	k2eAgInSc1d1
pojem	pojem	k1gInSc1
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jeho	jeho	k3xOp3gMnSc1
prostřednictvím	prostřednictvím	k7c2
mohl	moct	k5eAaImAgInS
prosazovat	prosazovat	k5eAaImF
určité	určitý	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
konstrukce	konstrukce	k1gFnSc1
však	však	k9
přitom	přitom	k6eAd1
určitý	určitý	k2eAgInSc1d1
typ	typ	k1gInSc1
žen	žena	k1gFnPc2
vylučuje	vylučovat	k5eAaImIp3nS
a	a	k8xC
znehodnocuje	znehodnocovat	k5eAaImIp3nS
(	(	kIx(
<g/>
lesby	lesba	k1gFnPc1
<g/>
,	,	kIx,
bezdětné	bezdětný	k2eAgFnPc1d1
<g/>
,	,	kIx,
černošky	černoška	k1gFnPc1
<g/>
,	,	kIx,
nezápadní	západní	k2eNgFnPc1d1
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
podkopává	podkopávat	k5eAaImIp3nS
samotný	samotný	k2eAgInSc1d1
feminismus	feminismus	k1gInSc1
jako	jako	k8xC,k8xS
hnutí	hnutí	k1gNnSc2
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
ženy	žena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představuje	představovat	k5eAaImIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
nový	nový	k2eAgInSc1d1
feminismus	feminismus	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
umět	umět	k5eAaImF
zpochybňovat	zpochybňovat	k5eAaImF
vlastní	vlastní	k2eAgInPc4d1
předpoklady	předpoklad	k1gInPc4
a	a	k8xC
být	být	k5eAaImF
"	"	kIx"
<g/>
nikdy	nikdy	k6eAd1
nekončícím	končící	k2eNgInSc7d1
revizionismem	revizionismus	k1gInSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
je	být	k5eAaImIp3nS
Butlerová	Butlerová	k1gFnSc1
reprezentantkou	reprezentantka	k1gFnSc7
postmoderního	postmoderní	k2eAgInSc2d1
obratu	obrat	k1gInSc2
na	na	k7c6
poli	pole	k1gNnSc6
feminismu	feminismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kritizovala	kritizovat	k5eAaImAgFnS
také	také	k9
feministické	feministický	k2eAgFnPc4d1
tendence	tendence	k1gFnPc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
reprezentuje	reprezentovat	k5eAaImIp3nS
Catherine	Catherin	k1gInSc5
MacKinnonová	MacKinnonový	k2eAgFnSc1d1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
kritikou	kritika	k1gFnSc7
pornografie	pornografie	k1gFnSc2
a	a	k8xC
označováním	označování	k1gNnSc7
pornografie	pornografie	k1gFnSc2
za	za	k7c4
násilí	násilí	k1gNnSc4
vůči	vůči	k7c3
ženě	žena	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgInPc4
názory	názor	k1gInPc4
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
pouze	pouze	k6eAd1
puritánstvím	puritánství	k1gNnSc7
převlečeným	převlečený	k2eAgNnSc7d1
do	do	k7c2
feministického	feministický	k2eAgInSc2d1
dresu	dres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Polemika	polemika	k1gFnSc1
s	s	k7c7
Freudem	Freud	k1gInSc7
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
společenskému	společenský	k2eAgInSc3d1
řádu	řád	k1gInSc3
vládne	vládnout	k5eAaImIp3nS
"	"	kIx"
<g/>
heterosexuální	heterosexuální	k2eAgInSc4d1
model	model	k1gInSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tedy	tedy	k8xC
ideologický	ideologický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
určitým	určitý	k2eAgNnPc3d1
tělům	tělo	k1gNnPc3
a	a	k8xC
touhám	touha	k1gFnPc3
přisuzuje	přisuzovat	k5eAaImIp3nS
"	"	kIx"
<g/>
přirozenost	přirozenost	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
homosexuální	homosexuální	k2eAgNnSc1d1
téma	téma	k1gNnSc1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
řady	řada	k1gFnSc2
obsahů	obsah	k1gInPc2
pop-kultury	pop-kultura	k1gFnSc2
<g/>
,	,	kIx,
Butlerová	Butlerová	k1gFnSc1
dodala	dodat	k5eAaPmAgFnS
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
základní	základní	k2eAgFnSc3d1
tezi	teze	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
heterosexuální	heterosexuální	k2eAgInSc4d1
model	model	k1gInSc4
si	se	k3xPyFc3
osvojil	osvojit	k5eAaPmAgMnS
rafinovanější	rafinovaný	k2eAgInPc4d2
prostředky	prostředek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Teze	teze	k1gFnPc1
o	o	k7c6
"	"	kIx"
<g/>
heterosexuálním	heterosexuální	k2eAgInSc6d1
modelu	model	k1gInSc6
<g/>
"	"	kIx"
vyvěrají	vyvěrat	k5eAaImIp3nP
z	z	k7c2
polemiky	polemika	k1gFnSc2
s	s	k7c7
učením	učení	k1gNnSc7
Sigmunda	Sigmund	k1gMnSc2
Freuda	Freud	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
centru	centrum	k1gNnSc6
filozofie	filozofie	k1gFnSc2
Butlerové	Butlerová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
primárním	primární	k2eAgNnSc7d1
tabu	tabu	k1gNnSc7
kultury	kultura	k1gFnSc2
není	být	k5eNaImIp3nS
incest	incest	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
tvrdil	tvrdit	k5eAaImAgMnS
Freud	Freud	k1gInSc4
(	(	kIx(
<g/>
či	či	k8xC
Lévi-Strauss	Lévi-Strauss	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
tabu	tabu	k1gNnSc1
homosexuality	homosexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freud	Freud	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dítě	dítě	k1gNnSc1
je	být	k5eAaImIp3nS
primárně	primárně	k6eAd1
"	"	kIx"
<g/>
bisexuální	bisexuální	k2eAgMnSc1d1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
v	v	k7c6
různých	různý	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
jeho	jeho	k3xOp3gFnSc2
tvorby	tvorba	k1gFnSc2
to	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
různý	různý	k2eAgInSc4d1
význam	význam	k1gInSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
to	ten	k3xDgNnSc4
zcela	zcela	k6eAd1
popíral	popírat	k5eAaImAgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
že	že	k8xS
dokáže	dokázat	k5eAaPmIp3nS
získávat	získávat	k5eAaImF
slast	slast	k1gFnSc4
z	z	k7c2
mužské	mužský	k2eAgFnSc2d1
i	i	k8xC
ženské	ženský	k2eAgFnSc2d1
(	(	kIx(
<g/>
pasivní	pasivní	k2eAgFnSc2d1
i	i	k8xC
aktivní	aktivní	k2eAgFnSc2d1
<g/>
)	)	kIx)
pozice	pozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Butlerové	Butlerová	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
Freud	Freud	k1gMnSc1
mýlil	mýlit	k5eAaImAgMnS
<g/>
,	,	kIx,
dítě	dítě	k1gNnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
primárně	primárně	k6eAd1
homosexuální	homosexuální	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
homosexuality	homosexualita	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
vzdává	vzdávat	k5eAaImIp3nS
přijetím	přijetí	k1gNnSc7
oidipovského	oidipovský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mu	on	k3xPp3gMnSc3
umožní	umožnit	k5eAaPmIp3nS
nebýt	být	k5eNaImF
psychotickým	psychotický	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstane	zůstat	k5eAaPmIp3nS
v	v	k7c6
něm	on	k3xPp3gNnSc6
(	(	kIx(
<g/>
a	a	k8xC
v	v	k7c6
celé	celý	k2eAgFnSc6d1
heterosexuální	heterosexuální	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
)	)	kIx)
ovšem	ovšem	k9
stín	stín	k1gInSc4
melancholie	melancholie	k1gFnSc2
<g/>
,	,	kIx,
tesknoty	tesknota	k1gFnPc1
po	po	k7c6
ztraceném	ztracený	k2eAgInSc6d1
stejnopohlavním	stejnopohlavní	k2eAgInSc6d1
objektu	objekt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heterosexualita	Heterosexualita	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
vždy	vždy	k6eAd1
v	v	k7c6
zásadě	zásada	k1gFnSc6
potlačenou	potlačený	k2eAgFnSc7d1
primární	primární	k2eAgFnSc7d1
homosexualitou	homosexualita	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
kultuře	kultura	k1gFnSc6
běžně	běžně	k6eAd1
homosexualitou	homosexualita	k1gFnSc7
nazýváno	nazývat	k5eAaImNgNnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jen	jen	k9
jakýsi	jakýsi	k3yIgInSc1
nutný	nutný	k2eAgInSc1d1
dovětek	dovětek	k1gInSc1
útlaku	útlak	k1gInSc2
<g/>
,	,	kIx,
cosi	cosi	k3yInSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
vždy	vždy	k6eAd1
znovu	znovu	k6eAd1
vstoupí	vstoupit	k5eAaPmIp3nP
do	do	k7c2
hry	hra	k1gFnSc2
jako	jako	k8xC,k8xS
"	"	kIx"
<g/>
vzpomínka	vzpomínka	k1gFnSc1
<g/>
"	"	kIx"
při	při	k7c6
vynucování	vynucování	k1gNnSc6
heterosexuality	heterosexualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
by	by	kYmCp3nP
podle	podle	k7c2
Butlerové	Butlerová	k1gFnSc2
homosexualita	homosexualita	k1gFnSc1
bez	bez	k7c2
kulturního	kulturní	k2eAgInSc2d1
útlaku	útlak	k1gInSc2
neexistovala	existovat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homosexuální	homosexuální	k2eAgFnSc1d1
touha	touha	k1gFnSc1
je	být	k5eAaImIp3nS
kulturou	kultura	k1gFnSc7
vyvolávána	vyvoláván	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
vzápětí	vzápětí	k6eAd1
potlačena	potlačit	k5eAaPmNgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heterosexuální	heterosexuální	k2eAgInSc1d1
útlak	útlak	k1gInSc1
je	být	k5eAaImIp3nS
však	však	k9
nutný	nutný	k2eAgInSc1d1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nS
kultura	kultura	k1gFnSc1
nemohla	moct	k5eNaImAgFnS
přežít	přežít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Performativita	Performativita	k1gFnSc1
a	a	k8xC
gender	gender	k1gInSc1
</s>
<s>
Objekt	objekt	k1gInSc1
ztracené	ztracený	k2eAgFnSc2d1
touhy	touha	k1gFnSc2
si	se	k3xPyFc3
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
nachází	nacházet	k5eAaImIp3nP
v	v	k7c6
kultuře	kultura	k1gFnSc6
své	svůj	k3xOyFgInPc4
místo	místo	k1gNnSc1
–	–	k?
především	především	k6eAd1
v	v	k7c6
módě	móda	k1gFnSc6
a	a	k8xC
v	v	k7c6
subkulturách	subkultura	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kulturální	kulturální	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
jejích	její	k3xOp3gFnPc2
tezí	teze	k1gFnPc2
je	být	k5eAaImIp3nS
podstatná	podstatný	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
z	z	k7c2
ní	on	k3xPp3gFnSc2
vyvěrá	vyvěrat	k5eAaImIp3nS
obecná	obecný	k2eAgFnSc1d1
definice	definice	k1gFnSc1
kulturní	kulturní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
:	:	kIx,
ta	ten	k3xDgFnSc1
vždy	vždy	k6eAd1
dle	dle	k7c2
Butlerové	Butlerová	k1gFnSc2
postupuje	postupovat	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
periferii	periferie	k1gFnSc4
umístí	umístit	k5eAaPmIp3nS
určité	určitý	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jí	on	k3xPp3gFnSc3
jsou	být	k5eAaImIp3nP
protiváhou	protiváha	k1gFnSc7
a	a	k8xC
zdánlivě	zdánlivě	k6eAd1
ji	on	k3xPp3gFnSc4
podkopávají	podkopávat	k5eAaImIp3nP
(	(	kIx(
<g/>
například	například	k6eAd1
subkultury	subkultura	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
musejí	muset	k5eAaImIp3nP
tyto	tento	k3xDgInPc4
prostředky	prostředek	k1gInPc4
(	(	kIx(
<g/>
subkultury	subkultura	k1gFnPc4
<g/>
)	)	kIx)
přijmout	přijmout	k5eAaPmF
cosi	cosi	k3yInSc4
ze	z	k7c2
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
zplodil	zplodit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
naformují	naformovat	k5eAaPmIp3nP
<g/>
,	,	kIx,
přijímají	přijímat	k5eAaImIp3nP
určitý	určitý	k2eAgInSc4d1
rámec	rámec	k1gInSc4
<g/>
,	,	kIx,
proti	proti	k7c3
němuž	jenž	k3xRgInSc3
tak	tak	k6eAd1
mohou	moct	k5eAaImIp3nP
rezistovat	rezistovat	k5eAaBmF,k5eAaImF,k5eAaPmF
jen	jen	k9
částečně	částečně	k6eAd1
–	–	k?
částečně	částečně	k6eAd1
ho	on	k3xPp3gInSc4
zároveň	zároveň	k6eAd1
svou	svůj	k3xOyFgFnSc7
rezistencí	rezistence	k1gFnSc7
posilují	posilovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
koncepce	koncepce	k1gFnSc1
je	být	k5eAaImIp3nS
blízká	blízký	k2eAgFnSc1d1
teorii	teorie	k1gFnSc4
hegemonie	hegemonie	k1gFnSc2
Antonia	Antonio	k1gMnSc2
Gramsciho	Gramsci	k1gMnSc2
<g/>
,	,	kIx,
Butlerová	Butlerová	k1gFnSc1
však	však	k9
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
nenápadný	nápadný	k2eNgInSc1d1
útlak	útlak	k1gInSc1
není	být	k5eNaImIp3nS
primárně	primárně	k6eAd1
politický	politický	k2eAgMnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
si	se	k3xPyFc3
myslel	myslet	k5eAaImAgMnS
Gramsci	Gramsek	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
uskutečňuje	uskutečňovat	k5eAaImIp3nS
se	s	k7c7
tlakem	tlak	k1gInSc7
na	na	k7c4
formování	formování	k1gNnSc4
těla	tělo	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
obalu	obal	k1gInSc2
(	(	kIx(
<g/>
styl	styl	k1gInSc1
<g/>
,	,	kIx,
móda	móda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tlakem	tlak	k1gInSc7
v	v	k7c6
nejintimnější	intimní	k2eAgFnSc6d3
zóně	zóna	k1gFnSc6
<g/>
,	,	kIx,
zóně	zóna	k1gFnSc6
sexuality	sexualita	k1gFnSc2
a	a	k8xC
identity	identita	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
tom	ten	k3xDgNnSc6
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
Foucaulta	Foucault	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Techniku	technika	k1gFnSc4
útlaku	útlak	k1gInSc2
v	v	k7c6
intimní	intimní	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
nazývá	nazývat	k5eAaImIp3nS
performativita	performativita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krom	krom	k7c2
Foucaultova	Foucaultův	k2eAgInSc2d1
konceptu	koncept	k1gInSc2
z	z	k7c2
Dějin	dějiny	k1gFnPc2
sexuality	sexualita	k1gFnSc2
do	do	k7c2
pojmu	pojem	k1gInSc2
Butlerová	Butlerová	k1gFnSc1
zakomponovala	zakomponovat	k5eAaPmAgFnS
i	i	k9
Derridův	Derridův	k2eAgInSc4d1
koncept	koncept	k1gInSc4
"	"	kIx"
<g/>
Zákona	zákon	k1gInSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Austinův	Austinův	k2eAgInSc1d1
koncept	koncept	k1gInSc1
ilokučních	ilokuční	k2eAgMnPc2d1
(	(	kIx(
<g/>
též	též	k6eAd1
mluvních	mluvní	k2eAgInPc2d1
<g/>
)	)	kIx)
aktů	akt	k1gInPc2
a	a	k8xC
Althusserův	Althusserův	k2eAgInSc4d1
koncept	koncept	k1gInSc4
interpelace	interpelace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
Butlerová	Butlerový	k2eAgFnSc1d1
definuje	definovat	k5eAaBmIp3nS
performativitu	performativita	k1gFnSc4
jako	jako	k8xC,k8xS
aktivitu	aktivita	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
zdánlivě	zdánlivě	k6eAd1
jen	jen	k6eAd1
popisuje	popisovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
genderu	gender	k1gInSc2
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zdánlivý	zdánlivý	k2eAgInSc1d1
neutrální	neutrální	k2eAgInSc1d1
popis	popis	k1gInSc1
(	(	kIx(
<g/>
žena	žena	k1gFnSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
a	a	k8xC
to	ten	k3xDgNnSc4
<g/>
)	)	kIx)
teprve	teprve	k6eAd1
popisované	popisovaný	k2eAgNnSc1d1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
gender	gendrat	k5eAaPmRp2nS
žena	žena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gender	Gender	k1gInSc1
je	být	k5eAaImIp3nS
tak	tak	k9
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
zcela	zcela	k6eAd1
diskurzivní	diskurzivnět	k5eAaPmIp3nS
a	a	k8xC
kulturní	kulturní	k2eAgFnSc7d1
kategorií	kategorie	k1gFnSc7
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
nijak	nijak	k6eAd1
podmíněn	podmínit	k5eAaPmNgMnS
anatomií	anatomie	k1gFnSc7
těla	tělo	k1gNnSc2
a	a	k8xC
biologickým	biologický	k2eAgNnSc7d1
pohlavím	pohlaví	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
"	"	kIx"
<g/>
regulativní	regulativní	k2eAgInSc4d1
ideál	ideál	k1gInSc4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
její	její	k3xOp3gNnSc1
konstatování	konstatování	k1gNnSc1
v	v	k7c4
Gender	Gender	k1gInSc4
trouble	trouble	k6eAd1
vedlo	vést	k5eAaImAgNnS
ve	v	k7c6
feminismu	feminismus	k1gInSc6
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
ke	k	k7c3
snahám	snaha	k1gFnPc3
"	"	kIx"
<g/>
vytvořit	vytvořit	k5eAaPmF
si	se	k3xPyFc3
vlastní	vlastní	k2eAgInSc4d1
gender	gender	k1gInSc4
podle	podle	k7c2
svého	svůj	k3xOyFgNnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Butlerová	Butlerová	k1gFnSc1
v	v	k7c6
pozdějších	pozdní	k2eAgInPc6d2
pracích	prak	k1gInPc6
kritizovala	kritizovat	k5eAaImAgFnS
jako	jako	k9
nepochopení	nepochopení	k1gNnSc1
své	svůj	k3xOyFgFnSc2
myšlenky	myšlenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Butlerové	Butlerová	k1gFnSc2
totiž	totiž	k9
gender	gender	k1gInSc1
vždy	vždy	k6eAd1
musí	muset	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
žádné	žádný	k3yNgInPc1
esenciální	esenciální	k2eAgInPc1d1
já	já	k3xPp1nSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
odhodlat	odhodlat	k5eAaPmF
k	k	k7c3
tvorbě	tvorba	k1gFnSc3
vlastního	vlastní	k2eAgInSc2d1
genderu	gender	k1gInSc2
bez	bez	k7c2
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Polemika	polemika	k1gFnSc1
s	s	k7c7
Lacanem	Lacan	k1gInSc7
</s>
<s>
Její	její	k3xOp3gInSc1
psychoanalytický	psychoanalytický	k2eAgInSc1d1
přístup	přístup	k1gInSc1
je	být	k5eAaImIp3nS
krom	krom	k7c2
polemiky	polemika	k1gFnSc2
s	s	k7c7
Freudem	Freud	k1gInSc7
vymezen	vymezit	k5eAaPmNgInS
také	také	k9
její	její	k3xOp3gFnSc7
polemikou	polemika	k1gFnSc7
s	s	k7c7
Jacquesem	Jacques	k1gMnSc7
Lacanem	Lacan	k1gMnSc7
<g/>
,	,	kIx,
na	na	k7c4
nějž	jenž	k3xRgInSc4
navázala	navázat	k5eAaPmAgFnS
především	především	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lacan	Lacan	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vstup	vstup	k1gInSc1
dítěte	dítě	k1gNnSc2
do	do	k7c2
jazyka	jazyk	k1gInSc2
je	být	k5eAaImIp3nS
podmíněn	podmínit	k5eAaPmNgInS
přijetím	přijetí	k1gNnSc7
genderu	gender	k1gInSc2
(	(	kIx(
<g/>
"	"	kIx"
<g/>
jednoho	jeden	k4xCgInSc2
z	z	k7c2
pohlaví	pohlaví	k1gNnSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
u	u	k7c2
dítěte	dítě	k1gNnSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
psychóze	psychóza	k1gFnSc3
a	a	k8xC
k	k	k7c3
nemožnosti	nemožnost	k1gFnSc3
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
sociální	sociální	k2eAgFnSc7d1
bytostí	bytost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčovým	klíčový	k2eAgNnSc7d1
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
falus	falus	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
přijetí	přijetí	k1gNnSc1
genderu	gender	k1gInSc2
znamená	znamenat	k5eAaImIp3nS
přijmout	přijmout	k5eAaPmF
buď	buď	k8xC
ideu	idea	k1gFnSc4
že	že	k8xS
falus	falus	k1gInSc4
mám	mít	k5eAaImIp1nS
(	(	kIx(
<g/>
mužský	mužský	k2eAgInSc1d1
gender	gender	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
že	že	k8xS
falem	falos	k1gInSc7
jsem	být	k5eAaImIp1nS
(	(	kIx(
<g/>
ženský	ženský	k2eAgInSc4d1
gender	gender	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinou	jediný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
žena	žena	k1gFnSc1
dle	dle	k7c2
Lacana	Lacan	k1gMnSc2
může	moct	k5eAaImIp3nS
falus	falus	k1gInSc4
mít	mít	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
identifikuje	identifikovat	k5eAaBmIp3nS
s	s	k7c7
hrozivou	hrozivý	k2eAgFnSc7d1
figurou	figura	k1gFnSc7
falické	falický	k2eAgFnSc2d1
matky	matka	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
na	na	k7c6
rovině	rovina	k1gFnSc6
perverze	perverze	k1gFnSc2
může	moct	k5eAaImIp3nS
vyjevit	vyjevit	k5eAaPmF
například	například	k6eAd1
rolí	role	k1gFnPc2
"	"	kIx"
<g/>
dominy	domino	k1gNnPc7
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgFnSc1
<g/>
,	,	kIx,
ač	ač	k8xS
přijímá	přijímat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
celý	celý	k2eAgInSc1d1
Lacanův	Lacanův	k2eAgInSc1d1
model	model	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
ním	on	k3xPp3gMnSc7
polemizuje	polemizovat	k5eAaImIp3nS
právě	právě	k6eAd1
v	v	k7c6
názoru	názor	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
identifikace	identifikace	k1gFnSc1
s	s	k7c7
falickou	falický	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
žena	žena	k1gFnSc1
může	moct	k5eAaImIp3nS
k	k	k7c3
vlastnictví	vlastnictví	k1gNnSc3
falu	falos	k1gInSc2
dopracovat	dopracovat	k5eAaPmF
(	(	kIx(
<g/>
falem	falos	k1gInSc7
není	být	k5eNaImIp3nS
míněn	míněn	k2eAgInSc1d1
fyzický	fyzický	k2eAgInSc1d1
penis	penis	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
idea	idea	k1gFnSc1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
je	být	k5eAaImIp3nS
penis	penis	k1gInSc1
spojen	spojit	k5eAaPmNgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
sama	sám	k3xTgMnSc4
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
může	moct	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
i	i	k9
jiný	jiný	k2eAgInSc4d1
typ	typ	k1gInSc4
falické	falický	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Falická	falický	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
není	být	k5eNaImIp3nS
mocnou	mocný	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
učiní	učinit	k5eAaImIp3nS,k5eAaPmIp3nS
falus	falus	k1gInSc4
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falická	falický	k2eAgFnSc1d1
dívka	dívka	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k9
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
jediná	jediný	k2eAgFnSc1d1
alternativa	alternativa	k1gFnSc1
k	k	k7c3
moci	moc	k1gFnSc3
kultury	kultura	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
genderu	gender	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kritizovala	kritizovat	k5eAaImAgFnS
též	též	k9
lacanismus	lacanismus	k1gInSc4
(	(	kIx(
<g/>
potažmo	potažmo	k6eAd1
freudismus	freudismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
symbolické	symbolický	k2eAgNnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
oidipovský	oidipovský	k2eAgInSc1d1
řád	řád	k1gInSc1
významů	význam	k1gInPc2
spjatý	spjatý	k2eAgMnSc1d1
s	s	k7c7
falem	falos	k1gInSc7
a	a	k8xC
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
popisují	popisovat	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
vytvářejí	vytvářet	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
fungují	fungovat	k5eAaImIp3nP
tak	tak	k6eAd1
jako	jako	k8xS,k8xC
stráž	stráž	k1gFnSc1
kultury	kultura	k1gFnSc2
bojující	bojující	k2eAgFnSc2d1
proti	proti	k7c3
alternativě	alternativa	k1gFnSc3
a	a	k8xC
utopii	utopie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
druhé	druhý	k4xOgFnSc2
strany	strana	k1gFnSc2
byl	být	k5eAaImAgInS
ovšem	ovšem	k9
lacanismus	lacanismus	k1gInSc1
obviňován	obviňován	k2eAgInSc1d1
z	z	k7c2
přesně	přesně	k6eAd1
opačného	opačný	k2eAgInSc2d1
hříchu	hřích	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
pokusu	pokus	k1gInSc2
oidipovský	oidipovský	k2eAgInSc4d1
řád	řád	k1gInSc4
podkopat	podkopat	k5eAaPmF
ve	v	k7c6
jménu	jméno	k1gNnSc6
utopie	utopie	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Janine	Janin	k1gMnSc5
Chasseguet-Smirgelová	Chasseguet-Smirgelový	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Judith	Judith	k1gInSc1
Butlerová	Butlerová	k1gFnSc1
je	být	k5eAaImIp3nS
lesba	lesba	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
identifikuje	identifikovat	k5eAaBmIp3nS
se	se	k3xPyFc4
jako	jako	k9
nebinární	binární	k2eNgFnSc1d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vyhovuje	vyhovovat	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
být	být	k5eAaImF
označována	označován	k2eAgNnPc4d1
zájmeny	zájmeno	k1gNnPc7
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
i	i	k9
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Interview	interview	k1gNnSc1
with	with	k1gMnSc1
Judith	Judith	k1gMnSc1
Butler	Butler	k1gMnSc1
<g/>
(	(	kIx(
<g/>
english	english	k1gMnSc1
<g/>
)	)	kIx)
by	by	kYmCp3nS
Regina	Regina	k1gFnSc1
Michalik	Michalika	k1gFnPc2
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lola	Lola	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2006-12-19	2006-12-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Judith	Judith	k1gMnSc1
Butler	Butler	k1gMnSc1
<g/>
:	:	kIx,
As	as	k1gInSc1
a	a	k8xC
Jew	Jew	k1gFnSc1
<g/>
,	,	kIx,
I	i	k8xC
was	was	k?
taught	taught	k1gInSc1
it	it	k?
was	was	k?
ethically	ethicalla	k1gFnSc2
imperative	imperativ	k1gInSc5
to	ten	k3xDgNnSc4
speak	speak	k1gMnSc1
up	up	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haaretz	Haaretz	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAUGHENBAUGH	DAUGHENBAUGH	kA
<g/>
,	,	kIx,
Lynda	Lynda	k1gFnSc1
Robbirds	Robbirds	k1gInSc1
<g/>
;	;	kIx,
SHAW	SHAW	kA
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
L.	L.	kA
A	a	k8xC
critical	criticat	k5eAaPmAgMnS
pedagogy	pedagog	k1gMnPc4
of	of	k?
resistance	resistance	k1gFnSc1
<g/>
:	:	kIx,
34	#num#	k4
pedagogues	pedagogues	k1gMnSc1
we	we	k?
need	need	k1gMnSc1
to	ten	k3xDgNnSc1
know	know	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Kirylo	Kirylo	k1gFnSc2
James	James	k1gMnSc1
D.	D.	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
94	#num#	k4
<g/>
-	-	kIx~
<g/>
6209	#num#	k4
<g/>
-	-	kIx~
<g/>
374	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
978	#num#	k4
<g/>
-	-	kIx~
<g/>
94	#num#	k4
<g/>
-	-	kIx~
<g/>
6209	#num#	k4
<g/>
-	-	kIx~
<g/>
374	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
_	_	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Judith	Judith	k1gMnSc1
Butler	Butler	k1gMnSc1
<g/>
:	:	kIx,
Philosophy	Philosopha	k1gMnSc2
of	of	k?
Resistance	Resistanec	k1gMnSc2
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Judith	Judith	k1gMnSc1
Butler	Butler	k1gMnSc1
on	on	k3xPp3gMnSc1
her	hra	k1gFnPc2
Philosophy	Philosopha	k1gFnSc2
and	and	k?
Current	Current	k1gInSc1
Events	Events	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interviews	Interviews	k1gInSc4
by	by	k9
Kian	Kian	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-27	2019-12-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zero	Zero	k6eAd1
Books	Booksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matt	Matt	k1gMnSc1
McManus	McManus	k1gMnSc1
Interviews	Interviewsa	k1gFnPc2
Judith	Judith	k1gMnSc1
Butler	Butler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
YouTube	YouTub	k1gInSc5
[	[	kIx(
<g/>
video	video	k1gNnSc1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-07-21	2020-07-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Judith	Judith	k1gMnSc1
Butler	Butler	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
culture	cultur	k1gMnSc5
wars	wars	k1gInSc1
<g/>
,	,	kIx,
JK	JK	kA
Rowling	Rowling	k1gInSc1
and	and	k?
living	living	k1gInSc1
in	in	k?
“	“	k?
<g/>
anti-intellectual	anti-intellectual	k1gMnSc1
times	times	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
www.newstatesman.com	www.newstatesman.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
McROBBIE	McROBBIE	k?
<g/>
,	,	kIx,
Angela	Angela	k1gFnSc1
<g/>
:	:	kIx,
Aktuální	aktuální	k2eAgNnPc4d1
témata	téma	k1gNnPc4
kulturálních	kulturální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Portál	portál	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7367-156-5	80-7367-156-5	k4
</s>
<s>
BARKER	BARKER	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
:	:	kIx,
Slovník	slovník	k1gInSc1
kulturálních	kulturální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Portál	portál	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7367-099-2	80-7367-099-2	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
psychoanalytické	psychoanalytický	k2eAgInPc1d1
směry	směr	k1gInPc1
</s>
<s>
Jacques	Jacques	k1gMnSc1
Lacan	Lacan	k1gMnSc1
</s>
<s>
Michel	Michel	k1gMnSc1
Foucault	Foucault	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Judith	Juditha	k1gFnPc2
Butlerová	Butlerová	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Judith	Judith	k1gInSc1
Butlerová	Butlerový	k2eAgFnSc1d1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Judith	Judith	k1gInSc1
Butlerová	Butlerový	k2eAgFnSc1d1
na	na	k7c4
Perlentaucher	Perlentauchra	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
k	k	k7c3
tématu	téma	k1gNnSc3
Judith	Juditha	k1gFnPc2
Butlerová	Butlerová	k1gFnSc1
na	na	k7c4
Obalkyknih	Obalkyknih	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Psychoanalýza	psychoanalýza	k1gFnSc1
Hlavní	hlavní	k2eAgFnSc1d1
osobnosti	osobnost	k1gFnSc3
</s>
<s>
Sigmund	Sigmund	k1gMnSc1
Freud	Freud	k1gMnSc1
•	•	k?
Anna	Anna	k1gFnSc1
Freudová	Freudová	k1gFnSc1
•	•	k?
Alfred	Alfred	k1gMnSc1
Adler	Adler	k1gMnSc1
•	•	k?
Carl	Carl	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Jung	Jung	k1gMnSc1
•	•	k?
Margaret	Margareta	k1gFnPc2
Mahlerová	Mahlerový	k2eAgFnSc1d1
•	•	k?
Melanie	Melanie	k1gFnSc1
Kleinová	Kleinová	k1gFnSc1
•	•	k?
Jacques	Jacques	k1gMnSc1
Lacan	Lacan	k1gMnSc1
•	•	k?
Donald	Donald	k1gMnSc1
Woods	Woods	k1gInSc4
Winnicott	Winnicott	k2eAgInSc4d1
Další	další	k2eAgFnSc4d1
osobnosti	osobnost	k1gFnPc4
</s>
<s>
Karl	Karl	k1gMnSc1
Abraham	Abraham	k1gMnSc1
•	•	k?
Ernest	Ernest	k1gMnSc1
Jones	Jones	k1gMnSc1
•	•	k?
Otto	Otto	k1gMnSc1
Rank	rank	k1gInSc1
•	•	k?
Sándor	Sándor	k1gInSc4
Ferenczi	Ferencze	k1gFnSc4
•	•	k?
Theodor	Theodor	k1gMnSc1
Reik	Reik	k1gMnSc1
•	•	k?
Helene	Helen	k1gInSc5
Deutschová	Deutschová	k1gFnSc1
•	•	k?
Wilhelm	Wilhelm	k1gMnSc1
Reich	Reich	k?
•	•	k?
Edith	Edith	k1gInSc1
Jacobsonová	Jacobsonová	k1gFnSc1
•	•	k?
Herbert	Herbert	k1gMnSc1
Marcuse	Marcuse	k1gFnSc2
•	•	k?
Erich	Erich	k1gMnSc1
Fromm	Fromm	k1gMnSc1
•	•	k?
Hans	Hans	k1gMnSc1
Loewald	Loewald	k1gMnSc1
•	•	k?
Erik	Erik	k1gMnSc1
Erikson	Erikson	k1gMnSc1
•	•	k?
Slavoj	Slavoj	k1gMnSc1
Žižek	Žižek	k1gMnSc1
•	•	k?
Harry	Harra	k1gFnSc2
Stack	Stack	k1gMnSc1
Sullivan	Sullivan	k1gMnSc1
•	•	k?
Julia	Julius	k1gMnSc4
Kristeva	Kristev	k1gMnSc4
•	•	k?
Ronald	Ronald	k1gMnSc1
David	David	k1gMnSc1
Laing	Laing	k1gMnSc1
•	•	k?
Roy	Roy	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Schafer	Schafer	k1gMnSc1
•	•	k?
Heinz	Heinz	k1gMnSc1
Hartmann	Hartmann	k1gMnSc1
•	•	k?
Bruno	Bruno	k1gMnSc1
Bettelheim	Bettelheim	k1gMnSc1
•	•	k?
Janine	Janin	k1gInSc5
Chasseguet-Smirgelová	Chasseguet-Smirgelová	k1gFnSc1
•	•	k?
Judith	Juditha	k1gFnPc2
Butlerová	Butlerový	k2eAgFnSc1d1
•	•	k?
Nancy	Nancy	k1gFnSc1
Chodorowová	Chodorowová	k1gFnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Balint	Balint	k1gMnSc1
•	•	k?
Harry	Harra	k1gFnSc2
Guntrip	Guntrip	k1gMnSc1
•	•	k?
Joseph	Joseph	k1gMnSc1
Sandler	Sandler	k1gMnSc1
•	•	k?
Otto	Otto	k1gMnSc1
Kernberg	Kernberg	k1gMnSc1
•	•	k?
Ronald	Ronald	k1gMnSc1
Fairbairn	Fairbairn	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Bowlby	Bowlba	k1gFnSc2
•	•	k?
Clara	Clara	k1gFnSc1
Thompsonová	Thompsonová	k1gFnSc1
•	•	k?
Edgar	Edgar	k1gMnSc1
Levenson	Levenson	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Stern	sternum	k1gNnPc2
•	•	k?
Stephen	Stephen	k2eAgMnSc1d1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
René	René	k1gMnSc1
Spitz	Spitz	k1gMnSc1
•	•	k?
Wilfred	Wilfred	k1gMnSc1
Bion	Bion	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Green	Green	k2eAgMnSc1d1
•	•	k?
Heinz	Heinz	k1gMnSc1
Kohut	Kohut	k1gMnSc1
Ovlivněni	ovlivněn	k2eAgMnPc1d1
psychoanalýzou	psychoanalýza	k1gFnSc7
</s>
<s>
Bohuslav	Bohuslav	k1gMnSc1
Brouk	brouk	k1gMnSc1
•	•	k?
Záviš	Záviš	k1gMnSc1
Kalandra	Kalandr	k1gMnSc2
•	•	k?
Karel	Karel	k1gMnSc1
Teige	Teig	k1gFnSc2
•	•	k?
Vratislav	Vratislav	k1gMnSc1
Effenberger	Effenberger	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Pechar	Pechar	k1gMnSc1
•	•	k?
Zbyněk	Zbyněk	k1gMnSc1
Havlíček	Havlíček	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Švankmajer	Švankmajer	k1gMnSc1
•	•	k?
Gaston	Gaston	k1gInSc1
Bachelard	Bachelard	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Breton	Breton	k1gMnSc1
•	•	k?
Salvador	Salvador	k1gMnSc1
Dalí	Dalí	k1gMnSc1
•	•	k?
Roland	Roland	k1gInSc1
Barthes	Barthes	k1gMnSc1
•	•	k?
Gilles	Gilles	k1gMnSc1
Deleuze	Deleuze	k1gFnSc2
•	•	k?
Paul	Paul	k1gMnSc1
Ricoeur	Ricoeur	k1gMnSc1
•	•	k?
Géza	géz	k1gMnSc4
Róheim	Róheim	k1gMnSc1
Koncepty	koncept	k1gInPc4
a	a	k8xC
pojmy	pojem	k1gInPc4
</s>
<s>
Nevědomí	nevědomí	k1gNnSc4
•	•	k?
Předvědomí	předvědomý	k2eAgMnPc1d1
•	•	k?
Libido	libido	k1gNnSc4
•	•	k?
Ego	ego	k1gNnSc2
<g/>
,	,	kIx,
superego	superego	k1gMnSc1
a	a	k8xC
id	idy	k1gFnPc2
•	•	k?
Freudovské	freudovský	k2eAgNnSc4d1
přeřeknutí	přeřeknutí	k1gNnSc4
•	•	k?
Oidipovský	oidipovský	k2eAgInSc4d1
komplex	komplex	k1gInSc4
•	•	k?
Vytěsnění	vytěsnění	k1gNnSc2
•	•	k?
Narcismus	narcismus	k1gInSc4
•	•	k?
Volné	volný	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
•	•	k?
Psychoanalytické	psychoanalytický	k2eAgInPc4d1
směry	směr	k1gInPc4
•	•	k?
Pud	pud	k1gInSc4
života	život	k1gInSc2
a	a	k8xC
pud	pud	k1gInSc1
smrti	smrt	k1gFnSc2
•	•	k?
Zrcadlová	zrcadlový	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
•	•	k?
Přenos	přenos	k1gInSc1
•	•	k?
Archetyp	archetyp	k1gInSc4
•	•	k?
Bytostné	bytostný	k2eAgFnSc2d1
Já	já	k3xPp1nSc1
•	•	k?
Projektivní	projektivní	k2eAgFnSc1d1
identifikace	identifikace	k1gFnSc1
•	•	k?
Attachment	Attachment	k1gInSc1
(	(	kIx(
<g/>
citová	citový	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2009507994	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119237873	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2283	#num#	k4
3554	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
99035460	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
98486723	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
99035460	#num#	k4
</s>
