<s>
Judith	Judith	k1gInSc1
Butlerová	Butlerová	k1gFnSc1
</s>
<s>
Polemika	polemika	k1gFnSc1
s	s	k7c7
Freudem	Freud	k1gInSc7
</s>
<s>
Teze	teze	k1gFnPc1
o	o	k7c6
"	"	kIx"
<g/>
heterosexuálním	heterosexuální	k2eAgInSc6d1
modelu	model	k1gInSc6
<g/>
"	"	kIx"
vyvěrají	vyvěrat	k5eAaImIp3nP
z	z	k7c2
polemiky	polemika	k1gFnSc2
s	s	k7c7
učením	učení	k1gNnSc7
Sigmunda	Sigmund	k1gMnSc2
Freuda	Freud	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
centru	centrum	k1gNnSc6
filozofie	filozofie	k1gFnSc2
Butlerové	Butlerová	k1gFnSc2
<g/>
.	.	kIx.
</s>