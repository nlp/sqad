<p>
<s>
Jaekelopterus	Jaekelopterus	k1gInSc1	Jaekelopterus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
křídlo	křídlo	k1gNnSc1	křídlo
Otto	Otto	k1gMnSc1	Otto
von	von	k1gInSc4	von
Jaekela	Jaekel	k1gMnSc4	Jaekel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rod	rod	k1gInSc1	rod
obřích	obří	k2eAgMnPc2d1	obří
prvohorních	prvohorní	k2eAgMnPc2d1	prvohorní
členovců	členovec	k1gMnPc2	členovec
patřících	patřící	k2eAgFnPc2d1	patřící
mezi	mezi	k7c4	mezi
klepítkatce	klepítkatec	k1gMnPc4	klepítkatec
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
vodní	vodní	k2eAgMnPc1d1	vodní
predátoři	predátor	k1gMnPc1	predátor
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
období	období	k1gNnSc6	období
devonu	devon	k1gInSc2	devon
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
390	[number]	k4	390
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
při	při	k7c6	při
délce	délka	k1gFnSc6	délka
až	až	k9	až
přes	přes	k7c4	přes
2,5	[number]	k4	2,5
metru	metr	k1gInSc2	metr
představují	představovat	k5eAaImIp3nP	představovat
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
známých	známý	k2eAgMnPc2d1	známý
členovců	členovec	k1gMnPc2	členovec
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
"	"	kIx"	"
<g/>
mořští	mořský	k2eAgMnPc1d1	mořský
štíři	štír	k1gMnPc1	štír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
vývojové	vývojový	k2eAgFnSc2d1	vývojová
skupiny	skupina	k1gFnSc2	skupina
klepítkatců	klepítkatec	k1gMnPc2	klepítkatec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
–	–	k?	–
J.	J.	kA	J.
rhenaniae	rhenaniaat	k5eAaPmIp3nS	rhenaniaat
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
J.	J.	kA	J.
howelli	howelle	k1gFnSc4	howelle
z	z	k7c2	z
Wyomingu	Wyoming	k1gInSc2	Wyoming
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
velkému	velký	k2eAgInSc3d1	velký
geografickému	geografický	k2eAgInSc3d1	geografický
rozptylu	rozptyl	k1gInSc3	rozptyl
můžeme	moct	k5eAaImIp1nP	moct
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
skupinu	skupina	k1gFnSc4	skupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
druh	druh	k1gInSc1	druh
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
paleontologem	paleontolog	k1gMnSc7	paleontolog
Ottou	Otta	k1gMnSc7	Otta
von	von	k1gInSc4	von
Jaekelem	Jaekel	k1gInSc7	Jaekel
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Pterygotus	Pterygotus	k1gInSc1	Pterygotus
rhenaniae	rhenania	k1gInSc2	rhenania
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
pak	pak	k6eAd1	pak
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
o	o	k7c4	o
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
Waterson	Waterson	k1gNnSc4	Waterson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velikost	velikost	k1gFnSc1	velikost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
objevu	objev	k1gInSc2	objev
obřího	obří	k2eAgNnSc2d1	obří
klepeta	klepeto	k1gNnSc2	klepeto
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
46	[number]	k4	46
cm	cm	kA	cm
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
délka	délka	k1gFnSc1	délka
jednoho	jeden	k4xCgInSc2	jeden
německého	německý	k2eAgInSc2d1	německý
exempláře	exemplář	k1gInSc2	exemplář
asi	asi	k9	asi
na	na	k7c4	na
233	[number]	k4	233
až	až	k9	až
259	[number]	k4	259
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
se	s	k7c7	s
střední	střední	k2eAgFnSc7d1	střední
hodnotou	hodnota	k1gFnSc7	hodnota
246	[number]	k4	246
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
nataženými	natažený	k2eAgFnPc7d1	natažená
chelicerami	chelicera	k1gFnPc7	chelicera
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgMnSc1	tento
klepítkatec	klepítkatec	k1gMnSc1	klepítkatec
ještě	ještě	k9	ještě
asi	asi	k9	asi
o	o	k7c4	o
metr	metr	k1gInSc4	metr
delší	dlouhý	k2eAgInSc4d2	delší
<g/>
,	,	kIx,	,
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
tedy	tedy	k9	tedy
délky	délka	k1gFnSc2	délka
kolem	kolem	k7c2	kolem
3,5	[number]	k4	3,5
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
C.	C.	kA	C.
Lamsdell	Lamsdell	k1gMnSc1	Lamsdell
and	and	k?	and
Paul	Paul	k1gMnSc1	Paul
A.	A.	kA	A.
Selden	Seldna	k1gFnPc2	Seldna
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Babes	Babes	k1gInSc1	Babes
in	in	k?	in
the	the	k?	the
wood	wood	k1gInSc1	wood
–	–	k?	–
a	a	k8xC	a
unique	uniqu	k1gFnSc2	uniqu
window	window	k?	window
into	into	k1gMnSc1	into
sea	sea	k?	sea
scorpion	scorpion	k1gInSc1	scorpion
ontogeny	ontogen	k1gInPc1	ontogen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
BMC	BMC	kA	BMC
Evolutionary	Evolutionar	k1gInPc1	Evolutionar
Biology	biolog	k1gMnPc7	biolog
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
:	:	kIx,	:
98	[number]	k4	98
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
<g/>
10.118	[number]	k4	10.118
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1471	[number]	k4	1471
<g/>
-	-	kIx~	-
<g/>
2148	[number]	k4	2148
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Braddy	Braddy	k6eAd1	Braddy
<g/>
,	,	kIx,	,
Simon	Simon	k1gMnSc1	Simon
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Poschmann	Poschmann	k1gMnSc1	Poschmann
<g/>
,	,	kIx,	,
Markus	Markus	k1gMnSc1	Markus
<g/>
;	;	kIx,	;
Tetlie	Tetlie	k1gFnSc1	Tetlie
<g/>
,	,	kIx,	,
O.	O.	kA	O.
Erik	Erik	k1gMnSc1	Erik
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Giant	Giant	k1gInSc1	Giant
claw	claw	k?	claw
reveals	reveals	k1gInSc1	reveals
the	the	k?	the
largest	largest	k1gInSc1	largest
ever	ever	k1gMnSc1	ever
arthropod	arthropod	k1gInSc1	arthropod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Biology	biolog	k1gMnPc4	biolog
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
.	.	kIx.	.
4	[number]	k4	4
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
106	[number]	k4	106
<g/>
–	–	k?	–
<g/>
109	[number]	k4	109
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
<g/>
10.109	[number]	k4	10.109
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
rsbl	rsbnout	k5eAaPmAgInS	rsbnout
<g/>
.2007	.2007	k4	.2007
<g/>
.0491	.0491	k4	.0491
<g/>
.	.	kIx.	.
</s>
</p>
