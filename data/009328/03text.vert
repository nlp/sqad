<p>
<s>
Střapec	střapec	k1gInSc1	střapec
je	být	k5eAaImIp3nS	být
visící	visící	k2eAgInSc4d1	visící
pramen	pramen	k1gInSc4	pramen
šňůr	šňůra	k1gFnPc2	šňůra
nebo	nebo	k8xC	nebo
třásní	třáseň	k1gFnPc2	třáseň
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
svázaný	svázaný	k2eAgInSc1d1	svázaný
poutkem	poutko	k1gNnSc7	poutko
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
k	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
<g/>
Střapce	střapec	k1gInPc1	střapec
se	se	k3xPyFc4	se
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
řemeslnou	řemeslný	k2eAgFnSc7d1	řemeslná
výrobou	výroba	k1gFnSc7	výroba
střapců	střapec	k1gInPc2	střapec
začali	začít	k5eAaPmAgMnP	začít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
francouzští	francouzský	k2eAgMnPc1d1	francouzský
pozamentáři	pozamentář	k1gMnPc1	pozamentář
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
spojování	spojování	k1gNnSc2	spojování
šňůrek	šňůrka	k1gFnPc2	šňůrka
a	a	k8xC	a
třásní	třáseň	k1gFnPc2	třáseň
pozamentů	pozamentů	k?	pozamentů
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
umělecké	umělecký	k2eAgNnSc1d1	umělecké
řemeslo	řemeslo	k1gNnSc1	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sedmileté	sedmiletý	k2eAgFnSc6d1	sedmiletá
učební	učební	k2eAgFnSc6d1	učební
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
mohl	moct	k5eAaImAgMnS	moct
adept	adept	k1gMnSc1	adept
stát	stát	k5eAaPmF	stát
mistrem	mistr	k1gMnSc7	mistr
v	v	k7c6	v
cechu	cech	k1gInSc6	cech
pozamentářů	pozamentář	k1gMnPc2	pozamentář
<g/>
.	.	kIx.	.
<g/>
Pozamentáři	Pozamentář	k1gMnPc1	Pozamentář
dostávali	dostávat	k5eAaImAgMnP	dostávat
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
zakázky	zakázka	k1gFnSc2	zakázka
k	k	k7c3	k
výzdobě	výzdoba	k1gFnSc3	výzdoba
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
střapce	střapec	k1gInPc1	střapec
používat	používat	k5eAaImF	používat
jako	jako	k8xS	jako
doplňky	doplněk	k1gInPc4	doplněk
oděvů	oděv	k1gInPc2	oděv
a	a	k8xC	a
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
jiných	jiný	k2eAgInPc2d1	jiný
ozdobných	ozdobný	k2eAgInPc2d1	ozdobný
účelů	účel	k1gInPc2	účel
<g/>
.	.	kIx.	.
<g/>
Asi	asi	k9	asi
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
používají	používat	k5eAaImIp3nP	používat
střapce	střapec	k1gInPc1	střapec
často	často	k6eAd1	často
ženy	žena	k1gFnSc2	žena
na	na	k7c6	na
oděvech	oděv	k1gInPc6	oděv
(	(	kIx(	(
<g/>
i	i	k9	i
na	na	k7c6	na
nahé	nahý	k2eAgFnSc6d1	nahá
kůži	kůže	k1gFnSc6	kůže
<g/>
)	)	kIx)	)
při	při	k7c6	při
výstupech	výstup	k1gInPc6	výstup
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
revuích	revue	k1gFnPc6	revue
<g/>
,	,	kIx,	,
burleskách	burleska	k1gFnPc6	burleska
nebo	nebo	k8xC	nebo
manifestacích	manifestace	k1gFnPc6	manifestace
<g/>
.	.	kIx.	.
<g/>
Střapec	střapec	k1gInSc4	střapec
je	být	k5eAaImIp3nS	být
častou	častý	k2eAgFnSc7d1	častá
součástí	součást	k1gFnSc7	součást
erbů	erb	k1gInPc2	erb
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
heraldice	heraldika	k1gFnSc6	heraldika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Pozament	Pozament	k?	Pozament
</s>
</p>
<p>
<s>
Třásně	třáseň	k1gFnPc1	třáseň
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Střapec	střapec	k1gInSc1	střapec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
