<p>
<s>
Elektron	elektron	k1gInSc1	elektron
je	být	k5eAaImIp3nS	být
subatomární	subatomární	k2eAgFnSc1d1	subatomární
částice	částice	k1gFnSc1	částice
se	s	k7c7	s
záporným	záporný	k2eAgInSc7d1	záporný
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
tvoří	tvořit	k5eAaImIp3nP	tvořit
obal	obal	k1gInSc4	obal
atomu	atom	k1gInSc2	atom
kolem	kolem	k7c2	kolem
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
nositeli	nositel	k1gMnPc7	nositel
náboje	náboj	k1gInSc2	náboj
při	při	k7c6	při
vedení	vedení	k1gNnSc6	vedení
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
kovech	kov	k1gInPc6	kov
<g/>
,	,	kIx,	,
polovodičích	polovodič	k1gInPc6	polovodič
(	(	kIx(	(
<g/>
majoritní	majoritní	k2eAgFnPc4d1	majoritní
v	v	k7c6	v
typu	typ	k1gInSc6	typ
N	N	kA	N
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
elektrických	elektrický	k2eAgInPc6d1	elektrický
výbojích	výboj	k1gInPc6	výboj
v	v	k7c6	v
plynech	plyn	k1gInPc6	plyn
i	i	k8xC	i
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
katodové	katodový	k2eAgNnSc1d1	katodové
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
záření	záření	k1gNnSc1	záření
beta	beta	k1gNnSc1	beta
(	(	kIx(	(
<g/>
β	β	k?	β
<g/>
–	–	k?	–
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
elektrony	elektron	k1gInPc5	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektron	elektron	k1gInSc1	elektron
jakožto	jakožto	k8xS	jakožto
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
leptony	lepton	k1gInPc7	lepton
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mezi	mezi	k7c4	mezi
částice	částice	k1gFnPc4	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
silné	silný	k2eAgFnPc1d1	silná
interakce	interakce	k1gFnPc1	interakce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
elektromagnetické	elektromagnetický	k2eAgFnPc4d1	elektromagnetická
a	a	k8xC	a
slabé	slabý	k2eAgFnPc4d1	slabá
interakce	interakce	k1gFnPc4	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
polovinový	polovinový	k2eAgInSc4d1	polovinový
spin	spin	k1gInSc4	spin
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
fermion	fermion	k1gInSc4	fermion
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
Fermiho-Diracovou	Fermiho-Diracův	k2eAgFnSc7d1	Fermiho-Diracův
statistikou	statistika	k1gFnSc7	statistika
-	-	kIx~	-
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
Pauliho	Pauli	k1gMnSc4	Pauli
vylučovací	vylučovací	k2eAgInSc1d1	vylučovací
princip	princip	k1gInSc1	princip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
elektron	elektron	k1gInSc1	elektron
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
jantar	jantar	k1gInSc1	jantar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ή	ή	k?	ή
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zavedl	zavést	k5eAaPmAgMnS	zavést
William	William	k1gInSc4	William
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgInPc4d1	elektrický
jevy	jev	k1gInPc4	jev
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
Thales	Thales	k1gMnSc1	Thales
Milétský	Milétský	k2eAgMnSc1d1	Milétský
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
jantarového	jantarový	k2eAgInSc2d1	jantarový
nástroje	nástroj	k1gInSc2	nástroj
<g/>
,	,	kIx,	,
užívaného	užívaný	k2eAgInSc2d1	užívaný
při	při	k7c6	při
předení	předení	k1gNnSc6	předení
lnu	len	k1gInSc2	len
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc4d1	základní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
elektronů	elektron	k1gInPc2	elektron
==	==	k?	==
</s>
</p>
<p>
<s>
symbol	symbol	k1gInSc1	symbol
<g/>
:	:	kIx,	:
e	e	k0	e
<g/>
–	–	k?	–
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
e	e	k0	e
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
elementární	elementární	k2eAgFnSc4d1	elementární
částici	částice	k1gFnSc4	částice
<g/>
,	,	kIx,	,
lepton	lepton	k1gInSc4	lepton
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
leptonů	lepton	k1gInPc2	lepton
</s>
</p>
<p>
<s>
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
m	m	kA	m
<g/>
0	[number]	k4	0
=	=	kIx~	=
9,109	[number]	k4	9,109
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
31	[number]	k4	31
kg	kg	kA	kg
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
:	:	kIx,	:
q	q	k?	q
=	=	kIx~	=
–	–	k?	–
e	e	k0	e
=	=	kIx~	=
–	–	k?	–
1,602	[number]	k4	1,602
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
C	C	kA	C
(	(	kIx(	(
<g/>
záporný	záporný	k2eAgInSc4d1	záporný
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
elektrický	elektrický	k2eAgInSc4d1	elektrický
dipólový	dipólový	k2eAgInSc4d1	dipólový
moment	moment	k1gInSc4	moment
<g/>
:	:	kIx,	:
|	|	kIx~	|
<g/>
d	d	k?	d
<g/>
|	|	kIx~	|
<	<	kIx(	<
1,1	[number]	k4	1,1
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
29	[number]	k4	29
e	e	k0	e
m	m	kA	m
</s>
</p>
<p>
<s>
magnetický	magnetický	k2eAgInSc1d1	magnetický
dipólový	dipólový	k2eAgInSc1d1	dipólový
moment	moment	k1gInSc1	moment
<g/>
:	:	kIx,	:
μ	μ	k?	μ
=	=	kIx~	=
–	–	k?	–
928,5	[number]	k4	928,5
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
26	[number]	k4	26
JT	JT	kA	JT
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
jeden	jeden	k4xCgInSc1	jeden
záporný	záporný	k2eAgInSc1d1	záporný
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
magneton	magneton	k1gInSc1	magneton
</s>
</p>
<p>
<s>
spin	spin	k1gInSc1	spin
<g/>
:	:	kIx,	:
s	s	k7c7	s
=	=	kIx~	=
1⁄	1⁄	k?	1⁄
<g/>
,	,	kIx,	,
elektron	elektron	k1gInSc1	elektron
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
fermion	fermion	k1gInSc1	fermion
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc1d1	střední
doba	doba	k1gFnSc1	doba
života	život	k1gInSc2	život
<g/>
:	:	kIx,	:
τ	τ	k?	τ
>	>	kIx)	>
4,6	[number]	k4	4,6
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
stabilní	stabilní	k2eAgFnSc4d1	stabilní
částici	částice	k1gFnSc4	částice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
antičástice	antičástice	k1gFnSc1	antičástice
<g/>
:	:	kIx,	:
pozitron	pozitron	k1gInSc1	pozitron
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
sjednocení	sjednocení	k1gNnSc4	sjednocení
názvů	název	k1gInPc2	název
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
elektron	elektron	k1gInSc4	elektron
na	na	k7c4	na
negatron	negatron	k1gInSc4	negatron
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hypotetickým	hypotetický	k2eAgMnSc7d1	hypotetický
supersymetrickým	supersymetrický	k2eAgMnSc7d1	supersymetrický
partnerem	partner	k1gMnSc7	partner
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
selektron	selektron	k1gInSc1	selektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektron	elektron	k1gInSc4	elektron
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
==	==	k?	==
</s>
</p>
<p>
<s>
Elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
protony	proton	k1gInPc7	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc7	neutron
tvořícími	tvořící	k2eAgInPc7d1	tvořící
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
<g/>
)	)	kIx)	)
základními	základní	k2eAgFnPc7d1	základní
stavebními	stavební	k2eAgFnPc7d1	stavební
částicemi	částice	k1gFnPc7	částice
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tvoří	tvořit	k5eAaImIp3nP	tvořit
elektronový	elektronový	k2eAgInSc4d1	elektronový
obal	obal	k1gInSc4	obal
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
atomu	atom	k1gInSc2	atom
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
tvořené	tvořený	k2eAgFnPc4d1	tvořená
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
na	na	k7c4	na
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
zářivé	zářivý	k2eAgFnPc4d1	zářivá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
vyzařované	vyzařovaný	k2eAgFnPc4d1	vyzařovaná
i	i	k8xC	i
absorpční	absorpční	k2eAgNnSc1d1	absorpční
spektrum	spektrum	k1gNnSc1	spektrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
jádra	jádro	k1gNnSc2	jádro
-	-	kIx~	-
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
-	-	kIx~	-
se	se	k3xPyFc4	se
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
atomu	atom	k1gInSc6	atom
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	-li	k?	-li
k	k	k7c3	k
odtržení	odtržení	k1gNnSc3	odtržení
nebo	nebo	k8xC	nebo
přidání	přidání	k1gNnSc3	přidání
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
atomu	atom	k1gInSc2	atom
iont	iont	k1gInSc1	iont
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
lze	lze	k6eAd1	lze
chování	chování	k1gNnSc1	chování
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
atomovém	atomový	k2eAgInSc6d1	atomový
obalu	obal	k1gInSc6	obal
dobře	dobře	k6eAd1	dobře
popisovat	popisovat	k5eAaImF	popisovat
a	a	k8xC	a
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Názornějších	názorný	k2eAgFnPc2d2	názornější
zjednodušujících	zjednodušující	k2eAgFnPc2d1	zjednodušující
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
popisu	popis	k1gInSc2	popis
blízkého	blízký	k2eAgNnSc2d1	blízké
Schrödingerově	Schrödingerův	k2eAgInSc6d1	Schrödingerův
obrazu	obraz	k1gInSc6	obraz
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vlnové	vlnový	k2eAgFnPc4d1	vlnová
mechaniky	mechanika	k1gFnPc4	mechanika
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
elektrony	elektron	k1gInPc1	elektron
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
orbitalech	orbital	k1gInPc6	orbital
daných	daný	k2eAgInPc6d1	daný
elektronovou	elektronový	k2eAgFnSc7d1	elektronová
konfigurací	konfigurace	k1gFnSc7	konfigurace
každého	každý	k3xTgInSc2	každý
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
orbitaly	orbital	k1gInPc1	orbital
neurčují	určovat	k5eNaImIp3nP	určovat
přesně	přesně	k6eAd1	přesně
polohu	poloha	k1gFnSc4	poloha
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
největší	veliký	k2eAgFnSc1d3	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
jeho	on	k3xPp3gInSc2	on
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
reakcích	reakce	k1gFnPc6	reakce
se	se	k3xPyFc4	se
též	též	k9	též
používá	používat	k5eAaImIp3nS	používat
představa	představa	k1gFnSc1	představa
o	o	k7c4	o
uspořádání	uspořádání	k1gNnSc4	uspořádání
elektronů	elektron	k1gInPc2	elektron
do	do	k7c2	do
slupek	slupka	k1gFnPc2	slupka
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
podle	podle	k7c2	podle
elektronové	elektronový	k2eAgFnSc2d1	elektronová
konfigurace	konfigurace	k1gFnSc2	konfigurace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
chemické	chemický	k2eAgFnSc2d1	chemická
vazby	vazba	k1gFnSc2	vazba
účastní	účastnit	k5eAaImIp3nS	účastnit
pouze	pouze	k6eAd1	pouze
poslední	poslední	k2eAgFnSc1d1	poslední
slupka	slupka	k1gFnSc1	slupka
(	(	kIx(	(
<g/>
valenční	valenční	k2eAgFnSc1d1	valenční
slupka	slupka	k1gFnSc1	slupka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přechody	přechod	k1gInPc1	přechod
elektronů	elektron	k1gInPc2	elektron
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
energetickými	energetický	k2eAgFnPc7d1	energetická
hladinami	hladina	k1gFnPc7	hladina
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
jsou	být	k5eAaImIp3nP	být
doprovázeny	doprovázen	k2eAgFnPc1d1	doprovázena
emisí	emise	k1gFnSc7	emise
nebo	nebo	k8xC	nebo
absorpcí	absorpce	k1gFnPc2	absorpce
fotonů	foton	k1gInPc2	foton
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
základních	základní	k2eAgFnPc2d1	základní
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
charakteristik	charakteristika	k1gFnPc2	charakteristika
vodíku	vodík	k1gInSc2	vodík
podobných	podobný	k2eAgInPc2d1	podobný
atomů	atom	k1gInPc2	atom
postačuje	postačovat	k5eAaImIp3nS	postačovat
zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
k	k	k7c3	k
vysvětlení	vysvětlení	k1gNnSc3	vysvětlení
vlastností	vlastnost	k1gFnPc2	vlastnost
spekter	spektrum	k1gNnPc2	spektrum
atomů	atom	k1gInPc2	atom
se	s	k7c7	s
složitějším	složitý	k2eAgInSc7d2	složitější
obalem	obal	k1gInSc7	obal
a	a	k8xC	a
změny	změna	k1gFnPc4	změna
spekter	spektrum	k1gNnPc2	spektrum
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
již	již	k9	již
potřeba	potřeba	k1gFnSc1	potřeba
Schrödingerova	Schrödingerův	k2eAgInSc2d1	Schrödingerův
kvantově-mechanického	kvantověechanický	k2eAgInSc2d1	kvantově-mechanický
popisu	popis	k1gInSc2	popis
a	a	k8xC	a
započtení	započtení	k1gNnSc2	započtení
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
kvantové	kvantový	k2eAgFnSc2d1	kvantová
interakce	interakce	k1gFnSc2	interakce
spinů	spin	k1gInPc2	spin
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
jemná	jemný	k2eAgFnSc1d1	jemná
a	a	k8xC	a
hyperjemná	hyperjemný	k2eAgFnSc1d1	hyperjemná
struktura	struktura	k1gFnSc1	struktura
spektra	spektrum	k1gNnSc2	spektrum
již	již	k6eAd1	již
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
relativistický	relativistický	k2eAgInSc4d1	relativistický
Diracův	Diracův	k2eAgInSc4d1	Diracův
popis	popis	k1gInSc4	popis
a	a	k8xC	a
započtení	započtení	k1gNnSc4	započtení
kvantové	kvantový	k2eAgFnSc2d1	kvantová
interakce	interakce	k1gFnSc2	interakce
se	se	k3xPyFc4	se
spinem	spin	k1gInSc7	spin
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Elektronová	elektronový	k2eAgFnSc1d1	elektronová
konfigurace	konfigurace	k1gFnSc1	konfigurace
===	===	k?	===
</s>
</p>
<p>
<s>
Stav	stav	k1gInSc1	stav
elektronu	elektron	k1gInSc2	elektron
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
elektronovou	elektronový	k2eAgFnSc7d1	elektronová
konfigurací	konfigurace	k1gFnSc7	konfigurace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
několika	několik	k4yIc7	několik
kvantovými	kvantový	k2eAgNnPc7d1	kvantové
čísly	číslo	k1gNnPc7	číslo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
kvantové	kvantový	k2eAgNnSc1d1	kvantové
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
energií	energie	k1gFnSc7	energie
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
...	...	k?	...
</s>
</p>
<p>
<s>
Vedlejší	vedlejší	k2eAgNnSc1d1	vedlejší
kvantové	kvantový	k2eAgNnSc1d1	kvantové
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
orbitálním	orbitální	k2eAgInSc7d1	orbitální
momentem	moment	k1gInSc7	moment
hybnosti	hybnost	k1gFnSc2	hybnost
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
l	l	kA	l
=	=	kIx~	=
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
n-	n-	k?	n-
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
kvantové	kvantový	k2eAgNnSc1d1	kvantové
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
orbitálním	orbitální	k2eAgInSc7d1	orbitální
magnetickým	magnetický	k2eAgInSc7d1	magnetický
momentem	moment	k1gInSc7	moment
hybnosti	hybnost	k1gFnSc2	hybnost
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
m	m	kA	m
=	=	kIx~	=
-l	-l	k?	-l
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
-1	-1	k4	-1
<g/>
,	,	kIx,	,
0	[number]	k4	0
,1	,1	k4	,1
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
l	l	kA	l
</s>
</p>
<p>
<s>
Spin	spin	k1gInSc1	spin
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
spinovým	spinův	k2eAgInSc7d1	spinův
momentem	moment	k1gInSc7	moment
hybnosti	hybnost	k1gFnSc2	hybnost
ms	ms	k?	ms
=	=	kIx~	=
+	+	kIx~	+
<g/>
1⁄	1⁄	k?	1⁄
nebo	nebo	k8xC	nebo
-1⁄	-1⁄	k?	-1⁄
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Elektron	elektron	k1gInSc1	elektron
jako	jako	k8xC	jako
částice	částice	k1gFnPc1	částice
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
J.	J.	kA	J.
J.	J.	kA	J.
Thomsonem	Thomson	k1gMnSc7	Thomson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
přenášení	přenášení	k1gNnSc1	přenášení
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
vysvětlovalo	vysvětlovat	k5eAaImAgNnS	vysvětlovat
pomocí	pomocí	k7c2	pomocí
přelévání	přelévání	k1gNnSc2	přelévání
elektrického	elektrický	k2eAgNnSc2d1	elektrické
fluida	fluidum	k1gNnSc2	fluidum
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
J.	J.	kA	J.
Thomson	Thomson	k1gMnSc1	Thomson
prováděl	provádět	k5eAaImAgMnS	provádět
pokus	pokus	k1gInSc4	pokus
s	s	k7c7	s
katodovou	katodový	k2eAgFnSc7d1	katodová
trubicí	trubice	k1gFnSc7	trubice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
částice	částice	k1gFnSc2	částice
emitované	emitovaný	k2eAgFnSc6d1	emitovaná
ze	z	k7c2	z
žhavícího	žhavící	k2eAgNnSc2d1	žhavící
vlákna	vlákno	k1gNnSc2	vlákno
procházely	procházet	k5eAaImAgFnP	procházet
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
a	a	k8xC	a
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
těmito	tento	k3xDgFnPc7	tento
poli	pole	k1gNnSc6	pole
vychylovány	vychylován	k2eAgInPc1d1	vychylován
<g/>
.	.	kIx.	.
</s>
<s>
Thomson	Thomson	k1gInSc1	Thomson
z	z	k7c2	z
výchylky	výchylka	k1gFnSc2	výchylka
určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
dopadající	dopadající	k2eAgFnPc1d1	dopadající
na	na	k7c4	na
stínítko	stínítko	k1gNnSc4	stínítko
mají	mít	k5eAaImIp3nP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
asi	asi	k9	asi
1000	[number]	k4	1000
<g/>
krát	krát	k6eAd1	krát
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
částice	částice	k1gFnPc4	částice
vyskytující	vyskytující	k2eAgFnPc4d1	vyskytující
se	se	k3xPyFc4	se
uvnitř	uvnitř	k6eAd1	uvnitř
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
a	a	k8xC	a
nazval	nazvat	k5eAaBmAgMnS	nazvat
je	on	k3xPp3gFnPc4	on
korpuskule	korpuskule	k1gFnPc4	korpuskule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
elektron	elektron	k1gInSc4	elektron
splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
objevy	objev	k1gInPc4	objev
dalších	další	k2eAgFnPc2d1	další
subatomárních	subatomární	k2eAgFnPc2d1	subatomární
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
představy	představa	k1gFnPc1	představa
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
atomu	atom	k1gInSc2	atom
(	(	kIx(	(
<g/>
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
též	též	k9	též
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
–	–	k?	–
od	od	k7c2	od
chaotického	chaotický	k2eAgNnSc2d1	chaotické
rozmístění	rozmístění	k1gNnSc2	rozmístění
ve	v	k7c6	v
zbylé	zbylý	k2eAgFnSc6d1	zbylá
kladné	kladný	k2eAgFnSc6d1	kladná
hmotě	hmota	k1gFnSc6	hmota
(	(	kIx(	(
<g/>
pudinkový	pudinkový	k2eAgInSc1d1	pudinkový
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
oběhy	oběh	k1gInPc4	oběh
kolem	kolem	k7c2	kolem
jádra	jádro	k1gNnSc2	jádro
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
planety	planeta	k1gFnPc1	planeta
kolem	kolem	k7c2	kolem
<g />
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
planetární	planetární	k2eAgInSc1d1	planetární
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
kruhové	kruhový	k2eAgFnPc4d1	kruhová
dráhy	dráha	k1gFnPc4	dráha
(	(	kIx(	(
<g/>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
a	a	k8xC	a
složité	složitý	k2eAgFnSc2d1	složitá
stáčející	stáčející	k2eAgFnSc2d1	stáčející
se	se	k3xPyFc4	se
eliptické	eliptický	k2eAgFnSc2d1	eliptická
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
Sommerfeldův	Sommerfeldův	k2eAgInSc1d1	Sommerfeldův
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
pravděpodobnostní	pravděpodobnostní	k2eAgInPc4d1	pravděpodobnostní
výskyty	výskyt	k1gInPc4	výskyt
v	v	k7c6	v
orbitalech	orbital	k1gInPc6	orbital
(	(	kIx(	(
<g/>
Erwin	Erwin	k1gMnSc1	Erwin
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Born	Born	k1gMnSc1	Born
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gFnSc1	Dirac
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Pozitron	pozitron	k1gInSc1	pozitron
</s>
</p>
<p>
<s>
Neutrino	neutrino	k1gNnSc1	neutrino
</s>
</p>
<p>
<s>
Elektromagnetismus	elektromagnetismus	k1gInSc1	elektromagnetismus
</s>
</p>
<p>
<s>
Leptonové	Leptonový	k2eAgNnSc1d1	Leptonové
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
elektron	elektron	k1gInSc1	elektron
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
elektron	elektron	k1gInSc1	elektron
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
