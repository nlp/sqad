<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Tirana	Tirana	k1gFnSc1	Tirana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
895	[number]	k4	895
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
finančním	finanční	k2eAgMnSc7d1	finanční
centrem	centr	k1gMnSc7	centr
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
