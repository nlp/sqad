<p>
<s>
Potok	potok	k1gInSc1	potok
Jasenka	Jasenka	k1gFnSc1	Jasenka
je	být	k5eAaImIp3nS	být
pravostranným	pravostranný	k2eAgInSc7d1	pravostranný
přítokem	přítok	k1gInSc7	přítok
Vsetínské	vsetínský	k2eAgFnSc2d1	vsetínská
Bečvy	Bečva	k1gFnSc2	Bečva
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
6,1	[number]	k4	6,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
8,6	[number]	k4	8,6
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Potok	potok	k1gInSc1	potok
pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
Vsetínských	vsetínský	k2eAgInPc6d1	vsetínský
vrších	vrch	k1gInPc6	vrch
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
vrchu	vrch	k1gInSc2	vrch
Dušná	dušný	k2eAgFnSc1d1	dušná
(	(	kIx(	(
<g/>
730	[number]	k4	730
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protéká	protékat	k5eAaImIp3nS	protékat
údolím	údolí	k1gNnSc7	údolí
Horní	horní	k2eAgFnSc1d1	horní
Jasenka	Jasenka	k1gFnSc1	Jasenka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Dolní	dolní	k2eAgNnSc1d1	dolní
Jasenkou	Jasenka	k1gFnSc7	Jasenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgFnSc7d1	místní
částí	část	k1gFnSc7	část
Vsetína	Vsetín	k1gInSc2	Vsetín
s	s	k7c7	s
vilovou	vilový	k2eAgFnSc7d1	vilová
zástavbou	zástavba	k1gFnSc7	zástavba
a	a	k8xC	a
sídlištěm	sídliště	k1gNnSc7	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mostu	most	k1gInSc2	most
spojujícího	spojující	k2eAgInSc2d1	spojující
dolní	dolní	k2eAgInPc4d1	dolní
a	a	k8xC	a
horní	horní	k2eAgNnSc4d1	horní
město	město	k1gNnSc4	město
se	se	k3xPyFc4	se
potok	potok	k1gInSc1	potok
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Vsetínské	vsetínský	k2eAgFnSc2d1	vsetínská
Bečvy	Bečva	k1gFnSc2	Bečva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jasenka	Jasenka	k1gFnSc1	Jasenka
(	(	kIx(	(
<g/>
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
