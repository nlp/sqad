<s>
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
i	i	k9	i
po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
školu	škola	k1gFnSc4	škola
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
výborného	výborný	k2eAgInSc2d1	výborný
prospěchu	prospěch	k1gInSc2	prospěch
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
obdržel	obdržet	k5eAaPmAgInS	obdržet
jednorázové	jednorázový	k2eAgNnSc4d1	jednorázové
Telekiho	Telekiha	k1gFnSc5	Telekiha
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zde	zde	k6eAd1	zde
poznal	poznat	k5eAaPmAgMnS	poznat
i	i	k8xC	i
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
Emílii	Emílie	k1gFnSc4	Emílie
Chovanovou	Chovanův	k2eAgFnSc7d1	Chovanův
<g/>
.	.	kIx.	.
</s>
