<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Królestwo	Królestwo	k6eAd1	Królestwo
Polskie	Polskie	k1gFnPc4	Polskie
též	též	k6eAd1	též
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Rzeczpospolita	Rzeczpospolit	k2eAgNnPc1d1	Rzeczpospolit
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Königreich	Königreich	k1gInSc1	Königreich
Polen	poleno	k1gNnPc2	poleno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
<g/>
,	,	kIx,	,
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
přestávkami	přestávka	k1gFnPc7	přestávka
a	a	k8xC	a
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
částmi	část	k1gFnPc7	část
území	území	k1gNnSc2	území
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1025	[number]	k4	1025
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgInPc2d3	nejmocnější
států	stát	k1gInPc2	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
paradoxně	paradoxně	k6eAd1	paradoxně
se	se	k3xPyFc4	se
o	o	k7c4	o
pár	pár	k4xCyI	pár
století	století	k1gNnSc2	století
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
kořistí	kořist	k1gFnPc2	kořist
pro	pro	k7c4	pro
sousední	sousední	k2eAgFnPc4d1	sousední
mocnosti	mocnost	k1gFnPc4	mocnost
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1025	[number]	k4	1025
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
polský	polský	k2eAgMnSc1d1	polský
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
korunován	korunován	k2eAgInSc4d1	korunován
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Druhá	druhý	k4xOgFnSc1	druhý
polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historická	historický	k2eAgNnPc1d1	historické
období	období	k1gNnPc1	období
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
období	období	k1gNnPc1	období
polského	polský	k2eAgNnSc2d1	polské
království	království	k1gNnSc2	království
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1025	[number]	k4	1025
<g/>
–	–	k?	–
<g/>
1385	[number]	k4	1385
<g/>
)	)	kIx)	)
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Piastovců	Piastovec	k1gMnPc2	Piastovec
<g/>
,	,	kIx,	,
počátek	počátek	k1gInSc4	počátek
království	království	k1gNnSc2	království
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1385	[number]	k4	1385
<g/>
–	–	k?	–
<g/>
1569	[number]	k4	1569
<g/>
)	)	kIx)	)
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jagellonců	Jagellonec	k1gInPc2	Jagellonec
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1569	[number]	k4	1569
<g/>
–	–	k?	–
<g/>
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
–	–	k?	–
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
Republiky	republika	k1gFnSc2	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
</s>
</p>
<p>
<s>
Kongresové	kongresový	k2eAgNnSc1d1	Kongresové
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1831	[number]	k4	1831
<g/>
/	/	kIx~	/
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
–	–	k?	–
tzv.	tzv.	kA	tzv.
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
v	v	k7c6	v
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
–	–	k?	–
vazalský	vazalský	k2eAgInSc4d1	vazalský
stát	stát	k1gInSc4	stát
Ústředních	ústřední	k2eAgFnPc2d1	ústřední
mocností	mocnost	k1gFnPc2	mocnost
</s>
</p>
<p>
<s>
==	==	k?	==
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Piastovců	Piastovec	k1gMnPc2	Piastovec
a	a	k8xC	a
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
historicky	historicky	k6eAd1	historicky
doloženým	doložený	k2eAgMnSc7d1	doložený
polským	polský	k2eAgMnSc7d1	polský
knížetem	kníže	k1gMnSc7	kníže
byl	být	k5eAaImAgMnS	být
Měšek	Měšek	k1gMnSc1	Měšek
I.	I.	kA	I.
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Piastovců	Piastovec	k1gInPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
<s>
Měškův	Měškův	k2eAgMnSc1d1	Měškův
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
(	(	kIx(	(
<g/>
992	[number]	k4	992
<g/>
–	–	k?	–
<g/>
1025	[number]	k4	1025
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úspěšně	úspěšně	k6eAd1	úspěšně
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
území	území	k1gNnSc2	území
rodícího	rodící	k2eAgMnSc2d1	rodící
se	se	k3xPyFc4	se
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
upevnění	upevnění	k1gNnSc4	upevnění
knížecí	knížecí	k2eAgFnSc2d1	knížecí
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
povznesení	povznesení	k1gNnSc2	povznesení
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
prestiže	prestiž	k1gFnSc2	prestiž
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
jeho	jeho	k3xOp3gFnPc1	jeho
vlády	vláda	k1gFnPc1	vláda
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
několik	několik	k4yIc4	několik
okolností	okolnost	k1gFnPc2	okolnost
<g/>
:	:	kIx,	:
oslabení	oslabení	k1gNnSc2	oslabení
vlády	vláda	k1gFnSc2	vláda
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
univerzalistické	univerzalistický	k2eAgInPc1d1	univerzalistický
plány	plán	k1gInPc1	plán
císaře	císař	k1gMnSc2	císař
Oty	Ota	k1gMnSc2	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
také	také	k9	také
mučednická	mučednický	k2eAgFnSc1d1	mučednická
smrt	smrt	k1gFnSc1	smrt
pražského	pražský	k2eAgMnSc2d1	pražský
biskupa	biskup	k1gMnSc2	biskup
Slavníkovce	Slavníkovec	k1gMnSc2	Slavníkovec
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
během	během	k7c2	během
misie	misie	k1gFnSc2	misie
v	v	k7c6	v
Prusích	Prusích	k?	Prusích
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
vykoupil	vykoupit	k5eAaPmAgInS	vykoupit
<g/>
,	,	kIx,	,
převezl	převézt	k5eAaPmAgInS	převézt
do	do	k7c2	do
Hnězdna	Hnězdno	k1gNnSc2	Hnězdno
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
zde	zde	k6eAd1	zde
jeho	jeho	k3xOp3gInSc1	jeho
kult	kult	k1gInSc1	kult
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgInSc4d1	mladý
stát	stát	k1gInSc4	stát
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgMnS	získat
vlastního	vlastní	k2eAgMnSc4d1	vlastní
mučedníka	mučedník	k1gMnSc4	mučedník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
již	již	k9	již
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
svatořečen	svatořečen	k2eAgMnSc1d1	svatořečen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přispělo	přispět	k5eAaPmAgNnS	přispět
ke	k	k7c3	k
sblížení	sblížení	k1gNnSc3	sblížení
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Otou	Ota	k1gMnSc7	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
Vojtěchovým	Vojtěchův	k2eAgMnSc7d1	Vojtěchův
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
kanonizaci	kanonizace	k1gFnSc3	kanonizace
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zřídil	zřídit	k5eAaPmAgMnS	zřídit
papež	papež	k1gMnSc1	papež
Silvestr	Silvestr	k1gMnSc1	Silvestr
II	II	kA	II
<g/>
.	.	kIx.	.
nad	nad	k7c7	nad
Vojtěchovým	Vojtěchův	k2eAgInSc7d1	Vojtěchův
hrobem	hrob	k1gInSc7	hrob
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
a	a	k8xC	a
podřídil	podřídit	k5eAaPmAgMnS	podřídit
mu	on	k3xPp3gMnSc3	on
nově	nově	k6eAd1	nově
ustavená	ustavený	k2eAgNnPc4d1	ustavené
biskupství	biskupství	k1gNnPc4	biskupství
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
Kolobřehu	Kolobřeh	k1gInSc6	Kolobřeh
a	a	k8xC	a
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
signalizovalo	signalizovat	k5eAaImAgNnS	signalizovat
příslušnost	příslušnost	k1gFnSc4	příslušnost
Krakovska	Krakovsko	k1gNnSc2	Krakovsko
<g/>
,	,	kIx,	,
Pomořan	Pomořany	k1gInPc2	Pomořany
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
k	k	k7c3	k
polskému	polský	k2eAgInSc3d1	polský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poznaňské	poznaňský	k2eAgNnSc4d1	poznaňské
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
dosud	dosud	k6eAd1	dosud
misijní	misijní	k2eAgInSc4d1	misijní
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
podřízeno	podřízen	k2eAgNnSc1d1	podřízeno
papežství	papežství	k1gNnSc1	papežství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
vymezena	vymezen	k2eAgFnSc1d1	vymezena
sféra	sféra	k1gFnSc1	sféra
působnosti	působnost	k1gFnSc2	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
hnězdenským	hnězdenský	k2eAgMnSc7d1	hnězdenský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vojtěchův	Vojtěchův	k2eAgMnSc1d1	Vojtěchův
polorodý	polorodý	k2eAgMnSc1d1	polorodý
bratr	bratr	k1gMnSc1	bratr
Radim	Radim	k1gMnSc1	Radim
(	(	kIx(	(
<g/>
latinským	latinský	k2eAgNnSc7d1	latinské
jménem	jméno	k1gNnSc7	jméno
Gaudentius	Gaudentius	k1gMnSc1	Gaudentius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
ho	on	k3xPp3gNnSc4	on
uvedl	uvést	k5eAaPmAgMnS	uvést
samotný	samotný	k2eAgMnSc1d1	samotný
císař	císař	k1gMnSc1	císař
<g/>
,	,	kIx,	,
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
podnikl	podniknout	k5eAaPmAgMnS	podniknout
pouť	pouť	k1gFnSc4	pouť
k	k	k7c3	k
hrobu	hrob	k1gInSc3	hrob
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
investitury	investitura	k1gFnSc2	investitura
pro	pro	k7c4	pro
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
i	i	k8xC	i
nová	nový	k2eAgNnPc4d1	nové
biskupství	biskupství	k1gNnPc4	biskupství
udělil	udělit	k5eAaPmAgMnS	udělit
Boleslavovi	Boleslav	k1gMnSc3	Boleslav
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
nástupcům	nástupce	k1gMnPc3	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Upevnění	upevnění	k1gNnSc4	upevnění
moci	moct	k5eAaImF	moct
za	za	k7c2	za
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
návštěvě	návštěva	k1gFnSc6	návštěva
se	se	k3xPyFc4	se
Ota	Ota	k1gMnSc1	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
okázalého	okázalý	k2eAgInSc2d1	okázalý
sjezdu	sjezd	k1gInSc2	sjezd
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
řešil	řešit	k5eAaImAgInS	řešit
politické	politický	k2eAgFnPc4d1	politická
i	i	k8xC	i
církevní	církevní	k2eAgFnPc4d1	církevní
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
zprostil	zprostit	k5eAaPmAgMnS	zprostit
knížectví	knížectví	k1gNnSc4	knížectví
poplatnosti	poplatnost	k1gFnSc2	poplatnost
na	na	k7c4	na
říši	říše	k1gFnSc4	říše
a	a	k8xC	a
učinil	učinit	k5eAaImAgInS	učinit
Boleslava	Boleslav	k1gMnSc4	Boleslav
"	"	kIx"	"
<g/>
pánem	pán	k1gMnSc7	pán
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
latinské	latinský	k2eAgFnSc6d1	Latinská
kronice	kronika	k1gFnSc6	kronika
Thietmara	Thietmara	k1gFnSc1	Thietmara
Merseburského	Merseburský	k2eAgInSc2d1	Merseburský
je	on	k3xPp3gNnPc4	on
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
dominus	dominus	k1gInSc1	dominus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uznal	uznat	k5eAaPmAgInS	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozdějších	pozdní	k2eAgInPc2d2	pozdější
údajů	údaj	k1gInPc2	údaj
kronikáře	kronikář	k1gMnSc2	kronikář
Galla	Gall	k1gMnSc2	Gall
Anonyma	anonym	k1gMnSc2	anonym
prý	prý	k9	prý
císař	císař	k1gMnSc1	císař
vložil	vložit	k5eAaPmAgMnS	vložit
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
na	na	k7c4	na
Boleslavovu	Boleslavův	k2eAgFnSc4d1	Boleslavova
hlavu	hlava	k1gFnSc4	hlava
svůj	svůj	k3xOyFgInSc4	svůj
diadém	diadém	k1gInSc4	diadém
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
ho	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
bratrem	bratr	k1gMnSc7	bratr
a	a	k8xC	a
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
druhem	druh	k1gMnSc7	druh
římského	římský	k2eAgInSc2d1	římský
lidu	lid	k1gInSc2	lid
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
vykládají	vykládat	k5eAaImIp3nP	vykládat
tato	tento	k3xDgNnPc1	tento
symbolická	symbolický	k2eAgNnPc1d1	symbolické
gesta	gesto	k1gNnPc1	gesto
jako	jako	k8xC	jako
udělení	udělení	k1gNnSc1	udělení
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
šlo	jít	k5eAaImAgNnS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
Otovy	Otův	k2eAgInPc4d1	Otův
plány	plán	k1gInPc4	plán
na	na	k7c4	na
renovatio	renovatio	k1gNnSc4	renovatio
imperii	imperie	k1gFnSc4	imperie
<g/>
,	,	kIx,	,
vybudování	vybudování	k1gNnSc4	vybudování
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
skládat	skládat	k5eAaImF	skládat
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
Galie	Galie	k1gFnPc4	Galie
<g/>
,	,	kIx,	,
Germanie	Germanie	k1gFnPc4	Germanie
<g/>
,	,	kIx,	,
Romy	Rom	k1gMnPc4	Rom
a	a	k8xC	a
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
Sclavinie	Sclavinie	k1gFnPc4	Sclavinie
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
by	by	kYmCp3nS	by
vládl	vládnout	k5eAaImAgMnS	vládnout
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
kníže	kníže	k1gMnSc1	kníže
sám	sám	k3xTgMnSc1	sám
Otova	Otův	k2eAgNnPc1d1	Otovo
gesta	gesto	k1gNnPc1	gesto
jako	jako	k8xS	jako
udělení	udělení	k1gNnSc1	udělení
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
nechápal	chápat	k5eNaImAgInS	chápat
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
královská	královský	k2eAgFnSc1d1	královská
korunovace	korunovace	k1gFnSc1	korunovace
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
vyjednána	vyjednán	k2eAgFnSc1d1	vyjednána
u	u	k7c2	u
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nepříznivých	příznivý	k2eNgFnPc2d1	nepříznivá
okolností	okolnost	k1gFnPc2	okolnost
povýšen	povýšen	k2eAgInSc4d1	povýšen
ke	k	k7c3	k
krále	král	k1gMnSc4	král
uherský	uherský	k2eAgMnSc1d1	uherský
kníže	kníže	k1gMnSc1	kníže
Štěpán	Štěpán	k1gMnSc1	Štěpán
I.	I.	kA	I.
Konečně	konečně	k6eAd1	konečně
Boleslav	Boleslav	k1gMnSc1	Boleslav
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
získání	získání	k1gNnSc6	získání
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
usiloval	usilovat	k5eAaImAgInS	usilovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
výsledky	výsledek	k1gInPc4	výsledek
hnězdenského	hnězdenský	k2eAgInSc2d1	hnězdenský
sjezdu	sjezd	k1gInSc2	sjezd
představovaly	představovat	k5eAaImAgFnP	představovat
významný	významný	k2eAgInSc4d1	významný
úspěch	úspěch	k1gInSc4	úspěch
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
na	na	k7c6	na
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
církevní	církevní	k2eAgFnSc4d1	církevní
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
například	například	k6eAd1	například
české	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
čekalo	čekat	k5eAaImAgNnS	čekat
až	až	k6eAd1	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
Boleslavova	Boleslavův	k2eAgNnSc2d1	Boleslavovo
panování	panování	k1gNnSc2	panování
bylo	být	k5eAaImAgNnS	být
vyplněno	vyplnit	k5eAaPmNgNnS	vyplnit
ustavičnými	ustavičný	k2eAgFnPc7d1	ustavičná
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Válčil	válčit	k5eAaImAgMnS	válčit
s	s	k7c7	s
českým	český	k2eAgNnSc7d1	české
knížectvím	knížectví	k1gNnSc7	knížectví
<g/>
,	,	kIx,	,
po	po	k7c6	po
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
Oty	Ota	k1gMnSc2	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1002	[number]	k4	1002
s	s	k7c7	s
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
s	s	k7c7	s
Kyjevskou	kyjevský	k2eAgFnSc7d1	Kyjevská
Rusí	Rus	k1gFnSc7	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
připojení	připojení	k1gNnSc1	připojení
Krakovska	Krakovsko	k1gNnSc2	Krakovsko
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
patřícího	patřící	k2eAgMnSc2d1	patřící
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
státu	stát	k1gInSc3	stát
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
přepadl	přepadnout	k5eAaPmAgInS	přepadnout
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
Boleslava	Boleslav	k1gMnSc4	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
dočasné	dočasný	k2eAgNnSc1d1	dočasné
obsazení	obsazení	k1gNnSc1	obsazení
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
1003	[number]	k4	1003
<g/>
–	–	k?	–
<g/>
1004	[number]	k4	1004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
válkách	válka	k1gFnPc6	válka
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Jindřichem	Jindřich	k1gMnSc7	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
budínským	budínský	k2eAgInSc7d1	budínský
mírem	mír	k1gInSc7	mír
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1018	[number]	k4	1018
přiznáno	přiznat	k5eAaPmNgNnS	přiznat
držení	držení	k1gNnSc1	držení
Lužice	Lužice	k1gFnSc2	Lužice
a	a	k8xC	a
Milčanska	Milčansko	k1gNnSc2	Milčansko
(	(	kIx(	(
<g/>
též	též	k9	též
Milska	Milsko	k1gNnSc2	Milsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
Otově	Otův	k2eAgFnSc6d1	Otova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
ztratil	ztratit	k5eAaPmAgInS	ztratit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
navrátit	navrátit	k5eAaPmF	navrátit
Polsku	Polska	k1gFnSc4	Polska
Červoňské	Červoňský	k2eAgInPc1d1	Červoňský
hrady	hrad	k1gInPc1	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
papeže	papež	k1gMnSc2	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
Polsku	Polska	k1gFnSc4	Polska
nakloněn	nakloněn	k2eAgMnSc1d1	nakloněn
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Boleslav	Boleslav	k1gMnSc1	Boleslav
energicky	energicky	k6eAd1	energicky
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
v	v	k7c6	v
papežské	papežský	k2eAgFnSc6d1	Papežská
kurii	kurie	k1gFnSc6	kurie
o	o	k7c6	o
udělení	udělení	k1gNnSc6	udělení
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1025	[number]	k4	1025
byl	být	k5eAaImAgInS	být
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
svojí	svůj	k3xOyFgFnSc7	svůj
smrtí	smrtit	k5eAaImIp3nS	smrtit
korunován	korunovat	k5eAaBmNgInS	korunovat
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
nového	nový	k2eAgMnSc2d1	nový
německého	německý	k2eAgMnSc2d1	německý
panovníka	panovník	k1gMnSc2	panovník
Konráda	Konrád	k1gMnSc2	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
polsko	polsko	k6eAd1	polsko
dědičný	dědičný	k2eAgInSc1d1	dědičný
královský	královský	k2eAgInSc1d1	královský
titul	titul	k1gInSc1	titul
uznán	uznat	k5eAaPmNgInS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
byl	být	k5eAaImAgMnS	být
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
schopným	schopný	k2eAgMnSc7d1	schopný
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
i	i	k8xC	i
dobrým	dobrý	k2eAgInSc7d1	dobrý
diplomatem	diplomat	k1gInSc7	diplomat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
dobře	dobře	k6eAd1	dobře
využíval	využívat	k5eAaPmAgInS	využívat
sňatkovou	sňatkový	k2eAgFnSc4d1	sňatková
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Dosažení	dosažení	k1gNnSc1	dosažení
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
růstu	růst	k1gInSc2	růst
prestiže	prestiž	k1gFnSc2	prestiž
jeho	on	k3xPp3gNnSc2	on
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
také	také	k9	také
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
bojovali	bojovat	k5eAaImAgMnP	bojovat
jeho	jeho	k3xOp3gNnSc4	jeho
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko	Polsko	k1gNnSc1	Polsko
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
===	===	k?	===
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgInSc2d1	chrabrý
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
boje	boj	k1gInPc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
příslušníky	příslušník	k1gMnPc7	příslušník
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
ne	ne	k9	ne
nepodobné	podobný	k2eNgFnSc3d1	nepodobná
situaci	situace	k1gFnSc3	situace
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Boleslavův	Boleslavův	k2eAgMnSc1d1	Boleslavův
syn	syn	k1gMnSc1	syn
Měšek	Měšek	k1gMnSc1	Měšek
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Lambert	Lambert	k1gMnSc1	Lambert
(	(	kIx(	(
<g/>
1025	[number]	k4	1025
<g/>
–	–	k?	–
<g/>
31	[number]	k4	31
a	a	k8xC	a
opět	opět	k6eAd1	opět
v	v	k7c6	v
letech	let	k1gInPc6	let
1032	[number]	k4	1032
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
vlády	vláda	k1gFnSc2	vláda
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Říše	říš	k1gFnSc2	říš
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
Kazimír	Kazimír	k1gMnSc1	Kazimír
I.	I.	kA	I.
Obnovitel	obnovitel	k1gMnSc1	obnovitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
odporu	odpor	k1gInSc3	odpor
šlechty	šlechta	k1gFnSc2	šlechta
nedal	dát	k5eNaPmAgInS	dát
korunovat	korunovat	k5eAaBmF	korunovat
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vládcem	vládce	k1gMnSc7	vládce
Polska	Polsko	k1gNnSc2	Polsko
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Smělý	Smělý	k1gMnSc1	Smělý
(	(	kIx(	(
<g/>
1058	[number]	k4	1058
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
díky	díky	k7c3	díky
spojenectví	spojenectví	k1gNnSc3	spojenectví
s	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
získal	získat	k5eAaPmAgMnS	získat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
uznání	uznání	k1gNnSc4	uznání
polského	polský	k2eAgInSc2d1	polský
královského	královský	k2eAgInSc2d1	královský
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
korunovat	korunovat	k5eAaBmF	korunovat
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
1076	[number]	k4	1076
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
po	po	k7c6	po
3	[number]	k4	3
letech	léto	k1gNnPc6	léto
ho	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
vyhnalo	vyhnat	k5eAaPmAgNnS	vyhnat
povstání	povstání	k1gNnSc1	povstání
a	a	k8xC	a
královský	královský	k2eAgInSc1d1	královský
titul	titul	k1gInSc1	titul
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
ztracen	ztratit	k5eAaPmNgInS	ztratit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Vladislav	Vladislav	k1gMnSc1	Vladislav
I.	I.	kA	I.
Heřman	Heřman	k1gMnSc1	Heřman
(	(	kIx(	(
<g/>
1079	[number]	k4	1079
<g/>
–	–	k?	–
<g/>
1102	[number]	k4	1102
<g/>
)	)	kIx)	)
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c6	o
zlepšení	zlepšení	k1gNnSc6	zlepšení
vztahů	vztah	k1gInPc2	vztah
k	k	k7c3	k
Říši	říš	k1gFnSc3	říš
a	a	k8xC	a
Čechám	Čechy	k1gFnPc3	Čechy
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
dcera	dcera	k1gFnSc1	dcera
knížete	kníže	k1gMnSc2	kníže
<g/>
/	/	kIx~	/
<g/>
krále	král	k1gMnSc2	král
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
dcera	dcera	k1gFnSc1	dcera
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vnitropoliticky	vnitropoliticky	k6eAd1	vnitropoliticky
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
nepomohlo	pomoct	k5eNaPmAgNnS	pomoct
<g/>
:	:	kIx,	:
těžce	těžce	k6eAd1	těžce
zdolával	zdolávat	k5eAaImAgInS	zdolávat
povstání	povstání	k1gNnSc4	povstání
v	v	k7c6	v
Pomořanech	Pomořany	k1gInPc6	Pomořany
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
;	;	kIx,	;
ještě	ještě	k9	ještě
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
dělit	dělit	k5eAaImF	dělit
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
syny	syn	k1gMnPc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
si	se	k3xPyFc3	se
synové	syn	k1gMnPc1	syn
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Křivoústý	křivoústý	k2eAgMnSc1d1	křivoústý
(	(	kIx(	(
<g/>
1102	[number]	k4	1102
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
<g/>
)	)	kIx)	)
a	a	k8xC	a
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
Zbyhněv	Zbyhněv	k1gFnSc1	Zbyhněv
(	(	kIx(	(
<g/>
1102	[number]	k4	1102
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
zemi	zem	k1gFnSc6	zem
fakticky	fakticky	k6eAd1	fakticky
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
–	–	k?	–
Zbyhněv	Zbyhněv	k1gMnSc1	Zbyhněv
měl	mít	k5eAaImAgMnS	mít
severní	severní	k2eAgFnSc2d1	severní
polské	polský	k2eAgFnSc2d1	polská
země	zem	k1gFnSc2	zem
a	a	k8xC	a
Boleslav	Boleslav	k1gMnSc1	Boleslav
jižní	jižní	k2eAgMnSc1d1	jižní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
–	–	k?	–
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
a	a	k8xC	a
vyhnání	vyhnání	k1gNnSc6	vyhnání
bratra	bratr	k1gMnSc2	bratr
–	–	k?	–
posledním	poslední	k2eAgMnSc7d1	poslední
piastovským	piastovský	k2eAgMnSc7d1	piastovský
panovníkem	panovník	k1gMnSc7	panovník
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vládl	vládnout	k5eAaImAgMnS	vládnout
sjednocenému	sjednocený	k2eAgNnSc3d1	sjednocené
státu	stát	k1gInSc2	stát
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
rozpadem	rozpad	k1gInSc7	rozpad
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
sám	sám	k3xTgMnSc1	sám
napomohl	napomoct	k5eAaPmAgMnS	napomoct
svým	svůj	k3xOyFgInSc7	svůj
nástupnickým	nástupnický	k2eAgInSc7d1	nástupnický
testamentem	testament	k1gInSc7	testament
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1138	[number]	k4	1138
<g/>
.	.	kIx.	.
</s>
<s>
Závětí	závětí	k1gNnSc1	závětí
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
polský	polský	k2eAgInSc1d1	polský
stát	stát	k1gInSc1	stát
na	na	k7c4	na
úděly	úděl	k1gInPc4	úděl
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstaršímu	starý	k2eAgInSc3d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
zajistí	zajistit	k5eAaPmIp3nS	zajistit
seniorátní	seniorátní	k2eAgInSc1d1	seniorátní
úděl	úděl	k1gInSc1	úděl
(	(	kIx(	(
<g/>
Krakovsko	Krakovsko	k1gNnSc1	Krakovsko
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Velkopolska	Velkopolska	k1gFnSc1	Velkopolska
s	s	k7c7	s
Hnězdnem	Hnězdno	k1gNnSc7	Hnězdno
a	a	k8xC	a
Kališí	Kališ	k1gFnSc7	Kališ
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
vrchní	vrchní	k2eAgFnSc1d1	vrchní
moc	moc	k1gFnSc1	moc
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
snažil	snažit	k5eAaImAgMnS	snažit
zajistit	zajistit	k5eAaPmF	zajistit
jednotu	jednota	k1gFnSc4	jednota
svého	svůj	k3xOyFgInSc2	svůj
státu	stát	k1gInSc2	stát
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ji	on	k3xPp3gFnSc4	on
sladit	sladit	k5eAaPmF	sladit
s	s	k7c7	s
odstředivými	odstředivý	k2eAgFnPc7d1	odstředivá
snahami	snaha	k1gFnPc7	snaha
a	a	k8xC	a
ambicemi	ambice	k1gFnPc7	ambice
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
členů	člen	k1gMnPc2	člen
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jen	jen	k9	jen
kompromis	kompromis	k1gInSc4	kompromis
odsouzený	odsouzený	k2eAgInSc4d1	odsouzený
k	k	k7c3	k
brzkému	brzký	k2eAgInSc3d1	brzký
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Boleslavově	Boleslavův	k2eAgFnSc6d1	Boleslavova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
údělech	úděl	k1gInPc6	úděl
maximálně	maximálně	k6eAd1	maximálně
osamostatnili	osamostatnit	k5eAaPmAgMnP	osamostatnit
a	a	k8xC	a
vrchní	vrchní	k2eAgFnSc1d1	vrchní
vláda	vláda	k1gFnSc1	vláda
rychle	rychle	k6eAd1	rychle
slábla	slábnout	k5eAaImAgFnS	slábnout
<g/>
,	,	kIx,	,
až	až	k9	až
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
zcela	zcela	k6eAd1	zcela
<g/>
.	.	kIx.	.
</s>
<s>
Česko-polské	českoolský	k2eAgInPc1d1	česko-polský
spory	spor	k1gInPc1	spor
byly	být	k5eAaImAgInP	být
dočasně	dočasně	k6eAd1	dočasně
vyřešeny	vyřešit	k5eAaPmNgInP	vyřešit
smlouvou	smlouva	k1gFnSc7	smlouva
o	o	k7c6	o
přátelských	přátelský	k2eAgInPc6d1	přátelský
vztazích	vztah	k1gInPc6	vztah
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
vzdal	vzdát	k5eAaPmAgMnS	vzdát
poplatku	poplatek	k1gInSc3	poplatek
za	za	k7c4	za
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
na	na	k7c4	na
několik	několik	k4yIc4	několik
menších	malý	k2eAgNnPc2d2	menší
údělných	údělný	k2eAgNnPc2d1	údělné
knížectví	knížectví	k1gNnPc2	knížectví
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
příslušníků	příslušník	k1gMnPc2	příslušník
dynastie	dynastie	k1gFnSc2	dynastie
Piastovců	Piastovec	k1gMnPc2	Piastovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
polskými	polský	k2eAgMnPc7d1	polský
králi	král	k1gMnPc7	král
===	===	k?	===
</s>
</p>
<p>
<s>
Silné	silný	k2eAgNnSc1d1	silné
a	a	k8xC	a
jednotnější	jednotný	k2eAgNnSc1d2	jednotnější
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
až	až	k9	až
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Přemyslem	Přemysl	k1gMnSc7	Přemysl
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Velkopolským	velkopolský	k2eAgMnSc7d1	velkopolský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
1296	[number]	k4	1296
<g/>
)	)	kIx)	)
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1289	[number]	k4	1289
začal	začít	k5eAaPmAgMnS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
svoji	svůj	k3xOyFgFnSc4	svůj
lenní	lenní	k2eAgFnSc4d1	lenní
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
nad	nad	k7c7	nad
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
slezskými	slezský	k2eAgNnPc7d1	Slezské
knížectvími	knížectví	k1gNnPc7	knížectví
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1291	[number]	k4	1291
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
díky	díky	k7c3	díky
smrti	smrt	k1gFnSc3	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratrance	bratranec	k1gMnSc2	bratranec
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Krakovsko	Krakovsko	k6eAd1	Krakovsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
,	,	kIx,	,
Piastovec	Piastovec	k1gMnSc1	Piastovec
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lokýtek	lokýtek	k1gInSc4	lokýtek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
sjednotit	sjednotit	k5eAaPmF	sjednotit
polská	polský	k2eAgNnPc1d1	polské
knížectví	knížectví	k1gNnPc1	knížectví
pod	pod	k7c7	pod
svojí	svůj	k3xOyFgFnSc7	svůj
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
uprchnout	uprchnout	k5eAaPmF	uprchnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
se	se	k3xPyFc4	se
ovdovělý	ovdovělý	k2eAgMnSc1d1	ovdovělý
Václav	Václav	k1gMnSc1	Václav
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
dvanáctiletou	dvanáctiletý	k2eAgFnSc7d1	dvanáctiletá
princeznou	princezna	k1gFnSc7	princezna
Eliškou	Eliška	k1gFnSc7	Eliška
Rejčkou	Rejčka	k1gFnSc7	Rejčka
<g/>
,	,	kIx,	,
jejímuž	jejíž	k3xOyRp3gMnSc3	jejíž
otci	otec	k1gMnSc3	otec
Přemyslu	Přemysl	k1gMnSc3	Přemysl
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
podařilo	podařit	k5eAaPmAgNnS	podařit
opět	opět	k6eAd1	opět
obnovit	obnovit	k5eAaPmF	obnovit
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
hnězdenský	hnězdenský	k2eAgMnSc1d1	hnězdenský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
městě	město	k1gNnSc6	město
korunoval	korunovat	k5eAaBmAgInS	korunovat
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polákům	Polák	k1gMnPc3	Polák
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
nelíbila	líbit	k5eNaImAgFnS	líbit
personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
Čechami	Čechy	k1gFnPc7	Čechy
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
zemi	zem	k1gFnSc4	zem
hospodářsky	hospodářsky	k6eAd1	hospodářsky
pozvedl	pozvednout	k5eAaPmAgMnS	pozvednout
zavedením	zavedení	k1gNnSc7	zavedení
pražského	pražský	k2eAgInSc2d1	pražský
groše	groš	k1gInSc2	groš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1305	[number]	k4	1305
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
uherské	uherský	k2eAgFnSc2d1	uherská
koruny	koruna	k1gFnSc2	koruna
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
větší	veliký	k2eAgFnSc4d2	veliký
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lokýtek	lokýtek	k1gInSc4	lokýtek
dobyl	dobýt	k5eAaPmAgInS	dobýt
královský	královský	k2eAgInSc1d1	královský
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
skončila	skončit	k5eAaPmAgFnS	skončit
královská	královský	k2eAgFnSc1d1	královská
linie	linie	k1gFnSc1	linie
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
vražda	vražda	k1gFnSc1	vražda
udělala	udělat	k5eAaPmAgFnS	udělat
tečku	tečka	k1gFnSc4	tečka
za	za	k7c7	za
česko-polskou	českoolský	k2eAgFnSc7d1	česko-polská
aliancí	aliance	k1gFnSc7	aliance
<g/>
,	,	kIx,	,
z	z	k7c2	z
reforem	reforma	k1gFnPc2	reforma
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
Polsko	Polsko	k1gNnSc1	Polsko
čerpalo	čerpat	k5eAaImAgNnS	čerpat
ještě	ještě	k6eAd1	ještě
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
titulárního	titulární	k2eAgNnSc2d1	titulární
<g/>
)	)	kIx)	)
ovšem	ovšem	k9	ovšem
drželi	držet	k5eAaImAgMnP	držet
čeští	český	k2eAgMnPc1d1	český
panovníci	panovník	k1gMnPc1	panovník
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
tím	ten	k3xDgNnSc7	ten
skutečným	skutečný	k2eAgNnSc7d1	skutečné
byl	být	k5eAaImAgMnS	být
Vladislav	Vladislav	k1gMnSc1	Vladislav
Lokýtek	lokýtek	k1gInSc4	lokýtek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opětovná	opětovný	k2eAgFnSc1d1	opětovná
vláda	vláda	k1gFnSc1	vláda
piastovských	piastovský	k2eAgMnPc2d1	piastovský
králů	král	k1gMnPc2	král
===	===	k?	===
</s>
</p>
<p>
<s>
Kazimír	Kazimír	k1gMnSc1	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Vladislava	Vladislav	k1gMnSc2	Vladislav
Lokýtka	Lokýtek	k1gMnSc2	Lokýtek
byl	být	k5eAaImAgInS	být
skvělým	skvělý	k2eAgInSc7d1	skvělý
diplomatem	diplomat	k1gInSc7	diplomat
polští	polský	k2eAgMnPc1d1	polský
historici	historik	k1gMnPc1	historik
ho	on	k3xPp3gNnSc4	on
tradičně	tradičně	k6eAd1	tradičně
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
středověkých	středověký	k2eAgMnPc2d1	středověký
vládců	vládce	k1gMnPc2	vládce
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
otci	otec	k1gMnSc3	otec
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
podařilo	podařit	k5eAaPmAgNnS	podařit
znovu	znovu	k6eAd1	znovu
sjednotit	sjednotit	k5eAaPmF	sjednotit
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc1d1	celé
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půldruhé	půldruhý	k2eAgNnSc1d1	půldruhé
století	století	k1gNnSc1	století
politicky	politicky	k6eAd1	politicky
rozdrobené	rozdrobený	k2eAgNnSc1d1	rozdrobené
<g/>
,	,	kIx,	,
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kazimírovi	Kazimír	k1gMnSc6	Kazimír
pak	pak	k6eAd1	pak
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokončil	dokončit	k5eAaPmAgInS	dokončit
sjednocení	sjednocení	k1gNnSc4	sjednocení
země	zem	k1gFnSc2	zem
–	–	k?	–
neboť	neboť	k8xC	neboť
slezská	slezský	k2eAgNnPc4d1	Slezské
knížectví	knížectví	k1gNnPc4	knížectví
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
uváděna	uvádět	k5eAaImNgFnS	uvádět
do	do	k7c2	do
lenní	lenní	k2eAgFnSc2d1	lenní
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
českém	český	k2eAgMnSc6d1	český
králi	král	k1gMnSc6	král
<g/>
,	,	kIx,	,
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
Krakova	Krakov	k1gInSc2	Krakov
odmítali	odmítat	k5eAaImAgMnP	odmítat
uznat	uznat	k5eAaPmF	uznat
také	také	k9	také
mazovští	mazovský	k2eAgMnPc1d1	mazovský
Piastovci	Piastovec	k1gMnPc1	Piastovec
–	–	k?	–
a	a	k8xC	a
upevnil	upevnit	k5eAaPmAgInS	upevnit
královskou	královský	k2eAgFnSc4d1	královská
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zemi	zem	k1gFnSc4	zem
vyčerpané	vyčerpaný	k2eAgInPc1d1	vyčerpaný
zápasy	zápas	k1gInPc1	zápas
za	za	k7c2	za
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
zajistil	zajistit	k5eAaPmAgInS	zajistit
klidný	klidný	k2eAgInSc4d1	klidný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
zmírnění	zmírnění	k1gNnSc4	zmírnění
odporu	odpor	k1gInSc2	odpor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
sjednocování	sjednocování	k1gNnSc4	sjednocování
polským	polský	k2eAgMnSc7d1	polský
zemí	zem	k1gFnPc2	zem
kladli	klást	k5eAaImAgMnP	klást
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
a	a	k8xC	a
řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trnem	trn	k1gInSc7	trn
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
Kazimíra	Kazimír	k1gMnSc4	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnPc4	jeho
předchůdce	předchůdce	k1gMnPc4	předchůdce
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
polský	polský	k2eAgInSc4d1	polský
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
drželi	držet	k5eAaImAgMnP	držet
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zpochybňovalo	zpochybňovat	k5eAaImAgNnS	zpochybňovat
Vladislavovo	Vladislavův	k2eAgNnSc1d1	Vladislavovo
i	i	k8xC	i
Kazimírovo	Kazimírův	k2eAgNnSc1d1	Kazimírův
postavení	postavení	k1gNnSc1	postavení
polského	polský	k2eAgMnSc2d1	polský
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Kazimír	Kazimír	k1gMnSc1	Kazimír
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snažil	snažit	k5eAaImAgMnS	snažit
zajistit	zajistit	k5eAaPmF	zajistit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nynějšímu	nynější	k2eAgMnSc3d1	nynější
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
Janu	Jan	k1gMnSc3	Jan
Lucemburskému	lucemburský	k2eAgMnSc3d1	lucemburský
spojenectvím	spojenectví	k1gNnSc7	spojenectví
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Bavorem	Bavor	k1gMnSc7	Bavor
<g/>
,	,	kIx,	,
Habsburky	Habsburk	k1gMnPc7	Habsburk
a	a	k8xC	a
bavorskými	bavorský	k2eAgMnPc7d1	bavorský
Wittelsbachy	Wittelsbach	k1gMnPc7	Wittelsbach
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
počítal	počítat	k5eAaImAgMnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistě	jistě	k9	jistě
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vojenskému	vojenský	k2eAgNnSc3d1	vojenské
střetnutí	střetnutí	k1gNnSc3	střetnutí
a	a	k8xC	a
sbíral	sbírat	k5eAaImAgInS	sbírat
již	již	k6eAd1	již
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabí	markrabí	k1gMnSc1	markrabí
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
chopil	chopit	k5eAaPmAgMnS	chopit
iniciativy	iniciativa	k1gFnPc4	iniciativa
a	a	k8xC	a
navázal	navázat	k5eAaPmAgMnS	navázat
s	s	k7c7	s
Kazimírem	Kazimír	k1gMnSc7	Kazimír
diplomatické	diplomatický	k2eAgFnPc4d1	diplomatická
jednání	jednání	k1gNnPc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
řešení	řešení	k1gNnSc1	řešení
ožehavé	ožehavý	k2eAgFnSc2d1	ožehavá
situace	situace	k1gFnSc2	situace
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
titulu	titul	k1gInSc2	titul
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
a	a	k8xC	a
získá	získat	k5eAaPmIp3nS	získat
zato	zato	k6eAd1	zato
trvale	trvale	k6eAd1	trvale
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
Piastovec	Piastovec	k1gMnSc1	Piastovec
Kazimír	Kazimír	k1gMnSc1	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
nezplodil	zplodit	k5eNaPmAgMnS	zplodit
mužského	mužský	k2eAgMnSc4d1	mužský
dědice	dědic	k1gMnSc4	dědic
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
dědicem	dědic	k1gMnSc7	dědic
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
stal	stát	k5eAaPmAgMnS	stát
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
také	také	k9	také
neměl	mít	k5eNaImAgMnS	mít
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Šlechta	šlechta	k1gFnSc1	šlechta
nechtěla	chtít	k5eNaImAgFnS	chtít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
Uherskem	Uhersko	k1gNnSc7	Uhersko
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
po	po	k7c6	po
Ludvíkově	Ludvíkův	k2eAgFnSc6d1	Ludvíkova
smrti	smrt	k1gFnSc6	smrt
vybrala	vybrat	k5eAaPmAgFnS	vybrat
jako	jako	k8xC	jako
novou	nový	k2eAgFnSc4d1	nová
polskou	polský	k2eAgFnSc4d1	polská
královnu	královna	k1gFnSc4	královna
jeho	jeho	k3xOp3gFnSc4	jeho
dceru	dcera	k1gFnSc4	dcera
Hedviku	Hedvika	k1gFnSc4	Hedvika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
ve	v	k7c6	v
Velkopolsku	Velkopolsko	k1gNnSc6	Velkopolsko
(	(	kIx(	(
<g/>
1383	[number]	k4	1383
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
konečně	konečně	k6eAd1	konečně
Hedvika	Hedvika	k1gFnSc1	Hedvika
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
korunována	korunovat	k5eAaBmNgFnS	korunovat
jako	jako	k8xC	jako
Hedwig	Hedwig	k1gInSc1	Hedwig
Rex	Rex	k1gMnSc2	Rex
Poloniae	Polonia	k1gMnSc2	Polonia
(	(	kIx(	(
<g/>
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
Regina	Regina	k1gFnSc1	Regina
Poloniae	Poloniae	k1gFnSc1	Poloniae
(	(	kIx(	(
<g/>
královna	královna	k1gFnSc1	královna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zdůraznilo	zdůraznit	k5eAaPmAgNnS	zdůraznit
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vládcem	vládce	k1gMnSc7	vládce
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
vlastního	vlastní	k2eAgNnSc2d1	vlastní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
jako	jako	k8xS	jako
něčí	něčí	k3xOyIgFnSc1	něčí
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vymření	vymření	k1gNnSc2	vymření
polských	polský	k2eAgMnPc2d1	polský
piastovců	piastovec	k1gMnPc2	piastovec
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1385	[number]	k4	1385
smlouvou	smlouva	k1gFnSc7	smlouva
v	v	k7c6	v
Krevě	Kreva	k1gFnSc6	Kreva
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Litvy	Litva	k1gFnSc2	Litva
v	v	k7c6	v
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
položen	položen	k2eAgInSc4d1	položen
základ	základ	k1gInSc4	základ
budoucímu	budoucí	k2eAgNnSc3d1	budoucí
soustátí	soustátí	k1gNnSc3	soustátí
Polsko-Litva	Polsko-Litvo	k1gNnSc2	Polsko-Litvo
<g/>
.	.	kIx.	.
</s>
<s>
Litevský	litevský	k2eAgMnSc1d1	litevský
velkokníže	velkokníže	k1gMnSc1	velkokníže
Jogaila	Jogail	k1gMnSc4	Jogail
přijal	přijmout	k5eAaPmAgMnS	přijmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jméno	jméno	k1gNnSc4	jméno
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
také	také	k9	také
konala	konat	k5eAaImAgFnS	konat
svatba	svatba	k1gFnSc1	svatba
36	[number]	k4	36
<g/>
letého	letý	k2eAgInSc2d1	letý
Jogaila	Jogaila	k1gFnSc1	Jogaila
s	s	k7c7	s
12	[number]	k4	12
<g/>
letou	letý	k2eAgFnSc7d1	letá
Hedvikou	Hedvika	k1gFnSc7	Hedvika
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
byl	být	k5eAaImAgInS	být
Jogailo	Jogaila	k1gFnSc5	Jogaila
korunován	korunován	k2eAgMnSc1d1	korunován
na	na	k7c4	na
polského	polský	k2eAgMnSc4d1	polský
krále	král	k1gMnSc4	král
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonského	jagellonský	k2eAgInSc2d1	jagellonský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
spojení	spojení	k1gNnSc3	spojení
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Litvy	Litva	k1gFnSc2	Litva
stavěly	stavět	k5eAaImAgFnP	stavět
okolní	okolní	k2eAgFnPc1d1	okolní
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
roku	rok	k1gInSc2	rok
1409	[number]	k4	1409
křižáckým	křižácký	k2eAgNnSc7d1	křižácké
tažením	tažení	k1gNnSc7	tažení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nakonec	nakonec	k6eAd1	nakonec
skončilo	skončit	k5eAaPmAgNnS	skončit
slavným	slavný	k2eAgNnSc7d1	slavné
polským	polský	k2eAgNnSc7d1	polské
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Grunwaldu	Grunwald	k1gInSc2	Grunwald
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
Poláci	Polák	k1gMnPc1	Polák
s	s	k7c7	s
Litevci	Litevec	k1gMnPc7	Litevec
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
uhájili	uhájit	k5eAaPmAgMnP	uhájit
i	i	k9	i
vůči	vůči	k7c3	vůči
římskému	římský	k2eAgMnSc3d1	římský
králi	král	k1gMnSc3	král
Zikmundu	Zikmund	k1gMnSc3	Zikmund
Lucemburskému	lucemburský	k2eAgMnSc3d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Vladislava	Vladislav	k1gMnSc2	Vladislav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1434	[number]	k4	1434
zvolila	zvolit	k5eAaPmAgFnS	zvolit
polská	polský	k2eAgFnSc1d1	polská
šlechta	šlechta	k1gFnSc1	šlechta
králem	král	k1gMnSc7	král
jeho	jeho	k3xOp3gMnSc1	jeho
desetiletého	desetiletý	k2eAgMnSc4d1	desetiletý
syna	syn	k1gMnSc4	syn
Vladislava	Vladislav	k1gMnSc2	Vladislav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
roku	rok	k1gInSc3	rok
1440	[number]	k4	1440
po	po	k7c6	po
dynastické	dynastický	k2eAgFnSc6d1	dynastická
stránce	stránka	k1gFnSc6	stránka
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
a	a	k8xC	a
již	již	k9	již
roku	rok	k1gInSc2	rok
1444	[number]	k4	1444
jako	jako	k8xC	jako
dvacetiletý	dvacetiletý	k2eAgMnSc1d1	dvacetiletý
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Varny	Varna	k1gFnSc2	Varna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zmatkům	zmatek	k1gInPc3	zmatek
a	a	k8xC	a
sporům	spor	k1gInPc3	spor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vyšel	vyjít	k5eAaPmAgMnS	vyjít
vítězně	vítězně	k6eAd1	vítězně
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Vladislava	Vladislav	k1gMnSc2	Vladislav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Kazimír	Kazimír	k1gMnSc1	Kazimír
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1466	[number]	k4	1466
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
jako	jako	k9	jako
Kazimír	Kazimír	k1gMnSc1	Kazimír
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
po	po	k7c6	po
třináctileté	třináctiletý	k2eAgFnSc6d1	třináctiletá
válce	válka	k1gFnSc6	válka
druhý	druhý	k4xOgInSc1	druhý
toruňský	toruňský	k2eAgInSc1d1	toruňský
mír	mír	k1gInSc1	mír
s	s	k7c7	s
Řádem	řád	k1gInSc7	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
získal	získat	k5eAaPmAgMnS	získat
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
pruského	pruský	k2eAgNnSc2d1	pruské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
říkalo	říkat	k5eAaImAgNnS	říkat
královské	královský	k2eAgNnSc1d1	královské
prusko	prusko	k1gNnSc1	prusko
(	(	kIx(	(
<g/>
a	a	k8xC	a
symbolicky	symbolicky	k6eAd1	symbolicky
byl	být	k5eAaImAgInS	být
územím	území	k1gNnSc7	území
nutným	nutný	k2eAgNnSc7d1	nutné
pro	pro	k7c4	pro
titul	titul	k1gInSc4	titul
pruského	pruský	k2eAgMnSc2d1	pruský
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
stal	stát	k5eAaPmAgInS	stát
českým	český	k2eAgMnSc7d1	český
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1491	[number]	k4	1491
králem	král	k1gMnSc7	král
uherským	uherský	k2eAgMnSc7d1	uherský
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
rodové	rodový	k2eAgFnSc2d1	rodová
Jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
državy	država	k1gFnSc2	država
na	na	k7c6	na
území	území	k1gNnSc6	území
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
táhly	táhnout	k5eAaImAgFnP	táhnout
od	od	k7c2	od
Baltského	baltský	k2eAgNnSc2d1	Baltské
k	k	k7c3	k
Černému	černý	k2eAgNnSc3d1	černé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Kazimíra	Kazimír	k1gMnSc2	Kazimír
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonského	jagellonský	k2eAgMnSc2d1	jagellonský
se	se	k3xPyFc4	se
na	na	k7c6	na
polském	polský	k2eAgInSc6d1	polský
trůně	trůn	k1gInSc6	trůn
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
postupně	postupně	k6eAd1	postupně
jeho	jeho	k3xOp3gMnPc1	jeho
tři	tři	k4xCgMnPc1	tři
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vedli	vést	k5eAaImAgMnP	vést
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tři	tři	k4xCgFnPc1	tři
války	válka	k1gFnPc1	válka
s	s	k7c7	s
moskevským	moskevský	k2eAgNnSc7d1	moskevské
knížectvím	knížectví	k1gNnSc7	knížectví
<g/>
,	,	kIx,	,
vyplňované	vyplňovaný	k2eAgFnPc1d1	vyplňovaná
dílčími	dílčí	k2eAgNnPc7d1	dílčí
střetnutími	střetnutí	k1gNnPc7	střetnutí
s	s	k7c7	s
Tatary	Tatar	k1gMnPc7	Tatar
<g/>
.	.	kIx.	.
</s>
<s>
Polsku	Polska	k1gFnSc4	Polska
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhnout	vyhnout	k5eAaPmF	vyhnout
přímé	přímý	k2eAgInPc4d1	přímý
válce	válec	k1gInPc4	válec
s	s	k7c7	s
Turky	Turek	k1gMnPc7	Turek
a	a	k8xC	a
navázalo	navázat	k5eAaPmAgNnS	navázat
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
křižáky	křižák	k1gMnPc7	křižák
<g/>
.	.	kIx.	.
</s>
<s>
Rozbroje	rozbroj	k1gInPc1	rozbroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Litvou	Litva	k1gFnSc7	Litva
byly	být	k5eAaImAgFnP	být
zažehnány	zažehnat	k5eAaPmNgFnP	zažehnat
roku	rok	k1gInSc3	rok
1569	[number]	k4	1569
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
tzv.	tzv.	kA	tzv.
lublinskou	lublinský	k2eAgFnSc7d1	Lublinská
unií	unie	k1gFnSc7	unie
těsněji	těsně	k6eAd2	těsně
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
k	k	k7c3	k
polskému	polský	k2eAgNnSc3d1	polské
království	království	k1gNnSc3	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
součástí	součást	k1gFnPc2	součást
Republiky	republika	k1gFnSc2	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vymření	vymření	k1gNnSc2	vymření
dynastie	dynastie	k1gFnSc2	dynastie
Jagellonců	Jagellonec	k1gMnPc2	Jagellonec
svolal	svolat	k5eAaPmAgMnS	svolat
poslední	poslední	k2eAgMnSc1d1	poslední
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
a	a	k8xC	a
litevský	litevský	k2eAgMnSc1d1	litevský
velkokníže	velkokníže	k1gMnSc1	velkokníže
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
dynastie	dynastie	k1gFnSc2	dynastie
Zikmund	Zikmund	k1gMnSc1	Zikmund
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1569	[number]	k4	1569
do	do	k7c2	do
Lublinu	Lublin	k1gInSc2	Lublin
sejm	sejm	k1gInSc1	sejm
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
a	a	k8xC	a
litevské	litevský	k2eAgNnSc1d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc1	velkoknížectví
budou	být	k5eAaImBp3nP	být
spojeno	spojit	k5eAaPmNgNnS	spojit
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
celku	celek	k1gInSc2	celek
–	–	k?	–
Rzeczpospolita	Rzeczpospolita	k1gFnSc1	Rzeczpospolita
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
společným	společný	k2eAgMnSc7d1	společný
panovníkem	panovník	k1gMnSc7	panovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pod	pod	k7c7	pod
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
legislativou	legislativa	k1gFnSc7	legislativa
<g/>
,	,	kIx,	,
měnou	měna	k1gFnSc7	měna
a	a	k8xC	a
úřední	úřední	k2eAgFnSc7d1	úřední
řečí	řeč	k1gFnSc7	řeč
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
polština	polština	k1gFnSc1	polština
a	a	k8xC	a
latina	latina	k1gFnSc1	latina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reálná	reálný	k2eAgFnSc1d1	reálná
unie	unie	k1gFnSc1	unie
polska	polsk	k1gInSc2	polsk
a	a	k8xC	a
litvy	litva	k1gFnSc2	litva
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Lublinská	lublinský	k2eAgFnSc1d1	Lublinská
unie	unie	k1gFnSc1	unie
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Krevské	Krevský	k2eAgFnSc2d1	Krevský
unie	unie	k1gFnSc2	unie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
volenou	volený	k2eAgFnSc7d1	volená
monarchií	monarchie	k1gFnSc7	monarchie
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
již	již	k9	již
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
šlechtickou	šlechtický	k2eAgFnSc7d1	šlechtická
republikou	republika	k1gFnSc7	republika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nakonec	nakonec	k6eAd1	nakonec
přivodilo	přivodit	k5eAaBmAgNnS	přivodit
její	její	k3xOp3gInSc4	její
zánik	zánik	k1gInSc4	zánik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
společenství	společenství	k1gNnSc1	společenství
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
velmi	velmi	k6eAd1	velmi
výraznější	výrazný	k2eAgMnSc1d2	výraznější
než	než	k8xS	než
okrajová	okrajový	k2eAgFnSc1d1	okrajová
Litva	Litva	k1gFnSc1	Litva
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
mluví	mluvit	k5eAaImIp3nS	mluvit
prostě	prostě	k9	prostě
jen	jen	k9	jen
o	o	k7c6	o
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vytvořením	vytvoření	k1gNnSc7	vytvoření
Republiky	republika	k1gFnSc2	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mezinárodnímu	mezinárodní	k2eAgNnSc3d1	mezinárodní
posílení	posílení	k1gNnSc3	posílení
státu	stát	k1gInSc3	stát
a	a	k8xC	a
k	k	k7c3	k
potlačení	potlačení	k1gNnSc3	potlačení
autonomistických	autonomistický	k2eAgFnPc2d1	autonomistická
tendencí	tendence	k1gFnPc2	tendence
zejména	zejména	k9	zejména
u	u	k7c2	u
litevské	litevský	k2eAgFnSc2d1	Litevská
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státním	státní	k2eAgNnSc6d1	státní
zřízení	zřízení	k1gNnSc6	zřízení
měla	mít	k5eAaImAgFnS	mít
velice	velice	k6eAd1	velice
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
královské	královský	k2eAgFnSc2d1	královská
rady	rada	k1gFnSc2	rada
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dle	dle	k7c2	dle
usnesení	usnesení	k1gNnSc2	usnesení
Nihil	Nihila	k1gFnPc2	Nihila
novi	novi	k6eAd1	novi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1505	[number]	k4	1505
stal	stát	k5eAaPmAgInS	stát
prakticky	prakticky	k6eAd1	prakticky
předsedou	předseda	k1gMnSc7	předseda
této	tento	k3xDgFnSc2	tento
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
nemohl	moct	k5eNaImAgMnS	moct
činit	činit	k5eAaImF	činit
významná	významný	k2eAgNnPc1d1	významné
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
sněmovních	sněmovní	k2eAgMnPc2d1	sněmovní
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
dynastie	dynastie	k1gFnSc2	dynastie
o	o	k7c4	o
polský	polský	k2eAgInSc4d1	polský
trůn	trůn	k1gInSc4	trůn
soupeřilo	soupeřit	k5eAaImAgNnS	soupeřit
několik	několik	k4yIc1	několik
evropských	evropský	k2eAgFnPc2d1	Evropská
dynastií	dynastie	k1gFnPc2	dynastie
a	a	k8xC	a
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1573	[number]	k4	1573
byl	být	k5eAaImAgInS	být
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Jindřich	Jindřich	k1gMnSc1	Jindřich
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
francouzského	francouzský	k2eAgMnSc2d1	francouzský
panovníka	panovník	k1gMnSc2	panovník
Karla	Karel	k1gMnSc2	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
musel	muset	k5eAaImAgMnS	muset
splnit	splnit	k5eAaPmF	splnit
mnoho	mnoho	k4c4	mnoho
stanovených	stanovený	k2eAgFnPc2d1	stanovená
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byla	být	k5eAaImAgFnS	být
zásada	zásada	k1gFnSc1	zásada
zřeknutí	zřeknutí	k1gNnSc2	zřeknutí
se	se	k3xPyFc4	se
dědičnosti	dědičnost	k1gFnSc3	dědičnost
<g/>
.	.	kIx.	.
</s>
<s>
Panoval	panovat	k5eAaImAgMnS	panovat
však	však	k9	však
pouze	pouze	k6eAd1	pouze
cca	cca	kA	cca
120	[number]	k4	120
dní	den	k1gInPc2	den
a	a	k8xC	a
jakmile	jakmile	k8xS	jakmile
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
IX	IX	kA	IX
<g/>
.	.	kIx.	.
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
dojmem	dojem	k1gInSc7	dojem
omezování	omezování	k1gNnSc2	omezování
moci	moc	k1gFnSc2	moc
navrátil	navrátit	k5eAaPmAgMnS	navrátit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
ujal	ujmout	k5eAaPmAgMnS	ujmout
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úpadek	úpadek	k1gInSc1	úpadek
moci	moc	k1gFnSc2	moc
Polska	Polsko	k1gNnSc2	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozkvětu	rozkvět	k1gInSc3	rozkvět
polsko-litevského	polskoitevský	k2eAgInSc2d1	polsko-litevský
státu	stát	k1gInSc2	stát
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgFnSc7d1	negativní
stránkou	stránka	k1gFnSc7	stránka
bylo	být	k5eAaImAgNnS	být
přílišné	přílišný	k2eAgNnSc1d1	přílišné
oslabování	oslabování	k1gNnSc4	oslabování
moci	moct	k5eAaImF	moct
panovníka	panovník	k1gMnSc4	panovník
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
růst	růst	k1gInSc4	růst
vlivu	vliv	k1gInSc2	vliv
početné	početný	k2eAgFnSc2d1	početná
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
tento	tento	k3xDgInSc1	tento
stát	stát	k1gInSc1	stát
nazýván	nazýván	k2eAgInSc1d1	nazýván
Rzecz	Rzecz	k1gInSc1	Rzecz
Pospolita	Pospolita	k1gFnSc1	Pospolita
(	(	kIx(	(
<g/>
=	=	kIx~	=
věc	věc	k1gFnSc1	věc
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
,	,	kIx,	,
Rzeczpospolita	Rzeczpospolit	k2eAgFnSc1d1	Rzeczpospolita
–	–	k?	–
stejný	stejný	k2eAgInSc4d1	stejný
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
res	res	k?	res
publica	publicum	k1gNnSc2	publicum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
sláblo	slábnout	k5eAaImAgNnS	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
Pruský	pruský	k2eAgMnSc1d1	pruský
"	"	kIx"	"
<g/>
velký	velký	k2eAgMnSc1d1	velký
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
<g/>
"	"	kIx"	"
Fridrich	Fridrich	k1gMnSc1	Fridrich
Vilém	Vilém	k1gMnSc1	Vilém
(	(	kIx(	(
<g/>
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1688	[number]	k4	1688
<g/>
)	)	kIx)	)
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
zrušení	zrušení	k1gNnSc4	zrušení
vazalských	vazalský	k2eAgInPc2d1	vazalský
závazků	závazek	k1gInPc2	závazek
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1657	[number]	k4	1657
velavskou	velavský	k2eAgFnSc7d1	velavský
smlouvou	smlouva	k1gFnSc7	smlouva
i	i	k8xC	i
zrušení	zrušení	k1gNnSc1	zrušení
lenního	lenní	k2eAgInSc2d1	lenní
poměru	poměr	k1gInSc2	poměr
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
uznáno	uznat	k5eAaPmNgNnS	uznat
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1660	[number]	k4	1660
olivským	olivský	k2eAgInSc7d1	olivský
mírem	mír	k1gInSc7	mír
i	i	k8xC	i
ostatními	ostatní	k2eAgFnPc7d1	ostatní
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bezesporu	bezesporu	k9	bezesporu
nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
a	a	k8xC	a
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
panovníkem	panovník	k1gMnSc7	panovník
polsko-litevského	polskoitevský	k2eAgNnSc2d1	polsko-litevské
společenství	společenství	k1gNnSc2	společenství
byl	být	k5eAaImAgInS	být
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sobieski	Sobiesk	k1gMnSc3	Sobiesk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1674	[number]	k4	1674
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Sobieski	Sobieski	k6eAd1	Sobieski
byl	být	k5eAaImAgInS	být
profrancouzsky	profrancouzsky	k6eAd1	profrancouzsky
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
jak	jak	k6eAd1	jak
vnitřním	vnitřní	k2eAgMnPc3d1	vnitřní
rozbrojům	rozbroj	k1gInPc3	rozbroj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dalšímu	další	k2eAgNnSc3d1	další
hrozícímu	hrozící	k2eAgNnSc3d1	hrozící
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gNnSc2	on
vojska	vojsko	k1gNnSc2	vojsko
pomohla	pomoct	k5eAaPmAgFnS	pomoct
roku	rok	k1gInSc2	rok
1683	[number]	k4	1683
porazit	porazit	k5eAaPmF	porazit
Turky	Turek	k1gMnPc4	Turek
při	při	k7c6	při
obléhání	obléhání	k1gNnSc4	obléhání
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válečné	válečný	k2eAgInPc1d1	válečný
konflikty	konflikt	k1gInPc1	konflikt
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
způsobily	způsobit	k5eAaPmAgInP	způsobit
Polsku	Polska	k1gFnSc4	Polska
obrovské	obrovský	k2eAgFnSc2d1	obrovská
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1697	[number]	k4	1697
<g/>
–	–	k?	–
<g/>
1733	[number]	k4	1733
vládl	vládnout	k5eAaImAgMnS	vládnout
Polsku	Polska	k1gFnSc4	Polska
(	(	kIx(	(
<g/>
s	s	k7c7	s
pětiletou	pětiletý	k2eAgFnSc7d1	pětiletá
přestávkou	přestávka	k1gFnSc7	přestávka
<g/>
)	)	kIx)	)
August	August	k1gMnSc1	August
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gNnSc2	on
panování	panování	k1gNnSc2	panování
i	i	k8xC	i
vlády	vláda	k1gFnSc2	vláda
nástupců	nástupce	k1gMnPc2	nástupce
ze	z	k7c2	z
saského	saský	k2eAgInSc2d1	saský
rodu	rod	k1gInSc2	rod
provázel	provázet	k5eAaImAgInS	provázet
národní	národní	k2eAgInSc1d1	národní
a	a	k8xC	a
politický	politický	k2eAgInSc1d1	politický
úpadek	úpadek	k1gInSc1	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
August	August	k1gMnSc1	August
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
ztracená	ztracený	k2eAgNnPc1d1	ztracené
území	území	k1gNnPc1	území
v	v	k7c6	v
Livonsku	Livonsko	k1gNnSc6	Livonsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
dánské	dánský	k2eAgFnSc2d1	dánská
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
od	od	k7c2	od
švédského	švédský	k2eAgMnSc2d1	švédský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
XII	XII	kA	XII
<g/>
.	.	kIx.	.
v	v	k7c6	v
tzv	tzv	kA	tzv
"	"	kIx"	"
<g/>
severní	severní	k2eAgFnSc3d1	severní
válce	válka	k1gFnSc3	válka
<g/>
"	"	kIx"	"
vpadla	vpadnout	k5eAaPmAgNnP	vpadnout
švédská	švédský	k2eAgNnPc1d1	švédské
vojska	vojsko	k1gNnPc1	vojsko
i	i	k9	i
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Augusta	Augusta	k1gMnSc1	Augusta
II	II	kA	II
<g/>
.	.	kIx.	.
sesadila	sesadit	k5eAaPmAgFnS	sesadit
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
něho	on	k3xPp3gNnSc2	on
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
v	v	k7c6	v
letech	let	k1gInPc6	let
1704	[number]	k4	1704
<g/>
–	–	k?	–
<g/>
1709	[number]	k4	1709
Stanislav	Stanislava	k1gFnPc2	Stanislava
Leszczyński	Leszczyński	k1gNnSc2	Leszczyński
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Švédů	Švéd	k1gMnPc2	Švéd
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Poltavy	Poltava	k1gFnSc2	Poltava
roku	rok	k1gInSc2	rok
1709	[number]	k4	1709
se	se	k3xPyFc4	se
August	August	k1gMnSc1	August
II	II	kA	II
<g/>
.	.	kIx.	.
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
polský	polský	k2eAgInSc4d1	polský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
využít	využít	k5eAaPmF	využít
příznivé	příznivý	k2eAgFnPc4d1	příznivá
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
zahraničně-politických	zahraničněolitický	k2eAgNnPc2d1	zahraničně-politické
jednání	jednání	k1gNnPc2	jednání
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
slabý	slabý	k2eAgMnSc1d1	slabý
byl	být	k5eAaImAgInS	být
i	i	k9	i
jeho	jeho	k3xOp3gInSc4	jeho
nástupce	nástupce	k1gMnSc1	nástupce
August	August	k1gMnSc1	August
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
Stanislav	Stanislav	k1gMnSc1	Stanislav
Poniatowski	Poniatowske	k1gFnSc4	Poniatowske
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
konečnému	konečný	k2eAgInSc3d1	konečný
úpadku	úpadek	k1gInSc3	úpadek
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
tři	tři	k4xCgFnPc4	tři
sousední	sousední	k2eAgFnPc4d1	sousední
velmoci	velmoc	k1gFnPc4	velmoc
a	a	k8xC	a
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Dělení	dělení	k1gNnSc4	dělení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Trojí	trojit	k5eAaImIp3nS	trojit
dělení	dělení	k1gNnSc1	dělení
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
1772	[number]	k4	1772
<g/>
–	–	k?	–
<g/>
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Posilování	posilování	k1gNnSc1	posilování
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
rozbroje	rozbroj	k1gInPc4	rozbroj
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
oslabily	oslabit	k5eAaPmAgInP	oslabit
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1772	[number]	k4	1772
<g/>
,	,	kIx,	,
1793	[number]	k4	1793
a	a	k8xC	a
1795	[number]	k4	1795
si	se	k3xPyFc3	se
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Prusko	Prusko	k1gNnSc1	Prusko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc1	Rakousko
v	v	k7c6	v
trojím	trojí	k4xRgInSc7	trojí
dělením	dělení	k1gNnSc7	dělení
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
Polsko	Polsko	k1gNnSc1	Polsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třetím	třetí	k4xOgNnSc6	třetí
dělení	dělení	k1gNnSc6	dělení
Polska	Polska	k1gFnSc1	Polska
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1795	[number]	k4	1795
tak	tak	k8xS	tak
Rzeczpospolita	Rzeczpospolita	k1gFnSc1	Rzeczpospolita
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
samostatné	samostatný	k2eAgNnSc1d1	samostatné
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
Polsko	Polsko	k1gNnSc4	Polsko
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1807	[number]	k4	1807
<g/>
–	–	k?	–
<g/>
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřídil	zřídit	k5eAaPmAgMnS	zřídit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
I.	I.	kA	I.
na	na	k7c6	na
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
předtím	předtím	k6eAd1	předtím
zabraným	zabraný	k2eAgNnSc7d1	zabrané
Pruskem	Prusko	k1gNnSc7	Prusko
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
Knížectví	knížectví	k1gNnSc2	knížectví
varšavské	varšavský	k2eAgFnSc2d1	Varšavská
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
spojencem	spojenec	k1gMnSc7	spojenec
a	a	k8xC	a
vazalem	vazal	k1gMnSc7	vazal
Napoleona	Napoleon	k1gMnSc2	Napoleon
a	a	k8xC	a
jehož	jehož	k3xOyRp3gMnSc7	jehož
knížetem	kníže	k1gMnSc7	kníže
byl	být	k5eAaImAgMnS	být
Fridrich	Fridrich	k1gMnSc1	Fridrich
August	August	k1gMnSc1	August
Saský	saský	k2eAgMnSc1d1	saský
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
předposledního	předposlední	k2eAgMnSc2d1	předposlední
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgFnSc6d1	Napoleonova
porážce	porážka	k1gFnSc6	porážka
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
autonomní	autonomní	k2eAgNnSc1d1	autonomní
Království	království	k1gNnSc1	království
polské	polský	k2eAgNnSc1d1	polské
také	také	k9	také
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
"	"	kIx"	"
<g/>
Kongresovka	Kongresovka	k1gFnSc1	Kongresovka
<g/>
"	"	kIx"	"
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
ruskem	ruskem	k6eAd1	ruskem
a	a	k8xC	a
"	"	kIx"	"
<g/>
autonomní	autonomní	k2eAgNnSc1d1	autonomní
<g/>
"	"	kIx"	"
velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
poznaňské	poznaňský	k2eAgNnSc1d1	poznaňské
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
Pruskem	Prusko	k1gNnSc7	Prusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kongresové	kongresový	k2eAgNnSc1d1	Kongresové
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
–	–	k?	–
Kongresovka	Kongresovka	k1gFnSc1	Kongresovka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
pádu	pád	k1gInSc6	pád
bylo	být	k5eAaImAgNnS	být
Vídeňským	vídeňský	k2eAgInSc7d1	vídeňský
kongresem	kongres	k1gInSc7	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
Varšavského	varšavský	k2eAgNnSc2d1	Varšavské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgInSc2	který
bylo	být	k5eAaImAgNnS	být
odděleno	oddělit	k5eAaPmNgNnS	oddělit
Poznaňsko	Poznaňsko	k1gNnSc1	Poznaňsko
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Prusku	Prusko	k1gNnSc3	Prusko
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
autonomní	autonomní	k2eAgNnSc1d1	autonomní
<g/>
"	"	kIx"	"
Velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
poznaňské	poznaňský	k2eAgNnSc1d1	poznaňské
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
odděleno	oddělen	k2eAgNnSc1d1	odděleno
město	město	k1gNnSc1	město
Krakov	Krakov	k1gInSc1	Krakov
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
svobodným	svobodný	k2eAgNnSc7d1	svobodné
městem	město	k1gNnSc7	město
Krakov	Krakov	k1gInSc1	Krakov
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
všech	všecek	k3xTgFnPc2	všecek
3	[number]	k4	3
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
celé	celý	k2eAgNnSc1d1	celé
rozdělení	rozdělení	k1gNnSc1	rozdělení
bývá	bývat	k5eAaImIp3nS	bývat
nazýváno	nazývat	k5eAaImNgNnS	nazývat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
čtvrté	čtvrtá	k1gFnPc1	čtvrtá
dělení	dělení	k1gNnSc2	dělení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
uvedeným	uvedený	k2eAgFnPc3d1	uvedená
obnoveným	obnovený	k2eAgNnPc3d1	obnovené
Polským	polský	k2eAgNnPc3d1	polské
královstvím	království	k1gNnPc3	království
více	hodně	k6eAd2	hodně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
Kongresové	kongresový	k2eAgNnSc1d1	Kongresové
Polsko	Polsko	k1gNnSc1	Polsko
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
"	"	kIx"	"
<g/>
kongresovka	kongresovka	k1gFnSc1	kongresovka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
dědičně	dědičně	k6eAd1	dědičně
určen	určen	k2eAgMnSc1d1	určen
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
polské	polský	k2eAgInPc1d1	polský
stavy	stav	k1gInPc1	stav
přijaly	přijmout	k5eAaPmAgInP	přijmout
za	za	k7c4	za
krále	král	k1gMnPc4	král
(	(	kIx(	(
<g/>
musely	muset	k5eAaImAgFnP	muset
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
na	na	k7c4	na
polského	polský	k2eAgMnSc4d1	polský
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Personální	personální	k2eAgFnSc1d1	personální
unie	unie	k1gFnSc1	unie
však	však	k9	však
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
podání	podání	k1gNnSc6	podání
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
nefungovala	fungovat	k5eNaImAgFnS	fungovat
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
reálná	reálný	k2eAgFnSc1d1	reálná
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
carem	car	k1gMnSc7	car
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
také	také	k9	také
korunovat	korunovat	k5eAaBmF	korunovat
polským	polský	k2eAgMnSc7d1	polský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snažil	snažit	k5eAaImAgMnS	snažit
omezit	omezit	k5eAaPmF	omezit
polskou	polský	k2eAgFnSc4d1	polská
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
k	k	k7c3	k
devět	devět	k4xCc4	devět
ničivých	ničivý	k2eAgInPc2d1	ničivý
měsíců	měsíc	k1gInPc2	měsíc
trvajícímu	trvající	k2eAgNnSc3d1	trvající
tzv.	tzv.	kA	tzv.
Listopadové	listopadový	k2eAgNnSc1d1	listopadové
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
carskými	carský	k2eAgFnPc7d1	carská
silami	síla	k1gFnPc7	síla
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polské	polský	k2eAgNnSc4d1	polské
kongresové	kongresový	k2eAgNnSc4d1	Kongresové
království	království	k1gNnSc4	království
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
postavení	postavení	k1gNnSc1	postavení
suverénního	suverénní	k2eAgInSc2d1	suverénní
státu	stát	k1gInSc2	stát
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k6eAd1	tak
de	de	k?	de
facto	facto	k1gNnSc4	facto
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
stejně	stejně	k6eAd1	stejně
proměnila	proměnit	k5eAaPmAgFnS	proměnit
v	v	k7c6	v
unii	unie	k1gFnSc6	unie
reálnou	reálný	k2eAgFnSc7d1	reálná
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
bylo	být	k5eAaImAgNnS	být
degradováno	degradovat	k5eAaBmNgNnS	degradovat
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
provincie	provincie	k1gFnSc2	provincie
ruské	ruský	k2eAgFnSc2d1	ruská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
o	o	k7c6	o
posledním	poslední	k2eAgInSc6d1	poslední
byt	byt	k1gInSc1	byt
formální	formální	k2eAgFnSc6d1	formální
polském	polský	k2eAgMnSc6d1	polský
králi	král	k1gMnSc6	král
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
Alexandrovi	Alexandrův	k2eAgMnPc1d1	Alexandrův
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgMnPc1d1	další
ruští	ruský	k2eAgMnPc1d1	ruský
carové	car	k1gMnPc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
již	již	k6eAd1	již
nosily	nosit	k5eAaImAgInP	nosit
titul	titul	k1gInSc4	titul
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
pouze	pouze	k6eAd1	pouze
titulárně	titulárně	k6eAd1	titulárně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
zbytků	zbytek	k1gInPc2	zbytek
polské	polský	k2eAgFnSc2d1	polská
autonomie	autonomie	k1gFnSc2	autonomie
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
protiruského	protiruský	k2eAgNnSc2d1	protiruské
Lednového	lednový	k2eAgNnSc2d1	lednové
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
včleněno	včleněn	k2eAgNnSc1d1	včleněno
do	do	k7c2	do
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
jako	jako	k8xS	jako
Poviselský	poviselský	k2eAgInSc1d1	poviselský
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
П	П	k?	П
к	к	k?	к
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
a	a	k8xC	a
nesmlouvavá	smlouvavý	k2eNgFnSc1d1	nesmlouvavá
rusifikace	rusifikace	k1gFnSc1	rusifikace
</s>
</p>
<p>
<s>
==	==	k?	==
Regentské	regentský	k2eAgNnSc1d1	regentský
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Ústředními	ústřední	k2eAgInPc7d1	ústřední
mocnostmi	mocnost	k1gFnPc7	mocnost
tj.	tj.	kA	tj.
Německým	německý	k2eAgNnSc7d1	německé
císařstvím	císařství	k1gNnSc7	císařství
a	a	k8xC	a
Rakouskem-Uherskem	Rakouskem-Uhersko	k1gNnSc7	Rakouskem-Uhersko
resp.	resp.	kA	resp.
<g/>
proklamací	proklamace	k1gFnPc2	proklamace
německým	německý	k2eAgMnSc7d1	německý
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
rakouským	rakouský	k2eAgMnSc7d1	rakouský
císařem	císař	k1gMnSc7	císař
německého	německý	k2eAgInSc2d1	německý
na	na	k7c6	na
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
obývaných	obývaný	k2eAgInPc2d1	obývaný
polským	polský	k2eAgNnSc7d1	polské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
vytvořeno	vytvořen	k2eAgNnSc1d1	vytvořeno
tzv.	tzv.	kA	tzv.
Regentské	regentský	k2eAgNnSc1d1	regentský
království	království	k1gNnSc1	království
polské	polský	k2eAgNnSc1d1	polské
někdy	někdy	k6eAd1	někdy
označované	označovaný	k2eAgNnSc1d1	označované
jako	jako	k8xS	jako
Polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
či	či	k8xC	či
obnovení	obnovení	k1gNnSc4	obnovení
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
polské	polský	k2eAgNnSc1d1	polské
(	(	kIx(	(
<g/>
regentské	regentský	k2eAgNnSc1d1	regentský
<g/>
)	)	kIx)	)
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
vazalem	vazal	k1gMnSc7	vazal
Ústředních	ústřední	k2eAgFnPc2d1	ústřední
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
,	,	kIx,	,
vládla	vládnout	k5eAaImAgFnS	vládnout
za	za	k7c4	za
něj	on	k3xPp3gInSc4	on
tříčlenná	tříčlenný	k2eAgFnSc1d1	tříčlenná
regentská	regentský	k2eAgFnSc1d1	regentská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
ovládáno	ovládat	k5eAaImNgNnS	ovládat
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
varšavským	varšavský	k2eAgMnSc7d1	varšavský
za	za	k7c4	za
Německé	německý	k2eAgNnSc4d1	německé
císařství	císařství	k1gNnSc4	císařství
(	(	kIx(	(
<g/>
generál	generál	k1gMnSc1	generál
Hans	hansa	k1gFnPc2	hansa
von	von	k1gInSc1	von
Beseler	Beseler	k1gMnSc1	Beseler
<g/>
)	)	kIx)	)
a	a	k8xC	a
generálním	generální	k2eAgMnSc7d1	generální
guvernérem	guvernér	k1gMnSc7	guvernér
lublinským	lublinský	k2eAgMnSc7d1	lublinský
za	za	k7c4	za
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
(	(	kIx(	(
<g/>
generál	generál	k1gMnSc1	generál
Karl	Karl	k1gMnSc1	Karl
Kuk	kuka	k1gFnPc2	kuka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
polská	polský	k2eAgFnSc1d1	polská
armáda	armáda	k1gFnSc1	armáda
bojovala	bojovat	k5eAaImAgFnS	bojovat
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
německým	německý	k2eAgMnPc3d1	německý
a	a	k8xC	a
rakousko-uherským	rakouskoherský	k2eAgMnPc3d1	rakousko-uherský
proti	proti	k7c3	proti
Ruskému	ruský	k2eAgNnSc3d1	ruské
impériu	impérium	k1gNnSc3	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
na	na	k7c6	na
východě	východ	k1gInSc6	východ
nebylo	být	k5eNaImAgNnS	být
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
frontové	frontový	k2eAgFnSc2d1	frontová
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Druhá	druhý	k4xOgFnSc1	druhý
Polská	polský	k2eAgFnSc1d1	polská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tímto	tento	k3xDgNnSc7	tento
polské	polský	k2eAgNnSc1d1	polské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
prozatím	prozatím	k6eAd1	prozatím
<g/>
)	)	kIx)	)
definitivně	definitivně	k6eAd1	definitivně
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
Poland	Polanda	k1gFnPc2	Polanda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SCHOEPS	SCHOEPS	kA	SCHOEPS
<g/>
,	,	kIx,	,
Hans-Joachim	Hans-Joachim	k1gInSc1	Hans-Joachim
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86379	[number]	k4	86379
<g/>
-	-	kIx~	-
<g/>
59	[number]	k4	59
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Polska	Polsko	k1gNnSc2	Polsko
</s>
</p>
<p>
<s>
Dělení	dělení	k1gNnSc1	dělení
Polska	Polsko	k1gNnSc2	Polsko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
polských	polský	k2eAgMnPc2d1	polský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
polských	polský	k2eAgFnPc2d1	polská
kněžen	kněžna	k1gFnPc2	kněžna
a	a	k8xC	a
královen	královna	k1gFnPc2	královna
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
titulárních	titulární	k2eAgMnPc2d1	titulární
polských	polský	k2eAgMnPc2d1	polský
králů	král	k1gMnPc2	král
</s>
</p>
<p>
<s>
Pruské	pruský	k2eAgNnSc1d1	pruské
vévodství	vévodství	k1gNnSc1	vévodství
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Polské	polský	k2eAgFnSc2d1	polská
království	království	k1gNnSc4	království
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
