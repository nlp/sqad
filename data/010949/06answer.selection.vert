<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vymření	vymření	k1gNnSc2	vymření
polských	polský	k2eAgMnPc2d1	polský
piastovců	piastovec	k1gMnPc2	piastovec
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1385	[number]	k4	1385
smlouvou	smlouva	k1gFnSc7	smlouva
v	v	k7c6	v
Krevě	Kreva	k1gFnSc6	Kreva
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Litvy	Litva	k1gFnSc2	Litva
v	v	k7c6	v
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
položen	položen	k2eAgInSc4d1	položen
základ	základ	k1gInSc4	základ
budoucímu	budoucí	k2eAgNnSc3d1	budoucí
soustátí	soustátí	k1gNnSc3	soustátí
Polsko-Litva	Polsko-Litvo	k1gNnSc2	Polsko-Litvo
<g/>
.	.	kIx.	.
</s>
