<p>
<s>
Spahnův	Spahnův	k2eAgInSc1d1	Spahnův
ranč	ranč	k1gInSc1	ranč
byl	být	k5eAaImAgInS	být
filmový	filmový	k2eAgInSc1d1	filmový
ranč	ranč	k1gInSc1	ranč
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
County	Counta	k1gFnSc2	Counta
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
westernových	westernový	k2eAgInPc2d1	westernový
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Horský	horský	k2eAgInSc4d1	horský
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
balvany	balvan	k1gInPc4	balvan
a	a	k8xC	a
staré	starý	k2eAgNnSc4d1	staré
westernové	westernový	k2eAgNnSc4d1	westernové
městečko	městečko	k1gNnSc4	městečko
dělali	dělat	k5eAaImAgMnP	dělat
ze	z	k7c2	z
Spahnova	Spahnův	k2eAgInSc2d1	Spahnův
ranče	ranč	k1gInSc2	ranč
všestranné	všestranný	k2eAgNnSc1d1	všestranné
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
mnoha	mnoho	k4c2	mnoho
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
Simi	Sim	k1gInSc6	Sim
Hills	Hills	k1gInSc1	Hills
a	a	k8xC	a
Santa	Santa	k1gFnSc1	Santa
Susana	Susana	k1gFnSc1	Susana
Mountains	Mountains	k1gInSc4	Mountains
nad	nad	k7c4	nad
Chatsworth	Chatsworth	k1gInSc4	Chatsworth
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
ranč	ranč	k1gFnSc1	ranč
součástí	součást	k1gFnSc7	součást
Santa	Sant	k1gMnSc2	Sant
Susana	Susan	k1gMnSc2	Susan
Pass	Pass	k1gInSc1	Pass
State	status	k1gInSc5	status
Historic	Historic	k1gMnSc1	Historic
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Ranč	ranč	k1gInSc1	ranč
byl	být	k5eAaImAgInS	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1970	[number]	k4	1970
zničen	zničit	k5eAaPmNgInS	zničit
lesním	lesní	k2eAgInSc7d1	lesní
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
</s>
</p>
