<p>
<s>
Milimetr	milimetr	k1gInSc1	milimetr
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
milimetr	milimetr	k1gInSc1	milimetr
je	být	k5eAaImIp3nS	být
tisícina	tisícina	k1gFnSc1	tisícina
metru	metr	k1gInSc2	metr
<g/>
:	:	kIx,	:
1	[number]	k4	1
mm	mm	kA	mm
=	=	kIx~	=
0,001	[number]	k4	0,001
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Udávání	udávání	k1gNnSc1	udávání
rozměrů	rozměr	k1gInPc2	rozměr
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
například	například	k6eAd1	například
ve	v	k7c6	v
strojnictví	strojnictví	k1gNnSc6	strojnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
za	za	k7c4	za
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
také	také	k9	také
udává	udávat	k5eAaImIp3nS	udávat
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
meteorologii	meteorologie	k1gFnSc6	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
milimetr	milimetr	k1gInSc1	milimetr
srážek	srážka	k1gFnPc2	srážka
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
metr	metr	k1gInSc4	metr
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
napršel	napršet	k5eAaPmAgInS	napršet
jeden	jeden	k4xCgInSc1	jeden
litr	litr	k1gInSc1	litr
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
1	[number]	k4	1
mm	mm	kA	mm
×	×	k?	×
1	[number]	k4	1
m2	m2	k4	m2
=	=	kIx~	=
0,001	[number]	k4	0,001
m3	m3	k4	m3
=	=	kIx~	=
1	[number]	k4	1
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Řádová	řádový	k2eAgFnSc1d1	řádová
velikost	velikost	k1gFnSc1	velikost
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
</s>
</p>
