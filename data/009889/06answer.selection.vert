<s>
Jako	jako	k8xC	jako
tudorovské	tudorovský	k2eAgNnSc1d1	tudorovské
období	období	k1gNnSc1	období
v	v	k7c6	v
anglických	anglický	k2eAgFnPc6d1	anglická
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
doba	doba	k1gFnSc1	doba
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
po	po	k7c4	po
konec	konec	k1gInSc4	konec
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1558	[number]	k4	1558
<g/>
.	.	kIx.	.
</s>
