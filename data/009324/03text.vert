<p>
<s>
Reinhard	Reinhard	k1gInSc1	Reinhard
Tristan	Tristan	k1gInSc1	Tristan
Eugen	Eugen	k2eAgInSc1d1	Eugen
Heydrich	Heydrich	k1gInSc1	Heydrich
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1904	[number]	k4	1904
Halle	Halle	k1gFnSc2	Halle
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Saale	Saale	k1gInSc1	Saale
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prominentní	prominentní	k2eAgMnSc1d1	prominentní
nacista	nacista	k1gMnSc1	nacista
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgMnSc1d1	blízký
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Heinricha	Heinrich	k1gMnSc4	Heinrich
Himmlera	Himmler	k1gMnSc4	Himmler
<g/>
,	,	kIx,	,
SS-Obergruppenführer	SS-Obergruppenführer	k1gMnSc1	SS-Obergruppenführer
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
Hlavního	hlavní	k2eAgInSc2d1	hlavní
úřadu	úřad	k1gInSc2	úřad
říšské	říšský	k2eAgFnSc2d1	říšská
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
RSHA	RSHA	kA	RSHA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
služby	služba	k1gFnPc1	služba
(	(	kIx(	(
<g/>
SD	SD	kA	SD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Interpolu	interpol	k1gInSc2	interpol
v	v	k7c6	v
letech	let	k1gInPc6	let
1940	[number]	k4	1940
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
zastupující	zastupující	k2eAgInSc4d1	zastupující
říšský	říšský	k2eAgInSc4d1	říšský
protektor	protektor	k1gInSc4	protektor
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
perzekuci	perzekuce	k1gFnSc6	perzekuce
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
organizátorů	organizátor	k1gMnPc2	organizátor
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Předsedal	předsedat	k5eAaImAgMnS	předsedat
konferenci	konference	k1gFnSc4	konference
ve	v	k7c6	v
Wannsee	Wannsee	k1gFnSc6	Wannsee
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uvedla	uvést	k5eAaPmAgFnS	uvést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
tzv.	tzv.	kA	tzv.
Konečné	Konečné	k2eAgNnSc1d1	Konečné
řešení	řešení	k1gNnSc1	řešení
židovské	židovský	k2eAgFnSc2d1	židovská
otázky	otázka	k1gFnSc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zastupující	zastupující	k2eAgMnSc1d1	zastupující
říšský	říšský	k2eAgMnSc1d1	říšský
protektor	protektor	k1gMnSc1	protektor
využíval	využívat	k5eAaPmAgMnS	využívat
politiku	politika	k1gFnSc4	politika
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
biče	bič	k1gInSc2	bič
–	–	k?	–
sociální	sociální	k2eAgFnPc4d1	sociální
výhody	výhoda	k1gFnPc4	výhoda
dělníkům	dělník	k1gMnPc3	dělník
pracujícím	pracující	k2eAgMnPc3d1	pracující
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnSc2	potřeba
Třetí	třetí	k4xOgFnSc1	třetí
Říše	říše	k1gFnSc1	říše
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
brutální	brutální	k2eAgFnSc4d1	brutální
perzekuci	perzekuce	k1gFnSc4	perzekuce
vůči	vůči	k7c3	vůči
neloajálním	loajální	k2eNgInPc3d1	neloajální
a	a	k8xC	a
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Českoslovenští	československý	k2eAgMnPc1d1	československý
výsadkáři	výsadkář	k1gMnPc1	výsadkář
vyslaní	vyslanit	k5eAaPmIp3nP	vyslanit
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
provedli	provést	k5eAaPmAgMnP	provést
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
speciální	speciální	k2eAgFnSc4d1	speciální
diverzní	diverzní	k2eAgFnSc4d1	diverzní
operaci	operace	k1gFnSc4	operace
jeho	jeho	k3xOp3gFnSc2	jeho
likvidace	likvidace	k1gFnSc2	likvidace
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
následky	následek	k1gInPc4	následek
po	po	k7c6	po
osmi	osm	k4xCc6	osm
dnech	den	k1gInPc6	den
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
bývá	bývat	k5eAaImIp3nS	bývat
dodnes	dodnes	k6eAd1	dodnes
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
atentát	atentát	k1gInSc4	atentát
<g/>
"	"	kIx"	"
přesně	přesně	k6eAd1	přesně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
od	od	k7c2	od
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
šířila	šířit	k5eAaImAgFnS	šířit
nacistická	nacistický	k2eAgFnSc1d1	nacistická
propaganda	propaganda	k1gFnSc1	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odplatě	odplata	k1gFnSc6	odplata
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
zavedli	zavést	k5eAaPmAgMnP	zavést
nacisté	nacista	k1gMnPc1	nacista
krutá	krutý	k2eAgNnPc4d1	kruté
opatření	opatření	k1gNnPc4	opatření
známá	známý	k2eAgNnPc4d1	známé
jako	jako	k8xS	jako
druhá	druhý	k4xOgFnSc1	druhý
heydrichiáda	heydrichiáda	k1gFnSc1	heydrichiáda
a	a	k8xC	a
povraždili	povraždit	k5eAaPmAgMnP	povraždit
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
Protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
"	"	kIx"	"
<g/>
počest	počest	k1gFnSc4	počest
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
operace	operace	k1gFnSc1	operace
Reinhard	Reinharda	k1gFnPc2	Reinharda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prakticky	prakticky	k6eAd1	prakticky
vyhladila	vyhladit	k5eAaPmAgFnS	vyhladit
židovské	židovský	k2eAgNnSc4d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Reinhard	Reinhard	k1gInSc1	Reinhard
Tristan	Tristan	k1gInSc1	Tristan
Eugen	Eugna	k1gFnPc2	Eugna
Heydrich	Heydrich	k1gMnSc1	Heydrich
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Halle	Halla	k1gFnSc6	Halla
nad	nad	k7c7	nad
Sálou	Sála	k1gFnSc7	Sála
nedaleko	nedaleko	k7c2	nedaleko
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc2d1	situovaná
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
pěvec	pěvec	k1gMnSc1	pěvec
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ředitel	ředitel	k1gMnSc1	ředitel
hallské	hallský	k2eAgFnSc2d1	hallský
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Richard	Richard	k1gMnSc1	Richard
Bruno	Bruno	k1gMnSc1	Bruno
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svých	svůj	k3xOyFgMnPc2	svůj
úspěchů	úspěch	k1gInPc2	úspěch
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
hudebním	hudební	k2eAgMnSc7d1	hudební
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
píli	píle	k1gFnSc3	píle
a	a	k8xC	a
odhodlání	odhodlání	k1gNnSc4	odhodlání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
Anna	Anna	k1gFnSc1	Anna
Maria	Maria	k1gFnSc1	Maria
Amalia	Amalia	k1gFnSc1	Amalia
Heydrichová	Heydrichový	k2eAgFnSc1d1	Heydrichový
rozená	rozený	k2eAgFnSc1d1	rozená
Krantzová	Krantzová	k1gFnSc1	Krantzová
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
Reinhard	Reinharda	k1gFnPc2	Reinharda
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
obdržel	obdržet	k5eAaPmAgInS	obdržet
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
hrdinů	hrdina	k1gMnPc2	hrdina
oper	opera	k1gFnPc2	opera
Amen	amen	k1gNnSc2	amen
(	(	kIx(	(
<g/>
dílo	dílo	k1gNnSc1	dílo
Richarda	Richard	k1gMnSc2	Richard
Heydricha	Heydrich	k1gMnSc2	Heydrich
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
a	a	k8xC	a
Izolda	Izolda	k1gFnSc1	Izolda
(	(	kIx(	(
<g/>
dílo	dílo	k1gNnSc1	dílo
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Eugen	Eugen	k1gInSc1	Eugen
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgInS	mít
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
dědečkovi	dědeček	k1gMnSc3	dědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
–	–	k?	–
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Eugen	Eugna	k1gFnPc2	Eugna
Krantz	Krantz	k1gMnSc1	Krantz
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
ředitele	ředitel	k1gMnSc2	ředitel
Drážďanské	drážďanský	k2eAgFnSc2d1	Drážďanská
královské	královský	k2eAgFnSc2d1	královská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
jako	jako	k9	jako
14	[number]	k4	14
<g/>
letý	letý	k2eAgInSc4d1	letý
mladík	mladík	k1gInSc4	mladík
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gInSc7	člen
Freikorpsu	Freikorpsa	k1gFnSc4	Freikorpsa
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gFnPc6	jejichž
řadách	řada	k1gFnPc6	řada
působil	působit	k5eAaImAgInS	působit
přibližně	přibližně	k6eAd1	přibližně
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
proti	proti	k7c3	proti
bolševické	bolševický	k2eAgFnSc3d1	bolševická
revoluci	revoluce	k1gFnSc3	revoluce
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
však	však	k9	však
byla	být	k5eAaImAgFnS	být
formální	formální	k2eAgFnSc1d1	formální
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
věku	věk	k1gInSc3	věk
a	a	k8xC	a
nulovým	nulový	k2eAgFnPc3d1	nulová
bojovým	bojový	k2eAgFnPc3d1	bojová
zkušenostem	zkušenost	k1gFnPc3	zkušenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
k	k	k7c3	k
námořnictvu	námořnictvo	k1gNnSc3	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Kielu	Kiel	k1gInSc6	Kiel
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
na	na	k7c6	na
křižníku	křižník	k1gInSc6	křižník
německého	německý	k2eAgNnSc2d1	německé
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
předchozí	předchozí	k2eAgFnSc6d1	předchozí
milostné	milostný	k2eAgFnSc6d1	milostná
aféře	aféra	k1gFnSc6	aféra
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
dívkou	dívka	k1gFnSc7	dívka
obviněn	obvinit	k5eAaPmNgInS	obvinit
z	z	k7c2	z
nečestného	čestný	k2eNgNnSc2d1	nečestné
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
stavu	stav	k1gInSc2	stav
<g/>
)	)	kIx)	)
z	z	k7c2	z
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
propuštěn	propustit	k5eAaPmNgMnS	propustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
u	u	k7c2	u
SS	SS	kA	SS
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Linou	Lina	k1gFnSc7	Lina
von	von	k1gInSc4	von
Ostenovou	Ostenová	k1gFnSc7	Ostenová
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
přivedla	přivést	k5eAaPmAgFnS	přivést
k	k	k7c3	k
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
založil	založit	k5eAaPmAgMnS	založit
Bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
službu	služba	k1gFnSc4	služba
–	–	k?	–
Sicherheitsdienst	Sicherheitsdienst	k1gFnSc1	Sicherheitsdienst
(	(	kIx(	(
<g/>
SD	SD	kA	SD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
"	"	kIx"	"
<g/>
noci	noc	k1gFnPc4	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
padělatel	padělatel	k1gMnSc1	padělatel
důkazů	důkaz	k1gInPc2	důkaz
proti	proti	k7c3	proti
Ernstu	Ernst	k1gMnSc3	Ernst
Röhmovi	Röhm	k1gMnSc3	Röhm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInPc7	jeho
vedením	vedení	k1gNnSc7	vedení
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Hlavní	hlavní	k2eAgInSc1d1	hlavní
úřad	úřad	k1gInSc1	úřad
říšské	říšský	k2eAgFnSc2d1	říšská
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
RSHA	RSHA	kA	RSHA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
šlechtičnou	šlechtična	k1gFnSc7	šlechtična
Linou	linout	k5eAaImIp3nP	linout
von	von	k1gInSc4	von
Osten	osten	k1gInSc1	osten
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
Heydrich	Heydrich	k1gMnSc1	Heydrich
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heider	Heider	k1gMnSc1	Heider
Heydrich	Heydrich	k1gMnSc1	Heydrich
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Silke	Silke	k1gFnSc1	Silke
Heydrich	Heydricha	k1gFnPc2	Heydricha
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marte	Mars	k1gMnSc5	Mars
Heydrich	Heydrich	k1gMnSc1	Heydrich
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
šermem	šerm	k1gInSc7	šerm
<g/>
,	,	kIx,	,
v	v	k7c6	v
šermu	šerm	k1gInSc6	šerm
byl	být	k5eAaImAgMnS	být
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
přeborník	přeborník	k1gMnSc1	přeborník
SS	SS	kA	SS
<g/>
,	,	kIx,	,
ovládal	ovládat	k5eAaImAgInS	ovládat
plynně	plynně	k6eAd1	plynně
několik	několik	k4yIc4	několik
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Heydrich	Heydrich	k1gMnSc1	Heydrich
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
přepadení	přepadení	k1gNnSc4	přepadení
vysílačky	vysílačka	k1gFnSc2	vysílačka
v	v	k7c6	v
Gliwicích	Gliwice	k1gFnPc6	Gliwice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
doložit	doložit	k5eAaPmF	doložit
polskou	polský	k2eAgFnSc4d1	polská
agresi	agrese	k1gFnSc4	agrese
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
před	před	k7c7	před
světem	svět	k1gInSc7	svět
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
německý	německý	k2eAgInSc1d1	německý
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
rezervním	rezervní	k2eAgInSc7d1	rezervní
stíhacím	stíhací	k2eAgInSc7d1	stíhací
pilotem	pilot	k1gInSc7	pilot
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
<g/>
,	,	kIx,	,
podnikl	podniknout	k5eAaPmAgMnS	podniknout
dokonce	dokonce	k9	dokonce
několik	několik	k4yIc4	několik
bojových	bojový	k2eAgInPc2d1	bojový
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgInS	být
sestřelen	sestřelen	k2eAgInSc1d1	sestřelen
nad	nad	k7c7	nad
nepřátelským	přátelský	k2eNgNnSc7d1	nepřátelské
územím	území	k1gNnSc7	území
a	a	k8xC	a
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
nezvěstný	zvěstný	k2eNgMnSc1d1	nezvěstný
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
Hitler	Hitler	k1gMnSc1	Hitler
další	další	k2eAgFnPc4d1	další
bojové	bojový	k2eAgFnPc4d1	bojová
lety	léto	k1gNnPc7	léto
zakázal	zakázat	k5eAaPmAgMnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
okupaci	okupace	k1gFnSc6	okupace
Polska	Polsko	k1gNnSc2	Polsko
se	se	k3xPyFc4	se
Heydrich	Heydrich	k1gInSc1	Heydrich
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
vyhlazovacích	vyhlazovací	k2eAgFnPc6d1	vyhlazovací
akcích	akce	k1gFnPc6	akce
proti	proti	k7c3	proti
Polákům	Polák	k1gMnPc3	Polák
pod	pod	k7c7	pod
krycími	krycí	k2eAgNnPc7d1	krycí
jmény	jméno	k1gNnPc7	jméno
operace	operace	k1gFnSc2	operace
Tannenberg	Tannenberg	k1gInSc1	Tannenberg
a	a	k8xC	a
Intelligenzaktion	Intelligenzaktion	k1gInSc1	Intelligenzaktion
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
příslušníků	příslušník	k1gMnPc2	příslušník
polské	polský	k2eAgFnSc2d1	polská
elity	elita	k1gFnSc2	elita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Holokaust	holokaust	k1gInSc4	holokaust
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1942	[number]	k4	1942
předsedal	předsedat	k5eAaImAgMnS	předsedat
konferenci	konference	k1gFnSc4	konference
ve	v	k7c6	v
Wannsee	Wannsee	k1gFnSc6	Wannsee
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
usmrcení	usmrcení	k1gNnSc4	usmrcení
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
židovské	židovský	k2eAgFnSc2d1	židovská
národnosti	národnost	k1gFnSc2	národnost
ve	v	k7c6	v
vyhlazovacích	vyhlazovací	k2eAgInPc6d1	vyhlazovací
táborech	tábor	k1gInPc6	tábor
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Majdanek	Majdanka	k1gFnPc2	Majdanka
<g/>
,	,	kIx,	,
Treblinka	Treblinka	k1gFnSc1	Treblinka
<g/>
,	,	kIx,	,
Osvětim	Osvětim	k1gFnSc1	Osvětim
<g/>
,	,	kIx,	,
Chelmno	Chelmno	k6eAd1	Chelmno
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zastupující	zastupující	k2eAgInSc1d1	zastupující
říšský	říšský	k2eAgInSc1d1	říšský
protektor	protektor	k1gInSc1	protektor
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
den	den	k1gInSc4	den
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
stal	stát	k5eAaPmAgInS	stát
zastupujícím	zastupující	k2eAgInSc7d1	zastupující
říšským	říšský	k2eAgInSc7d1	říšský
protektorem	protektor	k1gInSc7	protektor
pro	pro	k7c4	pro
Protektorát	protektorát	k1gInSc4	protektorát
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
též	též	k9	též
ředitelem	ředitel	k1gMnSc7	ředitel
protektorátní	protektorátní	k2eAgFnSc2d1	protektorátní
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Panenských	panenský	k2eAgInPc6d1	panenský
Břežanech	Břežany	k1gInPc6	Břežany
asi	asi	k9	asi
20	[number]	k4	20
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Heydrich	Heydrich	k1gMnSc1	Heydrich
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
konečné	konečný	k2eAgNnSc4d1	konečné
řešení	řešení	k1gNnSc4	řešení
české	český	k2eAgFnSc2d1	Česká
otázky	otázka	k1gFnSc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1941	[number]	k4	1941
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
kruhu	kruh	k1gInSc6	kruh
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Čech	Čech	k1gMnSc1	Čech
v	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
prostoru	prostor	k1gInSc6	prostor
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
koneckonců	koneckonců	k9	koneckonců
co	co	k3yQnSc4	co
pohledávat	pohledávat	k5eAaImF	pohledávat
<g/>
"	"	kIx"	"
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
pokoušet	pokoušet	k5eAaImF	pokoušet
"	"	kIx"	"
<g/>
předělávat	předělávat	k5eAaImF	předělávat
tuhle	tenhle	k3xDgFnSc4	tenhle
svoloč	svoloč	k1gFnSc4	svoloč
českou	český	k2eAgFnSc4d1	Česká
podle	podle	k7c2	podle
staré	starý	k2eAgFnSc2d1	stará
metody	metoda	k1gFnSc2	metoda
na	na	k7c4	na
Němce	Němec	k1gMnPc4	Němec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Panenských	panenský	k2eAgInPc2d1	panenský
Břežan	Břežany	k1gInPc2	Břežany
do	do	k7c2	do
kanceláře	kancelář	k1gFnSc2	kancelář
jeho	jeho	k3xOp3gInSc2	jeho
úřadu	úřad	k1gInSc2	úřad
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
Praze-Libni	Praze-Libni	k1gFnSc6	Praze-Libni
provedli	provést	k5eAaPmAgMnP	provést
atentát	atentát	k1gInSc4	atentát
českoslovenští	československý	k2eAgMnPc1d1	československý
výsadkáři	výsadkář	k1gMnPc1	výsadkář
Jan	Jan	k1gMnSc1	Jan
Kubiš	Kubiš	k1gMnSc1	Kubiš
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Gabčík	Gabčík	k1gMnSc1	Gabčík
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
jeho	jeho	k3xOp3gNnSc4	jeho
zranění	zranění	k1gNnSc4	zranění
nevypadala	vypadat	k5eNaPmAgFnS	vypadat
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
atentátníky	atentátník	k1gMnPc4	atentátník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
Na	na	k7c6	na
Bulovce	Bulovka	k1gFnSc6	Bulovka
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
zotavoval	zotavovat	k5eAaImAgMnS	zotavovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
objevily	objevit	k5eAaPmAgFnP	objevit
nečekané	čekaný	k2eNgFnPc1d1	nečekaná
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
následky	následek	k1gInPc4	následek
zemřel	zemřít	k5eAaPmAgMnS	zemřít
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyla	být	k5eNaImAgFnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
antibiotika	antibiotikum	k1gNnSc2	antibiotikum
<g/>
)	)	kIx)	)
–	–	k?	–
smrt	smrt	k1gFnSc1	smrt
nastala	nastat	k5eAaPmAgFnS	nastat
následkem	následkem	k7c2	následkem
poškození	poškození	k1gNnSc2	poškození
životně	životně	k6eAd1	životně
důležitých	důležitý	k2eAgInPc2d1	důležitý
parenchymatózních	parenchymatózní	k2eAgInPc2d1	parenchymatózní
orgánů	orgán	k1gInPc2	orgán
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jejich	jejich	k3xOp3gInPc1	jejich
jedy	jed	k1gInPc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
zavlečeny	zavleknout	k5eAaPmNgFnP	zavleknout
střelným	střelný	k2eAgNnSc7d1	střelné
poraněním	poranění	k1gNnSc7	poranění
výbušninou	výbušnina	k1gFnSc7	výbušnina
a	a	k8xC	a
usídlily	usídlit	k5eAaPmAgFnP	usídlit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
hrudní	hrudní	k2eAgFnSc6d1	hrudní
dutině	dutina	k1gFnSc6	dutina
<g/>
,	,	kIx,	,
v	v	k7c6	v
bránici	bránice	k1gFnSc6	bránice
a	a	k8xC	a
krajině	krajina	k1gFnSc6	krajina
sleziny	slezina	k1gFnSc2	slezina
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
pomnožily	pomnožit	k5eAaPmAgFnP	pomnožit
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
poctami	pocta	k1gFnPc7	pocta
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
invalidů	invalid	k1gMnPc2	invalid
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
již	již	k9	již
jeho	jeho	k3xOp3gInSc1	jeho
hrob	hrob	k1gInSc1	hrob
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německé	německý	k2eAgFnPc1d1	německá
represálie	represálie	k1gFnPc1	represálie
na	na	k7c6	na
území	území	k1gNnSc6	území
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
následovaly	následovat	k5eAaImAgInP	následovat
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
a	a	k8xC	a
zejména	zejména	k9	zejména
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
neznaly	znát	k5eNaImAgFnP	znát
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
heydrichiáda	heydrichiáda	k1gFnSc1	heydrichiáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
stříleni	střílet	k5eAaImNgMnP	střílet
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
vyvraždění	vyvraždění	k1gNnSc1	vyvraždění
obcí	obec	k1gFnPc2	obec
Lidice	Lidice	k1gInPc4	Lidice
a	a	k8xC	a
Ležáky	Ležáky	k1gInPc4	Ležáky
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
epizod	epizoda	k1gFnPc2	epizoda
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
německého	německý	k2eAgInSc2d1	německý
útlaku	útlak	k1gInSc2	útlak
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
odvetě	odveta	k1gFnSc6	odveta
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
zabito	zabít	k5eAaPmNgNnS	zabít
minimálně	minimálně	k6eAd1	minimálně
1585	[number]	k4	1585
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
údaj	údaj	k1gInSc4	údaj
stanných	stanný	k2eAgInPc2d1	stanný
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
bude	být	k5eAaImBp3nS	být
nicméně	nicméně	k8xC	nicméně
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
odbojový	odbojový	k2eAgInSc4d1	odbojový
čin	čin	k1gInSc4	čin
v	v	k7c6	v
okupované	okupovaný	k2eAgFnSc6d1	okupovaná
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
takový	takový	k3xDgInSc4	takový
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
že	že	k8xS	že
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
odvolaly	odvolat	k5eAaPmAgInP	odvolat
své	svůj	k3xOyFgNnSc4	svůj
podpisy	podpis	k1gInPc1	podpis
pod	pod	k7c7	pod
Mnichovským	mnichovský	k2eAgInSc7d1	mnichovský
diktátem	diktát	k1gInSc7	diktát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
připravil	připravit	k5eAaPmAgInS	připravit
Československo	Československo	k1gNnSc4	Československo
o	o	k7c6	o
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
třetinu	třetina	k1gFnSc4	třetina
jeho	on	k3xPp3gNnSc2	on
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
historii	historie	k1gFnSc6	historie
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
ojedinělý	ojedinělý	k2eAgInSc4d1	ojedinělý
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
podařilo	podařit	k5eAaPmAgNnS	podařit
násilně	násilně	k6eAd1	násilně
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
některého	některý	k3yIgMnSc4	některý
z	z	k7c2	z
vysokých	vysoká	k1gFnPc2	vysoká
nacistických	nacistický	k2eAgMnPc2d1	nacistický
pohlavárů	pohlavár	k1gMnPc2	pohlavár
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
zcela	zcela	k6eAd1	zcela
unikátní	unikátní	k2eAgFnSc3d1	unikátní
akci	akce	k1gFnSc3	akce
<g/>
.	.	kIx.	.
</s>
<s>
Kruté	krutý	k2eAgNnSc1d1	kruté
řádění	řádění	k1gNnSc1	řádění
nacistů	nacista	k1gMnPc2	nacista
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
odvetou	odveta	k1gFnSc7	odveta
za	za	k7c4	za
Heydrichovu	Heydrichův	k2eAgFnSc4d1	Heydrichova
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
uznání	uznání	k1gNnSc1	uznání
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
odsunu	odsun	k1gInSc2	odsun
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
zastupujícího	zastupující	k2eAgMnSc4d1	zastupující
říšského	říšský	k2eAgMnSc4d1	říšský
protektora	protektor	k1gMnSc4	protektor
Heydricha	Heydrich	k1gMnSc4	Heydrich
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Kurt	Kurt	k1gMnSc1	Kurt
Daluege	Dalueg	k1gInSc2	Dalueg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
německé	německý	k2eAgFnSc2d1	německá
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
kancléř	kancléř	k1gMnSc1	kancléř
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
<g/>
,	,	kIx,	,
říšský	říšský	k2eAgMnSc1d1	říšský
vedoucí	vedoucí	k1gMnSc1	vedoucí
SS	SS	kA	SS
<g/>
,	,	kIx,	,
Heydrichův	Heydrichův	k2eAgMnSc1d1	Heydrichův
přímý	přímý	k2eAgMnSc1d1	přímý
nadřízený	nadřízený	k1gMnSc1	nadřízený
</s>
</p>
<p>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
von	von	k1gInSc4	von
Neurath	Neurath	k1gMnSc1	Neurath
<g/>
,	,	kIx,	,
říšský	říšský	k2eAgMnSc1d1	říšský
protektor	protektor	k1gMnSc1	protektor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
postupně	postupně	k6eAd1	postupně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
kterého	který	k3yIgMnSc4	který
Heydrich	Heydrich	k1gMnSc1	Heydrich
jako	jako	k8xC	jako
zastupující	zastupující	k2eAgMnSc1d1	zastupující
říšský	říšský	k2eAgMnSc1d1	říšský
protektor	protektor	k1gMnSc1	protektor
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
de	de	k?	de
facto	facto	k1gNnSc1	facto
nahradil	nahradit	k5eAaPmAgInS	nahradit
</s>
</p>
<p>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc1	Göring
<g/>
,	,	kIx,	,
říšský	říšský	k2eAgMnSc1d1	říšský
maršál	maršál	k1gMnSc1	maršál
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Goebbels	Goebbels	k1gInSc1	Goebbels
<g/>
,	,	kIx,	,
říšský	říšský	k2eAgMnSc1d1	říšský
ministr	ministr	k1gMnSc1	ministr
propagandy	propaganda	k1gFnSc2	propaganda
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Bormann	Bormann	k1gMnSc1	Bormann
<g/>
,	,	kIx,	,
říšský	říšský	k2eAgMnSc1d1	říšský
vedoucí	vedoucí	k1gMnSc1	vedoucí
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
Hitlerův	Hitlerův	k2eAgMnSc1d1	Hitlerův
osobní	osobní	k2eAgMnSc1d1	osobní
sekretář	sekretář	k1gMnSc1	sekretář
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Müller	Müller	k1gMnSc1	Müller
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
gestapa	gestapo	k1gNnSc2	gestapo
v	v	k7c6	v
RSHA	RSHA	kA	RSHA
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Eichmann	Eichmann	k1gMnSc1	Eichmann
<g/>
,	,	kIx,	,
Heydrichův	Heydrichův	k2eAgMnSc1d1	Heydrichův
důvěrník	důvěrník	k1gMnSc1	důvěrník
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
židovských	židovský	k2eAgFnPc2d1	židovská
deportací	deportace	k1gFnPc2	deportace
do	do	k7c2	do
ghett	ghetto	k1gNnPc2	ghetto
a	a	k8xC	a
plynových	plynový	k2eAgFnPc2d1	plynová
komor	komora	k1gFnPc2	komora
v	v	k7c6	v
RSHA	RSHA	kA	RSHA
</s>
</p>
<p>
<s>
Werner	Werner	k1gMnSc1	Werner
Best	Best	k1gMnSc1	Best
<g/>
,	,	kIx,	,
Heydrichův	Heydrichův	k2eAgMnSc1d1	Heydrichův
zástupce	zástupce	k1gMnSc1	zástupce
v	v	k7c6	v
SD	SD	kA	SD
a	a	k8xC	a
v	v	k7c6	v
RSHA	RSHA	kA	RSHA
</s>
</p>
<p>
<s>
Karl	Karl	k1gMnSc1	Karl
Hermann	Hermann	k1gMnSc1	Hermann
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
Heydrichův	Heydrichův	k2eAgMnSc1d1	Heydrichův
zástupce	zástupce	k1gMnSc1	zástupce
v	v	k7c6	v
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
Schellenberg	Schellenberg	k1gMnSc1	Schellenberg
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
špionážní	špionážní	k2eAgFnSc2d1	špionážní
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
rozvědky	rozvědka	k1gFnSc2	rozvědka
v	v	k7c6	v
RSHA	RSHA	kA	RSHA
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
Ohlendorf	Ohlendorf	k1gMnSc1	Ohlendorf
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
zásahových	zásahový	k2eAgFnPc2d1	zásahová
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
vražedných	vražedný	k2eAgNnPc2d1	vražedné
komand	komando	k1gNnPc2	komando
v	v	k7c6	v
SS	SS	kA	SS
</s>
</p>
<p>
<s>
Bruno	Bruno	k1gMnSc1	Bruno
Streckenbach	Streckenbach	k1gMnSc1	Streckenbach
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
SS	SS	kA	SS
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
obchodního	obchodní	k2eAgNnSc2d1	obchodní
oddělení	oddělení	k1gNnSc2	oddělení
RSHA	RSHA	kA	RSHA
<g/>
,	,	kIx,	,
po	po	k7c6	po
Heydrichově	Heydrichův	k2eAgFnSc6d1	Heydrichova
smrti	smrt	k1gFnSc6	smrt
vedl	vést	k5eAaImAgMnS	vést
RSHA	RSHA	kA	RSHA
až	až	k9	až
do	do	k7c2	do
jmenování	jmenování	k1gNnSc2	jmenování
Ernsta	Ernst	k1gMnSc2	Ernst
Kaltenbrunnera	Kaltenbrunner	k1gMnSc2	Kaltenbrunner
</s>
</p>
<p>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
zásahové	zásahový	k2eAgFnSc2d1	zásahová
jednotky	jednotka	k1gFnSc2	jednotka
SS	SS	kA	SS
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
kriminálního	kriminální	k2eAgInSc2d1	kriminální
úřadu	úřad	k1gInSc2	úřad
RSHA	RSHA	kA	RSHA
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
udržoval	udržovat	k5eAaImAgMnS	udržovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
odbojem	odboj	k1gInSc7	odboj
</s>
</p>
<p>
<s>
Franz	Franz	k1gMnSc1	Franz
Alfred	Alfred	k1gMnSc1	Alfred
Six	Six	k1gMnSc1	Six
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
úřadu	úřad	k1gInSc2	úřad
RSHA	RSHA	kA	RSHA
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
vědecké	vědecký	k2eAgNnSc4d1	vědecké
zkoumání	zkoumání	k1gNnSc4	zkoumání
protivníků	protivník	k1gMnPc2	protivník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
komanda	komando	k1gNnSc2	komando
SS	SS	kA	SS
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Höß	Höß	k1gMnSc1	Höß
<g/>
,	,	kIx,	,
podvakrát	podvakrát	k6eAd1	podvakrát
velitel	velitel	k1gMnSc1	velitel
likvidačního	likvidační	k2eAgInSc2d1	likvidační
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
,	,	kIx,	,
dozorce	dozorce	k1gMnSc1	dozorce
v	v	k7c6	v
Dachau	Dachaus	k1gInSc6	Dachaus
a	a	k8xC	a
v	v	k7c4	v
Sachsenhausenu	Sachsenhausen	k2eAgFnSc4d1	Sachsenhausen
</s>
</p>
<p>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Backe	Back	k1gInSc2	Back
<g/>
,	,	kIx,	,
Heydrichův	Heydrichův	k2eAgMnSc1d1	Heydrichův
osobní	osobní	k2eAgMnSc1d1	osobní
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zemědělství	zemědělství	k1gNnSc2	zemědělství
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
Hangmen	Hangmen	k2eAgMnSc1d1	Hangmen
Also	Also	k1gMnSc1	Also
Die	Die	k1gMnSc1	Die
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
inspirovaném	inspirovaný	k2eAgInSc6d1	inspirovaný
atentátem	atentát	k1gInSc7	atentát
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
Heydricha	Heydrich	k1gMnSc4	Heydrich
německý	německý	k2eAgMnSc1d1	německý
herec	herec	k1gMnSc1	herec
Hans	Hans	k1gMnSc1	Hans
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Twardowski	Twardowski	k1gNnSc2	Twardowski
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Madman	Madman	k1gMnSc1	Madman
se	se	k3xPyFc4	se
titulní	titulní	k2eAgFnSc2d1	titulní
role	role	k1gFnSc2	role
zhostil	zhostit	k5eAaPmAgMnS	zhostit
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
John	John	k1gMnSc1	John
Carradine	Carradin	k1gMnSc5	Carradin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
filmu	film	k1gInSc6	film
Atentát	atentát	k1gInSc1	atentát
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ztvárněn	ztvárnit	k5eAaPmNgMnS	ztvárnit
východoněmeckým	východoněmecký	k2eAgMnSc7d1	východoněmecký
hercem	herec	k1gMnSc7	herec
Siegfriedem	Siegfried	k1gMnSc7	Siegfried
Loydou	Loyda	k1gMnSc7	Loyda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dvoudílném	dvoudílný	k2eAgInSc6d1	dvoudílný
filmu	film	k1gInSc6	film
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
role	role	k1gFnSc1	role
zastupujícího	zastupující	k2eAgMnSc2d1	zastupující
říšského	říšský	k2eAgMnSc2d1	říšský
protektora	protektor	k1gMnSc2	protektor
svěřena	svěřit	k5eAaPmNgFnS	svěřit
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
východoněmeckému	východoněmecký	k2eAgMnSc3d1	východoněmecký
<g/>
)	)	kIx)	)
herci	herec	k1gMnSc3	herec
Hanjo	Hanjo	k6eAd1	Hanjo
Hassemu	Hassema	k1gFnSc4	Hassema
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
Operace	operace	k1gFnSc1	operace
Daybreak	Daybreak	k1gInSc1	Daybreak
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
hrál	hrát	k5eAaImAgMnS	hrát
německý	německý	k2eAgMnSc1d1	německý
herec	herec	k1gMnSc1	herec
Anton	Anton	k1gMnSc1	Anton
Diffring	Diffring	k1gInSc4	Diffring
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
filmu	film	k1gInSc6	film
Lidice	Lidice	k1gInPc1	Lidice
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
koprodukčním	koprodukční	k2eAgInSc6d1	koprodukční
filmu	film	k1gInSc6	film
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
představoval	představovat	k5eAaImAgMnS	představovat
německý	německý	k2eAgMnSc1d1	německý
herec	herec	k1gMnSc1	herec
Detlef	Detlef	k1gMnSc1	Detlef
Bothe	Bothe	k1gFnSc1	Bothe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
atentátu	atentát	k1gInSc3	atentát
na	na	k7c4	na
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
operace	operace	k1gFnSc2	operace
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
však	však	k9	však
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
i	i	k9	i
život	život	k1gInSc4	život
a	a	k8xC	a
kariéru	kariéra	k1gFnSc4	kariéra
samotného	samotný	k2eAgMnSc4d1	samotný
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
ho	on	k3xPp3gNnSc4	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
australský	australský	k2eAgMnSc1d1	australský
herec	herec	k1gMnSc1	herec
Jason	Jason	k1gMnSc1	Jason
Clarke	Clarke	k1gFnSc1	Clarke
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
Letecký	letecký	k2eAgInSc1d1	letecký
pilotní	pilotní	k2eAgInSc1d1	pilotní
odznak	odznak	k1gInSc1	odznak
</s>
</p>
<p>
<s>
Frontová	frontový	k2eAgFnSc1d1	frontová
letecká	letecký	k2eAgFnSc1d1	letecká
spona	spona	k1gFnSc1	spona
<g/>
,	,	kIx,	,
průzkum	průzkum	k1gInSc1	průzkum
–	–	k?	–
bronzová	bronzový	k2eAgFnSc1d1	bronzová
</s>
</p>
<p>
<s>
SS-Ehrenring	SS-Ehrenring	k1gInSc1	SS-Ehrenring
</s>
</p>
<p>
<s>
Sudetská	sudetský	k2eAgFnSc1d1	sudetská
pamětní	pamětní	k2eAgFnSc1d1	pamětní
medaile	medaile	k1gFnSc1	medaile
</s>
</p>
<p>
<s>
Medaile	medaile	k1gFnSc1	medaile
za	za	k7c4	za
Anschluss	Anschluss	k1gInSc4	Anschluss
</s>
</p>
<p>
<s>
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
I.	I.	kA	I.
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Řád	řád	k1gInSc1	řád
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Německý	německý	k2eAgInSc1d1	německý
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
I.	I.	kA	I.
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odznak	odznak	k1gInSc1	odznak
za	za	k7c4	za
zranění	zranění	k1gNnSc4	zranění
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
zlatý	zlatý	k2eAgInSc1d1	zlatý
</s>
</p>
<p>
<s>
Záslužný	záslužný	k2eAgInSc1d1	záslužný
řád	řád	k1gInSc1	řád
Německého	německý	k2eAgMnSc2d1	německý
orla	orel	k1gMnSc2	orel
</s>
</p>
<p>
<s>
Medaile	medaile	k1gFnSc1	medaile
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
návratu	návrat	k1gInSc2	návrat
Memelu	Memel	k1gInSc2	Memel
</s>
</p>
<p>
<s>
Medaile	medaile	k1gFnSc1	medaile
Atlantického	atlantický	k2eAgInSc2d1	atlantický
valu	val	k1gInSc2	val
</s>
</p>
<p>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
stranický	stranický	k2eAgInSc1d1	stranický
odznak	odznak	k1gInSc1	odznak
</s>
</p>
<p>
<s>
Služební	služební	k2eAgNnSc1d1	služební
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
I.	I.	kA	I.
stupeň	stupeň	k1gInSc1	stupeň
–	–	k?	–
bronzové	bronzový	k2eAgFnPc4d1	bronzová
</s>
</p>
<p>
<s>
Gdaňský	gdaňský	k2eAgInSc1d1	gdaňský
křížúdaje	křížúdat	k5eAaImSgInS	křížúdat
použity	použít	k5eAaPmNgInP	použít
z	z	k7c2	z
<g/>
:	:	kIx,	:
polská	polský	k2eAgFnSc1d1	polská
Wikipedie-Reinhard	Wikipedie-Reinhard	k1gInSc1	Wikipedie-Reinhard
Hendrich	Hendrich	k1gMnSc1	Hendrich
<g/>
/	/	kIx~	/
<g/>
Odznaczenia	Odznaczenium	k1gNnPc1	Odznaczenium
a	a	k8xC	a
ruská	ruský	k2eAgFnSc1d1	ruská
Wikipedie-Г	Wikipedie-Г	k1gFnSc1	Wikipedie-Г
<g/>
,	,	kIx,	,
Р	Р	k?	Р
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BERWID-BUQUOY	BERWID-BUQUOY	k?	BERWID-BUQUOY
<g/>
:	:	kIx,	:
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Attentat	Attentat	k1gFnSc1	Attentat
auf	auf	k?	auf
den	den	k1gInSc4	den
Stellvertertenden	Stellvertertendna	k1gFnPc2	Stellvertertendna
Reichsprotektor	Reichsprotektor	k1gMnSc1	Reichsprotektor
von	von	k1gInSc4	von
Böhmen	Böhmen	k2eAgInSc4d1	Böhmen
und	und	k?	und
Mähren	Mährna	k1gFnPc2	Mährna
<g/>
,	,	kIx,	,
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Offene	Offen	k1gMnSc5	Offen
Fragen	Fragen	k1gInSc4	Fragen
in	in	k?	in
den	den	k1gInSc1	den
Publikationen	Publikationen	k1gInSc1	Publikationen
tschechischer	tschechischra	k1gFnPc2	tschechischra
Autoren	Autorna	k1gFnPc2	Autorna
<g/>
.	.	kIx.	.
</s>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
:	:	kIx,	:
Verlag	Verlag	k1gInSc1	Verlag
Bohemia	bohemia	k1gFnSc1	bohemia
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BURIAN	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
ANTHROPOID	ANTHROPOID	kA	ANTHROPOID
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BUTLER	BUTLER	kA	BUTLER
<g/>
,	,	kIx,	,
Rupert	Rupert	k1gMnSc1	Rupert
<g/>
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc1	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7352	[number]	k4	7352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ČVANČARA	čvančara	k1gMnSc1	čvančara
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Gallery	Galler	k1gInPc1	Galler
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
336	[number]	k4	336
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86990	[number]	k4	86990
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DEDERICHS	DEDERICHS	kA	DEDERICHS
<g/>
,	,	kIx,	,
Mario	Mario	k1gMnSc1	Mario
R.	R.	kA	R.
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
Gesicht	Gesicht	k1gInSc1	Gesicht
des	des	k1gNnSc1	des
Bösen	Bösen	k1gInSc1	Bösen
<g/>
.	.	kIx.	.
</s>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
492	[number]	k4	492
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4543	[number]	k4	4543
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
DESCHNER	DESCHNER	kA	DESCHNER
<g/>
,	,	kIx,	,
Günther	Günthra	k1gFnPc2	Günthra
<g/>
.	.	kIx.	.
</s>
<s>
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
:	:	kIx,	:
architekt	architekt	k1gMnSc1	architekt
totální	totální	k2eAgFnSc2d1	totální
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	Rybka	k1gMnSc1	Rybka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
62	[number]	k4	62
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GERWARTH	GERWARTH	kA	GERWARTH
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
,	,	kIx,	,
Hitlerův	Hitlerův	k2eAgMnSc1d1	Hitlerův
kat	kat	k1gMnSc1	kat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
252	[number]	k4	252
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HEYDRICHOVÁ	HEYDRICHOVÁ	kA	HEYDRICHOVÁ
<g/>
,	,	kIx,	,
Lina	Lina	k1gFnSc1	Lina
<g/>
.	.	kIx.	.
</s>
<s>
Verheiratet	Verheiratet	k1gInSc1	Verheiratet
mit	mit	k?	mit
einem	einem	k1gInSc1	einem
Kriegsverbrecher	Kriegsverbrechra	k1gFnPc2	Kriegsverbrechra
<g/>
.	.	kIx.	.
</s>
<s>
Pfaffenhofen	Pfaffenhofen	k1gInSc1	Pfaffenhofen
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7787	[number]	k4	7787
<g/>
-	-	kIx~	-
<g/>
1025	[number]	k4	1025
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KÁRNÝ	kárný	k2eAgMnSc1d1	kárný
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protektorátní	protektorátní	k2eAgFnSc1d1	protektorátní
politika	politika	k1gFnSc1	politika
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
TEPS	TEPS	kA	TEPS
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
301	[number]	k4	301
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7065	[number]	k4	7065
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
455	[number]	k4	455
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Heydrichiáda	heydrichiáda	k1gFnSc1	heydrichiáda
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Anthropoid	Anthropoid	k1gInSc1	Anthropoid
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Generalplan	Generalplan	k1gMnSc1	Generalplan
Ost	Ost	k1gMnSc1	Ost
</s>
</p>
<p>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
řešení	řešení	k1gNnSc1	řešení
české	český	k2eAgFnSc2d1	Česká
otázky	otázka	k1gFnSc2	otázka
</s>
</p>
<p>
<s>
Holokaust	holokaust	k1gInSc1	holokaust
</s>
</p>
<p>
<s>
Gestapo	gestapo	k1gNnSc1	gestapo
</s>
</p>
<p>
<s>
SS	SS	kA	SS
</s>
</p>
<p>
<s>
Lina	Lina	k1gFnSc1	Lina
Heydrichová	Heydrichová	k1gFnSc1	Heydrichová
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Reinhard	Reinharda	k1gFnPc2	Reinharda
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Reinhard	Reinharda	k1gFnPc2	Reinharda
Heydrich	Heydricha	k1gFnPc2	Heydricha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Reinhard	Reinharda	k1gFnPc2	Reinharda
Heydrich	Heydricha	k1gFnPc2	Heydricha
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
</s>
</p>
<p>
<s>
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
:	:	kIx,	:
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
manipulátor	manipulátor	k1gInSc1	manipulátor
</s>
</p>
<p>
<s>
Projev	projev	k1gInSc1	projev
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
o	o	k7c6	o
plánech	plán	k1gInPc6	plán
na	na	k7c4	na
likvidaci	likvidace	k1gFnSc4	likvidace
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
</s>
</p>
<p>
<s>
Směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
koncepce	koncepce	k1gFnSc1	koncepce
R.	R.	kA	R.
Heydricha	Heydrich	k1gMnSc2	Heydrich
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
do	do	k7c2	do
r.	r.	kA	r.
1942	[number]	k4	1942
</s>
</p>
<p>
<s>
Stodola	stodola	k1gFnSc1	stodola
skrývala	skrývat	k5eAaImAgFnS	skrývat
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
seděl	sedět	k5eAaImAgMnS	sedět
Heydrich	Heydrich	k1gMnSc1	Heydrich
při	při	k7c6	při
atentátu	atentát	k1gInSc6	atentát
–	–	k?	–
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
údajném	údajný	k2eAgInSc6d1	údajný
nálezu	nález	k1gInSc6	nález
pravého	pravý	k2eAgInSc2d1	pravý
mercedesu	mercedes	k1gInSc2	mercedes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
–	–	k?	–
video	video	k1gNnSc4	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Heydrich	Heydrich	k1gInSc1	Heydrich
–	–	k?	–
konečné	konečná	k1gFnSc2	konečná
řešení	řešení	k1gNnSc2	řešení
–	–	k?	–
cyklus	cyklus	k1gInSc4	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
</s>
</p>
<p>
<s>
Před	před	k7c7	před
70	[number]	k4	70
lety	let	k1gInPc7	let
padli	padnout	k5eAaPmAgMnP	padnout
nejen	nejen	k6eAd1	nejen
výsadkáři	výsadkář	k1gMnPc1	výsadkář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pardubicemi	Pardubice	k1gInPc7	Pardubice
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
SILVER	SILVER	kA	SILVER
A	a	k9	a
</s>
</p>
