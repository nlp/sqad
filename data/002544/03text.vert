<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
akademie	akademie	k1gFnSc1	akademie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Pontificia	Pontificia	k1gFnSc1	Pontificia
Academia	academia	k1gFnSc1	academia
Pro	pro	k7c4	pro
Vita	vit	k2eAgMnSc4d1	vit
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
dekretem	dekret	k1gInSc7	dekret
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vitae	Vitae	k6eAd1	Vitae
Mysterium	mysterium	k1gNnSc1	mysterium
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
akademie	akademie	k1gFnSc2	akademie
je	být	k5eAaImIp3nS	být
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
řešení	řešení	k1gNnSc4	řešení
základních	základní	k2eAgInPc2d1	základní
problémů	problém	k1gInPc2	problém
etiky	etika	k1gFnSc2	etika
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
zacházení	zacházení	k1gNnSc2	zacházení
se	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
studium	studium	k1gNnSc1	studium
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
podpoře	podpora	k1gFnSc3	podpora
a	a	k8xC	a
obraně	obrana	k1gFnSc3	obrana
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
také	také	k9	také
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
založena	založen	k2eAgFnSc1d1	založena
nadace	nadace	k1gFnSc1	nadace
"	"	kIx"	"
<g/>
Vitae	Vitae	k1gInSc1	Vitae
Mysterium	mysterium	k1gNnSc1	mysterium
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
autonomní	autonomní	k2eAgInSc1d1	autonomní
organizací	organizace	k1gFnSc7	organizace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Vatikánu	Vatikán	k1gInSc2	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
předsedou	předseda	k1gMnSc7	předseda
je	být	k5eAaImIp3nS	být
biskup	biskup	k1gMnSc1	biskup
Ignacio	Ignacio	k1gMnSc1	Ignacio
Carrasco	Carrasco	k1gMnSc1	Carrasco
de	de	k?	de
Paula	Paula	k1gFnSc1	Paula
(	(	kIx(	(
<g/>
od	od	k7c2	od
r.	r.	kA	r.
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
řešenými	řešený	k2eAgFnPc7d1	řešená
otázkami	otázka	k1gFnPc7	otázka
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k6eAd1	zejména
potraty	potrat	k1gInPc4	potrat
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgNnSc4d1	umělé
oplodnění	oplodnění	k1gNnSc4	oplodnění
<g/>
,	,	kIx,	,
genetické	genetický	k2eAgFnPc4d1	genetická
manipulace	manipulace	k1gFnPc4	manipulace
<g/>
,	,	kIx,	,
eutanazie	eutanazie	k1gFnPc4	eutanazie
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
předsedů	předseda	k1gMnPc2	předseda
Papežské	papežský	k2eAgFnSc2d1	Papežská
akademie	akademie	k1gFnSc2	akademie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Papežská	papežský	k2eAgFnSc1d1	Papežská
akademie	akademie	k1gFnSc1	akademie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Shrnutí	shrnutý	k2eAgMnPc1d1	shrnutý
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Vatikánu	Vatikán	k1gInSc2	Vatikán
Zakládací	zakládací	k2eAgInSc4d1	zakládací
dekret	dekret	k1gInSc4	dekret
"	"	kIx"	"
<g/>
Vitae	Vitae	k1gInSc1	Vitae
mysterium	mysterium	k1gNnSc1	mysterium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
</s>
