<s>
Wolfram	wolfram	k1gInSc1	wolfram
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
W	W	kA	W
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Wolframium	Wolframium	k1gNnSc1	Wolframium
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Tungsten	Tungsten	k2eAgMnSc1d1	Tungsten
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
až	až	k9	až
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
těžký	těžký	k2eAgInSc1d1	těžký
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
obtížně	obtížně	k6eAd1	obtížně
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
kov	kov	k1gInSc1	kov
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
po	po	k7c6	po
uhlíku	uhlík	k1gInSc6	uhlík
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
