<s>
Sinfonietta	Sinfonietta	k1gFnSc1	Sinfonietta
je	být	k5eAaImIp3nS	být
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
skladba	skladba	k1gFnSc1	skladba
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
předvedena	předveden	k2eAgFnSc1d1	předvedena
pražskému	pražský	k2eAgNnSc3d1	Pražské
publiku	publikum	k1gNnSc3	publikum
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Václava	Václav	k1gMnSc2	Václav
Talicha	Talich	k1gMnSc2	Talich
současně	současně	k6eAd1	současně
s	s	k7c7	s
Glagolskou	glagolský	k2eAgFnSc7d1	Glagolská
mší	mše	k1gFnSc7	mše
<g/>
.	.	kIx.	.
</s>
