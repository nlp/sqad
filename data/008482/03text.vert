<p>
<s>
Sinfonietta	Sinfonietta	k1gFnSc1	Sinfonietta
je	být	k5eAaImIp3nS	být
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
skladba	skladba	k1gFnSc1	skladba
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
předvedena	předveden	k2eAgFnSc1d1	předvedena
pražskému	pražský	k2eAgNnSc3d1	Pražské
publiku	publikum	k1gNnSc3	publikum
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1926	[number]	k4	1926
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Václava	Václav	k1gMnSc2	Václav
Talicha	Talich	k1gMnSc2	Talich
současně	současně	k6eAd1	současně
s	s	k7c7	s
Glagolskou	glagolský	k2eAgFnSc7d1	Glagolská
mší	mše	k1gFnSc7	mše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc6	část
==	==	k?	==
</s>
</p>
<p>
<s>
Allegretto	allegretto	k1gNnSc1	allegretto
(	(	kIx(	(
<g/>
Fanfára	fanfára	k1gFnSc1	fanfára
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Andante	andante	k1gNnSc1	andante
(	(	kIx(	(
<g/>
Hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moderato	moderato	k6eAd1	moderato
(	(	kIx(	(
<g/>
Králové	Králové	k2eAgInSc1d1	Králové
klášter	klášter	k1gInSc1	klášter
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Allegretto	allegretto	k1gNnSc1	allegretto
(	(	kIx(	(
<g/>
Ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Andante	andante	k1gNnSc1	andante
con	con	k?	con
moto	moto	k1gNnSc1	moto
(	(	kIx(	(
<g/>
Radnice	radnice	k1gFnSc1	radnice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Nástrojové	nástrojový	k2eAgNnSc1d1	nástrojové
obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
Janáčkově	Janáčkův	k2eAgNnSc6d1	Janáčkovo
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
žesťová	žesťový	k2eAgFnSc1d1	žesťová
sekce	sekce	k1gFnSc1	sekce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dřeva	dřevo	k1gNnPc1	dřevo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pikola	pikola	k1gFnSc1	pikola
</s>
</p>
<p>
<s>
2	[number]	k4	2
flétny	flétna	k1gFnPc4	flétna
</s>
</p>
<p>
<s>
2	[number]	k4	2
hoboje	hoboj	k1gInSc2	hoboj
</s>
</p>
<p>
<s>
anglický	anglický	k2eAgInSc1d1	anglický
roh	roh	k1gInSc1	roh
</s>
</p>
<p>
<s>
Es	es	k1gNnSc1	es
klarinet	klarinet	k1gInSc1	klarinet
</s>
</p>
<p>
<s>
2	[number]	k4	2
B	B	kA	B
klarinety	klarinet	k1gInPc4	klarinet
</s>
</p>
<p>
<s>
Basklarinet	basklarinet	k1gInSc1	basklarinet
</s>
</p>
<p>
<s>
2	[number]	k4	2
fagotyŽestě	fagotyŽestě	k6eAd1	fagotyŽestě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
4	[number]	k4	4
lesní	lesní	k2eAgInPc1d1	lesní
rohy	roh	k1gInPc1	roh
in	in	k?	in
F	F	kA	F
</s>
</p>
<p>
<s>
9	[number]	k4	9
trubek	trubka	k1gFnPc2	trubka
in	in	k?	in
C	C	kA	C
</s>
</p>
<p>
<s>
3	[number]	k4	3
Trubky	trubka	k1gFnSc2	trubka
in	in	k?	in
F	F	kA	F
</s>
</p>
<p>
<s>
2	[number]	k4	2
basové	basový	k2eAgFnPc4d1	basová
trubky	trubka	k1gFnPc4	trubka
</s>
</p>
<p>
<s>
4	[number]	k4	4
pozouny	pozoun	k1gInPc4	pozoun
</s>
</p>
<p>
<s>
2	[number]	k4	2
eufonia	eufonium	k1gNnPc1	eufonium
</s>
</p>
<p>
<s>
TubaBicí	TubaBicí	k2eAgInPc1d1	TubaBicí
nástroje	nástroj	k1gInPc1	nástroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tympány	tympán	k1gInPc1	tympán
</s>
</p>
<p>
<s>
Činely	činela	k1gFnPc1	činela
</s>
</p>
<p>
<s>
ZvonyStrunné	ZvonyStrunný	k2eAgInPc1d1	ZvonyStrunný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Harfa	harfa	k1gFnSc1	harfa
</s>
</p>
<p>
<s>
Housle	housle	k1gFnPc1	housle
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
</s>
</p>
<p>
<s>
Violy	Viola	k1gFnPc1	Viola
</s>
</p>
<p>
<s>
Violoncella	violoncello	k1gNnPc1	violoncello
</s>
</p>
<p>
<s>
KontrabasyDevět	KontrabasyDevět	k1gInSc1	KontrabasyDevět
C	C	kA	C
trubek	trubka	k1gFnPc2	trubka
<g/>
,	,	kIx,	,
basové	basový	k2eAgFnPc1d1	basová
trubky	trubka	k1gFnPc1	trubka
a	a	k8xC	a
eufonia	eufonium	k1gNnPc1	eufonium
hrají	hrát	k5eAaImIp3nP	hrát
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
první	první	k4xOgFnSc6	první
a	a	k8xC	a
poslední	poslední	k2eAgFnSc3d1	poslední
větě	věta	k1gFnSc3	věta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úpravy	úprava	k1gFnPc1	úprava
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
věta	věta	k1gFnSc1	věta
Sinfonietty	Sinfonietta	k1gFnSc2	Sinfonietta
byla	být	k5eAaImAgFnS	být
přetvořena	přetvořit	k5eAaPmNgFnS	přetvořit
progresivně	progresivně	k6eAd1	progresivně
rockovou	rockový	k2eAgFnSc7d1	rocková
skupinou	skupina	k1gFnSc7	skupina
Emerson	Emersona	k1gFnPc2	Emersona
<g/>
,	,	kIx,	,
Lake	Lake	k1gFnPc2	Lake
&	&	k?	&
Palmer	Palmer	k1gMnSc1	Palmer
do	do	k7c2	do
písně	píseň	k1gFnSc2	píseň
Knife-Edge	Knife-Edg	k1gFnSc2	Knife-Edg
a	a	k8xC	a
nahrána	nahrát	k5eAaBmNgFnS	nahrát
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Listening	Listening	k1gInSc1	Listening
Guide	Guid	k1gInSc5	Guid
<g/>
:	:	kIx,	:
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Sinfonietta	Sinfonietta	k1gMnSc1	Sinfonietta
with	with	k1gMnSc1	with
Jakub	Jakub	k1gMnSc1	Jakub
Hrůša	Hrůša	k1gMnSc1	Hrůša
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
