<s>
Anthelmintikum	Anthelmintikum	k1gNnSc1	Anthelmintikum
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
léčivou	léčivý	k2eAgFnSc4d1	léčivá
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
parazitickým	parazitický	k2eAgMnPc3d1	parazitický
helmintům	helmint	k1gMnPc3	helmint
(	(	kIx(	(
<g/>
červům	červ	k1gMnPc3	červ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
