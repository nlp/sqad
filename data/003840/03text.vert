<s>
Anthelmintikum	Anthelmintikum	k1gNnSc1	Anthelmintikum
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
léčivou	léčivý	k2eAgFnSc4d1	léčivá
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
parazitickým	parazitický	k2eAgMnPc3d1	parazitický
helmintům	helmint	k1gMnPc3	helmint
(	(	kIx(	(
<g/>
červům	červ	k1gMnPc3	červ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anthelmintika	Anthelmintikum	k1gNnSc2	Anthelmintikum
různými	různý	k2eAgInPc7d1	různý
mechanismy	mechanismus	k1gInPc7	mechanismus
usmrcují	usmrcovat	k5eAaImIp3nP	usmrcovat
<g/>
,	,	kIx,	,
inaktivují	inaktivovat	k5eAaBmIp3nP	inaktivovat
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
paralyzují	paralyzovat	k5eAaBmIp3nP	paralyzovat
parazitární	parazitární	k2eAgFnPc4d1	parazitární
helminty	helminta	k1gFnPc4	helminta
v	v	k7c6	v
hostitelském	hostitelský	k2eAgInSc6d1	hostitelský
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
k	k	k7c3	k
odčervení	odčervení	k1gNnSc3	odčervení
(	(	kIx(	(
<g/>
dehelmintizaci	dehelmintizace	k1gFnSc4	dehelmintizace
<g/>
)	)	kIx)	)
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
širokospektrá	širokospektrat	k5eAaPmIp3nS	širokospektrat
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
různým	různý	k2eAgFnPc3d1	různá
skupinám	skupina	k1gFnPc3	skupina
červů	červ	k1gMnPc2	červ
-	-	kIx~	-
např.	např.	kA	např.
albendazol	albendazol	k1gInSc1	albendazol
-	-	kIx~	-
účinný	účinný	k2eAgInSc1d1	účinný
proti	proti	k7c3	proti
hlísticím	hlístice	k1gFnPc3	hlístice
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
proti	proti	k7c3	proti
motolicím	motolice	k1gFnPc3	motolice
<g/>
,	,	kIx,	,
tasemnicím	tasemnice	k1gFnPc3	tasemnice
<g/>
)	)	kIx)	)
úzce	úzko	k6eAd1	úzko
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
konkrétním	konkrétní	k2eAgInPc3d1	konkrétní
druhům	druh	k1gInPc3	druh
-	-	kIx~	-
např.	např.	kA	např.
triklabendazol	triklabendazol	k1gInSc1	triklabendazol
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
účinný	účinný	k2eAgInSc4d1	účinný
pouze	pouze	k6eAd1	pouze
proti	proti	k7c3	proti
motolicím	motolice	k1gFnPc3	motolice
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Fasciolidae	Fasciolida	k1gFnSc2	Fasciolida
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
antinematoda	antinematoda	k1gFnSc1	antinematoda
-	-	kIx~	-
léčiva	léčivo	k1gNnPc4	léčivo
s	s	k7c7	s
účinkem	účinek	k1gInSc7	účinek
proti	proti	k7c3	proti
hlísticím	hlístice	k1gFnPc3	hlístice
anticestoda	anticestod	k1gMnSc4	anticestod
-	-	kIx~	-
léčiva	léčivo	k1gNnSc2	léčivo
s	s	k7c7	s
účinkem	účinek	k1gInSc7	účinek
proti	proti	k7c3	proti
tasemnicím	tasemnice	k1gFnPc3	tasemnice
antitrematoda	antitrematod	k1gMnSc4	antitrematod
-	-	kIx~	-
léčiva	léčivo	k1gNnSc2	léčivo
s	s	k7c7	s
účinkem	účinek	k1gInSc7	účinek
proti	proti	k7c3	proti
motolicím	motolice	k1gFnPc3	motolice
Dle	dle	k7c2	dle
chemické	chemický	k2eAgFnSc2d1	chemická
struktury	struktura	k1gFnSc2	struktura
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
anthelmintika	anthelmintika	k1gFnSc1	anthelmintika
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
benzimidazoly	benzimidazol	k1gInPc1	benzimidazol
<g/>
,	,	kIx,	,
makrocyklické	makrocyklický	k2eAgInPc1d1	makrocyklický
laktony	lakton	k1gInPc1	lakton
<g/>
,	,	kIx,	,
imidazothiazoly	imidazothiazol	k1gInPc1	imidazothiazol
<g/>
,	,	kIx,	,
tetrahydropyrimidiny	tetrahydropyrimidin	k1gInPc1	tetrahydropyrimidin
<g/>
,	,	kIx,	,
salicylanilidy	salicylanilida	k1gFnPc1	salicylanilida
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
anthelmintických	anthelmintický	k2eAgFnPc2d1	anthelmintický
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc2	jejich
zástupců	zástupce	k1gMnPc2	zástupce
a	a	k8xC	a
mechanismus	mechanismus	k1gInSc1	mechanismus
působení	působení	k1gNnSc2	působení
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Odčervení	Odčervení	k1gNnSc1	Odčervení
(	(	kIx(	(
<g/>
dehelmintizace	dehelmintizace	k1gFnSc1	dehelmintizace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
léčebný	léčebný	k2eAgInSc4d1	léčebný
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
infikovanému	infikovaný	k2eAgNnSc3d1	infikované
člověku	člověk	k1gMnSc6	člověk
nebo	nebo	k8xC	nebo
zvířeti	zvíře	k1gNnSc6	zvíře
podávají	podávat	k5eAaImIp3nP	podávat
léčivé	léčivý	k2eAgFnPc1d1	léčivá
látky	látka	k1gFnPc1	látka
–	–	k?	–
tedy	tedy	k8xC	tedy
anthelmintika	anthelmintika	k1gFnSc1	anthelmintika
-	-	kIx~	-
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zbavit	zbavit	k5eAaPmF	zbavit
organismus	organismus	k1gInSc4	organismus
parazitických	parazitický	k2eAgMnPc2d1	parazitický
helmintů	helmint	k1gMnPc2	helmint
<g/>
.	.	kIx.	.
</s>
<s>
Anthelmintika	Anthelmintikum	k1gNnPc1	Anthelmintikum
se	se	k3xPyFc4	se
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
<g/>
:	:	kIx,	:
per	prát	k5eAaImRp2nS	prát
orálně	orálně	k6eAd1	orálně
(	(	kIx(	(
<g/>
tablety	tableta	k1gFnPc1	tableta
<g/>
,	,	kIx,	,
tobolky	tobolka	k1gFnPc1	tobolka
<g/>
,	,	kIx,	,
kapky	kapka	k1gFnPc1	kapka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
injekčně	injekčně	k6eAd1	injekčně
(	(	kIx(	(
<g/>
podkožně	podkožně	k6eAd1	podkožně
nebo	nebo	k8xC	nebo
do	do	k7c2	do
svalu	sval	k1gInSc2	sval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
způsoby	způsob	k1gInPc1	způsob
jako	jako	k8xC	jako
perorální	perorální	k2eAgInPc1d1	perorální
roztoky	roztok	k1gInPc1	roztok
(	(	kIx(	(
<g/>
drenčování	drenčování	k1gNnPc4	drenčování
u	u	k7c2	u
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
perorální	perorální	k2eAgFnPc1d1	perorální
pasty	pasta	k1gFnPc1	pasta
(	(	kIx(	(
<g/>
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
)	)	kIx)	)
či	či	k8xC	či
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
prášku	prášek	k1gInSc2	prášek
do	do	k7c2	do
krmiva	krmivo	k1gNnSc2	krmivo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jelenovití	jelenovití	k1gMnPc1	jelenovití
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
anthelmintika	anthelmintikum	k1gNnPc1	anthelmintikum
se	se	k3xPyFc4	se
vstřebávají	vstřebávat	k5eAaImIp3nP	vstřebávat
kůží	kůže	k1gFnSc7	kůže
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
aplikovat	aplikovat	k5eAaBmF	aplikovat
přes	přes	k7c4	přes
kůži	kůže	k1gFnSc4	kůže
zvířat	zvíře	k1gNnPc2	zvíře
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
spot	spot	k1gInSc1	spot
on	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
<g/>
)	)	kIx)	)
či	či	k8xC	či
pour	pour	k1gMnSc1	pour
on	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
přežvýkavci	přežvýkavec	k1gMnSc6	přežvýkavec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
anthelmintika	anthelmintikum	k1gNnSc2	anthelmintikum
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
fyzikálně-chemických	fyzikálněhemický	k2eAgFnPc6d1	fyzikálně-chemická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc2	způsob
a	a	k8xC	a
formě	forma	k1gFnSc6	forma
podání	podání	k1gNnSc2	podání
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
látek	látka	k1gFnPc2	látka
se	se	k3xPyFc4	se
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
metabolizuje	metabolizovat	k5eAaImIp3nS	metabolizovat
(	(	kIx(	(
<g/>
biotransformace	biotransformace	k1gFnSc1	biotransformace
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
se	se	k3xPyFc4	se
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Anthelmintika	Anthelmintika	k1gFnSc1	Anthelmintika
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
metabolizované	metabolizovaný	k2eAgFnSc6d1	metabolizovaná
či	či	k8xC	či
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
distribuují	distribuovat	k5eAaBmIp3nP	distribuovat
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
krevním	krevní	k2eAgInSc7d1	krevní
oběhem	oběh	k1gInSc7	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
vylučovat	vylučovat	k5eAaImF	vylučovat
nejčastěji	často	k6eAd3	často
močí	moč	k1gFnSc7	moč
přes	přes	k7c4	přes
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
žlučí	žlučit	k5eAaPmIp3nS	žlučit
s	s	k7c7	s
trusem	trus	k1gInSc7	trus
<g/>
,	,	kIx,	,
mlékem	mléko	k1gNnSc7	mléko
či	či	k8xC	či
potem	pot	k1gInSc7	pot
a	a	k8xC	a
slinami	slina	k1gFnPc7	slina
<g/>
.	.	kIx.	.
</s>
<s>
Anthelmintická	Anthelmintický	k2eAgFnSc1d1	Anthelmintický
rezistence	rezistence	k1gFnSc1	rezistence
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
AR	ar	k1gInSc1	ar
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
selekčním	selekční	k2eAgInSc7d1	selekční
tlakem	tlak	k1gInSc7	tlak
získaná	získaný	k2eAgFnSc1d1	získaná
odolnost	odolnost	k1gFnSc1	odolnost
helmintů	helmint	k1gMnPc2	helmint
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
anthelmintik	anthelmintika	k1gFnPc2	anthelmintika
<g/>
.	.	kIx.	.
</s>
<s>
Rezistence	rezistence	k1gFnSc1	rezistence
je	být	k5eAaImIp3nS	být
též	též	k9	též
definovaná	definovaný	k2eAgFnSc1d1	definovaná
jako	jako	k8xS	jako
schopnost	schopnost	k1gFnSc1	schopnost
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
kmene	kmen	k1gInSc2	kmen
helmintického	helmintický	k2eAgMnSc2d1	helmintický
parazita	parazit	k1gMnSc2	parazit
tolerovat	tolerovat	k5eAaImF	tolerovat
dávku	dávka	k1gFnSc4	dávka
anthelmintika	anthelmintikum	k1gNnSc2	anthelmintikum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
normální	normální	k2eAgFnSc4d1	normální
<g/>
,	,	kIx,	,
vnímavou	vnímavý	k2eAgFnSc4d1	vnímavá
populaci	populace	k1gFnSc4	populace
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
parazita	parazita	k1gMnSc1	parazita
letální	letální	k2eAgInSc1d1	letální
<g/>
.	.	kIx.	.
</s>
<s>
Rezistence	rezistence	k1gFnSc1	rezistence
vzniká	vznikat	k5eAaImIp3nS	vznikat
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
čase	čas	k1gInSc6	čas
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
geneticky	geneticky	k6eAd1	geneticky
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
rezistence	rezistence	k1gFnSc1	rezistence
u	u	k7c2	u
hlístic	hlístice	k1gFnPc2	hlístice
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
a	a	k8xC	a
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
první	první	k4xOgInPc4	první
případy	případ	k1gInPc4	případ
neúčinné	účinný	k2eNgFnSc2d1	neúčinná
terapie	terapie	k1gFnSc2	terapie
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
či	či	k8xC	či
AR	ar	k1gInSc1	ar
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
problém	problém	k1gInSc4	problém
je	být	k5eAaImIp3nS	být
rezistence	rezistence	k1gFnSc1	rezistence
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
i	i	k9	i
koz	koza	k1gFnPc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
Pastevní	pastevní	k2eAgFnPc1d1	pastevní
parazitózy	parazitóza	k1gFnPc1	parazitóza
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
(	(	kIx(	(
<g/>
hlístice	hlístice	k1gFnSc1	hlístice
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Trichostrongylidae	Trichostrongylida	k1gFnSc2	Trichostrongylida
<g/>
)	)	kIx)	)
představují	představovat	k5eAaImIp3nP	představovat
závážný	závážný	k2eAgInSc4d1	závážný
problém	problém	k1gInSc4	problém
a	a	k8xC	a
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
limitujicí	limitujicí	k2eAgInSc4d1	limitujicí
faktor	faktor	k1gInSc4	faktor
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
anthelmintik	anthelmintika	k1gFnPc2	anthelmintika
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
ovcí	ovce	k1gFnPc2	ovce
zcela	zcela	k6eAd1	zcela
běžnou	běžný	k2eAgFnSc7d1	běžná
praxí	praxe	k1gFnSc7	praxe
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
zdraví	zdraví	k1gNnSc4	zdraví
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
produktivitu	produktivita	k1gFnSc4	produktivita
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
celoročním	celoroční	k2eAgInSc7d1	celoroční
pastevním	pastevní	k2eAgInSc7d1	pastevní
odchovem	odchov	k1gInSc7	odchov
se	se	k3xPyFc4	se
stáda	stádo	k1gNnPc1	stádo
odčervují	odčervovat	k5eAaImIp3nP	odčervovat
i	i	k9	i
3-4	[number]	k4	3-4
krát	krát	k6eAd1	krát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
vysoká	vysoký	k2eAgFnSc1d1	vysoká
četnost	četnost	k1gFnSc1	četnost
použití	použití	k1gNnSc2	použití
anthelmintik	anthelmintika	k1gFnPc2	anthelmintika
<g/>
,	,	kIx,	,
hromadná	hromadný	k2eAgFnSc1d1	hromadná
aplikace	aplikace	k1gFnSc1	aplikace
všem	všecek	k3xTgMnPc3	všecek
zvířatům	zvíře	k1gNnPc3	zvíře
–	–	k?	–
i	i	k9	i
těm	ten	k3xDgInPc3	ten
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
počtem	počet	k1gInSc7	počet
parazitů	parazit	k1gMnPc2	parazit
<g/>
,	,	kIx,	,
opakované	opakovaný	k2eAgFnPc1d1	opakovaná
aplikace	aplikace	k1gFnPc1	aplikace
látek	látka	k1gFnPc2	látka
stejného	stejný	k2eAgInSc2d1	stejný
mechanismu	mechanismus	k1gInSc2	mechanismus
a	a	k8xC	a
nesprávné	správný	k2eNgFnSc2d1	nesprávná
aplikace	aplikace	k1gFnSc2	aplikace
(	(	kIx(	(
<g/>
poddávkování	poddávkování	k1gNnSc1	poddávkování
<g/>
,	,	kIx,	,
nesprávný	správný	k2eNgInSc1d1	nesprávný
čas	čas	k1gInSc1	čas
léčby	léčba	k1gFnSc2	léčba
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
předpokladem	předpoklad	k1gInSc7	předpoklad
pro	pro	k7c4	pro
vysoký	vysoký	k2eAgInSc4d1	vysoký
selekční	selekční	k2eAgInSc4d1	selekční
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
rezistence	rezistence	k1gFnSc2	rezistence
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
rezistentních	rezistentní	k2eAgFnPc2d1	rezistentní
střevních	střevní	k2eAgFnPc2d1	střevní
hlístic	hlístice	k1gFnPc2	hlístice
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
případy	případ	k1gInPc1	případ
rezistence	rezistence	k1gFnSc2	rezistence
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
již	již	k6eAd1	již
60	[number]	k4	60
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
zemích	zem	k1gFnPc6	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
rezistence	rezistence	k1gFnSc1	rezistence
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
vlasovky	vlasovka	k1gFnSc2	vlasovka
slezové	slezový	k2eAgInPc1d1	slezový
(	(	kIx(	(
<g/>
Haemonchus	Haemonchus	k1gInSc1	Haemonchus
contortus	contortus	k1gInSc1	contortus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
uvedením	uvedení	k1gNnSc7	uvedení
nové	nový	k2eAgFnSc2d1	nová
látky	látka	k1gFnSc2	látka
na	na	k7c4	na
trh	trh	k1gInSc4	trh
a	a	k8xC	a
objevením	objevení	k1gNnSc7	objevení
prvních	první	k4xOgInPc2	první
případů	případ	k1gInPc2	případ
rezistetních	rezistetní	k2eAgFnPc2d1	rezistetní
vlasovek	vlasovka	k1gFnPc2	vlasovka
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgNnSc1d2	kratší
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
AR	ar	k1gInSc4	ar
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
i	i	k9	i
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
bez	bez	k7c2	bez
celoročního	celoroční	k2eAgInSc2d1	celoroční
pastevního	pastevní	k2eAgInSc2d1	pastevní
systému	systém	k1gInSc2	systém
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Rezistence	rezistence	k1gFnSc1	rezistence
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
proti	proti	k7c3	proti
několika	několik	k4yIc2	několik
skupinám	skupina	k1gFnPc3	skupina
anthelmintik	anthelmintika	k1gFnPc2	anthelmintika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
benzimidazolů	benzimidazol	k1gInPc2	benzimidazol
(	(	kIx(	(
<g/>
BZ	bz	k0	bz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
makrocyklických	makrocyklický	k2eAgInPc2d1	makrocyklický
laktonů	lakton	k1gInPc2	lakton
(	(	kIx(	(
<g/>
ML	ml	kA	ml
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tetrahydropyrimidinů	tetrahydropyrimidin	k1gInPc2	tetrahydropyrimidin
a	a	k8xC	a
levamizolu	levamizol	k1gInSc2	levamizol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
i	i	k9	i
přítomnost	přítomnost	k1gFnSc1	přítomnost
multirezistetních	multirezistetní	k2eAgInPc2d1	multirezistetní
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
paraziti	parazit	k1gMnPc1	parazit
s	s	k7c7	s
rezistencí	rezistence	k1gFnSc7	rezistence
vůči	vůči	k7c3	vůči
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc3	jeden
skupině	skupina	k1gFnSc3	skupina
anthelmintik	anthelmintikum	k1gNnPc2	anthelmintikum
<g/>
,	,	kIx,	,
např.	např.	kA	např.
BZ	bz	k0	bz
<g/>
+	+	kIx~	+
<g/>
ML.	ml.	kA	ml.
Nové	Nové	k2eAgNnSc1d1	Nové
řešení	řešení	k1gNnSc1	řešení
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
rezistentním	rezistentní	k2eAgFnPc3d1	rezistentní
hlísticím	hlístice	k1gFnPc3	hlístice
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
slibavalo	slibavat	k5eAaImAgNnS	slibavat
uvedení	uvedení	k1gNnSc1	uvedení
nového	nový	k2eAgNnSc2d1	nové
anthelmintika	anthelmintikum	k1gNnSc2	anthelmintikum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jím	on	k3xPp3gNnSc7	on
látka	látka	k1gFnSc1	látka
monepantel	monepantela	k1gFnPc2	monepantela
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
derivátu	derivát	k1gInSc2	derivát
aminoacetonitrilu	aminoacetonitril	k1gInSc2	aminoacetonitril
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
proti	proti	k7c3	proti
BZ	bz	k0	bz
nebo	nebo	k8xC	nebo
ML-rezistentním	MLezistentní	k2eAgFnPc3d1	ML-rezistentní
hlísticím	hlístice	k1gFnPc3	hlístice
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
první	první	k4xOgInPc1	první
případy	případ	k1gInPc1	případ
rezistence	rezistence	k1gFnSc2	rezistence
na	na	k7c4	na
monepantel	monepantel	k1gInSc4	monepantel
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
byly	být	k5eAaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
zhruba	zhruba	k6eAd1	zhruba
už	už	k6eAd1	už
3-4	[number]	k4	3-4
roky	rok	k1gInPc4	rok
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
přípravku	přípravek	k1gInSc2	přípravek
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
byly	být	k5eAaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
monepantel-rezistentní	monepantelezistentní	k2eAgFnPc1d1	monepantel-rezistentní
hlístice	hlístice	k1gFnPc1	hlístice
dokonce	dokonce	k9	dokonce
2	[number]	k4	2
roky	rok	k1gInPc7	rok
od	od	k7c2	od
registrace	registrace	k1gFnSc2	registrace
přípravku	přípravek	k1gInSc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
koz	koza	k1gFnPc2	koza
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc1	chov
i	i	k8xC	i
spektrum	spektrum	k1gNnSc1	spektrum
pastevních	pastevní	k2eAgMnPc2d1	pastevní
parazitů	parazit	k1gMnPc2	parazit
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
rezistence	rezistence	k1gFnSc2	rezistence
bežný	bežný	k2eAgInSc1d1	bežný
i	i	k9	i
u	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
malých	malý	k2eAgMnPc2d1	malý
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
hlístic	hlístice	k1gFnPc2	hlístice
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
malými	malý	k2eAgMnPc7d1	malý
přežvýkavci	přežvýkavec	k1gMnPc7	přežvýkavec
problém	problém	k1gInSc4	problém
rezistence	rezistence	k1gFnSc2	rezistence
menší	malý	k2eAgInSc4d2	menší
a	a	k8xC	a
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
tak	tak	k6eAd1	tak
globálních	globální	k2eAgInPc2d1	globální
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
hlístic	hlístice	k1gFnPc2	hlístice
byla	být	k5eAaImAgFnS	být
rezistence	rezistence	k1gFnSc1	rezistence
popsána	popsat	k5eAaPmNgFnS	popsat
také	také	k9	také
u	u	k7c2	u
motolic	motolice	k1gFnPc2	motolice
<g/>
.	.	kIx.	.
</s>
<s>
Rezistence	rezistence	k1gFnSc1	rezistence
motolice	motolice	k1gFnSc2	motolice
jaterní	jaterní	k2eAgFnSc2d1	jaterní
na	na	k7c4	na
triklabendazol	triklabendazol	k1gInSc4	triklabendazol
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
skotu	skot	k1gInSc2	skot
byla	být	k5eAaImAgNnP	být
hlášena	hlásit	k5eAaImNgNnP	hlásit
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Holandska	Holandsko	k1gNnSc2	Holandsko
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Argentíny	Argentína	k1gFnSc2	Argentína
<g/>
.	.	kIx.	.
</s>
<s>
Albendazol-rezistentní	Albendazolezistentní	k2eAgInPc1d1	Albendazol-rezistentní
kmeny	kmen	k1gInPc1	kmen
byly	být	k5eAaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
v	v	k7c6	v
Argentíně	Argentína	k1gFnSc6	Argentína
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgMnPc4d3	Nejběžnější
parazity	parazit	k1gMnPc4	parazit
u	u	k7c2	u
koní	kůň	k1gMnPc2	kůň
patří	patřit	k5eAaImIp3nP	patřit
malí	malý	k2eAgMnPc1d1	malý
a	a	k8xC	a
velcí	velký	k2eAgMnPc1d1	velký
stongylidi	stongylid	k1gMnPc1	stongylid
nebo	nebo	k8xC	nebo
škrkavky	škrkavka	k1gFnPc1	škrkavka
rodu	rod	k1gInSc2	rod
Parascaris	Parascaris	k1gFnPc1	Parascaris
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odčervování	odčervování	k1gNnSc3	odčervování
koní	kůň	k1gMnPc2	kůň
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
BZ	bz	k0	bz
<g/>
,	,	kIx,	,
ML	ml	kA	ml
nebo	nebo	k8xC	nebo
tetrahydropyrimidiny	tetrahydropyrimidin	k2eAgFnPc1d1	tetrahydropyrimidin
(	(	kIx(	(
<g/>
pyrantel	pyrantel	k1gInSc1	pyrantel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
odčervovaly	odčervovat	k5eAaImAgFnP	odčervovat
všechny	všechen	k3xTgMnPc4	všechen
koně	kůň	k1gMnSc4	kůň
ve	v	k7c6	v
stádu	stádo	k1gNnSc6	stádo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
preventivně	preventivně	k6eAd1	preventivně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
vzniku	vznik	k1gInSc2	vznik
rezistence	rezistence	k1gFnSc2	rezistence
u	u	k7c2	u
strongylidů	strongylid	k1gInPc2	strongylid
a	a	k8xC	a
škrkavek	škrkavka	k1gFnPc2	škrkavka
<g/>
.	.	kIx.	.
</s>
<s>
Rezistence	rezistence	k1gFnSc1	rezistence
na	na	k7c4	na
ivermektin	ivermektin	k1gInSc4	ivermektin
<g/>
,	,	kIx,	,
moxidektin	moxidektin	k1gInSc4	moxidektin
<g/>
,	,	kIx,	,
fenbendazol	fenbendazol	k1gInSc4	fenbendazol
či	či	k8xC	či
pyrantel	pyrantel	k1gInSc4	pyrantel
u	u	k7c2	u
strongylidů	strongylid	k1gInPc2	strongylid
a	a	k8xC	a
škrkavek	škrkavka	k1gFnPc2	škrkavka
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
psích	psí	k2eAgMnPc2d1	psí
parazitů	parazit	k1gMnPc2	parazit
se	se	k3xPyFc4	se
AR	ar	k1gInSc1	ar
téměř	téměř	k6eAd1	téměř
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
rezistentních	rezistentní	k2eAgInPc6d1	rezistentní
kmenech	kmen	k1gInPc6	kmen
měchovců	měchovec	k1gMnPc2	měchovec
či	či	k8xC	či
vlasovců	vlasovec	k1gMnPc2	vlasovec
psích	psí	k2eAgMnPc2d1	psí
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
i	i	k9	i
výskyt	výskyt	k1gInSc1	výskyt
rezistence	rezistence	k1gFnSc2	rezistence
u	u	k7c2	u
lidských	lidský	k2eAgMnPc2d1	lidský
helmintů	helmint	k1gMnPc2	helmint
velmi	velmi	k6eAd1	velmi
vzácným	vzácný	k2eAgInSc7d1	vzácný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
však	však	k9	však
popsáno	popsat	k5eAaPmNgNnS	popsat
několik	několik	k4yIc1	několik
případů	případ	k1gInPc2	případ
podezření	podezřeň	k1gFnPc2	podezřeň
na	na	k7c4	na
rezistenci	rezistence	k1gFnSc4	rezistence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
byly	být	k5eAaImAgInP	být
dokumentovány	dokumentován	k2eAgInPc1d1	dokumentován
případy	případ	k1gInPc1	případ
krevniček	krevnička	k1gFnPc2	krevnička
Schistosoma	Schistosoma	k1gNnSc4	Schistosoma
mansoni	manson	k1gMnPc1	manson
nereagující	reagující	k2eNgMnPc1d1	nereagující
na	na	k7c4	na
prazikvantel	prazikvantel	k1gInSc4	prazikvantel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
na	na	k7c6	na
Mali	Mali	k1gNnSc6	Mali
neúčinkoval	účinkovat	k5eNaImAgInS	účinkovat
mebendazol	mebendazol	k1gInSc1	mebendazol
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
měchovcem	měchovec	k1gMnSc7	měchovec
lidským	lidský	k2eAgMnSc7d1	lidský
<g/>
.	.	kIx.	.
</s>
