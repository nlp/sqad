<s>
Frédéric	Frédéric	k1gMnSc1	Frédéric
Mistral	mistral	k1gInSc1	mistral
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1830	[number]	k4	1830
<g/>
,	,	kIx,	,
Maillaine	Maillain	k1gMnSc5	Maillain
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Maillaine	Maillain	k1gMnSc5	Maillain
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lyrický	lyrický	k2eAgMnSc1d1	lyrický
a	a	k8xC	a
epický	epický	k2eAgMnSc1d1	epický
básník	básník	k1gMnSc1	básník
z	z	k7c2	z
Provence	Provence	k1gFnSc2	Provence
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
obrození	obrození	k1gNnSc4	obrození
provensálského	provensálský	k2eAgInSc2d1	provensálský
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
okcitánštiny	okcitánština	k1gFnSc2	okcitánština
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
obnovu	obnova	k1gFnSc4	obnova
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
společně	společně	k6eAd1	společně
se	s	k7c7	s
španělským	španělský	k2eAgMnSc7d1	španělský
dramatikem	dramatik	k1gMnSc7	dramatik
Josém	José	k1gNnSc6	José
Echegaraym	Echegaraymum	k1gNnPc2	Echegaraymum
y	y	k?	y
Eizaguirre	Eizaguirr	k1gMnSc5	Eizaguirr
<g/>
.	.	kIx.	.
</s>
<s>
Fréderic	Fréderic	k1gMnSc1	Fréderic
Mistral	mistral	k1gInSc4	mistral
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
v	v	k7c6	v
Maillaine	Maillain	k1gInSc5	Maillain
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
jako	jako	k8xS	jako
syn	syn	k1gMnSc1	syn
farmáře	farmář	k1gMnSc2	farmář
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Královské	královský	k2eAgFnSc6d1	královská
koleji	kolej	k1gFnSc6	kolej
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
antické	antický	k2eAgMnPc4d1	antický
básníky	básník	k1gMnPc4	básník
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Aix-en-Provence	Aixn-Provenec	k1gMnPc4	Aix-en-Provenec
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
tamějším	tamější	k2eAgMnSc7d1	tamější
profesorem	profesor	k1gMnSc7	profesor
Josephem	Joseph	k1gInSc7	Joseph
Roumanillem	Roumanill	k1gInSc7	Roumanill
studovat	studovat	k5eAaImF	studovat
starou	starý	k2eAgFnSc4d1	stará
provensálskou	provensálský	k2eAgFnSc4d1	provensálská
poesii	poesie	k1gFnSc4	poesie
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
obnovit	obnovit	k5eAaPmF	obnovit
tento	tento	k3xDgInSc4	tento
národní	národní	k2eAgInSc4d1	národní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
vydali	vydat	k5eAaPmAgMnP	vydat
Mistral	mistral	k1gInSc4	mistral
a	a	k8xC	a
Roumanille	Roumanille	k1gNnSc4	Roumanille
první	první	k4xOgInSc1	první
sborník	sborník	k1gInSc1	sborník
živých	živý	k2eAgMnPc2d1	živý
provensálských	provensálský	k2eAgMnPc2d1	provensálský
básníků	básník	k1gMnPc2	básník
Li	li	k8xS	li
Prouvencalo	Prouvencalo	k1gMnPc2	Prouvencalo
a	a	k8xC	a
pokusili	pokusit	k5eAaPmAgMnP	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
reformu	reforma	k1gFnSc4	reforma
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
založil	založit	k5eAaPmAgMnS	založit
Mistral	mistral	k1gInSc4	mistral
asociaci	asociace	k1gFnSc3	asociace
provensálských	provensálský	k2eAgMnPc2d1	provensálský
básníků	básník	k1gMnPc2	básník
Felibrige	Felibrig	k1gFnSc2	Felibrig
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
sjezdu	sjezd	k1gInSc6	sjezd
ve	v	k7c6	v
Fontsegugne	Fontsegugn	k1gInSc5	Fontsegugn
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
jazykové	jazykový	k2eAgFnSc3d1	jazyková
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc3d1	literární
i	i	k8xC	i
politické	politický	k2eAgFnSc3d1	politická
renesanci	renesance	k1gFnSc3	renesance
francouzského	francouzský	k2eAgInSc2d1	francouzský
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
asociace	asociace	k1gFnSc2	asociace
nebylo	být	k5eNaImAgNnS	být
oživit	oživit	k5eAaPmF	oživit
starou	starý	k2eAgFnSc4d1	stará
okcitánštinu	okcitánština	k1gFnSc4	okcitánština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
moderní	moderní	k2eAgInSc4d1	moderní
a	a	k8xC	a
živý	živý	k2eAgInSc4d1	živý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
snahy	snaha	k1gFnPc4	snaha
korunoval	korunovat	k5eAaBmAgInS	korunovat
Mistral	mistral	k1gInSc1	mistral
svým	svůj	k3xOyFgInSc7	svůj
velkým	velký	k2eAgInSc7d1	velký
novoprovensálským	novoprovensálský	k2eAgInSc7d1	novoprovensálský
slovníkem	slovník	k1gInSc7	slovník
Trésor	Trésor	k1gInSc1	Trésor
dóu	dóu	k?	dóu
Félibrige	Félibrige	k1gInSc1	Félibrige
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obrovitým	obrovitý	k2eAgNnSc7d1	obrovité
dílem	dílo	k1gNnSc7	dílo
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
pracoval	pracovat	k5eAaImAgMnS	pracovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
kodifikoval	kodifikovat	k5eAaBmAgMnS	kodifikovat
bohatství	bohatství	k1gNnSc4	bohatství
okcitánských	okcitánský	k2eAgInPc2d1	okcitánský
dialektů	dialekt	k1gInPc2	dialekt
používaných	používaný	k2eAgInPc2d1	používaný
mezi	mezi	k7c7	mezi
lidem	lid	k1gInSc7	lid
a	a	k8xC	a
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
tak	tak	k6eAd1	tak
nesmrtelný	smrtelný	k2eNgInSc4d1	nesmrtelný
pomník	pomník	k1gInSc4	pomník
znovuzrozeného	znovuzrozený	k2eAgInSc2d1	znovuzrozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dílo	dílo	k1gNnSc4	dílo
stvořil	stvořit	k5eAaPmAgMnS	stvořit
Mistral	mistral	k1gInSc4	mistral
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
okcitánštině	okcitánština	k1gFnSc6	okcitánština
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
provensálské	provensálský	k2eAgFnSc6d1	provensálská
variantě	varianta	k1gFnSc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
společně	společně	k6eAd1	společně
se	s	k7c7	s
španělským	španělský	k2eAgMnSc7d1	španělský
dramatikem	dramatik	k1gMnSc7	dramatik
Josém	José	k1gNnSc6	José
Echegaraym	Echegaraymum	k1gNnPc2	Echegaraymum
y	y	k?	y
Eizaguirre	Eizaguirr	k1gInSc5	Eizaguirr
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
se	s	k7c7	s
zřetelem	zřetel	k1gInSc7	zřetel
k	k	k7c3	k
originálním	originální	k2eAgFnPc3d1	originální
<g/>
,	,	kIx,	,
geniálním	geniální	k2eAgFnPc3d1	geniální
a	a	k8xC	a
vpravdě	vpravdě	k9	vpravdě
uměleckým	umělecký	k2eAgInPc3d1	umělecký
rysům	rys	k1gInPc3	rys
jeho	jeho	k3xOp3gFnSc2	jeho
básnické	básnický	k2eAgFnSc2d1	básnická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
věrně	věrně	k6eAd1	věrně
zrcadlí	zrcadlit	k5eAaImIp3nS	zrcadlit
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
život	život	k1gInSc4	život
lidu	lid	k1gInSc2	lid
jeho	jeho	k3xOp3gInSc2	jeho
rodného	rodný	k2eAgInSc2d1	rodný
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
významné	významný	k2eAgFnSc3d1	významná
činnosti	činnost	k1gFnSc3	činnost
provensálského	provensálský	k2eAgMnSc2d1	provensálský
filologa	filolog	k1gMnSc2	filolog
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInSc1d1	finanční
výnos	výnos	k1gInSc1	výnos
z	z	k7c2	z
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
použil	použít	k5eAaPmAgInS	použít
Mistral	mistral	k1gInSc1	mistral
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
etnografického	etnografický	k2eAgNnSc2d1	etnografické
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc4d1	dnešní
Arlaten	Arlaten	k2eAgInSc4d1	Arlaten
Folk	folk	k1gInSc4	folk
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mirè	Mirè	k?	Mirè
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
báseň	báseň	k1gFnSc4	báseň
použil	použít	k5eAaPmAgMnS	použít
skladatel	skladatel	k1gMnSc1	skladatel
Charles	Charles	k1gMnSc1	Charles
Gounod	Gounoda	k1gFnPc2	Gounoda
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
operu	opera	k1gFnSc4	opera
Mireille	Mireille	k1gFnSc2	Mireille
<g/>
.	.	kIx.	.
</s>
<s>
Calendau	Calendau	k6eAd1	Calendau
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
báseň	báseň	k1gFnSc1	báseň
o	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
zpěvech	zpěv	k1gInPc6	zpěv
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnPc3	svůj
hrdinným	hrdinný	k2eAgMnPc3d1	hrdinný
a	a	k8xC	a
místy	místy	k6eAd1	místy
až	až	k6eAd1	až
didaktickým	didaktický	k2eAgInSc7d1	didaktický
rázem	ráz	k1gInSc7	ráz
stala	stát	k5eAaPmAgFnS	stát
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
kánonem	kánon	k1gInSc7	kánon
všech	všecek	k3xTgFnPc2	všecek
provensálských	provensálský	k2eAgFnPc2d1	provensálská
národních	národní	k2eAgFnPc2d1	národní
snah	snaha	k1gFnPc2	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Lis	lis	k1gInSc1	lis
isclo	isclo	k1gNnSc1	isclo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
or	or	k?	or
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Zlaté	zlatý	k2eAgInPc1d1	zlatý
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
nejkrásnějších	krásný	k2eAgInPc2d3	nejkrásnější
vlasteneckých	vlastenecký	k2eAgInPc2d1	vlastenecký
i	i	k8xC	i
intimních	intimní	k2eAgInPc2d1	intimní
Mistralových	Mistralův	k2eAgInPc2d1	Mistralův
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Lou	Lou	k?	Lou
tresor	tresor	k1gInSc1	tresor
dóu	dóu	k?	dóu
félibrige	félibrige	k1gInSc1	félibrige
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novoprovensálský	novoprovensálský	k2eAgInSc1d1	novoprovensálský
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
raço	raço	k6eAd1	raço
latino	latina	k1gFnSc5	latina
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nerto	Nerto	k1gNnSc1	Nerto
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
rè	rè	k?	rè
Jano	Jano	k1gMnSc1	Jano
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Královna	královna	k1gFnSc1	královna
Jana	Jana	k1gFnSc1	Jana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tragédie	tragédie	k1gFnSc1	tragédie
z	z	k7c2	z
národní	národní	k2eAgFnSc2d1	národní
historie	historie	k1gFnSc2	historie
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Lou	Lou	k?	Lou
Pouè	Pouè	k1gFnSc4	Pouè
dóu	dóu	k?	dóu
rose	rosa	k1gFnSc3	rosa
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Báseň	báseň	k1gFnSc1	báseň
Rhony	Rhona	k1gFnSc2	Rhona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrchol	vrchol	k1gInSc4	vrchol
Mistralovy	Mistralův	k2eAgFnSc2d1	Mistralův
epiky	epika	k1gFnSc2	epika
<g/>
.	.	kIx.	.
</s>
<s>
Discours	Discours	k1gInSc1	Discours
et	et	k?	et
dicho	dic	k1gMnSc2	dic
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moun	Moun	k1gMnSc1	Moun
espelido	espelida	k1gFnSc5	espelida
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
Lis	lis	k1gInSc4	lis
Oulivado	Oulivada	k1gFnSc5	Oulivada
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nerto	Nerto	k1gNnSc1	Nerto
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kotrba	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Otakar	Otakar	k1gMnSc1	Otakar
J.	J.	kA	J.
Janota	Janota	k1gMnSc1	Janota
<g/>
,	,	kIx,	,
Mirè	Mirè	k1gMnSc1	Mirè
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Sigismund	Sigismund	k1gMnSc1	Sigismund
Bouška	Bouška	k1gMnSc1	Bouška
<g/>
,	,	kIx,	,
Zlaté	zlatý	k2eAgInPc1d1	zlatý
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
a	a	k8xC	a
úvod	úvod	k1gInSc1	úvod
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kadlec	Kadlec	k1gMnSc1	Kadlec
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Frédéric	Frédérice	k1gInPc2	Frédérice
Mistral	mistral	k1gInSc4	mistral
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Frédéric	Frédéric	k1gMnSc1	Frédéric
Mistral	mistral	k1gInSc4	mistral
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
http://nobelprize.org/nobel_prizes/literature/laureates/1904/mistral-bio.html	[url]	k1gInSc1	http://nobelprize.org/nobel_prizes/literature/laureates/1904/mistral-bio.html
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
http://www.nimausensis.com/Nimes/Mistral/BaccalaureatMistral.htm	[url]	k6eAd1	http://www.nimausensis.com/Nimes/Mistral/BaccalaureatMistral.htm
(	(	kIx(	(
<g/>
okcitánsky	okcitánsky	k6eAd1	okcitánsky
<g/>
)	)	kIx)	)
Mirè	Mirè	k1gFnSc1	Mirè
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
http://www.up.univ-mrs.fr/tresoc/libre/integral/libr0243.pdf	[url]	k1gInSc1	http://www.up.univ-mrs.fr/tresoc/libre/integral/libr0243.pdf
Mireille	Mireille	k1gFnSc2	Mireille
http://www.britannica.com/nobel/micro/397_21.html	[url]	k5eAaPmAgInS	http://www.britannica.com/nobel/micro/397_21.html
</s>
