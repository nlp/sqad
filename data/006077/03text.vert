<s>
Kosmogonie	kosmogonie	k1gFnSc1	kosmogonie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
κ	κ	k?	κ
[	[	kIx(	[
<g/>
kosmos	kosmos	k1gInSc1	kosmos
<g/>
]	]	kIx)	]
"	"	kIx"	"
<g/>
pořádek	pořádek	k1gInSc1	pořádek
<g/>
,	,	kIx,	,
vesmír	vesmír	k1gInSc1	vesmír
<g/>
"	"	kIx"	"
a	a	k8xC	a
γ	γ	k?	γ
[	[	kIx(	[
<g/>
gignomai	gignoma	k1gFnSc2	gignoma
<g/>
]	]	kIx)	]
"	"	kIx"	"
<g/>
rodím	rodit	k5eAaImIp1nS	rodit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pravěkých	pravěký	k2eAgInPc2d1	pravěký
a	a	k8xC	a
starověkých	starověký	k2eAgInPc2d1	starověký
národů	národ	k1gInPc2	národ
bývá	bývat	k5eAaImIp3nS	bývat
součástí	součást	k1gFnSc7	součást
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
termínem	termín	k1gInSc7	termín
označována	označovat	k5eAaImNgFnS	označovat
přírodní	přírodní	k2eAgFnSc1d1	přírodní
nauka	nauka	k1gFnSc1	nauka
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
vědecky	vědecky	k6eAd1	vědecky
podloženými	podložený	k2eAgFnPc7d1	podložená
teoriemi	teorie	k1gFnPc7	teorie
vzniku	vznik	k1gInSc2	vznik
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
např.	např.	kA	např.
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Kosmologie	kosmologie	k1gFnSc1	kosmologie
</s>
