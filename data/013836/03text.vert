<s>
Moje	můj	k3xOp1gFnSc1
krásná	krásný	k2eAgFnSc1d1
čarodějka	čarodějka	k1gFnSc1
</s>
<s>
Moje	můj	k3xOp1gFnSc1
krásná	krásný	k2eAgFnSc1d1
čarodějka	čarodějka	k1gFnSc1
Původní	původní	k2eAgFnSc1d1
název	název	k1gInSc4
</s>
<s>
Bewitched	Bewitched	k1gInSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
102	#num#	k4
minut	minuta	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
komedie	komedie	k1gFnPc1
fantasy	fantas	k1gInPc1
romantický	romantický	k2eAgInSc1d1
Námět	námět	k1gInSc4
</s>
<s>
Sol	sol	k1gInSc1
Saks	Saks	k1gMnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Nora	Nor	k1gMnSc2
Ephron	Ephron	k1gMnSc1
Delia	Delius	k1gMnSc2
Ephron	Ephron	k1gMnSc1
Adam	Adam	k1gMnSc1
McKay	McKa	k2eAgFnPc4d1
Režie	režie	k1gFnPc4
</s>
<s>
Nora	Nora	k1gFnSc1
Ephron	Ephron	k1gInSc1
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Nicole	Nicola	k1gFnSc3
Kidman	Kidman	k1gMnSc1
Will	Will	k1gMnSc1
Ferrell	Ferrell	k1gMnSc1
Shirley	Shirlea	k1gFnSc2
MacLaineMichael	MacLaineMichael	k1gMnSc1
CaineJason	CaineJason	k1gMnSc1
Schwartzman	Schwartzman	k1gMnSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Tim	Tim	k?
Bevan	Bevan	k1gInSc1
Eric	Eric	k1gFnSc1
Fellner	Fellner	k1gMnSc1
Kevin	Kevin	k1gMnSc1
Misher	Mishra	k1gFnPc2
Hudba	hudba	k1gFnSc1
</s>
<s>
George	George	k1gInSc1
Fenton	Fenton	k1gInSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
John	John	k1gMnSc1
Lindley	Lindlea	k1gFnSc2
Kostýmy	kostým	k1gInPc4
</s>
<s>
Mary	Mary	k1gFnSc1
Zophres	Zophresa	k1gFnPc2
Střih	střih	k1gInSc4
</s>
<s>
Tia	Tia	k?
Nolan	Nolan	k1gInSc1
Zvuk	zvuk	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
MacMillan	MacMillan	k1gMnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Neil	Neil	k1gMnSc1
Spisak	Spisak	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2005	#num#	k4
Produkční	produkční	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
</s>
<s>
Red	Red	k?
Wagon	Wagon	k1gInSc1
Entertainment	Entertainment	k1gMnSc1
Distribuce	distribuce	k1gFnSc1
</s>
<s>
Columbia	Columbia	k1gFnSc1
Pictures	Pictures	k1gMnSc1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
85	#num#	k4
000	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
Tržby	tržba	k1gFnSc2
</s>
<s>
131	#num#	k4
400	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
Moje	můj	k3xOp1gFnSc1
krásná	krásný	k2eAgFnSc1d1
čarodějka	čarodějka	k1gFnSc1
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
FDb	FDb	k1gFnPc1
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Moje	můj	k3xOp1gFnSc1
krásná	krásný	k2eAgFnSc1d1
čarodějka	čarodějka	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
Bewitched	Bewitched	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
filmová	filmový	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režisérkou	režisérka	k1gFnSc7
filmu	film	k1gInSc2
je	být	k5eAaImIp3nS
Nora	Nora	k1gFnSc1
Ephron	Ephron	k1gFnSc1xF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
ve	v	k7c6
filmu	film	k1gInSc6
ztvárnili	ztvárnit	k5eAaPmAgMnP
Nicole	Nicole	k1gFnSc1
Kidman	Kidman	k1gMnSc1
<g/>
,	,	kIx,
Will	Will	k1gMnSc1
Ferrell	Ferrell	k1gMnSc1
<g/>
,	,	kIx,
Shirley	Shirlea	k1gFnPc1
MacLaine	MacLain	k1gInSc5
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Caine	Cain	k1gInSc5
a	a	k8xC
Jason	Jason	k1gMnSc1
Schwartzman	Schwartzman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Nicole	Nicola	k1gFnSc3
Kidman	Kidman	k1gMnSc1
</s>
<s>
Isabel	Isabela	k1gFnPc2
Bigelow	Bigelow	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Samantha	Samantha	k1gFnSc1
Stephens	Stephensa	k1gFnPc2
</s>
<s>
Will	Will	k1gMnSc1
Ferrell	Ferrell	k1gMnSc1
</s>
<s>
Jack	Jack	k1gMnSc1
Wyatt	Wyatt	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Darrin	Darrin	k1gInSc1
Stephens	Stephensa	k1gFnPc2
</s>
<s>
Shirley	Shirlea	k1gFnPc1
MacLaine	MacLain	k1gMnSc5
</s>
<s>
Iris	iris	k1gInSc1
Smythson	Smythson	k1gInSc1
<g/>
/	/	kIx~
<g/>
Endora	Endora	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Caine	Cain	k1gInSc5
</s>
<s>
Nigel	Nigel	k1gMnSc1
Bigelow	Bigelow	k1gMnSc1
</s>
<s>
Jason	Jason	k1gMnSc1
Schwartzman	Schwartzman	k1gMnSc1
</s>
<s>
Ritchie	Ritchie	k1gFnSc1
</s>
<s>
Kristin	Kristin	k2eAgInSc1d1
Chenoweth	Chenoweth	k1gInSc1
</s>
<s>
Maria	Maria	k1gFnSc1
Kelly	Kella	k1gFnSc2
</s>
<s>
Heather	Heathra	k1gFnPc2
Burns	Burnsa	k1gFnPc2
</s>
<s>
Nina	Nina	k1gFnSc1
</s>
<s>
Jim	on	k3xPp3gMnPc3
Turner	turner	k1gMnSc1
</s>
<s>
Larry	Larra	k1gFnPc1
</s>
<s>
Stephen	Stephen	k2eAgInSc1d1
Colbert	Colbert	k1gInSc1
</s>
<s>
Stu	sto	k4xCgNnSc3
Robison	Robisona	k1gFnPc2
</s>
<s>
Steve	Steve	k1gMnSc1
Carell	Carell	k1gMnSc1
</s>
<s>
strýc	strýc	k1gMnSc1
Arthur	Arthur	k1gMnSc1
</s>
<s>
Ed	Ed	k?
McMahon	McMahon	k1gMnSc1
</s>
<s>
sebe	sebe	k3xPyFc4
</s>
<s>
Reakce	reakce	k1gFnSc1
</s>
<s>
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
12	#num#	k4
<g/>
.	.	kIx.
červenci	červenec	k1gInSc3
2018	#num#	k4
</s>
<s>
csfd	csfd	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
imdb	imdb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
</s>
<s>
fdb	fdb	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Moje	můj	k3xOp1gFnSc1
krásná	krásný	k2eAgFnSc1d1
čarodějka	čarodějka	k1gFnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Moje	můj	k3xOp1gFnSc1
krásná	krásný	k2eAgFnSc1d1
čarodějka	čarodějka	k1gFnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Moje	můj	k3xOp1gFnSc1
krásná	krásný	k2eAgFnSc1d1
čarodějka	čarodějka	k1gFnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
