<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
kontaminace	kontaminace	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
tedy	tedy	k8xC
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
samovolný	samovolný	k2eAgInSc4d1
přírodní	přírodní	k2eAgInSc4d1
děj	děj	k1gInSc4
<g/>
.	.	kIx.
</s>