<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
kontaminace	kontaminace	k1gFnSc1
</s>
<s>
Výstražný	výstražný	k2eAgInSc1d1
znak	znak	k1gInSc1
radioaktivity	radioaktivita	k1gFnSc2
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
kontaminace	kontaminace	k1gFnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
nežádoucí	žádoucí	k2eNgNnSc4d1
znečištění	znečištění	k1gNnSc4
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
ovzduší	ovzduší	k1gNnSc2
<g/>
,	,	kIx,
budov	budova	k1gFnPc2
aj.	aj.	kA
<g/>
,	,	kIx,
látkami	látka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
radionuklidy	radionuklid	k1gInPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
látkami	látka	k1gFnPc7
<g/>
,	,	kIx,
emitujícími	emitující	k2eAgInPc7d1
ionizující	ionizující	k2eAgFnSc4d1
záření	záření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
kontaminace	kontaminace	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
tedy	tedy	k8xC
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c4
samovolný	samovolný	k2eAgInSc4d1
přírodní	přírodní	k2eAgInSc4d1
děj	děj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
vzniku	vznik	k1gInSc2
zamoření	zamoření	k1gNnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
jednat	jednat	k5eAaImF
o	o	k7c4
případ	případ	k1gInSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
jaderného	jaderný	k2eAgInSc2d1
výbuchu	výbuch	k1gInSc2
(	(	kIx(
<g/>
Hirošima	Hirošima	k1gFnSc1
<g/>
,	,	kIx,
Nagasaki	Nagasaki	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
havárie	havárie	k1gFnSc1
jaderného	jaderný	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
jaderné	jaderný	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
(	(	kIx(
<g/>
Černobyl	Černobyl	k1gInSc1
<g/>
,	,	kIx,
Fukušima	Fukušima	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
samotným	samotný	k2eAgInSc7d1
provozem	provoz	k1gInSc7
jaderného	jaderný	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
využívající	využívající	k2eAgFnSc2d1
radionuklidů	radionuklid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Přípustné	přípustný	k2eAgNnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
pro	pro	k7c4
zdraví	zdraví	k1gNnSc4
„	„	k?
<g/>
neškodné	škodný	k2eNgFnSc2d1
<g/>
“	“	k?
dávky	dávka	k1gFnSc2
radioaktivního	radioaktivní	k2eAgNnSc2d1
záření	záření	k1gNnSc2
jsou	být	k5eAaImIp3nP
normalizovány	normalizován	k2eAgInPc1d1
státními	státní	k2eAgInPc7d1
úřady	úřad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Malá	malý	k2eAgFnSc1d1
československá	československý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
V.	V.	kA
svazek	svazek	k1gInSc4
<g/>
,	,	kIx,
vydala	vydat	k5eAaPmAgFnS
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1986	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
československá	československý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
V.	V.	kA
svazek	svazek	k1gInSc4
<g/>
,	,	kIx,
vydala	vydat	k5eAaPmAgFnS
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1986	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
</s>
