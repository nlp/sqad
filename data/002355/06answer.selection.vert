<s>
Jsou	být	k5eAaImIp3nP	být
inaktivovány	inaktivovat	k5eAaBmNgFnP	inaktivovat
teplotou	teplota	k1gFnSc7	teplota
nad	nad	k7c4	nad
48	[number]	k4	48
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pasterizaci	pasterizace	k1gFnSc4	pasterizace
nebo	nebo	k8xC	nebo
vaření	vaření	k1gNnSc1	vaření
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
nepřežijí	přežít	k5eNaPmIp3nP	přežít
<g/>
.	.	kIx.	.
</s>
