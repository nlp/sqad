<s>
Kampylobakterióza	Kampylobakterióza	k1gFnSc1	Kampylobakterióza
je	být	k5eAaImIp3nS	být
závažná	závažný	k2eAgFnSc1d1	závažná
alimentární	alimentární	k2eAgFnSc1d1	alimentární
infekce	infekce	k1gFnSc1	infekce
člověka	člověk	k1gMnSc2	člověk
připomínající	připomínající	k2eAgFnSc4d1	připomínající
salmonelózu	salmonelóza	k1gFnSc4	salmonelóza
<g/>
,	,	kIx,	,
vyvolávaná	vyvolávaný	k2eAgFnSc1d1	vyvolávaná
termofilní	termofilní	k2eAgFnSc7d1	termofilní
bakterií	bakterie	k1gFnSc7	bakterie
Campylobacter	Campylobactra	k1gFnPc2	Campylobactra
jejuni	jejuni	k5eAaImNgMnP	jejuni
<g/>
,	,	kIx,	,
a	a	k8xC	a
související	související	k2eAgMnSc1d1	související
často	často	k6eAd1	často
s	s	k7c7	s
konzumací	konzumace	k1gFnSc7	konzumace
nedostatečně	dostatečně	k6eNd1	dostatečně
tepelně	tepelně	k6eAd1	tepelně
upravených	upravený	k2eAgFnPc2d1	upravená
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
drůbežího	drůbeží	k2eAgNnSc2d1	drůbeží
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
drůbežích	drůbeží	k2eAgInPc2d1	drůbeží
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
enterokolitidou	enterokolitida	k1gFnSc7	enterokolitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
tenkého	tenký	k2eAgNnSc2d1	tenké
a	a	k8xC	a
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
systémovým	systémový	k2eAgNnSc7d1	systémové
onemocněním	onemocnění	k1gNnSc7	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
probíhá	probíhat	k5eAaImIp3nS	probíhat
onemocnění	onemocnění	k1gNnSc1	onemocnění
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
subklinicky	subklinicky	k6eAd1	subklinicky
až	až	k6eAd1	až
chronicky	chronicky	k6eAd1	chronicky
probíhající	probíhající	k2eAgFnSc2d1	probíhající
střevní	střevní	k2eAgFnSc2d1	střevní
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
enteritidy	enteritida	k1gFnSc2	enteritida
a	a	k8xC	a
hepatitidy	hepatitida	k1gFnSc2	hepatitida
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
spirálovitá	spirálovitý	k2eAgFnSc1d1	spirálovitá
podmíněně	podmíněně	k6eAd1	podmíněně
patogenní	patogenní	k2eAgFnSc1d1	patogenní
bakterie	bakterie	k1gFnSc1	bakterie
Campylobacter	Campylobacter	k1gInSc1	Campylobacter
jejuni	jejun	k1gMnPc1	jejun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
příčina	příčina	k1gFnSc1	příčina
humánních	humánní	k2eAgFnPc2d1	humánní
enteritid	enteritida	k1gFnPc2	enteritida
identifikována	identifikován	k2eAgFnSc1d1	identifikována
skupina	skupina	k1gFnSc1	skupina
termofilních	termofilní	k2eAgFnPc2d1	termofilní
vibrio-like	vibrioike	k1gFnPc2	vibrio-like
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
mikroaerofilními	mikroaerofilní	k2eAgNnPc7d1	mikroaerofilní
vibrii	vibrio	k1gNnPc7	vibrio
skotu	skot	k1gInSc2	skot
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
přeřazeny	přeřadit	k5eAaPmNgInP	přeřadit
do	do	k7c2	do
samostatného	samostatný	k2eAgInSc2d1	samostatný
rodu	rod	k1gInSc2	rod
Campylobacter	Campylobactra	k1gFnPc2	Campylobactra
<g/>
.	.	kIx.	.
</s>
<s>
Kampylobaktery	Kampylobakter	k1gInPc1	Kampylobakter
byly	být	k5eAaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
ve	v	k7c6	v
střevním	střevní	k2eAgInSc6d1	střevní
traktu	trakt	k1gInSc6	trakt
mnoha	mnoho	k4c2	mnoho
domácích	domácí	k1gMnPc2	domácí
i	i	k8xC	i
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgNnPc2d1	žijící
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
zdravých	zdravý	k1gMnPc2	zdravý
i	i	k8xC	i
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Drůbež	drůbež	k1gFnSc1	drůbež
představuje	představovat	k5eAaImIp3nS	představovat
důležitý	důležitý	k2eAgInSc4d1	důležitý
rezervoár	rezervoár	k1gInSc4	rezervoár
těchto	tento	k3xDgFnPc2	tento
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
zdroj	zdroj	k1gInSc1	zdroj
infekce	infekce	k1gFnSc2	infekce
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
termofilních	termofilní	k2eAgInPc2d1	termofilní
kampylobakterů	kampylobakter	k1gInPc2	kampylobakter
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
vyššími	vysoký	k2eAgInPc7d2	vyšší
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
kultivační	kultivační	k2eAgFnSc4d1	kultivační
teplotu	teplota	k1gFnSc4	teplota
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
3	[number]	k4	3
druhy	druh	k1gInPc4	druh
-	-	kIx~	-
C.	C.	kA	C.
jejuni	jejueň	k1gFnSc6	jejueň
<g/>
,	,	kIx,	,
C.	C.	kA	C.
coli	col	k1gFnSc2	col
a	a	k8xC	a
C.	C.	kA	C.
lari	lari	k1gInSc1	lari
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
postavení	postavení	k1gNnSc4	postavení
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
C.	C.	kA	C.
jejuni	jejueň	k1gFnSc6	jejueň
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xC	jako
nepatogenní	patogenní	k2eNgMnSc1d1	nepatogenní
komensál	komensál	k1gMnSc1	komensál
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
střevním	střevní	k2eAgInSc6d1	střevní
traktu	trakt	k1gInSc6	trakt
i	i	k9	i
klinicky	klinicky	k6eAd1	klinicky
zdravé	zdravý	k2eAgFnSc2d1	zdravá
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
coli	coli	k6eAd1	coli
je	být	k5eAaImIp3nS	být
častěji	často	k6eAd2	často
izolován	izolován	k2eAgMnSc1d1	izolován
z	z	k7c2	z
prasat	prase	k1gNnPc2	prase
<g/>
,	,	kIx,	,
C.	C.	kA	C.
lari	lari	k1gInPc1	lari
z	z	k7c2	z
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
racků	racek	k1gMnPc2	racek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kampylobaktery	Kampylobakter	k1gInPc1	Kampylobakter
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc4d1	široké
hostitelské	hostitelský	k2eAgNnSc4d1	hostitelské
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
kampylobakterů	kampylobakter	k1gMnPc2	kampylobakter
byly	být	k5eAaImAgInP	být
izolovány	izolovat	k5eAaBmNgInP	izolovat
z	z	k7c2	z
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
holubů	holub	k1gMnPc2	holub
<g/>
,	,	kIx,	,
pštrosů	pštros	k1gMnPc2	pštros
<g/>
,	,	kIx,	,
vrabců	vrabec	k1gMnPc2	vrabec
<g/>
,	,	kIx,	,
dropů	drop	k1gMnPc2	drop
<g/>
,	,	kIx,	,
vran	vrána	k1gFnPc2	vrána
a	a	k8xC	a
pernaté	pernatý	k2eAgFnSc2d1	pernatá
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
koroptví	koroptev	k1gFnPc2	koroptev
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
a	a	k8xC	a
křepelek	křepelka	k1gFnPc2	křepelka
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
izolace	izolace	k1gFnPc1	izolace
z	z	k7c2	z
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
albatrosi	albatros	k1gMnPc1	albatros
a	a	k8xC	a
buřňáci	buřňák	k1gMnPc1	buřňák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
racků	racek	k1gMnPc2	racek
a	a	k8xC	a
migrujících	migrující	k2eAgMnPc2d1	migrující
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
C.	C.	kA	C.
jejuni	jejuen	k2eAgMnPc1d1	jejuen
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
8	[number]	k4	8
%	%	kIx~	%
vzorků	vzorek	k1gInPc2	vzorek
odebraných	odebraný	k2eAgInPc2d1	odebraný
od	od	k7c2	od
8	[number]	k4	8
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
řádů	řád	k1gInPc2	řád
Columbiformes	Columbiformesa	k1gFnPc2	Columbiformesa
a	a	k8xC	a
Passeriformes	Passeriformesa	k1gFnPc2	Passeriformesa
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
astrildovití	astrildovitý	k2eAgMnPc1d1	astrildovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
frekvence	frekvence	k1gFnSc1	frekvence
izolací	izolace	k1gFnPc2	izolace
je	být	k5eAaImIp3nS	být
zjišťována	zjišťovat	k5eAaImNgFnS	zjišťovat
u	u	k7c2	u
mrchožravých	mrchožravý	k2eAgMnPc2d1	mrchožravý
a	a	k8xC	a
všežravých	všežravý	k2eAgMnPc2d1	všežravý
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
semenožravými	semenožravý	k2eAgMnPc7d1	semenožravý
<g/>
.	.	kIx.	.
</s>
<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
jsou	být	k5eAaImIp3nP	být
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
C.	C.	kA	C.
jejuni	jejuen	k2eAgMnPc1d1	jejuen
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
myši	myš	k1gFnPc1	myš
<g/>
,	,	kIx,	,
krysy	krysa	k1gFnPc1	krysa
<g/>
,	,	kIx,	,
křečci	křeček	k1gMnPc1	křeček
<g/>
,	,	kIx,	,
fretky	fretka	k1gFnPc1	fretka
a	a	k8xC	a
primáti	primát	k1gMnPc1	primát
<g/>
.	.	kIx.	.
</s>
<s>
Drůbež	drůbež	k1gFnSc1	drůbež
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c2	za
primárního	primární	k2eAgMnSc2d1	primární
rezervoárového	rezervoárový	k2eAgMnSc2d1	rezervoárový
hostitele	hostitel	k1gMnSc2	hostitel
kampylobakterů	kampylobakter	k1gInPc2	kampylobakter
-	-	kIx~	-
80-100	[number]	k4	80-100
%	%	kIx~	%
hrabavé	hrabavý	k2eAgFnSc2d1	hrabavá
i	i	k8xC	i
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
infikováno	infikován	k2eAgNnSc1d1	infikováno
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
dospělou	dospělý	k2eAgFnSc4d1	dospělá
drůbež	drůbež	k1gFnSc4	drůbež
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
většinou	většina	k1gFnSc7	většina
nepatogenní	patogenní	k2eNgFnSc7d1	nepatogenní
<g/>
.	.	kIx.	.
</s>
<s>
Hejno	hejno	k1gNnSc1	hejno
drůbeže	drůbež	k1gFnSc2	drůbež
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
infikováno	infikovat	k5eAaBmNgNnS	infikovat
i	i	k9	i
více	hodně	k6eAd2	hodně
druhy	druh	k1gInPc4	druh
kampylobakterů	kampylobakter	k1gInPc2	kampylobakter
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
infekce	infekce	k1gFnPc1	infekce
šíří	šířit	k5eAaImIp3nP	šířit
nejčastěji	často	k6eAd3	často
nepřímo	přímo	k6eNd1	přímo
přes	přes	k7c4	přes
trusem	trus	k1gInSc7	trus
kontaminovanou	kontaminovaný	k2eAgFnSc4d1	kontaminovaná
podestýlku	podestýlka	k1gFnSc4	podestýlka
<g/>
,	,	kIx,	,
krmivo	krmivo	k1gNnSc4	krmivo
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
infekce	infekce	k1gFnSc2	infekce
dostačuje	dostačovat	k5eAaImIp3nS	dostačovat
jen	jen	k9	jen
nepatrné	patrný	k2eNgNnSc1d1	nepatrné
množství	množství	k1gNnSc1	množství
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
možný	možný	k2eAgInSc4d1	možný
přenos	přenos	k1gInSc4	přenos
vektory	vektor	k1gInPc7	vektor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
mouchy	moucha	k1gFnPc4	moucha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kontaminované	kontaminovaný	k2eAgFnSc2d1	kontaminovaná
částice	částice	k1gFnSc2	částice
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
nečistot	nečistota	k1gFnPc2	nečistota
vznášející	vznášející	k2eAgFnSc2d1	vznášející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
také	také	k9	také
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
a	a	k8xC	a
pracovní	pracovní	k2eAgFnPc1d1	pracovní
pomůcky	pomůcka	k1gFnPc1	pomůcka
kontaminované	kontaminovaný	k2eAgFnPc1d1	kontaminovaná
trusem	trus	k1gInSc7	trus
nebo	nebo	k8xC	nebo
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
či	či	k8xC	či
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavlečení	zavlečení	k1gNnSc6	zavlečení
nákazy	nákaza	k1gFnSc2	nákaza
do	do	k7c2	do
chovů	chov	k1gInPc2	chov
na	na	k7c6	na
podestýlce	podestýlka	k1gFnSc6	podestýlka
se	se	k3xPyFc4	se
rozšíří	rozšířit	k5eAaPmIp3nP	rozšířit
infekce	infekce	k1gFnPc1	infekce
na	na	k7c4	na
všechna	všechen	k3xTgNnPc4	všechen
zvířata	zvíře	k1gNnPc4	zvíře
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
během	během	k7c2	během
1-2	[number]	k4	1-2
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
většina	většina	k1gFnSc1	většina
dospělé	dospělý	k2eAgFnSc2d1	dospělá
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
infikována	infikovat	k5eAaBmNgFnS	infikovat
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
vertikálním	vertikální	k2eAgInSc6d1	vertikální
přenosu	přenos	k1gInSc6	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
ve	v	k7c6	v
vaječném	vaječný	k2eAgInSc6d1	vaječný
obsahu	obsah	k1gInSc6	obsah
ani	ani	k8xC	ani
na	na	k7c6	na
skořápce	skořápka	k1gFnSc6	skořápka
konzumních	konzumní	k2eAgNnPc2d1	konzumní
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělá	ojedinělý	k2eAgFnSc1d1	ojedinělá
izolace	izolace	k1gFnSc1	izolace
C.	C.	kA	C.
jejuni	jejuň	k1gMnSc3	jejuň
z	z	k7c2	z
podskořápečných	podskořápečný	k2eAgFnPc2d1	podskořápečný
membrán	membrána	k1gFnPc2	membrána
je	být	k5eAaImIp3nS	být
připisována	připisován	k2eAgFnSc1d1	připisována
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
poškození	poškození	k1gNnSc3	poškození
vaječné	vaječný	k2eAgFnSc2d1	vaječná
skořápky	skořápka	k1gFnSc2	skořápka
<g/>
.	.	kIx.	.
</s>
<s>
Orální	orální	k2eAgInSc1d1	orální
příjem	příjem	k1gInSc1	příjem
původce	původce	k1gMnSc2	původce
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
buď	buď	k8xC	buď
jen	jen	k9	jen
osídlení	osídlení	k1gNnSc1	osídlení
nebo	nebo	k8xC	nebo
i	i	k9	i
infekci	infekce	k1gFnSc4	infekce
střevní	střevní	k2eAgFnSc2d1	střevní
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ilea	ileum	k1gNnSc2	ileum
(	(	kIx(	(
<g/>
kyčelník	kyčelník	k1gInSc1	kyčelník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slepých	slepý	k2eAgNnPc2d1	slepé
střev	střevo	k1gNnPc2	střevo
a	a	k8xC	a
konečníku	konečník	k1gInSc2	konečník
<g/>
.	.	kIx.	.
</s>
<s>
Původce	původce	k1gMnSc1	původce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
podmíněného	podmíněný	k2eAgMnSc4d1	podmíněný
patogena	patogen	k1gMnSc4	patogen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
může	moct	k5eAaImIp3nS	moct
perzistovat	perzistovat	k5eAaPmF	perzistovat
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc4	příčina
vzniku	vznik	k1gInSc2	vznik
nemoci	nemoc	k1gFnSc2	nemoc
u	u	k7c2	u
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
nejsou	být	k5eNaImIp3nP	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
patogennímu	patogenní	k2eAgNnSc3d1	patogenní
působení	působení	k1gNnSc3	působení
kampylobakterů	kampylobakter	k1gInPc2	kampylobakter
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pomnožení	pomnožení	k1gNnSc2	pomnožení
ve	v	k7c6	v
střevním	střevní	k2eAgNnSc6d1	střevní
luminu	lumen	k1gNnSc6	lumen
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
adheze	adheze	k1gFnSc2	adheze
na	na	k7c4	na
buňky	buňka	k1gFnPc4	buňka
sliznice	sliznice	k1gFnSc2	sliznice
střevní	střevní	k2eAgFnSc2d1	střevní
<g/>
)	)	kIx)	)
a	a	k8xC	a
produkcí	produkce	k1gFnSc7	produkce
toxinů	toxin	k1gInPc2	toxin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vodnatý	vodnatý	k2eAgInSc4d1	vodnatý
průjem	průjem	k1gInSc4	průjem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pomnožením	pomnožení	k1gNnSc7	pomnožení
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
sliznice	sliznice	k1gFnSc2	sliznice
a	a	k8xC	a
proniknutím	proniknutí	k1gNnSc7	proniknutí
až	až	k9	až
do	do	k7c2	do
lamina	lamin	k2eAgNnSc2d1	lamino
propria	proprium	k1gNnSc2	proprium
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc7d1	krevní
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
bakterie	bakterie	k1gFnPc1	bakterie
dostávají	dostávat	k5eAaImIp3nP	dostávat
sekundárně	sekundárně	k6eAd1	sekundárně
do	do	k7c2	do
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
sleziny	slezina	k1gFnSc2	slezina
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
a	a	k8xC	a
virulence	virulence	k1gFnSc1	virulence
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
geneticky	geneticky	k6eAd1	geneticky
determinovaná	determinovaný	k2eAgFnSc1d1	determinovaná
vnímavost	vnímavost	k1gFnSc1	vnímavost
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
a	a	k8xC	a
věk	věk	k1gInSc1	věk
hostitele	hostitel	k1gMnSc2	hostitel
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
možnými	možný	k2eAgInPc7d1	možný
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c4	v
patogenezi	patogeneze	k1gFnSc4	patogeneze
nemoci	nemoc	k1gFnSc2	nemoc
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
po	po	k7c4	po
experimentální	experimentální	k2eAgFnSc4d1	experimentální
inokulaci	inokulace	k1gFnSc4	inokulace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
2-4	[number]	k4	2-4
dny	den	k1gInPc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
u	u	k7c2	u
dospělé	dospělý	k2eAgFnSc2d1	dospělá
drůbeže	drůbež	k1gFnSc2	drůbež
probíhají	probíhat	k5eAaImIp3nP	probíhat
zpravidla	zpravidla	k6eAd1	zpravidla
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
.	.	kIx.	.
</s>
<s>
Toxigenní	Toxigenní	k2eAgInPc1d1	Toxigenní
a	a	k8xC	a
invazivní	invazivní	k2eAgInPc1d1	invazivní
kmeny	kmen	k1gInPc1	kmen
C.	C.	kA	C.
jejuni	jejuň	k1gMnPc7	jejuň
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
enteritidu	enteritida	k1gFnSc4	enteritida
a	a	k8xC	a
úhyn	úhyn	k1gInSc4	úhyn
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
onemocnění	onemocnění	k1gNnSc2	onemocnění
závisí	záviset	k5eAaImIp3nS	záviset
zejména	zejména	k9	zejména
na	na	k7c6	na
věku	věk	k1gInSc6	věk
infikované	infikovaný	k2eAgFnSc2d1	infikovaná
drůbeže	drůbež	k1gFnSc2	drůbež
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
věková	věkový	k2eAgFnSc1d1	věková
rezistence	rezistence	k1gFnSc1	rezistence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc2	velikost
infekční	infekční	k2eAgFnSc2d1	infekční
dávky	dávka	k1gFnSc2	dávka
a	a	k8xC	a
invazivnosti	invazivnost	k1gFnSc2	invazivnost
kmene	kmen	k1gInSc2	kmen
C.	C.	kA	C.
jejuni	jejun	k1gMnPc5	jejun
<g/>
.	.	kIx.	.
</s>
<s>
Konkurentní	Konkurentní	k2eAgFnPc1d1	Konkurentní
nemoci	nemoc	k1gFnPc1	nemoc
<g/>
,	,	kIx,	,
stresory	stresor	k1gInPc1	stresor
prostředí	prostředí	k1gNnSc2	prostředí
či	či	k8xC	či
imunosuprese	imunosuprese	k1gFnSc2	imunosuprese
mohou	moct	k5eAaImIp3nP	moct
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
patogenitu	patogenita	k1gFnSc4	patogenita
C.	C.	kA	C.
jejuni	jejuň	k1gMnSc3	jejuň
<g/>
.	.	kIx.	.
</s>
<s>
Sporadicky	sporadicky	k6eAd1	sporadicky
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
akutní	akutní	k2eAgInSc1d1	akutní
průběh	průběh	k1gInSc1	průběh
kampylobakteriózy	kampylobakterióza	k1gFnSc2	kampylobakterióza
se	se	k3xPyFc4	se
u	u	k7c2	u
mláďat	mládě	k1gNnPc2	mládě
projevuje	projevovat	k5eAaImIp3nS	projevovat
depresí	deprese	k1gFnSc7	deprese
<g/>
,	,	kIx,	,
anorexií	anorexie	k1gFnSc7	anorexie
<g/>
,	,	kIx,	,
zpomalením	zpomalení	k1gNnSc7	zpomalení
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
trvajícím	trvající	k2eAgMnPc3d1	trvající
vodnatým	vodnatý	k2eAgMnPc3d1	vodnatý
až	až	k8xS	až
mukózním	mukózní	k2eAgMnPc3d1	mukózní
průjem	průjem	k1gInSc1	průjem
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlhkou	vlhký	k2eAgFnSc7d1	vlhká
podestýlkou	podestýlka	k1gFnSc7	podestýlka
a	a	k8xC	a
krátkým	krátký	k2eAgNnSc7d1	krátké
trváním	trvání	k1gNnSc7	trvání
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
mortalita	mortalita	k1gFnSc1	mortalita
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
u	u	k7c2	u
výletků	výletek	k1gInPc2	výletek
malých	malý	k2eAgMnPc2d1	malý
zpěvných	zpěvný	k2eAgMnPc2d1	zpěvný
ptáků	pták	k1gMnPc2	pták
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
kampylobaktery	kampylobakter	k1gInPc4	kampylobakter
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
C.	C.	kA	C.
jejuni	jejunit	k5eAaImRp2nS	jejunit
se	se	k3xPyFc4	se
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
projevuje	projevovat	k5eAaImIp3nS	projevovat
dilatací	dilatace	k1gFnSc7	dilatace
střevního	střevní	k2eAgInSc2d1	střevní
traktu	trakt	k1gInSc2	trakt
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
akumulace	akumulace	k1gFnSc2	akumulace
hlenu	hlen	k1gInSc2	hlen
a	a	k8xC	a
vodnaté	vodnatý	k2eAgFnSc2d1	vodnatá
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
cytotoxických	cytotoxický	k2eAgFnPc6d1	cytotoxická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
infikujícího	infikující	k2eAgNnSc2d1	infikující
agens	agens	k1gNnSc2	agens
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
hemoragie	hemoragie	k1gFnSc1	hemoragie
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bažantů	bažant	k1gMnPc2	bažant
<g/>
,	,	kIx,	,
sýce	sýc	k1gMnSc2	sýc
rousného	rousný	k2eAgMnSc2d1	rousný
a	a	k8xC	a
lysek	lyska	k1gFnPc2	lyska
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
enteritida	enteritida	k1gFnSc1	enteritida
<g/>
,	,	kIx,	,
u	u	k7c2	u
racků	racek	k1gMnPc2	racek
eroze	eroze	k1gFnSc2	eroze
na	na	k7c6	na
svalnatém	svalnatý	k2eAgInSc6d1	svalnatý
žaludku	žaludek	k1gInSc6	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
kampylobakteriózy	kampylobakterióza	k1gFnSc2	kampylobakterióza
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
klinickém	klinický	k2eAgInSc6d1	klinický
a	a	k8xC	a
pitevním	pitevní	k2eAgInSc6d1	pitevní
nálezu	nález	k1gInSc6	nález
<g/>
,	,	kIx,	,
na	na	k7c6	na
izolaci	izolace	k1gFnSc6	izolace
a	a	k8xC	a
identifikaci	identifikace	k1gFnSc6	identifikace
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
Kampylobakterové	Kampylobakterový	k2eAgFnPc1d1	Kampylobakterový
enteritidy	enteritida	k1gFnPc1	enteritida
u	u	k7c2	u
nově	nově	k6eAd1	nově
líhnutých	líhnutý	k2eAgNnPc2d1	líhnutý
kuřat	kuře	k1gNnPc2	kuře
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišit	odlišit	k5eAaPmF	odlišit
zejména	zejména	k9	zejména
od	od	k7c2	od
infekcí	infekce	k1gFnPc2	infekce
Salmonella	Salmonell	k1gMnSc2	Salmonell
spp	spp	k?	spp
<g/>
.	.	kIx.	.
a	a	k8xC	a
E.	E.	kA	E.
coli	coli	k6eAd1	coli
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bakterie	bakterie	k1gFnSc1	bakterie
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Vibrio	vibrio	k1gNnSc4	vibrio
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
především	především	k9	především
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
také	také	k9	také
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
prokázána	prokázán	k2eAgFnSc1d1	prokázána
bakterie	bakterie	k1gFnSc1	bakterie
podobná	podobný	k2eAgFnSc1d1	podobná
Brachyspira	Brachyspira	k1gFnSc1	Brachyspira
hyodysenteriae	hyodysenteriae	k1gFnSc1	hyodysenteriae
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgNnSc1d1	způsobující
střevní	střevní	k2eAgNnSc1d1	střevní
poškození	poškození	k1gNnSc1	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Kampylobakterióza	Kampylobakterióza	k1gFnSc1	Kampylobakterióza
většinou	většina	k1gFnSc7	většina
probíhá	probíhat	k5eAaImIp3nS	probíhat
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
,	,	kIx,	,
enteritidy	enteritida	k1gFnPc1	enteritida
bývají	bývat	k5eAaImIp3nP	bývat
přehlíženy	přehlížen	k2eAgFnPc1d1	přehlížena
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
jiných	jiný	k2eAgFnPc2d1	jiná
infekcí	infekce	k1gFnPc2	infekce
či	či	k8xC	či
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potvrzení	potvrzení	k1gNnSc2	potvrzení
diagnózy	diagnóza	k1gFnSc2	diagnóza
a	a	k8xC	a
zjištění	zjištění	k1gNnSc2	zjištění
citlivosti	citlivost	k1gFnSc2	citlivost
původce	původce	k1gMnSc2	původce
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
použít	použít	k5eAaPmF	použít
účinné	účinný	k2eAgFnSc2d1	účinná
antimikrobiální	antimikrobiální	k2eAgFnSc2d1	antimikrobiální
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
návrat	návrat	k1gInSc1	návrat
nemoci	nemoc	k1gFnSc2	nemoc
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
používání	používání	k1gNnSc1	používání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
rezistentních	rezistentní	k2eAgInPc2d1	rezistentní
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
preventivní	preventivní	k2eAgInPc1d1	preventivní
se	se	k3xPyFc4	se
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
všechna	všechen	k3xTgNnPc4	všechen
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
ničí	ničit	k5eAaImIp3nP	ničit
původce	původce	k1gMnSc2	původce
a	a	k8xC	a
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
zavlečení	zavlečení	k1gNnSc4	zavlečení
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
kompletní	kompletní	k2eAgFnSc1d1	kompletní
eradikace	eradikace	k1gFnSc1	eradikace
kampylobakterů	kampylobakter	k1gMnPc2	kampylobakter
z	z	k7c2	z
chovů	chov	k1gInPc2	chov
drůbeže	drůbež	k1gFnSc2	drůbež
asi	asi	k9	asi
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgMnPc1d2	významnější
jsou	být	k5eAaImIp3nP	být
opatření	opatření	k1gNnPc4	opatření
na	na	k7c6	na
porážkách	porážka	k1gFnPc6	porážka
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
porážkou	porážka	k1gFnSc7	porážka
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
zvířata	zvíře	k1gNnPc4	zvíře
krmit	krmit	k5eAaImF	krmit
<g/>
,	,	kIx,	,
transportní	transportní	k2eAgInPc4d1	transportní
prostředky	prostředek	k1gInPc4	prostředek
nutno	nutno	k6eAd1	nutno
dezinfikovat	dezinfikovat	k5eAaBmF	dezinfikovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
porážení	porážení	k1gNnSc6	porážení
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
ke	k	k7c3	k
kontaminaci	kontaminace	k1gFnSc3	kontaminace
povrchů	povrch	k1gInPc2	povrch
opracovaných	opracovaný	k2eAgInPc2d1	opracovaný
kuřat	kuře	k1gNnPc2	kuře
střevním	střevní	k2eAgInSc7d1	střevní
obsahem	obsah	k1gInSc7	obsah
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
provádět	provádět	k5eAaImF	provádět
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
povrchu	povrch	k1gInSc2	povrch
kuřat	kuře	k1gNnPc2	kuře
suchým	suchý	k2eAgInSc7d1	suchý
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
organickými	organický	k2eAgFnPc7d1	organická
kyselinami	kyselina	k1gFnPc7	kyselina
nebo	nebo	k8xC	nebo
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Kampylobaktery	Kampylobakter	k1gInPc1	Kampylobakter
přežívají	přežívat	k5eAaImIp3nP	přežívat
na	na	k7c6	na
potravinách	potravina	k1gFnPc6	potravina
skladovaných	skladovaný	k2eAgFnPc6d1	skladovaná
v	v	k7c6	v
chladničkách	chladnička	k1gFnPc6	chladnička
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
mohou	moct	k5eAaImIp3nP	moct
přežít	přežít	k5eAaPmF	přežít
i	i	k9	i
mražení	mražení	k1gNnSc4	mražení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
inaktivovány	inaktivovat	k5eAaBmNgFnP	inaktivovat
teplotou	teplota	k1gFnSc7	teplota
nad	nad	k7c4	nad
48	[number]	k4	48
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pasterizaci	pasterizace	k1gFnSc4	pasterizace
nebo	nebo	k8xC	nebo
vaření	vaření	k1gNnSc1	vaření
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
nepřežijí	přežít	k5eNaPmIp3nP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
manipulaci	manipulace	k1gFnSc6	manipulace
se	s	k7c7	s
syrovým	syrový	k2eAgNnSc7d1	syrové
masem	maso	k1gNnSc7	maso
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dodržovat	dodržovat	k5eAaImF	dodržovat
základní	základní	k2eAgNnPc4d1	základní
hygienická	hygienický	k2eAgNnPc4d1	hygienické
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
