<s>
Jakub	Jakub	k1gMnSc1	Jakub
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Jan	Jana	k1gFnPc2	Jana
<g/>
)	)	kIx)	)
o	o	k7c4	o
nejčastější	častý	k2eAgNnSc4d3	nejčastější
české	český	k2eAgNnSc4d1	české
mužské	mužský	k2eAgNnSc4d1	mužské
jméno	jméno	k1gNnSc4	jméno
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jakub	Jakub	k1gMnSc1	Jakub
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
patu	pata	k1gFnSc4	pata
–	–	k?	–
podle	podle	k7c2	podle
Bible	bible	k1gFnSc2	bible
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
patriarcha	patriarcha	k1gMnSc1	patriarcha
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
národa	národ	k1gInSc2	národ
Jákob	Jákob	k1gMnSc1	Jákob
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
druhé	druhý	k4xOgNnSc1	druhý
z	z	k7c2	z
dvojčat	dvojče	k1gNnPc2	dvojče
a	a	k8xC	a
držel	držet	k5eAaImAgInS	držet
se	se	k3xPyFc4	se
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
Ezaua	Ezau	k1gMnSc2	Ezau
za	za	k7c4	za
patu	pata	k1gFnSc4	pata
<g/>
.	.	kIx.	.
</s>
<s>
Vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
za	za	k7c4	za
patu	pata	k1gFnSc4	pata
<g/>
,	,	kIx,	,
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kuba	Kuba	k1gMnSc1	Kuba
<g/>
,	,	kIx,	,
Kubík	Kubík	k1gMnSc1	Kubík
<g/>
,	,	kIx,	,
Kubíček	Kubíček	k1gMnSc1	Kubíček
<g/>
,	,	kIx,	,
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
<g/>
,	,	kIx,	,
Kubajs	Kubajs	k1gInSc1	Kubajs
<g/>
,	,	kIx,	,
Kubča	Kubča	k1gFnSc1	Kubča
<g/>
,	,	kIx,	,
Kubíno	Kubína	k1gFnSc5	Kubína
<g/>
,	,	kIx,	,
Kubina	Kubin	k2eAgFnSc1d1	Kubina
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+9,2	+9,2	k4	+9,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
značném	značný	k2eAgInSc6d1	značný
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejčastější	častý	k2eAgNnSc4d3	nejčastější
mužské	mužský	k2eAgNnSc4d1	mužské
jméno	jméno	k1gNnSc4	jméno
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobných	podobný	k2eAgInPc6d1	podobný
průzkumech	průzkum	k1gInPc6	průzkum
za	za	k7c4	za
leden	leden	k1gInSc4	leden
1999	[number]	k4	1999
až	až	k9	až
leden	leden	k1gInSc1	leden
2006	[number]	k4	2006
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc4	jméno
Jakub	Jakub	k1gMnSc1	Jakub
vždy	vždy	k6eAd1	vždy
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
až	až	k8xS	až
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
–	–	k?	–
střídavě	střídavě	k6eAd1	střídavě
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Jakub	Jakub	k1gMnSc1	Jakub
Německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
<g/>
:	:	kIx,	:
Jakob	Jakob	k1gMnSc1	Jakob
Anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Jacob	Jacoba	k1gFnPc2	Jacoba
nebo	nebo	k8xC	nebo
James	James	k1gMnSc1	James
Albansky	Albansky	k1gMnSc1	Albansky
<g/>
:	:	kIx,	:
Jakup	Jakup	k1gMnSc1	Jakup
(	(	kIx(	(
<g/>
muslimská	muslimský	k2eAgFnSc1d1	muslimská
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jakov	Jakov	k1gInSc1	Jakov
(	(	kIx(	(
<g/>
katolická	katolický	k2eAgFnSc1d1	katolická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
Nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
:	:	kIx,	:
Jacob	Jacoba	k1gFnPc2	Jacoba
Francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Jacques	Jacques	k1gMnSc1	Jacques
žensky	žensky	k6eAd1	žensky
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Italsky	italsky	k6eAd1	italsky
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Giacobbe	Giacobb	k1gInSc5	Giacobb
nebo	nebo	k8xC	nebo
Giacomo	Giacoma	k1gFnSc5	Giacoma
Španělsky	Španělsko	k1gNnPc7	Španělsko
<g/>
:	:	kIx,	:
Jacobo	Jacoba	k1gFnSc5	Jacoba
<g/>
,	,	kIx,	,
Jaime	Jaim	k1gMnSc5	Jaim
<g/>
,	,	kIx,	,
Diego	Diega	k1gFnSc5	Diega
<g/>
,	,	kIx,	,
Jago	Jaga	k1gFnSc5	Jaga
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Santiago	Santiago	k1gNnSc1	Santiago
Rusky	Ruska	k1gFnSc2	Ruska
<g/>
,	,	kIx,	,
srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
Я	Я	k?	Я
(	(	kIx(	(
<g/>
Jakov	Jakov	k1gInSc1	Jakov
<g/>
)	)	kIx)	)
Maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Jakab	Jakab	k1gInSc1	Jakab
Hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
Ya	Ya	k1gFnSc1	Ya
<g/>
'	'	kIx"	'
<g/>
aqov	aqov	k1gInSc1	aqov
י	י	k?	י
Portugalsky	portugalsky	k6eAd1	portugalsky
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Tiago	Tiago	k6eAd1	Tiago
Finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
Jaakko	Jaakko	k1gNnSc1	Jaakko
Jákob	Jákob	k1gMnSc1	Jákob
–	–	k?	–
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
biblický	biblický	k2eAgMnSc1d1	biblický
patriarcha	patriarcha	k1gMnSc1	patriarcha
Jákob	Jákob	k1gMnSc1	Jákob
ze	z	k7c2	z
Sarúgu	Sarúg	k1gInSc2	Sarúg
<g/>
,	,	kIx,	,
syrský	syrský	k2eAgMnSc1d1	syrský
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
autor	autor	k1gMnSc1	autor
Jakub	Jakub	k1gMnSc1	Jakub
Spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Páně	páně	k2eAgNnSc2d1	páně
sv.	sv.	kA	sv.
Jakub	Jakub	k1gMnSc1	Jakub
Alfeův	Alfeův	k2eAgMnSc1d1	Alfeův
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
12	[number]	k4	12
apoštolů	apoštol	k1gMnPc2	apoštol
sv.	sv.	kA	sv.
Jakub	Jakub	k1gMnSc1	Jakub
Starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
12	[number]	k4	12
apoštolů	apoštol	k1gMnPc2	apoštol
sv.	sv.	kA	sv.
Diego	Diego	k6eAd1	Diego
z	z	k7c2	z
Alcalá	Alcalý	k2eAgFnSc1d1	Alcalá
sv.	sv.	kA	sv.
Juan	Juan	k1gMnSc1	Juan
<g />
.	.	kIx.	.
</s>
<s>
Diego	Diego	k1gNnSc1	Diego
Cuauhtlatoatzin	Cuauhtlatoatzina	k1gFnPc2	Cuauhtlatoatzina
Čeští	český	k2eAgMnPc1d1	český
nositelé	nositel	k1gMnPc1	nositel
Mistr	mistr	k1gMnSc1	mistr
Jakoubek	Jakoubek	k1gMnSc1	Jakoubek
ze	z	k7c2	z
Stříbra	stříbro	k1gNnSc2	stříbro
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
–	–	k?	–
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Jakub	Jakub	k1gMnSc1	Jakub
Bažant	Bažant	k1gMnSc1	Bažant
–	–	k?	–
český	český	k2eAgMnSc1d1	český
sportovní	sportovní	k2eAgMnSc1d1	sportovní
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
komentátor	komentátor	k1gMnSc1	komentátor
Jakub	Jakub	k1gMnSc1	Jakub
Deml	Deml	k1gMnSc1	Deml
–	–	k?	–
český	český	k2eAgMnSc1d1	český
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jakub	Jakub	k1gMnSc1	Jakub
Jan	Jan	k1gMnSc1	Jan
Ryba	Ryba	k1gMnSc1	Ryba
–	–	k?	–
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Jakub	Jakub	k1gMnSc1	Jakub
Janda	Janda	k1gMnSc1	Janda
–	–	k?	–
český	český	k2eAgMnSc1d1	český
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Jakub	Jakub	k1gMnSc1	Jakub
Schikaneder	Schikaneder	k1gMnSc1	Schikaneder
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
Jakub	Jakub	k1gMnSc1	Jakub
Smolík	Smolík	k1gMnSc1	Smolík
–	–	k?	–
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
Jakub	Jakub	k1gMnSc1	Jakub
Vágner	Vágner	k1gMnSc1	Vágner
–	–	k?	–
český	český	k2eAgMnSc1d1	český
rybář	rybář	k1gMnSc1	rybář
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gInSc4	moderátor
Zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
nositelé	nositel	k1gMnPc1	nositel
Jakob	Jakob	k1gMnSc1	Jakob
Anund	Anund	k1gMnSc1	Anund
–	–	k?	–
švédský	švédský	k2eAgMnSc1d1	švédský
král	král	k1gMnSc1	král
Jakub	Jakub	k1gMnSc1	Jakub
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Jakov	Jakov	k1gInSc4	Jakov
Jurovskij	Jurovskij	k1gFnSc2	Jurovskij
–	–	k?	–
exekutor	exekutor	k1gMnSc1	exekutor
carské	carský	k2eAgFnSc2d1	carská
rodiny	rodina	k1gFnSc2	rodina
Jacques	Jacques	k1gMnSc1	Jacques
Offenbach	Offenbach	k1gMnSc1	Offenbach
–	–	k?	–
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Jean-Jacques	Jean-Jacques	k1gMnSc1	Jean-Jacques
Rousseau	Rousseau	k1gMnSc1	Rousseau
–	–	k?	–
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
Diego	Diego	k1gMnSc1	Diego
Maradona	Maradon	k1gMnSc2	Maradon
–	–	k?	–
argentinský	argentinský	k2eAgInSc4d1	argentinský
<g />
.	.	kIx.	.
</s>
<s>
fotbalista	fotbalista	k1gMnSc1	fotbalista
Jakov	Jakov	k1gInSc1	Jakov
Sverdlov	Sverdlov	k1gInSc4	Sverdlov
–	–	k?	–
ruský	ruský	k2eAgMnSc1d1	ruský
politik	politik	k1gMnSc1	politik
Jakub	Jakub	k1gMnSc1	Jakub
Wajda	Wajda	k1gMnSc1	Wajda
–	–	k?	–
polský	polský	k2eAgMnSc1d1	polský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc1	oběť
katyňského	katyňský	k2eAgInSc2d1	katyňský
masakru	masakr	k1gInSc2	masakr
Názvy	název	k1gInPc1	název
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
postav	postava	k1gFnPc2	postava
Jakub	Jakub	k1gMnSc1	Jakub
fatalista	fatalista	k1gMnSc1	fatalista
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
pán	pán	k1gMnSc1	pán
–	–	k?	–
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Denise	Denisa	k1gFnSc3	Denisa
Diderota	Diderot	k1gMnSc2	Diderot
Jakub	Jakub	k1gMnSc1	Jakub
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
pán	pán	k1gMnSc1	pán
–	–	k?	–
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
Kapr	kapr	k1gMnSc1	kapr
Jakub	Jakub	k1gMnSc1	Jakub
–	–	k?	–
kreslená	kreslený	k2eAgFnSc1d1	kreslená
postavička	postavička	k1gFnSc1	postavička
<g/>
,	,	kIx,	,
hrdina	hrdina	k1gMnSc1	hrdina
knihy	kniha	k1gFnSc2	kniha
Pohádky	pohádka	k1gFnSc2	pohádka
z	z	k7c2	z
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Šumavy	Šumava	k1gFnSc2	Šumava
aneb	aneb	k?	aneb
Vyprávění	vyprávění	k1gNnSc1	vyprávění
kapra	kapr	k1gMnSc2	kapr
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
seriál	seriál	k1gInSc1	seriál
pro	pro	k7c4	pro
pořad	pořad	k1gInSc4	pořad
Večerníček	večerníček	k1gInSc1	večerníček
<g/>
.	.	kIx.	.
</s>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Svatý	svatý	k2eAgMnSc1d1	svatý
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
)	)	kIx)	)
–	–	k?	–
osada	osada	k1gFnSc1	osada
obce	obec	k1gFnSc2	obec
Církvice	Církvice	k1gFnSc2	Církvice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
s	s	k7c7	s
románským	románský	k2eAgInSc7d1	románský
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Jakub	Jakub	k1gMnSc1	Jakub
–	–	k?	–
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
Jakubčovice	Jakubčovice	k1gFnSc2	Jakubčovice
nad	nad	k7c7	nad
Odrou	Odra	k1gFnSc7	Odra
Jakubov	Jakubov	k1gInSc1	Jakubov
u	u	k7c2	u
Moravských	moravský	k2eAgInPc2d1	moravský
Budějovic	Budějovice	k1gInPc2	Budějovice
Jakubovice	Jakubovice	k1gFnSc2	Jakubovice
Lesní	lesní	k2eAgInSc1d1	lesní
Jakubov	Jakubov	k1gInSc1	Jakubov
Saint-Jacques	Saint-Jacquesa	k1gFnPc2	Saint-Jacquesa
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Compostela	Compostela	k1gFnSc1	Compostela
Santiago	Santiago	k1gNnSc1	Santiago
de	de	k?	de
Chile	Chile	k1gNnSc6	Chile
Santiago	Santiago	k1gNnSc4	Santiago
de	de	k?	de
Cuba	Cubum	k1gNnSc2	Cubum
Jakobíni	jakobín	k1gMnPc1	jakobín
Jakobité	jakobita	k1gMnPc1	jakobita
List	lista	k1gFnPc2	lista
Jakubův	Jakubův	k2eAgInSc4d1	Jakubův
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
"	"	kIx"	"
</s>
