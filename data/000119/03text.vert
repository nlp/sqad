<s>
Parník	parník	k1gInSc4	parník
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
loď	loď	k1gFnSc1	loď
poháněná	poháněný	k2eAgFnSc1d1	poháněná
parním	parní	k2eAgInSc7d1	parní
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
parní	parní	k2eAgFnSc7d1	parní
turbínou	turbína	k1gFnSc7	turbína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgNnSc7	tento
slovem	slovo	k1gNnSc7	slovo
neodborně	odborně	k6eNd1	odborně
označována	označován	k2eAgFnSc1d1	označována
i	i	k8xC	i
motorová	motorový	k2eAgFnSc1d1	motorová
loď	loď	k1gFnSc1	loď
pro	pro	k7c4	pro
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
podobou	podoba	k1gFnSc7	podoba
parníku	parník	k1gInSc2	parník
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
je	být	k5eAaImIp3nS	být
kolesový	kolesový	k2eAgInSc1d1	kolesový
parník	parník	k1gInSc1	parník
<g/>
.	.	kIx.	.
</s>
<s>
Šroubové	šroubový	k2eAgInPc1d1	šroubový
parníky	parník	k1gInPc1	parník
byly	být	k5eAaImAgInP	být
pak	pak	k6eAd1	pak
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
stavěny	stavit	k5eAaImNgInP	stavit
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
až	až	k6eAd1	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
parník	parník	k1gInSc1	parník
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
Robert	Robert	k1gMnSc1	Robert
Fulton	Fulton	k1gInSc4	Fulton
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Parníky	parník	k1gInPc1	parník
brázdily	brázdit	k5eAaImAgInP	brázdit
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
po	po	k7c6	po
Vltavě	Vltava	k1gFnSc6	Vltava
na	na	k7c4	na
Slapskou	slapský	k2eAgFnSc4d1	Slapská
přehradu	přehrada	k1gFnSc4	přehrada
nebo	nebo	k8xC	nebo
po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
od	od	k7c2	od
saských	saský	k2eAgInPc2d1	saský
Drážďan	Drážďany	k1gInPc2	Drážďany
přes	přes	k7c4	přes
Děčín	Děčín	k1gInSc4	Děčín
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc4	Litoměřice
a	a	k8xC	a
Mělník	Mělník	k1gInSc1	Mělník
a	a	k8xC	a
po	po	k7c6	po
Vltavě	Vltava	k1gFnSc6	Vltava
až	až	k9	až
po	po	k7c4	po
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
úsecích	úsek	k1gInPc6	úsek
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgInP	mít
původně	původně	k6eAd1	původně
i	i	k9	i
nemalý	malý	k2eNgInSc4d1	nemalý
dopravní	dopravní	k2eAgInSc4d1	dopravní
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
vedlo	vést	k5eAaImAgNnS	vést
méně	málo	k6eAd2	málo
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
paroplavebními	paroplavební	k2eAgInPc7d1	paroplavební
pokusy	pokus	k1gInPc7	pokus
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Božek	Božek	k1gMnSc1	Božek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
předvedl	předvést	k5eAaPmAgMnS	předvést
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
paroloď	paroloď	k1gFnSc4	paroloď
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
Stromovce	Stromovka	k1gFnSc6	Stromovka
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1817	[number]	k4	1817
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
praktickému	praktický	k2eAgNnSc3d1	praktické
využití	využití	k1gNnSc3	využití
parníků	parník	k1gInPc2	parník
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
desetiletí	desetiletí	k1gNnPc4	desetiletí
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Nemalou	malý	k2eNgFnSc7d1	nemalá
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispělo	přispět	k5eAaPmAgNnS	přispět
odstranění	odstranění	k1gNnSc1	odstranění
četných	četný	k2eAgFnPc2d1	četná
cel	cela	k1gFnPc2	cela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
značně	značně	k6eAd1	značně
komplikovala	komplikovat	k5eAaBmAgFnS	komplikovat
a	a	k8xC	a
zdržovala	zdržovat	k5eAaImAgFnS	zdržovat
plavbu	plavba	k1gFnSc4	plavba
po	po	k7c6	po
Labi	Labe	k1gNnSc6	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uvolnění	uvolnění	k1gNnSc1	uvolnění
plavby	plavba	k1gFnSc2	plavba
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
posléze	posléze	k6eAd1	posléze
využila	využít	k5eAaPmAgFnS	využít
skupina	skupina	k1gFnSc1	skupina
pražských	pražský	k2eAgMnPc2d1	pražský
podnikatelů	podnikatel	k1gMnPc2	podnikatel
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1822	[number]	k4	1822
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Pražská	pražský	k2eAgFnSc1d1	Pražská
plavební	plavební	k2eAgFnSc1d1	plavební
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovanou	přejmenovaný	k2eAgFnSc4d1	přejmenovaná
na	na	k7c4	na
Pražskou	pražský	k2eAgFnSc4d1	Pražská
společnost	společnost	k1gFnSc4	společnost
pro	pro	k7c4	pro
plavbu	plavba	k1gFnSc4	plavba
parní	parní	k2eAgFnPc1d1	parní
a	a	k8xC	a
plachetní	plachetní	k2eAgFnPc1d1	plachetní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Pražská	pražský	k2eAgFnSc1d1	Pražská
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
paroplavbu	paroplavba	k1gFnSc4	paroplavba
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
,	,	kIx,	,
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sehrál	sehrát	k5eAaPmAgMnS	sehrát
pozdější	pozdní	k2eAgMnSc1d2	pozdější
primátor	primátor	k1gMnSc1	primátor
František	František	k1gMnSc1	František
Dittrich	Dittrich	k1gMnSc1	Dittrich
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
parníkem	parník	k1gInSc7	parník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
udělení	udělení	k1gNnSc4	udělení
koncese	koncese	k1gFnSc2	koncese
císařem	císař	k1gMnSc7	císař
Františkem	František	k1gMnSc7	František
spuštěn	spustit	k5eAaPmNgInS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
parník	parník	k1gInSc1	parník
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Prag	Prag	k1gInSc1	Prag
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prusko-rakouské	pruskoakouský	k2eAgFnSc2d1	prusko-rakouská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
společnosti	společnost	k1gFnSc3	společnost
dařilo	dařit	k5eAaImAgNnS	dařit
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
flotila	flotila	k1gFnSc1	flotila
parníků	parník	k1gInPc2	parník
značně	značně	k6eAd1	značně
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
už	už	k6eAd1	už
čítala	čítat	k5eAaImAgFnS	čítat
8	[number]	k4	8
velkých	velký	k2eAgNnPc2d1	velké
a	a	k8xC	a
2	[number]	k4	2
menší	malý	k2eAgInPc4d2	menší
kolesové	kolesový	k2eAgInPc4d1	kolesový
parníky	parník	k1gInPc4	parník
<g/>
,	,	kIx,	,
13	[number]	k4	13
vrtulových	vrtulový	k2eAgInPc2d1	vrtulový
parníků	parník	k1gInPc2	parník
a	a	k8xC	a
23	[number]	k4	23
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
pohonem	pohon	k1gInSc7	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgInPc1d1	pražský
parníky	parník	k1gInPc1	parník
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
i	i	k9	i
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
humoristické	humoristický	k2eAgFnSc2d1	humoristická
literatury	literatura	k1gFnSc2	literatura
knihou	kniha	k1gFnSc7	kniha
Vzpoura	vzpoura	k1gFnSc1	vzpoura
na	na	k7c6	na
parníku	parník	k1gInSc6	parník
Primátor	primátor	k1gMnSc1	primátor
Dittrich	Dittrich	k1gInSc1	Dittrich
<g/>
,	,	kIx,	,
tentýž	týž	k3xTgInSc1	týž
parník	parník	k1gInSc1	parník
pak	pak	k6eAd1	pak
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
Hrdinný	hrdinný	k2eAgMnSc1d1	hrdinný
kapitán	kapitán	k1gMnSc1	kapitán
Korkorán	Korkorán	k2eAgMnSc1d1	Korkorán
<g/>
.	.	kIx.	.
</s>
<s>
Scénami	scéna	k1gFnPc7	scéna
na	na	k7c6	na
parníku	parník	k1gInSc6	parník
končí	končit	k5eAaImIp3nS	končit
i	i	k9	i
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
Jak	jak	k6eAd1	jak
utopit	utopit	k5eAaPmF	utopit
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mráčka	Mráček	k1gMnSc2	Mráček
aneb	aneb	k?	aneb
Konec	konec	k1gInSc1	konec
vodníků	vodník	k1gMnPc2	vodník
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
Kolesový	kolesový	k2eAgInSc1d1	kolesový
parník	parník	k1gInSc1	parník
Zaoceánský	zaoceánský	k2eAgInSc1d1	zaoceánský
parník	parník	k1gInSc1	parník
Říční	říční	k2eAgInSc1d1	říční
parník	parník	k1gInSc1	parník
Saská	saský	k2eAgFnSc1d1	saská
paroplavba	paroplavba	k1gFnSc1	paroplavba
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
parník	parník	k1gInSc1	parník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
parník	parník	k1gInSc1	parník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
