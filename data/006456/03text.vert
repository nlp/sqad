<s>
Bard	bard	k1gMnSc1	bard
(	(	kIx(	(
<g/>
z	z	k7c2	z
galského	galský	k2eAgMnSc2d1	galský
bardus	bardus	k1gInSc1	bardus
–	–	k?	–
básník	básník	k1gMnSc1	básník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
lidového	lidový	k2eAgMnSc2d1	lidový
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
který	který	k3yRgInSc1	který
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
především	především	k6eAd1	především
epické	epický	k2eAgNnSc1d1	epické
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
zpěvy	zpěv	k1gInPc1	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
folkem	folk	k1gInSc7	folk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
národními	národní	k2eAgFnPc7d1	národní
nebo	nebo	k8xC	nebo
etnickými	etnický	k2eAgFnPc7d1	etnická
variantami	varianta	k1gFnPc7	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nadřazen	nadřazen	k2eAgInSc1d1	nadřazen
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
celkový	celkový	k2eAgInSc1d1	celkový
projev	projev	k1gInSc1	projev
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
dotvářen	dotvářen	k2eAgInSc1d1	dotvářen
umělcem	umělec	k1gMnSc7	umělec
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
způsobem	způsob	k1gInSc7	způsob
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Merta	Merta	k1gMnSc1	Merta
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hutka	Hutka	k1gMnSc1	Hutka
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kočko	kočka	k1gFnSc5	kočka
Karel	Karel	k1gMnSc1	Karel
Kryl	Kryl	k1gMnSc1	Kryl
Petr	Petr	k1gMnSc1	Petr
Lutka	Lutka	k1gMnSc1	Lutka
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
Minstrel	minstrel	k1gMnSc1	minstrel
Trubadúr	trubadúr	k1gMnSc1	trubadúr
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bard	bard	k1gMnSc1	bard
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
