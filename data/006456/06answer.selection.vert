<s>
Bard	bard	k1gMnSc1	bard
(	(	kIx(	(
<g/>
z	z	k7c2	z
galského	galský	k2eAgMnSc2d1	galský
bardus	bardus	k1gInSc1	bardus
–	–	k?	–
básník	básník	k1gMnSc1	básník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
lidového	lidový	k2eAgMnSc2d1	lidový
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
který	který	k3yRgInSc1	který
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
především	především	k6eAd1	především
epické	epický	k2eAgNnSc1d1	epické
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc1d1	historická
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
básně	báseň	k1gFnPc1	báseň
a	a	k8xC	a
zpěvy	zpěv	k1gInPc1	zpěv
<g/>
.	.	kIx.	.
</s>
