<p>
<s>
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
Compton	Compton	k1gInSc1	Compton
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
rapper	rapper	k1gMnSc1	rapper
a	a	k8xC	a
třináctinásobný	třináctinásobný	k2eAgMnSc1d1	třináctinásobný
držitel	držitel	k1gMnSc1	držitel
ceny	cena	k1gFnSc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Black	Blacka	k1gFnPc2	Blacka
Hippy	Hippa	k1gFnSc2	Hippa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
rappeři	rapper	k1gMnPc1	rapper
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
jsou	být	k5eAaImIp3nP	být
Jay	Jay	k1gFnSc1	Jay
Rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
Schoolboy	Schoolbo	k2eAgFnPc4d1	Schoolbo
Q	Q	kA	Q
a	a	k8xC	a
Ab-Soul	Ab-Soul	k1gInSc1	Ab-Soul
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
rapper	rapper	k1gInSc1	rapper
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Pulitzerovou	Pulitzerová	k1gFnSc7	Pulitzerová
cenu	cena	k1gFnSc4	cena
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc6	rok
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgMnS	vydat
kritikami	kritika	k1gFnPc7	kritika
vynášené	vynášený	k2eAgNnSc4d1	vynášené
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
album	album	k1gNnSc4	album
Section	Section	k1gInSc1	Section
<g/>
.80	.80	k4	.80
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
smlouvu	smlouva	k1gFnSc4	smlouva
u	u	k7c2	u
labelu	label	k1gInSc2	label
Aftermath	Aftermath	k1gMnSc1	Aftermath
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
vytvořit	vytvořit	k5eAaPmF	vytvořit
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
good	good	k1gMnSc1	good
kid	kid	k?	kid
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
A.A.d	A.A.d	k1gMnSc1	A.A.d
city	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Compton	Compton	k1gInSc1	Compton
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
lokální	lokální	k2eAgFnSc4d1	lokální
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Centennial	Centennial	k1gInSc1	Centennial
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
příbuzné	příbuzná	k1gFnPc4	příbuzná
patří	patřit	k5eAaImIp3nS	patřit
známý	známý	k2eAgMnSc1d1	známý
basketbalista	basketbalista	k1gMnSc1	basketbalista
NBA	NBA	kA	NBA
Nick	Nick	k1gMnSc1	Nick
Young	Young	k1gMnSc1	Young
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
šestnáct	šestnáct	k4xCc1	šestnáct
<g/>
,	,	kIx,	,
nahrál	nahrát	k5eAaBmAgMnS	nahrát
první	první	k4xOgMnSc1	první
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
s	s	k7c7	s
názvem	název	k1gInSc7	název
Youngest	Youngest	k1gMnSc1	Youngest
Head	Head	k1gMnSc1	Head
Nigga	Nigg	k1gMnSc4	Nigg
In	In	k1gMnSc4	In
Charge	Charg	k1gMnSc4	Charg
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
používal	používat	k5eAaImAgInS	používat
pseudonym	pseudonym	k1gInSc1	pseudonym
K-Dot	K-Dot	k1gInSc1	K-Dot
<g/>
.	.	kIx.	.
</s>
<s>
Mixtape	Mixtapat	k5eAaPmIp3nS	Mixtapat
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
zájem	zájem	k1gInSc4	zájem
u	u	k7c2	u
tamního	tamní	k2eAgInSc2d1	tamní
nezávislého	závislý	k2eNgInSc2d1	nezávislý
labelu	label	k1gInSc2	label
Top	topit	k5eAaImRp2nS	topit
Dawg	Dawg	k1gMnSc1	Dawg
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
vedení	vedení	k1gNnSc1	vedení
mu	on	k3xPp3gMnSc3	on
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
vydal	vydat	k5eAaPmAgMnS	vydat
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
Training	Training	k1gInSc4	Training
Day	Day	k1gFnSc2	Day
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
ho	on	k3xPp3gNnSc4	on
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
tvorba	tvorba	k1gFnSc1	tvorba
rappera	rappera	k1gFnSc1	rappera
2	[number]	k4	2
<g/>
Pac	pac	k1gFnPc2	pac
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
čerpá	čerpat	k5eAaImIp3nS	čerpat
inspiraci	inspirace	k1gFnSc4	inspirace
od	od	k7c2	od
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
The	The	k1gMnSc1	The
Notorious	Notorious	k1gMnSc1	Notorious
B.I.G.	B.I.G.	k1gMnSc7	B.I.G.
<g/>
,	,	kIx,	,
Jay-Z	Jay-Z	k1gMnSc7	Jay-Z
<g/>
,	,	kIx,	,
Nas	Nas	k1gMnSc7	Nas
a	a	k8xC	a
Eminem	Emin	k1gMnSc7	Emin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
rapperem	rapper	k1gMnSc7	rapper
Game	game	k1gInSc4	game
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
nahráli	nahrát	k5eAaBmAgMnP	nahrát
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Cali	Cal	k1gFnSc2	Cal
Niggaz	Niggaz	k1gInSc1	Niggaz
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Cypha	Cypha	k1gMnSc1	Cypha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
Game	game	k1gInSc4	game
vybral	vybrat	k5eAaPmAgMnS	vybrat
jako	jako	k9	jako
předskonana	předskonan	k1gMnSc4	předskonan
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
i	i	k9	i
Jay	Jay	k1gMnSc1	Jay
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgInS	vydat
třetí	třetí	k4xOgFnSc4	třetí
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnPc1d1	používající
instrumentální	instrumentální	k2eAgFnPc1d1	instrumentální
nahrávky	nahrávka	k1gFnPc1	nahrávka
z	z	k7c2	z
alba	album	k1gNnSc2	album
Tha	Tha	k1gFnSc2	Tha
Carter	Carter	k1gMnSc1	Carter
III	III	kA	III
od	od	k7c2	od
Lil	lít	k5eAaImAgInS	lít
Wayne	Wayn	k1gMnSc5	Wayn
<g/>
,	,	kIx,	,
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
přestal	přestat	k5eAaPmAgInS	přestat
používat	používat	k5eAaImF	používat
pseudonym	pseudonym	k1gInSc1	pseudonym
K-Dot	K-Dota	k1gFnPc2	K-Dota
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
své	svůj	k3xOyFgNnSc4	svůj
rodné	rodný	k2eAgNnSc4d1	rodné
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
ještě	ještě	k6eAd1	ještě
vydal	vydat	k5eAaPmAgInS	vydat
nezávislé	závislý	k2eNgFnPc4d1	nezávislá
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kendrick	Kendrick	k1gInSc1	Kendrick
Lamar	Lamara	k1gFnPc2	Lamara
EP	EP	kA	EP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Overly	Overla	k1gMnSc2	Overla
Dedicated	Dedicated	k1gInSc1	Dedicated
a	a	k8xC	a
Section	Section	k1gInSc1	Section
<g/>
.80	.80	k4	.80
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
-	-	kIx~	-
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Independent	independent	k1gMnSc1	independent
Grind	Grind	k1gMnSc1	Grind
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
rappery	rapper	k1gInPc7	rapper
jako	jako	k8xC	jako
Tech	Tech	k?	Tech
N	N	kA	N
<g/>
9	[number]	k4	9
<g/>
ne	ne	k9	ne
a	a	k8xC	a
Jay	Jay	k1gMnSc1	Jay
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgMnS	vydat
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
Overly	Overl	k1gMnPc4	Overl
Dedicated	Dedicated	k1gInSc1	Dedicated
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Ignorance	ignorance	k1gFnSc1	ignorance
Is	Is	k1gMnSc1	Is
Bliss	Bliss	k1gInSc1	Bliss
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
oslavuje	oslavovat	k5eAaImIp3nS	oslavovat
Gangsta	Gangsta	k1gFnSc1	Gangsta
rap	rapa	k1gFnPc2	rapa
a	a	k8xC	a
pouliční	pouliční	k2eAgNnSc4d1	pouliční
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
byla	být	k5eAaImAgFnS	být
prvním	první	k4xOgInSc7	první
podnětem	podnět	k1gInSc7	podnět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
zájem	zájem	k1gInSc4	zájem
hudebního	hudební	k2eAgMnSc2d1	hudební
producenta	producent	k1gMnSc2	producent
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc2	on
poté	poté	k6eAd1	poté
pozval	pozvat	k5eAaPmAgMnS	pozvat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
na	na	k7c6	na
plánovaném	plánovaný	k2eAgNnSc6d1	plánované
albu	album	k1gNnSc6	album
Detox	Detox	k1gInSc1	Detox
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgInS	vydat
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
album	album	k1gNnSc4	album
Section	Section	k1gInSc1	Section
<g/>
.80	.80	k4	.80
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sklidilo	sklidit	k5eAaPmAgNnS	sklidit
velmi	velmi	k6eAd1	velmi
kladné	kladný	k2eAgFnSc2d1	kladná
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
k	k	k7c3	k
digitálnímu	digitální	k2eAgInSc3d1	digitální
prodeji	prodej	k1gInSc3	prodej
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
5	[number]	k4	5
300	[number]	k4	300
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
113	[number]	k4	113
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
žabříčku	žabříček	k1gInSc2	žabříček
Top	topit	k5eAaImRp2nS	topit
Heatseekers	Heatseekers	k1gInSc4	Heatseekers
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
bez	bez	k7c2	bez
mediální	mediální	k2eAgFnSc2d1	mediální
propagace	propagace	k1gFnSc2	propagace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mixtape	Mixtapat	k5eAaPmIp3nS	Mixtapat
mu	on	k3xPp3gMnSc3	on
vynesl	vynést	k5eAaPmAgInS	vynést
pozvání	pozvání	k1gNnSc4	pozvání
na	na	k7c6	na
spolupráce	spolupráce	k1gFnSc2	spolupráce
mainstreamových	mainstreamový	k2eAgMnPc2d1	mainstreamový
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xC	jako
Game	game	k1gInSc1	game
<g/>
,	,	kIx,	,
Meek	Meek	k1gMnSc1	Meek
Mill	Mill	k1gMnSc1	Mill
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Khaled	Khaled	k1gInSc1	Khaled
nebo	nebo	k8xC	nebo
Drake	Drake	k1gInSc1	Drake
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
společně	společně	k6eAd1	společně
s	s	k7c7	s
legendami	legenda	k1gFnPc7	legenda
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
<g/>
,	,	kIx,	,
Snoop	Snoop	k1gInSc1	Snoop
Dogg	Dogg	k1gInSc1	Dogg
a	a	k8xC	a
Game	game	k1gInSc1	game
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
posléze	posléze	k6eAd1	posléze
"	"	kIx"	"
<g/>
korunovali	korunovat	k5eAaBmAgMnP	korunovat
<g/>
"	"	kIx"	"
novým	nový	k2eAgMnSc7d1	nový
králem	král	k1gMnSc7	král
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
good	good	k1gInSc1	good
kid	kid	k?	kid
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
A.A.d	A.A.d	k1gInSc1	A.A.d
city	city	k1gFnSc1	city
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
-	-	kIx~	-
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
podepsal	podepsat	k5eAaPmAgMnS	podepsat
label	label	k1gMnSc1	label
Top	topit	k5eAaImRp2nS	topit
Dawg	Dawg	k1gMnSc1	Dawg
joint	joint	k1gMnSc1	joint
venture	ventur	k1gMnSc5	ventur
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
nahrávacími	nahrávací	k2eAgFnPc7d1	nahrávací
společnostmi	společnost	k1gFnPc7	společnost
Aftermath	Aftermatha	k1gFnPc2	Aftermatha
a	a	k8xC	a
Interscope	Interscop	k1gMnSc5	Interscop
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
stal	stát	k5eAaPmAgMnS	stát
mainstreamovým	mainstreamový	k2eAgMnSc7d1	mainstreamový
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
umělcem	umělec	k1gMnSc7	umělec
z	z	k7c2	z
Top	topit	k5eAaImRp2nS	topit
Dawg	Dawg	k1gMnSc1	Dawg
Ent	Ent	k1gMnSc1	Ent
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
smlouvu	smlouva	k1gFnSc4	smlouva
u	u	k7c2	u
Aftermath	Aftermatha	k1gFnPc2	Aftermatha
Ent	Ent	k1gFnSc2	Ent
<g/>
.	.	kIx.	.
s	s	k7c7	s
distribucí	distribuce	k1gFnSc7	distribuce
u	u	k7c2	u
Interscope	Interscop	k1gInSc5	Interscop
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
umělci	umělec	k1gMnPc1	umělec
z	z	k7c2	z
labelu	label	k1gInSc2	label
Top	topit	k5eAaImRp2nS	topit
Dawg	Dawg	k1gInSc4	Dawg
mají	mít	k5eAaImIp3nP	mít
zajištěnou	zajištěný	k2eAgFnSc4d1	zajištěná
distribuci	distribuce	k1gFnSc4	distribuce
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
Interscope	Interscop	k1gInSc5	Interscop
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
byl	být	k5eAaImAgMnS	být
zveřejněn	zveřejněn	k2eAgMnSc1d1	zveřejněn
první	první	k4xOgMnSc1	první
major	major	k1gMnSc1	major
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Recipe	recipe	k1gNnSc2	recipe
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
38	[number]	k4	38
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Hot	hot	k0	hot
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
<g/>
/	/	kIx~	/
<g/>
Hip-Hop	Hip-Hop	k1gInSc1	Hip-Hop
Songs	Songsa	k1gFnPc2	Songsa
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
jako	jako	k9	jako
bonus	bonus	k1gInSc1	bonus
track	tracka	k1gFnPc2	tracka
na	na	k7c4	na
deluxe	deluxe	k1gFnPc4	deluxe
verzi	verze	k1gFnSc4	verze
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
době	doba	k1gFnSc6	doba
oznámil	oznámit	k5eAaPmAgMnS	oznámit
plánované	plánovaný	k2eAgNnSc4d1	plánované
vydání	vydání	k1gNnSc4	vydání
svého	svůj	k3xOyFgNnSc2	svůj
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
good	good	k1gMnSc1	good
kid	kid	k?	kid
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
A.A.d	A.A.d	k1gInSc4	A.A.d
city	city	k1gFnSc2	city
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
prodeje	prodej	k1gInSc2	prodej
v	v	k7c6	v
USA	USA	kA	USA
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
s	s	k7c7	s
241	[number]	k4	241
000	[number]	k4	000
prodanými	prodaný	k2eAgInPc7d1	prodaný
kusy	kus	k1gInPc7	kus
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
oficiálním	oficiální	k2eAgInSc7d1	oficiální
singlem	singl	k1gInSc7	singl
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Swimming	Swimming	k1gInSc1	Swimming
Pools	Pools	k1gInSc1	Pools
(	(	kIx(	(
<g/>
Drank	Drank	k1gInSc1	Drank
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
a	a	k8xC	a
na	na	k7c4	na
63	[number]	k4	63
<g/>
.	.	kIx.	.
v	v	k7c6	v
UK	UK	kA	UK
Singles	Singles	k1gInSc4	Singles
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
v	v	k7c6	v
USA	USA	kA	USA
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
certifikaci	certifikace	k1gFnSc4	certifikace
zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
500	[number]	k4	500
000	[number]	k4	000
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
oceněno	oceněn	k2eAgNnSc1d1	oceněno
certifikací	certifikace	k1gFnSc7	certifikace
platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
1	[number]	k4	1
324	[number]	k4	324
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Swimming	Swimming	k1gInSc1	Swimming
Pools	Pools	k1gInSc1	Pools
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Poetic	Poetice	k1gFnPc2	Poetice
Justice	justice	k1gFnSc2	justice
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgFnP	být
asociací	asociace	k1gFnSc7	asociace
RIAA	RIAA	kA	RIAA
oceněny	oceněn	k2eAgInPc1d1	oceněn
certifikací	certifikace	k1gFnPc2	certifikace
zlatý	zlatý	k2eAgInSc4d1	zlatý
singl	singl	k1gInSc4	singl
za	za	k7c4	za
překročení	překročení	k1gNnSc4	překročení
prodeje	prodej	k1gInSc2	prodej
500	[number]	k4	500
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
nahrál	nahrát	k5eAaPmAgMnS	nahrát
sloku	sloka	k1gFnSc4	sloka
pro	pro	k7c4	pro
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Control	Control	k1gInSc4	Control
<g/>
"	"	kIx"	"
od	od	k7c2	od
Big	Big	k1gFnSc2	Big
Seana	Sean	k1gInSc2	Sean
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c2	za
krále	král	k1gMnSc2	král
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
pohodlí	pohodlí	k1gNnSc4	pohodlí
rapperů	rapper	k1gMnPc2	rapper
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
odmítají	odmítat	k5eAaImIp3nP	odmítat
pustit	pustit	k5eAaPmF	pustit
do	do	k7c2	do
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
připomněl	připomnět	k5eAaPmAgMnS	připomnět
"	"	kIx"	"
<g/>
Beef	Beef	k1gInSc4	Beef
<g/>
"	"	kIx"	"
kulturu	kultura	k1gFnSc4	kultura
hip-hopu	hipop	k1gInSc2	hip-hop
z	z	k7c2	z
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
prvních	první	k4xOgNnPc2	první
let	léto	k1gNnPc2	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
interview	interview	k1gNnSc6	interview
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
text	text	k1gInSc1	text
nebyl	být	k5eNaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
s	s	k7c7	s
nenávistí	nenávist	k1gFnSc7	nenávist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c7	za
cílem	cíl	k1gInSc7	cíl
vyvolat	vyvolat	k5eAaPmF	vyvolat
zdravou	zdravý	k2eAgFnSc4d1	zdravá
konkurenci	konkurence	k1gFnSc4	konkurence
mezi	mezi	k7c7	mezi
rappery	rapper	k1gInPc7	rapper
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
prohlášení	prohlášení	k1gNnSc6	prohlášení
následně	následně	k6eAd1	následně
reagovala	reagovat	k5eAaBmAgFnS	reagovat
řada	řada	k1gFnSc1	řada
rapperů	rapper	k1gMnPc2	rapper
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
rozhovorech	rozhovor	k1gInPc6	rozhovor
či	či	k8xC	či
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
nahráli	nahrát	k5eAaBmAgMnP	nahrát
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
zpochybnili	zpochybnit	k5eAaPmAgMnP	zpochybnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
kdy	kdy	k6eAd1	kdy
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
králem	král	k1gMnSc7	král
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Joell	Joell	k1gMnSc1	Joell
Ortiz	Ortiz	k1gMnSc1	Ortiz
-	-	kIx~	-
"	"	kIx"	"
<g/>
Outta	Outta	k1gFnSc1	Outta
Control	Controla	k1gFnPc2	Controla
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
B.o.B	B.o.B	k1gMnSc1	B.o.B
-	-	kIx~	-
"	"	kIx"	"
<g/>
How	How	k1gFnPc2	How
2	[number]	k4	2
Rap	rapa	k1gFnPc2	rapa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Lupe	lupat	k5eAaImIp3nS	lupat
Fiasco	Fiasco	k6eAd1	Fiasco
-	-	kIx~	-
"	"	kIx"	"
<g/>
SLR	SLR	kA	SLR
2	[number]	k4	2
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
Papoose	Papoosa	k1gFnSc6	Papoosa
-	-	kIx~	-
"	"	kIx"	"
<g/>
Control	Control	k1gInSc1	Control
Freestyle	Freestyl	k1gInSc5	Freestyl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
56	[number]	k4	56
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
obdržel	obdržet	k5eAaPmAgMnS	obdržet
sedm	sedm	k4xCc4	sedm
nominací	nominace	k1gFnPc2	nominace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žádnou	žádný	k3yNgFnSc4	žádný
neproměnil	proměnit	k5eNaPmAgMnS	proměnit
<g/>
.	.	kIx.	.
</s>
<s>
Rapper	Rapprat	k5eAaPmRp2nS	Rapprat
Macklemore	Macklemor	k1gMnSc5	Macklemor
<g/>
,	,	kIx,	,
výherce	výherce	k1gMnSc2	výherce
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
rapové	rapový	k2eAgNnSc4d1	rapové
album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
po	po	k7c6	po
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
ceremonii	ceremonie	k1gFnSc6	ceremonie
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
více	hodně	k6eAd2	hodně
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
albem	album	k1gNnSc7	album
good	good	k6eAd1	good
kid	kid	k?	kid
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
A.A.d.	A.A.d.	k1gFnSc1	A.A.d.
city	city	k1gFnSc1	city
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
To	to	k9	to
Pimp	Pimp	k1gInSc1	Pimp
a	a	k8xC	a
Butterfly	butterfly	k1gInSc1	butterfly
a	a	k8xC	a
untitled	untitled	k1gMnSc1	untitled
unmastered	unmastered	k1gMnSc1	unmastered
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
-	-	kIx~	-
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
třetím	třetí	k4xOgNnSc6	třetí
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
nahrál	nahrát	k5eAaBmAgMnS	nahrát
okolo	okolo	k7c2	okolo
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
například	například	k6eAd1	například
s	s	k7c7	s
producenty	producent	k1gMnPc7	producent
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dre	Dre	k1gMnSc1	Dre
<g/>
,	,	kIx,	,
Tae	Tae	k1gMnSc1	Tae
Beast	Beast	k1gMnSc1	Beast
a	a	k8xC	a
Sounwave	Sounwav	k1gInSc5	Sounwav
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
nemyslel	myslet	k5eNaImAgInS	myslet
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
toho	ten	k3xDgNnSc2	ten
předchozího	předchozí	k2eAgNnSc2d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chystá	chystat	k5eAaImIp3nS	chystat
premiéru	premiéra	k1gFnSc4	premiéra
svého	svůj	k3xOyFgInSc2	svůj
krátkometrážního	krátkometrážní	k2eAgInSc2d1	krátkometrážní
filmu	film	k1gInSc2	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
m.	m.	k?	m.
<g/>
A.A.d.	A.A.d.	k1gFnSc2	A.A.d.
Inspirací	inspirace	k1gFnPc2	inspirace
snímku	snímek	k1gInSc2	snímek
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
album	album	k1gNnSc1	album
good	good	k1gMnSc1	good
kid	kid	k?	kid
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
A.A.d	A.A.d	k1gInSc4	A.A.d
city	city	k1gFnSc2	city
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Sundance	Sundance	k1gFnSc2	Sundance
NEXT	NEXT	kA	NEXT
Fest	fest	k6eAd1	fest
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
2014	[number]	k4	2014
nahrál	nahrát	k5eAaPmAgMnS	nahrát
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Flying	Flying	k1gInSc4	Flying
Lotus	Lotus	kA	Lotus
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
spoluprací	spolupráce	k1gFnPc2	spolupráce
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Never	Never	k1gMnSc1	Never
Catch	Catch	k1gMnSc1	Catch
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgNnP	být
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xS	jako
singl	singl	k1gInSc1	singl
k	k	k7c3	k
producentovu	producentův	k2eAgNnSc3d1	producentovo
albu	album	k1gNnSc3	album
You	You	k1gMnPc1	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Dead	Dead	k1gInSc1	Dead
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
nebudou	být	k5eNaImBp3nP	být
žádní	žádný	k3yNgMnPc1	žádný
přizvaní	přizvaný	k2eAgMnPc1d1	přizvaný
umělci	umělec	k1gMnPc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
album	album	k1gNnSc1	album
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
vydal	vydat	k5eAaPmAgMnS	vydat
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
39	[number]	k4	39
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
amerického	americký	k2eAgInSc2d1	americký
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
i	i	k9	i
<g/>
"	"	kIx"	"
oceněna	oceněn	k2eAgFnSc1d1	oceněna
dvěma	dva	k4xCgFnPc7	dva
cenami	cena	k1gFnPc7	cena
Grammy	Gramma	k1gFnPc4	Gramma
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
rapovou	rapový	k2eAgFnSc4d1	rapová
píseň	píseň	k1gFnSc4	píseň
a	a	k8xC	a
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
rapový	rapový	k2eAgInSc4d1	rapový
počin	počin	k1gInSc4	počin
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vydal	vydat	k5eAaPmAgInS	vydat
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Blacker	Blacker	k1gMnSc1	Blacker
the	the	k?	the
Berry	Berra	k1gFnSc2	Berra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
US	US	kA	US
žebříčku	žebříček	k1gInSc6	žebříček
nejdříve	dříve	k6eAd3	dříve
neumístil	umístit	k5eNaPmAgMnS	umístit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
66	[number]	k4	66
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
vydáno	vydat	k5eAaPmNgNnS	vydat
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
přes	přes	k7c4	přes
Twitter	Twitter	k1gInSc4	Twitter
oznámil	oznámit	k5eAaPmAgInS	oznámit
název	název	k1gInSc1	název
alba	album	k1gNnSc2	album
To	to	k9	to
Pimp	Pimp	k1gInSc4	Pimp
a	a	k8xC	a
Butterfly	butterfly	k1gInSc4	butterfly
a	a	k8xC	a
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
překvapivě	překvapivě	k6eAd1	překvapivě
vydáno	vydat	k5eAaPmNgNnS	vydat
již	již	k6eAd1	již
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
týden	týden	k1gInSc1	týden
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
324	[number]	k4	324
403	[number]	k4	403
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
tím	ten	k3xDgNnSc7	ten
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
příčkách	příčka	k1gFnPc6	příčka
US	US	kA	US
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
39	[number]	k4	39
milionů	milion	k4xCgInPc2	milion
streamů	stream	k1gInPc2	stream
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
umístily	umístit	k5eAaPmAgFnP	umístit
i	i	k9	i
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
Alright	Alright	k1gInSc1	Alright
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
82	[number]	k4	82
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Wesley	Weslea	k1gFnPc1	Weslea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Theory	Theor	k1gInPc7	Theor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
91	[number]	k4	91
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
These	these	k1gFnSc1	these
Walls	Wallsa	k1gFnPc2	Wallsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
94	[number]	k4	94
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Institutionalized	Institutionalized	k1gInSc1	Institutionalized
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
singlem	singl	k1gInSc7	singl
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
King	King	k1gMnSc1	King
Kunta	Kunta	k?	Kunta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
58	[number]	k4	58
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
certifikováno	certifikovat	k5eAaImNgNnS	certifikovat
společností	společnost	k1gFnPc2	společnost
RIAA	RIAA	kA	RIAA
jako	jako	k8xS	jako
platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
ks	ks	kA	ks
v	v	k7c6	v
distribuci	distribuce	k1gFnSc6	distribuce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
pravidel	pravidlo	k1gNnPc2	pravidlo
RIAA	RIAA	kA	RIAA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
přičítat	přičítat	k5eAaImF	přičítat
i	i	k9	i
streamování	streamování	k1gNnSc4	streamování
audio	audio	k2eAgNnSc4d1	audio
a	a	k8xC	a
video	video	k1gNnSc4	video
obsahu	obsah	k1gInSc2	obsah
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
850	[number]	k4	850
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
osmi	osm	k4xCc6	osm
městech	město	k1gNnPc6	město
USA	USA	kA	USA
turné	turné	k1gNnSc3	turné
1	[number]	k4	1
<g/>
st	st	kA	st
Annual	Annual	k1gMnSc1	Annual
Kunta	Kunta	k?	Kunta
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Groove	Groov	k1gInSc5	Groov
Sessions	Sessions	k1gInSc1	Sessions
<g/>
.	.	kIx.	.
</s>
<s>
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
To	to	k9	to
Pimp	Pimp	k1gInSc4	Pimp
a	a	k8xC	a
Butterfly	butterfly	k1gInSc4	butterfly
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
turné	turné	k1gNnSc4	turné
ho	on	k3xPp3gInSc2	on
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
jeho	jeho	k3xOp3gFnSc1	jeho
podpůrná	podpůrný	k2eAgFnSc1d1	podpůrná
kapela	kapela	k1gFnSc1	kapela
The	The	k1gFnSc2	The
Wesley	Weslea	k1gFnSc2	Weslea
Theory	Theora	k1gFnSc2	Theora
a	a	k8xC	a
rapper	rapper	k1gMnSc1	rapper
Jay	Jay	k1gFnSc2	Jay
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
symfonickým	symfonický	k2eAgInSc7d1	symfonický
orchestrem	orchestr	k1gInSc7	orchestr
v	v	k7c6	v
prestižním	prestižní	k2eAgInSc6d1	prestižní
Kennedyho	Kennedy	k1gMnSc2	Kennedy
Centru	centr	k1gInSc2	centr
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
vyprodaný	vyprodaný	k2eAgInSc1d1	vyprodaný
během	během	k7c2	během
několika	několik	k4yIc3	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušky	Fanoušek	k1gMnPc4	Fanoušek
pobouřilo	pobouřit	k5eAaPmAgNnS	pobouřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přednost	přednost	k1gFnSc4	přednost
dostali	dostat	k5eAaPmAgMnP	dostat
členové	člen	k1gMnPc1	člen
Kennedyho	Kennedy	k1gMnSc2	Kennedy
Centra	centr	k1gMnSc2	centr
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
lístky	lístek	k1gInPc4	lístek
vykoupili	vykoupit	k5eAaPmAgMnP	vykoupit
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
58	[number]	k4	58
<g/>
.	.	kIx.	.
předávání	předávání	k1gNnSc1	předávání
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
(	(	kIx(	(
<g/>
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgMnS	obdržet
11	[number]	k4	11
nominací	nominace	k1gFnPc2	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
proměnil	proměnit	k5eAaPmAgMnS	proměnit
pět	pět	k4xCc1	pět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
rapové	rapový	k2eAgNnSc4d1	rapové
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
Pimp	Pimp	k1gInSc4	Pimp
a	a	k8xC	a
Butterfly	butterfly	k1gInSc4	butterfly
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
za	za	k7c2	za
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Alright	Alright	k1gInSc1	Alright
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
za	za	k7c2	za
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
These	these	k1gFnSc1	these
Walls	Wallsa	k1gFnPc2	Wallsa
<g/>
"	"	kIx"	"
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
videoklip	videoklip	k1gInSc4	videoklip
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bad	Bad	k1gFnSc1	Bad
Blood	Blooda	k1gFnPc2	Blooda
<g/>
"	"	kIx"	"
–	–	k?	–
Taylor	Taylor	k1gMnSc1	Taylor
Swift	Swift	k1gMnSc1	Swift
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
mu	on	k3xPp3gNnSc3	on
starostka	starostka	k1gFnSc1	starostka
Aja	Aja	k1gFnSc2	Aja
Brown	Brown	k1gInSc1	Brown
předala	předat	k5eAaPmAgFnS	předat
symbolický	symbolický	k2eAgInSc4d1	symbolický
klíč	klíč	k1gInSc4	klíč
od	od	k7c2	od
města	město	k1gNnSc2	město
Compton	Compton	k1gInSc4	Compton
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
bez	bez	k7c2	bez
předchozího	předchozí	k2eAgNnSc2d1	předchozí
oznámení	oznámení	k1gNnSc2	oznámení
či	či	k8xC	či
propagace	propagace	k1gFnSc2	propagace
vydal	vydat	k5eAaPmAgInS	vydat
kompilační	kompilační	k2eAgInSc1d1	kompilační
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
untitled	untitled	k1gMnSc1	untitled
unmastered	unmastered	k1gMnSc1	unmastered
<g/>
..	..	k?	..
EP	EP	kA	EP
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
osm	osm	k4xCc1	osm
"	"	kIx"	"
<g/>
untitled	untitled	k1gMnSc1	untitled
<g/>
"	"	kIx"	"
písní	píseň	k1gFnSc7	píseň
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
34	[number]	k4	34
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
písněmi	píseň	k1gFnPc7	píseň
z	z	k7c2	z
EP	EP	kA	EP
v	v	k7c6	v
TV	TV	kA	TV
pořadech	pořad	k1gInPc6	pořad
The	The	k1gFnSc1	The
Colbert	Colbert	k1gInSc1	Colbert
Report	report	k1gInSc1	report
a	a	k8xC	a
The	The	k1gFnSc1	The
Tonight	Tonight	k1gInSc4	Tonight
Show	show	k1gFnSc2	show
starring	starring	k1gInSc4	starring
Jimmy	Jimma	k1gFnSc2	Jimma
Fallon	Fallon	k1gInSc1	Fallon
<g/>
.	.	kIx.	.
</s>
<s>
EP	EP	kA	EP
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
hudebních	hudební	k2eAgFnPc6d1	hudební
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
DAMN	DAMN	kA	DAMN
<g/>
.	.	kIx.	.
a	a	k8xC	a
Black	Black	k1gInSc1	Black
Panther	Panthra	k1gFnPc2	Panthra
OST	OST	kA	OST
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
-	-	kIx~	-
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
vydal	vydat	k5eAaPmAgInS	vydat
propagační	propagační	k2eAgInSc1d1	propagační
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Heart	Heart	k1gInSc4	Heart
Part	part	k1gInSc1	part
4	[number]	k4	4
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
22	[number]	k4	22
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgInS	vydat
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Humble	Humble	k1gFnSc1	Humble
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
stylizováno	stylizován	k2eAgNnSc1d1	stylizováno
jako	jako	k9	jako
"	"	kIx"	"
<g/>
HUMBLE	HUMBLE	kA	HUMBLE
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
dosud	dosud	k6eAd1	dosud
jeho	jeho	k3xOp3gInSc1	jeho
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
singl	singl	k1gInSc1	singl
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
debut	debut	k1gInSc4	debut
rapové	rapový	k2eAgFnSc2d1	rapová
písně	píseň	k1gFnSc2	píseň
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
si	se	k3xPyFc3	se
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
vydání	vydání	k1gNnSc2	vydání
koupilo	koupit	k5eAaPmAgNnS	koupit
111	[number]	k4	111
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
49,8	[number]	k4	49,8
<g/>
milionkrát	milionkrát	k6eAd1	milionkrát
streamována	streamovat	k5eAaImNgFnS	streamovat
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
singlem	singl	k1gInSc7	singl
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
vydán	vydat	k5eAaPmNgInS	vydat
i	i	k9	i
videoklip	videoklip	k1gInSc1	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
píseň	píseň	k1gFnSc1	píseň
vyhoupla	vyhoupnout	k5eAaPmAgFnS	vyhoupnout
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
,	,	kIx,	,
jako	jako	k9	jako
Lamarův	Lamarův	k2eAgMnSc1d1	Lamarův
první	první	k4xOgInSc1	první
number-one	numbern	k1gInSc5	number-on
hit	hit	k1gInSc4	hit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
profilu	profil	k1gInSc6	profil
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
obal	obal	k1gInSc1	obal
a	a	k8xC	a
seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
svého	své	k1gNnSc2	své
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Damn	Damn	k1gInSc1	Damn
(	(	kIx(	(
<g/>
stylizováno	stylizován	k2eAgNnSc1d1	stylizováno
jako	jako	k8xS	jako
DAMN	DAMN	kA	DAMN
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
hostuje	hostovat	k5eAaImIp3nS	hostovat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Rihanna	Rihanna	k1gFnSc1	Rihanna
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
názvy	název	k1gInPc1	název
písní	píseň	k1gFnPc2	píseň
jsou	být	k5eAaImIp3nP	být
jednoslovné	jednoslovný	k2eAgFnPc1d1	jednoslovná
<g/>
,	,	kIx,	,
psané	psaný	k2eAgFnPc1d1	psaná
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
a	a	k8xC	a
s	s	k7c7	s
tečkou	tečka	k1gFnSc7	tečka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
týden	týden	k1gInSc1	týden
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
600	[number]	k4	600
tisíc	tisíc	k4xCgInSc1	tisíc
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
(	(	kIx(	(
<g/>
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
přímý	přímý	k2eAgInSc1d1	přímý
prodej	prodej	k1gInSc1	prodej
a	a	k8xC	a
streamy	stream	k1gInPc1	stream
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
již	již	k6eAd1	již
Lamarovo	Lamarův	k2eAgNnSc1d1	Lamarův
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
druhý	druhý	k4xOgInSc4	druhý
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
dalších	další	k2eAgInPc2d1	další
230	[number]	k4	230
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
písní	píseň	k1gFnPc2	píseň
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
dostalo	dostat	k5eAaPmAgNnS	dostat
všech	všecek	k3xTgFnPc2	všecek
čtrnáct	čtrnáct	k4xCc4	čtrnáct
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnPc1d3	nejúspěšnější
byly	být	k5eAaImAgFnP	být
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
DNA	DNA	kA	DNA
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Loyalty	Loyalt	k1gInPc4	Loyalt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Rihanna	Rihanna	k1gFnSc1	Rihanna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Element	element	k1gInSc1	element
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Zacari	Zacari	k1gNnSc1	Zacari
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
tak	tak	k9	tak
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
popovějším	popový	k2eAgInSc7d2	popovější
zvukem	zvuk	k1gInSc7	zvuk
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
(	(	kIx(	(
<g/>
95	[number]	k4	95
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
100	[number]	k4	100
na	na	k7c6	na
serveru	server	k1gInSc6	server
Metacritic	Metacritice	k1gFnPc2	Metacritice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
u	u	k7c2	u
komerčního	komerční	k2eAgNnSc2d1	komerční
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
album	album	k1gNnSc4	album
v	v	k7c6	v
USA	USA	kA	USA
získalo	získat	k5eAaPmAgNnS	získat
certifikaci	certifikace	k1gFnSc4	certifikace
platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
prvního	první	k4xOgNnSc2	první
pololetí	pololetí	k1gNnSc2	pololetí
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
prodalo	prodat	k5eAaPmAgNnS	prodat
1	[number]	k4	1
772	[number]	k4	772
000	[number]	k4	000
ks	ks	kA	ks
(	(	kIx(	(
<g/>
i	i	k9	i
se	s	k7c7	s
streamy	stream	k1gInPc7	stream
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
album	album	k1gNnSc1	album
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
certifikaci	certifikace	k1gFnSc4	certifikace
2	[number]	k4	2
<g/>
x	x	k?	x
platinová	platinový	k2eAgFnSc1d1	platinová
deska	deska	k1gFnSc1	deska
za	za	k7c4	za
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
v	v	k7c6	v
distribuci	distribuce	k1gFnSc6	distribuce
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
streamů	stream	k1gInPc2	stream
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
2	[number]	k4	2
747	[number]	k4	747
000	[number]	k4	000
ks	ks	kA	ks
po	po	k7c6	po
započítání	započítání	k1gNnSc6	započítání
streamů	stream	k1gInPc2	stream
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
930	[number]	k4	930
000	[number]	k4	000
ks	ks	kA	ks
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
prodeji	prodej	k1gInSc6	prodej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
druhým	druhý	k4xOgNnSc7	druhý
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
dalších	další	k2eAgInPc2d1	další
830	[number]	k4	830
000	[number]	k4	000
ks	ks	kA	ks
(	(	kIx(	(
<g/>
po	po	k7c6	po
započítání	započítání	k1gNnSc6	započítání
streamů	stream	k1gInPc2	stream
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Obsah	obsah	k1gInSc4	obsah
alba	album	k1gNnSc2	album
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
cen	cena	k1gFnPc2	cena
grammy	gramma	k1gFnSc2	gramma
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nahrávky	nahrávka	k1gFnSc2	nahrávka
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Humble	Humble	k1gFnSc4	Humble
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
alba	album	k1gNnSc2	album
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
DAMN	DAMN	kA	DAMN
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
předávání	předávání	k1gNnSc6	předávání
cen	cena	k1gFnPc2	cena
celkem	celkem	k6eAd1	celkem
obdržel	obdržet	k5eAaPmAgMnS	obdržet
pět	pět	k4xCc4	pět
cen	cena	k1gFnPc2	cena
<g/>
:	:	kIx,	:
tři	tři	k4xCgMnPc1	tři
za	za	k7c2	za
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Humble	Humble	k1gFnSc1	Humble
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rapový	rapový	k2eAgInSc1d1	rapový
počin	počin	k1gInSc1	počin
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
rapová	rapový	k2eAgFnSc1d1	rapová
píseň	píseň	k1gFnSc1	píseň
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
videoklip	videoklip	k1gInSc1	videoklip
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
za	za	k7c2	za
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Loyalty	Loyalt	k1gInPc1	Loyalt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Rihanna	Rihanna	k1gFnSc1	Rihanna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rap	rap	k1gMnSc1	rap
<g/>
/	/	kIx~	/
<g/>
zpěv	zpěv	k1gInSc1	zpěv
spolupráce	spolupráce	k1gFnSc2	spolupráce
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
za	za	k7c4	za
album	album	k1gNnSc4	album
DAMN	DAMN	kA	DAMN
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rapové	rapový	k2eAgNnSc1d1	rapové
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lamar	Lamar	k1gInSc1	Lamar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
obdržel	obdržet	k5eAaPmAgMnS	obdržet
za	za	k7c4	za
album	album	k1gNnSc4	album
DAMN	DAMN	kA	DAMN
<g/>
.	.	kIx.	.
</s>
<s>
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgInS	vybrat
režisérem	režisér	k1gMnSc7	režisér
Ryanem	Ryan	k1gMnSc7	Ryan
Cooglerem	Coogler	k1gMnSc7	Coogler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
umělci	umělec	k1gMnPc7	umělec
z	z	k7c2	z
labelu	label	k1gInSc2	label
Top	topit	k5eAaImRp2nS	topit
Dawg	Dawg	k1gMnSc1	Dawg
Entertainment	Entertainment	k1gMnSc1	Entertainment
produkoval	produkovat	k5eAaImAgMnS	produkovat
a	a	k8xC	a
nahrál	nahrát	k5eAaPmAgMnS	nahrát
soundtrack	soundtrack	k1gInSc4	soundtrack
ke	k	k7c3	k
komiksovému	komiksový	k2eAgInSc3d1	komiksový
filmu	film	k1gInSc2	film
Black	Blacka	k1gFnPc2	Blacka
Panther	Panthra	k1gFnPc2	Panthra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byly	být	k5eAaImAgInP	být
vydány	vydán	k2eAgInPc1d1	vydán
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
the	the	k?	the
Stars	Stars	k1gInSc1	Stars
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
se	s	k7c7	s
SZA	SZA	kA	SZA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dead	Deada	k1gFnPc2	Deada
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
Jay	Jay	k1gFnSc7	Jay
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
Future	Futur	k1gMnSc5	Futur
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pray	Praa	k1gFnPc4	Praa
For	forum	k1gNnPc2	forum
Me	Me	k1gFnPc2	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
The	The	k1gFnSc7	The
Weeknd	Weeknd	k1gMnSc1	Weeknd
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
61	[number]	k4	61
<g/>
.	.	kIx.	.
předávání	předávání	k1gNnSc1	předávání
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dead	Deada	k1gFnPc2	Deada
<g/>
"	"	kIx"	"
vybrána	vybrat	k5eAaPmNgFnS	vybrat
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
rapový	rapový	k2eAgInSc4d1	rapový
počin	počin	k1gInSc4	počin
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
Section	Section	k1gInSc1	Section
<g/>
.80	.80	k4	.80
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
good	good	k1gMnSc1	good
kid	kid	k?	kid
<g/>
,	,	kIx,	,
m.	m.	k?	m.
<g/>
A.A.d	A.A.d	k1gInSc1	A.A.d
city	city	k1gFnSc1	city
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
Pimp	Pimp	k1gMnSc1	Pimp
a	a	k8xC	a
Butterfly	butterfly	k1gInSc1	butterfly
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Damn	Damn	k1gNnSc1	Damn
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
EP	EP	kA	EP
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
untitled	untitled	k1gMnSc1	untitled
unmastered	unmastered	k1gMnSc1	unmastered
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Mixtapy	Mixtap	k1gInPc4	Mixtap
===	===	k?	===
</s>
</p>
<p>
<s>
Youngest	Youngest	k1gFnSc1	Youngest
Head	Heada	k1gFnPc2	Heada
Nigga	Nigg	k1gMnSc2	Nigg
in	in	k?	in
Charge	Charg	k1gMnSc2	Charg
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Training	Training	k1gInSc1	Training
Day	Day	k1gFnSc2	Day
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
No	no	k9	no
Sleep	Sleep	k1gInSc1	Sleep
'	'	kIx"	'
<g/>
Til	til	k1gInSc1	til
NYC	NYC	kA	NYC
(	(	kIx(	(
<g/>
s	s	k7c7	s
Jay	Jay	k1gFnSc7	Jay
Rock	rock	k1gInSc1	rock
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
C4	C4	k4	C4
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Overly	Overla	k1gMnSc2	Overla
Dedicated	Dedicated	k1gMnSc1	Dedicated
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kendrick	Kendricka	k1gFnPc2	Kendricka
Lamar	Lamara	k1gFnPc2	Lamara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kendrick	Kendrick	k1gMnSc1	Kendrick
Lamar	Lamar	k1gMnSc1	Lamar
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
kendricklamar	kendricklamar	k1gInSc1	kendricklamar
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
<g/>
.	.	kIx.	.
</s>
</p>
