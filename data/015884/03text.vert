<s>
Masožraví	masožraví	k1gMnPc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Masožravci	masožravec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Masožraví	masožraví	k1gMnPc1
Cicindela	Cicindela	k1gFnSc1
Hybrida	hybrida	k1gFnSc1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
šestinozí	šestinohý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Hexapoda	Hexapoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
Insecta	Insecta	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
brouci	brouk	k1gMnPc1
(	(	kIx(
<g/>
Coleoptera	Coleopter	k1gMnSc2
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
masožraví	masožraví	k1gMnPc1
(	(	kIx(
<g/>
Adephaga	Adephaga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Schellenberg	Schellenberg	k1gMnSc1
<g/>
,	,	kIx,
1806	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
několika	několik	k4yIc2
jedinců	jedinec	k1gMnPc2
od	od	k7c2
Harolda	Haroldo	k1gNnSc2
Maxwell-Lefroye	Maxwell-Lefroy	k1gFnSc2
</s>
<s>
Masožraví	masožraví	k1gMnPc1
(	(	kIx(
<g/>
Adephaga	Adephaga	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
podřád	podřád	k1gInSc1
brouků	brouk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Masožraví	masožraví	k1gMnPc1
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
podřád	podřád	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
devět	devět	k4xCc4
čeledí	čeleď	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
přibližně	přibližně	k6eAd1
35	#num#	k4
<g/>
–	–	k?
<g/>
38	#num#	k4
tisíc	tisíc	k4xCgInPc2
rodů	rod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
podřád	podřád	k1gInSc1
nemá	mít	k5eNaImIp3nS
vyhraněný	vyhraněný	k2eAgInSc1d1
biotop	biotop	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
zástupce	zástupce	k1gMnSc1
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
na	na	k7c6
souši	souš	k1gFnSc6
i	i	k8xC
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
druhů	druh	k1gInPc2
obsažených	obsažený	k2eAgInPc2d1
v	v	k7c6
tomto	tento	k3xDgInSc6
podřádu	podřád	k1gInSc6
má	mít	k5eAaImIp3nS
dravé	dravý	k2eAgMnPc4d1
dospělé	dospělí	k1gMnPc4
jedince	jedinec	k1gMnSc4
<g/>
,	,	kIx,
společným	společný	k2eAgInSc7d1
znakem	znak	k1gInSc7
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
jsou	být	k5eAaImIp3nP
dravé	dravý	k2eAgFnPc4d1
larvy	larva	k1gFnPc4
<g/>
,	,	kIx,
nepohyblivé	pohyblivý	k2eNgInPc4d1
kyčle	kyčel	k1gInPc4
posledního	poslední	k2eAgInSc2d1
páru	pár	k1gInSc2
nohou	noha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
zástupců	zástupce	k1gMnPc2
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
užitečné	užitečný	k2eAgNnSc4d1
<g/>
,	,	kIx,
protože	protože	k8xS
jsou	být	k5eAaImIp3nP
přirozenými	přirozený	k2eAgMnPc7d1
nepřáteli	nepřítel	k1gMnPc7
mnoha	mnoho	k4c2
škůdců	škůdce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Čeledi	čeleď	k1gFnPc1
</s>
<s>
Plavčíkovití	Plavčíkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Haliplidae	Haliplidae	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Potápníkovití	Potápníkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Dytiscidae	Dytiscidae	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Střevlíkovití	Střevlíkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Carabidae	Carabidae	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Svižníkovití	Svižníkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Cicindelinae	Cicindelinae	k1gNnSc7
<g/>
)	)	kIx)
–	–	k?
dříve	dříve	k6eAd2
samostatná	samostatný	k2eAgFnSc1d1
čeleď	čeleď	k1gFnSc1
Cicindelidae	Cicindelida	k1gFnSc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
většinou	většina	k1gFnSc7
podčeleď	podčeleď	k1gFnSc4
</s>
<s>
Vírníkovití	Vírníkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Gyrinidae	Gyrinidae	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
Amphizoidae	Amphizoidae	k6eAd1
</s>
<s>
Hygrobiidae	Hygrobiidae	k6eAd1
</s>
<s>
Noteridae	Noteridae	k6eAd1
</s>
<s>
Rhysodidae	Rhysodidae	k6eAd1
</s>
<s>
Trachypachidae	Trachypachidae	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Seznam	seznam	k1gInSc1
čeledí	čeleď	k1gFnPc2
brouků	brouk	k1gMnPc2
(	(	kIx(
<g/>
Coleoptera	Coleopter	k1gMnSc2
<g/>
)	)	kIx)
Říše	říše	k1gFnSc1
<g/>
:	:	kIx,
živočichové	živočich	k1gMnPc1
•	•	k?
Kmen	kmen	k1gInSc1
<g/>
:	:	kIx,
členovci	členovec	k1gMnPc1
•	•	k?
Třída	třída	k1gFnSc1
<g/>
:	:	kIx,
hmyz	hmyz	k1gInSc1
•	•	k?
Podtřída	podtřída	k1gFnSc1
<g/>
:	:	kIx,
Pterygota	Pterygota	k1gFnSc1
Adephaga	Adephaga	k1gFnSc1
(	(	kIx(
<g/>
masožraví	masožraví	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Amphizoidae	Amphizoidae	k1gFnSc1
•	•	k?
Carabidae	Carabida	k1gMnSc2
•	•	k?
Dytiscidae	Dytiscida	k1gMnSc2
•	•	k?
Gyrinidae	Gyrinida	k1gMnSc2
•	•	k?
Haliplidae	Haliplida	k1gMnSc2
•	•	k?
Hygrobiidae	Hygrobiida	k1gMnSc2
•	•	k?
Noteridae	Noterida	k1gMnSc2
•	•	k?
Rhysodidae	Rhysodida	k1gMnSc2
•	•	k?
Trachypachidae	Trachypachida	k1gMnSc2
Archostemata	Archostema	k1gNnPc1
(	(	kIx(
<g/>
prvožraví	prvožravit	k5eAaPmIp3nP
<g/>
)	)	kIx)
</s>
<s>
Crowsoniellidae	Crowsoniellidae	k1gFnSc1
•	•	k?
Cupedidae	Cupedida	k1gMnSc2
•	•	k?
Jurodidae	Jurodida	k1gMnSc2
•	•	k?
Micromalthidae	Micromalthida	k1gMnSc2
•	•	k?
Ommatidae	Ommatida	k1gMnSc2
Myxophaga	Myxophag	k1gMnSc2
(	(	kIx(
<g/>
řasožraví	řasožravit	k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
<s>
Hydroscaphidae	Hydroscaphidae	k1gFnSc1
•	•	k?
Lepiceridae	Lepiceridae	k1gInSc1
•	•	k?
Sphaeriusidae	Sphaeriusidae	k1gInSc1
•	•	k?
Torridincolidae	Torridincolidae	k1gInSc1
Polyphaga	Polyphaga	k1gFnSc1
(	(	kIx(
<g/>
všežraví	všežravý	k2eAgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Bostrichiformia	Bostrichiformia	k1gFnSc1
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Bostrichoidea	Bostrichoide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Anobiidae	Anobiidae	k1gNnPc1
•	•	k?
Bostrichidae	Bostrichida	k1gMnSc2
•	•	k?
Dermestidae	Dermestida	k1gMnSc2
•	•	k?
Jacobsoniidae	Jacobsoniida	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Derodontoidea	Derodontoidea	k1gFnSc1
<g/>
:	:	kIx,
Derodontidae	Derodontidae	k1gFnSc1
Cucujiformia	Cucujiformia	k1gFnSc1
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Chrysomeloidea	Chrysomeloide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Cerambycidae	Cerambycidae	k1gNnPc1
•	•	k?
Chrysomelidae	Chrysomelida	k1gMnSc2
•	•	k?
Megalopodidae	Megalopodida	k1gMnSc2
•	•	k?
Orsodacnidae	Orsodacnida	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Cleroidea	Cleroide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Acanthocnemidae	Acanthocnemidae	k1gNnPc1
•	•	k?
Chaetosomatidae	Chaetosomatida	k1gMnSc2
•	•	k?
Cleridae	Clerida	k1gMnSc2
•	•	k?
Melyridae	Melyrida	k1gMnSc2
•	•	k?
Phloiophilidae	Phloiophilida	k1gMnSc2
•	•	k?
Phycosecidae	Phycosecida	k1gMnSc2
•	•	k?
Prionoceridae	Prionocerida	k1gMnSc2
•	•	k?
Trogossitidaenadčeleď	Trogossitidaenadčeleď	k1gMnSc2
Cucujoidea	Cucujoideus	k1gMnSc2
<g/>
:	:	kIx,
Alexiidae	Alexiida	k1gMnSc2
•	•	k?
Biphyllidae	Biphyllida	k1gMnSc2
•	•	k?
Boganiidae	Boganiida	k1gMnSc2
•	•	k?
Bothrideridae	Bothriderida	k1gMnSc2
•	•	k?
Cavognathidae	Cavognathida	k1gMnSc2
•	•	k?
Cerylonidae	Cerylonida	k1gMnSc2
•	•	k?
Coccinellidae	Coccinellida	k1gMnSc2
•	•	k?
Corylophidae	Corylophida	k1gMnSc2
•	•	k?
Cryptophagidae	Cryptophagida	k1gMnSc2
•	•	k?
Cucujidae	Cucujida	k1gMnSc2
•	•	k?
Discolomatidae	Discolomatida	k1gMnSc2
•	•	k?
Endomychidae	Endomychida	k1gMnSc2
•	•	k?
Erotylidae	Erotylida	k1gMnSc2
•	•	k?
Helotidae	Helotida	k1gMnSc2
•	•	k?
Hobartiidae	Hobartiida	k1gMnSc2
•	•	k?
Kateretidae	Kateretida	k1gMnSc2
•	•	k?
Laemophloeidae	Laemophloeida	k1gMnSc2
•	•	k?
Lamingtoniidae	Lamingtoniida	k1gMnSc2
•	•	k?
Languriidae	Languriida	k1gMnSc2
•	•	k?
Monotomidae	Monotomida	k1gMnSc2
•	•	k?
Nitidulidae	Nitidulida	k1gMnSc2
•	•	k?
Passandridae	Passandrida	k1gMnSc2
•	•	k?
Phalacridae	Phalacrida	k1gMnSc2
•	•	k?
Phloeostichidae	Phloeostichida	k1gMnSc2
•	•	k?
Propalticidae	Propalticida	k1gMnSc2
•	•	k?
Protocucujidae	Protocucujida	k1gMnSc2
•	•	k?
Silvanidae	Silvanida	k1gMnSc2
•	•	k?
Smicripidae	Smicripida	k1gMnSc2
•	•	k?
Sphindidae	Sphindida	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Curculionoidea	Curculionoide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Anthribidae	Anthribidae	k1gNnPc1
•	•	k?
Attelabidae	Attelabida	k1gMnSc2
•	•	k?
Belidae	Belida	k1gMnSc2
•	•	k?
Brentidae	Brentida	k1gMnSc2
•	•	k?
Caridae	Carida	k1gMnSc2
•	•	k?
Curculionidae	Curculionida	k1gMnSc2
•	•	k?
Ithyceridae	Ithycerida	k1gMnSc2
•	•	k?
Nemonychidae	Nemonychida	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Lymexyloidea	Lymexyloidea	k1gFnSc1
<g/>
:	:	kIx,
Lymexylidae	Lymexylidae	k1gFnSc1
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Tenebrionoidea	Tenebrionoide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Aderidae	Aderidae	k1gNnPc1
•	•	k?
Anthicidae	Anthicida	k1gMnSc2
•	•	k?
Archeocrypticidae	Archeocrypticida	k1gMnSc2
•	•	k?
Boridae	Borida	k1gMnSc2
•	•	k?
Chalcodryidae	Chalcodryida	k1gMnSc2
•	•	k?
Ciidae	Ciida	k1gMnSc2
•	•	k?
Melandryidae	Melandryida	k1gMnSc2
•	•	k?
Meloidae	Meloida	k1gMnSc2
•	•	k?
Mordellidae	Mordellida	k1gMnSc2
•	•	k?
Mycetophagidae	Mycetophagida	k1gMnSc2
•	•	k?
Mycteridae	Mycterida	k1gMnSc2
•	•	k?
Oedemeridae	Oedemerida	k1gMnSc2
•	•	k?
Perimylopidae	Perimylopida	k1gMnSc2
•	•	k?
Prostomidae	Prostomida	k1gMnSc2
•	•	k?
Pterogeniidae	Pterogeniida	k1gMnSc2
•	•	k?
Pyrochroidae	Pyrochroida	k1gMnSc2
•	•	k?
Pythidae	Pythida	k1gMnSc2
•	•	k?
Salpingidae	Salpingida	k1gMnSc2
•	•	k?
Scraptiidae	Scraptiida	k1gMnSc2
•	•	k?
Stenotrachelidae	Stenotrachelida	k1gMnSc2
•	•	k?
Synchroidae	Synchroida	k1gMnSc2
•	•	k?
Tenebrionidae	Tenebrionida	k1gMnSc2
•	•	k?
Tetratomidae	Tetratomida	k1gMnSc2
•	•	k?
Trachelostenidae	Trachelostenida	k1gMnSc2
•	•	k?
Trictenotomidae	Trictenotomida	k1gMnSc2
•	•	k?
Ulodidae	Ulodida	k1gMnSc2
•	•	k?
Zopheridae	Zopherida	k1gMnSc2
Elateriformia	Elateriformius	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Buprestoidea	Buprestoidea	k1gFnSc1
<g/>
:	:	kIx,
Buprestidae	Buprestidae	k1gInSc1
•	•	k?
Schizopodidae	Schizopodidae	k1gInSc1
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Byrrhoidea	Byrrhoide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Byrrhidae	Byrrhidae	k1gNnPc1
•	•	k?
Callirhipidae	Callirhipida	k1gMnSc2
•	•	k?
Chelonariidae	Chelonariida	k1gMnSc2
•	•	k?
Cneoglossidae	Cneoglossida	k1gMnSc2
•	•	k?
Dryopidae	Dryopida	k1gMnSc2
•	•	k?
Elmidae	Elmida	k1gMnSc2
•	•	k?
Eulichadidae	Eulichadida	k1gMnSc2
•	•	k?
Heteroceridae	Heterocerida	k1gMnSc2
•	•	k?
Limnichidae	Limnichida	k1gMnSc2
•	•	k?
Lutrochidae	Lutrochida	k1gMnSc2
•	•	k?
Psephenidae	Psephenida	k1gMnSc2
•	•	k?
Ptilodactylidae	Ptilodactylida	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Dascilloidea	Dascilloidea	k1gFnSc1
<g/>
:	:	kIx,
Dascillidae	Dascillidae	k1gInSc1
•	•	k?
Rhipiceridae	Rhipiceridae	k1gInSc1
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Elateroidea	Elateroide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Artematopodidae	Artematopodidae	k1gNnPc1
•	•	k?
Brachypsectridae	Brachypsectrida	k1gMnSc2
•	•	k?
Cantharidae	Cantharida	k1gMnSc2
•	•	k?
Cerophytidae	Cerophytida	k1gMnSc2
•	•	k?
Drilidae	Drilida	k1gMnSc2
•	•	k?
Elateridae	Elaterida	k1gMnSc2
•	•	k?
Eucnemidae	Eucnemida	k1gMnSc2
•	•	k?
Jurasaidae	Jurasaida	k1gMnSc2
•	•	k?
Lampyridae	Lampyrida	k1gMnSc2
•	•	k?
Lissomidae	Lissomida	k1gMnSc2
•	•	k?
Lycidae	Lycida	k1gMnSc2
•	•	k?
Omalisidae	Omalisida	k1gMnSc2
•	•	k?
Omethidae	Omethida	k1gMnSc2
•	•	k?
Phengodidae	Phengodida	k1gMnSc2
•	•	k?
Plastoceridae	Plastocerida	k1gMnSc2
•	•	k?
Podabrocephalidae	Podabrocephalida	k1gMnSc2
•	•	k?
Rhinorhipidae	Rhinorhipida	k1gMnSc2
•	•	k?
Telegeusidae	Telegeusida	k1gMnSc2
•	•	k?
Throscidae	Throscida	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Scirtoidea	Scirtoide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Clambidae	Clambidae	k1gNnPc1
•	•	k?
Eucinetidae	Eucinetida	k1gMnSc2
•	•	k?
Scirtidae	Scirtida	k1gMnSc2
Scarabaeiformia	Scarabaeiformius	k1gMnSc2
</s>
<s>
Belohinidae	Belohinidae	k1gFnSc1
•	•	k?
Bolboceratidae	Bolboceratida	k1gMnSc2
•	•	k?
Ceratocanthidae	Ceratocanthida	k1gMnSc2
•	•	k?
Diphyllostomatidae	Diphyllostomatida	k1gMnSc2
•	•	k?
Geotrupidae	Geotrupida	k1gMnSc2
•	•	k?
Glaphyridae	Glaphyrida	k1gMnSc2
•	•	k?
Glaresidae	Glaresida	k1gMnSc2
•	•	k?
Hybosoridae	Hybosorida	k1gMnSc2
•	•	k?
Lucanidae	Lucanida	k1gMnSc2
•	•	k?
Ochodaeidae	Ochodaeida	k1gMnSc2
•	•	k?
Passalidae	Passalida	k1gMnSc2
•	•	k?
Pleocomidae	Pleocomida	k1gMnSc2
•	•	k?
Scarabaeidae	Scarabaeida	k1gMnSc2
•	•	k?
Trogidae	Trogida	k1gMnSc2
Staphyliniformia	Staphyliniformius	k1gMnSc2
</s>
<s>
nadčeleď	nadčeleď	k1gFnSc1
Hydrophiloidea	Hydrophiloide	k2eAgFnSc1d1
<g/>
:	:	kIx,
Histeridae	Histeridae	k1gNnPc1
•	•	k?
Hydrophilidae	Hydrophilida	k1gMnSc2
•	•	k?
Sphaeritidae	Sphaeritida	k1gMnSc2
•	•	k?
Synteliidaenadčeleď	Synteliidaenadčeleď	k1gMnSc2
Staphylinoidea	Staphylinoideus	k1gMnSc2
<g/>
:	:	kIx,
Agyrtidae	Agyrtida	k1gMnSc2
•	•	k?
Hydraenidae	Hydraenida	k1gMnSc2
•	•	k?
Leiodidae	Leiodida	k1gMnSc2
•	•	k?
Ptiliidae	Ptiliida	k1gMnSc2
•	•	k?
Scydmaenidae	Scydmaenida	k1gMnSc2
•	•	k?
Silphidae	Silphida	k1gMnSc2
•	•	k?
Staphylinidae	Staphylinida	k1gMnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Taxon	taxon	k1gInSc1
Adephaga	Adephag	k1gMnSc2
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Entomologie	entomologie	k1gFnSc1
</s>
