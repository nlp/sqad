<s>
Anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
tlouštík	tlouštík	k1gMnSc1	tlouštík
nebo	nebo	k8xC	nebo
tlustý	tlustý	k2eAgMnSc1d1	tlustý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
/	/	kIx~	/
<g/>
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
nesla	nést	k5eAaImAgFnS	nést
plutoniová	plutoniový	k2eAgFnSc1d1	plutoniová
jaderná	jaderný	k2eAgFnSc1d1	jaderná
puma	puma	k1gFnSc1	puma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
v	v	k7c4	v
11.02	[number]	k4	11.02
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
svržena	svrhnout	k5eAaPmNgFnS	svrhnout
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
bombardéru	bombardér	k1gInSc2	bombardér
B-	B-	k1gFnSc2	B-
<g/>
29	[number]	k4	29
<g/>
,	,	kIx,	,
pojmenovaného	pojmenovaný	k2eAgMnSc2d1	pojmenovaný
Bockscar	Bockscar	k1gMnSc1	Bockscar
<g/>
,	,	kIx,	,
na	na	k7c4	na
japonské	japonský	k2eAgNnSc4d1	Japonské
přístavní	přístavní	k2eAgNnSc4d1	přístavní
město	město	k1gNnSc4	město
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Puma	puma	k1gFnSc1	puma
explodovala	explodovat	k5eAaBmAgFnS	explodovat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
550	[number]	k4	550
m	m	kA	m
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
a	a	k8xC	a
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
energii	energie	k1gFnSc4	energie
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
výbuchu	výbuch	k1gInSc3	výbuch
22	[number]	k4	22
000	[number]	k4	000
t	t	k?	t
TNT	TNT	kA	TNT
tedy	tedy	k9	tedy
88	[number]	k4	88
TJ.	tj.	kA	tj.
Výbuch	výbuch	k1gInSc1	výbuch
způsobil	způsobit	k5eAaPmAgInS	způsobit
devastaci	devastace	k1gFnSc4	devastace
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
a	a	k8xC	a
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
smrt	smrt	k1gFnSc4	smrt
asi	asi	k9	asi
40	[number]	k4	40
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dalších	další	k2eAgNnPc2d1	další
25	[number]	k4	25
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc1	tisíc
dalších	další	k2eAgNnPc6d1	další
později	pozdě	k6eAd2	pozdě
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
asi	asi	k9	asi
74	[number]	k4	74
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1	atomová
bomby	bomba	k1gFnPc1	bomba
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
i	i	k9	i
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
byly	být	k5eAaImAgInP	být
zkonstruovány	zkonstruovat	k5eAaPmNgInP	zkonstruovat
v	v	k7c6	v
tajném	tajný	k2eAgInSc6d1	tajný
komplexu	komplex	k1gInSc6	komplex
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jaderného	jaderný	k2eAgMnSc2d1	jaderný
fyzika	fyzik	k1gMnSc2	fyzik
Roberta	Robert	k1gMnSc2	Robert
Oppenheimera	Oppenheimer	k1gMnSc2	Oppenheimer
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
pumy	puma	k1gFnSc2	puma
<g/>
:	:	kIx,	:
3,66	[number]	k4	3,66
m	m	kA	m
Průměr	průměr	k1gInSc1	průměr
<g/>
:	:	kIx,	:
1,52	[number]	k4	1,52
m	m	kA	m
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
4	[number]	k4	4
670	[number]	k4	670
kg	kg	kA	kg
Výbuch	výbuch	k1gInSc1	výbuch
o	o	k7c6	o
ekvivalentu	ekvivalent	k1gInSc6	ekvivalent
<g/>
:	:	kIx,	:
22	[number]	k4	22
kt	kt	k?	kt
TNT	TNT	kA	TNT
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
Atomové	atomový	k2eAgNnSc1d1	atomové
bombardování	bombardování	k1gNnSc1	bombardování
Hirošimy	Hirošima	k1gFnSc2	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
