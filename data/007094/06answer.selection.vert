<s>
Atomové	atomový	k2eAgFnPc1d1	atomová
bomby	bomba	k1gFnPc1	bomba
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
i	i	k9	i
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
byly	být	k5eAaImAgInP	být
zkonstruovány	zkonstruovat	k5eAaPmNgInP	zkonstruovat
v	v	k7c6	v
tajném	tajný	k2eAgInSc6d1	tajný
komplexu	komplex	k1gInSc6	komplex
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jaderného	jaderný	k2eAgMnSc2d1	jaderný
fyzika	fyzik	k1gMnSc2	fyzik
Roberta	Robert	k1gMnSc2	Robert
Oppenheimera	Oppenheimer	k1gMnSc2	Oppenheimer
<g/>
.	.	kIx.	.
</s>
