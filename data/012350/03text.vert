<p>
<s>
Slovem	slovem	k6eAd1	slovem
festival	festival	k1gInSc4	festival
označujeme	označovat	k5eAaImIp1nP	označovat
obvykle	obvykle	k6eAd1	obvykle
organizovanou	organizovaný	k2eAgFnSc4d1	organizovaná
sadu	sada	k1gFnSc4	sada
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
společenských	společenský	k2eAgFnPc2d1	společenská
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hudebních	hudební	k2eAgInPc2d1	hudební
nebo	nebo	k8xC	nebo
filmových	filmový	k2eAgNnPc2d1	filmové
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
festival	festival	k1gInSc1	festival
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
festive	festiv	k1gInSc5	festiv
<g/>
,	,	kIx,	,
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
shodného	shodný	k2eAgInSc2d1	shodný
významu	význam	k1gInSc2	význam
je	být	k5eAaImIp3nS	být
také	také	k9	také
slovo	slovo	k1gNnSc1	slovo
feast	feast	k5eAaPmF	feast
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhé	druhý	k4xOgFnSc2	druhý
verze	verze	k1gFnSc2	verze
slovo	slovo	k1gNnSc1	slovo
festival	festival	k1gInSc1	festival
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
středověké	středověký	k2eAgFnSc2d1	středověká
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
francouzštiny	francouzština	k1gFnSc2	francouzština
se	s	k7c7	s
základem	základ	k1gInSc7	základ
ve	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
latinském	latinský	k2eAgNnSc6d1	latinské
slově	slovo	k1gNnSc6	slovo
festivus	festivus	k1gMnSc1	festivus
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgMnSc1d1	znamenající
slavit	slavit	k5eAaImF	slavit
nebo	nebo	k8xC	nebo
oslavu	oslava	k1gFnSc4	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
festival	festival	k1gInSc1	festival
je	být	k5eAaImIp3nS	být
známé	známý	k1gMnPc4	známý
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1589	[number]	k4	1589
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
náboženských	náboženský	k2eAgInPc2d1	náboženský
svátků	svátek	k1gInPc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
festivaly	festival	k1gInPc4	festival
pořádali	pořádat	k5eAaImAgMnP	pořádat
kmenoví	kmenový	k2eAgMnPc1d1	kmenový
šamani	šaman	k1gMnPc1	šaman
jako	jako	k9	jako
oslavu	oslava	k1gFnSc4	oslava
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Čerpáno	čerpán	k2eAgNnSc1d1	čerpáno
z	z	k7c2	z
buletinu	buletin	k2eAgMnSc3d1	buletin
ČD	ČD	kA	ČD
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Vlakem	vlak	k1gInSc7	vlak
na	na	k7c4	na
festivaly	festival	k1gInPc4	festival
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Festival	festival	k1gInSc1	festival
jako	jako	k8xS	jako
přehlídka	přehlídka	k1gFnSc1	přehlídka
==	==	k?	==
</s>
</p>
<p>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
přehlídka	přehlídka	k1gFnSc1	přehlídka
soutěžní	soutěžní	k2eAgFnSc2d1	soutěžní
i	i	k8xC	i
nesoutěžní	soutěžní	k2eNgFnSc2d1	nesoutěžní
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
uměleckých	umělecký	k2eAgInPc2d1	umělecký
výkonů	výkon	k1gInPc2	výkon
a	a	k8xC	a
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
,	,	kIx,	,
společenská	společenský	k2eAgFnSc1d1	společenská
událost	událost	k1gFnSc1	událost
často	často	k6eAd1	často
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
rázu	ráz	k1gInSc2	ráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
umění	umění	k1gNnSc6	umění
</s>
</p>
<p>
<s>
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
</s>
</p>
<p>
<s>
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
</s>
</p>
<p>
<s>
divadelní	divadelní	k2eAgInSc4d1	divadelní
festival	festival	k1gInSc4	festival
</s>
</p>
<p>
<s>
literární	literární	k2eAgInSc4d1	literární
festival	festival	k1gInSc4	festival
</s>
</p>
<p>
<s>
V	v	k7c6	v
duchovní	duchovní	k2eAgFnSc6d1	duchovní
rovině	rovina	k1gFnSc6	rovina
</s>
</p>
<p>
<s>
duchovní	duchovní	k2eAgInSc1d1	duchovní
festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
přehlídka	přehlídka	k1gFnSc1	přehlídka
zbožnosti	zbožnost	k1gFnSc2	zbožnost
<g/>
,	,	kIx,	,
festivit	festivita	k1gFnPc2	festivita
<g/>
,	,	kIx,	,
církevních	církevní	k2eAgInPc2d1	církevní
úkonů	úkon	k1gInPc2	úkon
a	a	k8xC	a
pobožností	pobožnost	k1gFnPc2	pobožnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
festival	festival	k1gInSc1	festival
(	(	kIx(	(
<g/>
setkání	setkání	k1gNnSc1	setkání
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
také	také	k9	také
sjezd	sjezd	k1gInSc4	sjezd
<g/>
,	,	kIx,	,
sněm	sněm	k1gInSc1	sněm
či	či	k8xC	či
slet	slet	k1gInSc1	slet
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
doprovodným	doprovodný	k2eAgInSc7d1	doprovodný
společenským	společenský	k2eAgInSc7d1	společenský
programem	program	k1gInSc7	program
</s>
</p>
<p>
<s>
v	v	k7c6	v
totalitních	totalitní	k2eAgFnPc6d1	totalitní
dobách	doba	k1gFnPc6	doba
např.	např.	kA	např.
Festival	festival	k1gInSc4	festival
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
–	–	k?	–
slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
nepoužívá	používat	k5eNaImIp3nS	používat
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
filmových	filmový	k2eAgInPc2d1	filmový
festivalů	festival	k1gInPc2	festival
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
festival	festival	k1gInSc1	festival
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
