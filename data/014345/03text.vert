<s>
Funkční	funkční	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
rezonance	rezonance	k1gFnSc1
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
rezonance	rezonance	k1gFnSc1
(	(	kIx(
<g/>
fMRI	fMRI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc1d1
zobrazovací	zobrazovací	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
sloužící	sloužící	k2eAgFnSc1d1
k	k	k7c3
funkčnímu	funkční	k2eAgNnSc3d1
zobrazování	zobrazování	k1gNnSc3
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
mapování	mapování	k1gNnSc2
mozkové	mozkový	k2eAgFnSc2d1
odezvy	odezva	k1gFnSc2
na	na	k7c4
vnější	vnější	k2eAgInSc4d1
či	či	k8xC
vnitřní	vnitřní	k2eAgInSc4d1
podnět	podnět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vývojem	vývoj	k1gInSc7
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
a	a	k8xC
statistických	statistický	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
se	se	k3xPyFc4
rozvíjí	rozvíjet	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
fMRI	fMRI	k?
jako	jako	k8xC,k8xS
nástroj	nástroj	k1gInSc1
pro	pro	k7c4
vizualizaci	vizualizace	k1gFnSc4
anatomických	anatomický	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
mozku	mozek	k1gInSc2
zapojených	zapojený	k2eAgInPc2d1
do	do	k7c2
mechanismů	mechanismus	k1gInPc2
vnímání	vnímání	k1gNnSc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc2
motoriky	motorik	k1gMnPc7
a	a	k8xC
myšlení	myšlení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
od	od	k7c2
standardní	standardní	k2eAgFnSc2d1
magnetické	magnetický	k2eAgFnSc2d1
rezonance	rezonance	k1gFnSc2
schopností	schopnost	k1gFnPc2
detekovat	detekovat	k5eAaImF
dynamické	dynamický	k2eAgFnPc4d1
změny	změna	k1gFnPc4
signálu	signál	k1gInSc2
způsobené	způsobený	k2eAgFnSc2d1
lokálním	lokální	k2eAgNnSc7d1
kolísáním	kolísání	k1gNnSc7
poměru	poměr	k1gInSc2
oxyhemoglobinu	oxyhemoglobina	k1gFnSc4
a	a	k8xC
deoxyhemobloginu	deoxyhemoblogina	k1gFnSc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
neuronální	neuronální	k2eAgFnSc6d1
aktivitě	aktivita	k1gFnSc6
(	(	kIx(
<g/>
BOLD	BOLD	kA
<g/>
,	,	kIx,
tzn.	tzn.	kA
Blood	Blood	k1gInSc1
Oxygenation	Oxygenation	k1gInSc1
Level	level	k1gInSc1
Dependent	Dependent	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
fMRI	fMRI	k?
mapuje	mapovat	k5eAaImIp3nS
neuronální	neuronální	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
pouze	pouze	k6eAd1
nepřímo	přímo	k6eNd1
<g/>
,	,	kIx,
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
lokální	lokální	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
oxygenace	oxygenace	k1gFnSc2
a	a	k8xC
perfuze	perfuze	k1gFnSc2
mozkové	mozkový	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
také	také	k9
vychází	vycházet	k5eAaImIp3nS
její	její	k3xOp3gFnPc4
přednosti	přednost	k1gFnPc4
a	a	k8xC
limitace	limitace	k1gFnPc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
funkčního	funkční	k2eAgNnSc2d1
mapování	mapování	k1gNnSc2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meze	mez	k1gFnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
jsou	být	k5eAaImIp3nP
dány	dán	k2eAgFnPc1d1
tzv.	tzv.	kA
časovou	časový	k2eAgFnSc7d1
a	a	k8xC
prostorovou	prostorový	k2eAgFnSc7d1
rozlišovací	rozlišovací	k2eAgFnSc7d1
schopností	schopnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkční	funkční	k2eAgInSc1d1
MRI	MRI	kA
má	mít	k5eAaImIp3nS
relativně	relativně	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
prostorovou	prostorový	k2eAgFnSc4d1
rozlišovací	rozlišovací	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
(	(	kIx(
<g/>
řád	řád	k1gInSc4
jednotek	jednotka	k1gFnPc2
milimetrů	milimetr	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
časová	časový	k2eAgFnSc1d1
rozlišovací	rozlišovací	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
EEG	EEG	kA
(	(	kIx(
<g/>
elektroencefalografie	elektroencefalografie	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
MEG	MEG	kA
(	(	kIx(
<g/>
magnetoencefalografie	magnetoencefalografie	k1gFnSc1
<g/>
)	)	kIx)
omezená	omezený	k2eAgFnSc1d1
<g/>
.	.	kIx.
fMRI	fMRI	k?
nalézá	nalézat	k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc4
především	především	k9
v	v	k7c6
neurofyziologickém	neurofyziologický	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
řadě	řad	k1gInSc6
pracovišť	pracoviště	k1gNnPc2
se	se	k3xPyFc4
fMRI	fMRI	k?
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
např.	např.	kA
jako	jako	k8xS,k8xC
doplňující	doplňující	k2eAgNnSc1d1
vyšetření	vyšetření	k1gNnSc1
před	před	k7c7
neurochirurgickou	neurochirurgický	k2eAgFnSc7d1
intervencí	intervence	k1gFnSc7
v	v	k7c6
oblastech	oblast	k1gFnPc6
kritických	kritický	k2eAgFnPc6d1
pro	pro	k7c4
řečové	řečový	k2eAgFnPc4d1
či	či	k8xC
motorické	motorický	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
.	.	kIx.
fMRI	fMRI	k?
umožňuje	umožňovat	k5eAaImIp3nS
zpřesnění	zpřesnění	k1gNnSc4
diagnostiky	diagnostika	k1gFnSc2
některých	některý	k3yIgNnPc2
neurologických	urologický	k2eNgNnPc2d1,k2eAgNnPc2d1
i	i	k8xC
psychiatrických	psychiatrický	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
a	a	k8xC
nabízí	nabízet	k5eAaImIp3nS
možnosti	možnost	k1gFnPc1
plánování	plánování	k1gNnSc2
chirurgických	chirurgický	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Magnetické	magnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
jádra	jádro	k1gNnSc2
</s>
<s>
Atomové	atomový	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
nukleonů	nukleon	k1gInPc2
(	(	kIx(
<g/>
protonů	proton	k1gInPc2
a	a	k8xC
neutronů	neutron	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protony	proton	k1gInPc7
neustále	neustále	k6eAd1
rotují	rotovat	k5eAaImIp3nP
kolem	kolem	k7c2
své	svůj	k3xOyFgFnSc2
vlastní	vlastní	k2eAgFnSc2d1
osy	osa	k1gFnSc2
a	a	k8xC
tento	tento	k3xDgInSc1
pohyb	pohyb	k1gInSc1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
jako	jako	k9
spin	spin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
nabitá	nabitý	k2eAgFnSc1d1
částice	částice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
<g/>
,	,	kIx,
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
okolí	okolí	k1gNnSc6
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
a	a	k8xC
vykazuje	vykazovat	k5eAaImIp3nS
magnetický	magnetický	k2eAgInSc4d1
moment	moment	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protony	proton	k1gInPc1
si	se	k3xPyFc3
lze	lze	k6eAd1
tedy	tedy	k9
představit	představit	k5eAaPmF
jako	jako	k9
miniaturní	miniaturní	k2eAgInPc1d1
magnety	magnet	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atomová	atomový	k2eAgFnSc1d1
jádra	jádro	k1gNnPc4
se	s	k7c7
sudým	sudý	k2eAgNnSc7d1
nukleonovým	nukleonův	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
se	se	k3xPyFc4
nechovají	chovat	k5eNaImIp3nP
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
okolí	okolí	k1gNnSc3
magneticky	magneticky	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInPc1
magnetické	magnetický	k2eAgInPc1d1
momenty	moment	k1gInPc1
ruší	rušit	k5eAaImIp3nP
a	a	k8xC
nelze	lze	k6eNd1
je	být	k5eAaImIp3nS
využít	využít	k5eAaPmF
pro	pro	k7c4
MR	MR	kA
zobrazování	zobrazování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atomová	atomový	k2eAgFnSc1d1
jádra	jádro	k1gNnPc4
s	s	k7c7
lichým	lichý	k2eAgNnSc7d1
nukleonovým	nukleonův	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
si	se	k3xPyFc3
svůj	svůj	k3xOyFgInSc4
magnetický	magnetický	k2eAgInSc4d1
moment	moment	k1gInSc4
zachovávají	zachovávat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickým	charakteristický	k2eAgNnSc7d1
zástupcem	zástupce	k1gMnSc7
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
je	být	k5eAaImIp3nS
atom	atom	k1gInSc4
vodíku	vodík	k1gInSc2
1	#num#	k4
<g/>
H	H	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
proton	proton	k1gInSc1
a	a	k8xC
vykazuje	vykazovat	k5eAaImIp3nS
relativně	relativně	k6eAd1
velký	velký	k2eAgInSc1d1
magnetický	magnetický	k2eAgInSc1d1
moment	moment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
organismu	organismus	k1gInSc6
je	být	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
%	%	kIx~
vody	voda	k1gFnSc2
a	a	k8xC
1H	1H	k4
je	být	k5eAaImIp3nS
tedy	tedy	k9
nejvhodnějším	vhodný	k2eAgInSc7d3
objektem	objekt	k1gInSc7
pro	pro	k7c4
MR	MR	kA
zobrazování	zobrazování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
zástupci	zástupce	k1gMnSc3
jsou	být	k5eAaImIp3nP
13	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
19	#num#	k4
<g/>
F	F	kA
<g/>
,	,	kIx,
</s>
<s>
23	#num#	k4
<g/>
Na	na	k7c4
<g/>
,	,	kIx,
31	#num#	k4
<g/>
P.	P.	kA
Za	za	k7c2
normálních	normální	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
je	být	k5eAaImIp3nS
orientace	orientace	k1gFnSc1
rotačních	rotační	k2eAgInPc2d1
</s>
<s>
os	osa	k1gFnPc2
protonů	proton	k1gInPc2
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gInPc2
magnetických	magnetický	k2eAgInPc2d1
pólů	pól	k1gInPc2
<g/>
)	)	kIx)
ve	v	k7c6
tkáních	tkáň	k1gFnPc6
nahodilá	nahodilý	k2eAgFnSc1d1
<g/>
,	,	kIx,
magnetické	magnetický	k2eAgInPc1d1
momenty	moment	k1gInPc1
</s>
<s>
jednotlivých	jednotlivý	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
se	se	k3xPyFc4
tedy	tedy	k9
navzájem	navzájem	k6eAd1
ruší	rušit	k5eAaImIp3nS
a	a	k8xC
tkáň	tkáň	k1gFnSc1
se	se	k3xPyFc4
navenek	navenek	k6eAd1
jeví	jevit	k5eAaImIp3nS
nemagneticky	magneticky	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Vložíme	vložit	k5eAaPmIp1nP
<g/>
-li	-li	k?
tkáň	tkáň	k1gFnSc4
do	do	k7c2
silného	silný	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
uspořádají	uspořádat	k5eAaPmIp3nP
se	se	k3xPyFc4
rotační	rotační	k2eAgFnPc1d1
osy	osa	k1gFnPc1
protonů	proton	k1gInPc2
</s>
<s>
rovnoběžně	rovnoběžně	k6eAd1
se	s	k7c7
siločárami	siločára	k1gFnPc7
vnějšího	vnější	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInSc1d2
počet	počet	k1gInSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
</s>
<s>
v	v	k7c6
poloze	poloha	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jejich	jejich	k3xOp3gInSc1
magnetický	magnetický	k2eAgInSc1d1
moment	moment	k1gInSc1
je	být	k5eAaImIp3nS
orientován	orientovat	k5eAaBmNgInS
souhlasně	souhlasně	k6eAd1
(	(	kIx(
<g/>
paralelně	paralelně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
s	s	k7c7
vektorem	vektor	k1gInSc7
vnějšího	vnější	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
a	a	k8xC
menší	malý	k2eAgInSc1d2
počet	počet	k1gInSc1
protonů	proton	k1gInPc2
je	být	k5eAaImIp3nS
energeticky	energeticky	k6eAd1
</s>
<s>
náročnější	náročný	k2eAgNnSc1d2
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
jich	on	k3xPp3gMnPc2
méně	málo	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
„	„	k?
<g/>
nerovnováha	nerovnováha	k1gFnSc1
<g/>
“	“	k?
způsobí	způsobit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
tkáň	tkáň	k1gFnSc1
vykazuje	vykazovat	k5eAaImIp3nS
celkový	celkový	k2eAgInSc4d1
magnetický	magnetický	k2eAgInSc4d1
moment	moment	k1gInSc4
a	a	k8xC
navenek	navenek	k6eAd1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
magneticky	magneticky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
vlastnost	vlastnost	k1gFnSc4
</s>
<s>
je	být	k5eAaImIp3nS
základním	základní	k2eAgInSc7d1
principem	princip	k1gInSc7
MR	MR	kA
<g/>
.	.	kIx.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
</s>
<s>
tkáně	tkáň	k1gFnPc1
mají	mít	k5eAaImIp3nP
různou	různý	k2eAgFnSc4d1
biochemickou	biochemický	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
různé	různý	k2eAgNnSc1d1
zastoupení	zastoupení	k1gNnSc1
protonů	proton	k1gInPc2
a	a	k8xC
</s>
<s>
navenek	navenek	k6eAd1
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
různě	různě	k6eAd1
velikými	veliký	k2eAgInPc7d1
magnetickými	magnetický	k2eAgInPc7d1
momenty	moment	k1gInPc7
a	a	k8xC
dávají	dávat	k5eAaImIp3nP
tak	tak	k6eAd1
informaci	informace	k1gFnSc4
</s>
<s>
o	o	k7c6
svém	svůj	k3xOyFgNnSc6
složení	složení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protony	proton	k1gInPc1
umístěné	umístěný	k2eAgInPc1d1
v	v	k7c6
magnetickém	magnetický	k2eAgNnSc6d1
poli	pole	k1gNnSc6
konají	konat	k5eAaImIp3nP
kromě	kromě	k7c2
svého	svůj	k3xOyFgNnSc2
</s>
<s>
původního	původní	k2eAgInSc2d1
rotačního	rotační	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
(	(	kIx(
<g/>
spin	spin	k1gInSc1
<g/>
)	)	kIx)
ještě	ještě	k9
pohyb	pohyb	k1gInSc1
precesní	precesní	k2eAgInSc1d1
–	–	k?
po	po	k7c6
plášti	plášť	k1gInSc6
rotačního	rotační	k2eAgInSc2d1
</s>
<s>
kužele	kužel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frekvence	frekvence	k1gFnSc1
precesního	precesní	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Larmorova	Larmorův	k2eAgFnSc1d1
frekvence	frekvence	k1gFnSc1
<g/>
,	,	kIx,
závisí	záviset	k5eAaImIp3nS
na	na	k7c4
</s>
<s>
magnetických	magnetický	k2eAgFnPc6d1
vlastnostech	vlastnost	k1gFnPc6
daného	daný	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
vyjádřených	vyjádřený	k2eAgInPc2d1
v	v	k7c6
tzv.	tzv.	kA
gyromagnetickém	gyromagnetický	k2eAgNnSc6d1
</s>
<s>
poměru	poměra	k1gFnSc4
a	a	k8xC
na	na	k7c6
intenzitě	intenzita	k1gFnSc6
vnějšího	vnější	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
Princip	princip	k1gInSc1
</s>
<s>
NMR	NMR	kA
(	(	kIx(
<g/>
nukleární	nukleární	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
rezonance	rezonance	k1gFnSc1
<g/>
)	)	kIx)
spočívá	spočívat	k5eAaImIp3nS
</s>
<s>
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
rotující	rotující	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
umístěno	umístit	k5eAaPmNgNnS
v	v	k7c6
konstantním	konstantní	k2eAgNnSc6d1
magnetickém	magnetický	k2eAgNnSc6d1
poli	pole	k1gNnSc6
B	B	kA
<g/>
0	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
srovnání	srovnání	k1gNnSc3
magnetických	magnetický	k2eAgInPc2d1
momentů	moment	k1gInPc2
(	(	kIx(
<g/>
os	osa	k1gFnPc2
rotace	rotace	k1gFnSc2
<g/>
)	)	kIx)
s	s	k7c7
vnějším	vnější	k2eAgNnSc7d1
magnetickým	magnetický	k2eAgNnSc7d1
polem	pole	k1gNnSc7
</s>
<s>
a	a	k8xC
osa	osa	k1gFnSc1
jádra	jádro	k1gNnSc2
bude	být	k5eAaImBp3nS
lehce	lehko	k6eAd1
rotovat	rotovat	k5eAaImF
kolem	kolem	k7c2
směru	směr	k1gInSc2
působícího	působící	k2eAgNnSc2d1
pole	pole	k1gNnSc2
B	B	kA
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pohyb	pohyb	k1gInSc1
</s>
<s>
vzniká	vznikat	k5eAaImIp3nS
při	při	k7c6
každé	každý	k3xTgFnSc6
změně	změna	k1gFnSc6
působícího	působící	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
jádro	jádro	k1gNnSc4
v	v	k7c6
dané	daný	k2eAgFnSc6d1
</s>
<s>
poloze	poloha	k1gFnSc3
neustálí	ustálit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
vnější	vnější	k2eAgNnSc4d1
pole	pole	k1gNnSc4
přestane	přestat	k5eAaPmIp3nS
působit	působit	k5eAaImF
<g/>
,	,	kIx,
vrací	vracet	k5eAaImIp3nS
se	se	k3xPyFc4
jádro	jádro	k1gNnSc1
do	do	k7c2
své	svůj	k3xOyFgFnSc2
</s>
<s>
původní	původní	k2eAgFnSc2d1
klidové	klidový	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
přidá	přidat	k5eAaPmIp3nS
druhé	druhý	k4xOgInPc1
kolmo	kolmo	k6eAd1
působící	působící	k2eAgInPc1d1
(	(	kIx(
<g/>
transverzální	transverzální	k2eAgInPc1d1
<g/>
)	)	kIx)
</s>
<s>
pole	pole	k1gNnSc1
BT	BT	kA
<g/>
,	,	kIx,
začne	začít	k5eAaPmIp3nS
jádro	jádro	k1gNnSc1
opět	opět	k6eAd1
rotovat	rotovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	k9
byla	být	k5eAaImAgNnP
jádra	jádro	k1gNnPc1
udržena	udržen	k2eAgNnPc1d1
ve	v	k7c6
stálém	stálý	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
vysokofrekvenční	vysokofrekvenční	k2eAgFnPc1d1
magnetické	magnetický	k2eAgFnPc1d1
pole	pole	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
současně	současně	k6eAd1
rotuje	rotovat	k5eAaImIp3nS
v	v	k7c6
rovině	rovina	k1gFnSc6
XY	XY	kA
<g/>
.	.	kIx.
</s>
<s>
Volbou	volba	k1gFnSc7
velikosti	velikost	k1gFnSc2
prvního	první	k4xOgNnSc2
statického	statický	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
B0	B0	k1gFnSc2
a	a	k8xC
volby	volba	k1gFnSc2
velikosti	velikost	k1gFnSc2
</s>
<s>
pro	pro	k7c4
transverzální	transverzální	k2eAgFnSc4d1
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
BT	BT	kA
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
velice	velice	k6eAd1
přesně	přesně	k6eAd1
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
</s>
<s>
jádra	jádro	k1gNnPc1
budou	být	k5eAaImBp3nP
v	v	k7c6
rezonanci	rezonance	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rezonancí	rezonance	k1gFnPc2
je	být	k5eAaImIp3nS
magnetický	magnetický	k2eAgInSc1d1
moment	moment	k1gInSc1
m	m	kA
jádra	jádro	k1gNnPc1
</s>
<s>
překlopen	překlopen	k2eAgMnSc1d1
o	o	k7c4
90	#num#	k4
<g/>
°	°	k?
do	do	k7c2
roviny	rovina	k1gFnSc2
XY	XY	kA
a	a	k8xC
osa	osa	k1gFnSc1
pak	pak	k6eAd1
rotuje	rotovat	k5eAaImIp3nS
podle	podle	k7c2
transverzálního	transverzální	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
</s>
<s>
je	být	k5eAaImIp3nS
transverzální	transverzální	k2eAgNnSc1d1
pole	pole	k1gNnSc1
odpojeno	odpojen	k2eAgNnSc1d1
<g/>
,	,	kIx,
rotuje	rotovat	k5eAaImIp3nS
jádro	jádro	k1gNnSc1
stále	stále	k6eAd1
v	v	k7c6
rovině	rovina	k1gFnSc6
XY	XY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přiblížením	přiblížení	k1gNnSc7
</s>
<s>
cívky	cívka	k1gFnPc1
do	do	k7c2
blízkosti	blízkost	k1gFnSc2
rotujícího	rotující	k2eAgInSc2d1
magnetického	magnetický	k2eAgInSc2d1
momentu	moment	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
indukuje	indukovat	k5eAaBmIp3nS
napětí	napětí	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
měřeno	měřit	k5eAaImNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjednodušeně	zjednodušeně	k6eAd1
je	být	k5eAaImIp3nS
velikost	velikost	k1gFnSc4
naměřeného	naměřený	k2eAgNnSc2d1
napětí	napětí	k1gNnSc2
</s>
<s>
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
poloze	poloha	k1gFnSc6
a	a	k8xC
typu	typ	k1gInSc2
tkáně	tkáň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
BOLD	BOLD	kA
fMRI	fMRI	k?
</s>
<s>
K	k	k7c3
funkčnímu	funkční	k2eAgNnSc3d1
mapování	mapování	k1gNnSc3
mozkové	mozkový	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
pomocí	pomocí	k7c2
MR	MR	kA
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
užít	užít	k5eAaPmF
dvou	dva	k4xCgInPc6
principů	princip	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
buď	buď	k8xC
o	o	k7c4
změnu	změna	k1gFnSc4
perfuze	perfuze	k1gFnSc2
a	a	k8xC
objemu	objem	k1gInSc2
krve	krev	k1gFnSc2
v	v	k7c6
místě	místo	k1gNnSc6
</s>
<s>
neuronální	neuronální	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
(	(	kIx(
<g/>
perfuzní	perfuzní	k2eAgFnSc1d1
fMRI	fMRI	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
anebo	anebo	k8xC
navíc	navíc	k6eAd1
o	o	k7c4
změnu	změna	k1gFnSc4
poměru	poměr	k1gInSc2
okysličené	okysličený	k2eAgFnSc3d1
a	a	k8xC
</s>
<s>
neokysličené	okysličený	k2eNgFnPc1d1
formy	forma	k1gFnPc1
hemoglobinu	hemoglobin	k1gInSc2
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
(	(	kIx(
<g/>
BOLD	BOLD	kA
fMRI	fMRI	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podstatou	podstata	k1gFnSc7
</s>
<s>
metody	metoda	k1gFnPc4
je	být	k5eAaImIp3nS
změna	změna	k1gFnSc1
prokrvení	prokrvení	k1gNnSc2
a	a	k8xC
objemu	objem	k1gInSc2
krve	krev	k1gFnSc2
v	v	k7c6
aktivní	aktivní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
mozkové	mozkový	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
perfuzní	perfuzeň	k1gFnPc2
fMRI	fMRI	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
změna	změna	k1gFnSc1
mezi	mezi	k7c7
poměrem	poměr	k1gInSc7
okysličené	okysličený	k2eAgFnSc2d1
formy	forma	k1gFnSc2
hemoglobinu	hemoglobin	k1gInSc2
(	(	kIx(
<g/>
BOLD	BOLD	kA
</s>
<s>
fMRI	fMRI	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neokysličená	okysličený	k2eNgFnSc1d1
forma	forma	k1gFnSc1
hemoglobinu	hemoglobin	k1gInSc2
má	mít	k5eAaImIp3nS
paramagnetické	paramagnetický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
a	a	k8xC
chová	chovat	k5eAaImIp3nS
se	se	k3xPyFc4
</s>
<s>
jako	jako	k8xC,k8xS
přirozená	přirozený	k2eAgFnSc1d1
MR	MR	kA
kontrastní	kontrastní	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zkracuje	zkracovat	k5eAaImIp3nS
relaxační	relaxační	k2eAgInSc4d1
čas	čas	k1gInSc4
T	T	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivní	aktivní	k2eAgFnSc1d1
</s>
<s>
oblast	oblast	k1gFnSc1
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
spotřebovává	spotřebovávat	k5eAaImIp3nS
více	hodně	k6eAd2
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
potom	potom	k6eAd1
poskytuje	poskytovat	k5eAaImIp3nS
silnější	silný	k2eAgInSc1d2
signál	signál	k1gInSc1
</s>
<s>
než	než	k8xS
okolí	okolí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
fMRI	fMRI	k?
vyšetření	vyšetření	k1gNnSc1
se	se	k3xPyFc4
opakovaným	opakovaný	k2eAgNnSc7d1
skenováním	skenování	k1gNnSc7
získávají	získávat	k5eAaImIp3nP
obrazy	obraz	k1gInPc1
celého	celý	k2eAgInSc2d1
</s>
<s>
objemu	objem	k1gInSc3
mozku	mozek	k1gInSc2
v	v	k7c6
klidu	klid	k1gInSc6
i	i	k8xC
při	při	k7c6
aktivním	aktivní	k2eAgNnSc6d1
řešení	řešení	k1gNnSc6
úkolů	úkol	k1gInPc2
(	(	kIx(
<g/>
reakce	reakce	k1gFnSc2
na	na	k7c4
podnět	podnět	k1gInSc4
<g/>
,	,	kIx,
pohyb	pohyb	k1gInSc4
</s>
<s>
končetin	končetina	k1gFnPc2
<g/>
,	,	kIx,
tvorba	tvorba	k1gFnSc1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
Změny	změna	k1gFnPc1
mozkové	mozkový	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
jsou	být	k5eAaImIp3nP
měřeny	měřit	k5eAaImNgFnP
z	z	k7c2
rozdílu	rozdíl	k1gInSc2
</s>
<s>
dvojic	dvojice	k1gFnPc2
obrazů	obraz	k1gInPc2
podřízených	podřízený	k2eAgInPc2d1
v	v	k7c6
klidu	klid	k1gInSc6
a	a	k8xC
při	při	k7c6
mozkové	mozkový	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Difuzní	difuzní	k2eAgFnSc1d1
MRI	MRI	kA
</s>
<s>
DTI	DTI	kA
obraz	obraz	k1gInSc1
mozku	mozek	k1gInSc2
zobrazující	zobrazující	k2eAgInPc4d1
směry	směr	k1gInPc4
difuze	difuze	k1gFnSc2
molekul	molekula	k1gFnPc2
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Difuzní	difuzní	k2eAgInSc1d1
MRI	MRI	kA
zobrazuje	zobrazovat	k5eAaImIp3nS
změny	změna	k1gFnPc4
signálu	signál	k1gInSc2
způsobené	způsobený	k2eAgFnSc2d1
difuzí	difuze	k1gFnSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
molekul	molekula	k1gFnPc2
vody	voda	k1gFnSc2
ve	v	k7c6
tkáních	tkáň	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
zobrazení	zobrazení	k1gNnSc1
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
nezávislé	závislý	k2eNgNnSc1d1
na	na	k7c6
relaxačních	relaxační	k2eAgFnPc6d1
</s>
<s>
časech	čas	k1gInPc6
T	T	kA
<g/>
1	#num#	k4
<g/>
,	,	kIx,
T2	T2	k1gFnSc1
i	i	k9
na	na	k7c6
hustotě	hustota	k1gFnSc6
protonových	protonový	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
(	(	kIx(
<g/>
PD	PD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Difuzní	difuzní	k2eAgInSc1d1
zobrazení	zobrazení	k1gNnSc2
se	s	k7c7
</s>
<s>
uplatňuje	uplatňovat	k5eAaImIp3nS
především	především	k9
při	při	k7c6
hodnocení	hodnocení	k1gNnSc6
patologických	patologický	k2eAgInPc2d1
stavů	stav	k1gInPc2
mozku	mozek	k1gInSc2
(	(	kIx(
<g/>
stáří	stáří	k1gNnSc2
ischemického	ischemický	k2eAgNnSc2d1
postižení	postižení	k1gNnSc2
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
traumatické	traumatický	k2eAgFnPc4d1
změny	změna	k1gFnPc4
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
posuzování	posuzování	k1gNnSc4
buněčného	buněčný	k2eAgNnSc2d1
složení	složení	k1gNnSc2
</s>
<s>
mozkových	mozkový	k2eAgInPc2d1
nádorů	nádor	k1gInPc2
nebo	nebo	k8xC
změny	změna	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1
choroby	choroba	k1gFnSc2
<g/>
,	,	kIx,
autismu	autismus	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
schizofrenie	schizofrenie	k1gFnSc1
<g/>
,	,	kIx,
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Směr	směr	k1gInSc1
difuze	difuze	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zcela	zcela	k6eAd1
náhodný	náhodný	k2eAgInSc1d1
všemi	všecek	k3xTgInPc7
směry	směr	k1gInPc7
(	(	kIx(
<g/>
např.	např.	kA
</s>
<s>
v	v	k7c6
mozkomíšním	mozkomíšní	k2eAgInSc6d1
moku	mok	k1gInSc6
nebo	nebo	k8xC
šedé	šedý	k2eAgFnSc6d1
hmotě	hmota	k1gFnSc6
mozku	mozek	k1gInSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
omezený	omezený	k2eAgInSc1d1
pouze	pouze	k6eAd1
na	na	k7c4
některé	některý	k3yIgFnPc4
</s>
<s>
směry	směr	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
bílé	bílý	k2eAgFnSc6d1
hmotě	hmota	k1gFnSc6
mozku	mozek	k1gInSc2
převládá	převládat	k5eAaImIp3nS
difuze	difuze	k1gFnSc1
ve	v	k7c6
směru	směr	k1gInSc6
dlouhých	dlouhý	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
</s>
<s>
axonů	axon	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
všesměrové	všesměrový	k2eAgFnSc3d1
difuzi	difuze	k1gFnSc3
bráněno	bráněn	k2eAgNnSc4d1
</s>
<s>
bariérou	bariéra	k1gFnSc7
např.	např.	kA
buněčných	buněčný	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směr	směr	k1gInSc1
</s>
<s>
difuze	difuze	k1gFnSc1
lze	lze	k6eAd1
zjistit	zjistit	k5eAaPmF
mnohonásobným	mnohonásobný	k2eAgNnSc7d1
skenováním	skenování	k1gNnSc7
zvolené	zvolený	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
tkáně	tkáň	k1gFnSc2
</s>
<s>
v	v	k7c6
několika	několik	k4yIc6
směrech	směr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
směry	směr	k1gInPc1
zobrazení	zobrazení	k1gNnSc2
se	se	k3xPyFc4
získávají	získávat	k5eAaImIp3nP
změnou	změna	k1gFnSc7
</s>
<s>
orientace	orientace	k1gFnSc1
magnetických	magnetický	k2eAgInPc2d1
gradientů	gradient	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
směr	směr	k1gInSc1
gradientu	gradient	k1gInSc2
potom	potom	k6eAd1
zobrazuje	zobrazovat	k5eAaImIp3nS
jiný	jiný	k2eAgInSc1d1
</s>
<s>
směr	směr	k1gInSc1
difuze	difuze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
směr	směr	k1gInSc1
difuze	difuze	k1gFnSc2
vypočítává	vypočítávat	k5eAaImIp3nS
nejméně	málo	k6eAd3
z	z	k7c2
6	#num#	k4
směrů	směr	k1gInPc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
</s>
<s>
však	však	k9
z	z	k7c2
12	#num#	k4
až	až	k9
256	#num#	k4
směrů	směr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
difuzního	difuzní	k2eAgNnSc2d1
zobrazení	zobrazení	k1gNnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
šedoškálová	šedoškálový	k2eAgFnSc1d1
</s>
<s>
mapa	mapa	k1gFnSc1
velikosti	velikost	k1gFnSc2
difuze	difuze	k1gFnSc1
(	(	kIx(
<g/>
metoda	metoda	k1gFnSc1
DWI	DWI	kA
–	–	k?
Diffusion	Diffusion	k1gInSc1
Weighted	Weighted	k1gMnSc1
Imaging	Imaging	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
barevná	barevný	k2eAgFnSc1d1
</s>
<s>
mapa	mapa	k1gFnSc1
mozku	mozek	k1gInSc2
(	(	kIx(
<g/>
metoda	metoda	k1gFnSc1
DTI	DTI	kA
–	–	k?
Diffusion	Diffusion	k1gInSc1
Tensor	tensor	k1gInSc1
Imaging	Imaging	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
určují	určovat	k5eAaImIp3nP
</s>
<s>
jednotlivé	jednotlivý	k2eAgInPc1d1
směry	směr	k1gInPc1
difuze	difuze	k1gFnSc2
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázka	ukázka	k1gFnSc1
DTI	DTI	kA
obrazu	obraz	k1gInSc2
mozku	mozek	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c4
</s>
<s>
obrázku	obrázek	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Difuze	difuze	k1gFnSc2
=	=	kIx~
</s>
<s>
přechod	přechod	k1gInSc1
částic	částice	k1gFnPc2
látky	látka	k1gFnSc2
z	z	k7c2
míst	místo	k1gNnPc2
vyšší	vysoký	k2eAgMnSc1d2
koncentrací	koncentrace	k1gFnSc7
látky	látka	k1gFnSc2
do	do	k7c2
míst	místo	k1gNnPc2
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
</s>
<s>
koncentrací	koncentrace	k1gFnSc7
látky	látka	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
tepelného	tepelný	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Difuze	difuze	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
</s>
<s>
vyrovnání	vyrovnání	k1gNnSc1
koncentrací	koncentrace	k1gFnPc2
látky	látka	k1gFnSc2
v	v	k7c6
celém	celý	k2eAgInSc6d1
objemu	objem	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Metodika	metodika	k1gFnSc1
vyšetření	vyšetření	k1gNnPc2
fMRI	fMRI	k?
(	(	kIx(
<g/>
designování	designování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgNnSc1d1
omezení	omezení	k1gNnSc1
fMRI	fMRI	k?
spočívá	spočívat	k5eAaImIp3nS
ve	v	k7c6
velmi	velmi	k6eAd1
</s>
<s>
malé	malý	k2eAgFnSc3d1
změně	změna	k1gFnSc3
intenzity	intenzita	k1gFnSc2
signálu	signál	k1gInSc2
(	(	kIx(
<g/>
T	T	kA
<g/>
2	#num#	k4
<g/>
*	*	kIx~
<g/>
)	)	kIx)
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
hemodynamickou	hemodynamický	k2eAgFnSc7d1
odpovědí	odpověď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Navíc	navíc	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
standardizovaná	standardizovaný	k2eAgFnSc1d1
klidová	klidový	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
BOLD	BOLD	kA
signálu	signál	k1gInSc2
(	(	kIx(
<g/>
rozdíly	rozdíl	k1gInPc1
jsou	být	k5eAaImIp3nP
</s>
<s>
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
osobami	osoba	k1gFnPc7
i	i	k8xC
jednotlivými	jednotlivý	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
mozkové	mozkový	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
</s>
<s>
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
výchozí	výchozí	k2eAgFnSc1d1
referenční	referenční	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
dokázali	dokázat	k5eAaPmAgMnP
odlišit	odlišit	k5eAaPmF
</s>
<s>
na	na	k7c6
experimentu	experiment	k1gInSc6
závislé	závislý	k2eAgFnSc2d1
změny	změna	k1gFnSc2
signálu	signál	k1gInSc2
od	od	k7c2
šumu	šum	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
zdaleka	zdaleka	k6eAd1
nepostačuje	postačovat	k5eNaImIp3nS
pouhé	pouhý	k2eAgNnSc1d1
</s>
<s>
srovnání	srovnání	k1gNnSc4
dvou	dva	k4xCgInPc2
snímků	snímek	k1gInPc2
(	(	kIx(
<g/>
klid	klid	k1gInSc1
versus	versus	k7c1
aktivita	aktivita	k1gFnSc1
<g/>
)	)	kIx)
dané	daný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
dostatečné	dostatečný	k2eAgFnSc3d1
statistické	statistický	k2eAgFnSc3d1
robustnosti	robustnost	k1gFnSc3
výsledků	výsledek	k1gInPc2
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
získat	získat	k5eAaPmF
daných	daný	k2eAgInPc2d1
</s>
<s>
snímků	snímek	k1gInPc2
hned	hned	k6eAd1
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc4d1
objem	objem	k1gInSc4
mozku	mozek	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
32	#num#	k4
řezů	řez	k1gInPc2
<g/>
)	)	kIx)
tedy	tedy	k9
snímáme	snímat	k5eAaImIp1nP
</s>
<s>
několikrát	několikrát	k6eAd1
jak	jak	k8xC,k8xS
při	při	k7c6
aktivním	aktivní	k2eAgNnSc6d1
provádění	provádění	k1gNnSc6
dané	daný	k2eAgFnSc2d1
úlohy	úloha	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
vymýšlení	vymýšlení	k1gNnSc1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
pohyb	pohyb	k1gInSc1
</s>
<s>
končetinou	končetina	k1gFnSc7
<g/>
,	,	kIx,
reakce	reakce	k1gFnSc1
na	na	k7c4
určitý	určitý	k2eAgInSc4d1
podnět	podnět	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
klidu	klid	k1gInSc6
(	(	kIx(
<g/>
resp.	resp.	kA
při	při	k7c6
provádění	provádění	k1gNnSc6
</s>
<s>
kontrolní	kontrolní	k2eAgFnPc4d1
úlohy	úloha	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
výsledky	výsledek	k1gInPc4
následně	následně	k6eAd1
statisticky	statisticky	k6eAd1
vyhodnocujeme	vyhodnocovat	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgInPc4
</s>
<s>
vyšetření	vyšetření	k1gNnSc1
je	být	k5eAaImIp3nS
charakterizováno	charakterizovat	k5eAaBmNgNnS
určitým	určitý	k2eAgNnSc7d1
časovým	časový	k2eAgNnSc7d1
uspořádáním	uspořádání	k1gNnSc7
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yIgInSc3,k3yRgInSc3
říkáme	říkat	k5eAaImIp1nP
</s>
<s>
design	design	k1gInSc1
experimentu	experiment	k1gInSc2
(	(	kIx(
<g/>
měření	měření	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc4
základní	základní	k2eAgInPc4d1
typy	typ	k1gInPc4
designů	design	k1gInPc2
–	–	k?
blokový	blokový	k2eAgInSc4d1
</s>
<s>
design	design	k1gInSc1
a	a	k8xC
event-related	event-related	k1gInSc1
design	design	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
typy	typ	k1gInPc1
designů	design	k1gInPc2
pak	pak	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc4
vzájemnou	vzájemný	k2eAgFnSc4d1
</s>
<s>
kombinací	kombinace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
fMRI	fMRI	k?
snímek	snímek	k1gInSc1
během	během	k7c2
zátěže	zátěž	k1gFnSc2
paměti	paměť	k1gFnSc2
</s>
<s>
Blokový	blokový	k2eAgInSc1d1
design	design	k1gInSc1
</s>
<s>
je	být	k5eAaImIp3nS
jednodušší	jednoduchý	k2eAgMnSc1d2
jak	jak	k8xS,k8xC
na	na	k7c4
vlastní	vlastní	k2eAgNnSc4d1
měření	měření	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k9
na	na	k7c4
</s>
<s>
následné	následný	k2eAgNnSc4d1
statické	statický	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vyšetření	vyšetření	k1gNnSc6
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
střídání	střídání	k1gNnSc3
dvou	dva	k4xCgFnPc2
či	či	k8xC
více	hodně	k6eAd2
</s>
<s>
bloků	blok	k1gInPc2
událostí	událost	k1gFnPc2
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
aktivita	aktivita	k1gFnSc1
versus	versus	k7c1
klid	klid	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
každého	každý	k3xTgInSc2
bloku	blok	k1gInSc2
změříme	změřit	k5eAaPmIp1nP
vždy	vždy	k6eAd1
</s>
<s>
několik	několik	k4yIc1
funkčních	funkční	k2eAgInPc2d1
skenů	sken	k1gInPc2
celého	celý	k2eAgInSc2d1
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
výsledná	výsledný	k2eAgNnPc4d1
data	datum	k1gNnPc4
pak	pak	k6eAd1
statisticky	statisticky	k6eAd1
</s>
<s>
porovnáváme	porovnávat	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řazením	řazení	k1gNnSc7
stimulačních	stimulační	k2eAgInPc2d1
podnětů	podnět	k1gInPc2
do	do	k7c2
bloků	blok	k1gInPc2
získáme	získat	k5eAaPmIp1nP
vyšší	vysoký	k2eAgFnSc4d2
</s>
<s>
hladinu	hladina	k1gFnSc4
BOLD	BOLD	kA
signálu	signál	k1gInSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
odpovědí	odpověď	k1gFnSc7
na	na	k7c4
jediný	jediný	k2eAgInSc4d1
krátký	krátký	k2eAgInSc4d1
podnět	podnět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Připravujeme	připravovat	k5eAaImIp1nP
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
možnost	možnost	k1gFnSc4
detekovat	detekovat	k5eAaImF
tvar	tvar	k1gInSc4
hemodynamické	hemodynamický	k2eAgFnSc2d1
odezvy	odezva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blokový	blokový	k1gMnSc1
</s>
<s>
design	design	k1gInSc1
je	být	k5eAaImIp3nS
preferenčně	preferenčně	k6eAd1
využívám	využívat	k5eAaPmIp1nS,k5eAaImIp1nS
při	při	k7c6
snímání	snímání	k1gNnSc6
fMRI	fMRI	k?
pro	pro	k7c4
klinické	klinický	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
z	z	k7c2
principu	princip	k1gInSc2
</s>
<s>
nelze	lze	k6eNd1
tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
designu	design	k1gInSc2
využít	využít	k5eAaPmF
při	při	k7c6
vyšetřování	vyšetřování	k1gNnSc6
pozornosti	pozornost	k1gFnSc2
či	či	k8xC
kognitivních	kognitivní	k2eAgFnPc2d1
</s>
<s>
úlohách	úloha	k1gFnPc6
založených	založený	k2eAgInPc2d1
na	na	k7c6
spontánním	spontánní	k2eAgNnSc6d1
provádění	provádění	k1gNnSc6
určité	určitý	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Event-related	Event-related	k1gInSc1
design	design	k1gInSc1
</s>
<s>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
design	design	k1gInSc4
určený	určený	k2eAgInSc4d1
k	k	k7c3
detekci	detekce	k1gFnSc3
hemodynamické	hemodynamický	k2eAgFnSc2d1
odezvy	odezva	k1gFnSc2
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
události	událost	k1gFnPc4
</s>
<s>
(	(	kIx(
<g/>
či	či	k8xC
specifické	specifický	k2eAgFnPc1d1
posloupnosti	posloupnost	k1gFnPc1
událostí	událost	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejjednodušší	jednoduchý	k2eAgFnSc6d3
koncepci	koncepce	k1gFnSc6
trvá	trvat	k5eAaImIp3nS
stimulační	stimulační	k2eAgInSc4d1
</s>
<s>
podnět	podnět	k1gInSc4
krátce	krátce	k6eAd1
(	(	kIx(
<g/>
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
akvizičním	akviziční	k2eAgInSc7d1
časem	čas	k1gInSc7
celého	celý	k2eAgInSc2d1
objemu	objem	k1gInSc2
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
tj.	tj.	kA
3	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
</s>
<s>
sekundami	sekunda	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stimulační	stimulační	k2eAgInPc1d1
podněty	podnět	k1gInPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
od	od	k7c2
sebe	se	k3xPyFc2
vzdáleny	vzdálit	k5eAaPmNgInP
několik	několik	k4yIc4
akvizičních	akviziční	k2eAgFnPc2d1
</s>
<s>
časů	čas	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožňuje	umožňovat	k5eAaImIp3nS
detailní	detailní	k2eAgNnSc4d1
sledování	sledování	k1gNnSc4
průběhu	průběh	k1gInSc2
BOLD	BOLD	kA
signálu	signál	k1gInSc2
v	v	k7c6
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Výsledkem	výsledek	k1gInSc7
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
uspořádání	uspořádání	k1gNnSc2
designu	design	k1gInSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
experiment	experiment	k1gInSc1
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
</s>
<s>
nasnímaných	nasnímaný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
projeví	projevit	k5eAaPmIp3nS
větší	veliký	k2eAgFnSc7d2
výpočetní	výpočetní	k2eAgFnSc7d1
a	a	k8xC
časovou	časový	k2eAgFnSc7d1
náročností	náročnost	k1gFnSc7
</s>
<s>
zpracování	zpracování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
statistické	statistický	k2eAgFnSc2d1
výtěžnosti	výtěžnost	k1gFnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
použít	použít	k5eAaPmF
optimalizace	optimalizace	k1gFnSc1
</s>
<s>
posloupnosti	posloupnost	k1gFnPc1
stimulačních	stimulační	k2eAgInPc2d1
podnětů	podnět	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
pokud	pokud	k8xS
nejsou	být	k5eNaImIp3nP
jednotlivé	jednotlivý	k2eAgFnPc4d1
události	událost	k1gFnPc4
od	od	k7c2
</s>
<s>
sebe	sebe	k3xPyFc4
dostatečně	dostatečně	k6eAd1
vzdáleny	vzdálit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
designu	design	k1gInSc3
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
spíše	spíše	k9
v	v	k7c6
neurovědním	urovědní	k2eNgInSc6d1
</s>
<s>
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
klinické	klinický	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gInSc7
téměř	téměř	k6eAd1
nesetkáme	setkat	k5eNaPmIp1nP
<g/>
.	.	kIx.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1
měření	měření	k1gNnSc1
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1
vyšetření	vyšetření	k1gNnSc1
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
obdobně	obdobně	k6eAd1
jako	jako	k8xC,k8xS
</s>
<s>
konvenční	konvenční	k2eAgNnSc1d1
vyšetření	vyšetření	k1gNnSc1
magnetickou	magnetický	k2eAgFnSc7d1
rezonancí	rezonance	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jedné	jeden	k4xCgFnSc6
fázi	fáze	k1gFnSc6
měření	měření	k1gNnSc2
se	se	k3xPyFc4
sejmou	sejmout	k5eAaPmIp3nP
</s>
<s>
anatomické	anatomický	k2eAgInPc1d1
snímky	snímek	k1gInPc1
ve	v	k7c6
vysokém	vysoký	k2eAgNnSc6d1
rozlišení	rozlišení	k1gNnSc6
(	(	kIx(
<g/>
např.	např.	kA
T1	T1	k1gMnSc1
vážené	vážený	k2eAgInPc4d1
snímky	snímek	k1gInPc4
<g/>
,	,	kIx,
512	#num#	k4
x	x	k?
512	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
matice	matice	k1gFnSc1
pro	pro	k7c4
zobrazení	zobrazení	k1gNnSc4
výsledných	výsledný	k2eAgFnPc2d1
aktivačních	aktivační	k2eAgFnPc2d1
map	mapa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
</s>
<s>
vlastní	vlastní	k2eAgNnSc4d1
funkční	funkční	k2eAgNnSc4d1
vyšetření	vyšetření	k1gNnSc4
dle	dle	k7c2
již	již	k6eAd1
zaběhlého	zaběhlý	k2eAgInSc2d1
designu	design	k1gInSc2
s	s	k7c7
akvizicí	akvizice	k1gFnSc7
</s>
<s>
funkčních	funkční	k2eAgInPc2d1
skenů	sken	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
gradien-echo	gradien-echo	k6eAd1
EPI	EPI	kA
–	–	k?
sekvence	sekvence	k1gFnSc1
dostatečně	dostatečně	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
a	a	k8xC
citlivá	citlivý	k2eAgFnSc1d1
</s>
<s>
k	k	k7c3
nehomogenitám	nehomogenita	k1gFnPc3
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
–	–	k?
T	T	kA
<g/>
2	#num#	k4
<g/>
*	*	kIx~
vážení	vážení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stimulační	stimulační	k2eAgInPc1d1
podněty	podnět	k1gInPc1
a	a	k8xC
</s>
<s>
komunikace	komunikace	k1gFnSc1
s	s	k7c7
pacientem	pacient	k1gMnSc7
probíhají	probíhat	k5eAaImIp3nP
prostřednictvím	prostřednictvím	k7c2
sluchátek	sluchátko	k1gNnPc2
či	či	k8xC
vizuálně	vizuálně	k6eAd1
</s>
<s>
(	(	kIx(
<g/>
projekce	projekce	k1gFnSc2
obrazu	obraz	k1gInSc2
do	do	k7c2
gantry	gantr	k1gInPc4
datovým	datový	k2eAgInSc7d1
projektorem	projektor	k1gInSc7
či	či	k8xC
speciálním	speciální	k2eAgInSc7d1
LCD	LCD	kA
zobrazovačem	zobrazovač	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
fMRI	fMRI	k?
je	být	k5eAaImIp3nS
vyšetření	vyšetření	k1gNnSc1
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
</s>
<s>
nutný	nutný	k2eAgInSc1d1
vysoký	vysoký	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
spolupráce	spolupráce	k1gFnSc2
pacientů	pacient	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správné	správný	k2eAgNnSc1d1
provedení	provedení	k1gNnSc1
daného	daný	k2eAgInSc2d1
úkolu	úkol	k1gInSc2
je	být	k5eAaImIp3nS
</s>
<s>
naprosto	naprosto	k6eAd1
zásadním	zásadní	k2eAgInSc7d1
bodem	bod	k1gInSc7
v	v	k7c6
celém	celý	k2eAgInSc6d1
vyšetřovacím	vyšetřovací	k2eAgInSc6d1
řetězci	řetězec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgMnPc2
</s>
<s>
kognitivních	kognitivní	k2eAgFnPc2d1
úloh	úloha	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
test	test	k1gMnSc1
slovní	slovní	k2eAgFnSc2d1
plynulosti	plynulost	k1gFnSc2
<g/>
)	)	kIx)
navíc	navíc	k6eAd1
nelze	lze	k6eNd1
při	při	k7c6
snímání	snímání	k1gNnSc6
fMRI	fMRI	k?
</s>
<s>
dat	datum	k1gNnPc2
výkon	výkon	k1gInSc1
pacienta	pacient	k1gMnSc4
přímo	přímo	k6eAd1
objektivizovat	objektivizovat	k5eAaBmF
a	a	k8xC
ověřit	ověřit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
zda	zda	k8xS
vykonával	vykonávat	k5eAaImAgMnS
úlohu	úloha	k1gFnSc4
</s>
<s>
správně	správně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akvizice	akvizice	k1gFnSc1
funkčních	funkční	k2eAgInPc2d1
snímků	snímek	k1gInPc2
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
velmi	velmi	k6eAd1
citlivá	citlivý	k2eAgFnSc1d1
k	k	k7c3
pohybovým	pohybový	k2eAgNnPc3d1
</s>
<s>
artefaktům	artefakt	k1gInPc3
(	(	kIx(
<g/>
včetně	včetně	k7c2
minimálních	minimální	k2eAgInPc2d1
pohybů	pohyb	k1gInPc2
hlavy	hlava	k1gFnSc2
při	při	k7c6
polykání	polykání	k1gNnSc6
či	či	k8xC
mrkání	mrkání	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souhrnu	souhrn	k1gInSc6
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
lékař	lékař	k1gMnSc1
indikující	indikující	k2eAgNnSc1d1
vyšetření	vyšetření	k1gNnSc1
fMRI	fMRI	k?
by	by	kYmCp3nP
měl	mít	k5eAaImAgMnS
zodpovědět	zodpovědět	k5eAaPmF
</s>
<s>
několik	několik	k4yIc1
základních	základní	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
od	od	k7c2
vyšetření	vyšetření	k1gNnPc2
očekávám	očekávat	k5eAaImIp1nS
a	a	k8xC
jaký	jaký	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
vliv	vliv	k1gInSc4
na	na	k7c4
další	další	k2eAgNnSc4d1
léčení	léčení	k1gNnSc4
pacienta	pacient	k1gMnSc2
<g/>
?	?	kIx.
</s>
<s>
Jaký	jaký	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
pacientův	pacientův	k2eAgInSc1d1
mentální	mentální	k2eAgInSc1d1
stav	stav	k1gInSc1
–	–	k?
bude	být	k5eAaImBp3nS
schopen	schopit	k5eAaPmNgInS
správně	správně	k6eAd1
vykonat	vykonat	k5eAaPmF
požadovanou	požadovaný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s>
Jaký	jaký	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
pacientův	pacientův	k2eAgInSc1d1
klinický	klinický	k2eAgInSc1d1
stav	stav	k1gInSc1
–	–	k?
bude	být	k5eAaImBp3nS
schopen	schopen	k2eAgMnSc1d1
ležet	ležet	k5eAaImF
cca	cca	kA
20	#num#	k4
minut	minuta	k1gFnPc2
v	v	k7c6
naprostém	naprostý	k2eAgInSc6d1
klidu	klid	k1gInSc6
<g/>
?	?	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
pacient	pacient	k1gMnSc1
na	na	k7c4
medikaci	medikace	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Před	před	k7c7
vyšetřením	vyšetření	k1gNnSc7
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
vysadit	vysadit	k5eAaPmF
sedativně	sedativně	k6eAd1
působící	působící	k2eAgNnPc4d1
farmaka	farmakum	k1gNnPc4
jako	jako	k8xS,k8xC
benzodiazepiny	benzodiazepin	k1gInPc4
či	či	k8xC
barbituráty	barbiturát	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
měření	měření	k1gNnPc2
jsou	být	k5eAaImIp3nP
získaná	získaný	k2eAgNnPc1d1
data	datum	k1gNnPc1
(	(	kIx(
<g/>
funkční	funkční	k2eAgInPc4d1
snímky	snímek	k1gInPc4
<g/>
,	,	kIx,
</s>
<s>
anatomické	anatomický	k2eAgInPc4d1
snímky	snímek	k1gInPc4
<g/>
,	,	kIx,
protokol	protokol	k1gInSc4
o	o	k7c4
měření	měření	k1gNnSc4
<g/>
)	)	kIx)
převedena	převeden	k2eAgFnSc1d1
po	po	k7c6
síti	síť	k1gFnSc6
z	z	k7c2
vlastního	vlastní	k2eAgNnSc2d1
MR	MR	kA
</s>
<s>
přístroje	přístroj	k1gInPc1
či	či	k8xC
centrálního	centrální	k2eAgInSc2d1
serveru	server	k1gInSc2
(	(	kIx(
<g/>
PACS	PACS	kA
<g/>
)	)	kIx)
na	na	k7c4
místo	místo	k1gNnSc4
dalšího	další	k2eAgNnSc2d1
zpracování	zpracování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předzpracování	předzpracování	k1gNnSc1
<g/>
,	,	kIx,
statistická	statistický	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
a	a	k8xC
vyhodnocení	vyhodnocení	k1gNnSc1
</s>
<s>
výsledků	výsledek	k1gInPc2
již	již	k9
většinou	většinou	k6eAd1
probíhá	probíhat	k5eAaImIp3nS
mimo	mimo	k7c4
vlastní	vlastní	k2eAgNnSc4d1
MR	MR	kA
pracoviště	pracoviště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
</s>
<s>
doba	doba	k1gFnSc1
zpracování	zpracování	k1gNnSc2
u	u	k7c2
jednoduché	jednoduchý	k2eAgFnSc2d1
blokového	blokový	k1gMnSc2
designu	design	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
případě	případ	k1gInSc6
klinických	klinický	k2eAgFnPc2d1
</s>
<s>
měření	měření	k1gNnSc1
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
jedné	jeden	k4xCgFnSc2
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInPc1d1
MR	MR	kA
tomografy	tomograf	k1gInPc1
již	již	k9
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vybaveny	vybavit	k5eAaPmNgInP
</s>
<s>
softwarem	software	k1gInSc7
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
fMRI	fMRI	k?
<g/>
.	.	kIx.
</s>
<s>
Statistická	statistický	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
-	-	kIx~
voxel-by-voxel	voxel-by-voxel	k1gInSc1
detekční	detekční	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
</s>
<s>
Ke	k	k7c3
statistické	statistický	k2eAgFnSc3d1
detekci	detekce	k1gFnSc3
lze	lze	k6eAd1
použít	použít	k5eAaPmF
následující	následující	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Prostá	prostý	k2eAgFnSc1d1
subtrakce	subtrakce	k?
(	(	kIx(
<g/>
prostý	prostý	k2eAgInSc1d1
rozdíl	rozdíl	k1gInSc1
průměrné	průměrný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
signálu	signál	k1gInSc2
získaného	získaný	k2eAgInSc2d1
v	v	k7c6
době	doba	k1gFnSc6
aktivity	aktivita	k1gFnSc2
a	a	k8xC
průměrné	průměrný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
signálu	signál	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
klidu	klid	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Studentův	studentův	k2eAgInSc1d1
t-test	t-test	k1gInSc1
(	(	kIx(
<g/>
rozdíl	rozdíl	k1gInSc1
průměru	průměr	k1gInSc2
je	být	k5eAaImIp3nS
vážen	vážit	k5eAaImNgInS
směrodatnou	směrodatný	k2eAgFnSc7d1
odchylkou	odchylka	k1gFnSc7
</s>
<s>
Korelační	korelační	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
</s>
<s>
Regresní	regresní	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
</s>
<s>
Analýza	analýza	k1gFnSc1
rozptylu	rozptyl	k1gInSc2
</s>
<s>
GLM	GLM	kA
(	(	kIx(
<g/>
Obecné	obecný	k2eAgNnSc1d1
lineární	lineární	k2eAgNnSc1d1
modelování	modelování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Jistým	jistý	k2eAgNnSc7d1
zobecněním	zobecnění	k1gNnSc7
a	a	k8xC
zapouzdřením	zapouzdření	k1gNnSc7
výše	vysoce	k6eAd2
uvedených	uvedený	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
obecného	obecný	k2eAgInSc2d1
lineárního	lineární	k2eAgInSc2d1
modelu	model	k1gInSc2
(	(	kIx(
<g/>
GLM	GLM	kA
=	=	kIx~
General	General	k1gFnSc1
Linear	Linear	k1gMnSc1
model	model	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
vlastně	vlastně	k9
o	o	k7c4
určité	určitý	k2eAgNnSc4d1
zobecnění	zobecnění	k1gNnSc4
lineární	lineární	k2eAgFnSc2d1
regresní	regresní	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
dle	dle	k7c2
způsobu	způsob	k1gInSc2
sestavení	sestavení	k1gNnSc2
modelu	model	k1gInSc2
a	a	k8xC
následného	následný	k2eAgNnSc2d1
testování	testování	k1gNnSc2
a	a	k8xC
interpretace	interpretace	k1gFnSc2
regresních	regresní	k2eAgInPc2d1
koeficientů	koeficient	k1gInPc2
z	z	k7c2
něj	on	k3xPp3gInSc2
získáme	získat	k5eAaPmIp1nP
např.	např.	kA
t-test	t-test	k1gInSc4
nebo	nebo	k8xC
ANOVu	ANOVa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
je	být	k5eAaImIp3nS
znázorněna	znázornit	k5eAaPmNgFnS
na	na	k7c6
následujícím	následující	k2eAgInSc6d1
obrázku	obrázek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
DCM	DCM	kA
(	(	kIx(
<g/>
Dynamické	dynamický	k2eAgNnSc1d1
kauzální	kauzální	k2eAgNnSc1d1
modelování	modelování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
K	k	k7c3
modelování	modelování	k1gNnSc3
vnitřní	vnitřní	k2eAgFnSc2d1
dynamiky	dynamika	k1gFnSc2
používá	používat	k5eAaImIp3nS
DCM	DCM	kA
stavový	stavový	k2eAgInSc4d1
popis	popis	k1gInSc4
systému	systém	k1gInSc2
</s>
<s>
Vstupem	vstup	k1gInSc7
jsou	být	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
experimentální	experimentální	k2eAgInPc1d1
stimuly	stimul	k1gInPc1
(	(	kIx(
<g/>
psychologické	psychologický	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Výstupem	výstup	k1gInSc7
jsou	být	k5eAaImIp3nP
časové	časový	k2eAgInPc1d1
průběhy	průběh	k1gInPc1
měřeného	měřený	k2eAgInSc2d1
signálu	signál	k1gInSc2
</s>
<s>
Vnitřními	vnitřní	k2eAgInPc7d1
stavy	stav	k1gInPc7
jsou	být	k5eAaImIp3nP
vlastní	vlastní	k2eAgInPc1d1
stavy	stav	k1gInPc1
neurálních	neurální	k2eAgFnPc2d1
populací	populace	k1gFnPc2
</s>
<s>
Koncepce	koncepce	k1gFnSc1
DCM	DCM	kA
uvažuje	uvažovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc4
vstupů	vstup	k1gInPc2
</s>
<s>
Přímý	přímý	k2eAgInSc1d1
nebo	nebo	k8xC
také	také	k9
řídící	řídící	k2eAgInSc4d1
vstup	vstup	k1gInSc4
ovlivňuje	ovlivňovat	k5eAaImIp3nS
přímo	přímo	k6eAd1
neurální	neurální	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
primárních	primární	k2eAgFnPc6d1
senzorických	senzorický	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Kontextuální	kontextuální	k2eAgInSc1d1
vstup	vstup	k1gInSc1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
sílu	síla	k1gFnSc4
vazeb	vazba	k1gFnPc2
mezi	mezi	k7c7
oblastmi	oblast	k1gFnPc7
(	(	kIx(
<g/>
např.	např.	kA
změna	změna	k1gFnSc1
pozornosti	pozornost	k1gFnSc2
<g/>
,	,	kIx,
efekt	efekt	k1gInSc4
učení	učení	k1gNnSc2
<g/>
,	,	kIx,
zpracování	zpracování	k1gNnSc2
odlišné	odlišný	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
podnětu	podnět	k1gInSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
použití	použití	k1gNnSc6
DCM	DCM	kA
můžeme	moct	k5eAaImIp1nP
testovat	testovat	k5eAaImF
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc4
hypotéz	hypotéza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sledujeme	sledovat	k5eAaImIp1nP
významnost	významnost	k1gFnSc4
konkrétních	konkrétní	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
(	(	kIx(
<g/>
vyjádřenou	vyjádřený	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
aposteriorní	aposteriorní	k2eAgFnSc2d1
pravděpodobnosti	pravděpodobnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srovnáváme	srovnávat	k5eAaImIp1nP
vhodnost	vhodnost	k1gFnSc4
různých	různý	k2eAgInPc2d1
modelů	model	k1gInPc2
lišících	lišící	k2eAgInPc2d1
se	s	k7c7
strukturou	struktura	k1gFnSc7
</s>
<s>
povolených	povolený	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
a	a	k8xC
z	z	k7c2
nich	on	k3xPp3gMnPc2
vybíráme	vybírat	k5eAaImIp1nP
pomocí	pomocí	k7c2
Bayesovského	Bayesovský	k2eAgInSc2d1
výběru	výběr	k1gInSc2
(	(	kIx(
<g/>
BMS	BMS	kA
<g/>
,	,	kIx,
Bayesian	Bayesian	k1gInSc4
model	model	k1gInSc4
selection	selection	k1gInSc4
<g/>
)	)	kIx)
nejvhodnější	vhodný	k2eAgInSc4d3
model	model	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Výsledek	výsledek	k1gInSc1
voxel-by-voxel	voxel-by-voxela	k1gFnPc2
detekční	detekční	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
získáváme	získávat	k5eAaImIp1nP
statistickou	statistický	k2eAgFnSc4d1
parametrickou	parametrický	k2eAgFnSc4d1
mapu	mapa	k1gFnSc4
(	(	kIx(
<g/>
SPM	SPM	kA
<g/>
=	=	kIx~
statistical	statisticat	k5eAaPmAgMnS
parametric	parametric	k1gMnSc1
map	mapa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
provedení	provedení	k1gNnSc6
prahování	prahování	k1gNnSc2
(	(	kIx(
<g/>
pro	pro	k7c4
každý	každý	k3xTgInSc4
bod	bod	k1gInSc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c4
významnosti	významnost	k1gFnPc4
či	či	k8xC
nevýznamnosti	nevýznamnost	k1gFnPc4
statistické	statistický	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
)	)	kIx)
mapy	mapa	k1gFnPc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
získána	získán	k2eAgFnSc1d1
výsledná	výsledný	k2eAgFnSc1d1
mapu	mapa	k1gFnSc4
detekovaných	detekovaný	k2eAgFnPc2d1
aktivací	aktivace	k1gFnPc2
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
hodnocení	hodnocení	k1gNnSc3
výsledku	výsledek	k1gInSc2
experimentu	experiment	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aplikace	aplikace	k1gFnSc1
v	v	k7c6
klinické	klinický	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
indikací	indikace	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
lze	lze	k6eAd1
fMRI	fMRI	k?
s	s	k7c7
úspěchem	úspěch	k1gInSc7
využít	využít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
například	například	k6eAd1
předoperační	předoperační	k2eAgNnSc1d1
plánování	plánování	k1gNnSc1
resekcí	resekce	k1gFnPc2
u	u	k7c2
expanzivních	expanzivní	k2eAgInPc2d1
nitrolebních	nitrolební	k2eAgInPc2d1
procesů	proces	k1gInPc2
např.	např.	kA
u	u	k7c2
tumoru	tumor	k1gInSc2
či	či	k8xC
AV	AV	kA
malformace	malformace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
doménou	doména	k1gFnSc7
fMRI	fMRI	k?
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
epileptochirurgie	epileptochirurgie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
pacientů	pacient	k1gMnPc2
s	s	k7c7
farmakorezistentní	farmakorezistentní	k2eAgFnSc7d1
epilepsií	epilepsie	k1gFnSc7
temporálního	temporální	k2eAgInSc2d1
laloku	lalok	k1gInSc2
(	(	kIx(
<g/>
TLE	TLE	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
často	často	k6eAd1
indikována	indikován	k2eAgFnSc1d1
parciální	parciální	k2eAgFnSc1d1
resekce	resekce	k1gFnSc1
v	v	k7c6
postiženém	postižený	k2eAgInSc6d1
temporálním	temporální	k2eAgInSc6d1
laloku	lalok	k1gInSc6
<g/>
.	.	kIx.
fMRI	fMRI	k?
se	se	k3xPyFc4
nabízí	nabízet	k5eAaImIp3nS
jako	jako	k9
neinvazivní	invazivní	k2eNgFnSc1d1
alternativa	alternativa	k1gFnSc1
WADA	WADA	kA
testu	test	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
NAVRÁTIL	Navrátil	k1gMnSc1
<g/>
,	,	kIx,
Leoš	Leoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Medicínská	medicínský	k2eAgFnSc1d1
biofyzika	biofyzika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
,	,	kIx,
524	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1152	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KASSIN	KASSIN	kA
<g/>
,	,	kIx,
Saul	Saul	k1gMnSc1
M.	M.	kA
Psychologie	psychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Computer	computer	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
xxiii	xxiie	k1gFnSc6
<g/>
,	,	kIx,
771	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
251	#num#	k4
<g/>
-	-	kIx~
<g/>
1716	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VINAŘ	vinař	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budeme	být	k5eAaImBp1nP
číst	číst	k5eAaImF
myšlenky	myšlenka	k1gFnPc4
v	v	k7c6
mozku	mozek	k1gInSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
Vesmír	vesmír	k1gInSc1
87	#num#	k4
<g/>
,	,	kIx,
442	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.vesmir.cz/clanky/clanek/id/7809	http://www.vesmir.cz/clanky/clanek/id/7809	k4
</s>
<s>
SEDLÁŘ	Sedlář	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
Erik	Erik	k1gMnSc1
STAFFA	STAFFA	kA
a	a	k8xC
Vojtěch	Vojtěch	k1gMnSc1
MORNSTEIN	MORNSTEIN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zobrazovací	zobrazovací	k2eAgFnPc4d1
metody	metoda	k1gFnPc4
využívající	využívající	k2eAgFnSc4d1
neionizující	ionizující	k2eNgNnSc1d1
záření	záření	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Biofyzikální	biofyzikální	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
Dostupné	dostupný	k2eAgNnSc1d1
z	z	k7c2
http://www.med.muni.cz/biofyz/zobrazovacimetody/files/zobrazovaci_metody.pdf	http://www.med.muni.cz/biofyz/zobrazovacimetody/files/zobrazovaci_metody.pdf	k1gInSc1
</s>
<s>
CHLEBUS	CHLEBUS	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
MIKL	miknout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
BRÁZDIL	brázdit	k5eAaImAgMnS
a	a	k8xC
Petr	Petr	k1gMnSc1
KRUPA	krupa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkční	funkční	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
rezonance	rezonance	k1gFnSc1
<g/>
:	:	kIx,
Úvod	úvod	k1gInSc1
do	do	k7c2
problematiky	problematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neurologie	neurologie	k1gFnSc1
pro	pro	k7c4
praxi	praxe	k1gFnSc4
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
z	z	k7c2
</s>
<s>
http://fmri.mchmi.com/articles/chlebus_prehled.pdf	http://fmri.mchmi.com/articles/chlebus_prehled.pdf	k1gMnSc1
</s>
<s>
LF	LF	kA
MU	MU	kA
BRNO	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FMRI	FMRI	kA
Brno	Brno	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://fmri.mchmi.com	http://fmri.mchmi.com	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7618675-1	7618675-1	k4
</s>
