<s>
Funkční	funkční	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
rezonance	rezonance	k1gFnSc1
(	(	kIx(
<g/>
fMRI	fMRI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc1d1
zobrazovací	zobrazovací	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
sloužící	sloužící	k2eAgFnSc1d1
k	k	k7c3
funkčnímu	funkční	k2eAgNnSc3d1
zobrazování	zobrazování	k1gNnSc3
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
resp.	resp.	kA
mapování	mapování	k1gNnSc2
mozkové	mozkový	k2eAgFnSc2d1
odezvy	odezva	k1gFnSc2
na	na	k7c4
vnější	vnější	k2eAgInSc4d1
či	či	k8xC
vnitřní	vnitřní	k2eAgInSc4d1
podnět	podnět	k1gInSc4
<g/>
.	.	kIx.
</s>