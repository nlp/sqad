<s>
SMS	SMS	kA
marketing	marketing	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
SMS	SMS	kA
marketing	marketing	k1gInSc4
v	v	k7c6
sobě	sebe	k3xPyFc6
zahrnuje	zahrnovat	k5eAaImIp3nS
odesílání	odesílání	k1gNnSc4
textových	textový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
jak	jak	k8xS,k8xC
v	v	k7c6
podobě	podoba	k1gFnSc6
transakčních	transakční	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
zpráv	zpráva	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
marketingové	marketingový	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SMS	SMS	kA
zprávy	zpráva	k1gFnSc2
v	v	k7c6
sobě	sebe	k3xPyFc6
nejčastěji	často	k6eAd3
obsahují	obsahovat	k5eAaImIp3nP
informace	informace	k1gFnPc4
o	o	k7c6
časově	časově	k6eAd1
omezených	omezený	k2eAgFnPc6d1
nabídkách	nabídka	k1gFnPc6
<g/>
,	,	kIx,
upozorněních	upozornění	k1gNnPc6
<g/>
,	,	kIx,
aktualizacích	aktualizace	k1gFnPc6
aj.	aj.	kA
všem	všecek	k3xTgFnPc3
osobám	osoba	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dobrovolně	dobrovolně	k6eAd1
rozhodli	rozhodnout	k5eAaPmAgMnP
přijímat	přijímat	k5eAaImF
tyto	tento	k3xDgFnPc4
zprávy	zpráva	k1gFnPc4
od	od	k7c2
odesílatele	odesílatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
SMS	SMS	kA
marketing	marketing	k1gInSc1
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
například	například	k6eAd1
pro	pro	k7c4
e-commerce	e-commerka	k1gFnSc6
obchody	obchod	k1gInPc4
<g/>
,	,	kIx,
cestovní	cestovní	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
<g/>
,	,	kIx,
firmy	firma	k1gFnSc2
nabízející	nabízející	k2eAgFnSc2d1
služby	služba	k1gFnSc2
vyžadující	vyžadující	k2eAgFnSc2d1
schůzky	schůzka	k1gFnSc2
s	s	k7c7
klienty	klient	k1gMnPc7
a	a	k8xC
velké	velký	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
se	se	k3xPyFc4
100	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
zaměstnanci	zaměstnanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Fungování	fungování	k1gNnSc2
hromadných	hromadný	k2eAgInPc2d1
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
</s>
<s>
Hromadné	hromadný	k2eAgFnPc4d1
SMS	SMS	kA
zprávy	zpráva	k1gFnPc4
se	se	k3xPyFc4
odesílají	odesílat	k5eAaImIp3nP
prostřednictvím	prostřednictvím	k7c2
SMS	SMS	kA
brány	brána	k1gFnSc2
subjektů	subjekt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
tuto	tento	k3xDgFnSc4
službu	služba	k1gFnSc4
nabízí	nabízet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odesílatel	odesílatel	k1gMnSc1
se	se	k3xPyFc4
zaregistruje	zaregistrovat	k5eAaPmIp3nS
k	k	k7c3
vybranému	vybraný	k2eAgInSc3d1
subjektu	subjekt	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
mu	on	k3xPp3gMnSc3
umožní	umožnit	k5eAaPmIp3nS
přístup	přístup	k1gInSc1
do	do	k7c2
služby	služba	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
<g/>
,	,	kIx,
níž	jenž	k3xRgFnSc7
SMS	SMS	kA
zprávy	zpráva	k1gFnPc1
vytvoří	vytvořit	k5eAaPmIp3nP
<g/>
,	,	kIx,
importuje	importovat	k5eAaBmIp3nS
kontakty	kontakt	k1gInPc4
buď	buď	k8xC
manuálně	manuálně	k6eAd1
nebo	nebo	k8xC
je	on	k3xPp3gFnPc4
propojí	propojit	k5eAaPmIp3nS
s	s	k7c7
interní	interní	k2eAgFnSc7d1
databází	databáze	k1gFnSc7
prostřednictvím	prostřednictvím	k7c2
API	API	kA
a	a	k8xC
vytvořené	vytvořený	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
následně	následně	k6eAd1
odešle	odešle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SMS	SMS	kA
zprávy	zpráva	k1gFnSc2
lze	lze	k6eAd1
odesílat	odesílat	k5eAaImF
jednorázově	jednorázově	k6eAd1
i	i	k9
v	v	k7c6
pravidelných	pravidelný	k2eAgInPc6d1
intervalech	interval	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
typy	typ	k1gInPc1
zpráv	zpráva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
lze	lze	k6eAd1
rozesílat	rozesílat	k5eAaImF
v	v	k7c6
rámci	rámec	k1gInSc6
SMS	SMS	kA
marketingu	marketing	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
SMS	SMS	kA
kampaň	kampaň	k1gFnSc1
<g/>
:	:	kIx,
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
hromadné	hromadný	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
tzv.	tzv.	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
ke	k	k7c3
komunikaci	komunikace	k1gFnSc3
propagačních	propagační	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
(	(	kIx(
<g/>
kupónů	kupón	k1gInPc2
<g/>
,	,	kIx,
slev	sleva	k1gFnPc2
<g/>
,	,	kIx,
časově	časově	k6eAd1
omezených	omezený	k2eAgFnPc2d1
nabídek	nabídka	k1gFnPc2
<g/>
,	,	kIx,
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
je	být	k5eAaImIp3nS
využít	využít	k5eAaPmF
i	i	k9
k	k	k7c3
rozesílání	rozesílání	k1gNnSc3
obecných	obecný	k2eAgFnPc2d1
informací	informace	k1gFnPc2
(	(	kIx(
<g/>
aktualizace	aktualizace	k1gFnSc1
<g/>
,	,	kIx,
upozornění	upozornění	k1gNnSc1
<g/>
,	,	kIx,
aj.	aj.	kA
<g/>
)	)	kIx)
</s>
<s>
Transakční	transakční	k2eAgFnSc1d1
SMS	SMS	kA
<g/>
:	:	kIx,
Zprávy	zpráva	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
vyvolány	vyvolat	k5eAaPmNgFnP
na	na	k7c6
základě	základ	k1gInSc6
určité	určitý	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
potvrzení	potvrzení	k1gNnSc4
objednávky	objednávka	k1gFnSc2
<g/>
,	,	kIx,
oznámení	oznámení	k1gNnSc1
o	o	k7c6
přepravě	přeprava	k1gFnSc6
zakoupeného	zakoupený	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
zprávy	zpráva	k1gFnPc4
se	se	k3xPyFc4
odesílají	odesílat	k5eAaImIp3nP
automatizovaně	automatizovaně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Krátkodobá	krátkodobý	k2eAgFnSc1d1
propagace	propagace	k1gFnSc1
pro	pro	k7c4
maloobchod	maloobchod	k1gInSc4
nebo	nebo	k8xC
e-commerce	e-commerka	k1gFnSc6
podniky	podnik	k1gInPc4
(	(	kIx(
<g/>
propagační	propagační	k2eAgFnSc1d1
akce	akce	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
jeden	jeden	k4xCgInSc4
ze	z	k7c2
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
zvýšit	zvýšit	k5eAaPmF
poptávku	poptávka	k1gFnSc4
po	po	k7c6
nabízených	nabízený	k2eAgInPc6d1
produktech	produkt	k1gInPc6
či	či	k8xC
službách	služba	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
objednávkách	objednávka	k1gFnPc6
(	(	kIx(
<g/>
komunikace	komunikace	k1gFnSc1
stavu	stav	k1gInSc2
objednávek	objednávka	k1gFnPc2
<g/>
,	,	kIx,
obecné	obecný	k2eAgInPc1d1
informace	informace	k1gFnPc4
nebo	nebo	k8xC
aktualizaci	aktualizace	k1gFnSc4
objednávek	objednávka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Připomínky	připomínka	k1gFnPc4
důležitých	důležitý	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
(	(	kIx(
<g/>
připomenutí	připomenutí	k1gNnSc2
plánovaných	plánovaný	k2eAgInPc2d1
termínů	termín	k1gInPc2
schůzek	schůzka	k1gFnPc2
či	či	k8xC
firemních	firemní	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Interní	interní	k2eAgNnSc1d1
upozornění	upozornění	k1gNnSc1
(	(	kIx(
<g/>
informace	informace	k1gFnPc1
o	o	k7c4
uzavření	uzavření	k1gNnSc4
kanceláří	kancelář	k1gFnPc2
<g/>
,	,	kIx,
aktualizacích	aktualizace	k1gFnPc6
systému	systém	k1gInSc2
<g/>
,	,	kIx,
aj.	aj.	kA
zaměstnancům	zaměstnanec	k1gMnPc3
společnosti	společnost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výhody	výhoda	k1gFnPc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
míra	míra	k1gFnSc1
a	a	k8xC
všudypřítomnost	všudypřítomnost	k1gFnSc1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Spolehlivost	spolehlivost	k1gFnSc1
doručení	doručení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přímé	přímý	k2eAgNnSc1d1
oslovení	oslovení	k1gNnSc1
koncového	koncový	k2eAgMnSc2d1
zákazníka	zákazník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Doplnění	doplnění	k1gNnSc1
nebo	nebo	k8xC
nahrazení	nahrazení	k1gNnSc1
e-mail	e-mail	k1gInSc4
kampaní	kampaň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
efektivita	efektivita	k1gFnSc1
doručení	doručení	k1gNnSc4
oproti	oproti	k7c3
jiným	jiný	k2eAgInPc3d1
marketingovým	marketingový	k2eAgInPc3d1
kanálům	kanál	k1gInPc3
<g/>
.	.	kIx.
</s>
