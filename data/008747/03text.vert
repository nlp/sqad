<p>
<s>
Emil	Emil	k1gMnSc1	Emil
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
o	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Émile	Émile	k1gFnSc1	Émile
ou	ou	k0	ou
De	De	k?	De
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
éducation	éducation	k1gInSc1	éducation
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pojednání	pojednání	k1gNnSc1	pojednání
Jeana-Jacquese	Jeana-Jacquese	k1gFnSc2	Jeana-Jacquese
Rousseaua	Rousseau	k1gMnSc2	Rousseau
o	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
umění	umění	k1gNnSc4	umění
formování	formování	k1gNnSc2	formování
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Spis	spis	k1gInSc1	spis
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1762	[number]	k4	1762
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejčtenější	čtený	k2eAgFnSc4d3	Nejčtenější
a	a	k8xC	a
nejpopulárnější	populární	k2eAgFnSc2d3	nejpopulárnější
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
dokonce	dokonce	k9	dokonce
úřady	úřad	k1gInPc1	úřad
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnPc1	mládež
všem	všecek	k3xTgFnPc3	všecek
mateřským	mateřský	k2eAgFnPc3d1	mateřská
školám	škola	k1gFnPc3	škola
čtení	čtení	k1gNnSc2	čtení
Emila	Emil	k1gMnSc2	Emil
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
spisu	spis	k1gInSc2	spis
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
Dopisy	dopis	k1gInPc7	dopis
synovi	syn	k1gMnSc3	syn
/	/	kIx~	/
Lettres	Lettres	k1gMnSc1	Lettres
à	à	k?	à
mon	mon	k?	mon
fils	fils	k1gInSc1	fils
Mme	Mme	k1gMnSc1	Mme
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Épinay	Épinaa	k1gFnPc1	Épinaa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
napsány	napsat	k5eAaPmNgFnP	napsat
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
čtyři	čtyři	k4xCgFnPc1	čtyři
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
řazené	řazený	k2eAgMnPc4d1	řazený
chronologicky	chronologicky	k6eAd1	chronologicky
a	a	k8xC	a
popisují	popisovat	k5eAaImIp3nP	popisovat
ideální	ideální	k2eAgFnSc4d1	ideální
výchovu	výchova	k1gFnSc4	výchova
mladého	mladý	k2eAgMnSc4d1	mladý
fiktivního	fiktivní	k2eAgMnSc4d1	fiktivní
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
,	,	kIx,	,
Emila	Emil	k1gMnSc4	Emil
(	(	kIx(	(
<g/>
Émile	Émile	k1gNnSc2	Émile
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
popisují	popisovat	k5eAaImIp3nP	popisovat
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
<g/>
,	,	kIx,	,
výchovné	výchovný	k2eAgFnPc1d1	výchovná
otázky	otázka	k1gFnPc1	otázka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
postupně	postupně	k6eAd1	postupně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mladík	mladík	k1gMnSc1	mladík
roste	růst	k5eAaImIp3nS	růst
a	a	k8xC	a
dospívá	dospívat	k5eAaImIp3nS	dospívat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
kniha	kniha	k1gFnSc1	kniha
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
"	"	kIx"	"
<g/>
výchově	výchova	k1gFnSc6	výchova
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
přesněji	přesně	k6eAd2	přesně
o	o	k7c4	o
absenci	absence	k1gFnSc4	absence
výchovy	výchova	k1gFnSc2	výchova
u	u	k7c2	u
dívek	dívka	k1gFnPc2	dívka
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
další	další	k2eAgFnSc2d1	další
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
postavy	postava	k1gFnSc2	postava
Sophie	Sophie	k1gFnSc1	Sophie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
vychovávána	vychováván	k2eAgFnSc1d1	vychovávána
jako	jako	k8xC	jako
budoucí	budoucí	k2eAgFnSc1d1	budoucí
manželka	manželka	k1gFnSc1	manželka
Emila	Emil	k1gMnSc2	Emil
<g/>
.	.	kIx.	.
</s>
<s>
Rousseau	Rousseau	k1gMnSc1	Rousseau
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
odmítá	odmítat	k5eAaImIp3nS	odmítat
výchovu	výchova	k1gFnSc4	výchova
mladých	mladý	k2eAgFnPc2d1	mladá
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
zastává	zastávat	k5eAaImIp3nS	zastávat
velmi	velmi	k6eAd1	velmi
sexistický	sexistický	k2eAgInSc4d1	sexistický
postoj	postoj	k1gInSc4	postoj
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
vytrženo	vytržen	k2eAgNnSc1d1	vytrženo
z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
analýza	analýza	k1gFnSc1	analýza
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
své	svůj	k3xOyFgFnSc3	svůj
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
místu	místo	k1gNnSc3	místo
i	i	k8xC	i
roli	role	k1gFnSc3	role
udělené	udělený	k2eAgFnSc2d1	udělená
ženám	žena	k1gFnPc3	žena
<g/>
)	)	kIx)	)
o	o	k7c6	o
roli	role	k1gFnSc6	role
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
k	k	k7c3	k
obrazu	obraz	k1gInSc3	obraz
jejich	jejich	k3xOp3gFnSc2	jejich
role	role	k1gFnSc2	role
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
