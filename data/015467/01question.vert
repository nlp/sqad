<s>
Jaká	jaký	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
vesmírnou	vesmírný	k2eAgFnSc4d1
sondu	sonda	k1gFnSc4
Explorer	Explorer	k1gInSc1
91	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
vypuštěna	vypustit	k5eAaPmNgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2008	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
sluneční	sluneční	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
a	a	k8xC
mezihvězdným	mezihvězdný	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
?	?	kIx.
</s>