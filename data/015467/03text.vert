<s>
IBEX	IBEX	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
probíhající	probíhající	k2eAgFnSc7d1
vesmírnou	vesmírný	k2eAgFnSc7d1
misí	mise	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Informace	informace	k1gFnPc1
zde	zde	k6eAd1
uvedené	uvedený	k2eAgInPc4d1
se	s	k7c7
vzhledem	vzhled	k1gInSc7
k	k	k7c3
neustálému	neustálý	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
mohou	moct	k5eAaImIp3nP
průběžně	průběžně	k6eAd1
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
je	být	k5eAaImIp3nS
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
aktualizovat	aktualizovat	k5eAaBmF
a	a	k8xC
doplňovat	doplňovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
IBEX	IBEX	kA
Jiné	jiný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Interstellar	Interstellar	k1gMnSc1
Boundary	Boundara	k1gFnSc2
ExplorerExplorer	ExplorerExplorer	k1gInSc1
91	#num#	k4
COSPAR	COSPAR	kA
</s>
<s>
2008-051A	2008-051A	k4
Katalogové	katalogový	k2eAgInPc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
33401	#num#	k4
Start	start	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Kwajalein	Kwajalein	k1gInSc1
Nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
Pegasus	Pegasus	k1gMnSc1
XL	XL	kA
Stav	stav	k1gInSc1
objektu	objekt	k1gInSc2
</s>
<s>
aktivní	aktivní	k2eAgNnSc1d1
Trvání	trvání	k1gNnSc1
mise	mise	k1gFnSc2
</s>
<s>
12	#num#	k4
let	let	k1gInSc4
a	a	k8xC
175	#num#	k4
dníplán	dníplán	k2eAgInSc4d1
2	#num#	k4
roky	rok	k1gInPc1
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
Southwest	Southwest	k1gMnSc1
Research	Research	k1gMnSc1
Institute	institut	k1gInSc5
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Orbital	orbital	k1gInSc1
Science	Science	k1gFnSc2
CorporationLockheed	CorporationLockheed	k1gMnSc1
MartinLos	MartinLos	k1gMnSc1
Alamos	Alamos	k1gMnSc1
National	National	k1gMnSc3
Laboratory	Laborator	k1gMnPc4
Druh	druh	k1gInSc4
</s>
<s>
vědecká	vědecký	k2eAgFnSc1d1
družice	družice	k1gFnSc1
Program	program	k1gInSc1
</s>
<s>
Explorer	Explorer	k1gInSc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
110	#num#	k4
kg	kg	kA
Parametry	parametr	k1gInPc1
dráhy	dráha	k1gFnSc2
Apogeum	apogeum	k1gNnSc5
</s>
<s>
220	#num#	k4
886	#num#	k4
km	km	kA
Perigeum	perigeum	k1gNnSc1
</s>
<s>
7	#num#	k4
000	#num#	k4
km	km	kA
Sklon	sklon	k1gInSc4
dráhy	dráha	k1gFnSc2
</s>
<s>
10,99	10,99	k4
°	°	k?
Doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
</s>
<s>
6	#num#	k4
604	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Epocha	epocha	k1gFnSc1
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
</s>
<s>
Centrální	centrální	k2eAgFnSc3d1
tělesoZemě	tělesoZema	k1gFnSc3
</s>
<s>
Apogeum	apogeum	k1gNnSc1
<g/>
6249	#num#	k4
km	km	kA
</s>
<s>
Perigeum	perigeum	k1gNnSc1
<g/>
304274	#num#	k4
km	km	kA
</s>
<s>
Sklon	sklon	k1gInSc1
dráhy	dráha	k1gFnSc2
<g/>
25,52	25,52	k4
°	°	k?
</s>
<s>
Doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
<g/>
10779	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
Aktuální	aktuální	k2eAgFnPc1d1
poziceheavens-above	poziceheavens-abov	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
http://www.ibex.swri.edu/	http://www.ibex.swri.edu/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
IBEX	IBEX	kA
(	(	kIx(
<g/>
Interstellar	Interstellar	k1gMnSc1
Boundary	Boundara	k1gFnSc2
Explorer	Explorer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
Explorer	Explorer	k1gInSc1
91	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vesmírná	vesmírný	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
NASA	NASA	kA
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
vypuštěna	vypustit	k5eAaPmNgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2008	#num#	k4
a	a	k8xC
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
sluneční	sluneční	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
a	a	k8xC
mezihvězdným	mezihvězdný	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Úkoly	úkol	k1gInPc1
mise	mise	k1gFnSc2
</s>
<s>
Sonda	sonda	k1gFnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
starší	starší	k1gMnPc4
mise	mise	k1gFnSc2
Voyager	Voyager	k1gInSc4
1	#num#	k4
a	a	k8xC
Voyager	Voyager	k1gInSc1
2	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
plánované	plánovaný	k2eAgFnSc2d1
dvouleté	dvouletý	k2eAgFnSc2d1
mise	mise	k1gFnSc2
měla	mít	k5eAaImAgFnS
předávat	předávat	k5eAaImF
informace	informace	k1gFnPc4
z	z	k7c2
oblastí	oblast	k1gFnPc2
na	na	k7c6
pokraji	pokraj	k1gInSc6
Sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
hledat	hledat	k5eAaImF
vlastnosti	vlastnost	k1gFnPc4
případného	případný	k2eAgInSc2d1
ochranného	ochranný	k2eAgInSc2d1
štítu	štít	k1gInSc2
před	před	k7c7
nebezpečným	bezpečný	k2eNgNnSc7d1
zářením	záření	k1gNnSc7
z	z	k7c2
dalších	další	k2eAgFnPc2d1
částí	část	k1gFnPc2
galaxie	galaxie	k1gFnSc2
Mléčné	mléčný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
totiž	totiž	k9
protitlak	protitlak	k1gInSc4
slunečního	sluneční	k2eAgInSc2d1
větru	vítr	k1gInSc2
velmi	velmi	k6eAd1
nízký	nízký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
obou	dva	k4xCgFnPc2
misí	mise	k1gFnPc2
Voyagerů	Voyager	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
míří	mířit	k5eAaImIp3nP
mimo	mimo	k7c4
Sluneční	sluneční	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
IBEX	IBEX	kA
nevzdálí	vzdálit	k5eNaPmIp3nS
od	od	k7c2
Země	zem	k1gFnSc2
dále	daleko	k6eAd2
než	než	k8xS
na	na	k7c4
tří-čtvrtinovou	tří-čtvrtinový	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapování	mapování	k1gNnSc1
hranice	hranice	k1gFnSc2
heliosféry	heliosféra	k1gFnSc2
je	být	k5eAaImIp3nS
prováděno	provádět	k5eAaImNgNnS
pomocí	pomocí	k7c2
takzvaných	takzvaný	k2eAgInPc2d1
energetických	energetický	k2eAgInPc2d1
neutrálních	neutrální	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vznikají	vznikat	k5eAaImIp3nP
při	při	k7c6
výměně	výměna	k1gFnSc6
elektrického	elektrický	k2eAgInSc2d1
náboje	náboj	k1gInSc2
mezi	mezi	k7c7
částicemi	částice	k1gFnPc7
slunečního	sluneční	k2eAgInSc2d1
větru	vítr	k1gInSc2
a	a	k8xC
částicemi	částice	k1gFnPc7
mezihvězdného	mezihvězdný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
atomy	atom	k1gInPc4
poté	poté	k6eAd1
cestují	cestovat	k5eAaImIp3nP
zpět	zpět	k6eAd1
k	k	k7c3
našemu	náš	k3xOp1gNnSc3
Slunci	slunce	k1gNnSc3
a	a	k8xC
při	při	k7c6
této	tento	k3xDgFnSc6
cestě	cesta	k1gFnSc6
je	být	k5eAaImIp3nS
IBEX	IBEX	kA
detekuje	detekovat	k5eAaImIp3nS
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
mapu	mapa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
sondy	sonda	k1gFnSc2
</s>
<s>
Základní	základní	k2eAgFnSc3d1
konstrukci	konstrukce	k1gFnSc3
sondy	sonda	k1gFnSc2
měla	mít	k5eAaImAgFnS
na	na	k7c6
starosti	starost	k1gFnSc6
společnost	společnost	k1gFnSc1
Orbital	orbital	k1gInSc4
Science	Science	k1gFnSc2
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
výrobcem	výrobce	k1gMnSc7
nosné	nosný	k2eAgFnSc2d1
rakety	raketa	k1gFnSc2
Pegasus	Pegasus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgInPc7d1
vědeckými	vědecký	k2eAgInPc7d1
nástroji	nástroj	k1gInPc7
je	být	k5eAaImIp3nS
dvojice	dvojice	k1gFnSc1
detektorů	detektor	k1gInPc2
energetických	energetický	k2eAgInPc2d1
neutrálních	neutrální	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
detektor	detektor	k1gInSc1
je	být	k5eAaImIp3nS
citlivý	citlivý	k2eAgInSc1d1
na	na	k7c4
částice	částice	k1gFnPc4
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
energií	energie	k1gFnSc7
(	(	kIx(
<g/>
300	#num#	k4
eV	eV	k?
až	až	k9
6	#num#	k4
keV	keV	k?
<g/>
)	)	kIx)
a	a	k8xC
druhý	druhý	k4xOgInSc4
na	na	k7c6
částice	částice	k1gFnPc1
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
energií	energie	k1gFnSc7
(	(	kIx(
<g/>
10	#num#	k4
eV	eV	k?
až	až	k9
2	#num#	k4
keV	keV	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
dokáží	dokázat	k5eAaPmIp3nP
zaznamenávat	zaznamenávat	k5eAaImF
časy	čas	k1gInPc1
<g/>
,	,	kIx,
směry	směr	k1gInPc1
<g/>
,	,	kIx,
hmotnosti	hmotnost	k1gFnPc1
a	a	k8xC
energie	energie	k1gFnPc1
detekovaných	detekovaný	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc4
a	a	k8xC
výrobu	výroba	k1gFnSc4
detektorů	detektor	k1gInPc2
zajišťovaly	zajišťovat	k5eAaImAgInP
Lockheed	Lockheed	k1gInSc4
Martin	Martin	k1gMnSc1
Advanced	Advanced	k1gMnSc1
Technology	technolog	k1gMnPc7
Center	centrum	k1gNnPc2
a	a	k8xC
Los	los	k1gMnSc1
Alamos	Alamos	k1gMnSc1
National	National	k1gFnSc2
Laboratory	Laborator	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Řízení	řízení	k1gNnSc1
mise	mise	k1gFnSc2
</s>
<s>
Bylo	být	k5eAaImAgNnS
svěřeno	svěřen	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
vědeckého	vědecký	k2eAgInSc2d1
výzkumného	výzkumný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
v	v	k7c4
Southwest	Southwest	k1gFnSc4
Research	Researcha	k1gFnPc2
Institute	institut	k1gInSc5
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
San	San	k1gMnSc6
Antoniu	Antonio	k1gMnSc6
(	(	kIx(
<g/>
USA	USA	kA
-	-	kIx~
stát	stát	k1gInSc1
Texas	Texas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
mise	mise	k1gFnSc2
</s>
<s>
Příprava	příprava	k1gFnSc1
sondy	sonda	k1gFnSc2
před	před	k7c7
startem	start	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Sondu	sonda	k1gFnSc4
vypustil	vypustit	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
raketou	raketa	k1gFnSc7
Pegasus	Pegasus	k1gMnSc1
XL	XL	kA
/	/	kIx~
<g/>
Star	Star	kA
27	#num#	k4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
12	#num#	k4
km	km	kA
nad	nad	k7c7
Zemí	zem	k1gFnSc7
letoun	letoun	k1gMnSc1
Lockheed	Lockheed	k1gMnSc1
L-1011	L-1011	k1gMnSc1
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
svému	svůj	k3xOyFgInSc3
letu	let	k1gInSc3
odstartoval	odstartovat	k5eAaPmAgMnS
z	z	k7c2
atolu	atol	k1gInSc2
Kwajalein	Kwajaleina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšném	úspěšný	k2eAgInSc6d1
startu	start	k1gInSc6
bylo	být	k5eAaImAgNnS
sondě	sonda	k1gFnSc6
agenturou	agentura	k1gFnSc7
COSPAR	COSPAR	kA
přiděleno	přidělen	k2eAgNnSc1d1
označení	označení	k1gNnSc1
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
51	#num#	k4
<g/>
A.	A.	kA
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
byla	být	k5eAaImAgFnS
sonda	sonda	k1gFnSc1
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c4
silně	silně	k6eAd1
eliptickou	eliptický	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
s	s	k7c7
nízkým	nízký	k2eAgNnSc7d1
perigeem	perigeum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
apogeu	apogeum	k1gNnSc6
zažehnut	zažehnut	k2eAgInSc1d1
motor	motor	k1gInSc1
na	na	k7c4
tuhá	tuhý	k2eAgNnPc4d1
paliva	palivo	k1gNnPc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zvýšil	zvýšit	k5eAaPmAgInS
i	i	k9
perigeum	perigeum	k1gNnSc4
a	a	k8xC
navedl	navést	k5eAaPmAgMnS
sondu	sonda	k1gFnSc4
na	na	k7c4
požadovanou	požadovaný	k2eAgFnSc4d1
vysokou	vysoký	k2eAgFnSc4d1
eliptickou	eliptický	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
IBEXu	IBEXus	k1gInSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
eliptická	eliptický	k2eAgFnSc1d1
oběžná	oběžný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Země	zem	k1gFnSc2
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
excentricitou	excentricita	k1gFnSc7
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
výška	výška	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
perigea	perigeum	k1gNnSc2
ve	v	k7c6
výšce	výška	k1gFnSc6
6000	#num#	k4
až	až	k9
11	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
apogeum	apogeum	k1gNnSc4
ve	v	k7c6
výšce	výška	k1gFnSc6
až	až	k9
300	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
tři-čtvrtiny	tři-čtvrtin	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
mezi	mezi	k7c7
Zemí	zem	k1gFnSc7
a	a	k8xC
Měsícem	měsíc	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
orbitální	orbitální	k2eAgNnPc1d1
data	datum	k1gNnPc1
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nP
v	v	k7c6
důsledku	důsledek	k1gInSc6
odporu	odpor	k1gInSc2
atmosféry	atmosféra	k1gFnSc2
v	v	k7c6
perigeu	perigeum	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
gravitačních	gravitační	k2eAgFnPc2d1
turbulencí	turbulence	k1gFnPc2
z	z	k7c2
gravitačního	gravitační	k2eAgNnSc2d1
pole	pole	k1gNnSc2
Měsíce	měsíc	k1gInSc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
kosmických	kosmický	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoké	vysoký	k2eAgInPc1d1
apogeum	apogeum	k1gNnSc4
umožňuje	umožňovat	k5eAaImIp3nS
sondě	sonda	k1gFnSc3
opustit	opustit	k5eAaPmF
magnetosféru	magnetosféra	k1gFnSc4
Země	zem	k1gFnSc2
i	i	k8xC
Van	vana	k1gFnPc2
Allenovy	Allenův	k2eAgInPc1d1
pásy	pás	k1gInPc1
a	a	k8xC
provádět	provádět	k5eAaImF
měření	měření	k1gNnSc4
<g/>
,	,	kIx,
bez	bez	k7c2
rušivých	rušivý	k2eAgInPc2d1
vlivů	vliv	k1gInPc2
od	od	k7c2
nabitých	nabitý	k2eAgFnPc2d1
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
přibližně	přibližně	k6eAd1
70	#num#	k4
000	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Země	zem	k1gFnSc2
již	již	k6eAd1
neprobíhají	probíhat	k5eNaImIp3nP
měření	měření	k1gNnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
sonda	sonda	k1gFnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
jiným	jiný	k2eAgFnPc3d1
činnostem	činnost	k1gFnPc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
odesílání	odesílání	k1gNnSc1
nasbíraných	nasbíraný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
zpět	zpět	k6eAd1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
měření	měření	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
hustoty	hustota	k1gFnSc2
energetických	energetický	k2eAgInPc2d1
neutrálních	neutrální	k2eAgInPc2d1
atomů	atom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Prvotní	prvotní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
neočekávaný	očekávaný	k2eNgInSc1d1
<g/>
,	,	kIx,
relativně	relativně	k6eAd1
tenký	tenký	k2eAgInSc4d1
pruh	pruh	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
je	být	k5eAaImIp3nS
hustota	hustota	k1gFnSc1
energetických	energetický	k2eAgInPc2d1
neutrálních	neutrální	k2eAgInPc2d1
atomů	atom	k1gInPc2
dva	dva	k4xCgMnPc1
až	až	k9
třikrát	třikrát	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
jinde	jinde	k6eAd1
na	na	k7c6
obloze	obloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pruh	pruh	k1gInSc1
se	se	k3xPyFc4
nenachází	nacházet	k5eNaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
nazývané	nazývaný	k2eAgFnSc6d1
bow	bow	k?
shock	shock	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
na	na	k7c6
pomezí	pomezí	k1gNnSc6
heliosféry	heliosféra	k1gFnSc2
a	a	k8xC
mezihvězdného	mezihvězdný	k2eAgNnSc2d1
média	médium	k1gNnSc2
při	při	k7c6
pohybu	pohyb	k1gInSc6
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
mezihvězdným	mezihvězdný	k2eAgInSc7d1
prostorem	prostor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
tohoto	tento	k3xDgInSc2
jevu	jev	k1gInSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
zatím	zatím	k6eAd1
nejasný	jasný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
teorií	teorie	k1gFnPc2
je	být	k5eAaImIp3nS
zvýšená	zvýšený	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
energetických	energetický	k2eAgInPc2d1
neutrálních	neutrální	k2eAgInPc2d1
atomů	atom	k1gInPc2
způsobena	způsoben	k2eAgFnSc1d1
interakci	interakce	k1gFnSc3
mezi	mezi	k7c7
heliosférou	heliosféra	k1gFnSc7
a	a	k8xC
lokálním	lokální	k2eAgNnSc7d1
magnetickým	magnetický	k2eAgNnSc7d1
polem	pole	k1gNnSc7
naší	náš	k3xOp1gFnSc2
galaxie	galaxie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
magnetické	magnetický	k2eAgNnSc1d1
pole	pole	k1gNnSc1
Slunce	slunce	k1gNnSc2
deformuje	deformovat	k5eAaImIp3nS
siločáry	siločára	k1gFnPc4
galaktického	galaktický	k2eAgNnSc2d1
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
stlačování	stlačování	k1gNnSc3
heliosféry	heliosféra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fyzikální	fyzikální	k2eAgFnSc1d1
podstata	podstata	k1gFnSc1
tohoto	tento	k3xDgInSc2
jevu	jev	k1gInSc2
je	být	k5eAaImIp3nS
však	však	k9
stále	stále	k6eAd1
neznámá	známý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Měření	měření	k1gNnSc1
IBEXu	IBEXus	k1gInSc2
jsou	být	k5eAaImIp3nP
porovnávána	porovnáván	k2eAgNnPc1d1
s	s	k7c7
daty	datum	k1gNnPc7
odeslanými	odeslaný	k2eAgInPc7d1
oběma	dva	k4xCgInPc7
Voyagery	Voyager	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
již	již	k6eAd1
dorazily	dorazit	k5eAaPmAgInP
do	do	k7c2
oblasti	oblast	k1gFnSc2
vzniku	vznik	k1gInSc2
energetických	energetický	k2eAgInPc2d1
neutrálních	neutrální	k2eAgInPc2d1
atomů	atom	k1gInPc2
a	a	k8xC
s	s	k7c7
daty	datum	k1gNnPc7
od	od	k7c2
sondy	sonda	k1gFnSc2
Cassini	Cassin	k2eAgMnPc1d1
u	u	k7c2
Saturnu	Saturn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snímkování	snímkování	k1gNnSc1
celé	celý	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
trvá	trvat	k5eAaImIp3nS
půl	půl	k1xP
roku	rok	k1gInSc2
a	a	k8xC
rozdíly	rozdíl	k1gInPc4
ve	v	k7c6
snímaných	snímaný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
sekvencemi	sekvence	k1gFnPc7
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
lišit	lišit	k5eAaImF
podle	podle	k7c2
sluneční	sluneční	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
hustoty	hustota	k1gFnSc2
mezihvězdného	mezihvězdný	k2eAgNnSc2d1
media	medium	k1gNnSc2
a	a	k8xC
změn	změna	k1gFnPc2
magnetických	magnetický	k2eAgFnPc2d1
polí	pole	k1gFnPc2
slunce	slunce	k1gNnSc2
a	a	k8xC
galaxie	galaxie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaje	údaj	k1gInSc2
IBEXu	IBEXa	k1gFnSc4
tak	tak	k6eAd1
mohou	moct	k5eAaImIp3nP
pomoci	pomoct	k5eAaPmF
k	k	k7c3
lepšímu	dobrý	k2eAgNnSc3d2
pochopení	pochopení	k1gNnSc3
těchto	tento	k3xDgInPc2
dějů	děj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
očekává	očekávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
získané	získaný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
pomohou	pomoct	k5eAaPmIp3nP
lépe	dobře	k6eAd2
stanovit	stanovit	k5eAaPmF
polohu	poloha	k1gFnSc4
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
v	v	k7c6
mléčné	mléčný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Cena	cena	k1gFnSc1
mise	mise	k1gFnSc2
je	být	k5eAaImIp3nS
165	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
přepočtu	přepočet	k1gInSc6
cca	cca	kA
3,1	3,1	k4
miliard	miliarda	k4xCgFnPc2
Kč	Kč	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
IBEX	IBEX	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
IBEX	IBEX	kA
na	na	k7c6
SPACE	SPACE	kA
</s>
<s>
Novinový	novinový	k2eAgInSc1d1
článek	článek	k1gInSc1
o	o	k7c6
sondě	sonda	k1gFnSc6
</s>
<s>
osel	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Tajemný	tajemný	k2eAgInSc1d1
pás	pás	k1gInSc1
na	na	k7c4
hranici	hranice	k1gFnSc4
Sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
je	být	k5eAaImIp3nS
kolébkou	kolébka	k1gFnSc7
rychlých	rychlý	k2eAgInPc2d1
atomů	atom	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Program	program	k1gInSc1
Explorer	Explorer	k1gInSc1
1958	#num#	k4
-	-	kIx~
1959	#num#	k4
</s>
<s>
1	#num#	k4
•	•	k?
2	#num#	k4
•	•	k?
3	#num#	k4
•	•	k?
4	#num#	k4
•	•	k?
5	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
7	#num#	k4
1960	#num#	k4
-	-	kIx~
1969	#num#	k4
</s>
<s>
8	#num#	k4
•	•	k?
9	#num#	k4
•	•	k?
10	#num#	k4
•	•	k?
11	#num#	k4
•	•	k?
12	#num#	k4
•	•	k?
13	#num#	k4
•	•	k?
14	#num#	k4
•	•	k?
15	#num#	k4
•	•	k?
16	#num#	k4
•	•	k?
17	#num#	k4
•	•	k?
18	#num#	k4
•	•	k?
19	#num#	k4
•	•	k?
20	#num#	k4
•	•	k?
21	#num#	k4
•	•	k?
22	#num#	k4
•	•	k?
23	#num#	k4
•	•	k?
24	#num#	k4
•	•	k?
25	#num#	k4
•	•	k?
26	#num#	k4
•	•	k?
27	#num#	k4
•	•	k?
28	#num#	k4
•	•	k?
29	#num#	k4
•	•	k?
30	#num#	k4
•	•	k?
31	#num#	k4
•	•	k?
32	#num#	k4
•	•	k?
33	#num#	k4
•	•	k?
34	#num#	k4
•	•	k?
35	#num#	k4
•	•	k?
36	#num#	k4
•	•	k?
37	#num#	k4
•	•	k?
38	#num#	k4
•	•	k?
39	#num#	k4
•	•	k?
40	#num#	k4
•	•	k?
41	#num#	k4
1970	#num#	k4
-	-	kIx~
1979	#num#	k4
</s>
<s>
42	#num#	k4
•	•	k?
43	#num#	k4
•	•	k?
44	#num#	k4
•	•	k?
45	#num#	k4
•	•	k?
46	#num#	k4
•	•	k?
47	#num#	k4
•	•	k?
48	#num#	k4
•	•	k?
49	#num#	k4
•	•	k?
50	#num#	k4
•	•	k?
51	#num#	k4
•	•	k?
52	#num#	k4
•	•	k?
53	#num#	k4
•	•	k?
54	#num#	k4
•	•	k?
55	#num#	k4
•	•	k?
56	#num#	k4
•	•	k?
57	#num#	k4
•	•	k?
58	#num#	k4
•	•	k?
59	#num#	k4
•	•	k?
60	#num#	k4
•	•	k?
61	#num#	k4
1980	#num#	k4
-	-	kIx~
1989	#num#	k4
</s>
<s>
62	#num#	k4
(	(	kIx(
<g/>
DE-	DE-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
63	#num#	k4
(	(	kIx(
<g/>
DE-	DE-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
64	#num#	k4
(	(	kIx(
<g/>
SME	SME	k?
<g/>
)	)	kIx)
•	•	k?
65	#num#	k4
(	(	kIx(
<g/>
CCE	CCE	kA
<g/>
)	)	kIx)
•	•	k?
66	#num#	k4
(	(	kIx(
<g/>
COBE	COBE	kA
<g/>
)	)	kIx)
1990	#num#	k4
-	-	kIx~
1999	#num#	k4
</s>
<s>
67	#num#	k4
(	(	kIx(
<g/>
EUVE	EUVE	kA
<g/>
)	)	kIx)
•	•	k?
68	#num#	k4
(	(	kIx(
<g/>
SAMPEX	SAMPEX	kA
<g/>
)	)	kIx)
•	•	k?
69	#num#	k4
(	(	kIx(
<g/>
RXTE	RXTE	kA
<g/>
)	)	kIx)
•	•	k?
70	#num#	k4
(	(	kIx(
<g/>
FAST	FAST	kA
<g/>
)	)	kIx)
•	•	k?
71	#num#	k4
(	(	kIx(
<g/>
ACE	ACE	kA
<g/>
)	)	kIx)
•	•	k?
72	#num#	k4
(	(	kIx(
<g/>
SNOE	SNOE	kA
<g/>
)	)	kIx)
•	•	k?
73	#num#	k4
(	(	kIx(
<g/>
TRACE	TRACE	kA
<g/>
)	)	kIx)
•	•	k?
74	#num#	k4
(	(	kIx(
<g/>
SWAS	SWAS	kA
<g/>
)	)	kIx)
•	•	k?
75	#num#	k4
(	(	kIx(
<g/>
WIRE	WIRE	kA
<g/>
)	)	kIx)
•	•	k?
76	#num#	k4
(	(	kIx(
<g/>
TERRIERS	TERRIERS	kA
<g/>
)	)	kIx)
•	•	k?
77	#num#	k4
(	(	kIx(
<g/>
FUSE	fuse	k1gFnSc2
<g/>
)	)	kIx)
2000	#num#	k4
-	-	kIx~
2009	#num#	k4
</s>
<s>
78	#num#	k4
(	(	kIx(
<g/>
IMAGE	image	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
79	#num#	k4
(	(	kIx(
<g/>
HETE	HETE	kA
<g/>
)	)	kIx)
•	•	k?
80	#num#	k4
(	(	kIx(
<g/>
WMAP	WMAP	kA
<g/>
)	)	kIx)
•	•	k?
81	#num#	k4
(	(	kIx(
<g/>
RHESSI	RHESSI	kA
<g/>
)	)	kIx)
•	•	k?
82	#num#	k4
(	(	kIx(
<g/>
CHIPSat	CHIPSat	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
83	#num#	k4
(	(	kIx(
<g/>
GALEX	GALEX	kA
<g/>
)	)	kIx)
•	•	k?
84	#num#	k4
(	(	kIx(
<g/>
SWIFT	SWIFT	kA
<g/>
)	)	kIx)
•	•	k?
85-89	85-89	k4
(	(	kIx(
<g/>
THEMIS	Themis	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
90	#num#	k4
(	(	kIx(
<g/>
AIM	AIM	kA
<g/>
)	)	kIx)
•	•	k?
91	#num#	k4
(	(	kIx(
<g/>
IBEX	IBEX	kA
<g/>
)	)	kIx)
•	•	k?
92	#num#	k4
(	(	kIx(
<g/>
WISE	WISE	kA
<g/>
)	)	kIx)
2010	#num#	k4
-	-	kIx~
</s>
<s>
93	#num#	k4
(	(	kIx(
<g/>
NuSTAR	NuSTAR	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
94	#num#	k4
(	(	kIx(
<g/>
IRIS	iris	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
