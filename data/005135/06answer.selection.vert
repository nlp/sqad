<s>
Deutsches	Deutsches	k1gMnSc1	Deutsches
Wörterbuch	Wörterbuch	k1gMnSc1	Wörterbuch
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Německý	německý	k2eAgInSc1d1	německý
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
DWB	DWB	kA	DWB
nebo	nebo	k8xC	nebo
der	drát	k5eAaImRp2nS	drát
Grimm	Grimm	k1gFnPc3	Grimm
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
výkladový	výkladový	k2eAgInSc1d1	výkladový
a	a	k8xC	a
etymologický	etymologický	k2eAgInSc1d1	etymologický
slovník	slovník	k1gInSc1	slovník
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
