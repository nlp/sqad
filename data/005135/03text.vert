<s>
Deutsches	Deutsches	k1gMnSc1	Deutsches
Wörterbuch	Wörterbuch	k1gMnSc1	Wörterbuch
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Německý	německý	k2eAgInSc1d1	německý
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
DWB	DWB	kA	DWB
nebo	nebo	k8xC	nebo
der	drát	k5eAaImRp2nS	drát
Grimm	Grimm	k1gFnPc3	Grimm
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc2	svůj
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
výkladový	výkladový	k2eAgInSc1d1	výkladový
a	a	k8xC	a
etymologický	etymologický	k2eAgInSc1d1	etymologický
slovník	slovník	k1gInSc1	slovník
německého	německý	k2eAgInSc2d1	německý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
byla	být	k5eAaImAgNnP	být
započata	započnout	k5eAaPmNgNnP	započnout
bratry	bratr	k1gMnPc7	bratr
Grimmovými	Grimmová	k1gFnPc7	Grimmová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
úplně	úplně	k6eAd1	úplně
dokončen	dokončit	k5eAaPmNgInS	dokončit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
32	[number]	k4	32
svazků	svazek	k1gInPc2	svazek
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
350	[number]	k4	350
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
další	další	k2eAgInPc1d1	další
dodatky	dodatek	k1gInPc1	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
výklady	výklad	k1gInPc1	výklad
příslušných	příslušný	k2eAgNnPc2d1	příslušné
slov	slovo	k1gNnPc2	slovo
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
latině	latina	k1gFnSc6	latina
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
podrobné	podrobný	k2eAgFnPc1d1	podrobná
informace	informace	k1gFnPc1	informace
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
etymologii	etymologie	k1gFnSc4	etymologie
a	a	k8xC	a
příklady	příklad	k1gInPc4	příklad
použití	použití	k1gNnSc2	použití
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
i	i	k8xC	i
překladové	překladový	k2eAgFnSc2d1	překladová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
jak	jak	k8xC	jak
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
soudobá	soudobý	k2eAgNnPc4d1	soudobé
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
slova	slovo	k1gNnPc4	slovo
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Deutsches	Deutsches	k1gMnSc1	Deutsches
Wörterbuch	Wörterbuch	k1gMnSc1	Wörterbuch
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Deutsches	Deutschesa	k1gFnPc2	Deutschesa
Wörterbuch	Wörterbucha	k1gFnPc2	Wörterbucha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Deutsches	Deutsches	k1gMnSc1	Deutsches
Wörterbuch	Wörterbuch	k1gMnSc1	Wörterbuch
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
