<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgInSc7d1	domovský
světem	svět	k1gInSc7	svět
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c6	na
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
působení	působení	k1gNnSc6	působení
skrze	skrze	k?	skrze
diplomacii	diplomacie	k1gFnSc4	diplomacie
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc4	cestování
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
