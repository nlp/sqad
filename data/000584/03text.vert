<s>
Lucie	Lucie	k1gFnSc1	Lucie
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Hana	Hana	k1gFnSc1	Hana
Zaňáková	Zaňáková	k1gFnSc1	Zaňáková
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
Otvovice	Otvovice	k1gFnSc1	Otvovice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc1d1	jiné
"	"	kIx"	"
<g/>
Láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Trouba	trouba	k1gFnSc1	trouba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jsi	být	k5eAaImIp2nS	být
můj	můj	k3xOp1gMnSc1	můj
pán	pán	k1gMnSc1	pán
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
eSeMeS	eSeMeS	k?	eSeMeS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
je	být	k5eAaImIp3nS	být
společnicí	společnice	k1gFnSc7	společnice
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
FANTAZMA	fantazma	k1gNnSc1	fantazma
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
Divadlo	divadlo	k1gNnSc4	divadlo
Ta	ten	k3xDgFnSc1	ten
Fantastika	fantastika	k1gFnSc1	fantastika
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
středočeských	středočeský	k2eAgFnPc6d1	Středočeská
Otvovicích	Otvovice	k1gFnPc6	Otvovice
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
zpěvu	zpěv	k1gInSc2	zpěv
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
již	již	k6eAd1	již
na	na	k7c6	na
Lidové	lidový	k2eAgFnSc6d1	lidová
škole	škola	k1gFnSc6	škola
umění	umění	k1gNnSc1	umění
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
ZUŠ	ZUŠ	kA	ZUŠ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
showbyznysu	showbyznys	k1gInSc2	showbyznys
<g/>
,	,	kIx,	,
vyučila	vyučit	k5eAaPmAgFnS	vyučit
se	se	k3xPyFc4	se
dámskou	dámský	k2eAgFnSc4d1	dámská
krejčovou	krejčová	k1gFnSc4	krejčová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zkušenosti	zkušenost	k1gFnPc1	zkušenost
na	na	k7c6	na
hudebních	hudební	k2eAgNnPc6d1	hudební
pódiích	pódium	k1gNnPc6	pódium
získávala	získávat	k5eAaImAgFnS	získávat
jako	jako	k9	jako
členka	členka	k1gFnSc1	členka
skupin	skupina	k1gFnPc2	skupina
Rock-Automat	Rock-Automat	k1gInSc4	Rock-Automat
<g/>
,	,	kIx,	,
Arakain	Arakain	k2eAgInSc4d1	Arakain
a	a	k8xC	a
Vitacit	Vitacit	k2eAgInSc4d1	Vitacit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc2	on
všiml	všimnout	k5eAaPmAgMnS	všimnout
producent	producent	k1gMnSc1	producent
Petr	Petr	k1gMnSc1	Petr
Hannig	Hannig	k1gMnSc1	Hannig
<g/>
,	,	kIx,	,
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
jí	jíst	k5eAaImIp3nS	jíst
pseudonym	pseudonym	k1gInSc4	pseudonym
Lucie	Lucie	k1gFnSc1	Lucie
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
napsal	napsat	k5eAaBmAgMnS	napsat
jí	on	k3xPp3gFnSc2	on
písně	píseň	k1gFnSc2	píseň
Neposlušné	poslušný	k2eNgFnSc2d1	neposlušná
tenisky	teniska	k1gFnSc2	teniska
a	a	k8xC	a
Horší	zlý	k2eAgMnSc1d2	horší
než	než	k8xS	než
kluk	kluk	k1gMnSc1	kluk
<g/>
.	.	kIx.	.
</s>
<s>
Pořádný	pořádný	k2eAgInSc1d1	pořádný
úspěch	úspěch	k1gInSc1	úspěch
ji	on	k3xPp3gFnSc4	on
teprve	teprve	k6eAd1	teprve
čekal	čekat	k5eAaImAgInS	čekat
v	v	k7c6	v
příštím	příští	k2eAgNnSc6d1	příští
desetiletí	desetiletí	k1gNnSc6	desetiletí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prošla	projít	k5eAaPmAgFnS	projít
nejznámějšími	známý	k2eAgInPc7d3	nejznámější
muzikály	muzikál	k1gInPc7	muzikál
uváděnými	uváděný	k2eAgInPc7d1	uváděný
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Misérables	Misérables	k1gInSc1	Misérables
<g/>
,	,	kIx,	,
Dracula	Dracula	k1gFnSc1	Dracula
<g/>
,	,	kIx,	,
Krysař	krysař	k1gMnSc1	krysař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
řadu	řada	k1gFnSc4	řada
sólových	sólový	k2eAgFnPc2d1	sólová
alb	alba	k1gFnPc2	alba
<g/>
,	,	kIx,	,
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
i	i	k9	i
jako	jako	k8xC	jako
herečka	herečka	k1gFnSc1	herečka
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
držela	držet	k5eAaImAgFnS	držet
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
popularity	popularita	k1gFnSc2	popularita
ve	v	k7c6	v
výroční	výroční	k2eAgFnSc6d1	výroční
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1984-1986	[number]	k4	1984-1986
působila	působit	k5eAaImAgFnS	působit
též	též	k9	též
v	v	k7c6	v
heavy-thrash	heavyhrash	k1gMnSc1	heavy-thrash
metalové	metalový	k2eAgFnSc3d1	metalová
kapele	kapela	k1gFnSc3	kapela
Arakain	Arakain	k1gInSc4	Arakain
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
Alešem	Aleš	k1gMnSc7	Aleš
Brichtou	Brichta	k1gMnSc7	Brichta
tvořili	tvořit	k5eAaImAgMnP	tvořit
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
duet	duet	k1gInSc4	duet
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
z	z	k7c2	z
Arakainu	Arakain	k1gInSc2	Arakain
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
každých	každý	k3xTgNnPc2	každý
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
znovu	znovu	k6eAd1	znovu
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
výročních	výroční	k2eAgInPc6d1	výroční
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
mají	mít	k5eAaImIp3nP	mít
za	za	k7c7	za
sebou	se	k3xPyFc7	se
turné	turné	k1gNnSc4	turné
uskutečněné	uskutečněný	k2eAgInPc1d1	uskutečněný
ke	k	k7c3	k
třiatřicetiletému	třiatřicetiletý	k2eAgNnSc3d1	třiatřicetileté
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Lucie	Lucie	k1gFnSc1	Lucie
Bílá	bílý	k2eAgFnSc1d1	bílá
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
dětskou	dětský	k2eAgFnSc7d1	dětská
filmovou	filmový	k2eAgFnSc7d1	filmová
hvězdou	hvězda	k1gFnSc7	hvězda
Tomášem	Tomáš	k1gMnSc7	Tomáš
Holým	Holý	k1gMnSc7	Holý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
asi	asi	k9	asi
po	po	k7c6	po
šesti	šest	k4xCc6	šest
měsících	měsíc	k1gInPc6	měsíc
ukončila	ukončit	k5eAaPmAgFnS	ukončit
Tomášova	Tomášův	k2eAgFnSc1d1	Tomášova
autonehoda	autonehoda	k1gFnSc1	autonehoda
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
následné	následný	k2eAgNnSc4d1	následné
úmrtí	úmrtí	k1gNnSc4	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1995	[number]	k4	1995
porodila	porodit	k5eAaPmAgFnS	porodit
příteli	přítel	k1gMnSc3	přítel
Petru	Petr	k1gMnSc3	Petr
Kratochvílovi	Kratochvíl	k1gMnSc3	Kratochvíl
syna	syn	k1gMnSc4	syn
Filipa	Filip	k1gMnSc4	Filip
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
partnerství	partnerství	k1gNnSc4	partnerství
neudržela	udržet	k5eNaPmAgFnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Kratochvíl	Kratochvíl	k1gMnSc1	Kratochvíl
ji	on	k3xPp3gFnSc4	on
opustil	opustit	k5eAaPmAgMnS	opustit
kvůli	kvůli	k7c3	kvůli
Pavlíně	Pavlína	k1gFnSc3	Pavlína
Babůrkové	Babůrková	k1gFnSc3	Babůrková
<g/>
,	,	kIx,	,
vítězce	vítězka	k1gFnSc3	vítězka
Miss	miss	k1gFnSc2	miss
Československo	Československo	k1gNnSc1	Československo
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
nakrátko	nakrátko	k6eAd1	nakrátko
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
hudebníka	hudebník	k1gMnSc4	hudebník
Stanislava	Stanislav	k1gMnSc4	Stanislav
Penka	Penek	k1gMnSc4	Penek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozvodu	rozvod	k1gInSc6	rozvod
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
žila	žít	k5eAaImAgFnS	žít
jen	jen	k9	jen
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Filipem	Filip	k1gMnSc7	Filip
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgFnPc2	svůj
čtyřicátých	čtyřicátý	k4xOgFnPc2	čtyřicátý
narozenin	narozeniny	k1gFnPc2	narozeniny
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
konzulátě	konzulát	k1gInSc6	konzulát
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
provdala	provdat	k5eAaPmAgFnS	provdat
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
muzikant	muzikant	k1gMnSc1	muzikant
Václav	Václav	k1gMnSc1	Václav
Noid	Noid	k1gMnSc1	Noid
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svědky	svědek	k1gMnPc4	svědek
jim	on	k3xPp3gMnPc3	on
šli	jít	k5eAaImAgMnP	jít
Luciin	Luciin	k2eAgMnSc1d1	Luciin
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
Zaňák	Zaňák	k1gMnSc1	Zaňák
a	a	k8xC	a
kamarád	kamarád	k1gMnSc1	kamarád
ženicha	ženich	k1gMnSc2	ženich
Martin	Martin	k1gMnSc1	Martin
Lébl	Lébl	k1gMnSc1	Lébl
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
oznámen	oznámen	k2eAgInSc1d1	oznámen
jejich	jejich	k3xOp3gInSc1	jejich
rozvod	rozvod	k1gInSc1	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
porotkyní	porotkyně	k1gFnSc7	porotkyně
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
show	show	k1gFnSc6	show
Česko	Česko	k1gNnSc1	Česko
Slovensko	Slovensko	k1gNnSc4	Slovensko
má	mít	k5eAaImIp3nS	mít
talent	talent	k1gInSc1	talent
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
již	již	k6eAd1	již
pátý	pátý	k4xOgInSc4	pátý
rok	rok	k1gInSc4	rok
vysílá	vysílat	k5eAaImIp3nS	vysílat
Prima	prima	k1gFnSc1	prima
TV	TV	kA	TV
a	a	k8xC	a
TV	TV	kA	TV
JOJ	jojo	k1gNnPc2	jojo
<g/>
.	.	kIx.	.
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
také	také	k9	také
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
rolí	role	k1gFnPc2	role
v	v	k7c6	v
muzikálovém	muzikálový	k2eAgInSc6d1	muzikálový
filmu	film	k1gInSc6	film
V	v	k7c6	v
Peřině	peřina	k1gFnSc6	peřina
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
F.	F.	kA	F.
A.	A.	kA	A.
Brabce	Brabec	k1gMnSc2	Brabec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
patronkou	patronka	k1gFnSc7	patronka
projektu	projekt	k1gInSc2	projekt
"	"	kIx"	"
<g/>
For	forum	k1gNnPc2	forum
You	You	k1gFnPc2	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
vás	vy	k3xPp2nPc4	vy
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Černobílé	černobílý	k2eAgNnSc1d1	černobílé
turné	turné	k1gNnSc1	turné
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
50	[number]	k4	50
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
projela	projet	k5eAaPmAgFnS	projet
osm	osm	k4xCc4	osm
měst	město	k1gNnPc2	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgNnPc1	dva
města	město	k1gNnPc1	město
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velkému	velký	k2eAgInSc3d1	velký
zájmu	zájem	k1gInSc3	zájem
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
natočeno	natočit	k5eAaBmNgNnS	natočit
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
akce	akce	k1gFnSc1	akce
opakovala	opakovat	k5eAaImAgFnS	opakovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Černobílé	černobílý	k2eAgNnSc1d1	černobílé
turné	turné	k1gNnSc1	turné
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
navštívila	navštívit	k5eAaPmAgFnS	navštívit
jen	jen	k6eAd1	jen
pět	pět	k4xCc4	pět
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc1	dva
slovenská	slovenský	k2eAgNnPc1d1	slovenské
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lucii	Lucie	k1gFnSc4	Lucie
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
několik	několik	k4yIc1	několik
techniků	technik	k1gMnPc2	technik
<g/>
,	,	kIx,	,
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
Petr	Petr	k1gMnSc1	Petr
Malásek	Malásek	k1gMnSc1	Malásek
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgFnSc1d1	taneční
skupina	skupina	k1gFnSc1	skupina
The	The	k1gMnSc1	The
Pastels	Pastels	k1gInSc1	Pastels
<g/>
,	,	kIx,	,
světlomalíř	světlomalíř	k1gMnSc1	světlomalíř
Alex	Alex	k1gMnSc1	Alex
Dowis	Dowis	k1gFnSc1	Dowis
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
audiovizuální	audiovizuální	k2eAgFnSc4d1	audiovizuální
show	show	k1gFnSc4	show
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
mnoho	mnoho	k4c1	mnoho
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
fanoušci	fanoušek	k1gMnPc1	fanoušek
Lucky	Lucka	k1gFnSc2	Lucka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
turné	turné	k1gNnSc3	turné
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
písničky	písnička	k1gFnPc4	písnička
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stop	stop	k1gInSc1	stop
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc1	ten
já	já	k3xPp1nSc1	já
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
Nesmíš	smět	k5eNaImIp2nS	smět
to	ten	k3xDgNnSc1	ten
vzdát	vzdát	k5eAaPmF	vzdát
•	•	k?	•
demo	demo	k2eAgNnSc4d1	demo
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
Arakain	Arakain	k1gMnSc1	Arakain
•	•	k?	•
"	"	kIx"	"
<g/>
Satanica	Satanica	k1gMnSc1	Satanica
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zimní	zimní	k2eAgFnSc1d1	zimní
královna	královna	k1gFnSc1	královna
<g/>
"	"	kIx"	"
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Cornuto	Cornut	k2eAgNnSc1d1	Cornut
<g/>
"	"	kIx"	"
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
O5	O5	k1gFnSc1	O5
•	•	k?	•
album	album	k1gNnSc1	album
Jiřího	Jiří	k1gMnSc2	Jiří
Korna	korna	k1gFnSc1	korna
•	•	k?	•
"	"	kIx"	"
<g/>
Sex	sex	k1gInSc1	sex
Ghost	Ghost	k1gInSc1	Ghost
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
mou	můj	k3xOp1gFnSc4	můj
vyholenou	vyholený	k2eAgFnSc4d1	vyholená
<g/>
"	"	kIx"	"
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
History	Histor	k1gInPc1	Histor
Live	Liv	k1gFnSc2	Liv
•	•	k?	•
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnPc1	skupina
Arakain	Arakain	k1gMnSc1	Arakain
•	•	k?	•
"	"	kIx"	"
<g/>
Dotyky	dotyk	k1gInPc1	dotyk
<g/>
"	"	kIx"	"
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
Duny	duna	k1gFnPc1	duna
•	•	k?	•
album	album	k1gNnSc1	album
<g />
.	.	kIx.	.
</s>
<s>
Jiřího	Jiří	k1gMnSc4	Jiří
Korna	korna	k1gFnSc1	korna
•	•	k?	•
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
hřích	hřích	k1gInSc1	hřích
<g/>
"	"	kIx"	"
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Citová	citový	k2eAgFnSc1d1	citová
investice	investice	k1gFnSc1	investice
•	•	k?	•
album	album	k1gNnSc1	album
Petra	Petr	k1gMnSc2	Petr
Hapky	hapek	k1gInPc1	hapek
a	a	k8xC	a
Michala	Michala	k1gFnSc1	Michala
Horáčka	Horáček	k1gMnSc2	Horáček
•	•	k?	•
"	"	kIx"	"
<g/>
Dívám	dívat	k5eAaImIp1nS	dívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
dívám	dívat	k5eAaImIp1nS	dívat
<g/>
"	"	kIx"	"
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Znamení	znamení	k1gNnSc1	znamení
•	•	k?	•
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
České	český	k2eAgNnSc1d1	české
srdce	srdce	k1gNnSc1	srdce
•	•	k?	•
"	"	kIx"	"
<g/>
Bosou	bosý	k2eAgFnSc7d1	bosá
nohou	noha	k1gFnSc7	noha
žárem	žár	k1gInSc7	žár
<g/>
"	"	kIx"	"
2001	[number]	k4	2001
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nP	by
tu	tu	k6eAd1	tu
být	být	k5eAaImF	být
líp	dobře	k6eAd2	dobře
•	•	k?	•
album	album	k1gNnSc4	album
Hapky	hapek	k1gInPc4	hapek
a	a	k8xC	a
Horáčka	Horáček	k1gMnSc4	Horáček
•	•	k?	•
"	"	kIx"	"
<g/>
Unesený	unesený	k2eAgMnSc1d1	unesený
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Okna	okno	k1gNnSc2	okno
dokořán	dokořán	k6eAd1	dokořán
<g/>
"	"	kIx"	"
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Archeology	archeolog	k1gMnPc4	archeolog
•	•	k?	•
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
Arakain	Arakain	k1gMnSc1	Arakain
•	•	k?	•
"	"	kIx"	"
<g/>
Cornouto	Cornout	k2eAgNnSc1d1	Cornout
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Zimní	zimní	k2eAgFnSc1d1	zimní
královna	královna	k1gFnSc1	královna
<g/>
"	"	kIx"	"
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g />
.	.	kIx.	.
</s>
<s>
let	léto	k1gNnPc2	léto
natvrdo	natvrdo	k6eAd1	natvrdo
•	•	k?	•
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
skupiny	skupina	k1gFnSc2	skupina
Arakain	Arakaina	k1gFnPc2	Arakaina
v	v	k7c6	v
Lucerně	lucerna	k1gFnSc6	lucerna
•	•	k?	•
"	"	kIx"	"
<g/>
Cornouto	Cornout	k2eAgNnSc1d1	Cornout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dotyky	dotyk	k1gInPc7	dotyk
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Zimní	zimní	k2eAgFnSc1d1	zimní
královna	královna	k1gFnSc1	královna
<g/>
"	"	kIx"	"
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Noid	Noida	k1gFnPc2	Noida
•	•	k?	•
album	album	k1gNnSc4	album
Václava	Václav	k1gMnSc2	Václav
Noid	Noid	k1gMnSc1	Noid
Bárty	Bárta	k1gMnSc2	Bárta
•	•	k?	•
"	"	kIx"	"
<g/>
ParaNoid	ParaNoid	k1gInSc1	ParaNoid
<g/>
"	"	kIx"	"
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
XXV	XXV	kA	XXV
<g />
.	.	kIx.	.
</s>
<s>
Eden	Eden	k1gInSc4	Eden
•	•	k?	•
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
skupiny	skupina	k1gFnSc2	skupina
Arakain	Arakaina	k1gFnPc2	Arakaina
•	•	k?	•
"	"	kIx"	"
<g/>
Zimní	zimní	k2eAgFnSc4d1	zimní
královna	královna	k1gFnSc1	královna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Cornouto	Cornout	k2eAgNnSc4d1	Cornout
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dotyky	dotyk	k1gInPc7	dotyk
<g/>
"	"	kIx"	"
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Proměna	proměna	k1gFnSc1	proměna
•	•	k?	•
album	album	k1gNnSc4	album
Jana	Jan	k1gMnSc2	Jan
Budaře	Budař	k1gMnSc2	Budař
a	a	k8xC	a
Eliščina	Eliščin	k2eAgInSc2d1	Eliščin
bandu	band	k1gInSc2	band
•	•	k?	•
"	"	kIx"	"
<g/>
Odpouštím	odpouštět	k5eAaImIp1nS	odpouštět
<g/>
"	"	kIx"	"
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Cardamon	Cardamona	k1gFnPc2	Cardamona
•	•	k?	•
<g />
.	.	kIx.	.
</s>
<s>
společné	společný	k2eAgInPc1d1	společný
CD	CD	kA	CD
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
umělci	umělec	k1gMnPc7	umělec
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
vrátíš	vrátit	k5eAaPmIp2nS	vrátit
<g/>
"	"	kIx"	"
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zítřejší	zítřejší	k2eAgNnSc1d1	zítřejší
ráno	ráno	k1gNnSc1	ráno
<g/>
"	"	kIx"	"
•	•	k?	•
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Pepou	Pepa	k1gMnSc7	Pepa
Vojtkem	Vojtek	k1gMnSc7	Vojtek
k	k	k7c3	k
seriálu	seriál	k1gInSc3	seriál
Cesty	cesta	k1gFnSc2	cesta
domů	domů	k6eAd1	domů
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
svírám	svírat	k5eAaImIp1nS	svírat
kříž	kříž	k1gInSc1	kříž
<g/>
"	"	kIx"	"
•	•	k?	•
píseň	píseň	k1gFnSc1	píseň
s	s	k7c7	s
Arakain	Arakaina	k1gFnPc2	Arakaina
ke	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
společnému	společný	k2eAgNnSc3d1	společné
turné	turné	k1gNnSc3	turné
XXX	XXX	kA	XXX
Best	Best	k1gMnSc1	Best
of	of	k?	of
Tour	Tour	k1gMnSc1	Tour
2012	[number]	k4	2012
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
XXX	XXX	kA	XXX
•	•	k?	•
DVD	DVD	kA	DVD
záznam	záznam	k1gInSc1	záznam
ze	z	k7c2	z
zkoušky	zkouška	k1gFnSc2	zkouška
ke	k	k7c3	k
společnému	společný	k2eAgNnSc3d1	společné
turné	turné	k1gNnSc3	turné
s	s	k7c7	s
Arakain	Arakain	k1gInSc1	Arakain
•	•	k?	•
2	[number]	k4	2
duety	duet	k1gInPc4	duet
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Toužímským	Toužímský	k2eAgNnPc3d1	Toužímský
a	a	k8xC	a
5	[number]	k4	5
písní	píseň	k1gFnPc2	píseň
sólově	sólově	k6eAd1	sólově
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
XXX	XXX	kA	XXX
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
PVA	PVA	kA	PVA
Expo	Expo	k1gNnSc1	Expo
•	•	k?	•
DVD	DVD	kA	DVD
záznam	záznam	k1gInSc4	záznam
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
koncertu	koncert	k1gInSc2	koncert
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
s	s	k7c7	s
Arakain	Arakain	k1gInSc1	Arakain
•	•	k?	•
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Cornutto	Cornutto	k1gNnSc4	Cornutto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Satanica	Satanicum	k1gNnSc2	Satanicum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
nálezy	nález	k1gInPc4	nález
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
V	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
svírám	svírat	k5eAaImIp1nS	svírat
kříž	kříž	k1gInSc4	kříž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dotyky	dotyk	k1gInPc4	dotyk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zimní	zimní	k2eAgFnSc4d1	zimní
královna	královna	k1gFnSc1	královna
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tygřice	tygřice	k1gFnSc2	tygřice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Teď	teď	k6eAd1	teď
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
vím	vědět	k5eAaImIp1nS	vědět
<g/>
...	...	k?	...
Možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Jen	jen	k9	jen
krátká	krátký	k2eAgFnSc1d1	krátká
návštěva	návštěva	k1gFnSc1	návštěva
potěší	potěšit	k5eAaPmIp3nS	potěšit
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
slávě	sláva	k1gFnSc3	sláva
•	•	k?	•
napsal	napsat	k5eAaBmAgMnS	napsat
Robert	Robert	k1gMnSc1	Robert
Rohál	Rohál	k1gMnSc1	Rohál
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Můj	můj	k3xOp1gInSc1	můj
fantastický	fantastický	k2eAgInSc1d1	fantastický
rok	rok	k1gInSc1	rok
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
náhoda	náhoda	k1gFnSc1	náhoda
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
zebra	zebra	k1gFnSc1	zebra
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
(	(	kIx(	(
<g/>
Lucie	Lucie	k1gFnSc1	Lucie
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Lucie	Lucie	k1gFnSc1	Lucie
011	[number]	k4	011
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
náhoda	náhoda	k1gFnSc1	náhoda
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
-	-	kIx~	-
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
-	-	kIx~	-
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
-	-	kIx~	-
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
ji	on	k3xPp3gFnSc4	on
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
udělil	udělit	k5eAaPmAgMnS	udělit
státní	státní	k2eAgNnSc4d1	státní
vyznamenání	vyznamenání	k1gNnSc4	vyznamenání
ČR	ČR	kA	ČR
-	-	kIx~	-
medaili	medaile	k1gFnSc4	medaile
Za	za	k7c2	za
zásluhy	zásluha	k1gFnSc2	zásluha
I.	I.	kA	I.
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
