<s>
Wisława	Wisława	k6eAd1	Wisława
Szymborská	Szymborský	k2eAgFnSc1d1	Szymborská
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
Bnin	Bnina	k1gFnPc2	Bnina
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
polská	polský	k2eAgFnSc1d1	polská
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
esejistka	esejistka	k1gFnSc1	esejistka
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
kritička	kritička	k1gFnSc1	kritička
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
francouzské	francouzský	k2eAgFnSc2d1	francouzská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
laureátka	laureátka	k1gFnSc1	laureátka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Szymborskou	Szymborský	k2eAgFnSc7d1	Szymborská
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
překládá	překládat	k5eAaImIp3nS	překládat
zejména	zejména	k9	zejména
Vlasta	Vlasta	k1gFnSc1	Vlasta
Dvořáčková	Dvořáčková	k1gFnSc1	Dvořáčková
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
překladatelé	překladatel	k1gMnPc1	překladatel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Bogdan	Bogdan	k1gMnSc1	Bogdan
Trojak	Trojak	k1gMnSc1	Trojak
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Bninu	Bnin	k1gInSc6	Bnin
u	u	k7c2	u
Poznaně	Poznaň	k1gFnSc2	Poznaň
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
součásti	součást	k1gFnPc4	součást
Kórniku	Kórnik	k1gInSc2	Kórnik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
Wincenty	Wincent	k1gInPc4	Wincent
Szymborski	Szymborsk	k1gFnSc2	Szymborsk
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
–	–	k?	–
správce	správka	k1gFnSc6	správka
zakopanských	zakopanský	k2eAgNnPc2d1	zakopanský
panství	panství	k1gNnPc2	panství
hraběte	hrabě	k1gMnSc2	hrabě
Zamoyského	Zamoyský	k1gMnSc2	Zamoyský
(	(	kIx(	(
<g/>
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
narozením	narození	k1gNnSc7	narození
dcery	dcera	k1gFnSc2	dcera
Wisławy	Wisława	k1gFnSc2	Wisława
byl	být	k5eAaImAgInS	být
přemístěn	přemístit	k5eAaPmNgInS	přemístit
do	do	k7c2	do
Kórniku	Kórnik	k1gInSc2	Kórnik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
tamější	tamější	k2eAgFnPc4d1	tamější
finanční	finanční	k2eAgFnPc4d1	finanční
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
)	)	kIx)	)
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Maria	Maria	k1gFnSc1	Maria
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Rottermundová	Rottermundový	k2eAgFnSc1d1	Rottermundový
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
rodina	rodina	k1gFnSc1	rodina
Szymborských	Szymborský	k2eAgMnPc2d1	Szymborský
bydlela	bydlet	k5eAaImAgFnS	bydlet
w	w	k?	w
Toruni	Toruň	k1gFnSc6	Toruň
<g/>
,	,	kIx,	,
od	od	k7c2	od
r.	r.	kA	r.
1929	[number]	k4	1929
nebo	nebo	k8xC	nebo
1931	[number]	k4	1931
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
v	v	k7c6	v
Radziwiłłowské	Radziwiłłowská	k1gFnSc6	Radziwiłłowská
ul	ul	kA	ul
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
Wisława	Wisława	k1gFnSc1	Wisława
tam	tam	k6eAd1	tam
začala	začít	k5eAaPmAgFnS	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
Obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
Józefy	Józef	k1gInPc1	Józef
Joteykové	Joteykový	k2eAgInPc1d1	Joteykový
v	v	k7c6	v
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Podwale	Podwala	k1gFnSc3	Podwala
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
od	od	k7c2	od
září	září	k1gNnSc2	září
1935	[number]	k4	1935
do	do	k7c2	do
Gymnázia	gymnázium	k1gNnSc2	gymnázium
Sester	sestra	k1gFnPc2	sestra
Uršulinek	Uršulinka	k1gFnPc2	Uršulinka
ve	v	k7c4	v
Starowiślné	Starowiślný	k2eAgFnPc4d1	Starowiślný
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
na	na	k7c6	na
tajných	tajný	k2eAgInPc6d1	tajný
kurzech	kurz	k1gInPc6	kurz
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
jako	jako	k8xS	jako
úřednice	úřednice	k1gFnSc2	úřednice
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
pracovnímu	pracovní	k2eAgInSc3d1	pracovní
nasazení	nasazení	k1gNnSc1	nasazení
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
ilustrovala	ilustrovat	k5eAaBmAgFnS	ilustrovat
knížku	knížka	k1gFnSc4	knížka
(	(	kIx(	(
<g/>
učebnici	učebnice	k1gFnSc4	učebnice
angličtiny	angličtina	k1gFnSc2	angličtina
First	First	k1gInSc1	First
steps	steps	k1gInSc1	steps
in	in	k?	in
English	English	k1gInSc1	English
Jana	Jan	k1gMnSc2	Jan
Stanisławského	Stanisławský	k1gMnSc2	Stanisławský
<g/>
)	)	kIx)	)
i	i	k9	i
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
povídky	povídka	k1gFnPc4	povídka
a	a	k8xC	a
občas	občas	k6eAd1	občas
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
literárního	literární	k2eAgInSc2d1	literární
života	život	k1gInSc2	život
Krakova	Krakov	k1gInSc2	Krakov
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
básnířčiných	básnířčin	k2eAgFnPc2d1	básnířčina
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
měl	mít	k5eAaImAgMnS	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
Czesław	Czesław	k1gMnSc1	Czesław
Miłosz	Miłosz	k1gMnSc1	Miłosz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k6eAd1	také
zahájila	zahájit	k5eAaPmAgFnS	zahájit
studium	studium	k1gNnSc4	studium
polonistiky	polonistika	k1gFnSc2	polonistika
na	na	k7c6	na
Jagelonské	jagelonský	k2eAgFnSc6d1	Jagelonská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
sociologii	sociologie	k1gFnSc4	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
ale	ale	k8xC	ale
nedokončila	dokončit	k5eNaPmAgFnS	dokončit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
těžké	těžký	k2eAgFnPc4d1	těžká
hmotné	hmotný	k2eAgFnPc4d1	hmotná
situace	situace	k1gFnPc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
básně	báseň	k1gFnPc4	báseň
otiskla	otisknout	k5eAaPmAgFnS	otisknout
v	v	k7c6	v
krakovském	krakovský	k2eAgInSc6d1	krakovský
Polském	polský	k2eAgInSc6d1	polský
Deníku	deník	k1gInSc6	deník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Boji	boj	k1gInSc6	boj
(	(	kIx(	(
<g/>
Walka	Walko	k1gNnSc2	Walko
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Generaci	generace	k1gFnSc6	generace
(	(	kIx(	(
<g/>
Pokolenie	Pokolenie	k1gFnSc1	Pokolenie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
Szymborska	Szymborsk	k1gInSc2	Szymborsk
byla	být	k5eAaImAgFnS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
prostředím	prostředí	k1gNnSc7	prostředí
akceptujícím	akceptující	k2eAgNnSc7d1	akceptující
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
až	až	k9	až
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
tajemnicí	tajemnice	k1gFnSc7	tajemnice
osvětového	osvětový	k2eAgInSc2d1	osvětový
dvojtýdeníku	dvojtýdeník	k1gInSc2	dvojtýdeník
Krakovská	krakovský	k2eAgFnSc1d1	Krakovská
síň	síň	k1gFnSc1	síň
(	(	kIx(	(
<g/>
Świetlica	Świetlic	k2eAgFnSc1d1	Świetlic
Krakowska	Krakowska	k1gFnSc1	Krakowska
<g/>
)	)	kIx)	)
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
knižními	knižní	k2eAgFnPc7d1	knižní
ilustracemi	ilustrace	k1gFnPc7	ilustrace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1948	[number]	k4	1948
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
básníka	básník	k1gMnSc4	básník
Adama	Adam	k1gMnSc4	Adam
Włodka	Włodek	k1gMnSc4	Włodek
<g/>
.	.	kIx.	.
</s>
<s>
Novomanželé	novomanžel	k1gMnPc1	novomanžel
se	se	k3xPyFc4	se
ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
v	v	k7c6	v
krakovské	krakovský	k2eAgFnSc6d1	Krakovská
"	"	kIx"	"
<g/>
kolonii	kolonie	k1gFnSc6	kolonie
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
"	"	kIx"	"
v	v	k7c4	v
Krupniczí	Krupniczí	k1gNnSc4	Krupniczí
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Neopakovatelné	opakovatelný	k2eNgNnSc1d1	neopakovatelné
klima	klima	k1gNnSc1	klima
tohoto	tento	k3xDgNnSc2	tento
společenství	společenství	k1gNnSc2	společenství
mělo	mít	k5eAaImAgNnS	mít
inspirující	inspirující	k2eAgInSc4d1	inspirující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pochována	pochovat	k5eAaPmNgFnS	pochovat
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
'	'	kIx"	'
<g/>
Cmentarz	Cmentarz	k1gInSc1	Cmentarz
Rakowicki	Rakowick	k1gFnSc2	Rakowick
<g/>
'	'	kIx"	'
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
první	první	k4xOgInPc4	první
svazeček	svazeček	k1gInSc4	svazeček
Szymborské	Szymborský	k2eAgNnSc1d1	Szymborský
básní	báseň	k1gFnSc7	báseň
Básně	báseň	k1gFnSc2	báseň
(	(	kIx(	(
<g/>
Wiersze	Wiersze	k1gFnSc1	Wiersze
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
Šití	šití	k1gNnSc2	šití
praporů	prapor	k1gInPc2	prapor
(	(	kIx(	(
<g/>
Szycie	Szycie	k1gFnSc1	Szycie
sztandarów	sztandarów	k?	sztandarów
<g/>
))	))	k?	))
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
nedomnívám	domnívat	k5eNaImIp1nS	domnívat
socialistické	socialistický	k2eAgFnPc4d1	socialistická
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Knižním	knižní	k2eAgInSc7d1	knižní
debutem	debut	k1gInSc7	debut
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
svazek	svazek	k1gInSc1	svazek
básní	báseň	k1gFnPc2	báseň
Proto	proto	k8xC	proto
žijeme	žít	k5eAaImIp1nP	žít
(	(	kIx(	(
<g/>
Dlatego	Dlatego	k1gNnSc4	Dlatego
żyjemy	żyjema	k1gFnSc2	żyjema
<g/>
)	)	kIx)	)
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
s	s	k7c7	s
básněmi	báseň	k1gFnPc7	báseň
jako	jako	k8xS	jako
Mládeži	mládež	k1gFnSc3	mládež
budující	budující	k2eAgFnSc4d1	budující
Novou	nový	k2eAgFnSc4d1	nová
Huť	huť	k1gFnSc4	huť
(	(	kIx(	(
<g/>
Młodzieży	Młodzieża	k1gFnSc2	Młodzieża
budującej	budującat	k5eAaPmRp2nS	budującat
Nową	Nową	k1gMnSc1	Nową
Hutę	Hutę	k1gMnSc1	Hutę
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Szymborska	Szymborska	k1gFnSc1	Szymborska
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
Svazu	svaz	k1gInSc2	svaz
polských	polský	k2eAgMnPc2d1	polský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
Polské	polský	k2eAgFnSc2d1	polská
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
PSDS	PSDS	kA	PSDS
<g/>
;	;	kIx,	;
PZPR	PZPR	kA	PZPR
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
podepsala	podepsat	k5eAaPmAgFnS	podepsat
společnou	společný	k2eAgFnSc4d1	společná
rezoluci	rezoluce	k1gFnSc4	rezoluce
skupiny	skupina	k1gFnSc2	skupina
členů	člen	k1gMnPc2	člen
krakovské	krakovský	k2eAgFnSc2d1	Krakovská
organizace	organizace	k1gFnSc2	organizace
ZKP	ZKP	kA	ZKP
<g/>
,	,	kIx,	,
odsuzující	odsuzující	k2eAgFnPc1d1	odsuzující
duchovní	duchovní	k2eAgFnPc1d1	duchovní
odsouzené	odsouzená	k1gFnPc1	odsouzená
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
kněží	kněz	k1gMnPc1	kněz
krakovské	krakovský	k2eAgFnSc2d1	Krakovská
kurie	kurie	k1gFnSc2	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
v	v	k7c6	v
r.	r.	kA	r.
1957	[number]	k4	1957
Szymborska	Szymborsk	k1gInSc2	Szymborsk
navázala	navázat	k5eAaPmAgFnS	navázat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
Kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
Jerzym	Jerzym	k1gInSc4	Jerzym
Giedroyciem	Giedroycium	k1gNnSc7	Giedroycium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
Szymborska	Szymborska	k1gFnSc1	Szymborska
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
mezi	mezi	k7c7	mezi
signatáři	signatář	k1gMnPc7	signatář
státními	státní	k2eAgInPc7d1	státní
orgány	orgán	k1gInPc7	orgán
podvrženého	podvržený	k2eAgInSc2d1	podvržený
protestu	protest	k1gInSc2	protest
odsuzujícího	odsuzující	k2eAgInSc2d1	odsuzující
Rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
za	za	k7c4	za
odvysílání	odvysílání	k1gNnSc4	odvysílání
Dopisu	dopis	k1gInSc2	dopis
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
podepsala	podepsat	k5eAaPmAgFnS	podepsat
protestní	protestní	k2eAgInSc4d1	protestní
dopis	dopis	k1gInSc4	dopis
59	[number]	k4	59
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
přední	přední	k2eAgMnPc1d1	přední
polští	polský	k2eAgMnPc1d1	polský
intelektuálové	intelektuál	k1gMnPc1	intelektuál
protestovali	protestovat	k5eAaBmAgMnP	protestovat
proti	proti	k7c3	proti
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zařazovaly	zařazovat	k5eAaImAgFnP	zařazovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c4	o
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
roli	role	k1gFnSc4	role
PSDS	PSDS	kA	PSDS
a	a	k8xC	a
věčném	věčné	k1gNnSc6	věčné
spojenectví	spojenectví	k1gNnSc2	spojenectví
se	se	k3xPyFc4	se
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Szymborska	Szymborska	k1gFnSc1	Szymborska
je	být	k5eAaImIp3nS	být
neoddělitelně	oddělitelně	k6eNd1	oddělitelně
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
Krakovem	Krakov	k1gInSc7	Krakov
a	a	k8xC	a
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
své	svůj	k3xOyFgNnSc4	svůj
pouto	pouto	k1gNnSc4	pouto
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
až	až	k9	až
1981	[number]	k4	1981
byla	být	k5eAaImAgFnS	být
členem	člen	k1gMnSc7	člen
redakce	redakce	k1gFnSc2	redakce
revue	revue	k1gFnSc2	revue
Literární	literární	k2eAgInSc4d1	literární
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
Życie	Życius	k1gMnSc5	Życius
Literackie	Literackius	k1gMnSc5	Literackius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
vedla	vést	k5eAaImAgFnS	vést
stálou	stálý	k2eAgFnSc4d1	stálá
rubriku	rubrika	k1gFnSc4	rubrika
Nepovinná	povinný	k2eNgFnSc1d1	nepovinná
četba	četba	k1gFnSc1	četba
(	(	kIx(	(
<g/>
Lektury	lektura	k1gFnPc1	lektura
nadobowiązkowe	nadobowiązkow	k1gFnPc1	nadobowiązkow
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
publikována	publikovat	k5eAaBmNgFnS	publikovat
i	i	k9	i
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
byla	být	k5eAaImAgFnS	být
také	také	k9	také
členkou	členka	k1gFnSc7	členka
redakční	redakční	k2eAgFnSc2d1	redakční
rady	rada	k1gFnSc2	rada
krakovského	krakovský	k2eAgInSc2d1	krakovský
měsíčníku	měsíčník	k1gInSc2	měsíčník
List	list	k1gInSc1	list
(	(	kIx(	(
<g/>
Pismo	Pisma	k1gFnSc5	Pisma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
Szymborské	Szymborský	k2eAgFnPc1d1	Szymborská
mají	mít	k5eAaImIp3nP	mít
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
lyriku	lyrika	k1gFnSc4	lyrika
s	s	k7c7	s
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
intelektuální	intelektuální	k2eAgFnSc7d1	intelektuální
reflexí	reflexe	k1gFnSc7	reflexe
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
často	často	k6eAd1	často
zřetelný	zřetelný	k2eAgInSc4d1	zřetelný
filozofický	filozofický	k2eAgInSc4d1	filozofický
podtext	podtext	k1gInSc4	podtext
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
tvorbu	tvorba	k1gFnSc4	tvorba
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
preciznost	preciznost	k1gFnSc1	preciznost
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
lapidárnost	lapidárnost	k1gFnSc1	lapidárnost
<g/>
,	,	kIx,	,
časté	častý	k2eAgNnSc1d1	časté
užití	užití	k1gNnSc1	užití
ironie	ironie	k1gFnSc2	ironie
<g/>
,	,	kIx,	,
paradoxu	paradox	k1gInSc2	paradox
<g/>
,	,	kIx,	,
posměchu	posměch	k1gInSc2	posměch
anebo	anebo	k8xC	anebo
žertovného	žertovný	k2eAgInSc2d1	žertovný
odstupu	odstup	k1gInSc2	odstup
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
hlubokých	hluboký	k2eAgInPc2d1	hluboký
obsahů	obsah	k1gInPc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
moralistickým	moralistický	k2eAgNnPc3d1	moralistické
tématům	téma	k1gNnPc3	téma
a	a	k8xC	a
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
existenciální	existenciální	k2eAgFnSc4d1	existenciální
situaci	situace	k1gFnSc4	situace
člověka	člověk	k1gMnSc2	člověk
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
i	i	k8xC	i
lidského	lidský	k2eAgNnSc2d1	lidské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Laureátka	laureátka	k1gFnSc1	laureátka
Goetheho	Goethe	k1gMnSc2	Goethe
ceny	cena	k1gFnSc2	cena
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Herderovy	Herderův	k2eAgFnPc1d1	Herderova
ceny	cena	k1gFnPc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Přímluvcem	přímluvce	k1gMnSc7	přímluvce
poezie	poezie	k1gFnSc2	poezie
Szymborské	Szymborský	k2eAgInPc4d1	Szymborský
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
je	být	k5eAaImIp3nS	být
Karl	Karl	k1gMnSc1	Karl
Dedecius	Dedecius	k1gMnSc1	Dedecius
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
polské	polský	k2eAgFnSc2d1	polská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
ve	v	k7c6	v
zdůvodnění	zdůvodnění	k1gNnSc6	zdůvodnění
přiznání	přiznání	k1gNnSc2	přiznání
ceny	cena	k1gFnSc2	cena
básnířce	básnířka	k1gFnSc6	básnířka
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
za	za	k7c4	za
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
ironickou	ironický	k2eAgFnSc7d1	ironická
přesností	přesnost	k1gFnSc7	přesnost
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
historickým	historický	k2eAgNnSc7d1	historické
i	i	k8xC	i
biologickým	biologický	k2eAgFnPc3d1	biologická
souvislostem	souvislost	k1gFnPc3	souvislost
projevit	projevit	k5eAaPmF	projevit
se	se	k3xPyFc4	se
ve	v	k7c6	v
fragmentech	fragment	k1gInPc6	fragment
lidské	lidský	k2eAgFnSc2d1	lidská
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
Syndikátu	syndikát	k1gInSc2	syndikát
polských	polský	k2eAgMnPc2d1	polský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
(	(	kIx(	(
<g/>
Stowarzyszenie	Stowarzyszenie	k1gFnSc1	Stowarzyszenie
Pisarzy	Pisarza	k1gFnSc2	Pisarza
Polskich	Polskicha	k1gFnPc2	Polskicha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
obdržela	obdržet	k5eAaPmAgFnS	obdržet
titul	titul	k1gInSc4	titul
Člověk	člověk	k1gMnSc1	člověk
roku	rok	k1gInSc2	rok
týdeníku	týdeník	k1gInSc2	týdeník
Wprost	Wprost	k1gFnSc1	Wprost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
obdržela	obdržet	k5eAaPmAgFnS	obdržet
na	na	k7c6	na
Wawelu	Wawel	k1gInSc6	Wawel
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
polského	polský	k2eAgMnSc2d1	polský
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
Bronisława	Bronisławus	k1gMnSc2	Bronisławus
Komorowského	Komorowský	k1gMnSc2	Komorowský
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
polské	polský	k2eAgNnSc1d1	polské
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
řád	řád	k1gInSc4	řád
bílé	bílý	k2eAgFnSc2d1	bílá
orlice	orlice	k1gFnSc2	orlice
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
Order	Order	k1gMnSc1	Order
Orła	Orła	k1gMnSc1	Orła
Białego	Białego	k1gMnSc1	Białego
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
žijeme	žít	k5eAaImIp1nP	žít
(	(	kIx(	(
<g/>
Dlatego	Dlatego	k6eAd1	Dlatego
żyjemy	żyjema	k1gFnPc4	żyjema
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
Otázky	otázka	k1gFnPc1	otázka
pokládané	pokládaný	k2eAgFnPc1d1	pokládaná
sobě	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
Pytania	Pytanium	k1gNnSc2	Pytanium
zadawane	zadawanout	k5eAaPmIp3nS	zadawanout
sobie	sobie	k1gFnSc1	sobie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Volání	volání	k1gNnSc3	volání
k	k	k7c3	k
Yettimu	yetti	k1gMnSc3	yetti
(	(	kIx(	(
<g/>
Wołanie	Wołanie	k1gFnSc2	Wołanie
do	do	k7c2	do
Yeti	yeti	k1gMnSc1	yeti
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
Sól	sólo	k1gNnPc2	sólo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
101	[number]	k4	101
básní	báseň	k1gFnPc2	báseň
(	(	kIx(	(
<g/>
101	[number]	k4	101
wierszy	wiersz	k1gInPc4	wiersz
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Sto	sto	k4xCgNnSc4	sto
radostí	radost	k1gFnSc7	radost
(	(	kIx(	(
<g/>
Sto	sto	k4xCgNnSc4	sto
pociech	pocius	k1gMnPc6	pocius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
Wiersze	Wiersze	k1gFnPc1	Wiersze
wybrane	wybran	k1gInSc5	wybran
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Vybrané	vybraný	k2eAgFnSc2d1	vybraná
poezie	poezie	k1gFnSc2	poezie
(	(	kIx(	(
<g/>
Poezje	Poezje	k1gFnSc1	Poezje
wybrane	wybran	k1gInSc5	wybran
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Každý	každý	k3xTgInSc1	každý
případ	případ	k1gInSc1	případ
(	(	kIx(	(
<g/>
Wszelki	Wszelki	k1gNnSc1	Wszelki
wypadek	wypadka	k1gFnPc2	wypadka
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Velké	velký	k2eAgNnSc1d1	velké
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
Wielka	Wielka	k1gMnSc1	Wielka
liczba	liczba	k1gMnSc1	liczba
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
mostě	most	k1gInSc6	most
(	(	kIx(	(
<g/>
Ludzie	Ludzie	k1gFnSc1	Ludzie
na	na	k7c4	na
moście	moście	k1gFnPc4	moście
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Poezie	poezie	k1gFnSc1	poezie
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
Poezje	Poezje	k1gFnSc1	Poezje
<g/>
:	:	kIx,	:
Poems	Poems	k1gInSc1	Poems
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
polsko-anglické	polskonglický	k2eAgNnSc1d1	polsko-anglický
dvoujazyčné	dvoujazyčný	k2eAgNnSc1d1	dvoujazyčné
vydání	vydání	k1gNnSc1	vydání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Nepovinná	povinný	k2eNgFnSc1d1	nepovinná
četba	četba	k1gFnSc1	četba
(	(	kIx(	(
<g/>
Lektury	lektura	k1gFnPc1	lektura
nadobowiązkowe	nadobowiązkow	k1gFnPc1	nadobowiązkow
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Konec	konec	k1gInSc1	konec
a	a	k8xC	a
počátek	počátek	k1gInSc1	počátek
(	(	kIx(	(
<g/>
Koniec	Koniec	k1gMnSc1	Koniec
i	i	k8xC	i
początek	początek	k1gMnSc1	początek
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Pohled	pohled	k1gInSc1	pohled
<g />
.	.	kIx.	.
</s>
<s>
se	s	k7c7	s
zrnkem	zrnko	k1gNnSc7	zrnko
písku	písek	k1gInSc2	písek
(	(	kIx(	(
<g/>
Widok	Widok	k1gInSc1	Widok
z	z	k7c2	z
ziarnkiem	ziarnkius	k1gMnSc7	ziarnkius
piasku	piasek	k1gInSc2	piasek
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Sto	sto	k4xCgNnSc4	sto
básní	báseň	k1gFnPc2	báseň
-	-	kIx~	-
sto	sto	k4xCgNnSc4	sto
radostí	radost	k1gFnSc7	radost
(	(	kIx(	(
<g/>
Sto	sto	k4xCgNnSc4	sto
wierszy	wiersz	k1gInPc1	wiersz
–	–	k?	–
sto	sto	k4xCgNnSc4	sto
pociech	pocius	k1gMnPc6	pocius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Okamžik	okamžik	k1gInSc1	okamžik
(	(	kIx(	(
<g/>
Chwila	Chwila	k1gFnSc1	Chwila
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Říkanky	říkanka	k1gFnSc2	říkanka
pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
děti	dítě	k1gFnPc4	dítě
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Rymowanki	Rymowank	k1gMnSc3	Rymowank
dla	dla	k?	dla
dużych	dużych	k1gMnSc1	dużych
dzieci	dziece	k1gFnSc4	dziece
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
(	(	kIx(	(
<g/>
Dwukropek	Dwukropek	k1gInSc1	Dwukropek
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Sůl	sůl	k1gFnSc1	sůl
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
V	v	k7c6	v
Hérakleitově	Hérakleitův	k2eAgFnSc6d1	Hérakleitova
řece	řeka	k1gFnSc6	řeka
(	(	kIx(	(
<g/>
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Konec	konec	k1gInSc1	konec
a	a	k8xC	a
počátek	počátek	k1gInSc1	počátek
(	(	kIx(	(
<g/>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Okamžik	okamžik	k1gInSc1	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
(	(	kIx(	(
<g/>
Pistorius	Pistorius	k1gMnSc1	Pistorius
a	a	k8xC	a
Olšanská	olšanský	k2eAgFnSc1d1	Olšanská
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
