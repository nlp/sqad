<s>
Wisława	Wisława	k6eAd1	Wisława
Szymborská	Szymborský	k2eAgFnSc1d1	Szymborská
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1923	[number]	k4	1923
Bnin	Bnina	k1gFnPc2	Bnina
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
polská	polský	k2eAgFnSc1d1	polská
básnířka	básnířka	k1gFnSc1	básnířka
<g/>
,	,	kIx,	,
esejistka	esejistka	k1gFnSc1	esejistka
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
kritička	kritička	k1gFnSc1	kritička
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
francouzské	francouzský	k2eAgFnSc2d1	francouzská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
laureátka	laureátka	k1gFnSc1	laureátka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
