<s>
Minecraft	Minecraft	k1gInSc1	Minecraft
je	být	k5eAaImIp3nS	být
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
otevřeném	otevřený	k2eAgInSc6d1	otevřený
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
hráč	hráč	k1gMnSc1	hráč
neomezenou	omezený	k2eNgFnSc4d1	neomezená
svobodu	svoboda	k1gFnSc4	svoboda
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
Javě	Jav	k1gInSc6	Jav
a	a	k8xC	a
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
švédským	švédský	k2eAgMnSc7d1	švédský
vývojářem	vývojář	k1gMnSc7	vývojář
Markusem	Markus	k1gMnSc7	Markus
Perssonem	Persson	k1gMnSc7	Persson
<g/>
,	,	kIx,	,
známým	známý	k1gMnSc7	známý
též	též	k9	též
jako	jako	k9	jako
Notch	Notch	k1gMnSc1	Notch
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hrou	hra	k1gFnSc7	hra
založil	založit	k5eAaPmAgMnS	založit
společnost	společnost	k1gFnSc1	společnost
Mojang	Mojang	k1gInSc1	Mojang
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hru	hra	k1gFnSc4	hra
stále	stále	k6eAd1	stále
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
od	od	k7c2	od
Markuse	Markuse	k1gFnSc2	Markuse
Perssona	Perssona	k1gFnSc1	Perssona
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
za	za	k7c4	za
2,3	[number]	k4	2,3
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
společnost	společnost	k1gFnSc1	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
herní	herní	k2eAgInSc1d1	herní
svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kostek	kostka	k1gFnPc2	kostka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
materiály	materiál	k1gInPc1	materiál
s	s	k7c7	s
různorodými	různorodý	k2eAgFnPc7d1	různorodá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
lze	lze	k6eAd1	lze
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
singleplayer	singleplayra	k1gFnPc2	singleplayra
(	(	kIx(	(
<g/>
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
hráče	hráč	k1gMnSc4	hráč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
multiplayer	multiplayer	k1gInSc1	multiplayer
(	(	kIx(	(
<g/>
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
herních	herní	k2eAgInPc2d1	herní
módů	mód	k1gInPc2	mód
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
lze	lze	k6eAd1	lze
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
hráč	hráč	k1gMnSc1	hráč
nesmrtelný	nesmrtelný	k1gMnSc1	nesmrtelný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
smazat	smazat	k5eAaPmF	smazat
aktuální	aktuální	k2eAgInSc4d1	aktuální
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgInPc2d1	základní
herních	herní	k2eAgInPc2d1	herní
módů	mód	k1gInPc2	mód
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
také	také	k9	také
4	[number]	k4	4
stupně	stupeň	k1gInSc2	stupeň
obtížnosti	obtížnost	k1gFnSc2	obtížnost
(	(	kIx(	(
<g/>
peaceful	peaceful	k1gInSc1	peaceful
<g/>
,	,	kIx,	,
easy	easy	k1gInPc1	easy
<g/>
,	,	kIx,	,
normal	normal	k1gInSc1	normal
a	a	k8xC	a
hard	hard	k1gInSc1	hard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
náročnost	náročnost	k1gFnSc4	náročnost
hraní	hraní	k1gNnSc2	hraní
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
výskytu	výskyt	k1gInSc2	výskyt
příšer	příšera	k1gFnPc2	příšera
a	a	k8xC	a
doplňování	doplňování	k1gNnSc1	doplňování
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
probíhá	probíhat	k5eAaImIp3nS	probíhat
střídání	střídání	k1gNnSc1	střídání
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
během	během	k7c2	během
noci	noc	k1gFnSc2	noc
objevují	objevovat	k5eAaImIp3nP	objevovat
různé	různý	k2eAgFnPc1d1	různá
příšery	příšera	k1gFnPc1	příšera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
lační	lačnit	k5eAaImIp3nP	lačnit
po	po	k7c6	po
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
přes	přes	k7c4	přes
den	den	k1gInSc4	den
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
o	o	k7c6	o
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
starat	starat	k5eAaImF	starat
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jídlo	jídlo	k1gNnSc1	jídlo
atd.	atd.	kA	atd.
Nakonec	nakonec	k6eAd1	nakonec
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
vesničané	vesničan	k1gMnPc1	vesničan
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
lze	lze	k6eAd1	lze
obchodovat	obchodovat	k5eAaImF	obchodovat
<g/>
.	.	kIx.	.
</s>
<s>
Alfa	alfa	k1gFnSc1	alfa
verze	verze	k1gFnSc2	verze
hry	hra	k1gFnSc2	hra
pro	pro	k7c4	pro
PC	PC	kA	PC
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
objevila	objevit	k5eAaPmAgFnS	objevit
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
plná	plný	k2eAgFnSc1d1	plná
verze	verze	k1gFnSc1	verze
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
Android	android	k1gInSc4	android
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
už	už	k6eAd1	už
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
a	a	k8xC	a
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
iOS	iOS	k?	iOS
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Minecraft	Minecraft	k1gInSc1	Minecraft
pro	pro	k7c4	pro
Xbox	Xbox	k1gInSc4	Xbox
360	[number]	k4	360
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
PlayStation	PlayStation	k1gInSc4	PlayStation
3	[number]	k4	3
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
PlayStation	PlayStation	k1gInSc4	PlayStation
4	[number]	k4	4
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
a	a	k8xC	a
pro	pro	k7c4	pro
PlayStation	PlayStation	k1gInSc4	PlayStation
Vita	vít	k5eAaImNgNnP	vít
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
hry	hra	k1gFnSc2	hra
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
Phone	Phon	k1gInSc5	Phon
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
verze	verze	k1gFnPc1	verze
hry	hra	k1gFnSc2	hra
jsou	být	k5eAaImIp3nP	být
průběžně	průběžně	k6eAd1	průběžně
aktualizovány	aktualizován	k2eAgFnPc1d1	aktualizována
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
hra	hra	k1gFnSc1	hra
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
kreativitu	kreativita	k1gFnSc4	kreativita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
PC	PC	kA	PC
a	a	k8xC	a
Mac	Mac	kA	Mac
již	již	k9	již
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
přes	přes	k7c4	přes
22	[number]	k4	22
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
hry	hra	k1gFnSc2	hra
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
po	po	k7c6	po
registraci	registrace	k1gFnSc6	registrace
zahrát	zahrát	k5eAaPmF	zahrát
neplacenou	placený	k2eNgFnSc4d1	neplacená
demoverzi	demoverze	k1gFnSc4	demoverze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
100	[number]	k4	100
minut	minuta	k1gFnPc2	minuta
hraní	hraní	k1gNnSc2	hraní
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
zablokované	zablokovaný	k2eAgFnPc1d1	zablokovaná
některé	některý	k3yIgFnPc1	některý
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Minecraft	Minecraft	k1gInSc1	Minecraft
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
herních	herní	k2eAgInPc2d1	herní
módů	mód	k1gInPc2	mód
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
nového	nový	k2eAgInSc2d1	nový
herního	herní	k2eAgInSc2d1	herní
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
mapy	mapa	k1gFnPc1	mapa
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
volí	volit	k5eAaImIp3nS	volit
herní	herní	k2eAgInSc4d1	herní
mód	mód	k1gInSc4	mód
z	z	k7c2	z
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Vybrat	vybrat	k5eAaPmF	vybrat
lze	lze	k6eAd1	lze
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
<g/>
:	:	kIx,	:
survival	survivat	k5eAaPmAgInS	survivat
<g/>
,	,	kIx,	,
hardcore	hardcor	k1gInSc5	hardcor
a	a	k8xC	a
creative	creativ	k1gInSc5	creativ
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc4d1	zbylý
dva	dva	k4xCgInPc4	dva
módy	mód	k1gInPc4	mód
(	(	kIx(	(
<g/>
adventure	adventur	k1gMnSc5	adventur
a	a	k8xC	a
spectator	spectator	k1gInSc1	spectator
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nastavit	nastavit	k5eAaPmF	nastavit
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
pomocí	pomocí	k7c2	pomocí
konzole	konzola	k1gFnSc3	konzola
multiplayerové	multiplayerový	k2eAgFnSc2d1	multiplayerová
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Herní	herní	k2eAgInSc4d1	herní
mód	mód	k1gInSc4	mód
můžete	moct	k5eAaImIp2nP	moct
změnit	změnit	k5eAaPmF	změnit
v	v	k7c4	v
chatu	chata	k1gFnSc4	chata
(	(	kIx(	(
<g/>
zapíná	zapínat	k5eAaImIp3nS	zapínat
se	se	k3xPyFc4	se
písmenem	písmeno	k1gNnSc7	písmeno
T	T	kA	T
<g/>
)	)	kIx)	)
příkazem	příkaz	k1gInSc7	příkaz
/	/	kIx~	/
<g/>
gamemode	gamemod	k1gInSc5	gamemod
X	X	kA	X
(	(	kIx(	(
X	X	kA	X
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc4	číslo
buď	buď	k8xC	buď
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
nebo	nebo	k8xC	nebo
3	[number]	k4	3
<g/>
.	.	kIx.	.
0	[number]	k4	0
=	=	kIx~	=
Survival	Survival	k1gMnSc1	Survival
<g/>
,	,	kIx,	,
1	[number]	k4	1
=	=	kIx~	=
Creative	Creativ	k1gInSc5	Creativ
<g/>
,	,	kIx,	,
2	[number]	k4	2
=	=	kIx~	=
Adventure	Adventur	k1gMnSc5	Adventur
<g/>
,	,	kIx,	,
3	[number]	k4	3
=	=	kIx~	=
Spectator	Spectator	k1gInSc1	Spectator
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
základní	základní	k2eAgInSc4d1	základní
herní	herní	k2eAgInSc4d1	herní
mód	mód	k1gInSc4	mód
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
snaží	snažit	k5eAaImIp3nS	snažit
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
nehostinném	hostinný	k2eNgNnSc6d1	nehostinné
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
světě	svět	k1gInSc6	svět
a	a	k8xC	a
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gInSc3	on
pouze	pouze	k6eAd1	pouze
přírodní	přírodní	k2eAgInPc4d1	přírodní
zdroje	zdroj	k1gInPc4	zdroj
-	-	kIx~	-
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
si	se	k3xPyFc3	se
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
brání	bránit	k5eAaImIp3nS	bránit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
příšerám	příšera	k1gFnPc3	příšera
<g/>
,	,	kIx,	,
staví	stavit	k5eAaPmIp3nS	stavit
si	se	k3xPyFc3	se
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
či	či	k8xC	či
farmaří	farmařit	k5eAaImIp3nS	farmařit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejné	k1gNnSc1	stejné
jako	jako	k8xS	jako
survival	survivat	k5eAaImAgMnS	survivat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
jediný	jediný	k2eAgInSc4d1	jediný
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
20	[number]	k4	20
bodů	bod	k1gInPc2	bod
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
smaže	smazat	k5eAaPmIp3nS	smazat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hardcore	hardcor	k1gInSc5	hardcor
módu	móda	k1gFnSc4	móda
hráč	hráč	k1gMnSc1	hráč
nemůže	moct	k5eNaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
obtížnost	obtížnost	k1gFnSc1	obtížnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
hardcore	hardcor	k1gMnSc5	hardcor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
"	"	kIx"	"
<g/>
hard	hard	k1gMnSc1	hard
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kreativní	kreativní	k2eAgInSc1d1	kreativní
mód	mód	k1gInSc1	mód
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
je	být	k5eAaImIp3nS	být
nesmrtelný	smrtelný	k2eNgMnSc1d1	nesmrtelný
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
ničit	ničit	k5eAaImF	ničit
všechny	všechen	k3xTgInPc4	všechen
bloky	blok	k1gInPc4	blok
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
neomezeným	omezený	k2eNgFnPc3d1	neomezená
zásobám	zásoba	k1gFnPc3	zásoba
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
také	také	k9	také
ničit	ničit	k5eAaImF	ničit
podloží	podloží	k1gNnSc4	podloží
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
bedrock	bedrock	k1gInSc1	bedrock
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
survival	survivat	k5eAaImAgMnS	survivat
módu	móda	k1gFnSc4	móda
nezničitelný	zničitelný	k2eNgInSc4d1	nezničitelný
blok	blok	k1gInSc4	blok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
módů	mód	k1gInPc2	mód
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
bloky	blok	k1gInPc1	blok
nedají	dát	k5eNaPmIp3nP	dát
bořit	bořit	k5eAaImF	bořit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
pokládat	pokládat	k5eAaImF	pokládat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemáte	mít	k5eNaImIp2nP	mít
příslušný	příslušný	k2eAgInSc4d1	příslušný
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ho	on	k3xPp3gNnSc4	on
máte	mít	k5eAaImIp2nP	mít
<g/>
,	,	kIx,	,
blok	blok	k1gInSc4	blok
zničit	zničit	k5eAaPmF	zničit
můžete	moct	k5eAaImIp2nP	moct
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
interakce	interakce	k1gFnSc1	interakce
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
stvořeními	stvoření	k1gNnPc7	stvoření
<g/>
,	,	kIx,	,
předměty	předmět	k1gInPc7	předmět
a	a	k8xC	a
některými	některý	k3yIgInPc7	některý
bloky	blok	k1gInPc7	blok
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tlačítka	tlačítko	k1gNnSc2	tlačítko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
adventure	adventur	k1gMnSc5	adventur
mod	mod	k?	mod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
možná	možná	k9	možná
vůbec	vůbec	k9	vůbec
žádná	žádný	k3yNgFnSc1	žádný
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Můžete	moct	k5eAaImIp2nP	moct
prolétávat	prolétávat	k5eAaImF	prolétávat
bloky	blok	k1gInPc4	blok
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
mobů	mob	k1gMnPc2	mob
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
jen	jen	k9	jen
létá	létat	k5eAaImIp3nS	létat
a	a	k8xC	a
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
kliknete	kliknout	k5eAaPmIp2nP	kliknout
na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vidíte	vidět	k5eAaImIp2nP	vidět
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
vidí	vidět	k5eAaImIp3nS	vidět
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
4	[number]	k4	4
základní	základní	k2eAgInPc1d1	základní
stupně	stupeň	k1gInPc1	stupeň
obtížnosti	obtížnost	k1gFnSc2	obtížnost
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
obtížnosti	obtížnost	k1gFnSc2	obtížnost
lze	lze	k6eAd1	lze
průběžně	průběžně	k6eAd1	průběžně
měnit	měnit	k5eAaImF	měnit
v	v	k7c6	v
nastavení	nastavení	k1gNnSc6	nastavení
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Peaceful	Peaceful	k1gInSc1	Peaceful
(	(	kIx(	(
<g/>
Mírumilovná	mírumilovný	k2eAgFnSc1d1	mírumilovná
<g/>
)	)	kIx)	)
–	–	k?	–
Nejsou	být	k5eNaImIp3nP	být
zde	zde	k6eAd1	zde
žádní	žádný	k3yNgMnPc1	žádný
nepřátelé	nepřítel	k1gMnPc1	nepřítel
kromě	kromě	k7c2	kromě
Ender	Ender	k1gMnSc1	Ender
Draka	drak	k1gMnSc2	drak
a	a	k8xC	a
Withera	Wither	k1gMnSc2	Wither
(	(	kIx(	(
<g/>
neútočí	útočit	k5eNaImIp3nS	útočit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
průběžně	průběžně	k6eAd1	průběžně
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
životy	život	k1gInPc4	život
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vypnutý	vypnutý	k2eAgInSc4d1	vypnutý
hlad	hlad	k1gInSc4	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Easy	Easy	k1gInPc1	Easy
(	(	kIx(	(
<g/>
Lehká	Lehká	k1gFnSc1	Lehká
<g/>
)	)	kIx)	)
–	–	k?	–
Hráč	hráč	k1gMnSc1	hráč
dostává	dostávat	k5eAaImIp3nS	dostávat
menší	malý	k2eAgNnSc4d2	menší
zranění	zranění	k1gNnSc4	zranění
od	od	k7c2	od
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
výbuchů	výbuch	k1gInPc2	výbuch
<g/>
,	,	kIx,	,
při	při	k7c6	při
hladovění	hladovění	k1gNnSc6	hladovění
se	se	k3xPyFc4	se
život	život	k1gInSc1	život
sníží	snížit	k5eAaPmIp3nS	snížit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
(	(	kIx(	(
<g/>
5	[number]	k4	5
srdcí	srdce	k1gNnPc2	srdce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
příšery	příšera	k1gFnPc1	příšera
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
útočí	útočit	k5eAaImIp3nP	útočit
na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Normal	Normal	k1gInSc1	Normal
(	(	kIx(	(
<g/>
Střední	střední	k2eAgFnSc1d1	střední
<g/>
/	/	kIx~	/
<g/>
Normální	normální	k2eAgFnSc1d1	normální
<g/>
)	)	kIx)	)
–	–	k?	–
Normální	normální	k2eAgFnSc1d1	normální
obtížnost	obtížnost	k1gFnSc1	obtížnost
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
při	při	k7c6	při
hladovění	hladovění	k1gNnSc6	hladovění
hráči	hráč	k1gMnPc7	hráč
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jen	jen	k9	jen
půl	půl	k1xP	půl
srdce	srdce	k1gNnSc2	srdce
(	(	kIx(	(
<g/>
1	[number]	k4	1
bod	bod	k1gInSc1	bod
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
příšery	příšera	k1gFnPc1	příšera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
útočí	útočit	k5eAaImIp3nP	útočit
na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
jeskynní	jeskynní	k2eAgMnPc1d1	jeskynní
pavouci	pavouk	k1gMnPc1	pavouk
mohou	moct	k5eAaImIp3nP	moct
hráče	hráč	k1gMnSc4	hráč
otrávit	otrávit	k5eAaPmF	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Hard	Hard	k1gInSc1	Hard
(	(	kIx(	(
<g/>
Těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
)	)	kIx)	)
–	–	k?	–
Hráč	hráč	k1gMnSc1	hráč
dostává	dostávat	k5eAaImIp3nS	dostávat
větší	veliký	k2eAgNnPc4d2	veliký
zranění	zranění	k1gNnPc4	zranění
od	od	k7c2	od
nepřátel	nepřítel	k1gMnPc2	nepřítel
a	a	k8xC	a
výbuchů	výbuch	k1gInPc2	výbuch
<g/>
,	,	kIx,	,
zombie	zombie	k1gFnPc1	zombie
mohou	moct	k5eAaImIp3nP	moct
ničit	ničit	k5eAaImF	ničit
dveře	dveře	k1gFnPc4	dveře
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
vyhladovět	vyhladovět	k5eAaPmF	vyhladovět
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
příšery	příšera	k1gFnPc1	příšera
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
útočí	útočit	k5eAaImIp3nP	útočit
na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
jeskynní	jeskynní	k2eAgMnPc1d1	jeskynní
pavouci	pavouk	k1gMnPc1	pavouk
mohou	moct	k5eAaImIp3nP	moct
hráče	hráč	k1gMnSc4	hráč
otrávit	otrávit	k5eAaPmF	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hře	hra	k1gFnSc6	hra
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc4	tři
dimenze	dimenze	k1gFnPc4	dimenze
<g/>
:	:	kIx,	:
Overworld	Overworld	k1gInSc1	Overworld
<g/>
,	,	kIx,	,
Nether	Nethra	k1gFnPc2	Nethra
a	a	k8xC	a
The	The	k1gFnPc2	The
End	End	k1gFnSc2	End
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
modifikace	modifikace	k1gFnPc1	modifikace
mohou	moct	k5eAaImIp3nP	moct
přidat	přidat	k5eAaPmF	přidat
další	další	k2eAgFnPc4d1	další
dimenze	dimenze	k1gFnPc4	dimenze
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Moon	Moon	k1gInSc1	Moon
<g/>
,	,	kIx,	,
Aether	Aethra	k1gFnPc2	Aethra
<g/>
,	,	kIx,	,
Twilight	Twilight	k2eAgInSc1d1	Twilight
Forest	Forest	k1gInSc1	Forest
či	či	k8xC	či
Ore	Ore	k1gFnSc1	Ore
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
dimenze	dimenze	k1gFnPc1	dimenze
jsou	být	k5eAaImIp3nP	být
propojeny	propojen	k2eAgInPc1d1	propojen
portály	portál	k1gInPc1	portál
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
struktura	struktura	k1gFnSc1	struktura
každého	každý	k3xTgInSc2	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
použité	použitý	k2eAgInPc1d1	použitý
bloky	blok	k1gInPc1	blok
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc1	způsob
aktivace	aktivace	k1gFnSc1	aktivace
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc4	jaký
dimenze	dimenze	k1gFnSc1	dimenze
propojuje	propojovat	k5eAaImIp3nS	propojovat
<g/>
.	.	kIx.	.
</s>
<s>
Overworld	Overworld	k1gInSc1	Overworld
(	(	kIx(	(
<g/>
okolní	okolní	k2eAgInSc1d1	okolní
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dimenze	dimenze	k1gFnSc1	dimenze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
hráč	hráč	k1gMnSc1	hráč
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
prvního	první	k4xOgMnSc2	první
Minecraftu	Minecraft	k1gInSc2	Minecraft
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
256	[number]	k4	256
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
Beta	beta	k1gNnSc1	beta
1.0	[number]	k4	1.0
je	být	k5eAaImIp3nS	být
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
mapy	mapa	k1gFnSc2	mapa
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
skalním	skalní	k2eAgNnSc7d1	skalní
podložím	podloží	k1gNnSc7	podloží
(	(	kIx(	(
<g/>
Bedrock	Bedrocko	k1gNnPc2	Bedrocko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
lze	lze	k6eAd1	lze
zničit	zničit	k5eAaPmF	zničit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
kreativním	kreativní	k2eAgInSc6d1	kreativní
módu	mód	k1gInSc6	mód
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
příšery	příšera	k1gFnPc1	příšera
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
štěrku	štěrk	k1gInSc2	štěrk
<g/>
,	,	kIx,	,
písku	písek	k1gInSc2	písek
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
60	[number]	k4	60
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
<g/>
60	[number]	k4	60
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
<g/>
256	[number]	k4	256
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Generují	generovat	k5eAaImIp3nP	generovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
struktury	struktura	k1gFnPc1	struktura
jako	jako	k8xS	jako
dungeony	dungeona	k1gFnPc1	dungeona
<g/>
,	,	kIx,	,
chrámy	chrám	k1gInPc1	chrám
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
,	,	kIx,	,
opuštěné	opuštěný	k2eAgInPc4d1	opuštěný
doly	dol	k1gInPc4	dol
<g/>
,	,	kIx,	,
trhliny	trhlina	k1gFnPc4	trhlina
<g/>
,	,	kIx,	,
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
jezírka	jezírko	k1gNnSc2	jezírko
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
NPC	NPC	kA	NPC
vesničky	vesnička	k1gFnPc4	vesnička
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
biomů	biom	k1gInPc2	biom
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
variací	variace	k1gFnPc2	variace
a	a	k8xC	a
několik	několik	k4yIc1	několik
technických	technický	k2eAgInPc2d1	technický
biomů	biom	k1gInPc2	biom
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
biom	biom	k1gInSc1	biom
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
určitou	určitý	k2eAgFnSc4d1	určitá
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
klesá	klesat	k5eAaImIp3nS	klesat
o	o	k7c4	o
0,05	[number]	k4	0,05
každých	každý	k3xTgInPc2	každý
30	[number]	k4	30
výškových	výškový	k2eAgInPc2d1	výškový
bloků	blok	k1gInPc2	blok
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
1,5	[number]	k4	1,5
tak	tak	k6eAd1	tak
v	v	k7c6	v
biomu	biom	k1gInSc6	biom
bude	být	k5eAaImBp3nS	být
pršet	pršet	k5eAaImF	pršet
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
0,15	[number]	k4	0,15
tak	tak	k6eAd1	tak
biom	biom	k6eAd1	biom
je	být	k5eAaImIp3nS	být
sněžný	sněžný	k2eAgMnSc1d1	sněžný
<g/>
/	/	kIx~	/
<g/>
zamrzlý	zamrzlý	k2eAgMnSc1d1	zamrzlý
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
biomy	biom	k1gMnPc4	biom
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
pouště	poušť	k1gFnPc1	poušť
<g/>
,	,	kIx,	,
džungle	džungle	k1gFnPc1	džungle
<g/>
,	,	kIx,	,
bažiny	bažina	k1gFnPc1	bažina
<g/>
,	,	kIx,	,
planiny	planina	k1gFnPc1	planina
<g/>
,	,	kIx,	,
oceány	oceán	k1gInPc1	oceán
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Nether	Nethra	k1gFnPc2	Nethra
(	(	kIx(	(
<g/>
podsvětí	podsvětí	k1gNnSc1	podsvětí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dimenze	dimenze	k1gFnSc1	dimenze
podobná	podobný	k2eAgFnSc1d1	podobná
peklu	peklo	k1gNnSc3	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Netheru	Nether	k1gInSc2	Nether
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
pomocí	pomocí	k7c2	pomocí
portálu	portál	k1gInSc2	portál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
obsidiánů	obsidián	k1gInPc2	obsidián
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
jako	jako	k8xC	jako
Overworld	Overworld	k1gInSc4	Overworld
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
souřadnice	souřadnice	k1gFnPc4	souřadnice
oproti	oproti	k7c3	oproti
Overworldu	Overworld	k1gMnSc3	Overworld
jsou	být	k5eAaImIp3nP	být
8	[number]	k4	8
<g/>
krát	krát	k6eAd1	krát
menší	malý	k2eAgFnSc4d2	menší
–	–	k?	–
uražení	uražení	k1gNnSc2	uražení
1	[number]	k4	1
bloku	blok	k1gInSc2	blok
v	v	k7c6	v
Netheru	Nether	k1gInSc6	Nether
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
8	[number]	k4	8
bloků	blok	k1gInPc2	blok
v	v	k7c6	v
Overworldu	Overworld	k1gInSc6	Overworld
<g/>
.	.	kIx.	.
</s>
<s>
Nether	Nethra	k1gFnPc2	Nethra
tvoří	tvořit	k5eAaImIp3nS	tvořit
zejména	zejména	k9	zejména
Netherrack	Netherrack	k1gInSc1	Netherrack
a	a	k8xC	a
láva	láva	k1gFnSc1	láva
<g/>
,	,	kIx,	,
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
i	i	k9	i
soul	soul	k1gInSc1	soul
sand	sanda	k1gFnPc2	sanda
<g/>
,	,	kIx,	,
glowstone	glowston	k1gInSc5	glowston
<g/>
,	,	kIx,	,
nether	nethra	k1gFnPc2	nethra
bricky	bricka	k1gFnSc2	bricka
<g/>
,	,	kIx,	,
ploty	plot	k1gInPc4	plot
či	či	k8xC	či
quartz	quartz	k1gInSc4	quartz
ore	ore	k?	ore
(	(	kIx(	(
<g/>
křemen	křemen	k1gInSc1	křemen
<g/>
)	)	kIx)	)
Bedrockem	Bedrock	k1gInSc7	Bedrock
je	být	k5eAaImIp3nS	být
ohraničen	ohraničen	k2eAgInSc1d1	ohraničen
zdola	zdola	k6eAd1	zdola
i	i	k9	i
shora	shora	k6eAd1	shora
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Ghasti	Ghast	k1gFnPc1	Ghast
<g/>
,	,	kIx,	,
Blaze	blaze	k6eAd1	blaze
<g/>
,	,	kIx,	,
Magma	magma	k1gNnSc1	magma
Cube	Cube	k1gInSc1	Cube
<g/>
,	,	kIx,	,
Skeleton	skeleton	k1gInSc1	skeleton
<g/>
,	,	kIx,	,
Wither	Withra	k1gFnPc2	Withra
Skeleton	skeleton	k1gInSc1	skeleton
a	a	k8xC	a
nejpočetněji	početně	k6eAd3	početně
Zombie	Zombie	k1gFnPc4	Zombie
Pigman	Pigman	k1gMnSc1	Pigman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Netheru	Nether	k1gInSc6	Nether
se	se	k3xPyFc4	se
také	také	k9	také
nalézají	nalézat	k5eAaImIp3nP	nalézat
Nether	Nethra	k1gFnPc2	Nethra
pevnosti	pevnost	k1gFnSc3	pevnost
z	z	k7c2	z
Nether	Nethra	k1gFnPc2	Nethra
Bricků	Bricek	k1gMnPc2	Bricek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
wither	withra	k1gFnPc2	withra
skeletoni	skeleton	k1gMnPc1	skeleton
<g/>
,	,	kIx,	,
blazové	blazové	k2eAgMnPc1d1	blazové
i	i	k8xC	i
normální	normální	k2eAgMnPc1d1	normální
skeletoni	skeleton	k1gMnPc1	skeleton
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Netheru	Nether	k1gInSc6	Nether
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Nether	Nethra	k1gFnPc2	Nethra
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
Alpha	Alpha	k1gFnSc1	Alpha
1.2	[number]	k4	1.2
<g/>
.0	.0	k4	.0
(	(	kIx(	(
<g/>
Halloween	Halloween	k2eAgInSc1d1	Halloween
Update	update	k1gInSc1	update
<g/>
)	)	kIx)	)
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
vylepšen	vylepšen	k2eAgMnSc1d1	vylepšen
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
1.0	[number]	k4	1.0
<g/>
.0	.0	k4	.0
(	(	kIx(	(
<g/>
Nether	Nethra	k1gFnPc2	Nethra
Update	update	k1gInSc1	update
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
End	End	k1gFnSc1	End
(	(	kIx(	(
<g/>
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
na	na	k7c6	na
End	End	k1gFnSc6	End
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
dimenze	dimenze	k1gFnSc1	dimenze
(	(	kIx(	(
<g/>
po	po	k7c6	po
dohrání	dohrání	k1gNnSc6	dohrání
hra	hra	k1gFnSc1	hra
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
dostane	dostat	k5eAaPmIp3nS	dostat
po	po	k7c6	po
doplnění	doplnění	k1gNnSc6	doplnění
ender	endra	k1gFnPc2	endra
očí	oko	k1gNnPc2	oko
do	do	k7c2	do
portálu	portál	k1gInSc2	portál
ve	v	k7c6	v
strongholdu	stronghold	k1gInSc6	stronghold
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
najde	najít	k5eAaPmIp3nS	najít
házením	házení	k1gNnSc7	házení
ender	endra	k1gFnPc2	endra
očí	oko	k1gNnPc2	oko
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
podložím	podloží	k1gNnSc7	podloží
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
lze	lze	k6eAd1	lze
spadnout	spadnout	k5eAaPmF	spadnout
do	do	k7c2	do
voidu	void	k1gInSc2	void
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
prázdný	prázdný	k2eAgInSc1d1	prázdný
prostor	prostor	k1gInSc1	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgInSc6	první
vstupu	vstup	k1gInSc6	vstup
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
drak	drak	k1gInSc1	drak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
létá	létat	k5eAaImIp3nS	létat
<g/>
,	,	kIx,	,
plive	plive	k?	plive
oheň	oheň	k1gInSc1	oheň
(	(	kIx(	(
<g/>
Dragon	Dragon	k1gMnSc1	Dragon
breath	breath	k1gMnSc1	breath
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dračí	dračí	k2eAgInSc1d1	dračí
dech	dech	k1gInSc1	dech
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
lektvarů	lektvar	k1gInPc2	lektvar
<g/>
)	)	kIx)	)
vystrkuje	vystrkovat	k5eAaImIp3nS	vystrkovat
hráče	hráč	k1gMnPc4	hráč
přes	přes	k7c4	přes
okraj	okraj	k1gInSc4	okraj
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
střílí	střílet	k5eAaImIp3nP	střílet
ohnivé	ohnivý	k2eAgFnPc1d1	ohnivá
koule	koule	k1gFnPc1	koule
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
po	po	k7c6	po
dopadnutí	dopadnutí	k1gNnSc6	dopadnutí
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
drači	drač	k1gFnSc3	drač
dech	dech	k1gInSc4	dech
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
Endermanů	Enderman	k1gMnPc2	Enderman
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
dimenze	dimenze	k1gFnSc1	dimenze
je	být	k5eAaImIp3nS	být
stvořena	stvořit	k5eAaPmNgFnS	stvořit
z	z	k7c2	z
Endstone	Endston	k1gInSc5	Endston
a	a	k8xC	a
obsidiánu	obsidián	k1gInSc6	obsidián
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
End	End	k1gFnSc2	End
bloku	blok	k1gInSc2	blok
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
povrch	povrch	k1gInSc1	povrch
<g/>
,	,	kIx,	,
z	z	k7c2	z
obsidiánu	obsidián	k1gInSc2	obsidián
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
jsou	být	k5eAaImIp3nP	být
Ender	Ender	k1gInSc4	Ender
Crystaly	Crystaly	k1gFnPc2	Crystaly
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
uzdravují	uzdravovat	k5eAaImIp3nP	uzdravovat
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zničeny	zničit	k5eAaPmNgInP	zničit
úderem	úder	k1gInSc7	úder
či	či	k8xC	či
projektilem	projektil	k1gInSc7	projektil
(	(	kIx(	(
<g/>
sněhová	sněhový	k2eAgFnSc1d1	sněhová
koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
vajčko	vajčko	k1gNnSc1	vajčko
<g/>
,	,	kIx,	,
šíp	šíp	k1gInSc1	šíp
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zabití	zabití	k1gNnSc6	zabití
draka	drak	k1gMnSc2	drak
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
začnou	začít	k5eAaPmIp3nP	začít
padat	padat	k5eAaImF	padat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
(	(	kIx(	(
<g/>
XP	XP	kA	XP
<g/>
)	)	kIx)	)
a	a	k8xC	a
pod	pod	k7c7	pod
drakem	drak	k1gInSc7	drak
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
portál	portál	k1gInSc1	portál
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Overworldu	Overworld	k1gInSc2	Overworld
(	(	kIx(	(
<g/>
normálního	normální	k2eAgInSc2d1	normální
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
s	s	k7c7	s
dračím	dračí	k2eAgNnSc7d1	dračí
vejcem	vejce	k1gNnSc7	vejce
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
portál	portál	k1gInSc1	portál
do	do	k7c2	do
ostatních	ostatní	k2eAgInPc2d1	ostatní
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
pomocí	pomocí	k7c2	pomocí
ender	endra	k1gFnPc2	endra
perly	perla	k1gFnSc2	perla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
portálu	portál	k1gInSc2	portál
se	se	k3xPyFc4	se
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
titulky	titulek	k1gInPc1	titulek
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
zpátky	zpátky	k6eAd1	zpátky
v	v	k7c6	v
Overworldu	Overworld	k1gInSc6	Overworld
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
draka	drak	k1gMnSc2	drak
hráč	hráč	k1gMnSc1	hráč
dostane	dostat	k5eAaPmIp3nS	dostat
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
je	on	k3xPp3gFnPc4	on
dostupné	dostupný	k2eAgFnPc4d1	dostupná
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc4	šest
možností	možnost	k1gFnPc2	možnost
generování	generování	k1gNnSc2	generování
mapy	mapa	k1gFnSc2	mapa
<g/>
:	:	kIx,	:
Default	default	k1gInSc1	default
(	(	kIx(	(
<g/>
výchozí	výchozí	k2eAgMnSc1d1	výchozí
<g/>
)	)	kIx)	)
-	-	kIx~	-
klasicky	klasicky	k6eAd1	klasicky
generovaný	generovaný	k2eAgInSc1d1	generovaný
svět	svět	k1gInSc1	svět
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
biomy	biom	k1gInPc4	biom
Superflat	Superfle	k1gNnPc2	Superfle
(	(	kIx(	(
<g/>
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
rovina	rovina	k1gFnSc1	rovina
<g/>
)	)	kIx)	)
-	-	kIx~	-
svět	svět	k1gInSc1	svět
generovaný	generovaný	k2eAgInSc1d1	generovaný
po	po	k7c6	po
patrech	patro	k1gNnPc6	patro
–	–	k?	–
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
výšce	výška	k1gFnSc6	výška
terénu	terén	k1gInSc2	terén
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
jediný	jediný	k2eAgInSc4d1	jediný
typ	typ	k1gInSc4	typ
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
generování	generování	k1gNnSc6	generování
světa	svět	k1gInSc2	svět
zadat	zadat	k5eAaPmF	zadat
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
výšce	výška	k1gFnSc6	výška
budou	být	k5eAaImBp3nP	být
jaké	jaký	k3yRgInPc1	jaký
bloky	blok	k1gInPc1	blok
a	a	k8xC	a
které	který	k3yQgFnPc1	který
struktury	struktura	k1gFnPc1	struktura
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
generovat	generovat	k5eAaImF	generovat
<g/>
;	;	kIx,	;
hra	hra	k1gFnSc1	hra
nabízí	nabízet	k5eAaImIp3nS	nabízet
několik	několik	k4yIc4	několik
předpřipravených	předpřipravený	k2eAgNnPc2d1	předpřipravené
nastavení	nastavení	k1gNnPc2	nastavení
Large	Larg	k1gMnSc2	Larg
Biomes	Biomesa	k1gFnPc2	Biomesa
(	(	kIx(	(
<g/>
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
biomy	bioma	k1gFnSc2	bioma
<g/>
)	)	kIx)	)
-	-	kIx~	-
klasicky	klasicky	k6eAd1	klasicky
generovaný	generovaný	k2eAgInSc1d1	generovaný
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
biomy	bioma	k1gFnPc1	bioma
jsou	být	k5eAaImIp3nP	být
16	[number]	k4	16
<g/>
×	×	k?	×
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
klasickém	klasický	k2eAgInSc6d1	klasický
světě	svět	k1gInSc6	svět
Amplified	Amplified	k1gInSc1	Amplified
(	(	kIx(	(
<g/>
zesílený	zesílený	k2eAgMnSc1d1	zesílený
<g/>
)	)	kIx)	)
-	-	kIx~	-
klasicky	klasicky	k6eAd1	klasicky
generovaný	generovaný	k2eAgInSc1d1	generovaný
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výška	výška	k1gFnSc1	výška
terénu	terén	k1gInSc2	terén
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
maximální	maximální	k2eAgFnSc1d1	maximální
hranice	hranice	k1gFnSc1	hranice
256	[number]	k4	256
bloků	blok	k1gInPc2	blok
Customized	Customized	k1gMnSc1	Customized
(	(	kIx(	(
<g/>
přispůsobený	přispůsobený	k2eAgMnSc1d1	přispůsobený
<g/>
)	)	kIx)	)
-	-	kIx~	-
hráčem	hráč	k1gMnSc7	hráč
namodifikovaný	namodifikovaný	k2eAgInSc4d1	namodifikovaný
svět	svět	k1gInSc4	svět
Debug	Debuga	k1gFnPc2	Debuga
Mode	modus	k1gInSc5	modus
-	-	kIx~	-
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
bloků	blok	k1gInPc2	blok
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
poskládány	poskládán	k2eAgInPc1d1	poskládán
kostku	kostka	k1gFnSc4	kostka
od	od	k7c2	od
sebe	se	k3xPyFc2	se
v	v	k7c6	v
prázdném	prázdný	k2eAgInSc6d1	prázdný
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vygenerovat	vygenerovat	k5eAaPmF	vygenerovat
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
pří	pře	k1gFnSc7	pře
výběru	výběr	k1gInSc2	výběr
držíte	držet	k5eAaImIp2nP	držet
shift	shift	k1gInSc4	shift
Mobové	Mobová	k1gFnSc2	Mobová
jsou	být	k5eAaImIp3nP	být
živá	živý	k2eAgNnPc4d1	živé
stvoření	stvoření	k1gNnPc4	stvoření
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
setká	setkat	k5eAaPmIp3nS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
podle	podle	k7c2	podle
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
:	:	kIx,	:
Nikdy	nikdy	k6eAd1	nikdy
hráče	hráč	k1gMnSc4	hráč
nenapadnou	napadnout	k5eNaPmIp3nP	napadnout
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
mu	on	k3xPp3gMnSc3	on
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
prospěšní	prospěšný	k2eAgMnPc1d1	prospěšný
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
například	například	k6eAd1	například
vesničan	vesničan	k1gMnSc1	vesničan
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
oliheň	oliheň	k1gFnSc1	oliheň
<g/>
,	,	kIx,	,
slepice	slepice	k1gFnSc1	slepice
<g/>
,	,	kIx,	,
kráva	kráva	k1gFnSc1	kráva
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
a	a	k8xC	a
kůň	kůň	k1gMnSc1	kůň
Na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
nezaútočí	zaútočit	k5eNaPmIp3nS	zaútočit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
vyprovokováni	vyprovokován	k2eAgMnPc1d1	vyprovokován
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
Pigman	Pigman	k1gMnSc1	Pigman
a	a	k8xC	a
Enderman	Enderman	k1gMnSc1	Enderman
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyprovokování	vyprovokování	k1gNnSc6	vyprovokování
jednoho	jeden	k4xCgMnSc2	jeden
člena	člen	k1gMnSc2	člen
skupiny	skupina	k1gFnSc2	skupina
vlk	vlk	k1gMnSc1	vlk
a	a	k8xC	a
pigman	pigman	k1gMnSc1	pigman
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nP	bránit
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
útočí	útočit	k5eAaImIp3nS	útočit
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyprovokování	vyprovokování	k1gNnSc3	vyprovokování
Endermana	Enderman	k1gMnSc2	Enderman
stačí	stačit	k5eAaBmIp3nS	stačit
pohled	pohled	k1gInSc4	pohled
do	do	k7c2	do
jeho	jeho	k3xOp3gNnPc2	jeho
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgNnSc1d1	agresivní
stvoření	stvoření	k1gNnSc1	stvoření
ihned	ihned	k6eAd1	ihned
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ho	on	k3xPp3gMnSc4	on
zaregistrují	zaregistrovat	k5eAaPmIp3nP	zaregistrovat
(	(	kIx(	(
<g/>
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
16	[number]	k4	16
bloků	blok	k1gInPc2	blok
<g/>
;	;	kIx,	;
Zombie	Zombie	k1gFnSc1	Zombie
však	však	k9	však
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
určitou	určitý	k2eAgFnSc4d1	určitá
úroveň	úroveň	k1gFnSc4	úroveň
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zrodili	zrodit	k5eAaPmAgMnP	zrodit
(	(	kIx(	(
<g/>
přichází	přicházet	k5eAaImIp3nS	přicházet
za	za	k7c2	za
tmy	tma	k1gFnSc2	tma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
agresivní	agresivní	k2eAgNnSc4d1	agresivní
stvoření	stvoření	k1gNnSc4	stvoření
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Creeper	Creeper	k1gInSc1	Creeper
<g/>
,	,	kIx,	,
Skeleton	skeleton	k1gInSc1	skeleton
<g/>
,	,	kIx,	,
Zombie	Zombie	k1gFnSc1	Zombie
<g/>
,	,	kIx,	,
Ghast	Ghast	k1gFnSc1	Ghast
<g/>
,	,	kIx,	,
Blaze	blaze	k6eAd1	blaze
<g/>
,	,	kIx,	,
Endermite	Endermit	k1gInSc5	Endermit
<g/>
,	,	kIx,	,
Witch	Witcha	k1gFnPc2	Witcha
<g/>
,	,	kIx,	,
Wither	Withra	k1gFnPc2	Withra
skeleton	skeleton	k1gInSc1	skeleton
<g/>
,	,	kIx,	,
Husk	Husk	k1gInSc1	Husk
a	a	k8xC	a
Stray	Straa	k1gFnPc1	Straa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
:	:	kIx,	:
normální	normální	k2eAgMnSc1d1	normální
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
x	x	k?	x
<g/>
2	[number]	k4	2
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
za	za	k7c2	za
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
neutrálními	neutrální	k2eAgMnPc7d1	neutrální
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeskynní	jeskynní	k2eAgMnPc1d1	jeskynní
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
velcí	velký	k2eAgMnPc1d1	velký
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
1	[number]	k4	1
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
blok	blok	k1gInSc1	blok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeskynní	jeskynní	k2eAgMnSc1d1	jeskynní
pavouk	pavouk	k1gMnSc1	pavouk
může	moct	k5eAaImIp3nS	moct
hráče	hráč	k1gMnPc4	hráč
otrávit	otrávit	k5eAaPmF	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc4	některý
zvířata	zvíře	k1gNnPc4	zvíře
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
ochočit	ochočit	k5eAaPmF	ochočit
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
(	(	kIx(	(
<g/>
po	po	k7c6	po
ochočení	ochočení	k1gNnSc6	ochočení
ocelota	ocelot	k1gMnSc2	ocelot
rybou	ryba	k1gFnSc7	ryba
<g/>
)	)	kIx)	)
-	-	kIx~	-
plaší	plašit	k5eAaImIp3nP	plašit
Creepery	Creepera	k1gFnPc1	Creepera
<g/>
.	.	kIx.	.
</s>
<s>
Pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
po	po	k7c6	po
ochočení	ochočení	k1gNnSc6	ochočení
vlka	vlk	k1gMnSc2	vlk
kostí	kost	k1gFnPc2	kost
<g/>
)	)	kIx)	)
-	-	kIx~	-
brání	bránit	k5eAaImIp3nS	bránit
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
plaší	plašit	k5eAaImIp3nP	plašit
kostlivce	kostlivec	k1gMnSc4	kostlivec
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
pasivní	pasivní	k2eAgFnPc4d1	pasivní
moby	moba	k1gFnPc4	moba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
hráč	hráč	k1gMnSc1	hráč
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Barvivem	barvivo	k1gNnSc7	barvivo
lze	lze	k6eAd1	lze
změnit	změnit	k5eAaPmF	změnit
barva	barva	k1gFnSc1	barva
obojku	obojek	k1gInSc2	obojek
<g/>
.	.	kIx.	.
</s>
<s>
Kráva	kráva	k1gFnSc1	kráva
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
slepice	slepice	k1gFnSc1	slepice
<g/>
,	,	kIx,	,
prase	prase	k1gNnSc1	prase
(	(	kIx(	(
<g/>
následují	následovat	k5eAaImIp3nP	následovat
hráče	hráč	k1gMnPc4	hráč
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
pšenici	pšenice	k1gFnSc6	pšenice
[	[	kIx(	[
<g/>
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
kráva	kráva	k1gFnSc1	kráva
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
mrkev	mrkev	k1gFnSc1	mrkev
[	[	kIx(	[
<g/>
prase	prase	k1gNnSc1	prase
<g/>
,	,	kIx,	,
zajíc	zajíc	k1gMnSc1	zajíc
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
semínka	semínko	k1gNnPc4	semínko
[	[	kIx(	[
<g/>
slepice	slepice	k1gFnSc1	slepice
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
pampeliška	pampeliška	k1gFnSc1	pampeliška
[	[	kIx(	[
<g/>
jen	jen	k9	jen
<g/>
,	,	kIx,	,
<g/>
zajíc	zajíc	k1gMnSc1	zajíc
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
chovaní	chovaný	k2eAgMnPc1d1	chovaný
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
masu	maso	k1gNnSc3	maso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalším	další	k2eAgFnPc3d1	další
surovinám	surovina	k1gFnPc3	surovina
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Kůň	kůň	k1gMnSc1	kůň
-	-	kIx~	-
určen	určen	k2eAgInSc1d1	určen
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
<g/>
,	,	kIx,	,
podržením	podržení	k1gNnSc7	podržení
mezerníku	mezerník	k1gInSc2	mezerník
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
skákat	skákat	k5eAaImF	skákat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zabití	zabití	k1gNnSc6	zabití
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
padá	padat	k5eAaImIp3nS	padat
kůže	kůže	k1gFnSc1	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
koňské	koňský	k2eAgNnSc4d1	koňské
brnění	brnění	k1gNnSc4	brnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
nelze	lze	k6eNd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
dungeonech	dungeon	k1gInPc6	dungeon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
skočit	skočit	k5eAaPmF	skočit
max	max	kA	max
<g/>
.	.	kIx.	.
5	[number]	k4	5
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Osel	osel	k1gMnSc1	osel
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
umístit	umístit	k5eAaPmF	umístit
truhlu	truhla	k1gFnSc4	truhla
<g/>
.	.	kIx.	.
</s>
<s>
Jezdí	jezdit	k5eAaImIp3nS	jezdit
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
</s>
<s>
Mula	mula	k1gFnSc1	mula
-	-	kIx~	-
kříženec	kříženec	k1gMnSc1	kříženec
osla	osel	k1gMnSc2	osel
a	a	k8xC	a
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaBmF	nalézt
volně	volně	k6eAd1	volně
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Lama	lama	k1gFnSc1	lama
-	-	kIx~	-
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
ochočit	ochočit	k5eAaPmF	ochočit
a	a	k8xC	a
umístit	umístit	k5eAaPmF	umístit
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
truhlu	truhla	k1gFnSc4	truhla
<g/>
.	.	kIx.	.
počet	počet	k1gInSc4	počet
slotů	slot	k1gInPc2	slot
v	v	k7c6	v
truhle	truhla	k1gFnSc6	truhla
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
tzv.	tzv.	kA	tzv.
síla	síla	k1gFnSc1	síla
lamy	lama	k1gFnSc2	lama
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
ochočenou	ochočený	k2eAgFnSc4d1	ochočená
lamu	lama	k1gFnSc4	lama
se	se	k3xPyFc4	se
seřadí	seřadit	k5eAaPmIp3nS	seřadit
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
lam	lama	k1gFnPc2	lama
<g/>
.	.	kIx.	.
lamu	lama	k1gFnSc4	lama
můžete	moct	k5eAaImIp2nP	moct
ozdobit	ozdobit	k5eAaPmF	ozdobit
kobercem	koberec	k1gInSc7	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Papoušek	Papoušek	k1gMnSc1	Papoušek
-	-	kIx~	-
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ochočit	ochočit	k5eAaPmF	ochočit
pomocí	pomocí	k7c2	pomocí
sušenek	sušenka	k1gFnPc2	sušenka
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
papoušek	papoušek	k1gMnSc1	papoušek
ochočený	ochočený	k2eAgMnSc1d1	ochočený
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	s	k7c7	s
kliknutím	kliknutí	k1gNnSc7	kliknutí
myši	myš	k1gFnSc2	myš
papouška	papoušek	k1gMnSc4	papoušek
posadit	posadit	k5eAaPmF	posadit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ovšem	ovšem	k9	ovšem
nesedí	sedit	k5eNaImIp3nS	sedit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
přiletí	přiletět	k5eAaPmIp3nS	přiletět
hráči	hráč	k1gMnSc3	hráč
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
rameno	rameno	k1gNnSc4	rameno
(	(	kIx(	(
<g/>
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
na	na	k7c6	na
ramenech	rameno	k1gNnPc6	rameno
dva	dva	k4xCgMnPc4	dva
papoušky	papoušek	k1gMnPc4	papoušek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papoušek	Papoušek	k1gMnSc1	Papoušek
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
pěti	pět	k4xCc6	pět
barvách	barva	k1gFnPc6	barva
(	(	kIx(	(
<g/>
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
šedé	šedý	k2eAgFnPc1d1	šedá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
modré	modrý	k2eAgFnPc1d1	modrá
a	a	k8xC	a
světle	světle	k6eAd1	světle
modré	modrý	k2eAgNnSc1d1	modré
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
u	u	k7c2	u
papoušků	papoušek	k1gMnPc2	papoušek
položí	položit	k5eAaPmIp3nS	položit
hrací	hrací	k2eAgFnSc1d1	hrací
skříň	skříň	k1gFnSc1	skříň
a	a	k8xC	a
následně	následně	k6eAd1	následně
vloží	vložit	k5eAaPmIp3nS	vložit
libovolná	libovolný	k2eAgFnSc1d1	libovolná
gramofonová	gramofonový	k2eAgFnSc1d1	gramofonová
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
papouškové	papoušek	k1gMnPc1	papoušek
začnou	začít	k5eAaPmIp3nP	začít
měnit	měnit	k5eAaImF	měnit
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
"	"	kIx"	"
<g/>
tančit	tančit	k5eAaImF	tančit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
hudba	hudba	k1gFnSc1	hudba
přestane	přestat	k5eAaPmIp3nS	přestat
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Papoušek	Papoušek	k1gMnSc1	Papoušek
dokáže	dokázat	k5eAaPmIp3nS	dokázat
imitovat	imitovat	k5eAaBmF	imitovat
zvuky	zvuk	k1gInPc4	zvuk
nepřátelských	přátelský	k2eNgInPc2d1	nepřátelský
mobů	mob	k1gInPc2	mob
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vytvořeni	vytvořen	k2eAgMnPc1d1	vytvořen
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
Golem	Golem	k1gMnSc1	Golem
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
též	též	k9	též
objevit	objevit	k5eAaPmF	objevit
ve	v	k7c4	v
větší	veliký	k2eAgFnSc4d2	veliký
NPC	NPC	kA	NPC
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
agresivní	agresivní	k2eAgMnSc1d1	agresivní
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
neutrální	neutrální	k2eAgInPc1d1	neutrální
moby	moby	k1gInPc1	moby
<g/>
,	,	kIx,	,
po	po	k7c6	po
napadení	napadení	k1gNnSc2	napadení
vás	vy	k3xPp2nPc4	vy
může	moct	k5eAaImIp3nS	moct
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nezaútočí	zaútočit	k5eNaPmIp3nS	zaútočit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sněžný	sněžný	k2eAgMnSc1d1	sněžný
Golem	Golem	k1gMnSc1	Golem
(	(	kIx(	(
<g/>
sněhulák	sněhulák	k1gMnSc1	sněhulák
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
,	,	kIx,	,
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
pomalu	pomalu	k6eAd1	pomalu
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
od	od	k7c2	od
novějších	nový	k2eAgFnPc2d2	novější
verzí	verze	k1gFnPc2	verze
mu	on	k3xPp3gMnSc3	on
můžete	moct	k5eAaImIp2nP	moct
sundat	sundat	k5eAaPmF	sundat
pomocí	pomocí	k7c2	pomocí
nůžek	nůžky	k1gFnPc2	nůžky
jeho	jeho	k3xOp3gFnSc4	jeho
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
dýně	dýně	k1gFnSc1	dýně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vůdcovští	vůdcovský	k2eAgMnPc1d1	vůdcovský
mobové	mobus	k1gMnPc1	mobus
mají	mít	k5eAaImIp3nP	mít
složitější	složitý	k2eAgInPc4d2	složitější
útočné	útočný	k2eAgInPc4d1	útočný
vzory	vzor	k1gInPc4	vzor
a	a	k8xC	a
pohyby	pohyb	k1gInPc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
berou	brát	k5eAaImIp3nP	brát
výrazně	výrazně	k6eAd1	výrazně
více	hodně	k6eAd2	hodně
životů	život	k1gInPc2	život
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
většinou	většinou	k6eAd1	většinou
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
mobové	mobus	k1gMnPc1	mobus
<g/>
.	.	kIx.	.
</s>
<s>
Drak	drak	k1gInSc1	drak
z	z	k7c2	z
Endu	Endus	k1gInSc2	Endus
(	(	kIx(	(
<g/>
Ender	Ender	k1gMnSc1	Ender
Dragon	Dragon	k1gMnSc1	Dragon
<g/>
)	)	kIx)	)
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obrovského	obrovský	k2eAgMnSc4d1	obrovský
draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
létá	létat	k5eAaImIp3nS	létat
v	v	k7c6	v
Endu	Endus	k1gInSc6	Endus
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
200	[number]	k4	200
životů	život	k1gInPc2	život
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
zregenerovat	zregenerovat	k5eAaPmF	zregenerovat
díky	dík	k1gInPc1	dík
Ender	Endra	k1gFnPc2	Endra
krystalům	krystal	k1gInPc3	krystal
<g/>
,	,	kIx,	,
umístěných	umístěný	k2eAgInPc2d1	umístěný
na	na	k7c6	na
věžích	věž	k1gFnPc6	věž
z	z	k7c2	z
obsidiánu	obsidián	k1gInSc2	obsidián
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
draka	drak	k1gMnSc4	drak
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zabití	zabití	k1gNnSc2	zabití
draka	drak	k1gMnSc2	drak
získáte	získat	k5eAaPmIp2nP	získat
1000	[number]	k4	1000
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
dračí	dračí	k2eAgNnPc4d1	dračí
vejce	vejce	k1gNnPc4	vejce
a	a	k8xC	a
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
portál	portál	k1gInSc1	portál
do	do	k7c2	do
normálního	normální	k2eAgInSc2d1	normální
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
1.9	[number]	k4	1.9
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
objeví	objevit	k5eAaPmIp3nS	objevit
portál	portál	k1gInSc1	portál
do	do	k7c2	do
"	"	kIx"	"
<g/>
Temných	temný	k2eAgFnPc2d1	temná
hlubin	hlubina	k1gFnPc2	hlubina
Endu	Endus	k1gInSc2	Endus
<g/>
"	"	kIx"	"
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
draka	drak	k1gMnSc4	drak
znovu	znovu	k6eAd1	znovu
oživit	oživit	k5eAaPmF	oživit
vyskládáním	vyskládání	k1gNnSc7	vyskládání
čtyř	čtyři	k4xCgFnPc2	čtyři
Ender	Endra	k1gFnPc2	Endra
krystalů	krystal	k1gInPc2	krystal
kolem	kolem	k7c2	kolem
portálu	portál	k1gInSc2	portál
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
zabití	zabití	k1gNnSc4	zabití
pak	pak	k6eAd1	pak
získáte	získat	k5eAaPmIp2nP	získat
už	už	k6eAd1	už
jen	jen	k9	jen
500	[number]	k4	500
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Wither	Withra	k1gFnPc2	Withra
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tříhlavou	tříhlavý	k2eAgFnSc4d1	tříhlavá
létající	létající	k2eAgFnSc4d1	létající
příšeru	příšera	k1gFnSc4	příšera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
střílí	střílet	k5eAaImIp3nS	střílet
tzv.	tzv.	kA	tzv.
Witherovy	Witherův	k2eAgFnPc4d1	Witherův
lebky	lebka	k1gFnPc4	lebka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
destruktivní	destruktivní	k2eAgInPc4d1	destruktivní
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
pokud	pokud	k8xS	pokud
hráče	hráč	k1gMnPc4	hráč
taková	takový	k3xDgFnSc1	takový
hlava	hlava	k1gFnSc1	hlava
trefí	trefit	k5eAaPmIp3nS	trefit
<g/>
,	,	kIx,	,
ubudou	ubýt	k5eAaPmIp3nP	ubýt
mu	on	k3xPp3gMnSc3	on
4	[number]	k4	4
srdíčka	srdíčko	k1gNnSc2	srdíčko
(	(	kIx(	(
<g/>
8	[number]	k4	8
bodů	bod	k1gInPc2	bod
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
dostane	dostat	k5eAaPmIp3nS	dostat
elixírový	elixírový	k2eAgInSc1d1	elixírový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
usychání	usychání	k1gNnSc1	usychání
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hráči	hráč	k1gMnPc1	hráč
ubírá	ubírat	k5eAaImIp3nS	ubírat
životy	život	k1gInPc7	život
a	a	k8xC	a
Wither	Withra	k1gFnPc2	Withra
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
<g/>
.	.	kIx.	.
</s>
<s>
Wither	Withra	k1gFnPc2	Withra
se	se	k3xPyFc4	se
přírodně	přírodně	k6eAd1	přírodně
nikde	nikde	k6eAd1	nikde
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jej	on	k3xPp3gInSc4	on
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
vyrobit	vyrobit	k5eAaPmF	vyrobit
naskládáním	naskládání	k1gNnSc7	naskládání
4	[number]	k4	4
písků	písek	k1gInPc2	písek
duší	duše	k1gFnPc2	duše
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
písmena	písmeno	k1gNnSc2	písmeno
T	T	kA	T
a	a	k8xC	a
3	[number]	k4	3
lebek	lebka	k1gFnPc2	lebka
wither	withra	k1gFnPc2	withra
kostlivců	kostlivec	k1gMnPc2	kostlivec
<g/>
.	.	kIx.	.
</s>
<s>
Wither	Withra	k1gFnPc2	Withra
má	mít	k5eAaImIp3nS	mít
150	[number]	k4	150
srdíček	srdíčko	k1gNnPc2	srdíčko
(	(	kIx(	(
<g/>
300	[number]	k4	300
životů	život	k1gInPc2	život
<g/>
)	)	kIx)	)
a	a	k8xC	a
napadá	napadat	k5eAaPmIp3nS	napadat
všechny	všechen	k3xTgMnPc4	všechen
živé	živý	k2eAgMnPc4d1	živý
tvory	tvor	k1gMnPc4	tvor
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zabití	zabití	k1gNnSc6	zabití
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
padne	padnout	k5eAaPmIp3nS	padnout
Nether	Nethra	k1gFnPc2	Nethra
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
majáku	maják	k1gInSc2	maják
(	(	kIx(	(
<g/>
beaconu	beacon	k1gInSc2	beacon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgFnSc2d1	oficiální
verze	verze	k1gFnSc2	verze
hry	hra	k1gFnSc2	hra
vývojáři	vývojář	k1gMnPc7	vývojář
pravidelně	pravidelně	k6eAd1	pravidelně
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
tzv.	tzv.	kA	tzv.
snapshoty	snapshota	k1gFnPc1	snapshota
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
jejich	jejich	k3xOp3gNnSc2	jejich
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
patche	patch	k1gInSc2	patch
jsou	být	k5eAaImIp3nP	být
vydány	vydat	k5eAaPmNgFnP	vydat
tzv.	tzv.	kA	tzv.
pre-release	preelease	k6eAd1	pre-release
verze	verze	k1gFnPc1	verze
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
tvůrce	tvůrce	k1gMnPc4	tvůrce
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
command	commanda	k1gFnPc2	commanda
id	idy	k1gFnPc2	idy
mobů	mob	k1gMnPc2	mob
<g/>
:	:	kIx,	:
witherBoss	witherBossa	k1gFnPc2	witherBossa
=	=	kIx~	=
wither	withra	k1gFnPc2	withra
<g/>
,	,	kIx,	,
ozelot	ozelot	k1gMnSc1	ozelot
=	=	kIx~	=
ocelot	ocelot	k1gMnSc1	ocelot
<g/>
...	...	k?	...
2	[number]	k4	2
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
vyprané	vypraný	k2eAgFnPc4d1	vypraná
<g/>
"	"	kIx"	"
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
bloky	blok	k1gInPc1	blok
<g/>
:	:	kIx,	:
16	[number]	k4	16
barev	barva	k1gFnPc2	barva
<g/>
:	:	kIx,	:
glazed	glazed	k1gInSc1	glazed
terracota	terracot	k1gMnSc2	terracot
<g/>
,	,	kIx,	,
concreate	concreat	k1gInSc5	concreat
powder	powdrat	k5eAaPmRp2nS	powdrat
<g/>
,	,	kIx,	,
concreate	concreat	k1gMnSc5	concreat
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Narrator	Narrator	k1gInSc1	Narrator
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zapne	zapnout	k5eAaPmIp3nS	zapnout
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
bude	být	k5eAaImBp3nS	být
v	v	k7c4	v
chatu	chata	k1gFnSc4	chata
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Přejmenovaný	přejmenovaný	k2eAgInSc1d1	přejmenovaný
stained	stained	k1gInSc1	stained
hardened	hardened	k1gInSc1	hardened
clay	claa	k1gMnSc2	claa
na	na	k7c4	na
terracota	terracot	k1gMnSc4	terracot
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
achivementů	achivement	k1gInPc2	achivement
nyní	nyní	k6eAd1	nyní
advancementy	advancement	k1gInPc1	advancement
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
ůpravy	ůprava	k1gFnSc2	ůprava
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
zvuky	zvuk	k1gInPc1	zvuk
note	note	k1gNnSc2	note
blocků	block	k1gInPc2	block
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Přidán	přidán	k2eAgInSc4d1	přidán
"	"	kIx"	"
<g/>
receptář	receptář	k1gInSc4	receptář
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
recipe	recipe	k1gNnSc1	recipe
book	booka	k1gFnPc2	booka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
odemčených	odemčený	k2eAgInPc2d1	odemčený
receptů	recept	k1gInPc2	recept
<g/>
.	.	kIx.	.
</s>
<s>
Používa	Používa	k6eAd1	Používa
stejný	stejný	k2eAgInSc1d1	stejný
custom	custom	k1gInSc1	custom
syntax	syntax	k1gFnSc4	syntax
jako	jako	k8xS	jako
custom	custom	k1gInSc4	custom
advancementy	advancement	k1gInPc1	advancement
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
itemy	item	k1gInPc1	item
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odemykají	odemykat	k5eAaImIp3nP	odemykat
speciální	speciální	k2eAgFnSc7d1	speciální
"	"	kIx"	"
<g/>
kondicí	kondice	k1gFnSc7	kondice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
U	u	k7c2	u
lodě	loď	k1gFnSc2	loď
třeba	třeba	k8wRxS	třeba
dotknutí	dotknutí	k1gNnSc4	dotknutí
se	se	k3xPyFc4	se
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
<g/>
!	!	kIx.	!
</s>
<s>
ochočitelní	ochočitelný	k2eAgMnPc1d1	ochočitelný
semínkama	semínkama	k?	semínkama
<g/>
,	,	kIx,	,
spawnují	spawnovat	k5eAaPmIp3nP	spawnovat
se	se	k3xPyFc4	se
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozor	pozor	k1gInSc4	pozor
<g/>
!	!	kIx.	!
</s>
<s>
džungle	džungle	k1gFnPc1	džungle
vygenerované	vygenerovaný	k2eAgFnPc1d1	vygenerovaná
před	před	k7c4	před
1.12	[number]	k4	1.12
nefungují	fungovat	k5eNaImIp3nP	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
zabít	zabít	k5eAaPmF	zabít
nakrmením	nakrmení	k1gNnSc7	nakrmení
sušenkama	sušenkama	k?	sušenkama
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
mojang	mojang	k1gMnSc1	mojang
je	on	k3xPp3gInPc4	on
udělal	udělat	k5eAaPmAgMnS	udělat
před	před	k7c4	před
relese	relese	k1gFnPc4	relese
ochočitelné	ochočitelný	k2eAgNnSc4d1	ochočitelné
sušenkama	sušenkama	k?	sušenkama
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sušenky	sušenka	k1gFnPc1	sušenka
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
čokoládou	čokoláda	k1gFnSc7	čokoláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
papoušky	papoušek	k1gMnPc4	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
viděly	vidět	k5eAaImAgFnP	vidět
i	i	k9	i
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc1	ten
zkoušely	zkoušet	k5eAaImAgInP	zkoušet
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Odstaněn	Odstaněn	k2eAgInSc1d1	Odstaněn
Herobrine	Herobrin	k1gInSc5	Herobrin
<g/>
...	...	k?	...
Dne	den	k1gInSc2	den
14.11	[number]	k4	14.11
<g/>
.2016	.2016	k4	.2016
vydal	vydat	k5eAaPmAgMnS	vydat
Mojang	Mojang	k1gMnSc1	Mojang
verzi	verze	k1gFnSc4	verze
Minecraftu	Minecrafta	k1gFnSc4	Minecrafta
1.11	[number]	k4	1.11
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Exploration	Exploration	k1gInSc1	Exploration
update	update	k1gInSc4	update
<g/>
,	,	kIx,	,
hlavními	hlavní	k2eAgFnPc7d1	hlavní
novinkami	novinka	k1gFnPc7	novinka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Spawn	Spawn	k1gNnSc4	Spawn
eggy	egga	k1gFnSc2	egga
na	na	k7c4	na
Wither	Withra	k1gFnPc2	Withra
Skeleton	skeleton	k1gInSc4	skeleton
<g/>
,	,	kIx,	,
Mule	mula	k1gFnSc3	mula
<g/>
,	,	kIx,	,
Skeleton	skeleton	k1gInSc4	skeleton
Horse	Horse	k1gFnSc2	Horse
<g/>
,	,	kIx,	,
Zombie	Zombie	k1gFnSc2	Zombie
Horse	Horse	k1gFnSc2	Horse
<g/>
,	,	kIx,	,
Donkey	Donkea	k1gFnSc2	Donkea
<g/>
,	,	kIx,	,
Elder	Elder	k1gMnSc1	Elder
Guardian	Guardian	k1gMnSc1	Guardian
<g/>
,	,	kIx,	,
Stray	Stra	k2eAgFnPc4d1	Stra
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Husk	Husk	k1gInSc1	Husk
<g/>
,	,	kIx,	,
Zombie	Zombie	k1gFnSc1	Zombie
Villager	Villager	k1gInSc1	Villager
Pár	pár	k4xCyI	pár
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
:	:	kIx,	:
doWeatherCycle	doWeatherCycle	k1gInSc1	doWeatherCycle
-	-	kIx~	-
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
/	/	kIx~	/
<g/>
povoluje	povolovat	k5eAaImIp3nS	povolovat
automatickou	automatický	k2eAgFnSc4d1	automatická
změnu	změna	k1gFnSc4	změna
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
maxEntityCramming	maxEntityCramming	k1gInSc1	maxEntityCramming
-	-	kIx~	-
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
/	/	kIx~	/
<g/>
povoluje	povolovat	k5eAaImIp3nS	povolovat
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
entity	entita	k1gFnPc4	entita
Particly	Particla	k1gFnSc2	Particla
<g/>
:	:	kIx,	:
Splt	Splt	k1gInSc1	Splt
(	(	kIx(	(
<g/>
Plivnutí	plivnutí	k1gNnSc1	plivnutí
lamy	lama	k1gFnSc2	lama
<g/>
)	)	kIx)	)
a	a	k8xC	a
Totem	totem	k1gInSc1	totem
Nový	nový	k2eAgInSc1d1	nový
<g />
.	.	kIx.	.
</s>
<s>
item	item	k1gMnSc1	item
Shulker	Shulker	k1gMnSc1	Shulker
Shell	Shell	k1gMnSc1	Shell
který	který	k3yQgMnSc1	který
dropuje	dropovat	k5eAaPmIp3nS	dropovat
Shulker	Shulker	k1gInSc4	Shulker
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
Shulker	Shulker	k1gInSc1	Shulker
Box	box	k1gInSc1	box
Nový	nový	k2eAgInSc1d1	nový
item	item	k1gInSc1	item
Totem	totem	k1gInSc1	totem
of	of	k?	of
Undying	Undying	k1gInSc1	Undying
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
při	při	k7c6	při
smrti	smrt	k1gFnSc6	smrt
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
oživí	oživit	k5eAaPmIp3nP	oživit
ho	on	k3xPp3gInSc4	on
Nový	nový	k2eAgInSc4d1	nový
příkaz	příkaz	k1gInSc4	příkaz
/	/	kIx~	/
<g/>
locate	locat	k1gMnSc5	locat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
objeví	objevit	k5eAaPmIp3nS	objevit
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
monument	monument	k1gInSc4	monument
<g/>
,	,	kIx,	,
vesnici	vesnice	k1gFnSc4	vesnice
Štít	štít	k1gInSc4	štít
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
účinější	účiný	k2eAgInSc1d2	účinější
Dne	den	k1gInSc2	den
9.6	[number]	k4	9.6
<g/>
.2016	.2016	k4	.2016
vydal	vydat	k5eAaPmAgMnS	vydat
Mojang	Mojang	k1gMnSc1	Mojang
verzi	verze	k1gFnSc4	verze
Minecraftu	Minecrafta	k1gFnSc4	Minecrafta
1.10	[number]	k4	1.10
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Frostburn	Frostburna	k1gFnPc2	Frostburna
update	update	k1gInSc1	update
<g/>
,	,	kIx,	,
hlavními	hlavní	k2eAgFnPc7d1	hlavní
novinkami	novinka	k1gFnPc7	novinka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Přidány	přidán	k2eAgFnPc1d1	přidána
Nether	Nethra	k1gFnPc2	Nethra
wart	wart	k5eAaPmF	wart
bloky	blok	k1gInPc4	blok
<g/>
,	,	kIx,	,
červené	červená	k1gFnPc4	červená
nether	nethra	k1gFnPc2	nethra
bricky	bricka	k1gFnPc4	bricka
a	a	k8xC	a
Magma	magma	k1gNnSc4	magma
blocky	blocka	k1gFnSc2	blocka
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
používání	používání	k1gNnSc2	používání
ender	endra	k1gFnPc2	endra
perel	perla	k1gFnPc2	perla
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
z	z	k7c2	z
chorusu	chorus	k1gInSc2	chorus
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
sedíte	sedit	k5eAaImIp2nP	sedit
v	v	k7c6	v
loďce	loďka	k1gFnSc6	loďka
<g/>
,	,	kIx,	,
či	či	k8xC	či
vozíku	vozík	k1gInSc2	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Přidány	přidán	k2eAgInPc4d1	přidán
nové	nový	k2eAgInPc4d1	nový
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Přidány	přidán	k2eAgFnPc1d1	přidána
nové	nový	k2eAgFnPc1d1	nová
příšery	příšera	k1gFnPc1	příšera
<g/>
:	:	kIx,	:
Husk	Husk	k1gInSc1	Husk
(	(	kIx(	(
<g/>
zombík	zombík	k1gInSc1	zombík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
spawnuje	spawnovat	k5eAaImIp3nS	spawnovat
na	na	k7c6	na
poušti	poušť	k1gFnSc6	poušť
a	a	k8xC	a
nehoří	hořet	k5eNaImIp3nS	hořet
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stray	Straa	k1gFnSc2	Straa
(	(	kIx(	(
<g/>
kostlivec	kostlivec	k1gMnSc1	kostlivec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
spawnuje	spawnovat	k5eAaPmIp3nS	spawnovat
v	v	k7c6	v
ledovém	ledový	k2eAgInSc6d1	ledový
biomu	biom	k1gInSc6	biom
a	a	k8xC	a
střílí	střílet	k5eAaImIp3nS	střílet
šípy	šíp	k1gInPc4	šíp
s	s	k7c7	s
efektem	efekt	k1gInSc7	efekt
zpomalení	zpomalení	k1gNnSc1	zpomalení
<g/>
)	)	kIx)	)
Přidán	přidán	k2eAgMnSc1d1	přidán
lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
vydrážděn	vydrážděn	k2eAgMnSc1d1	vydrážděn
<g/>
,	,	kIx,	,
na	na	k7c4	na
hráče	hráč	k1gMnPc4	hráč
neútočí	útočit	k5eNaImIp3nS	útočit
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
zapnutí	zapnutí	k1gNnSc2	zapnutí
auto-skoku	autokok	k1gInSc2	auto-skok
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
jednoho	jeden	k4xCgInSc2	jeden
bloku	blok	k1gInSc2	blok
můžete	moct	k5eAaImIp2nP	moct
automaticky	automaticky	k6eAd1	automaticky
skákat	skákat	k5eAaImF	skákat
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
zobrazení	zobrazení	k1gNnSc2	zobrazení
hranic	hranice	k1gFnPc2	hranice
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
chunků	chunek	k1gInPc2	chunek
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
objevit	objevit	k5eAaPmF	objevit
vesnici	vesnice	k1gFnSc4	vesnice
i	i	k9	i
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
biomech	biomo	k1gNnPc6	biomo
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Savana	savana	k1gFnSc1	savana
<g/>
,	,	kIx,	,
Tajga	tajga	k1gFnSc1	tajga
<g/>
,	,	kIx,	,
Mega	mega	k1gNnSc1	mega
Tajga	tajga	k1gFnSc1	tajga
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
objevíte	objevit	k5eAaPmIp2nP	objevit
vesnici	vesnice	k1gFnSc4	vesnice
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
biomech	biom	k1gInPc6	biom
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
vlastní	vlastní	k2eAgFnSc4d1	vlastní
barvu	barva	k1gFnSc4	barva
dřeva	dřevo	k1gNnSc2	dřevo
např.	např.	kA	např.
<g/>
:	:	kIx,	:
v	v	k7c6	v
Savaně	savana	k1gFnSc6	savana
má	mít	k5eAaImIp3nS	mít
oranžové	oranžový	k2eAgNnSc1d1	oranžové
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Tajze	tajga	k1gFnSc6	tajga
má	mít	k5eAaImIp3nS	mít
hnědé	hnědý	k2eAgNnSc4d1	hnědé
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29.2	[number]	k4	29.2
<g/>
.2016	.2016	k4	.2016
vypustil	vypustit	k5eAaPmAgInS	vypustit
Mojang	Mojang	k1gInSc1	Mojang
do	do	k7c2	do
světa	svět	k1gInSc2	svět
Minecraft	Minecrafta	k1gFnPc2	Minecrafta
verzi	verze	k1gFnSc4	verze
1.9	[number]	k4	1.9
neboli	neboli	k8xC	neboli
Boutiful	Boutiful	k1gInSc1	Boutiful
update	update	k1gInSc1	update
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc1d1	hlavní
novinky	novinka	k1gFnPc1	novinka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Přidány	přidán	k2eAgInPc1d1	přidán
štíty	štít	k1gInPc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Přidány	přidán	k2eAgFnPc4d1	přidána
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Přidány	přidán	k2eAgInPc1d1	přidán
nové	nový	k2eAgInPc1d1	nový
blocky	block	k1gInPc1	block
<g/>
.	.	kIx.	.
</s>
<s>
Přidána	přidán	k2eAgFnSc1d1	přidána
nová	nový	k2eAgFnSc1d1	nová
příšera	příšera	k1gFnSc1	příšera
Shulker	Shulkra	k1gFnPc2	Shulkra
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
zvuky	zvuk	k1gInPc1	zvuk
a	a	k8xC	a
animace	animace	k1gFnSc1	animace
v	v	k7c6	v
systému	systém	k1gInSc6	systém
bojování	bojování	k1gNnSc2	bojování
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
držení	držení	k1gNnSc2	držení
dvou	dva	k4xCgInPc2	dva
předmětů	předmět	k1gInPc2	předmět
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
rukách	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Předělaný	předělaný	k2eAgMnSc1d1	předělaný
The	The	k1gMnSc1	The
End	End	k1gMnSc1	End
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
oživení	oživení	k1gNnSc2	oživení
draka	drak	k1gMnSc2	drak
a	a	k8xC	a
portálem	portál	k1gInSc7	portál
do	do	k7c2	do
"	"	kIx"	"
<g/>
Hlubin	hlubina	k1gFnPc2	hlubina
The	The	k1gMnPc2	The
End	End	k1gFnSc2	End
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
nůžek	nůžky	k1gFnPc2	nůžky
na	na	k7c4	na
dýňovou	dýňový	k2eAgFnSc4d1	dýňová
hlavu	hlava	k1gFnSc4	hlava
sněhuláka	sněhulák	k1gMnSc2	sněhulák
získá	získat	k5eAaPmIp3nS	získat
sněhulák	sněhulák	k1gMnSc1	sněhulák
normální	normální	k2eAgInSc4d1	normální
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
pavučiny	pavučina	k1gFnSc2	pavučina
z	z	k7c2	z
dolů	dol	k1gInPc2	dol
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
na	na	k7c6	na
nůžkách	nůžky	k1gFnPc6	nůžky
něžný	něžný	k2eAgInSc4d1	něžný
dotek	dotek	k1gInSc4	dotek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zničení	zničení	k1gNnSc6	zničení
suchého	suchý	k2eAgNnSc2d1	suché
křoví	křoví	k1gNnSc2	křoví
máme	mít	k5eAaImIp1nP	mít
malou	malý	k2eAgFnSc4d1	malá
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
vám	vy	k3xPp2nPc3	vy
padne	padnout	k5eAaPmIp3nS	padnout
klacek	klacek	k1gInSc4	klacek
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
používání	používání	k1gNnSc2	používání
ender	endra	k1gFnPc2	endra
perel	perla	k1gFnPc2	perla
v	v	k7c6	v
creative	creativ	k1gInSc5	creativ
módu	móda	k1gFnSc4	móda
<g/>
.	.	kIx.	.
</s>
<s>
Iglů	Igl	k1gInPc2	Igl
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
biomu	biomo	k1gNnSc6	biomo
<g/>
.	.	kIx.	.
</s>
<s>
Cooldown	Cooldown	k1gInSc1	Cooldown
při	při	k7c6	při
hitování	hitování	k1gNnSc6	hitování
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2.7	[number]	k4	2.7
<g/>
.2014	.2014	k4	.2014
vypustil	vypustit	k5eAaPmAgInS	vypustit
Mojang	Mojang	k1gInSc1	Mojang
do	do	k7c2	do
světa	svět	k1gInSc2	svět
Minecraft	Minecrafta	k1gFnPc2	Minecrafta
verzi	verze	k1gFnSc4	verze
1.8	[number]	k4	1.8
neboli	neboli	k8xC	neboli
combat	combat	k5eAaPmF	combat
update	update	k1gInSc4	update
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc4d1	hlavní
novinky	novinka	k1gFnPc4	novinka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Slime	Slim	k1gInSc5	Slim
Blocky	Block	k1gInPc7	Block
-	-	kIx~	-
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
9	[number]	k4	9
slimeballů	slimeball	k1gInPc2	slimeball
<g/>
,	,	kIx,	,
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
trampolíny	trampolína	k1gFnPc4	trampolína
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
když	když	k8xS	když
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
hráč	hráč	k1gMnSc1	hráč
<g/>
/	/	kIx~	/
<g/>
entita	entita	k1gFnSc1	entita
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
budou	být	k5eAaImBp3nP	být
na	na	k7c6	na
bloku	blok	k1gInSc6	blok
skákat	skákat	k5eAaImF	skákat
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
"	"	kIx"	"
<g/>
přilepit	přilepit	k5eAaPmF	přilepit
<g/>
"	"	kIx"	"
a	a	k8xC	a
posunovat	posunovat	k5eAaImF	posunovat
bloky	blok	k1gInPc4	blok
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
pístů	píst	k1gInPc2	píst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Granite	granit	k1gInSc5	granit
(	(	kIx(	(
<g/>
Žula	žula	k1gFnSc1	žula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andesite	andesit	k1gInSc5	andesit
<g/>
,	,	kIx,	,
Diorite	diorit	k1gInSc5	diorit
-	-	kIx~	-
nové	nový	k2eAgInPc4d1	nový
typy	typ	k1gInPc4	typ
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
generují	generovat	k5eAaImIp3nP	generovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
kusech	kus	k1gInPc6	kus
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
šťerk	šťerk	k1gInSc4	šťerk
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
craftovány	craftovat	k5eAaPmNgFnP	craftovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Polished	Polished	k1gInSc1	Polished
(	(	kIx(	(
<g/>
vyhlazeného	vyhlazený	k2eAgNnSc2d1	vyhlazené
<g/>
)	)	kIx)	)
Granite	granit	k1gInSc5	granit
<g/>
,	,	kIx,	,
Andesite	andesit	k1gInSc5	andesit
<g/>
,	,	kIx,	,
Diorite	diorit	k1gInSc5	diorit
Nové	Nové	k2eAgInPc1d1	Nové
typy	typ	k1gInPc1	typ
sandstonu	sandston	k1gInSc2	sandston
-	-	kIx~	-
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
sandstone	sandston	k1gInSc5	sandston
<g/>
,	,	kIx,	,
generující	generující	k2eAgFnSc7d1	generující
se	se	k3xPyFc4	se
v	v	k7c6	v
mesa	mesa	k6eAd1	mesa
biomech	biom	k1gInPc6	biom
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
z	z	k7c2	z
červeného	červený	k2eAgInSc2d1	červený
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
-	-	kIx~	-
stakují	stakovat	k5eAaImIp3nP	stakovat
se	se	k3xPyFc4	se
po	po	k7c4	po
64	[number]	k4	64
<g/>
,	,	kIx,	,
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
se	se	k3xPyFc4	se
3	[number]	k4	3
místo	místo	k7c2	místo
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Přibyly	přibýt	k5eAaPmAgFnP	přibýt
nové	nový	k2eAgFnPc4d1	nová
dveře	dveře	k1gFnPc4	dveře
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
druhu	druh	k1gInSc2	druh
dřeva	dřevo	k1gNnSc2	dřevo
ze	z	k7c2	z
kterého	který	k3yQgMnSc2	který
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
World	World	k1gMnSc1	World
Border	Border	k1gMnSc1	Border
-	-	kIx~	-
animovaná	animovaný	k2eAgFnSc1d1	animovaná
hranice	hranice	k1gFnSc1	hranice
mapy	mapa	k1gFnSc2	mapa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
absolutně	absolutně	k6eAd1	absolutně
nic	nic	k3yNnSc4	nic
projít	projít	k5eAaPmF	projít
skrz	skrz	k6eAd1	skrz
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
jste	být	k5eAaImIp2nP	být
ve	v	k7c4	v
"	"	kIx"	"
<g/>
spectator	spectator	k1gInSc4	spectator
<g/>
"	"	kIx"	"
módu	móda	k1gFnSc4	móda
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
upravit	upravit	k5eAaPmF	upravit
s	s	k7c7	s
těmito	tento	k3xDgNnPc7	tento
následujícími	následující	k2eAgNnPc7d1	následující
commandy	commando	k1gNnPc7	commando
<g/>
:	:	kIx,	:
/	/	kIx~	/
<g/>
worldborder	worldborder	k1gInSc1	worldborder
center	centrum	k1gNnPc2	centrum
X	X	kA	X
Y	Y	kA	Y
Z	Z	kA	Z
-	-	kIx~	-
nastaví	nastavit	k5eAaPmIp3nS	nastavit
centrum	centrum	k1gNnSc1	centrum
hranice	hranice	k1gFnSc2	hranice
/	/	kIx~	/
<g/>
worldborder	worldbordrat	k5eAaPmRp2nS	worldbordrat
set	set	k1gInSc1	set
(	(	kIx(	(
<g/>
velikost	velikost	k1gFnSc1	velikost
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
čas	čas	k1gInSc4	čas
<g/>
]	]	kIx)	]
-	-	kIx~	-
nastaví	nastavit	k5eAaPmIp3nS	nastavit
velikost	velikost	k1gFnSc1	velikost
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
načasována	načasován	k2eAgFnSc1d1	načasována
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
tak	tak	k6eAd1	tak
<g />
.	.	kIx.	.
</s>
<s>
zezelená	zezelenat	k5eAaPmIp3nS	zezelenat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
smršťuje	smršťovat	k5eAaImIp3nS	smršťovat
tak	tak	k8xC	tak
zčervená	zčervenat	k5eAaPmIp3nS	zčervenat
/	/	kIx~	/
<g/>
worldborder	worldbordrat	k5eAaPmRp2nS	worldbordrat
damage	damage	k1gInSc1	damage
(	(	kIx(	(
<g/>
buffer	buffer	k1gInSc1	buffer
<g/>
|	|	kIx~	|
<g/>
ammount	ammount	k1gInSc1	ammount
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
)	)	kIx)	)
-	-	kIx~	-
možnost	možnost	k1gFnSc1	možnost
buffer	buffer	k1gInSc1	buffer
nastaví	nastavit	k5eAaPmIp3nS	nastavit
<g/>
,	,	kIx,	,
za	za	k7c4	za
jaký	jaký	k3yQgInSc4	jaký
počet	počet	k1gInSc4	počet
bloků	blok	k1gInPc2	blok
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
dostane	dostat	k5eAaPmIp3nS	dostat
hráč	hráč	k1gMnSc1	hráč
<g/>
/	/	kIx~	/
<g/>
mob	mob	k?	mob
poškození	poškození	k1gNnSc1	poškození
<g/>
;	;	kIx,	;
možnost	možnost	k1gFnSc1	možnost
damage	damage	k6eAd1	damage
nastaví	nastavit	k5eAaPmIp3nS	nastavit
míru	míra	k1gFnSc4	míra
poškození	poškození	k1gNnSc2	poškození
Spectator	Spectator	k1gInSc1	Spectator
mode	modus	k1gInSc5	modus
-	-	kIx~	-
hráč	hráč	k1gMnSc1	hráč
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
interakci	interakce	k1gFnSc4	interakce
se	s	k7c7	s
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
s	s	k7c7	s
páčkami	páčka	k1gFnPc7	páčka
<g/>
,	,	kIx,	,
tlačítkami	tlačítka	k1gFnPc7	tlačítka
<g/>
,	,	kIx,	,
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
přes	přes	k7c4	přes
oči	oko	k1gNnPc4	oko
jiného	jiný	k2eAgNnSc2d1	jiné
moba	mobum	k1gNnSc2	mobum
či	či	k8xC	či
hráče	hráč	k1gMnSc4	hráč
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
klikne	kliknout	k5eAaPmIp3nS	kliknout
levým	levý	k2eAgNnSc7d1	levé
tlačítkem	tlačítko	k1gNnSc7	tlačítko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Spectator	Spectator	k1gInSc4	Spectator
modu	modus	k1gInSc2	modus
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
otevřít	otevřít	k5eAaPmF	otevřít
truhla	truhla	k1gFnSc1	truhla
<g/>
,	,	kIx,	,
dispenser	dispenser	k1gMnSc1	dispenser
<g/>
,	,	kIx,	,
dropper	dropper	k1gMnSc1	dropper
atd.	atd.	kA	atd.
pravým	pravý	k2eAgNnSc7d1	pravé
tlačítkem	tlačítko	k1gNnSc7	tlačítko
<g/>
.	.	kIx.	.
</s>
<s>
Endermite	Endermit	k1gInSc5	Endermit
-	-	kIx~	-
nový	nový	k2eAgMnSc1d1	nový
agresivní	agresivní	k2eAgMnSc1d1	agresivní
mob	mob	k?	mob
<g/>
,	,	kIx,	,
podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
Silverfishovi	Silverfishův	k2eAgMnPc1d1	Silverfishův
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
fialové	fialový	k2eAgNnSc4d1	fialové
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Spawnuje	Spawnovat	k5eAaPmIp3nS	Spawnovat
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
hráč	hráč	k1gMnSc1	hráč
teleportuje	teleportovat	k5eAaPmIp3nS	teleportovat
ender	endero	k1gNnPc2	endero
perlou	perla	k1gFnSc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Guardiani	Guardian	k1gMnPc1	Guardian
-	-	kIx~	-
noví	nový	k2eAgMnPc1d1	nový
vodní	vodní	k2eAgMnPc1d1	vodní
mobové	mobus	k1gMnPc1	mobus
<g/>
.	.	kIx.	.
</s>
<s>
Vypadají	vypadat	k5eAaImIp3nP	vypadat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
zelenomodrou	zelenomodrý	k2eAgFnSc4d1	zelenomodrá
barvu	barva	k1gFnSc4	barva
s	s	k7c7	s
oranžovými	oranžový	k2eAgInPc7d1	oranžový
ostny	osten	k1gInPc7	osten
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
oko	oko	k1gNnSc1	oko
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
s	s	k7c7	s
ploutví	ploutev	k1gFnSc7	ploutev
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vůči	vůči	k7c3	vůči
hráči	hráč	k1gMnSc3	hráč
a	a	k8xC	a
squidům	squid	k1gMnPc3	squid
agresivní	agresivní	k2eAgInSc1d1	agresivní
<g/>
.	.	kIx.	.
</s>
<s>
Hlídají	hlídat	k5eAaImIp3nP	hlídat
totiž	totiž	k9	totiž
novou	nový	k2eAgFnSc4d1	nová
strukturu	struktura	k1gFnSc4	struktura
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
-	-	kIx~	-
podvodní	podvodní	k2eAgInSc1d1	podvodní
monument	monument	k1gInSc1	monument
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
nové	nový	k2eAgInPc4d1	nový
bloky	blok	k1gInPc4	blok
jako	jako	k8xS	jako
Prismarine	Prismarin	k1gInSc5	Prismarin
a	a	k8xC	a
mořské	mořský	k2eAgFnSc2d1	mořská
lucerny	lucerna	k1gFnSc2	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
palác	palác	k1gInSc1	palác
vždy	vždy	k6eAd1	vždy
hlídají	hlídat	k5eAaImIp3nP	hlídat
tři	tři	k4xCgMnPc1	tři
velcí	velký	k2eAgMnPc1d1	velký
guardiani	guardian	k1gMnPc1	guardian
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Elder	Elder	k1gMnSc1	Elder
Guardian	Guardian	k1gMnSc1	Guardian
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
normálních	normální	k2eAgInPc2d1	normální
guardianů	guardian	k1gInPc2	guardian
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
šedý	šedý	k2eAgInSc1d1	šedý
s	s	k7c7	s
fialovými	fialový	k2eAgInPc7d1	fialový
ostny	osten	k1gInPc7	osten
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
vám	vy	k3xPp2nPc3	vy
víc	hodně	k6eAd2	hodně
života	život	k1gInSc2	život
a	a	k8xC	a
přidělá	přidělat	k5eAaPmIp3nS	přidělat
vám	vy	k3xPp2nPc3	vy
negativní	negativní	k2eAgFnSc1d1	negativní
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
guardianové	guardianové	k2eAgMnPc1d1	guardianové
"	"	kIx"	"
<g/>
vysílají	vysílat	k5eAaImIp3nP	vysílat
<g/>
"	"	kIx"	"
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
paprsky	paprsek	k1gInPc7	paprsek
na	na	k7c4	na
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
http://minecraft.gamepedia.com/Guardian	[url]	k1gInSc4	http://minecraft.gamepedia.com/Guardian
Králíci	Králík	k1gMnPc1	Králík
-	-	kIx~	-
Po	po	k7c6	po
zabití	zabití	k1gNnSc6	zabití
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
padá	padat	k5eAaImIp3nS	padat
králičí	králičí	k2eAgNnSc1d1	králičí
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
králičí	králičí	k2eAgFnSc1d1	králičí
noha	noha	k1gFnSc1	noha
<g/>
,	,	kIx,	,
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
ochočeni	ochočit	k5eAaPmNgMnP	ochočit
mrkví	mrkev	k1gFnSc7	mrkev
<g/>
,	,	kIx,	,
zlatou	zlatý	k2eAgFnSc7d1	zlatá
mrkví	mrkev	k1gFnSc7	mrkev
a	a	k8xC	a
pampeliškou	pampeliška	k1gFnSc7	pampeliška
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
2	[number]	k4	2
tajné	tajný	k2eAgFnSc2d1	tajná
varianty	varianta	k1gFnSc2	varianta
králíků	králík	k1gMnPc2	králík
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
agresivní	agresivní	k2eAgMnSc1d1	agresivní
králík	králík	k1gMnSc1	králík
s	s	k7c7	s
rudýma	rudý	k2eAgNnPc7d1	Rudé
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
je	být	k5eAaImIp3nS	být
králík	králík	k1gMnSc1	králík
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
name	name	k6eAd1	name
tagem	tag	k1gInSc7	tag
"	"	kIx"	"
<g/>
Toast	toast	k1gInSc1	toast
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
však	však	k9	však
přirozeně	přirozeně	k6eAd1	přirozeně
nespawnuje	spawnovat	k5eNaBmIp3nS	spawnovat
<g/>
.	.	kIx.	.
</s>
<s>
Požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
hardware	hardware	k1gInSc4	hardware
a	a	k8xC	a
software	software	k1gInSc4	software
nutné	nutný	k2eAgNnSc4d1	nutné
pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
požadavky	požadavek	k1gInPc4	požadavek
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
Mojang	Mojang	k1gInSc1	Mojang
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
tzv.	tzv.	kA	tzv.
Skin	skin	k1gMnSc1	skin
je	být	k5eAaImIp3nS	být
vzhled	vzhled	k1gInSc4	vzhled
vaší	váš	k3xOp2gFnSc2	váš
postavy	postava	k1gFnSc2	postava
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obrázku	obrázek	k1gInSc6	obrázek
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
existují	existovat	k5eAaImIp3nP	existovat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
editory	editor	k1gInPc1	editor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vystačíte	vystačit	k5eAaBmIp2nP	vystačit
si	se	k3xPyFc3	se
i	i	k9	i
s	s	k7c7	s
základními	základní	k2eAgInPc7d1	základní
pixel	pixel	k1gInSc4	pixel
editory	editor	k1gInPc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
webové	webový	k2eAgInPc4d1	webový
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
např	např	kA	např
novaskin	novaskin	k1gInSc1	novaskin
<g/>
.	.	kIx.	.
</s>
<s>
Forge	Forge	k1gInSc1	Forge
je	být	k5eAaImIp3nS	být
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
API	API	kA	API
na	na	k7c4	na
client	client	k1gInSc4	client
site	sitat	k5eAaPmIp3nS	sitat
i	i	k9	i
server	server	k1gInSc1	server
side	sidat	k5eAaPmIp3nS	sidat
<g/>
.	.	kIx.	.
</s>
<s>
Módy	móda	k1gFnPc1	móda
se	se	k3xPyFc4	se
instalují	instalovat	k5eAaBmIp3nP	instalovat
jednoduše	jednoduše	k6eAd1	jednoduše
přesunutím	přesunutí	k1gNnSc7	přesunutí
do	do	k7c2	do
složky	složka	k1gFnSc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
nerozšířená	rozšířený	k2eNgFnSc1d1	nerozšířená
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
client	client	k1gMnSc1	client
side	sid	k1gFnSc2	sid
API	API	kA	API
<g/>
.	.	kIx.	.
</s>
<s>
Client	Client	k1gInSc1	Client
side	sidat	k5eAaPmIp3nS	sidat
i	i	k9	i
server	server	k1gInSc1	server
side	sidat	k5eAaPmIp3nS	sidat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používáním	používání	k1gNnSc7	používání
těchto	tento	k3xDgInPc2	tento
módů	mód	k1gInPc2	mód
zasahujete	zasahovat	k5eAaImIp2nP	zasahovat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
kódu	kód	k1gInSc2	kód
minecraftu	minecraft	k1gInSc2	minecraft
a	a	k8xC	a
můžete	moct	k5eAaImIp2nP	moct
poškodit	poškodit	k5eAaPmF	poškodit
hru	hra	k1gFnSc4	hra
či	či	k8xC	či
vám	vy	k3xPp2nPc3	vy
můžou	můžou	k?	můžou
dokonce	dokonce	k9	dokonce
ukrást	ukrást	k5eAaPmF	ukrást
ůčet	ůčet	k1gInSc4	ůčet
<g/>
.	.	kIx.	.
</s>
<s>
Resource	Resourka	k1gFnSc3	Resourka
packy	packy	k6eAd1	packy
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Texture	Textur	k1gMnSc5	Textur
packy	packa	k1gFnPc4	packa
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
změnit	změnit	k5eAaPmF	změnit
zvuky	zvuk	k1gInPc4	zvuk
a	a	k8xC	a
textury	textura	k1gFnPc4	textura
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Minecraft	Minecraft	k1gInSc1	Minecraft
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
in-game	inam	k1gInSc5	in-gam
nastavení	nastavení	k1gNnPc1	nastavení
a	a	k8xC	a
texture	textur	k1gMnSc5	textur
packy	packa	k1gFnPc4	packa
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
podporovány	podporovat	k5eAaImNgInP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Shadery	Shader	k1gInPc1	Shader
jsou	být	k5eAaImIp3nP	být
něco	něco	k3yInSc4	něco
jako	jako	k8xS	jako
resourcepacky	resourcepacky	k6eAd1	resourcepacky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nS	měnit
minecraft	minecraft	k5eAaPmF	minecraft
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spuštění	spuštění	k1gNnSc6	spuštění
používají	používat	k5eAaImIp3nP	používat
mód	mód	k1gInSc4	mód
optifine	optifinout	k5eAaPmIp3nS	optifinout
<g/>
,	,	kIx,	,
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
taky	taky	k6eAd1	taky
na	na	k7c4	na
vylepšení	vylepšení	k1gNnSc4	vylepšení
některých	některý	k3yIgInPc2	některý
resourcepacků	resourcepack	k1gInPc2	resourcepack
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Minecraft	Minecraft	k2eAgInSc1d1	Minecraft
velmi	velmi	k6eAd1	velmi
komplexní	komplexní	k2eAgFnSc7d1	komplexní
hrou	hra	k1gFnSc7	hra
(	(	kIx(	(
<g/>
sandbox	sandbox	k1gInSc1	sandbox
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
na	na	k7c4	na
návody	návod	k1gInPc4	návod
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
herních	herní	k2eAgInPc2d1	herní
prvků	prvek	k1gInPc2	prvek
Minecraftu	Minecraft	k1gInSc2	Minecraft
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
zřejmých	zřejmý	k2eAgMnPc2d1	zřejmý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
se	se	k3xPyFc4	se
bez	bez	k1gInSc1	bez
návodů	návod	k1gInPc2	návod
běžný	běžný	k2eAgMnSc1d1	běžný
hráč	hráč	k1gMnSc1	hráč
zcela	zcela	k6eAd1	zcela
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
crafting	crafting	k1gInSc4	crafting
<g/>
,	,	kIx,	,
farmaření	farmaření	k1gNnSc4	farmaření
<g/>
,	,	kIx,	,
redstonové	redstonový	k2eAgInPc4d1	redstonový
obvody	obvod	k1gInPc4	obvod
a	a	k8xC	a
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
návodům	návod	k1gInPc3	návod
patří	patřit	k5eAaImIp3nS	patřit
příkazy	příkaz	k1gInPc1	příkaz
a	a	k8xC	a
příkazové	příkazový	k2eAgInPc1d1	příkazový
bloky	blok	k1gInPc1	blok
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
dobrodružných	dobrodružný	k2eAgFnPc6d1	dobrodružná
mapách	mapa	k1gFnPc6	mapa
nebo	nebo	k8xC	nebo
na	na	k7c6	na
serverech	server	k1gInPc6	server
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
1.7	[number]	k4	1.7
a	a	k8xC	a
1.8	[number]	k4	1.8
jdou	jít	k5eAaImIp3nP	jít
s	s	k7c7	s
command	command	k1gInSc4	command
blocky	blocky	k6eAd1	blocky
dělat	dělat	k5eAaImF	dělat
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgFnPc2d1	různá
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
použití	použití	k1gNnSc1	použití
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
složité	složitý	k2eAgInPc4d1	složitý
tutoriály	tutoriál	k1gInPc4	tutoriál
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ale	ale	k9	ale
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
nepotřebujete	potřebovat	k5eNaImIp2nP	potřebovat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
jenom	jenom	k9	jenom
o	o	k7c4	o
přídavek	přídavek	k1gInSc4	přídavek
<g/>
.	.	kIx.	.
</s>
<s>
Mojang	Mojang	k1gMnSc1	Mojang
nedávno	nedávno	k6eAd1	nedávno
vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
pozoruhodných	pozoruhodný	k2eAgFnPc2d1	pozoruhodná
a	a	k8xC	a
užitečných	užitečný	k2eAgFnPc2d1	užitečná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Každá	každý	k3xTgFnSc1	každý
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stavení	stavení	k1gNnSc1	stavení
<g/>
,	,	kIx,	,
těžení	těžení	k1gNnSc1	těžení
<g/>
,	,	kIx,	,
craftění	craftění	k1gNnSc1	craftění
<g/>
...	...	k?	...
Úspěch	úspěch	k1gInSc1	úspěch
Minecraftu	Minecraft	k1gInSc2	Minecraft
vyústil	vyústit	k5eAaPmAgInS	vyústit
i	i	k9	i
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
verze	verze	k1gFnSc2	verze
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
a	a	k8xC	a
tablety	tableta	k1gFnPc4	tableta
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
věcech	věc	k1gFnPc6	věc
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
metu	meta	k1gFnSc4	meta
překonal	překonat	k5eAaPmAgMnS	překonat
Minecraft	Minecraft	k1gMnSc1	Minecraft
<g/>
:	:	kIx,	:
Pocket	Pocket	k1gInSc1	Pocket
Edition	Edition	k1gInSc1	Edition
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeno	zjednodušen	k2eAgNnSc1d1	zjednodušeno
je	být	k5eAaImIp3nS	být
generování	generování	k1gNnSc1	generování
map	mapa	k1gFnPc2	mapa
i	i	k8xC	i
crafting	crafting	k1gInSc1	crafting
<g/>
.	.	kIx.	.
</s>
<s>
Dostupný	dostupný	k2eAgMnSc1d1	dostupný
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
na	na	k7c6	na
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c4	na
Android	android	k1gInSc4	android
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnSc1d3	nejnovější
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
1.0.5.0	[number]	k4	1.0.5.0
Přibyly	přibýt	k5eAaPmAgFnP	přibýt
zde	zde	k6eAd1	zde
např.	např.	kA	např.
křídla	křídlo	k1gNnSc2	křídlo
(	(	kIx(	(
<g/>
elytra	elytra	k1gFnSc1	elytra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
End	End	k1gFnSc4	End
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc4d1	nový
bloky	blok	k1gInPc4	blok
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Upravené	upravený	k2eAgNnSc1d1	upravené
menu	menu	k1gNnSc1	menu
-	-	kIx~	-
více	hodně	k6eAd2	hodně
podobné	podobný	k2eAgFnSc6d1	podobná
verzi	verze	k1gFnSc6	verze
na	na	k7c4	na
Windows	Windows	kA	Windows
10	[number]	k4	10
Minecraft	Minecraft	k2eAgInSc1d1	Minecraft
realms	realms	k1gInSc1	realms
servery	server	k1gInPc4	server
Upravené	upravený	k2eAgNnSc4d1	upravené
tlačítko	tlačítko	k1gNnSc4	tlačítko
pro	pro	k7c4	pro
otevření	otevření	k1gNnSc4	otevření
inventáře	inventář	k1gInSc2	inventář
Pistony	piston	k1gInPc4	piston
<g/>
,	,	kIx,	,
sticky	sticky	k6eAd1	sticky
pistony	piston	k1gInPc1	piston
Observer	Observra	k1gFnPc2	Observra
-	-	kIx~	-
"	"	kIx"	"
<g/>
detektor	detektor	k1gInSc1	detektor
pohybu	pohyb	k1gInSc2	pohyb
<g/>
"	"	kIx"	"
Koně	kůň	k1gMnPc1	kůň
(	(	kIx(	(
<g/>
a	a	k8xC	a
brnění	brnění	k1gNnSc4	brnění
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zombie	zombie	k1gFnSc1	zombie
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
skeleton	skeleton	k1gInSc4	skeleton
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
oslové	osel	k1gMnPc1	osel
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
muly	mula	k1gFnSc2	mula
Spawnovací	Spawnovací	k2eAgNnPc1d1	Spawnovací
vajíčka	vajíčko	k1gNnPc1	vajíčko
na	na	k7c4	na
více	hodně	k6eAd2	hodně
mobů	mob	k1gInPc2	mob
Nové	Nové	k2eAgInPc4d1	Nové
typy	typ	k1gInPc4	typ
šípů	šíp	k1gInPc2	šíp
Z	z	k7c2	z
ovcí	ovce	k1gFnPc2	ovce
padá	padat	k5eAaImIp3nS	padat
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
i	i	k9	i
opéct	opéct	k5eAaPmF	opéct
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
praseti	prase	k1gNnSc6	prase
Mrkev	mrkev	k1gFnSc1	mrkev
na	na	k7c6	na
tyčce	tyčka	k1gFnSc6	tyčka
-	-	kIx~	-
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
řídí	řídit	k5eAaImIp3nS	řídit
prase	prase	k1gNnSc1	prase
Name	Nam	k1gFnSc2	Nam
tagy	tag	k1gInPc1	tag
<g/>
,	,	kIx,	,
lasa	laso	k1gNnPc1	laso
<g/>
,	,	kIx,	,
fire	fire	k6eAd1	fire
charge	charge	k6eAd1	charge
Kostlivec	kostlivec	k1gMnSc1	kostlivec
má	mít	k5eAaImIp3nS	mít
jinak	jinak	k6eAd1	jinak
vytvarovanou	vytvarovaný	k2eAgFnSc4d1	vytvarovaná
kostru	kostra	k1gFnSc4	kostra
Dřevo	dřevo	k1gNnSc1	dřevo
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
vesnicích	vesnice	k1gFnPc6	vesnice
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
barvu	barva	k1gFnSc4	barva
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yQgInSc6	jaký
biomu	biom	k1gInSc6	biom
leží	ležet	k5eAaImIp3nS	ležet
Vesnice	vesnice	k1gFnSc1	vesnice
jen	jen	k6eAd1	jen
se	se	k3xPyFc4	se
zombie	zombie	k1gFnSc1	zombie
villagery	villagera	k1gFnSc2	villagera
-	-	kIx~	-
hodně	hodně	k6eAd1	hodně
vzácná	vzácný	k2eAgFnSc1d1	vzácná
na	na	k7c6	na
nalezení	nalezení	k1gNnSc6	nalezení
Chrámy	chrám	k1gInPc4	chrám
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
,	,	kIx,	,
podvodní	podvodní	k2eAgInPc4d1	podvodní
chrámy	chrám	k1gInPc4	chrám
Jiná	jiný	k2eAgFnSc1d1	jiná
textura	textura	k1gFnSc1	textura
některých	některý	k3yIgFnPc2	některý
květin	květina	k1gFnPc2	květina
Trapdoory	Trapdoora	k1gFnSc2	Trapdoora
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
opěrný	opěrný	k2eAgInSc4d1	opěrný
blok	blok	k1gInSc4	blok
Hoppery	Hoppera	k1gFnSc2	Hoppera
<g/>
,	,	kIx,	,
droppery	droppera	k1gFnSc2	droppera
<g/>
,	,	kIx,	,
dispensery	dispensera	k1gFnSc2	dispensera
Comparatory	Comparator	k1gMnPc4	Comparator
<g/>
,	,	kIx,	,
repeatery	repeater	k1gMnPc4	repeater
rámečky	rámeček	k1gInPc4	rámeček
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc4	mapa
Slime	Slim	k1gInSc5	Slim
bloky	blok	k1gInPc1	blok
Truhly	truhla	k1gFnSc2	truhla
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
otevřít	otevřít	k5eAaPmF	otevřít
i	i	k9	i
v	v	k7c6	v
kreativním	kreativní	k2eAgInSc6d1	kreativní
módu	mód	k1gInSc6	mód
<g/>
.	.	kIx.	.
</s>
<s>
Dýně	dýně	k1gFnPc1	dýně
a	a	k8xC	a
hlavy	hlava	k1gFnPc1	hlava
stvoření	stvoření	k1gNnSc2	stvoření
lze	lze	k6eAd1	lze
nasadit	nasadit	k5eAaPmF	nasadit
<g/>
.	.	kIx.	.
</s>
<s>
Čarodejnice	Čarodejnice	k1gFnPc1	Čarodejnice
Různé	různý	k2eAgFnPc1d1	různá
typy	typ	k1gInPc4	typ
vozíků	vozík	k1gInPc2	vozík
Červený	červený	k2eAgInSc4d1	červený
pískovec	pískovec	k1gInSc4	pískovec
a	a	k8xC	a
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
Do	do	k7c2	do
Minecraftu	Minecraft	k1gInSc2	Minecraft
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
přidat	přidat	k5eAaPmF	přidat
různé	různý	k2eAgFnPc4d1	různá
modifikace	modifikace	k1gFnPc4	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
ty	ten	k3xDgInPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
už	už	k6eAd1	už
klasický	klasický	k2eAgInSc1d1	klasický
Minecraft	Minecraft	k1gInSc1	Minecraft
přestal	přestat	k5eAaPmAgInS	přestat
bavit	bavit	k5eAaImF	bavit
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
přidat	přidat	k5eAaPmF	přidat
od	od	k7c2	od
různých	různý	k2eAgFnPc2d1	různá
maličkostí	maličkost	k1gFnPc2	maličkost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
obarvené	obarvený	k2eAgInPc4d1	obarvený
postele	postel	k1gInPc4	postel
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
možnost	možnost	k1gFnSc4	možnost
komplexního	komplexní	k2eAgInSc2d1	komplexní
těžícího	těžící	k2eAgInSc2d1	těžící
<g/>
,	,	kIx,	,
farmářského	farmářský	k2eAgInSc2d1	farmářský
a	a	k8xC	a
třídícího	třídící	k2eAgInSc2d1	třídící
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
Twilight	Twilight	k1gMnSc1	Twilight
Forest	Forest	k1gMnSc1	Forest
<g/>
,	,	kIx,	,
Galacticraft	Galacticraft	k1gMnSc1	Galacticraft
<g/>
,	,	kIx,	,
Thaumcraft	Thaumcraft	k1gMnSc1	Thaumcraft
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Tyto	tento	k3xDgFnPc1	tento
modifikace	modifikace	k1gFnPc1	modifikace
ovšem	ovšem	k9	ovšem
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
hardwarové	hardwarový	k2eAgInPc4d1	hardwarový
nároky	nárok	k1gInPc4	nárok
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
podporovány	podporovat	k5eAaImNgInP	podporovat
Minecraftem	Minecrafto	k1gNnSc7	Minecrafto
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
nainstalovaný	nainstalovaný	k2eAgInSc1d1	nainstalovaný
pomocný	pomocný	k2eAgInSc1d1	pomocný
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
Forge	Forge	k1gInSc1	Forge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
modifikace	modifikace	k1gFnPc1	modifikace
se	se	k3xPyFc4	se
často	často	k6eAd1	často
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
modpacků	modpacek	k1gMnPc2	modpacek
(	(	kIx(	(
<g/>
Čarovný	čarovný	k2eAgMnSc1d1	čarovný
Minecraft	Minecraft	k1gMnSc1	Minecraft
<g/>
,	,	kIx,	,
FutureCraft	FutureCraft	k1gMnSc1	FutureCraft
<g/>
,	,	kIx,	,
Hexitt	Hexitt	k1gMnSc1	Hexitt
<g/>
,	,	kIx,	,
Tekkit	Tekkit	k1gMnSc1	Tekkit
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
do	do	k7c2	do
Minecraftu	Minecraft	k1gInSc2	Minecraft
dají	dát	k5eAaPmIp3nP	dát
přidat	přidat	k5eAaPmF	přidat
resource	resourka	k1gFnSc3	resourka
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
texture	textur	k1gMnSc5	textur
<g/>
)	)	kIx)	)
packy	packa	k1gFnPc1	packa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mění	měnit	k5eAaImIp3nP	měnit
textury	textura	k1gFnPc4	textura
bloků	blok	k1gInPc2	blok
a	a	k8xC	a
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
<g/>
,	,	kIx,	,
písma	písmo	k1gNnPc1	písmo
a	a	k8xC	a
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Minecraft	Minecrafta	k1gFnPc2	Minecrafta
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
hra	hra	k1gFnSc1	hra
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
světoznámé	světoznámý	k2eAgFnSc2d1	světoznámá
stavebnice	stavebnice	k1gFnSc2	stavebnice
Lego	lego	k1gNnSc1	lego
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
Princ	princ	k1gMnSc1	princ
of	of	k?	of
Persia	Persia	k1gFnSc1	Persia
písky	písek	k1gInPc4	písek
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
zakoupit	zakoupit	k5eAaPmF	zakoupit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
7	[number]	k4	7
takovýchto	takovýto	k3xDgInPc2	takovýto
setů	set	k1gInPc2	set
stavebnice	stavebnice	k1gFnSc2	stavebnice
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
