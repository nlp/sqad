<p>
<s>
Botulotoxin	botulotoxin	k1gInSc1	botulotoxin
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
botulus	botulus	k1gInSc1	botulus
=	=	kIx~	=
klobása	klobása	k1gFnSc1	klobása
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
botulin	botulin	k1gInSc1	botulin
nebo	nebo	k8xC	nebo
klobásový	klobásový	k2eAgInSc1d1	klobásový
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
toxická	toxický	k2eAgFnSc1d1	toxická
polypeptidická	polypeptidický	k2eAgFnSc1d1	polypeptidický
dvojsložková	dvojsložkový	k2eAgFnSc1d1	dvojsložková
směs	směs	k1gFnSc1	směs
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
bakteriemi	bakterie	k1gFnPc7	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
botulinum	botulinum	k1gInSc4	botulinum
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
probíhá	probíhat	k5eAaImIp3nS	probíhat
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
anaerobních	anaerobní	k2eAgFnPc2d1	anaerobní
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
optimální	optimální	k2eAgMnSc1d1	optimální
pH	ph	kA	ph
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
4,8	[number]	k4	4,8
<g/>
–	–	k?	–
<g/>
8,5	[number]	k4	8,5
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
kolem	kolem	k7c2	kolem
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejúčinnějších	účinný	k2eAgInPc2d3	nejúčinnější
jedů	jed	k1gInPc2	jed
(	(	kIx(	(
<g/>
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
je	být	k5eAaImIp3nS	být
nejjedovatější	jedovatý	k2eAgFnSc1d3	nejjedovatější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možná	možná	k9	možná
absolutně	absolutně	k6eAd1	absolutně
nejúčinnější	účinný	k2eAgInPc1d3	nejúčinnější
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgMnSc7	druhý
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c6	na
absolutně	absolutně	k6eAd1	absolutně
nejsilnější	silný	k2eAgInSc1d3	nejsilnější
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
batrachotoxin	batrachotoxin	k2eAgInSc1d1	batrachotoxin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
1,3	[number]	k4	1,3
<g/>
–	–	k?	–
<g/>
2,1	[number]	k4	2,1
ng	ng	k?	ng
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
nitrožilně	nitrožilně	k6eAd1	nitrožilně
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
ng	ng	k?	ng
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
při	při	k7c6	při
inhalaci	inhalace	k1gFnSc6	inhalace
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
botulinem	botulin	k1gInSc7	botulin
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
onemocnění	onemocnění	k1gNnSc1	onemocnění
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
botulismus	botulismus	k1gInSc4	botulismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
vůči	vůči	k7c3	vůči
HCl	HCl	k1gFnSc3	HCl
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
žaludeční	žaludeční	k2eAgFnSc6d1	žaludeční
šťávě	šťáva	k1gFnSc6	šťáva
a	a	k8xC	a
trávení	trávení	k1gNnSc6	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Neporušený	porušený	k2eNgMnSc1d1	neporušený
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
střevní	střevní	k2eAgFnSc7d1	střevní
sliznicí	sliznice	k1gFnSc7	sliznice
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
transportován	transportovat	k5eAaBmNgInS	transportovat
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
krví	krev	k1gFnPc2	krev
a	a	k8xC	a
lymfou	lymfa	k1gFnSc7	lymfa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Botulin	botulin	k1gInSc1	botulin
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
termolabilní	termolabilní	k2eAgNnSc1d1	termolabilní
a	a	k8xC	a
ničí	ničit	k5eAaImIp3nS	ničit
ho	on	k3xPp3gMnSc4	on
teplota	teplota	k1gFnSc1	teplota
nad	nad	k7c7	nad
60	[number]	k4	60
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Botulotoxin	botulotoxin	k1gInSc1	botulotoxin
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
polypeptidy	polypeptid	k1gInPc7	polypeptid
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
19	[number]	k4	19
různých	různý	k2eAgFnPc2d1	různá
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgInPc7	dva
bílkovinnými	bílkovinný	k2eAgInPc7d1	bílkovinný
řetězci	řetězec	k1gInPc7	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Těžký	těžký	k2eAgInSc1d1	těžký
řetězec	řetězec	k1gInSc1	řetězec
H	H	kA	H
a	a	k8xC	a
lehký	lehký	k2eAgInSc4d1	lehký
řetězec	řetězec	k1gInSc4	řetězec
L	L	kA	L
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
disulfidickými	disulfidický	k2eAgInPc7d1	disulfidický
můstky	můstek	k1gInPc7	můstek
<g/>
.	.	kIx.	.
</s>
<s>
H-řetězec	H-řetězec	k1gInSc1	H-řetězec
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
nervové	nervový	k2eAgFnPc4d1	nervová
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
L-řetězec	L-řetězec	k1gInSc4	L-řetězec
zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
průnik	průnik	k1gInSc1	průnik
do	do	k7c2	do
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
L-řetězec	L-řetězec	k1gInSc1	L-řetězec
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
zinek	zinek	k1gInSc4	zinek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
proteiny	protein	k1gInPc4	protein
zodpovědné	zodpovědný	k2eAgInPc4d1	zodpovědný
za	za	k7c4	za
transport	transport	k1gInSc4	transport
vezikul	vezikul	k1gInSc4	vezikul
s	s	k7c7	s
acetylcholinem	acetylcholin	k1gInSc7	acetylcholin
<g/>
.	.	kIx.	.
</s>
<s>
Zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
tak	tak	k9	tak
vyplavování	vyplavování	k1gNnSc1	vyplavování
tohoto	tento	k3xDgInSc2	tento
neuromediátoru	neuromediátor	k1gInSc2	neuromediátor
a	a	k8xC	a
ochrnutí	ochrnutí	k1gNnSc2	ochrnutí
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dostává	dostávat	k5eAaImIp3nS	dostávat
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
kontaminované	kontaminovaný	k2eAgFnSc2d1	kontaminovaná
sporami	spora	k1gFnPc7	spora
bakterií	bakterie	k1gFnPc2	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
botulinum	botulinum	k1gInSc4	botulinum
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
anaerobní	anaerobní	k2eAgNnSc4d1	anaerobní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
již	již	k6eAd1	již
samotné	samotný	k2eAgFnPc1d1	samotná
živé	živý	k2eAgFnPc1d1	živá
bakterie	bakterie	k1gFnPc1	bakterie
nejsou	být	k5eNaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
špatně	špatně	k6eAd1	špatně
tepelně	tepelně	k6eAd1	tepelně
opracované	opracovaný	k2eAgFnPc4d1	opracovaná
konzervy	konzerva	k1gFnPc4	konzerva
a	a	k8xC	a
uzeniny	uzenina	k1gFnPc4	uzenina
<g/>
,	,	kIx,	,
nakládanou	nakládaný	k2eAgFnSc4d1	nakládaná
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc4	houba
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
apod	apod	kA	apod
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
neprošly	projít	k5eNaPmAgFnP	projít
teplotou	teplota	k1gFnSc7	teplota
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
121	[number]	k4	121
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnSc1	bakterie
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
medu	med	k1gInSc6	med
(	(	kIx(	(
<g/>
u	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
dává	dávat	k5eAaImIp3nS	dávat
vzniku	vznik	k1gInSc2	vznik
ranému	raný	k2eAgInSc3d1	raný
botulismu	botulismus	k1gInSc3	botulismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výskytu	výskyt	k1gInSc2	výskyt
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
7	[number]	k4	7
antigenních	antigenní	k2eAgInPc6d1	antigenní
typech	typ	k1gInPc6	typ
(	(	kIx(	(
<g/>
A-G	A-G	k1gMnSc1	A-G
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
se	se	k3xPyFc4	se
toxin	toxin	k1gInSc1	toxin
ničí	ničit	k5eAaImIp3nS	ničit
varem	var	k1gInSc7	var
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
10	[number]	k4	10
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Tvorba	tvorba	k1gFnSc1	tvorba
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
Clostridií	Clostridie	k1gFnPc2	Clostridie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
neurotoxin	neurotoxin	k1gInSc1	neurotoxin
E	E	kA	E
tvoří	tvořit	k5eAaImIp3nS	tvořit
Clostridium	Clostridium	k1gNnSc4	Clostridium
butyricum	butyricum	k1gInSc1	butyricum
a	a	k8xC	a
neurotoxin	neurotoxin	k1gInSc1	neurotoxin
typu	typ	k1gInSc2	typ
F	F	kA	F
tvoří	tvořit	k5eAaImIp3nP	tvořit
Clostridium	Clostridium	k1gNnSc4	Clostridium
baratii	baratie	k1gFnSc3	baratie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Epidemiologie	epidemiologie	k1gFnSc2	epidemiologie
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
bakterie	bakterie	k1gFnSc1	bakterie
je	být	k5eAaImIp3nS	být
komenzálem	komenzál	k1gInSc7	komenzál
střevního	střevní	k2eAgNnSc2d1	střevní
ústrojí	ústrojí	k1gNnSc2	ústrojí
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
ptáků	pták	k1gMnPc2	pták
i	i	k8xC	i
ryb	ryba	k1gFnPc2	ryba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
střevní	střevní	k2eAgInSc4d1	střevní
trakt	trakt	k1gInSc4	trakt
těmito	tento	k3xDgFnPc7	tento
bakteriemi	bakterie	k1gFnPc7	bakterie
kolonizován	kolonizovat	k5eAaBmNgInS	kolonizovat
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
u	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
závažné	závažný	k2eAgFnSc3d1	závažná
intoxikaci	intoxikace	k1gFnSc3	intoxikace
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
Clostridium	Clostridium	k1gNnSc1	Clostridium
botulinum	botulinum	k1gInSc4	botulinum
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Geografické	geografický	k2eAgInPc1d1	geografický
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
určovány	určovat	k5eAaImNgInP	určovat
podle	podle	k7c2	podle
produkce	produkce	k1gFnSc2	produkce
určitého	určitý	k2eAgInSc2d1	určitý
typu	typ	k1gInSc2	typ
neurotoxinu	urotoxinout	k5eNaPmIp1nS	urotoxinout
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
toxinu	toxin	k1gInSc2	toxin
typu	typ	k1gInSc2	typ
A	A	kA	A
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
kontaminuje	kontaminovat	k5eAaBmIp3nS	kontaminovat
především	především	k9	především
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
B	B	kA	B
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
a	a	k8xC	a
kontaminuje	kontaminovat	k5eAaBmIp3nS	kontaminovat
převážně	převážně	k6eAd1	převážně
masové	masový	k2eAgInPc4d1	masový
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
E	E	kA	E
je	být	k5eAaImIp3nS	být
častý	častý	k2eAgInSc1d1	častý
v	v	k7c6	v
přímořských	přímořský	k2eAgFnPc6d1	přímořská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
otrav	otrava	k1gFnPc2	otrava
rybím	rybí	k2eAgNnSc7d1	rybí
masem	maso	k1gNnSc7	maso
nebo	nebo	k8xC	nebo
mořskými	mořský	k2eAgInPc7d1	mořský
plody	plod	k1gInPc7	plod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
již	již	k6eAd1	již
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Statisticky	statisticky	k6eAd1	statisticky
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytlo	vyskytnout	k5eAaPmAgNnS	vyskytnout
103	[number]	k4	103
případů	případ	k1gInPc2	případ
onemocnění	onemocnění	k1gNnSc4	onemocnění
botulismem	botulismus	k1gInSc7	botulismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Botulotoxin	botulotoxin	k1gInSc1	botulotoxin
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
estetické	estetický	k2eAgFnSc6d1	estetická
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
mimických	mimický	k2eAgFnPc2d1	mimická
vrásek	vráska	k1gFnPc2	vráska
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
dočasný	dočasný	k2eAgInSc1d1	dočasný
efekt	efekt	k1gInSc1	efekt
3-4	[number]	k4	3-4
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
též	též	k9	též
využití	využití	k1gNnSc1	využití
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
desítky	desítka	k1gFnPc1	desítka
schválených	schválený	k2eAgFnPc2d1	schválená
indikací	indikace	k1gFnPc2	indikace
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
neurologie	neurologie	k1gFnSc2	neurologie
a	a	k8xC	a
dermatovenerologie	dermatovenerologie	k1gFnSc2	dermatovenerologie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
také	také	k9	také
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
pocení	pocení	k1gNnSc2	pocení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
u	u	k7c2	u
onemocnění	onemocnění	k1gNnSc2	onemocnění
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
vysokým	vysoký	k2eAgInSc7d1	vysoký
svalovým	svalový	k2eAgInSc7d1	svalový
tonusem	tonus	k1gInSc7	tonus
nebo	nebo	k8xC	nebo
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
blefarospazmu	blefarospazmus	k1gInSc2	blefarospazmus
(	(	kIx(	(
<g/>
nedobrovolné	dobrovolný	k2eNgInPc4d1	nedobrovolný
stahy	stah	k1gInPc4	stah
svalstva	svalstvo	k1gNnSc2	svalstvo
kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
považován	považován	k2eAgInSc1d1	považován
</s>
</p>
<p>
<s>
za	za	k7c4	za
ideální	ideální	k2eAgFnSc4d1	ideální
náplň	náplň	k1gFnSc4	náplň
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bakterie	bakterie	k1gFnSc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tento	tento	k3xDgInSc4	tento
jed	jed	k1gInSc4	jed
produkují	produkovat	k5eAaImIp3nP	produkovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
anaerobní	anaerobní	k2eAgInSc4d1	anaerobní
a	a	k8xC	a
jed	jed	k1gInSc4	jed
samotný	samotný	k2eAgInSc4d1	samotný
se	se	k3xPyFc4	se
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
rychle	rychle	k6eAd1	rychle
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
oblast	oblast	k1gFnSc1	oblast
zasažená	zasažený	k2eAgFnSc1d1	zasažená
rozptýlením	rozptýlení	k1gNnSc7	rozptýlení
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
bude	být	k5eAaImBp3nS	být
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc3	uplynutí
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
opět	opět	k6eAd1	opět
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Bioinformatics	Bioinformatics	k6eAd1	Bioinformatics
Resource	Resourka	k1gFnSc6	Resourka
Center	centrum	k1gNnPc2	centrum
-	-	kIx~	-
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc4	ten
Pathema-Clostridium	Pathema-Clostridium	k1gNnSc4	Pathema-Clostridium
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
inspekce	inspekce	k1gFnSc1	inspekce
-	-	kIx~	-
Botulismus	botulismus	k1gInSc1	botulismus
</s>
</p>
<p>
<s>
https://www.stream.cz/fenomen/611300-botox-jed-ktery-vam-vrati-mladi	[url]	k1gMnPc1	https://www.stream.cz/fenomen/611300-botox-jed-ktery-vam-vrati-mladi
-	-	kIx~	-
díl	díl	k1gInSc1	díl
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Fenomén	fenomén	k1gInSc1	fenomén
na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
televizi	televize	k1gFnSc6	televize
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
pojednávající	pojednávající	k2eAgFnSc1d1	pojednávající
o	o	k7c6	o
botoxu	botox	k1gInSc6	botox
</s>
</p>
