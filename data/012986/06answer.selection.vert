<s>
Botulotoxin	botulotoxin	k1gInSc1	botulotoxin
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
botulus	botulus	k1gInSc1	botulus
=	=	kIx~	=
klobása	klobása	k1gFnSc1	klobása
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
botulin	botulin	k1gInSc1	botulin
nebo	nebo	k8xC	nebo
klobásový	klobásový	k2eAgInSc1d1	klobásový
jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
toxická	toxický	k2eAgFnSc1d1	toxická
polypeptidická	polypeptidický	k2eAgFnSc1d1	polypeptidický
dvojsložková	dvojsložkový	k2eAgFnSc1d1	dvojsložková
směs	směs	k1gFnSc1	směs
produkovaná	produkovaný	k2eAgFnSc1d1	produkovaná
bakteriemi	bakterie	k1gFnPc7	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
botulinum	botulinum	k1gInSc4	botulinum
<g/>
.	.	kIx.	.
</s>
