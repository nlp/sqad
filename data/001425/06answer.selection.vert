<s>
Už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
první	první	k4xOgInPc1	první
příznaky	příznak	k1gInPc1	příznak
jeho	jeho	k3xOp3gFnSc2	jeho
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
amyotrofická	amyotrofický	k2eAgFnSc1d1	amyotrofická
laterální	laterální	k2eAgFnSc1d1	laterální
skleróza	skleróza	k1gFnSc1	skleróza
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
významných	významný	k2eAgMnPc2d1	významný
neurologů	neurolog	k1gMnPc2	neurolog
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c6	o
ALS	ALS	kA	ALS
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c6	o
spinální	spinální	k2eAgFnSc6d1	spinální
muskulární	muskulární	k2eAgFnSc6d1	muskulární
atrofii	atrofie	k1gFnSc6	atrofie
-	-	kIx~	-
SMA	SMA	kA	SMA
<g/>
,	,	kIx,	,
Hawkingův	Hawkingův	k2eAgInSc1d1	Hawkingův
případ	případ	k1gInSc1	případ
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
plně	plně	k6eAd1	plně
objasněn	objasněn	k2eAgMnSc1d1	objasněn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
napadá	napadat	k5eAaImIp3nS	napadat
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
postupné	postupný	k2eAgNnSc4d1	postupné
ochrnutí	ochrnutí	k1gNnSc4	ochrnutí
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
