<s>
Bauhaus	Bauhaus	k1gInSc1	Bauhaus
byl	být	k5eAaImAgInS	být
mimořádně	mimořádně	k6eAd1	mimořádně
důležitým	důležitý	k2eAgNnSc7d1	důležité
učilištěm	učiliště	k1gNnSc7	učiliště
a	a	k8xC	a
střediskem	středisko	k1gNnSc7	středisko
moderní	moderní	k2eAgFnSc2d1	moderní
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
avantgardních	avantgardní	k2eAgFnPc2d1	avantgardní
škol	škola	k1gFnPc2	škola
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
designu	design	k1gInSc2	design
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bauhaus	Bauhaus	k1gMnSc1	Bauhaus
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
založil	založit	k5eAaPmAgMnS	založit
Henry	Henry	k1gMnSc1	Henry
van	vana	k1gFnPc2	vana
de	de	k?	de
Velde	Veld	k1gInSc5	Veld
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Walter	Walter	k1gMnSc1	Walter
Gropius	Gropius	k1gMnSc1	Gropius
sloučil	sloučit	k5eAaPmAgMnS	sloučit
původní	původní	k2eAgFnSc4d1	původní
školu	škola	k1gFnSc4	škola
s	s	k7c7	s
výmarskou	výmarský	k2eAgFnSc7d1	Výmarská
akademií	akademie	k1gFnSc7	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
školu	škola	k1gFnSc4	škola
nazval	nazvat	k5eAaBmAgMnS	nazvat
Bauhaus	Bauhaus	k1gMnSc1	Bauhaus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
školu	škola	k1gFnSc4	škola
také	také	k6eAd1	také
sám	sám	k3xTgMnSc1	sám
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
měla	mít	k5eAaImAgFnS	mít
obnovit	obnovit	k5eAaPmF	obnovit
"	"	kIx"	"
<g/>
jednotu	jednota	k1gFnSc4	jednota
umění	umění	k1gNnSc2	umění
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
obnovit	obnovit	k5eAaPmF	obnovit
těsný	těsný	k2eAgInSc4d1	těsný
vztah	vztah	k1gInSc4	vztah
umění	umění	k1gNnPc2	umění
k	k	k7c3	k
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
a	a	k8xC	a
technika	technika	k1gFnSc1	technika
mají	mít	k5eAaImIp3nP	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Konečným	konečný	k2eAgInSc7d1	konečný
cílem	cíl	k1gInSc7	cíl
vší	všecek	k3xTgFnSc2	všecek
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
stát	stát	k5eAaPmF	stát
stavba	stavba	k1gFnSc1	stavba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Každý	každý	k3xTgMnSc1	každý
student	student	k1gMnSc1	student
Bauhausu	Bauhaus	k1gMnSc3	Bauhaus
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
musel	muset	k5eAaImAgMnS	muset
naučit	naučit	k5eAaPmF	naučit
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
zemské	zemský	k2eAgFnSc2d1	zemská
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
Výmaru	Výmar	k1gInSc2	Výmar
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
Desavy	Desava	k1gFnSc2	Desava
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
však	však	k9	však
škola	škola	k1gFnSc1	škola
dlouho	dlouho	k6eAd1	dlouho
nevydržela	vydržet	k5eNaPmAgFnS	vydržet
a	a	k8xC	a
ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
podruhé	podruhé	k6eAd1	podruhé
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
výrazně	výrazně	k6eAd1	výrazně
pokrokový	pokrokový	k2eAgInSc4d1	pokrokový
a	a	k8xC	a
levicový	levicový	k2eAgInSc4d1	levicový
charakter	charakter	k1gInSc4	charakter
byla	být	k5eAaImAgFnS	být
škola	škola	k1gFnSc1	škola
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nacistů	nacista	k1gMnPc2	nacista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
obviněná	obviněný	k2eAgFnSc1d1	obviněná
z	z	k7c2	z
"	"	kIx"	"
<g/>
bolševické	bolševický	k2eAgFnSc2d1	bolševická
rozvratné	rozvratný	k2eAgFnSc2d1	rozvratná
činnosti	činnost	k1gFnSc2	činnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vynikající	vynikající	k2eAgFnSc6d1	vynikající
učební	učební	k2eAgFnSc6d1	učební
metodě	metoda	k1gFnSc6	metoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
využívá	využívat	k5eAaPmIp3nS	využívat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
architektonických	architektonický	k2eAgFnPc6d1	architektonická
školách	škola	k1gFnPc6	škola
<g/>
:	:	kIx,	:
výchova	výchova	k1gFnSc1	výchova
na	na	k7c6	na
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
úkolech	úkol	k1gInPc6	úkol
pro	pro	k7c4	pro
reálnou	reálný	k2eAgFnSc4d1	reálná
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
také	také	k9	také
publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
místních	místní	k2eAgMnPc2d1	místní
pedagogů	pedagog	k1gMnPc2	pedagog
(	(	kIx(	(
<g/>
Marcel	Marcel	k1gMnSc1	Marcel
Breuer	Breuer	k1gMnSc1	Breuer
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hilberseimer	Hilberseimer	k1gMnSc1	Hilberseimer
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Klee	Kle	k1gFnSc2	Kle
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
a	a	k8xC	a
myšlení	myšlení	k1gNnSc4	myšlení
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
i	i	k9	i
mnohé	mnohý	k2eAgMnPc4d1	mnohý
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
výtvarníky	výtvarník	k1gMnPc4	výtvarník
<g/>
,	,	kIx,	,
architekty	architekt	k1gMnPc4	architekt
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
příslušníky	příslušník	k1gMnPc4	příslušník
české	český	k2eAgFnSc2d1	Česká
moderny	moderna	k1gFnSc2	moderna
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gInSc2	Teig
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
letech	let	k1gInPc6	let
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
v	v	k7c6	v
Desavě	Desava	k1gFnSc6	Desava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvním	první	k4xOgInPc3	první
kontaktům	kontakt	k1gInPc3	kontakt
dochází	docházet	k5eAaImIp3nS	docházet
již	již	k6eAd1	již
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
zárodku	zárodek	k1gInSc6	zárodek
přizváním	přizvání	k1gNnSc7	přizvání
Wenzela	Wenzela	k1gFnSc2	Wenzela
Hablika	Hablika	k1gFnSc1	Hablika
k	k	k7c3	k
"	"	kIx"	"
<g/>
Výstavě	výstava	k1gFnSc3	výstava
neznámých	známý	k2eNgMnPc2d1	neznámý
architektů	architekt	k1gMnPc2	architekt
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Kontakty	kontakt	k1gInPc1	kontakt
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
avantgardou	avantgarda	k1gFnSc7	avantgarda
lze	lze	k6eAd1	lze
vystopovat	vystopovat	k5eAaPmF	vystopovat
již	již	k9	již
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
během	během	k7c2	během
výstavy	výstava	k1gFnSc2	výstava
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Chochol	Chochol	k1gMnSc1	Chochol
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fragner	Fragner	k1gMnSc1	Fragner
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Honzík	Honzík	k1gMnSc1	Honzík
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Krejcar	krejcar	k1gInSc1	krejcar
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Emil	Emil	k1gMnSc1	Emil
Koula	Koula	k1gMnSc1	Koula
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
Linhart	Linhart	k1gMnSc1	Linhart
a	a	k8xC	a
Vít	Vít	k1gMnSc1	Vít
Obrtel	Obrtel	k1gMnSc1	Obrtel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1924	[number]	k4	1924
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
"	"	kIx"	"
<g/>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
německých	německý	k2eAgMnPc2d1	německý
stavitelů	stavitel	k1gMnPc2	stavitel
<g/>
"	"	kIx"	"
přednášel	přednášet	k5eAaImAgMnS	přednášet
naopak	naopak	k6eAd1	naopak
Walter	Walter	k1gMnSc1	Walter
Gropius	Gropius	k1gMnSc1	Gropius
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
stavební	stavební	k2eAgNnSc1d1	stavební
oddělení	oddělení	k1gNnSc1	oddělení
Bauhausu	Bauhaus	k1gInSc2	Bauhaus
(	(	kIx(	(
<g/>
Bunzel	Bunzel	k1gMnSc1	Bunzel
<g/>
,	,	kIx,	,
Meyer	Meyer	k1gMnSc1	Meyer
<g/>
,	,	kIx,	,
Sharon	Sharon	k1gMnSc1	Sharon
<g/>
)	)	kIx)	)
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
urbanistické	urbanistický	k2eAgFnPc4d1	urbanistická
soutěže	soutěž	k1gFnPc4	soutěž
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
českým	český	k2eAgMnPc3d1	český
studentům	student	k1gMnPc3	student
fotografie	fotografia	k1gFnSc2	fotografia
na	na	k7c6	na
Bauhausu	Bauhaus	k1gInSc6	Bauhaus
patřil	patřit	k5eAaImAgMnS	patřit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Koch	Koch	k1gMnSc1	Koch
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
Walter	Walter	k1gMnSc1	Walter
Gropius	Gropius	k1gMnSc1	Gropius
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
Hannes	Hannes	k1gMnSc1	Hannes
Meyer	Meyer	k1gMnSc1	Meyer
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
Ludwig	Ludwiga	k1gFnPc2	Ludwiga
Mies	Miesa	k1gFnPc2	Miesa
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gMnPc4	Roh
</s>
