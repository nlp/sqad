<s>
Škola	škola	k1gFnSc1	škola
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bauhaus	Bauhaus	k1gMnSc1	Bauhaus
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
založil	založit	k5eAaPmAgMnS	založit
Henry	Henry	k1gMnSc1	Henry
van	vana	k1gFnPc2	vana
de	de	k?	de
Velde	Veld	k1gInSc5	Veld
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
.	.	kIx.	.
</s>
