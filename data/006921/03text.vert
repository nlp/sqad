<s>
Bunki	Bunki	k1gNnSc1	Bunki
(	(	kIx(	(
<g/>
文	文	k?	文
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
Japonskou	japonský	k2eAgFnSc7d1	japonská
érou	éra	k1gFnSc7	éra
v	v	k7c6	v
období	období	k1gNnSc6	období
Sengoku	Sengok	k1gInSc2	Sengok
<g/>
,	,	kIx,	,
v	v	k7c6	v
užším	úzký	k2eAgNnSc6d2	užší
období	období	k1gNnSc6	období
Ašikaga	Ašikaga	k1gFnSc1	Ašikaga
-	-	kIx~	-
Muromači	Muromač	k1gMnPc1	Muromač
(	(	kIx(	(
<g/>
足	足	k?	足
-	-	kIx~	-
室	室	k?	室
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
Meiō	Meiō	k1gFnSc7	Meiō
(	(	kIx(	(
<g/>
明	明	k?	明
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eišó	Eišó	k1gFnSc1	Eišó
(	(	kIx(	(
<g/>
永	永	k?	永
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trvala	trvalo	k1gNnPc1	trvalo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1501	[number]	k4	1501
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1504	[number]	k4	1504
<g/>
.	.	kIx.	.
</s>
<s>
1502	[number]	k4	1502
–	–	k?	–
Umírá	umírat	k5eAaImIp3nS	umírat
Džukó	Džukó	k1gFnSc1	Džukó
Murata	Murata	k1gFnSc1	Murata
(	(	kIx(	(
<g/>
村	村	k?	村
珠	珠	k?	珠
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
čajového	čajový	k2eAgInSc2d1	čajový
obřadu	obřad	k1gInSc2	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Go-Kašiwabara	Go-Kašiwabara	k1gFnSc1	Go-Kašiwabara
(	(	kIx(	(
<g/>
後	後	k?	後
<g/>
)	)	kIx)	)
Éry	éra	k1gFnSc2	éra
Bunšó	Bunšó	k1gFnSc2	Bunšó
(	(	kIx(	(
<g/>
文	文	k?	文
<g/>
)	)	kIx)	)
Ónin	Ónin	k1gMnSc1	Ónin
(	(	kIx(	(
<g/>
応	応	k?	応
<g/>
)	)	kIx)	)
Bunmei	Bunme	k1gFnSc2	Bunme
(	(	kIx(	(
<g/>
文	文	k?	文
<g/>
)	)	kIx)	)
Čókjó	Čókjó	k1gMnSc1	Čókjó
(	(	kIx(	(
<g/>
長	長	k?	長
<g/>
)	)	kIx)	)
Entoku	Entok	k1gInSc2	Entok
(	(	kIx(	(
<g/>
延	延	k?	延
<g/>
)	)	kIx)	)
Meió	Meió	k1gMnSc1	Meió
(	(	kIx(	(
<g/>
明	明	k?	明
<g/>
)	)	kIx)	)
Bunki	Bunk	k1gFnSc2	Bunk
(	(	kIx(	(
<g/>
文	文	k?	文
<g/>
)	)	kIx)	)
Eišó	Eišó	k1gMnSc1	Eišó
(	(	kIx(	(
<g/>
永	永	k?	永
<g/>
)	)	kIx)	)
Daiei	Daie	k1gFnSc2	Daie
(	(	kIx(	(
<g/>
大	大	k?	大
<g/>
)	)	kIx)	)
Kjóroku	Kjórok	k1gInSc2	Kjórok
(	(	kIx(	(
<g/>
享	享	k?	享
<g/>
)	)	kIx)	)
</s>
