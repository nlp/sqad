<p>
<s>
100	[number]	k4	100
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
←	←	k?	←
99101	[number]	k4	99101
→	→	k?	→
<g/>
100	[number]	k4	100
</s>
</p>
<p>
<s>
←	←	k?	←
</s>
</p>
<p>
<s>
100	[number]	k4	100
</s>
</p>
<p>
<s>
101	[number]	k4	101
</s>
</p>
<p>
<s>
102	[number]	k4	102
</s>
</p>
<p>
<s>
103	[number]	k4	103
</s>
</p>
<p>
<s>
104	[number]	k4	104
</s>
</p>
<p>
<s>
105	[number]	k4	105
</s>
</p>
<p>
<s>
106	[number]	k4	106
</s>
</p>
<p>
<s>
107	[number]	k4	107
</s>
</p>
<p>
<s>
108	[number]	k4	108
</s>
</p>
<p>
<s>
109	[number]	k4	109
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
Celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
100	[number]	k4	100
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
sto	sto	k4xCgNnSc4	sto
</s>
</p>
<p>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
5	[number]	k4	5
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
5	[number]	k4	5
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
Dělitelé	dělitel	k1gMnPc1	dělitel
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
,	,	kIx,	,
100	[number]	k4	100
</s>
</p>
<p>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
SI	si	k1gNnSc1	si
předpona	předpona	k1gFnSc1	předpona
</s>
</p>
<p>
<s>
hekto	hekto	k1gNnSc1	hekto
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
centi	centi	k1gNnSc1	centi
(	(	kIx(	(
<g/>
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dvojkově	dvojkově	k6eAd1	dvojkově
</s>
</p>
<p>
<s>
11001002	[number]	k4	11001002
</s>
</p>
<p>
<s>
Trojkově	trojkově	k6eAd1	trojkově
</s>
</p>
<p>
<s>
102013	[number]	k4	102013
</s>
</p>
<p>
<s>
Čtyřkově	Čtyřkově	k6eAd1	Čtyřkově
</s>
</p>
<p>
<s>
12104	[number]	k4	12104
</s>
</p>
<p>
<s>
Pětkově	pětkově	k6eAd1	pětkově
</s>
</p>
<p>
<s>
4005	[number]	k4	4005
</s>
</p>
<p>
<s>
Šestkově	šestkově	k6eAd1	šestkově
</s>
</p>
<p>
<s>
2446	[number]	k4	2446
</s>
</p>
<p>
<s>
Sedmičkově	Sedmičkově	k6eAd1	Sedmičkově
</s>
</p>
<p>
<s>
2027	[number]	k4	2027
</s>
</p>
<p>
<s>
Osmičkově	osmičkově	k6eAd1	osmičkově
</s>
</p>
<p>
<s>
1448	[number]	k4	1448
</s>
</p>
<p>
<s>
Šestnáctkově	šestnáctkově	k6eAd1	šestnáctkově
</s>
</p>
<p>
<s>
6416	[number]	k4	6416
</s>
</p>
<p>
<s>
100	[number]	k4	100
(	(	kIx(	(
<g/>
Jedno	jeden	k4xCgNnSc1	jeden
sto	sto	k4xCgNnSc1	sto
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přirozené	přirozený	k2eAgNnSc1d1	přirozené
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
číslu	číslo	k1gNnSc3	číslo
99	[number]	k4	99
a	a	k8xC	a
předchází	předcházet	k5eAaImIp3nS	předcházet
číslu	číslo	k1gNnSc3	číslo
101	[number]	k4	101
<g/>
.	.	kIx.	.
</s>
<s>
Římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
C	C	kA	C
jako	jako	k8xS	jako
centum	centum	k1gNnSc4	centum
<g/>
.	.	kIx.	.
</s>
<s>
Tutéž	týž	k3xTgFnSc4	týž
číselnou	číselný	k2eAgFnSc4d1	číselná
hodnotu	hodnota	k1gFnSc4	hodnota
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmeno	písmeno	k1gNnSc1	písmeno
kuf	kuf	k?	kuf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matematika	matematika	k1gFnSc1	matematika
</s>
</p>
<p>
<s>
100	[number]	k4	100
<g/>
,	,	kIx,	,
sto	sto	k4xCgNnSc4	sto
<g/>
,	,	kIx,	,
také	také	k9	také
jedno	jeden	k4xCgNnSc4	jeden
sto	sto	k4xCgNnSc4	sto
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
mocnina	mocnina	k1gFnSc1	mocnina
čísla	číslo	k1gNnSc2	číslo
základu	základ	k1gInSc2	základ
desítkové	desítkový	k2eAgFnSc2d1	desítková
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
10	[number]	k4	10
<g/>
,	,	kIx,	,
102	[number]	k4	102
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc1	sto
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
měnových	měnový	k2eAgMnPc2d1	měnový
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stovka	stovka	k1gFnSc1	stovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předpona	předpona	k1gFnSc1	předpona
SI	se	k3xPyFc3	se
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgNnSc1d1	standardní
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
SI	si	k1gNnSc1	si
předpona	předpona	k1gFnSc1	předpona
pro	pro	k7c4	pro
stovku	stovka	k1gFnSc4	stovka
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
hekto-	hekto-	k?	hekto-
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hektometr	hektometr	k1gInSc1	hektometr
<g/>
;	;	kIx,	;
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
se	se	k3xPyFc4	se
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
na	na	k7c4	na
"	"	kIx"	"
<g/>
hekt-	hekt-	k?	hekt-
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hektar	hektar	k1gInSc4	hektar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sto	sto	k4xCgNnSc1	sto
jako	jako	k8xC	jako
součet	součet	k1gInSc1	součet
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
součtem	součet	k1gInSc7	součet
prvních	první	k4xOgInPc2	první
devíti	devět	k4xCc2	devět
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
,	,	kIx,	,
součtem	součet	k1gInSc7	součet
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
47	[number]	k4	47
a	a	k8xC	a
53	[number]	k4	53
či	či	k8xC	či
součtem	součet	k1gInSc7	součet
třetích	třetí	k4xOgFnPc2	třetí
mocnin	mocnina	k1gFnPc2	mocnina
prvních	první	k4xOgNnPc2	první
čtyř	čtyři	k4xCgNnPc2	čtyři
přirozených	přirozený	k2eAgNnPc2d1	přirozené
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
platí	platit	k5eAaImIp3nS	platit
že	že	k8xS	že
26	[number]	k4	26
+	+	kIx~	+
62	[number]	k4	62
=	=	kIx~	=
100	[number]	k4	100
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
Leylandovo	Leylandův	k2eAgNnSc4d1	Leylandův
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Procentní	procentní	k2eAgFnSc1d1	procentní
část	část	k1gFnSc1	část
celku	celek	k1gInSc2	celek
</s>
</p>
<p>
<s>
100	[number]	k4	100
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
procent	procent	k1gInSc4	procent
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
%	%	kIx~	%
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
setinou	setina	k1gFnSc7	setina
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
<g/>
.	.	kIx.	.
100	[number]	k4	100
%	%	kIx~	%
tedy	tedy	k9	tedy
znamená	znamenat	k5eAaImIp3nS	znamenat
plné	plný	k2eAgNnSc4d1	plné
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vojenství	vojenství	k1gNnSc6	vojenství
</s>
</p>
<p>
<s>
Sto	sto	k4xCgNnSc1	sto
bývá	bývat	k5eAaImIp3nS	bývat
(	(	kIx(	(
<g/>
přibližný	přibližný	k2eAgInSc4d1	přibližný
<g/>
)	)	kIx)	)
počet	počet	k1gInSc4	počet
členů	člen	k1gMnPc2	člen
vojenské	vojenský	k2eAgFnSc2d1	vojenská
jednotky	jednotka	k1gFnSc2	jednotka
setniny	setnina	k1gFnSc2	setnina
(	(	kIx(	(
<g/>
v	v	k7c6	v
antickém	antický	k2eAgInSc6d1	antický
Římě	Řím	k1gInSc6	Řím
centurie	centurie	k1gFnSc2	centurie
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
centurion	centurion	k1gInSc1	centurion
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
velitel	velitel	k1gMnSc1	velitel
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
setník	setník	k1gMnSc1	setník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
časomíře	časomíra	k1gFnSc6	časomíra
</s>
</p>
<p>
<s>
100	[number]	k4	100
bylo	být	k5eAaImAgNnS	být
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
hodině	hodina	k1gFnSc6	hodina
(	(	kIx(	(
<g/>
těch	ten	k3xDgMnPc2	ten
bylo	být	k5eAaImAgNnS	být
10	[number]	k4	10
ve	v	k7c6	v
dni	den	k1gInSc6	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
100	[number]	k4	100
sekund	sekunda	k1gFnPc2	sekunda
v	v	k7c6	v
minutě	minuta	k1gFnSc6	minuta
v	v	k7c6	v
revoluční	revoluční	k2eAgFnSc6d1	revoluční
časomíře	časomíra	k1gFnSc6	časomíra
zavedené	zavedený	k2eAgFnSc6d1	zavedená
během	během	k7c2	během
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
měl	mít	k5eAaImAgInS	mít
tedy	tedy	k9	tedy
100	[number]	k4	100
000	[number]	k4	000
sekund	sekunda	k1gFnPc2	sekunda
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
srovnatelně	srovnatelně	k6eAd1	srovnatelně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
86	[number]	k4	86
400	[number]	k4	400
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
časomíře	časomíra	k1gFnSc6	časomíra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
významy	význam	k1gInPc1	význam
</s>
</p>
<p>
<s>
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
fermia	fermium	k1gNnSc2	fermium
</s>
</p>
<p>
<s>
100	[number]	k4	100
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
počet	počet	k1gInSc1	počet
let	léto	k1gNnPc2	léto
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
století	století	k1gNnSc6	století
</s>
</p>
<p>
<s>
obvyklý	obvyklý	k2eAgInSc4d1	obvyklý
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
základní	základní	k2eAgFnSc1d1	základní
měnová	měnový	k2eAgFnSc1d1	měnová
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
koruna	koruna	k1gFnSc1	koruna
má	mít	k5eAaImIp3nS	mít
sto	sto	k4xCgNnSc4	sto
haléřů	haléř	k1gInPc2	haléř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
100	[number]	k4	100
m	m	kA	m
-	-	kIx~	-
lehkoatletický	lehkoatletický	k2eAgInSc4d1	lehkoatletický
sprint	sprint	k1gInSc4	sprint
</s>
</p>
<p>
<s>
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sto	sto	k4xCgNnSc4	sto
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc3	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
100	[number]	k4	100
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
<g/>
mw-parser-output	mwarserutput	k1gInSc1	mw-parser-output
.	.	kIx.	.
<g/>
navbox-title	navboxitle	k1gFnSc1	navbox-title
.	.	kIx.	.
<g/>
mw-collapsible-toggle	mwollapsibleoggle	k1gNnSc1	mw-collapsible-toggle
<g/>
{	{	kIx(	{
<g/>
font-weight	fonteight	k2eAgInSc4d1	font-weight
<g/>
:	:	kIx,	:
<g/>
normal	normal	k1gInSc4	normal
<g/>
}	}	kIx)	}
Přirozená	přirozený	k2eAgNnPc4d1	přirozené
čísla	číslo	k1gNnPc4	číslo
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
199	[number]	k4	199
</s>
</p>
<p>
<s>
100	[number]	k4	100
•	•	k?	•
101	[number]	k4	101
•	•	k?	•
102	[number]	k4	102
•	•	k?	•
103	[number]	k4	103
•	•	k?	•
104	[number]	k4	104
•	•	k?	•
105	[number]	k4	105
•	•	k?	•
106	[number]	k4	106
•	•	k?	•
107	[number]	k4	107
•	•	k?	•
108	[number]	k4	108
•	•	k?	•
109	[number]	k4	109
•	•	k?	•
110	[number]	k4	110
•	•	k?	•
111	[number]	k4	111
•	•	k?	•
112	[number]	k4	112
•	•	k?	•
113	[number]	k4	113
•	•	k?	•
114	[number]	k4	114
•	•	k?	•
115	[number]	k4	115
•	•	k?	•
116	[number]	k4	116
•	•	k?	•
117	[number]	k4	117
•	•	k?	•
118	[number]	k4	118
•	•	k?	•
119	[number]	k4	119
•	•	k?	•
120	[number]	k4	120
•	•	k?	•
121	[number]	k4	121
•	•	k?	•
122	[number]	k4	122
•	•	k?	•
123	[number]	k4	123
•	•	k?	•
124	[number]	k4	124
•	•	k?	•
125	[number]	k4	125
<g />
.	.	kIx.	.
</s>
<s>
•	•	k?	•
126	[number]	k4	126
•	•	k?	•
127	[number]	k4	127
•	•	k?	•
128	[number]	k4	128
•	•	k?	•
129	[number]	k4	129
•	•	k?	•
130	[number]	k4	130
•	•	k?	•
131	[number]	k4	131
•	•	k?	•
132	[number]	k4	132
•	•	k?	•
133	[number]	k4	133
•	•	k?	•
134	[number]	k4	134
•	•	k?	•
135	[number]	k4	135
•	•	k?	•
136	[number]	k4	136
•	•	k?	•
137	[number]	k4	137
•	•	k?	•
138	[number]	k4	138
•	•	k?	•
139	[number]	k4	139
•	•	k?	•
140	[number]	k4	140
•	•	k?	•
141	[number]	k4	141
•	•	k?	•
142	[number]	k4	142
•	•	k?	•
143	[number]	k4	143
•	•	k?	•
144	[number]	k4	144
•	•	k?	•
145	[number]	k4	145
•	•	k?	•
146	[number]	k4	146
•	•	k?	•
147	[number]	k4	147
•	•	k?	•
148	[number]	k4	148
•	•	k?	•
149	[number]	k4	149
•	•	k?	•
150	[number]	k4	150
<g />
.	.	kIx.	.
</s>
<s>
•	•	k?	•
151	[number]	k4	151
•	•	k?	•
152	[number]	k4	152
•	•	k?	•
153	[number]	k4	153
•	•	k?	•
154	[number]	k4	154
•	•	k?	•
155	[number]	k4	155
•	•	k?	•
156	[number]	k4	156
•	•	k?	•
157	[number]	k4	157
•	•	k?	•
158	[number]	k4	158
•	•	k?	•
159	[number]	k4	159
•	•	k?	•
160	[number]	k4	160
•	•	k?	•
161	[number]	k4	161
•	•	k?	•
162	[number]	k4	162
•	•	k?	•
163	[number]	k4	163
•	•	k?	•
164	[number]	k4	164
•	•	k?	•
165	[number]	k4	165
•	•	k?	•
166	[number]	k4	166
•	•	k?	•
167	[number]	k4	167
•	•	k?	•
168	[number]	k4	168
•	•	k?	•
169	[number]	k4	169
•	•	k?	•
170	[number]	k4	170
•	•	k?	•
171	[number]	k4	171
•	•	k?	•
172	[number]	k4	172
•	•	k?	•
173	[number]	k4	173
•	•	k?	•
174	[number]	k4	174
•	•	k?	•
175	[number]	k4	175
<g />
.	.	kIx.	.
</s>
<s>
•	•	k?	•
176	[number]	k4	176
•	•	k?	•
177	[number]	k4	177
•	•	k?	•
178	[number]	k4	178
•	•	k?	•
179	[number]	k4	179
•	•	k?	•
180	[number]	k4	180
•	•	k?	•
181	[number]	k4	181
•	•	k?	•
182	[number]	k4	182
•	•	k?	•
183	[number]	k4	183
•	•	k?	•
184	[number]	k4	184
•	•	k?	•
185	[number]	k4	185
•	•	k?	•
186	[number]	k4	186
•	•	k?	•
187	[number]	k4	187
•	•	k?	•
188	[number]	k4	188
•	•	k?	•
189	[number]	k4	189
•	•	k?	•
190	[number]	k4	190
•	•	k?	•
191	[number]	k4	191
•	•	k?	•
192	[number]	k4	192
•	•	k?	•
193	[number]	k4	193
•	•	k?	•
194	[number]	k4	194
•	•	k?	•
195	[number]	k4	195
•	•	k?	•
196	[number]	k4	196
•	•	k?	•
197	[number]	k4	197
•	•	k?	•
198	[number]	k4	198
•	•	k?	•
199	[number]	k4	199
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
<g/>
99	[number]	k4	99
•	•	k?	•
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
199	[number]	k4	199
•	•	k?	•
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
299	[number]	k4	299
•	•	k?	•
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
399	[number]	k4	399
•	•	k?	•
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
499	[number]	k4	499
•	•	k?	•
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
599	[number]	k4	599
•	•	k?	•
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
699	[number]	k4	699
•	•	k?	•
700	[number]	k4	700
<g/>
–	–	k?	–
<g/>
799	[number]	k4	799
•	•	k?	•
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
899	[number]	k4	899
•	•	k?	•
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
999	[number]	k4	999
•	•	k?	•
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
1099	[number]	k4	1099
</s>
</p>
