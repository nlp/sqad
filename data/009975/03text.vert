<p>
<s>
Sisinius	Sisinius	k1gInSc1	Sisinius
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
650	[number]	k4	650
Sýrie	Sýrie	k1gFnSc1	Sýrie
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
708	[number]	k4	708
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
708	[number]	k4	708
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
dvacet	dvacet	k4xCc4	dvacet
jeden	jeden	k4xCgInSc4	jeden
den	dna	k1gFnPc2	dna
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
několika	několik	k4yIc3	několik
nejkratším	krátký	k2eAgFnPc3d3	nejkratší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
těžce	těžce	k6eAd1	těžce
postižen	postihnout	k5eAaPmNgInS	postihnout
dnou	dna	k1gFnSc7	dna
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
téměř	téměř	k6eAd1	téměř
ani	ani	k8xC	ani
hýbat	hýbat	k5eAaImF	hýbat
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
ho	on	k3xPp3gMnSc4	on
museli	muset	k5eAaImAgMnP	muset
krmit	krmit	k5eAaImF	krmit
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
kratičkého	kratičký	k2eAgInSc2d1	kratičký
pontifikátu	pontifikát	k1gInSc2	pontifikát
stačil	stačit	k5eAaBmAgMnS	stačit
vykonat	vykonat	k5eAaPmF	vykonat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zahájení	zahájení	k1gNnSc1	zahájení
obnovy	obnova	k1gFnSc2	obnova
římských	římský	k2eAgFnPc2d1	římská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
dokončili	dokončit	k5eAaPmAgMnP	dokončit
až	až	k9	až
jeho	jeho	k3xOp3gMnPc1	jeho
následovníci	následovník	k1gMnPc1	následovník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
účelu	účel	k1gInSc3	účel
založil	založit	k5eAaPmAgMnS	založit
několik	několik	k4yIc4	několik
vápenek	vápenka	k1gFnPc2	vápenka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sisiniovi	Sisinius	k1gMnSc6	Sisinius
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
papež	papež	k1gMnSc1	papež
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
Syřan	Syřan	k1gMnSc1	Syřan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
