<s>
Darwinova	Darwinův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
ocenění	ocenění	k1gNnSc1
udělované	udělovaný	k2eAgNnSc1d1
britskou	britský	k2eAgFnSc7d1
královskou	královský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Darwinova	Darwinův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
nebo	nebo	k8xC
americkou	americký	k2eAgFnSc4d1
filmovou	filmový	k2eAgFnSc4d1
komedii	komedie	k1gFnSc4
Darwinovy	Darwinův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Darwinova	Darwinův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
Darwin	Darwin	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ironické	ironický	k2eAgNnSc1d1
ocenění	ocenění	k1gNnSc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
„	„	kIx„
<g/>
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
zasloužili	zasloužit	k5eAaPmAgMnP
o	o	k7c4
zlepšení	zlepšení	k1gNnSc4
lidského	lidský	k2eAgInSc2d1
genofondu	genofond	k1gInSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gMnSc2
sami	sám	k3xTgMnPc1
odstranili	odstranit	k5eAaPmAgMnP
hloupým	hloupý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
“	“	kIx“
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
je	být	k5eAaImIp3nS
pojmenována	pojmenovat	k5eAaPmNgFnS
po	po	k7c6
Charlesi	Charles	k1gMnSc3
Darwinovi	Darwin	k1gMnSc3
<g/>
,	,	kIx,
zakladateli	zakladatel	k1gMnSc3
evoluční	evoluční	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejde	jít	k5eNaImIp3nS
o	o	k7c4
žádné	žádný	k3yNgNnSc4
hmotné	hmotný	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
uděluje	udělovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
internetové	internetový	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
DarwinAwards	DarwinAwards	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
jen	jen	k9
jako	jako	k9
určité	určitý	k2eAgInPc4d1
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
posmrtné	posmrtný	k2eAgNnSc1d1
<g/>
)	)	kIx)
uznání	uznání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Principem	princip	k1gInSc7
ceny	cena	k1gFnSc2
je	být	k5eAaImIp3nS
předpoklad	předpoklad	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
lidská	lidský	k2eAgFnSc1d1
hloupost	hloupost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
způsobí	způsobit	k5eAaPmIp3nS
smrt	smrt	k1gFnSc4
svému	svůj	k3xOyFgMnSc3
nositeli	nositel	k1gMnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
geneticky	geneticky	k6eAd1
a	a	k8xC
v	v	k7c6
duchu	duch	k1gMnSc6
Darwinovy	Darwinův	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
přirozeného	přirozený	k2eAgInSc2d1
výběru	výběr	k1gInSc2
se	se	k3xPyFc4
tak	tak	k9
tento	tento	k3xDgInSc1
nebezpečný	bezpečný	k2eNgInSc1d1
gen	gen	k1gInSc1
odstraňuje	odstraňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Příběhy	příběh	k1gInPc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
si	se	k3xPyFc3
sami	sám	k3xTgMnPc1
sobě	se	k3xPyFc3
přivodili	přivodit	k5eAaPmAgMnP,k5eAaBmAgMnP
kuriózní	kuriózní	k2eAgFnSc4d1
smrt	smrt	k1gFnSc4
neuvěřitelně	uvěřitelně	k6eNd1
hloupým	hloupý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
(	(	kIx(
<g/>
např.	např.	kA
smyšlený	smyšlený	k2eAgInSc1d1
příběh	příběh	k1gInSc1
muže	muž	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
si	se	k3xPyFc3
na	na	k7c4
své	svůj	k3xOyFgNnSc4
auto	auto	k1gNnSc4
namontoval	namontovat	k5eAaPmAgMnS
startovací	startovací	k2eAgFnSc4d1
raketu	raketa	k1gFnSc4
pro	pro	k7c4
nákladní	nákladní	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zpočátku	zpočátku	k6eAd1
kolovaly	kolovat	k5eAaImAgFnP
jen	jen	k9
skrze	skrze	k?
e-maily	e-mail	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
bioložka	bioložka	k1gFnSc1
Wendy	Wenda	k1gFnSc2
Northcutt	Northcutta	k1gFnPc2
začala	začít	k5eAaPmAgFnS
sbírat	sbírat	k5eAaImF
a	a	k8xC
později	pozdě	k6eAd2
založila	založit	k5eAaPmAgFnS
web	web	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
ceny	cena	k1gFnPc1
udělují	udělovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1
</s>
<s>
Podle	podle	k7c2
Wendy	Wenda	k1gFnSc2
Northcutt	Northcutt	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
získání	získání	k1gNnSc4
ceny	cena	k1gFnPc4
nutné	nutný	k2eAgNnSc1d1
splnit	splnit	k5eAaPmF
pět	pět	k4xCc4
požadavků	požadavek	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Neschopnost	neschopnost	k1gFnSc1
reprodukce	reprodukce	k1gFnSc2
–	–	k?
nominovaný	nominovaný	k2eAgMnSc1d1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
zabít	zabít	k5eAaPmF
nebo	nebo	k8xC
sterilizovat	sterilizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Skutečně	skutečně	k6eAd1
výjimečné	výjimečný	k2eAgNnSc1d1
a	a	k8xC
neuvěřitelné	uvěřitelný	k2eNgNnSc1d1
selhání	selhání	k1gNnSc1
soudnosti	soudnost	k1gFnSc2
–	–	k?
kandidátova	kandidátův	k2eAgFnSc1d1
hloupost	hloupost	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
jedinečná	jedinečný	k2eAgFnSc1d1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
příliš	příliš	k6eAd1
běžně	běžně	k6eAd1
konané	konaný	k2eAgFnSc2d1
hlouposti	hloupost	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
hraní	hraní	k1gNnPc4
ruské	ruský	k2eAgFnSc2d1
rulety	ruleta	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
je	být	k5eAaImIp3nS
výjimkou	výjimka	k1gFnSc7
ocenění	ocenění	k1gNnSc2
mladíka	mladík	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zmíněnou	zmíněný	k2eAgFnSc4d1
hru	hra	k1gFnSc4
hrál	hrát	k5eAaImAgInS
s	s	k7c7
automatickou	automatický	k2eAgFnSc7d1
pistolí	pistol	k1gFnSc7
<g/>
;	;	kIx,
močení	močení	k1gNnSc4
na	na	k7c4
třetí	třetí	k4xOgFnSc4
kolejnici	kolejnice	k1gFnSc4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
z	z	k7c2
nominací	nominace	k1gFnPc2
výslovně	výslovně	k6eAd1
vyloučené	vyloučený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Kandidát	kandidát	k1gMnSc1
se	se	k3xPyFc4
z	z	k7c2
genofondu	genofond	k1gInSc2
musí	muset	k5eAaImIp3nS
odstranit	odstranit	k5eAaPmF
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
svou	svůj	k3xOyFgFnSc7
vlastní	vlastní	k2eAgFnSc7d1
hloupostí	hloupost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Kandidát	kandidát	k1gMnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
schopen	schopen	k2eAgMnSc1d1
jasného	jasný	k2eAgInSc2d1
úsudku	úsudek	k1gInSc2
(	(	kIx(
<g/>
cenu	cena	k1gFnSc4
nemohou	moct	k5eNaImIp3nP
dostat	dostat	k5eAaPmF
mentálně	mentálně	k6eAd1
retardovaní	retardovaný	k2eAgMnPc1d1
<g/>
,	,	kIx,
duševně	duševně	k6eAd1
nemocní	nemocný	k1gMnPc1
a	a	k8xC
děti	dítě	k1gFnPc1
do	do	k7c2
šestnácti	šestnáct	k4xCc2
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pravdivost	pravdivost	k1gFnSc1
–	–	k?
událost	událost	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
ověřená	ověřený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Příhody	příhoda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
nebyly	být	k5eNaImAgInP
potvrzeny	potvrdit	k5eAaPmNgInP
nějakým	nějaký	k3yIgInSc7
důvěryhodným	důvěryhodný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
umisťovány	umisťovat	k5eAaImNgFnP
do	do	k7c2
speciální	speciální	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
pojmenované	pojmenovaný	k2eAgFnSc2d1
„	„	k?
<g/>
urban	urban	k1gMnSc1
legends	legends	k6eAd1
<g/>
“	“	k?
(	(	kIx(
<g/>
městské	městský	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Darwinovy	Darwinův	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Darwinova	Darwinův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
DarwinAwards	DarwinAwards	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
