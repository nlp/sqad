<s>
Darwinova	Darwinův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
Darwin	Darwin	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ironické	ironický	k2eAgNnSc1d1
ocenění	ocenění	k1gNnSc1
pro	pro	k7c4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
„	„	kIx„
<g/>
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
zasloužili	zasloužit	k5eAaPmAgMnP
o	o	k7c4
zlepšení	zlepšení	k1gNnSc4
lidského	lidský	k2eAgInSc2d1
genofondu	genofond	k1gInSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gMnSc2
sami	sám	k3xTgMnPc1
odstranili	odstranit	k5eAaPmAgMnP
hloupým	hloupý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
“	“	kIx“
<g/>
.	.	kIx.
</s>