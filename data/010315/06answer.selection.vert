<s>
Důsledkem	důsledek	k1gInSc7	důsledek
vysokého	vysoký	k2eAgInSc2d1	vysoký
podílu	podíl	k1gInSc2	podíl
solí	sůl	k1gFnPc2	sůl
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hustota	hustota	k1gFnSc1	hustota
vody	voda	k1gFnSc2	voda
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
vyšší	vysoký	k2eAgFnSc1d2	vyšší
vztlaková	vztlakový	k2eAgFnSc1d1	vztlaková
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c4	na
plovoucí	plovoucí	k2eAgInPc4d1	plovoucí
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
