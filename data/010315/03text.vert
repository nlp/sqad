<p>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
י	י	k?	י
<g/>
ָ	ָ	k?	ָ
<g/>
ם	ם	k?	ם
ה	ה	k?	ה
<g/>
ַ	ַ	k?	ַ
<g/>
מ	מ	k?	מ
<g/>
ֶ	ֶ	k?	ֶ
<g/>
ּ	ּ	k?	ּ
<g/>
ל	ל	k?	ל
<g/>
ַ	ַ	k?	ַ
<g/>
ח	ח	k?	ח
<g/>
,	,	kIx,	,
jam	jam	k1gInSc1	jam
ha-Melach	ha-Melacha	k1gFnPc2	ha-Melacha
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc4	moře
Soli	sůl	k1gFnSc2	sůl
<g/>
;	;	kIx,	;
<g/>
"	"	kIx"	"
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
ا	ا	k?	ا
<g/>
;	;	kIx,	;
al-bahr	alahr	k1gMnSc1	al-bahr
al-Majit	al-Majit	k1gMnSc1	al-Majit
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezodtoké	bezodtoký	k2eAgNnSc1d1	bezodtoké
slané	slaný	k2eAgNnSc1d1	slané
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Jordánskem	Jordánsko	k1gNnSc7	Jordánsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
420	[number]	k4	420
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgNnSc7d1	položené
odkrytým	odkrytý	k2eAgNnSc7d1	odkryté
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgNnSc7d1	položené
slaným	slaný	k2eAgNnSc7d1	slané
jezerem	jezero	k1gNnSc7	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maximální	maximální	k2eAgFnSc2d1	maximální
hloubky	hloubka	k1gFnSc2	hloubka
380	[number]	k4	380
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejhlubším	hluboký	k2eAgNnSc7d3	nejhlubší
hypersalinitním	hypersalinitní	k2eAgNnSc7d1	hypersalinitní
jezerem	jezero	k1gNnSc7	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
salinita	salinita	k1gFnSc1	salinita
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
33,7	[number]	k4	33,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejslanější	slaný	k2eAgNnPc4d3	nejslanější
jezera	jezero	k1gNnPc4	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc4d2	veliký
salinitu	salinita	k1gFnSc4	salinita
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
jezera	jezero	k1gNnSc2	jezero
Assal	Assal	k1gMnSc1	Assal
<g/>
,	,	kIx,	,
Garabogazköl	Garabogazköl	k1gMnSc1	Garabogazköl
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
hypersalinitní	hypersalinitní	k2eAgNnPc4d1	hypersalinitní
jezera	jezero	k1gNnPc4	jezero
v	v	k7c6	v
Dry	Dry	k1gFnSc6	Dry
Valleys	Valleysa	k1gFnPc2	Valleysa
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
McMurdo	McMurdo	k1gNnSc1	McMurdo
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
salinita	salinita	k1gFnSc1	salinita
je	být	k5eAaImIp3nS	být
8,8	[number]	k4	8,8
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
9,4	[number]	k4	9,4
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
salinita	salinita	k1gFnSc1	salinita
oceánská	oceánský	k2eAgFnSc1d1	oceánská
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
vlastnost	vlastnost	k1gFnSc1	vlastnost
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
činí	činit	k5eAaImIp3nS	činit
drsné	drsný	k2eAgNnSc1d1	drsné
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1050	[number]	k4	1050
km2	km2	k4	km2
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
–	–	k?	–
810	[number]	k4	810
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
67	[number]	k4	67
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
maximálně	maximálně	k6eAd1	maximálně
18	[number]	k4	18
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,,	,,	k?	,,
neboť	neboť	k8xC	neboť
jezero	jezero	k1gNnSc1	jezero
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
vysychá	vysychat	k5eAaImIp3nS	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Jordánském	jordánský	k2eAgNnSc6d1	Jordánské
údolí	údolí	k1gNnSc6	údolí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
přítokem	přítok	k1gInSc7	přítok
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
Jordán	Jordán	k1gInSc1	Jordán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
se	se	k3xPyFc4	se
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
al-bahr	alahr	k1gMnSc1	al-bahr
al-Majit	al-Majit	k1gMnSc1	al-Majit
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
jako	jako	k8xS	jako
bahr	bahr	k1gMnSc1	bahr
Lut	Lut	k1gMnSc1	Lut
(	(	kIx(	(
<g/>
ب	ب	k?	ب
ل	ل	k?	ل
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lotovo	Lotův	k2eAgNnSc4d1	Lotovo
moře	moře	k1gNnSc4	moře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
historickým	historický	k2eAgInSc7d1	historický
názvem	název	k1gInSc7	název
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc1	moře
Zo	Zo	k1gFnSc1	Zo
<g/>
'	'	kIx"	'
<g/>
ar	ar	k1gInSc1	ar
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
po	po	k7c6	po
nedaleko	daleko	k6eNd1	daleko
stojícím	stojící	k2eAgNnSc6d1	stojící
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
se	se	k3xPyFc4	se
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
jam	jam	k1gInSc1	jam
ha-Melach	ha-Melacha	k1gFnPc2	ha-Melacha
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc1	moře
Soli	sůl	k1gFnSc2	sůl
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
jam	jáma	k1gFnPc2	jáma
ha-Mavet	ha-Mavet	k1gMnSc1	ha-Mavet
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Moře	moře	k1gNnSc1	moře
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
bylo	být	k5eAaImAgNnS	být
někdy	někdy	k6eAd1	někdy
zmiňováno	zmiňovat	k5eAaImNgNnS	zmiňovat
jako	jako	k8xC	jako
jam	jam	k1gInSc1	jam
ha-Mizrachi	ha-Mizrachi	k1gNnSc2	ha-Mizrachi
(	(	kIx(	(
<g/>
י	י	k?	י
ה	ה	k?	ה
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Východní	východní	k2eAgNnSc4d1	východní
moře	moře	k1gNnSc4	moře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
jam	jam	k1gInSc4	jam
ha-Arava	ha-Arava	k1gFnSc1	ha-Arava
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Aravské	Aravský	k2eAgNnSc1d1	Aravský
moře	moře	k1gNnSc1	moře
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
zájem	zájem	k1gInSc1	zájem
návštěvníků	návštěvník	k1gMnPc2	návštěvník
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Středozemí	Středozem	k1gFnSc7	Středozem
už	už	k9	už
po	po	k7c6	po
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
ukrýval	ukrývat	k5eAaImAgMnS	ukrývat
Král	Král	k1gMnSc1	Král
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
světově	světově	k6eAd1	světově
prvních	první	k4xOgNnPc2	první
léčebných	léčebný	k2eAgNnPc2d1	léčebné
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
zdroj	zdroj	k1gInSc4	zdroj
mnoha	mnoho	k4c2	mnoho
produktů	produkt	k1gInPc2	produkt
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
od	od	k7c2	od
mumifikace	mumifikace	k1gFnSc2	mumifikace
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
až	až	k9	až
po	po	k7c4	po
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobřeží	pobřeží	k1gNnSc2	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Břehy	břeh	k1gInPc1	břeh
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
východě	východ	k1gInSc6	východ
jsou	být	k5eAaImIp3nP	být
srázné	srázný	k2eAgInPc1d1	srázný
a	a	k8xC	a
skalnaté	skalnatý	k2eAgInPc1d1	skalnatý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
táhlá	táhlý	k2eAgFnSc1d1	táhlá
rovina	rovina	k1gFnSc1	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
pusté	pustý	k2eAgNnSc1d1	pusté
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
oáz	oáza	k1gFnPc2	oáza
a	a	k8xC	a
turistických	turistický	k2eAgInPc2d1	turistický
resortů	resort	k1gInPc2	resort
s	s	k7c7	s
plážemi	pláž	k1gFnPc7	pláž
pro	pro	k7c4	pro
koupání	koupání	k1gNnSc4	koupání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jižního	jižní	k2eAgInSc2d1	jižní
břehu	břeh	k1gInSc2	břeh
probíhá	probíhat	k5eAaImIp3nS	probíhat
těžba	těžba	k1gFnSc1	těžba
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
chemický	chemický	k2eAgInSc4d1	chemický
komplex	komplex	k1gInSc4	komplex
pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
vytěžených	vytěžený	k2eAgFnPc2d1	vytěžená
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
řeka	řeka	k1gFnSc1	řeka
Jordán	Jordán	k1gInSc1	Jordán
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
ústí	ústit	k5eAaImIp3nS	ústit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
četná	četný	k2eAgNnPc4d1	četné
vádí	vádí	k1gNnPc4	vádí
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
ústí	ústit	k5eAaImIp3nS	ústit
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
i	i	k8xC	i
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
dešťovém	dešťový	k2eAgNnSc6d1	dešťové
období	období	k1gNnSc6	období
občasné	občasný	k2eAgInPc4d1	občasný
silné	silný	k2eAgInPc4d1	silný
vodní	vodní	k2eAgInPc4d1	vodní
průtoky	průtok	k1gInPc4	průtok
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
vádí	vádí	k1gNnPc1	vádí
nachal	nachal	k1gMnSc1	nachal
Chever	Chever	k1gMnSc1	Chever
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
Judské	judský	k2eAgFnSc2d1	Judská
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horké	Horké	k2eAgNnSc1d1	Horké
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
klima	klima	k1gNnSc1	klima
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nízké	nízký	k2eAgFnPc4d1	nízká
srážky	srážka	k1gFnPc4	srážka
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
a	a	k8xC	a
vysoké	vysoký	k2eAgNnSc1d1	vysoké
vypařování	vypařování	k1gNnSc1	vypařování
z	z	k7c2	z
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
době	doba	k1gFnSc6	doba
značně	značně	k6eAd1	značně
kolísala	kolísat	k5eAaImAgFnS	kolísat
hladina	hladina	k1gFnSc1	hladina
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
12	[number]	k4	12
m	m	kA	m
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
trvalému	trvalý	k2eAgInSc3d1	trvalý
poklesu	pokles	k1gInSc3	pokles
hladiny	hladina	k1gFnSc2	hladina
jezera	jezero	k1gNnSc2	jezero
především	především	k9	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
umělého	umělý	k2eAgNnSc2d1	umělé
zadržování	zadržování	k1gNnSc2	zadržování
vody	voda	k1gFnSc2	voda
řeky	řeka	k1gFnSc2	řeka
Jordán	Jordán	k1gInSc1	Jordán
v	v	k7c6	v
Galilejském	galilejský	k2eAgNnSc6d1	Galilejské
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
zveřejněné	zveřejněný	k2eAgInPc4d1	zveřejněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
30	[number]	k4	30
let	léto	k1gNnPc2	léto
ubylo	ubýt	k5eAaPmAgNnS	ubýt
v	v	k7c6	v
Mrtvém	mrtvý	k2eAgNnSc6d1	mrtvé
moři	moře	k1gNnSc6	moře
asi	asi	k9	asi
14	[number]	k4	14
km	km	kA	km
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hladina	hladina	k1gFnSc1	hladina
klesá	klesat	k5eAaImIp3nS	klesat
ročně	ročně	k6eAd1	ročně
asi	asi	k9	asi
o	o	k7c4	o
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
proces	proces	k1gInSc1	proces
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
stejným	stejný	k2eAgNnSc7d1	stejné
tempem	tempo	k1gNnSc7	tempo
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2050	[number]	k4	2050
zcela	zcela	k6eAd1	zcela
vyschnout	vyschnout	k5eAaPmF	vyschnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
plán	plán	k1gInSc1	plán
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
180	[number]	k4	180
kilometrů	kilometr	k1gInPc2	kilometr
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
kanálu	kanál	k1gInSc2	kanál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
z	z	k7c2	z
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
přes	přes	k7c4	přes
vádí	vádí	k1gNnPc4	vádí
al-Araba	al-Araba	k1gMnSc1	al-Araba
přiváděl	přivádět	k5eAaImAgMnS	přivádět
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
mořskou	mořský	k2eAgFnSc4d1	mořská
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záměr	záměr	k1gInSc1	záměr
ale	ale	k9	ale
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
řada	řada	k1gFnSc1	řada
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
Institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
studia	studio	k1gNnPc4	studio
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Arava	Aravo	k1gNnSc2	Aravo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ekologické	ekologický	k2eAgFnPc1d1	ekologická
organizace	organizace	k1gFnPc1	organizace
Přátelé	přítel	k1gMnPc1	přítel
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
varují	varovat	k5eAaImIp3nP	varovat
před	před	k7c7	před
devastujícím	devastující	k2eAgInSc7d1	devastující
dopadem	dopad	k1gInSc7	dopad
projektu	projekt	k1gInSc2	projekt
na	na	k7c6	na
údolí	údolí	k1gNnSc6	údolí
vádí	vádí	k1gNnSc2	vádí
Arava	Aravo	k1gNnSc2	Aravo
a	a	k8xC	a
na	na	k7c4	na
ekosystém	ekosystém	k1gInSc4	ekosystém
Akabského	Akabský	k2eAgInSc2d1	Akabský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Horké	Horké	k2eAgNnSc1d1	Horké
klima	klima	k1gNnSc1	klima
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
rovněž	rovněž	k9	rovněž
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
mineralizaci	mineralizace	k1gFnSc4	mineralizace
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Slanost	slanost	k1gFnSc1	slanost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
30	[number]	k4	30
až	až	k9	až
35	[number]	k4	35
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
hloubce	hloubka	k1gFnSc6	hloubka
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
mořskou	mořský	k2eAgFnSc7d1	mořská
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
10	[number]	k4	10
<g/>
×	×	k?	×
méně	málo	k6eAd2	málo
solí	sůl	k1gFnPc2	sůl
(	(	kIx(	(
<g/>
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
chlorid	chlorid	k1gInSc4	chlorid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
(	(	kIx(	(
<g/>
52	[number]	k4	52
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
a	a	k8xC	a
bromid	bromid	k1gInSc1	bromid
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
vysokého	vysoký	k2eAgInSc2d1	vysoký
podílu	podíl	k1gInSc2	podíl
solí	sůl	k1gFnPc2	sůl
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hustota	hustota	k1gFnSc1	hustota
vody	voda	k1gFnSc2	voda
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
vyšší	vysoký	k2eAgFnSc1d2	vyšší
vztlaková	vztlakový	k2eAgFnSc1d1	vztlaková
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c4	na
plovoucí	plovoucí	k2eAgInPc4d1	plovoucí
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
plavat	plavat	k5eAaImF	plavat
–	–	k?	–
stačí	stačit	k5eAaBmIp3nS	stačit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
usednout	usednout	k5eAaPmF	usednout
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
se	se	k3xPyFc4	se
nadnášet	nadnášet	k5eAaImF	nadnášet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
==	==	k?	==
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
"	"	kIx"	"
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
salinitě	salinita	k1gFnSc3	salinita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
životu	život	k1gInSc3	život
vodních	vodní	k2eAgInPc2d1	vodní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
vodní	vodní	k2eAgFnPc1d1	vodní
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
však	však	k9	však
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
archeí	archeí	k6eAd1	archeí
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Halobacteria	Halobacterium	k1gNnPc4	Halobacterium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
přívalu	příval	k1gInSc2	příval
vody	voda	k1gFnSc2	voda
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
hladina	hladina	k1gFnSc1	hladina
soli	sůl	k1gFnSc2	sůl
v	v	k7c6	v
Mrtvém	mrtvý	k2eAgNnSc6d1	mrtvé
moři	moře	k1gNnSc6	moře
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
obvyklých	obvyklý	k2eAgInPc2d1	obvyklý
35	[number]	k4	35
%	%	kIx~	%
na	na	k7c4	na
30	[number]	k4	30
%	%	kIx~	%
nebo	nebo	k8xC	nebo
ještě	ještě	k9	ještě
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
probudí	probudit	k5eAaPmIp3nS	probudit
k	k	k7c3	k
životu	život	k1gInSc3	život
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
deštivých	deštivý	k2eAgFnPc2d1	deštivá
zim	zima	k1gFnPc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
mimořádně	mimořádně	k6eAd1	mimořádně
deštivé	deštivý	k2eAgFnSc6d1	deštivá
zimě	zima	k1gFnSc6	zima
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
normálně	normálně	k6eAd1	normálně
tmavomodré	tmavomodrý	k2eAgNnSc1d1	tmavomodré
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
v	v	k7c6	v
červené	červená	k1gFnSc6	červená
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumníci	výzkumník	k1gMnPc1	výzkumník
z	z	k7c2	z
Hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
univerzity	univerzita	k1gFnSc2	univerzita
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
plné	plný	k2eAgFnSc2d1	plná
řasy	řasa	k1gFnSc2	řasa
zvané	zvaný	k2eAgFnPc4d1	zvaná
Dunaliella	Dunaliello	k1gNnPc4	Dunaliello
<g/>
.	.	kIx.	.
</s>
<s>
Dunaliella	Dunaliella	k1gFnSc1	Dunaliella
se	se	k3xPyFc4	se
živila	živit	k5eAaImAgFnS	živit
karotenem	karoten	k1gInSc7	karoten
obsahujícími	obsahující	k2eAgFnPc7d1	obsahující
halobacteriemi	halobacterie	k1gFnPc7	halobacterie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
změnu	změna	k1gFnSc4	změna
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
řasa	řasa	k1gFnSc1	řasa
se	se	k3xPyFc4	se
již	již	k9	již
v	v	k7c6	v
tak	tak	k6eAd1	tak
hojném	hojný	k2eAgInSc6d1	hojný
počtu	počet	k1gInSc6	počet
neobjevila	objevit	k5eNaPmAgFnS	objevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
obklopujících	obklopující	k2eAgInPc2d1	obklopující
Mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
moře	moře	k1gNnSc4	moře
si	se	k3xPyFc3	se
našlo	najít	k5eAaPmAgNnS	najít
domov	domov	k1gInSc4	domov
mnoho	mnoho	k4c4	mnoho
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
velbloudi	velbloud	k1gMnPc1	velbloud
<g/>
,	,	kIx,	,
kozorožci	kozorožec	k1gMnPc1	kozorožec
<g/>
,	,	kIx,	,
zajíci	zajíc	k1gMnPc1	zajíc
<g/>
,	,	kIx,	,
damani	daman	k1gMnPc1	daman
<g/>
,	,	kIx,	,
šakalové	šakal	k1gMnPc1	šakal
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc1	liška
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
levharti	levhart	k1gMnPc1	levhart
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
rovněž	rovněž	k9	rovněž
stovky	stovka	k1gFnPc1	stovka
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
i	i	k8xC	i
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
jezera	jezero	k1gNnSc2	jezero
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Delta	delta	k1gFnSc1	delta
řeky	řeka	k1gFnSc2	řeka
Jordán	Jordán	k1gInSc1	Jordán
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
džunglí	džungle	k1gFnSc7	džungle
papyrusu	papyrus	k1gInSc2	papyrus
a	a	k8xC	a
palem	palma	k1gFnPc2	palma
<g/>
.	.	kIx.	.
</s>
<s>
Iosephus	Iosephus	k1gMnSc1	Iosephus
Flavius	Flavius	k1gMnSc1	Flavius
popsal	popsat	k5eAaPmAgMnS	popsat
Jericho	Jericho	k1gNnSc4	Jericho
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nejúrodnější	úrodný	k2eAgNnSc1d3	nejúrodnější
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
Judsku	Judsko	k1gNnSc6	Judsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pěstování	pěstování	k1gNnSc3	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
henny	henna	k1gFnSc2	henna
a	a	k8xC	a
fíků	fík	k1gInPc2	fík
bylo	být	k5eAaImAgNnS	být
dolní	dolní	k2eAgNnSc1d1	dolní
údolí	údolí	k1gNnSc1	údolí
Jordánu	Jordán	k1gInSc2	Jordán
v	v	k7c6	v
římských	římský	k2eAgFnPc6d1	římská
a	a	k8xC	a
byzantských	byzantský	k2eAgFnPc6d1	byzantská
dobách	doba	k1gFnPc6	doba
bohatou	bohatý	k2eAgFnSc7d1	bohatá
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejcennějších	cenný	k2eAgInPc2d3	nejcennější
produktů	produkt	k1gInPc2	produkt
v	v	k7c6	v
Jerichu	Jericho	k1gNnSc6	Jericho
byla	být	k5eAaImAgFnS	být
míza	míza	k1gFnSc1	míza
z	z	k7c2	z
myrhovníku	myrhovník	k1gInSc2	myrhovník
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
parfémy	parfém	k1gInPc1	parfém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
М	М	k?	М
м	м	k?	м
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČEŘOVSKÝ	ČEŘOVSKÝ	kA	ČEŘOVSKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
palestinská	palestinský	k2eAgNnPc4d1	palestinské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
192	[number]	k4	192
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7033	[number]	k4	7033
<g/>
-	-	kIx~	-
<g/>
541	[number]	k4	541
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IZRAELSKÉ	izraelský	k2eAgNnSc1d1	izraelské
INFORMAČNÍ	informační	k2eAgNnSc1d1	informační
STŘEDISKO	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Fakta	faktum	k1gNnPc1	faktum
o	o	k7c6	o
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
:	:	kIx,	:
země	země	k1gFnSc1	země
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
:	:	kIx,	:
Ahva	Ahva	k1gFnSc1	Ahva
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
28	[number]	k4	28
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PAULÍK	PAULÍK	kA	PAULÍK
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Freytag	Freytag	k1gMnSc1	Freytag
&	&	k?	&
Berndt	Berndt	k1gMnSc1	Berndt
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7316	[number]	k4	7316
<g/>
-	-	kIx~	-
<g/>
202	[number]	k4	202
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Svitky	svitek	k1gInPc1	svitek
od	od	k7c2	od
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
Marianský	Marianský	k2eAgInSc1d1	Marianský
příkop	příkop	k1gInSc1	příkop
–	–	k?	–
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
povrchu	povrch	k1gInSc2	povrch
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
(	(	kIx(	(
<g/>
podmořská	podmořský	k2eAgFnSc1d1	podmořská
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
střetu	střet	k1gInSc2	střet
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
západního	západní	k2eAgInSc2d1	západní
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vostok	Vostok	k1gInSc1	Vostok
(	(	kIx(	(
<g/>
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Antarktidy	Antarktida	k1gFnSc2	Antarktida
jsou	být	k5eAaImIp3nP	být
sněhem	sníh	k1gInSc7	sníh
zaplněné	zaplněný	k2eAgFnPc1d1	zaplněná
prolákliny	proláklina	k1gFnPc1	proláklina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
položeny	položit	k5eAaPmNgFnP	položit
mnohem	mnohem	k6eAd1	mnohem
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
Mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
moře	moře	k1gNnSc4	moře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
American	American	k1gInSc1	American
University	universita	k1gFnSc2	universita
–	–	k?	–
Dead	Dead	k1gMnSc1	Dead
Sea	Sea	k1gMnSc1	Sea
Kanal	Kanal	k1gMnSc1	Kanal
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Monitoring	monitoring	k1gInSc4	monitoring
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fotografie	fotografia	k1gFnPc1	fotografia
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
s	s	k7c7	s
popisem	popis	k1gInSc7	popis
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fotografie	fotografia	k1gFnPc1	fotografia
odsolovacích	odsolovací	k2eAgFnPc2d1	odsolovací
nádrží	nádrž	k1gFnPc2	nádrž
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Satelitní	satelitní	k2eAgFnSc1d1	satelitní
fotografie	fotografie	k1gFnSc1	fotografie
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Eretz	Eretz	k1gInSc1	Eretz
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
moře	moře	k1gNnSc4	moře
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
soutěže	soutěž	k1gFnSc2	soutěž
o	o	k7c4	o
7	[number]	k4	7
přírodních	přírodní	k2eAgInPc2d1	přírodní
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Eretz	Eretz	k1gInSc1	Eretz
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
propojení	propojení	k1gNnSc6	propojení
s	s	k7c7	s
Rudým	rudý	k2eAgNnSc7d1	Rudé
mořem	moře	k1gNnSc7	moře
</s>
</p>
