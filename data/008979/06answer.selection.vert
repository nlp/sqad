<s>
Švédština	švédština	k1gFnSc1	švédština
(	(	kIx(	(
svenska	svensko	k1gNnSc2	svensko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
okolo	okolo	k7c2	okolo
10,5	[number]	k4	10,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
především	především	k6eAd1	především
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
části	část	k1gFnSc6	část
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
na	na	k7c6	na
Å	Å	k1gInPc6	Å
<g/>
.	.	kIx.	.
</s>
