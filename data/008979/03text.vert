<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
(	(	kIx(	(
svenska	svensko	k1gNnSc2	svensko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
okolo	okolo	k7c2	okolo
10,5	[number]	k4	10,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
především	především	k6eAd1	především
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
části	část	k1gFnSc6	část
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
na	na	k7c6	na
Å	Å	k1gInPc6	Å
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzájemně	vzájemně	k6eAd1	vzájemně
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
s	s	k7c7	s
norštinou	norština	k1gFnSc7	norština
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
též	též	k9	též
s	s	k7c7	s
dánštinou	dánština	k1gFnSc7	dánština
<g/>
.	.	kIx.	.
<g/>
Základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
švédštiny	švédština	k1gFnSc2	švédština
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
stará	starat	k5eAaImIp3nS	starat
severština	severština	k1gFnSc1	severština
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
švédština	švédština	k1gFnSc1	švédština
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
z	z	k7c2	z
nářečí	nářečí	k1gNnSc2	nářečí
středního	střední	k2eAgNnSc2d1	střední
Švédska	Švédsko	k1gNnSc2	Švédsko
(	(	kIx(	(
<g/>
Svealandu	Svealand	k1gInSc2	Svealand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
venkovských	venkovský	k2eAgNnPc2d1	venkovské
nářečí	nářečí	k1gNnPc2	nářečí
se	se	k3xPyFc4	se
mnohá	mnohý	k2eAgFnSc1d1	mnohá
dochovala	dochovat	k5eAaPmAgFnS	dochovat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mluvená	mluvený	k2eAgFnSc1d1	mluvená
a	a	k8xC	a
psaná	psaný	k2eAgFnSc1d1	psaná
švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
jednotná	jednotný	k2eAgFnSc1d1	jednotná
a	a	k8xC	a
standardizovaná	standardizovaný	k2eAgFnSc1d1	standardizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
nářečí	nářečí	k1gNnPc1	nářečí
se	se	k3xPyFc4	se
od	od	k7c2	od
standardní	standardní	k2eAgFnSc2d1	standardní
švédštiny	švédština	k1gFnSc2	švédština
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nS	lišit
jak	jak	k8xC	jak
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
slovní	slovní	k2eAgFnSc7d1	slovní
zásobou	zásoba	k1gFnSc7	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
nářečí	nářečí	k1gNnPc1	nářečí
jsou	být	k5eAaImIp3nP	být
omezena	omezit	k5eAaPmNgNnP	omezit
na	na	k7c6	na
venkovské	venkovský	k2eAgFnSc6d1	venkovská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jimi	on	k3xPp3gFnPc7	on
mluví	mluvit	k5eAaImIp3nS	mluvit
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
sociální	sociální	k2eAgFnSc7d1	sociální
mobilitou	mobilita	k1gFnSc7	mobilita
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
nečelí	čelit	k5eNaImIp3nS	čelit
bezprostřednímu	bezprostřední	k2eAgInSc3d1	bezprostřední
zániku	zánik	k1gInSc3	zánik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
posledního	poslední	k2eAgNnSc2d1	poslední
století	století	k1gNnSc2	století
upadala	upadat	k5eAaImAgFnS	upadat
navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
prozkoumána	prozkoumán	k2eAgNnPc4d1	prozkoumáno
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
používání	používání	k1gNnSc2	používání
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
podporováno	podporovat	k5eAaImNgNnS	podporovat
místními	místní	k2eAgInPc7d1	místní
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
švédštiny	švédština	k1gFnSc2	švédština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
latinka	latinka	k1gFnSc1	latinka
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c4	o
znaky	znak	k1gInPc4	znak
Å	Å	k?	Å
<g/>
,	,	kIx,	,
Ä	Ä	kA	Ä
a	a	k8xC	a
Ö.	Ö.	k1gFnSc1	Ö.
Mluvnice	mluvnice	k1gFnSc2	mluvnice
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
původní	původní	k2eAgFnSc3d1	původní
staré	starý	k2eAgFnSc3d1	stará
severštině	severština	k1gFnSc3	severština
značně	značně	k6eAd1	značně
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
švédština	švédština	k1gFnSc1	švédština
má	mít	k5eAaImIp3nS	mít
redukovanou	redukovaný	k2eAgFnSc4d1	redukovaná
flexi	flexe	k1gFnSc4	flexe
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jí	on	k3xPp3gFnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
převažující	převažující	k2eAgNnSc1d1	převažující
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Švédština	švédština	k1gFnSc1	švédština
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
jmenné	jmenný	k2eAgInPc4d1	jmenný
rody	rod	k1gInPc4	rod
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc4d1	společný
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgMnSc1d1	určitý
člen	člen	k1gMnSc1	člen
je	být	k5eAaImIp3nS	být
postpozitivní	postpozitivní	k2eAgMnSc1d1	postpozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Slovosled	slovosled	k1gInSc1	slovosled
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
typu	typ	k1gInSc2	typ
SVO	SVO	kA	SVO
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výslovnost	výslovnost	k1gFnSc4	výslovnost
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
melodický	melodický	k2eAgInSc1d1	melodický
přízvuk	přízvuk	k1gInSc1	přízvuk
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
dvojí	dvojí	k4xRgNnSc4	dvojí
intonací	intonace	k1gFnPc2	intonace
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
souhlásky	souhláska	k1gFnPc1	souhláska
(	(	kIx(	(
<g/>
v	v	k7c6	v
přízvučných	přízvučný	k2eAgFnPc6d1	přízvučná
slabikách	slabika	k1gFnPc6	slabika
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
komplementarity	komplementarita	k1gFnSc2	komplementarita
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
dosti	dosti	k6eAd1	dosti
specifická	specifický	k2eAgFnSc1d1	specifická
výslovnost	výslovnost	k1gFnSc1	výslovnost
hlásek	hláska	k1gFnPc2	hláska
"	"	kIx"	"
<g/>
u	u	k7c2	u
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
sje	sje	k?	sje
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
jazykem	jazyk	k1gInSc7	jazyk
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
indoevropský	indoevropský	k2eAgInSc4d1	indoevropský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
severogermánské	severogermánský	k2eAgFnSc2d1	severogermánský
větve	větev	k1gFnSc2	větev
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dánštinou	dánština	k1gFnSc7	dánština
a	a	k8xC	a
bokmå	bokmå	k?	bokmå
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
norštiny	norština	k1gFnSc2	norština
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
východoskandinávské	východoskandinávský	k2eAgInPc4d1	východoskandinávský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
norština	norština	k1gFnSc1	norština
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
varianta	varianta	k1gFnSc1	varianta
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
nynorsk	nynorsk	k1gInSc1	nynorsk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
faerština	faerština	k1gFnSc1	faerština
a	a	k8xC	a
islandština	islandština	k1gFnSc1	islandština
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
západoskandinávské	západoskandinávský	k2eAgNnSc4d1	západoskandinávský
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
fonologických	fonologický	k2eAgInPc2d1	fonologický
rozdílů	rozdíl	k1gInPc2	rozdíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
základ	základ	k1gInSc4	základ
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nářečního	nářeční	k2eAgNnSc2d1	nářeční
štěpení	štěpení	k1gNnSc2	štěpení
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
rozdělení	rozdělení	k1gNnSc1	rozdělení
řadí	řadit	k5eAaImIp3nS	řadit
švédštinu	švédština	k1gFnSc4	švédština
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dánštinou	dánština	k1gFnSc7	dánština
a	a	k8xC	a
norštinou	norština	k1gFnSc7	norština
mezi	mezi	k7c4	mezi
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
skandinávské	skandinávský	k2eAgInPc4d1	skandinávský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
charakter	charakter	k1gInSc1	charakter
analytických	analytický	k2eAgInPc2d1	analytický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovní	ostrovní	k2eAgInPc1d1	ostrovní
skandinávské	skandinávský	k2eAgInPc1d1	skandinávský
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
faerština	faerština	k1gFnSc1	faerština
<g/>
,	,	kIx,	,
islandština	islandština	k1gFnSc1	islandština
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
uchovaly	uchovat	k5eAaPmAgFnP	uchovat
charakter	charakter	k1gInSc4	charakter
flektivních	flektivní	k2eAgInPc2d1	flektivní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
(	(	kIx(	(
<g/>
fornnordiska	fornnordiska	k1gFnSc1	fornnordiska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	let	k1gInPc6	let
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
na	na	k7c4	na
západní	západní	k2eAgNnSc4d1	západní
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
a	a	k8xC	a
východní	východní	k2eAgNnSc1d1	východní
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
odlišovat	odlišovat	k5eAaImF	odlišovat
dánská	dánský	k2eAgNnPc4d1	dánské
a	a	k8xC	a
švédská	švédský	k2eAgNnPc4d1	švédské
nářečí	nářečí	k1gNnPc4	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
staré	starý	k2eAgFnSc6d1	stará
dánštině	dánština	k1gFnSc6	dánština
a	a	k8xC	a
staré	starý	k2eAgFnSc6d1	stará
švédštině	švédština	k1gFnSc6	švédština
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
řeči	řeč	k1gFnPc1	řeč
silně	silně	k6eAd1	silně
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
dolnoněmčinou	dolnoněmčina	k1gFnSc7	dolnoněmčina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
švédštiny	švédština	k1gFnSc2	švédština
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
do	do	k7c2	do
následujících	následující	k2eAgNnPc2d1	následující
období	období	k1gNnPc2	období
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
runová	runový	k2eAgFnSc1d1	runová
švédština	švédština	k1gFnSc1	švédština
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
1225	[number]	k4	1225
</s>
</p>
<p>
<s>
stará	starý	k2eAgFnSc1d1	stará
švédština	švédština	k1gFnSc1	švédština
</s>
</p>
<p>
<s>
klasická	klasický	k2eAgFnSc1d1	klasická
1225	[number]	k4	1225
<g/>
–	–	k?	–
<g/>
1375	[number]	k4	1375
</s>
</p>
<p>
<s>
pozdní	pozdní	k2eAgMnSc1d1	pozdní
1375	[number]	k4	1375
<g/>
–	–	k?	–
<g/>
1526	[number]	k4	1526
</s>
</p>
<p>
<s>
nová	nový	k2eAgFnSc1d1	nová
švédština	švédština	k1gFnSc1	švédština
</s>
</p>
<p>
<s>
raná	raný	k2eAgFnSc1d1	raná
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1732	[number]	k4	1732
</s>
</p>
<p>
<s>
pozdní	pozdní	k2eAgMnSc1d1	pozdní
1732	[number]	k4	1732
<g/>
–	–	k?	–
<g/>
souč	souč	k1gFnSc1	souč
<g/>
.	.	kIx.	.
<g/>
Východní	východní	k2eAgFnSc1d1	východní
stará	starý	k2eAgFnSc1d1	stará
severština	severština	k1gFnSc1	severština
se	se	k3xPyFc4	se
od	od	k7c2	od
západní	západní	k2eAgFnSc2d1	západní
odlišila	odlišit	k5eAaPmAgFnS	odlišit
rozdílným	rozdílný	k2eAgInSc7d1	rozdílný
vývojem	vývoj	k1gInSc7	vývoj
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
a	a	k8xC	a
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
severština	severština	k1gFnSc1	severština
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
zapisovala	zapisovat	k5eAaImAgFnS	zapisovat
runovým	runový	k2eAgNnSc7d1	runové
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
o	o	k7c6	o
runové	runový	k2eAgFnSc6d1	runová
švédštině	švédština	k1gFnSc6	švédština
(	(	kIx(	(
<g/>
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
o	o	k7c6	o
runové	runový	k2eAgFnSc6d1	runová
dánštině	dánština	k1gFnSc6	dánština
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
však	však	k9	však
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
stejné	stejný	k2eAgNnSc4d1	stejné
nářečí	nářečí	k1gNnSc4	nářečí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dánská	dánský	k2eAgNnPc1d1	dánské
a	a	k8xC	a
švédská	švédský	k2eAgNnPc1d1	švédské
nářečí	nářečí	k1gNnPc1	nářečí
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
lišit	lišit	k5eAaImF	lišit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1100	[number]	k4	1100
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
staré	starý	k2eAgFnSc6d1	stará
švédštině	švédština	k1gFnSc6	švédština
(	(	kIx(	(
<g/>
fornsvenska	fornsvensko	k1gNnSc2	fornsvensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
památkou	památka	k1gFnSc7	památka
psanou	psaný	k2eAgFnSc7d1	psaná
latinkou	latinka	k1gFnSc7	latinka
je	být	k5eAaImIp3nS	být
zákoník	zákoník	k1gInSc1	zákoník
Västgötalagen	Västgötalagen	k1gInSc1	Västgötalagen
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
fragmenty	fragment	k1gInPc1	fragment
datované	datovaný	k2eAgInPc1d1	datovaný
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
písemnictví	písemnictví	k1gNnSc2	písemnictví
měla	mít	k5eAaImAgFnS	mít
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
klášterní	klášterní	k2eAgInPc1d1	klášterní
řády	řád	k1gInPc1	řád
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
tištěná	tištěný	k2eAgFnSc1d1	tištěná
kniha	kniha	k1gFnSc1	kniha
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1495	[number]	k4	1495
<g/>
.	.	kIx.	.
</s>
<s>
Skloňování	skloňování	k1gNnSc1	skloňování
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
staré	starý	k2eAgFnSc6d1	stará
švédštině	švédština	k1gFnSc6	švédština
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
systémem	systém	k1gInSc7	systém
4	[number]	k4	4
pádů	pád	k1gInPc2	pád
a	a	k8xC	a
3	[number]	k4	3
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
časování	časování	k1gNnPc2	časování
sloves	sloveso	k1gNnPc2	sloveso
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k6eAd1	rovněž
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Gramatiku	gramatik	k1gMnSc5	gramatik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
švédština	švédština	k1gFnSc1	švédština
(	(	kIx(	(
<g/>
nysvenska	nysvensko	k1gNnSc2	nysvensko
<g/>
)	)	kIx)	)
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
vynálezem	vynález	k1gInSc7	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
a	a	k8xC	a
s	s	k7c7	s
evropskou	evropský	k2eAgFnSc7d1	Evropská
reformací	reformace	k1gFnSc7	reformace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
Gustav	Gustav	k1gMnSc1	Gustav
I.	I.	kA	I.
Vasa	Vasa	k1gMnSc1	Vasa
objednal	objednat	k5eAaPmAgMnS	objednat
švédský	švédský	k2eAgInSc4d1	švédský
překlad	překlad	k1gInSc4	překlad
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Bible	bible	k1gFnSc1	bible
Gustava	Gustav	k1gMnSc2	Gustav
Vasy	Vasa	k1gMnSc2	Vasa
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
standard	standard	k1gInSc4	standard
spisovné	spisovný	k2eAgFnSc2d1	spisovná
švédštiny	švédština	k1gFnSc2	švédština
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgNnP	být
použita	použit	k2eAgNnPc1d1	použito
písmena	písmeno	k1gNnPc1	písmeno
"	"	kIx"	"
<g/>
å	å	k?	å
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ä	ä	k?	ä
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ö	ö	k?	ö
<g/>
"	"	kIx"	"
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
"	"	kIx"	"
<g/>
ck	ck	k?	ck
<g/>
"	"	kIx"	"
namísto	namísto	k7c2	namísto
"	"	kIx"	"
<g/>
kk	kk	k?	kk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
jasně	jasně	k6eAd1	jasně
odlišila	odlišit	k5eAaPmAgFnS	odlišit
od	od	k7c2	od
dánské	dánský	k2eAgFnSc2d1	dánská
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
švédštinu	švédština	k1gFnSc4	švédština
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
lingvistické	lingvistický	k2eAgFnSc6d1	lingvistická
terminologii	terminologie	k1gFnSc6	terminologie
nazývá	nazývat	k5eAaImIp3nS	nazývat
nusvenska	nusvensko	k1gNnPc5	nusvensko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
švédského	švédský	k2eAgInSc2d1	švédský
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
měst	město	k1gNnPc2	město
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zanechala	zanechat	k5eAaPmAgFnS	zanechat
svou	svůj	k3xOyFgFnSc4	svůj
stopu	stopa	k1gFnSc4	stopa
ve	v	k7c6	v
švédské	švédský	k2eAgFnSc6d1	švédská
literatuře	literatura	k1gFnSc6	literatura
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
nový	nový	k2eAgInSc4d1	nový
národní	národní	k2eAgInSc4d1	národní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
měl	mít	k5eAaImAgMnS	mít
August	August	k1gMnSc1	August
Strindberg	Strindberg	k1gMnSc1	Strindberg
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
Pravidel	pravidlo	k1gNnPc2	pravidlo
švédského	švédský	k2eAgInSc2d1	švédský
pravopisu	pravopis	k1gInSc2	pravopis
(	(	kIx(	(
<g/>
Ordlista	Ordlista	k1gMnSc1	Ordlista
öfver	öfver	k1gMnSc1	öfver
svenska	svensko	k1gNnSc2	svensko
språ	språ	k?	språ
<g/>
)	)	kIx)	)
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgFnSc4d1	radikální
reformu	reforma	k1gFnSc4	reforma
pravopisu	pravopis	k1gInSc2	pravopis
prosadil	prosadit	k5eAaPmAgInS	prosadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
Fridtjuv	Fridtjuv	k1gMnSc1	Fridtjuv
Berg	Berg	k1gMnSc1	Berg
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změna	změna	k1gFnSc1	změna
v	v	k7c6	v
oslovování	oslovování	k1gNnSc6	oslovování
===	===	k?	===
</s>
</p>
<p>
<s>
Významná	významný	k2eAgFnSc1d1	významná
změna	změna	k1gFnSc1	změna
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
du-reformy	dueforma	k1gFnSc2	du-reforma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
správný	správný	k2eAgInSc1d1	správný
způsob	způsob	k1gInSc1	způsob
oslovování	oslovování	k1gNnSc2	oslovování
lidí	člověk	k1gMnPc2	člověk
stejné	stejný	k2eAgFnSc2d1	stejná
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc2d2	vyšší
sociální	sociální	k2eAgFnSc2d1	sociální
skupiny	skupina	k1gFnSc2	skupina
byl	být	k5eAaImAgInS	být
pomocí	pomocí	k7c2	pomocí
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
příjmení	příjmení	k1gNnSc2	příjmení
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
oslovení	oslovení	k1gNnSc1	oslovení
"	"	kIx"	"
<g/>
herr	herr	k1gInSc1	herr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pane	pan	k1gMnSc5	pan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
fru	fru	k?	fru
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
paní	paní	k1gFnSc1	paní
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
fröken	fröken	k2eAgMnSc1d1	fröken
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
slečno	slečna	k1gFnSc5	slečna
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
jen	jen	k9	jen
pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
rozhovoru	rozhovor	k1gInSc2	rozhovor
s	s	k7c7	s
cizími	cizí	k2eAgMnPc7d1	cizí
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
člověk	člověk	k1gMnSc1	člověk
neznal	neznat	k5eAaImAgMnS	neznat
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
ani	ani	k8xC	ani
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
oslovovaný	oslovovaný	k2eAgMnSc1d1	oslovovaný
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
oslovován	oslovovat	k5eAaImNgMnS	oslovovat
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
osobě	osoba	k1gFnSc6	osoba
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
komplikování	komplikování	k1gNnSc3	komplikování
komunikace	komunikace	k1gFnSc2	komunikace
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
učiněn	učiněn	k2eAgInSc4d1	učiněn
neúspěšný	úspěšný	k2eNgInSc4d1	neúspěšný
pokus	pokus	k1gInSc4	pokus
nahradit	nahradit	k5eAaPmF	nahradit
neúprosné	úprosný	k2eNgNnSc4d1	neúprosné
trvání	trvání	k1gNnSc4	trvání
na	na	k7c6	na
titulech	titul	k1gInPc6	titul
vykáním	vykání	k1gNnSc7	vykání
–	–	k?	–
tedy	tedy	k8xC	tedy
používáním	používání	k1gNnSc7	používání
zájmena	zájmeno	k1gNnSc2	zájmeno
"	"	kIx"	"
<g/>
ni	on	k3xPp3gFnSc4	on
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vy	vy	k3xPp2nPc1	vy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
částečně	částečně	k6eAd1	částečně
ujalo	ujmout	k5eAaPmAgNnS	ujmout
jako	jako	k9	jako
méně	málo	k6eAd2	málo
arogantní	arogantní	k2eAgNnSc4d1	arogantní
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
du	du	k?	du
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
sloužilo	sloužit	k5eAaImAgNnS	sloužit
k	k	k7c3	k
oslovení	oslovení	k1gNnSc3	oslovení
lidí	člověk	k1gMnPc2	člověk
nižší	nízký	k2eAgFnSc1d2	nižší
společenské	společenský	k2eAgFnPc4d1	společenská
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
liberalizací	liberalizace	k1gFnSc7	liberalizace
a	a	k8xC	a
radikalizací	radikalizace	k1gFnSc7	radikalizace
švédské	švédský	k2eAgFnSc2d1	švédská
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
společenskými	společenský	k2eAgFnPc7d1	společenská
třídami	třída	k1gFnPc7	třída
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
významu	význam	k1gInSc6	význam
a	a	k8xC	a
oslovení	oslovení	k1gNnSc6	oslovení
"	"	kIx"	"
<g/>
du	du	k?	du
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tykání	tykání	k1gNnSc1	tykání
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stalo	stát	k5eAaPmAgNnS	stát
standardem	standard	k1gInSc7	standard
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
ve	v	k7c6	v
formálním	formální	k2eAgInSc6d1	formální
a	a	k8xC	a
oficiálním	oficiální	k2eAgInSc6d1	oficiální
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Vykání	vykání	k1gNnSc1	vykání
(	(	kIx(	(
<g/>
oslovení	oslovení	k1gNnSc1	oslovení
"	"	kIx"	"
<g/>
ni	on	k3xPp3gFnSc4	on
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mezi	mezi	k7c4	mezi
Švédy	Švéd	k1gMnPc4	Švéd
běžné	běžný	k2eAgMnPc4d1	běžný
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
např.	např.	kA	např.
někdy	někdy	k6eAd1	někdy
vykají	vykat	k5eAaImIp3nP	vykat
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
vykání	vykání	k1gNnSc1	vykání
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
o	o	k7c4	o
něco	něco	k3yInSc4	něco
častější	častý	k2eAgNnSc1d2	častější
než	než	k8xS	než
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
tykání	tykání	k1gNnSc1	tykání
však	však	k9	však
rovněž	rovněž	k9	rovněž
převládá	převládat	k5eAaImIp3nS	převládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
švédštiny	švédština	k1gFnSc2	švédština
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
germánská	germánský	k2eAgFnSc1d1	germánská
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
původem	původ	k1gInSc7	původ
germánský	germánský	k2eAgInSc4d1	germánský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přejímáním	přejímání	k1gNnSc7	přejímání
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
rozsahu	rozsah	k1gInSc6	rozsah
i	i	k9	i
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
germánských	germánský	k2eAgNnPc2d1	germánské
slov	slovo	k1gNnPc2	slovo
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
mus	musa	k1gFnPc2	musa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
myš	myš	k1gFnSc1	myš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kung	kung	k1gMnSc1	kung
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
gå	gå	k?	gå
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
husa	husa	k1gFnSc1	husa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
původu	původ	k1gInSc2	původ
latinského	latinský	k2eAgInSc2d1	latinský
nebo	nebo	k8xC	nebo
řeckého	řecký	k2eAgInSc2d1	řecký
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přejatých	přejatý	k2eAgFnPc2d1	přejatá
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Přejímání	přejímání	k1gNnSc1	přejímání
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jiných	jiný	k2eAgInPc2d1	jiný
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
též	též	k9	též
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
z	z	k7c2	z
dolnoněmčiny	dolnoněmčina	k1gFnSc2	dolnoněmčina
<g/>
,	,	kIx,	,
lingua	lingu	k2eAgFnSc1d1	lingua
franca	franca	k1gFnSc1	franca
Hanzovní	hanzovní	k2eAgFnSc2d1	hanzovní
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
hornoněmčiny	hornoněmčina	k1gFnSc2	hornoněmčina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
složeniny	složenina	k1gFnPc1	složenina
jsou	být	k5eAaImIp3nP	být
doslovnými	doslovný	k2eAgInPc7d1	doslovný
překlady	překlad	k1gInPc7	překlad
(	(	kIx(	(
<g/>
kalky	kalk	k1gInPc7	kalk
<g/>
)	)	kIx)	)
původně	původně	k6eAd1	původně
německých	německý	k2eAgFnPc2d1	německá
složenin	složenina	k1gFnPc2	složenina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
bomull	bomull	k1gMnSc1	bomull
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
)	)	kIx)	)
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
"	"	kIx"	"
<g/>
Baumwolle	Baumwolle	k1gNnSc2	Baumwolle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Finská	finský	k2eAgFnSc1d1	finská
švédština	švédština	k1gFnSc1	švédština
má	mít	k5eAaImIp3nS	mít
sadu	sada	k1gFnSc4	sada
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
termínů	termín	k1gInPc2	termín
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doslovných	doslovný	k2eAgInPc2d1	doslovný
překladů	překlad	k1gInPc2	překlad
finských	finský	k2eAgInPc2d1	finský
protějšků	protějšek	k1gInPc2	protějšek
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pojmů	pojem	k1gInPc2	pojem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významné	významný	k2eAgNnSc1d1	významné
množství	množství	k1gNnSc1	množství
francouzských	francouzský	k2eAgNnPc2d1	francouzské
slov	slovo	k1gNnPc2	slovo
bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
byla	být	k5eAaImAgNnP	být
přepisována	přepisovat	k5eAaImNgNnP	přepisovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
švédskými	švédský	k2eAgFnPc7d1	švédská
pravopisnými	pravopisný	k2eAgFnPc7d1	pravopisná
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
srozumitelnost	srozumitelnost	k1gFnSc4	srozumitelnost
pro	pro	k7c4	pro
francouzské	francouzský	k2eAgMnPc4d1	francouzský
mluvčí	mluvčí	k1gMnPc4	mluvčí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
nivå	nivå	k?	nivå
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
"	"	kIx"	"
<g/>
niveau	niveau	k1gNnSc1	niveau
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ateljé	ateljé	k6eAd1	ateljé
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
"	"	kIx"	"
<g/>
atelier	atelier	k1gNnSc1	atelier
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
paraply	parapnout	k5eAaPmAgInP	parapnout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
"	"	kIx"	"
<g/>
parapluie	parapluie	k1gFnSc1	parapluie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
deštník	deštník	k1gInSc1	deštník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
svých	svůj	k3xOyFgMnPc2	svůj
skandinávských	skandinávský	k2eAgMnPc2d1	skandinávský
sousedů	soused	k1gMnPc2	soused
švédština	švédština	k1gFnSc1	švédština
mnoho	mnoho	k4c4	mnoho
slov	slovo	k1gNnPc2	slovo
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
norštiny	norština	k1gFnSc2	norština
např.	např.	kA	např.
pochází	pocházet	k5eAaImIp3nS	pocházet
"	"	kIx"	"
<g/>
slalom	slalom	k1gInSc1	slalom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
dánštiny	dánština	k1gFnSc2	dánština
"	"	kIx"	"
<g/>
bagare	bagar	k1gMnSc5	bagar
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dán	dát	k5eAaPmNgInS	dát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
bager	bager	k1gInSc1	bager
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pekař	pekař	k1gMnSc1	pekař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
hänsyn	hänsyn	k1gMnSc1	hänsyn
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dán	dát	k5eAaPmNgInS	dát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
hensyn	hensyn	k1gInSc1	hensyn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ohled	ohled	k1gInSc1	ohled
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zkratkové	zkratkový	k2eAgNnSc1d1	zkratkové
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
bil	bít	k5eAaImAgInS	bít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
auto	auto	k1gNnSc1	auto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
islandštiny	islandština	k1gFnSc2	islandština
pochází	pocházet	k5eAaImIp3nS	pocházet
"	"	kIx"	"
<g/>
idrott	idrott	k5eAaPmF	idrott
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
sport	sport	k1gInSc1	sport
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
finštiny	finština	k1gFnSc2	finština
pochází	pocházet	k5eAaImIp3nS	pocházet
např.	např.	kA	např.
"	"	kIx"	"
<g/>
pojke	pojk	k1gMnSc2	pojk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
fin.	fin.	k?	fin.
"	"	kIx"	"
<g/>
poika	poika	k1gMnSc1	poika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přejímání	přejímání	k1gNnSc1	přejímání
mezi	mezi	k7c7	mezi
češtinou	čeština	k1gFnSc7	čeština
a	a	k8xC	a
švédštinou	švédština	k1gFnSc7	švédština
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
geografických	geografický	k2eAgInPc2d1	geografický
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
důvodů	důvod	k1gInPc2	důvod
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
pronikla	proniknout	k5eAaPmAgNnP	proniknout
některá	některý	k3yIgNnPc1	některý
švédská	švédský	k2eAgNnPc1d1	švédské
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
skansen	skansen	k1gInSc1	skansen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ombudsman	ombudsman	k1gMnSc1	ombudsman
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
švédštiny	švédština	k1gFnSc2	švédština
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
i	i	k9	i
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
proniklo	proniknout	k5eAaPmAgNnS	proniknout
české	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
robot	robot	k1gInSc1	robot
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byly	být	k5eAaImAgFnP	být
i	i	k9	i
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
snahy	snaha	k1gFnSc2	snaha
zbavit	zbavit	k5eAaPmF	zbavit
jazyk	jazyk	k1gInSc4	jazyk
záplavy	záplava	k1gFnSc2	záplava
cizích	cizí	k2eAgFnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
představitelem	představitel	k1gMnSc7	představitel
puristů	purista	k1gMnPc2	purista
byl	být	k5eAaImAgMnS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
Viktor	Viktor	k1gMnSc1	Viktor
Rydberg	Rydberg	k1gMnSc1	Rydberg
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
sám	sám	k3xTgInSc1	sám
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mnohá	mnohé	k1gNnPc1	mnohé
ujala	ujmout	k5eAaPmAgNnP	ujmout
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
slöjd	slöjd	k1gMnSc1	slöjd
<g/>
"	"	kIx"	"
vedle	vedle	k7c2	vedle
"	"	kIx"	"
<g/>
industri	industri	k1gNnSc2	industri
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
průmysl	průmysl	k1gInSc1	průmysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
flertal	flertat	k5eAaPmAgMnS	flertat
<g/>
"	"	kIx"	"
vedle	vedle	k7c2	vedle
"	"	kIx"	"
<g/>
majoritet	majoriteta	k1gFnPc2	majoriteta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kynne	kynnout	k5eAaPmIp3nS	kynnout
<g/>
"	"	kIx"	"
vedle	vedle	k7c2	vedle
"	"	kIx"	"
<g/>
karaktär	karaktära	k1gFnPc2	karaktära
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
povaha	povaha	k1gFnSc1	povaha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vrå	vrå	k?	vrå
<g/>
"	"	kIx"	"
vedle	vedle	k7c2	vedle
"	"	kIx"	"
<g/>
karykatyr	karykatyra	k1gFnPc2	karykatyra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
karikatura	karikatura	k1gFnSc1	karikatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
švédské	švédský	k2eAgFnPc1d1	švédská
písemné	písemný	k2eAgFnPc1d1	písemná
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
runové	runový	k2eAgInPc4d1	runový
nápisy	nápis	k1gInPc4	nápis
pocházející	pocházející	k2eAgNnSc1d1	pocházející
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svébytným	svébytný	k2eAgInSc7d1	svébytný
druhem	druh	k1gInSc7	druh
umění	umění	k1gNnSc2	umění
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byly	být	k5eAaImAgFnP	být
taneční	taneční	k2eAgFnPc1d1	taneční
balady	balada	k1gFnPc1	balada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
podání	podání	k1gNnSc6	podání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
velmocenským	velmocenský	k2eAgInSc7d1	velmocenský
rozmachem	rozmach	k1gInSc7	rozmach
Švédska	Švédsko	k1gNnSc2	Švédsko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
kulturnímu	kulturní	k2eAgInSc3d1	kulturní
rozkvětu	rozkvět	k1gInSc3	rozkvět
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
silné	silný	k2eAgNnSc4d1	silné
národní	národní	k2eAgNnSc4d1	národní
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
romantičtí	romantický	k2eAgMnPc1d1	romantický
básníci	básník	k1gMnPc1	básník
sdružení	sdružení	k1gNnSc2	sdružení
v	v	k7c6	v
Gótském	gótský	k2eAgInSc6d1	gótský
spolku	spolek	k1gInSc6	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
realismus	realismus	k1gInSc1	realismus
(	(	kIx(	(
<g/>
C.	C.	kA	C.
J.	J.	kA	J.
L.	L.	kA	L.
Almqvist	Almqvist	k1gMnSc1	Almqvist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
kritický	kritický	k2eAgInSc1d1	kritický
realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
A.	A.	kA	A.
Strindberg	Strindberg	k1gMnSc1	Strindberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přichází	přicházet	k5eAaImIp3nS	přicházet
novoromantismus	novoromantismus	k1gInSc1	novoromantismus
(	(	kIx(	(
<g/>
S.	S.	kA	S.
Lagerlöfová	Lagerlöfový	k2eAgFnSc1d1	Lagerlöfová
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
C.	C.	kA	C.
G.	G.	kA	G.
Verner	Verner	k1gMnSc1	Verner
von	von	k1gInSc1	von
Heidenstam	Heidenstam	k1gInSc1	Heidenstam
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
E.	E.	kA	E.
A.	A.	kA	A.
Karlfeldt	Karlfeldt	k1gMnSc1	Karlfeldt
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
dominuje	dominovat	k5eAaImIp3nS	dominovat
modernistická	modernistický	k2eAgFnSc1d1	modernistická
lyrika	lyrika	k1gFnSc1	lyrika
(	(	kIx(	(
<g/>
P.	P.	kA	P.
Lagerkvist	Lagerkvist	k1gInSc1	Lagerkvist
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Boyeová	Boyeová	k1gFnSc1	Boyeová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
proletářští	proletářský	k2eAgMnPc1d1	proletářský
spisovatelé	spisovatel	k1gMnPc1	spisovatel
(	(	kIx(	(
<g/>
H.	H.	kA	H.
Martinson	Martinson	k1gNnSc1	Martinson
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Lundkvist	Lundkvist	k1gInSc1	Lundkvist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
existencialismus	existencialismus	k1gInSc1	existencialismus
a	a	k8xC	a
formální	formální	k2eAgInPc1d1	formální
experimenty	experiment	k1gInPc1	experiment
(	(	kIx(	(
<g/>
E.	E.	kA	E.
Johnson	Johnson	k1gMnSc1	Johnson
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Dagerman	Dagerman	k1gMnSc1	Dagerman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
kritika	kritika	k1gFnSc1	kritika
švédské	švédský	k2eAgFnSc2d1	švédská
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
dokumentární	dokumentární	k2eAgFnSc6d1	dokumentární
tvorbě	tvorba	k1gFnSc6	tvorba
(	(	kIx(	(
<g/>
S.	S.	kA	S.
Lidmanová	Lidmanová	k1gFnSc1	Lidmanová
<g/>
,	,	kIx,	,
P.	P.	kA	P.
O.	O.	kA	O.
Enquist	Enquist	k1gMnSc1	Enquist
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
přehodnocujících	přehodnocující	k2eAgInPc6d1	přehodnocující
moderní	moderní	k2eAgFnSc7d1	moderní
dějiny	dějiny	k1gFnPc4	dějiny
Švédska	Švédsko	k1gNnSc2	Švédsko
(	(	kIx(	(
<g/>
S.	S.	kA	S.
Delblanc	Delblanc	k1gInSc1	Delblanc
<g/>
)	)	kIx)	)
a	a	k8xC	a
bořících	bořící	k2eAgInPc2d1	bořící
mýty	mýtus	k1gInPc7	mýtus
o	o	k7c6	o
dokonalé	dokonalý	k2eAgFnSc6d1	dokonalá
společnosti	společnost	k1gFnSc6	společnost
(	(	kIx(	(
<g/>
P.	P.	kA	P.
C.	C.	kA	C.
Jersild	Jersild	k1gMnSc1	Jersild
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Lindgrenová	Lindgrenová	k1gFnSc1	Lindgrenová
<g/>
,	,	kIx,	,
T.	T.	kA	T.
Janssonová	Janssonová	k1gFnSc1	Janssonová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc1	léto
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
všelidská	všelidský	k2eAgNnPc1d1	všelidské
témata	téma	k1gNnPc1	téma
a	a	k8xC	a
fantazie	fantazie	k1gFnPc1	fantazie
(	(	kIx(	(
<g/>
T.	T.	kA	T.
Lindgren	Lindgrna	k1gFnPc2	Lindgrna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc1	sedm
švédských	švédský	k2eAgMnPc2d1	švédský
autorů	autor	k1gMnPc2	autor
dosud	dosud	k6eAd1	dosud
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Standardní	standardní	k2eAgFnSc1d1	standardní
švédština	švédština	k1gFnSc1	švédština
==	==	k?	==
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
nářečích	nářečí	k1gNnPc6	nářečí
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
hovoří	hovořit	k5eAaImIp3nS	hovořit
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
švédsky	švédsky	k6eAd1	švédsky
hovořících	hovořící	k2eAgMnPc2d1	hovořící
obyvatel	obyvatel	k1gMnPc2	obyvatel
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
užívané	užívaný	k2eAgNnSc1d1	užívané
švédské	švédský	k2eAgNnSc1d1	švédské
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
standardní	standardní	k2eAgFnSc4d1	standardní
švédštinu	švédština	k1gFnSc4	švédština
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
rikssvenska	rikssvensko	k1gNnPc4	rikssvensko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
říšská	říšský	k2eAgFnSc1d1	říšská
švédština	švédština	k1gFnSc1	švédština
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
se	se	k3xPyFc4	se
též	též	k9	též
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
högsvenska	högsvensko	k1gNnPc4	högsvensko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vysoká	vysoký	k2eAgFnSc1d1	vysoká
švédština	švédština	k1gFnSc1	švédština
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
finskou	finský	k2eAgFnSc4d1	finská
švédštinu	švédština	k1gFnSc4	švédština
a	a	k8xC	a
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
místních	místní	k2eAgFnPc2d1	místní
variet	varieta	k1gFnPc2	varieta
standardního	standardní	k2eAgInSc2d1	standardní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
geografické	geografický	k2eAgNnSc4d1	geografické
rozšíření	rozšíření	k1gNnSc4	rozšíření
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
tyto	tento	k3xDgFnPc1	tento
variety	varieta	k1gFnPc1	varieta
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
ovlivňovány	ovlivňován	k2eAgInPc4d1	ovlivňován
původními	původní	k2eAgInPc7d1	původní
dialekty	dialekt	k1gInPc7	dialekt
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
gramatická	gramatický	k2eAgFnSc1d1	gramatická
a	a	k8xC	a
fonologická	fonologický	k2eAgFnSc1d1	fonologická
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
středošvédským	středošvédský	k2eAgInPc3d1	středošvédský
dialektům	dialekt	k1gInPc3	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
není	být	k5eNaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
že	že	k8xS	že
novináři	novinář	k1gMnPc1	novinář
hovoří	hovořit	k5eAaImIp3nP	hovořit
s	s	k7c7	s
jistým	jistý	k2eAgInSc7d1	jistý
regionálním	regionální	k2eAgInSc7d1	regionální
akcentem	akcent	k1gInSc7	akcent
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
formální	formální	k2eAgFnSc1d1	formální
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
středošvédském	středošvédský	k2eAgInSc6d1	středošvédský
standardu	standard	k1gInSc6	standard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Finská	finský	k2eAgFnSc1d1	finská
švédština	švédština	k1gFnSc1	švédština
==	==	k?	==
</s>
</p>
<p>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
Švédska	Švédsko	k1gNnSc2	Švédsko
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
finská	finský	k2eAgNnPc4d1	finské
území	území	k1gNnPc4	území
získalo	získat	k5eAaPmAgNnS	získat
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Švédština	švédština	k1gFnSc1	švédština
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jediným	jediný	k2eAgMnSc7d1	jediný
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
jazykem	jazyk	k1gInSc7	jazyk
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
ruské	ruský	k2eAgFnSc2d1	ruská
nadvlády	nadvláda	k1gFnSc2	nadvláda
švédština	švédština	k1gFnSc1	švédština
zůstala	zůstat	k5eAaPmAgFnS	zůstat
vedle	vedle	k7c2	vedle
ruštiny	ruština	k1gFnSc2	ruština
hlavním	hlavní	k2eAgInSc7d1	hlavní
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Švédský	švédský	k2eAgInSc4d1	švédský
vliv	vliv	k1gInSc4	vliv
ale	ale	k8xC	ale
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
postupně	postupně	k6eAd1	postupně
klesal	klesat	k5eAaImAgInS	klesat
a	a	k8xC	a
snižoval	snižovat	k5eAaImAgInS	snižovat
se	se	k3xPyFc4	se
i	i	k9	i
počet	počet	k1gInSc1	počet
švédských	švédský	k2eAgMnPc2d1	švédský
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
změna	změna	k1gFnSc1	změna
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k9	až
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
inteligence	inteligence	k1gFnSc1	inteligence
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
osvojit	osvojit	k5eAaPmF	osvojit
jazyk	jazyk	k1gInSc4	jazyk
prostého	prostý	k2eAgInSc2d1	prostý
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
Finsko	Finsko	k1gNnSc4	Finsko
pozvedne	pozvednout	k5eAaPmIp3nS	pozvednout
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
národních	národní	k2eAgInPc2d1	národní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prosté	prostý	k2eAgMnPc4d1	prostý
lidi	člověk	k1gMnPc4	člověk
mluvící	mluvící	k2eAgFnSc1d1	mluvící
švédsky	švédsky	k6eAd1	švédsky
se	se	k3xPyFc4	se
nebral	brát	k5eNaImAgInS	brát
příliš	příliš	k6eAd1	příliš
ohled	ohled	k1gInSc1	ohled
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
švédská	švédský	k2eAgFnSc1d1	švédská
inteligence	inteligence	k1gFnSc1	inteligence
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
Snellmanem	Snellman	k1gMnSc7	Snellman
věřila	věřit	k5eAaImAgFnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
ruštinou	ruština	k1gFnSc7	ruština
může	moct	k5eAaImIp3nS	moct
vyhrát	vyhrát	k5eAaPmF	vyhrát
jen	jen	k9	jen
finština	finština	k1gFnSc1	finština
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
švédskou	švédský	k2eAgFnSc4d1	švédská
nadvládu	nadvláda	k1gFnSc4	nadvláda
pak	pak	k6eAd1	pak
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
jako	jako	k9	jako
zničující	zničující	k2eAgFnSc1d1	zničující
pro	pro	k7c4	pro
finský	finský	k2eAgInSc4d1	finský
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
švédštinu	švédština	k1gFnSc4	švédština
tak	tak	k6eAd1	tak
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
byl	být	k5eAaImAgMnS	být
i	i	k9	i
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgFnSc2d1	ruská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
finské	finský	k2eAgNnSc4d1	finské
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
slibovala	slibovat	k5eAaImAgFnS	slibovat
omezení	omezení	k1gNnSc3	omezení
vlivu	vliv	k1gInSc2	vliv
švédské	švédský	k2eAgFnSc2d1	švédská
kultury	kultura	k1gFnSc2	kultura
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
mírnému	mírný	k2eAgInSc3d1	mírný
úbytku	úbytek	k1gInSc3	úbytek
švédských	švédský	k2eAgMnPc2d1	švédský
mluvčích	mluvčí	k1gMnPc2	mluvčí
dochází	docházet	k5eAaImIp3nS	docházet
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
5,5	[number]	k4	5,5
%	%	kIx~	%
finské	finský	k2eAgFnSc2d1	finská
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
asi	asi	k9	asi
290	[number]	k4	290
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
hovoří	hovořit	k5eAaImIp3nS	hovořit
švédsky	švédsky	k6eAd1	švédsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
švédsky	švédsky	k6eAd1	švédsky
mluvilo	mluvit	k5eAaImAgNnS	mluvit
necelých	celý	k2eNgNnPc2d1	necelé
13	[number]	k4	13
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
asi	asi	k9	asi
340	[number]	k4	340
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Švédsky	švédsky	k6eAd1	švédsky
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
zejména	zejména	k9	zejména
na	na	k7c4	na
Å	Å	k?	Å
(	(	kIx(	(
<g/>
16	[number]	k4	16
obcí	obec	k1gFnPc2	obec
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
švédskojazyčných	švédskojazyčný	k2eAgMnPc2d1	švédskojazyčný
včetně	včetně	k7c2	včetně
největšího	veliký	k2eAgNnSc2d3	veliký
města	město	k1gNnSc2	město
Mariehamn	Mariehamna	k1gFnPc2	Mariehamna
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
a	a	k8xC	a
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
tři	tři	k4xCgInPc1	tři
jen	jen	k6eAd1	jen
švédskojazyčné	švédskojazyčný	k2eAgInPc1d1	švédskojazyčný
<g/>
,	,	kIx,	,
23	[number]	k4	23
dvojjazyčných	dvojjazyčný	k2eAgFnPc2d1	dvojjazyčná
se	se	k3xPyFc4	se
švédskou	švédský	k2eAgFnSc7d1	švédská
většinou	většina	k1gFnSc7	většina
a	a	k8xC	a
21	[number]	k4	21
dvojjazyčných	dvojjazyčný	k2eAgFnPc2d1	dvojjazyčná
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
finskou	finský	k2eAgFnSc7d1	finská
většinou	většina	k1gFnSc7	většina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
švédsky	švédsky	k6eAd1	švédsky
mluvící	mluvící	k2eAgFnSc1d1	mluvící
menšina	menšina	k1gFnSc1	menšina
v	v	k7c6	v
jednojazyčné	jednojazyčný	k2eAgFnSc6d1	jednojazyčná
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Tampere	Tamper	k1gInSc5	Tamper
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
reformy	reforma	k1gFnSc2	reforma
školství	školství	k1gNnSc2	školství
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patří	patřit	k5eAaImIp3nS	patřit
finština	finština	k1gFnSc1	finština
i	i	k8xC	i
švédština	švédština	k1gFnSc1	švédština
mezi	mezi	k7c4	mezi
povinné	povinný	k2eAgInPc4d1	povinný
školní	školní	k2eAgInPc4d1	školní
předměty	předmět	k1gInPc4	předmět
v	v	k7c6	v
pevninském	pevninský	k2eAgNnSc6d1	pevninské
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
povinnou	povinný	k2eAgFnSc7d1	povinná
součástí	součást	k1gFnSc7	součást
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
jazyk	jazyk	k1gInSc1	jazyk
žáků	žák	k1gMnPc2	žák
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
"	"	kIx"	"
<g/>
modersmå	modersmå	k?	modersmå
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
"	"	kIx"	"
<g/>
äidinkieli	äidinkiet	k5eAaBmAgMnP	äidinkiet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
výuka	výuka	k1gFnSc1	výuka
některých	některý	k3yIgInPc2	některý
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgInSc1	druhý
domácí	domácí	k2eAgInSc1d1	domácí
jazyk	jazyk	k1gInSc1	jazyk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
"	"	kIx"	"
<g/>
andra	andra	k6eAd1	andra
inhemska	inhemsek	k1gMnSc2	inhemsek
språ	språ	k?	språ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
"	"	kIx"	"
<g/>
toinen	toinen	k2eAgInSc1d1	toinen
kotimainen	kotimainen	k1gInSc1	kotimainen
kieli	kiele	k1gFnSc3	kiele
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finština	finština	k1gFnSc1	finština
jakožto	jakožto	k8xS	jakožto
ugrofinský	ugrofinský	k2eAgInSc1d1	ugrofinský
jazyk	jazyk	k1gInSc1	jazyk
má	mít	k5eAaImIp3nS	mít
gramatiku	gramatika	k1gFnSc4	gramatika
i	i	k8xC	i
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
zcela	zcela	k6eAd1	zcela
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
švédštiny	švédština	k1gFnSc2	švédština
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
zcela	zcela	k6eAd1	zcela
nesrozumitelné	srozumitelný	k2eNgNnSc1d1	nesrozumitelné
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
mezi	mezi	k7c7	mezi
finštinou	finština	k1gFnSc7	finština
a	a	k8xC	a
švédštinou	švédština	k1gFnSc7	švédština
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
výuka	výuka	k1gFnSc1	výuka
švédštiny	švédština	k1gFnSc2	švédština
se	se	k3xPyFc4	se
netěší	těšit	k5eNaImIp3nS	těšit
mezi	mezi	k7c7	mezi
Finy	Fin	k1gMnPc7	Fin
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
trochu	trochu	k6eAd1	trochu
pejorativně	pejorativně	k6eAd1	pejorativně
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pakkoruotsi	pakkoruotse	k1gFnSc4	pakkoruotse
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
povinná	povinný	k2eAgFnSc1d1	povinná
<g/>
,	,	kIx,	,
nucená	nucený	k2eAgFnSc1d1	nucená
švédština	švédština	k1gFnSc1	švédština
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
Finů	Fin	k1gMnPc2	Fin
povinnou	povinný	k2eAgFnSc4d1	povinná
výuku	výuka	k1gFnSc4	výuka
švédštiny	švédština	k1gFnSc2	švédština
nechtějí	chtít	k5eNaImIp3nP	chtít
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
standardní	standardní	k2eAgFnSc7d1	standardní
a	a	k8xC	a
finskou	finský	k2eAgFnSc7d1	finská
švédštinou	švédština	k1gFnSc7	švédština
existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgInPc1d1	určitý
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
a	a	k8xC	a
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
,	,	kIx,	,
mluvnice	mluvnice	k1gFnSc1	mluvnice
a	a	k8xC	a
pravopis	pravopis	k1gInSc1	pravopis
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prestižní	prestižní	k2eAgFnSc4d1	prestižní
varietu	varieta	k1gFnSc4	varieta
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
obvykle	obvykle	k6eAd1	obvykle
považována	považován	k2eAgFnSc1d1	považována
"	"	kIx"	"
<g/>
högsvenska	högsvensko	k1gNnPc1	högsvensko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Standardní	standardní	k2eAgFnSc1d1	standardní
švédština	švédština	k1gFnSc1	švédština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c4	v
výslovnosti	výslovnost	k1gFnPc4	výslovnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
přední	přední	k2eAgMnPc1d1	přední
[	[	kIx(	[
<g/>
aː	aː	k?	aː
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
p	p	k?	p
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
t	t	k?	t
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
k	k	k7c3	k
<g/>
]	]	kIx)	]
bez	bez	k7c2	bez
aspirace	aspirace	k1gFnSc2	aspirace
(	(	kIx(	(
<g/>
přídechu	přídech	k1gInSc2	přídech
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
ɧ	ɧ	k?	ɧ
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
ɕ	ɕ	k?	ɕ
<g/>
/	/	kIx~	/
vyslovované	vyslovovaný	k2eAgFnSc2d1	vyslovovaná
jako	jako	k8xS	jako
[	[	kIx(	[
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
tʃ	tʃ	k?	tʃ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
jako	jako	k9	jako
české	český	k2eAgFnSc3d1	Česká
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
č	č	k0	č
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
skupiny	skupina	k1gFnPc1	skupina
/	/	kIx~	/
<g/>
rt	rt	k?	rt
<g/>
,	,	kIx,	,
rd	rd	k?	rd
<g/>
,	,	kIx,	,
rl	rl	k?	rl
<g/>
,	,	kIx,	,
rn	rn	k?	rn
<g/>
,	,	kIx,	,
rs	rs	k?	rs
<g/>
/	/	kIx~	/
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
asimilaci	asimilace	k1gFnSc4	asimilace
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
hlásky	hláska	k1gFnPc1	hláska
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
obvykle	obvykle	k6eAd1	obvykle
chybí	chybit	k5eAaPmIp3nS	chybit
melodický	melodický	k2eAgInSc1d1	melodický
přízvuk	přízvuk	k1gInSc1	přízvuk
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
intonace	intonace	k1gFnSc2	intonace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nářečí	nářečí	k1gNnSc2	nářečí
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
lingvistické	lingvistický	k2eAgFnSc2d1	lingvistická
definice	definice	k1gFnSc2	definice
švédská	švédský	k2eAgNnPc1d1	švédské
nářečí	nářečí	k1gNnPc1	nářečí
znamenají	znamenat	k5eAaImIp3nP	znamenat
místní	místní	k2eAgFnPc4d1	místní
varianty	varianta	k1gFnPc4	varianta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
výrazně	výrazně	k6eAd1	výrazně
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
standardní	standardní	k2eAgFnSc7d1	standardní
švédštinou	švédština	k1gFnSc7	švédština
a	a	k8xC	a
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vystopovat	vystopovat	k5eAaPmF	vystopovat
oddělený	oddělený	k2eAgInSc4d1	oddělený
vývoj	vývoj	k1gInSc4	vývoj
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
staré	starý	k2eAgFnSc2d1	stará
severštiny	severština	k1gFnSc2	severština
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
původních	původní	k2eAgNnPc2d1	původní
nářečí	nářečí	k1gNnPc2	nářečí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Orsa	Ors	k2eAgFnSc1d1	Orsa
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Dalarna	Dalarno	k1gNnSc2	Dalarno
nebo	nebo	k8xC	nebo
Närpes	Närpesa	k1gFnPc2	Närpesa
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Österbotten	Österbottno	k1gNnPc2	Österbottno
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
fonetických	fonetický	k2eAgInPc2d1	fonetický
a	a	k8xC	a
mluvnických	mluvnický	k2eAgInPc2d1	mluvnický
rysů	rys	k1gInPc2	rys
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
plurálové	plurálový	k2eAgInPc1d1	plurálový
formy	forma	k1gFnPc4	forma
sloves	sloveso	k1gNnPc2	sloveso
nebo	nebo	k8xC	nebo
archaické	archaický	k2eAgNnSc1d1	archaické
skloňování	skloňování	k1gNnSc1	skloňování
podle	podle	k7c2	podle
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
nářečí	nářečí	k1gNnPc1	nářečí
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
Švédů	Švéd	k1gMnPc2	Švéd
obtížně	obtížně	k6eAd1	obtížně
srozumitelná	srozumitelný	k2eAgFnSc1d1	srozumitelná
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
mluvčí	mluvčí	k1gFnSc1	mluvčí
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
hovoří	hovořit	k5eAaImIp3nS	hovořit
plynně	plynně	k6eAd1	plynně
i	i	k9	i
standardní	standardní	k2eAgFnSc7d1	standardní
švédštinou	švédština	k1gFnSc7	švédština
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
odlišná	odlišný	k2eAgNnPc1d1	odlišné
nářečí	nářečí	k1gNnPc1	nářečí
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vázána	vázat	k5eAaImNgFnS	vázat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
určité	určitý	k2eAgFnSc2d1	určitá
farnosti	farnost	k1gFnSc2	farnost
<g/>
,	,	kIx,	,
švédští	švédský	k2eAgMnPc1d1	švédský
jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
je	on	k3xPp3gFnPc4	on
často	často	k6eAd1	často
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
sockenmå	sockenmå	k?	sockenmå
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
farnostní	farnostní	k2eAgInSc4d1	farnostní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédská	švédský	k2eAgNnPc1d1	švédské
nářečí	nářečí	k1gNnPc1	nářečí
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Norrländska	Norrländska	k1gFnSc1	Norrländska
må	må	k?	må
(	(	kIx(	(
<g/>
norlandská	norlandský	k2eAgNnPc1d1	norlandský
<g/>
,	,	kIx,	,
severošvédská	severošvédský	k2eAgNnPc1d1	severošvédský
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
)	)	kIx)	)
–	–	k?	–
Norrland	Norrland	k1gInSc1	Norrland
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
polovina	polovina	k1gFnSc1	polovina
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
redukcí	redukce	k1gFnSc7	redukce
koncovek	koncovka	k1gFnPc2	koncovka
po	po	k7c6	po
historicky	historicky	k6eAd1	historicky
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
slabikách	slabika	k1gFnPc6	slabika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sveamå	Sveamå	k?	Sveamå
(	(	kIx(	(
<g/>
sveonská	sveonský	k2eAgNnPc1d1	sveonský
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
)	)	kIx)	)
–	–	k?	–
Svealand	Svealand	k1gInSc1	Svealand
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgNnSc1d1	centrální
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
uppsvenska	uppsvensko	k1gNnPc1	uppsvensko
(	(	kIx(	(
<g/>
hornošvédská	hornošvédský	k2eAgFnSc1d1	hornošvédský
<g/>
)	)	kIx)	)
–	–	k?	–
typický	typický	k2eAgInSc1d1	typický
zánik	zánik	k1gInSc1	zánik
-t	-t	k?	-t
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
členu	člen	k1gInSc6	člen
a	a	k8xC	a
supinu	supinum	k1gNnSc6	supinum
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
mellansvenska	mellansvensko	k1gNnPc1	mellansvensko
(	(	kIx(	(
<g/>
středošvédská	středošvédský	k2eAgFnSc1d1	středošvédský
<g/>
)	)	kIx)	)
–	–	k?	–
společné	společný	k2eAgInPc4d1	společný
rysy	rys	k1gInPc4	rys
s	s	k7c7	s
hornošvédskými	hornošvédský	k2eAgNnPc7d1	hornošvédský
a	a	k8xC	a
gótskými	gótský	k2eAgNnPc7d1	gótské
nářečími	nářečí	k1gNnPc7	nářečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Götamå	Götamå	k?	Götamå
(	(	kIx(	(
<g/>
gótská	gótský	k2eAgNnPc1d1	gótské
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
)	)	kIx)	)
–	–	k?	–
západní	západní	k2eAgInSc4d1	západní
a	a	k8xC	a
severní	severní	k2eAgInSc4d1	severní
Götaland	Götaland	k1gInSc4	Götaland
<g/>
,	,	kIx,	,
s	s	k7c7	s
tradičním	tradiční	k2eAgNnSc7d1	tradiční
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
Västergötlandu	Västergötland	k1gInSc6	Västergötland
<g/>
;	;	kIx,	;
redukce	redukce	k1gFnSc1	redukce
nepřízvučných	přízvučný	k2eNgFnPc2d1	nepřízvučná
samohlásek	samohláska	k1gFnPc2	samohláska
na	na	k7c6	na
[	[	kIx(	[
<g/>
ɘ	ɘ	k?	ɘ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
otevřenější	otevřený	k2eAgFnSc1d2	otevřenější
výslovnost	výslovnost	k1gFnSc1	výslovnost
i	i	k9	i
<g/>
,	,	kIx,	,
y	y	k?	y
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
ö	ö	k?	ö
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
(	(	kIx(	(
<g/>
jako	jako	k9	jako
o	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zánik	zánik	k1gInSc1	zánik
-r	-r	k?	-r
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sydsvenska	Sydsvensko	k1gNnPc1	Sydsvensko
må	må	k?	må
(	(	kIx(	(
<g/>
jihošvédská	jihošvédský	k2eAgNnPc1d1	jihošvédský
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
)	)	kIx)	)
–	–	k?	–
jižní	jižní	k2eAgNnSc4d1	jižní
Švédsko	Švédsko	k1gNnSc4	Švédsko
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Blekinge	Bleking	k1gInSc2	Bleking
<g/>
,	,	kIx,	,
jižního	jižní	k2eAgInSc2d1	jižní
Hallandu	Halland	k1gInSc2	Halland
a	a	k8xC	a
jižního	jižní	k2eAgMnSc2d1	jižní
Små	Små	k1gMnSc2	Små
<g/>
;	;	kIx,	;
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
dánštinou	dánština	k1gFnSc7	dánština
<g/>
,	,	kIx,	,
existence	existence	k1gFnSc1	existence
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
<g/>
,	,	kIx,	,
uvulární	uvulární	k2eAgFnSc7d1	uvulární
[	[	kIx(	[
<g/>
R	R	kA	R
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnSc2	skupina
/	/	kIx~	/
<g/>
rt	rt	k?	rt
<g/>
,	,	kIx,	,
rd	rd	k?	rd
<g/>
,	,	kIx,	,
rl	rl	k?	rl
<g/>
,	,	kIx,	,
rn	rn	k?	rn
<g/>
,	,	kIx,	,
rs	rs	k?	rs
<g/>
/	/	kIx~	/
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
asimilaci	asimilace	k1gFnSc4	asimilace
<g/>
,	,	kIx,	,
hlásky	hlásek	k1gInPc1	hlásek
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
znělé	znělý	k2eAgInPc4d1	znělý
explozivy	exploziv	k1gInPc4	exploziv
místo	místo	k7c2	místo
neznělých	znělý	k2eNgMnPc2d1	neznělý
mezi	mezi	k7c7	mezi
samohláskami	samohláska	k1gFnPc7	samohláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
gada	gada	k6eAd1	gada
místo	místo	k7c2	místo
gata	gatum	k1gNnSc2	gatum
<g/>
,	,	kIx,	,
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gotländska	Gotländska	k1gFnSc1	Gotländska
må	må	k?	må
(	(	kIx(	(
<g/>
gotlandská	gotlandský	k2eAgNnPc1d1	gotlandský
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
)	)	kIx)	)
–	–	k?	–
ostrov	ostrov	k1gInSc1	ostrov
Gotland	Gotland	k1gInSc1	Gotland
<g/>
;	;	kIx,	;
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
starou	starý	k2eAgFnSc4d1	stará
gotlandštinu	gotlandština	k1gFnSc4	gotlandština
(	(	kIx(	(
<g/>
forngutniska	forngutnisko	k1gNnSc2	forngutnisko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
existence	existence	k1gFnSc1	existence
dvojhlásek	dvojhláska	k1gFnPc2	dvojhláska
<g/>
,	,	kIx,	,
uchování	uchování	k1gNnSc3	uchování
původního	původní	k2eAgNnSc2d1	původní
/	/	kIx~	/
<g/>
aː	aː	k?	aː
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
skandinávských	skandinávský	k2eAgInPc6d1	skandinávský
jazycích	jazyk	k1gInPc6	jazyk
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
/	/	kIx~	/
<g/>
oː	oː	k?	oː
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
psané	psaný	k2eAgFnPc1d1	psaná
jako	jako	k8xC	jako
å	å	k?	å
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zachování	zachování	k1gNnSc4	zachování
výslovnosti	výslovnost	k1gFnSc2	výslovnost
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
sk	sk	k?	sk
<g/>
/	/	kIx~	/
před	před	k7c7	před
předními	přední	k2eAgFnPc7d1	přední
samohláskami	samohláska	k1gFnPc7	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Östsvenska	Östsvensko	k1gNnPc1	Östsvensko
må	må	k?	må
(	(	kIx(	(
<g/>
východošvédská	východošvédský	k2eAgNnPc1d1	východošvédský
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
)	)	kIx)	)
–	–	k?	–
souostroví	souostroví	k1gNnSc2	souostroví
Å	Å	k?	Å
a	a	k8xC	a
pevninské	pevninský	k2eAgNnSc1d1	pevninské
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
;	;	kIx,	;
značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
nářečími	nářečí	k1gNnPc7	nářečí
<g/>
,	,	kIx,	,
špatná	špatný	k2eAgFnSc1d1	špatná
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
srozumitelnost	srozumitelnost	k1gFnSc1	srozumitelnost
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
obecných	obecný	k2eAgInPc2d1	obecný
rysů	rys	k1gInPc2	rys
výslovnosti	výslovnost	k1gFnSc2	výslovnost
uvedených	uvedený	k2eAgInPc2d1	uvedený
v	v	k7c6	v
části	část	k1gFnSc6	část
Finská	finský	k2eAgFnSc1d1	finská
švédština	švédština	k1gFnSc1	švédština
také	také	k9	také
krátké	krátký	k2eAgFnPc1d1	krátká
slabiky	slabika	k1gFnPc1	slabika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
äta	äta	k?	äta
(	(	kIx(	(
<g/>
jíst	jíst	k5eAaImF	jíst
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
ɛ	ɛ	k?	ɛ
<g />
.	.	kIx.	.
</s>
<s>
<g/>
]	]	kIx)	]
místo	místo	k1gNnSc4	místo
[	[	kIx(	[
<g/>
ɛ	ɛ	k?	ɛ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
staré	starý	k2eAgFnPc1d1	stará
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stein	stein	k1gInSc1	stein
místo	místo	k1gNnSc1	místo
sten	sten	k1gInSc1	sten
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
öy	öy	k?	öy
místo	místo	k1gNnSc4	místo
ö	ö	k?	ö
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vypouštění	vypouštění	k1gNnSc4	vypouštění
koncovek	koncovka	k1gFnPc2	koncovka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
huset	huset	k1gInSc1	huset
[	[	kIx(	[
<g/>
ɦ	ɦ	k?	ɦ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
inte	inte	k1gFnSc1	inte
[	[	kIx(	[
<g/>
ɪ	ɪ	k?	ɪ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
mluvilo	mluvit	k5eAaImAgNnS	mluvit
švédsky	švédsky	k6eAd1	švédsky
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Estonska	Estonsko	k1gNnSc2	Estonsko
(	(	kIx(	(
<g/>
estonská	estonský	k2eAgFnSc1d1	Estonská
švédština	švédština	k1gFnSc1	švédština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
==	==	k?	==
</s>
</p>
<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
používá	používat	k5eAaImIp3nS	používat
latinku	latinka	k1gFnSc4	latinka
<g/>
,	,	kIx,	,
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
o	o	k7c4	o
některé	některý	k3yIgInPc4	některý
další	další	k2eAgInPc4d1	další
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnSc2	písmeno
Q	Q	kA	Q
a	a	k8xC	a
W	W	kA	W
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
pro	pro	k7c4	pro
některá	některý	k3yIgNnPc4	některý
cizí	cizí	k2eAgNnPc4d1	cizí
slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
bowling	bowling	k1gInSc1	bowling
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
Å	Å	k?	Å
<g/>
,	,	kIx,	,
Ä	Ä	kA	Ä
a	a	k8xC	a
Ö	Ö	kA	Ö
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
samostatná	samostatný	k2eAgNnPc4d1	samostatné
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
jen	jen	k9	jen
za	za	k7c4	za
znaky	znak	k1gInPc4	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
abecedě	abeceda	k1gFnSc6	abeceda
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pořadí	pořadí	k1gNnSc6	pořadí
za	za	k7c4	za
písmeno	písmeno	k1gNnSc4	písmeno
Z.	Z.	kA	Z.
Písmeno	písmeno	k1gNnSc1	písmeno
W	W	kA	W
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
švédské	švédský	k2eAgFnSc2d1	švédská
abecedy	abeceda	k1gFnSc2	abeceda
oficiálně	oficiálně	k6eAd1	oficiálně
přijato	přijmout	k5eAaPmNgNnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
variantu	varianta	k1gFnSc4	varianta
V.	V.	kA	V.
Ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
a	a	k8xC	a
abecedních	abecední	k2eAgInPc6d1	abecední
seznamech	seznam	k1gInPc6	seznam
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
znaky	znak	k1gInPc1	znak
řadily	řadit	k5eAaImAgInP	řadit
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k9	již
každý	každý	k3xTgMnSc1	každý
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
abeceda	abeceda	k1gFnSc1	abeceda
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
celkem	celkem	k6eAd1	celkem
29	[number]	k4	29
písmen	písmeno	k1gNnPc2	písmeno
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
28	[number]	k4	28
písmen	písmeno	k1gNnPc2	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédský	švédský	k2eAgInSc1d1	švédský
pravopis	pravopis	k1gInSc1	pravopis
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
fonologický	fonologický	k2eAgMnSc1d1	fonologický
<g/>
,	,	kIx,	,
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
však	však	k9	však
mnoho	mnoho	k4c4	mnoho
historických	historický	k2eAgInPc2d1	historický
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
psaní	psaní	k1gNnSc1	psaní
skupin	skupina	k1gFnPc2	skupina
dj	dj	k?	dj
<g/>
,	,	kIx,	,
hj	hj	k?	hj
<g/>
,	,	kIx,	,
lj	lj	k?	lj
<g/>
,	,	kIx,	,
různý	různý	k2eAgInSc4d1	různý
způsob	způsob	k1gInSc4	způsob
psaní	psaní	k1gNnSc2	psaní
téže	tenže	k3xDgFnSc2	tenže
hlásky	hláska	k1gFnSc2	hláska
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
sj	sj	k?	sj
<g/>
,	,	kIx,	,
sk	sk	k?	sk
<g/>
,	,	kIx,	,
skj	skj	k?	skj
<g/>
,	,	kIx,	,
sch	sch	k?	sch
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
výjimek	výjimka	k1gFnPc2	výjimka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
znaků	znak	k1gInPc2	znak
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
mnohé	mnohý	k2eAgFnPc4d1	mnohá
spřežky	spřežka	k1gFnPc4	spřežka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Výslovnost	výslovnost	k1gFnSc1	výslovnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Samohlásky	samohláska	k1gFnSc2	samohláska
===	===	k?	===
</s>
</p>
<p>
<s>
Švédské	švédský	k2eAgFnPc1d1	švédská
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
liší	lišit	k5eAaImIp3nS	lišit
svojí	svůj	k3xOyFgFnSc7	svůj
délkou	délka	k1gFnSc7	délka
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přízvučných	přízvučný	k2eAgFnPc6d1	přízvučná
slabikách	slabika	k1gFnPc6	slabika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Délka	délka	k1gFnSc1	délka
hlásek	hlásek	k1gInSc1	hlásek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
u	u	k7c2	u
předních	přední	k2eAgFnPc2d1	přední
samohlásek	samohláska	k1gFnPc2	samohláska
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
páry	pár	k1gInPc1	pár
se	s	k7c7	s
zaokrouhlenou	zaokrouhlený	k2eAgFnSc7d1	zaokrouhlená
a	a	k8xC	a
nezaokrouhlenou	zaokrouhlený	k2eNgFnSc7d1	nezaokrouhlená
výslovností	výslovnost	k1gFnSc7	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Švédština	švédština	k1gFnSc1	švédština
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
17	[number]	k4	17
samohláskových	samohláskový	k2eAgInPc2d1	samohláskový
fonémů	foném	k1gInPc2	foném
–	–	k?	–
9	[number]	k4	9
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
a	a	k8xC	a
8	[number]	k4	8
krátkých	krátká	k1gFnPc2	krátká
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
krátkého	krátké	k1gNnSc2	krátké
/	/	kIx~	/
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
a	a	k8xC	a
/	/	kIx~	/
<g/>
ä	ä	k?	ä
<g/>
/	/	kIx~	/
splývá	splývat	k5eAaImIp3nS	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
výslovnost	výslovnost	k1gFnSc1	výslovnost
nepřízvučného	přízvučný	k2eNgInSc2d1	nepřízvučný
/	/	kIx~	/
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
ə	ə	k?	ə
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
v	v	k7c6	v
hovorovém	hovorový	k2eAgInSc6d1	hovorový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
švédštině	švédština	k1gFnSc6	švédština
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
dvojhlásky	dvojhláska	k1gFnPc1	dvojhláska
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přejatých	přejatý	k2eAgNnPc6d1	přejaté
slovech	slovo	k1gNnPc6	slovo
(	(	kIx(	(
<g/>
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
paus	pausa	k1gFnPc2	pausa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Souhlásky	souhláska	k1gFnSc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
J	J	kA	J
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezdvojuje	zdvojovat	k5eNaImIp3nS	zdvojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
dlouze	dlouho	k6eAd1	dlouho
(	(	kIx(	(
<g/>
böja	böja	k1gFnSc1	böja
[	[	kIx(	[
<g/>
bø	bø	k?	bø
<g/>
:	:	kIx,	:
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
M	M	kA	M
a	a	k8xC	a
n	n	k0	n
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slabiky	slabika	k1gFnSc2	slabika
se	se	k3xPyFc4	se
též	též	k9	též
nezdvojují	zdvojovat	k5eNaImIp3nP	zdvojovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
dlouze	dlouho	k6eAd1	dlouho
(	(	kIx(	(
<g/>
glöm	glöm	k6eAd1	glöm
[	[	kIx(	[
<g/>
glø	glø	k?	glø
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
glömma	glömma	k1gFnSc1	glömma
[	[	kIx(	[
<g/>
glø	glø	k?	glø
<g/>
:	:	kIx,	:
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Specificky	specificky	k6eAd1	specificky
švédský	švédský	k2eAgInSc1d1	švédský
je	být	k5eAaImIp3nS	být
foném	foném	k1gInSc1	foném
/	/	kIx~	/
<g/>
ɧ	ɧ	k?	ɧ
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
sje	sje	k?	sje
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovnících	slovník	k1gInPc6	slovník
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
přepisován	přepisovat	k5eAaImNgMnS	přepisovat
jako	jako	k9	jako
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
<g/>
ʃ	ʃ	k?	ʃ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
od	od	k7c2	od
českého	český	k2eAgInSc2d1	český
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
výslovnost	výslovnost	k1gFnSc1	výslovnost
místně	místně	k6eAd1	místně
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nS	lišit
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
jako	jako	k8xS	jako
české	český	k2eAgFnPc1d1	Česká
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
však	však	k9	však
není	být	k5eNaImIp3nS	být
chybou	chyba	k1gFnSc7	chyba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
nářečích	nářečí	k1gNnPc6	nářečí
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
různě	různě	k6eAd1	různě
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výslovnost	výslovnost	k1gFnSc1	výslovnost
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
[	[	kIx(	[
<g/>
š	š	k?	š
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
ch	ch	k0	ch
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
můžeme	moct	k5eAaImIp1nP	moct
slyšet	slyšet	k5eAaImF	slyšet
ještě	ještě	k6eAd1	ještě
slabé	slabý	k2eAgNnSc1d1	slabé
[	[	kIx(	[
<g/>
w	w	k?	w
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
temná	temný	k2eAgFnSc1d1	temná
<g/>
"	"	kIx"	"
výslovnost	výslovnost	k1gFnSc1	výslovnost
blížící	blížící	k2eAgFnSc1d1	blížící
se	se	k3xPyFc4	se
[	[	kIx(	[
<g/>
ch	ch	k0	ch
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
nářečí	nářečí	k1gNnSc4	nářečí
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
spíše	spíše	k9	spíše
slyšíme	slyšet	k5eAaImIp1nP	slyšet
[	[	kIx(	[
<g/>
š	š	k?	š
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Temné	temný	k2eAgNnSc1d1	temné
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
ch	ch	k0	ch
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slov	slovo	k1gNnPc2	slovo
před	před	k7c7	před
přízvučnou	přízvučný	k2eAgFnSc7d1	přízvučná
samohláskou	samohláska	k1gFnSc7	samohláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
själv	själv	k1gInSc1	själv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nepřízvučnou	přízvučný	k2eNgFnSc7d1	nepřízvučná
samohláskou	samohláska	k1gFnSc7	samohláska
nebo	nebo	k8xC	nebo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
[	[	kIx(	[
<g/>
š	š	k?	š
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dusch	dusch	k1gInSc1	dusch
<g/>
,	,	kIx,	,
kanske	kanske	k1gInSc1	kanske
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
tje	tje	k?	tje
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
obvykle	obvykle	k6eAd1	obvykle
přepisované	přepisovaný	k2eAgInPc1d1	přepisovaný
jako	jako	k8xC	jako
[	[	kIx(	[
<g/>
č	č	k0	č
<g/>
/	/	kIx~	/
<g/>
tʃ	tʃ	k?	tʃ
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
blízké	blízký	k2eAgNnSc4d1	blízké
měkkému	měkký	k2eAgInSc3d1	měkký
polskému	polský	k2eAgInSc3d1	polský
/	/	kIx~	/
<g/>
ś	ś	k?	ś
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ɕ	ɕ	k?	ɕ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
/	/	kIx~	/
<g/>
ć	ć	k?	ć
<g/>
/	/	kIx~	/
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
tɕ	tɕ	k?	tɕ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
R	R	kA	R
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
nářečích	nářečí	k1gNnPc6	nářečí
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Švédska	Švédsko	k1gNnSc2	Švédsko
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
hrdelní	hrdelní	k2eAgNnSc1d1	hrdelní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slabikách	slabika	k1gFnPc6	slabika
rd	rd	k?	rd
[	[	kIx(	[
<g/>
ɖ	ɖ	k?	ɖ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
rl	rl	k?	rl
[	[	kIx(	[
<g/>
ɭ	ɭ	k?	ɭ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
rn	rn	k?	rn
[	[	kIx(	[
<g/>
ɳ	ɳ	k?	ɳ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
rs	rs	k?	rs
[	[	kIx(	[
<g/>
ʂ	ʂ	k?	ʂ
<g/>
]	]	kIx)	]
a	a	k8xC	a
rt	rt	k?	rt
[	[	kIx(	[
<g/>
ʈ	ʈ	k?	ʈ
<g/>
]	]	kIx)	]
se	s	k7c7	s
r	r	kA	r
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
výslovnost	výslovnost	k1gFnSc1	výslovnost
následující	následující	k2eAgFnSc2d1	následující
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
se	s	k7c7	s
špičkou	špička	k1gFnSc7	špička
jazyka	jazyk	k1gInSc2	jazyk
obrácenou	obrácený	k2eAgFnSc4d1	obrácená
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
vzhůru	vzhůru	k6eAd1	vzhůru
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
retroflexní	retroflexní	k2eAgInPc1d1	retroflexní
hlásky	hlásek	k1gInPc1	hlásek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
rs	rs	k?	rs
[	[	kIx(	[
<g/>
ʂ	ʂ	k?	ʂ
<g/>
]	]	kIx)	]
zní	znět	k5eAaImIp3nS	znět
asi	asi	k9	asi
jako	jako	k9	jako
české	český	k2eAgFnSc3d1	Česká
/	/	kIx~	/
<g/>
š	š	k?	š
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
tuto	tento	k3xDgFnSc4	tento
hlásku	hláska	k1gFnSc4	hláska
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
polské	polský	k2eAgNnSc1d1	polské
/	/	kIx~	/
<g/>
sz	sz	k?	sz
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
však	však	k8xC	však
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
asimilaci	asimilace	k1gFnSc3	asimilace
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
hlásky	hláska	k1gFnPc1	hláska
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
též	též	k9	též
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
vysoký	vysoký	k2eAgInSc4d1	vysoký
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neznělé	znělý	k2eNgFnPc1d1	neznělá
explozivy	exploziva	k1gFnPc1	exploziva
/	/	kIx~	/
<g/>
p	p	k?	p
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
pozic	pozice	k1gFnPc2	pozice
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
s	s	k7c7	s
aspirací	aspirace	k1gFnSc7	aspirace
(	(	kIx(	(
<g/>
přídechem	přídech	k1gInSc7	přídech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
[	[	kIx(	[
<g/>
ph	ph	k0wR	ph
<g/>
,	,	kIx,	,
th	th	k?	th
<g/>
,	,	kIx,	,
kh	kh	k0	kh
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
neexistují	existovat	k5eNaImIp3nP	existovat
hlásky	hlásek	k1gInPc1	hlásek
odpovídající	odpovídající	k2eAgInPc1d1	odpovídající
českému	český	k2eAgMnSc3d1	český
/	/	kIx~	/
<g/>
č	č	k0	č
<g/>
,	,	kIx,	,
ď	ď	k?	ď
<g/>
,	,	kIx,	,
dž	dž	k?	dž
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
,	,	kIx,	,
ň	ň	k?	ň
<g/>
,	,	kIx,	,
ř	ř	k?	ř
<g/>
,	,	kIx,	,
ť	ť	k?	ť
<g/>
,	,	kIx,	,
z	z	k0	z
<g/>
,	,	kIx,	,
ž	ž	k?	ž
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Délka	délka	k1gFnSc1	délka
hlásek	hláska	k1gFnPc2	hláska
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
i	i	k9	i
souhlásky	souhláska	k1gFnPc1	souhláska
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
a	a	k8xC	a
krátké	krátký	k2eAgFnSc6d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
přízvučných	přízvučný	k2eAgFnPc6d1	přízvučná
slabikách	slabika	k1gFnPc6	slabika
po	po	k7c6	po
samohlásce	samohláska	k1gFnSc6	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Dlouze	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
na	na	k7c6	na
konci	konec	k1gInSc6	konec
přízvučné	přízvučný	k2eAgFnSc2d1	přízvučná
slabiky	slabika	k1gFnSc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Přízvučné	přízvučný	k2eAgFnPc1d1	přízvučná
slabiky	slabika	k1gFnPc1	slabika
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Buď	buď	k8xC	buď
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
následována	následovat	k5eAaImNgFnS	následovat
krátkou	krátký	k2eAgFnSc7d1	krátká
nebo	nebo	k8xC	nebo
žádnou	žádný	k3yNgFnSc7	žádný
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
samohlásce	samohláska	k1gFnSc6	samohláska
následuje	následovat	k5eAaImIp3nS	následovat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
kombinace	kombinace	k1gFnPc1	kombinace
nejsou	být	k5eNaImIp3nP	být
možné	možný	k2eAgFnPc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
illa	illa	k1gFnSc1	illa
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
–	–	k?	–
špatný	špatný	k2eAgInSc1d1	špatný
(	(	kIx(	(
<g/>
zdvojená	zdvojený	k2eAgFnSc1d1	zdvojená
souhláska	souhláska	k1gFnSc1	souhláska
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
předcházející	předcházející	k2eAgFnSc1d1	předcházející
samohláska	samohláska	k1gFnSc1	samohláska
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
önska	önska	k1gFnSc1	önska
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
ska	ska	k?	ska
<g/>
]	]	kIx)	]
–	–	k?	–
přát	přát	k5eAaImF	přát
si	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc4	první
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
předcházející	předcházející	k2eAgFnSc1d1	předcházející
samohláska	samohláska	k1gFnSc1	samohláska
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
god	god	k?	god
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
d	d	k?	d
<g/>
]	]	kIx)	]
–	–	k?	–
dobrý	dobrý	k2eAgInSc1d1	dobrý
(	(	kIx(	(
<g/>
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
následovaná	následovaný	k2eAgFnSc1d1	následovaná
jednou	jeden	k4xCgFnSc7	jeden
souhláskou	souhláska	k1gFnSc7	souhláska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tala	tala	k1gFnSc1	tala
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
la	la	k1gNnSc1	la
<g/>
]	]	kIx)	]
–	–	k?	–
mluvit	mluvit	k5eAaImF	mluvit
(	(	kIx(	(
<g/>
totéž	týž	k3xTgNnSc1	týž
<g/>
,	,	kIx,	,
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
slabice	slabika	k1gFnSc6	slabika
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ö	ö	k?	ö
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
:	:	kIx,	:
<g/>
]	]	kIx)	]
–	–	k?	–
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
slabice	slabika	k1gFnSc6	slabika
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
samohláska	samohláska	k1gFnSc1	samohláska
<g/>
)	)	kIx)	)
<g/>
Nepřízvučné	přízvučný	k2eNgFnPc1d1	nepřízvučná
slabiky	slabika	k1gFnPc1	slabika
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
krátké	krátká	k1gFnPc1	krátká
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
krátké	krátký	k2eAgFnPc1d1	krátká
samohlásky	samohláska	k1gFnPc1	samohláska
i	i	k8xC	i
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Intonace	intonace	k1gFnSc2	intonace
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
švédskou	švédský	k2eAgFnSc4d1	švédská
výslovnost	výslovnost	k1gFnSc4	výslovnost
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
u	u	k7c2	u
každého	každý	k3xTgNnSc2	každý
slova	slovo	k1gNnSc2	slovo
intonaci	intonace	k1gFnSc4	intonace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
dvojí	dvojit	k5eAaImIp3nS	dvojit
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
intonace	intonace	k1gFnSc2	intonace
jsou	být	k5eAaImIp3nP	být
komplikovaná	komplikovaný	k2eAgNnPc1d1	komplikované
<g/>
.	.	kIx.	.
</s>
<s>
Intonace	intonace	k1gFnSc1	intonace
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
nářečích	nářečí	k1gNnPc6	nářečí
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
intonace	intonace	k1gFnSc1	intonace
(	(	kIx(	(
<g/>
akutová	akutový	k2eAgFnSc1d1	akutový
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
klesavá	klesavý	k2eAgFnSc1d1	klesavá
<g/>
,	,	kIx,	,
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
intonace	intonace	k1gFnSc1	intonace
(	(	kIx(	(
<g/>
gravisová	gravisový	k2eAgFnSc1d1	gravisový
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
přízvučná	přízvučný	k2eAgFnSc1d1	přízvučná
slabika	slabika	k1gFnSc1	slabika
se	se	k3xPyFc4	se
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
s	s	k7c7	s
klesající	klesající	k2eAgFnSc7d1	klesající
intonací	intonace	k1gFnSc7	intonace
<g/>
,	,	kIx,	,
následující	následující	k2eAgFnSc1d1	následující
slabika	slabika	k1gFnSc1	slabika
se	se	k3xPyFc4	se
však	však	k9	však
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
vyšším	vysoký	k2eAgInSc7d2	vyšší
tónem	tón	k1gInSc7	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intonací	intonace	k1gFnSc7	intonace
se	se	k3xPyFc4	se
často	často	k6eAd1	často
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
homonyma	homonymum	k1gNnPc1	homonymum
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
různé	různý	k2eAgInPc4d1	různý
významy	význam	k1gInPc4	význam
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
píší	psát	k5eAaImIp3nP	psát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
anden	anden	k1gInSc1	anden
[	[	kIx(	[
<g/>
ánden	ánden	k1gInSc1	ánden
<g/>
]	]	kIx)	]
–	–	k?	–
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
)	)	kIx)	)
kachna	kachna	k1gFnSc1	kachna
(	(	kIx(	(
<g/>
od	od	k7c2	od
"	"	kIx"	"
<g/>
and	and	k?	and
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
intonace	intonace	k1gFnSc1	intonace
</s>
</p>
<p>
<s>
anden	anden	k1gInSc1	anden
[	[	kIx(	[
<g/>
à	à	k?	à
<g/>
]	]	kIx)	]
–	–	k?	–
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
)	)	kIx)	)
duch	duch	k1gMnSc1	duch
(	(	kIx(	(
<g/>
od	od	k7c2	od
"	"	kIx"	"
<g/>
ande	ande	k1gNnSc2	ande
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
intonace	intonace	k1gFnSc1	intonace
</s>
</p>
<p>
<s>
===	===	k?	===
Přízvuk	přízvuk	k1gInSc4	přízvuk
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
je	být	k5eAaImIp3nS	být
přízvuk	přízvuk	k1gInSc1	přízvuk
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
slabice	slabika	k1gFnSc6	slabika
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
čárkou	čárka	k1gFnSc7	čárka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zároveň	zároveň	k6eAd1	zároveň
znamená	znamenat	k5eAaImIp3nS	znamenat
i	i	k9	i
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
týká	týkat	k5eAaImIp3nS	týkat
příjmení	příjmení	k1gNnSc4	příjmení
(	(	kIx(	(
<g/>
Linné	Linný	k2eAgMnPc4d1	Linný
<g/>
)	)	kIx)	)
a	a	k8xC	a
slov	slovo	k1gNnPc2	slovo
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
idé	idé	k?	idé
–	–	k?	–
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
domácího	domácí	k2eAgInSc2d1	domácí
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
přízvuk	přízvuk	k1gInSc1	přízvuk
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
slov	slovo	k1gNnPc2	slovo
s	s	k7c7	s
nepřízvučnými	přízvučný	k2eNgFnPc7d1	nepřízvučná
předponami	předpona	k1gFnPc7	předpona
be-	be-	k?	be-
<g/>
,	,	kIx,	,
ent-	ent-	k?	ent-
<g/>
,	,	kIx,	,
för-	för-	k?	för-
a	a	k8xC	a
ge-	ge-	k?	ge-
(	(	kIx(	(
<g/>
např.	např.	kA	např.
beskriva	beskriva	k1gFnSc1	beskriva
[	[	kIx(	[
<g/>
beˈ	beˈ	k?	beˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
popsat	popsat	k5eAaPmF	popsat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výpůjčkách	výpůjčka	k1gFnPc6	výpůjčka
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
přízvuk	přízvuk	k1gInSc1	přízvuk
nachází	nacházet	k5eAaImIp3nS	nacházet
často	často	k6eAd1	často
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
nebo	nebo	k8xC	nebo
předposlední	předposlední	k2eAgFnSc6d1	předposlední
slabice	slabika	k1gFnSc6	slabika
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nivå	nivå	k?	nivå
[	[	kIx(	[
<g/>
nɪ	nɪ	k?	nɪ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přízvuk	přízvuk	k1gInSc1	přízvuk
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
slabice	slabika	k1gFnSc6	slabika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
mluvnice	mluvnice	k1gFnSc1	mluvnice
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
podobných	podobný	k2eAgInPc2d1	podobný
principů	princip	k1gInPc2	princip
jako	jako	k8xS	jako
mluvnice	mluvnice	k1gFnSc1	mluvnice
ostatních	ostatní	k2eAgInPc2d1	ostatní
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
pochopení	pochopení	k1gNnSc4	pochopení
je	být	k5eAaImIp3nS	být
výhodou	výhoda	k1gFnSc7	výhoda
znalost	znalost	k1gFnSc1	znalost
jiného	jiný	k2eAgInSc2d1	jiný
germánského	germánský	k2eAgInSc2d1	germánský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
angličtiny	angličtina	k1gFnSc2	angličtina
nebo	nebo	k8xC	nebo
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
flexe	flexe	k1gFnSc1	flexe
dává	dávat	k5eAaImIp3nS	dávat
současné	současný	k2eAgFnSc3d1	současná
švédštině	švédština	k1gFnSc3	švédština
převažující	převažující	k2eAgInSc4d1	převažující
charakter	charakter	k1gInSc4	charakter
analytického	analytický	k2eAgInSc2d1	analytický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jmenný	jmenný	k2eAgInSc1d1	jmenný
rod	rod	k1gInSc1	rod
===	===	k?	===
</s>
</p>
<p>
<s>
Mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
rod	rod	k1gInSc4	rod
splynuly	splynout	k5eAaPmAgFnP	splynout
<g/>
,	,	kIx,	,
standardní	standardní	k2eAgFnSc1d1	standardní
švédština	švédština	k1gFnSc1	švédština
dnes	dnes	k6eAd1	dnes
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
2	[number]	k4	2
rody	rod	k1gInPc4	rod
<g/>
:	:	kIx,	:
společný	společný	k2eAgInSc4d1	společný
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
)	)	kIx)	)
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Člen	člen	k1gMnSc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
determinována	determinovat	k5eAaBmNgFnS	determinovat
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dvojí	dvojí	k4xRgMnSc1	dvojí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
neurčitý	určitý	k2eNgInSc1d1	neurčitý
<g/>
:	:	kIx,	:
en	en	k?	en
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc1d1	společný
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ett	ett	k?	ett
(	(	kIx(	(
<g/>
střední	střední	k2eAgInSc4d1	střední
rod	rod	k1gInSc4	rod
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
určitý	určitý	k2eAgInSc4d1	určitý
<g/>
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
postpozitivní	postpozitivní	k2eAgInSc1d1	postpozitivní
–	–	k?	–
v	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
-en	n	k?	-en
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc1d1	společný
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
-et	t	k?	-et
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
rod	rod	k1gInSc1	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
-na	a	k?	-na
<g/>
/	/	kIx~	/
<g/>
-en	n	k?	-en
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
přídavnými	přídavný	k2eAgNnPc7d1	přídavné
jmény	jméno	k1gNnPc7	jméno
navíc	navíc	k6eAd1	navíc
předchází	předcházet	k5eAaImIp3nS	předcházet
ještě	ještě	k6eAd1	ještě
volný	volný	k2eAgInSc1d1	volný
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
(	(	kIx(	(
<g/>
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
det	det	k?	det
<g/>
,	,	kIx,	,
de	de	k?	de
<g/>
)	)	kIx)	)
–	–	k?	–
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
determinováno	determinovat	k5eAaBmNgNnS	determinovat
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
tvoření	tvoření	k1gNnSc2	tvoření
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
5	[number]	k4	5
typů	typ	k1gInPc2	typ
skloňování	skloňování	k1gNnSc2	skloňování
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgNnSc1d3	nejčastější
zakončení	zakončení	k1gNnSc1	zakončení
je	být	k5eAaImIp3nS	být
-er	r	k?	-er
<g/>
,	,	kIx,	,
-ar	r	k?	-ar
nebo	nebo	k8xC	nebo
-or	r	k?	-or
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
-en	n	k?	-en
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
systému	systém	k1gInSc2	systém
4	[number]	k4	4
pádů	pád	k1gInPc2	pád
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
nominativ	nominativ	k1gInSc1	nominativ
a	a	k8xC	a
genitiv	genitiv	k1gInSc1	genitiv
(	(	kIx(	(
<g/>
zakončení	zakončení	k1gNnSc1	zakončení
-s	-s	k?	-s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
výlučně	výlučně	k6eAd1	výlučně
přivlastňovací	přivlastňovací	k2eAgFnSc4d1	přivlastňovací
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
nominativu	nominativ	k1gInSc2	nominativ
<g/>
.	.	kIx.	.
<g/>
Slovníková	slovníkový	k2eAgNnPc1d1	slovníkové
hesla	heslo	k1gNnPc1	heslo
u	u	k7c2	u
podstatných	podstatný	k2eAgNnPc2d1	podstatné
jmen	jméno	k1gNnPc2	jméno
kromě	kromě	k7c2	kromě
základního	základní	k2eAgInSc2d1	základní
tvaru	tvar	k1gInSc2	tvar
uvádějí	uvádět	k5eAaImIp3nP	uvádět
ještě	ještě	k9	ještě
zakončení	zakončení	k1gNnSc4	zakončení
určitého	určitý	k2eAgInSc2d1	určitý
tvaru	tvar	k1gInSc2	tvar
singuláru	singulár	k1gInSc2	singulár
(	(	kIx(	(
<g/>
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
členem	člen	k1gInSc7	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgMnSc4	který
uživatel	uživatel	k1gMnSc1	uživatel
pozná	poznat	k5eAaPmIp3nS	poznat
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
rodu	rod	k1gInSc3	rod
<g/>
,	,	kIx,	,
a	a	k8xC	a
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
tvary	tvar	k1gInPc1	tvar
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
snadno	snadno	k6eAd1	snadno
odvodí	odvodit	k5eAaPmIp3nP	odvodit
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kvinn	kvinn	k1gInSc1	kvinn
<g/>
|	|	kIx~	|
<g/>
a	a	k8xC	a
-an	n	k?	-an
-or	r	k?	-or
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
kvinnan	kvinnan	k1gInSc1	kvinnan
–	–	k?	–
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
kvinnor	kvinnor	k1gInSc1	kvinnor
–	–	k?	–
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
väg	väg	k?	väg
-en	n	k?	-en
-ar	r	k?	-ar
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
vägen	vägen	k2eAgInSc1d1	vägen
–	–	k?	–
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
vägar	vägar	k1gInSc1	vägar
–	–	k?	–
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hus	husa	k1gFnPc2	husa
-et	t	k?	-et
-	-	kIx~	-
dům	dům	k1gInSc4	dům
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
huset	huset	k1gInSc1	huset
–	–	k?	–
stř	stř	k?	stř
<g/>
.	.	kIx.	.
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
hus	husa	k1gFnPc2	husa
–	–	k?	–
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
äpple	äpple	k1gFnSc1	äpple
-t	-t	k?	-t
-n	-n	k?	-n
jablko	jablko	k1gNnSc4	jablko
(	(	kIx(	(
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
:	:	kIx,	:
äpplet	äpplet	k1gInSc1	äpplet
–	–	k?	–
stř	stř	k?	stř
<g/>
.	.	kIx.	.
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
äpplen	äpplen	k1gInSc1	äpplen
–	–	k?	–
mn	mn	k?	mn
<g/>
.	.	kIx.	.
č.	č.	k?	č.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Mají	mít	k5eAaImIp3nP	mít
dvojí	dvojí	k4xRgInSc4	dvojí
typ	typ	k1gInSc4	typ
skloňování	skloňování	k1gNnSc2	skloňování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
silné	silný	k2eAgFnPc1d1	silná
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
členem	člen	k1gInSc7	člen
určitým	určitý	k2eAgInSc7d1	určitý
<g/>
)	)	kIx)	)
–	–	k?	–
jednotné	jednotný	k2eAgNnSc1d1	jednotné
zakončení	zakončení	k1gNnSc1	zakončení
-a	-a	k?	-a
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
slabé	slabý	k2eAgNnSc1d1	slabé
(	(	kIx(	(
<g/>
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
případech	případ	k1gInPc6	případ
<g/>
)	)	kIx)	)
–	–	k?	–
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
rodě	rod	k1gInSc6	rod
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
přibírá	přibírat	k5eAaImIp3nS	přibírat
koncovku	koncovka	k1gFnSc4	koncovka
-t	-t	k?	-t
<g/>
,	,	kIx,	,
v	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
obou	dva	k4xCgInPc2	dva
rodů	rod	k1gInPc2	rod
-	-	kIx~	-
<g/>
a.	a.	k?	a.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesa	sloveso	k1gNnSc2	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
kategorii	kategorie	k1gFnSc4	kategorie
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc2	způsob
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
nemají	mít	k5eNaImIp3nP	mít
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vyjadřovány	vyjadřovat	k5eAaImNgFnP	vyjadřovat
podmětem	podmět	k1gInSc7	podmět
(	(	kIx(	(
<g/>
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
časování	časování	k1gNnSc2	časování
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
4	[number]	k4	4
tříd	třída	k1gFnPc2	třída
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
jsou	být	k5eAaImIp3nP	být
slabá	slabý	k2eAgNnPc4d1	slabé
slovesa	sloveso	k1gNnPc4	sloveso
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
jsou	být	k5eAaImIp3nP	být
silná	silný	k2eAgNnPc1d1	silné
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
zakončení	zakončení	k1gNnSc1	zakončení
-a	-a	k?	-a
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
končí	končit	k5eAaImIp3nS	končit
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
pojí	pojíst	k5eAaPmIp3nS	pojíst
s	s	k7c7	s
částicí	částice	k1gFnSc7	částice
att.	att.	k?	att.
</s>
</p>
<p>
<s>
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
zakončení	zakončení	k1gNnSc1	zakončení
-r	-r	k?	-r
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
perfekta	perfektum	k1gNnSc2	perfektum
a	a	k8xC	a
plusquamperfekta	plusquamperfektum	k1gNnSc2	plusquamperfektum
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
využívá	využívat	k5eAaPmIp3nS	využívat
supina	supinum	k1gNnSc2	supinum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovosled	slovosled	k1gInSc4	slovosled
===	===	k?	===
</s>
</p>
<p>
<s>
Švédština	švédština	k1gFnSc1	švédština
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
slovosled	slovosled	k1gInSc4	slovosled
typu	typ	k1gInSc2	typ
SVO	SVO	kA	SVO
(	(	kIx(	(
<g/>
podmět	podmět	k1gInSc1	podmět
–	–	k?	–
přísudek	přísudek	k1gInSc1	přísudek
–	–	k?	–
předmět	předmět	k1gInSc1	předmět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
tvoří	tvořit	k5eAaImIp3nP	tvořit
přehozením	přehození	k1gNnSc7	přehození
přísudku	přísudek	k1gInSc2	přísudek
s	s	k7c7	s
podmětem	podmět	k1gInSc7	podmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
švédština	švédština	k1gFnSc1	švédština
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
a	a	k8xC	a
liturgickém	liturgický	k2eAgInSc6d1	liturgický
styku	styk	k1gInSc6	styk
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
donedávna	donedávna	k6eAd1	donedávna
vyžadována	vyžadovat	k5eAaImNgFnS	vyžadovat
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Dílčí	dílčí	k2eAgInPc1d1	dílčí
zákony	zákon	k1gInPc1	zákon
řešící	řešící	k2eAgInPc1d1	řešící
používání	používání	k1gNnSc4	používání
jazyka	jazyk	k1gInSc2	jazyk
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
soudů	soud	k1gInPc2	soud
však	však	k9	však
existovaly	existovat	k5eAaImAgInP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
uzákonit	uzákonit	k5eAaPmF	uzákonit
švédštinu	švédština	k1gFnSc4	švédština
jako	jako	k8xC	jako
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
nikdy	nikdy	k6eAd1	nikdy
nezískaly	získat	k5eNaPmAgFnP	získat
ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
parlamentu	parlament	k1gInSc6	parlament
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
jazykový	jazykový	k2eAgInSc1d1	jazykový
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
švédštinu	švédština	k1gFnSc4	švédština
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
veřejné	veřejný	k2eAgFnPc1d1	veřejná
instituce	instituce	k1gFnPc1	instituce
budou	být	k5eAaImBp3nP	být
švédštinu	švédština	k1gFnSc4	švédština
užívat	užívat	k5eAaImF	užívat
a	a	k8xC	a
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
pobývá	pobývat	k5eAaImIp3nS	pobývat
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zaručenou	zaručený	k2eAgFnSc4d1	zaručená
možnost	možnost	k1gFnSc4	možnost
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
a	a	k8xC	a
používat	používat	k5eAaImF	používat
švédský	švédský	k2eAgInSc4d1	švédský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
zároveň	zároveň	k6eAd1	zároveň
zákonem	zákon	k1gInSc7	zákon
chrání	chránit	k5eAaImIp3nS	chránit
jazyky	jazyk	k1gInPc4	jazyk
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
<g/>
Švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
Å	Å	k?	Å
<g/>
,	,	kIx,	,
samostatné	samostatný	k2eAgFnPc4d1	samostatná
ostrovní	ostrovní	k2eAgFnSc4d1	ostrovní
provincii	provincie	k1gFnSc4	provincie
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
95	[number]	k4	95
%	%	kIx~	%
z	z	k7c2	z
26	[number]	k4	26
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
používá	používat	k5eAaImIp3nS	používat
švédštinu	švédština	k1gFnSc4	švédština
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
je	být	k5eAaImIp3nS	být
švédština	švédština	k1gFnSc1	švédština
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
druhý	druhý	k4xOgInSc4	druhý
národní	národní	k2eAgInSc4d1	národní
jazyk	jazyk	k1gInSc4	jazyk
vedle	vedle	k7c2	vedle
finštiny	finština	k1gFnSc2	finština
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgMnPc1d1	státní
úředníci	úředník	k1gMnPc1	úředník
musejí	muset	k5eAaImIp3nP	muset
ovládat	ovládat	k5eAaImF	ovládat
oba	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
<g/>
Švédština	švédština	k1gFnSc1	švédština
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
oficiálních	oficiální	k2eAgInPc2d1	oficiální
jazyků	jazyk	k1gInPc2	jazyk
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgFnPc1d1	jazyková
instituce	instituce	k1gFnPc1	instituce
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgFnPc4	žádný
oficiální	oficiální	k2eAgFnPc4d1	oficiální
regulační	regulační	k2eAgFnPc4d1	regulační
instituce	instituce	k1gFnPc4	instituce
pro	pro	k7c4	pro
švédský	švédský	k2eAgInSc4d1	švédský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Språ	Språ	k1gFnSc1	Språ
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Svenska	Svensko	k1gNnSc2	Svensko
språ	språ	k?	språ
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
polooficiální	polooficiální	k2eAgInSc4d1	polooficiální
status	status	k1gInSc4	status
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
švédskou	švédský	k2eAgFnSc7d1	švédská
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
rozhodovací	rozhodovací	k2eAgFnPc4d1	rozhodovací
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
mnoha	mnoho	k4c7	mnoho
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
Jazykovou	jazykový	k2eAgFnSc4d1	jazyková
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
Švédská	švédský	k2eAgFnSc1d1	švédská
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1786	[number]	k4	1786
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
slovníky	slovník	k1gInPc4	slovník
Svenska	Svensko	k1gNnSc2	Svensko
Akademiens	Akademiensa	k1gFnPc2	Akademiensa
ordlista	ordlista	k1gMnSc1	ordlista
(	(	kIx(	(
<g/>
Rejstřík	rejstřík	k1gInSc1	rejstřík
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
;	;	kIx,	;
aktualizace	aktualizace	k1gFnSc1	aktualizace
vychází	vycházet	k5eAaImIp3nS	vycházet
každých	každý	k3xTgNnPc2	každý
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Svenska	Svensko	k1gNnSc2	Svensko
Akademiens	Akademiensa	k1gFnPc2	Akademiensa
Ordbok	Ordbok	k1gInSc1	Ordbok
(	(	kIx(	(
<g/>
Slovník	slovník	k1gInSc1	slovník
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
–	–	k?	–
velký	velký	k2eAgInSc1d1	velký
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
postupně	postupně	k6eAd1	postupně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
hesel	heslo	k1gNnPc2	heslo
od	od	k7c2	od
A	A	kA	A
po	po	k7c6	po
T.	T.	kA	T.
Kompletní	kompletní	k2eAgFnSc1d1	kompletní
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
gramatiky	gramatika	k1gFnSc2	gramatika
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
jazykové	jazykový	k2eAgFnSc2d1	jazyková
příručky	příručka	k1gFnSc2	příručka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
slovníky	slovník	k1gInPc1	slovník
používány	používat	k5eAaImNgInP	používat
jako	jako	k9	jako
oficiální	oficiální	k2eAgFnPc1d1	oficiální
jazykové	jazykový	k2eAgFnPc1d1	jazyková
příručky	příručka	k1gFnPc1	příručka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
hlavní	hlavní	k2eAgInSc1d1	hlavní
účel	účel	k1gInSc1	účel
je	být	k5eAaImIp3nS	být
popis	popis	k1gInSc4	popis
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
má	mít	k5eAaImIp3nS	mít
oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
oddělení	oddělení	k1gNnSc4	oddělení
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
domácí	domácí	k2eAgInPc4d1	domácí
jazyky	jazyk	k1gInPc4	jazyk
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
Forskningscentralen	Forskningscentralen	k2eAgInSc1d1	Forskningscentralen
för	för	k?	för
de	de	k?	de
inhemska	inhemsk	k1gInSc2	inhemsk
språ	språ	k?	språ
i	i	k8xC	i
Finland	Finlando	k1gNnPc2	Finlando
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Švédská	švédský	k2eAgFnSc1d1	švédská
jazyková	jazykový	k2eAgFnSc1d1	jazyková
kancelář	kancelář	k1gFnSc1	kancelář
(	(	kIx(	(
<g/>
Svenska	Svensko	k1gNnPc1	Svensko
språ	språ	k?	språ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
regulační	regulační	k2eAgFnSc1d1	regulační
autorita	autorita	k1gFnSc1	autorita
pro	pro	k7c4	pro
švédštinu	švédština	k1gFnSc4	švédština
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
hlavní	hlavní	k2eAgInPc4d1	hlavní
úkoly	úkol	k1gInPc4	úkol
patří	patřit	k5eAaImIp3nS	patřit
udržování	udržování	k1gNnSc1	udržování
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
srozumitelnosti	srozumitelnost	k1gFnSc2	srozumitelnost
finské	finský	k2eAgFnSc2d1	finská
švédštiny	švédština	k1gFnSc2	švédština
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
mluveným	mluvený	k2eAgInSc7d1	mluvený
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Publikovala	publikovat	k5eAaBmAgFnS	publikovat
Finlandssvensk	Finlandssvensk	k1gInSc4	Finlandssvensk
ordbok	ordbok	k1gInSc1	ordbok
(	(	kIx(	(
<g/>
Slovník	slovník	k1gInSc1	slovník
finské	finský	k2eAgFnSc2d1	finská
švédštiny	švédština	k1gFnSc2	švédština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
švédštinou	švédština	k1gFnSc7	švédština
používanou	používaný	k2eAgFnSc7d1	používaná
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Možnosti	možnost	k1gFnSc3	možnost
studia	studio	k1gNnSc2	studio
švédštiny	švédština	k1gFnSc2	švédština
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Švédštinu	švédština	k1gFnSc4	švédština
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgInPc1d1	Karlův
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
Vysoká	vysoká	k1gFnSc1	vysoká
škola	škola	k1gFnSc1	škola
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obory	obora	k1gFnPc1	obora
nejsou	být	k5eNaImIp3nP	být
otvírány	otvírán	k2eAgInPc4d1	otvírán
pravidelně	pravidelně	k6eAd1	pravidelně
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
školy	škola	k1gFnPc1	škola
pořádají	pořádat	k5eAaImIp3nP	pořádat
kurzy	kurz	k1gInPc4	kurz
i	i	k9	i
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
===	===	k?	===
</s>
</p>
<p>
<s>
Krom	krom	k7c2	krom
běžné	běžný	k2eAgFnSc2d1	běžná
výuky	výuka	k1gFnSc2	výuka
švédštiny	švédština	k1gFnSc2	švédština
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc6d1	střední
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
rovněž	rovněž	k9	rovněž
kurzy	kurz	k1gInPc1	kurz
švédštiny	švédština	k1gFnSc2	švédština
pro	pro	k7c4	pro
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
pracovníky	pracovník	k1gMnPc4	pracovník
a	a	k8xC	a
studenty	student	k1gMnPc4	student
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
celodenní	celodenní	k2eAgInPc4d1	celodenní
či	či	k8xC	či
večerní	večerní	k2eAgInPc4d1	večerní
kurzy	kurz	k1gInPc4	kurz
švédštiny	švédština	k1gFnPc1	švédština
přístupné	přístupný	k2eAgFnPc1d1	přístupná
zpravidla	zpravidla	k6eAd1	zpravidla
od	od	k7c2	od
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bezplatné	bezplatný	k2eAgInPc1d1	bezplatný
kurzy	kurz	k1gInPc1	kurz
švédštiny	švédština	k1gFnSc2	švédština
pro	pro	k7c4	pro
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
nabízí	nabízet	k5eAaImIp3nS	nabízet
Svenskundervisning	Svenskundervisning	k1gInSc1	Svenskundervisning
för	för	k?	för
invandrare	invandrar	k1gMnSc5	invandrar
(	(	kIx(	(
<g/>
SFI	SFI	kA	SFI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
placenou	placený	k2eAgFnSc4d1	placená
výuku	výuka	k1gFnSc4	výuka
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
na	na	k7c6	na
institutu	institut	k1gInSc6	institut
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
Folkuniversitetet	Folkuniversiteteta	k1gFnPc2	Folkuniversiteteta
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
většina	většina	k1gFnSc1	většina
švédských	švédský	k2eAgFnPc2d1	švédská
univerzit	univerzita	k1gFnPc2	univerzita
(	(	kIx(	(
<g/>
Göteborgs	Göteborgsa	k1gFnPc2	Göteborgsa
Universitet	Universitet	k1gInSc1	Universitet
<g/>
,	,	kIx,	,
Uppsala	Uppsala	k1gFnSc1	Uppsala
Universitet	Universitet	k1gInSc1	Universitet
<g/>
,	,	kIx,	,
Stockholms	Stockholms	k1gInSc1	Stockholms
Universitet	Universitet	k1gInSc1	Universitet
<g/>
)	)	kIx)	)
nabízí	nabízet	k5eAaImIp3nS	nabízet
studium	studium	k1gNnSc4	studium
švédského	švédský	k2eAgInSc2d1	švédský
jazyka	jazyk	k1gInSc2	jazyk
svým	svůj	k3xOyFgMnPc3	svůj
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
studentům	student	k1gMnPc3	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Běžné	běžný	k2eAgFnPc1d1	běžná
švédské	švédský	k2eAgFnPc1d1	švédská
fráze	fráze	k1gFnPc1	fráze
a	a	k8xC	a
pozdravy	pozdrav	k1gInPc1	pozdrav
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
i	i	k8xC	i
německý	německý	k2eAgInSc1d1	německý
<g/>
,	,	kIx,	,
finský	finský	k2eAgInSc1d1	finský
a	a	k8xC	a
anglický	anglický	k2eAgInSc1d1	anglický
text	text	k1gInSc1	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Swedish	Swedish	k1gInSc4	Swedish
language	language	k1gFnSc2	language
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Svenska	Svensko	k1gNnSc2	Svensko
na	na	k7c6	na
švédské	švédský	k2eAgFnSc6d1	švédská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BERGMAN	Bergman	k1gMnSc1	Bergman
<g/>
,	,	kIx,	,
Nils	Nils	k1gInSc1	Nils
Gösta	Göst	k1gInSc2	Göst
<g/>
.	.	kIx.	.
</s>
<s>
Kortfattad	Kortfattad	k6eAd1	Kortfattad
svensk	svensk	k1gInSc1	svensk
språ	språ	k?	språ
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
:	:	kIx,	:
Prisma	prisma	k1gNnSc1	prisma
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
518	[number]	k4	518
<g/>
-	-	kIx~	-
<g/>
1747	[number]	k4	1747
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BERKOV	BERKOV	kA	BERKOV
<g/>
,	,	kIx,	,
Valerij	Valerij	k1gMnSc1	Valerij
Pavlovič	Pavlovič	k1gMnSc1	Pavlovič
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
germánské	germánský	k2eAgInPc1d1	germánský
jazyky	jazyk	k1gInPc1	jazyk
=	=	kIx~	=
Sovremennyje	Sovremennyje	k1gFnSc1	Sovremennyje
germanskije	germanskít	k5eAaPmIp3nS	germanskít
jazyki	jazyki	k6eAd1	jazyki
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Renata	Renata	k1gFnSc1	Renata
Blatná	blatný	k2eAgFnSc1d1	Blatná
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
268	[number]	k4	268
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOLANDER	BOLANDER	kA	BOLANDER
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Funktionell	Funktionell	k1gInSc1	Funktionell
svensk	svensk	k1gInSc1	svensk
grammatik	grammatika	k1gFnPc2	grammatika
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
:	:	kIx,	:
Liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5054	[number]	k4	5054
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CRYSTAL	CRYSTAL	kA	CRYSTAL
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Penguin	Penguin	k1gMnSc1	Penguin
Dictionary	Dictionara	k1gFnSc2	Dictionara
of	of	k?	of
Language	language	k1gFnSc2	language
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Penguin	Penguin	k1gMnSc1	Penguin
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
51416	[number]	k4	51416
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DAHL	dahl	k1gInSc1	dahl
<g/>
,	,	kIx,	,
Östen	Östen	k1gInSc1	Östen
<g/>
.	.	kIx.	.
</s>
<s>
Språ	Språ	k?	Språ
enhet	enhet	k1gInSc1	enhet
och	och	k0	och
må	må	k?	må
<g/>
.	.	kIx.	.
</s>
<s>
Lund	Lund	k1gMnSc1	Lund
<g/>
:	:	kIx,	:
Studentlitteratur	Studentlitteratura	k1gFnPc2	Studentlitteratura
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1158	[number]	k4	1158
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ENGSTRAND	ENGSTRAND	kA	ENGSTRAND
<g/>
,	,	kIx,	,
Olle	Olle	k1gNnSc2	Olle
<g/>
.	.	kIx.	.
</s>
<s>
Fonetikens	Fonetikens	k6eAd1	Fonetikens
grunder	grunder	k1gInSc1	grunder
<g/>
.	.	kIx.	.
</s>
<s>
Lund	Lund	k1gMnSc1	Lund
<g/>
:	:	kIx,	:
Studentlitteratur	Studentlitteratura	k1gFnPc2	Studentlitteratura
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4238	[number]	k4	4238
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ELERT	ELERT	kA	ELERT
<g/>
,	,	kIx,	,
Claes-Christian	Claes-Christian	k1gInSc1	Claes-Christian
<g/>
.	.	kIx.	.
</s>
<s>
Allmän	Allmän	k1gMnSc1	Allmän
och	och	k0	och
svensk	svensk	k1gInSc4	svensk
fonetik	fonetika	k1gFnPc2	fonetika
<g/>
.	.	kIx.	.
</s>
<s>
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
:	:	kIx,	:
Almqvist	Almqvist	k1gInSc1	Almqvist
&	&	k?	&
Wiksell	Wiksell	k1gInSc1	Wiksell
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GARLÉN	GARLÉN	kA	GARLÉN
<g/>
,	,	kIx,	,
Claes	Claes	k1gInSc1	Claes
<g/>
.	.	kIx.	.
</s>
<s>
Svenskans	Svenskans	k6eAd1	Svenskans
fonologi	fonologi	k6eAd1	fonologi
<g/>
.	.	kIx.	.
</s>
<s>
Lund	Lund	k1gMnSc1	Lund
<g/>
:	:	kIx,	:
Studentlitteratur	Studentlitteratura	k1gFnPc2	Studentlitteratura
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
28151	[number]	k4	28151
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
International	Internationat	k5eAaPmAgMnS	Internationat
Phonetic	Phonetice	k1gFnPc2	Phonetice
Association	Association	k1gInSc1	Association
<g/>
.	.	kIx.	.
</s>
<s>
Handbook	handbook	k1gInSc1	handbook
of	of	k?	of
the	the	k?	the
International	International	k1gFnSc2	International
Phonetic	Phonetice	k1gFnPc2	Phonetice
Association	Association	k1gInSc1	Association
<g/>
:	:	kIx,	:
A	a	k9	a
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Use	usus	k1gInSc5	usus
of	of	k?	of
the	the	k?	the
International	International	k1gFnSc2	International
Phonetic	Phonetice	k1gFnPc2	Phonetice
Alphabet	Alphabet	k1gMnSc1	Alphabet
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
U.K.	U.K.	k1gFnSc1	U.K.
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
63751	[number]	k4	63751
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOTSINAS	KOTSINAS	kA	KOTSINAS
<g/>
,	,	kIx,	,
Ulla-Britt	Ulla-Britt	k2eAgInSc1d1	Ulla-Britt
<g/>
.	.	kIx.	.
</s>
<s>
Ungdomssprå	Ungdomssprå	k?	Ungdomssprå
<g/>
.	.	kIx.	.
</s>
<s>
Uppsala	Uppsala	k1gFnSc1	Uppsala
<g/>
:	:	kIx,	:
Hallgren	Hallgrna	k1gFnPc2	Hallgrna
&	&	k?	&
Fallgren	Fallgrna	k1gFnPc2	Fallgrna
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
7382	[number]	k4	7382
<g/>
-	-	kIx~	-
<g/>
738	[number]	k4	738
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
KORNITZKY	KORNITZKY	kA	KORNITZKY
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gMnSc1	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
Langenscheidts	Langenscheidts	k6eAd1	Langenscheidts
Taschenwörterbuch	Taschenwörterbuch	k1gInSc1	Taschenwörterbuch
der	drát	k5eAaImRp2nS	drát
schwedischen	schwedischen	k1gInSc1	schwedischen
und	und	k?	und
deutschen	deutschen	k2eAgInSc1d1	deutschen
Sprache	Sprache	k1gInSc1	Sprache
=	=	kIx~	=
Langenscheidts	Langenscheidts	k1gInSc1	Langenscheidts
fickordbok	fickordbok	k1gInSc1	fickordbok
över	övera	k1gFnPc2	övera
svenska	svensko	k1gNnSc2	svensko
och	och	k0	och
tyska	tysko	k1gNnPc4	tysko
språ	språ	k?	språ
<g/>
.	.	kIx.	.
</s>
<s>
Neubearb	Neubearb	k1gInSc1	Neubearb
<g/>
.	.	kIx.	.
von	von	k1gInSc1	von
Eleonor	Eleonora	k1gFnPc2	Eleonora
Engbrant-Heider	Engbrant-Heidra	k1gFnPc2	Engbrant-Heidra
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Aufl	Aufl	k1gMnSc1	Aufl
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
[	[	kIx(	[
<g/>
u.a.	u.a.	k?	u.a.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Langenscheidt	Langenscheidt	k1gMnSc1	Langenscheidt
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
468	[number]	k4	468
<g/>
-	-	kIx~	-
<g/>
10301	[number]	k4	10301
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MENCÁK	MENCÁK	kA	MENCÁK
<g/>
,	,	kIx,	,
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
;	;	kIx,	;
FRYDRICH	FRYDRICH	kA	FRYDRICH
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Švédština	švédština	k1gFnSc1	švédština
pro	pro	k7c4	pro
samouky	samouk	k1gMnPc4	samouk
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
24005	[number]	k4	24005
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETTERSSON	PETTERSSON	kA	PETTERSSON
<g/>
,	,	kIx,	,
Gertrud	Gertrud	k1gInSc1	Gertrud
<g/>
.	.	kIx.	.
</s>
<s>
Svenska	Svensko	k1gNnSc2	Svensko
språ	språ	k?	språ
under	undra	k1gFnPc2	undra
sjuhundra	sjuhundra	k1gFnSc1	sjuhundra
å	å	k?	å
en	en	k?	en
historia	historium	k1gNnSc2	historium
om	om	k?	om
svenskan	svenskan	k1gMnSc1	svenskan
och	och	k0	och
dess	dess	k1gInSc1	dess
utforskande	utforskand	k1gInSc5	utforskand
<g/>
.	.	kIx.	.
</s>
<s>
Lund	Lund	k1gMnSc1	Lund
<g/>
:	:	kIx,	:
Studentlitteratur	Studentlitteratura	k1gFnPc2	Studentlitteratura
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
48221	[number]	k4	48221
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVENSSON	SVENSSON	kA	SVENSSON
<g/>
,	,	kIx,	,
Lars	Lars	k1gInSc1	Lars
<g/>
.	.	kIx.	.
</s>
<s>
Nordisk	Nordisk	k1gInSc1	Nordisk
paleografi	paleografi	k6eAd1	paleografi
Handbok	Handbok	k1gInSc1	Handbok
med	med	k1gInSc1	med
transkriberade	transkriberad	k1gInSc5	transkriberad
och	och	k0	och
kommenterade	kommenterad	k1gInSc5	kommenterad
skriftprov	skriftprov	k1gInSc1	skriftprov
<g/>
.	.	kIx.	.
</s>
<s>
Lundastudier	Lundastudier	k1gInSc1	Lundastudier
i	i	k8xC	i
Nordisk	Nordisk	k1gInSc1	Nordisk
Språ	Språ	k1gFnPc2	Språ
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Lund	Lund	k1gMnSc1	Lund
<g/>
:	:	kIx,	:
Studentlitteratur	Studentlitteratura	k1gFnPc2	Studentlitteratura
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
švédština	švédština	k1gFnSc1	švédština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
švédština	švédština	k1gFnSc1	švédština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Švédština	švédština	k1gFnSc1	švédština
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ethnologue	Ethnologue	k1gFnSc1	Ethnologue
report	report	k1gInSc1	report
for	forum	k1gNnPc2	forum
Swedish	Swedish	k1gInSc1	Swedish
–	–	k?	–
švédština	švédština	k1gFnSc1	švédština
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Ethnologue	Ethnologu	k1gFnSc2	Ethnologu
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
Svenska	Svensko	k1gNnPc1	Svensko
Akademiens	Akademiensa	k1gFnPc2	Akademiensa
ordbook	ordbook	k1gInSc1	ordbook
–	–	k?	–
Slovník	slovník	k1gInSc4	slovník
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
on-line	onin	k1gInSc5	on-lin
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
<g/>
)	)	kIx)	)
Institutet	Institutet	k1gInSc1	Institutet
för	för	k?	för
språ	språ	k?	språ
och	och	k0	och
folkminnen	folkminnen	k2eAgInSc1d1	folkminnen
–	–	k?	–
švédský	švédský	k2eAgInSc1d1	švédský
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
folklór	folklór	k1gInSc4	folklór
</s>
</p>
