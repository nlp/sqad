<s>
Byla	být	k5eAaImAgFnS
Edith	Edith	k1gFnSc1
Franková-Holländerová	Franková-Holländerová	k1gFnSc1
matka	matka	k1gFnSc1
Anne	Ann	k1gFnSc2
Frankové	Franková	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
si	se	k3xPyFc3
během	během	k7c2
holokaustu	holokaust	k1gInSc2
psala	psát	k5eAaImAgFnS
deník	deník	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
ji	on	k3xPp3gFnSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
proslavil	proslavit	k5eAaPmAgMnS
<g/>
?	?	kIx.
</s>