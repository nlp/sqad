<s>
Edith	Edith	k1gInSc1
Franková-Holländerová	Franková-Holländerová	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
</s>
<s>
Edith	Edith	k1gInSc1
Franková-Holländerová	Franková-Holländerový	k2eAgFnSc1d1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Edith	Edith	k1gMnSc1
Holländer	Holländer	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1900	#num#	k4
<g/>
Cáchy	Cáchy	k1gFnPc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
44	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Koncentrační	koncentrační	k2eAgInSc1d1
tábor	tábor	k1gInSc1
Osvětim	Osvětim	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
Židé	Žid	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Viktoriaschule	Viktoriaschule	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
žena	žena	k1gFnSc1
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
judaismus	judaismus	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Otto	Otto	k1gMnSc1
Frank	Frank	k1gMnSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Margot	Margot	k1gMnSc1
FrankAnne	FrankAnn	k1gInSc5
Franková	Franková	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Edith	Edith	k1gFnSc1
Franková	Franková	k1gFnSc1
(	(	kIx(
<g/>
rozená	rozený	k2eAgFnSc1d1
Hollanderová	Hollanderová	k1gFnSc1
<g/>
;	;	kIx,
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1900	#num#	k4
Cáchy	Cáchy	k1gFnPc4
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
Osvětim	Osvětim	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
matka	matka	k1gFnSc1
Anne	Ann	k1gFnSc2
Frankové	Franková	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
během	během	k7c2
holokaustu	holokaust	k1gInSc2
psala	psát	k5eAaImAgFnS
deník	deník	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
ji	on	k3xPp3gFnSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
proslavil	proslavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Edith	Edith	k1gInSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
Abrahamu	Abraham	k1gMnSc6
Hollanderovi	Hollander	k1gMnSc6
a	a	k8xC
Rose	Rosa	k1gFnSc6
Hollanderové	Hollanderový	k2eAgFnSc6d1
(	(	kIx(
<g/>
rozené	rozený	k2eAgFnSc6d1
Sternové	Sternová	k1gFnSc6
<g/>
)	)	kIx)
v	v	k7c6
Cáchách	Cáchy	k1gFnPc6
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
svých	svůj	k3xOyFgNnPc2
25	#num#	k4
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
letech	léto	k1gNnPc6
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
Otto	Otto	k1gMnSc1
Franka	Frank	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstěhovali	odstěhovat	k5eAaPmAgMnP
se	se	k3xPyFc4
společně	společně	k6eAd1
do	do	k7c2
Frankfurtu	Frankfurt	k1gInSc2
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodila	narodit	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
Margot	Margota	k1gFnPc2
Betti	Betti	k1gFnSc1
Franková	Franková	k1gFnSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
o	o	k7c4
4	#num#	k4
roky	rok	k1gInPc7
později	pozdě	k6eAd2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1929	#num#	k4
<g/>
)	)	kIx)
narodila	narodit	k5eAaPmAgFnS
2	#num#	k4
a	a	k8xC
poslední	poslední	k2eAgFnSc1d1
dcera	dcera	k1gFnSc1
Annelies	Anneliesa	k1gFnPc2
Marie	Marie	k1gFnSc1
Franková	Franková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
její	její	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
byla	být	k5eAaImAgFnS
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
Ottou	Otta	k1gMnSc7
dozvěděli	dozvědět	k5eAaPmAgMnP
o	o	k7c6
transportech	transport	k1gInPc6
do	do	k7c2
pracovních	pracovní	k2eAgInPc2d1
a	a	k8xC
vyhlazovacích	vyhlazovací	k2eAgInPc2d1
táborů	tábor	k1gInPc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
tam	tam	k6eAd1
ale	ale	k8xC
děje	děj	k1gInSc2
nevěděli	vědět	k5eNaImAgMnP
(	(	kIx(
<g/>
nevěděl	vědět	k5eNaImAgMnS
to	ten	k3xDgNnSc1
nikdo	nikdo	k3yNnSc1
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
samostatné	samostatný	k2eAgMnPc4d1
vězně	vězeň	k1gMnPc4
táborů	tábor	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
Ottem	Ottem	k?
rozhodli	rozhodnout	k5eAaPmAgMnP
utéct	utéct	k5eAaPmF
s	s	k7c7
dcery	dcera	k1gFnSc2
do	do	k7c2
Nizozemí	Nizozemí	k1gNnSc2
<g/>
,	,	kIx,
<g/>
aby	aby	kYmCp3nP
unikli	uniknout	k5eAaPmAgMnP
nacismu	nacismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
se	s	k7c7
<g/>
,	,	kIx,
ale	ale	k8xC
nacismus	nacismus	k1gInSc1
dostal	dostat	k5eAaPmAgInS
i	i	k9
do	do	k7c2
Nizozemí	Nizozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začaly	začít	k5eAaPmAgFnP
povídky	povídka	k1gFnPc1
o	o	k7c6
prvních	první	k4xOgInPc6
transportech	transport	k1gInPc6
do	do	k7c2
táborů	tábor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
manželům	manžel	k1gMnPc3
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gNnSc4
totéž	týž	k3xTgNnSc4
čeká	čekat	k5eAaImIp3nS
<g/>
…	…	k?
Když	když	k8xS
přišla	přijít	k5eAaPmAgFnS
Anne	Anne	k1gFnSc4
s	s	k7c7
Margot	Margot	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
nemohou	moct	k5eNaImIp3nP
chodit	chodit	k5eAaImF
do	do	k7c2
normální	normální	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
Bylo	být	k5eAaImAgNnS
jim	on	k3xPp3gMnPc3
jasné	jasný	k2eAgNnSc1d1
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Museli	muset	k5eAaImAgMnP
se	se	k3xPyFc4
jít	jít	k5eAaImF
nahlásit	nahlásit	k5eAaPmF
na	na	k7c4
speciální	speciální	k2eAgInSc4d1
úřad	úřad	k1gInSc4
pro	pro	k7c4
židy	žid	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
přišlo	přijít	k5eAaPmAgNnS
předvolaní	předvolaný	k2eAgMnPc1d1
pro	pro	k7c4
její	její	k3xOp3gFnSc4
dceru	dcera	k1gFnSc4
Margot	Margota	k1gFnPc2
Frankovou	frankový	k2eAgFnSc4d1
<g/>
,	,	kIx,
s	s	k7c7
Ottem	Ottem	k?
neváhaly	váhat	k5eNaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
skryly	skrýt	k5eAaPmAgFnP
se	se	k3xPyFc4
s	s	k7c7
rodinou	rodina	k1gFnSc7
do	do	k7c2
zadního	zadní	k2eAgInSc2d1
traktu	trakt	k1gInSc2
domů	dům	k1gInPc2
u	u	k7c2
Ottovy	Ottův	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skrývání	skrývání	k1gNnSc1
v	v	k7c6
zadním	zadní	k2eAgInSc6d1
traktu	trakt	k1gInSc6
domu	dům	k1gInSc2
</s>
<s>
Frankovi	Frankův	k2eAgMnPc1d1
se	se	k3xPyFc4
skryli	skrýt	k5eAaPmAgMnP
společně	společně	k6eAd1
s	s	k7c7
rodinou	rodina	k1gFnSc7
Van	van	k1gInSc4
Pelsových	Pelsových	k2eAgInSc4d1
a	a	k8xC
Pffefrem	Pffefr	k1gMnSc7
Fritzem	Fritz	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jejich	jejich	k3xOp3gNnSc6
tajným	tajný	k2eAgInSc7d1
úkrytu	úkryt	k1gInSc2
věděli	vědět	k5eAaImAgMnP
pouze	pouze	k6eAd1
ti	ten	k3xDgMnPc1
nejvěrnější	věrný	k2eAgMnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jejich	jejich	k3xOp3gInSc6
úkrytu	úkryt	k1gInSc6
věděli	vědět	k5eAaImAgMnP
pouze	pouze	k6eAd1
:	:	kIx,
<g/>
Miep	Miep	k1gInSc1
Giesová	Giesová	k1gFnSc1
<g/>
,	,	kIx,
Bep	Bep	k1gFnSc1
Voskuijlová	Voskuijlová	k1gFnSc1
<g/>
,	,	kIx,
Johannes	Johannes	k1gMnSc1
Kleinmann	Kleinmann	k1gMnSc1
<g/>
,	,	kIx,
Viktor	Viktor	k1gMnSc1
Kugler	Kugler	k1gMnSc1
<g/>
,	,	kIx,
Johannes	Johannes	k1gMnSc1
Hendrik	Hendrik	k1gMnSc1
Voskuijl	Voskuijl	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
Jan	Jan	k1gMnSc1
Gies	Giesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
lidé	člověk	k1gMnPc1
je	on	k3xPp3gFnPc4
pomáhali	pomáhat	k5eAaImAgMnP
ukrývat	ukrývat	k5eAaImF
v	v	k7c6
traktu	trakt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1944	#num#	k4
vrazila	vrazit	k5eAaPmAgFnS
do	do	k7c2
zadního	zadní	k2eAgInSc2d1
traktu	trakt	k1gInSc2
Zelená	zelený	k2eAgFnSc1d1
policie	policie	k1gFnSc1
(	(	kIx(
<g/>
gestapo	gestapo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
nechali	nechat	k5eAaPmAgMnP
je	on	k3xPp3gInPc4
převést	převést	k5eAaPmF
do	do	k7c2
tranzitního	tranzitní	k2eAgInSc2d1
tábora	tábor	k1gInSc2
Westerbork	Westerbork	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
je	být	k5eAaImIp3nS
z	z	k7c2
tranzitního	tranzitní	k2eAgInSc2d1
tábora	tábor	k1gInSc2
převezli	převézt	k5eAaPmAgMnP
do	do	k7c2
Osvětimi	Osvětim	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
z	z	k7c2
traktu	trakt	k1gInSc2
selekci	selekce	k1gFnSc4
přežili	přežít	k5eAaPmAgMnP
(	(	kIx(
<g/>
nikdo	nikdo	k3yNnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
nedostal	dostat	k5eNaPmAgMnS
do	do	k7c2
plynové	plynový	k2eAgFnSc2d1
komory	komora	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Margot	Margota	k1gFnPc2
<g/>
,	,	kIx,
Anne	Anne	k1gFnPc2
<g/>
,	,	kIx,
Edith	Editha	k1gFnPc2
a	a	k8xC
Auguste	August	k1gMnSc5
van	vana	k1gFnPc2
Pelsová	Pelsová	k1gFnSc1
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
na	na	k7c4
stranu	strana	k1gFnSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osvětimi	osvěti	k1gFnPc7
vypukla	vypuknout	k5eAaPmAgFnS
nějaká	nějaký	k3yIgFnSc1
epidemie	epidemie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Margot	Margot	k1gInSc1
s	s	k7c7
Anne	Anne	k1gFnSc7
a	a	k8xC
Augustou	Augusta	k1gMnSc7
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
na	na	k7c4
transport	transport	k1gInSc4
do	do	k7c2
koncentračního	koncentrační	k2eAgInSc2d1
tábora	tábor	k1gInSc2
Bergen-Belsen	Bergen-Belsna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zrovna	zrovna	k6eAd1
vypukla	vypuknout	k5eAaPmAgFnS
Tyfová	tyfový	k2eAgFnSc1d1
epidemie	epidemie	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
Anne	Anne	k1gFnSc1
s	s	k7c7
Margot	Margota	k1gFnPc2
a	a	k8xC
Augustou	Augusta	k1gMnSc7
tak	tak	k6eAd1
také	také	k9
tyfu	tyf	k1gInSc2
podlehli	podlehnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edith	Edith	k1gMnSc1
se	se	k3xPyFc4
bohužel	bohužel	k9
na	na	k7c4
transport	transport	k1gInSc4
k	k	k7c3
dcerám	dcera	k1gFnPc3
nedostala	dostat	k5eNaPmAgFnS
<g/>
,	,	kIx,
zemřela	zemřít	k5eAaPmAgFnS
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
na	na	k7c6
vyhladovění	vyhladovění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celé	celý	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
přežil	přežít	k5eAaPmAgMnS
pouze	pouze	k6eAd1
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
přežil	přežít	k5eAaPmAgMnS
„	„	k?
<g/>
pochod	pochod	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
Pffefer	Pffefer	k1gMnSc1
s	s	k7c7
van	van	k1gInSc1
Pelsovými	Pelsová	k1gFnPc7
nepřežili	přežít	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peter	Petra	k1gFnPc2
Van	vana	k1gFnPc2
pels	pels	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
koncentračním	koncentrační	k2eAgInSc6d1
táboře	tábor	k1gInSc6
Mauthausen	Mauthausen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pan	Pan	k1gMnSc1
Hermann	Hermann	k1gMnSc1
van	vana	k1gFnPc2
Pels	Pelsa	k1gFnPc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
plynové	plynový	k2eAgFnSc2d1
komory	komora	k1gFnSc2
asi	asi	k9
měsíc	měsíc	k1gInSc4
po	po	k7c6
příjezdu	příjezd	k1gInSc6
<g/>
…	…	k?
Otto	Otto	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
válce	válka	k1gFnSc6
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
139577920	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
101302956	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Hebraistika	hebraistika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
