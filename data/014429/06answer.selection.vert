<s>
Edith	Edith	k1gFnSc1
Franková	Franková	k1gFnSc1
(	(	kIx(
<g/>
rozená	rozený	k2eAgFnSc1d1
Hollanderová	Hollanderová	k1gFnSc1
<g/>
;	;	kIx,
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1900	#num#	k4
Cáchy	Cáchy	k1gFnPc4
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
Osvětim	Osvětim	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
matka	matka	k1gFnSc1
Anne	Ann	k1gFnSc2
Frankové	Franková	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
během	během	k7c2
holokaustu	holokaust	k1gInSc2
psala	psát	k5eAaImAgFnS
deník	deník	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
ji	on	k3xPp3gFnSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
proslavil	proslavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>