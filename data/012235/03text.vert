<p>
<s>
Usher	Usher	k1gMnSc1	Usher
Raymond	Raymond	k1gMnSc1	Raymond
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Usher	Usher	k1gMnSc1	Usher
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
choreograf	choreograf	k1gMnSc1	choreograf
<g/>
,	,	kIx,	,
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Usher	Usher	k1gMnSc1	Usher
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
během	během	k7c2	během
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahrál	nahrát	k5eAaPmAgMnS	nahrát
hity	hit	k1gInPc7	hit
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
You	You	k1gFnSc7	You
Make	Mak	k1gMnSc2	Mak
Me	Me	k1gMnSc2	Me
Wanna	Wann	k1gMnSc2	Wann
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nice	Nice	k1gFnSc1	Nice
and	and	k?	and
Slow	Slow	k1gFnSc1	Slow
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
My	my	k3xPp1nPc1	my
Way	Way	k1gMnPc1	Way
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
albem	album	k1gNnSc7	album
je	být	k5eAaImIp3nS	být
The	The	k1gFnSc1	The
Confessions	Confessionsa	k1gFnPc2	Confessionsa
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
získalo	získat	k5eAaPmAgNnS	získat
diamantové	diamantový	k2eAgNnSc1d1	diamantové
ocenění	ocenění	k1gNnSc1	ocenění
společnosti	společnost	k1gFnSc2	společnost
RIAA	RIAA	kA	RIAA
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejrychleji	rychle	k6eAd3	rychle
se	s	k7c7	s
prodávaným	prodávaný	k2eAgInSc7d1	prodávaný
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
albem	album	k1gNnSc7	album
<g/>
.	.	kIx.	.
</s>
<s>
Alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
na	na	k7c4	na
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
také	také	k9	také
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
jeho	jeho	k3xOp3gInPc4	jeho
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
hity	hit	k1gInPc4	hit
Yeah	Yeaha	k1gFnPc2	Yeaha
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Lil	lít	k5eAaImAgMnS	lít
Jon	Jon	k1gFnSc4	Jon
a	a	k8xC	a
Ludacris	Ludacris	k1gFnSc4	Ludacris
<g/>
)	)	kIx)	)
a	a	k8xC	a
Burn	Burn	k1gInSc4	Burn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sedminásobným	sedminásobný	k2eAgMnSc7d1	sedminásobný
držitelem	držitel	k1gMnSc7	držitel
prestižní	prestižní	k2eAgFnSc2d1	prestižní
ceny	cena	k1gFnSc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
společností	společnost	k1gFnSc7	společnost
Billboard	billboard	k1gInSc4	billboard
vyhlášen	vyhlášen	k2eAgInSc4d1	vyhlášen
druhým	druhý	k4xOgNnSc7	druhý
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
umělcem	umělec	k1gMnSc7	umělec
dekády	dekáda	k1gFnSc2	dekáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
===	===	k?	===
</s>
</p>
<p>
<s>
2018	[number]	k4	2018
-	-	kIx~	-
A	A	kA	A
(	(	kIx(	(
<g/>
se	s	k7c7	s
Zaytoven	Zaytovna	k1gFnPc2	Zaytovna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
EP	EP	kA	EP
===	===	k?	===
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Versus	versus	k7c1	versus
</s>
</p>
<p>
<s>
===	===	k?	===
Alba	alba	k1gFnSc1	alba
z	z	k7c2	z
živého	živý	k2eAgNnSc2d1	živé
vystoupení	vystoupení	k1gNnSc2	vystoupení
===	===	k?	===
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
-	-	kIx~	-
Usher	Usher	k1gInSc1	Usher
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
-	-	kIx~	-
My	my	k3xPp1nPc1	my
Megamix	Megamix	k1gInSc1	Megamix
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Sex	sex	k1gInSc1	sex
Appeal	Appeal	k1gInSc1	Appeal
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
And	Anda	k1gFnPc2	Anda
the	the	k?	the
Winner	Winner	k1gMnSc1	Winner
Is	Is	k1gMnSc1	Is
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Usher	Usher	k1gInSc1	Usher
and	and	k?	and
Friends	Friends	k1gInSc1	Friends
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Usher	Usher	k1gInSc1	Usher
and	and	k?	and
Friends	Friends	k1gInSc1	Friends
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
My	my	k3xPp1nPc1	my
Way	Way	k1gMnPc1	Way
<g/>
/	/	kIx~	/
<g/>
8701	[number]	k4	8701
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Usher	Usher	k1gInSc1	Usher
and	and	k?	and
Friends	Friends	k1gInSc1	Friends
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1-2	[number]	k4	1-2
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Essential	Essential	k1gMnSc1	Essential
Mixes	Mixes	k1gMnSc1	Mixes
</s>
</p>
<p>
<s>
===	===	k?	===
Úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
singly	singl	k1gInPc1	singl
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
-	-	kIx~	-
The	The	k1gFnSc1	The
Faculty	Facult	k1gInPc4	Facult
/	/	kIx~	/
(	(	kIx(	(
<g/>
Fakulta	fakulta	k1gFnSc1	fakulta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
She	She	k1gFnSc2	She
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
All	All	k1gFnSc7	All
That	That	k1gMnSc1	That
/	/	kIx~	/
(	(	kIx(	(
<g/>
Ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Light	Light	k1gMnSc1	Light
It	It	k1gMnSc1	It
Up	Up	k1gMnSc1	Up
/	/	kIx~	/
(	(	kIx(	(
<g/>
Zákony	zákon	k1gInPc1	zákon
Džungle	džungle	k1gFnSc2	džungle
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
Geppetto	Geppetto	k1gNnSc1	Geppetto
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
-	-	kIx~	-
Texas	Texas	k1gInSc1	Texas
Rangers	Rangers	k1gInSc1	Rangers
/	/	kIx~	/
(	(	kIx(	(
<g/>
Texaští	texaský	k2eAgMnPc1d1	texaský
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
-	-	kIx~	-
Ray	Ray	k1gMnSc1	Ray
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
In	In	k1gFnSc1	In
the	the	k?	the
Mix	mix	k1gInSc1	mix
/	/	kIx~	/
(	(	kIx(	(
<g/>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kole	kolo	k1gNnSc6	kolo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Killers	Killers	k1gInSc1	Killers
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
