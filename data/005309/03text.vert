<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
(	(	kIx(	(
<g/>
ECB	ECB	kA	ECB
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
zemí	zem	k1gFnPc2	zem
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Maastrichtské	maastrichtský	k2eAgFnSc2d1	Maastrichtská
smlouvy	smlouva	k1gFnSc2	smlouva
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1998	[number]	k4	1998
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
udržovat	udržovat	k5eAaImF	udržovat
stabilitu	stabilita	k1gFnSc4	stabilita
cen	cena	k1gFnPc2	cena
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
euro	euro	k1gNnSc4	euro
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
řídit	řídit	k5eAaImF	řídit
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Frankfurt	Frankfurt	k1gInSc4	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
provádí	provádět	k5eAaImIp3nS	provádět
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
devatenácti	devatenáct	k4xCc6	devatenáct
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jako	jako	k9	jako
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
přijaly	přijmout	k5eAaPmAgInP	přijmout
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
národními	národní	k2eAgFnPc7d1	národní
centrálními	centrální	k2eAgFnPc7d1	centrální
bankami	banka	k1gFnPc7	banka
všech	všecek	k3xTgFnPc2	všecek
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
tvoří	tvořit	k5eAaImIp3nS	tvořit
Evropský	evropský	k2eAgInSc1d1	evropský
systém	systém	k1gInSc1	systém
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
(	(	kIx(	(
<g/>
ESCB	ESCB	kA	ESCB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
mezi	mezi	k7c4	mezi
ESCB	ESCB	kA	ESCB
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
eurosystémem	eurosystém	k1gInSc7	eurosystém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
ECB	ECB	kA	ECB
a	a	k8xC	a
národními	národní	k2eAgFnPc7d1	národní
centrálními	centrální	k2eAgFnPc7d1	centrální
bankami	banka	k1gFnPc7	banka
těch	ten	k3xDgMnPc2	ten
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
přijaly	přijmout	k5eAaPmAgInP	přijmout
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ESCB	ESCB	kA	ESCB
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
Evropského	evropský	k2eAgInSc2d1	evropský
měnového	měnový	k2eAgInSc2d1	měnový
institutu	institut	k1gInSc2	institut
(	(	kIx(	(
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
etapu	etapa	k1gFnSc4	etapa
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
Evropské	evropský	k2eAgFnSc2d1	Evropská
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EMU	emu	k1gMnSc1	emu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
EMI	EMI	kA	EMI
bylo	být	k5eAaImAgNnS	být
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
centrálními	centrální	k2eAgFnPc7d1	centrální
bankami	banka	k1gFnPc7	banka
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
připravit	připravit	k5eAaPmF	připravit
je	on	k3xPp3gMnPc4	on
na	na	k7c4	na
přijetí	přijetí	k1gNnSc4	přijetí
eura	euro	k1gNnSc2	euro
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
Evropského	evropský	k2eAgInSc2d1	evropský
systému	systém	k1gInSc2	systém
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
(	(	kIx(	(
<g/>
ESCB	ESCB	kA	ESCB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1998	[number]	k4	1998
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Maastrichtské	maastrichtský	k2eAgFnSc2d1	Maastrichtská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
nahradila	nahradit	k5eAaPmAgFnS	nahradit
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
za	za	k7c4	za
provádění	provádění	k1gNnSc4	provádění
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
eurozóně	eurozóna	k1gFnSc6	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
byl	být	k5eAaImAgMnS	být
Wim	Wim	k1gMnSc1	Wim
Duisenberg	Duisenberg	k1gMnSc1	Duisenberg
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezident	prezident	k1gMnSc1	prezident
holandské	holandský	k2eAgFnSc2d1	holandská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
Evropského	evropský	k2eAgInSc2d1	evropský
měnového	měnový	k2eAgInSc2d1	měnový
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Duisenberg	Duisenberg	k1gInSc1	Duisenberg
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
na	na	k7c4	na
post	post	k1gInSc4	post
prvního	první	k4xOgMnSc2	první
prezidenta	prezident	k1gMnSc2	prezident
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Tricheta	Trichet	k2eAgMnSc4d1	Trichet
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc4d1	bývalý
guvernéra	guvernér	k1gMnSc4	guvernér
francouzské	francouzský	k2eAgFnSc2d1	francouzská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
ovšem	ovšem	k9	ovšem
nesouhlasily	souhlasit	k5eNaImAgFnP	souhlasit
vlády	vláda	k1gFnPc1	vláda
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
byl	být	k5eAaImAgInS	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
dohodou	dohoda	k1gFnSc7	dohoda
mezi	mezi	k7c7	mezi
zmíněnými	zmíněný	k2eAgFnPc7d1	zmíněná
vládami	vláda	k1gFnPc7	vláda
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvním	první	k4xOgInSc7	první
prezidentem	prezident	k1gMnSc7	prezident
bude	být	k5eAaImBp3nS	být
Duisenberg	Duisenberg	k1gMnSc1	Duisenberg
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Trichet	Trichet	k1gInSc1	Trichet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
tak	tak	k9	tak
skutečně	skutečně	k6eAd1	skutečně
stalo	stát	k5eAaPmAgNnS	stát
a	a	k8xC	a
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Trichet	Trichet	k1gMnSc1	Trichet
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
druhým	druhý	k4xOgMnSc7	druhý
prezidentem	prezident	k1gMnSc7	prezident
ECB	ECB	kA	ECB
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
nahradil	nahradit	k5eAaPmAgInS	nahradit
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Tricheta	Trichet	k2eAgNnPc4d1	Trichet
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
funkci	funkce	k1gFnSc6	funkce
Mario	Mario	k1gMnSc1	Mario
Draghi	Dragh	k1gFnSc2	Dragh
<g/>
,	,	kIx,	,
současný	současný	k2eAgMnSc1d1	současný
prezident	prezident	k1gMnSc1	prezident
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
měla	mít	k5eAaImAgFnS	mít
eurozóna	eurozóna	k1gFnSc1	eurozóna
celkem	celkem	k6eAd1	celkem
jedenáct	jedenáct	k4xCc1	jedenáct
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Řecko	Řecko	k1gNnSc1	Řecko
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Kypr	Kypr	k1gInSc1	Kypr
a	a	k8xC	a
Malta	Malta	k1gFnSc1	Malta
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
a	a	k8xC	a
Litva	Litva	k1gFnSc1	Litva
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
získala	získat	k5eAaPmAgFnS	získat
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
13	[number]	k4	13
oficiální	oficiální	k2eAgInSc4d1	oficiální
status	status	k1gInSc4	status
orgánu	orgán	k1gInSc2	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
změnila	změnit	k5eAaPmAgFnS	změnit
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
organizace	organizace	k1gFnSc2	organizace
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
3	[number]	k4	3
hlavní	hlavní	k2eAgFnPc1d1	hlavní
rozhodovací	rozhodovací	k2eAgFnPc1d1	rozhodovací
orgány	orgány	k1gFnPc1	orgány
<g/>
:	:	kIx,	:
rada	rada	k1gFnSc1	rada
guvernérů	guvernér	k1gMnPc2	guvernér
<g/>
,	,	kIx,	,
výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
generální	generální	k2eAgFnSc1d1	generální
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
ECB	ECB	kA	ECB
provádí	provádět	k5eAaImIp3nS	provádět
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
instrukcemi	instrukce	k1gFnPc7	instrukce
a	a	k8xC	a
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
Generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
zasedání	zasedání	k1gNnSc1	zasedání
Generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
předsedá	předsedat	k5eAaImIp3nS	předsedat
Radě	rada	k1gFnSc3	rada
guvernérů	guvernér	k1gMnPc2	guvernér
<g/>
,	,	kIx,	,
Výkonné	výkonný	k2eAgFnSc6d1	výkonná
radě	rada	k1gFnSc6	rada
i	i	k8xC	i
Generální	generální	k2eAgFnSc6d1	generální
radě	rada	k1gFnSc6	rada
ECB	ECB	kA	ECB
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
ECB	ECB	kA	ECB
na	na	k7c6	na
evropských	evropský	k2eAgFnPc6d1	Evropská
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgNnPc6d1	mezinárodní
zasedáních	zasedání	k1gNnPc6	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
guvernéru	guvernér	k1gMnSc3	guvernér
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
rozhodovacím	rozhodovací	k2eAgInSc7d1	rozhodovací
orgánem	orgán	k1gInSc7	orgán
ECB	ECB	kA	ECB
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
Výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
guvernérů	guvernér	k1gMnPc2	guvernér
národních	národní	k2eAgFnPc2d1	národní
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
států	stát	k1gInPc2	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
úkoly	úkol	k1gInPc4	úkol
rady	rada	k1gFnSc2	rada
guvernéru	guvernér	k1gMnSc3	guvernér
patří	patřit	k5eAaImIp3nS	patřit
přijímání	přijímání	k1gNnSc4	přijímání
obecných	obecný	k2eAgFnPc2d1	obecná
zásad	zásada	k1gFnPc2	zásada
a	a	k8xC	a
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
nezbytných	zbytný	k2eNgNnPc2d1	nezbytné
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
byla	být	k5eAaImAgFnS	být
ECB	ECB	kA	ECB
pověřena	pověřit	k5eAaPmNgFnS	pověřit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
formulují	formulovat	k5eAaImIp3nP	formulovat
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
střednědobých	střednědobý	k2eAgInPc2d1	střednědobý
měnových	měnový	k2eAgInPc2d1	měnový
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
a	a	k8xC	a
rezerv	rezerva	k1gFnPc2	rezerva
a	a	k8xC	a
výše	výše	k1gFnSc2	výše
rezerv	rezerva	k1gFnPc2	rezerva
v	v	k7c6	v
ESCB	ESCB	kA	ESCB
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
úkoly	úkol	k1gInPc4	úkol
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
přijímání	přijímání	k1gNnSc4	přijímání
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
souvisejících	související	k2eAgMnPc2d1	související
s	s	k7c7	s
všeobecným	všeobecný	k2eAgInSc7d1	všeobecný
rámcem	rámec	k1gInSc7	rámec
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
dohledu	dohled	k1gInSc2	dohled
přijímána	přijímán	k2eAgFnSc1d1	přijímána
<g/>
,	,	kIx,	,
a	a	k8xC	a
přijímání	přijímání	k1gNnSc1	přijímání
úplných	úplný	k2eAgInPc2d1	úplný
návrhů	návrh	k1gInPc2	návrh
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
navrhovaných	navrhovaný	k2eAgFnPc2d1	navrhovaná
Radou	rada	k1gFnSc7	rada
dohledu	dohled	k1gInSc2	dohled
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
postupu	postup	k1gInSc2	postup
neuplatnění	neuplatnění	k1gNnSc2	neuplatnění
námitek	námitka	k1gFnPc2	námitka
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
guvernérů	guvernér	k1gMnPc2	guvernér
zasedá	zasedat	k5eAaImIp3nS	zasedat
dvakrát	dvakrát	k6eAd1	dvakrát
měsíčně	měsíčně	k6eAd1	měsíčně
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Vyhodnocování	vyhodnocování	k1gNnSc1	vyhodnocování
vývoje	vývoj	k1gInSc2	vývoj
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
měnové	měnový	k2eAgFnSc2d1	měnová
situace	situace	k1gFnSc2	situace
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přijímáním	přijímání	k1gNnSc7	přijímání
měnových	měnový	k2eAgNnPc2d1	měnové
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
provádí	provádět	k5eAaImIp3nS	provádět
každých	každý	k3xTgNnPc2	každý
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostatních	ostatní	k2eAgNnPc6d1	ostatní
zasedáních	zasedání	k1gNnPc6	zasedání
se	se	k3xPyFc4	se
Rada	rada	k1gFnSc1	rada
guvernérů	guvernér	k1gMnPc2	guvernér
zabývá	zabývat	k5eAaImIp3nS	zabývat
dalšími	další	k2eAgFnPc7d1	další
otázkami	otázka	k1gFnPc7	otázka
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
úkolů	úkol	k1gInPc2	úkol
a	a	k8xC	a
činností	činnost	k1gFnSc7	činnost
ECB	ECB	kA	ECB
a	a	k8xC	a
Eurosystému	Eurosystý	k2eAgMnSc3d1	Eurosystý
<g/>
.	.	kIx.	.
</s>
<s>
Příslušná	příslušný	k2eAgNnPc1d1	příslušné
jednání	jednání	k1gNnPc1	jednání
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
odděleně	odděleně	k6eAd1	odděleně
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
oddělení	oddělení	k1gNnSc2	oddělení
úkolů	úkol	k1gInPc2	úkol
ECB	ECB	kA	ECB
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
souvisejících	související	k2eAgInPc2d1	související
úkolů	úkol	k1gInPc2	úkol
ECB	ECB	kA	ECB
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dohledu	dohled	k1gInSc2	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Měnová	měnový	k2eAgNnPc1d1	měnové
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
se	se	k3xPyFc4	se
podrobněji	podrobně	k6eAd2	podrobně
probírají	probírat	k5eAaImIp3nP	probírat
a	a	k8xC	a
zdůvodňují	zdůvodňovat	k5eAaImIp3nP	zdůvodňovat
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Tiskové	tiskový	k2eAgFnSc3d1	tisková
konferenci	konference	k1gFnSc3	konference
předsedá	předsedat	k5eAaImIp3nS	předsedat
prezident	prezident	k1gMnSc1	prezident
ECB	ECB	kA	ECB
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
asistuje	asistovat	k5eAaImIp3nS	asistovat
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
ECB	ECB	kA	ECB
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
záznamy	záznam	k1gInPc1	záznam
ze	z	k7c2	z
zasedání	zasedání	k1gNnSc2	zasedání
před	před	k7c7	před
datem	datum	k1gNnSc7	datum
dalšího	další	k2eAgNnSc2d1	další
zasedání	zasedání	k1gNnSc2	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
běžným	běžný	k2eAgInSc7d1	běžný
chodem	chod	k1gInSc7	chod
ECB	ECB	kA	ECB
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
viceprezident	viceprezident	k1gMnSc1	viceprezident
a	a	k8xC	a
4	[number]	k4	4
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
jmenování	jmenování	k1gNnSc4	jmenování
Evropskou	evropský	k2eAgFnSc7d1	Evropská
radou	rada	k1gFnSc7	rada
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
přijatého	přijatý	k2eAgInSc2d1	přijatý
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
většinou	většina	k1gFnSc7	většina
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
výborů	výbor	k1gInPc2	výbor
jsou	být	k5eAaImIp3nP	být
vybírání	vybírání	k1gNnSc4	vybírání
z	z	k7c2	z
uznávaných	uznávaný	k2eAgFnPc2d1	uznávaná
osobností	osobnost	k1gFnPc2	osobnost
se	s	k7c7	s
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
v	v	k7c6	v
měnové	měnový	k2eAgFnSc6d1	měnová
nebo	nebo	k8xC	nebo
bankovní	bankovní	k2eAgFnSc6d1	bankovní
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
členů	člen	k1gMnPc2	člen
je	být	k5eAaImIp3nS	být
osmileté	osmiletý	k2eAgNnSc1d1	osmileté
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
opakování	opakování	k1gNnSc2	opakování
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
státní	státní	k2eAgMnPc1d1	státní
příslušníci	příslušník	k1gMnPc1	příslušník
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
řízení	řízení	k1gNnSc1	řízení
běžných	běžný	k2eAgFnPc2d1	běžná
činností	činnost	k1gFnPc2	činnost
ECB	ECB	kA	ECB
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
zasedání	zasedání	k1gNnSc2	zasedání
Rady	rada	k1gFnSc2	rada
guvernérů	guvernér	k1gMnPc2	guvernér
<g/>
,	,	kIx,	,
provádění	provádění	k1gNnSc4	provádění
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
eurozóně	eurozóna	k1gFnSc6	eurozóna
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Radou	rada	k1gFnSc7	rada
guvernérů	guvernér	k1gMnPc2	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
výbor	výbor	k1gInSc4	výbor
také	také	k9	také
za	za	k7c4	za
vykonávání	vykonávání	k1gNnSc4	vykonávání
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jim	on	k3xPp3gMnPc3	on
svěří	svěřit	k5eAaPmIp3nS	svěřit
Rada	rada	k1gFnSc1	rada
guvernérů	guvernér	k1gMnPc2	guvernér
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
patří	patřit	k5eAaImIp3nS	patřit
částečně	částečně	k6eAd1	částečně
také	také	k9	také
regulatorní	regulatorní	k2eAgFnSc4d1	regulatorní
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgFnSc4d1	generální
radu	rada	k1gFnSc4	rada
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
rozhodovací	rozhodovací	k2eAgInSc4d1	rozhodovací
orgán	orgán	k1gInSc4	orgán
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
na	na	k7c4	na
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Plní	plnit	k5eAaImIp3nP	plnit
úkoly	úkol	k1gInPc1	úkol
převzaté	převzatý	k2eAgInPc1d1	převzatý
od	od	k7c2	od
Evropského	evropský	k2eAgInSc2d1	evropský
měnového	měnový	k2eAgInSc2d1	měnový
institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
ECB	ECB	kA	ECB
plnit	plnit	k5eAaImF	plnit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
třetí	třetí	k4xOgFnSc2	třetí
etapy	etapa	k1gFnSc2	etapa
Hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
existují	existovat	k5eAaImIp3nP	existovat
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
EU	EU	kA	EU
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nezavedly	zavést	k5eNaPmAgFnP	zavést
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgFnSc1d1	generální
rada	rada	k1gFnSc1	rada
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
statutem	statut	k1gInSc7	statut
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
všechny	všechen	k3xTgInPc1	všechen
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
EU	EU	kA	EU
zavedou	zavést	k5eAaPmIp3nP	zavést
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
a	a	k8xC	a
všech	všecek	k3xTgMnPc2	všecek
guvernérů	guvernér	k1gMnPc2	guvernér
národních	národní	k2eAgFnPc2d1	národní
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nevstoupili	vstoupit	k5eNaPmAgMnP	vstoupit
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
funkce	funkce	k1gFnPc4	funkce
generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
patří	patřit	k5eAaImIp3nS	patřit
podílení	podílení	k1gNnSc4	podílení
se	se	k3xPyFc4	se
na	na	k7c6	na
poradních	poradní	k2eAgFnPc6d1	poradní
funkcích	funkce	k1gFnPc6	funkce
ESCB	ESCB	kA	ESCB
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
čtvrtletních	čtvrtletní	k2eAgFnPc2d1	čtvrtletní
a	a	k8xC	a
výročních	výroční	k2eAgFnPc2d1	výroční
zpráv	zpráva	k1gFnPc2	zpráva
ECB	ECB	kA	ECB
<g/>
,	,	kIx,	,
týdenních	týdenní	k2eAgInPc2d1	týdenní
konsolidovaných	konsolidovaný	k2eAgInPc2d1	konsolidovaný
finančních	finanční	k2eAgInPc2d1	finanční
výkazů	výkaz	k1gInPc2	výkaz
<g/>
,	,	kIx,	,
stanovení	stanovení	k1gNnSc1	stanovení
pracovních	pracovní	k2eAgFnPc2d1	pracovní
podmínek	podmínka	k1gFnPc2	podmínka
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ECB	ECB	kA	ECB
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
také	také	k9	také
přípravami	příprava	k1gFnPc7	příprava
k	k	k7c3	k
zafixování	zafixování	k1gNnSc3	zafixování
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
vůči	vůči	k7c3	vůči
euru	euro	k1gNnSc3	euro
u	u	k7c2	u
takových	takový	k3xDgFnPc2	takový
měn	měna	k1gFnPc2	měna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Generální	generální	k2eAgFnSc6d1	generální
radě	rada	k1gFnSc6	rada
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
19	[number]	k4	19
států	stát	k1gInPc2	stát
eurozóny	eurozóna	k1gFnSc2	eurozóna
a	a	k8xC	a
9	[number]	k4	9
států	stát	k1gInPc2	stát
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
eurozónu	eurozóna	k1gFnSc4	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
Generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
účastnit	účastnit	k5eAaImF	účastnit
také	také	k9	také
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
Výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
ECB	ECB	kA	ECB
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Rady	rada	k1gFnSc2	rada
EU	EU	kA	EU
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
člen	člen	k1gMnSc1	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
však	však	k9	však
právo	právo	k1gNnSc4	právo
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
uváděným	uváděný	k2eAgInSc7d1	uváděný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
také	také	k9	také
rada	rada	k1gFnSc1	rada
dohledu	dohled	k1gInSc2	dohled
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
projednávat	projednávat	k5eAaImF	projednávat
<g/>
,	,	kIx,	,
plánovat	plánovat	k5eAaImF	plánovat
a	a	k8xC	a
plnit	plnit	k5eAaImF	plnit
úkoly	úkol	k1gInPc7	úkol
ECB	ECB	kA	ECB
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
dohledu	dohled	k1gInSc2	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
předsedy	předseda	k1gMnSc2	předseda
<g/>
,	,	kIx,	,
místopředsedy	místopředseda	k1gMnPc7	místopředseda
(	(	kIx(	(
<g/>
vybraného	vybraný	k2eAgNnSc2d1	vybrané
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
ECB	ECB	kA	ECB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtyř	čtyři	k4xCgMnPc2	čtyři
zástupců	zástupce	k1gMnPc2	zástupce
ECB	ECB	kA	ECB
a	a	k8xC	a
zástupců	zástupce	k1gMnPc2	zástupce
vnitrostátních	vnitrostátní	k2eAgInPc2d1	vnitrostátní
orgánů	orgán	k1gInPc2	orgán
dohledu	dohled	k1gInSc2	dohled
<g/>
.	.	kIx.	.
</s>
<s>
Zasedání	zasedání	k1gNnSc1	zasedání
rady	rada	k1gFnSc2	rada
dohledu	dohled	k1gInSc2	dohled
probíhají	probíhat	k5eAaImIp3nP	probíhat
dvakrát	dvakrát	k6eAd1	dvakrát
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
</s>
<s>
Činnosti	činnost	k1gFnPc4	činnost
Rady	rada	k1gFnSc2	rada
podporuje	podporovat	k5eAaImIp3nS	podporovat
řídící	řídící	k2eAgInSc1d1	řídící
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připravuje	připravovat	k5eAaImIp3nS	připravovat
zasedání	zasedání	k1gNnSc4	zasedání
<g/>
.	.	kIx.	.
</s>
<s>
Úkoly	úkol	k1gInPc1	úkol
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
úkoly	úkol	k1gInPc4	úkol
ECB	ECB	kA	ECB
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
Vymezení	vymezení	k1gNnSc4	vymezení
a	a	k8xC	a
provádění	provádění	k1gNnSc4	provádění
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
eurozóny	eurozóna	k1gFnSc2	eurozóna
–	–	k?	–
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
provádí	provádět	k5eAaImIp3nS	provádět
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svých	svůj	k3xOyFgInPc2	svůj
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
operací	operace	k1gFnPc2	operace
na	na	k7c6	na
volném	volný	k2eAgInSc6d1	volný
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
určování	určování	k1gNnSc3	určování
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
a	a	k8xC	a
povinných	povinný	k2eAgFnPc2d1	povinná
minimálních	minimální	k2eAgFnPc2d1	minimální
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
využívá	využívat	k5eAaImIp3nS	využívat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
prvořadého	prvořadý	k2eAgInSc2d1	prvořadý
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
cenová	cenový	k2eAgFnSc1d1	cenová
stabilita	stabilita	k1gFnSc1	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Provádění	provádění	k1gNnSc1	provádění
devizových	devizový	k2eAgFnPc2d1	devizová
operací	operace	k1gFnPc2	operace
–	–	k?	–
mezi	mezi	k7c4	mezi
devizové	devizový	k2eAgFnPc4d1	devizová
operace	operace	k1gFnPc4	operace
patří	patřit	k5eAaImIp3nS	patřit
devizové	devizový	k2eAgFnPc4d1	devizová
intervence	intervence	k1gFnPc4	intervence
a	a	k8xC	a
operace	operace	k1gFnPc4	operace
jako	jako	k8xS	jako
prodej	prodej	k1gInSc4	prodej
úrokových	úrokový	k2eAgInPc2d1	úrokový
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
a	a	k8xC	a
"	"	kIx"	"
<g/>
obchodní	obchodní	k2eAgFnSc1d1	obchodní
transakce	transakce	k1gFnSc1	transakce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Držba	držba	k1gFnSc1	držba
a	a	k8xC	a
správa	správa	k1gFnSc1	správa
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
zemí	zem	k1gFnPc2	zem
eurozóny	eurozóna	k1gFnSc2	eurozóna
(	(	kIx(	(
<g/>
správa	správa	k1gFnSc1	správa
portfolií	portfolio	k1gNnPc2	portfolio
<g/>
)	)	kIx)	)
–	–	k?	–
ECB	ECB	kA	ECB
spravuje	spravovat	k5eAaImIp3nS	spravovat
devizové	devizový	k2eAgFnPc4d1	devizová
rezervy	rezerva	k1gFnPc4	rezerva
eurozóny	eurozóna	k1gFnSc2	eurozóna
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
nákup	nákup	k1gInSc4	nákup
či	či	k8xC	či
prodej	prodej	k1gInSc4	prodej
měn	měna	k1gFnPc2	měna
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
stability	stabilita	k1gFnSc2	stabilita
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Devizové	devizový	k2eAgFnPc1d1	devizová
rezervy	rezerva	k1gFnPc1	rezerva
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ECB	ECB	kA	ECB
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
dostatek	dostatek	k1gInSc1	dostatek
likvidních	likvidní	k2eAgInPc2d1	likvidní
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
devizových	devizový	k2eAgFnPc2d1	devizová
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Portfolio	portfolio	k1gNnSc1	portfolio
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
japonských	japonský	k2eAgInPc2d1	japonský
jenů	jen	k1gInPc2	jen
a	a	k8xC	a
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
práv	právo	k1gNnPc2	právo
čerpání	čerpání	k1gNnSc2	čerpání
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
devizových	devizový	k2eAgFnPc2d1	devizová
rezerv	rezerva	k1gFnPc2	rezerva
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
tržní	tržní	k2eAgFnSc2d1	tržní
hodnoty	hodnota	k1gFnSc2	hodnota
investovaných	investovaný	k2eAgNnPc2d1	investované
aktiv	aktivum	k1gNnPc2	aktivum
i	i	k8xC	i
operací	operace	k1gFnSc7	operace
ECB	ECB	kA	ECB
s	s	k7c7	s
devizami	deviza	k1gFnPc7	deviza
a	a	k8xC	a
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
plynulého	plynulý	k2eAgNnSc2d1	plynulé
fungování	fungování	k1gNnSc2	fungování
platebních	platební	k2eAgInPc2d1	platební
systémů	systém	k1gInPc2	systém
–	–	k?	–
platební	platební	k2eAgInSc1d1	platební
systém	systém	k1gInSc1	systém
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
převodu	převod	k1gInSc3	převod
peněz	peníze	k1gInPc2	peníze
uvnitř	uvnitř	k7c2	uvnitř
bankovního	bankovní	k2eAgInSc2d1	bankovní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgInPc2d1	uvedený
základních	základní	k2eAgInPc2d1	základní
úkolů	úkol	k1gInPc2	úkol
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
ECB	ECB	kA	ECB
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
úkoly	úkol	k1gInPc1	úkol
<g/>
:	:	kIx,	:
Bankovky	bankovka	k1gFnPc1	bankovka
–	–	k?	–
ECB	ECB	kA	ECB
má	mít	k5eAaImIp3nS	mít
výlučné	výlučný	k2eAgNnSc4d1	výlučné
právo	právo	k1gNnSc4	právo
povolovat	povolovat	k5eAaImF	povolovat
zemím	zem	k1gFnPc3	zem
eurozóny	eurozóna	k1gFnSc2	eurozóna
tisknout	tisknout	k5eAaImF	tisknout
eurobankovky	eurobankovka	k1gFnPc4	eurobankovka
<g/>
.	.	kIx.	.
</s>
<s>
Statistika	statistika	k1gFnSc1	statistika
–	–	k?	–
ECB	ECB	kA	ECB
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
národními	národní	k2eAgFnPc7d1	národní
centrálními	centrální	k2eAgFnPc7d1	centrální
bankami	banka	k1gFnPc7	banka
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
statistické	statistický	k2eAgFnPc4d1	statistická
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
úkolů	úkol	k1gInPc2	úkol
ESCB	ESCB	kA	ESCB
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc1d1	finanční
stabilita	stabilita	k1gFnSc1	stabilita
a	a	k8xC	a
dohled	dohled	k1gInSc1	dohled
–	–	k?	–
provádí	provádět	k5eAaImIp3nS	provádět
obezřetný	obezřetný	k2eAgInSc4d1	obezřetný
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
úvěrovými	úvěrový	k2eAgFnPc7d1	úvěrová
institucemi	instituce	k1gFnPc7	instituce
a	a	k8xC	a
stabilitou	stabilita	k1gFnSc7	stabilita
finančního	finanční	k2eAgInSc2d1	finanční
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
spolupráce	spolupráce	k1gFnSc2	spolupráce
–	–	k?	–
ECB	ECB	kA	ECB
udržuje	udržovat	k5eAaImIp3nS	udržovat
pracovní	pracovní	k2eAgInPc4d1	pracovní
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
institucemi	instituce	k1gFnPc7	instituce
a	a	k8xC	a
fóry	fórum	k1gNnPc7	fórum
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
i	i	k8xC	i
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
plnění	plnění	k1gNnSc2	plnění
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
Eurosystému	Eurosystý	k2eAgMnSc3d1	Eurosystý
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Evropské	evropský	k2eAgFnSc2d1	Evropská
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
principů	princip	k1gInPc2	princip
její	její	k3xOp3gFnSc2	její
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nezbytným	nezbytný	k2eAgInSc7d1	nezbytný
předpokladem	předpoklad	k1gInSc7	předpoklad
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
provádění	provádění	k1gNnSc2	provádění
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Instituce	instituce	k1gFnSc1	instituce
ECB	ECB	kA	ECB
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
německé	německý	k2eAgFnSc2d1	německá
Bundesbanky	Bundesbanka	k1gFnSc2	Bundesbanka
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
má	mít	k5eAaImIp3nS	mít
zabránit	zabránit	k5eAaPmF	zabránit
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
společné	společný	k2eAgFnSc2d1	společná
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
jakýmikoli	jakýkoli	k3yIgInPc7	jakýkoli
vnějšími	vnější	k2eAgInPc7d1	vnější
vlivy	vliv	k1gInPc7	vliv
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jinými	jiný	k2eAgInPc7d1	jiný
orgány	orgán	k1gInPc7	orgán
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
nebo	nebo	k8xC	nebo
samotné	samotný	k2eAgFnSc2d1	samotná
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
ECB	ECB	kA	ECB
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
všech	všecek	k3xTgFnPc2	všecek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
tvořící	tvořící	k2eAgFnSc2d1	tvořící
Eurosystém	Eurosystý	k2eAgNnSc6d1	Eurosystý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
nezávislostí	nezávislost	k1gFnSc7	nezávislost
institucionální	institucionální	k2eAgFnSc7d1	institucionální
<g/>
,	,	kIx,	,
personální	personální	k2eAgFnSc7d1	personální
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc7d1	finanční
a	a	k8xC	a
funkční	funkční	k2eAgFnSc7d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgFnSc1d1	uvedená
forma	forma	k1gFnSc1	forma
nezávislosti	nezávislost	k1gFnSc2	nezávislost
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
pravomocí	pravomoc	k1gFnPc2	pravomoc
a	a	k8xC	a
plnění	plnění	k1gNnSc6	plnění
úkolů	úkol	k1gInPc2	úkol
a	a	k8xC	a
povinností	povinnost	k1gFnPc2	povinnost
nesmí	smět	k5eNaImIp3nS	smět
ECB	ECB	kA	ECB
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
ani	ani	k8xC	ani
žádný	žádný	k3yNgInSc1	žádný
člen	člen	k1gInSc1	člen
jejich	jejich	k3xOp3gInPc2	jejich
rozhodovacích	rozhodovací	k2eAgInPc2d1	rozhodovací
orgánů	orgán	k1gInPc2	orgán
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
ani	ani	k8xC	ani
přijímat	přijímat	k5eAaImF	přijímat
pokyny	pokyn	k1gInPc1	pokyn
od	od	k7c2	od
orgánů	orgán	k1gInPc2	orgán
Společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
od	od	k7c2	od
žádné	žádný	k3yNgFnSc2	žádný
vlády	vláda	k1gFnSc2	vláda
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
nebo	nebo	k8xC	nebo
od	od	k7c2	od
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
jiného	jiný	k2eAgInSc2d1	jiný
orgánu	orgán	k1gInSc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Orgány	orgán	k1gInPc1	orgán
a	a	k8xC	a
instituce	instituce	k1gFnPc1	instituce
Společenství	společenství	k1gNnSc2	společenství
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
zavazují	zavazovat	k5eAaImIp3nP	zavazovat
zachovávat	zachovávat	k5eAaImF	zachovávat
tuto	tento	k3xDgFnSc4	tento
zásadu	zásada	k1gFnSc4	zásada
a	a	k8xC	a
nesnažit	snažit	k5eNaImF	snažit
se	se	k3xPyFc4	se
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
členy	člen	k1gMnPc4	člen
rozhodovacích	rozhodovací	k2eAgInPc2d1	rozhodovací
orgánů	orgán	k1gInPc2	orgán
ECB	ECB	kA	ECB
či	či	k8xC	či
národních	národní	k2eAgFnPc2d1	národní
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
jejich	jejich	k3xOp3gInPc2	jejich
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
nezávislosti	nezávislost	k1gFnSc2	nezávislost
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
orgánů	orgán	k1gInPc2	orgán
ECB	ECB	kA	ECB
a	a	k8xC	a
také	také	k9	také
národních	národní	k2eAgFnPc2d1	národní
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
překrývat	překrývat	k5eAaImF	překrývat
s	s	k7c7	s
funkčním	funkční	k2eAgNnSc7d1	funkční
obdobím	období	k1gNnSc7	období
národních	národní	k2eAgFnPc2d1	národní
vlád	vláda	k1gFnPc2	vláda
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
však	však	k9	však
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
o	o	k7c4	o
provádění	provádění	k1gNnSc4	provádění
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
finanční	finanční	k2eAgFnSc2d1	finanční
nezávislosti	nezávislost	k1gFnSc2	nezávislost
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgMnPc7d1	jediný
upisovateli	upisovatel	k1gMnPc7	upisovatel
a	a	k8xC	a
vlastníky	vlastník	k1gMnPc7	vlastník
základního	základní	k2eAgInSc2d1	základní
kapitál	kapitál	k1gInSc4	kapitál
ECB	ECB	kA	ECB
jsou	být	k5eAaImIp3nP	být
národní	národní	k2eAgFnPc4d1	národní
centrální	centrální	k2eAgFnPc4d1	centrální
banky	banka	k1gFnPc4	banka
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnPc1d1	národní
vlády	vláda	k1gFnPc1	vláda
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
EU	EU	kA	EU
nesmí	smět	k5eNaImIp3nP	smět
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
činnost	činnost	k1gFnSc4	činnost
národních	národní	k2eAgFnPc2d1	národní
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
ani	ani	k8xC	ani
ECB	ECB	kA	ECB
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
nezávislosti	nezávislost	k1gFnSc2	nezávislost
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
autonomní	autonomní	k2eAgInSc4d1	autonomní
výkon	výkon	k1gInSc4	výkon
základních	základní	k2eAgFnPc2d1	základní
pravomocí	pravomoc	k1gFnPc2	pravomoc
svěřených	svěřený	k2eAgFnPc2d1	svěřená
Evropské	evropský	k2eAgFnSc3d1	Evropská
centrální	centrální	k2eAgFnSc3d1	centrální
bance	banka	k1gFnSc3	banka
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
disponuje	disponovat	k5eAaBmIp3nS	disponovat
veškerými	veškerý	k3xTgInPc7	veškerý
nástroji	nástroj	k1gInPc7	nástroj
a	a	k8xC	a
oprávněními	oprávnění	k1gNnPc7	oprávnění
nezbytnými	nezbytný	k2eAgInPc7d1	nezbytný
k	k	k7c3	k
provádění	provádění	k1gNnSc3	provádění
účinné	účinný	k2eAgFnSc2d1	účinná
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
oprávněna	oprávnit	k5eAaPmNgFnS	oprávnit
samostatně	samostatně	k6eAd1	samostatně
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gMnPc4	on
použije	použít	k5eAaPmIp3nS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Transparentnost	transparentnost	k1gFnSc1	transparentnost
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
trhům	trh	k1gInPc3	trh
veškeré	veškerý	k3xTgFnPc4	veškerý
příslušné	příslušný	k2eAgFnPc4d1	příslušná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
strategii	strategie	k1gFnSc6	strategie
<g/>
,	,	kIx,	,
závěrech	závěr	k1gInPc6	závěr
<g/>
,	,	kIx,	,
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
týkajících	týkající	k2eAgNnPc6d1	týkající
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc2	její
politiky	politika	k1gFnSc2	politika
i	i	k9	i
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
postupech	postup	k1gInPc6	postup
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
jsou	být	k5eAaImIp3nP	být
poskytovány	poskytovat	k5eAaImNgFnP	poskytovat
včas	včas	k6eAd1	včas
a	a	k8xC	a
otevřeným	otevřený	k2eAgInSc7d1	otevřený
a	a	k8xC	a
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
většina	většina	k1gFnSc1	většina
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ECB	ECB	kA	ECB
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
transparentnost	transparentnost	k1gFnSc1	transparentnost
za	za	k7c4	za
významnou	významný	k2eAgFnSc4d1	významná
součást	součást	k1gFnSc4	součást
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
to	ten	k3xDgNnSc1	ten
především	především	k9	především
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
jejich	jejich	k3xOp3gFnSc2	jejich
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
přikládá	přikládat	k5eAaImIp3nS	přikládat
účinné	účinný	k2eAgFnSc3d1	účinná
komunikaci	komunikace	k1gFnSc3	komunikace
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
</s>
<s>
Transparentnost	transparentnost	k1gFnSc1	transparentnost
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
porozumět	porozumět	k5eAaPmF	porozumět
měnové	měnový	k2eAgFnSc3d1	měnová
politice	politika	k1gFnSc3	politika
ECB	ECB	kA	ECB
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
lepšímu	dobrý	k2eAgNnSc3d2	lepší
porozumění	porozumění	k1gNnSc3	porozumění
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
veřejnosti	veřejnost	k1gFnSc2	veřejnost
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
politika	politika	k1gFnSc1	politika
hodnověrnější	hodnověrný	k2eAgFnSc1d2	hodnověrnější
a	a	k8xC	a
účinnější	účinný	k2eAgFnSc1d2	účinnější
<g/>
.	.	kIx.	.
</s>
<s>
Transparentnost	transparentnost	k1gFnSc1	transparentnost
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ECB	ECB	kA	ECB
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
svou	svůj	k3xOyFgFnSc4	svůj
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
úloze	úloha	k1gFnSc6	úloha
banky	banka	k1gFnSc2	banka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
cílů	cíl	k1gInPc2	cíl
své	svůj	k3xOyFgFnSc2	svůj
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
prostředí	prostředí	k1gNnSc4	prostředí
důvěryhodnosti	důvěryhodnost	k1gFnSc2	důvěryhodnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
svého	svůj	k3xOyFgNnSc2	svůj
pověření	pověření	k1gNnSc2	pověření
a	a	k8xC	a
v	v	k7c6	v
plnění	plnění	k1gNnSc6	plnění
svých	svůj	k3xOyFgInPc2	svůj
úkolů	úkol	k1gInPc2	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ECB	ECB	kA	ECB
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xS	jako
instituce	instituce	k1gFnSc1	instituce
schopná	schopný	k2eAgFnSc1d1	schopná
a	a	k8xC	a
ochotná	ochotný	k2eAgFnSc1d1	ochotná
plnit	plnit	k5eAaImF	plnit
úkoly	úkol	k1gInPc4	úkol
vyplývající	vyplývající	k2eAgNnSc1d1	vyplývající
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
mandátu	mandát	k1gInSc2	mandát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
cenová	cenový	k2eAgNnPc4d1	cenové
očekávání	očekávání	k1gNnPc4	očekávání
dobře	dobře	k6eAd1	dobře
zakotvena	zakotven	k2eAgNnPc4d1	zakotveno
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
především	především	k9	především
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
hodnocení	hodnocení	k1gNnSc6	hodnocení
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
situace	situace	k1gFnSc2	situace
centrální	centrální	k2eAgFnSc7d1	centrální
bankou	banka	k1gFnSc7	banka
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnPc1d1	centrální
banky	banka	k1gFnPc1	banka
by	by	kYmCp3nP	by
rovněž	rovněž	k9	rovněž
měly	mít	k5eAaImAgInP	mít
zaujímat	zaujímat	k5eAaImF	zaujímat
otevřený	otevřený	k2eAgInSc4d1	otevřený
a	a	k8xC	a
realistický	realistický	k2eAgInSc4d1	realistický
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
lze	lze	k6eAd1	lze
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
a	a	k8xC	a
zejména	zejména	k9	zejména
čeho	co	k3yRnSc2	co
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nelze	lze	k6eNd1	lze
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
vůle	vůle	k1gFnSc1	vůle
k	k	k7c3	k
zachování	zachování	k1gNnSc3	zachování
transparentnosti	transparentnost	k1gFnSc2	transparentnost
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
politiky	politika	k1gFnSc2	politika
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
ukázněnost	ukázněnost	k1gFnSc4	ukázněnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
a	a	k8xC	a
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
politiky	politika	k1gFnSc2	politika
tak	tak	k8xS	tak
budou	být	k5eAaImBp3nP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
konzistentní	konzistentní	k2eAgFnPc1d1	konzistentní
<g/>
.	.	kIx.	.
</s>
<s>
Usnadnění	usnadnění	k1gNnSc1	usnadnění
veřejné	veřejný	k2eAgFnSc2d1	veřejná
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
opatřeními	opatření	k1gNnPc7	opatření
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
posiluje	posilovat	k5eAaImIp3nS	posilovat
motivaci	motivace	k1gFnSc4	motivace
rozhodovacích	rozhodovací	k2eAgInPc2d1	rozhodovací
orgánů	orgán	k1gInPc2	orgán
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
plnily	plnit	k5eAaImAgFnP	plnit
své	svůj	k3xOyFgFnPc1	svůj
úlohy	úloha	k1gFnPc1	úloha
co	co	k9	co
možná	možná	k9	možná
nejlépe	dobře	k6eAd3	dobře
<g/>
.	.	kIx.	.
</s>
<s>
ECB	ECB	kA	ECB
veřejně	veřejně	k6eAd1	veřejně
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
svou	svůj	k3xOyFgFnSc4	svůj
měnověpolitickou	měnověpolitický	k2eAgFnSc4d1	měnověpolitická
strategii	strategie	k1gFnSc4	strategie
a	a	k8xC	a
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
své	svůj	k3xOyFgNnSc4	svůj
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
hodnocení	hodnocení	k1gNnSc4	hodnocení
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
trh	trh	k1gInSc4	trh
lépe	dobře	k6eAd2	dobře
porozumět	porozumět	k5eAaPmF	porozumět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
měnová	měnový	k2eAgFnSc1d1	měnová
politika	politika	k1gFnSc1	politika
systematicky	systematicky	k6eAd1	systematicky
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
náhlé	náhlý	k2eAgFnPc4d1	náhlá
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnPc1	opatření
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
ve	v	k7c6	v
střednědobém	střednědobý	k2eAgInSc6d1	střednědobý
výhledu	výhled	k1gInSc6	výhled
pro	pro	k7c4	pro
trhy	trh	k1gInPc4	trh
předvídatelnější	předvídatelný	k2eAgInPc4d2	předvídatelnější
a	a	k8xC	a
lze	lze	k6eAd1	lze
tak	tak	k6eAd1	tak
efektivněji	efektivně	k6eAd2	efektivně
a	a	k8xC	a
přesněji	přesně	k6eAd2	přesně
působit	působit	k5eAaImF	působit
na	na	k7c4	na
očekávání	očekávání	k1gNnSc4	očekávání
trhů	trh	k1gInPc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
mohou	moct	k5eAaImIp3nP	moct
tržní	tržní	k2eAgInPc1d1	tržní
subjekty	subjekt	k1gInPc1	subjekt
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bude	být	k5eAaImBp3nS	být
měnová	měnový	k2eAgFnSc1d1	měnová
politika	politika	k1gFnSc1	politika
reagovat	reagovat	k5eAaBmF	reagovat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
rychleji	rychle	k6eAd2	rychle
promítnout	promítnout	k5eAaPmF	promítnout
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
měnové	měnový	k2eAgFnSc6d1	měnová
politice	politika	k1gFnSc6	politika
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
zkrátit	zkrátit	k5eAaPmF	zkrátit
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
opatření	opatření	k1gNnPc1	opatření
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
přenášena	přenášet	k5eAaImNgFnS	přenášet
do	do	k7c2	do
rozhodování	rozhodování	k1gNnSc2	rozhodování
o	o	k7c6	o
investicích	investice	k1gFnPc6	investice
a	a	k8xC	a
spotřebě	spotřeba	k1gFnSc3	spotřeba
<g/>
,	,	kIx,	,
zrychlit	zrychlit	k5eAaPmF	zrychlit
případné	případný	k2eAgFnPc4d1	případná
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
úpravy	úprava	k1gFnPc4	úprava
a	a	k8xC	a
potenciálně	potenciálně	k6eAd1	potenciálně
zvýšit	zvýšit	k5eAaPmF	zvýšit
účinnost	účinnost	k1gFnSc4	účinnost
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
pracovním	pracovní	k2eAgInSc7d1	pracovní
jazykem	jazyk	k1gInSc7	jazyk
ECB	ECB	kA	ECB
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
výroční	výroční	k2eAgFnPc1d1	výroční
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
úředních	úřední	k2eAgInPc6d1	úřední
jazycích	jazyk	k1gInPc6	jazyk
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
