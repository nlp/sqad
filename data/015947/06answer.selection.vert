<s>
Zabijačka	zabijačka	k1gFnSc1
nebo	nebo	k8xC
též	též	k9
zabíjačka	zabíjačka	k1gFnSc1
je	být	k5eAaImIp3nS
domácí	domácí	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
vepře	vepřít	k5eAaPmIp3nS
svépomocí	svépomoc	k1gFnSc7
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
za	za	k7c2
účasti	účast	k1gFnSc2
řezníka	řezník	k1gMnSc2
<g/>
;	;	kIx,
následuje	následovat	k5eAaImIp3nS
rozporcování	rozporcování	k1gNnSc4
a	a	k8xC
zpracování	zpracování	k1gNnSc4
masa	maso	k1gNnSc2
do	do	k7c2
domácích	domácí	k2eAgFnPc2d1
zásob	zásoba	k1gFnPc2
před	před	k7c7
nadcházející	nadcházející	k2eAgFnSc7d1
zimou	zima	k1gFnSc7
<g/>
.	.	kIx.
</s>