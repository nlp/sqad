<s>
Zabijačka	zabijačka	k1gFnSc1
</s>
<s>
Plastika	plastika	k1gFnSc1
zabijačky	zabijačka	k1gFnSc2
umístěná	umístěný	k2eAgFnSc1d1
při	při	k7c6
Zlaté	zlatý	k2eAgFnSc6d1
bráně	brána	k1gFnSc6
Katedrály	katedrála	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Víta	Vít	k1gMnSc2
<g/>
,	,	kIx,
Václava	Václav	k1gMnSc2
a	a	k8xC
Vojtěcha	Vojtěch	k1gMnSc2
</s>
<s>
Zabijačka	zabijačka	k1gFnSc1
nebo	nebo	k8xC
též	též	k9
zabíjačka	zabíjačka	k1gFnSc1
je	být	k5eAaImIp3nS
domácí	domácí	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
vepře	vepřít	k5eAaPmIp3nS
svépomocí	svépomoc	k1gFnSc7
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
za	za	k7c2
účasti	účast	k1gFnSc2
řezníka	řezník	k1gMnSc2
<g/>
;	;	kIx,
následuje	následovat	k5eAaImIp3nS
rozporcování	rozporcování	k1gNnSc4
a	a	k8xC
zpracování	zpracování	k1gNnSc4
masa	maso	k1gNnSc2
do	do	k7c2
domácích	domácí	k2eAgFnPc2d1
zásob	zásoba	k1gFnPc2
před	před	k7c7
nadcházející	nadcházející	k2eAgFnSc7d1
zimou	zima	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
této	tento	k3xDgFnSc2
samozásobitelské	samozásobitelský	k2eAgFnSc2d1
role	role	k1gFnSc2
je	být	k5eAaImIp3nS
nepřímým	přímý	k2eNgInSc7d1
smyslem	smysl	k1gInSc7
zabijačky	zabijačka	k1gFnSc2
také	také	k6eAd1
její	její	k3xOp3gInSc4
společenský	společenský	k2eAgInSc4d1
přesah	přesah	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
bývá	bývat	k5eAaImIp3nS
značně	značně	k6eAd1
náročná	náročný	k2eAgFnSc1d1
na	na	k7c4
technické	technický	k2eAgNnSc4d1
(	(	kIx(
<g/>
kuchyňské	kuchyňský	k2eAgNnSc4d1
<g/>
)	)	kIx)
zázemí	zázemí	k1gNnSc4
i	i	k9
na	na	k7c4
počet	počet	k1gInSc4
spolupracovníků	spolupracovník	k1gMnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
době	doba	k1gFnSc6
zabijačky	zabijačka	k1gFnSc2
se	se	k3xPyFc4
ke	k	k7c3
členům	člen	k1gMnPc3
domácnosti	domácnost	k1gFnSc2
připojuje	připojovat	k5eAaImIp3nS
i	i	k9
širší	široký	k2eAgNnSc4d2
příbuzenstvo	příbuzenstvo	k1gNnSc4
<g/>
,	,	kIx,
sousedé	soused	k1gMnPc1
apod.	apod.	kA
<g/>
,	,	kIx,
vykonávající	vykonávající	k2eAgFnSc2d1
pomocné	pomocný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tradičně	tradičně	k6eAd1
se	se	k3xPyFc4
vepř	vepř	k1gMnSc1
poráží	porážet	k5eAaImIp3nS
v	v	k7c6
předvánočním	předvánoční	k2eAgNnSc6d1
období	období	k1gNnSc6
nebo	nebo	k8xC
v	v	k7c6
době	doba	k1gFnSc6
masopustu	masopust	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
masné	masný	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
v	v	k7c6
chladném	chladný	k2eAgNnSc6d1
období	období	k1gNnSc6
nepodléhají	podléhat	k5eNaImIp3nP
zkáze	zkáza	k1gFnSc3
a	a	k8xC
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
snáze	snadno	k6eAd2
uchovat	uchovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
byla	být	k5eAaImAgFnS
zabijačka	zabijačka	k1gFnSc1
na	na	k7c6
středoevropském	středoevropský	k2eAgInSc6d1
venkově	venkov	k1gInSc6
a	a	k8xC
v	v	k7c6
menších	malý	k2eAgNnPc6d2
městech	město	k1gNnPc6
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
pravidlem	pravidlem	k6eAd1
téměř	téměř	k6eAd1
v	v	k7c6
každém	každý	k3xTgInSc6
domě	dům	k1gInSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
její	její	k3xOp3gFnSc1
četnost	četnost	k1gFnSc1
výrazně	výrazně	k6eAd1
poklesla	poklesnout	k5eAaPmAgFnS
s	s	k7c7
ústupem	ústup	k1gInSc7
drobného	drobný	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
a	a	k8xC
také	také	k9
díky	díky	k7c3
prakticky	prakticky	k6eAd1
bezproblémové	bezproblémový	k2eAgFnSc3d1
celoroční	celoroční	k2eAgFnPc4d1
maloobchodní	maloobchodní	k2eAgFnPc4d1
dostupnosti	dostupnost	k1gFnPc4
vepřového	vepřový	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgFnSc7d1
určení	určení	k1gNnSc4
zabijačky	zabijačka	k1gFnSc2
jako	jako	k8xC,k8xS
zisk	zisk	k1gInSc1
masa	maso	k1gNnSc2
pro	pro	k7c4
výživu	výživa	k1gFnSc4
domácnosti	domácnost	k1gFnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
změnil	změnit	k5eAaPmAgInS
i	i	k9
na	na	k7c4
akci	akce	k1gFnSc4
folklórní	folklórní	k2eAgFnSc4d1
a	a	k8xC
společenskou	společenský	k2eAgFnSc4d1
<g/>
,	,	kIx,
zabijačka	zabijačka	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
součástí	součást	k1gFnSc7
předvádění	předvádění	k1gNnSc2
lidových	lidový	k2eAgNnPc2d1
řemesel	řemeslo	k1gNnPc2
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
jarmarků	jarmarků	k?
<g/>
,	,	kIx,
adventních	adventní	k2eAgInPc2d1
trhů	trh	k1gInPc2
<g/>
,	,	kIx,
masopustních	masopustní	k2eAgFnPc2d1
zábav	zábava	k1gFnPc2
<g/>
,	,	kIx,
festivalů	festival	k1gInPc2
atd.	atd.	kA
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
jako	jako	k9
součást	součást	k1gFnSc1
firemních	firemní	k2eAgInPc2d1
večírků	večírek	k1gInPc2
či	či	k8xC
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Přípravné	přípravný	k2eAgFnPc1d1
práce	práce	k1gFnPc1
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
prase	prase	k1gNnSc1
chováno	chován	k2eAgNnSc1d1
v	v	k7c6
domácích	domácí	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
(	(	kIx(
<g/>
tj.	tj.	kA
není	být	k5eNaImIp3nS
před	před	k7c7
porážkou	porážka	k1gFnSc7
zakoupeno	zakoupit	k5eAaPmNgNnS
u	u	k7c2
sedláka	sedlák	k1gInSc2
či	či	k8xC
ve	v	k7c6
vepříně	vepřín	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dobré	dobrý	k2eAgFnPc4d1
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
(	(	kIx(
<g/>
asi	asi	k9
1	#num#	k4
měsíc	měsíc	k1gInSc4
<g/>
)	)	kIx)
před	před	k7c7
porážkou	porážka	k1gFnSc7
přestat	přestat	k5eAaPmF
prase	prase	k1gNnSc4
krmit	krmit	k5eAaImF
zapáchajícími	zapáchající	k2eAgNnPc7d1
krmivy	krmivo	k1gNnPc7
a	a	k8xC
často	často	k6eAd1
a	a	k8xC
více	hodně	k6eAd2
mu	on	k3xPp3gMnSc3
podestýlat	podestýlat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
organismus	organismus	k1gInSc1
„	„	k?
<g/>
vyčistil	vyčistit	k5eAaPmAgInS
<g/>
“	“	k?
<g/>
:	:	kIx,
maso	maso	k1gNnSc1
a	a	k8xC
droby	droba	k1gFnPc1
ztratí	ztratit	k5eAaPmIp3nP
případný	případný	k2eAgInSc4d1
zápach	zápach	k1gInSc4
krmiva	krmivo	k1gNnSc2
<g/>
,	,	kIx,
kůže	kůže	k1gFnSc2
se	se	k3xPyFc4
vyčistí	vyčistit	k5eAaPmIp3nS
a	a	k8xC
zbaví	zbavit	k5eAaPmIp3nS
vad	vada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
porážku	porážka	k1gFnSc4
provádí	provádět	k5eAaImIp3nS
povolaný	povolaný	k2eAgInSc1d1
řezník-nečlen	řezník-nečlen	k1gInSc1
domácnosti	domácnost	k1gFnSc2
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
osvědčení	osvědčení	k1gNnSc4
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
potravinami	potravina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
zabijačky	zabijačka	k1gFnSc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
s	s	k7c7
řezníkem	řezník	k1gMnSc7
dohodnout	dohodnout	k5eAaPmF
včas	včas	k6eAd1
kvůli	kvůli	k7c3
vysoké	vysoký	k2eAgFnSc3d1
pravděpodobnosti	pravděpodobnost	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
více	hodně	k6eAd2
zakázek	zakázka	k1gFnPc2
v	v	k7c6
kolizním	kolizní	k2eAgInSc6d1
termínu	termín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvhodnějším	vhodný	k2eAgNnSc7d3
obdobím	období	k1gNnSc7
pro	pro	k7c4
zabijačku	zabijačka	k1gFnSc4
je	být	k5eAaImIp3nS
chladné	chladný	k2eAgNnSc1d1
suché	suchý	k2eAgNnSc1d1
počasí	počasí	k1gNnSc1
<g/>
,	,	kIx,
s	s	k7c7
teplotami	teplota	k1gFnPc7
5	#num#	k4
až	až	k9
12	#num#	k4
°	°	k?
<g/>
C	C	kA
–	–	k?
při	při	k7c6
nižších	nízký	k2eAgFnPc6d2
teplotách	teplota	k1gFnPc6
vzrůstá	vzrůstat	k5eAaImIp3nS
namáhavost	namáhavost	k1gFnSc1
bourání	bourání	k1gNnSc2
studeného	studený	k2eAgNnSc2d1
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
zvlášť	zvlášť	k6eAd1
v	v	k7c6
mraze	mráz	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
zvyšuje	zvyšovat	k5eAaImIp3nS
se	se	k3xPyFc4
nebezpečí	nebezpečí	k1gNnSc1
pracovního	pracovní	k2eAgInSc2d1
úrazu	úraz	k1gInSc2
například	například	k6eAd1
pořezáním	pořezání	k1gNnPc3
kvůli	kvůli	k7c3
prochladlým	prochladlý	k2eAgInPc3d1
prstům	prst	k1gInPc3
<g/>
,	,	kIx,
při	při	k7c6
vyšších	vysoký	k2eAgFnPc6d2
teplotách	teplota	k1gFnPc6
zase	zase	k9
roste	růst	k5eAaImIp3nS
pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
pracoviště	pracoviště	k1gNnSc4
zavítá	zavítat	k5eAaPmIp3nS
obtížný	obtížný	k2eAgInSc1d1
hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
zejména	zejména	k9
mouchy	moucha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Domácnost	domácnost	k1gFnSc4
<g/>
,	,	kIx,
připravující	připravující	k2eAgFnSc4d1
zabijačku	zabijačka	k1gFnSc4
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
obstarává	obstarávat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
surovin	surovina	k1gFnPc2
a	a	k8xC
připravuje	připravovat	k5eAaImIp3nS
zázemí	zázemí	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
neslazené	slazený	k2eNgNnSc4d1
bílé	bílý	k2eAgNnSc4d1
pečivo	pečivo	k1gNnSc4
(	(	kIx(
<g/>
neplněné	plněný	k2eNgFnSc2d1
buchty	buchta	k1gFnSc2
<g/>
,	,	kIx,
veky	veka	k1gFnSc2
<g/>
,	,	kIx,
nesypané	sypaný	k2eNgInPc4d1
rohlíky	rohlík	k1gInPc4
atd.	atd.	kA
<g/>
)	)	kIx)
–	–	k?
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
nechává	nechávat	k5eAaImIp3nS
v	v	k7c6
pokrájeném	pokrájený	k2eAgInSc6d1
nebo	nebo	k8xC
nalámaném	nalámaný	k2eAgInSc6d1
stavu	stav	k1gInSc6
proschnout	proschnout	k5eAaPmF
na	na	k7c6
suchém	suchý	k2eAgNnSc6d1
místě	místo	k1gNnSc6
</s>
<s>
koření	koření	k1gNnSc1
dle	dle	k7c2
místních	místní	k2eAgFnPc2d1
zvyklostí	zvyklost	k1gFnPc2
<g/>
,	,	kIx,
základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
umletý	umletý	k2eAgInSc1d1
pepř	pepř	k1gInSc1
<g/>
,	,	kIx,
prosetá	prosetý	k2eAgFnSc1d1
a	a	k8xC
promnutá	promnutý	k2eAgFnSc1d1
majoránka	majoránka	k1gFnSc1
a	a	k8xC
nadrcený	nadrcený	k2eAgInSc1d1
či	či	k8xC
celý	celý	k2eAgInSc1d1
kmín	kmín	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgInPc1d1
nebo	nebo	k8xC
uměle	uměle	k6eAd1
vyrobené	vyrobený	k2eAgInPc1d1
měchýře	měchýř	k1gInPc1
a	a	k8xC
potravinová	potravinový	k2eAgNnPc1d1
střívka	střívko	k1gNnPc1
pro	pro	k7c4
plněné	plněný	k2eAgInPc4d1
masové	masový	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
</s>
<s>
špejle	špejle	k1gFnSc1
pro	pro	k7c4
snadné	snadný	k2eAgNnSc4d1
zakončování	zakončování	k1gNnSc4
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
zašpičatělé	zašpičatělý	k2eAgFnPc1d1
(	(	kIx(
<g/>
toto	tento	k3xDgNnSc1
ostření	ostření	k1gNnSc1
lze	lze	k6eAd1
provést	provést	k5eAaPmF
doma	doma	k6eAd1
nožem	nůž	k1gInSc7
nebo	nebo	k8xC
na	na	k7c6
smirkové	smirkový	k2eAgFnSc6d1
brusce	bruska	k1gFnSc6
<g/>
)	)	kIx)
–	–	k?
špejle	špejle	k1gFnSc2
se	s	k7c7
<g/>
,	,	kIx,
po	po	k7c6
namotání	namotání	k1gNnSc6
střeva	střevo	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
propíchnutí	propíchnutí	k1gNnSc4
<g/>
,	,	kIx,
posune	posunout	k5eAaPmIp3nS
až	až	k9
na	na	k7c4
konec	konec	k1gInSc4
a	a	k8xC
zalomí	zalomit	k5eAaPmIp3nP
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
špička	špička	k1gFnSc1
na	na	k7c6
špejli	špejle	k1gFnSc6
zůstane	zůstat	k5eAaPmIp3nS
pro	pro	k7c4
další	další	k2eAgNnSc4d1
špejlování	špejlování	k1gNnSc4
</s>
<s>
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
kuchyňských	kuchyňský	k2eAgFnPc2d1
nádob	nádoba	k1gFnPc2
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
značného	značný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
horké	horký	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
míchání	míchání	k1gNnSc4
surovin	surovina	k1gFnPc2
i	i	k9
pro	pro	k7c4
samotnou	samotný	k2eAgFnSc4d1
tepelnou	tepelný	k2eAgFnSc4d1
úpravu	úprava	k1gFnSc4
masných	masný	k2eAgInPc2d1
pokrmů	pokrm	k1gInPc2
–	–	k?
vhodné	vhodný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
smaltované	smaltovaný	k2eAgInPc1d1
a	a	k8xC
nerezové	rezový	k2eNgInPc1d1
hrnce	hrnec	k1gInPc1
o	o	k7c6
objemu	objem	k1gInSc6
10	#num#	k4
až	až	k9
100	#num#	k4
litrů	litr	k1gInPc2
a	a	k8xC
obdobné	obdobný	k2eAgFnPc1d1
nádoby	nádoba	k1gFnPc1
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
kotle-pařáky	kotle-pařák	k1gInPc1
</s>
<s>
technické	technický	k2eAgNnSc4d1
zázemí	zázemí	k1gNnSc4
a	a	k8xC
vybavení	vybavení	k1gNnSc4
pracoviště	pracoviště	k1gNnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
je	být	k5eAaImIp3nS
myšlena	myšlen	k2eAgFnSc1d1
jednak	jednak	k8xC
rozporka	rozporka	k1gFnSc1
(	(	kIx(
<g/>
na	na	k7c6
trojnožce	trojnožka	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
háku	hák	k1gInSc6
apod.	apod.	kA
<g/>
)	)	kIx)
pro	pro	k7c4
zavěšení	zavěšení	k1gNnSc4
těla	tělo	k1gNnSc2
vepře	vepřít	k5eAaPmIp3nS
<g/>
,	,	kIx,
jednak	jednak	k8xC
varné	varný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
(	(	kIx(
<g/>
dostatečně	dostatečně	k6eAd1
prostorná	prostorný	k2eAgNnPc1d1
kuchyňská	kuchyňský	k2eAgNnPc1d1
kamna	kamna	k1gNnPc1
<g/>
,	,	kIx,
větší	veliký	k2eAgNnPc1d2
množství	množství	k1gNnPc1
sporáků	sporák	k1gInPc2
či	či	k8xC
dostatečně	dostatečně	k6eAd1
stabilních	stabilní	k2eAgInPc2d1
vařičů	vařič	k1gInPc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jednak	jednak	k8xC
stoly	stol	k1gInPc1
a	a	k8xC
tály	tál	k1gInPc1
pro	pro	k7c4
bourání	bourání	k1gNnSc4
masa	maso	k1gNnSc2
a	a	k8xC
přípravu	příprava	k1gFnSc4
masových	masový	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
</s>
<s>
základ	základ	k1gInSc1
pro	pro	k7c4
masné	masný	k2eAgInPc4d1
výrobky	výrobek	k1gInPc4
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
bývá	bývat	k5eAaImIp3nS
nadrobno	nadrobno	k6eAd1
nakrájená	nakrájený	k2eAgFnSc1d1
cibule	cibule	k1gFnSc1
osmahnutá	osmahnutý	k2eAgFnSc1d1
na	na	k7c6
sádle	sádlo	k1gNnSc6
a	a	k8xC
prolisovaný	prolisovaný	k2eAgInSc1d1
česnek	česnek	k1gInSc1
utřený	utřený	k2eAgInSc1d1
se	se	k3xPyFc4
solí	sůl	k1gFnSc7
</s>
<s>
pokud	pokud	k8xS
domácnost	domácnost	k1gFnSc1
dokáže	dokázat	k5eAaPmIp3nS
odhadnout	odhadnout	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
tělo	tělo	k1gNnSc1
vepře	vepř	k1gMnSc2
nebude	být	k5eNaImBp3nS
dostačovat	dostačovat	k5eAaImF
do	do	k7c2
požadovaných	požadovaný	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přikoupit	přikoupit	k5eAaPmF
i	i	k9
další	další	k2eAgFnPc4d1
masné	masný	k2eAgFnPc4d1
suroviny	surovina	k1gFnPc4
jako	jako	k8xS,k8xC
vepřové	vepřový	k2eAgFnPc4d1
hlavy	hlava	k1gFnPc4
<g/>
,	,	kIx,
droby	droba	k1gFnPc4
atd.	atd.	kA
</s>
<s>
nejpozději	pozdě	k6eAd3
den	den	k1gInSc1
před	před	k7c7
zabijačkou	zabijačka	k1gFnSc7
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
uklidit	uklidit	k5eAaPmF
místo	místo	k7c2
konání	konání	k1gNnSc2
zabijačky	zabijačka	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
na	na	k7c6
budoucím	budoucí	k2eAgNnSc6d1
řeznickém	řeznický	k2eAgNnSc6d1
pracovišti	pracoviště	k1gNnSc6
dostatek	dostatek	k1gInSc1
manipulačního	manipulační	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vepře	vepř	k1gMnPc4
je	být	k5eAaImIp3nS
vhodné	vhodný	k2eAgNnSc1d1
nakrmit	nakrmit	k5eAaPmF
nejpozději	pozdě	k6eAd3
24	#num#	k4
hodin	hodina	k1gFnPc2
před	před	k7c7
vlastní	vlastní	k2eAgFnSc7d1
porážkou	porážka	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
k	k	k7c3
dispozici	dispozice	k1gFnSc3
dostatek	dostatek	k1gInSc1
čisté	čistý	k2eAgFnSc2d1
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vyčištění	vyčištění	k1gNnSc3
vnitřností	vnitřnost	k1gFnPc2
přirozenou	přirozený	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Porážka	porážka	k1gFnSc1
</s>
<s>
Zachycení	zachycení	k1gNnSc1
porážky	porážka	k1gFnSc2
prasete	prase	k1gNnSc2
ve	v	k7c6
středověku	středověk	k1gInSc6
</s>
<s>
Zabijačka	zabijačka	k1gFnSc1
je	být	k5eAaImIp3nS
časově	časově	k6eAd1
náročný	náročný	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
porážka	porážka	k1gFnSc1
zpravidla	zpravidla	k6eAd1
koná	konat	k5eAaImIp3nS
krátce	krátce	k6eAd1
po	po	k7c6
rozednění	rozednění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Nejprve	nejprve	k6eAd1
řezník	řezník	k1gMnSc1
omráčí	omráčit	k5eAaPmIp3nS,k5eAaImIp3nS
vepře	vepř	k1gMnPc4
střelou	střela	k1gFnSc7
z	z	k7c2
jateční	jateční	k2eAgFnSc2d1
pistole	pistol	k1gFnSc2
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
sekerou	sekera	k1gFnSc7
či	či	k8xC
palicí	palice	k1gFnSc7
ranou	rána	k1gFnSc7
do	do	k7c2
čela	čelo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
vepř	vepř	k1gMnSc1
upadne	upadnout	k5eAaPmIp3nS
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
,	,	kIx,
řezník	řezník	k1gMnSc1
mu	on	k3xPp3gMnSc3
prořízne	proříznout	k5eAaPmIp3nS
krční	krční	k2eAgFnSc4d1
tepnu	tepna	k1gFnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
ho	on	k3xPp3gMnSc4
vlastně	vlastně	k9
usmrtí	usmrtit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
fungující	fungující	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
pak	pak	k6eAd1
vytlačí	vytlačit	k5eAaPmIp3nS
většinu	většina	k1gFnSc4
tělesné	tělesný	k2eAgFnSc2d1
krve	krev	k1gFnSc2
do	do	k7c2
připravené	připravený	k2eAgFnSc2d1
nádoby	nádoba	k1gFnSc2
–	–	k?
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
mělká	mělký	k2eAgFnSc1d1
nádoba	nádoba	k1gFnSc1
<g/>
,	,	kIx,
pekáč	pekáč	k1gInSc1
nebo	nebo	k8xC
nízký	nízký	k2eAgInSc1d1
lavor	lavor	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocník	pomocník	k1gMnSc1
musí	muset	k5eAaImIp3nS
krev	krev	k1gFnSc4
stále	stále	k6eAd1
míchat	míchat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
nesrazila	srazit	k5eNaPmAgFnS
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
vykrvení	vykrvení	k1gNnSc6
těla	tělo	k1gNnSc2
získanou	získaný	k2eAgFnSc4d1
krev	krev	k1gFnSc4
přeleje	přelít	k5eAaPmIp3nS
do	do	k7c2
větší	veliký	k2eAgFnSc2d2
nádoby	nádoba	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
z	z	k7c2
ní	on	k3xPp3gFnSc2
musí	muset	k5eAaImIp3nP
vymíchat	vymíchat	k5eAaPmF
vlákna	vlákno	k1gNnSc2
fibrinu	fibrin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěrečným	závěrečný	k2eAgNnSc7d1
zasolením	zasolení	k1gNnSc7
krve	krev	k1gFnSc2
definitivně	definitivně	k6eAd1
zamezí	zamezit	k5eAaPmIp3nS
jejímu	její	k3xOp3gNnSc3
sražení	sražení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Vykrvené	vykrvený	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
vepře	vepřít	k5eAaPmIp3nS
se	se	k3xPyFc4
poté	poté	k6eAd1
napaří	napařit	k5eAaPmIp3nP
(	(	kIx(
<g/>
podle	podle	k7c2
zvyklostí	zvyklost	k1gFnPc2
buď	buď	k8xC
na	na	k7c6
prkenné	prkenný	k2eAgFnSc6d1
podložce	podložka	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
častěji	často	k6eAd2
v	v	k7c6
neckách	necky	k1gFnPc6
<g/>
)	)	kIx)
–	–	k?
řezník	řezník	k1gMnSc1
horkou	horký	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
poleje	polít	k5eAaPmIp3nS
nejprve	nejprve	k6eAd1
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
prase	prase	k1gNnSc1
nebude	být	k5eNaImBp3nS
krupováno	krupovat	k5eAaImNgNnS,k5eAaBmNgNnS,k5eAaPmNgNnS
(	(	kIx(
<g/>
tj.	tj.	kA
hlava	hlava	k1gFnSc1
<g/>
,	,	kIx,
hrdlo	hrdlo	k1gNnSc1
<g/>
,	,	kIx,
pupek	pupek	k1gInSc1
<g/>
,	,	kIx,
vnitřní	vnitřní	k2eAgFnSc1d1
část	část	k1gFnSc1
kýt	kýta	k1gFnPc2
<g/>
,	,	kIx,
kolena	koleno	k1gNnPc1
<g/>
,	,	kIx,
nožky	nožka	k1gFnPc1
a	a	k8xC
oháňka	oháňka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
prase	prase	k1gNnSc1
obrátí	obrátit	k5eAaPmIp3nS
pomocí	pomocí	k7c2
řetízků	řetízek	k1gInPc2
a	a	k8xC
zbytek	zbytek	k1gInSc1
prasete	prase	k1gNnSc2
se	se	k3xPyFc4
paří	pařit	k5eAaImIp3nS
velmi	velmi	k6eAd1
opatrně	opatrně	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
krupon	krupon	k1gInSc1
nebyl	být	k5eNaImAgInS
poškozen	poškodit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kůže	kůže	k1gFnSc2
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
mechanicky	mechanicky	k6eAd1
odstranit	odstranit	k5eAaPmF
štětiny	štětina	k1gFnPc4
–	–	k?
zpravidla	zpravidla	k6eAd1
dvojmužně	dvojmužně	k6eAd1
pomocí	pomocí	k7c2
železných	železný	k2eAgInPc2d1
„	„	k?
<g/>
zvonků	zvonek	k1gInPc2
<g/>
“	“	k?
nebo	nebo	k8xC
řetězů	řetěz	k1gInPc2
<g/>
,	,	kIx,
odstranění	odstranění	k1gNnSc1
napomůže	napomoct	k5eAaPmIp3nS
posypání	posypání	k1gNnSc2
pomletou	pomletý	k2eAgFnSc7d1
kalafunou	kalafuna	k1gFnSc7
před	před	k7c7
pařením	paření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prase	prase	k1gNnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
pečlivě	pečlivě	k6eAd1
umyje	umýt	k5eAaPmIp3nS
teplou	teplý	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
řezník	řezník	k1gMnSc1
mu	on	k3xPp3gMnSc3
nařízne	naříznout	k5eAaPmIp3nS
na	na	k7c6
obou	dva	k4xCgFnPc6
zadních	zadní	k2eAgFnPc6d1
nožkách	nožka	k1gFnPc6
kůži	kůže	k1gFnSc4
mezi	mezi	k7c7
šlachou	šlacha	k1gFnSc7
a	a	k8xC
kostí	kost	k1gFnSc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
Achillovy	Achillův	k2eAgFnSc2d1
šlachy	šlacha	k1gFnSc2
<g/>
,	,	kIx,
tímto	tento	k3xDgInSc7
otvorem	otvor	k1gInSc7
se	se	k3xPyFc4
provléknou	provléknout	k5eAaPmIp3nP
berce	berce	k?
–	–	k?
háky	hák	k1gInPc7
rozporky	rozporka	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
bude	být	k5eAaImBp3nS
tělo	tělo	k1gNnSc1
vepře	vepřít	k5eAaPmIp3nS
zavěšeno	zavěsit	k5eAaPmNgNnS
během	během	k7c2
zpracování	zpracování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berce	Berce	k?
s	s	k7c7
vepřem	vepř	k1gMnSc7
se	se	k3xPyFc4
pak	pak	k6eAd1
vyzdvihnou	vyzdvihnout	k5eAaPmIp3nP
do	do	k7c2
vhodné	vhodný	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
pomocí	pomocí	k7c2
kladkostroje	kladkostroj	k1gInSc2
<g/>
)	)	kIx)
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
celé	celý	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
vepře	vepř	k1gMnSc4
bylo	být	k5eAaImAgNnS
zavěšeno	zavěšen	k2eAgNnSc1d1
a	a	k8xC
pod	pod	k7c7
rypákem	rypák	k1gInSc7
zbýval	zbývat	k5eAaImAgInS
dostatek	dostatek	k1gInSc4
prostoru	prostor	k1gInSc2
pro	pro	k7c4
nádoby	nádoba	k1gFnPc4
(	(	kIx(
<g/>
zavěšením	zavěšení	k1gNnPc3
tělo	tělo	k1gNnSc1
opticky	opticky	k6eAd1
zmohutní	zmohutnit	k5eAaPmIp3nS
a	a	k8xC
vlastní	vlastní	k2eAgFnSc7d1
vahou	váha	k1gFnSc7
se	se	k3xPyFc4
značně	značně	k6eAd1
protáhne	protáhnout	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyzdvižení	vyzdvižení	k1gNnSc6
těla	tělo	k1gNnSc2
se	se	k3xPyFc4
kůže	kůže	k1gFnSc1
dočistí	dočistit	k5eAaPmIp3nS
od	od	k7c2
zbytku	zbytek	k1gInSc2
štětin	štětina	k1gFnPc2
a	a	k8xC
naposledy	naposledy	k6eAd1
pečlivě	pečlivě	k6eAd1
umyje	umýt	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Zpracování	zpracování	k1gNnSc1
</s>
<s>
Dle	dle	k7c2
svého	svůj	k3xOyFgInSc2
zvyku	zvyk	k1gInSc2
řezník	řezník	k1gMnSc1
buď	buď	k8xC
stáhne	stáhnout	k5eAaPmIp3nS
celou	celý	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
(	(	kIx(
<g/>
krupon	krupon	k1gInSc4
<g/>
)	)	kIx)
hned	hned	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
<g/>
,	,	kIx,
anebo	anebo	k8xC
kruponuje	kruponovat	k5eAaImIp3nS
až	až	k9
při	při	k7c6
vaření	vaření	k1gNnSc6
rozbouraného	rozbouraný	k2eAgNnSc2d1
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
druhá	druhý	k4xOgFnSc1
varianta	varianta	k1gFnSc1
má	mít	k5eAaImIp3nS
výhodu	výhoda	k1gFnSc4
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c4
dobu	doba	k1gFnSc4
bourání	bourání	k1gNnPc2
kůže	kůže	k1gFnSc2
přirozeně	přirozeně	k6eAd1
drží	držet	k5eAaImIp3nS
maso	maso	k1gNnSc4
pohromadě	pohromadě	k6eAd1
a	a	k8xC
omezuje	omezovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
znečištění	znečištění	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
otevření	otevření	k1gNnSc3
břišní	břišní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
podélným	podélný	k2eAgInSc7d1
řezem	řez	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řezník	řezník	k1gMnSc1
opatrně	opatrně	k6eAd1
vyjme	vyjmout	k5eAaPmIp3nS
vnitřnosti	vnitřnost	k1gFnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nepoškodil	poškodit	k5eNaPmAgInS
střeva	střevo	k1gNnPc4
a	a	k8xC
jejich	jejich	k3xOp3gInSc7
obsahem	obsah	k1gInSc7
neznečistil	znečistit	k5eNaPmAgInS
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
vyňatá	vyňatý	k2eAgNnPc1d1
střeva	střevo	k1gNnPc1
rozdělí	rozdělit	k5eAaPmIp3nP
na	na	k7c4
tenká	tenký	k2eAgNnPc4d1
a	a	k8xC
tlustá	tlusté	k1gNnPc4
<g/>
,	,	kIx,
po	po	k7c6
obrání	obrání	k1gNnSc6
střevního	střevní	k2eAgNnSc2d1
sádla	sádlo	k1gNnSc2
tyto	tento	k3xDgFnPc4
střeva	střevo	k1gNnSc2
rozdělí	rozdělit	k5eAaPmIp3nP
na	na	k7c6
kratší	krátký	k2eAgFnSc6d2
části	část	k1gFnSc6
<g/>
,	,	kIx,
obrátí	obrátit	k5eAaPmIp3nS
naruby	naruby	k6eAd1
a	a	k8xC
vyčistí	vyčistit	k5eAaPmIp3nS
od	od	k7c2
obsahu	obsah	k1gInSc2
včetně	včetně	k7c2
vnitřních	vnitřní	k2eAgInPc2d1
klků	klk	k1gInPc2
(	(	kIx(
<g/>
této	tento	k3xDgFnSc2
činnosti	činnost	k1gFnSc2
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
šlemování	šlemování	k1gNnSc1
<g/>
)	)	kIx)
–	–	k?
dobře	dobře	k6eAd1
vypraná	vypraný	k2eAgNnPc1d1
střeva	střevo	k1gNnPc1
nezapáchají	zapáchat	k5eNaImIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
mírně	mírně	k6eAd1
zdrsnělá	zdrsnělý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
je	být	k5eAaImIp3nS
pokrájí	pokrájet	k5eAaPmIp3nS
na	na	k7c4
stejně	stejně	k6eAd1
dlouhé	dlouhý	k2eAgInPc4d1
kusy	kus	k1gInPc4
a	a	k8xC
ponoří	ponořit	k5eAaPmIp3nP
do	do	k7c2
studené	studený	k2eAgFnSc2d1
vody	voda	k1gFnSc2
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobným	obdobný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
vyčistí	vyčistit	k5eAaPmIp3nS
a	a	k8xC
vypere	vyprat	k5eAaPmIp3nS
žaludek	žaludek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
vymne	vymnout	k5eAaPmIp3nS
solí	sůl	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbenou	oblíbený	k2eAgFnSc7d1
kuriozitou	kuriozita	k1gFnSc7
je	být	k5eAaImIp3nS
vyčištění	vyčištění	k1gNnSc1
<g/>
,	,	kIx,
vymytí	vymytí	k1gNnSc1
a	a	k8xC
nafouknutí	nafouknutí	k1gNnSc1
močového	močový	k2eAgInSc2d1
měchýře	měchýř	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jater	játra	k1gNnPc2
musí	muset	k5eAaImIp3nS
řezník	řezník	k1gMnSc1
opatrně	opatrně	k6eAd1
vyjmout	vyjmout	k5eAaPmF
žluč	žluč	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvolněné	uvolněný	k2eAgFnSc2d1
vyjmuté	vyjmutý	k2eAgFnSc2d1
ledviny	ledvina	k1gFnSc2
jsou	být	k5eAaImIp3nP
indikátorem	indikátor	k1gInSc7
zdravotního	zdravotní	k2eAgInSc2d1
stavu	stav	k1gInSc2
vepře	vepř	k1gMnSc2
a	a	k8xC
měly	mít	k5eAaImAgFnP
by	by	kYmCp3nP
být	být	k5eAaImF
prohlédnuty	prohlédnout	k5eAaPmNgInP
veterinářem	veterinář	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vybrání	vybrání	k1gNnSc6
břišních	břišní	k2eAgFnPc2d1
vnitřností	vnitřnost	k1gFnPc2
řezník	řezník	k1gMnSc1
uvolní	uvolnit	k5eAaPmIp3nS
hrudní	hrudní	k2eAgFnSc4d1
dutinu	dutina	k1gFnSc4
a	a	k8xC
vyjme	vyjmout	k5eAaPmIp3nS
osrdí	osrdí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
rozdělí	rozdělit	k5eAaPmIp3nS
<g/>
,	,	kIx,
srdce	srdce	k1gNnSc1
prořízne	proříznout	k5eAaPmIp3nS
a	a	k8xC
droby	droba	k1gFnPc4
důkladně	důkladně	k6eAd1
opere	oprat	k5eAaPmIp3nS
v	v	k7c6
čisté	čistý	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Schematické	schematický	k2eAgNnSc4d1
znázornění	znázornění	k1gNnSc4
dílů	díl	k1gInPc2
vepřového	vepřové	k1gNnSc2
masa	maso	k1gNnSc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
hlava	hlava	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
lalok	lalok	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
hřbetní	hřbetní	k2eAgNnSc1d1
sádlo	sádlo	k1gNnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
krkovička	krkovička	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
plecko	plecko	k1gNnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
pečeně	pečeně	k1gFnSc2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
vepřová	vepřový	k2eAgFnSc1d1
pečeně	pečeně	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
8	#num#	k4
<g/>
.	.	kIx.
panenská	panenský	k2eAgFnSc1d1
svíčková	svíčková	k1gFnSc1
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
žebra	žebro	k1gNnPc4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
bůček	bůček	k1gInSc1
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
plec	plec	k1gFnSc1
(	(	kIx(
<g/>
horní	horní	k2eAgInSc1d1
šál	šál	k1gInSc1
<g/>
,	,	kIx,
dolní	dolní	k2eAgInSc1d1
šál	šál	k1gInSc1
a	a	k8xC
ořech	ořech	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
kýta	kýta	k1gFnSc1
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
koleno	koleno	k1gNnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
nožičky	nožička	k1gFnSc2
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
ocásek	ocásek	k1gInSc1
</s>
<s>
Po	po	k7c6
vyčištění	vyčištění	k1gNnSc6
břišní	břišní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
řezník	řezník	k1gMnSc1
odebere	odebrat	k5eAaPmIp3nS
z	z	k7c2
těla	tělo	k1gNnSc2
vepře	vepřít	k5eAaPmIp3nS
maso	maso	k1gNnSc4
k	k	k7c3
vaření	vaření	k1gNnSc3
=	=	kIx~
výrobě	výroba	k1gFnSc3
ovaru	ovar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
začíná	začínat	k5eAaImIp3nS
odříznutím	odříznutí	k1gNnSc7
vepřovy	vepřův	k2eAgFnSc2d1
hlavy	hlava	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
následně	následně	k6eAd1
zbaví	zbavit	k5eAaPmIp3nS
mozku	mozek	k1gInSc2
a	a	k8xC
rozsekne	rozseknout	k5eAaPmIp3nS
vejpůl	vejpůl	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smažený	smažený	k2eAgInSc1d1
mozeček	mozeček	k1gInSc1
s	s	k7c7
vejci	vejce	k1gNnPc7
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgInPc6
krajích	kraj	k1gInPc6
prvním	první	k4xOgInSc7
masným	masný	k2eAgInSc7d1
výrobkem	výrobek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
zabijačce	zabijačka	k1gFnSc6
servírován	servírován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgFnPc7d1
součástmi	součást	k1gFnPc7
ovaru	ovar	k1gInSc2
bývají	bývat	k5eAaImIp3nP
podbradek	podbradek	k1gInSc4
(	(	kIx(
<g/>
podhrdlí	podhrdlí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plíce	plíce	k1gFnSc1
<g/>
,	,	kIx,
srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
slezina	slezina	k1gFnSc1
<g/>
,	,	kIx,
půl	půl	k1xP
nebo	nebo	k8xC
celá	celý	k2eAgNnPc4d1
játra	játra	k1gNnPc4
<g/>
,	,	kIx,
měkké	měkký	k2eAgFnPc4d1
části	část	k1gFnPc4
boků	bok	k1gInPc2
(	(	kIx(
<g/>
paždíky	paždík	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kolínka	kolínko	k1gNnPc1
<g/>
,	,	kIx,
nožky	nožka	k1gFnPc1
zbavené	zbavený	k2eAgNnSc1d1
paznehtů	pazneht	k1gInPc2
a	a	k8xC
popřípadě	popřípadě	k6eAd1
jedna	jeden	k4xCgFnSc1
plec	plec	k1gFnSc1
na	na	k7c4
tlačenku	tlačenka	k1gFnSc4
–	–	k?
všechny	všechen	k3xTgFnPc4
tyto	tento	k3xDgFnPc4
tělesné	tělesný	k2eAgFnPc4d1
části	část	k1gFnPc4
se	se	k3xPyFc4
po	po	k7c6
důkladném	důkladný	k2eAgNnSc6d1
umytí	umytí	k1gNnSc6
vaří	vařit	k5eAaImIp3nS
v	v	k7c6
osolené	osolený	k2eAgFnSc6d1
nekořeněné	kořeněný	k2eNgFnSc6d1
vodě	voda	k1gFnSc6
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
hrnci	hrnec	k1gInSc6
nebo	nebo	k8xC
kotli	kotel	k1gInSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vzniká	vznikat	k5eAaImIp3nS
základ	základ	k1gInSc1
zabijačkové	zabijačkový	k2eAgFnSc2d1
polévky	polévka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgFnSc7d1
(	(	kIx(
<g/>
břišní	břišní	k2eAgFnSc7d1
<g/>
,	,	kIx,
střevní	střevní	k2eAgFnSc7d1
<g/>
)	)	kIx)
sádlo	sádlo	k1gNnSc1
se	se	k3xPyFc4
dává	dávat	k5eAaImIp3nS
škvařit	škvařit	k5eAaImF
na	na	k7c6
odlehlé	odlehlý	k2eAgFnSc6d1
nebo	nebo	k8xC
dobře	dobře	k6eAd1
větrané	větraný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
během	během	k7c2
zpracování	zpracování	k1gNnSc2
zapáchá	zapáchat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Závěrečné	závěrečný	k2eAgFnPc1d1
bourací	bourací	k2eAgFnPc1d1
práce	práce	k1gFnPc1
spočívají	spočívat	k5eAaImIp3nP
v	v	k7c6
podélném	podélný	k2eAgNnSc6d1
rozseknutí	rozseknutí	k1gNnSc6
prasečího	prasečí	k2eAgNnSc2d1
torza	torzo	k1gNnSc2
na	na	k7c4
dvě	dva	k4xCgFnPc4
poloviny	polovina	k1gFnPc4
a	a	k8xC
odebrání	odebrání	k1gNnSc4
sádelných	sádelný	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
,	,	kIx,
čisté	čistý	k2eAgNnSc4d1
maso	maso	k1gNnSc4
pak	pak	k6eAd1
řezník	řezník	k1gMnSc1
rozebere	rozebrat	k5eAaPmIp3nS
a	a	k8xC
rozdělí	rozdělit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
maso	maso	k1gNnSc1
zpracovávat	zpracovávat	k5eAaImF
samotné	samotný	k2eAgInPc1d1
(	(	kIx(
<g/>
na	na	k7c4
řízky	řízek	k1gInPc4
<g/>
,	,	kIx,
kotlety	kotleta	k1gFnPc4
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
nechat	nechat	k5eAaPmF
alespoň	alespoň	k9
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
vyvěsit	vyvěsit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Povinnosti	povinnost	k1gFnPc1
řezníka	řezník	k1gMnSc2
</s>
<s>
Zabíjačka	zabíjačka	k1gFnSc1
na	na	k7c6
obraze	obraz	k1gInSc6
vlámského	vlámský	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Pietera	Pieter	k1gMnSc2
Brueghela	Brueghel	k1gMnSc2
mladšího	mladý	k2eAgMnSc2d2
</s>
<s>
Povinnosti	povinnost	k1gFnPc1
řezníka	řezník	k1gMnSc2
se	se	k3xPyFc4
kraj	kraj	k1gInSc4
od	od	k7c2
kraje	kraj	k1gInSc2
liší	lišit	k5eAaImIp3nP
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
řídit	řídit	k5eAaImF
i	i	k9
vzájemnou	vzájemný	k2eAgFnSc7d1
dohodou	dohoda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
masných	masný	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
není	být	k5eNaImIp3nS
běžnou	běžný	k2eAgFnSc7d1
povinností	povinnost	k1gFnSc7
řezníka	řezník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
řezník	řezník	k1gMnSc1
i	i	k9
krájí	krájet	k5eAaImIp3nS
sádlo	sádlo	k1gNnSc4
ke	k	k7c3
škvaření	škvaření	k1gNnSc3
<g/>
,	,	kIx,
nasoluje	nasolovat	k5eAaImIp3nS
maso	maso	k1gNnSc4
k	k	k7c3
uzení	uzení	k1gNnSc3
a	a	k8xC
připravuje	připravovat	k5eAaImIp3nS
z	z	k7c2
ořezu	ořez	k1gInSc2
klobásy	klobása	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obvyklé	obvyklý	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
řezníka	řezník	k1gMnSc2
<g/>
:	:	kIx,
</s>
<s>
porážka	porážka	k1gFnSc1
zvířete	zvíře	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
spaření	spaření	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
sejmutí	sejmutí	k1gNnSc1
kruponu	krupon	k1gInSc2
a	a	k8xC
sádla	sádlo	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
odebrání	odebrání	k1gNnSc1
masa	maso	k1gNnSc2
z	z	k7c2
kusu	kus	k1gInSc2
k	k	k7c3
vaření	vaření	k1gNnSc3
<g/>
,	,	kIx,
</s>
<s>
rozetnutí	rozetnutí	k1gNnSc1
kusu	kus	k1gInSc2
<g/>
,	,	kIx,
úprava	úprava	k1gFnSc1
k	k	k7c3
veterinární	veterinární	k2eAgFnSc3d1
prohlídce	prohlídka	k1gFnSc3
<g/>
,	,	kIx,
</s>
<s>
šlemování	šlemování	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
úprava	úprava	k1gFnSc1
ovařeného	ovařený	k2eAgNnSc2d1
masa	maso	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
rozdělení	rozdělení	k1gNnSc4
k	k	k7c3
jednotlivým	jednotlivý	k2eAgInPc3d1
výrobkům	výrobek	k1gInPc3
<g/>
,	,	kIx,
</s>
<s>
výroba	výroba	k1gFnSc1
jednoho	jeden	k4xCgInSc2
nebo	nebo	k8xC
dvou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
tlačenky	tlačenka	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
výroba	výroba	k1gFnSc1
jitrnic	jitrnice	k1gFnPc2
a	a	k8xC
jednoho	jeden	k4xCgMnSc4
nebo	nebo	k8xC
dvou	dva	k4xCgInPc2
druhů	druh	k1gInPc2
jelit	jelito	k1gNnPc2
<g/>
,	,	kIx,
</s>
<s>
příprava	příprava	k1gFnSc1
zabijačkové	zabijačkový	k2eAgFnSc2d1
polévky	polévka	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
rozebrání	rozebrání	k1gNnSc1
kusu	kus	k1gInSc2
po	po	k7c6
veterinární	veterinární	k2eAgFnSc6d1
prohlídce	prohlídka	k1gFnSc6
<g/>
,	,	kIx,
</s>
<s>
stažení	stažení	k1gNnSc1
sádla	sádlo	k1gNnSc2
z	z	k7c2
kusu	kus	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
stažení	stažení	k1gNnSc1
sádla	sádlo	k1gNnSc2
z	z	k7c2
kůže	kůže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Orientační	orientační	k2eAgFnSc1d1
výtěžnost	výtěžnost	k1gFnSc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
vepřových	vepřový	k2eAgFnPc2d1
půlek	půlka	k1gFnPc2
s	s	k7c7
masem	maso	k1gNnSc7
se	s	k7c7
sádlem	sádlo	k1gNnSc7
<g/>
,	,	kIx,
nožkami	nožka	k1gFnPc7
a	a	k8xC
hlavou	hlava	k1gFnSc7
bez	bez	k7c2
drobů	drob	k1gInPc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
80	#num#	k4
<g/>
–	–	k?
<g/>
83	#num#	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc2
živého	živý	k2eAgNnSc2d1
prasete	prase	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
drobů	drob	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
plíce	plíce	k1gFnPc1
<g/>
,	,	kIx,
srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
játra	játra	k1gNnPc1
<g/>
,	,	kIx,
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
mozeček	mozeček	k1gInSc1
a	a	k8xC
slezina	slezina	k1gFnSc1
činí	činit	k5eAaImIp3nS
okolo	okolo	k7c2
3	#num#	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc2
živého	živý	k2eAgNnSc2d1
prasete	prase	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
jednoho	jeden	k4xCgNnSc2
prasete	prase	k1gNnSc2
získáme	získat	k5eAaPmIp1nP
16	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
m	m	kA
tenkého	tenký	k2eAgNnSc2d1
střeva	střevo	k1gNnSc2
a	a	k8xC
4	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
m	m	kA
tlustého	tlustý	k2eAgNnSc2d1
střeva	střevo	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
nichž	jenž	k3xRgMnPc2
pak	pak	k6eAd1
můžeme	moct	k5eAaImIp1nP
naplnit	naplnit	k5eAaPmF
jitrnice	jitrnice	k1gFnPc4
<g/>
,	,	kIx,
jelita	jelito	k1gNnPc4
či	či	k8xC
klobásy	klobása	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Výrobky	výrobek	k1gInPc1
</s>
<s>
Výroba	výroba	k1gFnSc1
klobás	klobása	k1gFnPc2
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
</s>
<s>
mozeček	mozeček	k1gInSc1
s	s	k7c7
vejci	vejce	k1gNnPc7
</s>
<s>
zabijačkový	zabijačkový	k2eAgInSc4d1
guláš	guláš	k1gInSc4
(	(	kIx(
<g/>
zvaný	zvaný	k2eAgInSc4d1
též	též	k9
smažák	smažák	k1gInSc4
<g/>
,	,	kIx,
míchaný	míchaný	k2eAgInSc4d1
<g/>
,	,	kIx,
drobový	drobový	k2eAgInSc4d1
<g/>
,	,	kIx,
brzlík	brzlík	k1gInSc1
<g/>
,	,	kIx,
předehra	předehra	k1gFnSc1
nebo	nebo	k8xC
partička	partička	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ovar	ovar	k1gInSc1
</s>
<s>
tlačenka	tlačenka	k1gFnSc1
masová	masový	k2eAgFnSc1d1
nebo	nebo	k8xC
krvavá	krvavý	k2eAgFnSc1d1
</s>
<s>
tlačenka	tlačenka	k1gFnSc1
slezská	slezský	k2eAgFnSc1d1
</s>
<s>
jitrnice	jitrnice	k1gFnSc1
</s>
<s>
jelita	jelito	k1gNnPc1
kroupová	kroupový	k2eAgNnPc1d1
nebo	nebo	k8xC
žemlová	žemlový	k2eAgNnPc1d1
</s>
<s>
krevní	krevní	k2eAgInSc4d1
(	(	kIx(
<g/>
jelítkový	jelítkový	k2eAgInSc4d1
<g/>
,	,	kIx,
liberecký	liberecký	k2eAgInSc4d1
<g/>
)	)	kIx)
salám	salám	k1gInSc4
</s>
<s>
vývarka	vývarka	k1gFnSc1
</s>
<s>
Vaření	vaření	k1gNnSc1
zabíjačkové	zabíjačkový	k2eAgFnSc2d1
polévky	polévka	k1gFnSc2
–	–	k?
prdelačky	prdelačka	k1gFnSc2
</s>
<s>
černá	černý	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
(	(	kIx(
<g/>
zvaná	zvaný	k2eAgFnSc1d1
též	též	k9
prdelačka	prdelačka	k1gFnSc1
nebo	nebo	k8xC
bouřka	bouřka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
huspenina	huspenina	k1gFnSc1
(	(	kIx(
<g/>
zvaný	zvaný	k2eAgInSc4d1
též	též	k9
sulc	sulc	k1gInSc4
nebo	nebo	k8xC
vepřový	vepřový	k2eAgInSc4d1
rosol	rosol	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
různé	různý	k2eAgInPc1d1
druhy	druh	k1gInPc1
klobás	klobása	k1gFnPc2
</s>
<s>
sekaná	sekaná	k1gFnSc1
(	(	kIx(
<g/>
ze	z	k7c2
směsi	směs	k1gFnSc2
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
druhy	druh	k1gInPc7
masa	maso	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
domácí	domácí	k2eAgFnSc1d1
paštika	paštika	k1gFnSc1
</s>
<s>
Odrazy	odraz	k1gInPc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Význam	význam	k1gInSc1
zabijačky	zabijačka	k1gFnSc2
v	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
venkovské	venkovský	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
nespočíval	spočívat	k5eNaImAgMnS
jen	jen	k9
v	v	k7c6
obstarávání	obstarávání	k1gNnSc6
potravy	potrava	k1gFnSc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
též	též	k9
roli	role	k1gFnSc4
společenského	společenský	k2eAgInSc2d1
fenoménu	fenomén	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zabijačce	zabijačka	k1gFnSc6
tradičně	tradičně	k6eAd1
byla	být	k5eAaImAgFnS
a	a	k8xC
místy	místo	k1gNnPc7
dosud	dosud	k6eAd1
je	být	k5eAaImIp3nS
rozdílena	rozdílen	k2eAgFnSc1d1
výslužka	výslužka	k1gFnSc1
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	s	k7c7
zabijačkou	zabijačka	k1gFnSc7
pomáhali	pomáhat	k5eAaImAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
pro	pro	k7c4
sousedy	soused	k1gMnPc4
a	a	k8xC
příbuzné	příbuzný	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výslužka	výslužka	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
venkova	venkov	k1gInSc2
někdy	někdy	k6eAd1
dosud	dosud	k6eAd1
zasílá	zasílat	k5eAaImIp3nS
či	či	k8xC
dováží	dovážet	k5eAaImIp3nS,k5eAaPmIp3nS
příbuzným	příbuzný	k1gMnPc3
či	či	k8xC
rodinným	rodinný	k2eAgMnPc3d1
přátelům	přítel	k1gMnPc3
do	do	k7c2
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několikrát	několikrát	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
české	český	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
zabijačka	zabijačka	k1gFnSc1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
výtvarných	výtvarný	k2eAgInPc2d1
<g/>
,	,	kIx,
literárních	literární	k2eAgInPc2d1
<g/>
,	,	kIx,
hudebních	hudební	k2eAgNnPc2d1
a	a	k8xC
jiných	jiný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
Již	již	k9
ve	v	k7c6
středověkých	středověký	k2eAgInPc6d1
kalendářích	kalendář	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgInP
jednotlivé	jednotlivý	k2eAgInPc1d1
měsíce	měsíc	k1gInPc1
představovány	představován	k2eAgInPc1d1
výjevy	výjev	k1gInPc1
zemědělského	zemědělský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
obrázek	obrázek	k1gInSc1
zabijačky	zabijačka	k1gFnSc2
symbolizuje	symbolizovat	k5eAaImIp3nS
měsíc	měsíc	k1gInSc4
prosinec	prosinec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc1d1
význam	význam	k1gInSc1
má	mít	k5eAaImIp3nS
i	i	k9
obrázek	obrázek	k1gInSc1
zabijačky	zabijačka	k1gFnSc2
od	od	k7c2
Josefa	Josef	k1gMnSc2
Mánese	Mánes	k1gMnSc5
na	na	k7c6
kalendářní	kalendářní	k2eAgFnSc6d1
desce	deska	k1gFnSc6
Staroměstského	staroměstský	k2eAgInSc2d1
orloje	orloj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabijačku	zabijačka	k1gFnSc4
zobrazují	zobrazovat	k5eAaImIp3nP
též	též	k9
ilustrace	ilustrace	k1gFnPc1
Mikoláše	Mikoláš	k1gMnSc2
Alše	Aleš	k1gMnSc2
a	a	k8xC
zvláště	zvláště	k9
Josefa	Josef	k1gMnSc4
Lady	Lada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Nepil	pít	k5eNaImAgMnS
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
v	v	k7c6
knize	kniha	k1gFnSc6
Střevíce	střevíc	k1gInSc2
z	z	k7c2
lýčí	lýčí	k1gNnSc2
zabijačce	zabijačka	k1gFnSc3
celou	celý	k2eAgFnSc4d1
samostatnou	samostatný	k2eAgFnSc4d1
kapitolu	kapitola	k1gFnSc4
<g/>
,	,	kIx,
popisuje	popisovat	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
jak	jak	k8xC,k8xS
technicko-organizační	technicko-organizační	k2eAgInSc1d1
průběh	průběh	k1gInSc1
zabijačky	zabijačka	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k9
její	její	k3xOp3gFnSc4
společenskou	společenský	k2eAgFnSc4d1
roli	role	k1gFnSc4
na	na	k7c6
vesnici	vesnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
jednoaktovce	jednoaktovka	k1gFnSc6
Prase	prase	k1gNnSc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
motiv	motiv	k1gInSc4
nákupu	nákup	k1gInSc2
prasete	prase	k1gNnSc2
určeného	určený	k2eAgNnSc2d1
k	k	k7c3
zabijačce	zabijačka	k1gFnSc3
i	i	k8xC
zabijačky	zabijačka	k1gFnPc1
samotné	samotný	k2eAgFnPc1d1
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
české	český	k2eAgFnSc2d1
přízemnosti	přízemnost	k1gFnSc2
<g/>
,	,	kIx,
sobectví	sobectví	k1gNnSc2
a	a	k8xC
hamižnosti	hamižnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bohumil	Bohumil	k1gMnSc1
Hrabal	Hrabal	k1gMnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
novele	novela	k1gFnSc6
Postřižiny	postřižiny	k1gFnPc1
zabijačce	zabijačka	k1gFnSc6
mnoho	mnoho	k6eAd1
prostoru	prostor	k1gInSc2
a	a	k8xC
popisuje	popisovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
prostřednictvím	prostřednictvím	k7c2
poetických	poetický	k2eAgInPc2d1
obrazů	obraz	k1gInPc2
s	s	k7c7
mnoha	mnoho	k4c7
svéráznými	svérázný	k2eAgFnPc7d1
metaforami	metafora	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabíjačka	zabíjačka	k1gFnSc1
hraje	hrát	k5eAaImIp3nS
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
i	i	k9
ve	v	k7c6
filmovém	filmový	k2eAgInSc6d1
zpracováni	zpracovat	k5eAaPmNgMnP
tohoto	tento	k3xDgNnSc2
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ivo	Ivo	k1gMnSc1
Jahelka	jahelka	k1gFnSc1
popsal	popsat	k5eAaPmAgInS
v	v	k7c6
Baladě	balada	k1gFnSc6
o	o	k7c6
strašlivém	strašlivý	k2eAgInSc6d1
úderu	úder	k1gInSc6
šilhavého	šilhavý	k2eAgMnSc2d1
řezníka	řezník	k1gMnSc2
Josky	Joska	k1gMnSc2
těžký	těžký	k2eAgInSc4d1
úraz	úraz	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
utrpěl	utrpět	k5eAaPmAgMnS
„	„	k?
<g/>
bratranec	bratranec	k1gMnSc1
Rudla	Rudla	k1gMnSc1
<g/>
“	“	k?
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
si	se	k3xPyFc3
špatně	špatně	k6eAd1
vyložil	vyložit	k5eAaPmAgMnS
řezníkův	řezníkův	k2eAgInSc4d1
pokyn	pokyn	k1gInSc4
při	při	k7c6
omračování	omračování	k1gNnSc6
vepře	vepřít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
večerníčcích	večerníček	k1gInPc6
Jája	Jája	k1gFnSc1
a	a	k8xC
Pája	Pája	k?
a	a	k8xC
Chaloupka	chaloupka	k1gFnSc1
na	na	k7c6
vršku	vršek	k1gInSc6
se	se	k3xPyFc4
také	také	k9
objevuje	objevovat	k5eAaImIp3nS
zabijačka	zabijačka	k1gFnSc1
<g/>
,	,	kIx,
zobrazená	zobrazený	k2eAgFnSc1d1
v	v	k7c6
kontextu	kontext	k1gInSc6
tradic	tradice	k1gFnPc2
ročního	roční	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Steklač	Steklač	k1gMnSc1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
kapitole	kapitola	k1gFnSc6
knihy	kniha	k1gFnSc2
Pekelná	pekelný	k2eAgFnSc1d1
třída	třída	k1gFnSc1
popisuje	popisovat	k5eAaImIp3nS
chování	chování	k1gNnSc4
pražské	pražský	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
na	na	k7c6
venkovské	venkovský	k2eAgFnSc6d1
zabijačce	zabijačka	k1gFnSc6
v	v	k7c6
Pekelci	pekelec	k1gInSc6
u	u	k7c2
babičky	babička	k1gFnSc2
hlavního	hlavní	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
Boříka	Bořík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Dvojice	dvojice	k1gFnSc1
Šimek	Šimek	k1gMnSc1
<g/>
&	&	k?
<g/>
Grossmann	Grossmann	k1gMnSc1
věnovala	věnovat	k5eAaPmAgNnP,k5eAaImAgNnP
nedobrovolné	dobrovolný	k2eNgFnSc6d1
zabijačce	zabijačka	k1gFnSc6
v	v	k7c6
bytě	byt	k1gInSc6
v	v	k7c6
pražském	pražský	k2eAgInSc6d1
činžáku	činžák	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
předcházejícímu	předcházející	k2eAgNnSc3d1
dění	dění	k1gNnSc3
<g/>
,	,	kIx,
známou	známý	k2eAgFnSc4d1
humoristickou	humoristický	k2eAgFnSc4d1
povídku	povídka	k1gFnSc4
Jak	jak	k8xS,k8xC
jsme	být	k5eAaImIp1nP
chovali	chovat	k5eAaImAgMnP
užitečné	užitečný	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BENEŠOVÁ	Benešová	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kuchařka	kuchařka	k1gFnSc1
naší	náš	k3xOp1gFnSc2
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Cesty	cesta	k1gFnSc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
511	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7181	#num#	k4
<g/>
-	-	kIx~
<g/>
237	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GAHM	GAHM	kA
<g/>
,	,	kIx,
Bernhard	Bernhard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgFnSc1d1
zabíjačka	zabíjačka	k1gFnSc1
:	:	kIx,
porážení	porážení	k1gNnSc1
<g/>
,	,	kIx,
bourání	bourání	k1gNnSc1
<g/>
,	,	kIx,
zpracování	zpracování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Blesk	blesk	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
142	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85606	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MAYER	Mayer	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgFnSc1d1
zabijačka	zabijačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
VPK	VPK	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
106	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86081	#num#	k4
<g/>
-	-	kIx~
<g/>
68	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PŮHONÝ	PŮHONÝ	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgFnSc1d1
zabijačka	zabijačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brázda	Brázda	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
111	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
209	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
299	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
RUNŠTUK	RUNŠTUK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Receptury	receptura	k1gFnPc4
teplých	teplý	k2eAgInPc2d1
pokrmů	pokrm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divec	Divec	k1gInSc1
<g/>
:	:	kIx,
R	R	kA
plus	plus	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
600	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904093	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Prase	prase	k1gNnSc1
domácí	domácí	k2eAgFnSc2d1
</s>
<s>
Vepřové	vepřový	k2eAgNnSc1d1
maso	maso	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Specializovaný	specializovaný	k2eAgInSc1d1
web	web	k1gInSc1
o	o	k7c6
zabijačkách	zabijačka	k1gFnPc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zabijačka	zabijačka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Zabijačka	zabijačka	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
zabijačka	zabijačka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4113857-0	4113857-0	k4
</s>
