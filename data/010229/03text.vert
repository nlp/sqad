<p>
<s>
Cuba	Cuba	k6eAd1	Cuba
Gooding	Gooding	k1gInSc1	Gooding
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Oscara	Oscar	k1gMnSc2	Oscar
za	za	k7c4	za
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jerry	Jerra	k1gFnSc2	Jerra
Maguire	Maguir	k1gInSc5	Maguir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
zpěvákovi	zpěvák	k1gMnSc3	zpěvák
Cubovi	Cuba	k1gMnSc3	Cuba
Goodingovi	Gooding	k1gMnSc3	Gooding
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
Shirley	Shirlea	k1gFnSc2	Shirlea
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
bratra	bratr	k1gMnSc4	bratr
herce	herec	k1gMnSc4	herec
Omara	Omar	k1gMnSc4	Omar
Goodinga	Gooding	k1gMnSc4	Gooding
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
skupina	skupina	k1gFnSc1	skupina
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
měla	mít	k5eAaImAgFnS	mít
hit	hit	k1gInSc4	hit
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
opustil	opustit	k5eAaPmAgMnS	opustit
rodinu	rodina	k1gFnSc4	rodina
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Gooding	Gooding	k1gInSc1	Gooding
střídal	střídat	k5eAaImAgInS	střídat
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
rozhovoru	rozhovor	k1gInSc6	rozhovor
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
rodinu	rodina	k1gFnSc4	rodina
otec	otec	k1gMnSc1	otec
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
hotelích	hotel	k1gInPc6	hotel
po	po	k7c4	po
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
Sarou	Sara	k1gFnSc7	Sara
Kapfer	Kapfra	k1gFnPc2	Kapfra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
syny	syn	k1gMnPc4	syn
(	(	kIx(	(
<g/>
Spencer	Spencer	k1gMnSc1	Spencer
a	a	k8xC	a
Mason	mason	k1gMnSc1	mason
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
dceru	dcera	k1gFnSc4	dcera
(	(	kIx(	(
<g/>
Piper	Piper	k1gInSc4	Piper
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
studoval	studovat	k5eAaImAgMnS	studovat
japonské	japonský	k2eAgNnSc4d1	Japonské
bojové	bojový	k2eAgNnSc4d1	bojové
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
hraní	hraní	k1gNnSc4	hraní
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
získal	získat	k5eAaPmAgMnS	získat
epizodní	epizodní	k2eAgFnPc4d1	epizodní
role	role	k1gFnPc4	role
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Hill	Hill	k1gMnSc1	Hill
Street	Street	k1gMnSc1	Street
Blues	blues	k1gNnPc2	blues
a	a	k8xC	a
MacGyver	MacGyvra	k1gFnPc2	MacGyvra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
hlavní	hlavní	k2eAgFnSc1d1	hlavní
role	role	k1gFnSc1	role
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Chlapci	chlapec	k1gMnPc1	chlapec
ze	z	k7c2	z
sousedství	sousedství	k1gNnSc2	sousedství
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
role	role	k1gFnPc1	role
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Pár	pár	k4xCyI	pár
správných	správný	k2eAgMnPc2d1	správný
chlapů	chlap	k1gMnPc2	chlap
<g/>
,	,	kIx,	,
Bleskový	bleskový	k2eAgMnSc1d1	bleskový
Jack	Jack	k1gMnSc1	Jack
a	a	k8xC	a
Smrtící	smrtící	k2eAgFnSc1d1	smrtící
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
získal	získat	k5eAaPmAgMnS	získat
roli	role	k1gFnSc4	role
arogantního	arogantní	k2eAgNnSc2d1	arogantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
loajálního	loajální	k2eAgMnSc2d1	loajální
hráče	hráč	k1gMnSc2	hráč
fotbalu	fotbal	k1gInSc2	fotbal
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Camerona	Cameron	k1gMnSc2	Cameron
Crowea	Croweus	k1gMnSc2	Croweus
Jerry	Jerra	k1gMnSc2	Jerra
Maguire	Maguir	k1gInSc5	Maguir
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
roli	role	k1gFnSc3	role
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stala	stát	k5eAaPmAgFnS	stát
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
získal	získat	k5eAaPmAgMnS	získat
také	také	k9	také
za	za	k7c4	za
roli	role	k1gFnSc4	role
mentálně	mentálně	k6eAd1	mentálně
handicapovaného	handicapovaný	k2eAgMnSc2d1	handicapovaný
muže	muž	k1gMnSc2	muž
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Radio	radio	k1gNnSc1	radio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
dostal	dostat	k5eAaPmAgMnS	dostat
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cuba	Cub	k1gInSc2	Cub
Gooding	Gooding	k1gInSc4	Gooding
<g/>
,	,	kIx,	,
Jr	Jr	k1gFnSc4	Jr
<g/>
.	.	kIx.	.
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cuba	Cub	k1gInSc2	Cub
Gooding	Gooding	k1gInSc1	Gooding
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Cuba	Cuba	k6eAd1	Cuba
Gooding	Gooding	k1gInSc1	Gooding
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Cuba	Cuba	k6eAd1	Cuba
Gooding	Gooding	k1gInSc1	Gooding
mladší	mladý	k2eAgInSc1d2	mladší
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Cuba	Cuba	k6eAd1	Cuba
Gooding	Gooding	k1gInSc1	Gooding
mladší	mladý	k2eAgInSc1d2	mladší
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Cuba	Cuba	k6eAd1	Cuba
Gooding	Gooding	k1gInSc1	Gooding
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
