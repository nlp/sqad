<p>
<s>
Agusta	Agusta	k1gFnSc1	Agusta
AZ	AZ	kA	AZ
<g/>
.8	.8	k4	.8
<g/>
L	L	kA	L
či	či	k8xC	či
Agusta-Zappata	Agusta-Zappata	k1gFnSc1	Agusta-Zappata
AZ	AZ	kA	AZ
<g/>
.8	.8	k4	.8
<g/>
L	L	kA	L
byl	být	k5eAaImAgMnS	být
prototyp	prototyp	k1gInSc4	prototyp
italského	italský	k2eAgInSc2d1	italský
dopravního	dopravní	k2eAgInSc2d1	dopravní
letounu	letoun	k1gInSc2	letoun
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
konvenčně	konvenčně	k6eAd1	konvenčně
řešený	řešený	k2eAgInSc4d1	řešený
dolnoplošník	dolnoplošník	k1gInSc4	dolnoplošník
celokovové	celokovový	k2eAgFnSc2d1	celokovová
konstrukce	konstrukce	k1gFnSc2	konstrukce
s	s	k7c7	s
příďovým	příďový	k2eAgInSc7d1	příďový
podvozkem	podvozek	k1gInSc7	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
dřívější	dřívější	k2eAgInSc4d1	dřívější
návrh	návrh	k1gInSc4	návrh
konstruktéra	konstruktér	k1gMnSc2	konstruktér
Filippo	Filippa	k1gFnSc5	Filippa
Zappaty	Zappat	k2eAgMnPc4d1	Zappat
<g/>
,	,	kIx,	,
dvoumotorový	dvoumotorový	k2eAgInSc4d1	dvoumotorový
AZ	AZ	kA	AZ
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
AZ	AZ	kA	AZ
<g/>
.8	.8	k4	.8
<g/>
L	L	kA	L
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
získat	získat	k5eAaPmF	získat
pozornost	pozornost	k1gFnSc4	pozornost
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
Agusta	Agusta	k1gFnSc1	Agusta
typ	typ	k1gInSc1	typ
opustila	opustit	k5eAaPmAgFnS	opustit
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
program	program	k1gInSc4	program
výroby	výroba	k1gFnSc2	výroba
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nový	nový	k2eAgInSc4d1	nový
Zappatův	Zappatův	k2eAgInSc4d1	Zappatův
projekt	projekt	k1gInSc4	projekt
A	a	k9	a
<g/>
.101	.101	k4	.101
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uživatelé	uživatel	k1gMnPc1	uživatel
==	==	k?	==
</s>
</p>
<p>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
<s>
Aeronautica	Aeronautica	k6eAd1	Aeronautica
Militare	Militar	k1gInSc5	Militar
</s>
</p>
<p>
<s>
==	==	k?	==
Specifikace	specifikace	k1gFnSc1	specifikace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
Osádka	osádka	k1gFnSc1	osádka
<g/>
:	:	kIx,	:
2	[number]	k4	2
</s>
</p>
<p>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
<g/>
:	:	kIx,	:
26	[number]	k4	26
pasažérů	pasažér	k1gMnPc2	pasažér
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
19,44	[number]	k4	19,44
m	m	kA	m
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
25,5	[number]	k4	25,5
m	m	kA	m
</s>
</p>
<p>
<s>
Plocha	plocha	k1gFnSc1	plocha
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
66,8	[number]	k4	66,8
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
6,66	[number]	k4	6,66
m	m	kA	m
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
prázdného	prázdný	k2eAgInSc2d1	prázdný
stroje	stroj	k1gInSc2	stroj
<g/>
:	:	kIx,	:
7	[number]	k4	7
100	[number]	k4	100
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
11	[number]	k4	11
300	[number]	k4	300
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
4	[number]	k4	4
×	×	k?	×
devítiválcový	devítiválcový	k2eAgInSc1d1	devítiválcový
vzduchem	vzduch	k1gInSc7	vzduch
chlazený	chlazený	k2eAgInSc4d1	chlazený
hvězdicový	hvězdicový	k2eAgInSc4d1	hvězdicový
motor	motor	k1gInSc4	motor
Alvis	Alvis	k1gFnSc1	Alvis
Leonides	Leonides	k1gInSc1	Leonides
503	[number]	k4	503
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
Výkon	výkon	k1gInSc1	výkon
pohonné	pohonný	k2eAgFnSc2d1	pohonná
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
400	[number]	k4	400
kW	kW	kA	kW
(	(	kIx(	(
<g/>
536,4	[number]	k4	536,4
hp	hp	k?	hp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vrtule	vrtule	k1gFnPc1	vrtule
<g/>
:	:	kIx,	:
třílisté	třílistý	k2eAgFnPc1d1	třílistá
plynule	plynule	k6eAd1	plynule
stavitelné	stavitelný	k2eAgFnPc1d1	stavitelná
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
427	[number]	k4	427
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
</s>
</p>
<p>
<s>
Dolet	dolet	k1gInSc1	dolet
<g/>
:	:	kIx,	:
2500	[number]	k4	2500
km	km	kA	km
</s>
</p>
<p>
<s>
Praktický	praktický	k2eAgInSc1d1	praktický
dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
7	[number]	k4	7
500	[number]	k4	500
m	m	kA	m
</s>
</p>
<p>
<s>
Stoupavost	stoupavost	k1gFnSc1	stoupavost
<g/>
:	:	kIx,	:
5	[number]	k4	5
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Agusta	Agusta	k1gFnSc1	Agusta
AZ	AZ	kA	AZ
<g/>
.8	.8	k4	.8
<g/>
L	L	kA	L
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
TAYLOR	TAYLOR	kA	TAYLOR
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
J.H.	J.H.	k1gMnSc1	J.H.
Jane	Jan	k1gMnSc5	Jan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Aviation	Aviation	k1gInSc1	Aviation
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Studio	studio	k1gNnSc1	studio
Editions	Editionsa	k1gFnPc2	Editionsa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
40	[number]	k4	40
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SIMPSON	SIMPSON	kA	SIMPSON
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
W.	W.	kA	W.
Airlife	Airlif	k1gInSc5	Airlif
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Helicopters	Helicopters	k1gInSc1	Helicopters
and	and	k?	and
Rotorcraft	Rotorcraft	k1gInSc1	Rotorcraft
<g/>
.	.	kIx.	.
</s>
<s>
Ramsbury	Ramsbura	k1gFnPc1	Ramsbura
<g/>
:	:	kIx,	:
Airlife	Airlif	k1gInSc5	Airlif
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
31	[number]	k4	31
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Agusta	Agusta	k1gFnSc1	Agusta
AZ	AZ	kA	AZ
<g/>
.8	.8	k4	.8
<g/>
L	L	kA	L
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
