<s>
Čchorocku	Čchorocka	k1gFnSc4
</s>
<s>
Čchorocku	Čchorock	k1gInSc2
ჩ	ჩ	k?
Centrum	centrum	k1gNnSc1
města	město	k1gNnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
42	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
42	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
130	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
4	#num#	k4
Stát	stát	k1gInSc1
</s>
<s>
Gruzie	Gruzie	k1gFnSc1
Gruzie	Gruzie	k1gFnSc2
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
gruz	gruz	k1gInSc1
<g/>
:	:	kIx,
mchare	mchar	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Samegrelo-Horní	Samegrelo-Horní	k2eAgFnSc1d1
Svanetie	Svanetie	k1gFnSc1
okres	okres	k1gInSc1
</s>
<s>
Čchorocku	Čchorocka	k1gFnSc4
</s>
<s>
Čchorocku	Čchorocka	k1gFnSc4
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
3	#num#	k4
141	#num#	k4
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
Etnické	etnický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Mingrelci	Mingrelec	k1gMnPc1
(	(	kIx(
<g/>
subetnikum	subetnikum	k1gNnSc1
Gruzínů	Gruzín	k1gMnPc2
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
+	+	kIx~
<g/>
995	#num#	k4
<g/>
)	)	kIx)
417	#num#	k4
PSČ	PSČ	kA
</s>
<s>
5000	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čchorocku	Čchorocka	k1gFnSc4
(	(	kIx(
<g/>
gruzínsky	gruzínsky	k6eAd1
ჩ	ჩ	k?
<g/>
,	,	kIx,
v	v	k7c6
překladu	překlad	k1gInSc6
devět	devět	k4xCc1
pramenů	pramen	k1gInPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
západogruzínské	západogruzínský	k2eAgNnSc1d1
okresní	okresní	k2eAgNnSc1d1
město	město	k1gNnSc1
stejnojmenného	stejnojmenný	k2eAgInSc2d1
okresu	okres	k1gInSc2
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
nížinné	nížinný	k2eAgFnSc6d1
části	část	k1gFnSc6
regionu	region	k1gInSc2
Samegrelo-Horní	Samegrelo-Horní	k2eAgFnSc2d1
Svanetie	Svanetie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
řece	řeka	k1gFnSc6
Chobi	Chob	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
známé	známý	k2eAgInPc1d1
archeologickými	archeologický	k2eAgFnPc7d1
vykopávkami	vykopávka	k1gFnPc7
z	z	k7c2
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
muzeum	muzeum	k1gNnSc1
s	s	k7c7
22	#num#	k4
390	#num#	k4
exponáty	exponát	k1gInPc7
z	z	k7c2
doby	doba	k1gFnSc2
kamenné	kamenný	k2eAgFnSc2d1
a	a	k8xC
bronzové	bronzový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
