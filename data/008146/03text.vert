<s>
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Chang-čcheng	Chang-čchenga	k1gFnPc2	Chang-čchenga
<g/>
,	,	kIx,	,
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Chángchéng	Chángchénga	k1gFnPc2	Chángchénga
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
长	长	k?	长
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc6d1	tradiční
長	長	k?	長
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
zeď	zeď	k1gFnSc1	zeď
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
systém	systém	k1gInSc1	systém
opevnění	opevnění	k1gNnSc2	opevnění
táhnoucí	táhnoucí	k2eAgMnSc1d1	táhnoucí
se	se	k3xPyFc4	se
napříč	napříč	k7c7	napříč
severní	severní	k2eAgFnSc7d1	severní
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
zdi	zeď	k1gFnSc2	zeď
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
za	za	k7c4	za
dynastie	dynastie	k1gFnPc4	dynastie
Ming	Minga	k1gFnPc2	Minga
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
konce	konec	k1gInSc2	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
chránit	chránit	k5eAaImF	chránit
Čínu	Čína	k1gFnSc4	Čína
před	před	k7c7	před
mongolskými	mongolský	k2eAgInPc7d1	mongolský
nájezdy	nájezd	k1gInPc7	nájezd
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
opevnění	opevněný	k2eAgMnPc1d1	opevněný
proti	proti	k7c3	proti
vpádům	vpád	k1gInPc3	vpád
kočovných	kočovný	k2eAgFnPc2d1	kočovná
kmenů	kmen	k1gInPc2	kmen
byla	být	k5eAaImAgFnS	být
však	však	k9	však
budována	budovat	k5eAaImNgFnS	budovat
již	již	k9	již
v	v	k7c6	v
dávnějších	dávný	k2eAgFnPc6d2	dávnější
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
cca	cca	kA	cca
3460	[number]	k4	3460
km	km	kA	km
(	(	kIx(	(
<g/>
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
odbočkami	odbočka	k1gFnPc7	odbočka
8858,8	[number]	k4	8858,8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východní	východní	k2eAgInSc1d1	východní
počátek	počátek	k1gInSc1	počátek
má	mít	k5eAaImIp3nS	mít
v	v	k7c4	v
Šan-chaj-kuan	Šanhajuan	k1gInSc4	Šan-chaj-kuan
při	při	k7c6	při
Pochajském	Pochajský	k2eAgInSc6d1	Pochajský
zálivu	záliv	k1gInSc6	záliv
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Che-pej	Cheej	k1gFnSc2	Che-pej
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
vlastní	vlastní	k2eAgFnSc2d1	vlastní
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
daleko	daleko	k6eAd1	daleko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
u	u	k7c2	u
Ťia-jü-kuan	Ťiaüuana	k1gFnPc2	Ťia-jü-kuana
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Kan-su	Kan	k1gInSc2	Kan-s
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
pouště	poušť	k1gFnSc2	poušť
Gobi	Gobi	k1gFnSc2	Gobi
<g/>
,	,	kIx,	,
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
přes	přes	k7c4	přes
roztroušené	roztroušený	k2eAgFnPc4d1	roztroušená
oázy	oáza	k1gFnPc4	oáza
kdysi	kdysi	k6eAd1	kdysi
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
Hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zeď	zeď	k1gFnSc1	zeď
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
úsecích	úsek	k1gInPc6	úsek
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
několika	několik	k4yIc6	několik
liniích	linie	k1gFnPc6	linie
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
odbočky	odbočka	k1gFnSc2	odbočka
(	(	kIx(	(
<g/>
započteno	započten	k2eAgNnSc1d1	započteno
do	do	k7c2	do
celkové	celkový	k2eAgFnSc2d1	celková
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
čínské	čínský	k2eAgInPc4d1	čínský
úřady	úřad	k1gInPc4	úřad
ohlásily	ohlásit	k5eAaPmAgFnP	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
dosud	dosud	k6eAd1	dosud
neznámé	známý	k2eNgInPc1d1	neznámý
zbytky	zbytek	k1gInPc1	zbytek
čínské	čínský	k2eAgFnSc2d1	čínská
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
zdi	zeď	k1gFnSc2	zeď
dnes	dnes	k6eAd1	dnes
tedy	tedy	k9	tedy
činí	činit	k5eAaImIp3nS	činit
8851	[number]	k4	8851
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
zeď	zeď	k1gFnSc4	zeď
nazývají	nazývat	k5eAaImIp3nP	nazývat
Wan	Wan	k1gFnPc1	Wan
<g/>
-li	i	k?	-li
čchang-čcheng	čchang-čcheng	k1gMnSc1	čchang-čcheng
(	(	kIx(	(
<g/>
ZZ	ZZ	kA	ZZ
<g/>
:	:	kIx,	:
万	万	k?	万
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
Wà	Wà	k1gMnSc1	Wà
Chángchéng	Chángchéng	k1gMnSc1	Chángchéng
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Zeď	zeď	k1gFnSc1	zeď
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInSc4	tisíc
li	li	k8xS	li
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInSc4	tisíc
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
tradičně	tradičně	k6eAd1	tradičně
synonymem	synonymum	k1gNnSc7	synonymum
nezměrného	změrný	k2eNgNnSc2d1	nezměrné
množství	množství	k1gNnSc2	množství
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
výstižnější	výstižný	k2eAgInSc1d2	výstižnější
překlad	překlad	k1gInSc1	překlad
"	"	kIx"	"
<g/>
Nekonečně	konečně	k6eNd1	konečně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
První	první	k4xOgFnSc1	první
Velká	velký	k2eAgFnSc1d1	velká
zeď	zeď	k1gFnSc1	zeď
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Prvního	první	k4xOgMnSc4	první
císaře	císař	k1gMnSc4	císař
(	(	kIx(	(
<g/>
Čchin	Čchin	k1gMnSc1	Čchin
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-chuang-ti	huang	k5eAaPmF	-chuang-t
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
epizodické	epizodický	k2eAgFnSc2d1	epizodická
dynastie	dynastie	k1gFnSc2	dynastie
Čchin	Čchin	k1gMnSc1	Čchin
(	(	kIx(	(
<g/>
Qin	Qin	k1gMnSc1	Qin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
byly	být	k5eAaImAgInP	být
nasazeny	nasazen	k2eAgInPc1d1	nasazen
statisíce	statisíce	k1gInPc1	statisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
především	především	k9	především
zajatců	zajatec	k1gMnPc2	zajatec
a	a	k8xC	a
trestanců	trestanec	k1gMnPc2	trestanec
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
budování	budování	k1gNnSc6	budování
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
již	již	k6eAd1	již
existujících	existující	k2eAgInPc2d1	existující
úseků	úsek	k1gInPc2	úsek
opevnění	opevnění	k1gNnSc2	opevnění
postavených	postavený	k2eAgInPc2d1	postavený
za	za	k7c2	za
časů	čas	k1gInPc2	čas
Období	období	k1gNnSc2	období
Válčících	válčící	k2eAgInPc2d1	válčící
států	stát	k1gInPc2	stát
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Probíhala	probíhat	k5eAaImAgFnS	probíhat
daleko	daleko	k6eAd1	daleko
severněji	severně	k6eAd2	severně
než	než	k8xS	než
zeď	zeď	k1gFnSc1	zeď
dnešní	dnešní	k2eAgFnSc1d1	dnešní
a	a	k8xC	a
dochovaly	dochovat	k5eAaPmAgInP	dochovat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
jen	jen	k9	jen
nepatrné	nepatrný	k2eAgInPc4d1	nepatrný
zbytky	zbytek	k1gInPc4	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
následující	následující	k2eAgFnPc1d1	následující
dynastie	dynastie	k1gFnPc1	dynastie
budovaly	budovat	k5eAaImAgFnP	budovat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
či	či	k8xC	či
menší	malý	k2eAgFnSc3d2	menší
míře	míra	k1gFnSc3	míra
obranné	obranný	k2eAgFnSc2d1	obranná
zdi	zeď	k1gFnSc2	zeď
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
získala	získat	k5eAaPmAgFnS	získat
zeď	zeď	k1gFnSc1	zeď
za	za	k7c4	za
dynastie	dynastie	k1gFnPc4	dynastie
Ming	Minga	k1gFnPc2	Minga
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
do	do	k7c2	do
velkorysých	velkorysý	k2eAgInPc2d1	velkorysý
rozměrů	rozměr	k1gInPc2	rozměr
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
trvanlivých	trvanlivý	k2eAgInPc2d1	trvanlivý
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
pálené	pálený	k2eAgFnPc1d1	pálená
cihly	cihla	k1gFnPc1	cihla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
pochopitelně	pochopitelně	k6eAd1	pochopitelně
neměla	mít	k5eNaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
odrazit	odrazit	k5eAaPmF	odrazit
masívní	masívní	k2eAgInSc4d1	masívní
útok	útok	k1gInSc4	útok
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
hradby	hradba	k1gFnSc2	hradba
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
zlézt	zlézt	k5eAaPmF	zlézt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
znesnadnit	znesnadnit	k5eAaPmF	znesnadnit
polokočovným	polokočovný	k2eAgInPc3d1	polokočovný
kmenům	kmen	k1gInPc3	kmen
podnikání	podnikání	k1gNnSc2	podnikání
loupeživých	loupeživý	k2eAgInPc2d1	loupeživý
nájezdů	nájezd	k1gInPc2	nájezd
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Čchin	Čchin	k1gInSc1	Čchin
(	(	kIx(	(
<g/>
Qin	Qin	k1gFnSc1	Qin
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
210	[number]	k4	210
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Chan	Chana	k1gFnPc2	Chana
(	(	kIx(	(
<g/>
Han	Hana	k1gFnPc2	Hana
<g/>
)	)	kIx)	)
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
období	období	k1gNnSc6	období
říše	říš	k1gFnSc2	říš
Ťin	Ťin	k1gFnSc2	Ťin
(	(	kIx(	(
<g/>
1138	[number]	k4	1138
<g/>
–	–	k?	–
<g/>
1198	[number]	k4	1198
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severněji	severně	k6eAd2	severně
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
čínské	čínský	k2eAgFnSc2d1	čínská
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
vnějšího	vnější	k2eAgNnSc2d1	vnější
a	a	k8xC	a
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Ming	k1gInSc1	Ming
(	(	kIx(	(
<g/>
1473	[number]	k4	1473
<g/>
–	–	k?	–
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
Odedávna	odedávna	k6eAd1	odedávna
užívaným	užívaný	k2eAgInSc7d1	užívaný
způsobem	způsob	k1gInSc7	způsob
budování	budování	k1gNnSc2	budování
opevnění	opevnění	k1gNnSc2	opevnění
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
bylo	být	k5eAaImAgNnS	být
dusání	dusání	k1gNnSc1	dusání
zeminy	zemina	k1gFnSc2	zemina
po	po	k7c6	po
tenkých	tenký	k2eAgFnPc6d1	tenká
vrstvách	vrstva	k1gFnPc6	vrstva
do	do	k7c2	do
připraveného	připravený	k2eAgNnSc2d1	připravené
bednění	bednění	k1gNnSc2	bednění
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
velmi	velmi	k6eAd1	velmi
pevné	pevný	k2eAgNnSc1d1	pevné
jádro	jádro	k1gNnSc1	jádro
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
obezděno	obezděn	k2eAgNnSc1d1	obezděno
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
rozmanitý	rozmanitý	k2eAgInSc1d1	rozmanitý
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
Pekingu	Peking	k1gInSc2	Peking
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
Velké	velká	k1gFnSc2	velká
zdi	zeď	k1gFnSc3	zeď
používány	používat	k5eAaImNgInP	používat
vápencové	vápencový	k2eAgInPc1d1	vápencový
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
žula	žula	k1gFnSc1	žula
a	a	k8xC	a
pálené	pálený	k2eAgFnPc1d1	pálená
cihly	cihla	k1gFnPc1	cihla
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
místních	místní	k2eAgInPc6d1	místní
zdrojích	zdroj	k1gInPc6	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
zeď	zeď	k1gFnSc1	zeď
zpravidla	zpravidla	k6eAd1	zpravidla
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
obtížně	obtížně	k6eAd1	obtížně
prostupném	prostupný	k2eAgInSc6d1	prostupný
terénu	terén	k1gInSc6	terén
po	po	k7c6	po
hřebenech	hřeben	k1gInPc6	hřeben
hor.	hor.	k?	hor.
Parametry	parametr	k1gInPc1	parametr
stavby	stavba	k1gFnSc2	stavba
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nS	různit
-	-	kIx~	-
opevnění	opevnění	k1gNnSc1	opevnění
z	z	k7c2	z
mingské	mingský	k2eAgFnSc2d1	mingský
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
paty	pata	k1gFnSc2	pata
šířku	šířka	k1gFnSc4	šířka
kolem	kolem	k7c2	kolem
7	[number]	k4	7
až	až	k9	až
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
kolem	kolem	k7c2	kolem
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
6	[number]	k4	6
až	až	k9	až
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
zdi	zeď	k1gFnSc2	zeď
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
spojovací	spojovací	k2eAgFnPc4d1	spojovací
chodby	chodba	k1gFnPc4	chodba
a	a	k8xC	a
skladovací	skladovací	k2eAgFnPc4d1	skladovací
prostory	prostora	k1gFnPc4	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Každých	každý	k3xTgMnPc2	každý
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
zeď	zeď	k1gFnSc1	zeď
zesílena	zesílen	k2eAgFnSc1d1	zesílena
věžemi	věž	k1gFnPc7	věž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
pozorovací	pozorovací	k2eAgNnSc4d1	pozorovací
a	a	k8xC	a
signální	signální	k2eAgNnSc4d1	signální
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
,	,	kIx,	,
skladiště	skladiště	k1gNnSc4	skladiště
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
i	i	k9	i
jako	jako	k8xC	jako
útočiště	útočiště	k1gNnSc1	útočiště
obránců	obránce	k1gMnPc2	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
věží	věž	k1gFnPc2	věž
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
rozestupech	rozestup	k1gInPc6	rozestup
pak	pak	k8xC	pak
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
kasárny	kasárny	k1gFnPc1	kasárny
<g/>
,	,	kIx,	,
zásobovací	zásobovací	k2eAgNnSc1d1	zásobovací
skladiště	skladiště	k1gNnSc1	skladiště
a	a	k8xC	a
velitelství	velitelství	k1gNnSc1	velitelství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
předávání	předávání	k1gNnSc3	předávání
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
nepřítele	nepřítel	k1gMnSc2	nepřítel
používaly	používat	k5eAaImAgFnP	používat
strážní	strážní	k2eAgFnPc1d1	strážní
hlídky	hlídka	k1gFnPc1	hlídka
kouřové	kouřový	k2eAgFnPc1d1	kouřová
signály	signál	k1gInPc4	signál
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
mingském	mingský	k2eAgNnSc6d1	mingský
období	období	k1gNnSc6	období
znamenal	znamenat	k5eAaImAgInS	znamenat
jeden	jeden	k4xCgInSc4	jeden
kouřový	kouřový	k2eAgInSc4d1	kouřový
signál	signál	k1gInSc4	signál
počet	počet	k1gInSc1	počet
100	[number]	k4	100
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
signály	signál	k1gInPc1	signál
500	[number]	k4	500
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
signály	signál	k1gInPc4	signál
přes	přes	k7c4	přes
1000	[number]	k4	1000
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
zeď	zeď	k1gFnSc1	zeď
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
slova	slovo	k1gNnSc2	slovo
písně	píseň	k1gFnSc2	píseň
Pochod	pochod	k1gInSc1	pochod
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
státní	státní	k2eAgFnSc2d1	státní
hymny	hymna	k1gFnSc2	hymna
ČLR	ČLR	kA	ČLR
<g/>
)	)	kIx)	)
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
novou	nový	k2eAgFnSc4d1	nová
Velkou	velký	k2eAgFnSc4d1	velká
zeď	zeď	k1gFnSc4	zeď
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mají	mít	k5eAaImIp3nP	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
čínští	čínský	k2eAgMnPc1d1	čínský
vlastenci	vlastenec	k1gMnPc1	vlastenec
tváří	tvářet	k5eAaImIp3nP	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
nepřátelské	přátelský	k2eNgFnSc3d1	nepřátelská
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
zdi	zeď	k1gFnSc3	zeď
dosud	dosud	k6eAd1	dosud
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
;	;	kIx,	;
v	v	k7c6	v
odlehlejších	odlehlý	k2eAgNnPc6d2	odlehlejší
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
zchátralá	zchátralý	k2eAgFnSc1d1	zchátralá
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rozebírání	rozebírání	k1gNnSc3	rozebírání
na	na	k7c4	na
stavební	stavební	k2eAgInSc4d1	stavební
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Zrestaurovány	zrestaurován	k2eAgFnPc1d1	zrestaurována
a	a	k8xC	a
turisticky	turisticky	k6eAd1	turisticky
zpřístupněny	zpřístupněn	k2eAgFnPc1d1	zpřístupněna
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
některé	některý	k3yIgInPc1	některý
menší	malý	k2eAgInPc1d2	menší
úseky	úsek	k1gInPc1	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
nových	nový	k2eAgInPc2d1	nový
sedm	sedm	k4xCc4	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
jako	jako	k8xS	jako
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
stavba	stavba	k1gFnSc1	stavba
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velká	velká	k1gFnSc1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
o	o	k7c6	o
Velké	velký	k2eAgFnSc6d1	velká
čínské	čínský	k2eAgFnSc6d1	čínská
zdi	zeď	k1gFnSc6	zeď
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Video	video	k1gNnSc1	video
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
čínské	čínský	k2eAgFnSc2d1	čínská
zdi	zeď	k1gFnSc2	zeď
-	-	kIx~	-
sekce	sekce	k1gFnSc1	sekce
Mutianyu	Mutianyus	k1gInSc2	Mutianyus
nedaleko	nedaleko	k7c2	nedaleko
Pekingu	Peking	k1gInSc2	Peking
Mapka	mapka	k1gFnSc1	mapka
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
fotografií	fotografia	k1gFnPc2	fotografia
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Great	Great	k1gInSc1	Great
Wall	Wall	k1gMnSc1	Wall
(	(	kIx(	(
<g/>
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
</s>
