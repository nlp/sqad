<s>
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Chang-čcheng	Chang-čchenga	k1gFnPc2	Chang-čchenga
<g/>
,	,	kIx,	,
pchin-jinem	pchinino	k1gNnSc7	pchin-jino
Chángchéng	Chángchénga	k1gFnPc2	Chángchénga
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
长	长	k?	长
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgFnSc6d1	tradiční
長	長	k?	長
<g/>
;	;	kIx,	;
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
zeď	zeď	k1gFnSc1	zeď
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
systém	systém	k1gInSc1	systém
opevnění	opevnění	k1gNnSc2	opevnění
táhnoucí	táhnoucí	k2eAgMnSc1d1	táhnoucí
se	se	k3xPyFc4	se
napříč	napříč	k7c7	napříč
severní	severní	k2eAgFnSc7d1	severní
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
