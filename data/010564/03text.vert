<p>
<s>
Limoges	Limoges	k1gInSc1	Limoges
Cercle	cercle	k1gInSc1	cercle
Saint-Pierre	Saint-Pierr	k1gInSc5	Saint-Pierr
je	on	k3xPp3gMnPc4	on
francouzský	francouzský	k2eAgInSc1d1	francouzský
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Týmové	týmový	k2eAgFnPc1d1	týmová
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
zelená	zelený	k2eAgNnPc1d1	zelené
a	a	k8xC	a
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgInPc1d1	domácí
zápasy	zápas	k1gInPc1	zápas
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Palais	Palais	k1gFnSc2	Palais
des	des	k1gNnSc1	des
sports	sports	k1gInSc1	sports
de	de	k?	de
Beaublanc	Beaublanc	k1gInSc1	Beaublanc
v	v	k7c4	v
Limoges	Limoges	k1gInSc4	Limoges
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
je	být	k5eAaImIp3nS	být
účastníkem	účastník	k1gMnSc7	účastník
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
francouzské	francouzský	k2eAgFnSc2d1	francouzská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jedenáctkrát	jedenáctkrát	k6eAd1	jedenáctkrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Historickým	historický	k2eAgInSc7d1	historický
úspěchem	úspěch	k1gInSc7	úspěch
je	být	k5eAaImIp3nS	být
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
Eurolize	Euroliz	k1gInSc6	Euroliz
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Limoges	Limogesa	k1gFnPc2	Limogesa
porazilo	porazit	k5eAaPmAgNnS	porazit
italského	italský	k2eAgMnSc2d1	italský
mistra	mistr	k1gMnSc2	mistr
Benetton	Benetton	k1gInSc4	Benetton
Treviso	Trevisa	k1gFnSc5	Trevisa
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1993	[number]	k4	1993
a	a	k8xC	a
CSP	CSP	kA	CSP
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
francouzským	francouzský	k2eAgInSc7d1	francouzský
klubem	klub	k1gInSc7	klub
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
pohárovou	pohárový	k2eAgFnSc4d1	pohárová
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
kolektivním	kolektivní	k2eAgInSc6d1	kolektivní
sportu	sport	k1gInSc6	sport
<g/>
:	:	kIx,	:
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
fotbalistům	fotbalista	k1gMnPc3	fotbalista
Olympique	Olympique	k1gInSc1	Olympique
de	de	k?	de
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
klub	klub	k1gInSc4	klub
hráli	hrát	k5eAaImAgMnP	hrát
stošedesátinásobný	stošedesátinásobný	k2eAgMnSc1d1	stošedesátinásobný
reprezentant	reprezentant	k1gMnSc1	reprezentant
Francie	Francie	k1gFnSc2	Francie
Richard	Richard	k1gMnSc1	Richard
Dacoury	Dacoura	k1gFnSc2	Dacoura
<g/>
,	,	kIx,	,
Slovinec	Slovinec	k1gMnSc1	Slovinec
Jure	Jur	k1gFnSc2	Jur
Zdovc	Zdovc	k1gFnSc1	Zdovc
nebo	nebo	k8xC	nebo
Američan	Američan	k1gMnSc1	Američan
Billy	Bill	k1gMnPc4	Bill
Knight	Knight	k2eAgMnSc1d1	Knight
<g/>
,	,	kIx,	,
zvolený	zvolený	k2eAgMnSc1d1	zvolený
do	do	k7c2	do
all-stars	alltarsa	k1gFnPc2	all-starsa
týmu	tým	k1gInSc2	tým
National	National	k1gMnSc1	National
Basketball	Basketball	k1gMnSc1	Basketball
Association	Association	k1gInSc4	Association
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
Francie	Francie	k1gFnSc2	Francie
<g/>
:	:	kIx,	:
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
88	[number]	k4	88
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
89	[number]	k4	89
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
93	[number]	k4	93
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
2014-15	[number]	k4	2014-15
</s>
</p>
<p>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
francouzského	francouzský	k2eAgInSc2d1	francouzský
poháru	pohár	k1gInSc2	pohár
<g/>
:	:	kIx,	:
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
83	[number]	k4	83
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
<g/>
,	,	kIx,	,
1999-00	[number]	k4	1999-00
</s>
</p>
<p>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
Euroligy	Euroliga	k1gFnSc2	Euroliga
<g/>
:	:	kIx,	:
1992-93	[number]	k4	1992-93
</s>
</p>
<p>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
Koračova	Koračův	k2eAgInSc2d1	Koračův
poháru	pohár	k1gInSc2	pohár
<g/>
:	:	kIx,	:
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
,	,	kIx,	,
1999-2000	[number]	k4	1999-2000
</s>
</p>
<p>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
Saportova	Saportův	k2eAgInSc2d1	Saportův
poháru	pohár	k1gInSc2	pohár
<g/>
:	:	kIx,	:
1987-88	[number]	k4	1987-88
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Limoges	Limogesa	k1gFnPc2	Limogesa
CSP	CSP	kA	CSP
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
