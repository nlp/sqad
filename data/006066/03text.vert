<s>
Ionizací	ionizace	k1gFnSc7	ionizace
plynu	plyn	k1gInSc2	plyn
vzniká	vznikat	k5eAaImIp3nS	vznikat
ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ionizaci	ionizace	k1gFnSc4	ionizace
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
molekuly	molekula	k1gFnPc1	molekula
plynu	plyn	k1gInSc2	plyn
rozštěpí	rozštěpit	k5eAaPmIp3nP	rozštěpit
na	na	k7c4	na
elektron	elektron	k1gInSc4	elektron
a	a	k8xC	a
kladný	kladný	k2eAgInSc4d1	kladný
iont	iont	k1gInSc4	iont
<g/>
.	.	kIx.	.
</s>
<s>
Plamen	plamen	k1gInSc1	plamen
nebo	nebo	k8xC	nebo
záření	záření	k1gNnSc2	záření
jsou	být	k5eAaImIp3nP	být
ionizátory	ionizátor	k1gInPc1	ionizátor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dodávají	dodávat	k5eAaImIp3nP	dodávat
elektronům	elektron	k1gInPc3	elektron
energii	energie	k1gFnSc4	energie
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
odtržení	odtržení	k1gNnSc3	odtržení
<g/>
.	.	kIx.	.
</s>
<s>
Elektron	elektron	k1gInSc1	elektron
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
neutrální	neutrální	k2eAgFnSc7d1	neutrální
molekulou	molekula	k1gFnSc7	molekula
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
záporný	záporný	k2eAgInSc4d1	záporný
iont	iont	k1gInSc4	iont
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
ionizován	ionizovat	k5eAaBmNgInS	ionizovat
účinkem	účinek	k1gInSc7	účinek
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c4	v
1	[number]	k4	1
cm3	cm3	k4	cm3
vzduchu	vzduch	k1gInSc2	vzduch
každou	každý	k3xTgFnSc4	každý
sekundu	sekunda	k1gFnSc4	sekunda
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
kladných	kladný	k2eAgMnPc2d1	kladný
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgFnPc1d1	elektrická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
můžeme	moct	k5eAaImIp1nP	moct
měřit	měřit	k5eAaImF	měřit
pomocí	pomocí	k7c2	pomocí
ionizační	ionizační	k2eAgFnSc2d1	ionizační
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
jako	jako	k8xS	jako
deskový	deskový	k2eAgInSc1d1	deskový
kondenzátor	kondenzátor	k1gInSc1	kondenzátor
v	v	k7c6	v
kovovém	kovový	k2eAgInSc6d1	kovový
krytu	kryt	k1gInSc6	kryt
s	s	k7c7	s
okénkem	okénko	k1gNnSc7	okénko
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
mezi	mezi	k7c7	mezi
deskami	deska	k1gFnPc7	deska
proniká	pronikat	k5eAaImIp3nS	pronikat
ionizující	ionizující	k2eAgNnSc1d1	ionizující
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvyšování	zvyšování	k1gNnSc6	zvyšování
napětí	napětí	k1gNnSc2	napětí
získáváme	získávat	k5eAaImIp1nP	získávat
měřením	měření	k1gNnSc7	měření
odporu	odpor	k1gInSc2	odpor
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
voltampérovou	voltampérový	k2eAgFnSc4d1	voltampérová
charakteristiku	charakteristika	k1gFnSc4	charakteristika
výboje	výboj	k1gInSc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Formou	forma	k1gFnSc7	forma
ionizovaného	ionizovaný	k2eAgInSc2d1	ionizovaný
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
