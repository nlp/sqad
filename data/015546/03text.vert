<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Napoleonské	napoleonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
François	François	k1gInSc1
Gérard	Gérard	k1gInSc1
<g/>
:	:	kIx,
Vítězný	vítězný	k2eAgInSc1d1
návrat	návrat	k1gInSc1
generála	generál	k1gMnSc2
Rappa	Rapp	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
Napoleonovi	Napoleon	k1gMnSc3
přiváží	přivázat	k5eAaPmIp3nP,k5eAaImIp3nP
ukořistěné	ukořistěný	k2eAgInPc1d1
ruské	ruský	k2eAgInPc1d1
prapory	prapor	k1gInPc1
a	a	k8xC
zajatého	zajatý	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
Repnina	Repnin	k2eAgInSc2d1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1805	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
západně	západně	k6eAd1
od	od	k7c2
Slavkova	Slavkov	k1gInSc2
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
Morava	Morava	k1gFnSc1
</s>
<s>
zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
47	#num#	k4
<g/>
′	′	k?
<g/>
7	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
drtivé	drtivý	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
zánik	zánik	k1gInSc1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgMnPc4d1
a	a	k8xC
posléze	posléze	k6eAd1
vznik	vznik	k1gInSc4
Rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Svatá	svatat	k5eAaImIp3nS
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Napoleon	Napoleon	k1gMnSc1
I.	I.	kA
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
M.	M.	kA
I.	I.	kA
Kutuzov	Kutuzov	k1gInSc1
František	františek	k1gInSc1
I.	I.	kA
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
cca	cca	kA
75	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
~	~	kIx~
160	#num#	k4
<g/>
–	–	k?
<g/>
200	#num#	k4
děl	dělo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
cca	cca	kA
75	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
cca	cca	kA
16	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
~	~	kIx~
260	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
děl	dělo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
8649	#num#	k4
<g/>
~	~	kIx~
1398	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
~	~	kIx~
7260	#num#	k4
zraněných	zraněný	k2eAgMnPc2d1
<g/>
~	~	kIx~
94	#num#	k4
<g/>
–	–	k?
<g/>
532	#num#	k4
zajatci	zajatec	k1gMnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
21	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
518	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
~	~	kIx~
asi	asi	k9
16	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
zraněných	zraněný	k1gMnPc2
<g/>
~	~	kIx~
asi	asi	k9
9500	#num#	k4
zajatců	zajatec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
5922	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
~	~	kIx~
asi	asi	k9
600	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
~	~	kIx~
asi	asi	k9
1200	#num#	k4
zraněných	zraněný	k1gMnPc2
<g/>
~	~	kIx~
asi	asi	k9
1670	#num#	k4
zajatců	zajatec	k1gMnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
spojenců	spojenec	k1gMnPc2
<g/>
~	~	kIx~
45	#num#	k4
standart	standarta	k1gFnPc2
<g/>
~	~	kIx~
185	#num#	k4
děl	dělo	k1gNnPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgFnSc1d1
též	též	k9
bitvou	bitva	k1gFnSc7
tří	tři	k4xCgMnPc2
císařů	císař	k1gMnPc2
nebo	nebo	k8xC
zřídka	zřídka	k6eAd1
bitvou	bitva	k1gFnSc7
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xC,k8xS
bitva	bitva	k1gFnSc1
u	u	k7c2
Austerlitz	Austerlitza	k1gFnPc2
(	(	kIx(
<g/>
německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
Slavkova	Slavkov	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
hlavních	hlavní	k2eAgNnPc2d1
střetnutí	střetnutí	k1gNnPc2
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejslavnějších	slavný	k2eAgFnPc2d3
bitev	bitva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
kdy	kdy	k6eAd1
odehrály	odehrát	k5eAaPmAgFnP
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
prosincegreg	prosincegreg	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
listopadujul	listopadujout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
roku	rok	k1gInSc2
1805	#num#	k4
(	(	kIx(
<g/>
nebo	nebo	k8xC
též	též	k9
11	#num#	k4
<g/>
.	.	kIx.
frimairu	frimairo	k1gNnSc6
roku	rok	k1gInSc2
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
franc	franc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
na	na	k7c6
Moravě	Morava	k1gFnSc6
v	v	k7c6
prostoru	prostor	k1gInSc6
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Brna	Brno	k1gNnSc2
a	a	k8xC
západně	západně	k6eAd1
od	od	k7c2
Slavkova	Slavkov	k1gInSc2
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
císařem	císař	k1gMnSc7
Napoleonem	Napoleon	k1gMnSc7
zde	zde	k6eAd1
drtivě	drtivě	k6eAd1
porazila	porazit	k5eAaPmAgFnS
vojsko	vojsko	k1gNnSc4
III	III	kA
<g/>
.	.	kIx.
koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
zastupovali	zastupovat	k5eAaImAgMnP
vojáci	voják	k1gMnPc1
Ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
carem	car	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
I.	I.	kA
a	a	k8xC
generálem	generál	k1gMnSc7
M.	M.	kA
I.	I.	kA
Kutuzovem	Kutuzov	k1gInSc7
a	a	k8xC
oddíly	oddíl	k1gInPc1
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
pod	pod	k7c7
vrchním	vrchní	k2eAgNnSc7d1
velením	velení	k1gNnSc7
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
I.	I.	kA
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
patří	patřit	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
bitvou	bitva	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Gaugamél	Gaugaméla	k1gFnPc2
<g/>
,	,	kIx,
bitvou	bitva	k1gFnSc7
u	u	k7c2
Kann	Kanny	k1gFnPc2
a	a	k8xC
bitvou	bitva	k1gFnSc7
u	u	k7c2
Leuthenu	Leuthen	k1gInSc2
mezi	mezi	k7c4
mistrovská	mistrovský	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
taktiky	taktika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Samotné	samotný	k2eAgNnSc1d1
Napoleonovo	Napoleonův	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
nejenže	nejenže	k6eAd1
rozbilo	rozbít	k5eAaPmAgNnS
svazek	svazek	k1gInSc4
III	III	kA
<g/>
.	.	kIx.
koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
mělo	mít	k5eAaImAgNnS
také	také	k9
vliv	vliv	k1gInSc4
na	na	k7c4
uspořádání	uspořádání	k1gNnSc4
státních	státní	k2eAgInPc2d1
celků	celek	k1gInPc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
tehdejší	tehdejší	k2eAgFnSc6d1
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
v	v	k7c6
římskoněmecké	římskoněmecký	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
francouzský	francouzský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
řídil	řídit	k5eAaImAgMnS
úvodní	úvodní	k2eAgFnPc4d1
fáze	fáze	k1gFnPc4
bitvy	bitva	k1gFnSc2
z	z	k7c2
návrší	návrší	k1gNnSc2
Žuráň	Žuráň	k1gFnSc1
(	(	kIx(
<g/>
286	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
v	v	k7c6
katastru	katastr	k1gInSc6
obce	obec	k1gFnSc2
Podolí	Podolí	k1gNnSc2
v	v	k7c6
okrese	okres	k1gInSc6
Brno-venkov	Brno-venkov	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
František	František	k1gMnSc1
I.	I.	kA
s	s	k7c7
carem	car	k1gMnSc7
Alexandrem	Alexandr	k1gMnSc7
měli	mít	k5eAaImAgMnP
svůj	svůj	k3xOyFgInSc4
hlavní	hlavní	k2eAgInSc4d1
stan	stan	k1gInSc4
zřízený	zřízený	k2eAgInSc4d1
v	v	k7c6
Křenovicích	Křenovice	k1gFnPc6
<g/>
,	,	kIx,
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
bitvu	bitva	k1gFnSc4
podle	podle	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
,	,	kIx,
vzdáleného	vzdálený	k2eAgMnSc2d1
od	od	k7c2
centra	centrum	k1gNnSc2
bojiště	bojiště	k1gNnSc2
zhruba	zhruba	k6eAd1
9	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údajně	údajně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
slavkovském	slavkovský	k2eAgInSc6d1
zámku	zámek	k1gInSc6
den	den	k1gInSc1
před	před	k7c7
bitvou	bitva	k1gFnSc7
nocovali	nocovat	k5eAaImAgMnP
oba	dva	k4xCgMnPc4
spojenečtí	spojenecký	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
snad	snad	k9
i	i	k9
na	na	k7c4
počest	počest	k1gFnSc4
zámku	zámek	k1gInSc2
samotného	samotný	k2eAgInSc2d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
na	na	k7c4
Napoleona	Napoleon	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
ubytoval	ubytovat	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
dojem	dojem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vzpomínkové	vzpomínkový	k2eAgFnPc1d1
akce	akce	k1gFnPc1
spojené	spojený	k2eAgFnPc1d1
s	s	k7c7
rekonstrukcí	rekonstrukce	k1gFnSc7
bitvy	bitva	k1gFnSc2
probíhají	probíhat	k5eAaImIp3nP
od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vždy	vždy	k6eAd1
o	o	k7c6
víkendu	víkend	k1gInSc6
kalendářně	kalendářně	k6eAd1
nejblíže	blízce	k6eAd3
2	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Střediskem	středisko	k1gNnSc7
vzpomínkových	vzpomínkový	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
je	být	k5eAaImIp3nS
památník	památník	k1gInSc1
Mohyla	mohyla	k1gFnSc1
míru	míra	k1gFnSc4
na	na	k7c6
Prackém	Pracký	k2eAgInSc6d1
kopci	kopec	k1gInSc6
zhruba	zhruba	k6eAd1
uprostřed	uprostřed	k7c2
někdejšího	někdejší	k2eAgNnSc2d1
bojiště	bojiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
Atlantiku	Atlantik	k1gInSc2
k	k	k7c3
Brnu	Brno	k1gNnSc3
</s>
<s>
Marie-Victoire	Marie-Victoir	k1gMnSc5
Jaquotot	Jaquotot	k1gMnSc1
<g/>
:	:	kIx,
Císař	Císař	k1gMnSc1
Napoleon	Napoleon	k1gMnSc1
I.	I.	kA
Bonaparte	bonapart	k1gInSc5
</s>
<s>
Válečná	válečný	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
roku	rok	k1gInSc2
1805	#num#	k4
</s>
<s>
Pro	pro	k7c4
rok	rok	k1gInSc4
1805	#num#	k4
naplánoval	naplánovat	k5eAaBmAgMnS
první	první	k4xOgMnSc1
konzul	konzul	k1gMnSc1
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
válečnou	válečný	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
měli	mít	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc1
vojáci	voják	k1gMnPc1
dobýt	dobýt	k5eAaPmF
Velkou	velký	k2eAgFnSc4d1
Británii	Británie	k1gFnSc4
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
byla	být	k5eAaImAgFnS
Francie	Francie	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1803	#num#	k4
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
si	se	k3xPyFc3
naprosto	naprosto	k6eAd1
jist	jist	k2eAgMnSc1d1
svým	svůj	k3xOyFgNnSc7
vítězstvím	vítězství	k1gNnSc7
a	a	k8xC
začal	začít	k5eAaPmAgMnS
proto	proto	k8xC
i	i	k9
ve	v	k7c6
zbytku	zbytek	k1gInSc6
Evropy	Evropa	k1gFnSc2
uplatňovat	uplatňovat	k5eAaImF
agresivní	agresivní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1803	#num#	k4
anektoval	anektovat	k5eAaBmAgInS
Hannoversko	Hannoversko	k1gNnSc4
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1804	#num#	k4
nechal	nechat	k5eAaPmAgMnS
z	z	k7c2
území	území	k1gNnSc2
Bádenska	Bádensko	k1gNnSc2
unést	unést	k5eAaPmF
a	a	k8xC
po	po	k7c6
zinscenovaném	zinscenovaný	k2eAgInSc6d1
procesu	proces	k1gInSc6
popravit	popravit	k5eAaPmF
blízkého	blízký	k2eAgMnSc4d1
příbuzného	příbuzný	k1gMnSc4
bývalého	bývalý	k2eAgMnSc4d1
francouzského	francouzský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Ludvíka	Ludvík	k1gMnSc4
XVI	XVI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vévodu	vévoda	k1gMnSc4
Antoina	Antoin	k1gMnSc4
d	d	k?
<g/>
'	'	kIx"
<g/>
Enghien	Enghien	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgInSc3
skutku	skutek	k1gInSc3
jej	on	k3xPp3gMnSc4
vedly	vést	k5eAaImAgInP
zejména	zejména	k9
indicie	indicie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
naznačovaly	naznačovat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
mladý	mladý	k2eAgMnSc1d1
příslušník	příslušník	k1gMnSc1
rodu	rod	k1gInSc2
Bourbonů	bourbon	k1gInPc2
je	být	k5eAaImIp3nS
zapleten	zaplést	k5eAaPmNgInS
do	do	k7c2
spiknutí	spiknutí	k1gNnSc2
připravovaného	připravovaný	k2eAgInSc2d1
britskými	britský	k2eAgMnPc7d1
agenty	agent	k1gMnPc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
byl	být	k5eAaImAgInS
státní	státní	k2eAgInSc1d1
převrat	převrat	k1gInSc1
a	a	k8xC
Napoleonova	Napoleonův	k2eAgFnSc1d1
násilná	násilný	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
pak	pak	k6eAd1
připojil	připojit	k5eAaPmAgInS
<g />
.	.	kIx.
</s>
<s hack="1">
k	k	k7c3
Francii	Francie	k1gFnSc3
Ligurskou	ligurský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1804	#num#	k4
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgInS
korunovat	korunovat	k5eAaBmF
francouzským	francouzský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1805	#num#	k4
přijal	přijmout	k5eAaPmAgInS
italskou	italský	k2eAgFnSc4d1
královskou	královský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Bonapartovo	Bonapartův	k2eAgNnSc1d1
počínání	počínání	k1gNnSc1
však	však	k9
již	již	k9
dlouhodobě	dlouhodobě	k6eAd1
vzbuzovalo	vzbuzovat	k5eAaImAgNnS
hněv	hněv	k1gInSc4
a	a	k8xC
nelibost	nelibost	k1gFnSc4
u	u	k7c2
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1803	#num#	k4
prozatím	prozatím	k6eAd1
neúspěšně	úspěšně	k6eNd1
pokoušel	pokoušet	k5eAaImAgMnS
uzavřít	uzavřít	k5eAaPmF
protifrancouzskou	protifrancouzský	k2eAgFnSc4d1
útočnou	útočný	k2eAgFnSc4d1
alianci	aliance	k1gFnSc4
s	s	k7c7
vládci	vládce	k1gMnPc7
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
nejvýraznějšímu	výrazný	k2eAgNnSc3d3
ochlazení	ochlazení	k1gNnSc3
francouzsko-ruských	francouzsko-ruský	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
došlo	dojít	k5eAaPmAgNnS
po	po	k7c6
popravě	poprava	k1gFnSc6
vévody	vévoda	k1gMnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Enghien	Enghien	k1gInSc1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1804	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
nicméně	nicméně	k8xC
Napoleon	napoleon	k1gInSc1
nechápal	chápat	k5eNaImAgInS
důvod	důvod	k1gInSc4
carova	carův	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
a	a	k8xC
společně	společně	k6eAd1
se	s	k7c7
svými	svůj	k3xOyFgInPc7
diplomaty	diplomat	k1gInPc7
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
jen	jen	k9
o	o	k7c4
dočasnou	dočasný	k2eAgFnSc4d1
záležitost	záležitost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Ruský	ruský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
však	však	k9
zatvrzele	zatvrzele	k6eAd1
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
některou	některý	k3yIgFnSc7
z	z	k7c2
evropských	evropský	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnPc3
pověřencům	pověřenec	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
dosáhnout	dosáhnout	k5eAaPmF
úspěchu	úspěch	k1gInSc3
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
uzavřel	uzavřít	k5eAaPmAgInS
dohodu	dohoda	k1gFnSc4
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1805	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
alianci	aliance	k1gFnSc3
se	se	k3xPyFc4
po	po	k7c6
diplomatickém	diplomatický	k2eAgInSc6d1
nátlaku	nátlak	k1gInSc6
následně	následně	k6eAd1
připojil	připojit	k5eAaPmAgInS
i	i	k9
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
proti	proti	k7c3
Francii	Francie	k1gFnSc3
popudila	popudit	k5eAaPmAgFnS
zejména	zejména	k9
Napoleonova	Napoleonův	k2eAgFnSc1d1
anexe	anexe	k1gFnSc1
Janovska	Janovsko	k1gNnSc2
a	a	k8xC
Luccy	Lucca	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
podepsání	podepsání	k1gNnSc3
britsko-rusko-rakouské	britsko-rusko-rakouský	k2eAgFnSc2d1
spojenecké	spojenecký	k2eAgFnSc2d1
deklarace	deklarace	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
vznikla	vzniknout	k5eAaPmAgFnS
III	III	kA
<g/>
.	.	kIx.
koalice	koalice	k1gFnSc1
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kapitulace	kapitulace	k1gFnSc1
rakouských	rakouský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Ulmu	Ulmus	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1805	#num#	k4
</s>
<s>
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
Napoleon	Napoleon	k1gMnSc1
vyhnul	vyhnout	k5eAaPmAgMnS
obranné	obranný	k2eAgInPc4d1
válce	válec	k1gInPc4
na	na	k7c6
několika	několik	k4yIc6
frontách	fronta	k1gFnPc6
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
nejbližšího	blízký	k2eAgMnSc4d3
nepřítele	nepřítel	k1gMnSc4
–	–	k?
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
posléze	posléze	k6eAd1
se	se	k3xPyFc4
vypořádat	vypořádat	k5eAaPmF
s	s	k7c7
Rusy	Rus	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Tažení	tažení	k1gNnPc2
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
na	na	k7c4
200	#num#	k4
000	#num#	k4
francouzských	francouzský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
bývalé	bývalý	k2eAgFnSc2d1
„	„	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Anglické	anglický	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
kampaň	kampaň	k1gFnSc1
nesla	nést	k5eAaImAgFnS
nové	nový	k2eAgNnSc4d1
Napoleonovo	Napoleonův	k2eAgNnSc4d1
přízvisko	přízvisko	k1gNnSc4
Grande	grand	k1gMnSc5
Armée	Arméus	k1gMnSc5
ďAllemagne	ďAllemagn	k1gMnSc5
<g/>
,	,	kIx,
„	„	k?
<g/>
Velká	velký	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
všeobecně	všeobecně	k6eAd1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
bude	být	k5eAaImBp3nS
říkat	říkat	k5eAaImF
jen	jen	k9
Grande	grand	k1gMnSc5
Armée	Arméus	k1gMnSc5
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Francouzští	francouzský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
tedy	tedy	k9
místo	místo	k1gNnSc4
připravovaného	připravovaný	k2eAgNnSc2d1
vylodění	vylodění	k1gNnSc2
do	do	k7c2
Británie	Británie	k1gFnSc2
vyrazili	vyrazit	k5eAaPmAgMnP
v	v	k7c6
sedmi	sedm	k4xCc6
proudech	proud	k1gInPc6
k	k	k7c3
rakouským	rakouský	k2eAgFnPc3d1
hranicím	hranice	k1gFnPc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
své	svůj	k3xOyFgFnPc4
síly	síla	k1gFnPc4
soustřeďoval	soustřeďovat	k5eAaImAgMnS
podmaršálek	podmaršálek	k1gMnSc1
Mack	Mack	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzské	francouzský	k2eAgInPc1d1
sbory	sbor	k1gInPc1
se	se	k3xPyFc4
přesunovaly	přesunovat	k5eAaImAgInP
rychlostí	rychlost	k1gFnSc7
25	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
km	km	kA
za	za	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
rakouský	rakouský	k2eAgMnSc1d1
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
tušil	tušit	k5eAaImAgMnS
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
dorazily	dorazit	k5eAaPmAgInP
sbory	sbor	k1gInPc1
francouzských	francouzský	k2eAgMnPc2d1
maršálů	maršál	k1gMnPc2
Soulta	Soulto	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Davouta	Davout	k1gMnSc2
a	a	k8xC
Lannese	Lannese	k1gFnPc4
a	a	k8xC
Muratovo	Muratův	k2eAgNnSc4d1
jezdectvo	jezdectvo	k1gNnSc4
k	k	k7c3
Dunaji	Dunaj	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
překročily	překročit	k5eAaPmAgFnP
60	#num#	k4
kilometrů	kilometr	k1gInPc2
v	v	k7c6
Mackově	Mackův	k2eAgInSc6d1
týlu	týl	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
podmaršálek	podmaršálek	k1gMnSc1
<g/>
,	,	kIx,
i	i	k9
s	s	k7c7
většinou	většina	k1gFnSc7
rakouské	rakouský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
obklíčen	obklíčit	k5eAaPmNgMnS
u	u	k7c2
Ulmu	Ulmus	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
před	před	k7c7
svým	svůj	k3xOyFgMnSc7
protivníkem	protivník	k1gMnSc7
kapituloval	kapitulovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
obklíčení	obklíčení	k1gNnSc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
muže	muž	k1gMnSc2
nevyprostila	vyprostit	k5eNaPmAgFnS
ani	ani	k8xC
Podolská	podolský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
ruského	ruský	k2eAgMnSc2d1
generála	generál	k1gMnSc2
Kutuzova	Kutuzův	k2eAgMnSc2d1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
Rakušanům	Rakušan	k1gMnPc3
vyslal	vyslat	k5eAaPmAgMnS
na	na	k7c4
pomoc	pomoc	k1gFnSc4
jejich	jejich	k3xOp3gMnSc1
spojenec	spojenec	k1gMnSc1
car	car	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
V	v	k7c6
průběhu	průběh	k1gInSc6
pokračujícího	pokračující	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
padla	padnout	k5eAaImAgNnP,k5eAaPmAgNnP
do	do	k7c2
francouzských	francouzský	k2eAgFnPc2d1
rukou	ruka	k1gFnPc2
i	i	k8xC
Vídeň	Vídeň	k1gFnSc1
a	a	k8xC
Napoleon	napoleon	k1gInSc1
se	se	k3xPyFc4
ve	v	k7c6
stopách	stopa	k1gFnPc6
ustupujících	ustupující	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
vydal	vydat	k5eAaPmAgInS
na	na	k7c4
Moravu	Morava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgInPc1d1
i	i	k8xC
zbytky	zbytek	k1gInPc1
rakouské	rakouský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
ustoupili	ustoupit	k5eAaPmAgMnP
k	k	k7c3
Olomouci	Olomouc	k1gFnSc3
<g/>
,	,	kIx,
Francouzi	Francouz	k1gMnPc1
tedy	tedy	k9
bez	bez	k7c2
boje	boj	k1gInSc2
obsadili	obsadit	k5eAaPmAgMnP
Brno	Brno	k1gNnSc4
a	a	k8xC
část	část	k1gFnSc4
jejich	jejich	k3xOp3gNnSc2
jezdectva	jezdectvo	k1gNnSc2
pronikla	proniknout	k5eAaPmAgFnS
až	až	k9
k	k	k7c3
Vyškovu	Vyškov	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Napoleon	Napoleon	k1gMnSc1
nehodlal	hodlat	k5eNaImAgMnS
své	svůj	k3xOyFgFnSc3
armádě	armáda	k1gFnSc3
dál	daleko	k6eAd2
neúnosně	únosně	k6eNd1
prodlužovat	prodlužovat	k5eAaImF
operační	operační	k2eAgFnSc2d1
linie	linie	k1gFnSc2
a	a	k8xC
toužil	toužit	k5eAaImAgInS
svést	svést	k5eAaPmF
rozhodující	rozhodující	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Navíc	navíc	k6eAd1
na	na	k7c4
Moravu	Morava	k1gFnSc4
směřovaly	směřovat	k5eAaImAgFnP
další	další	k2eAgFnPc1d1
ruské	ruský	k2eAgFnPc1d1
zálohy	záloha	k1gFnPc1
a	a	k8xC
k	k	k7c3
francouzskému	francouzský	k2eAgMnSc3d1
císaři	císař	k1gMnSc3
mířil	mířit	k5eAaImAgMnS
vyslanec	vyslanec	k1gMnSc1
Haugwitz	Haugwitz	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
mu	on	k3xPp3gMnSc3
přivážel	přivážet	k5eAaImAgMnS
pruské	pruský	k2eAgNnSc4d1
ultimátum	ultimátum	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
soudilo	soudit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
ultimátum	ultimátum	k1gNnSc1
nebude	být	k5eNaImBp3nS
přijato	přijmout	k5eAaPmNgNnS
a	a	k8xC
Grande	grand	k1gMnSc5
Armée	Armé	k1gMnPc4
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
sevřena	sevřít	k5eAaPmNgFnS
do	do	k7c2
kleští	kleště	k1gFnPc2
spojenců	spojenec	k1gMnPc2
a	a	k8xC
rozdrcena	rozdrcen	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Napoleonovým	Napoleonův	k2eAgInSc7d1
prioritním	prioritní	k2eAgInSc7d1
zájmem	zájem	k1gInSc7
proto	proto	k8xC
bylo	být	k5eAaImAgNnS
spojence	spojenec	k1gMnPc4
k	k	k7c3
vzájemné	vzájemný	k2eAgFnSc3d1
konfrontaci	konfrontace	k1gFnSc3
přilákat	přilákat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
snaha	snaha	k1gFnSc1
však	však	k9
nemusela	muset	k5eNaImAgFnS
být	být	k5eAaImF
nijak	nijak	k6eAd1
přehnaná	přehnaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
svést	svést	k5eAaPmF
generální	generální	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
bylo	být	k5eAaImAgNnS
i	i	k9
úmyslem	úmysl	k1gInSc7
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
protivníka	protivník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morálka	morálka	k1gFnSc1
Rusů	Rus	k1gMnPc2
byla	být	k5eAaImAgFnS
koncem	koncem	k7c2
listopadu	listopad	k1gInSc2
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
k	k	k7c3
vojsku	vojsko	k1gNnSc3
se	se	k3xPyFc4
připojila	připojit	k5eAaPmAgFnS
Volyňská	volyňský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
generála	generál	k1gMnSc2
Buxhöwdena	Buxhöwden	k1gMnSc2
a	a	k8xC
dorazil	dorazit	k5eAaPmAgMnS
i	i	k9
car	car	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Novým	nový	k2eAgInSc7d1
plánem	plán	k1gInSc7
spojenců	spojenec	k1gMnPc2
bylo	být	k5eAaImAgNnS
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Napoleonovo	Napoleonův	k2eAgNnSc4d1
pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
a	a	k8xC
odříznout	odříznout	k5eAaPmF
mu	on	k3xPp3gMnSc3
spojnici	spojnice	k1gFnSc4
Brno	Brno	k1gNnSc1
–	–	k?
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
24	#num#	k4
<g/>
]	]	kIx)
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
proto	proto	k8xC
Bagrationův	Bagrationův	k2eAgInSc1d1
předvoj	předvoj	k1gInSc1
<g/>
,	,	kIx,
posílený	posílený	k2eAgInSc1d1
58	#num#	k4
eskadronami	eskadrona	k1gFnPc7
kavalérie	kavalérie	k1gFnPc4
<g/>
,	,	kIx,
vyhnal	vyhnat	k5eAaPmAgMnS
z	z	k7c2
Vyškova	Vyškov	k1gInSc2
předsunuté	předsunutý	k2eAgNnSc4d1
francouzské	francouzský	k2eAgNnSc4d1
jezdectvo	jezdectvo	k1gNnSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
generálů	generál	k1gMnPc2
Milhauda	Milhaudo	k1gNnSc2
a	a	k8xC
Treilliarda	Treilliard	k1gMnSc4
a	a	k8xC
následně	následně	k6eAd1
vyrazila	vyrazit	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
spojenecká	spojenecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
po	po	k7c6
olomoucké	olomoucký	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
k	k	k7c3
Brnu	Brno	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
rusko-rakouské	rusko-rakouský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
dorazilo	dorazit	k5eAaPmAgNnS
k	k	k7c3
dějišti	dějiště	k1gNnSc3
budoucí	budoucí	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Armády	armáda	k1gFnPc1
</s>
<s>
Francouzi	Francouz	k1gMnPc1
</s>
<s>
Hippolyte	Hippolyt	k1gInSc5
Bellangé	Bellangé	k1gNnSc6
<g/>
:	:	kIx,
voják	voják	k1gMnSc1
a	a	k8xC
důstojník	důstojník	k1gMnSc1
granátníků	granátník	k1gMnPc2
Napoleonovy	Napoleonův	k2eAgFnSc2d1
císařské	císařský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1805	#num#	k4
dělila	dělit	k5eAaImAgFnS
na	na	k7c4
lehkou	lehký	k2eAgFnSc4d1
a	a	k8xC
řadovou	řadový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
byli	být	k5eAaImAgMnP
oblečeni	obléct	k5eAaPmNgMnP
do	do	k7c2
tmavomodrého	tmavomodrý	k2eAgInSc2d1
fraku	frak	k1gInSc2
<g/>
,	,	kIx,
vzadu	vzadu	k6eAd1
opatřeného	opatřený	k2eAgInSc2d1
šosy	šos	k1gInPc4
a	a	k8xC
vpředu	vpředu	k6eAd1
bílým	bílý	k2eAgNnSc7d1
revérem	revérem	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušnost	příslušnost	k1gFnSc1
k	k	k7c3
jednotkám	jednotka	k1gFnPc3
se	se	k3xPyFc4
určovala	určovat	k5eAaImAgFnS
podle	podle	k7c2
barvy	barva	k1gFnSc2
doplňků	doplněk	k1gInPc2
(	(	kIx(
<g/>
epolety	epoleta	k1gFnPc1
<g/>
,	,	kIx,
manžety	manžeta	k1gFnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickým	charakteristický	k2eAgInSc7d1
znakem	znak	k1gInSc7
pěšáka	pěšák	k1gMnSc2
byl	být	k5eAaImAgInS
černý	černý	k2eAgInSc1d1
dvourohý	dvourohý	k2eAgInSc1d1
klobouk	klobouk	k1gInSc1
s	s	k7c7
kokardou	kokarda	k1gFnSc7
a	a	k8xC
pomponem	pompon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Infanteristé	infanterista	k1gMnPc1
byli	být	k5eAaImAgMnP
vyzbrojeni	vyzbrojen	k2eAgMnPc1d1
převážně	převážně	k6eAd1
puškou	puška	k1gFnSc7
s	s	k7c7
hladkou	hladký	k2eAgFnSc7d1
hlavní	hlavní	k2eAgFnSc7d1
a	a	k8xC
křesadlovým	křesadlový	k2eAgInSc7d1
zámkem	zámek	k1gInSc7
fusil	fusit	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
ďinfanterie	ďinfanterie	k1gFnSc2
vzor	vzor	k1gInSc1
An	An	k1gFnSc2
IX	IX	kA
a	a	k8xC
An	An	k1gMnSc1
IX	IX	kA
<g/>
–	–	k?
<g/>
An	An	k1gMnSc7
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
váha	váha	k1gFnSc1
byla	být	k5eAaImAgFnS
4,6	4,6	k4
kg	kg	kA
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
152,9	152,9	k4
cm	cm	kA
a	a	k8xC
ráže	ráže	k1gFnSc1
17,48	17,48	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Základní	základní	k2eAgInSc1d1
operační	operační	k2eAgInSc1d1
svazek	svazek	k1gInSc1
armády	armáda	k1gFnSc2
byl	být	k5eAaImAgInS
armádní	armádní	k2eAgInSc4d1
sbor	sbor	k1gInSc4
(	(	kIx(
<g/>
corps	corps	k1gInSc4
ďarmé	ďarmý	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
síla	síla	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
sborů	sbor	k1gInPc2
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
lišila	lišit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbory	sbor	k1gInPc1
se	se	k3xPyFc4
skládaly	skládat	k5eAaImAgInP
z	z	k7c2
dvou	dva	k4xCgNnPc2
nebo	nebo	k8xC
více	hodně	k6eAd2
divizí	divize	k1gFnSc7
pěchoty	pěchota	k1gFnSc2
<g/>
,	,	kIx,
lehké	lehký	k2eAgFnSc2d1
divize	divize	k1gFnSc2
jezdectva	jezdectvo	k1gNnSc2
a	a	k8xC
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Divize	divize	k1gFnSc1
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
dále	daleko	k6eAd2
dělila	dělit	k5eAaImAgFnS
na	na	k7c4
tři	tři	k4xCgFnPc4
brigády	brigáda	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
jedna	jeden	k4xCgFnSc1
byla	být	k5eAaImAgFnS
lehká	lehký	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěší	pěší	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
formovala	formovat	k5eAaImAgFnS
ze	z	k7c2
dvou	dva	k4xCgInPc2
až	až	k9
tří	tři	k4xCgInPc2
pluků	pluk	k1gInPc2
(	(	kIx(
<g/>
podle	podle	k7c2
okolností	okolnost	k1gFnPc2
1200	#num#	k4
<g/>
–	–	k?
<g/>
3300	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc4
základní	základní	k2eAgFnSc4d1
organizační	organizační	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
tvořil	tvořit	k5eAaImAgInS
prapor	prapor	k1gInSc1
(	(	kIx(
<g/>
batalion	batalion	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Ty	ten	k3xDgFnPc1
se	se	k3xPyFc4
členily	členit	k5eAaImAgFnP
na	na	k7c4
kompanie	kompanie	k1gFnPc4
a	a	k8xC
takticky	takticky	k6eAd1
na	na	k7c4
pelotony	peloton	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
pelotony	peloton	k1gInPc1
dávaly	dávat	k5eAaImAgInP
jeden	jeden	k4xCgInSc4
divizion	divizion	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Francouzské	francouzský	k2eAgNnSc1d1
jezdectvo	jezdectvo	k1gNnSc1
se	se	k3xPyFc4
podle	podle	k7c2
způsobu	způsob	k1gInSc2
použití	použití	k1gNnSc2
dělilo	dělit	k5eAaImAgNnS
na	na	k7c4
těžké	těžký	k2eAgInPc4d1
(	(	kIx(
<g/>
kyrysníci	kyrysník	k1gMnPc1
<g/>
,	,	kIx,
karabiníci	karabiník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
řadové	řadový	k2eAgNnSc4d1
(	(	kIx(
<g/>
dragouni	dragoun	k1gMnPc5
<g/>
)	)	kIx)
a	a	k8xC
lehké	lehký	k2eAgNnSc1d1
(	(	kIx(
<g/>
jízdní	jízdní	k2eAgMnPc1d1
myslivci	myslivec	k1gMnPc1
<g/>
,	,	kIx,
husaři	husar	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
Napoleonovy	Napoleonův	k2eAgFnSc2d1
reorganizace	reorganizace	k1gFnSc2
disponovala	disponovat	k5eAaBmAgFnS
Grande	grand	k1gMnSc5
Armée	Armée	k1gFnSc7
tzv.	tzv.	kA
jezdeckou	jezdecký	k2eAgFnSc7d1
zálohou	záloha	k1gFnSc7
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gNnSc7
velením	velení	k1gNnSc7
byl	být	k5eAaImAgInS
pověřen	pověřit	k5eAaPmNgMnS
maršál	maršál	k1gMnSc1
Joachim	Joachim	k1gMnSc1
Murat	Murat	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnovala	zahrnovat	k5eAaImAgFnS
dvě	dva	k4xCgFnPc4
divize	divize	k1gFnPc4
těžkého	těžký	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
<g/>
,	,	kIx,
pět	pět	k4xCc1
divizí	divize	k1gFnPc2
dragounů	dragoun	k1gMnPc2
(	(	kIx(
<g/>
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
pro	pro	k7c4
nedostatek	nedostatek	k1gInSc4
koní	koní	k2eAgMnPc1d1
pěší	pěší	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
podpůrné	podpůrný	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
jízdní	jízdní	k2eAgFnSc7d1
zálohou	záloha	k1gFnSc7
Napoleon	Napoleon	k1gMnSc1
zřídil	zřídit	k5eAaPmAgInS
i	i	k9
zálohu	záloha	k1gFnSc4
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
zbytek	zbytek	k1gInSc4
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
disponovala	disponovat	k5eAaBmAgFnS
šesti-	šesti-	k?
a	a	k8xC
osmiliberními	osmiliberními	k?
děly	dělo	k1gNnPc7
systému	systém	k1gInSc2
Gribeauval	Gribeauval	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěší	pěší	k2eAgMnPc1d1
dělostřelci	dělostřelec	k1gMnPc1
nosili	nosit	k5eAaImAgMnP
modré	modrý	k2eAgFnPc4d1
uniformy	uniforma	k1gFnPc4
s	s	k7c7
modrým	modrý	k2eAgInSc7d1
<g/>
,	,	kIx,
červeně	červeně	k6eAd1
olemovaným	olemovaný	k2eAgInSc7d1
revérem	revérem	k?
a	a	k8xC
šosy	šos	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
hlavě	hlava	k1gFnSc6
pak	pak	k6eAd1
černé	černý	k2eAgNnSc1d1
čáko	čáko	k1gNnSc1
s	s	k7c7
pomponem	pompon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
Slavkovu	Slavkov	k1gInSc3
přivedl	přivést	k5eAaPmAgMnS
Napoleon	Napoleon	k1gMnSc1
i	i	k9
císařskou	císařský	k2eAgFnSc4d1
gardu	garda	k1gFnSc4
pod	pod	k7c7
velením	velení	k1gNnSc7
maršála	maršál	k1gMnSc2
Bessiè	Bessiè	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
zastupovalo	zastupovat	k5eAaImAgNnS
3885	#num#	k4
pěšáků	pěšák	k1gMnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
1130	#num#	k4
jezdců	jezdec	k1gMnPc2
a	a	k8xC
660	#num#	k4
dělostřelců	dělostřelec	k1gMnPc2
s	s	k7c7
24	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Celkově	celkově	k6eAd1
stálo	stát	k5eAaImAgNnS
v	v	k7c6
poli	pole	k1gNnSc6
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
zhruba	zhruba	k6eAd1
605	#num#	k4
vojáků	voják	k1gMnPc2
francouzského	francouzský	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
,	,	kIx,
58	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
<g/>
,	,	kIx,
11	#num#	k4
500	#num#	k4
jezdců	jezdec	k1gMnPc2
a	a	k8xC
4200	#num#	k4
dělostřelců	dělostřelec	k1gMnPc2
a	a	k8xC
příslušníků	příslušník	k1gMnPc2
trénu	trén	k1gInSc2
se	s	k7c7
157	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rusové	Rus	k1gMnPc1
</s>
<s>
Pobočník	pobočník	k1gMnSc1
a	a	k8xC
štábní	štábní	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
oděná	oděný	k2eAgFnSc1d1
v	v	k7c6
tmavozeleném	tmavozelený	k2eAgNnSc6d1
saku	sako	k1gNnSc6
se	se	k3xPyFc4
dělila	dělit	k5eAaImAgFnS
na	na	k7c4
těžkou	těžký	k2eAgFnSc4d1
(	(	kIx(
<g/>
granátníci	granátník	k1gMnPc1
<g/>
,	,	kIx,
mušketýři	mušketýr	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
lehkou	lehký	k2eAgFnSc4d1
(	(	kIx(
<g/>
myslivci	myslivec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejefektivnějších	efektivní	k2eAgFnPc2d3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Mužstvo	mužstvo	k1gNnSc4
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
tvořili	tvořit	k5eAaImAgMnP
odvedenci	odvedenec	k1gMnPc1
převážně	převážně	k6eAd1
z	z	k7c2
řad	řada	k1gFnPc2
nevolníků	nevolník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
nevzdělaní	vzdělaný	k2eNgMnPc1d1
<g/>
,	,	kIx,
hluboce	hluboko	k6eAd1
zbožní	zbožní	k2eAgMnPc1d1
<g/>
,	,	kIx,
pověrčiví	pověrčivý	k2eAgMnPc1d1
<g/>
,	,	kIx,
oddaní	oddaný	k2eAgMnPc1d1
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
nesmírně	smírně	k6eNd1
stateční	statečný	k2eAgMnPc1d1
<g/>
,	,	kIx,
obětaví	obětavý	k2eAgMnPc1d1
a	a	k8xC
skromní	skromný	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
jejich	jejich	k3xOp3gInSc6
výcviku	výcvik	k1gInSc6
bylo	být	k5eAaImAgNnS
využíváno	využívat	k5eAaPmNgNnS,k5eAaImNgNnS
již	již	k9
zastaralé	zastaralý	k2eAgFnSc2d1
pruské	pruský	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
doktríny	doktrína	k1gFnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
vojáci	voják	k1gMnPc1
postrádali	postrádat	k5eAaImAgMnP
zkušenosti	zkušenost	k1gFnPc4
z	z	k7c2
evropských	evropský	k2eAgNnPc2d1
bojišť	bojiště	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
armádě	armáda	k1gFnSc6
neexistovaly	existovat	k5eNaImAgInP
sbory	sbor	k1gInPc1
<g/>
,	,	kIx,
jen	jen	k9
armádní	armádní	k2eAgInPc1d1
celky	celek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
organizovaly	organizovat	k5eAaBmAgInP
do	do	k7c2
kolon	kolona	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
skladba	skladba	k1gFnSc1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
zbraní	zbraň	k1gFnPc2
měnila	měnit	k5eAaImAgFnS
takřka	takřka	k6eAd1
ze	z	k7c2
dne	den	k1gInSc2
na	na	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
jednotek	jednotka	k1gFnPc2
organizačně	organizačně	k6eAd1
existovaly	existovat	k5eAaImAgInP
pouze	pouze	k6eAd1
pluk	pluk	k1gInSc4
<g/>
,	,	kIx,
prapor	prapor	k1gInSc1
a	a	k8xC
rota	rota	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
koordinace	koordinace	k1gFnSc1
mezi	mezi	k7c7
pluky	pluk	k1gInPc7
nebyla	být	k5eNaImAgFnS
do	do	k7c2
výcviku	výcvik	k1gInSc2
zahrnuta	zahrnut	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojsko	vojsko	k1gNnSc1
Alexandra	Alexandr	k1gMnSc2
I.	I.	kA
postrádalo	postrádat	k5eAaImAgNnS
generální	generální	k2eAgInSc4d1
štáb	štáb	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
poli	pole	k1gNnSc6
existoval	existovat	k5eAaImAgInS
pouze	pouze	k6eAd1
orgán	orgán	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
Svita	svit	k2eAgFnSc1d1
Jego	Jego	k6eAd1
Imperatorskogo	Imperatorskogo	k6eAd1
Veličestva	Veličestvo	k1gNnSc2
po	po	k7c4
kvartirmejsterskoj	kvartirmejsterskoj	k1gInSc4
časti	častit	k5eAaImRp2nS
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
úkoly	úkol	k1gInPc1
spočívaly	spočívat	k5eAaImAgInP
jen	jen	k9
v	v	k7c6
logistice	logistika	k1gFnSc6
<g/>
;	;	kIx,
bojové	bojový	k2eAgFnSc2d1
akce	akce	k1gFnSc2
plánovalo	plánovat	k5eAaImAgNnS
kolegium	kolegium	k1gNnSc1
sídlící	sídlící	k2eAgNnSc1d1
v	v	k7c6
Sankt-Petěrburgu	Sankt-Petěrburg	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1805	#num#	k4
nastupovala	nastupovat	k5eAaImAgFnS
do	do	k7c2
boje	boj	k1gInSc2
převážně	převážně	k6eAd1
s	s	k7c7
nedrážkovanou	drážkovaný	k2eNgFnSc4d1
144	#num#	k4
cm	cm	kA
dlouhou	dlouhý	k2eAgFnSc7d1
mušketou	mušketa	k1gFnSc7
vzor	vzor	k1gInSc1
1798	#num#	k4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
hmotnost	hmotnost	k1gFnSc1
byla	být	k5eAaImAgFnS
5	#num#	k4
kg	kg	kA
a	a	k8xC
ráže	ráže	k1gFnSc1
19,8	19,8	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důstojníci	důstojník	k1gMnPc1
rot	rota	k1gFnPc2
a	a	k8xC
dvacítka	dvacítka	k1gFnSc1
nejlepších	dobrý	k2eAgMnPc2d3
střelců	střelec	k1gMnPc2
mysliveckých	myslivecký	k2eAgInPc2d1
pluků	pluk	k1gInPc2
vlastnili	vlastnit	k5eAaImAgMnP
myslivecký	myslivecký	k2eAgMnSc1d1
štucer	štucer	k1gMnSc1
vzor	vzor	k1gInSc4
1798	#num#	k4
s	s	k7c7
drážkovanou	drážkovaný	k2eAgFnSc7d1
hlavní	hlavní	k2eAgFnSc7d1
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
4,2	4,2	k4
kg	kg	kA
a	a	k8xC
ráži	ráže	k1gFnSc6
15,6	15,6	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Ruské	ruský	k2eAgNnSc1d1
jezdectvo	jezdectvo	k1gNnSc1
se	se	k3xPyFc4
dělilo	dělit	k5eAaImAgNnS
na	na	k7c4
gardové	gardový	k2eAgInPc4d1
(	(	kIx(
<g/>
Tělesný	tělesný	k2eAgMnSc1d1
<g/>
–	–	k?
<g/>
gardový	gardový	k2eAgInSc1d1
jezdecký	jezdecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
Kavalergardský	Kavalergardský	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
Tělesný	tělesný	k2eAgInSc1d1
<g/>
–	–	k?
<g/>
gardový	gardový	k2eAgInSc1d1
dragounský	dragounský	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
Tělesný	tělesný	k2eAgInSc1d1
<g/>
–	–	k?
<g/>
gardový	gardový	k2eAgInSc1d1
husarský	husarský	k2eAgInSc1d1
pluk	pluk	k1gInSc1
a	a	k8xC
Tělesný	tělesný	k2eAgInSc1d1
<g/>
–	–	k?
<g/>
gardový	gardový	k2eAgInSc1d1
kozácký	kozácký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
řadové	řadový	k2eAgInPc1d1
(	(	kIx(
<g/>
tj.	tj.	kA
pravidelné	pravidelný	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
zastoupeno	zastoupen	k2eAgNnSc1d1
kyrysníky	kyrysník	k1gMnPc4
(	(	kIx(
<g/>
bez	bez	k7c2
kyrysů	kyrys	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dragouny	dragoun	k1gMnPc7
<g/>
,	,	kIx,
hulány	hulán	k1gMnPc7
a	a	k8xC
husary	husar	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nepravidelných	pravidelný	k2eNgFnPc2d1
jednotek	jednotka	k1gFnPc2
se	se	k3xPyFc4
na	na	k7c6
bojišti	bojiště	k1gNnSc6
nacházelo	nacházet	k5eAaImAgNnS
několik	několik	k4yIc1
kozáckých	kozácký	k2eAgInPc2d1
regimentů	regiment	k1gInPc2
<g/>
,	,	kIx,
rozdělených	rozdělený	k2eAgInPc2d1
do	do	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
kolon	kolona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělostřelectvo	dělostřelectvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
vyzbrojováno	vyzbrojovat	k5eAaImNgNnS
kanóny	kanón	k1gInPc7
s	s	k7c7
hladkou	hladký	k2eAgFnSc7d1
hlavní	hlavní	k2eAgFnSc7d1
pro	pro	k7c4
střelbu	střelba	k1gFnSc4
koulemi	koule	k1gFnPc7
<g/>
,	,	kIx,
kartáči	kartáč	k1gInPc7
a	a	k8xC
zápalnými	zápalný	k2eAgFnPc7d1
střelami	střela	k1gFnPc7
a	a	k8xC
jednorožci	jednorožec	k1gMnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
určeny	určit	k5eAaPmNgFnP
zejména	zejména	k9
pro	pro	k7c4
střelbu	střelba	k1gFnSc4
granáty	granát	k1gInPc4
a	a	k8xC
bombami	bomba	k1gFnPc7
<g/>
,	,	kIx,
kartáči	kartáč	k1gInSc3
(	(	kIx(
<g/>
jen	jen	k6eAd1
přímou	přímý	k2eAgFnSc7d1
střelbou	střelba	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c4
omezenou	omezený	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
i	i	k8xC
plnými	plný	k2eAgFnPc7d1
koulemi	koule	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
Slavkova	Slavkov	k1gInSc2
tvořilo	tvořit	k5eAaImAgNnS
ruskou	ruský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
přibližně	přibližně	k6eAd1
50	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
<g/>
,	,	kIx,
14	#num#	k4
000	#num#	k4
jezdců	jezdec	k1gMnPc2
a	a	k8xC
7800	#num#	k4
dělostřelců	dělostřelec	k1gMnPc2
a	a	k8xC
příslušníků	příslušník	k1gMnPc2
trénu	trén	k1gInSc2
s	s	k7c7
318	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rakušané	Rakušan	k1gMnPc1
</s>
<s>
Příslušník	příslušník	k1gMnSc1
jednotky	jednotka	k1gFnSc2
rakouských	rakouský	k2eAgMnPc2d1
myslivců	myslivec	k1gMnPc2
v	v	k7c6
uniformě	uniforma	k1gFnSc6
z	z	k7c2
let	léto	k1gNnPc2
1798	#num#	k4
<g/>
–	–	k?
<g/>
1805	#num#	k4
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
infanterie	infanterie	k1gFnSc1
se	se	k3xPyFc4
dělila	dělit	k5eAaImAgFnS
na	na	k7c4
řadovou	řadový	k2eAgFnSc4d1
pěchotu	pěchota	k1gFnSc4
(	(	kIx(
<g/>
německá	německý	k2eAgFnSc1d1
a	a	k8xC
uherská	uherský	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hraničáře	hraničář	k1gMnSc2
a	a	k8xC
myslivce	myslivec	k1gMnSc2
(	(	kIx(
<g/>
Tyroler	Tyroler	k1gMnSc1
<g/>
–	–	k?
<g/>
Jäger	Jäger	k1gMnSc1
<g/>
–	–	k?
<g/>
Regiment	regiment	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušníci	příslušník	k1gMnPc1
řadové	řadový	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
v	v	k7c6
bílých	bílý	k2eAgInPc6d1
jednořadových	jednořadový	k2eAgInPc6d1
kabátcích	kabátec	k1gInPc6
s	s	k7c7
rovným	rovný	k2eAgNnSc7d1
zapínáním	zapínání	k1gNnSc7
byli	být	k5eAaImAgMnP
vyzbrojeni	vyzbrojen	k2eAgMnPc1d1
150,1	150,1	k4
cm	cm	kA
dlouhou	dlouhý	k2eAgFnSc4d1
a	a	k8xC
4,8	4,8	k4
kg	kg	kA
těžkou	těžký	k2eAgFnSc7d1
mušketou	mušketa	k1gFnSc7
Infanterie-Gewehr	Infanterie-Gewehr	k1gMnSc1
M	M	kA
1798	#num#	k4
mit	mit	k?
Stichbajonett	Stichbajonetta	k1gFnPc2
s	s	k7c7
kalibrem	kalibr	k1gInSc7
hlavně	hlavně	k9
17,58	17,58	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hraničáři	hraničář	k1gMnPc7
<g/>
,	,	kIx,
verbovaní	verbovaný	k2eAgMnPc1d1
v	v	k7c6
balkánských	balkánský	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
odění	odění	k1gNnSc1
do	do	k7c2
bílého	bílý	k2eAgInSc2d1
fraku	frak	k1gInSc2
<g/>
,	,	kIx,
bílých	bílý	k2eAgFnPc2d1
kalhot	kalhoty	k1gFnPc2
a	a	k8xC
kašketu	kašket	k1gInSc2
<g/>
,	,	kIx,
používali	používat	k5eAaImAgMnP
dvouhlavňovou	dvouhlavňový	k2eAgFnSc4d1
kozlici	kozlice	k1gFnSc4
Doppelstutzen	Doppelstutzna	k1gFnPc2
für	für	k?
Grenzenscharfschützen	Grenzenscharfschützna	k1gFnPc2
M	M	kA
1795	#num#	k4
se	s	k7c7
sedmidrážkovým	sedmidrážkový	k2eAgInSc7d1
vývrtem	vývrt	k1gInSc7
ráže	ráže	k1gFnSc1
14,8	14,8	k4
mm	mm	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
při	při	k7c6
střelbě	střelba	k1gFnSc6
stabilizovala	stabilizovat	k5eAaBmAgFnS
o	o	k7c4
2,53	2,53	k4
m	m	kA
dlouhou	dlouhý	k2eAgFnSc4d1
a	a	k8xC
2	#num#	k4
kg	kg	kA
těžkou	těžký	k2eAgFnSc4d1
píku	píka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotky	jednotka	k1gFnPc1
myslivců	myslivec	k1gMnPc2
byly	být	k5eAaImAgFnP
oděny	odět	k5eAaPmNgFnP
v	v	k7c6
uniformě	uniforma	k1gFnSc6
barvy	barva	k1gFnSc2
štičí	štičí	k2eAgFnSc2d1
šedi	šeď	k1gFnSc2
(	(	kIx(
<g/>
hechtgrau	hechtgrau	k6eAd1
<g/>
)	)	kIx)
s	s	k7c7
pěchotní	pěchotní	k2eAgFnSc7d1
přilbou	přilba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzbrojeny	vyzbrojen	k2eAgFnPc4d1
byly	být	k5eAaImAgFnP
dlouhým	dlouhý	k2eAgInSc7d1
sečným	sečný	k2eAgInSc7d1
bajonetem	bajonet	k1gInSc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
mysliveckým	myslivecký	k2eAgFnPc3d1
štucem	štucem	k?
se	s	k7c7
sedmidrážkovým	sedmidrážkový	k2eAgInSc7d1
vývrtem	vývrt	k1gInSc7
Jägerstutzen	Jägerstutzna	k1gFnPc2
M	M	kA
1795	#num#	k4
(	(	kIx(
<g/>
kalibr	kalibr	k1gInSc1
14,5	14,5	k4
mm	mm	kA
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
105,2	105,2	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Rakouské	rakouský	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
se	se	k3xPyFc4
dělilo	dělit	k5eAaImAgNnS
na	na	k7c4
řadové	řadový	k2eAgMnPc4d1
a	a	k8xC
záložní	záložní	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadové	řadový	k2eAgFnPc4d1
disponovalo	disponovat	k5eAaBmAgNnS
tří	tři	k4xCgInPc2
<g/>
–	–	k?
<g/>
,	,	kIx,
šesti	šest	k4xCc6
<g/>
–	–	k?
a	a	k8xC
dvanáctiliberními	dvanáctiliberními	k?
děly	dělo	k1gNnPc7
<g/>
,	,	kIx,
záložní	záložní	k2eAgInPc4d1
osmnáctiliberními	osmnáctiliberními	k?
kanóny	kanón	k1gInPc4
<g/>
,	,	kIx,
šestiliberními	šestiliberními	k?
jezdeckými	jezdecký	k2eAgInPc7d1
kanóny	kanón	k1gInPc7
<g/>
,	,	kIx,
sedmiliberními	sedmiliberními	k?
pěšími	pěší	k2eAgFnPc7d1
a	a	k8xC
jezdeckými	jezdecký	k2eAgFnPc7d1
houfnicemi	houfnice	k1gFnPc7
a	a	k8xC
desetiliberními	desetiliberními	k?
houfnicemi	houfnice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1805	#num#	k4
se	se	k3xPyFc4
řadové	řadový	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
přidělovalo	přidělovat	k5eAaImAgNnS
plukům	pluk	k1gInPc3
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
část	část	k1gFnSc1
děl	dělo	k1gNnPc2
připadla	připadnout	k5eAaPmAgFnS
jednotlivým	jednotlivý	k2eAgInPc3d1
praporům	prapor	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
jezdectva	jezdectvo	k1gNnSc2
se	se	k3xPyFc4
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
nacházely	nacházet	k5eAaImAgInP
tři	tři	k4xCgInPc1
rakouské	rakouský	k2eAgInPc1d1
kyrysnické	kyrysnický	k2eAgInPc1d1
regimenty	regiment	k1gInPc1
<g/>
,	,	kIx,
švališérský	švališérský	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
pluk	pluk	k1gInSc1
hraničářských	hraničářský	k2eAgMnPc2d1
husarů	husar	k1gMnPc2
a	a	k8xC
tři	tři	k4xCgInPc1
pluky	pluk	k1gInPc1
hulánů	hulán	k1gMnPc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
kvalitu	kvalita	k1gFnSc4
pluků	pluk	k1gInPc2
<g/>
,	,	kIx,
koní	kůň	k1gMnPc2
a	a	k8xC
péči	péče	k1gFnSc4
o	o	k7c4
ně	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
Rakušané	Rakušan	k1gMnPc1
považovaní	považovaný	k2eAgMnPc1d1
za	za	k7c4
nejlepší	dobrý	k2eAgMnPc4d3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
taktika	taktika	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
Francouzi	Francouz	k1gMnPc7
zastaralá	zastaralý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
rakouského	rakouský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
císař	císař	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
v	v	k7c6
bitvě	bitva	k1gFnSc6
disponoval	disponovat	k5eAaBmAgInS
zhruba	zhruba	k6eAd1
11	#num#	k4
000	#num#	k4
pěšáky	pěšák	k1gMnPc7
<g/>
,	,	kIx,
3000	#num#	k4
jezdci	jezdec	k1gInPc7
a	a	k8xC
1700	#num#	k4
dělostřelci	dělostřelec	k1gMnPc7
a	a	k8xC
příslušníky	příslušník	k1gMnPc7
trénu	trén	k1gInSc2
s	s	k7c7
70	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bojová	bojový	k2eAgFnSc1d1
sestava	sestava	k1gFnSc1
(	(	kIx(
<g/>
Ordre	ordre	k1gInSc1
de	de	k?
bataille	bataille	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Grande	grand	k1gMnSc5
Armée	Arméus	k1gMnSc5
</s>
<s>
I.	I.	kA
sbor	sbor	k1gInSc1
–	–	k?
maršál	maršál	k1gMnSc1
Bernadotte	Bernadott	k1gInSc5
(	(	kIx(
<g/>
Rivaudova	Rivaudův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Droetova	Droetův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
III	III	kA
<g/>
.	.	kIx.
sbor	sbor	k1gInSc1
–	–	k?
maršál	maršál	k1gMnSc1
Davout	Davout	k1gMnSc1
(	(	kIx(
<g/>
Friantova	Friantův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Bourcierova	Bourcierův	k2eAgFnSc1d1
dragounská	dragounský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
IV	IV	kA
<g/>
.	.	kIx.
sbor	sbor	k1gInSc1
–	–	k?
maršál	maršál	k1gMnSc1
Soult	Soult	k1gMnSc1
(	(	kIx(
<g/>
Saint-Hilairova	Saint-Hilairov	k1gInSc2
pěší	pěší	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Vandammova	Vandammův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Legrandova	Legrandův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Margaronova	Margaronův	k2eAgFnSc1d1
lehká	lehký	k2eAgFnSc1d1
jízdní	jízdní	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
V.	V.	kA
sbor	sbor	k1gInSc1
–	–	k?
maršál	maršál	k1gMnSc1
Lannes	Lannes	k1gMnSc1
(	(	kIx(
<g/>
Caffarelliho	Caffarelli	k1gMnSc2
pěší	pěší	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Suchetova	Suchetův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jezdecká	jezdecký	k2eAgFnSc1d1
záloha	záloha	k1gFnSc1
–	–	k?
maršál	maršál	k1gMnSc1
Murat	Murat	k2eAgMnSc1d1
(	(	kIx(
<g/>
Nansoutyho	Nansouty	k1gMnSc4
těžká	těžkat	k5eAaImIp3nS
jezdecká	jezdecký	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
ďHautpoulova	ďHautpoulův	k2eAgFnSc1d1
těžká	těžký	k2eAgFnSc1d1
jízdecká	jízdecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Waltherova	Waltherův	k2eAgFnSc1d1
dragounská	dragounský	k2eAgFnSc1d1
a	a	k8xC
Kellermannova	Kellermannův	k2eAgFnSc1d1
lehká	lehký	k2eAgFnSc1d1
jezdecká	jezdecký	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
záloha	záloha	k1gFnSc1
–	–	k?
císař	císař	k1gMnSc1
Napoleon	Napoleon	k1gMnSc1
(	(	kIx(
<g/>
Bessiè	Bessiè	k2eAgFnSc1d1
císařská	císařský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
,	,	kIx,
Oudinotova	Oudinotův	k2eAgFnSc1d1
a	a	k8xC
Durocova	Durocův	k2eAgFnSc1d1
granátnická	granátnický	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
Beaumontova	Beaumontův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Boyého	Boyého	k2eAgFnSc1d1
<g/>
)	)	kIx)
dragounská	dragounský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojenci	spojenec	k1gMnPc1
</s>
<s>
Armádní	armádní	k2eAgInSc1d1
předvoj	předvoj	k1gInSc1
–	–	k?
generálporučík	generálporučík	k1gMnSc1
Bagration	Bagration	k1gInSc1
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
pěchoty	pěchota	k1gFnSc2
<g/>
:	:	kIx,
generálmajoři	generálmajor	k1gMnPc1
Markov	Markov	k1gInSc4
<g/>
,	,	kIx,
Ulanius	Ulanius	k1gMnSc1
<g/>
,	,	kIx,
Kamenskij	Kamenskij	k1gMnSc1
II	II	kA
<g/>
,	,	kIx,
Engelhardt	Engelhardt	k1gMnSc1
<g/>
,	,	kIx,
generál-adjutant	generál-adjutant	k1gMnSc1
kníže	kníže	k1gMnSc1
Dolgorukov	Dolgorukov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
předvoj	předvoj	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
kolony	kolona	k1gFnPc1
–	–	k?
podmaršálek	podmaršálek	k1gMnSc1
Kienmayer	Kienmayer	k1gMnSc1
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
<g/>
:	:	kIx,
generálmajoři	generálmajor	k1gMnPc1
Carneville	Carneville	k1gNnSc2
<g/>
,	,	kIx,
Stutterheim	Stutterheim	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Nostic	Nostice	k1gFnPc2
<g/>
,	,	kIx,
Mořic	Mořic	k1gMnSc1
z	z	k7c2
Liechtensteina	Liechtenstein	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
–	–	k?
generálporučík	generálporučík	k1gMnSc1
Dochturov	Dochturov	k1gInSc1
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
pěchoty	pěchota	k1gFnSc2
<g/>
:	:	kIx,
generálmajoři	generálmajor	k1gMnPc1
Lewis	Lewis	k1gFnSc7
<g/>
,	,	kIx,
Urusov	Urusov	k1gInSc1
<g/>
,	,	kIx,
Lieders	Lieders	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
–	–	k?
generálporučík	generálporučík	k1gMnSc1
Langeron	Langeron	k1gMnSc1
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
pěchoty	pěchota	k1gFnSc2
<g/>
:	:	kIx,
generálmajoří	generálmajořit	k5eAaPmIp3nS,k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
Olsufjev	Olsufjev	k1gMnSc1
<g/>
,	,	kIx,
Kamenskij	Kamenskij	k1gMnSc1
I	I	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
–	–	k?
generálporučík	generálporučík	k1gMnSc1
Przybyszewski	Przybyszewsk	k1gFnSc2
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
pěchoty	pěchota	k1gFnSc2
<g/>
:	:	kIx,
generálporučík	generálporučík	k1gMnSc1
Wimpfen	Wimpfna	k1gFnPc2
<g/>
,	,	kIx,
generálmajoři	generálmajor	k1gMnPc1
Müller	Müller	k1gMnSc1
<g/>
,	,	kIx,
Selechov	Selechov	k1gInSc1
<g/>
,	,	kIx,
Strik	strika	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
(	(	kIx(
<g/>
smíšená	smíšený	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
podmaršálek	podmaršálek	k1gMnSc1
Kolowrat	Kolowrat	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
generálporučík	generálporučík	k1gMnSc1
Miloradovič	Miloradovič	k1gMnSc1
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
pěchoty	pěchota	k1gFnSc2
<g/>
:	:	kIx,
generálmajoři	generálmajor	k1gMnPc1
Berg	Berg	k1gMnSc1
<g/>
,	,	kIx,
Repninskij	Repninskij	k1gMnSc1
<g/>
,	,	kIx,
Rottermund	Rottermund	k1gMnSc1
<g/>
,	,	kIx,
Jirčík	Jirčík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
(	(	kIx(
<g/>
jízdní	jízdní	k2eAgFnSc1d1
smíšená	smíšený	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
podmaršálek	podmaršálek	k1gMnSc1
kníže	kníže	k1gMnSc1
Jan	Jan	k1gMnSc1
z	z	k7c2
Liechtensteina	Liechtenstein	k1gMnSc2
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
jezdectva	jezdectvo	k1gNnSc2
<g/>
:	:	kIx,
podmaršálek	podmaršálek	k1gMnSc1
Hohenlohe	Hohenloh	k1gMnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
generálmajoři	generálmajor	k1gMnPc1
Weeber	Weebra	k1gFnPc2
<g/>
,	,	kIx,
Caramelli	Caramell	k1gMnPc1
<g/>
,	,	kIx,
generálporučíci	generálporučík	k1gMnPc1
Uvarov	Uvarovo	k1gNnPc2
<g/>
,	,	kIx,
Essen	Essen	k1gInSc1
II	II	kA
<g/>
,	,	kIx,
Šepelev	Šepelev	k1gMnSc1
<g/>
,	,	kIx,
Pernickij	Pernickij	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
záloha	záloha	k1gFnSc1
(	(	kIx(
<g/>
carská	carský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
velkokníže	velkokníže	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
Pavlovič	Pavlovič	k1gMnSc1
(	(	kIx(
<g/>
velitelé	velitel	k1gMnPc1
pěchoty	pěchota	k1gFnSc2
<g/>
:	:	kIx,
generálmajoří	generálmajořit	k5eAaImIp3nS,k5eAaPmIp3nS
Maljutin	Maljutin	k1gMnSc1
<g/>
,	,	kIx,
Depreradovič	Depreradovič	k1gMnSc1
<g/>
,	,	kIx,
Lobanov	Lobanov	k1gInSc1
<g/>
;	;	kIx,
velitelé	velitel	k1gMnPc1
jezdectva	jezdectvo	k1gNnSc2
<g/>
:	:	kIx,
generálporučík	generálporučík	k1gMnSc1
Kologrivov	Kologrivov	k1gInSc4
<g/>
,	,	kIx,
generálmajoři	generálmajor	k1gMnPc1
Depreradovič	Depreradovič	k1gMnSc1
<g/>
,	,	kIx,
Jankovič	Jankovič	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bojiště	bojiště	k1gNnSc1
</s>
<s>
Hranice	hranice	k1gFnSc1
památkové	památkový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
bojiště	bojiště	k1gNnSc2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
</s>
<s>
Slavkovské	slavkovský	k2eAgNnSc1d1
bojiště	bojiště	k1gNnSc1
je	být	k5eAaImIp3nS
oblast	oblast	k1gFnSc4
rozkládající	rozkládající	k2eAgFnSc4d1
se	se	k3xPyFc4
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
dvou	dva	k4xCgMnPc2
okresů	okres	k1gInPc2
Vyškov	Vyškov	k1gInSc1
a	a	k8xC
Brno-venkov	Brno-venkov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
šířka	šířka	k1gFnSc1
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
8	#num#	k4
km	km	kA
a	a	k8xC
hloubka	hloubka	k1gFnSc1
kolem	kolem	k7c2
6	#num#	k4
km	km	kA
<g/>
,	,	kIx,
celé	celý	k2eAgNnSc1d1
území	území	k1gNnSc1
má	mít	k5eAaImIp3nS
pak	pak	k6eAd1
rozlohu	rozloha	k1gFnSc4
asi	asi	k9
120	#num#	k4
km²	km²	k?
a	a	k8xC
pojímá	pojímat	k5eAaImIp3nS
25	#num#	k4
obcí	obec	k1gFnPc2
a	a	k8xC
měst	město	k1gNnPc2
<g/>
:	:	kIx,
Bedřichovice	Bedřichovice	k1gFnSc1
<g/>
,	,	kIx,
Blažovice	Blažovice	k1gFnSc1
<g/>
,	,	kIx,
Holubice	holubice	k1gFnSc1
<g/>
,	,	kIx,
Hostěrádky-Rešov	Hostěrádky-Rešov	k1gInSc1
<g/>
,	,	kIx,
Hrušky	hruška	k1gFnPc1
<g/>
,	,	kIx,
Jiříkovice	Jiříkovice	k1gFnPc1
<g/>
,	,	kIx,
Kobylnice	Kobylnice	k1gFnPc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Kovalovice	Kovalovice	k1gFnSc1
<g/>
,	,	kIx,
Křenovice	Křenovice	k1gFnSc1
<g/>
,	,	kIx,
Měnín	Měnín	k1gInSc1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnSc1
<g/>
,	,	kIx,
Ponětovice	Ponětovice	k1gFnSc1
<g/>
,	,	kIx,
Pozořice	Pozořice	k1gFnSc1
<g/>
,	,	kIx,
Prace	Prace	k1gFnSc1
<g/>
,	,	kIx,
Sivice	Sivice	k1gFnSc1
<g/>
,	,	kIx,
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
Sokolnice	sokolnice	k1gFnSc1
<g/>
,	,	kIx,
Šaratice	šaratice	k1gFnSc1
<g/>
,	,	kIx,
Šlapanice	Šlapanice	k1gFnSc1
<g/>
,	,	kIx,
Telnice	Telnice	k1gFnSc1
<g/>
,	,	kIx,
Tvarožná	Tvarožný	k2eAgFnSc1d1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Bosenice	Bosenice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Újezd	Újezd	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
Velešovice	Velešovice	k1gFnSc1
<g/>
,	,	kIx,
Zbýšov	Zbýšov	k1gInSc1
<g/>
,	,	kIx,
Žatčany	Žatčan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lokalita	lokalita	k1gFnSc1
bojiště	bojiště	k1gNnSc2
je	být	k5eAaImIp3nS
vymezená	vymezený	k2eAgFnSc1d1
obcemi	obec	k1gFnPc7
Pozořice	Pozořice	k1gFnSc2
(	(	kIx(
<g/>
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
(	(	kIx(
<g/>
z	z	k7c2
východu	východ	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Žatčany	Žatčan	k1gMnPc4
(	(	kIx(
<g/>
z	z	k7c2
jihu	jih	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
Šlapanice	Šlapanice	k1gFnSc1
(	(	kIx(
<g/>
ze	z	k7c2
západu	západ	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
též	též	k9
starou	starý	k2eAgFnSc7d1
císařskou	císařský	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
(	(	kIx(
<g/>
ze	z	k7c2
severu	sever	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zlatým	zlatý	k2eAgInSc7d1
potokem	potok	k1gInSc7
neboli	neboli	k8xC
Říčkou	říčka	k1gFnSc7
(	(	kIx(
<g/>
ze	z	k7c2
západu	západ	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
říčkou	říčka	k1gFnSc7
Litavou	Litava	k1gFnSc7
(	(	kIx(
<g/>
z	z	k7c2
jihovýchodu	jihovýchod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jižním	jižní	k2eAgInSc6d1
cípu	cíp	k1gInSc6
bojiště	bojiště	k1gNnSc2
Litava	Litava	k1gFnSc1
plnila	plnit	k5eAaImAgFnS
dva	dva	k4xCgMnPc1
dnes	dnes	k6eAd1
již	již	k6eAd1
zaniklé	zaniklý	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
Žatčanský	Žatčanský	k2eAgInSc1d1
a	a	k8xC
Měnínský	Měnínský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
místě	místo	k1gNnSc6
u	u	k7c2
Ponětovic	Ponětovice	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Velatický	Velatický	k2eAgInSc1d1
potok	potok	k1gInSc1
slévá	slévat	k5eAaImIp3nS
s	s	k7c7
potokem	potok	k1gInSc7
Říčka	říčka	k1gFnSc1
ve	v	k7c4
Zlatý	zlatý	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
terén	terén	k1gInSc1
značně	značně	k6eAd1
podmáčený	podmáčený	k2eAgInSc1d1
a	a	k8xC
plný	plný	k2eAgInSc1d1
mokřin	mokřina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
severozápadu	severozápad	k1gInSc2
k	k	k7c3
jihovýchodu	jihovýchod	k1gInSc3
bojiště	bojiště	k1gNnSc2
protíná	protínat	k5eAaImIp3nS
Pratecké	pratecký	k2eAgNnSc4d1
návrší	návrší	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
hřeben	hřeben	k1gInSc4
korunují	korunovat	k5eAaBmIp3nP
kóta	kóta	k1gFnSc1
298	#num#	k4
Staré	Staré	k2eAgInPc1d1
vinohrady	vinohrad	k1gInPc1
a	a	k8xC
nejvyšší	vysoký	k2eAgFnSc1d3
kóta	kóta	k1gFnSc1
324	#num#	k4
Pracký	Pracký	k2eAgInSc4d1
kopec	kopec	k1gInSc4
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
zde	zde	k6eAd1
stojí	stát	k5eAaImIp3nS
Mohyla	mohyla	k1gFnSc1
míru	mír	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
severní	severní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
bojiště	bojiště	k1gNnSc4
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
ji	on	k3xPp3gFnSc4
zhruba	zhruba	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
vymezuje	vymezovat	k5eAaImIp3nS
dálnice	dálnice	k1gFnSc1
D	D	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
stojí	stát	k5eAaImIp3nS
u	u	k7c2
olomoucké	olomoucký	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
vrch	vrch	k1gInSc1
Padělek	padělek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
francouzští	francouzský	k2eAgMnPc1d1
veteráni	veterán	k1gMnPc1
přejmenovali	přejmenovat	k5eAaPmAgMnP
na	na	k7c4
Santon	Santon	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
jim	on	k3xPp3gMnPc3
připomínal	připomínat	k5eAaImAgInS
jiný	jiný	k2eAgInSc4d1
kopec	kopec	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
nějž	jenž	k3xRgMnSc4
narazili	narazit	k5eAaPmAgMnP
při	při	k7c6
tažení	tažení	k1gNnSc6
do	do	k7c2
Egypta	Egypt	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1798	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
východní	východní	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
strany	strana	k1gFnSc2
vyčnívá	vyčnívat	k5eAaImIp3nS
nad	nad	k7c4
terén	terén	k1gInSc4
o	o	k7c4
15	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
ze	z	k7c2
strany	strana	k1gFnSc2
západní	západní	k2eAgFnSc2d1
pak	pak	k6eAd1
o	o	k7c6
metrů	metr	k1gInPc2
80	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Dalším	další	k2eAgInSc7d1
významným	významný	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
za	za	k7c7
levým	levý	k2eAgNnSc7d1
francouzským	francouzský	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
je	být	k5eAaImIp3nS
kóta	kóta	k1gFnSc1
286	#num#	k4
Žuráň	Žuráň	k1gFnSc1
<g/>
,	,	kIx,
stojící	stojící	k2eAgFnSc1d1
uprostřed	uprostřed	k7c2
trojúhelníku	trojúhelník	k1gInSc2
obcí	obec	k1gFnPc2
Tvarožná	Tvarožný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Podolí	Podolí	k1gNnSc1
a	a	k8xC
Šlapanice	Šlapanice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
kopce	kopec	k1gInSc2
si	se	k3xPyFc3
v	v	k7c6
dopoledních	dopolední	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
Napoleon	Napoleon	k1gMnSc1
zřídil	zřídit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
velitelské	velitelský	k2eAgNnSc4d1
stanoviště	stanoviště	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plánování	plánování	k1gNnSc1
před	před	k7c7
bitvou	bitva	k1gFnSc7
</s>
<s>
Napoleonův	Napoleonův	k2eAgInSc1d1
záměr	záměr	k1gInSc1
</s>
<s>
Žuráň	Žuráň	k1gFnSc1
–	–	k?
vrchol	vrchol	k1gInSc4
kopce	kopec	k1gInSc2
s	s	k7c7
památníkem	památník	k1gInSc7
a	a	k8xC
mapou	mapa	k1gFnSc7
bojiště	bojiště	k1gNnSc2
</s>
<s>
Na	na	k7c4
Staré	Staré	k2eAgInPc4d1
vinohrady	vinohrad	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
měly	mít	k5eAaImAgInP
tvořit	tvořit	k5eAaImF
střed	střed	k1gInSc4
budoucího	budoucí	k2eAgNnSc2d1
bojiště	bojiště	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc1
z	z	k7c2
francouzských	francouzský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
dorazila	dorazit	k5eAaPmAgFnS
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
Margaronova	Margaronův	k2eAgFnSc1d1
jízdní	jízdní	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
bod	bod	k1gInSc1
i	i	k9
dnes	dnes	k6eAd1
ovládá	ovládat	k5eAaImIp3nS
okolní	okolní	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
a	a	k8xC
Napoleonovým	Napoleonův	k2eAgInSc7d1
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
rusko-rakouské	rusko-rakouský	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
,	,	kIx,
postupující	postupující	k2eAgInPc1d1
po	po	k7c6
olomoucké	olomoucký	k2eAgFnSc6d1
(	(	kIx(
<g/>
císařské	císařský	k2eAgFnSc6d1
<g/>
)	)	kIx)
silnici	silnice	k1gFnSc6
<g/>
,	,	kIx,
tuto	tento	k3xDgFnSc4
takticky	takticky	k6eAd1
výhodnou	výhodný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
opanovaly	opanovat	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přilákat	přilákat	k5eAaPmF
je	on	k3xPp3gNnSc4
sem	sem	k6eAd1
bylo	být	k5eAaImAgNnS
právě	právě	k6eAd1
Margaronovým	Margaronův	k2eAgInSc7d1
úkolem	úkol	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Rozhodnutí	rozhodnutí	k1gNnSc4
umožnit	umožnit	k5eAaPmF
nepříteli	nepřítel	k1gMnSc3
zmocnit	zmocnit	k5eAaPmF
se	se	k3xPyFc4
výšin	výšin	k1gInSc1
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Prace	Prace	k1gFnSc2
přesně	přesně	k6eAd1
zapadalo	zapadat	k5eAaImAgNnS,k5eAaPmAgNnS
do	do	k7c2
Napoleonova	Napoleonův	k2eAgInSc2d1
plánu	plán	k1gInSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
nechtěl	chtít	k5eNaImAgMnS
svést	svést	k5eAaPmF
pouze	pouze	k6eAd1
generální	generální	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc7
úmyslem	úmysl	k1gInSc7
bylo	být	k5eAaImAgNnS
nepřítele	nepřítel	k1gMnSc4
zlomit	zlomit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Jak	jak	k8xS,k8xC
sám	sám	k3xTgMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Jako	jako	k8xS,k8xC
pán	pán	k1gMnSc1
tohoto	tento	k3xDgNnSc2
nádherného	nádherný	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
bych	by	kYmCp1nS
mohl	moct	k5eAaImAgInS
Rusy	Rus	k1gMnPc4
zastavit	zastavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžu	Můžu	k?
jim	on	k3xPp3gFnPc3
je	být	k5eAaImIp3nS
však	však	k9
vydat	vydat	k5eAaPmF
a	a	k8xC
ustoupit	ustoupit	k5eAaPmF
s	s	k7c7
pravým	pravý	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestli	jestli	k8xS
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
na	na	k7c4
ty	ten	k3xDgFnPc4
výšiny	výšina	k1gFnPc4
vyšplhat	vyšplhat	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mě	já	k3xPp1nSc4
obklíčili	obklíčit	k5eAaPmAgMnP
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
neodvratně	odvratně	k6eNd1
ztraceni	ztracen	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Císař	Císař	k1gMnSc1
Francouzů	Francouz	k1gMnPc2
se	se	k3xPyFc4
tedy	tedy	k9
rozhodl	rozhodnout	k5eAaPmAgMnS
využít	využít	k5eAaPmF
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
spojenečtí	spojenecký	k2eAgMnPc1d1
generálové	generál	k1gMnPc1
Prateckou	pratecký	k2eAgFnSc4d1
pahorkatinu	pahorkatina	k1gFnSc4
opustí	opustit	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
napadnou	napadnout	k5eAaPmIp3nP
jeho	jeho	k3xOp3gMnSc1
zdánlivě	zdánlivě	k6eAd1
slabší	slabý	k2eAgNnSc4d2
postavení	postavení	k1gNnSc4
na	na	k7c6
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
a	a	k8xC
pokusí	pokusit	k5eAaPmIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
tak	tak	k6eAd1
odříznout	odříznout	k5eAaPmF
cestu	cesta	k1gFnSc4
na	na	k7c4
Vídeň	Vídeň	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Podobu	podoba	k1gFnSc4
svého	svůj	k3xOyFgInSc2
záměru	záměr	k1gInSc2
začal	začít	k5eAaPmAgInS
zevrubněji	zevrubně	k6eAd2
upřesňovat	upřesňovat	k5eAaImF
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
hlavě	hlava	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
rodil	rodit	k5eAaImAgInS
již	již	k6eAd1
přinejmenším	přinejmenším	k6eAd1
týden	týden	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
francouzským	francouzský	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
podařilo	podařit	k5eAaPmAgNnS
udělat	udělat	k5eAaPmF
zrcadlově	zrcadlově	k6eAd1
stejný	stejný	k2eAgInSc4d1
manévr	manévr	k1gInSc4
a	a	k8xC
opět	opět	k6eAd1
by	by	kYmCp3nP
se	se	k3xPyFc4
zmocnily	zmocnit	k5eAaPmAgFnP
Prateckého	pratecký	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
<g/>
,	,	kIx,
odřízly	odříznout	k5eAaPmAgFnP
by	by	kYmCp3nP
rusko-rakouské	rusko-rakouský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
od	od	k7c2
ústupových	ústupový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
na	na	k7c4
Olomouc	Olomouc	k1gFnSc4
a	a	k8xC
Vyškov	Vyškov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Davout	Davout	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
blížil	blížit	k5eAaImAgMnS
od	od	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
,	,	kIx,
by	by	kYmCp3nS
pak	pak	k6eAd1
fungoval	fungovat	k5eAaImAgInS
jako	jako	k9
druhá	druhý	k4xOgFnSc1
čelist	čelist	k1gFnSc1
kleští	kleštit	k5eAaImIp3nS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
nepřítel	nepřítel	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
absolutního	absolutní	k2eAgNnSc2d1
obklíčení	obklíčení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
plán	plán	k1gInSc1
se	se	k3xPyFc4
však	však	k9
zakládal	zakládat	k5eAaImAgInS
na	na	k7c6
předpokladu	předpoklad	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
ruští	ruský	k2eAgMnPc1d1
a	a	k8xC
rakouští	rakouský	k2eAgMnPc1d1
generálové	generál	k1gMnPc1
dopustili	dopustit	k5eAaPmAgMnP
školáckých	školácký	k2eAgFnPc2d1
chyb	chyba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
by	by	kYmCp3nP
opravdu	opravdu	k6eAd1
spojenci	spojenec	k1gMnPc1
sestoupili	sestoupit	k5eAaPmAgMnP
z	z	k7c2
Prateckých	pratecký	k2eAgFnPc2d1
výšin	výšina	k1gFnPc2
<g/>
,	,	kIx,
Napoleon	napoleon	k1gInSc1
se	se	k3xPyFc4
zcela	zcela	k6eAd1
realisticky	realisticky	k6eAd1
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nejspíš	nejspíš	k9
udeří	udeřit	k5eAaPmIp3nP
proti	proti	k7c3
jeho	jeho	k3xOp3gInSc3
středu	střed	k1gInSc3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
původní	původní	k2eAgInSc1d1
plán	plán	k1gInSc1
byl	být	k5eAaImAgInS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
ofenzivní	ofenzivní	k2eAgMnSc1d1
a	a	k8xC
počítal	počítat	k5eAaImAgInS
s	s	k7c7
přesně	přesně	k6eAd1
koordinovanými	koordinovaný	k2eAgInPc7d1
manévry	manévr	k1gInPc7
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
nakonec	nakonec	k6eAd1
stala	stát	k5eAaPmAgFnS
klasickým	klasický	k2eAgNnSc7d1
lineárním	lineární	k2eAgNnSc7d1
střetnutím	střetnutí	k1gNnSc7
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
její	její	k3xOp3gFnSc1
závěrečná	závěrečný	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
spočívala	spočívat	k5eAaImAgFnS
ve	v	k7c6
frontálním	frontální	k2eAgInSc6d1
útoku	útok	k1gInSc6
nepříteli	nepřítel	k1gMnSc3
do	do	k7c2
zad	záda	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojenecké	spojenecký	k2eAgFnPc1d1
dispozice	dispozice	k1gFnPc1
</s>
<s>
Autorem	autor	k1gMnSc7
bojového	bojový	k2eAgInSc2d1
plánu	plán	k1gInSc2
spojenců	spojenec	k1gMnPc2
byl	být	k5eAaImAgMnS
náčelník	náčelník	k1gMnSc1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
generálmajor	generálmajor	k1gMnSc1
Franz	Franz	k1gMnSc1
von	von	k1gInSc4
Weyrother	Weyrothra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
záměr	záměr	k1gInSc1
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
aby	aby	k9
tři	tři	k4xCgFnPc4
kolony	kolona	k1gFnPc4
pod	pod	k7c7
velením	velení	k1gNnSc7
generála	generál	k1gMnSc2
Buxhöwdena	Buxhöwden	k1gMnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochturova	Dochturův	k2eAgFnSc1d1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Langeronova	Langeronův	k2eAgFnSc1d1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Przybyszewského	Przybyszewského	k2eAgMnPc2d1
<g/>
)	)	kIx)
v	v	k7c6
7.00	7.00	k4
zahájily	zahájit	k5eAaPmAgInP
sestup	sestup	k1gInSc4
z	z	k7c2
Prateckého	pratecký	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
a	a	k8xC
zaútočily	zaútočit	k5eAaPmAgFnP
na	na	k7c4
Napoleonovo	Napoleonův	k2eAgNnSc4d1
pravé	pravý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
prolomení	prolomení	k1gNnSc6
francouzských	francouzský	k2eAgFnPc2d1
řad	řada	k1gFnPc2
měly	mít	k5eAaImAgInP
přejít	přejít	k5eAaPmF
Zlatý	zlatý	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
se	se	k3xPyFc4
obrátily	obrátit	k5eAaPmAgFnP
vpravo	vpravo	k6eAd1
a	a	k8xC
zformovaly	zformovat	k5eAaPmAgInP
se	se	k3xPyFc4
do	do	k7c2
jedné	jeden	k4xCgFnSc2
linie	linie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
by	by	kYmCp3nP
postupovaly	postupovat	k5eAaImAgFnP
napříč	napříč	k6eAd1
protivníkovým	protivníkův	k2eAgNnSc7d1
pravým	pravý	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
až	až	k9
ke	k	k7c3
středu	střed	k1gInSc2
jeho	jeho	k3xOp3gFnSc2
formace	formace	k1gFnSc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
by	by	kYmCp3nS
mezitím	mezitím	k6eAd1
prošla	projít	k5eAaPmAgFnS
Kobylnicí	Kobylnice	k1gFnSc7
a	a	k8xC
následně	následně	k6eAd1
by	by	kYmCp3nS
s	s	k7c7
Buxhöwdenovými	Buxhöwdenův	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
srovnala	srovnat	k5eAaPmAgFnS
linii	linie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
předvoje	předvoj	k1gInSc2
generála	generál	k1gMnSc4
Bagrationa	Bagration	k1gMnSc4
bylo	být	k5eAaImAgNnS
postupovat	postupovat	k5eAaImF
podél	podél	k7c2
císařské	císařský	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
a	a	k8xC
vázat	vázat	k5eAaImF
tak	tak	k6eAd1
nepřátelské	přátelský	k2eNgNnSc4d1
levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
narušení	narušení	k1gNnSc6
protivníkových	protivníkův	k2eAgFnPc2d1
řad	řada	k1gFnPc2
měl	mít	k5eAaImAgInS
v	v	k7c6
součinnosti	součinnost	k1gFnSc6
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
kolonami	kolona	k1gFnPc7
pronásledovat	pronásledovat	k5eAaImF
Francouze	Francouz	k1gMnPc4
až	až	k9
k	k	k7c3
Brnu	Brno	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
Lichtenštejnova	Lichtenštejnův	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
bylo	být	k5eAaImAgNnS
spolu	spolu	k6eAd1
s	s	k7c7
Bagrationem	Bagration	k1gInSc7
postupovat	postupovat	k5eAaImF
podél	podél	k7c2
císařské	císařský	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
<g/>
,	,	kIx,
podpořit	podpořit	k5eAaPmF
útok	útok	k1gInSc4
pěchoty	pěchota	k1gFnSc2
a	a	k8xC
posléze	posléze	k6eAd1
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
prchajícího	prchající	k2eAgMnSc4d1
nepřítele	nepřítel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zálohou	záloha	k1gFnSc7
byla	být	k5eAaImAgFnS
pro	pro	k7c4
den	den	k1gInSc4
bitvy	bitva	k1gFnSc2
určena	určit	k5eAaPmNgFnS
carská	carský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
sbor	sbor	k1gInSc1
Jeho	jeho	k3xOp3gFnSc2
Výsosti	výsost	k1gFnSc2
Konstantina	Konstantin	k1gMnSc2
Pavloviče	Pavlovič	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
svítání	svítání	k1gNnSc4
zaujmout	zaujmout	k5eAaPmF
postavení	postavení	k1gNnSc4
za	za	k7c7
Blažovicemi	Blažovice	k1gFnPc7
a	a	k8xC
Kruhem	kruh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
podporovat	podporovat	k5eAaImF
knížete	kníže	k1gMnSc4
Lichtenštejna	Lichtenštejn	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
měl	mít	k5eAaImAgMnS
chránit	chránit	k5eAaImF
levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
Bagrationova	Bagrationův	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
u	u	k7c2
Santonu	Santon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
neúspěšného	úspěšný	k2eNgInSc2d1
vývoje	vývoj	k1gInSc2
bitvy	bitva	k1gFnSc2
měla	mít	k5eAaImAgFnS
armáda	armáda	k1gFnSc1
ustoupit	ustoupit	k5eAaPmF
směrem	směr	k1gInSc7
na	na	k7c4
Slavkov	Slavkov	k1gInSc4
<g/>
,	,	kIx,
kam	kam	k6eAd1
už	už	k6eAd1
bylo	být	k5eAaImAgNnS
předem	předem	k6eAd1
odesláno	odeslat	k5eAaPmNgNnS
vozatajstvo	vozatajstvo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poprvé	poprvé	k6eAd1
byli	být	k5eAaImAgMnP
jednotliví	jednotlivý	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
spojeneckých	spojenecký	k2eAgFnPc2d1
kolon	kolona	k1gFnPc2
s	s	k7c7
dispozicemi	dispozice	k1gFnPc7
seznámeni	seznámen	k2eAgMnPc1d1
krátce	krátce	k6eAd1
po	po	k7c6
půlnoci	půlnoc	k1gFnSc6
v	v	k7c4
den	den	k1gInSc4
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byl	být	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
plán	plán	k1gInSc1
přepisován	přepisován	k2eAgInSc1d1
do	do	k7c2
desítek	desítka	k1gFnPc2
kopií	kopie	k1gFnPc2
a	a	k8xC
ty	ten	k3xDgFnPc1
byly	být	k5eAaImAgFnP
pobočníky	pobočník	k1gMnPc4
rozváženy	rozvážen	k2eAgFnPc1d1
k	k	k7c3
jednotlivým	jednotlivý	k2eAgMnPc3d1
velitelům	velitel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dispozice	dispozice	k1gFnSc1
byly	být	k5eAaImAgFnP
psány	psát	k5eAaImNgFnP
německy	německy	k6eAd1
<g/>
,	,	kIx,
pro	pro	k7c4
ruské	ruský	k2eAgMnPc4d1
velitele	velitel	k1gMnPc4
je	být	k5eAaImIp3nS
tedy	tedy	k8xC
bylo	být	k5eAaImAgNnS
nutno	nutno	k6eAd1
přeložit	přeložit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naneštěstí	naneštěstí	k9
byly	být	k5eAaImAgInP
tyto	tento	k3xDgInPc1
překlady	překlad	k1gInPc1
velmi	velmi	k6eAd1
špatné	špatný	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
a	a	k8xC
tudíž	tudíž	k8xC
mnohdy	mnohdy	k6eAd1
nesrozumitelné	srozumitelný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znění	znění	k1gNnSc1
rozkazů	rozkaz	k1gInPc2
pobočníci	pobočník	k1gMnPc1
v	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
velitelům	velitel	k1gMnPc3
pouze	pouze	k6eAd1
přečetli	přečíst	k5eAaPmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
písemných	písemný	k2eAgFnPc2d1
kopií	kopie	k1gFnPc2
se	se	k3xPyFc4
nedostávalo	dostávat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
některým	některý	k3yIgMnPc3
velitelům	velitel	k1gMnPc3
dispozice	dispozice	k1gFnSc2
dorazily	dorazit	k5eAaPmAgInP
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
už	už	k6eAd1
byla	být	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
v	v	k7c6
plném	plný	k2eAgInSc6d1
proudu	proud	k1gInSc6
<g/>
,	,	kIx,
k	k	k7c3
některým	některý	k3yIgInPc3
nedorazily	dorazit	k5eNaPmAgInP
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Car	car	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
</s>
<s>
Císař	Císař	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
</s>
<s>
Michail	Michail	k1gMnSc1
Illarionovič	Illarionovič	k1gMnSc1
Kutuzov	Kutuzov	k1gInSc4
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1
přípravy	příprava	k1gFnPc1
</s>
<s>
Nástup	nástup	k1gInSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
jižně	jižně	k6eAd1
od	od	k7c2
olomoucké	olomoucký	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
a	a	k8xC
postavení	postavení	k1gNnSc4
Francouzů	Francouz	k1gMnPc2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1805	#num#	k4
</s>
<s>
Podle	podle	k7c2
Napoleonova	Napoleonův	k2eAgNnSc2d1
očekávání	očekávání	k1gNnSc2
spojenci	spojenec	k1gMnSc3
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
Pratecké	pratecký	k2eAgFnSc2d1
výšiny	výšina	k1gFnSc2
skutečně	skutečně	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
se	se	k3xPyFc4
však	však	k9
dozvěděl	dozvědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
maršál	maršál	k1gMnSc1
Davout	Davout	k1gMnSc1
přivede	přivést	k5eAaPmIp3nS
do	do	k7c2
bitvy	bitva	k1gFnSc2
pouze	pouze	k6eAd1
značně	značně	k6eAd1
oslabenou	oslabený	k2eAgFnSc4d1
Friantovu	Friantův	k2eAgFnSc4d1
pěší	pěší	k2eAgFnSc4d1
a	a	k8xC
Bourcierovu	Bourcierův	k2eAgFnSc4d1
jezdeckou	jezdecký	k2eAgFnSc4d1
divizi	divize	k1gFnSc4
a	a	k8xC
nepřítel	nepřítel	k1gMnSc1
posunul	posunout	k5eAaPmAgMnS
svá	svůj	k3xOyFgNnPc4
postavení	postavení	k1gNnPc4
mnohem	mnohem	k6eAd1
víc	hodně	k6eAd2
k	k	k7c3
jihu	jih	k1gInSc3
než	než	k8xS
počítal	počítat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
informace	informace	k1gFnPc1
o	o	k7c6
absenci	absence	k1gFnSc6
kompletního	kompletní	k2eAgNnSc2d1
III	III	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
znamenala	znamenat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
nebude	být	k5eNaImBp3nS
schopen	schopen	k2eAgMnSc1d1
uskutečnit	uskutečnit	k5eAaPmF
záměr	záměr	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
chtěl	chtít	k5eAaImAgMnS
sevřít	sevřít	k5eAaPmF
spojence	spojenec	k1gMnSc4
do	do	k7c2
kleští	kleště	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posílil	posílit	k5eAaPmAgMnS
tedy	tedy	k9
Legrandovu	Legrandův	k2eAgFnSc4d1
divizi	divize	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c6
nejjižnějším	jižní	k2eAgInSc6d3
cípu	cíp	k1gInSc6
pravého	pravý	k2eAgNnSc2d1
křídla	křídlo	k1gNnSc2
nést	nést	k5eAaImF
největší	veliký	k2eAgFnSc4d3
tíži	tíže	k1gFnSc4
zítřejšího	zítřejší	k2eAgInSc2d1
protivníkova	protivníkův	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
A	a	k8xC
právě	právě	k9
tehdy	tehdy	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
definitivní	definitivní	k2eAgInSc1d1
plán	plán	k1gInSc1
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
měly	mít	k5eAaImAgFnP
Pratecké	pratecký	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
obsadit	obsadit	k5eAaPmF
Vandammova	Vandammův	k2eAgFnSc1d1
a	a	k8xC
Saint-Hilairova	Saint-Hilairův	k2eAgFnSc1d1
divize	divize	k1gFnSc1
náležející	náležející	k2eAgFnSc1d1
ke	k	k7c3
IV	IV	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
maršála	maršál	k1gMnSc4
Soulta	Soult	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměr	záměr	k1gInSc1
se	se	k3xPyFc4
z	z	k7c2
defenzivního	defenzivní	k2eAgMnSc2d1
rázem	rázem	k6eAd1
změnil	změnit	k5eAaPmAgInS
na	na	k7c4
ofenzivní	ofenzivní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
tímto	tento	k3xDgNnSc7
rozhodnutím	rozhodnutí	k1gNnSc7
určené	určený	k2eAgFnSc2d1
Soultovy	Soultův	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
za	za	k7c4
tmy	tma	k1gFnPc4
překročily	překročit	k5eAaPmAgFnP
Zlatý	zlatý	k2eAgInSc4d1
potok	potok	k1gInSc4
v	v	k7c6
prostoru	prostor	k1gInSc6
mezi	mezi	k7c7
Ponětovicemi	Ponětovice	k1gFnPc7
a	a	k8xC
Kobylnicemi	Kobylnice	k1gFnPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
vytvořily	vytvořit	k5eAaPmAgFnP
jednu	jeden	k4xCgFnSc4
souvislou	souvislý	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Severně	severně	k6eAd1
od	od	k7c2
nich	on	k3xPp3gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
francouzském	francouzský	k2eAgNnSc6d1
levém	levý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
<g/>
,	,	kIx,
stál	stát	k5eAaImAgMnS
V.	V.	kA
sbor	sbor	k1gInSc1
maršála	maršál	k1gMnSc2
Lannese	Lannese	k1gFnSc2
<g/>
,	,	kIx,
kterému	který	k3yIgNnSc3,k3yQgNnSc3,k3yRgNnSc3
připadla	připadnout	k5eAaPmAgFnS
úloha	úloha	k1gFnSc1
zabránit	zabránit	k5eAaPmF
jakémukoliv	jakýkoliv	k3yIgInSc3
obchvatu	obchvat	k1gInSc3
z	z	k7c2
prostoru	prostor	k1gInSc2
u	u	k7c2
olomoucké	olomoucký	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
I.	I.	kA
sboru	sbor	k1gInSc3
maršál	maršál	k1gMnSc1
Bernadotte	Bernadott	k1gInSc5
obdržel	obdržet	k5eAaPmAgInS
instrukce	instrukce	k1gFnPc4
nad	nad	k7c7
ránem	ráno	k1gNnSc7
překročit	překročit	k5eAaPmF
potok	potok	k1gInSc4
v	v	k7c6
prostoru	prostor	k1gInSc6
u	u	k7c2
Jiříkovic	Jiříkovice	k1gFnPc2
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
podpořit	podpořit	k5eAaPmF
útočící	útočící	k2eAgFnPc4d1
divize	divize	k1gFnPc4
IV	IV	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maršál	maršál	k1gMnSc1
Murat	Murat	k1gInSc4
byl	být	k5eAaImAgMnS
pověřen	pověřen	k2eAgMnSc1d1
přivést	přivést	k5eAaPmF
své	svůj	k3xOyFgNnSc4
jezdectvo	jezdectvo	k1gNnSc4
do	do	k7c2
takových	takový	k3xDgMnPc2
postavení	postavený	k2eAgMnPc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pokryl	pokrýt	k5eAaPmAgInS
případný	případný	k2eAgInSc1d1
útok	útok	k1gInSc1
do	do	k7c2
prázdného	prázdný	k2eAgNnSc2d1
místa	místo	k1gNnSc2
mezi	mezi	k7c7
pravým	pravý	k2eAgNnSc7d1
Lannesovým	Lannesový	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
a	a	k8xC
Soultem	Soult	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maršálu	maršál	k1gMnSc3
Davoutovi	Davout	k1gMnSc3
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc2
jednotky	jednotka	k1gFnSc2
se	se	k3xPyFc4
shromažďovaly	shromažďovat	k5eAaImAgInP
v	v	k7c6
Rajhradu	Rajhrad	k1gInSc6
<g/>
,	,	kIx,
ukládaly	ukládat	k5eAaImAgFnP
dispozice	dispozice	k1gFnPc1
v	v	k7c4
pět	pět	k4xCc4
hodin	hodina	k1gFnPc2
ráno	ráno	k6eAd1
vyrazit	vyrazit	k5eAaPmF
k	k	k7c3
Telnici	Telnice	k1gFnSc3
a	a	k8xC
Sokolnicím	sokolnice	k1gFnPc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
společně	společně	k6eAd1
s	s	k7c7
generálem	generál	k1gMnSc7
Legrandem	Legrand	k1gInSc7
za	za	k7c2
neustálého	neustálý	k2eAgInSc2d1
boje	boj	k1gInSc2
zadržovat	zadržovat	k5eAaImF
nepřítele	nepřítel	k1gMnSc4
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
bylo	být	k5eAaImAgNnS
nutno	nutno	k6eAd1
zabránit	zabránit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
na	na	k7c4
Pratecké	pratecký	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zálohu	záloh	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
přímo	přímo	k6eAd1
Napoleonovi	Napoleon	k1gMnSc3
u	u	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
velitelského	velitelský	k2eAgNnSc2d1
stanoviště	stanoviště	k1gNnSc2
na	na	k7c4
Žuráni	Žurán	k1gMnPc1
<g/>
,	,	kIx,
tvořila	tvořit	k5eAaImAgFnS
císařská	císařský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
jednou	jeden	k4xCgFnSc7
divizí	divize	k1gFnSc7
granátníků	granátník	k1gMnPc2
a	a	k8xC
jednou	jednou	k6eAd1
divizí	divize	k1gFnSc7
dragounů	dragoun	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInPc1
boje	boj	k1gInPc1
u	u	k7c2
Telnice	Telnice	k1gFnSc2
</s>
<s>
První	první	k4xOgInPc1
výstřely	výstřel	k1gInPc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
padly	padnout	k5eAaImAgFnP,k5eAaPmAgFnP
v	v	k7c6
pozdních	pozdní	k2eAgFnPc6d1
nočních	noční	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
před	před	k7c7
Telnicí	Telnice	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
střežilo	střežit	k5eAaImAgNnS
100	#num#	k4
až	až	k8xS
150	#num#	k4
pádských	pádský	k2eAgInPc2d1
tirailleurů	tirailleur	k1gInPc2
<g/>
,	,	kIx,
objevila	objevit	k5eAaPmAgFnS
půleskadrona	půleskadrona	k1gFnSc1
rakouských	rakouský	k2eAgMnPc2d1
švališerů	švališer	k1gMnPc2
CLR3	CLR3	k1gFnSc2
O	o	k7c4
<g/>
′	′	k?
<g/>
Reilly	Reilla	k1gFnPc4
<g/>
,	,	kIx,
náležející	náležející	k2eAgNnSc4d1
k	k	k7c3
předvoji	předvoj	k1gInSc3
1	#num#	k4
<g/>
.	.	kIx.
kolony	kolona	k1gFnSc2
podmaršálka	podmaršálek	k1gMnSc4
Kienmayera	Kienmayer	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
začali	začít	k5eAaPmAgMnP
protivníka	protivník	k1gMnSc4
ostřelovat	ostřelovat	k5eAaImF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
k	k	k7c3
tomuto	tento	k3xDgInSc3
úseku	úsek	k1gInSc3
bojiště	bojiště	k1gNnSc2
přilákalo	přilákat	k5eAaPmAgNnS
pozornost	pozornost	k1gFnSc4
jak	jak	k8xC,k8xS
Napoleona	Napoleon	k1gMnSc4
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
generála	generál	k1gMnSc2
Legranda	legranda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
kolem	kolem	k7c2
půlnoci	půlnoc	k1gFnSc2
poslal	poslat	k5eAaPmAgMnS
tiraillerům	tirailler	k1gMnPc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
řadového	řadový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
a	a	k8xC
Rakušané	Rakušan	k1gMnPc1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
vesnici	vesnice	k1gFnSc3
vyklidit	vyklidit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
tento	tento	k3xDgInSc1
ústup	ústup	k1gInSc1
byl	být	k5eAaImAgInS
jen	jen	k9
dočasný	dočasný	k2eAgInSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
po	po	k7c6
dvou	dva	k4xCgFnPc6
hodinách	hodina	k1gFnPc6
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
doprovázeni	doprovázen	k2eAgMnPc1d1
jednotkou	jednotka	k1gFnSc7
hraničářů	hraničář	k1gMnPc2
a	a	k8xC
pokusili	pokusit	k5eAaPmAgMnP
se	se	k3xPyFc4
dobýt	dobýt	k5eAaPmF
Telnici	Telnice	k1gFnSc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gInPc1
útoky	útok	k1gInPc1
však	však	k9
byly	být	k5eAaImAgInP
Francouzi	Francouz	k1gMnPc1
ukrytými	ukrytý	k2eAgFnPc7d1
za	za	k7c7
zídkami	zídka	k1gFnPc7
zahrad	zahrada	k1gFnPc2
a	a	k8xC
v	v	k7c6
oknech	okno	k1gNnPc6
domů	domů	k6eAd1
opakovaně	opakovaně	k6eAd1
odraženy	odražen	k2eAgInPc1d1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
generál	generál	k1gMnSc1
Legrand	legranda	k1gFnPc2
do	do	k7c2
Telnice	Telnice	k1gFnSc2
odeslal	odeslat	k5eAaPmAgMnS
zbytek	zbytek	k1gInSc4
3	#num#	k4
<g/>
.	.	kIx.
řadového	řadový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmaršálek	podmaršálek	k1gMnSc1
Kienmayer	Kienmayer	k1gMnSc1
<g/>
,	,	kIx,
vědom	vědom	k2eAgMnSc1d1
si	se	k3xPyFc3
taktického	taktický	k2eAgInSc2d1
významu	význam	k1gInSc2
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
vyslal	vyslat	k5eAaPmAgMnS
vojáky	voják	k1gMnPc4
do	do	k7c2
opětovného	opětovný	k2eAgInSc2d1
útoku	útok	k1gInSc2
asi	asi	k9
kolem	kolem	k7c2
páté	pátý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
první	první	k4xOgInPc4
dva	dva	k4xCgInPc4
nápory	nápor	k1gInPc1
byly	být	k5eAaImAgInP
odraženy	odrazit	k5eAaPmNgInP
ještě	ještě	k6eAd1
před	před	k7c7
vsí	ves	k1gFnSc7
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
třetí	třetí	k4xOgInSc4
úder	úder	k1gInSc4
řetěz	řetěz	k1gInSc4
francouzských	francouzský	k2eAgMnPc2d1
voltižérů	voltižér	k1gMnPc2
a	a	k8xC
tirailleurů	tirailleur	k1gMnPc2
prolomil	prolomit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakušané	Rakušan	k1gMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
opět	opět	k5eAaPmF
odraženi	odrazit	k5eAaPmNgMnP
vojáky	voják	k1gMnPc7
ukrytými	ukrytý	k2eAgFnPc7d1
v	v	k7c6
úvozech	úvoz	k1gInPc6
před	před	k7c7
vesnicí	vesnice	k1gFnSc7
a	a	k8xC
čtyřmi	čtyři	k4xCgInPc7
děly	dělo	k1gNnPc7
i	i	k8xC
pěchotou	pěchota	k1gFnSc7
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
samotné	samotný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
část	část	k1gFnSc4
hraničářů	hraničář	k1gMnPc2
se	se	k3xPyFc4
probila	probít	k5eAaPmAgFnS
mezi	mezi	k7c4
chalupy	chalupa	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
podpory	podpora	k1gFnSc2
byli	být	k5eAaImAgMnP
brzy	brzy	k6eAd1
nuceni	nutit	k5eAaImNgMnP
své	svůj	k3xOyFgFnPc4
pozice	pozice	k1gFnPc4
s	s	k7c7
vysokými	vysoký	k2eAgFnPc7d1
ztrátami	ztráta	k1gFnPc7
vyklidit	vyklidit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Někdy	někdy	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
o	o	k7c4
7	#num#	k4
<g/>
.	.	kIx.
hodině	hodina	k1gFnSc6
ranní	ranní	k2eAgFnSc6d1
<g/>
)	)	kIx)
ruští	ruský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
na	na	k7c6
Prateckém	pratecký	k2eAgNnSc6d1
návrší	návrší	k1gNnSc6
odložili	odložit	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
tornistry	tornistra	k1gFnPc4
a	a	k8xC
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
Weyrotherovou	Weyrotherův	k2eAgFnSc7d1
dispozicí	dispozice	k1gFnSc7
začali	začít	k5eAaPmAgMnP
sestupovat	sestupovat	k5eAaImF
směrem	směr	k1gInSc7
k	k	k7c3
postavením	postavení	k1gNnPc3
Francouzů	Francouz	k1gMnPc2
v	v	k7c6
údolí	údolí	k1gNnSc6
Zlatého	zlatý	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několikatisícové	několikatisícový	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
se	se	k3xPyFc4
ovšem	ovšem	k9
v	v	k7c6
neznámém	známý	k2eNgInSc6d1
terénu	terén	k1gInSc6
a	a	k8xC
stávající	stávající	k2eAgFnSc6d1
tmě	tma	k1gFnSc6
začaly	začít	k5eAaPmAgInP
vzájemně	vzájemně	k6eAd1
proplétat	proplétat	k5eAaImF
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
příčinou	příčina	k1gFnSc7
nevyhnutelného	vyhnutelný	k2eNgInSc2d1
zmatku	zmatek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Pár	pár	k4xCyI
minut	minuta	k1gFnPc2
před	před	k7c7
sedmou	sedma	k1gFnSc7
začalo	začít	k5eAaPmAgNnS
svítat	svítat	k5eAaImF
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
nad	nad	k7c7
prostorem	prostor	k1gInSc7
bojiště	bojiště	k1gNnSc2
panovala	panovat	k5eAaImAgFnS
hustá	hustý	k2eAgFnSc1d1
mlha	mlha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
ní	on	k3xPp3gFnSc2
se	se	k3xPyFc4
mezi	mezi	k7c4
půl	půl	k1xP
osmou	osmý	k4xOgFnSc7
a	a	k8xC
osmou	osmý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
před	před	k7c7
Telnicí	Telnice	k1gFnSc7
vynořilo	vynořit	k5eAaPmAgNnS
čelo	čelo	k1gNnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochturovovy	Dochturovův	k2eAgFnPc4d1
kolony	kolona	k1gFnPc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
musela	muset	k5eAaImAgFnS
kvůli	kvůli	k7c3
špatnému	špatný	k2eAgInSc3d1
terénu	terén	k1gInSc3
zvolit	zvolit	k5eAaPmF
okliku	oklika	k1gFnSc4
přes	přes	k7c4
Hostěrádky	Hostěrádka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Ruský	ruský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
okamžitě	okamžitě	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
do	do	k7c2
boje	boj	k1gInSc2
brigádu	brigáda	k1gFnSc4
generálmajora	generálmajor	k1gMnSc2
Lewise	Lewise	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vnikla	vniknout	k5eAaPmAgFnS
mezi	mezi	k7c4
domy	dům	k1gInPc4
a	a	k8xC
přinutila	přinutit	k5eAaPmAgFnS
od	od	k7c2
půlnoci	půlnoc	k1gFnSc2
vzdorující	vzdorující	k2eAgFnSc2d1
Francouze	Francouz	k1gMnSc2
vesnici	vesnice	k1gFnSc4
vyklidit	vyklidit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
V	v	k7c4
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
však	však	k9
generál	generál	k1gMnSc1
Dochturov	Dochturov	k1gInSc1
i	i	k8xC
podmaršálek	podmaršálek	k1gMnSc1
Kienmayer	Kienmayer	k1gMnSc1
obdrželi	obdržet	k5eAaPmAgMnP
od	od	k7c2
generála	generál	k1gMnSc2
Buxhöwdena	Buxhöwdena	k1gFnSc1
povel	povel	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zastavili	zastavit	k5eAaPmAgMnP
další	další	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
Weyrotherova	Weyrotherův	k2eAgFnSc1d1
dispozice	dispozice	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
koloně	kolona	k1gFnSc6
ukládala	ukládat	k5eAaImAgFnS
útočit	útočit	k5eAaImF
v	v	k7c6
jedné	jeden	k4xCgFnSc6
linii	linie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
by	by	kYmCp3nS
generál	generál	k1gMnSc1
Dochturov	Dochturov	k1gInSc4
své	svůj	k3xOyFgMnPc4
muže	muž	k1gMnPc4
rád	rád	k6eAd1
vedl	vést	k5eAaImAgMnS
vpřed	vpřed	k6eAd1
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
se	se	k3xPyFc4
tomuto	tento	k3xDgInSc3
rozkazu	rozkaz	k1gInSc3
podřídit	podřídit	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
kolony	kolona	k1gFnPc1
generálů	generál	k1gMnPc2
Przybyszewského	Przybyszewský	k1gMnSc2
a	a	k8xC
Langerona	Langeron	k1gMnSc2
prozatím	prozatím	k6eAd1
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
pozic	pozice	k1gFnPc2
nedorazily	dorazit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
jednotky	jednotka	k1gFnPc1
snad	snad	k9
až	až	k9
o	o	k7c4
celou	celý	k2eAgFnSc4d1
hodinu	hodina	k1gFnSc4
zdržela	zdržet	k5eAaPmAgFnS
5	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
knížete	kníže	k1gMnSc2
Lichtensteina	Lichtenstein	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
jim	on	k3xPp3gMnPc3
zkřížila	zkřížit	k5eAaPmAgFnS
cestu	cesta	k1gFnSc4
při	při	k7c6
zrychleném	zrychlený	k2eAgInSc6d1
přesunu	přesun	k1gInSc6
do	do	k7c2
sektoru	sektor	k1gInSc2
mezi	mezi	k7c7
Blažovicemi	Blažovice	k1gFnPc7
a	a	k8xC
císařskou	císařský	k2eAgFnSc7d1
silnicí	silnice	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
Křenovic	Křenovice	k1gFnPc2
prozatím	prozatím	k6eAd1
nevyrazila	vyrazit	k5eNaPmAgFnS
ani	ani	k9
4	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
podmaršálka	podmaršálek	k1gMnSc2
Kolovrata	Kolovrat	k1gMnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
ji	on	k3xPp3gFnSc4
zde	zde	k6eAd1
zdržel	zdržet	k5eAaPmAgMnS
nominální	nominální	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
hrabě	hrabě	k1gMnSc1
Kutuzov	Kutuzov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slavkovské	slavkovský	k2eAgNnSc4d1
bojiště	bojiště	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohled	pohled	k1gInSc1
z	z	k7c2
Prackého	Pracký	k2eAgInSc2d1
kopce	kopec	k1gInSc2
směrem	směr	k1gInSc7
k	k	k7c3
severu	sever	k1gInSc3
<g/>
,	,	kIx,
vesnice	vesnice	k1gFnSc1
Prace	Prace	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
popředí	popředí	k1gNnSc6
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1
na	na	k7c6
Zlatém	zlatý	k2eAgInSc6d1
potoku	potok	k1gInSc6
</s>
<s>
Telnice	Telnice	k1gFnSc1
</s>
<s>
V	v	k7c4
šest	šest	k4xCc4
hodin	hodina	k1gFnPc2
ráno	ráno	k6eAd1
opustily	opustit	k5eAaPmAgInP
Rajhrad	Rajhrad	k1gInSc4
Friantova	Friantův	k2eAgFnSc1d1
pěší	pěší	k2eAgFnSc1d1
a	a	k8xC
Bourcierova	Bourcierův	k2eAgFnSc1d1
jízdní	jízdní	k2eAgFnSc1d1
divize	divize	k1gFnSc1
III	III	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
francouzského	francouzský	k2eAgMnSc4d1
maršála	maršál	k1gMnSc4
Davouta	Davout	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
jednotky	jednotka	k1gFnPc1
měly	mít	k5eAaImAgFnP
podle	podle	k7c2
nových	nový	k2eAgInPc2d1
rozkazů	rozkaz	k1gInPc2
posílit	posílit	k5eAaPmF
osamoceného	osamocený	k2eAgMnSc4d1
generála	generál	k1gMnSc4
Legranda	legranda	k1gFnSc1
u	u	k7c2
Sokolnic	sokolnice	k1gFnPc2
a	a	k8xC
Telnice	Telnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	nedlouho	k1gNnSc1
před	před	k7c7
příchodem	příchod	k1gInSc7
na	na	k7c4
bojiště	bojiště	k1gNnSc4
(	(	kIx(
<g/>
asi	asi	k9
v	v	k7c4
8	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
)	)	kIx)
k	k	k7c3
maršálovi	maršál	k1gMnSc3
přispěchal	přispěchat	k5eAaPmAgMnS
jezdecký	jezdecký	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
s	s	k7c7
naléhavou	naléhavý	k2eAgFnSc7d1
žádostí	žádost	k1gFnSc7
o	o	k7c4
podporu	podpora	k1gFnSc4
pěchoty	pěchota	k1gFnSc2
za	za	k7c7
Telnicí	Telnice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
boje	boj	k1gInSc2
byla	být	k5eAaImAgFnS
okamžitě	okamžitě	k6eAd1
vyslána	vyslat	k5eAaPmNgFnS
Heudeletova	Heudeletův	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
součinnosti	součinnost	k1gFnSc6
se	s	k7c7
zbytky	zbytek	k1gInPc7
3	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
zhruba	zhruba	k6eAd1
v	v	k7c4
9	#num#	k4
hodin	hodina	k1gFnPc2
přešla	přejít	k5eAaPmAgFnS
z	z	k7c2
pochodu	pochod	k1gInSc2
rovnou	rovnou	k6eAd1
do	do	k7c2
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
cílem	cíl	k1gInSc7
byly	být	k5eAaImAgFnP
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
právě	právě	k6eAd1
nacházely	nacházet	k5eAaImAgFnP
v	v	k7c6
Telnici	Telnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
rozhodným	rozhodný	k2eAgInSc7d1
bodákovým	bodákový	k2eAgInSc7d1
útokem	útok	k1gInSc7
je	on	k3xPp3gNnSc4
vytlačily	vytlačit	k5eAaPmAgInP
z	z	k7c2
obsazených	obsazený	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
a	a	k8xC
Rusové	Rus	k1gMnPc1
ve	v	k7c6
zmatku	zmatek	k1gInSc6
prchli	prchnout	k5eAaPmAgMnP
zpět	zpět	k6eAd1
směrem	směr	k1gInSc7
k	k	k7c3
Újezdu	Újezd	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
však	však	k9
pominulo	pominout	k5eAaPmAgNnS
první	první	k4xOgNnPc1
překvapení	překvapení	k1gNnPc1
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
Francouzi	Francouz	k1gMnPc1
spojeneckými	spojenecký	k2eAgMnPc7d1
pěšáky	pěšák	k1gMnPc7
a	a	k8xC
Hessen-Homburskými	Hessen-Homburský	k2eAgMnPc7d1
husary	husar	k1gMnPc7
zatlačeni	zatlačen	k2eAgMnPc1d1
zpět	zpět	k6eAd1
do	do	k7c2
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
narychlo	narychlo	k6eAd1
opevnit	opevnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
generálporučík	generálporučík	k1gMnSc1
Dochturov	Dochturov	k1gInSc4
řadil	řadit	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
pěchotu	pěchota	k1gFnSc4
k	k	k7c3
novému	nový	k2eAgInSc3d1
útoku	útok	k1gInSc3
<g/>
,	,	kIx,
mezi	mezi	k7c7
Telnicí	Telnice	k1gFnSc7
a	a	k8xC
Sokolnicemi	sokolnice	k1gFnPc7
se	se	k3xPyFc4
ve	v	k7c6
zbytcích	zbytek	k1gInPc6
mlhy	mlha	k1gFnSc2
a	a	k8xC
dýmu	dým	k1gInSc2
z	z	k7c2
pušek	puška	k1gFnPc2
rozpoutala	rozpoutat	k5eAaPmAgFnS
nechtěná	chtěný	k2eNgFnSc1d1
přestřelka	přestřelka	k1gFnSc1
mezi	mezi	k7c7
108	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
pěším	pěší	k2eAgInSc7d1
plukem	pluk	k1gInSc7
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Tohoto	tento	k3xDgInSc2
zmatku	zmatek	k1gInSc2
využil	využít	k5eAaPmAgMnS
Dochturov	Dochturov	k1gInSc4
k	k	k7c3
útoku	útok	k1gInSc3
od	od	k7c2
Újezda	Újezdo	k1gNnSc2
a	a	k8xC
Telnici	Telnice	k1gFnSc4
znova	znova	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ustupující	ustupující	k2eAgMnPc1d1
Legrandovi	Legrandův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
spořádaně	spořádaně	k6eAd1
stáhli	stáhnout	k5eAaPmAgMnP
za	za	k7c4
Zlatý	zlatý	k2eAgInSc4d1
potok	potok	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
vytvořili	vytvořit	k5eAaPmAgMnP
novou	nový	k2eAgFnSc4d1
obrannou	obranný	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sokolnice	sokolnice	k1gFnSc1
</s>
<s>
Situace	situace	k1gFnSc1
na	na	k7c6
bojišti	bojiště	k1gNnSc6
kolem	kolem	k6eAd1
9.00	9.00	k4
hod	hod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postup	postup	k1gInSc1
ruských	ruský	k2eAgFnPc2d1
kolon	kolona	k1gFnPc2
kolem	kolem	k7c2
Prateckého	pratecký	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
<g/>
,	,	kIx,
útok	útok	k1gInSc1
dvou	dva	k4xCgFnPc2
Soultových	Soultův	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
<g/>
,	,	kIx,
přesuny	přesun	k1gInPc1
Lichtenštejnovy	Lichtenštejnův	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
příchod	příchod	k1gInSc4
ruské	ruský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
a	a	k8xC
útok	útok	k1gInSc1
Bagrationa	Bagration	k1gMnSc2
u	u	k7c2
olomoucké	olomoucký	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
</s>
<s>
Před	před	k7c7
devátou	devátý	k4xOgFnSc7
hodinou	hodina	k1gFnSc7
k	k	k7c3
Sokolnicím	sokolnice	k1gFnPc3
konečně	konečně	k6eAd1
dorazila	dorazit	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
generála	generál	k1gMnSc2
Langeronona	Langeronon	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
bez	bez	k7c2
pozdržené	pozdržený	k2eAgFnSc2d1
Kamenského	Kamenského	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
momentálně	momentálně	k6eAd1
disponovala	disponovat	k5eAaBmAgFnS
zhruba	zhruba	k6eAd1
7000	#num#	k4
vojáky	voják	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
ruský	ruský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
nespatřil	spatřit	k5eNaPmAgMnS
větší	veliký	k2eAgFnPc4d2
protivníkovy	protivníkův	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
nesnažil	snažit	k5eNaImAgMnS
se	se	k3xPyFc4
útok	útok	k1gInSc4
uspěchat	uspěchat	k5eAaPmF
a	a	k8xC
vyčkal	vyčkat	k5eAaPmAgMnS
<g/>
,	,	kIx,
až	až	k8xS
se	se	k3xPyFc4
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
ukáže	ukázat	k5eAaPmIp3nS
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Przybyszewského	Przybyszewského	k2eAgFnSc1d1
kolona	kolona	k1gFnSc1
s	s	k7c7
10	#num#	k4
000	#num#	k4
muži	muž	k1gMnPc7
a	a	k8xC
30	#num#	k4
děly	dělo	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
z	z	k7c2
Prateckého	pratecký	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
sestupovala	sestupovat	k5eAaImAgFnS
chvíli	chvíle	k1gFnSc4
za	za	k7c7
jeho	jeho	k3xOp3gFnPc7
jednotkami	jednotka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
nechal	nechat	k5eAaPmAgInS
své	svůj	k3xOyFgInPc4
oddíly	oddíl	k1gInPc4
se	se	k3xPyFc4
ve	v	k7c6
dvou	dva	k4xCgInPc6
sledech	sled	k1gInPc6
rozvinout	rozvinout	k5eAaPmF
do	do	k7c2
linie	linie	k1gFnSc2
a	a	k8xC
rozmístil	rozmístit	k5eAaPmAgMnS
třicet	třicet	k4xCc4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
ihned	ihned	k6eAd1
zahájila	zahájit	k5eAaPmAgFnS
palbu	palba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Jakmile	jakmile	k8xS
začalo	začít	k5eAaPmAgNnS
čelo	čelo	k1gNnSc4
Przybyszewského	Przybyszewského	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
zaujímat	zaujímat	k5eAaImF
své	svůj	k3xOyFgFnPc4
pozice	pozice	k1gFnPc4
<g/>
,	,	kIx,
vyrazil	vyrazit	k5eAaPmAgMnS
Langeron	Langeron	k1gInSc4
obsadit	obsadit	k5eAaPmF
Sokolnice	sokolnice	k1gFnPc4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
posléze	posléze	k6eAd1
udeřila	udeřit	k5eAaPmAgFnS
do	do	k7c2
prostoru	prostor	k1gInSc2
u	u	k7c2
sokolnického	sokolnický	k2eAgInSc2d1
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
zdejší	zdejší	k2eAgInSc1d1
sektor	sektor	k1gInSc1
však	však	k9
bránil	bránit	k5eAaImAgInS
pouze	pouze	k6eAd1
26	#num#	k4
<g/>
.	.	kIx.
lehký	lehký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
přinuceni	přinucen	k2eAgMnPc1d1
ustoupit	ustoupit	k5eAaPmF
až	až	k9
na	na	k7c4
svahy	svah	k1gInPc4
za	za	k7c7
západním	západní	k2eAgInSc7d1
okrajem	okraj	k1gInSc7
Sokolnic	sokolnice	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
odehrály	odehrát	k5eAaPmAgFnP
první	první	k4xOgFnPc1
vážnější	vážní	k2eAgFnPc1d2
srážky	srážka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
i	i	k9
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
francouzský	francouzský	k2eAgInSc1d1
odpor	odpor	k1gInSc1
brzy	brzy	k6eAd1
zlomen	zlomen	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
10.00	10.00	k4
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
prorazí	prorazit	k5eAaPmIp3nP
skrz	skrz	k7c4
protivníkovu	protivníkův	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
však	však	k9
na	na	k7c4
bojiště	bojiště	k1gNnSc4
dorazila	dorazit	k5eAaPmAgFnS
pěší	pěší	k2eAgFnSc1d1
Friantova	Friantův	k2eAgFnSc1d1
divize	divize	k1gFnSc1
náležející	náležející	k2eAgFnSc1d1
k	k	k7c3
III	III	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
maršála	maršál	k1gMnSc4
Davouta	Davout	k1gMnSc4
<g/>
.	.	kIx.
48	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
111	#num#	k4
<g/>
.	.	kIx.
pluk	pluk	k1gInSc1
přešly	přejít	k5eAaPmAgInP
do	do	k7c2
okamžitého	okamžitý	k2eAgInSc2d1
útoku	útok	k1gInSc2
a	a	k8xC
za	za	k7c4
využití	využití	k1gNnSc4
momentu	moment	k1gInSc2
překvapení	překvapení	k1gNnPc2
vytlačily	vytlačit	k5eAaPmAgFnP
Langeronovy	Langeronův	k2eAgFnPc1d1
a	a	k8xC
Przybyszewského	Przybyszewského	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
ze	z	k7c2
svahů	svah	k1gInPc2
až	až	k9
do	do	k7c2
Sokolnic	sokolnice	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Začal	začít	k5eAaPmAgInS
tuhý	tuhý	k2eAgInSc4d1
boj	boj	k1gInSc4
o	o	k7c6
vesnici	vesnice	k1gFnSc6
vedený	vedený	k2eAgInSc1d1
dům	dům	k1gInSc1
od	od	k7c2
domu	dům	k1gInSc2
v	v	k7c6
němž	jenž	k3xRgMnSc6
však	však	k9
silnější	silný	k2eAgFnPc1d2
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
začaly	začít	k5eAaPmAgFnP
získávat	získávat	k5eAaImF
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
tři	tři	k4xCgFnPc4
čtvrtě	čtvrt	k1gFnPc4
hodinovém	hodinový	k2eAgNnSc6d1
zápolení	zápolení	k1gNnSc6
se	se	k3xPyFc4
k	k	k7c3
francouzskému	francouzský	k2eAgInSc3d1
útoku	útok	k1gInSc3
připojily	připojit	k5eAaPmAgFnP
i	i	k9
15	#num#	k4
<g/>
.	.	kIx.
lehký	lehký	k2eAgInSc1d1
a	a	k8xC
33	#num#	k4
<g/>
.	.	kIx.
řadový	řadový	k2eAgInSc1d1
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
vesnice	vesnice	k1gFnSc2
pak	pak	k6eAd1
trvaly	trvat	k5eAaImAgFnP
celé	celý	k2eAgNnSc4d1
dopoledne	dopoledne	k1gNnSc4
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
po	po	k7c6
poledni	poledne	k1gNnSc6
se	se	k3xPyFc4
francouzská	francouzský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
<g/>
,	,	kIx,
krytá	krytý	k2eAgFnSc1d1
Margaronovým	Margaronův	k2eAgNnSc7d1
a	a	k8xC
Bourcierovým	Bourcierův	k2eAgNnSc7d1
jezdectvem	jezdectvo	k1gNnSc7
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
přeskupení	přeskupení	k1gNnSc3
sil	síla	k1gFnPc2
stáhla	stáhnout	k5eAaPmAgFnS
směrem	směr	k1gInSc7
k	k	k7c3
Otmarovu	Otmarův	k2eAgNnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusům	Rus	k1gMnPc3
zůstaly	zůstat	k5eAaPmAgFnP
v	v	k7c6
rukou	ruka	k1gFnPc6
Telnice	Telnice	k1gFnSc2
i	i	k8xC
Sokolnice	sokolnice	k1gFnSc2
<g/>
,	,	kIx,
Davout	Davout	k1gMnSc1
a	a	k8xC
Legrand	legranda	k1gFnPc2
však	však	k9
splnili	splnit	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
úkol	úkol	k1gInSc4
přesně	přesně	k6eAd1
podle	podle	k7c2
Napoleonova	Napoleonův	k2eAgNnSc2d1
očekávání	očekávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buxhöwdenovy	Buxhöwdenův	k2eAgFnPc4d1
kolony	kolona	k1gFnPc4
zůstaly	zůstat	k5eAaPmAgFnP
v	v	k7c6
prostoru	prostor	k1gInSc6
podél	podél	k7c2
Zlatého	zlatý	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
umožnilo	umožnit	k5eAaPmAgNnS
požadované	požadovaný	k2eAgNnSc1d1
zakončení	zakončení	k1gNnSc1
obchvatného	obchvatný	k2eAgInSc2d1
manévru	manévr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pratecké	pratecký	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
</s>
<s>
Louis	Louis	k1gMnSc1
Francois	Francois	k1gMnSc1
Lejeune	Lejeun	k1gInSc5
<g/>
:	:	kIx,
Napoleon	napoleon	k1gInSc1
pod	pod	k7c7
Santonem	Santon	k1gInSc7
</s>
<s>
Kolem	kolem	k7c2
osmé	osmý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c4
Napoleonův	Napoleonův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
daly	dát	k5eAaPmAgFnP
do	do	k7c2
pohybu	pohyb	k1gInSc2
Vandammova	Vandammův	k2eAgFnSc1d1
a	a	k8xC
Saint-Hilairova	Saint-Hilairův	k2eAgFnSc1d1
divize	divize	k1gFnSc1
IV	IV	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
maršála	maršál	k1gMnSc4
Soulta	Soult	k1gMnSc4
<g/>
,	,	kIx,
doposud	doposud	k6eAd1
skryté	skrytý	k2eAgInPc4d1
před	před	k7c7
pohledy	pohled	k1gInPc7
spojenců	spojenec	k1gMnPc2
mlhou	mlha	k1gFnSc7
v	v	k7c6
údolí	údolí	k1gNnSc6
Velatického	Velatický	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Saint-Hilairova	Saint-Hilairov	k1gInSc2
divize	divize	k1gFnSc2
(	(	kIx(
<g/>
brigády	brigáda	k1gFnSc2
generálů	generál	k1gMnPc2
Moranda	Morando	k1gNnSc2
<g/>
,	,	kIx,
Thiébaulta	Thiébaulto	k1gNnSc2
a	a	k8xC
Varého	Varé	k1gNnSc2
<g/>
)	)	kIx)
vyrazila	vyrazit	k5eAaPmAgFnS
od	od	k7c2
Ponětovic	Ponětovice	k1gFnPc2
jako	jako	k9
první	první	k4xOgFnSc1
a	a	k8xC
jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
byla	být	k5eAaImAgFnS
kóta	kóta	k1gFnSc1
324	#num#	k4
Pratecký	pratecký	k2eAgInSc4d1
vrch	vrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vandammova	Vandammův	k2eAgFnSc1d1
divize	divize	k1gFnSc1
se	se	k3xPyFc4
pohnula	pohnout	k5eAaPmAgFnS
s	s	k7c7
menším	malý	k2eAgNnSc7d2
zpožděním	zpoždění	k1gNnSc7
a	a	k8xC
jejím	její	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
obsadit	obsadit	k5eAaPmF
kótu	kóta	k1gFnSc4
298	#num#	k4
Staré	Staré	k2eAgInPc7d1
vinohrady	vinohrad	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Napoleon	Napoleon	k1gMnSc1
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
této	tento	k3xDgFnSc6
chvíli	chvíle	k1gFnSc6
budou	být	k5eAaImBp3nP
výšiny	výšina	k1gFnPc4
téměř	téměř	k6eAd1
zcela	zcela	k6eAd1
vyklizeny	vyklizen	k2eAgFnPc1d1
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
informoval	informovat	k5eAaBmAgInS
i	i	k9
své	svůj	k3xOyFgMnPc4
podřízené	podřízený	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
proto	proto	k8xC
neočekávali	očekávat	k5eNaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
narazí	narazit	k5eAaPmIp3nS
na	na	k7c4
silnější	silný	k2eAgInSc4d2
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c7
terénním	terénní	k2eAgInSc7d1
zlomem	zlom	k1gInSc7
Starých	Starých	k2eAgInPc2d1
vinohradů	vinohrad	k1gInPc2
však	však	k9
stále	stále	k6eAd1
zůstávala	zůstávat	k5eAaImAgFnS
4	#num#	k4
<g/>
.	.	kIx.
smíšená	smíšený	k2eAgFnSc1d1
kolona	kolona	k1gFnSc1
generála	generál	k1gMnSc2
Miloradoviče	Miloradovič	k1gMnSc2
a	a	k8xC
podmaršálka	podmaršálek	k1gMnSc2
Kolovrata	Kolovrat	k1gMnSc2
pozdržená	pozdržený	k2eAgFnSc1d1
hrabětem	hrabě	k1gMnSc7
Kutuzovem	Kutuzov	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
prvnímu	první	k4xOgInSc3
ozbrojenému	ozbrojený	k2eAgInSc3d1
střetu	střet	k1gInSc3
v	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
bojiště	bojiště	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
kolem	kolem	k7c2
deváté	devátý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Prace	Prace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thiébaultově	Thiébaultův	k2eAgFnSc6d1
brigádě	brigáda	k1gFnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
rozprášit	rozprášit	k5eAaPmF
vojáky	voják	k1gMnPc4
tří	tři	k4xCgInPc2
předsunutých	předsunutý	k2eAgInPc2d1
ruských	ruský	k2eAgInPc2d1
praporů	prapor	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
svým	svůj	k3xOyFgInSc7
panickým	panický	k2eAgInSc7d1
útěkem	útěk	k1gInSc7
upozornili	upozornit	k5eAaPmAgMnP
na	na	k7c4
francouzskou	francouzský	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
generála	generál	k1gMnSc2
Kutuzova	Kutuzův	k2eAgMnSc2d1
a	a	k8xC
ruského	ruský	k2eAgMnSc2d1
cara	car	k1gMnSc2
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
právě	právě	k6eAd1
u	u	k7c2
4	#num#	k4
<g/>
.	.	kIx.
spojenecké	spojenecký	k2eAgFnSc2d1
kolony	kolona	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
84	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
nečekaný	čekaný	k2eNgInSc4d1
útok	útok	k1gInSc4
hrabě	hrabě	k1gMnSc1
Kutuzov	Kutuzov	k1gInSc4
rozkázal	rozkázat	k5eAaPmAgMnS
Kolowratovým	Kolowratův	k2eAgMnSc7d1
Rakušanům	Rakušan	k1gMnPc3
obsadit	obsadit	k5eAaPmF
vrchol	vrchol	k1gInSc4
Prateckého	pratecký	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
mířil	mířit	k5eAaImAgMnS
generál	generál	k1gMnSc1
Saint-Hilaire	Saint-Hilair	k1gMnSc5
<g/>
,	,	kIx,
a	a	k8xC
Miloradovičovi	Miloradovičův	k2eAgMnPc1d1
Rusové	Rus	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
povel	povel	k1gInSc4
dobýt	dobýt	k5eAaPmF
Prace	Prace	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
obranu	obrana	k1gFnSc4
převzala	převzít	k5eAaPmAgFnS
francouzská	francouzský	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
generála	generál	k1gMnSc2
Varé	Varý	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Krátce	krátce	k6eAd1
po	po	k7c6
deváté	devátý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
vyrazil	vyrazit	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Miloradovič	Miloradovič	k1gMnSc1
do	do	k7c2
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžké	těžký	k2eAgInPc1d1
boje	boj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
rozhořely	rozhořet	k5eAaPmAgInP
již	již	k6eAd1
před	před	k7c7
vesnicí	vesnice	k1gFnSc7
<g/>
,	,	kIx,
trvaly	trvat	k5eAaImAgInP
zhruba	zhruba	k6eAd1
jednu	jeden	k4xCgFnSc4
hodinu	hodina	k1gFnSc4
a	a	k8xC
průběžně	průběžně	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
nich	on	k3xPp3gInPc2
zapojili	zapojit	k5eAaPmAgMnP
vojáci	voják	k1gMnPc1
celé	celý	k2eAgFnSc2d1
Vandammovy	Vandammův	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc4
nakonec	nakonec	k6eAd1
také	také	k9
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
díky	díky	k7c3
taktické	taktický	k2eAgFnSc3d1
převaze	převaha	k1gFnSc3
protivníka	protivník	k1gMnSc2
odrazili	odrazit	k5eAaPmAgMnP
a	a	k8xC
způsobili	způsobit	k5eAaPmAgMnP
mu	on	k3xPp3gInSc3
vysoké	vysoký	k2eAgFnPc4d1
ztráty	ztráta	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Zcela	zcela	k6eAd1
rozvrácené	rozvrácený	k2eAgInPc1d1
Miloradovičovy	Miloradovičův	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
ve	v	k7c6
zmatku	zmatek	k1gInSc6
prchaly	prchat	k5eAaImAgInP
za	za	k7c4
hřeben	hřeben	k1gInSc4
Prateckého	pratecký	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
již	již	k6eAd1
řadilo	řadit	k5eAaImAgNnS
5000	#num#	k4
mužů	muž	k1gMnPc2
podmaršálka	podmaršálek	k1gMnSc2
Kolowrata	Kolowrat	k1gMnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
šesti	šest	k4xCc7
prapory	prapor	k1gInPc7
IR	Ir	k1gMnSc1
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salzburg	Salzburg	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Rakušané	Rakušan	k1gMnPc1
ovládali	ovládat	k5eAaImAgMnP
hřeben	hřeben	k1gInSc4
návrší	návrší	k1gNnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
k	k	k7c3
razantnější	razantní	k2eAgFnSc3d2
obraně	obrana	k1gFnSc3
potřebovali	potřebovat	k5eAaImAgMnP
výraznější	výrazný	k2eAgFnSc4d2
podporu	podpora	k1gFnSc4
generála	generál	k1gMnSc2
Miloradoviče	Miloradovič	k1gMnSc2
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
však	však	k9
na	na	k7c4
carův	carův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
ve	v	k7c6
zmatku	zmatek	k1gInSc6
ustupoval	ustupovat	k5eAaImAgInS
k	k	k7c3
jižnímu	jižní	k2eAgInSc3d1
svahu	svah	k1gInSc2
výšin	výšina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Generál	generál	k1gMnSc1
Vandamme	Vandamme	k1gMnSc1
proti	proti	k7c3
Rakušanům	Rakušan	k1gMnPc3
nasadil	nasadit	k5eAaPmAgMnS
Féreyovu	Féreyův	k2eAgFnSc4d1
brigádu	brigáda	k1gFnSc4
spolu	spolu	k6eAd1
s	s	k7c7
55	#num#	k4
<g/>
.	.	kIx.
řadovým	řadový	k2eAgInSc7d1
plukem	pluk	k1gInSc7
Varého	Varého	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
zprava	zprava	k6eAd1
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
lehkým	lehký	k2eAgInSc7d1
plukem	pluk	k1gInSc7
Schinnerovy	Schinnerův	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
zleva	zleva	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
teprve	teprve	k6eAd1
po	po	k7c6
tvrdém	tvrdé	k1gNnSc6
<g/>
,	,	kIx,
snad	snad	k9
až	až	k9
hodinu	hodina	k1gFnSc4
trvajícím	trvající	k2eAgInSc6d1
boji	boj	k1gInSc6
(	(	kIx(
<g/>
zasáhl	zasáhnout	k5eAaPmAgMnS
do	do	k7c2
něj	on	k3xPp3gInSc2
i	i	k8xC
zbytek	zbytek	k1gInSc4
Varého	Varého	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
závěru	závěr	k1gInSc6
vedeném	vedený	k2eAgInSc6d1
na	na	k7c4
bodáky	bodák	k1gInPc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
Rakušané	Rakušan	k1gMnPc1
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
postavení	postavení	k1gNnSc2
vytlačeni	vytlačit	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Zhruba	zhruba	k6eAd1
kolem	kolem	k7c2
jedenácté	jedenáctý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
vojáci	voják	k1gMnPc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
I.	I.	kA
ustoupili	ustoupit	k5eAaPmAgMnP
za	za	k7c7
Miloradovičovými	Miloradovičův	k2eAgFnPc7d1
jednotkami	jednotka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbloudilou	zbloudilý	k2eAgFnSc7d1
střelou	střela	k1gFnSc7
z	z	k7c2
těchto	tento	k3xDgInPc2
bojů	boj	k1gInPc2
byl	být	k5eAaImAgInS
na	na	k7c6
tváři	tvář	k1gFnSc6
zraněn	zranit	k5eAaPmNgMnS
i	i	k9
generál	generál	k1gMnSc1
Kutuzov	Kutuzov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
Caton	Caton	k1gMnSc1
Woodville	Woodville	k1gFnSc1
<g/>
:	:	kIx,
Napoleon	napoleon	k1gInSc1
u	u	k7c2
břehu	břeh	k1gInSc2
Litavy	Litava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zadní	zadní	k2eAgFnSc6d1
části	část	k1gFnSc6
plátna	plátno	k1gNnSc2
je	být	k5eAaImIp3nS
zobrazena	zobrazen	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
sv.	sv.	kA
Antonína	Antonín	k1gMnSc2
Paduánského	paduánský	k2eAgMnSc2d1
stojící	stojící	k2eAgInSc4d1
nad	nad	k7c7
Újezdem	Újezd	k1gInSc7
</s>
<s>
Jižně	jižně	k6eAd1
od	od	k7c2
Vandamma	Vandammum	k1gNnSc2
Thiébaultova	Thiébaultův	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
ze	z	k7c2
Saint-Hilairovy	Saint-Hilairův	k2eAgFnSc2d1
divize	divize	k1gFnSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
vrcholu	vrchol	k1gInSc6
Prateckého	pratecký	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
kolem	kolem	k7c2
desáté	desátá	k1gFnSc2
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
očekávala	očekávat	k5eAaImAgFnS
spojení	spojení	k1gNnSc4
s	s	k7c7
brigádu	brigáda	k1gFnSc4
generála	generál	k1gMnSc2
Moranda	Morando	k1gNnSc2
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
však	však	k9
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
křížku	křížek	k1gInSc2
s	s	k7c7
opožděnou	opožděný	k2eAgFnSc7d1
ruskou	ruský	k2eAgFnSc7d1
brigádou	brigáda	k1gFnSc7
generála	generál	k1gMnSc2
Kamenského	Kamenský	k1gMnSc2
<g/>
,	,	kIx,
patřící	patřící	k2eAgFnSc1d1
k	k	k7c3
2	#num#	k4
<g/>
.	.	kIx.
spojenecké	spojenecký	k2eAgFnSc3d1
koloně	kolona	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamenského	Kamenského	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
Langeronův	Langeronův	k2eAgInSc4d1
zadní	zadní	k2eAgInSc4d1
voj	voj	k1gInSc4
a	a	k8xC
zkušený	zkušený	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
<g/>
,	,	kIx,
jakým	jaký	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
ruský	ruský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
byl	být	k5eAaImAgMnS
<g/>
,	,	kIx,
rychle	rychle	k6eAd1
vyhodnotil	vyhodnotit	k5eAaPmAgMnS
nebezpečí	nebezpečí	k1gNnSc4
hrozící	hrozící	k2eAgFnSc2d1
vlastním	vlastnit	k5eAaImIp1nS
jednotkám	jednotka	k1gFnPc3
u	u	k7c2
Sokolnic	sokolnice	k1gFnPc2
(	(	kIx(
<g/>
o	o	k7c6
masivním	masivní	k2eAgInSc6d1
francouzském	francouzský	k2eAgInSc6d1
útoku	útok	k1gInSc6
ihned	ihned	k6eAd1
informoval	informovat	k5eAaBmAgMnS
generála	generál	k1gMnSc4
Langerona	Langeron	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
před	před	k7c7
desátou	desátá	k1gFnSc7
předal	předat	k5eAaPmAgMnS
velení	velení	k1gNnSc4
u	u	k7c2
Sokolnic	sokolnice	k1gFnPc2
generálovi	generál	k1gMnSc3
Olsufjevovi	Olsufjeva	k1gMnSc3
a	a	k8xC
přijel	přijet	k5eAaPmAgInS
se	se	k3xPyFc4
přesvědčit	přesvědčit	k5eAaPmF
osobně	osobně	k6eAd1
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Křenovic	Křenovice	k1gFnPc2
se	se	k3xPyFc4
k	k	k7c3
Morandovi	Morand	k1gMnSc3
navíc	navíc	k6eAd1
blížilo	blížit	k5eAaImAgNnS
patnáct	patnáct	k4xCc1
rakouských	rakouský	k2eAgFnPc2d1
(	(	kIx(
<g/>
Kolowratových	Kolowratův	k2eAgInPc2d1
<g/>
)	)	kIx)
praporů	prapor	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
jejichž	jejichž	k3xOyRp3gInSc6
čele	čelo	k1gNnSc6
pochodovala	pochodovat	k5eAaImAgFnS
Jirčíkova	Jirčíkův	k2eAgFnSc1d1
brigáda	brigáda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Kritickou	kritický	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
vyřešil	vyřešit	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Saint-Hilaire	Saint-Hilair	k1gInSc5
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
ohrožený	ohrožený	k2eAgInSc4d1
úsek	úsek	k1gInSc4
přesunul	přesunout	k5eAaPmAgMnS
36	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
část	část	k1gFnSc1
14	#num#	k4
<g/>
.	.	kIx.
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
podpořilo	podpořit	k5eAaPmAgNnS
šest	šest	k4xCc1
dvacetiliberních	dvacetiliberních	k?
děl	dělo	k1gNnPc2
vyslaných	vyslaný	k2eAgMnPc2d1
sem	sem	k6eAd1
údajně	údajně	k6eAd1
Napoleonem	napoleon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmito	tento	k3xDgFnPc7
jednotkami	jednotka	k1gFnPc7
zacelil	zacelit	k5eAaPmAgMnS
linii	linie	k1gFnSc4
mezi	mezi	k7c7
Starými	starý	k2eAgInPc7d1
vinohrady	vinohrad	k1gInPc7
a	a	k8xC
Prateckým	pratecký	k2eAgNnSc7d1
návrším	návrší	k1gNnSc7
a	a	k8xC
Morandovy	Morandův	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
v	v	k7c6
téměř	téměř	k6eAd1
pravém	pravý	k2eAgInSc6d1
úhlu	úhel	k1gInSc6
kryly	krýt	k5eAaImAgInP
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
bráněného	bráněný	k2eAgNnSc2d1
návrší	návrší	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Saint-Hilairově	Saint-Hilairův	k2eAgFnSc3d1
divizi	divize	k1gFnSc3
stálo	stát	k5eAaImAgNnS
55	#num#	k4
Kamenského	Kamenského	k2eAgInPc2d1
praporů	prapor	k1gInPc2
spolu	spolu	k6eAd1
s	s	k7c7
3000	#num#	k4
<g/>
–	–	k?
<g/>
4000	#num#	k4
muži	muž	k1gMnPc7
Jirčíkovy	Jirčíkův	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastávající	nastávající	k2eAgInSc1d1
útok	útok	k1gInSc1
trval	trvat	k5eAaImAgInS
dvacet	dvacet	k4xCc4
minut	minuta	k1gFnPc2
a	a	k8xC
Rakušané	Rakušan	k1gMnPc1
nakrátko	nakrátko	k6eAd1
ovládli	ovládnout	k5eAaPmAgMnP
Pratecký	pratecký	k2eAgInSc4d1
vrch	vrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
tvrdým	tvrdý	k2eAgInSc7d1
protiútokem	protiútok	k1gInSc7
však	však	k9
byli	být	k5eAaImAgMnP
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
pozice	pozice	k1gFnSc1
vytlačeni	vytlačen	k2eAgMnPc1d1
a	a	k8xC
přinuceni	přinucen	k2eAgMnPc1d1
ustoupit	ustoupit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Jen	jen	k9
část	část	k1gFnSc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
jednotek	jednotka	k1gFnPc2
se	se	k3xPyFc4
připojila	připojit	k5eAaPmAgFnS
ke	k	k7c3
Kamenského	Kamenského	k2eAgFnSc3d1
brigádě	brigáda	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
níž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
na	na	k7c4
rozkaz	rozkaz	k1gInSc4
generálmajora	generálmajor	k1gMnSc2
Volkonského	Volkonský	k2eAgNnSc2d1
ještě	ještě	k6eAd1
jednou	jednou	k6eAd1
pokusili	pokusit	k5eAaPmAgMnP
ztracené	ztracený	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
získat	získat	k5eAaPmF
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
akce	akce	k1gFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
již	již	k6eAd1
předem	předem	k6eAd1
odsouzena	odsouzet	k5eAaImNgFnS,k5eAaPmNgFnS
k	k	k7c3
nezdaru	nezdar	k1gInSc3
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
byl	být	k5eAaImAgMnS
při	při	k7c6
ní	on	k3xPp3gFnSc6
smrtelně	smrtelně	k6eAd1
zraněn	zraněn	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Jirčík	Jirčík	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Hrabě	Hrabě	k1gMnSc1
Kutuzov	Kutuzov	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
právě	právě	k6eAd1
nacházel	nacházet	k5eAaImAgMnS
v	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
bojiště	bojiště	k1gNnSc4
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgInS
generálu	generál	k1gMnSc3
Kamenskému	Kamenský	k1gMnSc3
rozkaz	rozkaz	k1gInSc4
k	k	k7c3
ústupu	ústup	k1gInSc3
do	do	k7c2
údolí	údolí	k1gNnSc2
směrem	směr	k1gInSc7
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c4
Hostěrádky	Hostěrádek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Rakušané	Rakušan	k1gMnPc1
v	v	k7c6
tomto	tento	k3xDgNnSc6
střetnutí	střetnutí	k1gNnSc6
ztratili	ztratit	k5eAaPmAgMnP
2388	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
18	#num#	k4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
Saint-Hilairova	Saint-Hilairův	k2eAgFnSc1d1
divize	divize	k1gFnSc1
měla	mít	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
1800	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
raněných	raněný	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
kolem	kolem	k7c2
jedenácté	jedenáctý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
ovládli	ovládnout	k5eAaPmAgMnP
jak	jak	k8xS,k8xC
Staré	Staré	k2eAgInPc1d1
vinohrady	vinohrad	k1gInPc1
<g/>
,	,	kIx,
tak	tak	k9
Pratecký	pratecký	k2eAgInSc4d1
vrch	vrch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Mušketýrské	mušketýrský	k2eAgInPc4d1
pluky	pluk	k1gInPc4
Kurský	kurský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
prapory	prapor	k1gInPc7
<g/>
)	)	kIx)
a	a	k8xC
Podolský	podolský	k2eAgMnSc1d1
(	(	kIx(
<g/>
tvořící	tvořící	k2eAgFnSc1d1
část	část	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Przybyszewského	Przybyszewského	k2eAgFnPc4d1
kolony	kolona	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
z	z	k7c2
Langeronovy	Langeronův	k2eAgFnSc2d1
iniciativy	iniciativa	k1gFnSc2
vyslány	vyslat	k5eAaPmNgFnP
spojencům	spojenec	k1gMnPc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
přišly	přijít	k5eAaPmAgFnP
již	již	k6eAd1
pozdě	pozdě	k6eAd1
a	a	k8xC
byly	být	k5eAaImAgInP
s	s	k7c7
velkými	velký	k2eAgFnPc7d1
ztrátami	ztráta	k1gFnPc7
vrženy	vržen	k2eAgFnPc1d1
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Generál	generál	k1gMnSc1
Thiébault	Thiébault	k1gMnSc1
o	o	k7c6
boji	boj	k1gInSc6
na	na	k7c6
Prateckém	pratecký	k2eAgNnSc6d1
návrší	návrší	k1gNnSc6
podal	podat	k5eAaPmAgInS
zprávu	zpráva	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Až	až	k9
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
hodiny	hodina	k1gFnSc2
bitvy	bitva	k1gFnSc2
jsme	být	k5eAaImIp1nP
nebrali	brát	k5eNaImAgMnP
zajatce	zajatec	k1gMnSc4
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
neriskovali	riskovat	k5eNaBmAgMnP
<g/>
;	;	kIx,
některý	některý	k3yIgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
nemusel	muset	k5eNaImAgMnS
zastavit	zastavit	k5eAaPmF
před	před	k7c7
ničím	ničí	k3xOyNgNnSc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
v	v	k7c6
našem	náš	k3xOp1gInSc6
týle	týl	k1gInSc6
nezůstal	zůstat	k5eNaPmAgMnS
jediný	jediný	k2eAgMnSc1d1
živý	živý	k2eAgMnSc1d1
nepřítel	nepřítel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
jedenácté	jedenáctý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
opustil	opustit	k5eAaPmAgInS
Napoleon	napoleon	k1gInSc1
Žuráň	Žuráň	k1gFnSc4
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
gardou	garda	k1gFnSc7
a	a	k8xC
částí	část	k1gFnSc7
záloh	záloha	k1gFnPc2
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgInS
ke	k	k7c3
Starým	starý	k2eAgInPc3d1
vinohradům	vinohrad	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
sebe	sebe	k3xPyFc4
si	se	k3xPyFc3
ponechal	ponechat	k5eAaPmAgMnS
i	i	k9
šest	šest	k4xCc4
praporů	prapor	k1gInPc2
Oudinotových	Oudinotův	k2eAgMnPc2d1
granátníků	granátník	k1gMnPc2
a	a	k8xC
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
prapory	prapor	k1gInPc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
generála	generál	k1gMnSc2
Duroca	Durocus	k1gMnSc2
vyslal	vyslat	k5eAaPmAgMnS
k	k	k7c3
Tuřanům	Tuřan	k1gInPc3
k	k	k7c3
podpoře	podpora	k1gFnSc3
Davoutova	Davoutův	k2eAgFnSc1d1
III	III	kA
<g/>
.	.	kIx.
sboru	sbor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boj	boj	k1gInSc1
u	u	k7c2
olomoucké	olomoucký	k2eAgFnSc2d1
silnice	silnice	k1gFnSc2
</s>
<s>
Vrch	vrch	k1gInSc1
Santon	Santon	k1gInSc1
s	s	k7c7
kaplí	kaple	k1gFnSc7
</s>
<s>
Na	na	k7c6
nejzazším	zadní	k2eAgNnSc6d3
pravém	pravý	k2eAgNnSc6d1
spojeneckém	spojenecký	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
stála	stát	k5eAaImAgFnS
v	v	k7c6
brzkých	brzký	k2eAgFnPc6d1
ranních	ranní	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
pouze	pouze	k6eAd1
brigáda	brigáda	k1gFnSc1
generálmajora	generálmajor	k1gMnSc2
Uvarova	Uvarův	k2eAgMnSc2d1
z	z	k7c2
Essenovy	Essenův	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
dislokovaná	dislokovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
čelní	čelní	k2eAgNnSc1d1
zajištění	zajištění	k1gNnSc1
Bagrationova	Bagrationův	k2eAgInSc2d1
předvoje	předvoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
8.30	8.30	k4
se	se	k3xPyFc4
na	na	k7c6
jejím	její	k3xOp3gInSc6
levém	levý	k2eAgInSc6d1
boku	bok	k1gInSc6
objevila	objevit	k5eAaPmAgFnS
rakouská	rakouský	k2eAgFnSc1d1
část	část	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
kolony	kolona	k1gFnPc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
těžcí	těžký	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
pod	pod	k7c7
velením	velení	k1gNnSc7
podmaršálka	podmaršálek	k1gMnSc2
hraběte	hrabě	k1gMnSc2
Hohenloheho	Hohenlohe	k1gMnSc2
(	(	kIx(
<g/>
dorazil	dorazit	k5eAaPmAgMnS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
i	i	k9
velitel	velitel	k1gMnSc1
kolony	kolona	k1gFnSc2
kníže	kníže	k1gMnSc1
Lichtenstein	Lichtenstein	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
asi	asi	k9
v	v	k7c6
9.00	9.00	k4
se	se	k3xPyFc4
mezi	mezi	k7c4
obě	dva	k4xCgNnPc4
tyto	tento	k3xDgFnPc4
jednotky	jednotka	k1gFnPc4
vřadila	vřadit	k5eAaPmAgFnS
brigáda	brigáda	k1gFnSc1
generálmajora	generálmajor	k1gMnSc2
Šepeljeva	Šepeljev	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
tvořila	tvořit	k5eAaImAgFnS
druhou	druhý	k4xOgFnSc4
část	část	k1gFnSc4
Essenových	Essenův	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
přítomných	přítomný	k2eAgFnPc2d1
spojeneckých	spojenecký	k2eAgFnPc2d1
formací	formace	k1gFnPc2
bylo	být	k5eAaImAgNnS
krýt	krýt	k5eAaImF
levé	levý	k2eAgNnSc4d1
Bagrationovo	Bagrationův	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
,	,	kIx,
ruský	ruský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
v	v	k7c4
tento	tento	k3xDgInSc4
moment	moment	k1gInSc4
nacházel	nacházet	k5eAaImAgMnS
zhruba	zhruba	k6eAd1
3	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanedlouho	zanedlouho	k6eAd1
po	po	k7c6
jezdectvu	jezdectvo	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
začala	začít	k5eAaPmAgFnS
objevovat	objevovat	k5eAaImF
první	první	k4xOgFnSc1
část	část	k1gFnSc1
imperátorské	imperátorský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
carova	carův	k2eAgMnSc2d1
bratra	bratr	k1gMnSc2
velkoknížete	velkokníže	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
Pavloviče	Pavlovič	k1gMnSc2
(	(	kIx(
<g/>
celkově	celkově	k6eAd1
10	#num#	k4
600	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zůstala	zůstat	k5eAaPmAgFnS
stát	stát	k5eAaPmF,k5eAaImF
na	na	k7c6
vyvýšenině	vyvýšenina	k1gFnSc6
mezi	mezi	k7c7
Holubicemi	holubice	k1gFnPc7
a	a	k8xC
Blažovicemi	Blažovice	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
pozicích	pozice	k1gFnPc6
před	před	k7c7
obcí	obec	k1gFnSc7
Tvarožná	Tvarožný	k2eAgFnSc1d1
a	a	k8xC
západně	západně	k6eAd1
od	od	k7c2
Blažovic	Blažovice	k1gFnPc2
stál	stát	k5eAaImAgInS
proti	proti	k7c3
spojencům	spojenec	k1gMnPc3
V.	V.	kA
sbor	sbor	k1gInSc1
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
maršála	maršál	k1gMnSc2
Lannese	Lannese	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
13	#num#	k4
230	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celá	k1gFnSc6
jeho	jeho	k3xOp3gNnSc1
postavení	postavení	k1gNnSc1
se	se	k3xPyFc4
opíralo	opírat	k5eAaImAgNnS
o	o	k7c4
kopec	kopec	k1gInSc4
Santon	Santon	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
vojáci	voják	k1gMnPc1
do	do	k7c2
okopů	okop	k1gInPc2
na	na	k7c6
východním	východní	k2eAgInSc6d1
svahu	svah	k1gInSc6
rozmístili	rozmístit	k5eAaPmAgMnP
18	#num#	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Levé	levý	k2eAgNnSc1d1
křídlo	křídlo	k1gNnSc1
linie	linie	k1gFnSc2
V.	V.	kA
sboru	sbor	k1gInSc2
tvořila	tvořit	k5eAaImAgFnS
Suchetova	Suchetův	k2eAgFnSc1d1
divize	divize	k1gFnSc1
<g/>
,	,	kIx,
křídlo	křídlo	k1gNnSc1
pravé	pravý	k2eAgFnSc2d1
divize	divize	k1gFnSc2
generála	generál	k1gMnSc2
Caffarelliho	Caffarelli	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lannes	Lannesa	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
velitel	velitel	k1gMnSc1
I.	I.	kA
sboru	sbor	k1gInSc3
maršál	maršál	k1gMnSc1
Bernadotte	Bernadott	k1gInSc5
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
podřízen	podřídit	k5eAaPmNgInS
maršálovi	maršál	k1gMnSc3
Muratovi	Murat	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
záložním	záložní	k2eAgNnSc7d1
jezdectvem	jezdectvo	k1gNnSc7
prozatím	prozatím	k6eAd1
vyčkával	vyčkávat	k5eAaImAgInS
u	u	k7c2
Šlapanic	Šlapanice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Napoleonovy	Napoleonův	k2eAgFnSc2d1
direktivy	direktiva	k1gFnSc2
bylo	být	k5eAaImAgNnS
úkolem	úkol	k1gInSc7
francouzského	francouzský	k2eAgNnSc2d1
levého	levý	k2eAgNnSc2d1
křídla	křídlo	k1gNnSc2
pozvolna	pozvolna	k6eAd1
postupovat	postupovat	k5eAaImF
vpřed	vpřed	k6eAd1
<g/>
,	,	kIx,
srovnávat	srovnávat	k5eAaImF
linii	linie	k1gFnSc4
se	s	k7c7
Soultovými	Soultův	k2eAgFnPc7d1
divizemi	divize	k1gFnPc7
zdolávajícími	zdolávající	k2eAgFnPc7d1
Pratecké	pratecký	k2eAgInPc1d1
výšiny	výšina	k1gFnPc4
a	a	k8xC
poté	poté	k6eAd1
se	se	k3xPyFc4
stočit	stočit	k5eAaPmF
vpravo	vpravo	k6eAd1
<g/>
,	,	kIx,
udeřit	udeřit	k5eAaPmF
spojencům	spojenec	k1gMnPc3
do	do	k7c2
boku	bok	k1gInSc2
a	a	k8xC
pokusit	pokusit	k5eAaPmF
se	se	k3xPyFc4
jim	on	k3xPp3gNnPc3
přetnout	přetnout	k5eAaPmF
operační	operační	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc4
fázi	fáze	k1gFnSc4
tohoto	tento	k3xDgInSc2
plánu	plán	k1gInSc2
se	se	k3xPyFc4
maršál	maršál	k1gMnSc1
Lannes	Lannes	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
uskutečnit	uskutečnit	k5eAaPmF
asi	asi	k9
v	v	k7c4
9.15	9.15	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
pohyb	pohyb	k1gInSc1
jeho	jeho	k3xOp3gNnPc2
kolon	kolon	k1gNnPc2
neunikl	uniknout	k5eNaPmAgMnS
spojeneckým	spojenecký	k2eAgMnSc7d1
velitelům	velitel	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
nebezpečné	bezpečný	k2eNgFnSc2d1
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
doposud	doposud	k6eAd1
neobjevil	objevit	k5eNaPmAgInS
Bagration	Bagration	k1gInSc1
a	a	k8xC
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
izolaci	izolace	k1gFnSc3
některých	některý	k3yIgFnPc2
částí	část	k1gFnPc2
křídla	křídlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouský	rakouský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Jan	Jan	k1gMnSc1
z	z	k7c2
Lichtensteina	Lichtensteino	k1gNnSc2
se	se	k3xPyFc4
proto	proto	k8xC
rozhodl	rozhodnout	k5eAaPmAgInS
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
Kellermannovy	Kellermannův	k2eAgInPc4d1
lehké	lehký	k2eAgInPc4d1
jezdecké	jezdecký	k2eAgInPc4d1
pluky	pluk	k1gInPc4
operující	operující	k2eAgMnSc1d1
před	před	k7c7
francouzskou	francouzský	k2eAgFnSc7d1
pěchotou	pěchota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
9.30	9.30	k4
proti	proti	k7c3
těmto	tento	k3xDgFnPc3
jednotkám	jednotka	k1gFnPc3
před	před	k7c4
Caffarelliho	Caffarelli	k1gMnSc4
divizí	divize	k1gFnPc2
vyrazilo	vyrazit	k5eAaPmAgNnS
10	#num#	k4
eskadron	eskadrona	k1gFnPc2
hulánů	hulán	k1gMnPc2
(	(	kIx(
<g/>
1368	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
)	)	kIx)
z	z	k7c2
Šepeljevovy	Šepeljevův	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
díky	díky	k7c3
Lannesově	Lannesův	k2eAgFnSc3d1
výcviku	výcvik	k1gInSc3
vojska	vojsko	k1gNnSc2
v	v	k7c6
součinnosti	součinnost	k1gFnSc6
pěchoty	pěchota	k1gFnSc2
a	a	k8xC
jezdectva	jezdectvo	k1gNnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
z	z	k7c2
boje	boj	k1gInSc2
vyřazeno	vyřazen	k2eAgNnSc1d1
na	na	k7c4
300	#num#	k4
Rusů	Rus	k1gMnPc2
včetně	včetně	k7c2
majitele	majitel	k1gMnSc2
pluku	pluk	k1gInSc2
generálporučíka	generálporučík	k1gMnSc2
Essena	Essen	k1gMnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
francouzském	francouzský	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Husaři	husar	k1gMnPc1
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
pustili	pustit	k5eAaPmAgMnP
za	za	k7c4
hulány	hulán	k1gMnPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
kolem	kolem	k7c2
10.00	10.00	k4
na	na	k7c4
bojiště	bojiště	k1gNnSc4
dorazilo	dorazit	k5eAaPmAgNnS
lehké	lehký	k2eAgNnSc1d1
jezdectvo	jezdectvo	k1gNnSc1
náležející	náležející	k2eAgNnSc1d1
k	k	k7c3
Bagrationovu	Bagrationův	k2eAgInSc3d1
předvoji	předvoj	k1gInSc3
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
pronásledovatele	pronásledovatel	k1gMnPc4
odrazilo	odrazit	k5eAaPmAgNnS
za	za	k7c7
pomocí	pomoc	k1gFnSc7
děl	dělo	k1gNnPc2
své	svůj	k3xOyFgFnSc2
jízdní	jízdní	k2eAgFnSc2d1
baterie	baterie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Ustupující	ustupující	k2eAgMnPc1d1
Francouzi	Francouz	k1gMnPc1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
čelit	čelit	k5eAaImF
protiútoku	protiútok	k1gInSc3
dalších	další	k2eAgFnPc2d1
spojeneckých	spojenecký	k2eAgFnPc2d1
eskadron	eskadrona	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
odpoutat	odpoutat	k5eAaPmF
teprve	teprve	k6eAd1
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
generál	generál	k1gMnSc1
Kellermann	Kellermann	k1gMnSc1
do	do	k7c2
boje	boj	k1gInSc2
nasadil	nasadit	k5eAaPmAgMnS
dragouny	dragoun	k1gInPc7
Waltherovy	Waltherův	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jean	Jean	k1gMnSc1
Peyron	Peyron	k1gMnSc1
<g/>
:	:	kIx,
zranění	zranění	k1gNnSc1
generála	generál	k1gMnSc2
Valhuberta	Valhubert	k1gMnSc2
</s>
<s>
Asi	asi	k9
v	v	k7c6
10.00	10.00	k4
dal	dát	k5eAaPmAgMnS
velkokníže	velkokníže	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
povel	povel	k1gInSc1
gardovým	gardový	k2eAgMnPc3d1
myslivcům	myslivec	k1gMnPc3
zaujmout	zaujmout	k5eAaPmF
pozici	pozice	k1gFnSc4
v	v	k7c6
Blažovicích	Blažovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sotva	sotva	k8xS
vojáci	voják	k1gMnPc1
vesnici	vesnice	k1gFnSc4
obsadili	obsadit	k5eAaPmAgMnP
<g/>
,	,	kIx,
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
ně	on	k3xPp3gMnPc4
13	#num#	k4
<g/>
.	.	kIx.
lehký	lehký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
od	od	k7c2
Demontovy	Demontův	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
nápor	nápor	k1gInSc4
Francouzů	Francouz	k1gMnPc2
se	se	k3xPyFc4
Rusům	Rus	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
odrazit	odrazit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
druhému	druhý	k4xOgMnSc3
útoku	útok	k1gInSc3
<g/>
,	,	kIx,
podporovanému	podporovaný	k2eAgMnSc3d1
51	#num#	k4
<g/>
.	.	kIx.
plukem	pluk	k1gInSc7
Debillyho	Debilly	k1gMnSc2
brigády	brigáda	k1gFnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
po	po	k7c6
příchodu	příchod	k1gInSc6
dvou	dva	k4xCgInPc2
praporů	prapor	k1gInPc2
Tělesného	tělesný	k2eAgNnSc2d1
<g/>
–	–	k?
<g/>
gardového	gardový	k2eAgInSc2d1
Semenovského	Semenovský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
odolávat	odolávat	k5eAaImF
nedokázali	dokázat	k5eNaPmAgMnP
a	a	k8xC
zhruba	zhruba	k6eAd1
v	v	k7c6
11.00	11.00	k4
ustoupili	ustoupit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průběhu	průběh	k1gInSc6
tohoto	tento	k3xDgInSc2
boje	boj	k1gInSc2
dal	dát	k5eAaPmAgMnS
carevič	carevič	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
pokyn	pokyn	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
batalionu	batalion	k1gInSc2
Izmajlovského	Izmajlovský	k2eAgInSc2d1
gardového	gardový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
Pratecké	pratecký	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
a	a	k8xC
podpořil	podpořit	k5eAaPmAgMnS
hraběte	hrabě	k1gMnSc4
Kutuzova	Kutuzův	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
dostala	dostat	k5eAaPmAgFnS
i	i	k9
druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
gardových	gardový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
pod	pod	k7c7
velením	velení	k1gNnSc7
generálmajora	generálmajor	k1gMnSc2
Maljutina	Maljutin	k1gMnSc2
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
přicházející	přicházející	k2eAgFnPc1d1
na	na	k7c4
bitevní	bitevní	k2eAgNnSc4d1
pole	pole	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
odchodem	odchod	k1gInSc7
části	část	k1gFnSc2
gardových	gardový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
musel	muset	k5eAaImAgMnS
kníže	kníže	k1gMnSc1
Liechtenstein	Liechtenstein	k1gMnSc1
vyplnit	vyplnit	k5eAaPmF
uvolněný	uvolněný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
a	a	k8xC
přimknout	přimknout	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
Uvarovově	Uvarovův	k2eAgFnSc3d1
a	a	k8xC
Šepeljevově	Šepeljevův	k2eAgFnSc3d1
brigádě	brigáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tu	ten	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
se	se	k3xPyFc4
však	však	k9
na	na	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Blažovic	Blažovice	k1gFnPc2
objevila	objevit	k5eAaPmAgFnS
bojem	boj	k1gInSc7
zatím	zatím	k6eAd1
netknutá	tknutý	k2eNgFnSc1d1
Rivaudova	Rivaudův	k2eAgFnSc1d1
divize	divize	k1gFnSc1
Bernadottova	Bernadottův	k2eAgInSc2d1
I.	I.	kA
sboru	sbor	k1gInSc2
pochodující	pochodující	k2eAgFnPc4d1
na	na	k7c4
pomoc	pomoc	k1gFnSc4
generálu	generál	k1gMnSc3
Vandammovi	Vandamm	k1gMnSc3
a	a	k8xC
rakouský	rakouský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
tuto	tento	k3xDgFnSc4
pochodovou	pochodový	k2eAgFnSc4d1
kolonu	kolona	k1gFnSc4
napadnout	napadnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útoku	útok	k1gInSc2
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
kyrysnická	kyrysnický	k2eAgFnSc1d1
divize	divize	k1gFnSc1
Hohenlohe	Hohenlohe	k1gFnSc1
<g/>
,	,	kIx,
jezdci	jezdec	k1gMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
odraženi	odrazit	k5eAaPmNgMnP
a	a	k8xC
ustoupili	ustoupit	k5eAaPmAgMnP
směrem	směr	k1gInSc7
za	za	k7c7
ruskou	ruský	k2eAgFnSc7d1
gardou	garda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
boje	boj	k1gInPc1
jezdectva	jezdectvo	k1gNnSc2
a	a	k8xC
pěchoty	pěchota	k1gFnPc1
probíhaly	probíhat	k5eAaImAgFnP
v	v	k7c6
prostoru	prostor	k1gInSc6
mezi	mezi	k7c7
Blažovicemi	Blažovice	k1gFnPc7
a	a	k8xC
Tvarožnou	Tvarožný	k2eAgFnSc7d1
(	(	kIx(
<g/>
Bosenicemi	Bosenice	k1gFnPc7
<g/>
)	)	kIx)
až	až	k9
zhruba	zhruba	k6eAd1
do	do	k7c2
12.30	12.30	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitevní	bitevní	k2eAgFnSc1d1
pole	pole	k1gFnSc1
nakonec	nakonec	k6eAd1
opanovaly	opanovat	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
maršála	maršál	k1gMnSc2
Murata	Murat	k1gMnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
takticky	takticky	k6eAd1
byli	být	k5eAaImAgMnP
úspěšnější	úspěšný	k2eAgMnPc1d2
spojenci	spojenec	k1gMnPc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
neumožnili	umožnit	k5eNaPmAgMnP
protivníkovi	protivníkův	k2eAgMnPc1d1
vpadnout	vpadnout	k5eAaPmF
do	do	k7c2
týlu	týl	k1gInSc2
jednotkám	jednotka	k1gFnPc3
na	na	k7c6
Prateckém	pratecký	k2eAgNnSc6d1
návrší	návrší	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Situace	situace	k1gFnSc1
na	na	k7c6
bojišti	bojiště	k1gNnSc6
mezi	mezi	k7c7
14.00	14.00	k4
<g/>
–	–	k?
<g/>
17.00	17.00	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapa	mapa	k1gFnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
všeobecný	všeobecný	k2eAgInSc4d1
ústup	ústup	k1gInSc4
spojeneckých	spojenecký	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
a	a	k8xC
manévry	manévr	k1gInPc1
Grande	grand	k1gMnSc5
Armée	Armé	k1gMnSc2
</s>
<s>
Asi	asi	k9
ve	v	k7c4
12.30	12.30	k4
se	se	k3xPyFc4
maršál	maršál	k1gMnSc1
Lannes	Lannes	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
zaútočit	zaútočit	k5eAaPmF
celým	celý	k2eAgFnPc3d1
V.	V.	kA
sborem	sbor	k1gInSc7
proti	proti	k7c3
Bagrationovu	Bagrationův	k2eAgInSc3d1
předvoji	předvoj	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c6
důsledku	důsledek	k1gInSc6
předchozích	předchozí	k2eAgInPc2d1
bojů	boj	k1gInPc2
osamocen	osamocen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
pěší	pěší	k2eAgFnSc2d1
divize	divize	k1gFnSc2
rozmístěné	rozmístěný	k2eAgFnPc4d1
napříč	napříč	k7c7
císařskou	císařský	k2eAgFnSc7d1
silnicí	silnice	k1gFnSc7
postupovaly	postupovat	k5eAaImAgFnP
jen	jen	k6eAd1
pozvolna	pozvolna	k6eAd1
a	a	k8xC
pravým	pravý	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgInP
na	na	k7c4
dohled	dohled	k1gInSc4
Kruhu	kruh	k1gInSc2
asi	asi	k9
po	po	k7c6
hodinovém	hodinový	k2eAgInSc6d1
pochodu	pochod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bagrationovy	Bagrationův	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
stály	stát	k5eAaImAgFnP
s	s	k7c7
Lannesem	Lannes	k1gInSc7
takřka	takřka	k6eAd1
v	v	k7c6
paralelním	paralelní	k2eAgNnSc6d1
postavení	postavení	k1gNnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
levým	levá	k1gFnPc3
bokem	bokem	k6eAd1
opíraly	opírat	k5eAaImAgFnP
o	o	k7c4
Holubice	holubice	k1gFnPc4
<g/>
,	,	kIx,
pravým	pravý	k2eAgMnSc7d1
pak	pak	k6eAd1
o	o	k7c6
Sivice	Sivika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
ruskému	ruský	k2eAgMnSc3d1
veliteli	velitel	k1gMnSc3
dorazily	dorazit	k5eAaPmAgInP
první	první	k4xOgFnPc4
dispozice	dispozice	k1gFnPc4
pro	pro	k7c4
nadcházející	nadcházející	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
kolem	kolem	k7c2
osmé	osmý	k4xOgFnSc2
hodiny	hodina	k1gFnSc2
ranní	ranní	k2eAgInSc4d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
měl	mít	k5eAaImAgInS
tedy	tedy	k9
dostatečný	dostatečný	k2eAgInSc1d1
čas	čas	k1gInSc1
pro	pro	k7c4
uspořádání	uspořádání	k1gNnSc4
mužstva	mužstvo	k1gNnSc2
i	i	k8xC
jezdeckých	jezdecký	k2eAgFnPc2d1
eskadron	eskadrona	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
k	k	k7c3
němu	on	k3xPp3gInSc3
ustupovaly	ustupovat	k5eAaImAgFnP
v	v	k7c6
průběhu	průběh	k1gInSc6
dopoledních	dopolední	k2eAgFnPc2d1
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
nápor	nápor	k1gInSc4
proti	proti	k7c3
jeho	jeho	k3xOp3gNnSc3
levému	levý	k2eAgNnSc3d1
křídlu	křídlo	k1gNnSc3
v	v	k7c6
Holubicích	holubice	k1gFnPc6
zahájila	zahájit	k5eAaPmAgFnS
Caffarelliho	Caffarelli	k1gMnSc2
řadová	řadový	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
zhruba	zhruba	k6eAd1
o	o	k7c4
půl	půl	k1xP
druhé	druhý	k4xOgFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
kartáčové	kartáčový	k2eAgFnSc3d1
střelbě	střelba	k1gFnSc3
zde	zde	k6eAd1
rozmístěných	rozmístěný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
však	však	k9
byla	být	k5eAaImAgFnS
s	s	k7c7
vysokými	vysoký	k2eAgFnPc7d1
ztrátami	ztráta	k1gFnPc7
odražena	odrazit	k5eAaPmNgFnS
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byly	být	k5eAaImAgInP
pluky	pluk	k1gInPc1
Bagrationova	Bagrationův	k2eAgInSc2d1
středu	střed	k1gInSc2
(	(	kIx(
<g/>
Staroingermanlandský	Staroingermanlandský	k2eAgMnSc1d1
a	a	k8xC
Pskovský	Pskovský	k2eAgMnSc1d1
<g/>
)	)	kIx)
nuceny	nutit	k5eAaImNgFnP
čelit	čelit	k5eAaImF
úderu	úder	k1gInSc2
Suchetovy	Suchetův	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
půlhodině	půlhodina	k1gFnSc6
bojů	boj	k1gInPc2
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
Francouzi	Francouz	k1gMnPc1
stahovat	stahovat	k5eAaImF
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
ruské	ruský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnSc2
podnítili	podnítit	k5eAaPmAgMnP
k	k	k7c3
rozhodnému	rozhodný	k2eAgInSc3d1
protiútoku	protiútok	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nastalém	nastalý	k2eAgInSc6d1
boji	boj	k1gInSc6
utržili	utržit	k5eAaPmAgMnP
zranění	zraněný	k1gMnPc1
gnenerál	gnenerál	k1gMnSc1
Suchet	Suchet	k1gMnSc1
i	i	k8xC
velitel	velitel	k1gMnSc1
jeho	jeho	k3xOp3gFnPc4
první	první	k4xOgFnPc4
brigády	brigáda	k1gFnPc4
generál	generál	k1gMnSc1
Beker	Beker	k1gMnSc1
a	a	k8xC
situaci	situace	k1gFnSc4
ve	v	k7c6
francouzských	francouzský	k2eAgFnPc6d1
řadách	řada	k1gFnPc6
stabilizoval	stabilizovat	k5eAaBmAgInS
teprve	teprve	k6eAd1
až	až	k9
zásah	zásah	k1gInSc1
Valhubertovy	Valhubertův	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
(	(	kIx(
<g/>
64	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
88	#num#	k4
<g/>
.	.	kIx.
řadový	řadový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
manévr	manévr	k1gInSc4
Rusy	Rus	k1gMnPc4
odrazil	odrazit	k5eAaPmAgMnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
střepina	střepina	k1gFnSc1
granátu	granát	k1gInSc2
při	při	k7c6
něm	on	k3xPp3gMnSc6
způsobila	způsobit	k5eAaPmAgFnS
smrtelné	smrtelný	k2eAgNnSc4d1
zranění	zranění	k1gNnSc4
generálu	generál	k1gMnSc3
Valhubertovi	Valhubert	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
Paralelně	paralelně	k6eAd1
s	s	k7c7
tímto	tento	k3xDgInSc7
bojem	boj	k1gInSc7
17	#num#	k4
<g/>
.	.	kIx.
lehký	lehký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
za	za	k7c4
podpory	podpora	k1gFnPc4
lehkého	lehký	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
generálů	generál	k1gMnPc2
Milhauda	Milhaud	k1gMnSc2
a	a	k8xC
Treillarda	Treillard	k1gMnSc2
ovládl	ovládnout	k5eAaPmAgMnS
Sivice	Sivice	k1gFnSc1
a	a	k8xC
maršál	maršál	k1gMnSc1
Murat	Murat	k1gInSc4
nařídil	nařídit	k5eAaPmAgMnS
části	část	k1gFnSc3
svého	svůj	k3xOyFgNnSc2
jezdectva	jezdectvo	k1gNnSc2
na	na	k7c6
levém	levý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
zahnat	zahnat	k5eAaPmF
Čaplicovy	Čaplicův	k2eAgMnPc4d1
husary	husar	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sotva	sotva	k8xS
generál	generál	k1gMnSc1
Suchet	Suchet	k1gMnSc1
uspořádal	uspořádat	k5eAaPmAgMnS
své	svůj	k3xOyFgFnPc4
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
vyslal	vyslat	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
maršál	maršál	k1gMnSc1
Lannes	Lannes	k1gMnSc1
do	do	k7c2
dalšího	další	k2eAgInSc2d1
útoku	útok	k1gInSc2
proti	proti	k7c3
spojeneckému	spojenecký	k2eAgInSc3d1
středu	střed	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Caffareliho	Caffareli	k1gMnSc4
divize	divize	k1gFnSc2
na	na	k7c6
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
obnovila	obnovit	k5eAaPmAgFnS
ofenzivní	ofenzivní	k2eAgFnSc1d1
akce	akce	k1gFnSc1
krátce	krátce	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
neúspěchu	neúspěch	k1gInSc6
a	a	k8xC
kolem	kolo	k1gNnSc7
14.00	14.00	k4
začala	začít	k5eAaPmAgFnS
získávat	získávat	k5eAaImF
Holubice	holubice	k1gFnSc1
pod	pod	k7c4
svoji	svůj	k3xOyFgFnSc4
kontrolu	kontrola	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
Zhruba	zhruba	k6eAd1
ve	v	k7c6
14.30	14.30	k4
rostoucí	rostoucí	k2eAgInSc1d1
francouzský	francouzský	k2eAgInSc1d1
tlak	tlak	k1gInSc1
přinutil	přinutit	k5eAaPmAgInS
celou	celý	k2eAgFnSc4d1
ruskou	ruský	k2eAgFnSc4d1
linii	linie	k1gFnSc4
ke	k	k7c3
spořádanému	spořádaný	k2eAgInSc3d1
ústupu	ústup	k1gInSc3
<g/>
,	,	kIx,
krytému	krytý	k2eAgMnSc3d1
palbou	palba	k1gFnSc7
dvou	dva	k4xCgFnPc2
baterií	baterie	k1gFnPc2
těžkých	těžký	k2eAgFnPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
kanónů	kanón	k1gInPc2
rakouského	rakouský	k2eAgMnSc2d1
majora	major	k1gMnSc2
Frierenbergera	Frierenberger	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
dorazily	dorazit	k5eAaPmAgInP
na	na	k7c4
bojiště	bojiště	k1gNnSc4
od	od	k7c2
Olomouce	Olomouc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
půl	půl	k1xP
čtvrté	čtvrtá	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
u	u	k7c2
Kovalovic	Kovalovice	k1gFnPc2
ještě	ještě	k9
k	k	k7c3
dalším	další	k2eAgInPc3d1
střetům	střet	k1gInPc3
<g/>
,	,	kIx,
při	při	k7c6
nichž	jenž	k3xRgFnPc6
Francouzi	Francouz	k1gMnPc1
zajali	zajmout	k5eAaPmAgMnP
několik	několik	k4yIc4
mužů	muž	k1gMnPc2
a	a	k8xC
získali	získat	k5eAaPmAgMnP
jedno	jeden	k4xCgNnSc4
dělo	dělo	k1gNnSc4
<g/>
,	,	kIx,
vzápětí	vzápětí	k6eAd1
však	však	k9
dal	dát	k5eAaPmAgMnS
maršál	maršál	k1gMnSc1
Lannes	Lannes	k1gMnSc1
rozkaz	rozkaz	k1gInSc4
ke	k	k7c3
stažení	stažení	k1gNnSc3
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
byly	být	k5eAaImAgFnP
operace	operace	k1gFnPc1
na	na	k7c6
severní	severní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
ukončeny	ukončen	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozhodující	rozhodující	k2eAgInPc1d1
manévry	manévr	k1gInPc1
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1
gard	garda	k1gFnPc2
</s>
<s>
Viktor	Viktor	k1gMnSc1
Mazurovsky	Mazurovsky	k1gMnSc1
<g/>
:	:	kIx,
ukořistění	ukořistění	k1gNnSc1
orlice	orlice	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
řadového	řadový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
</s>
<s>
Mezi	mezi	k7c7
12.15	12.15	k4
<g/>
–	–	k?
<g/>
12.30	12.30	k4
se	se	k3xPyFc4
ve	v	k7c6
směru	směr	k1gInSc6
od	od	k7c2
Křenovic	Křenovice	k1gFnPc2
začaly	začít	k5eAaPmAgInP
u	u	k7c2
Prateckých	pratecký	k2eAgFnPc2d1
výšin	výšina	k1gFnPc2
objevovat	objevovat	k5eAaImF
ruské	ruský	k2eAgFnPc4d1
gardové	gardový	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
vyvázaly	vyvázat	k5eAaPmAgFnP
z	z	k7c2
boje	boj	k1gInSc2
u	u	k7c2
Blažovic	Blažovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměrem	záměr	k1gInSc7
velkoknížete	velkokníže	k1gMnSc2
Konstantina	Konstantin	k1gMnSc2
bylo	být	k5eAaImAgNnS
navázat	navázat	k5eAaPmF
kontakt	kontakt	k1gInSc4
se	s	k7c7
zbytky	zbytek	k1gInPc7
4	#num#	k4
<g/>
.	.	kIx.
spojenecké	spojenecký	k2eAgFnSc2d1
kolony	kolona	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
s	s	k7c7
úmyslem	úmysl	k1gInSc7
dát	dát	k5eAaPmF
jim	on	k3xPp3gMnPc3
možnost	možnost	k1gFnSc4
vzpamatovat	vzpamatovat	k5eAaPmF
se	se	k3xPyFc4
a	a	k8xC
spořádaně	spořádaně	k6eAd1
ustoupit	ustoupit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnPc1
vojáci	voják	k1gMnPc1
směřovali	směřovat	k5eAaImAgMnP
do	do	k7c2
levého	levý	k2eAgInSc2d1
boku	bok	k1gInSc2
Vandammovy	Vandammův	k2eAgFnSc2d1
divize	divize	k1gFnSc2
v	v	k7c6
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
zaujímal	zaujímat	k5eAaImAgMnS
pozici	pozice	k1gFnSc4
4	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
řadový	řadový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
před	před	k7c7
bitvou	bitva	k1gFnSc7
1800	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
pod	pod	k7c7
velením	velení	k1gNnSc7
majora	major	k1gMnSc2
Bigarré	Bigarrý	k2eAgFnSc2d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
prapor	prapor	k1gInSc1
této	tento	k3xDgFnSc2
jednotky	jednotka	k1gFnSc2
vysunul	vysunout	k5eAaPmAgMnS
maršál	maršál	k1gMnSc1
Soult	Soult	k1gMnSc1
na	na	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
Starých	Starých	k2eAgInPc2d1
vinohradů	vinohrad	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zde	zde	k6eAd1
pozoroval	pozorovat	k5eAaImAgMnS
pohyby	pohyb	k1gInPc4
spojenců	spojenec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
Sotva	sotva	k8xS
major	major	k1gMnSc1
Bigarré	Bigarrý	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gMnSc3
zprava	zprava	k6eAd1
blíží	blížit	k5eAaImIp3nS
ruská	ruský	k2eAgFnSc1d1
kavalerie	kavalerie	k1gFnSc1
a	a	k8xC
zleva	zleva	k6eAd1
pěchota	pěchota	k1gFnSc1
<g/>
,	,	kIx,
podal	podat	k5eAaPmAgMnS
hlášení	hlášení	k1gNnSc2
svým	svůj	k3xOyFgNnSc7
nadřízením	nadřízení	k1gNnSc7
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
praporem	prapor	k1gInSc7
ke	k	k7c3
zbytku	zbytek	k1gInSc3
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
získané	získaný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
generál	generál	k1gMnSc1
Vandamme	Vandamme	k1gMnSc1
vyslal	vyslat	k5eAaPmAgMnS
Bigarrému	Bigarrý	k2eAgMnSc3d1
na	na	k7c4
pomoc	pomoc	k1gFnSc4
vojáky	voják	k1gMnPc4
24	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
lehkého	lehký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
1300	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
než	než	k8xS
tato	tento	k3xDgFnSc1
podpora	podpora	k1gFnSc1
stačila	stačit	k5eAaBmAgFnS
dorazit	dorazit	k5eAaPmF
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
se	s	k7c7
4	#num#	k4
<g/>
.	.	kIx.
řadový	řadový	k2eAgInSc1d1
pluk	pluk	k1gInSc1
zformovat	zformovat	k5eAaPmF
do	do	k7c2
karé	karé	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
neboť	neboť	k8xC
proti	proti	k7c3
němu	on	k3xPp3gNnSc3
za	za	k7c2
podpory	podpora	k1gFnSc2
kartáčové	kartáčový	k2eAgFnSc2d1
palby	palba	k1gFnSc2
čtyř	čtyři	k4xCgFnPc2
hlavní	hlavní	k2eAgFnSc7d1
gardového	gardový	k2eAgNnSc2d1
jízdního	jízdní	k2eAgNnSc2d1
dělostřelectva	dělostřelectvo	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
zaútočilo	zaútočit	k5eAaPmAgNnS
pět	pět	k4xCc4
eskadron	eskadrona	k1gFnPc2
Tělesného	tělesný	k2eAgNnSc2d1
<g/>
–	–	k?
<g/>
gardového	gardový	k2eAgInSc2d1
jezdeckého	jezdecký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
(	(	kIx(
<g/>
760	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc4
ruský	ruský	k2eAgInSc4d1
útok	útok	k1gInSc4
se	se	k3xPyFc4
Francouzům	Francouz	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
salvou	salva	k1gFnSc7
z	z	k7c2
mušket	mušketa	k1gFnPc2
odrazit	odrazit	k5eAaPmF
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
druhý	druhý	k4xOgInSc1
nájezd	nájezd	k1gInSc1
sestavu	sestava	k1gFnSc4
rozrazil	rozrazit	k5eAaPmAgInS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
pluk	pluk	k1gInSc1
ztratil	ztratit	k5eAaPmAgInS
na	na	k7c4
200	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
svou	svůj	k3xOyFgFnSc4
zástavu	zástava	k1gFnSc4
(	(	kIx(
<g/>
orlici	orlice	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
Útočící	útočící	k2eAgMnPc4d1
ruské	ruský	k2eAgMnPc4d1
gardisty	gardista	k1gMnPc4
následně	následně	k6eAd1
podpořilo	podpořit	k5eAaPmAgNnS
pět	pět	k4xCc4
eskadron	eskadrona	k1gFnPc2
Tělesného	tělesný	k2eAgNnSc2d1
<g/>
–	–	k?
<g/>
gardového	gardový	k2eAgInSc2d1
husarského	husarský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
(	(	kIx(
<g/>
690	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
ve	v	k7c6
vzájemné	vzájemný	k2eAgFnSc6d1
součinnosti	součinnost	k1gFnSc6
atakovali	atakovat	k5eAaBmAgMnP
a	a	k8xC
rozprášili	rozprášit	k5eAaPmAgMnP
i	i	k9
24	#num#	k4
<g/>
.	.	kIx.
lehký	lehký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
střetnutí	střetnutí	k1gNnSc1
se	se	k3xPyFc4
již	již	k6eAd1
odehrálo	odehrát	k5eAaPmAgNnS
před	před	k7c7
očima	oko	k1gNnPc7
Napoleona	Napoleon	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
právě	právě	k6eAd1
dorazil	dorazit	k5eAaPmAgMnS
na	na	k7c4
Staré	Staré	k2eAgInPc4d1
vinohrady	vinohrad	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
viděl	vidět	k5eAaImAgInS
debakl	debakl	k1gInSc1
pěších	pěší	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
vyslal	vyslat	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
pobočníka	pobočník	k1gMnSc4
brigádního	brigádní	k2eAgMnSc2d1
generála	generál	k1gMnSc2
Jeana	Jean	k1gMnSc2
Rappa	Rapp	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
do	do	k7c2
boje	boj	k1gInSc2
proti	proti	k7c3
postupujícím	postupující	k2eAgMnPc3d1
Rusům	Rus	k1gMnPc3
nasměroval	nasměrovat	k5eAaPmAgInS
gardové	gardový	k2eAgNnSc4d1
jezdectvo	jezdectvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protiútoku	protiútok	k1gInSc2
se	se	k3xPyFc4
účastnily	účastnit	k5eAaImAgFnP
čtyři	čtyři	k4xCgFnPc1
eskadrony	eskadrona	k1gFnPc1
gardových	gardový	k2eAgMnPc2d1
jízdních	jízdní	k2eAgMnPc2d1
myslivců	myslivec	k1gMnPc2
a	a	k8xC
čtyři	čtyři	k4xCgFnPc4
eskadrony	eskadrona	k1gFnPc1
gardových	gardový	k2eAgMnPc2d1
jízdních	jízdní	k2eAgMnPc2d1
granátníků	granátník	k1gMnPc2
rozvinutých	rozvinutý	k2eAgMnPc2d1
do	do	k7c2
třech	tři	k4xCgInPc2
sledů	sled	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
těmito	tento	k3xDgFnPc7
jednotkami	jednotka	k1gFnPc7
postupovalo	postupovat	k5eAaImAgNnS
i	i	k8xC
čtyřicet	čtyřicet	k4xCc1
osm	osm	k4xCc1
mameluků	mameluk	k1gMnPc2
gardy	garda	k1gFnSc2
a	a	k8xC
eskadrona	eskadrona	k1gFnSc1
vélitů	vélita	k1gMnPc2
od	od	k7c2
gardových	gardový	k2eAgMnPc2d1
jízdních	jízdní	k2eAgMnPc2d1
granátníků	granátník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
francouzský	francouzský	k2eAgInSc1d1
útok	útok	k1gInSc1
<g/>
,	,	kIx,
podpořený	podpořený	k2eAgInSc1d1
střelbou	střelba	k1gFnSc7
gardové	gardový	k2eAgFnSc2d1
jízdní	jízdní	k2eAgFnSc2d1
baterie	baterie	k1gFnSc2
<g/>
,	,	kIx,
asi	asi	k9
ve	v	k7c4
13.30	13.30	k4
dopadl	dopadnout	k5eAaPmAgInS
na	na	k7c4
tělesný	tělesný	k2eAgInSc4d1
<g/>
–	–	k?
<g/>
gardový	gardový	k2eAgInSc1d1
husarský	husarský	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
pronásledování	pronásledování	k1gNnSc2
protivníkovy	protivníkův	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
nestačil	stačit	k5eNaBmAgInS
zcela	zcela	k6eAd1
seskupit	seskupit	k5eAaPmF
a	a	k8xC
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
ustoupit	ustoupit	k5eAaPmF
za	za	k7c4
mušketýry	mušketýr	k1gMnPc4
Semenovského	Semenovský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
právě	právě	k6eAd1
stoupali	stoupat	k5eAaImAgMnP
do	do	k7c2
svahu	svah	k1gInSc2
Starých	Starých	k2eAgInPc2d1
vinohradů	vinohrad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozba	hrozba	k1gFnSc1
útoku	útok	k1gInSc2
francouzského	francouzský	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
přiměla	přimět	k5eAaPmAgFnS
ruské	ruský	k2eAgFnPc4d1
velitele	velitel	k1gMnSc2
jednotku	jednotka	k1gFnSc4
přeskupit	přeskupit	k5eAaPmF
do	do	k7c2
dvou	dva	k4xCgNnPc2
karé	karé	k1gNnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
první	první	k4xOgInSc4
protivníkův	protivníkův	k2eAgInSc4d1
nájezd	nájezd	k1gInSc4
úspěšně	úspěšně	k6eAd1
odrazili	odrazit	k5eAaPmAgMnP
(	(	kIx(
<g/>
v	v	k7c6
boji	boj	k1gInSc6
byl	být	k5eAaImAgInS
smrtelně	smrtelně	k6eAd1
zraněn	zraněn	k2eAgMnSc1d1
francouzský	francouzský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
Morlan	Morlan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
pokračujících	pokračující	k2eAgInPc2d1
střetů	střet	k1gInPc2
na	na	k7c4
bojiště	bojiště	k1gNnSc4
dorazila	dorazit	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
půlka	půlka	k1gFnSc1
imperátorské	imperátorský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
generálporučíka	generálporučík	k1gMnSc2
Maljutina	Maljutin	k1gMnSc2
a	a	k8xC
velkokníže	velkokníže	k1gMnSc1
Konstantin	Konstantin	k1gMnSc1
povolal	povolat	k5eAaPmAgMnS
k	k	k7c3
podpoře	podpora	k1gFnSc3
zle	zle	k6eAd1
tísněné	tísněný	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
pět	pět	k4xCc4
eskadron	eskadrona	k1gFnPc2
petrohradské	petrohradský	k2eAgFnSc2d1
kavalergardy	kavalergarda	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
770	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
Tělesný	tělesný	k2eAgInSc1d1
<g/>
–	–	k?
<g/>
gardový	gardový	k2eAgInSc1d1
kozácký	kozácký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
(	(	kIx(
<g/>
300	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k9
první	první	k4xOgMnSc1
se	se	k3xPyFc4
do	do	k7c2
boje	boj	k1gInSc2
pravděpodobně	pravděpodobně	k6eAd1
pustili	pustit	k5eAaPmAgMnP
kozáci	kozák	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
vrhli	vrhnout	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c4
jízdní	jízdní	k2eAgMnPc4d1
myslivce	myslivec	k1gMnPc4
dotírající	dotírající	k2eAgInSc1d1
na	na	k7c4
Semenovský	Semenovský	k2eAgInSc4d1
pluk	pluk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
svahu	svah	k1gInSc6
nad	nad	k7c7
Křenovicemi	Křenovice	k1gFnPc7
do	do	k7c2
boje	boj	k1gInSc2
pustila	pustit	k5eAaPmAgFnS
část	část	k1gFnSc1
Kavalergardy	Kavalergarda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
napadla	napadnout	k5eAaPmAgFnS
Drouetovu	Drouetův	k2eAgFnSc4d1
divizi	divize	k1gFnSc4
Bernadotova	Bernadotův	k2eAgInSc2d1
I.	I.	kA
sboru	sbor	k1gInSc2
<g/>
,	,	kIx,
směřující	směřující	k2eAgInSc4d1
podpořit	podpořit	k5eAaPmF
Sain	Sain	k1gNnSc4
<g/>
–	–	k?
<g/>
Hilairovu	Hilairův	k2eAgFnSc4d1
divizi	divize	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzským	francouzský	k2eAgMnSc7d1
pěšákům	pěšák	k1gMnPc3
však	však	k9
dorazil	dorazit	k5eAaPmAgMnS
na	na	k7c4
pomoc	pomoc	k1gFnSc4
první	první	k4xOgInSc4
sled	sled	k1gInSc4
francouzského	francouzský	k2eAgNnSc2d1
gardového	gardový	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
generálů	generál	k1gMnPc2
Rappa	Rapp	k1gMnSc2
a	a	k8xC
Dahlmanna	Dahlmann	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
za	za	k7c2
neustávajícího	ustávající	k2eNgInSc2d1
boje	boj	k1gInSc2
obrátili	obrátit	k5eAaPmAgMnP
Rusy	Rus	k1gMnPc4
na	na	k7c4
ústup	ústup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
se	se	k3xPyFc4
však	však	k9
do	do	k7c2
střetu	střet	k1gInSc2
zapojila	zapojit	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
kavalergardy	kavalergarda	k1gFnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
knížetem	kníže	k1gMnSc7
Repninem-Volkonským-	Repninem-Volkonským-	k1gMnSc7
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
podpořily	podpořit	k5eAaPmAgInP
i	i	k9
dvě	dva	k4xCgFnPc1
eskadrony	eskadrona	k1gFnPc1
Tělesného	tělesný	k2eAgInSc2d1
<g/>
–	–	k?
<g/>
gardového	gardový	k2eAgInSc2d1
jezdeckého	jezdecký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
útvary	útvar	k1gInPc1
se	se	k3xPyFc4
neprodleně	prodleně	k6eNd1
zapojily	zapojit	k5eAaPmAgFnP
do	do	k7c2
probíhajícího	probíhající	k2eAgNnSc2d1
zápolení	zápolení	k1gNnSc2
<g/>
,	,	kIx,
po	po	k7c6
asi	asi	k9
patnáctiminutové	patnáctiminutový	k2eAgFnSc6d1
vřavě	vřava	k1gFnSc6
však	však	k9
maršál	maršál	k1gMnSc1
Bessiè	Bessiè	k1gMnSc1
do	do	k7c2
boje	boj	k1gInSc2
vyslal	vyslat	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
eskadrony	eskadrona	k1gFnPc4
gardových	gardový	k2eAgMnPc2d1
jízdních	jízdní	k2eAgMnPc2d1
granátníků	granátník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
vítězství	vítězství	k1gNnSc4
obrátili	obrátit	k5eAaPmAgMnP
na	na	k7c4
francouzskou	francouzský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
Francouzská	francouzský	k2eAgFnSc1d1
kavalerie	kavalerie	k1gFnSc1
ihned	ihned	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
pronásledování	pronásledování	k1gNnSc6
a	a	k8xC
vrhla	vrhnout	k5eAaPmAgFnS,k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c4
osamocenou	osamocený	k2eAgFnSc4d1
pěchotu	pěchota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
již	již	k6eAd1
novému	nový	k2eAgInSc3d1
náporu	nápor	k1gInSc3
odolávat	odolávat	k5eAaImF
nedokázala	dokázat	k5eNaPmAgFnS
a	a	k8xC
záhy	záhy	k6eAd1
celá	celý	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
prchala	prchat	k5eAaImAgFnS
směrem	směr	k1gInSc7
ke	k	k7c3
Křenovicím	Křenovice	k1gFnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zpomalila	zpomalit	k5eAaPmAgFnS
až	až	k6eAd1
u	u	k7c2
potoka	potok	k1gInSc2
Rakovec	Rakovec	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úder	úder	k1gInSc1
na	na	k7c6
Zlatém	zlatý	k2eAgInSc6d1
potoku	potok	k1gInSc6
</s>
<s>
Thomas	Thomas	k1gMnSc1
Campbell	Campbell	k1gMnSc1
<g/>
:	:	kIx,
proboření	proboření	k1gNnSc1
ledu	led	k1gInSc2
na	na	k7c6
Žatčanském	Žatčanský	k2eAgInSc6d1
rybníku	rybník	k1gInSc6
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
13.00	13.00	k4
Napoleon	napoleon	k1gInSc1
rozkázal	rozkázat	k5eAaPmAgInS
maršálu	maršál	k1gMnSc3
Soultovi	Soult	k1gMnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jeho	jeho	k3xOp3gInSc1
sbor	sbor	k1gInSc1
vpadl	vpadnout	k5eAaPmAgInS
do	do	k7c2
zad	záda	k1gNnPc2
třem	tři	k4xCgFnPc3
spojeneckým	spojenecký	k2eAgFnPc3d1
kolonám	kolona	k1gFnPc3
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgNnPc7
se	se	k3xPyFc4
od	od	k7c2
dopoledních	dopolední	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
potýkali	potýkat	k5eAaImAgMnP
maršál	maršál	k1gMnSc1
Davout	Davout	k1gMnSc1
a	a	k8xC
generál	generál	k1gMnSc1
Legrand	legranda	k1gFnPc2
u	u	k7c2
Telnice	Telnice	k1gFnSc2
a	a	k8xC
Sokolnic	sokolnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc7
zahájila	zahájit	k5eAaPmAgFnS
z	z	k7c2
Prateckého	pratecký	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
sestup	sestup	k1gInSc1
Saint-Hilairova	Saint-Hilairov	k1gInSc2
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
úder	úder	k1gInSc1
směřoval	směřovat	k5eAaImAgInS
do	do	k7c2
zad	záda	k1gNnPc2
3	#num#	k4
<g/>
.	.	kIx.
spojenecké	spojenecký	k2eAgFnSc3d1
koloně	kolona	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Przybyszewski	Przybyszewske	k1gFnSc4
byl	být	k5eAaImAgMnS
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
nucen	nutit	k5eAaImNgMnS
čelit	čelit	k5eAaImF
útoku	útok	k1gInSc2
ze	z	k7c2
tří	tři	k4xCgFnPc2
stran	strana	k1gFnPc2
a	a	k8xC
kolem	kolem	k7c2
15.00	15.00	k4
mu	on	k3xPp3gMnSc3
nezbylo	zbýt	k5eNaPmAgNnS
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
asi	asi	k9
3000	#num#	k4
muži	muž	k1gMnPc7
pokusit	pokusit	k5eAaPmF
prorazit	prorazit	k5eAaPmF
směrem	směr	k1gInSc7
proti	proti	k7c3
proudu	proud	k1gInSc3
Zlatého	zlatý	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
ústupu	ústup	k1gInSc6
jej	on	k3xPp3gMnSc4
však	však	k9
důrazně	důrazně	k6eAd1
pronásledovalo	pronásledovat	k5eAaImAgNnS
francouzské	francouzský	k2eAgNnSc4d1
jezdectvo	jezdectvo	k1gNnSc4
a	a	k8xC
zanedlouho	zanedlouho	k6eAd1
i	i	k9
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
vojáky	voják	k1gMnPc7
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
do	do	k7c2
zajetí	zajetí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
něco	něco	k3yInSc4
jižněji	jižně	k6eAd2
mezi	mezi	k7c7
13.30	13.30	k4
<g/>
–	–	k?
<g/>
13.45	13.45	k4
zatím	zatím	k6eAd1
generál	generál	k1gMnSc1
Buxhövden	Buxhövdna	k1gFnPc2
začal	začít	k5eAaPmAgMnS
organizovat	organizovat	k5eAaBmF
ústup	ústup	k1gInSc4
většiny	většina	k1gFnSc2
svých	svůj	k3xOyFgFnPc2
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
aktivitu	aktivita	k1gFnSc4
však	však	k9
prováděl	provádět	k5eAaImAgMnS
se	s	k7c7
značným	značný	k2eAgNnSc7d1
zpožděním	zpoždění	k1gNnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
rozkaz	rozkaz	k1gInSc1
od	od	k7c2
generála	generál	k1gMnSc2
Kutuzova	Kutuzův	k2eAgInSc2d1
ke	k	k7c3
stažení	stažení	k1gNnSc3
spojeneckých	spojenecký	k2eAgFnPc2d1
sil	síla	k1gFnPc2
obdržel	obdržet	k5eAaPmAgMnS
už	už	k6eAd1
po	po	k7c6
12.30	12.30	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
Zhruba	zhruba	k6eAd1
ve	v	k7c6
14.00	14.00	k4
dorazil	dorazit	k5eAaPmAgMnS
k	k	k7c3
tomuto	tento	k3xDgInSc3
úseku	úsek	k1gInSc3
bojiště	bojiště	k1gNnSc1
Napoleon	Napoleon	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
si	se	k3xPyFc3
jako	jako	k9
své	svůj	k3xOyFgNnSc4
stanoviště	stanoviště	k1gNnSc4
zvolil	zvolit	k5eAaPmAgMnS
kopec	kopec	k1gInSc4
u	u	k7c2
kaple	kaple	k1gFnSc2
sv.	sv.	kA
Antonína	Antonín	k1gMnSc4
Paduánského	paduánský	k2eAgMnSc4d1
nad	nad	k7c7
Újezdem	Újezd	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
hodinu	hodina	k1gFnSc4
později	pozdě	k6eAd2
zaútočily	zaútočit	k5eAaPmAgFnP
brigády	brigáda	k1gFnPc1
generálů	generál	k1gMnPc2
Varého	Varý	k1gMnSc2
(	(	kIx(
<g/>
IV	IV	kA
<g/>
.	.	kIx.
sbor	sbor	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Fereye	Fereye	k1gFnSc1
(	(	kIx(
<g/>
I.	I.	kA
sbor	sbor	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c4
ustupující	ustupující	k2eAgFnPc4d1
kolony	kolona	k1gFnPc4
generálů	generál	k1gMnPc2
Dochturova	Dochturův	k2eAgMnSc2d1
a	a	k8xC
Langerona	Langeron	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
generál	generál	k1gMnSc1
Buxhövden	Buxhövdno	k1gNnPc2
vedl	vést	k5eAaImAgMnS
směrem	směr	k1gInSc7
na	na	k7c4
Rychmanov	Rychmanov	k1gInSc4
v	v	k7c6
úzkém	úzký	k2eAgInSc6d1
pruhu	pruh	k1gInSc6
mezi	mezi	k7c7
pravým	pravý	k2eAgInSc7d1
břehem	břeh	k1gInSc7
Litavy	Litava	k1gFnSc2
a	a	k8xC
úpatím	úpatí	k1gNnSc7
Prateckých	pratecký	k2eAgFnPc2d1
výšin	výšina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojenecká	spojenecký	k2eAgFnSc1d1
formace	formace	k1gFnSc1
byla	být	k5eAaImAgFnS
tímto	tento	k3xDgInSc7
úderem	úder	k1gInSc7
rozetnutá	rozetnutý	k2eAgFnSc1d1
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
pouze	pouze	k6eAd1
té	ten	k3xDgFnSc3
menší	malý	k2eAgInPc1d2
pod	pod	k7c7
přímým	přímý	k2eAgNnSc7d1
Buxhövdenovým	Buxhövdenův	k2eAgNnSc7d1
velením	velení	k1gNnSc7
<g/>
,	,	kIx,
zůstala	zůstat	k5eAaPmAgFnS
volná	volný	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ve	v	k7c6
směru	směr	k1gInSc6
ústupu	ústup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
vojákům	voják	k1gMnPc3
Vandammovy	Vandammův	k2eAgFnSc2d1
divize	divize	k1gFnSc2
při	při	k7c6
akci	akce	k1gFnSc6
podařilo	podařit	k5eAaPmAgNnS
obsadit	obsadit	k5eAaPmF
i	i	k9
Újezd	Újezd	k1gInSc4
<g/>
,	,	kIx,
umožňovala	umožňovat	k5eAaImAgFnS
zbytku	zbytek	k1gInSc2
zle	zle	k6eAd1
tísněných	tísněný	k2eAgMnPc2d1
Rusů	Rus	k1gMnPc2
a	a	k8xC
Rakušanů	Rakušan	k1gMnPc2
jedinou	jediný	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
z	z	k7c2
francouzského	francouzský	k2eAgInSc2d1
kotle	kotel	k1gInSc2
pouze	pouze	k6eAd1
úzká	úzký	k2eAgFnSc1d1
hráz	hráz	k1gFnSc1
mezi	mezi	k7c7
zamrzlými	zamrzlý	k2eAgInPc7d1
rybníky	rybník	k1gInPc7
Žatčanským	Žatčanský	k2eAgNnPc3d1
a	a	k8xC
Měnínským	Měnínský	k2eAgNnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
ústupových	ústupový	k2eAgInPc2d1
bojů	boj	k1gInPc2
však	však	k9
na	na	k7c6
hrázi	hráz	k1gFnSc6
vybuchla	vybuchnout	k5eAaPmAgFnS
muniční	muniční	k2eAgFnSc1d1
kára	kára	k1gFnSc1
a	a	k8xC
průchod	průchod	k1gInSc1
po	po	k7c6
souši	souš	k1gFnSc6
se	se	k3xPyFc4
záhy	záhy	k6eAd1
stal	stát	k5eAaPmAgInS
takřka	takřka	k6eAd1
nemožným	možný	k2eNgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
jednotek	jednotka	k1gFnPc2
se	se	k3xPyFc4
proto	proto	k8xC
rozhodla	rozhodnout	k5eAaPmAgFnS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
cestě	cesta	k1gFnSc6
přes	přes	k7c4
led	led	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
Svědkové	svědek	k1gMnPc1
vývoj	vývoj	k1gInSc4
dalších	další	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
popisovali	popisovat	k5eAaImAgMnP
různě	různě	k6eAd1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
nahromadění	nahromadění	k1gNnPc2
dělostřeleckých	dělostřelecký	k2eAgInPc2d1
povozů	povoz	k1gInPc2
a	a	k8xC
masy	masa	k1gFnSc2
lidských	lidský	k2eAgNnPc2d1
a	a	k8xC
koňských	koňský	k2eAgNnPc2d1
těl	tělo	k1gNnPc2
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
snad	snad	k9
i	i	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
dělostřelby	dělostřelba	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c4
ustupující	ustupující	k2eAgNnSc4d1
nařídil	nařídit	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
Napoleon	Napoleon	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
zamrzlá	zamrzlý	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
na	na	k7c6
mnohých	mnohý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
probořila	probořit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
pasti	past	k1gFnSc2
nakonec	nakonec	k6eAd1
uniklo	uniknout	k5eAaPmAgNnS
pouze	pouze	k6eAd1
8000	#num#	k4
mužů	muž	k1gMnPc2
spolu	spolu	k6eAd1
s	s	k7c7
generálem	generál	k1gMnSc7
Dochturovem	Dochturov	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
s	s	k7c7
nastávajícím	nastávající	k2eAgInSc7d1
soumrakem	soumrak	k1gInSc7
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
v	v	k7c4
16.00	16.00	k4
<g/>
,	,	kIx,
nařídil	nařídit	k5eAaPmAgMnS
svým	svůj	k3xOyFgFnPc3
jednotkám	jednotka	k1gFnPc3
zamířit	zamířit	k5eAaPmF
do	do	k7c2
bezpečí	bezpečí	k1gNnSc2
směrem	směr	k1gInSc7
přes	přes	k7c4
Bošovice	Bošovice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
pochodovali	pochodovat	k5eAaImAgMnP
celou	celý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
v	v	k7c6
silném	silný	k2eAgInSc6d1
dešti	dešť	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ničil	ničit	k5eAaImAgInS
cesty	cesta	k1gFnPc4
a	a	k8xC
znemožňoval	znemožňovat	k5eAaImAgInS
přepravu	přeprava	k1gFnSc4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
část	část	k1gFnSc1
uvízla	uvíznout	k5eAaPmAgFnS
v	v	k7c6
bahně	bahno	k1gNnSc6
a	a	k8xC
byla	být	k5eAaImAgFnS
ponechána	ponechat	k5eAaPmNgFnS
na	na	k7c6
trase	trasa	k1gFnSc6
pochodu	pochod	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Už	už	k6eAd1
jsem	být	k5eAaImIp1nS
viděl	vidět	k5eAaImAgMnS
mnohé	mnohé	k1gNnSc4
prohrané	prohraný	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c6
takové	takový	k3xDgFnSc6
porážce	porážka	k1gFnSc6
jsem	být	k5eAaImIp1nS
neměl	mít	k5eNaImAgMnS
ani	ani	k8xC
zdání	zdání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Louis	Louis	k1gMnSc1
Alexandre	Alexandr	k1gInSc5
Andrault	Andrault	k2eAgInSc1d1
de	de	k?
Langeron	Langeron	k1gInSc1
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Důsledky	důsledek	k1gInPc1
bitvy	bitva	k1gFnSc2
</s>
<s>
Antoine-Jean	Antoine-Jean	k1gInSc1
Gros	gros	k1gInSc1
<g/>
:	:	kIx,
Setkání	setkání	k1gNnSc1
Napoleona	Napoleon	k1gMnSc2
a	a	k8xC
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
u	u	k7c2
Spáleného	spálený	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
</s>
<s>
V	v	k7c6
noci	noc	k1gFnSc6
po	po	k7c6
bitvě	bitva	k1gFnSc6
spojenecká	spojenecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nastoupila	nastoupit	k5eAaPmAgFnS
zpáteční	zpáteční	k2eAgInSc4d1
pochod	pochod	k1gInSc4
po	po	k7c6
uherské	uherský	k2eAgFnSc6d1
silnici	silnice	k1gFnSc6
přes	přes	k7c4
Čejč	Čejč	k1gFnSc4
<g/>
,	,	kIx,
Hodonín	Hodonín	k1gInSc1
a	a	k8xC
Holíč	Holíč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1805	#num#	k4
přenocoval	přenocovat	k5eAaPmAgMnS
ve	v	k7c6
Vrchnostenském	vrchnostenský	k2eAgInSc6d1
úředním	úřední	k2eAgInSc6d1
domu	dům	k1gInSc6wR
v	v	k7c6
Žarošicích	Žarošice	k1gFnPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
u	u	k7c2
čtvrtláníka	čtvrtláník	k1gMnSc2
Isidora	Isidor	k1gMnSc2
Valy	Vala	k1gMnSc2
v	v	k7c6
Žarošicích	Žarošice	k1gFnPc6
č.	č.	k?
45	#num#	k4
<g/>
,	,	kIx,
šest	šest	k4xCc4
generálů	generál	k1gMnPc2
na	na	k7c6
žarošické	žarošický	k2eAgFnSc6d1
faře	fara	k1gFnSc6
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Napoleon	napoleon	k1gInSc4
na	na	k7c6
Pozořické	Pozořický	k2eAgFnSc6d1
poště	pošta	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
odkud	odkud	k6eAd1
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
přesídlil	přesídlit	k5eAaPmAgMnS
do	do	k7c2
slavkovského	slavkovský	k2eAgInSc2d1
zámku	zámek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
nadiktoval	nadiktovat	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
proklamaci	proklamace	k1gFnSc4
k	k	k7c3
armádě	armáda	k1gFnSc3
a	a	k8xC
kolem	kolem	k7c2
16.00	16.00	k4
přijal	přijmout	k5eAaPmAgInS
knížete	kníže	k1gNnSc4wR
Jana	Jan	k1gMnSc2
z	z	k7c2
Lichtenštejna	Lichtenštejn	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
do	do	k7c2
Slavkova	Slavkov	k1gInSc2
dorazil	dorazit	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
parlamentář	parlamentář	k1gMnSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
ve	v	k7c6
věci	věc	k1gFnSc6
sjednání	sjednání	k1gNnSc2
separátního	separátní	k2eAgInSc2d1
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pronásledování	pronásledování	k1gNnPc1
protivníka	protivník	k1gMnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
francouzský	francouzský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
nařídil	nařídit	k5eAaPmAgMnS
jezdectvu	jezdectvo	k1gNnSc6
maršála	maršál	k1gMnSc4
Murata	Murat	k1gMnSc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
ve	v	k7c6
výsledku	výsledek	k1gInSc6
neúspěšným	úspěšný	k2eNgMnPc3d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
obě	dva	k4xCgFnPc1
armády	armáda	k1gFnPc1
ztratily	ztratit	k5eAaPmAgFnP
přes	přes	k7c4
noc	noc	k1gFnSc4
vzájemné	vzájemný	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
setkání	setkání	k1gNnSc3
Napoleona	Napoleon	k1gMnSc2
a	a	k8xC
Františka	František	k1gMnSc2
I.	I.	kA
u	u	k7c2
Spáleného	spálený	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
poblíž	poblíž	k7c2
Násedlovic	Násedlovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
panovníci	panovník	k1gMnPc1
dohodli	dohodnout	k5eAaPmAgMnP
na	na	k7c6
příměří	příměří	k1gNnSc6
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
Rakouskem	Rakousko	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
podepsáno	podepsat	k5eAaPmNgNnS
na	na	k7c6
slavkovském	slavkovský	k2eAgInSc6d1
zámku	zámek	k1gInSc6
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mír	mír	k1gInSc1
byl	být	k5eAaImAgInS
uzavřen	uzavřít	k5eAaPmNgInS
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c6
Prešpurku	Prešpurku	k?
(	(	kIx(
<g/>
Prešpurský	prešpurský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko	Rakousko	k1gNnSc1
se	se	k3xPyFc4
muselo	muset	k5eAaImAgNnS
vzdát	vzdát	k5eAaPmF
italských	italský	k2eAgFnPc2d1
a	a	k8xC
německých	německý	k2eAgFnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
připadla	připadnout	k5eAaPmAgFnS
Francii	Francie	k1gFnSc4
nebo	nebo	k8xC
jejím	její	k3xOp3gMnPc3
spojencům	spojenec	k1gMnPc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
celkem	celkem	k6eAd1
63	#num#	k4
000	#num#	k4
km²	km²	k?
území	území	k1gNnSc2
s	s	k7c7
téměř	téměř	k6eAd1
3	#num#	k4
000	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
Tyroly	Tyrol	k1gMnPc4
<g/>
,	,	kIx,
Vorarlbersko	Vorarlbersko	k1gNnSc4
a	a	k8xC
Augsburg	Augsburg	k1gInSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
přešly	přejít	k5eAaPmAgInP
pod	pod	k7c4
Bavorsko	Bavorsko	k1gNnSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
o	o	k7c4
Benátsko	Benátsko	k1gNnSc4
<g/>
,	,	kIx,
Istrii	Istrie	k1gFnSc4
a	a	k8xC
Dalmácii	Dalmácie	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
přešly	přejít	k5eAaPmAgInP
pod	pod	k7c4
Napoleonovo	Napoleonův	k2eAgNnSc4d1
nové	nový	k2eAgNnSc4d1
Italské	italský	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakousko	Rakousko	k1gNnSc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
vzdalo	vzdát	k5eAaPmAgNnS
všech	všecek	k3xTgInPc2
nároků	nárok	k1gInPc2
na	na	k7c4
německé	německý	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
kompenzace	kompenzace	k1gFnPc4
mu	on	k3xPp3gMnSc3
připadlo	připadnout	k5eAaPmAgNnS
Salcbursko	Salcbursko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
smlouvě	smlouva	k1gFnSc6
byly	být	k5eAaImAgInP
také	také	k9
zahrnuty	zahrnout	k5eAaPmNgInP
náhrady	náhrada	k1gFnSc2
Francii	Francie	k1gFnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
40	#num#	k4
miliónů	milión	k4xCgInPc2
franků	frank	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
se	se	k3xPyFc4
britský	britský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
William	William	k1gInSc4
Pitt	Pitt	k1gMnSc1
dozvěděl	dozvědět	k5eAaPmAgMnS
o	o	k7c4
vítězství	vítězství	k1gNnSc4
Napoleona	Napoleon	k1gMnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
poručil	poručit	k5eAaPmAgMnS
svým	svůj	k3xOyFgMnPc3
služebníkům	služebník	k1gMnPc3
„	„	k?
<g/>
svinout	svinout	k5eAaPmF
mapu	mapa	k1gFnSc4
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebudeme	být	k5eNaImBp1nP
ji	on	k3xPp3gFnSc4
příštích	příští	k2eAgNnPc2d1
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
potřebovat	potřebovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Devět	devět	k4xCc4
měsíců	měsíc	k1gInPc2
od	od	k7c2
podpisu	podpis	k1gInSc2
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1806	#num#	k4
<g/>
,	,	kIx,
zanikla	zaniknout	k5eAaPmAgFnS
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
a	a	k8xC
císaři	císař	k1gMnSc3
Františkovi	František	k1gMnSc3
zůstal	zůstat	k5eAaPmAgMnS
pouze	pouze	k6eAd1
titul	titul	k1gInSc4
rakouského	rakouský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgMnPc2d1
se	se	k3xPyFc4
stáhli	stáhnout	k5eAaPmAgMnP
zpět	zpět	k6eAd1
na	na	k7c4
své	svůj	k3xOyFgNnSc4
území	území	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
mírovou	mírový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
Alexandr	Alexandr	k1gMnSc1
s	s	k7c7
Napoleonem	napoleon	k1gInSc7
uzavřít	uzavřít	k5eAaPmF
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
jsem	být	k5eAaImIp1nS
s	s	k7c7
vámi	vy	k3xPp2nPc7
spokojen	spokojen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slavkovském	slavkovský	k2eAgInSc6d1
dni	den	k1gInSc6
jste	být	k5eAaImIp2nP
dostáli	dostát	k5eAaPmAgMnP
všemu	všecek	k3xTgMnSc3
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
jsem	být	k5eAaImIp1nS
od	od	k7c2
vaší	váš	k3xOp2gFnSc2
neohroženosti	neohroženost	k1gFnSc2
očekával	očekávat	k5eAaImAgInS
<g/>
;	;	kIx,
svoje	svůj	k3xOyFgFnPc4
orlice	orlice	k1gFnPc4
jste	být	k5eAaImIp2nP
ověnčili	ověnčit	k5eAaPmAgMnP
nesmrtelnou	smrtelný	k2eNgFnSc7d1
slávou	sláva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
o	o	k7c4
100	#num#	k4
000	#num#	k4
mužích	muž	k1gMnPc6
pod	pod	k7c7
velením	velení	k1gNnSc7
císařů	císař	k1gMnPc2
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Rakouska	Rakousko	k1gNnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
méně	málo	k6eAd2
než	než	k8xS
čtyřech	čtyři	k4xCgMnPc6
hodinách	hodina	k1gFnPc6
buď	buď	k8xC
rozetnuta	rozetnut	k2eAgFnSc1d1
nebo	nebo	k8xC
rozprášena	rozprášen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k8xS
neuniklo	uniknout	k5eNaPmAgNnS
vašim	váš	k3xOp2gFnPc3
ostřím	ostří	k1gNnSc7
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
utopilo	utopit	k5eAaPmAgNnS
v	v	k7c6
jezerech	jezero	k1gNnPc6
<g/>
,	,	kIx,
čtyřicet	čtyřicet	k4xCc4
praporů	prapor	k1gInPc2
<g/>
,	,	kIx,
zástavy	zástava	k1gFnSc2
ruské	ruský	k2eAgFnSc2d1
imperátorské	imperátorský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
,	,	kIx,
sto	sto	k4xCgNnSc1
dvacet	dvacet	k4xCc1
kusů	kus	k1gInPc2
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
dvacet	dvacet	k4xCc1
generálů	generál	k1gMnPc2
<g/>
,	,	kIx,
přes	přes	k7c4
30	#num#	k4
000	#num#	k4
zajatců	zajatec	k1gMnPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
výsledek	výsledek	k1gInSc1
dne	den	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
sláva	sláva	k1gFnSc1
nikdy	nikdy	k6eAd1
nepohasne	pohasnout	k5eNaPmIp3nS
<g/>
.	.	kIx.
...	...	k?
Vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
až	až	k6eAd1
bude	být	k5eAaImBp3nS
završeno	završen	k2eAgNnSc1d1
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
pro	pro	k7c4
blaho	blaho	k1gNnSc4
a	a	k8xC
rozkvět	rozkvět	k1gInSc4
naší	náš	k3xOp1gFnSc2
vlasti	vlast	k1gFnSc2
<g/>
,	,	kIx,
odvedu	odvést	k5eAaPmIp1nS
vás	vy	k3xPp2nPc4
zpět	zpět	k6eAd1
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
;	;	kIx,
budu	být	k5eAaImBp1nS
tam	tam	k6eAd1
o	o	k7c4
vás	vy	k3xPp2nPc4
s	s	k7c7
láskou	láska	k1gFnSc7
pečovat	pečovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můj	můj	k1gMnSc1
lid	lid	k1gInSc1
vás	vy	k3xPp2nPc4
pozdraví	pozdravit	k5eAaPmIp3nS
s	s	k7c7
radostí	radost	k1gFnSc7
a	a	k8xC
vám	vy	k3xPp2nPc3
postačí	postačit	k5eAaPmIp3nS
říci	říct	k5eAaPmF
<g/>
:	:	kIx,
,	,	kIx,
<g/>
Byl	být	k5eAaImAgInS
jsem	být	k5eAaImIp1nS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
’	’	k?
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
odpověděli	odpovědět	k5eAaPmAgMnP
<g/>
:	:	kIx,
,	,	kIx,
<g/>
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
hrdina	hrdina	k1gMnSc1
<g/>
.	.	kIx.
<g/>
’	’	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Napoleonova	Napoleonův	k2eAgFnSc1d1
proklamace	proklamace	k1gFnSc1
k	k	k7c3
armádě	armáda	k1gFnSc3
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1805	#num#	k4
(	(	kIx(
<g/>
část	část	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mrtví	mrtvý	k1gMnPc1
z	z	k7c2
bitvy	bitva	k1gFnSc2
byli	být	k5eAaImAgMnP
pohřbeni	pohřbít	k5eAaPmNgMnP
do	do	k7c2
dvaadvaceti	dvaadvacet	k4xCc2
masových	masový	k2eAgInPc2d1
hrobů	hrob	k1gInPc2
přímo	přímo	k6eAd1
na	na	k7c6
bojišti	bojiště	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
ranění	ranění	k1gNnSc6
byli	být	k5eAaImAgMnP
odváženi	odvážit	k5eAaPmNgMnP,k5eAaImNgMnP
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
mimo	mimo	k7c4
nemocnice	nemocnice	k1gFnPc4
umisťováni	umisťován	k2eAgMnPc1d1
i	i	k9
do	do	k7c2
provizorních	provizorní	k2eAgFnPc2d1
ošetřoven	ošetřovna	k1gFnPc2
zřízených	zřízený	k2eAgFnPc2d1
z	z	k7c2
kostelů	kostel	k1gInPc2
<g/>
,	,	kIx,
továren	továrna	k1gFnPc2
a	a	k8xC
soukromých	soukromý	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
zranění	zraněný	k2eAgMnPc1d1
rakouští	rakouský	k2eAgMnPc1d1
a	a	k8xC
ruští	ruský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
byli	být	k5eAaImAgMnP
převezeni	převézt	k5eAaPmNgMnP
do	do	k7c2
lazaretu	lazaret	k1gInSc2
v	v	k7c6
zámečku	zámeček	k1gInSc6
rodu	rod	k1gInSc2
Podstatských	Podstatský	k2eAgMnPc2d1
v	v	k7c6
obci	obec	k1gFnSc6
Veselíčko	veselíčko	k1gNnSc1
u	u	k7c2
Přerova	Přerov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
dnech	den	k1gInPc6
po	po	k7c6
bitvě	bitva	k1gFnSc6
propukla	propuknout	k5eAaPmAgFnS
epidemie	epidemie	k1gFnSc1
tyfu	tyf	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
vyžádala	vyžádat	k5eAaPmAgNnP
mnoho	mnoho	k6eAd1
obětí	oběť	k1gFnSc7
mezi	mezi	k7c7
vojáky	voják	k1gMnPc7
i	i	k8xC
civilním	civilní	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Poddaní	poddaný	k1gMnPc1
z	z	k7c2
vesnic	vesnice	k1gFnPc2
okolo	okolo	k7c2
bojiště	bojiště	k1gNnSc2
byli	být	k5eAaImAgMnP
nahnáni	nahnán	k2eAgMnPc1d1
na	na	k7c6
kopání	kopání	k1gNnSc6
hromadných	hromadný	k2eAgInPc2d1
hrobů	hrob	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochovávali	pochovávat	k5eAaImAgMnP
padlé	padlý	k2eAgNnSc4d1
po	po	k7c6
desítkách	desítka	k1gFnPc6
a	a	k8xC
často	často	k6eAd1
prý	prý	k9
zahrabávali	zahrabávat	k5eAaImAgMnP
ruské	ruský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
ještě	ještě	k6eAd1
jevili	jevit	k5eAaImAgMnP
známky	známka	k1gFnPc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutily	nutit	k5eAaImAgInP
je	být	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
francouzské	francouzský	k2eAgFnPc1d1
hlídky	hlídka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
dohlížely	dohlížet	k5eAaImAgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pohřbívání	pohřbívání	k1gNnSc1
probíhalo	probíhat	k5eAaImAgNnS
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
nejrychleji	rychle	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
bojištěm	bojiště	k1gNnSc7
visela	viset	k5eAaImAgFnS
hrozba	hrozba	k1gFnSc1
epidemie	epidemie	k1gFnSc1
a	a	k8xC
s	s	k7c7
pohřbíváním	pohřbívání	k1gNnSc7
se	se	k3xPyFc4
nesmělo	smět	k5eNaImAgNnS
otálet	otálet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
tento	tento	k3xDgInSc4
spěch	spěch	k1gInSc4
bylo	být	k5eAaImAgNnS
ještě	ještě	k9
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
na	na	k7c6
bitevním	bitevní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
přes	přes	k7c4
sto	sto	k4xCgNnSc4
nepohřbených	pohřbený	k2eNgFnPc2d1
mrtvol	mrtvola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgInSc2
dne	den	k1gInSc2
našli	najít	k5eAaPmAgMnP
pod	pod	k7c7
širým	širý	k2eAgNnSc7d1
nebem	nebe	k1gNnSc7
ještě	ještě	k9
jedenáct	jedenáct	k4xCc1
raněných	raněný	k1gMnPc2
Rusů	Rus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Brně	Brno	k1gNnSc6
šla	jít	k5eAaImAgFnS
řeč	řeč	k1gFnSc1
také	také	k9
o	o	k7c6
čtyřech	čtyři	k4xCgMnPc6
ruských	ruský	k2eAgMnPc6d1
vojácích	voják	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
nohy	noha	k1gFnPc4
a	a	k8xC
s	s	k7c7
vypětím	vypětí	k1gNnSc7
posledních	poslední	k2eAgFnPc2d1
sil	síla	k1gFnPc2
se	se	k3xPyFc4
doplazili	doplazit	k5eAaPmAgMnP
ze	z	k7c2
Sokolnic	sokolnice	k1gFnPc2
do	do	k7c2
Tuřan	Tuřany	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k9
dobrých	dobrý	k2eAgFnPc2d1
šest	šest	k4xCc1
až	až	k9
sedm	sedm	k4xCc1
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
jen	jen	k9
aby	aby	kYmCp3nP
byli	být	k5eAaImAgMnP
blíže	blízce	k6eAd2
městu	město	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
–	–	k?
jak	jak	k6eAd1
věřili	věřit	k5eAaImAgMnP
–	–	k?
najdou	najít	k5eAaPmIp3nP
přece	přece	k9
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Dušan	Dušan	k1gMnSc1
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Brněnský	brněnský	k2eAgInSc1d1
pitaval	pitaval	k1gInSc1
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slavkovské	slavkovský	k2eAgFnPc1d1
legendy	legenda	k1gFnPc1
</s>
<s>
Polidor	Polidor	k1gInSc1
Babaev	Babaev	k1gFnSc1
<g/>
:	:	kIx,
umírající	umírající	k1gFnSc1
poddůstojník	poddůstojník	k1gMnSc1
Stačikov	Stačikov	k1gInSc1
<g/>
,	,	kIx,
předávající	předávající	k2eAgNnPc1d1
spolubojovníkovi	spolubojovník	k1gMnSc3
zástavu	zástava	k1gFnSc4
pluku	pluk	k1gInSc2
Azovských	azovský	k2eAgMnPc2d1
mušketýrů	mušketýr	k1gMnPc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
ukryl	ukrýt	k5eAaPmAgMnS
na	na	k7c6
hrudi	hruď	k1gFnSc6
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jej	on	k3xPp3gNnSc4
mezi	mezi	k7c7
Telnicí	Telnice	k1gFnSc7
a	a	k8xC
Sokolnicemi	sokolnice	k1gFnPc7
zajímali	zajímat	k5eAaImAgMnP
Francouzi	Francouz	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prapor	prapor	k1gInSc1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
uchovat	uchovat	k5eAaPmF
a	a	k8xC
dopravit	dopravit	k5eAaPmF
zpět	zpět	k6eAd1
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
epizod	epizoda	k1gFnPc2
bitvy	bitva	k1gFnSc2
se	se	k3xPyFc4
ustupující	ustupující	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
pod	pod	k7c7
velením	velení	k1gNnSc7
generála	generál	k1gMnSc2
Dochturova	Dochturův	k2eAgInSc2d1
pokusily	pokusit	k5eAaPmAgFnP
přejít	přejít	k5eAaPmF
zamrzlé	zamrzlý	k2eAgFnPc1d1
hladiny	hladina	k1gFnPc1
rybníků	rybník	k1gInPc2
Žatčanského	Žatčanský	k2eAgNnSc2d1
a	a	k8xC
Měnínského	Měnínský	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Led	led	k1gInSc4
však	však	k9
nevydržel	vydržet	k5eNaPmAgMnS
zátěž	zátěž	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
masy	masa	k1gFnPc1
prchajících	prchající	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
a	a	k8xC
početné	početný	k2eAgInPc1d1
dělostřelecké	dělostřelecký	k2eAgInPc1d1
povozy	povoz	k1gInPc1
představovaly	představovat	k5eAaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
za	za	k7c2
dramatických	dramatický	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
se	se	k3xPyFc4
probořil	probořit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
tragédii	tragédie	k1gFnSc6
<g/>
,	,	kIx,
za	za	k7c2
níž	jenž	k3xRgFnSc2
údajně	údajně	k6eAd1
stála	stát	k5eAaImAgFnS
dělostřelba	dělostřelba	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
nařídil	nařídit	k5eAaPmAgInS
Napoleon	napoleon	k1gInSc1
od	od	k7c2
kaple	kaple	k1gFnSc2
sv.	sv.	kA
Antonína	Antonín	k1gMnSc4
Paduánského	paduánský	k2eAgMnSc4d1
nad	nad	k7c7
Újezdem	Újezd	k1gInSc7
<g/>
,	,	kIx,
prý	prý	k9
zahynuly	zahynout	k5eAaPmAgInP
tisíce	tisíc	k4xCgInPc1
ruských	ruský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
za	za	k7c7
střetnutím	střetnutí	k1gNnSc7
učinili	učinit	k5eAaImAgMnP,k5eAaPmAgMnP
jakousi	jakýsi	k3yIgFnSc4
osudovou	osudový	k2eAgFnSc4d1
tečku	tečka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypodobnění	vypodobnění	k1gNnSc1
této	tento	k3xDgFnSc2
legendy	legenda	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgMnS
zejména	zejména	k9
sám	sám	k3xTgMnSc1
francouzský	francouzský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
svému	svůj	k3xOyFgMnSc3
vítězství	vítězství	k1gNnSc4
dodat	dodat	k5eAaPmF
ještě	ještě	k6eAd1
většího	veliký	k2eAgInSc2d2
lesku	lesk	k1gInSc2
a	a	k8xC
v	v	k7c6
podobném	podobný	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
pak	pak	k9
o	o	k7c6
události	událost	k1gFnSc6
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
memoárech	memoáry	k1gInPc6
hovoří	hovořit	k5eAaImIp3nS
i	i	k9
řada	řada	k1gFnSc1
pamětníků	pamětník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
Výjimkou	výjimka	k1gFnSc7
nebyl	být	k5eNaImAgMnS
ani	ani	k8xC
pozdější	pozdní	k2eAgMnSc1d2
generál	generál	k1gMnSc1
Marcellin	Marcellina	k1gFnPc2
de	de	k?
Marbot	Marbot	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
Pamětech	paměť	k1gFnPc6
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
<g/>
Konečně	konečně	k6eAd1
největší	veliký	k2eAgInSc1d3
počet	počet	k1gInSc1
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
hledal	hledat	k5eAaImAgInS
přechod	přechod	k1gInSc1
po	po	k7c6
ledě	led	k1gInSc6
rybníků	rybník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
silný	silný	k2eAgInSc1d1
a	a	k8xC
pět	pět	k4xCc1
či	či	k8xC
šest	šest	k4xCc1
tisíc	tisíc	k4xCgInSc1
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
zachovávajících	zachovávající	k2eAgMnPc2d1
zbytek	zbytek	k1gInSc4
kázně	kázeň	k1gFnSc2
<g/>
,	,	kIx,
prošlo	projít	k5eAaPmAgNnS
do	do	k7c2
půli	půle	k1gFnSc3
Žatčanského	Žatčanský	k2eAgInSc2d1
rybníku	rybník	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
Napoleon	Napoleon	k1gMnSc1
povolal	povolat	k5eAaPmAgMnS
dělostřelce	dělostřelec	k1gMnPc4
gardy	garda	k1gFnSc2
a	a	k8xC
přikázal	přikázat	k5eAaPmAgInS
střílet	střílet	k5eAaImF
kulemi	kule	k1gFnPc7
na	na	k7c4
led	led	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
protrhl	protrhnout	k5eAaPmAgMnS
a	a	k8xC
bylo	být	k5eAaImAgNnS
slyšet	slyšet	k5eAaImF
obrovské	obrovský	k2eAgNnSc4d1
praskání	praskání	k1gNnSc4
<g/>
...	...	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
pronikající	pronikající	k2eAgFnSc2d1
trhlinami	trhlina	k1gFnPc7
brzy	brzy	k6eAd1
zaplavila	zaplavit	k5eAaPmAgFnS
led	led	k1gInSc4
a	a	k8xC
my	my	k3xPp1nPc1
viděli	vidět	k5eAaImAgMnP
tisíce	tisíc	k4xCgInSc2
Rusů	Rus	k1gMnPc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jejich	jejich	k3xOp3gMnPc4
četné	četný	k2eAgMnPc4d1
koně	kůň	k1gMnPc4
<g/>
,	,	kIx,
děla	dělo	k1gNnPc1
a	a	k8xC
povozy	povoz	k1gInPc1
mizet	mizet	k5eAaImF
zvolna	zvolna	k6eAd1
v	v	k7c6
propasti	propast	k1gFnSc6
<g/>
...	...	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
V	v	k7c6
okamžiku	okamžik	k1gInSc6
se	se	k3xPyFc4
hladina	hladina	k1gFnSc1
rybníka	rybník	k1gInSc2
pokryla	pokrýt	k5eAaPmAgFnS
vším	všecek	k3xTgNnSc7
<g/>
,	,	kIx,
co	co	k8xS
mohlo	moct	k5eAaImAgNnS
a	a	k8xC
umělo	umět	k5eAaImAgNnS
plavat	plavat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
<g/>
,	,	kIx,
ve	v	k7c6
velmi	velmi	k6eAd1
malém	malý	k2eAgInSc6d1
počtu	počet	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
dokázali	dokázat	k5eAaPmAgMnP
zachránit	zachránit	k5eAaPmF
s	s	k7c7
pomocí	pomoc	k1gFnSc7
bidel	bidlo	k1gNnPc2
a	a	k8xC
provazů	provaz	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jim	on	k3xPp3gMnPc3
naši	náš	k3xOp1gFnSc4
vojáci	voják	k1gMnPc1
nastavovali	nastavovat	k5eAaImAgMnP
do	do	k7c2
břehů	břeh	k1gInPc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
utopila	utopit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Marcellin	Marcellin	k1gInSc1
de	de	k?
Marbot	Marbot	k1gInSc1
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
tímto	tento	k3xDgNnSc7
tvrzením	tvrzení	k1gNnSc7
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
v	v	k7c6
rozporu	rozpor	k1gInSc6
zpráva	zpráva	k1gFnSc1
vrchního	vrchní	k2eAgNnSc2d1
chrlického	chrlický	k2eAgNnSc2d1
panství	panství	k1gNnSc2
Brutmanna	Brutmann	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ve	v	k7c6
věci	věc	k1gFnSc6
informuje	informovat	k5eAaBmIp3nS
inspekci	inspekce	k1gFnSc4
arcibiskupských	arcibiskupský	k2eAgInPc2d1
statků	statek	k1gInPc2
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
relace	relace	k1gFnSc2
byl	být	k5eAaImAgInS
Žatčanský	Žatčanský	k2eAgInSc1d1
rybník	rybník	k1gInSc1
na	na	k7c4
výslovný	výslovný	k2eAgInSc4d1
Napoleonův	Napoleonův	k2eAgInSc4d1
rozkaz	rozkaz	k1gInSc4
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
francouzských	francouzský	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
mezi	mezi	k7c7
8	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
16	#num#	k4
<g/>
.	.	kIx.
prosincem	prosinec	k1gInSc7
vypuštěn	vypustit	k5eAaPmNgInS
a	a	k8xC
nebylo	být	k5eNaImAgNnS
v	v	k7c6
něm	on	k3xPp3gNnSc6
nalezeno	nalezen	k2eAgNnSc1d1
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
než	než	k8xS
dva	dva	k4xCgMnPc1
utonulí	utonulý	k2eAgMnPc1d1
ruští	ruský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
180	#num#	k4
dělostřeleckých	dělostřelecký	k2eAgMnPc2d1
koní	kůň	k1gMnPc2
a	a	k8xC
18	#num#	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
152	#num#	k4
<g/>
]	]	kIx)
Legendu	legenda	k1gFnSc4
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
definitivě	definitiva	k1gFnSc6
vyvrátila	vyvrátit	k5eAaPmAgFnS
studie	studie	k1gFnSc1
brněnského	brněnský	k2eAgMnSc2d1
kněze	kněz	k1gMnSc2
a	a	k8xC
profesora	profesor	k1gMnSc2
Aloise	Alois	k1gMnSc4
Slováka	Slovák	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
pro	pro	k7c4
podporu	podpora	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
tvrzení	tvrzení	k1gNnSc2
shromáždil	shromáždit	k5eAaPmAgInS
všechny	všechen	k3xTgInPc4
důležité	důležitý	k2eAgInPc4d1
vrchnostenské	vrchnostenský	k2eAgInPc4d1
zápisy	zápis	k1gInPc4
a	a	k8xC
kronikářské	kronikářský	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Měnínském	Měnínský	k2eAgInSc6d1
rybníku	rybník	k1gInSc6
podle	podle	k7c2
zprávy	zpráva	k1gFnSc2
panství	panství	k1gNnSc1
Židlochovického	židlochovický	k2eAgNnSc2d1
ani	ani	k8xC
jediný	jediný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
ani	ani	k8xC
kůň	kůň	k1gMnSc1
nezahynul	zahynout	k5eNaPmAgMnS
<g/>
,	,	kIx,
ba	ba	k9
ani	ani	k8xC
jaké	jaký	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
kolo	kolo	k1gNnSc1
od	od	k7c2
vozu	vůz	k1gInSc2
nalezeno	nalezen	k2eAgNnSc1d1
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
v	v	k7c6
7.00	7.00	k4
ráno	ráno	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
hlavním	hlavní	k2eAgInSc6d1
stanu	stan	k1gInSc6
jižně	jižně	k6eAd1
pod	pod	k7c7
Žuráněm	Žuráň	k1gMnSc7
konala	konat	k5eAaImAgFnS
schůzka	schůzka	k1gFnSc1
velitelů	velitel	k1gMnPc2
francouzských	francouzský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesně	přesně	k6eAd1
v	v	k7c4
půl	půl	k1xP
osmé	osmý	k4xOgFnSc2
se	se	k3xPyFc4
nad	nad	k7c7
ustupující	ustupující	k2eAgFnSc7d1
mlhou	mlha	k1gFnSc7
na	na	k7c6
východním	východní	k2eAgInSc6d1
obzoru	obzor	k1gInSc6
objevil	objevit	k5eAaPmAgInS
rudý	rudý	k2eAgInSc1d1
kotouč	kotouč	k1gInSc1
slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
zůstal	zůstat	k5eAaPmAgMnS
viset	viset	k5eAaImF
nad	nad	k7c7
Slavkovem	Slavkov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
okamžik	okamžik	k1gInSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
pojmem	pojem	k1gInSc7
a	a	k8xC
ode	ode	k7c2
dne	den	k1gInSc2
bitvy	bitva	k1gFnSc2
bude	být	k5eAaImBp3nS
Napoleon	napoleon	k1gInSc1
spojovat	spojovat	k5eAaImF
Slavkovské	slavkovský	k2eAgNnSc4d1
slunce	slunce	k1gNnSc4
(	(	kIx(
<g/>
le	le	k?
Soleil	Soleil	k1gMnSc1
ďAusterlitz	ďAusterlitz	k1gMnSc1
<g/>
)	)	kIx)
s	s	k7c7
nimbem	nimb	k1gInSc7
vlastních	vlastní	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Část	část	k1gFnSc1
zdi	zeď	k1gFnSc2
sokolnické	sokolnický	k2eAgFnSc2d1
bažantnice	bažantnice	k1gFnSc2
s	s	k7c7
vyznačením	vyznačení	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
údajně	údajně	k6eAd1
umístěno	umístit	k5eAaPmNgNnS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
francouzských	francouzský	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
bitvou	bitva	k1gFnSc7
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
zmiňovaná	zmiňovaný	k2eAgFnSc1d1
legenda	legenda	k1gFnSc1
o	o	k7c6
boji	boj	k1gInSc6
v	v	k7c6
sokolnické	sokolnický	k2eAgFnSc6d1
bažantnici	bažantnice	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
údajně	údajně	k6eAd1
bránili	bránit	k5eAaImAgMnP
korsičtí	korsický	k2eAgMnPc1d1
tirailleři	tirailler	k1gMnPc1
a	a	k8xC
francouzská	francouzský	k2eAgFnSc1d1
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
baterie	baterie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
cihlové	cihlový	k2eAgFnSc6d1
zdi	zeď	k1gFnSc6
okolo	okolo	k7c2
objektu	objekt	k1gInSc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
směru	směr	k1gInSc6
na	na	k7c4
Pratecké	pratecký	k2eAgNnSc4d1
návrší	návrší	k1gNnPc4
dodnes	dodnes	k6eAd1
vyznačeny	vyznačen	k2eAgFnPc4d1
proluky	proluka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
údajně	údajně	k6eAd1
dělostřelci	dělostřelec	k1gMnPc1
vytvořili	vytvořit	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zde	zde	k6eAd1
mohli	moct	k5eAaImAgMnP
umístit	umístit	k5eAaPmF
své	svůj	k3xOyFgFnPc4
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
žádná	žádný	k3yNgFnSc1
z	z	k7c2
oficiálních	oficiální	k2eAgFnPc2d1
relací	relace	k1gFnPc2
o	o	k7c6
bitvě	bitva	k1gFnSc6
artilerii	artilerie	k1gFnSc6
v	v	k7c6
bažantnici	bažantnice	k1gFnSc6
nezmiňuje	zmiňovat	k5eNaImIp3nS
<g/>
,	,	kIx,
takticky	takticky	k6eAd1
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
takové	takový	k3xDgNnSc1
využití	využití	k1gNnSc1
děl	dít	k5eAaBmAgInS,k5eAaImAgInS
bez	bez	k7c2
logiky	logika	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
doloženo	doložen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1805	#num#	k4
žádná	žádný	k3yNgFnSc1
zeď	zeď	k1gFnSc1
z	z	k7c2
této	tento	k3xDgFnSc2
strany	strana	k1gFnSc2
bažantnice	bažantnice	k1gFnSc2
nestála	stát	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
korsičtí	korsický	k2eAgMnPc1d1
tirailleři	tirailler	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
té	ten	k3xDgFnSc6
době	době	k6eAd1
dislokovaní	dislokovaný	k2eAgMnPc1d1
u	u	k7c2
Kobylnic	Kobylnice	k1gFnPc2
a	a	k8xC
jediná	jediný	k2eAgNnPc1d1
dvě	dva	k4xCgNnPc1
děla	dělo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yRgNnPc4,k3yIgNnPc4
měli	mít	k5eAaImAgMnP
obránci	obránce	k1gMnPc1
Sokolnic	sokolnice	k1gFnPc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
stála	stát	k5eAaImAgFnS
na	na	k7c6
výšinách	výšina	k1gFnPc6
za	za	k7c7
vesnicí	vesnice	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
nočních	noční	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
obdržel	obdržet	k5eAaPmAgMnS
Napoleon	Napoleon	k1gMnSc1
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mezi	mezi	k7c7
Telnicí	Telnice	k1gFnSc7
a	a	k8xC
Újezdem	Újezd	k1gInSc7
objevilo	objevit	k5eAaPmAgNnS
větší	veliký	k2eAgNnSc1d2
uskupení	uskupení	k1gNnSc1
nepřátelského	přátelský	k2eNgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
proto	proto	k8xC
osobně	osobně	k6eAd1
na	na	k7c4
průzkum	průzkum	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Prace	Prace	k1gFnSc2
jej	on	k3xPp3gMnSc4
málem	málem	k6eAd1
zajal	zajmout	k5eAaPmAgMnS
oddíl	oddíl	k1gInSc4
kozáků	kozák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
oddanosti	oddanost	k1gFnSc3
myslivců	myslivec	k1gMnPc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
doprovodu	doprovod	k1gInSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
bezpečně	bezpečně	k6eAd1
najít	najít	k5eAaPmF
cestu	cesta	k1gFnSc4
do	do	k7c2
francouzského	francouzský	k2eAgNnSc2d1
ležení	ležení	k1gNnSc2
nedaleko	nedaleko	k7c2
Jiříkovic	Jiříkovice	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jej	on	k3xPp3gMnSc4
začaly	začít	k5eAaPmAgFnP
bouřlivě	bouřlivě	k6eAd1
vítat	vítat	k5eAaImF
masy	masa	k1gFnPc4
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnPc7
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
zapalovali	zapalovat	k5eAaImAgMnP
louče	louč	k1gFnPc4
nebo	nebo	k8xC
věchýtky	věchýtka	k1gFnPc4
slámy	sláma	k1gFnSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
podobným	podobný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
rozsvítila	rozsvítit	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
v	v	k7c6
jiříkovickém	jiříkovický	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
a	a	k8xC
posléze	posléze	k6eAd1
i	i	k9
celá	celý	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
linie	linie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
měla	mít	k5eAaImAgFnS
připadnout	připadnout	k5eAaPmF
na	na	k7c4
den	den	k1gInSc4
prvního	první	k4xOgNnSc2
výročí	výročí	k1gNnSc2
Napoleonovy	Napoleonův	k2eAgFnSc2d1
korunovace	korunovace	k1gFnSc2
<g/>
,	,	kIx,
zavládlo	zavládnout	k5eAaPmAgNnS
proto	proto	k8xC
všeobecné	všeobecný	k2eAgNnSc4d1
nadšení	nadšení	k1gNnSc4
a	a	k8xC
tisíce	tisíc	k4xCgInPc1
vojáků	voják	k1gMnPc2
začaly	začít	k5eAaPmAgInP
císařovi	císařův	k2eAgMnPc1d1
spontánně	spontánně	k6eAd1
provolávat	provolávat	k5eAaImF
slávu	sláva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiříkovické	Jiříkovický	k2eAgInPc1d1
ohně	oheň	k1gInPc1
se	se	k3xPyFc4
posléze	posléze	k6eAd1
objevily	objevit	k5eAaPmAgFnP
nejen	nejen	k6eAd1
v	v	k7c6
memoárech	memoáry	k1gInPc6
mnoha	mnoho	k4c2
účastníků	účastník	k1gMnPc2
slavkovské	slavkovský	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
staly	stát	k5eAaPmAgFnP
se	se	k3xPyFc4
i	i	k9
jednou	jeden	k4xCgFnSc7
z	z	k7c2
místních	místní	k2eAgFnPc2d1
<g/>
,	,	kIx,
každoročně	každoročně	k6eAd1
upomínaných	upomínaný	k2eAgFnPc2d1
<g/>
,	,	kIx,
tradic	tradice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
drželi	držet	k5eAaImAgMnP
pozice	pozice	k1gFnPc4
na	na	k7c6
Santonu	Santon	k1gInSc6
<g/>
,	,	kIx,
před	před	k7c7
bitvou	bitva	k1gFnSc7
rozebrali	rozebrat	k5eAaPmAgMnP
mariánskou	mariánský	k2eAgFnSc4d1
kapli	kaple	k1gFnSc4
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
vrcholu	vrchol	k1gInSc6
a	a	k8xC
materiál	materiál	k1gInSc4
použili	použít	k5eAaPmAgMnP
na	na	k7c4
zpevnění	zpevnění	k1gNnSc4
okopů	okop	k1gInPc2
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgMnPc2,k3yRgMnPc2,k3yIgMnPc2
umístili	umístit	k5eAaPmAgMnP
svá	svůj	k3xOyFgNnPc4
děla	dělo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřevěné	dřevěný	k2eAgFnPc4d1
části	část	k1gFnPc4
a	a	k8xC
příslušenství	příslušenství	k1gNnSc1
jim	on	k3xPp3gMnPc3
pak	pak	k6eAd1
v	v	k7c6
chladné	chladný	k2eAgFnSc6d1
prosincové	prosincový	k2eAgFnSc6d1
noci	noc	k1gFnSc6
posloužily	posloužit	k5eAaPmAgInP
jako	jako	k9
topivo	topivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
těchto	tento	k3xDgFnPc2
věcí	věc	k1gFnPc2
byla	být	k5eAaImAgFnS
i	i	k9
soška	soška	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
stávala	stávat	k5eAaImAgFnS
uvnitř	uvnitř	k7c2
kaple	kaple	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Traduje	tradovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
plameny	plamen	k1gInPc1
ohně	oheň	k1gInSc2
se	se	k3xPyFc4
této	tento	k3xDgFnSc2
sošky	soška	k1gFnSc2
ani	ani	k8xC
nedotkly	dotknout	k5eNaPmAgFnP
a	a	k8xC
soška	soška	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
neporušená	porušený	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
proč	proč	k6eAd1
byla	být	k5eAaImAgFnS
kaplička	kaplička	k1gFnSc1
znovu	znovu	k6eAd1
vystavěna	vystavěn	k2eAgFnSc1d1
a	a	k8xC
nějaký	nějaký	k3yIgInSc4
čas	čas	k1gInSc4
byla	být	k5eAaImAgFnS
i	i	k9
cílem	cíl	k1gInSc7
poutí	poutí	k1gNnSc2
k	k	k7c3
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
okolí	okolí	k1gNnSc6
Slavkova	Slavkov	k1gInSc2
se	se	k3xPyFc4
stále	stále	k6eAd1
drží	držet	k5eAaImIp3nS
pověst	pověst	k1gFnSc1
o	o	k7c6
ruském	ruský	k2eAgInSc6d1
pokladu	poklad	k1gInSc6
zakopaném	zakopaný	k2eAgInSc6d1
Preobraženským	Preobraženský	k2eAgInSc7d1
plukem	pluk	k1gInSc7
za	za	k7c2
ústupu	ústup	k1gInSc2
z	z	k7c2
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
dost	dost	k6eAd1
hledačů	hledač	k1gInPc2
pokladů	poklad	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
ochotni	ochoten	k2eAgMnPc1d1
kopat	kopat	k5eAaImF
v	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
bojiště	bojiště	k1gNnSc2
v	v	k7c6
naději	naděje	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
poklad	poklad	k1gInSc4
naleznou	naleznout	k5eAaPmIp3nP,k5eAaBmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Památníky	památník	k1gInPc1
spojené	spojený	k2eAgInPc1d1
s	s	k7c7
bitvou	bitva	k1gFnSc7
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
památek	památka	k1gFnPc2
Slavkovského	slavkovský	k2eAgNnSc2d1
bojiště	bojiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
následujících	následující	k2eAgNnPc6d1
po	po	k7c6
bitvě	bitva	k1gFnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
okolí	okolí	k1gNnSc6
postižených	postižený	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
zřízena	zřízen	k2eAgFnSc1d1
řada	řada	k1gFnSc1
památníků	památník	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
připomínat	připomínat	k5eAaImF
jak	jak	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc4d1
epizody	epizoda	k1gFnPc4
střetnutí	střetnutí	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k9
tisíce	tisíc	k4xCgInPc4
jejich	jejich	k3xOp3gFnPc2
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nejvyšším	vysoký	k2eAgInSc6d3
bodě	bod	k1gInSc6
slavkovského	slavkovský	k2eAgNnSc2d1
bojiště	bojiště	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Prackého	Pracký	k2eAgInSc2d1
kopce	kopec	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
českých	český	k2eAgMnPc2d1
vlastenců	vlastenec	k1gMnPc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
kněze	kněz	k1gMnSc2
Aloise	Alois	k1gMnSc2
Slováka	Slovák	k1gMnSc2
v	v	k7c6
letech	let	k1gInPc6
1910	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
vystavěn	vystavěn	k2eAgInSc4d1
první	první	k4xOgInSc4
mírový	mírový	k2eAgInSc4d1
památník	památník	k1gInSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
Mohyla	mohyla	k1gFnSc1
míru	mír	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gNnSc1
otevření	otevření	k1gNnSc1
naplánované	naplánovaný	k2eAgNnSc1d1
na	na	k7c4
rok	rok	k1gInSc4
1914	#num#	k4
paradoxně	paradoxně	k6eAd1
oddálila	oddálit	k5eAaPmAgFnS
první	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
slavnostní	slavnostní	k2eAgInSc1d1
ceremoniál	ceremoniál	k1gInSc1
tedy	tedy	k8xC
proběhl	proběhnout	k5eAaPmAgInS
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1923	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
Pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
také	také	k9
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Žuráně	Žurán	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
symbolicky	symbolicky	k6eAd1
uznáván	uznáván	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
exteritoriální	exteritoriální	k2eAgNnSc1d1
území	území	k1gNnSc1
Francouzské	francouzský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
na	na	k7c4
bitvu	bitva	k1gFnSc4
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
monumentem	monument	k1gInSc7
je	být	k5eAaImIp3nS
Pyramide	pyramid	k1gInSc5
van	vana	k1gFnPc2
Austerlitz	Austerlitza	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
na	na	k7c4
oslavu	oslava	k1gFnSc4
kampaně	kampaň	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1805	#num#	k4
poblíž	poblíž	k7c2
Utrechtu	Utrecht	k1gInSc2
v	v	k7c6
Nizozemí	Nizozemí	k1gNnSc6
vybudovali	vybudovat	k5eAaPmAgMnP
zde	zde	k6eAd1
dislokovaní	dislokovaný	k2eAgMnPc1d1
francouzští	francouzský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Paříži	Paříž	k1gFnSc6
na	na	k7c6
Place	plac	k1gInSc6
Vendôme	Vendôm	k1gInSc5
dále	daleko	k6eAd2
stojí	stát	k5eAaImIp3nS
Napoleonův	Napoleonův	k2eAgMnSc1d1
oslavný	oslavný	k2eAgMnSc1d1
<g/>
,	,	kIx,
44	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc1d1
bronzový	bronzový	k2eAgInSc1d1
sloup	sloup	k1gInSc1
Colonne	Colonn	k1gMnSc5
Vendôme	Vendôm	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
dílo	dílo	k1gNnSc1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
vrcholu	vrchol	k1gInSc6
nese	nést	k5eAaImIp3nS
Napoleonovu	Napoleonův	k2eAgFnSc4d1
sochu	socha	k1gFnSc4
a	a	k8xC
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
považovat	považovat	k5eAaImF
za	za	k7c4
kopii	kopie	k1gFnSc4
Trajánova	Trajánův	k2eAgInSc2d1
sloupu	sloup	k1gInSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monument	monument	k1gInSc1
se	se	k3xPyFc4
původně	původně	k6eAd1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Slavkovský	slavkovský	k2eAgInSc4d1
sloup	sloup	k1gInSc4
(	(	kIx(
<g/>
Colonne	Colonn	k1gMnSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Austerlitz	Austerlitz	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
podle	podle	k7c2
legendy	legenda	k1gFnSc2
byl	být	k5eAaImAgInS
odlit	odlit	k2eAgInSc1d1
z	z	k7c2
roztavených	roztavený	k2eAgFnPc2d1
hlavní	hlavní	k2eAgFnSc7d1
spojeneckých	spojenecký	k2eAgFnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
pocházejících	pocházející	k2eAgFnPc2d1
z	z	k7c2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Paříži	Paříž	k1gFnSc6
toto	tento	k3xDgNnSc1
střetnutí	střetnutí	k1gNnSc1
připomíná	připomínat	k5eAaImIp3nS
i	i	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgNnPc2d1
míst	místo	k1gNnPc2
a	a	k8xC
veřejných	veřejný	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
Slavkovské	slavkovský	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
<g/>
,	,	kIx,
Slavkovské	slavkovský	k2eAgNnSc1d1
nábřeží	nábřeží	k1gNnSc1
nebo	nebo	k8xC
Slavkovský	slavkovský	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
jeden	jeden	k4xCgInSc4
z	z	k7c2
největších	veliký	k2eAgInPc2d3
triumfů	triumf	k1gInPc2
je	být	k5eAaImIp3nS
vítězství	vítězství	k1gNnSc1
připomínáno	připomínat	k5eAaImNgNnS
v	v	k7c6
kruhu	kruh	k1gInSc6
kolem	kolem	k7c2
Napoleonova	Napoleonův	k2eAgInSc2d1
sarkofágu	sarkofág	k1gInSc2
v	v	k7c6
pařížské	pařížský	k2eAgFnSc6d1
Invalidovně	invalidovna	k1gFnSc6
a	a	k8xC
výjev	výjev	k1gInSc4
z	z	k7c2
průběhu	průběh	k1gInSc2
bitvy	bitva	k1gFnSc2
je	být	k5eAaImIp3nS
vyobrazen	vyobrazit	k5eAaPmNgInS
i	i	k9
na	na	k7c6
basreliéfu	basreliéf	k1gInSc6
východního	východní	k2eAgInSc2d1
pilíře	pilíř	k1gInSc2
Vítězného	vítězný	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mohyla	mohyla	k1gFnSc1
míru	mír	k1gInSc2
</s>
<s>
Pyramida	pyramida	k1gFnSc1
u	u	k7c2
vsi	ves	k1gFnSc2
Austerlitz	Austerlitza	k1gFnPc2
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
</s>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1
sloup	sloup	k1gInSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Slavkovské	slavkovský	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
(	(	kIx(
<g/>
Gare	Gare	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Austerlitz	Austerlitz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Slavkovský	slavkovský	k2eAgInSc1d1
most	most	k1gInSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
</s>
<s>
Přístav	přístav	k1gInSc1
Port	port	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Austerlitz	Austerlitz	k1gMnSc1
na	na	k7c6
Seine	Sein	k1gInSc5
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Jako	jako	k8xS,k8xC
těžkou	těžký	k2eAgFnSc4d1
urážku	urážka	k1gFnSc4
vnímal	vnímat	k5eAaImAgInS
car	car	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
zejména	zejména	k9
odezvu	odezvat	k5eAaPmIp1nS
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
ostrou	ostrý	k2eAgFnSc4d1
nótu	nóta	k1gFnSc4
v	v	k7c6
záležitosti	záležitost	k1gFnSc6
s	s	k7c7
popravou	poprava	k1gFnSc7
bourbonského	bourbonský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleon	napoleon	k1gInSc1
v	v	k7c6
odpovědi	odpověď	k1gFnSc6
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
konstatoval	konstatovat	k5eAaBmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
bývalo	bývat	k5eAaImAgNnS
vědělo	vědět	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Anglií	Anglie	k1gFnSc7
podněcované	podněcovaný	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
chystají	chystat	k5eAaImIp3nP
vraždu	vražda	k1gFnSc4
Pavla	Pavel	k1gMnSc2
I.	I.	kA
a	a	k8xC
jsou	být	k5eAaImIp3nP
na	na	k7c6
míli	míle	k1gFnSc6
od	od	k7c2
ruské	ruský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
nespěchalo	spěchat	k5eNaImAgNnS
by	by	kYmCp3nP
Rusko	Rusko	k1gNnSc1
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
zatčením	zatčení	k1gNnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Ve	v	k7c6
větě	věta	k1gFnSc6
se	se	k3xPyFc4
skrývala	skrývat	k5eAaImAgFnS
narážka	narážka	k1gFnSc1
na	na	k7c4
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
ruský	ruský	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
namísto	namísto	k7c2
tvrdých	tvrdý	k2eAgInPc2d1
trestů	trest	k1gInPc2
vrahům	vrah	k1gMnPc3
svého	své	k1gNnSc2
otce	otec	k1gMnSc4
udělil	udělit	k5eAaPmAgInS
tituly	titul	k1gInPc7
a	a	k8xC
úřady	úřad	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
vojáky	voják	k1gMnPc4
dvou	dva	k4xCgInPc2
praporů	prapor	k1gInPc2
Novgorodského	novgorodský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
a	a	k8xC
praporu	prapor	k1gInSc2
granátníků	granátník	k1gMnPc2
Apšeronského	Apšeronský	k2eAgInSc2d1
pluku	pluk	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
počtu	počet	k1gInSc2
bylo	být	k5eAaImAgNnS
jen	jen	k9
18	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Zbytky	zbytek	k1gInPc7
4	#num#	k4
<g/>
.	.	kIx.
řadového	řadový	k2eAgInSc2d1
pluku	pluk	k1gInSc2
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
lehkého	lehký	k2eAgInSc2d1
pluku	pluk	k1gInSc2
shromáždil	shromáždit	k5eAaPmAgMnS
major	major	k1gMnSc1
Bigarré	Bigarrý	k2eAgFnSc2d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
s	s	k7c7
vojáky	voják	k1gMnPc7
vyrazil	vyrazit	k5eAaPmAgMnS
směrem	směr	k1gInSc7
k	k	k7c3
Prateckému	pratecký	k2eAgInSc3d1
kopci	kopec	k1gInSc3
a	a	k8xC
posléze	posléze	k6eAd1
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
útoku	útok	k1gInSc2
na	na	k7c4
Sokolnice	sokolnice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
prozaickém	prozaický	k2eAgNnSc6d1
podání	podání	k1gNnSc6
se	se	k3xPyFc4
střet	střet	k1gInSc1
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
knize	kniha	k1gFnSc6
L.	L.	kA
N.	N.	kA
Tolstého	Tolstý	k2eAgMnSc4d1
Vojna	vojna	k1gFnSc1
a	a	k8xC
mír	mír	k1gInSc1
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slunce	slunce	k1gNnSc1
nad	nad	k7c7
Slavkovem	Slavkov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
92	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
261	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Uhlíř	Uhlíř	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
356.1	356.1	k4
2	#num#	k4
CASTLE	CASTLE	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkov	Slavkov	k1gInSc1
1805	#num#	k4
-	-	kIx~
Osud	osud	k1gInSc1
císařství	císařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
publishing	publishing	k1gInSc1
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1895	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
62	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Castle	Castle	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
DUPUY	DUPUY	kA
<g/>
,	,	kIx,
R.	R.	kA
Ernest	Ernest	k1gMnSc1
<g/>
;	;	kIx,
DUPUY	DUPUY	kA
<g/>
,	,	kIx,
Trevor	Trevor	k1gMnSc1
N.	N.	kA
Vojenské	vojenský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Forma	forma	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
832	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
200	#num#	k4
let	léto	k1gNnPc2
tradic	tradice	k1gFnPc2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Onufrius	Onufrius	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
124	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903432	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Kopecký	Kopecký	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
MANFRED	MANFRED	kA
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
Z.	Z.	kA
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
205	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
129	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
363	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Manfred	Manfred	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
19	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
68	#num#	k4
a	a	k8xC
76	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleonova	Napoleonův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
I.	I.	kA
-	-	kIx~
Vítězné	vítězný	k2eAgInPc4d1
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
261	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
18	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k6eAd1
<g/>
:	:	kIx,
Napoleonova	Napoleonův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
I.	I.	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
109	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
109	#num#	k4
<g/>
–	–	k?
<g/>
111	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
112	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slunce	slunce	k1gNnSc1
nad	nad	k7c7
Slavkovem	Slavkov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
777	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Uhlíř	Uhlíř	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Manfred	Manfred	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
370.1	370.1	k4
2	#num#	k4
Napoleonova	Napoleonův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
I.	I.	kA
<g/>
,	,	kIx,
s.	s.	k?
30	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
38	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
36	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
40	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
12.1	12.1	k4
2	#num#	k4
Manfred	Manfred	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
375	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
182	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
99	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
104	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
107	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkov	Slavkov	k1gInSc1
1805	#num#	k4
-	-	kIx~
Napoleonův	Napoleonův	k2eAgInSc1d1
největší	veliký	k2eAgInSc1d3
triumf	triumf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
983	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
206	#num#	k4
<g/>
–	–	k?
<g/>
207	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
339	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
19	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
395	#num#	k4
<g/>
–	–	k?
<g/>
397	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
206	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
229	#num#	k4
<g/>
–	–	k?
<g/>
230	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
20	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
413	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
20	#num#	k4
a	a	k8xC
44	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Todd	Todd	k1gInSc1
Fisher	Fishra	k1gFnPc2
&	&	k?
Gregory	Gregor	k1gMnPc4
Fremont-Barnes	Fremont-Barnes	k1gInSc4
<g/>
,	,	kIx,
The	The	k1gFnPc3
Napoleonic	Napoleonice	k1gFnPc2
Wars	Warsa	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Rise	Ris	k1gFnSc2
and	and	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Fall	Fall	k1gMnSc1
of	of	k?
an	an	k?
Empire	empir	k1gInSc5
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
33	#num#	k4
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
430	#num#	k4
<g/>
–	–	k?
<g/>
341	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
430	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
431	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
341	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
450	#num#	k4
<g/>
–	–	k?
<g/>
451	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
453	#num#	k4
<g/>
–	–	k?
<g/>
454.1	454.1	k4
2	#num#	k4
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
46	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
189	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
191	#num#	k4
a	a	k8xC
340	#num#	k4
<g/>
-	-	kIx~
<g/>
341	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
422	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
47	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
455	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
399	#num#	k4
<g/>
–	–	k?
<g/>
401	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
–	–	k?
<g/>
45	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
401	#num#	k4
<g/>
–	–	k?
<g/>
404	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
46	#num#	k4
<g/>
–	–	k?
<g/>
47.1	47.1	k4
2	#num#	k4
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
397	#num#	k4
<g/>
–	–	k?
<g/>
398	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ROZBOŘIL	rozbořit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historická	historický	k2eAgFnSc1d1
exkurze	exkurze	k1gFnSc1
regionem	region	k1gInSc7
Austerlitz	Austerlitza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
PedF	PedF	k1gMnPc1
MU	MU	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
3.1	3.1	k4
<g/>
,	,	kIx,
s.	s.	k?
27	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
489	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
229	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
114	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SÉGUR	SÉGUR	kA
<g/>
,	,	kIx,
Philipe	Philip	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnSc1
aide-de-camp	aide-de-camp	k1gMnSc1
of	of	k?
Napoleon	Napoleon	k1gMnSc1
1800	#num#	k4
<g/>
-	-	kIx~
<g/>
1812	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
D.	D.	kA
Appleton	Appleton	k1gInSc1
and	and	k?
company	compana	k1gFnSc2
<g/>
,	,	kIx,
1895	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
391	#num#	k4
<g/>
–	–	k?
<g/>
392.1	392.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
115	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
s.	s.	k?
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
247	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
403	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
250	#num#	k4
<g/>
–	–	k?
<g/>
251	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
119	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
236	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
128	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
253	#num#	k4
<g/>
–	–	k?
<g/>
254	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
407	#num#	k4
<g/>
–	–	k?
<g/>
410	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
259	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
264	#num#	k4
<g/>
–	–	k?
<g/>
266	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
140	#num#	k4
<g/>
–	–	k?
<g/>
141	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
261	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
262	#num#	k4
a	a	k8xC
266.1	266.1	k4
2	#num#	k4
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
141	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
424	#num#	k4
<g/>
–	–	k?
<g/>
425	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
142	#num#	k4
<g/>
–	–	k?
<g/>
143	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
417	#num#	k4
<g/>
–	–	k?
<g/>
418	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
452	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
425	#num#	k4
<g/>
–	–	k?
<g/>
431	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
279	#num#	k4
<g/>
–	–	k?
<g/>
283	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
268	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
148	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
148	#num#	k4
<g/>
–	–	k?
<g/>
149	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
149	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
150	#num#	k4
a	a	k8xC
154	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
154	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
154	#num#	k4
<g/>
–	–	k?
<g/>
155	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
443	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
164	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
295	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
474	#num#	k4
<g/>
–	–	k?
<g/>
475	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
290	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
161	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
291	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
297	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
293	#num#	k4
<g/>
–	–	k?
<g/>
294	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
475	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ELBL	ELBL	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
B.	B.	kA
Maršál	maršál	k1gMnSc1
Lannes	Lannes	k1gMnSc1
-	-	kIx~
Francouzský	francouzský	k2eAgMnSc1d1
Achilles	Achilles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
529	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
210	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Elbl	Elbl	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
299.1	299.1	k4
2	#num#	k4
REGAN	REGAN	kA
<g/>
,	,	kIx,
Geoffrey	Geoffrey	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
181	#num#	k4
<g/>
–	–	k?
<g/>
183	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
301	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
447	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Elbl	Elbl	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
213	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
447	#num#	k4
<g/>
–	–	k?
<g/>
449	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
172	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
451	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
452	#num#	k4
<g/>
–	–	k?
<g/>
453	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Elbl	Elbl	k1gInSc1
s.	s.	k?
214	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
457	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
462	#num#	k4
<g/>
–	–	k?
<g/>
465	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
519	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
304	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
520	#num#	k4
<g/>
–	–	k?
<g/>
523	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
524	#num#	k4
<g/>
–	–	k?
<g/>
526	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HOCHEL	HOCHEL	kA
<g/>
,	,	kIx,
Marian	Mariana	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojiště	bojiště	k1gNnSc1
Bitvy	bitva	k1gFnSc2
tří	tři	k4xCgMnPc2
císařů	císař	k1gMnPc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
:	:	kIx,
Průvodce	průvodce	k1gMnSc1
po	po	k7c6
památkové	památkový	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
ČSNS	ČSNS	kA
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
6229	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
133	#num#	k4
<g/>
–	–	k?
<g/>
137	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slunce	slunce	k1gNnSc1
nad	nad	k7c7
Slavkovem	Slavkov	k1gInSc7
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
777	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
328	#num#	k4
<g/>
–	–	k?
<g/>
351	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
557.1	557.1	k4
2	#num#	k4
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
179	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
326.1	326.1	k4
2	#num#	k4
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
180	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
327	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
492	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
181	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
495	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
496	#num#	k4
<g/>
–	–	k?
<g/>
497	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
508	#num#	k4
<g/>
–	–	k?
<g/>
509	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FIDLER	FIDLER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkovská	slavkovský	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gFnSc1
s.	s.	k?
r.	r.	kA
o.	o.	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
381	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
128	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TOLSTOJ	Tolstoj	k1gMnSc1
<g/>
,	,	kIx,
Lev	Lev	k1gMnSc1
Nikolajevič	Nikolajevič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojna	vojna	k1gFnSc1
a	a	k8xC
mír	mír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
352	#num#	k4
<g/>
–	–	k?
<g/>
353	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
509	#num#	k4
<g/>
–	–	k?
<g/>
512	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
185	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
189	#num#	k4
<g/>
–	–	k?
<g/>
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
536	#num#	k4
<g/>
–	–	k?
<g/>
539	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
541	#num#	k4
<g/>
–	–	k?
<g/>
542	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
348	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
348	#num#	k4
<g/>
–	–	k?
<g/>
351.1	351.1	k4
2	#num#	k4
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
551	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
354	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FRYDRYCH	Frydrych	k1gMnSc1
<g/>
,	,	kIx,
Karol	Karol	k1gInSc1
<g/>
.	.	kIx.
200	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věstník	věstník	k1gInSc1
Historicko-vlastivědného	historicko-vlastivědný	k2eAgInSc2d1
kroužku	kroužek	k1gInSc2
v	v	k7c6
Žarošicích	Žarošice	k1gFnPc6
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
15	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
41	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VLACH	Vlach	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kde	kde	k6eAd1
přenocoval	přenocovat	k5eAaPmAgMnS
v	v	k7c6
Žarošicích	Žarošik	k1gInPc6
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Hradské	hradská	k1gFnSc2
cesty	cesta	k1gFnSc2
1965	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
.	.	kIx.
1966	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SLOVÁK	Slovák	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bojišti	bojiště	k1gNnSc6
slavkovském	slavkovský	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Cizinecký	cizinecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Slovák	Slovák	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
HANÁK	Hanák	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
;	;	kIx,
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stará	starý	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
a	a	k8xC
slavkovská	slavkovský	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
AVE	ave	k1gNnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
16	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902242	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
563	#num#	k4
<g/>
–	–	k?
<g/>
564	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
388	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Castle	Castle	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
88	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Slovák	Slovák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Slovák	Slovák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
72	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Slovák	Slovák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
63	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Památník	památník	k1gInSc1
Napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec-veselicko	Obec-veselicko	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brněnský	brněnský	k2eAgInSc1d1
pitaval	pitaval	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
184	#num#	k4
<g/>
–	–	k?
<g/>
190	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
536	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
350	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DE	DE	k?
MARBOT	MARBOT	kA
<g/>
,	,	kIx,
Marcellin	Marcellin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paměti	paměť	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jiří	Jiří	k1gMnSc1
Kovařík	Kovařík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902353	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
148	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Slovák	Slovák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
267	#num#	k4
<g/>
–	–	k?
<g/>
268	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
427	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Byla	být	k5eAaImAgFnS
v	v	k7c6
průběhu	průběh	k1gInSc6
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
bráněná	bráněný	k2eAgFnSc1d1
sokolnická	sokolnický	k2eAgFnSc1d1
bažantnice	bažantnice	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
Austerlitz	Austerlitz	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
398	#num#	k4
<g/>
–	–	k?
<g/>
401	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
257	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
305	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Uhlíř	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
392.1	392.1	k4
2	#num#	k4
Kopecký	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GUÉRIN	GUÉRIN	kA
<g/>
,	,	kIx,
Leo	Leo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesty	cesta	k1gFnSc2
po	po	k7c6
světě	svět	k1gInSc6
-	-	kIx~
Díl	díl	k1gInSc1
I.	I.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Šanta	šanta	k1gFnSc1
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
A.	A.	kA
Augusta	Augusta	k1gMnSc1
<g/>
,	,	kIx,
1855	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
76	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Kovařík	Kovařík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
592	#num#	k4
<g/>
–	–	k?
<g/>
595	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
CASTLE	CASTLE	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkov	Slavkov	k1gInSc1
1805	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Jiří	Jiří	k1gMnSc1
Hutečka	Hutečka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Amercom	Amercom	k1gInSc1
SA	SA	kA
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
94	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
261	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
628	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FIDLER	FIDLER	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkovská	slavkovský	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
:	:	kIx,
válka	válka	k1gFnSc1
roku	rok	k1gInSc2
1805	#num#	k4
a	a	k8xC
bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
278	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7217	#num#	k4
<g/>
-	-	kIx~
<g/>
381	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HANÁK	Hanák	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
;	;	kIx,
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stará	starý	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
a	a	k8xC
slavkovská	slavkovský	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
AVE	ave	k1gNnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
23	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902242	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KALÁB	Kaláb	k1gMnSc1
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
tří	tři	k4xCgMnPc2
císařů	císař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
A.	A.	kA
Odehnal	Odehnal	k1gMnSc1
<g/>
,	,	kIx,
1928	#num#	k4
<g/>
.	.	kIx.
89	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napoleonova	Napoleonův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
–	–	k?
Vítězné	vítězný	k2eAgInPc4d1
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
485	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
261	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkov	Slavkov	k1gInSc1
1805	#num#	k4
-	-	kIx~
Napoleonův	Napoleonův	k2eAgInSc1d1
největší	veliký	k2eAgInSc1d3
triumf	triumf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
608	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
983	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MICHAJLOVSKIJ-DANILEVSKIJ	MICHAJLOVSKIJ-DANILEVSKIJ	k?
<g/>
,	,	kIx,
Aleksandr	Aleksandr	k1gInSc1
Ivanovič	Ivanovič	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
1805	#num#	k4
:	:	kIx,
první	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
cara	car	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
s	s	k7c7
Napoleonem	Napoleon	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
62	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FUČÍK	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
císařským	císařský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1526	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
555	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SLOVÁK	Slovák	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
:	:	kIx,
s	s	k7c7
mapkou	mapka	k1gFnSc7
bojiště	bojiště	k1gNnSc2
a	a	k8xC
obrazem	obraz	k1gInSc7
tří	tři	k4xCgInPc2
císařů	císař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
nákladem	náklad	k1gInSc7
vlastním	vlastnit	k5eAaImIp1nS
<g/>
,	,	kIx,
1898	#num#	k4
<g/>
.	.	kIx.
154	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
tří	tři	k4xCgMnPc2
císařů	císař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavkov	Slavkov	k1gInSc1
/	/	kIx~
Austerlitz	Austerlitz	k1gInSc1
1805	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Ave	ave	k1gNnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
135	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
900941	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
UHLÍŘ	Uhlíř	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slunce	slunce	k1gNnSc1
nad	nad	k7c7
Slavkovem	Slavkov	k1gInSc7
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
468	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
92	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Projektu	projekt	k1gInSc2
Bellum	Bellum	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Projektu	projekt	k1gInSc2
Austerlitz	Austerlitza	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Pohled	pohled	k1gInSc1
na	na	k7c4
místo	místo	k1gNnSc4
bitvy	bitva	k1gFnSc2
–	–	k?
virtuální	virtuální	k2eAgFnSc1d1
prohlídka	prohlídka	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Rekonstrukce	rekonstrukce	k1gFnSc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
2005	#num#	k4
–	–	k?
dokument	dokument	k1gInSc4
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
nizozemsky	nizozemsky	k6eAd1
<g/>
)	)	kIx)
3D	3D	k4
model	model	k1gInSc1
Piramide	Piramid	k1gInSc5
van	van	k1gInSc4
Austerlitz	Austerlitz	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
nizozemsky	nizozemsky	k6eAd1
<g/>
)	)	kIx)
Video	video	k1gNnSc1
Piramide	Piramid	k1gInSc5
van	van	k1gInSc1
Austerlitz	Austerlitz	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bitvy	bitva	k1gFnPc1
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wertingen	Wertingen	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Elchingenu	Elchingen	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ulmu	Ulmus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Trafalgaru	Trafalgar	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Caldiera	Caldiero	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Amstettenu	Amstetten	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dürnsteinu	Dürnstein	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Schöngrabernu	Schöngraberna	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Štoků	štok	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Maidy	Maida	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Saalfeldu	Saalfeld	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jeny	jen	k1gInPc7
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Auerstedtu	Auerstedt	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Prenzlau	Prenzlaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Pultuska	Pultuska	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jílového	jílový	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Heilsbergu	Heilsberg	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Friedlandu	Friedland	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Medina	Medina	k1gFnSc1
del	del	k?
Rio	Rio	k1gMnSc1
Seco	Seco	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bailénu	Bailén	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Somosierry	Somosierra	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
La	la	k1gNnSc2
Coruñ	Coruñ	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarragony	Tarragona	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Eggmühlu	Eggmühl	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Aspern	Asperna	k1gFnPc2
a	a	k8xC
Esslingu	Essling	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wagramu	Wagram	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Znojma	Znojmo	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ciudad	Ciudad	k1gInSc4
Rodrigo	Rodrigo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Salamancy	Salamanca	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ostrovna	Ostrovna	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Smolensk	Smolensk	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Borodina	Borodin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarutina	Tarutin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Malojaroslavce	Malojaroslavec	k1gMnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Krasného	Krasný	k2eAgInSc2d1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Polocku	Polock	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Berezině	Berezina	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Möckern	Möckern	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lützenu	Lützen	k2eAgFnSc4d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Budyšína	Budyšín	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Vitorie	Vitorie	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Grossbeeren	Grossbeerna	k1gFnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Drážďan	Drážďany	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Chlumce	Chlumec	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Kačavě	Kačava	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dennewitz	Dennewitz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wartenburgu	Wartenburg	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hanau	Hanaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Brienne	Brienn	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
La	la	k1gNnSc2
Rothiè	Rothiè	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Champaubertu	Champaubert	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Montmirailu	Montmirail	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Chateau-Thierry	Chateau-Thierra	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Montereau	Montereaus	k1gInSc2
•	•	k?
Obležení	obležení	k1gNnPc2
Soissons	Soissons	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Craonne	Craonn	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Laonu	Laon	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Remeše	Remeš	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arcis-sur-Aube	Arcis-sur-Aub	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Paříž	Paříž	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tolentina	Tolentin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
San	San	k1gFnSc2
Germana	German	k1gMnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ligny	Ligna	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Quatre-Bras	Quatre-Bras	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Waterloo	Waterloo	k1gNnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
Francie	Francie	k1gFnSc2
</s>
<s>
Napoleon	Napoleon	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
francouzský	francouzský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Berthier	Berthier	k1gInSc1
(	(	kIx(
<g/>
náčelník	náčelník	k1gInSc1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Bernadotte	Bernadott	k1gInSc5
(	(	kIx(
<g/>
I.	I.	kA
sbor	sbor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Davout	Davout	k1gMnSc1
(	(	kIx(
<g/>
III	III	kA
<g/>
.	.	kIx.
sbor	sbor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Soult	Soult	k1gMnSc1
(	(	kIx(
<g/>
IV	IV	kA
<g/>
.	.	kIx.
sbor	sbor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Lannes	Lannes	k1gMnSc1
(	(	kIx(
<g/>
V.	V.	kA
sbor	sbor	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Murat	Murat	k1gInSc1
(	(	kIx(
<g/>
jízda	jízda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bessiè	Bessiè	k1gInSc1
(	(	kIx(
<g/>
císařská	císařský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
)	)	kIx)
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Kutuzov	Kutuzov	k1gInSc1
(	(	kIx(
<g/>
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Weyrother	Weyrothra	k1gFnPc2
(	(	kIx(
<g/>
náčelník	náčelník	k1gInSc1
generálního	generální	k2eAgInSc2d1
štábu	štáb	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Kienmayer	Kienmayer	k1gInSc1
(	(	kIx(
<g/>
předvoj	předvoj	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
kolony	kolona	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Buxhöwden	Buxhöwdna	k1gFnPc2
(	(	kIx(
<g/>
velitel	velitel	k1gMnSc1
prvních	první	k4xOgInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
tří	tři	k4xCgFnPc2
kolon	kolona	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Dochturov	Dochturov	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Langeron	Langeron	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Przybyszewski	Przybyszewski	k1gNnSc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kolovrat	kolovrat	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Miloradovič	Miloradovič	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Lichtenštejn	Lichtenštejn	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
kolona	kolona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bagration	Bagration	k1gInSc1
(	(	kIx(
<g/>
spojenecký	spojenecký	k2eAgInSc1d1
předvoj	předvoj	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Konstantin	Konstantin	k1gMnSc1
Pavlovič	Pavlovič	k1gMnSc1
(	(	kIx(
<g/>
carská	carský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
)	)	kIx)
Bojiště	bojiště	k1gNnSc1
</s>
<s>
Obce	obec	k1gFnPc1
památkové	památkový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
</s>
<s>
Blažovice	Blažovice	k1gFnSc1
•	•	k?
Holubice	holubice	k1gFnSc2
•	•	k?
Hostěrádky-Rešov	Hostěrádky-Rešov	k1gInSc1
•	•	k?
Jiříkovice	Jiříkovice	k1gFnSc2
•	•	k?
Kobylnice	Kobylnice	k1gFnPc4
•	•	k?
Křenovice	Křenovice	k1gFnSc1
•	•	k?
Podolí	Podolí	k1gNnPc2
•	•	k?
Ponětovice	Ponětovice	k1gFnSc2
•	•	k?
Prace	Prace	k1gFnSc2
•	•	k?
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Sokolnice	sokolnice	k1gFnSc2
•	•	k?
Šlapanice	Šlapanice	k1gFnSc2
•	•	k?
Telnice	Telnice	k1gFnSc2
•	•	k?
Tvarožná	Tvarožný	k2eAgFnSc1d1
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Zbýšov	Zbýšov	k1gInSc1
•	•	k?
Žatčany	Žatčan	k1gMnPc4
Obce	obec	k1gFnSc2
mimo	mimo	k7c4
zónu	zóna	k1gFnSc4
</s>
<s>
Bedřichovice	Bedřichovice	k1gFnSc1
•	•	k?
Hrušky	hruška	k1gFnSc2
•	•	k?
Kovalovice	Kovalovice	k1gFnSc2
•	•	k?
Měnín	Měnín	k1gMnSc1
•	•	k?
Pozořice	Pozořice	k1gFnSc2
•	•	k?
Sivice	Sivice	k1gFnSc2
•	•	k?
Šaratice	šaratice	k1gFnSc2
•	•	k?
Velešovice	Velešovice	k1gFnSc2
Další	další	k2eAgNnPc1d1
místa	místo	k1gNnPc1
bojiště	bojiště	k1gNnSc2
</s>
<s>
Kandie	Kandie	k1gFnSc1
•	•	k?
Mohyla	mohyla	k1gFnSc1
míru	mír	k1gInSc2
•	•	k?
Pindulka	Pindulka	k1gFnSc1
•	•	k?
Santon	Santon	k1gInSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
pozořická	pozořický	k2eAgFnSc1d1
pošta	pošta	k1gFnSc1
•	•	k?
Staré	Staré	k2eAgInPc1d1
vinohrady	vinohrad	k1gInPc1
•	•	k?
Zámek	zámek	k1gInSc1
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Žuráň	Žuráň	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Novověk	novověk	k1gInSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
