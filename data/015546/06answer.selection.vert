<s desamb="1">
Údajně	údajně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
slavkovském	slavkovský	k2eAgInSc6d1
zámku	zámek	k1gInSc6
den	den	k1gInSc1
před	před	k7c7
bitvou	bitva	k1gFnSc7
nocovali	nocovat	k5eAaImAgMnP
oba	dva	k4xCgMnPc4
spojenečtí	spojenecký	k2eAgMnPc1d1
panovníci	panovník	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
snad	snad	k9
i	i	k9
na	na	k7c4
počest	počest	k1gFnSc4
zámku	zámek	k1gInSc2
samotného	samotný	k2eAgInSc2d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
na	na	k7c4
Napoleona	Napoleon	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
ubytoval	ubytovat	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
,	,	kIx,
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
dojem	dojem	k1gInSc4
<g/>
.	.	kIx.
</s>