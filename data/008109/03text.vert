<s>
Krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
působí	působit	k5eAaImIp3nS	působit
krev	krev	k1gFnSc1	krev
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
cévy	céva	k1gFnSc2	céva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vytvářen	vytvářet	k5eAaImNgInS	vytvářet
působením	působení	k1gNnSc7	působení
srdce	srdce	k1gNnSc2	srdce
jako	jako	k8xC	jako
krevní	krevní	k2eAgFnSc2d1	krevní
pumpy	pumpa	k1gFnSc2	pumpa
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
se	se	k3xPyFc4	se
stavbou	stavba	k1gFnSc7	stavba
a	a	k8xC	a
funkcemi	funkce	k1gFnPc7	funkce
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tlak	tlak	k1gInSc1	tlak
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
krevního	krevní	k2eAgInSc2d1	krevní
řečiště	řečiště	k1gNnPc1	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
tlakem	tlak	k1gInSc7	tlak
krve	krev	k1gFnSc2	krev
míní	mínit	k5eAaImIp3nS	mínit
arteriální	arteriální	k2eAgInSc4d1	arteriální
(	(	kIx(	(
<g/>
tepenný	tepenný	k2eAgInSc4d1	tepenný
<g/>
)	)	kIx)	)
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tlak	tlak	k1gInSc1	tlak
krve	krev	k1gFnSc2	krev
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
tepnách	tepna	k1gFnPc6	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
cévách	céva	k1gFnPc6	céva
mění	měnit	k5eAaImIp3nS	měnit
také	také	k9	také
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
čase	čas	k1gInSc6	čas
–	–	k?	–
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hodnot	hodnota	k1gFnPc2	hodnota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ve	v	k7c6	v
vypuzovací	vypuzovací	k2eAgFnSc6d1	vypuzovací
fázi	fáze	k1gFnSc6	fáze
srdeční	srdeční	k2eAgFnSc2d1	srdeční
akce	akce	k1gFnSc2	akce
(	(	kIx(	(
<g/>
systolický	systolický	k2eAgInSc1d1	systolický
tlak	tlak	k1gInSc1	tlak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
plnění	plnění	k1gNnSc4	plnění
srdečních	srdeční	k2eAgFnPc2d1	srdeční
komor	komora	k1gFnPc2	komora
(	(	kIx(	(
<g/>
diastolický	diastolický	k2eAgInSc1d1	diastolický
tlak	tlak	k1gInSc1	tlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měření	měření	k1gNnSc2	měření
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
dvě	dva	k4xCgNnPc1	dva
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
oddělená	oddělený	k2eAgFnSc1d1	oddělená
lomítkem	lomítko	k1gNnSc7	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
systolický	systolický	k2eAgInSc1d1	systolický
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
diastolický	diastolický	k2eAgInSc1d1	diastolický
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyjadřován	vyjadřovat	k5eAaImNgMnS	vyjadřovat
v	v	k7c6	v
torrech	torr	k1gInPc6	torr
(	(	kIx(	(
<g/>
milimetrech	milimetr	k1gInPc6	milimetr
rtuťového	rtuťový	k2eAgInSc2d1	rtuťový
sloupce	sloupec	k1gInSc2	sloupec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgNnSc1d1	běžné
měření	měření	k1gNnSc1	měření
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
rtuťového	rtuťový	k2eAgInSc2d1	rtuťový
tonometru	tonometr	k1gInSc2	tonometr
a	a	k8xC	a
fonendoskopu	fonendoskop	k1gInSc2	fonendoskop
<g/>
.	.	kIx.	.
</s>
<s>
Manžeta	manžeta	k1gFnSc1	manžeta
manometru	manometr	k1gInSc2	manometr
se	se	k3xPyFc4	se
upevní	upevnit	k5eAaPmIp3nS	upevnit
nejčastěji	často	k6eAd3	často
na	na	k7c4	na
paži	paže	k1gFnSc4	paže
nad	nad	k7c7	nad
loktem	loket	k1gInSc7	loket
a	a	k8xC	a
naplní	naplnit	k5eAaPmIp3nS	naplnit
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
tlak	tlak	k1gInSc1	tlak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
stupnice	stupnice	k1gFnSc2	stupnice
manometru	manometr	k1gInSc2	manometr
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
zaškrcení	zaškrcený	k2eAgMnPc1d1	zaškrcený
pažní	pažní	k2eAgFnSc2d1	pažní
tepny	tepna	k1gFnPc1	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Vzduch	vzduch	k1gInSc1	vzduch
z	z	k7c2	z
manžety	manžeta	k1gFnSc2	manžeta
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
upouští	upouštět	k5eAaImIp3nS	upouštět
a	a	k8xC	a
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
tlak	tlak	k1gInSc1	tlak
vzduchu	vzduch	k1gInSc2	vzduch
vyrovná	vyrovnat	k5eAaPmIp3nS	vyrovnat
systolickému	systolický	k2eAgInSc3d1	systolický
tlaku	tlak	k1gInSc3	tlak
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
pažní	pažní	k2eAgFnSc6d1	pažní
tepně	tepna	k1gFnSc6	tepna
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
do	do	k7c2	do
končetiny	končetina	k1gFnSc2	končetina
opět	opět	k6eAd1	opět
proudit	proudit	k5eAaPmF	proudit
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zvukové	zvukový	k2eAgInPc1d1	zvukový
fenomény	fenomén	k1gInPc1	fenomén
tvořené	tvořený	k2eAgInPc1d1	tvořený
proudící	proudící	k2eAgFnSc7d1	proudící
krví	krev	k1gFnSc7	krev
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
registrovat	registrovat	k5eAaBmF	registrovat
fonendoskopem	fonendoskop	k1gInSc7	fonendoskop
přiloženým	přiložený	k2eAgInSc7d1	přiložený
v	v	k7c6	v
loketní	loketní	k2eAgFnSc6d1	loketní
jamce	jamka	k1gFnSc6	jamka
nad	nad	k7c7	nad
průběhem	průběh	k1gInSc7	průběh
pažní	pažní	k2eAgFnSc2d1	pažní
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
turbulentní	turbulentní	k2eAgNnSc4d1	turbulentní
(	(	kIx(	(
<g/>
vířivé	vířivý	k2eAgNnSc4d1	vířivé
<g/>
)	)	kIx)	)
proudění	proudění	k1gNnSc4	proudění
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Korotkovovy	Korotkovův	k2eAgInPc1d1	Korotkovův
fenomény	fenomén	k1gInPc1	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Systolický	systolický	k2eAgInSc1d1	systolický
tlak	tlak	k1gInSc1	tlak
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
odečte	odečíst	k5eAaPmIp3nS	odečíst
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
prvního	první	k4xOgInSc2	první
zaslechnutého	zaslechnutý	k2eAgInSc2d1	zaslechnutý
zvukového	zvukový	k2eAgInSc2d1	zvukový
fenoménu	fenomén	k1gInSc2	fenomén
(	(	kIx(	(
<g/>
rytmické	rytmický	k2eAgNnSc1d1	rytmické
klapání	klapání	k1gNnSc1	klapání
frekvencí	frekvence	k1gFnSc7	frekvence
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
srdeční	srdeční	k2eAgFnSc4d1	srdeční
akci	akce	k1gFnSc4	akce
<g/>
)	)	kIx)	)
na	na	k7c4	na
stupnici	stupnice	k1gFnSc4	stupnice
manometru	manometr	k1gInSc2	manometr
<g/>
.	.	kIx.	.
</s>
<s>
Diastolický	diastolický	k2eAgInSc1d1	diastolický
tlak	tlak	k1gInSc1	tlak
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
za	za	k7c2	za
dalšího	další	k2eAgNnSc2d1	další
snižování	snižování	k1gNnSc2	snižování
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
manžetě	manžeta	k1gFnSc6	manžeta
odečítá	odečítat	k5eAaImIp3nS	odečítat
ve	v	k7c4	v
chvíli	chvíle	k1gFnSc4	chvíle
vymizení	vymizení	k1gNnSc2	vymizení
zvukových	zvukový	k2eAgInPc2d1	zvukový
fenomenů	fenomen	k1gInPc2	fenomen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proudění	proudění	k1gNnSc1	proudění
krve	krev	k1gFnSc2	krev
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
laminární	laminární	k2eAgNnSc4d1	laminární
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k9	i
invazivními	invazivní	k2eAgFnPc7d1	invazivní
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přesnější	přesný	k2eAgMnSc1d2	přesnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pacienta	pacient	k1gMnSc4	pacient
více	hodně	k6eAd2	hodně
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
nutností	nutnost	k1gFnSc7	nutnost
umístit	umístit	k5eAaPmF	umístit
čidlo	čidlo	k1gNnSc4	čidlo
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
na	na	k7c4	na
zjištění	zjištění	k1gNnSc4	zjištění
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
plicním	plicní	k2eAgInSc6d1	plicní
oběhu	oběh	k1gInSc6	oběh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
nutnosti	nutnost	k1gFnSc6	nutnost
opakovaného	opakovaný	k2eAgNnSc2d1	opakované
měření	měření	k1gNnSc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provádět	provádět	k5eAaImF	provádět
neinvazivní	invazivní	k2eNgFnSc7d1	neinvazivní
metodou	metoda	k1gFnSc7	metoda
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
deformační	deformační	k2eAgFnSc2d1	deformační
paměti	paměť	k1gFnSc2	paměť
tepen	tepna	k1gFnPc2	tepna
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnPc1d1	související
změnou	změna	k1gFnSc7	změna
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
osob	osoba	k1gFnPc2	osoba
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
lékaře	lékař	k1gMnSc2	lékař
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
naměřené	naměřený	k2eAgFnPc1d1	naměřená
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
skutečné	skutečný	k2eAgFnPc1d1	skutečná
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
syndrom	syndrom	k1gInSc4	syndrom
bílého	bílý	k2eAgInSc2d1	bílý
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
pacient	pacient	k1gMnSc1	pacient
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
stresu	stres	k1gInSc2	stres
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyloučení	vyloučení	k1gNnSc3	vyloučení
tohoto	tento	k3xDgInSc2	tento
fenoménu	fenomén	k1gInSc2	fenomén
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
vyšší	vysoký	k2eAgInSc1d2	vyšší
naměřený	naměřený	k2eAgInSc1d1	naměřený
tlak	tlak	k1gInSc1	tlak
měřit	měřit	k5eAaImF	měřit
opakovaně	opakovaně	k6eAd1	opakovaně
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
při	při	k7c6	při
třech	tři	k4xCgFnPc6	tři
návštěvách	návštěva	k1gFnPc6	návštěva
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lze	lze	k6eAd1	lze
doporučit	doporučit	k5eAaPmF	doporučit
24	[number]	k4	24
<g/>
hodinové	hodinový	k2eAgInPc1d1	hodinový
monitorování	monitorování	k1gNnSc4	monitorování
tlaku	tlak	k1gInSc2	tlak
Holterovým	Holterův	k2eAgInSc7d1	Holterův
monitorem	monitor	k1gInSc7	monitor
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
arteriální	arteriální	k2eAgFnSc1d1	arteriální
hypertenze	hypertenze	k1gFnSc1	hypertenze
<g/>
)	)	kIx)	)
nastává	nastávat	k5eAaImIp3nS	nastávat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
systolický	systolický	k2eAgInSc1d1	systolický
arteriální	arteriální	k2eAgInSc1d1	arteriální
tlak	tlak	k1gInSc1	tlak
opakovaně	opakovaně	k6eAd1	opakovaně
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
nad	nad	k7c7	nad
140	[number]	k4	140
<g/>
/	/	kIx~	/
<g/>
90	[number]	k4	90
mm	mm	kA	mm
Hg	Hg	k1gFnPc2	Hg
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
vysokém	vysoký	k2eAgInSc6d1	vysoký
tlaku	tlak	k1gInSc6	tlak
jsou	být	k5eAaImIp3nP	být
ohroženy	ohrožen	k2eAgFnPc4d1	ohrožena
cévy	céva	k1gFnPc4	céva
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnPc4	srdce
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
arteriální	arteriální	k2eAgFnSc1d1	arteriální
hypotenze	hypotenze	k1gFnSc1	hypotenze
<g/>
)	)	kIx)	)
nastává	nastávat	k5eAaImIp3nS	nastávat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
hodnoty	hodnota	k1gFnPc4	hodnota
systolického	systolický	k2eAgInSc2d1	systolický
tlaku	tlak	k1gInSc2	tlak
krve	krev	k1gFnSc2	krev
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
pod	pod	k7c7	pod
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
mm	mm	kA	mm
Hg	Hg	k1gFnPc2	Hg
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
snížení	snížení	k1gNnSc4	snížení
prokrvení	prokrvení	k1gNnSc2	prokrvení
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Průvodním	průvodní	k2eAgInSc7d1	průvodní
jevem	jev	k1gInSc7	jev
nízkého	nízký	k2eAgInSc2d1	nízký
tlaku	tlak	k1gInSc2	tlak
jsou	být	k5eAaImIp3nP	být
závratě	závrať	k1gFnPc4	závrať
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc4	pocení
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc4	pocit
chladu	chlad	k1gInSc2	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
působí	působit	k5eAaImIp3nS	působit
mnoho	mnoho	k4c1	mnoho
vlivů	vliv	k1gInPc2	vliv
<g/>
:	:	kIx,	:
věk	věk	k1gInSc1	věk
<g/>
,	,	kIx,	,
aktuální	aktuální	k2eAgNnSc4d1	aktuální
mentální	mentální	k2eAgNnSc4d1	mentální
a	a	k8xC	a
fyzické	fyzický	k2eAgNnSc4d1	fyzické
rozpoložení	rozpoložení	k1gNnSc4	rozpoložení
<g/>
,	,	kIx,	,
okolní	okolní	k2eAgFnSc1d1	okolní
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
systolický	systolický	k2eAgInSc1d1	systolický
a	a	k8xC	a
nízký	nízký	k2eAgInSc1d1	nízký
diastolický	diastolický	k2eAgInSc1d1	diastolický
tlak	tlak	k1gInSc1	tlak
normální	normální	k2eAgInSc1d1	normální
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
opotřebení	opotřebení	k1gNnSc2	opotřebení
a	a	k8xC	a
menší	malý	k2eAgFnSc2d2	menší
pružnosti	pružnost	k1gFnSc2	pružnost
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
nebezpečnější	bezpečný	k2eNgInSc1d2	nebezpečnější
vysoký	vysoký	k2eAgInSc1d1	vysoký
spodní	spodní	k2eAgInSc1d1	spodní
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejnižší	nízký	k2eAgInSc4d3	nejnižší
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
řečišti	řečiště	k1gNnSc6	řečiště
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
namáhá	namáhat	k5eAaImIp3nS	namáhat
neustále	neustále	k6eAd1	neustále
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
www.ordinace.cz	www.ordinace.cza	k1gFnPc2	www.ordinace.cza
Jaký	jaký	k3yRgInSc4	jaký
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
je	být	k5eAaImIp3nS	být
normální	normální	k2eAgMnSc1d1	normální
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c6	na
záznamu	záznam	k1gInSc6	záznam
pořadu	pořad	k1gInSc2	pořad
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
</s>
