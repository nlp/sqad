<s>
Vrstva	vrstva	k1gFnSc1	vrstva
stratosféry	stratosféra	k1gFnSc2	stratosféra
mezi	mezi	k7c7	mezi
25	[number]	k4	25
až	až	k9	až
35	[number]	k4	35
km	km	kA	km
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ozonová	ozonový	k2eAgFnSc1d1	ozonová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
relativně	relativně	k6eAd1	relativně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
koncentraci	koncentrace	k1gFnSc4	koncentrace
ozonu	ozon	k1gInSc2	ozon
(	(	kIx(	(
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
