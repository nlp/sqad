<s>
Ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
muricata	muricata	k1gFnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Vignea	Vigne	k2eAgFnSc1d1
muricata	muricata	k1gFnSc1
<g/>
,	,	kIx,
Carex	Carex	k1gInSc1
pairae	pairaat	k5eAaPmIp3nS
auct	auct	k5eAaPmF
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
jednoděložné	jednoděložný	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
šáchorovité	šáchorovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Cyperaceae	Cyperacea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>