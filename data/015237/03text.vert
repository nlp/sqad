<s>
Ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
</s>
<s>
Ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
Ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
muricata	muricata	k1gFnSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
jednoděložné	jednoděložná	k1gFnPc1
(	(	kIx(
<g/>
Liliopsida	Liliopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
lipnicotvaré	lipnicotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Poales	Poales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
šáchorovité	šáchorovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Cyperaceae	Cyperacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
ostřice	ostřice	k1gFnSc1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Carex	Carex	k1gInSc1
muricataL	muricataL	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1753	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
muricata	muricata	k1gFnSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Vignea	Vigne	k2eAgFnSc1d1
muricata	muricata	k1gFnSc1
<g/>
,	,	kIx,
Carex	Carex	k1gInSc1
pairae	pairaat	k5eAaPmIp3nS
auct	auct	k5eAaPmF
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
jednoděložné	jednoděložný	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
šáchorovité	šáchorovitý	k2eAgFnSc2d1
(	(	kIx(
<g/>
Cyperaceae	Cyperacea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
udávána	udáván	k2eAgFnSc1d1
také	také	k9
pod	pod	k7c7
jménem	jméno	k1gNnSc7
tuřice	tuřice	k1gFnPc4
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druh	druh	k1gInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
taxonomicky	taxonomicky	k6eAd1
složitého	složitý	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
Carex	Carex	k1gInSc1
muricata	muricata	k1gFnSc1
agg	agg	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
ostřici	ostřice	k1gFnSc6
měkkoostenné	měkkoostenný	k2eAgFnSc6d1
v	v	k7c6
užším	úzký	k2eAgNnSc6d2
pojetí	pojetí	k1gNnSc6
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
muricata	muricata	k1gFnSc1
s.	s.	k?
str	str	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
rostlinu	rostlina	k1gFnSc4
dosahující	dosahující	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
nejčastěji	často	k6eAd3
30	#num#	k4
<g/>
–	–	k?
<g/>
110	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vytrvalá	vytrvalý	k2eAgFnSc1d1
a	a	k8xC
trsnatá	trsnatý	k2eAgFnSc1d1
s	s	k7c7
dřevnatým	dřevnatý	k2eAgInSc7d1
oddenkem	oddenek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
střídavé	střídavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
přisedlé	přisedlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
s	s	k7c7
listovými	listový	k2eAgFnPc7d1
pochvami	pochva	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodyha	lodyha	k1gFnSc1
je	být	k5eAaImIp3nS
trojhranná	trojhranný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nahoře	nahoře	k6eAd1
drsná	drsný	k2eAgFnSc1d1
<g/>
,	,	kIx,
delší	dlouhý	k2eAgFnSc1d2
než	než	k8xS
listy	list	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čepele	čepel	k1gInSc2
listu	list	k1gInSc2
jsou	být	k5eAaImIp3nP
asi	asi	k9
2	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
mm	mm	kA
široké	široký	k2eAgFnPc1d1
<g/>
,	,	kIx,
ploché	plochý	k2eAgFnPc1d1
nebo	nebo	k8xC
uprostřed	uprostřed	k7c2
žlábkovité	žlábkovitý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochvy	pochva	k1gFnPc4
dolních	dolní	k2eAgInPc2d1
listů	list	k1gInPc2
jsou	být	k5eAaImIp3nP
proužkované	proužkovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
hnědé	hnědý	k2eAgFnPc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
až	až	k6eAd1
černé	černý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bázi	báze	k1gFnSc6
listové	listový	k2eAgFnSc2d1
čepele	čepel	k1gFnSc2
je	být	k5eAaImIp3nS
jazýček	jazýček	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
širší	široký	k2eAgFnSc4d2
než	než	k8xS
delší	dlouhý	k2eAgFnSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
stejnoklasé	stejnoklasý	k2eAgFnPc4d1
ostřice	ostřice	k1gFnPc4
<g/>
,	,	kIx,
všechny	všechen	k3xTgInPc1
klásky	klásek	k1gInPc1
vypadají	vypadat	k5eAaImIp3nP,k5eAaPmIp3nP
víceméně	víceméně	k9
stejně	stejně	k6eAd1
a	a	k8xC
obsahují	obsahovat	k5eAaImIp3nP
samčí	samčí	k2eAgInPc4d1
a	a	k8xC
samičí	samičí	k2eAgInPc4d1
květy	květ	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
klásku	klásek	k1gInSc2
jsou	být	k5eAaImIp3nP
samičí	samičí	k2eAgInPc1d1
květy	květ	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
horní	horní	k2eAgFnSc6d1
samčí	samčí	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klásky	klásek	k1gInPc7
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgFnP
do	do	k7c2
cca	cca	kA
1,8	1,8	k4
<g/>
–	–	k?
<g/>
4,2	4,2	k4
cm	cm	kA
dlouhého	dlouhý	k2eAgInSc2d1
lichoklasu	lichoklas	k1gInSc2
(	(	kIx(
<g/>
klasu	klas	k1gInSc2
klásků	klásek	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
obsahuje	obsahovat	k5eAaImIp3nS
cca	cca	kA
4	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
klásků	klásek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
bázi	báze	k1gFnSc6
klásku	klásek	k1gInSc2
většinou	většinou	k6eAd1
nejsou	být	k5eNaImIp3nP
štětinovité	štětinovitý	k2eAgInPc4d1
listeny	listen	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
by	by	kYmCp3nP
výrazně	výrazně	k6eAd1
přesahovaly	přesahovat	k5eAaImAgFnP
klásek	klásek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květenství	květenství	k1gNnPc4
je	být	k5eAaImIp3nS
víceméně	víceméně	k9
souvislé	souvislý	k2eAgFnSc2d1
<g/>
,	,	kIx,
dolní	dolní	k2eAgFnSc2d1
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
klásky	klásek	k1gInPc7
jsou	být	k5eAaImIp3nP
jen	jen	k9
mírně	mírně	k6eAd1
oddáleny	oddálen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okvětí	okvětí	k1gNnPc4
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
samčích	samčí	k2eAgInPc6d1
květech	květ	k1gInPc6
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
3	#num#	k4
tyčinky	tyčinka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čnělky	čnělka	k1gFnPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
mošnička	mošnička	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
zralosti	zralost	k1gFnSc2
až	až	k9
hnědá	hnědat	k5eAaImIp3nS
<g/>
,	,	kIx,
vejcovitého	vejcovitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
cca	cca	kA
3,5	3,5	k4
<g/>
–	–	k?
<g/>
4,7	4,7	k4
mm	mm	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
náhle	náhle	k6eAd1
zúžená	zúžený	k2eAgFnSc1d1
do	do	k7c2
relativně	relativně	k6eAd1
krátkého	krátký	k2eAgInSc2d1
dvouzubého	dvouzubý	k2eAgInSc2d1
zobánku	zobánek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
mošnička	mošnička	k1gFnSc1
je	být	k5eAaImIp3nS
podepřená	podepřený	k2eAgFnSc1d1
plevou	pleva	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
za	za	k7c4
zralosti	zralost	k1gFnPc4
tmavá	tmavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
tmavohnědá	tmavohnědý	k2eAgFnSc1d1
až	až	k9
rezavě	rezavě	k6eAd1
hnědá	hnědý	k2eAgFnSc1d1
<g/>
,	,	kIx,
3,3	3,3	k4
<g/>
–	–	k?
<g/>
4,5	4,5	k4
mm	mm	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvete	kvést	k5eAaImIp3nS
nejčastěji	často	k6eAd3
v	v	k7c6
květnu	květen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
chromozómů	chromozóm	k1gInPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
n	n	k0
<g/>
=	=	kIx~
<g/>
56	#num#	k4
nebo	nebo	k8xC
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
roste	růst	k5eAaImIp3nS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
snad	snad	k9
sahá	sahat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
západní	západní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určit	určit	k5eAaPmF
přesné	přesný	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
je	být	k5eAaImIp3nS
těžké	těžký	k2eAgNnSc1d1
vzhledem	vzhledem	k7c3
k	k	k7c3
taxonomické	taxonomický	k2eAgFnSc3d1
obtížnosti	obtížnost	k1gFnSc3
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
V	v	k7c6
ČR	ČR	kA
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
běžně	běžně	k6eAd1
od	od	k7c2
nížin	nížina	k1gFnPc2
do	do	k7c2
podhůří	podhůří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roste	růst	k5eAaImIp3nS
hlavně	hlavně	k9
ve	v	k7c6
světlých	světlý	k2eAgInPc6d1
listnatých	listnatý	k2eAgInPc6d1
lesích	les	k1gInPc6
a	a	k8xC
v	v	k7c6
křovinách	křovina	k1gFnPc6
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
i	i	k9
na	na	k7c6
pasekách	paseka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Příbuzné	příbuzný	k2eAgInPc1d1
druhy	druh	k1gInPc1
</s>
<s>
V	v	k7c6
ČR	ČR	kA
roste	růst	k5eAaImIp3nS
ještě	ještě	k9
několik	několik	k4yIc4
dalších	další	k2eAgInPc2d1
druhů	druh	k1gInPc2
z	z	k7c2
taxonomicky	taxonomicky	k6eAd1
obtížné	obtížný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
ostřice	ostřice	k1gFnSc2
měkkoostenné	měkkoostenný	k2eAgInPc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
muricata	muricat	k1gMnSc2
agg	agg	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
ostřice	ostřice	k1gFnSc1
klasnatá	klasnatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
contigua	contigu	k1gInSc2
s.	s.	k?
str	str	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
Pairaova	Pairaov	k1gInSc2
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
pairae	paira	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
mnoholistá	mnoholistý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
leersiana	leersian	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostřice	ostřice	k1gFnSc1
Chabertova	Chabertův	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
chabertii	chabertie	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
ostřice	ostřice	k1gFnSc1
přetrhovaná	přetrhovaný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carex	Carex	k1gInSc1
divulsa	divuls	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Grulich	Grulich	k1gMnSc1
V.	V.	kA
et	et	k?
Řepka	řepka	k1gFnSc1
V	v	k7c6
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Carex	Carex	k1gInSc1
L.	L.	kA
In	In	k1gFnSc2
<g/>
:	:	kIx,
Klíč	klíč	k1gInSc1
ke	k	k7c3
Květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Kubát	Kubát	k1gMnSc1
K.	K.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Květena	květena	k1gFnSc1
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
Dostál	Dostál	k1gMnSc1
J.	J.	kA
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ostřice	ostřice	k1gFnSc1
měkkoostenná	měkkoostenný	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
–	–	k?
taxonomická	taxonomický	k2eAgFnSc1d1
studie	studie	k1gFnSc1
<g/>
,	,	kIx,
Radomír	Radomír	k1gMnSc1
Řepka	řepka	k1gFnSc1
<g/>
,	,	kIx,
Preslia	Preslia	k1gFnSc1
</s>
<s>
–	–	k?
mapky	mapka	k1gFnSc2
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Carex	Carex	k1gInSc1
interactive	interactiv	k1gInSc5
identification	identification	k1gInSc4
key	key	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
</s>
