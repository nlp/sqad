<s>
591	#num#	k4
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
←	←	k?
590	#num#	k4
</s>
<s>
591	#num#	k4
</s>
<s>
592	#num#	k4
→	→	k?
</s>
<s>
Celé	celý	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
591	#num#	k4
</s>
<s>
pět	pět	k4xCc4
set	sto	k4xCgNnPc2
devadesát	devadesát	k4xCc4
jedna	jeden	k4xCgFnSc1
</s>
<s>
Rozklad	rozklad	k1gInSc1
</s>
<s>
3	#num#	k4
·	·	k?
197	#num#	k4
</s>
<s>
Dělitelé	dělitel	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
197	#num#	k4
<g/>
,	,	kIx,
591	#num#	k4
</s>
<s>
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
</s>
<s>
DXCI	DXCI	kA
</s>
<s>
Dvojkově	dvojkově	k6eAd1
</s>
<s>
1001001111	#num#	k4
</s>
<s>
Trojkově	trojkově	k6eAd1
</s>
<s>
210220	#num#	k4
</s>
<s>
Čtyřkově	Čtyřkově	k6eAd1
</s>
<s>
21033	#num#	k4
</s>
<s>
Pětkově	pětkově	k6eAd1
</s>
<s>
4331	#num#	k4
</s>
<s>
Šestkově	šestkově	k6eAd1
</s>
<s>
2423	#num#	k4
</s>
<s>
Sedmičkově	Sedmičkově	k6eAd1
</s>
<s>
1503	#num#	k4
</s>
<s>
Osmičkově	osmičkově	k6eAd1
</s>
<s>
1117	#num#	k4
</s>
<s>
Dvanáctkově	dvanáctkově	k6eAd1
</s>
<s>
403	#num#	k4
</s>
<s>
Šestnáctkově	šestnáctkově	k6eAd1
</s>
<s>
24F	24F	k4
</s>
<s>
Pět	pět	k4xCc4
set	sto	k4xCgNnPc2
devadesát	devadesát	k4xCc4
jedna	jeden	k4xCgFnSc1
je	být	k5eAaImIp3nS
přirozené	přirozený	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
DXCI	DXCI	kA
a	a	k8xC
řeckými	řecký	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
φ	φ	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
po	po	k7c6
čísle	číslo	k1gNnSc6
pět	pět	k4xCc4
set	sto	k4xCgNnPc2
devadesát	devadesát	k4xCc1
a	a	k8xC
předchází	předcházet	k5eAaImIp3nS
číslu	číslo	k1gNnSc3
pět	pět	k4xCc4
set	sto	k4xCgNnPc2
devadesát	devadesát	k4xCc4
dva	dva	k4xCgInPc4
<g/>
.	.	kIx.
</s>
<s>
Matematika	matematika	k1gFnSc1
</s>
<s>
591	#num#	k4
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Deficientní	Deficientní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Poloprvočíslo	Poloprvočísnout	k5eAaPmAgNnS
</s>
<s>
Nešťastné	šťastný	k2eNgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
591	#num#	k4
</s>
<s>
591	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
500	#num#	k4
<g/>
–	–	k?
<g/>
599	#num#	k4
</s>
<s>
500	#num#	k4
•	•	k?
501	#num#	k4
•	•	k?
502	#num#	k4
•	•	k?
503	#num#	k4
•	•	k?
504	#num#	k4
•	•	k?
505	#num#	k4
•	•	k?
506	#num#	k4
•	•	k?
507	#num#	k4
•	•	k?
508	#num#	k4
•	•	k?
509	#num#	k4
•	•	k?
510	#num#	k4
•	•	k?
511	#num#	k4
•	•	k?
512	#num#	k4
•	•	k?
513	#num#	k4
•	•	k?
514	#num#	k4
•	•	k?
515	#num#	k4
•	•	k?
516	#num#	k4
•	•	k?
517	#num#	k4
•	•	k?
518	#num#	k4
•	•	k?
519	#num#	k4
•	•	k?
520	#num#	k4
•	•	k?
521	#num#	k4
•	•	k?
522	#num#	k4
•	•	k?
523	#num#	k4
•	•	k?
524	#num#	k4
•	•	k?
525	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
526	#num#	k4
•	•	k?
527	#num#	k4
•	•	k?
528	#num#	k4
•	•	k?
529	#num#	k4
•	•	k?
530	#num#	k4
•	•	k?
531	#num#	k4
•	•	k?
532	#num#	k4
•	•	k?
533	#num#	k4
•	•	k?
534	#num#	k4
•	•	k?
535	#num#	k4
•	•	k?
536	#num#	k4
•	•	k?
537	#num#	k4
•	•	k?
538	#num#	k4
•	•	k?
539	#num#	k4
•	•	k?
540	#num#	k4
•	•	k?
541	#num#	k4
•	•	k?
542	#num#	k4
•	•	k?
543	#num#	k4
•	•	k?
544	#num#	k4
•	•	k?
545	#num#	k4
•	•	k?
546	#num#	k4
•	•	k?
547	#num#	k4
•	•	k?
548	#num#	k4
•	•	k?
549	#num#	k4
•	•	k?
550	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
551	#num#	k4
•	•	k?
552	#num#	k4
•	•	k?
553	#num#	k4
•	•	k?
554	#num#	k4
•	•	k?
555	#num#	k4
•	•	k?
556	#num#	k4
•	•	k?
557	#num#	k4
•	•	k?
558	#num#	k4
•	•	k?
559	#num#	k4
•	•	k?
560	#num#	k4
•	•	k?
561	#num#	k4
•	•	k?
562	#num#	k4
•	•	k?
563	#num#	k4
•	•	k?
564	#num#	k4
•	•	k?
565	#num#	k4
•	•	k?
566	#num#	k4
•	•	k?
567	#num#	k4
•	•	k?
568	#num#	k4
•	•	k?
569	#num#	k4
•	•	k?
570	#num#	k4
•	•	k?
571	#num#	k4
•	•	k?
572	#num#	k4
•	•	k?
573	#num#	k4
•	•	k?
574	#num#	k4
•	•	k?
575	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
576	#num#	k4
•	•	k?
577	#num#	k4
•	•	k?
578	#num#	k4
•	•	k?
579	#num#	k4
•	•	k?
580	#num#	k4
•	•	k?
581	#num#	k4
•	•	k?
582	#num#	k4
•	•	k?
583	#num#	k4
•	•	k?
584	#num#	k4
•	•	k?
585	#num#	k4
•	•	k?
586	#num#	k4
•	•	k?
587	#num#	k4
•	•	k?
588	#num#	k4
•	•	k?
589	#num#	k4
•	•	k?
590	#num#	k4
•	•	k?
591	#num#	k4
•	•	k?
592	#num#	k4
•	•	k?
593	#num#	k4
•	•	k?
594	#num#	k4
•	•	k?
595	#num#	k4
•	•	k?
596	#num#	k4
•	•	k?
597	#num#	k4
•	•	k?
598	#num#	k4
•	•	k?
599	#num#	k4
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
99	#num#	k4
•	•	k?
100	#num#	k4
<g/>
–	–	k?
<g/>
199	#num#	k4
•	•	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
299	#num#	k4
•	•	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
399	#num#	k4
•	•	k?
400	#num#	k4
<g/>
–	–	k?
<g/>
499	#num#	k4
•	•	k?
500	#num#	k4
<g/>
–	–	k?
<g/>
599	#num#	k4
•	•	k?
600	#num#	k4
<g/>
–	–	k?
<g/>
699	#num#	k4
•	•	k?
700	#num#	k4
<g/>
–	–	k?
<g/>
799	#num#	k4
•	•	k?
800	#num#	k4
<g/>
–	–	k?
<g/>
899	#num#	k4
•	•	k?
900	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
•	•	k?
1000	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
