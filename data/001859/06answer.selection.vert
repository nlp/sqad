<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
Zoonomia	Zoonomia	k1gFnSc1	Zoonomia
(	(	kIx(	(
<g/>
1794	[number]	k4	1794
<g/>
-	-	kIx~	-
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
patologií	patologie	k1gFnSc7	patologie
<g/>
,	,	kIx,	,
anatomií	anatomie	k1gFnSc7	anatomie
a	a	k8xC	a
psychologií	psychologie	k1gFnSc7	psychologie
<g/>
.	.	kIx.	.
</s>
