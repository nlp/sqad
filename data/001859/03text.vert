<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1731	[number]	k4	1731
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgMnSc1d1	přírodní
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
fyziolog	fyziolog	k1gMnSc1	fyziolog
<g/>
,	,	kIx,	,
abolicionista	abolicionista	k1gMnSc1	abolicionista
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
básních	báseň	k1gFnPc6	báseň
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
přírodními	přírodní	k2eAgFnPc7d1	přírodní
vědami	věda	k1gFnPc7	věda
a	a	k8xC	a
psal	psát	k5eAaImAgInS	psát
i	i	k9	i
o	o	k7c4	o
evoluci	evoluce	k1gFnSc4	evoluce
a	a	k8xC	a
příbuznosti	příbuznost	k1gFnSc2	příbuznost
všech	všecek	k3xTgFnPc2	všecek
živých	živý	k2eAgFnPc2d1	živá
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
zakladatelem	zakladatel	k1gMnSc7	zakladatel
Měsíční	měsíční	k2eAgFnSc2d1	měsíční
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Lunar	Lunar	k1gInSc1	Lunar
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
Derbské	Derbský	k2eAgFnSc2d1	Derbský
filosofické	filosofický	k2eAgFnSc2d1	filosofická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
vnuky	vnuk	k1gMnPc7	vnuk
byli	být	k5eAaImAgMnP	být
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
a	a	k8xC	a
Francis	Francis	k1gFnSc1	Francis
Galton	Galton	k1gInSc1	Galton
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c4	v
Elston	Elston	k1gInSc4	Elston
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
Nottinghamshire	Nottinghamshir	k1gInSc5	Nottinghamshir
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Chesterfieldu	Chesterfield	k1gInSc6	Chesterfield
a	a	k8xC	a
poté	poté	k6eAd1	poté
St	St	kA	St
John	John	k1gMnSc1	John
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Lékařské	lékařský	k2eAgNnSc1d1	lékařské
vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1756	[number]	k4	1756
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Lichfieldu	Lichfield	k1gInSc2	Lichfield
a	a	k8xC	a
započal	započnout	k5eAaPmAgInS	započnout
tam	tam	k6eAd1	tam
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Dařilo	dařit	k5eAaImAgNnS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
britský	britský	k2eAgMnSc1d1	britský
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
místo	místo	k1gNnSc4	místo
královského	královský	k2eAgMnSc2d1	královský
lékaře	lékař	k1gMnSc2	lékař
<g/>
;	;	kIx,	;
Darwin	Darwin	k1gMnSc1	Darwin
ale	ale	k8xC	ale
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
14	[number]	k4	14
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1757	[number]	k4	1757
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
Marry	Marra	k1gFnPc4	Marra
Howardovou	Howardův	k2eAgFnSc7d1	Howardova
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
-	-	kIx~	-
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dva	dva	k4xCgMnPc1	dva
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
dcera	dcera	k1gFnSc1	dcera
<g/>
)	)	kIx)	)
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
Robert	Robert	k1gMnSc1	Robert
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Charlese	Charles	k1gMnSc2	Charles
Darwina	Darwin	k1gMnSc2	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
Marry	Marra	k1gFnSc2	Marra
Darwinová	Darwinová	k1gFnSc1	Darwinová
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
najala	najmout	k5eAaPmAgFnS	najmout
si	se	k3xPyFc3	se
rodina	rodina	k1gFnSc1	rodina
na	na	k7c6	na
hlídání	hlídání	k1gNnSc6	hlídání
Roberta	Robert	k1gMnSc4	Robert
guvernantku	guvernantka	k1gFnSc4	guvernantka
Mary	Mary	k1gFnSc1	Mary
Parkerovou	Parkerová	k1gFnSc7	Parkerová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
Erasmus	Erasmus	k1gMnSc1	Erasmus
Darwin	Darwin	k1gMnSc1	Darwin
sblížil	sblížit	k5eAaPmAgMnS	sblížit
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
se	se	k3xPyFc4	se
Mary	Mary	k1gFnSc1	Mary
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Darwin	Darwin	k1gMnSc1	Darwin
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
další	další	k2eAgFnSc4d1	další
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
vdanou	vdaný	k2eAgFnSc4d1	vdaná
Lucy	Lucy	k1gInPc4	Lucy
Swiftovou	Swiftová	k1gFnSc4	Swiftová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
poznal	poznat	k5eAaPmAgMnS	poznat
Elizabeth	Elizabeth	k1gFnSc4	Elizabeth
Poleovou	Poleův	k2eAgFnSc7d1	Poleův
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1780	[number]	k4	1780
její	její	k3xOp3gFnSc4	její
manžel	manžel	k1gMnSc1	manžel
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
Darwin	Darwin	k1gMnSc1	Darwin
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc4	čtyři
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
jeden	jeden	k4xCgInSc1	jeden
zemřel	zemřít	k5eAaPmAgInS	zemřít
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dcerami	dcera	k1gFnPc7	dcera
byla	být	k5eAaImAgFnS	být
Frances	Frances	k1gInSc4	Frances
Ann	Ann	k1gFnSc1	Ann
Violetta	Violetta	k1gFnSc1	Violetta
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Francise	Francise	k1gFnSc1	Francise
Galtona	Galtona	k1gFnSc1	Galtona
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
založil	založit	k5eAaPmAgMnS	založit
Lichfieldskou	Lichfieldský	k2eAgFnSc4d1	Lichfieldský
botanickou	botanický	k2eAgFnSc4d1	botanická
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přeložit	přeložit	k5eAaPmF	přeložit
díla	dílo	k1gNnSc2	dílo
švédského	švédský	k2eAgMnSc2d1	švédský
botanika	botanik	k1gMnSc2	botanik
Carla	Carl	k1gMnSc2	Carl
Linného	Linný	k1gMnSc2	Linný
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
díla	dílo	k1gNnPc4	dílo
<g/>
:	:	kIx,	:
Soustava	soustava	k1gFnSc1	soustava
zeleniny	zelenina	k1gFnSc2	zelenina
(	(	kIx(	(
<g/>
1785	[number]	k4	1785
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rostlinné	rostlinný	k2eAgInPc1d1	rostlinný
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
knihách	kniha	k1gFnPc6	kniha
Darwin	Darwin	k1gMnSc1	Darwin
zavedl	zavést	k5eAaPmAgMnS	zavést
několik	několik	k4yIc4	několik
anglických	anglický	k2eAgInPc2d1	anglický
názvů	název	k1gInPc2	název
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
napsal	napsat	k5eAaPmAgMnS	napsat
dvě	dva	k4xCgFnPc4	dva
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
The	The	k1gFnPc4	The
Loves	Lovesa	k1gFnPc2	Lovesa
of	of	k?	of
the	the	k?	the
Plants	Plants	k1gInSc1	Plants
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
Lásky	láska	k1gFnPc1	láska
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
a	a	k8xC	a
Economy	Econom	k1gInPc1	Econom
of	of	k?	of
Vegetation	Vegetation	k1gInSc1	Vegetation
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
Rostlinné	rostlinný	k2eAgNnSc1d1	rostlinné
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yRgFnPc2	který
pak	pak	k6eAd1	pak
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
knihu	kniha	k1gFnSc4	kniha
The	The	k1gFnSc2	The
Botanic	Botanice	k1gFnPc2	Botanice
Garden	Gardna	k1gFnPc2	Gardna
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
Zoonomia	Zoonomia	k1gFnSc1	Zoonomia
(	(	kIx(	(
<g/>
1794	[number]	k4	1794
<g/>
-	-	kIx~	-
<g/>
1796	[number]	k4	1796
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
patologií	patologie	k1gFnSc7	patologie
<g/>
,	,	kIx,	,
anatomií	anatomie	k1gFnSc7	anatomie
a	a	k8xC	a
psychologií	psychologie	k1gFnSc7	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
počáteční	počáteční	k2eAgFnPc4d1	počáteční
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c4	o
evoluci	evoluce	k1gFnSc4	evoluce
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
později	pozdě	k6eAd2	pozdě
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
.	.	kIx.	.
</s>
