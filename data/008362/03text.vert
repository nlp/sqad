<p>
<s>
Sir	sir	k1gMnSc1	sir
Clive	Cliev	k1gFnSc2	Cliev
Marles	Marles	k1gMnSc1	Marles
Sinclair	Sinclair	k1gMnSc1	Sinclair
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
podnikatel	podnikatel	k1gMnSc1	podnikatel
známý	známý	k1gMnSc1	známý
především	především	k6eAd1	především
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
<g/>
"	"	kIx"	"
domácích	domácí	k2eAgInPc2d1	domácí
počítačů	počítač	k1gInPc2	počítač
ZX	ZX	kA	ZX
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
ZX81	ZX81	k1gFnPc1	ZX81
a	a	k8xC	a
řady	řada	k1gFnPc1	řada
počítačů	počítač	k1gInPc2	počítač
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc1	Spectrum
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
prodat	prodat	k5eAaPmF	prodat
svoji	svůj	k3xOyFgFnSc4	svůj
firmu	firma	k1gFnSc4	firma
Sinclair	Sinclair	k1gInSc1	Sinclair
Research	Research	k1gMnSc1	Research
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
společnost	společnost	k1gFnSc4	společnost
Cambridge	Cambridge	k1gFnSc2	Cambridge
Computers	Computersa	k1gFnPc2	Computersa
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
výrobkem	výrobek	k1gInSc7	výrobek
byl	být	k5eAaImAgInS	být
počítač	počítač	k1gInSc1	počítač
Cambridge	Cambridge	k1gFnSc2	Cambridge
Z	z	k7c2	z
<g/>
88	[number]	k4	88
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
neujal	ujmout	k5eNaPmAgMnS	ujmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
jeho	jeho	k3xOp3gInSc7	jeho
43	[number]	k4	43
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
Sir	sir	k1gMnSc1	sir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Sinclair	Sinclair	k1gInSc1	Sinclair
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
Ann	Ann	k1gFnSc2	Ann
Trevor-Briscoe	Trevor-Brisco	k1gFnSc2	Trevor-Brisco
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
:	:	kIx,	:
Belindu	Belind	k1gInSc2	Belind
<g/>
,	,	kIx,	,
Crispina	Crispin	k2eAgInSc2d1	Crispin
and	and	k?	and
Bartholomewa	Bartholomew	k1gInSc2	Bartholomew
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
tanečnicí	tanečnice	k1gFnSc7	tanečnice
Angie	Angie	k1gFnSc2	Angie
Bowness	Bownessa	k1gFnPc2	Bownessa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
poznal	poznat	k5eAaPmAgMnS	poznat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc1	dětství
<g/>
,	,	kIx,	,
dospívání	dospívání	k1gNnSc1	dospívání
==	==	k?	==
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
otec	otec	k1gMnSc1	otec
George	Georg	k1gFnSc2	Georg
William	William	k1gInSc1	William
Carter	Carter	k1gMnSc1	Carter
Sinclair	Sinclair	k1gMnSc1	Sinclair
a	a	k8xC	a
děda	děd	k1gMnSc2	děd
George	Georg	k1gMnSc2	Georg
Sinclair	Sinclair	k1gInSc1	Sinclair
byly	být	k5eAaImAgFnP	být
techničtí	technický	k2eAgMnPc1d1	technický
inženýři	inženýr	k1gMnPc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
mladší	mladý	k2eAgMnSc1d2	mladší
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc4	bratr
Iaina	Iain	k1gMnSc4	Iain
a	a	k8xC	a
sestru	sestra	k1gFnSc4	sestra
Fionu	Fion	k1gInSc2	Fion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
byl	být	k5eAaImAgInS	být
raději	rád	k6eAd2	rád
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
dospělých	dospělý	k2eAgMnPc2d1	dospělý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yRgMnPc2	který
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
něco	něco	k3yInSc4	něco
naučit	naučit	k5eAaPmF	naučit
<g/>
.	.	kIx.	.
</s>
<s>
Cliva	Cliva	k6eAd1	Cliva
bavila	bavit	k5eAaImAgFnS	bavit
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
měl	mít	k5eAaImAgMnS	mít
plný	plný	k2eAgInSc4d1	plný
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
prošel	projít	k5eAaPmAgInS	projít
několik	několik	k4yIc4	několik
<g/>
,	,	kIx,	,
závěrečné	závěrečný	k2eAgFnPc4d1	závěrečná
zkoušky	zkouška	k1gFnPc4	zkouška
vykonal	vykonat	k5eAaPmAgInS	vykonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
na	na	k7c4	na
Highgate	Highgat	k1gInSc5	Highgat
school	school	k1gInSc1	school
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
byl	být	k5eAaImAgMnS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
v	v	k7c6	v
předmětech	předmět	k1gInPc6	předmět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ho	on	k3xPp3gNnSc4	on
bavily	bavit	k5eAaImAgFnP	bavit
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
předmětech	předmět	k1gInPc6	předmět
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ho	on	k3xPp3gNnSc4	on
nebavily	bavit	k5eNaImAgFnP	bavit
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
psal	psát	k5eAaImAgMnS	psát
příspěvky	příspěvek	k1gInPc4	příspěvek
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Practical	Practical	k1gFnSc2	Practical
Wireless	Wirelessa	k1gFnPc2	Wirelessa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nenastoupit	nastoupit	k5eNaPmF	nastoupit
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
raději	rád	k6eAd2	rád
přijal	přijmout	k5eAaPmAgMnS	přijmout
místo	místo	k7c2	místo
asistenta	asistent	k1gMnSc2	asistent
vydavatele	vydavatel	k1gMnSc2	vydavatel
tohoto	tento	k3xDgInSc2	tento
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
redakční	redakční	k2eAgFnSc1d1	redakční
rada	rada	k1gFnSc1	rada
tohoto	tento	k3xDgInSc2	tento
časopisu	časopis	k1gInSc2	časopis
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
ho	on	k3xPp3gInSc4	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Začátky	začátek	k1gInPc1	začátek
vlastního	vlastní	k2eAgNnSc2d1	vlastní
podnikání	podnikání	k1gNnSc2	podnikání
==	==	k?	==
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
společnost	společnost	k1gFnSc4	společnost
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
Radionics	Radionicsa	k1gFnPc2	Radionicsa
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
prvním	první	k4xOgInSc7	první
výrobkem	výrobek	k1gInSc7	výrobek
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
miniaturní	miniaturní	k2eAgNnSc4d1	miniaturní
rádio	rádio	k1gNnSc4	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgInS	mít
dostatek	dostatek	k1gInSc4	dostatek
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgInS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
místa	místo	k1gNnSc2	místo
technického	technický	k2eAgMnSc2d1	technický
redaktora	redaktor	k1gMnSc2	redaktor
časopisu	časopis	k1gInSc2	časopis
Instrumental	Instrumental	k1gMnSc1	Instrumental
Practice	Practice	k1gFnSc1	Practice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
bližšího	blízký	k2eAgInSc2d2	bližší
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
výrobci	výrobce	k1gMnPc7	výrobce
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
časopisu	časopis	k1gInSc2	časopis
Clive	Cliev	k1gFnSc2	Cliev
Sinclair	Sinclair	k1gMnSc1	Sinclair
pracoval	pracovat	k5eAaImAgMnS	pracovat
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
výrobkem	výrobek	k1gInSc7	výrobek
společnosti	společnost	k1gFnSc2	společnost
Sinclair	Sinclair	k1gInSc1	Sinclair
Radionics	Radionics	k1gInSc1	Radionics
byl	být	k5eAaImAgInS	být
miniaturní	miniaturní	k2eAgInSc4d1	miniaturní
zesilovač	zesilovač	k1gInSc4	zesilovač
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
výrobek	výrobek	k1gInSc1	výrobek
<g/>
,	,	kIx,	,
miniaturní	miniaturní	k2eAgNnSc1d1	miniaturní
rádio	rádio	k1gNnSc1	rádio
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
Slimline	Slimlin	k1gInSc5	Slimlin
<g/>
,	,	kIx,	,
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
větší	veliký	k2eAgInSc4d2	veliký
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
než	než	k8xS	než
Sinclair	Sinclair	k1gMnSc1	Sinclair
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
urychlení	urychlení	k1gNnSc2	urychlení
výroby	výroba	k1gFnSc2	výroba
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
stavebnice	stavebnice	k1gFnSc1	stavebnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgInSc1	první
počítač	počítač	k1gInSc1	počítač
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
počítač	počítač	k1gInSc1	počítač
Commodore	Commodor	k1gInSc5	Commodor
PET	PET	kA	PET
za	za	k7c4	za
700	[number]	k4	700
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
noviny	novina	k1gFnSc2	novina
The	The	k1gMnSc1	The
Financial	Financial	k1gMnSc1	Financial
Times	Times	k1gInSc4	Times
předpovídaly	předpovídat	k5eAaImAgFnP	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ceny	cena	k1gFnPc1	cena
počítačů	počítač	k1gInPc2	počítač
by	by	kYmCp3nP	by
během	během	k7c2	během
pěti	pět	k4xCc3	pět
let	léto	k1gNnPc2	léto
mohly	moct	k5eAaImAgFnP	moct
klesnout	klesnout	k5eAaPmF	klesnout
až	až	k9	až
na	na	k7c4	na
100	[number]	k4	100
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Sinclair	Sinclair	k1gInSc1	Sinclair
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
toto	tento	k3xDgNnSc4	tento
snížení	snížení	k1gNnSc4	snížení
ceny	cena	k1gFnSc2	cena
"	"	kIx"	"
<g/>
urychlit	urychlit	k5eAaPmF	urychlit
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
objevil	objevit	k5eAaPmAgInS	objevit
počítač	počítač	k1gInSc1	počítač
ZX	ZX	kA	ZX
<g/>
80	[number]	k4	80
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
99,95	[number]	k4	99,95
liber	libra	k1gFnPc2	libra
jako	jako	k8xS	jako
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnPc1d1	funkční
a	a	k8xC	a
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
79	[number]	k4	79
liber	libra	k1gFnPc2	libra
jako	jako	k8xC	jako
stavebnice	stavebnice	k1gFnSc2	stavebnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1980	[number]	k4	1980
už	už	k9	už
firma	firma	k1gFnSc1	firma
Sinclair	Sinclair	k1gMnSc1	Sinclair
Research	Research	k1gMnSc1	Research
měla	mít	k5eAaImAgNnP	mít
prodaných	prodaný	k2eAgNnPc2d1	prodané
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
počítače	počítač	k1gInSc2	počítač
ZX	ZX	kA	ZX
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
přídavná	přídavný	k2eAgFnSc1d1	přídavná
paměť	paměť	k1gFnSc1	paměť
pro	pro	k7c4	pro
ZX80	ZX80	k1gFnSc4	ZX80
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
16	[number]	k4	16
KiB	KiB	k1gFnPc2	KiB
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
70	[number]	k4	70
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
počítače	počítač	k1gInSc2	počítač
ZX	ZX	kA	ZX
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úpadek	úpadek	k1gInSc1	úpadek
Sinclair	Sinclair	k1gMnSc1	Sinclair
Research	Research	k1gMnSc1	Research
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
počítačích	počítač	k1gInPc6	počítač
ZX81	ZX81	k1gFnSc1	ZX81
a	a	k8xC	a
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc1	Spectrum
Sinclair	Sinclair	k1gMnSc1	Sinclair
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
počítač	počítač	k1gInSc4	počítač
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
QL	QL	kA	QL
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
největším	veliký	k2eAgInSc7d3	veliký
triumfem	triumf	k1gInSc7	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Počítač	počítač	k1gInSc1	počítač
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
trh	trh	k1gInSc4	trh
jako	jako	k8xS	jako
nedokončený	dokončený	k2eNgInSc4d1	nedokončený
a	a	k8xC	a
obsahující	obsahující	k2eAgInSc4d1	obsahující
mnoho	mnoho	k4c1	mnoho
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Sinclairovo	Sinclairův	k2eAgNnSc1d1	Sinclairův
jméno	jméno	k1gNnSc1	jméno
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nekompatibilitě	nekompatibilita	k1gFnSc3	nekompatibilita
s	s	k7c7	s
počítači	počítač	k1gMnPc7	počítač
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc1	Spectrum
se	s	k7c7	s
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
QL	QL	kA	QL
příliš	příliš	k6eAd1	příliš
neprodával	prodávat	k5eNaImAgMnS	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
financí	finance	k1gFnPc2	finance
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
vyčerpáno	vyčerpán	k2eAgNnSc1d1	vyčerpáno
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
elektromobilu	elektromobil	k1gInSc2	elektromobil
Sinclair	Sinclair	k1gInSc1	Sinclair
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
nezachránilo	zachránit	k5eNaPmAgNnS	zachránit
ani	ani	k8xC	ani
uvedení	uvedení	k1gNnSc1	uvedení
počítače	počítač	k1gInSc2	počítač
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc4	Spectrum
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
trh	trh	k1gInSc1	trh
již	již	k9	již
byl	být	k5eAaImAgInS	být
počítači	počítač	k1gInPc7	počítač
nasycen	nasytit	k5eAaPmNgInS	nasytit
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
počítače	počítač	k1gInSc2	počítač
ZX	ZX	kA	ZX
Spectrum	Spectrum	k1gNnSc4	Spectrum
128	[number]	k4	128
<g/>
+	+	kIx~	+
Sinclair	Sinclair	k1gMnSc1	Sinclair
prodal	prodat	k5eAaPmAgMnS	prodat
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
divizi	divize	k1gFnSc4	divize
svojí	svůj	k3xOyFgFnSc2	svůj
společnosti	společnost	k1gFnSc2	společnost
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
Research	Research	k1gMnSc1	Research
společnosti	společnost	k1gFnSc2	společnost
Amstrad	Amstrad	k1gInSc1	Amstrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
je	být	k5eAaImIp3nS	být
Clive	Cliev	k1gFnPc4	Cliev
Sinclair	Sinclair	k1gMnSc1	Sinclair
jediným	jediný	k2eAgMnSc7d1	jediný
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
Sinclair	Sinclair	k1gMnSc1	Sinclair
Research	Research	k1gMnSc1	Research
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
aktivity	aktivita	k1gFnPc1	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Sinclair	Sinclair	k1gMnSc1	Sinclair
založil	založit	k5eAaPmAgMnS	založit
firmu	firma	k1gFnSc4	firma
Cambridge	Cambridge	k1gFnSc2	Cambridge
Computers	Computers	k1gInSc1	Computers
a	a	k8xC	a
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
počítač	počítač	k1gInSc1	počítač
Cambridge	Cambridge	k1gFnSc2	Cambridge
Z	z	k7c2	z
<g/>
88	[number]	k4	88
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
neujal	ujmout	k5eNaPmAgMnS	ujmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sinclair	Sinclair	k1gMnSc1	Sinclair
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
věnuje	věnovat	k5eAaImIp3nS	věnovat
vývoji	vývoj	k1gInSc3	vývoj
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
prorazit	prorazit	k5eAaPmF	prorazit
i	i	k9	i
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dopravy	doprava	k1gFnSc2	doprava
i	i	k9	i
přes	přes	k7c4	přes
neúspěch	neúspěch	k1gInSc4	neúspěch
elektromobilu	elektromobil	k1gInSc2	elektromobil
Sinclair	Sinclair	k1gInSc1	Sinclair
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
elektrokolo	elektrokola	k1gFnSc5	elektrokola
Zike	Zike	k1gNnSc1	Zike
<g/>
,	,	kIx,	,
skládací	skládací	k2eAgNnSc1d1	skládací
kolo	kolo	k1gNnSc1	kolo
A-bike	Aike	k1gNnSc1	A-bike
či	či	k8xC	či
elektrické	elektrický	k2eAgNnSc1d1	elektrické
kolo	kolo	k1gNnSc1	kolo
Sinclair	Sinclair	k1gInSc1	Sinclair
X-	X-	k1gFnSc1	X-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
-yves-	ves-	k?	-yves-
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Sinclair	Sinclair	k1gInSc1	Sinclair
Story	story	k1gFnPc2	story
alebo	aleba	k1gFnSc5	aleba
ako	ako	k?	ako
vznikol	vznikol	k1gInSc1	vznikol
prvý	prvý	k4xOgInSc4	prvý
ľudový	ľudový	k2eAgInSc4d1	ľudový
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Bit	bit	k1gInSc1	bit
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
92	[number]	k4	92
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
10	[number]	k4	10
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
242	[number]	k4	242
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
-yves-	ves-	k?	-yves-
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Sinclair	Sinclair	k1gInSc1	Sinclair
Story	story	k1gFnPc2	story
alebo	aleba	k1gFnSc5	aleba
ako	ako	k?	ako
vznikol	vznikol	k1gInSc1	vznikol
prvý	prvý	k4xOgInSc4	prvý
ľudový	ľudový	k2eAgInSc4d1	ľudový
počítač	počítač	k1gInSc4	počítač
(	(	kIx(	(
<g/>
dokončenie	dokončenie	k1gFnSc1	dokončenie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bit	bit	k1gInSc1	bit
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
92	[number]	k4	92
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
10	[number]	k4	10
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
242	[number]	k4	242
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ERBEN	Erben	k1gMnSc1	Erben
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
hackerů	hacker	k1gMnPc2	hacker
<g/>
:	:	kIx,	:
sir	sir	k1gMnSc1	sir
Clive	Cliev	k1gFnSc2	Cliev
Sinclair	Sinclaira	k1gFnPc2	Sinclaira
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
"	"	kIx"	"
<g/>
gumák	gumák	k1gInSc1	gumák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Root	Root	k1gInSc1	Root
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2013-10-08	[number]	k4	2013-10-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1212	[number]	k4	1212
<g/>
-	-	kIx~	-
<g/>
8309	[number]	k4	8309
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Clive	Cliev	k1gFnSc2	Cliev
Sinclair	Sinclair	k1gInSc1	Sinclair
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Počítačový	počítačový	k2eAgInSc1d1	počítačový
<g/>
"	"	kIx"	"
Sinclair	Sinclair	k1gInSc1	Sinclair
dnes	dnes	k6eAd1	dnes
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
létajících	létající	k2eAgInPc6d1	létající
autech	aut	k1gInPc6	aut
na	na	k7c6	na
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Sinclair	Sinclair	k1gMnSc1	Sinclair
X-1	X-1	k1gMnSc1	X-1
-	-	kIx~	-
elektroskútr	elektroskútr	k1gMnSc1	elektroskútr
nebo	nebo	k8xC	nebo
elektrokolo	elektrokola	k1gFnSc5	elektrokola
<g/>
?	?	kIx.	?
</s>
<s>
na	na	k7c4	na
Hybrid	hybrid	k1gInSc4	hybrid
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Sinclair	Sinclair	k1gMnSc1	Sinclair
předvídá	předvídat	k5eAaImIp3nS	předvídat
létající	létající	k2eAgNnPc4d1	létající
auta	auto	k1gNnPc4	auto
na	na	k7c4	na
elektřinu	elektřina	k1gFnSc4	elektřina
na	na	k7c4	na
Hybrid	hybrid	k1gInSc4	hybrid
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Sir	sir	k1gMnSc1	sir
Clive	Cliev	k1gFnSc2	Cliev
Sinclair	Sinclair	k1gMnSc1	Sinclair
uvádí	uvádět	k5eAaImIp3nS	uvádět
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
vynález	vynález	k1gInSc4	vynález
-	-	kIx~	-
skládací	skládací	k2eAgNnSc4d1	skládací
kolo	kolo	k1gNnSc4	kolo
na	na	k7c6	na
zive	zive	k1gFnSc6	zive
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Horizon	Horizon	k1gInSc1	Horizon
<g/>
:	:	kIx,	:
Clive	Cliev	k1gFnSc2	Cliev
Sinclair	Sinclair	k1gMnSc1	Sinclair
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Anatomy	anatom	k1gMnPc4	anatom
of	of	k?	of
an	an	k?	an
Inventor	Inventor	k1gInSc4	Inventor
-	-	kIx~	-
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
o	o	k7c6	o
Clivu	Cliv	k1gMnSc6	Cliv
Sinclairovi	Sinclair	k1gMnSc6	Sinclair
</s>
</p>
<p>
<s>
Micro	Micro	k1gNnSc1	Micro
Men	Men	k1gFnSc2	Men
-	-	kIx~	-
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
filmu	film	k1gInSc6	film
dokumentujícího	dokumentující	k2eAgInSc2d1	dokumentující
část	část	k1gFnSc1	část
života	život	k1gInSc2	život
Cliva	Cliva	k1gFnSc1	Cliva
Sinclaira	Sinclaira	k1gFnSc1	Sinclaira
</s>
</p>
