<s>
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
je	být	k5eAaImIp3nS	být
profesionální	profesionální	k2eAgInSc1d1	profesionální
americký	americký	k2eAgInSc1d1	americký
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
Metropolitní	metropolitní	k2eAgFnSc4d1	metropolitní
divizi	divize	k1gFnSc4	divize
v	v	k7c6	v
severoamerické	severoamerický	k2eAgFnSc6d1	severoamerická
NHL	NHL	kA	NHL
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týmu	tým	k1gInSc6	tým
hrají	hrát	k5eAaImIp3nP	hrát
současné	současný	k2eAgFnPc4d1	současná
hokejové	hokejový	k2eAgFnPc4d1	hokejová
hvězdy	hvězda	k1gFnPc4	hvězda
Sidney	Sidnea	k1gMnSc2	Sidnea
Crosby	Crosba	k1gMnSc2	Crosba
a	a	k8xC	a
Jevgenij	Jevgenij	k1gMnSc2	Jevgenij
Malkin	Malkina	k1gFnPc2	Malkina
<g/>
.	.	kIx.	.
<g/>
Visí	viset	k5eAaImIp3nS	viset
tam	tam	k6eAd1	tam
i	i	k9	i
dres	dres	k1gInSc4	dres
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Jágra	Jágr	k1gMnSc2	Jágr
<g/>
.	.	kIx.	.
</s>
<s>
NHL	NHL	kA	NHL
<g/>
/	/	kIx~	/
<g/>
Sheraton	Sheraton	k1gInSc1	Sheraton
Road	Road	k1gMnSc1	Road
Performer	Performer	k1gMnSc1	Performer
Award	Award	k1gMnSc1	Award
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Malkin	Malkin	k1gMnSc1	Malkin
<g/>
:	:	kIx,	:
2008-2009	[number]	k4	2008-2009
Ab	Ab	k1gMnSc1	Ab
McDonald	McDonald	k1gMnSc1	McDonald
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
bez	bez	k7c2	bez
kapitána	kapitán	k1gMnSc2	kapitán
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
Ron	Ron	k1gMnSc1	Ron
Schock	Schock	k1gMnSc1	Schock
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
Jean	Jean	k1gMnSc1	Jean
Pronovost	Pronovost	k1gFnSc4	Pronovost
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
Orest	Orest	k1gMnSc1	Orest
Kindrachuk	Kindrachuk	k1gMnSc1	Kindrachuk
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1981	[number]	k4	1981
Randy	rand	k1gInPc4	rand
Carlyle	Carlyl	k1gInSc5	Carlyl
<g />
.	.	kIx.	.
</s>
<s>
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
Mike	Mike	k1gNnSc7	Mike
Bullard	Bullarda	k1gFnPc2	Bullarda
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
Mike	Mike	k1gNnPc2	Mike
Bullard	Bullarda	k1gFnPc2	Bullarda
a	a	k8xC	a
Terry	Terra	k1gFnSc2	Terra
Ruskowski	Ruskowsk	k1gFnSc2	Ruskowsk
1986	[number]	k4	1986
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
Dan	Dana	k1gFnPc2	Dana
Frawley	Frawlea	k1gFnSc2	Frawlea
a	a	k8xC	a
Mario	Mario	k1gMnSc1	Mario
Lemieux	Lemieux	k1gInSc4	Lemieux
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
Mario	Mario	k1gMnSc1	Mario
Lemieux	Lemieux	k1gInSc4	Lemieux
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
Ron	ron	k1gInSc1	ron
Francis	Francis	k1gFnSc2	Francis
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
Mario	Mario	k1gMnSc1	Mario
Lemieux	Lemieux	k1gInSc4	Lemieux
1995	[number]	k4	1995
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
Ron	ron	k1gInSc1	ron
Francis	Francis	k1gFnSc2	Francis
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
Mario	Mario	k1gMnSc1	Mario
Lemieux	Lemieux	k1gInSc4	Lemieux
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
Mario	Mario	k1gMnSc1	Mario
Lemieux	Lemieux	k1gInSc1	Lemieux
&	&	k?	&
bez	bez	k7c2	bez
kapitána	kapitán	k1gMnSc2	kapitán
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
bez	bez	k7c2	bez
kapitána	kapitán	k1gMnSc2	kapitán
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
Sidney	Sidnea	k1gFnSc2	Sidnea
Crosby	Crosba	k1gFnSc2	Crosba
2007-	[number]	k4	2007-
Tučně	tučně	k6eAd1	tučně
-	-	kIx~	-
Stanley	Stanlea	k1gFnPc1	Stanlea
Cup	cup	k1gInSc4	cup
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc4	Penguins
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Pittsburgh	Pittsburgha	k1gFnPc2	Pittsburgha
Penguins	Penguinsa	k1gFnPc2	Penguinsa
Česká	český	k2eAgFnSc1d1	Česká
stránka	stránka	k1gFnSc1	stránka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
klubu	klub	k1gInSc6	klub
</s>
