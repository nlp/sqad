<s>
DotA	DotA	k?	DotA
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
Defense	defense	k1gFnSc2	defense
of	of	k?	of
the	the	k?	the
Ancients	Ancients	k1gInSc1	Ancients
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
Obrana	obrana	k1gFnSc1	obrana
Prastarých	prastarý	k2eAgMnPc2d1	prastarý
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
mapa	mapa	k1gFnSc1	mapa
hry	hra	k1gFnSc2	hra
Warcraft	Warcraft	k1gInSc1	Warcraft
3	[number]	k4	3
The	The	k1gFnSc2	The
Frozen	Frozna	k1gFnPc2	Frozna
Throne	Thron	k1gInSc5	Thron
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
technicky	technicky	k6eAd1	technicky
podporována	podporovat	k5eAaImNgFnS	podporovat
společností	společnost	k1gFnSc7	společnost
Blizzard	Blizzard	k1gMnSc1	Blizzard
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
týmovou	týmový	k2eAgFnSc4d1	týmová
strategickou	strategický	k2eAgFnSc4d1	strategická
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
nejvíce	nejvíce	k6eAd1	nejvíce
pro	pro	k7c4	pro
týmy	tým	k1gInPc4	tým
tří	tři	k4xCgFnPc2	tři
proti	proti	k7c3	proti
třem	tři	k4xCgFnPc3	tři
a	a	k8xC	a
pěti	pět	k4xCc3	pět
proti	proti	k7c3	proti
pěti	pět	k4xCc3	pět
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
zápasy	zápas	k1gInPc1	zápas
dvou	dva	k4xCgInPc2	dva
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
jednoho	jeden	k4xCgInSc2	jeden
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
při	při	k7c6	při
dvou	dva	k4xCgInPc6	dva
členech	člen	k1gInPc6	člen
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pokrýt	pokrýt	k5eAaPmF	pokrýt
celou	celý	k2eAgFnSc4d1	celá
mapu	mapa	k1gFnSc4	mapa
a	a	k8xC	a
u	u	k7c2	u
hry	hra	k1gFnSc2	hra
jednoho	jeden	k4xCgInSc2	jeden
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
týmová	týmový	k2eAgFnSc1d1	týmová
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
hráčů	hráč	k1gMnPc2	hráč
je	být	k5eAaImIp3nS	být
proniknout	proniknout	k5eAaPmF	proniknout
do	do	k7c2	do
základny	základna	k1gFnSc2	základna
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
týmu	tým	k1gInSc2	tým
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
budovami	budova	k1gFnPc7	budova
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zničit	zničit	k5eAaPmF	zničit
nejdříve	dříve	k6eAd3	dříve
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dokud	dokud	k6eAd1	dokud
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
budova	budova	k1gFnSc1	budova
"	"	kIx"	"
<g/>
nezranitelná	zranitelný	k2eNgFnSc1d1	nezranitelná
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
předčasnému	předčasný	k2eAgNnSc3d1	předčasné
ukončování	ukončování	k1gNnSc3	ukončování
hry	hra	k1gFnSc2	hra
hráči	hráč	k1gMnPc1	hráč
nerespektující	respektující	k2eNgInSc4d1	nerespektující
psaná	psaný	k2eAgNnPc4d1	psané
i	i	k8xC	i
nepsaná	psaný	k2eNgNnPc4d1	nepsané
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
myšlen	myšlen	k2eAgInSc4d1	myšlen
3D	[number]	k4	3D
objekt	objekt	k1gInSc4	objekt
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
engine	enginout	k5eAaPmIp3nS	enginout
hry	hra	k1gFnPc4	hra
ani	ani	k8xC	ani
slang	slang	k1gInSc4	slang
komunity	komunita	k1gFnSc2	komunita
nijak	nijak	k6eAd1	nijak
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
týmu	tým	k1gInSc2	tým
Pohromy	pohroma	k1gFnSc2	pohroma
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Scourge	Scourge	k1gInSc1	Scourge
-	-	kIx~	-
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
mapy	mapa	k1gFnSc2	mapa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
budovou	budova	k1gFnSc7	budova
Ledový	ledový	k2eAgInSc4d1	ledový
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
u	u	k7c2	u
týmu	tým	k1gInSc2	tým
Strážců	strážce	k1gMnPc2	strážce
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Sentinel	sentinel	k1gInSc1	sentinel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gInSc7	on
Strom	strom	k1gInSc1	strom
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zničení	zničení	k1gNnSc4	zničení
tohoto	tento	k3xDgInSc2	tento
hlavního	hlavní	k2eAgInSc2d1	hlavní
objektu	objekt	k1gInSc2	objekt
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
ta	ten	k3xDgFnSc1	ten
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
budovu	budova	k1gFnSc4	budova
zničila	zničit	k5eAaPmAgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
průběhu	průběh	k1gInSc6	průběh
a	a	k8xC	a
okolnostech	okolnost	k1gFnPc6	okolnost
trvat	trvat	k5eAaImF	trvat
od	od	k7c2	od
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
až	až	k9	až
po	po	k7c4	po
dvě	dva	k4xCgFnPc4	dva
i	i	k8xC	i
více	hodně	k6eAd2	hodně
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
však	však	k9	však
trvá	trvat	k5eAaImIp3nS	trvat
kolem	kolem	k7c2	kolem
45	[number]	k4	45
až	až	k9	až
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
ovládá	ovládat	k5eAaImIp3nS	ovládat
jediného	jediný	k2eAgMnSc4d1	jediný
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
probíjí	probíjet	k5eAaImIp3nP	probíjet
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
soupeřově	soupeřův	k2eAgFnSc3d1	soupeřova
základně	základna	k1gFnSc3	základna
přes	přes	k7c4	přes
nepřátelské	přátelský	k2eNgFnPc4d1	nepřátelská
jednotky	jednotka	k1gFnPc4	jednotka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Creep	Creep	k1gInSc1	Creep
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
automaticky	automaticky	k6eAd1	automaticky
a	a	k8xC	a
současně	současně	k6eAd1	současně
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
u	u	k7c2	u
budov	budova	k1gFnPc2	budova
obklopujících	obklopující	k2eAgFnPc2d1	obklopující
hlavní	hlavní	k2eAgFnSc4d1	hlavní
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jednotky	jednotka	k1gFnPc1	jednotka
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
nelze	lze	k6eNd1	lze
ovládat	ovládat	k5eAaImF	ovládat
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
bezhlavě	bezhlavě	k6eAd1	bezhlavě
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
nepřátele	nepřítel	k1gMnPc4	nepřítel
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
nepřátelské	přátelský	k2eNgFnSc3d1	nepřátelská
základně	základna	k1gFnSc3	základna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
vedlejším	vedlejší	k2eAgMnSc7d1	vedlejší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
méně	málo	k6eAd2	málo
podstatným	podstatný	k2eAgInSc7d1	podstatný
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dodávat	dodávat	k5eAaImF	dodávat
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnPc4	zkušenost
hrdinům	hrdina	k1gMnPc3	hrdina
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
je	on	k3xPp3gFnPc4	on
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rostla	růst	k5eAaImAgFnS	růst
síla	síla	k1gFnSc1	síla
hráčových	hráčův	k2eAgMnPc2d1	hráčův
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
si	se	k3xPyFc3	se
dovolit	dovolit	k5eAaPmF	dovolit
nakoupit	nakoupit	k5eAaPmF	nakoupit
lepší	dobrý	k2eAgNnSc4d2	lepší
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hráčům	hráč	k1gMnPc3	hráč
v	v	k7c6	v
postupu	postup	k1gInSc6	postup
brání	bránit	k5eAaImIp3nP	bránit
hrdinové	hrdina	k1gMnPc1	hrdina
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síla	k1gFnPc4	síla
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
schopnostech	schopnost	k1gFnPc6	schopnost
samotných	samotný	k2eAgMnPc2d1	samotný
hráčů	hráč	k1gMnPc2	hráč
zkoordinovat	zkoordinovat	k5eAaPmF	zkoordinovat
své	svůj	k3xOyFgInPc4	svůj
útoky	útok	k1gInPc4	útok
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
využívat	využívat	k5eAaPmF	využívat
silných	silný	k2eAgFnPc2d1	silná
stránek	stránka	k1gFnPc2	stránka
svých	svůj	k3xOyFgMnPc2	svůj
hrdinů	hrdina	k1gMnPc2	hrdina
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
i	i	k8xC	i
defenzívě	defenzíva	k1gFnSc6	defenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
je	být	k5eAaImIp3nS	být
mapa	mapa	k1gFnSc1	mapa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
základ	základ	k1gInSc4	základ
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgMnSc1d1	souměrný
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
místa	místo	k1gNnSc2	místo
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgFnPc7	tři
hlavními	hlavní	k2eAgFnPc7d1	hlavní
silnicemi	silnice	k1gFnPc7	silnice
do	do	k7c2	do
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
základen	základna	k1gFnPc2	základna
se	se	k3xPyFc4	se
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráčský	hráčský	k2eAgMnSc1d1	hráčský
hrdina	hrdina	k1gMnSc1	hrdina
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgMnSc1d1	unikátní
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc4d1	jiný
vzhled	vzhled	k1gInSc4	vzhled
i	i	k8xC	i
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
jiný	jiný	k2eAgInSc4d1	jiný
styl	styl	k1gInSc4	styl
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
hrdinu	hrdina	k1gMnSc4	hrdina
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
různá	různý	k2eAgFnSc1d1	různá
kombinace	kombinace	k1gFnSc1	kombinace
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
hrdina	hrdina	k1gMnSc1	hrdina
není	být	k5eNaImIp3nS	být
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
ani	ani	k8xC	ani
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
proti	proti	k7c3	proti
každému	každý	k3xTgNnSc3	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
najde	najít	k5eAaPmIp3nS	najít
nějaký	nějaký	k3yIgMnSc1	nějaký
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
v	v	k7c6	v
boji	boj	k1gInSc6	boj
efektivní	efektivní	k2eAgFnSc4d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
Nejefektivnější	efektivní	k2eAgFnSc1d3	nejefektivnější
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
týmová	týmový	k2eAgFnSc1d1	týmová
hra	hra	k1gFnSc1	hra
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
pozorné	pozorný	k2eAgNnSc4d1	pozorné
sledování	sledování	k1gNnSc4	sledování
kroků	krok	k1gInPc2	krok
soupeře	soupeř	k1gMnSc2	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Nájezd	nájezd	k1gInSc4	nájezd
několika	několik	k4yIc2	několik
hrdinů	hrdina	k1gMnPc2	hrdina
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
nepozorného	pozorný	k2eNgMnSc4d1	nepozorný
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většina	k1gFnSc7	většina
smrtící	smrtící	k2eAgFnSc7d1	smrtící
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
hrdina	hrdina	k1gMnSc1	hrdina
může	moct	k5eAaImIp3nS	moct
nést	nést	k5eAaImF	nést
až	až	k9	až
6	[number]	k4	6
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
jejich	jejich	k3xOp3gFnPc2	jejich
možných	možný	k2eAgFnPc2d1	možná
kombinací	kombinace	k1gFnPc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
hrdinova	hrdinův	k2eAgInSc2d1	hrdinův
inventáře	inventář	k1gInSc2	inventář
hráč	hráč	k1gMnSc1	hráč
volí	volit	k5eAaImIp3nS	volit
podle	podle	k7c2	podle
stylu	styl	k1gInSc2	styl
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
preferuje	preferovat	k5eAaImIp3nS	preferovat
<g/>
,	,	kIx,	,
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
podle	podle	k7c2	podle
potřeb	potřeba	k1gFnPc2	potřeba
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hrdina	hrdina	k1gMnSc1	hrdina
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
odstraněn	odstranit	k5eAaPmNgInS	odstranit
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
po	po	k7c4	po
uplynutí	uplynutí	k1gNnSc4	uplynutí
tohoto	tento	k3xDgInSc2	tento
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
narodí	narodit	k5eAaPmIp3nP	narodit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
základně	základna	k1gFnSc6	základna
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
u	u	k7c2	u
fontány	fontána	k1gFnSc2	fontána
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
hráči	hráč	k1gMnPc1	hráč
mohou	moct	k5eAaImIp3nP	moct
doléčit	doléčit	k5eAaPmF	doléčit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
se	s	k7c7	s
hráči	hráč	k1gMnPc7	hráč
strhne	strhnout	k5eAaPmIp3nS	strhnout
určitý	určitý	k2eAgInSc1d1	určitý
obnos	obnos	k1gInSc1	obnos
zlata	zlato	k1gNnSc2	zlato
-	-	kIx~	-
kolem	kolem	k7c2	kolem
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nepřátelskému	přátelský	k2eNgMnSc3d1	nepřátelský
hrdinovi	hrdina	k1gMnSc3	hrdina
uštědří	uštědřit	k5eAaPmIp3nS	uštědřit
poslední	poslední	k2eAgFnSc4d1	poslední
ránu	rána	k1gFnSc4	rána
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
touto	tento	k3xDgFnSc7	tento
sumou	suma	k1gFnSc7	suma
finančně	finančně	k6eAd1	finančně
odměněn	odměněn	k2eAgMnSc1d1	odměněn
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
povede	povést	k5eAaPmIp3nS	povést
zabít	zabít	k5eAaPmF	zabít
více	hodně	k6eAd2	hodně
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přehrají	přehrát	k5eAaPmIp3nP	přehrát
zvukové	zvukový	k2eAgFnPc4d1	zvuková
hlášky	hláška	k1gFnPc4	hláška
oznamující	oznamující	k2eAgFnSc1d1	oznamující
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
některý	některý	k3yIgMnSc1	některý
hrdina	hrdina	k1gMnSc1	hrdina
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabije	zabít	k5eAaPmIp3nS	zabít
mnoho	mnoho	k4c1	mnoho
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
sám	sám	k3xTgMnSc1	sám
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
také	také	k9	také
hlasově	hlasově	k6eAd1	hlasově
oznámena	oznámit	k5eAaPmNgFnS	oznámit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hlášky	hláška	k1gFnPc1	hláška
byly	být	k5eAaImAgFnP	být
vývojáři	vývojář	k1gMnPc7	vývojář
modu	modus	k1gInSc2	modus
vypůjčeny	vypůjčit	k5eAaPmNgInP	vypůjčit
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Unreal	Unreal	k1gMnSc1	Unreal
Tournament	Tournament	k1gMnSc1	Tournament
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
takováto	takovýto	k3xDgNnPc4	takovýto
několikanásobná	několikanásobný	k2eAgNnPc4d1	několikanásobné
zabití	zabití	k1gNnPc4	zabití
je	být	k5eAaImIp3nS	být
hráči	hráč	k1gMnSc6	hráč
navrch	navrch	k6eAd1	navrch
přidán	přidán	k2eAgMnSc1d1	přidán
ještě	ještě	k9	ještě
dodatečně	dodatečně	k6eAd1	dodatečně
menší	malý	k2eAgInSc4d2	menší
finanční	finanční	k2eAgInSc4d1	finanční
obnos	obnos	k1gInSc4	obnos
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
finančně	finančně	k6eAd1	finančně
ohodnocen	ohodnocen	k2eAgInSc1d1	ohodnocen
je	být	k5eAaImIp3nS	být
i	i	k9	i
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dominanci	dominance	k1gFnSc4	dominance
jiného	jiné	k1gNnSc2	jiné
ukončí	ukončit	k5eAaPmIp3nP	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hráč	hráč	k1gMnSc1	hráč
zabit	zabít	k5eAaPmNgInS	zabít
neutrálními	neutrální	k2eAgFnPc7d1	neutrální
jednotkami	jednotka	k1gFnPc7	jednotka
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
nedostane	dostat	k5eNaPmIp3nS	dostat
nikdo	nikdo	k3yNnSc1	nikdo
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zabit	zabít	k5eAaPmNgMnS	zabít
budovou	budova	k1gFnSc7	budova
nebo	nebo	k8xC	nebo
creepy	creep	k1gInPc7	creep
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odměna	odměna	k1gFnSc1	odměna
za	za	k7c2	za
zabití	zabití	k1gNnSc2	zabití
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
mezi	mezi	k7c4	mezi
hráče	hráč	k1gMnPc4	hráč
celého	celý	k2eAgInSc2d1	celý
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinové	Hrdinové	k2eAgMnSc1d1	Hrdinové
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
primárních	primární	k2eAgInPc2d1	primární
atributů	atribut	k1gInPc2	atribut
<g/>
:	:	kIx,	:
Agility	Agilita	k1gFnPc1	Agilita
–	–	k?	–
obratnost	obratnost	k1gFnSc1	obratnost
(	(	kIx(	(
<g/>
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
obranu	obrana	k1gFnSc4	obrana
zmírňující	zmírňující	k2eAgFnSc2d1	zmírňující
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
škody	škoda	k1gFnSc2	škoda
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
útok	útok	k1gInSc1	útok
<g/>
)	)	kIx)	)
Strength	Strength	k1gInSc1	Strength
–	–	k?	–
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hrdinovi	hrdina	k1gMnSc3	hrdina
výdrž	výdrž	k1gFnSc4	výdrž
<g/>
)	)	kIx)	)
Inteligence	inteligence	k1gFnSc1	inteligence
–	–	k?	–
inteligence	inteligence	k1gFnSc2	inteligence
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hrdinovi	hrdina	k1gMnSc3	hrdina
hladinu	hladina	k1gFnSc4	hladina
many	mana	k1gFnSc2	mana
<g/>
)	)	kIx)	)
Další	další	k2eAgInSc1d1	další
způsob	způsob	k1gInSc1	způsob
základního	základní	k2eAgNnSc2d1	základní
rozdělení	rozdělení	k1gNnSc2	rozdělení
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Ranged	Ranged	k1gMnSc1	Ranged
–	–	k?	–
útočník	útočník	k1gMnSc1	útočník
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
(	(	kIx(	(
<g/>
střelec	střelec	k1gMnSc1	střelec
<g/>
,	,	kIx,	,
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
)	)	kIx)	)
Melee	Melee	k1gFnSc1	Melee
–	–	k?	–
útočník	útočník	k1gMnSc1	útočník
na	na	k7c4	na
blízko	blízko	k1gNnSc4	blízko
Hrdinové	Hrdinová	k1gFnSc2	Hrdinová
se	se	k3xPyFc4	se
také	také	k9	také
liší	lišit	k5eAaImIp3nS	lišit
stylem	styl	k1gInSc7	styl
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
a	a	k8xC	a
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
různé	různý	k2eAgFnPc4d1	různá
specifické	specifický	k2eAgFnPc4d1	specifická
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
přísluší	příslušet	k5eAaImIp3nP	příslušet
<g/>
:	:	kIx,	:
Supporter	Supporter	k1gInSc1	Supporter
–	–	k?	–
podpůrný	podpůrný	k2eAgMnSc1d1	podpůrný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
nějakou	nějaký	k3yIgFnSc4	nějaký
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mohou	moct	k5eAaImIp3nP	moct
dobře	dobře	k6eAd1	dobře
pomoci	pomoct	k5eAaPmF	pomoct
ostatním	ostatní	k2eAgMnPc3d1	ostatní
hrdinům	hrdina	k1gMnPc3	hrdina
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
léčivá	léčivý	k2eAgNnPc4d1	léčivé
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
zpomalení	zpomalení	k1gNnSc2	zpomalení
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhý	druhý	k4xOgMnSc1	druhý
hrdina	hrdina	k1gMnSc1	hrdina
pak	pak	k6eAd1	pak
zabije	zabít	k5eAaPmIp3nS	zabít
nepřátelského	přátelský	k2eNgMnSc4d1	nepřátelský
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Babysitter	Babysitter	k1gInSc1	Babysitter
–	–	k?	–
doslova	doslova	k6eAd1	doslova
chůva	chůva	k1gFnSc1	chůva
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
přátelskému	přátelský	k2eAgMnSc3d1	přátelský
hrdinovi	hrdina	k1gMnSc3	hrdina
např.	např.	kA	např.
doplňováním	doplňování	k1gNnSc7	doplňování
many	mana	k1gFnSc2	mana
<g/>
,	,	kIx,	,
léčením	léčení	k1gNnSc7	léčení
<g/>
,	,	kIx,	,
poskytováním	poskytování	k1gNnSc7	poskytování
aur	aura	k1gFnPc2	aura
při	při	k7c6	při
výpadech	výpad	k1gInPc6	výpad
vůči	vůči	k7c3	vůči
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každý	každý	k3xTgMnSc1	každý
podpůrný	podpůrný	k2eAgMnSc1d1	podpůrný
hrdina	hrdina	k1gMnSc1	hrdina
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
dobrá	dobrý	k2eAgFnSc1d1	dobrá
chůva	chůva	k1gFnSc1	chůva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc1	všechen
chůvy	chůva	k1gFnPc1	chůva
patří	patřit	k5eAaImIp3nP	patřit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
mezi	mezi	k7c4	mezi
podpůrné	podpůrný	k2eAgMnPc4d1	podpůrný
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Disabler	Disabler	k1gMnSc1	Disabler
–	–	k?	–
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc1	schopnost
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
odstavit	odstavit	k5eAaPmF	odstavit
nepřátelského	přátelský	k2eNgMnSc4d1	nepřátelský
hrdinu	hrdina	k1gMnSc4	hrdina
z	z	k7c2	z
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
nějakou	nějaký	k3yIgFnSc7	nějaký
formou	forma	k1gFnSc7	forma
omráčení	omráčení	k1gNnSc2	omráčení
<g/>
,	,	kIx,	,
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
,	,	kIx,	,
umlčení	umlčení	k1gNnSc1	umlčení
(	(	kIx(	(
<g/>
znemožnění	znemožnění	k1gNnSc4	znemožnění
kouzlit	kouzlit	k5eAaImF	kouzlit
po	po	k7c4	po
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spoutání	spoutání	k1gNnPc1	spoutání
<g/>
,	,	kIx,	,
přeměny	přeměna	k1gFnPc1	přeměna
na	na	k7c4	na
éterickou	éterický	k2eAgFnSc4d1	éterická
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Ganker	Ganker	k1gMnSc1	Ganker
–	–	k?	–
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
předurčen	předurčit	k5eAaPmNgInS	předurčit
k	k	k7c3	k
překvapení	překvapení	k1gNnSc3	překvapení
a	a	k8xC	a
zaskočení	zaskočení	k1gNnSc3	zaskočení
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
určitým	určitý	k2eAgInSc7d1	určitý
druhem	druh	k1gInSc7	druh
omráčení	omráčení	k1gNnSc2	omráčení
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
cíleným	cílený	k2eAgNnSc7d1	cílené
<g/>
,	,	kIx,	,
plošným	plošný	k2eAgNnSc7d1	plošné
či	či	k8xC	či
nějakým	nějaký	k3yIgMnSc7	nějaký
jiným	jiný	k2eAgMnSc7d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Roamer	Roamer	k1gMnSc1	Roamer
–	–	k?	–
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
při	při	k7c6	při
strategii	strategie	k1gFnSc6	strategie
přebíhání	přebíhání	k1gNnSc2	přebíhání
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
trasy	trasa	k1gFnSc2	trasa
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
překvapit	překvapit	k5eAaPmF	překvapit
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
pomoci	pomoct	k5eAaPmF	pomoct
hrdinům	hrdina	k1gMnPc3	hrdina
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
či	či	k8xC	či
při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
hrdiny	hrdina	k1gMnSc2	hrdina
utíkajícího	utíkající	k2eAgInSc2d1	utíkající
z	z	k7c2	z
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
prohrál	prohrát	k5eAaPmAgMnS	prohrát
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
bojoval	bojovat	k5eAaImAgMnS	bojovat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
role	role	k1gFnSc1	role
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
rolí	role	k1gFnSc7	role
gankera	gankero	k1gNnSc2	gankero
<g/>
.	.	kIx.	.
</s>
<s>
Harrasers	Harrasers	k6eAd1	Harrasers
–	–	k?	–
typ	typ	k1gInSc4	typ
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
schopen	schopit	k5eAaPmNgMnS	schopit
spamovat	spamovat	k5eAaPmF	spamovat
nepřátelské	přátelský	k2eNgMnPc4d1	nepřátelský
hrdiny	hrdina	k1gMnPc4	hrdina
opakovaně	opakovaně	k6eAd1	opakovaně
svými	svůj	k3xOyFgNnPc7	svůj
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
,	,	kIx,	,
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
rovnou	rovnou	k6eAd1	rovnou
oslabené	oslabený	k2eAgNnSc1d1	oslabené
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
DPS	DPS	kA	DPS
–	–	k?	–
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
fyzický	fyzický	k2eAgInSc4d1	fyzický
útok	útok	k1gInSc4	útok
a	a	k8xC	a
udílení	udílení	k1gNnSc4	udílení
co	co	k9	co
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
škod	škoda	k1gFnPc2	škoda
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
díky	díky	k7c3	díky
schopnostem	schopnost	k1gFnPc3	schopnost
zvyšujícím	zvyšující	k2eAgMnSc7d1	zvyšující
sílu	síla	k1gFnSc4	síla
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rychlým	rychlý	k2eAgInSc7d1	rychlý
útokem	útok	k1gInSc7	útok
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgFnPc2d1	speciální
dovedností	dovednost	k1gFnPc2	dovednost
krátce	krátce	k6eAd1	krátce
zvyšující	zvyšující	k2eAgFnSc4d1	zvyšující
prudce	prudko	k6eAd1	prudko
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hrdinů	hrdina	k1gMnPc2	hrdina
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
rovněž	rovněž	k9	rovněž
zařadit	zařadit	k5eAaPmF	zařadit
mezi	mezi	k7c4	mezi
DPS	DPS	kA	DPS
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
díky	díky	k7c3	díky
předmětům	předmět	k1gInPc3	předmět
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
ale	ale	k8xC	ale
harraseři	harraser	k1gMnPc1	harraser
a	a	k8xC	a
gankeři	ganker	k1gMnPc1	ganker
<g/>
.	.	kIx.	.
</s>
<s>
Tank	tank	k1gInSc1	tank
–	–	k?	–
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
schopnost	schopnost	k1gFnSc4	schopnost
absorpce	absorpce	k1gFnSc2	absorpce
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hladinu	hladina	k1gFnSc4	hladina
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
eventuálně	eventuálně	k6eAd1	eventuálně
i	i	k9	i
schopnosti	schopnost	k1gFnSc2	schopnost
redukce	redukce	k1gFnSc2	redukce
magických	magický	k2eAgFnPc2d1	magická
škod	škoda	k1gFnPc2	škoda
nebo	nebo	k8xC	nebo
hodně	hodně	k6eAd1	hodně
podceňovaného	podceňovaný	k2eAgInSc2d1	podceňovaný
úhybu	úhyb	k1gInSc2	úhyb
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
šanci	šance	k1gFnSc4	šance
vyhnutí	vyhnutí	k1gNnSc2	vyhnutí
se	se	k3xPyFc4	se
soupeřově	soupeřův	k2eAgInSc6d1	soupeřův
útoku	útok	k1gInSc6	útok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určený	určený	k2eAgInSc1d1	určený
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
vlastnostem	vlastnost	k1gFnPc3	vlastnost
mohl	moct	k5eAaImAgMnS	moct
bez	bez	k7c2	bez
velkého	velký	k2eAgNnSc2d1	velké
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
rychlé	rychlý	k2eAgFnSc2d1	rychlá
smrti	smrt	k1gFnSc2	smrt
začínat	začínat	k5eAaImF	začínat
souboje	souboj	k1gInPc4	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Carry	Carra	k1gFnPc1	Carra
–	–	k?	–
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
největší	veliký	k2eAgFnSc1d3	veliký
síla	síla	k1gFnSc1	síla
přichází	přicházet	k5eAaImIp3nS	přicházet
až	až	k9	až
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
mají	mít	k5eAaImIp3nP	mít
drahé	drahý	k2eAgInPc4d1	drahý
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
mají	mít	k5eAaImIp3nP	mít
tito	tento	k3xDgMnPc1	tento
hrdinové	hrdina	k1gMnPc1	hrdina
nějakou	nějaký	k3yIgFnSc4	nějaký
speciální	speciální	k2eAgFnSc4d1	speciální
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
až	až	k9	až
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
fázích	fáze	k1gFnPc6	fáze
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dělá	dělat	k5eAaImIp3nS	dělat
dobré	dobrý	k2eAgMnPc4d1	dobrý
hrdiny	hrdina	k1gMnPc4	hrdina
namísto	namísto	k7c2	namísto
průměrných	průměrný	k2eAgInPc2d1	průměrný
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
pomoc	pomoc	k1gFnSc4	pomoc
od	od	k7c2	od
podpůrných	podpůrný	k2eAgMnPc2d1	podpůrný
hrdinů	hrdina	k1gMnPc2	hrdina
nebo	nebo	k8xC	nebo
babysitterů	babysitter	k1gMnPc2	babysitter
k	k	k7c3	k
přežívání	přežívání	k1gNnSc3	přežívání
začátků	začátek	k1gInPc2	začátek
a	a	k8xC	a
dávání	dávání	k1gNnSc4	dávání
prostoru	prostor	k1gInSc2	prostor
k	k	k7c3	k
trénování	trénování	k1gNnSc3	trénování
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Pusher	Pushra	k1gFnPc2	Pushra
–	–	k?	–
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgMnS	vybavit
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gNnSc3	on
pomohou	pomoct	k5eAaPmIp3nP	pomoct
rychle	rychle	k6eAd1	rychle
ničit	ničit	k5eAaImF	ničit
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgFnPc4	takový
patří	patřit	k5eAaImIp3nP	patřit
většinou	většina	k1gFnSc7	většina
vyvolávači	vyvolávač	k1gMnPc1	vyvolávač
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
přivolají	přivolat	k5eAaPmIp3nP	přivolat
pomocné	pomocný	k2eAgFnPc4d1	pomocná
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
hrdinové	hrdina	k1gMnPc1	hrdina
s	s	k7c7	s
kouzly	kouzlo	k1gNnPc7	kouzlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc4	jenž
přímo	přímo	k6eAd1	přímo
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Iniciátor	iniciátor	k1gMnSc1	iniciátor
–	–	k?	–
hrdina	hrdina	k1gMnSc1	hrdina
který	který	k3yRgMnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
boj	boj	k1gInSc4	boj
nebo	nebo	k8xC	nebo
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
týmový	týmový	k2eAgInSc4d1	týmový
útok	útok	k1gInSc4	útok
či	či	k8xC	či
překvapivý	překvapivý	k2eAgInSc4d1	překvapivý
výpad	výpad	k1gInSc4	výpad
zpoza	zpoza	k7c2	zpoza
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
nepřátele	nepřítel	k1gMnPc4	nepřítel
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
nejprve	nejprve	k6eAd1	nejprve
překvapí	překvapit	k5eAaPmIp3nS	překvapit
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
tým	tým	k1gInSc1	tým
plošným	plošný	k2eAgNnSc7d1	plošné
dalekonosným	dalekonosný	k2eAgNnSc7d1	dalekonosné
kouzlem	kouzlo	k1gNnSc7	kouzlo
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
vyřadí	vyřadit	k5eAaPmIp3nS	vyřadit
z	z	k7c2	z
boje	boj	k1gInSc2	boj
ter	ter	k?	ter
<g/>
)	)	kIx)	)
<g/>
ry	ry	k?	ry
<g/>
,	,	kIx,	,
semi-disabler	semiisabler	k1gMnSc1	semi-disabler
nebo	nebo	k8xC	nebo
ganker	ganker	k1gMnSc1	ganker
<g/>
/	/	kIx~	/
<g/>
roamer	roamer	k1gMnSc1	roamer
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
týmu	tým	k1gInSc2	tým
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c4	v
chatu	chata	k1gFnSc4	chata
domlouvat	domlouvat	k5eAaImF	domlouvat
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
strategii	strategie	k1gFnSc6	strategie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vyhrát	vyhrát	k5eAaPmF	vyhrát
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
a	a	k8xC	a
předem	předem	k6eAd1	předem
si	se	k3xPyFc3	se
rozdělit	rozdělit	k5eAaPmF	rozdělit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
role	role	k1gFnPc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tyto	tento	k3xDgFnPc1	tento
strategie	strategie	k1gFnPc1	strategie
<g/>
:	:	kIx,	:
Late	lat	k1gInSc5	lat
Game	game	k1gInSc4	game
Domination	Domination	k1gInSc1	Domination
<g/>
/	/	kIx~	/
<g/>
Carry	Carr	k1gInPc1	Carr
strategie	strategie	k1gFnSc2	strategie
<g/>
:	:	kIx,	:
tým	tým	k1gInSc1	tým
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
carry	carr	k1gMnPc4	carr
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
postupně	postupně	k6eAd1	postupně
nakoupí	nakoupit	k5eAaPmIp3nP	nakoupit
drahé	drahý	k2eAgInPc1d1	drahý
předměty	předmět	k1gInPc1	předmět
a	a	k8xC	a
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
hry	hra	k1gFnSc2	hra
sám	sám	k3xTgMnSc1	sám
zničí	zničit	k5eAaPmIp3nP	zničit
celý	celý	k2eAgInSc4d1	celý
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
týmu	tým	k1gInSc2	tým
bude	být	k5eAaImBp3nS	být
podporovat	podporovat	k5eAaImF	podporovat
tohoto	tento	k3xDgMnSc4	tento
hrdinu	hrdina	k1gMnSc4	hrdina
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
protáhnout	protáhnout	k5eAaPmF	protáhnout
hru	hra	k1gFnSc4	hra
dostatečně	dostatečně	k6eAd1	dostatečně
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
právě	právě	k9	právě
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
carry	carro	k1gNnPc7	carro
hrdina	hrdina	k1gMnSc1	hrdina
stačil	stačit	k5eAaBmAgMnS	stačit
na	na	k7c4	na
potřebné	potřebný	k2eAgInPc4d1	potřebný
předměty	předmět	k1gInPc4	předmět
našetřit	našetřit	k5eAaPmF	našetřit
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
strategie	strategie	k1gFnSc1	strategie
je	být	k5eAaImIp3nS	být
určitě	určitě	k6eAd1	určitě
nejklasičtější	klasický	k2eAgFnSc1d3	nejklasičtější
v	v	k7c6	v
Dotě	Dota	k1gFnSc6	Dota
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgInSc7d3	nejznámější
zápasem	zápas	k1gInSc7	zápas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
úspěšně	úspěšně	k6eAd1	úspěšně
demonstrovaná	demonstrovaný	k2eAgFnSc1d1	demonstrovaná
Late	lat	k1gInSc5	lat
Game	game	k1gInSc1	game
Domination	Domination	k1gInSc4	Domination
strategie	strategie	k1gFnSc2	strategie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
týmů	tým	k1gInPc2	tým
LGD	LGD	kA	LGD
vs	vs	k?	vs
EHOME	EHOME	kA	EHOME
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráč	hráč	k1gMnSc1	hráč
ZSMJ	ZSMJ	kA	ZSMJ
dokázal	dokázat	k5eAaPmAgMnS	dokázat
s	s	k7c7	s
Medúsou	Medúsa	k1gFnSc7	Medúsa
našetřit	našetřit	k5eAaPmF	našetřit
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
nejdražší	drahý	k2eAgInPc4d3	nejdražší
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	a
potom	potom	k6eAd1	potom
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
sám	sám	k3xTgMnSc1	sám
zdecimoval	zdecimovat	k5eAaPmAgMnS	zdecimovat
celý	celý	k2eAgInSc4d1	celý
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Early	earl	k1gMnPc4	earl
Game	game	k1gInSc4	game
Domination	Domination	k1gInSc1	Domination
<g/>
/	/	kIx~	/
<g/>
Beat	beat	k1gInSc1	beat
down	down	k1gInSc1	down
strategie	strategie	k1gFnSc1	strategie
<g/>
:	:	kIx,	:
opak	opak	k1gInSc1	opak
Late	lat	k1gInSc5	lat
Game	game	k1gInSc1	game
Domination	Domination	k1gInSc1	Domination
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
strategií	strategie	k1gFnSc7	strategie
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
masivní	masivní	k2eAgNnPc4d1	masivní
zabíjení	zabíjení	k1gNnPc4	zabíjení
nepřátel	nepřítel	k1gMnPc2	nepřítel
už	už	k9	už
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
výběr	výběr	k1gInSc1	výběr
hrdinů	hrdina	k1gMnPc2	hrdina
typu	typ	k1gInSc2	typ
harrasserů	harrasser	k1gMnPc2	harrasser
a	a	k8xC	a
iniciátorů	iniciátor	k1gMnPc2	iniciátor
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
způsobí	způsobit	k5eAaPmIp3nS	způsobit
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
hrdinové	hrdina	k1gMnPc1	hrdina
nebudou	být	k5eNaImBp3nP	být
mít	mít	k5eAaImF	mít
ani	ani	k8xC	ani
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
časem	časem	k6eAd1	časem
ani	ani	k8xC	ani
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
už	už	k6eAd1	už
nebudou	být	k5eNaImBp3nP	být
schopní	schopný	k2eAgMnPc1d1	schopný
klást	klást	k5eAaImF	klást
větší	veliký	k2eAgInSc4d2	veliký
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
už	už	k9	už
nebude	být	k5eNaImBp3nS	být
pro	pro	k7c4	pro
tým	tým	k1gInSc4	tým
problém	problém	k1gInSc4	problém
zničit	zničit	k5eAaPmF	zničit
všechny	všechen	k3xTgFnPc4	všechen
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
té	ten	k3xDgFnSc2	ten
hlavní	hlavní	k2eAgFnSc2d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Team	team	k1gInSc1	team
Fight	Fight	k1gInSc1	Fight
strategie	strategie	k1gFnSc1	strategie
<g/>
:	:	kIx,	:
účelem	účel	k1gInSc7	účel
této	tento	k3xDgFnSc2	tento
strategie	strategie	k1gFnSc2	strategie
je	být	k5eAaImIp3nS	být
vyhrávat	vyhrávat	k5eAaImF	vyhrávat
velké	velký	k2eAgInPc4d1	velký
týmové	týmový	k2eAgInPc4d1	týmový
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
střetnou	střetnout	k5eAaPmIp3nP	střetnout
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
hrdinové	hrdina	k1gMnPc1	hrdina
s	s	k7c7	s
aspoň	aspoň	k9	aspoň
třemi	tři	k4xCgInPc7	tři
členy	člen	k1gMnPc7	člen
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhraném	vyhraný	k2eAgInSc6d1	vyhraný
souboji	souboj	k1gInSc6	souboj
má	mít	k5eAaImIp3nS	mít
vítězný	vítězný	k2eAgInSc1d1	vítězný
tým	tým	k1gInSc1	tým
zpravidla	zpravidla	k6eAd1	zpravidla
čas	čas	k1gInSc4	čas
na	na	k7c4	na
ničení	ničení	k1gNnSc4	ničení
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
nepřátelští	přátelský	k2eNgMnPc1d1	nepřátelský
hrdinové	hrdina	k1gMnPc1	hrdina
vrátí	vrátit	k5eAaPmIp3nP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
zničené	zničený	k2eAgFnPc1d1	zničená
všechny	všechen	k3xTgFnPc1	všechen
důležité	důležitý	k2eAgFnPc1d1	důležitá
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
rovnou	rovnou	k6eAd1	rovnou
hlavní	hlavní	k2eAgFnPc4d1	hlavní
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
strategie	strategie	k1gFnSc1	strategie
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
mít	mít	k5eAaImF	mít
vhodné	vhodný	k2eAgMnPc4d1	vhodný
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Push	Push	k1gInSc1	Push
<g/>
:	:	kIx,	:
jak	jak	k8xC	jak
napovídá	napovídat	k5eAaBmIp3nS	napovídat
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
této	tento	k3xDgFnSc2	tento
strategie	strategie	k1gFnSc2	strategie
není	být	k5eNaImIp3nS	být
zabíjet	zabíjet	k5eAaImF	zabíjet
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychle	rychle	k6eAd1	rychle
zničit	zničit	k5eAaPmF	zničit
všechny	všechen	k3xTgFnPc4	všechen
klíčové	klíčový	k2eAgFnPc4d1	klíčová
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
obranné	obranný	k2eAgFnSc2d1	obranná
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
než	než	k8xS	než
začne	začít	k5eAaPmIp3nS	začít
být	být	k5eAaImF	být
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
tým	tým	k1gInSc1	tým
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zničení	zničení	k1gNnSc4	zničení
budov	budova	k1gFnPc2	budova
totiž	totiž	k9	totiž
dostává	dostávat	k5eAaImIp3nS	dostávat
tým	tým	k1gInSc4	tým
také	také	k9	také
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
logicky	logicky	k6eAd1	logicky
vyústí	vyústit	k5eAaPmIp3nS	vyústit
v	v	k7c4	v
nákup	nákup	k1gInSc4	nákup
lepších	dobrý	k2eAgInPc2d2	lepší
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
získání	získání	k1gNnPc4	získání
značné	značný	k2eAgFnPc4d1	značná
výhody	výhoda	k1gFnPc4	výhoda
oproti	oproti	k7c3	oproti
nepřátelskému	přátelský	k2eNgInSc3d1	nepřátelský
týmu	tým	k1gInSc3	tým
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
zbořit	zbořit	k5eAaPmF	zbořit
i	i	k9	i
klíčové	klíčový	k2eAgFnPc4d1	klíčová
budovy	budova	k1gFnPc4	budova
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
raxy	raxa	k1gFnSc2	raxa
<g/>
)	)	kIx)	)
v	v	k7c6	v
nepřátelské	přátelský	k2eNgFnSc6d1	nepřátelská
základně	základna	k1gFnSc6	základna
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
přátelští	přátelský	k2eAgMnPc1d1	přátelský
creepové	creep	k1gMnPc1	creep
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgMnPc1d2	silnější
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
problém	problém	k1gInSc4	problém
pro	pro	k7c4	pro
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
musí	muset	k5eAaImIp3nP	muset
potýkat	potýkat	k5eAaImF	potýkat
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgFnSc7d1	agresivní
pushovací	pushovací	k2eAgFnSc7d1	pushovací
strategií	strategie	k1gFnSc7	strategie
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
profesionální	profesionální	k2eAgInSc1d1	profesionální
tým	tým	k1gInSc1	tým
Na	na	k7c6	na
<g/>
'	'	kIx"	'
<g/>
Vi	Vi	k?	Vi
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dotě	Dot	k1gInSc6	Dot
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
hráči	hráč	k1gMnPc1	hráč
často	často	k6eAd1	často
využívají	využívat	k5eAaImIp3nP	využívat
spousty	spousta	k1gFnPc4	spousta
slangových	slangový	k2eAgInPc2d1	slangový
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Last	Last	k2eAgInSc1d1	Last
Hit	hit	k1gInSc1	hit
–	–	k?	–
poslední	poslední	k2eAgInSc1d1	poslední
úder	úder	k1gInSc1	úder
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zabité	zabitý	k2eAgMnPc4d1	zabitý
creepy	creep	k1gMnPc4	creep
dostává	dostávat	k5eAaImIp3nS	dostávat
zlato	zlato	k1gNnSc4	zlato
pouze	pouze	k6eAd1	pouze
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
dal	dát	k5eAaPmAgMnS	dát
creepovi	creep	k1gMnSc3	creep
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
ránu	rána	k1gFnSc4	rána
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vhodné	vhodný	k2eAgNnSc1d1	vhodné
creepům	creep	k1gInPc3	creep
dávat	dávat	k5eAaImF	dávat
pouze	pouze	k6eAd1	pouze
poslední	poslední	k2eAgFnPc4d1	poslední
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c4	za
ně	on	k3xPp3gNnSc4	on
hrdina	hrdina	k1gMnSc1	hrdina
dostával	dostávat	k5eAaImAgMnS	dostávat
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Popř.	popř.	kA	popř.
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
dát	dát	k5eAaPmF	dát
poslední	poslední	k2eAgFnSc4d1	poslední
ránu	rána	k1gFnSc4	rána
nepřátelské	přátelský	k2eNgFnSc3d1	nepřátelská
věži	věž	k1gFnSc3	věž
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
dostane	dostat	k5eAaPmIp3nS	dostat
hráč	hráč	k1gMnSc1	hráč
velký	velký	k2eAgInSc4d1	velký
obnos	obnos	k1gInSc4	obnos
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
týmu	tým	k1gInSc2	tým
za	za	k7c4	za
zničenou	zničený	k2eAgFnSc4d1	zničená
věž	věž	k1gFnSc4	věž
dostanou	dostat	k5eAaPmIp3nP	dostat
menší	malý	k2eAgFnSc4d2	menší
částku	částka	k1gFnSc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
nezdaří	zdařit	k5eNaPmIp3nP	zdařit
last	last	k2eAgInSc4d1	last
hit	hit	k1gInSc4	hit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
věž	věž	k1gFnSc1	věž
zničí	zničit	k5eAaPmIp3nS	zničit
creep	creep	k1gInSc1	creep
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
členy	člen	k1gMnPc4	člen
týmu	tým	k1gInSc2	tým
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Deny	Dena	k1gFnPc1	Dena
–	–	k?	–
odepření	odepření	k1gNnSc1	odepření
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zabijete	zabít	k5eAaPmIp2nP	zabít
svého	svůj	k3xOyFgMnSc4	svůj
creepa	creep	k1gMnSc4	creep
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
minimum	minimum	k1gNnSc4	minimum
HP	HP	kA	HP
<g/>
,	,	kIx,	,
nepřítel	nepřítel	k1gMnSc1	nepřítel
dostane	dostat	k5eAaPmIp3nS	dostat
méně	málo	k6eAd2	málo
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
máte	mít	k5eAaImIp2nP	mít
šanci	šance	k1gFnSc4	šance
zabránit	zabránit	k5eAaPmF	zabránit
jeho	jeho	k3xOp3gInSc3	jeho
poslednímu	poslední	k2eAgInSc3d1	poslední
úderu	úder	k1gInSc3	úder
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduše	jednoduše	k6eAd1	jednoduše
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
provádí	provádět	k5eAaImIp3nS	provádět
s	s	k7c7	s
hrdiny	hrdina	k1gMnPc7	hrdina
střelci	střelec	k1gMnPc7	střelec
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
creepy	creep	k1gInPc4	creep
lze	lze	k6eAd1	lze
zranit	zranit	k5eAaPmF	zranit
<g/>
,	,	kIx,	,
až	až	k8xS	až
když	když	k8xS	když
mají	mít	k5eAaImIp3nP	mít
pod	pod	k7c7	pod
50	[number]	k4	50
%	%	kIx~	%
zdraví	zdraví	k1gNnSc6	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
své	svůj	k3xOyFgFnSc2	svůj
jednotky	jednotka	k1gFnSc2	jednotka
žádnou	žádný	k3yNgFnSc4	žádný
odměnu	odměna	k1gFnSc4	odměna
přímo	přímo	k6eAd1	přímo
nedostanete	dostat	k5eNaPmIp2nP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zničíte	zničit	k5eAaPmIp2nP	zničit
silně	silně	k6eAd1	silně
poškozenou	poškozený	k2eAgFnSc4d1	poškozená
vlastní	vlastní	k2eAgFnSc4d1	vlastní
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
tým	tým	k1gInSc4	tým
dostane	dostat	k5eAaPmIp3nS	dostat
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
Před	před	k7c7	před
verzí	verze	k1gFnSc7	verze
6.55	[number]	k4	6.55
nedostával	dostávat	k5eNaImAgMnS	dostávat
žádné	žádný	k3yNgNnSc4	žádný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ničit	ničit	k5eAaImF	ničit
svoji	svůj	k3xOyFgFnSc4	svůj
věž	věž	k1gFnSc4	věž
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
věž	věž	k1gFnSc1	věž
pod	pod	k7c7	pod
10	[number]	k4	10
%	%	kIx~	%
výdrže	výdrž	k1gFnSc2	výdrž
<g/>
.	.	kIx.	.
</s>
<s>
Gankování	Gankování	k1gNnSc1	Gankování
–	–	k?	–
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc4	styl
hry	hra	k1gFnSc2	hra
nebo	nebo	k8xC	nebo
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
hrdinové	hrdina	k1gMnPc1	hrdina
jednoho	jeden	k4xCgInSc2	jeden
teamu	team	k1gInSc2	team
v	v	k7c4	v
určitý	určitý	k2eAgInSc4d1	určitý
okamžik	okamžik	k1gInSc4	okamžik
překvapit	překvapit	k5eAaPmF	překvapit
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
druhý	druhý	k4xOgInSc1	druhý
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejlépe	dobře	k6eAd3	dobře
v	v	k7c6	v
přesile	přesila	k1gFnSc6	přesila
<g/>
.	.	kIx.	.
</s>
<s>
Gangovat	Gangovat	k5eAaBmF	Gangovat
se	se	k3xPyFc4	se
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
především	především	k6eAd1	především
carry	carr	k1gMnPc4	carr
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
málo	málo	k4c4	málo
možností	možnost	k1gFnPc2	možnost
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Gangování	Gangování	k1gNnSc1	Gangování
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
mapě	mapa	k1gFnSc6	mapa
a	a	k8xC	a
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
se	se	k3xPyFc4	se
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
mít	mít	k5eAaImF	mít
předměty	předmět	k1gInPc4	předmět
jako	jako	k8xC	jako
Bottle	Bottle	k1gFnPc4	Bottle
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
vzali	vzít	k5eAaPmAgMnP	vzít
runu	run	k1gInSc2	run
<g/>
,	,	kIx,	,
a	a	k8xC	a
Scroll	Scroll	kA	Scroll
of	of	k?	of
Town	Town	k1gMnSc1	Town
Portal	Portal	k1gMnSc1	Portal
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
TP	TP	kA	TP
–	–	k?	–
městský	městský	k2eAgInSc4d1	městský
portál	portál	k1gInSc4	portál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
přemístit	přemístit	k5eAaPmF	přemístit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
věži	věž	k1gFnSc3	věž
a	a	k8xC	a
umocnit	umocnit	k5eAaPmF	umocnit
moment	moment	k1gInSc4	moment
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nepřítel	nepřítel	k1gMnSc1	nepřítel
zrovna	zrovna	k6eAd1	zrovna
blízko	blízko	k6eAd1	blízko
vaší	váš	k3xOp2gFnSc3	váš
věži	věž	k1gFnSc3	věž
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgMnSc1d1	ideální
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
svitkem	svitek	k1gInSc7	svitek
teleportovat	teleportovat	k5eAaImF	teleportovat
a	a	k8xC	a
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
nepřátelského	přátelský	k2eNgMnSc4d1	nepřátelský
hrdinu	hrdina	k1gMnSc4	hrdina
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
tým	tým	k1gInSc1	tým
totiž	totiž	k9	totiž
většinou	většina	k1gFnSc7	většina
za	za	k7c4	za
tak	tak	k6eAd1	tak
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
nestihne	stihnout	k5eNaPmIp3nS	stihnout
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
vaše	váš	k3xOp2gNnSc4	váš
zmizení	zmizení	k1gNnSc4	zmizení
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Stealování	Stealování	k1gNnSc1	Stealování
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
projev	projev	k1gInSc4	projev
neslušnosti	neslušnost	k1gFnSc2	neslušnost
u	u	k7c2	u
hráčů	hráč	k1gMnPc2	hráč
stejného	stejný	k2eAgInSc2d1	stejný
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
hráč	hráč	k1gMnSc1	hráč
udělí	udělit	k5eAaPmIp3nS	udělit
poslední	poslední	k2eAgInSc1d1	poslední
úder	úder	k1gInSc1	úder
hrdinovi	hrdina	k1gMnSc3	hrdina
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc2	který
ten	ten	k3xDgInSc1	ten
druhý	druhý	k4xOgMnSc1	druhý
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nic	nic	k3yNnSc1	nic
nedělal	dělat	k5eNaImAgMnS	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
mu	on	k3xPp3gMnSc3	on
vlastně	vlastně	k9	vlastně
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
bonusy	bonus	k1gInPc4	bonus
za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgInS	mít
připsat	připsat	k5eAaPmF	připsat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
posoudit	posoudit	k5eAaPmF	posoudit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
kradení	kradení	k1gNnSc4	kradení
a	a	k8xC	a
co	co	k3yRnSc4	co
už	už	k9	už
je	být	k5eAaImIp3nS	být
týmová	týmový	k2eAgFnSc1d1	týmová
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
zloděj	zloděj	k1gMnSc1	zloděj
(	(	kIx(	(
<g/>
stealer	stealer	k1gMnSc1	stealer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
jevem	jev	k1gInSc7	jev
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
ve	v	k7c6	v
hrách	hra	k1gFnPc6	hra
s	s	k7c7	s
málo	málo	k6eAd1	málo
zkušenými	zkušený	k2eAgMnPc7d1	zkušený
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Backdoor	Backdoor	k1gInSc1	Backdoor
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
neslušnou	slušný	k2eNgFnSc4d1	neslušná
a	a	k8xC	a
často	často	k6eAd1	často
zakazovanou	zakazovaný	k2eAgFnSc4d1	zakazovaná
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrdina	hrdina	k1gMnSc1	hrdina
ničí	ničit	k5eAaImIp3nS	ničit
kteroukoliv	kterýkoliv	k3yIgFnSc4	kterýkoliv
z	z	k7c2	z
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
klíčových	klíčový	k2eAgFnPc2d1	klíčová
budov	budova	k1gFnPc2	budova
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
sám	sám	k3xTgMnSc1	sám
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
spřátelených	spřátelený	k2eAgInPc2d1	spřátelený
creepů	creep	k1gInPc2	creep
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
bojující	bojující	k2eAgMnPc4d1	bojující
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
hrdiny	hrdina	k1gMnPc7	hrdina
a	a	k8xC	a
creepy	creep	k1gMnPc7	creep
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
budovy	budova	k1gFnSc2	budova
obejde	obejít	k5eAaPmIp3nS	obejít
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
mohou	moct	k5eAaImIp3nP	moct
výrazně	výrazně	k6eAd1	výrazně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
výsledek	výsledek	k1gInSc4	výsledek
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nových	nový	k2eAgFnPc6d1	nová
verzích	verze	k1gFnPc6	verze
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
backdooru	backdoor	k1gInSc3	backdoor
udělalo	udělat	k5eAaPmAgNnS	udělat
opatření	opatření	k1gNnSc1	opatření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
budova	budova	k1gFnSc1	budova
sama	sám	k3xTgFnSc1	sám
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
výdrž	výdrž	k1gFnSc1	výdrž
(	(	kIx(	(
<g/>
čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
poškozována	poškozován	k2eAgFnSc1d1	poškozována
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
životů	život	k1gInPc2	život
jí	on	k3xPp3gFnSc3	on
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
věž	věž	k1gFnSc4	věž
zničit	zničit	k5eAaPmF	zničit
sám	sám	k3xTgMnSc1	sám
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
budovy	budova	k1gFnPc4	budova
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
smí	smět	k5eAaImIp3nS	smět
útočit	útočit	k5eAaImF	útočit
jen	jen	k9	jen
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
spřátelených	spřátelený	k2eAgInPc2d1	spřátelený
creepů	creep	k1gInPc2	creep
<g/>
.	.	kIx.	.
</s>
<s>
Aggro	Aggro	k6eAd1	Aggro
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
může	moct	k5eAaImIp3nS	moct
hráč	hráč	k1gMnSc1	hráč
upoutat	upoutat	k5eAaPmF	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
creepů	creep	k1gInPc2	creep
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dotě	Dota	k1gFnSc6	Dota
buď	buď	k8xC	buď
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
danou	daný	k2eAgFnSc4d1	daná
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
jednoduše	jednoduše	k6eAd1	jednoduše
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
ho	on	k3xPp3gNnSc4	on
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
její	její	k3xOp3gFnSc1	její
pozornost	pozornost	k1gFnSc1	pozornost
upoutá	upoutat	k5eAaPmIp3nS	upoutat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
hrdinu	hrdina	k1gMnSc4	hrdina
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
bývá	bývat	k5eAaImIp3nS	bývat
hojně	hojně	k6eAd1	hojně
využíváno	využívat	k5eAaPmNgNnS	využívat
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
harassováním	harassování	k1gNnSc7	harassování
či	či	k8xC	či
pullováním	pullování	k1gNnSc7	pullování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nepřátelští	přátelský	k2eNgMnPc1d1	nepřátelský
creepové	creep	k1gMnPc1	creep
odstranili	odstranit	k5eAaPmAgMnP	odstranit
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Pulling	Pulling	k1gInSc1	Pulling
–	–	k?	–
odlákání	odlákání	k1gNnSc2	odlákání
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vybíjení	vybíjení	k1gNnSc4	vybíjení
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vlastních	vlastní	k2eAgMnPc2d1	vlastní
<g/>
,	,	kIx,	,
či	či	k8xC	či
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
bývá	bývat	k5eAaImIp3nS	bývat
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
ovládání	ovládání	k1gNnSc4	ovládání
aggra	aggro	k1gNnSc2	aggro
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Stackování	Stackování	k1gNnSc1	Stackování
-	-	kIx~	-
Proces	proces	k1gInSc1	proces
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
v	v	k7c4	v
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
odlákají	odlákat	k5eAaPmIp3nP	odlákat
neutrální	neutrální	k2eAgFnPc1d1	neutrální
jednotky	jednotka	k1gFnPc1	jednotka
z	z	k7c2	z
lesa	les	k1gInSc2	les
pryč	pryč	k6eAd1	pryč
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
místa	místo	k1gNnSc2	místo
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
hra	hra	k1gFnSc1	hra
nepozná	poznat	k5eNaPmIp3nS	poznat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nejsou	být	k5eNaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
jednotky	jednotka	k1gFnPc4	jednotka
již	již	k6eAd1	již
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
a	a	k8xC	a
přidá	přidat	k5eAaPmIp3nS	přidat
tam	tam	k6eAd1	tam
další	další	k2eAgFnPc4d1	další
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnSc1d1	původní
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
"	"	kIx"	"
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
"	"	kIx"	"
Doty	Dota	k1gFnPc1	Dota
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Eul	Eul	k1gMnSc1	Eul
<g/>
,	,	kIx,	,
mapu	mapa	k1gFnSc4	mapa
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
dále	daleko	k6eAd2	daleko
nevyvíjel	vyvíjet	k5eNaImAgInS	vyvíjet
a	a	k8xC	a
ani	ani	k8xC	ani
neoptimalizoval	optimalizovat	k5eNaBmAgMnS	optimalizovat
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
Warcraft	Warcraft	k1gInSc4	Warcraft
III	III	kA	III
<g/>
:	:	kIx,	:
Reign	Reign	k1gMnSc1	Reign
of	of	k?	of
Chaos	chaos	k1gInSc1	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgMnSc6	tento
hráči	hráč	k1gMnSc6	hráč
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
předmětů	předmět	k1gInPc2	předmět
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
–	–	k?	–
Eul	Eul	k1gFnPc2	Eul
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Scepter	sceptrum	k1gNnPc2	sceptrum
of	of	k?	of
Divinity	Divinita	k1gFnSc2	Divinita
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc4	vývoj
převzal	převzít	k5eAaPmAgMnS	převzít
Guinsoo	Guinsoo	k1gMnSc1	Guinsoo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mapu	mapa	k1gFnSc4	mapa
vydával	vydávat	k5eAaImAgInS	vydávat
až	až	k6eAd1	až
do	do	k7c2	do
verze	verze	k1gFnSc2	verze
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
on	on	k3xPp3gMnSc1	on
má	mít	k5eAaImIp3nS	mít
památný	památný	k2eAgInSc4d1	památný
předmět	předmět	k1gInSc4	předmět
–	–	k?	–
Guinsoo	Guinsoo	k6eAd1	Guinsoo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Scythe	Scythe	k1gInSc1	Scythe
of	of	k?	of
Vyse	Vyse	k1gInSc1	Vyse
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
6	[number]	k4	6
vydává	vydávat	k5eAaPmIp3nS	vydávat
mapu	mapa	k1gFnSc4	mapa
IceFrog	IceFroga	k1gFnPc2	IceFroga
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
DotA	DotA	k1gFnSc2	DotA
AllStars	AllStarsa	k1gFnPc2	AllStarsa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
mutací	mutace	k1gFnPc2	mutace
mapy	mapa	k1gFnSc2	mapa
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
stala	stát	k5eAaPmAgFnS	stát
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
<g/>
.	.	kIx.	.
</s>
<s>
IceFrog	IceFrog	k1gInSc1	IceFrog
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hlavním	hlavní	k2eAgMnSc7d1	hlavní
vývojářem	vývojář	k1gMnSc7	vývojář
mapy	mapa	k1gFnSc2	mapa
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
předchozích	předchozí	k2eAgMnPc2d1	předchozí
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
obnáší	obnášet	k5eAaImIp3nS	obnášet
vylepšování	vylepšování	k1gNnSc1	vylepšování
mapy	mapa	k1gFnSc2	mapa
–	–	k?	–
přidávání	přidávání	k1gNnSc4	přidávání
nových	nový	k2eAgFnPc2d1	nová
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
balancování	balancování	k1gNnSc1	balancování
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc3	odstraňování
bugů	bug	k1gInPc2	bug
<g/>
.	.	kIx.	.
</s>
<s>
IceFrog	IceFrog	k1gInSc1	IceFrog
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozího	předchozí	k2eAgMnSc2d1	předchozí
vývojáře	vývojář	k1gMnSc2	vývojář
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
komunitou	komunita	k1gFnSc7	komunita
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gInPc2	jeho
kroků	krok	k1gInPc2	krok
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
popud	popud	k1gInSc4	popud
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
vydáním	vydání	k1gNnSc7	vydání
verze	verze	k1gFnSc1	verze
6.68	[number]	k4	6.68
od	od	k7c2	od
přízviska	přízvisko	k1gNnSc2	přízvisko
AllStars	AllStarsa	k1gFnPc2	AllStarsa
upuštěno	upustit	k5eAaPmNgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
verzí	verze	k1gFnPc2	verze
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nepsaným	nepsaný	k2eAgNnSc7d1	nepsaný
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
ta	ten	k3xDgFnSc1	ten
nejnovější	nový	k2eAgFnSc1d3	nejnovější
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
stabilní	stabilní	k2eAgFnSc1d1	stabilní
mapa	mapa	k1gFnSc1	mapa
–	–	k?	–
prověřená	prověřený	k2eAgFnSc1d1	prověřená
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
chyby	chyba	k1gFnPc4	chyba
atd.	atd.	kA	atd.
V	v	k7c6	v
ligách	liga	k1gFnPc6	liga
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
výhradně	výhradně	k6eAd1	výhradně
stabilní	stabilní	k2eAgFnPc1d1	stabilní
mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
vyjde	vyjít	k5eAaPmIp3nS	vyjít
také	také	k9	také
oprava	oprava	k1gFnSc1	oprava
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
příponou	přípona	k1gFnSc7	přípona
b	b	k?	b
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
i	i	k9	i
ta	ten	k3xDgFnSc1	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
nahradí	nahradit	k5eAaPmIp3nS	nahradit
ji	on	k3xPp3gFnSc4	on
c	c	k0	c
atd.	atd.	kA	atd.
Pro	pro	k7c4	pro
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
nejnovější	nový	k2eAgFnSc1d3	nejnovější
verze	verze	k1gFnSc1	verze
6.81	[number]	k4	6.81
<g/>
c.	c.	k?	c.
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
novější	nový	k2eAgFnSc1d2	novější
je	být	k5eAaImIp3nS	být
Dota	Dota	k1gFnSc1	Dota
2	[number]	k4	2
<g/>
)	)	kIx)	)
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
104	[number]	k4	104
hrdinů	hrdina	k1gMnPc2	hrdina
a	a	k8xC	a
123	[number]	k4	123
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
verzemi	verze	k1gFnPc7	verze
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DotA	DotA	k?	DotA
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
kromě	kromě	k7c2	kromě
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
public	publicum	k1gNnPc2	publicum
<g/>
"	"	kIx"	"
úrovni	úroveň	k1gFnSc6	úroveň
i	i	k8xC	i
na	na	k7c6	na
profesionální	profesionální	k2eAgFnSc6d1	profesionální
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
jsou	být	k5eAaImIp3nP	být
pořádány	pořádán	k2eAgFnPc4d1	pořádána
soutěže	soutěž	k1gFnPc4	soutěž
a	a	k8xC	a
prestižní	prestižní	k2eAgInPc4d1	prestižní
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
DotA	DotA	k1gFnPc2	DotA
na	na	k7c6	na
programu	program	k1gInSc6	program
celostátních	celostátní	k2eAgInPc2d1	celostátní
či	či	k8xC	či
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
herních	herní	k2eAgInPc2d1	herní
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
například	například	k6eAd1	například
InvexCup	InvexCup	k1gInSc4	InvexCup
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
také	také	k6eAd1	také
pořádáno	pořádat	k5eAaImNgNnS	pořádat
mnoho	mnoho	k4c1	mnoho
turnajů	turnaj	k1gInPc2	turnaj
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
společnosti	společnost	k1gFnSc2	společnost
ProGamers	ProGamers	k1gInSc1	ProGamers
o	o	k7c4	o
různé	různý	k2eAgFnPc4d1	různá
věcné	věcný	k2eAgFnPc4d1	věcná
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
turnaje	turnaj	k1gInPc4	turnaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
DotA	DotA	k1gFnSc1	DotA
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
WCG	WCG	kA	WCG
nebo	nebo	k8xC	nebo
The	The	k1gMnSc1	The
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
online	onlinout	k5eAaPmIp3nS	onlinout
turnajů	turnaj	k1gInPc2	turnaj
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
turnaj	turnaj	k1gInSc4	turnaj
The	The	k1gFnSc2	The
Defense	defense	k1gFnSc2	defense
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
profesionální	profesionální	k2eAgInPc4d1	profesionální
týmy	tým	k1gInPc4	tým
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
DotA	DotA	k1gFnSc2	DotA
scény	scéna	k1gFnSc2	scéna
patří	patřit	k5eAaImIp3nS	patřit
především	především	k6eAd1	především
<g/>
:	:	kIx,	:
Alliance	Alliance	k1gFnSc1	Alliance
<g/>
,	,	kIx,	,
Dignitas	Dignitas	k1gInSc1	Dignitas
<g/>
,	,	kIx,	,
DK	DK	kA	DK
<g/>
,	,	kIx,	,
EG	ego	k1gNnPc2	ego
<g/>
,	,	kIx,	,
Fnatic	Fnatice	k1gFnPc2	Fnatice
<g/>
,	,	kIx,	,
iG	iG	k?	iG
<g/>
,	,	kIx,	,
Kaipi	Kaipi	k1gNnSc1	Kaipi
<g/>
,	,	kIx,	,
LGD	LGD	kA	LGD
<g/>
,	,	kIx,	,
Liquid	Liquid	k1gInSc1	Liquid
<g/>
,	,	kIx,	,
Na	na	k7c6	na
<g/>
'	'	kIx"	'
<g/>
Vi	Vi	k?	Vi
<g/>
,	,	kIx,	,
mouz	mouza	k1gFnPc2	mouza
<g/>
,	,	kIx,	,
Quantic	Quantice	k1gFnPc2	Quantice
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
TongFu	TongFa	k1gMnSc4	TongFa
<g/>
,	,	kIx,	,
VP	VP	kA	VP
<g/>
,	,	kIx,	,
Zenith	Zenith	k1gInSc4	Zenith
Za	za	k7c4	za
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
hráče	hráč	k1gMnPc4	hráč
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považují	považovat	k5eAaImIp3nP	považovat
<g/>
:	:	kIx,	:
<g/>
Ferrari_	Ferrari_	k1gFnSc1	Ferrari_
<g/>
430	[number]	k4	430
<g/>
,	,	kIx,	,
<g/>
AdmiralBulldog	AdmiralBulldog	k1gInSc1	AdmiralBulldog
<g/>
,	,	kIx,	,
Akke	Akke	k1gInSc1	Akke
<g/>
,	,	kIx,	,
ArsArt	ArsArt	k1gInSc1	ArsArt
(	(	kIx(	(
<g/>
Smile	smil	k1gInSc5	smil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ArtStyle	ArtStyl	k1gInSc5	ArtStyl
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
<g/>
,	,	kIx,	,
BurNing	BurNing	k1gInSc1	BurNing
<g/>
,	,	kIx,	,
DDC	DDC	kA	DDC
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Dendi	Dend	k1gMnPc1	Dend
<g/>
,	,	kIx,	,
EGM	EGM	kA	EGM
<g/>
,,	,,	k?	,,
HyHy	HyHa	k1gMnSc2	HyHa
<g/>
,	,	kIx,	,
Iceiceice	Iceiceic	k1gMnSc2	Iceiceic
<g/>
,	,	kIx,	,
Korok	Korok	k1gInSc1	Korok
<g/>
,	,	kIx,	,
KuroKy	KuroKy	k1gInPc1	KuroKy
<g/>
,	,	kIx,	,
LightOfHeaven	LightOfHeaven	k2eAgMnSc1d1	LightOfHeaven
<g/>
,	,	kIx,	,
Loda	Loda	k1gMnSc1	Loda
<g/>
,	,	kIx,	,
LongDD	LongDD	k1gMnSc1	LongDD
<g/>
,	,	kIx,	,
Maelk	Maelk	k1gMnSc1	Maelk
<g/>
,	,	kIx,	,
Merlini	Merlin	k2eAgMnPc1d1	Merlin
<g/>
,	,	kIx,	,
Misery	Miser	k1gInPc1	Miser
<g/>
,	,	kIx,	,
NS	NS	kA	NS
<g/>
,	,	kIx,	,
Pajkatt	Pajkatt	k1gInSc1	Pajkatt
<g/>
,	,	kIx,	,
Puppey	Puppey	k1gInPc1	Puppey
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
SexyBamboe	SexyBamboe	k1gFnSc1	SexyBamboe
<g/>
,	,	kIx,	,
<g/>
SingSing	SingSing	k1gInSc1	SingSing
<g/>
,	,	kIx,	,
Sylar	Sylar	k1gInSc1	Sylar
<g/>
,	,	kIx,	,
Vigoss	Vigoss	k1gInSc1	Vigoss
<g/>
,	,	kIx,	,
YamateH	YamateH	k1gMnSc1	YamateH
<g/>
,	,	kIx,	,
YaphetS	YaphetS	k1gMnSc1	YaphetS
(	(	kIx(	(
<g/>
PIS	Pisa	k1gFnPc2	Pisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
xiao	xiao	k6eAd1	xiao
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
yyF	yyF	k?	yyF
<g/>
,	,	kIx,	,
zhou	zhoa	k1gMnSc4	zhoa
<g/>
,	,	kIx,	,
ZSMJ	ZSMJ	kA	ZSMJ
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
YaphetS	YaphetS	k1gMnPc1	YaphetS
<g/>
,	,	kIx,	,
Merlini	Merlin	k2eAgMnPc1d1	Merlin
a	a	k8xC	a
820	[number]	k4	820
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
speciální	speciální	k2eAgNnSc4d1	speciální
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
hráči	hráč	k1gMnPc7	hráč
DotA	DotA	k1gMnSc1	DotA
<g/>
.	.	kIx.	.
</s>
<s>
YaphetS	YaphetS	k?	YaphetS
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
hrdiny	hrdina	k1gMnSc2	hrdina
Nevermore	Nevermor	k1gInSc5	Nevermor
(	(	kIx(	(
<g/>
Shadow	Shadow	k1gMnSc1	Shadow
Friend	Friend	k1gMnSc1	Friend
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Merlini	Merlin	k2eAgMnPc1d1	Merlin
se	s	k7c7	s
stal	stát	k5eAaPmAgInS	stát
legendárním	legendární	k2eAgMnSc6d1	legendární
díky	díky	k7c3	díky
hrám	hra	k1gFnPc3	hra
za	za	k7c4	za
hrdinu	hrdina	k1gMnSc4	hrdina
Zeuse	Zeusa	k1gFnSc6	Zeusa
<g/>
,	,	kIx,	,
820	[number]	k4	820
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
průkopníkem	průkopník	k1gMnSc7	průkopník
hry	hra	k1gFnSc2	hra
za	za	k7c4	za
hrdinu	hrdina	k1gMnSc4	hrdina
Vengeful	Vengeful	k1gInSc4	Vengeful
Spirit	Spirita	k1gFnPc2	Spirita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pocta	pocta	k1gFnSc1	pocta
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
3	[number]	k4	3
hráče	hráč	k1gMnPc4	hráč
se	se	k3xPyFc4	se
zmiňovaní	zmiňovaný	k2eAgMnPc1d1	zmiňovaný
hrdinové	hrdina	k1gMnPc1	hrdina
občas	občas	k6eAd1	občas
nejmenují	jmenovat	k5eNaImIp3nP	jmenovat
Nevermore	Nevermor	k1gInSc5	Nevermor
<g/>
,	,	kIx,	,
Zeus	Zeusa	k1gFnPc2	Zeusa
a	a	k8xC	a
Vengeful	Vengefula	k1gFnPc2	Vengefula
Spirit	Spirita	k1gFnPc2	Spirita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
YaphetS	YaphetS	k1gMnPc1	YaphetS
<g/>
,	,	kIx,	,
Merlini	Merlin	k2eAgMnPc1d1	Merlin
a	a	k8xC	a
820	[number]	k4	820
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
poznamenání	poznamenání	k1gNnSc4	poznamenání
také	také	k9	také
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
DotA	DotA	k1gFnSc1	DotA
"	"	kIx"	"
<g/>
jen	jen	k6eAd1	jen
<g/>
"	"	kIx"	"
módem	mód	k1gInSc7	mód
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
Warcraft	Warcraft	k2eAgInSc4d1	Warcraft
3	[number]	k4	3
Ledový	ledový	k2eAgInSc4d1	ledový
trůn	trůn	k1gInSc4	trůn
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
i	i	k9	i
samostatná	samostatný	k2eAgFnSc1d1	samostatná
hra	hra	k1gFnSc1	hra
Dota	Dota	k1gFnSc1	Dota
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
se	se	k3xPyFc4	se
její	její	k3xOp3gInPc1	její
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
systém	systém	k1gInSc4	systém
oproti	oproti	k7c3	oproti
samotnému	samotný	k2eAgMnSc3d1	samotný
Warcraftu	Warcraft	k1gMnSc3	Warcraft
III	III	kA	III
tFT	tFT	k?	tFT
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Abyste	aby	kYmCp2nP	aby
mohli	moct	k5eAaImAgMnP	moct
Dotu	Dota	k1gFnSc4	Dota
hrát	hrát	k5eAaImF	hrát
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Vás	vy	k3xPp2nPc4	vy
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
části	část	k1gFnSc6	část
hry	hra	k1gFnSc2	hra
nezaskočilo	zaskočit	k5eNaPmAgNnS	zaskočit
sekání	sekání	k1gNnSc4	sekání
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc4d1	jiný
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
Váš	váš	k3xOp2gInSc4	váš
počítač	počítač	k1gInSc4	počítač
splňovat	splňovat	k5eAaImF	splňovat
minimálně	minimálně	k6eAd1	minimálně
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
Processor	Processor	k1gMnSc1	Processor
:	:	kIx,	:
Intel	Intel	kA	Intel
Pentium	Pentium	kA	Pentium
4	[number]	k4	4
1600	[number]	k4	1600
<g/>
Mhz	Mhz	kA	Mhz
/	/	kIx~	/
AMD	AMD	kA	AMD
Athlon	Athlon	k1gInSc4	Athlon
hodnocený	hodnocený	k2eAgInSc4d1	hodnocený
na	na	k7c4	na
1400	[number]	k4	1400
<g/>
+	+	kIx~	+
RAM	RAM	kA	RAM
:	:	kIx,	:
512	[number]	k4	512
MB	MB	kA	MB
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
2000	[number]	k4	2000
/	/	kIx~	/
XP	XP	kA	XP
<g/>
)	)	kIx)	)
;	;	kIx,	;
1GB	[number]	k4	1GB
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Win	Win	k1gFnSc4	Win
Vista	vista	k2eAgMnSc1d1	vista
<g/>
)	)	kIx)	)
Grafická	grafický	k2eAgFnSc1d1	grafická
karta	karta	k1gFnSc1	karta
:	:	kIx,	:
GeForce	GeForka	k1gFnSc6	GeForka
4	[number]	k4	4
s	s	k7c7	s
32-64MB	[number]	k4	32-64MB
onboard	onboard	k1gInSc4	onboard
paměti	paměť	k1gFnSc2	paměť
nebo	nebo	k8xC	nebo
ekvivalentní	ekvivalentní	k2eAgNnSc4d1	ekvivalentní
Internetové	internetový	k2eAgNnSc4d1	internetové
připojení	připojení	k1gNnSc4	připojení
:	:	kIx,	:
256	[number]	k4	256
<g/>
/	/	kIx~	/
<g/>
128	[number]	k4	128
kb	kb	kA	kb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
down	down	k1gMnSc1	down
<g/>
/	/	kIx~	/
<g/>
up	up	k?	up
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
byste	by	kYmCp2nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
hry	hra	k1gFnPc4	hra
zakládat	zakládat	k5eAaImF	zakládat
(	(	kIx(	(
<g/>
hostovat	hostovat	k5eAaImF	hostovat
<g/>
)	)	kIx)	)
tak	tak	k9	tak
512	[number]	k4	512
<g/>
/	/	kIx~	/
<g/>
256	[number]	k4	256
kb	kb	kA	kb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
down	down	k1gMnSc1	down
<g/>
/	/	kIx~	/
<g/>
up	up	k?	up
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nasměrovaný	nasměrovaný	k2eAgInSc1d1	nasměrovaný
herní	herní	k2eAgInSc1d1	herní
port	port	k1gInSc1	port
(	(	kIx(	(
<g/>
výchozí	výchozí	k2eAgFnSc1d1	výchozí
6112	[number]	k4	6112
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připojení	připojení	k1gNnSc4	připojení
s	s	k7c7	s
nízkým	nízký	k2eAgNnSc7d1	nízké
a	a	k8xC	a
stálým	stálý	k2eAgNnSc7d1	stálé
pingem	pingo	k1gNnSc7	pingo
bez	bez	k7c2	bez
packet	packet	k1gInSc4	packet
lossu	lossa	k1gFnSc4	lossa
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nesmíme	smět	k5eNaImIp1nP	smět
opomenout	opomenout	k5eAaPmF	opomenout
různé	různý	k2eAgFnPc4d1	různá
platformy	platforma	k1gFnPc4	platforma
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
Dotu	Dota	k1gFnSc4	Dota
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Battle	Battle	k1gFnPc1	Battle
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgInSc1d1	oficiální
server	server	k1gInSc1	server
od	od	k7c2	od
Blizzard	Blizzarda	k1gFnPc2	Blizzarda
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eurobattle	Eurobattle	k1gFnSc1	Eurobattle
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
patří	patřit	k5eAaImIp3nS	patřit
Garena	Garena	k1gFnSc1	Garena
a	a	k8xC	a
Hamachi	Hamach	k1gFnPc1	Hamach
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
využívají	využívat	k5eAaPmIp3nP	využívat
rozhraní	rozhraní	k1gNnSc4	rozhraní
LAN	lano	k1gNnPc2	lano
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
emulují	emulovat	k5eAaImIp3nP	emulovat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
také	také	k6eAd1	také
hodnocené	hodnocený	k2eAgMnPc4d1	hodnocený
herní	herní	k2eAgMnPc4d1	herní
klienty	klient	k1gMnPc4	klient
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
lze	lze	k6eAd1	lze
za	za	k7c4	za
dobré	dobrý	k2eAgNnSc4d1	dobré
umístění	umístění	k1gNnSc4	umístění
získat	získat	k5eAaPmF	získat
i	i	k9	i
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
patří	patřit	k5eAaImIp3nS	patřit
RGC	RGC	kA	RGC
a	a	k8xC	a
Dotalicious	Dotalicious	k1gInSc1	Dotalicious
<g/>
.	.	kIx.	.
</s>
