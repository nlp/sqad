<s>
Max	Max	k1gMnSc1	Max
Theiler	Theiler	k1gMnSc1	Theiler
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Pretoria	Pretorium	k1gNnSc2	Pretorium
JAR	Jara	k1gFnPc2	Jara
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Haven	Haven	k1gInSc1	Haven
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jihoafrický	jihoafrický	k2eAgMnSc1d1	jihoafrický
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
virolog	virolog	k1gMnSc1	virolog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelová	k1gFnSc7	Nobelová
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
za	za	k7c4	za
vyvinutí	vyvinutí	k1gNnSc4	vyvinutí
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
žluté	žlutý	k2eAgFnSc3d1	žlutá
zimnici	zimnice	k1gFnSc3	zimnice
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Max	max	kA	max
Theiler	Theiler	k1gInSc4	Theiler
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
