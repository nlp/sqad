<p>
<s>
Kolonizace	kolonizace	k1gFnSc1	kolonizace
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
obsazení	obsazení	k1gNnSc2	obsazení
posledních	poslední	k2eAgNnPc2d1	poslední
míst	místo	k1gNnPc2	místo
patřících	patřící	k2eAgFnPc2d1	patřící
domorodým	domorodý	k2eAgMnPc3d1	domorodý
obyvatelům	obyvatel	k1gMnPc3	obyvatel
(	(	kIx(	(
<g/>
indiánům	indián	k1gMnPc3	indián
<g/>
)	)	kIx)	)
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kolonizaci	kolonizace	k1gFnSc6	kolonizace
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
se	se	k3xPyFc4	se
angažovali	angažovat	k5eAaBmAgMnP	angažovat
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
také	také	k9	také
Švédové	Švéd	k1gMnPc1	Švéd
a	a	k8xC	a
Rusové	Rus	k1gMnPc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Geograficky	geograficky	k6eAd1	geograficky
se	se	k3xPyFc4	se
Angličané	Angličan	k1gMnPc1	Angličan
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
soustředili	soustředit	k5eAaPmAgMnP	soustředit
především	především	k6eAd1	především
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
severnějších	severní	k2eAgFnPc2d2	severnější
oblastí	oblast	k1gFnPc2	oblast
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
nejsevernější	severní	k2eAgFnPc1d3	nejsevernější
oblasti	oblast	k1gFnPc1	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Aljašky	Aljaška	k1gFnSc2	Aljaška
byly	být	k5eAaImAgInP	být
kolonizovány	kolonizovat	k5eAaBmNgInP	kolonizovat
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
Aljašku	Aljaška	k1gFnSc4	Aljaška
prodalo	prodat	k5eAaPmAgNnS	prodat
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
jih	jih	k1gInSc1	jih
dnešního	dnešní	k2eAgMnSc2d1	dnešní
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ovládalo	ovládat	k5eAaImAgNnS	ovládat
celou	celá	k1gFnSc4	celá
či	či	k8xC	či
většinu	většina	k1gFnSc4	většina
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
Texasu	Texas	k1gInSc2	Texas
<g/>
,	,	kIx,	,
Arizony	Arizona	k1gFnSc2	Arizona
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
i	i	k8xC	i
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
Mexiko	Mexiko	k1gNnSc1	Mexiko
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
španělskými	španělský	k2eAgMnPc7d1	španělský
conquistadory	conquistador	k1gMnPc7	conquistador
hned	hned	k6eAd1	hned
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
oblastí	oblast	k1gFnPc2	oblast
pod	pod	k7c7	pod
evropskou	evropský	k2eAgFnSc7d1	Evropská
kontrolou	kontrola	k1gFnSc7	kontrola
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentě	kontinent	k1gInSc6	kontinent
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Začátky	začátek	k1gInPc1	začátek
kolonizace	kolonizace	k1gFnSc1	kolonizace
dnešních	dnešní	k2eAgInPc2d1	dnešní
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podnikali	podnikat	k5eAaImAgMnP	podnikat
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
severnějších	severní	k2eAgFnPc2d2	severnější
oblastí	oblast	k1gFnPc2	oblast
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
cestovali	cestovat	k5eAaImAgMnP	cestovat
především	především	k6eAd1	především
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
státy	stát	k1gInPc4	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
a	a	k8xC	a
Texas	Texas	k1gInSc1	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Francouzi	Francouz	k1gMnPc1	Francouz
prozkoumávali	prozkoumávat	k5eAaImAgMnP	prozkoumávat
spíše	spíše	k9	spíše
sever	sever	k1gInSc4	sever
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
dali	dát	k5eAaPmAgMnP	dát
název	název	k1gInSc4	název
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tordesillaská	Tordesillaský	k2eAgFnSc1d1	Tordesillaská
smlouva	smlouva	k1gFnSc1	smlouva
přisoudila	přisoudit	k5eAaPmAgFnS	přisoudit
všechny	všechen	k3xTgFnPc4	všechen
dosud	dosud	k6eAd1	dosud
neobjevené	objevený	k2eNgFnPc4d1	neobjevená
země	zem	k1gFnPc4	zem
Španělům	Španěl	k1gMnPc3	Španěl
a	a	k8xC	a
Portugalcům	Portugalec	k1gMnPc3	Portugalec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
králi	král	k1gMnSc3	král
</s>
</p>
<p>
<s>
Františku	František	k1gMnSc3	František
I.	I.	kA	I.
Ten	ten	k3xDgInSc1	ten
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
monopolní	monopolní	k2eAgNnSc4d1	monopolní
postavení	postavení	k1gNnSc4	postavení
svých	svůj	k3xOyFgMnPc2	svůj
sousedů	soused	k1gMnPc2	soused
nebude	být	k5eNaImBp3nS	být
již	již	k9	již
nadále	nadále	k6eAd1	nadále
respektovat	respektovat	k5eAaImF	respektovat
a	a	k8xC	a
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
smlouvu	smlouva	k1gFnSc4	smlouva
pověřil	pověřit	k5eAaPmAgMnS	pověřit
roku	rok	k1gInSc2	rok
1523	[number]	k4	1523
velmi	velmi	k6eAd1	velmi
zkušeného	zkušený	k2eAgMnSc2d1	zkušený
italského	italský	k2eAgMnSc2d1	italský
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
Giovanniho	Giovanni	k1gMnSc2	Giovanni
da	da	k?	da
Verrazano	Verrazana	k1gFnSc5	Verrazana
nalezením	nalezení	k1gNnSc7	nalezení
legendárního	legendární	k2eAgInSc2d1	legendární
průjezdu	průjezd	k1gInSc2	průjezd
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
přes	přes	k7c4	přes
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Verrazano	Verrazana	k1gFnSc5	Verrazana
proplul	proplout	k5eAaPmAgMnS	proplout
okolo	okolo	k7c2	okolo
pobřeží	pobřeží	k1gNnSc2	pobřeží
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
a	a	k8xC	a
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
Foundlandu	Foundland	k1gInSc3	Foundland
a	a	k8xC	a
pak	pak	k6eAd1	pak
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
nepřinesla	přinést	k5eNaPmAgFnS	přinést
žádné	žádný	k3yNgFnPc4	žádný
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
průplavu	průplav	k1gInSc6	průplav
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
krále	král	k1gMnSc4	král
velmi	velmi	k6eAd1	velmi
potěšila	potěšit	k5eAaPmAgFnS	potěšit
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
činit	činit	k5eAaImF	činit
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
území	území	k1gNnPc4	území
pojmenované	pojmenovaný	k2eAgFnPc1d1	pojmenovaná
Nová	Nová	k1gFnSc1	Nová
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Války	válka	k1gFnPc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
Františkovi	František	k1gMnSc3	František
znemožnily	znemožnit	k5eAaPmAgInP	znemožnit
další	další	k2eAgMnPc4d1	další
výpravy	výprava	k1gMnPc4	výprava
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
neobdržel	obdržet	k5eNaPmAgInS	obdržet
dopis	dopis	k1gInSc1	dopis
od	od	k7c2	od
Jacquese	Jacques	k1gMnSc2	Jacques
Cartiera	Cartier	k1gMnSc2	Cartier
<g/>
.	.	kIx.	.
</s>
<s>
Cartier	Cartier	k1gMnSc1	Cartier
byl	být	k5eAaImAgMnS	být
rybářem	rybář	k1gMnSc7	rybář
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Atlantiku	Atlantik	k1gInSc6	Atlantik
a	a	k8xC	a
posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
trávil	trávit	k5eAaImAgInS	trávit
přepadáním	přepadání	k1gNnSc7	přepadání
španělských	španělský	k2eAgFnPc2d1	španělská
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
králi	král	k1gMnSc3	král
se	se	k3xPyFc4	se
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
jako	jako	k9	jako
vůdce	vůdce	k1gMnSc1	vůdce
nové	nový	k2eAgFnSc2d1	nová
výpravy	výprava	k1gFnSc2	výprava
pátrající	pátrající	k2eAgMnPc1d1	pátrající
po	po	k7c6	po
průjezdu	průjezd	k1gInSc6	průjezd
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
neváhal	váhat	k5eNaImAgMnS	váhat
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Cartierovi	Cartier	k1gMnSc3	Cartier
dvě	dva	k4xCgFnPc1	dva
menší	malý	k2eAgFnPc1d2	menší
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cartier	Cartier	k1gInSc1	Cartier
plul	plout	k5eAaImAgInS	plout
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
Foundlandu	Foundland	k1gInSc3	Foundland
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
řeky	řeka	k1gFnSc2	řeka
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zakotvil	zakotvit	k5eAaPmAgInS	zakotvit
u	u	k7c2	u
poloostrova	poloostrov	k1gInSc2	poloostrov
Gaspé	Gaspý	k2eAgNnSc1d1	Gaspé
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
přátelskými	přátelský	k2eAgMnPc7d1	přátelský
indiány	indián	k1gMnPc7	indián
kmene	kmen	k1gInSc2	kmen
Mikmak	Mikmak	k1gMnSc1	Mikmak
<g/>
.	.	kIx.	.
</s>
<s>
Mikmakové	Mikmakový	k2eAgNnSc4d1	Mikmakový
chtěli	chtít	k5eAaImAgMnP	chtít
hned	hned	k9	hned
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
obchodovat	obchodovat	k5eAaImF	obchodovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
za	za	k7c4	za
několik	několik	k4yIc4	několik
cetek	cetka	k1gFnPc2	cetka
získali	získat	k5eAaPmAgMnP	získat
námořníci	námořník	k1gMnPc1	námořník
od	od	k7c2	od
indiánů	indián	k1gMnPc2	indián
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
;	;	kIx,	;
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
pak	pak	k6eAd1	pak
Cartier	Cartier	k1gMnSc1	Cartier
nechal	nechat	k5eAaPmAgMnS	nechat
vztyčit	vztyčit	k5eAaPmF	vztyčit
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
vzal	vzít	k5eAaPmAgMnS	vzít
dva	dva	k4xCgMnPc4	dva
Mikmaky	Mikmak	k1gMnPc4	Mikmak
od	od	k7c2	od
nichž	jenž	k3xRgInPc2	jenž
získal	získat	k5eAaPmAgInS	získat
cenné	cenný	k2eAgFnPc4d1	cenná
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dobré	dobrý	k2eAgMnPc4d1	dobrý
tlumočníky	tlumočník	k1gMnPc4	tlumočník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
ještě	ještě	k6eAd1	ještě
příznivější	příznivý	k2eAgMnSc1d2	příznivější
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
Cartier	Cartier	k1gInSc1	Cartier
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	s	k7c7	s
3	[number]	k4	3
loděmi	loď	k1gFnPc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc7d1	stejná
cestou	cesta	k1gFnSc7	cesta
jako	jako	k8xS	jako
při	při	k7c6	při
předchozí	předchozí	k2eAgFnSc6d1	předchozí
výpravě	výprava	k1gFnSc6	výprava
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
den	den	k1gInSc1	den
svátku	svátek	k1gInSc2	svátek
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
)	)	kIx)	)
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
veliké	veliký	k2eAgFnSc2d1	veliká
řeky	řeka	k1gFnSc2	řeka
tehdy	tehdy	k6eAd1	tehdy
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
řekou	řeka	k1gFnSc7	řeka
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k9	až
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
zvané	zvaný	k2eAgFnSc2d1	zvaná
Hochelaga	Hochelaga	k1gFnSc1	Hochelaga
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Montreal	Montreal	k1gInSc1	Montreal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
také	také	k9	také
dověděl	dovědět	k5eAaPmAgMnS	dovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
jsou	být	k5eAaImIp3nP	být
početné	početný	k2eAgFnPc1d1	početná
peřeje	peřej	k1gFnPc1	peřej
naprosto	naprosto	k6eAd1	naprosto
vylučující	vylučující	k2eAgInSc4d1	vylučující
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Cartier	Cartier	k1gMnSc1	Cartier
byl	být	k5eAaImAgMnS	být
zklamán	zklamat	k5eAaPmNgMnS	zklamat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
věřil	věřit	k5eAaImAgMnS	věřit
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
podniká	podnikat	k5eAaImIp3nS	podnikat
další	další	k2eAgFnSc4d1	další
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
pochopitelně	pochopitelně	k6eAd1	pochopitelně
neúspěšnou	úspěšný	k2eNgFnSc4d1	neúspěšná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
výpravy	výprava	k1gFnPc1	výprava
znemožnily	znemožnit	k5eAaPmAgFnP	znemožnit
náboženské	náboženský	k2eAgFnPc1d1	náboženská
války	válka	k1gFnPc1	válka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
občas	občas	k6eAd1	občas
zabloudily	zabloudit	k5eAaPmAgFnP	zabloudit
rybářské	rybářský	k2eAgFnPc1d1	rybářská
lodě	loď	k1gFnPc1	loď
Francouzů	Francouz	k1gMnPc2	Francouz
k	k	k7c3	k
Newfoundlandu	Newfoundlando	k1gNnSc3	Newfoundlando
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
velmi	velmi	k6eAd1	velmi
levně	levně	k6eAd1	levně
získali	získat	k5eAaPmAgMnP	získat
kožešiny	kožešina	k1gFnSc2	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
obchod	obchod	k1gInSc1	obchod
tak	tak	k6eAd1	tak
výnosný	výnosný	k2eAgInSc1d1	výnosný
<g/>
,	,	kIx,	,
že	že	k8xS	že
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
prvních	první	k4xOgFnPc2	první
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
měli	mít	k5eAaImAgMnP	mít
sloužit	sloužit	k5eAaImF	sloužit
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
uděloval	udělovat	k5eAaImAgMnS	udělovat
určitým	určitý	k2eAgNnSc7d1	určité
osobám	osoba	k1gFnPc3	osoba
monopol	monopol	k1gInSc1	monopol
na	na	k7c4	na
koupi	koupě	k1gFnSc4	koupě
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
usídlit	usídlit	k5eAaPmF	usídlit
jako	jako	k9	jako
kolonisté	kolonista	k1gMnPc1	kolonista
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
obchodníci	obchodník	k1gMnPc1	obchodník
měli	mít	k5eAaImAgMnP	mít
větší	veliký	k2eAgInSc4d2	veliký
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
obchod	obchod	k1gInSc4	obchod
než	než	k8xS	než
o	o	k7c4	o
kolonizaci	kolonizace	k1gFnSc4	kolonizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
by	by	kYmCp3nP	by
Francouzi	Francouz	k1gMnPc1	Francouz
nikdy	nikdy	k6eAd1	nikdy
nic	nic	k3yNnSc4	nic
trvale	trvale	k6eAd1	trvale
nezískali	získat	k5eNaPmAgMnP	získat
nebýt	být	k5eNaImF	být
nadlidského	nadlidský	k2eAgNnSc2d1	nadlidské
úsilí	úsilí	k1gNnSc2	úsilí
Samuela	Samuel	k1gMnSc2	Samuel
De	De	k?	De
Champlaina	Champlain	k1gMnSc2	Champlain
<g/>
.	.	kIx.	.
</s>
<s>
Champlain	Champlain	k1gMnSc1	Champlain
byl	být	k5eAaImAgMnS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
katolík	katolík	k1gMnSc1	katolík
a	a	k8xC	a
velký	velký	k2eAgMnSc1d1	velký
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnSc1d1	působící
zprvu	zprvu	k6eAd1	zprvu
ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Francouzskému	francouzský	k2eAgMnSc3d1	francouzský
králi	král	k1gMnSc3	král
Jindřichu	Jindřich	k1gMnSc3	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
předal	předat	k5eAaPmAgMnS	předat
zprávu	zpráva	k1gFnSc4	zpráva
a	a	k8xC	a
mapy	mapa	k1gFnPc4	mapa
většiny	většina	k1gFnSc2	většina
významných	významný	k2eAgInPc2d1	významný
španělských	španělský	k2eAgInPc2d1	španělský
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
udělil	udělit	k5eAaPmAgInS	udělit
šlechtický	šlechtický	k2eAgInSc1d1	šlechtický
titul	titul	k1gInSc1	titul
a	a	k8xC	a
osobní	osobní	k2eAgFnSc4d1	osobní
rentu	renta	k1gFnSc4	renta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Champlaina	Champlain	k1gMnSc4	Champlain
však	však	k9	však
život	život	k1gInSc4	život
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
nebyl	být	k5eNaImAgMnS	být
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1603	[number]	k4	1603
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
výpravě	výprava	k1gFnSc3	výprava
jistého	jistý	k2eAgMnSc2d1	jistý
majitele	majitel	k1gMnSc2	majitel
monopolu	monopol	k1gInSc2	monopol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výprava	výprava	k1gFnSc1	výprava
zamířila	zamířit	k5eAaPmAgFnS	zamířit
k	k	k7c3	k
oné	onen	k3xDgFnSc3	onen
vesnici	vesnice	k1gFnSc3	vesnice
Hochelage	Hochelage	k1gInSc4	Hochelage
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
již	již	k6eAd1	již
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
téměř	téměř	k6eAd1	téměř
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc4	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Algonkinové	Algonkinová	k1gFnSc2	Algonkinová
totiž	totiž	k9	totiž
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
před	před	k7c7	před
loupeživými	loupeživý	k2eAgMnPc7d1	loupeživý
Irokézy	Irokéza	k1gFnSc2	Irokéza
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
výprava	výprava	k1gFnSc1	výprava
skončila	skončit	k5eAaPmAgFnS	skončit
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
nemalé	malý	k2eNgNnSc1d1	nemalé
množství	množství	k1gNnSc1	množství
kožešin	kožešina	k1gFnPc2	kožešina
za	za	k7c4	za
směšnou	směšný	k2eAgFnSc4d1	směšná
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
Champlain	Champlain	k1gMnSc1	Champlain
přidává	přidávat	k5eAaImIp3nS	přidávat
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
monopolní	monopolní	k2eAgFnSc3d1	monopolní
výpravě	výprava	k1gFnSc3	výprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
usídlit	usídlit	k5eAaPmF	usídlit
100	[number]	k4	100
kolonistů	kolonista	k1gMnPc2	kolonista
<g/>
.	.	kIx.	.
</s>
<s>
Kolonisté	kolonista	k1gMnPc1	kolonista
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
usídlit	usídlit	k5eAaPmF	usídlit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sv.	sv.	kA	sv.
Kříže	Kříž	k1gMnSc2	Kříž
<g/>
,	,	kIx,	,
krutá	krutý	k2eAgFnSc1d1	krutá
zima	zima	k1gFnSc1	zima
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
jižněji	jižně	k6eAd2	jižně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
mírná	mírný	k2eAgFnSc1d1	mírná
zima	zima	k1gFnSc1	zima
pomohla	pomoct	k5eAaPmAgFnS	pomoct
udržet	udržet	k5eAaPmF	udržet
nadále	nadále	k6eAd1	nadále
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Champlain	Champlain	k1gInSc1	Champlain
poté	poté	k6eAd1	poté
zmapoval	zmapovat	k5eAaPmAgInS	zmapovat
celé	celý	k2eAgNnSc4d1	celé
atlantské	atlantský	k2eAgNnSc4d1	Atlantské
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1607	[number]	k4	1607
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
za	za	k7c4	za
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
vrátil	vrátit	k5eAaPmAgMnS	vrátit
jako	jako	k9	jako
zástupce	zástupce	k1gMnSc1	zástupce
Sieura	Sieur	k1gMnSc2	Sieur
de	de	k?	de
Monts	Monts	k1gInSc1	Monts
<g/>
,	,	kIx,	,
majitele	majitel	k1gMnPc4	majitel
monopolu	monopol	k1gInSc2	monopol
<g/>
.	.	kIx.	.
</s>
<s>
Monts	Monts	k6eAd1	Monts
chtěl	chtít	k5eAaImAgMnS	chtít
zřídit	zřídit	k5eAaPmF	zřídit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
stanici	stanice	k1gFnSc4	stanice
na	na	k7c4	na
co	co	k3yQnSc4	co
možná	možná	k6eAd1	možná
nejlepším	dobrý	k2eAgNnSc6d3	nejlepší
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Champlaina	Champlaina	k1gMnSc1	Champlaina
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
našel	najít	k5eAaPmAgMnS	najít
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
bývalé	bývalý	k2eAgFnSc2d1	bývalá
huronské	huronský	k2eAgFnSc2d1	huronský
vesnice	vesnice	k1gFnSc2	vesnice
Stadaconě	Stadacoň	k1gFnSc2	Stadacoň
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
zde	zde	k6eAd1	zde
pevnůstku	pevnůstka	k1gFnSc4	pevnůstka
<g/>
,	,	kIx,	,
skladiště	skladiště	k1gNnSc4	skladiště
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
Québec	Québec	k1gInSc4	Québec
<g/>
.	.	kIx.	.
</s>
<s>
Champlain	Champlain	k1gMnSc1	Champlain
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
nutnost	nutnost	k1gFnSc4	nutnost
dobře	dobře	k6eAd1	dobře
vycházet	vycházet	k5eAaImF	vycházet
s	s	k7c7	s
indiány	indián	k1gMnPc7	indián
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
zdejší	zdejší	k2eAgMnPc1d1	zdejší
indiáni	indián	k1gMnPc1	indián
(	(	kIx(	(
<g/>
Huroni	Huron	k1gMnPc1	Huron
a	a	k8xC	a
Algonkinové	Algonkin	k1gMnPc1	Algonkin
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
naučili	naučit	k5eAaPmAgMnP	naučit
pevnůstku	pevnůstka	k1gFnSc4	pevnůstka
bez	bez	k7c2	bez
bázně	bázeň	k1gFnSc2	bázeň
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
a	a	k8xC	a
výhodně	výhodně	k6eAd1	výhodně
směňovat	směňovat	k5eAaImF	směňovat
kožešiny	kožešina	k1gFnPc4	kožešina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
už	už	k6eAd1	už
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
mohl	moct	k5eAaImAgInS	moct
Champlain	Champlain	k1gInSc1	Champlain
poslat	poslat	k5eAaPmF	poslat
část	část	k1gFnSc4	část
posádky	posádka	k1gFnSc2	posádka
domů	dům	k1gInPc2	dům
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
nákladem	náklad	k1gInSc7	náklad
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
pevnosti	pevnost	k1gFnSc2	pevnost
Huroni	Huron	k1gMnPc1	Huron
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Irokézům	Irokéz	k1gMnPc3	Irokéz
<g/>
.	.	kIx.	.
</s>
<s>
Champlain	Champlain	k1gMnSc1	Champlain
se	se	k3xPyFc4	se
k	k	k7c3	k
tažení	tažení	k1gNnSc3	tažení
připojil	připojit	k5eAaPmAgInS	připojit
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
bělochů	běloch	k1gMnPc2	běloch
a	a	k8xC	a
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
učinila	učinit	k5eAaPmAgFnS	učinit
toto	tento	k3xDgNnSc4	tento
vítězství	vítězství	k1gNnSc4	vítězství
velice	velice	k6eAd1	velice
lehkým	lehký	k2eAgMnPc3d1	lehký
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Champlain	Champlain	k1gInSc1	Champlain
udělal	udělat	k5eAaPmAgInS	udělat
zcela	zcela	k6eAd1	zcela
osudovou	osudový	k2eAgFnSc4d1	osudová
taktickou	taktický	k2eAgFnSc4d1	taktická
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Irokézové	Irokéz	k1gMnPc1	Irokéz
byli	být	k5eAaImAgMnP	být
hrdými	hrdý	k2eAgMnPc7d1	hrdý
válečníky	válečník	k1gMnPc7	válečník
a	a	k8xC	a
taky	taky	k6eAd1	taky
poměrně	poměrně	k6eAd1	poměrně
početným	početný	k2eAgInSc7d1	početný
kmenem	kmen	k1gInSc7	kmen
a	a	k8xC	a
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
příkoří	příkoří	k1gNnSc4	příkoří
nikdy	nikdy	k6eAd1	nikdy
nezapomněli	zapomenout	k5eNaPmAgMnP	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Plenili	plenit	k5eAaImAgMnP	plenit
francouzské	francouzský	k2eAgFnPc4d1	francouzská
kolonie	kolonie	k1gFnPc4	kolonie
a	a	k8xC	a
sehráli	sehrát	k5eAaPmAgMnP	sehrát
také	také	k9	také
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
Québecu	Québecus	k1gInSc2	Québecus
Brity	Brit	k1gMnPc7	Brit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
Champlain	Champlain	k1gMnSc1	Champlain
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
sám	sám	k3xTgMnSc1	sám
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
opětovný	opětovný	k2eAgInSc1d1	opětovný
rychlý	rychlý	k2eAgInSc1d1	rychlý
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Québecu	Québecus	k1gInSc2	Québecus
zachránil	zachránit	k5eAaPmAgInS	zachránit
dost	dost	k6eAd1	dost
možná	možná	k9	možná
celou	celý	k2eAgFnSc4d1	celá
Novou	nový	k2eAgFnSc4d1	nová
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Monts	Monts	k1gInSc1	Monts
totiž	totiž	k9	totiž
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
monopol	monopol	k1gInSc4	monopol
a	a	k8xC	a
houfy	houf	k1gInPc4	houf
chamtivých	chamtivý	k2eAgMnPc2d1	chamtivý
obchodníků	obchodník	k1gMnPc2	obchodník
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
valit	valit	k5eAaImF	valit
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Bobří	bobří	k2eAgMnPc1d1	bobří
kožešiny	kožešina	k1gFnSc2	kožešina
získávali	získávat	k5eAaImAgMnP	získávat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
pálenky	pálenka	k1gFnSc2	pálenka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
rozbroje	rozbroj	k1gInPc4	rozbroj
mezi	mezi	k7c7	mezi
samotnými	samotný	k2eAgMnPc7d1	samotný
indiány	indián	k1gMnPc7	indián
<g/>
.	.	kIx.	.
</s>
<s>
Champlain	Champlain	k1gMnSc1	Champlain
nakonec	nakonec	k6eAd1	nakonec
s	s	k7c7	s
nadlidským	nadlidský	k2eAgNnSc7d1	nadlidské
úsilím	úsilí	k1gNnSc7	úsilí
udržel	udržet	k5eAaPmAgMnS	udržet
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
nucen	nutit	k5eAaImNgMnS	nutit
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
vyprosit	vyprosit	k5eAaPmF	vyprosit
si	se	k3xPyFc3	se
oficiální	oficiální	k2eAgFnSc4d1	oficiální
pravomoc	pravomoc	k1gFnSc4	pravomoc
nad	nad	k7c7	nad
Quebecem	Quebeec	k1gInSc7	Quebeec
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
Huronů	Huron	k1gMnPc2	Huron
již	již	k6eAd1	již
utekla	utéct	k5eAaPmAgFnS	utéct
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1612	[number]	k4	1612
čekalo	čekat	k5eAaImAgNnS	čekat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
Huronů	Huron	k1gInPc2	Huron
marně	marně	k6eAd1	marně
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zklamaný	zklamaný	k2eAgMnSc1d1	zklamaný
Champlain	Champlain	k1gMnSc1	Champlain
opět	opět	k6eAd1	opět
odjel	odjet	k5eAaPmAgMnS	odjet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
potkal	potkat	k5eAaPmAgInS	potkat
s	s	k7c7	s
františkánským	františkánský	k2eAgInSc7d1	františkánský
mnichem	mnich	k1gInSc7	mnich
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Québecu	Québecus	k1gInSc2	Québecus
vrátil	vrátit	k5eAaPmAgInS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
bezohledné	bezohledný	k2eAgMnPc4d1	bezohledný
obchodníky	obchodník	k1gMnPc4	obchodník
a	a	k8xC	a
podporou	podpora	k1gFnSc7	podpora
sice	sice	k8xC	sice
neúspěšného	úspěšný	k2eNgNnSc2d1	neúspěšné
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
prospěšného	prospěšný	k2eAgInSc2d1	prospěšný
útoku	útok	k1gInSc2	útok
vůči	vůči	k7c3	vůči
Irokézům	Irokéz	k1gMnPc3	Irokéz
získal	získat	k5eAaPmAgInS	získat
zpět	zpět	k6eAd1	zpět
důvěru	důvěra	k1gFnSc4	důvěra
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
kolonie	kolonie	k1gFnSc2	kolonie
a	a	k8xC	a
posilováním	posilování	k1gNnSc7	posilování
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
obchodem	obchod	k1gInSc7	obchod
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1627	[number]	k4	1627
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k2eAgMnSc1d1	skotský
pirát	pirát	k1gMnSc1	pirát
David	David	k1gMnSc1	David
Kirke	Kirke	k1gInSc4	Kirke
zablokoval	zablokovat	k5eAaPmAgMnS	zablokovat
kolonii	kolonie	k1gFnSc3	kolonie
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
donutil	donutit	k5eAaPmAgInS	donutit
ji	on	k3xPp3gFnSc4	on
hladovět	hladovět	k5eAaImF	hladovět
a	a	k8xC	a
následně	následně	k6eAd1	následně
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Champlain	Champlain	k1gMnSc1	Champlain
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
konec	konec	k1gInSc4	konec
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
však	však	k9	však
nemělo	mít	k5eNaImAgNnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1632	[number]	k4	1632
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
mír	mír	k1gInSc1	mír
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
Francie	Francie	k1gFnSc1	Francie
byla	být	k5eAaImAgFnS	být
vrácena	vrátit	k5eAaPmNgFnS	vrátit
"	"	kIx"	"
<g/>
staré	starý	k2eAgNnSc1d1	staré
<g/>
"	"	kIx"	"
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
událostech	událost	k1gFnPc6	událost
začal	začít	k5eAaPmAgInS	začít
Quebec	Quebec	k1gInSc1	Quebec
skutečně	skutečně	k6eAd1	skutečně
nabývat	nabývat	k5eAaImF	nabývat
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
opětovným	opětovný	k2eAgNnSc7d1	opětovné
Champlainovým	Champlainův	k2eAgNnSc7d1	Champlainův
vedením	vedení	k1gNnSc7	vedení
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
významným	významný	k2eAgMnSc7d1	významný
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
nepostradatelným	postradatelný	k2eNgMnSc7d1	nepostradatelný
centrem	centr	k1gMnSc7	centr
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naneštěstí	naneštěstí	k9	naneštěstí
Champlain	Champlain	k1gMnSc1	Champlain
zemřel	zemřít	k5eAaPmAgMnS	zemřít
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kolonie	kolonie	k1gFnSc2	kolonie
se	se	k3xPyFc4	se
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
zcela	zcela	k6eAd1	zcela
přestaly	přestat	k5eAaPmAgInP	přestat
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolonie	kolonie	k1gFnSc1	kolonie
Francie	Francie	k1gFnSc2	Francie
již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zabíraly	zabírat	k5eAaImAgFnP	zabírat
území	území	k1gNnSc4	území
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
povodí	povodí	k1gNnSc2	povodí
řeky	řeka	k1gFnSc2	řeka
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
,	,	kIx,	,
problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
nebylo	být	k5eNaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
navíc	navíc	k6eAd1	navíc
byli	být	k5eAaImAgMnP	být
misionáři	misionář	k1gMnPc1	misionář
a	a	k8xC	a
jeptišky	jeptiška	k1gFnPc1	jeptiška
<g/>
,	,	kIx,	,
drtivou	drtivý	k2eAgFnSc7d1	drtivá
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Nové	Nová	k1gFnSc2	Nová
Francie	Francie	k1gFnSc1	Francie
tvořili	tvořit	k5eAaImAgMnP	tvořit
obchodníci	obchodník	k1gMnPc1	obchodník
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zdržovali	zdržovat	k5eAaImAgMnP	zdržovat
pouze	pouze	k6eAd1	pouze
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
osídlené	osídlený	k2eAgFnPc1d1	osídlená
kolonie	kolonie	k1gFnPc1	kolonie
žily	žít	k5eAaImAgFnP	žít
z	z	k7c2	z
potravin	potravina	k1gFnPc2	potravina
dovezených	dovezený	k2eAgFnPc2d1	dovezená
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
nebo	nebo	k8xC	nebo
zakoupených	zakoupený	k2eAgMnPc2d1	zakoupený
od	od	k7c2	od
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Stačilo	stačit	k5eAaBmAgNnS	stačit
aby	aby	kYmCp3nS	aby
jednu	jeden	k4xCgFnSc4	jeden
loď	loď	k1gFnSc4	loď
se	s	k7c7	s
zásobami	zásoba	k1gFnPc7	zásoba
rozbila	rozbít	k5eAaPmAgFnS	rozbít
bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
o	o	k7c6	o
pár	pár	k4xCyI	pár
týdnů	týden	k1gInPc2	týden
zpozdila	zpozdit	k5eAaPmAgFnS	zpozdit
a	a	k8xC	a
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Francii	Francie	k1gFnSc6	Francie
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
hladomory	hladomor	k1gInPc1	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Francie	Francie	k1gFnSc1	Francie
měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1658	[number]	k4	1658
cca	cca	kA	cca
3000	[number]	k4	3000
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
rozmístěných	rozmístěný	k2eAgMnPc2d1	rozmístěný
na	na	k7c6	na
území	území	k1gNnSc6	území
3	[number]	k4	3
<g/>
x	x	k?	x
větším	většit	k5eAaImIp1nS	většit
než	než	k8xS	než
sama	sám	k3xTgFnSc1	sám
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Anglie	Anglie	k1gFnSc2	Anglie
(	(	kIx(	(
<g/>
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgMnSc1d1	rozvíjející
soupeř	soupeř	k1gMnSc1	soupeř
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
přišlo	přijít	k5eAaPmAgNnS	přijít
3000	[number]	k4	3000
osadníků	osadník	k1gMnPc2	osadník
jen	jen	k9	jen
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
rok	rok	k1gInSc4	rok
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgMnS	být
ještě	ještě	k9	ještě
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaPmF	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ustání	ustání	k1gNnSc2	ustání
museli	muset	k5eAaImAgMnP	muset
kolonisté	kolonista	k1gMnPc1	kolonista
čelit	čelit	k5eAaImF	čelit
útokům	útok	k1gInPc3	útok
Irokézů	Irokéz	k1gMnPc2	Irokéz
<g/>
,	,	kIx,	,
mrazivému	mrazivý	k2eAgNnSc3d1	mrazivé
počasí	počasí	k1gNnSc3	počasí
<g/>
,	,	kIx,	,
nemocem	nemoc	k1gFnPc3	nemoc
a	a	k8xC	a
následkům	následek	k1gInPc3	následek
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
začal	začít	k5eAaPmAgInS	začít
i	i	k9	i
životadárný	životadárný	k2eAgInSc1d1	životadárný
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
upadat	upadat	k5eAaPmF	upadat
a	a	k8xC	a
i	i	k9	i
to	ten	k3xDgNnSc1	ten
málo	málo	k1gNnSc1	málo
obyvatel	obyvatel	k1gMnPc2	obyvatel
co	co	k3yRnSc4	co
zde	zde	k6eAd1	zde
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
přežít	přežít	k5eAaPmF	přežít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
hrnout	hrnout	k5eAaImF	hrnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zreformoval	zreformovat	k5eAaPmAgInS	zreformovat
zdejší	zdejší	k2eAgInPc4d1	zdejší
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Novou	nový	k2eAgFnSc4d1	nová
Francii	Francie	k1gFnSc4	Francie
královskou	královský	k2eAgFnSc7d1	královská
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
poslal	poslat	k5eAaPmAgMnS	poslat
tam	tam	k6eAd1	tam
konvoj	konvoj	k1gInSc4	konvoj
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
kolonisty	kolonista	k1gMnPc7	kolonista
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
zásob	zásoba	k1gFnPc2	zásoba
a	a	k8xC	a
pluk	pluk	k1gInSc1	pluk
skvěle	skvěle	k6eAd1	skvěle
vycvičených	vycvičený	k2eAgMnPc2d1	vycvičený
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
Intendant	intendant	k1gMnSc1	intendant
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavou	hlava	k1gFnSc7	hlava
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
mohl	moct	k5eAaImAgInS	moct
konečně	konečně	k6eAd1	konečně
opřít	opřít	k5eAaPmF	opřít
o	o	k7c4	o
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
Intendantem	intendant	k1gMnSc7	intendant
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Jean	Jean	k1gMnSc1	Jean
Talon	talon	k1gInSc1	talon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgMnS	ukázat
jako	jako	k9	jako
schopný	schopný	k2eAgMnSc1d1	schopný
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1667	[number]	k4	1667
zarazil	zarazit	k5eAaPmAgMnS	zarazit
útoky	útok	k1gInPc4	útok
Irokézů	Irokéz	k1gMnPc2	Irokéz
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgInS	nařídit
vybudovat	vybudovat	k5eAaPmF	vybudovat
systém	systém	k1gInSc1	systém
pevností	pevnost	k1gFnPc2	pevnost
zabraňující	zabraňující	k2eAgFnSc2d1	zabraňující
dalším	další	k2eAgMnPc3d1	další
přímým	přímý	k2eAgMnPc3d1	přímý
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
situaci	situace	k1gFnSc4	situace
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
zbavila	zbavit	k5eAaPmAgFnS	zbavit
závislosti	závislost	k1gFnPc4	závislost
na	na	k7c6	na
dodávkách	dodávka	k1gFnPc6	dodávka
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
počty	počet	k1gInPc4	počet
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželský	manželský	k2eAgInSc1d1	manželský
pár	pár	k1gInSc1	pár
který	který	k3yQgInSc4	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
alespoň	alespoň	k9	alespoň
12	[number]	k4	12
dětí	dítě	k1gFnPc2	dítě
obdrží	obdržet	k5eAaPmIp3nS	obdržet
do	do	k7c2	do
trvalého	trvalý	k2eAgNnSc2d1	trvalé
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
40	[number]	k4	40
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
skutečně	skutečně	k6eAd1	skutečně
fungoval	fungovat	k5eAaImAgInS	fungovat
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
Francie	Francie	k1gFnSc1	Francie
začala	začít	k5eAaPmAgFnS	začít
konečně	konečně	k6eAd1	konečně
přicházet	přicházet	k5eAaImF	přicházet
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
Talon	talon	k1gInSc1	talon
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
vlastně	vlastně	k9	vlastně
zdejší	zdejší	k2eAgFnSc1d1	zdejší
kolonie	kolonie	k1gFnSc1	kolonie
stály	stát	k5eAaImAgFnP	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kožešinách	kožešina	k1gFnPc6	kožešina
pohádkově	pohádkově	k6eAd1	pohádkově
zbohatli	zbohatnout	k5eAaPmAgMnP	zbohatnout
také	také	k9	také
dva	dva	k4xCgMnPc1	dva
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
Radisson	Radisson	k1gInSc4	Radisson
a	a	k8xC	a
Medár	Medár	k1gInSc4	Medár
Chouart	Chouarta	k1gFnPc2	Chouarta
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
muži	muž	k1gMnPc1	muž
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
obchodní	obchodní	k2eAgFnSc6d1	obchodní
činnosti	činnost	k1gFnSc6	činnost
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
vnitrozemské	vnitrozemský	k2eAgNnSc4d1	vnitrozemské
území	území	k1gNnSc4	území
a	a	k8xC	a
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
Společnosti	společnost	k1gFnSc2	společnost
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
Talon	talon	k1gInSc1	talon
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
prozkoumaná	prozkoumaný	k2eAgNnPc4d1	prozkoumané
území	území	k1gNnPc4	území
zabral	zabrat	k5eAaPmAgInS	zabrat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
činem	čin	k1gInSc7	čin
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Nové	Nové	k2eAgFnSc3d1	Nové
Francii	Francie	k1gFnSc3	Francie
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
celá	celý	k2eAgFnSc1d1	celá
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
osídlení	osídlený	k2eAgMnPc1d1	osídlený
Francouzi	Francouz	k1gMnPc1	Francouz
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
roznesla	roznést	k5eAaPmAgFnS	roznést
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
řece	řeka	k1gFnSc6	řeka
tekoucí	tekoucí	k2eAgNnPc1d1	tekoucí
z	z	k7c2	z
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Talona	Talon	k1gMnSc4	Talon
skvělá	skvělý	k2eAgFnSc1d1	skvělá
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
totiž	totiž	k9	totiž
řeka	řeka	k1gFnSc1	řeka
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
tou	ten	k3xDgFnSc7	ten
dlouho	dlouho	k6eAd1	dlouho
hledanou	hledaný	k2eAgFnSc7d1	hledaná
cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
Francii	Francie	k1gFnSc3	Francie
další	další	k2eAgFnSc4d1	další
možnost	možnost	k1gFnSc4	možnost
jak	jak	k8xS	jak
dopravit	dopravit	k5eAaPmF	dopravit
kožešiny	kožešina	k1gFnPc4	kožešina
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
tím	ten	k3xDgNnSc7	ten
Francie	Francie	k1gFnSc1	Francie
získá	získat	k5eAaPmIp3nS	získat
území	území	k1gNnSc4	území
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Pověřil	pověřit	k5eAaPmAgMnS	pověřit
tedy	tedy	k9	tedy
Louise	Louis	k1gMnPc4	Louis
Jolieta	Joliet	k2eAgFnSc1d1	Joliet
prozkoumáním	prozkoumání	k1gNnSc7	prozkoumání
a	a	k8xC	a
zmapováním	zmapování	k1gNnSc7	zmapování
této	tento	k3xDgFnSc2	tento
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
v	v	k7c6	v
indiánském	indiánský	k2eAgNnSc6d1	indiánské
nářečí	nářečí	k1gNnSc6	nářečí
Mississippi	Mississippi	k1gFnSc2	Mississippi
(	(	kIx(	(
<g/>
misi	mise	k1gFnSc4	mise
=	=	kIx~	=
velká	velká	k1gFnSc1	velká
<g/>
,	,	kIx,	,
sipi	sipi	k1gNnSc1	sipi
=	=	kIx~	=
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
celkem	celkem	k6eAd1	celkem
šťastně	šťastně	k6eAd1	šťastně
dorazili	dorazit	k5eAaPmAgMnP	dorazit
až	až	k6eAd1	až
přítoku	přítok	k1gInSc2	přítok
Arkansasu	Arkansas	k1gInSc2	Arkansas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
narazili	narazit	k5eAaPmAgMnP	narazit
na	na	k7c4	na
nepřátelské	přátelský	k2eNgInPc4d1	nepřátelský
Kvapany	Kvapan	k1gInPc4	Kvapan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
jsou	být	k5eAaImIp3nP	být
španělské	španělský	k2eAgFnPc1d1	španělská
misionářské	misionářský	k2eAgFnPc1d1	misionářská
osady	osada	k1gFnPc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
španělským	španělský	k2eAgNnSc7d1	španělské
zajetím	zajetí	k1gNnSc7	zajetí
a	a	k8xC	a
nepřátelskými	přátelský	k2eNgInPc7d1	nepřátelský
kmeny	kmen	k1gInPc7	kmen
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Joliet	Joliet	k1gInSc1	Joliet
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Québecu	Québecus	k1gInSc2	Québecus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jolietova	Jolietův	k2eAgFnSc1d1	Jolietův
výprava	výprava	k1gFnSc1	výprava
však	však	k9	však
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
nebyla	být	k5eNaImAgFnS	být
<g/>
,	,	kIx,	,
pořízené	pořízený	k2eAgFnPc1d1	pořízená
mapy	mapa	k1gFnPc1	mapa
a	a	k8xC	a
záznamy	záznam	k1gInPc1	záznam
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
utopily	utopit	k5eAaPmAgFnP	utopit
v	v	k7c6	v
peřejích	peřej	k1gFnPc6	peřej
jen	jen	k9	jen
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
před	před	k7c7	před
Québecem	Québeec	k1gInSc7	Québeec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
část	část	k1gFnSc4	část
podařilo	podařit	k5eAaPmAgNnS	podařit
nakreslit	nakreslit	k5eAaPmF	nakreslit
Jolietovi	Joliet	k1gMnSc3	Joliet
zpaměti	zpaměti	k6eAd1	zpaměti
<g/>
,	,	kIx,	,
na	na	k7c4	na
zabrání	zabrání	k1gNnSc4	zabrání
území	území	k1gNnSc2	území
to	ten	k3xDgNnSc1	ten
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgMnSc1d1	nový
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
jím	on	k3xPp3gMnSc7	on
Robert	Robert	k1gMnSc1	Robert
Cavelier	Cavelier	k1gMnSc1	Cavelier
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
francouzské	francouzský	k2eAgFnSc2d1	francouzská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Cavelier	Cavelier	k1gMnSc1	Cavelier
byl	být	k5eAaImAgMnS	být
původně	původně	k6eAd1	původně
vychovávaný	vychovávaný	k2eAgMnSc1d1	vychovávaný
jako	jako	k8xC	jako
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
kněžství	kněžství	k1gNnSc1	kněžství
se	se	k3xPyFc4	se
však	však	k9	však
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
majetku	majetek	k1gInSc2	majetek
odjel	odjet	k5eAaPmAgMnS	odjet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
do	do	k7c2	do
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
prorazit	prorazit	k5eAaPmF	prorazit
a	a	k8xC	a
otevřít	otevřít	k5eAaPmF	otevřít
si	se	k3xPyFc3	se
prosperující	prosperující	k2eAgInSc4d1	prosperující
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
kožešinami	kožešina	k1gFnPc7	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
obchod	obchod	k1gInSc4	obchod
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1669	[number]	k4	1669
prodal	prodat	k5eAaPmAgInS	prodat
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
zkoumat	zkoumat	k5eAaImF	zkoumat
oblast	oblast	k1gFnSc4	oblast
pod	pod	k7c7	pod
Velkými	velký	k2eAgNnPc7d1	velké
jezery	jezero	k1gNnPc7	jezero
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Montrealu	Montreal	k1gInSc2	Montreal
byl	být	k5eAaImAgInS	být
guvernérem	guvernér	k1gMnSc7	guvernér
Frontenacem	Frontenace	k1gMnSc7	Frontenace
bohatě	bohatě	k6eAd1	bohatě
odměněn	odměněn	k2eAgInSc4d1	odměněn
pozemky	pozemek	k1gInPc4	pozemek
poblíž	poblíž	k7c2	poblíž
dnešního	dnešní	k2eAgInSc2d1	dnešní
Kingstonu	Kingston	k1gInSc2	Kingston
v	v	k7c6	v
Ontariu	Ontario	k1gNnSc6	Ontario
<g/>
,	,	kIx,	,
výhodná	výhodný	k2eAgFnSc1d1	výhodná
poloha	poloha	k1gFnSc1	poloha
těchto	tento	k3xDgInPc2	tento
pozemků	pozemek	k1gInPc2	pozemek
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
snadno	snadno	k6eAd1	snadno
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
zbohatnout	zbohatnout	k5eAaPmF	zbohatnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
ctižádostivému	ctižádostivý	k2eAgMnSc3d1	ctižádostivý
Cavelierovi	Cavelier	k1gMnSc3	Cavelier
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
,	,	kIx,	,
snil	snít	k5eAaImAgMnS	snít
o	o	k7c6	o
pořádné	pořádný	k2eAgFnSc6d1	pořádná
velké	velký	k2eAgFnSc6d1	velká
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
v	v	k7c6	v
dostatečném	dostatečný	k2eAgNnSc6d1	dostatečné
množství	množství	k1gNnSc6	množství
převážela	převážet	k5eAaImAgFnS	převážet
bobří	bobří	k2eAgFnPc4d1	bobří
kožešiny	kožešina	k1gFnPc4	kožešina
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
Velkých	velký	k2eAgNnPc2d1	velké
jezer	jezero	k1gNnPc2	jezero
do	do	k7c2	do
Montrealu	Montreal	k1gInSc2	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
dohromady	dohromady	k6eAd1	dohromady
skupinu	skupina	k1gFnSc4	skupina
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
k	k	k7c3	k
Niagaře	Niagara	k1gFnSc3	Niagara
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1679	[number]	k4	1679
založil	založit	k5eAaPmAgMnS	založit
pevnůstku	pevnůstka	k1gFnSc4	pevnůstka
a	a	k8xC	a
budoucí	budoucí	k2eAgFnSc4d1	budoucí
obchodní	obchodní	k2eAgFnSc4d1	obchodní
stanici	stanice	k1gFnSc4	stanice
Fort	Fort	k?	Fort
Niagara	Niagara	k1gFnSc1	Niagara
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavbu	stavba	k1gFnSc4	stavba
lodi	loď	k1gFnSc2	loď
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
sledovali	sledovat	k5eAaImAgMnP	sledovat
nepřátelští	přátelský	k2eNgMnPc1d1	nepřátelský
Irokezové	Irokez	k1gMnPc1	Irokez
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
párkrát	párkrát	k6eAd1	párkrát
troufli	troufnout	k5eAaPmAgMnP	troufnout
i	i	k9	i
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
potyčky	potyčka	k1gFnPc4	potyčka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
loď	loď	k1gFnSc4	loď
podařilo	podařit	k5eAaPmAgNnS	podařit
rychle	rychle	k6eAd1	rychle
dokončit	dokončit	k5eAaPmF	dokončit
a	a	k8xC	a
spustit	spustit	k5eAaPmF	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
Grifflin	Grifflina	k1gFnPc2	Grifflina
<g/>
.	.	kIx.	.
</s>
<s>
Grifflin	Grifflin	k1gInSc4	Grifflin
svému	svůj	k3xOyFgMnSc3	svůj
majiteli	majitel	k1gMnSc3	majitel
opravdu	opravdu	k6eAd1	opravdu
vynášel	vynášet	k5eAaImAgMnS	vynášet
a	a	k8xC	a
Cavelier	Cavelier	k1gMnSc1	Cavelier
už	už	k6eAd1	už
snil	snít	k5eAaImAgMnS	snít
o	o	k7c6	o
celé	celý	k2eAgFnSc6d1	celá
flotile	flotila	k1gFnSc6	flotila
těchto	tento	k3xDgFnPc2	tento
lodí	loď	k1gFnPc2	loď
brázdící	brázdící	k2eAgInSc1d1	brázdící
pravidelně	pravidelně	k6eAd1	pravidelně
vody	voda	k1gFnSc2	voda
Velkých	velký	k2eAgFnPc2d1	velká
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Cavelier	Cavelier	k1gMnSc1	Cavelier
de	de	k?	de
La	la	k1gNnSc2	la
Salle	Salle	k1gFnSc1	Salle
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
chytil	chytit	k5eAaPmAgMnS	chytit
dalšího	další	k2eAgInSc2d1	další
nápadu	nápad	k1gInSc2	nápad
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
výpravou	výprava	k1gFnSc7	výprava
plout	plout	k5eAaImF	plout
dále	daleko	k6eAd2	daleko
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
Talonův	Talonův	k2eAgInSc4d1	Talonův
sen	sen	k1gInSc4	sen
o	o	k7c6	o
francouzské	francouzský	k2eAgFnSc6d1	francouzská
nadvládě	nadvláda	k1gFnSc6	nadvláda
až	až	k9	až
k	k	k7c3	k
Mexickému	mexický	k2eAgInSc3d1	mexický
zálivu	záliv	k1gInSc3	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
dohromady	dohromady	k6eAd1	dohromady
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1680	[number]	k4	1680
osadu	osada	k1gFnSc4	osada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
člunu	člun	k1gInSc2	člun
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
dovezl	dovézt	k5eAaPmAgInS	dovézt
až	až	k6eAd1	až
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
měl	mít	k5eAaImAgInS	mít
Grifflin	Grifflin	k1gInSc1	Grifflin
prodat	prodat	k5eAaPmF	prodat
kožešiny	kožešina	k1gFnPc4	kožešina
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Michiganského	michiganský	k2eAgNnSc2d1	Michiganské
jezera	jezero	k1gNnSc2	jezero
se	s	k7c7	s
zásobami	zásoba	k1gFnPc7	zásoba
pro	pro	k7c4	pro
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
se	se	k3xPyFc4	se
potopila	potopit	k5eAaPmAgFnS	potopit
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
cenným	cenný	k2eAgInSc7d1	cenný
nákladem	náklad	k1gInSc7	náklad
a	a	k8xC	a
La	la	k1gNnSc7	la
Salle	Salle	k1gFnSc2	Salle
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
mizině	mizina	k1gFnSc6	mizina
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
výmluvnosti	výmluvnost	k1gFnSc3	výmluvnost
sehnal	sehnat	k5eAaPmAgInS	sehnat
dost	dost	k6eAd1	dost
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
potřebné	potřebný	k2eAgFnPc4d1	potřebná
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
vybavení	vybavení	k1gNnSc4	vybavení
pro	pro	k7c4	pro
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
ale	ale	k9	ale
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
vzpoura	vzpoura	k1gFnSc1	vzpoura
<g/>
,	,	kIx,	,
vzbouřenci	vzbouřenec	k1gMnPc1	vzbouřenec
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
La	la	k1gNnSc4	la
Sallova	Sallův	k2eAgMnSc2d1	Sallův
zástupce	zástupce	k1gMnSc2	zástupce
Tontiho	Tonti	k1gMnSc2	Tonti
<g/>
,	,	kIx,	,
zničili	zničit	k5eAaPmAgMnP	zničit
osadu	osada	k1gFnSc4	osada
a	a	k8xC	a
utekli	utéct	k5eAaPmAgMnP	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Cavelier	Cavelier	k1gMnSc1	Cavelier
strávil	strávit	k5eAaPmAgMnS	strávit
poté	poté	k6eAd1	poté
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
hledáním	hledání	k1gNnSc7	hledání
nezvěstného	zvěstný	k2eNgMnSc2d1	nezvěstný
Tontiho	Tonti	k1gMnSc2	Tonti
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
opět	opět	k6eAd1	opět
spřádat	spřádat	k5eAaImF	spřádat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vzít	vzít	k5eAaPmF	vzít
18	[number]	k4	18
indiánských	indiánský	k2eAgMnPc2d1	indiánský
válečníků	válečník	k1gMnPc2	válečník
i	i	k9	i
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
rodinami	rodina	k1gFnPc7	rodina
<g/>
,	,	kIx,	,
než	než	k8xS	než
nespolehlivé	spolehlivý	k2eNgMnPc4d1	nespolehlivý
dobrodruhy	dobrodruh	k1gMnPc4	dobrodruh
<g/>
.	.	kIx.	.
</s>
<s>
Netrpělivě	trpělivě	k6eNd1	trpělivě
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
ještě	ještě	k9	ještě
před	před	k7c7	před
oblevou	obleva	k1gFnSc7	obleva
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
přes	přes	k7c4	přes
zamrzlou	zamrzlý	k2eAgFnSc4d1	zamrzlá
Illinois	Illinois	k1gFnSc4	Illinois
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
kanoí	kanoe	k1gFnPc2	kanoe
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1682	[number]	k4	1682
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
před	před	k7c7	před
dobrodruhy	dobrodruh	k1gMnPc7	dobrodruh
otevřel	otevřít	k5eAaPmAgInS	otevřít
širý	širý	k2eAgInSc1d1	širý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
Salle	Salle	k1gFnSc2	Salle
zde	zde	k6eAd1	zde
vztyčil	vztyčit	k5eAaPmAgMnS	vztyčit
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
jménem	jméno	k1gNnSc7	jméno
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
zabral	zabrat	k5eAaPmAgMnS	zabrat
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
ještě	ještě	k6eAd1	ještě
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Illinois	Illinois	k1gFnSc6	Illinois
pevnůstku	pevnůstka	k1gFnSc4	pevnůstka
Fort	Fort	k?	Fort
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
samotného	samotný	k2eAgMnSc2d1	samotný
krále	král	k1gMnSc2	král
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
zabrání	zabrání	k1gNnSc4	zabrání
území	území	k1gNnSc2	území
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
srdci	srdce	k1gNnSc6	srdce
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
konvojem	konvoj	k1gInSc7	konvoj
mající	mající	k2eAgMnPc1d1	mající
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
založit	založit	k5eAaPmF	založit
na	na	k7c6	na
ústí	ústí	k1gNnSc6	ústí
Mississippi	Mississippi	k1gNnPc2	Mississippi
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
mnoha	mnoho	k4c3	mnoho
bažinatým	bažinatý	k2eAgNnPc3d1	bažinaté
ramenům	rameno	k1gNnPc3	rameno
se	se	k3xPyFc4	se
výpravě	výprava	k1gFnSc3	výprava
nikdy	nikdy	k6eAd1	nikdy
nepodařilo	podařit	k5eNaPmAgNnS	podařit
ústí	ústí	k1gNnSc1	ústí
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
důstojníci	důstojník	k1gMnPc1	důstojník
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
měl	mít	k5eAaImAgInS	mít
La	la	k1gNnSc4	la
Salle	Salle	k1gFnSc2	Salle
mnohé	mnohý	k2eAgInPc1d1	mnohý
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Zoufalý	zoufalý	k2eAgMnSc1d1	zoufalý
Cavelieri	Cavelier	k1gFnPc4	Cavelier
zůstal	zůstat	k5eAaPmAgMnS	zůstat
s	s	k7c7	s
několika	několik	k4yIc7	několik
muži	muž	k1gMnPc7	muž
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
marně	marně	k6eAd1	marně
hledal	hledat	k5eAaImAgInS	hledat
ústí	ústí	k1gNnSc4	ústí
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
vlastními	vlastní	k2eAgInPc7d1	vlastní
lidmi	člověk	k1gMnPc7	člověk
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tím	ten	k3xDgNnSc7	ten
ale	ale	k9	ale
francouzské	francouzský	k2eAgFnSc2d1	francouzská
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
Mississippi	Mississippi	k1gFnSc2	Mississippi
neskončily	skončit	k5eNaPmAgInP	skončit
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
dobrodruzi	dobrodruh	k1gMnPc1	dobrodruh
zakládali	zakládat	k5eAaImAgMnP	zakládat
další	další	k2eAgFnSc4d1	další
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1718	[number]	k4	1718
založen	založit	k5eAaPmNgInS	založit
New	New	k1gFnPc7	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
La	la	k1gNnSc2	la
Sallova	Sallův	k2eAgInSc2d1	Sallův
snu	sen	k1gInSc2	sen
na	na	k7c6	na
ústí	ústí	k1gNnSc6	ústí
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
tím	ten	k3xDgNnSc7	ten
získané	získaný	k2eAgFnSc2d1	získaná
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
pro	pro	k7c4	pro
Novou	nový	k2eAgFnSc4d1	nová
Francii	Francie	k1gFnSc4	Francie
spíše	spíše	k9	spíše
oficiální	oficiální	k2eAgFnSc4d1	oficiální
než	než	k8xS	než
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
državou	država	k1gFnSc7	država
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Quebecu	Quebecus	k1gInSc2	Quebecus
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
zde	zde	k6eAd1	zde
ani	ani	k8xC	ani
nezbyly	zbýt	k5eNaPmAgFnP	zbýt
žádné	žádný	k3yNgFnPc1	žádný
stopy	stopa	k1gFnPc1	stopa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
vážné	vážný	k2eAgInPc1d1	vážný
pokusy	pokus	k1gInPc1	pokus
Anglie	Anglie	k1gFnSc2	Anglie
o	o	k7c6	o
kolonizaci	kolonizace	k1gFnSc6	kolonizace
<g/>
,	,	kIx,	,
podnikl	podniknout	k5eAaPmAgMnS	podniknout
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sir	sir	k1gMnSc1	sir
Walter	Walter	k1gMnSc1	Walter
Raleigh	Raleigh	k1gMnSc1	Raleigh
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Virginia	Virginium	k1gNnPc4	Virginium
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kolonie	kolonie	k1gFnSc1	kolonie
sice	sice	k8xC	sice
roku	rok	k1gInSc2	rok
1607	[number]	k4	1607
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
,	,	kIx,	,
Raleigh	Raleigh	k1gInSc1	Raleigh
však	však	k9	však
později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgMnS	založit
úspěšnější	úspěšný	k2eAgFnSc4d2	úspěšnější
kolonii	kolonie	k1gFnSc4	kolonie
jménem	jméno	k1gNnSc7	jméno
Jamestown	Jamestown	k1gMnSc1	Jamestown
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
náboženských	náboženský	k2eAgInPc2d1	náboženský
důvodů	důvod	k1gInPc2	důvod
odplula	odplout	k5eAaPmAgFnS	odplout
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
Plymouthu	Plymouth	k1gInSc2	Plymouth
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
skupina	skupina	k1gFnSc1	skupina
náboženských	náboženský	k2eAgMnPc2d1	náboženský
nonkonformistů	nonkonformista	k1gMnPc2	nonkonformista
<g/>
,	,	kIx,	,
takzvaných	takzvaný	k2eAgMnPc2d1	takzvaný
Otců	otec	k1gMnPc2	otec
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Přistáli	přistát	k5eAaPmAgMnP	přistát
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mysu	mys	k1gInSc2	mys
Cod	coda	k1gFnPc2	coda
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
malou	malý	k2eAgFnSc4d1	malá
osadu	osada	k1gFnSc4	osada
(	(	kIx(	(
<g/>
Plymouthské	Plymouthský	k2eAgFnPc1d1	Plymouthský
plantáže	plantáž	k1gFnPc1	plantáž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
osady	osada	k1gFnSc2	osada
byly	být	k5eAaImAgFnP	být
bídné	bídný	k2eAgFnPc1d1	bídná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
kolonisté	kolonista	k1gMnPc1	kolonista
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1624	[number]	k4	1624
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
Západoindická	západoindický	k2eAgFnSc1d1	Západoindická
společnost	společnost	k1gFnSc1	společnost
založila	založit	k5eAaPmAgFnS	založit
kolonii	kolonie	k1gFnSc4	kolonie
Nové	Nová	k1gFnSc2	Nová
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
postavila	postavit	k5eAaPmAgFnS	postavit
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Manhattan	Manhattan	k1gInSc1	Manhattan
a	a	k8xC	a
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
jej	on	k3xPp3gInSc4	on
Nový	nový	k2eAgInSc4d1	nový
Amsterodam	Amsterodam	k1gInSc4	Amsterodam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Boj	boj	k1gInSc1	boj
Evropanů	Evropan	k1gMnPc2	Evropan
o	o	k7c4	o
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
a	a	k8xC	a
Španělé	Španěl	k1gMnPc1	Španěl
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
požadovali	požadovat	k5eAaImAgMnP	požadovat
velká	velký	k2eAgNnPc4d1	velké
území	území	k1gNnPc4	území
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
tam	tam	k6eAd1	tam
žilo	žít	k5eAaImAgNnS	žít
mnoho	mnoho	k4c1	mnoho
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
obrátit	obrátit	k5eAaPmF	obrátit
domorodce	domorodec	k1gMnPc4	domorodec
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Kolonisté	kolonista	k1gMnPc1	kolonista
bojovali	bojovat	k5eAaImAgMnP	bojovat
s	s	k7c7	s
domorodými	domorodý	k2eAgMnPc7d1	domorodý
Američany	Američan	k1gMnPc7	Američan
a	a	k8xC	a
s	s	k7c7	s
konkurenčními	konkurenční	k2eAgMnPc7d1	konkurenční
kolonisty	kolonista	k1gMnPc7	kolonista
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
půdy	půda	k1gFnSc2	půda
<g/>
;	;	kIx,	;
postupně	postupně	k6eAd1	postupně
je	on	k3xPp3gMnPc4	on
vytlačovali	vytlačovat	k5eAaImAgMnP	vytlačovat
v	v	k7c4	v
USA	USA	kA	USA
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Španělská	španělský	k2eAgFnSc1d1	španělská
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předkolumbovské	předkolumbovský	k2eAgNnSc4d1	předkolumbovské
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Oblasti	oblast	k1gFnPc4	oblast
jihu	jih	k1gInSc2	jih
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
oblasti	oblast	k1gFnPc4	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
osídleny	osídlit	k5eAaPmNgFnP	osídlit
člověkem	člověk	k1gMnSc7	člověk
podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
před	před	k7c7	před
30	[number]	k4	30
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
osídlení	osídlení	k1gNnSc6	osídlení
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
kultura	kultura	k1gFnSc1	kultura
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
kultuře	kultura	k1gFnSc3	kultura
lovců	lovec	k1gMnPc2	lovec
a	a	k8xC	a
sběračů	sběrač	k1gMnPc2	sběrač
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
let	léto	k1gNnPc2	léto
5000	[number]	k4	5000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
protoneolit	protoneolit	k5eAaImF	protoneolit
a	a	k8xC	a
následně	následně	k6eAd1	následně
neolitická	neolitický	k2eAgFnSc1d1	neolitická
revoluce	revoluce	k1gFnSc1	revoluce
s	s	k7c7	s
hlubokými	hluboký	k2eAgInPc7d1	hluboký
důsledky	důsledek	k1gInPc7	důsledek
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
těchto	tento	k3xDgFnPc2	tento
lidských	lidský	k2eAgFnPc2d1	lidská
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2500	[number]	k4	2500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nejstarší	starý	k2eAgFnSc1d3	nejstarší
mexická	mexický	k2eAgFnSc1d1	mexická
keramika	keramika	k1gFnSc1	keramika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
první	první	k4xOgFnSc4	první
mesoamerickou	mesoamerický	k2eAgFnSc4d1	mesoamerický
civilizaci	civilizace	k1gFnSc4	civilizace
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
olmécká	olmécký	k2eAgFnSc1d1	olmécká
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Olmékové	Olméek	k1gMnPc1	Olméek
obývali	obývat	k5eAaImAgMnP	obývat
především	především	k6eAd1	především
území	území	k1gNnSc4	území
kolem	kolem	k7c2	kolem
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Veracruz	Veracruza	k1gFnPc2	Veracruza
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Tabasco	Tabasco	k6eAd1	Tabasco
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
budovali	budovat	k5eAaImAgMnP	budovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
sakrální	sakrální	k2eAgFnPc4d1	sakrální
i	i	k8xC	i
profánní	profánní	k2eAgFnPc4d1	profánní
stavby	stavba	k1gFnPc4	stavba
a	a	k8xC	a
vynikali	vynikat	k5eAaImAgMnP	vynikat
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
monumentálních	monumentální	k2eAgFnPc2d1	monumentální
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
vliv	vliv	k1gInSc1	vliv
Olméků	Olméek	k1gMnPc2	Olméek
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dlouho	dlouho	k6eAd1	dlouho
po	po	k7c6	po
rozkvětu	rozkvět	k1gInSc6	rozkvět
jejich	jejich	k3xOp3gFnSc2	jejich
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
například	například	k6eAd1	například
další	další	k2eAgFnSc4d1	další
severoamerickou	severoamerický	k2eAgFnSc4d1	severoamerická
civilizaci	civilizace	k1gFnSc4	civilizace
Mixtéků	Mixtéek	k1gMnPc2	Mixtéek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
kolem	kolo	k1gNnSc7	kolo
roku	rok	k1gInSc2	rok
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
postavili	postavit	k5eAaPmAgMnP	postavit
význačné	význačný	k2eAgFnPc4d1	význačná
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
kultovní	kultovní	k2eAgNnSc1d1	kultovní
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
město	město	k1gNnSc1	město
Monte	Mont	k1gInSc5	Mont
Albán	Albán	k1gInSc1	Albán
s	s	k7c7	s
komplexem	komplex	k1gInSc7	komplex
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
chrámových	chrámový	k2eAgFnPc2d1	chrámová
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
obydlí	obydlí	k1gNnPc2	obydlí
pro	pro	k7c4	pro
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sehrávali	sehrávat	k5eAaImAgMnP	sehrávat
v	v	k7c6	v
životě	život	k1gInSc6	život
mixtécké	mixtécký	k2eAgFnSc2d1	mixtécká
společnosti	společnost	k1gFnSc2	společnost
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Mixtékové	Mixtékové	k2eAgMnSc1d1	Mixtékové
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
vynikali	vynikat	k5eAaImAgMnP	vynikat
výrobou	výroba	k1gFnSc7	výroba
keramiky	keramika	k1gFnPc4	keramika
a	a	k8xC	a
především	především	k6eAd1	především
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
spravováním	spravování	k1gNnSc7	spravování
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
dobří	dobrý	k2eAgMnPc1d1	dobrý
zemědělci	zemědělec	k1gMnPc1	zemědělec
a	a	k8xC	a
řemeslníci	řemeslník	k1gMnPc1	řemeslník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
město	město	k1gNnSc1	město
Teotihuacán	Teotihuacán	k2eAgMnSc1d1	Teotihuacán
<g/>
,	,	kIx,	,
<g/>
které	který	k3yQgNnSc1	který
plnilo	plnit	k5eAaImAgNnS	plnit
funkce	funkce	k1gFnPc4	funkce
náboženského	náboženský	k2eAgMnSc2d1	náboženský
a	a	k8xC	a
kulturního	kulturní	k2eAgNnSc2d1	kulturní
centra	centrum	k1gNnSc2	centrum
pozdní	pozdní	k2eAgFnSc2d1	pozdní
olmécké	olmécký	k2eAgFnSc2d1	olmécká
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgNnPc3d3	nejvýznamnější
městským	městský	k2eAgNnPc3d1	Městské
osídlením	osídlení	k1gNnPc3	osídlení
v	v	k7c6	v
Amerikách	Amerika	k1gFnPc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Teotihuacán	Teotihuacán	k2eAgMnSc1d1	Teotihuacán
měl	mít	k5eAaImAgMnS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
asi	asi	k9	asi
25	[number]	k4	25
km2	km2	k4	km2
a	a	k8xC	a
mezi	mezi	k7c4	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
jeho	jeho	k3xOp3gMnPc2	jeho
obyvatel	obyvatel	k1gMnPc2	obyvatel
odhadován	odhadován	k2eAgInSc4d1	odhadován
na	na	k7c4	na
100	[number]	k4	100
až	až	k9	až
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Srdcem	srdce	k1gNnSc7	srdce
města	město	k1gNnSc2	město
byl	být	k5eAaImAgMnS	být
komplex	komplex	k1gInSc4	komplex
několika	několik	k4yIc2	několik
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
byly	být	k5eAaImAgFnP	být
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
pyramidy	pyramida	k1gFnPc1	pyramida
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Quentalcoatlova	Quentalcoatlův	k2eAgFnSc1d1	Quentalcoatlův
pyramida	pyramida	k1gFnSc1	pyramida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
poloostrova	poloostrov	k1gInSc2	poloostrov
Yucatánu	Yucatán	k2eAgFnSc4d1	Yucatán
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Guatemaly	Guatemala	k1gFnSc2	Guatemala
<g/>
,	,	kIx,	,
Salvadoru	Salvador	k1gInSc2	Salvador
<g/>
,	,	kIx,	,
Belize	Belize	k1gFnSc2	Belize
a	a	k8xC	a
Hondurasu	Honduras	k1gInSc2	Honduras
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
objevila	objevit	k5eAaPmAgFnS	objevit
další	další	k2eAgFnPc4d1	další
civilizace	civilizace	k1gFnPc4	civilizace
Mayů	May	k1gMnPc2	May
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
existovala	existovat	k5eAaImAgFnS	existovat
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
historických	historický	k2eAgFnPc6d1	historická
fázích	fáze	k1gFnPc6	fáze
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
<g/>
.	.	kIx.	.
</s>
<s>
Civilizace	civilizace	k1gFnSc1	civilizace
Mayů	May	k1gMnPc2	May
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejvyspělejší	vyspělý	k2eAgFnSc4d3	nejvyspělejší
civilizaci	civilizace	k1gFnSc4	civilizace
předkolumbovské	předkolumbovský	k2eAgFnSc2d1	předkolumbovská
Ameriky	Amerika	k1gFnSc2	Amerika
s	s	k7c7	s
vyspělou	vyspělý	k2eAgFnSc7d1	vyspělá
úrovní	úroveň	k1gFnSc7	úroveň
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
kalendářem	kalendář	k1gInSc7	kalendář
a	a	k8xC	a
logosylabickým	logosylabický	k2eAgNnSc7d1	logosylabický
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
procházela	procházet	k5eAaImAgFnS	procházet
mayská	mayský	k2eAgFnSc1d1	mayská
civilizace	civilizace	k1gFnSc1	civilizace
úpadkem	úpadek	k1gInSc7	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
Toltéků	Toltéek	k1gMnPc2	Toltéek
vedl	vést	k5eAaImAgInS	vést
ale	ale	k9	ale
k	k	k7c3	k
následné	následný	k2eAgFnSc3d1	následná
renesanci	renesance	k1gFnSc3	renesance
a	a	k8xC	a
militarizaci	militarizace	k1gFnSc3	militarizace
mayské	mayský	k2eAgFnSc2d1	mayská
společnosti	společnost	k1gFnSc2	společnost
během	během	k7c2	během
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mexika	Mexiko	k1gNnSc2	Mexiko
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Aztékové	Azték	k1gMnPc1	Azték
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mezi	mezi	k7c7	mezi
rokem	rok	k1gInSc7	rok
1324-1325	[number]	k4	1324-1325
založili	založit	k5eAaPmAgMnP	založit
město	město	k1gNnSc4	město
Tenochtitlán	Tenochtitlán	k2eAgInSc4d1	Tenochtitlán
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
jezera	jezero	k1gNnSc2	jezero
Texcoco	Texcoco	k1gMnSc1	Texcoco
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
expanzi	expanze	k1gFnSc4	expanze
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nebyly	být	k5eNaImAgFnP	být
pro	pro	k7c4	pro
Aztéky	Azték	k1gMnPc4	Azték
příliš	příliš	k6eAd1	příliš
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
se	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
dalšími	další	k2eAgNnPc7d1	další
městy	město	k1gNnPc7	město
a	a	k8xC	a
rozšiřovali	rozšiřovat	k5eAaImAgMnP	rozšiřovat
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
počátku	počátek	k1gInSc2	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
získal	získat	k5eAaPmAgMnS	získat
Tenochtitlán	Tenochtitlán	k2eAgMnSc1d1	Tenochtitlán
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
trojspolku	trojspolek	k1gInSc2	trojspolek
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
příjemcem	příjemce	k1gMnSc7	příjemce
tributu	tribut	k1gInSc2	tribut
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
vzácného	vzácný	k2eAgNnSc2d1	vzácné
peří	peří	k1gNnSc2	peří
ptáka	pták	k1gMnSc2	pták
quetzala	quetzat	k5eAaPmAgFnS	quetzat
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
že	že	k8xS	že
by	by	kYmCp3nS	by
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
teritoriálně	teritoriálně	k6eAd1	teritoriálně
kontrolovaný	kontrolovaný	k2eAgInSc1d1	kontrolovaný
a	a	k8xC	a
centralizovaný	centralizovaný	k2eAgInSc1d1	centralizovaný
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
Aztékové	Azték	k1gMnPc1	Azték
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
a	a	k8xC	a
dostávali	dostávat	k5eAaImAgMnP	dostávat
tribut	tribut	k1gInSc4	tribut
od	od	k7c2	od
asi	asi	k9	asi
350	[number]	k4	350
různých	různý	k2eAgInPc2d1	různý
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
a	a	k8xC	a
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Aztécká	aztécký	k2eAgFnSc1d1	aztécká
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
důrazem	důraz	k1gInSc7	důraz
na	na	k7c6	na
roli	role	k1gFnSc6	role
náboženství	náboženství	k1gNnPc2	náboženství
(	(	kIx(	(
<g/>
a	a	k8xC	a
kněží	kněz	k1gMnPc1	kněz
<g/>
)	)	kIx)	)
a	a	k8xC	a
silnou	silný	k2eAgFnSc7d1	silná
orientací	orientace	k1gFnSc7	orientace
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
expanzi	expanze	k1gFnSc4	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
jevy	jev	k1gInPc1	jev
byly	být	k5eAaImAgInP	být
společně	společně	k6eAd1	společně
spojené	spojený	k2eAgInPc1d1	spojený
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
významným	významný	k2eAgMnSc7d1	významný
aztéckým	aztécký	k2eAgMnSc7d1	aztécký
bohem	bůh	k1gMnSc7	bůh
byl	být	k5eAaImAgMnS	být
Huitzilopochtli	Huitzilopochtle	k1gFnSc4	Huitzilopochtle
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
lidské	lidský	k2eAgFnPc4d1	lidská
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
při	při	k7c6	při
vysvěcení	vysvěcení	k1gNnSc6	vysvěcení
jeho	jeho	k3xOp3gMnSc1	jeho
chrámu	chrámat	k5eAaImIp1nS	chrámat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1486	[number]	k4	1486
obětováno	obětovat	k5eAaBmNgNnS	obětovat
na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
potřeba	potřeba	k1gFnSc1	potřeba
lidských	lidský	k2eAgFnPc2d1	lidská
obětí	oběť	k1gFnPc2	oběť
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
motorů	motor	k1gInPc2	motor
aztécké	aztécký	k2eAgFnSc2d1	aztécká
expanze	expanze	k1gFnSc2	expanze
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
španělské	španělský	k2eAgFnSc2d1	španělská
conquisty	conquista	k1gFnSc2	conquista
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1492	[number]	k4	1492
se	se	k3xPyFc4	se
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
zaměřovali	zaměřovat	k5eAaImAgMnP	zaměřovat
na	na	k7c4	na
objevování	objevování	k1gNnSc4	objevování
a	a	k8xC	a
kontrolu	kontrola	k1gFnSc4	kontrola
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Karibského	karibský	k2eAgNnSc2d1	Karibské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
osad	osada	k1gFnPc2	osada
na	na	k7c6	na
Hispaniole	Hispaniola	k1gFnSc6	Hispaniola
a	a	k8xC	a
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
se	s	k7c7	s
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
vypravovaly	vypravovat	k5eAaImAgInP	vypravovat
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1493	[number]	k4	1493
četné	četný	k2eAgFnPc1d1	četná
expedice	expedice	k1gFnPc1	expedice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
postupně	postupně	k6eAd1	postupně
mapovat	mapovat	k5eAaImF	mapovat
pobřežní	pobřežní	k2eAgFnPc4d1	pobřežní
oblasti	oblast	k1gFnPc4	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Floridy	Florida	k1gFnSc2	Florida
<g/>
,	,	kIx,	,
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
vyslal	vyslat	k5eAaPmAgMnS	vyslat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
guvernér	guvernér	k1gMnSc1	guvernér
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
Diego	Diego	k1gMnSc1	Diego
Velázquez	Velázquez	k1gMnSc1	Velázquez
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
poloostrova	poloostrov	k1gInSc2	poloostrov
Yucatánu	Yucatán	k2eAgFnSc4d1	Yucatán
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
vylodění	vylodění	k1gNnSc6	vylodění
a	a	k8xC	a
podniknutí	podniknutí	k1gNnSc6	podniknutí
expediční	expediční	k2eAgFnSc2d1	expediční
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
ale	ale	k8xC	ale
narážely	narážet	k5eAaPmAgFnP	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
místních	místní	k2eAgMnPc2d1	místní
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
další	další	k2eAgFnSc1d1	další
výprava	výprava	k1gFnSc1	výprava
vyslaná	vyslaný	k2eAgFnSc1d1	vyslaná
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Velázquezem	Velázquez	k1gInSc7	Velázquez
nebyla	být	k5eNaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
zlatém	zlatý	k1gInSc6	zlatý
bohatství	bohatství	k1gNnSc2	bohatství
místního	místní	k2eAgNnSc2d1	místní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
podpořily	podpořit	k5eAaPmAgFnP	podpořit
vznik	vznik	k1gInSc4	vznik
další	další	k2eAgFnSc2d1	další
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
Velázquez	Velázquez	k1gMnSc1	Velázquez
pověřil	pověřit	k5eAaPmAgMnS	pověřit
vedením	vedení	k1gNnSc7	vedení
svého	svůj	k3xOyFgMnSc2	svůj
osobního	osobní	k2eAgMnSc2d1	osobní
tajemníka	tajemník	k1gMnSc2	tajemník
Hernána	Hernán	k2eAgFnSc1d1	Hernána
Cortése	Cortése	k1gFnSc1	Cortése
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
s	s	k7c7	s
výpravou	výprava	k1gFnSc7	výprava
asi	asi	k9	asi
500	[number]	k4	500
mužů	muž	k1gMnPc2	muž
zamířil	zamířit	k5eAaPmAgMnS	zamířit
na	na	k7c4	na
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1519	[number]	k4	1519
posádka	posádka	k1gFnSc1	posádka
zakotvila	zakotvit	k5eAaPmAgFnS	zakotvit
na	na	k7c6	na
vhodném	vhodný	k2eAgNnSc6d1	vhodné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
zde	zde	k6eAd1	zde
Cortés	Cortés	k1gInSc1	Cortés
první	první	k4xOgNnSc4	první
evropské	evropský	k2eAgNnSc4d1	Evropské
město	město	k1gNnSc4	město
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentě	kontinent	k1gInSc6	kontinent
Veracruz	Veracruz	k1gInSc1	Veracruz
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
Cortés	Cortés	k1gInSc4	Cortés
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
vydal	vydat	k5eAaPmAgMnS	vydat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
centru	centrum	k1gNnSc3	centrum
Aztécké	aztécký	k2eAgFnSc2d1	aztécká
říše	říš	k1gFnSc2	říš
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aztécký	aztécký	k2eAgMnSc1d1	aztécký
panovník	panovník	k1gMnSc1	panovník
Montezuma	Montezumum	k1gNnSc2	Montezumum
II	II	kA	II
<g/>
.	.	kIx.	.
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
novým	nový	k2eAgMnPc3d1	nový
vetřelcům	vetřelec	k1gMnPc3	vetřelec
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Aztékové	Azték	k1gMnPc1	Azték
měli	mít	k5eAaImAgMnP	mít
nad	nad	k7c4	nad
Španěly	Španěly	k1gInPc4	Španěly
početní	početní	k2eAgFnSc4d1	početní
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
technologická	technologický	k2eAgFnSc1d1	technologická
převaha	převaha	k1gFnSc1	převaha
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
vedle	vedle	k7c2	vedle
koní	kůň	k1gMnPc2	kůň
také	také	k9	také
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Aztékům	Azték	k1gMnPc3	Azték
nebylo	být	k5eNaImAgNnS	být
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
jasné	jasný	k2eAgNnSc1d1	jasné
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
příchod	příchod	k1gInSc4	příchod
bohů	bůh	k1gMnPc2	bůh
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gMnPc2	jejich
poslů	posel	k1gMnPc2	posel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Cortés	Cortés	k1gInSc1	Cortés
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1519	[number]	k4	1519
do	do	k7c2	do
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
dorazil	dorazit	k5eAaPmAgMnS	dorazit
<g/>
,	,	kIx,	,
uvítal	uvítat	k5eAaPmAgMnS	uvítat
ho	on	k3xPp3gMnSc4	on
Montezuma	Montezum	k1gMnSc4	Montezum
přátelsky	přátelsky	k6eAd1	přátelsky
a	a	k8xC	a
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
ho	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Cortés	Cortés	k6eAd1	Cortés
brzy	brzy	k6eAd1	brzy
využil	využít	k5eAaPmAgMnS	využít
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
Montezumu	Montezum	k1gInSc2	Montezum
zajal	zajmout	k5eAaPmAgInS	zajmout
a	a	k8xC	a
snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
získat	získat	k5eAaPmF	získat
výhody	výhoda	k1gFnPc4	výhoda
vyplývající	vyplývající	k2eAgFnPc4d1	vyplývající
z	z	k7c2	z
držení	držení	k1gNnSc2	držení
vzácného	vzácný	k2eAgNnSc2d1	vzácné
rukojmí	rukojmí	k1gNnSc2	rukojmí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
už	už	k6eAd1	už
nejenom	nejenom	k6eAd1	nejenom
otřesená	otřesený	k2eAgFnSc1d1	otřesená
pozice	pozice	k1gFnSc1	pozice
Cortése	Cortése	k1gFnSc2	Cortése
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
místnímu	místní	k2eAgNnSc3d1	místní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nechtělo	chtít	k5eNaImAgNnS	chtít
podvolit	podvolit	k5eAaPmF	podvolit
jeho	jeho	k3xOp3gFnSc4	jeho
požadavkům	požadavek	k1gInPc3	požadavek
na	na	k7c4	na
uznání	uznání	k1gNnSc4	uznání
suverenity	suverenita	k1gFnSc2	suverenita
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
ke	k	k7c3	k
guvernérovi	guvernér	k1gMnSc3	guvernér
Velázquezovi	Velázquez	k1gMnSc3	Velázquez
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
proti	proti	k7c3	proti
Cortésovi	Cortés	k1gMnSc3	Cortés
poslal	poslat	k5eAaPmAgMnS	poslat
trestnou	trestný	k2eAgFnSc4d1	trestná
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
situaci	situace	k1gFnSc6	situace
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
Tenochtitláně	Tenochtitlán	k1gInSc6	Tenochtitlán
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
Cortés	Cortés	k1gInSc1	Cortés
musel	muset	k5eAaImAgInS	muset
město	město	k1gNnSc4	město
za	za	k7c2	za
velkých	velký	k2eAgFnPc2d1	velká
ztrát	ztráta	k1gFnPc2	ztráta
(	(	kIx(	(
<g/>
Noche	Nochus	k1gMnSc5	Nochus
Triste	Trist	k1gMnSc5	Trist
<g/>
)	)	kIx)	)
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Cortésovi	Cortés	k1gMnSc3	Cortés
podařilo	podařit	k5eAaPmAgNnS	podařit
upevnit	upevnit	k5eAaPmF	upevnit
si	se	k3xPyFc3	se
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
nové	nový	k2eAgFnPc4d1	nová
posily	posila	k1gFnPc4	posila
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1521	[number]	k4	1521
ho	on	k3xPp3gInSc4	on
dobyl	dobýt	k5eAaPmAgInS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
poslední	poslední	k2eAgMnPc1d1	poslední
aztéčtí	aztécký	k2eAgMnPc1d1	aztécký
panovníci	panovník	k1gMnPc1	panovník
a	a	k8xC	a
nástupci	nástupce	k1gMnPc1	nástupce
Montezumy	Montezum	k1gInPc4	Montezum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1519	[number]	k4	1519
<g/>
,	,	kIx,	,
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
města	město	k1gNnSc2	město
Cortés	Cortésa	k1gFnPc2	Cortésa
založil	založit	k5eAaPmAgMnS	založit
nové	nový	k2eAgNnSc4d1	nové
město	město	k1gNnSc4	město
a	a	k8xC	a
nazval	nazvat	k5eAaBmAgMnS	nazvat
ho	on	k3xPp3gNnSc4	on
Mexikem	Mexiko	k1gNnSc7	Mexiko
(	(	kIx(	(
<g/>
Mexico	Mexico	k6eAd1	Mexico
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
španělští	španělský	k2eAgMnPc1d1	španělský
conquistadoři	conquistador	k1gMnPc1	conquistador
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
území	území	k1gNnSc2	území
sousedících	sousedící	k2eAgInPc2d1	sousedící
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
Aztécké	aztécký	k2eAgFnSc2d1	aztécká
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
severu	sever	k1gInSc6	sever
i	i	k8xC	i
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
objevování	objevování	k1gNnSc4	objevování
pobřeží	pobřeží	k1gNnSc4	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
oblastí	oblast	k1gFnPc2	oblast
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
představoval	představovat	k5eAaImAgInS	představovat
nejvýraznější	výrazný	k2eAgFnSc4d3	nejvýraznější
osobnost	osobnost	k1gFnSc4	osobnost
španělské	španělský	k2eAgFnSc2d1	španělská
conquisty	conquista	k1gFnSc2	conquista
Hernán	Hernán	k2eAgInSc1d1	Hernán
Cortés	Cortés	k1gInSc1	Cortés
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc1	jeho
zájmy	zájem	k1gInPc1	zájem
se	se	k3xPyFc4	se
dostávaly	dostávat	k5eAaImAgInP	dostávat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
se	s	k7c7	s
zájmy	zájem	k1gInPc7	zájem
Koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1535	[number]	k4	1535
představoval	představovat	k5eAaImAgMnS	představovat
místokrál	místokrál	k1gMnSc1	místokrál
Nového	Nového	k2eAgNnSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
Antonio	Antonio	k1gMnSc1	Antonio
de	de	k?	de
Mendoza	Mendoza	k1gFnSc1	Mendoza
<g/>
.	.	kIx.	.
</s>
<s>
Mendoza	Mendoz	k1gMnSc4	Mendoz
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
Cortésovým	Cortésův	k2eAgMnSc7d1	Cortésův
nadřízeným	nadřízený	k1gMnSc7	nadřízený
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
koloniální	koloniální	k2eAgFnSc2d1	koloniální
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
chránila	chránit	k5eAaImAgFnS	chránit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
zájmy	zájem	k1gInPc4	zájem
Koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
daně	daň	k1gFnPc1	daň
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
indiánské	indiánský	k2eAgFnSc2d1	indiánská
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zájmy	zájem	k1gInPc1	zájem
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
soukromými	soukromý	k2eAgInPc7d1	soukromý
zájmy	zájem	k1gInPc7	zájem
prvních	první	k4xOgInPc2	první
conquistadorů	conquistador	k1gMnPc2	conquistador
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Cortés	Cortés	k1gInSc1	Cortés
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1540	[number]	k4	1540
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
Nové	Nové	k2eAgNnSc4d1	Nové
Španělsko	Španělsko	k1gNnSc4	Španělsko
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1547	[number]	k4	1547
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Koruna	koruna	k1gFnSc1	koruna
projevovala	projevovat	k5eAaImAgFnS	projevovat
o	o	k7c4	o
efektivnější	efektivní	k2eAgFnSc4d2	efektivnější
kontrolu	kontrola	k1gFnSc4	kontrola
nově	nově	k6eAd1	nově
dobytých	dobytý	k2eAgFnPc2d1	dobytá
oblastí	oblast	k1gFnPc2	oblast
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
conquistadorů	conquistador	k1gMnPc2	conquistador
postupně	postupně	k6eAd1	postupně
nahrazována	nahrazovat	k5eAaImNgFnS	nahrazovat
novým	nový	k2eAgInSc7d1	nový
systémem	systém	k1gInSc7	systém
koloniální	koloniální	k2eAgFnSc2d1	koloniální
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
<g/>
,	,	kIx,	,
generálních	generální	k2eAgInPc2d1	generální
kapitanátů	kapitanát	k1gInPc2	kapitanát
a	a	k8xC	a
audiencií	audiencie	k1gFnPc2	audiencie
<g/>
.	.	kIx.	.
</s>
<s>
Polofeudální	polofeudální	k2eAgInSc1d1	polofeudální
systém	systém	k1gInSc1	systém
encomiendy	encomienda	k1gFnSc2	encomienda
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
repartiementem	repartiement	k1gInSc7	repartiement
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
haciendou	hacienda	k1gFnSc7	hacienda
<g/>
,	,	kIx,	,
orientovanou	orientovaný	k2eAgFnSc7d1	orientovaná
na	na	k7c6	na
produkci	produkce	k1gFnSc6	produkce
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
významnou	významný	k2eAgFnSc7d1	významná
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
činnosti	činnost	k1gFnSc2	činnost
Koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
činnost	činnost	k1gFnSc4	činnost
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
evangelizace	evangelizace	k1gFnSc2	evangelizace
indiánského	indiánský	k2eAgNnSc2d1	indiánské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
importu	import	k1gInSc2	import
evropské	evropský	k2eAgFnSc2d1	Evropská
kultury	kultura	k1gFnSc2	kultura
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koloniální	koloniální	k2eAgNnSc4d1	koloniální
období	období	k1gNnSc4	období
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
koloniální	koloniální	k2eAgFnSc2d1	koloniální
správy	správa	k1gFnSc2	správa
i	i	k8xC	i
formy	forma	k1gFnSc2	forma
společenských	společenský	k2eAgFnPc2d1	společenská
<g/>
,	,	kIx,	,
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
i	i	k8xC	i
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
severoamerického	severoamerický	k2eAgNnSc2d1	severoamerické
panství	panství	k1gNnSc2	panství
Španělska	Španělsko	k1gNnSc2	Španělsko
nelišil	lišit	k5eNaImAgMnS	lišit
od	od	k7c2	od
systému	systém	k1gInSc2	systém
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Severoamerické	severoamerický	k2eAgFnPc1d1	severoamerická
oblasti	oblast	k1gFnPc1	oblast
byly	být	k5eAaImAgFnP	být
organizovány	organizovat	k5eAaBmNgFnP	organizovat
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
s	s	k7c7	s
názvem	název	k1gInSc7	název
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
1535	[number]	k4	1535
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
jehož	jehož	k3xOyRp3gFnPc4	jehož
pravomoci	pravomoc	k1gFnPc4	pravomoc
ale	ale	k8xC	ale
spadal	spadat	k5eAaPmAgInS	spadat
také	také	k9	také
generální	generální	k2eAgInSc1d1	generální
kapitanát	kapitanát	k1gInSc1	kapitanát
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
panství	panství	k1gNnSc1	panství
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
teritoriálně	teritoriálně	k6eAd1	teritoriálně
ustáleným	ustálený	k2eAgInSc7d1	ustálený
jevem	jev	k1gInSc7	jev
nebo	nebo	k8xC	nebo
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Koloniální	koloniální	k2eAgFnSc1d1	koloniální
expanze	expanze	k1gFnSc1	expanze
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
16	[number]	k4	16
<g/>
.	.	kIx.	.
i	i	k8xC	i
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
především	především	k9	především
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
dnešních	dnešní	k2eAgInPc2d1	dnešní
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
španělské	španělský	k2eAgFnSc2d1	španělská
kolonizace	kolonizace	k1gFnSc2	kolonizace
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
třemi	tři	k4xCgMnPc7	tři
hlavními	hlavní	k2eAgMnPc7d1	hlavní
činiteli	činitel	k1gMnPc7	činitel
koloniální	koloniální	k2eAgFnSc2d1	koloniální
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
byly	být	k5eAaImAgFnP	být
presidios	presidios	k1gMnSc1	presidios
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vojenská	vojenský	k2eAgNnPc1d1	vojenské
města	město	k1gNnPc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pueblos	pueblos	k1gInSc1	pueblos
(	(	kIx(	(
<g/>
civilní	civilní	k2eAgNnPc1d1	civilní
města	město	k1gNnPc1	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
misie	misie	k1gFnSc1	misie
<g/>
.	.	kIx.	.
<g/>
Centrem	centr	k1gInSc7	centr
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
části	část	k1gFnSc2	část
španělských	španělský	k2eAgFnPc2d1	španělská
kolonií	kolonie	k1gFnPc2	kolonie
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Mexika	Mexiko	k1gNnSc2	Mexiko
(	(	kIx(	(
<g/>
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
Mexico	Mexico	k1gNnSc1	Mexico
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlily	sídlit	k5eAaImAgFnP	sídlit
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
koloniální	koloniální	k2eAgFnPc4d1	koloniální
instituce	instituce	k1gFnPc4	instituce
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
místokrálovský	místokrálovský	k2eAgInSc1d1	místokrálovský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
Královská	královský	k2eAgFnSc1d1	královská
a	a	k8xC	a
pontifikální	pontifikální	k2eAgFnSc1d1	pontifikální
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
metropolitní	metropolitní	k2eAgNnSc1d1	metropolitní
biskupství	biskupství	k1gNnSc1	biskupství
(	(	kIx(	(
<g/>
1530	[number]	k4	1530
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
(	(	kIx(	(
<g/>
1546	[number]	k4	1546
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Španělska	Španělsko	k1gNnSc2	Španělsko
podléhala	podléhat	k5eAaImAgFnS	podléhat
koloniální	koloniální	k2eAgFnSc1d1	koloniální
správa	správa	k1gFnSc1	správa
centrálním	centrální	k2eAgInPc3d1	centrální
úřadům	úřad	k1gInPc3	úřad
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
(	(	kIx(	(
<g/>
Kontratační	Kontratační	k2eAgInSc1d1	Kontratační
úřad	úřad	k1gInSc1	úřad
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
Indie	Indie	k1gFnPc4	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koloniální	koloniální	k2eAgFnSc4d1	koloniální
společnost	společnost	k1gFnSc4	společnost
charakterizovalo	charakterizovat	k5eAaBmAgNnS	charakterizovat
rasově	rasově	k6eAd1	rasově
založené	založený	k2eAgNnSc1d1	založené
a	a	k8xC	a
segregační	segregační	k2eAgNnSc1d1	segregační
rozdělení	rozdělení	k1gNnSc1	rozdělení
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
republiku	republika	k1gFnSc4	republika
Španělů	Španěl	k1gMnPc2	Španěl
a	a	k8xC	a
republiku	republika	k1gFnSc4	republika
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
vnitřně	vnitřně	k6eAd1	vnitřně
diferencovány	diferencován	k2eAgFnPc1d1	diferencována
a	a	k8xC	a
platily	platit	k5eAaImAgFnP	platit
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
aspektech	aspekt	k1gInPc6	aspekt
ale	ale	k9	ale
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
republika	republika	k1gFnSc1	republika
indiánů	indián	k1gMnPc2	indián
byla	být	k5eAaImAgFnS	být
podřízená	podřízená	k1gFnSc1	podřízená
republice	republika	k1gFnSc3	republika
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
fáze	fáze	k1gFnSc1	fáze
conquisty	conquista	k1gFnSc2	conquista
brzy	brzy	k6eAd1	brzy
přešla	přejít	k5eAaPmAgFnS	přejít
pod	pod	k7c4	pod
větší	veliký	k2eAgFnSc4d2	veliký
kontrolu	kontrola	k1gFnSc4	kontrola
Koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
stanovit	stanovit	k5eAaPmF	stanovit
pevná	pevný	k2eAgNnPc4d1	pevné
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
uspořádání	uspořádání	k1gNnSc4	uspořádání
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
tzv.	tzv.	kA	tzv.
Novými	nový	k2eAgInPc7d1	nový
zákony	zákon	k1gInPc7	zákon
(	(	kIx(	(
<g/>
Leyes	Leyes	k1gMnSc1	Leyes
Nuevos	Nuevos	k1gMnSc1	Nuevos
<g/>
,	,	kIx,	,
1542	[number]	k4	1542
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tyto	tento	k3xDgFnPc1	tento
snahy	snaha	k1gFnPc1	snaha
narážely	narážet	k5eAaImAgFnP	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
conquistadorů	conquistador	k1gMnPc2	conquistador
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
místních	místní	k2eAgFnPc2d1	místní
kreolských	kreolský	k2eAgFnPc2d1	kreolská
elit	elita	k1gFnPc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
metropole	metropol	k1gFnSc2	metropol
a	a	k8xC	a
úrovni	úroveň	k1gFnSc6	úroveň
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
komunikace	komunikace	k1gFnSc1	komunikace
byla	být	k5eAaImAgFnS	být
vymahatelnost	vymahatelnost	k1gFnSc4	vymahatelnost
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
spíše	spíše	k9	spíše
problematická	problematický	k2eAgFnSc1d1	problematická
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
koloniální	koloniální	k2eAgFnSc1d1	koloniální
správa	správa	k1gFnSc1	správa
konsolidovala	konsolidovat	k5eAaBmAgFnS	konsolidovat
a	a	k8xC	a
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
období	období	k1gNnSc2	období
stagnace	stagnace	k1gFnSc2	stagnace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
postupným	postupný	k2eAgInSc7d1	postupný
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
a	a	k8xC	a
politickým	politický	k2eAgInSc7d1	politický
úpadkem	úpadek	k1gInSc7	úpadek
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýrazněji	výrazně	k6eAd3	výrazně
se	se	k3xPyFc4	se
koloniální	koloniální	k2eAgFnPc1d1	koloniální
správy	správa	k1gFnPc1	správa
dotkly	dotknout	k5eAaPmAgFnP	dotknout
tzv.	tzv.	kA	tzv.
bourbonské	bourbonský	k2eAgFnSc2d1	Bourbonská
reformy	reforma	k1gFnSc2	reforma
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
reflektovat	reflektovat	k5eAaImF	reflektovat
osvícenské	osvícenský	k2eAgFnPc1d1	osvícenská
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
vládní	vládní	k2eAgFnSc6d1	vládní
praxi	praxe	k1gFnSc6	praxe
a	a	k8xC	a
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
oživení	oživení	k1gNnSc4	oživení
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
reforem	reforma	k1gFnPc2	reforma
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
správní	správní	k2eAgFnPc1d1	správní
jednotky	jednotka	k1gFnPc1	jednotka
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
intendencie	intendencie	k1gFnSc2	intendencie
(	(	kIx(	(
<g/>
1786	[number]	k4	1786
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
intendantem	intendant	k1gMnSc7	intendant
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
hlavními	hlavní	k2eAgMnPc7d1	hlavní
úkoly	úkol	k1gInPc4	úkol
byly	být	k5eAaImAgInP	být
nově	nově	k6eAd1	nově
specifikovány	specifikován	k2eAgInPc1d1	specifikován
-	-	kIx~	-
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
například	například	k6eAd1	například
zhotovení	zhotovení	k1gNnSc1	zhotovení
nových	nový	k2eAgFnPc2d1	nová
topografických	topografický	k2eAgFnPc2d1	topografická
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
ekonomických	ekonomický	k2eAgNnPc2d1	ekonomické
studií	studio	k1gNnPc2	studio
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
šíření	šíření	k1gNnSc4	šíření
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
technologických	technologický	k2eAgFnPc2d1	technologická
<g />
.	.	kIx.	.
</s>
<s>
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
atp.	atp.	kA	atp.
Ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zrušilo	zrušit	k5eAaPmAgNnS	zrušit
Radu	Rada	k1gMnSc4	Rada
pro	pro	k7c4	pro
Indie	Indie	k1gFnPc4	Indie
a	a	k8xC	a
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
ji	on	k3xPp3gFnSc4	on
Sekretariátem	sekretariát	k1gInSc7	sekretariát
úřadu	úřad	k1gInSc2	úřad
Indií	Indie	k1gFnSc7	Indie
(	(	kIx(	(
<g/>
Secretaría	Secretaría	k1gFnSc1	Secretaría
del	del	k?	del
Despacho	Despacha	k1gFnSc5	Despacha
de	de	k?	de
Indias	Indias	k1gInSc4	Indias
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Severoamerické	severoamerický	k2eAgFnPc1d1	severoamerická
součásti	součást	k1gFnPc1	součást
španělského	španělský	k2eAgNnSc2d1	španělské
panství	panství	k1gNnSc2	panství
byly	být	k5eAaImAgFnP	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
záhy	záhy	k6eAd1	záhy
integrovány	integrovat	k5eAaBmNgFnP	integrovat
do	do	k7c2	do
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
dominoval	dominovat	k5eAaImAgInS	dominovat
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
především	především	k9	především
zájem	zájem	k1gInSc4	zájem
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Naleziště	naleziště	k1gNnSc1	naleziště
v	v	k7c6	v
Zacatecas	Zacatecas	k1gMnSc1	Zacatecas
(	(	kIx(	(
<g/>
1548	[number]	k4	1548
<g/>
)	)	kIx)	)
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
Španělům	Španěl	k1gMnPc3	Španěl
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
ochranu	ochrana	k1gFnSc4	ochrana
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vynakládat	vynakládat	k5eAaImF	vynakládat
nemalé	malý	k2eNgNnSc4d1	nemalé
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
žili	žít	k5eAaImAgMnP	žít
nepřátelští	přátelský	k2eNgMnPc1d1	nepřátelský
Chichimékové	Chichiméek	k1gMnPc1	Chichiméek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přepadali	přepadat	k5eAaImAgMnP	přepadat
španělské	španělský	k2eAgInPc4d1	španělský
konvoje	konvoj	k1gInPc4	konvoj
se	s	k7c7	s
stříbrem	stříbro	k1gNnSc7	stříbro
a	a	k8xC	a
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
španělské	španělský	k2eAgInPc4d1	španělský
doly	dol	k1gInPc4	dol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1650-1750	[number]	k4	1650-1750
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
stagnaci	stagnace	k1gFnSc3	stagnace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
těžby	těžba	k1gFnSc2	těžba
stříbra	stříbro	k1gNnSc2	stříbro
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
konkurovat	konkurovat	k5eAaImF	konkurovat
produkci	produkce	k1gFnSc4	produkce
s	s	k7c7	s
Peru	Peru	k1gNnSc7	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
stříbra	stříbro	k1gNnSc2	stříbro
se	se	k3xPyFc4	se
koloniální	koloniální	k2eAgNnSc1d1	koloniální
hospodářství	hospodářství	k1gNnSc1	hospodářství
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
na	na	k7c4	na
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
výrobu	výroba	k1gFnSc4	výroba
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
zámořský	zámořský	k2eAgInSc4d1	zámořský
trh	trh	k1gInSc4	trh
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
produkce	produkce	k1gFnSc1	produkce
barviv	barvivo	k1gNnPc2	barvivo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
<g/>
)	)	kIx)	)
a	a	k8xC	a
část	část	k1gFnSc1	část
na	na	k7c4	na
místní	místní	k2eAgInSc4d1	místní
trh	trh	k1gInSc4	trh
(	(	kIx(	(
<g/>
dobytek	dobytek	k1gInSc1	dobytek
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc1	obilí
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
profity	profit	k1gInPc1	profit
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
obchodní	obchodní	k2eAgFnSc4d1	obchodní
činnost	činnost	k1gFnSc4	činnost
byly	být	k5eAaImAgFnP	být
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
bělochů	běloch	k1gMnPc2	běloch
(	(	kIx(	(
<g/>
Španělů	Španěl	k1gMnPc2	Španěl
nebo	nebo	k8xC	nebo
kreolů	kreol	k1gMnPc2	kreol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlivy	vliv	k1gInPc1	vliv
dalších	další	k2eAgFnPc2d1	další
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
velmocí	velmoc	k1gFnPc2	velmoc
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
počínající	počínající	k2eAgFnSc3d1	počínající
výhodě	výhoda	k1gFnSc3	výhoda
a	a	k8xC	a
geografickým	geografický	k2eAgFnPc3d1	geografická
podmínkám	podmínka	k1gFnPc3	podmínka
byl	být	k5eAaImAgInS	být
vlastní	vlastní	k2eAgInSc1d1	vlastní
jih	jih	k1gInSc1	jih
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
zhruba	zhruba	k6eAd1	zhruba
dnešnímu	dnešní	k2eAgNnSc3d1	dnešní
Mexiku	Mexiko	k1gNnSc3	Mexiko
<g/>
)	)	kIx)	)
relativně	relativně	k6eAd1	relativně
pevně	pevně	k6eAd1	pevně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
ale	ale	k8xC	ale
nebránila	bránit	k5eNaImAgFnS	bránit
tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
formám	forma	k1gFnPc3	forma
soupeření	soupeření	k1gNnSc2	soupeření
o	o	k7c6	o
oblasti	oblast	k1gFnSc6	oblast
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
pod	pod	k7c7	pod
španělskou	španělský	k2eAgFnSc7d1	španělská
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
o	o	k7c6	o
soupeření	soupeření	k1gNnSc6	soupeření
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInPc1d1	prvotní
výhody	výhod	k1gInPc1	výhod
vyplývající	vyplývající	k2eAgInPc1d1	vyplývající
z	z	k7c2	z
pionýrského	pionýrský	k2eAgNnSc2d1	pionýrské
postavení	postavení	k1gNnSc2	postavení
Španělska	Španělsko	k1gNnSc2	Španělsko
jako	jako	k8xC	jako
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
novodobých	novodobý	k2eAgFnPc2d1	novodobá
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
,	,	kIx,	,
vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stagnace	stagnace	k1gFnSc2	stagnace
a	a	k8xC	a
postupný	postupný	k2eAgInSc4d1	postupný
úpadek	úpadek	k1gInSc4	úpadek
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
a	a	k8xC	a
mocenského	mocenský	k2eAgInSc2d1	mocenský
vlivu	vliv	k1gInSc2	vliv
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
oblasti	oblast	k1gFnSc6	oblast
začala	začít	k5eAaPmAgFnS	začít
během	během	k7c2	během
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jednoznačně	jednoznačně	k6eAd1	jednoznačně
dominovat	dominovat	k5eAaImF	dominovat
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
například	například	k6eAd1	například
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
anglické	anglický	k2eAgInPc1d1	anglický
zájmy	zájem	k1gInPc1	zájem
začaly	začít	k5eAaPmAgInP	začít
postupně	postupně	k6eAd1	postupně
určovat	určovat	k5eAaImF	určovat
poptávku	poptávka	k1gFnSc4	poptávka
i	i	k8xC	i
odbyt	odbyt	k1gInSc4	odbyt
zboží	zboží	k1gNnSc2	zboží
ze	z	k7c2	z
španělských	španělský	k2eAgFnPc2d1	španělská
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
postupovali	postupovat	k5eAaImAgMnP	postupovat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
stále	stále	k6eAd1	stále
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
objevovali	objevovat	k5eAaImAgMnP	objevovat
i	i	k8xC	i
kolonizovali	kolonizovat	k5eAaBmAgMnP	kolonizovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
dnešních	dnešní	k2eAgInPc6d1	dnešní
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Anglosaský	anglosaský	k2eAgInSc1d1	anglosaský
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
omezoval	omezovat	k5eAaImAgMnS	omezovat
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
území	území	k1gNnSc6	území
13	[number]	k4	13
původních	původní	k2eAgFnPc2d1	původní
kolonií	kolonie	k1gFnPc2	kolonie
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jihu	jih	k1gInSc2	jih
dnešních	dnešní	k2eAgInPc2d1	dnešní
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
státy	stát	k1gInPc1	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
,	,	kIx,	,
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nárazníková	nárazníkový	k2eAgFnSc1d1	nárazníková
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
vedle	vedle	k7c2	vedle
Španělů	Španěl	k1gMnPc2	Španěl
pronikaly	pronikat	k5eAaImAgFnP	pronikat
také	také	k6eAd1	také
francouzské	francouzský	k2eAgInPc1d1	francouzský
vlivy	vliv	k1gInPc1	vliv
(	(	kIx(	(
<g/>
z	z	k7c2	z
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Louisiany	Louisiana	k1gFnSc2	Louisiana
<g/>
)	)	kIx)	)
-	-	kIx~	-
jako	jako	k8xS	jako
například	například	k6eAd1	například
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
-	-	kIx~	-
i	i	k9	i
anglosaské	anglosaský	k2eAgInPc1d1	anglosaský
vlivy	vliv	k1gInPc1	vliv
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
ale	ale	k9	ale
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
byla	být	k5eAaImAgFnS	být
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
území	území	k1gNnSc4	území
severu	sever	k1gInSc2	sever
Nového	Nového	k2eAgNnSc2d1	Nového
Španělska	Španělsko	k1gNnSc2	Španělsko
podstoupena	podstoupen	k2eAgFnSc1d1	podstoupena
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
smyslu	smysl	k1gInSc6	smysl
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
území	území	k1gNnPc4	území
opět	opět	k6eAd1	opět
kolonizována	kolonizovat	k5eAaBmNgNnP	kolonizovat
tentokrát	tentokrát	k6eAd1	tentokrát
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nezávislost	nezávislost	k1gFnSc4	nezávislost
španělské	španělský	k2eAgFnSc2d1	španělská
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Osvícenské	osvícenský	k2eAgNnSc1d1	osvícenské
myšlení	myšlení	k1gNnSc1	myšlení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
konsolidace	konsolidace	k1gFnSc1	konsolidace
kreolských	kreolský	k2eAgFnPc2d1	kreolská
elit	elita	k1gFnPc2	elita
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
také	také	k9	také
inspirace	inspirace	k1gFnSc1	inspirace
americkou	americký	k2eAgFnSc4d1	americká
revoluci	revoluce	k1gFnSc4	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
a	a	k8xC	a
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
revolucí	revoluce	k1gFnSc7	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
a	a	k8xC	a
jevy	jev	k1gInPc1	jev
stály	stát	k5eAaImAgInP	stát
za	za	k7c7	za
hnutím	hnutí	k1gNnSc7	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
španělských	španělský	k2eAgFnPc2d1	španělská
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
na	na	k7c4	na
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
formovaly	formovat	k5eAaImAgInP	formovat
prakticky	prakticky	k6eAd1	prakticky
během	během	k7c2	během
celého	celý	k2eAgNnSc2d1	celé
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
reformní	reformní	k2eAgFnPc4d1	reformní
snahy	snaha	k1gFnPc4	snaha
nové	nový	k2eAgFnSc2d1	nová
bourbonské	bourbonský	k2eAgFnSc2d1	Bourbonská
dynastie	dynastie	k1gFnSc2	dynastie
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
během	během	k7c2	během
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nepodařilo	podařit	k5eNaPmAgNnS	podařit
znovu	znovu	k6eAd1	znovu
upevnit	upevnit	k5eAaPmF	upevnit
a	a	k8xC	a
oživit	oživit	k5eAaPmF	oživit
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
metropolí	metropol	k1gFnSc7	metropol
a	a	k8xC	a
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svojit	k5eAaImIp3nS	svojit
roli	role	k1gFnSc4	role
hrála	hrát	k5eAaImAgFnS	hrát
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
loďstvo	loďstvo	k1gNnSc1	loďstvo
plně	plně	k6eAd1	plně
kontrolovaly	kontrolovat	k5eAaImAgInP	kontrolovat
vody	voda	k1gFnPc4	voda
i	i	k8xC	i
obchod	obchod	k1gInSc4	obchod
mezi	mezi	k7c7	mezi
koloniemi	kolonie	k1gFnPc7	kolonie
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
zájmy	zájem	k1gInPc1	zájem
kreolů	kreol	k1gMnPc2	kreol
byly	být	k5eAaImAgFnP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
orientovány	orientovat	k5eAaBmNgInP	orientovat
na	na	k7c6	na
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
už	už	k9	už
tak	tak	k6eAd1	tak
problematických	problematický	k2eAgInPc2d1	problematický
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
španělskou	španělský	k2eAgFnSc7d1	španělská
metropolí	metropol	k1gFnSc7	metropol
a	a	k8xC	a
koloniemi	kolonie	k1gFnPc7	kolonie
byly	být	k5eAaImAgFnP	být
napoleonské	napoleonský	k2eAgFnPc1d1	napoleonská
války	válka	k1gFnPc1	válka
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
mělo	mít	k5eAaImAgNnS	mít
Španělsko	Španělsko	k1gNnSc1	Španělsko
dva	dva	k4xCgMnPc4	dva
panovníky	panovník	k1gMnPc4	panovník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
oba	dva	k4xCgMnPc1	dva
nárokovali	nárokovat	k5eAaImAgMnP	nárokovat
titul	titul	k1gInSc4	titul
španělských	španělský	k2eAgMnPc2d1	španělský
králů	král	k1gMnPc2	král
a	a	k8xC	a
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
plynoucí	plynoucí	k2eAgFnPc4d1	plynoucí
kontroly	kontrola	k1gFnPc4	kontrola
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
místní	místní	k2eAgFnPc1d1	místní
kreolské	kreolský	k2eAgFnPc1d1	kreolská
elity	elita	k1gFnPc1	elita
viděly	vidět	k5eAaImAgFnP	vidět
koloniální	koloniální	k2eAgInSc4d1	koloniální
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
měli	mít	k5eAaImAgMnP	mít
podřízené	podřízený	k2eAgNnSc4d1	podřízené
postavení	postavení	k1gNnSc4	postavení
stále	stále	k6eAd1	stále
kritičtěji	kriticky	k6eAd2	kriticky
<g/>
,	,	kIx,	,
nepřispěla	přispět	k5eNaPmAgFnS	přispět
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
centru	centrum	k1gNnSc3	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
tak	tak	k9	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Querétaru	Querétar	k1gInSc6	Querétar
spiklenecká	spiklenecký	k2eAgFnSc1d1	spiklenecká
junta	junta	k1gFnSc1	junta
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
připravit	připravit	k5eAaPmF	připravit
povstání	povstání	k1gNnSc1	povstání
proti	proti	k7c3	proti
Španělům	Španěl	k1gMnPc3	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
juntě	junta	k1gFnSc3	junta
se	se	k3xPyFc4	se
mimo	mimo	k6eAd1	mimo
kreolských	kreolský	k2eAgMnPc2d1	kreolský
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
přidal	přidat	k5eAaPmAgMnS	přidat
také	také	k9	také
kněz	kněz	k1gMnSc1	kněz
Miguel	Miguel	k1gMnSc1	Miguel
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
y	y	k?	y
Costilla	Costilla	k1gFnSc1	Costilla
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
četba	četba	k1gFnSc1	četba
osvícenských	osvícenský	k2eAgMnPc2d1	osvícenský
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
znalost	znalost	k1gFnSc1	znalost
poměrů	poměr	k1gInPc2	poměr
mezi	mezi	k7c7	mezi
vesnickým	vesnický	k2eAgNnSc7d1	vesnické
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
přivedly	přivést	k5eAaPmAgFnP	přivést
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
kritiků	kritik	k1gMnPc2	kritik
koloniální	koloniální	k2eAgFnSc2d1	koloniální
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1810	[number]	k4	1810
vydal	vydat	k5eAaPmAgMnS	vydat
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
provolání	provolání	k1gNnSc4	provolání
nazvané	nazvaný	k2eAgNnSc4d1	nazvané
Výzva	výzva	k1gFnSc1	výzva
z	z	k7c2	z
Dolores	Doloresa	k1gFnPc2	Doloresa
(	(	kIx(	(
<g/>
Grito	Grito	k1gNnSc1	Grito
de	de	k?	de
Dolores	Dolores	k1gMnSc1	Dolores
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
žádal	žádat	k5eAaImAgMnS	žádat
navrácení	navrácení	k1gNnSc1	navrácení
země	zem	k1gFnSc2	zem
indiánským	indiánský	k2eAgInPc3d1	indiánský
obcím	obec	k1gFnPc3	obec
<g/>
,	,	kIx,	,
boj	boj	k1gInSc1	boj
proti	proti	k7c3	proti
Španělům	Španěl	k1gMnPc3	Španěl
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc3	jejich
daním	daň	k1gFnPc3	daň
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
zrušení	zrušení	k1gNnSc2	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
první	první	k4xOgInSc1	první
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
bod	bod	k1gInSc1	bod
o	o	k7c4	o
navrácené	navrácený	k2eAgFnPc4d1	navrácená
země	zem	k1gFnPc4	zem
indiánským	indiánský	k2eAgInPc3d1	indiánský
obcím	obec	k1gFnPc3	obec
dal	dát	k5eAaPmAgMnS	dát
hnutí	hnutí	k1gNnSc4	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
masovou	masový	k2eAgFnSc4d1	masová
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
otevřenému	otevřený	k2eAgInSc3d1	otevřený
konfliktu	konflikt	k1gInSc3	konflikt
se	s	k7c7	s
španělskými	španělský	k2eAgFnPc7d1	španělská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
porážkou	porážka	k1gFnSc7	porážka
Hidalga	Hidalgo	k1gMnSc2	Hidalgo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
popravou	poprava	k1gFnSc7	poprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
ale	ale	k8xC	ale
Hidalgovou	Hidalgův	k2eAgFnSc7d1	Hidalgův
smrtí	smrt	k1gFnSc7	smrt
zdaleka	zdaleka	k6eAd1	zdaleka
neskončil	skončit	k5eNaPmAgMnS	skončit
a	a	k8xC	a
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
v	v	k7c4	v
partyzánskou	partyzánský	k2eAgFnSc4d1	Partyzánská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
návratem	návrat	k1gInSc7	návrat
bourbonského	bourbonský	k2eAgMnSc2d1	bourbonský
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
VII	VII	kA	VII
<g/>
.	.	kIx.	.
dojde	dojít	k5eAaPmIp3nS	dojít
také	také	k9	také
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Kompromisem	kompromis	k1gInSc7	kompromis
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Plánu	plán	k1gInSc6	plán
z	z	k7c2	z
Igualy	Iguala	k1gFnSc2	Iguala
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
velitel	velitel	k1gMnSc1	velitel
španělských	španělský	k2eAgFnPc2d1	španělská
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
jednotek	jednotka	k1gFnPc2	jednotka
generál	generál	k1gMnSc1	generál
Agustín	Agustín	k1gMnSc1	Agustín
de	de	k?	de
Iturbide	Iturbid	k1gInSc5	Iturbid
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
s	s	k7c7	s
vůdcem	vůdce	k1gMnSc7	vůdce
partyzánů	partyzán	k1gMnPc2	partyzán
Vicentem	Vicent	k1gMnSc7	Vicent
Guerrerou	Guerrera	k1gMnSc7	Guerrera
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
konstituční	konstituční	k2eAgFnSc4d1	konstituční
monarchii	monarchie	k1gFnSc4	monarchie
s	s	k7c7	s
katolickým	katolický	k2eAgMnSc7d1	katolický
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
stanovením	stanovení	k1gNnSc7	stanovení
regenta	regens	k1gMnSc2	regens
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	s	k7c7	s
sjednocením	sjednocení	k1gNnSc7	sjednocení
kreolů	kreol	k1gMnPc2	kreol
a	a	k8xC	a
Španělů	Španěl	k1gMnPc2	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
Plán	plán	k1gInSc1	plán
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
získával	získávat	k5eAaImAgMnS	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Plánem	plán	k1gInSc7	plán
ale	ale	k8xC	ale
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
Madrid	Madrid	k1gInSc4	Madrid
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
nový	nový	k2eAgMnSc1d1	nový
místokrál	místokrál	k1gMnSc1	místokrál
Juan	Juan	k1gMnSc1	Juan
O	o	k7c6	o
Donujú	Donujú	k1gFnSc6	Donujú
uznáním	uznání	k1gNnSc7	uznání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Mexika	Mexiko	k1gNnSc2	Mexiko
tzv.	tzv.	kA	tzv.
Smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
Cordóby	Cordóba	k1gFnSc2	Cordóba
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Mexika	Mexiko	k1gNnSc2	Mexiko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Kanady	Kanada	k1gFnSc2	Kanada
</s>
</p>
<p>
<s>
Karibik	Karibik	k1gMnSc1	Karibik
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
