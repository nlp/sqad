<s>
Čandraján-	Čandraján-	k?
<g/>
2	#num#	k4
</s>
<s>
Čandraján-	Čandraján-	k?
<g/>
2	#num#	k4
Start	start	k1gInSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Šríharikota	Šríharikota	k1gFnSc1
Nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
GSLV	GSLV	kA
Mk	Mk	k1gFnSc1
<g/>
.3	.3	k4
Stav	stav	k1gInSc1
objektu	objekt	k1gInSc2
</s>
<s>
úspěšně	úspěšně	k6eAd1
vynesl	vynést	k5eAaPmAgInS
vesmírnou	vesmírný	k2eAgFnSc4d1
sondu	sonda	k1gFnSc4
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
ISRO	ISRO	kA
Výrobce	výrobce	k1gMnSc1
</s>
<s>
ISRO	ISRO	kA
Druh	druh	k1gMnSc1
</s>
<s>
komplex	komplex	k1gInSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
Měsíce	měsíc	k1gInSc2
Mateřské	mateřský	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
</s>
<s>
Měsíc	měsíc	k1gInSc1
Program	program	k1gInSc1
</s>
<s>
Chandrayaan	Chandrayaan	k1gInSc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
:	:	kIx,
3890	#num#	k4
kgOrbiter	kgOrbitra	k1gFnPc2
<g/>
:	:	kIx,
2379	#num#	k4
kgLander	kgLandra	k1gFnPc2
<g/>
:	:	kIx,
1471	#num#	k4
kgRover	kgRovra	k1gFnPc2
<g/>
:	:	kIx,
27	#num#	k4
kgOdražeč	kgOdražeč	k1gInSc1
<g/>
:	:	kIx,
asi	asi	k9
13	#num#	k4
kg	kg	kA
</s>
<s>
Orbiter	Orbiter	k1gMnSc1
</s>
<s>
Druh	druh	k1gInSc1
</s>
<s>
umělá	umělý	k2eAgFnSc1d1
družice	družice	k1gFnSc1
Měsíce	měsíc	k1gInSc2
</s>
<s>
Cíl	cíl	k1gInSc1
</s>
<s>
oběžná	oběžný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Měsíce	měsíc	k1gInSc2
</s>
<s>
Doba	doba	k1gFnSc1
letu	let	k1gInSc2
</s>
<s>
1	#num#	k4
rok	rok	k1gInSc4
</s>
<s>
Stav	stav	k1gInSc1
</s>
<s>
na	na	k7c6
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
kolem	kolem	k7c2
Měsíce	měsíc	k1gInSc2
</s>
<s>
Provozovatel	provozovatel	k1gMnSc1
</s>
<s>
ISRO	ISRO	kA
</s>
<s>
Výrobce	výrobce	k1gMnSc1
</s>
<s>
ISRO	ISRO	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
2349	#num#	k4
kg	kg	kA
</s>
<s>
Parametry	parametr	k1gInPc1
dráhy	dráha	k1gFnSc2
Apogeum	apogeum	k1gNnSc1
</s>
<s>
100	#num#	k4
km	km	kA
Perigeum	perigeum	k1gNnSc1
</s>
<s>
100	#num#	k4
km	km	kA
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
https://www.isro.gov.in/chandrayaan2-home-0	https://www.isro.gov.in/chandrayaan2-home-0	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čandraján-	Čandraján-	k1gFnSc1
<g/>
2	#num#	k4
(	(	kIx(
<g/>
Sanskrt	sanskrta	k1gFnPc2
<g/>
:	:	kIx,
च	च	k?
<g/>
्	्	k?
<g/>
द	द	k?
<g/>
्	्	k?
<g/>
र	र	k?
<g/>
ा	ा	k?
<g/>
न	न	k?
<g/>
२	२	k?
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Chandrayaan-	Chandrayaan-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
měsíční	měsíční	k2eAgFnSc1d1
průzkumná	průzkumný	k2eAgFnSc1d1
mise	mise	k1gFnSc1
Indie	Indie	k1gFnSc2
po	po	k7c6
Čandraján-	Čandraján-	k1gFnSc6
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Vyvinutá	vyvinutý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
Indickou	indický	k2eAgFnSc7d1
kosmickou	kosmický	k2eAgFnSc7d1
agenturou	agentura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
start	start	k1gInSc4
ze	z	k7c2
Země	zem	k1gFnSc2
použila	použít	k5eAaPmAgFnS
novou	nový	k2eAgFnSc4d1
indickou	indický	k2eAgFnSc4d1
nosnou	nosný	k2eAgFnSc4d1
raketu	raketa	k1gFnSc4
GSLV	GSLV	kA
Mk	Mk	k1gFnSc1
<g/>
.3	.3	k4
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Náklad	náklad	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
lunárního	lunární	k2eAgInSc2d1
orbiteru	orbiter	k1gInSc2
(	(	kIx(
<g/>
umělá	umělý	k2eAgFnSc1d1
družice	družice	k1gFnSc1
Měsíce	měsíc	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
landeru	lander	k1gMnSc3
a	a	k8xC
roveru	rover	k1gMnSc3
<g/>
,	,	kIx,
vyvinutých	vyvinutý	k2eAgNnPc2d1
Indií	indium	k1gNnPc2
a	a	k8xC
z	z	k7c2
laserového	laserový	k2eAgInSc2d1
odrážeče	odrážeč	k1gInSc2
NASA	NASA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Start	start	k1gInSc1
Čandrajánu-	Čandrajánu-	k1gFnSc1
<g/>
2	#num#	k4
úspěšně	úspěšně	k6eAd1
proběhl	proběhnout	k5eAaPmAgInS
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
o	o	k7c4
měkké	měkký	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
pozemní	pozemní	k2eAgFnSc2d1
sondy	sonda	k1gFnSc2
s	s	k7c7
vozítkem	vozítko	k1gNnSc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
krátery	kráter	k1gInPc7
Manzinus	Manzinus	k1gInSc1
C	C	kA
a	a	k8xC
Simpelius	Simpelius	k1gInSc1
N	N	kA
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
měsíční	měsíční	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
asi	asi	k9
70	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
sonda	sonda	k1gFnSc1
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
by	by	kYmCp3nS
se	se	k3xPyFc4
druhou	druhý	k4xOgFnSc7
misí	mise	k1gFnSc7
s	s	k7c7
přistáním	přistání	k1gNnSc7
roveru	rover	k1gMnSc3
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
lunárního	lunární	k2eAgInSc2d1
jižního	jižní	k2eAgInSc2d1
pólu	pól	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čandraján-	Čandraján-	k?
<g/>
2	#num#	k4
měl	mít	k5eAaImAgInS
naplánovaný	naplánovaný	k2eAgInSc1d1
start	start	k1gInSc1
na	na	k7c4
duben	duben	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Start	start	k1gInSc1
naplánovaný	naplánovaný	k2eAgInSc1d1
na	na	k7c6
14	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
v	v	k7c4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
UTC	UTC	kA
(	(	kIx(
<g/>
v	v	k7c6
Indii	Indie	k1gFnSc6
bude	být	k5eAaImBp3nS
už	už	k6eAd1
15	#num#	k4
<g/>
.	.	kIx.
<g/>
hodin	hodina	k1gFnPc2
ITC	ITC	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
přerušený	přerušený	k2eAgMnSc1d1
v	v	k7c6
čase	čas	k1gInSc6
-56	-56	k4
minut	minuta	k1gFnPc2
před	před	k7c7
startem	start	k1gInSc7
kvůli	kvůli	k7c3
technické	technický	k2eAgFnSc3d1
poruše	porucha	k1gFnSc3
<g/>
,	,	kIx,
úspěšně	úspěšně	k6eAd1
odstartoval	odstartovat	k5eAaPmAgInS
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
v	v	k7c4
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
UTC	UTC	kA
(	(	kIx(
<g/>
14	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
IST	IST	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemní	pozemní	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
s	s	k7c7
vozítkem	vozítko	k1gNnSc7
se	se	k3xPyFc4
pokusila	pokusit	k5eAaPmAgFnS
o	o	k7c4
měkké	měkký	k2eAgNnSc4d1
přistátí	přistátí	k1gNnSc4
6	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nP
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
krátery	kráter	k1gInPc7
Manzinus	Manzinus	k1gInSc1
C	C	kA
a	a	k8xC
Simpelius	Simpelius	k1gInSc1
N	N	kA
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
měsíční	měsíční	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
asi	asi	k9
70	#num#	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výšce	výška	k1gFnSc6
2,1	2,1	k4
km	km	kA
nad	nad	k7c7
povrchem	povrch	k1gInSc7
se	se	k3xPyFc4
spojení	spojení	k1gNnSc1
s	s	k7c7
přistávacím	přistávací	k2eAgInSc7d1
modulem	modul	k1gInSc7
ztratilo	ztratit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Orbiter	Orbiter	k1gInSc1
a	a	k8xC
přistávací	přistávací	k2eAgInSc1d1
modul	modul	k1gInSc1
(	(	kIx(
<g/>
lander	lander	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
vozítkem	vozítko	k1gNnSc7
(	(	kIx(
<g/>
rover	rover	k1gMnSc1
<g/>
)	)	kIx)
uvnitř	uvnitř	k7c2
povrchového	povrchový	k2eAgInSc2d1
výložníku	výložník	k1gInSc2
</s>
<s>
Pro	pro	k7c4
misi	mise	k1gFnSc4
byla	být	k5eAaImAgFnS
využita	využít	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
indická	indický	k2eAgFnSc1d1
nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
GSLV	GSLV	kA
Mk	Mk	k1gFnSc1
<g/>
.3	.3	k4
<g/>
,	,	kIx,
hmotnost	hmotnost	k1gFnSc1
nákladu	náklad	k1gInSc2
byla	být	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
3890	#num#	k4
kg	kg	kA
a	a	k8xC
start	start	k1gInSc1
proběhl	proběhnout	k5eAaPmAgInS
z	z	k7c2
vesmírného	vesmírný	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
Satish	Satish	k1gMnSc1
Dhawan	Dhawan	k1gMnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Sriharikota	Sriharikota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Orbiter	Orbiter	k1gMnSc1
</s>
<s>
Orbiter	Orbiter	k1gInSc1
obíhá	obíhat	k5eAaImIp3nS
Měsíc	měsíc	k1gInSc4
ve	v	k7c6
výšce	výška	k1gFnSc6
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orbiter	Orbitra	k1gFnPc2
má	mít	k5eAaImIp3nS
osm	osm	k4xCc1
vědeckých	vědecký	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
<g/>
;	;	kIx,
dva	dva	k4xCgMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
jsou	být	k5eAaImIp3nP
vylepšenými	vylepšený	k2eAgFnPc7d1
verzemi	verze	k1gFnPc7
ze	z	k7c2
sondy	sonda	k1gFnSc2
Čandraján-	Čandraján-	k1gFnSc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližná	přibližný	k2eAgFnSc1d1
počáteční	počáteční	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
byla	být	k5eAaImAgFnS
2379	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamera	kamera	k1gFnSc1
Orbiter	Orbiter	k1gInSc4
High	High	k1gInSc1
Resolution	Resolution	k1gInSc1
Camera	Camera	k1gFnSc1
(	(	kIx(
<g/>
OHRC	OHRC	kA
<g/>
)	)	kIx)
bude	být	k5eAaImBp3nS
vykonávat	vykonávat	k5eAaImF
pozorování	pozorování	k1gNnSc4
místa	místo	k1gNnSc2
přistání	přistání	k1gNnSc2
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
rozlišením	rozlišení	k1gNnSc7
před	před	k7c7
oddělením	oddělení	k1gNnSc7
přistávacího	přistávací	k2eAgInSc2d1
modulu	modul	k1gInSc2
od	od	k7c2
orbiteru	orbiter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orbiter	Orbitra	k1gFnPc2
vyrobila	vyrobit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Hindustan	Hindustan	k1gInSc1
Aeronautics	Aeronautics	k1gInSc1
Limited	limited	k2eAgFnSc4d1
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
dodána	dodat	k5eAaPmNgFnS
do	do	k7c2
satelitního	satelitní	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
ISRO	ISRO	kA
<g/>
.	.	kIx.
</s>
<s>
Lander	Lander	k1gMnSc1
</s>
<s>
Lander	Lander	k1gInSc1
mise	mise	k1gFnSc2
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
sa	sa	k?
nazývá	nazývat	k5eAaImIp3nS
Vikram	Vikram	k1gInSc4
a	a	k8xC
je	být	k5eAaImIp3nS
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
po	po	k7c6
panu	pan	k1gMnSc3
jménem	jméno	k1gNnSc7
Vikram	Vikram	k1gInSc4
Sarabhai	Sarabhai	k1gNnPc7
(	(	kIx(
<g/>
1919	#num#	k4
–	–	k?
1971	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
považovaný	považovaný	k2eAgInSc1d1
za	za	k7c4
otce	otec	k1gMnSc4
indického	indický	k2eAgInSc2d1
vesmírného	vesmírný	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližná	přibližný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
landeru	lander	k1gInSc2
a	a	k8xC
roveru	rover	k1gMnSc3
je	být	k5eAaImIp3nS
1471	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
landeru	landrat	k5eAaPmIp1nS
se	se	k3xPyFc4
skladá	skladý	k2eAgFnSc1d1
z	z	k7c2
osmi	osm	k4xCc2
motorů	motor	k1gInPc2
o	o	k7c6
výkonu	výkon	k1gInSc6
50	#num#	k4
N	N	kA
pro	pro	k7c4
řízení	řízení	k1gNnSc4
polohy	poloha	k1gFnSc2
a	a	k8xC
pěti	pět	k4xCc2
hlavních	hlavní	k2eAgInPc2d1
motorů	motor	k1gInPc2
o	o	k7c6
výkonu	výkon	k1gInSc6
800	#num#	k4
N	N	kA
odvozených	odvozený	k2eAgInPc2d1
z	z	k7c2
ISP	ISP	kA
440	#num#	k4
N	N	kA
Liquid	Liquida	k1gFnPc2
Apogee	Apogee	k1gFnSc1
Motor	motor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vikram	Vikram	k1gInSc1
může	moct	k5eAaImIp3nS
bezpečně	bezpečně	k6eAd1
přistát	přistát	k5eAaImF,k5eAaPmF
na	na	k7c6
svahu	svah	k1gInSc6
do	do	k7c2
12	#num#	k4
°	°	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2019	#num#	k4
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
přistání	přistání	k1gNnSc1
nepovedlo	povést	k5eNaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
zatím	zatím	k6eAd1
není	být	k5eNaImIp3nS
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
proč	proč	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rover	rover	k1gMnSc1
</s>
<s>
Náčrtek	náčrtek	k1gInSc1
Roveru	rover	k1gMnSc6
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
roveru	rover	k1gMnSc3
je	být	k5eAaImIp3nS
asi	asi	k9
27	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
napájený	napájený	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
ze	z	k7c2
solárního	solární	k2eAgInSc2d1
panelu	panel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rover	rover	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
měsíčním	měsíční	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
měl	mít	k5eAaImAgInS
pohybovat	pohybovat	k5eAaImF
na	na	k7c6
šesti	šest	k4xCc6
kolech	kolo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
provedení	provedení	k1gNnSc6
chemické	chemický	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
na	na	k7c6
místě	místo	k1gNnSc6
by	by	kYmCp3nS
poslal	poslat	k5eAaPmAgInS
data	datum	k1gNnPc4
na	na	k7c4
orbitu	orbita	k1gFnSc4
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
by	by	kYmCp3nP
se	se	k3xPyFc4
přenesly	přenést	k5eAaPmAgFnP
na	na	k7c4
pozemní	pozemní	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vědecké	vědecký	k2eAgInPc1d1
přístroje	přístroj	k1gInPc1
</s>
<s>
ISRO	ISRO	kA
vybral	vybrat	k5eAaPmAgInS
osm	osm	k4xCc4
vědeckých	vědecký	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
pro	pro	k7c4
orbiter	orbiter	k1gInSc4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgMnPc1
pro	pro	k7c4
lander	lander	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dva	dva	k4xCgMnPc1
pro	pro	k7c4
rover	rover	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
NASA	NASA	kA
a	a	k8xC
ESA	eso	k1gNnSc2
by	by	kYmCp3nS
se	se	k3xPyFc4
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
misi	mise	k1gFnSc6
zúčastnili	zúčastnit	k5eAaPmAgMnP
poskytnutím	poskytnutí	k1gNnSc7
některých	některý	k3yIgInPc2
vědeckých	vědecký	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
pro	pro	k7c4
orbiter	orbiter	k1gInSc4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
ISRO	ISRO	kA
později	pozdě	k6eAd2
objasnil	objasnit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
důvodu	důvod	k1gInSc2
omezení	omezení	k1gNnSc2
hmotnosti	hmotnost	k1gFnSc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
tato	tento	k3xDgFnSc1
mise	mise	k1gFnSc1
mít	mít	k5eAaImF
jen	jen	k9
omezené	omezený	k2eAgNnSc1d1
zahraniční	zahraniční	k2eAgNnSc1d1
užitečné	užitečný	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
(	(	kIx(
<g/>
laserový	laserový	k2eAgInSc1d1
retroreflexní	retroreflexní	k2eAgInSc1d1
odražeč	odražeč	k1gInSc1
od	od	k7c2
NASA	NASA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Čandraján-	Čandraján-	k1gFnSc2
<g/>
2	#num#	k4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
www.kosmonautix.cz	www.kosmonautix.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-07-17	2019-07-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ISRO	ISRO	kA
<g/>
.	.	kIx.
@	@	kIx~
<g/>
isro	isro	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
T	T	kA
<g/>
22	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ISRO	ISRO	kA
begins	begins	k1gInSc4
flight	flight	k2eAgInSc1d1
integration	integration	k1gInSc1
activity	activita	k1gFnSc2
for	forum	k1gNnPc2
Chandrayaan-	Chandrayaan-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
,	,	kIx,
as	as	k9
scientists	scientists	k6eAd1
tests	tests	k6eAd1
lander	lander	k1gMnSc1
and	and	k?
rover	rover	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Indian	Indiana	k1gFnPc2
Express	express	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
October	October	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
December	December	k1gInSc1
2017	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chandrayaan-	Chandrayaan-	k1gFnSc7
<g/>
2	#num#	k4
launch	launcha	k1gFnPc2
put	puta	k1gFnPc2
off	off	k?
<g/>
:	:	kIx,
India	indium	k1gNnPc1
<g/>
,	,	kIx,
Israel	Israel	k1gInSc1
in	in	k?
lunar	lunar	k1gInSc1
race	race	k1gFnPc2
for	forum	k1gNnPc2
4	#num#	k4
<g/>
th	th	k?
position	position	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Times	Times	k1gMnSc1
of	of	k?
India	indium	k1gNnSc2
<g/>
.	.	kIx.
5	#num#	k4
August	August	k1gMnSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
15	#num#	k4
August	August	k1gMnSc1
2018	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
India	indium	k1gNnSc2
Slips	Slipsa	k1gFnPc2
in	in	k?
Lunar	Lunar	k1gMnSc1
Race	Rac	k1gFnSc2
with	with	k1gMnSc1
Israel	Israel	k1gMnSc1
As	as	k9
Ambitious	Ambitious	k1gInSc4
Mission	Mission	k1gInSc1
Hits	Hitsa	k1gFnPc2
Delays	Delaysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NDTV	NDTV	kA
<g/>
.	.	kIx.
4	#num#	k4
August	August	k1gMnSc1
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
15	#num#	k4
August	August	k1gMnSc1
2018	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Indická	indický	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
na	na	k7c4
druhý	druhý	k4xOgInSc4
pokus	pokus	k1gInSc4
odstartovala	odstartovat	k5eAaPmAgFnS
a	a	k8xC
letí	letět	k5eAaImIp3nS
k	k	k7c3
Měsíci	měsíc	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BAGLA	BAGLA	kA
<g/>
,	,	kIx,
Pallava	Pallava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
India	indium	k1gNnPc1
plans	plans	k6eAd1
tricky	tricky	k6eAd1
and	and	k?
unprecedented	unprecedented	k1gInSc1
landing	landing	k1gInSc1
near	near	k1gInSc1
moon	moon	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
south	south	k1gInSc1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
31	#num#	k4
January	Januara	k1gFnSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
February	Februara	k1gFnSc2
2018	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CHANDRAYAAN	CHANDRAYAAN	kA
-2	-2	k4
LAUNCH	LAUNCH	kA
RESCHEDULED	RESCHEDULED	kA
ON	on	k3xPp3gMnSc1
22ND	22ND	k4
JULY	Jula	k1gMnPc7
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
AT	AT	kA
14	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
HRS	HRS	kA
-	-	kIx~
ISRO	ISRO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Wayback	Wayback	k1gInSc1
Machine	Machin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Meet	Meet	k2eAgInSc4d1
Vikram	Vikram	k1gInSc4
—	—	k?
Chandrayaan	Chandrayaan	k1gInSc1
2	#num#	k4
<g/>
’	’	k?
<g/>
s	s	k7c7
Lander	Landra	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SUBRAMANIAN	SUBRAMANIAN	kA
<g/>
,	,	kIx,
T.	T.	kA
S.	S.	kA
Chandrayaan	Chandrayaan	k1gInSc4
2	#num#	k4
<g/>
:	:	kIx,
Giant	Giant	k1gMnSc1
leap	leap	k1gMnSc1
for	forum	k1gNnPc2
ISRO	ISRO	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frontline	Frontlin	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Najdeme	najít	k5eAaPmIp1nP
sestupový	sestupový	k2eAgInSc4d1
modul	modul	k1gInSc4
Čandraján-	Čandraján-	k1gMnPc2
<g/>
2	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.kosmonautix.cz	www.kosmonautix.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chandrayaan	Chandrayaan	k1gInSc1
<g/>
2	#num#	k4
Payloads	Payloadsa	k1gFnPc2
-	-	kIx~
ISRO	ISRO	kA
<g/>
.	.	kIx.
www.isro.gov.in	www.isro.gov.in	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BAGLA	BAGLA	kA
<g/>
,	,	kIx,
Pallava	Pallava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
India	indium	k1gNnPc1
plans	plans	k6eAd1
tricky	tricky	k6eAd1
and	and	k?
unprecedented	unprecedented	k1gInSc1
landing	landing	k1gInSc1
near	near	k1gInSc1
moon	moon	k1gInSc4
<g/>
’	’	k?
<g/>
s	s	k7c7
south	south	k1gMnSc1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
31	#num#	k4
January	Januara	k1gFnSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
March	March	k1gInSc1
2018	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
T.	T.	kA
A.	A.	kA
Three	Three	k1gFnPc2
new	new	k?
Indian	Indiana	k1gFnPc2
payloads	payloads	k6eAd1
for	forum	k1gNnPc2
Chandrayaan	Chandrayaana	k1gFnPc2
2	#num#	k4
<g/>
,	,	kIx,
decides	decides	k1gInSc1
ISRO	ISRO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Indian	Indiana	k1gFnPc2
Express	express	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
August	August	k1gMnSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
31	#num#	k4
August	August	k1gMnSc1
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BEARY	BEARY	kA
<g/>
,	,	kIx,
Habib	Habib	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
and	and	k?
ESA	eso	k1gNnSc2
to	ten	k3xDgNnSc1
partner	partner	k1gMnSc1
for	forum	k1gNnPc2
Chandrayaan-	Chandrayaan-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sakal	Sakal	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
4	#num#	k4
February	Februara	k1gFnSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
22	#num#	k4
February	Februara	k1gFnSc2
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chandrayaan	Chandrayaan	k1gInSc1
2	#num#	k4
To	to	k9
Carry	Carra	k1gFnPc1
NASA	NASA	kA
Laser	laser	k1gInSc4
Instruments	Instruments	kA
To	ten	k3xDgNnSc4
Moon	Moon	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Know	Know	k1gMnSc1
About	About	k1gMnSc1
Them	Them	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NDTV	NDTV	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chandrayaan	Chandrayaan	k1gInSc4
2	#num#	k4
to	ten	k3xDgNnSc4
carry	carra	k1gFnPc4
NASA	NASA	kA
science	science	k1gFnSc1
instrument	instrument	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Economic	Economic	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LAXMAN	LAXMAN	kA
<g/>
,	,	kIx,
Srinivas	Srinivas	k1gMnSc1
<g/>
.	.	kIx.
'	'	kIx"
<g/>
We	We	k1gFnSc1
<g/>
'	'	kIx"
<g/>
re	re	k9
launching	launching	k1gInSc1
Chandrayaan-	Chandrayaan-	k1gMnPc2
<g/>
2	#num#	k4
for	forum	k1gNnPc2
a	a	k8xC
total	totat	k5eAaImAgInS
coverage	coverage	k1gInSc1
of	of	k?
the	the	k?
moon	moon	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Times	Times	k1gMnSc1
of	of	k?
India	indium	k1gNnSc2
<g/>
.	.	kIx.
5	#num#	k4
September	September	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Čandraján-	Čandraján-	k?
<g/>
1	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
NASA	NASA	kA
ukázala	ukázat	k5eAaPmAgFnS
zbytky	zbytek	k1gInPc7
indického	indický	k2eAgInSc2d1
modulu	modul	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
rozbil	rozbít	k5eAaPmAgInS
o	o	k7c4
Měsíc	měsíc	k1gInSc4
-	-	kIx~
iDnes	iDnes	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Lander	Lander	k1gMnSc1
sondy	sonda	k1gFnSc2
Čandraján	Čandraján	k1gInSc1
2	#num#	k4
se	se	k3xPyFc4
při	při	k7c6
tvrdém	tvrdý	k2eAgNnSc6d1
přistání	přistání	k1gNnSc6
(	(	kIx(
<g/>
snad	snad	k9
<g/>
)	)	kIx)
nerozpadl	rozpadnout	k5eNaPmAgInS
-	-	kIx~
kosmonautix	kosmonautix	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Indii	Indie	k1gFnSc3
se	se	k3xPyFc4
asi	asi	k9
nepodařilo	podařit	k5eNaPmAgNnS
přistát	přistát	k5eAaPmF,k5eAaImF
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modul	modul	k1gInSc1
se	se	k3xPyFc4
odmlčel	odmlčet	k5eAaPmAgInS
nad	nad	k7c7
povrchem	povrch	k1gInSc7
-	-	kIx~
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Indické	indický	k2eAgFnSc2d1
pristátie	pristátie	k1gFnSc2
na	na	k7c6
Mesiaci	Mesiace	k1gFnSc6
sa	sa	k?
zrejme	zrejit	k5eAaPmRp1nP
nepodarilo	podarít	k5eNaPmAgNnS,k5eNaBmAgNnS,k5eNaImAgNnS
<g/>
,	,	kIx,
s	s	k7c7
modulom	modulom	k1gInSc1
stratený	stratený	k2eAgInSc4d1
kontakt	kontakt	k1gInSc4
-	-	kIx~
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Indický	indický	k2eAgInSc1d1
modul	modul	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c4
finálnej	finálnat	k5eAaPmRp2nS,k5eAaImRp2nS
orbite	orbit	k1gInSc5
pred	pred	k6eAd1
piatkovým	piatkův	k2eAgNnPc3d1
pristátím	pristátí	k1gNnPc3
na	na	k7c6
Mesiaci	Mesiace	k1gFnSc6
-	-	kIx~
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Indická	indický	k2eAgFnSc1d1
misia	misia	k1gFnSc1
na	na	k7c4
povrch	povrch	k1gInSc4
Mesiaca	Mesiaca	k1gFnSc1
sa	sa	k?
mu	on	k3xPp3gMnSc3
priblížila	priblížit	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
,	,	kIx,
odfotila	odfotit	k5eAaImAgFnS,k5eAaPmAgFnS
ho	on	k3xPp3gMnSc4
-	-	kIx~
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Indická	indický	k2eAgFnSc1d1
misia	misia	k1gFnSc1
na	na	k7c4
povrch	povrch	k1gInSc4
Mesiaca	Mesiacum	k1gNnSc2
úspešne	úspešnout	k5eAaImIp3nS,k5eAaPmIp3nS
vstúpila	vstúpit	k5eAaPmAgFnS,k5eAaImAgFnS
na	na	k7c4
obežnú	obežnú	k?
dráhu	dráha	k1gFnSc4
-	-	kIx~
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Indická	indický	k2eAgFnSc1d1
loď	loď	k1gFnSc1
míří	mířit	k5eAaImIp3nS
k	k	k7c3
Měsíci	měsíc	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Udělá	udělat	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
3D	3D	k4
snímek	snímek	k1gInSc4
a	a	k8xC
vysadí	vysadit	k5eAaPmIp3nS
na	na	k7c6
něm	on	k3xPp3gInSc6
vozítko	vozítko	k1gNnSc1
-	-	kIx~
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Na	na	k7c6
jižním	jižní	k2eAgInSc6d1
pólu	pól	k1gInSc6
Měsíce	měsíc	k1gInSc2
bude	být	k5eAaImBp3nS
brzy	brzy	k6eAd1
husto	husto	k6eAd1
-	-	kIx~
www.osel.cz	www.osel.cz	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Indovia	Indovia	k1gFnSc1
sa	sa	k?
idú	idú	k?
pokúsiť	pokúsiť	k1gFnSc1
pristáť	pristáť	k1gFnSc1
na	na	k7c6
Mesiaci	Mesiace	k1gFnSc6
-	-	kIx~
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Čandraján-	Čandraján-	k1gFnSc1
<g/>
2	#num#	k4
-	-	kIx~
www.isro.gov.in	www.isro.gov.in	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
