<s>
Jon	Jon	k?	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
John	John	k1gMnSc1	John
Francis	Francis	k1gFnSc2	Francis
Bongiovi	Bongius	k1gMnSc3	Bongius
jr	jr	k?	jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
bratry	bratr	k1gMnPc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1989	[number]	k4	1989
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Dorotheou	Dorothea	k1gFnSc7	Dorothea
R.	R.	kA	R.
Hurley	Hurlea	k1gFnPc4	Hurlea
s	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
4	[number]	k4	4
děti	dítě	k1gFnPc1	dítě
(	(	kIx(	(
<g/>
Stephanie	Stephanie	k1gFnSc1	Stephanie
Rose	Ros	k1gMnSc2	Ros
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
;	;	kIx,	;
Jesse	Jesse	k1gFnSc2	Jesse
James	James	k1gMnSc1	James
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
;	;	kIx,	;
Jacob	Jacoba	k1gFnPc2	Jacoba
Hurley	Hurlea	k1gFnSc2	Hurlea
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
a	a	k8xC	a
Romeo	Romeo	k1gMnSc1	Romeo
Jon	Jon	k1gMnSc1	Jon
<g/>
,	,	kIx,	,
nar	nar	kA	nar
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
hudebními	hudební	k2eAgInPc7d1	hudební
vzory	vzor	k1gInPc7	vzor
jsou	být	k5eAaImIp3nP	být
Frank	frank	k1gInSc4	frank
Sinatra	Sinatrum	k1gNnSc2	Sinatrum
a	a	k8xC	a
Bruce	Bruce	k1gFnSc2	Bruce
Springsteen	Springstena	k1gFnPc2	Springstena
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
vydal	vydat	k5eAaPmAgInS	vydat
tři	tři	k4xCgNnPc1	tři
sólová	sólový	k2eAgNnPc1d1	sólové
alba	album	k1gNnPc1	album
a	a	k8xC	a
dvacet	dvacet	k4xCc4	dvacet
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
k	k	k7c3	k
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
dni	den	k1gInSc3	den
prodala	prodat	k5eAaPmAgFnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
130	[number]	k4	130
milionů	milion	k4xCgInPc2	milion
nosičů	nosič	k1gInPc2	nosič
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
světově	světově	k6eAd1	světově
nejprodávanějších	prodávaný	k2eAgMnPc2d3	nejprodávanější
hudebních	hudební	k2eAgMnPc2d1	hudební
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
Jon	Jon	k1gFnSc3	Jon
Bon	bona	k1gFnPc2	bona
Jovi	Jovi	k1gNnSc2	Jovi
věnuje	věnovat	k5eAaImIp3nS	věnovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
také	také	k9	také
herectví	herectví	k1gNnSc1	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
jako	jako	k8xC	jako
Moonlight	Moonlight	k1gInSc1	Moonlight
and	and	k?	and
Valentino	Valentina	k1gFnSc5	Valentina
a	a	k8xC	a
U	u	k7c2	u
-	-	kIx~	-
571	[number]	k4	571
a	a	k8xC	a
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
i	i	k9	i
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
či	či	k8xC	či
Ally	All	k2eAgFnPc4d1	Alla
McBealová	McBealový	k2eAgNnPc4d1	McBealový
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
skladatel	skladatel	k1gMnSc1	skladatel
byl	být	k5eAaImAgMnS	být
Jon	Jon	k1gFnSc3	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
"	"	kIx"	"
<g/>
Songwriters	Songwritersa	k1gFnPc2	Songwritersa
Hall	Hallum	k1gNnPc2	Hallum
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Jon	Jon	k1gFnSc1	Jon
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
zařadil	zařadit	k5eAaPmAgInS	zařadit
na	na	k7c4	na
50	[number]	k4	50
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
časopisu	časopis	k1gInSc2	časopis
"	"	kIx"	"
<g/>
Power	Power	k1gInSc1	Power
100	[number]	k4	100
<g/>
"	"	kIx"	"
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejvýkonnější	výkonný	k2eAgMnSc1d3	nejvýkonnější
a	a	k8xC	a
nejvlivnější	vlivný	k2eAgMnSc1d3	nejvlivnější
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
byznysu	byznys	k1gInSc6	byznys
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
jej	on	k3xPp3gMnSc4	on
časopis	časopis	k1gInSc1	časopis
People	People	k1gMnSc1	People
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
50	[number]	k4	50
nejkrásnějších	krásný	k2eAgMnPc2d3	nejkrásnější
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
stejný	stejný	k2eAgInSc1d1	stejný
časopis	časopis	k1gInSc1	časopis
jej	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgInS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nejvíce	hodně	k6eAd3	hodně
sexy	sex	k1gInPc4	sex
rockovou	rockový	k2eAgFnSc4d1	rocková
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
"	"	kIx"	"
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
také	také	k9	také
třináctý	třináctý	k4xOgMnSc1	třináctý
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
VH1	VH1	k1gFnSc2	VH1
"	"	kIx"	"
<g/>
100	[number]	k4	100
nejvíce	hodně	k6eAd3	hodně
sexy	sex	k1gInPc1	sex
umělců	umělec	k1gMnPc2	umělec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgMnS	být
Jon	Jon	k1gMnSc1	Jon
Bon	bon	k1gInSc4	bon
Jovi	Jov	k1gFnSc2	Jov
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
a	a	k8xC	a
většinovým	většinový	k2eAgMnSc7d1	většinový
vlastníkem	vlastník	k1gMnSc7	vlastník
týmu	tým	k1gInSc2	tým
Arena	Aren	k1gInSc2	Aren
Football	Footballa	k1gFnPc2	Footballa
League	League	k1gFnSc1	League
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Soul	Soul	k1gInSc1	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zakladatelem	zakladatel	k1gMnSc7	zakladatel
The	The	k1gFnSc2	The
Jon	Jon	k1gFnSc2	Jon
Bon	bona	k1gFnPc2	bona
Jovi	Jov	k1gFnSc2	Jov
Soul	Soul	k1gInSc1	Soul
Foundation	Foundation	k1gInSc1	Foundation
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
a	a	k8xC	a
věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
boji	boj	k1gInSc3	boj
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedou	vést	k5eAaImIp3nP	vést
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
jednotlivce	jednotlivec	k1gMnSc4	jednotlivec
do	do	k7c2	do
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
zoufalství	zoufalství	k1gNnSc2	zoufalství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
podporoval	podporovat	k5eAaImAgInS	podporovat
kandidáta	kandidát	k1gMnSc4	kandidát
demokratů	demokrat	k1gMnPc2	demokrat
Ala	ala	k1gFnSc1	ala
Gora	Gora	k1gFnSc1	Gora
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Kerryho	Kerry	k1gMnSc2	Kerry
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
Baracka	Baracka	k1gFnSc1	Baracka
Obamu	Obam	k1gInSc2	Obam
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
jej	on	k3xPp3gMnSc4	on
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
přizval	přizvat	k5eAaPmAgMnS	přizvat
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
komunitní	komunitní	k2eAgNnSc4d1	komunitní
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
mu	on	k3xPp3gNnSc3	on
také	také	k9	také
udělen	udělen	k2eAgInSc1d1	udělen
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
humanitních	humanitní	k2eAgFnPc2d1	humanitní
studií	studie	k1gFnPc2	studie
z	z	k7c2	z
Monmouth	Monmoutha	k1gFnPc2	Monmoutha
University	universita	k1gFnSc2	universita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Bon	bon	k1gInSc4	bon
Jovi	Jovi	k1gNnPc7	Jovi
<g/>
:	:	kIx,	:
1984	[number]	k4	1984
-	-	kIx~	-
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc1	Jovi
1985	[number]	k4	1985
-	-	kIx~	-
7800	[number]	k4	7800
<g/>
°	°	k?	°
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
1986	[number]	k4	1986
-	-	kIx~	-
Slippery	Slippera	k1gFnSc2	Slippera
When	When	k1gInSc1	When
Wet	Wet	k1gFnSc4	Wet
1988	[number]	k4	1988
-	-	kIx~	-
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
1992	[number]	k4	1992
-	-	kIx~	-
Keep	Keep	k1gMnSc1	Keep
The	The	k1gMnSc1	The
Faith	Faith	k1gMnSc1	Faith
1994	[number]	k4	1994
-	-	kIx~	-
Crossroad	Crossroad	k1gInSc1	Crossroad
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
Of	Of	k1gFnPc2	Of
Bon	bon	k1gInSc4	bon
Jovi	Jove	k1gFnSc4	Jove
1995	[number]	k4	1995
-	-	kIx~	-
These	these	k1gFnSc1	these
Days	Days	k1gInSc1	Days
2000	[number]	k4	2000
-	-	kIx~	-
Crush	Crush	k1gInSc1	Crush
2001	[number]	k4	2001
-	-	kIx~	-
One	One	k1gMnSc1	One
Wild	Wild	k1gMnSc1	Wild
Night	Night	k1gMnSc1	Night
<g />
.	.	kIx.	.
</s>
<s>
Live	Live	k1gInSc1	Live
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
2002	[number]	k4	2002
-	-	kIx~	-
Bounce	Bounec	k1gInSc2	Bounec
2003	[number]	k4	2003
-	-	kIx~	-
This	This	k1gInSc4	This
Left	Left	k2eAgInSc4d1	Left
Feels	Feels	k1gInSc4	Feels
Right	Right	k1gInSc1	Right
2004	[number]	k4	2004
-	-	kIx~	-
100,000,000	[number]	k4	100,000,000
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
Fans	Fansa	k1gFnPc2	Fansa
Can	Can	k1gFnSc7	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Wrong	Wrong	k1gMnSc1	Wrong
<g/>
...	...	k?	...
2005	[number]	k4	2005
-	-	kIx~	-
Have	Hav	k1gInSc2	Hav
A	a	k9	a
Nice	Nice	k1gFnSc1	Nice
Day	Day	k1gFnSc1	Day
2007	[number]	k4	2007
-	-	kIx~	-
Lost	Lost	k1gInSc1	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
2009	[number]	k4	2009
-	-	kIx~	-
The	The	k1gFnPc2	The
Circle	Circle	k1gNnPc2	Circle
2010	[number]	k4	2010
-	-	kIx~	-
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
2013	[number]	k4	2013
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
What	What	k2eAgInSc1d1	What
About	About	k1gInSc1	About
Now	Now	k1gFnSc1	Now
2015	[number]	k4	2015
-	-	kIx~	-
Burning	Burning	k1gInSc1	Burning
Bridges	Bridges	k1gInSc1	Bridges
2016	[number]	k4	2016
-	-	kIx~	-
This	This	k1gInSc1	This
House	house	k1gNnSc1	house
Is	Is	k1gFnPc2	Is
Not	nota	k1gFnPc2	nota
for	forum	k1gNnPc2	forum
Sale	Sal	k1gInSc2	Sal
Sólová	sólový	k2eAgFnSc1d1	sólová
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
:	:	kIx,	:
1990	[number]	k4	1990
-	-	kIx~	-
Blaze	blaze	k6eAd1	blaze
of	of	k?	of
Glory	Glora	k1gFnPc4	Glora
1997	[number]	k4	1997
-	-	kIx~	-
Destination	Destination	k1gInSc1	Destination
Anywhere	Anywher	k1gInSc5	Anywher
2001	[number]	k4	2001
-	-	kIx~	-
The	The	k1gFnSc1	The
Power	Power	k1gInSc4	Power
Station	station	k1gInSc1	station
Years	Years	k1gInSc1	Years
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Unreleased	Unreleased	k1gInSc4	Unreleased
Recordings	Recordings	k1gInSc1	Recordings
Filmografie	filmografie	k1gFnSc1	filmografie
<g/>
:	:	kIx,	:
1990	[number]	k4	1990
-	-	kIx~	-
Mladé	mladý	k2eAgFnSc2d1	mladá
pušky	puška	k1gFnSc2	puška
2	[number]	k4	2
1995	[number]	k4	1995
-	-	kIx~	-
Moonlight	Moonlight	k1gInSc1	Moonlight
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Valentino	Valentina	k1gFnSc5	Valentina
1996	[number]	k4	1996
-	-	kIx~	-
Muž	muž	k1gMnSc1	muž
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
1997	[number]	k4	1997
-	-	kIx~	-
Destination	Destination	k1gInSc1	Destination
Anywhere	Anywher	k1gInSc5	Anywher
1997	[number]	k4	1997
-	-	kIx~	-
Little	Little	k1gFnSc1	Little
city	city	k1gNnSc1	city
1998	[number]	k4	1998
-	-	kIx~	-
Homegrown	Homegrown	k1gInSc1	Homegrown
1998	[number]	k4	1998
-	-	kIx~	-
Neohlížej	ohlížet	k5eNaImRp2nS	ohlížet
se	s	k7c7	s
1998	[number]	k4	1998
-	-	kIx~	-
Row	Row	k1gMnSc1	Row
Your	Your	k1gMnSc1	Your
Boat	Boat	k1gMnSc1	Boat
1999	[number]	k4	1999
-	-	kIx~	-
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
2000	[number]	k4	2000
-	-	kIx~	-
U-571	U-571	k1gFnSc1	U-571
2000	[number]	k4	2000
-	-	kIx~	-
Pay	Pay	k1gMnSc1	Pay
It	It	k1gMnSc1	It
Forward	Forward	k1gMnSc1	Forward
2001-2002	[number]	k4	2001-2002
-	-	kIx~	-
Ally	Ally	k1gInPc1	Ally
McBealová	McBealová	k1gFnSc1	McBealová
2002	[number]	k4	2002
-	-	kIx~	-
Upíři	upír	k1gMnPc1	upír
2005	[number]	k4	2005
-	-	kIx~	-
Cry	Cry	k1gMnSc1	Cry
Wolf	Wolf	k1gMnSc1	Wolf
2006	[number]	k4	2006
-	-	kIx~	-
National	National	k1gMnSc1	National
Lampoon	Lampoon	k1gMnSc1	Lampoon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Pucked	Pucked	k1gInSc1	Pucked
2006	[number]	k4	2006
-	-	kIx~	-
The	The	k1gMnSc1	The
West	West	k1gMnSc1	West
Wing	Wing	k1gMnSc1	Wing
2010	[number]	k4	2010
-	-	kIx~	-
30	[number]	k4	30
Rock	rock	k1gInSc1	rock
2011	[number]	k4	2011
-	-	kIx~	-
Šťastný	šťastný	k2eAgInSc1d1	šťastný
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
</s>
