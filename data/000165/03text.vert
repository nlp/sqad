<s>
Béatrice	Béatrika	k1gFnSc3	Béatrika
Martinová	Martinová	k1gFnSc1	Martinová
<g/>
,	,	kIx,	,
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Cœ	Cœ	k1gFnSc2	Cœ
de	de	k?	de
pirate	pirat	k1gMnSc5	pirat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kanadská	kanadský	k2eAgFnSc1d1	kanadská
indie	indie	k1gFnSc1	indie
popová	popový	k2eAgFnSc1d1	popová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
22	[number]	k4	22
<g/>
.	.	kIx.	.
záři	zář	k1gFnSc6	zář
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
quebecká	quebecký	k2eAgFnSc1d1	quebecká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
již	již	k6eAd1	již
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
tří	tři	k4xCgInPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
December	December	k1gMnSc1	December
Strikes	Strikes	k1gMnSc1	Strikes
First	First	k1gMnSc1	First
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
indie-popové	indieopový	k2eAgFnSc6d1	indie-popová
skupině	skupina	k1gFnSc6	skupina
Bonjour	Bonjoura	k1gFnPc2	Bonjoura
Brumaire	brumaire	k1gInSc4	brumaire
z	z	k7c2	z
Montrealu	Montreal	k1gInSc2	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Béatrice	Béatrika	k1gFnSc3	Béatrika
Martin	Martina	k1gFnPc2	Martina
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pózovala	pózovat	k5eAaImAgFnS	pózovat
nahá	nahý	k2eAgFnSc1d1	nahá
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
nezletilá	zletilý	k2eNgFnSc1d1	nezletilá
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
600	[number]	k4	600
jejích	její	k3xOp3gFnPc2	její
fotek	fotka	k1gFnPc2	fotka
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejněn	k2eAgNnSc1d1	zveřejněno
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
alt-porn	altorn	k1gNnSc4	alt-porn
webové	webový	k2eAgFnSc6d1	webová
stránce	stránka	k1gFnSc6	stránka
GodsGirls	GodsGirlsa	k1gFnPc2	GodsGirlsa
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
pózovala	pózovat	k5eAaImAgFnS	pózovat
jako	jako	k9	jako
nahá	nahý	k2eAgFnSc1d1	nahá
modelka	modelka	k1gFnSc1	modelka
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
Coeur	Coeur	k1gMnSc1	Coeur
de	de	k?	de
pirate	pirat	k1gInSc5	pirat
(	(	kIx(	(
<g/>
album	album	k1gNnSc4	album
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
text	text	k1gInSc1	text
-	-	kIx~	-
Béatrice	Béatrika	k1gFnSc6	Béatrika
Martin	Martina	k1gFnPc2	Martina
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Le	Le	k1gMnSc1	Le
long	long	k1gMnSc1	long
du	du	k?	du
large	large	k1gFnSc1	large
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Comme	Comm	k1gMnSc5	Comm
des	des	k1gNnPc6	des
enfants	enfants	k1gInSc4	enfants
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Fondu	fond	k1gInSc2	fond
au	au	k0	au
noir	noir	k1gInSc4	noir
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Corbeau	Corbeaa	k1gFnSc4	Corbeaa
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Berceuse	Berceuse	k1gFnSc1	Berceuse
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Intermission	Intermission	k1gInSc1	Intermission
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Printemps	Printemps	k1gInSc1	Printemps
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Ensemble	ensemble	k1gInSc1	ensemble
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
vie	vie	k?	vie
est	est	k?	est
ailleurs	ailleurs	k1gInSc1	ailleurs
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Pour	Pour	k1gMnSc1	Pour
un	un	k?	un
infidè	infidè	k?	infidè
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
duet	duet	k1gInSc1	duet
s	s	k7c7	s
Jimmy	Jimm	k1gMnPc7	Jimm
Hunt	hunt	k1gInSc4	hunt
<g/>
,	,	kIx,	,
kanadská	kanadský	k2eAgFnSc1d1	kanadská
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
a	a	k8xC	a
Julien	Julien	k1gInSc1	Julien
Doré	Dorá	k1gFnSc2	Dorá
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
<g />
.	.	kIx.	.
</s>
<s>
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Francis	Francis	k1gFnSc1	Francis
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
était	était	k1gMnSc1	était
salement	salement	k1gMnSc1	salement
romantique	romantiquat	k5eAaPmIp3nS	romantiquat
<g/>
"	"	kIx"	"
Blonde	blond	k1gInSc5	blond
(	(	kIx(	(
<g/>
album	album	k1gNnSc4	album
-	-	kIx~	-
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Trauma	trauma	k1gNnSc1	trauma
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
-	-	kIx~	-
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
http://www.coeurdepirate.com/	[url]	k?	http://www.coeurdepirate.com/
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
http://blogdepirate.artistes.universalmusic.fr/site/#/actus	[url]	k1gMnSc1	http://blogdepirate.artistes.universalmusic.fr/site/#/actus
-	-	kIx~	-
Universal	Universal	k1gMnSc1	Universal
Music	Music	k1gMnSc1	Music
http://www.myspace.com/coeurdepirate	[url]	k1gInSc5	http://www.myspace.com/coeurdepirate
-	-	kIx~	-
MySpace	MySpace	k1gFnSc1	MySpace
-	-	kIx~	-
poslech	poslech	k1gInSc1	poslech
http://www.myspace.com/pearlsrules	[url]	k1gInSc1	http://www.myspace.com/pearlsrules
-	-	kIx~	-
MySpace	MySpace	k1gFnSc1	MySpace
-	-	kIx~	-
poslech	poslech	k1gInSc1	poslech
</s>
