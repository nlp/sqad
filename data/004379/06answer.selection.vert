<s>
Zeyerovo	Zeyerův	k2eAgNnSc4d1	Zeyerovo
dramatické	dramatický	k2eAgNnSc4d1	dramatické
dílo	dílo	k1gNnSc4	dílo
ale	ale	k8xC	ale
často	často	k6eAd1	často
zlákalo	zlákat	k5eAaPmAgNnS	zlákat
hudebníky	hudebník	k1gMnPc4	hudebník
–	–	k?	–
k	k	k7c3	k
Radúzovi	Radúz	k1gMnSc3	Radúz
a	a	k8xC	a
Mahuleně	Mahulena	k1gFnSc3	Mahulena
složil	složit	k5eAaPmAgMnS	složit
hudbu	hudba	k1gFnSc4	hudba
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
si	se	k3xPyFc3	se
zase	zase	k9	zase
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Šárku	Šárka	k1gFnSc4	Šárka
jako	jako	k8xS	jako
libreto	libreto	k1gNnSc4	libreto
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
a	a	k8xC	a
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Kunálovy	Kunálův	k2eAgFnSc2d1	Kunálův
oči	oko	k1gNnPc4	oko
složil	složit	k5eAaPmAgMnS	složit
operu	opera	k1gFnSc4	opera
Otakar	Otakar	k1gMnSc1	Otakar
Ostrčil	Ostrčil	k1gMnSc1	Ostrčil
<g/>
.	.	kIx.	.
</s>
