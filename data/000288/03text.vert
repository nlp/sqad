<s>
Radiohead	Radiohead	k6eAd1	Radiohead
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
alternativní	alternativní	k2eAgFnSc1d1	alternativní
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Oxfordshire	Oxfordshir	k1gInSc5	Oxfordshir
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Thom	Tho	k1gNnSc7	Tho
Yorke	Yorke	k1gNnSc2	Yorke
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jonny	Jonn	k1gInPc1	Jonn
Greenwood	Greenwood	k1gInSc1	Greenwood
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc1d1	hlavní
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
nástroje	nástroj	k1gInPc4	nástroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ed	Ed	k1gFnSc1	Ed
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k2eAgInSc4d1	Brien
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Colin	Colin	k2eAgInSc1d1	Colin
Greenwood	Greenwood	k1gInSc1	Greenwood
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
syntezátor	syntezátor	k1gInSc1	syntezátor
<g/>
)	)	kIx)	)
a	a	k8xC	a
Phil	Phil	k1gMnSc1	Phil
Selway	Selwaa	k1gFnSc2	Selwaa
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Radiohead	Radiohead	k6eAd1	Radiohead
vydali	vydat	k5eAaPmAgMnP	vydat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
devět	devět	k4xCc1	devět
studiových	studiový	k2eAgFnPc2d1	studiová
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
jich	on	k3xPp3gMnPc2	on
skupina	skupina	k1gFnSc1	skupina
prodala	prodat	k5eAaPmAgFnS	prodat
přes	přes	k7c4	přes
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Radiohead	Radiohead	k1gInSc4	Radiohead
vydali	vydat	k5eAaPmAgMnP	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
Creep	Creep	k1gInSc4	Creep
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Pablo	Pablo	k1gNnSc1	Pablo
Honey	Honea	k1gFnSc2	Honea
následovalo	následovat	k5eAaImAgNnS	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Creep	Creep	k1gInSc1	Creep
nebyla	být	k5eNaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
hitem	hit	k1gInSc7	hit
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
až	až	k9	až
po	po	k7c6	po
dotisku	dotisk	k1gInSc6	dotisk
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
začínala	začínat	k5eAaImAgFnS	začínat
být	být	k5eAaImF	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
skupina	skupina	k1gFnSc1	skupina
jednoho	jeden	k4xCgInSc2	jeden
hitu	hit	k1gInSc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
Radiohead	Radiohead	k1gInSc1	Radiohead
se	se	k3xPyFc4	se
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
druhého	druhý	k4xOgNnSc2	druhý
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Bends	Bendsa	k1gFnPc2	Bendsa
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vyvážená	vyvážený	k2eAgFnSc1d1	vyvážená
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
,	,	kIx,	,
naplněná	naplněný	k2eAgFnSc1d1	naplněná
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
barevností	barevnost	k1gFnSc7	barevnost
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
Yorkovým	Yorkův	k2eAgInSc7d1	Yorkův
falzetem	falzet	k1gInSc7	falzet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vřele	vřele	k6eAd1	vřele
přijato	přijmout	k5eAaPmNgNnS	přijmout
jak	jak	k8xC	jak
kritiky	kritika	k1gFnPc4	kritika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
především	především	k9	především
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
deska	deska	k1gFnSc1	deska
OK	oka	k1gFnPc2	oka
Computer	computer	k1gInSc1	computer
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
skupině	skupina	k1gFnSc6	skupina
zajistila	zajistit	k5eAaPmAgFnS	zajistit
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
rozpínavého	rozpínavý	k2eAgInSc2d1	rozpínavý
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
tématem	téma	k1gNnSc7	téma
odcizení	odcizení	k1gNnSc2	odcizení
se	se	k3xPyFc4	se
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
zásadní	zásadní	k2eAgInSc1d1	zásadní
mezník	mezník	k1gInSc1	mezník
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vydáním	vydání	k1gNnSc7	vydání
alb	alba	k1gFnPc2	alba
Kid	Kid	k1gMnSc2	Kid
A	A	kA	A
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
Amnesiac	Amnesiac	k1gFnSc1	Amnesiac
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Radiohead	Radiohead	k1gInSc4	Radiohead
vrcholu	vrchol	k1gInSc2	vrchol
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
na	na	k7c4	na
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
snášely	snášet	k5eAaImAgFnP	snášet
velmi	velmi	k6eAd1	velmi
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
ohlasy	ohlas	k1gInPc1	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
tvůrčím	tvůrčí	k2eAgNnSc6d1	tvůrčí
období	období	k1gNnSc6	období
prochází	procházet	k5eAaImIp3nS	procházet
Radiohead	Radiohead	k1gInSc4	Radiohead
změnou	změna	k1gFnSc7	změna
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
hudby	hudba	k1gFnSc2	hudba
začínají	začínat	k5eAaImIp3nP	začínat
více	hodně	k6eAd2	hodně
začleňovat	začleňovat	k5eAaImF	začleňovat
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc4	prvek
krautrocku	krautrock	k1gInSc2	krautrock
a	a	k8xC	a
jazzu	jazz	k1gInSc2	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Hail	Hainout	k5eAaPmAgInS	Hainout
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Thief	Thief	k1gInSc1	Thief
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
míchající	míchající	k2eAgFnSc4d1	míchající
kytarovou	kytarový	k2eAgFnSc4d1	kytarová
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
elektronickou	elektronický	k2eAgFnSc7d1	elektronická
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jejich	jejich	k3xOp3gNnSc7	jejich
posledním	poslední	k2eAgNnSc7d1	poslední
albem	album	k1gNnSc7	album
vydaným	vydaný	k2eAgFnPc3d1	vydaná
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
společnosti	společnost	k1gFnSc2	společnost
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Heptalogii	Heptalogie	k1gFnSc4	Heptalogie
alb	alba	k1gFnPc2	alba
zatím	zatím	k6eAd1	zatím
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
deska	deska	k1gFnSc1	deska
In	In	k1gFnSc2	In
Rainbows	Rainbowsa	k1gFnPc2	Rainbowsa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
toto	tento	k3xDgNnSc4	tento
album	album	k1gNnSc4	album
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
internet	internet	k1gInSc4	internet
a	a	k8xC	a
potenciální	potenciální	k2eAgMnPc1d1	potenciální
kupci	kupec	k1gMnPc1	kupec
za	za	k7c2	za
něj	on	k3xPp3gMnSc4	on
mohli	moct	k5eAaImAgMnP	moct
zaplatit	zaplatit	k5eAaPmF	zaplatit
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
částku	částka	k1gFnSc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
mohli	moct	k5eAaImAgMnP	moct
stáhnout	stáhnout	k5eAaPmF	stáhnout
zcela	zcela	k6eAd1	zcela
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
za	za	k7c4	za
něj	on	k3xPp3gNnSc4	on
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
počin	počin	k1gInSc1	počin
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
jak	jak	k8xC	jak
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Thom	Thom	k1gInSc1	Thom
Yorke	Yorke	k1gInSc1	Yorke
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
Ed	Ed	k1gFnSc2	Ed
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k2eAgInSc4d1	Brien
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Jonny	Jonna	k1gFnSc2	Jonna
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
Colin	Colina	k1gFnPc2	Colina
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Phil	Phila	k1gFnPc2	Phila
Selway	Selwaa	k1gFnSc2	Selwaa
-	-	kIx~	-
bicí	bicí	k2eAgInPc1d1	bicí
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Radiohead	Radiohead	k1gInSc4	Radiohead
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
Radiohead	Radiohead	k1gInSc4	Radiohead
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
kapele	kapela	k1gFnSc6	kapela
</s>
