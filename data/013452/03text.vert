<p>
<s>
Vovča	Vovča	k1gFnSc1	Vovča
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
В	В	k?	В
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Doněcké	doněcký	k2eAgFnSc6d1	Doněcká
a	a	k8xC	a
Dněpropetrovské	Dněpropetrovský	k2eAgFnSc6d1	Dněpropetrovská
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
323	[number]	k4	323
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
13	[number]	k4	13
300	[number]	k4	300
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Ústí	ústí	k1gNnSc1	ústí
zleva	zleva	k6eAd1	zleva
do	do	k7c2	do
Samary	Samara	k1gFnSc2	Samara
(	(	kIx(	(
<g/>
povodí	povodí	k1gNnSc2	povodí
Dněpru	Dněpr	k1gInSc2	Dněpr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
81	[number]	k4	81
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
činí	činit	k5eAaImIp3nS	činit
5,3	[number]	k4	5,3
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
leží	ležet	k5eAaImIp3nS	ležet
města	město	k1gNnPc1	město
Kurachivka	Kurachivka	k1gFnSc1	Kurachivka
<g/>
,	,	kIx,	,
Kurachove	Kurachov	k1gInSc5	Kurachov
a	a	k8xC	a
Pavlohrad	Pavlohrada	k1gFnPc2	Pavlohrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
В	В	k?	В
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vovča	Vovč	k1gInSc2	Vovč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
