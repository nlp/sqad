<s>
Skandium	skandium	k1gNnSc1
</s>
<s>
Skandium	skandium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Ar	ar	k1gInSc1
<g/>
]	]	kIx)
3	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
4	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
Sc	Sc	kA
</s>
<s>
21	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Skandium	skandium	k1gNnSc1
<g/>
,	,	kIx,
Sc	Sc	k1gFnSc1
<g/>
,	,	kIx,
21	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Scandium	Scandium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
d	d	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
5	#num#	k4
až	až	k6eAd1
22	#num#	k4
ppm	ppm	k?
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
</s>
<s>
0,00004	0,00004	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l	l	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
a	a	k8xC
výrazně	výrazně	k6eAd1
lehký	lehký	k2eAgInSc1d1
kovov	kovov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-20-2	7440-20-2	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
44,955	44,955	k4
<g/>
9	#num#	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
162	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
170	#num#	k4
pm	pm	k?
</s>
<s>
Van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Waalsův	Waalsův	k2eAgMnSc1d1
poloměr	poloměr	k1gInSc4
</s>
<s>
211	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
81	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Ar	ar	k1gInSc1
<g/>
]	]	kIx)
3	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
4	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
+	+	kIx~
<g/>
III	III	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,36	1,36	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
633,1	633,1	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1235,0	1235,0	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
2388	#num#	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
Šesterečná	šesterečný	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
15,00	15,00	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
2,985	2,985	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
Pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
2,5	2,5	k4
</s>
<s>
Tlak	tlak	k1gInSc1
syté	sytý	k2eAgFnSc2d1
páry	pára	k1gFnSc2
</s>
<s>
100	#num#	k4
Pa	Pa	kA
při	při	k7c6
2006K	2006K	k4
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
15,8	15,8	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
1540,85	1540,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
814	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2835,85	2835,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
109	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
14,1	14,1	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
332,7	332,7	k4
KJ	KJ	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
25,52	25,52	k4
Jmol	Jmol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
568	#num#	k4
Jkg	Jkg	k1gFnSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
1,77	1,77	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
562	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
-2,03	-2,03	k4
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R11	R11	k4
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S	s	k7c7
<g/>
16	#num#	k4
<g/>
,	,	kIx,
S43	S43	k1gFnSc1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
44	#num#	k4
<g/>
Sc	Sc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
58,61	58,61	k4
hod	hod	k1gInSc1
</s>
<s>
ε	ε	k?
β	β	k?
<g/>
+	+	kIx~
</s>
<s>
3,923	3,923	k4
9	#num#	k4
</s>
<s>
44	#num#	k4
<g/>
Ca	ca	kA
</s>
<s>
γ	γ	k?
</s>
<s>
0,271	0,271	k4
24	#num#	k4
</s>
<s>
44	#num#	k4
<g/>
Ca	ca	kA
</s>
<s>
45	#num#	k4
<g/>
Sc	Sc	k1gFnSc1
</s>
<s>
100	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
je	být	k5eAaImIp3nS
stabilní	stabilní	k2eAgMnSc1d1
s	s	k7c7
24	#num#	k4
neutrony	neutron	k1gInPc7
</s>
<s>
46	#num#	k4
<g/>
Sc	Sc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
83,79	83,79	k4
dne	den	k1gInSc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,3569	0,3569	k4
</s>
<s>
46	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,889	0,889	k4
</s>
<s>
46	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
</s>
<s>
47	#num#	k4
<g/>
Sc	Sc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
3,349	3,349	k4
<g/>
2	#num#	k4
dne	den	k1gInSc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,44	0,44	k4
</s>
<s>
47	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
</s>
<s>
γ	γ	k?
</s>
<s>
1,159	1,159	k4
</s>
<s>
47	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
</s>
<s>
48	#num#	k4
<g/>
Sc	Sc	k1gFnSc1
</s>
<s>
umělý	umělý	k2eAgInSc4d1
</s>
<s>
46,67	46,67	k4
hod	hod	k1gInSc1
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,661	0,661	k4
</s>
<s>
48	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
</s>
<s>
γ	γ	k?
</s>
<s>
0,9	0,9	k4
</s>
<s>
48	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vápník	vápník	k1gInSc1
≺	≺	k?
<g/>
Sc	Sc	k1gFnSc2
<g/>
≻	≻	k?
Titan	titan	k1gInSc1
</s>
<s>
⋎	⋎	k?
<g/>
Y	Y	kA
</s>
<s>
Skandium	skandium	k1gNnSc1
ve	v	k7c6
zkumavce	zkumavka	k1gFnSc6
</s>
<s>
Skandium	skandium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Sc	Sc	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Scandium	Scandium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
elektropozitivní	elektropozitivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxidační	oxidační	k2eAgInSc4d1
číslo	číslo	k1gNnSc1
skandia	skandium	k1gNnSc2
ve	v	k7c6
většině	většina	k1gFnSc6
sloučenin	sloučenina	k1gFnPc2
je	být	k5eAaImIp3nS
+3	+3	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průmyslové	průmyslový	k2eAgInPc1d1
uplatnění	uplatnění	k1gNnSc4
skandia	skandium	k1gNnSc2
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
malé	malý	k2eAgNnSc1d1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
využití	využití	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
při	při	k7c6
výrobě	výroba	k1gFnSc6
světelných	světelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Skandium	skandium	k1gNnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
prvky	prvek	k1gInPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc1
existenci	existence	k1gFnSc4
předpověděl	předpovědět	k5eAaPmAgMnS
ruský	ruský	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
a	a	k8xC
tvůrce	tvůrce	k1gMnSc1
periodické	periodický	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
prvků	prvek	k1gInPc2
Dmitrij	Dmitrij	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
Mendělejev	Mendělejev	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1869	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
článek	článek	k1gInSc4
o	o	k7c6
předkládaných	předkládaný	k2eAgFnPc6d1
vlastnostech	vlastnost	k1gFnPc6
doposud	doposud	k6eAd1
neobjeveného	objevený	k2eNgInSc2d1
prvku	prvek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nazval	nazvat	k5eAaPmAgInS,k5eAaBmAgInS
ekabor	ekabor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Objev	objev	k1gInSc4
skandia	skandium	k1gNnSc2
učinil	učinit	k5eAaPmAgMnS,k5eAaImAgMnS
švédský	švédský	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
Lars	Larsa	k1gFnPc2
Fredrick	Fredrick	k1gMnSc1
Nilson	Nilson	k1gMnSc1
roku	rok	k1gInSc2
1879	#num#	k4
pomocí	pomocí	k7c2
spektrální	spektrální	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
spektru	spektrum	k1gNnSc6
směsi	směs	k1gFnSc2
prvků	prvek	k1gInPc2
vzácných	vzácný	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
z	z	k7c2
minerálů	minerál	k1gInPc2
euxtenitu	euxtenit	k1gInSc2
a	a	k8xC
gadolinitu	gadolinit	k1gInSc2
objevil	objevit	k5eAaPmAgInS
doposud	doposud	k6eAd1
neznámé	známý	k2eNgFnSc2d1
spekrální	spekrální	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkladem	rozklad	k1gInSc7
těchto	tento	k3xDgInPc2
minerálů	minerál	k1gInPc2
a	a	k8xC
chemickým	chemický	k2eAgNnSc7d1
dělením	dělení	k1gNnSc7
vzniklé	vzniklý	k2eAgFnSc2d1
směsi	směs	k1gFnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
2	#num#	k4
g	g	kA
vysoce	vysoce	k6eAd1
čistého	čistý	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
skanditého	skanditý	k2eAgInSc2d1
(	(	kIx(
<g/>
Sc	Sc	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čisté	čistý	k2eAgNnSc1d1
kovové	kovový	k2eAgNnSc1d1
elementární	elementární	k2eAgNnSc1d1
skandium	skandium	k1gNnSc1
bylo	být	k5eAaImAgNnS
připraveno	připravit	k5eAaPmNgNnS
až	až	k9
roku	rok	k1gInSc2
1937	#num#	k4
elektrolýzou	elektrolýza	k1gFnSc7
taveniny	tavenina	k1gFnSc2
směsi	směs	k1gFnSc2
draslíku	draslík	k1gInSc2
<g/>
,	,	kIx,
lithia	lithium	k1gNnSc2
a	a	k8xC
chloridu	chlorid	k1gInSc2
skanditého	skanditý	k2eAgInSc2d1
ScCl	ScCl	k1gInSc1
<g/>
3	#num#	k4
při	při	k7c6
teplotě	teplota	k1gFnSc6
700	#num#	k4
<g/>
–	–	k?
<g/>
800	#num#	k4
°	°	k?
<g/>
C	C	kA
na	na	k7c6
wolframové	wolframový	k2eAgFnSc6d1
elektrodě	elektroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
kovové	kovový	k2eAgNnSc4d1
skandium	skandium	k1gNnSc4
</s>
<s>
Skandium	skandium	k1gNnSc1
je	být	k5eAaImIp3nS
stříbřitě	stříbřitě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
a	a	k8xC
výrazně	výrazně	k6eAd1
lehký	lehký	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
podobný	podobný	k2eAgInSc1d1
svými	svůj	k3xOyFgMnPc7
vlastnostmi	vlastnost	k1gFnPc7
hliníku	hliník	k1gInSc2
<g/>
,	,	kIx,
titanu	titan	k1gInSc2
a	a	k8xC
lanthanoidům	lanthanoid	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Chemicky	chemicky	k6eAd1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
stálé	stálý	k2eAgNnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
vzduchu	vzduch	k1gInSc6
se	se	k3xPyFc4
pozvolna	pozvolna	k6eAd1
pokrývá	pokrývat	k5eAaImIp3nS
vrstvičkou	vrstvička	k1gFnSc7
nažloutlého	nažloutlý	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
jej	on	k3xPp3gMnSc4
chrání	chránit	k5eAaImIp3nS
před	před	k7c7
další	další	k2eAgFnSc7d1
korozí	koroze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
odolné	odolný	k2eAgNnSc1d1
proti	proti	k7c3
působení	působení	k1gNnSc3
vlhkosti	vlhkost	k1gFnSc2
a	a	k8xC
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k6eAd1
odolává	odolávat	k5eAaImIp3nS
působení	působení	k1gNnSc4
oxidačních	oxidační	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Skandium	skandium	k1gNnSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
přírodě	příroda	k1gFnSc6
v	v	k7c6
relativně	relativně	k6eAd1
velkém	velký	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
5	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
značně	značně	k6eAd1
nízký	nízký	k2eAgInSc1d1
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
0,000	0,000	k4
04	#num#	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
l.	l.	k?
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
skandia	skandium	k1gNnSc2
přibližně	přibližně	k6eAd1
na	na	k7c4
1	#num#	k4
miliardu	miliarda	k4xCgFnSc4
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
neexistují	existovat	k5eNaImIp3nP
žádná	žádný	k3yNgNnPc1
velká	velký	k2eAgNnPc1d1
ložiska	ložisko	k1gNnPc1
rud	ruda	k1gFnPc2
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
skandia	skandium	k1gNnSc2
<g/>
,	,	kIx,
značné	značný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
a	a	k8xC
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
při	při	k7c6
zpracování	zpracování	k1gNnSc6
uranových	uranový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
nerost	nerost	k1gInSc4
obsahující	obsahující	k2eAgNnSc4d1
větší	veliký	k2eAgNnSc4d2
množství	množství	k1gNnSc4
skandia	skandium	k1gNnSc2
–	–	k?
thortveitit	thortveitit	k1gInSc1
Sc	Sc	k1gFnSc1
<g/>
2	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
největší	veliký	k2eAgNnSc4d3
naleziště	naleziště	k1gNnSc4
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
Norsku	Norsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
a	a	k8xC
použití	použití	k1gNnSc1
</s>
<s>
Jelikož	jelikož	k8xS
nemá	mít	k5eNaImIp3nS
skandium	skandium	k1gNnSc1
žádný	žádný	k3yNgInSc4
velký	velký	k2eAgInSc4d1
technický	technický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
,	,	kIx,
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
v	v	k7c6
malém	malý	k2eAgNnSc6d1
množství	množství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
se	se	k3xPyFc4
získává	získávat	k5eAaImIp3nS
z	z	k7c2
odpadu	odpad	k1gInSc2
při	při	k7c6
zpracování	zpracování	k1gNnSc6
uranových	uranový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
<g/>
,	,	kIx,
dalším	další	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
je	být	k5eAaImIp3nS
thortveitit	thortveitit	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
obsahuje	obsahovat	k5eAaImIp3nS
35	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
%	%	kIx~
oxidu	oxid	k1gInSc2
skanditého	skanditý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Skandium	skandium	k1gNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
výrobě	výroba	k1gFnSc6
vysoce	vysoce	k6eAd1
intenzivních	intenzivní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
46	#num#	k4
<g/>
Sc	Sc	k1gMnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
při	při	k7c6
rafinaci	rafinace	k1gFnSc6
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
využití	využití	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
slitinách	slitina	k1gFnPc6
s	s	k7c7
hliníkem	hliník	k1gInSc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
v	v	k7c6
leteckém	letecký	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
a	a	k8xC
při	při	k7c6
výrobě	výroba	k1gFnSc6
sportovního	sportovní	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
(	(	kIx(
<g/>
kola	kolo	k1gNnSc2
<g/>
,	,	kIx,
baseballové	baseballový	k2eAgFnSc2d1
pálky	pálka	k1gFnSc2
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
jako	jako	k9
konstrukční	konstrukční	k2eAgInSc1d1
kov	kov	k1gInSc1
v	v	k7c6
kosmonautice	kosmonautika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Izotopy	izotop	k1gInPc4
skandia	skandium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1
skandium	skandium	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
stabilní	stabilní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
45	#num#	k4
<g/>
Sc	Sc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
celkem	celkem	k6eAd1
25	#num#	k4
radioizotopů	radioizotop	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
</s>
<s>
nichž	jenž	k3xRgFnPc2
nejstabilnější	stabilní	k2eAgFnSc2d3
je	být	k5eAaImIp3nS
46	#num#	k4
<g/>
Sc	Sc	k1gFnPc2
(	(	kIx(
<g/>
poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc2
=	=	kIx~
83,79	83,79	k4
dnů	den	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Jursík	Jursík	k1gMnSc1
F.	F.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
nekovů	nekov	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7080-504-8	80-7080-504-8	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
skandium	skandium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
skandium	skandium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Chemický	chemický	k2eAgInSc1d1
vzdělávací	vzdělávací	k2eAgInSc1d1
portál	portál	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4051871-1	4051871-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5756	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85117977	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85117977	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
