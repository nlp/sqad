<p>
<s>
George	George	k1gInSc1	George
Smith	Smith	k1gInSc1	Smith
Patton	Patton	k1gInSc4	Patton
ml.	ml.	kA	ml.
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Gabriel	Gabriel	k1gMnSc1	Gabriel
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
čtyřhvězdičkový	čtyřhvězdičkový	k2eAgMnSc1d1	čtyřhvězdičkový
generál	generál	k1gMnSc1	generál
armády	armáda	k1gFnSc2	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
3	[number]	k4	3
<g/>
.	.	kIx.	.
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
velel	velet	k5eAaImAgInS	velet
<g/>
,	,	kIx,	,
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
mj.	mj.	kA	mj.
západ	západ	k1gInSc4	západ
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc4	jihozápad
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
měst	město	k1gNnPc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
Přeštic	Přeštice	k1gFnPc2	Přeštice
<g/>
,	,	kIx,	,
Rokycan	Rokycany	k1gInPc2	Rokycany
<g/>
,	,	kIx,	,
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
,	,	kIx,	,
Domažlic	Domažlice	k1gFnPc2	Domažlice
a	a	k8xC	a
Chebu	Cheb	k1gInSc2	Cheb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
historií	historie	k1gFnSc7	historie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
a	a	k8xC	a
prastrýc	prastrýc	k1gMnSc1	prastrýc
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
Americké	americký	k2eAgFnSc6d1	americká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
George	Georg	k1gFnSc2	Georg
Smith	Smith	k1gMnSc1	Smith
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generálem	generál	k1gMnSc7	generál
v	v	k7c6	v
jedenadvaceti	jedenadvacet	k4xCc6	jedenadvacet
letech	let	k1gInPc6	let
a	a	k8xC	a
padl	padnout	k5eAaImAgInS	padnout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
22	[number]	k4	22
<g/>
.	.	kIx.	.
virginského	virginský	k2eAgInSc2d1	virginský
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Prastrýc	prastrýc	k1gMnSc1	prastrýc
<g/>
,	,	kIx,	,
plukovník	plukovník	k1gMnSc1	plukovník
Waller	Waller	k1gMnSc1	Waller
Patton	Patton	k1gInSc4	Patton
<g/>
,	,	kIx,	,
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Gettysburgu	Gettysburg	k1gInSc2	Gettysburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
trpěl	trpět	k5eAaImAgMnS	trpět
dyslexií	dyslexie	k1gFnSc7	dyslexie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyrovnával	vyrovnávat	k5eAaImAgMnS	vyrovnávat
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
disciplínou	disciplína	k1gFnSc7	disciplína
a	a	k8xC	a
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
americké	americký	k2eAgFnSc6d1	americká
vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
ve	v	k7c4	v
West	West	k1gInSc4	West
Pointu	pointa	k1gFnSc4	pointa
tak	tak	k6eAd1	tak
urputnou	urputný	k2eAgFnSc7d1	urputná
snahou	snaha	k1gFnSc7	snaha
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
brilantních	brilantní	k2eAgInPc2d1	brilantní
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předposledním	předposlední	k2eAgInSc6d1	předposlední
ročníku	ročník	k1gInSc6	ročník
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
kadetů	kadet	k1gMnPc2	kadet
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
mu	on	k3xPp3gNnSc3	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
a	a	k8xC	a
v	v	k7c6	v
premiéře	premiéra	k1gFnSc6	premiéra
moderního	moderní	k2eAgInSc2d1	moderní
pětiboje	pětiboj	k1gInSc2	pětiboj
skončil	skončit	k5eAaPmAgInS	skončit
pátý	pátý	k4xOgInSc1	pátý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
zastánců	zastánce	k1gMnPc2	zastánce
tankové	tankový	k2eAgFnSc2d1	tanková
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
účastník	účastník	k1gMnSc1	účastník
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
náčelníkem	náčelník	k1gInSc7	náčelník
štábu	štáb	k1gInSc2	štáb
generála	generál	k1gMnSc2	generál
Pershinga	Pershing	k1gMnSc2	Pershing
<g/>
)	)	kIx)	)
záhy	záhy	k6eAd1	záhy
pochopil	pochopit	k5eAaPmAgInS	pochopit
důležitost	důležitost	k1gFnSc4	důležitost
tanků	tank	k1gInPc2	tank
a	a	k8xC	a
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
prosazení	prosazení	k1gNnSc4	prosazení
a	a	k8xC	a
zapracování	zapracování	k1gNnSc4	zapracování
do	do	k7c2	do
americké	americký	k2eAgFnSc2d1	americká
armádní	armádní	k2eAgFnSc2d1	armádní
výzbroje	výzbroj	k1gFnSc2	výzbroj
a	a	k8xC	a
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
zastával	zastávat	k5eAaImAgMnS	zastávat
významné	významný	k2eAgFnPc4d1	významná
funkce	funkce	k1gFnPc4	funkce
během	během	k7c2	během
amerických	americký	k2eAgFnPc2d1	americká
kampaní	kampaň	k1gFnPc2	kampaň
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
invaze	invaze	k1gFnSc1	invaze
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
polních	polní	k2eAgMnPc2d1	polní
velitelů	velitel	k1gMnPc2	velitel
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
brilantního	brilantní	k2eAgMnSc4d1	brilantní
taktika	taktik	k1gMnSc4	taktik
mobilní	mobilní	k2eAgFnSc2d1	mobilní
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
pověst	pověst	k1gFnSc4	pověst
ryzího	ryzí	k2eAgMnSc2d1	ryzí
a	a	k8xC	a
urputného	urputný	k2eAgMnSc2d1	urputný
válečníka	válečník	k1gMnSc2	válečník
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
přezdíváno	přezdívat	k5eAaImNgNnS	přezdívat
Stará	starý	k2eAgFnSc1d1	stará
krev	krev	k1gFnSc1	krev
a	a	k8xC	a
odvaha	odvaha	k1gFnSc1	odvaha
<g/>
,	,	kIx,	,
Krev	krev	k1gFnSc1	krev
a	a	k8xC	a
bláto	bláto	k1gNnSc1	bláto
nebo	nebo	k8xC	nebo
též	též	k9	též
Tankový	tankový	k2eAgInSc4d1	tankový
rozrážeč	rozrážeč	k1gInSc4	rozrážeč
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
velkou	velký	k2eAgFnSc7d1	velká
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
však	však	k9	však
byla	být	k5eAaImAgFnS	být
prudká	prudký	k2eAgFnSc1d1	prudká
povaha	povaha	k1gFnSc1	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc4	několik
incidentů	incident	k1gInPc2	incident
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
polních	polní	k2eAgFnPc2d1	polní
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
napadal	napadat	k5eAaImAgMnS	napadat
"	"	kIx"	"
<g/>
simulanty	simulant	k1gMnPc7	simulant
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
nevykazoval	vykazovat	k5eNaImAgMnS	vykazovat
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
nafackoval	nafackovat	k5eAaPmAgMnS	nafackovat
či	či	k8xC	či
vynadal	vynadat	k5eAaPmAgMnS	vynadat
vojákům	voják	k1gMnPc3	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
psychicky	psychicky	k6eAd1	psychicky
zhroutili	zhroutit	k5eAaPmAgMnP	zhroutit
nebo	nebo	k8xC	nebo
trpěli	trpět	k5eAaImAgMnP	trpět
například	například	k6eAd1	například
úplavicí	úplavice	k1gFnSc7	úplavice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
málem	málem	k6eAd1	málem
ukončil	ukončit	k5eAaPmAgInS	ukončit
incident	incident	k1gInSc1	incident
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
nemocnici	nemocnice	k1gFnSc6	nemocnice
ze	z	k7c2	z
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
srpna	srpen	k1gInSc2	srpen
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Vojín	vojín	k1gMnSc1	vojín
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
<g/>
H.	H.	kA	H.
<g/>
Kuhl	Kuhl	k1gInSc1	Kuhl
prokázal	prokázat	k5eAaPmAgInS	prokázat
dle	dle	k7c2	dle
Pattona	Patton	k1gMnSc2	Patton
zbabělost	zbabělost	k1gFnSc1	zbabělost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
dělostřelby	dělostřelba	k1gFnPc1	dělostřelba
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgMnSc1d1	vrchní
nadřízený	nadřízený	k1gMnSc1	nadřízený
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
nařídil	nařídit	k5eAaPmAgMnS	nařídit
generálovi	generál	k1gMnSc3	generál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vojákovi	voják	k1gMnSc3	voják
omluvil	omluvit	k5eAaPmAgMnS	omluvit
a	a	k8xC	a
média	médium	k1gNnSc2	médium
žádala	žádat	k5eAaImAgFnS	žádat
pro	pro	k7c4	pro
Pattona	Patton	k1gMnSc4	Patton
vojenský	vojenský	k2eAgInSc1d1	vojenský
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nekonal	konat	k5eNaImAgMnS	konat
a	a	k8xC	a
"	"	kIx"	"
<g/>
Starej	starat	k5eAaImRp2nS	starat
sekáč	sekáč	k1gInSc1	sekáč
<g/>
"	"	kIx"	"
nebyl	být	k5eNaImAgInS	být
sice	sice	k8xC	sice
propuštěn	propustit	k5eAaPmNgInS	propustit
ze	z	k7c2	z
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
byl	být	k5eAaImAgInS	být
zbaven	zbaven	k2eAgInSc1d1	zbaven
velení	velení	k1gNnSc2	velení
své	svůj	k3xOyFgFnSc2	svůj
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
deset	deset	k4xCc4	deset
měsíců	měsíc	k1gInPc2	měsíc
aktivně	aktivně	k6eAd1	aktivně
nesloužil	sloužit	k5eNaImAgInS	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
negativem	negativ	k1gInSc7	negativ
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
soutěživost	soutěživost	k1gFnSc1	soutěživost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
své	svůj	k3xOyFgMnPc4	svůj
důstojníky	důstojník	k1gMnPc4	důstojník
výslovně	výslovně	k6eAd1	výslovně
instruoval	instruovat	k5eAaBmAgMnS	instruovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
dobýt	dobýt	k5eAaPmF	dobýt
Messinu	Messina	k1gFnSc4	Messina
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
Monty	Monta	k1gFnPc1	Monta
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
britský	britský	k2eAgMnSc1d1	britský
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
Bernard	Bernard	k1gMnSc1	Bernard
Law	Law	k1gMnSc1	Law
Montgomery	Montgomera	k1gFnSc2	Montgomera
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
to	ten	k3xDgNnSc1	ten
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
to	ten	k3xDgNnSc4	ten
stojí	stát	k5eAaImIp3nS	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
výbuchy	výbuch	k1gInPc4	výbuch
vzteku	vztek	k1gInSc2	vztek
měl	mít	k5eAaImAgMnS	mít
několikrát	několikrát	k6eAd1	několikrát
vážné	vážný	k2eAgFnPc4d1	vážná
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
nadřízenými	nadřízená	k1gFnPc7	nadřízená
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
při	při	k7c6	při
invazi	invaze	k1gFnSc6	invaze
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
jemu	on	k3xPp3gMnSc3	on
původně	původně	k6eAd1	původně
podřízený	podřízený	k1gMnSc1	podřízený
generál	generál	k1gMnSc1	generál
Omar	Omar	k1gMnSc1	Omar
Bradley	Bradlea	k1gFnSc2	Bradlea
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nadřízeným	nadřízený	k1gMnSc7	nadřízený
velitelem	velitel	k1gMnSc7	velitel
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Acate	Acat	k1gInSc5	Acat
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
vojáci	voják	k1gMnPc1	voják
pod	pod	k7c7	pod
Pattonovým	Pattonový	k2eAgNnSc7d1	Pattonový
velením	velení	k1gNnSc7	velení
zmasakrovali	zmasakrovat	k5eAaPmAgMnP	zmasakrovat
72	[number]	k4	72
italských	italský	k2eAgMnPc2d1	italský
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Patton	Patton	k1gInSc1	Patton
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
Bradleym	Bradleym	k1gInSc1	Bradleym
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Bradley	Bradle	k1gMnPc7	Bradle
chtěl	chtít	k5eAaImAgInS	chtít
celý	celý	k2eAgInSc1d1	celý
incident	incident	k1gInSc1	incident
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Patton	Patton	k1gInSc1	Patton
chtěl	chtít	k5eAaImAgInS	chtít
masakr	masakr	k1gInSc4	masakr
zatajit	zatajit	k5eAaPmF	zatajit
před	před	k7c7	před
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
nezabývat	zabývat	k5eNaImF	zabývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Operace	operace	k1gFnSc1	operace
Fortitude	Fortitud	k1gInSc5	Fortitud
===	===	k?	===
</s>
</p>
<p>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tajných	tajný	k2eAgFnPc2d1	tajná
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
MI	já	k3xPp1nSc3	já
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Patton	Patton	k1gInSc1	Patton
určen	určit	k5eAaPmNgInS	určit
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
velitel	velitel	k1gMnSc1	velitel
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
při	při	k7c6	při
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
dezinformační	dezinformační	k2eAgFnSc6d1	dezinformační
operaci	operace	k1gFnSc6	operace
Fortitude	Fortitud	k1gInSc5	Fortitud
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jara	jaro	k1gNnSc2	jaro
1944	[number]	k4	1944
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
neexistující	existující	k2eNgFnSc2d1	neexistující
armády	armáda	k1gFnSc2	armáda
Fusag	Fusaga	k1gFnPc2	Fusaga
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
vylodit	vylodit	k5eAaPmF	vylodit
v	v	k7c4	v
Pas	pas	k1gNnSc4	pas
de	de	k?	de
Calais	Calais	k1gNnSc2	Calais
<g/>
.	.	kIx.	.
</s>
<s>
Pattonova	Pattonův	k2eAgFnSc1d1	Pattonova
neúčast	neúčast	k1gFnSc1	neúčast
na	na	k7c4	na
vylodění	vylodění	k1gNnSc4	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
straně	strana	k1gFnSc6	strana
prezentována	prezentován	k2eAgNnPc1d1	prezentováno
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důkazů	důkaz	k1gInPc2	důkaz
pro	pro	k7c4	pro
vůdcovo	vůdcův	k2eAgNnSc4d1	vůdcovo
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
jen	jen	k9	jen
o	o	k7c4	o
krycí	krycí	k2eAgInSc4d1	krycí
manévr	manévr	k1gInSc4	manévr
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
vylodění	vylodění	k1gNnSc1	vylodění
ještě	ještě	k6eAd1	ještě
neproběhlo	proběhnout	k5eNaPmAgNnS	proběhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
<g/>
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
aktivována	aktivován	k2eAgFnSc1d1	aktivována
3	[number]	k4	3
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
Patton	Patton	k1gInSc1	Patton
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gMnSc7	její
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
posléze	posléze	k6eAd1	posléze
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
jeho	jeho	k3xOp3gFnSc1	jeho
vojenská	vojenský	k2eAgFnSc1d1	vojenská
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
obešel	obejít	k5eAaPmAgMnS	obejít
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
severní	severní	k2eAgFnSc3d1	severní
Francii	Francie	k1gFnSc3	Francie
zejména	zejména	k9	zejména
rychlým	rychlý	k2eAgInSc7d1	rychlý
a	a	k8xC	a
agresivním	agresivní	k2eAgInSc7d1	agresivní
postupem	postup	k1gInSc7	postup
tankových	tankový	k2eAgNnPc2d1	tankové
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
obcházel	obcházet	k5eAaImAgInS	obcházet
hlavní	hlavní	k2eAgNnPc4d1	hlavní
ohniska	ohnisko	k1gNnPc4	ohnisko
odporu	odpor	k1gInSc2	odpor
a	a	k8xC	a
minimalizoval	minimalizovat	k5eAaBmAgMnS	minimalizovat
tak	tak	k6eAd1	tak
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
čelní	čelní	k2eAgInPc1d1	čelní
útoky	útok	k1gInPc1	útok
a	a	k8xC	a
ztráty	ztráta	k1gFnPc1	ztráta
svých	svůj	k3xOyFgMnPc2	svůj
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nečekaně	nečekaně	k6eAd1	nečekaně
vrchním	vrchní	k2eAgNnSc7d1	vrchní
velením	velení	k1gNnSc7	velení
zastavena	zastaven	k2eAgFnSc1d1	zastavena
31	[number]	k4	31
<g/>
.	.	kIx.	.
<g/>
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
u	u	k7c2	u
města	město	k1gNnSc2	město
Mety	meta	k1gFnSc2	meta
došly	dojít	k5eAaPmAgInP	dojít
jeho	jeho	k3xOp3gInSc7	jeho
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
armádě	armáda	k1gFnSc3	armáda
pohonné	pohonný	k2eAgFnSc2d1	pohonná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
musel	muset	k5eAaImAgInS	muset
Patton	Patton	k1gInSc1	Patton
dobývat	dobývat	k5eAaImF	dobývat
Mety	met	k1gInPc4	met
až	až	k9	až
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
<g/>
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
postupu	postup	k1gInSc6	postup
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
k	k	k7c3	k
Rýnu	Rýn	k1gInSc3	Rýn
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1944	[number]	k4	1944
pohotově	pohotově	k6eAd1	pohotově
stočil	stočit	k5eAaPmAgMnS	stočit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zmařil	zmařit	k5eAaPmAgMnS	zmařit
německou	německý	k2eAgFnSc4d1	německá
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
v	v	k7c6	v
Ardennách	Ardenna	k1gFnPc6	Ardenna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
složitý	složitý	k2eAgInSc1d1	složitý
takticko-logistický	taktickoogistický	k2eAgInSc1d1	takticko-logistický
manévr	manévr	k1gInSc1	manévr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
obklíčenou	obklíčený	k2eAgFnSc4d1	obklíčená
elitní	elitní	k2eAgNnSc1d1	elitní
101	[number]	k4	101
<g/>
.	.	kIx.	.
výsadkovou	výsadkový	k2eAgFnSc4d1	výsadková
divizi	divize	k1gFnSc4	divize
u	u	k7c2	u
Bastogne	Bastogn	k1gMnSc5	Bastogn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
vojenských	vojenský	k2eAgMnPc2d1	vojenský
historiků	historik	k1gMnPc2	historik
jeho	jeho	k3xOp3gInSc7	jeho
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
vojska	vojsko	k1gNnSc2	vojsko
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Sárské	sárský	k2eAgFnSc2d1	Sárská
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řeku	řeka	k1gFnSc4	řeka
Rýn	rýna	k1gFnPc2	rýna
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
překročit	překročit	k5eAaPmF	překročit
až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g/>
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
most	most	k1gInSc1	most
u	u	k7c2	u
Remagenu	Remagen	k1gInSc2	Remagen
získala	získat	k5eAaPmAgFnS	získat
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
armáda	armáda	k1gFnSc1	armáda
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Porýní	Porýní	k1gNnSc6	Porýní
a	a	k8xC	a
Falcku	Falcka	k1gFnSc4	Falcka
zajal	zajmout	k5eAaPmAgInS	zajmout
Patton	Patton	k1gInSc1	Patton
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tisíc	tisíc	k4xCgInPc2	tisíc
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
května	květen	k1gInSc2	květen
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
května	květen	k1gInSc2	květen
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
3	[number]	k4	3
<g/>
.	.	kIx.	.
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
západní	západní	k2eAgMnPc4d1	západní
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
jeji	jeje	k1gFnSc4	jeje
metropoli	metropole	k1gFnSc4	metropole
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Georg	Georg	k1gInSc1	Georg
Patton	Patton	k1gInSc1	Patton
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
a	a	k8xC	a
rozhodnut	rozhodnut	k2eAgMnSc1d1	rozhodnut
osvobodit	osvobodit	k5eAaPmF	osvobodit
také	také	k9	také
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
nedostal	dostat	k5eNaPmAgMnS	dostat
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
operaci	operace	k1gFnSc4	operace
z	z	k7c2	z
Washingtonu	Washington	k1gInSc2	Washington
politickou	politický	k2eAgFnSc4d1	politická
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
armáda	armáda	k1gFnSc1	armáda
tak	tak	k6eAd1	tak
musela	muset	k5eAaImAgFnS	muset
dodržet	dodržet	k5eAaPmF	dodržet
tzv.	tzv.	kA	tzv.
demarkační	demarkační	k2eAgFnSc6d1	demarkační
linii	linie	k1gFnSc6	linie
střetu	střet	k1gInSc2	střet
osvobozenecký	osvobozenecký	k2eAgInSc4d1	osvobozenecký
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
dohodnutou	dohodnutý	k2eAgFnSc4d1	dohodnutá
již	již	k6eAd1	již
na	na	k7c6	na
Jaltské	jaltský	k2eAgFnSc6d1	Jaltská
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Kraslice	kraslice	k1gFnSc1	kraslice
–	–	k?	–
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
–	–	k?	–
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
Rokycany	Rokycany	k1gInPc1	Rokycany
–	–	k?	–
Písek	Písek	k1gInSc1	Písek
–	–	k?	–
Netolice	Netolice	k1gFnPc1	Netolice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vojenským	vojenský	k2eAgMnSc7d1	vojenský
guvernérem	guvernér	k1gMnSc7	guvernér
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1945	[number]	k4	1945
se	se	k3xPyFc4	se
s	s	k7c7	s
obrovským	obrovský	k2eAgInSc7d1	obrovský
úspěchem	úspěch	k1gInSc7	úspěch
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
kariéra	kariéra	k1gFnSc1	kariéra
tohoto	tento	k3xDgMnSc4	tento
mimořádného	mimořádný	k2eAgMnSc4d1	mimořádný
generála	generál	k1gMnSc4	generál
již	již	k6eAd1	již
však	však	k9	však
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
sebevědomý	sebevědomý	k2eAgMnSc1d1	sebevědomý
a	a	k8xC	a
ambiciózní	ambiciózní	k2eAgMnSc1d1	ambiciózní
velitel	velitel	k1gMnSc1	velitel
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
nesl	nést	k5eAaImAgMnS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
aktivně	aktivně	k6eAd1	aktivně
nezasáhne	zasáhnout	k5eNaPmIp3nS	zasáhnout
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
politicky	politicky	k6eAd1	politicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
vytlačení	vytlačení	k1gNnSc4	vytlačení
komunismu	komunismus	k1gInSc2	komunismus
a	a	k8xC	a
Sovětů	Sovět	k1gMnPc2	Sovět
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgInSc4d1	vědom
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
svobodné	svobodný	k2eAgFnSc2d1	svobodná
Evropy	Evropa	k1gFnSc2	Evropa
chtěl	chtít	k5eAaImAgMnS	chtít
prosadit	prosadit	k5eAaPmF	prosadit
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
války	válka	k1gFnSc2	válka
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
jej	on	k3xPp3gMnSc4	on
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
odvolal	odvolat	k5eAaPmAgMnS	odvolat
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
přeložil	přeložit	k5eAaPmAgInS	přeložit
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
fakticky	fakticky	k6eAd1	fakticky
neexistovala	existovat	k5eNaImAgFnS	existovat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
pouze	pouze	k6eAd1	pouze
formálně	formálně	k6eAd1	formálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tragický	tragický	k2eAgInSc4d1	tragický
závěr	závěr	k1gInSc4	závěr
===	===	k?	===
</s>
</p>
<p>
<s>
Georg	Georg	k1gInSc1	Georg
Smith	Smith	k1gInSc1	Smith
Patton	Patton	k1gInSc4	Patton
III	III	kA	III
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
automobilové	automobilový	k2eAgFnSc2d1	automobilová
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gInSc1	jeho
štábní	štábní	k2eAgInSc1d1	štábní
cadillac	cadillac	k1gInSc1	cadillac
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1945	[number]	k4	1945
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Mannheimu	Mannheim	k1gInSc2	Mannheim
srazil	srazit	k5eAaPmAgMnS	srazit
s	s	k7c7	s
nákladním	nákladní	k2eAgNnSc7d1	nákladní
autem	auto	k1gNnSc7	auto
<g/>
.	.	kIx.	.
</s>
<s>
Nehoda	nehoda	k1gFnSc1	nehoda
nevypadala	vypadat	k5eNaPmAgFnS	vypadat
nijak	nijak	k6eAd1	nijak
dramaticky	dramaticky	k6eAd1	dramaticky
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
vyvázli	vyváznout	k5eAaPmAgMnP	vyváznout
nezraněni	zranit	k5eNaPmNgMnP	zranit
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
smůlu	smůla	k1gFnSc4	smůla
<g/>
.	.	kIx.	.
</s>
<s>
Náraz	náraz	k1gInSc1	náraz
jej	on	k3xPp3gMnSc4	on
vymrštil	vymrštit	k5eAaPmAgInS	vymrštit
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
narazil	narazit	k5eAaPmAgMnS	narazit
hlavou	hlava	k1gFnSc7	hlava
do	do	k7c2	do
střechy	střecha	k1gFnSc2	střecha
vozu	vůz	k1gInSc2	vůz
tak	tak	k6eAd1	tak
nešťastně	šťastně	k6eNd1	šťastně
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zlomil	zlomit	k5eAaPmAgMnS	zlomit
dva	dva	k4xCgInPc4	dva
krční	krční	k2eAgInPc4d1	krční
obratle	obratel	k1gInPc4	obratel
a	a	k8xC	a
vážně	vážně	k6eAd1	vážně
si	se	k3xPyFc3	se
poranil	poranit	k5eAaPmAgMnS	poranit
míchu	mícha	k1gFnSc4	mícha
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
ochrnul	ochrnout	k5eAaPmAgMnS	ochrnout
od	od	k7c2	od
krku	krk	k1gInSc2	krk
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
časem	časem	k6eAd1	časem
začal	začít	k5eAaPmAgInS	začít
mít	mít	k5eAaImF	mít
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
na	na	k7c4	na
plicní	plicní	k2eAgFnSc4d1	plicní
embolii	embolie	k1gFnSc4	embolie
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1	pohřben
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
přání	přání	k1gNnSc2	přání
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
vojenském	vojenský	k2eAgInSc6d1	vojenský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Hamme	Hamm	k1gInSc5	Hamm
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
vedle	vedle	k7c2	vedle
dalších	další	k2eAgMnPc2d1	další
příslušníků	příslušník	k1gMnPc2	příslušník
3	[number]	k4	3
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Generál	generál	k1gMnSc1	generál
Patton	Patton	k1gInSc4	Patton
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
duchovně	duchovně	k6eAd1	duchovně
založený	založený	k2eAgMnSc1d1	založený
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
věřil	věřit	k5eAaImAgMnS	věřit
v	v	k7c6	v
reinkarnaci	reinkarnace	k1gFnSc6	reinkarnace
–	–	k?	–
převtělování	převtělování	k1gNnSc2	převtělování
lidské	lidský	k2eAgFnSc2d1	lidská
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
symbolický	symbolický	k2eAgInSc1d1	symbolický
hrob	hrob	k1gInSc1	hrob
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
episkopálního	episkopální	k2eAgInSc2d1	episkopální
chrámu	chrám	k1gInSc2	chrám
Spasitele	spasitel	k1gMnSc2	spasitel
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
San	San	k1gMnSc1	San
Gabriel	Gabriel	k1gMnSc1	Gabriel
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtěn	k2eAgInSc1d1	pokřtěn
<g/>
,	,	kIx,	,
svatě	svatě	k6eAd1	svatě
přijímán	přijímat	k5eAaImNgMnS	přijímat
a	a	k8xC	a
také	také	k9	také
biřmován	biřmován	k2eAgMnSc1d1	biřmován
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
členy	člen	k1gInPc7	člen
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vojenských	vojenský	k2eAgMnPc2d1	vojenský
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
nadaného	nadaný	k2eAgMnSc4d1	nadaný
<g/>
,	,	kIx,	,
všestranného	všestranný	k2eAgMnSc4d1	všestranný
<g/>
,	,	kIx,	,
arogantního	arogantní	k2eAgMnSc4d1	arogantní
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
velmi	velmi	k6eAd1	velmi
schopného	schopný	k2eAgMnSc4d1	schopný
a	a	k8xC	a
jedinečného	jedinečný	k2eAgMnSc4d1	jedinečný
velitele	velitel	k1gMnSc4	velitel
i	i	k8xC	i
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
mentalitě	mentalita	k1gFnSc3	mentalita
a	a	k8xC	a
impulsivnímu	impulsivní	k2eAgInSc3d1	impulsivní
temperamentu	temperament	k1gInSc3	temperament
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejen	nejen	k6eAd1	nejen
armádními	armádní	k2eAgFnPc7d1	armádní
složkami	složka	k1gFnPc7	složka
spojenců	spojenec	k1gMnPc2	spojenec
rozporuplně	rozporuplně	k6eAd1	rozporuplně
vnímán	vnímat	k5eAaImNgInS	vnímat
a	a	k8xC	a
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
charakteristickému	charakteristický	k2eAgInSc3d1	charakteristický
image	image	k1gInPc1	image
nerozlučně	rozlučně	k6eNd1	rozlučně
patřily	patřit	k5eAaImAgInP	patřit
2	[number]	k4	2
revolvery	revolver	k1gInPc1	revolver
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nosil	nosit	k5eAaImAgInS	nosit
téměř	téměř	k6eAd1	téměř
neustále	neustále	k6eAd1	neustále
za	za	k7c7	za
pasem	pas	k1gInSc7	pas
<g/>
.	.	kIx.	.
:	:	kIx,	:
Colt	Colt	k1gInSc1	Colt
45	[number]	k4	45
a	a	k8xC	a
Smith	Smith	k1gMnSc1	Smith
&	&	k?	&
Wesson	Wesson	k1gMnSc1	Wesson
357	[number]	k4	357
Magnum	Magnum	k1gInSc1	Magnum
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
s	s	k7c7	s
rukojeťmi	rukojeť	k1gFnPc7	rukojeť
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
většiny	většina	k1gFnSc2	většina
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
generála	generál	k1gMnSc4	generál
osobně	osobně	k6eAd1	osobně
potkali	potkat	k5eAaPmAgMnP	potkat
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nesmírně	smírně	k6eNd1	smírně
charismatickou	charismatický	k2eAgFnSc4d1	charismatická
a	a	k8xC	a
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
údajně	údajně	k6eAd1	údajně
jediný	jediný	k2eAgMnSc1d1	jediný
spojenecký	spojenecký	k2eAgMnSc1d1	spojenecký
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
skutečně	skutečně	k6eAd1	skutečně
vážně	vážně	k6eAd1	vážně
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
způsob	způsob	k1gInSc4	způsob
a	a	k8xC	a
přístup	přístup	k1gInSc4	přístup
v	v	k7c6	v
taktice	taktika	k1gFnSc6	taktika
a	a	k8xC	a
způsobu	způsob	k1gInSc6	způsob
vedení	vedení	k1gNnSc2	vedení
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
velmi	velmi	k6eAd1	velmi
slavný	slavný	k2eAgInSc1d1	slavný
osmi	osm	k4xCc2	osm
Oscary	Oscara	k1gFnSc2	Oscara
oceněný	oceněný	k2eAgInSc1d1	oceněný
film	film	k1gInSc1	film
Generál	generál	k1gMnSc1	generál
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
scénáře	scénář	k1gInSc2	scénář
sepsal	sepsat	k5eAaPmAgMnS	sepsat
slavný	slavný	k2eAgMnSc1d1	slavný
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
Francis	Francis	k1gFnSc2	Francis
Ford	ford	k1gInSc1	ford
Coppola	Coppola	k1gFnSc1	Coppola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnosti	hodnost	k1gFnSc6	hodnost
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vojenská	vojenský	k2eAgNnPc1d1	vojenské
vyznamenání	vyznamenání	k1gNnPc1	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odrazy	odraz	k1gInPc1	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
uveden	uvést	k5eAaPmNgInS	uvést
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
životopisný	životopisný	k2eAgInSc1d1	životopisný
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
Generál	generál	k1gMnSc1	generál
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
<g/>
Franklin	Franklin	k1gInSc1	Franklin
J.	J.	kA	J.
Schaffner	Schaffner	k1gInSc1	Schaffner
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Francise	Francise	k1gFnSc2	Francise
Forda	ford	k1gMnSc2	ford
Coppoly	Coppola	k1gFnSc2	Coppola
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
úlohu	úloha	k1gFnSc4	úloha
zde	zde	k6eAd1	zde
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
George	George	k1gFnSc7	George
C.	C.	kA	C.
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
americký	americký	k2eAgInSc1d1	americký
televizní	televizní	k2eAgInSc1d1	televizní
hraný	hraný	k2eAgInSc1d1	hraný
snímek	snímek	k1gInSc1	snímek
Generál	generál	k1gMnSc1	generál
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
:	:	kIx,	:
Velitel	velitel	k1gMnSc1	velitel
invaze	invaze	k1gFnSc1	invaze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
také	také	k9	také
víceméně	víceméně	k9	víceméně
epizodně	epizodně	k6eAd1	epizodně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Proslulé	proslulý	k2eAgInPc1d1	proslulý
výroky	výrok	k1gInPc1	výrok
==	==	k?	==
</s>
</p>
<p>
<s>
Moji	můj	k3xOp1gMnPc1	můj
vojáci	voják	k1gMnPc1	voják
mohou	moct	k5eAaImIp3nP	moct
jíst	jíst	k5eAaImF	jíst
své	svůj	k3xOyFgInPc4	svůj
opasky	opasek	k1gInPc4	opasek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
moje	můj	k3xOp1gInPc1	můj
tanky	tank	k1gInPc1	tank
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
benzín	benzín	k1gInSc4	benzín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veďte	vést	k5eAaImRp2nP	vést
mě	já	k3xPp1nSc2	já
<g/>
,	,	kIx,	,
následujte	následovat	k5eAaImRp2nP	následovat
mě	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mi	já	k3xPp1nSc3	já
jděte	jít	k5eAaImRp2nP	jít
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nechť	nechť	k9	nechť
má	mít	k5eAaImIp3nS	mít
Bůh	bůh	k1gMnSc1	bůh
slitování	slitování	k1gNnSc4	slitování
s	s	k7c7	s
mými	můj	k3xOp1gMnPc7	můj
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
já	já	k3xPp1nSc1	já
je	být	k5eAaImIp3nS	být
mít	mít	k5eAaImF	mít
nebudu	být	k5eNaImBp1nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žádnej	Žádnej	k?	Žádnej
bastard	bastard	k1gMnSc1	bastard
ještě	ještě	k6eAd1	ještě
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
válku	válka	k1gFnSc4	válka
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ji	on	k3xPp3gFnSc4	on
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
donutil	donutit	k5eAaPmAgMnS	donutit
jinýho	jinýho	k?	jinýho
pitomce	pitomec	k1gMnSc4	pitomec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
on	on	k3xPp3gMnSc1	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pro	pro	k7c4	pro
tu	ten	k3xDgFnSc4	ten
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochodujte	pochodovat	k5eAaImRp2nP	pochodovat
proti	proti	k7c3	proti
zvuku	zvuk	k1gInSc3	zvuk
palby	palba	k1gFnSc2	palba
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
obvykle	obvykle	k6eAd1	obvykle
najdete	najít	k5eAaPmIp2nP	najít
před	před	k7c7	před
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
něco	něco	k3yInSc4	něco
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
,	,	kIx,	,
než	než	k8xS	než
žít	žít	k5eAaImF	žít
pro	pro	k7c4	pro
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
mít	mít	k5eAaImF	mít
německou	německý	k2eAgFnSc4d1	německá
divizi	divize	k1gFnSc4	divize
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
než	než	k8xS	než
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
divizi	divize	k1gFnSc4	divize
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
plán	plán	k1gInSc1	plán
provedený	provedený	k2eAgInSc1d1	provedený
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
plán	plán	k1gInSc1	plán
provedený	provedený	k2eAgInSc1d1	provedený
zítra	zítra	k6eAd1	zítra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Porazili	porazit	k5eAaPmAgMnP	porazit
jsme	být	k5eAaImIp1nP	být
špatného	špatný	k2eAgMnSc4d1	špatný
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Ladislas	Ladislas	k1gMnSc1	Ladislas
Farago	Farago	k1gMnSc1	Farago
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
last	last	k1gMnSc1	last
days	days	k6eAd1	days
of	of	k?	of
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
Berkley	Berklea	k1gFnPc1	Berklea
Books	Books	k1gInSc1	Books
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-425-05388-1	[number]	k4	0-425-05388-1
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc4	Georg
Forty	Forty	k?	Forty
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
armies	armies	k1gMnSc1	armies
of	of	k?	of
George	George	k1gInSc1	George
S.	S.	kA	S.
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
Arms	Arms	k1gInSc1	Arms
&	&	k?	&
Armour	Armoura	k1gFnPc2	Armoura
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-85409-295-2	[number]	k4	1-85409-295-2
</s>
</p>
<p>
<s>
Stanley	Stanley	k1gInPc1	Stanley
P.	P.	kA	P.
Hirshson	Hirshson	k1gNnSc4	Hirshson
<g/>
:	:	kIx,	:
General	General	k1gFnSc1	General
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
Harper	Harper	k1gInSc1	Harper
Collins	Collins	k1gInSc1	Collins
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-06-000982-9	[number]	k4	0-06-000982-9
</s>
</p>
<p>
<s>
Earle	earl	k1gMnSc5	earl
Rice	Ricus	k1gMnSc5	Ricus
<g/>
:	:	kIx,	:
George	George	k1gFnPc2	George
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
Chelsea	Chelsea	k1gMnSc1	Chelsea
House	house	k1gNnSc1	house
Publ	Publ	k1gMnSc1	Publ
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-7910-7403-X	[number]	k4	0-7910-7403-X
</s>
</p>
<p>
<s>
Trevor	Trevor	k1gMnSc1	Trevor
Royle	Royle	k1gFnSc2	Royle
<g/>
:	:	kIx,	:
Patton	Patton	k1gInSc4	Patton
<g/>
:	:	kIx,	:
Old	Olda	k1gFnPc2	Olda
Blood	Blood	k1gInSc1	Blood
and	and	k?	and
Guts	Guts	k1gInSc1	Guts
<g/>
.	.	kIx.	.
–	–	k?	–
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Weidenfeld	Weidenfeld	k1gInSc1	Weidenfeld
&	&	k?	&
Nicolson	Nicolson	k1gInSc1	Nicolson
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-297-84676-0	[number]	k4	0-297-84676-0
</s>
</p>
<p>
<s>
Patton	Patton	k1gInSc1	Patton
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Smith	Smith	k1gMnSc1	Smith
<g/>
:	:	kIx,	:
Válka	válka	k1gFnSc1	válka
mýma	můj	k3xOp1gNnPc7	můj
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-205-0236-X	[number]	k4	80-205-0236-X
</s>
</p>
<p>
<s>
Linda	Linda	k1gFnSc1	Linda
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Patton	Patton	k1gInSc1	Patton
byl	být	k5eAaImAgInS	být
váš	váš	k3xOp2gInSc4	váš
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g/>
,	,	kIx,	,
AB	AB	kA	AB
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Forty	Forty	k?	Forty
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
<g/>
:	:	kIx,	:
Velitelé	velitel	k1gMnPc1	velitel
tankových	tankový	k2eAgFnPc2d1	tanková
vojsk	vojsko	k1gNnPc2	vojsko
Rytíři	rytíř	k1gMnPc1	rytíř
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Brož	Brož	k1gMnSc1	Brož
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
:	:	kIx,	:
Manažeři	manažer	k1gMnPc1	manažer
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Orel	Orel	k1gMnSc1	Orel
:	:	kIx,	:
Největší	veliký	k2eAgInSc1d3	veliký
vojevůdci	vojevůdce	k1gMnSc3	vojevůdce
2	[number]	k4	2
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Alpress	Alpress	k1gInSc1	Alpress
Frýdek-Místek	Frýdek-Místek	k1gInSc1	Frýdek-Místek
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
George	George	k1gFnPc2	George
S.	S.	kA	S.
Patton	Patton	k1gInSc1	Patton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnPc1	galerie
George	Georg	k1gMnSc2	Georg
S.	S.	kA	S.
Patton	Patton	k1gInSc1	Patton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
George	Georg	k1gFnSc2	Georg
S.	S.	kA	S.
Patton	Patton	k1gInSc1	Patton
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
George	George	k1gInSc1	George
S.	S.	kA	S.
Patton	Patton	k1gInSc1	Patton
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c4	v
Hamm	Hamm	k1gInSc4	Hamm
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
49	[number]	k4	49
<g/>
°	°	k?	°
36	[number]	k4	36
<g/>
'	'	kIx"	'
41.71	[number]	k4	41.71
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
+6	+6	k4	+6
<g/>
°	°	k?	°
11	[number]	k4	11
<g/>
'	'	kIx"	'
9.41	[number]	k4	9.41
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
