<s>
William	William	k6eAd1	William
Seward	Seward	k1gInSc1	Seward
Burroughs	Burroughsa	k1gFnPc2	Burroughsa
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
,	,	kIx,	,
Missouri	Missouri	k1gFnSc1	Missouri
-	-	kIx~	-
1997	[number]	k4	1997
Lawrence	Lawrenec	k1gInSc2	Lawrenec
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc1	Kansas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
William	William	k1gInSc1	William
S.	S.	kA	S.
Burroughs	Burroughs	k1gInSc1	Burroughs
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
American	American	k1gMnSc1	American
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Letters	Letters	k1gInSc1	Letters
a	a	k8xC	a
Commandeur	Commandeur	k1gMnSc1	Commandeur
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Ordre	ordre	k1gInSc1	ordre
des	des	k1gNnSc1	des
Arts	Arts	k1gInSc1	Arts
et	et	k?	et
des	des	k1gNnSc7	des
Lettres	Lettresa	k1gFnPc2	Lettresa
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kultovní	kultovní	k2eAgMnSc1d1	kultovní
americký	americký	k2eAgMnSc1d1	americký
romanopisec	romanopisec	k1gMnSc1	romanopisec
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
