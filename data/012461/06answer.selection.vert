<s>
Lancia	Lancia	k1gFnSc1	Lancia
Appia	Appium	k1gNnSc2	Appium
je	být	k5eAaImIp3nS	být
osobní	osobní	k2eAgInSc4d1	osobní
automobil	automobil	k1gInSc4	automobil
nižší	nízký	k2eAgFnSc2d2	nižší
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
až	až	k9	až
1963	[number]	k4	1963
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
italská	italský	k2eAgFnSc1d1	italská
automobilka	automobilka	k1gFnSc1	automobilka
Lancia	Lancia	k1gFnSc1	Lancia
<g/>
.	.	kIx.	.
</s>
