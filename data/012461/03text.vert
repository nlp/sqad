<p>
<s>
Lancia	Lancia	k1gFnSc1	Lancia
Appia	Appium	k1gNnSc2	Appium
je	být	k5eAaImIp3nS	být
osobní	osobní	k2eAgInSc4d1	osobní
automobil	automobil	k1gInSc4	automobil
nižší	nízký	k2eAgFnSc2d2	nižší
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
až	až	k9	až
1963	[number]	k4	1963
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
italská	italský	k2eAgFnSc1d1	italská
automobilka	automobilka	k1gFnSc1	automobilka
Lancia	Lancia	k1gFnSc1	Lancia
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byla	být	k5eAaImAgFnS	být
Lancia	Lancia	k1gFnSc1	Lancia
Ardea	Ardea	k1gMnSc1	Ardea
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
nástupcem	nástupce	k1gMnSc7	nástupce
byla	být	k5eAaImAgFnS	být
Lancia	Lancia	k1gFnSc1	Lancia
Fulvia	Fulvia	k1gFnSc1	Fulvia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lancia	Lancia	k1gFnSc1	Lancia
Appia	Appius	k1gMnSc2	Appius
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
