<s>
Hobiti	hobit	k1gMnPc1	hobit
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
hobbits	hobbits	k6eAd1	hobbits
<g/>
,	,	kIx,	,
sindarsky	sindarsky	k6eAd1	sindarsky
Periannath	Periannatha	k1gFnPc2	Periannatha
<g/>
,	,	kIx,	,
lidskou	lidský	k2eAgFnSc7d1	lidská
západštinou	západština	k1gFnSc7	západština
"	"	kIx"	"
<g/>
Banakil	Banakil	k1gInSc1	Banakil
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Hobitsky	Hobitsky	k1gFnSc1	Hobitsky
"	"	kIx"	"
<g/>
Kudukové	Kuduk	k1gMnPc1	Kuduk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
půlčíci	půlčík	k1gMnPc1	půlčík
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Halflings	Halflings	k1gInSc1	Halflings
<g/>
))	))	k?	))
jsou	být	k5eAaImIp3nP	být
lidem	lid	k1gInSc7	lid
podobné	podobný	k2eAgFnSc2d1	podobná
bytosti	bytost	k1gFnSc2	bytost
drobného	drobný	k2eAgInSc2d1	drobný
vzrůstu	vzrůst	k1gInSc2	vzrůst
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
popsané	popsaný	k2eAgInPc1d1	popsaný
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
