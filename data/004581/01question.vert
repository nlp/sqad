<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
bytosti	bytost	k1gFnPc1	bytost
podobné	podobný	k2eAgFnPc1d1	podobná
lidem	člověk	k1gMnPc3	člověk
drobného	drobný	k2eAgInSc2d1	drobný
růstu	růst	k1gInSc2	růst
<g/>
,	,	kIx,	,
popsané	popsaný	k2eAgInPc1d1	popsaný
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
?	?	kIx.	?
</s>
