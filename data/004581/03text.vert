<s>
Hobiti	hobit	k1gMnPc1	hobit
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
hobbits	hobbits	k6eAd1	hobbits
<g/>
,	,	kIx,	,
sindarsky	sindarsky	k6eAd1	sindarsky
Periannath	Periannatha	k1gFnPc2	Periannatha
<g/>
,	,	kIx,	,
lidskou	lidský	k2eAgFnSc7d1	lidská
západštinou	západština	k1gFnSc7	západština
"	"	kIx"	"
<g/>
Banakil	Banakil	k1gInSc1	Banakil
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Hobitsky	Hobitsky	k1gFnSc1	Hobitsky
"	"	kIx"	"
<g/>
Kudukové	Kuduk	k1gMnPc1	Kuduk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
půlčíci	půlčík	k1gMnPc1	půlčík
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Halflings	Halflings	k1gInSc1	Halflings
<g/>
))	))	k?	))
jsou	být	k5eAaImIp3nP	být
lidem	lid	k1gInSc7	lid
podobné	podobný	k2eAgFnSc2d1	podobná
bytosti	bytost	k1gFnSc2	bytost
drobného	drobný	k2eAgInSc2d1	drobný
vzrůstu	vzrůst	k1gInSc2	vzrůst
ze	z	k7c2	z
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
popsané	popsaný	k2eAgInPc1d1	popsaný
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Hobit	hobit	k1gMnSc1	hobit
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
románu	román	k1gInSc2	román
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
Cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Hobbit	Hobbit	k1gMnSc5	Hobbit
Or	Or	k1gMnSc5	Or
There	Ther	k1gMnSc5	Ther
and	and	k?	and
Back	Back	k1gMnSc1	Back
Again	Again	k1gMnSc1	Again
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Frodo	Frodo	k1gNnSc1	Frodo
Pytlík	pytlík	k1gMnSc1	pytlík
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
hrdinů	hrdina	k1gMnPc2	hrdina
volného	volný	k2eAgNnSc2d1	volné
pokračování	pokračování	k1gNnSc2	pokračování
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Ringsa	k1gFnPc2	Ringsa
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
malým	malý	k2eAgInSc7d1	malý
vzrůstem	vzrůst	k1gInSc7	vzrůst
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
polovičním	poloviční	k2eAgInSc7d1	poloviční
oproti	oproti	k7c3	oproti
běžnému	běžný	k2eAgMnSc3d1	běžný
člověku	člověk	k1gMnSc3	člověk
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
rovněž	rovněž	k9	rovněž
říká	říkat	k5eAaImIp3nS	říkat
půlčíci	půlčík	k1gMnPc1	půlčík
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
halflings	halflings	k6eAd1	halflings
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
výšky	výška	k1gFnSc2	výška
přibližně	přibližně	k6eAd1	přibližně
tří	tři	k4xCgFnPc2	tři
až	až	k9	až
čtyř	čtyři	k4xCgFnPc2	čtyři
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
drobnější	drobný	k2eAgFnPc4d2	drobnější
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
vzrůstu	vzrůst	k1gInSc3	vzrůst
mají	mít	k5eAaImIp3nP	mít
velká	velký	k2eAgNnPc1d1	velké
<g/>
,	,	kIx,	,
chlupatá	chlupatý	k2eAgNnPc1d1	chlupaté
chodidla	chodidlo	k1gNnPc1	chodidlo
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
kůží	kůže	k1gFnSc7	kůže
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zpravidla	zpravidla	k6eAd1	zpravidla
chodí	chodit	k5eAaImIp3nP	chodit
bosí	bosý	k2eAgMnPc1d1	bosý
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
vzrůstu	vzrůst	k1gInSc2	vzrůst
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
přes	přes	k7c4	přes
čtyři	čtyři	k4xCgInPc4	čtyři
a	a	k8xC	a
půl	půl	k1xP	půl
stopy	stopa	k1gFnSc2	stopa
(	(	kIx(	(
<g/>
140	[number]	k4	140
cm	cm	kA	cm
<g/>
)	)	kIx)	)
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
hobiti	hobit	k1gMnPc1	hobit
Smělmír	Smělmíra	k1gFnPc2	Smělmíra
Brandorád	Brandoráda	k1gFnPc2	Brandoráda
a	a	k8xC	a
Peregrin	Peregrin	k1gInSc1	Peregrin
Bral	brát	k5eAaImAgInS	brát
díky	díky	k7c3	díky
kouzelným	kouzelný	k2eAgInPc3d1	kouzelný
nápojům	nápoj	k1gInPc3	nápoj
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
je	být	k5eAaImIp3nS	být
pohostil	pohostit	k5eAaPmAgInS	pohostit
ent	ent	k?	ent
Stromovous	Stromovous	k1gInSc1	Stromovous
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
pohlední	pohlední	k2eAgNnSc1d1	pohlední
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
působí	působit	k5eAaImIp3nS	působit
příjemně	příjemně	k6eAd1	příjemně
a	a	k8xC	a
přátelsky	přátelsky	k6eAd1	přátelsky
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
mají	mít	k5eAaImIp3nP	mít
kudrnaté	kudrnatý	k2eAgInPc4d1	kudrnatý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
žádným	žádný	k3yNgMnPc3	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nerostou	růst	k5eNaImIp3nP	růst
vousy	vous	k1gInPc4	vous
<g/>
.	.	kIx.	.
</s>
<s>
Dožívají	dožívat	k5eAaImIp3nP	dožívat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
kolem	kolem	k6eAd1	kolem
sta	sto	k4xCgNnPc4	sto
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
za	za	k7c4	za
plnoleté	plnoletý	k2eAgFnPc4d1	plnoletá
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
33	[number]	k4	33
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
nemají	mít	k5eNaImIp3nP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
–	–	k?	–
přesně	přesně	k6eAd1	přesně
řečeno	říct	k5eAaPmNgNnS	říct
jejich	jejich	k3xOp3gInSc1	jejich
původní	původní	k2eAgInSc1d1	původní
jazyk	jazyk	k1gInSc1	jazyk
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zapomenut	zapomnět	k5eAaImNgInS	zapomnět
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
jsou	být	k5eAaImIp3nP	být
hobití	hobití	k1gNnPc1	hobití
křestní	křestní	k2eAgNnPc1d1	křestní
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jiné	jiný	k2eAgInPc1d1	jiný
národy	národ	k1gInPc1	národ
Středozemě	Středozem	k1gFnSc2	Středozem
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
běžným	běžný	k2eAgInSc7d1	běžný
středozemským	středozemský	k2eAgInSc7d1	středozemský
jazykem	jazyk	k1gInSc7	jazyk
západštinou	západština	k1gFnSc7	západština
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Obecnou	obecný	k2eAgFnSc7d1	obecná
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
obohacenou	obohacený	k2eAgFnSc7d1	obohacená
o	o	k7c4	o
některá	některý	k3yIgNnPc4	některý
svá	svůj	k3xOyFgNnPc4	svůj
vlastní	vlastní	k2eAgNnPc4d1	vlastní
slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
hobit	hobit	k1gMnSc1	hobit
<g/>
,	,	kIx,	,
pelouch	pelouch	k1gInSc1	pelouch
<g/>
,	,	kIx,	,
názvy	název	k1gInPc1	název
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
původ	původ	k1gInSc1	původ
hobitů	hobit	k1gMnPc2	hobit
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
odrůdou	odrůda	k1gFnSc7	odrůda
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
povahou	povaha	k1gFnSc7	povaha
i	i	k9	i
vzhledem	vzhledem	k7c3	vzhledem
mnohem	mnohem	k6eAd1	mnohem
bližší	blízký	k2eAgFnSc3d2	bližší
než	než	k8xS	než
trpaslíkům	trpaslík	k1gMnPc3	trpaslík
<g/>
,	,	kIx,	,
elfům	elf	k1gMnPc3	elf
nebo	nebo	k8xC	nebo
skřetům	skřet	k1gMnPc3	skřet
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
Hůrka	hůrka	k1gFnSc1	hůrka
dokonce	dokonce	k9	dokonce
hobiti	hobit	k1gMnPc1	hobit
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
žijí	žít	k5eAaImIp3nP	žít
společně	společně	k6eAd1	společně
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
většinou	většinou	k6eAd1	většinou
bývají	bývat	k5eAaImIp3nP	bývat
dobrosrdeční	dobrosrdečný	k2eAgMnPc1d1	dobrosrdečný
<g/>
,	,	kIx,	,
veselí	veselit	k5eAaImIp3nP	veselit
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
dobrém	dobrý	k2eAgNnSc6d1	dobré
jídle	jídlo	k1gNnSc6	jídlo
<g/>
,	,	kIx,	,
pití	pití	k1gNnSc6	pití
a	a	k8xC	a
kouření	kouření	k1gNnSc6	kouření
dýmky	dýmka	k1gFnSc2	dýmka
<g/>
.	.	kIx.	.
</s>
<s>
Libují	libovat	k5eAaImIp3nP	libovat
si	se	k3xPyFc3	se
v	v	k7c6	v
pohodlném	pohodlný	k2eAgInSc6d1	pohodlný
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
neradi	nerad	k2eAgMnPc1d1	nerad
opouštějí	opouštět	k5eAaImIp3nP	opouštět
své	svůj	k3xOyFgNnSc4	svůj
rodinné	rodinný	k2eAgNnSc4d1	rodinné
sídlo	sídlo	k1gNnSc4	sídlo
a	a	k8xC	a
cestují	cestovat	k5eAaImIp3nP	cestovat
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Věnují	věnovat	k5eAaPmIp3nP	věnovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
řemeslům	řemeslo	k1gNnPc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
ani	ani	k9	ani
neučí	učit	k5eNaImIp3nS	učit
číst	číst	k5eAaImF	číst
a	a	k8xC	a
psát	psát	k5eAaImF	psát
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
prostého	prostý	k2eAgNnSc2d1	prosté
založení	založení	k1gNnSc2	založení
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
přímé	přímý	k2eAgNnSc4d1	přímé
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
a	a	k8xC	a
málokteří	málokterý	k3yIgMnPc1	málokterý
se	se	k3xPyFc4	se
zajímají	zajímat	k5eAaImIp3nP	zajímat
o	o	k7c4	o
záhady	záhada	k1gFnPc4	záhada
<g/>
.	.	kIx.	.
</s>
<s>
Rádi	rád	k2eAgMnPc1d1	rád
se	se	k3xPyFc4	se
oblékají	oblékat	k5eAaImIp3nP	oblékat
do	do	k7c2	do
světlých	světlý	k2eAgFnPc2d1	světlá
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zelené	zelený	k2eAgFnPc1d1	zelená
a	a	k8xC	a
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Hobit	hobit	k1gMnSc1	hobit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
dárky	dárek	k1gInPc4	dárek
ostatním	ostatní	k1gNnSc7	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
jídel	jídlo	k1gNnPc2	jídlo
mají	mít	k5eAaImIp3nP	mít
nejraději	rád	k6eAd3	rád
houby	houba	k1gFnPc1	houba
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nebojují	bojovat	k5eNaImIp3nP	bojovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
jen	jen	k9	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
se	se	k3xPyFc4	se
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
do	do	k7c2	do
válek	válka	k1gFnPc2	válka
nebo	nebo	k8xC	nebo
i	i	k9	i
do	do	k7c2	do
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
jiných	jiný	k2eAgInPc2d1	jiný
sporů	spor	k1gInPc2	spor
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgFnPc1	žádný
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
dobří	dobrý	k2eAgMnPc1d1	dobrý
lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
bystrý	bystrý	k2eAgInSc4d1	bystrý
zrak	zrak	k1gInSc4	zrak
a	a	k8xC	a
jistou	jistý	k2eAgFnSc4d1	jistá
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
velmi	velmi	k6eAd1	velmi
tichou	tichý	k2eAgFnSc7d1	tichá
chůzí	chůze	k1gFnSc7	chůze
–	–	k?	–
dokáží	dokázat	k5eAaPmIp3nP	dokázat
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
naprosto	naprosto	k6eAd1	naprosto
neslyšně	slyšně	k6eNd1	slyšně
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
neumí	umět	k5eNaImIp3nS	umět
plavat	plavat	k5eAaImF	plavat
a	a	k8xC	a
vodě	voda	k1gFnSc3	voda
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
rod	rod	k1gInSc4	rod
Brandorádů	Brandorád	k1gMnPc2	Brandorád
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Brandyvíny	Brandyvína	k1gFnSc2	Brandyvína
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
rybářskými	rybářský	k2eAgFnPc7d1	rybářská
loďkami	loďka	k1gFnPc7	loďka
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
stateční	statečný	k2eAgMnPc1d1	statečný
v	v	k7c6	v
úzkých	úzký	k2eAgFnPc6d1	úzká
<g/>
"	"	kIx"	"
-	-	kIx~	-
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
nesnází	nesnáz	k1gFnPc2	nesnáz
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
těžké	těžký	k2eAgNnSc1d1	těžké
je	být	k5eAaImIp3nS	být
zastrašit	zastrašit	k5eAaPmF	zastrašit
<g/>
,	,	kIx,	,
zabít	zabít	k5eAaPmF	zabít
nebo	nebo	k8xC	nebo
zlomit	zlomit	k5eAaPmF	zlomit
jejich	jejich	k3xOp3gFnSc4	jejich
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejsmělejší	smělý	k2eAgMnPc4d3	nejsmělejší
hobity	hobit	k1gMnPc4	hobit
patří	patřit	k5eAaImIp3nS	patřit
rod	rod	k1gInSc1	rod
Bralů	Bral	k1gMnPc2	Bral
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vydávali	vydávat	k5eAaImAgMnP	vydávat
na	na	k7c4	na
dobrodružné	dobrodružný	k2eAgFnPc4d1	dobrodružná
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
nečekané	čekaný	k2eNgFnSc3d1	nečekaná
statečnosti	statečnost	k1gFnSc3	statečnost
hobitů	hobit	k1gMnPc2	hobit
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
,	,	kIx,	,
když	když	k8xS	když
během	během	k7c2	během
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
zimy	zima	k1gFnSc2	zima
přišel	přijít	k5eAaPmAgMnS	přijít
pomoci	pomoct	k5eAaPmF	pomoct
hobitům	hobit	k1gMnPc3	hobit
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
nebojácného	bojácný	k2eNgMnSc4d1	nebojácný
ducha	duch	k1gMnSc4	duch
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
přežít	přežít	k5eAaPmF	přežít
nesmírné	smírný	k2eNgNnSc4d1	nesmírné
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
si	se	k3xPyFc3	se
tradičně	tradičně	k6eAd1	tradičně
budují	budovat	k5eAaImIp3nP	budovat
bydliště	bydliště	k1gNnSc4	bydliště
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
,	,	kIx,	,
nazývají	nazývat	k5eAaImIp3nP	nazývat
je	on	k3xPp3gInPc4	on
pelouchy	pelouch	k1gInPc4	pelouch
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
hobitů	hobit	k1gMnPc2	hobit
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
budovat	budovat	k5eAaImF	budovat
pouze	pouze	k6eAd1	pouze
podzemní	podzemní	k2eAgNnPc4d1	podzemní
sídla	sídlo	k1gNnPc4	sídlo
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
si	se	k3xPyFc3	se
stavět	stavět	k5eAaImF	stavět
i	i	k9	i
obyčejné	obyčejný	k2eAgInPc4d1	obyčejný
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
patrové	patrový	k2eAgNnSc4d1	patrové
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
domy	dům	k1gInPc4	dům
proto	proto	k8xC	proto
staví	stavit	k5eAaImIp3nS	stavit
pouze	pouze	k6eAd1	pouze
přízemní	přízemní	k2eAgFnSc1d1	přízemní
<g/>
,	,	kIx,	,
široké	široký	k2eAgNnSc1d1	široké
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zálibou	záliba	k1gFnSc7	záliba
v	v	k7c6	v
podzemních	podzemní	k2eAgNnPc6d1	podzemní
bydlištích	bydliště	k1gNnPc6	bydliště
mají	mít	k5eAaImIp3nP	mít
i	i	k8xC	i
jejich	jejich	k3xOp3gInPc1	jejich
domy	dům	k1gInPc1	dům
kruhová	kruhový	k2eAgNnPc4d1	kruhové
okna	okno	k1gNnPc4	okno
i	i	k8xC	i
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Hobité	Hobita	k1gMnPc1	Hobita
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
odrůdy	odrůda	k1gFnPc4	odrůda
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
Chluponozi	Chluponoze	k1gFnSc4	Chluponoze
<g/>
,	,	kIx,	,
Statové	Stat	k1gMnPc1	Stat
a	a	k8xC	a
Plavíni	Plavín	k1gMnPc1	Plavín
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
odrůdy	odrůda	k1gFnPc1	odrůda
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
navzájem	navzájem	k6eAd1	navzájem
smísily	smísit	k5eAaPmAgFnP	smísit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
například	například	k6eAd1	například
v	v	k7c6	v
rodech	rod	k1gInPc6	rod
Bralů	Bral	k1gMnPc2	Bral
a	a	k8xC	a
Brandorádů	Brandorád	k1gMnPc2	Brandorád
dosud	dosud	k6eAd1	dosud
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
plavínské	plavínský	k2eAgInPc4d1	plavínský
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analogii	analogie	k1gFnSc6	analogie
k	k	k7c3	k
anglickým	anglický	k2eAgFnPc3d1	anglická
dějinám	dějiny	k1gFnPc3	dějiny
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
tyto	tento	k3xDgFnPc4	tento
tři	tři	k4xCgFnPc4	tři
odrůdy	odrůda	k1gFnPc4	odrůda
přirovnat	přirovnat	k5eAaPmF	přirovnat
ke	k	k7c3	k
třem	tři	k4xCgInPc3	tři
kmenům	kmen	k1gInPc3	kmen
anglosasů	anglosas	k1gInPc2	anglosas
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
Anglům	Angl	k1gMnPc3	Angl
<g/>
,	,	kIx,	,
Sasům	Sas	k1gMnPc3	Sas
a	a	k8xC	a
Jutům	Jut	k1gMnPc3	Jut
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Chluponozi	Chluponoh	k1gMnPc1	Chluponoh
a	a	k8xC	a
Plavíni	Plavín	k1gMnPc1	Plavín
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
hobitům	hobit	k1gMnPc3	hobit
typu	typ	k1gInSc2	typ
hobilerů	hobiler	k1gInPc2	hobiler
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
feudálním	feudální	k2eAgMnPc3d1	feudální
nájemcům	nájemce	k1gMnPc3	nájemce
půdy	půda	k1gFnSc2	půda
sloužícím	sloužící	k2eAgMnSc7d1	sloužící
v	v	k7c6	v
domobraně	domobrana	k1gFnSc6	domobrana
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ke	k	k7c3	k
Statům	status	k1gInPc3	status
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
slovo	slovo	k1gNnSc4	slovo
hobbler	hobbler	k1gMnSc1	hobbler
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vleče	vléct	k5eAaImIp3nS	vléct
říční	říční	k2eAgFnSc4d1	říční
loďku	loďka	k1gFnSc4	loďka
na	na	k7c6	na
lanech	lano	k1gNnPc6	lano
pěšky	pěšky	k6eAd1	pěšky
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
či	či	k8xC	či
veslicí	veslice	k1gFnSc7	veslice
<g/>
.	.	kIx.	.
</s>
<s>
Chluponozi	Chluponoh	k1gMnPc1	Chluponoh
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
<g/>
Harfoot	Harfoot	k1gInSc1	Harfoot
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
nejmenší	malý	k2eAgMnPc4d3	nejmenší
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
hobitovití	hobitovitý	k2eAgMnPc1d1	hobitovitý
a	a	k8xC	a
nejpočetnější	početní	k2eAgMnPc1d3	nejpočetnější
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
hobitů	hobit	k1gMnPc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
povětšinou	povětšinou	k6eAd1	povětšinou
hnědé	hnědý	k2eAgInPc4d1	hnědý
kudrnaté	kudrnatý	k2eAgInPc4d1	kudrnatý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
snědou	snědý	k2eAgFnSc4d1	snědá
pleť	pleť	k1gFnSc4	pleť
a	a	k8xC	a
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
návycích	návyk	k1gInPc6	návyk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nejméně	málo	k6eAd3	málo
dobrodružní	dobrodružný	k2eAgMnPc1d1	dobrodružný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
s	s	k7c7	s
trpaslíky	trpaslík	k1gMnPc7	trpaslík
a	a	k8xC	a
Mlžné	mlžný	k2eAgFnPc4d1	mlžná
hory	hora	k1gFnPc4	hora
překročili	překročit	k5eAaPmAgMnP	překročit
jako	jako	k8xS	jako
první	první	k4xOgNnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Harfoot	Harfoot	k1gInSc1	Harfoot
je	být	k5eAaImIp3nS	být
skutečné	skutečný	k2eAgNnSc4d1	skutečné
anglické	anglický	k2eAgNnSc4d1	anglické
příjmení	příjmení	k1gNnSc4	příjmení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
staroanglické	staroanglický	k2eAgFnSc2d1	staroanglická
přezdívky	přezdívka	k1gFnSc2	přezdívka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
:	:	kIx,	:
Zaječí	zaječí	k2eAgFnSc1d1	zaječí
noha	noha	k1gFnSc1	noha
<g/>
.	.	kIx.	.
tato	tento	k3xDgFnSc1	tento
přezdívka	přezdívka	k1gFnSc1	přezdívka
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
používána	používat	k5eAaImNgFnS	používat
docela	docela	k6eAd1	docela
často	často	k6eAd1	často
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
vyzdvižen	vyzdvižen	k2eAgMnSc1d1	vyzdvižen
jako	jako	k8xC	jako
rychlý	rychlý	k2eAgMnSc1d1	rychlý
a	a	k8xC	a
hbitý	hbitý	k2eAgMnSc1d1	hbitý
běžec	běžec	k1gMnSc1	běžec
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
samo	sám	k3xTgNnSc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
již	již	k6eAd1	již
dobře	dobře	k6eAd1	dobře
pasuje	pasovat	k5eAaImIp3nS	pasovat
na	na	k7c4	na
povahu	povaha	k1gFnSc4	povaha
hobitů	hobit	k1gMnPc2	hobit
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
slovo	slovo	k1gNnSc1	slovo
hair	hair	k1gMnSc1	hair
znamená	znamenat	k5eAaImIp3nS	znamenat
vlasy	vlas	k1gInPc4	vlas
či	či	k8xC	či
chlupy	chlup	k1gInPc4	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějšími	častý	k2eAgNnPc7d3	nejčastější
jmény	jméno	k1gNnPc7	jméno
chluponohů	chluponoh	k1gInPc2	chluponoh
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Hnědáček	hnědáček	k1gMnSc1	hnědáček
<g/>
,	,	kIx,	,
Pískohrab	Pískohrab	k1gMnSc1	Pískohrab
<g/>
,	,	kIx,	,
Tunelík	Tunelík	k1gMnSc1	Tunelík
<g/>
,	,	kIx,	,
Pelíšek	pelíšek	k1gInSc1	pelíšek
<g/>
,	,	kIx,	,
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
a	a	k8xC	a
Provazník	Provazník	k1gMnSc1	Provazník
<g/>
.	.	kIx.	.
</s>
<s>
Statové	Stat	k1gMnPc1	Stat
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Stoor	Stoor	k1gInSc1	Stoor
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
hobitů	hobit	k1gMnPc2	hobit
největší	veliký	k2eAgInSc1d3	veliký
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
silou	síla	k1gFnSc7	síla
oplývající	oplývající	k2eAgFnSc2d1	oplývající
a	a	k8xC	a
nejpodobnější	podobný	k2eAgFnSc2d3	nejpodobnější
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pocházeli	pocházet	k5eAaImAgMnP	pocházet
hůrečtí	hůrecký	k2eAgMnPc1d1	hůrecký
hobiti	hobit	k1gMnPc1	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Nejraději	rád	k6eAd3	rád
se	se	k3xPyFc4	se
usídlují	usídlovat	k5eAaImIp3nP	usídlovat
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
okolo	okolo	k7c2	okolo
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
v	v	k7c6	v
bažinatých	bažinatý	k2eAgFnPc6d1	bažinatá
krajinách	krajina	k1gFnPc6	krajina
a	a	k8xC	a
asi	asi	k9	asi
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
stavět	stavět	k5eAaImF	stavět
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
hobitů	hobit	k1gMnPc2	hobit
nosí	nosit	k5eAaImIp3nS	nosit
někdy	někdy	k6eAd1	někdy
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Občasně	občasně	k6eAd1	občasně
jim	on	k3xPp3gMnPc3	on
rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
krátké	krátký	k2eAgInPc4d1	krátký
vousy	vous	k1gInPc4	vous
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgInPc4d2	kratší
nežli	nežli	k8xS	nežli
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
trpaslíkům	trpaslík	k1gMnPc3	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Nebojí	bát	k5eNaImIp3nP	bát
se	se	k3xPyFc4	se
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
plaví	plavit	k5eAaImIp3nS	plavit
se	se	k3xPyFc4	se
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
a	a	k8xC	a
utvářejí	utvářet	k5eAaImIp3nP	utvářet
obchod	obchod	k1gInSc4	obchod
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
hobity	hobit	k1gMnPc7	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Stoor	Stoor	k1gInSc1	Stoor
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
shodně	shodně	k6eAd1	shodně
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
store	stor	k1gInSc5	stor
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
skladovat	skladovat	k5eAaImF	skladovat
či	či	k8xC	či
ukládat	ukládat	k5eAaImF	ukládat
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
používaným	používaný	k2eAgNnSc7d1	používané
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Statů	status	k1gInPc2	status
Tlapka	tlapka	k1gFnSc1	tlapka
(	(	kIx(	(
<g/>
Puddifoot	Puddifoot	k1gInSc1	Puddifoot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
připomíná	připomínat	k5eAaImIp3nS	připomínat
dvojslovo	dvojslovo	k1gNnSc1	dvojslovo
"	"	kIx"	"
<g/>
puddle-foot	puddleoot	k1gInSc1	puddle-foot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
louže-noha	loužeoha	k1gFnSc1	louže-noha
<g/>
.	.	kIx.	.
</s>
<s>
Coby	Coby	k?	Coby
anglické	anglický	k2eAgNnSc4d1	anglické
příjmení	příjmení	k1gNnSc4	příjmení
ono	onen	k3xDgNnSc4	onen
slovo	slovo	k1gNnSc1	slovo
znělo	znět	k5eAaImAgNnS	znět
Pudephat	Pudephat	k1gFnSc7	Pudephat
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pudding	pudding	k1gInSc1	pudding
vat	vat	k1gInSc1	vat
<g/>
"	"	kIx"	"
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
vypouklý	vypouklý	k2eAgInSc4d1	vypouklý
sud	sud	k1gInSc4	sud
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
břichatec	břichatec	k?	břichatec
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
jménem	jméno	k1gNnSc7	jméno
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
Stránský	Stránský	k1gMnSc1	Stránský
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
nejspíše	nejspíše	k9	nejspíše
od	od	k7c2	od
stráně	stráň	k1gFnSc2	stráň
u	u	k7c2	u
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
jako	jako	k8xS	jako
Chalúpecký	Chalúpecký	k2eAgInSc1d1	Chalúpecký
<g/>
,	,	kIx,	,
Chalúpek	Chalúpek	k1gInSc1	Chalúpek
a	a	k8xC	a
Chaloupka	chaloupka	k1gFnSc1	chaloupka
pocházela	pocházet	k5eAaImAgFnS	pocházet
ponejspíše	ponejspíš	k1gInPc4	ponejspíš
také	také	k9	také
od	od	k7c2	od
Statů	status	k1gInPc2	status
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
bylo	být	k5eAaImAgNnS	být
Plavínů	plavín	k1gInPc2	plavín
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Fallohide	Fallohid	k1gMnSc5	Fallohid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
poněkud	poněkud	k6eAd1	poněkud
dobrodružnější	dobrodružný	k2eAgMnPc1d2	dobrodružnější
<g/>
,	,	kIx,	,
odvážnější	odvážný	k2eAgMnPc1d2	odvážnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
stávali	stávat	k5eAaImAgMnP	stávat
vůdci	vůdce	k1gMnPc1	vůdce
ostatních	ostatní	k2eAgInPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jediní	jediný	k2eAgMnPc1d1	jediný
se	se	k3xPyFc4	se
přátelili	přátelit	k5eAaImAgMnP	přátelit
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
raději	rád	k6eAd2	rád
lov	lov	k1gInSc4	lov
než	než	k8xS	než
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
bydlívali	bydlívat	k5eAaImAgMnP	bydlívat
v	v	k7c6	v
lesnaté	lesnatý	k2eAgFnSc6d1	lesnatá
krajině	krajina	k1gFnSc6	krajina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
pleť	pleť	k1gFnSc1	pleť
a	a	k8xC	a
vlasy	vlas	k1gInPc1	vlas
jsou	být	k5eAaImIp3nP	být
světlejší	světlý	k2eAgInPc1d2	světlejší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
hobitů	hobit	k1gMnPc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstem	vzrůst	k1gInSc7	vzrůst
jsou	být	k5eAaImIp3nP	být
střední	střední	k2eAgFnPc1d1	střední
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
Chluponohy	Chluponoh	k1gInPc7	Chluponoh
a	a	k8xC	a
Staty	status	k1gInPc7	status
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Fallohide	Fallohid	k1gInSc5	Fallohid
je	být	k5eAaImIp3nS	být
utvořen	utvořen	k2eAgMnSc1d1	utvořen
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
falo	falo	k6eAd1	falo
(	(	kIx(	(
<g/>
Starohornoněmecký	Starohornoněmecký	k2eAgInSc1d1	Starohornoněmecký
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
světle	světle	k6eAd1	světle
žlutou	žlutý	k2eAgFnSc4d1	žlutá
či	či	k8xC	či
narudle	narudle	k6eAd1	narudle
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
srnců	srnec	k1gMnPc2	srnec
<g/>
)	)	kIx)	)
a	a	k8xC	a
hide	hidat	k5eAaPmIp3nS	hidat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kůže	kůže	k1gFnSc1	kůže
či	či	k8xC	či
srst	srst	k1gFnSc1	srst
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
název	název	k1gInSc1	název
rozdělit	rozdělit	k5eAaPmF	rozdělit
jako	jako	k9	jako
fallow-hide	fallowid	k1gMnSc5	fallow-hid
<g/>
.	.	kIx.	.
</s>
<s>
Fallow	Fallow	k?	Fallow
je	být	k5eAaImIp3nS	být
staroanglické	staroanglický	k2eAgNnSc4d1	staroanglické
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
čerstvě	čerstvě	k6eAd1	čerstvě
zoranou	zoraný	k2eAgFnSc4d1	zoraná
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
hide	hide	k5eAaImRp2nP	hide
sloveso	sloveso	k1gNnSc4	sloveso
schovávat	schovávat	k5eAaImF	schovávat
<g/>
,	,	kIx,	,
tajit	tajit	k5eAaImF	tajit
<g/>
,	,	kIx,	,
či	či	k8xC	či
plošná	plošný	k2eAgFnSc1d1	plošná
míra	míra	k1gFnSc1	míra
akorát	akorát	k6eAd1	akorát
veliká	veliký	k2eAgFnSc1d1	veliká
pro	pro	k7c4	pro
usedlost	usedlost	k1gFnSc4	usedlost
(	(	kIx(	(
<g/>
cca	cca	kA	cca
sto	sto	k4xCgNnSc4	sto
akrů	akr	k1gInPc2	akr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plavíni	Plavín	k1gMnPc1	Plavín
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
jmenují	jmenovat	k5eAaImIp3nP	jmenovat
Zlaťák	zlaťák	k1gInSc4	zlaťák
<g/>
,	,	kIx,	,
Zlatníček	zlatníček	k1gInSc4	zlatníček
<g/>
,	,	kIx,	,
Tvrdohlavý	tvrdohlavý	k2eAgMnSc1d1	tvrdohlavý
a	a	k8xC	a
Bulík	bulík	k1gMnSc1	bulík
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
a	a	k8xC	a
dějinách	dějiny	k1gFnPc6	dějiny
hobitů	hobit	k1gMnPc2	hobit
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
nenápadnosti	nenápadnost	k1gFnSc3	nenápadnost
a	a	k8xC	a
snaze	snaha	k1gFnSc3	snaha
nevměšovat	vměšovat	k5eNaImF	vměšovat
se	se	k3xPyFc4	se
do	do	k7c2	do
cizích	cizí	k2eAgFnPc2d1	cizí
záležitostí	záležitost	k1gFnPc2	záležitost
si	se	k3xPyFc3	se
jich	on	k3xPp3gMnPc2	on
jiné	jiný	k2eAgInPc4d1	jiný
národy	národ	k1gInPc4	národ
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
vůbec	vůbec	k9	vůbec
nevšímaly	všímat	k5eNaImAgFnP	všímat
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
sami	sám	k3xTgMnPc1	sám
hobiti	hobit	k1gMnPc1	hobit
nikdy	nikdy	k6eAd1	nikdy
neměli	mít	k5eNaImAgMnP	mít
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
dějiny	dějiny	k1gFnPc4	dějiny
ani	ani	k8xC	ani
dějiny	dějiny	k1gFnPc4	dějiny
světa	svět	k1gInSc2	svět
nijak	nijak	k6eAd1	nijak
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
rodopisu	rodopis	k1gInSc2	rodopis
–	–	k?	–
hobiti	hobit	k1gMnPc1	hobit
si	se	k3xPyFc3	se
libují	libovat	k5eAaImIp3nP	libovat
v	v	k7c6	v
rodových	rodový	k2eAgFnPc6d1	rodová
historiích	historie	k1gFnPc6	historie
a	a	k8xC	a
podrobném	podrobný	k2eAgNnSc6d1	podrobné
zkoumání	zkoumání	k1gNnSc6	zkoumání
vlastních	vlastní	k2eAgInPc2d1	vlastní
příbuzenských	příbuzenský	k2eAgInPc2d1	příbuzenský
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Pravlast	pravlast	k1gFnSc1	pravlast
hobitů	hobit	k1gMnPc2	hobit
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
předpokládána	předpokládat	k5eAaImNgFnS	předpokládat
na	na	k7c4	na
území	území	k1gNnSc4	území
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
Anduiny	Anduina	k1gFnSc2	Anduina
(	(	kIx(	(
<g/>
Velké	velký	k2eAgFnSc2d1	velká
řeky	řeka	k1gFnSc2	řeka
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
Kosatcovou	kosatcový	k2eAgFnSc7d1	Kosatcová
řekou	řeka	k1gFnSc7	řeka
a	a	k8xC	a
Skalbalem	Skalbal	k1gInSc7	Skalbal
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
řeky	řeka	k1gFnSc2	řeka
Anduiny	Anduina	k1gFnSc2	Anduina
se	se	k3xPyFc4	se
nalézala	nalézat	k5eAaImAgFnS	nalézat
první	první	k4xOgNnPc4	první
známá	známý	k2eAgNnPc4d1	známé
sídliště	sídliště	k1gNnPc4	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
zřejmě	zřejmě	k6eAd1	zřejmě
měli	mít	k5eAaImAgMnP	mít
určité	určitý	k2eAgInPc4d1	určitý
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
předky	předek	k1gMnPc7	předek
pozdějších	pozdní	k2eAgInPc2d2	pozdější
rohanských	rohanský	k2eAgInPc2d1	rohanský
jezdců	jezdec	k1gInPc2	jezdec
–	–	k?	–
Rohirové	Rohir	k1gMnPc1	Rohir
byli	být	k5eAaImAgMnP	být
jediným	jediný	k2eAgInSc7d1	jediný
národem	národ	k1gInSc7	národ
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
pověsti	pověst	k1gFnPc1	pověst
se	se	k3xPyFc4	se
zmiňovaly	zmiňovat	k5eAaImAgFnP	zmiňovat
o	o	k7c6	o
hobitech	hobit	k1gMnPc6	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
výše	vysoce	k6eAd2	vysoce
zmiňované	zmiňovaný	k2eAgInPc4d1	zmiňovaný
tři	tři	k4xCgInPc4	tři
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Plavíni	Plavín	k1gMnPc1	Plavín
žili	žít	k5eAaImAgMnP	žít
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kmenů	kmen	k1gInPc2	kmen
nejseverněji	severně	k6eAd3	severně
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Chluponozi	Chluponoh	k1gMnPc1	Chluponoh
sídlili	sídlit	k5eAaImAgMnP	sídlit
ve	v	k7c6	v
vrchovinách	vrchovina	k1gFnPc6	vrchovina
a	a	k8xC	a
dělali	dělat	k5eAaImAgMnP	dělat
si	se	k3xPyFc3	se
nory	nora	k1gFnSc2	nora
v	v	k7c6	v
úbočí	úbočí	k1gNnSc6	úbočí
kopců	kopec	k1gInPc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Statové	Stat	k1gMnPc1	Stat
nejspíše	nejspíše	k9	nejspíše
žili	žít	k5eAaImAgMnP	žít
jižněji	jižně	k6eAd2	jižně
a	a	k8xC	a
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
nížiny	nížina	k1gFnPc4	nížina
a	a	k8xC	a
břehy	břeh	k1gInPc4	břeh
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
Velkého	velký	k2eAgInSc2d1	velký
zeleného	zelený	k2eAgInSc2d1	zelený
hvozdu	hvozd	k1gInSc2	hvozd
začal	začít	k5eAaPmAgInS	začít
stávat	stávat	k5eAaImF	stávat
Temný	temný	k2eAgInSc1d1	temný
hvozd	hvozd	k1gInSc1	hvozd
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
hvozdu	hvozd	k1gInSc2	hvozd
v	v	k7c4	v
Dol	dol	k1gInSc4	dol
Gulduru	Guldur	k1gInSc2	Guldur
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
Sauron	Sauron	k1gMnSc1	Sauron
a	a	k8xC	a
krajina	krajina	k1gFnSc1	krajina
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
hobiti	hobit	k1gMnPc1	hobit
postupně	postupně	k6eAd1	postupně
putovali	putovat	k5eAaImAgMnP	putovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
překročili	překročit	k5eAaPmAgMnP	překročit
Mlžné	mlžný	k2eAgFnPc4d1	mlžná
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Eriadoru	Eriador	k1gInSc2	Eriador
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1050	[number]	k4	1050
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
vydalo	vydat	k5eAaPmAgNnS	vydat
několik	několik	k4yIc4	několik
prvních	první	k4xOgFnPc2	první
Chluponohů	Chluponoh	k1gInPc2	Chluponoh
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Eriadoru	Eriador	k1gInSc2	Eriador
<g/>
,	,	kIx,	,
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
pak	pak	k6eAd1	pak
až	až	k9	až
k	k	k7c3	k
Větrovu	Větrov	k1gInSc3	Větrov
<g/>
.	.	kIx.	.
</s>
<s>
Plavíni	Plavín	k1gMnPc1	Plavín
překonali	překonat	k5eAaPmAgMnP	překonat
Mlžné	mlžný	k2eAgFnPc4d1	mlžná
hory	hora	k1gFnPc4	hora
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
od	od	k7c2	od
Roklinky	roklinka	k1gFnSc2	roklinka
a	a	k8xC	a
Statové	Stat	k1gMnPc1	Stat
prošli	projít	k5eAaPmAgMnP	projít
průsmykem	průsmyk	k1gInSc7	průsmyk
Rudorohu	Rudoroh	k1gInSc2	Rudoroh
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
spojili	spojit	k5eAaPmAgMnP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Koutci	Koutec	k1gInSc6	Koutec
<g/>
,	,	kIx,	,
místu	místo	k1gNnSc3	místo
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Bouřnou	bouřný	k2eAgFnSc7d1	Bouřná
a	a	k8xC	a
Mšenou	Mšený	k2eAgFnSc7d1	Mšený
<g/>
.	.	kIx.	.
</s>
<s>
Plavíni	Plavín	k1gMnPc1	Plavín
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
bylo	být	k5eAaImAgNnS	být
málo	málo	k1gNnSc1	málo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ponejvíce	ponejvíce	k6eAd1	ponejvíce
smísili	smísit	k5eAaPmAgMnP	smísit
s	s	k7c7	s
Chluponohy	Chluponoh	k1gInPc7	Chluponoh
a	a	k8xC	a
Staty	status	k1gInPc7	status
<g/>
.	.	kIx.	.
</s>
<s>
Statové	Stat	k1gMnPc1	Stat
navíc	navíc	k6eAd1	navíc
sídlili	sídlit	k5eAaImAgMnP	sídlit
i	i	k8xC	i
kousek	kousek	k1gInSc4	kousek
dále	daleko	k6eAd2	daleko
u	u	k7c2	u
Tharbadu	Tharbad	k1gInSc2	Tharbad
a	a	k8xC	a
ve	v	k7c6	v
Vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
v	v	k7c6	v
Eriadoru	Eriador	k1gInSc6	Eriador
založili	založit	k5eAaPmAgMnP	založit
několik	několik	k4yIc1	několik
sídel	sídlo	k1gNnPc2	sídlo
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Špalíček	špalíček	k1gInSc4	špalíček
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
město	město	k1gNnSc4	město
Hůrka	hůrka	k1gFnSc1	hůrka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přečkalo	přečkat	k5eAaPmAgNnS	přečkat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Nacházelo	nacházet	k5eAaImAgNnS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
pradávné	pradávný	k2eAgFnSc6d1	pradávná
křižovatce	křižovatka	k1gFnSc6	křižovatka
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlili	sídlit	k5eAaImAgMnP	sídlit
hobiti	hobit	k1gMnPc1	hobit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
hobiti	hobit	k1gMnPc1	hobit
však	však	k9	však
opustili	opustit	k5eAaPmAgMnP	opustit
údolí	údolí	k1gNnSc4	údolí
Anduiny	Anduina	k1gFnSc2	Anduina
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
tam	tam	k6eAd1	tam
přebývali	přebývat	k5eAaImAgMnP	přebývat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
však	však	k9	však
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
zůstavší	zůstavší	k2eAgMnPc1d1	zůstavší
nuceni	nucen	k2eAgMnPc1d1	nucen
znovu	znovu	k6eAd1	znovu
prchat	prchat	k5eAaImF	prchat
před	před	k7c7	před
Angmarem	Angmar	k1gInSc7	Angmar
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ze	z	k7c2	z
Statů	status	k1gInPc2	status
šli	jít	k5eAaImAgMnP	jít
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
přidali	přidat	k5eAaPmAgMnP	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
příbuzným	příbuzný	k1gMnPc3	příbuzný
u	u	k7c2	u
Tharbadu	Tharbad	k1gInSc2	Tharbad
na	na	k7c6	na
Vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k2eAgMnPc1d1	jiný
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Divočiny	divočina	k1gFnSc2	divočina
k	k	k7c3	k
životu	život	k1gInSc3	život
u	u	k7c2	u
Kosatcové	kosatcový	k2eAgFnSc2d1	Kosatcová
řeky	řeka	k1gFnSc2	řeka
jako	jako	k8xS	jako
například	například	k6eAd1	například
předkové	předek	k1gMnPc1	předek
známého	známý	k2eAgMnSc2d1	známý
Gluma	Glum	k1gMnSc2	Glum
Nejvíce	nejvíce	k6eAd1	nejvíce
hobitů	hobit	k1gMnPc2	hobit
se	se	k3xPyFc4	se
však	však	k9	však
odstěhovalo	odstěhovat	k5eAaPmAgNnS	odstěhovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1601	[number]	k4	1601
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
se	se	k3xPyFc4	se
hobití	hobitý	k2eAgMnPc1d1	hobitý
bratři	bratr	k1gMnPc1	bratr
Marko	Marko	k1gMnSc1	Marko
a	a	k8xC	a
Blanko	blanko	k6eAd1	blanko
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
hobitími	hobití	k1gNnPc7	hobití
rodinami	rodina	k1gFnPc7	rodina
vypravili	vypravit	k5eAaPmAgMnP	vypravit
z	z	k7c2	z
Hůrky	hůrka	k1gFnSc2	hůrka
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Branduínu	Branduín	k1gInSc2	Branduín
a	a	k8xC	a
osídlili	osídlit	k5eAaPmAgMnP	osídlit
krajinu	krajina	k1gFnSc4	krajina
patřící	patřící	k2eAgFnSc1d1	patřící
králi	král	k1gMnSc3	král
Arnoru	Arnor	k1gMnSc3	Arnor
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
téměř	téměř	k6eAd1	téměř
opuštěnou	opuštěný	k2eAgFnSc4d1	opuštěná
<g/>
,	,	kIx,	,
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
státeček	státeček	k1gInSc4	státeček
<g/>
,	,	kIx,	,
podléhající	podléhající	k2eAgNnSc4d1	podléhající
Severnímu	severní	k2eAgNnSc3d1	severní
království	království	k1gNnSc3	království
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Kraj	kraj	k1gInSc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Argeleb	Argelba	k1gFnPc2	Argelba
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vládce	vládce	k1gMnSc4	vládce
Arthedainu	Arthedain	k1gInSc2	Arthedain
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
zbytkem	zbytek	k1gInSc7	zbytek
arnorského	arnorský	k2eAgNnSc2d1	arnorský
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
jim	on	k3xPp3gInPc3	on
dal	dát	k5eAaPmAgInS	dát
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
osídlení	osídlení	k1gNnSc3	osídlení
oblasti	oblast	k1gFnSc2	oblast
pokud	pokud	k8xS	pokud
budou	být	k5eAaImBp3nP	být
chránit	chránit	k5eAaImF	chránit
západní	západní	k2eAgFnSc4d1	západní
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
uznávat	uznávat	k5eAaImF	uznávat
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
Severního	severní	k2eAgNnSc2d1	severní
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hobitím	hobití	k1gNnSc7	hobití
putování	putování	k1gNnSc2	putování
opět	opět	k6eAd1	opět
můžeme	moct	k5eAaImIp1nP	moct
shledat	shledat	k5eAaPmF	shledat
analogii	analogie	k1gFnSc3	analogie
Anglosasům	Anglosas	k1gMnPc3	Anglosas
a	a	k8xC	a
konkrétně	konkrétně	k6eAd1	konkrétně
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
putování	putování	k1gNnSc3	putování
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Hobité	Hobita	k1gMnPc1	Hobita
se	se	k3xPyFc4	se
v	v	k7c6	v
části	část	k1gFnSc6	část
své	svůj	k3xOyFgFnSc6	svůj
pouti	pouť	k1gFnSc6	pouť
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
domova	domov	k1gInSc2	domov
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
Koutec	Koutec	k1gInSc1	Koutec
(	(	kIx(	(
<g/>
Angle	Angl	k1gMnSc5	Angl
<g/>
)	)	kIx)	)
nacházejícího	nacházející	k2eAgInSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
klínovém	klínový	k2eAgNnSc6d1	klínové
území	území	k1gNnSc6	území
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Bouřnou	bouřný	k2eAgFnSc7d1	Bouřná
a	a	k8xC	a
Mšenou	Mšený	k2eAgFnSc7d1	Mšený
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Bradyvínu	Bradyvín	k1gInSc2	Bradyvín
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
později	pozdě	k6eAd2	pozdě
nazvali	nazvat	k5eAaBmAgMnP	nazvat
Kraj	kraj	k1gInSc4	kraj
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
bratrů	bratr	k1gMnPc2	bratr
Marka	marka	k1gFnSc1	marka
Blanka	Blanka	k1gFnSc1	Blanka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Anglosasové	Anglosas	k1gMnPc1	Anglosas
opustili	opustit	k5eAaPmAgMnP	opustit
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
nájezdníky	nájezdník	k1gMnPc4	nájezdník
svůj	svůj	k3xOyFgMnSc1	svůj
domov	domov	k1gInSc4	domov
též	též	k9	též
známý	známý	k2eAgInSc4d1	známý
jako	jako	k8xS	jako
Angle	Angl	k1gMnSc5	Angl
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
místo	místo	k7c2	místo
tvaru	tvar	k1gInSc2	tvar
klínu	klín	k1gInSc2	klín
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Flensburským	Flensburský	k2eAgInSc7d1	Flensburský
fjordem	fjord	k1gInSc7	fjord
a	a	k8xC	a
řekou	řeka	k1gFnSc7	řeka
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Schlei	Schle	k1gMnSc3	Schle
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
kanál	kanál	k1gInSc4	kanál
La	la	k1gNnSc2	la
Manche	Manche	k1gFnPc7	Manche
do	do	k7c2	do
končiny	končina	k1gFnSc2	končina
později	pozdě	k6eAd2	pozdě
zvané	zvaný	k2eAgInPc1d1	zvaný
Anglické	anglický	k2eAgInPc1d1	anglický
kraje	kraj	k1gInPc1	kraj
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
bratří	bratr	k1gMnPc2	bratr
Hengista	Hengist	k1gMnSc2	Hengist
a	a	k8xC	a
Horsa	Hors	k1gMnSc2	Hors
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jméno	jméno	k1gNnSc4	jméno
Hengist	Hengist	k1gInSc1	Hengist
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
angličtině	angličtina	k1gFnSc6	angličtina
znamená	znamenat	k5eAaImIp3nS	znamenat
kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
Horsa	Hors	k1gMnSc2	Hors
rovněž	rovněž	k9	rovněž
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
Marko	Marko	k1gMnSc1	Marko
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velštině	velština	k1gFnSc6	velština
<g/>
:	:	kIx,	:
march	march	k1gInSc1	march
<g/>
,	,	kIx,	,
gaelsky	gaelsky	k6eAd1	gaelsky
<g/>
:	:	kIx,	:
marc	marc	k6eAd1	marc
<g/>
,	,	kIx,	,
staroanglicky	staroanglicky	k6eAd1	staroanglicky
<g/>
:	:	kIx,	:
mearh	mearh	k1gInSc1	mearh
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
také	také	k9	také
kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
Blanko	blanko	k6eAd1	blanko
(	(	kIx(	(
<g/>
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
:	:	kIx,	:
Blanca	Blanc	k1gMnSc2	Blanc
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
Norštině	norština	k1gFnSc6	norština
<g/>
:	:	kIx,	:
Blakkr	Blakkr	k1gInSc1	Blakkr
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
rovněž	rovněž	k9	rovněž
kůň	kůň	k1gMnSc1	kůň
(	(	kIx(	(
<g/>
bílý	bílý	k1gMnSc1	bílý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
staroanglického	staroanglický	k2eAgMnSc2d1	staroanglický
mearh	mearh	k1gInSc1	mearh
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
novoanglické	novoanglický	k2eAgNnSc1d1	novoanglické
slovo	slovo	k1gNnSc1	slovo
mare	mare	k1gFnSc1	mare
(	(	kIx(	(
<g/>
kobyla	kobyla	k1gFnSc1	kobyla
<g/>
)	)	kIx)	)
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
také	také	k9	také
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
Tolkiena	Tolkiena	k1gFnSc1	Tolkiena
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
plemene	plemeno	k1gNnSc2	plemeno
Mears	Mearsa	k1gFnPc2	Mearsa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bílých	bílý	k2eAgMnPc2d1	bílý
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
využívali	využívat	k5eAaPmAgMnP	využívat
Jezdci	jezdec	k1gMnPc1	jezdec
z	z	k7c2	z
Marky	marka	k1gFnSc2	marka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
si	se	k3xPyFc3	se
zvolili	zvolit	k5eAaPmAgMnP	zvolit
svého	svůj	k3xOyFgMnSc4	svůj
vládce	vládce	k1gMnSc4	vládce
<g/>
,	,	kIx,	,
vladyku	vladyka	k1gMnSc4	vladyka
Kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
velitele	velitel	k1gMnSc2	velitel
zbrojného	zbrojný	k2eAgNnSc2d1	zbrojný
hobitstva	hobitstvo	k1gNnSc2	hobitstvo
zvaného	zvaný	k2eAgNnSc2d1	zvané
Radda	Raddo	k1gNnSc2	Raddo
(	(	kIx(	(
<g/>
Bucca	Bucca	k1gFnSc1	Bucca
<g/>
)	)	kIx)	)
z	z	k7c2	z
Blat	Blata	k1gNnPc2	Blata
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zástupcem	zástupce	k1gMnSc7	zástupce
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
titul	titul	k1gInSc1	titul
byl	být	k5eAaImAgInS	být
dědičný	dědičný	k2eAgMnSc1d1	dědičný
a	a	k8xC	a
tak	tak	k6eAd1	tak
až	až	k9	až
po	po	k7c4	po
čtyři	čtyři	k4xCgNnPc4	čtyři
století	století	k1gNnPc2	století
pocházeli	pocházet	k5eAaImAgMnP	pocházet
všichni	všechen	k3xTgMnPc1	všechen
další	další	k2eAgMnPc1d1	další
vladykové	vladyka	k1gMnPc1	vladyka
z	z	k7c2	z
přímé	přímý	k2eAgFnSc2d1	přímá
radové	radová	k1gFnSc2	radová
linie	linie	k1gFnSc2	linie
od	od	k7c2	od
Raddy	Radda	k1gFnSc2	Radda
z	z	k7c2	z
Blat	Blata	k1gNnPc2	Blata
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Raddy	Radda	k1gFnSc2	Radda
z	z	k7c2	z
Blat	Blata	k1gNnPc2	Blata
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Starorádové	Starorád	k1gMnPc5	Starorád
(	(	kIx(	(
<g/>
Zaragamba	Zaragamba	k1gFnSc1	Zaragamba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Starorádové	Starorád	k1gMnPc1	Starorád
vůbec	vůbec	k9	vůbec
prvními	první	k4xOgNnPc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
mezi	mezi	k7c7	mezi
hobity	hobit	k1gMnPc7	hobit
používal	používat	k5eAaImAgMnS	používat
příjmení	příjmení	k1gNnSc4	příjmení
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
používalo	používat	k5eAaImAgNnS	používat
jako	jako	k9	jako
titul	titul	k1gInSc4	titul
pro	pro	k7c4	pro
právě	právě	k6eAd1	právě
vládnoucího	vládnoucí	k2eAgMnSc4d1	vládnoucí
vladyku	vladyka	k1gMnSc4	vladyka
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
začali	začít	k5eAaPmAgMnP	začít
žít	žít	k5eAaImF	žít
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
a	a	k8xC	a
nevměšovat	vměšovat	k5eNaImF	vměšovat
se	se	k3xPyFc4	se
do	do	k7c2	do
událostí	událost	k1gFnPc2	událost
vnějšího	vnější	k2eAgInSc2d1	vnější
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
vladyka	vladyka	k1gMnSc1	vladyka
Kraje	kraj	k1gInSc2	kraj
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
ve	v	k7c6	v
Fornostu	Fornost	k1gInSc6	Fornost
v	v	k7c6	v
r.	r.	kA	r.
1974	[number]	k4	1974
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
proti	proti	k7c3	proti
Černokněžnému	černokněžný	k2eAgMnSc3d1	černokněžný
králi	král	k1gMnSc3	král
Angmaru	Angmar	k1gMnSc3	Angmar
družinu	družina	k1gFnSc4	družina
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
sporné	sporný	k2eAgNnSc1d1	sporné
–	–	k?	–
letopisci	letopisec	k1gMnSc3	letopisec
Arnoru	Arnor	k1gMnSc3	Arnor
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nezmiňují	zmiňovat	k5eNaImIp3nP	zmiňovat
a	a	k8xC	a
také	také	k9	také
sám	sám	k3xTgMnSc1	sám
Černokněžný	černokněžný	k2eAgMnSc1d1	černokněžný
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
později	pozdě	k6eAd2	pozdě
pátral	pátrat	k5eAaImAgMnS	pátrat
po	po	k7c6	po
hobitech	hobit	k1gMnPc6	hobit
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
tušení	tušení	k1gNnSc4	tušení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Eriadoru	Eriador	k1gInSc6	Eriador
a	a	k8xC	a
hledal	hledat	k5eAaImAgInS	hledat
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Anduiny	Anduina	k1gFnSc2	Anduina
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
přečkali	přečkat	k5eAaPmAgMnP	přečkat
i	i	k9	i
pád	pád	k1gInSc4	pád
Severního	severní	k2eAgNnSc2d1	severní
království	království	k1gNnSc2	království
a	a	k8xC	a
spravovali	spravovat	k5eAaImAgMnP	spravovat
si	se	k3xPyFc3	se
své	svůj	k3xOyFgInPc4	svůj
záležitosti	záležitost	k1gFnSc6	záležitost
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
kontakty	kontakt	k1gInPc4	kontakt
udržovali	udržovat	k5eAaImAgMnP	udržovat
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
s	s	k7c7	s
Hůrkou	hůrka	k1gFnSc7	hůrka
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
krajskými	krajský	k2eAgMnPc7d1	krajský
a	a	k8xC	a
hůreckými	hůrecký	k2eAgMnPc7d1	hůrecký
hobity	hobit	k1gMnPc7	hobit
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
jistá	jistý	k2eAgFnSc1d1	jistá
přátelská	přátelský	k2eAgFnSc1d1	přátelská
řevnivost	řevnivost	k1gFnSc1	řevnivost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
v	v	k7c6	v
dobrém	dobré	k1gNnSc6	dobré
na	na	k7c6	na
období	období	k1gNnSc6	období
arnorského	arnorský	k2eAgNnSc2d1	arnorský
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
mluvili	mluvit	k5eAaImAgMnP	mluvit
o	o	k7c6	o
něčem	něco	k3yInSc6	něco
příjemném	příjemný	k2eAgNnSc6d1	příjemné
<g/>
,	,	kIx,	,
co	co	k9	co
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
přijít	přijít	k5eAaPmF	přijít
<g/>
,	,	kIx,	,
užívali	užívat	k5eAaImAgMnP	užívat
obrat	obrat	k1gInSc4	obrat
"	"	kIx"	"
<g/>
až	až	k6eAd1	až
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
2747	[number]	k4	2747
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
zabloudila	zabloudit	k5eAaPmAgFnS	zabloudit
početná	početný	k2eAgFnSc1d1	početná
skupina	skupina	k1gFnSc1	skupina
skřetů	skřet	k1gMnPc2	skřet
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
porazil	porazit	k5eAaPmAgInS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Zeleném	zelený	k2eAgNnSc6d1	zelené
poli	pole	k1gNnSc6	pole
Bučivoj	Bučivoj	k1gInSc4	Bučivoj
Bral	brát	k5eAaImAgMnS	brát
a	a	k8xC	a
zahubil	zahubit	k5eAaPmAgMnS	zahubit
jejich	jejich	k3xOp3gMnSc4	jejich
vůdce	vůdce	k1gMnSc4	vůdce
Golfimbula	Golfimbul	k1gMnSc4	Golfimbul
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgNnPc4	tři
století	století	k1gNnPc2	století
poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
vybojovaná	vybojovaný	k2eAgFnSc1d1	vybojovaná
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
zimy	zima	k1gFnSc2	zima
v	v	k7c6	v
r.	r.	kA	r.
2758	[number]	k4	2758
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
přišel	přijít	k5eAaPmAgMnS	přijít
hobitům	hobit	k1gMnPc3	hobit
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
podivoval	podivovat	k5eAaImAgMnS	podivovat
jejich	jejich	k3xOp3gFnSc2	jejich
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
dokázali	dokázat	k5eAaPmAgMnP	dokázat
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
kruté	krutý	k2eAgFnSc3d1	krutá
zimě	zima	k1gFnSc3	zima
a	a	k8xC	a
hladu	hlad	k1gInSc3	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	s	k7c7	s
Starým	Starý	k1gMnSc7	Starý
Bralem	Bral	k1gMnSc7	Bral
<g/>
,	,	kIx,	,
dědem	děd	k1gMnSc7	děd
Bilba	Bilb	k1gMnSc2	Bilb
Pytlíka	pytlík	k1gMnSc2	pytlík
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
pořádal	pořádat	k5eAaImAgInS	pořádat
krásné	krásný	k2eAgInPc4d1	krásný
ohňostroje	ohňostroj	k1gInPc4	ohňostroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
legendární	legendární	k2eAgInSc4d1	legendární
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
Čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
věku	věk	k1gInSc2	věk
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Saurona	Saurona	k1gFnSc1	Saurona
Kraj	kraj	k1gInSc1	kraj
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Saruman	Saruman	k1gMnSc1	Saruman
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
zločinní	zločinný	k2eAgMnPc1d1	zločinný
tuláci	tulák	k1gMnPc1	tulák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
poraženi	porazit	k5eAaPmNgMnP	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Povodí	povodí	k1gNnSc2	povodí
hobitími	hobití	k1gNnPc7	hobití
bojovníky	bojovník	k1gMnPc4	bojovník
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Smělmíra	Smělmír	k1gMnSc2	Smělmír
Brandoráda	Brandoráda	k1gFnSc1	Brandoráda
a	a	k8xC	a
Peregrina	Peregrina	k1gFnSc1	Peregrina
Brala	brát	k5eAaImAgFnS	brát
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Elessar	Elessar	k1gMnSc1	Elessar
potom	potom	k6eAd1	potom
nechal	nechat	k5eAaPmAgMnS	nechat
rozšířit	rozšířit	k5eAaPmF	rozšířit
Kraj	kraj	k1gInSc4	kraj
o	o	k7c4	o
Západní	západní	k2eAgFnSc4d1	západní
marku	marka	k1gFnSc4	marka
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgMnSc1	žádný
člověk	člověk	k1gMnSc1	člověk
nesmí	smět	k5eNaImIp3nS	smět
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
velmi	velmi	k6eAd1	velmi
oblíben	oblíben	k2eAgMnSc1d1	oblíben
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
časech	čas	k1gInPc6	čas
hobiti	hobit	k1gMnPc1	hobit
poněkud	poněkud	k6eAd1	poněkud
více	hodně	k6eAd2	hodně
začali	začít	k5eAaPmAgMnP	začít
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Hobit	hobit	k1gMnSc1	hobit
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Sméagol-Glum	Sméagol-Glum	k1gNnSc1	Sméagol-Glum
<g/>
:	:	kIx,	:
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hobitů	hobit	k1gMnPc2	hobit
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
nevydali	vydat	k5eNaPmAgMnP	vydat
přes	přes	k7c4	přes
Mlžné	mlžný	k2eAgFnPc4d1	mlžná
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
sídlili	sídlit	k5eAaImAgMnP	sídlit
stále	stále	k6eAd1	stále
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Anduiny	Anduina	k1gFnSc2	Anduina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
Déagol	Déagol	k1gInSc1	Déagol
nalezl	nalézt	k5eAaBmAgInS	nalézt
v	v	k7c6	v
r.	r.	kA	r.
2463	[number]	k4	2463
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
v	v	k7c6	v
Anduině	Anduina	k1gFnSc6	Anduina
Prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Sméagol	Sméagol	k1gInSc1	Sméagol
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
zatoužil	zatoužit	k5eAaPmAgMnS	zatoužit
<g/>
,	,	kIx,	,
Déagola	Déagola	k1gFnSc1	Déagola
zabil	zabít	k5eAaPmAgMnS	zabít
a	a	k8xC	a
Prsten	prsten	k1gInSc4	prsten
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
případ	případ	k1gInSc1	případ
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hobit	hobit	k1gMnSc1	hobit
zabil	zabít	k5eAaPmAgMnS	zabít
jiného	jiný	k2eAgMnSc4d1	jiný
hobita	hobit	k1gMnSc4	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
odnesl	odnést	k5eAaPmAgMnS	odnést
do	do	k7c2	do
temných	temný	k2eAgFnPc2d1	temná
jeskyní	jeskyně	k1gFnPc2	jeskyně
pod	pod	k7c7	pod
Mlžnými	mlžný	k2eAgFnPc7d1	mlžná
horami	hora	k1gFnPc7	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
a	a	k8xC	a
mocí	moc	k1gFnSc7	moc
Prstenu	prsten	k1gInSc2	prsten
získal	získat	k5eAaPmAgMnS	získat
nepřirozeně	přirozeně	k6eNd1	přirozeně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
život	život	k1gInSc4	život
–	–	k?	–
dožil	dožít	k5eAaPmAgMnS	dožít
se	se	k3xPyFc4	se
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
věku	věk	k1gInSc2	věk
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
hobitů	hobit	k1gMnPc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2941	[number]	k4	2941
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
Prsten	prsten	k1gInSc1	prsten
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
nalezl	naleznout	k5eAaPmAgInS	naleznout
Bilbo	Bilba	k1gFnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Sméagol	Sméagol	k1gInSc1	Sméagol
upadl	upadnout	k5eAaPmAgInS	upadnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
Saurona	Sauron	k1gMnSc2	Sauron
a	a	k8xC	a
přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
duše	duše	k1gFnSc1	duše
byla	být	k5eAaImAgFnS	být
zlá	zlý	k2eAgFnSc1d1	zlá
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
odvážil	odvážit	k5eAaPmAgMnS	odvážit
Temnému	temný	k2eAgMnSc3d1	temný
pánu	pán	k1gMnSc3	pán
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
–	–	k?	–
prokázal	prokázat	k5eAaPmAgInS	prokázat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
hobita	hobit	k1gMnSc4	hobit
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
zlomit	zlomit	k5eAaPmF	zlomit
<g/>
.	.	kIx.	.
</s>
<s>
Zalhal	zalhat	k5eAaPmAgMnS	zalhat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
zloděj	zloděj	k1gMnSc1	zloděj
Prstenu	prsten	k1gInSc2	prsten
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hobitích	hobití	k1gNnPc6	hobití
rodin	rodina	k1gFnPc2	rodina
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Anduiny	Anduina	k1gFnSc2	Anduina
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
tušil	tušit	k5eAaImAgMnS	tušit
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
polohu	poloha	k1gFnSc4	poloha
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Sauronovy	Sauronův	k2eAgInPc1d1	Sauronův
Prstenové	prstenový	k2eAgInPc1d1	prstenový
přízraky	přízrak	k1gInPc1	přízrak
vydaly	vydat	k5eAaPmAgInP	vydat
nesprávným	správný	k2eNgInSc7d1	nesprávný
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
dorazily	dorazit	k5eAaPmAgInP	dorazit
až	až	k9	až
s	s	k7c7	s
mnohaměsíčním	mnohaměsíční	k2eAgNnSc7d1	mnohaměsíční
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
propuštěn	propustit	k5eAaPmNgMnS	propustit
<g/>
,	,	kIx,	,
stopoval	stopovat	k5eAaImAgMnS	stopovat
Společenstvo	společenstvo	k1gNnSc4	společenstvo
Prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
Frodem	Frod	k1gMnSc7	Frod
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
ukázal	ukázat	k5eAaPmAgInS	ukázat
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Mordoru	Mordor	k1gInSc2	Mordor
<g/>
.	.	kIx.	.
</s>
<s>
Zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
ohních	oheň	k1gInPc6	oheň
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
:	:	kIx,	:
Hobit	hobit	k1gMnSc1	hobit
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
knihy	kniha	k1gFnSc2	kniha
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
cesta	cesta	k1gFnSc1	cesta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
<g/>
,	,	kIx,	,
vnuk	vnuk	k1gMnSc1	vnuk
Gandalfova	Gandalfův	k2eAgMnSc2d1	Gandalfův
přítele	přítel	k1gMnSc2	přítel
Starého	Starý	k1gMnSc2	Starý
Brala	brát	k5eAaImAgFnS	brát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
původem	původ	k1gInSc7	původ
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
a	a	k8xC	a
usedlé	usedlý	k2eAgFnSc2d1	usedlá
rodiny	rodina	k1gFnSc2	rodina
Pytlíků	pytlík	k1gMnPc2	pytlík
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
hobitem	hobit	k1gMnSc7	hobit
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
významně	významně	k6eAd1	významně
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
Středozemě	Středozem	k1gFnSc2	Středozem
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
Gandalf	Gandalf	k1gInSc4	Gandalf
vybral	vybrat	k5eAaPmAgMnS	vybrat
jako	jako	k9	jako
společníka	společník	k1gMnSc4	společník
Thorina	Thorino	k1gNnSc2	Thorino
Pavézy	pavéza	k1gFnSc2	pavéza
<g/>
,	,	kIx,	,
trpasličího	trpasličí	k2eAgMnSc2d1	trpasličí
krále	král	k1gMnSc2	král
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
výpravu	výprava	k1gFnSc4	výprava
k	k	k7c3	k
Osamělé	osamělý	k2eAgFnSc3d1	osamělá
hoře	hora	k1gFnSc3	hora
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
trpaslíkům	trpaslík	k1gMnPc3	trpaslík
zabral	zabrat	k5eAaPmAgInS	zabrat
drak	drak	k1gInSc1	drak
Šmak	šmak	k1gInSc1	šmak
<g/>
.	.	kIx.	.
</s>
<s>
Gandalf	Gandalf	k1gMnSc1	Gandalf
později	pozdě	k6eAd2	pozdě
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc4	jeho
pohnutky	pohnutka	k1gFnPc4	pohnutka
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
přiměl	přimět	k5eAaPmAgInS	přimět
hobita	hobit	k1gMnSc4	hobit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
spojencem	spojenec	k1gMnSc7	spojenec
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgInP	být
zcela	zcela	k6eAd1	zcela
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
ani	ani	k8xC	ani
jemu	on	k3xPp3gNnSc3	on
samotnému	samotný	k2eAgNnSc3d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Popudil	popudit	k5eAaPmAgMnS	popudit
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
Thorinův	Thorinův	k2eAgInSc4d1	Thorinův
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
hobitům	hobit	k1gMnPc3	hobit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
směšné	směšný	k2eAgMnPc4d1	směšný
zelináře	zelinář	k1gMnPc4	zelinář
a	a	k8xC	a
zbabělce	zbabělec	k1gMnPc4	zbabělec
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
mu	on	k3xPp3gMnSc3	on
chtěl	chtít	k5eAaImAgMnS	chtít
dokázat	dokázat	k5eAaPmF	dokázat
jejich	jejich	k3xOp3gFnSc4	jejich
cenu	cena	k1gFnSc4	cena
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
hobiti	hobit	k1gMnPc1	hobit
mají	mít	k5eAaImIp3nP	mít
nesmírně	smírně	k6eNd1	smírně
tichý	tichý	k2eAgInSc4d1	tichý
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výpravě	výprava	k1gFnSc3	výprava
užitečné	užitečný	k2eAgFnPc1d1	užitečná
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
hobiti	hobit	k1gMnPc1	hobit
nějak	nějak	k6eAd1	nějak
zapojili	zapojit	k5eAaPmAgMnP	zapojit
do	do	k7c2	do
středozemských	středozemský	k2eAgFnPc2d1	středozemská
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
si	se	k3xPyFc3	se
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
připravili	připravit	k5eAaPmAgMnP	připravit
se	se	k3xPyFc4	se
na	na	k7c4	na
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vzestupujícího	vzestupující	k2eAgMnSc4d1	vzestupující
Temného	temný	k2eAgMnSc4d1	temný
pána	pán	k1gMnSc4	pán
<g/>
.	.	kIx.	.
</s>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
během	během	k7c2	během
výpravy	výprava	k1gFnSc2	výprava
našel	najít	k5eAaPmAgMnS	najít
Sméagolův	Sméagolův	k2eAgInSc4d1	Sméagolův
Prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
mnohokrát	mnohokrát	k6eAd1	mnohokrát
pomohl	pomoct	k5eAaPmAgInS	pomoct
trpaslíkům	trpaslík	k1gMnPc3	trpaslík
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
šťastně	šťastně	k6eAd1	šťastně
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Roklince	roklinka	k1gFnSc6	roklinka
<g/>
.	.	kIx.	.
</s>
<s>
Dožil	dožít	k5eAaPmAgMnS	dožít
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
131	[number]	k4	131
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
o	o	k7c6	o
rok	rok	k1gInSc1	rok
překonal	překonat	k5eAaPmAgMnS	překonat
Starého	Starého	k2eAgFnSc7d1	Starého
Brala	brát	k5eAaImAgFnS	brát
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejstarším	starý	k2eAgMnSc7d3	nejstarší
hobitem	hobit	k1gMnSc7	hobit
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
Pytlík	pytlík	k1gMnSc1	pytlík
<g/>
:	:	kIx,	:
Bilbův	Bilbův	k2eAgMnSc1d1	Bilbův
synovec	synovec	k1gMnSc1	synovec
z	z	k7c2	z
druhého	druhý	k4xOgMnSc2	druhý
i	i	k8xC	i
třetího	třetí	k4xOgNnSc2	třetí
kolene	kolen	k1gInSc5	kolen
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
knihy	kniha	k1gFnSc2	kniha
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Bilbo	Bilba	k1gMnSc5	Bilba
se	se	k3xPyFc4	se
po	po	k7c6	po
nesmírném	smírný	k2eNgNnSc6d1	nesmírné
úsilí	úsilí	k1gNnSc6	úsilí
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zříci	zříct	k5eAaPmF	zříct
Prstenu	prsten	k1gInSc2	prsten
a	a	k8xC	a
Frodovi	Froda	k1gMnSc3	Froda
jej	on	k3xPp3gMnSc4	on
věnoval	věnovat	k5eAaImAgMnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Frodo	Frodo	k1gNnSc1	Frodo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
Temného	temný	k2eAgMnSc2d1	temný
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Prsten	prsten	k1gInSc1	prsten
vhodil	vhodit	k5eAaPmAgInS	vhodit
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
Hory	hora	k1gFnSc2	hora
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgInS	ukončit
tak	tak	k6eAd1	tak
Sauronovu	Sauronův	k2eAgFnSc4d1	Sauronova
vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Samvěd	Samvěd	k6eAd1	Samvěd
Křepelka	křepelka	k1gFnSc1	křepelka
<g/>
:	:	kIx,	:
Frodův	Frodův	k2eAgMnSc1d1	Frodův
zahradník	zahradník	k1gMnSc1	zahradník
a	a	k8xC	a
společník	společník	k1gMnSc1	společník
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
starostou	starosta	k1gMnSc7	starosta
Kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
blízkým	blízký	k2eAgMnSc7d1	blízký
přítelem	přítel	k1gMnSc7	přítel
krále	král	k1gMnSc2	král
Elessara	Elessar	k1gMnSc2	Elessar
<g/>
.	.	kIx.	.
</s>
<s>
Peregrin	Peregrin	k1gInSc4	Peregrin
Bral	brát	k5eAaImAgMnS	brát
<g/>
:	:	kIx,	:
Frodův	Frodův	k2eAgMnSc1d1	Frodův
bratranec	bratranec	k1gMnSc1	bratranec
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Války	válka	k1gFnSc2	válka
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
zachránil	zachránit	k5eAaPmAgInS	zachránit
život	život	k1gInSc1	život
Faramirovi	Faramir	k1gMnSc3	Faramir
<g/>
,	,	kIx,	,
synu	syn	k1gMnSc3	syn
gondorského	gondorský	k2eAgMnSc2d1	gondorský
správce	správce	k1gMnSc2	správce
Denethora	Denethor	k1gMnSc2	Denethor
a	a	k8xC	a
také	také	k6eAd1	také
gondorskému	gondorský	k2eAgMnSc3d1	gondorský
rytíři	rytíř	k1gMnSc3	rytíř
Beregondovi	Beregond	k1gMnSc3	Beregond
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
gondorským	gondorský	k2eAgMnSc7d1	gondorský
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Povodí	povodí	k1gNnSc2	povodí
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Velkým	velký	k2eAgMnSc7d1	velký
Bralem	Bral	k1gMnSc7	Bral
a	a	k8xC	a
vladykou	vladyka	k1gMnSc7	vladyka
Kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Smělmírem	Smělmír	k1gMnSc7	Smělmír
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
hobity	hobit	k1gMnPc7	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Smělmír	Smělmír	k1gMnSc1	Smělmír
Brandorád	Brandoráda	k1gFnPc2	Brandoráda
<g/>
:	:	kIx,	:
Frodův	Frodův	k2eAgMnSc1d1	Frodův
bratranec	bratranec	k1gMnSc1	bratranec
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Společenstva	společenstvo	k1gNnSc2	společenstvo
prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Války	válka	k1gFnSc2	válka
o	o	k7c4	o
Prsten	prsten	k1gInSc4	prsten
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc4	život
Éowyn	Éowyn	k1gMnSc1	Éowyn
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
jí	jíst	k5eAaImIp3nS	jíst
zabít	zabít	k5eAaPmF	zabít
Pána	pán	k1gMnSc4	pán
nazgû	nazgû	k?	nazgû
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
své	svůj	k3xOyFgFnSc2	svůj
služby	služba	k1gFnSc2	služba
byl	být	k5eAaImAgMnS	být
pasován	pasovat	k5eAaImNgMnS	pasovat
rohanským	rohanský	k2eAgMnSc7d1	rohanský
jezdcem	jezdec	k1gMnSc7	jezdec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Kraje	kraj	k1gInSc2	kraj
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Povodí	povodí	k1gNnSc2	povodí
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zabil	zabít	k5eAaPmAgMnS	zabít
vůdce	vůdce	k1gMnSc1	vůdce
Sarumanových	Sarumanův	k2eAgMnPc2d1	Sarumanův
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Pánem	pán	k1gMnSc7	pán
Rádovska	Rádovsko	k1gNnSc2	Rádovsko
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Peregrinem	Peregrin	k1gInSc7	Peregrin
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nejvyššími	vysoký	k2eAgMnPc7d3	nejvyšší
hobity	hobit	k1gMnPc7	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
si	se	k3xPyFc3	se
tradičně	tradičně	k6eAd1	tradičně
budují	budovat	k5eAaImIp3nP	budovat
bydliště	bydliště	k1gNnSc4	bydliště
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
sami	sám	k3xTgMnPc1	sám
nazývají	nazývat	k5eAaImIp3nP	nazývat
pelouchy	pelouch	k1gInPc4	pelouch
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
pelouchem	pelouch	k1gInSc7	pelouch
je	být	k5eAaImIp3nS	být
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Dno	dno	k1gNnSc1	dno
pytle	pytel	k1gInSc2	pytel
rodiny	rodina	k1gFnSc2	rodina
pytlíků	pytlík	k1gMnPc2	pytlík
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
jinak	jinak	k6eAd1	jinak
běžné	běžný	k2eAgInPc1d1	běžný
pelouchy	pelouch	k1gInPc1	pelouch
<g/>
.	.	kIx.	.
</s>
<s>
Hobití	Hobití	k1gNnSc1	Hobití
pelouch	pelouch	k1gInSc1	pelouch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
dobře	dobře	k6eAd1	dobře
rozvržený	rozvržený	k2eAgInSc1d1	rozvržený
<g/>
,	,	kIx,	,
teplý	teplý	k2eAgInSc1d1	teplý
<g/>
,	,	kIx,	,
útulný	útulný	k2eAgInSc1d1	útulný
<g/>
,	,	kIx,	,
pohodlný	pohodlný	k2eAgInSc1d1	pohodlný
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
okázalý	okázalý	k2eAgMnSc1d1	okázalý
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
hobité	hobita	k1gMnPc1	hobita
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
svých	svůj	k3xOyFgInPc2	svůj
pelouchů	pelouch	k1gInPc2	pelouch
drží	držet	k5eAaImIp3nS	držet
jednoho	jeden	k4xCgInSc2	jeden
základního	základní	k2eAgInSc2d1	základní
plánu	plán	k1gInSc2	plán
jen	jen	k9	jen
v	v	k7c6	v
mírných	mírný	k2eAgFnPc6d1	mírná
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
úbočí	úbočí	k1gNnSc2	úbočí
kopce	kopec	k1gInSc2	kopec
bývá	bývat	k5eAaImIp3nS	bývat
vykopán	vykopán	k2eAgInSc1d1	vykopán
hlavní	hlavní	k2eAgInSc1d1	hlavní
tunel	tunel	k1gInSc1	tunel
čili	čili	k8xC	čili
pelouch	pelouch	k1gInSc1	pelouch
a	a	k8xC	a
následně	následně	k6eAd1	následně
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
tohoto	tento	k3xDgInSc2	tento
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
místnosti	místnost	k1gFnPc1	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ústřední	ústřední	k2eAgInSc1d1	ústřední
tunel	tunel	k1gInSc1	tunel
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
hlouben	hloubit	k5eAaImNgInS	hloubit
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
se	s	k7c7	s
svahem	svah	k1gInSc7	svah
kopce	kopec	k1gInSc2	kopec
a	a	k8xC	a
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
vnořuje	vnořovat	k5eAaImIp3nS	vnořovat
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavní	hlavní	k2eAgFnPc4d1	hlavní
dveře	dveře	k1gFnPc4	dveře
a	a	k8xC	a
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
zas	zas	k6eAd1	zas
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dveře	dveře	k1gFnPc1	dveře
zadní	zadní	k2eAgFnPc1d1	zadní
<g/>
.	.	kIx.	.
</s>
<s>
Místnosti	místnost	k1gFnSc2	místnost
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
svahu	svah	k1gInSc3	svah
mají	mít	k5eAaImIp3nP	mít
kulatá	kulatý	k2eAgNnPc4d1	kulaté
okna	okno	k1gNnPc4	okno
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
místnosti	místnost	k1gFnSc2	místnost
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
chodby	chodba	k1gFnSc2	chodba
okna	okno	k1gNnSc2	okno
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hobití	hobití	k1gNnSc6	hobití
domácnosti	domácnost	k1gFnSc2	domácnost
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nenajdou	najít	k5eNaPmIp3nP	najít
žádné	žádný	k3yNgInPc4	žádný
schody	schod	k1gInPc4	schod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pelouchu	pelouch	k1gInSc6	pelouch
nesmí	smět	k5eNaImIp3nS	smět
chybět	chybět	k5eAaImF	chybět
prostorná	prostorný	k2eAgFnSc1d1	prostorná
spižírna	spižírna	k1gFnSc1	spižírna
<g/>
,	,	kIx,	,
kuchyně	kuchyně	k1gFnSc1	kuchyně
a	a	k8xC	a
sklepy	sklep	k1gInPc1	sklep
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc4	pití
ale	ale	k8xC	ale
také	také	k9	také
šatníky	šatník	k1gInPc1	šatník
<g/>
,	,	kIx,	,
skříně	skříň	k1gFnPc1	skříň
<g/>
,	,	kIx,	,
truhly	truhla	k1gFnPc1	truhla
a	a	k8xC	a
šuplíky	šuplíky	k?	šuplíky
na	na	k7c6	na
oblečení	oblečení	k1gNnSc6	oblečení
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
všechny	všechen	k3xTgMnPc4	všechen
hobity	hobit	k1gMnPc4	hobit
nashromážděné	nashromážděný	k2eAgFnSc2d1	nashromážděná
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
krásné	krásný	k2eAgFnPc4d1	krásná
podlahy	podlaha	k1gFnPc4	podlaha
z	z	k7c2	z
dlaždic	dlaždice	k1gFnPc2	dlaždice
a	a	k8xC	a
s	s	k7c7	s
koberci	koberec	k1gInPc7	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
bývají	bývat	k5eAaImIp3nP	bývat
obložené	obložený	k2eAgFnPc4d1	obložená
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Pokoje	pokoj	k1gInPc1	pokoj
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
útulné	útulný	k2eAgInPc1d1	útulný
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
větrané	větraný	k2eAgFnPc1d1	větraná
i	i	k8xC	i
dobře	dobře	k6eAd1	dobře
osvětlené	osvětlený	k2eAgFnPc1d1	osvětlená
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vytápěné	vytápěný	k2eAgInPc1d1	vytápěný
velkými	velký	k2eAgInPc7d1	velký
krby	krb	k1gInPc7	krb
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
krásný	krásný	k2eAgInSc4d1	krásný
nábytek	nábytek	k1gInSc4	nábytek
naleštěný	naleštěný	k2eAgInSc4d1	naleštěný
<g/>
,	,	kIx,	,
nalakovaný	nalakovaný	k2eAgInSc4d1	nalakovaný
a	a	k8xC	a
vyčalouněný	vyčalouněný	k2eAgInSc4d1	vyčalouněný
právě	právě	k9	právě
pro	pro	k7c4	pro
co	co	k3yQnSc4	co
největší	veliký	k2eAgNnSc4d3	veliký
pohodlí	pohodlí	k1gNnSc4	pohodlí
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
kouřili	kouřit	k5eAaImAgMnP	kouřit
dýmku	dýmka	k1gFnSc4	dýmka
s	s	k7c7	s
vonnými	vonný	k2eAgFnPc7d1	vonná
bylinami	bylina	k1gFnPc7	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
národem	národ	k1gInSc7	národ
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kouření	kouření	k1gNnSc4	kouření
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
<g/>
,	,	kIx,	,
počátky	počátek	k1gInPc1	počátek
kouření	kouření	k1gNnSc2	kouření
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zapomenuty	zapomnět	k5eAaImNgInP	zapomnět
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
hobitem	hobit	k1gMnSc7	hobit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vypěstoval	vypěstovat	k5eAaPmAgMnS	vypěstovat
pravé	pravý	k2eAgNnSc4d1	pravé
dýmkové	dýmkový	k2eAgNnSc4d1	dýmkové
koření	koření	k1gNnSc4	koření
byl	být	k5eAaImAgInS	být
Tobold	Tobold	k1gInSc1	Tobold
Troubil	troubit	k5eAaImAgInS	troubit
z	z	k7c2	z
Dolan	Dolany	k1gInPc2	Dolany
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
2670	[number]	k4	2670
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
kuřivo	kuřivo	k1gNnSc4	kuřivo
používal	používat	k5eAaImAgInS	používat
listí	listí	k1gNnSc4	listí
rostliny	rostlina	k1gFnSc2	rostlina
galenasu	galenas	k1gInSc2	galenas
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
hobiti	hobit	k1gMnPc1	hobit
nazývali	nazývat	k5eAaImAgMnP	nazývat
západní	západní	k2eAgInSc4d1	západní
kořen	kořen	k1gInSc4	kořen
a	a	k8xC	a
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
byla	být	k5eAaImAgFnS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Númenoru	Númenor	k1gInSc2	Númenor
<g/>
.	.	kIx.	.
</s>
<s>
Rostla	růst	k5eAaImAgFnS	růst
i	i	k9	i
v	v	k7c6	v
Gondoru	Gondor	k1gInSc6	Gondor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
lidé	člověk	k1gMnPc1	člověk
znali	znát	k5eAaImAgMnP	znát
jen	jen	k9	jen
pro	pro	k7c4	pro
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
vůni	vůně	k1gFnSc4	vůně
jejích	její	k3xOp3gInPc2	její
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Tobold	Tobold	k1gMnSc1	Tobold
vypěstoval	vypěstovat	k5eAaPmAgMnS	vypěstovat
několik	několik	k4yIc4	několik
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgNnSc4d3	nejznámější
bylo	být	k5eAaImAgNnS	být
Dolanské	Dolanské	k2eAgNnSc1d1	Dolanské
listí	listí	k1gNnSc1	listí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
dýmkové	dýmkový	k2eAgNnSc4d1	dýmkové
koření	koření	k1gNnSc4	koření
ve	v	k7c6	v
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
prvním	první	k4xOgInPc3	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
od	od	k7c2	od
hobitů	hobit	k1gMnPc2	hobit
převzal	převzít	k5eAaPmAgInS	převzít
zvyk	zvyk	k1gInSc1	zvyk
kouřit	kouřit	k5eAaImF	kouřit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
čaroděj	čaroděj	k1gMnSc1	čaroděj
Gandalf	Gandalf	k1gMnSc1	Gandalf
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
velmi	velmi	k6eAd1	velmi
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
nebývalou	bývalý	k2eNgFnSc4d1	bývalý
zručnost	zručnost	k1gFnSc4	zručnost
ve	v	k7c6	v
vyfukování	vyfukování	k1gNnSc6	vyfukování
kouřových	kouřový	k2eAgInPc2d1	kouřový
kroužků	kroužek	k1gInPc2	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
zapálil	zapálit	k5eAaPmAgMnS	zapálit
dýmku	dýmka	k1gFnSc4	dýmka
během	během	k7c2	během
setkání	setkání	k1gNnSc2	setkání
Rady	rada	k1gFnSc2	rada
v	v	k7c6	v
r.	r.	kA	r.
2851	[number]	k4	2851
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
Saruman	Saruman	k1gMnSc1	Saruman
mu	on	k3xPp3gMnSc3	on
vyčetl	vyčíst	k5eAaPmAgMnS	vyčíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
znevažuje	znevažovat	k5eAaImIp3nS	znevažovat
jednání	jednání	k1gNnSc1	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
však	však	k9	však
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
dýmku	dýmka	k1gFnSc4	dýmka
kouřit	kouřit	k5eAaImF	kouřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bál	bát	k5eAaImAgMnS	bát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
Gandalf	Gandalf	k1gInSc1	Gandalf
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
začal	začít	k5eAaPmAgMnS	začít
Dolanské	Dolanské	k2eAgNnSc4d1	Dolanské
listí	listí	k1gNnSc4	listí
kupovat	kupovat	k5eAaImF	kupovat
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
nesmírně	smírně	k6eNd1	smírně
tajně	tajně	k6eAd1	tajně
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
však	však	k9	však
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
zvědů	zvěd	k1gMnPc2	zvěd
vypracovat	vypracovat	k5eAaPmF	vypracovat
podrobné	podrobný	k2eAgFnPc4d1	podrobná
mapy	mapa	k1gFnPc4	mapa
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
obrátil	obrátit	k5eAaPmAgMnS	obrátit
ke	k	k7c3	k
zlu	zlo	k1gNnSc3	zlo
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
sloužit	sloužit	k5eAaImF	sloužit
Sauronovi	Sauronův	k2eAgMnPc1d1	Sauronův
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
veliké	veliký	k2eAgNnSc1d1	veliké
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
od	od	k7c2	od
hobitů	hobit	k1gMnPc2	hobit
naučili	naučit	k5eAaPmAgMnP	naučit
kouřit	kouřit	k5eAaImF	kouřit
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
Hraničáři	hraničár	k1gMnPc1	hraničár
i	i	k8xC	i
jiní	jiný	k2eAgMnPc1d1	jiný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
nekouřili	kouřit	k5eNaImAgMnP	kouřit
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
o	o	k7c6	o
původu	původ	k1gInSc6	původ
dýmkového	dýmkový	k2eAgNnSc2d1	dýmkové
koření	koření	k1gNnSc2	koření
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
sepsal	sepsat	k5eAaPmAgMnS	sepsat
Smělmír	Smělmír	k1gMnSc1	Smělmír
Brandorád	Brandoráda	k1gFnPc2	Brandoráda
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
Čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
věku	věk	k1gInSc2	věk
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
Rostlinopisu	rostlinopis	k1gInSc6	rostlinopis
Kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Tobold	Tobolda	k1gFnPc2	Tobolda
Troubil	troubit	k5eAaImAgMnS	troubit
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
filologických	filologický	k2eAgInPc2d1	filologický
Tolkienových	Tolkienův	k2eAgInPc2d1	Tolkienův
žertů	žert	k1gInPc2	žert
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Tobold	Tobolda	k1gFnPc2	Tobolda
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
Theobold	Theobolda	k1gFnPc2	Theobolda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
příliš	příliš	k6eAd1	příliš
smělý	smělý	k2eAgMnSc1d1	smělý
(	(	kIx(	(
<g/>
Too	Too	k1gMnSc1	Too
Bold	Bold	k1gMnSc1	Bold
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
jako	jako	k8xS	jako
old	old	k?	old
(	(	kIx(	(
<g/>
starý	starý	k2eAgInSc4d1	starý
<g/>
)	)	kIx)	)
Toby	Tob	k2eAgFnPc1d1	Toba
ale	ale	k8xC	ale
především	především	k6eAd1	především
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
nápadně	nápadně	k6eAd1	nápadně
podobné	podobný	k2eAgNnSc1d1	podobné
slovu	slout	k5eAaImIp1nS	slout
Tobacco	Tobacco	k1gNnSc1	Tobacco
(	(	kIx(	(
<g/>
tabák	tabák	k1gInSc1	tabák
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tolkien	Tolkien	k1gInSc1	Tolkien
tímto	tento	k3xDgNnSc7	tento
jakoby	jakoby	k8xS	jakoby
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
žert	žert	k1gInSc4	žert
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
známé	známý	k2eAgNnSc1d1	známé
slovo	slovo	k1gNnSc1	slovo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
vlastně	vlastně	k9	vlastně
až	až	k9	až
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
tohoto	tento	k3xDgMnSc2	tento
hobita	hobit	k1gMnSc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
z	z	k7c2	z
Kraje	kraj	k1gInSc2	kraj
zavedli	zavést	k5eAaPmAgMnP	zavést
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
letopočet	letopočet	k1gInSc4	letopočet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začínal	začínat	k5eAaImAgInS	začínat
rokem	rok	k1gInSc7	rok
založení	založení	k1gNnSc2	založení
Kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
rok	rok	k1gInSc1	rok
1	[number]	k4	1
krajového	krajový	k2eAgInSc2d1	krajový
letopočtu	letopočet	k1gInSc2	letopočet
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
roku	rok	k1gInSc2	rok
1601	[number]	k4	1601
Třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
užívali	užívat	k5eAaImAgMnP	užívat
vlastní	vlastní	k2eAgInPc4d1	vlastní
názvy	název	k1gInPc4	název
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
měsíc	měsíc	k1gInSc1	měsíc
měl	mít	k5eAaImAgInS	mít
třicet	třicet	k4xCc4	třicet
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
kolem	kolem	k7c2	kolem
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
(	(	kIx(	(
<g/>
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k9	jako
radostniny	radostnina	k1gFnSc2	radostnina
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
roku	rok	k1gInSc2	rok
jako	jako	k8xS	jako
novorok	novorok	k1gInSc4	novorok
<g/>
.	.	kIx.	.
</s>
<s>
Radostniny	Radostnin	k1gInPc1	Radostnin
a	a	k8xC	a
novorok	novorok	k1gInSc1	novorok
byly	být	k5eAaImAgInP	být
obdobím	období	k1gNnSc7	období
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
nepatřily	patřit	k5eNaImAgFnP	patřit
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
měsíci	měsíc	k1gInSc3	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hobití	hobití	k1gNnSc3	hobití
zálibě	záliba	k1gFnSc6	záliba
v	v	k7c6	v
jednoduchosti	jednoduchost	k1gFnSc6	jednoduchost
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
slunovrat	slunovrat	k1gInSc1	slunovrat
(	(	kIx(	(
<g/>
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
po	po	k7c6	po
slunovratu	slunovrat	k1gInSc6	slunovrat
<g/>
)	)	kIx)	)
nebude	být	k5eNaImBp3nS	být
patřit	patřit	k5eAaImF	patřit
ani	ani	k8xC	ani
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
dni	den	k1gInSc3	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
roce	rok	k1gInSc6	rok
připadal	připadat	k5eAaPmAgInS	připadat
k	k	k7c3	k
určitému	určitý	k2eAgNnSc3d1	určité
datu	datum	k1gNnSc3	datum
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Náhodou	náhodou	k6eAd1	náhodou
tak	tak	k9	tak
žádný	žádný	k3yNgInSc4	žádný
měsíc	měsíc	k1gInSc4	měsíc
nezačínal	začínat	k5eNaImAgMnS	začínat
pátkem	pátek	k1gInSc7	pátek
<g/>
;	;	kIx,	;
hobitím	hobití	k1gNnPc3	hobití
žertem	žertem	k6eAd1	žertem
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
pátku	pátek	k1gInSc6	pátek
prvního	první	k4xOgInSc2	první
<g/>
"	"	kIx"	"
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
až	až	k6eAd1	až
naprší	napršet	k5eAaPmIp3nS	napršet
a	a	k8xC	a
uschne	uschnout	k5eAaPmIp3nS	uschnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
J.	J.	kA	J.
<g/>
R.	R.	kA	R.
<g/>
R.	R.	kA	R.
Tolkien	Tolkien	k1gInSc1	Tolkien
vymýšlel	vymýšlet	k5eAaImAgInS	vymýšlet
své	svůj	k3xOyFgInPc4	svůj
příběhy	příběh	k1gInPc4	příběh
často	často	k6eAd1	často
dle	dle	k7c2	dle
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
si	se	k3xPyFc3	se
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
s	s	k7c7	s
jakými	jaký	k3yRgNnPc7	jaký
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
filologicky	filologicky	k6eAd1	filologicky
podobné	podobný	k2eAgInPc1d1	podobný
a	a	k8xC	a
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
a	a	k8xC	a
dle	dle	k7c2	dle
tohoto	tento	k3xDgInSc2	tento
vymýšlel	vymýšlet	k5eAaImAgInS	vymýšlet
často	často	k6eAd1	často
celou	celý	k2eAgFnSc4d1	celá
charakteristiku	charakteristika	k1gFnSc4	charakteristika
postav	postava	k1gFnPc2	postava
i	i	k8xC	i
hlavní	hlavní	k2eAgInPc4d1	hlavní
rysy	rys	k1gInPc4	rys
a	a	k8xC	a
motivy	motiv	k1gInPc4	motiv
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
hobit	hobit	k1gMnSc1	hobit
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
žádnou	žádný	k3yNgFnSc7	žádný
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hledání	hledání	k1gNnSc6	hledání
Tolkienovi	Tolkien	k1gMnSc3	Tolkien
inspirace	inspirace	k1gFnSc1	inspirace
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
podívat	podívat	k5eAaPmF	podívat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
kdy	kdy	k6eAd1	kdy
vydal	vydat	k5eAaPmAgMnS	vydat
spisovatel	spisovatel	k1gMnSc1	spisovatel
George	Georg	k1gInSc2	Georg
MacDonald	Macdonald	k1gMnSc1	Macdonald
svou	svůj	k3xOyFgFnSc4	svůj
pohádku	pohádka	k1gFnSc4	pohádka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Princezna	princezna	k1gFnSc1	princezna
a	a	k8xC	a
skřítek	skřítek	k1gMnSc1	skřítek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pohádka	pohádka	k1gFnSc1	pohádka
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
nejvíce	hodně	k6eAd3	hodně
o	o	k7c6	o
střetech	střet	k1gInPc6	střet
mezi	mezi	k7c7	mezi
permoníky	permoník	k1gMnPc7	permoník
a	a	k8xC	a
skřety	skřet	k1gMnPc7	skřet
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
chodbách	chodba	k1gFnPc6	chodba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
se	se	k3xPyFc4	se
zpívá	zpívat	k5eAaImIp3nS	zpívat
písnička	písnička	k1gFnSc1	písnička
jenž	jenž	k3xRgMnSc1	jenž
začíná	začínat	k5eAaImIp3nS	začínat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
skřítek	skřítek	k1gMnSc1	skřítek
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
při	při	k7c6	při
opravování	opravování	k1gNnSc6	opravování
maturitních	maturitní	k2eAgFnPc2d1	maturitní
prací	práce	k1gFnPc2	práce
napsal	napsat	k5eAaPmAgInS	napsat
již	již	k6eAd1	již
Tolkien	Tolkien	k2eAgInSc1d1	Tolkien
na	na	k7c4	na
prázdnou	prázdný	k2eAgFnSc4d1	prázdná
stránku	stránka	k1gFnSc4	stránka
papíru	papír	k1gInSc2	papír
"	"	kIx"	"
<g/>
V	v	k7c6	v
jisté	jistý	k2eAgFnSc6d1	jistá
podzemní	podzemní	k2eAgFnSc6d1	podzemní
noře	nora	k1gFnSc6	nora
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
jeden	jeden	k4xCgMnSc1	jeden
hobit	hobit	k1gMnSc1	hobit
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
sám	sám	k3xTgMnSc1	sám
uvedl	uvést	k5eAaPmAgMnS	uvést
ho	on	k3xPp3gNnSc2	on
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byli	být	k5eAaImAgMnP	být
hobiti	hobit	k1gMnPc1	hobit
zač	zač	k6eAd1	zač
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
malý	malý	k2eAgInSc4d1	malý
filologický	filologický	k2eAgInSc4d1	filologický
žert	žert	k1gInSc4	žert
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
hobit	hobit	k1gMnSc1	hobit
totiž	totiž	k9	totiž
bylo	být	k5eAaImAgNnS	být
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
podobou	podoba	k1gFnSc7	podoba
slova	slovo	k1gNnSc2	slovo
holbytla	holbytnout	k5eAaPmAgFnS	holbytnout
utvořeného	utvořený	k2eAgInSc2d1	utvořený
dle	dle	k7c2	dle
staré	starý	k2eAgFnSc2d1	stará
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
znamenajícího	znamenající	k2eAgInSc2d1	znamenající
stavitel	stavitel	k1gMnSc1	stavitel
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Novoanglické	Novoanglický	k2eAgNnSc1d1	Novoanglický
slovo	slovo	k1gNnSc1	slovo
hole	hole	k1gFnSc1	hole
(	(	kIx(	(
<g/>
nora	nora	k1gFnSc1	nora
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staroanglického	staroanglický	k2eAgInSc2d1	staroanglický
hollow	hollow	k?	hollow
což	což	k3yRnSc4	což
zas	zas	k6eAd1	zas
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staroněmeckého	staroněmecký	k2eAgNnSc2d1	staroněmecké
hohl	hohnout	k5eAaPmAgMnS	hohnout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
novoanglické	novoanglický	k2eAgNnSc4d1	novoanglické
hole	hole	k1gNnSc4	hole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oxfordském	oxfordský	k2eAgInSc6d1	oxfordský
slovníku	slovník	k1gInSc6	slovník
následuje	následovat	k5eAaImIp3nS	následovat
slovo	slovo	k1gNnSc4	slovo
hobit	hobit	k1gMnSc1	hobit
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
slovu	slovo	k1gNnSc6	slovo
hoax	hoax	k1gInSc4	hoax
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
forma	forma	k1gFnSc1	forma
slova	slovo	k1gNnSc2	slovo
hokus-pokus	hokusokus	k1gInSc1	hokus-pokus
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
znamená	znamenat	k5eAaImIp3nS	znamenat
spíše	spíše	k9	spíše
podvrh	podvrh	k1gInSc1	podvrh
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgInSc1d1	kanadský
žert	žert	k1gInSc1	žert
či	či	k8xC	či
smyšlený	smyšlený	k2eAgInSc1d1	smyšlený
příběh	příběh	k1gInSc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
tvořil	tvořit	k5eAaImAgInS	tvořit
charakteristiku	charakteristika	k1gFnSc4	charakteristika
této	tento	k3xDgFnSc2	tento
rasy	rasa	k1gFnSc2	rasa
dle	dle	k7c2	dle
prvních	první	k4xOgNnPc2	první
třinácti	třináct	k4xCc3	třináct
slov	slovo	k1gNnPc2	slovo
ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
následujících	následující	k2eAgInPc2d1	následující
po	po	k7c6	po
slově	slovo	k1gNnSc6	slovo
hoax	hoax	k1gInSc4	hoax
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
po	po	k7c6	po
slově	slovo	k1gNnSc6	slovo
hobit	hobit	k1gMnSc1	hobit
<g/>
)	)	kIx)	)
Hob	Hob	k1gMnSc1	Hob
-	-	kIx~	-
skřítek	skřítek	k1gMnSc1	skřítek
či	či	k8xC	či
elf	elf	k1gMnSc1	elf
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
původního	původní	k2eAgInSc2d1	původní
významu	význam	k1gInSc2	význam
slova	slovo	k1gNnSc2	slovo
elf	elf	k1gMnSc1	elf
<g/>
)	)	kIx)	)
či	či	k8xC	či
samec	samec	k1gMnSc1	samec
fretky	fretka	k1gFnSc2	fretka
napůl	napůl	k6eAd1	napůl
ochočený	ochočený	k2eAgInSc4d1	ochočený
a	a	k8xC	a
používaný	používaný	k2eAgInSc4d1	používaný
k	k	k7c3	k
vyhánění	vyhánění	k1gNnSc3	vyhánění
králíků	králík	k1gMnPc2	králík
z	z	k7c2	z
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
hob	hob	k?	hob
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
původně	původně	k6eAd1	původně
dolnoněmeckého	dolnoněmecký	k2eAgNnSc2d1	dolnoněmecké
slova	slovo	k1gNnSc2	slovo
hump	hump	k1gInSc1	hump
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
kopec	kopec	k1gInSc4	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Australské	australský	k2eAgFnSc2d1	australská
humpy	humpa	k1gFnSc2	humpa
je	být	k5eAaImIp3nS	být
pak	pak	k9	pak
významu	význam	k1gInSc2	význam
chata	chata	k1gFnSc1	chata
<g/>
.	.	kIx.	.
</s>
<s>
Hob	Hob	k?	Hob
je	být	k5eAaImIp3nS	být
také	také	k9	také
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bydlí	bydlet	k5eAaImIp3nS	bydlet
v	v	k7c6	v
dutém	dutý	k2eAgInSc6d1	dutý
kopci	kopec	k1gInSc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Hob	Hob	k?	Hob
nebo	nebo	k8xC	nebo
také	také	k9	také
Hobman	Hobman	k1gMnSc1	Hobman
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
různé	různý	k2eAgMnPc4d1	různý
chlupaté	chlupatý	k1gMnPc4	chlupatý
asi	asi	k9	asi
tři	tři	k4xCgFnPc1	tři
stopy	stopa	k1gFnPc1	stopa
vysoké	vysoký	k2eAgMnPc4d1	vysoký
dobrotivé	dobrotivý	k2eAgMnPc4d1	dobrotivý
skřítky	skřítek	k1gMnPc4	skřítek
podobné	podobný	k2eAgNnSc1d1	podobné
člověku	člověk	k1gMnSc6	člověk
Hobbledehoy	Hobbledehoa	k1gFnSc2	Hobbledehoa
-	-	kIx~	-
dospívající	dospívající	k2eAgFnSc2d1	dospívající
<g/>
,	,	kIx,	,
poloviční	poloviční	k2eAgMnSc1d1	poloviční
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
poloviční	poloviční	k2eAgMnSc1d1	poloviční
chlapec	chlapec	k1gMnSc1	chlapec
Hobnob-	Hobnob-	k1gMnSc1	Hobnob-
společně	společně	k6eAd1	společně
pít	pít	k5eAaImF	pít
a	a	k8xC	a
hovořit	hovořit	k5eAaImF	hovořit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
klábosit	klábosit	k5eAaImF	klábosit
<g/>
)	)	kIx)	)
Hobbyhorse	Hobbyhorse	k1gFnSc1	Hobbyhorse
-	-	kIx~	-
lidový	lidový	k2eAgMnSc1d1	lidový
tanečník	tanečník	k1gMnSc1	tanečník
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
Hobble	Hobble	k1gFnSc2	Hobble
-	-	kIx~	-
zmást	zmást	k5eAaPmF	zmást
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k8xC	i
hobití	hobití	k1gNnSc4	hobití
náklonnost	náklonnost	k1gFnSc1	náklonnost
k	k	k7c3	k
hádankám	hádanka	k1gFnPc3	hádanka
<g/>
)	)	kIx)	)
Hoblike	Hoblike	k1gFnSc3	Hoblike
-	-	kIx~	-
přihlouplý	přihlouplý	k2eAgInSc4d1	přihlouplý
<g/>
,	,	kIx,	,
neotesaný	otesaný	k2eNgInSc4d1	neotesaný
Hobnail	Hobnail	k1gInSc4	Hobnail
-	-	kIx~	-
vesnický	vesnický	k2eAgMnSc1d1	vesnický
buran	buran	k1gMnSc1	buran
Hobble	Hobble	k1gMnSc1	Hobble
-	-	kIx~	-
překážet	překážet	k5eAaImF	překážet
a	a	k8xC	a
činit	činit	k5eAaImF	činit
těžkosti	těžkost	k1gFnPc4	těžkost
Hobbyhorsical	Hobbyhorsical	k1gFnSc2	Hobbyhorsical
-	-	kIx~	-
zábavný	zábavný	k2eAgInSc1d1	zábavný
<g/>
,	,	kIx,	,
rozmařilý	rozmařilý	k2eAgInSc1d1	rozmařilý
Hobby	hobby	k1gNnSc3	hobby
-	-	kIx~	-
česky	česky	k6eAd1	česky
koníček	koníček	k1gInSc4	koníček
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
záliba	záliba	k1gFnSc1	záliba
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
většinou	většinou	k6eAd1	většinou
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
běžnému	běžný	k2eAgInSc3d1	běžný
životu	život	k1gInSc3	život
výstřední	výstřední	k2eAgFnSc2d1	výstřední
a	a	k8xC	a
ne	ne	k9	ne
primárně	primárně	k6eAd1	primárně
potřebné	potřebný	k2eAgFnPc4d1	potřebná
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
francouzského	francouzský	k2eAgMnSc2d1	francouzský
hobet	hobet	k1gInSc4	hobet
(	(	kIx(	(
<g/>
v	v	k7c6	v
Latině	latina	k1gFnSc6	latina
hobetus	hobetus	k1gMnSc1	hobetus
<g/>
)	)	kIx)	)
což	což	k3yRnSc1	což
jest	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
lovecký	lovecký	k2eAgMnSc1d1	lovecký
sokol	sokol	k1gMnSc1	sokol
<g/>
.	.	kIx.	.
</s>
<s>
Hobit	hobit	k1gMnSc1	hobit
-	-	kIx~	-
houfnice	houfnice	k1gFnSc1	houfnice
nebo	nebo	k8xC	nebo
katapult	katapult	k1gInSc1	katapult
(	(	kIx(	(
<g/>
hobiti	hobit	k1gMnPc1	hobit
jsou	být	k5eAaImIp3nP	být
mistři	mistr	k1gMnPc1	mistr
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
prakem	prak	k1gInSc7	prak
<g/>
)	)	kIx)	)
Hobel	Hobel	k1gInSc1	Hobel
-	-	kIx~	-
šíp	šíp	k1gInSc1	šíp
(	(	kIx(	(
<g/>
též	též	k9	též
jsou	být	k5eAaImIp3nP	být
dobří	dobrý	k2eAgMnPc1d1	dobrý
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
lukem	luk	k1gInSc7	luk
<g/>
)	)	kIx)	)
Hobgoblin	Hobgoblin	k1gInSc1	Hobgoblin
-	-	kIx~	-
skřítek-skřet	skřítekkřet	k5eAaImF	skřítek-skřet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
elf-skřet	elfkřet	k5eAaPmF	elf-skřet
(	(	kIx(	(
<g/>
hobité	hobita	k1gMnPc1	hobita
mají	mít	k5eAaImIp3nP	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
elfům	elf	k1gMnPc3	elf
ale	ale	k8xC	ale
špatný	špatný	k2eAgInSc4d1	špatný
se	s	k7c7	s
skřety	skřet	k1gMnPc7	skřet
<g/>
)	)	kIx)	)
Hobiler	Hobiler	k1gInSc4	Hobiler
-	-	kIx~	-
lehce	lehko	k6eAd1	lehko
ozbrojený	ozbrojený	k2eAgMnSc1d1	ozbrojený
člen	člen	k1gMnSc1	člen
domoobrany	domoobrana	k1gFnSc2	domoobrana
z	z	k7c2	z
období	období	k1gNnSc2	období
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Přísahal	přísahat	k5eAaImAgMnS	přísahat
věrnost	věrnost	k1gFnSc4	věrnost
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Nebojoval	bojovat	k5eNaImAgMnS	bojovat
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
nosil	nosit	k5eAaImAgInS	nosit
králi	král	k1gMnSc3	král
zprávy	zpráva	k1gFnSc2	zpráva
a	a	k8xC	a
vyzvídal	vyzvídat	k5eAaImAgInS	vyzvídat
Hobbiler	Hobbiler	k1gInSc1	Hobbiler
-	-	kIx~	-
feudální	feudální	k2eAgMnSc1d1	feudální
nájemní	nájemní	k2eAgMnSc1d1	nájemní
sedlák	sedlák	k1gMnSc1	sedlák
a	a	k8xC	a
válečník	válečník	k1gMnSc1	válečník
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
chluponozi	chluponoh	k1gMnPc1	chluponoh
-	-	kIx~	-
sedláci	sedlák	k1gMnPc1	sedlák
<g/>
)	)	kIx)	)
Hobbler	Hobbler	k1gMnSc1	Hobbler
-	-	kIx~	-
osoba	osoba	k1gFnSc1	osoba
vláčící	vláčící	k2eAgFnSc1d1	vláčící
člun	člun	k1gInSc4	člun
na	na	k7c4	na
laně	laň	k1gFnPc4	laň
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
statové	statové	k2eAgMnPc1d1	statové
-	-	kIx~	-
vodáci	vodák	k1gMnPc1	vodák
<g/>
)	)	kIx)	)
Hob	Hob	k1gMnSc1	Hob
<g/>
/	/	kIx~	/
<g/>
hob-i-t-hurst	hoburst	k1gMnSc1	hob-i-t-hurst
-	-	kIx~	-
lesní	lesní	k2eAgMnSc1d1	lesní
skřítek	skřítek	k1gMnSc1	skřítek
nebo	nebo	k8xC	nebo
elf	elf	k1gMnSc1	elf
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
plavíni-lesáci	plavíniesák	k1gMnPc1	plavíni-lesák
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
<g/>
)	)	kIx)	)
Hobo	Hobo	k6eAd1	Hobo
-	-	kIx~	-
pracující	pracující	k2eAgMnSc1d1	pracující
tulák	tulák	k1gMnSc1	tulák
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
hobo	hobo	k6eAd1	hobo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
hoe-boy	hoeoa	k1gFnSc2	hoe-boa
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
kluk	kluk	k1gMnSc1	kluk
s	s	k7c7	s
motykou	motyka	k1gFnSc7	motyka
tedy	tedy	k8xC	tedy
putující	putující	k2eAgMnSc1d1	putující
zemědělský	zemědělský	k2eAgMnSc1d1	zemědělský
nádeník	nádeník	k1gMnSc1	nádeník
Hobité	Hobita	k1gMnPc1	Hobita
si	se	k3xPyFc3	se
však	však	k9	však
sami	sám	k3xTgMnPc1	sám
navzájem	navzájem	k6eAd1	navzájem
říkali	říkat	k5eAaImAgMnP	říkat
kuduk	kuduk	k6eAd1	kuduk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc4	tvar
zkrácený	zkrácený	k2eAgInSc4d1	zkrácený
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
kud-dukan	kudukana	k1gFnPc2	kud-dukana
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
stavitel	stavitel	k1gMnSc1	stavitel
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
kunstrukci	kunstrukce	k1gFnSc4	kunstrukce
Tolkien	Tolkien	k1gInSc1	Tolkien
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
gótštinyn	gótštinyna	k1gFnPc2	gótštinyna
z	z	k7c2	z
pragermánského	pragermánský	k2eAgNnSc2d1	pragermánský
slova	slovo	k1gNnSc2	slovo
khulaz	khulaz	k1gInSc1	khulaz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
otvor	otvor	k1gInSc4	otvor
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
ono	onen	k3xDgNnSc1	onen
starogermánské	starogermánský	k2eAgInPc1d1	starogermánský
hohl	hohl	k1gInSc1	hohl
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
pochází	pocházet	k5eAaImIp3nS	pocházet
staroanglické	staroanglický	k2eAgInPc4d1	staroanglický
hollow	hollow	k?	hollow
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
novoanglické	novoanglický	k2eAgNnSc1d1	novoanglické
hole	hole	k1gNnSc1	hole
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
elfové	elf	k1gMnPc1	elf
označovali	označovat	k5eAaImAgMnP	označovat
hobity	hobit	k1gMnPc4	hobit
spíše	spíše	k9	spíše
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
než	než	k8xS	než
dle	dle	k7c2	dle
stavění	stavění	k1gNnSc2	stavění
a	a	k8xC	a
obývání	obývání	k1gNnSc2	obývání
nor	nora	k1gFnPc2	nora
<g/>
,	,	kIx,	,
říkali	říkat	k5eAaImAgMnP	říkat
jim	on	k3xPp3gMnPc3	on
tedy	tedy	k9	tedy
půlčíci	půlčík	k1gMnPc1	půlčík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elfské	elfský	k2eAgFnSc6d1	elfská
sindarštině	sindarština	k1gFnSc6	sindarština
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
slovo	slovo	k1gNnSc4	slovo
periananth	periananth	k1gInSc1	periananth
(	(	kIx(	(
<g/>
hobití	hobití	k1gNnSc1	hobití
národ	národ	k1gInSc1	národ
<g/>
)	)	kIx)	)
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
sindarského	sindarský	k2eAgInSc2d1	sindarský
periain	periaina	k1gFnPc2	periaina
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
půlčík	půlčík	k1gInSc1	půlčík
<g/>
)	)	kIx)	)
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
západštině	západština	k1gFnSc6	západština
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
slovo	slovo	k1gNnSc1	slovo
banakil	banakila	k1gFnPc2	banakila
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Tolkienova	Tolkienův	k2eAgMnSc2d1	Tolkienův
Pána	pán	k1gMnSc2	pán
Prstenů	prsten	k1gInPc2	prsten
byla	být	k5eAaImAgNnP	být
režisérem	režisér	k1gMnSc7	režisér
Peterem	Peter	k1gMnSc7	Peter
Jacksonem	Jackson	k1gMnSc7	Jackson
natočena	natočen	k2eAgFnSc1d1	natočena
filmová	filmový	k2eAgFnSc1d1	filmová
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
připravil	připravit	k5eAaPmAgMnS	připravit
Jackson	Jackson	k1gInSc4	Jackson
trojdílný	trojdílný	k2eAgInSc4d1	trojdílný
film	film	k1gInSc4	film
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Hobita	hobit	k1gMnSc2	hobit
jehož	jehož	k3xOyRp3gFnSc1	jehož
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
Hobit	hobit	k1gMnSc1	hobit
<g/>
:	:	kIx,	:
Neočekávaná	očekávaný	k2eNgFnSc1d1	neočekávaná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hobit	hobit	k1gMnSc1	hobit
<g/>
:	:	kIx,	:
Šmakova	Šmakův	k2eAgFnSc1d1	Šmakův
dračí	dračí	k2eAgFnSc1d1	dračí
poušť	poušť	k1gFnSc1	poušť
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
Hobit	hobit	k1gMnSc1	hobit
<g/>
:	:	kIx,	:
Bitva	bitva	k1gFnSc1	bitva
pěti	pět	k4xCc2	pět
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
diváci	divák	k1gMnPc1	divák
poprvé	poprvé	k6eAd1	poprvé
vidět	vidět	k5eAaImF	vidět
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
Hobita	hobit	k1gMnSc2	hobit
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krátký	krátký	k2eAgInSc4d1	krátký
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
režie	režie	k1gFnSc1	režie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Gene	gen	k1gInSc5	gen
Deitch	Deitch	k1gMnSc1	Deitch
<g/>
,	,	kIx,	,
animátor	animátor	k1gMnSc1	animátor
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Producentem	producent	k1gMnSc7	producent
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgMnS	být
Oscarem	Oscar	k1gInSc7	Oscar
oceněný	oceněný	k2eAgInSc4d1	oceněný
William	William	k1gInSc4	William
Snyder	Snydra	k1gFnPc2	Snydra
a	a	k8xC	a
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
filmu	film	k1gInSc2	film
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Snyder	Snyder	k1gMnSc1	Snyder
byl	být	k5eAaImAgMnS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
zfilmování	zfilmování	k1gNnSc4	zfilmování
Hobita	hobit	k1gMnSc2	hobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedařilo	dařit	k5eNaImAgNnS	dařit
splnit	splnit	k5eAaPmF	splnit
základní	základní	k2eAgFnSc4d1	základní
podmínku	podmínka	k1gFnSc4	podmínka
pro	pro	k7c4	pro
udržení	udržení	k1gNnSc3	udržení
těchto	tento	k3xDgNnPc2	tento
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
natočení	natočení	k1gNnSc1	natočení
filmu	film	k1gInSc2	film
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
využil	využít	k5eAaPmAgMnS	využít
svých	svůj	k3xOyFgMnPc2	svůj
kontaktů	kontakt	k1gInPc2	kontakt
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
animovaných	animovaný	k2eAgInPc2d1	animovaný
seriálů	seriál	k1gInPc2	seriál
pro	pro	k7c4	pro
velká	velký	k2eAgNnPc4d1	velké
hollywoodská	hollywoodský	k2eAgNnPc4d1	hollywoodské
studia	studio	k1gNnPc4	studio
(	(	kIx(	(
<g/>
např.	např.	kA	např.
i	i	k8xC	i
Tom	Tom	k1gMnSc1	Tom
a	a	k8xC	a
Jerry	Jerra	k1gFnPc1	Jerra
<g/>
)	)	kIx)	)
a	a	k8xC	a
oslovil	oslovit	k5eAaPmAgInS	oslovit
Deitche	Deitche	k1gInSc1	Deitche
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
.	.	kIx.	.
</s>
<s>
Deitch	Deitch	k1gMnSc1	Deitch
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Bornem	Born	k1gMnSc7	Born
rychle	rychle	k6eAd1	rychle
natočili	natočit	k5eAaBmAgMnP	natočit
krátký	krátký	k2eAgInSc4d1	krátký
dvanáctiminutový	dvanáctiminutový	k2eAgInSc4d1	dvanáctiminutový
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
na	na	k7c6	na
You	You	k1gFnSc6	You
Tube	tubus	k1gInSc5	tubus
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Hobit	hobit	k1gMnSc1	hobit
–	–	k?	–
Never	Never	k1gMnSc1	Never
released	released	k1gMnSc1	released
1966	[number]	k4	1966
animation	animation	k1gInSc1	animation
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Snyder	Snyder	k1gInSc1	Snyder
v	v	k7c6	v
termínu	termín	k1gInSc6	termín
promítl	promítnout	k5eAaPmAgMnS	promítnout
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc1	právo
na	na	k7c4	na
zfilmování	zfilmování	k1gNnSc4	zfilmování
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
začal	začít	k5eAaPmAgInS	začít
znovu	znovu	k6eAd1	znovu
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c4	o
natočení	natočení	k1gNnSc4	natočení
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
Hobit	hobit	k1gMnSc1	hobit
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
Jiřího	Jiří	k1gMnSc4	Jiří
Trnku	Trnka	k1gMnSc4	Trnka
.	.	kIx.	.
</s>
<s>
Trnka	Trnka	k1gMnSc1	Trnka
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
pro	pro	k7c4	pro
film	film	k1gInSc4	film
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
nestačil	stačit	k5eNaBmAgMnS	stačit
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Snyder	Snyder	k1gInSc1	Snyder
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
Hobita	hobit	k1gMnSc4	hobit
nakonec	nakonec	k6eAd1	nakonec
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
natočil	natočit	k5eAaBmAgMnS	natočit
animovanou	animovaný	k2eAgFnSc4d1	animovaná
filmovou	filmový	k2eAgFnSc4d1	filmová
verzi	verze	k1gFnSc4	verze
Hobita	hobit	k1gMnSc2	hobit
režisér	režisér	k1gMnSc1	režisér
Jules	Jules	k1gMnSc1	Jules
Bass	Bass	k1gMnSc1	Bass
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
režisérem	režisér	k1gMnSc7	režisér
Arthurem	Arthur	k1gMnSc7	Arthur
Rankinem	Rankin	k1gMnSc7	Rankin
Juniorem	junior	k1gMnSc7	junior
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
filmařů	filmař	k1gMnPc2	filmař
o	o	k7c4	o
Tolkienovo	Tolkienův	k2eAgNnSc4d1	Tolkienovo
dílo	dílo	k1gNnSc4	dílo
rozhodně	rozhodně	k6eAd1	rozhodně
nebyl	být	k5eNaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Adaptaci	adaptace	k1gFnSc4	adaptace
pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
natočit	natočit	k5eAaBmF	natočit
již	již	k6eAd1	již
Steven	Steven	k2eAgInSc4d1	Steven
Spielberg	Spielberg	k1gInSc4	Spielberg
<g/>
,	,	kIx,	,
či	či	k8xC	či
režisér	režisér	k1gMnSc1	režisér
Ridley	Ridlea	k1gFnSc2	Ridlea
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Boorman	Boorman	k1gMnSc1	Boorman
využil	využít	k5eAaPmAgMnS	využít
nakonec	nakonec	k6eAd1	nakonec
některé	některý	k3yIgInPc1	některý
kostýmy	kostým	k1gInPc1	kostým
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
rekvizity	rekvizit	k1gInPc1	rekvizit
původně	původně	k6eAd1	původně
určené	určený	k2eAgInPc1d1	určený
do	do	k7c2	do
filmového	filmový	k2eAgNnSc2d1	filmové
zpracování	zpracování	k1gNnSc2	zpracování
Pána	pán	k1gMnSc2	pán
Prstenů	prsten	k1gInPc2	prsten
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
filmu	film	k1gInSc6	film
Excalibour	Excaliboura	k1gFnPc2	Excaliboura
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Tolkien	Tolkien	k1gInSc1	Tolkien
byl	být	k5eAaImAgInS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tehdejším	tehdejší	k2eAgMnPc3d1	tehdejší
filmařům	filmař	k1gMnPc3	filmař
velmi	velmi	k6eAd1	velmi
skeptický	skeptický	k2eAgMnSc1d1	skeptický
a	a	k8xC	a
náročný	náročný	k2eAgMnSc1d1	náročný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
prodal	prodat	k5eAaPmAgInS	prodat
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
Saulu	Saul	k1gMnSc3	Saul
Zaentzovi	Zaentz	k1gMnSc3	Zaentz
pro	pro	k7c4	pro
kterého	který	k3yQgMnSc4	který
režisér	režisér	k1gMnSc1	režisér
Ralph	Ralph	k1gMnSc1	Ralph
Bakshi	Baksh	k1gFnSc2	Baksh
natočil	natočit	k5eAaBmAgMnS	natočit
první	první	k4xOgInSc4	první
animovaný	animovaný	k2eAgInSc4d1	animovaný
díl	díl	k1gInSc4	díl
filmu	film	k1gInSc2	film
dle	dle	k7c2	dle
Tolkienova	Tolkienův	k2eAgInSc2d1	Tolkienův
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
nemělo	mít	k5eNaImAgNnS	mít
valný	valný	k2eAgInSc4d1	valný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Zaentz	Zaentz	k1gInSc1	Zaentz
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
už	už	k6eAd1	už
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Ralphem	Ralph	k1gInSc7	Ralph
Baskhilem	Baskhil	k1gMnSc7	Baskhil
na	na	k7c6	na
adaptaci	adaptace	k1gFnSc6	adaptace
"	"	kIx"	"
<g/>
Pána	pán	k1gMnSc2	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
"	"	kIx"	"
nespolupracoval	spolupracovat	k5eNaImAgInS	spolupracovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zcela	zcela	k6eAd1	zcela
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
nezanevřel	zanevřít	k5eNaPmAgMnS	zanevřít
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
najít	najít	k5eAaPmF	najít
nějaké	nějaký	k3yIgNnSc4	nějaký
veliké	veliký	k2eAgNnSc4d1	veliké
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Hobiti	hobit	k1gMnPc1	hobit
nebo	nebo	k8xC	nebo
půlčíci	půlčík	k1gMnPc1	půlčík
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
ras	rasa	k1gFnPc2	rasa
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
počítačových	počítačový	k2eAgFnPc6d1	počítačová
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Heroes	Heroes	k1gMnSc1	Heroes
of	of	k?	of
Might	Might	k1gMnSc1	Might
and	and	k?	and
Magic	Magic	k1gMnSc1	Magic
<g/>
)	)	kIx)	)
a	a	k8xC	a
hrách	hrách	k1gInSc1	hrách
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
–	–	k?	–
například	například	k6eAd1	například
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
nebo	nebo	k8xC	nebo
českém	český	k2eAgNnSc6d1	české
Dračím	dračí	k2eAgNnSc6d1	dračí
doupěti	doupě	k1gNnSc6	doupě
<g/>
.	.	kIx.	.
</s>
<s>
Hobit	hobit	k1gMnSc1	hobit
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
Kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
Středozem	Středozem	k1gFnSc1	Středozem
<g/>
)	)	kIx)	)
Dračí	dračí	k2eAgFnSc1d1	dračí
doupě	doupě	k1gNnSc4	doupě
Czech	Czech	k1gInSc1	Czech
Hobbiton	Hobbiton	k1gInSc4	Hobbiton
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Hobit	hobit	k1gMnSc1	hobit
aneb	aneb	k?	aneb
ceta	ceta	k1gFnSc1	ceta
tam	tam	k6eAd1	tam
a	a	k8xC	a
zase	zase	k9	zase
zpátky	zpátky	k6eAd1	zpátky
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
506	[number]	k4	506
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Dlouho	dlouho	k6eAd1	dlouho
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
dýchánek	dýchánek	k1gInSc4	dýchánek
<g/>
.	.	kIx.	.
</s>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
-	-	kIx~	-
Společenstvo	společenstvo	k1gNnSc1	společenstvo
Prstenu	prsten	k1gInSc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
362	[number]	k4	362
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Prolog	prolog	k1gInSc1	prolog
<g/>
.	.	kIx.	.
</s>
<s>
TOLKIEN	TOLKIEN	kA	TOLKIEN
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Ronald	Ronald	k1gMnSc1	Ronald
Reuel	Reuel	k1gMnSc1	Reuel
<g/>
.	.	kIx.	.
</s>
<s>
Pán	pán	k1gMnSc1	pán
Prstenů	prsten	k1gInPc2	prsten
-	-	kIx~	-
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
373	[number]	k4	373
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Dodatky	dodatek	k1gInPc4	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
DAY	DAY	kA	DAY
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
hobitů	hobit	k1gMnPc2	hobit
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
globator	globator	k1gInSc1	globator
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
503	[number]	k4	503
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
I.	I.	kA	I.
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
bylo	být	k5eAaImAgNnS	být
slovo	slovo	k1gNnSc1	slovo
hobit	hobit	k1gMnSc1	hobit
/	/	kIx~	/
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
Hokuspokusnický	Hokuspokusnický	k2eAgInSc1d1	Hokuspokusnický
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
s.	s.	k?	s.
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hobit	hobit	k1gMnSc1	hobit
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
http://www.jcsoft.cz/fantasy/printnews.asp?id=185	[url]	k4	http://www.jcsoft.cz/fantasy/printnews.asp?id=185
http://tolkien.cz/?page_id=518	[url]	k4	http://tolkien.cz/?page_id=518
http://www.cartoonbrew.com/classic/gene-deitchs-the-hobbit.html	[url]	k1gFnPc2	http://www.cartoonbrew.com/classic/gene-deitchs-the-hobbit.html
http://www.imdb.com/title/tt0077687/	[url]	k4	http://www.imdb.com/title/tt0077687/
</s>
