<p>
<s>
Linka	linka	k1gFnSc1	linka
A	a	k9	a
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
vede	vést	k5eAaImIp3nS	vést
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
severozápad-východ	severozápadýchod	k1gInSc1	severozápad-východ
<g/>
,	,	kIx,	,
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
17	[number]	k4	17
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
17,13	[number]	k4	17,13
km	km	kA	km
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
ji	on	k3xPp3gFnSc4	on
projede	projet	k5eAaPmIp3nS	projet
za	za	k7c4	za
cca	cca	kA	cca
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
databázích	databáze	k1gFnPc6	databáze
jízdních	jízdní	k2eAgInPc2d1	jízdní
řádů	řád	k1gInPc2	řád
PID	PID	kA	PID
je	být	k5eAaImIp3nS	být
linka	linka	k1gFnSc1	linka
kódována	kódovat	k5eAaBmNgFnS	kódovat
číslem	číslo	k1gNnSc7	číslo
991	[number]	k4	991
<g/>
.	.	kIx.	.
<g/>
Hlasatelkou	hlasatelka	k1gFnSc7	hlasatelka
linky	linka	k1gFnSc2	linka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
zprovoznění	zprovoznění	k1gNnSc2	zprovoznění
Světlana	Světlana	k1gFnSc1	Světlana
Lavičková	Lavičková	k1gFnSc1	Lavičková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
této	tento	k3xDgFnSc2	tento
trati	trať	k1gFnSc2	trať
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
otevřena	otevřít	k5eAaPmNgFnS	otevřít
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
(	(	kIx(	(
<g/>
I.A	I.A	k1gMnPc2	I.A
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Leninova	Leninův	k2eAgNnSc2d1	Leninovo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
)	)	kIx)	)
a	a	k8xC	a
Náměstí	náměstí	k1gNnSc1	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
4,7	[number]	k4	4,7
km	km	kA	km
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
7	[number]	k4	7
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
první	první	k4xOgInSc1	první
podchod	podchod	k1gInSc1	podchod
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
pod	pod	k7c7	pod
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
14	[number]	k4	14
m	m	kA	m
pod	pod	k7c7	pod
dnem	dno	k1gNnSc7	dno
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výstavba	výstavba	k1gFnSc1	výstavba
úseku	úsek	k1gInSc2	úsek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
2,6	[number]	k4	2,6
km	km	kA	km
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
stanice	stanice	k1gFnSc1	stanice
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
Flora	Floro	k1gNnSc2	Floro
a	a	k8xC	a
Želivského	želivský	k2eAgNnSc2d1	Želivské
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1987	[number]	k4	1987
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
úsek	úsek	k1gInSc1	úsek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trasu	trasa	k1gFnSc4	trasa
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
o	o	k7c4	o
1,3	[number]	k4	1,3
km	km	kA	km
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Strašnická	strašnický	k2eAgFnSc1d1	Strašnická
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
pak	pak	k6eAd1	pak
úsek	úsek	k1gInSc1	úsek
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
přidal	přidat	k5eAaPmAgMnS	přidat
dalších	další	k2eAgFnPc2d1	další
1,4	[number]	k4	1,4
km	km	kA	km
a	a	k8xC	a
stanici	stanice	k1gFnSc6	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
o	o	k7c6	o
stanici	stanice	k1gFnSc6	stanice
Depo	depo	k1gNnSc1	depo
Hostivař	Hostivař	k1gFnSc4	Hostivař
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
existující	existující	k2eAgFnSc2d1	existující
spojky	spojka	k1gFnSc2	spojka
do	do	k7c2	do
depa	depo	k1gNnSc2	depo
vedené	vedený	k2eAgFnSc2d1	vedená
částečně	částečně	k6eAd1	částečně
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
každou	každý	k3xTgFnSc4	každý
sobotu	sobota	k1gFnSc4	sobota
do	do	k7c2	do
souprav	souprava	k1gFnPc2	souprava
řazen	řadit	k5eAaImNgInS	řadit
"	"	kIx"	"
<g/>
komunikační	komunikační	k2eAgInSc1d1	komunikační
vagon	vagon	k1gInSc1	vagon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
cestující	cestující	k1gMnPc1	cestující
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
možnost	možnost	k1gFnSc4	možnost
seznámit	seznámit	k5eAaPmF	seznámit
se	se	k3xPyFc4	se
a	a	k8xC	a
případně	případně	k6eAd1	případně
navázat	navázat	k5eAaPmF	navázat
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Seznamovací	seznamovací	k2eAgInSc1d1	seznamovací
vagón	vagón	k1gInSc1	vagón
byl	být	k5eAaImAgInS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
souprav	souprava	k1gFnPc2	souprava
metra	metro	k1gNnSc2	metro
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
konce	konec	k1gInSc2	konec
prázdnin	prázdniny	k1gFnPc2	prázdniny
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prázdnin	prázdniny	k1gFnPc2	prázdniny
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
zařazován	zařazovat	k5eAaImNgInS	zařazovat
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
označovaném	označovaný	k2eAgMnSc6d1	označovaný
V.A	V.A	k1gMnSc6	V.A
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
trať	trať	k1gFnSc4	trať
z	z	k7c2	z
Dejvické	dejvický	k2eAgFnSc2d1	Dejvická
do	do	k7c2	do
Motola	Motola	k1gFnSc1	Motola
o	o	k7c4	o
stanice	stanice	k1gFnPc4	stanice
<g/>
:	:	kIx,	:
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc1	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
,	,	kIx,	,
Petřiny	Petřiny	k1gFnPc1	Petřiny
a	a	k8xC	a
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
úsek	úsek	k1gInSc1	úsek
stál	stát	k5eAaImAgInS	stát
cca	cca	kA	cca
20	[number]	k4	20
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výluky	výluka	k1gFnSc2	výluka
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
zaplavena	zaplaven	k2eAgFnSc1d1	zaplavena
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
provoz	provoz	k1gInSc1	provoz
a	a	k8xC	a
zaplavené	zaplavený	k2eAgFnPc1d1	zaplavená
stanice	stanice	k1gFnPc1	stanice
a	a	k8xC	a
úseky	úsek	k1gInPc1	úsek
prošly	projít	k5eAaPmAgInP	projít
před	před	k7c7	před
obnovením	obnovení	k1gNnSc7	obnovení
provozu	provoz	k1gInSc2	provoz
rekonstrukcí	rekonstrukce	k1gFnPc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
metra	metro	k1gNnSc2	metro
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
i	i	k9	i
během	běh	k1gInSc7	běh
povodní	povodeň	k1gFnPc2	povodeň
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
všech	všecek	k3xTgFnPc2	všecek
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
přerušen	přerušit	k5eAaPmNgInS	přerušit
protivládní	protivládní	k2eAgFnSc7d1	protivládní
stávkou	stávka	k1gFnSc7	stávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Můstek	můstek	k1gInSc1	můstek
<g/>
–	–	k?	–
<g/>
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2012	[number]	k4	2012
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
trase	trasa	k1gFnSc6	trasa
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
plánované	plánovaný	k2eAgFnPc1d1	plánovaná
výluky	výluka	k1gFnPc1	výluka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
červencové	červencový	k2eAgFnSc6d1	červencová
výluce	výluka	k1gFnSc6	výluka
byla	být	k5eAaImAgFnS	být
opravena	opraven	k2eAgFnSc1d1	opravena
výhybka	výhybka	k1gFnSc1	výhybka
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Zářijová	zářijový	k2eAgFnSc1d1	zářijová
výluka	výluka	k1gFnSc1	výluka
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
kvůli	kvůli	k7c3	kvůli
výměně	výměna	k1gFnSc3	výměna
jedné	jeden	k4xCgFnSc2	jeden
výhybky	výhybka	k1gFnSc2	výhybka
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jedné	jeden	k4xCgFnSc2	jeden
odstávky	odstávka	k1gFnSc2	odstávka
výměna	výměna	k1gFnSc1	výměna
tří	tři	k4xCgFnPc2	tři
výhybek	výhybka	k1gFnPc2	výhybka
<g/>
,	,	kIx,	,
dvou	dva	k4xCgInPc2	dva
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
výměny	výměna	k1gFnSc2	výměna
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
pražců	pražec	k1gInPc2	pražec
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Parametry	parametr	k1gInPc1	parametr
==	==	k?	==
</s>
</p>
<p>
<s>
Přepravní	přepravní	k2eAgFnSc1d1	přepravní
kapacita	kapacita	k1gFnSc1	kapacita
trasy	trasa	k1gFnSc2	trasa
A	a	k9	a
přepočtená	přepočtený	k2eAgFnSc1d1	přepočtená
na	na	k7c4	na
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
činí	činit	k5eAaImIp3nS	činit
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
cca	cca	kA	cca
20	[number]	k4	20
320	[number]	k4	320
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
v	v	k7c6	v
době	doba	k1gFnSc6	doba
snížených	snížený	k2eAgInPc2d1	snížený
přepravních	přepravní	k2eAgInPc2d1	přepravní
nároků	nárok	k1gInPc2	nárok
<g/>
,	,	kIx,	,
cca	cca	kA	cca
10	[number]	k4	10
500	[number]	k4	500
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
stanic	stanice	k1gFnPc2	stanice
linky	linka	k1gFnSc2	linka
A	A	kA	A
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Konstrukce	konstrukce	k1gFnSc2	konstrukce
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
řešení	řešení	k1gNnSc2	řešení
Praha	Praha	k1gFnSc1	Praha
získala	získat	k5eAaPmAgFnS	získat
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc7	první
velmi	velmi	k6eAd1	velmi
hluboko	hluboko	k6eAd1	hluboko
založené	založený	k2eAgFnPc4d1	založená
stanice	stanice	k1gFnPc4	stanice
(	(	kIx(	(
<g/>
desítky	desítka	k1gFnPc4	desítka
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
trojlodní	trojlodní	k2eAgFnPc4d1	trojlodní
stanice	stanice	k1gFnPc4	stanice
ražené	ražený	k2eAgFnPc4d1	ražená
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
první	první	k4xOgFnSc2	první
stanice	stanice	k1gFnSc2	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
)	)	kIx)	)
a	a	k8xC	a
pilířové	pilířový	k2eAgNnSc1d1	pilířové
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
stanice	stanice	k1gFnSc2	stanice
Můstek	můstek	k1gInSc1	můstek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
sloupová	sloupový	k2eAgFnSc1d1	sloupová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pilíře	pilíř	k1gInPc1	pilíř
stanic	stanice	k1gFnPc2	stanice
jsou	být	k5eAaImIp3nP	být
masivní	masivní	k2eAgNnPc1d1	masivní
a	a	k8xC	a
nástupiště	nástupiště	k1gNnPc1	nástupiště
tudíž	tudíž	k8xC	tudíž
široká	široký	k2eAgFnSc1d1	široká
(	(	kIx(	(
<g/>
teprve	teprve	k6eAd1	teprve
až	až	k9	až
od	od	k7c2	od
úseku	úsek	k1gInSc2	úsek
metra	metro	k1gNnSc2	metro
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
A	A	kA	A
a	a	k8xC	a
I.	I.	kA	I.
B	B	kA	B
jsou	být	k5eAaImIp3nP	být
pilíře	pilíř	k1gInPc1	pilíř
užší	úzký	k2eAgFnSc2d2	užší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnPc1	stanice
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
jediný	jediný	k2eAgInSc4d1	jediný
výstup	výstup	k1gInSc4	výstup
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
druhý	druhý	k4xOgInSc1	druhý
dobudovat	dobudovat	k5eAaPmF	dobudovat
(	(	kIx(	(
<g/>
původní	původní	k2eAgInPc4d1	původní
plány	plán	k1gInPc4	plán
počítaly	počítat	k5eAaImAgFnP	počítat
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
některých	některý	k3yIgFnPc2	některý
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
těch	ten	k3xDgMnPc2	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
odstoupilo	odstoupit	k5eAaPmAgNnS	odstoupit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Interiéry	interiér	k1gInPc4	interiér
===	===	k?	===
</s>
</p>
<p>
<s>
Interiéry	interiér	k1gInPc1	interiér
stanic	stanice	k1gFnPc2	stanice
byly	být	k5eAaImAgInP	být
provedeny	provést	k5eAaPmNgInP	provést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
působily	působit	k5eAaImAgFnP	působit
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
exkluzivně	exkluzivně	k6eAd1	exkluzivně
a	a	k8xC	a
až	až	k6eAd1	až
tajemně	tajemně	k6eAd1	tajemně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
vyznění	vyznění	k1gNnSc6	vyznění
mělo	mít	k5eAaImAgNnS	mít
velice	velice	k6eAd1	velice
propracované	propracovaný	k2eAgNnSc1d1	propracované
zacházení	zacházení	k1gNnSc1	zacházení
se	s	k7c7	s
světelnými	světelný	k2eAgInPc7d1	světelný
zdroji	zdroj	k1gInPc7	zdroj
a	a	k8xC	a
vhodný	vhodný	k2eAgInSc4d1	vhodný
výběr	výběr	k1gInSc4	výběr
použitých	použitý	k2eAgInPc2d1	použitý
materiálů	materiál	k1gInPc2	materiál
na	na	k7c4	na
obklady	obklad	k1gInPc4	obklad
stěn	stěna	k1gFnPc2	stěna
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celkovým	celkový	k2eAgNnSc7d1	celkové
barevným	barevný	k2eAgNnSc7d1	barevné
laděním	ladění	k1gNnSc7	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Cestování	cestování	k1gNnSc2	cestování
linkou	linka	k1gFnSc7	linka
I.A	I.A	k1gFnSc2	I.A
mělo	mít	k5eAaImAgNnS	mít
mj.	mj.	kA	mj.
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestující	cestující	k1gMnSc1	cestující
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pod	pod	k7c7	pod
historickým	historický	k2eAgNnSc7d1	historické
ceněným	ceněný	k2eAgNnSc7d1	ceněné
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Právě	právě	k9	právě
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
interiéry	interiér	k1gInPc1	interiér
stanic	stanice	k1gFnPc2	stanice
působily	působit	k5eAaImAgInP	působit
unikátně	unikátně	k6eAd1	unikátně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
Malostranská	malostranský	k2eAgFnSc1d1	Malostranská
–	–	k?	–
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stanice	stanice	k1gFnSc1	stanice
Můstek	můstek	k1gInSc1	můstek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zakomponován	zakomponován	k2eAgInSc1d1	zakomponován
starý	starý	k2eAgInSc1d1	starý
můstek	můstek	k1gInSc1	můstek
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
vestibulu	vestibul	k1gInSc6	vestibul
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
unikátní	unikátní	k2eAgFnSc4d1	unikátní
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
odlišitelná	odlišitelný	k2eAgFnSc1d1	odlišitelná
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Obklady	obklad	k1gInPc4	obklad
====	====	k?	====
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
linka	linka	k1gFnSc1	linka
je	být	k5eAaImIp3nS	být
vyvedena	vyvést	k5eAaPmNgFnS	vyvést
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
barvě	barva	k1gFnSc6	barva
–	–	k?	–
zlatavé	zlatavý	k2eAgMnPc4d1	zlatavý
champagne	champagnout	k5eAaImIp3nS	champagnout
<g/>
.	.	kIx.	.
</s>
<s>
Obklady	obklad	k1gInPc1	obklad
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
kulatých	kulatý	k2eAgInPc2d1	kulatý
výlisků	výlisek	k1gInPc2	výlisek
z	z	k7c2	z
eloxovaného	eloxovaný	k2eAgInSc2d1	eloxovaný
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
konvexního	konvexní	k2eAgInSc2d1	konvexní
a	a	k8xC	a
konkávního	konkávní	k2eAgInSc2d1	konkávní
tvaru	tvar	k1gInSc2	tvar
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
linky	linka	k1gFnSc2	linka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
ploché	plochý	k2eAgFnPc1d1	plochá
(	(	kIx(	(
<g/>
Želivského	želivský	k2eAgNnSc2d1	Želivské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
vzájemných	vzájemný	k2eAgFnPc6d1	vzájemná
kombinacích	kombinace	k1gFnPc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
obklady	obklad	k1gInPc1	obklad
stanic	stanice	k1gFnPc2	stanice
jsou	být	k5eAaImIp3nP	být
provedeny	provést	k5eAaPmNgInP	provést
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zlatavé	zlatavý	k2eAgFnSc6d1	zlatavá
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
stanici	stanice	k1gFnSc6	stanice
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
prostředku	prostředek	k1gInSc6	prostředek
stěny	stěna	k1gFnSc2	stěna
za	za	k7c7	za
nástupištěm	nástupiště	k1gNnSc7	nástupiště
barevný	barevný	k2eAgInSc1d1	barevný
pruh	pruh	k1gInSc1	pruh
tvořený	tvořený	k2eAgInSc1d1	tvořený
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
odstínů	odstín	k1gInPc2	odstín
jedné	jeden	k4xCgFnSc2	jeden
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
stanici	stanice	k1gFnSc4	stanice
unikátní	unikátní	k2eAgMnSc1d1	unikátní
<g/>
.	.	kIx.	.
</s>
<s>
Hliníkové	hliníkový	k2eAgNnSc4d1	hliníkové
obložení	obložení	k1gNnSc4	obložení
ražených	ražený	k2eAgFnPc2d1	ražená
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
hlavní	hlavní	k2eAgMnSc1d1	hlavní
architekt	architekt	k1gMnSc1	architekt
pražského	pražský	k2eAgMnSc2d1	pražský
metra	metr	k1gMnSc2	metr
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Otruba	otruba	k1gFnSc1	otruba
<g/>
.	.	kIx.	.
</s>
<s>
Střídání	střídání	k1gNnSc1	střídání
dutých	dutý	k2eAgFnPc2d1	dutá
a	a	k8xC	a
vypouklých	vypouklý	k2eAgFnPc2d1	vypouklá
desek	deska	k1gFnPc2	deska
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
tříštit	tříštit	k5eAaImF	tříštit
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlny	vlna	k1gFnPc4	vlna
od	od	k7c2	od
podvozků	podvozek	k1gInPc2	podvozek
projíždějících	projíždějící	k2eAgFnPc2d1	projíždějící
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgNnSc4d1	barevné
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
grafik	grafik	k1gMnSc1	grafik
Jiří	Jiří	k1gMnSc1	Jiří
Rathouský	Rathouský	k1gMnSc1	Rathouský
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
užit	užít	k5eAaPmNgInS	užít
na	na	k7c4	na
strop	strop	k1gInSc4	strop
středních	střední	k2eAgFnPc2d1	střední
a	a	k8xC	a
stěny	stěna	k1gFnPc4	stěna
za	za	k7c7	za
kolejištěm	kolejiště	k1gNnSc7	kolejiště
+	+	kIx~	+
strop	strop	k1gInSc1	strop
lodí	loď	k1gFnPc2	loď
bočních	boční	k2eAgFnPc2d1	boční
byly	být	k5eAaImAgFnP	být
užity	užít	k5eAaPmNgFnP	užít
také	také	k9	také
i	i	k8xC	i
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
druhy	druh	k1gInPc4	druh
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
mramor	mramor	k1gInSc4	mramor
<g/>
,	,	kIx,	,
či	či	k8xC	či
žula	žula	k1gFnSc1	žula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejné	stejný	k2eAgInPc1d1	stejný
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
obklady	obklad	k1gInPc1	obklad
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
pro	pro	k7c4	pro
stanice	stanice	k1gFnPc4	stanice
linky	linka	k1gFnSc2	linka
A	A	kA	A
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
k	k	k7c3	k
obložení	obložení	k1gNnSc3	obložení
společenského	společenský	k2eAgInSc2d1	společenský
klubu	klub	k1gInSc2	klub
ve	v	k7c6	v
Šternberku	Šternberk	k1gInSc6	Šternberk
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
19	[number]	k4	19
<g/>
.	.	kIx.	.
reprezentačního	reprezentační	k2eAgInSc2d1	reprezentační
městského	městský	k2eAgInSc2d1	městský
plesu	ples	k1gInSc2	ples
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
začalo	začít	k5eAaPmAgNnS	začít
prodávat	prodávat	k5eAaImF	prodávat
certifikáty	certifikát	k1gInPc4	certifikát
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgInPc4	který
jejich	jejich	k3xOp3gMnPc1	jejich
držitelé	držitel	k1gMnPc1	držitel
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
Městského	městský	k2eAgInSc2d1	městský
klubu	klub	k1gInSc2	klub
dostanou	dostat	k5eAaPmIp3nP	dostat
demontované	demontovaný	k2eAgInPc1d1	demontovaný
obklady	obklad	k1gInPc1	obklad
<g/>
,	,	kIx,	,
do	do	k7c2	do
prodeje	prodej	k1gInSc2	prodej
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
400	[number]	k4	400
kusů	kus	k1gInPc2	kus
po	po	k7c4	po
500	[number]	k4	500
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
prodeje	prodej	k1gInSc2	prodej
měl	mít	k5eAaImAgInS	mít
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
pokrytí	pokrytí	k1gNnSc3	pokrytí
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
šternberské	šternberský	k2eAgFnSc2d1	šternberská
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
na	na	k7c4	na
charitativní	charitativní	k2eAgInPc4d1	charitativní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Kachle	kachel	k1gInPc4	kachel
nakupovali	nakupovat	k5eAaBmAgMnP	nakupovat
především	především	k9	především
milovníci	milovník	k1gMnPc1	milovník
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
informační	informační	k2eAgInSc4d1	informační
systém	systém	k1gInSc4	systém
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
další	další	k2eAgInPc4d1	další
nápisy	nápis	k1gInPc4	nápis
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
trasách	trasa	k1gFnPc6	trasa
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
druh	druh	k1gInSc1	druh
písma	písmo	k1gNnSc2	písmo
zvaný	zvaný	k2eAgInSc1d1	zvaný
Metron	Metron	k1gInSc1	Metron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Eskalátorové	Eskalátorový	k2eAgInPc1d1	Eskalátorový
tunely	tunel	k1gInPc1	tunel
====	====	k?	====
</s>
</p>
<p>
<s>
Eskalátorové	Eskalátorový	k2eAgInPc1d1	Eskalátorový
tunely	tunel	k1gInPc1	tunel
měly	mít	k5eAaImAgInP	mít
rovněž	rovněž	k9	rovněž
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
pruhy	pruh	k1gInPc4	pruh
z	z	k7c2	z
tenkých	tenký	k2eAgFnPc2d1	tenká
hliníkových	hliníkový	k2eAgFnPc2d1	hliníková
lamel	lamela	k1gFnPc2	lamela
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
Champagne	Champagn	k1gMnSc5	Champagn
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
navíc	navíc	k6eAd1	navíc
provedeny	provést	k5eAaPmNgFnP	provést
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
nebyl	být	k5eNaImAgInS	být
prakticky	prakticky	k6eAd1	prakticky
slyšet	slyšet	k5eAaImF	slyšet
žádný	žádný	k3yNgInSc4	žádný
technický	technický	k2eAgInSc4d1	technický
hluk	hluk	k1gInSc4	hluk
z	z	k7c2	z
jezdících	jezdící	k2eAgInPc2d1	jezdící
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
stropní	stropní	k2eAgFnSc6d1	stropní
části	část	k1gFnSc6	část
tunelu	tunel	k1gInSc2	tunel
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
reklamní	reklamní	k2eAgInPc1d1	reklamní
panely	panel	k1gInPc1	panel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
však	však	k9	však
byly	být	k5eAaImAgFnP	být
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
používány	používat	k5eAaImNgFnP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
těchto	tento	k3xDgInPc2	tento
tunelů	tunel	k1gInPc2	tunel
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
odstraněny	odstraněn	k2eAgInPc4d1	odstraněn
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
zatékáním	zatékání	k1gNnSc7	zatékání
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
zastaralý	zastaralý	k2eAgInSc1d1	zastaralý
typ	typ	k1gInSc1	typ
sovětských	sovětský	k2eAgInPc2d1	sovětský
eskalátorů	eskalátor	k1gInPc2	eskalátor
novějšími	nový	k2eAgInPc7d2	novější
<g/>
,	,	kIx,	,
vyrobenými	vyrobený	k2eAgInPc7d1	vyrobený
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rekonstrukce	rekonstrukce	k1gFnPc4	rekonstrukce
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
architektonické	architektonický	k2eAgNnSc4d1	architektonické
vyznění	vyznění	k1gNnSc4	vyznění
stanic	stanice	k1gFnPc2	stanice
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ztratily	ztratit	k5eAaPmAgFnP	ztratit
stanice	stanice	k1gFnPc1	stanice
kvůli	kvůli	k7c3	kvůli
rekonstrukcím	rekonstrukce	k1gFnPc3	rekonstrukce
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc1	část
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
původního	původní	k2eAgInSc2d1	původní
architektonického	architektonický	k2eAgInSc2d1	architektonický
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
ho	on	k3xPp3gMnSc4	on
co	co	k9	co
nejcitlivěji	citlivě	k6eAd3	citlivě
zachovat	zachovat	k5eAaPmF	zachovat
(	(	kIx(	(
<g/>
původní	původní	k2eAgInPc1d1	původní
obklady	obklad	k1gInPc1	obklad
byly	být	k5eAaImAgInP	být
vyčištěny	vyčistit	k5eAaPmNgInP	vyčistit
a	a	k8xC	a
ponechány	ponechat	k5eAaPmNgInP	ponechat
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
změnou	změna	k1gFnSc7	změna
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
použití	použití	k1gNnSc4	použití
nových	nový	k2eAgNnPc2d1	nové
závěsných	závěsný	k2eAgNnPc2d1	závěsné
osvětlovacích	osvětlovací	k2eAgNnPc2d1	osvětlovací
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
změnilo	změnit	k5eAaPmAgNnS	změnit
původní	původní	k2eAgNnSc1d1	původní
vyznění	vyznění	k1gNnSc1	vyznění
stanic	stanice	k1gFnPc2	stanice
úseku	úsek	k1gInSc2	úsek
I.	I.	kA	I.
A	A	kA	A
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
zbývající	zbývající	k2eAgFnSc1d1	zbývající
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosud	dosud	k6eAd1	dosud
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Flora	Flora	k1gFnSc1	Flora
(	(	kIx(	(
<g/>
otevřena	otevřen	k2eAgFnSc1d1	otevřena
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odstraněny	odstraněn	k2eAgFnPc1d1	odstraněna
byly	být	k5eAaImAgFnP	být
také	také	k9	také
původní	původní	k2eAgInPc4d1	původní
obklady	obklad	k1gInPc4	obklad
eskalátorových	eskalátorův	k2eAgInPc2d1	eskalátorův
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
obklady	obklad	k1gInPc1	obklad
v	v	k7c6	v
eskalátorovém	eskalátorový	k2eAgInSc6d1	eskalátorový
tunelu	tunel	k1gInSc6	tunel
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Želivského	želivský	k2eAgInSc2d1	želivský
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
silně	silně	k6eAd1	silně
dezolátním	dezolátní	k2eAgInSc6d1	dezolátní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Za	za	k7c2	za
svých	svůj	k3xOyFgFnPc2	svůj
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
let	léto	k1gNnPc2	léto
provozu	provoz	k1gInSc2	provoz
stanice	stanice	k1gFnSc2	stanice
na	na	k7c6	na
úsecích	úsek	k1gInPc6	úsek
I.A	I.A	k1gFnSc2	I.A
chátraly	chátrat	k5eAaImAgFnP	chátrat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
rekonstruovány	rekonstruován	k2eAgFnPc1d1	rekonstruována
<g/>
.	.	kIx.	.
</s>
<s>
Zub	zub	k1gInSc1	zub
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
i	i	k9	i
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
i	i	k8xC	i
těchto	tento	k3xDgFnPc2	tento
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Prodloužení	prodloužení	k1gNnSc1	prodloužení
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
konci	konec	k1gInSc6	konec
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vedly	vést	k5eAaImAgInP	vést
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
prodloužit	prodloužit	k5eAaPmF	prodloužit
metro	metro	k1gNnSc1	metro
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
až	až	k9	až
na	na	k7c4	na
Letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
spojení	spojení	k1gNnSc4	spojení
použít	použít	k5eAaPmF	použít
příměstskou	příměstský	k2eAgFnSc4d1	příměstská
železnici	železnice	k1gFnSc4	železnice
do	do	k7c2	do
Kladna	Kladno	k1gNnSc2	Kladno
a	a	k8xC	a
metro	metro	k1gNnSc1	metro
protáhnout	protáhnout	k5eAaPmF	protáhnout
jen	jen	k9	jen
na	na	k7c4	na
Petřiny	Petřiny	k1gFnPc4	Petřiny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2005	[number]	k4	2005
vydal	vydat	k5eAaPmAgInS	vydat
pražský	pražský	k2eAgInSc1d1	pražský
magistrát	magistrát	k1gInSc1	magistrát
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
metro	metro	k1gNnSc1	metro
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
na	na	k7c4	na
Červený	červený	k2eAgInSc4d1	červený
vrch	vrch	k1gInSc4	vrch
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
Bořislavku	Bořislavek	k1gInSc6	Bořislavek
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nádraží	nádraží	k1gNnSc1	nádraží
Veleslavín	Veleslavín	k1gMnSc1	Veleslavín
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obloukem	oblouk	k1gInSc7	oblouk
přes	přes	k7c4	přes
Petřiny	Petřiny	k1gFnPc4	Petřiny
<g/>
,	,	kIx,	,
nemocnici	nemocnice	k1gFnSc4	nemocnice
Motol	Motol	k1gInSc4	Motol
<g/>
,	,	kIx,	,
Bílou	bílý	k2eAgFnSc4d1	bílá
Horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
sídliště	sídliště	k1gNnSc4	sídliště
Dědina	dědina	k1gFnSc1	dědina
<g/>
,	,	kIx,	,
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
Míli	míle	k1gFnSc4	míle
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
bude	být	k5eAaImBp3nS	být
přestup	přestup	k1gInSc1	přestup
na	na	k7c4	na
vlak	vlak	k1gInSc4	vlak
<g/>
)	)	kIx)	)
a	a	k8xC	a
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Ruzyni	Ruzyně	k1gFnSc6	Ruzyně
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
měla	mít	k5eAaImAgFnS	mít
začít	začít	k5eAaPmF	začít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
financí	finance	k1gFnPc2	finance
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
budovat	budovat	k5eAaImF	budovat
dvě	dva	k4xCgFnPc4	dva
trasy	trasa	k1gFnPc4	trasa
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
vedly	vést	k5eAaImAgInP	vést
politické	politický	k2eAgInPc1d1	politický
spory	spor	k1gInPc1	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
dána	dát	k5eAaPmNgFnS	dát
přednost	přednost	k1gFnSc1	přednost
trase	trasa	k1gFnSc6	trasa
D.	D.	kA	D.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
první	první	k4xOgFnPc1	první
přípravné	přípravný	k2eAgFnPc1d1	přípravná
práce	práce	k1gFnPc1	práce
výstavby	výstavba	k1gFnSc2	výstavba
prodloužení	prodloužení	k1gNnSc2	prodloužení
přes	přes	k7c4	přes
Petřiny	Petřiny	k1gFnPc4	Petřiny
do	do	k7c2	do
Motola	Motola	k1gFnSc1	Motola
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
prodloužení	prodloužení	k1gNnSc2	prodloužení
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
ponecháno	ponechán	k2eAgNnSc1d1	ponecháno
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
územní	územní	k2eAgFnSc1d1	územní
rezerva	rezerva	k1gFnSc1	rezerva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
železničního	železniční	k2eAgNnSc2d1	železniční
spojení	spojení	k1gNnSc2	spojení
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
budovaném	budovaný	k2eAgInSc6d1	budovaný
úseku	úsek	k1gInSc6	úsek
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
nové	nový	k2eAgFnPc4d1	nová
stanice	stanice	k1gFnPc4	stanice
Červený	červený	k2eAgInSc1d1	červený
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc1	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
,	,	kIx,	,
Petřiny	Petřiny	k1gFnPc1	Petřiny
a	a	k8xC	a
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Vypichu	vypich	k1gInSc2	vypich
metro	metro	k1gNnSc1	metro
podjede	podjet	k5eAaPmIp3nS	podjet
bez	bez	k7c2	bez
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
v	v	k7c6	v
tunelu	tunel	k1gInSc6	tunel
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
Vypichu	vypich	k1gInSc6	vypich
konal	konat	k5eAaImAgInS	konat
"	"	kIx"	"
<g/>
den	den	k1gInSc1	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Portálu	portál	k1gInSc2	portál
hl.	hl.	k?	hl.
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
podobná	podobný	k2eAgFnSc1d1	podobná
akce	akce	k1gFnSc1	akce
po	po	k7c6	po
asi	asi	k9	asi
40	[number]	k4	40
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
Vypichu	vypich	k1gInSc2	vypich
na	na	k7c4	na
Motol	Motol	k1gInSc4	Motol
vede	vést	k5eAaImIp3nS	vést
jeden	jeden	k4xCgInSc1	jeden
velký	velký	k2eAgInSc1d1	velký
dvojkolejný	dvojkolejný	k2eAgInSc1d1	dvojkolejný
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
ražený	ražený	k2eAgInSc1d1	ražený
starší	starý	k2eAgFnSc7d2	starší
metodou	metoda	k1gFnSc7	metoda
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
Nová	nový	k2eAgFnSc1d1	nová
rakouská	rakouský	k2eAgFnSc1d1	rakouská
tunelovací	tunelovací	k2eAgFnSc1d1	tunelovací
metoda	metoda	k1gFnSc1	metoda
(	(	kIx(	(
<g/>
NRTM	NRTM	kA	NRTM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Vypichu	vypich	k1gInSc2	vypich
na	na	k7c4	na
Dejvickou	dejvický	k2eAgFnSc4d1	Dejvická
byly	být	k5eAaImAgFnP	být
pomocí	pomocí	k7c2	pomocí
razicích	razicí	k2eAgInPc2d1	razicí
štítů	štít	k1gInPc2	štít
Adéla	Adéla	k1gFnSc1	Adéla
a	a	k8xC	a
Tonda	Tonda	k1gFnSc1	Tonda
novější	nový	k2eAgFnSc1d2	novější
technologií	technologie	k1gFnSc7	technologie
TBM-EPB	TBM-EPB	k1gMnPc2	TBM-EPB
vyraženy	vyrazit	k5eAaPmNgInP	vyrazit
dva	dva	k4xCgInPc1	dva
menší	malý	k2eAgInPc1d2	menší
jednokolejné	jednokolejný	k2eAgInPc1d1	jednokolejný
tunely	tunel	k1gInPc1	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Ražby	ražba	k1gFnPc1	ražba
těmito	tento	k3xDgFnPc7	tento
stroji	stroj	k1gInPc7	stroj
byly	být	k5eAaImAgInP	být
dokončeny	dokončen	k2eAgFnPc4d1	dokončena
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
původních	původní	k2eAgInPc2d1	původní
plánů	plán	k1gInPc2	plán
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
přestupní	přestupní	k2eAgInSc1d1	přestupní
uzel	uzel	k1gInSc1	uzel
na	na	k7c4	na
příměstskou	příměstský	k2eAgFnSc4d1	příměstská
návaznou	návazný	k2eAgFnSc4d1	návazná
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
parkoviště	parkoviště	k1gNnSc4	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
míle	míle	k1gFnSc1	míle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
stavět	stavět	k5eAaImF	stavět
též	též	k9	též
nová	nový	k2eAgFnSc1d1	nová
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
odkladům	odklad	k1gInPc3	odklad
výstavby	výstavba	k1gFnSc2	výstavba
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
trasy	trasa	k1gFnSc2	trasa
A	a	k8xC	a
i	i	k9	i
železniční	železniční	k2eAgFnPc4d1	železniční
tratě	trať	k1gFnPc4	trať
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
objevila	objevit	k5eAaPmAgFnS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
autobusový	autobusový	k2eAgInSc4d1	autobusový
terminál	terminál	k1gInSc4	terminál
a	a	k8xC	a
parkoviště	parkoviště	k1gNnSc4	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
budou	být	k5eAaImBp3nP	být
vybudovány	vybudovat	k5eAaPmNgFnP	vybudovat
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
Nádraží	nádraží	k1gNnSc2	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
.	.	kIx.	.
</s>
<s>
Investorem	investor	k1gMnSc7	investor
terminálu	terminál	k1gInSc2	terminál
je	být	k5eAaImIp3nS	být
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
projektantem	projektant	k1gMnSc7	projektant
Metroprojekt	Metroprojekt	k1gInSc4	Metroprojekt
<g/>
,	,	kIx,	,
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
terminálu	terminál	k1gInSc2	terminál
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
blížit	blížit	k5eAaImF	blížit
miliardě	miliarda	k4xCgFnSc3	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Autobusový	autobusový	k2eAgInSc1d1	autobusový
terminál	terminál	k1gInSc1	terminál
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
jedno	jeden	k4xCgNnSc1	jeden
podzemní	podzemní	k2eAgNnSc1d1	podzemní
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
nadzemní	nadzemní	k2eAgNnSc1d1	nadzemní
podlaží	podlaží	k1gNnSc1	podlaží
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
třech	tři	k4xCgFnPc6	tři
podzemních	podzemní	k2eAgFnPc6d1	podzemní
podlažích	podlaží	k1gNnPc6	podlaží
parkoviště	parkoviště	k1gNnPc1	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
obojí	oboj	k1gFnPc2	oboj
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
propojeno	propojen	k2eAgNnSc1d1	propojeno
s	s	k7c7	s
vestibulem	vestibulum	k1gNnSc7	vestibulum
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
využití	využití	k1gNnSc1	využití
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dočasné	dočasný	k2eAgNnSc1d1	dočasné
<g/>
,	,	kIx,	,
po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
nového	nový	k2eAgInSc2d1	nový
terminálu	terminál	k1gInSc2	terminál
a	a	k8xC	a
parkoviště	parkoviště	k1gNnSc2	parkoviště
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
terminál	terminál	k1gInSc1	terminál
přestavěn	přestavět	k5eAaPmNgInS	přestavět
dostavěním	dostavění	k1gNnSc7	dostavění
dalších	další	k2eAgNnPc2d1	další
nadzemních	nadzemní	k2eAgNnPc2d1	nadzemní
podlaží	podlaží	k1gNnPc2	podlaží
na	na	k7c4	na
administrativní	administrativní	k2eAgFnSc4d1	administrativní
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
konceptu	koncept	k1gInSc6	koncept
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
zveřejněném	zveřejněný	k2eAgInSc6d1	zveřejněný
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zahrnut	zahrnout	k5eAaPmNgInS	zahrnout
nový	nový	k2eAgInSc1d1	nový
úsek	úsek	k1gInSc1	úsek
V.A	V.A	k1gFnSc4	V.A
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
–	–	k?	–
Motol	Motol	k1gInSc1	Motol
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
6,1	[number]	k4	6,1
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Větve	větev	k1gFnPc1	větev
Motol	Motol	k1gInSc1	Motol
–	–	k?	–
Letiště	letiště	k1gNnSc2	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
a	a	k8xC	a
Motol	Motol	k1gInSc1	Motol
–	–	k?	–
Zličín	Zličín	k1gInSc4	Zličín
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
konceptu	koncept	k1gInSc6	koncept
vedeny	veden	k2eAgFnPc1d1	vedena
jako	jako	k8xS	jako
územní	územní	k2eAgFnSc1d1	územní
rezerva	rezerva	k1gFnSc1	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prodloužením	prodloužení	k1gNnSc7	prodloužení
na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Strašnic	Strašnice	k1gFnPc2	Strašnice
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
ÚP	Úpa	k1gFnPc2	Úpa
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
Metropolitní	metropolitní	k2eAgInSc1d1	metropolitní
plán	plán	k1gInSc1	plán
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
s	s	k7c7	s
žádným	žádný	k3yNgNnSc7	žádný
prodlužováním	prodlužování	k1gNnSc7	prodlužování
linky	linka	k1gFnSc2	linka
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
zahájena	zahájen	k2eAgFnSc1d1	zahájena
stavba	stavba	k1gFnSc1	stavba
prodloužení	prodloužení	k1gNnSc2	prodloužení
linky	linka	k1gFnSc2	linka
A	a	k8xC	a
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužení	prodloužení	k1gNnSc1	prodloužení
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
zlepšit	zlepšit	k5eAaPmF	zlepšit
obslužnost	obslužnost	k1gFnSc4	obslužnost
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
motolské	motolský	k2eAgFnSc2d1	motolská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
nemocnicí	nemocnice	k1gFnSc7	nemocnice
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
úsek	úsek	k1gInSc1	úsek
byl	být	k5eAaImAgInS	být
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
části	část	k1gFnSc6	část
trasy	trasa	k1gFnSc2	trasa
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
stanice	stanice	k1gFnPc1	stanice
<g/>
:	:	kIx,	:
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
(	(	kIx(	(
<g/>
předtím	předtím	k6eAd1	předtím
pracovně	pracovně	k6eAd1	pracovně
nazývána	nazývat	k5eAaImNgFnS	nazývat
Červený	červený	k2eAgInSc1d1	červený
Vrch	vrch	k1gInSc1	vrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nádraží	nádraží	k1gNnSc1	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
,	,	kIx,	,
Petřiny	Petřiny	k1gFnPc1	Petřiny
a	a	k8xC	a
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
stanic	stanice	k1gFnPc2	stanice
navržené	navržený	k2eAgFnPc4d1	navržená
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
Dopravním	dopravní	k2eAgInSc7d1	dopravní
podnikem	podnik	k1gInSc7	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
schválila	schválit	k5eAaPmAgFnS	schválit
Rada	rada	k1gFnSc1	rada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
stanic	stanice	k1gFnPc2	stanice
prosadila	prosadit	k5eAaPmAgFnS	prosadit
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
korespondovat	korespondovat	k5eAaImF	korespondovat
s	s	k7c7	s
komerčními	komerční	k2eAgInPc7d1	komerční
zájmy	zájem	k1gInPc7	zájem
majitele	majitel	k1gMnSc2	majitel
budovaného	budovaný	k2eAgNnSc2d1	budované
polyfunkčního	polyfunkční	k2eAgNnSc2d1	polyfunkční
centra	centrum	k1gNnSc2	centrum
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
médií	médium	k1gNnPc2	médium
nejmenovaní	jmenovaný	k2eNgMnPc1d1	nejmenovaný
odborníci	odborník	k1gMnPc1	odborník
doporučovali	doporučovat	k5eAaImAgMnP	doporučovat
název	název	k1gInSc4	název
Horoměřická	Horoměřická	k1gFnSc1	Horoměřická
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
název	název	k1gInSc1	název
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
totální	totální	k2eAgInSc1d1	totální
chaos	chaos	k1gInSc1	chaos
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
orientačního	orientační	k2eAgInSc2d1	orientační
a	a	k8xC	a
také	také	k6eAd1	také
historického	historický	k2eAgInSc2d1	historický
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Praha	Praha	k1gFnSc1	Praha
5	[number]	k4	5
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
,	,	kIx,	,
radou	rada	k1gFnSc7	rada
města	město	k1gNnSc2	město
akceptován	akceptován	k2eAgMnSc1d1	akceptován
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
místopisná	místopisný	k2eAgFnSc1d1	místopisná
komise	komise	k1gFnSc1	komise
Rady	rada	k1gFnSc2	rada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
doporučila	doporučit	k5eAaPmAgFnS	doporučit
změnit	změnit	k5eAaPmF	změnit
název	název	k1gInSc4	název
Motol	Motol	k1gInSc1	Motol
na	na	k7c4	na
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
a	a	k8xC	a
název	název	k1gInSc1	název
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
na	na	k7c6	na
Nádraží	nádraží	k1gNnSc6	nádraží
Veleslavín	Veleslavín	k1gMnSc1	Veleslavín
<g/>
.	.	kIx.	.
</s>
<s>
Shodla	shodnout	k5eAaBmAgFnS	shodnout
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednoslovné	jednoslovný	k2eAgInPc1d1	jednoslovný
názvy	název	k1gInPc1	název
byly	být	k5eAaImAgInP	být
zavádějící	zavádějící	k2eAgInPc4d1	zavádějící
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
stanice	stanice	k1gFnSc1	stanice
leží	ležet	k5eAaImIp3nS	ležet
<g/>
.	.	kIx.	.
</s>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
Motola	Motola	k1gFnSc1	Motola
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
jinde	jinde	k6eAd1	jinde
než	než	k8xS	než
stanice	stanice	k1gFnSc1	stanice
a	a	k8xC	a
podobné	podobný	k2eAgNnSc1d1	podobné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Veleslavína	Veleslavín	k1gInSc2	Veleslavín
<g/>
;	;	kIx,	;
jednoslovné	jednoslovný	k2eAgInPc1d1	jednoslovný
názvy	název	k1gInPc1	název
by	by	kYmCp3nP	by
prý	prý	k9	prý
byly	být	k5eAaImAgInP	být
obdobné	obdobný	k2eAgInPc1d1	obdobný
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
se	se	k3xPyFc4	se
stanice	stanice	k1gFnSc2	stanice
Muzeum	muzeum	k1gNnSc4	muzeum
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Vinohrady	Vinohrady	k1gInPc4	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Zdůvodnění	zdůvodnění	k1gNnSc1	zdůvodnění
médiím	médium	k1gNnPc3	médium
sdělil	sdělit	k5eAaPmAgMnS	sdělit
předseda	předseda	k1gMnSc1	předseda
komise	komise	k1gFnSc2	komise
Albert	Albert	k1gMnSc1	Albert
Kubišta	Kubišta	k1gMnSc1	Kubišta
(	(	kIx(	(
<g/>
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
doporučení	doporučení	k1gNnSc2	doporučení
komise	komise	k1gFnSc2	komise
projednala	projednat	k5eAaPmAgFnS	projednat
a	a	k8xC	a
názvy	název	k1gInPc1	název
byly	být	k5eAaImAgInP	být
změněny	změnit	k5eAaPmNgInP	změnit
<g/>
.	.	kIx.	.
<g/>
Prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
trasa	trasa	k1gFnSc1	trasa
Pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Financování	financování	k1gNnSc1	financování
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
10,7	[number]	k4	10,7
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
na	na	k7c4	na
prodloužení	prodloužení	k1gNnSc4	prodloužení
z	z	k7c2	z
Dejvické	dejvický	k2eAgFnSc2d1	Dejvická
do	do	k7c2	do
Motola	Motola	k1gFnSc1	Motola
<g/>
,	,	kIx,	,
zbylých	zbylý	k2eAgFnPc2d1	zbylá
8	[number]	k4	8
miliard	miliarda	k4xCgFnPc2	miliarda
má	mít	k5eAaImIp3nS	mít
tvořit	tvořit	k5eAaImF	tvořit
příspěvek	příspěvek	k1gInSc4	příspěvek
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
fondů	fond	k1gInPc2	fond
a	a	k8xC	a
celková	celkový	k2eAgFnSc1d1	celková
cena	cena	k1gFnSc1	cena
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
18,7	[number]	k4	18,7
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
cena	cena	k1gFnSc1	cena
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
na	na	k7c4	na
22,5	[number]	k4	22,5
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
zastaví	zastavit	k5eAaPmIp3nS	zastavit
na	na	k7c6	na
cca	cca	kA	cca
20	[number]	k4	20
mld	mld	k?	mld
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
provázejí	provázet	k5eAaImIp3nP	provázet
chyby	chyba	k1gFnPc1	chyba
při	při	k7c6	při
zadávání	zadávání	k1gNnSc6	zadávání
výběrových	výběrový	k2eAgNnPc2d1	výběrové
řízení	řízení	k1gNnPc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
dostal	dostat	k5eAaPmAgInS	dostat
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
od	od	k7c2	od
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
odpovědné	odpovědný	k2eAgNnSc1d1	odpovědné
za	za	k7c2	za
evropské	evropský	k2eAgFnSc2d1	Evropská
dopravní	dopravní	k2eAgFnSc2d1	dopravní
dotace	dotace	k1gFnSc2	dotace
<g/>
,	,	kIx,	,
pokutu	pokuta	k1gFnSc4	pokuta
4,2	[number]	k4	4,2
miliardy	miliarda	k4xCgFnSc2	miliarda
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
porušil	porušit	k5eAaPmAgMnS	porušit
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
veřejných	veřejný	k2eAgFnPc6d1	veřejná
zakázkách	zakázka	k1gFnPc6	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
DPP	DPP	kA	DPP
přijít	přijít	k5eAaPmF	přijít
také	také	k9	také
o	o	k7c4	o
úvěr	úvěr	k1gInSc4	úvěr
11	[number]	k4	11
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
od	od	k7c2	od
Evropské	evropský	k2eAgFnSc2d1	Evropská
investiční	investiční	k2eAgFnSc2d1	investiční
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yQgNnSc2	který
už	už	k6eAd1	už
8,5	[number]	k4	8,5
miliardy	miliarda	k4xCgFnSc2	miliarda
vyčerpal	vyčerpat	k5eAaPmAgInS	vyčerpat
<g/>
.	.	kIx.	.
</s>
<s>
Okolnosti	okolnost	k1gFnPc1	okolnost
stavby	stavba	k1gFnPc1	stavba
začal	začít	k5eAaPmAgInS	začít
prověřovat	prověřovat	k5eAaImF	prověřovat
také	také	k9	také
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
stavby	stavba	k1gFnSc2	stavba
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k9	až
2015	[number]	k4	2015
zadány	zadán	k2eAgFnPc1d1	zadána
následující	následující	k2eAgFnPc1d1	následující
veřejné	veřejný	k2eAgFnPc1d1	veřejná
zakázky	zakázka	k1gFnPc1	zakázka
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
vyšší	vysoký	k2eAgFnSc7d2	vyšší
než	než	k8xS	než
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Prodloužení	prodloužení	k1gNnSc1	prodloužení
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
konci	konec	k1gInSc6	konec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
výhledu	výhled	k1gInSc6	výhled
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Strašnická	strašnický	k2eAgFnSc1d1	Strašnická
oddělit	oddělit	k5eAaPmF	oddělit
větev	větev	k1gFnSc4	větev
A2	A2	k1gFnSc3	A2
se	s	k7c7	s
stanicemi	stanice	k1gFnPc7	stanice
Nádraží	nádraží	k1gNnSc1	nádraží
Strašnice	Strašnice	k1gFnPc1	Strašnice
<g/>
,	,	kIx,	,
Zahradní	zahradní	k2eAgNnSc1d1	zahradní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Groši	groš	k1gInSc6	groš
a	a	k8xC	a
Nádraží	nádraží	k1gNnSc3	nádraží
Hostivař	Hostivař	k1gFnSc1	Hostivař
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Nádraží	nádraží	k1gNnPc2	nádraží
Hostivař	Hostivař	k1gFnSc1	Hostivař
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
tato	tento	k3xDgFnSc1	tento
větev	větev	k1gFnSc1	větev
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
konceptu	koncept	k1gInSc6	koncept
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
zveřejněném	zveřejněný	k2eAgInSc6d1	zveřejněný
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
prodloužením	prodloužení	k1gNnSc7	prodloužení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Strašnic	Strašnice	k1gFnPc2	Strašnice
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanice	stanice	k1gFnSc1	stanice
==	==	k?	==
</s>
</p>
<p>
<s>
Linka	linka	k1gFnSc1	linka
A	a	k9	a
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
vede	vést	k5eAaImIp3nS	vést
zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
severozápad-východ	severozápadýchod	k1gInSc1	severozápad-východ
<g/>
,	,	kIx,	,
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
17	[number]	k4	17
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
17,13	[number]	k4	17,13
km	km	kA	km
<g/>
,	,	kIx,	,
vlak	vlak	k1gInSc1	vlak
ji	on	k3xPp3gFnSc4	on
projede	projet	k5eAaPmIp3nS	projet
za	za	k7c4	za
cca	cca	kA	cca
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
databázích	databáze	k1gFnPc6	databáze
jízdních	jízdní	k2eAgInPc2d1	jízdní
řádů	řád	k1gInPc2	řád
PID	PID	kA	PID
je	být	k5eAaImIp3nS	být
linka	linka	k1gFnSc1	linka
kódována	kódovat	k5eAaBmNgFnS	kódovat
číslem	číslo	k1gNnSc7	číslo
991	[number]	k4	991
<g/>
.	.	kIx.	.
<g/>
Výstavba	výstavba	k1gFnSc1	výstavba
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
této	tento	k3xDgFnSc2	tento
trati	trať	k1gFnSc2	trať
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
otevřena	otevřít	k5eAaPmNgFnS	otevřít
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úsek	úsek	k1gInSc1	úsek
(	(	kIx(	(
<g/>
I.A	I.A	k1gMnPc2	I.A
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
mezi	mezi	k7c7	mezi
stanicemi	stanice	k1gFnPc7	stanice
Leninova	Leninův	k2eAgNnSc2d1	Leninovo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
)	)	kIx)	)
a	a	k8xC	a
Náměstí	náměstí	k1gNnSc1	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
4,7	[number]	k4	4,7
km	km	kA	km
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
7	[number]	k4	7
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
první	první	k4xOgInSc1	první
podchod	podchod	k1gInSc1	podchod
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
pod	pod	k7c7	pod
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
14	[number]	k4	14
m	m	kA	m
pod	pod	k7c7	pod
dnem	den	k1gInSc7	den
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc1	Motol
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
Nemocnice	nemocnice	k1gFnSc1	nemocnice
Motol	Motol	k1gInSc4	Motol
je	být	k5eAaImIp3nS	být
hloubená	hloubený	k2eAgFnSc1d1	hloubená
a	a	k8xC	a
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
boční	boční	k2eAgNnPc4d1	boční
nástupiště	nástupiště	k1gNnPc4	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouhých	pouhý	k2eAgInPc2d1	pouhý
5,6	[number]	k4	5,6
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
koncovou	koncový	k2eAgFnSc7d1	koncová
stanicí	stanice	k1gFnSc7	stanice
metra	metro	k1gNnSc2	metro
A.	A.	kA	A.
Stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
orientována	orientovat	k5eAaBmNgFnS	orientovat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
východ-západ	východápad	k6eAd1	východ-západ
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
vestibulem	vestibul	k1gInSc7	vestibul
přístupným	přístupný	k2eAgInSc7d1	přístupný
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
čela	čelo	k1gNnSc2	čelo
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vestibul	vestibul	k1gInSc4	vestibul
navazuje	navazovat	k5eAaImIp3nS	navazovat
podchod	podchod	k1gInSc1	podchod
pod	pod	k7c7	pod
Kukulovou	Kukulův	k2eAgFnSc7d1	Kukulova
ulicí	ulice	k1gFnSc7	ulice
s	s	k7c7	s
pokračováním	pokračování	k1gNnSc7	pokračování
až	až	k9	až
k	k	k7c3	k
nemocnici	nemocnice	k1gFnSc3	nemocnice
Motol	Motol	k1gInSc1	Motol
a	a	k8xC	a
přístupy	přístup	k1gInPc1	přístup
k	k	k7c3	k
zastávkám	zastávka	k1gFnPc3	zastávka
autobusů	autobus	k1gInPc2	autobus
MHD	MHD	kA	MHD
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
prakticky	prakticky	k6eAd1	prakticky
jako	jako	k8xC	jako
povrchová	povrchový	k2eAgFnSc1d1	povrchová
s	s	k7c7	s
částečným	částečný	k2eAgNnSc7d1	částečné
překrytím	překrytí	k1gNnSc7	překrytí
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
předpokládané	předpokládaný	k2eAgFnSc3d1	předpokládaná
etapizaci	etapizace	k1gFnSc3	etapizace
výstavby	výstavba	k1gFnSc2	výstavba
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
vybavena	vybavit	k5eAaPmNgFnS	vybavit
jako	jako	k8xC	jako
dočasně	dočasně	k6eAd1	dočasně
konečná	konečný	k2eAgFnSc1d1	konečná
s	s	k7c7	s
kolejištěm	kolejiště	k1gNnSc7	kolejiště
pro	pro	k7c4	pro
obrat	obrat	k1gInSc4	obrat
a	a	k8xC	a
v	v	k7c6	v
etapě	etapa	k1gFnSc6	etapa
i	i	k9	i
deponování	deponování	k1gNnSc1	deponování
souprav	souprava	k1gFnPc2	souprava
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tříkolejného	tříkolejný	k2eAgNnSc2d1	tříkolejné
uspořádání	uspořádání	k1gNnSc2	uspořádání
za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
laděná	laděný	k2eAgFnSc1d1	laděná
do	do	k7c2	do
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
se	se	k3xPyFc4	se
žíhaně	žíhaně	k6eAd1	žíhaně
zelenou	zelená	k1gFnSc7	zelená
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnPc1	stanice
Petřiny	Petřiny	k1gFnPc1	Petřiny
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnPc1	stanice
Petřiny	Petřin	k2eAgFnPc1d1	Petřina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
pod	pod	k7c7	pod
Brunclíkovou	Brunclíkův	k2eAgFnSc7d1	Brunclíkova
ulicí	ulice	k1gFnSc7	ulice
se	s	k7c7	s
středem	střed	k1gInSc7	střed
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
ulice	ulice	k1gFnSc2	ulice
Fajmanové	Fajmanová	k1gFnSc2	Fajmanová
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
navržena	navržen	k2eAgFnSc1d1	navržena
jako	jako	k8xS	jako
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
37,6	[number]	k4	37,6
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
terénu	terén	k1gInSc2	terén
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
podpovrchovým	podpovrchový	k2eAgInSc7d1	podpovrchový
vestibulem	vestibul	k1gInSc7	vestibul
nacházejícím	nacházející	k2eAgFnPc3d1	nacházející
se	se	k3xPyFc4	se
před	před	k7c7	před
křižovatkou	křižovatka	k1gFnSc7	křižovatka
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Na	na	k7c6	na
Petřinách	Petřiny	k1gFnPc6	Petřiny
(	(	kIx(	(
<g/>
vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
obchodně	obchodně	k6eAd1	obchodně
společenské	společenský	k2eAgNnSc4d1	společenské
centrum	centrum	k1gNnSc4	centrum
Petřin	Petřiny	k1gFnPc2	Petřiny
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vestibul	vestibul	k1gInSc4	vestibul
navazuje	navazovat	k5eAaImIp3nS	navazovat
podchod	podchod	k1gInSc1	podchod
s	s	k7c7	s
výstupy	výstup	k1gInPc7	výstup
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
ulice	ulice	k1gFnSc2	ulice
Na	na	k7c6	na
Petřinách	Petřiny	k1gFnPc6	Petřiny
včetně	včetně	k7c2	včetně
výstupů	výstup	k1gInPc2	výstup
k	k	k7c3	k
upraveným	upravený	k2eAgInPc3d1	upravený
nástupním	nástupní	k2eAgInPc3d1	nástupní
ostrůvkům	ostrůvek	k1gInPc3	ostrůvek
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
této	tento	k3xDgFnSc2	tento
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
také	také	k9	také
stanoviště	stanoviště	k1gNnSc4	stanoviště
pro	pro	k7c4	pro
dozorčí	dozorčí	k2eAgFnPc4d1	dozorčí
stanice	stanice	k1gFnPc4	stanice
s	s	k7c7	s
kruhovým	kruhový	k2eAgNnSc7d1	kruhové
oknem	okno	k1gNnSc7	okno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
"	"	kIx"	"
<g/>
ponorka	ponorka	k1gFnSc1	ponorka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
stanice	stanice	k1gFnSc2	stanice
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
kancelářích	kancelář	k1gFnPc6	kancelář
Metroprojektu	Metroprojekt	k1gInSc2	Metroprojekt
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
architekt	architekt	k1gMnSc1	architekt
Jiří	Jiří	k1gMnSc1	Jiří
Pešata	Pešata	k1gFnSc1	Pešata
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
přímý	přímý	k2eAgInSc1d1	přímý
úsek	úsek	k1gInSc1	úsek
a	a	k8xC	a
sklonové	sklonový	k2eAgInPc1d1	sklonový
poměry	poměr	k1gInPc1	poměr
traťových	traťový	k2eAgInPc2d1	traťový
tunelů	tunel	k1gInPc2	tunel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
Motol	Motol	k1gInSc1	Motol
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tuto	tento	k3xDgFnSc4	tento
stanici	stanice	k1gFnSc4	stanice
realizovat	realizovat	k5eAaBmF	realizovat
případně	případně	k6eAd1	případně
také	také	k9	také
jako	jako	k9	jako
dočasně	dočasně	k6eAd1	dočasně
koncovou	koncový	k2eAgFnSc4d1	koncová
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
etapizace	etapizace	k1gFnSc2	etapizace
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Motol	Motol	k1gInSc4	Motol
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
navrženy	navržen	k2eAgFnPc1d1	navržena
obratové	obratový	k2eAgFnPc1d1	obratová
a	a	k8xC	a
odstavné	odstavný	k2eAgFnPc1d1	odstavná
koleje	kolej	k1gFnPc1	kolej
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
čtyřkolejném	čtyřkolejný	k2eAgNnSc6d1	čtyřkolejný
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
uspořádání	uspořádání	k1gNnSc1	uspořádání
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c4	na
tříkolejné	tříkolejný	k2eAgNnSc4d1	tříkolejné
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
obratovým	obratový	k2eAgInSc7d1	obratový
tunelem	tunel	k1gInSc7	tunel
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
nacházet	nacházet	k5eAaImF	nacházet
zázemí	zázemí	k1gNnSc4	zázemí
vzduchotechniky	vzduchotechnika	k1gFnSc2	vzduchotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Středový	středový	k2eAgInSc1d1	středový
tunel	tunel	k1gInSc1	tunel
sloužil	sloužit	k5eAaImAgInS	sloužit
během	během	k7c2	během
výstavby	výstavba	k1gFnSc2	výstavba
stanice	stanice	k1gFnSc2	stanice
jako	jako	k8xC	jako
přístupový	přístupový	k2eAgMnSc1d1	přístupový
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
štola	štola	k1gFnSc1	štola
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
přístup	přístup	k1gInSc4	přístup
techniky	technika	k1gFnSc2	technika
směrem	směr	k1gInSc7	směr
z	z	k7c2	z
Břevnova	Břevnov	k1gInSc2	Břevnov
<g/>
.	.	kIx.	.
</s>
<s>
Štola	štola	k1gFnSc1	štola
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stanice	stanice	k1gFnSc2	stanice
zasypána	zasypat	k5eAaPmNgFnS	zasypat
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
laděná	laděný	k2eAgFnSc1d1	laděná
do	do	k7c2	do
oranžové	oranžový	k2eAgFnSc2d1	oranžová
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
opálově	opálově	k6eAd1	opálově
zelenou	zelená	k1gFnSc7	zelená
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnPc1	stanice
Nádraží	nádraží	k1gNnPc2	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
stávající	stávající	k2eAgFnSc7d1	stávající
tratí	trať	k1gFnSc7	trať
SŽDC	SŽDC	kA	SŽDC
obvod	obvod	k1gInSc1	obvod
žst	žst	k?	žst
<g/>
.	.	kIx.	.
</s>
<s>
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
a	a	k8xC	a
východní	východní	k2eAgFnSc7d1	východní
částí	část	k1gFnSc7	část
ulice	ulice	k1gFnSc2	ulice
K	k	k7c3	k
Červenému	červené	k1gNnSc3	červené
vrchu	vrch	k1gInSc2	vrch
<g/>
,	,	kIx,	,
se	s	k7c7	s
středem	střed	k1gInSc7	střed
přibližně	přibližně	k6eAd1	přibližně
pod	pod	k7c7	pod
ulicí	ulice	k1gFnSc7	ulice
Evropskou	evropský	k2eAgFnSc7d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mělce	mělce	k6eAd1	mělce
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
asi	asi	k9	asi
20,5	[number]	k4	20,5
m	m	kA	m
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
podpovrchovým	podpovrchový	k2eAgInSc7d1	podpovrchový
vestibulem	vestibul	k1gInSc7	vestibul
situovaným	situovaný	k2eAgInSc7d1	situovaný
mezi	mezi	k7c4	mezi
železniční	železniční	k2eAgFnPc4d1	železniční
trať	trať	k1gFnSc4	trať
a	a	k8xC	a
křižovatku	křižovatka	k1gFnSc4	křižovatka
ulic	ulice	k1gFnPc2	ulice
Evropská	evropský	k2eAgFnSc1d1	Evropská
–	–	k?	–
Kladenská	kladenský	k2eAgFnSc1d1	kladenská
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
vestibulu	vestibul	k1gInSc2	vestibul
a	a	k8xC	a
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
výstupů	výstup	k1gInPc2	výstup
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
v	v	k7c6	v
etapě	etapa	k1gFnSc6	etapa
přímé	přímý	k2eAgFnSc2d1	přímá
vazby	vazba	k1gFnSc2	vazba
na	na	k7c4	na
dočasný	dočasný	k2eAgInSc4d1	dočasný
autobusový	autobusový	k2eAgInSc4d1	autobusový
terminál	terminál	k1gInSc4	terminál
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
přestavby	přestavba	k1gFnSc2	přestavba
v	v	k7c6	v
předstihu	předstih	k1gInSc6	předstih
reagovaly	reagovat	k5eAaBmAgInP	reagovat
na	na	k7c4	na
předpokládanou	předpokládaný	k2eAgFnSc4d1	předpokládaná
modernizaci	modernizace	k1gFnSc4	modernizace
stávající	stávající	k2eAgFnSc2d1	stávající
tratě	trať	k1gFnSc2	trať
Českých	český	k2eAgFnPc2d1	Česká
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
zastávka	zastávka	k1gFnSc1	zastávka
v	v	k7c6	v
zahloubené	zahloubený	k2eAgFnSc6d1	zahloubená
poloze	poloha	k1gFnSc6	poloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
budoucí	budoucí	k2eAgFnSc1d1	budoucí
urbanizace	urbanizace	k1gFnSc1	urbanizace
prostoru	prostor	k1gInSc2	prostor
mezi	mezi	k7c7	mezi
železniční	železniční	k2eAgFnSc7d1	železniční
tratí	trať	k1gFnSc7	trať
a	a	k8xC	a
Evropskou	evropský	k2eAgFnSc7d1	Evropská
ulicí	ulice	k1gFnSc7	ulice
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
takto	takto	k6eAd1	takto
umístěné	umístěný	k2eAgFnSc2d1	umístěná
stanice	stanice	k1gFnSc2	stanice
dobře	dobře	k6eAd1	dobře
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Uvedeným	uvedený	k2eAgInPc3d1	uvedený
předpokladům	předpoklad	k1gInPc3	předpoklad
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
navržený	navržený	k2eAgInSc4d1	navržený
princip	princip	k1gInSc4	princip
uspořádání	uspořádání	k1gNnSc2	uspořádání
podchodu	podchod	k1gInSc2	podchod
a	a	k8xC	a
vestibulu	vestibul	k1gInSc2	vestibul
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
výškových	výškový	k2eAgFnPc6d1	výšková
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Autobusový	autobusový	k2eAgInSc1d1	autobusový
terminál	terminál	k1gInSc1	terminál
má	mít	k5eAaImIp3nS	mít
umožnit	umožnit	k5eAaPmF	umožnit
ukončení	ukončení	k1gNnSc4	ukončení
části	část	k1gFnSc2	část
městských	městský	k2eAgFnPc2d1	městská
<g/>
,	,	kIx,	,
příměstských	příměstský	k2eAgFnPc2d1	příměstská
a	a	k8xC	a
regionálních	regionální	k2eAgFnPc2d1	regionální
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
namísto	namísto	k7c2	namísto
Dejvické	dejvický	k2eAgFnSc2d1	Dejvická
(	(	kIx(	(
<g/>
Vítězného	vítězný	k2eAgNnSc2d1	vítězné
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
stanice	stanice	k1gFnSc2	stanice
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
kancelářích	kancelář	k1gFnPc6	kancelář
Metroprojektu	Metroprojekt	k1gInSc2	Metroprojekt
<g/>
,	,	kIx,	,
autorkou	autorka	k1gFnSc7	autorka
je	být	k5eAaImIp3nS	být
architektka	architektka	k1gFnSc1	architektka
Hana	Hana	k1gFnSc1	Hana
Vermachová	Vermachová	k1gFnSc1	Vermachová
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
laděná	laděný	k2eAgFnSc1d1	laděná
do	do	k7c2	do
třešnové	třešnový	k2eAgFnSc2d1	třešnový
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
opálově	opálově	k6eAd1	opálově
zelenou	zelená	k1gFnSc7	zelená
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Bořislavka	Bořislavka	k1gFnSc1	Bořislavka
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
zbudována	zbudovat	k5eAaPmNgFnS	zbudovat
pod	pod	k7c7	pod
ulicí	ulice	k1gFnSc7	ulice
Evropskou	evropský	k2eAgFnSc7d1	Evropská
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
křižovatkami	křižovatka	k1gFnPc7	křižovatka
ulic	ulice	k1gFnPc2	ulice
Evropská	evropský	k2eAgFnSc1d1	Evropská
<g/>
–	–	k?	–
<g/>
Arabská	arabský	k2eAgFnSc1d1	arabská
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
<g/>
–	–	k?	–
<g/>
Horoměřická	Horoměřická	k1gFnSc1	Horoměřická
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
jednolodní	jednolodní	k2eAgFnSc4d1	jednolodní
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vestibuly	vestibul	k1gInPc7	vestibul
jeden	jeden	k4xCgInSc4	jeden
povrchový	povrchový	k2eAgInSc4d1	povrchový
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
hloubený	hloubený	k2eAgInSc4d1	hloubený
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
je	být	k5eAaImIp3nS	být
že	že	k9	že
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
nenajdete	najít	k5eNaPmIp2nP	najít
eskalátor	eskalátor	k1gInSc4	eskalátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
výtahy	výtah	k1gInPc4	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
středu	střed	k1gInSc2	střed
stanice	stanice	k1gFnSc2	stanice
činí	činit	k5eAaImIp3nS	činit
26,7	[number]	k4	26,7
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vestibul	vestibul	k1gInSc1	vestibul
přístupný	přístupný	k2eAgInSc1d1	přístupný
z	z	k7c2	z
východního	východní	k2eAgNnSc2d1	východní
čela	čelo	k1gNnSc2	čelo
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
do	do	k7c2	do
křižovatky	křižovatka	k1gFnSc2	křižovatka
s	s	k7c7	s
Horoměřickou	Horoměřický	k2eAgFnSc7d1	Horoměřická
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
s	s	k7c7	s
vazbou	vazba	k1gFnSc7	vazba
na	na	k7c4	na
uvažovaný	uvažovaný	k2eAgInSc4d1	uvažovaný
malý	malý	k2eAgInSc4d1	malý
terminál	terminál	k1gInSc4	terminál
městských	městský	k2eAgFnPc2d1	městská
a	a	k8xC	a
příměstských	příměstský	k2eAgFnPc2d1	příměstská
linek	linka	k1gFnPc2	linka
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
směřujících	směřující	k2eAgFnPc2d1	směřující
sem	sem	k6eAd1	sem
zejména	zejména	k9	zejména
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Nebušic	Nebušice	k1gInPc2	Nebušice
<g/>
,	,	kIx,	,
Jenerálky	jenerálka	k1gFnSc2	jenerálka
a	a	k8xC	a
Horoměřic	Horoměřice	k1gFnPc2	Horoměřice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vestibul	vestibul	k1gInSc1	vestibul
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
chodbový	chodbový	k2eAgInSc4d1	chodbový
podchod	podchod	k1gInSc4	podchod
pod	pod	k7c7	pod
Evropskou	evropský	k2eAgFnSc7d1	Evropská
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
realizován	realizovat	k5eAaBmNgInS	realizovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výstavby	výstavba	k1gFnSc2	výstavba
obchodně	obchodně	k6eAd1	obchodně
administrativního	administrativní	k2eAgNnSc2d1	administrativní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
kvadrantu	kvadrant	k1gInSc6	kvadrant
křižovatky	křižovatka	k1gFnSc2	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Vestibul	vestibul	k1gInSc1	vestibul
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
přímého	přímý	k2eAgInSc2d1	přímý
přestupu	přestup	k1gInSc2	přestup
na	na	k7c4	na
stávající	stávající	k2eAgFnSc4d1	stávající
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
trať	trať	k1gFnSc4	trať
na	na	k7c6	na
Evropské	evropský	k2eAgFnSc6d1	Evropská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
laděná	laděný	k2eAgFnSc1d1	laděná
do	do	k7c2	do
třešnové	třešnový	k2eAgFnSc2d1	třešnový
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
opálově	opálově	k6eAd1	opálově
zelenou	zelená	k1gFnSc7	zelená
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Leninova	Leninův	k2eAgFnSc1d1	Leninova
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
hloubená	hloubený	k2eAgFnSc1d1	hloubená
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
jámě	jáma	k1gFnSc6	jáma
<g/>
,	,	kIx,	,
konstruovaná	konstruovaný	k2eAgNnPc1d1	konstruované
jako	jako	k8xC	jako
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
koncová	koncový	k2eAgFnSc1d1	koncová
(	(	kIx(	(
<g/>
za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
odstavné	odstavný	k2eAgFnPc1d1	odstavná
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
obratové	obratový	k2eAgFnPc1d1	obratová
koleje	kolej	k1gFnPc1	kolej
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Včetně	včetně	k7c2	včetně
těchto	tento	k3xDgFnPc2	tento
kolejí	kolej	k1gFnPc2	kolej
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
301	[number]	k4	301
m	m	kA	m
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
11,5	[number]	k4	11,5
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
Evropské	evropský	k2eAgFnSc2d1	Evropská
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Nástupiště	nástupiště	k1gNnSc1	nástupiště
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgNnSc1d1	ostrovní
<g/>
,	,	kIx,	,
jednolodní	jednolodní	k2eAgNnSc1d1	jednolodní
<g/>
,	,	kIx,	,
bezsloupové	bezsloupový	k2eAgNnSc1d1	bezsloupový
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
halu	hala	k1gFnSc4	hala
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
na	na	k7c4	na
Evropskou	evropský	k2eAgFnSc4d1	Evropská
třídu	třída	k1gFnSc4	třída
je	být	k5eAaImIp3nS	být
obložen	obložen	k2eAgInSc1d1	obložen
keramickými	keramický	k2eAgFnPc7d1	keramická
glazovanými	glazovaný	k2eAgFnPc7d1	glazovaná
tvarovkami	tvarovka	k1gFnPc7	tvarovka
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
typu	typ	k1gInSc2	typ
Hurdis	Hurdis	k1gFnSc2	Hurdis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vynaloženo	vynaložit	k5eAaPmNgNnS	vynaložit
301	[number]	k4	301
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
však	však	k9	však
ve	v	k7c4	v
stanici	stanice	k1gFnSc4	stanice
započaly	započnout	k5eAaPmAgFnP	započnout
úpravy	úprava	k1gFnPc1	úprava
k	k	k7c3	k
napojení	napojení	k1gNnSc3	napojení
na	na	k7c6	na
nově	nova	k1gFnSc6	nova
budovaný	budovaný	k2eAgInSc4d1	budovaný
úsek	úsek	k1gInSc4	úsek
do	do	k7c2	do
linky	linka	k1gFnSc2	linka
A	a	k9	a
do	do	k7c2	do
Motola	Motola	k1gFnSc1	Motola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
zde	zde	k6eAd1	zde
končila	končit	k5eAaImAgFnS	končit
jen	jen	k9	jen
část	část	k1gFnSc1	část
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
končily	končit	k5eAaImAgInP	končit
vlaky	vlak	k1gInPc1	vlak
minimálně	minimálně	k6eAd1	minimálně
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Petřiny	Petřin	k2eAgFnSc2d1	Petřina
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
končí	končit	k5eAaImIp3nS	končit
všechny	všechen	k3xTgInPc1	všechen
vlaky	vlak	k1gInPc1	vlak
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Motol	Motol	k1gInSc1	Motol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
===	===	k?	===
</s>
</p>
<p>
<s>
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
je	být	k5eAaImIp3nS	být
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
stanice	stanice	k1gFnSc1	stanice
se	s	k7c7	s
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
střední	střední	k2eAgFnSc7d1	střední
lodí	loď	k1gFnSc7	loď
s	s	k7c7	s
devíti	devět	k4xCc7	devět
páry	pár	k1gInPc7	pár
prostupů	prostup	k1gInPc2	prostup
na	na	k7c6	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
43	[number]	k4	43
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Hradčanské	hradčanský	k2eAgFnSc2d1	Hradčanská
vede	vést	k5eAaImIp3nS	vést
jeden	jeden	k4xCgInSc4	jeden
výstup	výstup	k1gInSc4	výstup
po	po	k7c6	po
eskalátorovém	eskalátorový	k2eAgInSc6d1	eskalátorový
tunelu	tunel	k1gInSc6	tunel
s	s	k7c7	s
tříramennými	tříramenný	k2eAgInPc7d1	tříramenný
hlubinnými	hlubinný	k2eAgInPc7d1	hlubinný
eskalátory	eskalátor	k1gInPc7	eskalátor
k	k	k7c3	k
stejnojmenné	stejnojmenný	k2eAgFnSc3d1	stejnojmenná
tramvajové	tramvajový	k2eAgFnSc3d1	tramvajová
zastávce	zastávka	k1gFnSc3	zastávka
a	a	k8xC	a
stanovišti	stanoviště	k1gNnSc3	stanoviště
autobusů	autobus	k1gInPc2	autobus
<g/>
;	;	kIx,	;
možný	možný	k2eAgInSc1d1	možný
je	být	k5eAaImIp3nS	být
i	i	k9	i
přestup	přestup	k1gInSc4	přestup
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
Praha-Dejvice	Praha-Dejvice	k1gFnSc2	Praha-Dejvice
<g/>
.	.	kIx.	.
</s>
<s>
Vestibul	vestibul	k1gInSc1	vestibul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
pod	pod	k7c7	pod
tramvajovou	tramvajový	k2eAgFnSc7d1	tramvajová
zastávkou	zastávka	k1gFnSc7	zastávka
a	a	k8xC	a
do	do	k7c2	do
kterého	který	k3yIgMnSc4	který
eskalátorový	eskalátorový	k2eAgInSc1d1	eskalátorový
tunel	tunel	k1gInSc1	tunel
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
přímo	přímo	k6eAd1	přímo
veprostřed	veprostřed	k6eAd1	veprostřed
je	být	k5eAaImIp3nS	být
vyložen	vyložit	k5eAaPmNgInS	vyložit
opukou	opuka	k1gFnSc7	opuka
s	s	k7c7	s
reliéfní	reliéfní	k2eAgFnSc7d1	reliéfní
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
samotná	samotný	k2eAgFnSc1d1	samotná
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
nástupiště	nástupiště	k1gNnSc2	nástupiště
obložená	obložený	k2eAgFnSc1d1	obložená
hliníkovými	hliníkový	k2eAgInPc7d1	hliníkový
eloxovanými	eloxovaný	k2eAgInPc7d1	eloxovaný
výlisky	výlisek	k1gInPc7	výlisek
oranžové	oranžový	k2eAgFnSc2d1	oranžová
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
plynule	plynule	k6eAd1	plynule
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
barvy	barva	k1gFnSc2	barva
zlatavé	zlatavý	k2eAgFnSc2d1	zlatavá
Champagne	Champagn	k1gInSc5	Champagn
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
úsek	úsek	k1gInSc4	úsek
I.A.	I.A.	k1gFnSc1	I.A.
Výstavba	výstavba	k1gFnSc1	výstavba
Hradčanské	hradčanský	k2eAgNnSc1d1	Hradčanské
v	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
stála	stát	k5eAaImAgFnS	stát
369	[number]	k4	369
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
stanice	stanice	k1gFnSc2	stanice
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
dostavbu	dostavba	k1gFnSc4	dostavba
dalšího	další	k2eAgInSc2d1	další
vestibulu	vestibul	k1gInSc2	vestibul
<g/>
,	,	kIx,	,
směřujícího	směřující	k2eAgInSc2d1	směřující
zhruba	zhruba	k6eAd1	zhruba
k	k	k7c3	k
ulici	ulice	k1gFnSc3	ulice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
vystavěn	vystavět	k5eAaPmNgMnS	vystavět
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
sice	sice	k8xC	sice
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
s	s	k7c7	s
vybudováním	vybudování	k1gNnSc7	vybudování
druhého	druhý	k4xOgInSc2	druhý
výstupu	výstup	k1gInSc2	výstup
se	se	k3xPyFc4	se
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
uvažovat	uvažovat	k5eAaImF	uvažovat
i	i	k9	i
opět	opět	k6eAd1	opět
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
zvažovat	zvažovat	k5eAaImF	zvažovat
systém	systém	k1gInSc4	systém
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Hradčanská	hradčanský	k2eAgFnSc1d1	Hradčanská
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
významným	významný	k2eAgInSc7d1	významný
přestupním	přestupní	k2eAgInSc7d1	přestupní
bodem	bod	k1gInSc7	bod
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
druhy	druh	k1gMnPc7	druh
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Malostranská	malostranský	k2eAgFnSc1d1	Malostranská
===	===	k?	===
</s>
</p>
<p>
<s>
Malostranská	malostranský	k2eAgFnSc1d1	Malostranská
je	být	k5eAaImIp3nS	být
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	s	k7c7	s
32	[number]	k4	32
m	m	kA	m
pod	pod	k7c7	pod
Klárovem	Klárov	k1gInSc7	Klárov
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc1	čtyři
páry	pára	k1gFnPc1	pára
prostupů	prostup	k1gInPc2	prostup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
a	a	k8xC	a
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
střední	střední	k2eAgFnSc4d1	střední
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Dostavba	dostavba	k1gFnSc1	dostavba
dodatečného	dodatečný	k2eAgInSc2d1	dodatečný
vestibulu	vestibul	k1gInSc2	vestibul
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
umístění	umístění	k1gNnSc2	umístění
stanice	stanice	k1gFnSc2	stanice
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Malostranské	malostranský	k2eAgFnSc2d1	Malostranská
vede	vést	k5eAaImIp3nS	vést
jeden	jeden	k4xCgInSc4	jeden
vestibul	vestibul	k1gInSc4	vestibul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nástupištěm	nástupiště	k1gNnSc7	nástupiště
spojen	spojit	k5eAaPmNgInS	spojit
dvěma	dva	k4xCgFnPc7	dva
trojicemi	trojice	k1gFnPc7	trojice
eskalátorů	eskalátor	k1gInPc2	eskalátor
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
78	[number]	k4	78
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
probíhala	probíhat	k5eAaImAgFnS	probíhat
výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
vybudování	vybudování	k1gNnSc4	vybudování
vynaloženo	vynaložit	k5eAaPmNgNnS	vynaložit
248	[number]	k4	248
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
zatopena	zatopen	k2eAgFnSc1d1	zatopena
pětisetletou	pětisetletý	k2eAgFnSc7d1	pětisetletá
povodní	povodeň	k1gFnSc7	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Obkladem	obklad	k1gInSc7	obklad
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc4d1	šedý
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
kámen	kámen	k1gInSc4	kámen
a	a	k8xC	a
eloxovanéhliníkové	eloxovanéhliníkový	k2eAgInPc4d1	eloxovanéhliníkový
výlisky	výlisek	k1gInPc4	výlisek
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
===	===	k?	===
</s>
</p>
<p>
<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
je	být	k5eAaImIp3nS	být
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
ražená	ražený	k2eAgFnSc1d1	ražená
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
se	s	k7c7	s
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
středním	střední	k2eAgInSc7d1	střední
tunelem	tunel	k1gInSc7	tunel
a	a	k8xC	a
s	s	k7c7	s
osmi	osm	k4xCc7	osm
páry	pár	k1gInPc4	pár
prostupů	prostup	k1gInPc2	prostup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
nacházející	nacházející	k2eAgNnSc4d1	nacházející
se	s	k7c7	s
28	[number]	k4	28
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
vede	vést	k5eAaImIp3nS	vést
jeden	jeden	k4xCgInSc4	jeden
eskalátorový	eskalátorový	k2eAgInSc4d1	eskalátorový
tunel	tunel	k1gInSc4	tunel
do	do	k7c2	do
vestibulu	vestibul	k1gInSc2	vestibul
pod	pod	k7c7	pod
Kaprovou	kaprový	k2eAgFnSc7d1	Kaprová
ulicí	ulice	k1gFnSc7	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
přestup	přestup	k1gInSc1	přestup
na	na	k7c4	na
tramvaje	tramvaj	k1gFnPc4	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
také	také	k9	také
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
vybudování	vybudování	k1gNnSc6	vybudování
druhého	druhý	k4xOgInSc2	druhý
vestibulu	vestibul	k1gInSc2	vestibul
s	s	k7c7	s
vyústěním	vyústění	k1gNnSc7	vyústění
u	u	k7c2	u
Staroměstského	staroměstský	k2eAgNnSc2d1	Staroměstské
náměstí	náměstí	k1gNnSc2	náměstí
–	–	k?	–
koncepce	koncepce	k1gFnSc2	koncepce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
počítala	počítat	k5eAaImAgFnS	počítat
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
výstavbou	výstavba	k1gFnSc7	výstavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
myšlenka	myšlenka	k1gFnSc1	myšlenka
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
odložena	odložit	k5eAaPmNgFnS	odložit
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
kvůli	kvůli	k7c3	kvůli
extrémně	extrémně	k6eAd1	extrémně
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Obkladem	obklad	k1gInSc7	obklad
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
kámen	kámen	k1gInSc1	kámen
a	a	k8xC	a
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
nástupiště	nástupiště	k1gNnSc2	nástupiště
červené	červená	k1gFnSc2	červená
a	a	k8xC	a
zlaté	zlatý	k2eAgInPc1d1	zlatý
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
eloxované	eloxovaný	k2eAgInPc1d1	eloxovaný
výlisky	výlisek	k1gInPc1	výlisek
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc1	stanice
stála	stát	k5eAaImAgFnS	stát
265	[number]	k4	265
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Můstek	můstek	k1gInSc1	můstek
===	===	k?	===
</s>
</p>
<p>
<s>
Přestupní	přestupní	k2eAgFnSc1d1	přestupní
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
<g/>
,	,	kIx,	,
ražená	ražený	k2eAgFnSc1d1	ražená
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
délkou	délka	k1gFnSc7	délka
středního	střední	k2eAgInSc2d1	střední
tunelu	tunel	k1gInSc2	tunel
167	[number]	k4	167
m	m	kA	m
a	a	k8xC	a
24	[number]	k4	24
páry	pára	k1gFnSc2	pára
sloupů	sloup	k1gInPc2	sloup
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgNnSc3d1	vysoké
vytížení	vytížení	k1gNnSc3	vytížení
nebyly	být	k5eNaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
prostupy	prostup	k1gInPc1	prostup
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
velmi	velmi	k6eAd1	velmi
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
výstavbu	výstavba	k1gFnSc4	výstavba
jak	jak	k6eAd1	jak
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	a	k9	a
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
29,3	[number]	k4	29,3
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
výstupy	výstup	k1gInPc4	výstup
-	-	kIx~	-
jeden	jeden	k4xCgInSc1	jeden
vychází	vycházet	k5eAaImIp3nS	vycházet
eskalátorovým	eskalátorův	k2eAgInSc7d1	eskalátorův
tunelem	tunel	k1gInSc7	tunel
do	do	k7c2	do
podpovrchového	podpovrchový	k2eAgInSc2d1	podpovrchový
vestibulu	vestibul	k1gInSc2	vestibul
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
Václavského	václavský	k2eAgNnSc2d1	Václavské
náměstí	náměstí	k1gNnSc2	náměstí
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
vede	vést	k5eAaImIp3nS	vést
obdobně	obdobně	k6eAd1	obdobně
pod	pod	k7c4	pod
křižovatku	křižovatka	k1gFnSc4	křižovatka
Můstek	můstek	k1gInSc1	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Eskalátory	eskalátor	k1gInPc1	eskalátor
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
nejdříve	dříve	k6eAd3	dříve
sovětské	sovětský	k2eAgInPc1d1	sovětský
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byly	být	k5eAaImAgFnP	být
jako	jako	k9	jako
první	první	k4xOgInSc4	první
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	a	k9	a
vyměněny	vyměněn	k2eAgFnPc1d1	vyměněna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
oba	dva	k4xCgMnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Sloupy	sloup	k1gInPc1	sloup
jsou	být	k5eAaImIp3nP	být
obložené	obložený	k2eAgMnPc4d1	obložený
hnědým	hnědý	k2eAgInSc7d1	hnědý
dekorativním	dekorativní	k2eAgInSc7d1	dekorativní
kamenem	kámen	k1gInSc7	kámen
<g/>
,	,	kIx,	,
strop	strop	k1gInSc1	strop
střední	střední	k2eAgFnSc2d1	střední
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
stěny	stěna	k1gFnSc2	stěna
za	za	k7c7	za
nástupištěm	nástupiště	k1gNnSc7	nástupiště
pak	pak	k6eAd1	pak
hliníkovými	hliníkový	k2eAgInPc7d1	hliníkový
eloxovanými	eloxovaný	k2eAgInPc7d1	eloxovaný
výlisky	výlisek	k1gInPc7	výlisek
ve	v	k7c6	v
žluté	žlutý	k2eAgFnSc6d1	žlutá
a	a	k8xC	a
zlatavé	zlatavý	k2eAgFnSc6d1	zlatavá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Muzeum	muzeum	k1gNnSc4	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
<g/>
,	,	kIx,	,
pilířová	pilířový	k2eAgFnSc1d1	pilířová
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zkrácenou	zkrácený	k2eAgFnSc4d1	zkrácená
střední	střední	k2eAgFnSc4d1	střední
loď	loď	k1gFnSc4	loď
na	na	k7c4	na
69,1	[number]	k4	69,1
m	m	kA	m
<g/>
,	,	kIx,	,
9	[number]	k4	9
párů	pár	k1gInPc2	pár
prostupů	prostup	k1gInPc2	prostup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
lodi	loď	k1gFnSc2	loď
vede	vést	k5eAaImIp3nS	vést
eskalátorový	eskalátorový	k2eAgInSc1d1	eskalátorový
tunel	tunel	k1gInSc1	tunel
vedoucí	vedoucí	k1gFnSc2	vedoucí
pod	pod	k7c4	pod
nástupiště	nástupiště	k1gNnSc4	nástupiště
trasy	trasa	k1gFnSc2	trasa
C	C	kA	C
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
přestup	přestup	k1gInSc1	přestup
umožněn	umožnit	k5eAaPmNgInS	umožnit
krátkými	krátký	k2eAgInPc7d1	krátký
eskalátory	eskalátor	k1gInPc7	eskalátor
vedoucími	vedoucí	k2eAgInPc7d1	vedoucí
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
výstup	výstup	k1gInSc1	výstup
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
vede	vést	k5eAaImIp3nS	vést
bokem	bokem	k6eAd1	bokem
nad	nad	k7c7	nad
jižní	jižní	k2eAgFnSc7d1	jižní
kolejí	kolej	k1gFnSc7	kolej
eskalátorovým	eskalátorův	k2eAgFnPc3d1	eskalátorův
tunelem	tunel	k1gInSc7	tunel
nacházejícím	nacházející	k2eAgFnPc3d1	nacházející
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
s	s	k7c7	s
nástupištěm	nástupiště	k1gNnSc7	nástupiště
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
chodbou	chodba	k1gFnSc7	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Obkladem	obklad	k1gInSc7	obklad
stanice	stanice	k1gFnSc2	stanice
jsou	být	k5eAaImIp3nP	být
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
eloxované	eloxovaný	k2eAgInPc1d1	eloxovaný
výlisky	výlisek	k1gInPc1	výlisek
hnědé	hnědý	k2eAgFnSc2d1	hnědá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
mramorové	mramorový	k2eAgFnSc2d1	mramorová
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
však	však	k9	však
interiér	interiér	k1gInSc4	interiér
stanice	stanice	k1gFnSc2	stanice
Muzeum	muzeum	k1gNnSc4	muzeum
A	a	k8xC	a
již	již	k6eAd1	již
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
původnímu	původní	k2eAgInSc3d1	původní
stavu	stav	k1gInSc3	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
použita	použit	k2eAgNnPc1d1	použito
neodpovídající	odpovídající	k2eNgNnPc1d1	neodpovídající
závěsná	závěsný	k2eAgNnPc1d1	závěsné
svítidla	svítidlo	k1gNnPc1	svítidlo
a	a	k8xC	a
ta	ten	k3xDgNnPc1	ten
původní	původní	k2eAgNnPc1d1	původní
jsou	být	k5eAaImIp3nP	být
nevyužívána	využíván	k2eNgNnPc1d1	nevyužíváno
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
nejsou	být	k5eNaImIp3nP	být
nasvíceny	nasvítit	k5eAaPmNgInP	nasvítit
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
výlisky	výlisek	k1gInPc1	výlisek
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
původních	původní	k2eAgInPc2d1	původní
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stanice	stanice	k1gFnSc1	stanice
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
I.	I.	kA	I.
A.	A.	kA	A.
neprošla	projít	k5eNaPmAgFnS	projít
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
stanice	stanice	k1gFnSc2	stanice
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
staré	stará	k1gFnPc1	stará
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
let	léto	k1gNnPc2	léto
a	a	k8xC	a
nevyměňované	vyměňovaný	k2eNgInPc4d1	vyměňovaný
<g/>
;	;	kIx,	;
obklady	obklad	k1gInPc4	obklad
reznou	reznout	k5eAaImIp3nP	reznout
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
ošetřovány	ošetřovat	k5eAaImNgInP	ošetřovat
od	od	k7c2	od
zbytků	zbytek	k1gInPc2	zbytek
průsakové	průsakový	k2eAgFnSc2d1	průsaková
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Osvětlení	osvětlení	k1gNnSc1	osvětlení
mezi	mezi	k7c7	mezi
nosnými	nosný	k2eAgInPc7d1	nosný
pilíři	pilíř	k1gInPc7	pilíř
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
vytrháno	vytrhán	k2eAgNnSc1d1	vytrháno
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
části	část	k1gFnPc1	část
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
A	a	k8xC	a
a	a	k8xC	a
bez	bez	k7c2	bez
výtahu	výtah	k1gInSc2	výtah
<g/>
)	)	kIx)	)
činily	činit	k5eAaImAgInP	činit
230	[number]	k4	230
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Náměstí	náměstí	k1gNnSc2	náměstí
Míru	mír	k1gInSc2	mír
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
délkou	délka	k1gFnSc7	délka
střední	střední	k2eAgFnSc2d1	střední
lodi	loď	k1gFnSc2	loď
na	na	k7c4	na
68,8	[number]	k4	68,8
m	m	kA	m
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc2	sedm
páry	pár	k1gInPc4	pár
prostupů	prostup	k1gInPc2	prostup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
konstruována	konstruovat	k5eAaImNgFnS	konstruovat
jako	jako	k9	jako
koncová	koncový	k2eAgFnSc1d1	koncová
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
303	[number]	k4	303
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
53	[number]	k4	53
m	m	kA	m
hluboko	hluboko	k6eAd1	hluboko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
suverénně	suverénně	k6eAd1	suverénně
nejhlubší	hluboký	k2eAgFnSc7d3	nejhlubší
stanicí	stanice	k1gFnSc7	stanice
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
;	;	kIx,	;
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
je	být	k5eAaImIp3nS	být
139	[number]	k4	139
m.	m.	k?	m.
Ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
lodi	loď	k1gFnSc2	loď
vede	vést	k5eAaImIp3nS	vést
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
eskalátorový	eskalátorový	k2eAgInSc1d1	eskalátorový
tunel	tunel	k1gInSc1	tunel
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
a	a	k8xC	a
v	v	k7c6	v
EU	EU	kA	EU
-	-	kIx~	-
délka	délka	k1gFnSc1	délka
87	[number]	k4	87
m	m	kA	m
<g/>
)	)	kIx)	)
do	do	k7c2	do
vestibulu	vestibul	k1gInSc2	vestibul
pod	pod	k7c7	pod
náměstím	náměstí	k1gNnSc7	náměstí
Míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
nástupiště	nástupiště	k1gNnSc2	nástupiště
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
lodě	loď	k1gFnPc4	loď
obloženy	obložen	k2eAgFnPc4d1	obložena
hliníkovými	hliníkový	k2eAgInPc7d1	hliníkový
eloxovanými	eloxovaný	k2eAgInPc7d1	eloxovaný
výlisky	výlisek	k1gInPc7	výlisek
ve	v	k7c6	v
zlatavé	zlatavý	k2eAgFnSc6d1	zlatavá
a	a	k8xC	a
modré	modrý	k2eAgFnSc6d1	modrá
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
sloupy	sloup	k1gInPc1	sloup
jsou	být	k5eAaImIp3nP	být
obloženy	obložen	k2eAgInPc1d1	obložen
nerezovýmiprvky	nerezovýmiprvek	k1gInPc1	nerezovýmiprvek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
kompletně	kompletně	k6eAd1	kompletně
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
dispozice	dispozice	k1gFnSc1	dispozice
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vybudovat	vybudovat	k5eAaPmF	vybudovat
další	další	k2eAgInSc4d1	další
eskalátorový	eskalátorový	k2eAgInSc4d1	eskalátorový
tunel	tunel	k1gInSc4	tunel
s	s	k7c7	s
vestibulem	vestibul	k1gInSc7	vestibul
ve	v	k7c6	v
Vinohradské	vinohradský	k2eAgFnSc6d1	Vinohradská
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
případně	případně	k6eAd1	případně
přestup	přestup	k1gInSc4	přestup
na	na	k7c4	na
projektovanou	projektovaný	k2eAgFnSc4d1	projektovaná
trasu	trasa	k1gFnSc4	trasa
D.	D.	kA	D.
Za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
Muzeum	muzeum	k1gNnSc1	muzeum
ústí	ústit	k5eAaImIp3nS	ústit
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
kolejích	kolej	k1gFnPc6	kolej
traťové	traťový	k2eAgFnPc1d1	traťová
spojky	spojka	k1gFnPc1	spojka
spojující	spojující	k2eAgFnSc4d1	spojující
linku	linka	k1gFnSc4	linka
A	a	k9	a
s	s	k7c7	s
linkou	linka	k1gFnSc7	linka
C	C	kA	C
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
ústí	ústit	k5eAaImIp3nP	ústit
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgInSc2d1	Pavlův
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
koncipována	koncipovat	k5eAaBmNgFnS	koncipovat
jako	jako	k9	jako
koncová	koncový	k2eAgFnSc1d1	koncová
bez	bez	k7c2	bez
odstavných	odstavný	k2eAgFnPc2d1	odstavná
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Obraty	obrat	k1gInPc1	obrat
vlaků	vlak	k1gInPc2	vlak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
provádět	provádět	k5eAaImF	provádět
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
Skalka	Skalka	k1gMnSc1	Skalka
přejezdem	přejezd	k1gInSc7	přejezd
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
po	po	k7c6	po
kolejové	kolejový	k2eAgFnSc6d1	kolejová
spojce	spojka	k1gFnSc6	spojka
před	před	k7c7	před
stanicí	stanice	k1gFnSc7	stanice
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
se	se	k3xPyFc4	se
souprava	souprava	k1gFnSc1	souprava
po	po	k7c4	po
vysazení	vysazení	k1gNnSc4	vysazení
cestujících	cestující	k1gMnPc2	cestující
obrátí	obrátit	k5eAaPmIp3nS	obrátit
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
spojku	spojka	k1gFnSc4	spojka
obráceně	obráceně	k6eAd1	obráceně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
pilířová	pilířový	k2eAgFnSc1d1	pilířová
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc4d1	trojlodní
se	s	k7c7	s
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
středním	střední	k2eAgInSc7d1	střední
tunelem	tunel	k1gInSc7	tunel
na	na	k7c4	na
34	[number]	k4	34
m	m	kA	m
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc2	sedm
páry	pár	k1gInPc4	pár
prostupů	prostup	k1gInPc2	prostup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dostavět	dostavět	k5eAaPmF	dostavět
druhý	druhý	k4xOgInSc4	druhý
eskalátorový	eskalátorový	k2eAgInSc4d1	eskalátorový
tunel	tunel	k1gInSc4	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
107	[number]	k4	107
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
a	a	k8xC	a
45	[number]	k4	45
m	m	kA	m
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Náměstí	náměstí	k1gNnSc1	náměstí
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
výstup	výstup	k1gInSc1	výstup
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
lodi	loď	k1gFnSc2	loď
eskalátorovým	eskalátorův	k2eAgInSc7d1	eskalátorův
tunelem	tunel	k1gInSc7	tunel
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
73	[number]	k4	73
m	m	kA	m
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
na	na	k7c4	na
podzemní	podzemní	k2eAgInSc4d1	podzemní
vestibul	vestibul	k1gInSc4	vestibul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
nástupiště	nástupiště	k1gNnSc2	nástupiště
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
lodě	loď	k1gFnPc4	loď
obloženy	obložen	k2eAgFnPc4d1	obložena
hliníkovými	hliníkový	k2eAgInPc7d1	hliníkový
výlisky	výlisek	k1gInPc7	výlisek
<g/>
,	,	kIx,	,
zlatavými	zlatavý	k2eAgInPc7d1	zlatavý
a	a	k8xC	a
tyrkysovými	tyrkysový	k2eAgInPc7d1	tyrkysový
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
okrasnými	okrasný	k2eAgFnPc7d1	okrasná
kamennými	kamenný	k2eAgFnPc7d1	kamenná
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
stanice	stanice	k1gFnSc2	stanice
bylo	být	k5eAaImAgNnS	být
vynaloženo	vynaložit	k5eAaPmNgNnS	vynaložit
264	[number]	k4	264
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Flora	Flora	k1gFnSc1	Flora
===	===	k?	===
</s>
</p>
<p>
<s>
Flora	Flora	k1gFnSc1	Flora
je	být	k5eAaImIp3nS	být
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
<g/>
,	,	kIx,	,
ražená	ražený	k2eAgFnSc1d1	ražená
stanice	stanice	k1gFnSc1	stanice
se	s	k7c7	s
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
středním	střední	k2eAgInSc7d1	střední
tunelem	tunel	k1gInSc7	tunel
na	na	k7c4	na
33	[number]	k4	33
m	m	kA	m
<g/>
,	,	kIx,	,
šesti	šest	k4xCc6	šest
páry	pár	k1gInPc4	pár
prostupů	prostup	k1gInPc2	prostup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
a	a	k8xC	a
s	s	k7c7	s
montovaným	montovaný	k2eAgNnSc7d1	montované
litinovým	litinový	k2eAgNnSc7d1	litinové
ostěním	ostění	k1gNnSc7	ostění
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
je	být	k5eAaImIp3nS	být
108	[number]	k4	108
m	m	kA	m
a	a	k8xC	a
hluboko	hluboko	k6eAd1	hluboko
jen	jen	k9	jen
25,4	[number]	k4	25,4
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejmělčích	mělký	k2eAgFnPc2d3	nejmělčí
stanic	stanice	k1gFnPc2	stanice
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
trase	trasa	k1gFnSc6	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
lodi	loď	k1gFnSc2	loď
vede	vést	k5eAaImIp3nS	vést
jeden	jeden	k4xCgInSc4	jeden
výstup	výstup	k1gInSc4	výstup
eskalátorovým	eskalátorův	k2eAgInSc7d1	eskalátorův
tunelem	tunel	k1gInSc7	tunel
do	do	k7c2	do
podzemního	podzemní	k2eAgInSc2d1	podzemní
vestibulu	vestibul	k1gInSc2	vestibul
(	(	kIx(	(
<g/>
4,75	[number]	k4	4,75
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
napojeného	napojený	k2eAgMnSc4d1	napojený
na	na	k7c4	na
Palác	palác	k1gInSc4	palác
Flora	Flor	k1gInSc2	Flor
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
přímým	přímý	k2eAgInSc7d1	přímý
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
podzemního	podzemní	k2eAgNnSc2d1	podzemní
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
vede	vést	k5eAaImIp3nS	vést
dalších	další	k2eAgInPc2d1	další
šest	šest	k4xCc4	šest
výstupů	výstup	k1gInPc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
obložena	obložit	k5eAaPmNgFnS	obložit
zlatavými	zlatavý	k2eAgInPc7d1	zlatavý
a	a	k8xC	a
vínovými	vínový	k2eAgInPc7d1	vínový
hliníkovými	hliníkový	k2eAgInPc7d1	hliníkový
výlisky	výlisek	k1gInPc7	výlisek
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
okrasným	okrasný	k2eAgInSc7d1	okrasný
šedivým	šedivý	k2eAgInSc7d1	šedivý
kamenem	kámen	k1gInSc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc2	stanice
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
stála	stát	k5eAaImAgFnS	stát
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
254	[number]	k4	254
miliónů	milión	k4xCgInPc2	milión
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
tanice	tanice	k1gFnSc1	tanice
je	být	k5eAaImIp3nS	být
propojená	propojený	k2eAgFnSc1d1	propojená
s	s	k7c7	s
nákupním	nákupní	k2eAgNnSc7d1	nákupní
centrem	centrum	k1gNnSc7	centrum
Palác	palác	k1gInSc1	palác
Flora	Flora	k1gFnSc1	Flora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnPc4	stanice
Želivského	želivský	k2eAgInSc2d1	želivský
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
ražená	ražený	k2eAgFnSc1d1	ražená
<g/>
,	,	kIx,	,
pilířová	pilířový	k2eAgFnSc1d1	pilířová
<g/>
,	,	kIx,	,
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
se	s	k7c7	s
zkrácenou	zkrácený	k2eAgFnSc7d1	zkrácená
střední	střední	k2eAgFnSc7d1	střední
lodí	loď	k1gFnSc7	loď
a	a	k8xC	a
s	s	k7c7	s
10	[number]	k4	10
páry	pára	k1gFnSc2	pára
prostupů	prostup	k1gInPc2	prostup
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
26,6	[number]	k4	26,6
m	m	kA	m
hluboko	hluboko	k6eAd1	hluboko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
148	[number]	k4	148
m	m	kA	m
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nástupiště	nástupiště	k1gNnSc2	nástupiště
100	[number]	k4	100
m.	m.	k?	m.
Obkladem	obklad	k1gInSc7	obklad
stanice	stanice	k1gFnSc2	stanice
jsou	být	k5eAaImIp3nP	být
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
eloxované	eloxovaný	k2eAgInPc1d1	eloxovaný
výlisky	výlisek	k1gInPc1	výlisek
v	v	k7c6	v
hnědožluté	hnědožlutý	k2eAgFnSc6d1	hnědožlutá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
vestibul	vestibul	k1gInSc4	vestibul
pod	pod	k7c7	pod
Vinohradskou	vinohradský	k2eAgFnSc7d1	Vinohradská
třídou	třída	k1gFnSc7	třída
<g/>
,	,	kIx,	,
5,6	[number]	k4	5,6
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vestibul	vestibul	k1gInSc1	vestibul
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
spojen	spojit	k5eAaPmNgInS	spojit
třemi	tři	k4xCgInPc7	tři
výstupy	výstup	k1gInPc7	výstup
s	s	k7c7	s
eskalátory	eskalátor	k1gInPc7	eskalátor
a	a	k8xC	a
s	s	k7c7	s
nástupištěm	nástupiště	k1gNnSc7	nástupiště
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
eskalátorovým	eskalátorův	k2eAgInSc7d1	eskalátorův
tunelem	tunel	k1gInSc7	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
dvě	dva	k4xCgFnPc1	dva
odstavné	odstavný	k2eAgFnPc1d1	odstavná
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
obratová	obratový	k2eAgFnSc1d1	obratová
kolej	kolej	k1gFnSc1	kolej
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgNnSc1d1	umožňující
ukončení	ukončení	k1gNnSc1	ukončení
celé	celý	k2eAgFnSc2d1	celá
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Proražením	proražení	k1gNnSc7	proražení
tunelů	tunel	k1gInPc2	tunel
za	za	k7c7	za
odstavnými	odstavný	k2eAgFnPc7d1	odstavná
kolejemi	kolej	k1gFnPc7	kolej
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
trasy	trasa	k1gFnSc2	trasa
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Strašnická	strašnický	k2eAgFnSc1d1	Strašnická
<g/>
.	.	kIx.	.
</s>
<s>
Obratová	obratový	k2eAgFnSc1d1	obratová
kolej	kolej	k1gFnSc1	kolej
se	se	k3xPyFc4	se
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
obratům	obrat	k1gInPc3	obrat
nebo	nebo	k8xC	nebo
deponování	deponování	k1gNnSc3	deponování
souprav	souprava	k1gFnPc2	souprava
např.	např.	kA	např.
při	při	k7c6	při
mimořádnostech	mimořádnost	k1gFnPc6	mimořádnost
(	(	kIx(	(
<g/>
závada	závada	k1gFnSc1	závada
na	na	k7c6	na
soupravě	souprava	k1gFnSc6	souprava
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
stála	stát	k5eAaImAgFnS	stát
301	[number]	k4	301
milionů	milion	k4xCgInPc2	milion
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
zatím	zatím	k6eAd1	zatím
neprošla	projít	k5eNaPmAgFnS	projít
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc7d1	poslední
ukázkou	ukázka	k1gFnSc7	ukázka
původní	původní	k2eAgFnSc2d1	původní
architektury	architektura	k1gFnSc2	architektura
trasy	trasa	k1gFnSc2	trasa
AI	AI	kA	AI
resp.	resp.	kA	resp.
AII	AII	kA	AII
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
závěsných	závěsný	k2eAgNnPc2d1	závěsné
svítidel	svítidlo	k1gNnPc2	svítidlo
v	v	k7c6	v
eskalátorovém	eskalátorový	k2eAgInSc6d1	eskalátorový
tunelu	tunel	k1gInSc6	tunel
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tvarovaným	tvarovaný	k2eAgNnSc7d1	tvarované
hliníkovým	hliníkový	k2eAgNnSc7d1	hliníkové
obložením	obložení	k1gNnSc7	obložení
stěn	stěna	k1gFnPc2	stěna
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
pohlcovač	pohlcovač	k1gInSc4	pohlcovač
provozního	provozní	k2eAgInSc2d1	provozní
hluku	hluk	k1gInSc2	hluk
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInPc1d1	dolní
staniční	staniční	k2eAgInPc1d1	staniční
prostory	prostor	k1gInPc1	prostor
jsou	být	k5eAaImIp3nP	být
znehodnoceny	znehodnotit	k5eAaPmNgInP	znehodnotit
průsaky	průsak	k1gInPc1	průsak
a	a	k8xC	a
nánosy	nános	k1gInPc1	nános
rzi	rez	k1gFnSc2	rez
na	na	k7c6	na
hliníkových	hliníkový	k2eAgInPc6d1	hliníkový
a	a	k8xC	a
tombakových	tombakový	k2eAgInPc6d1	tombakový
obkladech	obklad	k1gInPc6	obklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Strašnická	strašnický	k2eAgFnSc1d1	Strašnická
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
Strašnická	strašnický	k2eAgFnSc1d1	Strašnická
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
ulicí	ulice	k1gFnSc7	ulice
Starostrašnická	Starostrašnický	k2eAgFnSc1d1	Starostrašnická
<g/>
,	,	kIx,	,
7,5	[number]	k4	7,5
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hloubená	hloubený	k2eAgFnSc1d1	hloubená
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
jámě	jáma	k1gFnSc6	jáma
<g/>
,	,	kIx,	,
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
koncová	koncový	k2eAgFnSc1d1	koncová
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
stanicí	stanice	k1gFnSc7	stanice
je	být	k5eAaImIp3nS	být
kolejové	kolejový	k2eAgNnSc1d1	kolejové
křížení	křížení	k1gNnSc1	křížení
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
dočasné	dočasný	k2eAgNnSc4d1	dočasné
ukončení	ukončení	k1gNnSc4	ukončení
trasy	trasa	k1gFnSc2	trasa
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
obratovými	obratový	k2eAgFnPc7d1	obratová
kolejemi	kolej	k1gFnPc7	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
nadzemní	nadzemní	k2eAgInSc4d1	nadzemní
vestibul	vestibul	k1gInSc4	vestibul
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
o	o	k7c6	o
půdorysu	půdorys	k1gInSc6	půdorys
kruhu	kruh	k1gInSc2	kruh
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
cca	cca	kA	cca
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
u	u	k7c2	u
křižovatky	křižovatka	k1gFnSc2	křižovatka
Starostrašnické	Starostrašnický	k2eAgFnSc2d1	Starostrašnická
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
ulice	ulice	k1gFnSc2	ulice
V	v	k7c6	v
Olšinách	olšina	k1gFnPc6	olšina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vestibulu	vestibul	k1gInSc2	vestibul
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
jihovýchodního	jihovýchodní	k2eAgInSc2d1	jihovýchodní
konce	konec	k1gInSc2	konec
nástupiště	nástupiště	k1gNnSc4	nástupiště
pevné	pevný	k2eAgNnSc1d1	pevné
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
části	část	k1gFnSc6	část
byla	být	k5eAaImAgFnS	být
dodatečně	dodatečně	k6eAd1	dodatečně
vybudována	vybudován	k2eAgFnSc1d1	vybudována
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
šikmá	šikmý	k2eAgFnSc1d1	šikmá
plošina	plošina	k1gFnSc1	plošina
pro	pro	k7c4	pro
handicapované	handicapovaný	k2eAgMnPc4d1	handicapovaný
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Skalka	skalka	k1gFnSc1	skalka
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
Skalka	skalka	k1gFnSc1	skalka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
sídlišti	sídliště	k1gNnSc6	sídliště
Skalka	skalka	k1gFnSc1	skalka
pod	pod	k7c7	pod
ulicí	ulice	k1gFnSc7	ulice
Na	na	k7c6	na
Padesátém	padesátý	k4xOgMnSc6	padesátý
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hloubená	hloubený	k2eAgFnSc1d1	hloubená
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
jámě	jáma	k1gFnSc6	jáma
<g/>
,	,	kIx,	,
budovaná	budovaný	k2eAgFnSc1d1	budovaná
za	za	k7c2	za
provozu	provoz	k1gInSc2	provoz
jako	jako	k8xS	jako
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
koncová	koncový	k2eAgFnSc1d1	koncová
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
monolitická	monolitický	k2eAgFnSc1d1	monolitická
a	a	k8xC	a
přizpůsobená	přizpůsobený	k2eAgFnSc1d1	přizpůsobená
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
stavěna	stavit	k5eAaImNgFnS	stavit
za	za	k7c2	za
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Skalka	skalka	k1gFnSc1	skalka
je	být	k5eAaImIp3nS	být
9,25	[number]	k4	9,25
m	m	kA	m
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
vestibul	vestibul	k1gInSc4	vestibul
spojený	spojený	k2eAgInSc4d1	spojený
s	s	k7c7	s
nástupištěm	nástupiště	k1gNnSc7	nástupiště
schodištěm	schodiště	k1gNnSc7	schodiště
vybíhajícím	vybíhající	k2eAgFnPc3d1	vybíhající
z	z	k7c2	z
prostředku	prostředek	k1gInSc2	prostředek
nástupiště	nástupiště	k1gNnSc2	nástupiště
a	a	k8xC	a
potom	potom	k6eAd1	potom
vedoucího	vedoucí	k1gMnSc4	vedoucí
šikmo	šikmo	k6eAd1	šikmo
mimo	mimo	k7c4	mimo
osu	osa	k1gFnSc4	osa
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
obložena	obložit	k5eAaPmNgFnS	obložit
glazovanými	glazovaný	k2eAgFnPc7d1	glazovaná
tvarovkami	tvarovka	k1gFnPc7	tvarovka
Hurdis	Hurdis	k1gFnPc7	Hurdis
bílými	bílý	k2eAgFnPc7d1	bílá
<g/>
,	,	kIx,	,
modrými	modrý	k2eAgFnPc7d1	modrá
a	a	k8xC	a
hnědými	hnědý	k2eAgFnPc7d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
letech	let	k1gInPc6	let
1987	[number]	k4	1987
–	–	k?	–
<g/>
1990	[number]	k4	1990
stála	stát	k5eAaImAgFnS	stát
291	[number]	k4	291
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
výtahu	výtah	k1gInSc2	výtah
pro	pro	k7c4	pro
imobilní	imobilní	k2eAgFnPc4d1	imobilní
osoby	osoba	k1gFnPc4	osoba
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Skalka	skalka	k1gFnSc1	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
mohli	moct	k5eAaImAgMnP	moct
cestující	cestující	k1gMnPc1	cestující
již	již	k6eAd1	již
výtah	výtah	k1gInSc4	výtah
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stanice	stanice	k1gFnSc1	stanice
Depo	depo	k1gNnSc1	depo
Hostivař	Hostivař	k1gFnSc1	Hostivař
===	===	k?	===
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
neobvyklé	obvyklý	k2eNgFnPc4d1	neobvyklá
koncepce	koncepce	k1gFnPc4	koncepce
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
depa	depo	k1gNnSc2	depo
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
zabírá	zabírat	k5eAaImIp3nS	zabírat
jeho	jeho	k3xOp3gFnPc1	jeho
dvě	dva	k4xCgFnPc1	dva
západní	západní	k2eAgFnPc1d1	západní
koleje	kolej	k1gFnPc1	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudován	k2eAgNnSc4d1	vybudováno
ostrovní	ostrovní	k2eAgNnSc4d1	ostrovní
nástupiště	nástupiště	k1gNnSc4	nástupiště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
síti	síť	k1gFnSc6	síť
metra	metro	k1gNnSc2	metro
vůbec	vůbec	k9	vůbec
nejužší	úzký	k2eAgNnSc1d3	nejužší
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
bývalé	bývalý	k2eAgFnSc2d1	bývalá
myčky	myčka	k1gFnSc2	myčka
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Osvětlení	osvětlení	k1gNnSc1	osvětlení
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
dne	den	k1gInSc2	den
řešeno	řešen	k2eAgNnSc1d1	řešeno
prosklenou	prosklený	k2eAgFnSc7d1	prosklená
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
malým	malý	k2eAgInSc7d1	malý
vestibulem	vestibul	k1gInSc7	vestibul
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
terminálu	terminál	k1gInSc3	terminál
příměstských	příměstský	k2eAgFnPc2d1	příměstská
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
jako	jako	k9	jako
přestupní	přestupní	k2eAgFnSc1d1	přestupní
mezi	mezi	k7c7	mezi
metrem	metro	k1gNnSc7	metro
a	a	k8xC	a
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
dopravou	doprava	k1gFnSc7	doprava
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
Depo	depo	k1gNnSc4	depo
Hostivař	Hostivař	k1gFnSc1	Hostivař
zamýšlena	zamýšlen	k2eAgFnSc1d1	zamýšlena
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gInSc1	její
hlavní	hlavní	k2eAgInSc1d1	hlavní
účel	účel	k1gInSc1	účel
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
i	i	k9	i
záchytné	záchytný	k2eAgNnSc4d1	záchytné
parkoviště	parkoviště	k1gNnSc4	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R.	R.	kA	R.
<g/>
Výstavba	výstavba	k1gFnSc1	výstavba
stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
probíhala	probíhat	k5eAaImAgFnS	probíhat
za	za	k7c2	za
provozu	provoz	k1gInSc2	provoz
depa	depo	k1gNnSc2	depo
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
v	v	k7c4	v
15.30	[number]	k4	15.30
hod	hod	k1gInSc4	hod
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
související	související	k2eAgFnPc1d1	související
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
povrchové	povrchový	k2eAgFnSc6d1	povrchová
dopravě	doprava	k1gFnSc6	doprava
byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výstavby	výstavba	k1gFnSc2	výstavba
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přestavět	přestavět	k5eAaPmF	přestavět
část	část	k1gFnSc4	část
kolejiště	kolejiště	k1gNnSc2	kolejiště
před	před	k7c7	před
depem	depo	k1gNnSc7	depo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
zhlaví	zhlaví	k1gNnSc4	zhlaví
<g/>
)	)	kIx)	)
a	a	k8xC	a
upravit	upravit	k5eAaPmF	upravit
jednu	jeden	k4xCgFnSc4	jeden
halu	hala	k1gFnSc4	hala
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nyní	nyní	k6eAd1	nyní
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
vlastní	vlastní	k2eAgFnPc4d1	vlastní
stanice	stanice	k1gFnPc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
tak	tak	k9	tak
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
metra	metro	k1gNnSc2	metro
A	a	k9	a
muselo	muset	k5eAaImAgNnS	muset
vypravovat	vypravovat	k5eAaImF	vypravovat
vlaky	vlak	k1gInPc4	vlak
depo	depo	k1gNnSc1	depo
Kačerov	Kačerov	k1gInSc1	Kačerov
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
komplexu	komplex	k1gInSc2	komplex
včetně	včetně	k7c2	včetně
autobusového	autobusový	k2eAgInSc2d1	autobusový
terminálu	terminál	k1gInSc2	terminál
a	a	k8xC	a
záchytného	záchytný	k2eAgNnSc2d1	záchytné
parkoviště	parkoviště	k1gNnSc2	parkoviště
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgFnP	uvádět
kolem	kolem	k7c2	kolem
1,3	[number]	k4	1,3
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
na	na	k7c4	na
samotnou	samotný	k2eAgFnSc4d1	samotná
stanici	stanice	k1gFnSc4	stanice
metra	metro	k1gNnSc2	metro
860	[number]	k4	860
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
stanice	stanice	k1gFnSc2	stanice
Skalka	Skalka	k1gMnSc1	Skalka
(	(	kIx(	(
<g/>
dřívější	dřívější	k2eAgFnSc2d1	dřívější
konečné	konečná	k1gFnSc2	konečná
<g/>
)	)	kIx)	)
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
1	[number]	k4	1
020	[number]	k4	020
m.	m.	k?	m.
Část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
trati	trať	k1gFnSc2	trať
přes	přes	k7c4	přes
zhlaví	zhlaví	k1gNnSc4	zhlaví
depa	depo	k1gNnSc2	depo
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
nekrytým	krytý	k2eNgInSc7d1	nekrytý
úsekem	úsek	k1gInSc7	úsek
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
s	s	k7c7	s
přepravou	přeprava	k1gFnSc7	přeprava
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
stanice	stanice	k1gFnSc2	stanice
městské	městský	k2eAgFnSc2d1	městská
železnice	železnice	k1gFnSc2	železnice
Praha-Malešice	Praha-Malešice	k1gFnSc2	Praha-Malešice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Linka	linka	k1gFnSc1	linka
A	a	k9	a
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ortofotomapa	Ortofotomapa	k1gFnSc1	Ortofotomapa
se	se	k3xPyFc4	se
zakreslením	zakreslení	k1gNnSc7	zakreslení
tras	trasa	k1gFnPc2	trasa
Dejvická-Ruzyně	Dejvická-Ruzyně	k1gFnSc2	Dejvická-Ruzyně
a	a	k8xC	a
Bílá	bílý	k2eAgFnSc1d1	bílá
Hora-Zličín	Hora-Zličín	k1gInSc1	Hora-Zličín
od	od	k7c2	od
Útvaru	útvar	k1gInSc2	útvar
rozvoje	rozvoj	k1gInSc2	rozvoj
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Ortofotomapa	Ortofotomapa	k1gFnSc1	Ortofotomapa
se	s	k7c7	s
zakreslením	zakreslení	k1gNnSc7	zakreslení
trasy	trasa	k1gFnSc2	trasa
Dejvická-Ruzyně	Dejvická-Ruzyně	k1gFnSc1	Dejvická-Ruzyně
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c6	o
stanici	stanice	k1gFnSc6	stanice
Staré	Staré	k2eAgNnSc2d1	Staré
Letiště	letiště	k1gNnSc2	letiště
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Prodloužení	prodloužení	k1gNnSc1	prodloužení
trasy	trasa	k1gFnSc2	trasa
"	"	kIx"	"
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
metra	metr	k1gMnSc2	metr
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Magistrátu	magistrát	k1gInSc2	magistrát
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
Prodloužení	prodloužení	k1gNnSc1	prodloužení
tratě	trať	k1gFnSc2	trať
A	a	k9	a
<g/>
,	,	kIx,	,
na	na	k7c6	na
webu	web	k1gInSc6	web
Lepší	dobrý	k2eAgFnSc1d2	lepší
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
nedatováno	datován	k2eNgNnSc1d1	nedatováno
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
