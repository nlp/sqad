<s>
ECHL	ECHL	kA
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
nazývána	nazýván	k2eAgFnSc1d1
East	East	k2eAgFnSc1d1
Coast	Coast	k2eAgFnSc1d1
Hockey	Hockea	k2eAgFnSc1d1
League	Leagu	k1gFnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Východopobřežní	Východopobřežní	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
rozšířenému	rozšířený	k2eAgNnSc3d1
působení	působení	k1gNnSc3
změnila	změnit	k5eAaPmAgFnS
oficiální	oficiální	k2eAgFnSc1d1
název	název	k1gInSc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
zkratku	zkratka	k1gFnSc4
<g/>
.	.	kIx.
</s>