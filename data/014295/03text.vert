<s>
ECHL	ECHL	kA
</s>
<s>
ECHL	ECHL	kA
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
nazývána	nazýván	k2eAgFnSc1d1
East	East	k2eAgFnSc1d1
Coast	Coast	k2eAgFnSc1d1
Hockey	Hockea	k2eAgFnSc1d1
League	Leagu	k1gFnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Východopobřežní	Východopobřežní	k2eAgFnSc1d1
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
vzhledem	vzhledem	k7c3
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
rozšířenému	rozšířený	k2eAgNnSc3d1
působení	působení	k1gNnSc3
změnila	změnit	k5eAaPmAgFnS
oficiální	oficiální	k2eAgFnSc1d1
název	název	k1gInSc4
na	na	k7c4
tuto	tento	k3xDgFnSc4
zkratku	zkratka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ECHL	ECHL	kA
je	být	k5eAaImIp3nS
hokejová	hokejový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
soutěži	soutěž	k1gFnSc6
působí	působit	k5eAaImIp3nS
27	#num#	k4
mužstev	mužstvo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
dvě	dva	k4xCgFnPc1
kanadská	kanadský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liga	liga	k1gFnSc1
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
od	od	k7c2
sezony	sezona	k1gFnSc2
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kluby	klub	k1gInPc1
mají	mít	k5eAaImIp3nP
podepsané	podepsaný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
alespoň	alespoň	k9
s	s	k7c7
jedním	jeden	k4xCgMnSc7
z	z	k7c2
celků	celek	k1gInPc2
NHL	NHL	kA
a	a	k8xC
jsou	být	k5eAaImIp3nP
jejich	jejich	k3xOp3gNnPc7
záložními	záložní	k2eAgNnPc7d1
mužstvy	mužstvo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalitnější	kvalitní	k2eAgMnPc1d2
hokejisté	hokejista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
týmům	tým	k1gInPc3
z	z	k7c2
NHL	NHL	kA
nevejdou	vejít	k5eNaPmIp3nP
do	do	k7c2
základních	základní	k2eAgFnPc2d1
sestav	sestava	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
většinou	většina	k1gFnSc7
putují	putovat	k5eAaImIp3nP
do	do	k7c2
American	Americana	k1gFnPc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vítěz	vítěz	k1gMnSc1
soutěže	soutěž	k1gFnSc2
získává	získávat	k5eAaImIp3nS
trofej	trofej	k1gFnSc4
Kelly	Kella	k1gFnSc2
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Trofej	trofej	k1gInSc1
pro	pro	k7c4
vítězný	vítězný	k2eAgInSc4d1
tým	tým	k1gInSc4
</s>
<s>
Přehled	přehled	k1gInSc1
finále	finále	k1gNnSc2
soutěže	soutěž	k1gFnSc2
</s>
<s>
Sezona	sezona	k1gFnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Finálová	finálový	k2eAgFnSc1d1
série	série	k1gFnSc1
</s>
<s>
Poražený	poražený	k1gMnSc1
finalista	finalista	k1gMnSc1
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
<g/>
Carolina	Carolina	k1gFnSc1
Thunderbirds	Thunderbirdsa	k1gFnPc2
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
Johnstown	Johnstown	k1gInSc1
Chiefs	Chiefs	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
<g/>
Greensboro	Greensbora	k1gFnSc5
Monarchs	Monarchs	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Winston-Salem	Winston-Sal	k1gInSc7
Thunderbirds	Thunderbirds	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
<g/>
Hampton	Hampton	k1gInSc1
Roads	Roads	k1gInSc4
Admirals	Admirals	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Greensboro	Greensbora	k1gFnSc5
Monarchs	Monarchs	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
Hampton	Hampton	k1gInSc1
Roads	Roads	k1gInSc4
Admirals	Admirals	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
Louisville	Louisville	k1gInSc1
Icehawks	Icehawks	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
<g/>
Toledo	Toledo	k1gNnSc1
Storm	Storm	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
Wheeling	Wheeling	k1gInSc1
Thunderbirds	Thunderbirds	k1gInSc4
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
<g/>
Toledo	Toledo	k1gNnSc1
Storm	Storm	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Raleigh	Raleigh	k1gInSc1
Icecaps	Icecaps	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
Richmond	Richmond	k1gMnSc1
Renegades	Renegades	k1gMnSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Greensboro	Greensbora	k1gFnSc5
Monarchs	Monarchs	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
Charlotte	Charlott	k1gInSc5
Checkers	Checkers	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
Jacksonville	Jacksonville	k1gFnSc2
Lizard	Lizarda	k1gFnPc2
Kings	Kings	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
<g/>
South	Southa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Stingrays	Stingrays	k1gInSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Louisiana	Louisiana	k1gFnSc1
IceGators	IceGatorsa	k1gFnPc2
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
<g/>
Hampton	Hampton	k1gInSc1
Roads	Roads	k1gInSc4
Admirals	Admirals	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
Pensacola	Pensacola	k1gFnSc1
Ice	Ice	k1gFnSc1
Pilots	Pilots	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
<g/>
Mississippi	Mississippi	k1gNnPc2
Sea	Sea	k1gMnSc1
Wolves	Wolves	k1gMnSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
Richmond	Richmond	k1gInSc1
Renegades	Renegades	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
Peoria	Peorium	k1gNnSc2
Rivermen	Rivermen	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
Louisiana	Louisiana	k1gFnSc1
IceGators	IceGatorsa	k1gFnPc2
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
<g/>
South	Southa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Stingrays	Stingrays	k1gInSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Trenton	Trenton	k1gInSc1
Titans	Titans	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
<g/>
Greenville	Greenville	k1gNnPc2
Grrrowl	Grrrowla	k1gFnPc2
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
Dayton	Dayton	k1gInSc1
Bombers	Bombers	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
<g/>
Atlantic	Atlantice	k1gFnPc2
City	City	k1gFnSc2
Boardwalk	Boardwalk	k1gMnSc1
Bullies	Bullies	k1gMnSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Columbia	Columbia	k1gFnSc1
Inferno	inferno	k1gNnSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
<g/>
Idaho	Ida	k1gMnSc2
Steelheads	Steelheads	k1gInSc4
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Florida	Florida	k1gFnSc1
Everblades	Everbladesa	k1gFnPc2
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
Trenton	Trenton	k1gInSc1
Titans	Titans	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
Florida	Florida	k1gFnSc1
Everblades	Everbladesa	k1gFnPc2
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g/>
Alaska	Alask	k1gInSc2
Aces	Aces	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Gwinnett	Gwinnett	k2eAgInSc4d1
Gladiators	Gladiators	k1gInSc4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
<g/>
Idaho	Ida	k1gMnSc2
Steelheads	Steelheads	k1gInSc4
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Dayton	Dayton	k1gInSc1
Bombers	Bombers	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
<g/>
Cincinnati	Cincinnati	k1gMnSc1
Cyclones	Cyclones	k1gMnSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
Wranglers	Wranglersa	k1gFnPc2
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
South	Southa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Stingrays	Stingrays	k1gInSc1
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
Alaska	Alask	k1gInSc2
Aces	Aces	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
Cincinnati	Cincinnati	k1gMnSc1
Cyclones	Cyclones	k1gMnSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Idaho	Ida	k1gMnSc2
Steelheads	Steelheads	k1gInSc4
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
<g/>
Alaska	Alask	k1gInSc2
Aces	Aces	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Kalamazoo	Kalamazoo	k1gNnSc1
Wings	Wingsa	k1gFnPc2
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
Florida	Florida	k1gFnSc1
Everblades	Everbladesa	k1gFnPc2
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
Wranglers	Wranglersa	k1gFnPc2
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
Reading	Reading	k1gInSc1
Royals	Royals	k1gInSc1
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
Stockton	Stockton	k1gInSc1
Thunder	Thunder	k1gInSc4
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
Alaska	Alask	k1gInSc2
Aces	Aces	k1gInSc1
(	(	kIx(
<g/>
klub	klub	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
Cincinnati	Cincinnati	k1gMnSc2
Cyclones	Cyclones	k1gInSc4
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
Allen	Allen	k1gMnSc1
Americans	Americans	k1gInSc4
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
South	Southa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Stingrays	Stingrays	k1gInSc1
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
Allen	Allen	k1gMnSc1
Americans	Americans	k1gInSc4
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
Wheeling	Wheeling	k1gInSc1
Nailers	Nailers	k1gInSc4
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
Colorado	Colorado	k1gNnSc1
Eagles	Eaglesa	k1gFnPc2
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
South	Southa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Stingrays	Stingrays	k1gInSc1
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
Colorado	Colorado	k1gNnSc1
Eagles	Eaglesa	k1gFnPc2
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
Florida	Florida	k1gFnSc1
Everblades	Everbladesa	k1gFnPc2
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
Newfoundland	Newfoundlando	k1gNnPc2
Growlers	Growlersa	k1gFnPc2
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
Toledo	Toledo	k1gNnSc1
Walleye	Walleye	k1gFnPc2
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
<g/>
sezona	sezona	k1gFnSc1
nedohrána	dohrát	k5eNaPmNgFnS
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
koronaviru	koronavir	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ECHL	ECHL	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
web	web	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Seniorské	seniorský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
ledního	lední	k2eAgInSc2d1
hokeje	hokej	k1gInSc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
Nejvyšší	vysoký	k2eAgFnSc1d3
profesionální	profesionální	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
National	Nationat	k5eAaPmAgMnS,k5eAaImAgMnS
Hockey	Hocke	k2eAgInPc4d1
League	Leagu	k1gInPc4
Profesionální	profesionální	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
vysoké	vysoký	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
</s>
<s>
American	American	k1gInSc1
Hockey	Hockea	k1gFnSc2
League	League	k1gFnSc3
Profesionální	profesionální	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
střední	střední	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
</s>
<s>
ECHL	ECHL	kA
Profesionální	profesionální	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
nízké	nízký	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
</s>
<s>
Federal	Federat	k5eAaPmAgMnS,k5eAaImAgMnS
Hockey	Hockea	k1gFnPc4
League	Leagu	k1gInSc2
•	•	k?
Ligue	Liguus	k1gMnSc5
Nord-Américaine	Nord-Américain	k1gMnSc5
de	de	k?
Hockey	Hockey	k1gInPc1
•	•	k?
Southern	Southern	k1gInSc1
Professional	Professional	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gFnSc2
Zaniklé	zaniklý	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
</s>
<s>
All	All	k?
American	American	k1gInSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
All-American	All-American	k1gInSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
Amateur	Amateur	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Association	Association	k1gInSc1
of	of	k?
Canada	Canada	k1gFnSc1
•	•	k?
American	American	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Association	Association	k1gInSc1
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
American	American	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Association	Association	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlantic	Atlantice	k1gFnPc2
Coast	Coast	k1gFnSc1
Hockey	Hockea	k1gFnSc2
League	League	k1gNnSc2
•	•	k?
Atlantic	Atlantice	k1gFnPc2
Coast	Coast	k1gFnSc1
Hockey	Hockea	k1gFnSc2
League	League	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
California	Californium	k1gNnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
Canadian	Canadian	k1gMnSc1
Amateur	Amateur	k1gMnSc1
Hockey	Hockea	k1gFnSc2
League	League	k1gInSc1
•	•	k?
Canadian-American	Canadian-American	k1gInSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
Canadian	Canadian	k1gInSc1
Professional	Professional	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
Central	Central	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Central	Central	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Central	Central	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Hockey	Hocke	k2eAgInPc1d1
League	Leagu	k1gInPc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Eastern	Eastern	k1gInSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
Eastern	Eastern	k1gInSc1
Professional	Professional	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Eastern	Eastern	k1gInSc1
Professional	Professional	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
International	International	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
1929	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
•	•	k?
International	International	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
International	International	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maritime	Maritim	k1gInSc5
Major	major	k1gMnSc1
Hockey	Hockea	k1gMnSc2
League	League	k1gNnSc2
•	•	k?
Mid-Atlantic	Mid-Atlantice	k1gFnPc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
National	National	k1gMnSc2
Hockey	Hockea	k1gMnSc2
Association	Association	k1gInSc1
•	•	k?
North	North	k1gInSc1
American	American	k1gInSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
North	North	k1gInSc1
Eastern	Eastern	k1gInSc1
Hockey	Hocke	k2eAgInPc1d1
League	Leagu	k1gInPc1
•	•	k?
North	North	k1gInSc1
West	West	k1gInSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
Pacific	Pacific	k1gMnSc1
Coast	Coast	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Association	Association	k1gInSc1
•	•	k?
Pacific	Pacifice	k1gInPc2
Coast	Coast	k1gMnSc1
Hockey	Hockea	k1gMnSc2
League	League	k1gNnSc2
•	•	k?
Pacific	Pacific	k1gMnSc1
Hockey	Hocke	k2eAgInPc4d1
League	Leagu	k1gInPc4
•	•	k?
Prairie	Prairie	k1gFnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
Ligue	Ligu	k1gMnSc2
de	de	k?
hockey	hockea	k1gFnSc2
senior	senior	k1gMnSc1
du	du	k?
Québec	Québec	k1gMnSc1
•	•	k?
South	South	k1gMnSc1
East	East	k1gMnSc1
Hockey	Hockea	k1gMnSc2
League	League	k1gNnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Southern	Southern	k1gInSc1
Hockey	Hockea	k1gFnSc2
League	Leagu	k1gFnSc2
•	•	k?
Sunshine	Sunshin	k1gInSc5
Hockey	Hocke	k1gMnPc7
League	League	k1gNnSc1
•	•	k?
Tropical	Tropical	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
United	United	k1gInSc1
States	States	k1gInSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
•	•	k?
West	West	k2eAgInSc4d1
Coast	Coast	k1gInSc4
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gInSc2
•	•	k?
Western	western	k1gInSc1
Canada	Canada	k1gFnSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Western	western	k1gInSc1
Canada	Canada	k1gFnSc1
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Western	Western	kA
Hockey	Hockea	k1gFnSc2
League	Leagu	k1gFnSc2
•	•	k?
Western	Western	kA
Professional	Professional	k1gFnSc2
Hockey	Hockea	k1gFnSc2
League	Leagu	k1gFnSc2
•	•	k?
World	World	k1gMnSc1
Hockey	Hockea	k1gFnSc2
Association	Association	k1gInSc1
•	•	k?
World	World	k1gInSc4
Hockey	Hockea	k1gFnSc2
Association	Association	k1gInSc4
2	#num#	k4
</s>
<s>
ECHL	ECHL	kA
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Západní	západní	k2eAgFnPc1d1
konferenceVýchodní	konferenceVýchodní	k2eAgFnPc1d1
konference	konference	k1gFnPc1
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
Cincinnati	Cincinnat	k5eAaImF,k5eAaPmF
Cyclones	Cyclones	k1gInSc4
</s>
<s>
Fort	Fort	k?
Wayne	Wayn	k1gMnSc5
Komets	Komets	k1gInSc4
</s>
<s>
Indy	Indus	k1gInPc1
Fuel	Fuela	k1gFnPc2
</s>
<s>
Kalamazoo	Kalamazoo	k6eAd1
Wings	Wings	k1gInSc1
</s>
<s>
Toledo	Toledo	k1gNnSc1
Walleye	Walley	k1gInSc2
</s>
<s>
Wheeling	Wheeling	k1gInSc1
Nailers	Nailersa	k1gFnPc2
</s>
<s>
Horská	horský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
Allen	Allen	k1gMnSc1
Americans	Americansa	k1gFnPc2
</s>
<s>
Idaho	Idaze	k6eAd1
Steelheads	Steelheads	k1gInSc1
</s>
<s>
Kansas	Kansas	k1gInSc1
City	city	k1gNnSc1
Mavericks	Mavericksa	k1gFnPc2
</s>
<s>
Rapid	rapid	k1gInSc1
City	city	k1gNnSc1
Rush	Rusha	k1gFnPc2
</s>
<s>
Tulsa	Tulsa	k1gFnSc1
Oilers	Oilersa	k1gFnPc2
</s>
<s>
Utah	Utah	k1gInSc1
Grizzlies	Grizzliesa	k1gFnPc2
</s>
<s>
Wichita	Wichita	k1gMnSc1
Thunder	Thunder	k1gMnSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
Adirondack	Adirondack	k1gMnSc1
Thunder	Thunder	k1gMnSc1
</s>
<s>
Brampton	Brampton	k1gInSc1
Beast	Beast	k1gInSc1
</s>
<s>
Maine	Mainout	k5eAaImIp3nS,k5eAaPmIp3nS
Mariners	Mariners	k1gInSc1
</s>
<s>
Manchester	Manchester	k1gInSc1
Monarchs	Monarchsa	k1gFnPc2
</s>
<s>
Reading	Reading	k1gInSc1
Royals	Royalsa	k1gFnPc2
</s>
<s>
Newfoundland	Newfoundland	k1gInSc1
Growlers	Growlersa	k1gFnPc2
</s>
<s>
Worcester	Worcester	k1gInSc1
Railers	Railersa	k1gFnPc2
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
Atlanta	Atlanta	k1gFnSc1
Gladiators	Gladiatorsa	k1gFnPc2
</s>
<s>
Florida	Florida	k1gFnSc1
Everblades	Everbladesa	k1gFnPc2
</s>
<s>
Greenville	Greenville	k6eAd1
Swamp	Swamp	k1gInSc1
Rabbits	Rabbitsa	k1gFnPc2
</s>
<s>
Jacksonville	Jacksonville	k6eAd1
Icemen	Icemen	k2eAgInSc1d1
</s>
<s>
Norfolk	Norfolk	k6eAd1
Admirals	Admirals	k1gInSc1
</s>
<s>
Orlando	Orlando	k6eAd1
Solar	Solar	k1gInSc1
Bears	Bearsa	k1gFnPc2
</s>
<s>
South	Southa	k1gFnPc2
Carolina	Carolina	k1gFnSc1
Stingrays	Stingrays	k1gInSc1
</s>
<s>
Bývalé	bývalý	k2eAgInPc1d1
kluby	klub	k1gInPc1
ECHL	ECHL	kA
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Virginia	Virginium	k1gNnPc1
Lancers	Lancersa	k1gFnPc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carolina	Carolina	k1gFnSc1
Thunderbirds	Thunderbirdsa	k1gFnPc2
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Huntsville	Huntsville	k1gInSc1
Blast	Blast	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Louisville	Louisville	k1gInSc1
Icehawks	Icehawks	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Greensboro	Greensbora	k1gFnSc5
Monarchs	Monarchsa	k1gFnPc2
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Erie	Erie	k1gInSc1
Panthers	Panthers	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Knoxville	Knoxville	k1gInSc1
Cherokees	Cherokees	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Louisville	Louisville	k1gInSc1
RiverFrogs	RiverFrogs	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Raleigh	Raleigh	k1gInSc1
IceCaps	IceCaps	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Columbus	Columbus	k1gMnSc1
Chill	Chill	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chesapeake	Chesapeake	k1gInSc1
Icebreakers	Icebreakers	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Miami	Miami	k1gNnSc2
Matadors	Matadorsa	k1gFnPc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hampton	Hampton	k1gInSc1
Roads	Roads	k1gInSc1
Admirals	Admirals	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jacksonville	Jacksonville	k1gInSc1
Lizard	Lizard	k1gMnSc1
Kings	Kings	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Birmingham	Birmingham	k1gInSc1
Bulls	Bulls	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tallahassee	Tallahassee	k1gInSc1
Tiger	Tiger	k1gMnSc1
Sharks	Sharks	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Macon	Macon	k1gInSc1
Whoopee	Whoopee	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mobile	mobile	k1gNnSc2
Mysticks	Mysticksa	k1gFnPc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
New	New	k1gFnSc1
Orleans	Orleans	k1gInSc1
Brass	Brass	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arkansas	Arkansas	k1gInSc1
RiverBlades	RiverBlades	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baton	baton	k1gInSc1
Rouge	rouge	k1gFnSc1
Kingfish	Kingfish	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jackson	Jackson	k1gInSc1
Bandits	Bandits	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lexington	Lexington	k1gInSc1
Men	Men	k1gFnPc2
O	o	k7c6
<g/>
'	'	kIx"
War	War	k1gFnSc6
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Richmond	Richmond	k1gMnSc1
Renegades	Renegades	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Columbus	Columbus	k1gInSc1
Cottonmouths	Cottonmouths	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Greensboro	Greensbora	k1gFnSc5
Generals	Generalsa	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roanoke	Roanoke	k1gInSc1
Express	express	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Atlantic	Atlantice	k1gFnPc2
City	city	k1gNnSc1
Boardwalk	Boardwalk	k1gMnSc1
Bullies	Bullies	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Louisiana	Louisiana	k1gFnSc1
IceGators	IceGatorsa	k1gFnPc2
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pee	Pee	k1gMnSc5
Dee	Dee	k1gMnSc5
Pride	Prid	k1gMnSc5
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peoria	Peorium	k1gNnSc2
Rivermen	Rivermen	k1gInSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Greenville	Greenville	k1gInSc1
Grrrowl	Grrrowl	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
San	San	k1gFnSc1
Diego	Diego	k1gMnSc1
Gulls	Gulls	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Long	Long	k1gMnSc1
Beach	Beach	k1gMnSc1
Ice	Ice	k1gMnSc1
Dogs	Dogsa	k1gFnPc2
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Toledo	Toledo	k1gNnSc1
Storm	Storm	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Augusta	Augusta	k1gMnSc1
Lynx	Lynx	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Columbia	Columbia	k1gFnSc1
Inferno	inferno	k1gNnSc4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pensacola	Pensacola	k1gFnSc1
Ice	Ice	k1gFnSc2
Pilots	Pilotsa	k1gFnPc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Texas	Texas	k1gInSc1
Wildcatters	Wildcatters	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dayton	Dayton	k1gInSc1
Bombers	Bombers	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Fresno	Fresno	k6eAd1
Falcons	Falconsa	k1gFnPc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mississippi	Mississippi	k1gFnSc2
Sea	Sea	k1gMnSc1
Wolves	Wolves	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Phoenix	Phoenix	k1gInSc1
RoadRunners	RoadRunners	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charlotte	Charlott	k1gInSc5
Checkers	Checkersa	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Johnstown	Johnstown	k1gInSc1
Chiefs	Chiefs	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Victoria	Victorium	k1gNnSc2
Salmon	Salmona	k1gFnPc2
Kings	Kingsa	k1gFnPc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chicago	Chicago	k1gNnSc1
Express	express	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Trenton	Trenton	k1gInSc1
Titans	Titans	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
13	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Las	laso	k1gNnPc2
Vegas	Vegas	k1gInSc1
Wranglers	Wranglers	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
•	•	k?
San	San	k1gFnSc1
Francisco	Francisco	k1gMnSc1
Bulls	Bulls	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bakersfield	Bakersfield	k1gInSc1
Condors	Condors	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ontario	Ontario	k1gNnSc1
Reign	Reign	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stockton	Stockton	k1gInSc1
Thunder	Thunder	k1gInSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Evansville	Evansville	k1gInSc1
IceMen	IceMen	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alaska	Alask	k1gInSc2
Aces	Acesa	k1gFnPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Elmira	Elmir	k1gInSc2
Jackals	Jackalsa	k1gFnPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Colorado	Colorado	k1gNnSc1
Eagles	Eagles	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Quad	Quad	k1gInSc1
City	city	k1gNnSc1
Mallards	Mallards	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
</s>
