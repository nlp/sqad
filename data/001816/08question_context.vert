<s>
Duna	duna	k1gFnSc1	duna
je	být	k5eAaImIp3nS	být
výpravný	výpravný	k2eAgInSc4d1	výpravný
sci-fi	scii	k1gFnSc6	sci-fi
film	film	k1gInSc4	film
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Franka	Frank	k1gMnSc2	Frank
Herberta	Herbert	k1gMnSc2	Herbert
o	o	k7c6	o
pouštní	pouštní	k2eAgFnSc6d1	pouštní
planetě	planeta	k1gFnSc6	planeta
Arrakis	Arrakis	k1gFnSc2	Arrakis
<g/>
.	.	kIx.	.
</s>
