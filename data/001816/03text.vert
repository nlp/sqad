<s>
Duna	duna	k1gFnSc1	duna
je	být	k5eAaImIp3nS	být
výpravný	výpravný	k2eAgInSc4d1	výpravný
sci-fi	scii	k1gFnSc6	sci-fi
film	film	k1gInSc4	film
Davida	David	k1gMnSc2	David
Lynche	Lynch	k1gMnSc2	Lynch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Franka	Frank	k1gMnSc2	Frank
Herberta	Herbert	k1gMnSc2	Herbert
o	o	k7c6	o
pouštní	pouštní	k2eAgFnSc6d1	pouštní
planetě	planeta	k1gFnSc6	planeta
Arrakis	Arrakis	k1gFnSc2	Arrakis
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
hrála	hrát	k5eAaImAgFnS	hrát
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgInPc2d1	významný
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
patřil	patřit	k5eAaImAgInS	patřit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
k	k	k7c3	k
nejnákladnějším	nákladný	k2eAgNnPc3d3	nejnákladnější
filmovým	filmový	k2eAgNnPc3d1	filmové
dílům	dílo	k1gNnPc3	dílo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
komerčně	komerčně	k6eAd1	komerčně
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
stal	stát	k5eAaPmAgInS	stát
kultovní	kultovní	k2eAgFnSc7d1	kultovní
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
(	(	kIx(	(
<g/>
sestřihů	sestřih	k1gInPc2	sestřih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
10191	[number]	k4	10191
<g/>
.	.	kIx.	.
</s>
<s>
Lidmi	člověk	k1gMnPc7	člověk
obývaná	obývaný	k2eAgFnSc1d1	obývaná
část	část	k1gFnSc1	část
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
planety	planeta	k1gFnPc1	planeta
patří	patřit	k5eAaImIp3nP	patřit
různým	různý	k2eAgInPc3d1	různý
feudálním	feudální	k2eAgInPc3d1	feudální
rodům	rod	k1gInPc3	rod
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dopravu	doprava	k1gFnSc4	doprava
mezi	mezi	k7c7	mezi
planetami	planeta	k1gFnPc7	planeta
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
gilda	gilda	k1gFnSc1	gilda
kosmických	kosmický	k2eAgInPc2d1	kosmický
navigátorů	navigátor	k1gInPc2	navigátor
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
droze	droga	k1gFnSc6	droga
<g/>
,	,	kIx,	,
koření	kořenit	k5eAaImIp3nS	kořenit
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
Arrakis	Arrakis	k1gFnSc2	Arrakis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
svou	svůj	k3xOyFgFnSc4	svůj
velkou	velká	k1gFnSc4	velká
roli	role	k1gFnSc4	role
sehrává	sehrávat	k5eAaImIp3nS	sehrávat
ženský	ženský	k2eAgInSc1d1	ženský
náboženský	náboženský	k2eAgInSc1d1	náboženský
řád	řád	k1gInSc1	řád
Bene	bene	k6eAd1	bene
Gesserit	Gesserit	k1gInSc1	Gesserit
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
vyšlechtit	vyšlechtit	k5eAaPmF	vyšlechtit
nadčlověka	nadčlověk	k1gMnSc4	nadčlověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
umí	umět	k5eAaImIp3nS	umět
vidět	vidět	k5eAaImF	vidět
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
to	ten	k3xDgNnSc4	ten
dlouho	dlouho	k6eAd1	dlouho
sám	sám	k3xTgMnSc1	sám
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
vévody	vévoda	k1gMnSc2	vévoda
Atreida	Atreid	k1gMnSc2	Atreid
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
musí	muset	k5eAaImIp3nS	muset
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
k	k	k7c3	k
Fremenům	Fremen	k1gInPc3	Fremen
<g/>
,	,	kIx,	,
schopných	schopný	k2eAgFnPc2d1	schopná
ovládat	ovládat	k5eAaImF	ovládat
nebezpečné	bezpečný	k2eNgMnPc4d1	nebezpečný
obří	obří	k2eAgMnPc4d1	obří
červy	červ	k1gMnPc4	červ
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
vrahy	vrah	k1gMnPc4	vrah
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
i	i	k9	i
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
v	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
válce	válka	k1gFnSc6	válka
porazí	porazit	k5eAaPmIp3nS	porazit
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	s	k7c7	s
vládcem	vládce	k1gMnSc7	vládce
planety	planeta	k1gFnSc2	planeta
i	i	k8xC	i
novým	nový	k2eAgMnSc7d1	nový
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
podle	podle	k7c2	podle
uvedení	uvedení	k1gNnSc2	uvedení
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
<g/>
)	)	kIx)	)
Lady	lady	k1gFnPc6	lady
Jessica	Jessic	k1gInSc2	Jessic
:	:	kIx,	:
Francesca	Francesca	k1gMnSc1	Francesca
Annis	Annis	k1gFnSc2	Annis
Lékař	lékař	k1gMnSc1	lékař
Vladimira	Vladimira	k1gMnSc1	Vladimira
Harkonena	Harkonena	k1gFnSc1	Harkonena
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
Cimino	Cimino	k1gNnSc1	Cimino
Piter	Piter	k1gMnSc1	Piter
De	De	k?	De
Vries	Vries	k1gMnSc1	Vries
:	:	kIx,	:
Brad	brada	k1gFnPc2	brada
Dourif	Dourif	k1gInSc1	Dourif
<g />
.	.	kIx.	.
</s>
<s>
Padišáh	Padišáh	k1gMnSc1	Padišáh
Imperátor	imperátor	k1gMnSc1	imperátor
Shaddam	Shaddam	k1gInSc4	Shaddam
IV	IV	kA	IV
:	:	kIx,	:
José	Josá	k1gFnSc2	Josá
Ferrer	Ferrer	k1gMnSc1	Ferrer
Šedout	Šedout	k1gMnSc1	Šedout
Mapes	Mapes	k1gMnSc1	Mapes
(	(	kIx(	(
<g/>
fremenka	fremenka	k1gFnSc1	fremenka
<g/>
)	)	kIx)	)
:	:	kIx,	:
Linda	Linda	k1gFnSc1	Linda
Hunt	hunt	k1gInSc1	hunt
Thufir	Thufir	k1gMnSc1	Thufir
Hawat	Hawat	k1gInSc1	Hawat
:	:	kIx,	:
Freddie	Freddie	k1gFnSc1	Freddie
Jones	Jones	k1gMnSc1	Jones
Duncan	Duncan	k1gMnSc1	Duncan
Idaho	Ida	k1gMnSc2	Ida
:	:	kIx,	:
Richard	Richard	k1gMnSc1	Richard
Jordan	Jordan	k1gMnSc1	Jordan
Paul	Paul	k1gMnSc1	Paul
Atreides	Atreides	k1gMnSc1	Atreides
:	:	kIx,	:
Kyle	Kyle	k1gFnSc1	Kyle
MacLachlan	MacLachlany	k1gInPc2	MacLachlany
Princezna	princezna	k1gFnSc1	princezna
Irulán	Irulán	k1gInSc1	Irulán
:	:	kIx,	:
Virginia	Virginium	k1gNnSc2	Virginium
Madsen	Madsno	k1gNnPc2	Madsno
Ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
matka	matka	k1gFnSc1	matka
Ramallo	Ramallo	k1gNnSc4	Ramallo
:	:	kIx,	:
Silvana	Silvana	k1gFnSc1	Silvana
Mangano	Mangana	k1gFnSc5	Mangana
Stilgar	Stilgar	k1gInSc1	Stilgar
(	(	kIx(	(
<g/>
frementský	frementský	k2eAgInSc1d1	frementský
náčelník	náčelník	k1gInSc1	náčelník
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Everett	Everett	k2eAgMnSc1d1	Everett
McGill	McGill	k1gMnSc1	McGill
Baron	baron	k1gMnSc1	baron
Vladimir	Vladimir	k1gMnSc1	Vladimir
Harkonnen	Harkonnen	k2eAgMnSc1d1	Harkonnen
:	:	kIx,	:
Kenneth	Kenneth	k1gMnSc1	Kenneth
McMillan	McMillan	k1gMnSc1	McMillan
Kapitán	kapitán	k1gMnSc1	kapitán
Iakin	Iakin	k1gMnSc1	Iakin
Nefud	Nefud	k1gMnSc1	Nefud
:	:	kIx,	:
Jack	Jack	k1gMnSc1	Jack
Nance	Nanka	k1gFnSc6	Nanka
Ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
matka	matka	k1gFnSc1	matka
Gaius	Gaius	k1gInSc1	Gaius
Helena	Helena	k1gFnSc1	Helena
Mohiamová	Mohiamový	k2eAgFnSc1d1	Mohiamový
:	:	kIx,	:
Siân	Siâno	k1gNnPc2	Siâno
Phillips	Phillipsa	k1gFnPc2	Phillipsa
Sestra	sestra	k1gFnSc1	sestra
Bene	bene	k6eAd1	bene
Gesseritu	Gesserit	k1gInSc2	Gesserit
:	:	kIx,	:
Angélica	Angélica	k1gMnSc1	Angélica
Aragón	Aragón	k1gMnSc1	Aragón
Vévoda	vévoda	k1gMnSc1	vévoda
Leto	Leto	k1gMnSc1	Leto
Atreides	Atreides	k1gMnSc1	Atreides
:	:	kIx,	:
Jürgen	Jürgen	k1gInSc1	Jürgen
Prochnow	Prochnow	k1gFnSc2	Prochnow
Bestie	bestie	k1gFnSc2	bestie
Rabban	Rabban	k1gMnSc1	Rabban
Paul	Paul	k1gMnSc1	Paul
L.	L.	kA	L.
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
jako	jako	k8xC	jako
Paul	Paul	k1gMnSc1	Paul
Smith	Smith	k1gMnSc1	Smith
<g/>
)	)	kIx)	)
Gurney	Gurnea	k1gMnSc2	Gurnea
<g />
.	.	kIx.	.
</s>
<s>
Halleck	Halleck	k1gMnSc1	Halleck
:	:	kIx,	:
Patrick	Patrick	k1gMnSc1	Patrick
Stewart	Stewart	k1gMnSc1	Stewart
Feyd-Rautha	Feyd-Rautha	k1gMnSc1	Feyd-Rautha
(	(	kIx(	(
<g/>
synovec	synovec	k1gMnSc1	synovec
Vladimira	Vladimir	k1gMnSc2	Vladimir
Harkonena	Harkoneno	k1gNnSc2	Harkoneno
<g/>
)	)	kIx)	)
:	:	kIx,	:
Sting	Sting	k1gMnSc1	Sting
Doktor	doktor	k1gMnSc1	doktor
Wellington	Wellington	k1gInSc4	Wellington
Yueh	Yueh	k1gMnSc1	Yueh
:	:	kIx,	:
Dean	Dean	k1gMnSc1	Dean
Stockwell	Stockwell	k1gMnSc1	Stockwell
Doktor	doktor	k1gMnSc1	doktor
Kynes	Kynes	k1gMnSc1	Kynes
:	:	kIx,	:
Max	max	kA	max
von	von	k1gInSc1	von
Sydow	Sydow	k1gFnSc1	Sydow
Alia	Alia	k1gMnSc1	Alia
:	:	kIx,	:
Alicia	Alicia	k1gFnSc1	Alicia
Witt	Witt	k1gMnSc1	Witt
(	(	kIx(	(
<g/>
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
jako	jako	k8xC	jako
Alicia	Alicium	k1gNnSc2	Alicium
Roanne	Roann	k1gInSc5	Roann
Witt	Witt	k2eAgMnSc1d1	Witt
<g/>
)	)	kIx)	)
Chani	Chan	k1gMnPc1	Chan
(	(	kIx(	(
<g/>
Paulova	Paulův	k2eAgFnSc1d1	Paulova
fremenská	fremenský	k2eAgFnSc1d1	fremenský
konkubína	konkubína	k1gFnSc1	konkubína
<g/>
)	)	kIx)	)
:	:	kIx,	:
Sean	Sean	k1gInSc1	Sean
Young	Young	k1gMnSc1	Young
Otheym	Otheym	k1gInSc1	Otheym
(	(	kIx(	(
<g/>
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
jako	jako	k9	jako
Honorato	Honorat	k2eAgNnSc4d1	Honorat
Magalone	Magalon	k1gInSc5	Magalon
<g/>
)	)	kIx)	)
:	:	kIx,	:
Honorato	Honorat	k2eAgNnSc1d1	Honorat
Magaloni	Magaloň	k1gFnSc3	Magaloň
Jamis	Jamis	k1gInSc1	Jamis
:	:	kIx,	:
Judd	Judd	k1gInSc1	Judd
Omen	omen	k1gNnSc1	omen
Harah	Harah	k1gInSc1	Harah
(	(	kIx(	(
<g/>
Jamisova	Jamisův	k2eAgFnSc1d1	Jamisův
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
:	:	kIx,	:
Molly	Molla	k1gMnSc2	Molla
Wryn	Wryn	k1gNnSc4	Wryn
Příprava	příprava	k1gFnSc1	příprava
k	k	k7c3	k
natáčení	natáčení	k1gNnSc3	natáčení
byla	být	k5eAaImAgFnS	být
zdlouhavá	zdlouhavý	k2eAgFnSc1d1	zdlouhavá
a	a	k8xC	a
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Točit	točit	k5eAaImF	točit
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
až	až	k9	až
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
sedmé	sedmý	k4xOgFnSc2	sedmý
verze	verze	k1gFnSc2	verze
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výrobě	výroba	k1gFnSc6	výroba
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
600	[number]	k4	600
techniků	technik	k1gMnPc2	technik
<g/>
,	,	kIx,	,
15000	[number]	k4	15000
statistů	statista	k1gMnPc2	statista
a	a	k8xC	a
proto	proto	k8xC	proto
také	také	k9	také
náklady	náklad	k1gInPc1	náklad
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
mnohým	mnohý	k2eAgMnPc3d1	mnohý
příznivcům	příznivec	k1gMnPc3	příznivec
známého	známý	k2eAgInSc2d1	známý
románu	román	k1gInSc2	román
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
film	film	k1gInSc1	film
navštěvován	navštěvován	k2eAgInSc1d1	navštěvován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgNnP	být
natočena	natočen	k2eAgNnPc1d1	natočeno
další	další	k2eAgNnPc1d1	další
pokračování	pokračování	k1gNnPc1	pokračování
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
jich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc1	pět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
také	také	k9	také
podílel	podílet	k5eAaImAgMnS	podílet
velice	velice	k6eAd1	velice
známý	známý	k2eAgMnSc1d1	známý
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
umělec	umělec	k1gMnSc1	umělec
H.	H.	kA	H.
<g/>
R.	R.	kA	R.
Giger	Giger	k1gMnSc1	Giger
(	(	kIx(	(
<g/>
zakladatel	zakladatel	k1gMnSc1	zakladatel
biomechanického	biomechanický	k2eAgInSc2d1	biomechanický
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
)	)	kIx)	)
jenž	jenž	k3xRgMnSc1	jenž
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
s	s	k7c7	s
nákresy	nákres	k1gInPc7	nákres
,	,	kIx,	,
desingnem	desingno	k1gNnSc7	desingno
a	a	k8xC	a
nábytkem	nábytek	k1gInSc7	nábytek
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
</s>
