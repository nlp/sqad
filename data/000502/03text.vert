<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
pátá	pátá	k1gFnSc1	pátá
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xS	jako
dvojsystém	dvojsystý	k2eAgInSc6d1	dvojsystý
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Jupiteru	Jupiter	k1gInSc2	Jupiter
jako	jako	k8xC	jako
hlavních	hlavní	k2eAgInPc2d1	hlavní
dvou	dva	k4xCgMnPc2	dva
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
menších	malý	k2eAgNnPc2d2	menší
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
či	či	k8xC	či
planety	planeta	k1gFnPc1	planeta
jupiterského	jupiterský	k2eAgInSc2d1	jupiterský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
přibližně	přibližně	k6eAd1	přibližně
jedné	jeden	k4xCgFnSc2	jeden
tisíciny	tisícina	k1gFnSc2	tisícina
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
Jovovi	Jova	k1gMnSc6	Jova
(	(	kIx(	(
<g/>
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
pádě	pád	k1gInSc6	pád
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
znázornění	znázornění	k1gNnSc4	znázornění
božského	božský	k2eAgInSc2d1	božský
blesku	blesk	k1gInSc2	blesk
(	(	kIx(	(
<g/>
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
<g/>
:	:	kIx,	:
♃	♃	k?	♃
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gInSc1	Jupiter
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
již	již	k6eAd1	již
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
<g/>
,	,	kIx,	,
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
Jupiter	Jupiter	k1gMnSc1	Jupiter
magnitudu	magnitud	k1gInSc2	magnitud
-2,8	-2,8	k4	-2,8
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
činí	činit	k5eAaImIp3nS	činit
třetí	třetí	k4xOgInSc1	třetí
nejjasnější	jasný	k2eAgInSc1d3	nejjasnější
objekt	objekt	k1gInSc1	objekt
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
po	po	k7c6	po
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
Venuši	Venuše	k1gFnSc6	Venuše
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
před	před	k7c4	před
Jupiter	Jupiter	k1gInSc4	Jupiter
v	v	k7c6	v
jasnosti	jasnost	k1gFnSc6	jasnost
dostane	dostat	k5eAaPmIp3nS	dostat
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ideální	ideální	k2eAgFnSc6d1	ideální
pozici	pozice	k1gFnSc6	pozice
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
oběhu	oběh	k1gInSc2	oběh
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
slabé	slabý	k2eAgInPc1d1	slabý
prstence	prstenec	k1gInPc1	prstenec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
špatně	špatně	k6eAd1	špatně
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ji	on	k3xPp3gFnSc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
silné	silný	k2eAgNnSc1d1	silné
radiační	radiační	k2eAgNnSc1d1	radiační
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
jsou	být	k5eAaImIp3nP	být
viditelné	viditelný	k2eAgFnPc1d1	viditelná
horní	horní	k2eAgFnPc1d1	horní
vrstvy	vrstva	k1gFnPc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
rozčleněny	rozčleněn	k2eAgInPc4d1	rozčleněn
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
planetární	planetární	k2eAgFnSc6d1	planetární
šířce	šířka	k1gFnSc6	šířka
do	do	k7c2	do
různě	různě	k6eAd1	různě
barevných	barevný	k2eAgInPc2d1	barevný
pruhů	pruh	k1gInPc2	pruh
a	a	k8xC	a
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
atmosférickými	atmosférický	k2eAgFnPc7d1	atmosférická
bouřemi	bouř	k1gFnPc7	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
takovouto	takovýto	k3xDgFnSc7	takovýto
bouří	bouř	k1gFnSc7	bouř
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgNnPc4d1	známé
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnSc2	jaký
vrstvy	vrstva	k1gFnSc2	vrstva
planetu	planeta	k1gFnSc4	planeta
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
současné	současný	k2eAgInPc4d1	současný
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
její	její	k3xOp3gInSc4	její
průzkum	průzkum	k1gInSc4	průzkum
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiter	Jupiter	k1gInSc1	Jupiter
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
kamenné	kamenný	k2eAgNnSc1d1	kamenné
jádro	jádro	k1gNnSc1	jádro
tvořené	tvořený	k2eAgNnSc1d1	tvořené
těžšími	těžký	k2eAgInPc7d2	těžší
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gInSc1	Jupiter
byl	být	k5eAaImAgInS	být
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
několika	několik	k4yIc7	několik
automatickými	automatický	k2eAgFnPc7d1	automatická
sondami	sonda	k1gFnPc7	sonda
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
programu	program	k1gInSc2	program
Pioneer	Pioneer	kA	Pioneer
a	a	k8xC	a
programu	program	k1gInSc3	program
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
sondy	sonda	k1gFnPc1	sonda
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
proletěly	proletět	k5eAaPmAgInP	proletět
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc2	Jupiter
zamířila	zamířit	k5eAaPmAgFnS	zamířit
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
po	po	k7c4	po
necelých	celý	k2eNgNnPc2d1	necelé
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
obíhala	obíhat	k5eAaImAgFnS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgNnPc1d3	nejnovější
data	datum	k1gNnPc1	datum
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2007	[number]	k4	2007
použila	použít	k5eAaPmAgFnS	použít
planetu	planeta	k1gFnSc4	planeta
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
plánují	plánovat	k5eAaImIp3nP	plánovat
další	další	k2eAgFnPc4d1	další
mise	mise	k1gFnPc4	mise
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
převážně	převážně	k6eAd1	převážně
hypotetické	hypotetický	k2eAgInPc1d1	hypotetický
oceány	oceán	k1gInPc1	oceán
pod	pod	k7c7	pod
ledovou	ledový	k2eAgFnSc7d1	ledová
kůrou	kůra	k1gFnSc7	kůra
jeho	on	k3xPp3gInSc2	on
měsíce	měsíc	k1gInSc2	měsíc
Europy	Europa	k1gFnSc2	Europa
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
nejméně	málo	k6eAd3	málo
79	[number]	k4	79
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
čtyři	čtyři	k4xCgMnPc4	čtyři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc2	Galilei
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
velké	velký	k2eAgInPc4d1	velký
měsíce	měsíc	k1gInPc4	měsíc
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
Europu	Europ	k1gInSc2	Europ
<g/>
,	,	kIx,	,
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xC	jako
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
nebeského	nebeský	k2eAgInSc2d1	nebeský
pohybu	pohyb	k1gInSc2	pohyb
bylo	být	k5eAaImAgNnS	být
zřetelné	zřetelný	k2eAgNnSc1d1	zřetelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc7	jeho
centrem	centrum	k1gNnSc7	centrum
není	být	k5eNaImIp3nS	být
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
bodem	bod	k1gInSc7	bod
obhajoby	obhajoba	k1gFnSc2	obhajoba
Koperníkovy	Koperníkův	k2eAgFnSc2d1	Koperníkova
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
planet	planeta	k1gFnPc2	planeta
<g/>
;	;	kIx,	;
Galileiho	Galilei	k1gMnSc2	Galilei
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
podpory	podpora	k1gFnPc4	podpora
Koperníkově	Koperníkův	k2eAgFnSc3d1	Koperníkova
teorii	teorie	k1gFnSc3	teorie
jej	on	k3xPp3gMnSc4	on
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
inkvizicí	inkvizice	k1gFnSc7	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gInSc1	Jupiter
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
před	před	k7c7	před
4,6	[number]	k4	4,6
až	až	k9	až
4,7	[number]	k4	4,7
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohly	moct	k5eAaImAgFnP	moct
velké	velký	k2eAgFnPc4d1	velká
plynné	plynný	k2eAgFnPc4d1	plynná
planety	planeta	k1gFnPc4	planeta
vzniknout	vzniknout	k5eAaPmF	vzniknout
a	a	k8xC	a
zformovat	zformovat	k5eAaPmF	zformovat
se	se	k3xPyFc4	se
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
teorii	teorie	k1gFnSc4	teorie
akrece	akrece	k1gFnSc2	akrece
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
akrece	akrece	k1gFnSc2	akrece
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
postupně	postupně	k6eAd1	postupně
slepovaly	slepovat	k5eAaImAgFnP	slepovat
drobné	drobný	k2eAgFnPc1d1	drobná
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
větší	veliký	k2eAgFnPc1d2	veliký
částice	částice	k1gFnPc1	částice
až	až	k9	až
posléze	posléze	k6eAd1	posléze
balvany	balvan	k1gInPc4	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgFnPc4d1	neustálá
srážky	srážka	k1gFnPc4	srážka
těles	těleso	k1gNnPc2	těleso
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
narůstání	narůstání	k1gNnSc1	narůstání
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
tělesa	těleso	k1gNnPc1	těleso
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
velká	velký	k2eAgNnPc1d1	velké
železokamenitá	železokamenitý	k2eAgNnPc1d1	železokamenitý
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zárodky	zárodek	k1gInPc7	zárodek
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobná	podobný	k2eAgNnPc1d1	podobné
tělesa	těleso	k1gNnPc1	těleso
mohla	moct	k5eAaImAgNnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
ve	v	k7c6	v
vzdálenějších	vzdálený	k2eAgFnPc6d2	vzdálenější
oblastech	oblast	k1gFnPc6	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlivem	vliv	k1gInSc7	vliv
velké	velký	k2eAgFnSc2d1	velká
gravitace	gravitace	k1gFnSc2	gravitace
začala	začít	k5eAaPmAgFnS	začít
strhávat	strhávat	k5eAaImF	strhávat
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgInS	začít
nabalovat	nabalovat	k5eAaImF	nabalovat
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
planeta	planeta	k1gFnSc1	planeta
dorostla	dorůst	k5eAaPmAgFnS	dorůst
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
na	na	k7c6	na
"	"	kIx"	"
<g/>
povrchu	povrch	k1gInSc6	povrch
<g/>
"	"	kIx"	"
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
59,54	[number]	k4	59,54
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
daleko	daleko	k6eAd1	daleko
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
rychlost	rychlost	k1gFnSc4	rychlost
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
nejspíše	nejspíše	k9	nejspíše
původní	původní	k2eAgNnPc4d1	původní
složení	složení	k1gNnPc4	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nabalil	nabalit	k5eAaPmAgMnS	nabalit
už	už	k6eAd1	už
během	během	k7c2	během
vzniku	vznik	k1gInSc2	vznik
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgFnPc1d1	velká
planety	planeta	k1gFnPc1	planeta
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
postupným	postupný	k2eAgNnSc7d1	postupné
slepováním	slepování	k1gNnSc7	slepování
drobných	drobný	k2eAgFnPc2d1	drobná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
rychlým	rychlý	k2eAgNnSc7d1	rychlé
smrštěním	smrštění	k1gNnSc7	smrštění
z	z	k7c2	z
nahuštěného	nahuštěný	k2eAgInSc2d1	nahuštěný
shluku	shluk	k1gInSc2	shluk
v	v	k7c6	v
zárodečném	zárodečný	k2eAgNnSc6d1	zárodečné
disku	disco	k1gNnSc6	disco
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
několika	několik	k4yIc2	několik
gravitačních	gravitační	k2eAgInPc2d1	gravitační
kolapsů	kolaps	k1gInPc2	kolaps
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Alan	Alan	k1gMnSc1	Alan
Boss	boss	k1gMnSc1	boss
z	z	k7c2	z
Carnegie	Carnegie	k1gFnSc2	Carnegie
Institution	Institution	k1gInSc1	Institution
of	of	k?	of
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Jupiteru	Jupiter	k1gInSc2	Jupiter
trval	trvat	k5eAaImAgInS	trvat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
velkých	velký	k2eAgInPc2d1	velký
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
vznikaly	vznikat	k5eAaImAgFnP	vznikat
kamenné	kamenný	k2eAgFnPc4d1	kamenná
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
lehce	lehko	k6eAd1	lehko
tavitelných	tavitelný	k2eAgFnPc2d1	tavitelná
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
disku	disk	k1gInSc2	disk
okolo	okolo	k7c2	okolo
vznikající	vznikající	k2eAgFnSc2d1	vznikající
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgFnSc1d1	svrchní
atmosféra	atmosféra	k1gFnSc1	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
z	z	k7c2	z
88	[number]	k4	88
až	až	k9	až
92	[number]	k4	92
%	%	kIx~	%
vodíkem	vodík	k1gInSc7	vodík
a	a	k8xC	a
zbylých	zbylý	k2eAgInPc2d1	zbylý
8	[number]	k4	8
až	až	k9	až
12	[number]	k4	12
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměru	poměr	k1gInSc3	poměr
plynných	plynný	k2eAgFnPc2d1	plynná
molekul	molekula	k1gFnPc2	molekula
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
molekula	molekula	k1gFnSc1	molekula
hélia	hélium	k1gNnSc2	hélium
přibližně	přibližně	k6eAd1	přibližně
dvakrát	dvakrát	k6eAd1	dvakrát
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
molekula	molekula	k1gFnSc1	molekula
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc1	složení
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
v	v	k7c6	v
hmotnostních	hmotnostní	k2eAgInPc6d1	hmotnostní
procentech	procent	k1gInPc6	procent
na	na	k7c4	na
poměr	poměr	k1gInSc4	poměr
75	[number]	k4	75
%	%	kIx~	%
připadajících	připadající	k2eAgInPc2d1	připadající
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
24	[number]	k4	24
%	%	kIx~	%
na	na	k7c4	na
hélium	hélium	k1gNnSc4	hélium
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zbývající	zbývající	k2eAgNnSc1d1	zbývající
procento	procento	k1gNnSc1	procento
připadne	připadnout	k5eAaPmIp3nS	připadnout
na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
prvky	prvek	k1gInPc4	prvek
obsažené	obsažený	k2eAgInPc4d1	obsažený
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
složení	složení	k1gNnSc1	složení
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
odlišné	odlišný	k2eAgNnSc1d1	odlišné
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
obsahu	obsah	k1gInSc2	obsah
ostatních	ostatní	k2eAgInPc2d1	ostatní
prvků	prvek	k1gInPc2	prvek
vůči	vůči	k7c3	vůči
zastoupení	zastoupení	k1gNnSc3	zastoupení
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
vrstvách	vrstva	k1gFnPc6	vrstva
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
71	[number]	k4	71
hmotnostních	hmotnostní	k2eAgFnPc2d1	hmotnostní
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
24	[number]	k4	24
hmotnostních	hmotnostní	k2eAgFnPc2d1	hmotnostní
%	%	kIx~	%
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
5	[number]	k4	5
hmotnostních	hmotnostní	k2eAgInPc2d1	hmotnostní
%	%	kIx~	%
ostatních	ostatní	k2eAgInPc2d1	ostatní
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stopová	stopový	k2eAgNnPc4d1	stopové
množství	množství	k1gNnPc4	množství
methanu	methan	k1gInSc2	methan
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
čpavku	čpavek	k1gInSc2	čpavek
a	a	k8xC	a
křemičitanů	křemičitan	k1gInPc2	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
těchto	tento	k3xDgFnPc2	tento
hojnějších	hojný	k2eAgFnPc2d2	hojnější
sloučenin	sloučenina	k1gFnPc2	sloučenina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
atmosféra	atmosféra	k1gFnSc1	atmosféra
taktéž	taktéž	k?	taktéž
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
ethanu	ethan	k1gInSc2	ethan
<g/>
,	,	kIx,	,
sulfanu	sulfan	k1gInSc2	sulfan
<g/>
,	,	kIx,	,
neonu	neon	k1gInSc2	neon
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzdálenější	vzdálený	k2eAgFnSc1d3	nejvzdálenější
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ledové	ledový	k2eAgFnSc2d1	ledová
krystalky	krystalka	k1gFnSc2	krystalka
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
čpavku	čpavek	k1gInSc2	čpavek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
měření	měření	k1gNnPc2	měření
provedených	provedený	k2eAgNnPc2d1	provedené
v	v	k7c6	v
infračerveném	infračervený	k2eAgMnSc6d1	infračervený
a	a	k8xC	a
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
světle	světlo	k1gNnSc6	světlo
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
benzenu	benzen	k1gInSc2	benzen
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
vodíkem	vodík	k1gInSc7	vodík
a	a	k8xC	a
héliem	hélium	k1gNnSc7	hélium
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
blízko	blízko	k6eAd1	blízko
teoretickému	teoretický	k2eAgNnSc3d1	teoretické
složení	složení	k1gNnSc3	složení
původní	původní	k2eAgFnSc2d1	původní
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
celá	celý	k2eAgFnSc1d1	celá
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
neon	neon	k1gInSc4	neon
obsažený	obsažený	k2eAgInSc4d1	obsažený
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
pouze	pouze	k6eAd1	pouze
poměrem	poměr	k1gInSc7	poměr
20	[number]	k4	20
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
milión	milión	k4xCgInSc4	milión
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
desetiny	desetina	k1gFnSc2	desetina
průměrné	průměrný	k2eAgFnSc2d1	průměrná
hodnoty	hodnota	k1gFnSc2	hodnota
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
hélia	hélium	k1gNnSc2	hélium
je	být	k5eAaImIp3nS	být
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
80	[number]	k4	80
%	%	kIx~	%
zastoupení	zastoupení	k1gNnSc2	zastoupení
oproti	oproti	k7c3	oproti
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
podíl	podíl	k1gInSc1	podíl
hélia	hélium	k1gNnSc2	hélium
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výsledkem	výsledek	k1gInSc7	výsledek
srážkové	srážkový	k2eAgFnSc2d1	srážková
činnosti	činnost	k1gFnSc2	činnost
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
oblastí	oblast	k1gFnPc2	oblast
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
zastoupení	zastoupení	k1gNnSc1	zastoupení
těžších	těžký	k2eAgInPc2d2	těžší
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
třikrát	třikrát	k6eAd1	třikrát
hojnější	hojný	k2eAgFnPc1d2	hojnější
než	než	k8xS	než
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
spektroskopická	spektroskopický	k2eAgNnPc1d1	spektroskopické
měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gInSc1	Saturn
je	být	k5eAaImIp3nS	být
složením	složení	k1gNnSc7	složení
nejspíše	nejspíše	k9	nejspíše
podobný	podobný	k2eAgInSc1d1	podobný
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
další	další	k2eAgMnPc1d1	další
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
jako	jako	k8xS	jako
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
detailnější	detailní	k2eAgNnPc1d2	detailnější
data	datum	k1gNnPc1	datum
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
zastoupení	zastoupení	k1gNnSc2	zastoupení
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
u	u	k7c2	u
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
vyjma	vyjma	k7c2	vyjma
Jupiteru	Jupiter	k1gInSc2	Jupiter
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gFnSc2	jejich
atmosféry	atmosféra	k1gFnSc2	atmosféra
zatím	zatím	k6eAd1	zatím
nebyly	být	k5eNaImAgInP	být
prozkoumány	prozkoumat	k5eAaPmNgInP	prozkoumat
žádnými	žádný	k3yNgFnPc7	žádný
atmosférickými	atmosférický	k2eAgFnPc7d1	atmosférická
sondami	sonda	k1gFnPc7	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
2,5	[number]	k4	2,5
<g/>
×	×	k?	×
hmotnější	hmotný	k2eAgInSc1d2	hmotnější
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
výrazně	výrazně	k6eAd1	výrazně
těžiště	těžiště	k1gNnSc1	těžiště
(	(	kIx(	(
<g/>
barycentrum	barycentrum	k1gNnSc1	barycentrum
<g/>
)	)	kIx)	)
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Odchylka	odchylka	k1gFnSc1	odchylka
způsobená	způsobený	k2eAgFnSc1d1	způsobená
Jupiterem	Jupiter	k1gInSc7	Jupiter
je	být	k5eAaImIp3nS	být
742	[number]	k4	742
792	[number]	k4	792
km	km	kA	km
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
těžiště	těžiště	k1gNnSc1	těžiště
nacházelo	nacházet	k5eAaImAgNnS	nacházet
mimo	mimo	k7c4	mimo
těleso	těleso	k1gNnSc4	těleso
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
působení	působení	k1gNnSc3	působení
ostatních	ostatní	k2eAgNnPc2d1	ostatní
těles	těleso	k1gNnPc2	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
ostatních	ostatní	k2eAgMnPc2d1	ostatní
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
těžiště	těžiště	k1gNnSc1	těžiště
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
36	[number]	k4	36
<g/>
%	%	kIx~	%
času	čas	k1gInSc2	čas
uvnitř	uvnitř	k7c2	uvnitř
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Slunce	slunce	k1gNnSc2	slunce
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
těžiště	těžiště	k1gNnSc2	těžiště
je	být	k5eAaImIp3nS	být
0,00228	[number]	k4	0,00228
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
317,81	[number]	k4	317,81
<g/>
×	×	k?	×
hmotnější	hmotný	k2eAgFnSc2d2	hmotnější
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
poloměr	poloměr	k1gInSc1	poloměr
má	mít	k5eAaImIp3nS	mít
11,21	[number]	k4	11,21
<g/>
×	×	k?	×
větší	veliký	k2eAgInSc1d2	veliký
a	a	k8xC	a
objem	objem	k1gInSc1	objem
1321	[number]	k4	1321
<g/>
×	×	k?	×
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
nepovedenou	povedený	k2eNgFnSc4d1	nepovedená
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
toto	tento	k3xDgNnSc1	tento
srovnání	srovnání	k1gNnSc1	srovnání
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nalezené	nalezený	k2eAgFnPc1d1	nalezená
extrasolární	extrasolární	k2eAgFnPc1d1	extrasolární
planety	planeta	k1gFnPc1	planeta
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
hmotnější	hmotný	k2eAgNnPc1d2	hmotnější
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
výběrovým	výběrový	k2eAgInSc7d1	výběrový
efektem	efekt	k1gInSc7	efekt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hmotnější	hmotný	k2eAgMnPc1d2	hmotnější
průvodci	průvodce	k1gMnPc1	průvodce
jiných	jiný	k2eAgFnPc2d1	jiná
hvězd	hvězda	k1gFnPc2	hvězda
se	s	k7c7	s
současnými	současný	k2eAgInPc7d1	současný
prostředky	prostředek	k1gInPc7	prostředek
snáze	snadno	k6eAd2	snadno
detekují	detekovat	k5eAaImIp3nP	detekovat
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
velikost	velikost	k1gFnSc1	velikost
poloměru	poloměr	k1gInSc2	poloměr
podobné	podobný	k2eAgFnSc2d1	podobná
planety	planeta	k1gFnSc2	planeta
už	už	k6eAd1	už
prakticky	prakticky	k6eAd1	prakticky
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
větší	veliký	k2eAgFnSc1d2	veliký
hmotnost	hmotnost	k1gFnSc1	hmotnost
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pouze	pouze	k6eAd1	pouze
další	další	k2eAgNnSc4d1	další
smršťování	smršťování	k1gNnSc4	smršťování
(	(	kIx(	(
<g/>
dokud	dokud	k6eAd1	dokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
nastartování	nastartování	k1gNnSc3	nastartování
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiter	Jupiter	k1gMnSc1	Jupiter
emituje	emitovat	k5eAaBmIp3nS	emitovat
více	hodně	k6eAd2	hodně
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
než	než	k8xS	než
dostává	dostávat	k5eAaImIp3nS	dostávat
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hnědé	hnědý	k2eAgMnPc4d1	hnědý
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
,	,	kIx,	,
jadernou	jaderný	k2eAgFnSc4d1	jaderná
syntézu	syntéza	k1gFnSc4	syntéza
sice	sice	k8xC	sice
provázejí	provázet	k5eAaImIp3nP	provázet
specifické	specifický	k2eAgFnPc4d1	specifická
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
75	[number]	k4	75
<g/>
×	×	k?	×
hmotnější	hmotný	k2eAgFnSc2d2	hmotnější
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaPmF	stát
hnědým	hnědý	k2eAgMnSc7d1	hnědý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiterův	Jupiterův	k2eAgInSc1d1	Jupiterův
objem	objem	k1gInSc1	objem
je	být	k5eAaImIp3nS	být
1321	[number]	k4	1321
<g/>
×	×	k?	×
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
objem	objem	k1gInSc1	objem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
318	[number]	k4	318
<g/>
×	×	k?	×
hmotnější	hmotný	k2eAgFnSc2d2	hmotnější
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MJ	mj	kA	mj
or	or	k?	or
MJup	MJup	k1gInSc1	MJup
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
pro	pro	k7c4	pro
popisování	popisování	k1gNnSc4	popisování
hmotnosti	hmotnost	k1gFnSc2	hmotnost
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
extrasolárních	extrasolární	k2eAgFnPc2d1	extrasolární
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
hnědých	hnědý	k2eAgMnPc2d1	hnědý
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
extrasolární	extrasolární	k2eAgFnSc1d1	extrasolární
planeta	planeta	k1gFnSc1	planeta
HD	HD	kA	HD
209458	[number]	k4	209458
b	b	k?	b
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
0,69	[number]	k4	0,69
MJ	mj	kA	mj
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
COROT-	COROT-	k1gFnSc1	COROT-
<g/>
7	[number]	k4	7
<g/>
b	b	k?	b
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
pouze	pouze	k6eAd1	pouze
0,015	[number]	k4	0,015
MJ.	mj.	kA	mj.
Teoretické	teoretický	k2eAgInPc4d1	teoretický
modely	model	k1gInPc4	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiter	Jupiter	k1gMnSc1	Jupiter
měl	mít	k5eAaImAgMnS	mít
dříve	dříve	k6eAd2	dříve
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc4d2	veliký
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
změny	změna	k1gFnPc4	změna
hmotnosti	hmotnost	k1gFnSc2	hmotnost
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
průměr	průměr	k1gInSc1	průměr
planety	planeta	k1gFnSc2	planeta
měnil	měnit	k5eAaImAgInS	měnit
jen	jen	k9	jen
nepatrně	patrně	k6eNd1	patrně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
hmotnost	hmotnost	k1gFnSc1	hmotnost
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
hmotnost	hmotnost	k1gFnSc4	hmotnost
čtyř	čtyři	k4xCgMnPc2	čtyři
Jupiterů	Jupiter	k1gMnPc2	Jupiter
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
oblasti	oblast	k1gFnPc1	oblast
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
natolik	natolik	k6eAd1	natolik
stlačené	stlačený	k2eAgFnPc1d1	stlačená
vlivem	vliv	k1gInSc7	vliv
působící	působící	k2eAgFnPc1d1	působící
gravitace	gravitace	k1gFnPc1	gravitace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
vedla	vést	k5eAaImAgFnS	vést
některé	některý	k3yIgMnPc4	některý
astronomy	astronom	k1gMnPc4	astronom
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
o	o	k7c6	o
Jupiteru	Jupiter	k1gInSc6	Jupiter
začali	začít	k5eAaPmAgMnP	začít
referovat	referovat	k5eAaBmF	referovat
jako	jako	k9	jako
o	o	k7c6	o
nepovedené	povedený	k2eNgFnSc6d1	nepovedená
hvězdě	hvězda	k1gFnSc6	hvězda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
procesy	proces	k1gInPc4	proces
vedoucí	vedoucí	k2eAgInPc4d1	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
planet	planeta	k1gFnPc2	planeta
jako	jako	k8xC	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
jsou	být	k5eAaImIp3nP	být
stejné	stejné	k1gNnSc4	stejné
jako	jako	k8xC	jako
procesy	proces	k1gInPc4	proces
formující	formující	k2eAgInPc4d1	formující
vícehvězdné	vícehvězdný	k2eAgInPc4d1	vícehvězdný
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Jupiter	Jupiter	k1gMnSc1	Jupiter
zažehl	zažehnout	k5eAaPmAgMnS	zažehnout
termonukleární	termonukleární	k2eAgFnSc4d1	termonukleární
reakci	reakce	k1gFnSc4	reakce
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
ke	k	k7c3	k
spalování	spalování	k1gNnSc3	spalování
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
k	k	k7c3	k
proměně	proměna	k1gFnSc3	proměna
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
<g/>
×	×	k?	×
hmotnější	hmotný	k2eAgFnSc2d2	hmotnější
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgMnPc1d3	nejmenší
známí	známý	k2eAgMnPc1d1	známý
červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jupiter	Jupiter	k1gMnSc1	Jupiter
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
než	než	k8xS	než
dostává	dostávat	k5eAaImIp3nS	dostávat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
vznikajícího	vznikající	k2eAgNnSc2d1	vznikající
uvnitř	uvnitř	k7c2	uvnitř
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
rovné	rovný	k2eAgNnSc1d1	rovné
množství	množství	k1gNnSc1	množství
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
obdrží	obdržet	k5eAaPmIp3nP	obdržet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vyzařované	vyzařovaný	k2eAgNnSc1d1	vyzařované
teplo	teplo	k1gNnSc1	teplo
vzniká	vznikat	k5eAaImIp3nS	vznikat
Kelvin-Helmholtzovým	Kelvin-Helmholtzův	k2eAgInSc7d1	Kelvin-Helmholtzův
mechanismem	mechanismus	k1gInSc7	mechanismus
vlivem	vlivem	k7c2	vlivem
adiabatické	adiabatický	k2eAgFnSc2d1	adiabatická
kontrakce	kontrakce	k1gFnSc2	kontrakce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
planetárnímu	planetární	k2eAgNnSc3d1	planetární
smršťování	smršťování	k1gNnSc3	smršťování
rychlostí	rychlost	k1gFnPc2	rychlost
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
cm	cm	kA	cm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
byl	být	k5eAaImAgInS	být
Jupiter	Jupiter	k1gInSc1	Jupiter
mnohem	mnohem	k6eAd1	mnohem
teplejší	teplý	k2eAgInSc1d2	teplejší
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
poloměr	poloměr	k1gInSc1	poloměr
byl	být	k5eAaImAgInS	být
přibližně	přibližně	k6eAd1	přibližně
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
tvoří	tvořit	k5eAaImIp3nS	tvořit
husté	hustý	k2eAgNnSc4d1	husté
planetární	planetární	k2eAgNnSc4d1	planetární
jádro	jádro	k1gNnSc4	jádro
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
obklopené	obklopený	k2eAgFnSc2d1	obklopená
vrstvou	vrstva	k1gFnSc7	vrstva
tekutého	tekutý	k2eAgInSc2d1	tekutý
kovového	kovový	k2eAgInSc2d1	kovový
vodíku	vodík	k1gInSc2	vodík
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
molekulárního	molekulární	k2eAgInSc2d1	molekulární
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgNnSc7	tento
zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
skrývá	skrývat	k5eAaImIp3nS	skrývat
řada	řada	k1gFnSc1	řada
tajemství	tajemství	k1gNnPc2	tajemství
a	a	k8xC	a
nejasností	nejasnost	k1gFnPc2	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
často	často	k6eAd1	často
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
kamenné	kamenný	k2eAgNnSc1d1	kamenné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc4	jeho
skutečné	skutečný	k2eAgNnSc4d1	skutečné
detailnější	detailní	k2eAgNnSc4d2	detailnější
složení	složení	k1gNnSc4	složení
je	být	k5eAaImIp3nS	být
neznámé	známý	k2eNgNnSc1d1	neznámé
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
vlastnosti	vlastnost	k1gFnPc1	vlastnost
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
ho	on	k3xPp3gInSc4	on
měly	mít	k5eAaImAgFnP	mít
tvořit	tvořit	k5eAaImF	tvořit
za	za	k7c2	za
tlaků	tlak	k1gInPc2	tlak
a	a	k8xC	a
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
této	tento	k3xDgFnSc2	tento
obří	obří	k2eAgFnSc2d1	obří
planety	planeta	k1gFnSc2	planeta
musí	muset	k5eAaImIp3nS	muset
panovat	panovat	k5eAaImF	panovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
měřením	měření	k1gNnSc7	měření
naznačena	naznačit	k5eAaPmNgFnS	naznačit
existence	existence	k1gFnSc1	existence
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
mezi	mezi	k7c7	mezi
12	[number]	k4	12
až	až	k9	až
45	[number]	k4	45
hmotnostmi	hmotnost	k1gFnPc7	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
až	až	k9	až
15	[number]	k4	15
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
jádra	jádro	k1gNnSc2	jádro
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
předpokládána	předpokládat	k5eAaImNgFnS	předpokládat
i	i	k9	i
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
měřením	měření	k1gNnSc7	měření
aspoň	aspoň	k9	aspoň
po	po	k7c4	po
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
historie	historie	k1gFnSc2	historie
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
modely	model	k1gInPc1	model
naznačovaly	naznačovat	k5eAaImAgInP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
planety	planeta	k1gFnSc2	planeta
musela	muset	k5eAaImAgFnS	muset
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vzniknout	vzniknout	k5eAaPmF	vzniknout
kamenoledová	kamenoledový	k2eAgNnPc4d1	kamenoledový
protoplaneta	protoplaneto	k1gNnPc4	protoplaneto
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
by	by	kYmCp3nP	by
byla	být	k5eAaImAgNnP	být
schopna	schopen	k2eAgFnSc1d1	schopna
svojí	svůj	k3xOyFgFnSc7	svůj
hmotností	hmotnost	k1gFnSc7	hmotnost
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
helium	helium	k1gNnSc4	helium
z	z	k7c2	z
protosluneční	protosluneční	k2eAgFnSc2d1	protosluneční
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
tedy	tedy	k9	tedy
jádro	jádro	k1gNnSc1	jádro
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
historie	historie	k1gFnSc2	historie
planety	planeta	k1gFnSc2	planeta
existovalo	existovat	k5eAaImAgNnS	existovat
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
spekulovat	spekulovat	k5eAaImF	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
teplým	teplý	k2eAgInSc7d1	teplý
kovovým	kovový	k2eAgInSc7d1	kovový
vodíkem	vodík	k1gInSc7	vodík
smíchaným	smíchaný	k2eAgMnSc7d1	smíchaný
s	s	k7c7	s
nataveným	natavený	k2eAgMnSc7d1	natavený
či	či	k8xC	či
tavícím	tavící	k2eAgMnSc7d1	tavící
se	se	k3xPyFc4	se
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
jeho	jeho	k3xOp3gInPc1	jeho
stavební	stavební	k2eAgInPc1d1	stavební
prvky	prvek	k1gInPc1	prvek
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
u	u	k7c2	u
dnešního	dnešní	k2eAgInSc2d1	dnešní
Jupiteru	Jupiter	k1gInSc2	Jupiter
neexistuje	existovat	k5eNaImIp3nS	existovat
a	a	k8xC	a
že	že	k8xS	že
gravitační	gravitační	k2eAgNnPc1d1	gravitační
měření	měření	k1gNnPc1	měření
jsou	být	k5eAaImIp3nP	být
chybná	chybný	k2eAgNnPc1d1	chybné
vlivem	vlivem	k7c2	vlivem
nekvalitních	kvalitní	k2eNgNnPc2d1	nekvalitní
měření	měření	k1gNnSc2	měření
současnou	současný	k2eAgFnSc7d1	současná
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesnost	nepřesnost	k1gFnSc1	nepřesnost
modelů	model	k1gInPc2	model
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
chybou	chyba	k1gFnSc7	chyba
rozpětí	rozpětí	k1gNnSc2	rozpětí
u	u	k7c2	u
dosud	dosud	k6eAd1	dosud
měřených	měřený	k2eAgInPc2d1	měřený
parametrů	parametr	k1gInPc2	parametr
<g/>
:	:	kIx,	:
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
rotačních	rotační	k2eAgInPc2d1	rotační
koeficientů	koeficient	k1gInPc2	koeficient
(	(	kIx(	(
<g/>
J	J	kA	J
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
použitého	použitý	k2eAgInSc2d1	použitý
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
gravitačního	gravitační	k2eAgInSc2d1	gravitační
momentu	moment	k1gInSc2	moment
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
rovníkového	rovníkový	k2eAgInSc2d1	rovníkový
poloměru	poloměr	k1gInSc2	poloměr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
teploty	teplota	k1gFnSc2	teplota
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
1	[number]	k4	1
baru	bar	k1gInSc2	bar
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Juno	Juno	k1gFnSc1	Juno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
přinést	přinést	k5eAaPmF	přinést
zpřesnění	zpřesnění	k1gNnSc4	zpřesnění
těchto	tento	k3xDgInPc2	tento
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
učinit	učinit	k5eAaPmF	učinit
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c4	v
pochopení	pochopení	k1gNnSc4	pochopení
problematiky	problematika	k1gFnSc2	problematika
Jupiterova	Jupiterův	k2eAgNnSc2d1	Jupiterovo
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
hypotetického	hypotetický	k2eAgNnSc2d1	hypotetické
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
obklopena	obklopit	k5eAaPmNgFnS	obklopit
hustým	hustý	k2eAgInSc7d1	hustý
kovovým	kovový	k2eAgInSc7d1	kovový
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
rozkládat	rozkládat	k5eAaImF	rozkládat
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
78	[number]	k4	78
%	%	kIx~	%
poloměru	poloměr	k1gInSc2	poloměr
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Procesem	proces	k1gInSc7	proces
podobným	podobný	k2eAgInSc7d1	podobný
dešti	dešť	k1gInSc6	dešť
by	by	k9	by
hélium	hélium	k1gNnSc1	hélium
a	a	k8xC	a
neon	neon	k1gInSc1	neon
měly	mít	k5eAaImAgInP	mít
prostupovat	prostupovat	k5eAaImF	prostupovat
touto	tento	k3xDgFnSc7	tento
vrstvou	vrstva	k1gFnSc7	vrstva
sníženého	snížený	k2eAgNnSc2d1	snížené
zastoupení	zastoupení	k1gNnSc2	zastoupení
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vrstvou	vrstva	k1gFnSc7	vrstva
kovového	kovový	k2eAgInSc2d1	kovový
vodíku	vodík	k1gInSc2	vodík
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
tekutého	tekutý	k2eAgInSc2d1	tekutý
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
vrstva	vrstva	k1gFnSc1	vrstva
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
směrem	směr	k1gInSc7	směr
dolů	dolů	k6eAd1	dolů
z	z	k7c2	z
vrstvy	vrstva	k1gFnSc2	vrstva
mračen	mračno	k1gNnPc2	mračno
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
asi	asi	k9	asi
1000	[number]	k4	1000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
ostrého	ostrý	k2eAgInSc2d1	ostrý
přechodu	přechod	k1gInSc2	přechod
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
vrstvami	vrstva	k1gFnPc7	vrstva
vodíku	vodík	k1gInSc2	vodík
bude	být	k5eAaImBp3nS	být
nejspíše	nejspíše	k9	nejspíše
přechod	přechod	k1gInSc1	přechod
pozvolný	pozvolný	k2eAgInSc1d1	pozvolný
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedno	jeden	k4xCgNnSc1	jeden
skupenství	skupenství	k1gNnSc1	skupenství
vodíku	vodík	k1gInSc2	vodík
bude	být	k5eAaImBp3nS	být
volně	volně	k6eAd1	volně
přecházet	přecházet	k5eAaImF	přecházet
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
bez	bez	k7c2	bez
jasně	jasně	k6eAd1	jasně
definované	definovaný	k2eAgFnSc2d1	definovaná
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hladký	hladký	k2eAgInSc1d1	hladký
přechod	přechod	k1gInSc1	přechod
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
nad	nad	k7c7	nad
kritickou	kritický	k2eAgFnSc7d1	kritická
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vodík	vodík	k1gInSc4	vodík
pouhých	pouhý	k2eAgInPc2d1	pouhý
33	[number]	k4	33
K.	K.	kA	K.
Teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
uvnitř	uvnitř	k7c2	uvnitř
Jupiteru	Jupiter	k1gInSc2	Jupiter
postupně	postupně	k6eAd1	postupně
narůstají	narůstat	k5eAaImIp3nP	narůstat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hypotetickému	hypotetický	k2eAgNnSc3d1	hypotetické
jádru	jádro	k1gNnSc3	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fázového	fázový	k2eAgInSc2d1	fázový
přechodu	přechod	k1gInSc2	přechod
mezi	mezi	k7c7	mezi
tekutým	tekutý	k2eAgInSc7d1	tekutý
a	a	k8xC	a
kovovým	kovový	k2eAgInSc7d1	kovový
vodíkem	vodík	k1gInSc7	vodík
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
teplota	teplota	k1gFnSc1	teplota
nejspíše	nejspíše	k9	nejspíše
kolem	kolem	k7c2	kolem
10	[number]	k4	10
000	[number]	k4	000
K	K	kA	K
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
GPa	GPa	k1gFnPc2	GPa
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
36	[number]	k4	36
000	[number]	k4	000
K	K	kA	K
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
mezi	mezi	k7c7	mezi
3000	[number]	k4	3000
až	až	k9	až
4500	[number]	k4	4500
GPa	GPa	k1gFnPc2	GPa
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
89,8	[number]	k4	89,8
hmotnostních	hmotnostní	k2eAgFnPc2d1	hmotnostní
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
10,2	[number]	k4	10,2
%	%	kIx~	%
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stopové	stopový	k2eAgNnSc4d1	stopové
množství	množství	k1gNnSc4	množství
methanu	methan	k1gInSc2	methan
<g/>
,	,	kIx,	,
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
,	,	kIx,	,
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
"	"	kIx"	"
<g/>
kamení	kamení	k1gNnSc1	kamení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nalézají	nalézat	k5eAaImIp3nP	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
nepatrná	nepatrný	k2eAgNnPc4d1	nepatrný
množství	množství	k1gNnPc4	množství
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
ethanu	ethan	k1gInSc2	ethan
<g/>
,	,	kIx,	,
sulfanu	sulfan	k1gInSc2	sulfan
<g/>
,	,	kIx,	,
neonu	neon	k1gInSc2	neon
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
fosfanu	fosfan	k1gInSc2	fosfan
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
složení	složení	k1gNnSc4	složení
sluneční	sluneční	k2eAgFnSc2d1	sluneční
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Saturn	Saturn	k1gInSc1	Saturn
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
mají	mít	k5eAaImIp3nP	mít
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
pásy	pás	k1gInPc1	pás
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
atmosféry	atmosféra	k1gFnSc2	atmosféra
rotují	rotovat	k5eAaImIp3nP	rotovat
různou	různý	k2eAgFnSc7d1	různá
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
pozorován	pozorovat	k5eAaImNgInS	pozorovat
Cassinim	Cassinim	k1gInSc1	Cassinim
(	(	kIx(	(
<g/>
1690	[number]	k4	1690
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rotace	rotace	k1gFnSc1	rotace
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
polární	polární	k2eAgFnSc2d1	polární
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
rotace	rotace	k1gFnSc1	rotace
jeho	jeho	k3xOp3gFnSc2	jeho
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
pásy	pás	k1gInPc1	pás
mraků	mrak	k1gInPc2	mrak
různé	různý	k2eAgInPc1d1	různý
šíře	široko	k6eAd2	široko
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
stálých	stálý	k2eAgInPc2d1	stálý
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
těchto	tento	k3xDgMnPc2	tento
konfliktních	konfliktní	k2eAgMnPc2d1	konfliktní
proudů	proud	k1gInPc2	proud
vznikají	vznikat	k5eAaImIp3nP	vznikat
bouře	bouř	k1gFnPc4	bouř
a	a	k8xC	a
turbulence	turbulence	k1gFnPc4	turbulence
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
600	[number]	k4	600
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
v	v	k7c6	v
nejhlubších	hluboký	k2eAgNnPc6d3	nejhlubší
místech	místo	k1gNnPc6	místo
měření	měření	k1gNnSc2	měření
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
20	[number]	k4	20
atm	atm	k?	atm
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
km	km	kA	km
pod	pod	k7c7	pod
vrcholkem	vrcholek	k1gInSc7	vrcholek
oblaků	oblak	k1gInPc2	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
permanentně	permanentně	k6eAd1	permanentně
zakryt	zakryt	k2eAgInSc1d1	zakryt
mračny	mračna	k1gFnSc2	mračna
tvořenými	tvořený	k2eAgInPc7d1	tvořený
krystalky	krystalek	k1gInPc7	krystalek
čpavku	čpavek	k1gInSc2	čpavek
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
hydrosulfidem	hydrosulfid	k1gInSc7	hydrosulfid
amonným	amonný	k2eAgInSc7d1	amonný
((	((	k?	((
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
SH	SH	kA	SH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mračna	mračno	k1gNnPc1	mračno
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
tropopauze	tropopauza	k1gFnSc6	tropopauza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
roztroušena	roztroušet	k5eAaImNgNnP	roztroušet
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
výškách	výška	k1gFnPc6	výška
známých	známá	k1gFnPc2	známá
jako	jako	k8xS	jako
tropické	tropický	k2eAgFnSc2d1	tropická
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
mezi	mezi	k7c4	mezi
světlejší	světlý	k2eAgFnPc4d2	světlejší
barevné	barevný	k2eAgFnPc4d1	barevná
zóny	zóna	k1gFnPc4	zóna
a	a	k8xC	a
tmavší	tmavý	k2eAgInPc4d2	tmavší
pásy	pás	k1gInPc4	pás
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
interakce	interakce	k1gFnSc1	interakce
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
cirkulujícími	cirkulující	k2eAgFnPc7d1	cirkulující
skupinami	skupina	k1gFnPc7	skupina
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
bouřemi	bouř	k1gFnPc7	bouř
a	a	k8xC	a
turbulencemi	turbulence	k1gFnPc7	turbulence
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
barevných	barevný	k2eAgFnPc2d1	barevná
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
intenzity	intenzita	k1gFnSc2	intenzita
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gInPc4	on
mohli	moct	k5eAaImAgMnP	moct
astronomové	astronom	k1gMnPc1	astronom
pozorovat	pozorovat	k5eAaImF	pozorovat
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
identifikovat	identifikovat	k5eAaBmF	identifikovat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
mraků	mrak	k1gInPc2	mrak
je	být	k5eAaImIp3nS	být
mocná	mocný	k2eAgFnSc1d1	mocná
pouze	pouze	k6eAd1	pouze
50	[number]	k4	50
km	km	kA	km
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořená	tvořený	k2eAgFnSc1d1	tvořená
dvěma	dva	k4xCgFnPc7	dva
vrstvami	vrstva	k1gFnPc7	vrstva
mračen	mračit	k5eAaImNgInS	mračit
<g/>
:	:	kIx,	:
tenčí	tenčit	k5eAaImIp3nS	tenčit
nižší	nízký	k2eAgFnSc7d2	nižší
vrstvou	vrstva	k1gFnSc7	vrstva
a	a	k8xC	a
silnější	silný	k2eAgFnSc7d2	silnější
čiřejší	čirý	k2eAgFnSc7d2	čirý
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vrstvou	vrstva	k1gFnSc7	vrstva
čpavkových	čpavkový	k2eAgNnPc2d1	čpavkové
mračen	mračno	k1gNnPc2	mračno
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
přítomné	přítomný	k2eAgInPc1d1	přítomný
mraky	mrak	k1gInPc1	mrak
tvořené	tvořený	k2eAgNnSc1d1	tvořené
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
odrazy	odraz	k1gInPc4	odraz
blesků	blesk	k1gInPc2	blesk
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
polární	polární	k2eAgFnSc1d1	polární
molekula	molekula	k1gFnSc1	molekula
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
může	moct	k5eAaImIp3nS	moct
nést	nést	k5eAaImF	nést
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
separovat	separovat	k5eAaBmF	separovat
kladné	kladný	k2eAgInPc4d1	kladný
a	a	k8xC	a
záporné	záporný	k2eAgInPc4d1	záporný
náboje	náboj	k1gInPc4	náboj
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vytvořit	vytvořit	k5eAaPmF	vytvořit
blesk	blesk	k1gInSc4	blesk
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Takto	takto	k6eAd1	takto
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
elektrické	elektrický	k2eAgNnSc1d1	elektrické
napětí	napětí	k1gNnSc1	napětí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tisíckrát	tisíckrát	k6eAd1	tisíckrát
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
u	u	k7c2	u
blesků	blesk	k1gInPc2	blesk
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bouře	bouře	k1gFnSc1	bouře
ve	v	k7c6	v
vodní	vodní	k2eAgFnSc6d1	vodní
vrstvě	vrstva	k1gFnSc6	vrstva
mračen	mračna	k1gFnPc2	mračna
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
vznikat	vznikat	k5eAaImF	vznikat
vlivem	vliv	k1gInSc7	vliv
tepla	teplo	k1gNnSc2	teplo
uvolňovaného	uvolňovaný	k2eAgNnSc2d1	uvolňované
ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
vrstvách	vrstva	k1gFnPc6	vrstva
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
oranžové	oranžový	k2eAgFnPc1d1	oranžová
a	a	k8xC	a
hnědé	hnědý	k2eAgNnSc1d1	hnědé
zbarvení	zbarvení	k1gNnSc1	zbarvení
mračen	mračno	k1gNnPc2	mračno
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
barevnými	barevný	k2eAgFnPc7d1	barevná
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
,	,	kIx,	,
známými	známá	k1gFnPc7	známá
jako	jako	k8xS	jako
chromofory	chromofor	k1gInPc7	chromofor
<g/>
.	.	kIx.	.
</s>
<s>
Vystupují	vystupovat	k5eAaImIp3nP	vystupovat
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
teplejších	teplý	k2eAgNnPc2d2	teplejší
spodních	spodní	k2eAgNnPc2d1	spodní
mračen	mračno	k1gNnPc2	mračno
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
vystaveny	vystavit	k5eAaPmNgInP	vystavit
ultrafialovému	ultrafialový	k2eAgNnSc3d1	ultrafialové
záření	záření	k1gNnSc3	záření
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
těchto	tento	k3xDgFnPc2	tento
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
stále	stále	k6eAd1	stále
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
obsahovat	obsahovat	k5eAaImF	obsahovat
fosfor	fosfor	k1gInSc4	fosfor
<g/>
,	,	kIx,	,
síru	síra	k1gFnSc4	síra
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Zóny	zóna	k1gFnPc1	zóna
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
konvekční	konvekční	k2eAgFnPc1d1	konvekční
buňky	buňka	k1gFnPc1	buňka
tvořené	tvořený	k2eAgFnSc2d1	tvořená
krystalky	krystalka	k1gFnSc2	krystalka
amoniaku	amoniak	k1gInSc2	amoniak
zakryjí	zakrýt	k5eAaPmIp3nP	zakrýt
nižší	nízký	k2eAgFnSc1d2	nižší
mračna	mračna	k1gFnSc1	mračna
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
Jupiteru	Jupiter	k1gInSc2	Jupiter
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblasti	oblast	k1gFnPc1	oblast
pólů	pól	k1gInPc2	pól
dostávají	dostávat	k5eAaImIp3nP	dostávat
méně	málo	k6eAd2	málo
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
oblasti	oblast	k1gFnSc2	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Proudění	proudění	k1gNnSc4	proudění
tepla	teplo	k1gNnSc2	teplo
probíhající	probíhající	k2eAgNnSc4d1	probíhající
uvnitř	uvnitř	k7c2	uvnitř
planety	planeta	k1gFnSc2	planeta
transportuje	transportovat	k5eAaBmIp3nS	transportovat
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc1	energie
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vyrovnává	vyrovnávat	k5eAaImIp3nS	vyrovnávat
teploty	teplota	k1gFnPc4	teplota
ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
útvar	útvar	k1gInSc1	útvar
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stabilní	stabilní	k2eAgFnSc1d1	stabilní
anticyklonální	anticyklonální	k2eAgFnSc1d1	anticyklonální
bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
22	[number]	k4	22
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
skvrna	skvrna	k1gFnSc1	skvrna
byla	být	k5eAaImAgFnS	být
jistě	jistě	k9	jistě
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dokonce	dokonce	k9	dokonce
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
<g/>
.	.	kIx.	.
</s>
<s>
Matematické	matematický	k2eAgInPc1d1	matematický
modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skvrna	skvrna	k1gFnSc1	skvrna
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nP	by
jít	jít	k5eAaImF	jít
o	o	k7c4	o
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
stabilní	stabilní	k2eAgInSc4d1	stabilní
až	až	k8xS	až
permanentní	permanentní	k2eAgInSc4d1	permanentní
útvar	útvar	k1gInSc4	útvar
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jí	jíst	k5eAaImIp3nS	jíst
pozorovat	pozorovat	k5eAaImF	pozorovat
pozemskými	pozemský	k2eAgInPc7d1	pozemský
teleskopy	teleskop	k1gInPc7	teleskop
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
clonu	clona	k1gFnSc4	clona
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
12	[number]	k4	12
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Skvrna	skvrna	k1gFnSc1	skvrna
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
protisměru	protisměr	k1gInSc6	protisměr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
s	s	k7c7	s
rotační	rotační	k2eAgFnSc7d1	rotační
periodou	perioda	k1gFnSc7	perioda
okolo	okolo	k7c2	okolo
šesti	šest	k4xCc2	šest
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
má	mít	k5eAaImIp3nS	mít
rozměry	rozměr	k1gInPc4	rozměr
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
000	[number]	k4	000
km	km	kA	km
×	×	k?	×
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
000	[number]	k4	000
km	km	kA	km
<g/>
;	;	kIx,	;
vešly	vejít	k5eAaPmAgFnP	vejít
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
dvě	dva	k4xCgFnPc1	dva
až	až	k8xS	až
tři	tři	k4xCgFnPc1	tři
Země	zem	k1gFnPc1	zem
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Skvrna	skvrna	k1gFnSc1	skvrna
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
maximálně	maximálně	k6eAd1	maximálně
okolo	okolo	k7c2	okolo
8	[number]	k4	8
km	km	kA	km
nad	nad	k7c4	nad
okolní	okolní	k2eAgInPc4d1	okolní
vrcholky	vrcholek	k1gInPc4	vrcholek
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Bouře	bouře	k1gFnSc1	bouře
jako	jako	k9	jako
tato	tento	k3xDgFnSc1	tento
jsou	být	k5eAaImIp3nP	být
typickým	typický	k2eAgInSc7d1	typický
projevem	projev	k1gInSc7	projev
v	v	k7c6	v
atmosférách	atmosféra	k1gFnPc6	atmosféra
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
hnědé	hnědý	k2eAgFnPc1d1	hnědá
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
bezejmenné	bezejmenný	k2eAgInPc1d1	bezejmenný
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
skvrny	skvrna	k1gFnPc1	skvrna
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvořeny	tvořit	k5eAaImNgInP	tvořit
relativně	relativně	k6eAd1	relativně
studenými	studený	k2eAgInPc7d1	studený
mračny	mračna	k1gFnSc2	mračna
uvnitř	uvnitř	k7c2	uvnitř
svrchní	svrchní	k2eAgFnSc2d1	svrchní
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Hnědé	hnědý	k2eAgFnPc1d1	hnědá
skvrny	skvrna	k1gFnPc1	skvrna
jsou	být	k5eAaImIp3nP	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
nejspíše	nejspíše	k9	nejspíše
teplejší	teplý	k2eAgInSc1d2	teplejší
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
mračna	mračno	k1gNnPc4	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
bouře	bouř	k1gFnPc1	bouř
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
od	od	k7c2	od
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
až	až	k6eAd1	až
po	po	k7c4	po
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
před	před	k7c4	před
přílety	přílet	k1gInPc4	přílet
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
(	(	kIx(	(
<g/>
prolétly	prolétnout	k5eAaPmAgFnP	prolétnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
skvrny	skvrna	k1gFnPc1	skvrna
nejsou	být	k5eNaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
žádnými	žádný	k3yNgInPc7	žádný
procesy	proces	k1gInPc7	proces
vycházejícími	vycházející	k2eAgMnPc7d1	vycházející
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
skvrny	skvrna	k1gFnPc1	skvrna
chovají	chovat	k5eAaImIp3nP	chovat
samostatně	samostatně	k6eAd1	samostatně
bez	bez	k7c2	bez
očividného	očividný	k2eAgInSc2d1	očividný
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
okolní	okolní	k2eAgFnSc3d1	okolní
atmosféře	atmosféra	k1gFnSc3	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
okolní	okolní	k2eAgFnPc1d1	okolní
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
současně	současně	k6eAd1	současně
rotovat	rotovat	k5eAaImF	rotovat
i	i	k9	i
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
vůči	vůči	k7c3	vůči
okolí	okolí	k1gNnSc3	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
existujících	existující	k2eAgInPc2d1	existující
záznamů	záznam	k1gInPc2	záznam
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
skvrny	skvrna	k1gFnPc4	skvrna
oběhly	oběhnout	k5eAaPmAgInP	oběhnout
planetu	planeta	k1gFnSc4	planeta
několikrát	několikrát	k6eAd1	několikrát
bez	bez	k7c2	bez
žádného	žádný	k3yNgInSc2	žádný
náznaku	náznak	k1gInSc2	náznak
spojení	spojení	k1gNnPc2	spojení
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
či	či	k8xC	či
se	s	k7c7	s
spodními	spodní	k2eAgFnPc7d1	spodní
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
bouře	bouř	k1gFnSc2	bouř
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobná	podobný	k2eAgFnSc1d1	podobná
Velké	velký	k2eAgFnSc3d1	velká
rudé	rudý	k2eAgFnSc3d1	rudá
skvrně	skvrna	k1gFnSc3	skvrna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
sloučení	sloučení	k1gNnSc2	sloučení
několika	několik	k4yIc2	několik
menších	malý	k2eAgFnPc2d2	menší
bouří	bouř	k1gFnPc2	bouř
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
menší	malý	k2eAgFnPc4d2	menší
bílé	bílý	k2eAgFnPc4d1	bílá
bouře	bouř	k1gFnPc4	bouř
pozorované	pozorovaný	k2eAgFnPc4d1	pozorovaná
již	již	k9	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2005	[number]	k4	2005
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
tuto	tento	k3xDgFnSc4	tento
novou	nový	k2eAgFnSc4d1	nová
bouři	bouře	k1gFnSc4	bouře
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
Ovál	ovál	k1gInSc4	ovál
BA	ba	k9	ba
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
přezdívku	přezdívka	k1gFnSc4	přezdívka
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
junior	junior	k1gMnSc1	junior
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vzniku	vznik	k1gInSc2	vznik
narostla	narůst	k5eAaPmAgFnS	narůst
její	její	k3xOp3gFnSc1	její
intenzita	intenzita	k1gFnSc1	intenzita
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
její	její	k3xOp3gFnSc2	její
barvy	barva	k1gFnSc2	barva
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
na	na	k7c4	na
červenou	červená	k1gFnSc4	červená
během	během	k7c2	během
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
a	a	k8xC	a
silnou	silný	k2eAgFnSc4d1	silná
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Projevy	projev	k1gInPc1	projev
jeho	on	k3xPp3gNnSc2	on
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
jevit	jevit	k5eAaImF	jevit
až	až	k9	až
5	[number]	k4	5
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vzdálenější	vzdálený	k2eAgMnSc1d2	vzdálenější
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
14	[number]	k4	14
<g/>
krát	krát	k6eAd1	krát
silnější	silný	k2eAgFnSc2d2	silnější
než	než	k8xS	než
zemské	zemský	k2eAgFnSc2d1	zemská
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
intenzita	intenzita	k1gFnSc1	intenzita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
4,2	[number]	k4	4,2
gausse	gauss	k1gInSc5	gauss
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
0,42	[number]	k4	0,42
mT	mT	k?	mT
<g/>
)	)	kIx)	)
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
10	[number]	k4	10
až	až	k9	až
14	[number]	k4	14
gaussů	gauss	k1gInPc2	gauss
(	(	kIx(	(
<g/>
1	[number]	k4	1
až	až	k9	až
1,4	[number]	k4	1,4
mT	mT	k?	mT
<g/>
)	)	kIx)	)
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mohutné	mohutný	k2eAgInPc4d1	mohutný
výrony	výron	k1gInPc4	výron
urychlených	urychlený	k2eAgFnPc2d1	urychlená
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
Jupiterových	Jupiterův	k2eAgInPc6d1	Jupiterův
radiačních	radiační	k2eAgInPc6d1	radiační
pásech	pás	k1gInPc6	pás
<g/>
,	,	kIx,	,
interaguje	interagovat	k5eAaPmIp3nS	interagovat
s	s	k7c7	s
měsícem	měsíc	k1gInSc7	měsíc
Io	Io	k1gFnSc2	Io
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vodivou	vodivý	k2eAgFnSc4d1	vodivá
trubici	trubice	k1gFnSc4	trubice
a	a	k8xC	a
plazmový	plazmový	k2eAgInSc1d1	plazmový
prstenec	prstenec	k1gInSc1	prstenec
okolo	okolo	k7c2	okolo
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Jupiterova	Jupiterův	k2eAgFnSc1d1	Jupiterova
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
strukturou	struktura	k1gFnSc7	struktura
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pole	pole	k1gNnSc1	pole
vzniká	vznikat	k5eAaImIp3nS	vznikat
vířivými	vířivý	k2eAgInPc7d1	vířivý
proudy	proud	k1gInPc7	proud
uvnitř	uvnitř	k7c2	uvnitř
jádra	jádro	k1gNnSc2	jádro
tvořeného	tvořený	k2eAgInSc2d1	tvořený
kovovým	kovový	k2eAgInSc7d1	kovový
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Pole	pole	k1gFnSc1	pole
zachytává	zachytávat	k5eAaImIp3nS	zachytávat
ionizované	ionizovaný	k2eAgFnSc2d1	ionizovaná
částice	částice	k1gFnSc2	částice
ze	z	k7c2	z
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
vysokoenergetického	vysokoenergetický	k2eAgNnSc2d1	vysokoenergetické
pole	pole	k1gNnSc2	pole
mimo	mimo	k7c4	mimo
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
plazmatického	plazmatický	k2eAgInSc2d1	plazmatický
povlaku	povlak	k1gInSc2	povlak
ionizují	ionizovat	k5eAaBmIp3nP	ionizovat
mračna	mračno	k1gNnPc1	mračno
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
torusu	torus	k1gInSc2	torus
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
aktivitou	aktivita	k1gFnSc7	aktivita
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Vodíkové	vodíkový	k2eAgFnPc1d1	vodíková
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
uniklé	uniklý	k2eAgFnPc1d1	uniklá
z	z	k7c2	z
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
zachyceny	zachycen	k2eAgInPc1d1	zachycen
v	v	k7c6	v
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
generují	generovat	k5eAaImIp3nP	generovat
silné	silný	k2eAgInPc1d1	silný
rádiové	rádiový	k2eAgInPc1d1	rádiový
signály	signál	k1gInPc1	signál
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0,6	[number]	k4	0,6
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
existenci	existence	k1gFnSc4	existence
Jupiterova	Jupiterův	k2eAgNnSc2d1	Jupiterovo
mohutného	mohutný	k2eAgNnSc2d1	mohutné
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Citlivé	citlivý	k2eAgInPc1d1	citlivý
přístroje	přístroj	k1gInPc1	přístroj
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
odhalily	odhalit	k5eAaPmAgFnP	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiterův	Jupiterův	k2eAgMnSc1d1	Jupiterův
"	"	kIx"	"
<g/>
severní	severní	k2eAgMnSc1d1	severní
<g/>
"	"	kIx"	"
magnetický	magnetický	k2eAgInSc1d1	magnetický
pól	pól	k1gInSc1	pól
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
geografickém	geografický	k2eAgInSc6d1	geografický
pólu	pól	k1gInSc6	pól
planety	planeta	k1gFnSc2	planeta
s	s	k7c7	s
odchylkou	odchylka	k1gFnSc7	odchylka
11	[number]	k4	11
stupňů	stupeň	k1gInPc2	stupeň
od	od	k7c2	od
jupiterské	jupiterský	k2eAgFnSc2d1	Jupiterská
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
a	a	k8xC	a
se	s	k7c7	s
středem	střed	k1gInSc7	střed
pole	pole	k1gNnSc4	pole
posunutým	posunutý	k2eAgMnSc7d1	posunutý
mimo	mimo	k7c4	mimo
střed	střed	k1gInSc4	střed
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
u	u	k7c2	u
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pioneer	Pioneer	kA	Pioneer
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
rázovou	rázový	k2eAgFnSc4d1	rázová
vlnu	vlna	k1gFnSc4	vlna
jupiterské	jupiterský	k2eAgFnSc2d1	Jupiterská
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
ještě	ještě	k9	ještě
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
26	[number]	k4	26
miliónů	milión	k4xCgInPc2	milión
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
magnetický	magnetický	k2eAgInSc1d1	magnetický
ohon	ohon	k1gInSc1	ohon
dosahující	dosahující	k2eAgInSc1d1	dosahující
až	až	k9	až
za	za	k7c4	za
Saturnovu	Saturnův	k2eAgFnSc4d1	Saturnova
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
tohoto	tento	k3xDgInSc2	tento
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
obrácené	obrácený	k2eAgNnSc1d1	obrácené
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
rychle	rychle	k6eAd1	rychle
kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
tlaku	tlak	k1gInSc2	tlak
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
byl	být	k5eAaImAgInS	být
blíže	blízce	k6eAd2	blízce
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
při	při	k7c6	při
dvou	dva	k4xCgFnPc6	dva
misích	mise	k1gFnPc6	mise
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šoková	šokový	k2eAgFnSc1d1	šoková
vlna	vlna	k1gFnSc1	vlna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
75	[number]	k4	75
poloměrů	poloměr	k1gInPc2	poloměr
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
proudy	proud	k1gInPc1	proud
vysokoenergetických	vysokoenergetický	k2eAgFnPc2d1	vysokoenergetická
částic	částice	k1gFnPc2	částice
jsou	být	k5eAaImIp3nP	být
vyvrhovány	vyvrhován	k2eAgFnPc1d1	vyvrhována
až	až	k9	až
k	k	k7c3	k
oběžné	oběžný	k2eAgFnSc3d1	oběžná
dráze	dráha	k1gFnSc3	dráha
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jupiterovských	jupiterovský	k2eAgInPc6d1	jupiterovský
radiačních	radiační	k2eAgInPc6d1	radiační
pásech	pás	k1gInPc6	pás
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
a	a	k8xC	a
naměřeny	naměřen	k2eAgInPc1d1	naměřen
vysokoenergetické	vysokoenergetický	k2eAgInPc1d1	vysokoenergetický
protony	proton	k1gInPc1	proton
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
některými	některý	k3yIgFnPc7	některý
jeho	jeho	k3xOp3gInPc7	jeho
měsíci	měsíc	k1gInPc7	měsíc
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
Io	Io	k1gMnSc2	Io
<g/>
)	)	kIx)	)
protékají	protékat	k5eAaImIp3nP	protékat
elektrické	elektrický	k2eAgInPc1d1	elektrický
proudy	proud	k1gInPc1	proud
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
čtyři	čtyři	k4xCgInPc1	čtyři
velké	velký	k2eAgInPc1d1	velký
měsíce	měsíc	k1gInPc1	měsíc
ale	ale	k8xC	ale
leží	ležet	k5eAaImIp3nP	ležet
uvnitř	uvnitř	k7c2	uvnitř
tohoto	tento	k3xDgNnSc2	tento
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
chráněny	chránit	k5eAaImNgFnP	chránit
před	před	k7c7	před
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
magnetopauza	magnetopauza	k1gFnSc1	magnetopauza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
okraji	okraj	k1gInSc6	okraj
přechodové	přechodový	k2eAgFnSc2d1	přechodová
vrstvy	vrstva	k1gFnSc2	vrstva
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
planety	planeta	k1gFnSc2	planeta
stává	stávat	k5eAaImIp3nS	stávat
slabým	slabý	k2eAgInSc7d1	slabý
a	a	k8xC	a
neuspořádaným	uspořádaný	k2eNgInSc7d1	neuspořádaný
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
krátké	krátký	k2eAgInPc4d1	krátký
rádiové	rádiový	k2eAgInPc4d1	rádiový
záblesky	záblesk	k1gInPc4	záblesk
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
aktivita	aktivita	k1gFnSc1	aktivita
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
dodává	dodávat	k5eAaImIp3nS	dodávat
do	do	k7c2	do
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
torus	torus	k1gInSc4	torus
částic	částice	k1gFnPc2	částice
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Io	Io	k1gFnSc1	Io
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
skrze	skrze	k?	skrze
tento	tento	k3xDgInSc4	tento
torus	torus	k1gInSc4	torus
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
interakcí	interakce	k1gFnSc7	interakce
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Alfvénových	Alfvénový	k2eAgFnPc2d1	Alfvénový
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přenášejí	přenášet	k5eAaImIp3nP	přenášet
ionizované	ionizovaný	k2eAgFnPc1d1	ionizovaná
částice	částice	k1gFnPc1	částice
do	do	k7c2	do
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vznik	vznik	k1gInSc4	vznik
radiových	radiový	k2eAgFnPc2d1	radiová
vln	vlna	k1gFnPc2	vlna
vlivem	vlivem	k7c2	vlivem
mechanismu	mechanismus	k1gInSc2	mechanismus
cyklotronového	cyklotronový	k2eAgInSc2d1	cyklotronový
astrofyzikálního	astrofyzikální	k2eAgInSc2d1	astrofyzikální
maseru	maser	k1gInSc2	maser
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
šířena	šířit	k5eAaImNgFnS	šířit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
podél	podél	k7c2	podél
povrchu	povrch	k1gInSc2	povrch
kuželu	kužel	k1gInSc2	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Země	země	k1gFnSc1	země
prochází	procházet	k5eAaImIp3nS	procházet
tímto	tento	k3xDgInSc7	tento
kuželem	kužel	k1gInSc7	kužel
<g/>
,	,	kIx,	,
můžou	můžou	k?	můžou
radiové	radiový	k2eAgInPc4d1	radiový
signály	signál	k1gInPc4	signál
přehlušit	přehlušit	k5eAaPmF	přehlušit
šum	šum	k1gInSc4	šum
způsobovaný	způsobovaný	k2eAgInSc4d1	způsobovaný
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Rádiové	rádiový	k2eAgFnPc1d1	rádiová
vlny	vlna	k1gFnPc1	vlna
Jupitera	Jupiter	k1gMnSc2	Jupiter
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
vesmírem	vesmír	k1gInSc7	vesmír
na	na	k7c6	na
frekvencích	frekvence	k1gFnPc6	frekvence
od	od	k7c2	od
15	[number]	k4	15
do	do	k7c2	do
38	[number]	k4	38
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
mezí	mez	k1gFnSc7	mez
jsou	být	k5eAaImIp3nP	být
vlny	vlna	k1gFnPc1	vlna
odráženy	odrážen	k2eAgFnPc1d1	odrážen
ionosférou	ionosféra	k1gFnSc7	ionosféra
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
nad	nad	k7c7	nad
mezí	mez	k1gFnSc7	mez
je	být	k5eAaImIp3nS	být
intenzita	intenzita	k1gFnSc1	intenzita
vln	vlna	k1gFnPc2	vlna
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Rádiové	rádiový	k2eAgFnPc1d1	rádiová
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
7	[number]	k4	7
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
módů	mód	k1gInPc2	mód
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
frekvenční	frekvenční	k2eAgInPc1d1	frekvenční
kanály	kanál	k1gInPc1	kanál
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
Jupitera	Jupiter	k1gMnSc2	Jupiter
a	a	k8xC	a
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gFnSc2	Io
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
frekvencích	frekvence	k1gFnPc6	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
módy	mód	k1gInPc1	mód
uvedeny	uveden	k2eAgInPc1d1	uveden
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prstence	prstenec	k1gInSc2	prstenec
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
nezřetelný	zřetelný	k2eNgInSc4d1	nezřetelný
systém	systém	k1gInSc4	systém
planetárních	planetární	k2eAgInPc2d1	planetární
prstenců	prstenec	k1gInPc2	prstenec
skládajících	skládající	k2eAgInPc2d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
torusu	torus	k1gInSc2	torus
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
jasného	jasný	k2eAgInSc2d1	jasný
hlavního	hlavní	k2eAgInSc2d1	hlavní
prstence	prstenec	k1gInSc2	prstenec
a	a	k8xC	a
vnějšího	vnější	k2eAgInSc2d1	vnější
slabšího	slabý	k2eAgInSc2d2	slabší
prstence	prstenec	k1gInSc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
prstencům	prstenec	k1gInPc3	prstenec
Saturnu	Saturn	k1gInSc2	Saturn
nejsou	být	k5eNaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInSc1d1	vnější
prstenec	prstenec	k1gInSc1	prstenec
je	být	k5eAaImIp3nS	být
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
podobných	podobný	k2eAgInPc2d1	podobný
kouři	kouř	k1gInPc7	kouř
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
po	po	k7c6	po
dopadech	dopad	k1gInPc6	dopad
meteoritů	meteorit	k1gInPc2	meteorit
vymrštěny	vymrštěn	k2eAgInPc1d1	vymrštěn
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
prstenec	prstenec	k1gInSc1	prstenec
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
prachem	prach	k1gInSc7	prach
ze	z	k7c2	z
satelitů	satelit	k1gInPc2	satelit
Adrastea	Adrasteum	k1gNnSc2	Adrasteum
a	a	k8xC	a
Metis	Metis	k1gFnSc2	Metis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
aby	aby	kYmCp3nS	aby
spadl	spadnout	k5eAaPmAgInS	spadnout
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
působením	působení	k1gNnSc7	působení
Jupiteru	Jupiter	k1gInSc2	Jupiter
zachycen	zachycen	k2eAgInSc4d1	zachycen
a	a	k8xC	a
přitahován	přitahován	k2eAgInSc4d1	přitahován
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
impakty	impakt	k1gInPc1	impakt
pak	pak	k6eAd1	pak
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
nový	nový	k2eAgInSc4d1	nový
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
široké	široký	k2eAgInPc4d1	široký
jemné	jemný	k2eAgInPc4d1	jemný
prstence	prstenec	k1gInPc4	prstenec
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
hlavní	hlavní	k2eAgInPc1d1	hlavní
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
měsíců	měsíc	k1gInPc2	měsíc
Thebe	Theb	k1gInSc5	Theb
a	a	k8xC	a
Amalthea	Amaltheum	k1gNnSc2	Amaltheum
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
velmi	velmi	k6eAd1	velmi
řídký	řídký	k2eAgInSc1d1	řídký
a	a	k8xC	a
vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
vnější	vnější	k2eAgInSc1d1	vnější
prstenec	prstenec	k1gInSc1	prstenec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
krouží	kroužit	k5eAaImIp3nS	kroužit
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
nejistý	jistý	k2eNgInSc1d1	nejistý
<g/>
,	,	kIx,	,
snad	snad	k9	snad
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zachyceným	zachycený	k2eAgInSc7d1	zachycený
meziplanetárním	meziplanetární	k2eAgInSc7d1	meziplanetární
prachem	prach	k1gInSc7	prach
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
těžiště	těžiště	k1gNnSc1	těžiště
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
leží	ležet	k5eAaImIp3nS	ležet
mimo	mimo	k7c4	mimo
objem	objem	k1gInSc4	objem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jen	jen	k9	jen
o	o	k7c4	o
7	[number]	k4	7
%	%	kIx~	%
jeho	jeho	k3xOp3gInPc2	jeho
poloměru	poloměr	k1gInSc2	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
je	být	k5eAaImIp3nS	být
778	[number]	k4	778
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
5,2	[number]	k4	5,2
AU	au	k0	au
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolem	kolem	k6eAd1	kolem
Slunce	slunce	k1gNnSc1	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
11,86	[number]	k4	11,86
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
oběžné	oběžný	k2eAgFnSc2d1	oběžná
doby	doba	k1gFnSc2	doba
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
má	mít	k5eAaImIp3nS	mít
dráhovou	dráhový	k2eAgFnSc4d1	dráhová
rezonanci	rezonance	k1gFnSc4	rezonance
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
dráhy	dráha	k1gFnSc2	dráha
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
1,31	[number]	k4	1,31
<g/>
°	°	k?	°
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ekliptice	ekliptika	k1gFnSc3	ekliptika
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
excentricitu	excentricita	k1gFnSc4	excentricita
rovnou	rovnou	k6eAd1	rovnou
0,048	[number]	k4	0,048
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
mezi	mezi	k7c7	mezi
perihéliem	perihélium	k1gNnSc7	perihélium
a	a	k8xC	a
aféliem	afélium	k1gNnSc7	afélium
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
75	[number]	k4	75
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
Jupiteru	Jupiter	k1gInSc2	Jupiter
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
3,13	[number]	k4	3,13
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tak	tak	k6eAd1	tak
malého	malý	k2eAgInSc2d1	malý
sklonu	sklon	k1gInSc2	sklon
osy	osa	k1gFnSc2	osa
se	se	k3xPyFc4	se
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
neprojevují	projevovat	k5eNaImIp3nP	projevovat
sezónní	sezónní	k2eAgFnPc1d1	sezónní
variace	variace	k1gFnPc1	variace
počasí	počasí	k1gNnSc2	počasí
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
či	či	k8xC	či
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
nejrychlejší	rychlý	k2eAgFnSc4d3	nejrychlejší
rotaci	rotace	k1gFnSc4	rotace
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
otočku	otočka	k1gFnSc4	otočka
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
vyklenutí	vyklenutí	k1gNnSc4	vyklenutí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
viditelná	viditelný	k2eAgFnSc1d1	viditelná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
i	i	k9	i
amatérskými	amatérský	k2eAgInPc7d1	amatérský
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnSc3	takový
rotaci	rotace	k1gFnSc3	rotace
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
odstředivé	odstředivý	k2eAgNnSc1d1	odstředivé
zrychlení	zrychlení	k1gNnSc1	zrychlení
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
okolo	okolo	k7c2	okolo
1,67	[number]	k4	1,67
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s2	s2	k4	s2
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
čistou	čistý	k2eAgFnSc4d1	čistá
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
24,79	[number]	k4	24,79
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s2	s2	k4	s2
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgNnSc1d1	výsledné
gravitační	gravitační	k2eAgNnSc1d1	gravitační
zrychlení	zrychlení	k1gNnSc1	zrychlení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
pouze	pouze	k6eAd1	pouze
23,12	[number]	k4	23,12
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s2	s2	k4	s2
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
rotačního	rotační	k2eAgInSc2d1	rotační
sferoidu	sferoid	k1gInSc2	sferoid
<g/>
,	,	kIx,	,
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
9275	[number]	k4	9275
km	km	kA	km
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
polární	polární	k2eAgFnSc4d1	polární
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
není	být	k5eNaImIp3nS	být
těleso	těleso	k1gNnSc4	těleso
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
části	část	k1gFnPc4	část
jeho	jeho	k3xOp3gFnSc2	jeho
svrchní	svrchní	k2eAgFnSc2d1	svrchní
atmosféry	atmosféra	k1gFnSc2	atmosféra
mají	mít	k5eAaImIp3nP	mít
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
rotaci	rotace	k1gFnSc4	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Rotační	rotační	k2eAgFnSc1d1	rotační
doba	doba	k1gFnSc1	doba
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
delší	dlouhý	k2eAgFnSc4d2	delší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rotační	rotační	k2eAgFnSc1d1	rotační
doba	doba	k1gFnSc1	doba
atmosféry	atmosféra	k1gFnSc2	atmosféra
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
těchto	tento	k3xDgFnPc2	tento
vrstev	vrstva	k1gFnPc2	vrstva
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tři	tři	k4xCgFnPc1	tři
referenční	referenční	k2eAgFnPc1d1	referenční
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
popsat	popsat	k5eAaPmF	popsat
pohyb	pohyb	k1gInSc1	pohyb
částic	částice	k1gFnPc2	částice
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
oblastmi	oblast	k1gFnPc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
I	i	k9	i
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
oblasti	oblast	k1gFnPc4	oblast
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
až	až	k8xS	až
10	[number]	k4	10
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
s	s	k7c7	s
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
dobou	doba	k1gFnSc7	doba
rotace	rotace	k1gFnSc2	rotace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
9	[number]	k4	9
hod	hod	k1gInSc1	hod
50	[number]	k4	50
min	mina	k1gFnPc2	mina
a	a	k8xC	a
30	[number]	k4	30
s.	s.	k?	s.
Systém	systém	k1gInSc1	systém
II	II	kA	II
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
10	[number]	k4	10
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
hod	hod	k1gInSc1	hod
55	[number]	k4	55
min	mina	k1gFnPc2	mina
a	a	k8xC	a
40,6	[number]	k4	40,6
s.	s.	k?	s.
Systém	systém	k1gInSc1	systém
III	III	kA	III
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
kvůli	kvůli	k7c3	kvůli
radioastronomii	radioastronomie	k1gFnSc3	radioastronomie
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rotaci	rotace	k1gFnSc4	rotace
planetární	planetární	k2eAgFnSc2d1	planetární
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
oficiální	oficiální	k2eAgFnSc1d1	oficiální
doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc2	rotace
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
nejjasnějším	jasný	k2eAgInSc7d3	nejjasnější
objektem	objekt	k1gInSc7	objekt
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
Měsíci	měsíc	k1gInSc6	měsíc
a	a	k8xC	a
Venuši	Venuše	k1gFnSc6	Venuše
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jasnějším	jasný	k2eAgFnPc3d2	jasnější
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
stane	stanout	k5eAaPmIp3nS	stanout
planeta	planeta	k1gFnSc1	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
více	hodně	k6eAd2	hodně
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
Jupiterova	Jupiterův	k2eAgFnSc1d1	Jupiterova
magnituda	magnituda	k1gFnSc1	magnituda
od	od	k7c2	od
-	-	kIx~	-
<g/>
2,9	[number]	k4	2,9
v	v	k7c6	v
době	doba	k1gFnSc6	doba
opozice	opozice	k1gFnSc2	opozice
až	až	k9	až
na	na	k7c6	na
-	-	kIx~	-
<g/>
1,6	[number]	k4	1,6
v	v	k7c6	v
době	doba	k1gFnSc6	doba
konjunkce	konjunkce	k1gFnSc2	konjunkce
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gNnSc4	on
pozorovat	pozorovat	k5eAaImF	pozorovat
triedrem	triedr	k1gInSc7	triedr
i	i	k8xC	i
na	na	k7c6	na
denní	denní	k2eAgFnSc6d1	denní
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Úhlová	úhlový	k2eAgFnSc1d1	úhlová
velikost	velikost	k1gFnSc1	velikost
Jupiteru	Jupiter	k1gInSc2	Jupiter
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
mezi	mezi	k7c7	mezi
50,1	[number]	k4	50,1
a	a	k8xC	a
29,8	[number]	k4	29,8
úhlové	úhlový	k2eAgFnSc2d1	úhlová
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Příznivé	příznivý	k2eAgFnPc1d1	příznivá
opozice	opozice	k1gFnPc1	opozice
nastávají	nastávat	k5eAaImIp3nP	nastávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
Jupiter	Jupiter	k1gMnSc1	Jupiter
prochází	procházet	k5eAaImIp3nS	procházet
perihéliem	perihélium	k1gNnSc7	perihélium
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
nastává	nastávat	k5eAaImIp3nS	nastávat
jednou	jeden	k4xCgFnSc7	jeden
během	během	k7c2	během
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
perihéliu	perihélium	k1gNnSc6	perihélium
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
a	a	k8xC	a
příznivá	příznivý	k2eAgFnSc1d1	příznivá
opozice	opozice	k1gFnSc1	opozice
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Každých	každý	k3xTgInPc2	každý
398,9	[number]	k4	398,9
dnů	den	k1gInPc2	den
obíhání	obíhání	k1gNnSc2	obíhání
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
Země	zem	k1gFnSc2	zem
předstihne	předstihnout	k5eAaPmIp3nS	předstihnout
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
synodická	synodický	k2eAgFnSc1d1	synodická
perioda	perioda	k1gFnSc1	perioda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgInSc6	ten
Jupiter	Jupiter	k1gMnSc1	Jupiter
projde	projít	k5eAaPmIp3nS	projít
retrográdní	retrográdní	k2eAgFnSc4d1	retrográdní
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
hvězdy	hvězda	k1gFnPc4	hvězda
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
to	ten	k3xDgNnSc4	ten
vypadá	vypadat	k5eAaPmIp3nS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Jupiter	Jupiter	k1gMnSc1	Jupiter
po	po	k7c4	po
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
utváří	utvářit	k5eAaPmIp3nS	utvářit
tak	tak	k6eAd1	tak
smyčku	smyčka	k1gFnSc4	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
počet	počet	k1gInSc1	počet
znamení	znamení	k1gNnSc2	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
historický	historický	k2eAgInSc4d1	historický
původ	původ	k1gInSc4	původ
těchto	tento	k3xDgNnPc2	tento
znamení	znamení	k1gNnPc2	znamení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
Jupiter	Jupiter	k1gMnSc1	Jupiter
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
opozice	opozice	k1gFnSc1	opozice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
posunutý	posunutý	k2eAgInSc1d1	posunutý
na	na	k7c4	na
východ	východ	k1gInSc4	východ
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
šířka	šířka	k1gFnSc1	šířka
znamení	znamení	k1gNnSc2	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Protože	protože	k8xS	protože
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Jupiteru	Jupiter	k1gInSc2	Jupiter
je	být	k5eAaImIp3nS	být
vně	vně	k6eAd1	vně
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
fázový	fázový	k2eAgInSc4d1	fázový
úhel	úhel	k1gInSc4	úhel
Jupiteru	Jupiter	k1gInSc2	Jupiter
sledovaného	sledovaný	k2eAgInSc2d1	sledovaný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
nikdy	nikdy	k6eAd1	nikdy
nepřekročí	překročit	k5eNaPmIp3nS	překročit
11,5	[number]	k4	11,5
<g/>
°	°	k?	°
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
blízko	blízko	k6eAd1	blízko
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
skrze	skrze	k?	skrze
zemské	zemský	k2eAgInPc4d1	zemský
dalekohledy	dalekohled	k1gInPc4	dalekohled
vždy	vždy	k6eAd1	vždy
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
osvětlená	osvětlený	k2eAgFnSc1d1	osvětlená
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
zčásti	zčásti	k6eAd1	zčásti
zatemněného	zatemněný	k2eAgInSc2d1	zatemněný
Jupiteru	Jupiter	k1gInSc2	Jupiter
byly	být	k5eAaImAgInP	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
vesmírných	vesmírný	k2eAgFnPc6d1	vesmírná
misích	mise	k1gFnPc6	mise
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc7	Galilei
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
malého	malý	k2eAgInSc2d1	malý
dalekohledu	dalekohled	k1gInSc2	dalekohled
objevil	objevit	k5eAaPmAgMnS	objevit
čtyři	čtyři	k4xCgInPc4	čtyři
největší	veliký	k2eAgInPc4d3	veliký
měsíce	měsíc	k1gInPc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
-	-	kIx~	-
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
Europu	Europ	k1gInSc2	Europ
<g/>
,	,	kIx,	,
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vžil	vžít	k5eAaPmAgInS	vžít
název	název	k1gInSc4	název
Galileovy	Galileův	k2eAgInPc1d1	Galileův
měsíce	měsíc	k1gInPc1	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pozorování	pozorování	k1gNnSc1	pozorování
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
první	první	k4xOgNnSc1	první
pozorování	pozorování	k1gNnSc1	pozorování
měsíce	měsíc	k1gInSc2	měsíc
jiné	jiný	k2eAgFnSc2d1	jiná
planety	planeta	k1gFnSc2	planeta
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nutno	nutno	k6eAd1	nutno
ale	ale	k9	ale
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínský	čínský	k2eAgMnSc1d1	čínský
historik	historik	k1gMnSc1	historik
astronomie	astronomie	k1gFnSc2	astronomie
Xi	Xi	k1gMnSc1	Xi
Zezong	Zezong	k1gMnSc1	Zezong
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínský	čínský	k2eAgMnSc1d1	čínský
astronom	astronom	k1gMnSc1	astronom
Gan	Gan	k1gFnSc2	Gan
De	De	k?	De
objevil	objevit	k5eAaPmAgInS	objevit
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
měsíců	měsíc	k1gInPc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
362	[number]	k4	362
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
pozorování	pozorování	k1gNnSc1	pozorování
doložitelné	doložitelný	k2eAgNnSc1d1	doložitelné
a	a	k8xC	a
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
předběhlo	předběhnout	k5eAaPmAgNnS	předběhnout
by	by	kYmCp3nS	by
Galilea	Galilea	k1gFnSc1	Galilea
o	o	k7c6	o
téměř	téměř	k6eAd1	téměř
dvě	dva	k4xCgNnPc4	dva
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Galileo	Galilea	k1gFnSc5	Galilea
současně	současně	k6eAd1	současně
tímto	tento	k3xDgInSc7	tento
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
objevil	objevit	k5eAaPmAgInS	objevit
nebeská	nebeský	k2eAgNnPc4d1	nebeské
tělesa	těleso	k1gNnPc4	těleso
obíhající	obíhající	k2eAgFnSc1d1	obíhající
kolem	kolem	k7c2	kolem
jiného	jiný	k2eAgInSc2d1	jiný
objektu	objekt	k1gInSc2	objekt
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
heliocentrický	heliocentrický	k2eAgInSc4d1	heliocentrický
model	model	k1gInSc4	model
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
Koperníka	Koperník	k1gMnSc2	Koperník
<g/>
.	.	kIx.	.
</s>
<s>
Galileova	Galileův	k2eAgFnSc1d1	Galileova
podpora	podpora	k1gFnSc1	podpora
nového	nový	k2eAgNnSc2d1	nové
pojetí	pojetí	k1gNnSc2	pojetí
chápání	chápání	k1gNnSc2	chápání
vesmíru	vesmír	k1gInSc6	vesmír
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
inkvizicí	inkvizice	k1gFnSc7	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
Domenico	Domenico	k6eAd1	Domenico
Cassini	Cassin	k2eAgMnPc1d1	Cassin
použil	použít	k5eAaPmAgMnS	použít
nový	nový	k2eAgInSc4d1	nový
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
objevil	objevit	k5eAaPmAgMnS	objevit
skvrny	skvrna	k1gFnSc2	skvrna
a	a	k8xC	a
barevné	barevný	k2eAgInPc1d1	barevný
pásy	pás	k1gInPc1	pás
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
zploštění	zploštění	k1gNnSc6	zploštění
planety	planeta	k1gFnSc2	planeta
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
určil	určit	k5eAaPmAgInS	určit
dobu	doba	k1gFnSc4	doba
otáčení	otáčení	k1gNnSc2	otáčení
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
si	se	k3xPyFc3	se
Cassini	Cassin	k2eAgMnPc1d1	Cassin
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
rotuje	rotovat	k5eAaImIp3nS	rotovat
různými	různý	k2eAgFnPc7d1	různá
rychlostmi	rychlost	k1gFnPc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
je	být	k5eAaImIp3nS	být
prominentní	prominentní	k2eAgInSc1d1	prominentní
oválný	oválný	k2eAgInSc1d1	oválný
útvar	útvar	k1gInSc1	útvar
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1664	[number]	k4	1664
Robertem	Robert	k1gMnSc7	Robert
Hookem	Hooek	k1gMnSc7	Hooek
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1665	[number]	k4	1665
Giovannim	Giovannim	k1gMnSc1	Giovannim
Cassinim	Cassinim	k1gMnSc1	Cassinim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozorování	pozorování	k1gNnPc1	pozorování
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
průkazná	průkazný	k2eAgFnSc1d1	průkazná
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
nákres	nákres	k1gInSc1	nákres
skvrny	skvrna	k1gFnSc2	skvrna
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
od	od	k7c2	od
Heinricha	Heinrich	k1gMnSc2	Heinrich
Schwabeho	Schwabe	k1gMnSc2	Schwabe
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1665	[number]	k4	1665
až	až	k8xS	až
1708	[number]	k4	1708
ztratila	ztratit	k5eAaPmAgFnS	ztratit
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
opět	opět	k6eAd1	opět
jasně	jasně	k6eAd1	jasně
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
její	její	k3xOp3gFnSc2	její
viditelnosti	viditelnost	k1gFnSc2	viditelnost
došlo	dojít	k5eAaPmAgNnS	dojít
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Giovanni	Giovanň	k1gMnSc5	Giovanň
Alfonso	Alfonso	k1gMnSc5	Alfonso
Borelli	Borell	k1gMnSc5	Borell
i	i	k9	i
Cassini	Cassin	k2eAgMnPc1d1	Cassin
pečlivě	pečlivě	k6eAd1	pečlivě
zaznamenávali	zaznamenávat	k5eAaImAgMnP	zaznamenávat
pohyby	pohyb	k1gInPc4	pohyb
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
předpovídat	předpovídat	k5eAaImF	předpovídat
přesné	přesný	k2eAgInPc4d1	přesný
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
měsíce	měsíc	k1gInPc1	měsíc
přejdou	přejít	k5eAaPmIp3nP	přejít
před	před	k7c7	před
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
jestli	jestli	k8xS	jestli
přejdou	přejít	k5eAaPmIp3nP	přejít
před	před	k7c7	před
planetou	planeta	k1gFnSc7	planeta
či	či	k8xC	či
za	za	k7c7	za
planetou	planeta	k1gFnSc7	planeta
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
předpokládané	předpokládaný	k2eAgInPc1d1	předpokládaný
časy	čas	k1gInPc1	čas
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
zpožďovaly	zpožďovat	k5eAaImAgFnP	zpožďovat
o	o	k7c4	o
17	[number]	k4	17
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ole	Ola	k1gFnSc3	Ola
Rø	Rø	k1gFnPc2	Rø
odvodil	odvodit	k5eAaPmAgMnS	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorování	pozorování	k1gNnSc1	pozorování
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
okamžité	okamžitý	k2eAgNnSc1d1	okamžité
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
využito	využít	k5eAaPmNgNnS	využít
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
Edward	Edward	k1gMnSc1	Edward
Emerson	Emerson	k1gMnSc1	Emerson
Barnard	Barnard	k1gMnSc1	Barnard
pátý	pátý	k4xOgInSc4	pátý
měsíc	měsíc	k1gInSc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
dalekohledu	dalekohled	k1gInSc2	dalekohled
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
910	[number]	k4	910
mm	mm	kA	mm
na	na	k7c6	na
Lickově	Lickův	k2eAgFnSc6d1	Lickova
observatoři	observatoř	k1gFnSc6	observatoř
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc4	objev
tohoto	tento	k3xDgInSc2	tento
relativně	relativně	k6eAd1	relativně
malého	malý	k2eAgInSc2d1	malý
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
svědčící	svědčící	k2eAgInSc1d1	svědčící
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
bystrém	bystrý	k2eAgInSc6d1	bystrý
zraku	zrak	k1gInSc6	zrak
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
rychle	rychle	k6eAd1	rychle
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
Amalthea	Amalthea	k1gMnSc1	Amalthea
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
současně	současně	k6eAd1	současně
i	i	k9	i
posledním	poslední	k2eAgInSc7d1	poslední
objevem	objev	k1gInSc7	objev
měsíce	měsíc	k1gInSc2	měsíc
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
přímého	přímý	k2eAgNnSc2d1	přímé
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
objevila	objevit	k5eAaPmAgFnS	objevit
až	až	k9	až
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
1	[number]	k4	1
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
Rupert	Rupert	k1gMnSc1	Rupert
Wildt	Wildt	k1gMnSc1	Wildt
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
ve	v	k7c6	v
spektrálních	spektrální	k2eAgFnPc6d1	spektrální
čarách	čára	k1gFnPc6	čára
Jupiteru	Jupiter	k1gInSc2	Jupiter
čpavek	čpavek	k1gInSc1	čpavek
a	a	k8xC	a
methan	methan	k1gInSc1	methan
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
dlouhotrvající	dlouhotrvající	k2eAgFnPc4d1	dlouhotrvající
anticyklóny	anticyklóna	k1gFnPc4	anticyklóna
vyskytující	vyskytující	k2eAgFnPc4d1	vyskytující
se	se	k3xPyFc4	se
poblíž	poblíž	k6eAd1	poblíž
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bílých	bílý	k2eAgInPc2d1	bílý
oválů	ovál	k1gInPc2	ovál
byly	být	k5eAaImAgFnP	být
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
po	po	k7c6	po
několika	několik	k4yIc6	několik
desetiletích	desetiletí	k1gNnPc6	desetiletí
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
nacházely	nacházet	k5eAaImAgFnP	nacházet
individuálně	individuálně	k6eAd1	individuálně
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přibližovaly	přibližovat	k5eAaImAgFnP	přibližovat
<g/>
.	.	kIx.	.
</s>
<s>
Nespojily	spojit	k5eNaPmAgInP	spojit
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
pohltily	pohltit	k5eAaPmAgInP	pohltit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
struktura	struktura	k1gFnSc1	struktura
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Oval	ovalit	k5eAaPmRp2nS	ovalit
BA	ba	k9	ba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
Bernard	Bernard	k1gMnSc1	Bernard
Burke	Burk	k1gInSc2	Burk
a	a	k8xC	a
Keneth	Keneth	k1gInSc1	Keneth
Franklin	Franklina	k1gFnPc2	Franklina
objevili	objevit	k5eAaPmAgMnP	objevit
záblesky	záblesk	k1gInPc4	záblesk
radiového	radiový	k2eAgInSc2d1	radiový
signálu	signál	k1gInSc2	signál
přicházejícího	přicházející	k2eAgInSc2d1	přicházející
z	z	k7c2	z
Jupiteru	Jupiter	k1gInSc2	Jupiter
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
22,2	[number]	k4	22,2
MHz	Mhz	kA	Mhz
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
záblesky	záblesk	k1gInPc1	záblesk
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
rotace	rotace	k1gFnSc2	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
taktéž	taktéž	k?	taktéž
vědci	vědec	k1gMnPc1	vědec
využili	využít	k5eAaPmAgMnP	využít
pro	pro	k7c4	pro
zpřesnění	zpřesnění	k1gNnSc4	zpřesnění
doby	doba	k1gFnSc2	doba
rotace	rotace	k1gFnSc2	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
z	z	k7c2	z
Jupiteru	Jupiter	k1gInSc2	Jupiter
přicházejí	přicházet	k5eAaImIp3nP	přicházet
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
formách	forma	k1gFnPc6	forma
<g/>
:	:	kIx,	:
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
záblesky	záblesk	k1gInPc4	záblesk
(	(	kIx(	(
<g/>
L-záblesky	Lábleska	k1gFnPc4	L-zábleska
<g/>
)	)	kIx)	)
trvající	trvající	k2eAgFnPc4d1	trvající
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
krátké	krátký	k2eAgInPc4d1	krátký
záblesky	záblesk	k1gInPc4	záblesk
(	(	kIx(	(
<g/>
či	či	k8xC	či
S-záblesky	Sábleska	k1gFnSc2	S-zábleska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trvají	trvat	k5eAaImIp3nP	trvat
jen	jen	k9	jen
setiny	setina	k1gFnPc4	setina
vteřiny	vteřina	k1gFnPc4	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Jupiteru	Jupiter	k1gInSc2	Jupiter
vycházejí	vycházet	k5eAaImIp3nP	vycházet
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
radiového	radiový	k2eAgInSc2d1	radiový
signálu	signál	k1gInSc2	signál
<g/>
:	:	kIx,	:
dekametrické	dekametrický	k2eAgInPc4d1	dekametrický
radiové	radiový	k2eAgInPc4d1	radiový
záblesky	záblesk	k1gInPc4	záblesk
(	(	kIx(	(
<g/>
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
rotací	rotace	k1gFnSc7	rotace
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
interakcemi	interakce	k1gFnPc7	interakce
měsíce	měsíc	k1gInPc4	měsíc
Io	Io	k1gMnSc1	Io
s	s	k7c7	s
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
Jupiteru	Jupiter	k1gInSc2	Jupiter
decimetrická	decimetrický	k2eAgFnSc1d1	decimetrický
rádiová	rádiový	k2eAgFnSc1d1	rádiová
emise	emise	k1gFnSc1	emise
(	(	kIx(	(
<g/>
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
prvně	prvně	k?	prvně
pozorována	pozorován	k2eAgFnSc1d1	pozorována
Frankem	Frank	k1gMnSc7	Frank
Drakem	drak	k1gMnSc7	drak
a	a	k8xC	a
Heinem	Hein	k1gMnSc7	Hein
Hvatumem	Hvatum	k1gInSc7	Hvatum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
tohoto	tento	k3xDgInSc2	tento
signálu	signál	k1gInSc2	signál
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
pásu	pás	k1gInSc2	pás
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc2	rovník
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
ohonu	ohon	k1gInSc2	ohon
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ho	on	k3xPp3gMnSc4	on
cyklotronové	cyklotronový	k2eAgNnSc1d1	cyklotronový
záření	záření	k1gNnSc1	záření
elektronů	elektron	k1gInPc2	elektron
urychlovaných	urychlovaný	k2eAgInPc2d1	urychlovaný
v	v	k7c6	v
magnetickém	magnetický	k2eAgNnSc6d1	magnetické
poli	pole	k1gNnSc6	pole
planety	planeta	k1gFnSc2	planeta
tepelné	tepelný	k2eAgNnSc1d1	tepelné
záření	záření	k1gNnSc1	záření
vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
proletělo	proletět	k5eAaPmAgNnS	proletět
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
několik	několik	k4yIc4	několik
automatických	automatický	k2eAgFnPc2d1	automatická
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Lety	let	k1gInPc4	let
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
planetám	planeta	k1gFnPc3	planeta
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
potřebné	potřebný	k2eAgFnSc2d1	potřebná
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
uniknout	uniknout	k5eAaPmF	uniknout
tělesu	těleso	k1gNnSc3	těleso
z	z	k7c2	z
gravitačního	gravitační	k2eAgInSc2d1	gravitační
vlivu	vliv	k1gInSc2	vliv
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
dosažení	dosažení	k1gNnSc2	dosažení
cílové	cílový	k2eAgFnSc2d1	cílová
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
Jupiteru	Jupiter	k1gInSc2	Jupiter
musí	muset	k5eAaImIp3nP	muset
tělesa	těleso	k1gNnPc1	těleso
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rychlosti	rychlost	k1gFnSc3	rychlost
delta-v	delta	k1gInSc1	delta-v
9,2	[number]	k4	9,2
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
9,7	[number]	k4	9,7
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
potřebnou	potřebný	k2eAgFnSc7d1	potřebná
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
pozemské	pozemský	k2eAgNnSc4d1	pozemské
nízké	nízký	k2eAgFnPc4d1	nízká
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
Jupiteru	Jupiter	k1gInSc2	Jupiter
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
gravitačního	gravitační	k2eAgInSc2d1	gravitační
praku	prak	k1gInSc2	prak
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
energetické	energetický	k2eAgInPc4d1	energetický
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
sondy	sonda	k1gFnPc4	sonda
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
směřují	směřovat	k5eAaImIp3nP	směřovat
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
gravitačního	gravitační	k2eAgInSc2d1	gravitační
praku	prak	k1gInSc2	prak
tak	tak	k6eAd1	tak
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
snížení	snížení	k1gNnSc3	snížení
nákladů	náklad	k1gInPc2	náklad
sond	sonda	k1gFnPc2	sonda
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
násobně	násobně	k6eAd1	násobně
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gInSc2	jejich
letu	let	k1gInSc2	let
a	a	k8xC	a
dosažení	dosažení	k1gNnSc4	dosažení
cílové	cílový	k2eAgFnSc2d1	cílová
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
provedlo	provést	k5eAaPmAgNnS	provést
několik	několik	k4yIc4	několik
sond	sonda	k1gFnPc2	sonda
gravitační	gravitační	k2eAgInSc4d1	gravitační
manévr	manévr	k1gInSc4	manévr
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
přineslo	přinést	k5eAaPmAgNnS	přinést
množství	množství	k1gNnSc1	množství
příležitostí	příležitost	k1gFnPc2	příležitost
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
této	tento	k3xDgFnSc2	tento
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
pozemskou	pozemský	k2eAgFnSc7d1	pozemská
sondou	sonda	k1gFnSc7	sonda
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
a	a	k8xC	a
11	[number]	k4	11
pořídily	pořídit	k5eAaPmAgFnP	pořídit
první	první	k4xOgFnPc4	první
barevné	barevný	k2eAgFnPc4d1	barevná
snímky	snímka	k1gFnPc4	snímka
Jupiterovy	Jupiterův	k2eAgFnSc2d1	Jupiterova
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
několika	několik	k4yIc3	několik
jeho	jeho	k3xOp3gMnPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
zblízka	zblízka	k6eAd1	zblízka
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
nacházejí	nacházet	k5eAaImIp3nP	nacházet
značně	značně	k6eAd1	značně
silnější	silný	k2eAgInPc1d2	silnější
radiační	radiační	k2eAgInPc1d1	radiační
pásy	pás	k1gInPc1	pás
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
obě	dva	k4xCgFnPc1	dva
sondy	sonda	k1gFnPc1	sonda
přežily	přežít	k5eAaPmAgFnP	přežít
průlet	průlet	k1gInSc4	průlet
radiační	radiační	k2eAgInSc4d1	radiační
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
odhadu	odhad	k1gInSc2	odhad
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Joviánského	Joviánský	k2eAgInSc2d1	Joviánský
systému	systém	k1gInSc2	systém
byly	být	k5eAaImAgFnP	být
následně	následně	k6eAd1	následně
využity	využít	k5eAaPmNgFnP	využít
změny	změna	k1gFnPc1	změna
trajektorie	trajektorie	k1gFnSc2	trajektorie
jejich	jejich	k3xOp3gInSc3	jejich
letu	let	k1gInSc3	let
<g/>
.	.	kIx.	.
</s>
<s>
Průlet	průlet	k1gInSc1	průlet
také	také	k9	také
pomohl	pomoct	k5eAaPmAgInS	pomoct
zpřesnit	zpřesnit	k5eAaPmF	zpřesnit
velikost	velikost	k1gFnSc4	velikost
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
polárního	polární	k2eAgNnSc2d1	polární
zploštění	zploštění	k1gNnSc2	zploštění
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
porozumění	porozumění	k1gNnSc3	porozumění
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Galileovým	Galileův	k2eAgInPc3d1	Galileův
měsícům	měsíc	k1gInPc3	měsíc
přispěly	přispět	k5eAaPmAgInP	přispět
sondy	sonda	k1gFnPc4	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
prstence	prstenec	k1gInPc4	prstenec
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velká	velký	k2eAgFnSc1d1	velká
rudá	rudý	k2eAgFnSc1d1	rudá
skvrna	skvrna	k1gFnSc1	skvrna
je	být	k5eAaImIp3nS	být
anticyklóna	anticyklóna	k1gFnSc1	anticyklóna
<g/>
.	.	kIx.	.
</s>
<s>
Porovnání	porovnání	k1gNnSc1	porovnání
snímků	snímek	k1gInPc2	snímek
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
barva	barva	k1gFnSc1	barva
skvrny	skvrna	k1gFnSc2	skvrna
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
průletu	průlet	k1gInSc2	průlet
sond	sonda	k1gFnPc2	sonda
Pioneer	Pioneer	kA	Pioneer
změnila	změnit	k5eAaPmAgFnS	změnit
z	z	k7c2	z
oranžové	oranžový	k2eAgFnSc2d1	oranžová
barvy	barva	k1gFnSc2	barva
na	na	k7c4	na
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k6eAd1	okolo
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
měsíce	měsíc	k1gInPc4	měsíc
Io	Io	k1gFnSc2	Io
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
ohon	ohon	k1gInSc1	ohon
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
i	i	k9	i
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
zrovna	zrovna	k6eAd1	zrovna
během	běh	k1gInSc7	běh
erupcí	erupce	k1gFnPc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
sondy	sonda	k1gFnPc1	sonda
přeletěly	přeletět	k5eAaPmAgFnP	přeletět
planetu	planeta	k1gFnSc4	planeta
a	a	k8xC	a
ocitly	ocitnout	k5eAaPmAgInP	ocitnout
se	se	k3xPyFc4	se
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
pozorovaly	pozorovat	k5eAaImAgInP	pozorovat
blesky	blesk	k1gInPc1	blesk
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
straně	strana	k1gFnSc6	strana
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
misí	mise	k1gFnSc7	mise
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jupiteru	Jupiter	k1gInSc2	Jupiter
byla	být	k5eAaImAgFnS	být
sluneční	sluneční	k2eAgFnSc1d1	sluneční
sonda	sonda	k1gFnSc1	sonda
Ulysses	Ulyssesa	k1gFnPc2	Ulyssesa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provedla	provést	k5eAaPmAgFnS	provést
průlet	průlet	k1gInSc4	průlet
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
polární	polární	k2eAgFnSc4d1	polární
orbitu	orbita	k1gFnSc4	orbita
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sonda	sonda	k1gFnSc1	sonda
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ale	ale	k8xC	ale
sonda	sonda	k1gFnSc1	sonda
nebyla	být	k5eNaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
žádnými	žádný	k3yNgFnPc7	žádný
kamerami	kamera	k1gFnPc7	kamera
<g/>
,	,	kIx,	,
z	z	k7c2	z
mise	mise	k1gFnSc2	mise
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
snímky	snímek	k1gInPc1	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
průlet	průlet	k1gInSc1	průlet
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
značně	značně	k6eAd1	značně
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
sonda	sonda	k1gFnSc1	sonda
Cassini	Cassin	k2eAgMnPc1d1	Cassin
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Saturnu	Saturn	k1gInSc3	Saturn
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
během	během	k7c2	během
čehož	což	k3yRnSc2	což
pořídila	pořídit	k5eAaPmAgFnS	pořídit
několik	několik	k4yIc4	několik
snímků	snímek	k1gInPc2	snímek
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
pořídila	pořídit	k5eAaPmAgFnS	pořídit
sonda	sonda	k1gFnSc1	sonda
snímek	snímek	k1gInSc1	snímek
měsíce	měsíc	k1gInSc2	měsíc
Himalia	Himalium	k1gNnSc2	Himalium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozlišení	rozlišení	k1gNnSc1	rozlišení
snímku	snímek	k1gInSc2	snímek
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
nějaké	nějaký	k3yIgInPc4	nějaký
detaily	detail	k1gInPc4	detail
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
New	New	k1gFnSc2	New
Horizons	Horizonsa	k1gFnPc2	Horizonsa
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Plutu	plut	k1gInSc3	plut
proletěla	proletět	k5eAaPmAgFnS	proletět
okolo	okolo	k7c2	okolo
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
když	když	k8xS	když
využila	využít	k5eAaPmAgFnS	využít
jeho	jeho	k3xOp3gFnSc4	jeho
gravitaci	gravitace	k1gFnSc4	gravitace
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
se	se	k3xPyFc4	se
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Kamera	kamera	k1gFnSc1	kamera
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sondy	sonda	k1gFnSc2	sonda
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
a	a	k8xC	a
měření	měření	k1gNnSc4	měření
výtrysků	výtrysk	k1gInPc2	výtrysk
plazmatu	plazma	k1gNnSc2	plazma
ze	z	k7c2	z
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
Io	Io	k1gFnSc6	Io
a	a	k8xC	a
současně	současně	k6eAd1	současně
studovala	studovat	k5eAaImAgFnS	studovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
velké	velký	k2eAgInPc4d1	velký
Galileovy	Galileův	k2eAgInPc4d1	Galileův
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
vnější	vnější	k2eAgInPc4d1	vnější
měsíce	měsíc	k1gInPc4	měsíc
Himalia	Himalium	k1gNnSc2	Himalium
a	a	k8xC	a
Elara	Elaro	k1gNnSc2	Elaro
<g/>
.	.	kIx.	.
</s>
<s>
Snímkování	snímkování	k1gNnSc1	snímkování
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
systému	systém	k1gInSc2	systém
začalo	začít	k5eAaPmAgNnS	začít
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
jediná	jediný	k2eAgFnSc1d1	jediná
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obíhala	obíhat	k5eAaImAgFnS	obíhat
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sonda	sonda	k1gFnSc1	sonda
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tak	tak	k6eAd1	tak
činila	činit	k5eAaImAgFnS	činit
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
navedena	navést	k5eAaPmNgNnP	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
následně	následně	k6eAd1	následně
obíhala	obíhat	k5eAaImAgFnS	obíhat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgFnPc2	který
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
mnoho	mnoho	k4c4	mnoho
průletů	průlet	k1gInPc2	průlet
kolem	kolem	k7c2	kolem
Galileových	Galileův	k2eAgInPc2d1	Galileův
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
měsíce	měsíc	k1gInSc2	měsíc
Amalthea	Amalthe	k1gInSc2	Amalthe
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
byla	být	k5eAaImAgFnS	být
současně	současně	k6eAd1	současně
svědkem	svědek	k1gMnSc7	svědek
dopadu	dopad	k1gInSc2	dopad
komety	kometa	k1gFnSc2	kometa
Shoemaker-Levy	Shoemaker-Leva	k1gFnSc2	Shoemaker-Leva
9	[number]	k4	9
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
získané	získaný	k2eAgNnSc1d1	získané
množství	množství	k1gNnSc1	množství
dat	datum	k1gNnPc2	datum
bylo	být	k5eAaImAgNnS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
,	,	kIx,	,
misi	mise	k1gFnSc6	mise
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
špatně	špatně	k6eAd1	špatně
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
parabolická	parabolický	k2eAgFnSc1d1	parabolická
anténa	anténa	k1gFnSc1	anténa
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
množství	množství	k1gNnSc1	množství
přenesených	přenesený	k2eAgFnPc2d1	přenesená
informací	informace	k1gFnPc2	informace
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
obrázků	obrázek	k1gInPc2	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
atmosférická	atmosférický	k2eAgFnSc1d1	atmosférická
sonda	sonda	k1gFnSc1	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
následně	následně	k6eAd1	následně
na	na	k7c6	na
padáku	padák	k1gInSc6	padák
padala	padat	k5eAaImAgFnS	padat
150	[number]	k4	150
km	km	kA	km
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
57,6	[number]	k4	57,6
minuty	minuta	k1gFnSc2	minuta
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yQgFnPc2	který
získávala	získávat	k5eAaImAgFnS	získávat
data	datum	k1gNnSc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
sonda	sonda	k1gFnSc1	sonda
rozdrcena	rozdrtit	k5eAaPmNgFnS	rozdrtit
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
panuje	panovat	k5eAaImIp3nS	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Nefunkční	funkční	k2eNgFnSc1d1	nefunkční
sonda	sonda	k1gFnSc1	sonda
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
padala	padat	k5eAaImAgFnS	padat
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
celá	celá	k1gFnSc1	celá
roztavila	roztavit	k5eAaPmAgFnS	roztavit
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vypařila	vypařit	k5eAaPmAgFnS	vypařit
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
osud	osud	k1gInSc1	osud
postihl	postihnout	k5eAaPmAgInS	postihnout
i	i	k9	i
sondu	sonda	k1gFnSc4	sonda
Galileo	Galilea	k1gFnSc5	Galilea
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jejího	její	k3xOp3gNnSc2	její
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
uměle	uměle	k6eAd1	uměle
navedena	navést	k5eAaPmNgFnS	navést
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
rychlostí	rychlost	k1gFnSc7	rychlost
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Takto	takto	k6eAd1	takto
řízené	řízený	k2eAgNnSc1d1	řízené
zničení	zničení	k1gNnSc1	zničení
sondy	sonda	k1gFnSc2	sonda
mělo	mít	k5eAaImAgNnS	mít
zabránit	zabránit	k5eAaPmF	zabránit
potenciální	potenciální	k2eAgFnSc4d1	potenciální
kontaminaci	kontaminace	k1gFnSc4	kontaminace
Europy	Europa	k1gFnSc2	Europa
pozemským	pozemský	k2eAgInSc7d1	pozemský
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mohl	moct	k5eAaImAgInS	moct
přežít	přežít	k5eAaPmF	přežít
sterilizaci	sterilizace	k1gFnSc4	sterilizace
sondy	sonda	k1gFnSc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
sonda	sonda	k1gFnSc1	sonda
Juno	Juno	k1gFnSc1	Juno
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příletu	přílet	k1gInSc6	přílet
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
polární	polární	k2eAgFnSc4d1	polární
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
studuje	studovat	k5eAaImIp3nS	studovat
gravitační	gravitační	k2eAgNnSc1d1	gravitační
a	a	k8xC	a
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
její	její	k3xOp3gFnSc2	její
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnPc1	měření
budou	být	k5eAaImBp3nP	být
probíhat	probíhat	k5eAaImF	probíhat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgNnSc2	který
sonda	sonda	k1gFnSc1	sonda
vykoná	vykonat	k5eAaPmIp3nS	vykonat
33	[number]	k4	33
oběhů	oběh	k1gInPc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
bude	být	k5eAaImBp3nS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
řízeným	řízený	k2eAgInSc7d1	řízený
zánikem	zánik	k1gInSc7	zánik
sondy	sonda	k1gFnSc2	sonda
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
obří	obří	k2eAgFnSc2d1	obří
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
ESA	eso	k1gNnPc1	eso
společně	společně	k6eAd1	společně
s	s	k7c7	s
NASA	NASA	kA	NASA
plánuje	plánovat	k5eAaImIp3nS	plánovat
misi	mise	k1gFnSc4	mise
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
EJSM	EJSM	kA	EJSM
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
mezi	mezi	k7c7	mezi
agenturami	agentura	k1gFnPc7	agentura
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
dostane	dostat	k5eAaPmIp3nS	dostat
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
misí	mise	k1gFnSc7	mise
Titan	titan	k1gInSc4	titan
Saturn	Saturn	k1gInSc1	Saturn
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
ESA	eso	k1gNnSc2	eso
bude	být	k5eAaImBp3nS	být
i	i	k9	i
tak	tak	k6eAd1	tak
nadále	nadále	k6eAd1	nadále
čelit	čelit	k5eAaImF	čelit
konkurenci	konkurence	k1gFnSc4	konkurence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ostatních	ostatní	k2eAgInPc2d1	ostatní
financovaných	financovaný	k2eAgInPc2d1	financovaný
projektů	projekt	k1gInPc2	projekt
ESA	eso	k1gNnSc2	eso
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
případný	případný	k2eAgInSc1d1	případný
start	start	k1gInSc1	start
měl	mít	k5eAaImAgInS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
části	část	k1gFnSc2	část
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
NASA	NASA	kA	NASA
zvané	zvaný	k2eAgFnSc2d1	zvaná
Jupiter	Jupiter	k1gInSc4	Jupiter
Europa	Europa	k1gFnSc1	Europa
Orbiter	Orbitrum	k1gNnPc2	Orbitrum
a	a	k8xC	a
částí	část	k1gFnPc2	část
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
ESA	eso	k1gNnSc2	eso
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
modulu	modul	k1gInSc2	modul
Jupiter	Jupiter	k1gMnSc1	Jupiter
Ganymede	Ganymed	k1gMnSc5	Ganymed
Orbiter	Orbiter	k1gInSc4	Orbiter
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
i	i	k8xC	i
evropská	evropský	k2eAgFnSc1d1	Evropská
sonda	sonda	k1gFnSc1	sonda
budou	být	k5eAaImBp3nP	být
zcela	zcela	k6eAd1	zcela
oddělené	oddělený	k2eAgInPc1d1	oddělený
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
odstartují	odstartovat	k5eAaPmIp3nP	odstartovat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
doletí	doletět	k5eAaPmIp3nP	doletět
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
sond	sonda	k1gFnPc2	sonda
JEO	JEO	kA	JEO
a	a	k8xC	a
JGO	JGO	kA	JGO
bude	být	k5eAaImBp3nS	být
primárně	primárně	k6eAd1	primárně
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
sekundárně	sekundárně	k6eAd1	sekundárně
na	na	k7c4	na
další	další	k2eAgMnPc4d1	další
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
tedy	tedy	k9	tedy
bude	být	k5eAaImBp3nS	být
zkoumat	zkoumat	k5eAaImF	zkoumat
dva	dva	k4xCgInPc4	dva
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
velkých	velký	k2eAgInPc2d1	velký
měsíců	měsíc	k1gInPc2	měsíc
největší	veliký	k2eAgFnSc2d3	veliký
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
existuje	existovat	k5eAaImIp3nS	existovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
Europy	Europa	k1gFnSc2	Europa
<g/>
,	,	kIx,	,
Ganymedu	Ganymed	k1gMnSc3	Ganymed
a	a	k8xC	a
Callista	Callista	k1gMnSc1	Callista
nacházejí	nacházet	k5eAaImIp3nP	nacházet
oceány	oceán	k1gInPc1	oceán
tvořené	tvořený	k2eAgNnSc1d1	tvořené
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
ledové	ledový	k2eAgInPc4d1	ledový
měsíce	měsíc	k1gInPc4	měsíc
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
vědců	vědec	k1gMnPc2	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
rozpočtem	rozpočet	k1gInSc7	rozpočet
způsobily	způsobit	k5eAaPmAgInP	způsobit
zpoždění	zpoždění	k1gNnSc4	zpoždění
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
světů	svět	k1gInPc2	svět
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
mise	mise	k1gFnSc2	mise
Jupiter	Jupiter	k1gInSc1	Jupiter
Icy	Icy	k1gMnSc1	Icy
Moons	Moonsa	k1gFnPc2	Moonsa
Orbiter	Orbiter	k1gMnSc1	Orbiter
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
ESA	eso	k1gNnSc2	eso
zvažovala	zvažovat	k5eAaImAgFnS	zvažovat
misi	mise	k1gFnSc4	mise
Jovian	Joviana	k1gFnPc2	Joviana
Europa	Europa	k1gFnSc1	Europa
Orbiter	Orbiter	k1gInSc1	Orbiter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
misí	mise	k1gFnSc7	mise
Europa	Europa	k1gFnSc1	Europa
Jupiter	Jupiter	k1gMnSc1	Jupiter
System	Syst	k1gInSc7	Syst
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
EJSM	EJSM	kA	EJSM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měsíce	měsíc	k1gInSc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
má	mít	k5eAaImIp3nS	mít
63	[number]	k4	63
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
47	[number]	k4	47
jich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
měsíce	měsíc	k1gInPc1	měsíc
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
největší	veliký	k2eAgInPc1d3	veliký
měsíce	měsíc	k1gInPc1	měsíc
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
galileovské	galileovský	k2eAgInPc4d1	galileovský
měsíce	měsíc	k1gInPc4	měsíc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Io	Io	k1gMnSc4	Io
<g/>
,	,	kIx,	,
Europa	Europ	k1gMnSc4	Europ
<g/>
,	,	kIx,	,
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
Io	Io	k1gFnSc2	Io
<g/>
,	,	kIx,	,
Europy	Europa	k1gFnSc2	Europa
a	a	k8xC	a
Ganymeda	Ganymed	k1gMnSc4	Ganymed
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
dráhovou	dráhový	k2eAgFnSc4d1	dráhová
rezonanci	rezonance	k1gFnSc4	rezonance
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Laplaceova	Laplaceův	k2eAgFnSc1d1	Laplaceova
rezonance	rezonance	k1gFnSc1	rezonance
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c4	na
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
oběhy	oběh	k1gInPc4	oběh
Io	Io	k1gFnPc2	Io
kolem	kolem	k7c2	kolem
Jupiteru	Jupiter	k1gInSc2	Jupiter
uskuteční	uskutečnit	k5eAaPmIp3nS	uskutečnit
Europa	Europa	k1gFnSc1	Europa
přesně	přesně	k6eAd1	přesně
dva	dva	k4xCgInPc4	dva
oběhy	oběh	k1gInPc4	oběh
a	a	k8xC	a
Ganymed	Ganymed	k1gMnSc1	Ganymed
přesně	přesně	k6eAd1	přesně
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rezonance	rezonance	k1gFnSc1	rezonance
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
gravitační	gravitační	k2eAgInSc4d1	gravitační
efekt	efekt	k1gInSc4	efekt
deformující	deformující	k2eAgFnSc2d1	deformující
dráhy	dráha	k1gFnSc2	dráha
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
eliptických	eliptický	k2eAgFnPc2d1	eliptická
křivek	křivka	k1gFnPc2	křivka
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
měsíců	měsíc	k1gInPc2	měsíc
obdrží	obdržet	k5eAaPmIp3nS	obdržet
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
sousedů	soused	k1gMnPc2	soused
tah	tah	k1gInSc4	tah
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
slapové	slapový	k2eAgFnSc2d1	slapová
síly	síla	k1gFnSc2	síla
Jupiteru	Jupiter	k1gInSc2	Jupiter
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
držet	držet	k5eAaImF	držet
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
kruhových	kruhový	k2eAgFnPc6d1	kruhová
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přetahovaná	přetahovaný	k2eAgFnSc1d1	přetahovaná
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
změny	změna	k1gFnPc4	změna
tvarů	tvar	k1gInPc2	tvar
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
Jupiterova	Jupiterův	k2eAgFnSc1d1	Jupiterova
gravitace	gravitace	k1gFnSc1	gravitace
napíná	napínat	k5eAaImIp3nS	napínat
měsíce	měsíc	k1gInPc4	měsíc
mnohem	mnohem	k6eAd1	mnohem
silněji	silně	k6eAd2	silně
v	v	k7c6	v
jemu	on	k3xPp3gInSc3	on
bližší	blízký	k2eAgFnPc4d2	bližší
části	část	k1gFnPc4	část
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
a	a	k8xC	a
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
opětovné	opětovný	k2eAgNnSc1d1	opětovné
smrštění	smrštění	k1gNnSc1	smrštění
do	do	k7c2	do
kulovitějšího	kulovitý	k2eAgInSc2d2	kulovitý
tvaru	tvar	k1gInSc2	tvar
ve	v	k7c6	v
vzdálenější	vzdálený	k2eAgFnSc6d2	vzdálenější
části	část	k1gFnSc6	část
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
tvaru	tvar	k1gInSc2	tvar
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
slapové	slapový	k2eAgNnSc4d1	slapové
ohřívání	ohřívání	k1gNnSc4	ohřívání
jader	jádro	k1gNnPc2	jádro
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nejdramatičtěji	dramaticky	k6eAd3	dramaticky
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
projevuje	projevovat	k5eAaImIp3nS	projevovat
neobyčejnou	obyčejný	k2eNgFnSc7d1	neobyčejná
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
aktivitou	aktivita	k1gFnSc7	aktivita
Io	Io	k1gFnSc2	Io
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
méně	málo	k6eAd2	málo
dramaticky	dramaticky	k6eAd1	dramaticky
geologicky	geologicky	k6eAd1	geologicky
mladým	mladý	k2eAgInSc7d1	mladý
povrchem	povrch	k1gInSc7	povrch
Europy	Europa	k1gFnSc2	Europa
značícím	značící	k2eAgFnPc3d1	značící
nedávné	dávný	k2eNgNnSc1d1	nedávné
zalití	zalití	k1gNnSc1	zalití
povrchu	povrch	k1gInSc2	povrch
tekutou	tekutý	k2eAgFnSc7d1	tekutá
hmotou	hmota	k1gFnSc7	hmota
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
věk	věk	k1gInSc1	věk
povrchu	povrch	k1gInSc2	povrch
Europy	Europa	k1gFnSc2	Europa
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
až	až	k9	až
180	[number]	k4	180
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jupiterovy	Jupiterův	k2eAgInPc4d1	Jupiterův
měsíce	měsíc	k1gInPc4	měsíc
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
skupin	skupina	k1gFnPc2	skupina
po	po	k7c6	po
čtyřech	čtyři	k4xCgMnPc6	čtyři
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
poslední	poslední	k2eAgInPc4d1	poslední
objevy	objev	k1gInPc4	objev
mnoha	mnoho	k4c2	mnoho
nových	nový	k2eAgFnPc2d1	nová
malých	malá	k1gFnPc2	malá
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
měsíců	měsíc	k1gInPc2	měsíc
toto	tento	k3xDgNnSc4	tento
rozdělení	rozdělení	k1gNnSc4	rozdělení
zkomplikovaly	zkomplikovat	k5eAaPmAgInP	zkomplikovat
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
nyní	nyní	k6eAd1	nyní
členění	členění	k1gNnSc4	členění
na	na	k7c4	na
šest	šest	k4xCc4	šest
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
různorodější	různorodý	k2eAgInPc1d2	různorodější
než	než	k8xS	než
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
hlubší	hluboký	k2eAgInSc4d2	hlubší
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
ze	z	k7c2	z
společného	společný	k2eAgInSc2d1	společný
základu	základ	k1gInSc2	základ
-	-	kIx~	-
většího	veliký	k2eAgInSc2d2	veliký
měsíce	měsíc	k1gInSc2	měsíc
nebo	nebo	k8xC	nebo
zachyceného	zachycený	k2eAgNnSc2d1	zachycené
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
na	na	k7c4	na
více	hodně	k6eAd2	hodně
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
nepravidelné	pravidelný	k2eNgInPc4d1	nepravidelný
a	a	k8xC	a
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
skupina	skupina	k1gFnSc1	skupina
osmi	osm	k4xCc2	osm
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
kruhovou	kruhový	k2eAgFnSc4d1	kruhová
dráhu	dráha	k1gFnSc4	dráha
poblíž	poblíž	k7c2	poblíž
roviny	rovina	k1gFnSc2	rovina
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
měsíce	měsíc	k1gInPc1	měsíc
neznámého	známý	k2eNgInSc2d1	neznámý
počtu	počet	k1gInSc2	počet
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
drahách	draha	k1gFnPc6	draha
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
zachycena	zachycen	k2eAgFnSc1d1	zachycena
a	a	k8xC	a
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgInPc1d1	podobný
parametry	parametr	k1gInPc1	parametr
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
fragmenty	fragment	k1gInPc1	fragment
většího	veliký	k2eAgInSc2d2	veliký
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
silou	síla	k1gFnSc7	síla
Jupiteru	Jupiter	k1gInSc2	Jupiter
rozdrcen	rozdrtit	k5eAaPmNgInS	rozdrtit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
přispěl	přispět	k5eAaPmAgInS	přispět
Jupiter	Jupiter	k1gInSc1	Jupiter
gravitačním	gravitační	k2eAgNnSc7d1	gravitační
působením	působení	k1gNnSc7	působení
k	k	k7c3	k
zformování	zformování	k1gNnSc3	zformování
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
většiny	většina	k1gFnSc2	většina
planet	planeta	k1gFnPc2	planeta
leží	ležet	k5eAaImIp3nS	ležet
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
oběžné	oběžný	k2eAgFnSc3d1	oběžná
rovině	rovina	k1gFnSc3	rovina
Jupiteru	Jupiter	k1gInSc2	Jupiter
než	než	k8xS	než
rovníkové	rovníkový	k2eAgFnSc3d1	Rovníková
rovině	rovina	k1gFnSc3	rovina
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
planetou	planeta	k1gFnSc7	planeta
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
drahou	draha	k1gFnSc7	draha
ležící	ležící	k2eAgFnSc2d1	ležící
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
slunečního	sluneční	k2eAgInSc2d1	sluneční
rovníku	rovník	k1gInSc2	rovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kirkwoodova	Kirkwoodův	k2eAgFnSc1d1	Kirkwoodův
mezera	mezera	k1gFnSc1	mezera
v	v	k7c6	v
pásu	pás	k1gInSc6	pás
asteroidů	asteroid	k1gInPc2	asteroid
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
Jupiterem	Jupiter	k1gInSc7	Jupiter
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mohl	moct	k5eAaImAgInS	moct
způsobit	způsobit	k5eAaPmF	způsobit
i	i	k9	i
období	období	k1gNnSc6	období
pozdního	pozdní	k2eAgNnSc2d1	pozdní
velkého	velký	k2eAgNnSc2d1	velké
bombardování	bombardování	k1gNnSc2	bombardování
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
ovládá	ovládat	k5eAaImIp3nS	ovládat
kromě	kromě	k7c2	kromě
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
i	i	k8xC	i
množství	množství	k1gNnSc4	množství
asteroidů	asteroid	k1gInPc2	asteroid
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c4	v
Lagrangeovo	Lagrangeův	k2eAgNnSc4d1	Lagrangeův
bodě	bod	k1gInSc6	bod
před	před	k7c7	před
i	i	k8xC	i
za	za	k7c7	za
Jupiterem	Jupiter	k1gMnSc7	Jupiter
a	a	k8xC	a
které	který	k3yRgFnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k6eAd1	kolem
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
asteroidy	asteroida	k1gFnPc1	asteroida
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
Trojáni	Troján	k2eAgMnPc1d1	Troján
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
asteroid	asteroid	k1gInSc1	asteroid
588	[number]	k4	588
Achilles	Achilles	k1gMnSc1	Achilles
byl	být	k5eAaImAgMnS	být
objeven	objevit	k5eAaPmNgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
Maxem	Max	k1gMnSc7	Max
Wolfem	Wolf	k1gMnSc7	Wolf
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc1	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
624	[number]	k4	624
Hektor	Hektor	k1gMnSc1	Hektor
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
obrovské	obrovský	k2eAgNnSc4d1	obrovské
gravitační	gravitační	k2eAgNnSc4d1	gravitační
působení	působení	k1gNnSc4	působení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
značnou	značný	k2eAgFnSc4d1	značná
gravitační	gravitační	k2eAgFnSc4d1	gravitační
studni	studně	k1gFnSc4	studně
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
vysavač	vysavač	k1gInSc1	vysavač
vakua	vakuum	k1gNnSc2	vakuum
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgInSc7d3	nejčastější
cílem	cíl	k1gInSc7	cíl
dopadů	dopad	k1gInPc2	dopad
komet	kometa	k1gFnPc2	kometa
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
funguje	fungovat	k5eAaImIp3nS	fungovat
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
planety	planeta	k1gFnPc4	planeta
jako	jako	k8xC	jako
štít	štít	k1gInSc4	štít
před	před	k7c7	před
dopady	dopad	k1gInPc7	dopad
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozdější	pozdní	k2eAgFnPc4d2	pozdější
počítačové	počítačový	k2eAgFnPc4d1	počítačová
modelace	modelace	k1gFnPc4	modelace
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouhá	pouhý	k2eAgFnSc1d1	pouhá
přítomnost	přítomnost	k1gFnSc1	přítomnost
Jupiteru	Jupiter	k1gInSc2	Jupiter
nezmenšuje	zmenšovat	k5eNaImIp3nS	zmenšovat
významně	významně	k6eAd1	významně
množství	množství	k1gNnSc1	množství
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
části	část	k1gFnSc2	část
soustavy	soustava	k1gFnSc2	soustava
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gNnSc1	jeho
gravitační	gravitační	k2eAgNnSc1d1	gravitační
působení	působení	k1gNnSc1	působení
některé	některý	k3yIgFnSc2	některý
komety	kometa	k1gFnSc2	kometa
přitáhne	přitáhnout	k5eAaPmIp3nS	přitáhnout
a	a	k8xC	a
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
jen	jen	k9	jen
odkloní	odklonit	k5eAaPmIp3nS	odklonit
a	a	k8xC	a
opět	opět	k6eAd1	opět
odhodí	odhodit	k5eAaPmIp3nS	odhodit
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
ale	ale	k9	ale
nepanuje	panovat	k5eNaImIp3nS	panovat
mezi	mezi	k7c4	mezi
astronomy	astronom	k1gMnPc4	astronom
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
Jupiter	Jupiter	k1gMnSc1	Jupiter
chrání	chránit	k5eAaImIp3nS	chránit
Zemi	zem	k1gFnSc4	zem
před	před	k7c7	před
kometami	kometa	k1gFnPc7	kometa
či	či	k8xC	či
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Uvažují	uvažovat	k5eAaImIp3nP	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sice	sice	k8xC	sice
může	moct	k5eAaImIp3nS	moct
zachycovat	zachycovat	k5eAaImF	zachycovat
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
komety	kometa	k1gFnPc4	kometa
ze	z	k7c2	z
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
Oortova	Oortův	k2eAgNnSc2d1	Oortovo
mračna	mračno	k1gNnSc2	mračno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
změny	změna	k1gFnPc1	změna
drah	draha	k1gFnPc2	draha
komet	kometa	k1gFnPc2	kometa
v	v	k7c6	v
bližším	blízký	k2eAgInSc6d2	bližší
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
pro	pro	k7c4	pro
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
průzkum	průzkum	k1gInSc1	průzkum
historických	historický	k2eAgFnPc2d1	historická
kreseb	kresba	k1gFnPc2	kresba
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
astronom	astronom	k1gMnSc1	astronom
Cassini	Cassin	k2eAgMnPc1d1	Cassin
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1690	[number]	k4	1690
jizvu	jizva	k1gFnSc4	jizva
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
dopadem	dopad	k1gInSc7	dopad
neznámého	známý	k2eNgNnSc2d1	neznámé
tělesa	těleso	k1gNnSc2	těleso
na	na	k7c4	na
Jupiter	Jupiter	k1gInSc4	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
8	[number]	k4	8
podobných	podobný	k2eAgInPc2d1	podobný
případů	případ	k1gInPc2	případ
studie	studie	k1gFnSc2	studie
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
vyloučila	vyloučit	k5eAaPmAgFnS	vyloučit
nebo	nebo	k8xC	nebo
naznačila	naznačit	k5eAaPmAgFnS	naznačit
jen	jen	k9	jen
malou	malý	k2eAgFnSc4d1	malá
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
impakt	impakt	k1gInSc4	impakt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
16	[number]	k4	16
<g/>
.	.	kIx.	.
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1994	[number]	k4	1994
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
Jupiteru	Jupiter	k1gInSc2	Jupiter
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
částí	část	k1gFnPc2	část
rozpadlého	rozpadlý	k2eAgNnSc2d1	rozpadlé
jádra	jádro	k1gNnSc2	jádro
komety	kometa	k1gFnSc2	kometa
Shoemaker-Levy	Shoemaker-Leva	k1gFnSc2	Shoemaker-Leva
9	[number]	k4	9
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dalo	dát	k5eAaPmAgNnS	dát
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
přímému	přímý	k2eAgInSc3d1	přímý
pozorování	pozorování	k1gNnPc2	pozorování
srážky	srážka	k1gFnSc2	srážka
dvou	dva	k4xCgInPc2	dva
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Kolize	kolize	k1gFnSc1	kolize
komety	kometa	k1gFnSc2	kometa
přinesla	přinést	k5eAaPmAgFnS	přinést
důležité	důležitý	k2eAgInPc4d1	důležitý
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
objeveno	objeven	k2eAgNnSc1d1	objeveno
místo	místo	k1gNnSc1	místo
dopadu	dopad	k1gInSc2	dopad
dalšího	další	k2eAgNnSc2d1	další
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
216	[number]	k4	216
<g/>
°	°	k?	°
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Impakt	Impakt	k1gInSc4	Impakt
za	za	k7c7	za
sebou	se	k3xPyFc7	se
zanechal	zanechat	k5eAaPmAgMnS	zanechat
velkou	velká	k1gFnSc4	velká
černou	černý	k2eAgFnSc4d1	černá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
velikostí	velikost	k1gFnSc7	velikost
odpovídala	odpovídat	k5eAaImAgNnP	odpovídat
Oválu	ovál	k1gInSc2	ovál
BA	ba	k9	ba
(	(	kIx(	(
<g/>
útvaru	útvar	k1gInSc2	útvar
v	v	k7c6	v
Jupiterově	Jupiterův	k2eAgFnSc6d1	Jupiterova
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
podobnému	podobný	k2eAgInSc3d1	podobný
Velké	velký	k2eAgFnSc3d1	velká
rudé	rudý	k2eAgFnSc3d1	rudá
skvrně	skvrna	k1gFnSc3	skvrna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jasnější	jasný	k2eAgFnSc4d2	jasnější
oblast	oblast	k1gFnSc4	oblast
poblíž	poblíž	k7c2	poblíž
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnSc2d1	vyznačující
místo	místo	k7c2	místo
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
zahřáté	zahřátý	k2eAgFnSc6d1	zahřátá
třením	tření	k1gNnSc7	tření
tělesa	těleso	k1gNnSc2	těleso
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
sestupu	sestup	k1gInSc6	sestup
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
jako	jako	k8xC	jako
největší	veliký	k2eAgFnSc1d3	veliký
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
pouze	pouze	k6eAd1	pouze
pády	pád	k1gInPc1	pád
těles	těleso	k1gNnPc2	těleso
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nP	měnit
také	také	k9	také
dráhy	dráha	k1gFnPc1	dráha
komet	kometa	k1gFnPc2	kometa
a	a	k8xC	a
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
proletí	proletět	k5eAaPmIp3nP	proletět
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
relativně	relativně	k6eAd1	relativně
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
způsobil	způsobit	k5eAaPmAgMnS	způsobit
změnu	změna	k1gFnSc4	změna
dráhy	dráha	k1gFnSc2	dráha
komety	kometa	k1gFnSc2	kometa
Honda-Mrkos-Pajdušáková	Honda-Mrkos-Pajdušáková	k1gFnSc1	Honda-Mrkos-Pajdušáková
<g/>
.	.	kIx.	.
</s>
<s>
Kometa	kometa	k1gFnSc1	kometa
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
0,08	[number]	k4	0,08
AU	au	k0	au
(	(	kIx(	(
<g/>
asi	asi	k9	asi
12	[number]	k4	12
miliónu	milión	k4xCgInSc2	milión
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
změnilo	změnit	k5eAaPmAgNnS	změnit
její	její	k3xOp3gFnSc4	její
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dobu	doba	k1gFnSc4	doba
z	z	k7c2	z
5,53	[number]	k4	5,53
na	na	k7c4	na
5,27	[number]	k4	5,27
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgInP	změnit
elementy	element	k1gInPc1	element
její	její	k3xOp3gFnSc2	její
dráhy	dráha	k1gFnSc2	dráha
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
na	na	k7c4	na
0,58	[number]	k4	0,58
AU	au	k0	au
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
0,64	[number]	k4	0,64
AU	au	k0	au
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gInSc1	Jupiter
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
mění	měnit	k5eAaImIp3nS	měnit
dráhy	dráha	k1gFnPc4	dráha
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
komet	kometa	k1gFnPc2	kometa
a	a	k8xC	a
planetek	planetka	k1gFnPc2	planetka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
mnohem	mnohem	k6eAd1	mnohem
výrazněji	výrazně	k6eAd2	výrazně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
Millerův-Ureyův	Millerův-Ureyův	k2eAgInSc1d1	Millerův-Ureyův
experiment	experiment	k1gInSc1	experiment
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kombinací	kombinace	k1gFnSc7	kombinace
blesků	blesk	k1gInPc2	blesk
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
sloučenin	sloučenina	k1gFnPc2	sloučenina
existujících	existující	k2eAgFnPc2d1	existující
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
primitivní	primitivní	k2eAgFnSc2d1	primitivní
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
složitější	složitý	k2eAgFnSc2d2	složitější
organické	organický	k2eAgFnSc2d1	organická
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
základní	základní	k2eAgInPc4d1	základní
stavební	stavební	k2eAgInPc4d1	stavební
kameny	kámen	k1gInPc4	kámen
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Simulovaná	simulovaný	k2eAgFnSc1d1	simulovaná
atmosféra	atmosféra	k1gFnSc1	atmosféra
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
methan	methan	k1gInSc4	methan
<g/>
,	,	kIx,	,
čpavek	čpavek	k1gInSc4	čpavek
a	a	k8xC	a
molekulární	molekulární	k2eAgInSc4d1	molekulární
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
atmosféra	atmosféra	k1gFnSc1	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
má	mít	k5eAaImIp3nS	mít
silnou	silný	k2eAgFnSc4d1	silná
vertikální	vertikální	k2eAgFnSc4d1	vertikální
cirkulaci	cirkulace	k1gFnSc4	cirkulace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
tyto	tento	k3xDgFnPc4	tento
komponenty	komponenta	k1gFnPc4	komponenta
zanášet	zanášet	k5eAaImF	zanášet
do	do	k7c2	do
spodních	spodní	k2eAgFnPc2d1	spodní
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
způsobila	způsobit	k5eAaPmAgFnS	způsobit
jejich	jejich	k3xOp3gInSc4	jejich
rozpad	rozpad	k1gInSc4	rozpad
a	a	k8xC	a
tak	tak	k6eAd1	tak
i	i	k9	i
bránila	bránit	k5eAaImAgFnS	bránit
vzniku	vznik	k1gInSc3	vznik
podobného	podobný	k2eAgInSc2d1	podobný
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
existuje	existovat	k5eAaImIp3nS	existovat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
nacházel	nacházet	k5eAaImAgMnS	nacházet
život	život	k1gInSc4	život
podobný	podobný	k2eAgInSc4d1	podobný
tomu	ten	k3xDgNnSc3	ten
pozemskému	pozemský	k2eAgInSc3d1	pozemský
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
případný	případný	k2eAgInSc4d1	případný
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
extrémnímu	extrémní	k2eAgInSc3d1	extrémní
tlaku	tlak	k1gInSc3	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
před	před	k7c4	před
průlety	průlet	k1gInPc4	průlet
sond	sonda	k1gFnPc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
hypotetické	hypotetický	k2eAgFnPc1d1	hypotetická
spekulace	spekulace	k1gFnPc1	spekulace
naznačující	naznačující	k2eAgFnSc4d1	naznačující
možnost	možnost	k1gFnSc4	možnost
existence	existence	k1gFnSc2	existence
života	život	k1gInSc2	život
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
či	či	k8xC	či
čpavku	čpavek	k1gInSc6	čpavek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
ve	v	k7c6	v
svrchních	svrchní	k2eAgFnPc6d1	svrchní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
životě	život	k1gInSc6	život
v	v	k7c6	v
pozemských	pozemský	k2eAgInPc6d1	pozemský
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
organismy	organismus	k1gInPc1	organismus
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
planktonu	plankton	k1gInSc2	plankton
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
svrchních	svrchní	k2eAgFnPc6d1	svrchní
vrstvách	vrstva	k1gFnPc6	vrstva
a	a	k8xC	a
pod	pod	k7c7	pod
nimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ryby	ryba	k1gFnPc4	ryba
konzumující	konzumující	k2eAgNnSc1d1	konzumující
právě	právě	k9	právě
plankton	plankton	k1gInSc4	plankton
a	a	k8xC	a
predátoři	predátor	k1gMnPc1	predátor
lovící	lovící	k2eAgFnSc2d1	lovící
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
již	již	k6eAd1	již
od	od	k7c2	od
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Babyloňany	Babyloňan	k1gMnPc4	Babyloňan
představoval	představovat	k5eAaImAgMnS	představovat
Jupiter	Jupiter	k1gMnSc1	Jupiter
boha	bůh	k1gMnSc2	bůh
Marduka	Marduk	k1gMnSc2	Marduk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
12	[number]	k4	12
<g/>
letá	letý	k2eAgFnSc1d1	letá
oběžná	oběžný	k2eAgFnSc1d1	oběžná
doba	doba	k1gFnSc1	doba
okolo	okolo	k7c2	okolo
ekliptiky	ekliptika	k1gFnSc2	ekliptika
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaPmNgFnS	využívat
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
babylonského	babylonský	k2eAgInSc2d1	babylonský
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
,	,	kIx,	,
Korejci	Korejec	k1gMnPc1	Korejec
<g/>
,	,	kIx,	,
Japonci	Japonec	k1gMnPc1	Japonec
a	a	k8xC	a
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
planetě	planeta	k1gFnSc6	planeta
jako	jako	k8xC	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
dřevěné	dřevěný	k2eAgFnSc6d1	dřevěná
hvězdě	hvězda	k1gFnSc6	hvězda
<g/>
,	,	kIx,	,
zápis	zápis	k1gInSc1	zápis
pomocí	pomocí	k7c2	pomocí
čínských	čínský	k2eAgInPc2d1	čínský
znaků	znak	k1gInPc2	znak
木	木	k?	木
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
pěti	pět	k4xCc7	pět
elementy	element	k1gInPc7	element
dle	dle	k7c2	dle
čínské	čínský	k2eAgFnSc2d1	čínská
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
Jupiter	Jupiter	k1gInSc4	Jupiter
nazývali	nazývat	k5eAaImAgMnP	nazývat
Φ	Φ	k?	Φ
<g/>
,	,	kIx,	,
Faethón	Faethón	k1gMnSc1	Faethón
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
planoucí	planoucí	k2eAgFnSc1d1	planoucí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
védské	védský	k2eAgFnSc6d1	védská
astrologii	astrologie	k1gFnSc6	astrologie
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
hinduističtí	hinduistický	k2eAgMnPc1d1	hinduistický
astrologové	astrolog	k1gMnPc1	astrolog
planetu	planeta	k1gFnSc4	planeta
po	po	k7c6	po
bohovi	bůh	k1gMnSc6	bůh
Brhaspati	Brhaspati	k1gMnSc6	Brhaspati
<g/>
,	,	kIx,	,
učiteli	učitel	k1gMnSc6	učitel
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Guru	guru	k1gMnSc1	guru
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
je	být	k5eAaImIp3nS	být
den	den	k1gInSc4	den
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
</s>
<s>
Thursday	Thursday	k1gInPc1	Thursday
<g/>
)	)	kIx)	)
spojen	spojen	k2eAgMnSc1d1	spojen
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Thórem	Thór	k1gMnSc7	Thór
(	(	kIx(	(
<g/>
Thor	Thor	k1gMnSc1	Thor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
day	day	k?	day
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
spojován	spojován	k2eAgMnSc1d1	spojován
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
v	v	k7c6	v
severské	severský	k2eAgFnSc6d1	severská
mytologii	mytologie	k1gFnSc6	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
planetu	planeta	k1gFnSc4	planeta
po	po	k7c6	po
bohu	bůh	k1gMnSc6	bůh
Jupiteru	Jupiter	k1gMnSc6	Jupiter
(	(	kIx(	(
<g/>
také	také	k9	také
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Jova	Jova	k1gMnSc1	Jova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
bohem	bůh	k1gMnSc7	bůh
římské	římský	k2eAgFnSc2d1	římská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
protoindoevropského	protoindoevropský	k2eAgInSc2d1	protoindoevropský
vokativu	vokativ	k1gInSc2	vokativ
*	*	kIx~	*
<g/>
dyeu	dyeu	k6eAd1	dyeu
ph	ph	kA	ph
<g/>
2	[number]	k4	2
<g/>
ter	ter	k?	ter
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgMnSc1d1	znamenající
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
Otec	otec	k1gMnSc1	otec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Astronomický	astronomický	k2eAgInSc1d1	astronomický
symbol	symbol	k1gInSc1	symbol
pro	pro	k7c4	pro
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
,	,	kIx,	,
je	on	k3xPp3gNnSc4	on
stylistické	stylistický	k2eAgNnSc4d1	stylistické
znázornění	znázornění	k1gNnSc4	znázornění
božského	božský	k2eAgInSc2d1	božský
blesku	blesk	k1gInSc2	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnSc1d1	původní
řecký	řecký	k2eAgMnSc1d1	řecký
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Zeus	Zeus	k1gInSc1	Zeus
<g/>
,	,	kIx,	,
přijatý	přijatý	k2eAgInSc1d1	přijatý
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
kořen	kořen	k1gInSc1	kořen
"	"	kIx"	"
<g/>
zeno-	zeno-	k?	zeno-
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
slov	slovo	k1gNnPc2	slovo
spojených	spojený	k2eAgInPc6d1	spojený
s	s	k7c7	s
Jupiterem	Jupiter	k1gInSc7	Jupiter
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zenografický	zenografický	k2eAgInSc4d1	zenografický
(	(	kIx(	(
<g/>
změřený	změřený	k2eAgInSc4d1	změřený
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
jako	jako	k9	jako
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
slovo	slovo	k1gNnSc4	slovo
jovian	joviana	k1gFnPc2	joviana
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
-	-	kIx~	-
převážně	převážně	k6eAd1	převážně
astrology	astrolog	k1gMnPc7	astrolog
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
-	-	kIx~	-
používaná	používaný	k2eAgFnSc1d1	používaná
forma	forma	k1gFnSc1	forma
jovial	jovial	k1gMnSc1	jovial
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
žoviální	žoviální	k2eAgNnSc1d1	žoviální
<g/>
)	)	kIx)	)
dnes	dnes	k6eAd1	dnes
znamená	znamenat	k5eAaImIp3nS	znamenat
veselý	veselý	k2eAgMnSc1d1	veselý
či	či	k8xC	či
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odráží	odrážet	k5eAaImIp3nS	odrážet
astrologickou	astrologický	k2eAgFnSc4d1	astrologická
charakteristiku	charakteristika	k1gFnSc4	charakteristika
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
