<p>
<s>
Chorol	Chorol	k1gInSc1	Chorol
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
i	i	k8xC	i
rusky	rusky	k6eAd1	rusky
Chorol	Chorola	k1gFnPc2	Chorola
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Choroł	Choroł	k1gFnSc1	Choroł
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Poltavské	poltavský	k2eAgFnSc6d1	Poltavská
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
západně	západně	k6eAd1	západně
od	od	k7c2	od
Poltavy	Poltava	k1gFnSc2	Poltava
<g/>
,	,	kIx,	,
správního	správní	k2eAgNnSc2d1	správní
střediska	středisko	k1gNnSc2	středisko
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Lubnů	Lubn	k1gInPc2	Lubn
<g/>
.	.	kIx.	.
</s>
<s>
Východně	východně	k6eAd1	východně
od	od	k7c2	od
Chorolu	Chorol	k1gInSc2	Chorol
prochází	procházet	k5eAaImIp3nS	procházet
řeka	řeka	k1gFnSc1	řeka
Chorol	Chorola	k1gFnPc2	Chorola
a	a	k8xC	a
severovýchodně	severovýchodně	k6eAd1	severovýchodně
jej	on	k3xPp3gInSc4	on
míjí	míjet	k5eAaImIp3nS	míjet
dálnice	dálnice	k1gFnSc1	dálnice
M	M	kA	M
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
evropská	evropský	k2eAgFnSc1d1	Evropská
silnice	silnice	k1gFnSc1	silnice
E	E	kA	E
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Chorolu	Chorol	k1gInSc6	Chorol
přes	přes	k7c4	přes
třináct	třináct	k4xCc4	třináct
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
za	za	k7c2	za
Kyjevská	kyjevský	k2eAgFnSc1d1	Kyjevská
Rus	Rus	k1gFnSc1	Rus
za	za	k7c2	za
Vladimíra	Vladimír	k1gMnSc2	Vladimír
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Monomacha	Monomacha	k1gFnSc1	Monomacha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1083	[number]	k4	1083
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnPc2	součást
řetězu	řetěz	k1gInSc2	řetěz
opevnění	opevnění	k1gNnSc2	opevnění
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
Sule	Sul	k1gInSc2	Sul
<g/>
,	,	kIx,	,
Chorolu	Chorol	k1gInSc2	Chorol
<g/>
,	,	kIx,	,
Pselu	Psel	k1gInSc2	Psel
a	a	k8xC	a
Vorskle	Vorskl	k1gInSc5	Vorskl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
nájezdníky	nájezdník	k1gMnPc7	nájezdník
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejtěžším	těžký	k2eAgInPc3d3	nejtěžší
bojům	boj	k1gInPc3	boj
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1107	[number]	k4	1107
<g/>
,	,	kIx,	,
1111	[number]	k4	1111
<g/>
,	,	kIx,	,
1185	[number]	k4	1185
a	a	k8xC	a
1215	[number]	k4	1215
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1362	[number]	k4	1362
patřil	patřit	k5eAaImAgInS	patřit
Chorol	Chorol	k1gInSc1	Chorol
k	k	k7c3	k
litevskému	litevský	k2eAgNnSc3d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc3	velkoknížectví
a	a	k8xC	a
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1569	[number]	k4	1569
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Lublinské	lublinský	k2eAgFnSc2d1	Lublinská
unie	unie	k1gFnSc2	unie
k	k	k7c3	k
Republice	republika	k1gFnSc6	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Chorol	Chorol	k1gInSc1	Chorol
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rodáci	rodák	k1gMnPc5	rodák
===	===	k?	===
</s>
</p>
<p>
<s>
Ben	Ben	k1gInSc1	Ben
Cijon	Cijon	k1gMnSc1	Cijon
Dinur	Dinur	k1gMnSc1	Dinur
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
–	–	k?	–
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
</s>
</p>
<p>
<s>
Arje	Arje	k1gFnSc1	Arje
Dvorecki	Dvoreck	k1gFnSc2	Dvoreck
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
–	–	k?	–
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
matematik	matematik	k1gMnSc1	matematik
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chorol	Chorol	k1gInSc1	Chorol
(	(	kIx(	(
<g/>
Stadt	Stadt	k1gInSc1	Stadt
<g/>
)	)	kIx)	)
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chorol	Chorola	k1gFnPc2	Chorola
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
