<p>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
ve	v	k7c6	v
dnech	den	k1gInPc6	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
byly	být	k5eAaImAgFnP	být
předčasné	předčasný	k2eAgFnPc1d1	předčasná
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
přijali	přijmout	k5eAaPmAgMnP	přijmout
ústavní	ústavní	k2eAgNnSc4d1	ústavní
usnesení	usnesení	k1gNnSc4	usnesení
o	o	k7c4	o
žádosti	žádost	k1gFnPc4	žádost
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
o	o	k7c4	o
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
35	[number]	k4	35
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
sněmovnu	sněmovna	k1gFnSc4	sněmovna
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
sociálně	sociálně	k6eAd1	sociálně
demokratická	demokratický	k2eAgFnSc1d1	demokratická
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
20,45	[number]	k4	20,45
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
50	[number]	k4	50
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
překvapivě	překvapivě	k6eAd1	překvapivě
skončilo	skončit	k5eAaPmAgNnS	skončit
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
18,65	[number]	k4	18,65
%	%	kIx~	%
a	a	k8xC	a
47	[number]	k4	47
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc3	třetí
skončila	skončit	k5eAaPmAgFnS	skončit
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc2d1	další
pak	pak	k6eAd1	pak
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vládní	vládní	k2eAgFnSc2d1	vládní
strany	strana	k1gFnSc2	strana
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
dostaly	dostat	k5eAaPmAgInP	dostat
Úsvit	úsvit	k1gInSc4	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
Tomia	Tomium	k1gNnSc2	Tomium
Okamury	Okamura	k1gFnSc2	Okamura
a	a	k8xC	a
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
unie	unie	k1gFnSc1	unie
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
lidová	lidový	k2eAgFnSc1d1	lidová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Státní	státní	k2eAgFnPc1d1	státní
volební	volební	k2eAgFnPc1d1	volební
komise	komise	k1gFnPc1	komise
oficiální	oficiální	k2eAgInPc4d1	oficiální
výsledky	výsledek	k1gInPc4	výsledek
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
sdělení	sdělení	k1gNnSc1	sdělení
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
č.	č.	k?	č.
343	[number]	k4	343
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Vítězem	vítěz	k1gMnSc7	vítěz
voleb	volba	k1gFnPc2	volba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ČSSD	ČSSD	kA	ČSSD
s	s	k7c7	s
20,45	[number]	k4	20,45
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
místo	místo	k1gNnSc1	místo
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
hnutí	hnutí	k1gNnSc4	hnutí
ANO	ano	k9	ano
s	s	k7c7	s
18,65	[number]	k4	18,65
%	%	kIx~	%
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
připadlo	připadnout	k5eAaPmAgNnS	připadnout
KSČM	KSČM	kA	KSČM
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
14,91	[number]	k4	14,91
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
TOP	topit	k5eAaImRp2nS	topit
O9	O9	k1gMnSc7	O9
s	s	k7c7	s
11,99	[number]	k4	11,99
%	%	kIx~	%
<g/>
,	,	kIx,	,
na	na	k7c6	na
pátém	pátý	k4xOgInSc6	pátý
místě	místo	k1gNnSc6	místo
skončila	skončit	k5eAaPmAgFnS	skončit
ODS	ODS	kA	ODS
se	s	k7c7	s
7,72	[number]	k4	7,72
%	%	kIx~	%
<g/>
,	,	kIx,	,
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
hnutí	hnutí	k1gNnSc1	hnutí
Úsvit	úsvit	k1gInSc4	úsvit
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
6,88	[number]	k4	6,88
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
se	s	k7c7	s
6,78	[number]	k4	6,78
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
strany	strana	k1gFnPc1	strana
nepřekročily	překročit	k5eNaPmAgFnP	překročit
pětiprocentní	pětiprocentní	k2eAgNnSc4d1	pětiprocentní
kvórum	kvórum	k1gNnSc4	kvórum
a	a	k8xC	a
do	do	k7c2	do
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nedostaly	dostat	k5eNaPmAgInP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc1d1	volební
účast	účast	k1gFnSc1	účast
byla	být	k5eAaImAgFnS	být
59,48	[number]	k4	59,48
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
odevzdáno	odevzdat	k5eAaPmNgNnS	odevzdat
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
007	[number]	k4	007
212	[number]	k4	212
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
platných	platný	k2eAgNnPc2d1	platné
4	[number]	k4	4
969	[number]	k4	969
984	[number]	k4	984
<g/>
)	)	kIx)	)
z	z	k7c2	z
8	[number]	k4	8
424	[number]	k4	424
227	[number]	k4	227
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podrobné	podrobný	k2eAgInPc1d1	podrobný
výsledky	výsledek	k1gInPc1	výsledek
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Podrobné	podrobný	k2eAgInPc1d1	podrobný
výsledky	výsledek	k1gInPc1	výsledek
stran	strana	k1gFnPc2	strana
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Zvolení	zvolený	k2eAgMnPc1d1	zvolený
poslanci	poslanec	k1gMnPc1	poslanec
podle	podle	k7c2	podle
politické	politický	k2eAgFnSc2d1	politická
příslušnosti	příslušnost	k1gFnSc2	příslušnost
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Výsledky	výsledek	k1gInPc4	výsledek
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Rozdělení	rozdělení	k1gNnSc1	rozdělení
mandátů	mandát	k1gInPc2	mandát
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Mapy	mapa	k1gFnPc4	mapa
výsledků	výsledek	k1gInPc2	výsledek
====	====	k?	====
</s>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
výsledků	výsledek	k1gInPc2	výsledek
sedmi	sedm	k4xCc2	sedm
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Politická	politický	k2eAgFnSc1d1	politická
krize	krize	k1gFnSc1	krize
2013	[number]	k4	2013
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
červnovou	červnový	k2eAgFnSc4d1	červnová
aféru	aféra	k1gFnSc4	aféra
podal	podat	k5eAaPmAgMnS	podat
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
demisi	demise	k1gFnSc4	demise
premiér	premiér	k1gMnSc1	premiér
Petr	Petr	k1gMnSc1	Petr
Nečas	Nečas	k1gMnSc1	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
strany	strana	k1gFnPc1	strana
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
středopravicové	středopravicový	k2eAgFnSc2d1	středopravicová
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
LIDEM	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
na	na	k7c6	na
pokračování	pokračování	k1gNnSc6	pokračování
koaliční	koaliční	k2eAgFnSc2d1	koaliční
spolupráce	spolupráce	k1gFnSc2	spolupráce
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nové	nový	k2eAgFnSc2d1	nová
premiérky	premiérka	k1gFnSc2	premiérka
Miroslavy	Miroslava	k1gFnSc2	Miroslava
Němcové	Němcová	k1gFnSc2	Němcová
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
ale	ale	k8xC	ale
přání	přání	k1gNnSc4	přání
trojkoalice	trojkoalice	k1gFnSc2	trojkoalice
nevyslyšel	vyslyšet	k5eNaPmAgMnS	vyslyšet
a	a	k8xC	a
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
bývalého	bývalý	k2eAgMnSc2d1	bývalý
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
se	se	k3xPyFc4	se
bývalá	bývalý	k2eAgFnSc1d1	bývalá
trojkoalice	trojkoalice	k1gFnSc1	trojkoalice
silně	silně	k6eAd1	silně
ohradila	ohradit	k5eAaPmAgFnS	ohradit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
opoziční	opoziční	k2eAgFnPc1d1	opoziční
strany	strana	k1gFnPc1	strana
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
VV	VV	kA	VV
<g/>
)	)	kIx)	)
uvítaly	uvítat	k5eAaPmAgInP	uvítat
konec	konec	k1gInSc4	konec
Nečasovy	Nečasův	k2eAgFnSc2d1	Nečasova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgInS	být
Rusnokův	Rusnokův	k2eAgInSc1d1	Rusnokův
kabinet	kabinet	k1gInSc1	kabinet
jmenován	jmenován	k2eAgInSc1d1	jmenován
<g/>
,	,	kIx,	,
koalice	koalice	k1gFnSc1	koalice
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
týdnech	týden	k1gInPc6	týden
setrvala	setrvat	k5eAaPmAgFnS	setrvat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
stanovisku	stanovisko	k1gNnSc6	stanovisko
dostat	dostat	k5eAaPmF	dostat
šanci	šance	k1gFnSc4	šance
pro	pro	k7c4	pro
sestavení	sestavení	k1gNnSc4	sestavení
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odůvodňovala	odůvodňovat	k5eAaImAgFnS	odůvodňovat
podporou	podpora	k1gFnSc7	podpora
101	[number]	k4	101
poslanců	poslanec	k1gMnPc2	poslanec
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
půdorystu	půdoryst	k1gInSc6	půdoryst
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
vládní	vládní	k2eAgFnSc2d1	vládní
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
po	po	k7c6	po
jmenování	jmenování	k1gNnSc6	jmenování
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
ČSSD	ČSSD	kA	ČSSD
pokusila	pokusit	k5eAaPmAgFnS	pokusit
rozpustit	rozpustit	k5eAaPmF	rozpustit
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
pouze	pouze	k6eAd1	pouze
96	[number]	k4	96
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
VV	VV	kA	VV
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
nezařazení	zařazený	k2eNgMnPc1d1	nezařazený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
předstoupila	předstoupit	k5eAaPmAgFnS	předstoupit
vláda	vláda	k1gFnSc1	vláda
před	před	k7c7	před
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c6	o
vyslovení	vyslovení	k1gNnSc6	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
sice	sice	k8xC	sice
nebyla	být	k5eNaImAgFnS	být
poměrem	poměr	k1gInSc7	poměr
93	[number]	k4	93
ku	k	k7c3	k
100	[number]	k4	100
hlasům	hlas	k1gInPc3	hlas
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlasování	hlasování	k1gNnSc1	hlasování
současně	současně	k6eAd1	současně
představovalo	představovat	k5eAaImAgNnS	představovat
rozpad	rozpad	k1gInSc4	rozpad
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
většiny	většina	k1gFnSc2	většina
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
LIDEM	Lido	k1gNnSc7	Lido
<g/>
.	.	kIx.	.
</s>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
dva	dva	k4xCgMnPc1	dva
poslanci	poslanec	k1gMnPc1	poslanec
za	za	k7c4	za
ODS	ODS	kA	ODS
Tomáš	Tomáš	k1gMnSc1	Tomáš
Úlehla	Úlehla	k1gMnSc1	Úlehla
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Florián	Florián	k1gMnSc1	Florián
a	a	k8xC	a
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
ze	z	k7c2	z
sálu	sál	k1gInSc2	sál
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
odešla	odejít	k5eAaPmAgFnS	odejít
i	i	k9	i
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
strany	strana	k1gFnSc2	strana
LIDEM	Lido	k1gNnSc7	Lido
Karolína	Karolína	k1gFnSc1	Karolína
Peake	Peake	k1gNnSc2	Peake
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředseda	místopředseda	k1gMnSc1	místopředseda
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Rusnokově	Rusnokův	k2eAgFnSc3d1	Rusnokova
vládě	vláda	k1gFnSc3	vláda
aktivně	aktivně	k6eAd1	aktivně
nepostaví	postavit	k5eNaPmIp3nP	postavit
101	[number]	k4	101
koaličních	koaliční	k2eAgMnPc2d1	koaliční
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
podpoří	podpořit	k5eAaPmIp3nS	podpořit
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
konání	konání	k1gNnPc2	konání
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
Rusnokově	Rusnokův	k2eAgFnSc3d1	Rusnokova
vládě	vláda	k1gFnSc3	vláda
aktivně	aktivně	k6eAd1	aktivně
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
pouze	pouze	k6eAd1	pouze
98	[number]	k4	98
koaličních	koaliční	k2eAgMnPc2d1	koaliční
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
po	po	k7c4	po
hlasování	hlasování	k1gNnSc4	hlasování
předseda	předseda	k1gMnSc1	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
Starostové	Starostové	k2eAgMnSc1d1	Starostové
Petr	Petr	k1gMnSc1	Petr
Gazdík	Gazdík	k1gMnSc1	Gazdík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předložil	předložit	k5eAaPmAgMnS	předložit
seznam	seznam	k1gInSc4	seznam
podpisů	podpis	k1gInPc2	podpis
všech	všecek	k3xTgMnPc2	všecek
42	[number]	k4	42
poslanců	poslanec	k1gMnPc2	poslanec
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
Starostů	Starosta	k1gMnPc2	Starosta
ke	k	k7c3	k
svolání	svolání	k1gNnSc3	svolání
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
schůze	schůze	k1gFnSc2	schůze
k	k	k7c3	k
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozpuštění	rozpuštění	k1gNnPc4	rozpuštění
sněmovny	sněmovna	k1gFnSc2	sněmovna
===	===	k?	===
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
zástupci	zástupce	k1gMnPc1	zástupce
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
ohledně	ohledně	k7c2	ohledně
podání	podání	k1gNnSc2	podání
společného	společný	k2eAgInSc2d1	společný
návrhu	návrh	k1gInSc2	návrh
k	k	k7c3	k
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
k	k	k7c3	k
návrhu	návrh	k1gInSc2	návrh
připojila	připojit	k5eAaPmAgFnS	připojit
KSČM	KSČM	kA	KSČM
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
Věci	věc	k1gFnSc2	věc
veřejné	veřejný	k2eAgFnSc2d1	veřejná
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
35	[number]	k4	35
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
podalo	podat	k5eAaPmAgNnS	podat
celkem	celkem	k6eAd1	celkem
126	[number]	k4	126
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Předčasné	předčasný	k2eAgFnPc1d1	předčasná
volby	volba	k1gFnPc1	volba
za	za	k7c4	za
jediné	jediný	k2eAgNnSc4d1	jediné
rozumné	rozumný	k2eAgNnSc4d1	rozumné
východisko	východisko	k1gNnSc4	východisko
označila	označit	k5eAaPmAgFnS	označit
i	i	k8xC	i
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
LIDEM	Lido	k1gNnSc7	Lido
Karolína	Karolína	k1gFnSc1	Karolína
Peake	Peake	k1gNnSc2	Peake
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
vyjadření	vyjadření	k1gNnSc2	vyjadření
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
předčasné	předčasný	k2eAgFnPc4d1	předčasná
volby	volba	k1gFnPc4	volba
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
i	i	k8xC	i
dva	dva	k4xCgMnPc1	dva
poslanci	poslanec	k1gMnPc1	poslanec
strany	strana	k1gFnSc2	strana
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
a	a	k8xC	a
nezařazená	zařazený	k2eNgFnSc1d1	nezařazená
Kristýna	Kristýna	k1gFnSc1	Kristýna
Kočí	Kočí	k1gFnSc2	Kočí
<g/>
.	.	kIx.	.
</s>
<s>
Nejednoznačný	jednoznačný	k2eNgInSc1d1	nejednoznačný
byl	být	k5eAaImAgInS	být
postoj	postoj	k1gInSc1	postoj
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
sněmovny	sněmovna	k1gFnSc2	sněmovna
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
se	se	k3xPyFc4	se
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
ke	k	k7c3	k
konání	konání	k1gNnSc3	konání
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
předseda	předseda	k1gMnSc1	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
Marek	Marek	k1gMnSc1	Marek
Benda	Benda	k1gMnSc1	Benda
oznámil	oznámit	k5eAaPmAgMnS	oznámit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
představitelů	představitel	k1gMnPc2	představitel
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
schůze	schůze	k1gFnSc1	schůze
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
hned	hned	k6eAd1	hned
přerušena	přerušit	k5eAaPmNgFnS	přerušit
a	a	k8xC	a
o	o	k7c6	o
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
až	až	k9	až
v	v	k7c4	v
následující	následující	k2eAgNnSc4d1	následující
úterý	úterý	k1gNnSc4	úterý
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
hlasování	hlasování	k1gNnSc2	hlasování
neúčastnit	účastnit	k5eNaImF	účastnit
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
ze	z	k7c2	z
sálu	sál	k1gInSc2	sál
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byla	být	k5eAaImAgFnS	být
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
sněmovny	sněmovna	k1gFnSc2	sněmovna
Miroslava	Miroslava	k1gFnSc1	Miroslava
Němcová	Němcová	k1gFnSc1	Němcová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
usnesení	usnesení	k1gNnSc2	usnesení
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
prezidentu	prezident	k1gMnSc3	prezident
k	k	k7c3	k
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
pohodlně	pohodlně	k6eAd1	pohodlně
schválen	schválit	k5eAaPmNgInS	schválit
<g/>
,	,	kIx,	,
když	když	k8xS	když
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
celkem	celkem	k6eAd1	celkem
140	[number]	k4	140
poslanců	poslanec	k1gMnPc2	poslanec
(	(	kIx(	(
<g/>
především	především	k9	především
poslanecké	poslanecký	k2eAgInPc1d1	poslanecký
kluby	klub	k1gInPc1	klub
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
KSČM	KSČM	kA	KSČM
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
VV	VV	kA	VV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
sedm	sedm	k4xCc1	sedm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ústavní	ústavní	k2eAgFnPc1d1	ústavní
stížnosti	stížnost	k1gFnPc1	stížnost
===	===	k?	===
</s>
</p>
<p>
<s>
Poslanec	poslanec	k1gMnSc1	poslanec
Petr	Petr	k1gMnSc1	Petr
Skokan	Skokan	k1gMnSc1	Skokan
(	(	kIx(	(
<g/>
VV	VV	kA	VV
<g/>
)	)	kIx)	)
podal	podat	k5eAaPmAgInS	podat
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
ústavní	ústavní	k2eAgFnSc4d1	ústavní
stížnost	stížnost	k1gFnSc4	stížnost
proti	proti	k7c3	proti
způsobu	způsob	k1gInSc3	způsob
svolání	svolání	k1gNnSc2	svolání
a	a	k8xC	a
průběhu	průběh	k1gInSc2	průběh
schůze	schůze	k1gFnSc2	schůze
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
byla	být	k5eAaImAgFnS	být
projednávána	projednáván	k2eAgFnSc1d1	projednávána
žádost	žádost	k1gFnSc1	žádost
o	o	k7c6	o
vyslovení	vyslovení	k1gNnSc6	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
vládě	vláda	k1gFnSc3	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
napadl	napadnout	k5eAaPmAgMnS	napadnout
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
nebyla	být	k5eNaImAgFnS	být
schůze	schůze	k1gFnSc1	schůze
svolána	svolat	k5eAaPmNgFnS	svolat
řádně	řádně	k6eAd1	řádně
a	a	k8xC	a
včas	včas	k6eAd1	včas
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k6eAd1	také
samotný	samotný	k2eAgInSc1d1	samotný
její	její	k3xOp3gInSc1	její
průběh	průběh	k1gInSc1	průběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
dožadoval	dožadovat	k5eAaImAgInS	dožadovat
se	se	k3xPyFc4	se
svolání	svolání	k1gNnSc3	svolání
nové	nový	k2eAgFnSc2d1	nová
schůze	schůze	k1gFnSc2	schůze
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
navíc	navíc	k6eAd1	navíc
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
soud	soud	k1gInSc1	soud
vydal	vydat	k5eAaPmAgInS	vydat
předběžné	předběžný	k2eAgNnSc4d1	předběžné
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
by	by	kYmCp3nS	by
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnPc1	republika
zakázal	zakázat	k5eAaPmAgInS	zakázat
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
jakékoli	jakýkoli	k3yIgInPc4	jakýkoli
podněty	podnět	k1gInPc4	podnět
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
přijdou	přijít	k5eAaPmIp3nP	přijít
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
a	a	k8xC	a
vypsání	vypsání	k1gNnSc4	vypsání
nových	nový	k2eAgFnPc2d1	nová
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
druhá	druhý	k4xOgFnSc1	druhý
ústavní	ústavní	k2eAgFnSc1d1	ústavní
stížnost	stížnost	k1gFnSc1	stížnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
sám	sám	k3xTgMnSc1	sám
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgFnPc4	který
ale	ale	k9	ale
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
směřuje	směřovat	k5eAaImIp3nS	směřovat
proti	proti	k7c3	proti
rychlosti	rychlost	k1gFnSc3	rychlost
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
nových	nový	k2eAgFnPc2d1	nová
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Nevyloučil	vyloučit	k5eNaPmAgMnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
projednání	projednání	k1gNnSc1	projednání
ústavních	ústavní	k2eAgFnPc2d1	ústavní
stížností	stížnost	k1gFnPc2	stížnost
může	moct	k5eAaImIp3nS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
odklad	odklad	k1gInSc4	odklad
voleb	volba	k1gFnPc2	volba
až	až	k6eAd1	až
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
však	však	k9	však
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
stížnost	stížnost	k1gFnSc4	stížnost
Petra	Petr	k1gMnSc2	Petr
Skokana	Skokan	k1gMnSc2	Skokan
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
projednání	projednání	k1gNnSc3	projednání
není	být	k5eNaImIp3nS	být
příslušný	příslušný	k2eAgInSc4d1	příslušný
a	a	k8xC	a
protože	protože	k8xS	protože
rozpuštěné	rozpuštěný	k2eAgFnSc6d1	rozpuštěná
sněmovně	sněmovna	k1gFnSc6	sněmovna
už	už	k6eAd1	už
nelze	lze	k6eNd1	lze
uložit	uložit	k5eAaPmF	uložit
žádnou	žádný	k3yNgFnSc4	žádný
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
předsedové	předseda	k1gMnPc1	předseda
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
zastoupených	zastoupený	k2eAgFnPc2d1	zastoupená
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
–	–	k?	–
Bohuslav	Bohuslava	k1gFnPc2	Bohuslava
Sobotka	Sobotka	k1gMnSc1	Sobotka
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Kuba	Kuba	k1gMnSc1	Kuba
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
(	(	kIx(	(
<g/>
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vít	Vít	k1gMnSc1	Vít
Bárta	Bárta	k1gMnSc1	Bárta
(	(	kIx(	(
<g/>
Věci	věc	k1gFnPc1	věc
veřejné	veřejný	k2eAgFnPc1d1	veřejná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
předsedům	předseda	k1gMnPc3	předseda
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
volby	volba	k1gFnPc4	volba
hodlá	hodlat	k5eAaImIp3nS	hodlat
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svolá	svolat	k5eAaPmIp3nS	svolat
zahajující	zahajující	k2eAgFnSc3d1	zahajující
schůzi	schůze	k1gFnSc3	schůze
nové	nový	k2eAgFnSc2d1	nová
sněmovny	sněmovna	k1gFnSc2	sněmovna
na	na	k7c4	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
rozpuštění	rozpuštění	k1gNnSc6	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
o	o	k7c6	o
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
voleb	volba	k1gFnPc2	volba
vydal	vydat	k5eAaPmAgInS	vydat
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Harmonogram	harmonogram	k1gInSc4	harmonogram
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
příprav	příprava	k1gFnPc2	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Níže	nízce	k6eAd2	nízce
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
harmonogram	harmonogram	k1gInSc1	harmonogram
voleb	volba	k1gFnPc2	volba
se	s	k7c7	s
základními	základní	k2eAgInPc7d1	základní
termíny	termín	k1gInPc7	termín
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgInSc1d1	detailní
harmonogram	harmonogram	k1gInSc1	harmonogram
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
server	server	k1gInSc4	server
ČTK	ČTK	kA	ČTK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
úterý	úterý	k1gNnSc1	úterý
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
:	:	kIx,	:
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
odhlasovala	odhlasovat	k5eAaPmAgFnS	odhlasovat
své	svůj	k3xOyFgNnSc4	svůj
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
</s>
</p>
<p>
<s>
středa	středa	k1gFnSc1	středa
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
:	:	kIx,	:
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
vydal	vydat	k5eAaPmAgMnS	vydat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c4	o
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
termín	termín	k1gInSc1	termín
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
do	do	k7c2	do
neděle	neděle	k1gFnSc2	neděle
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
:	:	kIx,	:
Občané	občan	k1gMnPc1	občan
mohou	moct	k5eAaImIp3nP	moct
požádat	požádat	k5eAaPmF	požádat
o	o	k7c6	o
hlasování	hlasování	k1gNnSc6	hlasování
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
</s>
</p>
<p>
<s>
úterý	úterý	k1gNnSc1	úterý
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
38	[number]	k4	38
dní	den	k1gInPc2	den
před	před	k7c7	před
prvním	první	k4xOgMnSc7	první
dnem	dnem	k7c2	dnem
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Nejzazší	zadní	k2eAgInSc1d3	nejzazší
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
podání	podání	k1gNnSc4	podání
kandidátních	kandidátní	k2eAgFnPc2d1	kandidátní
listin	listina	k1gFnPc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
4	[number]	k4	4
dny	den	k1gInPc4	den
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
ještě	ještě	k6eAd1	ještě
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
doplňovat	doplňovat	k5eAaImF	doplňovat
</s>
</p>
<p>
<s>
pondělí	pondělí	k1gNnSc1	pondělí
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
:	:	kIx,	:
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
musí	muset	k5eAaImIp3nP	muset
krajské	krajský	k2eAgInPc1d1	krajský
úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
pražský	pražský	k2eAgInSc1d1	pražský
magistrát	magistrát	k1gInSc1	magistrát
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
kandidátek	kandidátka	k1gFnPc2	kandidátka
</s>
</p>
<p>
<s>
středa	středa	k1gFnSc1	středa
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
:	:	kIx,	:
Rozlosování	rozlosování	k1gNnSc2	rozlosování
čísel	číslo	k1gNnPc2	číslo
stranám	strana	k1gFnPc3	strana
Státní	státní	k2eAgInSc1d1	státní
volební	volební	k2eAgFnSc7d1	volební
komisí	komise	k1gFnSc7	komise
</s>
</p>
<p>
<s>
od	od	k7c2	od
pondělí	pondělí	k1gNnSc2	pondělí
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
středy	středa	k1gFnSc2	středa
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
Vysílání	vysílání	k1gNnSc2	vysílání
volebních	volební	k2eAgInPc2d1	volební
spotů	spot	k1gInPc2	spot
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
rozhlase	rozhlas	k1gInSc6	rozhlas
</s>
</p>
<p>
<s>
od	od	k7c2	od
úterý	úterý	k1gNnSc2	úterý
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
3	[number]	k4	3
dny	den	k1gInPc4	den
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
voleb	volba	k1gFnPc2	volba
<g/>
)	)	kIx)	)
do	do	k7c2	do
konce	konec	k1gInSc2	konec
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
:	:	kIx,	:
Zákaz	zákaz	k1gInSc1	zákaz
zveřejňování	zveřejňování	k1gNnSc2	zveřejňování
výsledků	výsledek	k1gInPc2	výsledek
předvolebních	předvolební	k2eAgInPc2d1	předvolební
průzkumů	průzkum	k1gInPc2	průzkum
</s>
</p>
<p>
<s>
do	do	k7c2	do
středy	středa	k1gFnSc2	středa
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
Možnost	možnost	k1gFnSc1	možnost
osobně	osobně	k6eAd1	osobně
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
voličský	voličský	k2eAgInSc4d1	voličský
průkaz	průkaz	k1gInSc4	průkaz
</s>
</p>
<p>
<s>
pátek	pátek	k1gInSc1	pátek
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
sobota	sobota	k1gFnSc1	sobota
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
Volby	volba	k1gFnPc1	volba
</s>
</p>
<p>
<s>
==	==	k?	==
Kandidující	kandidující	k2eAgFnSc2d1	kandidující
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
volbám	volba	k1gFnPc3	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
celkem	celkem	k6eAd1	celkem
24	[number]	k4	24
politických	politický	k2eAgNnPc2d1	politické
seskupení	seskupení	k1gNnPc2	seskupení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
krajích	kraj	k1gInPc6	kraj
ovšem	ovšem	k9	ovšem
kandidátní	kandidátní	k2eAgFnPc4d1	kandidátní
listiny	listina	k1gFnPc4	listina
předložilo	předložit	k5eAaPmAgNnS	předložit
jen	jen	k9	jen
17	[number]	k4	17
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Aktiv	aktiv	k1gInSc1	aktiv
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
občanů	občan	k1gMnPc2	občan
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
nezaplacení	nezaplacení	k1gNnSc3	nezaplacení
povinného	povinný	k2eAgInSc2d1	povinný
volebního	volební	k2eAgInSc2d1	volební
příspěvku	příspěvek	k1gInSc2	příspěvek
vyřazen	vyřadit	k5eAaPmNgInS	vyřadit
z	z	k7c2	z
volebního	volební	k2eAgNnSc2d1	volební
klání	klání	k1gNnSc2	klání
v	v	k7c6	v
pěti	pět	k4xCc6	pět
krajích	kraj	k1gInPc6	kraj
<g/>
,	,	kIx,	,
kandidovat	kandidovat	k5eAaImF	kandidovat
bude	být	k5eAaImBp3nS	být
jen	jen	k9	jen
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInPc1d1	volební
principy	princip	k1gInPc1	princip
prázdných	prázdný	k2eAgFnPc2d1	prázdná
a	a	k8xC	a
blokovaných	blokovaný	k2eAgFnPc2d1	blokovaná
židlí	židle	k1gFnPc2	židle
(	(	kIx(	(
<g/>
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
kandidátní	kandidátní	k2eAgFnSc3d1	kandidátní
listinu	listina	k1gFnSc4	listina
předložily	předložit	k5eAaPmAgFnP	předložit
jen	jen	k9	jen
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
vyřazeny	vyřadit	k5eAaPmNgFnP	vyřadit
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nebyly	být	k5eNaImAgInP	být
ani	ani	k9	ani
předem	předem	k6eAd1	předem
zaregistrovány	zaregistrován	k2eAgFnPc1d1	zaregistrována
jako	jako	k8xS	jako
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
či	či	k8xC	či
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
socialistická	socialistický	k2eAgFnSc1d1	socialistická
(	(	kIx(	(
<g/>
č.	č.	k?	č.
19	[number]	k4	19
<g/>
)	)	kIx)	)
kandidovala	kandidovat	k5eAaImAgFnS	kandidovat
jen	jen	k9	jen
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesplnila	splnit	k5eNaPmAgFnS	splnit
náležitosti	náležitost	k1gFnPc4	náležitost
přihlášky	přihláška	k1gFnSc2	přihláška
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
proto	proto	k6eAd1	proto
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předvolební	předvolební	k2eAgInPc1d1	předvolební
průzkumy	průzkum	k1gInPc1	průzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výsledky	výsledek	k1gInPc1	výsledek
předvolebních	předvolební	k2eAgInPc2d1	předvolební
průzkumů	průzkum	k1gInPc2	průzkum
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
zveřejněny	zveřejněn	k2eAgInPc1d1	zveřejněn
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stranické	stranický	k2eAgFnPc4d1	stranická
preference	preference	k1gFnPc4	preference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Volební	volební	k2eAgInSc1d1	volební
model	model	k1gInSc1	model
===	===	k?	===
</s>
</p>
<p>
<s>
Strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
průzkumu	průzkum	k1gInSc6	průzkum
získaly	získat	k5eAaPmAgFnP	získat
nad	nad	k7c7	nad
1	[number]	k4	1
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
Strana	strana	k1gFnSc1	strana
zelených	zelený	k2eAgMnPc2d1	zelený
(	(	kIx(	(
<g/>
3,1	[number]	k4	3,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Úsvit	úsvit	k1gInSc1	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
(	(	kIx(	(
<g/>
2,9	[number]	k4	2,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
(	(	kIx(	(
<g/>
2,9	[number]	k4	2,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
strana	strana	k1gFnSc1	strana
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
(	(	kIx(	(
<g/>
2,8	[number]	k4	2,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
pirátská	pirátský	k2eAgFnSc1d1	pirátská
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
2,6	[number]	k4	2,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Suverenita	suverenita	k1gFnSc1	suverenita
-	-	kIx~	-
Blok	blok	k1gInSc1	blok
Jany	Jana	k1gFnSc2	Jana
Bobošíkové	Bobošíková	k1gFnSc2	Bobošíková
(	(	kIx(	(
<g/>
2,1	[number]	k4	2,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Starostové	Starosta	k1gMnPc1	Starosta
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
(	(	kIx(	(
<g/>
1,8	[number]	k4	1,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
průzkumu	průzkum	k1gInSc6	průzkum
nad	nad	k7c7	nad
1	[number]	k4	1
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
získaly	získat	k5eAaPmAgFnP	získat
Suverenita	suverenita	k1gFnSc1	suverenita
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strana	strana	k1gFnSc1	strana
zelených	zelený	k2eAgMnPc2d1	zelený
(	(	kIx(	(
<g/>
2,8	[number]	k4	2,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Úsvit	úsvit	k1gInSc1	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
(	(	kIx(	(
<g/>
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
pirátská	pirátský	k2eAgFnSc1d1	pirátská
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
2,4	[number]	k4	2,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
(	(	kIx(	(
<g/>
2,0	[number]	k4	2,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
průzkumu	průzkum	k1gInSc2	průzkum
by	by	k9	by
nad	nad	k7c7	nad
3	[number]	k4	3
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
získaly	získat	k5eAaPmAgInP	získat
Úsvit	úsvit	k1gInSc4	úsvit
(	(	kIx(	(
<g/>
3,7	[number]	k4	3,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Suverenita	suverenita	k1gFnSc1	suverenita
SBB	SBB	kA	SBB
(	(	kIx(	(
<g/>
3,3	[number]	k4	3,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
3	[number]	k4	3
%	%	kIx~	%
by	by	kYmCp3nP	by
zůstali	zůstat	k5eAaPmAgMnP	zůstat
Piráti	pirát	k1gMnPc1	pirát
<g/>
,	,	kIx,	,
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
<g/>
,	,	kIx,	,
DSSS	DSSS	kA	DSSS
a	a	k8xC	a
LEV	Lev	k1gMnSc1	Lev
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povolební	povolební	k2eAgFnSc1d1	povolební
situace	situace	k1gFnSc1	situace
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
nastala	nastat	k5eAaPmAgFnS	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
česká	český	k2eAgFnSc1d1	Česká
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
scéna	scéna	k1gFnSc1	scéna
značně	značně	k6eAd1	značně
nepřehlednou	přehledný	k2eNgFnSc7d1	nepřehledná
a	a	k8xC	a
roztříštěnou	roztříštěný	k2eAgFnSc7d1	roztříštěná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
parlamentní	parlamentní	k2eAgFnSc4d1	parlamentní
půdu	půda	k1gFnSc4	půda
poslaly	poslat	k5eAaPmAgInP	poslat
volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
řadu	řad	k1gInSc2	řad
nových	nový	k2eAgMnPc2d1	nový
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
dokonce	dokonce	k9	dokonce
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnSc6d1	silná
pozici	pozice	k1gFnSc6	pozice
(	(	kIx(	(
<g/>
hnutí	hnutí	k1gNnSc4	hnutí
ANO	ano	k9	ano
Andreje	Andrej	k1gMnSc4	Andrej
Babiše	Babiše	k1gFnSc2	Babiše
získalo	získat	k5eAaPmAgNnS	získat
přes	přes	k7c4	přes
18,5	[number]	k4	18,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
Úsvit	úsvit	k1gInSc1	úsvit
Tomia	Tomium	k1gNnSc2	Tomium
Okamury	Okamura	k1gFnSc2	Okamura
téměř	téměř	k6eAd1	téměř
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
citelně	citelně	k6eAd1	citelně
oslabily	oslabit	k5eAaPmAgFnP	oslabit
obě	dva	k4xCgFnPc1	dva
velké	velký	k2eAgFnPc1d1	velká
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
soupeření	soupeření	k1gNnSc1	soupeření
i	i	k9	i
občasné	občasný	k2eAgNnSc1d1	občasné
pragmatické	pragmatický	k2eAgNnSc1d1	pragmatické
spojenectví	spojenectví	k1gNnSc1	spojenectví
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
osou	osa	k1gFnSc7	osa
stranického	stranický	k2eAgInSc2d1	stranický
systému	systém	k1gInSc2	systém
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
ČSSD	ČSSD	kA	ČSSD
získala	získat	k5eAaPmAgFnS	získat
necelých	celý	k2eNgNnPc6d1	necelé
20,5	[number]	k4	20,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jí	jíst	k5eAaImIp3nS	jíst
sice	sice	k8xC	sice
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oproti	oproti	k7c3	oproti
očekávání	očekávání	k1gNnSc3	očekávání
<g/>
,	,	kIx,	,
předvolebním	předvolební	k2eAgInPc3d1	předvolební
průzkumům	průzkum	k1gInPc3	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
i	i	k9	i
oproti	oproti	k7c3	oproti
dosavadním	dosavadní	k2eAgInPc3d1	dosavadní
volebním	volební	k2eAgInPc3d1	volební
výsledkům	výsledek	k1gInPc3	výsledek
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
překvapivě	překvapivě	k6eAd1	překvapivě
nízký	nízký	k2eAgInSc4d1	nízký
zisk	zisk	k1gInSc4	zisk
<g/>
.	.	kIx.	.
</s>
<s>
ODS	ODS	kA	ODS
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
7,72	[number]	k4	7,72
%	%	kIx~	%
a	a	k8xC	a
pouhých	pouhý	k2eAgNnPc2d1	pouhé
16	[number]	k4	16
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgMnSc1d1	zvolený
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
předseda	předseda	k1gMnSc1	předseda
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsme	být	k5eAaImIp1nP	být
jí	on	k3xPp3gFnSc7	on
dosud	dosud	k6eAd1	dosud
znali	znát	k5eAaImAgMnP	znát
<g/>
,	,	kIx,	,
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
KSČM	KSČM	kA	KSČM
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgNnSc3d1	předchozí
volebnímu	volební	k2eAgNnSc3d1	volební
období	období	k1gNnSc3	období
mírně	mírně	k6eAd1	mírně
posílila	posílit	k5eAaPmAgFnS	posílit
na	na	k7c4	na
bezmála	bezmála	k6eAd1	bezmála
15	[number]	k4	15
%	%	kIx~	%
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
získala	získat	k5eAaPmAgFnS	získat
12	[number]	k4	12
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
sama	sám	k3xTgFnSc1	sám
považovala	považovat	k5eAaImAgFnS	považovat
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
ostře	ostro	k6eAd1	ostro
neoliberálnímu	neoliberální	k2eAgInSc3d1	neoliberální
proreformnímu	proreformní	k2eAgInSc3d1	proreformní
kursu	kurs	k1gInSc3	kurs
v	v	k7c6	v
Nečasově	Nečasův	k2eAgFnSc6d1	Nečasova
vládě	vláda	k1gFnSc6	vláda
za	za	k7c4	za
relativní	relativní	k2eAgInSc4d1	relativní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
lavic	lavice	k1gFnPc2	lavice
se	se	k3xPyFc4	se
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
mimoparlamentním	mimoparlamentní	k2eAgNnSc6d1	mimoparlamentní
období	období	k1gNnSc6	období
vrátila	vrátit	k5eAaPmAgFnS	vrátit
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
(	(	kIx(	(
<g/>
6,78	[number]	k4	6,78
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomio	Tomio	k1gMnSc1	Tomio
Okamura	Okamura	k1gFnSc1	Okamura
svou	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
formaci	formace	k1gFnSc4	formace
Úsvit	úsvit	k1gInSc1	úsvit
přímé	přímý	k2eAgFnSc2d1	přímá
demokracie	demokracie	k1gFnSc2	demokracie
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
6,9	[number]	k4	6,9
%	%	kIx~	%
zastoupení	zastoupení	k1gNnSc4	zastoupení
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nesplnily	splnit	k5eNaPmAgFnP	splnit
se	se	k3xPyFc4	se
prognózy	prognóza	k1gFnSc2	prognóza
rekordního	rekordní	k2eAgNnSc2d1	rekordní
množství	množství	k1gNnSc2	množství
propadlých	propadlý	k2eAgInPc2d1	propadlý
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Mimoparlamentními	mimoparlamentní	k2eAgFnPc7d1	mimoparlamentní
stranami	strana	k1gFnPc7	strana
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Strana	strana	k1gFnSc1	strana
zelených	zelený	k2eAgMnPc2d1	zelený
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Piráti	pirát	k1gMnPc1	pirát
(	(	kIx(	(
<g/>
2,7	[number]	k4	2,7
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
Strana	strana	k1gFnSc1	strana
svobodných	svobodný	k2eAgMnPc2d1	svobodný
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překvapením	překvapení	k1gNnSc7	překvapení
byl	být	k5eAaImAgInS	být
nízký	nízký	k2eAgInSc1d1	nízký
zisk	zisk	k1gInSc1	zisk
SPOZ	SPOZ	kA	SPOZ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přitom	přitom	k6eAd1	přitom
byla	být	k5eAaImAgFnS	být
páteří	páteř	k1gFnSc7	páteř
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
(	(	kIx(	(
<g/>
parlamentem	parlament	k1gInSc7	parlament
nepodpořené	podpořený	k2eNgFnSc2d1	nepodpořená
<g/>
)	)	kIx)	)
vlády	vláda	k1gFnSc2	vláda
Jiřího	Jiří	k1gMnSc2	Jiří
Rusnoka	Rusnoek	k1gMnSc2	Rusnoek
s	s	k7c7	s
neskrývanou	skrývaný	k2eNgFnSc7d1	neskrývaná
podporou	podpora	k1gFnSc7	podpora
prezidenta	prezident	k1gMnSc2	prezident
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
necelých	celý	k2eNgInPc2d1	necelý
0,5	[number]	k4	0,5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
hnutí	hnutí	k1gNnSc4	hnutí
Jany	Jana	k1gFnSc2	Jana
Bobošíkové	Bobošíkové	k2eAgFnSc4d1	Bobošíkové
HLAVU	hlava	k1gFnSc4	hlava
VZHŮRU	vzhůru	k6eAd1	vzhůru
podporované	podporovaný	k2eAgInPc1d1	podporovaný
nepřímo	přímo	k6eNd1	přímo
Václavem	Václav	k1gMnSc7	Václav
Klausem	Klaus	k1gMnSc7	Klaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
svolal	svolat	k5eAaPmAgMnS	svolat
zasedání	zasedání	k1gNnSc4	zasedání
nově	nově	k6eAd1	nově
zvolené	zvolený	k2eAgFnSc2d1	zvolená
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
199	[number]	k4	199
přítomných	přítomný	k2eAgMnPc2d1	přítomný
poslanců	poslanec	k1gMnPc2	poslanec
složilo	složit	k5eAaPmAgNnS	složit
ústavní	ústavní	k2eAgInSc4d1	ústavní
slib	slib	k1gInSc4	slib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
povolebních	povolební	k2eAgNnPc2d1	povolební
jednání	jednání	k1gNnPc2	jednání
vzešla	vzejít	k5eAaPmAgFnS	vzejít
vládní	vládní	k2eAgFnSc1d1	vládní
koalice	koalice	k1gFnSc1	koalice
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
ANO	ano	k9	ano
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
111	[number]	k4	111
poslanců	poslanec	k1gMnPc2	poslanec
z	z	k7c2	z
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podpořena	podpořit	k5eAaPmNgFnS	podpořit
sněmovnou	sněmovna	k1gFnSc7	sněmovna
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
vládě	vláda	k1gFnSc6	vláda
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
2013	[number]	k4	2013
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Skončily	skončit	k5eAaPmAgFnP	skončit
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
-	-	kIx~	-
European	European	k1gInSc1	European
Journal	Journal	k1gFnSc2	Journal
of	of	k?	of
Political	Political	k1gFnSc1	Political
Research-Political	Research-Political	k1gFnSc2	Research-Political
Data	datum	k1gNnSc2	datum
Yearbook	Yearbook	k1gInSc1	Yearbook
<g/>
:	:	kIx,	:
InteractiveVolební	InteractiveVolební	k2eAgInSc1d1	InteractiveVolební
web	web	k1gInSc1	web
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
</s>
</p>
