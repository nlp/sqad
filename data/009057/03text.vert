<p>
<s>
Vic	Vic	k?	Vic
Damone	Damon	k1gMnSc5	Damon
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Vito	vít	k5eAaImNgNnS	vít
Rocco	Rocco	k1gNnSc1	Rocco
Farinola	Farinola	k1gFnSc1	Farinola
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc1	Miami
Beach	Beacha	k1gFnPc2	Beacha
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
věnoval	věnovat	k5eAaImAgMnS	věnovat
zpěvu	zpěv	k1gInSc3	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
nahrávka	nahrávka	k1gFnSc1	nahrávka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
I	i	k9	i
Have	Have	k1gNnSc2	Have
But	But	k1gFnSc2	But
One	One	k1gMnSc1	One
Heart	Heart	k1gInSc1	Heart
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
sedmé	sedmý	k4xOgFnSc6	sedmý
příčce	příčka	k1gFnSc6	příčka
hitparády	hitparáda	k1gFnSc2	hitparáda
časopisu	časopis	k1gInSc2	časopis
Billboard	billboard	k1gInSc1	billboard
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
až	až	k9	až
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
manželkou	manželka	k1gFnSc7	manželka
italská	italský	k2eAgFnSc1d1	italská
herečka	herečka	k1gFnSc1	herečka
Pier	pier	k1gInSc1	pier
Angeli	angel	k1gMnSc5	angel
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
ještě	ještě	k6eAd1	ještě
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
−	−	k?	−
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
až	až	k9	až
1996	[number]	k4	1996
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Diahann	Diahanna	k1gFnPc2	Diahanna
Carroll	Carrolla	k1gFnPc2	Carrolla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
Songwriters	Songwritersa	k1gFnPc2	Songwritersa
Hall	Hallum	k1gNnPc2	Hallum
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgInS	vydat
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
knihu	kniha	k1gFnSc4	kniha
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Singing	Singing	k1gInSc1	Singing
Was	Was	k1gMnSc1	Was
the	the	k?	the
Easy	Easa	k1gFnSc2	Easa
Part	parta	k1gFnPc2	parta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vic	Vic	k1gFnSc2	Vic
Damone	Damon	k1gInSc5	Damon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vic	Vic	k?	Vic
Damone	Damon	k1gMnSc5	Damon
v	v	k7c6	v
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
