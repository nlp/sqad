<s>
Ion	ion	k1gInSc1	ion
nebo	nebo	k8xC	nebo
iont	iont	k1gInSc1	iont
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
nabitá	nabitý	k2eAgFnSc1d1	nabitá
částice	částice	k1gFnSc1	částice
atomární	atomární	k2eAgFnSc2d1	atomární
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
atom	atom	k1gInSc1	atom
<g/>
,	,	kIx,	,
molekula	molekula	k1gFnSc1	molekula
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
skupina	skupina	k1gFnSc1	skupina
atomů	atom	k1gInPc2	atom
či	či	k8xC	či
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
"	"	kIx"	"
<g/>
ión	ión	k?	ión
<g/>
"	"	kIx"	"
–	–	k?	–
poutník	poutník	k1gMnSc1	poutník
<g/>
.	.	kIx.	.
</s>
<s>
Ionty	ion	k1gInPc1	ion
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
chemickým	chemický	k2eAgInPc3d1	chemický
označením	označení	k1gNnSc7	označení
částice	částice	k1gFnSc2	částice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vpravo	vpravo	k6eAd1	vpravo
nahoře	nahoře	k6eAd1	nahoře
se	se	k3xPyFc4	se
uvede	uvést	k5eAaPmIp3nS	uvést
velikost	velikost	k1gFnSc1	velikost
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
iontu	ion	k1gInSc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Kationty	kation	k1gInPc1	kation
např.	např.	kA	např.
K	K	kA	K
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Mg	mg	kA	mg
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
Anionty	anion	k1gInPc7	anion
např.	např.	kA	např.
F-	F-	k1gMnPc2	F-
<g/>
,	,	kIx,	,
CO	co	k6eAd1	co
<g/>
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
NO3-	NO3-	k1gMnSc1	NO3-
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
kationt	kationt	k1gInSc4	kationt
a	a	k8xC	a
aniont	aniont	k1gInSc4	aniont
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
<g/>
:	:	kIx,	:
Kationty	kation	k1gInPc4	kation
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
kladně	kladně	k6eAd1	kladně
nabité	nabitý	k2eAgInPc1d1	nabitý
ionty	ion	k1gInPc1	ion
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
atomy	atom	k1gInPc1	atom
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnPc1	molekula
(	(	kIx(	(
<g/>
části	část	k1gFnPc1	část
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
odevzdaly	odevzdat	k5eAaPmAgFnP	odevzdat
elektron	elektron	k1gInSc4	elektron
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
kationt	kationt	k1gInSc1	kationt
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
méně	málo	k6eAd2	málo
elektronů	elektron	k1gInPc2	elektron
než	než	k8xS	než
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
atom	atom	k1gInSc4	atom
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
putují	putovat	k5eAaImIp3nP	putovat
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
katodě	katoda	k1gFnSc3	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
elektropozitivních	elektropozitivní	k2eAgInPc2d1	elektropozitivní
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
nebo	nebo	k8xC	nebo
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Anionty	anion	k1gInPc1	anion
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
záporně	záporně	k6eAd1	záporně
nabité	nabitý	k2eAgInPc1d1	nabitý
ionty	ion	k1gInPc1	ion
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
atomy	atom	k1gInPc1	atom
nebo	nebo	k8xC	nebo
molekuly	molekula	k1gFnPc1	molekula
(	(	kIx(	(
<g/>
části	část	k1gFnPc1	část
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přijaly	přijmout	k5eAaPmAgFnP	přijmout
elektron	elektron	k1gInSc4	elektron
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
aniont	aniont	k1gInSc1	aniont
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
více	hodně	k6eAd2	hodně
elektronů	elektron	k1gInPc2	elektron
než	než	k8xS	než
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
atom	atom	k1gInSc4	atom
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
putují	putovat	k5eAaImIp3nP	putovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
anodě	anoda	k1gFnSc3	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
elektronegativní	elektronegativní	k2eAgInPc1d1	elektronegativní
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
síru	síra	k1gFnSc4	síra
nebo	nebo	k8xC	nebo
chlor	chlor	k1gInSc4	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Zwitterionty	Zwitterionta	k1gFnPc1	Zwitterionta
jsou	být	k5eAaImIp3nP	být
neutrální	neutrální	k2eAgInPc4d1	neutrální
ionty	ion	k1gInPc4	ion
nesoucí	nesoucí	k2eAgInSc4d1	nesoucí
záporný	záporný	k2eAgInSc4d1	záporný
i	i	k8xC	i
kladný	kladný	k2eAgInSc4d1	kladný
náboj	náboj	k1gInSc4	náboj
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
molekuly	molekula	k1gFnSc2	molekula
Proces	proces	k1gInSc1	proces
vzniku	vznik	k1gInSc2	vznik
iontu	ion	k1gInSc2	ion
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ionizací	ionizace	k1gFnSc7	ionizace
<g/>
.	.	kIx.	.
</s>
<s>
Obrácený	obrácený	k2eAgInSc1d1	obrácený
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
vytvoření	vytvoření	k1gNnSc1	vytvoření
neutrálního	neutrální	k2eAgInSc2d1	neutrální
atomu	atom	k1gInSc2	atom
z	z	k7c2	z
iontu	ion	k1gInSc2	ion
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
rekombinace	rekombinace	k1gFnSc1	rekombinace
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
elektronů	elektron	k1gInPc2	elektron
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
podslupce	podslupka	k1gFnSc6	podslupka
atomu	atom	k1gInSc2	atom
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
ionizační	ionizační	k2eAgFnSc1d1	ionizační
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
ionizační	ionizační	k2eAgInSc1d1	ionizační
potenciál	potenciál	k1gInSc1	potenciál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1	ionizační
energie	energie	k1gFnSc1	energie
nám	my	k3xPp1nPc3	my
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pevně	pevně	k6eAd1	pevně
jsou	být	k5eAaImIp3nP	být
vnější	vnější	k2eAgInPc4d1	vnější
elektrony	elektron	k1gInPc4	elektron
k	k	k7c3	k
atomu	atom	k1gInSc6	atom
vázány	vázat	k5eAaImNgFnP	vázat
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
elektronu	elektron	k1gInSc2	elektron
k	k	k7c3	k
atomu	atom	k1gInSc3	atom
určitého	určitý	k2eAgInSc2d1	určitý
prvku	prvek	k1gInSc2	prvek
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
jisté	jistý	k2eAgFnSc2d1	jistá
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
elektronová	elektronový	k2eAgFnSc1d1	elektronová
afinita	afinita	k1gFnSc1	afinita
<g/>
.	.	kIx.	.
</s>
<s>
Ionty	ion	k1gInPc1	ion
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
solí	solit	k5eAaImIp3nS	solit
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
molekul	molekula	k1gFnPc2	molekula
rozpouštěné	rozpouštěný	k2eAgFnSc2d1	rozpouštěná
látky	látka	k1gFnSc2	látka
rozštěpí	rozštěpit	k5eAaPmIp3nS	rozštěpit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
iontů	ion	k1gInPc2	ion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
roztoky	roztok	k1gInPc1	roztok
jsou	být	k5eAaImIp3nP	být
elektricky	elektricky	k6eAd1	elektricky
vodivé	vodivý	k2eAgInPc1d1	vodivý
<g/>
.	.	kIx.	.
při	při	k7c6	při
ionizaci	ionizace	k1gFnSc6	ionizace
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rychle	rychle	k6eAd1	rychle
letící	letící	k2eAgFnSc1d1	letící
částice	částice	k1gFnSc1	částice
nárazem	náraz	k1gInSc7	náraz
rozštěpí	rozštěpit	k5eAaPmIp3nP	rozštěpit
molekulu	molekula	k1gFnSc4	molekula
na	na	k7c4	na
ionty	ion	k1gInPc4	ion
<g/>
.	.	kIx.	.
</s>
<s>
Ionizovaný	ionizovaný	k2eAgInSc1d1	ionizovaný
plyn	plyn	k1gInSc1	plyn
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stává	stávat	k5eAaImIp3nS	stávat
vodivým	vodivý	k2eAgMnSc7d1	vodivý
<g/>
.	.	kIx.	.
při	při	k7c6	při
tření	tření	k1gNnSc6	tření
(	(	kIx(	(
<g/>
kladný	kladný	k2eAgInSc1d1	kladný
náboj	náboj	k1gInSc1	náboj
-	-	kIx~	-
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
-	-	kIx~	-
plast	plast	k1gInSc1	plast
<g/>
)	)	kIx)	)
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
iontů	ion	k1gInPc2	ion
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
procesech	proces	k1gInPc6	proces
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
galvanické	galvanický	k2eAgNnSc4d1	galvanické
pokovování	pokovování	k1gNnSc4	pokovování
akumulátory	akumulátor	k1gInPc4	akumulátor
věrné	věrný	k2eAgInPc4d1	věrný
otisky	otisk	k1gInPc7	otisk
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
gramofonových	gramofonový	k2eAgFnPc2d1	gramofonová
desek	deska	k1gFnPc2	deska
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
mlžná	mlžný	k2eAgFnSc1d1	mlžná
komora	komora	k1gFnSc1	komora
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
dráhy	dráha	k1gFnSc2	dráha
částic	částice	k1gFnPc2	částice
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
záření	záření	k1gNnSc2	záření
Atom	atom	k1gInSc1	atom
Chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
Iontová	iontový	k2eAgFnSc1d1	iontová
vazba	vazba	k1gFnSc1	vazba
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ion	ion	k1gInSc1	ion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ion	ion	k1gInSc1	ion
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
