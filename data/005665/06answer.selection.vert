<s>
Ion	ion	k1gInSc1	ion
nebo	nebo	k8xC	nebo
iont	iont	k1gInSc1	iont
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
nabitá	nabitý	k2eAgFnSc1d1	nabitá
částice	částice	k1gFnSc1	částice
atomární	atomární	k2eAgFnSc2d1	atomární
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
atom	atom	k1gInSc1	atom
<g/>
,	,	kIx,	,
molekula	molekula	k1gFnSc1	molekula
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
skupina	skupina	k1gFnSc1	skupina
atomů	atom	k1gInPc2	atom
či	či	k8xC	či
molekul	molekula	k1gFnPc2	molekula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
