<s>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
e-mail	eail	k1gInSc1	e-mail
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
záměnu	záměna	k1gFnSc4	záměna
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
email	email	k1gInSc1	email
–	–	k?	–
nátěrová	nátěrový	k2eAgFnSc1d1	nátěrová
hmota	hmota	k1gFnSc1	hmota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
odesílání	odesílání	k1gNnSc2	odesílání
<g/>
,	,	kIx,	,
doručování	doručování	k1gNnSc2	doručování
a	a	k8xC	a
přijímání	přijímání	k1gNnSc2	přijímání
zpráv	zpráva	k1gFnPc2	zpráva
přes	přes	k7c4	přes
elektronické	elektronický	k2eAgInPc4d1	elektronický
komunikační	komunikační	k2eAgInPc4d1	komunikační
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
