<s>
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
Kostel	kostel	k1gInSc1
Navštívení	navštívení	k1gNnSc6
Panny	Panna	k1gFnSc2
MariePoloha	MariePoloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
292	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
10,2	10,2	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
581	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
155,2	155,2	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
Obec	obec	k1gFnSc1
Starostka	starostka	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Kallová	Kallová	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1247	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.malaida.sk	www.malaida.sk	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
055	#num#	k4
PSČ	PSČ	kA
</s>
<s>
044	#num#	k4
20	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
KS	ks	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
(	(	kIx(
<g/>
maďarsky	maďarsky	k6eAd1
Kisida	Kisid	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
v	v	k7c6
okrese	okres	k1gInSc6
Košice-okolí	Košice-okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Místopis	místopis	k1gInSc1
</s>
<s>
Obecní	obecní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
Obec	obec	k1gFnSc1
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Košické	košický	k2eAgFnSc2d1
kotliny	kotlina	k1gFnSc2
na	na	k7c6
nivě	niva	k1gFnSc6
potoka	potok	k1gInSc2
Ida	Ida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
se	se	k3xPyFc4
zvedají	zvedat	k5eAaImIp3nP
Volovské	volovský	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
<g/>
,	,	kIx,
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
katastru	katastr	k1gInSc2
však	však	k9
leží	ležet	k5eAaImIp3nS
v	v	k7c6
pahorkatině	pahorkatina	k1gFnSc6
z	z	k7c2
třetihorních	třetihorní	k2eAgFnPc2d1
usazenin	usazenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgInSc4d1
a	a	k8xC
západní	západní	k2eAgInSc4d1
okraj	okraj	k1gInSc4
katastru	katastr	k1gInSc2
pokrývají	pokrývat	k5eAaImIp3nP
lesy	les	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
převládají	převládat	k5eAaImIp3nP
duby	dub	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střed	střed	k1gInSc1
obce	obec	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
306	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
v	v	k7c6
katastru	katastr	k1gInSc6
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
290	#num#	k4
do	do	k7c2
507	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Obcí	obec	k1gFnSc7
prochází	procházet	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
548	#num#	k4
(	(	kIx(
<g/>
Košice	Košice	k1gInPc1
<g/>
–	–	k?
<g/>
Jasov	Jasov	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
zde	zde	k6eAd1
křižuje	křižovat	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
III	III	kA
<g/>
/	/	kIx~
<g/>
3405	#num#	k4
(	(	kIx(
<g/>
Košice-Šaca	Košice-Šac	k1gInSc2
–	–	k?
Bukovec	Bukovec	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrum	centrum	k1gNnSc1
Košic	Košice	k1gInPc2
je	být	k5eAaImIp3nS
vzdáleno	vzdálit	k5eAaPmNgNnS
11	#num#	k4
km	km	kA
východním	východní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1
toky	toka	k1gFnPc1
</s>
<s>
Přes	přes	k7c4
obec	obec	k1gFnSc4
protéká	protékat	k5eAaImIp3nS
říčka	říčka	k1gFnSc1
Ida	Ida	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pramení	pramenit	k5eAaImIp3nS
pod	pod	k7c7
Bílým	bílý	k2eAgInSc7d1
kamenem	kámen	k1gInSc7
(	(	kIx(
<g/>
1	#num#	k4
134,5	134,5	k4
m	m	kA
<g/>
)	)	kIx)
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Kojšovské	Kojšovský	k2eAgFnSc2d1
hole	hole	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Písemně	písemně	k6eAd1
doložené	doložený	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
obce	obec	k1gFnSc2
sahají	sahat	k5eAaImIp3nP
do	do	k7c2
roku	rok	k1gInSc2
1247	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
datován	datován	k2eAgInSc1d1
převod	převod	k1gInSc1
majetku	majetek	k1gInSc2
–	–	k?
území	území	k1gNnSc2
dnešní	dnešní	k2eAgFnSc2d1
Malé	Malé	k2eAgFnSc2d1
Idy	Ida	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
spišského	spišský	k2eAgMnSc4d1
probošta	probošt	k1gMnSc4
Matěje	Matěj	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvedeny	uveden	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zem	zem	k1gFnSc4
a	a	k8xC
lesy	les	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
ležely	ležet	k5eAaImAgInP
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
řeky	řeka	k1gFnSc2
Ida	Ida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemky	pozemek	k1gInPc7
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1280	#num#	k4
kníže	kníže	k1gMnSc1
David	David	k1gMnSc1
z	z	k7c2
Omodejovské	Omodejovský	k2eAgFnSc2d1
linie	linie	k1gFnSc2
Abovců	Abovce	k1gMnPc2
a	a	k8xC
v	v	k7c6
listinách	listina	k1gFnPc6
z	z	k7c2
roku	rok	k1gInSc2
1300	#num#	k4
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
nazývá	nazývat	k5eAaImIp3nS
Omodeus	Omodeus	k1gMnSc1
dictus	dictus	k1gMnSc1
Fejes	Fejes	k1gMnSc1
de	de	k?
Ida	Ida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Rozhanovců	Rozhanovec	k1gMnPc2
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
Robert	Robert	k1gMnSc1
zabral	zabrat	k5eAaPmAgMnS
omodejovské	omodejovský	k2eAgInPc4d1
majetky	majetek	k1gInPc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
i	i	k9
Malou	malý	k2eAgFnSc4d1
Idu	Ida	k1gFnSc4
a	a	k8xC
Poľov	Poľov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc7
novým	nový	k2eAgMnSc7d1
majitelem	majitel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
palatin	palatin	k1gMnSc1
Filip	Filip	k1gMnSc1
Drugetha	Drugetha	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
omilostnění	omilostnění	k1gNnSc6
Tomáše	Tomáš	k1gMnSc2
a	a	k8xC
Mikuláše	Mikuláš	k1gMnSc2
Čurky	čurka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1323	#num#	k4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
majetek	majetek	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1330	#num#	k4
si	se	k3xPyFc3
potomci	potomek	k1gMnPc1
Štěpána	Štěpán	k1gMnSc2
Čurky	čurka	k1gFnSc2
majetky	majetek	k1gInPc4
rozdělili	rozdělit	k5eAaPmAgMnP
a	a	k8xC
majitelem	majitel	k1gMnSc7
Malé	Malé	k2eAgFnSc2d1
Idy	Ida	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1367	#num#	k4
je	být	k5eAaImIp3nS
jako	jako	k9
vlastník	vlastník	k1gMnSc1
obce	obec	k1gFnSc2
uveden	uveden	k2eAgMnSc1d1
Tomášův	Tomášův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
magistr	magistr	k1gMnSc1
Peter	Peter	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1406	#num#	k4
přešla	přejít	k5eAaPmAgFnS
do	do	k7c2
majetku	majetek	k1gInSc2
Spišské	spišský	k2eAgFnSc2d1
kapituly	kapitula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c6
obci	obec	k1gFnSc6
stál	stát	k5eAaImAgInS
už	už	k6eAd1
i	i	k9
kostel	kostel	k1gInSc1
s	s	k7c7
farou	fara	k1gFnSc7
vybudovaný	vybudovaný	k2eAgInSc4d1
v	v	k7c6
letech	let	k1gInPc6
1332	#num#	k4
<g/>
–	–	k?
<g/>
1335	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
zažívá	zažívat	k5eAaImIp3nS
obec	obec	k1gFnSc1
díky	díky	k7c3
individuální	individuální	k2eAgFnSc3d1
bytové	bytový	k2eAgFnSc3d1
výstavbě	výstavba	k1gFnSc3
velký	velký	k2eAgInSc4d1
rozmach	rozmach	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokončena	dokončen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
plynofikace	plynofikace	k1gFnSc1
<g/>
,	,	kIx,
vybudoval	vybudovat	k5eAaPmAgInS
se	se	k3xPyFc4
vodovod	vodovod	k1gInSc1
i	i	k8xC
splašková	splaškový	k2eAgFnSc1d1
kanalizace	kanalizace	k1gFnSc1
s	s	k7c7
čističkou	čistička	k1gFnSc7
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
užívání	užívání	k1gNnSc2
byl	být	k5eAaImAgInS
předán	předat	k5eAaPmNgInS
Dům	dům	k1gInSc1
smutku	smutek	k1gInSc2
i	i	k8xC
multifunkční	multifunkční	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
<g/>
,	,	kIx,
zrekonstruováno	zrekonstruován	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
fotbalové	fotbalový	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměny	proměna	k1gFnPc4
se	se	k3xPyFc4
dočkalo	dočkat	k5eAaPmAgNnS
centrum	centrum	k1gNnSc1
obce	obec	k1gFnSc2
s	s	k7c7
nádvořím	nádvoří	k1gNnSc7
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
přibyl	přibýt	k5eAaPmAgInS
prostor	prostor	k1gInSc1
na	na	k7c4
společenské	společenský	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
rekonstrukce	rekonstrukce	k1gFnSc1
obecních	obecní	k2eAgInPc2d1
komunikaci	komunikace	k1gFnSc3
a	a	k8xC
vybudování	vybudování	k1gNnSc3
chodníků	chodník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
mateřská	mateřský	k2eAgFnSc1d1
a	a	k8xC
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
navštěvují	navštěvovat	k5eAaImIp3nP
i	i	k9
děti	dítě	k1gFnPc1
z	z	k7c2
Bukovce	bukovka	k1gFnSc6
<g/>
,	,	kIx,
Šemše	Šemcha	k1gFnSc6
<g/>
,	,	kIx,
Hodkovcí	Hodkovcí	k1gMnSc1
a	a	k8xC
Nováčan	Nováčan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
se	se	k3xPyFc4
rozrůstá	rozrůstat	k5eAaImIp3nS
díky	díky	k7c3
individuální	individuální	k2eAgFnSc3d1
bytové	bytový	k2eAgFnSc3d1
výstavbě	výstavba	k1gFnSc3
v	v	k7c6
lokalitě	lokalita	k1gFnSc6
Pánský	pánský	k2eAgInSc4d1
les	les	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
plně	plně	k6eAd1
plynofikovaná	plynofikovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
problémem	problém	k1gInSc7
obce	obec	k1gFnSc2
v	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
nedostatek	nedostatek	k1gInSc4
kvalitní	kvalitní	k2eAgFnSc2d1
pitné	pitný	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
a	a	k8xC
zajímavosti	zajímavost	k1gFnSc2
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Římskokatolický	římskokatolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Navštívení	navštívení	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
jednolodní	jednolodní	k2eAgFnSc1d1
klasicistní	klasicistní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
s	s	k7c7
půlkruhově	půlkruhově	k6eAd1
ukončeným	ukončený	k2eAgInSc7d1
presbytářem	presbytář	k1gInSc7
a	a	k8xC
věží	věžit	k5eAaImIp3nS
tvořící	tvořící	k2eAgFnSc4d1
součást	součást	k1gFnSc4
stavby	stavba	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1795	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interiér	interiér	k1gInSc1
je	být	k5eAaImIp3nS
zaklenut	zaklenout	k5eAaPmNgInS
pruskou	pruský	k2eAgFnSc7d1
klenbou	klenba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
s	s	k7c7
obrazem	obraz	k1gInSc7
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
kazatelna	kazatelna	k1gFnSc1
jsou	být	k5eAaImIp3nP
klasicistní	klasicistní	k2eAgInPc1d1
a	a	k8xC
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
doby	doba	k1gFnSc2
vzniku	vznik	k1gInSc2
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volné	volný	k2eAgInPc4d1
obrazy	obraz	k1gInPc4
Madona	Madona	k1gFnSc1
a	a	k8xC
sv.	sv.	kA
Jan	Jan	k1gMnSc1
Křtitel	křtitel	k1gMnSc1
jsou	být	k5eAaImIp3nP
dílem	dílem	k6eAd1
V.	V.	kA
Klimkovicsa	Klimkovicsa	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1850	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Fasáda	fasáda	k1gFnSc1
kostela	kostel	k1gInSc2
je	být	k5eAaImIp3nS
členěna	členěn	k2eAgFnSc1d1
lizénami	lizéna	k1gFnPc7
<g/>
,	,	kIx,
průčelí	průčelí	k1gNnSc1
je	být	k5eAaImIp3nS
ukončeno	ukončit	k5eAaPmNgNnS
trojúhelníkovým	trojúhelníkový	k2eAgInSc7d1
štítem	štít	k1gInSc7
s	s	k7c7
tympanonem	tympanon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věž	věž	k1gFnSc1
s	s	k7c7
korunní	korunní	k2eAgFnSc7d1
římsou	římsa	k1gFnSc7
a	a	k8xC
terčíkem	terčík	k1gInSc7
je	být	k5eAaImIp3nS
ukončena	ukončit	k5eAaPmNgFnS
střechou	střecha	k1gFnSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
jehlanu	jehlan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Navštívení	navštívení	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Akce	akce	k1gFnSc1
</s>
<s>
Dny	den	k1gInPc1
Malé	Malé	k2eAgFnSc2d1
Idy	Ida	k1gFnSc2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
v	v	k7c6
červnu	červen	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
Silvestrovská	silvestrovský	k2eAgFnSc1d1
párty	párty	k1gFnSc1
s	s	k7c7
ohňostrojem	ohňostroj	k1gInSc7
</s>
<s>
Školství	školství	k1gNnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
tzv.	tzv.	kA
středisková	střediskový	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soustřeďuje	soustřeďovat	k5eAaImIp3nS
žáky	žák	k1gMnPc4
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
sousedních	sousední	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obci	obec	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
také	také	k9
mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Tenisový	tenisový	k2eAgInSc1d1
Klub	klub	k1gInSc1
(	(	kIx(
<g/>
celoročně	celoročně	k6eAd1
otevřený	otevřený	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Golfový	golfový	k2eAgInSc1d1
Klub	klub	k1gInSc1
Red	Red	k1gFnPc2
Fox	fox	k1gInSc1
(	(	kIx(
<g/>
otevřeno	otevřít	k5eAaPmNgNnS
celoročně	celoročně	k6eAd1
<g/>
,	,	kIx,
konají	konat	k5eAaImIp3nP
se	se	k3xPyFc4
turnaje	turnaj	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
SR	SR	kA
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
.	.	kIx.
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
-	-	kIx~
Kostol	Kostol	k1gInSc1
Navštívenia	Navštívenium	k1gNnSc2
Panny	Panna	k1gFnSc2
Márie	Márie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Košice-okolí	Košice-okolí	k1gNnPc2
</s>
<s>
Bačkovík	Bačkovík	k1gInSc1
•	•	k?
Baška	Bašek	k1gMnSc2
•	•	k?
Belža	Belžus	k1gMnSc2
•	•	k?
Beniakovce	Beniakovec	k1gMnSc2
•	•	k?
Bidovce	Bidovec	k1gMnSc2
•	•	k?
Blažice	Blažic	k1gMnSc2
•	•	k?
Bočiar	Bočiar	k1gInSc1
•	•	k?
Bohdanovce	Bohdanovec	k1gMnSc2
•	•	k?
Boliarov	Boliarov	k1gInSc1
•	•	k?
Budimír	Budimír	k1gMnSc1
•	•	k?
Bukovec	Bukovec	k1gMnSc1
•	•	k?
Bunetice	Bunetika	k1gFnSc6
•	•	k?
Buzica	Buzic	k2eAgFnSc1d1
•	•	k?
Cestice	Cestice	k1gFnSc1
•	•	k?
Čakanovce	Čakanovec	k1gMnSc2
•	•	k?
Čaňa	Čaňus	k1gMnSc2
•	•	k?
Čečejovce	Čečejovec	k1gMnSc2
•	•	k?
Čižatice	Čižatice	k1gFnSc2
•	•	k?
Debraď	Debraď	k1gMnSc1
•	•	k?
Drienovec	Drienovec	k1gMnSc1
•	•	k?
Družstevná	Družstevný	k2eAgFnSc1d1
pri	pri	k?
Hornáde	Hornád	k1gInSc5
•	•	k?
Ďurďošík	Ďurďošík	k1gInSc1
•	•	k?
Ďurkov	Ďurkov	k1gInSc1
•	•	k?
Dvorníky-Včeláre	Dvorníky-Včelár	k1gInSc5
•	•	k?
Geča	Geč	k2eAgMnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Gyňov	Gyňov	k1gInSc1
•	•	k?
Hačava	Hačava	k1gFnSc1
•	•	k?
Háj	háj	k1gInSc1
•	•	k?
Haniska	Haniska	k1gFnSc1
•	•	k?
Herľany	Herľana	k1gFnSc2
•	•	k?
Hodkovce	Hodkovec	k1gMnSc2
•	•	k?
Hosťovce	Hosťovec	k1gMnSc2
•	•	k?
Hrašovík	Hrašovík	k1gInSc1
•	•	k?
Hýľov	Hýľov	k1gInSc1
•	•	k?
Chorváty	Chorvát	k1gMnPc4
•	•	k?
Chrastné	Chrastný	k2eAgFnSc2d1
•	•	k?
Janík	Janík	k1gMnSc1
•	•	k?
Jasov	Jasov	k1gInSc1
•	•	k?
Kalša	Kalšus	k1gMnSc2
•	•	k?
Kecerovce	Kecerovec	k1gMnSc2
•	•	k?
Kecerovský	Kecerovský	k2eAgInSc4d1
Lipovec	Lipovec	k1gInSc4
•	•	k?
Kechnec	Kechnec	k1gInSc1
•	•	k?
Kokšov-Bakša	Kokšov-Bakšus	k1gMnSc2
•	•	k?
Komárovce	Komárovec	k1gMnSc2
•	•	k?
Kostoľany	Kostoľana	k1gFnSc2
nad	nad	k7c4
Hornádom	Hornádom	k1gInSc4
•	•	k?
Košická	košický	k2eAgFnSc1d1
Belá	Belá	k1gFnSc1
•	•	k?
Košická	košický	k2eAgFnSc1d1
Polianka	Polianka	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Košické	košický	k2eAgFnSc2d1
Oľšany	Oľšana	k1gFnSc2
•	•	k?
Košický	košický	k2eAgInSc1d1
Klečenov	Klečenov	k1gInSc1
•	•	k?
Kráľovce	Kráľovec	k1gInSc2
•	•	k?
Kysak	Kysak	k1gMnSc1
•	•	k?
Malá	malý	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
•	•	k?
Malá	malý	k2eAgFnSc1d1
Lodina	Lodina	k1gFnSc1
•	•	k?
Medzev	Medzev	k1gFnSc2
•	•	k?
Milhosť	Milhosť	k1gFnSc2
•	•	k?
Mokrance	Mokrance	k1gFnSc2
•	•	k?
Moldava	Moldava	k1gFnSc1
nad	nad	k7c7
Bodvou	Bodva	k1gFnSc7
•	•	k?
Mudrovce	Mudrovka	k1gFnSc6
•	•	k?
Nižná	nižný	k2eAgFnSc1d1
Hutka	Hutka	k1gFnSc1
•	•	k?
Nižná	nižný	k2eAgFnSc1d1
Kamenica	Kamenica	k1gFnSc1
•	•	k?
Nižná	nižný	k2eAgFnSc1d1
Myšľa	Myšľa	k1gFnSc1
•	•	k?
Nižný	nižný	k2eAgInSc1d1
Čaj	čaj	k1gInSc1
•	•	k?
Nižný	nižný	k2eAgInSc1d1
Klátov	Klátov	k1gInSc1
•	•	k?
Nižný	nižný	k2eAgInSc1d1
Lánec	Lánec	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Polhora	Polhora	k1gFnSc1
•	•	k?
Nováčany	Nováčan	k1gMnPc7
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nový	nový	k2eAgInSc4d1
Salaš	salaš	k1gInSc4
•	•	k?
Obišovce	Obišovec	k1gInSc2
•	•	k?
Olšovany	Olšovan	k1gMnPc4
•	•	k?
Opátka	Opátka	k1gFnSc1
•	•	k?
Opiná	Opiná	k1gFnSc1
•	•	k?
Paňovce	Paňovec	k1gInSc2
•	•	k?
Peder	Peder	k1gMnSc1
•	•	k?
Perín-Chym	Perín-Chymum	k1gNnPc2
•	•	k?
Ploské	ploský	k2eAgFnSc2d1
•	•	k?
Poproč	Poproč	k1gMnSc1
•	•	k?
Rákoš	Rákoš	k1gMnSc1
•	•	k?
Rankovce	Rankovec	k1gInPc1
•	•	k?
Rešica	Rešic	k1gInSc2
•	•	k?
Rozhanovce	Rozhanovec	k1gMnSc2
•	•	k?
Rudník	rudník	k1gMnSc1
•	•	k?
Ruskov	Ruskov	k1gInSc1
•	•	k?
Sady	sada	k1gFnSc2
nad	nad	k7c7
Torysou	Torysa	k1gFnSc7
•	•	k?
Seňa	Seňa	k1gMnSc1
•	•	k?
Skároš	Skároš	k1gMnSc1
•	•	k?
Slančík	Slančík	k1gMnSc1
•	•	k?
Slanec	Slanec	k1gMnSc1
•	•	k?
Slanská	Slanská	k1gFnSc1
Huta	Huta	k1gFnSc1
•	•	k?
Slanské	Slanská	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Nové	Nová	k1gFnSc6
Mesto	Mesto	k1gNnSc1
•	•	k?
Sokoľ	Sokoľ	k1gMnSc1
•	•	k?
Sokoľany	Sokoľana	k1gFnSc2
•	•	k?
Svinica	Svinica	k1gMnSc1
•	•	k?
Šemša	Šemša	k1gMnSc1
•	•	k?
Štós	Štós	k1gInSc1
•	•	k?
Trebejov	Trebejov	k1gInSc1
•	•	k?
Trsťany	Trsťan	k1gMnPc4
•	•	k?
Trstené	Trstený	k2eAgNnSc1d1
pri	pri	k?
Hornáde	Hornád	k1gInSc5
•	•	k?
Turňa	Turňum	k1gNnSc2
nad	nad	k7c4
Bodvou	Bodvý	k2eAgFnSc4d1
•	•	k?
Turnianska	Turniansko	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Vajkovce	Vajkovec	k1gMnSc2
•	•	k?
Valaliky	Valalika	k1gFnSc2
•	•	k?
Veľká	Veľký	k2eAgFnSc1d1
Ida	Ida	k1gFnSc1
•	•	k?
Veľká	Veľký	k2eAgFnSc1d1
Lodina	Lodina	k1gFnSc1
•	•	k?
Vtáčkovce	Vtáčkovec	k1gInSc2
•	•	k?
Vyšná	vyšný	k2eAgFnSc1d1
Hutka	Hutka	k1gFnSc1
•	•	k?
Vyšná	vyšný	k2eAgFnSc1d1
Kamenica	Kamenica	k1gFnSc1
•	•	k?
Vyšná	vyšný	k2eAgFnSc1d1
Myšľa	Myšľa	k1gFnSc1
•	•	k?
Vyšný	vyšný	k2eAgInSc1d1
Čaj	čaj	k1gInSc1
•	•	k?
Vyšný	vyšný	k2eAgInSc1d1
Klátov	Klátov	k1gInSc1
•	•	k?
Vyšný	vyšný	k2eAgMnSc1d1
Medzev	Medzev	k1gMnSc1
•	•	k?
Zádiel	Zádiel	k1gMnSc1
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
Idka	Idka	k1gFnSc1
•	•	k?
Žarnov	Žarnov	k1gInSc1
•	•	k?
Ždaňa	Ždaňa	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
