<s>
NaN	NaN	k?	NaN
jako	jako	k8xS	jako
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
Not	nota	k1gFnPc2	nota
a	a	k8xC	a
Number	Numbra	k1gFnPc2	Numbra
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nečíslo	čísnout	k5eNaPmAgNnS	čísnout
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
číslicové	číslicový	k2eAgFnSc6d1	číslicová
technice	technika	k1gFnSc6	technika
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nereprezentuje	reprezentovat	k5eNaImIp3nS	reprezentovat
(	(	kIx(	(
<g/>
konečné	konečný	k2eAgNnSc4d1	konečné
<g/>
)	)	kIx)	)
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
NaN	NaN	k1gFnSc7	NaN
počítají	počítat	k5eAaImIp3nP	počítat
návrhy	návrh	k1gInPc1	návrh
numerických	numerický	k2eAgInPc2d1	numerický
koprocesorů	koprocesor	k1gInPc2	koprocesor
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
též	též	k9	též
jako	jako	k9	jako
rezervovaná	rezervovaný	k2eAgFnSc1d1	rezervovaná
hodnota	hodnota	k1gFnSc1	hodnota
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
programovacích	programovací	k2eAgInPc6d1	programovací
a	a	k8xC	a
skriptovacích	skriptovací	k2eAgInPc6d1	skriptovací
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
JavaScript	JavaScript	k2eAgInSc1d1	JavaScript
<g/>
)	)	kIx)	)
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
přímo	přímo	k6eAd1	přímo
tato	tento	k3xDgFnSc1	tento
zkratka	zkratka	k1gFnSc1	zkratka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zobrazena	zobrazit	k5eAaPmNgNnP	zobrazit
na	na	k7c6	na
displeji	displej	k1gInSc6	displej
některých	některý	k3yIgFnPc2	některý
starších	starý	k2eAgFnPc2d2	starší
kalkulaček	kalkulačka	k1gFnPc2	kalkulačka
<g/>
,	,	kIx,	,
měřicích	měřicí	k2eAgInPc2d1	měřicí
přístrojů	přístroj	k1gInPc2	přístroj
apod.	apod.	kA	apod.
Příklad	příklad	k1gInSc1	příklad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
NaN	NaN	k1gFnSc4	NaN
vzniknout	vzniknout	k5eAaPmF	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
nějaké	nějaký	k3yIgFnSc2	nějaký
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
výpočet	výpočet	k1gInSc4	výpočet
funkční	funkční	k2eAgFnSc2d1	funkční
hodnoty	hodnota	k1gFnSc2	hodnota
funkcí	funkce	k1gFnPc2	funkce
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
limitně	limitně	k6eAd1	limitně
blíží	blížit	k5eAaImIp3nS	blížit
k	k	k7c3	k
nekonečnu	nekonečno	k1gNnSc3	nekonečno
nebo	nebo	k8xC	nebo
který	který	k3yIgMnSc1	který
nenáleží	náležet	k5eNaImIp3nS	náležet
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
oboru	obor	k1gInSc2	obor
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
koprocesorů	koprocesor	k1gInPc2	koprocesor
je	být	k5eAaImIp3nS	být
NaN	NaN	k1gFnSc1	NaN
permutací	permutace	k1gFnPc2	permutace
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
dohodnutých	dohodnutý	k2eAgInPc2d1	dohodnutý
standardů	standard	k1gInPc2	standard
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
permutace	permutace	k1gFnSc1	permutace
nereprezentuje	reprezentovat	k5eNaImIp3nS	reprezentovat
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
též	též	k9	též
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
operace	operace	k1gFnSc1	operace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
operandem	operand	k1gInSc7	operand
je	on	k3xPp3gNnPc4	on
NaN	NaN	k1gMnSc1	NaN
má	mít	k5eAaImIp3nS	mít
výsledek	výsledek	k1gInSc4	výsledek
též	tenž	k3xDgFnSc2	tenž
NaN	NaN	k1gFnSc2	NaN
<g/>
.	.	kIx.	.
</s>
<s>
NULL	NULL	kA	NULL
IEEE	IEEE	kA	IEEE
754	[number]	k4	754
Číslicová	číslicový	k2eAgFnSc1d1	číslicová
technika	technika	k1gFnSc1	technika
http://foldoc.org/?Not-a-Number	[url]	k1gFnPc2	http://foldoc.org/?Not-a-Number
</s>
