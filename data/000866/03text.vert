<s>
Lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
<g/>
,	,	kIx,	,
též	též	k9	též
lípa	lípa	k1gFnSc1	lípa
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statný	statný	k2eAgInSc1d1	statný
strom	strom	k1gInSc1	strom
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slézovitých	slézovitý	k2eAgMnPc2d1	slézovitý
<g/>
.	.	kIx.	.
</s>
<s>
Tilia	Tilius	k1gMnSc4	Tilius
ulmifolia	ulmifolius	k1gMnSc4	ulmifolius
Scopoli	Scopole	k1gFnSc6	Scopole
<g/>
,	,	kIx,	,
1771	[number]	k4	1771
Tilia	Tilium	k1gNnSc2	Tilium
europaea	europaeum	k1gNnSc2	europaeum
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
parvifolia	parvifolia	k1gFnSc1	parvifolia
Ehrhart	Ehrharta	k1gFnPc2	Ehrharta
<g/>
,	,	kIx,	,
1780	[number]	k4	1780
Tilia	Tilia	k1gFnSc1	Tilia
parvifolia	parvifolia	k1gFnSc1	parvifolia
(	(	kIx(	(
<g/>
Ehrhart	Ehrhart	k1gInSc1	Ehrhart
<g/>
,	,	kIx,	,
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
Ehrhart	Ehrhart	k1gInSc4	Ehrhart
ex	ex	k6eAd1	ex
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
,	,	kIx,	,
1791	[number]	k4	1791
Tilia	Tilium	k1gNnSc2	Tilium
bohemica	bohemicum	k1gNnSc2	bohemicum
Opiz	Opiz	k1gMnSc1	Opiz
<g/>
,	,	kIx,	,
1852	[number]	k4	1852
Tilia	Tilium	k1gNnSc2	Tilium
betulifolia	betulifolium	k1gNnSc2	betulifolium
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
ex	ex	k6eAd1	ex
Bayer	Bayer	k1gMnSc1	Bayer
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
Tilia	Tilium	k1gNnSc2	Tilium
sibirica	sibiricum	k1gNnSc2	sibiricum
Bayer	Bayer	k1gMnSc1	Bayer
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
Tilia	Tilium	k1gNnSc2	Tilium
silvestris	silvestris	k1gFnPc2	silvestris
Desfontaines	Desfontainesa	k1gFnPc2	Desfontainesa
ex	ex	k6eAd1	ex
Bonnier	Bonnier	k1gInSc1	Bonnier
&	&	k?	&
Layens	Layens	k1gInSc1	Layens
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
Lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
je	být	k5eAaImIp3nS	být
statný	statný	k2eAgInSc1d1	statný
opadavý	opadavý	k2eAgInSc1d1	opadavý
listnatý	listnatý	k2eAgInSc1d1	listnatý
strom	strom	k1gInSc1	strom
s	s	k7c7	s
košatou	košatý	k2eAgFnSc7d1	košatá
<g/>
,	,	kIx,	,
vysoko	vysoko	k6eAd1	vysoko
klenutou	klenutý	k2eAgFnSc7d1	klenutá
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
30	[number]	k4	30
a	a	k8xC	a
více	hodně	k6eAd2	hodně
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Statný	statný	k2eAgInSc1d1	statný
kmen	kmen	k1gInSc1	kmen
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
tenkou	tenký	k2eAgFnSc7d1	tenká
<g/>
,	,	kIx,	,
tmavou	tmavý	k2eAgFnSc7d1	tmavá
a	a	k8xC	a
mělce	mělce	k6eAd1	mělce
podélně	podélně	k6eAd1	podélně
zvrásnělou	zvrásnělý	k2eAgFnSc7d1	zvrásnělá
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Pupeny	pupen	k1gInPc1	pupen
jsou	být	k5eAaImIp3nP	být
černohnědé	černohnědý	k2eAgInPc1d1	černohnědý
<g/>
,	,	kIx,	,
vejcovité	vejcovitý	k2eAgInPc1d1	vejcovitý
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
dlouze	dlouho	k6eAd1	dlouho
řapíkaté	řapíkatý	k2eAgInPc1d1	řapíkatý
<g/>
,	,	kIx,	,
nesouměrně	souměrně	k6eNd1	souměrně
srdčité	srdčitý	k2eAgInPc1d1	srdčitý
a	a	k8xC	a
lysé	lysý	k2eAgInPc1d1	lysý
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
úhlech	úhel	k1gInPc6	úhel
velkých	velký	k2eAgFnPc2d1	velká
žilek	žilka	k1gFnPc2	žilka
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
listů	list	k1gInPc2	list
mají	mít	k5eAaImIp3nP	mít
rezavé	rezavý	k2eAgInPc1d1	rezavý
chomáčky	chomáček	k1gInPc1	chomáček
chlupů	chlup	k1gInPc2	chlup
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lípy	lípa	k1gFnSc2	lípa
velkolisté	velkolistý	k2eAgFnSc2d1	velkolistá
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
má	mít	k5eAaImIp3nS	mít
bělavé	bělavý	k2eAgNnSc1d1	bělavé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
oboupohlavné	oboupohlavný	k2eAgInPc1d1	oboupohlavný
<g/>
,	,	kIx,	,
žlutavě	žlutavě	k6eAd1	žlutavě
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
stopka	stopka	k1gFnSc1	stopka
vrcholíků	vrcholík	k1gInPc2	vrcholík
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
srostlá	srostlý	k2eAgFnSc1d1	srostlá
s	s	k7c7	s
jazykovým	jazykový	k2eAgInSc7d1	jazykový
blanitým	blanitý	k2eAgInSc7d1	blanitý
listenem	listen	k1gInSc7	listen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
květu	květ	k1gInSc2	květ
omamně	omamně	k6eAd1	omamně
voní	vonět	k5eAaImIp3nP	vonět
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
jednopouzdrý	jednopouzdrý	k2eAgInSc1d1	jednopouzdrý
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
oříšek	oříšek	k1gInSc1	oříšek
s	s	k7c7	s
tenkostěnným	tenkostěnný	k2eAgNnSc7d1	tenkostěnné
oplodím	oplodí	k1gNnSc7	oplodí
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
s	s	k7c7	s
lípou	lípa	k1gFnSc7	lípa
velkolistou	velkolistý	k2eAgFnSc7d1	velkolistá
(	(	kIx(	(
<g/>
výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc1d1	uvedený
rozpoznávací	rozpoznávací	k2eAgInSc1d1	rozpoznávací
znak	znak	k1gInSc1	znak
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
čistých	čistá	k1gFnPc2	čistá
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
existenci	existence	k1gFnSc3	existence
kříženců	kříženec	k1gMnPc2	kříženec
nezohledňuje	zohledňovat	k5eNaImIp3nS	zohledňovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kříženec	kříženec	k1gMnSc1	kříženec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
lípa	lípa	k1gFnSc1	lípa
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
x	x	k?	x
vulgaris	vulgaris	k1gInSc1	vulgaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
však	však	k9	však
vysazován	vysazovat	k5eAaImNgInS	vysazovat
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
parcích	park	k1gInPc6	park
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
původní	původní	k2eAgMnSc1d1	původní
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
V	V	kA	V
až	až	k9	až
po	po	k7c4	po
z.	z.	k?	z.
Sibiř	Sibiř	k1gFnSc4	Sibiř
a	a	k8xC	a
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
nad	nad	k7c7	nad
900	[number]	k4	900
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
však	však	k9	však
většinou	většinou	k6eAd1	většinou
zcela	zcela	k6eAd1	zcela
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
hojně	hojně	k6eAd1	hojně
v	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc6d1	smíšený
lesích	les	k1gInPc6	les
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
horských	horský	k2eAgFnPc2d1	horská
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typickou	typický	k2eAgFnSc7d1	typická
příměsí	příměs	k1gFnSc7	příměs
dubohabřin	dubohabřina	k1gFnPc2	dubohabřina
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Carpinion	Carpinion	k1gInSc1	Carpinion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
suťových	suťový	k2eAgInPc2d1	suťový
lesů	les	k1gInPc2	les
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Tilio-Acerion	Tilio-Acerion	k1gInSc1	Tilio-Acerion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lipových	lipový	k2eAgFnPc2d1	Lipová
bučin	bučina	k1gFnPc2	bučina
(	(	kIx(	(
<g/>
Tilio	Tilio	k6eAd1	Tilio
cordatae-Fagetum	cordatae-Fagetum	k1gNnSc1	cordatae-Fagetum
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
sušších	suchý	k2eAgFnPc6d2	sušší
variantách	varianta	k1gFnPc6	varianta
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
luhů	luh	k1gInPc2	luh
(	(	kIx(	(
<g/>
podsv	podsva	k1gFnPc2	podsva
<g/>
.	.	kIx.	.
</s>
<s>
Ulmenion	Ulmenion	k1gInSc1	Ulmenion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přirozeném	přirozený	k2eAgInSc6d1	přirozený
stavu	stav	k1gInSc6	stav
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k9	jen
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Monokultury	monokultura	k1gFnPc4	monokultura
lípy	lípa	k1gFnSc2	lípa
malolisté	malolistý	k2eAgFnSc2d1	malolistá
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
uměle	uměle	k6eAd1	uměle
založeny	založit	k5eAaPmNgFnP	založit
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
v	v	k7c6	v
parcích	park	k1gInPc6	park
a	a	k8xC	a
stromořadích	stromořadí	k1gNnPc6	stromořadí
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
především	především	k9	především
v	v	k7c6	v
lipovém	lipový	k2eAgInSc6d1	lipový
květu	květ	k1gInSc6	květ
<g/>
.	.	kIx.	.
</s>
<s>
Silice	silice	k1gFnSc1	silice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nepatrné	patrný	k2eNgNnSc4d1	nepatrné
množství	množství	k1gNnSc4	množství
alkanů	alkan	k1gInPc2	alkan
<g/>
,	,	kIx,	,
především	především	k9	především
<g/>
:	:	kIx,	:
hexan	hexan	k1gInSc1	hexan
heptan	heptan	k1gInSc1	heptan
kyselina	kyselina	k1gFnSc1	kyselina
kávová	kávový	k2eAgFnSc1d1	kávová
kyselina	kyselina	k1gFnSc1	kyselina
p-kumarová	pumarová	k1gFnSc1	p-kumarová
kyselina	kyselina	k1gFnSc1	kyselina
chlorogenová	chlorogenová	k1gFnSc1	chlorogenová
soli	sůl	k1gFnSc2	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
jablečné	jablečný	k2eAgFnSc2d1	jablečná
(	(	kIx(	(
<g/>
jablečnany	jablečnana	k1gFnSc2	jablečnana
<g/>
)	)	kIx)	)
soli	sůl	k1gFnSc2	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
vinné	vinný	k2eAgInPc1d1	vinný
(	(	kIx(	(
<g/>
vínany	vínan	k1gInPc1	vínan
<g/>
)	)	kIx)	)
Květy	květ	k1gInPc1	květ
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
3	[number]	k4	3
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
slizových	slizový	k2eAgInPc2d1	slizový
polysacharidů	polysacharid	k1gInPc2	polysacharid
<g/>
,	,	kIx,	,
především	především	k9	především
<g/>
:	:	kIx,	:
arabinogalaktany	arabinogalaktan	k1gInPc4	arabinogalaktan
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g />
.	.	kIx.	.
</s>
<s>
polysacharidy	polysacharid	k1gInPc1	polysacharid
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
jsou	být	k5eAaImIp3nP	být
obsaženy	obsažen	k2eAgInPc1d1	obsažen
následující	následující	k2eAgInPc1d1	následující
monosacharidy	monosacharid	k1gInPc1	monosacharid
<g/>
:	:	kIx,	:
rhamnosa	rhamnosa	k1gFnSc1	rhamnosa
galaktosa	galaktosa	k1gFnSc1	galaktosa
arabinosa	arabinosa	k1gFnSc1	arabinosa
xylosa	xylosa	k1gFnSc1	xylosa
manosa	manosa	k1gFnSc1	manosa
glukosa	glukosa	k1gFnSc1	glukosa
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
kyseliny	kyselina	k1gFnPc1	kyselina
uronové	uronový	k2eAgFnPc1d1	uronový
hesperidin	hesperidin	k1gInSc4	hesperidin
rutin	rutina	k1gFnPc2	rutina
hyperosid	hyperosida	k1gFnPc2	hyperosida
kvercitrin	kvercitrin	k1gInSc1	kvercitrin
isokvercitrin	isokvercitrin	k1gInSc1	isokvercitrin
astragalin	astragalin	k2eAgInSc1d1	astragalin
tilirosid	tilirosid	k1gInSc1	tilirosid
kempferol-	kempferol-	k?	kempferol-
<g/>
3	[number]	k4	3
<g/>
-O-glukosyl-	-Olukosyl-	k?	-O-glukosyl-
<g/>
7	[number]	k4	7
<g/>
-O-rhamnosid	-Ohamnosid	k1gInSc1	-O-rhamnosid
kempferol-	kempferol-	k?	kempferol-
<g/>
3,7	[number]	k4	3,7
<g/>
-O-dirhamnosid	-Oirhamnosid	k1gInSc1	-O-dirhamnosid
kvercetin-	kvercetin-	k?	kvercetin-
<g/>
3	[number]	k4	3
<g/>
-O-glukosyl-	-Olukosyl-	k?	-O-glukosyl-
<g/>
7	[number]	k4	7
<g/>
-O-rhamnosid	-Ohamnosid	k1gInSc1	-O-rhamnosid
tiliacin	tiliacin	k2eAgInSc4d1	tiliacin
farnesol	farnesol	k1gInSc4	farnesol
geraniol	geraniol	k1gInSc1	geraniol
nerolidol	nerolidol	k1gInSc1	nerolidol
eugenol	eugenol	k1gInSc1	eugenol
kempferol	kempferol	k1gInSc1	kempferol
tokoferol	tokoferol	k1gInSc1	tokoferol
proantokyanidiny	proantokyanidin	k2eAgInPc1d1	proantokyanidin
(	(	kIx(	(
<g/>
kondenzované	kondenzovaný	k2eAgInPc1d1	kondenzovaný
taniny	tanin	k1gInPc1	tanin
<g/>
)	)	kIx)	)
jód	jód	k1gInSc1	jód
mangan	mangan	k1gInSc1	mangan
Lípa	lípa	k1gFnSc1	lípa
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
užitečný	užitečný	k2eAgInSc1d1	užitečný
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Vysazuje	vysazovat	k5eAaImIp3nS	vysazovat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
okrasný	okrasný	k2eAgInSc1d1	okrasný
a	a	k8xC	a
stínící	stínící	k2eAgInSc1d1	stínící
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
medonosný	medonosný	k2eAgInSc1d1	medonosný
strom	strom	k1gInSc1	strom
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgFnPc1d1	ceněná
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
<g/>
.	.	kIx.	.
</s>
<s>
Lipové	lipový	k2eAgNnSc1d1	lipové
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
měkké	měkký	k2eAgNnSc1d1	měkké
a	a	k8xC	a
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
opracovatelné	opracovatelný	k2eAgInPc1d1	opracovatelný
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgInPc1d1	ceněný
řezbáři	řezbář	k1gMnPc5	řezbář
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
vyřezáváním	vyřezávání	k1gNnSc7	vyřezávání
zdobeného	zdobený	k2eAgInSc2d1	zdobený
nábytku	nábytek	k1gInSc2	nábytek
i	i	k9	i
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
řezbářské	řezbářský	k2eAgFnSc3d1	řezbářská
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
tzv.	tzv.	kA	tzv.
aktivovaného	aktivovaný	k2eAgNnSc2d1	aktivované
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
i	i	k9	i
použití	použití	k1gNnSc1	použití
jako	jako	k8xS	jako
živočišné	živočišný	k2eAgNnSc1d1	živočišné
uhlí	uhlí	k1gNnSc1	uhlí
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
Carbofit	Carbofita	k1gFnPc2	Carbofita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
droga	droga	k1gFnSc1	droga
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
květ	květ	k1gInSc1	květ
i	i	k9	i
s	s	k7c7	s
listenem	listen	k1gInSc7	listen
(	(	kIx(	(
<g/>
Flos	Flos	k1gInSc1	Flos
tiliae	tilia	k1gInSc2	tilia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
před	před	k7c7	před
plným	plný	k2eAgInSc7d1	plný
rozkvětem	rozkvět	k1gInSc7	rozkvět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nesmějí	smát	k5eNaImIp3nP	smát
být	být	k5eAaImF	být
plody	plod	k1gInPc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Natrhané	natrhaný	k2eAgInPc1d1	natrhaný
květy	květ	k1gInPc1	květ
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nP	sušit
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
lipového	lipový	k2eAgInSc2d1	lipový
čaje	čaj	k1gInSc2	čaj
nebo	nebo	k8xC	nebo
kombinovaných	kombinovaný	k2eAgFnPc2d1	kombinovaná
léčivých	léčivý	k2eAgFnPc2d1	léčivá
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sběru	sběr	k1gInSc6	sběr
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
záměnu	záměna	k1gFnSc4	záměna
s	s	k7c7	s
lípou	lípa	k1gFnSc7	lípa
stříbrnou	stříbrný	k2eAgFnSc7d1	stříbrná
nebo	nebo	k8xC	nebo
lípou	lípa	k1gFnSc7	lípa
americkou	americký	k2eAgFnSc7d1	americká
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
květy	květ	k1gInPc4	květ
nemají	mít	k5eNaImIp3nP	mít
žádané	žádaný	k2eAgInPc4d1	žádaný
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Záměna	záměna	k1gFnSc1	záměna
s	s	k7c7	s
lípou	lípa	k1gFnSc7	lípa
velkolistou	velkolistý	k2eAgFnSc7d1	velkolistá
či	či	k8xC	či
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
křížencem	kříženec	k1gMnSc7	kříženec
s	s	k7c7	s
lípou	lípa	k1gFnSc7	lípa
malolistou	malolistý	k2eAgFnSc7d1	malolistá
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
závadu	závada	k1gFnSc4	závada
<g/>
.	.	kIx.	.
</s>
<s>
Účinné	účinný	k2eAgFnPc1d1	účinná
látky	látka	k1gFnPc1	látka
mají	mít	k5eAaImIp3nP	mít
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
použití	použití	k1gNnSc2	použití
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mají	mít	k5eAaImIp3nP	mít
účinky	účinek	k1gInPc1	účinek
antispasmatické	antispasmatický	k2eAgInPc1d1	antispasmatický
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
křečím	křeč	k1gFnPc3	křeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diaforetické	diaforetický	k2eAgInPc1d1	diaforetický
(	(	kIx(	(
<g/>
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
pocení	pocení	k1gNnSc4	pocení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedativní	sedativní	k2eAgFnSc1d1	sedativní
(	(	kIx(	(
<g/>
uklidňující	uklidňující	k2eAgFnSc1d1	uklidňující
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hypotensivní	hypotensivní	k2eAgInSc4d1	hypotensivní
(	(	kIx(	(
<g/>
snižující	snižující	k2eAgInSc4d1	snižující
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvláčňující	zvláčňující	k2eAgFnSc4d1	zvláčňující
pokožku	pokožka	k1gFnSc4	pokožka
a	a	k8xC	a
slabě	slabě	k6eAd1	slabě
astringentní	astringentní	k2eAgFnPc1d1	astringentní
(	(	kIx(	(
<g/>
svíravé	svíravý	k2eAgFnPc1d1	svíravá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc1d1	používaný
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
bezem	bez	k1gInSc7	bez
černým	černý	k2eAgInSc7d1	černý
nebo	nebo	k8xC	nebo
hluchavkou	hluchavka	k1gFnSc7	hluchavka
bílou	bílý	k2eAgFnSc7d1	bílá
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
onemocněních	onemocnění	k1gNnPc6	onemocnění
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
nachlazení	nachlazení	k1gNnSc6	nachlazení
a	a	k8xC	a
při	při	k7c6	při
kašli	kašel	k1gInSc6	kašel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
hleny	hlen	k1gInPc4	hlen
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
užíván	užíván	k2eAgInSc1d1	užíván
při	při	k7c6	při
potížích	potíž	k1gFnPc6	potíž
s	s	k7c7	s
ledvinami	ledvina	k1gFnPc7	ledvina
a	a	k8xC	a
močovým	močový	k2eAgInSc7d1	močový
měchýřem	měchýř	k1gInSc7	měchýř
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
užitečnost	užitečnost	k1gFnSc1	užitečnost
i	i	k9	i
při	při	k7c6	při
slabších	slabý	k2eAgInPc6d2	slabší
problémech	problém	k1gInPc6	problém
se	s	k7c7	s
žlučníkem	žlučník	k1gInSc7	žlučník
<g/>
.	.	kIx.	.
</s>
<s>
Reguluje	regulovat	k5eAaImIp3nS	regulovat
činnost	činnost	k1gFnSc4	činnost
trávicího	trávicí	k2eAgNnSc2d1	trávicí
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odvar	odvar	k1gInSc1	odvar
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
nervovém	nervový	k2eAgNnSc6d1	nervové
napětí	napětí	k1gNnSc6	napětí
a	a	k8xC	a
úzkostlivosti	úzkostlivost	k1gFnSc6	úzkostlivost
<g/>
,	,	kIx,	,
snižuje	snižovat	k5eAaImIp3nS	snižovat
cholesterol	cholesterol	k1gInSc4	cholesterol
a	a	k8xC	a
zpevňuje	zpevňovat	k5eAaImIp3nS	zpevňovat
cévy	céva	k1gFnPc4	céva
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc1	lista
spařené	spařený	k2eAgFnPc1d1	spařená
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
smíchané	smíchaný	k2eAgNnSc4d1	smíchané
s	s	k7c7	s
vínem	víno	k1gNnSc7	víno
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
obklady	obklad	k1gInPc1	obklad
při	při	k7c6	při
popáleninách	popálenina	k1gFnPc6	popálenina
a	a	k8xC	a
svalových	svalový	k2eAgFnPc6d1	svalová
křečích	křeč	k1gFnPc6	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Nektar	nektar	k1gInSc1	nektar
sbíraný	sbíraný	k2eAgInSc1d1	sbíraný
včelami	včela	k1gFnPc7	včela
z	z	k7c2	z
květů	květ	k1gInPc2	květ
lípy	lípa	k1gFnSc2	lípa
má	mít	k5eAaImIp3nS	mít
nezaměnitelnou	zaměnitelný	k2eNgFnSc4d1	nezaměnitelná
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
lipového	lipový	k2eAgInSc2d1	lipový
medu	med	k1gInSc2	med
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lidovém	lidový	k2eAgNnSc6d1	lidové
léčitelství	léčitelství	k1gNnSc6	léčitelství
<g/>
.	.	kIx.	.
</s>
<s>
Lipové	lipový	k2eAgNnSc1d1	lipové
lýko	lýko	k1gNnSc1	lýko
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
obuvi	obuv	k1gFnSc2	obuv
a	a	k8xC	a
provazů	provaz	k1gInPc2	provaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
košíkářství	košíkářství	k1gNnSc6	košíkářství
a	a	k8xC	a
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dekorací	dekorace	k1gFnPc2	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Nejmohutnější	mohutný	k2eAgFnPc1d3	nejmohutnější
památné	památný	k2eAgFnPc1d1	památná
lípy	lípa	k1gFnPc1	lípa
malolisté	malolistý	k2eAgFnSc2d1	malolistá
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
Tatrovická	Tatrovický	k2eAgFnSc1d1	Tatrovická
lípa	lípa	k1gFnSc1	lípa
-	-	kIx~	-
1122	[number]	k4	1122
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Horní	horní	k2eAgFnSc1d1	horní
Popovská	popovský	k2eAgFnSc1d1	Popovská
lípa	lípa	k1gFnSc1	lípa
-	-	kIx~	-
907	[number]	k4	907
cm	cm	kA	cm
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
-	-	kIx~	-
870	[number]	k4	870
cm	cm	kA	cm
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
lípa	lípa	k1gFnSc1	lípa
-	-	kIx~	-
856	[number]	k4	856
cm	cm	kA	cm
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Některé	některý	k3yIgInPc1	některý
stromy	strom	k1gInPc1	strom
(	(	kIx(	(
<g/>
vlivem	vlivem	k7c2	vlivem
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
chorob	choroba	k1gFnPc2	choroba
či	či	k8xC	či
povětrnostních	povětrnostní	k2eAgFnPc2d1	povětrnostní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
)	)	kIx)	)
již	již	k9	již
o	o	k7c4	o
část	část	k1gFnSc4	část
kmene	kmen	k1gInSc2	kmen
přišly	přijít	k5eAaPmAgInP	přijít
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rekordních	rekordní	k2eAgFnPc2d1	rekordní
hodnot	hodnota	k1gFnPc2	hodnota
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
Tatrovická	Tatrovický	k2eAgFnSc1d1	Tatrovická
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
do	do	k7c2	do
vichřice	vichřice	k1gFnSc2	vichřice
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
obvod	obvod	k1gInSc1	obvod
1650	[number]	k4	1650
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
na	na	k7c4	na
Babí	babí	k2eAgInSc4d1	babí
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
kmen	kmen	k1gInSc1	kmen
o	o	k7c4	o
947	[number]	k4	947
cm	cm	kA	cm
poškodil	poškodit	k5eAaPmAgInS	poškodit
blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Křemenité	křemenitý	k2eAgFnSc6d1	křemenitá
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
obvodem	obvod	k1gInSc7	obvod
946	[number]	k4	946
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Maškovická	Maškovický	k2eAgFnSc1d1	Maškovický
lípa	lípa	k1gFnSc1	lípa
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
odumřelým	odumřelý	k2eAgInSc7d1	odumřelý
kmenem	kmen	k1gInSc7	kmen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dříve	dříve	k6eAd2	dříve
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
933	[number]	k4	933
cm	cm	kA	cm
a	a	k8xC	a
Jemčinská	Jemčinský	k2eAgFnSc1d1	Jemčinská
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
o	o	k7c4	o
původní	původní	k2eAgInPc4d1	původní
kmen	kmen	k1gInSc4	kmen
o	o	k7c6	o
obvodu	obvod	k1gInSc6	obvod
přes	přes	k7c4	přes
850	[number]	k4	850
cm	cm	kA	cm
přišla	přijít	k5eAaPmAgFnS	přijít
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nejstarší	starý	k2eAgFnSc2d3	nejstarší
živé	živý	k2eAgFnSc2d1	živá
lípy	lípa	k1gFnSc2	lípa
malolisté	malolistý	k2eAgFnSc2d1	malolistá
jsou	být	k5eAaImIp3nP	být
považované	považovaný	k2eAgMnPc4d1	považovaný
Žeberská	Žeberský	k2eAgFnSc1d1	Žeberská
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
700	[number]	k4	700
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
(	(	kIx(	(
<g/>
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
na	na	k7c4	na
Babí	babí	k2eAgInPc4d1	babí
(	(	kIx(	(
<g/>
550	[number]	k4	550
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maškovická	Maškovický	k2eAgFnSc1d1	Maškovický
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
Horní	horní	k2eAgFnSc1d1	horní
Popovská	popovský	k2eAgFnSc1d1	Popovská
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
viz	vidět	k5eAaImRp2nS	vidět
články	článek	k1gInPc4	článek
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Památné	památný	k2eAgFnSc2d1	památná
lípy	lípa	k1gFnSc2	lípa
malolisté	malolistý	k2eAgFnSc2d1	malolistá
<g/>
.	.	kIx.	.
</s>
<s>
Alej	alej	k1gFnSc4	alej
ke	k	k7c3	k
sv.	sv.	kA	sv.
Anně	Anna	k1gFnSc6	Anna
Alej	alej	k1gFnSc1	alej
u	u	k7c2	u
minerálního	minerální	k2eAgInSc2d1	minerální
pramene	pramen	k1gInSc2	pramen
Alej	alej	k1gFnSc4	alej
u	u	k7c2	u
náhonu	náhon	k1gInSc2	náhon
Alej	alej	k1gFnSc1	alej
vzdechů	vzdech	k1gInPc2	vzdech
Alfrédovská	Alfrédovský	k2eAgFnSc1d1	Alfrédovský
alej	alej	k1gFnSc1	alej
Kilometrovka	Kilometrovka	k1gFnSc1	Kilometrovka
Krušecká	Krušecký	k2eAgFnSc1d1	Krušecký
lipová	lipový	k2eAgFnSc1d1	Lipová
alej	alej	k1gFnSc1	alej
Lipová	lipový	k2eAgFnSc1d1	Lipová
alej	alej	k1gFnSc1	alej
ve	v	k7c6	v
Žlebech	žleb	k1gInPc6	žleb
Lipové	lipový	k2eAgNnSc1d1	lipové
stromořadí	stromořadí	k1gNnPc1	stromořadí
v	v	k7c6	v
Rosicích	Rosice	k1gFnPc6	Rosice
Trhanovská	Trhanovský	k2eAgFnSc1d1	Trhanovský
alej	alej	k1gFnSc1	alej
Valdštejnova	Valdštejnův	k2eAgFnSc1d1	Valdštejnova
alej	alej	k1gFnSc1	alej
</s>
