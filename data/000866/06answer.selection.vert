<s>
Lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
<g/>
,	,	kIx,	,
též	též	k9	též
lípa	lípa	k1gFnSc1	lípa
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statný	statný	k2eAgInSc1d1	statný
strom	strom	k1gInSc1	strom
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
slézovitých	slézovitý	k2eAgMnPc2d1	slézovitý
<g/>
.	.	kIx.	.
</s>
