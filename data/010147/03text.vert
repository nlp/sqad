<p>
<s>
Periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
též	též	k9	též
periodická	periodický	k2eAgFnSc1d1	periodická
soustava	soustava	k1gFnSc1	soustava
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uspořádání	uspořádání	k1gNnSc3	uspořádání
všech	všecek	k3xTgInPc2	všecek
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
prvky	prvek	k1gInPc1	prvek
seskupeny	seskupen	k2eAgInPc1d1	seskupen
podle	podle	k7c2	podle
rostoucích	rostoucí	k2eAgNnPc2d1	rostoucí
protonových	protonový	k2eAgNnPc2d1	protonové
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
elektronové	elektronový	k2eAgFnPc1d1	elektronová
konfigurace	konfigurace	k1gFnPc1	konfigurace
a	a	k8xC	a
cyklicky	cyklicky	k6eAd1	cyklicky
se	se	k3xPyFc4	se
opakujících	opakující	k2eAgFnPc2d1	opakující
podobných	podobný	k2eAgFnPc2d1	podobná
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
periodickým	periodický	k2eAgInSc7d1	periodický
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
seřadil	seřadit	k5eAaPmAgInS	seřadit
prvky	prvek	k1gInPc7	prvek
podle	podle	k7c2	podle
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
hmotnosti	hmotnost	k1gFnSc2	hmotnost
jejich	jejich	k3xOp3gInPc2	jejich
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
používaný	používaný	k2eAgInSc4d1	používaný
název	název	k1gInSc4	název
Mendělejevova	Mendělejevův	k2eAgFnSc1d1	Mendělejevova
tabulka	tabulka	k1gFnSc1	tabulka
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
striktně	striktně	k6eAd1	striktně
vzato	vzít	k5eAaPmNgNnS	vzít
pouze	pouze	k6eAd1	pouze
názvem	název	k1gInSc7	název
původní	původní	k2eAgFnSc2d1	původní
Mendělejevovy	Mendělejevův	k2eAgFnSc2d1	Mendělejevova
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
150	[number]	k4	150
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
publikace	publikace	k1gFnSc2	publikace
periodického	periodický	k2eAgInSc2d1	periodický
zákona	zákon	k1gInSc2	zákon
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
OSN	OSN	kA	OSN
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
za	za	k7c4	za
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
rok	rok	k1gInSc4	rok
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc4	některý
lehčí	lehký	k2eAgInPc4d2	lehčí
prvky	prvek	k1gInPc4	prvek
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
za	za	k7c7	za
těžšími	těžký	k2eAgInPc7d2	těžší
(	(	kIx(	(
<g/>
jod	jod	k1gInSc1	jod
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
tellurem	tellur	k1gInSc7	tellur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
Henry	henry	k1gInSc4	henry
Moseley	Moselea	k1gFnSc2	Moselea
opravil	opravit	k5eAaPmAgMnS	opravit
periodický	periodický	k2eAgInSc4d1	periodický
zákon	zákon	k1gInSc4	zákon
podle	podle	k7c2	podle
rostoucích	rostoucí	k2eAgNnPc2d1	rostoucí
protonových	protonový	k2eAgNnPc2d1	protonové
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
kromě	kromě	k7c2	kromě
chemického	chemický	k2eAgInSc2d1	chemický
symbolu	symbol	k1gInSc2	symbol
prvku	prvek	k1gInSc2	prvek
uvedeno	uvést	k5eAaPmNgNnS	uvést
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
relativní	relativní	k2eAgFnSc1d1	relativní
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
další	další	k2eAgInPc4d1	další
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
prvcích	prvek	k1gInPc6	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
118	[number]	k4	118
známých	známý	k2eAgInPc2d1	známý
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
94	[number]	k4	94
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
;	;	kIx,	;
zbylé	zbylý	k2eAgFnPc1d1	zbylá
byly	být	k5eAaImAgFnP	být
připraveny	připraven	k2eAgFnPc1d1	připravena
pouze	pouze	k6eAd1	pouze
uměle	uměle	k6eAd1	uměle
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
nebyl	být	k5eNaImAgMnS	být
objeven	objeven	k2eAgMnSc1d1	objeven
žádný	žádný	k1gMnSc1	žádný
jejich	jejich	k3xOp3gInSc4	jejich
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
uspořádání	uspořádání	k1gNnSc2	uspořádání
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
seskupení	seskupení	k1gNnSc1	seskupení
podle	podle	k7c2	podle
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
ležely	ležet	k5eAaImAgInP	ležet
prvky	prvek	k1gInPc1	prvek
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
valenčních	valenční	k2eAgInPc2d1	valenční
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
společné	společný	k2eAgFnSc6d1	společná
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
i	i	k9	i
podobné	podobný	k2eAgFnPc4d1	podobná
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
dělení	dělení	k1gNnSc2	dělení
skupin	skupina	k1gFnPc2	skupina
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
vedlejší	vedlejší	k2eAgNnSc4d1	vedlejší
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc4	prvek
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
skupinách	skupina	k1gFnPc6	skupina
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
ve	v	k7c6	v
sférách	sféra	k1gFnPc6	sféra
s	s	k7c7	s
a	a	k8xC	a
p	p	k?	p
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc4	prvek
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
skupin	skupina	k1gFnPc2	skupina
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
do	do	k7c2	do
slupek	slupka	k1gFnPc2	slupka
d	d	k?	d
a	a	k8xC	a
f.	f.	k?	f.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
skupiny	skupina	k1gFnSc2	skupina
prvků	prvek	k1gInPc2	prvek
jsou	být	k5eAaImIp3nP	být
alkalické	alkalický	k2eAgInPc1d1	alkalický
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
)	)	kIx)	)
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
skupině	skupina	k1gFnSc6	skupina
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
valenční	valenční	k2eAgInSc1d1	valenční
elektron	elektron	k1gInSc1	elektron
ve	v	k7c6	v
slupce	slupka	k1gFnSc6	slupka
s.	s.	k?	s.
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
halogeny	halogen	k1gInPc1	halogen
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc1	prvek
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgFnSc3d1	hlavní
skupině	skupina	k1gFnSc3	skupina
prvků	prvek	k1gInPc2	prvek
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
valenčními	valenční	k2eAgInPc7d1	valenční
elektrony	elektron	k1gInPc7	elektron
–	–	k?	–
dvěma	dva	k4xCgFnPc7	dva
ve	v	k7c6	v
slupce	slupka	k1gFnSc6	slupka
s	s	k7c7	s
a	a	k8xC	a
5	[number]	k4	5
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
p.	p.	k?	p.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
tabulek	tabulka	k1gFnPc2	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
formy	forma	k1gFnPc1	forma
tabulky	tabulka	k1gFnSc2	tabulka
</s>
</p>
<p>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
18	[number]	k4	18
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
vnitřně	vnitřně	k6eAd1	vnitřně
přechodné	přechodný	k2eAgInPc1d1	přechodný
kovy	kov	k1gInPc1	kov
jsou	být	k5eAaImIp3nP	být
vyjmuty	vyjmout	k5eAaPmNgInP	vyjmout
mimo	mimo	k7c4	mimo
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
;	;	kIx,	;
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
unie	unie	k1gFnSc1	unie
pro	pro	k7c4	pro
čistou	čistý	k2eAgFnSc4d1	čistá
a	a	k8xC	a
užitou	užitý	k2eAgFnSc4d1	užitá
chemii	chemie	k1gFnSc4	chemie
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
32	[number]	k4	32
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
vnitřně	vnitřně	k6eAd1	vnitřně
přechodné	přechodný	k2eAgInPc1d1	přechodný
kovy	kov	k1gInPc1	kov
jsou	být	k5eAaImIp3nP	být
vmezeřeny	vmezeřit	k5eAaPmNgInP	vmezeřit
mezi	mezi	k7c4	mezi
blok	blok	k1gInSc4	blok
s	s	k7c7	s
a	a	k8xC	a
přechodné	přechodný	k2eAgInPc4d1	přechodný
kovy	kov	k1gInPc4	kov
<g/>
;	;	kIx,	;
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
korektnost	korektnost	k1gFnSc1	korektnost
vymezení	vymezení	k1gNnPc2	vymezení
3	[number]	k4	3
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
</s>
</p>
<p>
<s>
Krátká	krátký	k2eAgFnSc1d1	krátká
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
8-10	[number]	k4	8-10
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnPc1	skupina
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
–	–	k?	–
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
historické	historický	k2eAgFnSc2d1	historická
Mendělejevovy	Mendělejevův	k2eAgFnSc2d1	Mendělejevova
prezentace	prezentace	k1gFnSc2	prezentace
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
v	v	k7c4	v
současnosti	současnost	k1gFnPc4	současnost
nepoužívaná	používaný	k2eNgFnSc1d1	nepoužívaná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
plně	plně	k6eAd1	plně
nevystihuje	vystihovat	k5eNaImIp3nS	vystihovat
specifika	specifika	k1gFnSc1	specifika
přechodných	přechodný	k2eAgInPc2d1	přechodný
kovů	kov	k1gInPc2	kov
</s>
</p>
<p>
<s>
Historické	historický	k2eAgFnPc1d1	historická
tabulky	tabulka	k1gFnPc1	tabulka
</s>
</p>
<p>
<s>
Mendělejevova	Mendělejevův	k2eAgFnSc1d1	Mendělejevova
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
zavedl	zavést	k5eAaPmAgMnS	zavést
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
a	a	k8xC	a
Lothar	Lothar	k1gMnSc1	Lothar
Meyer	Meyer	k1gMnSc1	Meyer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
</s>
</p>
<p>
<s>
Wernerova	Wernerův	k2eAgFnSc1d1	Wernerova
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
zavedl	zavést	k5eAaPmAgMnS	zavést
Alfred	Alfred	k1gMnSc1	Alfred
Werner	Werner	k1gMnSc1	Werner
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Mendělejevovy	Mendělejevův	k2eAgFnSc2d1	Mendělejevova
tabulky	tabulka	k1gFnSc2	tabulka
</s>
</p>
<p>
<s>
Rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
tabulky	tabulka	k1gFnPc1	tabulka
</s>
</p>
<p>
<s>
Seaborgův	Seaborgův	k2eAgInSc1d1	Seaborgův
model	model	k1gInSc1	model
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
50	[number]	k4	50
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
výstavbový	výstavbový	k2eAgInSc1d1	výstavbový
princip	princip	k1gInSc1	princip
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Glenna	Glenn	k1gMnSc4	Glenn
Seaborga	Seaborg	k1gMnSc4	Seaborg
</s>
</p>
<p>
<s>
Frickeho	Frickeze	k6eAd1	Frickeze
model	model	k1gInSc1	model
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
52	[number]	k4	52
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
řeší	řešit	k5eAaImIp3nP	řešit
nedostatky	nedostatek	k1gInPc4	nedostatek
výstavbového	výstavbový	k2eAgInSc2d1	výstavbový
principu	princip	k1gInSc2	princip
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
Burkharda	Burkhard	k1gMnSc2	Burkhard
Frickeho	Fricke	k1gMnSc2	Fricke
<g/>
,	,	kIx,	,
Waltera	Walter	k1gMnSc2	Walter
Greinera	Greiner	k1gMnSc2	Greiner
a	a	k8xC	a
Jamese	Jamese	k1gFnPc1	Jamese
Thomase	Thomas	k1gMnSc2	Thomas
Wabera	Waber	k1gMnSc2	Waber
</s>
</p>
<p>
<s>
Pyykkö	Pyykkö	k?	Pyykkö
model	model	k1gInSc1	model
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
50	[number]	k4	50
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
řeší	řešit	k5eAaImIp3nP	řešit
nedostatky	nedostatek	k1gInPc1	nedostatek
Frickeho	Fricke	k1gMnSc2	Fricke
výpočtů	výpočet	k1gInPc2	výpočet
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
Pekky	Pekka	k1gMnSc2	Pekka
Pyykkö	Pyykkö	k1gMnSc2	Pyykkö
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnPc1d1	alternativní
tabulky	tabulka	k1gFnPc1	tabulka
</s>
</p>
<p>
<s>
Levochodná	Levochodný	k2eAgFnSc1d1	Levochodný
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Charlese	Charles	k1gMnSc2	Charles
Janeta	Janet	k1gMnSc2	Janet
</s>
</p>
<p>
<s>
Spirální	spirální	k2eAgFnPc1d1	spirální
tabulky	tabulka	k1gFnPc1	tabulka
</s>
</p>
<p>
<s>
Vícerozměrné	vícerozměrný	k2eAgNnSc4d1	vícerozměrné
tabulkyPro	tabulkyPro	k1gNnSc4	tabulkyPro
snadnější	snadný	k2eAgNnSc4d2	snazší
zapamatování	zapamatování	k1gNnSc4	zapamatování
skupin	skupina	k1gFnPc2	skupina
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
mnemotechnické	mnemotechnický	k2eAgFnPc4d1	mnemotechnická
pomůcky	pomůcka	k1gFnPc4	pomůcka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
tabulkaVysvětlivky	tabulkaVysvětlivka	k1gFnPc1	tabulkaVysvětlivka
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Objev	objev	k1gInSc1	objev
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
umožnil	umožnit	k5eAaPmAgMnS	umožnit
italský	italský	k2eAgMnSc1d1	italský
chemik	chemik	k1gMnSc1	chemik
Stanislao	Stanislao	k1gMnSc1	Stanislao
Cannizzaro	Cannizzara	k1gFnSc5	Cannizzara
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
publikoval	publikovat	k5eAaBmAgInS	publikovat
soubor	soubor	k1gInSc1	soubor
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
atomových	atomový	k2eAgFnPc2d1	atomová
vah	váha	k1gFnPc2	váha
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
známých	známý	k2eAgInPc2d1	známý
jako	jako	k8xC	jako
hmotnostní	hmotnostní	k2eAgNnPc1d1	hmotnostní
čísla	číslo	k1gNnPc1	číslo
<g/>
)	)	kIx)	)
šedesáti	šedesát	k4xCc2	šedesát
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
tehdy	tehdy	k6eAd1	tehdy
známy	znám	k2eAgInPc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Seřazení	seřazení	k1gNnSc1	seřazení
prvků	prvek	k1gInPc2	prvek
podle	podle	k7c2	podle
vzrůstající	vzrůstající	k2eAgFnSc2d1	vzrůstající
atomové	atomový	k2eAgFnSc2d1	atomová
váhy	váha	k1gFnSc2	váha
odhalilo	odhalit	k5eAaPmAgNnS	odhalit
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
opakování	opakování	k1gNnSc1	opakování
chemických	chemický	k2eAgFnPc2d1	chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1864	[number]	k4	1864
anglický	anglický	k2eAgMnSc1d1	anglický
chemik	chemik	k1gMnSc1	chemik
John	John	k1gMnSc1	John
Newlands	Newlandsa	k1gFnPc2	Newlandsa
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gInSc1	jeho
"	"	kIx"	"
<g/>
zákon	zákon	k1gInSc1	zákon
oktáv	oktáv	k1gInSc1	oktáv
<g/>
"	"	kIx"	"
mu	on	k3xPp3gNnSc3	on
nepřinesl	přinést	k5eNaPmAgMnS	přinést
nic	nic	k3yNnSc1	nic
než	než	k8xS	než
výsměch	výsměch	k1gInSc4	výsměch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
učinil	učinit	k5eAaImAgInS	učinit
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
tentýž	týž	k3xTgInSc4	týž
objev	objev	k1gInSc4	objev
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
vykonal	vykonat	k5eAaPmAgMnS	vykonat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
významnější	významný	k2eAgNnSc1d2	významnější
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
plným	plný	k2eAgNnSc7d1	plné
právem	právo	k1gNnSc7	právo
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
pravého	pravý	k2eAgMnSc4d1	pravý
objevitele	objevitel	k1gMnSc4	objevitel
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mendělejevova	Mendělejevův	k2eAgFnSc1d1	Mendělejevova
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
prvků	prvek	k1gInPc2	prvek
nebyla	být	k5eNaImAgFnS	být
prvním	první	k4xOgInSc7	první
pokusem	pokus	k1gInSc7	pokus
sestavit	sestavit	k5eAaPmF	sestavit
prvky	prvek	k1gInPc4	prvek
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
doloženou	doložený	k2eAgFnSc4d1	doložená
tabulku	tabulka	k1gFnSc4	tabulka
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Louis-Bernard	Louis-Bernard	k1gInSc1	Louis-Bernard
Guyton	Guyton	k1gInSc4	Guyton
de	de	k?	de
Morveau	Morveaus	k1gInSc2	Morveaus
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
chemicky	chemicky	k6eAd1	chemicky
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
použil	použít	k5eAaPmAgMnS	použít
ji	on	k3xPp3gFnSc4	on
Antoine	Antoin	k1gInSc5	Antoin
Lavoisier	Lavoisiero	k1gNnPc2	Lavoisiero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Dumas	Dumas	k1gMnSc1	Dumas
základní	základní	k2eAgFnSc4d1	základní
periodickou	periodický	k2eAgFnSc4d1	periodická
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
32	[number]	k4	32
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
osmi	osm	k4xCc6	osm
sloupcích	sloupec	k1gInPc6	sloupec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
poukazovaly	poukazovat	k5eAaImAgInP	poukazovat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
společné	společný	k2eAgFnPc4d1	společná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
Alexandre-Emile	Alexandre-Emila	k1gFnSc3	Alexandre-Emila
Béguyer	Béguyer	k1gInSc1	Béguyer
de	de	k?	de
Chancourtois	Chancourtois	k1gInSc1	Chancourtois
poprvé	poprvé	k6eAd1	poprvé
prvky	prvek	k1gInPc4	prvek
podle	podle	k7c2	podle
vzrůstající	vzrůstající	k2eAgFnSc2d1	vzrůstající
atomové	atomový	k2eAgFnSc2d1	atomová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1	podobný
prvky	prvek	k1gInPc4	prvek
umístil	umístit	k5eAaPmAgInS	umístit
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tak	tak	k9	tak
šroubovicové	šroubovicový	k2eAgNnSc4d1	šroubovicové
uspořádání	uspořádání	k1gNnSc4	uspořádání
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Julius	Julius	k1gMnSc1	Julius
Lothar	Lothar	k1gMnSc1	Lothar
Meyer	Meyer	k1gMnSc1	Meyer
tabulku	tabulka	k1gFnSc4	tabulka
mocenství	mocenství	k1gNnSc2	mocenství
pro	pro	k7c4	pro
49	[number]	k4	49
tehdy	tehdy	k6eAd1	tehdy
známých	známý	k2eAgInPc2d1	známý
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
publikoval	publikovat	k5eAaBmAgMnS	publikovat
také	také	k9	také
William	William	k1gInSc4	William
Odling	Odling	k1gInSc1	Odling
svou	svůj	k3xOyFgFnSc4	svůj
téměř	téměř	k6eAd1	téměř
správnou	správný	k2eAgFnSc4d1	správná
tabulku	tabulka	k1gFnSc4	tabulka
se	s	k7c7	s
17	[number]	k4	17
svislými	svislý	k2eAgInPc7d1	svislý
sloupci	sloupec	k1gInPc7	sloupec
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
57	[number]	k4	57
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
periodický	periodický	k2eAgInSc4d1	periodický
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
63	[number]	k4	63
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
Mendělejev	Mendělejev	k1gFnSc2	Mendělejev
upravil	upravit	k5eAaPmAgMnS	upravit
a	a	k8xC	a
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
svou	svůj	k3xOyFgFnSc4	svůj
periodickou	periodický	k2eAgFnSc4d1	periodická
tabulku	tabulka	k1gFnSc4	tabulka
a	a	k8xC	a
předpověděl	předpovědět	k5eAaPmAgInS	předpovědět
objev	objev	k1gInSc1	objev
10	[number]	k4	10
prvků	prvek	k1gInPc2	prvek
–	–	k?	–
dnes	dnes	k6eAd1	dnes
známé	známý	k1gMnPc4	známý
jako	jako	k8xC	jako
Sc	Sc	k1gMnPc4	Sc
<g/>
,	,	kIx,	,
Ga	Ga	k1gMnPc4	Ga
<g/>
,	,	kIx,	,
Ge	Ge	k1gMnPc4	Ge
<g/>
,	,	kIx,	,
Tc	tc	k0	tc
<g/>
,	,	kIx,	,
Re	re	k9	re
<g/>
,	,	kIx,	,
Po	po	k7c6	po
<g/>
,	,	kIx,	,
Fr	fr	k0	fr
<g/>
,	,	kIx,	,
Ra	ra	k0	ra
<g/>
,	,	kIx,	,
Ac	Ac	k1gFnSc4	Ac
a	a	k8xC	a
Pd	Pd	k1gFnSc4	Pd
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
popsal	popsat	k5eAaPmAgInS	popsat
s	s	k7c7	s
udivující	udivující	k2eAgFnSc7d1	udivující
předvídavostí	předvídavost	k1gFnSc7	předvídavost
skandium	skandium	k1gNnSc1	skandium
<g/>
,	,	kIx,	,
gallium	gallium	k1gNnSc1	gallium
<g/>
,	,	kIx,	,
germanium	germanium	k1gNnSc1	germanium
a	a	k8xC	a
polonium	polonium	k1gNnSc1	polonium
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
neobjevené	objevený	k2eNgInPc4d1	neobjevený
prvky	prvek	k1gInPc4	prvek
nechal	nechat	k5eAaPmAgMnS	nechat
Mendělejev	Mendělejev	k1gFnPc4	Mendělejev
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
všechna	všechen	k3xTgNnPc1	všechen
volná	volný	k2eAgNnPc1d1	volné
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
nechal	nechat	k5eAaPmAgMnS	nechat
<g/>
,	,	kIx,	,
zaplnila	zaplnit	k5eAaPmAgFnS	zaplnit
nově	nově	k6eAd1	nově
objevenými	objevený	k2eAgInPc7d1	objevený
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
Mendělejevovým	Mendělejevův	k2eAgMnSc7d1	Mendělejevův
obhájcem	obhájce	k1gMnSc7	obhájce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gInSc1	jeho
zákon	zákon	k1gInSc1	zákon
nebyl	být	k5eNaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
chemik	chemik	k1gMnSc1	chemik
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Brauner	Brauner	k1gMnSc1	Brauner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
alternativní	alternativní	k2eAgNnSc1d1	alternativní
uspořádání	uspořádání	k1gNnSc1	uspořádání
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
zavedl	zavést	k5eAaPmAgMnS	zavést
Charles	Charles	k1gMnSc1	Charles
Janet	Janet	k1gMnSc1	Janet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tabulka	tabulka	k1gFnSc1	tabulka
je	být	k5eAaImIp3nS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
podle	podle	k7c2	podle
zaplňování	zaplňování	k1gNnSc2	zaplňování
orbitalů	orbital	k1gInPc2	orbital
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
fyziky	fyzik	k1gMnPc7	fyzik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Vědci	vědec	k1gMnPc1	vědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
objevit	objevit	k5eAaPmF	objevit
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
<g/>
Budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
rozvržení	rozvržení	k1gNnSc2	rozvržení
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
při	při	k7c6	při
objevování	objevování	k1gNnSc6	objevování
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
kvantověmechanickými	kvantověmechanický	k2eAgInPc7d1	kvantověmechanický
výpočty	výpočet	k1gInPc7	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Glenn	Glenn	k1gMnSc1	Glenn
T.	T.	kA	T.
Seaborg	Seaborg	k1gMnSc1	Seaborg
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
rozšíření	rozšíření	k1gNnSc4	rozšíření
o	o	k7c4	o
8	[number]	k4	8
<g/>
.	.	kIx.	.
periodu	perioda	k1gFnSc4	perioda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
prvcích	prvek	k1gInPc6	prvek
bloku	blok	k1gInSc2	blok
s	s	k7c7	s
nově	nově	k6eAd1	nově
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
podobě	podoba	k1gFnSc6	podoba
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
superaktinoidy	superaktinoida	k1gFnSc2	superaktinoida
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
prvku	prvek	k1gInSc2	prvek
153	[number]	k4	153
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
pokračující	pokračující	k2eAgFnSc2d1	pokračující
platnosti	platnost	k1gFnSc2	platnost
Madelungova	Madelungův	k2eAgNnSc2d1	Madelungův
pravidla	pravidlo	k1gNnSc2	pravidlo
–	–	k?	–
po	po	k7c6	po
bloku	blok	k1gInSc6	blok
s	s	k7c7	s
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
nejprve	nejprve	k6eAd1	nejprve
vložený	vložený	k2eAgInSc4d1	vložený
blok	blok	k1gInSc4	blok
g	g	kA	g
(	(	kIx(	(
<g/>
orbitaly	orbital	k1gInPc7	orbital
5	[number]	k4	5
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
blok	blok	k1gInSc1	blok
f	f	k?	f
(	(	kIx(	(
<g/>
orbitaly	orbital	k1gInPc7	orbital
6	[number]	k4	6
<g/>
f	f	k?	f
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podrobné	podrobný	k2eAgInPc1d1	podrobný
kvantověmechanické	kvantověmechanický	k2eAgInPc1d1	kvantověmechanický
výpočty	výpočet	k1gInPc1	výpočet
se	s	k7c7	s
započtením	započtení	k1gNnSc7	započtení
relativistických	relativistický	k2eAgInPc2d1	relativistický
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
provedené	provedený	k2eAgFnSc6d1	provedená
Pekkou	Pekká	k1gFnSc4	Pekká
Pyykkö	Pyykkö	k1gFnPc2	Pyykkö
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Frickem	Fricek	k1gMnSc7	Fricek
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
fyziky	fyzik	k1gMnPc7	fyzik
<g/>
,	,	kIx,	,
však	však	k9	však
zpochybnily	zpochybnit	k5eAaPmAgFnP	zpochybnit
takové	takový	k3xDgNnSc4	takový
pořadí	pořadí	k1gNnSc4	pořadí
zaplňování	zaplňování	k1gNnSc3	zaplňování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
9	[number]	k4	9
<g/>
.	.	kIx.	.
periodu	perioda	k1gFnSc4	perioda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
i	i	k9	i
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
8	[number]	k4	8
<g/>
.	.	kIx.	.
periody	perioda	k1gFnSc2	perioda
(	(	kIx(	(
<g/>
po	po	k7c6	po
prvcích	prvek	k1gInPc6	prvek
bloku	blok	k1gInSc2	blok
5	[number]	k4	5
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
prvku	prvek	k1gInSc2	prvek
139	[number]	k4	139
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInPc2	jejich
výsledků	výsledek	k1gInPc2	výsledek
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
změnilo	změnit	k5eAaPmAgNnS	změnit
pořadí	pořadí	k1gNnSc3	pořadí
zaplňování	zaplňování	k1gNnSc2	zaplňování
konce	konec	k1gInSc2	konec
8	[number]	k4	8
<g/>
.	.	kIx.	.
periody	perioda	k1gFnSc2	perioda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
analogická	analogický	k2eAgFnSc1d1	analogická
nižším	nízký	k2eAgFnPc3d2	nižší
periodám	perioda	k1gFnPc3	perioda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Že	že	k9	že
tradiční	tradiční	k2eAgFnSc1d1	tradiční
výstavba	výstavba	k1gFnSc1	výstavba
elektronového	elektronový	k2eAgInSc2d1	elektronový
obalu	obal	k1gInSc2	obal
podle	podle	k7c2	podle
Madelungova	Madelungův	k2eAgNnSc2d1	Madelungův
pravidla	pravidlo	k1gNnSc2	pravidlo
nebude	být	k5eNaImBp3nS	být
pro	pro	k7c4	pro
8	[number]	k4	8
<g/>
.	.	kIx.	.
periodu	perioda	k1gFnSc4	perioda
platit	platit	k5eAaImF	platit
<g/>
,	,	kIx,	,
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
již	již	k6eAd1	již
relativistické	relativistický	k2eAgInPc1d1	relativistický
kvantově-mechanické	kvantověechanický	k2eAgInPc1d1	kvantově-mechanický
výpočty	výpočet	k1gInPc1	výpočet
provedené	provedený	k2eAgInPc1d1	provedený
pro	pro	k7c4	pro
oganesson	oganesson	k1gInSc4	oganesson
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
obalu	obal	k1gInSc2	obal
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
spin-orbitální	spinrbitální	k2eAgFnSc6d1	spin-orbitální
interakci	interakce	k1gFnSc6	interakce
převažovat	převažovat	k5eAaImF	převažovat
nad	nad	k7c7	nad
LS	LS	kA	LS
vazbou	vazba	k1gFnSc7	vazba
vazba	vazba	k1gFnSc1	vazba
jj	jj	k?	jj
a	a	k8xC	a
klasický	klasický	k2eAgInSc4d1	klasický
popis	popis	k1gInSc4	popis
uspořádání	uspořádání	k1gNnSc2	uspořádání
obalu	obal	k1gInSc2	obal
do	do	k7c2	do
slupek	slupka	k1gFnPc2	slupka
a	a	k8xC	a
orbitalů	orbital	k1gInPc2	orbital
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nejeví	jevit	k5eNaImIp3nS	jevit
jako	jako	k9	jako
korektní	korektní	k2eAgFnSc1d1	korektní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
bližší	bližší	k1gNnSc4	bližší
Fermiho	Fermi	k1gMnSc2	Fermi
elektronovému	elektronový	k2eAgNnSc3d1	elektronové
plynu	plynout	k5eAaImIp1nS	plynout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Periodický	periodický	k2eAgInSc1d1	periodický
zákon	zákon	k1gInSc1	zákon
</s>
</p>
<p>
<s>
Výstavbový	výstavbový	k2eAgInSc1d1	výstavbový
princip	princip	k1gInSc1	princip
</s>
</p>
<p>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Interaktivní	interaktivní	k2eAgFnSc1d1	interaktivní
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
izotopů	izotop	k1gInPc2	izotop
(	(	kIx(	(
<g/>
od	od	k7c2	od
IUPAC	IUPAC	kA	IUPAC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Detailní	detailní	k2eAgFnSc1d1	detailní
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
<s>
Periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
s	s	k7c7	s
podrobnějšími	podrobný	k2eAgFnPc7d2	podrobnější
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
<s>
Dynamická	dynamický	k2eAgFnSc1d1	dynamická
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnPc4d1	alternativní
tabulky	tabulka	k1gFnPc4	tabulka
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
s	s	k7c7	s
flash	flasha	k1gFnPc2	flasha
videem	video	k1gNnSc7	video
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
prvku	prvek	k1gInSc3	prvek
(	(	kIx(	(
<g/>
názorná	názorný	k2eAgNnPc1d1	názorné
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
<g/>
,	,	kIx,	,
experimenty	experiment	k1gInPc1	experiment
<g/>
,	,	kIx,	,
historie	historie	k1gFnPc1	historie
objevu	objev	k1gInSc2	objev
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
