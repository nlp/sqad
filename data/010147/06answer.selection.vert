<s>
Řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
periodickým	periodický	k2eAgInSc7d1	periodický
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Mendělejev	Mendělejev	k1gMnSc1	Mendělejev
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
seřadil	seřadit	k5eAaPmAgInS	seřadit
prvky	prvek	k1gInPc7	prvek
podle	podle	k7c2	podle
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
hmotnosti	hmotnost	k1gFnSc2	hmotnost
jejich	jejich	k3xOp3gInPc2	jejich
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
