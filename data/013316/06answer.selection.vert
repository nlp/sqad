<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Jemenu	Jemen	k1gInSc6	Jemen
a	a	k8xC	a
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
.	.	kIx.	.
</s>
