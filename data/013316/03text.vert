<p>
<s>
Rosnička	rosnička	k1gFnSc1	rosnička
levantská	levantský	k2eAgFnSc1d1	Levantská
(	(	kIx(	(
<g/>
Hyla	Hyla	k1gFnSc1	Hyla
savignyi	savigny	k1gFnSc2	savigny
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
žáby	žába	k1gFnSc2	žába
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
rosničkovitých	rosničkovitý	k2eAgFnPc2d1	rosničkovitý
dorůstající	dorůstající	k2eAgFnSc6d1	dorůstající
velikosti	velikost	k1gFnSc3	velikost
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
47	[number]	k4	47
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
,	,	kIx,	,
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Jemenu	Jemen	k1gInSc6	Jemen
a	a	k8xC	a
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hyla	Hylum	k1gNnSc2	Hylum
savignyi	savigny	k1gFnSc2	savigny
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rosnička	rosnička	k1gFnSc1	rosnička
levantská	levantský	k2eAgFnSc1d1	Levantská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Hyla	Hyl	k1gInSc2	Hyl
savignyi	savignyi	k6eAd1	savignyi
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
rosnička	rosnička	k1gFnSc1	rosnička
levantská	levantský	k2eAgFnSc1d1	Levantská
(	(	kIx(	(
<g/>
Hyla	Hyla	k1gFnSc1	Hyla
savignyi	savigny	k1gFnSc2	savigny
<g/>
)	)	kIx)	)
</s>
</p>
