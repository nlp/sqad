<s>
iPhone	iPhon	k1gMnSc5	iPhon
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc1	produkt
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
smartphone	smartphon	k1gMnSc5	smartphon
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
chytrý	chytrý	k2eAgInSc4d1	chytrý
mobilní	mobilní	k2eAgInSc4d1	mobilní
telefon	telefon	k1gInSc4	telefon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojuje	spojovat	k5eAaImIp3nS	spojovat
funkce	funkce	k1gFnSc2	funkce
mobilního	mobilní	k2eAgInSc2d1	mobilní
telefonu	telefon	k1gInSc2	telefon
s	s	k7c7	s
kapesním	kapesní	k2eAgInSc7d1	kapesní
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
dotykového	dotykový	k2eAgInSc2d1	dotykový
displeje	displej	k1gInSc2	displej
s	s	k7c7	s
virtuální	virtuální	k2eAgFnSc7d1	virtuální
klávesnicí	klávesnice	k1gFnSc7	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
iPhone	iPhon	k1gInSc5	iPhon
přinesl	přinést	k5eAaPmAgInS	přinést
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
vícedotykové	vícedotykové	k2eAgNnSc2d1	vícedotykové
ovládání	ovládání	k1gNnSc2	ovládání
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
díky	díky	k7c3	díky
vysokým	vysoký	k2eAgInPc3d1	vysoký
prodejům	prodej	k1gInPc3	prodej
a	a	k8xC	a
oblibě	obliba	k1gFnSc6	obliba
popularizoval	popularizovat	k5eAaImAgMnS	popularizovat
celou	celý	k2eAgFnSc4d1	celá
kategorii	kategorie	k1gFnSc4	kategorie
smartphonů	smartphon	k1gInPc2	smartphon
<g/>
.	.	kIx.	.
</s>
<s>
iPhone	iPhon	k1gInSc5	iPhon
používá	používat	k5eAaImIp3nS	používat
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
iOS	iOS	k?	iOS
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
SDK	SDK	kA	SDK
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
již	již	k9	již
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
aplikací	aplikace	k1gFnPc2	aplikace
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
včetně	včetně	k7c2	včetně
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
v	v	k7c6	v
Apple	Apple	kA	Apple
App	App	k1gMnSc5	App
Store	Stor	k1gMnSc5	Stor
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1	aktuální
modely	model	k1gInPc1	model
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
a	a	k8xC	a
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
Plus	plus	k1gNnSc6	plus
byly	být	k5eAaImAgInP	být
představeny	představit	k5eAaPmNgInP	představit
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stříbrné	stříbrná	k1gFnSc6	stříbrná
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
růžově	růžově	k6eAd1	růžově
zlaté	zlatý	k2eAgFnSc6d1	zlatá
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
i	i	k9	i
v	v	k7c6	v
lesklé	lesklý	k2eAgFnSc6d1	lesklá
a	a	k8xC	a
matné	matný	k2eAgFnSc6d1	matná
černé	černá	k1gFnSc6	černá
<g/>
.	.	kIx.	.
</s>
<s>
Přípravu	příprava	k1gFnSc4	příprava
iPhone	iPhon	k1gInSc5	iPhon
oficiálně	oficiálně	k6eAd1	oficiálně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Apple	Apple	kA	Apple
Inc	Inc	k1gFnSc2	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobsa	k1gFnPc2	Jobsa
na	na	k7c4	na
Macworld	Macworld	k1gInSc4	Macworld
Conference	Conferenec	k1gMnSc2	Conferenec
&	&	k?	&
Expo	Expo	k1gNnSc1	Expo
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
koncovým	koncový	k2eAgMnPc3d1	koncový
uživatelům	uživatel	k1gMnPc3	uživatel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
zahájen	zahájit	k5eAaPmNgInS	zahájit
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nákupu	nákup	k1gInSc6	nákup
se	se	k3xPyFc4	se
nevyžadovaly	vyžadovat	k5eNaImAgInP	vyžadovat
žádné	žádný	k3yNgInPc1	žádný
osobní	osobní	k2eAgInPc1d1	osobní
údaje	údaj	k1gInPc1	údaj
<g/>
,	,	kIx,	,
dodatečně	dodatečně	k6eAd1	dodatečně
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
pomocí	pomocí	k7c2	pomocí
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
iTunes	iTunes	k1gMnSc1	iTunes
nutné	nutný	k2eAgNnSc1d1	nutné
uzavřít	uzavřít	k5eAaPmF	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
výhradním	výhradní	k2eAgInSc7d1	výhradní
operátorem	operátor	k1gInSc7	operátor
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
firma	firma	k1gFnSc1	firma
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
Mobility	mobilita	k1gFnSc2	mobilita
(	(	kIx(	(
<g/>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
lednového	lednový	k2eAgNnSc2d1	lednové
představení	představení	k1gNnSc2	představení
telefonu	telefon	k1gInSc2	telefon
ještě	ještě	k6eAd1	ještě
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
Cingular	Cingulara	k1gFnPc2	Cingulara
–	–	k?	–
toto	tento	k3xDgNnSc4	tento
logo	logo	k1gNnSc4	logo
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
nejstarších	starý	k2eAgInPc6d3	nejstarší
snímcích	snímek	k1gInPc6	snímek
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
byl	být	k5eAaImAgInS	být
pohodlný	pohodlný	k2eAgMnSc1d1	pohodlný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
zakoupení	zakoupení	k1gNnSc4	zakoupení
telefonu	telefon	k1gInSc2	telefon
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
následné	následný	k2eAgNnSc4d1	následné
nelegální	legální	k2eNgNnSc4d1	nelegální
odblokování	odblokování	k1gNnSc4	odblokování
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
musí	muset	k5eAaImIp3nS	muset
smlouva	smlouva	k1gFnSc1	smlouva
uzavírat	uzavírat	k5eAaImF	uzavírat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
iPhone	iPhon	k1gInSc5	iPhon
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
začalo	začít	k5eAaPmAgNnS	začít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
promyšlenou	promyšlený	k2eAgFnSc7d1	promyšlená
reklamní	reklamní	k2eAgFnSc7d1	reklamní
kampaní	kampaň	k1gFnSc7	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
součást	součást	k1gFnSc4	součást
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
považovat	považovat	k5eAaImF	považovat
již	již	k6eAd1	již
představení	představení	k1gNnSc4	představení
telefonu	telefon	k1gInSc2	telefon
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
z	z	k7c2	z
přístroje	přístroj	k1gInSc2	přístroj
udělalo	udělat	k5eAaPmAgNnS	udělat
hit	hit	k1gInSc4	hit
už	už	k9	už
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
jeho	jeho	k3xOp3gInSc2	jeho
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
Apple	Apple	kA	Apple
se	se	k3xPyFc4	se
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
30	[number]	k4	30
hodinách	hodina	k1gFnPc6	hodina
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
prodalo	prodat	k5eAaPmAgNnS	prodat
270	[number]	k4	270
000	[number]	k4	000
iPhonů	iPhon	k1gMnPc2	iPhon
<g/>
,	,	kIx,	,
u	u	k7c2	u
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
146	[number]	k4	146
000	[number]	k4	000
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
veřejnosti	veřejnost	k1gFnSc6	veřejnost
představena	představit	k5eAaPmNgFnS	představit
betaverze	betaverze	k1gFnSc1	betaverze
firmwaru	firmware	k1gInSc2	firmware
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vylepšuje	vylepšovat	k5eAaImIp3nS	vylepšovat
použitelnost	použitelnost	k1gFnSc4	použitelnost
v	v	k7c6	v
podnikové	podnikový	k2eAgNnSc4d1	podnikové
<g />
.	.	kIx.	.
</s>
<s>
sféře	sféra	k1gFnSc6	sféra
podporou	podpora	k1gFnSc7	podpora
Microsoft	Microsoft	kA	Microsoft
Exchange	Exchange	k1gInSc4	Exchange
Serveru	server	k1gInSc2	server
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
oficiálně	oficiálně	k6eAd1	oficiálně
vytvářet	vytvářet	k5eAaImF	vytvářet
nativní	nativní	k2eAgFnPc4d1	nativní
aplikace	aplikace	k1gFnPc4	aplikace
pomocí	pomocí	k7c2	pomocí
SDK	SDK	kA	SDK
<g/>
.9	.9	k4	.9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
konferenci	konference	k1gFnSc6	konference
vývojářů	vývojář	k1gMnPc2	vývojář
(	(	kIx(	(
<g/>
WWDC	WWDC	kA	WWDC
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
představena	představen	k2eAgFnSc1d1	představena
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
iPhonu	iPhon	k1gInSc2	iPhon
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
G.	G.	kA	G.
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
nové	nový	k2eAgFnPc4d1	nová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
patří	patřit	k5eAaImIp3nS	patřit
podpora	podpora	k1gFnSc1	podpora
UMTS	UMTS	kA	UMTS
sítí	síť	k1gFnSc7	síť
<g/>
,	,	kIx,	,
GPS	GPS	kA	GPS
navigace	navigace	k1gFnSc1	navigace
a	a	k8xC	a
drobné	drobný	k2eAgFnPc1d1	drobná
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
hardwaru	hardware	k1gInSc6	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
se	se	k3xPyFc4	se
výdrž	výdrž	k1gFnSc1	výdrž
akumulátoru	akumulátor	k1gInSc2	akumulátor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
předvedena	předveden	k2eAgFnSc1d1	předvedena
finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
SDK	SDK	kA	SDK
a	a	k8xC	a
firmwaru	firmware	k1gInSc2	firmware
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
model	model	k1gInSc4	model
telefonu	telefon	k1gInSc2	telefon
a	a	k8xC	a
také	také	k9	také
mezitím	mezitím	k6eAd1	mezitím
uvedený	uvedený	k2eAgInSc1d1	uvedený
multimediální	multimediální	k2eAgInSc1d1	multimediální
přehrávač	přehrávač	k1gInSc1	přehrávač
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
iPhone	iPhon	k1gInSc5	iPhon
prodává	prodávat	k5eAaImIp3nS	prodávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
70	[number]	k4	70
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gMnSc5	iPhon
v	v	k7c6	v
ČR	ČR	kA	ČR
nabízí	nabízet	k5eAaImIp3nS	nabízet
všichni	všechen	k3xTgMnPc1	všechen
3	[number]	k4	3
GSM	GSM	kA	GSM
operátoři	operátor	k1gMnPc1	operátor
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Vodafone	Vodafon	k1gInSc5	Vodafon
a	a	k8xC	a
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgFnSc2	druhý
vlny	vlna	k1gFnSc2	vlna
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
prodávat	prodávat	k5eAaImF	prodávat
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
prodeje	prodej	k1gInSc2	prodej
telefonu	telefon	k1gInSc2	telefon
se	se	k3xPyFc4	se
před	před	k7c4	před
obchody	obchod	k1gInPc4	obchod
tvořily	tvořit	k5eAaImAgFnP	tvořit
fronty	fronta	k1gFnPc1	fronta
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
oproti	oproti	k7c3	oproti
uvedení	uvedení	k1gNnSc3	uvedení
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Mediální	mediální	k2eAgFnSc4d1	mediální
pozornost	pozornost	k1gFnSc4	pozornost
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
polský	polský	k2eAgInSc1d1	polský
operátor	operátor	k1gInSc1	operátor
Orange	Orang	k1gInSc2	Orang
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
front	fronta	k1gFnPc2	fronta
angažoval	angažovat	k5eAaBmAgInS	angažovat
placené	placený	k2eAgInPc4d1	placený
herce	herc	k1gInPc4	herc
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
Celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
konferenci	konference	k1gFnSc6	konference
vývojářů	vývojář	k1gMnPc2	vývojář
(	(	kIx(	(
<g/>
WWDC	WWDC	kA	WWDC
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
představena	představen	k2eAgFnSc1d1	představena
inovovaná	inovovaný	k2eAgFnSc1d1	inovovaná
verze	verze	k1gFnSc1	verze
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
iPhone	iPhon	k1gInSc5	iPhon
3G	[number]	k4	3G
S	s	k7c7	s
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
však	však	k9	však
byla	být	k5eAaImAgFnS	být
<g />
.	.	kIx.	.
</s>
<s>
oznámena	oznámen	k2eAgFnSc1d1	oznámena
změna	změna	k1gFnSc1	změna
značení	značení	k1gNnSc2	značení
na	na	k7c6	na
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
se	se	k3xPyFc4	se
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
modelu	model	k1gInSc2	model
liší	lišit	k5eAaImIp3nP	lišit
rychlejším	rychlý	k2eAgInSc7d2	rychlejší
procesorem	procesor	k1gInSc7	procesor
<g/>
,	,	kIx,	,
dvojnásobnou	dvojnásobný	k2eAgFnSc7d1	dvojnásobná
velikostí	velikost	k1gFnSc7	velikost
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
kvalitnějším	kvalitní	k2eAgInSc7d2	kvalitnější
grafickým	grafický	k2eAgInSc7d1	grafický
procesorem	procesor	k1gInSc7	procesor
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
OpenGL	OpenGL	k1gFnSc7	OpenGL
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
kvalitnějším	kvalitní	k2eAgInSc7d2	kvalitnější
fotografickým	fotografický	k2eAgInSc7d1	fotografický
čipem	čip	k1gInSc7	čip
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
natáčení	natáčení	k1gNnSc2	natáčení
videa	video	k1gNnSc2	video
a	a	k8xC	a
přítomností	přítomnost	k1gFnSc7	přítomnost
magnetometru	magnetometr	k1gInSc2	magnetometr
(	(	kIx(	(
<g/>
digitálního	digitální	k2eAgInSc2d1	digitální
kompasu	kompas	k1gInSc2	kompas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
změněna	změnit	k5eAaPmNgFnS	změnit
úprava	úprava	k1gFnSc1	úprava
povrchu	povrch	k1gInSc2	povrch
displeje	displej	k1gInSc2	displej
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lépe	dobře	k6eAd2	dobře
odolávala	odolávat	k5eAaImAgFnS	odolávat
mastnotě	mastnota	k1gFnSc3	mastnota
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
čistit	čistit	k5eAaImF	čistit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
víkend	víkend	k1gInSc4	víkend
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
milion	milion	k4xCgInSc1	milion
kusů	kus	k1gInPc2	kus
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgInS	být
překonán	překonán	k2eAgInSc1d1	překonán
rekord	rekord	k1gInSc1	rekord
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
3G	[number]	k4	3G
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
(	(	kIx(	(
<g/>
GSM	GSM	kA	GSM
verze	verze	k1gFnSc1	verze
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
CDMA	CDMA	kA	CDMA
verze	verze	k1gFnSc1	verze
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
4S	[number]	k4	4S
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
5	[number]	k4	5
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
5C	[number]	k4	5C
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
5S	[number]	k4	5S
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
Plus	plus	k1gInSc1	plus
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
<g/>
s	s	k7c7	s
Plus	plus	k1gNnSc7	plus
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
SE	s	k7c7	s
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
Plus	plus	k1gInSc1	plus
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Hardwarové	hardwarový	k2eAgFnPc1d1	hardwarová
specifikace	specifikace	k1gFnPc1	specifikace
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
velice	velice	k6eAd1	velice
nadprůměrné	nadprůměrný	k2eAgNnSc4d1	nadprůměrné
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
procesor	procesor	k1gInSc4	procesor
Samsung	Samsung	kA	Samsung
S5L8900	S5L8900	k1gFnSc7	S5L8900
(	(	kIx(	(
<g/>
412	[number]	k4	412
MHz	Mhz	kA	Mhz
ARM	ARM	kA	ARM
1176	[number]	k4	1176
procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
MBX	MBX	kA	MBX
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
a	a	k8xC	a
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
128	[number]	k4	128
MB	MB	kA	MB
(	(	kIx(	(
<g/>
eDRAM	eDRAM	k?	eDRAM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnSc4	uživatel
bylo	být	k5eAaImAgNnS	být
připravené	připravený	k2eAgNnSc1d1	připravené
integrované	integrovaný	k2eAgNnSc1d1	integrované
flashové	flashový	k2eAgNnSc1d1	flashové
úložiště	úložiště	k1gNnSc1	úložiště
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
4	[number]	k4	4
<g/>
,	,	kIx,	,
8	[number]	k4	8
či	či	k8xC	či
16	[number]	k4	16
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc4d1	samotná
výrobu	výroba	k1gFnSc4	výroba
přístroje	přístroj	k1gInSc2	přístroj
prováděla	provádět	k5eAaImAgFnS	provádět
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
výrobků	výrobek	k1gInPc2	výrobek
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
firma	firma	k1gFnSc1	firma
Foxconn	Foxconn	k1gMnSc1	Foxconn
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
2G	[number]	k4	2G
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
předinstalovaným	předinstalovaný	k2eAgInSc7d1	předinstalovaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
1.0	[number]	k4	1.0
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
iPhone	iPhon	k1gInSc5	iPhon
nahrát	nahrát	k5eAaBmF	nahrát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
3.1	[number]	k4	3.1
<g/>
.3	.3	k4	.3
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
3G	[number]	k4	3G
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
2	[number]	k4	2
<g/>
.	.	kIx.	.
smartphone	smartphon	k1gInSc5	smartphon
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgMnS	představit
jako	jako	k9	jako
nástupce	nástupce	k1gMnSc1	nástupce
modelu	model	k1gInSc2	model
iPhone	iPhon	k1gInSc5	iPhon
2G	[number]	k4	2G
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
na	na	k7c6	na
Celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
konferenci	konference	k1gFnSc6	konference
vývojářů	vývojář	k1gMnPc2	vývojář
(	(	kIx(	(
<g/>
WWDC	WWDC	kA	WWDC
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
v	v	k7c6	v
Moscone	Moscon	k1gInSc5	Moscon
Center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
předchozí	předchozí	k2eAgFnPc1d1	předchozí
generace	generace	k1gFnPc1	generace
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
iPhone	iPhon	k1gInSc5	iPhon
3G	[number]	k4	3G
procesor	procesor	k1gInSc1	procesor
Samsung	Samsung	kA	Samsung
S5L8900	S5L8900	k1gFnSc2	S5L8900
(	(	kIx(	(
<g/>
412	[number]	k4	412
MHz	Mhz	kA	Mhz
ARM	ARM	kA	ARM
1176	[number]	k4	1176
procesor	procesor	k1gInSc1	procesor
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
MBX	MBX	kA	MBX
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
a	a	k8xC	a
operační	operační	k2eAgFnSc4d1	operační
paměť	paměť	k1gFnSc4	paměť
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
128	[number]	k4	128
MB	MB	kA	MB
(	(	kIx(	(
<g/>
eDRAM	eDRAM	k?	eDRAM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnSc4	uživatel
bylo	být	k5eAaImAgNnS	být
připravené	připravený	k2eAgNnSc1d1	připravené
integrované	integrovaný	k2eAgNnSc1d1	integrované
flashové	flashový	k2eAgNnSc1d1	flashové
úložiště	úložiště	k1gNnSc1	úložiště
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
8	[number]	k4	8
<g/>
,	,	kIx,	,
či	či	k8xC	či
16	[number]	k4	16
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
iPhonu	iPhon	k1gInSc2	iPhon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
iPhone	iPhon	k1gInSc5	iPhon
3G	[number]	k4	3G
dostupný	dostupný	k2eAgInSc1d1	dostupný
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
barevných	barevný	k2eAgNnPc6d1	barevné
provedeních	provedení	k1gNnPc6	provedení
(	(	kIx(	(
<g/>
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
a	a	k8xC	a
bílém	bílé	k1gNnSc6	bílé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nebyl	být	k5eNaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
na	na	k7c4	na
kryt	kryt	k1gInSc4	kryt
zadní	zadní	k2eAgFnSc2d1	zadní
strany	strana	k1gFnSc2	strana
přístroje	přístroj	k1gInSc2	přístroj
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plast	plast	k1gInSc1	plast
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc4d1	samotná
výrobu	výroba	k1gFnSc4	výroba
přístroje	přístroj	k1gInSc2	přístroj
prováděla	provádět	k5eAaImAgFnS	provádět
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
výrobků	výrobek	k1gInPc2	výrobek
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
firma	firma	k1gFnSc1	firma
Foxconn	Foxconn	k1gMnSc1	Foxconn
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
3G	[number]	k4	3G
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
předinstalovaným	předinstalovaný	k2eAgInSc7d1	předinstalovaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
2.0	[number]	k4	2.0
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
iPhone	iPhon	k1gInSc5	iPhon
nahrát	nahrát	k5eAaBmF	nahrát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
4.2	[number]	k4	4.2
<g/>
.1	.1	k4	.1
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
vypadá	vypadat	k5eAaPmIp3nS	vypadat
totožně	totožně	k6eAd1	totožně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
G.	G.	kA	G.
Veškeré	veškerý	k3xTgFnPc1	veškerý
inovace	inovace	k1gFnPc1	inovace
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
uvnitř	uvnitř	k7c2	uvnitř
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zařízení	zařízení	k1gNnSc6	zařízení
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
procesor	procesor	k1gInSc1	procesor
Samsung	Samsung	kA	Samsung
S5PC100	S5PC100	k1gFnSc1	S5PC100
ARMv	ARMv	k1gInSc1	ARMv
<g/>
7	[number]	k4	7
Cortex-A	Cortex-A	k1gFnSc2	Cortex-A
<g/>
8	[number]	k4	8
833	[number]	k4	833
MHz	Mhz	kA	Mhz
podtaktovaný	podtaktovaný	k2eAgInSc4d1	podtaktovaný
na	na	k7c4	na
600	[number]	k4	600
MHz	Mhz	kA	Mhz
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
MBX	MBX	kA	MBX
Lite	lit	k1gInSc5	lit
3D	[number]	k4	3D
a	a	k8xC	a
operační	operační	k2eAgFnSc1d1	operační
paměť	paměť	k1gFnSc1	paměť
byla	být	k5eAaImAgFnS	být
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
modelu	model	k1gInSc3	model
zdvojnásobena	zdvojnásoben	k2eAgFnSc1d1	zdvojnásobena
na	na	k7c4	na
256	[number]	k4	256
MB	MB	kA	MB
(	(	kIx(	(
<g/>
eDRAM	eDRAM	k?	eDRAM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnSc4	uživatel
bylo	být	k5eAaImAgNnS	být
připravené	připravený	k2eAgNnSc1d1	připravené
integrované	integrovaný	k2eAgNnSc1d1	integrované
flashové	flashový	k2eAgNnSc1d1	flashové
úložiště	úložiště	k1gNnSc1	úložiště
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
8	[number]	k4	8
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
či	či	k8xC	či
nově	nově	k6eAd1	nově
i	i	k9	i
32	[number]	k4	32
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
i	i	k9	i
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
dostupný	dostupný	k2eAgMnSc1d1	dostupný
jak	jak	k8xC	jak
v	v	k7c6	v
černé	černá	k1gFnSc6	černá
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
iPhone	iPhon	k1gInSc5	iPhon
dokáže	dokázat	k5eAaPmIp3nS	dokázat
nativně	nativně	k6eAd1	nativně
natáčet	natáčet	k5eAaImF	natáčet
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
dohrávat	dohrávat	k5eAaImF	dohrávat
aplikaci	aplikace	k1gFnSc4	aplikace
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rozlišení	rozlišení	k1gNnSc6	rozlišení
VGA	VGA	kA	VGA
s	s	k7c7	s
30	[number]	k4	30
fps	fps	k?	fps
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc4d1	samotná
výrobu	výroba	k1gFnSc4	výroba
přístroje	přístroj	k1gInSc2	přístroj
prováděla	provádět	k5eAaImAgFnS	provádět
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
výrobků	výrobek	k1gInPc2	výrobek
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
firma	firma	k1gFnSc1	firma
Foxconn	Foxconn	k1gMnSc1	Foxconn
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
předinstalovaným	předinstalovaný	k2eAgInSc7d1	předinstalovaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
3.0	[number]	k4	3.0
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
iPhone	iPhon	k1gInSc5	iPhon
nahrát	nahrát	k5eAaBmF	nahrát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
iOS	iOS	k?	iOS
6.1	[number]	k4	6.1
<g/>
.6	.6	k4	.6
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
se	se	k3xPyFc4	se
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
oblá	oblý	k2eAgNnPc4d1	oblé
záda	záda	k1gNnPc4	záda
<g/>
,	,	kIx,	,
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
původní	původní	k2eAgFnSc4d1	původní
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
modely	model	k1gInPc7	model
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
ostře	ostro	k6eAd1	ostro
řezaným	řezaný	k2eAgNnSc7d1	řezané
šasi	šasi	k1gNnSc7	šasi
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
skleněné	skleněný	k2eAgFnSc2d1	skleněná
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
<g/>
Steve	Steve	k1gMnSc1	Steve
Jobs	Jobs	k1gInSc4	Jobs
představil	představit	k5eAaPmAgMnS	představit
i	i	k9	i
bílou	bílý	k2eAgFnSc4d1	bílá
verzi	verze	k1gFnSc4	verze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
celého	celý	k2eAgInSc2d1	celý
přístroje	přístroj	k1gInSc2	přístroj
spojuje	spojovat	k5eAaImIp3nS	spojovat
ocelový	ocelový	k2eAgInSc4d1	ocelový
pruh	pruh	k1gInSc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
procesor	procesor	k1gInSc1	procesor
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc4d1	použit
Apple	Apple	kA	Apple
A4	A4	k1gFnSc2	A4
1	[number]	k4	1
GHz	GHz	k1gMnSc1	GHz
ARMv	ARMv	k1gMnSc1	ARMv
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
podtaktovaný	podtaktovaný	k2eAgMnSc1d1	podtaktovaný
na	na	k7c4	na
789	[number]	k4	789
MHz	Mhz	kA	Mhz
(	(	kIx(	(
<g/>
ARM	ARM	kA	ARM
Cortex-A	Cortex-A	k1gFnSc1	Cortex-A
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
SGX	SGX	kA	SGX
535	[number]	k4	535
<g/>
,	,	kIx,	,
technologie	technologie	k1gFnSc1	technologie
45	[number]	k4	45
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
RAM	RAM	kA	RAM
byla	být	k5eAaImAgFnS	být
oproti	oproti	k7c3	oproti
modelu	model	k1gInSc3	model
3GS	[number]	k4	3GS
zdvojnásobena	zdvojnásobit	k5eAaPmNgFnS	zdvojnásobit
na	na	k7c4	na
512	[number]	k4	512
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
paměť	paměť	k1gFnSc1	paměť
byla	být	k5eAaImAgFnS	být
pevná	pevný	k2eAgFnSc1d1	pevná
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
slotu	slot	k1gInSc2	slot
pro	pro	k7c4	pro
paměťové	paměťový	k2eAgFnPc4d1	paměťová
karty	karta	k1gFnPc4	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
velikosti	velikost	k1gFnSc6	velikost
8	[number]	k4	8
<g/>
,	,	kIx,	,
16	[number]	k4	16
a	a	k8xC	a
32	[number]	k4	32
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
8	[number]	k4	8
GB	GB	kA	GB
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
až	až	k9	až
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
prodeje	prodej	k1gInSc2	prodej
modelu	model	k1gInSc2	model
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyšší	vysoký	k2eAgFnPc1d2	vyšší
kapacity	kapacita	k1gFnPc1	kapacita
již	již	k9	již
nebyly	být	k5eNaImAgFnP	být
nabízeny	nabízet	k5eAaImNgFnP	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
je	být	k5eAaImIp3nS	být
iPhone	iPhon	k1gInSc5	iPhon
4S	[number]	k4	4S
prakticky	prakticky	k6eAd1	prakticky
totožný	totožný	k2eAgInSc1d1	totožný
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
plně	plně	k6eAd1	plně
konstrukčně	konstrukčně	k6eAd1	konstrukčně
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
změnou	změna	k1gFnSc7	změna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jiné	jiný	k2eAgNnSc1d1	jiné
rozložení	rozložení	k1gNnSc1	rozložení
antény	anténa	k1gFnSc2	anténa
(	(	kIx(	(
<g/>
černé	černý	k2eAgInPc1d1	černý
pruhy	pruh	k1gInPc1	pruh
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
telefonu	telefon	k1gInSc2	telefon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
procesor	procesor	k1gInSc1	procesor
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc4d1	použit
Apple	Apple	kA	Apple
A5	A5	k1gFnSc2	A5
1	[number]	k4	1
GHz	GHz	k1gMnSc1	GHz
ARMv	ARMv	k1gMnSc1	ARMv
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
ARM	ARM	kA	ARM
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
Cortex-A	Cortex-A	k1gFnSc2	Cortex-A
<g/>
9	[number]	k4	9
Dual	Dual	k1gMnSc1	Dual
Core	Cor	k1gInSc2	Cor
–	–	k?	–
2	[number]	k4	2
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
SGX	SGX	kA	SGX
543	[number]	k4	543
<g/>
MP	MP	kA	MP
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
technologie	technologie	k1gFnSc1	technologie
45	[number]	k4	45
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
RAM	RAM	kA	RAM
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
modelu	model	k1gInSc2	model
4	[number]	k4	4
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
512	[number]	k4	512
MB	MB	kA	MB
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
paměť	paměť	k1gFnSc1	paměť
byla	být	k5eAaImAgFnS	být
pevná	pevný	k2eAgFnSc1d1	pevná
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
slotu	slot	k1gInSc2	slot
pro	pro	k7c4	pro
paměťové	paměťový	k2eAgFnPc4d1	paměťová
karty	karta	k1gFnPc4	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
velikosti	velikost	k1gFnSc6	velikost
8	[number]	k4	8
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
,	,	kIx,	,
a	a	k8xC	a
64	[number]	k4	64
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
8	[number]	k4	8
GB	GB	kA	GB
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
až	až	k9	až
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
prodeje	prodej	k1gInSc2	prodej
modelu	model	k1gInSc2	model
5	[number]	k4	5
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyšší	vysoký	k2eAgFnPc1d2	vyšší
kapacity	kapacita	k1gFnPc1	kapacita
již	již	k9	již
nebyly	být	k5eNaImAgFnP	být
nabízeny	nabízet	k5eAaImNgFnP	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Telefon	telefon	k1gInSc1	telefon
byl	být	k5eAaImAgInS	být
stažen	stáhnout	k5eAaPmNgInS	stáhnout
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
nového	nový	k2eAgInSc2d1	nový
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
a	a	k8xC	a
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
Plus	plus	k1gNnSc1	plus
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gMnSc5	iPhon
4S	[number]	k4	4S
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
předinstalovaným	předinstalovaný	k2eAgInSc7d1	předinstalovaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iOS	iOS	k?	iOS
5.0	[number]	k4	5.0
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
podporuje	podporovat	k5eAaImIp3nS	podporovat
aktuální	aktuální	k2eAgFnSc4d1	aktuální
verzi	verze	k1gFnSc4	verze
iOS	iOS	k?	iOS
9.3	[number]	k4	9.3
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
</s>
<s>
Vydat	vydat	k5eAaPmF	vydat
tento	tento	k3xDgInSc4	tento
nový	nový	k2eAgInSc4d1	nový
iOS	iOS	k?	iOS
9	[number]	k4	9
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Apple	Apple	kA	Apple
také	také	k6eAd1	také
zařízením	zařízení	k1gNnSc7	zařízení
s	s	k7c7	s
čipem	čip	k1gInSc7	čip
A5	A5	k1gMnSc1	A5
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
mimochodem	mimochodem	k9	mimochodem
v	v	k7c6	v
sobě	se	k3xPyFc3	se
také	také	k9	také
iPad	iPad	k6eAd1	iPad
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
zařízení	zařízení	k1gNnPc4	zařízení
stabilnější	stabilní	k2eAgNnPc4d2	stabilnější
<g/>
,	,	kIx,	,
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
a	a	k8xC	a
kompatibilnější	kompatibilní	k2eAgInSc4d2	kompatibilnější
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
10	[number]	k4	10
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
iPhonu	iPhon	k1gInSc6	iPhon
podporována	podporovat	k5eAaImNgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
4S	[number]	k4	4S
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
iPhonem	iPhon	k1gMnSc7	iPhon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
hlasovou	hlasový	k2eAgFnSc7d1	hlasová
asistentkou	asistentka	k1gFnSc7	asistentka
Siri	Sir	k1gFnSc2	Sir
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
osobní	osobní	k2eAgMnSc1d1	osobní
asistent	asistent	k1gMnSc1	asistent
a	a	k8xC	a
navigátor	navigátor	k1gMnSc1	navigátor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Apple	Apple	kA	Apple
iOS	iOS	k?	iOS
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
používá	používat	k5eAaImIp3nS	používat
přirozený	přirozený	k2eAgInSc4d1	přirozený
mluvený	mluvený	k2eAgInSc4d1	mluvený
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Siri	Sir	k1gFnSc2	Sir
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
zkratky	zkratka	k1gFnSc2	zkratka
pro	pro	k7c4	pro
rozpoznávání	rozpoznávání	k1gNnSc4	rozpoznávání
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
bohužel	bohužel	k9	bohužel
podporována	podporován	k2eAgFnSc1d1	podporována
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
aktualizací	aktualizace	k1gFnPc2	aktualizace
iOS	iOS	k?	iOS
<g/>
;	;	kIx,	;
na	na	k7c4	na
4S	[number]	k4	4S
však	však	k9	však
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
iOS	iOS	k?	iOS
10	[number]	k4	10
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
podporován	podporovat	k5eAaImNgMnS	podporovat
iPhone	iPhon	k1gInSc5	iPhon
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
iPhone	iPhon	k1gInSc5	iPhon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc1d2	veliký
displej	displej	k1gInSc1	displej
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
4	[number]	k4	4
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
než	než	k8xS	než
3,5	[number]	k4	3,5
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měly	mít	k5eAaImAgFnP	mít
všechny	všechen	k3xTgFnPc4	všechen
předchozí	předchozí	k2eAgFnPc4d1	předchozí
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
narostly	narůst	k5eAaPmAgFnP	narůst
rozměry	rozměra	k1gFnPc1	rozměra
přístroje	přístroj	k1gInSc2	přístroj
(	(	kIx(	(
<g/>
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
šířka	šířka	k1gFnSc1	šířka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
rozměry	rozměra	k1gFnSc2	rozměra
v	v	k7c6	v
mm	mm	kA	mm
<g/>
:	:	kIx,	:
model	model	k1gInSc1	model
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
:	:	kIx,	:
115,2	[number]	k4	115,2
×	×	k?	×
58,66	[number]	k4	58,66
×	×	k?	×
9,3	[number]	k4	9,3
<g/>
;	;	kIx,	;
model	model	k1gInSc1	model
5	[number]	k4	5
<g/>
:	:	kIx,	:
123,8	[number]	k4	123,8
×	×	k?	×
58,6	[number]	k4	58,6
×	×	k?	×
7,6	[number]	k4	7,6
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
procesor	procesor	k1gInSc1	procesor
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc4d1	použit
Apple	Apple	kA	Apple
A6	A6	k1gFnSc2	A6
ARMv	ARMv	k1gInSc4	ARMv
<g/>
7	[number]	k4	7
taktovaný	taktovaný	k2eAgInSc4d1	taktovaný
na	na	k7c4	na
1,02	[number]	k4	1,02
GHz	GHz	k1gFnPc2	GHz
(	(	kIx(	(
<g/>
ARM	ARM	kA	ARM
Cortex-A	Cortex-A	k1gFnSc1	Cortex-A
<g/>
15	[number]	k4	15
Dual	Dual	k1gMnSc1	Dual
Core	Cor	k1gInSc2	Cor
–	–	k?	–
2	[number]	k4	2
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
SGX	SGX	kA	SGX
<g/>
543	[number]	k4	543
<g/>
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
technologie	technologie	k1gFnSc1	technologie
32	[number]	k4	32
<g/>
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
RAM	RAM	kA	RAM
byla	být	k5eAaImAgFnS	být
oproti	oproti	k7c3	oproti
modelu	model	k1gInSc3	model
4S	[number]	k4	4S
zdvojnásobena	zdvojnásobit	k5eAaPmNgFnS	zdvojnásobit
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
1	[number]	k4	1
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
paměť	paměť	k1gFnSc1	paměť
byla	být	k5eAaImAgFnS	být
pevná	pevný	k2eAgFnSc1d1	pevná
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
slotu	slot	k1gInSc2	slot
pro	pro	k7c4	pro
paměťové	paměťový	k2eAgFnPc4d1	paměťová
karty	karta	k1gFnPc4	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
velikosti	velikost	k1gFnSc6	velikost
16	[number]	k4	16
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
,	,	kIx,	,
a	a	k8xC	a
64	[number]	k4	64
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
5	[number]	k4	5
byl	být	k5eAaImAgInS	být
tradičně	tradičně	k6eAd1	tradičně
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
barevných	barevný	k2eAgNnPc6d1	barevné
provedeních	provedení	k1gNnPc6	provedení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
a	a	k8xC	a
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
5	[number]	k4	5
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
předinstalovaným	předinstalovaný	k2eAgInSc7d1	předinstalovaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iOS	iOS	k?	iOS
6.0	[number]	k4	6.0
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
podporuje	podporovat	k5eAaImIp3nS	podporovat
verzi	verze	k1gFnSc4	verze
iOS	iOS	k?	iOS
10.3	[number]	k4	10.3
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
se	se	k3xPyFc4	se
model	model	k1gInSc1	model
5C	[number]	k4	5C
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
polykarbonát	polykarbonát	k1gInSc1	polykarbonát
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
obvyklého	obvyklý	k2eAgInSc2d1	obvyklý
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nabízen	nabízet	k5eAaImNgInS	nabízet
v	v	k7c6	v
pěti	pět	k4xCc6	pět
barevných	barevný	k2eAgInPc6d1	barevný
provedeních	provedení	k1gNnPc6	provedení
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
obvyklých	obvyklý	k2eAgFnPc2d1	obvyklá
dvou	dva	k4xCgFnPc2	dva
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
černé	černý	k2eAgFnPc1d1	černá
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hardwarová	hardwarový	k2eAgFnSc1d1	hardwarová
konfigurace	konfigurace	k1gFnSc1	konfigurace
iPhone	iPhon	k1gInSc5	iPhon
5C	[number]	k4	5C
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
výbavy	výbava	k1gFnSc2	výbava
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
staršího	starý	k2eAgInSc2d2	starší
modelu	model	k1gInSc2	model
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
procesor	procesor	k1gInSc1	procesor
byl	být	k5eAaImAgInS	být
použit	použit	k2eAgInSc4d1	použit
Apple	Apple	kA	Apple
A6	A6	k1gFnSc2	A6
ARMv	ARMv	k1gInSc4	ARMv
<g/>
7	[number]	k4	7
taktovaný	taktovaný	k2eAgInSc4d1	taktovaný
na	na	k7c4	na
1,02	[number]	k4	1,02
GHz	GHz	k1gFnPc2	GHz
(	(	kIx(	(
<g/>
ARM	ARM	kA	ARM
Cortex-A	Cortex-A	k1gFnSc1	Cortex-A
<g/>
15	[number]	k4	15
Dual	Dual	k1gMnSc1	Dual
Core	Cor	k1gInSc2	Cor
–	–	k?	–
2	[number]	k4	2
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
koprocesor	koprocesor	k1gInSc1	koprocesor
PowerVR	PowerVR	k1gFnSc2	PowerVR
SGX	SGX	kA	SGX
<g/>
543	[number]	k4	543
<g/>
MP	MP	kA	MP
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
technologie	technologie	k1gFnSc1	technologie
32	[number]	k4	32
<g/>
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
RAM	RAM	kA	RAM
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
modelů	model	k1gInPc2	model
5	[number]	k4	5
a	a	k8xC	a
5	[number]	k4	5
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
1	[number]	k4	1
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
paměť	paměť	k1gFnSc1	paměť
byla	být	k5eAaImAgFnS	být
pevná	pevný	k2eAgFnSc1d1	pevná
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
slotu	slot	k1gInSc2	slot
pro	pro	k7c4	pro
paměťové	paměťový	k2eAgFnPc4d1	paměťová
karty	karta	k1gFnPc4	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
velikosti	velikost	k1gFnSc6	velikost
8	[number]	k4	8
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
,	,	kIx,	,
a	a	k8xC	a
64	[number]	k4	64
GB	GB	kA	GB
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
5C	[number]	k4	5C
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
předinstalovaným	předinstalovaný	k2eAgInSc7d1	předinstalovaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iOS	iOS	k?	iOS
7.0	[number]	k4	7.0
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
také	také	k9	také
iOS	iOS	k?	iOS
10.3	[number]	k4	10.3
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgFnPc3d1	předchozí
generacím	generace	k1gFnPc3	generace
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
model	model	k1gInSc1	model
5S	[number]	k4	5S
prvním	první	k4xOgInSc7	první
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kromě	kromě	k7c2	kromě
klasické	klasický	k2eAgFnSc2d1	klasická
černé	černá	k1gFnSc2	černá
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
šedé	šedý	k2eAgInPc1d1	šedý
<g/>
)	)	kIx)	)
a	a	k8xC	a
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
nabízel	nabízet	k5eAaImAgInS	nabízet
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
zlatém	zlatý	k2eAgNnSc6d1	Zlaté
provedení	provedení	k1gNnSc6	provedení
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
vychází	vycházet	k5eAaImIp3nS	vycházet
model	model	k1gInSc4	model
5S	[number]	k4	5S
z	z	k7c2	z
modelu	model	k1gInSc2	model
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
prakticky	prakticky	k6eAd1	prakticky
jediné	jediný	k2eAgFnPc4d1	jediná
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
odlišné	odlišný	k2eAgInPc1d1	odlišný
odstíny	odstín	k1gInPc1	odstín
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šedá	šedá	k1gFnSc1	šedá
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
space	space	k1gMnSc2	space
grey	grea	k1gMnSc2	grea
<g/>
,	,	kIx,	,
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
či	či	k8xC	či
dvojitý	dvojitý	k2eAgInSc4d1	dvojitý
blesk	blesk	k1gInSc4	blesk
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
klasického	klasický	k2eAgInSc2d1	klasický
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Touch	Touch	k1gInSc1	Touch
ID	Ida	k1gFnPc2	Ida
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
modelu	model	k1gInSc2	model
5S	[number]	k4	5S
použito	použít	k5eAaPmNgNnS	použít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
procesor	procesor	k1gInSc1	procesor
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
Apple	Apple	kA	Apple
A	A	kA	A
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pohybovým	pohybový	k2eAgInSc7d1	pohybový
koprocesorem	koprocesor	k1gInSc7	koprocesor
Apple	Apple	kA	Apple
M	M	kA	M
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
RAM	RAM	kA	RAM
měla	mít	k5eAaImAgFnS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
modelů	model	k1gInPc2	model
5	[number]	k4	5
a	a	k8xC	a
5	[number]	k4	5
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
1	[number]	k4	1
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
paměť	paměť	k1gFnSc1	paměť
byla	být	k5eAaImAgFnS	být
pevná	pevný	k2eAgFnSc1d1	pevná
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velikostech	velikost	k1gFnPc6	velikost
16	[number]	k4	16
<g/>
,	,	kIx,	,
32	[number]	k4	32
a	a	k8xC	a
64	[number]	k4	64
GB	GB	kA	GB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slot	slot	k1gInSc1	slot
pro	pro	k7c4	pro
paměťové	paměťový	k2eAgFnPc4d1	paměťová
karty	karta	k1gFnPc4	karta
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
5S	[number]	k4	5S
byl	být	k5eAaImAgInS	být
dodáván	dodávat	k5eAaImNgInS	dodávat
s	s	k7c7	s
předinstalovaným	předinstalovaný	k2eAgInSc7d1	předinstalovaný
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iOS	iOS	k?	iOS
7.0	[number]	k4	7.0
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
také	také	k9	také
iOS	iOS	k?	iOS
10.3	[number]	k4	10.3
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
SE	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
smartphone	smartphon	k1gInSc5	smartphon
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
firmy	firma	k1gFnPc1	firma
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
představen	představen	k2eAgMnSc1d1	představen
jako	jako	k8xS	jako
nástupce	nástupce	k1gMnSc1	nástupce
modelu	model	k1gInSc2	model
5	[number]	k4	5
<g/>
S.	S.	kA	S.
Vzhledem	vzhled	k1gInSc7	vzhled
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
iPhone	iPhon	k1gInSc5	iPhon
5	[number]	k4	5
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
příslušenství	příslušenství	k1gNnSc2	příslušenství
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
SE	se	k3xPyFc4	se
disponuje	disponovat	k5eAaBmIp3nS	disponovat
úhlopříčkou	úhlopříčka	k1gFnSc7	úhlopříčka
displeje	displej	k1gInSc2	displej
4	[number]	k4	4
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
starší	starý	k2eAgNnSc4d2	starší
4	[number]	k4	4
<g/>
"	"	kIx"	"
iPhony	iPhona	k1gFnSc2	iPhona
<g/>
.	.	kIx.	.
</s>
<s>
SE	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
zkratkou	zkratka	k1gFnSc7	zkratka
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
Special	Special	k1gInSc4	Special
Edition	Edition	k1gInSc4	Edition
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gMnSc5	iPhon
SE	s	k7c7	s
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
barevných	barevný	k2eAgFnPc6d1	barevná
variantách	varianta	k1gFnPc6	varianta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
i	i	k9	i
souběžně	souběžně	k6eAd1	souběžně
prodávaný	prodávaný	k2eAgInSc1d1	prodávaný
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c4	v
vesmírně	vesmírně	k6eAd1	vesmírně
šedé	šedý	k2eAgFnPc4d1	šedá
<g/>
,	,	kIx,	,
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
růžově	růžově	k6eAd1	růžově
zlaté	zlatý	k2eAgFnPc1d1	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Telefon	telefon	k1gInSc1	telefon
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
pevně	pevně	k6eAd1	pevně
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
,	,	kIx,	,
váha	váha	k1gFnSc1	váha
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
kapacita	kapacita	k1gFnSc1	kapacita
paměti	paměť	k1gFnSc2	paměť
16,64	[number]	k4	16,64
<g/>
,128	,128	k4	,128
<g/>
Gb	Gb	k1gMnSc2	Gb
Display	Displaa	k1gMnSc2	Displaa
iphone	iphon	k1gInSc5	iphon
6	[number]	k4	6
=	=	kIx~	=
Retina	retina	k1gFnSc1	retina
HD	HD	kA	HD
<g/>
,4	,4	k4	,4
<g/>
.7	.7	k4	.7
<g/>
palců	palec	k1gInPc2	palec
<g/>
,	,	kIx,	,
<g/>
diagonální	diagonální	k2eAgInSc1d1	diagonální
LED	LED	kA	LED
širokoůhlý	širokoůhlý	k2eAgInSc1d1	širokoůhlý
multidotykový	multidotykový	k2eAgInSc1d1	multidotykový
display	displa	k2eAgFnPc4d1	displa
s	s	k7c7	s
IPS	IPS	kA	IPS
technologií	technologie	k1gFnSc7	technologie
<g/>
,	,	kIx,	,
<g/>
rozlišení	rozlišení	k1gNnSc1	rozlišení
1334	[number]	k4	1334
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
<g/>
750	[number]	k4	750
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
jemností	jemnost	k1gFnPc2	jemnost
326	[number]	k4	326
ppi	ppi	kA	ppi
<g/>
,	,	kIx,	,
<g/>
kontrast	kontrast	k1gInSc1	kontrast
1400	[number]	k4	1400
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
(	(	kIx(	(
<g/>
typický	typický	k2eAgInSc1d1	typický
<g/>
)	)	kIx)	)
Display	Displaa	k1gFnPc1	Displaa
iphone	iphon	k1gInSc5	iphon
6	[number]	k4	6
plus	plus	k1gInSc1	plus
=	=	kIx~	=
Retina	retina	k1gFnSc1	retina
HD	HD	kA	HD
<g/>
,5	,5	k4	,5
<g/>
,5	,5	k4	,5
<g/>
palců	palec	k1gInPc2	palec
<g/>
,	,	kIx,	,
<g/>
diagonální	diagonální	k2eAgInSc1d1	diagonální
LED	LED	kA	LED
širokoúhlý	širokoúhlý	k2eAgInSc1d1	širokoúhlý
multidotykový	multidotykový	k2eAgInSc1d1	multidotykový
display	displa	k2eAgFnPc4d1	displa
s	s	k7c7	s
IPS	IPS	kA	IPS
technologií	technologie	k1gFnSc7	technologie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
rozlišení	rozlišení	k1gNnSc1	rozlišení
1920	[number]	k4	1920
<g/>
x	x	k?	x
<g/>
1080	[number]	k4	1080
(	(	kIx(	(
<g/>
full	fulnout	k5eAaPmAgMnS	fulnout
HD	HD	kA	HD
<g/>
)	)	kIx)	)
pixelů	pixel	k1gInPc2	pixel
a	a	k8xC	a
jemností	jemnost	k1gFnPc2	jemnost
401	[number]	k4	401
ppi	ppi	kA	ppi
<g/>
,	,	kIx,	,
kontrast	kontrast	k1gInSc1	kontrast
1300	[number]	k4	1300
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
typický	typický	k2eAgInSc1d1	typický
<g/>
)	)	kIx)	)
Procesor	procesor	k1gInSc1	procesor
=	=	kIx~	=
A8	A8	k1gFnSc1	A8
s	s	k7c7	s
64	[number]	k4	64
<g/>
-bit	it	k1gInSc4	-bit
architekturou	architektura	k1gFnSc7	architektura
a	a	k8xC	a
M8	M8	k1gFnSc7	M8
pohybový	pohybový	k2eAgInSc4d1	pohybový
koprocesorem	koprocesor	k1gInSc7	koprocesor
iSight	iSight	k2eAgInSc1d1	iSight
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
=	=	kIx~	=
8	[number]	k4	8
<g/>
-megapixelů	egapixel	k1gMnPc2	-megapixel
iSight	iSight	k2eAgMnSc1d1	iSight
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
1.5	[number]	k4	1.5
<g/>
μ	μ	k?	μ
pixel	pixel	k1gInSc1	pixel
<g/>
,	,	kIx,	,
autofocus	autofocus	k1gInSc1	autofocus
s	s	k7c7	s
focus	focus	k1gMnSc1	focus
pixel	pixel	k1gInSc1	pixel
<g/>
,	,	kIx,	,
ƒ	ƒ	k?	ƒ
<g/>
/	/	kIx~	/
<g/>
2.2	[number]	k4	2.2
clona	clona	k1gFnSc1	clona
<g/>
,	,	kIx,	,
<g/>
optická	optický	k2eAgFnSc1d1	optická
stabilizace	stabilizace	k1gFnSc1	stabilizace
<g/>
(	(	kIx(	(
<g/>
iphone	iphon	k1gInSc5	iphon
6	[number]	k4	6
plus	plus	k1gInSc1	plus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
true	truat	k5eAaPmIp3nS	truat
ton	ton	k?	ton
blesk	blesk	k1gInSc1	blesk
<g/>
,	,	kIx,	,
<g/>
hybridní	hybridní	k2eAgMnSc1d1	hybridní
IR	Ir	k1gMnSc1	Ir
filtr	filtr	k1gInSc1	filtr
<g/>
,	,	kIx,	,
<g/>
auto	auto	k1gNnSc1	auto
HDR	HDR	kA	HDR
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
panorama	panorama	k1gNnSc1	panorama
az	az	k?	az
43	[number]	k4	43
<g/>
megapixel	megapixet	k5eAaPmAgMnS	megapixet
video	video	k1gNnSc4	video
=	=	kIx~	=
<g/>
1080	[number]	k4	1080
<g/>
p	p	k?	p
HD	HD	kA	HD
nahravání	nahravánět	k5eAaImIp3nS	nahravánět
při	při	k7c6	při
30	[number]	k4	30
<g/>
fps	fps	k?	fps
<g/>
/	/	kIx~	/
<g/>
60	[number]	k4	60
<g/>
fps	fps	k?	fps
<g/>
,	,	kIx,	,
<g/>
slo-mo	sloo	k6eAd1	slo-mo
zábery	zábera	k1gFnPc1	zábera
120	[number]	k4	120
<g/>
fps	fps	k?	fps
<g/>
/	/	kIx~	/
<g/>
240	[number]	k4	240
<g/>
fps	fps	k?	fps
<g/>
,3	,3	k4	,3
<g/>
x	x	k?	x
zoom	zoom	k1gMnSc1	zoom
Touch	Touch	k1gMnSc1	Touch
ID	Ida	k1gFnPc2	Ida
=	=	kIx~	=
senzor	senzor	k1gInSc1	senzor
otisku	otisk	k1gInSc2	otisk
prstu	prst	k1gInSc2	prst
<g/>
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
scaner	scaner	k1gInSc1	scaner
na	na	k7c4	na
papilárie	papilárie	k1gFnPc4	papilárie
<g/>
)	)	kIx)	)
umístěný	umístěný	k2eAgInSc4d1	umístěný
v	v	k7c6	v
tlačítku	tlačítko	k1gNnSc6	tlačítko
HOME	HOME	kA	HOME
BUTTON	BUTTON	kA	BUTTON
<g/>
,	,	kIx,	,
<g/>
podpora	podpora	k1gFnSc1	podpora
Apple	Apple	kA	Apple
pay	pay	k?	pay
<g/>
,	,	kIx,	,
<g/>
nákup	nákup	k1gInSc4	nákup
v	v	k7c6	v
App	App	k1gFnSc6	App
Store	Stor	k1gInSc5	Stor
a	a	k8xC	a
iTunes	iTunes	k1gInSc1	iTunes
Konektivita	konektivita	k1gFnSc1	konektivita
=	=	kIx~	=
UMTS	UMTS	kA	UMTS
<g/>
/	/	kIx~	/
<g/>
HSPA	HSPA	kA	HSPA
<g/>
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
DC-HSDPA	DC-HSDPA	k1gMnSc1	DC-HSDPA
(	(	kIx(	(
<g/>
850	[number]	k4	850
<g/>
,	,	kIx,	,
900	[number]	k4	900
<g/>
,	,	kIx,	,
1700	[number]	k4	1700
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2100	[number]	k4	2100
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
2100	[number]	k4	2100
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
GSM	GSM	kA	GSM
<g/>
/	/	kIx~	/
<g/>
EDGE	EDGE	kA	EDGE
(	(	kIx(	(
<g/>
850	[number]	k4	850
<g/>
,	,	kIx,	,
900	[number]	k4	900
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
LTE	LTE	kA	LTE
(	(	kIx(	(
<g/>
Bands	Bands	k1gInSc1	Bands
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
)	)	kIx)	)
Wifi	Wifi	k1gNnSc6	Wifi
=	=	kIx~	=
802.11	[number]	k4	802.11
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
b	b	k?	b
<g/>
/	/	kIx~	/
<g/>
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
ac	ac	k?	ac
,	,	kIx,	,
<g/>
Blouetooth	Blouetooth	k1gInSc1	Blouetooth
4.2	[number]	k4	4.2
,	,	kIx,	,
NFC	NFC	kA	NFC
Audio	audio	k2eAgFnSc1d1	audio
=	=	kIx~	=
podpora	podpora	k1gFnSc1	podpora
audio	audio	k2eAgFnSc2d1	audio
formátů-AAC	formátů-AAC	k?	formátů-AAC
<g/>
,	,	kIx,	,
<g/>
HE-AAC	HE-AAC	k1gMnSc1	HE-AAC
<g/>
,	,	kIx,	,
<g/>
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
MP	MP	kA	MP
<g/>
3	[number]	k4	3
VBR	VBR	kA	VBR
<g/>
,	,	kIx,	,
<g/>
AAX	AAX	kA	AAX
<g/>
,	,	kIx,	,
<g/>
AAX	AAX	kA	AAX
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
<g/>
AIFF	AIFF	kA	AIFF
<g/>
,	,	kIx,	,
<g/>
WAW	WAW	kA	WAW
Telefon	telefon	k1gInSc1	telefon
podporuje	podporovat	k5eAaImIp3nS	podporovat
iOS	iOS	k?	iOS
10.3	[number]	k4	10.3
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
6S	[number]	k4	6S
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
iPhonu	iPhon	k1gInSc2	iPhon
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
i	i	k9	i
iPhone	iPhon	k1gInSc5	iPhon
6S	[number]	k4	6S
Plus	plus	k1gInSc1	plus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
nepřináší	přinášet	k5eNaImIp3nS	přinášet
mnoho	mnoho	k4c4	mnoho
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
výraznější	výrazný	k2eAgInSc4d2	výraznější
rozdíl	rozdíl	k1gInSc4	rozdíl
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
modelu	model	k1gInSc3	model
je	být	k5eAaImIp3nS	být
softwarový	softwarový	k2eAgInSc4d1	softwarový
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přináší	přinášet	k5eAaImIp3nS	přinášet
model	model	k1gInSc4	model
6S	[number]	k4	6S
nejvybavenější	vybavený	k2eAgInSc4d3	nejvybavenější
telefon	telefon	k1gInSc4	telefon
od	od	k7c2	od
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
<g/>
:	:	kIx,	:
vesmírně	vesmírně	k6eAd1	vesmírně
šedá	šedý	k2eAgNnPc1d1	šedé
<g/>
,	,	kIx,	,
stříbrná	stříbrný	k2eAgNnPc1d1	stříbrné
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgNnPc1d1	Zlaté
a	a	k8xC	a
růžová	růžový	k2eAgNnPc1d1	růžové
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
a	a	k8xC	a
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
Plus	plus	k1gInSc1	plus
jsou	být	k5eAaImIp3nP	být
smartphony	smartphon	k1gInPc4	smartphon
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
představeny	představit	k5eAaPmNgInP	představit
7	[number]	k4	7
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
v	v	k7c6	v
San	San	k1gMnSc6	San
Francisckém	Franciscký	k1gMnSc6	Franciscký
divadlu	divadlo	k1gNnSc3	divadlo
Billa	Bill	k1gMnSc2	Bill
Grahama	Graham	k1gMnSc2	Graham
jako	jako	k8xS	jako
nástupci	nástupce	k1gMnSc3	nástupce
modelu	model	k1gInSc2	model
iPhonu	iPhon	k1gInSc2	iPhon
6S	[number]	k4	6S
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
iPhonu	iPhon	k1gInSc2	iPhon
6S	[number]	k4	6S
Plus	plus	k1gInSc1	plus
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
disponuje	disponovat	k5eAaBmIp3nS	disponovat
úhlopříčkou	úhlopříčka	k1gFnSc7	úhlopříčka
4,7	[number]	k4	4,7
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
Plus	plus	k1gInSc1	plus
pak	pak	k6eAd1	pak
úhlopříčkou	úhlopříčka	k1gFnSc7	úhlopříčka
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
5,5	[number]	k4	5,5
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
displeje	displej	k1gInSc2	displej
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nS	lišit
od	od	k7c2	od
předchůdců	předchůdce	k1gMnPc2	předchůdce
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
<g/>
S.	S.	kA	S.
Oproti	oproti	k7c3	oproti
starším	starý	k2eAgMnPc3d2	starší
modelům	model	k1gInPc3	model
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
voděodolnost	voděodolnost	k1gFnSc1	voděodolnost
(	(	kIx(	(
<g/>
ve	v	k7c6	v
specifikaci	specifikace	k1gFnSc6	specifikace
IP	IP	kA	IP
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
většího	veliký	k2eAgInSc2d2	veliký
modelu	model	k1gInSc2	model
druhá	druhý	k4xOgFnSc1	druhý
čočka	čočka	k1gFnSc1	čočka
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
(	(	kIx(	(
<g/>
duální	duální	k2eAgFnSc1d1	duální
kamera	kamera	k1gFnSc1	kamera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
odebrán	odebrat	k5eAaPmNgInS	odebrat
3,5	[number]	k4	3,5
mm	mm	kA	mm
jack	jack	k6eAd1	jack
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
zvukového	zvukový	k2eAgInSc2d1	zvukový
výstupu	výstup	k1gInSc2	výstup
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sluchátka	sluchátko	k1gNnSc2	sluchátko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přibyla	přibýt	k5eAaPmAgFnS	přibýt
možnost	možnost	k1gFnSc1	možnost
si	se	k3xPyFc3	se
telefon	telefon	k1gInSc4	telefon
koupit	koupit	k5eAaPmF	koupit
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
černá	černat	k5eAaImIp3nS	černat
a	a	k8xC	a
leskle	leskle	k6eAd1	leskle
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Změnami	změna	k1gFnPc7	změna
prošel	projít	k5eAaPmAgInS	projít
tzv.	tzv.	kA	tzv.
Home	Home	k1gNnSc4	Home
Button	Button	k1gInSc1	Button
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
mechanický	mechanický	k2eAgInSc1d1	mechanický
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
sílu	síla	k1gFnSc4	síla
dotyku	dotyk	k1gInSc2	dotyk
pomocí	pomocí	k7c2	pomocí
haptické	haptický	k2eAgFnSc2d1	haptická
(	(	kIx(	(
<g/>
vibrační	vibrační	k2eAgFnSc2d1	vibrační
<g/>
)	)	kIx)	)
odezvy	odezva	k1gFnSc2	odezva
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
disponuje	disponovat	k5eAaBmIp3nS	disponovat
novými	nový	k2eAgInPc7d1	nový
stereo	stereo	k2eAgInPc7d1	stereo
reproduktory	reproduktor	k1gInPc7	reproduktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
hlasitější	hlasitý	k2eAgFnPc1d2	hlasitější
než	než	k8xS	než
u	u	k7c2	u
iPhone	iPhon	k1gMnSc5	iPhon
6	[number]	k4	6
<g/>
S.	S.	kA	S.
Telefon	telefon	k1gInSc1	telefon
pohání	pohánět	k5eAaImIp3nS	pohánět
čtyřjádrový	čtyřjádrový	k2eAgInSc1d1	čtyřjádrový
čipset	čipset	k1gInSc1	čipset
A10	A10	k1gFnPc2	A10
Fusion	Fusion	k1gInSc4	Fusion
a	a	k8xC	a
64	[number]	k4	64
bitový	bitový	k2eAgInSc4d1	bitový
procesor	procesor	k1gInSc4	procesor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
dvě	dva	k4xCgNnPc1	dva
jádra	jádro	k1gNnPc1	jádro
mají	mít	k5eAaImIp3nP	mít
vyšší	vysoký	k2eAgInSc4d2	vyšší
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
jádra	jádro	k1gNnPc4	jádro
pak	pak	k9	pak
výkon	výkon	k1gInSc1	výkon
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
zpracovávající	zpracovávající	k2eAgFnSc1d1	zpracovávající
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgFnPc4d1	náročná
operace	operace	k1gFnPc4	operace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
delší	dlouhý	k2eAgFnSc3d2	delší
výdrži	výdrž	k1gFnSc3	výdrž
baterie	baterie	k1gFnSc1	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
iOS	iOS	k?	iOS
10	[number]	k4	10
byly	být	k5eAaImAgFnP	být
připraveny	připraven	k2eAgFnPc1d1	připravena
2	[number]	k4	2
GB	GB	kA	GB
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
RAM	RAM	kA	RAM
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gInSc5	iPhon
7	[number]	k4	7
Plus	plus	k1gInSc1	plus
3	[number]	k4	3
<g/>
GB	GB	kA	GB
<g/>
.	.	kIx.	.
iPhony	iPhona	k1gFnSc2	iPhona
7	[number]	k4	7
a	a	k8xC	a
7	[number]	k4	7
Plus	plus	k1gInSc1	plus
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízet	k5eAaImNgInP	nabízet
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
v	v	k7c6	v
kapacitách	kapacita	k1gFnPc6	kapacita
32	[number]	k4	32
<g/>
,	,	kIx,	,
128	[number]	k4	128
a	a	k8xC	a
256	[number]	k4	256
GB	GB	kA	GB
<g/>
,	,	kIx,	,
v	v	k7c6	v
pěti	pět	k4xCc6	pět
barevných	barevný	k2eAgFnPc6d1	barevná
variantách	varianta	k1gFnPc6	varianta
–	–	k?	–
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
leskle	leskle	k6eAd1	leskle
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgFnSc1d1	zlatá
a	a	k8xC	a
rose	rosa	k1gFnSc3	rosa
gold	golda	k1gFnPc2	golda
(	(	kIx(	(
<g/>
růžová	růžový	k2eAgFnSc1d1	růžová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Apple	Apple	kA	Apple
představil	představit	k5eAaPmAgInS	představit
také	také	k9	také
červenou	červený	k2eAgFnSc4d1	červená
variantu	varianta	k1gFnSc4	varianta
těchto	tento	k3xDgInPc2	tento
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
produkt	produkt	k1gInSc4	produkt
RED	RED	kA	RED
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
operačním	operační	k2eAgInSc7d1	operační
systémem	systém	k1gInSc7	systém
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
počítačového	počítačový	k2eAgInSc2d1	počítačový
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
(	(	kIx(	(
<g/>
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
představení	představení	k1gNnSc3	představení
prvního	první	k4xOgInSc2	první
iPhonu	iPhon	k1gInSc2	iPhon
byl	být	k5eAaImAgInS	být
systém	systém	k1gInSc1	systém
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
OS	OS	kA	OS
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
intuitivní	intuitivní	k2eAgNnSc4d1	intuitivní
ovládání	ovládání	k1gNnSc4	ovládání
a	a	k8xC	a
propojení	propojení	k1gNnSc4	propojení
s	s	k7c7	s
internetem	internet	k1gInSc7	internet
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
Safari	safari	k1gNnSc2	safari
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc1d1	běžný
na	na	k7c6	na
Mac	Mac	kA	Mac
OS	OS	kA	OS
X.	X.	kA	X.
Na	na	k7c6	na
iPhonu	iPhon	k1gInSc6	iPhon
naleznete	naleznout	k5eAaPmIp2nP	naleznout
verzi	verze	k1gFnSc4	verze
zjednodušenou	zjednodušený	k2eAgFnSc4d1	zjednodušená
pro	pro	k7c4	pro
snadné	snadný	k2eAgNnSc4d1	snadné
ovládání	ovládání	k1gNnSc4	ovládání
pomocí	pomocí	k7c2	pomocí
dotykového	dotykový	k2eAgInSc2d1	dotykový
displeje	displej	k1gInSc2	displej
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
záložky	záložka	k1gFnPc4	záložka
a	a	k8xC	a
panely	panel	k1gInPc4	panel
(	(	kIx(	(
<g/>
tabs	tabs	k1gInSc4	tabs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
obsahující	obsahující	k2eAgInSc4d1	obsahující
HTML	HTML	kA	HTML
<g/>
,	,	kIx,	,
JavaScript	JavaScript	k1gInSc1	JavaScript
a	a	k8xC	a
AJAX	AJAX	kA	AJAX
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
technologie	technologie	k1gFnSc1	technologie
Java	Java	k1gFnSc1	Java
a	a	k8xC	a
Flash	Flash	k1gInSc1	Flash
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prohlížení	prohlížení	k1gNnSc4	prohlížení
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
e-mailů	eail	k1gInPc2	e-mail
slouží	sloužit	k5eAaImIp3nS	sloužit
aplikace	aplikace	k1gFnSc1	aplikace
Mail	mail	k1gInSc1	mail
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
Mapy	mapa	k1gFnSc2	mapa
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
mapy	mapa	k1gFnPc4	mapa
a	a	k8xC	a
družicové	družicový	k2eAgInPc4d1	družicový
snímky	snímek	k1gInPc4	snímek
Země	zem	k1gFnSc2	zem
stahované	stahovaný	k2eAgFnSc2d1	stahovaná
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
sestavené	sestavený	k2eAgInPc1d1	sestavený
Applem	Appl	k1gInSc7	Appl
<g/>
.	.	kIx.	.
</s>
<s>
Maps	Maps	k6eAd1	Maps
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
cesty	cesta	k1gFnPc4	cesta
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
<g/>
,	,	kIx,	,
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
intenzitu	intenzita	k1gFnSc4	intenzita
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
krokovou	krokový	k2eAgFnSc4d1	kroková
navigaci	navigace	k1gFnSc4	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
firmwaru	firmware	k1gInSc2	firmware
1.1	[number]	k4	1.1
<g/>
.3	.3	k4	.3
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stanovit	stanovit	k5eAaPmF	stanovit
polohu	poloha	k1gFnSc4	poloha
telefonu	telefon	k1gInSc2	telefon
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
podle	podle	k7c2	podle
okolních	okolní	k2eAgInPc2d1	okolní
přístupových	přístupový	k2eAgInPc2d1	přístupový
bodů	bod	k1gInPc2	bod
wi-fi	wi	k1gFnSc2	wi-f
a	a	k8xC	a
BTS	BTS	kA	BTS
telefonního	telefonní	k2eAgMnSc4d1	telefonní
operátora	operátor	k1gMnSc4	operátor
<g/>
,	,	kIx,	,
na	na	k7c4	na
3G	[number]	k4	3G
iPhonu	iPhon	k1gInSc2	iPhon
i	i	k8xC	i
pomocí	pomocí	k7c2	pomocí
GPS	GPS	kA	GPS
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
iPhonu	iPhon	k1gInSc6	iPhon
3GS	[number]	k4	3GS
a	a	k8xC	a
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
S	s	k7c7	s
a	a	k8xC	a
na	na	k7c6	na
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
S	s	k7c7	s
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nechat	nechat	k5eAaPmF	nechat
mapu	mapa	k1gFnSc4	mapa
automaticky	automaticky	k6eAd1	automaticky
natočit	natočit	k5eAaBmF	natočit
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
iPhone	iPhon	k1gInSc5	iPhon
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
multimediální	multimediální	k2eAgInSc1d1	multimediální
přehrávač	přehrávač	k1gInSc1	přehrávač
iPod	iPod	k1gInSc1	iPod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přehraje	přehrát	k5eAaPmIp3nS	přehrát
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
videa	video	k1gNnPc1	video
a	a	k8xC	a
podcasty	podcast	k1gInPc1	podcast
nahrané	nahraný	k2eAgInPc1d1	nahraný
na	na	k7c4	na
telefon	telefon	k1gInSc4	telefon
z	z	k7c2	z
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
iTunes	iTunesa	k1gFnPc2	iTunesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
otvírají	otvírat	k5eAaImIp3nP	otvírat
i	i	k9	i
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
videa	video	k1gNnSc2	video
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
Safari	safari	k1gNnSc1	safari
<g/>
,	,	kIx,	,
Mail	mail	k1gInSc1	mail
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prohlížet	prohlížet	k5eAaImF	prohlížet
a	a	k8xC	a
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
lze	lze	k6eAd1	lze
také	také	k9	také
obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zaznamenání	zaznamenání	k1gNnSc3	zaznamenání
slouží	sloužit	k5eAaImIp3nS	sloužit
digitální	digitální	k2eAgInSc4d1	digitální
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c6	na
iPhonu	iPhon	k1gInSc6	iPhon
naleznete	naleznout	k5eAaPmIp2nP	naleznout
kalkulačku	kalkulačka	k1gFnSc4	kalkulačka
(	(	kIx(	(
<g/>
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
2.0	[number]	k4	2.0
i	i	k9	i
s	s	k7c7	s
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
funkcemi	funkce	k1gFnPc7	funkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc4	poznámka
a	a	k8xC	a
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
internetu	internet	k1gInSc2	internet
také	také	k6eAd1	také
aktuální	aktuální	k2eAgNnSc4d1	aktuální
počasí	počasí	k1gNnSc4	počasí
a	a	k8xC	a
kurzy	kurz	k1gInPc4	kurz
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
prodeje	prodej	k1gInSc2	prodej
iPhonu	iPhon	k1gInSc2	iPhon
přibyla	přibýt	k5eAaPmAgFnS	přibýt
možnost	možnost	k1gFnSc4	možnost
využívat	využívat	k5eAaImF	využívat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
telefonu	telefon	k1gInSc6	telefon
iTunes	iTunesa	k1gFnPc2	iTunesa
Store	Stor	k1gInSc5	Stor
a	a	k8xC	a
ve	v	k7c6	v
firmwaru	firmware	k1gInSc6	firmware
2.0	[number]	k4	2.0
App	App	k1gMnSc5	App
Store	Stor	k1gMnSc5	Stor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
3.0	[number]	k4	3.0
přibyla	přibýt	k5eAaPmAgFnS	přibýt
možnost	možnost	k1gFnSc1	možnost
pořizování	pořizování	k1gNnSc2	pořizování
audiozáznamů	audiozáznam	k1gInPc2	audiozáznam
<g/>
,	,	kIx,	,
na	na	k7c4	na
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
je	být	k5eAaImIp3nS	být
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
natáčet	natáčet	k5eAaImF	natáčet
video	video	k1gNnSc4	video
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
aplikace	aplikace	k1gFnPc1	aplikace
jsou	být	k5eAaImIp3nP	být
předinstalovány	předinstalovat	k5eAaPmNgFnP	předinstalovat
na	na	k7c6	na
iPhonu	iPhon	k1gInSc6	iPhon
s	s	k7c7	s
firmware	firmware	k1gInSc1	firmware
3.0	[number]	k4	3.0
a	a	k8xC	a
vyšším	vysoký	k2eAgMnSc6d2	vyšší
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc6	pořadí
podle	podle	k7c2	podle
výchozího	výchozí	k2eAgNnSc2d1	výchozí
třídění	třídění	k1gNnSc2	třídění
<g/>
,	,	kIx,	,
na	na	k7c6	na
prvních	první	k4xOgInPc6	první
4	[number]	k4	4
místech	místo	k1gNnPc6	místo
aplikace	aplikace	k1gFnSc2	aplikace
v	v	k7c6	v
doku	dok	k1gInSc6	dok
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Telefon	telefon	k1gInSc1	telefon
Mail	mail	k1gInSc1	mail
Safari	safari	k1gNnSc2	safari
Hudba	hudba	k1gFnSc1	hudba
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
iPod	iPod	k6eAd1	iPod
<g/>
)	)	kIx)	)
Zprávy	zpráva	k1gFnPc1	zpráva
Kalendář	kalendář	k1gInSc4	kalendář
Obrázky	obrázek	k1gInPc1	obrázek
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
YouTube	YouTub	k1gInSc5	YouTub
Akcie	akcie	k1gFnSc1	akcie
Mapy	mapa	k1gFnSc2	mapa
Počasí	počasí	k1gNnSc1	počasí
Diktafon	diktafon	k1gInSc4	diktafon
Poznámky	poznámka	k1gFnSc2	poznámka
<g />
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
Kalkulačka	kalkulačka	k1gFnSc1	kalkulačka
Nastavení	nastavení	k1gNnSc2	nastavení
iTunes	iTunesa	k1gFnPc2	iTunesa
Store	Stor	k1gMnSc5	Stor
App	App	k1gMnSc5	App
Store	Stor	k1gMnSc5	Stor
Kompas	kompas	k1gInSc1	kompas
–	–	k?	–
pouze	pouze	k6eAd1	pouze
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
a	a	k8xC	a
novější	nový	k2eAgInPc1d2	novější
Kontakty	kontakt	k1gInPc1	kontakt
Game	game	k1gInSc4	game
Center	centrum	k1gNnPc2	centrum
–	–	k?	–
od	od	k7c2	od
iOS	iOS	k?	iOS
4.1	[number]	k4	4.1
na	na	k7c6	na
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
a	a	k8xC	a
novějších	nový	k2eAgInPc2d2	novější
<g/>
,	,	kIx,	,
u	u	k7c2	u
iOS	iOS	k?	iOS
10	[number]	k4	10
chybí	chybět	k5eAaImIp3nS	chybět
Kiosek	kiosek	k1gInSc1	kiosek
-	-	kIx~	-
od	od	k7c2	od
iOS	iOS	k?	iOS
5	[number]	k4	5
Připomínky	připomínka	k1gFnSc2	připomínka
-	-	kIx~	-
od	od	k7c2	od
iOS	iOS	k?	iOS
5	[number]	k4	5
Passbook	Passbook	k1gInSc1	Passbook
-	-	kIx~	-
od	od	k7c2	od
iOS	iOS	k?	iOS
6	[number]	k4	6
Podcasty	Podcast	k1gInPc4	Podcast
-	-	kIx~	-
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
iOS	iOS	k?	iOS
6	[number]	k4	6
Aplikace	aplikace	k1gFnSc1	aplikace
Telefon	telefon	k1gInSc1	telefon
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
telefonování	telefonování	k1gNnSc3	telefonování
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
skrytých	skrytý	k2eAgInPc2d1	skrytý
kódů	kód	k1gInPc2	kód
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zobrazit	zobrazit	k5eAaPmF	zobrazit
některé	některý	k3yIgFnPc4	některý
vlastnosti	vlastnost	k1gFnPc4	vlastnost
telefonu	telefon	k1gInSc2	telefon
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kód	kód	k1gInSc4	kód
*	*	kIx~	*
<g/>
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
#	#	kIx~	#
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
číslo	číslo	k1gNnSc4	číslo
IMEI	IMEI	kA	IMEI
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
31	[number]	k4	31
<g/>
#	#	kIx~	#
<g/>
telefonní	telefonní	k2eAgNnSc1d1	telefonní
číslo	číslo	k1gNnSc1	číslo
+	+	kIx~	+
"	"	kIx"	"
<g/>
Call	Call	k1gInSc1	Call
<g/>
"	"	kIx"	"
zablokuje	zablokovat	k5eAaPmIp3nS	zablokovat
zobrazování	zobrazování	k1gNnSc4	zobrazování
čísla	číslo	k1gNnSc2	číslo
uživatele	uživatel	k1gMnSc2	uživatel
v	v	k7c6	v
aktuálním	aktuální	k2eAgInSc6d1	aktuální
hovoru	hovor	k1gInSc6	hovor
<g/>
,	,	kIx,	,
či	či	k8xC	či
*	*	kIx~	*
<g/>
#	#	kIx~	#
<g/>
5005	[number]	k4	5005
<g/>
*	*	kIx~	*
<g/>
7672	[number]	k4	7672
<g/>
#	#	kIx~	#
+	+	kIx~	+
"	"	kIx"	"
<g/>
Call	Call	k1gInSc1	Call
<g/>
"	"	kIx"	"
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
číslo	číslo	k1gNnSc4	číslo
SMS	SMS	kA	SMS
centra	centr	k1gMnSc2	centr
aktuálního	aktuální	k2eAgMnSc2d1	aktuální
operátora	operátor	k1gMnSc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
schválené	schválený	k2eAgFnPc1d1	schválená
Applem	Appl	k1gInSc7	Appl
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
pomocí	pomocí	k7c2	pomocí
App	App	k1gFnSc2	App
Store	Stor	k1gMnSc5	Stor
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
předinstalovanou	předinstalovaný	k2eAgFnSc4d1	předinstalovaná
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
internetového	internetový	k2eAgInSc2d1	internetový
obchodu	obchod	k1gInSc2	obchod
iTunes	iTunes	k1gMnSc1	iTunes
Store	Stor	k1gInSc5	Stor
věnovaná	věnovaný	k2eAgNnPc1d1	věnované
aplikacím	aplikace	k1gFnPc3	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Přistupovat	přistupovat	k5eAaImF	přistupovat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
z	z	k7c2	z
počítače	počítač	k1gInSc2	počítač
pomocí	pomocí	k7c2	pomocí
programu	program	k1gInSc2	program
iTunes	iTunesa	k1gFnPc2	iTunesa
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
aplikace	aplikace	k1gFnPc1	aplikace
z	z	k7c2	z
iPhonu	iPhon	k1gInSc2	iPhon
s	s	k7c7	s
firmwarem	firmware	k1gInSc7	firmware
verze	verze	k1gFnSc1	verze
2.0	[number]	k4	2.0
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
Apple	Apple	kA	Apple
představil	představit	k5eAaPmAgInS	představit
betaverzi	betaverze	k1gFnSc4	betaverze
SDK	SDK	kA	SDK
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vytváření	vytváření	k1gNnSc4	vytváření
nativních	nativní	k2eAgFnPc2d1	nativní
aplikací	aplikace	k1gFnPc2	aplikace
třetím	třetí	k4xOgFnPc3	třetí
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc4	aplikace
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
instalovat	instalovat	k5eAaBmF	instalovat
na	na	k7c4	na
firmware	firmware	k1gInSc4	firmware
verze	verze	k1gFnSc2	verze
2.0	[number]	k4	2.0
a	a	k8xC	a
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
možnost	možnost	k1gFnSc1	možnost
nahrávání	nahrávání	k1gNnSc2	nahrávání
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
používat	používat	k5eAaImF	používat
aplikace	aplikace	k1gFnSc1	aplikace
Installer	Installer	k1gInSc4	Installer
–	–	k?	–
vlastně	vlastně	k9	vlastně
nelegální	legální	k2eNgFnSc1d1	nelegální
obdoba	obdoba	k1gFnSc1	obdoba
App	App	k1gMnSc5	App
Store	Stor	k1gMnSc5	Stor
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
iPhonu	iPhon	k1gInSc2	iPhon
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
iPodu	iPoda	k1gFnSc4	iPoda
Touch	Toucha	k1gFnPc2	Toucha
<g/>
)	)	kIx)	)
dá	dát	k5eAaPmIp3nS	dát
nahrát	nahrát	k5eAaBmF	nahrát
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
jailbreaku	jailbreak	k1gInSc6	jailbreak
(	(	kIx(	(
<g/>
zpřístupní	zpřístupnit	k5eAaPmIp3nS	zpřístupnit
iPhone	iPhon	k1gInSc5	iPhon
neschváleným	schválený	k2eNgFnPc3d1	neschválená
nativním	nativní	k2eAgFnPc3d1	nativní
aplikacím	aplikace	k1gFnPc3	aplikace
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
chyby	chyba	k1gFnSc2	chyba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1.0	[number]	k4	1.0
<g/>
.2	.2	k4	.2
–	–	k?	–
nejstarší	starý	k2eAgInSc1d3	nejstarší
firmware	firmware	k1gInSc1	firmware
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
volně	volně	k6eAd1	volně
prodávaných	prodávaný	k2eAgInPc2d1	prodávaný
telefonů	telefon	k1gInPc2	telefon
1.1	[number]	k4	1.1
<g/>
.1	.1	k4	.1
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nejoblíbenější	oblíbený	k2eAgInSc1d3	nejoblíbenější
firmware	firmware	k1gInSc1	firmware
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
neschválených	schválený	k2eNgFnPc2d1	neschválená
aplikací	aplikace	k1gFnPc2	aplikace
1.1	[number]	k4	1.1
<g/>
.2	.2	k4	.2
–	–	k?	–
posílení	posílení	k1gNnSc4	posílení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
šel	jít	k5eAaImAgMnS	jít
odblokovat	odblokovat	k5eAaPmF	odblokovat
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
TurboSIM	TurboSIM	k1gFnSc2	TurboSIM
1.1	[number]	k4	1.1
<g/>
.3	.3	k4	.3
–	–	k?	–
nové	nový	k2eAgFnSc2d1	nová
funkce	funkce	k1gFnSc2	funkce
–	–	k?	–
určení	určení	k1gNnSc1	určení
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
Store	Stor	k1gInSc5	Stor
1.1	[number]	k4	1.1
<g/>
.4	.4	k4	.4
–	–	k?	–
další	další	k2eAgFnSc2d1	další
nové	nový	k2eAgFnSc2d1	nová
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
aplikací	aplikace	k1gFnPc2	aplikace
instalovaných	instalovaný	k2eAgFnPc2d1	instalovaná
přes	přes	k7c4	přes
<g />
.	.	kIx.	.
</s>
<s>
Installer	Installer	k1gMnSc1	Installer
je	být	k5eAaImIp3nS	být
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
s	s	k7c7	s
1.1	[number]	k4	1.1
<g/>
.4	.4	k4	.4
2.0	[number]	k4	2.0
<g/>
.0	.0	k4	.0
–	–	k?	–
podpora	podpora	k1gFnSc1	podpora
Microsoft	Microsoft	kA	Microsoft
Exchange	Exchange	k1gFnSc1	Exchange
Serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
stahování	stahování	k1gNnSc4	stahování
a	a	k8xC	a
instalaci	instalace	k1gFnSc4	instalace
přídavných	přídavný	k2eAgFnPc2d1	přídavná
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
App	App	k1gMnSc5	App
Store	Stor	k1gMnSc5	Stor
<g/>
)	)	kIx)	)
2.0	[number]	k4	2.0
<g/>
.1	.1	k4	.1
2.0	[number]	k4	2.0
<g/>
.2	.2	k4	.2
–	–	k?	–
oprava	oprava	k1gFnSc1	oprava
problémů	problém	k1gInPc2	problém
s	s	k7c7	s
3G	[number]	k4	3G
připojením	připojení	k1gNnSc7	připojení
a	a	k8xC	a
pomalou	pomalý	k2eAgFnSc7d1	pomalá
odezvou	odezva	k1gFnSc7	odezva
klávesnice	klávesnice	k1gFnSc2	klávesnice
2.1	[number]	k4	2.1
<g/>
.0	.0	k4	.0
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
vydána	vydán	k2eAgFnSc1d1	vydána
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
českou	český	k2eAgFnSc4d1	Česká
QWERTY	QWERTY	kA	QWERTY
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
,	,	kIx,	,
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
stabilitu	stabilita	k1gFnSc4	stabilita
s	s	k7c7	s
aplikacemi	aplikace	k1gFnPc7	aplikace
třetích	třetí	k4xOgInPc6	třetí
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
výdrž	výdrž	k1gFnSc4	výdrž
baterie	baterie	k1gFnSc1	baterie
2.2	[number]	k4	2.2
<g/>
.0	.0	k4	.0
2.2	[number]	k4	2.2
<g/>
.1	.1	k4	.1
–	–	k?	–
drobné	drobný	k2eAgFnSc2d1	drobná
opravy	oprava	k1gFnSc2	oprava
problémů	problém	k1gInPc2	problém
3.0	[number]	k4	3.0
–	–	k?	–
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
vylepšení	vylepšení	k1gNnSc1	vylepšení
jako	jako	k8xS	jako
kopírování	kopírování	k1gNnSc1	kopírování
a	a	k8xC	a
vkládání	vkládání	k1gNnSc1	vkládání
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
sdílení	sdílení	k1gNnSc2	sdílení
připojení	připojení	k1gNnSc2	připojení
<g/>
,	,	kIx,	,
zasílání	zasílání	k1gNnSc2	zasílání
MMS	MMS	kA	MMS
<g/>
,	,	kIx,	,
nahrávání	nahrávání	k1gNnSc2	nahrávání
videa	video	k1gNnSc2	video
(	(	kIx(	(
<g/>
s	s	k7c7	s
modelem	model	k1gInSc7	model
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
a	a	k8xC	a
opravuje	opravovat	k5eAaImIp3nS	opravovat
46	[number]	k4	46
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
děr	děra	k1gFnPc2	děra
3.0	[number]	k4	3.0
<g/>
.1	.1	k4	.1
–	–	k?	–
vydána	vydán	k2eAgFnSc1d1	vydána
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
opravuje	opravovat	k5eAaImIp3nS	opravovat
závažnou	závažný	k2eAgFnSc4d1	závažná
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
–	–	k?	–
možnost	možnost	k1gFnSc1	možnost
spustit	spustit	k5eAaPmF	spustit
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
kód	kód	k1gInSc4	kód
zaslaný	zaslaný	k2eAgMnSc1d1	zaslaný
pomocí	pomoc	k1gFnSc7	pomoc
<g />
.	.	kIx.	.
</s>
<s>
SMS	SMS	kA	SMS
3.1	[number]	k4	3.1
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
vydána	vydán	k2eAgFnSc1d1	vydána
2	[number]	k4	2
<g/>
.	.	kIx.	.
betaverze	betaverze	k1gFnSc2	betaverze
<g/>
,	,	kIx,	,
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
díry	díra	k1gFnPc4	díra
<g/>
,	,	kIx,	,
vylepšuje	vylepšovat	k5eAaImIp3nS	vylepšovat
střih	střih	k1gInSc1	střih
videa	video	k1gNnSc2	video
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
připojení	připojení	k1gNnSc4	připojení
s	s	k7c7	s
iPhone	iPhon	k1gInSc5	iPhon
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
API	API	kA	API
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
videem	video	k1gNnSc7	video
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
umožní	umožnit	k5eAaPmIp3nS	umožnit
vznik	vznik	k1gInSc1	vznik
aplikací	aplikace	k1gFnPc2	aplikace
využívající	využívající	k2eAgFnSc2d1	využívající
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
<g />
.	.	kIx.	.
</s>
<s>
reality	realita	k1gFnPc1	realita
<g/>
.	.	kIx.	.
3.1	[number]	k4	3.1
<g/>
.2	.2	k4	.2
–	–	k?	–
opravuje	opravovat	k5eAaImIp3nS	opravovat
problém	problém	k1gInSc1	problém
z	z	k7c2	z
probuzení	probuzení	k1gNnSc2	probuzení
iPhonu	iPhon	k1gInSc2	iPhon
3.1	[number]	k4	3.1
<g/>
.3	.3	k4	.3
–	–	k?	–
opravuje	opravovat	k5eAaImIp3nS	opravovat
některé	některý	k3yIgFnPc4	některý
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnPc4d1	poslední
verze	verze	k1gFnPc4	verze
podporující	podporující	k2eAgFnPc4d1	podporující
původní	původní	k2eAgFnSc2d1	původní
iPhone	iPhon	k1gInSc5	iPhon
4.0	[number]	k4	4.0
–	–	k?	–
vydáno	vydán	k2eAgNnSc1d1	vydáno
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
100	[number]	k4	100
nových	nový	k2eAgFnPc2d1	nová
vlastností	vlastnost	k1gFnPc2	vlastnost
–	–	k?	–
multitasking	multitasking	k1gInSc1	multitasking
<g/>
,	,	kIx,	,
složky	složka	k1gFnPc1	složka
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
aplikace	aplikace	k1gFnSc2	aplikace
Mail	mail	k1gInSc1	mail
a	a	k8xC	a
reklamní	reklamní	k2eAgFnSc1d1	reklamní
platforma	platforma	k1gFnSc1	platforma
iAd	iAd	k?	iAd
4.0	[number]	k4	4.0
<g/>
.1	.1	k4	.1
–	–	k?	–
upravuje	upravovat	k5eAaImIp3nS	upravovat
citlivost	citlivost	k1gFnSc4	citlivost
a	a	k8xC	a
vzhled	vzhled	k1gInSc4	vzhled
ukazatele	ukazatel	k1gInPc1	ukazatel
signálu	signál	k1gInSc2	signál
4.0	[number]	k4	4.0
<g/>
.2	.2	k4	.2
–	–	k?	–
opravuje	opravovat	k5eAaImIp3nS	opravovat
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
chybu	chyba	k1gFnSc4	chyba
v	v	k7c6	v
prohlížeči	prohlížeč	k1gInSc6	prohlížeč
Safari	safari	k1gNnSc2	safari
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
automaticky	automaticky	k6eAd1	automaticky
otevírala	otevírat	k5eAaImAgFnS	otevírat
záškodnické	záškodnický	k2eAgInPc4d1	záškodnický
soubory	soubor	k1gInPc4	soubor
PDF	PDF	kA	PDF
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
chybě	chyba	k1gFnSc3	chyba
byl	být	k5eAaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
jailbreak	jailbreak	k1gInSc1	jailbreak
přes	přes	k7c4	přes
jailbreakme	jailbreakmat	k5eAaPmIp3nS	jailbreakmat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
4.1	[number]	k4	4.1
–	–	k?	–
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
rychlost	rychlost	k1gFnSc1	rychlost
iPhonu	iPhon	k1gInSc2	iPhon
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
do	do	k7c2	do
modelů	model	k1gInPc2	model
3GS	[number]	k4	3GS
a	a	k8xC	a
4	[number]	k4	4
přibyl	přibýt	k5eAaPmAgInS	přibýt
GameCenter	GameCenter	k1gInSc1	GameCenter
<g/>
,	,	kIx,	,
přidána	přidán	k2eAgFnSc1d1	přidána
funkce	funkce	k1gFnSc1	funkce
HDR	HDR	kA	HDR
fotografií	fotografia	k1gFnSc7	fotografia
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
předplatit	předplatit	k5eAaPmF	předplatit
si	se	k3xPyFc3	se
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
seriály	seriál	k1gInPc4	seriál
v	v	k7c4	v
iTunes	iTunes	k1gInSc4	iTunes
Store	Stor	k1gInSc5	Stor
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
přidána	přidán	k2eAgFnSc1d1	přidána
hudební	hudební	k2eAgFnSc1d1	hudební
sociální	sociální	k2eAgFnSc1d1	sociální
síť	síť	k1gFnSc1	síť
Applu	Appl	k1gInSc2	Appl
Ping	pingo	k1gNnPc2	pingo
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
uploadovat	uploadovat	k5eAaImF	uploadovat
nahraná	nahraný	k2eAgNnPc4d1	nahrané
videa	video	k1gNnPc4	video
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
4.2	[number]	k4	4.2
<g/>
.1	.1	k4	.1
–	–	k?	–
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
rychlost	rychlost	k1gFnSc1	rychlost
iPhonu	iPhon	k1gInSc2	iPhon
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
funkci	funkce	k1gFnSc4	funkce
AirPlay	AirPlaa	k1gFnSc2	AirPlaa
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
streamu	stream	k1gInSc2	stream
videa	video	k1gNnSc2	video
a	a	k8xC	a
hudby	hudba	k1gFnSc2	hudba
z	z	k7c2	z
iPhonu	iPhon	k1gInSc2	iPhon
do	do	k7c2	do
Apple	Apple	kA	Apple
TV	TV	kA	TV
nebo	nebo	k8xC	nebo
iTunes	iTunes	k1gMnSc1	iTunes
<g/>
,	,	kIx,	,
AirPrint	AirPrint	k1gMnSc1	AirPrint
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
tisknout	tisknout	k5eAaImF	tisknout
přes	přes	k7c4	přes
wifi	wifi	k1gNnSc4	wifi
a	a	k8xC	a
ovládání	ovládání	k1gNnSc4	ovládání
hlasitosti	hlasitost	k1gFnSc2	hlasitost
do	do	k7c2	do
multitasking	multitasking	k1gInSc4	multitasking
docku	docka	k1gFnSc4	docka
<g/>
.	.	kIx.	.
4.3	[number]	k4	4.3
–	–	k?	–
ukončena	ukončen	k2eAgFnSc1d1	ukončena
podpora	podpora	k1gFnSc1	podpora
modelu	model	k1gInSc2	model
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
drobná	drobný	k2eAgNnPc4d1	drobné
vylepšení	vylepšení	k1gNnPc4	vylepšení
služby	služba	k1gFnSc2	služba
AirPlay	AirPlaa	k1gFnSc2	AirPlaa
<g/>
,	,	kIx,	,
dramaticky	dramaticky	k6eAd1	dramaticky
zvýšen	zvýšen	k2eAgInSc1d1	zvýšen
výkon	výkon	k1gInSc1	výkon
jádra	jádro	k1gNnSc2	jádro
Safari	safari	k1gNnSc2	safari
(	(	kIx(	(
<g/>
webového	webový	k2eAgMnSc2d1	webový
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnSc3	možnost
vytvořit	vytvořit	k5eAaPmF	vytvořit
Wi-Fi	Wi-Fe	k1gFnSc4	Wi-Fe
hotspot	hotspota	k1gFnPc2	hotspota
(	(	kIx(	(
<g/>
jen	jen	k9	jen
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5.0	[number]	k4	5.0
–	–	k?	–
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
iPhone	iPhon	k1gInSc5	iPhon
4S	[number]	k4	4S
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k8xC	i
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
2	[number]	k4	2
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
touch	touch	k1gMnSc1	touch
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Přibylo	přibýt	k5eAaPmAgNnS	přibýt
např.	např.	kA	např.
nové	nový	k2eAgNnSc1d1	nové
notifikační	notifikační	k2eAgNnSc1d1	notifikační
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
integrace	integrace	k1gFnSc1	integrace
s	s	k7c7	s
Twitterem	Twitter	k1gInSc7	Twitter
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc1	aplikace
iMessage	iMessage	k1gFnSc1	iMessage
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
propojení	propojení	k1gNnSc2	propojení
s	s	k7c7	s
PC	PC	kA	PC
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
použít	použít	k5eAaPmF	použít
USB	USB	kA	USB
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zvýšeno	zvýšit	k5eAaPmNgNnS	zvýšit
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
proti	proti	k7c3	proti
úniku	únik	k1gInSc3	únik
citlivých	citlivý	k2eAgNnPc2d1	citlivé
dat	datum	k1gNnPc2	datum
při	při	k7c6	při
firemním	firemní	k2eAgNnSc6d1	firemní
nasazení	nasazení	k1gNnSc6	nasazení
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
větších	veliký	k2eAgFnPc2d2	veliký
možností	možnost	k1gFnPc2	možnost
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
správy	správa	k1gFnSc2	správa
IT	IT	kA	IT
administrátory	administrátor	k1gMnPc7	administrátor
<g/>
.	.	kIx.	.
5.0	[number]	k4	5.0
<g/>
.1	.1	k4	.1
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
oprava	oprava	k1gFnSc1	oprava
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
5.1	[number]	k4	5.1
–	–	k?	–
vylepšení	vylepšení	k1gNnSc4	vylepšení
výdrže	výdrž	k1gFnSc2	výdrž
baterie	baterie	k1gFnSc2	baterie
zejména	zejména	k9	zejména
u	u	k7c2	u
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
vypínaní	vypínaný	k2eAgMnPc1d1	vypínaný
3G	[number]	k4	3G
u	u	k7c2	u
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
na	na	k7c6	na
lockscreenu	lockscreeno	k1gNnSc6	lockscreeno
posuvné	posuvný	k2eAgNnSc4d1	posuvné
tlačítko	tlačítko	k1gNnSc4	tlačítko
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgMnSc2	který
lze	lze	k6eAd1	lze
aktivovat	aktivovat	k5eAaBmF	aktivovat
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
snadno	snadno	k6eAd1	snadno
i	i	k9	i
ze	z	k7c2	z
zamčené	zamčený	k2eAgFnSc2d1	zamčená
<g />
.	.	kIx.	.
</s>
<s>
obrazovky	obrazovka	k1gFnPc4	obrazovka
5.1	[number]	k4	5.1
<g/>
.1	.1	k4	.1
–	–	k?	–
spolehlivější	spolehlivý	k2eAgNnSc1d2	spolehlivější
používání	používání	k1gNnSc1	používání
funkce	funkce	k1gFnSc2	funkce
HDR	HDR	kA	HDR
při	při	k7c6	při
focení	focení	k1gNnSc6	focení
skrze	skrze	k?	skrze
zkratku	zkratka	k1gFnSc4	zkratka
ze	z	k7c2	z
zamykací	zamykací	k2eAgFnSc2d1	zamykací
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
,	,	kIx,	,
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
synchronizace	synchronizace	k1gFnSc1	synchronizace
záložek	záložka	k1gFnPc2	záložka
a	a	k8xC	a
seznamů	seznam	k1gInPc2	seznam
prohlížeče	prohlížeč	k1gMnSc2	prohlížeč
safari	safari	k1gNnPc2	safari
<g/>
,	,	kIx,	,
oprava	oprava	k1gFnSc1	oprava
chybové	chybový	k2eAgFnSc2d1	chybová
hlášky	hláška	k1gFnSc2	hláška
"	"	kIx"	"
<g/>
Nelze	lze	k6eNd1	lze
koupit	koupit	k5eAaPmF	koupit
<g/>
"	"	kIx"	"
po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
nákupu	nákup	k1gInSc6	nákup
<g/>
,	,	kIx,	,
oprava	oprava	k1gFnSc1	oprava
problémů	problém	k1gInPc2	problém
při	při	k7c6	při
přehrávání	přehrávání	k1gNnSc6	přehrávání
přes	přes	k7c4	přes
AirPlay	AirPlay	k1gInPc4	AirPlay
6.0	[number]	k4	6.0
–	–	k?	–
představena	představen	k2eAgFnSc1d1	představena
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
dostupná	dostupný	k2eAgFnSc1d1	dostupná
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Přináší	přinášet	k5eAaImIp3nS	přinášet
200	[number]	k4	200
nových	nový	k2eAgFnPc2d1	nová
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgNnPc7	jenž
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
mapy	mapa	k1gFnPc1	mapa
od	od	k7c2	od
Applu	Appl	k1gInSc2	Appl
<g/>
,	,	kIx,	,
automobilová	automobilový	k2eAgFnSc1d1	automobilová
navigace	navigace	k1gFnSc1	navigace
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnSc2d1	nová
funkce	funkce	k1gFnSc2	funkce
Siri	Siri	k1gNnSc1	Siri
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
režim	režim	k1gInSc4	režim
Nerušit	rušit	k5eNaImF	rušit
<g/>
,	,	kIx,	,
integrace	integrace	k1gFnSc1	integrace
Facebooku	Facebook	k1gInSc2	Facebook
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnPc1	aplikace
Passbook	Passbook	k1gInSc1	Passbook
<g/>
,	,	kIx,	,
Facetime	Facetim	k1gInSc5	Facetim
přes	přes	k7c4	přes
mobilní	mobilní	k2eAgFnPc4d1	mobilní
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
sdílený	sdílený	k2eAgInSc4d1	sdílený
Photostream	Photostream	k1gInSc4	Photostream
<g/>
<g />
.	.	kIx.	.
</s>
<s>
iOS	iOS	k?	iOS
6	[number]	k4	6
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
dostupný	dostupný	k2eAgMnSc1d1	dostupný
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
2	[number]	k4	2
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
3	[number]	k4	3
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
touch	touch	k1gMnSc1	touch
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
.	.	kIx.	.
6.0	[number]	k4	6.0
<g/>
.1	.1	k4	.1
-	-	kIx~	-
Oprava	oprava	k1gFnSc1	oprava
chyby	chyba	k1gFnSc2	chyba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zabraňovala	zabraňovat	k5eAaImAgFnS	zabraňovat
iPhone	iPhon	k1gInSc5	iPhon
5	[number]	k4	5
v	v	k7c6	v
bezdrátové	bezdrátový	k2eAgFnSc6d1	bezdrátová
instalaci	instalace	k1gFnSc6	instalace
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
chyby	chyba	k1gFnSc2	chyba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
zobrazení	zobrazení	k1gNnSc4	zobrazení
vodorovných	vodorovný	k2eAgFnPc2d1	vodorovná
linek	linka	k1gFnPc2	linka
na	na	k7c4	na
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mohl	moct	k5eAaImAgInS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
nespuštění	nespuštění	k1gNnSc4	nespuštění
blesku	blesk	k1gInSc2	blesk
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
iPhone	iPhon	k1gInSc5	iPhon
5	[number]	k4	5
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
touch	touch	k1gMnSc1	touch
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Šifrovaných	šifrovaný	k2eAgInPc2d1	šifrovaný
WPA	WPA	kA	WPA
2	[number]	k4	2
<g/>
,	,	kIx,	,
Wi-Fi	Wi-F	k1gFnPc1	Wi-F
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Řešení	řešení	k1gNnSc1	řešení
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
bránil	bránit	k5eAaImAgMnS	bránit
iPhone	iPhon	k1gInSc5	iPhon
v	v	k7c6	v
použití	použití	k1gNnSc3	použití
mobilní	mobilní	k2eAgFnSc2d1	mobilní
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Konsolidace	konsolidace	k1gFnSc1	konsolidace
přepínače	přepínač	k1gInSc2	přepínač
použití	použití	k1gNnSc2	použití
mobilních	mobilní	k2eAgNnPc2d1	mobilní
dat	datum	k1gNnPc2	datum
pro	pro	k7c4	pro
službu	služba	k1gFnSc4	služba
iTunes	iTunes	k1gMnSc1	iTunes
Match	Match	k1gMnSc1	Match
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
chyby	chyba	k1gFnSc2	chyba
v	v	k7c6	v
kódovém	kódový	k2eAgInSc6d1	kódový
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
podrobnostem	podrobnost	k1gFnPc3	podrobnost
v	v	k7c4	v
Passbook	Passbook	k1gInSc4	Passbook
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
chyby	chyba	k1gFnSc2	chyba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postihovala	postihovat	k5eAaImAgFnS	postihovat
schůzky	schůzka	k1gFnPc4	schůzka
v	v	k7c6	v
Exchange	Exchange	k1gNnSc6	Exchange
<g/>
.	.	kIx.	.
6.1	[number]	k4	6.1
-	-	kIx~	-
Update	update	k1gInSc1	update
přináší	přinášet	k5eAaImIp3nS	přinášet
podporu	podpora	k1gFnSc4	podpora
LTE	LTE	kA	LTE
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
36	[number]	k4	36
světových	světový	k2eAgInPc2d1	světový
operátorů	operátor	k1gInPc2	operátor
u	u	k7c2	u
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
23	[number]	k4	23
operátorů	operátor	k1gInPc2	operátor
pro	pro	k7c4	pro
iPad	iPad	k1gInSc4	iPad
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgNnPc7d1	další
vylepšeními	vylepšení	k1gNnPc7	vylepšení
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
úprava	úprava	k1gFnSc1	úprava
zobrazení	zobrazení	k1gNnSc2	zobrazení
ovládání	ovládání	k1gNnSc2	ovládání
puštěné	puštěný	k2eAgFnSc2d1	puštěná
hudby	hudba	k1gFnSc2	hudba
na	na	k7c6	na
zamčené	zamčený	k2eAgFnSc6d1	zamčená
obrazovce	obrazovka	k1gFnSc6	obrazovka
<g/>
,	,	kIx,	,
iTunes	iTunes	k1gMnSc1	iTunes
Match	Match	k1gMnSc1	Match
opět	opět	k6eAd1	opět
nabízí	nabízet	k5eAaImIp3nS	nabízet
stažení	stažení	k1gNnSc4	stažení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
6.1	[number]	k4	6.1
<g/>
.1	.1	k4	.1
-	-	kIx~	-
Verze	verze	k1gFnSc1	verze
určená	určený	k2eAgFnSc1d1	určená
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
opravující	opravující	k2eAgInPc4d1	opravující
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
do	do	k7c2	do
mobilní	mobilní	k2eAgFnSc2d1	mobilní
<g />
.	.	kIx.	.
</s>
<s>
sítě	síť	k1gFnPc1	síť
<g/>
.	.	kIx.	.
6.1	[number]	k4	6.1
<g/>
.2	.2	k4	.2
6.1	[number]	k4	6.1
<g/>
.3	.3	k4	.3
6.1	[number]	k4	6.1
<g/>
.4	.4	k4	.4
-	-	kIx~	-
vylepšen	vylepšen	k2eAgInSc1d1	vylepšen
hlasitý	hlasitý	k2eAgInSc1d1	hlasitý
odposlech	odposlech	k1gInSc1	odposlech
7.0	[number]	k4	7.0
<g/>
.0	.0	k4	.0
-	-	kIx~	-
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
iPhony	iPhon	k1gMnPc7	iPhon
v	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pro	pro	k7c4	pro
iPhony	iPhona	k1gFnPc4	iPhona
4	[number]	k4	4
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
5	[number]	k4	5
+	+	kIx~	+
5S	[number]	k4	5S
a	a	k8xC	a
5C	[number]	k4	5C
již	již	k6eAd1	již
mají	mít	k5eAaImIp3nP	mít
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
předinstalovaný	předinstalovaný	k2eAgMnSc1d1	předinstalovaný
<g/>
.	.	kIx.	.
</s>
<s>
Přináší	přinášet	k5eAaImIp3nS	přinášet
kompletně	kompletně	k6eAd1	kompletně
nový	nový	k2eAgInSc4d1	nový
vzhled	vzhled	k1gInSc4	vzhled
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc4	funkce
AirPlay	AirPlaa	k1gFnSc2	AirPlaa
<g/>
.	.	kIx.	.
7.0	[number]	k4	7.0
<g/>
.1	.1	k4	.1
-	-	kIx~	-
oprava	oprava	k1gFnSc1	oprava
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
7.0	[number]	k4	7.0
<g/>
.2	.2	k4	.2
7.0	[number]	k4	7.0
<g/>
.3	.3	k4	.3
7.0	[number]	k4	7.0
<g/>
.4	.4	k4	.4
-	-	kIx~	-
Oprava	oprava	k1gFnSc1	oprava
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
oprava	oprava	k1gFnSc1	oprava
samovolného	samovolný	k2eAgNnSc2d1	samovolné
zavěšování	zavěšování	k1gNnSc2	zavěšování
hovoru	hovor	k1gInSc2	hovor
v	v	k7c6	v
aplikaci	aplikace	k1gFnSc6	aplikace
Facetime	Facetim	k1gInSc5	Facetim
<g/>
.	.	kIx.	.
7.0	[number]	k4	7.0
<g/>
.5	.5	k4	.5
-	-	kIx~	-
Aktualizace	aktualizace	k1gFnSc1	aktualizace
je	být	k5eAaImIp3nS	být
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
5S	[number]	k4	5S
a	a	k8xC	a
5C	[number]	k4	5C
a	a	k8xC	a
upravuje	upravovat	k5eAaImIp3nS	upravovat
drobné	drobný	k2eAgFnPc4d1	drobná
chyby	chyba	k1gFnPc4	chyba
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
mobilními	mobilní	k2eAgFnPc7d1	mobilní
sítěmi	síť	k1gFnPc7	síť
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
7.0	[number]	k4	7.0
<g/>
.6	.6	k4	.6
-	-	kIx~	-
Aktualizace	aktualizace	k1gFnSc1	aktualizace
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
přináší	přinášet	k5eAaImIp3nS	přinášet
opravu	oprava	k1gFnSc4	oprava
ověřování	ověřování	k1gNnSc1	ověřování
připojení	připojení	k1gNnSc2	připojení
přes	přes	k7c4	přes
SSL	SSL	kA	SSL
<g/>
.	.	kIx.	.
</s>
<s>
Opravuje	opravovat	k5eAaImIp3nS	opravovat
chybu	chyba	k1gFnSc4	chyba
v	v	k7c6	v
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
všech	všecek	k3xTgInPc2	všecek
Apple	Apple	kA	Apple
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
7.1	[number]	k4	7.1
-	-	kIx~	-
velké	velký	k2eAgNnSc1d1	velké
zrychlení	zrychlení	k1gNnSc1	zrychlení
systému	systém	k1gInSc2	systém
u	u	k7c2	u
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
,	,	kIx,	,
přidán	přidán	k2eAgInSc1d1	přidán
mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
Siri	Sir	k1gFnSc2	Sir
<g/>
,	,	kIx,	,
opravení	opravení	k1gNnSc6	opravení
pádů	pád	k1gInPc2	pád
grafického	grafický	k2eAgNnSc2d1	grafické
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
softwarové	softwarový	k2eAgFnSc2d1	softwarová
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
.	.	kIx.	.
8.0	[number]	k4	8.0
-	-	kIx~	-
Systém	systém	k1gInSc1	systém
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
v	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
iPhony	iPhon	k1gMnPc7	iPhon
6	[number]	k4	6
a	a	k8xC	a
6	[number]	k4	6
Plus	plus	k1gNnPc2	plus
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
funkci	funkce	k1gFnSc4	funkce
Health	Healtha	k1gFnPc2	Healtha
<g/>
.	.	kIx.	.
</s>
