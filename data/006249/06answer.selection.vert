<s>
Územím	území	k1gNnSc7	území
čtvrtě	čtvrt	k1gFnSc2	čtvrt
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nedaleko	nedaleko	k7c2	nedaleko
areálu	areál	k1gInSc2	areál
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
výstaviště	výstaviště	k1gNnSc2	výstaviště
nachází	nacházet	k5eAaImIp3nS	nacházet
zalesněný	zalesněný	k2eAgInSc1d1	zalesněný
svah	svah	k1gInSc1	svah
s	s	k7c7	s
cyklistickou	cyklistický	k2eAgFnSc7d1	cyklistická
stezkou	stezka	k1gFnSc7	stezka
procházející	procházející	k2eAgFnSc7d1	procházející
kolem	kolem	k7c2	kolem
zdejšího	zdejší	k2eAgInSc2d1	zdejší
proslulého	proslulý	k2eAgInSc2d1	proslulý
pavilónu	pavilón	k1gInSc2	pavilón
Anthropos	Anthroposa	k1gFnPc2	Anthroposa
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
