<s>
Quicksort	Quicksort	k1gInSc1	Quicksort
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
rychlé	rychlý	k2eAgNnSc1d1	rychlé
řazení	řazení	k1gNnSc1	řazení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejrychlejších	rychlý	k2eAgInPc2d3	nejrychlejší
běžných	běžný	k2eAgInPc2d1	běžný
algoritmů	algoritmus	k1gInPc2	algoritmus
řazení	řazení	k1gNnSc2	řazení
založených	založený	k2eAgInPc2d1	založený
na	na	k7c4	na
porovnávání	porovnávání	k1gNnSc4	porovnávání
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
průměrná	průměrný	k2eAgFnSc1d1	průměrná
časová	časový	k2eAgFnSc1d1	časová
složitost	složitost	k1gFnSc1	složitost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
algoritmy	algoritmus	k1gInPc4	algoritmus
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
možná	možná	k9	možná
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
log	log	kA	log
N	N	kA	N
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
(	(	kIx(	(
<g/>
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
jde	jít	k5eAaImIp3nS	jít
obvykle	obvykle	k6eAd1	obvykle
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
časová	časový	k2eAgFnSc1d1	časová
náročnost	náročnost	k1gFnSc1	náročnost
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
algoritmu	algoritmus	k1gInSc2	algoritmus
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
jej	on	k3xPp3gMnSc4	on
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Antony	anton	k1gInPc7	anton
Richard	Richard	k1gMnSc1	Richard
Hoare	Hoar	k1gInSc5	Hoar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
quicksortu	quicksort	k1gInSc2	quicksort
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc4	rozdělení
řazené	řazený	k2eAgFnSc2d1	řazená
posloupnosti	posloupnost	k1gFnSc2	posloupnost
čísel	číslo	k1gNnPc2	číslo
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgFnSc6d1	stejná
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
quicksort	quicksort	k1gInSc1	quicksort
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
algoritmy	algoritmus	k1gInPc7	algoritmus
typu	typ	k1gInSc2	typ
rozděl	rozdělit	k5eAaPmRp2nS	rozdělit
a	a	k8xC	a
panuj	panovat	k5eAaImRp2nS	panovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
čísla	číslo	k1gNnPc4	číslo
větší	veliký	k2eAgNnPc4d2	veliký
a	a	k8xC	a
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
menší	malý	k2eAgFnSc3d2	menší
<g/>
,	,	kIx,	,
než	než	k8xS	než
nějaká	nějaký	k3yIgFnSc1	nějaký
zvolená	zvolený	k2eAgFnSc1d1	zvolená
hodnota	hodnota	k1gFnSc1	hodnota
(	(	kIx(	(
<g/>
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
pivot	pivot	k1gInSc1	pivot
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
střed	střed	k1gInSc1	střed
otáčení	otáčení	k1gNnSc2	otáčení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
zvolena	zvolit	k5eAaPmNgFnS	zvolit
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
budou	být	k5eAaImBp3nP	být
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
samostatně	samostatně	k6eAd1	samostatně
seřazeny	seřadit	k5eAaPmNgFnP	seřadit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
seřazené	seřazený	k2eAgInPc4d1	seřazený
i	i	k8xC	i
celé	celý	k2eAgNnSc4d1	celé
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rekurzivně	rekurzivně	k6eAd1	rekurzivně
řadí	řadit	k5eAaImIp3nS	řadit
stejným	stejný	k2eAgInSc7d1	stejný
postupem	postup	k1gInSc7	postup
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k9	ale
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
implementace	implementace	k1gFnSc1	implementace
musí	muset	k5eAaImIp3nS	muset
taky	taky	k6eAd1	taky
použít	použít	k5eAaPmF	použít
rekurzi	rekurze	k1gFnSc4	rekurze
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
celého	celý	k2eAgInSc2d1	celý
algoritmu	algoritmus	k1gInSc2	algoritmus
je	být	k5eAaImIp3nS	být
volba	volba	k1gFnSc1	volba
pivotu	pivot	k1gInSc2	pivot
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
volit	volit	k5eAaImF	volit
číslo	číslo	k1gNnSc1	číslo
blízké	blízký	k2eAgNnSc1d1	blízké
mediánu	mediána	k1gFnSc4	mediána
řazené	řazený	k2eAgFnSc3d1	řazená
části	část	k1gFnSc3	část
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
algoritmus	algoritmus	k1gInSc1	algoritmus
skutečně	skutečně	k6eAd1	skutečně
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
běhu	běh	k1gInSc2	běh
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
a	a	k8xC	a
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
časová	časový	k2eAgFnSc1d1	časová
složitost	složitost	k1gFnSc1	složitost
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přirozenou	přirozený	k2eAgFnSc7d1	přirozená
metodou	metoda	k1gFnSc7	metoda
na	na	k7c6	na
získání	získání	k1gNnSc6	získání
pivotu	pivot	k1gInSc2	pivot
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jeví	jevit	k5eAaImIp3nS	jevit
volit	volit	k5eAaImF	volit
za	za	k7c4	za
pivot	pivot	k1gInSc4	pivot
medián	mediána	k1gFnPc2	mediána
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
mediánu	medián	k1gInSc2	medián
(	(	kIx(	(
<g/>
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
k-tého	kého	k2eAgInSc2d1	k-tého
prvku	prvek	k1gInSc2	prvek
<g/>
)	)	kIx)	)
v	v	k7c6	v
posloupnosti	posloupnost	k1gFnSc6	posloupnost
běží	běžet	k5eAaImIp3nS	běžet
v	v	k7c6	v
lineárním	lineární	k2eAgInSc6d1	lineární
čase	čas	k1gInSc6	čas
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
počtu	počet	k1gInSc3	počet
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
dostaneme	dostat	k5eAaPmIp1nP	dostat
složitost	složitost	k1gFnSc4	složitost
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
log	log	kA	log
N	N	kA	N
<g/>
)	)	kIx)	)
quicksortu	quicksort	k1gInSc2	quicksort
v	v	k7c6	v
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
implementace	implementace	k1gFnSc1	implementace
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
rychlá	rychlý	k2eAgFnSc1d1	rychlá
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vysokých	vysoký	k2eAgFnPc2d1	vysoká
konstant	konstanta	k1gFnPc2	konstanta
schovaných	schovaný	k2eAgFnPc2d1	schovaná
v	v	k7c4	v
O	o	k7c4	o
notaci	notace	k1gFnSc4	notace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
alternativních	alternativní	k2eAgInPc2d1	alternativní
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
efektivně	efektivně	k6eAd1	efektivně
vybrat	vybrat	k5eAaPmF	vybrat
pivot	pivot	k1gInSc4	pivot
co	co	k8xS	co
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
mediánu	mediána	k1gFnSc4	mediána
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc4	seznam
některých	některý	k3yIgFnPc2	některý
metod	metoda	k1gFnPc2	metoda
<g/>
:	:	kIx,	:
První	první	k4xOgInSc1	první
prvek	prvek	k1gInSc1	prvek
-	-	kIx~	-
popřípadě	popřípadě	k6eAd1	popřípadě
kterákoli	kterýkoli	k3yIgFnSc1	kterýkoli
jiná	jiný	k2eAgFnSc1d1	jiná
fixní	fixní	k2eAgFnSc1d1	fixní
pozice	pozice	k1gFnSc1	pozice
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Fixní	fixní	k2eAgFnSc1d1	fixní
volba	volba	k1gFnSc1	volba
prvního	první	k4xOgInSc2	první
prvku	prvek	k1gInSc2	prvek
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nevýhodná	výhodný	k2eNgFnSc1d1	nevýhodná
na	na	k7c6	na
částečně	částečně	k6eAd1	částečně
seřazených	seřazený	k2eAgFnPc6d1	seřazená
množinách	množina	k1gFnPc6	množina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Náhodný	náhodný	k2eAgInSc4d1	náhodný
prvek	prvek	k1gInSc4	prvek
-	-	kIx~	-
často	často	k6eAd1	často
používaná	používaný	k2eAgFnSc1d1	používaná
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
přes	přes	k7c4	přes
každá	každý	k3xTgNnPc4	každý
data	datum	k1gNnPc4	datum
je	být	k5eAaImIp3nS	být
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
log	log	kA	log
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
průměr	průměr	k1gInSc1	průměr
bere	brát	k5eAaImIp3nS	brát
přes	přes	k7c4	přes
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
volby	volba	k1gFnPc4	volba
pivotů	pivot	k1gInPc2	pivot
(	(	kIx(	(
<g/>
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgInSc1d3	Nejhorší
případ	případ	k1gInSc1	případ
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
každá	každý	k3xTgNnPc4	každý
data	datum	k1gNnPc4	datum
může	moct	k5eAaImIp3nS	moct
náhoda	náhoda	k1gFnSc1	náhoda
nebo	nebo	k8xC	nebo
Velmi	velmi	k6eAd1	velmi
Inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
Protivník	protivník	k1gMnSc1	protivník
vybírat	vybírat	k5eAaImF	vybírat
soustavně	soustavně	k6eAd1	soustavně
nevhodného	vhodný	k2eNgMnSc4d1	nevhodný
pivota	pivot	k1gMnSc4	pivot
<g/>
,	,	kIx,	,
např.	např.	kA	např.
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
většinou	většina	k1gFnSc7	většina
není	být	k5eNaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
generátor	generátor	k1gInSc1	generátor
skutečně	skutečně	k6eAd1	skutečně
náhodných	náhodný	k2eAgNnPc2d1	náhodné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pseudonáhodný	pseudonáhodný	k2eAgInSc1d1	pseudonáhodný
výběr	výběr	k1gInSc1	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
mediánu	medián	k1gInSc2	medián
tří	tři	k4xCgFnPc2	tři
-	-	kIx~	-
případně	případně	k6eAd1	případně
pěti	pět	k4xCc2	pět
či	či	k9wB	či
jiného	jiný	k2eAgInSc2d1	jiný
počtu	počet	k1gInSc2	počet
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
pseudonáhodného	pseudonáhodný	k2eAgInSc2d1	pseudonáhodný
algoritmu	algoritmus	k1gInSc2	algoritmus
(	(	kIx(	(
<g/>
také	také	k9	také
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
fixní	fixní	k2eAgFnPc4d1	fixní
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
prostřední	prostřední	k2eAgFnSc1d1	prostřední
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
několik	několik	k4yIc4	několik
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
medián	medián	k1gInSc1	medián
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k9	jako
pivot	pivot	k1gInSc1	pivot
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pivota	pivot	k1gMnSc4	pivot
volíme	volit	k5eAaImIp1nP	volit
vždy	vždy	k6eAd1	vždy
z	z	k7c2	z
98	[number]	k4	98
%	%	kIx~	%
prvků	prvek	k1gInPc2	prvek
uprostřed	uprostřed	k6eAd1	uprostřed
a	a	k8xC	a
ne	ne	k9	ne
z	z	k7c2	z
1	[number]	k4	1
%	%	kIx~	%
na	na	k7c6	na
některé	některý	k3yIgFnSc6	některý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
algoritmus	algoritmus	k1gInSc4	algoritmus
by	by	kYmCp3nS	by
stále	stále	k6eAd1	stále
měl	mít	k5eAaImAgInS	mít
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
asymptotickou	asymptotický	k2eAgFnSc4d1	asymptotická
složitost	složitost	k1gFnSc4	složitost
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
log	log	kA	log
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
větší	veliký	k2eAgFnSc7d2	veliký
konstantou	konstanta	k1gFnSc7	konstanta
v	v	k7c6	v
O-notaci	Ootace	k1gFnSc6	O-notace
<g/>
.	.	kIx.	.
</s>
<s>
Praktické	praktický	k2eAgFnPc1d1	praktická
zkušenosti	zkušenost	k1gFnPc1	zkušenost
a	a	k8xC	a
testy	test	k1gInPc1	test
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
pseudonáhodných	pseudonáhodný	k2eAgFnPc6d1	pseudonáhodná
nebo	nebo	k8xC	nebo
reálných	reálný	k2eAgNnPc6d1	reálné
datech	datum	k1gNnPc6	datum
je	být	k5eAaImIp3nS	být
Quicksort	Quicksort	k1gInSc4	Quicksort
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
obecných	obecný	k2eAgInPc2d1	obecný
řadicích	řadicí	k2eAgInPc2d1	řadicí
algoritmů	algoritmus	k1gInPc2	algoritmus
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
Heapsort	Heapsort	k1gInSc1	Heapsort
a	a	k8xC	a
Mergesort	Mergesort	k1gInSc1	Mergesort
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
formálně	formálně	k6eAd1	formálně
rychlejší	rychlý	k2eAgFnPc1d2	rychlejší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
Quicksortu	Quicksort	k1gInSc2	Quicksort
však	však	k9	však
není	být	k5eNaImIp3nS	být
zaručena	zaručen	k2eAgFnSc1d1	zaručena
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
vstupy	vstup	k1gInPc4	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
časová	časový	k2eAgFnSc1d1	časová
náročnost	náročnost	k1gFnSc1	náročnost
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Quicksort	Quicksort	k1gInSc4	Quicksort
diskvalifikují	diskvalifikovat	k5eAaBmIp3nP	diskvalifikovat
pro	pro	k7c4	pro
kritické	kritický	k2eAgFnPc4d1	kritická
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
algoritmus	algoritmus	k1gInSc1	algoritmus
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jiné	jiný	k2eAgInPc1d1	jiný
pečlivou	pečlivý	k2eAgFnSc4d1	pečlivá
implementaci	implementace	k1gFnSc4	implementace
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
uvedená	uvedený	k2eAgFnSc1d1	uvedená
základní	základní	k2eAgFnSc1d1	základní
rekurzivní	rekurzivní	k2eAgFnSc1d1	rekurzivní
verze	verze	k1gFnSc1	verze
algoritmu	algoritmus	k1gInSc2	algoritmus
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
nasazení	nasazení	k1gNnSc4	nasazení
v	v	k7c4	v
praxi	praxe	k1gFnSc4	praxe
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
<g/>
,	,	kIx,	,
výše	vysoce	k6eAd2	vysoce
uvedený	uvedený	k2eAgInSc1d1	uvedený
algoritmus	algoritmus	k1gInSc1	algoritmus
pouze	pouze	k6eAd1	pouze
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
funkční	funkční	k2eAgInSc1d1	funkční
princip	princip	k1gInSc1	princip
quicksortu	quicksort	k1gInSc2	quicksort
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rekurzivní	rekurzivní	k2eAgInSc4d1	rekurzivní
algoritmus	algoritmus	k1gInSc4	algoritmus
a	a	k8xC	a
každé	každý	k3xTgNnSc1	každý
rekurzivní	rekurzivní	k2eAgNnSc1d1	rekurzivní
volání	volání	k1gNnSc1	volání
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
paměť	paměť	k1gFnSc4	paměť
zásobníku	zásobník	k1gInSc2	zásobník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
zmiňovaném	zmiňovaný	k2eAgInSc6d1	zmiňovaný
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
(	(	kIx(	(
<g/>
opakovaná	opakovaný	k2eAgFnSc1d1	opakovaná
volba	volba	k1gFnSc1	volba
nejmenšího	malý	k2eAgInSc2d3	nejmenší
prvku	prvek	k1gInSc2	prvek
za	za	k7c2	za
pivota	pivot	k1gMnSc2	pivot
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
spotřeba	spotřeba	k1gFnSc1	spotřeba
paměti	paměť	k1gFnSc2	paměť
rostla	růst	k5eAaImAgFnS	růst
lineárně	lineárně	k6eAd1	lineárně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dokonce	dokonce	k9	dokonce
způsobit	způsobit	k5eAaPmF	způsobit
pád	pád	k1gInSc4	pád
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
s	s	k7c7	s
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
rekurzí	rekurze	k1gFnSc7	rekurze
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nS	muset
při	při	k7c6	při
ladění	ladění	k1gNnSc6	ladění
projevit	projevit	k5eAaPmF	projevit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hloubka	hloubka	k1gFnSc1	hloubka
rekurze	rekurze	k1gFnSc2	rekurze
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
řazených	řazený	k2eAgNnPc6d1	řazené
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zabránit	zabránit	k5eAaPmF	zabránit
hluboké	hluboký	k2eAgFnSc3d1	hluboká
rekurzi	rekurze	k1gFnSc3	rekurze
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pivotizaci	pivotizace	k1gFnSc6	pivotizace
lze	lze	k6eAd1	lze
nejdříve	dříve	k6eAd3	dříve
řadit	řadit	k5eAaImF	řadit
menší	malý	k2eAgFnSc4d2	menší
část	část	k1gFnSc4	část
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
rekurze	rekurze	k1gFnSc2	rekurze
poté	poté	k6eAd1	poté
řadit	řadit	k5eAaImF	řadit
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
sekvence	sekvence	k1gFnSc2	sekvence
již	již	k6eAd1	již
nerekurzivně	rekurzivně	k6eNd1	rekurzivně
Tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hloubka	hloubka	k1gFnSc1	hloubka
rekurze	rekurze	k1gFnSc2	rekurze
neroste	růst	k5eNaImIp3nS	růst
nikdy	nikdy	k6eAd1	nikdy
lineárně	lineárně	k6eAd1	lineárně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvýše	nejvýše	k6eAd1	nejvýše
log	log	kA	log
<g/>
2	[number]	k4	2
N.	N.	kA	N.
Implementace	implementace	k1gFnSc2	implementace
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kvůli	kvůli	k7c3	kvůli
rychlosti	rychlost	k1gFnSc3	rychlost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
použití	použití	k1gNnSc4	použití
rekurze	rekurze	k1gFnSc2	rekurze
a	a	k8xC	a
ošetřuje	ošetřovat	k5eAaImIp3nS	ošetřovat
si	se	k3xPyFc3	se
zásobník	zásobník	k1gInSc1	zásobník
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
standard	standard	k1gInSc1	standard
MISRA	MISRA	kA	MISRA
C	C	kA	C
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
vestavěné	vestavěný	k2eAgInPc4d1	vestavěný
systémy	systém	k1gInPc4	systém
použití	použití	k1gNnSc2	použití
rekurze	rekurze	k1gFnSc2	rekurze
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zásobník	zásobník	k1gInSc4	zásobník
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
hranice	hranice	k1gFnPc1	hranice
ještě	ještě	k9	ještě
nezpracovaných	zpracovaný	k2eNgFnPc2d1	nezpracovaná
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
trik	trik	k1gInSc1	trik
pro	pro	k7c4	pro
zmenšení	zmenšení	k1gNnSc4	zmenšení
zásobníku	zásobník	k1gInSc2	zásobník
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
zásobník	zásobník	k1gInSc4	zásobník
uložit	uložit	k5eAaPmF	uložit
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
)	)	kIx)	)
tu	tu	k6eAd1	tu
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
vždy	vždy	k6eAd1	vždy
menší	malý	k2eAgFnSc4d2	menší
z	z	k7c2	z
rozdělených	rozdělená	k1gFnPc2	rozdělená
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zaručí	zaručit	k5eAaPmIp3nS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stačí	stačit	k5eAaBmIp3nS	stačit
zásobník	zásobník	k1gInSc1	zásobník
velikosti	velikost	k1gFnSc2	velikost
log	log	kA	log
<g/>
2	[number]	k4	2
N	N	kA	N
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
například	například	k6eAd1	například
pro	pro	k7c4	pro
miliardu	miliarda	k4xCgFnSc4	miliarda
prvků	prvek	k1gInPc2	prvek
stačí	stačit	k5eAaBmIp3nS	stačit
hloubka	hloubka	k1gFnSc1	hloubka
30	[number]	k4	30
položek	položka	k1gFnPc2	položka
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
této	tento	k3xDgFnSc2	tento
optimalizace	optimalizace	k1gFnSc2	optimalizace
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
paměť	paměť	k1gFnSc1	paměť
potřebná	potřebný	k2eAgFnSc1d1	potřebná
pro	pro	k7c4	pro
zásobník	zásobník	k1gInSc4	zásobník
až	až	k6eAd1	až
lineární	lineární	k2eAgInSc1d1	lineární
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
postup	postup	k1gInSc4	postup
ekvivalentní	ekvivalentní	k2eAgNnSc4d1	ekvivalentní
zabránění	zabránění	k1gNnSc4	zabránění
hluboké	hluboký	k2eAgFnSc2d1	hluboká
rekurze	rekurze	k1gFnSc2	rekurze
<g/>
.	.	kIx.	.
rychlost	rychlost	k1gFnSc1	rychlost
výpočtu	výpočet	k1gInSc2	výpočet
je	být	k5eAaImIp3nS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
log	log	kA	log
N	N	kA	N
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
při	při	k7c6	při
nejhorším	zlý	k2eAgInSc6d3	Nejhorší
případě	případ	k1gInSc6	případ
časová	časový	k2eAgFnSc1d1	časová
náročnost	náročnost	k1gFnSc1	náročnost
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
O	O	kA	O
<g/>
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
při	při	k7c6	při
správné	správný	k2eAgFnSc6d1	správná
implementaci	implementace	k1gFnSc6	implementace
prakticky	prakticky	k6eAd1	prakticky
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
paměť	paměť	k1gFnSc4	paměť
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
řadí	řadit	k5eAaImIp3nS	řadit
prvky	prvek	k1gInPc4	prvek
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nestabilní	stabilní	k2eNgInSc4d1	nestabilní
algoritmus	algoritmus	k1gInSc4	algoritmus
<g/>
,	,	kIx,	,
způsob	způsob	k1gInSc4	způsob
volby	volba	k1gFnSc2	volba
pivota	pivot	k1gMnSc2	pivot
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výsledek	výsledek	k1gInSc4	výsledek
řazení	řazení	k1gNnSc2	řazení
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nejhoršímu	zlý	k2eAgInSc3d3	Nejhorší
případu	případ	k1gInSc3	případ
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
vhodný	vhodný	k2eAgInSc4d1	vhodný
pro	pro	k7c4	pro
vestavěné	vestavěný	k2eAgInPc4d1	vestavěný
systémy	systém	k1gInPc4	systém
běžící	běžící	k2eAgInSc4d1	běžící
na	na	k7c6	na
slabším	slabý	k2eAgInSc6d2	slabší
hardware	hardware	k1gInSc1	hardware
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
známý	známý	k2eAgInSc4d1	známý
univerzální	univerzální	k2eAgInSc4d1	univerzální
algoritmus	algoritmus	k1gInSc4	algoritmus
pro	pro	k7c4	pro
řazení	řazení	k1gNnSc4	řazení
polí	pole	k1gFnPc2	pole
v	v	k7c6	v
operační	operační	k2eAgFnSc6d1	operační
paměti	paměť	k1gFnSc6	paměť
<g />
.	.	kIx.	.
</s>
<s>
počítače	počítač	k1gInPc4	počítač
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
nejhoršího	zlý	k2eAgInSc2d3	Nejhorší
případu	případ	k1gInSc2	případ
slouží	sloužit	k5eAaImIp3nP	sloužit
různé	různý	k2eAgInPc1d1	různý
postupy	postup	k1gInPc1	postup
volby	volba	k1gFnSc2	volba
pivota	pivot	k1gMnSc2	pivot
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rekurzivní	rekurzivní	k2eAgInSc4d1	rekurzivní
algoritmus	algoritmus	k1gInSc4	algoritmus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hloubku	hloubka	k1gFnSc4	hloubka
rekurze	rekurze	k1gFnSc2	rekurze
lze	lze	k6eAd1	lze
omezit	omezit	k5eAaPmF	omezit
<g/>
;	;	kIx,	;
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
zásobníku	zásobník	k1gInSc2	zásobník
jej	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
algoritmus	algoritmus	k1gInSc1	algoritmus
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
nerekurzivní	rekurzivní	k2eNgFnPc4d1	nerekurzivní
nerekurzivní	rekurzivní	k2eNgFnPc4d1	nerekurzivní
verze	verze	k1gFnPc4	verze
algoritmu	algoritmus	k1gInSc2	algoritmus
je	být	k5eAaImIp3nS	být
efektivnější	efektivní	k2eAgFnSc1d2	efektivnější
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
méně	málo	k6eAd2	málo
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
implementován	implementován	k2eAgMnSc1d1	implementován
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
systémových	systémový	k2eAgFnPc6d1	systémová
knihovnách	knihovna	k1gFnPc6	knihovna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
C	C	kA	C
funkce	funkce	k1gFnSc1	funkce
qsort	qsort	k1gInSc1	qsort
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
stdlib	stdlib	k1gInSc1	stdlib
<g/>
.	.	kIx.	.
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
Vícerozměrný	vícerozměrný	k2eAgInSc1d1	vícerozměrný
quicksort	quicksort	k1gInSc1	quicksort
v	v	k7c6	v
Javě	Jav	k1gInSc6	Jav
Tutorial	Tutorial	k1gInSc1	Tutorial
Quicksortu	Quicksort	k1gInSc2	Quicksort
s	s	k7c7	s
průběhem	průběh	k1gInSc7	průběh
<g/>
,	,	kIx,	,
diagramy	diagram	k1gInPc7	diagram
a	a	k8xC	a
zdrojovými	zdrojový	k2eAgInPc7d1	zdrojový
kódy	kód	k1gInPc7	kód
v	v	k7c6	v
Javě	Java	k1gFnSc6	Java
(	(	kIx(	(
<g/>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
C	C	kA	C
Library	Librara	k1gFnSc2	Librara
reference	reference	k1gFnSc2	reference
guide	guide	k6eAd1	guide
(	(	kIx(	(
<g/>
by	by	kYmCp3nS	by
Eric	Eric	k1gInSc1	Eric
Huss	Huss	k1gInSc4	Huss
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Wirth	Wirth	k1gInSc1	Wirth
<g/>
,	,	kIx,	,
N.	N.	kA	N.
<g/>
:	:	kIx,	:
Algoritmy	algoritmus	k1gInPc1	algoritmus
a	a	k8xC	a
štruktúry	štruktúra	k1gFnPc1	štruktúra
údajov	údajov	k1gInSc1	údajov
<g/>
,	,	kIx,	,
Alfa	alfa	k1gFnSc1	alfa
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
