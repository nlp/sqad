<s>
Disjunkce	disjunkce	k1gFnSc1	disjunkce
znamená	znamenat	k5eAaImIp3nS	znamenat
odloučení	odloučení	k1gNnSc4	odloučení
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
odloučené	odloučený	k2eAgFnPc4d1	odloučená
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
sloučení	sloučení	k1gNnSc4	sloučení
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
logický	logický	k2eAgInSc1d1	logický
součet	součet	k1gInSc1	součet
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
množinových	množinový	k2eAgInPc2d1	množinový
prvků	prvek	k1gInPc2	prvek
zařazených	zařazený	k2eAgInPc2d1	zařazený
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
skupiny	skupina	k1gFnSc2	skupina
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
překrývat	překrývat	k5eAaImF	překrývat
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
ve	v	k7c6	v
fytogeografii	fytogeografie	k1gFnSc6	fytogeografie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
genetice	genetika	k1gFnSc6	genetika
-	-	kIx~	-
disjunkce	disjunkce	k1gFnSc1	disjunkce
chromosomů	chromosom	k1gInPc2	chromosom
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
disjunkcí	disjunkce	k1gFnSc7	disjunkce
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
také	také	k9	také
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
u	u	k7c2	u
logických	logický	k2eAgInPc2d1	logický
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
hradel	hradlo	k1gNnPc2	hradlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgNnSc1d3	nejčastější
použití	použití	k1gNnSc1	použití
denotátu	denotát	k1gInSc2	denotát
disjunkce	disjunkce	k1gFnSc2	disjunkce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
<g/>
,	,	kIx,	,
v	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
logice	logika	k1gFnSc6	logika
<g/>
.	.	kIx.	.
</s>
<s>
Disjunkce	disjunkce	k1gFnSc1	disjunkce
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
znamená	znamenat	k5eAaImIp3nS	znamenat
logický	logický	k2eAgInSc4d1	logický
součet	součet	k1gInSc4	součet
<g/>
.	.	kIx.	.
</s>
<s>
Výroky	výrok	k1gInPc1	výrok
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
symbolem	symbol	k1gInSc7	symbol
OR	OR	kA	OR
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∨	∨	k?	∨
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
vee	vee	k?	vee
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
binárním	binární	k2eAgInSc7d1	binární
operátorem	operátor	k1gInSc7	operátor
<g/>
,	,	kIx,	,
používaným	používaný	k2eAgInSc7d1	používaný
pro	pro	k7c4	pro
binární	binární	k2eAgFnPc4d1	binární
logické	logický	k2eAgFnPc4d1	logická
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výrokové	výrokový	k2eAgFnSc6d1	výroková
logice	logika	k1gFnSc6	logika
může	moct	k5eAaImIp3nS	moct
nabýt	nabýt	k5eAaPmF	nabýt
logický	logický	k2eAgInSc1d1	logický
součet	součet	k1gInSc1	součet
dvou	dva	k4xCgInPc2	dva
výroků	výrok	k1gInPc2	výrok
pravdivostní	pravdivostní	k2eAgFnSc2d1	pravdivostní
hodnoty	hodnota	k1gFnSc2	hodnota
true	tru	k1gFnSc2	tru
<g/>
=	=	kIx~	=
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnSc6d1	označovaná
1	[number]	k4	1
<g/>
,	,	kIx,	,
když	když	k8xS	když
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
vstupních	vstupní	k2eAgInPc2d1	vstupní
výroků	výrok	k1gInPc2	výrok
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
false	false	k6eAd1	false
<g/>
=	=	kIx~	=
<g/>
"	"	kIx"	"
<g/>
nepravda	nepravda	k1gFnSc1	nepravda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
disjunkcí	disjunkce	k1gFnSc7	disjunkce
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
ve	v	k7c6	v
výrokové	výrokový	k2eAgFnSc6d1	výroková
logice	logika	k1gFnSc6	logika
<g/>
,	,	kIx,	,
predikátové	predikátový	k2eAgFnSc3d1	predikátová
logice	logika	k1gFnSc3	logika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verbální	verbální	k2eAgFnSc6d1	verbální
logice	logika	k1gFnSc6	logika
je	být	k5eAaImIp3nS	být
disjunkce	disjunkce	k1gFnSc1	disjunkce
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
nebo	nebo	k8xC	nebo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Vojta	Vojta	k1gMnSc1	Vojta
plave	plavat	k5eAaImIp3nS	plavat
nebo	nebo	k8xC	nebo
Lucka	Lucka	k1gFnSc1	Lucka
plave	plavat	k5eAaImIp3nS	plavat
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
disjunkce	disjunkce	k1gFnSc1	disjunkce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstupy	vstup	k1gInPc4	vstup
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
vypadá	vypadat	k5eAaImIp3nS	vypadat
pravdivostní	pravdivostní	k2eAgFnSc1d1	pravdivostní
tabulka	tabulka	k1gFnSc1	tabulka
disjunkce	disjunkce	k1gFnSc2	disjunkce
následovně	následovně	k6eAd1	následovně
(	(	kIx(	(
<g/>
0	[number]	k4	0
označuje	označovat	k5eAaImIp3nS	označovat
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
1	[number]	k4	1
označuje	označovat	k5eAaImIp3nS	označovat
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
řeči	řeč	k1gFnSc6	řeč
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
spojka	spojka	k1gFnSc1	spojka
"	"	kIx"	"
<g/>
nebo	nebo	k8xC	nebo
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
vylučovací	vylučovací	k2eAgMnPc1d1	vylučovací
"	"	kIx"	"
<g/>
buď	buď	k8xC	buď
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tomuto	tento	k3xDgInSc3	tento
vylučovacímu	vylučovací	k2eAgInSc3d1	vylučovací
případu	případ	k1gInSc3	případ
však	však	k9	však
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
exkluzivní	exkluzivní	k2eAgFnPc4d1	exkluzivní
disjunkce	disjunkce	k1gFnPc4	disjunkce
<g/>
,	,	kIx,	,
např	např	kA	např
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Venku	venku	k6eAd1	venku
je	být	k5eAaImIp3nS	být
mokro	mokro	k6eAd1	mokro
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
sucho	sucho	k1gNnSc1	sucho
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nemohou	moct	k5eNaImIp3nP	moct
tedy	tedy	k9	tedy
nastat	nastat	k5eAaPmF	nastat
oba	dva	k4xCgInPc4	dva
případy	případ	k1gInPc4	případ
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
spojka	spojka	k1gFnSc1	spojka
"	"	kIx"	"
<g/>
nebo	nebo	k8xC	nebo
<g/>
"	"	kIx"	"
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
znamenat	znamenat	k5eAaImF	znamenat
vyloučení	vyloučení	k1gNnSc4	vyloučení
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
nevyplývá	vyplývat	k5eNaImIp3nS	vyplývat
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
významu	význam	k1gInSc2	význam
slov	slovo	k1gNnPc2	slovo
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
komunikačním	komunikační	k2eAgNnPc3d1	komunikační
nedorozuměním	nedorozumění	k1gNnPc3	nedorozumění
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
komunikační	komunikační	k2eAgFnSc4d1	komunikační
sémantickou	sémantický	k2eAgFnSc4d1	sémantická
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyvarování	vyvarování	k1gNnSc3	vyvarování
se	se	k3xPyFc4	se
nesrovnalostí	nesrovnalost	k1gFnPc2	nesrovnalost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
právnické	právnický	k2eAgInPc4d1	právnický
výrazy	výraz	k1gInPc4	výraz
"	"	kIx"	"
<g/>
nebo	nebo	k8xC	nebo
<g/>
"	"	kIx"	"
bez	bez	k7c2	bez
čárky	čárka	k1gFnSc2	čárka
pro	pro	k7c4	pro
slučovací	slučovací	k2eAgInSc4d1	slučovací
význam	význam	k1gInSc4	význam
a	a	k8xC	a
s	s	k7c7	s
čárkou	čárka	k1gFnSc7	čárka
pro	pro	k7c4	pro
vylučovací	vylučovací	k2eAgMnPc4d1	vylučovací
<g/>
.	.	kIx.	.
</s>
<s>
Ostrá	ostrý	k2eAgFnSc1d1	ostrá
disjunkce	disjunkce	k1gFnSc1	disjunkce
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
symbolem	symbol	k1gInSc7	symbol
∨	∨	k?	∨
<g/>
,	,	kIx,	,
slovně	slovně	k6eAd1	slovně
nebo	nebo	k8xC	nebo
<g/>
.	.	kIx.	.
</s>
<s>
Výrok	výrok	k1gInSc1	výrok
je	být	k5eAaImIp3nS	být
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
výroků	výrok	k1gInPc2	výrok
pravdivý	pravdivý	k2eAgMnSc1d1	pravdivý
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
nepravdivý	pravdivý	k2eNgMnSc1d1	nepravdivý
<g/>
.	.	kIx.	.
</s>
<s>
Booleova	Booleův	k2eAgFnSc1d1	Booleova
algebra	algebra	k1gFnSc1	algebra
konjunkce	konjunkce	k1gFnSc2	konjunkce
existenční	existenční	k2eAgInSc1d1	existenční
kvantifikátor	kvantifikátor	k1gInSc1	kvantifikátor
logický	logický	k2eAgInSc1d1	logický
člen	člen	k1gInSc1	člen
OR	OR	kA	OR
Disjunktní	disjunktní	k2eAgFnSc2d1	disjunktní
množiny	množina	k1gFnSc2	množina
Výukový	výukový	k2eAgInSc1d1	výukový
kurs	kurs	k1gInSc1	kurs
Disjunkce	disjunkce	k1gFnSc2	disjunkce
<g/>
/	/	kIx~	/
<g/>
pro	pro	k7c4	pro
SŠ	SŠ	kA	SŠ
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
