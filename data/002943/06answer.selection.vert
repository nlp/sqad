<s>
Disjunkce	disjunkce	k1gFnSc1	disjunkce
znamená	znamenat	k5eAaImIp3nS	znamenat
odloučení	odloučení	k1gNnSc4	odloučení
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
odloučené	odloučený	k2eAgFnPc4d1	odloučená
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
sloučení	sloučení	k1gNnSc4	sloučení
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
logický	logický	k2eAgInSc1d1	logický
součet	součet	k1gInSc1	součet
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
množinových	množinový	k2eAgInPc2d1	množinový
prvků	prvek	k1gInPc2	prvek
zařazených	zařazený	k2eAgInPc2d1	zařazený
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
skupiny	skupina	k1gFnSc2	skupina
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
