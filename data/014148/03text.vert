<s>
Listopad	listopad	k1gInSc1
1989	#num#	k4
</s>
<s>
Listopad	listopad	k1gInSc4
1989	#num#	k4
Pád	Pád	k1gInSc1
berlínské	berlínský	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pozadí	pozadí	k1gNnSc6
je	být	k5eAaImIp3nS
Braniborská	braniborský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
Historické	historický	k2eAgNnSc1d1
období	období	k1gNnSc4
</s>
<s>
Pád	Pád	k1gInSc1
komunismu	komunismus	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
Místo	místo	k7c2
</s>
<s>
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
Východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Cíl	cíl	k1gInSc4
</s>
<s>
odstranění	odstranění	k1gNnSc1
komunistické	komunistický	k2eAgFnSc2d1
diktatury	diktatura	k1gFnSc2
</s>
<s>
Listopad	listopad	k1gInSc1
1989	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
myslích	mysl	k1gFnPc6
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
tehdy	tehdy	k6eAd1
existujících	existující	k2eAgMnPc2d1
států	stát	k1gInPc2
NDR	NDR	kA
a	a	k8xC
Československa	Československo	k1gNnSc2
zapsán	zapsán	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
období	období	k1gNnSc1
významných	významný	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
1989	#num#	k4
v	v	k7c6
podstatné	podstatný	k2eAgFnSc6d1
části	část	k1gFnSc6
střední	střední	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
vyvrcholil	vyvrcholit	k5eAaPmAgInS
proces	proces	k1gInSc1
pádu	pád	k1gInSc2
komunisticých	komunisticých	k2eAgInPc2d1
režimů	režim	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Události	událost	k1gFnPc1
</s>
<s>
Sametová	sametový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
na	na	k7c6
Václavském	václavský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Změny	změna	k1gFnPc1
totalitních	totalitní	k2eAgInPc2d1
režimů	režim	k1gInPc2
k	k	k7c3
počátkům	počátek	k1gInPc3
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
již	již	k6eAd1
proběhlé	proběhlý	k2eAgNnSc1d1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
a	a	k8xC
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
zasáhly	zasáhnout	k5eAaPmAgFnP
také	také	k9
tehdejší	tehdejší	k2eAgNnSc4d1
Československo	Československo	k1gNnSc4
a	a	k8xC
NDR	NDR	kA
(	(	kIx(
<g/>
východní	východní	k2eAgNnSc1d1
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
padla	padnout	k5eAaImAgFnS,k5eAaPmAgFnS
berlínská	berlínský	k2eAgFnSc1d1
zeď	zeď	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
známý	známý	k2eAgInSc1d1
symbol	symbol	k1gInSc1
komunistické	komunistický	k2eAgFnSc2d1
totality	totalita	k1gFnSc2
a	a	k8xC
nyní	nyní	k6eAd1
i	i	k9
jejího	její	k3xOp3gInSc2
pádu	pád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
byla	být	k5eAaImAgFnS
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c4
den	den	k1gInSc4
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc1
událostí	událost	k1gFnPc2
listopadu	listopad	k1gInSc2
1939	#num#	k4
<g/>
,	,	kIx,
policií	policie	k1gFnSc7
brutálně	brutálně	k6eAd1
zastavena	zastaven	k2eAgFnSc1d1
studentská	studentský	k2eAgFnSc1d1
demonstrace	demonstrace	k1gFnSc1
a	a	k8xC
tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgInS
započat	započat	k2eAgInSc4d1
proces	proces	k1gInSc4
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
sametová	sametový	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
přes	přes	k7c4
berlínskou	berlínský	k2eAgFnSc4d1
symboliku	symbolika	k1gFnSc4
a	a	k8xC
československou	československý	k2eAgFnSc4d1
„	„	k?
<g/>
sametovou	sametový	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
“	“	k?
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
listopad	listopad	k1gInSc4
1989	#num#	k4
lokální	lokální	k2eAgInSc4d1
pojem	pojem	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
Polsku	Polsko	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
politicko-společenským	politicko-společenský	k2eAgFnPc3d1
i	i	k8xC
ekonomickým	ekonomický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
už	už	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
1989	#num#	k4
a	a	k8xC
první	první	k4xOgFnSc2
svobodné	svobodný	k2eAgFnSc2d1
volby	volba	k1gFnSc2
tam	tam	k6eAd1
proběhly	proběhnout	k5eAaPmAgInP
(	(	kIx(
<g/>
byť	byť	k8xS
jen	jen	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
parlamentu	parlament	k1gInSc2
<g/>
)	)	kIx)
již	již	k6eAd1
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
už	už	k6eAd1
za	za	k7c2
vlády	vláda	k1gFnSc2
Tadeusze	Tadeusze	k1gFnSc2
Mazowieckého	Mazowiecký	k2eAgInSc2d1
rozbíhal	rozbíhat	k5eAaImAgMnS
Leszek	Leszek	k1gInSc4
Balcerowicz	Balcerowicz	k1gMnSc1
ekonomické	ekonomický	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
během	během	k7c2
několika	několik	k4yIc2
týdnů	týden	k1gInPc2
zbrzdily	zbrzdit	k5eAaPmAgInP
hyperinflaci	hyperinflace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1
změny	změna	k1gFnPc1
o	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
zasáhly	zasáhnout	k5eAaPmAgFnP
i	i	k9
Pobaltské	pobaltský	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
část	část	k1gFnSc4
bývalé	bývalý	k2eAgFnSc2d1
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
a	a	k8xC
Rumunsko	Rumunsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
revoluce	revoluce	k1gFnSc2
proběhla	proběhnout	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
prosinci	prosinec	k1gInSc6
1989	#num#	k4
a	a	k8xC
tamní	tamní	k2eAgMnSc1d1
diktátor	diktátor	k1gMnSc1
Nicolae	Nicola	k1gMnSc2
Ceauș	Ceauș	k1gInSc2
byl	být	k5eAaImAgInS
i	i	k9
s	s	k7c7
manželkou	manželka	k1gFnSc7
zastřelen	zastřelit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
událostí	událost	k1gFnPc2
ve	v	k7c6
východním	východní	k2eAgInSc6d1
bloku	blok	k1gInSc6
</s>
<s>
Následek	následek	k1gInSc1
takzvané	takzvaný	k2eAgFnSc2d1
Perestrojky	perestrojka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Časový	časový	k2eAgInSc1d1
přehled	přehled	k1gInSc1
hlavních	hlavní	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
rozpadu	rozpad	k1gInSc2
východoeropských	východoeropský	k2eAgInPc2d1
komunistických	komunistický	k2eAgInPc2d1
režimů	režim	k1gInPc2
v	v	k7c6
listopadu	listopad	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NDR	NDR	kA
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
rezignuje	rezignovat	k5eAaBmIp3nS
komunistická	komunistický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
NDR	NDR	kA
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
zůstává	zůstávat	k5eAaImIp3nS
Egon	Egon	k1gMnSc1
Krenz	Krenz	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
tajemník	tajemník	k1gMnSc1
SED	sed	k1gInSc1
<g/>
,	,	kIx,
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
pád	pád	k1gInSc1
Berlínské	berlínský	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
<g/>
,	,	kIx,
NDR	NDR	kA
otevírá	otevírat	k5eAaImIp3nS
hraniční	hraniční	k2eAgInPc4d1
přechody	přechod	k1gInPc4
v	v	k7c6
Berlínské	berlínský	k2eAgFnSc6d1
zdi	zeď	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
desetiletích	desetiletí	k1gNnPc6
mohou	moct	k5eAaImIp3nP
občané	občan	k1gMnPc1
NDR	NDR	kA
volně	volně	k6eAd1
překračovat	překračovat	k5eAaImF
hranice	hranice	k1gFnPc4
<g/>
,	,	kIx,
den	den	k1gInSc4
na	na	k7c4
to	ten	k3xDgNnSc4
začínají	začínat	k5eAaImIp3nP
oslavující	oslavující	k2eAgMnPc1d1
občané	občan	k1gMnPc1
strhávat	strhávat	k5eAaImF
zeď	zeď	k1gFnSc4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
po	po	k7c6
čtyřech	čtyři	k4xCgFnPc6
a	a	k8xC
půl	půl	k1xP
desetiletích	desetiletí	k1gNnPc6
vlády	vláda	k1gFnSc2
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
odstupuje	odstupovat	k5eAaImIp3nS
Todor	Todor	k1gMnSc1
Živkov	Živkov	k1gInSc1
a	a	k8xC
nahrazuje	nahrazovat	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
Petar	Petar	k1gInSc1
Mladenov	Mladenovo	k1gNnPc2
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
Socialistickou	socialistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Bulharska	Bulharsko	k1gNnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
studentské	studentský	k2eAgFnPc1d1
demonstrace	demonstrace	k1gFnPc1
zahajují	zahajovat	k5eAaImIp3nP
začátek	začátek	k1gInSc4
sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
pádu	pád	k1gInSc2
komunismu	komunismus	k1gInSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc6
–	–	k?
vojáci	voják	k1gMnPc1
základní	základní	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
VZS	VZS	kA
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
izolováni	izolován	k2eAgMnPc1d1
od	od	k7c2
vnějšího	vnější	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
do	do	k7c2
kasáren	kasárny	k1gFnPc2
přesto	přesto	k8xC
pronikly	proniknout	k5eAaPmAgFnP
a	a	k8xC
VZS	VZS	kA
dali	dát	k5eAaPmAgMnP
znát	znát	k5eAaImF
<g/>
,	,	kIx,
takže	takže	k8xS
politicky	politicky	k6eAd1
nebyla	být	k5eNaImAgFnS
armáda	armáda	k1gFnSc1
a	a	k8xC
s	s	k7c7
ní	on	k3xPp3gFnSc7
i	i	k9
bojová	bojový	k2eAgFnSc1d1
technika	technika	k1gFnSc1
v	v	k7c6
rukou	ruka	k1gFnPc6
důstojníků	důstojník	k1gMnPc2
a	a	k8xC
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
rukou	ruka	k1gFnPc6
VZS	VZS	kA
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
demonstrace	demonstrace	k1gFnSc2
pokračují	pokračovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jen	jen	k9
v	v	k7c6
Praze	Praha	k1gFnSc6
narůstají	narůstat	k5eAaImIp3nP
z	z	k7c2
200	#num#	k4
tisíc	tisíc	k4xCgInPc2
předešlých	předešlý	k2eAgInPc2d1
dnů	den	k1gInPc2
na	na	k7c4
půl	půl	k1xP
milionu	milion	k4xCgInSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
generální	generální	k2eAgFnSc1d1
stávka	stávka	k1gFnSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
–	–	k?
jako	jako	k8xC,k8xS
ostatní	ostatní	k2eAgMnPc4d1
již	již	k6eAd1
padlé	padlý	k2eAgInPc1d1
komunistické	komunistický	k2eAgInPc1d1
režimy	režim	k1gInPc1
oznamuje	oznamovat	k5eAaImIp3nS
i	i	k9
KSČ	KSČ	kA
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vzdá	vzdát	k5eAaPmIp3nS
mocenského	mocenský	k2eAgInSc2d1
monopolu	monopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
události	událost	k1gFnPc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
proběhly	proběhnout	k5eAaPmAgFnP
první	první	k4xOgFnPc1
svobodné	svobodný	k2eAgFnPc1d1
prezidentské	prezidentský	k2eAgFnPc1d1
volby	volba	k1gFnPc1
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
jihoafrický	jihoafrický	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Frederik	Frederik	k1gMnSc1
Willem	Will	k1gMnSc7
de	de	k?
Klerk	Klerk	k1gInSc1
ruší	rušit	k5eAaImIp3nS
segregační	segregační	k2eAgInPc4d1
zákony	zákon	k1gInPc4
Apartheidu	apartheid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
šest	šest	k4xCc4
jezuitských	jezuitský	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gMnPc3
hospodyně	hospodyně	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
dcera	dcera	k1gFnSc1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
salvadorskými	salvadorský	k2eAgFnPc7d1
vojáky	voják	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
vycvičení	vycvičený	k2eAgMnPc1d1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
Aleksinac	Aleksinac	k1gFnSc1
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
Jugoslávii	Jugoslávie	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
výbuchu	výbuch	k1gInSc3
metanu	metan	k1gInSc2
v	v	k7c6
dole	dol	k1gInSc6
a	a	k8xC
zahynulo	zahynout	k5eAaPmAgNnS
při	při	k7c6
tom	ten	k3xDgMnSc6
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
libanonský	libanonský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Rene	Ren	k1gFnSc2
Moawad	Moawad	k1gInSc1
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
explozí	exploze	k1gFnSc7
bomby	bomba	k1gFnSc2
ve	v	k7c6
východním	východní	k2eAgInSc6d1
Bejrútu	Bejrút	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
Alfred	Alfred	k1gMnSc1
Herrhausen	Herrhausna	k1gFnPc2
ředitel	ředitel	k1gMnSc1
Deutsche	Deutsch	k1gFnSc2
Bank	bank	k1gInSc1
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
explozí	exploze	k1gFnSc7
bomby	bomba	k1gFnSc2
nastražené	nastražený	k2eAgFnSc2d1
teroristickou	teroristický	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
Frakce	frakce	k1gFnSc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Listopad	listopad	k1gInSc1
1989	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Projekt	projekt	k1gInSc1
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
soudobé	soudobý	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
<g/>
:	:	kIx,
<g/>
Demokratická	demokratický	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
1989	#num#	k4
</s>
<s>
“	“	k?
<g/>
Chronik	chronik	k1gMnSc1
der	drát	k5eAaImRp2nS
Wende	Wend	k1gMnSc5
<g/>
"	"	kIx"
<g/>
,	,	kIx,
„	„	k?
<g/>
The	The	k1gMnSc1
Fall	Fall	k1gMnSc1
of	of	k?
the	the	k?
Wall	Wall	k1gInSc1
<g/>
“	“	k?
–	–	k?
německy	německy	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
|	|	kIx~
Společnost	společnost	k1gFnSc1
</s>
