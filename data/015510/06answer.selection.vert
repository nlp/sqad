<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
významná	významný	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
sídlící	sídlící	k2eAgFnSc1d1
převážně	převážně	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
k	k	k7c3
největším	veliký	k2eAgFnPc3d3
a	a	k8xC
nejstarším	starý	k2eAgFnPc3d3
technickým	technický	k2eAgFnPc3d1
vysokým	vysoký	k2eAgFnPc3d1
školám	škola	k1gFnPc3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>