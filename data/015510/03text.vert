<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
Logo	logo	k1gNnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
ČVUT	ČVUT	kA
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
1707	#num#	k4
Typ	typ	k1gInSc1
školy	škola	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgFnSc1d1
Vedení	vedení	k1gNnSc1
Rektor	rektor	k1gMnSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Vojtěch	Vojtěch	k1gMnSc1
Petráček	Petráček	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Kvestor	kvestor	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Jiří	Jiří	k1gMnSc1
Boháček	Boháček	k1gMnSc1
Kancléř	kancléř	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Lucie	Lucie	k1gFnSc1
Orgoníková	Orgoníková	k1gFnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
bakalářské	bakalářský	k2eAgNnSc4d1
a	a	k8xC
magisterské	magisterský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
</s>
<s>
doc.	doc.	kA
Dr	dr	kA
<g/>
.	.	kIx.
Ing.	ing.	kA
Gabriela	Gabriela	k1gFnSc1
Achtenová	Achtenová	k1gFnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
tvůrčí	tvůrčí	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
a	a	k8xC
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Zbyněk	Zbyněk	k1gMnSc1
Škvor	Škvor	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
strategie	strategie	k1gFnPc4
</s>
<s>
Ing.	ing.	kA
Veronika	Veronika	k1gFnSc1
Kramaříková	Kramaříková	k1gFnSc1
<g/>
,	,	kIx,
MBA	MBA	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
výstavbu	výstavba	k1gFnSc4
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Alena	Alena	k1gFnSc1
Kohoutková	Kohoutková	k1gFnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
<g/>
,	,	kIx,
FEng	FEng	k1gMnSc1
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
řízení	řízení	k1gNnSc4
kvality	kvalita	k1gFnSc2
</s>
<s>
Ing.	ing.	kA
Radek	Radek	k1gMnSc1
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
zahraniční	zahraniční	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
</s>
<s>
prof.	prof.	kA
Ing.	ing.	kA
Oldřich	Oldřich	k1gMnSc1
Starý	Starý	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Předseda	předseda	k1gMnSc1
akademického	akademický	k2eAgInSc2d1
senátu	senát	k1gInSc2
</s>
<s>
doc.	doc.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Počty	počet	k1gInPc1
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2019	#num#	k4
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
bakalářských	bakalářský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
10	#num#	k4
783	#num#	k4
Počet	počet	k1gInSc4
magisterských	magisterský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
5	#num#	k4
092	#num#	k4
Počet	počet	k1gInSc4
doktorandů	doktorand	k1gMnPc2
</s>
<s>
1	#num#	k4
735	#num#	k4
Počet	počet	k1gInSc4
studentů	student	k1gMnPc2
</s>
<s>
17	#num#	k4
229	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
1	#num#	k4
591	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Počet	počet	k1gInSc1
fakult	fakulta	k1gFnPc2
</s>
<s>
8	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Jugoslávských	jugoslávský	k2eAgMnPc2d1
partyzánů	partyzán	k1gMnPc2
1580	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
160	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
7,8	7,8	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
18,5	18,5	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kampus	kampus	k1gInSc1
</s>
<s>
Kampus	kampus	k1gInSc1
Dejvice	Dejvice	k1gFnPc1
</s>
<s>
http://www.cvut.cz	http://www.cvut.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
významná	významný	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
sídlící	sídlící	k2eAgFnSc1d1
převážně	převážně	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
k	k	k7c3
největším	veliký	k2eAgFnPc3d3
a	a	k8xC
nejstarším	starý	k2eAgFnPc3d3
technickým	technický	k2eAgFnPc3d1
vysokým	vysoký	k2eAgFnPc3d1
školám	škola	k1gFnPc3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
má	mít	k5eAaImIp3nS
ČVUT	ČVUT	kA
osm	osm	k4xCc4
fakult	fakulta	k1gFnPc2
a	a	k8xC
studuje	studovat	k5eAaImIp3nS
na	na	k7c6
něm	on	k3xPp3gInSc6
přes	přes	k7c4
18	#num#	k4
tisíc	tisíc	k4xCgInPc2
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
akademický	akademický	k2eAgInSc4d1
rok	rok	k1gInSc4
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
nabízí	nabízet	k5eAaImIp3nS
ČVUT	ČVUT	kA
svým	svůj	k3xOyFgNnSc7
studentům	student	k1gMnPc3
214	#num#	k4
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
v	v	k7c6
českém	český	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
84	#num#	k4
programů	program	k1gInPc2
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Rektoři	rektor	k1gMnPc1
ČTU	číst	k5eAaImIp1nS
<g/>
,	,	kIx,
rok	rok	k1gInSc4
1872	#num#	k4
</s>
<s>
Budova	budova	k1gFnSc1
B	B	kA
a	a	k8xC
C	C	kA
stavební	stavební	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
a	a	k8xC
fakulty	fakulta	k1gFnSc2
architektury	architektura	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
6	#num#	k4
-	-	kIx~
Dejvicích	Dejvice	k1gFnPc6
ve	v	k7c6
dnech	den	k1gInPc6
Sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
</s>
<s>
Rektorát	rektorát	k1gInSc1
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
Zikova	Zikův	k2eAgFnSc1d1
4	#num#	k4
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Roku	rok	k1gInSc2
1705	#num#	k4
požádal	požádat	k5eAaPmAgMnS
Christian	Christian	k1gMnSc1
Josef	Josef	k1gMnSc1
Willenberg	Willenberg	k1gMnSc1
císaře	císař	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
I.	I.	kA
o	o	k7c4
souhlas	souhlas	k1gInSc4
vyučovat	vyučovat	k5eAaImF
„	„	k?
<g/>
ingenieurský	ingenieurský	k2eAgInSc4d1
kunst	kunst	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
císařův	císařův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
ho	on	k3xPp3gMnSc4
na	na	k7c6
trůnu	trůn	k1gInSc6
vystřídal	vystřídat	k5eAaPmAgMnS
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1707	#num#	k4
reskriptem	reskript	k1gInSc7
nařídil	nařídit	k5eAaPmAgMnS
českým	český	k2eAgInPc3d1
stavům	stav	k1gInPc3
zajistit	zajistit	k5eAaPmF
v	v	k7c6
Praze	Praha	k1gFnSc6
inženýrskou	inženýrský	k2eAgFnSc4d1
výuku	výuka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
různých	různý	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
k	k	k7c3
tomu	ten	k3xDgNnSc3
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
nedošlo	dojít	k5eNaPmAgNnS
<g/>
;	;	kIx,
v	v	k7c6
říjnu	říjen	k1gInSc6
1716	#num#	k4
tedy	tedy	k8xC
Willenberg	Willenberg	k1gInSc1
svou	svůj	k3xOyFgFnSc4
žádost	žádost	k1gFnSc4
zopakoval	zopakovat	k5eAaPmAgMnS
(	(	kIx(
<g/>
českým	český	k2eAgInPc3d1
stavům	stav	k1gInPc3
i	i	k9
císaři	císař	k1gMnSc3
Karlu	Karel	k1gMnSc3
VI	VI	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
císařské	císařský	k2eAgFnSc6d1
urgenci	urgence	k1gFnSc6
byla	být	k5eAaImAgFnS
konečně	konečně	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1717	#num#	k4
dekretem	dekret	k1gInSc7
českých	český	k2eAgInPc2d1
stavů	stav	k1gInPc2
ustanovena	ustanoven	k2eAgFnSc1d1
Willenbergova	Willenbergův	k2eAgFnSc1d1
profesura	profesura	k1gFnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
inženýrská	inženýrský	k2eAgFnSc1d1
profesura	profesura	k1gFnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1718	#num#	k4
pak	pak	k6eAd1
začala	začít	k5eAaPmAgFnS
výuka	výuka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zpočátku	zpočátku	k6eAd1
učil	učit	k5eAaImAgInS,k5eAaPmAgInS
Willenberg	Willenberg	k1gInSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
bytě	byt	k1gInSc6
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
Mostecké	mostecký	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
pouhých	pouhý	k2eAgMnPc2d1
dvanáct	dvanáct	k4xCc1
studentů	student	k1gMnPc2
–	–	k?
šest	šest	k4xCc1
svobodných	svobodný	k2eAgMnPc2d1
pánů	pan	k1gMnPc2
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgMnPc4
rytíře	rytíř	k1gMnPc4
a	a	k8xC
dva	dva	k4xCgMnPc4
měšťany	měšťan	k1gMnPc4
<g/>
,	,	kIx,
každoročně	každoročně	k6eAd1
jmenované	jmenovaný	k2eAgFnPc1d1
na	na	k7c4
návrh	návrh	k1gInSc4
zemských	zemský	k2eAgInPc2d1
stavů	stav	k1gInPc2
–	–	k?
postupně	postupně	k6eAd1
však	však	k8xC
studentů	student	k1gMnPc2
přibývalo	přibývat	k5eAaImAgNnS
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1779	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
kolem	kolem	k6eAd1
200	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
výuka	výuka	k1gFnSc1
se	se	k3xPyFc4
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
vhodnějších	vhodný	k2eAgFnPc2d2
prostor	prostora	k1gFnPc2
na	na	k7c4
Staré	Staré	k2eAgNnSc4d1
Město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
byla	být	k5eAaImAgFnS
výuka	výuka	k1gFnSc1
zaměřena	zaměřit	k5eAaPmNgFnS
zejména	zejména	k9
na	na	k7c6
vojenství	vojenství	k1gNnSc6
(	(	kIx(
<g/>
kromě	kromě	k7c2
opevňování	opevňování	k1gNnSc2
se	se	k3xPyFc4
vyučovalo	vyučovat	k5eAaImAgNnS
zeměměřictví	zeměměřictví	k1gNnSc1
<g/>
,	,	kIx,
kartografie	kartografie	k1gFnSc1
<g/>
,	,	kIx,
odvodňování	odvodňování	k1gNnSc1
či	či	k8xC
mechanismy	mechanismus	k1gInPc1
zvedání	zvedání	k1gNnSc2
těžkých	těžký	k2eAgNnPc2d1
břemen	břemeno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
i	i	k9
jednou	jeden	k4xCgFnSc7
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
císařových	císařův	k2eAgFnPc2d1
motivací	motivace	k1gFnPc2
pro	pro	k7c4
zavedení	zavedení	k1gNnSc4
výuky	výuka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuka	výuka	k1gFnSc1
v	v	k7c6
prvním	první	k4xOgInSc6
roce	rok	k1gInSc6
trvala	trvat	k5eAaImAgFnS
jednu	jeden	k4xCgFnSc4
hodinu	hodina	k1gFnSc4
denně	denně	k6eAd1
<g/>
,	,	kIx,
ve	v	k7c6
druhém	druhý	k4xOgInSc6
roce	rok	k1gInSc6
již	již	k6eAd1
dvě	dva	k4xCgNnPc1
<g/>
.	.	kIx.
</s>
<s>
Nástupcem	nástupce	k1gMnSc7
prof.	prof.	kA
Willenberga	Willenberga	k1gFnSc1
byl	být	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Schor	Schor	k1gMnSc1
<g/>
,	,	kIx,
stavitel	stavitel	k1gMnSc1
vodních	vodní	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
v	v	k7c6
povodí	povodí	k1gNnSc6
Vltavy	Vltava	k1gFnSc2
a	a	k8xC
autor	autor	k1gMnSc1
na	na	k7c6
škole	škola	k1gFnSc6
využívané	využívaný	k2eAgFnPc1d1
učebnice	učebnice	k1gFnPc1
matematiky	matematika	k1gFnSc2
<g/>
;	;	kIx,
za	za	k7c4
jeho	jeho	k3xOp3gNnSc4
vedení	vedení	k1gNnSc4
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
vyučovat	vyučovat	k5eAaImF
optika	optika	k1gFnSc1
<g/>
,	,	kIx,
perspektiva	perspektiva	k1gFnSc1
<g/>
,	,	kIx,
technické	technický	k2eAgNnSc1d1
kreslení	kreslení	k1gNnSc1
a	a	k8xC
geografie	geografie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetím	třetí	k4xOgMnSc7
profesorem	profesor	k1gMnSc7
byl	být	k5eAaImAgMnS
František	František	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Herget	Herget	k1gMnSc1
<g/>
,	,	kIx,
vrchní	vrchní	k2eAgMnSc1d1
zemský	zemský	k2eAgMnSc1d1
stavební	stavební	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
a	a	k8xC
významný	významný	k2eAgMnSc1d1
zeměměřič	zeměměřič	k1gMnSc1
<g/>
;	;	kIx,
tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
již	již	k6eAd1
škola	škola	k1gFnSc1
zaměřovala	zaměřovat	k5eAaImAgFnS
hlavně	hlavně	k9
na	na	k7c4
civilní	civilní	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
stavební	stavební	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1776	#num#	k4
dala	dát	k5eAaPmAgFnS
Hergetovi	Hergetův	k2eAgMnPc1d1
svolení	svolení	k1gNnSc4
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
k	k	k7c3
využívání	využívání	k1gNnSc3
Klementina	Klementinum	k1gNnSc2
pro	pro	k7c4
účely	účel	k1gInPc4
výuky	výuka	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1786	#num#	k4
se	se	k3xPyFc4
Stavovská	stavovský	k2eAgFnSc1d1
inženýrská	inženýrský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
budovy	budova	k1gFnSc2
bývalého	bývalý	k2eAgInSc2d1
jezuitského	jezuitský	k2eAgInSc2d1
Svatováclavského	svatováclavský	k2eAgInSc2d1
semináře	seminář	k1gInSc2
v	v	k7c6
Dominikánské	dominikánský	k2eAgFnSc6d1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc6d1
Husově	Husův	k2eAgFnSc6d1
<g/>
)	)	kIx)
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
byla	být	k5eAaImAgFnS
škola	škola	k1gFnSc1
na	na	k7c6
základě	základ	k1gInSc6
dekretu	dekret	k1gInSc2
císaře	císař	k1gMnSc2
Josefa	Josef	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
pražskou	pražský	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Polytechnika	polytechnika	k1gFnSc1
–	–	k?
německá	německý	k2eAgFnSc1d1
a	a	k8xC
česká	český	k2eAgFnSc1d1
</s>
<s>
Budova	budova	k1gFnSc1
Českého	český	k2eAgInSc2d1
polytechnického	polytechnický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Průčelí	průčelí	k1gNnSc1
budovy	budova	k1gFnSc2
Českého	český	k2eAgInSc2d1
polytechnického	polytechnický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Interiér	interiér	k1gInSc1
budovy	budova	k1gFnSc2
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
Model	model	k1gInSc1
Foucaultova	Foucaultův	k2eAgNnSc2d1
kyvadla	kyvadlo	k1gNnSc2
v	v	k7c6
budově	budova	k1gFnSc6
ČVUT	ČVUT	kA
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1798	#num#	k4
vytvořil	vytvořit	k5eAaPmAgMnS
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Gerstner	Gerstner	k1gMnSc1
koncept	koncept	k1gInSc4
přetvoření	přetvoření	k1gNnSc2
inženýrské	inženýrský	k2eAgFnSc2d1
stavovské	stavovský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
na	na	k7c4
polytechniku	polytechnika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspirovala	inspirovat	k5eAaBmAgFnS
jej	on	k3xPp3gInSc4
nedlouho	dlouho	k6eNd1
předtím	předtím	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
pařížská	pařížský	k2eAgFnSc1d1
École	École	k1gFnSc1
polytechnique	polytechniquat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
návrh	návrh	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
císař	císař	k1gMnSc1
František	František	k1gMnSc1
I.	I.	kA
schválil	schválit	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1806	#num#	k4
nový	nový	k2eAgInSc1d1
Königliche	Königliche	k1gInSc1
böhmische	böhmische	k6eAd1
ständische	ständischat	k5eAaPmIp3nS
technische	technische	k6eAd1
Lehranstalt	Lehranstalt	k2eAgInSc1d1
zu	zu	k?
Prag	Prag	k1gInSc1
(	(	kIx(
<g/>
Královské	královský	k2eAgNnSc1d1
české	český	k2eAgNnSc1d1
stavovské	stavovský	k2eAgNnSc1d1
technické	technický	k2eAgNnSc1d1
učiliště	učiliště	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
zahájilo	zahájit	k5eAaPmAgNnS
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
pražská	pražský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
stala	stát	k5eAaPmAgFnS
jedinou	jediný	k2eAgFnSc7d1
institucí	instituce	k1gFnSc7
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Rakouském	rakouský	k2eAgNnSc6d1
císařství	císařství	k1gNnSc6
i	i	k8xC
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
jelikož	jelikož	k8xS
inženýrská	inženýrský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
již	již	k6eAd1
předtím	předtím	k6eAd1
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
však	však	k9
stále	stále	k6eAd1
oficiálně	oficiálně	k6eAd1
patřila	patřit	k5eAaImAgFnS
pod	pod	k7c4
pražskou	pražský	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
osamostatnila	osamostatnit	k5eAaPmAgFnS
se	se	k3xPyFc4
až	až	k9
8	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1815	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1863	#num#	k4
schválil	schválit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
Organický	organický	k2eAgInSc4d1
statut	statut	k1gInSc4
Polytechnického	polytechnický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
reforma	reforma	k1gFnSc1
polytechniky	polytechnika	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
představitelem	představitel	k1gMnSc7
školy	škola	k1gFnSc2
stal	stát	k5eAaPmAgMnS
volený	volený	k2eAgMnSc1d1
rektor	rektor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
byli	být	k5eAaImAgMnP
rozděleni	rozdělit	k5eAaPmNgMnP
do	do	k7c2
čtyř	čtyři	k4xCgInPc2
odborů	odbor	k1gInPc2
<g/>
:	:	kIx,
pozemní	pozemní	k2eAgNnSc1d1
stavitelství	stavitelství	k1gNnSc1
<g/>
,	,	kIx,
vodní	vodní	k2eAgNnSc1d1
a	a	k8xC
silniční	silniční	k2eAgNnSc1d1
stavitelství	stavitelství	k1gNnSc1
<g/>
,	,	kIx,
strojnictví	strojnictví	k1gNnSc1
a	a	k8xC
technická	technický	k2eAgFnSc1d1
lučba	lučba	k?
(	(	kIx(
<g/>
chemie	chemie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
rovnoprávný	rovnoprávný	k2eAgInSc4d1
vyučovací	vyučovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
byla	být	k5eAaImAgFnS
k	k	k7c3
němčině	němčina	k1gFnSc3
uznána	uznán	k2eAgFnSc1d1
čeština	čeština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpory	rozpora	k1gFnPc1
mezi	mezi	k7c7
českou	český	k2eAgFnSc7d1
a	a	k8xC
německou	německý	k2eAgFnSc7d1
částí	část	k1gFnSc7
profesorstva	profesorstvo	k1gNnSc2
však	však	k9
necelých	celý	k2eNgInPc2d1
šest	šest	k4xCc4
let	léto	k1gNnPc2
poté	poté	k6eAd1
vedly	vést	k5eAaImAgFnP
k	k	k7c3
rozdělení	rozdělení	k1gNnSc3
na	na	k7c4
český	český	k2eAgInSc4d1
a	a	k8xC
německý	německý	k2eAgInSc4d1
ústav	ústav	k1gInSc4
(	(	kIx(
<g/>
k	k	k7c3
8	#num#	k4
<g/>
.	.	kIx.
dubnu	duben	k1gInSc3
1869	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgFnSc1d1
Český	český	k2eAgInSc4d1
polytechnický	polytechnický	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1874	#num#	k4
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
nově	nově	k6eAd1
postavené	postavený	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
(	(	kIx(
<g/>
architekt	architekt	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
Ignác	Ignác	k1gMnSc1
Ullmann	Ullmann	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1875	#num#	k4
byly	být	k5eAaImAgFnP
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zemské	zemský	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
zestátněny	zestátněn	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
říšského	říšský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1878	#num#	k4
směli	smět	k5eAaImAgMnP
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
složili	složit	k5eAaPmAgMnP
na	na	k7c6
technice	technika	k1gFnSc6
dvě	dva	k4xCgFnPc4
státní	státní	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
<g/>
,	,	kIx,
používat	používat	k5eAaImF
tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
stavovské	stavovský	k2eAgNnSc4d1
označení	označení	k1gNnSc4
inženýr	inženýr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1879	#num#	k4
bylo	být	k5eAaImAgNnS
škole	škola	k1gFnSc6
dovoleno	dovolit	k5eAaPmNgNnS
nazývat	nazývat	k5eAaImF
se	se	k3xPyFc4
vysokou	vysoký	k2eAgFnSc7d1
školou	škola	k1gFnSc7
technickou	technický	k2eAgFnSc7d1
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
získala	získat	k5eAaPmAgFnS
škola	škola	k1gFnSc1
říšským	říšský	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
z	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
právo	právo	k1gNnSc4
udělovat	udělovat	k5eAaImF
doktoráty	doktorát	k1gInPc4
technických	technický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
vydal	vydat	k5eAaPmAgInS
profesorský	profesorský	k2eAgInSc1d1
sbor	sbor	k1gInSc1
doporučení	doporučení	k1gNnSc2
přijímat	přijímat	k5eAaImF
ženy	žena	k1gFnPc4
jako	jako	k8xC,k8xS
řádné	řádný	k2eAgFnPc4d1
posluchačky	posluchačka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
škola	škola	k1gFnSc1
využívala	využívat	k5eAaImAgFnS,k5eAaPmAgFnS
rozmachu	rozmach	k1gInSc3
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
měla	mít	k5eAaImAgFnS
přes	přes	k7c4
tři	tři	k4xCgInPc4
tisíce	tisíc	k4xCgInPc4
posluchačů	posluchač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
přednášel	přednášet	k5eAaImAgMnS
v	v	k7c6
budově	budova	k1gFnSc6
na	na	k7c6
Karlově	Karlův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
i	i	k9
profesor	profesor	k1gMnSc1
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
přednáška	přednáška	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
i	i	k9
knižně	knižně	k6eAd1
(	(	kIx(
<g/>
Student	student	k1gMnSc1
a	a	k8xC
politika	politika	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1909	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nebyl	být	k5eNaImAgInS
to	ten	k3xDgNnSc1
však	však	k9
zdaleka	zdaleka	k6eAd1
Masarykův	Masarykův	k2eAgInSc4d1
první	první	k4xOgInSc4
kontakt	kontakt	k1gInSc4
se	s	k7c7
školou	škola	k1gFnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
tam	tam	k6eAd1
přednášel	přednášet	k5eAaImAgMnS
již	již	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
příchodu	příchod	k1gInSc6
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
dr	dr	kA
<g/>
.	.	kIx.
Jan	Jan	k1gMnSc1
Herben	Herbna	k1gFnPc2
dne	den	k1gInSc2
„	„	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1882	#num#	k4
přednášel	přednášet	k5eAaImAgMnS
v	v	k7c6
sále	sál	k1gInSc6
české	český	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
na	na	k7c4
téma	téma	k1gNnSc4
<g/>
:	:	kIx,
Blaise	Blaise	k1gFnSc1
Pascal	pascal	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
život	život	k1gInSc1
a	a	k8xC
filosofie	filosofie	k1gFnSc1
s	s	k7c7
převažující	převažující	k2eAgFnSc7d1
otázkou	otázka	k1gFnSc7
náboženskou	náboženský	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následovalo	následovat	k5eAaImAgNnS
nesčetně	sčetně	k6eNd1
přednášek	přednáška	k1gFnPc2
mimo	mimo	k7c4
síně	síň	k1gFnPc4
universitní	universitní	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgFnSc2d1
</s>
<s>
Po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
ČSR	ČSR	kA
byla	být	k5eAaImAgFnS
škola	škola	k1gFnSc1
výnosem	výnos	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
a	a	k8xC
národní	národní	k2eAgFnSc2d1
osvěty	osvěta	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1920	#num#	k4
přeorganizována	přeorganizován	k2eAgFnSc1d1
–	–	k?
název	název	k1gInSc1
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
na	na	k7c4
České	český	k2eAgNnSc4d1
vysoké	vysoký	k2eAgNnSc4d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
<g/>
,	,	kIx,
místo	místo	k1gNnSc4
odborů	odbor	k1gInPc2
řízených	řízený	k2eAgInPc2d1
přednosty	přednosta	k1gMnPc7
byly	být	k5eAaImAgFnP
zavedeny	zaveden	k2eAgFnPc4d1
vysoké	vysoký	k2eAgFnPc4d1
školy	škola	k1gFnPc4
řízené	řízený	k2eAgFnPc4d1
děkany	děkan	k1gMnPc7
<g/>
,	,	kIx,
v	v	k7c6
čele	čelo	k1gNnSc6
akademického	akademický	k2eAgInSc2d1
senátu	senát	k1gInSc2
stál	stát	k5eAaImAgMnS
rektor	rektor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Tehdy	tehdy	k6eAd1
ČVUT	ČVUT	kA
tvořilo	tvořit	k5eAaImAgNnS
svazek	svazek	k1gInSc4
sedmi	sedm	k4xCc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
inženýrského	inženýrský	k2eAgNnSc2d1
stavitelství	stavitelství	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
kulturního	kulturní	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
architektury	architektura	k1gFnPc1
a	a	k8xC
pozemního	pozemní	k2eAgNnSc2d1
stavitelství	stavitelství	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
strojního	strojní	k2eAgMnSc2d1
a	a	k8xC
elektrotechnického	elektrotechnický	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
zemědělského	zemědělský	k2eAgMnSc2d1
a	a	k8xC
lesního	lesní	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
chemicko-technologického	chemicko-technologický	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
speciálních	speciální	k2eAgFnPc2d1
nauk	nauka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
kulturního	kulturní	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
začleněna	začlenit	k5eAaPmNgFnS
pod	pod	k7c4
Vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
inženýrského	inženýrský	k2eAgNnSc2d1
stavitelství	stavitelství	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1929	#num#	k4
se	s	k7c7
plnohodnotnou	plnohodnotný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
ČVUT	ČVUT	kA
stala	stát	k5eAaPmAgFnS
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
obchodní	obchodní	k2eAgFnSc1d1
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
pouze	pouze	k6eAd1
tříletá	tříletý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
založil	založit	k5eAaPmAgMnS
František	František	k1gMnSc1
Klokner	Klokner	k1gMnSc1
při	při	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
inženýrského	inženýrský	k2eAgNnSc2d1
stavitelství	stavitelství	k1gNnSc2
Výzkumný	výzkumný	k2eAgMnSc1d1
a	a	k8xC
zkušební	zkušební	k2eAgInSc1d1
ústav	ústav	k1gInSc1
hmot	hmota	k1gFnPc2
a	a	k8xC
konstrukcí	konstrukce	k1gFnPc2
stavebních	stavební	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
ČVUT	ČVUT	kA
postihlo	postihnout	k5eAaPmAgNnS
násilné	násilný	k2eAgNnSc1d1
uzavření	uzavření	k1gNnSc1
českých	český	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
za	za	k7c2
nacistické	nacistický	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1939	#num#	k4
do	do	k7c2
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
němu	on	k3xPp3gMnSc3
zůstala	zůstat	k5eAaPmAgFnS
nedokončena	dokončen	k2eNgFnSc1d1
i	i	k8xC
výstavba	výstavba	k1gFnSc1
budov	budova	k1gFnPc2
ČVUT	ČVUT	kA
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
<g/>
,	,	kIx,
započatá	započatý	k2eAgFnSc1d1
roku	rok	k1gInSc2
1925	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německá	německý	k2eAgFnSc1d1
technika	technika	k1gFnSc1
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
Benešovým	Benešův	k2eAgInSc7d1
dekretem	dekret	k1gInSc7
123	#num#	k4
<g/>
/	/	kIx~
<g/>
1945	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
ze	z	k7c2
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1948	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnPc4
</s>
<s>
Rok	rok	k1gInSc1
1948	#num#	k4
znamenal	znamenat	k5eAaImAgInS
pro	pro	k7c4
stovky	stovka	k1gFnPc4
studentů	student	k1gMnPc2
vyloučení	vyloučení	k1gNnSc2
ze	z	k7c2
studií	studio	k1gNnPc2
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
byla	být	k5eAaImAgFnS
také	také	k9
zrušena	zrušen	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
obchodní	obchodní	k2eAgFnSc1d1
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
největší	veliký	k2eAgFnSc1d3
fakulta	fakulta	k1gFnSc1
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
pod	pod	k7c7
názvem	název	k1gInSc7
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
věd	věda	k1gFnPc2
hospodářských	hospodářský	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
s	s	k7c7
ukončením	ukončení	k1gNnSc7
výuky	výuka	k1gFnSc2
na	na	k7c4
VŠO	VŠO	kA
(	(	kIx(
<g/>
Vysoké	vysoký	k2eAgFnSc3d1
škole	škola	k1gFnSc3
věd	věda	k1gFnPc2
hospodářských	hospodářský	k2eAgInPc2d1
<g/>
)	)	kIx)
roku	rok	k1gInSc2
1952	#num#	k4
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
na	na	k7c6
ČVUT	ČVUT	kA
Fakulta	fakulta	k1gFnSc1
ekonomicko-inženýrská	ekonomicko-inženýrský	k2eAgFnSc1d1
(	(	kIx(
<g/>
FEI	FEI	kA
–	–	k?
zrušena	zrušen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
1949	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
byla	být	k5eAaImAgFnS
univerzita	univerzita	k1gFnSc1
přeorganizována	přeorganizovat	k5eAaPmNgFnS
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgNnP
celoškolská	celoškolský	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
jako	jako	k8xS,k8xC
např.	např.	kA
katedry	katedra	k1gFnSc2
vojenské	vojenský	k2eAgFnSc2d1
<g/>
,	,	kIx,
marxismu-leninismu	marxismu-leninismus	k1gInSc2
<g/>
,	,	kIx,
branné	branný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
(	(	kIx(
<g/>
zrušeny	zrušit	k5eAaPmNgFnP
roku	rok	k1gInSc2
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1952	#num#	k4
byly	být	k5eAaImAgInP
z	z	k7c2
ČVUT	ČVUT	kA
vyděleny	vydělen	k2eAgFnPc1d1
Vysoká	vysoká	k1gFnSc1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
a	a	k8xC
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
zemědělská	zemědělský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
byl	být	k5eAaImAgInS
škole	škola	k1gFnSc3
udělen	udělen	k2eAgInSc1d1
Řád	řád	k1gInSc1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
sestávalo	sestávat	k5eAaImAgNnS
ČVUT	ČVUT	kA
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
fakult	fakulta	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
stavební	stavební	k2eAgFnSc1d1
(	(	kIx(
<g/>
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
stavební	stavební	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
strojní	strojní	k2eAgNnSc1d1
<g/>
,	,	kIx,
</s>
<s>
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
<g/>
,	,	kIx,
</s>
<s>
jaderná	jaderný	k2eAgFnSc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrská	inženýrský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
Fakulta	fakulta	k1gFnSc1
architektury	architektura	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
část	část	k1gFnSc4
stavební	stavební	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
pak	pak	k6eAd1
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
detašované	detašovaný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
FD	FD	kA
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
a	a	k8xC
FJFI	FJFI	kA
v	v	k7c6
Děčíně	Děčín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
k	k	k7c3
27	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2005	#num#	k4
přetransformoval	přetransformovat	k5eAaPmAgInS
na	na	k7c4
Fakultu	fakulta	k1gFnSc4
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Kladně	Kladno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
Fakulta	fakulta	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
</s>
<s>
ČVUT	ČVUT	kA
dnes	dnes	k6eAd1
má	mít	k5eAaImIp3nS
osm	osm	k4xCc1
fakult	fakulta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
podle	podle	k7c2
oborů	obor	k1gInPc2
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
katedry	katedra	k1gFnPc4
<g/>
/	/	kIx~
<g/>
ústavy	ústav	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuku	výuka	k1gFnSc4
také	také	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
pět	pět	k4xCc4
vysokoškolských	vysokoškolský	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
<g/>
,	,	kIx,
univerzita	univerzita	k1gFnSc1
také	také	k9
dále	daleko	k6eAd2
zahrnuje	zahrnovat	k5eAaImIp3nS
celoškolská	celoškolský	k2eAgNnPc4d1
účelová	účelový	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
a	a	k8xC
specializovaná	specializovaný	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Fakulty	fakulta	k1gFnPc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
stavební	stavební	k2eAgFnSc2d1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
jaderná	jaderný	k2eAgFnSc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrská	inženýrský	k2eAgFnSc1d1
</s>
<s>
Detašované	detašovaný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
Děčín	Děčín	k1gInSc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
architektury	architektura	k1gFnSc2
</s>
<s>
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc2d1
</s>
<s>
Detašované	detašovaný	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
Děčín	Děčín	k1gInSc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
</s>
<s>
Fakulta	fakulta	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
</s>
<s>
Vysokoškolské	vysokoškolský	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
</s>
<s>
Masarykův	Masarykův	k2eAgInSc4d1
ústav	ústav	k1gInSc4
vyšších	vysoký	k2eAgFnPc2d2
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
MÚVS	MÚVS	kA
<g/>
)	)	kIx)
</s>
<s>
Kloknerův	Kloknerův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
KÚ	KÚ	kA
<g/>
)	)	kIx)
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
energeticky	energeticky	k6eAd1
efektivních	efektivní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
(	(	kIx(
<g/>
UCEEB	UCEEB	kA
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
institut	institut	k1gInSc1
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
robotiky	robotika	k1gFnSc2
a	a	k8xC
kybernetiky	kybernetika	k1gFnSc2
(	(	kIx(
<g/>
CIIRC	CIIRC	kA
<g/>
)	)	kIx)
</s>
<s>
Ústav	ústav	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
(	(	kIx(
<g/>
ÚTVS	ÚTVS	kA
<g/>
)	)	kIx)
</s>
<s>
Ústav	ústav	k1gInSc1
technické	technický	k2eAgFnSc2d1
a	a	k8xC
experimentální	experimentální	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
ÚTEF	ÚTEF	kA
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
ČVUT	ČVUT	kA
</s>
<s>
Informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
ČVUT	ČVUT	kA
v	v	k7c6
AIR	AIR	kA
House	house	k1gNnSc1
</s>
<s>
Výpočetní	výpočetní	k2eAgNnSc1d1
a	a	k8xC
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
(	(	kIx(
<g/>
VIC	VIC	kA
<g/>
)	)	kIx)
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
ÚK	ÚK	kA
<g/>
)	)	kIx)
</s>
<s>
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
bylo	být	k5eAaImAgNnS
otevřeno	otevřít	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
Informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
ČVUT	ČVUT	kA
v	v	k7c6
objektu	objekt	k1gInSc6
AIR	AIR	kA
House	house	k1gNnSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
navrhli	navrhnout	k5eAaPmAgMnP
studenti	student	k1gMnPc1
Fakulty	fakulta	k1gFnSc2
architektury	architektura	k1gFnSc2
ČVUT	ČVUT	kA
pro	pro	k7c4
mezinárodní	mezinárodní	k2eAgFnSc4d1
studentskou	studentský	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
Solar	Solar	k1gMnSc1
Decathlon	Decathlon	k1gInSc4
2013	#num#	k4
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Účelová	účelový	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Rektorát	rektorát	k1gInSc1
ČVUT	ČVUT	kA
</s>
<s>
Správa	správa	k1gFnSc1
účelových	účelový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
zahrnuje	zahrnovat	k5eAaImIp3nS
i	i	k9
studentské	studentský	k2eAgFnPc4d1
koleje	kolej	k1gFnPc4
<g/>
,	,	kIx,
menzy	menza	k1gFnPc4
<g/>
,	,	kIx,
správu	správa	k1gFnSc4
Betlémské	betlémský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
technika	technika	k1gFnSc1
–	–	k?
nakladatelství	nakladatelství	k1gNnSc1
ČVUT	ČVUT	kA
</s>
<s>
Fakulty	fakulta	k1gFnPc1
a	a	k8xC
VŠ	vš	k0
ústavy	ústav	k1gInPc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
stavební	stavební	k2eAgFnSc2d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
stavební	stavební	k2eAgFnSc2d1
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
budovy	budova	k1gFnSc2
A	a	k9
Fakulty	fakulta	k1gFnPc1
stavební	stavební	k2eAgFnPc1d1
<g/>
,	,	kIx,
Thákurova	Thákurův	k2eAgFnSc1d1
7	#num#	k4
<g/>
Fakulta	fakulta	k1gFnSc1
stavební	stavební	k2eAgFnSc2d1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
osmi	osm	k4xCc2
fakult	fakulta	k1gFnPc2
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
pevným	pevný	k2eAgInSc7d1
základem	základ	k1gInSc7
univerzity	univerzita	k1gFnPc4
od	od	k7c2
jejího	její	k3xOp3gNnSc2
založení	založení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1707	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nejstarším	starý	k2eAgInSc7d3
samostatným	samostatný	k2eAgInSc7d1
oborem	obor	k1gInSc7
vznikajících	vznikající	k2eAgFnPc2d1
polytechnických	polytechnický	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
bylo	být	k5eAaImAgNnS
právě	právě	k6eAd1
stavitelské	stavitelský	k2eAgNnSc1d1
a	a	k8xC
kartografické	kartografický	k2eAgNnSc1d1
umění	umění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
historicky	historicky	k6eAd1
nejstarší	starý	k2eAgFnSc1d3
a	a	k8xC
patří	patřit	k5eAaImIp3nS
počtem	počet	k1gInSc7
studentů	student	k1gMnPc2
i	i	k8xC
dalšími	další	k2eAgMnPc7d1
parametry	parametr	k1gInPc4
svého	svůj	k3xOyFgInSc2
výkonu	výkon	k1gInSc2
k	k	k7c3
největším	veliký	k2eAgFnPc3d3
fakultám	fakulta	k1gFnPc3
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc7d1
vysokoškolskou	vysokoškolský	k2eAgFnSc7d1
institucí	instituce	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
neustále	neustále	k6eAd1
zdokonaluje	zdokonalovat	k5eAaImIp3nS
nabídku	nabídka	k1gFnSc4
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc4
skladbu	skladba	k1gFnSc4
i	i	k8xC
obsah	obsah	k1gInSc4
předmětů	předmět	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
mají	mít	k5eAaImIp3nP
řadu	řada	k1gFnSc4
příležitostí	příležitost	k1gFnPc2
absolvovat	absolvovat	k5eAaPmF
část	část	k1gFnSc4
studia	studio	k1gNnSc2
svého	svůj	k3xOyFgNnSc2
studia	studio	k1gNnSc2
také	také	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
si	se	k3xPyFc3
dává	dávat	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
držet	držet	k5eAaImF
vysokou	vysoký	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
kvality	kvalita	k1gFnSc2
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgInPc6
studijních	studijní	k2eAgInPc6d1
oborech	obor	k1gInPc6
garantuje	garantovat	k5eAaBmIp3nS
propojení	propojení	k1gNnSc1
na	na	k7c4
praxi	praxe	k1gFnSc4
a	a	k8xC
nové	nový	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
z	z	k7c2
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
se	se	k3xPyFc4
profilují	profilovat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
široká	široký	k2eAgFnSc1d1
řada	řada	k1gFnSc1
odborníků	odborník	k1gMnPc2
ve	v	k7c6
stavebnictví	stavebnictví	k1gNnSc6
s	s	k7c7
uplatněním	uplatnění	k1gNnSc7
napříč	napříč	k7c7
nejrůznějšími	různý	k2eAgFnPc7d3
oblastmi	oblast	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
vodní	vodní	k2eAgNnSc1d1
a	a	k8xC
lesní	lesní	k2eAgNnSc1d1
hospodářství	hospodářství	k1gNnSc1
<g/>
,	,	kIx,
energetika	energetika	k1gFnSc1
<g/>
,	,	kIx,
životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
a	a	k8xC
hygiena	hygiena	k1gFnSc1
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc1
<g/>
,	,	kIx,
zemědělství	zemědělství	k1gNnSc1
či	či	k8xC
průmysl	průmysl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
FS	FS	kA
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc7d3
strojní	strojní	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
vznik	vznik	k1gInSc1
je	být	k5eAaImIp3nS
datován	datovat	k5eAaImNgInS
rokem	rok	k1gInSc7
1864	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
fakultě	fakulta	k1gFnSc6
studuje	studovat	k5eAaImIp3nS
přes	přes	k7c4
3	#num#	k4
100	#num#	k4
studentů	student	k1gMnPc2
ve	v	k7c6
3	#num#	k4
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
bakalářského	bakalářský	k2eAgInSc2d1
<g/>
,	,	kIx,
5	#num#	k4
programech	program	k1gInPc6
navazujícího	navazující	k2eAgNnSc2d1
magisterského	magisterský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
a	a	k8xC
v	v	k7c6
doktorském	doktorský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
si	se	k3xPyFc3
mohou	moct	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
uvedených	uvedený	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
vybírat	vybírat	k5eAaImF
z	z	k7c2
velké	velký	k2eAgFnSc2d1
řady	řada	k1gFnSc2
oborů	obor	k1gInPc2
od	od	k7c2
aplikované	aplikovaný	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
po	po	k7c4
techniku	technika	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
má	mít	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
18	#num#	k4
ústavů	ústav	k1gInPc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
kateder	katedra	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
více	hodně	k6eAd2
než	než	k8xS
530	#num#	k4
kvalitních	kvalitní	k2eAgMnPc2d1
absolventů	absolvent	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nemají	mít	k5eNaImIp3nP
problém	problém	k1gInSc4
uplatnit	uplatnit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c6
trhu	trh	k1gInSc6
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
se	se	k3xPyFc4
podílí	podílet	k5eAaImIp3nS
i	i	k9
na	na	k7c6
činnosti	činnost	k1gFnSc6
dalších	další	k2eAgNnPc2d1
výzkumných	výzkumný	k2eAgNnPc2d1
center	centrum	k1gNnPc2
<g/>
,	,	kIx,
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
Inženýrskou	inženýrský	k2eAgFnSc7d1
akademií	akademie	k1gFnSc7
ČR	ČR	kA
<g/>
,	,	kIx,
Asociací	asociace	k1gFnPc2
výzkumných	výzkumný	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
Svazem	svaz	k1gInSc7
průmyslu	průmysl	k1gInSc2
a	a	k8xC
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
Svazem	svaz	k1gInSc7
výrobců	výrobce	k1gMnPc2
strojírenské	strojírenský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
řadou	řada	k1gFnSc7
velkých	velká	k1gFnPc2
<g/>
,	,	kIx,
středních	střední	k2eAgFnPc2d1
i	i	k8xC
malých	malý	k2eAgFnPc2d1
průmyslových	průmyslový	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
fakulty	fakulta	k1gFnSc2
je	být	k5eAaImIp3nS
špičkové	špičkový	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
a	a	k8xC
vědecké	vědecký	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc4
uznávané	uznávaný	k2eAgNnSc4d1
doma	doma	k6eAd1
i	i	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
největších	veliký	k2eAgInPc2d3
úspěchů	úspěch	k1gInPc2
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgFnSc1d1
ČVUT	ČVUT	kA
její	její	k3xOp3gInSc4
podíl	podíl	k1gInSc4
na	na	k7c6
objemu	objem	k1gInSc6
aplikovaného	aplikovaný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
FEL	FEL	kA
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
</s>
<s>
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
fakultou	fakulta	k1gFnSc7
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradici	tradice	k1gFnSc4
zde	zde	k6eAd1
má	mít	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
výuka	výuka	k1gFnSc1
informatiky	informatika	k1gFnPc1
<g/>
,	,	kIx,
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
ji	on	k3xPp3gFnSc4
označily	označit	k5eAaPmAgInP
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
českou	český	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studium	studium	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
bakalářském	bakalářský	k2eAgNnSc6d1
<g/>
,	,	kIx,
navazujících	navazující	k2eAgInPc2d1
magisterských	magisterský	k2eAgInPc2d1
a	a	k8xC
v	v	k7c6
doktorském	doktorský	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
fakultě	fakulta	k1gFnSc6
lze	lze	k6eAd1
studovat	studovat	k5eAaImF
programy	program	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Elektrotechnika	elektrotechnika	k1gFnSc1
<g/>
,	,	kIx,
energetika	energetika	k1gFnSc1
a	a	k8xC
management	management	k1gInSc1
</s>
<s>
Elektronika	elektronika	k1gFnSc1
a	a	k8xC
komunikace	komunikace	k1gFnSc1
</s>
<s>
Elektrotechnika	elektrotechnika	k1gFnSc1
<g/>
,	,	kIx,
elektronika	elektronika	k1gFnSc1
a	a	k8xC
komunikační	komunikační	k2eAgFnSc1d1
technika	technika	k1gFnSc1
</s>
<s>
Kybernetika	kybernetika	k1gFnSc1
a	a	k8xC
robotika	robotika	k1gFnSc1
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1
informatika	informatika	k1gFnSc1
</s>
<s>
Otevřené	otevřený	k2eAgInPc1d1
elektronické	elektronický	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
Softwarové	softwarový	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
a	a	k8xC
technologie	technologie	k1gFnSc1
</s>
<s>
Inteligentní	inteligentní	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
</s>
<s>
Biomedicínské	Biomedicínský	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
a	a	k8xC
informatika	informatika	k1gFnSc1
</s>
<s>
Elektrotechnika	elektrotechnika	k1gFnSc1
a	a	k8xC
informatika	informatika	k1gFnSc1
</s>
<s>
Lékařská	lékařský	k2eAgFnSc1d1
elektronika	elektronika	k1gFnSc1
a	a	k8xC
bioinformatika	bioinformatika	k1gFnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
předmětů	předmět	k1gInPc2
je	být	k5eAaImIp3nS
vyučována	vyučovat	k5eAaImNgFnS
i	i	k9
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
řadí	řadit	k5eAaImIp3nS
do	do	k7c2
přední	přední	k2eAgFnSc2d1
desítky	desítka	k1gFnSc2
výzkumných	výzkumný	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
produkuje	produkovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
30	#num#	k4
%	%	kIx~
výzkumných	výzkumný	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
celého	celý	k2eAgInSc2d1
ČVUT	ČVUT	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracuje	spolupracovat	k5eAaImIp3nS
se	s	k7c7
špičkovými	špičkový	k2eAgFnPc7d1
světovými	světový	k2eAgFnPc7d1
univerzitami	univerzita	k1gFnPc7
a	a	k8xC
výzkumnými	výzkumný	k2eAgInPc7d1
ústavy	ústav	k1gInPc7
i	i	k8xC
s	s	k7c7
řadou	řada	k1gFnSc7
významných	významný	k2eAgFnPc2d1
průmyslových	průmyslový	k2eAgFnPc2d1
<g/>
,	,	kIx,
energetických	energetický	k2eAgFnPc2d1
a	a	k8xC
telekomunikačních	telekomunikační	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Škoda	škoda	k6eAd1
Auto	auto	k1gNnSc1
<g/>
,	,	kIx,
Porsche	Porsche	k1gNnSc1
<g/>
,	,	kIx,
T-Mobile	T-Mobila	k1gFnSc6
či	či	k8xC
Samsung	Samsung	kA
<g/>
.	.	kIx.
</s>
<s>
Elektrofakulta	Elektrofakulta	k1gFnSc1
je	být	k5eAaImIp3nS
domovem	domov	k1gInSc7
mnoha	mnoho	k4c2
zajímavých	zajímavý	k2eAgInPc2d1
projektů	projekt	k1gInPc2
–	–	k?
například	například	k6eAd1
leteckého	letecký	k2eAgInSc2d1
simulátoru	simulátor	k1gInSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
aplikace	aplikace	k1gFnPc1
cykloplánovač	cykloplánovač	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Tým	tým	k1gInSc1
studentů	student	k1gMnPc2
se	se	k3xPyFc4
také	také	k9
již	již	k6eAd1
osmým	osmý	k4xOgInSc7
rokem	rok	k1gInSc7
úspěšně	úspěšně	k6eAd1
účastní	účastnit	k5eAaImIp3nP
celosvětové	celosvětový	k2eAgFnPc4d1
konstrukční	konstrukční	k2eAgFnPc4d1
a	a	k8xC
závodní	závodní	k2eAgFnPc4d1
soutěže	soutěž	k1gFnPc4
univerzitních	univerzitní	k2eAgMnPc2d1
týmů	tým	k1gInPc2
Formule	formule	k1gFnSc2
Student	student	k1gMnSc1
v	v	k7c6
kategorii	kategorie	k1gFnSc6
formulí	formule	k1gFnPc2
s	s	k7c7
elektrickým	elektrický	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fakulta	fakulta	k1gFnSc1
jaderná	jaderný	k2eAgFnSc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrská	inženýrský	k2eAgFnSc1d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
jaderná	jaderný	k2eAgFnSc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrská	inženýrský	k2eAgFnSc1d1
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
jaderná	jaderný	k2eAgFnSc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrská	inženýrský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Břehová	břehový	k2eAgFnSc1d1
7	#num#	k4
</s>
<s>
Na	na	k7c6
Fakultě	fakulta	k1gFnSc6
jaderné	jaderný	k2eAgFnSc6d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrské	inženýrský	k2eAgInPc1d1
(	(	kIx(
<g/>
FJFI	FJFI	kA
<g/>
)	)	kIx)
studuje	studovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
1300	#num#	k4
studentů	student	k1gMnPc2
v	v	k7c6
10	#num#	k4
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
bakalářského	bakalářský	k2eAgMnSc2d1
a	a	k8xC
10	#num#	k4
programech	program	k1gInPc6
magisterského	magisterský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
a	a	k8xC
v	v	k7c6
doktorském	doktorský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
má	mít	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
10	#num#	k4
kateder	katedra	k1gFnPc2
a	a	k8xC
jejími	její	k3xOp3gFnPc7
zdmi	zeď	k1gFnPc7
prošlo	projít	k5eAaPmAgNnS
již	již	k9
5	#num#	k4
631	#num#	k4
kvalitních	kvalitní	k2eAgMnPc2d1
a	a	k8xC
specializovaných	specializovaný	k2eAgMnPc2d1
absolventů	absolvent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současným	současný	k2eAgMnSc7d1
děkanem	děkan	k1gMnSc7
je	být	k5eAaImIp3nS
prof.	prof.	kA
Igor	Igor	k1gMnSc1
Jex	Jex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
založená	založený	k2eAgFnSc1d1
původně	původně	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
čs	čs	kA
<g/>
.	.	kIx.
jaderného	jaderný	k2eAgInSc2d1
programu	program	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
postupně	postupně	k6eAd1
rozšířila	rozšířit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
působnost	působnost	k1gFnSc4
na	na	k7c4
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
matematických	matematický	k2eAgInPc2d1
<g/>
,	,	kIx,
fyzikálních	fyzikální	k2eAgInPc2d1
a	a	k8xC
chemických	chemický	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
60	#num#	k4
let	let	k1gInSc4
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
si	se	k3xPyFc3
vydobyla	vydobýt	k5eAaPmAgFnS
pověst	pověst	k1gFnSc4
náročné	náročný	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
vzdělání	vzdělání	k1gNnSc4
vysoké	vysoký	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
s	s	k7c7
hlubokým	hluboký	k2eAgInSc7d1
matematicko-fyzikálním	matematicko-fyzikální	k2eAgInSc7d1
základem	základ	k1gInSc7
a	a	k8xC
individuálním	individuální	k2eAgInSc7d1
přístupem	přístup	k1gInSc7
ke	k	k7c3
studentům	student	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
podílejí	podílet	k5eAaImIp3nP
na	na	k7c4
práci	práce	k1gFnSc4
kateder	katedra	k1gFnPc2
a	a	k8xC
vědeckých	vědecký	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
naučí	naučit	k5eAaPmIp3nS
se	se	k3xPyFc4
nejméně	málo	k6eAd3
dva	dva	k4xCgInPc4
světové	světový	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
důvěrně	důvěrně	k6eAd1
se	se	k3xPyFc4
sžijí	sžít	k5eAaPmIp3nP
s	s	k7c7
výpočetní	výpočetní	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
a	a	k8xC
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
orientace	orientace	k1gFnPc4
v	v	k7c6
mezioborové	mezioborový	k2eAgFnSc6d1
problematice	problematika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
absolvuje	absolvovat	k5eAaPmIp3nS
studijní	studijní	k2eAgInPc4d1
pobyty	pobyt	k1gInPc4
na	na	k7c6
zahraničních	zahraniční	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
jsou	být	k5eAaImIp3nP
všestranně	všestranně	k6eAd1
připraveni	připravit	k5eAaPmNgMnP
a	a	k8xC
v	v	k7c6
praxi	praxe	k1gFnSc6
vysoce	vysoce	k6eAd1
úspěšní	úspěšný	k2eAgMnPc1d1
bez	bez	k7c2
problému	problém	k1gInSc2
uplatnit	uplatnit	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c6
trhu	trh	k1gInSc6
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
se	se	k3xPyFc4
také	také	k9
snaží	snažit	k5eAaImIp3nS
maximálně	maximálně	k6eAd1
vyjít	vyjít	k5eAaPmF
vstříc	vstříc	k6eAd1
handicapovaným	handicapovaný	k2eAgMnPc3d1
studentům	student	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celá	celý	k2eAgFnSc1d1
budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
bezbariérová	bezbariérový	k2eAgFnSc1d1
a	a	k8xC
díky	díky	k7c3
svému	svůj	k3xOyFgNnSc3
technickému	technický	k2eAgNnSc3d1
vybavení	vybavení	k1gNnSc3
umožňuje	umožňovat	k5eAaImIp3nS
studium	studium	k1gNnSc4
i	i	k9
zrakově	zrakově	k6eAd1
postiženým	postižený	k2eAgMnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funguje	fungovat	k5eAaImIp3nS
zde	zde	k6eAd1
Středisko	středisko	k1gNnSc1
pro	pro	k7c4
podporu	podpora	k1gFnSc4
studentů	student	k1gMnPc2
se	s	k7c7
specifickými	specifický	k2eAgFnPc7d1
potřebami	potřeba	k1gFnPc7
ČVUT	ČVUT	kA
ELSA	Elsa	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
handicapovaní	handicapovaný	k2eAgMnPc1d1
získávají	získávat	k5eAaImIp3nP
nejen	nejen	k6eAd1
technickou	technický	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
fakulta	fakulta	k1gFnSc1
snaží	snažit	k5eAaImIp3nS
podporovat	podporovat	k5eAaImF
nové	nový	k2eAgInPc4d1
trendy	trend	k1gInPc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
a	a	k8xC
konkurovat	konkurovat	k5eAaImF
zahraničním	zahraniční	k2eAgFnPc3d1
univerzitám	univerzita	k1gFnPc3
ve	v	k7c6
vědeckých	vědecký	k2eAgInPc6d1
projektech	projekt	k1gInPc6
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
jsou	být	k5eAaImIp3nP
ostatně	ostatně	k6eAd1
důsledkem	důsledek	k1gInSc7
nejen	nejen	k6eAd1
kvalitní	kvalitní	k2eAgFnPc4d1
vysoce	vysoce	k6eAd1
impaktované	impaktovaný	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
mezinárodních	mezinárodní	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
pro	pro	k7c4
pracovníky	pracovník	k1gMnPc4
nebo	nebo	k8xC
studenty	student	k1gMnPc4
FJFI	FJFI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmostí	samozřejmost	k1gFnPc2
je	být	k5eAaImIp3nS
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
mezinárodními	mezinárodní	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
velkými	velký	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
<g/>
,	,	kIx,
například	například	k6eAd1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
ČEZ	ČEZ	kA
fakulta	fakulta	k1gFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
provoz	provoz	k1gInSc4
a	a	k8xC
organizuje	organizovat	k5eAaBmIp3nS
využití	využití	k1gNnSc1
školního	školní	k2eAgInSc2d1
jaderného	jaderný	k2eAgInSc2d1
reaktoru	reaktor	k1gInSc2
VR-1	VR-1	k1gMnSc1
VRABEC	Vrabec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
unikátní	unikátní	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
v	v	k7c6
celém	celý	k2eAgInSc6d1
rezortu	rezort	k1gInSc6
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuky	výuka	k1gFnSc2
na	na	k7c6
lehkovodním	lehkovodní	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
bazénového	bazénový	k2eAgInSc2d1
typu	typ	k1gInSc2
se	se	k3xPyFc4
kromě	kromě	k7c2
kmenových	kmenový	k2eAgMnPc2d1
posluchačů	posluchač	k1gMnPc2
FJFI	FJFI	kA
v	v	k7c6
různé	různý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
účastní	účastnit	k5eAaImIp3nP
i	i	k9
studenti	student	k1gMnPc1
zhruba	zhruba	k6eAd1
12	#num#	k4
fakult	fakulta	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
mnoho	mnoho	k4c1
studentů	student	k1gMnPc2
ze	z	k7c2
zahraničních	zahraniční	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
exkurze	exkurze	k1gFnPc4
s	s	k7c7
ukázkou	ukázka	k1gFnSc7
provozu	provoz	k1gInSc2
jezdí	jezdit	k5eAaImIp3nS
také	také	k9
stále	stále	k6eAd1
větší	veliký	k2eAgInSc4d2
počet	počet	k1gInSc4
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
Dne	den	k1gInSc2
na	na	k7c6
Jaderce	Jaderka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
štěpného	štěpný	k2eAgInSc2d1
reaktoru	reaktor	k1gInSc2
fakulta	fakulta	k1gFnSc1
spravuje	spravovat	k5eAaImIp3nS
unikátní	unikátní	k2eAgInSc4d1
pokusný	pokusný	k2eAgInSc4d1
reaktor	reaktor	k1gInSc4
pro	pro	k7c4
zvládnutí	zvládnutí	k1gNnSc4
řízené	řízený	k2eAgFnSc2d1
termojaderné	termojaderný	k2eAgFnSc2d1
fúze	fúze	k1gFnSc2
<g/>
,	,	kIx,
tokamak	tokamak	k1gMnSc1
GOLEM	Golem	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
získala	získat	k5eAaPmAgFnS
roku	rok	k1gInSc2
2007	#num#	k4
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
AV	AV	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
záštitou	záštita	k1gFnSc7
FJFI	FJFI	kA
fungují	fungovat	k5eAaImIp3nP
spolky	spolek	k1gInPc4
Studentská	studentský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
při	při	k7c6
FJFI	FJFI	kA
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
organizuje	organizovat	k5eAaBmIp3nS
akce	akce	k1gFnSc1
jako	jako	k8xS,k8xC
Noc	noc	k1gFnSc1
na	na	k7c6
Jaderce	Jaderka	k1gFnSc6
<g/>
,	,	kIx,
reprezentační	reprezentační	k2eAgInSc1d1
ples	ples	k1gInSc1
Všejaderná	Všejaderný	k2eAgFnSc1d1
fúze	fúze	k1gFnSc1
či	či	k8xC
představení	představení	k1gNnSc1
fakultního	fakultní	k2eAgInSc2d1
divadelního	divadelní	k2eAgInSc2d1
spolku	spolek	k1gInSc2
Nucleus	Nucleus	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
sportovní	sportovní	k2eAgInSc1d1
tým	tým	k1gInSc1
Tralalalala	Tralalalala	k1gMnPc3
zajišťující	zajišťující	k2eAgNnSc4d1
sportovní	sportovní	k2eAgNnSc4d1
vyžití	vyžití	k1gNnSc4
na	na	k7c6
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
architektury	architektura	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
architektury	architektura	k1gFnSc2
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
Thákurova	Thákurův	k2eAgFnSc1d1
9	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
architektury	architektura	k1gFnSc2
(	(	kIx(
<g/>
FA	fa	kA
<g/>
)	)	kIx)
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
institucí	instituce	k1gFnSc7
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
ve	v	k7c6
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
Architektura	architektura	k1gFnSc1
a	a	k8xC
urbanismus	urbanismus	k1gInSc1
<g/>
,	,	kIx,
Krajinářská	krajinářský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
a	a	k8xC
Design	design	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakultu	fakulta	k1gFnSc4
sídlící	sídlící	k2eAgFnSc1d1
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
budově	budova	k1gFnSc6
ČVUT	ČVUT	kA
v	v	k7c6
akademickém	akademický	k2eAgInSc6d1
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
studovalo	studovat	k5eAaImAgNnS
1014	#num#	k4
studentů	student	k1gMnPc2
v	v	k7c6
bakalářském	bakalářský	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
navazujícím	navazující	k2eAgNnSc6d1
magisterském	magisterský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
629	#num#	k4
studentů	student	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
81	#num#	k4
zahraničních	zahraniční	k2eAgFnPc2d1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
doktorském	doktorský	k2eAgInSc6d1
programu	program	k1gInSc6
164	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Výuka	výuka	k1gFnSc1
architektury	architektura	k1gFnSc2
na	na	k7c6
Českém	český	k2eAgNnSc6d1
vysokém	vysoký	k2eAgNnSc6d1
učení	učení	k1gNnSc6
technickém	technický	k2eAgNnSc6d1
má	mít	k5eAaImIp3nS
hlubokou	hluboký	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
základy	základ	k1gInPc1
v	v	k7c6
samých	samý	k3xTgInPc6
počátcích	počátek	k1gInPc6
existence	existence	k1gFnSc2
Stavovské	stavovský	k2eAgFnSc2d1
inženýrské	inženýrský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
(	(	kIx(
<g/>
1707	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
pražské	pražský	k2eAgFnSc2d1
polytechniky	polytechnika	k1gFnSc2
(	(	kIx(
<g/>
1803	#num#	k4
–	–	k?
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
výuce	výuka	k1gFnSc6
podílela	podílet	k5eAaImAgFnS
řada	řada	k1gFnSc1
vynikajících	vynikající	k2eAgMnPc2d1
českých	český	k2eAgMnPc2d1
architektů	architekt	k1gMnPc2
jako	jako	k8xC,k8xS
např.	např.	kA
J.	J.	kA
Fischer	Fischer	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Zítek	Zítek	k1gMnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Schulz	Schulz	k1gMnSc1
nebo	nebo	k8xC
Antonín	Antonín	k1gMnSc1
Balšánek	balšánek	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Fanta	Fanta	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Koula	Koula	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
byla	být	k5eAaImAgFnS
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
architektury	architektura	k1gFnSc2
a	a	k8xC
pozemního	pozemní	k2eAgNnSc2d1
stavitelství	stavitelství	k1gNnSc2
(	(	kIx(
<g/>
od	od	k7c2
1950	#num#	k4
Fakulta	fakulta	k1gFnSc1
<g/>
)	)	kIx)
jednou	jednou	k6eAd1
ze	z	k7c2
sedmi	sedm	k4xCc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
ČVUT	ČVUT	kA
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sloučením	sloučení	k1gNnSc7
několika	několik	k4yIc2
fakult	fakulta	k1gFnPc2
vznikla	vzniknout	k5eAaPmAgFnS
Fakulta	fakulta	k1gFnSc1
stavební	stavební	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samostatná	samostatný	k2eAgFnSc1d1
existence	existence	k1gFnSc1
Fakulty	fakulta	k1gFnSc2
architektury	architektura	k1gFnSc2
se	se	k3xPyFc4
obnovila	obnovit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
a	a	k8xC
studijní	studijní	k2eAgInSc4d1
směr	směr	k1gInSc4
Pozemní	pozemní	k2eAgNnSc1d1
stavitelství	stavitelství	k1gNnSc1
zůstal	zůstat	k5eAaPmAgInS
součástí	součást	k1gFnSc7
velké	velký	k2eAgFnSc2d1
Fakulty	fakulta	k1gFnSc2
stavební	stavební	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
„	„	k?
<g/>
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
<g/>
“	“	k?
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
výrazným	výrazný	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
organizaci	organizace	k1gFnSc6
i	i	k8xC
struktuře	struktura	k1gFnSc6
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosavadní	dosavadní	k2eAgInSc1d1
klasický	klasický	k2eAgInSc1d1
sytém	sytý	k2eAgInSc6d1
typologicky	typologicky	k6eAd1
rozdělených	rozdělený	k2eAgFnPc2d1
kateder	katedra	k1gFnPc2
nahradil	nahradit	k5eAaPmAgInS
systém	systém	k1gInSc1
ústavů	ústav	k1gInPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
volnější	volný	k2eAgInSc4d2
<g/>
,	,	kIx,
založený	založený	k2eAgInSc4d1
na	na	k7c6
kombinaci	kombinace	k1gFnSc6
znalostní	znalostní	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
s	s	k7c7
tvorbou	tvorba	k1gFnSc7
ve	v	k7c6
„	„	k?
<g/>
vertikálním	vertikální	k2eAgInSc6d1
ateliéru	ateliér	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
pracují	pracovat	k5eAaImIp3nP
na	na	k7c6
různých	různý	k2eAgInPc6d1
typech	typ	k1gInPc6
projektů	projekt	k1gInPc2
studenti	student	k1gMnPc1
druhého	druhý	k4xOgMnSc4
až	až	k6eAd1
pátého	pátý	k4xOgInSc2
ročníku	ročník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ateliéry	ateliér	k1gInPc7
vedou	vést	k5eAaImIp3nP
zkušení	zkušený	k2eAgMnPc1d1
praktici	praktik	k1gMnPc1
<g/>
,	,	kIx,
přední	přední	k2eAgMnPc1d1
architekti	architekt	k1gMnPc1
<g/>
,	,	kIx,
urbanisté	urbanista	k1gMnPc1
a	a	k8xC
designéři	designér	k1gMnPc1
z	z	k7c2
ČR	ČR	kA
i	i	k8xC
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
probíhala	probíhat	k5eAaImAgFnS
na	na	k7c6
FA	fa	k1gNnSc6
ČVUT	ČVUT	kA
výuka	výuka	k1gFnSc1
ve	v	k7c6
56	#num#	k4
vertikálních	vertikální	k2eAgInPc6d1
ateliérech	ateliér	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Projekty	projekt	k1gInPc1
studentů	student	k1gMnPc2
Fakulty	fakulta	k1gFnSc2
architektury	architektura	k1gFnSc2
nezůstávají	zůstávat	k5eNaImIp3nP
jen	jen	k9
na	na	k7c6
papíře	papír	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Energeticky	energeticky	k6eAd1
soběstačný	soběstačný	k2eAgInSc4d1
dům	dům	k1gInSc4
AIR	AIR	kA
House	house	k1gNnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
se	se	k3xPyFc4
tým	tým	k1gInSc1
studentů	student	k1gMnPc2
ČVUT	ČVUT	kA
zúčastnil	zúčastnit	k5eAaPmAgMnS
mezinárodní	mezinárodní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
Solar	Solar	k1gMnSc1
Decathlon	Decathlon	k1gInSc1
pořádané	pořádaný	k2eAgNnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
energetiky	energetika	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
Department	department	k1gInSc1
of	of	k?
Energy	Energ	k1gInPc1
–	–	k?
DOE	DOE	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
ČVUT	ČVUT	kA
v	v	k7c6
dejvickém	dejvický	k2eAgInSc6d1
kampusu	kampus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Laboratoř	laboratoř	k1gFnSc1
ticha	ticho	k1gNnSc2
<g/>
,	,	kIx,
audiovizuální	audiovizuální	k2eAgFnSc4d1
instalaci	instalace	k1gFnSc4
<g/>
,	,	kIx,
navrženou	navržený	k2eAgFnSc4d1
pro	pro	k7c4
český	český	k2eAgInSc4d1
pavilon	pavilon	k1gInSc4
Světové	světový	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
EXPO	Expo	k1gNnSc1
2015	#num#	k4
<g/>
,	,	kIx,
lze	lze	k6eAd1
navštívit	navštívit	k5eAaPmF
v	v	k7c4
Národní	národní	k2eAgNnSc4d1
zemědělské	zemědělský	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc2d1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc2d1
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc2d1
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
Konviktská	konviktský	k2eAgFnSc1d1
22	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
má	mít	k5eAaImIp3nS
unikátní	unikátní	k2eAgInSc4d1
systém	systém	k1gInSc4
projektově	projektově	k6eAd1
orientované	orientovaný	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
<g/>
,	,	kIx,
studenti	student	k1gMnPc1
tak	tak	k6eAd1
od	od	k7c2
4	#num#	k4
<g/>
.	.	kIx.
semestru	semestr	k1gInSc2
pracují	pracovat	k5eAaImIp3nP
na	na	k7c6
konkrétních	konkrétní	k2eAgInPc6d1
projektech	projekt	k1gInPc6
úzce	úzko	k6eAd1
spjatých	spjatý	k2eAgMnPc2d1
s	s	k7c7
praxí	praxe	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
této	tento	k3xDgFnSc2
výuky	výuka	k1gFnSc2
jsou	být	k5eAaImIp3nP
řešena	řešen	k2eAgNnPc4d1
aktuální	aktuální	k2eAgNnPc4d1
témata	téma	k1gNnPc4
a	a	k8xC
problematiky	problematika	k1gFnPc4
z	z	k7c2
oboru	obor	k1gInSc2
dopravy	doprava	k1gFnSc2
a	a	k8xC
telekomunikací	telekomunikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
unikátní	unikátní	k2eAgInSc1d1
systém	systém	k1gInSc1
studia	studio	k1gNnSc2
dává	dávat	k5eAaImIp3nS
studentům	student	k1gMnPc3
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
vybrat	vybrat	k5eAaPmF
si	se	k3xPyFc3
svou	svůj	k3xOyFgFnSc7
specializací	specializace	k1gFnSc7
až	až	k9
po	po	k7c6
absolvování	absolvování	k1gNnSc6
1,5	1,5	k4
roku	rok	k1gInSc2
všeobecného	všeobecný	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
na	na	k7c6
fakultě	fakulta	k1gFnSc6
studuje	studovat	k5eAaImIp3nS
1012	#num#	k4
studentů	student	k1gMnPc2
ať	ať	k8xS,k8xC
už	už	k6eAd1
v	v	k7c6
bakalářském	bakalářský	k2eAgMnSc6d1
<g/>
,	,	kIx,
magisterském	magisterský	k2eAgInSc6d1
či	či	k8xC
doktorském	doktorský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
je	být	k5eAaImIp3nS
lokalizována	lokalizovat	k5eAaBmNgFnS
nejen	nejen	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
i	i	k9
detašované	detašovaný	k2eAgNnSc4d1
pracoviště	pracoviště	k1gNnSc4
v	v	k7c6
Děčíně	Děčín	k1gInSc6
s	s	k7c7
plnohodnotnou	plnohodnotný	k2eAgFnSc7d1
výukou	výuka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc1d1
jednou	jednou	k6eAd1
z	z	k7c2
nejmladších	mladý	k2eAgInPc2d3
na	na	k7c6
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nP
její	její	k3xOp3gMnPc1
absolventi	absolvent	k1gMnPc1
k	k	k7c3
těm	ten	k3xDgNnPc3
nejžádanějším	žádaný	k2eAgNnPc3d3
na	na	k7c6
trhu	trh	k1gInSc6
práce	práce	k1gFnSc2
v	v	k7c6
oblastech	oblast	k1gFnPc6
spojených	spojený	k2eAgInPc2d1
s	s	k7c7
dopravou	doprava	k1gFnSc7
<g/>
,	,	kIx,
telematikou	telematika	k1gFnSc7
a	a	k8xC
telekomunikacemi	telekomunikace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svou	svůj	k3xOyFgFnSc7
25	#num#	k4
<g/>
letou	letý	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
vybudovala	vybudovat	k5eAaPmAgFnS
mnoho	mnoho	k6eAd1
unikátních	unikátní	k2eAgNnPc2d1
vědecko-výzkumných	vědecko-výzkumný	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgMnPc6,k3yIgMnPc6,k3yRgMnPc6
projektová	projektový	k2eAgFnSc1d1
výuka	výuka	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yRgInPc3,k3yQgInPc3,k3yIgInPc3
je	být	k5eAaImIp3nS
úzce	úzko	k6eAd1
spjata	spjat	k2eAgFnSc1d1
s	s	k7c7
praxí	praxe	k1gFnSc7
a	a	k8xC
podílí	podílet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
řešení	řešení	k1gNnSc4
řady	řada	k1gFnSc2
grantových	grantový	k2eAgInPc2d1
i	i	k8xC
komerčních	komerční	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Například	například	k6eAd1
na	na	k7c6
modelu	model	k1gInSc6
v	v	k7c6
Dopravním	dopravní	k2eAgInSc6d1
sále	sál	k1gInSc6
<g/>
,	,	kIx,
lze	lze	k6eAd1
bez	bez	k7c2
rizika	riziko	k1gNnSc2
negativních	negativní	k2eAgInPc2d1
dopadů	dopad	k1gInPc2
na	na	k7c4
skutečný	skutečný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
nacvičovat	nacvičovat	k5eAaImF
technologii	technologie	k1gFnSc4
řízení	řízení	k1gNnSc2
železniční	železniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
řešení	řešení	k1gNnSc1
mimořádných	mimořádný	k2eAgFnPc2d1
situací	situace	k1gFnPc2
<g/>
,	,	kIx,
poruchových	poruchový	k2eAgInPc2d1
stavů	stav	k1gInPc2
apod.	apod.	kA
Dalším	další	k2eAgInSc7d1
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
100	#num#	k4
studentských	studentský	k2eAgInPc2d1
projektů	projekt	k1gInPc2
je	být	k5eAaImIp3nS
například	například	k6eAd1
projekt	projekt	k1gInSc1
MOTOSTUDENT	MOTOSTUDENT	kA
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
studenti	student	k1gMnPc1
vyvíjí	vyvíjet	k5eAaImIp3nP
motocykl	motocykl	k1gInSc4
vlastní	vlastní	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
a	a	k8xC
zúčastní	zúčastnit	k5eAaPmIp3nS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
mezinárodního	mezinárodní	k2eAgNnSc2d1
klání	klání	k1gNnSc2
studentských	studentský	k2eAgInPc2d1
týmů	tým	k1gInPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
zahraniční	zahraniční	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
na	na	k7c6
poli	pole	k1gNnSc6
studia	studio	k1gNnSc2
je	být	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
unikátní	unikátní	k2eAgFnSc1d1
především	především	k6eAd1
svými	svůj	k3xOyFgFnPc7
dual-degree	dual-degree	k1gFnPc7
studijními	studijní	k2eAgInPc7d1
programy	program	k1gInPc7
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
evropskými	evropský	k2eAgFnPc7d1
univerzitami	univerzita	k1gFnPc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
s	s	k7c7
americkou	americký	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
UTEP	UTEP	kA
ve	v	k7c6
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
SMART	SMART	kA
CITIES	CITIES	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
těchto	tento	k3xDgFnPc2
dual-degree	dual-degree	k1gFnPc2
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
získávají	získávat	k5eAaImIp3nP
závěrečné	závěrečný	k2eAgInPc1d1
akademické	akademický	k2eAgInPc1d1
tituly	titul	k1gInPc1
z	z	k7c2
obou	dva	k4xCgFnPc2
univerzit	univerzita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
veřejnou	veřejný	k2eAgFnSc7d1
vysokou	vysoký	k2eAgFnSc7d1
školou	škola	k1gFnSc7
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Kladně	Kladno	k1gNnSc6
a	a	k8xC
druhou	druhý	k4xOgFnSc7
nejmladší	mladý	k2eAgFnSc7d3
fakultou	fakulta	k1gFnSc7
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složení	složení	k1gNnSc1
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
charakter	charakter	k1gInSc4
výuky	výuka	k1gFnSc2
i	i	k8xC
zaměření	zaměření	k1gNnSc2
vědecké	vědecký	k2eAgFnSc2d1
a	a	k8xC
výzkumné	výzkumný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
interdisciplinárního	interdisciplinární	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
zahrnujícího	zahrnující	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
biologie	biologie	k1gFnSc2
i	i	k8xC
medicíny	medicína	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
praktickou	praktický	k2eAgFnSc4d1
výuku	výuka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
na	na	k7c6
FBMI	FBMI	kA
studuje	studovat	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
formách	forma	k1gFnPc6
studia	studio	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
některých	některý	k3yIgInPc6
oborech	obor	k1gInPc6
také	také	k9
v	v	k7c6
jazycích	jazyk	k1gInPc6
anglickém	anglický	k2eAgNnSc6d1
a	a	k8xC
ruském	ruský	k2eAgMnSc6d1
<g/>
)	)	kIx)
téměř	téměř	k6eAd1
2	#num#	k4
000	#num#	k4
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
9	#num#	k4
bakalářských	bakalářský	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
4	#num#	k4
navazující	navazující	k2eAgFnPc1d1
magisterské	magisterský	k2eAgFnPc1d1
a	a	k8xC
2	#num#	k4
doktorské	doktorský	k2eAgInPc4d1
studijní	studijní	k2eAgInPc4d1
obory	obor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
absolventi	absolvent	k1gMnPc1
mají	mít	k5eAaImIp3nP
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgNnSc4d1
uplatnění	uplatnění	k1gNnSc4
na	na	k7c6
trhu	trh	k1gInSc6
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
třináct	třináct	k4xCc4
let	léto	k1gNnPc2
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
vychovala	vychovat	k5eAaPmAgFnS
téměř	téměř	k6eAd1
1	#num#	k4
600	#num#	k4
absolventů	absolvent	k1gMnPc2
bakalářského	bakalářský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
1	#num#	k4
000	#num#	k4
absolventů	absolvent	k1gMnPc2
magisterského	magisterský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
a	a	k8xC
27	#num#	k4
absolventů	absolvent	k1gMnPc2
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
FBMI	FBMI	kA
disponuje	disponovat	k5eAaBmIp3nS
nejmodernějším	moderní	k2eAgNnSc7d3
laboratorním	laboratorní	k2eAgNnSc7d1
vybavením	vybavení	k1gNnSc7
pro	pro	k7c4
experimentální	experimentální	k2eAgFnSc4d1
výuku	výuka	k1gFnSc4
a	a	k8xC
výzkum	výzkum	k1gInSc4
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
čítající	čítající	k2eAgMnSc1d1
na	na	k7c4
30	#num#	k4
specializovaných	specializovaný	k2eAgFnPc2d1
laboratoří	laboratoř	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
těch	ten	k3xDgFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
simulují	simulovat	k5eAaImIp3nP
prostředí	prostředí	k1gNnSc4
vybraných	vybraný	k2eAgNnPc2d1
oddělení	oddělení	k1gNnPc2
urgentní	urgentní	k2eAgFnSc2d1
medicíny	medicína	k1gFnSc2
a	a	k8xC
intenzivní	intenzivní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
v	v	k7c6
nemocnicích	nemocnice	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
lze	lze	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
ČR	ČR	kA
považovat	považovat	k5eAaImF
v	v	k7c6
oblasti	oblast	k1gFnSc6
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
za	za	k7c4
unikátní	unikátní	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
unikátním	unikátní	k2eAgFnPc3d1
oblastem	oblast	k1gFnPc3
vědy	věda	k1gFnSc2
patří	patřit	k5eAaImIp3nS
i	i	k9
výzkum	výzkum	k1gInSc1
nanotechnologií	nanotechnologie	k1gFnPc2
pro	pro	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
a	a	k8xC
senzorika	senzorikum	k1gNnPc4
pro	pro	k7c4
biomedicínu	biomedicína	k1gFnSc4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
oblast	oblast	k1gFnSc1
biometrických	biometrický	k2eAgInPc2d1
systémů	systém	k1gInPc2
spojená	spojený	k2eAgFnSc1d1
se	s	k7c7
snímáním	snímání	k1gNnSc7
<g/>
,	,	kIx,
zpracováním	zpracování	k1gNnSc7
<g/>
,	,	kIx,
přenosem	přenos	k1gInSc7
<g/>
,	,	kIx,
archivací	archivace	k1gFnSc7
biologických	biologický	k2eAgInPc2d1
signálů	signál	k1gInPc2
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgNnSc4d1
uplatnění	uplatnění	k1gNnSc4
také	také	k9
v	v	k7c6
medicíně	medicína	k1gFnSc6
katastrof	katastrofa	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
výzkum	výzkum	k1gInSc1
interakce	interakce	k1gFnSc1
XUV	XUV	kA
záření	záření	k1gNnSc1
s	s	k7c7
biologickými	biologický	k2eAgInPc7d1
objekty	objekt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unikátní	unikátní	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
dosahuje	dosahovat	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
nekonvenční	konvenční	k2eNgFnSc2d1
umělé	umělý	k2eAgFnSc2d1
plicní	plicní	k2eAgFnSc2d1
ventilace	ventilace	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
kvantifikace	kvantifikace	k1gFnSc2
rehabilitačního	rehabilitační	k2eAgInSc2d1
procesu	proces	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
vyhodnocování	vyhodnocování	k1gNnSc6
polohy	poloha	k1gFnSc2
těla	tělo	k1gNnSc2
i	i	k8xC
končetin	končetina	k1gFnPc2
a	a	k8xC
očí	oko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
zavádí	zavádět	k5eAaImIp3nS
metody	metoda	k1gFnPc4
Helth	Helth	k1gInSc1
Technology	technolog	k1gMnPc4
Assessment	Assessment	k1gInSc1
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
Škola	škola	k1gFnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
širokou	široký	k2eAgFnSc4d1
výukovou	výukový	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
pro	pro	k7c4
budoucí	budoucí	k2eAgMnPc4d1
biomedicínské	biomedicínský	k2eAgMnPc4d1
techniky	technik	k1gMnPc4
a	a	k8xC
biomedicínské	biomedicínský	k2eAgMnPc4d1
inženýry	inženýr	k1gMnPc4
zahrnující	zahrnující	k2eAgInSc1d1
kompletní	kompletní	k2eAgInSc1d1
unikátní	unikátní	k2eAgInSc1d1
soubor	soubor	k1gInSc1
testerů	tester	k1gInPc2
bezpečnosti	bezpečnost	k1gFnSc2
zdravotnických	zdravotnický	k2eAgInPc2d1
elektrických	elektrický	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
od	od	k7c2
předních	přední	k2eAgMnPc2d1
světových	světový	k2eAgMnPc2d1
výrobců	výrobce	k1gMnPc2
<g/>
,	,	kIx,
analyzátorů	analyzátor	k1gInPc2
a	a	k8xC
simulátorů	simulátor	k1gInPc2
vybraných	vybraný	k2eAgInPc2d1
fyziologických	fyziologický	k2eAgInPc2d1
subsystémů	subsystém	k1gInPc2
<g/>
,	,	kIx,
či	či	k8xC
parametrů	parametr	k1gInPc2
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
též	též	k9
unikátním	unikátní	k2eAgInSc7d1
reálným	reálný	k2eAgInSc7d1
modelem	model	k1gInSc7
řídicího	řídicí	k2eAgInSc2d1
a	a	k8xC
monitorovacího	monitorovací	k2eAgInSc2d1
systému	systém	k1gInSc2
MEDICS	MEDICS	kA
pro	pro	k7c4
elektrické	elektrický	k2eAgInPc4d1
rozvody	rozvod	k1gInPc4
v	v	k7c6
nemocničních	nemocniční	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS
zázemí	zázemí	k1gNnSc1
pro	pro	k7c4
pacientskou	pacientský	k2eAgFnSc4d1
simulaci	simulace	k1gFnSc4
s	s	k7c7
využitím	využití	k1gNnSc7
nejmodernějších	moderní	k2eAgInPc2d3
prostředků	prostředek	k1gInPc2
zdravotnické	zdravotnický	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
výuku	výuka	k1gFnSc4
biomedicínské	biomedicínský	k2eAgFnSc2d1
informatiky	informatika	k1gFnSc2
včetně	včetně	k7c2
možnosti	možnost	k1gFnSc2
si	se	k3xPyFc3
vyzkoušet	vyzkoušet	k5eAaPmF
v	v	k7c6
různých	různý	k2eAgFnPc6d1
rolích	role	k1gFnPc6
nejčastěji	často	k6eAd3
používané	používaný	k2eAgInPc4d1
nemocniční	nemocniční	k2eAgInPc4d1
informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
v	v	k7c6
nemocničních	nemocniční	k2eAgNnPc6d1
zařízeních	zařízení	k1gNnPc6
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
Thákurova	Thákurův	k2eAgFnSc1d1
9	#num#	k4
</s>
<s>
Fakulta	fakulta	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
je	být	k5eAaImIp3nS
nejmladší	mladý	k2eAgFnSc7d3
fakultou	fakulta	k1gFnSc7
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
a	a	k8xC
sídlí	sídlet	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Fakultou	fakulta	k1gFnSc7
architektury	architektura	k1gFnSc2
v	v	k7c6
Nové	Nové	k2eAgFnSc6d1
budově	budova	k1gFnSc6
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Škola	škola	k1gFnSc1
zaměstnává	zaměstnávat	k5eAaImIp3nS
celkem	celkem	k6eAd1
125	#num#	k4
akademiků	akademik	k1gMnPc2
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
profesorů	profesor	k1gMnPc2
<g/>
,	,	kIx,
21	#num#	k4
docentů	docent	k1gMnPc2
a	a	k8xC
97	#num#	k4
odborných	odborný	k2eAgMnPc2d1
asistentů	asistent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
akademickém	akademický	k2eAgInSc6d1
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
studuje	studovat	k5eAaImIp3nS
na	na	k7c6
fakultě	fakulta	k1gFnSc6
2164	#num#	k4
studentů	student	k1gMnPc2
v	v	k7c6
6	#num#	k4
bakalářských	bakalářský	k2eAgInPc6d1
oborech	obor	k1gInPc6
a	a	k8xC
9	#num#	k4
magisterských	magisterský	k2eAgFnPc6d1
specializacích	specializace	k1gFnPc6
včetně	včetně	k7c2
jednoho	jeden	k4xCgInSc2
doktorského	doktorský	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Škola	škola	k1gFnSc1
má	mít	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
mnoho	mnoho	k4c4
moderních	moderní	k2eAgFnPc2d1
laboratoří	laboratoř	k1gFnPc2
a	a	k8xC
odborných	odborný	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
laboratoř	laboratoř	k1gFnSc4
etického	etický	k2eAgNnSc2d1
hackování	hackování	k1gNnSc2
<g/>
,	,	kIx,
laboratoř	laboratoř	k1gFnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
bezpečnostních	bezpečnostní	k2eAgInPc2d1
aspektů	aspekt	k1gInPc2
čipových	čipový	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
<g/>
,	,	kIx,
laboratoř	laboratoř	k1gFnSc1
3D	3D	k4
tisku	tisk	k1gInSc2
<g/>
,	,	kIx,
síťová	síťový	k2eAgFnSc1d1
multimediální	multimediální	k2eAgFnSc1d1
laboratoř	laboratoř	k1gFnSc1
SAGElab	SAGElab	k1gMnSc1
atd.	atd.	kA
</s>
<s>
Každoročně	každoročně	k6eAd1
škola	škola	k1gFnSc1
pořádá	pořádat	k5eAaImIp3nS
významné	významný	k2eAgFnPc4d1
konference	konference	k1gFnPc4
<g/>
,	,	kIx,
např.	např.	kA
LinuxDays	LinuxDays	k1gInSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgFnPc1d1
komunitní	komunitní	k2eAgFnPc1d1
konference	konference	k1gFnPc1
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
zejména	zejména	k9
svobodného	svobodný	k2eAgInSc2d1
softwaru	software	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
P2D2	P2D2	k1gFnSc1
(	(	kIx(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1
vývojářská	vývojářský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
relačním	relační	k2eAgInSc7d1
a	a	k8xC
open-source	open-sourka	k1gFnSc6
databázovým	databázový	k2eAgInSc7d1
strojem	stroj	k1gInSc7
PostgreSQL	PostgreSQL	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
The	The	k1gMnSc1
Prague	Pragu	k1gInSc2
Stringology	Stringolog	k1gMnPc4
Conference	Conferenec	k1gInSc2
(	(	kIx(
<g/>
vědecká	vědecký	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
konference	konference	k1gFnSc1
z	z	k7c2
oblastí	oblast	k1gFnPc2
stringologie	stringologie	k1gFnSc2
<g/>
,	,	kIx,
překladačů	překladač	k1gInPc2
<g/>
,	,	kIx,
komprese	komprese	k1gFnSc2
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
arbologie	arbologie	k1gFnSc2
a	a	k8xC
konečných	konečný	k2eAgInPc2d1
automatů	automat	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
LawFIT	LawFIT	k1gFnSc1
(	(	kIx(
<g/>
zabývá	zabývat	k5eAaImIp3nS
se	s	k7c7
právem	právo	k1gNnSc7
v	v	k7c6
IT	IT	kA
oblasti	oblast	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ARCS	ARCS	kA
(	(	kIx(
<g/>
nejvýznamnější	významný	k2eAgFnSc1d3
evropská	evropský	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
konference	konference	k1gFnSc1
o	o	k7c6
počítačových	počítačový	k2eAgFnPc6d1
architekturách	architektura	k1gFnPc6
a	a	k8xC
operačních	operační	k2eAgInPc6d1
systémech	systém	k1gInPc6
<g/>
)	)	kIx)
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Masarykův	Masarykův	k2eAgInSc4d1
ústav	ústav	k1gInSc4
vyšších	vysoký	k2eAgFnPc2d2
studií	studie	k1gFnPc2
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
vzdělávání	vzdělávání	k1gNnSc2
Masarykova	Masarykův	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
vyšších	vysoký	k2eAgFnPc2d2
studií	studie	k1gFnPc2
jsou	být	k5eAaImIp3nP
ekonomické	ekonomický	k2eAgFnPc1d1
a	a	k8xC
manažerské	manažerský	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
<g/>
,	,	kIx,
inženýrská	inženýrský	k2eAgFnSc1d1
pedagogika	pedagogika	k1gFnSc1
a	a	k8xC
jazyková	jazykový	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
se	se	k3xPyFc4
MÚVS	MÚVS	kA
snaží	snažit	k5eAaImIp3nP
obsáhnout	obsáhnout	k5eAaPmF
kompletní	kompletní	k2eAgNnSc4d1
portfolio	portfolio	k1gNnSc4
vysokoškolských	vysokoškolský	k2eAgInPc2d1
vzdělávacích	vzdělávací	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
propojují	propojovat	k5eAaImIp3nP
technické	technický	k2eAgInPc4d1
předměty	předmět	k1gInPc4
s	s	k7c7
předměty	předmět	k1gInPc7
humanitními	humanitní	k2eAgInPc7d1
a	a	k8xC
ekonomickými	ekonomický	k2eAgInPc7d1
a	a	k8xC
doplňují	doplňovat	k5eAaImIp3nP
tak	tak	k6eAd1
široké	široký	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
technických	technický	k2eAgInPc2d1
oborů	obor	k1gInPc2
na	na	k7c6
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
zde	zde	k6eAd1
studuje	studovat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
1300	#num#	k4
studentů	student	k1gMnPc2
v	v	k7c6
bakalářských	bakalářský	k2eAgInPc6d1
<g/>
,	,	kIx,
navazujících	navazující	k2eAgInPc6d1
magisterských	magisterský	k2eAgInPc6d1
a	a	k8xC
doktorských	doktorský	k2eAgInPc6d1
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paralelně	paralelně	k6eAd1
MÚVS	MÚVS	kA
nabízí	nabízet	k5eAaImIp3nS
výukové	výukový	k2eAgInPc4d1
programy	program	k1gInPc4
v	v	k7c6
rámci	rámec	k1gInSc6
celoživotního	celoživotní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
určené	určený	k2eAgInPc4d1
zejména	zejména	k9
absolventům	absolvent	k1gMnPc3
českých	český	k2eAgFnPc2d1
a	a	k8xC
zahraničních	zahraniční	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
realizované	realizovaný	k2eAgFnSc2d1
formou	forma	k1gFnSc7
studia	studio	k1gNnSc2
při	při	k7c6
zaměstnání	zaměstnání	k1gNnSc6
či	či	k8xC
krátkodobých	krátkodobý	k2eAgInPc2d1
kurzů	kurz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
pedagogické	pedagogický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
se	se	k3xPyFc4
akademičtí	akademický	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
ústavu	ústav	k1gInSc2
věnují	věnovat	k5eAaImIp3nP,k5eAaPmIp3nP
základnímu	základní	k2eAgMnSc3d1
a	a	k8xC
aplikovanému	aplikovaný	k2eAgInSc3d1
výzkumu	výzkum	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
především	především	k9
o	o	k7c4
výzkum	výzkum	k1gInSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
řízení	řízení	k1gNnSc2
podniku	podnik	k1gInSc2
a	a	k8xC
financí	finance	k1gFnPc2
podniku	podnik	k1gInSc2
<g/>
,	,	kIx,
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
rozhodování	rozhodování	k1gNnSc2
a	a	k8xC
strategického	strategický	k2eAgNnSc2d1
řízení	řízení	k1gNnSc2
<g/>
,	,	kIx,
marketingu	marketing	k1gInSc2
<g/>
,	,	kIx,
strategické	strategický	k2eAgNnSc4d1
prostorové	prostorový	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc4
měst	město	k1gNnPc2
a	a	k8xC
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc1
ve	v	k7c6
veřejné	veřejný	k2eAgFnSc6d1
správě	správa	k1gFnSc6
<g/>
,	,	kIx,
psychologie	psychologie	k1gFnSc1
<g/>
,	,	kIx,
sociologie	sociologie	k1gFnSc1
a	a	k8xC
pedagogiky	pedagogika	k1gFnSc2
<g/>
,	,	kIx,
historie	historie	k1gFnSc2
techniky	technika	k1gFnSc2
a	a	k8xC
průmyslového	průmyslový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kloknerův	Kloknerův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Kloknerův	Kloknerův	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kloknerův	Kloknerův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
je	být	k5eAaImIp3nS
samostatným	samostatný	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
ČVUT	ČVUT	kA
profesorem	profesor	k1gMnSc7
Františkem	František	k1gMnSc7
Kloknerem	Klokner	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Výzkumný	výzkumný	k2eAgInSc1d1
a	a	k8xC
zkušební	zkušební	k2eAgInSc1d1
ústav	ústav	k1gInSc1
hmot	hmota	k1gFnPc2
a	a	k8xC
konstrukcí	konstrukce	k1gFnPc2
stavebních	stavební	k2eAgFnPc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činnost	činnost	k1gFnSc4
a	a	k8xC
výsledky	výsledek	k1gInPc4
dosažené	dosažený	k2eAgInPc4d1
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
období	období	k1gNnSc6
prokazují	prokazovat	k5eAaImIp3nP
významné	významný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
ústavu	ústav	k1gInSc2
jak	jak	k8xC,k8xS
v	v	k7c6
rámci	rámec	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
ve	v	k7c6
světě	svět	k1gInSc6
zejména	zejména	k9
v	v	k7c6
oblastech	oblast	k1gFnPc6
diagnostiky	diagnostika	k1gFnSc2
<g/>
,	,	kIx,
monitorování	monitorování	k1gNnSc1
a	a	k8xC
hodnocení	hodnocení	k1gNnSc1
konstrukcí	konstrukce	k1gFnPc2
nebo	nebo	k8xC
výzkumu	výzkum	k1gInSc2
nových	nový	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
má	mít	k5eAaImIp3nS
čtyři	čtyři	k4xCgNnPc4
odborná	odborný	k2eAgNnPc4d1
oddělení	oddělení	k1gNnPc4
(	(	kIx(
<g/>
oddělení	oddělení	k1gNnSc3
spolehlivosti	spolehlivost	k1gFnSc2
<g/>
,	,	kIx,
stavebních	stavební	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
,	,	kIx,
mechaniky	mechanika	k1gFnPc1
a	a	k8xC
experimentální	experimentální	k2eAgFnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
akreditovanou	akreditovaný	k2eAgFnSc4d1
laboratoř	laboratoř	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
je	být	k5eAaImIp3nS
soudně-znaleckým	soudně-znalecký	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
v	v	k7c6
oboru	obor	k1gInSc6
stavebnictví	stavebnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
energeticky	energeticky	k6eAd1
efektivních	efektivní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
</s>
<s>
UCEEB	UCEEB	kA
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
jako	jako	k8xC,k8xS
samostatný	samostatný	k2eAgInSc4d1
vysokoškolský	vysokoškolský	k2eAgInSc4d1
ústav	ústav	k1gInSc4
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
za	za	k7c2
podpory	podpora	k1gFnSc2
Evropského	evropský	k2eAgInSc2d1
fondu	fond	k1gInSc2
pro	pro	k7c4
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
v	v	k7c6
Buštěhradě	Buštěhrad	k1gInSc6
byla	být	k5eAaImAgFnS
slavnostně	slavnostně	k6eAd1
otevřena	otevřen	k2eAgFnSc1d1
15	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
vzniku	vznik	k1gInSc6
centra	centrum	k1gNnSc2
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgFnP
celkem	celkem	k6eAd1
čtyři	čtyři	k4xCgFnPc4
fakulty	fakulta	k1gFnPc4
–	–	k?
stavební	stavební	k2eAgFnSc1d1
<g/>
,	,	kIx,
strojní	strojní	k2eAgFnSc1d1
<g/>
,	,	kIx,
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
a	a	k8xC
biomedicíny	biomedicína	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
sestavily	sestavit	k5eAaPmAgInP
pět	pět	k4xCc4
výzkumných	výzkumný	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
,	,	kIx,
pro	pro	k7c4
jejichž	jejichž	k3xOyRp3gNnPc4
zajištění	zajištění	k1gNnPc4
se	s	k7c7
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
účelové	účelový	k2eAgInPc4d1
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
z	z	k7c2
operačního	operační	k2eAgInSc2d1
programu	program	k1gInSc2
Věda	věda	k1gFnSc1
a	a	k8xC
výzkum	výzkum	k1gInSc1
pro	pro	k7c4
inovace	inovace	k1gFnPc4
<g/>
,	,	kIx,
řízeným	řízený	k2eAgMnSc7d1
MŠMT	MŠMT	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
UCEEB	UCEEB	kA
vznikl	vzniknout	k5eAaPmAgInS
za	za	k7c7
účelem	účel	k1gInSc7
zjednodušení	zjednodušení	k1gNnSc1
spolupráce	spolupráce	k1gFnSc2
univerzity	univerzita	k1gFnSc2
s	s	k7c7
průmyslem	průmysl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
komercializaci	komercializace	k1gFnSc6
výsledků	výsledek	k1gInPc2
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
transfer	transfer	k1gInSc1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
průmyslem	průmysl	k1gInSc7
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
synergické	synergický	k2eAgFnSc2d1
inovační	inovační	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
přidanou	přidaný	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
je	být	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc1
vyvíjet	vyvíjet	k5eAaImF
<g/>
,	,	kIx,
inovovat	inovovat	k5eAaBmF
a	a	k8xC
přicházet	přicházet	k5eAaImF
s	s	k7c7
originálními	originální	k2eAgNnPc7d1
řešeními	řešení	k1gNnPc7
v	v	k7c6
oblasti	oblast	k1gFnSc6
trvale	trvale	k6eAd1
udržitelných	udržitelný	k2eAgFnPc2d1
budov	budova	k1gFnPc2
v	v	k7c6
celém	celý	k2eAgInSc6d1
jejich	jejich	k3xOp3gInSc6
životním	životní	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
zdravého	zdravý	k2eAgNnSc2d1
vnitřního	vnitřní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
úsporu	úspora	k1gFnSc4
investičních	investiční	k2eAgInPc2d1
a	a	k8xC
provozních	provozní	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
i	i	k8xC
energií	energie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
dispozici	dispozice	k1gFnSc3
má	mít	k5eAaImIp3nS
praktickou	praktický	k2eAgFnSc4d1
integraci	integrace	k1gFnSc4
know-how	know-how	k?
vědců	vědec	k1gMnPc2
a	a	k8xC
inženýrů	inženýr	k1gMnPc2
převzatých	převzatý	k2eAgMnPc2d1
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
fakult	fakulta	k1gFnPc2
ČVUT	ČVUT	kA
z	z	k7c2
oblasti	oblast	k1gFnSc2
elektrotechniky	elektrotechnika	k1gFnSc2
<g/>
,	,	kIx,
stavebního	stavební	k2eAgInSc2d1
<g/>
,	,	kIx,
biomedicínského	biomedicínský	k2eAgMnSc2d1
a	a	k8xC
strojního	strojní	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špičkové	Špičkové	k2eAgFnSc1d1
technologické	technologický	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
výzkumné	výzkumný	k2eAgFnSc2d1
infrastruktury	infrastruktura	k1gFnSc2
a	a	k8xC
rozmanitost	rozmanitost	k1gFnSc4
laboratorního	laboratorní	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
nabízí	nabízet	k5eAaImIp3nS
místním	místní	k2eAgMnPc3d1
i	i	k8xC
zahraničním	zahraniční	k2eAgMnPc3d1
studentům	student	k1gMnPc3
možnost	možnost	k1gFnSc4
využívat	využívat	k5eAaImF,k5eAaPmF
laboratoře	laboratoř	k1gFnPc4
i	i	k8xC
zařízení	zařízení	k1gNnPc4
pro	pro	k7c4
studentské	studentský	k2eAgFnPc4d1
závěrečné	závěrečný	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
přednostně	přednostně	k6eAd1
navrhovány	navrhován	k2eAgInPc1d1
pro	pro	k7c4
zefektivnění	zefektivnění	k1gNnSc4
technologií	technologie	k1gFnPc2
používaných	používaný	k2eAgFnPc2d1
komerčně	komerčně	k6eAd1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
priorit	priorita	k1gFnPc2
univerzitního	univerzitní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
je	být	k5eAaImIp3nS
propojovat	propojovat	k5eAaImF
vědu	věda	k1gFnSc4
s	s	k7c7
průmyslem	průmysl	k1gInSc7
a	a	k8xC
spolupracovat	spolupracovat	k5eAaImF
na	na	k7c6
významných	významný	k2eAgInPc6d1
evropských	evropský	k2eAgInPc6d1
a	a	k8xC
světových	světový	k2eAgInPc6d1
projektech	projekt	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
institut	institut	k1gInSc1
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
robotiky	robotika	k1gFnSc2
a	a	k8xC
kybernetiky	kybernetika	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Český	český	k2eAgInSc1d1
institut	institut	k1gInSc1
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
robotiky	robotika	k1gFnSc2
a	a	k8xC
kybernetiky	kybernetika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
CIIRC	CIIRC	kA
v	v	k7c6
dejvickém	dejvický	k2eAgInSc6d1
kampusu	kampus	k1gInSc6
</s>
<s>
Český	český	k2eAgInSc1d1
institut	institut	k1gInSc1
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
robotiky	robotika	k1gFnSc2
a	a	k8xC
kybernetiky	kybernetika	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Czech	Czech	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Informatics	Informatics	k1gInSc1
<g/>
,	,	kIx,
Robotics	Robotics	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Cybernetics	Cybernetics	k1gInSc1
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
CIIRC	CIIRC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
založen	založit	k5eAaPmNgInS
byl	být	k5eAaImAgInS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc7
posláním	poslání	k1gNnSc7
je	být	k5eAaImIp3nS
být	být	k5eAaImF
mezinárodně	mezinárodně	k6eAd1
uznávaným	uznávaný	k2eAgNnSc7d1
výzkumným	výzkumný	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
<g/>
,	,	kIx,
podílejícím	podílející	k2eAgMnSc7d1
se	se	k3xPyFc4
na	na	k7c6
výuce	výuka	k1gFnSc6
studentů	student	k1gMnPc2
a	a	k8xC
místem	místo	k1gNnSc7
pro	pro	k7c4
přenos	přenos	k1gInSc4
technologií	technologie	k1gFnPc2
do	do	k7c2
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
dejvickém	dejvický	k2eAgInSc6d1
areálu	areál	k1gInSc6
ČVUT	ČVUT	kA
výstavba	výstavba	k1gFnSc1
dvou	dva	k4xCgFnPc2
nových	nový	k2eAgFnPc2d1
budov	budova	k1gFnPc2
určených	určený	k2eAgInPc2d1
pro	pro	k7c4
CIIRC	CIIRC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
dokončena	dokončen	k2eAgFnSc1d1
a	a	k8xC
budovy	budova	k1gFnPc1
osídleny	osídlit	k5eAaPmNgFnP
na	na	k7c6
konci	konec	k1gInSc6
léta	léto	k1gNnSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIIRC	CIIRC	kA
bude	být	k5eAaImBp3nS
sídlit	sídlit	k5eAaImF
v	v	k7c6
nově	nově	k6eAd1
zrekonstruované	zrekonstruovaný	k2eAgFnSc6d1
budově	budova	k1gFnSc6
v	v	k7c6
pražských	pražský	k2eAgFnPc6d1
Dejvicích	Dejvice	k1gFnPc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
dříve	dříve	k6eAd2
byla	být	k5eAaImAgFnS
Technická	technický	k2eAgFnSc1d1
menza	menza	k1gFnSc1
a	a	k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
českých	český	k2eAgFnPc2d1
prodejen	prodejna	k1gFnPc2
Billa	Bill	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
CIIRC	CIIRC	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
osmi	osm	k4xCc2
výzkumných	výzkumný	k2eAgNnPc2d1
oddělení	oddělení	k1gNnPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgNnPc7
jsou	být	k5eAaImIp3nP
CYPHY	CYPHY	kA
<g/>
:	:	kIx,
Kyberneticko-fyzikální	kyberneticko-fyzikální	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
INTSYS	INTSYS	kA
<g/>
:	:	kIx,
Inteligentní	inteligentní	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
IIG	IIG	kA
<g/>
:	:	kIx,
Průmyslová	průmyslový	k2eAgFnSc1d1
informatika	informatika	k1gFnSc1
<g/>
,	,	kIx,
RMP	RMP	kA
<g/>
:	:	kIx,
Robotika	robotika	k1gFnSc1
a	a	k8xC
strojové	strojový	k2eAgNnSc1d1
vnímání	vnímání	k1gNnSc1
<g/>
,	,	kIx,
IPA	IPA	kA
<g/>
:	:	kIx,
Průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
a	a	k8xC
automatizace	automatizace	k1gFnSc1
<g/>
,	,	kIx,
COGSYS	COGSYS	kA
<g/>
:	:	kIx,
Kognitivní	kognitivní	k2eAgInPc1d1
systémy	systém	k1gInPc1
a	a	k8xC
neurovědy	neurověda	k1gFnPc1
<g/>
,	,	kIx,
BEAT	beat	k1gInSc1
<g/>
:	:	kIx,
Biomedicína	Biomedicína	k1gFnSc1
a	a	k8xC
asistivní	asistivní	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
a	a	k8xC
PLAT	plat	k1gInSc1
<g/>
:	:	kIx,
Vědecké	vědecký	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
platforem	platforma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumná	výzkumný	k2eAgNnPc1d1
oddělení	oddělení	k1gNnPc1
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
členěná	členěný	k2eAgNnPc1d1
na	na	k7c4
výzkumné	výzkumný	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
CIIRC	CIIRC	kA
stojí	stát	k5eAaImIp3nS
Vladimír	Vladimír	k1gMnSc1
Mařík	Mařík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
dobu	doba	k1gFnSc4
stavby	stavba	k1gFnSc2
budovy	budova	k1gFnSc2
se	se	k3xPyFc4
CIIRC	CIIRC	kA
rozvíjí	rozvíjet	k5eAaImIp3nS
v	v	k7c6
provizorních	provizorní	k2eAgInPc6d1
prostorech	prostor	k1gInPc6
<g/>
,	,	kIx,
také	také	k9
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
fakultami	fakulta	k1gFnPc7
<g/>
,	,	kIx,
součástmi	součást	k1gFnPc7
ČVUT	ČVUT	kA
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIIRC	CIIRC	kA
postupně	postupně	k6eAd1
získává	získávat	k5eAaImIp3nS
vlastní	vlastní	k2eAgInPc4d1
výzkumné	výzkumný	k2eAgInPc4d1
projekty	projekt	k1gInPc4
<g/>
,	,	kIx,
zvyšuje	zvyšovat	k5eAaImIp3nS
počet	počet	k1gInSc1
výzkumných	výzkumný	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
výzkumný	výzkumný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ústav	ústav	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
Ústavu	ústav	k1gInSc2
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
ČVUT	ČVUT	kA
ÚTVS	ÚTVS	kA
je	být	k5eAaImIp3nS
zlepšení	zlepšení	k1gNnSc4
kvality	kvalita	k1gFnSc2
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
co	co	k9
nejvyššího	vysoký	k2eAgInSc2d3
počtu	počet	k1gInSc2
studentů	student	k1gMnPc2
a	a	k8xC
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
prostřednictvím	prostřednictvím	k7c2
pravidelně	pravidelně	k6eAd1
prováděných	prováděný	k2eAgFnPc2d1
sportovních	sportovní	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
spravuje	spravovat	k5eAaImIp3nS
a	a	k8xC
provozuje	provozovat	k5eAaImIp3nS
tělovýchovná	tělovýchovný	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
ČVUT	ČVUT	kA
<g/>
:	:	kIx,
sportovní	sportovní	k2eAgFnPc4d1
haly	hala	k1gFnPc4
<g/>
,	,	kIx,
víceúčelové	víceúčelový	k2eAgFnPc4d1
tělocvičny	tělocvična	k1gFnPc4
<g/>
,	,	kIx,
posilovny	posilovna	k1gFnPc4
<g/>
,	,	kIx,
lezecké	lezecký	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
<g/>
,	,	kIx,
lukostřelecké	lukostřelecký	k2eAgFnPc4d1
střelnice	střelnice	k1gFnPc4
<g/>
,	,	kIx,
tenisové	tenisový	k2eAgInPc4d1
kurty	kurt	k1gInPc4
atp.	atp.	kA
</s>
<s>
Ústav	ústav	k1gInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
semestrální	semestrální	k2eAgFnSc4d1
výuku	výuka	k1gFnSc4
TV	TV	kA
na	na	k7c6
fakultách	fakulta	k1gFnPc6
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
organizaci	organizace	k1gFnSc4
zimních	zimní	k2eAgInPc2d1
a	a	k8xC
letních	letní	k2eAgInPc2d1
výcvikových	výcvikový	k2eAgInPc2d1
kurzů	kurz	k1gInPc2
v	v	k7c6
ČR	ČR	kA
i	i	k8xC
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
jednorázové	jednorázový	k2eAgFnPc4d1
sportovní	sportovní	k2eAgFnPc4d1
akce	akce	k1gFnPc4
pro	pro	k7c4
studenty	student	k1gMnPc4
a	a	k8xC
zaměstnance	zaměstnanec	k1gMnPc4
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
sportovní	sportovní	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
ČVUT	ČVUT	kA
na	na	k7c6
sportovních	sportovní	k2eAgInPc6d1
přeborech	přebor	k1gInPc6
VŠ	vš	k0
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
Českých	český	k2eAgFnPc6d1
akademických	akademický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
(	(	kIx(
<g/>
ČAH	ČAH	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mezinárodních	mezinárodní	k2eAgFnPc6d1
sportovních	sportovní	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týdně	týdně	k6eAd1
učí	učit	k5eAaImIp3nS
na	na	k7c4
9	#num#	k4
000	#num#	k4
studentů	student	k1gMnPc2
s	s	k7c7
nabídkou	nabídka	k1gFnSc7
přibližně	přibližně	k6eAd1
40	#num#	k4
různých	různý	k2eAgFnPc2d1
pohybových	pohybový	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
žebříčku	žebříček	k1gInSc2
QS	QS	kA
World	World	k1gMnSc1
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
hodnotil	hodnotit	k5eAaImAgMnS
1604	#num#	k4
univerzit	univerzita	k1gFnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
pro	pro	k7c4
ročník	ročník	k1gInSc4
2021	#num#	k4
ČVUT	ČVUT	kA
umístila	umístit	k5eAaPmAgFnS
v	v	k7c6
celosvětovém	celosvětový	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
na	na	k7c4
432	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
na	na	k7c6
9	#num#	k4
<g/>
.	.	kIx.
pozici	pozice	k1gFnSc6
v	v	k7c6
regionálním	regionální	k2eAgNnSc6d1
hodnocení	hodnocení	k1gNnSc6
„	„	k?
<g/>
Emerging	Emerging	k1gInSc1
Europe	Europ	k1gInSc5
and	and	k?
Central	Central	k1gMnSc1
Asia	Asia	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
hodnocení	hodnocení	k1gNnSc2
pro	pro	k7c4
„	„	k?
<g/>
Engineering	Engineering	k1gInSc4
–	–	k?
Civil	civil	k1gMnSc1
and	and	k?
Structural	Structural	k1gMnSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
ČVUT	ČVUT	kA
mezi	mezi	k7c4
151	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
200	#num#	k4
<g/>
.	.	kIx.
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
v	v	k7c6
oblastech	oblast	k1gFnPc6
„	„	k?
<g/>
Engineering	Engineering	k1gInSc1
–	–	k?
Mechanical	Mechanical	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Engineering	Engineering	k1gInSc1
–	–	k?
Electrical	Electrical	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Physics	Physics	k1gInSc1
and	and	k?
Astronomy	astronom	k1gMnPc7
<g/>
“	“	k?
na	na	k7c6
201	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
250	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
Natural	Natural	k?
Sciences	Sciences	k1gInSc1
<g/>
“	“	k?
jsou	být	k5eAaImIp3nP
na	na	k7c4
271	#num#	k4
<g/>
.	.	kIx.
příčce	příčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblastech	oblast	k1gFnPc6
„	„	k?
<g/>
Computer	computer	k1gInSc1
Science	Science	k1gFnSc1
and	and	k?
Information	Information	k1gInSc1
Systems	Systems	k1gInSc1
<g/>
"	"	kIx"
a	a	k8xC
„	„	k?
<g/>
Material	Material	k1gInSc1
Science	Science	k1gFnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
na	na	k7c4
251	#num#	k4
<g/>
.	.	kIx.
až	až	k9
300	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
„	„	k?
<g/>
Mathematics	Mathematics	k1gInSc1
<g/>
“	“	k?
na	na	k7c4
351	#num#	k4
<g/>
.	.	kIx.
až	až	k9
400	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
„	„	k?
<g/>
Engineering	Engineering	k1gInSc1
and	and	k?
Technology	technolog	k1gMnPc7
<g/>
“	“	k?
je	být	k5eAaImIp3nS
ČVUT	ČVUT	kA
na	na	k7c4
255	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
má	mít	k5eAaImIp3nS
uzavřenu	uzavřen	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
smluv	smlouva	k1gFnPc2
<g/>
,	,	kIx,
buď	buď	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
mezivládních	mezivládní	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
<g/>
,	,	kIx,
či	či	k8xC
na	na	k7c6
principu	princip	k1gInSc6
meziuniverzitní	meziuniverzitní	k2eAgFnSc2d1
výměny	výměna	k1gFnSc2
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zapojeno	zapojit	k5eAaPmNgNnS
do	do	k7c2
celoevropských	celoevropský	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
Erasmus	Erasmus	k1gMnSc1
<g/>
+	+	kIx~
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
Monnet	Monnet	k1gMnSc1
<g/>
,	,	kIx,
CEEPUS	CEEPUS	kA
<g/>
,	,	kIx,
COST	COST	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
Curie	Curie	k1gMnPc2
a	a	k8xC
rozsáhlého	rozsáhlý	k2eAgInSc2d1
vědecko-technického	vědecko-technický	k2eAgInSc2d1
programu	program	k1gInSc2
Horizon	Horizon	k1gNnSc1
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Studium	studium	k1gNnSc1
a	a	k8xC
praxe	praxe	k1gFnSc1
studentů	student	k1gMnPc2
ČVUT	ČVUT	kA
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
</s>
<s>
ČVUT	ČVUT	kA
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
uzavírat	uzavírat	k5eAaImF,k5eAaPmF
smlouvy	smlouva	k1gFnPc4
o	o	k7c6
spolupráci	spolupráce	k1gFnSc6
především	především	k6eAd1
s	s	k7c7
univerzitami	univerzita	k1gFnPc7
z	z	k7c2
první	první	k4xOgFnSc2
pětistovky	pětistovka	k1gFnSc2
mezinárodně	mezinárodně	k6eAd1
uznávaného	uznávaný	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
QS	QS	kA
World	World	k1gMnSc1
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
má	mít	k5eAaImIp3nS
platné	platný	k2eAgFnPc4d1
dvoustranné	dvoustranný	k2eAgFnPc4d1
dohody	dohoda	k1gFnPc4
s	s	k7c7
84	#num#	k4
zahraničními	zahraniční	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
9	#num#	k4
je	být	k5eAaImIp3nS
z	z	k7c2
prestižní	prestižní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
univerzit	univerzita	k1gFnPc2
do	do	k7c2
stého	stý	k4xOgNnSc2
místa	místo	k1gNnSc2
v	v	k7c6
žebříčku	žebříček	k1gInSc6
(	(	kIx(
<g/>
Nanyang	Nanyang	k1gInSc1
Technological	Technological	k1gFnSc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
Purdue	Purdu	k1gFnSc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
Politecnico	Politecnico	k6eAd1
di	di	k?
Milano	Milana	k1gFnSc5
<g/>
,	,	kIx,
Katolická	katolický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Lovani	Lovaň	k1gFnSc6
<g/>
,	,	kIx,
TU	tu	k6eAd1
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
TU	tu	k6eAd1
Delft	Delft	k1gInSc1
<g/>
,	,	kIx,
TU	tu	k6eAd1
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
KTH	KTH	kA
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
National	National	k1gMnSc1
University	universita	k1gFnSc2
of	of	k?
Singapore	Singapor	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
ČVUT	ČVUT	kA
podařilo	podařit	k5eAaPmAgNnS
navázat	navázat	k5eAaPmF
kontakty	kontakt	k1gInPc4
a	a	k8xC
následně	následně	k6eAd1
uzavřít	uzavřít	k5eAaPmF
bilaterální	bilaterální	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
například	například	k6eAd1
s	s	k7c7
izraelským	izraelský	k2eAgInSc7d1
TECHNIONEM	TECHNIONEM	kA
(	(	kIx(
<g/>
Israel	Israel	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
<g/>
)	)	kIx)
z	z	k7c2
Haify	Haifa	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
jihoafrickou	jihoafrický	k2eAgFnSc7d1
Stellenbosch	Stellenbosch	k1gInSc1
University	universita	k1gFnPc1
<g/>
,	,	kIx,
s	s	k7c7
australskými	australský	k2eAgInPc7d1
University	universita	k1gFnSc2
of	of	k?
South	South	k1gInSc1
Australia	Australius	k1gMnSc2
v	v	k7c6
Adelaide	Adelaid	k1gInSc5
a	a	k8xC
Queensland	Queensland	k1gInSc1
University	universita	k1gFnSc2
of	of	k?
Technology	technolog	k1gMnPc4
v	v	k7c6
Brisbane	Brisban	k1gMnSc5
<g/>
,	,	kIx,
s	s	k7c7
korejskými	korejský	k2eAgMnPc7d1
Kyungpook	Kyungpook	k1gInSc4
National	National	k1gFnPc1
University	universita	k1gFnSc2
v	v	k7c6
Tegu	Tegus	k1gInSc6
a	a	k8xC
se	se	k3xPyFc4
univerzitou	univerzita	k1gFnSc7
Sŏ	Sŏ	k1gInSc1
v	v	k7c6
Soulu	Soul	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
bilaterálních	bilaterální	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
probíhají	probíhat	k5eAaImIp3nP
výměny	výměna	k1gFnPc1
zejména	zejména	k9
s	s	k7c7
mimoevropskými	mimoevropský	k2eAgFnPc7d1
univerzitami	univerzita	k1gFnPc7
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
<g/>
,	,	kIx,
Střední	střední	k2eAgFnSc6d1
i	i	k8xC
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
Asii	Asie	k1gFnSc6
a	a	k8xC
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
zájem	zájem	k1gInSc1
studentů	student	k1gMnPc2
je	být	k5eAaImIp3nS
o	o	k7c4
země	zem	k1gFnPc4
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Singapur	Singapur	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
a	a	k8xC
Japonsko	Japonsko	k1gNnSc1
(	(	kIx(
<g/>
seřazeno	seřazen	k2eAgNnSc1d1
od	od	k7c2
Zemí	zem	k1gFnPc2
o	o	k7c4
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc1d3
zájem	zájem	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
vyjíždějí	vyjíždět	k5eAaImIp3nP
studenti	student	k1gMnPc1
rovněž	rovněž	k9
do	do	k7c2
dalších	další	k2eAgFnPc2d1
atraktivních	atraktivní	k2eAgFnPc2d1
destinací	destinace	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Hong	Hong	k1gMnSc1
Kong	Kongo	k1gNnPc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Indonésie	Indonésie	k1gFnSc1
<g/>
,	,	kIx,
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
Kostarika	Kostarika	k1gFnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc1
<g/>
,	,	kIx,
Ruska	Ruska	k1gFnSc1
a	a	k8xC
Tchaj-wan	Tchaj-wan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
Erasmus	Erasmus	k1gMnSc1
směřuje	směřovat	k5eAaImIp3nS
přes	přes	k7c4
50	#num#	k4
%	%	kIx~
studentů	student	k1gMnPc2
na	na	k7c4
studijní	studijní	k2eAgInPc4d1
pobyty	pobyt	k1gInPc4
do	do	k7c2
4	#num#	k4
zemí	zem	k1gFnPc2
–	–	k?
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
odkud	odkud	k6eAd1
také	také	k9
do	do	k7c2
Česka	Česko	k1gNnSc2
přijíždí	přijíždět	k5eAaImIp3nS
nejvíce	hodně	k6eAd3,k6eAd1
studentů	student	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
kolem	kolem	k7c2
700	#num#	k4
studentů	student	k1gMnPc2
ČVUT	ČVUT	kA
pobývá	pobývat	k5eAaImIp3nS
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
semestry	semestr	k1gInPc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
na	na	k7c6
partnerských	partnerský	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Dvojí	dvojí	k4xRgInSc4
diplom	diplom	k1gInSc4
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
má	mít	k5eAaImIp3nS
ČVUT	ČVUT	kA
21	#num#	k4
platných	platný	k2eAgFnPc2d1
dohod	dohoda	k1gFnPc2
o	o	k7c6
společných	společný	k2eAgInPc6d1
diplomech	diplom	k1gInPc6
<g/>
,	,	kIx,
především	především	k6eAd1
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
stavební	stavební	k2eAgFnSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
studenti	student	k1gMnPc1
mohou	moct	k5eAaImIp3nP
přihlásit	přihlásit	k5eAaPmF
do	do	k7c2
pěti	pět	k4xCc2
společných	společný	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
čtyři	čtyři	k4xCgInPc4
programy	program	k1gInPc4
na	na	k7c4
získání	získání	k1gNnSc4
společného	společný	k2eAgInSc2d1
diplomu	diplom	k1gInSc2
nabízí	nabízet	k5eAaImIp3nS
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
<g/>
,	,	kIx,
po	po	k7c6
dvou	dva	k4xCgInPc6
programech	program	k1gInPc6
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc1d1
a	a	k8xC
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
program	program	k1gInSc4
získala	získat	k5eAaPmAgFnS
Fakulta	fakulta	k1gFnSc1
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
partnerských	partnerský	k2eAgFnPc2d1
zahraničních	zahraniční	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
jmenujme	jmenovat	k5eAaBmRp1nP,k5eAaImRp1nP
alespoň	alespoň	k9
ty	ten	k3xDgMnPc4
nejvýznamnější	významný	k2eAgMnPc4d3
<g/>
:	:	kIx,
Padovská	padovský	k2eAgFnSc5d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Katalánská	katalánský	k2eAgNnPc4d1
polytechnika	polytechnikum	k1gNnPc4
<g/>
,	,	kIx,
École	École	k1gFnPc4
Nationale	Nationale	k1gFnSc2
des	des	k1gNnSc2
Ponts	Pontsa	k1gFnPc2
et	et	k?
Chaussées	Chausséesa	k1gFnPc2
<g/>
,	,	kIx,
École	Écol	k1gMnSc5
Centrale	Central	k1gMnSc5
de	de	k?
Nantes	Nantesa	k1gFnPc2
<g/>
,	,	kIx,
RWTH	RWTH	kA
Aachen	Aachen	k1gInSc4
<g/>
,	,	kIx,
TU	tu	k6eAd1
Mnichov	Mnichov	k1gInSc4
<g/>
,	,	kIx,
Trinity	Trinit	k2eAgFnPc4d1
College	Colleg	k1gFnPc4
Dublin	Dublin	k1gInSc4
<g/>
,	,	kIx,
Universita	universita	k1gFnSc1
v	v	k7c6
Coimbře	Coimbra	k1gFnSc6
<g/>
,	,	kIx,
Tomská	Tomský	k2eAgNnPc4d1
polytechnická	polytechnický	k2eAgNnPc4d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
University	universita	k1gFnPc4
of	of	k?
Linköping	Linköping	k1gInSc4
<g/>
,	,	kIx,
University	universita	k1gFnSc2
of	of	k?
Texas	Texas	kA
at	at	k?
El	Ela	k1gFnPc2
Paso	Paso	k6eAd1
<g/>
,	,	kIx,
National	National	k1gFnSc3
Tsing	Tsinga	k1gFnPc2
Hua	Hua	k1gFnSc2
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgMnPc1d1
studenti	student	k1gMnPc1
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
nabízí	nabízet	k5eAaImIp3nS
44	#num#	k4
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
a	a	k8xC
ruském	ruský	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
bakalářském	bakalářský	k2eAgNnSc6d1
<g/>
,	,	kIx,
magisterském	magisterský	k2eAgNnSc6d1
i	i	k8xC
doktorském	doktorský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
svůj	svůj	k3xOyFgInSc1
akademický	akademický	k2eAgInSc1d1
život	život	k1gInSc1
na	na	k7c6
ČVUT	ČVUT	kA
tráví	trávit	k5eAaImIp3nP
více	hodně	k6eAd2
než	než	k8xS
3900	#num#	k4
zahraničních	zahraniční	k2eAgMnPc2d1
studentů	student	k1gMnPc2
ze	z	k7c2
108	#num#	k4
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
kolem	kolem	k6eAd1
1000	#num#	k4
jich	on	k3xPp3gInPc2
každý	každý	k3xTgInSc1
rok	rok	k1gInSc4
přijíždí	přijíždět	k5eAaImIp3nS
na	na	k7c4
výměnné	výměnný	k2eAgInPc4d1
programy	program	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
statistik	statistika	k1gFnPc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
čtvrtý	čtvrtý	k4xOgMnSc1
student	student	k1gMnSc1
ČVUT	ČVUT	kA
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
narostl	narůst	k5eAaPmAgInS
počet	počet	k1gInSc1
studentů	student	k1gMnPc2
samoplátců	samoplátce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
jejich	jejich	k3xOp3gFnSc4
počet	počet	k1gInSc1
převýšil	převýšit	k5eAaPmAgInS
600	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
80	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgNnSc4d3
zastoupení	zastoupení	k1gNnSc4
mají	mít	k5eAaImIp3nP
studenti	student	k1gMnPc1
z	z	k7c2
Indie	Indie	k1gFnSc2
(	(	kIx(
<g/>
131	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
(	(	kIx(
<g/>
103	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Turecka	Turecko	k1gNnSc2
(	(	kIx(
<g/>
42	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
(	(	kIx(
<g/>
32	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Kazachstánu	Kazachstán	k1gInSc2
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
z	z	k7c2
nich	on	k3xPp3gInPc2
studuje	studovat	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
strojní	strojní	k2eAgNnSc1d1
<g/>
,	,	kIx,
Fakultě	fakulta	k1gFnSc6
Elektrotechnické	elektrotechnický	k2eAgFnSc2d1
a	a	k8xC
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
informačních	informační	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studium	studium	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
malých	malý	k2eAgFnPc6d1
studijních	studijní	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
individuálnímu	individuální	k2eAgInSc3d1
přístupu	přístup	k1gInSc3
ke	k	k7c3
studentům	student	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
studentů	student	k1gMnPc2
pro	pro	k7c4
studium	studium	k1gNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
jsou	být	k5eAaImIp3nP
relativně	relativně	k6eAd1
nižší	nízký	k2eAgInPc1d2
náklady	náklad	k1gInPc1
na	na	k7c4
život	život	k1gInSc4
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
kvalitní	kvalitní	k2eAgFnSc7d1
výukou	výuka	k1gFnSc7
a	a	k8xC
výzkumem	výzkum	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
uplatnění	uplatnění	k1gNnSc1
na	na	k7c6
trhu	trh	k1gInSc6
práce	práce	k1gFnSc2
po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
absolvování	absolvování	k1gNnSc6
daného	daný	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
úspěšnými	úspěšný	k2eAgFnPc7d1
náborovými	náborový	k2eAgFnPc7d1
aktivitami	aktivita	k1gFnPc7
stojí	stát	k5eAaImIp3nS
především	především	k6eAd1
centralizovaný	centralizovaný	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Study	stud	k1gInPc1
in	in	k?
Prague	Pragu	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
propagaci	propagace	k1gFnSc4
pražských	pražský	k2eAgFnPc2d1
veřejných	veřejný	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
programy	program	k1gInPc1
v	v	k7c6
cizích	cizí	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2012	#num#	k4
ČVUT	ČVUT	kA
spustilo	spustit	k5eAaPmAgNnS
web	web	k1gInSc4
studyatctu	studyatct	k1gInSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
uchazeči	uchazeč	k1gMnPc1
najdou	najít	k5eAaPmIp3nP
kompletní	kompletní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájemci	zájemce	k1gMnPc1
o	o	k7c4
studium	studium	k1gNnSc4
na	na	k7c6
ČVUT	ČVUT	kA
je	být	k5eAaImIp3nS
poskytnuta	poskytnut	k2eAgFnSc1d1
veškerá	veškerý	k3xTgFnSc1
péče	péče	k1gFnSc1
od	od	k7c2
prvních	první	k4xOgInPc2
kroků	krok	k1gInPc2
nejen	nejen	k6eAd1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
studijních	studijní	k2eAgFnPc2d1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
problémů	problém	k1gInPc2
souvisejících	související	k2eAgInPc2d1
se	s	k7c7
životem	život	k1gInSc7
v	v	k7c6
cizí	cizí	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vždy	vždy	k6eAd1
aktuálních	aktuální	k2eAgFnPc2d1
informací	informace	k1gFnPc2
o	o	k7c6
možnostech	možnost	k1gFnPc6
studia	studio	k1gNnSc2
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
,	,	kIx,
procesu	proces	k1gInSc2
výběrového	výběrový	k2eAgNnSc2d1
konání	konání	k1gNnSc2
<g/>
,	,	kIx,
získaní	získaný	k2eAgMnPc1d1
víz	vízo	k1gNnPc2
<g/>
,	,	kIx,
nostrifikace	nostrifikace	k1gFnSc1
předešlého	předešlý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
ubytování	ubytování	k1gNnSc2
<g/>
,	,	kIx,
informace	informace	k1gFnPc4
o	o	k7c6
životních	životní	k2eAgInPc6d1
nákladech	náklad	k1gInPc6
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
kulturní	kulturní	k2eAgInSc4d1
život	život	k1gInSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
možnosti	možnost	k1gFnSc6
cestování	cestování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
svými	svůj	k3xOyFgInPc7
dotazy	dotaz	k1gInPc7
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
obrátit	obrátit	k5eAaPmF
na	na	k7c4
tzv.	tzv.	kA
Student	student	k1gMnSc1
Advisora	Advisor	k1gMnSc2
na	na	k7c4
e-mail	e-mail	k1gInSc4
<g/>
,	,	kIx,
Skype	Skyp	k1gInSc5
nebo	nebo	k8xC
na	na	k7c6
Facebooku	Facebook	k1gInSc6
prakticky	prakticky	k6eAd1
nonstop	nonstop	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
pozitivní	pozitivní	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
se	s	k7c7
získáváním	získávání	k1gNnSc7
zahraničních	zahraniční	k2eAgInPc2d1
studentů-samoplátců	studentů-samoplátec	k1gInPc2
navazuje	navazovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
projekt	projekt	k1gInSc1
Study	stud	k1gInPc1
in	in	k?
Prague	Pragu	k1gInSc2
<g/>
,	,	kIx,
koordinovaný	koordinovaný	k2eAgMnSc1d1
prostřednictvím	prostřednictvím	k7c2
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
sdružující	sdružující	k2eAgFnSc1d1
pět	pět	k4xCc1
významných	významný	k2eAgFnPc2d1
pražských	pražský	k2eAgFnPc2d1
veřejných	veřejný	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
ČVUT	ČVUT	kA
jsou	být	k5eAaImIp3nP
členy	člen	k1gMnPc7
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
VŠCHT	VŠCHT	kA
<g/>
,	,	kIx,
VŠE	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
a	a	k8xC
ČZU	ČZU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
představit	představit	k5eAaPmF
Prahu	Praha	k1gFnSc4
jako	jako	k8xC,k8xS
atraktivní	atraktivní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
ke	k	k7c3
studiu	studio	k1gNnSc3
<g/>
,	,	kIx,
nabízející	nabízející	k2eAgFnSc4d1
úplnou	úplný	k2eAgFnSc4d1
škálu	škála	k1gFnSc4
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
v	v	k7c6
angličtině	angličtina	k1gFnSc6
a	a	k8xC
pestrý	pestrý	k2eAgInSc1d1
studentský	studentský	k2eAgInSc1d1
život	život	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
dalšímů	dalším	k1gMnPc2
nárůstu	nárůst	k1gInSc2
studentů-samoplátců	studentů-samoplátec	k1gMnPc2
na	na	k7c6
všech	všecek	k3xTgFnPc6
zapojených	zapojený	k2eAgFnPc6d1
univerzitách	univerzita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
péči	péče	k1gFnSc6
o	o	k7c4
zahraniční	zahraniční	k2eAgMnPc4d1
studenty	student	k1gMnPc4
sehrává	sehrávat	k5eAaImIp3nS
na	na	k7c6
škole	škola	k1gFnSc6
působící	působící	k2eAgFnSc2d1
International	International	k1gFnSc2
Student	student	k1gMnSc1
Club	club	k1gInSc1
(	(	kIx(
<g/>
ISC	ISC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
například	například	k6eAd1
organizuje	organizovat	k5eAaBmIp3nS
tzv.	tzv.	kA
buddy	budda	k1gFnSc2
program	program	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
čeští	český	k2eAgMnPc1d1
studenti	student	k1gMnPc1
pomáhají	pomáhat	k5eAaImIp3nP
studentům	student	k1gMnPc3
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
překonávat	překonávat	k5eAaImF
počáteční	počáteční	k2eAgInSc4d1
kulturní	kulturní	k2eAgInSc4d1
rozdíly	rozdíl	k1gInPc4
prostřednictvím	prostřednictvím	k7c2
pestré	pestrý	k2eAgFnSc2d1
nabídky	nabídka	k1gFnSc2
mimoškolních	mimoškolní	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
(	(	kIx(
<g/>
mj.	mj.	kA
prezentace	prezentace	k1gFnSc2
zahraničních	zahraniční	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
jazykové	jazykový	k2eAgInPc4d1
kurzy	kurz	k1gInPc4
typu	typ	k1gInSc2
student	student	k1gMnSc1
učí	učit	k5eAaImIp3nS
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
exkurze	exkurze	k1gFnSc1
<g/>
,	,	kIx,
sport	sport	k1gInSc1
a	a	k8xC
výlety	výlet	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
pomáhají	pomáhat	k5eAaImIp3nP
českým	český	k2eAgMnPc3d1
studentům	student	k1gMnPc3
ke	k	k7c3
zvyšování	zvyšování	k1gNnSc3
jazykových	jazykový	k2eAgFnPc2d1
znalostí	znalost	k1gFnPc2
a	a	k8xC
získávání	získávání	k1gNnSc1
zahraničních	zahraniční	k2eAgMnPc2d1
přátel	přítel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výsledkem	výsledek	k1gInSc7
dobré	dobrý	k2eAgFnSc2d1
práce	práce	k1gFnSc2
se	s	k7c7
zahraničními	zahraniční	k2eAgMnPc7d1
studenty	student	k1gMnPc7
je	být	k5eAaImIp3nS
i	i	k9
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
většina	většina	k1gFnSc1
dotázaných	dotázaný	k2eAgMnPc2d1
studentů	student	k1gMnPc2
samoplátců	samoplátce	k1gMnPc2
(	(	kIx(
<g/>
80	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
označuje	označovat	k5eAaImIp3nS
ČVUT	ČVUT	kA
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc4
alternativu	alternativa	k1gFnSc4
při	při	k7c6
svém	svůj	k3xOyFgInSc6
výběru	výběr	k1gInSc6
univerzitního	univerzitní	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ubytování	ubytování	k1gNnSc1
a	a	k8xC
stravování	stravování	k1gNnSc1
studentů	student	k1gMnPc2
</s>
<s>
Škola	škola	k1gFnSc1
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgNnSc4d1
stravovací	stravovací	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
(	(	kIx(
<g/>
menzy	menza	k1gFnSc2
<g/>
)	)	kIx)
i	i	k9
ubytovací	ubytovací	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
(	(	kIx(
<g/>
koleje	kolej	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
celkem	celkem	k6eAd1
8	#num#	k4
000	#num#	k4
lůžek	lůžko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menzy	menza	k1gFnPc1
a	a	k8xC
koleje	kolej	k1gFnPc1
<g/>
,	,	kIx,
i	i	k9
včetně	včetně	k7c2
sportovišť	sportoviště	k1gNnPc2
<g/>
,	,	kIx,
spravuje	spravovat	k5eAaImIp3nS
Správa	správa	k1gFnSc1
účelových	účelový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
SÚZ	SÚZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
Dejvicích	Dejvice	k1gFnPc6
</s>
<s>
Koleje	kolej	k1gFnPc1
ČVUT	ČVUT	kA
</s>
<s>
Strahov	Strahov	k1gInSc1
(	(	kIx(
<g/>
Břevnov	Břevnov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Podolí	Podolí	k1gNnSc1
</s>
<s>
Bubeneč	Bubeneč	k1gFnSc1
</s>
<s>
Dejvická	dejvický	k2eAgFnSc1d1
(	(	kIx(
<g/>
Dejvice	Dejvice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Orlík	Orlík	k1gInSc1
(	(	kIx(
<g/>
Bubeneč	Bubeneč	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hlávkova	Hlávkův	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
(	(	kIx(
<g/>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Sinkuleho	Sinkuleze	k6eAd1
(	(	kIx(
<g/>
Dejvice	Dejvice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
(	(	kIx(
<g/>
Dejvice	Dejvice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
ubytovací	ubytovací	k2eAgInPc1d1
provozy	provoz	k1gInPc1
</s>
<s>
Novoměstský	novoměstský	k2eAgInSc1d1
hotel	hotel	k1gInSc1
(	(	kIx(
<g/>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
;	;	kIx,
web	web	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Menzy	menza	k1gFnPc1
ČVUT	ČVUT	kA
</s>
<s>
Studentský	studentský	k2eAgInSc4d1
dům	dům	k1gInSc4
(	(	kIx(
<g/>
kampus	kampus	k1gInSc4
Dejvice	Dejvice	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Technická	technický	k2eAgFnSc1d1
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
CIIRC	CIIRC	kA
<g/>
)	)	kIx)
</s>
<s>
Strahovská	strahovský	k2eAgFnSc1d1
(	(	kIx(
<g/>
na	na	k7c6
kolejích	kolej	k1gFnPc6
Strahov	Strahov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Podolská	podolský	k2eAgFnSc1d1
(	(	kIx(
<g/>
na	na	k7c6
kolejích	kolej	k1gFnPc6
v	v	k7c6
Podolí	Podolí	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
kolej	kolej	k1gFnSc1
(	(	kIx(
<g/>
Dejvice	Dejvice	k1gFnPc1
<g/>
,	,	kIx,
Thákurova	Thákurův	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Výdejna	výdejna	k1gFnSc1
Karlovo	Karlův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
(	(	kIx(
<g/>
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Menza	menza	k1gFnSc1
Kladno	Kladno	k1gNnSc4
(	(	kIx(
<g/>
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Výdejna	výdejna	k1gFnSc1
Horská	horský	k2eAgFnSc1d1
(	(	kIx(
<g/>
na	na	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
Albertově	Albertův	k2eAgMnSc6d1
<g/>
)	)	kIx)
</s>
<s>
Pizzerie	pizzerie	k1gFnSc1
La	la	k1gNnSc2
Fontanella	Fontanello	k1gNnSc2
(	(	kIx(
<g/>
kampus	kampus	k1gInSc4
Dejvice	Dejvice	k1gFnPc4
<g/>
)	)	kIx)
</s>
<s>
Jídelníček	jídelníček	k1gInSc1
Menz	menza	k1gFnPc2
ČVUT	ČVUT	kA
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1
jídelny	jídelna	k1gFnPc1
nabízejí	nabízet	k5eAaImIp3nP
denně	denně	k6eAd1
široký	široký	k2eAgInSc4d1
výběr	výběr	k1gInSc4
pokrmů	pokrm	k1gInPc2
<g/>
.	.	kIx.
<g/>
Ty	ty	k3xPp2nSc1
lze	lze	k6eAd1
vyhledat	vyhledat	k5eAaPmF
na	na	k7c6
následujícím	následující	k2eAgInSc6d1
odkazu	odkaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
stravovací	stravovací	k2eAgInPc1d1
provozy	provoz	k1gInPc1
</s>
<s>
MEGA	mega	k1gNnSc1
Buf	bufa	k1gFnPc2
Fat	fatum	k1gNnPc2
(	(	kIx(
<g/>
Fakulta	fakulta	k1gFnSc1
stavební	stavební	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Snack	Snack	k1gInSc1
Bar	bar	k1gInSc1
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
SÚZ	SÚZ	kA
<g/>
,	,	kIx,
kolej	kolej	k1gFnSc1
Strahov	Strahov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ArchiCafé	ArchiCafé	k1gNnSc1
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
FA	fa	kA
<g/>
,	,	kIx,
Dejvice	Dejvice	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Studentské	studentský	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
</s>
<s>
Studentské	studentský	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
jsou	být	k5eAaImIp3nP
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
vysokoškolského	vysokoškolský	k2eAgInSc2d1
studentského	studentský	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
zájem	zájem	k1gInSc4
o	o	k7c4
ně	on	k3xPp3gInPc4
se	se	k3xPyFc4
mezi	mezi	k7c7
studenty	student	k1gMnPc7
rok	rok	k1gInSc4
od	od	k7c2
roku	rok	k1gInSc2
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivity	aktivita	k1gFnSc2
studentských	studentský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
rozmanité	rozmanitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
od	od	k7c2
kulturních	kulturní	k2eAgInPc2d1
a	a	k8xC
sportovních	sportovní	k2eAgInPc2d1
<g/>
,	,	kIx,
přes	přes	k7c4
odborné	odborný	k2eAgInPc4d1
až	až	k9
po	po	k7c4
mezinárodní	mezinárodní	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
studentských	studentský	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
a	a	k8xC
klubů	klub	k1gInPc2
vykonávají	vykonávat	k5eAaImIp3nP
mnohdy	mnohdy	k6eAd1
vysoce	vysoce	k6eAd1
kvalifikované	kvalifikovaný	k2eAgFnPc1d1
činnosti	činnost	k1gFnPc1
spojené	spojený	k2eAgFnPc1d1
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
provozem	provoz	k1gInSc7
<g/>
,	,	kIx,
či	či	k8xC
s	s	k7c7
jejich	jejich	k3xOp3gFnSc7
vlastní	vlastní	k2eAgFnSc7d1
náplní	náplň	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zpravidla	zpravidla	k6eAd1
bez	bez	k7c2
nároků	nárok	k1gInPc2
na	na	k7c4
honorář	honorář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ČVUT	ČVUT	kA
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
SU	SU	k?
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
vznikala	vznikat	k5eAaImAgFnS
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
samotných	samotný	k2eAgMnPc2d1
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
z	z	k7c2
Akademického	akademický	k2eAgInSc2d1
senátu	senát	k1gInSc2
ČVUT	ČVUT	kA
pod	pod	k7c7
vedením	vedení	k1gNnSc7
tehdejšího	tehdejší	k2eAgMnSc2d1
místopředsedy	místopředseda	k1gMnSc2
AS	as	k1gNnSc2
ČVUT	ČVUT	kA
za	za	k7c7
studenty	student	k1gMnPc7
Jaroslava	Jaroslav	k1gMnSc2
Kuby	Kuba	k1gMnSc2
za	za	k7c2
pomoci	pomoc	k1gFnSc2
řady	řada	k1gFnSc2
dalších	další	k2eAgMnPc2d1
studentů	student	k1gMnPc2
postupně	postupně	k6eAd1
od	od	k7c2
října	říjen	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
centrálou	centrála	k1gFnSc7
<g/>
,	,	kIx,
kontrolním	kontrolní	k2eAgInSc7d1
výborem	výbor	k1gInSc7
<g/>
,	,	kIx,
kolejními	kolejní	k2eAgInPc7d1
<g/>
,	,	kIx,
zájmovými	zájmový	k2eAgInPc7d1
a	a	k8xC
externími	externí	k2eAgInPc7d1
kluby	klub	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodování	rozhodování	k1gNnSc1
o	o	k7c6
budoucnosti	budoucnost	k1gFnSc6
SU	SU	k?
ČVUT	ČVUT	kA
má	mít	k5eAaImIp3nS
na	na	k7c6
starosti	starost	k1gFnSc6
parlament	parlament	k1gInSc1
Studentské	studentský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
patnácti	patnáct	k4xCc7
delegáty	delegát	k1gMnPc4
z	z	k7c2
řad	řada	k1gFnPc2
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
delegátem	delegát	k1gMnSc7
za	za	k7c4
Akademický	akademický	k2eAgInSc4d1
senát	senát	k1gInSc4
ČVUT	ČVUT	kA
a	a	k8xC
presidentem	president	k1gMnSc7
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SU	SU	k?
ČVUT	ČVUT	kA
zastupuje	zastupovat	k5eAaImIp3nS
studenty	student	k1gMnPc4
před	před	k7c7
univerzitou	univerzita	k1gFnSc7
a	a	k8xC
Správou	správa	k1gFnSc7
účelových	účelový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
organizuje	organizovat	k5eAaBmIp3nS
společenské	společenský	k2eAgFnPc4d1
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgFnPc4d1
a	a	k8xC
sportovní	sportovní	k2eAgFnPc4d1
akce	akce	k1gFnPc4
a	a	k8xC
podílí	podílet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
reprezentaci	reprezentace	k1gFnSc4
univerzity	univerzita	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
veřejných	veřejný	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ČVUT	ČVUT	kA
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Informovanost	informovanost	k1gFnSc1
</s>
<s>
Průvodce	průvodce	k1gMnSc1
prváka	prvák	k1gMnSc2
po	po	k7c6
ČVUT	ČVUT	kA
–	–	k?
studentův	studentův	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
životem	život	k1gInSc7
na	na	k7c4
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
v	v	k7c6
elektronické	elektronický	k2eAgFnSc6d1
i	i	k8xC
tištěné	tištěný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Akce	akce	k1gFnSc1
prvák	prvák	k1gInSc1
–	–	k?
součást	součást	k1gFnSc1
Ahoj	ahoj	k0
techniko	technika	k1gFnSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
usnadnění	usnadnění	k1gNnSc4
studia	studio	k1gNnSc2
na	na	k7c4
vysoké	vysoký	k2eAgFnPc4d1
školy	škola	k1gFnPc4
pro	pro	k7c4
prváky	prvák	k1gMnPc4
</s>
<s>
webový	webový	k2eAgInSc1d1
portál	portál	k1gInSc1
Student	student	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cvut	cvut	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
podporu	podpora	k1gFnSc4
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
pobavení	pobavení	k1gNnSc4
studentů	student	k1gMnPc2
a	a	k8xC
informuje	informovat	k5eAaBmIp3nS
o	o	k7c6
všech	všecek	k3xTgFnPc6
akcích	akce	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
na	na	k7c4
ČVUT	ČVUT	kA
pořádají	pořádat	k5eAaImIp3nP
</s>
<s>
Zábavu	zábava	k1gFnSc4
</s>
<s>
Strahov	Strahov	k1gInSc1
Open	Opena	k1gFnPc2
Air	Air	k1gFnSc2
–	–	k?
součást	součást	k1gFnSc1
Ahoj	ahoj	k0
techniko	technika	k1gFnSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
hudební	hudební	k2eAgInSc1d1
festival	festival	k1gInSc1
pod	pod	k7c7
širým	širý	k2eAgNnSc7d1
nebem	nebe	k1gNnSc7
</s>
<s>
Studentský	studentský	k2eAgInSc1d1
ples	ples	k1gInSc1
–	–	k?
maškarní	maškarní	k2eAgInSc1d1
ples	ples	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
v	v	k7c6
prostorách	prostora	k1gFnPc6
Masarykovy	Masarykův	k2eAgFnSc2d1
koleje	kolej	k1gFnSc2
</s>
<s>
Mezi	mezi	k7c4
bloky	blok	k1gInPc4
–	–	k?
hudební	hudební	k2eAgInSc1d1
festival	festival	k1gInSc1
a	a	k8xC
největší	veliký	k2eAgFnSc1d3
pravidelná	pravidelný	k2eAgFnSc1d1
akce	akce	k1gFnSc1
pořádaná	pořádaný	k2eAgFnSc1d1
na	na	k7c6
Praze	Praha	k1gFnSc6
4	#num#	k4
</s>
<s>
MayDej	MayDej	k1gInSc1
–	–	k?
sportovně	sportovně	k6eAd1
společenský	společenský	k2eAgInSc4d1
den	den	k1gInSc4
plný	plný	k2eAgInSc4d1
zábavy	zábav	k1gInPc7
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1
večírek	večírek	k1gInSc1
–	–	k?
společenská	společenský	k2eAgFnSc1d1
akce	akce	k1gFnSc1
pořádaná	pořádaný	k2eAgFnSc1d1
před	před	k7c7
vánočními	vánoční	k2eAgInPc7d1
svátky	svátek	k1gInPc7
</s>
<s>
Volný	volný	k2eAgInSc1d1
čas	čas	k1gInSc1
</s>
<s>
Čajovna	čajovna	k1gFnSc1
–	–	k?
příjemné	příjemný	k2eAgNnSc4d1
posezení	posezení	k1gNnSc4
a	a	k8xC
široká	široký	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
nejrůznějších	různý	k2eAgInPc2d3
druhů	druh	k1gInPc2
čajů	čaj	k1gInPc2
</s>
<s>
Klub	klub	k1gInSc1
STOlních	stolní	k2eAgFnPc2d1
Her	hra	k1gFnPc2
(	(	kIx(
<g/>
STOH	stoh	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
hráčské	hráčský	k2eAgInPc1d1
večery	večer	k1gInPc1
pro	pro	k7c4
každého	každý	k3xTgMnSc4
a	a	k8xC
turnaje	turnaj	k1gInPc4
o	o	k7c4
zajímavé	zajímavý	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
</s>
<s>
Křesťani	Křesťan	k1gMnPc1
–	–	k?
podpora	podpora	k1gFnSc1
duchovního	duchovní	k2eAgInSc2d1
a	a	k8xC
osobního	osobní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
GALIBI	GALIBI	kA
–	–	k?
vysokoškolsky	vysokoškolsky	k6eAd1
gay-lesbický	gay-lesbický	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
posilovny	posilovna	k1gFnPc1
a	a	k8xC
sportoviště	sportoviště	k1gNnSc1
–	–	k?
na	na	k7c6
kolejích	kolej	k1gFnPc6
</s>
<s>
Tralalalala	Tralalalat	k5eAaPmAgFnS,k5eAaImAgFnS
–	–	k?
spolek	spolek	k1gInSc1
sportovních	sportovní	k2eAgInPc2d1
týmů	tým	k1gInPc2
</s>
<s>
Vzdělávání	vzdělávání	k1gNnSc1
</s>
<s>
Cisco	Cisco	k6eAd1
Networking	Networking	k1gInSc1
Academy	Academa	k1gFnSc2
–	–	k?
čtyř	čtyři	k4xCgMnPc2
semestrální	semestrální	k2eAgInSc1d1
vzdělávací	vzdělávací	k2eAgInSc1d1
program	program	k1gInSc1
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
síťové	síťový	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
</s>
<s>
Středisko	středisko	k1gNnSc1
UNIXových	unixový	k2eAgFnPc2d1
Technologií	technologie	k1gFnPc2
(	(	kIx(
<g/>
SUT	sut	k2eAgInSc1d1
<g/>
)	)	kIx)
–	–	k?
semináře	seminář	k1gInPc1
o	o	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
a	a	k8xC
svobodném	svobodný	k2eAgInSc6d1
softwaru	software	k1gInSc6
jako	jako	k8xS,k8xC
takovém	takový	k3xDgInSc6
</s>
<s>
InstallFest	InstallFest	k1gFnSc1
–	–	k?
akce	akce	k1gFnSc2
pro	pro	k7c4
zájemce	zájemce	k1gMnPc4
o	o	k7c4
operační	operační	k2eAgInSc4d1
systém	systém	k1gInSc4
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
</s>
<s>
Podporu	podpora	k1gFnSc4
</s>
<s>
Buddy	Buddy	k6eAd1
program	program	k1gInSc1
–	–	k?
přivítání	přivítání	k1gNnSc1
a	a	k8xC
péče	péče	k1gFnSc1
o	o	k7c4
zahraniční	zahraniční	k2eAgMnPc4d1
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
doprovázeno	doprovázen	k2eAgNnSc1d1
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
akcí	akce	k1gFnPc2
</s>
<s>
koleje	kolej	k1gFnPc1
–	–	k?
jednání	jednání	k1gNnSc1
s	s	k7c7
vedením	vedení	k1gNnSc7
Správy	správa	k1gFnSc2
účelových	účelový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
ČVUT	ČVUT	kA
</s>
<s>
tiskárny	tiskárna	k1gFnPc1
</s>
<s>
síťové	síťový	k2eAgFnPc1d1
služby	služba	k1gFnPc1
</s>
<s>
studovny	studovna	k1gFnPc1
<g/>
,	,	kIx,
společenské	společenský	k2eAgFnPc1d1
místnosti	místnost	k1gFnPc1
</s>
<s>
Veletrh	veletrh	k1gInSc1
iKariéra	iKariér	k1gMnSc2
–	–	k?
veletrh	veletrh	k1gInSc1
pro	pro	k7c4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
mají	mít	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
potkat	potkat	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
firmami	firma	k1gFnPc7
(	(	kIx(
<g/>
nabídky	nabídka	k1gFnPc1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
brigád	brigáda	k1gFnPc2
<g/>
,	,	kIx,
diplomových	diplomový	k2eAgFnPc2d1
či	či	k8xC
bakalářských	bakalářský	k2eAgFnPc2d1
prac	prac	k6eAd1
</s>
<s>
Kluby	klub	k1gInPc1
Studentské	studentský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
ČVUT	ČVUT	kA
</s>
<s>
Kolejní	kolejní	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Starají	starat	k5eAaImIp3nP
se	se	k3xPyFc4
o	o	k7c4
blaho	blaho	k1gNnSc4
studentů	student	k1gMnPc2
ubytovaných	ubytovaný	k2eAgMnPc2d1
na	na	k7c6
koleji	kolej	k1gFnSc6
<g/>
,	,	kIx,
poskytují	poskytovat	k5eAaImIp3nP
svým	svůj	k3xOyFgMnPc3
členům	člen	k1gMnPc3
připojení	připojení	k1gNnSc4
k	k	k7c3
internetu	internet	k1gInSc3
a	a	k8xC
další	další	k2eAgFnPc4d1
služby	služba	k1gFnPc4
jako	jako	k8xS,k8xC
tisk	tisk	k1gInSc4
dokumentů	dokument	k1gInPc2
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgFnSc4d1
emailovou	emailový	k2eAgFnSc4d1
schránku	schránka	k1gFnSc4
<g/>
,	,	kIx,
webový	webový	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
digitální	digitální	k2eAgFnSc4d1
televizi	televize	k1gFnSc4
po	po	k7c6
síti	síť	k1gFnSc6
jiné	jiný	k2eAgInPc4d1
projekty	projekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunikují	komunikovat	k5eAaImIp3nP
s	s	k7c7
vedením	vedení	k1gNnSc7
koleje	kolej	k1gFnSc2
<g/>
,	,	kIx,
spravují	spravovat	k5eAaImIp3nP
a	a	k8xC
půjčují	půjčovat	k5eAaImIp3nP
zájmové	zájmový	k2eAgFnPc4d1
místnosti	místnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
pořádají	pořádat	k5eAaImIp3nP
také	také	k9
různé	různý	k2eAgFnPc4d1
společenské	společenský	k2eAgFnPc4d1
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgFnPc4d1
a	a	k8xC
vzdělávací	vzdělávací	k2eAgFnPc4d1
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
všechny	všechen	k3xTgMnPc4
lze	lze	k6eAd1
jmenovat	jmenovat	k5eAaImF,k5eAaBmF
ten	ten	k3xDgInSc4
největší	veliký	k2eAgInSc4d3
<g/>
,	,	kIx,
strahovský	strahovský	k2eAgInSc4d1
Silicon	Silicon	kA
Hill	Hill	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zájmové	zájmový	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Studenti	student	k1gMnPc1
ČVUT	ČVUT	kA
mají	mít	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
rozličné	rozličný	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
technická	technický	k2eAgFnSc1d1
duše	duše	k1gFnSc1
si	se	k3xPyFc3
čas	čas	k1gInSc4
od	od	k7c2
času	čas	k1gInSc2
také	také	k9
potřebuje	potřebovat	k5eAaImIp3nS
povyrazit	povyrazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájmové	zájmový	k2eAgInPc1d1
kluby	klub	k1gInPc1
přináší	přinášet	k5eAaImIp3nS
do	do	k7c2
života	život	k1gInSc2
studentů	student	k1gMnPc2
zábavu	zábav	k1gInSc2
<g/>
,	,	kIx,
poučení	poučení	k1gNnSc1
i	i	k8xC
oddech	oddech	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
sport	sport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízejí	nabízet	k5eAaImIp3nP
jak	jak	k6eAd1
příjemné	příjemný	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k9
možnost	možnost	k1gFnSc4
otevřené	otevřený	k2eAgFnSc2d1
diskuse	diskuse	k1gFnSc2
na	na	k7c4
různá	různý	k2eAgNnPc4d1
témata	téma	k1gNnPc4
nebo	nebo	k8xC
možnost	možnost	k1gFnSc4
poměřit	poměřit	k5eAaPmF
síly	síla	k1gFnPc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Kluby	klub	k1gInPc1
zahraničního	zahraniční	k2eAgInSc2d1
typu	typ	k1gInSc2
se	se	k3xPyFc4
starají	starat	k5eAaImIp3nP
o	o	k7c4
zahraniční	zahraniční	k2eAgMnPc4d1
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
přijedou	přijet	k5eAaPmIp3nP
na	na	k7c6
ČVUT	ČVUT	kA
a	a	k8xC
pomáhají	pomáhat	k5eAaImIp3nP
jim	on	k3xPp3gMnPc3
překonávat	překonávat	k5eAaImF
počáteční	počáteční	k2eAgFnPc1d1
obtíže	obtíž	k1gFnPc1
v	v	k7c6
neznámém	známý	k2eNgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
nabízejí	nabízet	k5eAaImIp3nP
studentům	student	k1gMnPc3
vycestovat	vycestovat	k5eAaPmF
na	na	k7c6
praxi	praxe	k1gFnSc6
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
podívat	podívat	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
do	do	k7c2
světa	svět	k1gInSc2
a	a	k8xC
něco	něco	k3yInSc1
se	se	k3xPyFc4
přiučit	přiučit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivity	aktivita	k1gFnPc1
zahraničních	zahraniční	k2eAgInPc2d1
klubů	klub	k1gInPc2
jsou	být	k5eAaImIp3nP
však	však	k9
podstatně	podstatně	k6eAd1
širší	široký	k2eAgInSc4d2
a	a	k8xC
zahrnují	zahrnovat	k5eAaImIp3nP
pořádání	pořádání	k1gNnSc4
různých	různý	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
a	a	k8xC
mnoho	mnoho	k4c4
dalšího	další	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraniční	zahraniční	k2eAgInPc1d1
kluby	klub	k1gInPc1
zahrnují	zahrnovat	k5eAaImIp3nP
IAESTE	IAESTE	kA
<g/>
,	,	kIx,
International	International	k1gFnSc1
Student	student	k1gMnSc1
Club	club	k1gInSc1
ČVUT	ČVUT	kA
a	a	k8xC
BEST	BEST	kA
<g/>
.	.	kIx.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
na	na	k7c4
ČVUT	ČVUT	kA
</s>
<s>
Akademický	akademický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
ČVUT	ČVUT	kA
</s>
<s>
Akademický	akademický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
ČVUT	ČVUT	kA
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1
britského	britský	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
a	a	k8xC
sboru	sbor	k1gInSc2
„	„	k?
<g/>
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc1
orchestra	orchestra	k1gFnSc1
and	and	k?
chorus	chorus	k1gInSc1
<g/>
“	“	k?
v	v	k7c6
říjnu	říjen	k1gInSc6
2006	#num#	k4
na	na	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
koncertů	koncert	k1gInPc2
konaných	konaný	k2eAgInPc2d1
k	k	k7c3
300	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
vzniku	vznik	k1gInSc2
ČVUT	ČVUT	kA
podnítilo	podnítit	k5eAaPmAgNnS
prorektora	prorektor	k1gMnSc2
ČVUT	ČVUT	kA
prof.	prof.	kA
Vejražku	Vejražku	k?
k	k	k7c3
předložení	předložení	k1gNnSc3
návrhu	návrh	k1gInSc2
opatření	opatření	k1gNnPc2
vedoucích	vedoucí	k1gMnPc2
ke	k	k7c3
vzniku	vznik	k1gInSc3
orchestru	orchestr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dirigentská	dirigentský	k2eAgFnSc1d1
taktovka	taktovka	k1gFnSc1
byla	být	k5eAaImAgFnS
svěřena	svěřen	k2eAgFnSc1d1
Janu	Jan	k1gMnSc3
Šrámkovi	Šrámek	k1gMnSc3
spolu	spolu	k6eAd1
s	s	k7c7
úkolem	úkol	k1gInSc7
budovat	budovat	k5eAaImF
reprezentační	reprezentační	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakrátko	zakrátko	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
malý	malý	k2eAgInSc1d1
smyčcový	smyčcový	k2eAgInSc1d1
ansámbl	ansámbl	k1gInSc1
a	a	k8xC
flétnový	flétnový	k2eAgInSc1d1
soubor	soubor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
působí	působit	k5eAaImIp3nS
i	i	k9
jako	jako	k9
samostatné	samostatný	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orchestr	orchestr	k1gInSc1
ČVUT	ČVUT	kA
je	být	k5eAaImIp3nS
jedinečné	jedinečný	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
univerzitní	univerzitní	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
propojuje	propojovat	k5eAaImIp3nS
profesionální	profesionální	k2eAgMnPc4d1
mladé	mladý	k2eAgMnPc4d1
umělce	umělec	k1gMnPc4
<g/>
,	,	kIx,
s	s	k7c7
neprofesionálními	profesionální	k2eNgMnPc7d1
hudebníky	hudebník	k1gMnPc7
studující	studující	k2eAgFnSc2d1
vysokoškolské	vysokoškolský	k2eAgFnSc2d1
obory	obora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
jejich	jejich	k3xOp3gInPc1
koncerty	koncert	k1gInPc1
byly	být	k5eAaImAgInP
co	co	k9
nejzajímavější	zajímavý	k2eAgMnSc1d3
jak	jak	k8xS,k8xC
pro	pro	k7c4
publikum	publikum	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k9
pro	pro	k7c4
samotné	samotný	k2eAgMnPc4d1
členy	člen	k1gMnPc4
orchestru	orchestr	k1gInSc2
<g/>
,	,	kIx,
zvou	zvát	k5eAaImIp3nP
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc6
vynikající	vynikající	k2eAgMnPc4d1
sólisty	sólista	k1gMnPc4
a	a	k8xC
renomované	renomovaný	k2eAgMnPc4d1
hudebníky	hudebník	k1gMnPc4
české	český	k2eAgFnSc2d1
hudební	hudební	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stálým	stálý	k2eAgNnSc7d1
působištěm	působiště	k1gNnSc7
orchestru	orchestr	k1gInSc2
je	být	k5eAaImIp3nS
pražská	pražský	k2eAgFnSc1d1
Betlémská	betlémský	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
své	svůj	k3xOyFgInPc4
pravidelné	pravidelný	k2eAgInPc4d1
koncerty	koncert	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orchestru	orchestr	k1gInSc2
se	se	k3xPyFc4
dostává	dostávat	k5eAaImIp3nS
pozvání	pozvání	k1gNnSc4
i	i	k9
k	k	k7c3
vystoupením	vystoupení	k1gNnSc7
mimo	mimo	k7c4
akademickou	akademický	k2eAgFnSc4d1
obec	obec	k1gFnSc4
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
např.	např.	kA
na	na	k7c4
podzim	podzim	k1gInSc4
2008	#num#	k4
v	v	k7c6
Senátu	senát	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
v	v	k7c6
září	září	k1gNnSc6
2009	#num#	k4
ve	v	k7c6
Španělském	španělský	k2eAgInSc6d1
sále	sál	k1gInSc6
Pražského	pražský	k2eAgInSc2d1
Hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2013	#num#	k4
soubor	soubor	k1gInSc1
vystoupil	vystoupit	k5eAaPmAgInS
také	také	k9
na	na	k7c4
slavné	slavný	k2eAgFnPc4d1
University	universita	k1gFnPc4
of	of	k?
Cambridge	Cambridge	k1gFnPc4
k	k	k7c3
příležitosti	příležitost	k1gFnSc3
konání	konání	k1gNnSc2
akce	akce	k1gFnSc2
Jewels	Jewelsa	k1gFnPc2
of	of	k?
Czech	Czech	k1gMnSc1
Music	Music	k1gMnSc1
v	v	k7c6
Emmanuel	Emmanuel	k1gMnSc1
United	United	k1gMnSc1
Reformed	Reformed	k1gMnSc1
Church	Church	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
</s>
<s>
Smíšený	smíšený	k2eAgInSc1d1
Pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
ČVUT	ČVUT	kA
je	být	k5eAaImIp3nS
uměleckým	umělecký	k2eAgNnSc7d1
sdružením	sdružení	k1gNnSc7
s	s	k7c7
tradicí	tradice	k1gFnSc7
sahající	sahající	k2eAgInSc4d1
až	až	k6eAd1
k	k	k7c3
roku	rok	k1gInSc3
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
svými	svůj	k3xOyFgInPc7
skoro	skoro	k6eAd1
sto	sto	k4xCgNnSc4
členy	člen	k1gMnPc7
patří	patřit	k5eAaImIp3nS
k	k	k7c3
největším	veliký	k2eAgNnPc3d3
pražským	pražský	k2eAgNnPc3d1
hudebním	hudební	k2eAgNnPc3d1
tělesům	těleso	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
absolventů	absolvent	k1gMnPc2
a	a	k8xC
profesorů	profesor	k1gMnPc2
ČVUT	ČVUT	kA
jsou	být	k5eAaImIp3nP
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
řadách	řada	k1gFnPc6
i	i	k8xC
další	další	k2eAgMnPc1d1
zájemci	zájemce	k1gMnPc1
o	o	k7c4
sborový	sborový	k2eAgInSc4d1
zpěv	zpěv	k1gInSc4
nejrůznějších	různý	k2eAgNnPc2d3
povolání	povolání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
amatérský	amatérský	k2eAgInSc4d1
sbor	sbor	k1gInSc4
<g/>
,	,	kIx,
vystřídalo	vystřídat	k5eAaPmAgNnS
se	se	k3xPyFc4
během	během	k7c2
historie	historie	k1gFnSc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
vedení	vedení	k1gNnSc1
mnoho	mnoho	k6eAd1
vynikajících	vynikající	k2eAgMnPc2d1
profesionálních	profesionální	k2eAgMnPc2d1
hudebníků	hudebník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Repertoár	repertoár	k1gInSc1
sboru	sbor	k1gInSc2
obsahuje	obsahovat	k5eAaImIp3nS
vážnou	vážný	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
od	od	k7c2
renesance	renesance	k1gFnSc2
až	až	k9
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zvláštní	zvláštní	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
je	být	k5eAaImIp3nS
věnována	věnovat	k5eAaPmNgFnS,k5eAaImNgFnS
hudbě	hudba	k1gFnSc3
českých	český	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
to	ten	k3xDgNnSc4
se	se	k3xPyFc4
ale	ale	k8xC
sbor	sbor	k1gInSc1
nebrání	bránit	k5eNaImIp3nS
i	i	k9
jiným	jiný	k2eAgInPc3d1
hudebním	hudební	k2eAgInPc3d1
žánrům	žánr	k1gInPc3
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
úpravy	úprava	k1gFnPc4
lidových	lidový	k2eAgFnPc2d1
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
spirituály	spirituál	k1gInPc1
<g/>
,	,	kIx,
či	či	k8xC
aranže	aranže	k1gFnSc1
rockové	rockový	k2eAgFnSc2d1
a	a	k8xC
populární	populární	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
ČVUT	ČVUT	kA
pravidelně	pravidelně	k6eAd1
účinkuje	účinkovat	k5eAaImIp3nS
na	na	k7c6
koncertech	koncert	k1gInPc6
a	a	k8xC
hudebních	hudební	k2eAgInPc6d1
festivalech	festival	k1gInPc6
jak	jak	k8xC,k8xS
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
mnoha	mnoho	k4c7
profesionálními	profesionální	k2eAgMnPc7d1
sólisty	sólista	k1gMnPc7
a	a	k8xC
hudebními	hudební	k2eAgNnPc7d1
tělesy	těleso	k1gNnPc7
(	(	kIx(
<g/>
Symfonický	symfonický	k2eAgInSc4d1
orchestr	orchestr	k1gInSc4
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
<g/>
,	,	kIx,
Severočeská	severočeský	k2eAgFnSc1d1
filharmonie	filharmonie	k1gFnSc1
Teplice	Teplice	k1gFnPc1
<g/>
,	,	kIx,
Hudba	hudba	k1gFnSc1
Hradní	hradní	k2eAgFnSc2d1
stráže	stráž	k1gFnSc2
a	a	k8xC
policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
…	…	k?
<g/>
)	)	kIx)
s	s	k7c7
nimiž	jenž	k3xRgInPc7
vystupuje	vystupovat	k5eAaImIp3nS
na	na	k7c6
předních	přední	k2eAgNnPc6d1
pražských	pražský	k2eAgNnPc6d1
koncertních	koncertní	k2eAgNnPc6d1
pódiích	pódium	k1gNnPc6
(	(	kIx(
<g/>
Dvořákova	Dvořákův	k2eAgFnSc1d1
síň	síň	k1gFnSc1
Rudolfina	Rudolfin	k2eAgFnSc1d1
<g/>
,	,	kIx,
Smetanova	Smetanův	k2eAgFnSc1d1
síň	síň	k1gFnSc1
Obecního	obecní	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
,	,	kIx,
Sál	sál	k1gInSc4
paláce	palác	k1gInSc2
Žofín	Žofín	k1gInSc1
<g/>
,	,	kIx,
Španělský	španělský	k2eAgInSc1d1
sál	sál	k1gInSc1
Pražského	pražský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
,	,	kIx,
Katedrála	katedrála	k1gFnSc1
sv.	sv.	kA
Víta	Víta	k1gFnSc1
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbor	sbor	k1gInSc1
již	již	k6eAd1
několikrát	několikrát	k6eAd1
natáčel	natáčet	k5eAaImAgMnS
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
televizi	televize	k1gFnSc4
a	a	k8xC
bývá	bývat	k5eAaImIp3nS
zván	zvát	k5eAaImNgInS
Kanceláří	kancelář	k1gFnSc7
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
k	k	k7c3
uměleckému	umělecký	k2eAgNnSc3d1
vystupování	vystupování	k1gNnSc3
při	při	k7c6
slavnostních	slavnostní	k2eAgFnPc6d1
příležitostech	příležitost	k1gFnPc6
a	a	k8xC
výročích	výročí	k1gNnPc6
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sbormistrem	sbormistr	k1gMnSc7
tělesa	těleso	k1gNnSc2
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Steyer	Steyer	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Symfonický	symfonický	k2eAgInSc1d1
a	a	k8xC
Komorní	komorní	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
pak	pak	k6eAd1
Komorní	komorní	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgNnPc1
tělesa	těleso	k1gNnPc1
vznikla	vzniknout	k5eAaPmAgNnP
jako	jako	k9
orchestry	orchestr	k1gInPc1
SÚDOP	SÚDOP	kA
<g/>
,	,	kIx,
následně	následně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
po	po	k7c4
dobu	doba	k1gFnSc4
cca	cca	kA
tří	tři	k4xCgInPc2
let	léto	k1gNnPc2
působila	působit	k5eAaImAgFnS
jako	jako	k9
orchestry	orchestra	k1gFnPc1
ČSD	ČSD	kA
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
orchestry	orchestra	k1gFnPc4
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byť	byť	k8xS
orchestry	orchestr	k1gInPc1
původně	původně	k6eAd1
sdružovaly	sdružovat	k5eAaImAgInP
hráče	hráč	k1gMnPc4
z	z	k7c2
řad	řada	k1gFnPc2
zaměstnanců	zaměstnanec	k1gMnPc2
SÚDOPu	SÚDOPa	k1gFnSc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
dnes	dnes	k6eAd1
do	do	k7c2
orchestru	orchestr	k1gInSc2
přihlásit	přihlásit	k5eAaPmF
i	i	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nejsou	být	k5eNaImIp3nP
s	s	k7c7
touto	tento	k3xDgFnSc7
firmou	firma	k1gFnSc7
spojeni	spojit	k5eAaPmNgMnP
a	a	k8xC
ani	ani	k8xC
nemusejí	muset	k5eNaImIp3nP
být	být	k5eAaImF
studenty	student	k1gMnPc4
či	či	k8xC
absolventy	absolvent	k1gMnPc4
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Orchestry	orchestra	k1gFnPc1
jako	jako	k8xS,k8xC
dirigent	dirigent	k1gMnSc1
vedl	vést	k5eAaImAgMnS
Ivo	Ivo	k1gMnSc1
Kraupner	Kraupner	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
vystřídán	vystřídat	k5eAaPmNgInS
Janem	Jan	k1gMnSc7
Štrosem	Štros	k1gMnSc7
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
stojí	stát	k5eAaImIp3nS
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
Lukáš	Lukáš	k1gMnSc1
Kovařík	Kovařík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělesa	těleso	k1gNnPc1
vystupovala	vystupovat	k5eAaImAgNnP
jak	jak	k6eAd1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
v	v	k7c6
Miličíně	Miličína	k1gFnSc6
či	či	k8xC
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
Betlémské	betlémský	k2eAgFnSc6d1
kapli	kaple	k1gFnSc6
<g/>
,	,	kIx,
Smetanově	Smetanův	k2eAgFnSc6d1
síni	síň	k1gFnSc6
Obecního	obecní	k2eAgInSc2d1
domu	dům	k1gInSc2
nebo	nebo	k8xC
Dvořákově	Dvořákův	k2eAgFnSc3d1
síni	síň	k1gFnSc3
Rudolfina	Rudolfinum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
nebo	nebo	k8xC
Vatikán	Vatikán	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
orchestry	orchestr	k1gInPc7
spolupracovali	spolupracovat	k5eAaImAgMnP
též	též	k9
i	i	k9
významní	významný	k2eAgMnPc1d1
hudebníci	hudebník	k1gMnPc1
(	(	kIx(
<g/>
například	například	k6eAd1
houslista	houslista	k1gMnSc1
Čeněk	Čeněk	k1gMnSc1
Pavlík	Pavlík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řídící	řídící	k2eAgInPc1d1
a	a	k8xC
správní	správní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
</s>
<s>
Vrcholným	vrcholný	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
a	a	k8xC
reprezentantem	reprezentant	k1gMnSc7
ČVUT	ČVUT	kA
navenek	navenek	k6eAd1
je	být	k5eAaImIp3nS
rektor	rektor	k1gMnSc1
jmenovaný	jmenovaný	k2eAgMnSc1d1
prezidentem	prezident	k1gMnSc7
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rektor	rektor	k1gMnSc1
má	mít	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
jako	jako	k8xS,k8xC
poradní	poradní	k2eAgInPc1d1
orgány	orgán	k1gInPc1
kolegium	kolegium	k1gNnSc1
rektora	rektor	k1gMnSc2
(	(	kIx(
<g/>
prorektoři	prorektor	k1gMnPc1
<g/>
,	,	kIx,
děkani	děkan	k1gMnPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
<g/>
,	,	kIx,
kvestor	kvestor	k1gMnSc1
<g/>
,	,	kIx,
kancléř	kancléř	k1gMnSc1
a	a	k8xC
předseda	předseda	k1gMnSc1
AS	as	k1gNnSc2
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
grémium	grémium	k1gNnSc1
rektora	rektor	k1gMnSc2
(	(	kIx(
<g/>
členové	člen	k1gMnPc1
kolegia	kolegium	k1gNnSc2
<g/>
,	,	kIx,
ředitelé	ředitel	k1gMnPc1
součástí	součást	k1gFnPc2
ČVUT	ČVUT	kA
a	a	k8xC
místopředseda	místopředseda	k1gMnSc1
AS	as	k1gNnSc2
ČVUT	ČVUT	kA
za	za	k7c7
studenty	student	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
vedení	vedení	k1gNnSc1
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
rektor	rektor	k1gMnSc1
<g/>
,	,	kIx,
prorektoři	prorektor	k1gMnPc1
<g/>
,	,	kIx,
kvestor	kvestor	k1gMnSc1
<g/>
,	,	kIx,
kancléř	kancléř	k1gMnSc1
a	a	k8xC
předseda	předseda	k1gMnSc1
AS	as	k1gNnSc2
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prorektoři	prorektor	k1gMnPc1
zastupují	zastupovat	k5eAaImIp3nP
rektora	rektor	k1gMnSc4
v	v	k7c6
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jim	on	k3xPp3gMnPc3
rektor	rektor	k1gMnSc1
určí	určit	k5eAaPmIp3nS
(	(	kIx(
<g/>
pro	pro	k7c4
zahraniční	zahraniční	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
,	,	kIx,
pro	pro	k7c4
vědeckou	vědecký	k2eAgFnSc4d1
a	a	k8xC
výzkumnou	výzkumný	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
studium	studium	k1gNnSc4
a	a	k8xC
studentské	studentský	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
pro	pro	k7c4
informační	informační	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
výkon	výkon	k1gInSc4
své	svůj	k3xOyFgFnSc2
činnosti	činnost	k1gFnSc2
rektor	rektor	k1gMnSc1
zodpovídá	zodpovídat	k5eAaImIp3nS,k5eAaPmIp3nS
akademickému	akademický	k2eAgInSc3d1
senátu	senát	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
samosprávným	samosprávný	k2eAgMnSc7d1
zastupitelským	zastupitelský	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
<g/>
,	,	kIx,
voleným	volený	k2eAgInSc7d1
akademickou	akademický	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
ČVUT	ČVUT	kA
na	na	k7c4
tříleté	tříletý	k2eAgNnSc4d1
funkční	funkční	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
senátu	senát	k1gInSc6
je	být	k5eAaImIp3nS
každá	každý	k3xTgFnSc1
fakulta	fakulta	k1gFnSc1
zastoupena	zastoupit	k5eAaPmNgFnS
třemi	tři	k4xCgMnPc7
akademickými	akademický	k2eAgMnPc7d1
pracovníky	pracovník	k1gMnPc7
a	a	k8xC
dvěma	dva	k4xCgMnPc7
studenty	student	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rektorát	rektorát	k1gInSc1
<g/>
,	,	kIx,
vysokoškolské	vysokoškolský	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
součásti	součást	k1gFnPc1
ČVUT	ČVUT	kA
jsou	být	k5eAaImIp3nP
dohromady	dohromady	k6eAd1
zastoupeny	zastoupit	k5eAaPmNgFnP
třemi	tři	k4xCgMnPc7
akademickými	akademický	k2eAgMnPc7d1
pracovníky	pracovník	k1gMnPc7
a	a	k8xC
dvěma	dva	k4xCgMnPc7
studenty	student	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
schvaluje	schvalovat	k5eAaImIp3nS
vnitřní	vnitřní	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
<g/>
,	,	kIx,
rozpočet	rozpočet	k1gInSc1
předložený	předložený	k2eAgInSc1d1
rektorem	rektor	k1gMnSc7
<g/>
,	,	kIx,
kontroluje	kontrolovat	k5eAaImIp3nS
hospodaření	hospodaření	k1gNnSc1
a	a	k8xC
schvaluje	schvalovat	k5eAaImIp3nS
výroční	výroční	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
a	a	k8xC
dlouhodobý	dlouhodobý	k2eAgInSc4d1
záměr	záměr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řízení	řízení	k1gNnSc1
hospodaření	hospodaření	k1gNnSc2
a	a	k8xC
vnitřní	vnitřní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
školy	škola	k1gFnSc2
má	mít	k5eAaImIp3nS
na	na	k7c6
starosti	starost	k1gFnSc6
kvestor	kvestor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhodobý	dlouhodobý	k2eAgInSc1d1
záměr	záměr	k1gInSc1
školy	škola	k1gFnSc2
a	a	k8xC
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
projednává	projednávat	k5eAaImIp3nS
a	a	k8xC
schvaluje	schvalovat	k5eAaImIp3nS
vědecká	vědecký	k2eAgFnSc1d1
rada	rada	k1gFnSc1
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
jejímiž	jejíž	k3xOyRp3gMnPc7
členy	člen	k1gMnPc7
jsou	být	k5eAaImIp3nP
významní	významný	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgFnPc6,k3yRgFnPc6,k3yQgFnPc6
škola	škola	k1gFnSc1
působí	působit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
činnost	činnost	k1gFnSc4
univerzity	univerzita	k1gFnSc2
dohlíží	dohlížet	k5eAaImIp3nS
správní	správní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
jmenovaná	jmenovaný	k2eAgFnSc1d1
ministrem	ministr	k1gMnSc7
školství	školství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
kontroluje	kontrolovat	k5eAaImIp3nS
hlavně	hlavně	k9
rozpočet	rozpočet	k1gInSc4
či	či	k8xC
dlouhodobý	dlouhodobý	k2eAgInSc4d1
záměr	záměr	k1gInSc4
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
fakulta	fakulta	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
čele	čelo	k1gNnSc6
děkana	děkan	k1gMnSc2
jako	jako	k8xS,k8xC
nejvyššího	vysoký	k2eAgMnSc2d3
představitele	představitel	k1gMnSc2
<g/>
,	,	kIx,
akademický	akademický	k2eAgInSc1d1
senát	senát	k1gInSc1
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
zastupitelský	zastupitelský	k2eAgInSc1d1
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
děkana	děkan	k1gMnSc4
volí	volit	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
vědeckou	vědecký	k2eAgFnSc4d1
radu	rada	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
spravuje	spravovat	k5eAaImIp3nS
studijní	studijní	k2eAgInPc4d1
plány	plán	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poradním	poradní	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
děkana	děkan	k1gMnSc2
jsou	být	k5eAaImIp3nP
kolegium	kolegium	k1gNnSc4
děkana	děkan	k1gMnSc2
a	a	k8xC
grémium	grémium	k1gNnSc1
děkana	děkan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgInPc7
zástupci	zástupce	k1gMnPc1
pro	pro	k7c4
některé	některý	k3yIgFnPc4
činnosti	činnost	k1gFnPc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
děkan	děkan	k1gMnSc1
proděkany	proděkan	k1gMnPc4
a	a	k8xC
svého	svůj	k3xOyFgMnSc2
tajemníka	tajemník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Každá	každý	k3xTgFnSc1
fakulta	fakulta	k1gFnSc1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
disciplinární	disciplinární	k2eAgFnSc4d1
komisi	komise	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
projednává	projednávat	k5eAaImIp3nS
přestupky	přestupek	k1gInPc4
studentů	student	k1gMnPc2
zapsaných	zapsaný	k2eAgMnPc2d1
na	na	k7c6
dané	daný	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestupky	přestupek	k1gInPc4
studentů	student	k1gMnPc2
ČVUT	ČVUT	kA
zapsaných	zapsaný	k2eAgNnPc2d1
na	na	k7c6
mimofakultních	mimofakultní	k2eAgInPc6d1
vysokoškolských	vysokoškolský	k2eAgInPc6d1
ústavech	ústav	k1gInPc6
projednává	projednávat	k5eAaImIp3nS
disciplinární	disciplinární	k2eAgFnSc1d1
komise	komise	k1gFnSc1
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
akademické	akademický	k2eAgFnSc2d1
obce	obec	k1gFnSc2
ČVUT	ČVUT	kA
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
zaměstnanci	zaměstnanec	k1gMnPc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nP
etickými	etický	k2eAgInPc7d1
principy	princip	k1gInPc7
vzájemných	vzájemný	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
na	na	k7c6
ČVUT	ČVUT	kA
i	i	k8xC
vztahů	vztah	k1gInPc2
vůči	vůči	k7c3
společnosti	společnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrn	souhrn	k1gInSc1
těchto	tento	k3xDgInPc2
základních	základní	k2eAgInPc2d1
principů	princip	k1gInPc2
je	být	k5eAaImIp3nS
zakotven	zakotvit	k5eAaPmNgInS
v	v	k7c6
Etickém	etický	k2eAgInSc6d1
kodexu	kodex	k1gInSc6
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strategické	strategický	k2eAgFnPc4d1
vize	vize	k1gFnPc4
a	a	k8xC
mise	mise	k1gFnPc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
ve	v	k7c6
Strategii	strategie	k1gFnSc6
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s>
Absolventi	absolvent	k1gMnPc1
ČVUT	ČVUT	kA
</s>
<s>
Spolek	spolek	k1gInSc1
Alumni	Alumni	k1gFnSc2
ČVUT	ČVUT	kA
</s>
<s>
Spolek	spolek	k1gInSc1
absolventů	absolvent	k1gMnPc2
a	a	k8xC
přátel	přítel	k1gMnPc2
ČVUT	ČVUT	kA
(	(	kIx(
<g/>
www.absolventicvut.cz	www.absolventicvut.cz	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
navázání	navázání	k1gNnSc4
kontaktu	kontakt	k1gInSc2
s	s	k7c7
těmi	ten	k3xDgMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
na	na	k7c6
nejstarší	starý	k2eAgFnSc6d3
a	a	k8xC
největší	veliký	k2eAgFnSc6d3
technické	technický	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
ČR	ČR	kA
studovali	studovat	k5eAaImAgMnP
již	již	k6eAd1
před	před	k7c7
lety	léto	k1gNnPc7
a	a	k8xC
zároveň	zároveň	k6eAd1
má	mít	k5eAaImIp3nS
ČVUT	ČVUT	kA
zájem	zájem	k1gInSc1
prostřednictvím	prostřednictvím	k7c2
spolku	spolek	k1gInSc2
udržet	udržet	k5eAaPmF
kontakt	kontakt	k1gInSc4
s	s	k7c7
těmi	ten	k3xDgMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
teprve	teprve	k6eAd1
absolvovat	absolvovat	k5eAaPmF
budou	být	k5eAaImBp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
<g/>
,	,	kIx,
respektive	respektive	k9
členové	člen	k1gMnPc1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
získávat	získávat	k5eAaImF
pravidelné	pravidelný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c4
dění	dění	k1gNnSc4
na	na	k7c4
své	svůj	k3xOyFgMnPc4
Alma	alma	k1gFnSc1
Mater	mater	k1gFnSc2
a	a	k8xC
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
tak	tak	k6eAd1
zúčastňovat	zúčastňovat	k5eAaImF
akcí	akce	k1gFnPc2
(	(	kIx(
<g/>
koncerty	koncert	k1gInPc1
<g/>
,	,	kIx,
plesy	ples	k1gInPc1
<g/>
,	,	kIx,
kurzy	kurz	k1gInPc1
<g/>
,	,	kIx,
přednášky	přednáška	k1gFnPc1
<g/>
,	,	kIx,
atd.	atd.	kA
<g/>
)	)	kIx)
pořádaných	pořádaný	k2eAgInPc2d1
na	na	k7c6
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
mohou	moct	k5eAaImIp3nP
navštěvovat	navštěvovat	k5eAaImF
pravidelná	pravidelný	k2eAgNnPc1d1
setkávání	setkávání	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
spolek	spolek	k1gInSc4
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
členy	člen	k1gMnPc4
organizuje	organizovat	k5eAaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
členy	člen	k1gInPc4
spolku	spolek	k1gInSc2
jsou	být	k5eAaImIp3nP
nejen	nejen	k6eAd1
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
již	již	k6eAd1
ukončili	ukončit	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
studium	studium	k1gNnSc4
na	na	k7c6
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
studenti	student	k1gMnPc1
a	a	k8xC
zaměstnanci	zaměstnanec	k1gMnPc1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
vítáni	vítán	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
mají	mít	k5eAaImIp3nP
k	k	k7c3
Českému	český	k2eAgInSc3d1
vysokému	vysoký	k2eAgInSc3d1
učení	učení	k1gNnSc2
technickému	technický	k2eAgInSc3d1
v	v	k7c6
Praze	Praha	k1gFnSc6
vztah	vztah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Spolek	spolek	k1gInSc1
Alumni	Alumni	k1gFnSc2
ČVUT	ČVUT	kA
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
pořádá	pořádat	k5eAaImIp3nS
sbírku	sbírka	k1gFnSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
realizace	realizace	k1gFnSc2
klasických	klasický	k2eAgFnPc2d1
píšťalových	píšťalový	k2eAgFnPc2d1
varhan	varhany	k1gFnPc2
(	(	kIx(
<g/>
http://varhany.cvut.cz	http://varhany.cvut.cza	k1gFnPc2
<g/>
)	)	kIx)
do	do	k7c2
Betlémské	betlémský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
již	již	k9
řadu	řada	k1gFnSc4
let	léto	k1gNnPc2
slouží	sloužit	k5eAaImIp3nP
všem	všecek	k3xTgMnPc3
studentům	student	k1gMnPc3
jako	jako	k8xC,k8xS
slavnostní	slavnostní	k2eAgFnSc1d1
brána	brána	k1gFnSc1
pro	pro	k7c4
vstup	vstup	k1gInSc4
i	i	k8xC
opuštění	opuštění	k1gNnSc4
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
na	na	k7c6
ČVUT	ČVUT	kA
studovaly	studovat	k5eAaImAgFnP
</s>
<s>
(	(	kIx(
<g/>
Seznam	seznam	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
pouze	pouze	k6eAd1
některá	některý	k3yIgNnPc1
jména	jméno	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vešla	vejít	k5eAaPmAgFnS
do	do	k7c2
hlubšího	hluboký	k2eAgNnSc2d2
českého	český	k2eAgNnSc2d1
a	a	k8xC
československého	československý	k2eAgNnSc2d1
povědomí	povědomí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Pavel	Pavel	k1gMnSc1
Bobek	Bobek	k1gMnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
hudebník	hudebník	k1gMnSc1
<g/>
;	;	kIx,
na	na	k7c6
ČVUT	ČVUT	kA
vystudoval	vystudovat	k5eAaPmAgMnS
architekturu	architektura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Miloš	Miloš	k1gMnSc1
Čermák	Čermák	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
novinář	novinář	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
konzultant	konzultant	k1gMnSc1
a	a	k8xC
spolumajitel	spolumajitel	k1gMnSc1
firmy	firma	k1gFnSc2
NextBig	NextBig	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
;	;	kIx,
na	na	k7c6
ČVUT	ČVUT	kA
vystudoval	vystudovat	k5eAaPmAgMnS
kybernetiku	kybernetika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dana	Dana	k1gFnSc1
Drábová	Drábová	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
–	–	k?
česká	český	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
fyzička	fyzička	k1gFnSc1
<g/>
,	,	kIx,
dlouholetá	dlouholetý	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
Státního	státní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
pro	pro	k7c4
jadernou	jaderný	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Antonín	Antonín	k1gMnSc1
Engel	Engel	k1gMnSc1
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
–	–	k?
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
a	a	k8xC
urbanista	urbanista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Frolík	Frolík	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
společnosti	společnost	k1gFnSc2
LINET	LINET	kA
(	(	kIx(
<g/>
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
výrobců	výrobce	k1gMnPc2
nemocničních	nemocniční	k2eAgNnPc2d1
lůžek	lůžko	k1gNnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
viceprezident	viceprezident	k1gMnSc1
Svazu	svaz	k1gInSc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
dopravy	doprava	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ČVUT	ČVUT	kA
vystudoval	vystudovat	k5eAaPmAgMnS
kybernetiku	kybernetika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
esejista	esejista	k1gMnSc1
<g/>
,	,	kIx,
disident	disident	k1gMnSc1
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
ČSFR	ČSFR	kA
i	i	k8xC
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomickou	ekonomický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
studoval	studovat	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1955	#num#	k4
až	až	k9
1957	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
Hrdlička	Hrdlička	k1gMnSc1
(	(	kIx(
<g/>
1888	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
manažer	manažer	k1gMnSc1
<g/>
,	,	kIx,
mnoholetý	mnoholetý	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
automobilky	automobilka	k1gFnSc2
ASAP	ASAP	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Petr	Petr	k1gMnSc1
Hrdlička	Hrdlička	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
automobilový	automobilový	k2eAgMnSc1d1
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
vývojář	vývojář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nejznámějším	známý	k2eAgInSc7d3
dílem	díl	k1gInSc7
je	být	k5eAaImIp3nS
osobní	osobní	k2eAgInSc4d1
automobil	automobil	k1gInSc4
Škoda	Škoda	k1gMnSc1
Favorit	favorit	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Otakar	Otakar	k1gMnSc1
Husák	Husák	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
<g/>
,	,	kIx,
legionář	legionář	k1gMnSc1
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
ČSR	ČSR	kA
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
ředitel	ředitel	k1gMnSc1
továrny	továrna	k1gFnSc2
Explosia	Explosia	k1gFnSc1
Semtín	Semtín	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eva	Eva	k1gFnSc1
Jiřičná	Jiřičný	k2eAgFnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
–	–	k?
česká	český	k2eAgFnSc1d1
architektka	architektka	k1gFnSc1
a	a	k8xC
designérka	designérka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Viktor	Viktor	k1gMnSc1
Kotrubenko	Kotrubenka	k1gFnSc5
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
jazzový	jazzový	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
a	a	k8xC
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
expert	expert	k1gMnSc1
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
;	;	kIx,
na	na	k7c6
ČVUT	ČVUT	kA
vystudoval	vystudovat	k5eAaPmAgMnS
kybernetiku	kybernetika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Křižík	Křižík	k1gMnSc1
(	(	kIx(
<g/>
1847	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
elektrotechnik	elektrotechnik	k1gMnSc1
<g/>
;	;	kIx,
působil	působit	k5eAaImAgMnS
coby	coby	k?
vynálezce	vynálezce	k1gMnSc1
<g/>
,	,	kIx,
konstruktér	konstruktér	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
elektrotechnické	elektrotechnický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Mandel	mandel	k1gInSc4
</s>
<s>
František	František	k1gMnSc1
Moravec	Moravec	k1gMnSc1
alias	alias	k9
Lou	Lou	k1gMnSc1
Fanánek	Fanánek	k1gMnSc1
Hagen	Hagen	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
zpěvák	zpěvák	k1gMnSc1
a	a	k8xC
frontman	frontman	k1gMnSc1
skupiny	skupina	k1gFnSc2
Tři	tři	k4xCgFnPc4
sestry	sestra	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Prelog	Prelog	k1gMnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
chemik	chemik	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
ETH	ETH	kA
Zürich	Zürich	k1gMnSc1
a	a	k8xC
držitel	držitel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
chemii	chemie	k1gFnSc4
za	za	k7c4
rok	rok	k1gInSc4
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Roith	Roith	k1gMnSc1
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
například	například	k6eAd1
jako	jako	k8xC,k8xS
autor	autor	k1gMnSc1
budov	budova	k1gFnPc2
pro	pro	k7c4
ministerstva	ministerstvo	k1gNnPc4
<g/>
,	,	kIx,
banky	banka	k1gFnPc4
a	a	k8xC
podobné	podobný	k2eAgFnPc4d1
instituce	instituce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Adolf	Adolf	k1gMnSc1
Vamberský	vamberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1888	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
strojař	strojař	k1gMnSc1
<g/>
,	,	kIx,
přednosta	přednosta	k1gMnSc1
zbrojní	zbrojní	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
a	a	k8xC
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Škodových	Škodových	k2eAgInPc2d1
závodů	závod	k1gInPc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Vejrych	Vejrych	k1gMnSc1
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
–	–	k?
český	český	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
desítek	desítka	k1gFnPc2
budov	budova	k1gFnPc2
v	v	k7c6
neorenesančním	neorenesanční	k2eAgInSc6d1
a	a	k8xC
secesním	secesní	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Simon	Simon	k1gMnSc1
Wiesenthal	Wiesenthal	k1gMnSc1
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
–	–	k?
ukrajinský	ukrajinský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
známý	známý	k2eAgInSc1d1
spíše	spíše	k9
díky	díky	k7c3
vyhledávání	vyhledávání	k1gNnSc3
uprchlých	uprchlý	k2eAgMnPc2d1
nacistických	nacistický	k2eAgMnPc2d1
válečných	válečný	k2eAgMnPc2d1
zločinců	zločinec	k1gMnPc2
a	a	k8xC
pachatelů	pachatel	k1gMnPc2
holocaustu	holocaust	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
na	na	k7c6
ČVUT	ČVUT	kA
působily	působit	k5eAaImAgFnP
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
Brdlík	Brdlík	k1gMnSc1
(	(	kIx(
<g/>
26.7	26.7	k4
<g/>
.1879	.1879	k4
Žirovnice	Žirovnice	k1gFnSc1
-	-	kIx~
28.1	28.1	k4
<g/>
.1964	.1964	k4
Acron	Acrona	k1gFnPc2
USA	USA	kA
<g/>
)	)	kIx)
1920-21	1920-21	k4
ministr	ministr	k1gMnSc1
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
1921	#num#	k4
ministr	ministr	k1gMnSc1
pro	pro	k7c4
zásobování	zásobování	k1gNnSc4
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
vicepresident	vicepresident	k1gMnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
děkan	děkan	k1gMnSc1
lesnické	lesnický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
odborník	odborník	k1gMnSc1
v	v	k7c6
oboru	obor	k1gInSc6
lesního	lesní	k2eAgNnSc2d1
a	a	k8xC
vodního	vodní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
<g/>
,	,	kIx,
1924-25	1924-25	k4
rektor	rektor	k1gMnSc1
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
Gerstner	Gerstner	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
projektu	projekt	k1gInSc2
koněspřežní	koněspřežní	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
z	z	k7c2
Českých	český	k2eAgInPc2d1
Budějovic	Budějovice	k1gInPc2
do	do	k7c2
Lince	Linec	k1gInSc2
</s>
<s>
Christian	Christian	k1gMnSc1
Doppler	Doppler	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
matematiky	matematika	k1gFnSc2
a	a	k8xC
praktické	praktický	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
</s>
<s>
Ivan	Ivan	k1gMnSc1
Puluj	Puluj	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
fyziky	fyzika	k1gFnSc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
lékařské	lékařský	k2eAgFnSc2d1
radiologie	radiologie	k1gFnSc2
</s>
<s>
Gustav	Gustav	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
für	für	k?
Maschinenbau	Maschinenbaa	k1gMnSc4
(	(	kIx(
<g/>
strojírenství	strojírenství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
1868	#num#	k4
</s>
<s>
Antonín	Antonín	k1gMnSc1
Frič	Frič	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
paleontolog	paleontolog	k1gMnSc1
<g/>
,	,	kIx,
zoolog	zoolog	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Kořistka	Kořistka	k1gFnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Krejčí	Krejčí	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
geologie	geologie	k1gFnSc2
</s>
<s>
Josef	Josef	k1gMnSc1
Zítek	Zítek	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
architektury	architektura	k1gFnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Zvoníček	Zvoníček	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
teorie	teorie	k1gFnSc2
a	a	k8xC
návrhu	návrh	k1gInSc2
parních	parní	k2eAgInPc2d1
strojů	stroj	k1gInPc2
a	a	k8xC
kompresorů	kompresor	k1gInPc2
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Běhounek	běhounek	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
fyziky	fyzika	k1gFnSc2
</s>
<s>
Emil	Emil	k1gMnSc1
Votoček	Votočka	k1gFnPc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
chemie	chemie	k1gFnSc2
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Lukeš	Lukeš	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
organické	organický	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
za	za	k7c4
rok	rok	k1gInSc4
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
2019-06	2019-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HERBEN	HERBEN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
-	-	kIx~
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
presidenta	president	k1gMnSc2
Osvoboditele	osvoboditel	k1gMnSc2
<g/>
,	,	kIx,
páté	pátý	k4xOgNnSc4
vydání	vydání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sfinx	sfinx	k1gFnSc1
*	*	kIx~
Bohumil	Bohumil	k1gMnSc1
Janda	Janda	k1gMnSc1
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
.	.	kIx.
477	#num#	k4
s.	s.	k?
<g/>
↑	↑	k?
prazskyhradarchiv	prazskyhradarchiva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
prazskyhradarchiv	prazskyhradarchiv	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
položka	položka	k1gFnSc1
324	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
pdf	pdf	k?
<g/>
,	,	kIx,
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RYSZAWY	RYSZAWY	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k7c2
kampusu	kampus	k1gInSc2
přímo	přímo	k6eAd1
v	v	k7c6
solárním	solární	k2eAgInSc6d1
domě	dům	k1gInSc6
otevřelo	otevřít	k5eAaPmAgNnS
ČVUT	ČVUT	kA
své	svůj	k3xOyFgFnSc3
Informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
-	-	kIx~
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
2014-11-30	2014-11-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Seznam	seznam	k1gInSc1
všech	všecek	k3xTgNnPc2
pracovišť	pracoviště	k1gNnPc2
Fakulty	fakulta	k1gFnSc2
strojní	strojní	k2eAgFnSc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
FEL	FEL	kA
2017	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
Fakulty	fakulta	k1gFnSc2
elektrotechnické	elektrotechnický	k2eAgFnSc2d1
za	za	k7c4
rok	rok	k1gInSc4
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
leteckého	letecký	k2eAgInSc2d1
simulátoru	simulátor	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Internetová	internetový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
aplikace	aplikace	k1gFnSc2
Cykloplánovač	Cykloplánovač	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
týmu	tým	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.fjfi.cvut.cz/cz/veda-a-vyzkum/spoluprace	https://www.fjfi.cvut.cz/cz/veda-a-vyzkum/spoluprace	k1gFnSc1
<g/>
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
ČVUT	ČVUT	kA
FBMI	FBMI	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Reprezentační	reprezentační	k2eAgFnSc1d1
brožura	brožura	k1gFnSc1
ČVUT	ČVUT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
UCEEB	UCEEB	kA
-	-	kIx~
UNIVERZITNÍ	univerzitní	k2eAgNnSc1d1
CENTRUM	centrum	k1gNnSc1
ENERGETICKY	energeticky	k6eAd1
EFEKTIVNÍCH	efektivní	k2eAgFnPc2d1
BUDOV	budova	k1gFnPc2
|	|	kIx~
UCEEB	UCEEB	kA
<g/>
.	.	kIx.
www.uceeb.cz	www.uceeb.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.ciirc.cvut.cz/cs/	https://www.ciirc.cvut.cz/cs/	k?
<g/>
↑	↑	k?
Czech	Czech	k1gInSc4
Technical	Technical	k1gMnSc4
University	universita	k1gFnSc2
in	in	k?
Prague	Pragu	k1gFnSc2
na	na	k7c6
webu	web	k1gInSc6
QS	QS	kA
Top	topit	k5eAaImRp2nS
Universities	Universities	k1gInSc1
<g/>
↑	↑	k?
O	o	k7c6
Orchestru	orchestr	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
16.11	16.11	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KREJČÍ	Krejčí	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
O	o	k7c4
nás	my	k3xPp1nPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Českého	český	k2eAgNnSc2d1
vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
2010-09-07	2010-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KRAUPNER	KRAUPNER	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orchestry	orchestra	k1gFnSc2
ČVUT	ČVUT	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
ČVUT	ČVUT	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Pozvánka	pozvánka	k1gFnSc1
na	na	k7c4
Společenské	společenský	k2eAgNnSc4d1
setkání	setkání	k1gNnSc4
–	–	k?
podzimní	podzimní	k2eAgInSc1d1
koncert	koncert	k1gInSc1
pořádaný	pořádaný	k2eAgInSc1d1
spolkem	spolek	k1gInSc7
ELEKTRA	ELEKTRA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Spolek	spolek	k1gInSc1
ELEKTRA	ELEKTRA	kA
<g/>
,	,	kIx,
2009-10-26	2009-10-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Koncert	koncert	k1gInSc4
Komorního	komorní	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
ČVUT	ČVUT	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vatikán	Vatikán	k1gInSc1
<g/>
:	:	kIx,
Velvyslanectví	velvyslanectví	k1gNnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
při	při	k7c6
Svatém	svatý	k2eAgInSc6d1
stolci	stolec	k1gInSc6
<g/>
,	,	kIx,
2005-05-17	2005-05-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PRECLÍK	preclík	k1gInSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masaryk	Masaryk	k1gMnSc1
a	a	k8xC
legie	legie	k1gFnSc1
<g/>
,	,	kIx,
váz	váza	k1gFnPc2
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
219	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vydalo	vydat	k5eAaPmAgNnS
nakladatelství	nakladatelství	k1gNnSc1
Paris	Paris	k1gMnSc1
Karviná	Karviná	k1gFnSc1
<g/>
,	,	kIx,
Žižkova	Žižkův	k2eAgFnSc1d1
2379	#num#	k4
(	(	kIx(
<g/>
734	#num#	k4
01	#num#	k4
Karviná	Karviná	k1gFnSc1
<g/>
)	)	kIx)
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Masarykovým	Masarykův	k2eAgNnSc7d1
demokratickým	demokratický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87173	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
↑	↑	k?
PRECLÍK	preclík	k1gInSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
:	:	kIx,
K	k	k7c3
stému	stý	k4xOgNnSc3
výročí	výročí	k1gNnSc3
vzniku	vznik	k1gInSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
in	in	k?
Strojař	strojař	k1gMnSc1
<g/>
:	:	kIx,
časopis	časopis	k1gInSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
leden	leden	k1gInSc1
–	–	k?
červen	červen	k1gInSc1
2020	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XXIX	XXIX	kA
<g/>
.	.	kIx.
,	,	kIx,
dvojčíslo	dvojčíslo	k1gNnSc1
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
591	#num#	k4
<g/>
,	,	kIx,
registrace	registrace	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČR	ČR	kA
E	E	kA
<g/>
13559	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VELFLÍK	VELFLÍK	kA
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
technického	technický	k2eAgNnSc2d1
učení	učení	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
s	s	k7c7
dějinným	dějinný	k2eAgInSc7d1
přehledem	přehled	k1gInSc7
nejstarších	starý	k2eAgFnPc2d3
inženýrských	inženýrský	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
akademií	akademie	k1gFnPc2
a	a	k8xC
ústavů	ústav	k1gInPc2
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgNnPc6
bylo	být	k5eAaImAgNnS
vědám	věda	k1gFnPc3
inženýrským	inženýrský	k2eAgFnPc3d1
nejdříve	dříve	k6eAd3
vyučováno	vyučován	k2eAgNnSc1d1
:	:	kIx,
Pamětní	pamětní	k2eAgInSc4d1
spis	spis	k1gInSc4
na	na	k7c4
oslavu	oslava	k1gFnSc4
založení	založení	k1gNnSc2
inženýrské	inženýrský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
před	před	k7c7
200	#num#	k4
lety	léto	k1gNnPc7
a	a	k8xC
stoletého	stoletý	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
polytechnického	polytechnický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Pražského	pražský	k2eAgInSc2d1
;	;	kIx,
Díl	díl	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
prvního	první	k4xOgNnSc2
vyučování	vyučování	k1gNnSc2
inženýrským	inženýrský	k2eAgFnPc3d1
vědám	věda	k1gFnPc3
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
1686	#num#	k4
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
zakončení	zakončení	k1gNnSc2
utraquistického	utraquistický	k2eAgNnSc2d1
období	období	k1gNnSc2
polytechnického	polytechnický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
v	v	k7c6
Království	království	k1gNnSc6
českém	český	k2eAgNnSc6d1
roku	rok	k1gInSc2
1869	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
VELFLÍK	VELFLÍK	kA
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
<g/>
;	;	kIx,
KOLÁŘ	Kolář	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
technického	technický	k2eAgNnSc2d1
učení	učení	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
s	s	k7c7
dějinným	dějinný	k2eAgInSc7d1
přehledem	přehled	k1gInSc7
nejstarších	starý	k2eAgFnPc2d3
inženýrských	inženýrský	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
akademií	akademie	k1gFnPc2
a	a	k8xC
ústavů	ústav	k1gInPc2
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgNnPc6
bylo	být	k5eAaImAgNnS
vědám	věda	k1gFnPc3
inženýrským	inženýrský	k2eAgFnPc3d1
nejdříve	dříve	k6eAd3
vyučováno	vyučován	k2eAgNnSc1d1
:	:	kIx,
Pamětní	pamětní	k2eAgInSc4d1
spis	spis	k1gInSc4
na	na	k7c4
oslavu	oslava	k1gFnSc4
založení	založení	k1gNnSc2
stavovské	stavovský	k2eAgFnSc2d1
inženýrské	inženýrský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
před	před	k7c7
200	#num#	k4
lety	léto	k1gNnPc7
a	a	k8xC
stoletého	stoletý	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
polytechnického	polytechnický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Pražského	pražský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
matice	matice	k1gFnSc1
technická	technický	k2eAgFnSc1d1
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
brožura	brožura	k1gFnSc1
ČVUT	ČVUT	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
2013	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
České	český	k2eAgFnSc2d1
vysoké	vysoká	k1gFnSc2
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
Reskript	reskript	k1gInSc1
císaře	císař	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
českým	český	k2eAgMnPc3d1
stavům	stav	k1gInPc3
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ČVUT	ČVUT	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
www.cvut.cz	www.cvut.cz	k1gInSc1
Domácí	domácí	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
ČVUT	ČVUT	kA
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
ČVUT	ČVUT	kA
</s>
<s>
Správa	správa	k1gFnSc1
účelových	účelový	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
–	–	k?
koleje	kolej	k1gFnSc2
<g/>
,	,	kIx,
menzy	menza	k1gFnSc2
–	–	k?
www.suz.cvut.cz	www.suz.cvut.cz	k1gMnSc1
</s>
<s>
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
ČVUT	ČVUT	kA
v	v	k7c6
datech	datum	k1gNnPc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
univerzity	univerzita	k1gFnSc2
</s>
<s>
Milada	Milada	k1gFnSc1
Sekyrková	sekyrkový	k2eAgFnSc1d1
<g/>
:	:	kIx,
Rozdělení	rozdělení	k1gNnSc1
polytechniky	polytechnika	k1gFnSc2
–	–	k?
precedens	precedens	k1gNnSc1
pro	pro	k7c4
univerzitu	univerzita	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
in	in	k?
Práce	práce	k1gFnPc1
z	z	k7c2
dějin	dějiny	k1gFnPc2
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
svazek	svazek	k1gInSc4
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumné	výzkumný	k2eAgFnSc2d1
centrum	centrum	k1gNnSc4
pro	pro	k7c4
dějiny	dějiny	k1gFnPc4
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7285-027-X	80-7285-027-X	k4
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
energeticky	energeticky	k6eAd1
efektivních	efektivní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
</s>
<s>
QS	QS	kA
World	World	k1gInSc4
University	universita	k1gFnSc2
RankingsNejprestižnější	RankingsNejprestižný	k2eAgInSc4d2
žebříček	žebříček	k1gInSc4
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
</s>
<s>
www.StudyAtCTU.com	www.StudyAtCTU.com	k1gInSc1
Portál	portál	k1gInSc1
pro	pro	k7c4
zahraniční	zahraniční	k2eAgMnPc4d1
studenty	student	k1gMnPc4
–	–	k?
konkrétně	konkrétně	k6eAd1
zaměřený	zaměřený	k2eAgInSc4d1
na	na	k7c4
ČVUT	ČVUT	kA
</s>
<s>
www.StudyInPrague.com	www.StudyInPrague.com	k1gInSc1
Portál	portál	k1gInSc1
pro	pro	k7c4
zahraniční	zahraniční	k2eAgMnPc4d1
studenty	student	k1gMnPc4
</s>
<s>
Studentská	studentský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ČVUT	ČVUT	kA
</s>
<s>
Portál	portál	k1gInSc1
Student	student	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cvut	cvut	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
International	Internationat	k5eAaImAgMnS,k5eAaPmAgMnS
Student	student	k1gMnSc1
Club	club	k1gInSc4
</s>
<s>
IAESTE	IAESTE	kA
</s>
<s>
Studuj	studovat	k5eAaImRp2nS
na	na	k7c4
CVUT	CVUT	kA
</s>
<s>
Studentské	studentský	k2eAgFnPc1d1
formule	formule	k1gFnSc1
–	–	k?
CTU	CTU	kA
CarTech	CarTech	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
ČVUT	ČVUT	kA
<g/>
)	)	kIx)
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Fakulta	fakulta	k1gFnSc1
stavební	stavební	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
strojní	strojní	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
jaderná	jaderný	k2eAgFnSc1d1
a	a	k8xC
fyzikálně	fyzikálně	k6eAd1
inženýrská	inženýrský	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
architektury	architektura	k1gFnSc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
dopravní	dopravní	k2eAgFnSc1d1
•	•	k?
Fakulta	fakulta	k1gFnSc1
biomedicínského	biomedicínský	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
•	•	k?
Fakulta	fakulta	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
Ústavy	ústava	k1gFnSc2
a	a	k8xC
pracoviště	pracoviště	k1gNnSc2
</s>
<s>
Kloknerův	Kloknerův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
•	•	k?
Masarykův	Masarykův	k2eAgInSc1d1
ústav	ústav	k1gInSc1
vyšších	vysoký	k2eAgFnPc2d2
studií	studie	k1gFnPc2
•	•	k?
Český	český	k2eAgInSc1d1
institut	institut	k1gInSc1
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
robotiky	robotika	k1gFnSc2
a	a	k8xC
kybernetiky	kybernetika	k1gFnSc2
•	•	k?
Ústav	ústav	k1gInSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
sportu	sport	k1gInSc2
•	•	k?
Univerzitní	univerzitní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
energeticky	energeticky	k6eAd1
efektivních	efektivní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
•	•	k?
Výpočetní	výpočetní	k2eAgNnSc1d1
a	a	k8xC
informační	informační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
•	•	k?
Technologické	technologický	k2eAgNnSc1d1
a	a	k8xC
inovační	inovační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
•	•	k?
Výzkumné	výzkumný	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
průmyslového	průmyslový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
•	•	k?
Ústav	ústav	k1gInSc1
technické	technický	k2eAgFnSc2d1
a	a	k8xC
experimentální	experimentální	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
•	•	k?
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
radiochemii	radiochemie	k1gFnSc4
a	a	k8xC
radiační	radiační	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
Technologie	technologie	k1gFnSc2
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
</s>
<s>
Česká	český	k2eAgFnSc1d1
technika	technika	k1gFnSc1
•	•	k?
Pražská	pražský	k2eAgFnSc1d1
technika	technika	k1gFnSc1
•	•	k?
Acta	Acta	k1gMnSc1
Polytechnica	Polytechnica	k1gMnSc1
•	•	k?
Klementinum	Klementinum	k1gNnSc4
•	•	k?
Betlémská	betlémský	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
•	•	k?
Seznam	seznam	k1gInSc4
rektorů	rektor	k1gMnPc2
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Česká	český	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
polytechnická	polytechnický	k2eAgFnSc1d1
Jihlava	Jihlava	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
•	•	k?
Veterinární	veterinární	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
báňská	báňský	k2eAgFnSc1d1
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
Opava	Opava	k1gFnSc1
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
Zlín	Zlín	k1gInSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010710137	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2173	#num#	k4
8213	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81127021	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
143122915	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81127021	#num#	k4
</s>
