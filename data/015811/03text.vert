<s>
Tobias	Tobias	k1gMnSc1
Michael	Michael	k1gMnSc1
Carel	Carel	k1gMnSc1
Asser	Asser	k1gMnSc1
</s>
<s>
Tobias	Tobias	k1gMnSc1
Michael	Michael	k1gMnSc1
Carel	Carel	k1gMnSc1
Asser	Asser	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1838	#num#	k4
<g/>
Amsterdam	Amsterdam	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1913	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
The	The	k1gFnSc1
Hague	Hague	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
LeidenuAmsterdamská	LeidenuAmsterdamský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
soudce	soudce	k1gMnSc1
<g/>
,	,	kIx,
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
advokát	advokát	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
diplomat	diplomat	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Athenaeum	Athenaeum	k1gInSc1
Illustre	Illustr	k1gInSc5
of	of	k?
Amsterdam	Amsterdam	k1gInSc1
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
–	–	k?
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
<g/>
Amsterdamská	amsterdamský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
–	–	k?
<g/>
1893	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
mír	mír	k1gInSc4
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
čestný	čestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
Humboldtovy	Humboldtův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
<g/>
čestný	čestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Leidenu	Leideno	k1gNnSc6
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
<g/>
čestný	čestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Cambridgičestný	Cambridgičestný	k2eAgMnSc1d1
doktor	doktor	k1gMnSc1
Boloňské	boloňský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
Děti	dítě	k1gFnPc1
</s>
<s>
Carel	Carel	k1gMnSc1
Daniël	Daniël	k1gMnSc1
Asser	Asser	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Carel	Carel	k1gMnSc1
Daniël	Daniël	k1gMnSc1
Asser	Asser	k1gMnSc1
Funkce	funkce	k1gFnSc1
</s>
<s>
President	president	k1gMnSc1
of	of	k?
the	the	k?
Institut	institut	k1gInSc1
de	de	k?
Droit	Droit	k1gInSc1
International	International	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1897	#num#	k4
<g/>
)	)	kIx)
<g/>
President	president	k1gMnSc1
of	of	k?
the	the	k?
Institut	institut	k1gInSc1
de	de	k?
Droit	Droit	k1gInSc1
International	International	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1898	#num#	k4
<g/>
)	)	kIx)
<g/>
Minister	Minister	k1gInSc1
of	of	k?
State	status	k1gInSc5
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tobias	Tobias	k1gMnSc1
Michael	Michael	k1gMnSc1
Carel	Carel	k1gMnSc1
Asser	Asser	k1gMnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1838	#num#	k4
<g/>
,	,	kIx,
Amsterdam	Amsterdam	k1gInSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1913	#num#	k4
<g/>
,	,	kIx,
Haag	Haag	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
nizozemský	nizozemský	k2eAgMnSc1d1
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
míru	mír	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
Alfredem	Alfred	k1gMnSc7
Friedem	Fried	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Alfredem	Alfred	k1gMnSc7
Friedem	Fried	k1gMnSc7
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
míru	mír	k1gInSc2
za	za	k7c4
vytvoření	vytvoření	k1gNnSc4
Stálého	stálý	k2eAgMnSc2d1
rozhodčího	rozhodčí	k1gMnSc2
soudu	soud	k1gInSc2
na	na	k7c6
první	první	k4xOgFnSc6
haagské	haagský	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
obhajoval	obhajovat	k5eAaImAgMnS
vytvoření	vytvoření	k1gNnSc4
mezinárodní	mezinárodní	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
založení	založení	k1gNnSc3
Revue	revue	k1gFnSc2
de	de	k?
Droit	Droit	k1gMnSc1
International	International	k1gMnSc1
et	et	k?
de	de	k?
Législation	Législation	k1gInSc4
Comparée	Comparé	k1gFnSc2
<g/>
,	,	kIx,
Ústavu	ústav	k1gInSc2
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
a	a	k8xC
ke	k	k7c3
vzniku	vznik	k1gInSc3
Haagské	haagský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
Asserovi	Asser	k1gMnSc6
je	být	k5eAaImIp3nS
pojmenovaný	pojmenovaný	k2eAgInSc1d1
institut	institut	k1gInSc1
soukromého	soukromý	k2eAgNnSc2d1
a	a	k8xC
veřejného	veřejný	k2eAgNnSc2d1
mezinárodního	mezinárodní	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
evropského	evropský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
a	a	k8xC
mezinárodních	mezinárodní	k2eAgFnPc2d1
obchodních	obchodní	k2eAgFnPc2d1
arbitráží	arbitráž	k1gFnPc2
T.	T.	kA
<g/>
M.	M.	kA
<g/>
C.	C.	kA
Asser	Asser	k1gMnSc1
Instituut	Instituut	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
nizozemském	nizozemský	k2eAgInSc6d1
Haagu	Haag	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
strýcem	strýc	k1gMnSc7
byl	být	k5eAaImAgMnS
průkopník	průkopník	k1gMnSc1
fotografie	fotografia	k1gFnSc2
v	v	k7c6
Nizozemsku	Nizozemsko	k1gNnSc6
Eduard	Eduard	k1gMnSc1
Isaac	Isaac	k1gFnSc4
Asser	Assra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tobias	Tobias	k1gMnSc1
Michael	Michael	k1gMnSc1
Carel	Carel	k1gMnSc1
Asser	Asser	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tobias	Tobiasa	k1gFnPc2
Michael	Michael	k1gMnSc1
Carel	Carel	k1gMnSc1
Asser	Assra	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Biografie	biografie	k1gFnSc1
na	na	k7c6
oficiálním	oficiální	k2eAgInSc6d1
webu	web	k1gInSc6
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
</s>
<s>
T.	T.	kA
<g/>
M.	M.	kA
<g/>
C.	C.	kA
Asser	Asser	k1gMnSc1
Instituut	Instituut	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
mír	mír	k1gInSc4
</s>
<s>
Henri	Henri	k1gNnSc1
Dunant	Dunant	k1gMnSc1
/	/	kIx~
Frédéric	Frédéric	k1gMnSc1
Passy	Passa	k1gFnSc2
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Élie	Élie	k1gFnSc1
Ducommun	Ducommun	k1gMnSc1
/	/	kIx~
Charles	Charles	k1gMnSc1
Gobat	Gobat	k2eAgMnSc1d1
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
William	William	k1gInSc1
Cremer	Cremer	k1gInSc1
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Institut	institut	k1gInSc1
de	de	k?
droit	droit	k1gInSc1
international	internationat	k5eAaImAgInS,k5eAaPmAgInS
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Bertha	Bertha	k1gFnSc1
von	von	k1gInSc1
Suttnerová	Suttnerová	k1gFnSc1
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Ernesto	Ernesto	k6eAd1
Moneta	moneta	k1gFnSc1
/	/	kIx~
Louis	louis	k1gInSc1
Renault	renault	k1gInSc1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Klas	klas	k1gInSc1
Arnoldson	Arnoldson	k1gInSc1
/	/	kIx~
Fredrik	Fredrik	k1gMnSc1
Bajer	Bajer	k1gMnSc1
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
A.	A.	kA
<g/>
M.	M.	kA
<g/>
F.	F.	kA
Beernaert	Beernaert	k1gMnSc1
/	/	kIx~
Paul	Paul	k1gMnSc1
Estournelles	Estournelles	k1gMnSc1
de	de	k?
Constant	Constant	k1gMnSc1
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Stálý	stálý	k2eAgInSc1d1
mezinárodní	mezinárodní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
míru	mír	k1gInSc2
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Tobias	Tobias	k1gMnSc1
Asser	Asser	k1gMnSc1
/	/	kIx~
Alfred	Alfred	k1gMnSc1
Fried	Fried	k1gMnSc1
(	(	kIx(
<g/>
1911	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Elihu	Eliha	k1gMnSc4
Root	Root	k1gMnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Henri	Henri	k1gNnSc1
La	la	k1gNnSc2
Fontaine	Fontain	k1gInSc5
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
červený	červený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Woodrow	Woodrow	k?
Wilson	Wilson	k1gMnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Léon	Léon	k1gInSc1
Bourgeois	Bourgeois	k1gFnSc2
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Hjalmar	Hjalmar	k1gInSc1
Branting	Branting	k1gInSc1
/	/	kIx~
Christian	Christian	k1gMnSc1
Lange	Lang	k1gMnSc2
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Fridtjof	Fridtjof	k1gInSc1
Nansen	Nansen	k1gInSc1
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Austen	Austen	k2eAgMnSc1d1
Chamberlain	Chamberlain	k1gMnSc1
/	/	kIx~
Charles	Charles	k1gMnSc1
Dawes	Dawes	k1gMnSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2002149964	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
119074680	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8365	#num#	k4
3824	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81063374	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
24716169	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81063374	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Nizozemsko	Nizozemsko	k1gNnSc1
|	|	kIx~
Právo	právo	k1gNnSc1
</s>
