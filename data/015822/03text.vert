<s>
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
(	(	kIx(
<g/>
matematik	matematik	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Prof.	prof.	kA
Dr	dr	kA
<g/>
.	.	kIx.
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1862	#num#	k4
<g/>
ŘepníkyRakouské	ŘepníkyRakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1931	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Olšanské	olšanský	k2eAgInPc1d1
hřbitovy	hřbitov	k1gInPc1
(	(	kIx(
<g/>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
14	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
Národnost	národnost	k1gFnSc1
</s>
<s>
Češi	Čech	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
–	–	k?
<g/>
1886	#num#	k4
<g/>
)	)	kIx)
<g/>
Spolková	spolkový	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
technická	technický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Curychu	Curych	k1gInSc6
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgNnSc2d1
Povolání	povolání	k1gNnSc2
</s>
<s>
pedagog	pedagog	k1gMnSc1
a	a	k8xC
matematik	matematik	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1895	#num#	k4
<g/>
–	–	k?
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
<g/>
Vysoké	vysoká	k1gFnSc3
učení	učení	k1gNnSc2
technické	technický	k2eAgInPc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
–	–	k?
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
<g/>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
–	–	k?
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1862	#num#	k4
Řepníky	řepník	k1gInPc7
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1931	#num#	k4
<g/>
,	,	kIx,
Praha-Vinohrady	Praha-Vinohrada	k1gFnPc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabýval	zabývat	k5eAaImAgMnS
se	se	k3xPyFc4
geometrií	geometrie	k1gFnSc7
<g/>
,	,	kIx,
zejména	zejména	k9
deskriptivní	deskriptivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
německé	německý	k2eAgFnSc2d1
reálky	reálka	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Kampě	Kampa	k1gFnSc6
pokračoval	pokračovat	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1881	#num#	k4
<g/>
–	–	k?
<g/>
1886	#num#	k4
ve	v	k7c6
studiu	studio	k1gNnSc6
na	na	k7c6
české	český	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
na	na	k7c6
české	český	k2eAgFnSc6d1
technice	technika	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1886	#num#	k4
<g/>
–	–	k?
<g/>
1891	#num#	k4
byl	být	k5eAaImAgInS
asistentem	asistent	k1gMnSc7
na	na	k7c6
technice	technika	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
suploval	suplovat	k5eAaImAgMnS
přednášky	přednáška	k1gFnPc4
za	za	k7c4
Františka	František	k1gMnSc4
Tilšera	Tilšer	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
poslancem	poslanec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
studuje	studovat	k5eAaImIp3nS
Sobotka	Sobotka	k1gMnSc1
u	u	k7c2
Wilhelma	Wilhelmum	k1gNnSc2
Fiedlera	Fiedler	k1gMnSc2
na	na	k7c6
technice	technika	k1gFnSc6
v	v	k7c6
Curychu	Curych	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
suplování	suplování	k1gNnSc6
za	za	k7c4
profesora	profesor	k1gMnSc4
Tilšera	Tilšer	k1gMnSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1893	#num#	k4
odjel	odjet	k5eAaPmAgMnS
na	na	k7c4
studijní	studijní	k2eAgInSc4d1
pobyt	pobyt	k1gInSc4
do	do	k7c2
Vratislavi	Vratislav	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
nenašel	najít	k5eNaPmAgInS
místo	místo	k1gNnSc4
ani	ani	k8xC
na	na	k7c4
vysoké	vysoký	k2eAgNnSc4d1
<g/>
,	,	kIx,
ani	ani	k8xC
na	na	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
na	na	k7c4
reálku	reálka	k1gFnSc4
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
okrese	okres	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
asistentem	asistent	k1gMnSc7
deskriptivní	deskriptivní	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
technice	technika	k1gFnSc6
a	a	k8xC
v	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1897	#num#	k4
pak	pak	k6eAd1
mimořádným	mimořádný	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
deskriptivní	deskriptivní	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
<g/>
,	,	kIx,
projektivní	projektivní	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
a	a	k8xC
grafického	grafický	k2eAgNnSc2d1
počítání	počítání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
návratu	návrat	k1gInSc3
domů	dům	k1gInPc2
ho	on	k3xPp3gMnSc4
přiměla	přimět	k5eAaPmAgFnS
nabídka	nabídka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
dostal	dostat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
profesorem	profesor	k1gMnSc7
deskriptivní	deskriptivní	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
na	na	k7c6
nově	nově	k6eAd1
zřízené	zřízený	k2eAgFnSc6d1
české	český	k2eAgFnSc6d1
technice	technika	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1900	#num#	k4
<g/>
–	–	k?
<g/>
1901	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c6
tamním	tamní	k2eAgInSc6d1
odboru	odbor	k1gInSc6
stavebního	stavební	k2eAgNnSc2d1
inženýrství	inženýrství	k1gNnSc2
děkanem	děkan	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
odešel	odejít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
Sobotka	Sobotka	k1gMnSc1
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
profesorem	profesor	k1gMnSc7
matematiky	matematika	k1gFnSc2
na	na	k7c6
filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
na	na	k7c6
ní	on	k3xPp3gFnSc6
působil	působit	k5eAaImAgMnS
až	až	k8xS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hrob	hrob	k1gInSc1
Jana	Jan	k1gMnSc2
Sobotky	Sobotka	k1gMnSc2
na	na	k7c6
Olšanských	olšanský	k2eAgInPc6d1
hřbitovech	hřbitov	k1gInPc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1891	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Růženou	Růžena	k1gFnSc7
z	z	k7c2
Helmingerů	Helminger	k1gInPc2
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
—	—	k?
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
Sobotkovi	Sobotkův	k2eAgMnPc1d1
měli	mít	k5eAaImAgMnP
syna	syn	k1gMnSc4
Ericha	Erich	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
S	s	k7c7
publikační	publikační	k2eAgFnSc7d1
prací	práce	k1gFnSc7
začal	začít	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
oborem	obor	k1gInSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
vědeckého	vědecký	k2eAgInSc2d1
zájmu	zájem	k1gInSc2
byla	být	k5eAaImAgFnS
deskriptivní	deskriptivní	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
práce	práce	k1gFnPc1
jednak	jednak	k8xC
ze	z	k7c2
zobrazovacích	zobrazovací	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
z	z	k7c2
konstruktivní	konstruktivní	k2eAgFnSc2d1
geometrie	geometrie	k1gFnSc2
křivek	křivka	k1gFnPc2
a	a	k8xC
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
významné	významný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
jeho	jeho	k3xOp3gFnPc4
práce	práce	k1gFnPc4
z	z	k7c2
rovnoběžné	rovnoběžný	k2eAgFnSc2d1
axonometrie	axonometrie	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
přešly	přejít	k5eAaPmAgInP
do	do	k7c2
literatury	literatura	k1gFnSc2
tzv.	tzv.	kA
Sobotkovy	Sobotkův	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
uvádějí	uvádět	k5eAaImIp3nP
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
kosoúhlou	kosoúhlý	k2eAgFnSc7d1
axonometrií	axonometrie	k1gFnSc7
a	a	k8xC
sdruženými	sdružený	k2eAgInPc7d1
pravoúhlými	pravoúhlý	k2eAgInPc7d1
průměty	průmět	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgMnS
však	však	k9
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
geometrie	geometrie	k1gFnSc2
(	(	kIx(
<g/>
diferenciální	diferenciální	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
<g/>
,	,	kIx,
projektivní	projektivní	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
<g/>
,	,	kIx,
elementární	elementární	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
<g/>
,	,	kIx,
kinematická	kinematický	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
představuje	představovat	k5eAaImIp3nS
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
sto	sto	k4xCgNnSc4
často	často	k6eAd1
dosti	dosti	k6eAd1
obšírných	obšírný	k2eAgNnPc2d1
pojednání	pojednání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
stěžejní	stěžejní	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
Jana	Jan	k1gMnSc2
Sobotky	Sobotka	k1gMnSc2
patří	patřit	k5eAaImIp3nP
monografie	monografie	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Deskriptivní	deskriptivní	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
promítání	promítání	k1gNnSc2
paralelního	paralelní	k2eAgInSc2d1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1
geometrie	geometrie	k1gFnSc1
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c6
geometrických	geometrický	k2eAgFnPc6d1
příbuznostech	příbuznost	k1gFnPc6
(	(	kIx(
<g/>
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c4
první	první	k4xOgInSc4
z	z	k7c2
nich	on	k3xPp3gInPc2
podal	podat	k5eAaPmAgInS
v	v	k7c6
mnohých	mnohý	k2eAgFnPc6d1
částech	část	k1gFnPc6
zcela	zcela	k6eAd1
původní	původní	k2eAgInSc4d1
výklad	výklad	k1gInSc4
základních	základní	k2eAgFnPc2d1
zobrazovacích	zobrazovací	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
založených	založený	k2eAgFnPc2d1
na	na	k7c6
rovnoběžném	rovnoběžný	k2eAgNnSc6d1
promítání	promítání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
text	text	k1gInSc1
(	(	kIx(
<g/>
licence	licence	k1gFnSc1
CC-BY	CC-BY	k1gFnSc2
3.0	3.0	k4
Unported	Unported	k1gMnSc1
<g/>
)	)	kIx)
ze	z	k7c2
stránky	stránka	k1gFnSc2
z	z	k7c2
webu	web	k1gInSc2
Významní	významný	k2eAgMnPc1d1
matematici	matematik	k1gMnPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
původního	původní	k2eAgInSc2d1
textu	text	k1gInSc2
<g/>
:	:	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Folta	Folta	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Šišma	Šišma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Archiv	archiv	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
Matrika	matrika	k1gFnSc1
zemřelých	zemřelý	k1gMnPc2
církve	církev	k1gFnSc2
československé	československý	k2eAgFnSc2d1
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
<g/>
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSVIN	ČSVIN	kA
Z	z	k7c2
<g/>
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
343	#num#	k4
<g/>
↑	↑	k?
Matrika	matrika	k1gFnSc1
oddaných	oddaný	k2eAgInPc6d1
<g/>
,	,	kIx,
sv.	sv.	kA
Ludmila	Ludmila	k1gFnSc1
<g/>
,	,	kIx,
Král	Král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinohrady	Vinohrady	k1gInPc1
<g/>
,	,	kIx,
1891	#num#	k4
<g/>
-	-	kIx~
<g/>
1893	#num#	k4
<g/>
,	,	kIx,
snímek	snímek	k1gInSc1
23	#num#	k4
<g/>
↑	↑	k?
Soupis	soupis	k1gInSc1
pražských	pražský	k2eAgInPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
Sobotka	Sobotka	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
1862	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kadeřávek	Kadeřávek	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
pro	pro	k7c4
pěstování	pěstování	k1gNnSc4
mathematiky	mathematika	k1gFnSc2
a	a	k8xC
fysiky	fysika	k1gFnSc2
<g/>
.	.	kIx.
52	#num#	k4
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vorovka	Vorovka	k1gFnSc1
<g/>
,	,	kIx,
<g/>
K.	K.	kA
<g/>
:	:	kIx,
Sobotkovy	Sobotkův	k2eAgInPc1d1
názory	názor	k1gInPc1
didaktické	didaktický	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
pro	pro	k7c4
pěstování	pěstování	k1gNnSc4
mathematiky	mathematika	k1gFnSc2
a	a	k8xC
fysiky	fysika	k1gFnSc2
<g/>
.	.	kIx.
52	#num#	k4
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
Vančura	Vančura	k1gMnSc1
<g/>
,	,	kIx,
Z.	Z.	kA
<g/>
:	:	kIx,
Sté	stý	k4xOgNnSc4
výročí	výročí	k1gNnSc4
narození	narození	k1gNnSc2
profesora	profesor	k1gMnSc2
Jana	Jan	k1gMnSc2
Sobotky	Sobotka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
pro	pro	k7c4
pěstování	pěstování	k1gNnSc4
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
87	#num#	k4
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
242	#num#	k4
<g/>
-	-	kIx~
<g/>
525	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
:	:	kIx,
O	o	k7c6
životě	život	k1gInSc6
a	a	k8xC
díle	díl	k1gInSc6
profesora	profesor	k1gMnSc2
Jana	Jan	k1gMnSc2
Sobotky	Sobotka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokroky	pokrok	k1gInPc1
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
a	a	k8xC
astronomie	astronomie	k1gFnSc2
<g/>
.	.	kIx.
7	#num#	k4
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
355	#num#	k4
<g/>
-	-	kIx~
<g/>
359	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hrůza	Hrůza	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Události	událost	k1gFnSc6
na	na	k7c4
VUT	VUT	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
9	#num#	k4
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
č.	č.	k?
3	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Klapka	Klapka	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
věda	věda	k1gFnSc1
<g/>
.	.	kIx.
13	#num#	k4
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kotyk	Kotyk	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Za	za	k7c4
prof.	prof.	kA
dr	dr	kA
<g/>
.	.	kIx.
Janem	Jan	k1gMnSc7
Sobotkou	Sobotka	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhledy	rozhled	k1gInPc4
matematicko-fyzikální	matematicko-fyzikální	k2eAgMnSc1d1
<g/>
.	.	kIx.
41	#num#	k4
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
42	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Košťál	Košťál	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
:	:	kIx,
Vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
pobočky	pobočka	k1gFnSc2
JČMF	JČMF	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednota	jednota	k1gFnSc1
československých	československý	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
a	a	k8xC
fyziků	fyzik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
:	:	kIx,
Prof.	prof.	kA
PhDr.	PhDr.	kA
h.	h.	k?
c.	c.	k?
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matematika	matematika	k1gFnSc1
ve	v	k7c6
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
12	#num#	k4
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
-	-	kIx~
<g/>
62	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
623	#num#	k4
<g/>
-	-	kIx~
<g/>
626	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bydžovský	Bydžovský	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vyčichlo	vyčichnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
F.	F.	kA
<g/>
:	:	kIx,
Studium	studium	k1gNnSc1
a	a	k8xC
hodnocení	hodnocení	k1gNnSc1
díla	dílo	k1gNnSc2
prof.	prof.	kA
J.	J.	kA
Sobotky	Sobotka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MÜ	MÜ	kA
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Sobotkova	Sobotkův	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
matematiků	matematik	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Autor	autor	k1gMnSc1
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
(	(	kIx(
<g/>
matematik	matematik	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1
seznam	seznam	k1gInSc1
Sobotkových	Sobotkových	k2eAgFnPc2d1
prací	práce	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1120252	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5613	#num#	k4
3030	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83901845	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
</s>
