<p>
<s>
Kooperativa	kooperativa	k1gFnSc1	kooperativa
Národní	národní	k2eAgFnSc1d1	národní
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Kooperativa	kooperativa	k1gFnSc1	kooperativa
NBL	NBL	kA	NBL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
česká	český	k2eAgFnSc1d1	Česká
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2014	[number]	k4	2014
nesla	nést	k5eAaImAgFnS	nést
jméno	jméno	k1gNnSc4	jméno
Mattoni	Mattoň	k1gFnSc6	Mattoň
NBL	NBL	kA	NBL
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
týmem	tým	k1gInSc7	tým
soutěže	soutěž	k1gFnSc2	soutěž
je	být	k5eAaImIp3nS	být
ČEZ	ČEZ	kA	ČEZ
Basketball	Basketball	k1gInSc1	Basketball
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
13	[number]	k4	13
titulů	titul	k1gInPc2	titul
z	z	k7c2	z
dvaadvaceti	dvaadvacet	k4xCc2	dvaadvacet
odehraných	odehraný	k2eAgInPc2d1	odehraný
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c4	po
rozdělení	rozdělení	k1gNnSc4	rozdělení
Československa	Československo	k1gNnSc2	Československo
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
samostatné	samostatný	k2eAgInPc4d1	samostatný
státy	stát	k1gInPc4	stát
byl	být	k5eAaImAgInS	být
ročník	ročník	k1gInSc1	ročník
Československé	československý	k2eAgFnSc2d1	Československá
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
ligy	liga	k1gFnSc2	liga
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
93	[number]	k4	93
ukončen	ukončit	k5eAaPmNgInS	ukončit
už	už	k6eAd1	už
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
soutěže	soutěž	k1gFnSc2	soutěž
a	a	k8xC	a
play-off	playff	k1gInSc4	play-off
nebylo	být	k5eNaImAgNnS	být
hráno	hrát	k5eAaImNgNnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Kluby	klub	k1gInPc1	klub
československé	československý	k2eAgFnSc2d1	Československá
ligy	liga	k1gFnSc2	liga
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
samostatných	samostatný	k2eAgFnPc2d1	samostatná
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
národních	národní	k2eAgFnPc2d1	národní
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dostaly	dostat	k5eAaPmAgFnP	dostat
jména	jméno	k1gNnSc2	jméno
Česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
a	a	k8xC	a
Extraliga	extraliga	k1gFnSc1	extraliga
muži	muž	k1gMnPc1	muž
(	(	kIx(	(
<g/>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
zaregistrováno	zaregistrovat	k5eAaPmNgNnS	zaregistrovat
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
federace	federace	k1gFnSc1	federace
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Asociace	asociace	k1gFnSc1	asociace
ligových	ligový	k2eAgInPc2d1	ligový
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
ALK	ALK	kA	ALK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
zakládajícími	zakládající	k2eAgInPc7d1	zakládající
členy	člen	k1gInPc7	člen
bylo	být	k5eAaImAgNnS	být
sedm	sedm	k4xCc1	sedm
českých	český	k2eAgInPc2d1	český
klubů	klub	k1gInPc2	klub
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
šest	šest	k4xCc1	šest
českých	český	k2eAgInPc2d1	český
klubů	klub	k1gInPc2	klub
žen	žena	k1gFnPc2	žena
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
1	[number]	k4	1
<g/>
.	.	kIx.	.
československé	československý	k2eAgFnSc2d1	Československá
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Ustavující	ustavující	k2eAgFnSc1d1	ustavující
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
přijala	přijmout	k5eAaPmAgFnS	přijmout
Statut	statut	k1gInSc4	statut
ALK	ALK	kA	ALK
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
zvolení	zvolený	k2eAgMnPc1d1	zvolený
funkcionáři	funkcionář	k1gMnPc1	funkcionář
ALK	ALK	kA	ALK
–	–	k?	–
guvernér	guvernér	k1gMnSc1	guvernér
Miloš	Miloš	k1gMnSc1	Miloš
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
sekce	sekce	k1gFnSc2	sekce
ligy	liga	k1gFnSc2	liga
mužů	muž	k1gMnPc2	muž
Ing.	ing.	kA	ing.
Pavel	Pavel	k1gMnSc1	Pavel
Majerík	Majerík	k1gMnSc1	Majerík
a	a	k8xC	a
vedoucí	vedoucí	k1gMnSc1	vedoucí
sekce	sekce	k1gFnSc2	sekce
ligy	liga	k1gFnSc2	liga
žen	žena	k1gFnPc2	žena
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Šíp	Šíp	k1gMnSc1	Šíp
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1993	[number]	k4	1993
se	se	k3xPyFc4	se
Miloš	Miloš	k1gMnSc1	Miloš
Pražák	Pražák	k1gMnSc1	Pražák
vzdal	vzdát	k5eAaPmAgMnS	vzdát
funkce	funkce	k1gFnSc2	funkce
guvernéra	guvernér	k1gMnSc2	guvernér
a	a	k8xC	a
místo	místo	k7c2	místo
něho	on	k3xPp3gNnSc2	on
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Křivý	křivý	k2eAgInSc1d1	křivý
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
ALK	ALK	kA	ALK
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
Asociace	asociace	k1gFnSc1	asociace
ženských	ženský	k2eAgInPc2d1	ženský
ligových	ligový	k2eAgInPc2d1	ligový
klubů	klub	k1gInPc2	klub
(	(	kIx(	(
<g/>
AŽLK	AŽLK	kA	AŽLK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
ALK	ALK	kA	ALK
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bříza	Bříza	k1gMnSc1	Bříza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc1d1	herní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
soutěž	soutěž	k1gFnSc1	soutěž
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sestupování	sestupování	k1gNnSc3	sestupování
klubů	klub	k1gInPc2	klub
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
české	český	k2eAgFnSc2d1	Česká
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
soutěže	soutěž	k1gFnSc2	soutěž
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
ligy	liga	k1gFnSc2	liga
do	do	k7c2	do
NBL	NBL	kA	NBL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
části	část	k1gFnPc1	část
hrané	hraný	k2eAgFnPc1d1	hraná
systémem	systém	k1gInSc7	systém
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
a	a	k8xC	a
play-off	playff	k1gInSc4	play-off
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
osm	osm	k4xCc4	osm
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
družstev	družstvo	k1gNnPc2	družstvo
hraje	hrát	k5eAaImIp3nS	hrát
vylučovacím	vylučovací	k2eAgInSc7d1	vylučovací
způsobem	způsob	k1gInSc7	způsob
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
vítězná	vítězný	k2eAgNnPc4d1	vítězné
utkání	utkání	k1gNnPc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
základní	základní	k2eAgFnSc7d1	základní
částí	část	k1gFnSc7	část
a	a	k8xC	a
play-off	playff	k1gMnSc1	play-off
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
letech	let	k1gInPc6	let
hrála	hrát	k5eAaImAgFnS	hrát
nadstavbová	nadstavbový	k2eAgFnSc1d1	nadstavbová
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
část	část	k1gFnSc1	část
hrána	hrát	k5eAaImNgFnS	hrát
nejprve	nejprve	k6eAd1	nejprve
dvakrát	dvakrát	k6eAd1	dvakrát
každý	každý	k3xTgInSc1	každý
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
(	(	kIx(	(
<g/>
22	[number]	k4	22
kol	kolo	k1gNnPc2	kolo
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
dalších	další	k2eAgNnPc2d1	další
10	[number]	k4	10
kol	kolo	k1gNnPc2	kolo
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
skupinách	skupina	k1gFnPc6	skupina
(	(	kIx(	(
<g/>
lepších	dobrý	k2eAgMnPc2d2	lepší
šest	šest	k4xCc4	šest
zvlášť	zvlášť	k6eAd1	zvlášť
a	a	k8xC	a
horších	zlý	k2eAgNnPc2d2	horší
šest	šest	k4xCc4	šest
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
část	část	k1gFnSc1	část
hrána	hrát	k5eAaImNgFnS	hrát
systémem	systém	k1gInSc7	systém
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
každý	každý	k3xTgInSc1	každý
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
(	(	kIx(	(
<g/>
celkem	celek	k1gInSc7	celek
44	[number]	k4	44
kol	kolo	k1gNnPc2	kolo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
bez	bez	k7c2	bez
účasti	účast	k1gFnSc2	účast
Nymburka	Nymburk	k1gInSc2	Nymburk
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následovala	následovat	k5eAaImAgFnS	následovat
nadstavbová	nadstavbový	k2eAgFnSc1d1	nadstavbová
část	část	k1gFnSc1	část
a	a	k8xC	a
předkolo	předkolo	k1gNnSc1	předkolo
play-off	playff	k1gMnSc1	play-off
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Týmy	tým	k1gInPc7	tým
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2019	[number]	k4	2019
<g/>
/	/	kIx~	/
<g/>
2020	[number]	k4	2020
==	==	k?	==
</s>
</p>
<p>
<s>
BC	BC	kA	BC
Geosan	Geosan	k1gInSc1	Geosan
Kolín	Kolín	k1gInSc1	Kolín
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Armex	Armex	k1gInSc1	Armex
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
<p>
<s>
BK	BK	kA	BK
JIP	JIP	kA	JIP
Pardubice	Pardubice	k1gInPc1	Pardubice
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Olomoucko	Olomoucko	k1gNnSc1	Olomoucko
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Opava	Opava	k1gFnSc1	Opava
</s>
</p>
<p>
<s>
ČEZ	ČEZ	kA	ČEZ
Basketball	Basketball	k1gInSc1	Basketball
Nymburk	Nymburk	k1gInSc1	Nymburk
</s>
</p>
<p>
<s>
DEKSTONE	DEKSTONE	kA	DEKSTONE
Tuři	tur	k1gMnPc5	tur
Svitavy	Svitava	k1gFnPc1	Svitava
</s>
</p>
<p>
<s>
egoé	egoé	k6eAd1	egoé
Basket	basket	k1gInSc1	basket
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Kingspan	Kingspan	k1gInSc4	Kingspan
Královští	královský	k2eAgMnPc1d1	královský
sokoli	sokol	k1gMnPc1	sokol
</s>
</p>
<p>
<s>
NH	NH	kA	NH
Ostrava	Ostrava	k1gFnSc1	Ostrava
</s>
</p>
<p>
<s>
SLUNETA	SLUNETA	kA	SLUNETA
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
USK	USK	kA	USK
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
finále	finále	k1gNnSc6	finále
play-off	playff	k1gMnSc1	play-off
NBL	NBL	kA	NBL
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
přehledu	přehled	k1gInSc6	přehled
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
výsledky	výsledek	k1gInPc1	výsledek
zápasů	zápas	k1gInPc2	zápas
finále	finále	k1gNnSc2	finále
a	a	k8xC	a
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c4	v
play-off	playff	k1gInSc4	play-off
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
basketbalové	basketbalový	k2eAgFnSc6d1	basketbalová
lize	liga	k1gFnSc6	liga
<g/>
,	,	kIx,	,
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mattoni	Mattoň	k1gFnSc3	Mattoň
Národní	národní	k2eAgFnSc1d1	národní
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Mattoni	Matton	k1gMnPc1	Matton
NBL	NBL	kA	NBL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
utkání	utkání	k1gNnPc2	utkání
o	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
se	se	k3xPyFc4	se
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
umístí	umístit	k5eAaPmIp3nS	umístit
tým	tým	k1gInSc1	tým
na	na	k7c6	na
lepším	dobrý	k2eAgNnSc6d2	lepší
místě	místo	k1gNnSc6	místo
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
poražená	poražený	k2eAgNnPc1d1	poražené
družstva	družstvo	k1gNnPc1	družstvo
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
však	však	k9	však
získávají	získávat	k5eAaImIp3nP	získávat
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
medailistů	medailista	k1gMnPc2	medailista
v	v	k7c6	v
samostatné	samostatný	k2eAgFnSc6d1	samostatná
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
celkových	celkový	k2eAgInPc2d1	celkový
vítězů	vítěz	k1gMnPc2	vítěz
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
a	a	k8xC	a
české	český	k2eAgFnSc6d1	Česká
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
NBL	NBL	kA	NBL
v	v	k7c6	v
odkazech	odkaz	k1gInPc6	odkaz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nymburk	Nymburk	k1gInSc1	Nymburk
získal	získat	k5eAaPmAgInS	získat
osmý	osmý	k4xOgInSc4	osmý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
