<s>
Kooperativa	kooperativa	k1gFnSc1	kooperativa
Národní	národní	k2eAgFnSc1d1	národní
basketbalová	basketbalový	k2eAgFnSc1d1	basketbalová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Kooperativa	kooperativa	k1gFnSc1	kooperativa
NBL	NBL	kA	NBL
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
česká	český	k2eAgFnSc1d1	Česká
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
