<s>
Pascal	pascal	k1gInSc1	pascal
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
předchůdce	předchůdce	k1gMnSc4	předchůdce
moderní	moderní	k2eAgFnSc2d1	moderní
počítačové	počítačový	k2eAgFnSc2d1	počítačová
techniky	technika	k1gFnSc2	technika
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
jako	jako	k9	jako
pomůcku	pomůcka	k1gFnSc4	pomůcka
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
první	první	k4xOgMnSc1	první
mechanický	mechanický	k2eAgInSc1d1	mechanický
kalkulátor	kalkulátor	k1gInSc1	kalkulátor
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
sčítat	sčítat	k5eAaImF	sčítat
a	a	k8xC	a
odčítat	odčítat	k5eAaImF	odčítat
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Pascalina	Pascalina	k1gFnSc1	Pascalina
<g/>
.	.	kIx.	.
</s>
