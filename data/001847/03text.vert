<s>
Blaise	Blaise	k1gFnSc1	Blaise
Pascal	Pascal	k1gMnSc1	Pascal
[	[	kIx(	[
<g/>
bléz	bléz	k1gMnSc1	bléz
paskal	paskal	k1gMnSc1	paskal
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1623	[number]	k4	1623
Clermont	Clermonta	k1gFnPc2	Clermonta
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1662	[number]	k4	1662
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
náboženský	náboženský	k2eAgMnSc1d1	náboženský
filosof	filosof	k1gMnSc1	filosof
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
a	a	k8xC	a
vzdělané	vzdělaný	k2eAgFnSc2d1	vzdělaná
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
brzy	brzy	k6eAd1	brzy
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1631	[number]	k4	1631
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
vynikající	vynikající	k2eAgMnSc1d1	vynikající
humanitní	humanitní	k2eAgNnSc4d1	humanitní
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
;	;	kIx,	;
ač	ač	k8xS	ač
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
matematikem	matematik	k1gMnSc7	matematik
<g/>
,	,	kIx,	,
před	před	k7c7	před
chlapcem	chlapec	k1gMnSc7	chlapec
vědu	věda	k1gFnSc4	věda
zatajil	zatajit	k5eAaPmAgInS	zatajit
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
však	však	k9	však
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
desetiletý	desetiletý	k2eAgInSc4d1	desetiletý
Blaise	Blaise	k1gFnSc2	Blaise
už	už	k9	už
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
odvodil	odvodit	k5eAaPmAgMnS	odvodit
několik	několik	k4yIc4	několik
vět	věta	k1gFnPc2	věta
Eukleidovy	Eukleidův	k2eAgFnSc2d1	Eukleidova
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
-	-	kIx~	-
správce	správce	k1gMnSc1	správce
královských	královský	k2eAgFnPc2d1	královská
daní	daň	k1gFnPc2	daň
-	-	kIx~	-
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
kardinálem	kardinál	k1gMnSc7	kardinál
Richelieu	richelieu	k1gNnSc2	richelieu
kvůli	kvůli	k7c3	kvůli
novým	nový	k2eAgFnPc3d1	nová
daním	daň	k1gFnPc3	daň
a	a	k8xC	a
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Rouenu	Rouen	k1gInSc2	Rouen
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
Blaise	Blais	k1gInSc6	Blais
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
svůj	svůj	k3xOyFgInSc4	svůj
počítací	počítací	k2eAgInSc4d1	počítací
stroj	stroj	k1gInSc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
napsal	napsat	k5eAaBmAgMnS	napsat
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
kuželosečkách	kuželosečka	k1gFnPc6	kuželosečka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
ocenila	ocenit	k5eAaPmAgFnS	ocenit
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
královská	královský	k2eAgFnSc1d1	královská
akademie	akademie	k1gFnSc1	akademie
a	a	k8xC	a
Descartes	Descartes	k1gMnSc1	Descartes
je	on	k3xPp3gInPc4	on
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1647	[number]	k4	1647
vydal	vydat	k5eAaPmAgMnS	vydat
pojednání	pojednání	k1gNnSc3	pojednání
o	o	k7c6	o
tlaku	tlak	k1gInSc6	tlak
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
o	o	k7c6	o
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
přesných	přesný	k2eAgNnPc6d1	přesné
srovnávacích	srovnávací	k2eAgNnPc6d1	srovnávací
měřeních	měření	k1gNnPc6	měření
s	s	k7c7	s
Torricelliho	Torricelli	k1gMnSc4	Torricelli
trubicí	trubice	k1gFnSc7	trubice
na	na	k7c4	na
hoře	hoře	k1gNnSc4	hoře
Puy-de-Dome	Puye-Dom	k1gInSc5	Puy-de-Dom
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
1651	[number]	k4	1651
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
nákladný	nákladný	k2eAgInSc4d1	nákladný
život	život	k1gInSc4	život
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
dvorem	dvůr	k1gInSc7	dvůr
a	a	k8xC	a
rozešel	rozejít	k5eAaPmAgMnS	rozejít
se	se	k3xPyFc4	se
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
přísného	přísný	k2eAgInSc2d1	přísný
kláštera	klášter	k1gInSc2	klášter
Port-Royal	Port-Royal	k1gMnSc1	Port-Royal
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
měl	mít	k5eAaImAgMnS	mít
chatrné	chatrný	k2eAgNnSc1d1	chatrné
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgMnS	trpět
různými	různý	k2eAgFnPc7d1	různá
bolestmi	bolest	k1gFnPc7	bolest
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1647	[number]	k4	1647
na	na	k7c4	na
čas	čas	k1gInSc4	čas
ochrnul	ochrnout	k5eAaPmAgMnS	ochrnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
se	se	k3xPyFc4	se
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
vyjížďce	vyjížďka	k1gFnSc6	vyjížďka
málem	málem	k6eAd1	málem
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
mostě	most	k1gInSc6	most
v	v	k7c6	v
Neuilly-sur-Seine	Neuillyur-Sein	k1gInSc5	Neuilly-sur-Sein
splašili	splašit	k5eAaPmAgMnP	splašit
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
kočár	kočár	k1gInSc1	kočár
zůstal	zůstat	k5eAaPmAgInS	zůstat
viset	viset	k5eAaImF	viset
na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
;	;	kIx,	;
Pascal	Pascal	k1gMnSc1	Pascal
se	se	k3xPyFc4	se
zachránil	zachránit	k5eAaPmAgMnS	zachránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
upadl	upadnout	k5eAaPmAgMnS	upadnout
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
1654	[number]	k4	1654
přišel	přijít	k5eAaPmAgMnS	přijít
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
mystické	mystický	k2eAgNnSc4d1	mystické
vidění	vidění	k1gNnSc4	vidění
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
si	se	k3xPyFc3	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
Abrahamův	Abrahamův	k2eAgMnSc1d1	Abrahamův
<g/>
,	,	kIx,	,
Izákův	Izákův	k2eAgMnSc1d1	Izákův
a	a	k8xC	a
Jákobův	Jákobův	k2eAgMnSc1d1	Jákobův
<g/>
,	,	kIx,	,
ne	ne	k9	ne
Bůh	bůh	k1gMnSc1	bůh
filosofů	filosof	k1gMnPc2	filosof
a	a	k8xC	a
učenců	učenec	k1gMnPc2	učenec
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
filosofii	filosofie	k1gFnSc3	filosofie
a	a	k8xC	a
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
v	v	k7c4	v
Port-Royal	Port-Royal	k1gInSc4	Port-Royal
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobývala	pobývat	k5eAaImAgFnS	pobývat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
a	a	k8xC	a
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
zážitku	zážitek	k1gInSc6	zážitek
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
střediskem	středisko	k1gNnSc7	středisko
jansenismu	jansenismus	k1gInSc2	jansenismus
<g/>
,	,	kIx,	,
morálně	morálně	k6eAd1	morálně
přísného	přísný	k2eAgNnSc2d1	přísné
katolictví	katolictví	k1gNnSc2	katolictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
hledal	hledat	k5eAaImAgMnS	hledat
obhájce	obhájce	k1gMnSc4	obhájce
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
pařížskou	pařížský	k2eAgFnSc7d1	Pařížská
Sorbonnou	Sorbonna	k1gFnSc7	Sorbonna
a	a	k8xC	a
s	s	k7c7	s
jezuity	jezuita	k1gMnPc7	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
Pascal	pascal	k1gInSc1	pascal
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
velmi	velmi	k6eAd1	velmi
ostré	ostrý	k2eAgFnSc2d1	ostrá
kritiky	kritika	k1gFnSc2	kritika
na	na	k7c4	na
uvolněnou	uvolněný	k2eAgFnSc4d1	uvolněná
morální	morální	k2eAgFnSc4d1	morální
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kazuistiku	kazuistika	k1gFnSc4	kazuistika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hledala	hledat	k5eAaImAgFnS	hledat
různé	různý	k2eAgFnPc4d1	různá
omluvy	omluva	k1gFnPc4	omluva
pro	pro	k7c4	pro
mravně	mravně	k6eAd1	mravně
pochybná	pochybný	k2eAgNnPc4d1	pochybné
jednání	jednání	k1gNnPc4	jednání
a	a	k8xC	a
kterou	který	k3yRgFnSc4	který
šířili	šířit	k5eAaImAgMnP	šířit
zejména	zejména	k9	zejména
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
listy	list	k1gInPc1	list
vycházely	vycházet	k5eAaImAgInP	vycházet
anonymně	anonymně	k6eAd1	anonymně
a	a	k8xC	a
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
veliký	veliký	k2eAgInSc4d1	veliký
skandál	skandál	k1gInSc4	skandál
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
nechal	nechat	k5eAaPmAgMnS	nechat
1660	[number]	k4	1660
veřejně	veřejně	k6eAd1	veřejně
spálit	spálit	k5eAaPmF	spálit
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
je	být	k5eAaImIp3nS	být
i	i	k9	i
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
četly	číst	k5eAaImAgFnP	číst
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jansenismus	jansenismus	k1gInSc1	jansenismus
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
Port-Royal	Port-Royal	k1gInSc1	Port-Royal
zrušen	zrušen	k2eAgInSc1d1	zrušen
<g/>
,	,	kIx,	,
záměr	záměr	k1gInSc1	záměr
jeho	jeho	k3xOp3gInPc2	jeho
Listů	list	k1gInPc2	list
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
boji	boj	k1gInSc6	boj
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
kritiku	kritika	k1gFnSc4	kritika
církve	církev	k1gFnSc2	církev
navázal	navázat	k5eAaPmAgInS	navázat
pak	pak	k6eAd1	pak
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
obdivem	obdiv	k1gInSc7	obdiv
Voltaire	Voltair	k1gInSc5	Voltair
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
<g/>
,	,	kIx,	,
i	i	k8xC	i
Jean-Jacques	Jean-Jacques	k1gMnSc1	Jean-Jacques
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
knihu	kniha	k1gFnSc4	kniha
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nejvíc	nejvíc	k6eAd1	nejvíc
chtěl	chtít	k5eAaImAgInS	chtít
napsat	napsat	k5eAaBmF	napsat
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
Voltaire	Voltair	k1gInSc5	Voltair
právě	právě	k9	právě
"	"	kIx"	"
<g/>
Listy	lista	k1gFnSc2	lista
venkovanovi	venkovanův	k2eAgMnPc1d1	venkovanův
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pascalovo	Pascalův	k2eAgNnSc1d1	Pascalovo
bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
obrácení	obrácení	k1gNnSc1	obrácení
ještě	ještě	k6eAd1	ještě
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
uzdravení	uzdravení	k1gNnSc1	uzdravení
jeho	jeho	k3xOp3gFnSc2	jeho
desetileté	desetiletý	k2eAgFnSc2d1	desetiletá
neteře	neteř	k1gFnSc2	neteř
M.	M.	kA	M.
Perrier	Perrier	k1gInSc1	Perrier
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
trpěla	trpět	k5eAaImAgFnS	trpět
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
očním	oční	k2eAgInSc7d1	oční
nádorem	nádor	k1gInSc7	nádor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
po	po	k7c6	po
doteku	dotek	k1gInSc6	dotek
relikvie	relikvie	k1gFnSc2	relikvie
v	v	k7c4	v
Port-Royal	Port-Royal	k1gInSc4	Port-Royal
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Pascal	pascal	k1gInSc1	pascal
pak	pak	k6eAd1	pak
užíval	užívat	k5eAaImAgInS	užívat
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
emblém	emblém	k1gInSc4	emblém
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
obklopené	obklopený	k2eAgNnSc1d1	obklopené
trnovou	trnový	k2eAgFnSc7d1	Trnová
korunou	koruna	k1gFnSc7	koruna
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
jsem	být	k5eAaImIp1nS	být
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
žil	žít	k5eAaImAgMnS	žít
opět	opět	k6eAd1	opět
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
jako	jako	k8xC	jako
asketický	asketický	k2eAgMnSc1d1	asketický
poustevník	poustevník	k1gMnSc1	poustevník
<g/>
,	,	kIx,	,
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
chudým	chudý	k2eAgMnSc7d1	chudý
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgInS	odmítat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
velkému	velký	k2eAgInSc3d1	velký
projektu	projekt	k1gInSc3	projekt
obrany	obrana	k1gFnSc2	obrana
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
poznámky	poznámka	k1gFnPc1	poznámka
vyšly	vyjít	k5eAaPmAgFnP	vyjít
posmrtně	posmrtně	k6eAd1	posmrtně
jako	jako	k8xC	jako
Myšlenky	myšlenka	k1gFnSc2	myšlenka
(	(	kIx(	(
<g/>
Pensées	Pensées	k1gInSc1	Pensées
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gNnSc7	jeho
nejslavnějším	slavný	k2eAgNnSc7d3	nejslavnější
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Pascal	pascal	k1gInSc1	pascal
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
předchůdce	předchůdce	k1gMnSc4	předchůdce
moderní	moderní	k2eAgFnSc2d1	moderní
počítačové	počítačový	k2eAgFnSc2d1	počítačová
techniky	technika	k1gFnSc2	technika
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
jako	jako	k9	jako
pomůcku	pomůcka	k1gFnSc4	pomůcka
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
první	první	k4xOgMnSc1	první
mechanický	mechanický	k2eAgInSc1d1	mechanický
kalkulátor	kalkulátor	k1gInSc1	kalkulátor
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
sčítat	sčítat	k5eAaImF	sčítat
a	a	k8xC	a
odčítat	odčítat	k5eAaImF	odčítat
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Pascalina	Pascalina	k1gFnSc1	Pascalina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
života	život	k1gInSc2	život
jich	on	k3xPp3gFnPc2	on
pak	pak	k6eAd1	pak
nechal	nechat	k5eAaPmAgInS	nechat
vyrobit	vyrobit	k5eAaPmF	vyrobit
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
různě	různě	k6eAd1	různě
zdokonalených	zdokonalený	k2eAgMnPc2d1	zdokonalený
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc1d1	nazván
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
Pascal	pascal	k1gInSc1	pascal
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
především	především	k9	především
geometrii	geometrie	k1gFnSc4	geometrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
objevil	objevit	k5eAaPmAgMnS	objevit
tzv.	tzv.	kA	tzv.
Pascalovu	Pascalův	k2eAgFnSc4d1	Pascalova
větu	věta	k1gFnSc4	věta
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
body	bod	k1gInPc7	bod
na	na	k7c6	na
kuželosečkách	kuželosečka	k1gFnPc6	kuželosečka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zobecnění	zobecnění	k1gNnSc4	zobecnění
tzv.	tzv.	kA	tzv.
Pappovy	Pappův	k2eAgFnSc2d1	Pappova
úlohy	úloha	k1gFnSc2	úloha
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
také	také	k9	také
Descartes	Descartes	k1gMnSc1	Descartes
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
vyřešení	vyřešení	k1gNnSc4	vyřešení
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
doklad	doklad	k1gInSc4	doklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
současníci	současník	k1gMnPc1	současník
překonali	překonat	k5eAaPmAgMnP	překonat
staré	starý	k2eAgFnPc4d1	stará
Řeky	řeka	k1gFnPc4	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Pascal	Pascal	k1gMnSc1	Pascal
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
kombinatoriky	kombinatorika	k1gFnSc2	kombinatorika
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
objevil	objevit	k5eAaPmAgInS	objevit
tzv.	tzv.	kA	tzv.
Pascalův	Pascalův	k2eAgInSc1d1	Pascalův
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
důležitý	důležitý	k2eAgInSc1d1	důležitý
také	také	k9	také
v	v	k7c6	v
algebře	algebra	k1gFnSc6	algebra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
období	období	k1gNnSc2	období
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
hráčem	hráč	k1gMnSc7	hráč
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
souvislosti	souvislost	k1gFnSc6	souvislost
položil	položit	k5eAaPmAgMnS	položit
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
starším	starý	k2eAgMnSc7d2	starší
přítelem	přítel	k1gMnSc7	přítel
Fermatem	Fermat	k1gInSc7	Fermat
-	-	kIx~	-
základy	základ	k1gInPc1	základ
teorie	teorie	k1gFnSc2	teorie
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
matematické	matematický	k2eAgFnSc2d1	matematická
naděje	naděje	k1gFnSc2	naděje
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
práce	práce	k1gFnPc4	práce
o	o	k7c6	o
celočíselných	celočíselný	k2eAgFnPc6d1	celočíselná
řadách	řada	k1gFnPc6	řada
a	a	k8xC	a
o	o	k7c6	o
úplné	úplný	k2eAgFnSc6d1	úplná
(	(	kIx(	(
<g/>
matematické	matematický	k2eAgFnSc6d1	matematická
<g/>
)	)	kIx)	)
indukci	indukce	k1gFnSc3	indukce
se	se	k3xPyFc4	se
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
Leibniz	Leibniz	k1gInSc1	Leibniz
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
infinitesimálním	infinitesimální	k2eAgInSc7d1	infinitesimální
počtem	počet	k1gInSc7	počet
<g/>
,	,	kIx,	,
o	o	k7c4	o
nějž	jenž	k3xRgInSc4	jenž
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1650	[number]	k4	1650
také	také	k9	také
živě	živě	k6eAd1	živě
zajímal	zajímat	k5eAaImAgInS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
příspěvky	příspěvek	k1gInPc4	příspěvek
k	k	k7c3	k
axiomatice	axiomatika	k1gFnSc3	axiomatika
a	a	k8xC	a
filosofickým	filosofický	k2eAgInPc3d1	filosofický
základům	základ	k1gInPc3	základ
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc4	první
jasně	jasně	k6eAd1	jasně
formuloval	formulovat	k5eAaImAgMnS	formulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
známými	známý	k2eAgInPc7d1	známý
jevy	jev	k1gInPc7	jev
není	být	k5eNaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
hypotézy	hypotéza	k1gFnSc2	hypotéza
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jediný	jediný	k2eAgInSc1d1	jediný
empirický	empirický	k2eAgInSc1d1	empirický
nesouhlas	nesouhlas	k1gInSc1	nesouhlas
hypotézu	hypotéza	k1gFnSc4	hypotéza
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
navázaly	navázat	k5eAaPmAgInP	navázat
na	na	k7c4	na
Torricelliho	Torricelliha	k1gFnSc5	Torricelliha
pokusy	pokus	k1gInPc1	pokus
se	s	k7c7	s
rtuťovou	rtuťový	k2eAgFnSc7d1	rtuťová
trubicí	trubice	k1gFnSc7	trubice
<g/>
:	:	kIx,	:
ve	v	k7c6	v
Ferrandu	Ferrand	k1gInSc6	Ferrand
a	a	k8xC	a
na	na	k7c6	na
blízkém	blízký	k2eAgNnSc6d1	blízké
Puy-de-Dome	Puye-Dom	k1gInSc5	Puy-de-Dom
provedl	provést	k5eAaPmAgInS	provést
za	za	k7c4	za
veřejné	veřejný	k2eAgFnPc4d1	veřejná
asistence	asistence	k1gFnPc4	asistence
řadu	řad	k1gInSc2	řad
přesných	přesný	k2eAgNnPc2d1	přesné
srovnávacích	srovnávací	k2eAgNnPc2d1	srovnávací
měření	měření	k1gNnPc2	měření
rtuťového	rtuťový	k2eAgInSc2d1	rtuťový
sloupce	sloupec	k1gInSc2	sloupec
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
jednak	jednak	k8xC	jednak
možnost	možnost	k1gFnSc4	možnost
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
starší	starý	k2eAgFnSc1d2	starší
fyzika	fyzika	k1gFnSc1	fyzika
popírala	popírat	k5eAaImAgFnS	popírat
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rtuťový	rtuťový	k2eAgInSc1d1	rtuťový
sloupec	sloupec	k1gInSc1	sloupec
podléhá	podléhat	k5eAaImIp3nS	podléhat
pouze	pouze	k6eAd1	pouze
gravitaci	gravitace	k1gFnSc3	gravitace
a	a	k8xC	a
tlaku	tlak	k1gInSc3	tlak
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pokusy	pokus	k1gInPc1	pokus
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
spojitých	spojitý	k2eAgFnPc2d1	spojitá
nádob	nádoba	k1gFnPc2	nádoba
a	a	k8xC	a
šíření	šíření	k1gNnSc4	šíření
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
formuloval	formulovat	k5eAaImAgInS	formulovat
tzv.	tzv.	kA	tzv.
Pascalův	Pascalův	k2eAgInSc1d1	Pascalův
zákon	zákon	k1gInSc1	zákon
<g/>
:	:	kIx,	:
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
kapalině	kapalina	k1gFnSc6	kapalina
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
celá	celý	k2eAgFnSc1d1	celá
hydraulická	hydraulický	k2eAgFnSc1d1	hydraulická
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
jednotka	jednotka	k1gFnSc1	jednotka
tlaku	tlak	k1gInSc2	tlak
nazývá	nazývat	k5eAaImIp3nS	nazývat
Pascal	pascal	k1gInSc1	pascal
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
objevů	objev	k1gInPc2	objev
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
fyzice	fyzika	k1gFnSc6	fyzika
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
jako	jako	k8xC	jako
vynálezce	vynálezce	k1gMnSc1	vynálezce
také	také	k9	také
moderní	moderní	k2eAgFnSc4d1	moderní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgNnSc4	první
vozidlo	vozidlo	k1gNnSc4	vozidlo
typu	typ	k1gInSc2	typ
omnibusu	omnibus	k1gInSc2	omnibus
totiž	totiž	k9	totiž
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgMnS	označovat
koňmi	kůň	k1gMnPc7	kůň
tažený	tažený	k2eAgInSc4d1	tažený
vůz	vůz	k1gInSc4	vůz
pro	pro	k7c4	pro
8	[number]	k4	8
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Carosse	Carosse	k1gFnSc1	Carosse
<g/>
"	"	kIx"	"
předvedl	předvést	k5eAaPmAgMnS	předvést
Pascal	Pascal	k1gMnSc1	Pascal
roku	rok	k1gInSc2	rok
1662	[number]	k4	1662
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Montaigneovou	Montaigneův	k2eAgFnSc7d1	Montaigneův
skepsí	skepse	k1gFnSc7	skepse
<g/>
:	:	kIx,	:
náboženské	náboženský	k2eAgFnSc2d1	náboženská
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
otřásly	otřást	k5eAaPmAgFnP	otřást
důvěrou	důvěra	k1gFnSc7	důvěra
v	v	k7c4	v
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
v	v	k7c4	v
Písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
Montaigne	Montaign	k1gInSc5	Montaign
podryl	podrýt	k5eAaPmAgInS	podrýt
i	i	k9	i
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c6	v
možnosti	možnost	k1gFnSc6	možnost
lidského	lidský	k2eAgInSc2d1	lidský
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
svých	svůj	k3xOyFgMnPc2	svůj
více	hodně	k6eAd2	hodně
spekulativních	spekulativní	k2eAgMnPc2d1	spekulativní
předchůdců	předchůdce	k1gMnPc2	předchůdce
i	i	k8xC	i
současníků	současník	k1gMnPc2	současník
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Galileiho	Galilei	k1gMnSc2	Galilei
a	a	k8xC	a
Descarta	Descart	k1gMnSc2	Descart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
spoléhali	spoléhat	k5eAaImAgMnP	spoléhat
na	na	k7c4	na
racionální	racionální	k2eAgFnSc4d1	racionální
povahu	povaha	k1gFnSc4	povaha
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
stvořeného	stvořený	k2eAgInSc2d1	stvořený
podle	podle	k7c2	podle
matematických	matematický	k2eAgNnPc2d1	matematické
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tudíž	tudíž	k8xC	tudíž
stačí	stačit	k5eAaBmIp3nS	stačit
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
intuitivně	intuitivně	k6eAd1	intuitivně
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
pečlivě	pečlivě	k6eAd1	pečlivě
rozmyšlené	rozmyšlený	k2eAgInPc4d1	rozmyšlený
experimenty	experiment	k1gInPc4	experiment
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
dokumentovaná	dokumentovaný	k2eAgNnPc1d1	dokumentované
měření	měření	k1gNnPc1	měření
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
tak	tak	k9	tak
mezi	mezi	k7c4	mezi
zakladatele	zakladatel	k1gMnPc4	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
empirické	empirický	k2eAgFnSc2d1	empirická
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žádné	žádný	k3yNgInPc4	žádný
jiné	jiný	k2eAgInPc4d1	jiný
zdroje	zdroj	k1gInPc4	zdroj
nepokládá	pokládat	k5eNaImIp3nS	pokládat
za	za	k7c4	za
spolehlivé	spolehlivý	k2eAgFnPc4d1	spolehlivá
<g/>
.	.	kIx.	.
</s>
<s>
Pascalovým	Pascalův	k2eAgNnSc7d1	Pascalovo
hlavním	hlavní	k2eAgNnSc7d1	hlavní
filosofickým	filosofický	k2eAgNnSc7d1	filosofické
a	a	k8xC	a
teologickým	teologický	k2eAgNnSc7d1	teologické
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
Myšlenky	myšlenka	k1gFnSc2	myšlenka
(	(	kIx(	(
<g/>
Pensées	Pensées	k1gMnSc1	Pensées
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
stéblo	stéblo	k1gNnSc1	stéblo
<g/>
,	,	kIx,	,
ubohá	ubohý	k2eAgFnSc1d1	ubohá
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
a	a	k8xC	a
kapka	kapka	k1gFnSc1	kapka
vody	voda	k1gFnSc2	voda
ho	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
myslící	myslící	k2eAgNnSc1d1	myslící
stéblo	stéblo	k1gNnSc1	stéblo
<g/>
:	:	kIx,	:
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
ho	on	k3xPp3gInSc4	on
Vesmír	vesmír	k1gInSc4	vesmír
rozdrtil	rozdrtit	k5eAaPmAgMnS	rozdrtit
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
zůstane	zůstat	k5eAaPmIp3nS	zůstat
vznešenější	vznešený	k2eAgNnSc4d2	vznešenější
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ho	on	k3xPp3gMnSc4	on
zabilo	zabít	k5eAaPmAgNnS	zabít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
jakou	jaký	k3yRgFnSc4	jaký
má	mít	k5eAaImIp3nS	mít
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
Vesmír	vesmír	k1gInSc1	vesmír
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Vesmír	vesmír	k1gInSc1	vesmír
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
neví	vědět	k5eNaImIp3nS	vědět
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tento	tento	k3xDgInSc1	tento
paradox	paradox	k1gInSc1	paradox
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
anděl	anděl	k1gMnSc1	anděl
ani	ani	k8xC	ani
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
a	a	k8xC	a
bohužel	bohužel	k9	bohužel
kdykoli	kdykoli	k6eAd1	kdykoli
chce	chtít	k5eAaImIp3nS	chtít
dělat	dělat	k5eAaImF	dělat
anděla	anděl	k1gMnSc4	anděl
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	s	k7c7	s
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jádrem	jádro	k1gNnSc7	jádro
Myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Pascal	Pascal	k1gMnSc1	Pascal
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hlubokých	hluboký	k2eAgInPc2d1	hluboký
otřesů	otřes	k1gInPc2	otřes
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
podryly	podrýt	k5eAaPmAgFnP	podrýt
obecnou	obecný	k2eAgFnSc4d1	obecná
středověkou	středověký	k2eAgFnSc4d1	středověká
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c4	v
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
zjevení	zjevení	k1gNnSc4	zjevení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
možnosti	možnost	k1gFnSc6	možnost
lidského	lidský	k2eAgInSc2d1	lidský
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
strašný	strašný	k2eAgInSc1d1	strašný
pocit	pocit	k1gInSc1	pocit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
člověku	člověk	k1gMnSc3	člověk
ztrácí	ztrácet	k5eAaImIp3nP	ztrácet
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
měl	mít	k5eAaImAgMnS	mít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Dvojí	dvojí	k4xRgInSc1	dvojí
omyl	omyl	k1gInSc1	omyl
<g/>
:	:	kIx,	:
vyloučit	vyloučit	k5eAaPmF	vyloučit
rozum	rozum	k1gInSc4	rozum
a	a	k8xC	a
nepřipouštět	připouštět	k5eNaImF	připouštět
než	než	k8xS	než
rozum	rozum	k1gInSc4	rozum
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Každá	každý	k3xTgFnSc1	každý
věc	věc	k1gFnSc1	věc
je	být	k5eAaImIp3nS	být
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
nebo	nebo	k8xC	nebo
falešná	falešný	k2eAgFnSc1d1	falešná
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
které	který	k3yRgFnPc1	který
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
podíváme	podívat	k5eAaPmIp1nP	podívat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Říkat	říkat	k5eAaImF	říkat
pravdu	pravda	k1gFnSc4	pravda
je	být	k5eAaImIp3nS	být
prospěšné	prospěšný	k2eAgNnSc1d1	prospěšné
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
komu	kdo	k3yRnSc3	kdo
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevýhodné	výhodný	k2eNgNnSc1d1	nevýhodné
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ji	on	k3xPp3gFnSc4	on
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
vyslouží	vysloužit	k5eAaPmIp3nP	vysloužit
nenávist	nenávist	k1gFnSc4	nenávist
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
jen	jen	k9	jen
předstírání	předstírání	k1gNnSc4	předstírání
<g/>
,	,	kIx,	,
lež	lež	k1gFnSc4	lež
a	a	k8xC	a
pokrytectví	pokrytectví	k1gNnSc4	pokrytectví
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
i	i	k8xC	i
druhým	druhý	k4xOgMnSc7	druhý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pascal	pascal	k1gInSc1	pascal
je	být	k5eAaImIp3nS	být
myslitel	myslitel	k1gMnSc1	myslitel
hluboké	hluboký	k2eAgFnSc2d1	hluboká
skepse	skepse	k1gFnSc2	skepse
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
-	-	kIx~	-
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Descartes	Descartes	k1gMnSc1	Descartes
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jinou	jiný	k2eAgFnSc7d1	jiná
cestou	cesta	k1gFnSc7	cesta
-	-	kIx~	-
zoufale	zoufale	k6eAd1	zoufale
hledá	hledat	k5eAaImIp3nS	hledat
pevný	pevný	k2eAgInSc4d1	pevný
bod	bod	k1gInSc4	bod
nějaké	nějaký	k3yIgFnSc2	nějaký
jistoty	jistota	k1gFnSc2	jistota
poznání	poznání	k1gNnSc2	poznání
i	i	k8xC	i
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nemohu	moct	k5eNaImIp1nS	moct
odpustit	odpustit	k5eAaPmF	odpustit
Descartovi	Descart	k1gMnSc3	Descart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celá	k1gFnSc6	celá
své	svůj	k3xOyFgFnSc6	svůj
filosofii	filosofie	k1gFnSc6	filosofie
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
obejít	obejít	k5eAaPmF	obejít
bez	bez	k7c2	bez
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přece	přece	k9	přece
jen	jen	k9	jen
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Bůh	bůh	k1gMnSc1	bůh
svět	svět	k1gInSc1	svět
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
postrčil	postrčit	k5eAaPmAgMnS	postrčit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
<g/>
;	;	kIx,	;
potom	potom	k8xC	potom
už	už	k6eAd1	už
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
souvislosti	souvislost	k1gFnSc2	souvislost
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
slavná	slavný	k2eAgFnSc1d1	slavná
"	"	kIx"	"
<g/>
Pascalova	Pascalův	k2eAgFnSc1d1	Pascalova
sázka	sázka	k1gFnSc1	sázka
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
protože	protože	k8xS	protože
člověk	člověk	k1gMnSc1	člověk
nic	nic	k3yNnSc1	nic
neví	vědět	k5eNaImIp3nS	vědět
jistě	jistě	k6eAd1	jistě
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
aspoň	aspoň	k9	aspoň
jako	jako	k9	jako
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
možnou	možný	k2eAgFnSc7d1	možná
výhrou	výhra	k1gFnSc7	výhra
<g/>
.	.	kIx.	.
</s>
<s>
Ztratit	ztratit	k5eAaPmF	ztratit
nemůže	moct	k5eNaImIp3nS	moct
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
křesťanství	křesťanství	k1gNnSc4	křesťanství
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nekonečně	konečně	k6eNd1	konečně
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Měl	mít	k5eAaImAgInS	mít
bych	by	kYmCp1nS	by
větší	veliký	k2eAgInSc1d2	veliký
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zmýlím	zmýlit	k5eAaPmIp1nS	zmýlit
a	a	k8xC	a
zjistím	zjistit	k5eAaPmIp1nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
křesťanství	křesťanství	k1gNnSc1	křesťanství
má	mít	k5eAaImIp3nS	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
se	se	k3xPyFc4	se
nezmýlím	zmýlit	k5eNaPmIp1nS	zmýlit
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c4	v
ně	on	k3xPp3gInPc4	on
věřím	věřit	k5eAaImIp1nS	věřit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
životním	životní	k2eAgNnSc6d1	životní
dilematu	dilema	k1gNnSc6	dilema
bez	bez	k7c2	bez
naděje	naděje	k1gFnSc2	naděje
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
hluboké	hluboký	k2eAgFnPc1d1	hluboká
věci	věc	k1gFnPc1	věc
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
nejhůře	zle	k6eAd3	zle
snáší	snášet	k5eAaImIp3nS	snášet
člověk	člověk	k1gMnSc1	člověk
úplný	úplný	k2eAgInSc1d1	úplný
klid	klid	k1gInSc1	klid
bez	bez	k7c2	bez
vášní	vášeň	k1gFnPc2	vášeň
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
obstarávání	obstarávání	k1gNnSc2	obstarávání
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
rozptýlení	rozptýlení	k1gNnPc2	rozptýlení
a	a	k8xC	a
bez	bez	k7c2	bez
uplatnění	uplatnění	k1gNnSc2	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
totiž	totiž	k9	totiž
teprve	teprve	k6eAd1	teprve
cítí	cítit	k5eAaImIp3nS	cítit
svoji	svůj	k3xOyFgFnSc4	svůj
nicotu	nicota	k1gFnSc4	nicota
<g/>
,	,	kIx,	,
opuštěnost	opuštěnost	k1gFnSc4	opuštěnost
<g/>
,	,	kIx,	,
nedostatečnost	nedostatečnost	k1gFnSc4	nedostatečnost
<g/>
,	,	kIx,	,
závislost	závislost	k1gFnSc4	závislost
<g/>
,	,	kIx,	,
bezmoc	bezmoc	k1gFnSc4	bezmoc
a	a	k8xC	a
prázdnotu	prázdnota	k1gFnSc4	prázdnota
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Naše	náš	k3xOp1gFnSc1	náš
přirozená	přirozený	k2eAgFnSc1d1	přirozená
povaha	povaha	k1gFnSc1	povaha
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
pohyb	pohyb	k1gInSc1	pohyb
<g/>
:	:	kIx,	:
úplný	úplný	k2eAgInSc1d1	úplný
klid	klid	k1gInSc1	klid
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
málo	málo	k4c1	málo
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
nás	my	k3xPp1nPc4	my
mohly	moct	k5eAaImAgFnP	moct
těšit	těšit	k5eAaImF	těšit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jen	jen	k9	jen
málo	málo	k6eAd1	málo
co	co	k3yInSc1	co
nás	my	k3xPp1nPc4	my
skutečně	skutečně	k6eAd1	skutečně
trápí	trápit	k5eAaImIp3nS	trápit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
dvojí	dvojí	k4xRgInSc4	dvojí
druh	druh	k1gInSc4	druh
lidí	člověk	k1gMnPc2	člověk
<g/>
:	:	kIx,	:
spravedliví	spravedlivý	k2eAgMnPc1d1	spravedlivý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
hříšníci	hříšník	k1gMnPc1	hříšník
<g/>
,	,	kIx,	,
a	a	k8xC	a
hříšníci	hříšník	k1gMnPc1	hříšník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
spravedliví	spravedlivý	k2eAgMnPc1d1	spravedlivý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
bez	bez	k7c2	bez
síly	síla	k1gFnSc2	síla
je	být	k5eAaImIp3nS	být
bezmocná	bezmocný	k2eAgFnSc1d1	bezmocná
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
bez	bez	k7c2	bez
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
tyranská	tyranský	k2eAgFnSc1d1	tyranská
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jednám	jednat	k5eAaImIp1nS	jednat
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
dbát	dbát	k5eAaImF	dbát
-	-	kIx~	-
kromě	kromě	k7c2	kromě
jednání	jednání	k1gNnSc2	jednání
samého	samý	k3xTgMnSc4	samý
-	-	kIx~	-
jak	jak	k8xC	jak
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc4d1	přítomný
<g/>
,	,	kIx,	,
minulý	minulý	k2eAgInSc4d1	minulý
a	a	k8xC	a
budoucí	budoucí	k2eAgInSc4d1	budoucí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
stav	stav	k1gInSc4	stav
těch	ten	k3xDgFnPc2	ten
druhých	druhý	k4xOgMnPc2	druhý
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
týká	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vidět	vidět	k5eAaImF	vidět
souvislosti	souvislost	k1gFnPc4	souvislost
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Můžeme	moct	k5eAaImIp1nP	moct
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nebyli	být	k5eNaImAgMnP	být
zlí	zlý	k2eAgMnPc1d1	zlý
lidé	člověk	k1gMnPc1	člověk
<g/>
?	?	kIx.	?
</s>
<s>
Místo	místo	k7c2	místo
jednoho	jeden	k4xCgInSc2	jeden
tím	ten	k3xDgNnSc7	ten
jen	jen	k6eAd1	jen
naděláme	nadělat	k5eAaPmIp1nP	nadělat
dva	dva	k4xCgMnPc1	dva
<g/>
"	"	kIx"	"
Člověk	člověk	k1gMnSc1	člověk
tak	tak	k6eAd1	tak
stojí	stát	k5eAaImIp3nS	stát
vždycky	vždycky	k6eAd1	vždycky
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
propastí	propast	k1gFnSc7	propast
Vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
nekonečně	konečně	k6eNd1	konečně
malým	malý	k1gMnPc3	malý
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
pravdě	pravda	k1gFnSc6	pravda
a	a	k8xC	a
neschopností	neschopnost	k1gFnSc7	neschopnost
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
domoci	domoct	k5eAaPmF	domoct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Naši	náš	k3xOp1gFnSc4	náš
neschopnost	neschopnost	k1gFnSc4	neschopnost
dokazovat	dokazovat	k5eAaImF	dokazovat
nepřekoná	překonat	k5eNaPmIp3nS	překonat
žádný	žádný	k3yNgInSc1	žádný
dogmatismus	dogmatismus	k1gInSc1	dogmatismus
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
máme	mít	k5eAaImIp1nP	mít
ideu	idea	k1gFnSc4	idea
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nepřekoná	překonat	k5eNaPmIp3nS	překonat
žádná	žádný	k3yNgFnSc1	žádný
skepse	skepse	k1gFnSc1	skepse
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
...	...	k?	...
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
pravdy	pravda	k1gFnSc2	pravda
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
víme	vědět	k5eAaImIp1nP	vědět
i	i	k9	i
o	o	k7c6	o
té	ten	k3xDgFnSc6	ten
opačné	opačný	k2eAgInPc1d1	opačný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Děláme	dělat	k5eAaImIp1nP	dělat
si	se	k3xPyFc3	se
modlu	modla	k1gFnSc4	modla
z	z	k7c2	z
pravdy	pravda	k1gFnSc2	pravda
samé	samý	k3xTgFnPc1	samý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravda	pravda	k1gFnSc1	pravda
<g />
.	.	kIx.	.
</s>
<s>
bez	bez	k7c2	bez
lásky	láska	k1gFnSc2	láska
není	být	k5eNaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
a	a	k8xC	a
modla	modla	k1gFnSc1	modla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nemáme	mít	k5eNaImIp1nP	mít
milovat	milovat	k5eAaImF	milovat
ani	ani	k8xC	ani
ctít	ctít	k5eAaImF	ctít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ale	ale	k9	ale
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
poznání	poznání	k1gNnSc1	poznání
nestačí	stačit	k5eNaBmIp3nS	stačit
člověku	člověk	k1gMnSc3	člověk
ke	k	k7c3	k
štěstí	štěstí	k1gNnSc3	štěstí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jen	jen	k9	jen
z	z	k7c2	z
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
poznání	poznání	k1gNnSc1	poznání
Boha	bůh	k1gMnSc2	bůh
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
začít	začít	k5eAaPmF	začít
jej	on	k3xPp3gInSc4	on
milovat	milovat	k5eAaImF	milovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastnosti	vlastnost	k1gFnPc1	vlastnost
čísel	číslo	k1gNnPc2	číslo
jsou	být	k5eAaImIp3nP	být
věčné	věčný	k2eAgInPc1d1	věčný
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
první	první	k4xOgFnSc6	první
pravdě	pravda	k1gFnSc6	pravda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
nepokročil	pokročit	k5eNaPmAgInS	pokročit
mnoho	mnoho	k6eAd1	mnoho
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
spáse	spása	k1gFnSc3	spása
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
Bůh	bůh	k1gMnSc1	bůh
není	být	k5eNaImIp3nS	být
původce	původce	k1gMnSc1	původce
geometrických	geometrický	k2eAgFnPc2d1	geometrická
pravd	pravda	k1gFnPc2	pravda
a	a	k8xC	a
řádu	řád	k1gInSc2	řád
živlů	živel	k1gInPc2	živel
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
věc	věc	k1gFnSc1	věc
pohanů	pohan	k1gMnPc2	pohan
a	a	k8xC	a
epikurejců	epikurejec	k1gMnPc2	epikurejec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
Abrahámův	Abrahámův	k2eAgMnSc1d1	Abrahámův
<g/>
,	,	kIx,	,
Izákův	Izákův	k2eAgMnSc1d1	Izákův
<g/>
,	,	kIx,	,
Jákobův	Jákobův	k2eAgMnSc1d1	Jákobův
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
křesťanů	křesťan	k1gMnPc2	křesťan
je	být	k5eAaImIp3nS	být
Bůh	bůh	k1gMnSc1	bůh
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
útěchy	útěcha	k1gFnSc2	útěcha
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Srdce	srdce	k1gNnSc1	srdce
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
rozum	rozum	k1gInSc1	rozum
nemá	mít	k5eNaImIp3nS	mít
tušení	tušení	k1gNnSc4	tušení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Víra	víra	k1gFnSc1	víra
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
důkazu	důkaz	k1gInSc2	důkaz
<g/>
:	:	kIx,	:
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
<g />
.	.	kIx.	.
</s>
<s>
lidský	lidský	k2eAgMnSc1d1	lidský
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
víra	víra	k1gFnSc1	víra
je	být	k5eAaImIp3nS	být
dar	dar	k1gInSc1	dar
Boží	boží	k2eAgInSc1d1	boží
<g/>
.	.	kIx.	.
...	...	k?	...
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
a	a	k8xC	a
neříká	říkat	k5eNaImIp3nS	říkat
́	́	k?	́
<g/>
vím	vědět	k5eAaImIp1nS	vědět
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
́	́	k?	́
<g/>
věřím	věřit	k5eAaImIp1nS	věřit
<g/>
́	́	k?	́
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tyto	tento	k3xDgFnPc1	tento
myšlenky	myšlenka	k1gFnPc1	myšlenka
byly	být	k5eAaImAgFnP	být
osvícenské	osvícenský	k2eAgFnPc1d1	osvícenská
době	době	k6eAd1	době
zcela	zcela	k6eAd1	zcela
cizí	cizí	k2eAgFnSc6d1	cizí
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
víc	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
však	však	k9	však
uplatnily	uplatnit	k5eAaPmAgInP	uplatnit
v	v	k7c6	v
pietismu	pietismus	k1gInSc6	pietismus
a	a	k8xC	a
v	v	k7c6	v
romantismu	romantismus	k1gInSc6	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
vážnosti	vážnost	k1gFnSc2	vážnost
a	a	k8xC	a
opravdovosti	opravdovost	k1gFnSc2	opravdovost
vycházel	vycházet	k5eAaImAgMnS	vycházet
Sø	Sø	k1gMnSc1	Sø
Kierkegaard	Kierkegaard	k1gMnSc1	Kierkegaard
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
existenciální	existenciální	k2eAgFnSc1d1	existenciální
filosofie	filosofie	k1gFnSc1	filosofie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
bezohledná	bezohledný	k2eAgFnSc1d1	bezohledná
upřímnost	upřímnost	k1gFnSc1	upřímnost
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
F.	F.	kA	F.
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
řadu	řada	k1gFnSc4	řada
myslitelů	myslitel	k1gMnPc2	myslitel
postmoderny	postmoderna	k1gFnSc2	postmoderna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
Myšlenky	myšlenka	k1gFnPc4	myšlenka
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
psány	psán	k2eAgMnPc4d1	psán
krásným	krásný	k2eAgInSc7d1	krásný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Pascal	pascal	k1gInSc1	pascal
mezi	mezi	k7c7	mezi
klasiky	klasik	k1gMnPc7	klasik
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
i	i	k8xC	i
náboženského	náboženský	k2eAgNnSc2d1	náboženské
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Essai	Essai	k6eAd1	Essai
pour	pour	k1gInSc1	pour
les	les	k1gInSc1	les
coniques	coniques	k1gInSc4	coniques
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Esej	esej	k1gFnSc1	esej
o	o	k7c6	o
kuželosečkách	kuželosečka	k1gFnPc6	kuželosečka
<g/>
,	,	kIx,	,
1640	[number]	k4	1640
<g/>
)	)	kIx)	)
Expériences	Expériences	k1gMnSc1	Expériences
nouvelles	nouvelles	k1gMnSc1	nouvelles
touchant	touchant	k1gMnSc1	touchant
le	le	k?	le
vide	vid	k1gInSc5	vid
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgInPc1d1	Nové
pokusy	pokus	k1gInPc1	pokus
ohledně	ohledně	k7c2	ohledně
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,1647	,1647	k4	,1647
<g/>
)	)	kIx)	)
Récit	Récita	k1gFnPc2	Récita
de	de	k?	de
la	la	k1gNnSc4	la
grande	grand	k1gMnSc5	grand
expérience	expérienec	k1gInPc1	expérienec
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
équilibre	équilibr	k1gInSc5	équilibr
des	des	k1gNnPc6	des
liqueurs	liqueurs	k1gInSc4	liqueurs
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
velkého	velký	k2eAgInSc2d1	velký
pokusu	pokus	k1gInSc2	pokus
s	s	k7c7	s
rovnováhou	rovnováha	k1gFnSc7	rovnováha
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
,1648	,1648	k4	,1648
<g/>
)	)	kIx)	)
Traité	Traitý	k2eAgFnSc2d1	Traitý
du	du	k?	du
triangle	triangl	k1gInSc5	triangl
arithmétique	arithmétique	k1gNnPc7	arithmétique
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
aritmetickém	aritmetický	k2eAgInSc6d1	aritmetický
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
<g/>
,	,	kIx,	,
1654	[number]	k4	1654
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
Provinciales	Provinciales	k1gInSc1	Provinciales
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Listy	lista	k1gFnSc2	lista
1656	[number]	k4	1656
<g/>
-	-	kIx~	-
<g/>
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
Celkem	celkem	k6eAd1	celkem
18	[number]	k4	18
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vydáno	vydat	k5eAaPmNgNnS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Listy	list	k1gInPc1	list
proti	proti	k7c3	proti
jesuitům	jesuita	k1gMnPc3	jesuita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
titul	titul	k1gInSc1	titul
<g/>
:	:	kIx,	:
Lettres	Lettres	k1gInSc1	Lettres
écrites	écrites	k1gMnSc1	écrites
par	para	k1gFnPc2	para
Louis	Louis	k1gMnSc1	Louis
de	de	k?	de
Montalte	Montalte	k1gMnSc1	Montalte
à	à	k?	à
un	un	k?	un
provincial	provincial	k1gInSc1	provincial
de	de	k?	de
ses	ses	k?	ses
amis	amis	k1gInSc1	amis
et	et	k?	et
aux	aux	k?	aux
RR	RR	kA	RR
<g/>
.	.	kIx.	.
</s>
<s>
PP	PP	kA	PP
<g/>
.	.	kIx.	.
</s>
<s>
Jésuites	Jésuites	k1gInSc1	Jésuites
sur	sur	k?	sur
le	le	k?	le
sujet	sujet	k1gInSc1	sujet
de	de	k?	de
la	la	k1gNnSc4	la
morale	morale	k6eAd1	morale
et	et	k?	et
de	de	k?	de
la	la	k1gNnSc2	la
politique	politique	k1gFnPc2	politique
de	de	k?	de
ces	ces	k1gNnSc4	ces
Pè	Pè	k1gFnSc2	Pè
[	[	kIx(	[
<g/>
Listy	lista	k1gFnSc2	lista
Louise	Louis	k1gMnSc2	Louis
de	de	k?	de
Montalta	Montalta	k1gMnSc1	Montalta
venkovskému	venkovský	k2eAgMnSc3d1	venkovský
příteli	přítel	k1gMnSc3	přítel
a	a	k8xC	a
důstojným	důstojný	k2eAgMnPc3d1	důstojný
otcům	otec	k1gMnPc3	otec
jesuitům	jesuita	k1gMnPc3	jesuita
o	o	k7c6	o
mravouce	mravouka	k1gFnSc6	mravouka
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
těchto	tento	k3xDgMnPc2	tento
otců	otec	k1gMnPc2	otec
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spis	spis	k1gInSc4	spis
zařadila	zařadit	k5eAaPmAgFnS	zařadit
církev	církev	k1gFnSc1	církev
roku	rok	k1gInSc2	rok
1657	[number]	k4	1657
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
v	v	k7c6	v
Indexu	index	k1gInSc6	index
zpravidla	zpravidla	k6eAd1	zpravidla
uváděn	uvádět	k5eAaImNgInS	uvádět
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
údajného	údajný	k2eAgMnSc2d1	údajný
autora	autor	k1gMnSc2	autor
[	[	kIx(	[
<g/>
Montalte	Montalte	k1gMnSc1	Montalte
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
vydáních	vydání	k1gNnPc6	vydání
je	být	k5eAaImIp3nS	být
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pseudonym	pseudonym	k1gInSc4	pseudonym
Bl.	Bl.	k1gMnSc2	Bl.
Pascala	Pascal	k1gMnSc2	Pascal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Éléments	Éléments	k6eAd1	Éléments
de	de	k?	de
géométrie	géométrie	k1gFnSc1	géométrie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Základy	základ	k1gInPc1	základ
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,1657	,1657	k4	,1657
<g/>
)	)	kIx)	)
De	De	k?	De
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Esprit	esprit	k1gInSc1	esprit
géométrique	géométriqu	k1gFnSc2	géométriqu
et	et	k?	et
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
de	de	k?	de
persuader	persuader	k1gMnSc1	persuader
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
O	o	k7c6	o
geometrickém	geometrický	k2eAgMnSc6d1	geometrický
duchu	duch	k1gMnSc6	duch
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
<g/>
,	,	kIx,	,
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
Histoire	Histoir	k1gInSc5	Histoir
de	de	k?	de
la	la	k1gNnPc1	la
roulette	roulette	k5eAaPmIp2nP	roulette
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
rulety	ruleta	k1gFnSc2	ruleta
<g/>
,	,	kIx,	,
1658	[number]	k4	1658
<g/>
)	)	kIx)	)
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Art	Art	k1gMnSc1	Art
de	de	k?	de
persuader	persuader	k1gMnSc1	persuader
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Umění	umění	k1gNnSc4	umění
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
<g/>
,1660	,1660	k4	,1660
<g/>
)	)	kIx)	)
Pensées	Pensées	k1gMnSc1	Pensées
sur	sur	k?	sur
la	la	k1gNnPc2	la
religion	religion	k1gInSc1	religion
et	et	k?	et
autres	autres	k1gInSc1	autres
sujets	sujets	k1gInSc1	sujets
(	(	kIx(	(
<g/>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c6	o
náboženství	náboženství	k1gNnSc6	náboženství
i	i	k8xC	i
jiných	jiný	k2eAgFnPc6d1	jiná
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
1669	[number]	k4	1669
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
)	)	kIx)	)
PASCAL	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
:	:	kIx,	:
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Miloslav	Miloslav	k1gMnSc1	Miloslav
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
V	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
frontě	fronta	k1gFnSc6	fronta
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
Klasická	klasický	k2eAgFnSc1d1	klasická
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
PASCAL	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
úplné	úplný	k2eAgFnSc2d1	úplná
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
upr	upr	k?	upr
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
xciv	xciv	k1gMnSc1	xciv
<g/>
,	,	kIx,	,
557	[number]	k4	557
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
</s>
<s>
Otázky	otázka	k1gFnPc1	otázka
a	a	k8xC	a
názory	názor	k1gInPc1	názor
<g/>
;	;	kIx,	;
kniha	kniha	k1gFnSc1	kniha
27	[number]	k4	27
<g/>
.	.	kIx.	.
</s>
<s>
PASCAL	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
proti	proti	k7c3	proti
jesuitům	jesuita	k1gMnPc3	jesuita
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
provinciales	provinciales	k1gInSc1	provinciales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
Čechal	Čechal	k1gMnSc1	Čechal
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Hencl	Hencl	k1gInSc1	Hencl
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
345	[number]	k4	345
s.	s.	k?	s.
PASCAL	Pascal	kA	Pascal
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
<g/>
.	.	kIx.	.
</s>
<s>
Blažeje	Blažej	k1gMnSc4	Blažej
Pascala	Pascal	k1gMnSc4	Pascal
<g/>
:	:	kIx,	:
o	o	k7c6	o
duchu	duch	k1gMnSc6	duch
geometrickém	geometrický	k2eAgMnSc6d1	geometrický
<g/>
:	:	kIx,	:
dva	dva	k4xCgInPc1	dva
fragmenty	fragment	k1gInPc1	fragment
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Guth	Guth	k1gMnSc1	Guth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
typ	typ	k1gInSc1	typ
Edvard	Edvard	k1gMnSc1	Edvard
Grégr	Grégr	k1gMnSc1	Grégr
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
26	[number]	k4	26
s.	s.	k?	s.
PASCAL	Pascal	kA	Pascal
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
<g/>
.	.	kIx.	.
</s>
<s>
Blažeje	Blažej	k1gMnSc4	Blažej
Pascala	Pascal	k1gMnSc4	Pascal
O	o	k7c6	o
duchu	duch	k1gMnSc6	duch
geometrickém	geometrický	k2eAgMnSc6d1	geometrický
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Guth	Guth	k1gMnSc1	Guth
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Klátiková	Klátikový	k2eAgFnSc1d1	Klátikový
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
Klátiková	Klátikový	k2eAgFnSc1d1	Klátikový
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
30	[number]	k4	30
s.	s.	k?	s.
HORÁK	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
Blaise	Blaise	k1gFnSc2	Blaise
Pascala	Pascal	k1gMnSc2	Pascal
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
373	[number]	k4	373
s.	s.	k?	s.
[	[	kIx(	[
<g/>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
též	též	k9	též
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
některých	některý	k3yIgFnPc2	některý
prací	práce	k1gFnPc2	práce
Bl.	Bl.	k1gMnSc4	Bl.
Pascala	Pascal	k1gMnSc4	Pascal
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Listy	lista	k1gFnSc2	lista
venkovanovi	venkovanův	k2eAgMnPc1d1	venkovanův
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Listy	list	k1gInPc1	list
proti	proti	k7c3	proti
jezuitům	jezuita	k1gMnPc3	jezuita
<g/>
)	)	kIx)	)
č.	č.	k?	č.
I	I	kA	I
-	-	kIx~	-
X	X	kA	X
a	a	k8xC	a
XV	XV	kA	XV
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
105	[number]	k4	105
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
208	[number]	k4	208
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
de	de	k?	de
Saci	Sac	k1gMnPc7	Sac
<g/>
"	"	kIx"	"
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
209	[number]	k4	209
<g/>
-	-	kIx~	-
<g/>
230	[number]	k4	230
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
O	o	k7c6	o
geometrickém	geometrický	k2eAgMnSc6d1	geometrický
duchu	duch	k1gMnSc6	duch
<g/>
"	"	kIx"	"
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
O	o	k7c6	o
umění	umění	k1gNnSc6	umění
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
<g/>
"	"	kIx"	"
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
252	[number]	k4	252
<g/>
-	-	kIx~	-
<g/>
264	[number]	k4	264
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Spisy	spis	k1gInPc1	spis
o	o	k7c4	o
milosti	milost	k1gFnPc4	milost
<g/>
"	"	kIx"	"
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
291	[number]	k4	291
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
slovenský	slovenský	k2eAgInSc1d1	slovenský
překlad	překlad	k1gInSc1	překlad
<g/>
:	:	kIx,	:
PASCAL	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
Blaise	Blaise	k1gFnSc1	Blaise
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
proti	proti	k7c3	proti
jezuitom	jezuitom	k1gInSc1	jezuitom
<g/>
:	:	kIx,	:
Listy	lista	k1gFnSc2	lista
vidiečanovi	vidiečanův	k2eAgMnPc1d1	vidiečanův
o	o	k7c6	o
dišputách	dišputách	k?	dišputách
na	na	k7c6	na
Sorbonne	Sorbonn	k1gInSc5	Sorbonn
a	a	k8xC	a
iných	iných	k1gMnSc1	iných
zaujímavostiach	zaujímavostiach	k1gMnSc1	zaujímavostiach
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Vydav	vydávit	k5eAaPmRp2nS	vydávit
<g/>
.	.	kIx.	.
polit	polit	k2eAgInSc1d1	polit
<g/>
.	.	kIx.	.
lit.	lit.	k?	lit.
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
275	[number]	k4	275
s.	s.	k?	s.
</s>
