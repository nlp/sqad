<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgInSc2d1	Karlův
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
UK	UK	kA	UK
<g/>
,	,	kIx,	,
latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
Universitas	Universitas	k1gInSc1	Universitas
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
)	)	kIx)	)
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
česká	český	k2eAgFnSc1d1	Česká
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
evropských	evropský	k2eAgFnPc2d1	Evropská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
severně	severně	k6eAd1	severně
od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
