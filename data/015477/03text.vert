<s>
Kecskeméti	Kecskeméti	k1gNnSc1
TE	TE	kA
</s>
<s>
Kecskeméti	Kecskemét	k2eAgMnPc1d1
TENázev	TENázev	k1gMnPc1
</s>
<s>
Kecskeméti	Kecskeméť	k1gFnSc2
Testedző	Testedző	k1gFnPc1
Egyesület	Egyesület	k1gInSc4
Země	zem	k1gFnSc2
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Kecskemét	Kecskemét	k2eAgInSc1d1
Založen	založen	k2eAgInSc1d1
</s>
<s>
1911	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
maďarská	maďarský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2012-13	2012-13	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Stadion	stadion	k1gInSc1
Széktói	Széktó	k1gFnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
8	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
6	#num#	k4
300	#num#	k4
Vedení	vedení	k1gNnPc2
Předseda	předseda	k1gMnSc1
</s>
<s>
Pál	Pál	k?
Rózsa	Rózsa	k1gFnSc1
<g/>
,	,	kIx,
János	János	k1gMnSc1
Versegi	Verseg	k1gFnSc2
Trenér	trenér	k1gMnSc1
</s>
<s>
István	István	k2eAgMnSc1d1
Urbányi	Urbány	k1gMnPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kecskeméti	Kecskeméti	k1gNnSc1
TE	TE	kA
je	být	k5eAaImIp3nS
maďarský	maďarský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
sídlící	sídlící	k2eAgInSc1d1
ve	v	k7c6
městě	město	k1gNnSc6
Kecskemétu	Kecskemét	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácím	domácí	k2eAgNnSc7d1
hřištěm	hřiště	k1gNnSc7
klubu	klub	k1gInSc2
je	být	k5eAaImIp3nS
Stadion	stadion	k1gInSc1
Széktói	Széktó	k1gFnSc2
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
6	#num#	k4
300	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Trofeje	trofej	k1gFnPc1
</s>
<s>
1	#num#	k4
×	×	k?
vítěz	vítěz	k1gMnSc1
maďarského	maďarský	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Hungary	Hungara	k1gFnPc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Cup	cup	k1gInSc1
Finals	Finalsa	k1gFnPc2
na	na	k7c4
RSSSF	RSSSF	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Kecskemét	Kecskemét	k1gInSc1
stun	stun	k1gMnSc1
Videoton	Videoton	k1gInSc1
to	ten	k3xDgNnSc1
win	win	k?
Hungarian	Hungarian	k1gInSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Maďarsko	Maďarsko	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
