<s>
Borrelióza	Borrelióza	k1gFnSc1	Borrelióza
je	být	k5eAaImIp3nS	být
členovci	členovec	k1gMnSc3	členovec
přenosné	přenosný	k2eAgFnSc2d1	přenosná
akutní	akutní	k2eAgFnSc2d1	akutní
septikemické	septikemický	k2eAgNnSc1d1	septikemický
onemocnění	onemocnění	k1gNnSc3	onemocnění
ptáků	pták	k1gMnPc2	pták
vyvolávaná	vyvolávaný	k2eAgFnSc1d1	vyvolávaná
spirochetou	spirochést	k5eAaPmIp3nP	spirochést
Borrelia	Borrelius	k1gMnSc4	Borrelius
anserina	anserina	k1gFnSc1	anserina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
v	v	k7c6	v
endemicky	endemicky	k6eAd1	endemicky
postižených	postižený	k2eAgFnPc6d1	postižená
oblastech	oblast	k1gFnPc6	oblast
způsobovat	způsobovat	k5eAaImF	způsobovat
velké	velký	k2eAgFnSc2d1	velká
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
.	.	kIx.	.
</s>
