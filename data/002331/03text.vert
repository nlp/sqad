<s>
Borrelióza	Borrelióza	k1gFnSc1	Borrelióza
je	být	k5eAaImIp3nS	být
členovci	členovec	k1gMnSc3	členovec
přenosné	přenosný	k2eAgFnSc2d1	přenosná
akutní	akutní	k2eAgFnSc2d1	akutní
septikemické	septikemický	k2eAgNnSc1d1	septikemický
onemocnění	onemocnění	k1gNnSc3	onemocnění
ptáků	pták	k1gMnPc2	pták
vyvolávaná	vyvolávaný	k2eAgFnSc1d1	vyvolávaná
spirochetou	spirochést	k5eAaPmIp3nP	spirochést
Borrelia	Borrelius	k1gMnSc4	Borrelius
anserina	anserina	k1gFnSc1	anserina
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
v	v	k7c6	v
endemicky	endemicky	k6eAd1	endemicky
postižených	postižený	k2eAgFnPc6d1	postižená
oblastech	oblast	k1gFnPc6	oblast
způsobovat	způsobovat	k5eAaImF	způsobovat
velké	velký	k2eAgFnSc2d1	velká
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
význam	význam	k1gInSc1	význam
B.	B.	kA	B.
anserina	anserin	k1gMnSc2	anserin
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
antigenně	antigenně	k6eAd1	antigenně
úzce	úzko	k6eAd1	úzko
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
s	s	k7c7	s
B.	B.	kA	B.
burgdorferi	burgdorferi	k1gNnSc1	burgdorferi
<g/>
,	,	kIx,	,
původcem	původce	k1gMnSc7	původce
lymské	lymský	k2eAgFnSc2d1	lymská
borreliózy	borrelióza	k1gFnSc2	borrelióza
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
experimentálně	experimentálně	k6eAd1	experimentálně
infikováni	infikován	k2eAgMnPc1d1	infikován
a	a	k8xC	a
vylučovat	vylučovat	k5eAaImF	vylučovat
tuto	tento	k3xDgFnSc4	tento
spirochetu	spirocheta	k1gFnSc4	spirocheta
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vyloučit	vyloučit	k5eAaPmF	vyloučit
jako	jako	k8xS	jako
možný	možný	k2eAgInSc1d1	možný
zdroj	zdroj	k1gInSc1	zdroj
infekce	infekce	k1gFnSc2	infekce
B.	B.	kA	B.
burgdorferi	burgdorferi	k6eAd1	burgdorferi
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
borrelióza	borrelióza	k1gFnSc1	borrelióza
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsat	k5eAaPmNgFnS	popsat
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
Sacharovem	Sacharov	k1gInSc7	Sacharov
jako	jako	k9	jako
vážné	vážný	k2eAgNnSc4d1	vážné
septikemické	septikemický	k2eAgNnSc4d1	septikemický
onemocnění	onemocnění	k1gNnSc4	onemocnění
husí	husa	k1gFnPc2	husa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
byla	být	k5eAaImAgFnS	být
nemoc	nemoc	k1gFnSc1	nemoc
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
u	u	k7c2	u
drůbeže	drůbež	k1gFnSc2	drůbež
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
primární	primární	k2eAgFnSc1d1	primární
úloha	úloha	k1gFnSc1	úloha
členovců	členovec	k1gMnPc2	členovec
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
přenosu	přenos	k1gInSc6	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgFnS	být
borrelióza	borrelióza	k1gFnSc1	borrelióza
identifikována	identifikován	k2eAgFnSc1d1	identifikována
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc6d1	subtropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
naší	náš	k3xOp1gFnSc2	náš
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Borrelióza	Borrelióza	k1gFnSc1	Borrelióza
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
častým	častý	k2eAgInSc7d1	častý
výskytem	výskyt	k1gInSc7	výskyt
klíšťáků	klíšťák	k1gMnPc2	klíšťák
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Argas	Argasa	k1gFnPc2	Argasa
<g/>
.	.	kIx.	.
</s>
<s>
Spirochety	Spirocheta	k1gFnPc1	Spirocheta
jsou	být	k5eAaImIp3nP	být
jednobuněčné	jednobuněčný	k2eAgFnPc1d1	jednobuněčná
<g/>
,	,	kIx,	,
jemné	jemný	k2eAgFnPc1d1	jemná
spirální	spirální	k2eAgFnPc1d1	spirální
gramnegativní	gramnegativní	k2eAgFnPc1d1	gramnegativní
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
délky	délka	k1gFnPc1	délka
až	až	k9	až
500	[number]	k4	500
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Množí	množit	k5eAaImIp3nP	množit
se	s	k7c7	s
příčným	příčný	k2eAgNnSc7d1	příčné
dělením	dělení	k1gNnSc7	dělení
a	a	k8xC	a
běžnými	běžný	k2eAgFnPc7d1	běžná
barvícími	barvící	k2eAgFnPc7d1	barvící
technikami	technika	k1gFnPc7	technika
se	se	k3xPyFc4	se
nebarví	barvit	k5eNaImIp3nP	barvit
<g/>
;	;	kIx,	;
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
stříbření	stříbření	k1gNnSc1	stříbření
<g/>
.	.	kIx.	.
</s>
<s>
Spirochety	Spirocheta	k1gFnPc1	Spirocheta
jsou	být	k5eAaImIp3nP	být
zařazovány	zařazovat	k5eAaImNgFnP	zařazovat
do	do	k7c2	do
rodů	rod	k1gInPc2	rod
Spirochaeta	Spirochaeto	k1gNnSc2	Spirochaeto
<g/>
,	,	kIx,	,
Treponema	Treponemum	k1gNnSc2	Treponemum
<g/>
,	,	kIx,	,
Borrelia	Borrelium	k1gNnSc2	Borrelium
<g/>
,	,	kIx,	,
Brachyspira	Brachyspiro	k1gNnSc2	Brachyspiro
a	a	k8xC	a
Leptospira	Leptospiro	k1gNnSc2	Leptospiro
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
původci	původce	k1gMnPc1	původce
borreliózy	borrelióza	k1gFnSc2	borrelióza
a	a	k8xC	a
střevních	střevní	k2eAgNnPc2d1	střevní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
Borrelia	Borrelia	k1gFnSc1	Borrelia
anserina	anserina	k1gFnSc1	anserina
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Spirochaeta	Spirochaet	k2eAgFnSc1d1	Spirochaet
anserina	anserina	k1gFnSc1	anserina
<g/>
,	,	kIx,	,
S.	S.	kA	S.
gallinarum	gallinarum	k1gInSc1	gallinarum
<g/>
,	,	kIx,	,
S.	S.	kA	S.
anatis	anatis	k1gInSc1	anatis
a	a	k8xC	a
Treponema	Treponema	k1gFnSc1	Treponema
anserinum	anserinum	k1gInSc1	anserinum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgFnPc1d1	pohybující
spirální	spirální	k2eAgFnPc1d1	spirální
bakterie	bakterie	k1gFnPc1	bakterie
s	s	k7c7	s
5-8	[number]	k4	5-8
závity	závit	k1gInPc7	závit
<g/>
,	,	kIx,	,
měřící	měřící	k2eAgMnPc1d1	měřící
kolem	kolem	k7c2	kolem
6-30	[number]	k4	6-30
x	x	k?	x
0,3	[number]	k4	0,3
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
filtry	filtr	k1gInPc4	filtr
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
pórů	pór	k1gInPc2	pór
450	[number]	k4	450
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Borrelie	Borrelie	k1gFnPc1	Borrelie
mají	mít	k5eAaImIp3nP	mít
15-22	[number]	k4	15-22
osových	osový	k2eAgFnPc2d1	Osová
fibril	fibrila	k1gFnPc2	fibrila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nativním	nativní	k2eAgInSc6d1	nativní
preparátech	preparát	k1gInPc6	preparát
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
nebo	nebo	k8xC	nebo
tkání	tkáň	k1gFnPc2	tkáň
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
zástinu	zástin	k1gInSc6	zástin
nebo	nebo	k8xC	nebo
po	po	k7c6	po
obarvení	obarvení	k1gNnSc6	obarvení
Giemsou	Giemsý	k2eAgFnSc4d1	Giemsý
<g/>
.	.	kIx.	.
</s>
<s>
Borrelie	Borrelie	k1gFnPc1	Borrelie
rostou	růst	k5eAaImIp3nP	růst
za	za	k7c2	za
mikroaerofilních	mikroaerofilní	k2eAgFnPc2d1	mikroaerofilní
podmínek	podmínka	k1gFnPc2	podmínka
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
30-37	[number]	k4	30-37
°	°	k?	°
<g/>
C.	C.	kA	C.
Produkují	produkovat	k5eAaImIp3nP	produkovat
kyselinu	kyselina	k1gFnSc4	kyselina
z	z	k7c2	z
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Kultivace	kultivace	k1gFnSc1	kultivace
na	na	k7c6	na
umělých	umělý	k2eAgFnPc6d1	umělá
kultivačních	kultivační	k2eAgFnPc6d1	kultivační
půdách	půda	k1gFnPc6	půda
je	být	k5eAaImIp3nS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pomnožení	pomnožení	k1gNnSc3	pomnožení
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
slepičí	slepičí	k2eAgNnPc1d1	slepičí
nebo	nebo	k8xC	nebo
krůtí	krůtí	k2eAgNnPc1d1	krůtí
embrya	embryo	k1gNnPc1	embryo
infikovaná	infikovaný	k2eAgNnPc1d1	infikované
do	do	k7c2	do
žloutkového	žloutkový	k2eAgInSc2d1	žloutkový
vaku	vak	k1gInSc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Borrelie	Borrelie	k1gFnPc1	Borrelie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
tkáních	tkáň	k1gFnPc6	tkáň
embrya	embryo	k1gNnSc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
prostředí	prostředí	k1gNnSc6	prostředí
mimo	mimo	k7c4	mimo
tělo	tělo	k1gNnSc4	tělo
hostitele	hostitel	k1gMnSc2	hostitel
B.	B.	kA	B.
anserina	anserin	k1gMnSc2	anserin
nepřežívá	přežívat	k5eNaImIp3nS	přežívat
<g/>
.	.	kIx.	.
</s>
<s>
Klíšťáci	klíšťák	k1gMnPc1	klíšťák
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
rezervoárový	rezervoárový	k2eAgMnSc1d1	rezervoárový
hostitel	hostitel	k1gMnSc1	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kadáverech	kadáver	k1gInPc6	kadáver
spirochety	spirocheta	k1gFnPc1	spirocheta
přežívají	přežívat	k5eAaImIp3nP	přežívat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
až	až	k9	až
31	[number]	k4	31
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
séru	sérum	k1gNnSc6	sérum
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
skladovat	skladovat	k5eAaImF	skladovat
nejdéle	dlouho	k6eAd3	dlouho
3-4	[number]	k4	3-4
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
udržovat	udržovat	k5eAaImF	udržovat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
-	-	kIx~	-
<g/>
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
10-15	[number]	k4	10-15
%	%	kIx~	%
glycerolu	glycerol	k1gInSc2	glycerol
nebo	nebo	k8xC	nebo
DMSO	DMSO	kA	DMSO
k	k	k7c3	k
infekční	infekční	k2eAgFnSc3d1	infekční
krvi	krev	k1gFnSc3	krev
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
anserina	anserina	k1gFnSc1	anserina
je	být	k5eAaImIp3nS	být
patogenní	patogenní	k2eAgFnSc1d1	patogenní
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
neteční	tečný	k2eNgMnPc1d1	tečný
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
králíků	králík	k1gMnPc2	králík
a	a	k8xC	a
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přechodně	přechodně	k6eAd1	přechodně
infikováni	infikován	k2eAgMnPc1d1	infikován
<g/>
.	.	kIx.	.
</s>
<s>
Virulentní	virulentní	k2eAgInPc1d1	virulentní
kmeny	kmen	k1gInPc1	kmen
borrelií	borrelie	k1gFnPc2	borrelie
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
penetrovat	penetrovat	k5eAaBmF	penetrovat
přes	přes	k7c4	přes
neporušenou	porušený	k2eNgFnSc4d1	neporušená
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgFnPc1d1	přirozená
infekce	infekce	k1gFnPc1	infekce
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
krůt	krůta	k1gFnPc2	krůta
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
<g/>
,	,	kIx,	,
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
husí	husa	k1gFnPc2	husa
a	a	k8xC	a
kanárů	kanár	k1gInPc2	kanár
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
byla	být	k5eAaImAgFnS	být
borrelióza	borrelióza	k1gFnSc1	borrelióza
pozorována	pozorován	k2eAgFnSc1d1	pozorována
u	u	k7c2	u
tetřevů	tetřev	k1gMnPc2	tetřev
<g/>
,	,	kIx,	,
koroptví	koroptev	k1gFnPc2	koroptev
<g/>
,	,	kIx,	,
vran	vrána	k1gFnPc2	vrána
<g/>
,	,	kIx,	,
strak	straka	k1gFnPc2	straka
<g/>
,	,	kIx,	,
domácích	domácí	k2eAgMnPc2d1	domácí
vrabců	vrabec	k1gMnPc2	vrabec
a	a	k8xC	a
špačků	špaček	k1gMnPc2	špaček
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žaků	žako	k1gMnPc2	žako
(	(	kIx(	(
<g/>
Psittacus	Psittacus	k1gMnSc1	Psittacus
erithacus	erithacus	k1gMnSc1	erithacus
<g/>
)	)	kIx)	)
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
rychlý	rychlý	k2eAgInSc4d1	rychlý
úhyn	úhyn	k1gInSc4	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Holubi	holub	k1gMnPc1	holub
a	a	k8xC	a
perličky	perlička	k1gFnPc1	perlička
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
relativně	relativně	k6eAd1	relativně
odolní	odolný	k2eAgMnPc1d1	odolný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
u	u	k7c2	u
městských	městský	k2eAgMnPc2d1	městský
holubů	holub	k1gMnPc2	holub
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
3,26	[number]	k4	3,26
%	%	kIx~	%
sérologicky	sérologicky	k6eAd1	sérologicky
pozitivních	pozitivní	k2eAgMnPc2d1	pozitivní
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
Fabbi	Fabbi	k1gNnSc1	Fabbi
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Drůbež	drůbež	k1gFnSc1	drůbež
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
náchylná	náchylný	k2eAgFnSc1d1	náchylná
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejvnímavější	vnímavý	k2eAgMnPc1d3	nejvnímavější
jsou	být	k5eAaImIp3nP	být
mláďata	mládě	k1gNnPc4	mládě
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Borrelióza	Borrelióza	k1gFnSc1	Borrelióza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prakticky	prakticky	k6eAd1	prakticky
přenášena	přenášen	k2eAgFnSc1d1	přenášena
všemi	všecek	k3xTgInPc7	všecek
možnými	možný	k2eAgInPc7d1	možný
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
jedinci	jedinec	k1gMnPc1	jedinec
přijdou	přijít	k5eAaPmIp3nP	přijít
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
exkrety	exkret	k1gInPc7	exkret
nebo	nebo	k8xC	nebo
tkáněmi	tkáň	k1gFnPc7	tkáň
od	od	k7c2	od
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
živých	živý	k2eAgFnPc2d1	živá
nebo	nebo	k8xC	nebo
čerstvě	čerstvě	k6eAd1	čerstvě
uhynulých	uhynulý	k2eAgMnPc2d1	uhynulý
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
např.	např.	kA	např.
při	při	k7c6	při
kanibalismu	kanibalismus	k1gInSc6	kanibalismus
nebo	nebo	k8xC	nebo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kontaminovaného	kontaminovaný	k2eAgNnSc2d1	kontaminované
krmiva	krmivo	k1gNnSc2	krmivo
či	či	k8xC	či
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Závažnými	závažný	k2eAgMnPc7d1	závažný
přenašeči	přenašeč	k1gMnPc7	přenašeč
infekce	infekce	k1gFnSc2	infekce
jsou	být	k5eAaImIp3nP	být
členovci	členovec	k1gMnPc1	členovec
parazitující	parazitující	k2eAgMnPc1d1	parazitující
na	na	k7c6	na
ptácích	pták	k1gMnPc6	pták
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
komárů	komár	k1gMnPc2	komár
<g/>
,	,	kIx,	,
čmelíků	čmelík	k1gMnPc2	čmelík
a	a	k8xC	a
klíšťat	klíště	k1gNnPc2	klíště
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgNnSc1d1	dominantní
postavení	postavení	k1gNnSc1	postavení
ale	ale	k8xC	ale
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
klíšťáci	klíšťák	k1gMnPc1	klíšťák
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Argas	Argasa	k1gFnPc2	Argasa
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zejména	zejména	k9	zejména
A.	A.	kA	A.
persicus	persicus	k1gMnSc1	persicus
<g/>
,	,	kIx,	,
klíšťák	klíšťák	k1gMnSc1	klíšťák
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
infikováni	infikován	k2eAgMnPc1d1	infikován
slinami	slina	k1gFnPc7	slina
produkovanými	produkovaný	k2eAgFnPc7d1	produkovaná
klíšťákem	klíšťák	k1gMnSc7	klíšťák
při	při	k7c6	při
kousnutí	kousnutí	k1gNnSc6	kousnutí
nebo	nebo	k8xC	nebo
pozřením	pozření	k1gNnSc7	pozření
infikovaného	infikovaný	k2eAgMnSc2d1	infikovaný
klíšťáka	klíšťák	k1gMnSc2	klíšťák
<g/>
,	,	kIx,	,
nymfy	nymfa	k1gFnSc2	nymfa
<g/>
,	,	kIx,	,
larvy	larva	k1gFnPc4	larva
či	či	k8xC	či
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
anserina	anserina	k1gFnSc1	anserina
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
periferní	periferní	k2eAgFnSc6d1	periferní
krvi	krev	k1gFnSc6	krev
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
ptáků	pták	k1gMnPc2	pták
během	během	k7c2	během
4-9	[number]	k4	4-9
dní	den	k1gInPc2	den
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
přibližně	přibližně	k6eAd1	přibližně
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patogenezi	patogeneze	k1gFnSc6	patogeneze
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
endotoxin	endotoxin	k1gInSc1	endotoxin
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úhynům	úhyn	k1gInPc3	úhyn
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
embolie	embolie	k1gFnSc2	embolie
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgInPc4d1	vyvolaný
shluky	shluk	k1gInPc4	shluk
borrelií	borrelie	k1gFnPc2	borrelie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rekonvalescentních	rekonvalescentní	k2eAgNnPc2d1	rekonvalescentní
zvířat	zvíře	k1gNnPc2	zvíře
nevzniká	vznikat	k5eNaImIp3nS	vznikat
nosičství	nosičství	k1gNnSc1	nosičství
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
se	se	k3xPyFc4	se
typově	typově	k6eAd1	typově
specifická	specifický	k2eAgFnSc1d1	specifická
imunita	imunita	k1gFnSc1	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
infikování	infikování	k1gNnSc2	infikování
<g/>
,	,	kIx,	,
počtu	počet	k1gInSc2	počet
spirochet	spirocheta	k1gFnPc2	spirocheta
v	v	k7c6	v
inokulu	inokul	k1gInSc6	inokul
a	a	k8xC	a
virulenci	virulence	k1gFnSc3	virulence
kmene	kmen	k1gInSc2	kmen
B.	B.	kA	B.
anserina	anserin	k1gMnSc2	anserin
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přirozené	přirozený	k2eAgFnSc6d1	přirozená
infekci	infekce	k1gFnSc6	infekce
klíšťákem	klíšťák	k1gMnSc7	klíšťák
trvá	trvat	k5eAaImIp3nS	trvat
3-12	[number]	k4	3-12
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
po	po	k7c6	po
parenterální	parenterální	k2eAgFnSc6d1	parenterální
(	(	kIx(	(
<g/>
injekční	injekční	k2eAgFnSc6d1	injekční
<g/>
)	)	kIx)	)
inokulaci	inokulace	k1gFnSc6	inokulace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
virulentními	virulentní	k2eAgInPc7d1	virulentní
kmeny	kmen	k1gInPc7	kmen
B.	B.	kA	B.
anserina	anserina	k1gMnSc1	anserina
jsou	být	k5eAaImIp3nP	být
ospalí	ospalý	k2eAgMnPc1d1	ospalý
<g/>
,	,	kIx,	,
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
se	se	k3xPyFc4	se
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
kůže	kůže	k1gFnSc2	kůže
je	být	k5eAaImIp3nS	být
anemická	anemický	k2eAgFnSc1d1	anemická
nebo	nebo	k8xC	nebo
cyanotická	cyanotický	k2eAgFnSc1d1	cyanotická
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
načepýřené	načepýřený	k2eAgNnSc4d1	načepýřené
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
nazelenalý	nazelenalý	k2eAgInSc4d1	nazelenalý
průjem	průjem	k1gInSc4	průjem
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
o	o	k7c4	o
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
nálezem	nález	k1gInSc7	nález
u	u	k7c2	u
borreliózy	borrelióza	k1gFnSc2	borrelióza
je	být	k5eAaImIp3nS	být
náhlý	náhlý	k2eAgInSc1d1	náhlý
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc1d1	vysoký
vzestup	vzestup	k1gInSc1	vzestup
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
počínající	počínající	k2eAgFnSc7d1	počínající
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
<g/>
,	,	kIx,	,
a	a	k8xC	a
rychlá	rychlý	k2eAgFnSc1d1	rychlá
ztráta	ztráta	k1gFnSc1	ztráta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Výkaly	výkal	k1gInPc1	výkal
jsou	být	k5eAaImIp3nP	být
tekuté	tekutý	k2eAgNnSc4d1	tekuté
s	s	k7c7	s
nadbytkem	nadbytek	k1gInSc7	nadbytek
žluči	žluč	k1gFnSc2	žluč
a	a	k8xC	a
urátů	urát	k1gInPc2	urát
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
rychle	rychle	k6eAd1	rychle
slábnou	slábnout	k5eAaImIp3nP	slábnout
<g/>
,	,	kIx,	,
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
otupělými	otupělý	k2eAgMnPc7d1	otupělý
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
ochrnutí	ochrnutí	k1gNnSc1	ochrnutí
končetin	končetina	k1gFnPc2	končetina
i	i	k8xC	i
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
před	před	k7c7	před
úhynem	úhyn	k1gInSc7	úhyn
je	být	k5eAaImIp3nS	být
subnormální	subnormální	k2eAgNnSc1d1	subnormální
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
tmavočervené	tmavočervený	k2eAgNnSc1d1	tmavočervené
zabarvení	zabarvení	k1gNnSc1	zabarvení
hřebene	hřeben	k1gInSc2	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Rekonvalescentní	rekonvalescentní	k2eAgMnPc1d1	rekonvalescentní
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vyhublí	vyhublý	k2eAgMnPc1d1	vyhublý
<g/>
,	,	kIx,	,
s	s	k7c7	s
přetrvávajícím	přetrvávající	k2eAgNnSc7d1	přetrvávající
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
končetin	končetina	k1gFnPc2	končetina
anebo	anebo	k8xC	anebo
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
kmeny	kmen	k1gInPc1	kmen
o	o	k7c6	o
nízké	nízký	k2eAgFnSc6d1	nízká
virulenci	virulence	k1gFnSc6	virulence
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
subklinicky	subklinicky	k6eAd1	subklinicky
<g/>
.	.	kIx.	.
</s>
<s>
Morbidita	morbidita	k1gFnSc1	morbidita
a	a	k8xC	a
mortalita	mortalita	k1gFnSc1	mortalita
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
variabilní	variabilní	k2eAgFnPc1d1	variabilní
<g/>
,	,	kIx,	,
kolísají	kolísat	k5eAaImIp3nP	kolísat
od	od	k7c2	od
1-2	[number]	k4	1-2
%	%	kIx~	%
do	do	k7c2	do
100	[number]	k4	100
%	%	kIx~	%
podle	podle	k7c2	podle
vnímavosti	vnímavost	k1gFnSc2	vnímavost
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
akutních	akutní	k2eAgInPc6d1	akutní
případech	případ	k1gInPc6	případ
krátký	krátký	k2eAgInSc1d1	krátký
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
končit	končit	k5eAaImF	končit
úhynem	úhyn	k1gInSc7	úhyn
za	za	k7c4	za
3-5	[number]	k4	3-5
dní	den	k1gInPc2	den
po	po	k7c6	po
výskytu	výskyt	k1gInSc6	výskyt
klinických	klinický	k2eAgInPc2d1	klinický
příznaků	příznak	k1gInPc2	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Chronické	chronický	k2eAgInPc1d1	chronický
případy	případ	k1gInPc1	případ
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Dominujícím	dominující	k2eAgInSc7d1	dominující
nálezem	nález	k1gInSc7	nález
u	u	k7c2	u
borreliózy	borrelióza	k1gFnSc2	borrelióza
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
zvětšená	zvětšený	k2eAgFnSc1d1	zvětšená
a	a	k8xC	a
skvrnitá	skvrnitý	k2eAgFnSc1d1	skvrnitá
slezina	slezina	k1gFnSc1	slezina
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
bažantů	bažant	k1gMnPc2	bažant
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
bývá	bývat	k5eAaImIp3nS	bývat
nezměněna	změněn	k2eNgFnSc1d1	nezměněna
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
atrofovaná	atrofovaný	k2eAgFnSc1d1	atrofovaná
<g/>
.	.	kIx.	.
</s>
<s>
Játra	játra	k1gNnPc1	játra
jsou	být	k5eAaImIp3nP	být
zvětšená	zvětšený	k2eAgNnPc1d1	zvětšené
<g/>
,	,	kIx,	,
s	s	k7c7	s
hemoragiemi	hemoragie	k1gFnPc7	hemoragie
a	a	k8xC	a
nekrotickými	nekrotický	k2eAgNnPc7d1	nekrotické
ložisky	ložisko	k1gNnPc7	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Ledviny	ledvina	k1gFnPc1	ledvina
jsou	být	k5eAaImIp3nP	být
zduřelé	zduřelý	k2eAgFnPc1d1	zduřelá
<g/>
,	,	kIx,	,
močovody	močovod	k1gInPc1	močovod
naplněné	naplněný	k2eAgInPc1d1	naplněný
uráty	urát	k1gInPc1	urát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
mukoidní	mukoidní	k2eAgFnSc1d1	mukoidní
hemoragická	hemoragický	k2eAgFnSc1d1	hemoragická
enteritida	enteritida	k1gFnSc1	enteritida
<g/>
.	.	kIx.	.
</s>
<s>
Překonáním	překonání	k1gNnSc7	překonání
nemoci	nemoc	k1gFnSc2	nemoc
nebo	nebo	k8xC	nebo
po	po	k7c6	po
imunizaci	imunizace	k1gFnSc6	imunizace
vzniká	vznikat	k5eAaImIp3nS	vznikat
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
aktivní	aktivní	k2eAgFnSc1d1	aktivní
imunita	imunita	k1gFnSc1	imunita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
typově	typově	k6eAd1	typově
specifická	specifický	k2eAgFnSc1d1	specifická
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
rekonvalescenti	rekonvalescent	k1gMnPc1	rekonvalescent
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
opětovně	opětovně	k6eAd1	opětovně
infikováni	infikovat	k5eAaBmNgMnP	infikovat
jinými	jiný	k2eAgInPc7d1	jiný
sérotypy	sérotyp	k1gInPc4	sérotyp
B.	B.	kA	B.
anserina	anserina	k1gFnSc1	anserina
<g/>
.	.	kIx.	.
</s>
<s>
Imunní	imunní	k2eAgFnPc1d1	imunní
nosnice	nosnice	k1gFnPc1	nosnice
přenášejí	přenášet	k5eAaImIp3nP	přenášet
na	na	k7c4	na
potomstvo	potomstvo	k1gNnSc4	potomstvo
protilátky	protilátka	k1gFnSc2	protilátka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
je	být	k5eAaImIp3nS	být
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
nákazou	nákaza	k1gFnSc7	nákaza
asi	asi	k9	asi
5-6	[number]	k4	5-6
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pasivní	pasivní	k2eAgFnSc7d1	pasivní
imunizací	imunizace	k1gFnSc7	imunizace
hyperimunním	hyperimunní	k2eAgNnSc7d1	hyperimunní
sérem	sérum	k1gNnSc7	sérum
lze	lze	k6eAd1	lze
zajistit	zajistit	k5eAaPmF	zajistit
až	až	k9	až
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
trvající	trvající	k2eAgFnSc4d1	trvající
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Předběžnou	předběžný	k2eAgFnSc4d1	předběžná
diagnózu	diagnóza	k1gFnSc4	diagnóza
borreliózy	borrelióza	k1gFnSc2	borrelióza
lze	lze	k6eAd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
při	při	k7c6	při
nálezu	nález	k1gInSc6	nález
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
najde	najít	k5eAaPmIp3nS	najít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
u	u	k7c2	u
postižených	postižený	k2eAgMnPc2d1	postižený
ptáků	pták	k1gMnPc2	pták
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
chovném	chovný	k2eAgNnSc6d1	chovné
prostředí	prostředí	k1gNnSc6	prostředí
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
klíšťáků	klíšťák	k1gMnPc2	klíšťák
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
diagnózy	diagnóza	k1gFnSc2	diagnóza
prokázat	prokázat	k5eAaPmF	prokázat
přítomnost	přítomnost	k1gFnSc1	přítomnost
B.	B.	kA	B.
anserina	anserino	k1gNnSc2	anserino
nebo	nebo	k8xC	nebo
jejích	její	k3xOp3gInPc2	její
antigenů	antigen	k1gInPc2	antigen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
klinického	klinický	k2eAgNnSc2d1	klinické
onemocnění	onemocnění	k1gNnSc2	onemocnění
lze	lze	k6eAd1	lze
detekovat	detekovat	k5eAaImF	detekovat
spirochéty	spirochéta	k1gFnPc4	spirochéta
v	v	k7c6	v
barvených	barvený	k2eAgInPc6d1	barvený
krevních	krevní	k2eAgInPc6d1	krevní
nátěrech	nátěr	k1gInPc6	nátěr
nebo	nebo	k8xC	nebo
v	v	k7c6	v
otiskových	otiskův	k2eAgInPc6d1	otiskův
preparátech	preparát	k1gInPc6	preparát
z	z	k7c2	z
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vyšetřením	vyšetření	k1gNnSc7	vyšetření
nativních	nativní	k2eAgInPc2d1	nativní
preparátů	preparát	k1gInPc2	preparát
v	v	k7c6	v
zástinu	zástin	k1gInSc6	zástin
<g/>
,	,	kIx,	,
fázovým	fázový	k2eAgInSc7d1	fázový
kontrastem	kontrast	k1gInSc7	kontrast
či	či	k8xC	či
imunofluorescencí	imunofluorescence	k1gFnSc7	imunofluorescence
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
průkaz	průkaz	k1gInSc4	průkaz
spirochét	spirochéta	k1gFnPc2	spirochéta
ve	v	k7c6	v
tkáňových	tkáňový	k2eAgInPc6d1	tkáňový
řezech	řez	k1gInPc6	řez
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
technika	technika	k1gFnSc1	technika
impregnace	impregnace	k1gFnSc2	impregnace
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Borelióza	borelióza	k1gFnSc1	borelióza
může	moct	k5eAaImIp3nS	moct
připomínat	připomínat	k5eAaImF	připomínat
jiné	jiný	k2eAgFnPc4d1	jiná
akutní	akutní	k2eAgFnPc4d1	akutní
septikemické	septikemický	k2eAgFnPc4d1	septikemický
nemoci	nemoc	k1gFnPc4	nemoc
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
salmonelózy	salmonelóza	k1gFnPc1	salmonelóza
<g/>
,	,	kIx,	,
cholera	cholera	k1gFnSc1	cholera
a	a	k8xC	a
koliseptikémie	koliseptikémie	k1gFnSc1	koliseptikémie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
akutních	akutní	k2eAgFnPc2d1	akutní
virových	virový	k2eAgFnPc2d1	virová
infekcí	infekce	k1gFnPc2	infekce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nutné	nutný	k2eAgNnSc1d1	nutné
vyloučit	vyloučit	k5eAaPmF	vyloučit
velogenní	velogenní	k2eAgFnSc4d1	velogenní
viscerotropní	viscerotropní	k2eAgFnSc4d1	viscerotropní
formu	forma	k1gFnSc4	forma
Newcastleské	Newcastleský	k2eAgFnSc2d1	Newcastleská
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
fatální	fatální	k2eAgFnSc4d1	fatální
formu	forma	k1gFnSc4	forma
influenzy	influenza	k1gFnSc2	influenza
a	a	k8xC	a
akutní	akutní	k2eAgFnSc4d1	akutní
Markovu	Markův	k2eAgFnSc4d1	Markova
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
používané	používaný	k2eAgInPc1d1	používaný
arzenové	arzenový	k2eAgInPc1d1	arzenový
přípravky	přípravek	k1gInPc1	přípravek
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
nahrazenyantibiotiky	nahrazenyantibiotika	k1gFnPc1	nahrazenyantibiotika
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
anserina	anserina	k1gFnSc1	anserina
je	být	k5eAaImIp3nS	být
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
včetně	včetně	k7c2	včetně
penicilinu	penicilin	k1gInSc2	penicilin
<g/>
,	,	kIx,	,
<g/>
chloramfenikolu	chloramfenikol	k1gInSc2	chloramfenikol
<g/>
,	,	kIx,	,
kanamycinu	kanamycin	k1gInSc2	kanamycin
<g/>
,	,	kIx,	,
streptomycinu	streptomycin	k1gInSc2	streptomycin
<g/>
,	,	kIx,	,
tylosinu	tylosina	k1gFnSc4	tylosina
atetracyklinů	atetracyklin	k1gInPc2	atetracyklin
<g/>
.	.	kIx.	.
</s>
<s>
Preventivní	preventivní	k2eAgNnPc1d1	preventivní
opatření	opatření	k1gNnPc1	opatření
jsou	být	k5eAaImIp3nP	být
zaměřena	zaměřit	k5eAaPmNgNnP	zaměřit
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
zoohygienických	zoohygienický	k2eAgFnPc2d1	zoohygienický
zásad	zásada	k1gFnPc2	zásada
chovu	chov	k1gInSc2	chov
a	a	k8xC	a
zejména	zejména	k9	zejména
na	na	k7c4	na
ničení	ničení	k1gNnSc4	ničení
členovců	členovec	k1gMnPc2	členovec
(	(	kIx(	(
<g/>
insekticidní	insekticidní	k2eAgInPc1d1	insekticidní
přípravky	přípravek	k1gInPc1	přípravek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eradikace	Eradikace	k1gFnSc1	Eradikace
klíšťáků	klíšťák	k1gMnPc2	klíšťák
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
jako	jako	k9	jako
nosiči	nosič	k1gMnPc1	nosič
mohou	moct	k5eAaImIp3nP	moct
přežívat	přežívat	k5eAaImF	přežívat
po	po	k7c4	po
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
endemickým	endemický	k2eAgInSc7d1	endemický
výskytem	výskyt	k1gInSc7	výskyt
borreliózy	borrelióza	k1gFnSc2	borrelióza
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
autogenní	autogenní	k2eAgFnPc1d1	autogenní
nebo	nebo	k8xC	nebo
polyvalentní	polyvalentní	k2eAgFnPc1d1	polyvalentní
vakcíny	vakcína	k1gFnPc1	vakcína
inaktivované	inaktivovaný	k2eAgFnPc1d1	inaktivovaná
formalinem	formalin	k1gInSc7	formalin
nebo	nebo	k8xC	nebo
fenolem	fenol	k1gInSc7	fenol
<g/>
.	.	kIx.	.
</s>
<s>
JURAJDA	JURAJDA	kA	JURAJDA
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ptactva	ptactvo	k1gNnSc2	ptactvo
-	-	kIx~	-
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
a	a	k8xC	a
mykotické	mykotický	k2eAgFnSc2d1	mykotická
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
ES	ES	kA	ES
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7305	[number]	k4	7305
<g/>
-	-	kIx~	-
<g/>
464	[number]	k4	464
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
SAIF	SAIF	kA	SAIF
<g/>
,	,	kIx,	,
Y.	Y.	kA	Y.
<g/>
M.	M.	kA	M.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Diseases	Diseases	k1gInSc1	Diseases
of	of	k?	of
Poultry	Poultr	k1gMnPc7	Poultr
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ames	Ames	k1gInSc1	Ames
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Iowa	Iowa	k1gMnSc1	Iowa
State	status	k1gInSc5	status
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Blackwell	Blackwell	k1gMnSc1	Blackwell
Publ	Publ	k1gMnSc1	Publ
<g/>
.	.	kIx.	.
</s>
<s>
Comp	Comp	k1gInSc1	Comp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
1231	[number]	k4	1231
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8138	[number]	k4	8138
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
423	[number]	k4	423
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
ALTMAN	Altman	k1gMnSc1	Altman
<g/>
,	,	kIx,	,
R.	R.	kA	R.
<g/>
B.	B.	kA	B.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
Avian	Avian	k1gInSc1	Avian
Medicine	Medicin	k1gInSc5	Medicin
and	and	k?	and
Surgery	Surger	k1gMnPc7	Surger
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
:	:	kIx,	:
W.B.	W.B.	k1gFnSc1	W.B.
Saunders	Saunders	k1gInSc1	Saunders
Co	co	k3yQnSc1	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
1070	[number]	k4	1070
s.	s.	k?	s.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
RITCHIE	RITCHIE	kA	RITCHIE
<g/>
,	,	kIx,	,
B.W.	B.W.	k1gFnSc1	B.W.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
Avian	Avian	k1gInSc1	Avian
Medicine	Medicin	k1gInSc5	Medicin
<g/>
:	:	kIx,	:
Principles	Principles	k1gInSc1	Principles
and	and	k?	and
Application	Application	k1gInSc1	Application
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Wingers	Wingers	k1gInSc1	Wingers
Publ	Publ	k1gInSc1	Publ
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
1384	[number]	k4	1384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
9636996	[number]	k4	9636996
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
