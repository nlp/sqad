<p>
<s>
Nosorožcovití	Nosorožcovitý	k2eAgMnPc1d1	Nosorožcovitý
(	(	kIx(	(
<g/>
Rhinocerotidae	Rhinocerotidae	k1gNnSc7	Rhinocerotidae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
recentních	recentní	k2eAgFnPc2d1	recentní
(	(	kIx(	(
<g/>
žijících	žijící	k2eAgFnPc2d1	žijící
<g/>
)	)	kIx)	)
čeledí	čeleď	k1gFnPc2	čeleď
řádu	řád	k1gInSc2	řád
lichokopytníků	lichokopytník	k1gMnPc2	lichokopytník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
známe	znát	k5eAaImIp1nP	znát
5	[number]	k4	5
recentních	recentní	k2eAgMnPc2d1	recentní
druhů	druh	k1gMnPc2	druh
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
nosorožců	nosorožec	k1gMnPc2	nosorožec
jsou	být	k5eAaImIp3nP	být
ohrožené	ohrožený	k2eAgFnPc1d1	ohrožená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nosorožci	nosorožec	k1gMnPc1	nosorožec
mají	mít	k5eAaImIp3nP	mít
tělo	tělo	k1gNnSc4	tělo
pokryté	pokrytý	k2eAgFnPc4d1	pokrytá
kožovitými	kožovitý	k2eAgInPc7d1	kožovitý
pláty	plát	k1gInPc7	plát
jako	jako	k8xS	jako
pancířem	pancíř	k1gInSc7	pancíř
<g/>
,	,	kIx,	,
také	také	k9	také
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
žeber	žebro	k1gNnPc2	žebro
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
lichokopytníci	lichokopytník	k1gMnPc1	lichokopytník
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
hmotnosti	hmotnost	k1gFnSc3	hmotnost
až	až	k9	až
3,6	[number]	k4	3,6
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čich	čich	k1gInSc4	čich
a	a	k8xC	a
sluch	sluch	k1gInSc4	sluch
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
citlivý	citlivý	k2eAgInSc4d1	citlivý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
špatně	špatně	k6eAd1	špatně
vidí	vidět	k5eAaImIp3nP	vidět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
krátkozrací	krátkozraký	k2eAgMnPc1d1	krátkozraký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Roh	roh	k1gInSc4	roh
==	==	k?	==
</s>
</p>
<p>
<s>
Roh	roh	k1gInSc1	roh
nosorožce	nosorožec	k1gMnSc2	nosorožec
není	být	k5eNaImIp3nS	být
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
kostí	kost	k1gFnSc7	kost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
pouze	pouze	k6eAd1	pouze
rohovinou	rohovina	k1gFnSc7	rohovina
<g/>
,	,	kIx,	,
keratinem	keratin	k1gInSc7	keratin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lov	lov	k1gInSc1	lov
pro	pro	k7c4	pro
rohy	roh	k1gInPc4	roh
===	===	k?	===
</s>
</p>
<p>
<s>
Nosorožci	nosorožec	k1gMnPc1	nosorožec
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
rohy	roh	k1gInPc4	roh
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
ilegálně	ilegálně	k6eAd1	ilegálně
loveni	loven	k2eAgMnPc1d1	loven
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rozdrcený	rozdrcený	k2eAgInSc1d1	rozdrcený
roh	roh	k1gInSc1	roh
čili	čili	k8xC	čili
prášek	prášek	k1gInSc1	prášek
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
nosorožce	nosorožec	k1gMnPc4	nosorožec
tvoří	tvořit	k5eAaImIp3nS	tvořit
totiž	totiž	k9	totiž
součást	součást	k1gFnSc1	součást
některých	některý	k3yIgNnPc2	některý
lidových	lidový	k2eAgNnPc2d1	lidové
léčiv	léčivo	k1gNnPc2	léčivo
a	a	k8xC	a
afrodisiak	afrodisiakum	k1gNnPc2	afrodisiakum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
čínské	čínský	k2eAgFnSc6d1	čínská
medicíně	medicína	k1gFnSc6	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Čína	Čína	k1gFnSc1	Čína
již	již	k6eAd1	již
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
,	,	kIx,	,
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
přírody	příroda	k1gFnSc2	příroda
podle	podle	k7c2	podle
CITES	CITES	kA	CITES
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
z	z	k7c2	z
rohů	roh	k1gInPc2	roh
zase	zase	k9	zase
vyřezávají	vyřezávat	k5eAaImIp3nP	vyřezávat
rukojeti	rukojeť	k1gFnPc1	rukojeť
tradičních	tradiční	k2eAgInPc2d1	tradiční
nožů	nůž	k1gInPc2	nůž
a	a	k8xC	a
dýk	dýka	k1gFnPc2	dýka
<g/>
.	.	kIx.	.
<g/>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
"	"	kIx"	"
<g/>
nutnost	nutnost	k1gFnSc1	nutnost
lovu	lov	k1gInSc2	lov
nosorožců	nosorožec	k1gMnPc2	nosorožec
<g/>
"	"	kIx"	"
celosvětově	celosvětově	k6eAd1	celosvětově
odmítána	odmítán	k2eAgFnSc1d1	odmítána
jako	jako	k8xC	jako
přežitek	přežitek	k1gInSc4	přežitek
a	a	k8xC	a
barbarství	barbarství	k1gNnSc4	barbarství
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ovšem	ovšem	k9	ovšem
lidová	lidový	k2eAgFnSc1d1	lidová
praxe	praxe	k1gFnSc1	praxe
jim	on	k3xPp3gMnPc3	on
stále	stále	k6eAd1	stále
nepřeje	přát	k5eNaImIp3nS	přát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systém	systém	k1gInSc1	systém
nosorožcovitých	nosorožcovitý	k2eAgMnPc2d1	nosorožcovitý
==	==	k?	==
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
<g/>
Ceratotherium	Ceratotherium	k1gNnSc1	Ceratotherium
</s>
</p>
<p>
<s>
Nosorožec	nosorožec	k1gMnSc1	nosorožec
tuponosý	tuponosý	k2eAgMnSc1d1	tuponosý
(	(	kIx(	(
<g/>
Ceratotherium	Ceratotherium	k1gNnSc1	Ceratotherium
simum	simum	k1gNnSc1	simum
<g/>
)	)	kIx)	)
–	–	k?	–
bílý	bílý	k1gMnSc1	bílý
nosorožec	nosorožec	k1gMnSc1	nosorožec
<g/>
,	,	kIx,	,
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
</s>
</p>
<p>
<s>
Nosorožec	nosorožec	k1gMnSc1	nosorožec
tuponosý	tuponosý	k2eAgMnSc1d1	tuponosý
severní	severní	k2eAgFnSc7d1	severní
</s>
</p>
<p>
<s>
Nosorožec	nosorožec	k1gMnSc1	nosorožec
tuponosý	tuponosý	k2eAgInSc4d1	tuponosý
jižníRod	jižníRod	k1gInSc4	jižníRod
<g/>
:	:	kIx,	:
Diceros	Dicerosa	k1gFnPc2	Dicerosa
</s>
</p>
<p>
<s>
Nosorožec	nosorožec	k1gMnSc1	nosorožec
dvourohý	dvourohý	k2eAgMnSc1d1	dvourohý
(	(	kIx(	(
<g/>
Diceros	Dicerosa	k1gFnPc2	Dicerosa
bicornis	bicornis	k1gInSc1	bicornis
<g/>
)	)	kIx)	)
–	–	k?	–
černý	černý	k1gMnSc1	černý
nosorožec	nosorožec	k1gMnSc1	nosorožec
<g/>
,	,	kIx,	,
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
v	v	k7c4	v
AfriceRod	AfriceRod	k1gInSc4	AfriceRod
<g/>
:	:	kIx,	:
Dicerorhinus	Dicerorhinus	k1gInSc1	Dicerorhinus
</s>
</p>
<p>
<s>
Nosorožec	nosorožec	k1gMnSc1	nosorožec
sumaterský	sumaterský	k2eAgMnSc1d1	sumaterský
(	(	kIx(	(
<g/>
Dicerorhinus	Dicerorhinus	k1gInSc1	Dicerorhinus
sumatrensis	sumatrensis	k1gFnSc2	sumatrensis
<g/>
)	)	kIx)	)
–	–	k?	–
kriticky	kriticky	k6eAd1	kriticky
ohroženýRod	ohroženýRoda	k1gFnPc2	ohroženýRoda
<g/>
:	:	kIx,	:
Rhinoceros	rhinoceros	k1gMnSc1	rhinoceros
</s>
</p>
<p>
<s>
Nosorožec	nosorožec	k1gMnSc1	nosorožec
jávský	jávský	k2eAgMnSc1d1	jávský
(	(	kIx(	(
<g/>
Rhinoceros	rhinoceros	k1gMnSc1	rhinoceros
sondaicus	sondaicus	k1gMnSc1	sondaicus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nosorožec	nosorožec	k1gMnSc1	nosorožec
indický	indický	k2eAgMnSc1d1	indický
(	(	kIx(	(
<g/>
Rhinoceros	rhinoceros	k1gMnSc1	rhinoceros
unicornis	unicornis	k1gFnSc2	unicornis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhynulé	vyhynulý	k2eAgInPc1d1	vyhynulý
druhy	druh	k1gInPc1	druh
===	===	k?	===
</s>
</p>
<p>
<s>
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Coelodonta	Coelodonta	k1gFnSc1	Coelodonta
</s>
</p>
<p>
<s>
†	†	k?	†
Nosorožec	nosorožec	k1gMnSc1	nosorožec
srstnatý	srstnatý	k2eAgMnSc1d1	srstnatý
(	(	kIx(	(
<g/>
Coelodonta	Coelodonta	k1gMnSc1	Coelodonta
antiquitatis	antiquitatis	k1gFnSc2	antiquitatis
<g/>
)	)	kIx)	)
<g/>
Rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
Elasmotherium	Elasmotherium	k1gNnSc1	Elasmotherium
</s>
</p>
<p>
<s>
†	†	k?	†
Nosorožec	nosorožec	k1gMnSc1	nosorožec
jednorohý	jednorohý	k2eAgMnSc1d1	jednorohý
(	(	kIx(	(
<g/>
Elasmotherium	Elasmotherium	k1gNnSc1	Elasmotherium
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nosorožcovití	nosorožcovitý	k2eAgMnPc1d1	nosorožcovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
nosorožec	nosorožec	k1gMnSc1	nosorožec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Rhinocerotidae	Rhinocerotidae	k1gInSc1	Rhinocerotidae
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
