<s>
Petr	Petr	k1gMnSc1	Petr
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
1964	[number]	k4	1964
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
manažer	manažer	k1gMnSc1	manažer
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
generální	generální	k2eAgFnSc4d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
technickou	technický	k2eAgFnSc4d1	technická
kybernetiku	kybernetika	k1gFnSc4	kybernetika
na	na	k7c6	na
Elektrotechnické	elektrotechnický	k2eAgFnSc6d1	elektrotechnická
fakultě	fakulta	k1gFnSc6	fakulta
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
MBA	MBA	kA	MBA
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
PR	pr	k0	pr
agenturu	agentura	k1gFnSc4	agentura
B.I.G.	B.I.G.	k1gMnSc3	B.I.G.
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
působí	působit	k5eAaImIp3nP	působit
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
PPF	PPF	kA	PPF
<g/>
,	,	kIx,	,
za	za	k7c7	za
níž	jenž	k3xRgFnSc7	jenž
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
eBanky	eBanka	k1gMnSc2	eBanka
a	a	k8xC	a
v	v	k7c6	v
představenstvu	představenstvo	k1gNnSc6	představenstvo
Slavie	slavie	k1gFnSc2	slavie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
PPF	PPF	kA	PPF
do	do	k7c2	do
televize	televize	k1gFnSc2	televize
Nova	nova	k1gFnSc1	nova
a	a	k8xC	a
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
jednatelem	jednatel	k1gMnSc7	jednatel
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2003	[number]	k4	2003
pak	pak	k6eAd1	pak
generálním	generální	k2eAgMnSc7d1	generální
ředitelem	ředitel	k1gMnSc7	ředitel
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Novu	nova	k1gFnSc4	nova
koupila	koupit	k5eAaPmAgFnS	koupit
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
CME	CME	kA	CME
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vedení	vedení	k1gNnSc2	vedení
Novy	nova	k1gFnSc2	nova
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2010	[number]	k4	2010
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
skupiny	skupina	k1gFnSc2	skupina
CME	CME	kA	CME
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
také	také	k9	také
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
místopředsedy	místopředseda	k1gMnSc2	místopředseda
představenstva	představenstvo	k1gNnSc2	představenstvo
společnosti	společnost	k1gFnSc2	společnost
Gopas	Gopas	k1gMnSc1	Gopas
provozující	provozující	k2eAgFnSc4d1	provozující
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Radou	Rada	k1gMnSc7	Rada
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
televize	televize	k1gFnSc2	televize
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
mu	on	k3xPp3gMnSc3	on
skončí	skončit	k5eAaPmIp3nS	skončit
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Evu	Eva	k1gFnSc4	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozvedený	rozvedený	k2eAgMnSc1d1	rozvedený
<g/>
,	,	kIx,	,
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
partnerkou	partnerka	k1gFnSc7	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
zvolení	zvolení	k1gNnSc2	zvolení
pak	pak	k6eAd1	pak
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInSc2	komentář
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
přinesly	přinést	k5eAaPmAgFnP	přinést
informaci	informace	k1gFnSc4	informace
z	z	k7c2	z
Národního	národní	k2eAgInSc2d1	národní
archivu	archiv	k1gInSc2	archiv
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kandidátem	kandidát	k1gMnSc7	kandidát
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1987	[number]	k4	1987
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
členem	člen	k1gInSc7	člen
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Dvořák	Dvořák	k1gMnSc1	Dvořák
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnSc2	jeho
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
"	"	kIx"	"
členem	člen	k1gMnSc7	člen
KSČ	KSČ	kA	KSČ
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
a	a	k8xC	a
že	že	k8xS	že
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c4	po
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
kandidatury	kandidatura	k1gFnSc2	kandidatura
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
důvěryhodnost	důvěryhodnost	k1gFnSc4	důvěryhodnost
kartotéky	kartotéka	k1gFnSc2	kartotéka
členů	člen	k1gMnPc2	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Petra	Petr	k1gMnSc2	Petr
Blažka	Blažek	k1gMnSc2	Blažek
ovšem	ovšem	k9	ovšem
neexistuje	existovat	k5eNaImIp3nS	existovat
jediný	jediný	k2eAgInSc1d1	jediný
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nP	by
doklady	doklad	k1gInPc1	doklad
o	o	k7c6	o
Dvořákově	Dvořákův	k2eAgNnSc6d1	Dvořákovo
členství	členství	k1gNnSc6	členství
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
zmanipulovány	zmanipulován	k2eAgFnPc1d1	zmanipulována
<g/>
.	.	kIx.	.
</s>
<s>
Samotnou	samotný	k2eAgFnSc4d1	samotná
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Blažek	Blažka	k1gFnPc2	Blažka
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
]	]	kIx)	]
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
komunistické	komunistický	k2eAgFnSc3d1	komunistická
ideologii	ideologie	k1gFnSc3	ideologie
nevěřili	věřit	k5eNaImAgMnP	věřit
často	často	k6eAd1	často
ani	ani	k8xC	ani
samotní	samotný	k2eAgMnPc1d1	samotný
představitelé	představitel	k1gMnPc1	představitel
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
morální	morální	k2eAgFnSc1d1	morální
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
<g/>
,	,	kIx,	,
dejme	dát	k5eAaPmRp1nP	dát
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
rozhledu	rozhled	k1gInSc6	rozhled
či	či	k8xC	či
morálním	morální	k2eAgInSc6d1	morální
profilu	profil	k1gInSc6	profil
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
