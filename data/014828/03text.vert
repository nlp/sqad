<s>
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
doc.	doc.	kA
RNDr.	RNDr.	kA
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Dr	dr	kA
<g/>
.	.	kIx.
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
na	na	k7c6
festivalu	festival	k1gInSc6
Svět	svět	k1gInSc1
knihy	kniha	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1958	#num#	k4
(	(	kIx(
<g/>
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Louny	Louny	k1gInPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
pedagog	pedagog	k1gMnSc1
a	a	k8xC
jazykovědec	jazykovědec	k1gMnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
Témata	téma	k1gNnPc4
</s>
<s>
matematická	matematický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
(	(	kIx(
<g/>
dědeček	dědeček	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
citáty	citát	k1gInPc1
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1958	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
jazykovědec	jazykovědec	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odborně	odborně	k6eAd1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
matematickou	matematický	k2eAgFnSc7d1
lingvistikou	lingvistika	k1gFnSc7
a	a	k8xC
formální	formální	k2eAgFnSc7d1
syntaxí	syntax	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2003	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
ředitel	ředitel	k1gMnSc1
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
činný	činný	k2eAgMnSc1d1
na	na	k7c6
katedře	katedra	k1gFnSc6
informatiky	informatika	k1gFnSc2
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
J.	J.	kA
E.	E.	kA
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Profesní	profesní	k2eAgInSc1d1
životopis	životopis	k1gInSc1
</s>
<s>
Po	po	k7c6
maturitě	maturita	k1gFnSc6
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
vystudoval	vystudovat	k5eAaPmAgMnS
na	na	k7c6
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
UK	UK	kA
obor	obor	k1gInSc1
informatika	informatika	k1gFnSc1
se	s	k7c7
specializací	specializace	k1gFnSc7
na	na	k7c4
matematickou	matematický	k2eAgFnSc4d1
lingvistiku	lingvistika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozmezí	rozmezí	k1gNnSc6
let	léto	k1gNnPc2
1983	#num#	k4
až	až	k6eAd1
1988	#num#	k4
byl	být	k5eAaImAgMnS
interním	interní	k2eAgMnSc7d1
aspirantem	aspirant	k1gMnSc7
této	tento	k3xDgFnSc2
fakulty	fakulta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoji	svůj	k3xOyFgFnSc4
dokončenou	dokončený	k2eAgFnSc4d1
disertační	disertační	k2eAgFnSc4d1
práci	práce	k1gFnSc4
však	však	k9
mohl	moct	k5eAaImAgInS
obhájit	obhájit	k5eAaPmF
až	až	k9
osm	osm	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
Sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
UK	UK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ještě	ještě	k6eAd1
v	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1989	#num#	k4
odešel	odejít	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
ilegálně	ilegálně	k6eAd1
do	do	k7c2
Bulharska	Bulharsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k8xS,k8xC
vědecký	vědecký	k2eAgMnSc1d1
pracovník	pracovník	k1gMnSc1
sekce	sekce	k1gFnSc2
matematické	matematický	k2eAgFnSc2d1
lingvistiky	lingvistika	k1gFnSc2
Koordinačního	koordinační	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
pro	pro	k7c4
informatiku	informatika	k1gFnSc4
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
Bulharské	bulharský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
roku	rok	k1gInSc2
1989	#num#	k4
svůj	svůj	k3xOyFgInSc4
pobyt	pobyt	k1gInSc4
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
zlegalizoval	zlegalizovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Ústavu	ústav	k1gInSc6
matematické	matematický	k2eAgFnSc2d1
lingvistiky	lingvistika	k1gFnSc2
Sárské	sárský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Saarbrückenu	Saarbrücken	k1gInSc6
působil	působit	k5eAaImAgMnS
Oliva	Oliva	k1gMnSc1
od	od	k7c2
dubna	duben	k1gInSc2
roku	rok	k1gInSc2
1990	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tady	tady	k6eAd1
se	se	k3xPyFc4
také	také	k9
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
habilitoval	habilitovat	k5eAaBmAgMnS
v	v	k7c6
oboru	obor	k1gInSc6
všeobecná	všeobecný	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
listopadu	listopad	k1gInSc2
roku	rok	k1gInSc2
2001	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
působil	působit	k5eAaImAgMnS
v	v	k7c6
oddělení	oddělení	k1gNnSc6
matematické	matematický	k2eAgFnSc2d1
lingvistiky	lingvistika	k1gFnSc2
Rakouského	rakouský	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
(	(	kIx(
<g/>
OeFAI	OeFAI	k1gMnSc1
<g/>
)	)	kIx)
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Funkci	funkce	k1gFnSc4
ředitele	ředitel	k1gMnSc2
Ústavu	ústav	k1gInSc2
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
AV	AV	kA
ČR	ČR	kA
zastával	zastávat	k5eAaImAgMnS
v	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
června	červen	k1gInSc2
2003	#num#	k4
do	do	k7c2
září	září	k1gNnSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c4
ukončení	ukončení	k1gNnSc4
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
ředitelského	ředitelský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
nemělo	mít	k5eNaImAgNnS
nové	nový	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
ústavu	ústav	k1gInSc2
zájem	zájem	k1gInSc4
o	o	k7c4
jeho	jeho	k3xOp3gNnSc4
další	další	k2eAgNnSc4d1
působení	působení	k1gNnSc4
v	v	k7c6
instituci	instituce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oliva	Oliva	k1gMnSc1
se	se	k3xPyFc4
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
především	především	k6eAd1
aktivitám	aktivita	k1gFnPc3
popularizujícím	popularizující	k2eAgFnPc3d1
jazykovědu	jazykověda	k1gFnSc4
a	a	k8xC
jazykovědné	jazykovědný	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mj.	mj.	kA
autorem	autor	k1gMnSc7
téměř	téměř	k6eAd1
400	#num#	k4
scénářů	scénář	k1gInPc2
k	k	k7c3
rozhlasovému	rozhlasový	k2eAgInSc3d1
pořadu	pořad	k1gInSc3
Slovo	slovo	k1gNnSc4
nad	nad	k7c4
zlato	zlato	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
také	také	k9
vystupoval	vystupovat	k5eAaImAgMnS
spolu	spolu	k6eAd1
s	s	k7c7
moderátorem	moderátor	k1gMnSc7
Janem	Jan	k1gMnSc7
Rosákem	Rosák	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
autorem	autor	k1gMnSc7
více	hodně	k6eAd2
než	než	k8xS
150	#num#	k4
pořadů	pořad	k1gInPc2
z	z	k7c2
cyklu	cyklus	k1gInSc2
Uzlíky	uzlík	k1gInPc4
na	na	k7c6
jazyku	jazyk	k1gInSc6
na	na	k7c6
internetové	internetový	k2eAgFnSc6d1
televizi	televize	k1gFnSc6
Stream	Stream	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
se	se	k3xPyFc4
odborně	odborně	k6eAd1
zajímá	zajímat	k5eAaImIp3nS
o	o	k7c4
formální	formální	k2eAgFnSc4d1
syntax	syntax	k1gFnSc4
přirozených	přirozený	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
o	o	k7c4
inovativní	inovativní	k2eAgInPc4d1
přístupy	přístup	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vedou	vést	k5eAaImIp3nP
k	k	k7c3
praktickým	praktický	k2eAgFnPc3d1
(	(	kIx(
<g/>
tj.	tj.	kA
počítačovým	počítačový	k2eAgFnPc3d1
<g/>
)	)	kIx)
aplikacím	aplikace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
časopisech	časopis	k1gInPc6
a	a	k8xC
sbornících	sborník	k1gInPc6
odborných	odborný	k2eAgFnPc2d1
konferencí	konference	k1gFnPc2
publikoval	publikovat	k5eAaBmAgInS
přes	přes	k7c4
90	#num#	k4
původních	původní	k2eAgFnPc2d1
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
svého	svůj	k3xOyFgNnSc2
pestrého	pestrý	k2eAgNnSc2d1
profesního	profesní	k2eAgNnSc2d1
působení	působení	k1gNnSc2
zastával	zastávat	k5eAaImAgMnS
i	i	k8xC
funkce	funkce	k1gFnSc1
vedoucích	vedoucí	k1gMnPc2
výzkumných	výzkumný	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oliva	Oliva	k1gMnSc1
je	být	k5eAaImIp3nS
spoluautorem	spoluautor	k1gMnSc7
implementací	implementace	k1gFnPc2
řady	řada	k1gFnSc2
softwarových	softwarový	k2eAgInPc2d1
celků	celek	k1gInPc2
(	(	kIx(
<g/>
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
např.	např.	kA
korektoru	korektor	k1gInSc3
české	český	k2eAgFnSc2d1
gramatiky	gramatika	k1gFnSc2
pro	pro	k7c4
Microsoft	Microsoft	kA
Office	Office	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
bulharsky	bulharsky	k6eAd1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
<g/>
,	,	kIx,
německy	německy	k6eAd1
a	a	k8xC
rusky	rusky	k6eAd1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
ovládá	ovládat	k5eAaImIp3nS
i	i	k9
řadu	řada	k1gFnSc4
dalších	další	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Historie	historie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústav	ústav	k1gInSc1
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
2016-10-03	2016-10-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
končí	končit	k5eAaImIp3nS
v	v	k7c6
Ústavu	ústav	k1gInSc6
pro	pro	k7c4
jazyk	jazyk	k1gInSc4
český	český	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
mě	já	k3xPp1nSc4
nepotřebují	potřebovat	k5eNaImIp3nP
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-09-29	2016-09-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
roku	rok	k1gInSc2
2016	#num#	k4
podle	podle	k7c2
Olivy	oliva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-01-25	2017-01-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Povídka	povídka	k1gFnSc1
roku	rok	k1gInSc2
podruhé	podruhé	k6eAd1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
anonymně	anonymně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KUBIŠTOVÁ	Kubištová	k1gFnSc1
<g/>
,	,	kIx,
Pavla	Pavla	k1gFnSc1
<g/>
;	;	kIx,
RADINOVÁ	RADINOVÁ	kA
<g/>
,	,	kIx,
Jolana	Jolana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozstřel	rozstřel	k1gInSc1
<g/>
:	:	kIx,
Přechylování	přechylování	k1gNnPc2
se	se	k3xPyFc4
nevyhneme	vyhnout	k5eNaPmIp1nP
<g/>
,	,	kIx,
bez	bez	k7c2
něj	on	k3xPp3gInSc2
se	se	k3xPyFc4
věta	věta	k1gFnSc1
rozpadne	rozpadnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Oliva	oliva	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-10-07	2016-10-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Jazykověda	jazykověda	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karel	Karla	k1gFnPc2
Oliva	oliva	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
Show	show	k1gFnSc1
Jana	Jan	k1gMnSc2
Krause	Kraus	k1gMnSc2
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
mi	já	k3xPp1nSc3
nabídl	nabídnout	k5eAaPmAgMnS
práci	práce	k1gFnSc4
pro	pro	k7c4
brigádníky	brigádník	k1gMnPc4
<g/>
,	,	kIx,
urazil	urazit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ústavu	ústav	k1gInSc6
končím	končit	k5eAaImIp1nS
kvůli	kvůli	k7c3
drbům	drb	k1gInPc3
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Oliva	Oliva	k1gMnSc1
DVTV	DVTV	kA
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sorry	sorry	k9
jako	jako	k8xC,k8xS
=	=	kIx~
slovo	slovo	k1gNnSc1
roku	rok	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
by	by	kYmCp3nP
vybírat	vybírat	k5eAaImF
odborníci	odborník	k1gMnPc1
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Karel	Karel	k1gMnSc1
Oliva	Oliva	k1gMnSc1
Host	host	k1gMnSc1
Lucie	Lucie	k1gFnSc1
Výborné	výborná	k1gFnSc2
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mše	mše	k1gFnSc1
svatá	svatá	k1gFnSc1
není	být	k5eNaImIp3nS
posezení	posezení	k1gNnSc4
v	v	k7c6
hospodě	hospodě	k?
(	(	kIx(
<g/>
Katolický	katolický	k2eAgInSc1d1
týdeník	týdeník	k1gInSc1
36	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pna	pnout	k5eAaImSgInS
<g/>
2012711524	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
254000705	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Jazyk	jazyk	k1gInSc1
</s>
