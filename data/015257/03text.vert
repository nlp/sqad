<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
malém	malý	k2eAgInSc6d1
fotbalu	fotbal	k1gInSc6
2017	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
malém	malý	k2eAgInSc6d1
fotbalu	fotbal	k1gInSc6
2017	#num#	k4
EMF	EMF	kA
EURO	euro	k1gNnSc1
2017	#num#	k4
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1
turnaje	turnaj	k1gInSc2
</s>
<s>
Pořadatel	pořadatel	k1gMnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
</s>
<s>
Datum	datum	k1gNnSc1
konání	konání	k1gNnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2017	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
týmů	tým	k1gInPc2
</s>
<s>
24	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
stadionů	stadion	k1gInPc2
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
1	#num#	k4
městě	město	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Statistika	statistika	k1gFnSc1
turnaje	turnaj	k1gInSc2
</s>
<s>
Počet	počet	k1gInSc1
zápasů	zápas	k1gInPc2
</s>
<s>
52	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
gólů	gól	k1gInPc2
</s>
<s>
178	#num#	k4
(	(	kIx(
<g/>
3,42	3,42	k4
na	na	k7c4
zápas	zápas	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
(	(	kIx(
<g/>
střelci	střelec	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Smok	smok	k1gMnSc1
Patrik	Patrik	k1gMnSc1
Levčík	Levčík	k1gMnSc1
Michal	Michal	k1gMnSc1
Salák	Salák	k1gMnSc1
Mirko	Mirko	k1gMnSc1
Klikovac	Klikovac	k1gFnSc1
<g/>
(	(	kIx(
<g/>
všichni	všechen	k3xTgMnPc1
5	#num#	k4
gólů	gól	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
___________________________________________	___________________________________________	k?
</s>
<s>
←	←	k?
2016	#num#	k4
</s>
<s>
2018	#num#	k4
→	→	k?
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
malém	malý	k2eAgInSc6d1
fotbalu	fotbal	k1gInSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
ročníkem	ročník	k1gInSc7
ME	ME	kA
v	v	k7c6
malém	malý	k2eAgInSc6d1
fotbalu	fotbal	k1gInSc6
a	a	k8xC
konalo	konat	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c6
Česku	Česko	k1gNnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
v	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
9	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastnilo	účastnit	k5eAaImAgNnS
se	se	k3xPyFc4
ho	on	k3xPp3gInSc2
24	#num#	k4
týmů	tým	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
rozděleny	rozdělit	k5eAaPmNgInP
do	do	k7c2
6	#num#	k4
skupin	skupina	k1gFnPc2
po	po	k7c6
4	#num#	k4
týmech	tým	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
skupiny	skupina	k1gFnSc2
pak	pak	k6eAd1
postoupily	postoupit	k5eAaPmAgFnP
do	do	k7c2
vyřazovací	vyřazovací	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
první	první	k4xOgInSc4
a	a	k8xC
druhý	druhý	k4xOgInSc4
celek	celek	k1gInSc4
a	a	k8xC
čtyři	čtyři	k4xCgInPc4
nejlepší	dobrý	k2eAgInPc4d3
celky	celek	k1gInPc4
na	na	k7c6
třetích	třetí	k4xOgNnPc6
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřazovací	vyřazovací	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
16	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
zvítězili	zvítězit	k5eAaPmAgMnP
reprezentanti	reprezentant	k1gMnPc1
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
porazili	porazit	k5eAaPmAgMnP
výběr	výběr	k1gInSc4
Česka	Česko	k1gNnSc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
po	po	k7c6
penaltách	penalta	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
poprvé	poprvé	k6eAd1
tak	tak	k6eAd1
vyhráli	vyhrát	k5eAaPmAgMnP
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
v	v	k7c6
malém	malý	k2eAgInSc6d1
fotbalu	fotbal	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stadion	stadion	k1gInSc1
</s>
<s>
Turnaj	turnaj	k1gInSc1
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgInS
na	na	k7c6
jednom	jeden	k4xCgInSc6
stadionu	stadion	k1gInSc6
v	v	k7c6
jednom	jeden	k4xCgNnSc6
hostitelském	hostitelský	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
:	:	kIx,
Aréna	aréna	k1gFnSc1
Za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
Za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
</s>
<s>
Kapacita	kapacita	k1gFnSc1
<g/>
:	:	kIx,
3	#num#	k4
500	#num#	k4
</s>
<s>
[[	[[	k?
<g/>
file	file	k1gNnPc6
<g/>
:	:	kIx,
<g/>
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
adm	adm	k?
location	location	k1gInSc1
map	mapa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
375	#num#	k4
<g/>
px	px	k?
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
<g/>
|	|	kIx~
<g/>
{{{	{{{	k?
<g/>
alt	alt	k1gInSc1
<g/>
}}}	}}}	k?
<g/>
|	|	kIx~
<g/>
]]	]]	k?
<g/>
Brno	Brno	k1gNnSc1
</s>
<s>
Skupinová	skupinový	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
</s>
<s>
Týmy	tým	k1gInPc4
na	na	k7c6
prvních	první	k4xOgNnPc6
dvou	dva	k4xCgNnPc6
místech	místo	k1gNnPc6
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
osmifinále	osmifinále	k1gNnSc2
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
nejlépe	dobře	k6eAd3
umístěné	umístěný	k2eAgInPc1d1
týmy	tým	k1gInPc1
ze	z	k7c2
třetích	třetí	k4xOgNnPc2
míst	místo	k1gNnPc2
postupují	postupovat	k5eAaImIp3nP
do	do	k7c2
osmifinále	osmifinále	k1gNnSc2
</s>
<s>
Vítěz	vítěz	k1gMnSc1
zápasu	zápas	k1gInSc2
je	být	k5eAaImIp3nS
tučně	tučně	k6eAd1
zvýrazněn	zvýraznit	k5eAaPmNgInS
</s>
<s>
Všechny	všechen	k3xTgInPc1
časy	čas	k1gInPc1
zápasů	zápas	k1gInPc2
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
ve	v	k7c6
středoevropském	středoevropský	k2eAgInSc6d1
letním	letní	k2eAgInSc6d1
čase	čas	k1gInSc6
(	(	kIx(
<g/>
SELČ	SELČ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Skupina	skupina	k1gFnSc1
A	a	k9
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
IG	IG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+8	+8	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+12	+12	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
3	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
0	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Richárd	Richárd	k1gInSc1
Cseszneki	Cseszneki	k1gNnSc1
29	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
4	#num#	k4
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Oleg	Oleg	k1gMnSc1
Kuzmin	Kuzmin	k1gInSc4
19	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
34	#num#	k4
<g/>
'	'	kIx"
<g/>
Sharon	Sharon	k1gMnSc1
Adani	Adaň	k1gMnSc3
24	#num#	k4
<g/>
'	'	kIx"
<g/>
Yehonatan	Yehonatan	k1gInSc4
Zeharia	Zeharium	k1gNnSc2
38	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
6	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Imre	Imre	k1gFnSc1
Bozsoki	Bozsok	k1gFnSc2
10	#num#	k4
<g/>
'	'	kIx"
<g/>
Róbert	Róbert	k1gInSc4
Fekete	Feke	k1gNnSc2
17	#num#	k4
<g/>
'	'	kIx"
<g/>
Szabolcs	Szabolcs	k1gInSc1
Somogyi	Somogyi	k1gNnSc1
22	#num#	k4
<g/>
'	'	kIx"
<g/>
Imre	Imre	k1gFnSc1
Lak	lak	k1gInSc1
33	#num#	k4
<g/>
'	'	kIx"
<g/>
Ferenc	Ferenc	k1gMnSc1
Béres	Béres	k1gMnSc1
35	#num#	k4
<g/>
'	'	kIx"
<g/>
Csaba	Csaba	k1gFnSc1
Poncok	Poncok	k1gInSc1
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
6	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Michal	Michal	k1gMnSc1
Salák	Salák	k1gMnSc1
10	#num#	k4
<g/>
'	'	kIx"
<g/>
Jan	Jan	k1gMnSc1
Koudelka	Koudelka	k1gMnSc1
29	#num#	k4
<g/>
'	'	kIx"
<g/>
Patrik	Patrik	k1gMnSc1
Levčík	Levčík	k1gMnSc1
34	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
37	#num#	k4
<g/>
'	'	kIx"
<g/>
Ondřej	Ondřej	k1gMnSc1
Paděra	Paděra	k1gFnSc1
34	#num#	k4
<g/>
'	'	kIx"
<g/>
Michal	Michal	k1gMnSc1
Uhlíř	Uhlíř	k1gMnSc1
40	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Márkó	Márkó	k?
Sós	sós	k1gInSc1
25	#num#	k4
<g/>
'	'	kIx"
<g/>
Imre	Imre	k1gFnSc1
Lak	lak	k1gInSc1
37	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Andrejs	Andrejs	k1gInSc1
Sitiks	Sitiks	k1gInSc1
41	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
7	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Izrael	Izrael	k1gInSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Michal	Michal	k1gMnSc1
Uhlíř	Uhlíř	k1gMnSc1
3	#num#	k4
<g/>
'	'	kIx"
<g/>
Stanislav	Stanislav	k1gMnSc1
Mařík	Mařík	k1gMnSc1
7	#num#	k4
<g/>
'	'	kIx"
<g/>
Jan	Jan	k1gMnSc1
Koudelka	Koudelka	k1gMnSc1
7	#num#	k4
<g/>
'	'	kIx"
<g/>
Tomáš	Tomáš	k1gMnSc1
Mica	Mica	k1gMnSc1
9	#num#	k4
<g/>
'	'	kIx"
<g/>
Ondřej	Ondřej	k1gMnSc1
Paděra	Paděra	k1gFnSc1
23	#num#	k4
<g/>
'	'	kIx"
<g/>
Michal	Michal	k1gMnSc1
Salák	Salák	k1gMnSc1
28	#num#	k4
<g/>
'	'	kIx"
<g/>
Bohumír	Bohumír	k1gMnSc1
Doubravský	Doubravský	k2eAgMnSc1d1
35	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Skupina	skupina	k1gFnSc1
B	B	kA
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
IG	IG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
+2	+2	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+1	+1	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
3	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Bartłomiej	Bartłomiej	k1gInSc1
Dębicki	Dębicki	k1gNnSc1
16	#num#	k4
<g/>
'	'	kIx"
<g/>
Ariel	Ariel	k1gInSc1
Mnochy	Mnocha	k1gFnSc2
35	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
4	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Joerg	Joerg	k1gMnSc1
Wagner	Wagner	k1gMnSc1
17	#num#	k4
<g/>
'	'	kIx"
<g/>
Dominic	Dominice	k1gFnPc2
Reinold	Reinoldo	k1gNnPc2
29	#num#	k4
<g/>
'	'	kIx"
<g/>
Nikolay	Nikolaa	k1gFnSc2
Petrov	Petrov	k1gInSc1
31	#num#	k4
<g/>
'	'	kIx"
(	(	kIx(
<g/>
vl	vl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Dobrin	Dobrin	k1gInSc1
Mihaylov	Mihaylov	k1gInSc1
14	#num#	k4
<g/>
'	'	kIx"
<g/>
Nikolay	Nikolaa	k1gFnSc2
Petrov	Petrov	k1gInSc1
18	#num#	k4
<g/>
'	'	kIx"
<g/>
Martin	Martin	k1gMnSc1
Kostov	Kostov	k1gInSc4
19	#num#	k4
<g/>
'	'	kIx"
<g/>
Nedelyan	Nedelyana	k1gFnPc2
Kostadinov	Kostadinovo	k1gNnPc2
41	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Ariel	Ariel	k1gInSc1
Mnochy	Mnocha	k1gFnSc2
15	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Georgi	Georgi	k6eAd1
Nedyalkov	Nedyalkov	k1gInSc1
24	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Gerbi	Gerbi	k6eAd1
Kaplan	Kaplan	k1gMnSc1
40	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Artem	Artem	k6eAd1
Gordienko	Gordienka	k1gFnSc5
37	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
41	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Florian	Florian	k1gMnSc1
Thamm	Thamm	k1gMnSc1
37	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Aleksey	Aleksey	k1gInPc1
Stepanov	Stepanov	k1gInSc1
15	#num#	k4
<g/>
'	'	kIx"
<g/>
Dobrin	Dobrin	k1gInSc1
Mihaylov	Mihaylov	k1gInSc1
21	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Pavlo	Pavla	k1gFnSc5
Bovtunenko	Bovtunenka	k1gFnSc5
26	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Skupina	skupina	k1gFnSc1
C	C	kA
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
IG	IG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+3	+3	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
+4	+4	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
3	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
5	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Martin	Martin	k1gMnSc1
Mráz	Mráz	k1gMnSc1
20	#num#	k4
<g/>
'	'	kIx"
<g/>
Erik	Erik	k1gMnSc1
Szabo	Szaba	k1gFnSc5
27	#num#	k4
<g/>
'	'	kIx"
<g/>
Félix	Félix	k1gInSc1
Ľudovít	Ľudovít	k1gFnSc2
31	#num#	k4
<g/>
'	'	kIx"
<g/>
Ivan	Ivan	k1gMnSc1
Buchel	Buchel	k1gMnSc1
35	#num#	k4
<g/>
'	'	kIx"
<g/>
Matej	Matej	k1gInSc1
Vašíček	Vašíček	k1gMnSc1
38	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Ogün	Ogün	k1gInSc1
Bayrak	Bayrak	k1gInSc1
11	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Marcos	Marcos	k1gMnSc1
García	García	k1gMnSc1
Menéndez	Menéndez	k1gMnSc1
1	#num#	k4
<g/>
'	'	kIx"
<g/>
Sergi	Serg	k1gInSc3
Codina	Codin	k2eAgNnSc2d1
Talavera	Talavero	k1gNnSc2
29	#num#	k4
<g/>
'	'	kIx"
<g/>
Pablo	Pablo	k1gNnSc1
García	García	k1gFnSc1
Ardura	Ardura	k1gFnSc1
30	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Mehmet	Mehmet	k1gMnSc1
Kurt	Kurt	k1gMnSc1
23	#num#	k4
<g/>
'	'	kIx"
<g/>
Hakan	Hakana	k1gFnPc2
Gönül	Gönülum	k1gNnPc2
38	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Valerii	Valerie	k1gFnSc4
Likhobabenko	Likhobabenka	k1gFnSc5
14	#num#	k4
<g/>
'	'	kIx"
<g/>
Michail	Michaila	k1gFnPc2
Merkulov	Merkulovo	k1gNnPc2
22	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Silvester	Silvester	k1gInSc1
Jáger	Jáger	k1gInSc1
30	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Aleksandr	Aleksandr	k1gInSc1
Kuksov	Kuksov	k1gInSc1
9	#num#	k4
<g/>
'	'	kIx"
<g/>
Erik	Erik	k1gMnSc1
Korchagin	Korchagin	k1gMnSc1
22	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Félix	Félix	k1gInSc1
Ľudovít	Ľudovít	k1gFnSc2
30	#num#	k4
<g/>
'	'	kIx"
<g/>
Silvester	Silvestra	k1gFnPc2
Jáger	Jágero	k1gNnPc2
35	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Demir	Demir	k1gInSc1
Barı	Barı	k1gFnSc2
6	#num#	k4
<g/>
'	'	kIx"
<g/>
Ogün	Ogün	k1gInSc1
Bayrak	Bayrak	k1gInSc1
28	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Skupina	skupina	k1gFnSc1
D	D	kA
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
IG	IG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+4	+4	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
+2	+2	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
4	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
13	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Murat	Murat	k2eAgInSc1d1
Akhmetsharipov	Akhmetsharipov	k1gInSc1
6	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
37	#num#	k4
<g/>
'	'	kIx"
<g/>
Olzhas	Olzhasa	k1gFnPc2
Taibassarov	Taibassarovo	k1gNnPc2
7	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
4	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
George	George	k6eAd1
Adrian	Adrian	k1gMnSc1
Calugareanu	Calugarean	k1gInSc2
8	#num#	k4
<g/>
'	'	kIx"
<g/>
Claudiu	Claudium	k1gNnSc6
Vlad	Vlado	k1gNnPc2
11	#num#	k4
<g/>
'	'	kIx"
<g/>
Costin	Costin	k2eAgInSc1d1
Cristian	Cristian	k1gInSc1
Pusca	Pusca	k1gFnSc1
16	#num#	k4
<g/>
'	'	kIx"
<g/>
Toma	Tom	k1gMnSc4
Vincene	Vincen	k1gInSc5
37	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Nikolaos	Nikolaos	k1gInSc1
Stamos	Stamos	k1gInSc1
36	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Olzhas	Olzhas	k1gInSc1
Taibassarov	Taibassarov	k1gInSc1
6	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Nikolaos	Nikolaos	k1gInSc1
Stamos	Stamos	k1gInSc1
30	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Toma	Tom	k1gMnSc4
Vincene	Vincen	k1gInSc5
5	#num#	k4
<g/>
'	'	kIx"
<g/>
Claudiu	Claudium	k1gNnSc6
Vlad	Vlado	k1gNnPc2
28	#num#	k4
<g/>
'	'	kIx"
<g/>
Gabriel	Gabriel	k1gMnSc1
Tanase	Tanasa	k1gFnSc6
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Azamat	Azamat	k1gInSc1
Khassenov	Khassenov	k1gInSc1
9	#num#	k4
<g/>
'	'	kIx"
<g/>
Nurzhan	Nurzhana	k1gFnPc2
Abdrassilov	Abdrassilovo	k1gNnPc2
17	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
22	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Nikolaos	Nikolaos	k1gInSc1
Stamos	Stamos	k1gInSc1
41	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
41	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Joã	Joã	k1gFnSc5
Paulo	Paula	k1gFnSc5
30	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Skupina	skupina	k1gFnSc1
E	E	kA
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
IG	IG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+6	+6	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
+2	+2	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
0	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
4	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Marko	Marko	k1gMnSc1
Vujica	Vujic	k1gInSc2
13	#num#	k4
<g/>
'	'	kIx"
<g/>
Robert	Roberta	k1gFnPc2
Đotlo	Đotlo	k1gNnSc1
16	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
39	#num#	k4
<g/>
'	'	kIx"
<g/>
Marin	Marina	k1gFnPc2
Jukić	Jukić	k1gFnSc4
34	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Mirko	Mirko	k1gMnSc1
Klikovac	Klikovac	k1gInSc4
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Dragan	Dragan	k1gMnSc1
Mitrović	Mitrović	k1gMnSc1
16	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Mirko	Mirko	k1gMnSc1
Klikovac	Klikovac	k1gInSc4
22	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
34	#num#	k4
<g/>
'	'	kIx"
<g/>
Saša	Saša	k1gFnSc1
Vukčević	Vukčević	k1gFnSc1
30	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Saverio	Saverio	k1gMnSc1
Mastrojanni	Mastrojanň	k1gMnSc3
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Goran	Goran	k1gInSc1
Lovrinović	Lovrinović	k1gFnSc2
17	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
40	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
3	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Tomašević	Tomašević	k1gMnSc1
10	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
22	#num#	k4
<g/>
'	'	kIx"
<g/>
Mirko	Mirko	k1gMnSc1
Klikovac	Klikovac	k1gInSc4
33	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Mirza	Mirza	k1gFnSc1
Zahirović	Zahirović	k1gFnSc1
31	#num#	k4
<g/>
'	'	kIx"
<g/>
Goran	Goran	k1gInSc1
Lovrinović	Lovrinović	k1gFnSc2
35	#num#	k4
<g/>
'	'	kIx"
<g/>
Andreas	Andreas	k1gInSc1
Berović	Berović	k1gFnSc2
36	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Itálie	Itálie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Veselin	Veselin	k2eAgMnSc1d1
Guberinić	Guberinić	k1gMnSc1
10	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
20	#num#	k4
<g/>
'	'	kIx"
<g/>
Dejan	Dejan	k1gInSc1
Jevtić	Jevtić	k1gFnSc2
35	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Massimiliano	Massimiliana	k1gFnSc5
Berardini	Berardin	k2eAgMnPc1d1
12	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Skupina	skupina	k1gFnSc1
F	F	kA
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
IG	IG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+3	+3	k4
</s>
<s>
9	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+11	+11	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+9	+9	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
0	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
12	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Dejan	Dejan	k1gMnSc1
Kos	Kos	k1gMnSc1
6	#num#	k4
<g/>
'	'	kIx"
<g/>
Amar	Amar	k1gInSc1
Jamakovič	Jamakovič	k1gInSc1
15	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Alix	Alix	k1gInSc1
Gilsoul	Gilsoul	k1gInSc1
13	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Josip	Josip	k1gInSc1
Gusković	Gusković	k1gFnSc2
23	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Zoran	Zoran	k1gInSc1
Lukavečki	Lukavečki	k1gNnSc1
22	#num#	k4
<g/>
'	'	kIx"
(	(	kIx(
<g/>
vl	vl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
Christophe	Christophe	k1gFnSc1
Perrin	Perrin	k1gInSc1
35	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
David	David	k1gMnSc1
Šajnovič	Šajnovič	k1gMnSc1
21	#num#	k4
<g/>
'	'	kIx"
<g/>
Mak	mako	k1gNnPc2
Karabegovič	Karabegovič	k1gInSc1
32	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Omar	Omar	k1gMnSc1
Belbachir	Belbachir	k1gMnSc1
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
11	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Antun	Antun	k1gInSc1
Herzcigonja	Herzcigonja	k1gFnSc1
10	#num#	k4
<g/>
'	'	kIx"
<g/>
Mario	Mario	k1gMnSc1
Orlović	Orlović	k1gMnSc1
11	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
12	#num#	k4
<g/>
'	'	kIx"
<g/>
Ivan	Ivan	k1gMnSc1
Smok	smok	k1gMnSc1
14	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
17	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
39	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
40	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
40	#num#	k4
<g/>
'	'	kIx"
<g/>
Josip	Josip	k1gInSc1
Gusković	Gusković	k1gFnSc2
17	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
29	#num#	k4
<g/>
'	'	kIx"
<g/>
Zoran	Zoran	k1gInSc1
Lukavečki	Lukavečki	k1gNnSc1
25	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
11	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Belgie	Belgie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Nicolas	Nicolas	k1gInSc1
Huertos	Huertos	k1gInSc1
3	#num#	k4
<g/>
'	'	kIx"
<g/>
Labaig	Labaig	k1gMnSc1
Romain	Romain	k1gMnSc1
6	#num#	k4
<g/>
'	'	kIx"
<g/>
Bruno	Bruno	k1gMnSc1
Meilhon	Meilhon	k1gMnSc1
8	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
31	#num#	k4
<g/>
'	'	kIx"
<g/>
Jerome	Jerom	k1gInSc5
Boulin	Boulin	k1gInSc1
12	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
35	#num#	k4
<g/>
'	'	kIx"
15	#num#	k4
<g/>
'	'	kIx"
(	(	kIx(
<g/>
vl	vl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
Elie	Elie	k1gFnSc1
Dohin	Dohin	k1gInSc1
18	#num#	k4
<g/>
'	'	kIx"
<g/>
Christophe	Christophe	k1gFnSc1
Perrin	Perrin	k1gInSc1
25	#num#	k4
<g/>
'	'	kIx"
<g/>
Mickael	Mickael	k1gInSc1
Senac	Senac	k1gInSc1
26	#num#	k4
<g/>
'	'	kIx"
<g/>
Nicolas	Nicolas	k1gInSc1
Le	Le	k1gFnSc2
27	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Amar	Amar	k1gInSc1
Jamakovič	Jamakovič	k1gInSc1
34	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Žebříček	žebříček	k1gInSc1
týmů	tým	k1gInPc2
na	na	k7c6
třetích	třetí	k4xOgNnPc6
místech	místo	k1gNnPc6
</s>
<s>
Poz	Poz	k?
<g/>
.	.	kIx.
</s>
<s>
Sk	Sk	kA
<g/>
.	.	kIx.
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
IG	IG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
C	C	kA
Turecko	Turecko	k1gNnSc1
</s>
<s>
1115504	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
E	E	kA
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
1114404	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
D	D	kA
Řecko	Řecko	k1gNnSc1
</s>
<s>
11146	#num#	k4
<g/>
−	−	k?
<g/>
24	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
F	F	kA
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
10212393	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
B	B	kA
Ukrajina	Ukrajina	k1gFnSc1
</s>
<s>
10235	#num#	k4
<g/>
−	−	k?
<g/>
23	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
A	a	k8xC
Izrael	Izrael	k1gInSc1
</s>
<s>
102413	#num#	k4
<g/>
−	−	k?
<g/>
93	#num#	k4
</s>
<s>
Vyřazovací	vyřazovací	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
</s>
<s>
Pavouk	pavouk	k1gMnSc1
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
(	(	kIx(
<g/>
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
(	(	kIx(
<g/>
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
17	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
22	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
O	o	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
18	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
(	(	kIx(
<g/>
pen	pen	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
0	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
20	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
0	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
Osmifinále	osmifinále	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Zsolt	Zsolt	k2eAgMnSc1d1
Szabó	Szabó	k1gMnSc1
12	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Ariel	Ariel	k1gInSc1
Mnochy	Mnocha	k1gFnSc2
31	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
40	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Mirko	Mirko	k1gMnSc1
Klikovac	Klikovac	k1gInSc4
36	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
16	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
2	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Řecko	Řecko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Dejan	Dejan	k1gMnSc1
Kos	Kos	k1gMnSc1
3	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
19	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Dimitrios	Dimitrios	k1gInSc1
Gkialis	Gkialis	k1gFnSc2
2	#num#	k4
<g/>
'	'	kIx"
<g/>
Stefanos	Stefanos	k1gInSc1
Giotis	Giotis	k1gFnSc2
30	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Penaltový	penaltový	k2eAgInSc1d1
rozstřel	rozstřel	k1gInSc1
</s>
<s>
Semin	Semin	k2eAgMnSc1d1
Omerovič	Omerovič	k1gMnSc1
Amar	Amar	k1gMnSc1
Jamakovič	Jamakovič	k1gMnSc1
Nejc	Nejc	k1gFnSc4
Rober	Robra	k1gFnPc2
</s>
<s>
3	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Dionisios	Dionisios	k1gMnSc1
Tzanetos	Tzanetos	k1gMnSc1
Dimitrios	Dimitrios	k1gMnSc1
Gkialis	Gkialis	k1gFnSc2
Chrysanthos	Chrysanthos	k1gMnSc1
Lefkaritis	Lefkaritis	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Anton	Anton	k1gMnSc1
Kulagin	Kulagin	k1gMnSc1
33	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Kassim	Kassim	k1gInSc1
Boziyev	Boziyev	k1gFnSc1
22	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Penaltový	penaltový	k2eAgInSc1d1
rozstřel	rozstřel	k1gInSc1
</s>
<s>
Evgenii	Evgenie	k1gFnSc4
Vashurin	Vashurin	k1gInSc1
Erik	Erik	k1gMnSc1
Korchagin	Korchagin	k1gMnSc1
Valerii	Valerie	k1gFnSc4
Likhobabenko	Likhobabenka	k1gFnSc5
</s>
<s>
3	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Murat	Murat	k2eAgInSc1d1
Akhmetsharipov	Akhmetsharipov	k1gInSc1
Slyuimen	Slyuimen	k2eAgInSc1d1
Anvar	Anvar	k1gInSc1
Nurzhan	Nurzhany	k1gInPc2
Abdrassilov	Abdrassilov	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
4	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
George	George	k6eAd1
Adrian	Adrian	k1gMnSc1
Calugareanu	Calugarean	k1gInSc2
14	#num#	k4
<g/>
'	'	kIx"
<g/>
Claudiu	Claudium	k1gNnSc6
Vlad	Vlado	k1gNnPc2
15	#num#	k4
<g/>
'	'	kIx"
<g/>
Radu	rada	k1gFnSc4
Marian	Mariana	k1gFnPc2
Burciu	Burcium	k1gNnSc3
19	#num#	k4
<g/>
'	'	kIx"
<g/>
Bogdan	Bogdan	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Bobe	Bob	k1gMnSc5
40	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Veselin	Veselin	k2eAgMnSc1d1
Guberinić	Guberinić	k1gMnSc1
22	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Patrik	Patrik	k1gMnSc1
Levčík	Levčík	k1gMnSc1
33	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Erik	Erik	k1gMnSc1
Szabo	Szaba	k1gFnSc5
42	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Omar	Omar	k1gMnSc1
Belbachir	Belbachir	k1gMnSc1
17	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
18	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
22	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Marin	Marina	k1gFnPc2
Jukić	Jukić	k1gFnSc4
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Ferenc	Ferenc	k1gMnSc1
Béres	Béres	k1gMnSc1
38	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Cezary	Cezar	k1gInPc1
Szałek	Szałek	k6eAd1
8	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Penaltový	penaltový	k2eAgInSc1d1
rozstřel	rozstřel	k1gInSc1
</s>
<s>
András	András	k1gInSc1
Gál	Gál	k1gFnSc2
Márkó	Márkó	k1gFnPc2
Sós	sós	k1gInSc1
Róbert	Róbert	k1gMnSc1
Fekete	Feke	k1gNnSc2
Ferenc	Ferenc	k1gMnSc1
Béres	Béres	k1gMnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Mateusz	Mateusz	k1gMnSc1
Gliński	Gliński	k1gNnSc2
Krysztof	Krysztof	k1gMnSc1
Elsner	Elsner	k1gMnSc1
Grzywa	Grzywa	k1gMnSc1
Marcin	Marcin	k1gMnSc1
Grzegorz	Grzegorz	k1gMnSc1
Och	och	k0
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
4	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
František	František	k1gMnSc1
Hakl	Hakl	k1gMnSc1
13	#num#	k4
<g/>
'	'	kIx"
<g/>
Michal	Michal	k1gMnSc1
Salák	Salák	k1gMnSc1
30	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
31	#num#	k4
<g/>
'	'	kIx"
<g/>
Jan	Jan	k1gMnSc1
Koudelka	Koudelka	k1gMnSc1
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Popa	pop	k1gMnSc2
Ioan	Ioan	k1gMnSc1
Mircea	Mircea	k1gMnSc1
7	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Sergey	Sergey	k1gInPc1
Faustov	Faustov	k1gInSc1
31	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
3	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Aleksei	Alekse	k1gFnSc3
Medvedev	Medvedev	k1gFnSc1
17	#num#	k4
<g/>
'	'	kIx"
<g/>
Valerii	Valerie	k1gFnSc6
Likhobabenko	Likhobabenka	k1gFnSc5
30	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
41	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
21	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
3	#num#	k4
:	:	kIx,
4	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Claudiu	Claudium	k1gNnSc3
Vlad	Vlad	k1gInSc1
20	#num#	k4
<g/>
'	'	kIx"
<g/>
Radu	rada	k1gFnSc4
Marian	Mariana	k1gFnPc2
Burciu	Burcium	k1gNnSc3
20	#num#	k4
<g/>
'	'	kIx"
<g/>
Stefan	Stefan	k1gMnSc1
Leu	Leo	k1gMnSc3
44	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
David	David	k1gMnSc1
Bednář	Bednář	k1gMnSc1
12	#num#	k4
<g/>
'	'	kIx"
<g/>
Patrik	Patrik	k1gMnSc1
Levčík	Levčík	k1gMnSc1
21	#num#	k4
<g/>
'	'	kIx"
<g/>
,	,	kIx,
31	#num#	k4
<g/>
'	'	kIx"
<g/>
Michal	Michal	k1gMnSc1
Salák	Salák	k1gMnSc1
39	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
O	o	k7c6
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
18	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
0	#num#	k4
:	:	kIx,
0	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Penaltový	penaltový	k2eAgInSc1d1
rozstřel	rozstřel	k1gInSc1
</s>
<s>
Ferenc	Ferenc	k1gMnSc1
Béres	Béres	k1gMnSc1
Márkó	Márkó	k1gFnPc2
Sós	sós	k1gInSc1
Róbert	Róbert	k1gMnSc1
Fekete	Feket	k1gInSc5
</s>
<s>
3	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Vlad	Vlad	k1gInSc4
Claudiu	Claudium	k1gNnSc3
Gabriel	Gabriela	k1gFnPc2
Tanase	Tanas	k1gInSc6
Popa	pop	k1gMnSc2
Ioan	Ioan	k1gMnSc1
Mircea	Mircea	k1gMnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
1	#num#	k4
:	:	kIx,
1	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Aréna	aréna	k1gFnSc1
za	za	k7c7
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
</s>
<s>
Aleksei	Alekse	k1gFnSc3
Medvedev	Medvedev	k1gFnSc1
24	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Mařík	Mařík	k1gMnSc1
16	#num#	k4
<g/>
'	'	kIx"
</s>
<s>
Penaltový	penaltový	k2eAgInSc1d1
rozstřel	rozstřel	k1gInSc1
</s>
<s>
Evgenii	Evgenie	k1gFnSc4
Vashurin	Vashurin	k1gInSc1
Erik	Erik	k1gMnSc1
Korchagin	Korchagin	k1gMnSc1
Valerii	Valerie	k1gFnSc4
Likhobabenko	Likhobabenka	k1gFnSc5
</s>
<s>
3	#num#	k4
:	:	kIx,
2	#num#	k4
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Mařík	Mařík	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Mica	Mica	k1gMnSc1
Jan	Jan	k1gMnSc1
Koudelka	Koudelka	k1gMnSc1
</s>
<s>
Statistiky	statistika	k1gFnPc1
hráčů	hráč	k1gMnPc2
</s>
<s>
Střelci	Střelec	k1gMnPc1
</s>
<s>
Přehled	přehled	k1gInSc1
střelců	střelec	k1gMnPc2
šampionátu	šampionát	k1gInSc2
</s>
<s>
5	#num#	k4
gólů	gól	k1gInPc2
</s>
<s>
Ivan	Ivan	k1gMnSc1
Smok	smok	k1gMnSc1
</s>
<s>
Patrik	Patrik	k1gMnSc1
Levčík	Levčík	k1gMnSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Salák	Salák	k1gMnSc1
</s>
<s>
Mirko	Mirko	k1gMnSc1
Klikovac	Klikovac	k1gInSc4
</s>
<s>
Žluté	žlutý	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Přehled	přehled	k1gInSc1
hráčů	hráč	k1gMnPc2
alespoň	alespoň	k9
jednou	jednou	k6eAd1
potrestaných	potrestaný	k2eAgInPc2d1
žlutou	žlutý	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
</s>
<s>
3	#num#	k4
žluté	žlutý	k2eAgFnPc4d1
karty	karta	k1gFnPc4
</s>
<s>
András	András	k1gInSc1
Gál	Gál	k1gFnSc2
</s>
<s>
George	George	k1gInSc1
Adrian	Adriana	k1gFnPc2
Calugareanu	Calugarean	k1gInSc2
</s>
<s>
Claudiu	Claudium	k1gNnSc3
Vlad	Vlado	k1gNnPc2
</s>
<s>
İ	İ	k1gFnSc1
Demir	Demira	k1gFnPc2
</s>
<s>
Červené	Červené	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Přehled	přehled	k1gInSc1
hráčů	hráč	k1gMnPc2
nejméně	málo	k6eAd3
jednou	jednou	k6eAd1
potrestaných	potrestaný	k2eAgFnPc2d1
červenou	červený	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
(	(	kIx(
<g/>
přímo	přímo	k6eAd1
i	i	k8xC
po	po	k7c6
dvou	dva	k4xCgFnPc6
žlutých	žlutý	k2eAgFnPc6d1
kartách	karta	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
červená	červený	k2eAgFnSc1d1
karta	karta	k1gFnSc1
</s>
<s>
Petar	Petar	k1gInSc1
Zlatinov	Zlatinov	k1gInSc1
</s>
<s>
Antun	Antun	k1gMnSc1
Herzcigonja	Herzcigonja	k1gMnSc1
</s>
<s>
Pablo	Pablo	k1gNnSc1
Ardura	Ardur	k1gMnSc2
García	Garcíus	k1gMnSc2
</s>
<s>
Díaz	Díaz	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Somovilla	Somovilla	k1gMnSc1
</s>
<s>
Wesley	Weslea	k1gFnPc1
Liade	Liad	k1gInSc5
</s>
<s>
Daniyar	Daniyar	k1gInSc1
Kenzhegulov	Kenzhegulov	k1gInSc1
</s>
<s>
José	José	k6eAd1
Carlos	Carlos	k1gMnSc1
Ferreira	Ferreira	k1gMnSc1
</s>
<s>
Bogdan	Bogdan	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
Bobe	Bob	k1gMnSc5
</s>
<s>
Patrik	Patrik	k1gMnSc1
Juhoš	Juhoš	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
Szabo	Szaba	k1gMnSc5
</s>
<s>
Ismir	Ismir	k1gMnSc1
Berbič	Berbič	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
WWW.PETRKNOP.CZ	WWW.PETRKNOP.CZ	k1gMnSc1
<g/>
,	,	kIx,
HTML	HTML	kA
&	&	k?
CSS	CSS	kA
code-Antonín	code-Antonín	k1gMnSc1
Bureš-	Bureš-	k1gMnSc1
www	www	k?
antoninbures	antoninbures	k1gMnSc1
cz	cz	k?
<g/>
,	,	kIx,
JS	JS	kA
&	&	k?
PHP-	PHP-	k1gMnSc1
Petr	Petr	k1gMnSc1
Knop-	Knop-	k1gMnSc1
<g/>
.	.	kIx.
https://www.minifootball.eu/emf-euro-2017/	https://www.minifootball.eu/emf-euro-2017/	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
І	І	k?
ч	ч	k?
Є	Є	k?
з	з	k?
м	м	k?
<g/>
:	:	kIx,
у	у	k?
т	т	k?
п	п	k?
<g/>
.	.	kIx.
С	С	k?
<g/>
.	.	kIx.
<g/>
UA	UA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Brno	Brno	k1gNnSc1
hostí	hostit	k5eAaImIp3nS
mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
v	v	k7c6
malém	malý	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
<g/>
,	,	kIx,
Češi	Čech	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
doma	doma	k6eAd1
zlato	zlato	k1gNnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-06-08	2017-06-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čeští	český	k2eAgMnPc1d1
reprezentanti	reprezentant	k1gMnPc1
deklasovali	deklasovat	k5eAaBmAgMnP
na	na	k7c4
ME	ME	kA
v	v	k7c6
malé	malý	k2eAgFnSc6d1
kopané	kopaná	k1gFnSc6
Izrael	Izrael	k1gInSc4
a	a	k8xC
dále	daleko	k6eAd2
narazí	narazit	k5eAaPmIp3nP
na	na	k7c4
Bulhary	Bulhar	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-06-13	2017-06-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
malém	malý	k2eAgInSc6d1
fotbalu	fotbal	k1gInSc6
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
2010	#num#	k4
•	•	k?
Tulcea	Tulcea	k1gMnSc1
2011	#num#	k4
•	•	k?
Kišiněv	Kišiněv	k1gFnPc2
2012	#num#	k4
•	•	k?
Rethymno	Rethymno	k1gNnSc1
2013	#num#	k4
•	•	k?
Herceg	Herceg	k1gInSc1
Novi	Novi	k1gNnSc1
2014	#num#	k4
•	•	k?
Vrsar	Vrsar	k1gInSc1
2015	#num#	k4
•	•	k?
Székesfehérvár	Székesfehérvár	k1gInSc1
2016	#num#	k4
•	•	k?
Brno	Brno	k1gNnSc1
2017	#num#	k4
•	•	k?
Kyjev	Kyjev	k1gInSc1
2018	#num#	k4
•	•	k?
Košice	Košice	k1gInPc1
2020	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
|	|	kIx~
Evropa	Evropa	k1gFnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
