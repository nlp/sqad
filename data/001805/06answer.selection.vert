<s>
Své	svůj	k3xOyFgNnSc4	svůj
dnešní	dnešní	k2eAgNnSc4d1	dnešní
jméno	jméno	k1gNnSc4	jméno
Suomenlinna	Suomenlinn	k1gInSc2	Suomenlinn
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Finský	finský	k2eAgInSc1d1	finský
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
dostala	dostat	k5eAaPmAgFnS	dostat
pevnost	pevnost	k1gFnSc1	pevnost
až	až	k9	až
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
však	však	k9	však
lze	lze	k6eAd1	lze
i	i	k9	i
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
švédským	švédský	k2eAgInSc7d1	švédský
názvem	název	k1gInSc7	název
Sveaborg	Sveaborg	k1gInSc1	Sveaborg
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Švédský	švédský	k2eAgInSc1d1	švédský
hrad	hrad	k1gInSc1	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
