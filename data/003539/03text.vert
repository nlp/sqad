<s>
Světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
část	část	k1gFnSc1	část
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
frekvence	frekvence	k1gFnSc1	frekvence
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
3,9	[number]	k4	3,9
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
hertz	hertz	k1gInSc1	hertz
<g/>
)	)	kIx)	)
do	do	k7c2	do
7,9	[number]	k4	7,9
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
390	[number]	k4	390
<g/>
–	–	k?	–
<g/>
760	[number]	k4	760
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
vlnovými	vlnový	k2eAgFnPc7d1	vlnová
délkami	délka	k1gFnPc7	délka
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
světlem	světlo	k1gNnSc7	světlo
chápáno	chápat	k5eAaImNgNnS	chápat
i	i	k9	i
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
širšího	široký	k2eAgInSc2d2	širší
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
zasahujícího	zasahující	k2eAgInSc2d1	zasahující
do	do	k7c2	do
infračervené	infračervený	k2eAgFnSc2d1	infračervená
a	a	k8xC	a
ultrafialové	ultrafialový	k2eAgFnSc2d1	ultrafialová
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
pomocí	pomocí	k7c2	pomocí
několika	několik	k4yIc2	několik
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejzákladnější	základní	k2eAgNnSc4d3	nejzákladnější
patří	patřit	k5eAaImIp3nS	patřit
fotometrické	fotometrický	k2eAgFnPc4d1	fotometrická
charakteristiky	charakteristika	k1gFnPc4	charakteristika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
svítivost	svítivost	k1gFnSc4	svítivost
či	či	k8xC	či
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolorimetrické	kolorimetrický	k2eAgNnSc4d1	kolorimetrické
(	(	kIx(	(
<g/>
frekvenční	frekvenční	k2eAgNnSc4d1	frekvenční
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koherence	koherence	k1gFnSc1	koherence
a	a	k8xC	a
polarizace	polarizace	k1gFnSc1	polarizace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
pak	pak	k6eAd1	pak
závisí	záviset	k5eAaImIp3nS	záviset
i	i	k9	i
chování	chování	k1gNnSc1	chování
při	při	k7c6	při
odrazu	odraz	k1gInSc6	odraz
<g/>
,	,	kIx,	,
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
průchodu	průchod	k1gInSc2	průchod
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
při	při	k7c6	při
skládání	skládání	k1gNnSc6	skládání
a	a	k8xC	a
ohybu	ohyb	k1gInSc6	ohyb
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
dualitě	dualita	k1gFnSc3	dualita
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
vlnění	vlnění	k1gNnSc2	vlnění
má	mít	k5eAaImIp3nS	mít
světlo	světlo	k1gNnSc4	světlo
vlastnosti	vlastnost	k1gFnSc2	vlastnost
jak	jak	k8xC	jak
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc7	jeho
interakcemi	interakce	k1gFnPc7	interakce
s	s	k7c7	s
hmotou	hmota	k1gFnSc7	hmota
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
optika	optika	k1gFnSc1	optika
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vnímat	vnímat	k5eAaImF	vnímat
část	část	k1gFnSc4	část
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
spektra	spektrum	k1gNnSc2	spektrum
z	z	k7c2	z
rozsahu	rozsah	k1gInSc2	rozsah
frekvencí	frekvence	k1gFnSc7	frekvence
přibližně	přibližně	k6eAd1	přibližně
3,9	[number]	k4	3,9
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
hertz	hertz	k1gInSc1	hertz
<g/>
)	)	kIx)	)
až	až	k9	až
7,9	[number]	k4	7,9
<g/>
×	×	k?	×
<g/>
1014	[number]	k4	1014
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vlnovým	vlnový	k2eAgFnPc3d1	vlnová
délkám	délka	k1gFnPc3	délka
z	z	k7c2	z
rozsahu	rozsah	k1gInSc2	rozsah
přibližně	přibližně	k6eAd1	přibližně
390	[number]	k4	390
<g/>
–	–	k?	–
<g/>
760	[number]	k4	760
nm	nm	k?	nm
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
fázovou	fázový	k2eAgFnSc4d1	fázová
rychlost	rychlost	k1gFnSc4	rychlost
(	(	kIx(	(
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
frekvenci	frekvence	k1gFnSc4	frekvence
(	(	kIx(	(
<g/>
f	f	k?	f
)	)	kIx)	)
a	a	k8xC	a
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
(	(	kIx(	(
<g/>
λ	λ	k?	λ
<g/>
)	)	kIx)	)
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
=	=	kIx~	=
f	f	k?	f
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
rozsah	rozsah	k1gInSc1	rozsah
je	být	k5eAaImIp3nS	být
viditelným	viditelný	k2eAgNnSc7d1	viditelné
světlem	světlo	k1gNnSc7	světlo
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
živočichů	živočich	k1gMnPc2	živočich
vnímají	vnímat	k5eAaImIp3nP	vnímat
rozsah	rozsah	k1gInSc4	rozsah
jiný	jiný	k2eAgInSc4d1	jiný
-	-	kIx~	-
například	například	k6eAd1	například
včely	včela	k1gFnSc2	včela
jej	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
posunut	posunut	k2eAgInSc4d1	posunut
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
kratším	krátký	k2eAgFnPc3d2	kratší
vlnovým	vlnový	k2eAgFnPc3d1	vlnová
délkám	délka	k1gFnPc3	délka
(	(	kIx(	(
<g/>
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
někteří	některý	k3yIgMnPc1	některý
plazi	plaz	k1gMnPc1	plaz
vnímají	vnímat	k5eAaImIp3nP	vnímat
i	i	k9	i
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
vnímaných	vnímaný	k2eAgFnPc2d1	vnímaná
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgMnS	dát
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
není	být	k5eNaImIp3nS	být
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
ze	z	k7c2	z
Slunce	slunce	k1gNnSc2	slunce
absorbováno	absorbovat	k5eAaBmNgNnS	absorbovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
využitelné	využitelný	k2eAgNnSc1d1	využitelné
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
žijící	žijící	k2eAgInPc4d1	žijící
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pro	pro	k7c4	pro
zrakové	zrakový	k2eAgNnSc4d1	zrakové
vnímání	vnímání	k1gNnSc4	vnímání
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
rozprostraněnosti	rozprostraněnost	k1gFnSc2	rozprostraněnost
<g/>
.	.	kIx.	.
</s>
<s>
Evolucí	evoluce	k1gFnSc7	evoluce
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
příslušné	příslušný	k2eAgInPc4d1	příslušný
světločivné	světločivný	k2eAgInPc4d1	světločivný
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
je	být	k5eAaImIp3nS	být
i	i	k9	i
sítnice	sítnice	k1gFnSc1	sítnice
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nastavené	nastavený	k2eAgNnSc1d1	nastavené
<g/>
"	"	kIx"	"
právě	právě	k9	právě
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
rozsah	rozsah	k1gInSc4	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
potřebě	potřeba	k1gFnSc3	potřeba
objektivního	objektivní	k2eAgNnSc2d1	objektivní
kvantitativního	kvantitativní	k2eAgNnSc2d1	kvantitativní
vymezení	vymezení	k1gNnSc2	vymezení
viditelných	viditelný	k2eAgInPc2d1	viditelný
projevů	projev	k1gInPc2	projev
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
byla	být	k5eAaImAgFnS	být
vedle	vedle	k7c2	vedle
sady	sada	k1gFnSc2	sada
univerzálních	univerzální	k2eAgFnPc2d1	univerzální
radiometrických	radiometrický	k2eAgFnPc2d1	radiometrická
veličin	veličina	k1gFnPc2	veličina
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
<g/>
)	)	kIx)	)
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
sada	sada	k1gFnSc1	sada
jednoznačně	jednoznačně	k6eAd1	jednoznačně
definovaných	definovaný	k2eAgFnPc2d1	definovaná
veličin	veličina	k1gFnPc2	veličina
fotometrických	fotometrický	k2eAgFnPc2d1	fotometrická
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
světlo	světlo	k1gNnSc4	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgFnPc1d1	odpovídající
veličiny	veličina	k1gFnPc1	veličina
obou	dva	k4xCgFnPc2	dva
sad	sada	k1gFnPc2	sada
spolu	spolu	k6eAd1	spolu
souvisejí	souviset	k5eAaImIp3nP	souviset
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
převoditelné	převoditelný	k2eAgFnSc2d1	převoditelná
pomocí	pomoc	k1gFnSc7	pomoc
tzv.	tzv.	kA	tzv.
světelné	světelný	k2eAgFnSc2d1	světelná
účinnosti	účinnost	k1gFnSc2	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
průměrnému	průměrný	k2eAgInSc3d1	průměrný
lidskému	lidský	k2eAgInSc3d1	lidský
vnímání	vnímání	k1gNnSc6	vnímání
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Povahu	povaha	k1gFnSc4	povaha
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
vědci	vědec	k1gMnPc1	vědec
vystihnout	vystihnout	k5eAaPmF	vystihnout
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Platon	Platon	k1gMnSc1	Platon
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidské	lidský	k2eAgNnSc1d1	lidské
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
aktivními	aktivní	k2eAgInPc7d1	aktivní
zdroji	zdroj	k1gInPc7	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pojetí	pojetí	k1gNnSc1	pojetí
optiky	optika	k1gFnSc2	optika
bylo	být	k5eAaImAgNnS	být
přesně	přesně	k6eAd1	přesně
inverzní	inverzní	k2eAgMnSc1d1	inverzní
k	k	k7c3	k
dnešní	dnešní	k2eAgFnSc3d1	dnešní
paprskové	paprskový	k2eAgFnSc3d1	paprsková
optice	optika	k1gFnSc3	optika
(	(	kIx(	(
<g/>
stejné	stejný	k2eAgInPc4d1	stejný
paprsky	paprsek	k1gInPc4	paprsek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opačný	opačný	k2eAgInSc1d1	opačný
směr	směr	k1gInSc1	směr
pohybu	pohyb	k1gInSc2	pohyb
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
fyziků	fyzik	k1gMnPc2	fyzik
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
byl	být	k5eAaImAgMnS	být
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chápal	chápat	k5eAaImAgMnS	chápat
světlo	světlo	k1gNnSc4	světlo
jako	jako	k8xC	jako
proud	proud	k1gInSc1	proud
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
mechanickém	mechanický	k2eAgInSc6d1	mechanický
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
experimentem	experiment	k1gInSc7	experiment
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
od	od	k7c2	od
kolmice	kolmice	k1gFnSc2	kolmice
dopadu	dopad	k1gInSc2	dopad
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
světla	světlo	k1gNnSc2	světlo
z	z	k7c2	z
opticky	opticky	k6eAd1	opticky
řidšího	řídký	k2eAgNnSc2d2	řidší
prostředí	prostředí	k1gNnSc2	prostředí
do	do	k7c2	do
opticky	opticky	k6eAd1	opticky
hustšího	hustý	k2eAgNnSc2d2	hustší
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
vzduch-sklo	vzduchknout	k5eAaPmAgNnS	vzduch-sknout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlnové	vlnový	k2eAgFnPc1d1	vlnová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
světla	světlo	k1gNnSc2	světlo
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
poprvé	poprvé	k6eAd1	poprvé
Christiaan	Christiaan	k1gInSc1	Christiaan
Huygens	Huygensa	k1gFnPc2	Huygensa
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1678	[number]	k4	1678
<g/>
.	.	kIx.	.
</s>
<s>
Vlnová	vlnový	k2eAgFnSc1d1	vlnová
teorie	teorie	k1gFnSc1	teorie
světla	světlo	k1gNnSc2	světlo
dokázala	dokázat	k5eAaPmAgFnS	dokázat
podat	podat	k5eAaPmF	podat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Částicový	částicový	k2eAgInSc1d1	částicový
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
oživen	oživen	k2eAgMnSc1d1	oživen
až	až	k9	až
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
fyzikou	fyzika	k1gFnSc7	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
dualitě	dualita	k1gFnSc6	dualita
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
kvantované	kvantovaný	k2eAgNnSc4d1	kvantované
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lom	lom	k1gInSc1	lom
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Paprsky	paprsek	k1gInPc1	paprsek
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
při	při	k7c6	při
přechodu	přechod	k1gInSc2	přechod
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
prostředí	prostředí	k1gNnSc2	prostředí
do	do	k7c2	do
jiného	jiné	k1gNnSc2	jiné
lámou	lámat	k5eAaImIp3nP	lámat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
když	když	k8xS	když
světlo	světlo	k1gNnSc1	světlo
dopadá	dopadat	k5eAaImIp3nS	dopadat
šikmo	šikmo	k6eAd1	šikmo
na	na	k7c4	na
průhledný	průhledný	k2eAgInSc4d1	průhledný
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
sklo	sklo	k1gNnSc1	sklo
nebo	nebo	k8xC	nebo
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
materiály	materiál	k1gInPc1	materiál
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
světlo	světlo	k1gNnSc4	světlo
rozdílně	rozdílně	k6eAd1	rozdílně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lom	lom	k1gInSc1	lom
nastává	nastávat	k5eAaImIp3nS	nastávat
vždy	vždy	k6eAd1	vždy
pod	pod	k7c7	pod
jiným	jiný	k2eAgInSc7d1	jiný
úhlem	úhel	k1gInSc7	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
dokonalém	dokonalý	k2eAgNnSc6d1	dokonalé
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgNnSc1d1	univerzální
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
konstantou	konstanta	k1gFnSc7	konstanta
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
c	c	k0	c
=	=	kIx~	=
299	[number]	k4	299
792	[number]	k4	792
458	[number]	k4	458
ms	ms	k?	ms
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
byla	být	k5eAaImAgNnP	být
měřena	měřit	k5eAaImNgNnP	měřit
mnohokrát	mnohokrát	k6eAd1	mnohokrát
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
zdokumentovaných	zdokumentovaný	k2eAgNnPc2d1	zdokumentované
měření	měření	k1gNnPc2	měření
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
k	k	k7c3	k
přibližnému	přibližný	k2eAgInSc3d1	přibližný
výsledku	výsledek	k1gInSc3	výsledek
provedl	provést	k5eAaPmAgInS	provést
Dán	dán	k2eAgInSc1d1	dán
Ole	Ola	k1gFnSc3	Ola
Rø	Rø	k1gFnSc2	Rø
roku	rok	k1gInSc2	rok
1676	[number]	k4	1676
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
problematikou	problematika	k1gFnSc7	problematika
navigace	navigace	k1gFnSc2	navigace
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
pozoroval	pozorovat	k5eAaImAgInS	pozorovat
pohyb	pohyb	k1gInSc1	pohyb
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
měsíce	měsíc	k1gInSc2	měsíc
Io	Io	k1gMnPc2	Io
teleskopem	teleskop	k1gInSc7	teleskop
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
odchylku	odchylka	k1gFnSc4	odchylka
ve	v	k7c6	v
zdánlivé	zdánlivý	k2eAgFnSc6d1	zdánlivá
oběžné	oběžný	k2eAgFnSc6d1	oběžná
době	doba	k1gFnSc6	doba
Io	Io	k1gFnSc2	Io
<g/>
.	.	kIx.	.
</s>
<s>
Měřil	měřit	k5eAaImAgInS	měřit
čas	čas	k1gInSc1	čas
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
oběhů	oběh	k1gInPc2	oběh
Io	Io	k1gFnPc2	Io
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
Země	zem	k1gFnSc2	zem
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
a	a	k8xC	a
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
rozdíl	rozdíl	k1gInSc4	rozdíl
22	[number]	k4	22
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
správně	správně	k6eAd1	správně
přičetl	přičíst	k5eAaPmAgMnS	přičíst
konečné	konečný	k2eAgFnSc2d1	konečná
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
Rø	Rø	k1gMnSc1	Rø
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
zjištěním	zjištění	k1gNnSc7	zjištění
dále	daleko	k6eAd2	daleko
nepracoval	pracovat	k5eNaImAgInS	pracovat
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
hodnoty	hodnota	k1gFnSc2	hodnota
později	pozdě	k6eAd2	pozdě
vypočítali	vypočítat	k5eAaPmAgMnP	vypočítat
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
byl	být	k5eAaImAgMnS	být
význačný	význačný	k2eAgMnSc1d1	význačný
holandský	holandský	k2eAgMnSc1d1	holandský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astronom	astronom	k1gMnSc1	astronom
Christian	Christian	k1gMnSc1	Christian
Huygens	Huygens	k1gInSc4	Huygens
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
na	na	k7c4	na
220	[number]	k4	220
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
měření	měření	k1gNnSc1	měření
pozemskými	pozemský	k2eAgInPc7d1	pozemský
prostředky	prostředek	k1gInPc7	prostředek
provedl	provést	k5eAaPmAgInS	provést
Hippolyte	Hippolyt	k1gInSc5	Hippolyt
Fizeau	Fizea	k1gMnSc6	Fizea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
<g/>
.	.	kIx.	.
</s>
<s>
Fizeau	Fizeau	k6eAd1	Fizeau
poslal	poslat	k5eAaPmAgInS	poslat
svazek	svazek	k1gInSc1	svazek
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
vložil	vložit	k5eAaPmAgMnS	vložit
točící	točící	k2eAgMnSc1d1	točící
se	se	k3xPyFc4	se
ozubené	ozubený	k2eAgNnSc1d1	ozubené
kolo	kolo	k1gNnSc1	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
známé	známý	k2eAgFnSc6d1	známá
rychlosti	rychlost	k1gFnSc6	rychlost
otáčení	otáčení	k1gNnSc2	otáčení
kola	kolo	k1gNnSc2	kolo
vypočetl	vypočíst	k5eAaPmAgMnS	vypočíst
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
313	[number]	k4	313
000	[number]	k4	000
kms	kms	k?	kms
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
měření	měření	k1gNnSc1	měření
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
:	:	kIx,	:
po	po	k7c4	po
umístění	umístění	k1gNnSc4	umístění
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
se	se	k3xPyFc4	se
změřil	změřit	k5eAaPmAgInS	změřit
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
odražený	odražený	k2eAgInSc1d1	odražený
paprsek	paprsek	k1gInSc1	paprsek
laseru	laser	k1gInSc2	laser
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgFnSc7d1	univerzální
konstantou	konstanta	k1gFnSc7	konstanta
a	a	k8xC	a
čas	čas	k1gInSc4	čas
lze	lze	k6eAd1	lze
měřit	měřit	k5eAaImF	měřit
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
přesností	přesnost	k1gFnSc7	přesnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
metr	metr	k1gInSc1	metr
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnSc2	si
definována	definovat	k5eAaBmNgFnS	definovat
právě	právě	k6eAd1	právě
pomocí	pomocí	k7c2	pomocí
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
a	a	k8xC	a
hodnota	hodnota	k1gFnSc1	hodnota
této	tento	k3xDgFnSc2	tento
rychlosti	rychlost	k1gFnSc2	rychlost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zcela	zcela	k6eAd1	zcela
přesná	přesný	k2eAgFnSc1d1	přesná
a	a	k8xC	a
fixní	fixní	k2eAgFnSc1d1	fixní
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
případné	případný	k2eAgNnSc1d1	případné
zpřesňování	zpřesňování	k1gNnSc1	zpřesňování
měření	měření	k1gNnSc2	měření
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
nezměnilo	změnit	k5eNaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
šíří	šířit	k5eAaImIp3nS	šířit
rychlostí	rychlost	k1gFnSc7	rychlost
v	v	k7c6	v
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
c.	c.	k?	c.
Podíl	podíl	k1gInSc1	podíl
těchto	tento	k3xDgFnPc2	tento
rychlostí	rychlost	k1gFnPc2	rychlost
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
daného	daný	k2eAgNnSc2d1	dané
prostředí	prostředí	k1gNnSc2	prostředí
n	n	k0	n
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
n	n	k0	n
=	=	kIx~	=
c	c	k0	c
<g/>
/	/	kIx~	/
<g/>
v.	v.	k?	v.
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
dochází	docházet	k5eAaImIp3nP	docházet
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
hodnotami	hodnota	k1gFnPc7	hodnota
n	n	k0	n
k	k	k7c3	k
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
prostředí	prostředí	k1gNnSc1	prostředí
bez	bez	k7c2	bez
disperze	disperze	k1gFnSc2	disperze
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
disperzí	disperze	k1gFnSc7	disperze
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
fázovou	fázový	k2eAgFnSc4d1	fázová
a	a	k8xC	a
grupovou	grupový	k2eAgFnSc4d1	grupová
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
:	:	kIx,	:
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
popisuje	popisovat	k5eAaImIp3nS	popisovat
rychlost	rychlost	k1gFnSc4	rychlost
šíření	šíření	k1gNnSc4	šíření
ploch	plocha	k1gFnPc2	plocha
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
fází	fáze	k1gFnSc7	fáze
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
grupová	grupový	k2eAgFnSc1d1	grupová
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
obálce	obálka	k1gFnSc3	obálka
amplitudy	amplituda	k1gFnSc2	amplituda
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
k	k	k7c3	k
rychlosti	rychlost	k1gFnSc3	rychlost
šíření	šíření	k1gNnSc2	šíření
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
závislost	závislost	k1gFnSc1	závislost
indexu	index	k1gInSc2	index
lomu	lom	k1gInSc2	lom
na	na	k7c6	na
kruhové	kruhový	k2eAgFnSc6d1	kruhová
frekvenci	frekvence	k1gFnSc6	frekvence
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
ω	ω	k?	ω
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
(	(	kIx(	(
ω	ω	k?	ω
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
n	n	k0	n
(	(	kIx(	(
ω	ω	k?	ω
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
v	v	k7c4	v
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
a	a	k8xC	a
grupová	grupový	k2eAgFnSc1d1	grupová
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
ω	ω	k?	ω
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
n	n	k0	n
(	(	kIx(	(
ω	ω	k?	ω
)	)	kIx)	)
+	+	kIx~	+
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
dn	dn	k?	dn
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}}}}}	}}}}}	k?	}}}}}
.	.	kIx.	.
</s>
<s>
Fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
přenosem	přenos	k1gInSc7	přenos
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
téměř	téměř	k6eAd1	téměř
libovolných	libovolný	k2eAgFnPc2d1	libovolná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
c	c	k0	c
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
záporných	záporný	k2eAgInPc2d1	záporný
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
index	index	k1gInSc4	index
lomu	lom	k1gInSc2	lom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
grupová	grupový	k2eAgFnSc1d1	grupová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
hodnotu	hodnota	k1gFnSc4	hodnota
c	c	k0	c
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Šíření	šíření	k1gNnSc1	šíření
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
hmotě	hmota	k1gFnSc6	hmota
můžeme	moct	k5eAaImIp1nP	moct
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
opakované	opakovaný	k2eAgInPc1d1	opakovaný
pohlcovaní	pohlcovaný	k2eAgMnPc1d1	pohlcovaný
a	a	k8xC	a
vyzařovaní	vyzařovaný	k2eAgMnPc1d1	vyzařovaný
fotonů	foton	k1gInPc2	foton
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
ozáření	ozáření	k1gNnSc6	ozáření
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
atom	atom	k1gInSc1	atom
do	do	k7c2	do
excitovaného	excitovaný	k2eAgInSc2d1	excitovaný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
setrvá	setrvat	k5eAaPmIp3nS	setrvat
pouze	pouze	k6eAd1	pouze
zlomek	zlomek	k1gInSc4	zlomek
času	čas	k1gInSc2	čas
a	a	k8xC	a
následně	následně	k6eAd1	následně
foton	foton	k1gInSc1	foton
zpět	zpět	k6eAd1	zpět
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
následně	následně	k6eAd1	následně
pohltí	pohltit	k5eAaPmIp3nS	pohltit
další	další	k2eAgInSc4d1	další
atom	atom	k1gInSc4	atom
atd	atd	kA	atd
<g/>
...	...	k?	...
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pomaleji	pomale	k6eAd2	pomale
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
atomy	atom	k1gInPc1	atom
setrvávají	setrvávat	k5eAaImIp3nP	setrvávat
v	v	k7c6	v
excitovaném	excitovaný	k2eAgInSc6d1	excitovaný
stavu	stav	k1gInSc6	stav
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
tudíž	tudíž	k8xC	tudíž
ve	v	k7c6	v
hmotě	hmota	k1gFnSc6	hmota
šíří	šířit	k5eAaImIp3nS	šířit
rychlostí	rychlost	k1gFnSc7	rychlost
stejnou	stejný	k2eAgFnSc7d1	stejná
jako	jako	k8xS	jako
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
pohlcováno	pohlcovat	k5eAaImNgNnS	pohlcovat
a	a	k8xC	a
vyzařováno	vyzařovat	k5eAaImNgNnS	vyzařovat
atomy	atom	k1gInPc7	atom
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vhodně	vhodně	k6eAd1	vhodně
připraveném	připravený	k2eAgNnSc6d1	připravené
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
možné	možný	k2eAgNnSc1d1	možné
světlo	světlo	k1gNnSc1	světlo
dokonce	dokonce	k9	dokonce
zastavit	zastavit	k5eAaPmF	zastavit
a	a	k8xC	a
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
ho	on	k3xPp3gMnSc2	on
změnou	změna	k1gFnSc7	změna
vlastností	vlastnost	k1gFnPc2	vlastnost
prostředí	prostředí	k1gNnSc4	prostředí
uvolnit	uvolnit	k5eAaPmF	uvolnit
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
šíření	šíření	k1gNnSc3	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Absorpce	absorpce	k1gFnSc2	absorpce
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
světlo	světlo	k1gNnSc1	světlo
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
atomy	atom	k1gInPc7	atom
povrchu	povrch	k1gInSc3	povrch
daného	daný	k2eAgInSc2d1	daný
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
povrch	povrch	k6eAd1wR	povrch
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
druh	druh	k1gInSc1	druh
atomu	atom	k1gInSc2	atom
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
určité	určitý	k2eAgFnPc4d1	určitá
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
(	(	kIx(	(
<g/>
barvy	barva	k1gFnPc4	barva
<g/>
)	)	kIx)	)
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
povrchu	povrch	k1gInSc2	povrch
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
a	a	k8xC	a
které	který	k3yIgNnSc1	který
odráží	odrážet	k5eAaImIp3nS	odrážet
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
viděn	vidět	k5eAaImNgMnS	vidět
jako	jako	k8xC	jako
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
všechny	všechen	k3xTgFnPc4	všechen
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
a	a	k8xC	a
my	my	k3xPp1nPc1	my
vidíme	vidět	k5eAaImIp1nP	vidět
jen	jen	k9	jen
odrážené	odrážený	k2eAgNnSc4d1	odrážené
zelené	zelený	k2eAgNnSc4d1	zelené
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Interference	interference	k1gFnSc1	interference
<g/>
#	#	kIx~	#
<g/>
Interference	interference	k1gFnSc1	interference
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Šíří	šírý	k2eAgMnPc1d1	šírý
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
danou	daný	k2eAgFnSc7d1	daná
částí	část	k1gFnSc7	část
prostoru	prostor	k1gInSc2	prostor
více	hodně	k6eAd2	hodně
světelných	světelný	k2eAgFnPc2d1	světelná
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
skládání	skládání	k1gNnSc4	skládání
(	(	kIx(	(
<g/>
superpozici	superpozice	k1gFnSc4	superpozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
koherentních	koherentní	k2eAgInPc2d1	koherentní
světelných	světelný	k2eAgInPc2d1	světelný
svazků	svazek	k1gInPc2	svazek
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
interferenci	interference	k1gFnSc6	interference
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
vlny	vlna	k1gFnSc2	vlna
vzájemně	vzájemně	k6eAd1	vzájemně
posilují	posilovat	k5eAaImIp3nP	posilovat
(	(	kIx(	(
<g/>
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
<g/>
,	,	kIx,	,
též	též	k9	též
konstruktivní	konstruktivní	k2eAgFnSc1d1	konstruktivní
interference	interference	k1gFnSc1	interference
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
naopak	naopak	k6eAd1	naopak
zeslabují	zeslabovat	k5eAaImIp3nP	zeslabovat
(	(	kIx(	(
<g/>
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
,	,	kIx,	,
destruktivní	destruktivní	k2eAgFnSc1d1	destruktivní
interference	interference	k1gFnSc1	interference
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc4d1	různá
frekvence	frekvence	k1gFnPc4	frekvence
světla	světlo	k1gNnSc2	světlo
vidíme	vidět	k5eAaImIp1nP	vidět
jako	jako	k9	jako
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
od	od	k7c2	od
červeného	červený	k2eAgNnSc2d1	červené
světla	světlo	k1gNnSc2	světlo
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
frekvencí	frekvence	k1gFnSc7	frekvence
a	a	k8xC	a
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
po	po	k7c4	po
fialové	fialový	k2eAgFnPc4d1	fialová
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
frekvencí	frekvence	k1gFnSc7	frekvence
a	a	k8xC	a
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
ultrafialové	ultrafialový	k2eAgInPc1d1	ultrafialový
(	(	kIx(	(
<g/>
UV	UV	kA	UV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
kratších	krátký	k2eAgFnPc2d2	kratší
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
,	,	kIx,	,
a	a	k8xC	a
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
IR	Ir	k1gMnSc1	Ir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
delších	dlouhý	k2eAgFnPc2d2	delší
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
lidé	člověk	k1gMnPc1	člověk
nevidí	vidět	k5eNaImIp3nP	vidět
IR	Ir	k1gMnSc1	Ir
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
blízké	blízký	k2eAgFnPc1d1	blízká
IR	Ir	k1gMnSc1	Ir
cítit	cítit	k5eAaImF	cítit
jako	jako	k9	jako
teplo	teplo	k1gNnSc4	teplo
svými	svůj	k3xOyFgInPc7	svůj
receptory	receptor	k1gInPc7	receptor
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
zase	zase	k9	zase
na	na	k7c6	na
člověku	člověk	k1gMnSc6	člověk
projeví	projevit	k5eAaPmIp3nS	projevit
zvýšením	zvýšení	k1gNnSc7	zvýšení
pigmentace	pigmentace	k1gFnSc2	pigmentace
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
známým	známý	k2eAgNnSc7d1	známé
opálením	opálení	k1gNnSc7	opálení
<g/>
.	.	kIx.	.
</s>
<s>
Fotometrie	fotometrie	k1gFnPc1	fotometrie
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
měřením	měření	k1gNnSc7	měření
intenzity	intenzita	k1gFnSc2	intenzita
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
následující	následující	k2eAgFnSc1d1	následující
fotometrické	fotometrický	k2eAgFnPc4d1	fotometrická
veličiny	veličina	k1gFnPc4	veličina
<g/>
:	:	kIx,	:
jas	jas	k1gInSc1	jas
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
teplota	teplota	k1gFnSc1	teplota
<g/>
)	)	kIx)	)
intenzita	intenzita	k1gFnSc1	intenzita
osvětlení	osvětlení	k1gNnSc2	osvětlení
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
lux	lux	k1gInSc1	lux
<g/>
)	)	kIx)	)
světelný	světelný	k2eAgInSc1d1	světelný
tok	tok	k1gInSc1	tok
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
lumen	lumen	k1gNnSc1	lumen
<g/>
)	)	kIx)	)
svítivost	svítivost	k1gFnSc1	svítivost
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
kandela	kandela	k1gFnSc1	kandela
<g/>
)	)	kIx)	)
Kolorimetrie	kolorimetrie	k1gFnSc1	kolorimetrie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
popisem	popis	k1gInSc7	popis
a	a	k8xC	a
měřením	měření	k1gNnSc7	měření
barevného	barevný	k2eAgInSc2d1	barevný
projevu	projev	k1gInSc2	projev
vnímaného	vnímaný	k2eAgNnSc2d1	vnímané
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
založeném	založený	k2eAgNnSc6d1	založené
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
jasu	jas	k1gInSc2	jas
<g/>
)	)	kIx)	)
zejména	zejména	k9	zejména
na	na	k7c6	na
spektrálním	spektrální	k2eAgNnSc6d1	spektrální
složení	složení	k1gNnSc6	složení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zastoupení	zastoupení	k1gNnSc4	zastoupení
vln	vlna	k1gFnPc2	vlna
různé	různý	k2eAgFnSc2d1	různá
frekvence	frekvence	k1gFnSc2	frekvence
viditelného	viditelný	k2eAgInSc2d1	viditelný
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
měří	měřit	k5eAaImIp3nS	měřit
např.	např.	kA	např.
stupeň	stupeň	k1gInSc1	stupeň
koherence	koherence	k1gFnSc1	koherence
(	(	kIx(	(
<g/>
fázové	fázový	k2eAgFnSc2d1	fázová
shody	shoda	k1gFnSc2	shoda
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
ve	v	k7c6	v
světelném	světelný	k2eAgInSc6d1	světelný
svazku	svazek	k1gInSc6	svazek
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
interferenčním	interferenční	k2eAgInPc3d1	interferenční
jevům	jev	k1gInPc3	jev
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
stupeň	stupeň	k1gInSc1	stupeň
polarizace	polarizace	k1gFnSc2	polarizace
(	(	kIx(	(
<g/>
shody	shoda	k1gFnSc2	shoda
roviny	rovina	k1gFnSc2	rovina
kmitů	kmit	k1gInPc2	kmit
intenzity	intenzita	k1gFnSc2	intenzita
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
ve	v	k7c6	v
světelném	světelný	k2eAgInSc6d1	světelný
svazku	svazek	k1gInSc6	svazek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
projevující	projevující	k2eAgFnSc1d1	projevující
se	se	k3xPyFc4	se
při	při	k7c6	při
odrazu	odraz	k1gInSc6	odraz
a	a	k8xC	a
lomu	lom	k1gInSc6	lom
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dvojlom	dvojlom	k1gInSc1	dvojlom
<g/>
)	)	kIx)	)
a	a	k8xC	a
využívaný	využívaný	k2eAgMnSc1d1	využívaný
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
optické	optický	k2eAgFnSc2d1	optická
aktivity	aktivita	k1gFnSc2	aktivita
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
sálání	sálání	k1gNnSc1	sálání
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
záření	záření	k1gNnSc2	záření
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
záření	záření	k1gNnSc1	záření
žárovky	žárovka	k1gFnSc2	žárovka
sluneční	sluneční	k2eAgNnSc1d1	sluneční
světlo	světlo	k1gNnSc1	světlo
záření	záření	k1gNnSc2	záření
plazmatu	plazma	k1gNnSc2	plazma
(	(	kIx(	(
<g/>
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
oblouková	obloukový	k2eAgFnSc1d1	oblouková
lampa	lampa	k1gFnSc1	lampa
<g/>
)	)	kIx)	)
atomová	atomový	k2eAgFnSc1d1	atomová
spektrální	spektrální	k2eAgFnSc1d1	spektrální
<g />
.	.	kIx.	.
</s>
<s>
emise	emise	k1gFnPc1	emise
(	(	kIx(	(
<g/>
emise	emise	k1gFnPc1	emise
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stimulované	stimulovaný	k2eAgFnPc1d1	stimulovaná
nebo	nebo	k8xC	nebo
spontánní	spontánní	k2eAgFnPc1d1	spontánní
<g/>
)	)	kIx)	)
laser	laser	k1gInSc1	laser
a	a	k8xC	a
maser	maser	k1gInSc1	maser
(	(	kIx(	(
<g/>
stimulovaná	stimulovaný	k2eAgFnSc1d1	stimulovaná
emise	emise	k1gFnSc1	emise
<g/>
)	)	kIx)	)
světlo	světlo	k1gNnSc1	světlo
LED	LED	kA	LED
diody	dioda	k1gFnSc2	dioda
plynové	plynový	k2eAgFnSc2d1	plynová
výbojky	výbojka	k1gFnSc2	výbojka
urychlení	urychlení	k1gNnSc2	urychlení
volného	volný	k2eAgInSc2d1	volný
nosiče	nosič	k1gInSc2	nosič
proudu	proud	k1gInSc2	proud
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
synchrotronech	synchrotron	k1gInPc6	synchrotron
<g/>
)	)	kIx)	)
luminiscence	luminiscence	k1gFnPc1	luminiscence
<g/>
,	,	kIx,	,
Fotoluminiscence	fotoluminiscence	k1gFnSc1	fotoluminiscence
Elektroluminiscence	elektroluminiscence	k1gFnSc1	elektroluminiscence
Katodoluminiscence	Katodoluminiscence	k1gFnSc1	Katodoluminiscence
Chemiluminiscence	chemiluminiscence	k1gFnSc1	chemiluminiscence
Radioluminiscence	radioluminiscence	k1gFnSc1	radioluminiscence
Triboluminiscence	triboluminiscence	k1gFnSc1	triboluminiscence
fluorescence	fluorescence	k1gFnSc1	fluorescence
fosforescence	fosforescence	k1gFnSc2	fosforescence
katodové	katodový	k2eAgNnSc4d1	katodové
záření	záření	k1gNnSc4	záření
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
rozpad	rozpad	k1gInSc1	rozpad
anihilace	anihilace	k1gFnSc2	anihilace
páru	pár	k1gInSc2	pár
částice-antičástice	částicentičástika	k1gFnSc6	částice-antičástika
Světla	světlo	k1gNnSc2	světlo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
přístrojích	přístroj	k1gInPc6	přístroj
(	(	kIx(	(
<g/>
LCD	LCD	kA	LCD
obrazovkách	obrazovka	k1gFnPc6	obrazovka
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
přehrávačích	přehrávač	k1gInPc6	přehrávač
<g/>
,	,	kIx,	,
mobilech	mobil	k1gInPc6	mobil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
svařuje	svařovat	k5eAaImIp3nS	svařovat
i	i	k8xC	i
řeže	řezat	k5eAaImIp3nS	řezat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
operuje	operovat	k5eAaImIp3nS	operovat
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgFnSc2d1	výrobní
technologie	technologie	k1gFnSc2	technologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
světla	světlo	k1gNnSc2	světlo
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
lidé	člověk	k1gMnPc1	člověk
i	i	k8xC	i
vzdálená	vzdálený	k2eAgNnPc1d1	vzdálené
vesmírná	vesmírný	k2eAgNnPc1d1	vesmírné
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
<g/>
,	,	kIx,	,
odráží	odrážet	k5eAaImIp3nP	odrážet
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
