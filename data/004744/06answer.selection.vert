<s>
Nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
na	na	k7c6	na
základech	základ	k1gInPc6	základ
národního	národní	k2eAgNnSc2d1	národní
ponížení	ponížení	k1gNnSc2	ponížení
<g/>
,	,	kIx,	,
hanby	hanba	k1gFnSc2	hanba
a	a	k8xC	a
vzteku	vztek	k1gInSc2	vztek
vycházející	vycházející	k2eAgFnSc1d1	vycházející
ze	z	k7c2	z
znění	znění	k1gNnSc2	znění
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
Německu	Německo	k1gNnSc6	Německo
vnutily	vnutit	k5eAaPmAgFnP	vnutit
vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
