<s>
Základem	základ	k1gInSc7	základ
aristotelského	aristotelský	k2eAgNnSc2d1	aristotelské
třídění	třídění	k1gNnSc2	třídění
věcí	věc	k1gFnPc2	věc
je	být	k5eAaImIp3nS	být
představa	představa	k1gFnSc1	představa
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
genos	genos	k1gInSc1	genos
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
genus	genus	k1gInSc1	genus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
eidos	eidos	k1gInSc1	eidos
<g/>
,	,	kIx,	,
species	species	k1gFnSc1	species
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nP	lišit
"	"	kIx"	"
<g/>
specifickou	specifický	k2eAgFnSc7d1	specifická
<g/>
"	"	kIx"	"
čili	čili	k8xC	čili
druhovou	druhový	k2eAgFnSc7d1	druhová
diferencí	diference	k1gFnSc7	diference
<g/>
.	.	kIx.	.
</s>
