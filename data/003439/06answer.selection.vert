<s>
Zebřička	zebřička	k1gFnSc1	zebřička
pestrá	pestrý	k2eAgFnSc1d1	pestrá
(	(	kIx(	(
<g/>
Taeniopygia	Taeniopygia	k1gFnSc1	Taeniopygia
guttata	guttata	k1gFnSc1	guttata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
a	a	k8xC	a
nejznámějším	známý	k2eAgInSc7d3	nejznámější
druhem	druh	k1gInSc7	druh
astrildovitých	astrildovitý	k2eAgMnPc2d1	astrildovitý
ptáků	pták	k1gMnPc2	pták
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
