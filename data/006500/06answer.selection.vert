<s>
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Isidore	Isidor	k1gMnSc5	Isidor
Marie	Maria	k1gFnSc2	Maria
Auguste	August	k1gMnSc5	August
François	François	k1gInSc1	François
Xavier	Xavier	k1gInSc1	Xavier
Comte	Comt	k1gInSc5	Comt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1798	[number]	k4	1798
<g/>
,	,	kIx,	,
Montpellier	Montpellier	k1gInSc1	Montpellier
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1857	[number]	k4	1857
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgMnSc1d1	společenský
reformátor	reformátor	k1gMnSc1	reformátor
a	a	k8xC	a
originální	originální	k2eAgMnSc1d1	originální
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
