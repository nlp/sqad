<s>
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Isidore	Isidor	k1gMnSc5	Isidor
Marie	Maria	k1gFnSc2	Maria
Auguste	August	k1gMnSc5	August
François	François	k1gInSc1	François
Xavier	Xavier	k1gInSc1	Xavier
Comte	Comt	k1gInSc5	Comt
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1798	[number]	k4	1798
<g/>
,	,	kIx,	,
Montpellier	Montpellier	k1gInSc1	Montpellier
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1857	[number]	k4	1857
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgMnSc1d1	společenský
reformátor	reformátor	k1gMnSc1	reformátor
a	a	k8xC	a
originální	originální	k2eAgMnSc1d1	originální
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1798	[number]	k4	1798
v	v	k7c6	v
jihofrancouzském	jihofrancouzský	k2eAgInSc6d1	jihofrancouzský
Montpellier	Montpellier	k1gInSc1	Montpellier
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1807	[number]	k4	1807
až	až	k9	až
1814	[number]	k4	1814
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
prospěchem	prospěch	k1gInSc7	prospěch
lyceum	lyceum	k1gNnSc1	lyceum
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
École	École	k1gFnPc4	École
Polytechnique	Polytechniqu	k1gInSc2	Polytechniqu
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnSc1d1	přední
vědecká	vědecký	k2eAgFnSc1d1	vědecká
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
škola	škola	k1gFnSc1	škola
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
během	během	k7c2	během
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
a	a	k8xC	a
půl	půl	k1xP	půl
byl	být	k5eAaImAgMnS	být
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
protestu	protest	k1gInSc6	protest
proti	proti	k7c3	proti
zastaralému	zastaralý	k2eAgInSc3d1	zastaralý
způsobu	způsob	k1gInSc3	způsob
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Načas	načas	k6eAd1	načas
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
rodného	rodný	k2eAgNnSc2d1	rodné
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c4	po
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
však	však	k9	však
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgInS	živit
kondicemi	kondice	k1gFnPc7	kondice
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
emigraci	emigrace	k1gFnSc6	emigrace
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
byl	být	k5eAaImAgInS	být
Comte	Comt	k1gInSc5	Comt
představen	představit	k5eAaPmNgInS	představit
téměř	téměř	k6eAd1	téměř
šedesátiletému	šedesátiletý	k2eAgMnSc3d1	šedesátiletý
Saint-Simonovi	Saint-Simon	k1gMnSc3	Saint-Simon
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
osobním	osobní	k2eAgMnSc7d1	osobní
sekretářem	sekretář	k1gMnSc7	sekretář
<g/>
,	,	kIx,	,
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
a	a	k8xC	a
spoluautorem	spoluautor	k1gMnSc7	spoluautor
řady	řada	k1gFnSc2	řada
statí	stať	k1gFnPc2	stať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1822	[number]	k4	1822
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Comte	Comt	k1gInSc5	Comt
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Saint-Simonovy	Saint-Simonův	k2eAgFnSc2d1	Saint-Simonův
práce	práce	k1gFnSc2	práce
"	"	kIx"	"
<g/>
Katechismus	katechismus	k1gInSc1	katechismus
průmyslníků	průmyslník	k1gMnPc2	průmyslník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Catéchisme	Catéchismus	k1gInSc5	Catéchismus
des	des	k1gNnSc3	des
Industriels	Industriels	k1gInSc4	Industriels
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
pouhých	pouhý	k2eAgInPc2d1	pouhý
100	[number]	k4	100
výtisků	výtisk	k1gInPc2	výtisk
<g/>
)	)	kIx)	)
svůj	svůj	k3xOyFgInSc4	svůj
programový	programový	k2eAgInSc4d1	programový
text	text	k1gInSc4	text
o	o	k7c6	o
plánu	plán	k1gInSc6	plán
na	na	k7c6	na
reorganizaci	reorganizace	k1gFnSc6	reorganizace
společnosti	společnost	k1gFnSc2	společnost
pomocí	pomocí	k7c2	pomocí
vědy	věda	k1gFnSc2	věda
Plan	plan	k1gInSc1	plan
des	des	k1gNnSc1	des
travaux	travaux	k1gInSc1	travaux
scientifiques	scientifiques	k1gMnSc1	scientifiques
nécessaires	nécessaires	k1gMnSc1	nécessaires
pour	pour	k1gMnSc1	pour
réorganiser	réorganiser	k1gMnSc1	réorganiser
la	la	k1gNnPc2	la
société	sociétý	k2eAgFnPc1d1	sociétý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vyšel	vyjít	k5eAaPmAgInS	vyjít
tento	tento	k3xDgInSc4	tento
text	text	k1gInSc4	text
samostatně	samostatně	k6eAd1	samostatně
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
tisíc	tisíc	k4xCgInPc2	tisíc
výtisků	výtisk	k1gInPc2	výtisk
a	a	k8xC	a
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
Comtovým	Comtův	k2eAgNnSc7d1	Comtovo
autorstvím	autorství	k1gNnSc7	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
se	s	k7c7	s
Saint-Simonem	Saint-Simon	k1gInSc7	Saint-Simon
kvůli	kvůli	k7c3	kvůli
sporům	spor	k1gInPc3	spor
o	o	k7c4	o
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
prioritu	priorita	k1gFnSc4	priorita
společných	společný	k2eAgInPc2d1	společný
textů	text	k1gInPc2	text
a	a	k8xC	a
taktiky	taktika	k1gFnSc2	taktika
při	při	k7c6	při
reformování	reformování	k1gNnSc6	reformování
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
raná	raný	k2eAgFnSc1d1	raná
verze	verze	k1gFnSc1	verze
práce	práce	k1gFnSc2	práce
"	"	kIx"	"
<g/>
Systém	systém	k1gInSc1	systém
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
politiky	politika	k1gFnSc2	politika
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Systè	Systè	k1gFnSc1	Systè
de	de	k?	de
politique	politiquus	k1gMnSc5	politiquus
positive	positiv	k1gMnSc5	positiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
přivítala	přivítat	k5eAaPmAgFnS	přivítat
řada	řada	k1gFnSc1	řada
renomovaných	renomovaný	k2eAgMnPc2d1	renomovaný
vědců	vědec	k1gMnPc2	vědec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
Caroline	Carolin	k1gInSc5	Carolin
Massinovou	Massinová	k1gFnSc4	Massinová
<g/>
.	.	kIx.	.
</s>
<s>
Skromně	skromně	k6eAd1	skromně
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgInS	živit
kondicemi	kondice	k1gFnPc7	kondice
a	a	k8xC	a
psaním	psaní	k1gNnSc7	psaní
drobných	drobný	k2eAgInPc2d1	drobný
textů	text	k1gInPc2	text
do	do	k7c2	do
listu	list	k1gInSc2	list
Producteur	Producteura	k1gFnPc2	Producteura
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Saint-Simona	Saint-Simon	k1gMnSc2	Saint-Simon
vydávali	vydávat	k5eAaImAgMnP	vydávat
jeho	jeho	k3xOp3gMnPc1	jeho
žáci	žák	k1gMnPc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1826	[number]	k4	1826
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Comte	Comt	k1gInSc5	Comt
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
soukromý	soukromý	k2eAgInSc4d1	soukromý
kurs	kurs	k1gInSc4	kurs
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgMnSc6	který
seznamoval	seznamovat	k5eAaImAgMnS	seznamovat
zájemce	zájemce	k1gMnSc1	zájemce
se	s	k7c7	s
zásadami	zásada	k1gFnPc7	zásada
své	svůj	k3xOyFgFnSc2	svůj
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přepracování	přepracování	k1gNnSc2	přepracování
<g/>
,	,	kIx,	,
rodinných	rodinný	k2eAgFnPc2d1	rodinná
neshod	neshoda	k1gFnPc2	neshoda
a	a	k8xC	a
existenčních	existenční	k2eAgInPc2d1	existenční
problémů	problém	k1gInPc2	problém
se	se	k3xPyFc4	se
Comte	Comt	k1gInSc5	Comt
duševně	duševně	k6eAd1	duševně
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nucen	nucen	k2eAgMnSc1d1	nucen
své	svůj	k3xOyFgInPc4	svůj
kursy	kurs	k1gInPc4	kurs
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
obnovení	obnovení	k1gNnSc3	obnovení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
získal	získat	k5eAaPmAgMnS	získat
Comte	Comt	k1gInSc5	Comt
pomocnou	pomocný	k2eAgFnSc4d1	pomocná
učitelskou	učitelský	k2eAgFnSc4d1	učitelská
funkci	funkce	k1gFnSc4	funkce
na	na	k7c6	na
Polytechnice	polytechnika	k1gFnSc6	polytechnika
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žil	žít	k5eAaImAgInS	žít
ve	v	k7c6	v
vysloveně	vysloveně	k6eAd1	vysloveně
nuzných	nuzný	k2eAgFnPc6d1	nuzná
existenčních	existenční	k2eAgFnPc6d1	existenční
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
funkci	funkce	k1gFnSc4	funkce
externího	externí	k2eAgMnSc2d1	externí
examinátora	examinátor	k1gMnSc2	examinátor
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
ústavu	ústav	k1gInSc6	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
soukromě	soukromě	k6eAd1	soukromě
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
každou	každý	k3xTgFnSc4	každý
neděli	neděle	k1gFnSc4	neděle
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
hvězdárně	hvězdárna	k1gFnSc6	hvězdárna
pořádal	pořádat	k5eAaImAgInS	pořádat
bezplatné	bezplatný	k2eAgFnPc4d1	bezplatná
přednášky	přednáška	k1gFnPc4	přednáška
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
o	o	k7c4	o
astronomii	astronomie	k1gFnSc4	astronomie
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
lidé	člověk	k1gMnPc1	člověk
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
sociálních	sociální	k2eAgFnPc2d1	sociální
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Comte	Comte	k5eAaPmIp2nP	Comte
nikdy	nikdy	k6eAd1	nikdy
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
řádné	řádný	k2eAgFnPc4d1	řádná
akademické	akademický	k2eAgFnPc4d1	akademická
hodnosti	hodnost	k1gFnPc4	hodnost
ani	ani	k8xC	ani
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
žádosti	žádost	k1gFnPc1	žádost
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
Polytechnice	polytechnika	k1gFnSc6	polytechnika
byly	být	k5eAaImAgInP	být
opakovaně	opakovaně	k6eAd1	opakovaně
zamítány	zamítán	k2eAgInPc1d1	zamítán
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
obci	obec	k1gFnSc6	obec
získal	získat	k5eAaPmAgMnS	získat
několik	několik	k4yIc4	několik
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc4	von
Humboldt	Humboldt	k1gMnSc1	Humboldt
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Mill	k1gMnSc1	Mill
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
podporoval	podporovat	k5eAaImAgMnS	podporovat
finančně	finančně	k6eAd1	finančně
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
ho	on	k3xPp3gNnSc4	on
také	také	k9	také
vydavatel	vydavatel	k1gMnSc1	vydavatel
Émile	Émile	k1gFnSc2	Émile
Littré	Littrý	k2eAgFnSc2d1	Littrý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Comte	Comt	k1gInSc5	Comt
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
sám	sám	k3xTgMnSc1	sám
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
přestal	přestat	k5eAaPmAgInS	přestat
číst	číst	k5eAaImF	číst
odbornou	odborný	k2eAgFnSc4d1	odborná
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
četl	číst	k5eAaImAgMnS	číst
životopis	životopis	k1gInSc4	životopis
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
od	od	k7c2	od
středověkého	středověký	k2eAgMnSc2d1	středověký
autora	autor	k1gMnSc2	autor
Tomáše	Tomáš	k1gMnSc2	Tomáš
Kempenského	Kempenský	k2eAgMnSc2d1	Kempenský
<g/>
,	,	kIx,	,
učil	učit	k5eAaImAgMnS	učit
se	se	k3xPyFc4	se
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
a	a	k8xC	a
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
číst	číst	k5eAaImF	číst
Danta	Dante	k1gMnSc4	Dante
<g/>
,	,	kIx,	,
Ariosta	Ariost	k1gMnSc4	Ariost
<g/>
,	,	kIx,	,
Miltona	Milton	k1gMnSc4	Milton
<g/>
,	,	kIx,	,
Byrona	Byron	k1gMnSc4	Byron
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
k	k	k7c3	k
poslednímu	poslední	k2eAgNnSc3d1	poslední
dílu	dílo	k1gNnSc3	dílo
"	"	kIx"	"
<g/>
Kursu	kurs	k1gInSc2	kurs
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
"	"	kIx"	"
vyčetl	vyčíst	k5eAaPmAgMnS	vyčíst
akademické	akademický	k2eAgFnSc3d1	akademická
obci	obec	k1gFnSc3	obec
její	její	k3xOp3gInSc1	její
formalismus	formalismus	k1gInSc1	formalismus
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc1	neschopnost
specialistů	specialista	k1gMnPc2	specialista
vidět	vidět	k5eAaImF	vidět
za	za	k7c7	za
zkoumanými	zkoumaný	k2eAgInPc7d1	zkoumaný
jevy	jev	k1gInPc7	jev
hlubší	hluboký	k2eAgFnSc2d2	hlubší
souvislosti	souvislost	k1gFnSc2	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
jeho	jeho	k3xOp3gFnSc2	jeho
hlavní	hlavní	k2eAgFnSc2d1	hlavní
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
definitivně	definitivně	k6eAd1	definitivně
rozešla	rozejít	k5eAaPmAgFnS	rozejít
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
předtím	předtím	k6eAd1	předtím
několikrát	několikrát	k6eAd1	několikrát
dočasně	dočasně	k6eAd1	dočasně
rozcházel	rozcházet	k5eAaImAgMnS	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
vydal	vydat	k5eAaPmAgMnS	vydat
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc3	pojednání
o	o	k7c6	o
analytické	analytický	k2eAgFnSc6d1	analytická
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
byl	být	k5eAaImAgMnS	být
propuštěn	propustit	k5eAaPmNgMnS	propustit
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
examinátora	examinátor	k1gMnSc2	examinátor
a	a	k8xC	a
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgInPc2	svůj
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
darů	dar	k1gInPc2	dar
Johna	John	k1gMnSc2	John
Stuarta	Stuart	k1gMnSc2	Stuart
Milla	Mill	k1gMnSc2	Mill
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Clotildou	Clotilda	k1gMnSc7	Clotilda
de	de	k?	de
Vaux	Vaux	k1gInSc1	Vaux
<g/>
,	,	kIx,	,
sestrou	sestra	k1gFnSc7	sestra
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
její	její	k3xOp3gFnSc4	její
přízeň	přízeň	k1gFnSc4	přízeň
<g/>
.	.	kIx.	.
</s>
<s>
Clotilda	Clotilda	k1gFnSc1	Clotilda
po	po	k7c6	po
roce	rok	k1gInSc6	rok
a	a	k8xC	a
půl	půl	k6eAd1	půl
platonické	platonický	k2eAgFnSc2d1	platonická
lásky	láska	k1gFnSc2	láska
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Comte	Comt	k1gInSc5	Comt
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
Systém	systém	k1gInSc1	systém
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
politiky	politika	k1gFnSc2	politika
čili	čili	k8xC	čili
pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
sociologii	sociologie	k1gFnSc6	sociologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Systè	Systè	k1gFnSc1	Systè
de	de	k?	de
politique	politiquus	k1gMnSc5	politiquus
positive	positiv	k1gMnSc5	positiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
primát	primát	k1gMnSc1	primát
citu	cit	k1gInSc2	cit
nad	nad	k7c7	nad
intelektem	intelekt	k1gInSc7	intelekt
a	a	k8xC	a
koncipoval	koncipovat	k5eAaBmAgMnS	koncipovat
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c4	o
náboženství	náboženství	k1gNnSc4	náboženství
lidskosti	lidskost	k1gFnSc2	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
až	až	k9	až
1854	[number]	k4	1854
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
stále	stále	k6eAd1	stále
mystičtější	mystický	k2eAgInPc1d2	mystičtější
spisy	spis	k1gInPc4	spis
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
odváděly	odvádět	k5eAaImAgInP	odvádět
přízeň	přízeň	k1gFnSc4	přízeň
racionálně	racionálně	k6eAd1	racionálně
založených	založený	k2eAgMnPc2d1	založený
příznivců	příznivec	k1gMnPc2	příznivec
včetně	včetně	k7c2	včetně
Johna	John	k1gMnSc2	John
Stuarta	Stuart	k1gMnSc2	Stuart
Milla	Mill	k1gMnSc2	Mill
a	a	k8xC	a
Emila	Emil	k1gMnSc2	Emil
Littré	Littrý	k2eAgNnSc1d1	Littré
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
rozešel	rozejít	k5eAaPmAgMnS	rozejít
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
když	když	k8xS	když
publikoval	publikovat	k5eAaBmAgMnS	publikovat
nábožensko-mystický	náboženskoystický	k2eAgMnSc1d1	nábožensko-mystický
"	"	kIx"	"
<g/>
Pozitivistický	pozitivistický	k2eAgInSc1d1	pozitivistický
katechismus	katechismus	k1gInSc1	katechismus
čili	čili	k8xC	čili
shrnutí	shrnutí	k1gNnSc1	shrnutí
univerzálního	univerzální	k2eAgNnSc2d1	univerzální
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Catéchisme	Catéchismus	k1gInSc5	Catéchismus
positiviste	positivist	k1gMnSc5	positivist
ou	ou	k0	ou
sommaire	sommair	k1gInSc5	sommair
de	de	k?	de
la	la	k1gNnPc1	la
religion	religion	k1gInSc1	religion
universelle	universella	k1gFnSc3	universella
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
poslední	poslední	k2eAgInSc4d1	poslední
zdroj	zdroj	k1gInSc4	zdroj
svých	svůj	k3xOyFgInPc2	svůj
příjmů	příjem	k1gInPc2	příjem
na	na	k7c6	na
Polytechnice	polytechnika	k1gFnSc6	polytechnika
<g/>
,	,	kIx,	,
o	o	k7c4	o
další	další	k2eAgMnPc4d1	další
stoupence	stoupenec	k1gMnPc4	stoupenec
přišel	přijít	k5eAaPmAgInS	přijít
<g/>
,	,	kIx,	,
když	když	k8xS	když
podpořil	podpořit	k5eAaPmAgInS	podpořit
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Bonaparta	Bonapart	k1gMnSc2	Bonapart
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
žil	žít	k5eAaImAgMnS	žít
jen	jen	k9	jen
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
svých	svůj	k3xOyFgMnPc2	svůj
nejvěrnějších	věrný	k2eAgMnPc2d3	nejvěrnější
stoupenců	stoupenec	k1gMnPc2	stoupenec
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
založil	založit	k5eAaPmAgMnS	založit
Pozitivistickou	pozitivistický	k2eAgFnSc4d1	pozitivistická
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
pořádal	pořádat	k5eAaImAgMnS	pořádat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
žaludku	žaludek	k1gInSc2	žaludek
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
svých	svůj	k3xOyFgMnPc2	svůj
nejvěrnějších	věrný	k2eAgMnPc2d3	nejvěrnější
žáků	žák	k1gMnPc2	žák
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1857	[number]	k4	1857
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
.	.	kIx.	.
</s>
<s>
Katoličtí	katolický	k2eAgMnPc1d1	katolický
filozofové	filozof	k1gMnPc1	filozof
tomistické	tomistický	k2eAgFnSc2d1	tomistická
orientace	orientace	k1gFnSc2	orientace
ostře	ostro	k6eAd1	ostro
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
proti	proti	k7c3	proti
názorům	názor	k1gInPc3	názor
Augusta	August	k1gMnSc2	August
Comta	Comt	k1gMnSc2	Comt
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
profesora	profesor	k1gMnSc2	profesor
Eugena	Eugen	k1gMnSc2	Eugen
Kadeřávka	Kadeřávek	k1gMnSc2	Kadeřávek
byl	být	k5eAaImAgInS	být
Comte	Comt	k1gInSc5	Comt
"	"	kIx"	"
<g/>
hlasatel	hlasatel	k1gMnSc1	hlasatel
materialismu	materialismus	k1gInSc2	materialismus
<g/>
,	,	kIx,	,
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
modloslužebnosti	modloslužebnost	k1gFnSc2	modloslužebnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
dekretem	dekret	k1gInSc7	dekret
ze	z	k7c2	z
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1864	[number]	k4	1864
zařadila	zařadit	k5eAaPmAgFnS	zařadit
Comtův	Comtův	k2eAgInSc4d1	Comtův
Kurz	kurz	k1gInSc4	kurz
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Cours	Cours	k1gInSc1	Cours
de	de	k?	de
philosophie	philosophius	k1gMnSc5	philosophius
positive	positiv	k1gMnSc5	positiv
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
filosofií	filosofie	k1gFnSc7	filosofie
Augusta	August	k1gMnSc2	August
Comta	Comt	k1gMnSc2	Comt
byl	být	k5eAaImAgInS	být
také	také	k9	také
zaveden	zaveden	k2eAgInSc1d1	zaveden
pojem	pojem	k1gInSc1	pojem
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
jako	jako	k9	jako
jednotná	jednotný	k2eAgFnSc1d1	jednotná
metoda	metoda	k1gFnSc1	metoda
všech	všecek	k3xTgFnPc2	všecek
věd	věda	k1gFnPc2	věda
ustavuje	ustavovat	k5eAaImIp3nS	ustavovat
metoda	metoda	k1gFnSc1	metoda
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tzv.	tzv.	kA	tzv.
negativní	negativní	k2eAgFnSc1d1	negativní
filosofie	filosofie	k1gFnSc1	filosofie
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
hledání	hledání	k1gNnSc4	hledání
prvotních	prvotní	k2eAgFnPc2d1	prvotní
či	či	k8xC	či
konečných	konečný	k2eAgFnPc2d1	konečná
příčin	příčina	k1gFnPc2	příčina
a	a	k8xC	a
nevysvětluje	vysvětlovat	k5eNaImIp3nS	vysvětlovat
fakta	faktum	k1gNnSc2	faktum
silami	síla	k1gFnPc7	síla
nebo	nebo	k8xC	nebo
entitami	entita	k1gFnPc7	entita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nelze	lze	k6eNd1	lze
bezprostředně	bezprostředně	k6eAd1	bezprostředně
verifikovat	verifikovat	k5eAaBmF	verifikovat
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
faktů	fakt	k1gInPc2	fakt
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
pozitivním	pozitivní	k2eAgNnSc6d1	pozitivní
stádiu	stádium	k1gNnSc6	stádium
lidský	lidský	k2eAgMnSc1d1	lidský
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
nabýt	nabýt	k5eAaPmF	nabýt
absolutních	absolutní	k2eAgInPc2d1	absolutní
poznatků	poznatek	k1gInPc2	poznatek
<g/>
,	,	kIx,	,
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
se	se	k3xPyFc4	se
hledání	hledání	k1gNnSc1	hledání
původu	původ	k1gInSc2	původ
a	a	k8xC	a
účelu	účel	k1gInSc2	účel
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
poznání	poznání	k1gNnSc3	poznání
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
příčin	příčina	k1gFnPc2	příčina
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
velmi	velmi	k6eAd1	velmi
složitého	složitý	k2eAgNnSc2d1	složité
usuzování	usuzování	k1gNnSc2	usuzování
a	a	k8xC	a
pozorování	pozorování	k1gNnPc4	pozorování
věnoval	věnovat	k5eAaImAgMnS	věnovat
objevování	objevování	k1gNnSc4	objevování
jejich	jejich	k3xOp3gMnPc2	jejich
skutečných	skutečný	k2eAgMnPc2d1	skutečný
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
jejich	jejich	k3xOp3gInPc2	jejich
stálých	stálý	k2eAgInPc2d1	stálý
vztahů	vztah	k1gInPc2	vztah
následnosti	následnost	k1gFnSc2	následnost
a	a	k8xC	a
podobnosti	podobnost	k1gFnSc2	podobnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
"	"	kIx"	"
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
několika	několik	k4yIc2	několik
významů	význam	k1gInPc2	význam
<g/>
:	:	kIx,	:
něco	něco	k3yInSc1	něco
skutečného	skutečný	k2eAgNnSc2d1	skutečné
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
smysluplného	smysluplný	k2eAgNnSc2d1	smysluplné
a	a	k8xC	a
užitečného	užitečný	k2eAgNnSc2d1	užitečné
a	a	k8xC	a
něco	něco	k3yInSc4	něco
jednoznačně	jednoznačně	k6eAd1	jednoznačně
definovaného	definovaný	k2eAgNnSc2d1	definované
<g/>
.	.	kIx.	.
</s>
<s>
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
vymezil	vymezit	k5eAaPmAgInS	vymezit
pozitivistickou	pozitivistický	k2eAgFnSc4d1	pozitivistická
filosofii	filosofie	k1gFnSc4	filosofie
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
těmito	tento	k3xDgInPc7	tento
významy	význam	k1gInPc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivismus	pozitivismus	k1gInSc1	pozitivismus
se	se	k3xPyFc4	se
přidržuje	přidržovat	k5eAaImIp3nS	přidržovat
pouze	pouze	k6eAd1	pouze
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
daných	daný	k2eAgFnPc2d1	daná
fakt	faktum	k1gNnPc2	faktum
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
společensky	společensky	k6eAd1	společensky
užitečné	užitečný	k2eAgNnSc1d1	užitečné
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
nekonečným	konečný	k2eNgInPc3d1	nekonečný
sporům	spor	k1gInPc3	spor
dřívější	dřívější	k2eAgFnSc2d1	dřívější
metafyziky	metafyzika	k1gFnSc2	metafyzika
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
lze	lze	k6eAd1	lze
přesně	přesně	k6eAd1	přesně
definovat	definovat	k5eAaBmF	definovat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Störig	Störig	k1gMnSc1	Störig
<g/>
,	,	kIx,	,
s.	s.	k?	s.
341	[number]	k4	341
<g/>
)	)	kIx)	)
</s>
<s>
Comte	Comte	k5eAaPmIp2nP	Comte
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
zejména	zejména	k9	zejména
R.	R.	kA	R.
Descartem	Descart	k1gMnSc7	Descart
a	a	k8xC	a
F.	F.	kA	F.
Baconem	Bacon	k1gInSc7	Bacon
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
jeho	jeho	k3xOp3gFnSc2	jeho
filosofie	filosofie	k1gFnSc2	filosofie
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
přijímání	přijímání	k1gNnSc6	přijímání
faktů	fakt	k1gInPc2	fakt
a	a	k8xC	a
zkoumání	zkoumání	k1gNnSc2	zkoumání
jejich	jejich	k3xOp3gInPc2	jejich
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
zkoumání	zkoumání	k1gNnSc2	zkoumání
lze	lze	k6eAd1	lze
shrnout	shrnout	k5eAaPmF	shrnout
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
bodů	bod	k1gInPc2	bod
<g/>
:	:	kIx,	:
konstatovat	konstatovat	k5eAaBmF	konstatovat
fakta	faktum	k1gNnPc4	faktum
daná	daný	k2eAgFnSc1d1	daná
formou	forma	k1gFnSc7	forma
jevu	jev	k1gInSc2	jev
uspořádat	uspořádat	k5eAaPmF	uspořádat
je	on	k3xPp3gInPc4	on
podle	podle	k7c2	podle
určitých	určitý	k2eAgInPc2d1	určitý
zákonů	zákon	k1gInPc2	zákon
předvídat	předvídat	k5eAaImF	předvídat
ze	z	k7c2	z
zjištěných	zjištěný	k2eAgFnPc2d1	zjištěná
zákonitostí	zákonitost	k1gFnPc2	zákonitost
budoucí	budoucí	k2eAgInPc1d1	budoucí
jevy	jev	k1gInPc1	jev
a	a	k8xC	a
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
Jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
fakta	faktum	k1gNnPc4	faktum
Comte	Comt	k1gInSc5	Comt
spojuje	spojovat	k5eAaImIp3nS	spojovat
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
podobnosti	podobnost	k1gFnSc2	podobnost
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
získáváme	získávat	k5eAaImIp1nP	získávat
pojmy	pojem	k1gInPc1	pojem
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
posloupnosti	posloupnost	k1gFnSc2	posloupnost
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
zákony	zákon	k1gInPc1	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
filosofii	filosofie	k1gFnSc3	filosofie
Comte	Comt	k1gInSc5	Comt
logicky	logicky	k6eAd1	logicky
musel	muset	k5eAaImAgInS	muset
dospět	dospět	k5eAaPmF	dospět
ke	k	k7c3	k
klasifikaci	klasifikace	k1gFnSc3	klasifikace
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
vědy	věda	k1gFnPc4	věda
na	na	k7c4	na
procesy	proces	k1gInPc4	proces
anorganických	anorganický	k2eAgFnPc2d1	anorganická
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
všeho	všecek	k3xTgNnSc2	všecek
jsou	být	k5eAaImIp3nP	být
zákony	zákon	k1gInPc1	zákon
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Anorganické	anorganický	k2eAgInPc1d1	anorganický
jevy	jev	k1gInPc1	jev
dělí	dělit	k5eAaImIp3nP	dělit
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
obecné	obecný	k2eAgInPc4d1	obecný
procesy	proces	k1gInPc4	proces
v	v	k7c6	v
kosmu	kosmos	k1gInSc6	kosmos
(	(	kIx(	(
<g/>
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
)	)	kIx)	)
a	a	k8xC	a
anorganické	anorganický	k2eAgInPc1d1	anorganický
procesy	proces	k1gInPc1	proces
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc1	chemie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgInPc4d1	organický
jevy	jev	k1gInPc4	jev
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
individuu	individuum	k1gNnSc6	individuum
(	(	kIx(	(
<g/>
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
druhu	druh	k1gInSc6	druh
(	(	kIx(	(
<g/>
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zákon	zákon	k1gInSc1	zákon
tří	tři	k4xCgNnPc2	tři
stádií	stádium	k1gNnPc2	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
bouřlivé	bouřlivý	k2eAgFnSc6d1	bouřlivá
době	doba	k1gFnSc6	doba
revolucí	revoluce	k1gFnPc2	revoluce
a	a	k8xC	a
uvolňování	uvolňování	k1gNnSc1	uvolňování
morálky	morálka	k1gFnSc2	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
jeho	jeho	k3xOp3gNnSc2	jeho
učení	učení	k1gNnSc2	učení
byla	být	k5eAaImAgFnS	být
reforma	reforma	k1gFnSc1	reforma
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
obnova	obnova	k1gFnSc1	obnova
morálních	morální	k2eAgFnPc2d1	morální
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
stability	stabilita	k1gFnSc2	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
tří	tři	k4xCgFnPc2	tři
stádií	stádium	k1gNnPc2	stádium
dokládá	dokládat	k5eAaImIp3nS	dokládat
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
stádiích	stádium	k1gNnPc6	stádium
vývoj	vývoj	k1gInSc4	vývoj
myšlení	myšlení	k1gNnSc2	myšlení
lidského	lidský	k2eAgMnSc2d1	lidský
jedince	jedinec	k1gMnSc2	jedinec
i	i	k9	i
lidstva	lidstvo	k1gNnSc2	lidstvo
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
stádiu	stádium	k1gNnSc6	stádium
pak	pak	k6eAd1	pak
nabízí	nabízet	k5eAaImIp3nS	nabízet
svou	svůj	k3xOyFgFnSc4	svůj
vizi	vize	k1gFnSc4	vize
ideálního	ideální	k2eAgNnSc2d1	ideální
společenského	společenský	k2eAgNnSc2d1	společenské
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Stadium	stadium	k1gNnSc1	stadium
teologické	teologický	k2eAgNnSc1d1	teologické
neboli	neboli	k8xC	neboli
fiktivní	fiktivní	k2eAgMnSc1d1	fiktivní
Člověk	člověk	k1gMnSc1	člověk
hledá	hledat	k5eAaImIp3nS	hledat
absolutní	absolutní	k2eAgNnSc4d1	absolutní
poznání	poznání	k1gNnSc4	poznání
skrze	skrze	k?	skrze
zkoumání	zkoumání	k1gNnSc2	zkoumání
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
povahy	povaha	k1gFnSc2	povaha
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
každým	každý	k3xTgInSc7	každý
procesem	proces	k1gInSc7	proces
je	být	k5eAaImIp3nS	být
živá	živý	k2eAgFnSc1d1	živá
vůle	vůle	k1gFnSc1	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
stadium	stadium	k1gNnSc1	stadium
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
etapách	etapa	k1gFnPc6	etapa
<g/>
:	:	kIx,	:
animismus	animismus	k1gInSc4	animismus
<g/>
,	,	kIx,	,
polyteismus	polyteismus	k1gInSc4	polyteismus
a	a	k8xC	a
monoteismus	monoteismus	k1gInSc4	monoteismus
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Stadium	stadium	k1gNnSc1	stadium
metafyzické	metafyzický	k2eAgNnSc1d1	metafyzické
neboli	neboli	k8xC	neboli
abstraktní	abstraktní	k2eAgMnSc1d1	abstraktní
I	i	k9	i
zde	zde	k6eAd1	zde
hledá	hledat	k5eAaImIp3nS	hledat
člověk	člověk	k1gMnSc1	člověk
absolutní	absolutní	k2eAgNnSc1d1	absolutní
poznání	poznání	k1gNnSc1	poznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezkoumá	zkoumat	k5eNaImIp3nS	zkoumat
již	již	k6eAd1	již
nadpřirozené	nadpřirozený	k2eAgFnPc4d1	nadpřirozená
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
abstraktní	abstraktní	k2eAgInPc1d1	abstraktní
pojmy	pojem	k1gInPc1	pojem
a	a	k8xC	a
entity	entita	k1gFnPc1	entita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
obecnou	obecný	k2eAgFnSc4d1	obecná
entitu	entita	k1gFnSc4	entita
uznává	uznávat	k5eAaImIp3nS	uznávat
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vidí	vidět	k5eAaImIp3nS	vidět
pramen	pramen	k1gInSc1	pramen
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Stadium	stadium	k1gNnSc1	stadium
vědecké	vědecký	k2eAgNnSc1d1	vědecké
neboli	neboli	k8xC	neboli
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
Člověk	člověk	k1gMnSc1	člověk
dospívá	dospívat	k5eAaImIp3nS	dospívat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
absolutní	absolutní	k2eAgNnSc1d1	absolutní
poznání	poznání	k1gNnSc1	poznání
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pozorováním	pozorování	k1gNnSc7	pozorování
a	a	k8xC	a
rozumem	rozum	k1gInSc7	rozum
poznat	poznat	k5eAaPmF	poznat
podobnosti	podobnost	k1gFnSc3	podobnost
a	a	k8xC	a
posloupnosti	posloupnost	k1gFnSc3	posloupnost
v	v	k7c6	v
daných	daný	k2eAgInPc6d1	daný
faktech	fakt	k1gInPc6	fakt
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
podřídit	podřídit	k5eAaPmF	podřídit
je	on	k3xPp3gNnPc4	on
jedinému	jediný	k2eAgInSc3d1	jediný
všeobecnému	všeobecný	k2eAgInSc3d1	všeobecný
faktu	fakt	k1gInSc3	fakt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
pozitivního	pozitivní	k2eAgNnSc2d1	pozitivní
myšlení	myšlení	k1gNnSc2	myšlení
lidstva	lidstvo	k1gNnSc2	lidstvo
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
nastat	nastat	k5eAaPmF	nastat
podle	podle	k7c2	podle
Comta	Comt	k1gMnSc2	Comt
ideální	ideální	k2eAgFnSc1d1	ideální
forma	forma	k1gFnSc1	forma
společenského	společenský	k2eAgNnSc2d1	společenské
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
o	o	k7c4	o
stát	stát	k1gInSc4	stát
s	s	k7c7	s
pevně	pevně	k6eAd1	pevně
stanoveným	stanovený	k2eAgInSc7d1	stanovený
řádem	řád	k1gInSc7	řád
<g/>
,	,	kIx,	,
řízený	řízený	k2eAgMnSc1d1	řízený
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
odborníky	odborník	k1gMnPc4	odborník
a	a	k8xC	a
specialisty	specialista	k1gMnPc4	specialista
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
převažoval	převažovat	k5eAaImAgInS	převažovat
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Comte	Comte	k5eAaPmIp2nP	Comte
tak	tak	k9	tak
do	do	k7c2	do
filosofie	filosofie	k1gFnSc2	filosofie
dějin	dějiny	k1gFnPc2	dějiny
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vznik	vznik	k1gInSc1	vznik
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
pozitivnímu	pozitivní	k2eAgNnSc3d1	pozitivní
stadiu	stadion	k1gNnSc3	stadion
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
spočívat	spočívat	k5eAaImF	spočívat
v	v	k7c6	v
poznání	poznání	k1gNnSc6	poznání
univerzálních	univerzální	k2eAgInPc2d1	univerzální
zákonů	zákon	k1gInPc2	zákon
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
faktů	fakt	k1gInPc2	fakt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
zákonů	zákon	k1gInPc2	zákon
bude	být	k5eAaImBp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
řídit	řídit	k5eAaImF	řídit
běh	běh	k1gInSc4	běh
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Comte	Comit	k5eAaPmRp2nP	Comit
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
.	.	kIx.	.
</s>
<s>
Vidí	vidět	k5eAaImIp3nP	vidět
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xS	jako
filosofii	filosofie	k1gFnSc3	filosofie
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
procházejí	procházet	k5eAaImIp3nP	procházet
již	již	k6eAd1	již
zmíněnými	zmíněný	k2eAgInPc7d1	zmíněný
třemi	tři	k4xCgNnPc7	tři
stadii	stadion	k1gNnPc7	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Neodráží	odrážet	k5eNaImIp3nS	odrážet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
jen	jen	k9	jen
státní	státní	k2eAgInSc4d1	státní
<g/>
,	,	kIx,	,
právní	právní	k2eAgInSc4d1	právní
a	a	k8xC	a
společenský	společenský	k2eAgInSc4d1	společenský
vývoj	vývoj	k1gInSc4	vývoj
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
říká	říkat	k5eAaImIp3nS	říkat
Hegel	Hegel	k1gInSc1	Hegel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vývoj	vývoj	k1gInSc4	vývoj
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Sociologii	sociologie	k1gFnSc4	sociologie
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
Comte	Comt	k1gInSc5	Comt
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
teorii	teorie	k1gFnSc4	teorie
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
uspořádání	uspořádání	k1gNnSc2	uspořádání
(	(	kIx(	(
<g/>
společenská	společenský	k2eAgFnSc1d1	společenská
statika	statika	k1gFnSc1	statika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
uvažování	uvažování	k1gNnSc1	uvažování
o	o	k7c6	o
institucích	instituce	k1gFnPc6	instituce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
rovnováhu	rovnováha	k1gFnSc4	rovnováha
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
ty	ten	k3xDgMnPc4	ten
Comte	Comt	k1gInSc5	Comt
považuje	považovat	k5eAaImIp3nS	považovat
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
třem	tři	k4xCgFnPc3	tři
základním	základní	k2eAgFnPc3d1	základní
mozkovým	mozkový	k2eAgFnPc3d1	mozková
funkcím	funkce	k1gFnPc3	funkce
cítění	cítění	k1gNnSc2	cítění
(	(	kIx(	(
<g/>
rodina	rodina	k1gFnSc1	rodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednání	jednání	k1gNnSc1	jednání
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
intelektu	intelekt	k1gInSc2	intelekt
(	(	kIx(	(
<g/>
církev	církev	k1gFnSc1	církev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
částí	část	k1gFnSc7	část
Comtovy	Comtův	k2eAgFnSc2d1	Comtova
sociologie	sociologie	k1gFnSc2	sociologie
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
pokroku	pokrok	k1gInSc6	pokrok
(	(	kIx(	(
<g/>
společenská	společenský	k2eAgFnSc1d1	společenská
dynamika	dynamika	k1gFnSc1	dynamika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
jádrem	jádro	k1gNnSc7	jádro
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
tří	tři	k4xCgNnPc2	tři
stádií	stádium	k1gNnPc2	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
řečeno	říct	k5eAaPmNgNnS	říct
Comte	Comt	k1gInSc5	Comt
dal	dát	k5eAaPmAgInS	dát
sociologii	sociologie	k1gFnSc4	sociologie
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
program	program	k1gInSc4	program
(	(	kIx(	(
<g/>
řídit	řídit	k5eAaImF	řídit
společnost	společnost	k1gFnSc4	společnost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poznání	poznání	k1gNnSc2	poznání
jejích	její	k3xOp3gFnPc2	její
zákonitostí	zákonitost	k1gFnPc2	zákonitost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
jistě	jistě	k6eAd1	jistě
významné	významný	k2eAgFnPc1d1	významná
a	a	k8xC	a
inspirativní	inspirativní	k2eAgFnPc1d1	inspirativní
<g/>
,	,	kIx,	,
do	do	k7c2	do
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
hlavně	hlavně	k9	hlavně
název	název	k1gInSc1	název
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
sociologie	sociologie	k1gFnSc1	sociologie
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k9	jako
stabilizátor	stabilizátor	k1gInSc4	stabilizátor
společenského	společenský	k2eAgInSc2d1	společenský
řádu	řád	k1gInSc2	řád
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poznání	poznání	k1gNnSc2	poznání
zákonů	zákon	k1gInPc2	zákon
fungování	fungování	k1gNnSc2	fungování
společnosti	společnost	k1gFnSc2	společnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
zpochybněna	zpochybněn	k2eAgFnSc1d1	zpochybněna
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnozí	mnohý	k2eAgMnPc1d1	mnohý
sociologové	sociolog	k1gMnPc1	sociolog
vůbec	vůbec	k9	vůbec
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgInPc1	takový
zákony	zákon	k1gInPc1	zákon
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Povaha	povaha	k1gFnSc1	povaha
Comtovy	Comtův	k2eAgFnSc2d1	Comtova
sociologie	sociologie	k1gFnSc2	sociologie
je	být	k5eAaImIp3nS	být
snadněji	snadno	k6eAd2	snadno
pochopitelná	pochopitelný	k2eAgNnPc1d1	pochopitelné
uvážíme	uvážit	k5eAaPmIp1nP	uvážit
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mnozí	mnohý	k2eAgMnPc1d1	mnohý
zakladatelé	zakladatel	k1gMnPc1	zakladatel
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
sociologii	sociologie	k1gFnSc4	sociologie
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
Comte	Comt	k1gInSc5	Comt
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vědecké	vědecký	k2eAgNnSc4d1	vědecké
myšlení	myšlení	k1gNnSc4	myšlení
o	o	k7c6	o
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
"	"	kIx"	"
<g/>
Pozitivistických	pozitivistický	k2eAgFnPc2d1	pozitivistická
společností	společnost	k1gFnPc2	společnost
<g/>
"	"	kIx"	"
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
brazilská	brazilský	k2eAgFnSc1d1	brazilská
vlajka	vlajka	k1gFnSc1	vlajka
dodnes	dodnes	k6eAd1	dodnes
nese	nést	k5eAaImIp3nS	nést
Comtovo	Comtův	k2eAgNnSc1d1	Comtovo
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Řád	řád	k1gInSc1	řád
a	a	k8xC	a
pokrok	pokrok	k1gInSc1	pokrok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ordem	Ordem	k1gInSc1	Ordem
e	e	k0	e
progresso	progressa	k1gFnSc5	progressa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Comtovo	Comtův	k2eAgNnSc1d1	Comtovo
základní	základní	k2eAgNnSc1d1	základní
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
společenské	společenský	k2eAgFnSc3d1	společenská
skutečnosti	skutečnost	k1gFnSc3	skutečnost
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vysvětlovat	vysvětlovat	k5eAaImF	vysvětlovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkoumání	zkoumání	k1gNnPc2	zkoumání
společenských	společenský	k2eAgInPc2d1	společenský
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
Émile	Émile	k1gInSc1	Émile
Durkheim	Durkheima	k1gFnPc2	Durkheima
a	a	k8xC	a
na	na	k7c4	na
Comtovy	Comtův	k2eAgFnPc4d1	Comtova
myšlenky	myšlenka	k1gFnPc4	myšlenka
výslovně	výslovně	k6eAd1	výslovně
navazoval	navazovat	k5eAaImAgMnS	navazovat
i	i	k9	i
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Comtův	Comtův	k2eAgInSc4d1	Comtův
program	program	k1gInSc4	program
přímo	přímo	k6eAd1	přímo
navázal	navázat	k5eAaPmAgInS	navázat
logický	logický	k2eAgInSc1d1	logický
pozitivismus	pozitivismus	k1gInSc1	pozitivismus
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc1d2	pozdější
analytická	analytický	k2eAgFnSc1d1	analytická
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
filosofie	filosofie	k1gFnSc2	filosofie
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
)	)	kIx)	)
Kurz	kurz	k1gInSc1	kurz
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
filozofie	filozofie	k1gFnSc2	filozofie
(	(	kIx(	(
<g/>
6	[number]	k4	6
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c4	o
sociologii	sociologie	k1gFnSc4	sociologie
(	(	kIx(	(
<g/>
1851	[number]	k4	1851
<g/>
–	–	k?	–
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
Pozitivistický	pozitivistický	k2eAgInSc1d1	pozitivistický
katechismus	katechismus	k1gInSc1	katechismus
(	(	kIx(	(
<g/>
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
</s>
