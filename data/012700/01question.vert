<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
léčivá	léčivý	k2eAgFnSc1d1	léčivá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
parazitickým	parazitický	k2eAgInPc3d1	parazitický
helmintům	helmint	k1gInPc3	helmint
<g/>
?	?	kIx.	?
</s>
