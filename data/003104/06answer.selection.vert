<s>
Robot	robot	k1gMnSc1	robot
je	být	k5eAaImIp3nS	být
stroj	stroj	k1gInSc4	stroj
pracující	pracující	k1gFnSc2	pracující
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
mírou	míra	k1gFnSc7	míra
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
,	,	kIx,	,
vykonávající	vykonávající	k2eAgInPc4d1	vykonávající
určené	určený	k2eAgInPc4d1	určený
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
předepsaným	předepsaný	k2eAgInSc7d1	předepsaný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
mírách	míra	k1gFnPc6	míra
potřeby	potřeba	k1gFnSc2	potřeba
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
a	a	k8xC	a
se	s	k7c7	s
zadavatelem	zadavatel	k1gMnSc7	zadavatel
<g/>
:	:	kIx,	:
Robot	robot	k1gMnSc1	robot
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
vnímat	vnímat	k5eAaImF	vnímat
pomocí	pomocí	k7c2	pomocí
senzorů	senzor	k1gInPc2	senzor
<g/>
,	,	kIx,	,
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
si	se	k3xPyFc3	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
vytvářet	vytvářet	k5eAaImF	vytvářet
vlastní	vlastní	k2eAgFnSc4d1	vlastní
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
model	model	k1gInSc4	model
<g/>
.	.	kIx.	.
</s>
