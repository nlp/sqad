<s>
Robot	robot	k1gMnSc1	robot
je	být	k5eAaImIp3nS	být
stroj	stroj	k1gInSc4	stroj
pracující	pracující	k1gFnSc2	pracující
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
mírou	míra	k1gFnSc7	míra
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
,	,	kIx,	,
vykonávající	vykonávající	k2eAgInPc4d1	vykonávající
určené	určený	k2eAgInPc4d1	určený
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
předepsaným	předepsaný	k2eAgInSc7d1	předepsaný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
mírách	míra	k1gFnPc6	míra
potřeby	potřeba	k1gFnSc2	potřeba
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
a	a	k8xC	a
se	s	k7c7	s
zadavatelem	zadavatel	k1gMnSc7	zadavatel
<g/>
:	:	kIx,	:
Robot	robot	k1gMnSc1	robot
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
vnímat	vnímat	k5eAaImF	vnímat
pomocí	pomocí	k7c2	pomocí
senzorů	senzor	k1gInPc2	senzor
<g/>
,	,	kIx,	,
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
si	se	k3xPyFc3	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
vytvářet	vytvářet	k5eAaImF	vytvářet
vlastní	vlastní	k2eAgFnSc4d1	vlastní
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
model	model	k1gInSc4	model
<g/>
.	.	kIx.	.
</s>
<s>
Vnímáním	vnímání	k1gNnSc7	vnímání
světa	svět	k1gInSc2	svět
nejenže	nejenže	k6eAd1	nejenže
může	moct	k5eAaImIp3nS	moct
poznávat	poznávat	k5eAaImF	poznávat
svět	svět	k1gInSc1	svět
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
vyhodnocovat	vyhodnocovat	k5eAaImF	vyhodnocovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
využívat	využívat	k5eAaImF	využívat
tak	tak	k6eAd1	tak
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Robot	robot	k1gInSc1	robot
je	být	k5eAaImIp3nS	být
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
realizací	realizace	k1gFnSc7	realizace
obecnějšího	obecní	k2eAgInSc2d2	obecní
pojmu	pojem	k1gInSc2	pojem
agent	agent	k1gMnSc1	agent
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
robota	robot	k1gMnSc4	robot
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
otrocká	otrocký	k2eAgFnSc1d1	otrocká
práce	práce	k1gFnSc1	práce
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Mírně	mírně	k6eAd1	mírně
pozměněné	pozměněný	k2eAgNnSc1d1	pozměněné
jej	on	k3xPp3gNnSc4	on
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
stroj	stroj	k1gInSc1	stroj
použil	použít	k5eAaPmAgMnS	použít
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
Slovo	slovo	k1gNnSc4	slovo
mu	on	k3xPp3gMnSc3	on
poradil	poradit	k5eAaPmAgMnS	poradit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc4	on
Karel	Karel	k1gMnSc1	Karel
ptal	ptat	k5eAaImAgMnS	ptat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
umělou	umělý	k2eAgFnSc4d1	umělá
bytost	bytost	k1gFnSc4	bytost
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
zamýšlený	zamýšlený	k2eAgMnSc1d1	zamýšlený
labor	labor	k1gMnSc1	labor
zněl	znět	k5eAaImAgMnS	znět
autorovi	autor	k1gMnSc3	autor
příliš	příliš	k6eAd1	příliš
papírově	papírově	k6eAd1	papírově
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
já	já	k3xPp1nSc1	já
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jak	jak	k8xC	jak
mám	mít	k5eAaImIp1nS	mít
ty	ten	k3xDgMnPc4	ten
umělé	umělý	k2eAgMnPc4d1	umělý
dělníky	dělník	k1gMnPc4	dělník
nazvat	nazvat	k5eAaPmF	nazvat
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
bych	by	kYmCp1nS	by
jim	on	k3xPp3gInPc3	on
laboři	laboř	k1gFnSc3	laboř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připadá	připadat	k5eAaImIp3nS	připadat
mně	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
nějak	nějak	k6eAd1	nějak
papírové	papírový	k2eAgInPc1d1	papírový
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Tak	tak	k6eAd1	tak
jim	on	k3xPp3gMnPc3	on
řekni	říct	k5eAaPmRp2nS	říct
roboti	robot	k1gMnPc1	robot
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
mumlal	mumlat	k5eAaImAgMnS	mumlat
malíř	malíř	k1gMnSc1	malíř
se	s	k7c7	s
štětcem	štětec	k1gInSc7	štětec
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
a	a	k8xC	a
maloval	malovat	k5eAaImAgMnS	malovat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
slovo	slovo	k1gNnSc1	slovo
robot	robot	k1gInSc1	robot
výhradně	výhradně	k6eAd1	výhradně
neživotné	životný	k2eNgFnSc2d1	neživotná
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
les	les	k1gInSc1	les
<g/>
;	;	kIx,	;
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
roboty	robota	k1gFnSc2	robota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
inteligentní	inteligentní	k2eAgMnPc4d1	inteligentní
roboty	robot	k1gMnPc4	robot
(	(	kIx(	(
<g/>
podobné	podobný	k2eAgFnPc4d1	podobná
člověku	člověk	k1gMnSc6	člověk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
vědeckofantastické	vědeckofantastický	k2eAgFnSc6d1	vědeckofantastická
literatuře	literatura	k1gFnSc6	literatura
<g/>
)	)	kIx)	)
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
životné	životný	k2eAgNnSc1d1	životné
skloňování	skloňování	k1gNnSc1	skloňování
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
pán	pán	k1gMnSc1	pán
(	(	kIx(	(
<g/>
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
roboti	robot	k1gMnPc1	robot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neživotné	životný	k2eNgNnSc1d1	neživotné
skloňování	skloňování	k1gNnSc1	skloňování
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
člověku	člověk	k1gMnSc3	člověk
nepodobné	podobný	k2eNgFnSc2d1	nepodobná
roboty	robota	k1gFnSc2	robota
(	(	kIx(	(
<g/>
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kuchyňský	kuchyňský	k1gMnSc1	kuchyňský
robot	robot	k1gMnSc1	robot
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
generace	generace	k1gFnSc2	generace
na	na	k7c4	na
<g/>
:	:	kIx,	:
roboty	robota	k1gFnSc2	robota
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
-	-	kIx~	-
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pevného	pevný	k2eAgInSc2d1	pevný
programu	program	k1gInSc2	program
roboty	robota	k1gFnSc2	robota
2	[number]	k4	2
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
vybavené	vybavený	k2eAgInPc1d1	vybavený
senzory	senzor	k1gInPc1	senzor
a	a	k8xC	a
čidly	čidlo	k1gNnPc7	čidlo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgMnPc3	jenž
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
okolní	okolní	k2eAgFnPc4d1	okolní
podmínky	podmínka	k1gFnPc4	podmínka
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
schopnosti	schopnost	k1gFnSc2	schopnost
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
se	se	k3xPyFc4	se
na	na	k7c4	na
<g/>
:	:	kIx,	:
stacionární	stacionární	k2eAgFnSc1d1	stacionární
-	-	kIx~	-
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
místo	místo	k1gNnSc1	místo
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
manipulátory	manipulátor	k1gInPc1	manipulátor
<g/>
)	)	kIx)	)
mobilní	mobilní	k2eAgNnPc1d1	mobilní
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
sondy	sonda	k1gFnPc1	sonda
a	a	k8xC	a
vozítka	vozítko	k1gNnPc1	vozítko
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
také	také	k9	také
podle	podle	k7c2	podle
<g/>
:	:	kIx,	:
pohybových	pohybový	k2eAgFnPc2d1	pohybová
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
účelu	účel	k1gInSc3	účel
(	(	kIx(	(
<g/>
boj	boj	k1gInSc1	boj
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
tiskárny	tiskárna	k1gFnPc1	tiskárna
a	a	k8xC	a
plotry	plotr	k1gInPc1	plotr
<g/>
,	,	kIx,	,
přeprava	přeprava	k1gFnSc1	přeprava
<g/>
,	,	kIx,	,
průzkum	průzkum	k1gInSc1	průzkum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc2	způsob
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
<g/>
,	,	kIx,	,
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
způsobu	způsoba	k1gFnSc4	způsoba
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
aspektů	aspekt	k1gInPc2	aspekt
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tyto	tento	k3xDgInPc1	tento
roboty	robot	k1gInPc1	robot
<g/>
:	:	kIx,	:
Manipulátor	manipulátor	k1gInSc1	manipulátor
-	-	kIx~	-
stroj	stroj	k1gInSc1	stroj
nemající	mající	k2eNgInSc1d1	nemající
vlastní	vlastní	k2eAgFnSc3d1	vlastní
inteligenci	inteligence	k1gFnSc3	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovládán	ovládat	k5eAaImNgMnS	ovládat
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Kuchyňský	kuchyňský	k1gMnSc1	kuchyňský
robot	robot	k1gMnSc1	robot
-	-	kIx~	-
kombinace	kombinace	k1gFnSc1	kombinace
mixéru	mixér	k1gInSc2	mixér
<g/>
,	,	kIx,	,
hnětače	hnětač	k1gInSc2	hnětač
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
kuchyňských	kuchyňský	k2eAgInPc2d1	kuchyňský
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
provedený	provedený	k2eAgInSc1d1	provedený
jako	jako	k8xS	jako
motorová	motorový	k2eAgFnSc1d1	motorová
jednotka	jednotka	k1gFnSc1	jednotka
s	s	k7c7	s
nástavci	nástavec	k1gInSc6	nástavec
Android	android	k1gInSc1	android
-	-	kIx~	-
robot	robot	k1gInSc1	robot
podobný	podobný	k2eAgInSc1d1	podobný
člověku	člověk	k1gMnSc3	člověk
-	-	kIx~	-
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
biologické	biologický	k2eAgNnSc4d1	biologické
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Roboti	robot	k1gMnPc1	robot
v	v	k7c6	v
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
byli	být	k5eAaImAgMnP	být
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
dělení	dělený	k2eAgMnPc1d1	dělený
androidi	android	k1gMnPc1	android
<g/>
.	.	kIx.	.
</s>
<s>
Droid	Droid	k1gInSc1	Droid
-	-	kIx~	-
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
a	a	k8xC	a
samočinný	samočinný	k2eAgInSc1d1	samočinný
robot	robot	k1gInSc1	robot
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
i	i	k9	i
dron	dron	k1gInSc1	dron
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
droid	droid	k1gInSc1	droid
pracující	pracující	k2eAgMnSc1d1	pracující
jako	jako	k8xS	jako
dělník	dělník	k1gMnSc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Humanoid	Humanoid	k1gInSc1	Humanoid
-	-	kIx~	-
robot	robot	k1gInSc1	robot
podobný	podobný	k2eAgInSc1d1	podobný
člověku	člověk	k1gMnSc6	člověk
principiální	principiální	k2eAgFnSc2d1	principiální
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
zejména	zejména	k9	zejména
způsobem	způsob	k1gInSc7	způsob
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Anthropomorfní	Anthropomorfní	k2eAgInSc1d1	Anthropomorfní
-	-	kIx~	-
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
člověku	člověk	k1gMnSc3	člověk
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
(	(	kIx(	(
<g/>
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
ho	on	k3xPp3gMnSc4	on
<g/>
)	)	kIx)	)
buď	buď	k8xC	buď
fyzicky	fyzicky	k6eAd1	fyzicky
<g/>
,	,	kIx,	,
způsobem	způsob	k1gInSc7	způsob
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
mentálně	mentálně	k6eAd1	mentálně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
HAL	halo	k1gNnPc2	halo
9000	[number]	k4	9000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kyborg	Kyborg	k1gInSc1	Kyborg
(	(	kIx(	(
<g/>
kybernetický	kybernetický	k2eAgInSc1d1	kybernetický
organismus	organismus	k1gInSc1	organismus
<g/>
)	)	kIx)	)
-	-	kIx~	-
umělá	umělý	k2eAgFnSc1d1	umělá
bytost	bytost	k1gFnSc1	bytost
či	či	k8xC	či
mysl	mysl	k1gFnSc1	mysl
<g/>
,	,	kIx,	,
biologické	biologický	k2eAgNnSc4d1	biologické
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgNnSc4d1	přírodní
tělo	tělo	k1gNnSc4	tělo
plně	plně	k6eAd1	plně
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
nějaké	nějaký	k3yIgNnSc1	nějaký
bio-kybernetické	bioybernetický	k2eAgNnSc1d1	bio-kybernetický
propojení	propojení	k1gNnSc1	propojení
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
opačný	opačný	k2eAgInSc4d1	opačný
pól	pól	k1gInSc4	pól
je	být	k5eAaImIp3nS	být
živá	živý	k2eAgFnSc1d1	živá
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
bytost	bytost	k1gFnSc1	bytost
či	či	k8xC	či
mysl	mysl	k1gFnSc1	mysl
s	s	k7c7	s
uměle	uměle	k6eAd1	uměle
upraveným	upravený	k2eAgNnSc7d1	upravené
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obohaceným	obohacený	k2eAgInSc7d1	obohacený
o	o	k7c4	o
mechanické	mechanický	k2eAgFnPc4d1	mechanická
či	či	k8xC	či
elektronické	elektronický	k2eAgFnPc4d1	elektronická
součástky	součástka	k1gFnPc4	součástka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgMnSc2	který
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
mohl	moct	k5eAaImAgInS	moct
zůstat	zůstat	k5eAaPmF	zůstat
i	i	k9	i
jen	jen	k9	jen
mozek	mozek	k1gInSc4	mozek
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
bionika	bionik	k1gMnSc2	bionik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
robot	robot	k1gInSc1	robot
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
pro	pro	k7c4	pro
počítačové	počítačový	k2eAgInPc4d1	počítačový
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
majitele	majitel	k1gMnSc4	majitel
provádí	provádět	k5eAaImIp3nS	provádět
opakované	opakovaný	k2eAgFnPc4d1	opakovaná
činnosti	činnost	k1gFnPc4	činnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
robot	robot	k1gInSc1	robot
(	(	kIx(	(
<g/>
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
problémem	problém	k1gInSc7	problém
pohybu	pohyb	k1gInSc2	pohyb
robota	robot	k1gMnSc2	robot
je	být	k5eAaImIp3nS	být
nespočetné	spočetný	k2eNgNnSc1d1	nespočetné
množství	množství	k1gNnSc1	množství
možností	možnost	k1gFnPc2	možnost
umístění	umístění	k1gNnSc2	umístění
chapadla	chapadlo	k1gNnSc2	chapadlo
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kvůli	kvůli	k7c3	kvůli
následné	následný	k2eAgFnSc3d1	následná
možnosti	možnost	k1gFnSc3	možnost
interakce	interakce	k1gFnSc2	interakce
<g/>
/	/	kIx~	/
<g/>
kolize	kolize	k1gFnSc1	kolize
se	s	k7c7	s
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
konstrukce	konstrukce	k1gFnSc2	konstrukce
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
kloubové	kloubový	k2eAgFnPc1d1	kloubová
úhlové	úhlový	k2eAgFnPc1d1	úhlová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
otočné	otočný	k2eAgInPc1d1	otočný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chapadlo	chapadlo	k1gNnSc4	chapadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teleskopické	teleskopický	k2eAgInPc1d1	teleskopický
<g/>
,	,	kIx,	,
posuvné	posuvný	k2eAgInPc1d1	posuvný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vozík	vozík	k1gInSc1	vozík
na	na	k7c6	na
mostku	mostek	k1gInSc6	mostek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rozhodování	rozhodování	k1gNnSc4	rozhodování
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
nejen	nejen	k6eAd1	nejen
koncového	koncový	k2eAgInSc2d1	koncový
(	(	kIx(	(
<g/>
funkčního	funkční	k2eAgInSc2d1	funkční
<g/>
)	)	kIx)	)
bodu	bod	k1gInSc2	bod
chapadla	chapadlo	k1gNnSc2	chapadlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
klepeta	klepeto	k1gNnSc2	klepeto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
určení	určení	k1gNnSc3	určení
polohy	poloha	k1gFnSc2	poloha
všech	všecek	k3xTgInPc2	všecek
mezilehlých	mezilehlý	k2eAgInPc2d1	mezilehlý
kloubů	kloub	k1gInPc2	kloub
až	až	k9	až
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
základně	základna	k1gFnSc3	základna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
nutnosti	nutnost	k1gFnSc2	nutnost
určování	určování	k1gNnSc2	určování
polohy	poloha	k1gFnSc2	poloha
při	při	k7c6	při
velkých	velký	k2eAgInPc6d1	velký
počtech	počet	k1gInPc6	počet
stupňů	stupeň	k1gInPc2	stupeň
volnosti	volnost	k1gFnSc2	volnost
řeší	řešit	k5eAaImIp3nS	řešit
tzv.	tzv.	kA	tzv.
inverzní	inverzní	k2eAgFnSc1d1	inverzní
kinematická	kinematický	k2eAgFnSc1d1	kinematická
úloha	úloha	k1gFnSc1	úloha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
komplexnost	komplexnost	k1gFnSc1	komplexnost
řízení	řízení	k1gNnSc2	řízení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
pohonů	pohon	k1gInPc2	pohon
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
ovládají	ovládat	k5eAaImIp3nP	ovládat
sekvenčně	sekvenčně	k6eAd1	sekvenčně
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
v	v	k7c6	v
modelovém	modelový	k2eAgInSc6d1	modelový
prostoru	prostor	k1gInSc6	prostor
stavů	stav	k1gInPc2	stav
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
robot	robot	k1gMnSc1	robot
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
všemi	všecek	k3xTgInPc7	všecek
pohony	pohon	k1gInPc7	pohon
najednou	najednou	k6eAd1	najednou
(	(	kIx(	(
<g/>
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
kolmé	kolmý	k2eAgFnSc6d1	kolmá
síti	síť	k1gFnSc6	síť
vs	vs	k?	vs
<g/>
.	.	kIx.	.
úhlopříčně	úhlopříčně	k6eAd1	úhlopříčně
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
:	:	kIx,	:
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
potřeba	potřeba	k6eAd1	potřeba
nejen	nejen	k6eAd1	nejen
schopnost	schopnost	k1gFnSc4	schopnost
mít	mít	k5eAaImF	mít
pohon	pohon	k1gInSc4	pohon
zapnutý	zapnutý	k2eAgInSc4d1	zapnutý
<g/>
/	/	kIx~	/
<g/>
vypnutý	vypnutý	k2eAgInSc4d1	vypnutý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
<g/>
/	/	kIx~	/
<g/>
regulovat	regulovat	k5eAaImF	regulovat
rychlost	rychlost	k1gFnSc4	rychlost
každého	každý	k3xTgInSc2	každý
pohonu	pohon	k1gInSc2	pohon
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
např	např	kA	např
<g/>
:	:	kIx,	:
přímkový	přímkový	k2eAgInSc1d1	přímkový
pohyb	pohyb	k1gInSc1	pohyb
rotačně-kloubového	rotačněloubový	k2eAgNnSc2d1	rotačně-kloubový
chapadla	chapadlo	k1gNnSc2	chapadlo
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
oblý	oblý	k2eAgInSc1d1	oblý
pohyb	pohyb	k1gInSc1	pohyb
můstkového	můstkový	k2eAgInSc2d1	můstkový
vozíku	vozík	k1gInSc2	vozík
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
od	od	k7c2	od
samosvorných	samosvorný	k2eAgInPc2d1	samosvorný
krokových	krokový	k2eAgInPc2d1	krokový
motorků	motorek	k1gInPc2	motorek
dá	dát	k5eAaPmIp3nS	dát
postoupit	postoupit	k5eAaPmF	postoupit
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
volným	volný	k2eAgInPc3d1	volný
pohonům	pohon	k1gInPc3	pohon
sice	sice	k8xC	sice
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
analogově	analogově	k6eAd1	analogově
plynulého	plynulý	k2eAgInSc2d1	plynulý
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zase	zase	k9	zase
vyžadujících	vyžadující	k2eAgFnPc2d1	vyžadující
přesnou	přesný	k2eAgFnSc4d1	přesná
regulaci	regulace	k1gFnSc4	regulace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podrobnějším	podrobný	k2eAgNnSc7d2	podrobnější
povědomím	povědomí	k1gNnSc7	povědomí
o	o	k7c6	o
dynamice	dynamika	k1gFnSc6	dynamika
systému	systém	k1gInSc2	systém
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
pohyb	pohyb	k1gInSc4	pohyb
zefektivnit	zefektivnit	k5eAaPmF	zefektivnit
a	a	k8xC	a
ušetřit	ušetřit	k5eAaPmF	ušetřit
nejen	nejen	k6eAd1	nejen
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
energii	energie	k1gFnSc4	energie
<g/>
:	:	kIx,	:
Např.	např.	kA	např.
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
dolů	dolů	k6eAd1	dolů
musí	muset	k5eAaImIp3nS	muset
samosvorný	samosvorný	k2eAgInSc1d1	samosvorný
pohon	pohon	k1gInSc1	pohon
začít	začít	k5eAaPmF	začít
dodávat	dodávat	k5eAaImF	dodávat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
dynamický	dynamický	k2eAgInSc1d1	dynamický
jí	on	k3xPp3gFnSc3	on
naopak	naopak	k6eAd1	naopak
bude	být	k5eAaImBp3nS	být
dodávat	dodávat	k5eAaImF	dodávat
méně	málo	k6eAd2	málo
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
rekuperovat	rekuperovat	k5eAaBmF	rekuperovat
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
dynamika	dynamika	k1gFnSc1	dynamika
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dále	daleko	k6eAd2	daleko
komplikovat	komplikovat	k5eAaBmF	komplikovat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
plynulejších	plynulý	k2eAgInPc2d2	plynulejší
pohybů	pohyb	k1gInPc2	pohyb
<g/>
:	:	kIx,	:
Od	od	k7c2	od
řízení	řízení	k1gNnSc2	řízení
polohy	poloha	k1gFnSc2	poloha
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
ovládání	ovládání	k1gNnSc4	ovládání
rychlosti	rychlost	k1gFnSc6	rychlost
konstantním	konstantní	k2eAgNnSc7d1	konstantní
zrychlením	zrychlení	k1gNnSc7	zrychlení
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
regulaci	regulace	k1gFnSc4	regulace
zrychlování	zrychlování	k1gNnSc2	zrychlování
na	na	k7c4	na
max	max	kA	max
<g/>
.	.	kIx.	.
povolenou	povolený	k2eAgFnSc7d1	povolená
hodnotou	hodnota	k1gFnSc7	hodnota
až	až	k9	až
po	po	k7c4	po
obecný	obecný	k2eAgInSc4d1	obecný
Taylorův	Taylorův	k2eAgInSc4d1	Taylorův
rozvoj	rozvoj	k1gInSc4	rozvoj
diferenciálu	diferenciál	k1gInSc2	diferenciál
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zvýšením	zvýšení	k1gNnSc7	zvýšení
dynamiky	dynamika	k1gFnSc2	dynamika
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
potřeba	potřeba	k1gFnSc1	potřeba
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
:	:	kIx,	:
Zatímco	zatímco	k8xS	zatímco
primitivnímu	primitivní	k2eAgInSc3d1	primitivní
výtahu	výtah	k1gInSc3	výtah
s	s	k7c7	s
konstantní	konstantní	k2eAgFnSc7d1	konstantní
pojezdovou	pojezdový	k2eAgFnSc7d1	pojezdová
rychlostí	rychlost	k1gFnSc7	rychlost
stačí	stačit	k5eAaBmIp3nS	stačit
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
jen	jen	k9	jen
signální	signální	k2eAgInSc4d1	signální
kontakt	kontakt	k1gInSc4	kontakt
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
,	,	kIx,	,
když	když	k8xS	když
jinak	jinak	k6eAd1	jinak
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
jede	jet	k5eAaImIp3nS	jet
naslepo	naslepo	k6eAd1	naslepo
<g/>
,	,	kIx,	,
dynamické	dynamický	k2eAgInPc4d1	dynamický
stroje	stroj	k1gInPc4	stroj
kvůli	kvůli	k7c3	kvůli
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
a	a	k8xC	a
přesnosti	přesnost	k1gFnSc3	přesnost
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
interagovat	interagovat	k5eAaImF	interagovat
neustále	neustále	k6eAd1	neustále
<g/>
,	,	kIx,	,
např.	např.	kA	např.
CNC	CNC	kA	CNC
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
alespoň	alespoň	k9	alespoň
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
modelem	model	k1gInSc7	model
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zaručení	zaručení	k1gNnSc4	zaručení
přesnosti	přesnost	k1gFnSc2	přesnost
se	se	k3xPyFc4	se
od	od	k7c2	od
dynamiky	dynamika	k1gFnSc2	dynamika
dokonce	dokonce	k9	dokonce
úmyslně	úmyslně	k6eAd1	úmyslně
upouští	upouštět	k5eAaImIp3nS	upouštět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
derivací	derivace	k1gFnPc2	derivace
řízené	řízený	k2eAgFnSc2d1	řízená
veličiny	veličina	k1gFnSc2	veličina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
zpřísněním	zpřísnění	k1gNnSc7	zpřísnění
hodnot	hodnota	k1gFnPc2	hodnota
provozních	provozní	k2eAgInPc2d1	provozní
limitů	limit	k1gInPc2	limit
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pomalý	pomalý	k2eAgInSc4d1	pomalý
dojezd	dojezd	k1gInSc4	dojezd
výtahu	výtah	k1gInSc2	výtah
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
přiblížení	přiblížení	k1gNnSc6	přiblížení
skokově	skokově	k6eAd1	skokově
přepne	přepnout	k5eAaPmIp3nS	přepnout
do	do	k7c2	do
méně	málo	k6eAd2	málo
dynamického	dynamický	k2eAgInSc2d1	dynamický
režimu	režim	k1gInSc2	režim
<g/>
:	:	kIx,	:
Sice	sice	k8xC	sice
pomalejšího	pomalý	k2eAgMnSc2d2	pomalejší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bezpečnějšího	bezpečný	k2eAgInSc2d2	bezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
předvedeno	předvést	k5eAaPmNgNnS	předvést
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
míra	míra	k1gFnSc1	míra
vlastní	vlastní	k2eAgFnSc2d1	vlastní
interakce	interakce	k1gFnSc2	interakce
stroje	stroj	k1gInPc1	stroj
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
mírou	míra	k1gFnSc7	míra
dynamiky	dynamika	k1gFnSc2	dynamika
stroje	stroj	k1gInSc2	stroj
<g/>
:	:	kIx,	:
Čím	co	k3yInSc7	co
dynamičtější	dynamický	k2eAgInSc1d2	dynamičtější
stroj	stroj	k1gInSc1	stroj
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
přesnější	přesný	k2eAgNnSc1d2	přesnější
povědomí	povědomí	k1gNnSc1	povědomí
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
také	také	k9	také
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
/	/	kIx~	/
dovolí	dovolit	k5eAaPmIp3nS	dovolit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
méně	málo	k6eAd2	málo
zásahů	zásah	k1gInPc2	zásah
lidské	lidský	k2eAgFnSc2d1	lidská
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
lze	lze	k6eAd1	lze
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
různé	různý	k2eAgFnPc4d1	různá
autonomii	autonomie	k1gFnSc4	autonomie
stroje	stroj	k1gInSc2	stroj
na	na	k7c6	na
člověku	člověk	k1gMnSc6	člověk
<g/>
:	:	kIx,	:
Řízený	řízený	k2eAgInSc4d1	řízený
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgNnSc4d1	přímé
vedení	vedení	k1gNnSc4	vedení
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
rozhodovací	rozhodovací	k2eAgFnSc2d1	rozhodovací
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
člověka	člověk	k1gMnSc2	člověk
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výtah	výtah	k1gInSc4	výtah
jede	jet	k5eAaImIp3nS	jet
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
stisknutém	stisknutý	k2eAgNnSc6d1	stisknuté
tlačítku	tlačítko	k1gNnSc6	tlačítko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
činnost	činnost	k1gFnSc4	činnost
podle	podle	k7c2	podle
zadaného	zadaný	k2eAgInSc2d1	zadaný
pokynu	pokyn	k1gInSc2	pokyn
<g/>
,	,	kIx,	,
logická	logický	k2eAgFnSc1d1	logická
rozhodovací	rozhodovací	k2eAgFnSc1d1	rozhodovací
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
,	,	kIx,	,
konečný	konečný	k2eAgInSc1d1	konečný
automat	automat	k1gInSc1	automat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výtah	výtah	k1gInSc1	výtah
zastaví	zastavit	k5eAaPmIp3nS	zastavit
až	až	k9	až
v	v	k7c6	v
požadovaném	požadovaný	k2eAgNnSc6d1	požadované
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
inteligence	inteligence	k1gFnSc1	inteligence
s	s	k7c7	s
pamětí	paměť	k1gFnSc7	paměť
jednoho	jeden	k4xCgInSc2	jeden
bitu	bit	k1gInSc2	bit
<g/>
,	,	kIx,	,
přídržné	přídržný	k2eAgNnSc1d1	přídržné
tlačítko	tlačítko	k1gNnSc1	tlačítko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Regulovaný	regulovaný	k2eAgInSc1d1	regulovaný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
cíle	cíl	k1gInPc4	cíl
předem	předem	k6eAd1	předem
určeným	určený	k2eAgInSc7d1	určený
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
cíle	cíl	k1gInPc4	cíl
za	za	k7c2	za
různých	různý	k2eAgFnPc2d1	různá
podmínek	podmínka	k1gFnPc2	podmínka
různými	různý	k2eAgFnPc7d1	různá
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
analogové	analogový	k2eAgNnSc1d1	analogové
rozlišení	rozlišení	k1gNnSc1	rozlišení
míry	míra	k1gFnSc2	míra
intenzity	intenzita	k1gFnSc2	intenzita
jevu	jev	k1gInSc2	jev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výtah	výtah	k1gInSc4	výtah
<g/>
,	,	kIx,	,
při	při	k7c6	při
náhlé	náhlý	k2eAgFnSc6d1	náhlá
volbě	volba	k1gFnSc6	volba
nové	nový	k2eAgFnSc2d1	nová
cílové	cílový	k2eAgFnSc2d1	cílová
stanice	stanice	k1gFnSc2	stanice
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
raději	rád	k6eAd2	rád
přejede	přejet	k5eAaPmIp3nS	přejet
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nezastaví	zastavit	k5eNaPmIp3nS	zastavit
hned	hned	k6eAd1	hned
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
cestující	cestující	k1gMnPc1	cestující
nepodklesnou	podklesnout	k5eNaPmIp3nP	podklesnout
v	v	k7c6	v
kolenou	koleno	k1gNnPc6	koleno
ani	ani	k8xC	ani
neposkočí	poskočit	k5eNaPmIp3nP	poskočit
s	s	k7c7	s
žaludkem	žaludek	k1gInSc7	žaludek
v	v	k7c6	v
krku	krk	k1gInSc6	krk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autonomní	autonomní	k2eAgInSc1d1	autonomní
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
cíle	cíl	k1gInSc2	cíl
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
zvolí	zvolit	k5eAaPmIp3nS	zvolit
(	(	kIx(	(
<g/>
metodologie	metodologie	k1gFnSc1	metodologie
volby	volba	k1gFnSc2	volba
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
předepsána	předepsán	k2eAgFnSc1d1	předepsána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
může	moct	k5eAaImIp3nS	moct
držet	držet	k5eAaImF	držet
nejpřímější	přímý	k2eAgFnPc4d3	nejpřímější
předpokládané	předpokládaný	k2eAgFnPc4d1	předpokládaná
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nijak	nijak	k6eAd1	nijak
jí	on	k3xPp3gFnSc3	on
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
si	se	k3xPyFc3	se
jí	on	k3xPp3gFnSc2	on
znovu	znovu	k6eAd1	znovu
ověřuje	ověřovat	k5eAaImIp3nS	ověřovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
překážek	překážka	k1gFnPc2	překážka
i	i	k8xC	i
sám	sám	k3xTgInSc1	sám
hledá	hledat	k5eAaImIp3nS	hledat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
limitu	limit	k1gInSc2	limit
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
původního	původní	k2eAgInSc2d1	původní
přímého	přímý	k2eAgInSc2d1	přímý
směru	směr	k1gInSc2	směr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
algoritmus	algoritmus	k1gInSc1	algoritmus
A	a	k9	a
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inteligentní	inteligentní	k2eAgInSc1d1	inteligentní
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
sám	sám	k3xTgInSc1	sám
si	se	k3xPyFc3	se
volí	volit	k5eAaImIp3nS	volit
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
člověka	člověk	k1gMnSc4	člověk
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
utopie	utopie	k1gFnSc1	utopie
<g/>
:	:	kIx,	:
Hraniční	hraniční	k2eAgInSc1d1	hraniční
výsledek	výsledek	k1gInSc1	výsledek
oboru	obor	k1gInSc2	obor
umělé	umělý	k2eAgFnSc2d1	umělá
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vůbec	vůbec	k9	vůbec
prvního	první	k4xOgMnSc4	první
robota	robot	k1gMnSc4	robot
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
soustavu	soustava	k1gFnSc4	soustava
radar-počítač-kanóny	radaročítačanóna	k1gFnSc2	radar-počítač-kanóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bojové	bojový	k2eAgNnSc1d1	bojové
užití	užití	k1gNnSc1	užití
počítačů	počítač	k1gInPc2	počítač
bylo	být	k5eAaImAgNnS	být
prvotní	prvotní	k2eAgFnSc7d1	prvotní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
ENIAC	ENIAC	kA	ENIAC
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
systémy	systém	k1gInPc1	systém
řízení	řízení	k1gNnSc2	řízení
palby	palba	k1gFnSc2	palba
na	na	k7c6	na
palubách	paluba	k1gFnPc6	paluba
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Gun	Gun	k1gFnSc1	Gun
Fire	Fir	k1gFnSc2	Fir
Control	Controla	k1gFnPc2	Controla
System	Systo	k1gNnSc7	Systo
(	(	kIx(	(
<g/>
GFCS	GFCS	kA	GFCS
<g/>
)	)	kIx)	)
na	na	k7c6	na
těch	ten	k3xDgInPc6	ten
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
robot	robot	k1gInSc1	robot
Unimate	Unimat	k1gInSc5	Unimat
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Unimation	Unimation	k1gInSc1	Unimation
byl	být	k5eAaImAgInS	být
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
na	na	k7c6	na
výrobní	výrobní	k2eAgFnSc6d1	výrobní
lince	linka	k1gFnSc6	linka
General	General	k1gMnPc2	General
Motors	Motorsa	k1gFnPc2	Motorsa
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
převzala	převzít	k5eAaPmAgFnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
americkou	americký	k2eAgFnSc7d1	americká
společností	společnost	k1gFnSc7	společnost
Unimation	Unimation	k1gInSc1	Unimation
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
firma	firma	k1gFnSc1	firma
Stäubli	Stäubli	k1gFnSc1	Stäubli
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
robotů	robot	k1gInPc2	robot
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
robotiky	robotika	k1gFnSc2	robotika
brzy	brzy	k6eAd1	brzy
převzalo	převzít	k5eAaPmAgNnS	převzít
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
neuznávalo	uznávat	k5eNaImAgNnS	uznávat
patenty	patent	k1gInPc4	patent
Unimate	Unimat	k1gInSc5	Unimat
registrované	registrovaný	k2eAgFnPc4d1	registrovaná
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
Japonsko	Japonsko	k1gNnSc1	Japonsko
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
manipulátorů	manipulátor	k1gInPc2	manipulátor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
humanoidní	humanoidní	k2eAgFnSc1d1	humanoidní
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejdokonalejších	dokonalý	k2eAgMnPc2d3	nejdokonalejší
robotů	robot	k1gInPc2	robot
humanoidní	humanoidní	k2eAgFnSc2d1	humanoidní
konstrukce	konstrukce	k1gFnSc2	konstrukce
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
robota	robot	k1gMnSc4	robot
týmu	tým	k1gInSc2	tým
SCHAFT	SCHAFT	kA	SCHAFT
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
DARPA	DARPA	kA	DARPA
Robotics	Robotics	k1gInSc4	Robotics
Challenge	Challeng	k1gInSc2	Challeng
<g/>
.	.	kIx.	.
</s>
<s>
Těžko	těžko	k6eAd1	těžko
si	se	k3xPyFc3	se
představit	představit	k5eAaPmF	představit
obor	obor	k1gInSc4	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
roboty	robot	k1gInPc1	robot
nemohly	moct	k5eNaImAgInP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
dnes	dnes	k6eAd1	dnes
působí	působit	k5eAaImIp3nS	působit
např.	např.	kA	např.
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
<g/>
:	:	kIx,	:
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
manipulátory	manipulátor	k1gInPc1	manipulátor
<g/>
,	,	kIx,	,
dopravníková	dopravníkový	k2eAgFnSc1d1	dopravníková
soustavy	soustava	k1gFnPc1	soustava
<g/>
,	,	kIx,	,
lakovny	lakovna	k1gFnPc1	lakovna
<g/>
,	,	kIx,	,
svařovny	svařovna	k1gFnPc1	svařovna
<g/>
.	.	kIx.	.
průzkumy	průzkum	k1gInPc1	průzkum
a	a	k8xC	a
manipulace	manipulace	k1gFnSc1	manipulace
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
:	:	kIx,	:
Záchranářské	záchranářský	k2eAgFnSc2d1	záchranářská
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgInSc1d1	vojenský
průzkum	průzkum	k1gInSc1	průzkum
<g/>
,	,	kIx,	,
pyrotechnika	pyrotechnika	k1gFnSc1	pyrotechnika
<g/>
,	,	kIx,	,
potrubí	potrubí	k1gNnSc1	potrubí
<g/>
,	,	kIx,	,
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
teleskop	teleskop	k1gInSc1	teleskop
<g/>
<g />
.	.	kIx.	.
</s>
<s>
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
:	:	kIx,	:
operace	operace	k1gFnSc1	operace
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
,	,	kIx,	,
protetika	protetika	k1gFnSc1	protetika
<g/>
.	.	kIx.	.
osobní	osobní	k2eAgFnSc1d1	osobní
výpomoc	výpomoc	k1gFnSc1	výpomoc
<g/>
:	:	kIx,	:
domácí	domácí	k2eAgInSc1d1	domácí
vysavač	vysavač	k1gInSc1	vysavač
<g/>
,	,	kIx,	,
robotický	robotický	k2eAgMnSc1d1	robotický
administrativní	administrativní	k2eAgMnSc1d1	administrativní
asistent	asistent	k1gMnSc1	asistent
v	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
.	.	kIx.	.
kybersport	kybersport	k1gInSc4	kybersport
<g/>
:	:	kIx,	:
robofotbal	robofotbal	k1gInSc1	robofotbal
<g/>
.	.	kIx.	.
doprava	doprava	k1gFnSc1	doprava
<g/>
:	:	kIx,	:
letecký	letecký	k2eAgMnSc1d1	letecký
autopilot	autopilot	k1gMnSc1	autopilot
(	(	kIx(	(
<g/>
robotem	robot	k1gMnSc7	robot
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
celé	celý	k2eAgNnSc4d1	celé
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolejové	kolejový	k2eAgFnSc2d1	kolejová
<g />
.	.	kIx.	.
</s>
<s>
vozy	vůz	k1gInPc1	vůz
bez	bez	k7c2	bez
řidiče	řidič	k1gInSc2	řidič
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
samořízeného	samořízený	k2eAgInSc2d1	samořízený
automobilu	automobil	k1gInSc2	automobil
<g/>
.	.	kIx.	.
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
pole	pole	k1gNnSc2	pole
působení	působení	k1gNnSc2	působení
robotů	robot	k1gInPc2	robot
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
značně	značně	k6eAd1	značně
distribuované	distribuovaný	k2eAgInPc4d1	distribuovaný
systémy	systém	k1gInPc4	systém
<g/>
:	:	kIx,	:
Městské	městský	k2eAgInPc1d1	městský
semafory	semafor	k1gInPc1	semafor
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
dopravy	doprava	k1gFnSc2	doprava
např.	např.	kA	např.
v	v	k7c6	v
pražských	pražský	k2eAgInPc6d1	pražský
tunelech	tunel	k1gInPc6	tunel
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
závor	závora	k1gFnPc2	závora
a	a	k8xC	a
poloautonomních	poloautonomní	k2eAgFnPc2d1	poloautonomní
informačních	informační	k2eAgFnPc2d1	informační
tabulí	tabule	k1gFnPc2	tabule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
přímé	přímý	k2eAgNnSc4d1	přímé
programování	programování	k1gNnSc4	programování
<g />
.	.	kIx.	.
</s>
<s>
vedením	vedení	k1gNnSc7	vedení
robotova	robotův	k2eAgNnSc2d1	robotův
ramena	rameno	k1gNnSc2	rameno
(	(	kIx(	(
<g/>
teach-in	teachn	k1gInSc1	teach-in
<g/>
)	)	kIx)	)
zadáváním	zadávání	k1gNnSc7	zadávání
povelů	povel	k1gInPc2	povel
z	z	k7c2	z
ovládacího	ovládací	k2eAgInSc2d1	ovládací
panelu	panel	k1gInSc2	panel
nepřímé	přímý	k2eNgNnSc1d1	nepřímé
programování	programování	k1gNnSc1	programování
(	(	kIx(	(
<g/>
off-line	offin	k1gInSc5	off-lin
<g/>
)	)	kIx)	)
-	-	kIx~	-
zadáváme	zadávat	k5eAaImIp1nP	zadávat
prostorové	prostorový	k2eAgFnPc1d1	prostorová
křivky	křivka	k1gFnPc1	křivka
(	(	kIx(	(
<g/>
získané	získaný	k2eAgInPc1d1	získaný
z	z	k7c2	z
výkresů	výkres	k1gInPc2	výkres
<g/>
)	)	kIx)	)
plánování	plánování	k1gNnSc1	plánování
(	(	kIx(	(
<g/>
on-line	onin	k1gInSc5	on-lin
<g/>
)	)	kIx)	)
-	-	kIx~	-
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
předchozí	předchozí	k2eAgMnSc1d1	předchozí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
robot	robot	k1gInSc1	robot
se	se	k3xPyFc4	se
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
měnícím	měnící	k2eAgMnSc7d1	měnící
se	s	k7c7	s
vnějším	vnější	k2eAgMnSc7d1	vnější
<g />
.	.	kIx.	.
</s>
<s>
podmínkám	podmínka	k1gFnPc3	podmínka
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
čidel	čidlo	k1gNnPc2	čidlo
<g/>
)	)	kIx)	)
Zadávání	zadávání	k1gNnSc3	zadávání
pozice	pozice	k1gFnSc2	pozice
ramena	rameno	k1gNnSc2	rameno
může	moct	k5eAaImIp3nS	moct
principiálně	principiálně	k6eAd1	principiálně
probíhat	probíhat	k5eAaImF	probíhat
2	[number]	k4	2
způsoby	způsob	k1gInPc4	způsob
<g/>
:	:	kIx,	:
spojitá	spojitý	k2eAgFnSc1d1	spojitá
trasa	trasa	k1gFnSc1	trasa
(	(	kIx(	(
<g/>
continuos	continuos	k1gMnSc1	continuos
path	path	k1gMnSc1	path
<g/>
)	)	kIx)	)
-	-	kIx~	-
zadání	zadání	k1gNnSc1	zadání
přesné	přesný	k2eAgFnSc2d1	přesná
pozice	pozice	k1gFnSc2	pozice
ramena	rameno	k1gNnSc2	rameno
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
okamžicích	okamžik	k1gInPc6	okamžik
činností	činnost	k1gFnSc7	činnost
robota	robot	k1gMnSc2	robot
(	(	kIx(	(
<g/>
vedení	vedení	k1gNnSc1	vedení
ramena	rameno	k1gNnSc2	rameno
<g/>
,	,	kIx,	,
nepřímé	přímý	k2eNgNnSc1d1	nepřímé
programování	programování	k1gNnSc1	programování
<g/>
,	,	kIx,	,
přímé	přímý	k2eAgNnSc1d1	přímé
plánování	plánování	k1gNnSc1	plánování
<g/>
)	)	kIx)	)
od	od	k7c2	od
bodu	bod	k1gInSc2	bod
k	k	k7c3	k
bodu	bod	k1gInSc3	bod
(	(	kIx(	(
<g/>
point-to-point	pointooint	k1gMnSc1	point-to-point
<g/>
,	,	kIx,	,
way-point	wayoint	k1gMnSc1	way-point
<g/>
)	)	kIx)	)
-	-	kIx~	-
zadání	zadání	k1gNnSc1	zadání
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
rameno	rameno	k1gNnSc1	rameno
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
konkrétních	konkrétní	k2eAgInPc6d1	konkrétní
časech	čas	k1gInPc6	čas
činnosti	činnost	k1gFnSc2	činnost
nacházet	nacházet	k5eAaImF	nacházet
(	(	kIx(	(
<g/>
zadávání	zadávání	k1gNnSc4	zadávání
z	z	k7c2	z
ovládacího	ovládací	k2eAgInSc2d1	ovládací
panelu	panel	k1gInSc2	panel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
se	se	k3xPyFc4	se
bezproblémové	bezproblémový	k2eAgInPc1d1	bezproblémový
přímé	přímý	k2eAgInPc1d1	přímý
přechody	přechod	k1gInPc1	přechod
mezi	mezi	k7c7	mezi
elementárními	elementární	k2eAgFnPc7d1	elementární
pozicemi	pozice	k1gFnPc7	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
okolí	okolí	k1gNnSc6	okolí
využívají	využívat	k5eAaImIp3nP	využívat
roboty	robot	k1gInPc1	robot
různé	různý	k2eAgInPc1d1	různý
senzory	senzor	k1gInPc1	senzor
<g/>
.	.	kIx.	.
dotykové	dotykový	k2eAgFnPc4d1	dotyková
pružinová	pružinový	k2eAgNnPc4d1	pružinové
tykadla	tykadlo	k1gNnPc4	tykadlo
s	s	k7c7	s
mikrospínači	mikrospínač	k1gInPc7	mikrospínač
na	na	k7c4	na
detekci	detekce	k1gFnSc4	detekce
jejich	jejich	k3xOp3gNnSc2	jejich
ohnutí	ohnutí	k1gNnSc2	ohnutí
distanční	distanční	k2eAgFnSc4d1	distanční
sonarovou	sonarový	k2eAgFnSc4d1	sonarový
echolokaci	echolokace	k1gFnSc4	echolokace
laserová	laserový	k2eAgFnSc1d1	laserová
dálkoměry	dálkoměr	k1gInPc4	dálkoměr
vizuální	vizuální	k2eAgFnSc2d1	vizuální
prosté	prostý	k2eAgFnSc2d1	prostá
kamery	kamera	k1gFnSc2	kamera
stereo	stereo	k2eAgNnSc2d1	stereo
vidění	vidění	k1gNnSc2	vidění
panoramatické	panoramatický	k2eAgFnSc2d1	panoramatická
kamery	kamera	k1gFnSc2	kamera
hyperbolická	hyperbolický	k2eAgNnPc4d1	hyperbolické
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
radionavigaci	radionavigace	k1gFnSc3	radionavigace
s	s	k7c7	s
triangulací	triangulace	k1gFnSc7	triangulace
GPS	GPS	kA	GPS
I	i	k8xC	i
když	když	k8xS	když
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
jediné	jediný	k2eAgNnSc4d1	jediné
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
anebo	anebo	k8xC	anebo
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
svou	svůj	k3xOyFgFnSc4	svůj
polohu	poloha	k1gFnSc4	poloha
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
stacionární	stacionární	k2eAgMnPc1d1	stacionární
roboti	robot	k1gMnPc1	robot
stále	stále	k6eAd1	stále
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
konfigurace	konfigurace	k1gFnSc2	konfigurace
robota	robot	k1gMnSc2	robot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
tři	tři	k4xCgInPc4	tři
stupně	stupeň	k1gInPc4	stupeň
volnosti	volnost	k1gFnSc2	volnost
<g/>
:	:	kIx,	:
Descartes	Descartes	k1gMnSc1	Descartes
-	-	kIx~	-
všechna	všechen	k3xTgFnSc1	všechen
uložení	uložení	k1gNnSc4	uložení
posuvná	posuvný	k2eAgFnSc1d1	posuvná
PUMA	puma	k1gFnSc1	puma
uložení	uložení	k1gNnSc2	uložení
-	-	kIx~	-
všechna	všechen	k3xTgFnSc1	všechen
uložení	uložení	k1gNnSc4	uložení
na	na	k7c6	na
otočných	otočný	k2eAgInPc6d1	otočný
kloubech	kloub	k1gInPc6	kloub
válcové	válcový	k2eAgFnPc1d1	válcová
-	-	kIx~	-
dvě	dva	k4xCgNnPc1	dva
posuvná	posuvný	k2eAgNnPc1d1	posuvné
uložení	uložení	k1gNnPc1	uložení
na	na	k7c6	na
otočné	otočný	k2eAgFnSc6d1	otočná
základně	základna	k1gFnSc6	základna
SCARA	SCARA	kA	SCARA
-	-	kIx~	-
posuvné	posuvný	k2eAgNnSc1d1	posuvné
chapadlo	chapadlo	k1gNnSc1	chapadlo
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
otočných	otočný	k2eAgInPc6d1	otočný
kloubech	kloub	k1gInPc6	kloub
...	...	k?	...
<g/>
i	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
další	další	k2eAgNnSc4d1	další
uložení	uložení	k1gNnSc4	uložení
<g/>
,	,	kIx,	,
i	i	k8xC	i
složitější	složitý	k2eAgMnSc1d2	složitější
Podle	podle	k7c2	podle
využití	využití	k1gNnSc2	využití
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
<g/>
:	:	kIx,	:
bodové	bodový	k2eAgNnSc1d1	bodové
sváření	sváření	k1gNnSc1	sváření
/	/	kIx~	/
<g/>
spot	spot	k1gInSc1	spot
welding	welding	k1gInSc1	welding
<g/>
/	/	kIx~	/
-	-	kIx~	-
svařování	svařování	k1gNnSc1	svařování
karoserie	karoserie	k1gFnSc2	karoserie
automobilu	automobil	k1gInSc2	automobil
ARC	ARC	kA	ARC
welding	welding	k1gInSc1	welding
/	/	kIx~	/
<g/>
ARC	ARC	kA	ARC
welding	welding	k1gInSc1	welding
<g/>
/	/	kIx~	/
-	-	kIx~	-
souvislé	souvislý	k2eAgNnSc1d1	souvislé
sváření	sváření	k1gNnSc1	sváření
montáž	montáž	k1gFnSc1	montáž
/	/	kIx~	/
<g/>
assembly	assembnout	k5eAaPmAgFnP	assembnout
<g/>
/	/	kIx~	/
-	-	kIx~	-
instalace	instalace	k1gFnSc1	instalace
a	a	k8xC	a
kompletace	kompletace	k1gFnSc1	kompletace
aplikace	aplikace	k1gFnSc2	aplikace
/	/	kIx~	/
<g/>
application	application	k1gInSc1	application
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
-	-	kIx~	-
nanášení	nanášení	k1gNnSc1	nanášení
lepidel	lepidlo	k1gNnPc2	lepidlo
<g/>
,	,	kIx,	,
těsniv	těsnit	k5eAaPmDgInS	těsnit
<g/>
,	,	kIx,	,
tlumiv	tlumit	k5eAaPmDgInS	tlumit
lakování	lakování	k1gNnSc4	lakování
/	/	kIx~	/
<g/>
painting	painting	k1gInSc1	painting
<g/>
/	/	kIx~	/
-	-	kIx~	-
stříkání	stříkání	k1gNnSc1	stříkání
tekutých	tekutý	k2eAgFnPc2d1	tekutá
a	a	k8xC	a
práškových	práškový	k2eAgFnPc2d1	prášková
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
laků	lak	k1gInPc2	lak
manipulace	manipulace	k1gFnSc2	manipulace
/	/	kIx~	/
<g/>
handling	handling	k1gInSc1	handling
<g/>
/	/	kIx~	/
-	-	kIx~	-
překládání	překládání	k1gNnSc1	překládání
<g/>
,	,	kIx,	,
<g/>
nakládání	nakládání	k1gNnSc1	nakládání
a	a	k8xC	a
vykládání	vykládání	k1gNnSc1	vykládání
pro	pro	k7c4	pro
dopravníky	dopravník	k1gInPc4	dopravník
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
:	:	kIx,	:
pro	pro	k7c4	pro
simulace	simulace	k1gFnPc4	simulace
pohybu	pohyb	k1gInSc2	pohyb
modelu	model	k1gInSc2	model
v	v	k7c6	v
aerodynamickém	aerodynamický	k2eAgInSc6d1	aerodynamický
tunelu	tunel	k1gInSc6	tunel
<g/>
.	.	kIx.	.
paletizace	paletizace	k1gFnSc1	paletizace
/	/	kIx~	/
<g/>
palletizing	palletizing	k1gInSc1	palletizing
<g/>
/	/	kIx~	/
-	-	kIx~	-
skládání	skládání	k1gNnSc1	skládání
nebo	nebo	k8xC	nebo
vykládání	vykládání	k1gNnSc1	vykládání
výrobků	výrobek	k1gInPc2	výrobek
na	na	k7c6	na
paletách	paleta	k1gFnPc6	paleta
kontrola	kontrola	k1gFnSc1	kontrola
/	/	kIx~	/
<g/>
checking	checking	k1gInSc1	checking
<g/>
/	/	kIx~	/
-	-	kIx~	-
měření	měření	k1gNnSc1	měření
pomocí	pomocí	k7c2	pomocí
kamer	kamera	k1gFnPc2	kamera
<g/>
,	,	kIx,	,
laseru	laser	k1gInSc2	laser
a	a	k8xC	a
čidel	čidlo	k1gNnPc2	čidlo
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
manipulátory	manipulátor	k1gInPc1	manipulátor
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
hojně	hojně	k6eAd1	hojně
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
aplikovat	aplikovat	k5eAaBmF	aplikovat
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
výrobních	výrobní	k2eAgInPc2d1	výrobní
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
využívají	využívat	k5eAaPmIp3nP	využívat
většinou	většina	k1gFnSc7	většina
6	[number]	k4	6
os	osa	k1gFnPc2	osa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
osa	osa	k1gFnSc1	osa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
použita	použít	k5eAaPmNgFnS	použít
pro	pro	k7c4	pro
přesun	přesun	k1gInSc4	přesun
po	po	k7c6	po
koleji	kolej	k1gFnSc6	kolej
kdy	kdy	k6eAd1	kdy
robot	robot	k1gMnSc1	robot
popojíždí	popojíždět	k5eAaImIp3nS	popojíždět
vedle	vedle	k7c2	vedle
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
synchronizován	synchronizován	k2eAgMnSc1d1	synchronizován
s	s	k7c7	s
dopravníkem	dopravník	k1gInSc7	dopravník
a	a	k8xC	a
po	po	k7c6	po
vykonání	vykonání	k1gNnSc6	vykonání
úlohy	úloha	k1gFnSc2	úloha
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
sevření	sevření	k1gNnSc4	sevření
kleští	kleště	k1gFnPc2	kleště
při	při	k7c6	při
bodovém	bodový	k2eAgInSc6d1	bodový
<g />
.	.	kIx.	.
</s>
<s>
sváření	sváření	k1gNnSc4	sváření
<g/>
.	.	kIx.	.
tiskárny	tiskárna	k1gFnPc4	tiskárna
<g/>
,	,	kIx,	,
plottery	plotter	k1gInPc4	plotter
a	a	k8xC	a
rýsovače	rýsovač	k1gMnPc4	rýsovač
laserové	laserový	k2eAgNnSc1d1	laserové
nebo	nebo	k8xC	nebo
plazmové	plazmový	k2eAgNnSc1d1	plazmové
vypalování	vypalování	k1gNnSc1	vypalování
z	z	k7c2	z
plechu	plech	k1gInSc2	plech
jeřáby	jeřáb	k1gMnPc4	jeřáb
autonomní	autonomní	k2eAgMnPc4d1	autonomní
dálkově	dálkově	k6eAd1	dálkově
ovládané	ovládaný	k2eAgMnPc4d1	ovládaný
-	-	kIx~	-
pracuje	pracovat	k5eAaImIp3nS	pracovat
podle	podle	k7c2	podle
průběžných	průběžný	k2eAgInPc2d1	průběžný
pokynů	pokyn	k1gInPc2	pokyn
operátora	operátor	k1gMnSc2	operátor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získává	získávat	k5eAaImIp3nS	získávat
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
virtuální	virtuální	k2eAgFnSc2d1	virtuální
reality	realita	k1gFnSc2	realita
nebezpečnost	nebezpečnost	k1gFnSc1	nebezpečnost
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
)	)	kIx)	)
místa	místo	k1gNnSc2	místo
působení	působení	k1gNnSc2	působení
-	-	kIx~	-
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
nedostupnost	nedostupnost	k1gFnSc1	nedostupnost
místa	místo	k1gNnSc2	místo
působení	působení	k1gNnSc2	působení
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
potrubí	potrubí	k1gNnPc4	potrubí
<g/>
,	,	kIx,	,
jiná	jiný	k2eAgNnPc4d1	jiné
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
odstranění	odstranění	k1gNnSc2	odstranění
monotónních	monotónní	k2eAgFnPc2d1	monotónní
prací	práce	k1gFnPc2	práce
Dělíme	dělit	k5eAaImIp1nP	dělit
podle	podle	k7c2	podle
hlediska	hledisko	k1gNnSc2	hledisko
vzoru	vzor	k1gInSc3	vzor
vzniku	vznik	k1gInSc2	vznik
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
biologické	biologický	k2eAgFnSc2d1	biologická
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
kráčející	kráčející	k2eAgFnSc2d1	kráčející
<g/>
,	,	kIx,	,
plazivé	plazivý	k2eAgFnSc2d1	plazivá
<g/>
,	,	kIx,	,
šplhající	šplhající	k2eAgFnSc2d1	šplhající
<g/>
,	,	kIx,	,
létající	létající	k2eAgFnSc2d1	létající
roboty	robota	k1gFnSc2	robota
<g/>
)	)	kIx)	)
a	a	k8xC	a
umělé	umělý	k2eAgFnSc2d1	umělá
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
kolové	kolová	k1gFnSc2	kolová
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
pásové	pásový	k2eAgFnPc4d1	pásová
<g/>
,	,	kIx,	,
polštářové	polštářový	k2eAgFnPc4d1	polštářová
roboty	robota	k1gFnPc4	robota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
diferenciální	diferenciální	k2eAgInSc1d1	diferenciální
podvozek	podvozek	k1gInSc1	podvozek
-	-	kIx~	-
dvě	dva	k4xCgNnPc4	dva
hnaná	hnaný	k2eAgNnPc4d1	hnané
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
rovnováha	rovnováha	k1gFnSc1	rovnováha
udržována	udržovat	k5eAaImNgFnS	udržovat
opěrnými	opěrný	k2eAgInPc7d1	opěrný
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pasivním	pasivní	k2eAgNnSc7d1	pasivní
kolem	kolo	k1gNnSc7	kolo
(	(	kIx(	(
<g/>
koly	kola	k1gFnSc2	kola
<g/>
)	)	kIx)	)
synchronní	synchronní	k2eAgInSc1d1	synchronní
podvozek	podvozek	k1gInSc1	podvozek
-	-	kIx~	-
často	často	k6eAd1	často
3	[number]	k4	3
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
se	s	k7c7	s
2	[number]	k4	2
stupni	stupeň	k1gInPc7	stupeň
volnosti	volnost	k1gFnSc2	volnost
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
otáčet	otáčet	k5eAaImF	otáčet
<g />
.	.	kIx.	.
</s>
<s>
i	i	k9	i
natáčet	natáčet	k5eAaImF	natáčet
<g/>
)	)	kIx)	)
trojkolový	trojkolový	k2eAgInSc4d1	trojkolový
podvozek	podvozek	k1gInSc4	podvozek
s	s	k7c7	s
řízeným	řízený	k2eAgNnSc7d1	řízené
předním	přední	k2eAgNnSc7d1	přední
kolem	kolo	k1gNnSc7	kolo
-	-	kIx~	-
2	[number]	k4	2
hnaná	hnaný	k2eAgFnSc1d1	hnaná
kola	kola	k1gFnSc1	kola
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
motoricky	motoricky	k6eAd1	motoricky
natáčené	natáčený	k2eAgFnSc2d1	natáčená
Ackermanův	Ackermanův	k2eAgInSc4d1	Ackermanův
podvozek	podvozek	k1gInSc4	podvozek
-	-	kIx~	-
4	[number]	k4	4
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
2	[number]	k4	2
natáčená	natáčený	k2eAgFnSc1d1	natáčená
kola	kola	k1gFnSc1	kola
(	(	kIx(	(
<g/>
každé	každý	k3xTgNnSc4	každý
mírně	mírně	k6eAd1	mírně
jinak	jinak	k6eAd1	jinak
-	-	kIx~	-
vnitřní	vnitřní	k2eAgMnSc1d1	vnitřní
více	hodně	k6eAd2	hodně
a	a	k8xC	a
vnější	vnější	k2eAgFnPc1d1	vnější
méně	málo	k6eAd2	málo
-	-	kIx~	-
protože	protože	k8xS	protože
každé	každý	k3xTgNnSc1	každý
při	při	k7c6	při
zatáčení	zatáčení	k1gNnSc6	zatáčení
opisuje	opisovat	k5eAaImIp3nS	opisovat
<g />
.	.	kIx.	.
</s>
<s>
jinou	jiný	k2eAgFnSc4d1	jiná
dráhu	dráha	k1gFnSc4	dráha
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
podvozky	podvozek	k1gInPc1	podvozek
mají	mít	k5eAaImIp3nP	mít
běžné	běžný	k2eAgInPc1d1	běžný
automobily	automobil	k1gInPc1	automobil
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
podvozek	podvozek	k1gInSc4	podvozek
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
nezávisle	závisle	k6eNd1	závisle
poháněnými	poháněný	k2eAgNnPc7d1	poháněné
koly	kolo	k1gNnPc7	kolo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
osy	osa	k1gFnPc1	osa
procházejí	procházet	k5eAaImIp3nP	procházet
těžištěm	těžiště	k1gNnSc7	těžiště
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
povrch	povrch	k1gInSc4	povrch
(	(	kIx(	(
<g/>
složený	složený	k2eAgMnSc1d1	složený
obvykle	obvykle	k6eAd1	obvykle
z	z	k7c2	z
malých	malý	k2eAgNnPc2d1	malé
koleček	kolečko	k1gNnPc2	kolečko
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
volný	volný	k2eAgInSc4d1	volný
skluz	skluz	k1gInSc4	skluz
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
osy	osa	k1gFnSc2	osa
podvozky	podvozek	k1gInPc1	podvozek
se	s	k7c7	s
všesměrovými	všesměrový	k2eAgNnPc7d1	všesměrové
koly	kolo	k1gNnPc7	kolo
pásové	pásový	k2eAgInPc1d1	pásový
podvozky	podvozek	k1gInPc1	podvozek
kráčející	kráčející	k2eAgInPc1d1	kráčející
podvozky	podvozek	k1gInPc1	podvozek
Nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
se	se	k3xPyFc4	se
mobilní	mobilní	k2eAgFnSc3d1	mobilní
robotice	robotika	k1gFnSc3	robotika
využívají	využívat	k5eAaImIp3nP	využívat
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
elektromotorů	elektromotor	k1gInPc2	elektromotor
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
<g/>
:	:	kIx,	:
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
motor	motor	k1gInSc4	motor
(	(	kIx(	(
<g/>
DC	DC	kA	DC
motor	motor	k1gInSc1	motor
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejjednodušší	jednoduchý	k2eAgNnSc4d3	nejjednodušší
použití	použití	k1gNnSc4	použití
komutátorové	komutátorový	k2eAgNnSc4d1	komutátorový
bezkomutátorové	bezkomutátorový	k2eAgFnPc4d1	bezkomutátorový
krokové	krokový	k2eAgFnPc4d1	kroková
-	-	kIx~	-
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přesné	přesný	k2eAgNnSc4d1	přesné
natáčení	natáčení	k1gNnSc4	natáčení
s	s	k7c7	s
definovaným	definovaný	k2eAgNnSc7d1	definované
rozlišením	rozlišení	k1gNnSc7	rozlišení
střídavý	střídavý	k2eAgInSc4d1	střídavý
motor	motor	k1gInSc4	motor
(	(	kIx(	(
<g/>
AC	AC	kA	AC
motor	motor	k1gInSc1	motor
<g/>
)	)	kIx)	)
-	-	kIx~	-
oproti	oproti	k7c3	oproti
DC	DC	kA	DC
motorům	motor	k1gInPc3	motor
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgInPc4d2	menší
rozměry	rozměr	k1gInPc4	rozměr
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
stejného	stejný	k2eAgInSc2d1	stejný
výkonu	výkon	k1gInSc2	výkon
synchronní	synchronní	k2eAgInSc1d1	synchronní
asynchronní	asynchronní	k2eAgInSc1d1	asynchronní
servomotor	servomotor	k1gInSc1	servomotor
-	-	kIx~	-
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
převodovka	převodovka	k1gFnSc1	převodovka
a	a	k8xC	a
inkrementální	inkrementální	k2eAgInSc1d1	inkrementální
enkodér	enkodér	k1gInSc1	enkodér
Zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
mobilního	mobilní	k2eAgMnSc4d1	mobilní
robota	robot	k1gMnSc4	robot
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
baterie	baterie	k1gFnSc1	baterie
elektrických	elektrický	k2eAgInPc2d1	elektrický
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
primární	primární	k2eAgInSc1d1	primární
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gMnSc4	on
nabít	nabít	k5eAaBmF	nabít
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
použít	použít	k5eAaPmF	použít
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
akumulátor	akumulátor	k1gInSc1	akumulátor
(	(	kIx(	(
<g/>
sekundární	sekundární	k2eAgInSc1d1	sekundární
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
nabít	nabít	k5eAaBmF	nabít
a	a	k8xC	a
opětovně	opětovně	k6eAd1	opětovně
využít	využít	k5eAaPmF	využít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dead	Dead	k1gInSc1	Dead
reckoning	reckoning	k1gInSc1	reckoning
-	-	kIx~	-
matematická	matematický	k2eAgFnSc1d1	matematická
procedura	procedura	k1gFnSc1	procedura
pro	pro	k7c4	pro
určování	určování	k1gNnSc4	určování
současné	současný	k2eAgFnSc2d1	současná
pozice	pozice	k1gFnSc2	pozice
vozidla	vozidlo	k1gNnSc2	vozidlo
pomocí	pomocí	k7c2	pomocí
postupného	postupný	k2eAgNnSc2d1	postupné
přičítání	přičítání	k1gNnSc2	přičítání
díky	díky	k7c3	díky
známému	známý	k2eAgInSc3d1	známý
kurzu	kurz	k1gInSc3	kurz
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
nejjednodušší	jednoduchý	k2eAgFnSc7d3	nejjednodušší
implementací	implementace	k1gFnSc7	implementace
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
odometrie	odometrie	k1gFnSc1	odometrie
<g/>
)	)	kIx)	)
Sledování	sledování	k1gNnSc1	sledování
vodicí	vodicí	k2eAgFnSc2d1	vodicí
čáry	čára	k1gFnSc2	čára
(	(	kIx(	(
<g/>
guidepath	guidepath	k1gInSc1	guidepath
following	following	k1gInSc1	following
<g/>
)	)	kIx)	)
-	-	kIx~	-
robot	robot	k1gMnSc1	robot
opticky	opticky	k6eAd1	opticky
či	či	k8xC	či
pomocí	pomocí	k7c2	pomocí
magnetometrů	magnetometr	k1gInPc2	magnetometr
(	(	kIx(	(
<g/>
či	či	k8xC	či
Hallových	Hallových	k2eAgInSc1d1	Hallových
<g />
.	.	kIx.	.
</s>
<s>
sond	sonda	k1gFnPc2	sonda
<g/>
)	)	kIx)	)
sleduje	sledovat	k5eAaImIp3nS	sledovat
vodicí	vodicí	k2eAgFnPc4d1	vodicí
čáry	čára	k1gFnPc4	čára
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
metoda	metoda	k1gFnSc1	metoda
Inerciální	inerciální	k2eAgFnSc2d1	inerciální
navigace	navigace	k1gFnSc2	navigace
-	-	kIx~	-
využívá	využívat	k5eAaImIp3nS	využívat
gyroskopů	gyroskop	k1gInPc2	gyroskop
a	a	k8xC	a
akcelerometrů	akcelerometr	k1gInPc2	akcelerometr
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
zrychlení	zrychlení	k1gNnSc1	zrychlení
a	a	k8xC	a
následně	následně	k6eAd1	následně
tak	tak	k6eAd1	tak
určuje	určovat	k5eAaImIp3nS	určovat
výslednou	výsledný	k2eAgFnSc4d1	výsledná
pozici	pozice	k1gFnSc4	pozice
inerciální	inerciální	k2eAgInPc4d1	inerciální
snímače	snímač	k1gInPc4	snímač
otočení	otočení	k1gNnPc2	otočení
primárně	primárně	k6eAd1	primárně
snímající	snímající	k2eAgInPc1d1	snímající
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
rychlost	rychlost	k1gFnSc4	rychlost
primárně	primárně	k6eAd1	primárně
snímající	snímající	k2eAgFnSc4d1	snímající
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
polohu	poloha	k1gFnSc4	poloha
mechanické	mechanický	k2eAgInPc1d1	mechanický
gyroskopy	gyroskop	k1gInPc1	gyroskop
snímající	snímající	k2eAgInSc1d1	snímající
jeden	jeden	k4xCgInSc4	jeden
stupeň	stupeň	k1gInSc4	stupeň
volnosti	volnost	k1gFnSc2	volnost
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
single-degree-of-freedom	singleegreefreedom	k1gInSc4	single-degree-of-freedom
gyroscopes	gyroscopesa	k1gFnPc2	gyroscopesa
<g/>
,	,	kIx,	,
SDFG	SDFG	kA	SDFG
<g/>
)	)	kIx)	)
snímající	snímající	k2eAgInSc4d1	snímající
dva	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
volnosti	volnost	k1gFnSc2	volnost
(	(	kIx(	(
<g/>
two	two	k?	two
axis	axis	k1gInSc1	axis
<g/>
,	,	kIx,	,
free	free	k1gNnSc1	free
gyros	gyrosa	k1gFnPc2	gyrosa
-	-	kIx~	-
dvouosé	dvouosý	k2eAgInPc4d1	dvouosý
<g/>
,	,	kIx,	,
volné	volný	k2eAgInPc4d1	volný
gyroskopy	gyroskop	k1gInPc4	gyroskop
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
DFG	DFG	kA	DFG
<g/>
)	)	kIx)	)
optické	optický	k2eAgInPc4d1	optický
gyroskopy	gyroskop	k1gInPc4	gyroskop
-	-	kIx~	-
využívají	využívat	k5eAaPmIp3nP	využívat
Sagnacův	Sagnacův	k2eAgInSc4d1	Sagnacův
efekt	efekt	k1gInSc4	efekt
(	(	kIx(	(
<g/>
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
doba	doba	k1gFnSc1	doba
letu	let	k1gInSc2	let
2	[number]	k4	2
paprsků	paprsek	k1gInPc2	paprsek
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pohybujícího	pohybující	k2eAgMnSc2d1	pohybující
se	se	k3xPyFc4	se
senzoru	senzor	k1gInSc2	senzor
<g/>
)	)	kIx)	)
akcelerometry	akcelerometr	k1gInPc4	akcelerometr
-	-	kIx~	-
využívají	využívat	k5eAaImIp3nP	využívat
setrvačnosti	setrvačnost	k1gFnPc4	setrvačnost
hmoty	hmota	k1gFnSc2	hmota
GPS	GPS	kA	GPS
navigace	navigace	k1gFnSc1	navigace
pomocí	pomoc	k1gFnPc2	pomoc
taktilních	taktilní	k2eAgFnPc2d1	taktilní
(	(	kIx(	(
<g/>
dotykových	dotykový	k2eAgFnPc2d1	dotyková
<g/>
)	)	kIx)	)
a	a	k8xC	a
proximitních	proximitní	k2eAgInPc2d1	proximitní
(	(	kIx(	(
<g/>
bezdotykových	bezdotykový	k2eAgInPc2d1	bezdotykový
<g/>
)	)	kIx)	)
senzorů	senzor	k1gInPc2	senzor
</s>
