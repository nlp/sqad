<s>
Perkelt	perkelt	k1gInSc1	perkelt
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Pörkölt	Pörkölt	k2eAgInSc1d1	Pörkölt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pokrm	pokrm	k1gInSc1	pokrm
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jídlo	jídlo	k1gNnSc1	jídlo
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgInPc4d1	podobný
guláši	guláš	k1gInPc7	guláš
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
však	však	k9	však
méně	málo	k6eAd2	málo
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
přílohou	příloha	k1gFnSc7	příloha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tarhoňa	tarhoňum	k1gNnPc4	tarhoňum
<g/>
,	,	kIx,	,
noky	nok	k1gInPc1	nok
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnPc1	rýže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
připraven	připravit	k5eAaPmNgMnS	připravit
jak	jak	k6eAd1	jak
z	z	k7c2	z
hovězího	hovězí	k1gNnSc2	hovězí
nebo	nebo	k8xC	nebo
vepřového	vepřový	k2eAgNnSc2d1	vepřové
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
třeba	třeba	k6eAd1	třeba
z	z	k7c2	z
kuřecího	kuřecí	k2eAgInSc2d1	kuřecí
či	či	k8xC	či
rybího	rybí	k2eAgInSc2d1	rybí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc4	maso
nakrájíme	nakrájet	k5eAaPmIp1nP	nakrájet
na	na	k7c4	na
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnPc4d1	velká
kostky	kostka	k1gFnPc4	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Osmažíme	osmažit	k5eAaPmIp1nP	osmažit
nakrájenou	nakrájený	k2eAgFnSc4d1	nakrájená
cibuli	cibule	k1gFnSc4	cibule
dozlatova	dozlatova	k6eAd1	dozlatova
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
zasypeme	zasypat	k5eAaPmIp1nP	zasypat
paprikou	paprika	k1gFnSc7	paprika
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
chuti	chuť	k1gFnSc2	chuť
buď	buď	k8xC	buď
jemnou	jemný	k2eAgFnSc7d1	jemná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pálivou	pálivý	k2eAgFnSc7d1	pálivá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přidáme	přidat	k5eAaPmIp1nP	přidat
trochu	trocha	k1gFnSc4	trocha
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
trochu	trochu	k6eAd1	trochu
vyvaří	vyvařit	k5eAaPmIp3nS	vyvařit
<g/>
,	,	kIx,	,
přidáme	přidat	k5eAaPmIp1nP	přidat
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
kmín	kmín	k1gInSc4	kmín
a	a	k8xC	a
utřený	utřený	k2eAgInSc4d1	utřený
česnek	česnek	k1gInSc4	česnek
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
povaříme	povařit	k5eAaPmIp1nP	povařit
a	a	k8xC	a
přidáme	přidat	k5eAaPmIp1nP	přidat
oloupaná	oloupaný	k2eAgNnPc4d1	oloupané
rajčata	rajče	k1gNnPc4	rajče
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
asi	asi	k9	asi
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
minut	minuta	k1gFnPc2	minuta
dusíme	dusit	k5eAaImIp1nP	dusit
pod	pod	k7c7	pod
pokličkou	poklička	k1gFnSc7	poklička
<g/>
,	,	kIx,	,
než	než	k8xS	než
maso	maso	k1gNnSc1	maso
změkne	změknout	k5eAaPmIp3nS	změknout
<g/>
.	.	kIx.	.
</s>
