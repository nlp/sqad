<s>
Věra	Věra	k1gFnSc1	Věra
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
československá	československý	k2eAgFnSc1d1	Československá
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
<g/>
,	,	kIx,	,
trenérka	trenérka	k1gFnSc1	trenérka
a	a	k8xC	a
významná	významný	k2eAgFnSc1d1	významná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
funkcionářka	funkcionářka	k1gFnSc1	funkcionářka
<g/>
,	,	kIx,	,
sedminásobná	sedminásobný	k2eAgFnSc1d1	sedminásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
<g/>
,	,	kIx,	,
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jedenáctinásobná	jedenáctinásobný	k2eAgFnSc1d1	jedenáctinásobná
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
Sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
roku	rok	k1gInSc2	rok
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
předsedkyní	předsedkyně	k1gFnPc2	předsedkyně
Československého	československý	k2eAgInSc2d1	československý
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
také	také	k9	také
členkou	členka	k1gFnSc7	členka
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ruskou	ruský	k2eAgFnSc7d1	ruská
Larisou	Larisa	k1gFnSc7	Larisa
Latyninovou	Latyninový	k2eAgFnSc7d1	Latyninová
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
gymnastek	gymnastka	k1gFnPc2	gymnastka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgFnPc6d1	jdoucí
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
z	z	k7c2	z
víceboje	víceboj	k1gInSc2	víceboj
také	také	k9	také
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
a	a	k8xC	a
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1965	[number]	k4	1965
a	a	k8xC	a
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
již	již	k6eAd1	již
drží	držet	k5eAaImIp3nS	držet
mezi	mezi	k7c4	mezi
gymnasty	gymnast	k1gMnPc4	gymnast
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
absolutním	absolutní	k2eAgInSc6d1	absolutní
počtu	počet	k1gInSc6	počet
individuálních	individuální	k2eAgFnPc2d1	individuální
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
nebyla	být	k5eNaImAgFnS	být
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
závodu	závod	k1gInSc6	závod
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
poražena	porazit	k5eAaPmNgFnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
baletu	balet	k1gInSc3	balet
a	a	k8xC	a
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
v	v	k7c6	v
necelých	celý	k2eNgNnPc6d1	necelé
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Bosákovou	Bosáková	k1gFnSc7	Bosáková
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pod	pod	k7c7	pod
jejím	její	k3xOp3gNnSc7	její
vedením	vedení	k1gNnSc7	vedení
trénovat	trénovat	k5eAaImF	trénovat
gymnastiku	gymnastika	k1gFnSc4	gymnastika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
gymnastické	gymnastický	k2eAgNnSc4d1	gymnastické
mistrovství	mistrovství	k1gNnSc4	mistrovství
republiky	republika	k1gFnSc2	republika
dorostenek	dorostenka	k1gFnPc2	dorostenka
i	i	k8xC	i
juniorek	juniorka	k1gFnPc2	juniorka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
sportovní	sportovní	k2eAgFnSc6d1	sportovní
gymnastice	gymnastika	k1gFnSc6	gymnastika
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
obsadily	obsadit	k5eAaPmAgFnP	obsadit
československé	československý	k2eAgFnPc1d1	Československá
gymnastky	gymnastka	k1gFnPc1	gymnastka
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
družstev	družstvo	k1gNnPc2	družstvo
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
skončila	skončit	k5eAaPmAgFnS	skončit
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
osmá	osmý	k4xOgFnSc1	osmý
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
obsadila	obsadit	k5eAaPmAgFnS	obsadit
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Československa	Československo	k1gNnSc2	Československo
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
Bosákovou	Bosáková	k1gFnSc7	Bosáková
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
za	za	k7c4	za
cvičení	cvičení	k1gNnSc4	cvičení
na	na	k7c6	na
kladině	kladina	k1gFnSc6	kladina
a	a	k8xC	a
stříbrnou	stříbrná	k1gFnSc4	stříbrná
za	za	k7c4	za
přeskok	přeskok	k1gInSc4	přeskok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
začala	začít	k5eAaPmAgFnS	začít
Věra	Věra	k1gFnSc1	Věra
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Prorokem	prorok	k1gMnSc7	prorok
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
vedl	vést	k5eAaImAgMnS	vést
do	do	k7c2	do
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
s	s	k7c7	s
trenérkou	trenérka	k1gFnSc7	trenérka
Slávkou	Slávka	k1gFnSc7	Slávka
Matlochovou	Matlochův	k2eAgFnSc7d1	Matlochův
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
stříbrného	stříbrný	k2eAgNnSc2d1	stříbrné
československého	československý	k2eAgNnSc2d1	Československé
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
družstva	družstvo	k1gNnSc2	družstvo
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
v	v	k7c6	v
individuálních	individuální	k2eAgFnPc6d1	individuální
soutěžích	soutěž	k1gFnPc6	soutěž
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
žen	žena	k1gFnPc2	žena
osmá	osmý	k4xOgNnPc4	osmý
a	a	k8xC	a
na	na	k7c6	na
kladině	kladina	k1gFnSc6	kladina
šestá	šestý	k4xOgFnSc1	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
úspěch	úspěch	k1gInSc1	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
přeskok	přeskok	k1gInSc4	přeskok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
skončila	skončit	k5eAaPmAgFnS	skončit
druhá	druhý	k4xOgNnPc4	druhý
a	a	k8xC	a
v	v	k7c6	v
prostných	prostný	k2eAgFnPc6d1	prostná
třetí	třetí	k4xOgFnSc3	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgNnSc1d1	Československé
družstvo	družstvo	k1gNnSc1	družstvo
získalo	získat	k5eAaPmAgNnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
vrcholem	vrchol	k1gInSc7	vrchol
její	její	k3xOp3gFnSc2	její
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1964	[number]	k4	1964
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
,	,	kIx,	,
v	v	k7c6	v
přeskoku	přeskok	k1gInSc6	přeskok
a	a	k8xC	a
na	na	k7c6	na
kladině	kladina	k1gFnSc6	kladina
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgNnSc1d1	Československé
družstvo	družstvo	k1gNnSc1	družstvo
skončilo	skončit	k5eAaPmAgNnS	skončit
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
získala	získat	k5eAaPmAgFnS	získat
pět	pět	k4xCc4	pět
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
bradla	bradlo	k1gNnSc2	bradlo
<g/>
,	,	kIx,	,
kladina	kladina	k1gFnSc1	kladina
<g/>
,	,	kIx,	,
prostná	prostný	k2eAgFnSc1d1	prostná
<g/>
,	,	kIx,	,
přeskok	přeskok	k1gInSc1	přeskok
<g/>
,	,	kIx,	,
víceboj	víceboj	k1gInSc1	víceboj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Dortmundu	Dortmund	k1gInSc6	Dortmund
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
v	v	k7c6	v
přeskoku	přeskok	k1gInSc6	přeskok
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
získala	získat	k5eAaPmAgFnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
kladině	kladina	k1gFnSc6	kladina
a	a	k8xC	a
v	v	k7c6	v
prostných	prostný	k2eAgFnPc6d1	prostná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
družstev	družstvo	k1gNnPc2	družstvo
pak	pak	k8xC	pak
československé	československý	k2eAgFnSc2d1	Československá
gymnastky	gymnastka	k1gFnSc2	gymnastka
získaly	získat	k5eAaPmAgFnP	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
získala	získat	k5eAaPmAgFnS	získat
zlato	zlato	k1gNnSc4	zlato
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
soutěžních	soutěžní	k2eAgFnPc6d1	soutěžní
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
zlato	zlato	k1gNnSc4	zlato
za	za	k7c4	za
víceboj	víceboj	k1gInSc4	víceboj
<g/>
,	,	kIx,	,
přeskok	přeskok	k1gInSc4	přeskok
<g/>
,	,	kIx,	,
bradla	bradla	k1gNnPc4	bradla
a	a	k8xC	a
prostná	prostná	k1gNnPc4	prostná
<g/>
.	.	kIx.	.
</s>
<s>
Stříbro	stříbro	k1gNnSc1	stříbro
pak	pak	k6eAd1	pak
za	za	k7c4	za
kladinu	kladina	k1gFnSc4	kladina
a	a	k8xC	a
v	v	k7c6	v
družstvech	družstvo	k1gNnPc6	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
těchto	tento	k3xDgFnPc2	tento
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Josefa	Josef	k1gMnSc4	Josef
Odložila	odložit	k5eAaPmAgFnS	odložit
(	(	kIx(	(
<g/>
11.11	[number]	k4	11.11
<g/>
.1938	.1938	k4	.1938
Otrokovice	Otrokovice	k1gFnPc1	Otrokovice
-	-	kIx~	-
10.9	[number]	k4	10.9
<g/>
.1993	.1993	k4	.1993
Domašov	Domašovo	k1gNnPc2	Domašovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc4d1	český
reprezentanta	reprezentant	k1gMnSc4	reprezentant
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
předání	předání	k1gNnSc2	předání
medailí	medaile	k1gFnPc2	medaile
gesty	gest	k1gInPc1	gest
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
okupaci	okupace	k1gFnSc3	okupace
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
když	když	k8xS	když
stála	stát	k5eAaImAgFnS	stát
společně	společně	k6eAd1	společně
na	na	k7c6	na
stupínku	stupínek	k1gInSc6	stupínek
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
závodnicí	závodnice	k1gFnSc7	závodnice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
hraní	hraní	k1gNnSc2	hraní
sovětské	sovětský	k2eAgFnSc2d1	sovětská
hymny	hymna	k1gFnSc2	hymna
otočila	otočit	k5eAaPmAgFnS	otočit
hlavu	hlava	k1gFnSc4	hlava
od	od	k7c2	od
soupeřky	soupeřka	k1gFnSc2	soupeřka
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Radka	Radka	k1gFnSc1	Radka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
syn	syn	k1gMnSc1	syn
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Odložilem	Odložil	k1gMnSc7	Odložil
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
Věře	Věra	k1gFnSc3	Věra
Čáslavské	Čáslavská	k1gFnSc2	Čáslavská
začalo	začít	k5eAaPmAgNnS	začít
dařit	dařit	k5eAaImF	dařit
ve	v	k7c6	v
společenské	společenský	k2eAgFnSc6d1	společenská
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Dostala	dostat	k5eAaPmAgFnS	dostat
nabídky	nabídka	k1gFnPc4	nabídka
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
primátorky	primátorka	k1gFnSc2	primátorka
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
nebo	nebo	k8xC	nebo
velvyslankyně	velvyslankyně	k1gFnSc2	velvyslankyně
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přijala	přijmout	k5eAaPmAgFnS	přijmout
až	až	k9	až
místo	místo	k7c2	místo
poradkyně	poradkyně	k1gFnSc2	poradkyně
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
předsedkyní	předsedkyně	k1gFnSc7	předsedkyně
Československého	československý	k2eAgInSc2d1	československý
a	a	k8xC	a
poté	poté	k6eAd1	poté
Českého	český	k2eAgInSc2d1	český
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1993	[number]	k4	1993
ji	on	k3xPp3gFnSc4	on
potkala	potkat	k5eAaPmAgFnS	potkat
životní	životní	k2eAgFnSc1d1	životní
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
když	když	k8xS	když
její	její	k3xOp3gMnSc1	její
bývalý	bývalý	k2eAgMnSc1d1	bývalý
manžel	manžel	k1gMnSc1	manžel
Josef	Josef	k1gMnSc1	Josef
Odložil	odložit	k5eAaPmAgMnS	odložit
zemřel	zemřít	k5eAaPmAgMnS	zemřít
po	po	k7c6	po
potyčce	potyčka	k1gFnSc6	potyčka
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
Martinem	Martin	k1gMnSc7	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
měla	mít	k5eAaImAgFnS	mít
syna	syn	k1gMnSc4	syn
ve	v	k7c4	v
své	své	k1gNnSc4	své
výlučné	výlučný	k2eAgFnSc2d1	výlučná
výchově	výchova	k1gFnSc3	výchova
a	a	k8xC	a
podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
ze	z	k7c2	z
soudních	soudní	k2eAgInPc2d1	soudní
spisů	spis	k1gInPc2	spis
jej	on	k3xPp3gMnSc4	on
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
nerespektování	nerespektování	k1gNnSc3	nerespektování
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
procesu	proces	k1gInSc6	proces
a	a	k8xC	a
mediální	mediální	k2eAgFnSc6d1	mediální
kampani	kampaň	k1gFnSc6	kampaň
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
milostí	milost	k1gFnSc7	milost
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yIgFnSc4	který
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
nežádala	žádat	k5eNaImAgFnS	žádat
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
to	ten	k3xDgNnSc4	ten
tehdy	tehdy	k6eAd1	tehdy
popsaly	popsat	k5eAaPmAgInP	popsat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
milost	milost	k1gFnSc1	milost
dostala	dostat	k5eAaPmAgFnS	dostat
především	především	k6eAd1	především
ona	onen	k3xDgFnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
jí	on	k3xPp3gFnSc2	on
došla	dojít	k5eAaPmAgFnS	dojít
životní	životní	k2eAgFnSc1d1	životní
energie	energie	k1gFnSc1	energie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
stahovat	stahovat	k5eAaImF	stahovat
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgInPc2d1	následující
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
s	s	k7c7	s
nikým	nikdo	k3yNnSc7	nikdo
nestýkala	stýkat	k5eNaImAgFnS	stýkat
a	a	k8xC	a
nikde	nikde	k6eAd1	nikde
nevystupovala	vystupovat	k5eNaImAgFnS	vystupovat
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
s	s	k7c7	s
pečovatelskou	pečovatelský	k2eAgFnSc7d1	pečovatelská
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
depresemi	deprese	k1gFnPc7	deprese
se	se	k3xPyFc4	se
léčila	léčit	k5eAaImAgFnS	léčit
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
v	v	k7c6	v
Bohnicích	Bohnice	k1gInPc6	Bohnice
a	a	k8xC	a
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
i	i	k9	i
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
lécích	lék	k1gInPc6	lék
na	na	k7c6	na
spaní	spaní	k1gNnSc6	spaní
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
o	o	k7c6	o
depresi	deprese	k1gFnSc6	deprese
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
jako	jako	k9	jako
o	o	k7c4	o
strašné	strašný	k2eAgFnPc4d1	strašná
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yIgFnSc3	který
ztratila	ztratit	k5eAaPmAgFnS	ztratit
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
s	s	k7c7	s
ohromným	ohromný	k2eAgInSc7d1	ohromný
elánem	elán	k1gInSc7	elán
vracet	vracet	k5eAaImF	vracet
mezi	mezi	k7c4	mezi
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pomáhat	pomáhat	k5eAaImF	pomáhat
českým	český	k2eAgMnPc3d1	český
sportovcům	sportovec	k1gMnPc3	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2015	[number]	k4	2015
ale	ale	k8xC	ale
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
rakovinou	rakovina	k1gFnSc7	rakovina
slinivky	slinivka	k1gFnSc2	slinivka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
jela	jet	k5eAaImAgFnS	jet
na	na	k7c4	na
prohlídku	prohlídka	k1gFnSc4	prohlídka
do	do	k7c2	do
IKEMu	IKEMus	k1gInSc2	IKEMus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
zůstala	zůstat	k5eAaPmAgFnS	zůstat
a	a	k8xC	a
ve	v	k7c6	v
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
na	na	k7c4	na
následky	následek	k1gInPc4	následek
vleklé	vleklý	k2eAgFnSc2d1	vleklá
nemoci	nemoc	k1gFnSc2	nemoc
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
rodinném	rodinný	k2eAgInSc6d1	rodinný
kruhu	kruh	k1gInSc6	kruh
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
zpopelněny	zpopelněn	k2eAgInPc1d1	zpopelněn
v	v	k7c6	v
šumperském	šumperský	k2eAgNnSc6d1	šumperské
krematoriu	krematorium	k1gNnSc6	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
rozloučení	rozloučení	k1gNnSc1	rozloučení
pro	pro	k7c4	pro
představitele	představitel	k1gMnSc4	představitel
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
sportovce	sportovec	k1gMnSc4	sportovec
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc4	veřejnost
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
od	od	k7c2	od
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
budově	budova	k1gFnSc6	budova
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Rozloučení	rozloučení	k1gNnSc1	rozloučení
mělo	mít	k5eAaImAgNnS	mít
formu	forma	k1gFnSc4	forma
komponovaného	komponovaný	k2eAgInSc2d1	komponovaný
pořadu	pořad	k1gInSc2	pořad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
moderoval	moderovat	k5eAaBmAgMnS	moderovat
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
a	a	k8xC	a
na	na	k7c4	na
gymnastku	gymnastka	k1gFnSc4	gymnastka
zde	zde	k6eAd1	zde
zavzpomínali	zavzpomínat	k5eAaPmAgMnP	zavzpomínat
čeští	český	k2eAgMnPc1d1	český
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
a	a	k8xC	a
podepsala	podepsat	k5eAaPmAgFnS	podepsat
petici	petice	k1gFnSc6	petice
Dva	dva	k4xCgInPc1	dva
tisíce	tisíc	k4xCgInPc1	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byla	být	k5eAaImAgFnS	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
členů	člen	k1gMnPc2	člen
ČSTV	ČSTV	kA	ČSTV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Fakultu	fakulta	k1gFnSc4	fakulta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
věnovala	věnovat	k5eAaPmAgFnS	věnovat
se	se	k3xPyFc4	se
trenérské	trenérský	k2eAgFnSc3d1	trenérská
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jí	on	k3xPp3gFnSc7	on
bylo	být	k5eAaImAgNnS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
vykonávat	vykonávat	k5eAaImF	vykonávat
trenérskou	trenérský	k2eAgFnSc4d1	trenérská
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1990	[number]	k4	1990
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
poradkyní	poradkyně	k1gFnSc7	poradkyně
prezidenta	prezident	k1gMnSc2	prezident
ČSR	ČSR	kA	ČSR
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
pro	pro	k7c4	pro
sociální	sociální	k2eAgFnPc4d1	sociální
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
pro	pro	k7c4	pro
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
pak	pak	k9	pak
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnSc1	jeho
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
zastávala	zastávat	k5eAaImAgFnS	zastávat
funkci	funkce	k1gFnSc4	funkce
předsedkyně	předsedkyně	k1gFnSc2	předsedkyně
Československého	československý	k2eAgInSc2d1	československý
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
předsedkyní	předsedkyně	k1gFnPc2	předsedkyně
Českého	český	k2eAgInSc2d1	český
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
a	a	k8xC	a
členkou	členka	k1gFnSc7	členka
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
aktivně	aktivně	k6eAd1	aktivně
podpořila	podpořit	k5eAaPmAgFnS	podpořit
Karla	Karel	k1gMnSc4	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc4	Schwarzenberg
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
hlasitě	hlasitě	k6eAd1	hlasitě
protestovala	protestovat	k5eAaBmAgFnS	protestovat
proti	proti	k7c3	proti
Miloši	Miloš	k1gMnSc3	Miloš
Zemanovi	Zeman	k1gMnSc3	Zeman
a	a	k8xC	a
Janu	Jan	k1gMnSc3	Jan
Fischerovi	Fischer	k1gMnSc3	Fischer
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gNnSc3	jejich
bývalému	bývalý	k2eAgNnSc3d1	bývalé
členství	členství	k1gNnSc3	členství
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
napsala	napsat	k5eAaPmAgFnS	napsat
otevřený	otevřený	k2eAgInSc4d1	otevřený
vzkaz	vzkaz	k1gInSc4	vzkaz
adresovaný	adresovaný	k2eAgInSc1d1	adresovaný
české	český	k2eAgFnSc3d1	Česká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
apelovala	apelovat	k5eAaImAgFnS	apelovat
na	na	k7c4	na
solidaritu	solidarita	k1gFnSc4	solidarita
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prchají	prchat	k5eAaImIp3nP	prchat
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
během	během	k7c2	během
uprchlické	uprchlický	k2eAgFnSc2d1	uprchlická
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
–	–	k?	–
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
–	–	k?	–
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
druhá	druhý	k4xOgFnSc1	druhý
nejznámější	známý	k2eAgFnSc1d3	nejznámější
žena	žena	k1gFnSc1	žena
světa	svět	k1gInSc2	svět
1989	[number]	k4	1989
–	–	k?	–
Cena	cena	k1gFnSc1	cena
Pierra	Pierr	k1gInSc2	Pierr
de	de	k?	de
Coubertina	Coubertin	k1gMnSc2	Coubertin
<g/>
,	,	kIx,	,
udělil	udělit	k5eAaPmAgInS	udělit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
fair	fair	k6eAd1	fair
play	play	k0	play
při	při	k7c6	při
UNESCO	UNESCO	kA	UNESCO
1991	[number]	k4	1991
–	–	k?	–
uvedena	uvést	k5eAaPmNgNnP	uvést
do	do	k7c2	do
Dvorany	dvorana	k1gFnSc2	dvorana
slávy	sláva	k1gFnSc2	sláva
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
1991	[number]	k4	1991
–	–	k?	–
Olympijský	olympijský	k2eAgInSc1d1	olympijský
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
udělen	udělen	k2eAgInSc1d1	udělen
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
olympijským	olympijský	k2eAgInSc7d1	olympijský
výborem	výbor	k1gInSc7	výbor
1995	[number]	k4	1995
–	–	k?	–
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Medaile	medaile	k1gFnSc2	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
II	II	kA	II
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
udělil	udělit	k5eAaPmAgMnS	udělit
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
2010	[number]	k4	2010
–	–	k?	–
japonský	japonský	k2eAgInSc1d1	japonský
Řád	řád	k1gInSc1	řád
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
předal	předat	k5eAaPmAgMnS	předat
japonský	japonský	k2eAgMnSc1d1	japonský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Čikahito	Čikahit	k2eAgNnSc4d1	Čikahit
Harada	Harada	k1gFnSc1	Harada
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
rezidenci	rezidence	k1gFnSc6	rezidence
2014	[number]	k4	2014
–	–	k?	–
Cena	cena	k1gFnSc1	cena
Hanno	Hanen	k2eAgNnSc4d1	Hanno
R.	R.	kA	R.
Ellenbogenové	Ellenbogenová	k1gFnSc2	Ellenbogenová
2016	[number]	k4	2016
–	–	k?	–
cena	cena	k1gFnSc1	cena
Gratias	Gratias	k1gMnSc1	Gratias
Agit	Agit	k1gMnSc1	Agit
za	za	k7c4	za
šíření	šíření	k1gNnSc4	šíření
dobrého	dobrý	k2eAgNnSc2d1	dobré
jména	jméno	k1gNnSc2	jméno
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Věra	Věra	k1gFnSc1	Věra
68	[number]	k4	68
<g/>
,	,	kIx,	,
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
dokument	dokument	k1gInSc4	dokument
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Olga	Olga	k1gFnSc1	Olga
Sommerová	Sommerová	k1gFnSc1	Sommerová
Její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
nese	nést	k5eAaImIp3nS	nést
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
26986	[number]	k4	26986
<g/>
)	)	kIx)	)
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
(	(	kIx(	(
<g/>
provizorní	provizorní	k2eAgNnSc1d1	provizorní
jméno	jméno	k1gNnSc1	jméno
1997	[number]	k4	1997
VC	VC	kA	VC
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Čáslavské	Čáslavská	k1gFnSc3	Čáslavská
70	[number]	k4	70
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
poštovní	poštovní	k2eAgFnSc1d1	poštovní
dopisnice	dopisnice	k1gFnSc1	dopisnice
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kulhánek	Kulhánek	k1gMnSc1	Kulhánek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Věry	Věra	k1gFnSc2	Věra
Čáslavské	Čáslavská	k1gFnSc2	Čáslavská
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
nová	nový	k2eAgFnSc1d1	nová
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
v	v	k7c6	v
Černošicích	Černošice	k1gFnPc6	Černošice
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
byla	být	k5eAaImAgFnS	být
i	i	k9	i
zároveň	zároveň	k6eAd1	zároveň
patronka	patronka	k1gFnSc1	patronka
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
.	.	kIx.	.
</s>
