<s>
Věra	Věra	k1gFnSc1	Věra
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
československá	československý	k2eAgFnSc1d1	Československá
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
<g/>
,	,	kIx,	,
trenérka	trenérka	k1gFnSc1	trenérka
a	a	k8xC	a
významná	významný	k2eAgFnSc1d1	významná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
funkcionářka	funkcionářka	k1gFnSc1	funkcionářka
<g/>
,	,	kIx,	,
sedminásobná	sedminásobný	k2eAgFnSc1d1	sedminásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
<g/>
,	,	kIx,	,
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jedenáctinásobná	jedenáctinásobný	k2eAgFnSc1d1	jedenáctinásobná
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
Sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
roku	rok	k1gInSc2	rok
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
