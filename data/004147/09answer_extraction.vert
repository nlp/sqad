<s>
československá	československý	k2eAgFnSc1d1	Československá
sportovní	sportovní	k2eAgFnSc1d1	sportovní
gymnastka	gymnastka	k1gFnSc1	gymnastka
<g/>
,	,	kIx,	,
trenérka	trenérka	k1gFnSc1	trenérka
a	a	k8xC	a
významná	významný	k2eAgFnSc1d1	významná
sportovní	sportovní	k2eAgFnSc1d1	sportovní
funkcionářka	funkcionářka	k1gFnSc1	funkcionářka
<g/>
,	,	kIx,	,
sedminásobná	sedminásobný	k2eAgFnSc1d1	sedminásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
<g/>
,	,	kIx,	,
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
mistryně	mistryně	k1gFnSc1	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jedenáctinásobná	jedenáctinásobný	k2eAgFnSc1d1	jedenáctinásobná
mistryně	mistryně	k1gFnSc1	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
Sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
roku	rok	k1gInSc2	rok
Československa	Československo	k1gNnSc2	Československo
</s>
