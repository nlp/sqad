<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
při	při	k7c6	při
požívání	požívání	k1gNnSc6	požívání
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
nadměrné	nadměrný	k2eAgFnSc6d1	nadměrná
míře	míra	k1gFnSc6	míra
<g/>
?	?	kIx.	?
</s>
