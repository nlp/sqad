<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Estonska	Estonsko	k1gNnSc2	Estonsko
je	být	k5eAaImIp3nS	být
horizontální	horizontální	k2eAgFnSc1d1	horizontální
trikolóra	trikolóra	k1gFnSc1	trikolóra
tří	tři	k4xCgFnPc2	tři
barev	barva	k1gFnPc2	barva
<g/>
:	:	kIx,	:
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnSc2d1	černá
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnSc2	barva
estonské	estonský	k2eAgFnPc1d1	Estonská
vlajky	vlajka	k1gFnPc1	vlajka
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
<g/>
)	)	kIx)	)
a	a	k8xC	a
sníh	sníh	k1gInSc1	sníh
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
byly	být	k5eAaImAgFnP	být
vykládány	vykládat	k5eAaImNgInP	vykládat
i	i	k9	i
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
doplněná	doplněný	k2eAgNnPc4d1	doplněné
velkým	velký	k2eAgInSc7d1	velký
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Estonska	Estonsko	k1gNnSc2	Estonsko
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
již	již	k9	již
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1212	[number]	k4	1212
dobyli	dobýt	k5eAaPmAgMnP	dobýt
pevnost	pevnost	k1gFnSc4	pevnost
Toompea	Toompeum	k1gNnSc2	Toompeum
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Tallinnu	Tallinn	k1gInSc6	Tallinn
Dánové	Dán	k1gMnPc1	Dán
a	a	k8xC	a
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
jejich	jejich	k3xOp3gInSc4	jejich
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1219	[number]	k4	1219
bylo	být	k5eAaImAgNnS	být
severní	severní	k2eAgNnSc1d1	severní
Estonsko	Estonsko	k1gNnSc1	Estonsko
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Dánsku	Dánsko	k1gNnSc3	Dánsko
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
země	zem	k1gFnSc2	zem
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
řád	řád	k1gInSc1	řád
livonských	livonský	k2eAgMnPc2d1	livonský
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1237	[number]	k4	1237
spojil	spojit	k5eAaPmAgMnS	spojit
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
s	s	k7c7	s
územím	území	k1gNnSc7	území
řádu	řád	k1gInSc2	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
livonských	livonský	k2eAgFnPc2d1	livonská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
1583	[number]	k4	1583
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
Kuronsko	Kuronsko	k1gNnSc4	Kuronsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Estonsko	Estonsko	k1gNnSc1	Estonsko
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc4	on
rozčlenilo	rozčlenit	k5eAaPmAgNnS	rozčlenit
na	na	k7c4	na
Estonskou	estonský	k2eAgFnSc4d1	Estonská
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
chybně	chybně	k6eAd1	chybně
uvedena	uvést	k5eAaPmNgFnS	uvést
Kuronská	Kuronský	k2eAgFnSc1d1	Kuronská
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
Livonskou	Livonský	k2eAgFnSc4d1	Livonská
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc4d1	dnešní
jižní	jižní	k2eAgNnSc4d1	jižní
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
)	)	kIx)	)
gubernii	gubernie	k1gFnSc4	gubernie
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
užívat	užívat	k5eAaImF	užívat
ruskou	ruský	k2eAgFnSc4d1	ruská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1881	[number]	k4	1881
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
Tartu	Tart	k1gInSc6	Tart
studentské	studentský	k2eAgNnSc1d1	studentské
sdružení	sdružení	k1gNnSc1	sdružení
Vironia	Vironium	k1gNnSc2	Vironium
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
historický	historický	k2eAgInSc1d1	historický
kraj	kraj	k1gInSc1	kraj
Virumaa	Viruma	k1gInSc2	Viruma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
vyvěšena	vyvěsit	k5eAaPmNgFnS	vyvěsit
modro-černo-bílá	modro-černoílý	k2eAgFnSc1d1	modro-černo-bílý
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
estonskou	estonský	k2eAgFnSc7d1	Estonská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
nebe	nebe	k1gNnSc4	nebe
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
sníh	sníh	k1gInSc4	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
díky	díky	k7c3	díky
pokrokovým	pokrokový	k2eAgFnPc3d1	pokroková
myšlenkám	myšlenka	k1gFnPc3	myšlenka
sdružení	sdružení	k1gNnSc2	sdružení
znárodněla	znárodnět	k5eAaPmAgFnS	znárodnět
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
ale	ale	k8xC	ale
po	po	k7c6	po
bolševické	bolševický	k2eAgFnSc6d1	bolševická
revoluci	revoluce	k1gFnSc6	revoluce
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc1	oddělení
Estonska	Estonsko	k1gNnSc2	Estonsko
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
až	až	k9	až
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1922	[number]	k4	1922
uzákoněna	uzákoněn	k2eAgFnSc1d1	uzákoněna
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c6	na
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
ale	ale	k9	ale
byly	být	k5eAaImAgFnP	být
vykládány	vykládat	k5eAaImNgFnP	vykládat
odlišně	odlišně	k6eAd1	odlišně
<g/>
:	:	kIx,	:
modrá	modrat	k5eAaImIp3nS	modrat
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
důvěru	důvěra	k1gFnSc4	důvěra
a	a	k8xC	a
věrnost	věrnost	k1gFnSc4	věrnost
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
předchůdce	předchůdce	k1gMnSc1	předchůdce
Estonců	Estonec	k1gMnPc2	Estonec
a	a	k8xC	a
bílá	bílý	k2eAgNnPc4d1	bílé
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
obsadila	obsadit	k5eAaPmAgFnS	obsadit
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vojska	vojsko	k1gNnPc1	vojsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
hitlerovským	hitlerovský	k2eAgNnSc7d1	hitlerovské
Německem	Německo	k1gNnSc7	Německo
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
Estonská	estonský	k2eAgFnSc1d1	Estonská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
zavedena	zaveden	k2eAgFnSc1d1	zavedena
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
vlajky	vlajka	k1gFnSc2	vlajka
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
srpem	srp	k1gInSc7	srp
a	a	k8xC	a
kladivem	kladivo	k1gNnSc7	kladivo
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterým	který	k3yQgInSc7	který
byly	být	k5eAaImAgFnP	být
žluté	žlutý	k2eAgFnPc4d1	žlutá
iniciály	iniciála	k1gFnPc4	iniciála
ENSV	ENSV	kA	ENSV
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
Eesti	Eest	k2eAgMnPc1d1	Eest
Nõ	Nõ	k1gMnPc1	Nõ
Sotsialistlik	Sotsialistlik	k1gMnSc1	Sotsialistlik
Vabariik	Vabariik	k1gMnSc1	Vabariik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
užívala	užívat	k5eAaImAgFnS	užívat
pouze	pouze	k6eAd1	pouze
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
okupováno	okupovat	k5eAaBmNgNnS	okupovat
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
patřilo	patřit	k5eAaImAgNnS	patřit
pod	pod	k7c4	pod
Říšský	říšský	k2eAgInSc4d1	říšský
komisariát	komisariát	k1gInSc4	komisariát
Ostland	Ostland	k1gInSc1	Ostland
<g/>
)	)	kIx)	)
a	a	k8xC	a
užívalo	užívat	k5eAaImAgNnS	užívat
jeho	jeho	k3xOp3gFnSc4	jeho
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byly	být	k5eAaImAgFnP	být
vráceny	vrátit	k5eAaPmNgFnP	vrátit
předválečné	předválečný	k2eAgFnPc1d1	předválečná
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.6	.6	k4	.6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
Estonské	estonský	k2eAgFnSc2d1	Estonská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
tvořil	tvořit	k5eAaImAgInS	tvořit
nadále	nadále	k6eAd1	nadále
červený	červený	k2eAgInSc1d1	červený
list	list	k1gInSc1	list
se	s	k7c7	s
srpem	srp	k1gInSc7	srp
a	a	k8xC	a
kladivem	kladivo	k1gNnSc7	kladivo
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
ně	on	k3xPp3gMnPc4	on
přibyla	přibýt	k5eAaPmAgFnS	přibýt
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
žlutě	žlutě	k6eAd1	žlutě
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
<g/>
,	,	kIx,	,
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
na	na	k7c6	na
sovětské	sovětský	k2eAgFnSc6d1	sovětská
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
doplněna	doplnit	k5eAaPmNgFnS	doplnit
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
,	,	kIx,	,
zvlněným	zvlněný	k2eAgInSc7d1	zvlněný
pruhem	pruh	k1gInSc7	pruh
a	a	k8xC	a
přes	přes	k7c4	přes
něj	on	k3xPp3gMnSc4	on
ještě	ještě	k6eAd1	ještě
dvěma	dva	k4xCgInPc7	dva
úzkými	úzké	k1gInPc7	úzké
<g/>
,	,	kIx,	,
bílými	bílý	k1gMnPc7	bílý
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
zvlněnými	zvlněný	k2eAgMnPc7d1	zvlněný
<g/>
)	)	kIx)	)
proužky	proužek	k1gInPc4	proužek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Při	při	k7c6	při
velkých	velký	k2eAgFnPc6d1	velká
společenských	společenský	k2eAgFnPc6d1	společenská
změnách	změna	k1gFnPc6	změna
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
usnesením	usnesení	k1gNnSc7	usnesení
Estonského	estonský	k2eAgInSc2d1	estonský
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
povoleno	povolit	k5eAaPmNgNnS	povolit
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1988	[number]	k4	1988
užívat	užívat	k5eAaImF	užívat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
estonské	estonský	k2eAgFnPc4d1	Estonská
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vlajka	vlajka	k1gFnSc1	vlajka
Estonské	estonský	k2eAgFnSc2d1	Estonská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Estonsko	Estonsko	k1gNnSc1	Estonsko
svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
Estonská	estonský	k2eAgFnSc1d1	Estonská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
symbolice	symbolika	k1gFnSc6	symbolika
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
uzákoněna	uzákoněn	k2eAgFnSc1d1	uzákoněna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
úplná	úplný	k2eAgFnSc1d1	úplná
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nepřijaté	přijatý	k2eNgInPc4d1	nepřijatý
návrhy	návrh	k1gInPc4	návrh
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
estonských	estonský	k2eAgInPc2d1	estonský
krajů	kraj	k1gInPc2	kraj
==	==	k?	==
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
je	být	k5eAaImIp3nS	být
členěno	členit	k5eAaImNgNnS	členit
na	na	k7c4	na
15	[number]	k4	15
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
maakond	maakond	k1gInSc1	maakond
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgFnPc4	svůj
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
krajů	kraj	k1gInPc2	kraj
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
bílo-zelenou	bíloelený	k2eAgFnSc7d1	bílo-zelená
bikolórou	bikolóra	k1gFnSc7	bikolóra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
bílém	bílé	k1gNnSc6	bílé
pruhu	pruh	k1gInSc2	pruh
umístěny	umístit	k5eAaPmNgInP	umístit
znaky	znak	k1gInPc1	znak
příslušných	příslušný	k2eAgMnPc2d1	příslušný
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
oblastí	oblast	k1gFnPc2	oblast
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
mapách	mapa	k1gFnPc6	mapa
uvedeny	uvést	k5eAaPmNgInP	uvést
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-maa	a	k1gInSc2	-ma
<g/>
,	,	kIx,	,
označující	označující	k2eAgInSc1d1	označující
estonský	estonský	k2eAgInSc1d1	estonský
kraj	kraj	k1gInSc1	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
vlajky	vlajka	k1gFnPc1	vlajka
krajů	kraj	k1gInPc2	kraj
mají	mít	k5eAaImIp3nP	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Estonska	Estonsko	k1gNnSc2	Estonsko
</s>
</p>
<p>
<s>
Estonská	estonský	k2eAgFnSc1d1	Estonská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Estonska	Estonsko	k1gNnSc2	Estonsko
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Estonska	Estonsko	k1gNnSc2	Estonsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
estonská	estonský	k2eAgFnSc1d1	Estonská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
