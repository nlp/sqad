<s>
Avatar	Avatar	k1gInSc1	Avatar
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americko-britský	americkoritský	k2eAgMnSc1d1	americko-britský
sci-fi	scii	k1gNnPc6	sci-fi
film	film	k1gInSc4	film
Jamese	Jamese	k1gFnSc1	Jamese
Camerona	Camerona	k1gFnSc1	Camerona
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
natočen	natočit	k5eAaBmNgInS	natočit
kombinací	kombinace	k1gFnSc7	kombinace
živých	živý	k2eAgInPc2d1	živý
herců	herc	k1gInPc2	herc
a	a	k8xC	a
počítačových	počítačový	k2eAgFnPc2d1	počítačová
animací	animace	k1gFnPc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
střetu	střet	k1gInSc6	střet
pozemšťanů	pozemšťan	k1gMnPc2	pozemšťan
a	a	k8xC	a
domorodců	domorodec	k1gMnPc2	domorodec
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Pandora	Pandora	k1gFnSc1	Pandora
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
film	film	k1gInSc4	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
,	,	kIx,	,
především	především	k9	především
za	za	k7c4	za
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
(	(	kIx(	(
<g/>
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
2154	[number]	k4	2154
<g/>
)	)	kIx)	)
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
Pandora	Pandora	k1gFnSc1	Pandora
v	v	k7c6	v
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
hvězdném	hvězdný	k2eAgInSc6d1	hvězdný
systému	systém	k1gInSc6	systém
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centauri	k1gNnSc2	Centauri
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Zemi	zem	k1gFnSc3	zem
nepodobná	podobný	k2eNgFnSc1d1	nepodobná
příroda	příroda	k1gFnSc1	příroda
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
z	z	k7c2	z
ní	on	k3xPp3gFnSc6	on
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
svítí	svítit	k5eAaImIp3nS	svítit
<g/>
)	)	kIx)	)
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
žijí	žít	k5eAaImIp3nP	žít
lidem	lid	k1gInSc7	lid
podobní	podobný	k2eAgMnPc1d1	podobný
domorodci	domorodec	k1gMnPc1	domorodec
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
kůže	kůže	k1gFnSc2	kůže
v	v	k7c6	v
silné	silný	k2eAgFnSc6d1	silná
harmonii	harmonie	k1gFnSc6	harmonie
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Desátník	desátník	k1gMnSc1	desátník
Jake	Jak	k1gFnSc2	Jak
Sully	Sulla	k1gFnSc2	Sulla
(	(	kIx(	(
<g/>
Sam	Sam	k1gMnSc1	Sam
Worthington	Worthington	k1gInSc1	Worthington
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
příslušník	příslušník	k1gMnSc1	příslušník
americké	americký	k2eAgFnSc2d1	americká
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
upoután	upoután	k2eAgInSc1d1	upoután
na	na	k7c4	na
invalidní	invalidní	k2eAgInSc4d1	invalidní
vozík	vozík	k1gInSc4	vozík
<g/>
,	,	kIx,	,
přilétá	přilétat	k5eAaImIp3nS	přilétat
na	na	k7c4	na
Pandoru	Pandora	k1gFnSc4	Pandora
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nahradil	nahradit	k5eAaPmAgMnS	nahradit
svého	svůj	k1gMnSc4	svůj
náhle	náhle	k6eAd1	náhle
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
bratra-dvojče	bratravojec	k1gMnSc5	bratra-dvojec
Toma	Tom	k1gMnSc4	Tom
Sullyho	Sully	k1gMnSc2	Sully
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
Na	na	k7c6	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
proniknutí	proniknutí	k1gNnSc3	proniknutí
k	k	k7c3	k
domorodcům	domorodec	k1gMnPc3	domorodec
totiž	totiž	k9	totiž
slouží	sloužit	k5eAaImIp3nS	sloužit
tzv.	tzv.	kA	tzv.
avataři	avatař	k1gMnPc1	avatař
vypadající	vypadající	k2eAgMnPc1d1	vypadající
jako	jako	k8xS	jako
domorodci	domorodec	k1gMnPc1	domorodec
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
vědci	vědec	k1gMnPc1	vědec
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
kombinací	kombinace	k1gFnSc7	kombinace
lidské	lidský	k2eAgFnSc2d1	lidská
DNA	DNA	kA	DNA
a	a	k8xC	a
DNA	DNA	kA	DNA
Na	na	k7c4	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgMnSc4	tento
avatara	avatar	k1gMnSc4	avatar
může	moct	k5eAaImIp3nS	moct
pak	pak	k6eAd1	pak
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
byl	být	k5eAaImAgInS	být
avatar	avatar	k1gInSc1	avatar
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
jeho	jeho	k3xOp3gNnSc2	jeho
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
ovládat	ovládat	k5eAaImF	ovládat
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
svou	svůj	k3xOyFgFnSc4	svůj
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
skupiny	skupina	k1gFnSc2	skupina
vědců	vědec	k1gMnPc2	vědec
kolem	kolem	k7c2	kolem
botaničky	botanička	k1gFnSc2	botanička
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Grace	Grace	k1gMnSc2	Grace
Augustinové	Augustinové	k2eAgMnSc2d1	Augustinové
(	(	kIx(	(
<g/>
Sigourney	Sigournea	k1gMnSc2	Sigournea
Weaver	Weaver	k1gMnSc1	Weaver
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
také	také	k9	také
žoldácká	žoldácký	k2eAgFnSc1d1	žoldácká
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
velí	velet	k5eAaImIp3nS	velet
plukovník	plukovník	k1gMnSc1	plukovník
Miles	Miles	k1gMnSc1	Miles
Quaritch	Quaritch	k1gMnSc1	Quaritch
(	(	kIx(	(
<g/>
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Lang	Lang	k1gMnSc1	Lang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
podnikatelé	podnikatel	k1gMnPc1	podnikatel
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Parkerem	Parker	k1gMnSc7	Parker
Selfridgem	Selfridg	k1gMnSc7	Selfridg
(	(	kIx(	(
<g/>
Giovanni	Giovanň	k1gMnSc5	Giovanň
Ribisi	Ribis	k1gMnSc5	Ribis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
cennou	cenný	k2eAgFnSc4d1	cenná
horninu	hornina	k1gFnSc4	hornina
<g/>
,	,	kIx,	,
Unobtainium	Unobtainium	k1gNnSc4	Unobtainium
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
naleziště	naleziště	k1gNnSc1	naleziště
se	se	k3xPyFc4	se
ale	ale	k9	ale
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
obrovským	obrovský	k2eAgInSc7d1	obrovský
Rodným	rodný	k2eAgInSc7d1	rodný
stromem	strom	k1gInSc7	strom
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yIgNnSc2	který
je	být	k5eAaImIp3nS	být
soustředěno	soustředěn	k2eAgNnSc1d1	soustředěno
centrum	centrum	k1gNnSc1	centrum
života	život	k1gInSc2	život
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Jake	Jake	k1gInSc1	Jake
Sully	Sulla	k1gFnSc2	Sulla
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
mezi	mezi	k7c4	mezi
Na	na	k7c4	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
a	a	k8xC	a
má	můj	k3xOp1gFnSc1	můj
je	být	k5eAaImIp3nS	být
přimět	přimět	k5eAaPmF	přimět
k	k	k7c3	k
přestěhování	přestěhování	k1gNnSc3	přestěhování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
domorodé	domorodý	k2eAgFnSc2d1	domorodá
princezny	princezna	k1gFnSc2	princezna
Neytiri	Neytir	k1gFnSc2	Neytir
(	(	kIx(	(
<g/>
Zoe	Zoe	k1gFnSc1	Zoe
Saldana	Saldana	k1gFnSc1	Saldana
<g/>
)	)	kIx)	)
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
objevuje	objevovat	k5eAaImIp3nS	objevovat
a	a	k8xC	a
poznává	poznávat	k5eAaImIp3nS	poznávat
přírodu	příroda	k1gFnSc4	příroda
Pandory	Pandora	k1gFnSc2	Pandora
<g/>
,	,	kIx,	,
neznámá	známý	k2eNgNnPc4d1	neznámé
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
mytologii	mytologie	k1gFnSc4	mytologie
kmene	kmen	k1gInSc2	kmen
Na	na	k7c4	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
(	(	kIx(	(
<g/>
kořeny	kořen	k1gInPc1	kořen
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
veškerá	veškerý	k3xTgFnSc1	veškerý
příroda	příroda	k1gFnSc1	příroda
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
je	být	k5eAaImIp3nS	být
propojená	propojený	k2eAgFnSc1d1	propojená
obdobou	obdoba	k1gFnSc7	obdoba
nervových	nervový	k2eAgFnPc2d1	nervová
vláken	vlákna	k1gFnPc2	vlákna
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
i	i	k8xC	i
uchovávat	uchovávat	k5eAaImF	uchovávat
paměť	paměť	k1gFnSc4	paměť
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
domorodci	domorodec	k1gMnPc1	domorodec
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
síť	síť	k1gFnSc4	síť
často	často	k6eAd1	často
napojují	napojovat	k5eAaImIp3nP	napojovat
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
ji	on	k3xPp3gFnSc4	on
božstvem	božstvo	k1gNnSc7	božstvo
Eywa	Eywum	k1gNnSc2	Eywum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
je	být	k5eAaImIp3nS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
i	i	k9	i
mezi	mezi	k7c4	mezi
domorodce	domorodec	k1gMnPc4	domorodec
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
sice	sice	k8xC	sice
dokáží	dokázat	k5eAaPmIp3nP	dokázat
těžkou	těžký	k2eAgFnSc7d1	těžká
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
bojovými	bojový	k2eAgInPc7d1	bojový
vrtulníky	vrtulník	k1gInPc7	vrtulník
<g/>
,	,	kIx,	,
roboty	robot	k1gInPc1	robot
a	a	k8xC	a
bombardéry	bombardér	k1gInPc1	bombardér
zpustošit	zpustošit	k5eAaPmF	zpustošit
část	část	k1gFnSc4	část
pralesa	prales	k1gInSc2	prales
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
Rodný	rodný	k2eAgInSc4d1	rodný
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jakeovi	Jakeův	k2eAgMnPc1d1	Jakeův
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nP	podařit
stmelit	stmelit	k5eAaPmF	stmelit
obyvatele	obyvatel	k1gMnPc4	obyvatel
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
k	k	k7c3	k
boji	boj	k1gInSc3	boj
se	se	k3xPyFc4	se
přidají	přidat	k5eAaPmIp3nP	přidat
i	i	k9	i
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
a	a	k8xC	a
Jake	Jake	k1gNnSc1	Jake
Sully	Sulla	k1gFnSc2	Sulla
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
přenosem	přenos	k1gInSc7	přenos
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
svého	svůj	k3xOyFgMnSc2	svůj
avatara	avatar	k1gMnSc2	avatar
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
na	na	k7c6	na
Pandoře	Pandora	k1gFnSc6	Pandora
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
Na	na	k7c4	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
<g/>
.	.	kIx.	.
</s>
<s>
Distribuční	distribuční	k2eAgFnSc1d1	distribuční
společnost	společnost	k1gFnSc1	společnost
věnovala	věnovat	k5eAaImAgFnS	věnovat
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
a	a	k8xC	a
propagaci	propagace	k1gFnSc4	propagace
Avataru	Avatara	k1gFnSc4	Avatara
220	[number]	k4	220
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
celkový	celkový	k2eAgInSc1d1	celkový
rozpočet	rozpočet	k1gInSc1	rozpočet
s	s	k7c7	s
marketingem	marketing	k1gInSc7	marketing
činí	činit	k5eAaImIp3nS	činit
460	[number]	k4	460
000	[number]	k4	000
000	[number]	k4	000
$	$	kIx~	$
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgFnPc1	první
recenze	recenze	k1gFnPc1	recenze
film	film	k1gInSc4	film
vynášely	vynášet	k5eAaImAgFnP	vynášet
do	do	k7c2	do
výšin	výšina	k1gFnPc2	výšina
coby	coby	k?	coby
"	"	kIx"	"
<g/>
nejočekávanější	očekávaný	k2eAgInSc1d3	nejočekávanější
film	film	k1gInSc1	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
show	show	k1gFnSc1	show
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
naprostou	naprostý	k2eAgFnSc4d1	naprostá
extázi	extáze	k1gFnSc4	extáze
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
revoluční	revoluční	k2eAgInSc1d1	revoluční
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
nikdo	nikdo	k3yNnSc1	nikdo
nepřekoná	překonat	k5eNaPmIp3nS	překonat
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
výjimečně	výjimečně	k6eAd1	výjimečně
shodovaly	shodovat	k5eAaImAgFnP	shodovat
ve	v	k7c6	v
zdůrazňování	zdůrazňování	k1gNnSc6	zdůrazňování
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
3D	[number]	k4	3D
aspekt	aspekt	k1gInSc1	aspekt
v	v	k7c6	v
Avataru	Avatar	k1gInSc6	Avatar
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
efekt	efekt	k1gInSc4	efekt
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc7	jeho
integrální	integrální	k2eAgFnSc7d1	integrální
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nekritický	kritický	k2eNgInSc1d1	nekritický
obdiv	obdiv	k1gInSc1	obdiv
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
vystřídaly	vystřídat	k5eAaPmAgFnP	vystřídat
i	i	k9	i
kritické	kritický	k2eAgFnPc1d1	kritická
recenze	recenze	k1gFnPc1	recenze
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
autoři	autor	k1gMnPc1	autor
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
jmenovat	jmenovat	k5eAaBmF	jmenovat
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
<g/>
)	)	kIx)	)
Avatar	Avatar	k1gInSc1	Avatar
okopíroval	okopírovat	k5eAaPmAgInS	okopírovat
<g/>
.	.	kIx.	.
</s>
<s>
Armond	Armond	k1gMnSc1	Armond
White	Whit	k1gInSc5	Whit
z	z	k7c2	z
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Press	Pressa	k1gFnPc2	Pressa
o	o	k7c6	o
filmu	film	k1gInSc6	film
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Avatar	Avatar	k1gInSc1	Avatar
je	být	k5eAaImIp3nS	být
nejkýčovitější	kýčovitý	k2eAgInSc1d3	nejkýčovitější
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgMnS	natočit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bílý	bílý	k2eAgMnSc1d1	bílý
člověk	člověk	k1gMnSc1	člověk
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
najít	najít	k5eAaPmF	najít
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
identitu	identita	k1gFnSc4	identita
a	a	k8xC	a
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
<g/>
,	,	kIx,	,
rasovou	rasový	k2eAgFnSc4d1	rasová
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnSc4d1	sexuální
a	a	k8xC	a
historickou	historický	k2eAgFnSc4d1	historická
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
béčkový	béčkový	k2eAgInSc4d1	béčkový
film	film	k1gInSc4	film
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
rozpočtem	rozpočet	k1gInSc7	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Explicitní	explicitní	k2eAgNnSc4d1	explicitní
protizápadní	protizápadní	k2eAgNnSc4d1	protizápadní
a	a	k8xC	a
protibělošské	protibělošský	k2eAgNnSc4d1	protibělošský
vyznění	vyznění	k1gNnSc4	vyznění
filmu	film	k1gInSc2	film
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
též	též	k9	též
Brussels	Brussels	k1gInSc1	Brussels
Journal	Journal	k1gFnSc2	Journal
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Třešňák	Třešňák	k1gMnSc1	Třešňák
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
eseji	esej	k1gInSc6	esej
pro	pro	k7c4	pro
Respekt	respekt	k1gInSc4	respekt
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozruch	rozruch	k1gInSc1	rozruch
kolem	kolem	k7c2	kolem
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
dán	dát	k5eAaPmNgInS	dát
také	také	k9	také
"	"	kIx"	"
<g/>
zhmotněním	zhmotnění	k1gNnSc7	zhmotnění
duchovnosti	duchovnost	k1gFnSc2	duchovnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
myšlenkou	myšlenka	k1gFnSc7	myšlenka
posvátnosti	posvátnost	k1gFnSc2	posvátnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Richard	Richard	k1gMnSc1	Richard
Swier	Swier	k1gMnSc1	Swier
za	za	k7c7	za
Avatarem	Avatar	k1gMnSc7	Avatar
zase	zase	k9	zase
vidí	vidět	k5eAaImIp3nS	vidět
"	"	kIx"	"
<g/>
rekrutování	rekrutování	k1gNnSc1	rekrutování
pro	pro	k7c4	pro
ekoterorismus	ekoterorismus	k1gInSc4	ekoterorismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Avatar	Avatar	k1gInSc1	Avatar
je	být	k5eAaImIp3nS	být
statisticky	statisticky	k6eAd1	statisticky
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
film	film	k1gInSc1	film
současnosti	současnost	k1gFnSc2	současnost
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
(	(	kIx(	(
<g/>
divácky	divácky	k6eAd1	divácky
i	i	k9	i
komerčně	komerčně	k6eAd1	komerčně
<g/>
)	)	kIx)	)
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
filmů	film	k1gInPc2	film
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Drží	držet	k5eAaImIp3nP	držet
také	také	k9	také
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
za	za	k7c4	za
tržby	tržba	k1gFnPc4	tržba
v	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
od	od	k7c2	od
uvedení	uvedení	k1gNnSc2	uvedení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
chodí	chodit	k5eAaImIp3nP	chodit
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
zhlédli	zhlédnout	k5eAaPmAgMnP	zhlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Tržby	tržba	k1gFnPc1	tržba
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
týdne	týden	k1gInSc2	týden
jsou	být	k5eAaImIp3nP	být
historicky	historicky	k6eAd1	historicky
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
Titanikem	Titanic	k1gInSc7	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
víkendy	víkend	k1gInPc4	víkend
vydělal	vydělat	k5eAaPmAgInS	vydělat
77	[number]	k4	77
a	a	k8xC	a
75,6	[number]	k4	75,6
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rekordu	rekord	k1gInSc3	rekord
-	-	kIx~	-
za	za	k7c4	za
vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
filmová	filmový	k2eAgNnPc4d1	filmové
představení	představení	k1gNnPc4	představení
v	v	k7c6	v
USA	USA	kA	USA
-	-	kIx~	-
přispěl	přispět	k5eAaPmAgMnS	přispět
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
víkendu	víkend	k1gInSc6	víkend
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
výnosy	výnos	k1gInPc1	výnos
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
kinech	kino	k1gNnPc6	kino
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
částky	částka	k1gFnPc1	částka
270	[number]	k4	270
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
týdnů	týden	k1gInPc2	týden
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
zhlédnutí	zhlédnutí	k1gNnSc4	zhlédnutí
předčil	předčít	k5eAaPmAgMnS	předčít
volnou	volný	k2eAgFnSc4d1	volná
kapacitu	kapacita	k1gFnSc4	kapacita
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
kinosálech	kinosál	k1gInPc6	kinosál
a	a	k8xC	a
diváci	divák	k1gMnPc1	divák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
a	a	k8xC	a
již	již	k6eAd1	již
nedostali	dostat	k5eNaPmAgMnP	dostat
lístek	lístek	k1gInSc4	lístek
<g/>
,	,	kIx,	,
přispěli	přispět	k5eAaPmAgMnP	přispět
paralelně	paralelně	k6eAd1	paralelně
promítaným	promítaný	k2eAgInPc3d1	promítaný
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
17	[number]	k4	17
dnech	den	k1gInPc6	den
od	od	k7c2	od
uvedení	uvedení	k1gNnSc2	uvedení
vydělal	vydělat	k5eAaPmAgInS	vydělat
352	[number]	k4	352
111	[number]	k4	111
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
666	[number]	k4	666
700	[number]	k4	700
000	[number]	k4	000
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
udělalo	udělat	k5eAaPmAgNnS	udělat
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nejrychleji	rychle	k6eAd3	rychle
pokořil	pokořit	k5eAaPmAgInS	pokořit
miliardovou	miliardový	k2eAgFnSc4d1	miliardová
hranici	hranice	k1gFnSc4	hranice
<g/>
..	..	k?	..
Překonal	překonat	k5eAaPmAgMnS	překonat
jej	on	k3xPp3gInSc4	on
až	až	k6eAd1	až
Jurský	jurský	k2eAgInSc4d1	jurský
svět	svět	k1gInSc4	svět
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
pokořit	pokořit	k5eAaPmF	pokořit
miliardu	miliarda	k4xCgFnSc4	miliarda
za	za	k7c2	za
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
předběhl	předběhnout	k5eAaPmAgInS	předběhnout
film	film	k1gInSc1	film
Titanic	Titanic	k1gInSc1	Titanic
v	v	k7c6	v
tržbách	tržba	k1gFnPc6	tržba
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejvýdělečnějším	výdělečný	k2eAgInSc7d3	nejvýdělečnější
filmem	film	k1gInSc7	film
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
započtení	započtení	k1gNnSc2	započtení
inflace	inflace	k1gFnSc2	inflace
<g/>
)	)	kIx)	)
Film	film	k1gInSc1	film
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
celkově	celkově	k6eAd1	celkově
vydělal	vydělat	k5eAaPmAgInS	vydělat
2	[number]	k4	2
782	[number]	k4	782
000	[number]	k4	000
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
Avatar	Avatar	k1gMnSc1	Avatar
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
a	a	k8xC	a
jediný	jediný	k2eAgInSc4d1	jediný
film	film	k1gInSc4	film
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
překonal	překonat	k5eAaPmAgMnS	překonat
hranici	hranice	k1gFnSc4	hranice
2	[number]	k4	2
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2010	[number]	k4	2010
překonal	překonat	k5eAaPmAgMnS	překonat
stejnou	stejný	k2eAgFnSc4d1	stejná
částku	částka	k1gFnSc4	částka
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
tržbách	tržba	k1gFnPc6	tržba
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
tržil	tržit	k5eAaImAgMnS	tržit
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
-	-	kIx~	-
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
přes	přes	k7c4	přes
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
$	$	kIx~	$
-	-	kIx~	-
pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
utržil	utržit	k5eAaPmAgInS	utržit
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
470	[number]	k4	470
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
diváků	divák	k1gMnPc2	divák
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zavítalo	zavítat	k5eAaPmAgNnS	zavítat
do	do	k7c2	do
sálů	sál	k1gInPc2	sál
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
3D	[number]	k4	3D
projekce	projekce	k1gFnPc1	projekce
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
D	D	kA	D
digital	digital	k1gMnSc1	digital
<g/>
,	,	kIx,	,
IMAX	IMAX	kA	IMAX
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
kritika	kritika	k1gFnSc1	kritika
Avatar	Avatara	k1gFnPc2	Avatara
vyzdvihovala	vyzdvihovat	k5eAaImAgFnS	vyzdvihovat
nejen	nejen	k6eAd1	nejen
jako	jako	k8xS	jako
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
zážitek	zážitek	k1gInSc1	zážitek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
si	se	k3xPyFc3	se
vychutnat	vychutnat	k5eAaPmF	vychutnat
pomocí	pomocí	k7c2	pomocí
této	tento	k3xDgFnSc2	tento
technologie	technologie	k1gFnSc2	technologie
v	v	k7c6	v
takto	takto	k6eAd1	takto
vybavených	vybavený	k2eAgInPc6d1	vybavený
kinosálech	kinosál	k1gInPc6	kinosál
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
možností	možnost	k1gFnPc2	možnost
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
serveru	server	k1gInSc6	server
Movie	Movie	k1gFnSc2	Movie
Zone	Zon	k1gFnSc2	Zon
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
filmem	film	k1gInSc7	film
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
filmem	film	k1gInSc7	film
posledních	poslední	k2eAgNnPc2d1	poslední
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dramatický	dramatický	k2eAgInSc4d1	dramatický
snímek	snímek	k1gInSc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
dostal	dostat	k5eAaPmAgMnS	dostat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
tři	tři	k4xCgInPc4	tři
Oscary	Oscar	k1gInPc4	Oscar
<g/>
:	:	kIx,	:
za	za	k7c4	za
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
z	z	k7c2	z
kin	kino	k1gNnPc2	kino
jeho	jeho	k3xOp3gMnPc2	jeho
2D	[number]	k4	2D
verze	verze	k1gFnPc1	verze
(	(	kIx(	(
<g/>
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
jen	jen	k9	jen
3D	[number]	k4	3D
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
a	a	k8xC	a
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
i	i	k9	i
později	pozdě	k6eAd2	pozdě
dementované	dementovaný	k2eAgFnPc1d1	dementovaná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c4	o
stažení	stažení	k1gNnSc4	stažení
filmu	film	k1gInSc2	film
jako	jako	k8xC	jako
takového	takový	k3xDgNnSc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Čína	Čína	k1gFnSc1	Čína
omezila	omezit	k5eAaPmAgFnS	omezit
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
ochrana	ochrana	k1gFnSc1	ochrana
domácí	domácí	k2eAgFnSc2d1	domácí
filmografie	filmografie	k1gFnSc2	filmografie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Avatar	Avatar	k1gInSc1	Avatar
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
strhává	strhávat	k5eAaImIp3nS	strhávat
veškerou	veškerý	k3xTgFnSc4	veškerý
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
domácí	domácí	k2eAgInPc4d1	domácí
filmy	film	k1gInPc4	film
přílišnou	přílišný	k2eAgFnSc7d1	přílišná
konkurencí	konkurence	k1gFnSc7	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
zákaz	zákaz	k1gInSc1	zákaz
filmu	film	k1gInSc2	film
v	v	k7c6	v
nejlidnatější	lidnatý	k2eAgFnSc6d3	nejlidnatější
zemi	zem	k1gFnSc6	zem
světa	svět	k1gInSc2	svět
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
zámořské	zámořský	k2eAgMnPc4d1	zámořský
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
celkové	celkový	k2eAgFnSc2d1	celková
tržby	tržba	k1gFnSc2	tržba
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
důvodem	důvod	k1gInSc7	důvod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
strach	strach	k1gInSc4	strach
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
sami	sám	k3xTgMnPc1	sám
srovnávají	srovnávat	k5eAaImIp3nP	srovnávat
s	s	k7c7	s
utlačovaným	utlačovaný	k2eAgNnSc7d1	utlačované
lidem	lido	k1gNnSc7	lido
Na	na	k7c4	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filmu	film	k1gInSc3	film
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
i	i	k9	i
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Avatar	Avatar	k1gMnSc1	Avatar
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
za	za	k7c4	za
nedostatek	nedostatek	k1gInSc4	nedostatek
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
biocentrické	biocentrický	k2eAgNnSc4d1	biocentrické
"	"	kIx"	"
<g/>
uctívání	uctívání	k1gNnSc4	uctívání
přírody	příroda	k1gFnSc2	příroda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Avatar	Avatara	k1gFnPc2	Avatara
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
memem	memem	k6eAd1	memem
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
zpravodajské	zpravodajský	k2eAgInPc4d1	zpravodajský
servery	server	k1gInPc4	server
jej	on	k3xPp3gInSc4	on
několikrát	několikrát	k6eAd1	několikrát
použily	použít	k5eAaPmAgInP	použít
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
střetu	střet	k1gInSc2	střet
zájmů	zájem	k1gInPc2	zájem
britské	britský	k2eAgFnSc2d1	britská
těžařské	těžařský	k2eAgFnSc2d1	těžařská
společnosti	společnost	k1gFnSc2	společnost
Vedanta	Vedant	k1gMnSc2	Vedant
Resources	Resourcesa	k1gFnPc2	Resourcesa
na	na	k7c6	na
vytěžení	vytěžení	k1gNnSc6	vytěžení
bauxitu	bauxit	k1gInSc2	bauxit
z	z	k7c2	z
posvátné	posvátný	k2eAgFnSc2d1	posvátná
hory	hora	k1gFnSc2	hora
východoindického	východoindický	k2eAgInSc2d1	východoindický
kmene	kmen	k1gInSc2	kmen
Dongria	Dongrium	k1gNnSc2	Dongrium
Kondh	Kondha	k1gFnPc2	Kondha
<g/>
.	.	kIx.	.
</s>
<s>
Bolivijský	bolivijský	k2eAgMnSc1d1	bolivijský
levicový	levicový	k2eAgMnSc1d1	levicový
prezident	prezident	k1gMnSc1	prezident
Evo	Eva	k1gFnSc5	Eva
Moráles	Moráles	k1gMnSc1	Moráles
film	film	k1gInSc4	film
pochválil	pochválit	k5eAaPmAgMnS	pochválit
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
utlačovaným	utlačovaný	k2eAgInPc3d1	utlačovaný
národům	národ	k1gInPc3	národ
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
do	do	k7c2	do
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
kapitalismu	kapitalismus	k1gInSc3	kapitalismus
a	a	k8xC	a
neokolonialismu	neokolonialismus	k1gInSc3	neokolonialismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
kritiky	kritika	k1gFnSc2	kritika
kouření	kouření	k1gNnSc4	kouření
tabáku	tabák	k1gInSc2	tabák
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
hrdinek	hrdinka	k1gFnPc2	hrdinka
ve	v	k7c6	v
scénách	scéna	k1gFnPc6	scéna
hodně	hodně	k6eAd1	hodně
kouří	kouřit	k5eAaImIp3nS	kouřit
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
kritiku	kritika	k1gFnSc4	kritika
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmy	film	k1gInPc1	film
nemají	mít	k5eNaImIp3nP	mít
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
hezké	hezký	k2eAgNnSc1d1	hezké
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
to	ten	k3xDgNnSc1	ten
ošklivé	ošklivý	k2eAgNnSc1d1	ošklivé
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
je	být	k5eAaImIp3nS	být
James	James	k1gInSc1	James
Horner	Hornra	k1gFnPc2	Hornra
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterého	který	k3yQgMnSc4	který
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
třetí	třetí	k4xOgInSc1	třetí
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Jamesem	James	k1gMnSc7	James
Cameronem	Cameron	k1gMnSc7	Cameron
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
filmech	film	k1gInPc6	film
Vetřelci	vetřelec	k1gMnPc1	vetřelec
a	a	k8xC	a
Titanic	Titanic	k1gInSc1	Titanic
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
natáčení	natáčení	k1gNnSc4	natáčení
filmů	film	k1gInPc2	film
Avatar	Avatar	k1gInSc1	Avatar
2	[number]	k4	2
a	a	k8xC	a
Avatar	Avatar	k1gInSc1	Avatar
3	[number]	k4	3
a	a	k8xC	a
zmínil	zmínit	k5eAaPmAgInS	zmínit
i	i	k9	i
Avatar	Avatar	k1gInSc1	Avatar
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Avatar	Avatar	k1gInSc1	Avatar
2	[number]	k4	2
a	a	k8xC	a
Avatar	Avatar	k1gInSc4	Avatar
3	[number]	k4	3
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
navazovat	navazovat	k5eAaImF	navazovat
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
film	film	k1gInSc4	film
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
natáčeny	natáčet	k5eAaImNgInP	natáčet
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
Avatar	Avatar	k1gInSc4	Avatar
4	[number]	k4	4
by	by	k9	by
měl	mít	k5eAaImAgInS	mít
ději	děj	k1gInPc7	děj
původního	původní	k2eAgInSc2d1	původní
filmu	film	k1gInSc2	film
předcházet	předcházet	k5eAaImF	předcházet
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Avatar	Avatar	k1gInSc1	Avatar
2	[number]	k4	2
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
odehrávat	odehrávat	k5eAaImF	odehrávat
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
Pandory	Pandora	k1gFnSc2	Pandora
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnPc4d1	poslední
zprávy	zpráva	k1gFnPc4	zpráva
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
termín	termín	k1gInSc4	termín
pro	pro	k7c4	pro
uvedení	uvedení	k1gNnSc4	uvedení
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
rok	rok	k1gInSc4	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Pandorapedie	Pandorapedie	k1gFnSc1	Pandorapedie
je	být	k5eAaImIp3nS	být
anglicky	anglicky	k6eAd1	anglicky
psaná	psaný	k2eAgFnSc1d1	psaná
internetová	internetový	k2eAgFnSc1d1	internetová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
filmu	film	k1gInSc2	film
Avatar	Avatara	k1gFnPc2	Avatara
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
veškeré	veškerý	k3xTgInPc1	veškerý
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
zbraních	zbraň	k1gFnPc6	zbraň
<g/>
,	,	kIx,	,
fauně	fauna	k1gFnSc6	fauna
<g/>
,	,	kIx,	,
flóře	flóra	k1gFnSc6	flóra
<g/>
,	,	kIx,	,
místních	místní	k2eAgInPc6d1	místní
obyvatelích	obyvatel	k1gMnPc6	obyvatel
Na	na	k7c4	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc6	jejich
zvycích	zvyk	k1gInPc6	zvyk
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
o	o	k7c6	o
projektech	projekt	k1gInPc6	projekt
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Pandora	Pandora	k1gFnSc1	Pandora
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
scénáře	scénář	k1gInSc2	scénář
<g/>
)	)	kIx)	)
provozuje	provozovat	k5eAaImIp3nS	provozovat
vlastní	vlastní	k2eAgMnSc1d1	vlastní
James	James	k1gMnSc1	James
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Avatar	Avatar	k1gInSc1	Avatar
Wiki	Wiki	k1gNnSc6	Wiki
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
,,	,,	k?	,,
Avatar	Avatara	k1gFnPc2	Avatara
-	-	kIx~	-
90	[number]	k4	90
<g/>
%	%	kIx~	%
FITZPATRICK	FITZPATRICK	kA	FITZPATRICK
<g/>
,	,	kIx,	,
J.	J.	kA	J.
:	:	kIx,	:
Avatar	Avatar	k1gMnSc1	Avatar
<g/>
,	,	kIx,	,
Computer	computer	k1gInSc1	computer
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-251-2992-0	[number]	k4	978-80-251-2992-0
WILHELMOVÁ	WILHELMOVÁ	kA	WILHELMOVÁ
M.	M.	kA	M.
<g/>
,	,	kIx,	,
MATHISON	MATHISON	kA	MATHISON
D.	D.	kA	D.
:	:	kIx,	:
Avatar	Avatar	k1gMnSc1	Avatar
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
filmové	filmový	k2eAgNnSc1d1	filmové
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Egmont	Egmont	k1gInSc1	Egmont
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-252-1417-6	[number]	k4	978-80-252-1417-6
WILHELMOVÁ	WILHELMOVÁ	kA	WILHELMOVÁ
M.	M.	kA	M.
<g/>
,	,	kIx,	,
MATHISON	MATHISON	kA	MATHISON
D.	D.	kA	D.
:	:	kIx,	:
Avatar	Avatar	k1gMnSc1	Avatar
-	-	kIx~	-
Příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
<g/>
,	,	kIx,	,
Euromedia	Euromedium	k1gNnPc1	Euromedium
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-242-2698-9	[number]	k4	978-80-242-2698-9
PIRESA	PIRESA	kA	PIRESA
N.	N.	kA	N.
:	:	kIx,	:
Avatar	Avatar	k1gMnSc1	Avatar
-	-	kIx~	-
Výprava	výprava	k1gFnSc1	výprava
k	k	k7c3	k
Na	na	k7c4	na
<g/>
'	'	kIx"	'
<g/>
vi	vi	k?	vi
<g/>
,	,	kIx,	,
Egmont	Egmont	k1gInSc1	Egmont
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-252-1419-0	[number]	k4	978-80-252-1419-0
</s>
