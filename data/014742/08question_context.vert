<s>
Australský	australský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
Australského	australský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
i	i	k9
na	na	k7c6
australských	australský	k2eAgNnPc6d1
obydlených	obydlený	k2eAgNnPc6d1
zámořských	zámořský	k2eAgNnPc6d1
teritoriích	teritorium	k1gNnPc6
<g/>
:	:	kIx,
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
a	a	k8xC
Norfolk	Norfolk	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
tří	tři	k4xCgInPc2
ostrovních	ostrovní	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
:	:	kIx,
Nauru	Nauru	k1gInSc2
<g/>
,	,	kIx,
Kiribati	Kiribati	k1gInSc2
a	a	k8xC
Tuvalu	Tuvalu	k1gInSc2
<g/>
.	.	kIx.
</s>