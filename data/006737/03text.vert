<s>
Jako	jako	k8xS	jako
lumírovci	lumírovec	k1gMnSc3	lumírovec
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
skupina	skupina	k1gFnSc1	skupina
spisovatelů	spisovatel	k1gMnPc2	spisovatel
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
sdružující	sdružující	k2eAgFnSc2d1	sdružující
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
časopisu	časopis	k1gInSc2	časopis
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
ruchovci	ruchovec	k1gMnPc7	ruchovec
tvořili	tvořit	k5eAaImAgMnP	tvořit
novou	nový	k2eAgFnSc4d1	nová
generaci	generace	k1gFnSc4	generace
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přebírala	přebírat	k5eAaImAgFnS	přebírat
vliv	vliv	k1gInSc4	vliv
po	po	k7c6	po
odcházejících	odcházející	k2eAgMnPc6d1	odcházející
májovcích	májovec	k1gMnPc6	májovec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
největší	veliký	k2eAgFnSc7d3	veliký
snahou	snaha	k1gFnSc7	snaha
bylo	být	k5eAaImAgNnS	být
povznést	povznést	k5eAaPmF	povznést
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
třetina	třetina	k1gFnSc1	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
aktivizací	aktivizace	k1gFnSc7	aktivizace
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
byla	být	k5eAaImAgFnS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgInSc2d1	Karlův
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
část	část	k1gFnSc4	část
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
německou	německý	k2eAgFnSc4d1	německá
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
nová	nový	k2eAgNnPc1d1	nové
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
r.	r.	kA	r.
1871	[number]	k4	1871
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
r.	r.	kA	r.
1872	[number]	k4	1872
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
J.	J.	kA	J.
R.	R.	kA	R.
Vilímka	Vilímek	k1gMnSc2	Vilímek
<g/>
)	)	kIx)	)
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
množství	množství	k1gNnSc1	množství
almanachů	almanach	k1gInPc2	almanach
(	(	kIx(	(
<g/>
např.	např.	kA	např.
r.	r.	kA	r.
1868	[number]	k4	1868
almanach	almanach	k1gInSc1	almanach
Ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
r.	r.	kA	r.
1871	[number]	k4	1871
Anemonky	anemonka	k1gFnSc2	anemonka
r.	r.	kA	r.
1878	[number]	k4	1878
nový	nový	k2eAgInSc4d1	nový
almanach	almanach	k1gInSc4	almanach
Máj	máj	k1gInSc1	máj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
středu	střed	k1gInSc2	střed
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dostávají	dostávat	k5eAaImIp3nP	dostávat
Rukopis	rukopis	k1gInSc4	rukopis
královédvorský	královédvorský	k2eAgInSc4d1	královédvorský
a	a	k8xC	a
Rukopis	rukopis	k1gInSc4	rukopis
zelenohorský	zelenohorský	k2eAgInSc4d1	zelenohorský
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
lumírovců	lumírovec	k1gMnPc2	lumírovec
bylo	být	k5eAaImAgNnS	být
přiblížit	přiblížit	k5eAaPmF	přiblížit
české	český	k2eAgNnSc4d1	české
písemnictví	písemnictví	k1gNnSc4	písemnictví
vyspělým	vyspělý	k2eAgFnPc3d1	vyspělá
evropským	evropský	k2eAgFnPc3d1	Evropská
literaturám	literatura	k1gFnPc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
překladů	překlad	k1gInPc2	překlad
(	(	kIx(	(
<g/>
především	především	k9	především
z	z	k7c2	z
románské	románský	k2eAgFnSc2d1	románská
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
uváděli	uvádět	k5eAaImAgMnP	uvádět
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
evropské	evropský	k2eAgInPc4d1	evropský
trendy	trend	k1gInPc4	trend
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tematické	tematický	k2eAgInPc1d1	tematický
tak	tak	k6eAd1	tak
formální	formální	k2eAgInPc1d1	formální
<g/>
.	.	kIx.	.
</s>
<s>
Literárně	literárně	k6eAd1	literárně
hodně	hodně	k6eAd1	hodně
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
<g/>
,	,	kIx,	,
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
například	například	k6eAd1	například
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
neobvyklé	obvyklý	k2eNgFnSc2d1	neobvyklá
básnické	básnický	k2eAgFnSc2d1	básnická
formy	forma	k1gFnSc2	forma
–	–	k?	–
sonet	sonet	k1gInSc1	sonet
<g/>
,	,	kIx,	,
gazel	gazel	k1gInSc1	gazel
<g/>
,	,	kIx,	,
rondel	rondel	k1gInSc1	rondel
<g/>
.	.	kIx.	.
</s>
<s>
Neztotožňovali	ztotožňovat	k5eNaImAgMnP	ztotožňovat
se	se	k3xPyFc4	se
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
<g/>
,	,	kIx,	,
že	že	k8xS	že
literatura	literatura	k1gFnSc1	literatura
má	mít	k5eAaImIp3nS	mít
především	především	k6eAd1	především
sloužit	sloužit	k5eAaImF	sloužit
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
<s>
Filosofickým	filosofický	k2eAgInSc7d1	filosofický
základem	základ	k1gInSc7	základ
jejich	jejich	k3xOp3gFnSc2	jejich
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidstvo	lidstvo	k1gNnSc1	lidstvo
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
k	k	k7c3	k
humanitě	humanita	k1gFnSc3	humanita
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
spisovatelů	spisovatel	k1gMnPc2	spisovatel
na	na	k7c6	na
májovce	májovka	k1gFnSc6	májovka
<g/>
,	,	kIx,	,
ruchovce	ruchovec	k1gMnSc4	ruchovec
a	a	k8xC	a
lumírovce	lumírovec	k1gMnSc4	lumírovec
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
orientační	orientační	k2eAgFnSc1d1	orientační
a	a	k8xC	a
často	často	k6eAd1	často
zjednodušující	zjednodušující	k2eAgMnSc1d1	zjednodušující
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Panorama	panorama	k1gNnSc1	panorama
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
mezi	mezi	k7c4	mezi
lumírovce	lumírovec	k1gMnPc4	lumírovec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
epigony	epigon	k1gMnPc4	epigon
řadí	řadit	k5eAaImIp3nS	řadit
následující	následující	k2eAgMnPc4d1	následující
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
:	:	kIx,	:
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
Bohdan	Bohdan	k1gMnSc1	Bohdan
Jelínek	Jelínek	k1gMnSc1	Jelínek
Josef	Josef	k1gMnSc1	Josef
Thomayer	Thomayer	k1gMnSc1	Thomayer
Otokar	Otokar	k1gMnSc1	Otokar
Mokrý	Mokrý	k1gMnSc1	Mokrý
František	František	k1gMnSc1	František
Herites	Herites	k1gMnSc1	Herites
Částí	část	k1gFnPc2	část
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
K	k	k7c3	k
vymezování	vymezování	k1gNnSc1	vymezování
obou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
spisovatelů	spisovatel	k1gMnPc2	spisovatel
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
zažehla	zažehnout	k5eAaPmAgFnS	zažehnout
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
svou	svůj	k3xOyFgFnSc4	svůj
statí	stať	k1gFnSc7	stať
Obraz	obraz	k1gInSc4	obraz
novějšího	nový	k2eAgNnSc2d2	novější
básnictví	básnictví	k1gNnSc2	básnictví
českého	český	k2eAgNnSc2d1	české
(	(	kIx(	(
<g/>
Časopis	časopis	k1gInSc1	časopis
národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
dovolává	dovolávat	k5eAaImIp3nS	dovolávat
básníka	básník	k1gMnSc4	básník
ryze	ryze	k6eAd1	ryze
národního	národní	k2eAgNnSc2d1	národní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
opak	opak	k1gInSc1	opak
pak	pak	k6eAd1	pak
vidí	vidět	k5eAaImIp3nS	vidět
ve	v	k7c6	v
Vrchlickém	Vrchlický	k1gMnSc6	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
Soběslav	Soběslav	k1gMnSc1	Soběslav
Pinkas	Pinkas	k1gMnSc1	Pinkas
naopak	naopak	k6eAd1	naopak
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
Hálka	hálka	k1gFnSc1	hálka
<g/>
,	,	kIx,	,
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
vytkl	vytknout	k5eAaPmAgInS	vytknout
její	její	k3xOp3gFnSc4	její
uzavřenost	uzavřenost	k1gFnSc4	uzavřenost
a	a	k8xC	a
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
lumírovce	lumírovec	k1gMnSc4	lumírovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následných	následný	k2eAgInPc6d1	následný
sporech	spor	k1gInPc6	spor
se	se	k3xPyFc4	se
počala	počnout	k5eAaPmAgFnS	počnout
profilovat	profilovat	k5eAaImF	profilovat
škola	škola	k1gFnSc1	škola
kosmopolitní	kosmopolitní	k2eAgFnSc1d1	kosmopolitní
(	(	kIx(	(
<g/>
lumírovci	lumírovec	k1gMnPc1	lumírovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
škola	škola	k1gFnSc1	škola
národní	národní	k2eAgFnSc1d1	národní
(	(	kIx(	(
<g/>
ruchovci	ruchovec	k1gMnPc1	ruchovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Májovci	májovec	k1gMnSc3	májovec
Ruchovci	ruchovec	k1gMnSc3	ruchovec
</s>
