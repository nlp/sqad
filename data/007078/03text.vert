<s>
Hornina	hornina	k1gFnSc1	hornina
je	být	k5eAaImIp3nS	být
heterogenní	heterogenní	k2eAgFnSc1d1	heterogenní
směs	směs	k1gFnSc1	směs
tvořená	tvořený	k2eAgFnSc1d1	tvořená
různými	různý	k2eAgInPc7d1	různý
minerály	minerál	k1gInPc7	minerál
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
organickými	organický	k2eAgFnPc7d1	organická
složkami	složka	k1gFnPc7	složka
<g/>
,	,	kIx,	,
vulkanickým	vulkanický	k2eAgNnSc7d1	vulkanické
sklem	sklo	k1gNnSc7	sklo
či	či	k8xC	či
kombinací	kombinace	k1gFnSc7	kombinace
těchto	tento	k3xDgInPc2	tento
komponentů	komponent	k1gInPc2	komponent
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
monominerální	monominerální	k2eAgFnPc1d1	monominerální
horniny	hornina	k1gFnPc1	hornina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgFnPc1d1	tvořená
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
minerálem	minerál	k1gInSc7	minerál
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
hornina	hornina	k1gFnSc1	hornina
mramor	mramor	k1gInSc4	mramor
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pouze	pouze	k6eAd1	pouze
minerálem	minerál	k1gInSc7	minerál
kalcitem	kalcit	k1gInSc7	kalcit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kus	kus	k1gInSc1	kus
pevné	pevný	k2eAgFnSc2d1	pevná
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
surový	surový	k2eAgInSc1d1	surový
i	i	k8xC	i
opracovaný	opracovaný	k2eAgInSc1d1	opracovaný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
kusová	kusový	k2eAgFnSc1d1	kusová
hornina	hornina	k1gFnSc1	hornina
jako	jako	k8xS	jako
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
kamení	kamení	k1gNnSc1	kamení
či	či	k8xC	či
kamenivo	kamenivo	k1gNnSc1	kamenivo
<g/>
,	,	kIx,	,
výchoz	výchoz	k1gInSc1	výchoz
nebo	nebo	k8xC	nebo
podloží	podloží	k1gNnSc1	podloží
z	z	k7c2	z
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
horniny	hornina	k1gFnSc2	hornina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
sypké	sypký	k2eAgFnPc1d1	sypká
a	a	k8xC	a
tekuté	tekutý	k2eAgFnPc1d1	tekutá
horniny	hornina	k1gFnPc1	hornina
mívají	mívat	k5eAaImIp3nP	mívat
specifické	specifický	k2eAgInPc4d1	specifický
názvy	název	k1gInPc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
horninami	hornina	k1gFnPc7	hornina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
petrologie	petrologie	k1gFnSc1	petrologie
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc1	hornina
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
(	(	kIx(	(
<g/>
magmatické	magmatický	k2eAgFnSc2d1	magmatická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
usazené	usazený	k2eAgNnSc4d1	usazené
(	(	kIx(	(
<g/>
sedimentované	sedimentovaný	k2eAgNnSc4d1	sedimentovaný
<g/>
)	)	kIx)	)
a	a	k8xC	a
přeměněné	přeměněný	k2eAgNnSc4d1	přeměněné
(	(	kIx(	(
<g/>
metamorfované	metamorfovaný	k2eAgNnSc4d1	metamorfované
<g/>
)	)	kIx)	)
hlubinné	hlubinný	k2eAgNnSc4d1	hlubinné
(	(	kIx(	(
<g/>
plutonické	plutonický	k2eAgNnSc4d1	plutonický
<g/>
)	)	kIx)	)
gabro	gabro	k1gNnSc4	gabro
syenit	syenit	k1gInSc4	syenit
žula	žout	k5eAaImAgFnS	žout
porfyr	porfyr	k1gInSc4	porfyr
žilné	žilný	k2eAgFnPc1d1	žilná
(	(	kIx(	(
<g/>
neptunické	ptunický	k2eNgFnPc1d1	ptunický
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
pegmatit	pegmatit	k1gInSc4	pegmatit
výlevné	výlevný	k2eAgFnPc1d1	výlevná
(	(	kIx(	(
<g/>
vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
<g/>
)	)	kIx)	)
andezit	andezit	k1gInSc1	andezit
čedič	čedič	k1gInSc1	čedič
melafyr	melafyr	k1gInSc1	melafyr
trachyt	trachyt	k1gInSc1	trachyt
znělec	znělec	k1gInSc1	znělec
úlomkovité	úlomkovitý	k2eAgFnSc2d1	úlomkovitá
štěrk	štěrk	k1gInSc4	štěrk
pískovec	pískovec	k1gInSc1	pískovec
slepenec	slepenec	k1gInSc1	slepenec
jílovité	jílovitý	k2eAgFnPc1d1	jílovitá
hlína	hlína	k1gFnSc1	hlína
spraš	spraš	k1gFnSc1	spraš
jíl	jíl	k1gInSc1	jíl
opuka	opuka	k1gFnSc1	opuka
organogenní	organogenní	k2eAgInSc1d1	organogenní
vápenec	vápenec	k1gInSc1	vápenec
uhlí	uhlí	k1gNnSc2	uhlí
ropa	ropa	k1gFnSc1	ropa
zemní	zemnit	k5eAaImIp3nS	zemnit
plyn	plyn	k1gInSc4	plyn
chemické	chemický	k2eAgFnSc2d1	chemická
travertin	travertin	k1gInSc1	travertin
sůl	sůl	k1gFnSc4	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
rula	rula	k1gFnSc1	rula
svor	svor	k1gInSc1	svor
fylit	fylit	k1gInSc1	fylit
mramor	mramor	k1gInSc4	mramor
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
skupiny	skupina	k1gFnPc1	skupina
hornin	hornina	k1gFnPc2	hornina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
neustálém	neustálý	k2eAgInSc6d1	neustálý
koloběhu	koloběh	k1gInSc6	koloběh
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tlakově-teplotních	tlakověeplotní	k2eAgFnPc6d1	tlakově-teplotní
podmínkách	podmínka	k1gFnPc6	podmínka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pT	pT	k?	pT
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vlivem	vliv	k1gInSc7	vliv
změny	změna	k1gFnSc2	změna
jedné	jeden	k4xCgFnSc2	jeden
či	či	k9wB	či
obou	dva	k4xCgFnPc2	dva
složek	složka	k1gFnPc2	složka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přeměně	přeměna	k1gFnSc6	přeměna
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
v	v	k7c4	v
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
souše	souš	k1gFnSc2	souš
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
149	[number]	k4	149
milionů	milion	k4xCgInPc2	milion
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
horniny	hornina	k1gFnSc2	hornina
sedimentační	sedimentační	k2eAgFnPc1d1	sedimentační
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
horniny	hornina	k1gFnSc2	hornina
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
a	a	k8xC	a
metamorfované	metamorfovaný	k2eAgFnSc2d1	metamorfovaná
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
nejvíce	hodně	k6eAd3	hodně
5	[number]	k4	5
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
90	[number]	k4	90
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
tam	tam	k6eAd1	tam
<g/>
:	:	kIx,	:
břidlice	břidlice	k1gFnSc1	břidlice
–	–	k?	–
52	[number]	k4	52
%	%	kIx~	%
pískovce	pískovec	k1gInSc2	pískovec
–	–	k?	–
15	[number]	k4	15
%	%	kIx~	%
žuly	žula	k1gFnPc1	žula
a	a	k8xC	a
granodiority	granodiorit	k1gInPc1	granodiorit
–	–	k?	–
15	[number]	k4	15
%	%	kIx~	%
vápence	vápenec	k1gInSc2	vápenec
a	a	k8xC	a
dolomity	dolomit	k1gInPc4	dolomit
–	–	k?	–
7	[number]	k4	7
%	%	kIx~	%
bazalty	bazalt	k1gInPc1	bazalt
–	–	k?	–
3	[number]	k4	3
%	%	kIx~	%
ostatní	ostatní	k2eAgMnSc1d1	ostatní
–	–	k?	–
8	[number]	k4	8
%	%	kIx~	%
Stavební	stavební	k2eAgInSc1d1	stavební
kámen	kámen	k1gInSc1	kámen
Minerál	minerál	k1gInSc4	minerál
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hornina	hornina	k1gFnSc1	hornina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hornina	hornina	k1gFnSc1	hornina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Rozeznávání	rozeznávání	k1gNnSc2	rozeznávání
hornin	hornina	k1gFnPc2	hornina
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
MU	MU	kA	MU
Brno	Brno	k1gNnSc1	Brno
</s>
