<s>
Central	Centrat	k2eAgFc1d1
Intelligence	Intelligence	k2eAgFc1d1
Agency	Agenca	k1gFnSc1
(	(	kIx(
<g/>
CIA	CIA	kA
<g/>
,	,	kIx,
Ústřední	ústřední	k2eAgFnSc1d1
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
USA	USA	kA
s	s	k7c7
vnějším	vnější	k2eAgNnSc7d1
polem	pole	k1gNnSc7
působnosti	působnost	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
špionážním	špionážní	k2eAgNnSc7d1
<g/>
)	)	kIx)
mající	mající	k2eAgFnSc1d1
zároveň	zároveň	k6eAd1
za	za	k7c4
úkol	úkol	k1gInSc4
provádět	provádět	k5eAaImF
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
operace	operace	k1gFnSc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
USA	USA	kA
<g/>
.	.	kIx.
</s>