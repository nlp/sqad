<s>
Central	Centrat	k5eAaPmAgMnS,k5eAaImAgMnS
Intelligence	Intelligence	k1gFnPc4
Agency	Agenca	k1gFnPc4
</s>
<s>
Central	Centrat	k5eAaImAgMnS,k5eAaPmAgMnS
Intelligence	Intelligenec	k1gInPc4
Agency	Agenc	k2eAgInPc4d1
Motto	motto	k1gNnSc4
</s>
<s>
The	The	k?
Work	Work	k1gInSc1
of	of	k?
a	a	k8xC
Nation	Nation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Center	centrum	k1gNnPc2
of	of	k?
Intelligence	Intelligenec	k1gMnSc2
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
Office	Office	kA
of	of	k?
Strategic	Strategic	k1gMnSc1
Services	Services	k1gMnSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Dwight	Dwight	k1gMnSc1
D.	D.	kA
Eisenhower	Eisenhower	k1gMnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Joseph	Joseph	k1gMnSc1
Donovan	Donovan	k1gMnSc1
<g/>
,	,	kIx,
Allen	Allen	k1gMnSc1
Dulles	Dulles	k1gMnSc1
a	a	k8xC
Harry	Harra	k1gMnSc2
S.	S.	kA
Truman	Truman	k1gMnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1947	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1
agentura	agentura	k1gFnSc1
vlády	vláda	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
George	Georg	k1gMnSc2
Bush	Bush	k1gMnSc1
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Intelligence	Intelligenec	k1gInSc2
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
38	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
6,12	6,12	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
77	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
48,12	48,12	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Lídr	lídr	k1gMnSc1
</s>
<s>
Gina	Gin	k2eAgFnSc1d1
Haspelová	Haspelová	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
2018	#num#	k4
<g/>
)	)	kIx)
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgMnPc2d1
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
21	#num#	k4
575	#num#	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Doublespeak	Doublespeak	k1gMnSc1
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.cia.gov	www.cia.gov	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
CIA	CIA	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Central	Centrat	k2eAgFc1d1
Intelligence	Intelligence	k2eAgFc1d1
Agency	Agenca	k1gFnSc1
(	(	kIx(
<g/>
CIA	CIA	kA
<g/>
,	,	kIx,
Ústřední	ústřední	k2eAgFnSc1d1
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zpravodajská	zpravodajský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
USA	USA	kA
s	s	k7c7
vnějším	vnější	k2eAgNnSc7d1
polem	pole	k1gNnSc7
působnosti	působnost	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
špionážním	špionážní	k2eAgNnSc7d1
<g/>
)	)	kIx)
mající	mající	k2eAgFnSc1d1
zároveň	zároveň	k6eAd1
za	za	k7c4
úkol	úkol	k1gInSc4
provádět	provádět	k5eAaImF
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
operace	operace	k1gFnSc4
ve	v	k7c4
prospěch	prospěch	k1gInSc4
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
18	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1947	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
National	National	k1gFnSc2
Security	Securita	k1gFnSc2
Act	Act	k1gFnSc2
(	(	kIx(
<g/>
Zákona	zákon	k1gInSc2
o	o	k7c6
národní	národní	k2eAgFnSc6d1
bezpečnosti	bezpečnost	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
podepsal	podepsat	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Harry	Harra	k1gMnSc2
S.	S.	kA
Truman	Truman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
tak	tak	k9
nástupcem	nástupce	k1gMnSc7
OSS	OSS	kA
(	(	kIx(
<g/>
Office	Office	kA
of	of	k?
Strategic	Strategic	k1gMnSc1
Services	Services	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
CIA	CIA	kA
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
s	s	k7c7
cílem	cíl	k1gInSc7
soustředit	soustředit	k5eAaPmF
do	do	k7c2
jedné	jeden	k4xCgFnSc2
organizace	organizace	k1gFnSc2
všechny	všechen	k3xTgFnPc1
vládní	vládní	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
zabývající	zabývající	k2eAgFnPc1d1
se	s	k7c7
zpravodajstvím	zpravodajství	k1gNnSc7
za	za	k7c7
účelem	účel	k1gInSc7
zefektivnění	zefektivnění	k1gNnSc2
sběru	sběr	k1gInSc2
informací	informace	k1gFnPc2
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
složky	složka	k1gFnPc4
provádějící	provádějící	k2eAgInSc1d1
výkon	výkon	k1gInSc1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgInSc2
cíle	cíl	k1gInSc2
však	však	k9
začala	začít	k5eAaPmAgFnS
přímo	přímo	k6eAd1
provádět	provádět	k5eAaImF
americkou	americký	k2eAgFnSc4d1
zahraniční	zahraniční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vyústilo	vyústit	k5eAaPmAgNnS
k	k	k7c3
využívání	využívání	k1gNnSc3
CIA	CIA	kA
jako	jako	k8xC,k8xS
mocenskopolitického	mocenskopolitický	k2eAgInSc2d1
nástroje	nástroj	k1gInSc2
(	(	kIx(
<g/>
Invaze	invaze	k1gFnSc1
v	v	k7c6
zátoce	zátoka	k1gFnSc6
Sviní	sviní	k2eAgFnSc6d1
na	na	k7c6
Kubě	Kuba	k1gFnSc6
a	a	k8xC
Karibská	karibský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
účast	účast	k1gFnSc1
na	na	k7c4
svržení	svržení	k1gNnSc4
levicových	levicový	k2eAgFnPc2d1
vlád	vláda	k1gFnPc2
Mosaddeka	Mosaddeek	k1gInSc2
v	v	k7c6
Íránu	Írán	k1gInSc6
<g/>
,	,	kIx,
Árbenze	Árbenze	k1gFnSc1
v	v	k7c6
Guatemale	Guatemala	k1gFnSc6
nebo	nebo	k8xC
Lumumby	Lumumba	k1gFnSc2
v	v	k7c6
Kongu	Kongo	k1gNnSc6
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Zlomem	zlom	k1gInSc7
v	v	k7c6
činnosti	činnost	k1gFnSc6
CIA	CIA	kA
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
<g/>
,	,	kIx,
propuštění	propuštění	k1gNnSc4
ředitele	ředitel	k1gMnSc2
CIA	CIA	kA
Richarda	Richard	k1gMnSc2
Helmse	Helms	k1gMnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
aféry	aféra	k1gFnSc2
Watergate	Watergat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
byla	být	k5eAaImAgFnS
podřízena	podřídit	k5eAaPmNgFnS
kontrole	kontrola	k1gFnSc3
Kongresem	kongres	k1gInSc7
(	(	kIx(
<g/>
zvláštní	zvláštní	k2eAgInSc4d1
výboru	výbor	k1gInSc3
Senátu	senát	k1gInSc2
a	a	k8xC
Sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
pro	pro	k7c4
aktivity	aktivita	k1gFnPc4
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Národní	národní	k2eAgFnSc7d1
bezpečnostní	bezpečnostní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
bylo	být	k5eAaImAgNnS
nařízeno	nařízen	k2eAgNnSc1d1
ukončení	ukončení	k1gNnSc1
tajných	tajný	k2eAgFnPc2d1
operací	operace	k1gFnPc2
a	a	k8xC
zaměření	zaměření	k1gNnSc2
se	se	k3xPyFc4
pouze	pouze	k6eAd1
na	na	k7c4
špionáž	špionáž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
do	do	k7c2
konce	konec	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
ultrapravicové	ultrapravicový	k2eAgFnPc1d1
(	(	kIx(
<g/>
až	až	k6eAd1
fašistické	fašistický	k2eAgFnSc2d1
<g/>
)	)	kIx)
diktatury	diktatura	k1gFnSc2
v	v	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
s	s	k7c7
podporou	podpora	k1gFnSc7
CIA	CIA	kA
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
Operace	operace	k1gFnSc1
Kondor	kondor	k1gMnSc1
zavraždily	zavraždit	k5eAaPmAgFnP
nebo	nebo	k8xC
nechaly	nechat	k5eAaPmAgFnP
zmizet	zmizet	k5eAaPmF
až	až	k9
80	#num#	k4
000	#num#	k4
politických	politický	k2eAgMnPc2d1
odpůrců	odpůrce	k1gMnPc2
a	a	k8xC
levicově	levicově	k6eAd1
smýšlejících	smýšlející	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dalších	další	k2eAgFnPc2d1
asi	asi	k9
400	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
bylo	být	k5eAaImAgNnS
uvězněno	uvěznit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Nedemokratické	demokratický	k2eNgInPc1d1
režimy	režim	k1gInPc1
byly	být	k5eAaImAgInP
podporovány	podporovat	k5eAaImNgInP
z	z	k7c2
obavy	obava	k1gFnSc2
před	před	k7c7
sílícím	sílící	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
SSSR	SSSR	kA
a	a	k8xC
Kuby	Kuba	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
tzv.	tzv.	kA
zástupné	zástupné	k1gNnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
počátku	počátek	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
CIA	CIA	kA
podporovala	podporovat	k5eAaImAgFnS
povstalce	povstalec	k1gMnPc4
proti	proti	k7c3
prosovětské	prosovětský	k2eAgFnSc3d1
afghánské	afghánský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
a	a	k8xC
Sovětské	sovětský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
aférou	aféra	k1gFnSc7
80	#num#	k4
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
aféru	aféra	k1gFnSc4
Írán-Contras	Írán-Contrasa	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
odhaleno	odhalen	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Reaganova	Reaganův	k2eAgFnSc1d1
administrativa	administrativa	k1gFnSc1
tajně	tajně	k6eAd1
a	a	k8xC
ilegálně	ilegálně	k6eAd1
prodávala	prodávat	k5eAaImAgFnS
zbraně	zbraň	k1gFnPc4
Íránu	Írán	k1gInSc2
v	v	k7c6
irácko-íránské	irácko-íránský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
docílila	docílit	k5eAaPmAgFnS
propuštění	propuštění	k1gNnSc4
amerických	americký	k2eAgNnPc2d1
rukojmí	rukojmí	k1gNnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
zadržováni	zadržovat	k5eAaImNgMnP
v	v	k7c6
Libanonu	Libanon	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
výtěžku	výtěžek	k1gInSc2
financovala	financovat	k5eAaBmAgFnS
protivládní	protivládní	k2eAgNnPc4d1
povstalce	povstalec	k1gMnPc4
Contras	Contras	k1gInSc1
v	v	k7c6
Nikaragui	Nikaragua	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
CIA	CIA	kA
financovala	financovat	k5eAaBmAgFnS
a	a	k8xC
vyzbrojovala	vyzbrojovat	k5eAaImAgFnS
protisovětský	protisovětský	k2eAgInSc4d1
odboj	odboj	k1gInSc4
z	z	k7c2
řad	řada	k1gFnPc2
domácích	domácí	k2eAgMnPc2d1
mudžáhidů	mudžáhid	k1gMnPc2
i	i	k8xC
zahraničních	zahraniční	k2eAgMnPc2d1
dobrovolníků	dobrovolník	k1gMnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
islámského	islámský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yQgMnPc7,k3yIgMnPc7,k3yRgMnPc7
byl	být	k5eAaImAgMnS
i	i	k8xC
Usáma	Usáma	k1gNnSc1
bin	bin	k?
Ládin	Ládin	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
CIA	CIA	kA
otřásl	otřást	k5eAaPmAgMnS
případ	případ	k1gInSc4
Aldricha	Aldrich	k1gMnSc2
Amese	Ames	k1gMnSc2
<g/>
,	,	kIx,
šéfa	šéf	k1gMnSc2
sovětské	sovětský	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
kontrašpionážního	kontrašpionážní	k2eAgInSc2d1
odboru	odbor	k1gInSc2
CIA	CIA	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
od	od	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
tzv.	tzv.	kA
"	"	kIx"
<g/>
krtek	krtek	k1gMnSc1
<g/>
"	"	kIx"
pro	pro	k7c4
KGB	KGB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ames	Ames	k1gInSc1
měl	mít	k5eAaImAgInS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
funkci	funkce	k1gFnSc3
přístup	přístup	k1gInSc4
k	k	k7c3
tajným	tajný	k2eAgFnPc3d1
informacím	informace	k1gFnPc3
a	a	k8xC
prozradil	prozradit	k5eAaPmAgMnS
KGB	KGB	kA
mnoho	mnoho	k4c1
sovětských	sovětský	k2eAgMnPc2d1
agentů	agent	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zběhli	zběhnout	k5eAaPmAgMnP
na	na	k7c4
stranu	strana	k1gFnSc4
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
koncem	konec	k1gInSc7
tzv.	tzv.	kA
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
změn	změna	k1gFnPc2
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
změnily	změnit	k5eAaPmAgInP
i	i	k9
cíle	cíl	k1gInPc1
CIA	CIA	kA
<g/>
;	;	kIx,
dnes	dnes	k6eAd1
bojuje	bojovat	k5eAaImIp3nS
proti	proti	k7c3
teroristickým	teroristický	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
důvodu	důvod	k1gInSc2
špatné	špatný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
CIA	CIA	kA
<g/>
,	,	kIx,
FBI	FBI	kA
a	a	k8xC
NSA	NSA	kA
se	se	k3xPyFc4
nezabránilo	zabránit	k5eNaPmAgNnS
útokům	útok	k1gInPc3
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
útoků	útok	k1gInPc2
se	se	k3xPyFc4
spolupráce	spolupráce	k1gFnSc1
zlepšila	zlepšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tajném	tajný	k2eAgInSc6d1
programu	program	k1gInSc6
CIA	CIA	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zahrnoval	zahrnovat	k5eAaImAgInS
únosy	únos	k1gInPc4
a	a	k8xC
mučení	mučení	k1gNnSc4
lidí	člověk	k1gMnPc2
podezřelých	podezřelý	k2eAgMnPc2d1
z	z	k7c2
terorismu	terorismus	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
podílela	podílet	k5eAaImAgFnS
i	i	k9
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
se	se	k3xPyFc4
CIA	CIA	kA
angažuje	angažovat	k5eAaBmIp3nS
v	v	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
a	a	k8xC
při	při	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
svržení	svržení	k1gNnSc4
syrské	syrský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
vycvičila	vycvičit	k5eAaPmAgFnS
a	a	k8xC
vyzbrojila	vyzbrojit	k5eAaPmAgFnS
přibližně	přibližně	k6eAd1
10	#num#	k4
000	#num#	k4
protivládních	protivládní	k2eAgMnPc2d1
povstalců	povstalec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
začal	začít	k5eAaPmAgInS
server	server	k1gInSc1
WikiLeaks	WikiLeaksa	k1gFnPc2
publikovat	publikovat	k5eAaBmF
tajné	tajný	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
CIA	CIA	kA
v	v	k7c6
rámci	rámec	k1gInSc6
série	série	k1gFnSc2
publikací	publikace	k1gFnPc2
s	s	k7c7
názvem	název	k1gInSc7
Vault	Vault	k1gInSc4
7	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
obsahují	obsahovat	k5eAaImIp3nP
detailně	detailně	k6eAd1
popsané	popsaný	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
a	a	k8xC
možnosti	možnost	k1gFnPc4
CIA	CIA	kA
elektronicky	elektronicky	k6eAd1
sledovat	sledovat	k5eAaImF
lidi	člověk	k1gMnPc4
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
nabourat	nabourat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
jejich	jejich	k3xOp3gNnPc2
elektronických	elektronický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Status	status	k1gInSc1
</s>
<s>
CIA	CIA	kA
je	být	k5eAaImIp3nS
nezávislá	závislý	k2eNgFnSc1d1
státní	státní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1
prezidentovi	prezident	k1gMnSc3
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejná	veřejný	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
je	být	k5eAaImIp3nS
zajištěna	zajistit	k5eAaPmNgFnS
výbory	výbor	k1gInPc7
amerického	americký	k2eAgInSc2d1
Kongresu	kongres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roční	roční	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
schvaluje	schvalovat	k5eAaImIp3nS
Kongres	kongres	k1gInSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
americkému	americký	k2eAgMnSc3d1
daňovému	daňový	k2eAgMnSc3d1
poplatníku	poplatník	k1gMnSc3
znám	znám	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
podle	podle	k7c2
zdrojů	zdroj	k1gInPc2
s	s	k7c7
různou	různý	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
důvěryhodnosti	důvěryhodnost	k1gFnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
řádu	řád	k1gInSc6
jednotek	jednotka	k1gFnPc2
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
CIA	CIA	kA
stojí	stát	k5eAaImIp3nS
ředitel	ředitel	k1gMnSc1
(	(	kIx(
<g/>
Director	Director	k1gMnSc1
of	of	k?
Central	Central	k1gMnSc1
Intelligence	Intelligence	k1gFnSc2
–	–	k?
DCI	DCI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
prezident	prezident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
CIA	CIA	kA
má	mít	k5eAaImIp3nS
asi	asi	k9
20	#num#	k4
000	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Základními	základní	k2eAgInPc7d1
úkoly	úkol	k1gInPc7
CIA	CIA	kA
jsou	být	k5eAaImIp3nP
sběr	sběr	k1gInSc1
<g/>
,	,	kIx,
analýza	analýza	k1gFnSc1
a	a	k8xC
vyhodnocování	vyhodnocování	k1gNnSc1
zpravodajsky	zpravodajsky	k6eAd1
významných	významný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
jedním	jeden	k4xCgInSc7
z	z	k7c2
aspektů	aspekt	k1gInPc2
je	být	k5eAaImIp3nS
uchování	uchování	k1gNnSc4
stávajících	stávající	k2eAgFnPc2d1
amerických	americký	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
a	a	k8xC
vlivy	vliv	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
je	on	k3xPp3gMnPc4
mohly	moct	k5eAaImAgFnP
ohrozit	ohrozit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získaná	získaný	k2eAgNnPc1d1
data	datum	k1gNnPc1
jsou	být	k5eAaImIp3nP
analyzována	analyzován	k2eAgNnPc1d1
a	a	k8xC
vyhodnocené	vyhodnocený	k2eAgFnPc1d1
informace	informace	k1gFnPc1
předávány	předáván	k2eAgFnPc1d1
zadavateli	zadavatel	k1gMnSc3
(	(	kIx(
<g/>
americké	americký	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
podklad	podklad	k1gInSc4
pro	pro	k7c4
rozhodování	rozhodování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
zákona	zákon	k1gInSc2
nesmí	smět	k5eNaImIp3nS
výzvědná	výzvědný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
působit	působit	k5eAaImF
na	na	k7c6
území	území	k1gNnSc6
USA	USA	kA
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Sídlo	sídlo	k1gNnSc1
CIA	CIA	kA
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c4
Langley	Langlea	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
leží	ležet	k5eAaImIp3nP
ve	v	k7c6
státě	stát	k1gInSc6
Virginie	Virginie	k1gFnSc2
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
tamní	tamní	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
základny	základna	k1gFnSc2
<g/>
,	,	kIx,
řeky	řeka	k1gFnSc2
Potomac	Potomac	k1gInSc1
a	a	k8xC
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Washingtonu	Washington	k1gInSc2
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
komplex	komplex	k1gInSc4
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
další	další	k2eAgFnPc1d1
důležité	důležitý	k2eAgFnPc1d1
vládní	vládní	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejvíce	nejvíce	k6eAd1,k6eAd3
hlídaných	hlídaný	k2eAgFnPc2d1
budov	budova	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
BIS	BIS	kA
</s>
<s>
Ředitel	ředitel	k1gMnSc1
BIS	BIS	kA
Michal	Michal	k1gMnSc1
Koudelka	Koudelka	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
v	v	k7c6
březnu	březen	k1gInSc6
2019	#num#	k4
v	v	k7c6
sídle	sídlo	k1gNnSc6
CIA	CIA	kA
v	v	k7c4
Langley	Langlea	k1gFnPc4
Tenetovu	Tenetův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
zahraniční	zahraniční	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
Tenetovu	Tenetův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
korunní	korunní	k2eAgMnSc1d1
princ	princ	k1gMnSc1
ze	z	k7c2
Saúdské	saúdský	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k9
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
blízkým	blízký	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Koudelka	Koudelka	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Toto	tento	k3xDgNnSc4
nejvýznamnější	významný	k2eAgNnSc4d3
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
CIA	CIA	kA
dává	dávat	k5eAaImIp3nS
<g/>
,	,	kIx,
beru	brát	k5eAaImIp1nS
nejen	nejen	k6eAd1
jako	jako	k8xS,k8xC
ocenění	ocenění	k1gNnSc1
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k9
jako	jako	k9
ocenění	ocenění	k1gNnSc4
práce	práce	k1gFnSc2
BIS	BIS	kA
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Orgány	orgán	k1gInPc1
CIA	CIA	kA
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Ředitel	ředitel	k1gMnSc1
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
David	David	k1gMnSc1
S.	S.	kA
Cohen	Cohen	k1gInSc1
–	–	k?
zastupující	zastupující	k2eAgInSc1d1
</s>
<s>
Zástupce	zástupce	k1gMnSc1
ředitele	ředitel	k1gMnSc2
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
funkce	funkce	k1gFnSc1
není	být	k5eNaImIp3nS
obsazena	obsadit	k5eAaPmNgFnS
</s>
<s>
Ředitelství	ředitelství	k1gNnSc1
zpravodajství	zpravodajství	k1gNnSc1
(	(	kIx(
<g/>
Directorate	Directorat	k1gMnSc5
of	of	k?
Intelligence	Intelligence	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
zpravodajství	zpravodajství	k1gNnSc2
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
funkce	funkce	k1gFnSc1
není	být	k5eNaImIp3nS
obsazena	obsadit	k5eAaPmNgFnS
</s>
<s>
Ředitelství	ředitelství	k1gNnSc1
Ústřední	ústřední	k2eAgFnSc2d1
utajovací	utajovací	k2eAgFnSc2d1
služby	služba	k1gFnSc2
(	(	kIx(
<g/>
Directorate	Directorat	k1gInSc5
of	of	k?
National	National	k1gMnSc5
Clandestine	Clandestin	k1gMnSc5
Service	Service	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
Ústřední	ústřední	k2eAgFnSc2d1
utajovací	utajovací	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
tajné	tajný	k2eAgFnSc2d1
</s>
<s>
Ředitelství	ředitelství	k1gNnSc1
pro	pro	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
výzkum	výzkum	k1gInSc4
(	(	kIx(
<g/>
Directorate	Directorat	k1gMnSc5
of	of	k?
Science	Science	k1gFnSc1
&	&	k?
Technology	technolog	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
vědy	věda	k1gFnSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
funkce	funkce	k1gFnSc1
není	být	k5eNaImIp3nS
obsazena	obsadit	k5eAaPmNgFnS
</s>
<s>
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
zpravodajství	zpravodajství	k1gNnSc2
(	(	kIx(
<g/>
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
the	the	k?
Study	stud	k1gInPc1
of	of	k?
Intelligence	Intelligenec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
pro	pro	k7c4
studie	studie	k1gFnPc4
zpravodajství	zpravodajství	k1gNnSc2
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
funkce	funkce	k1gFnSc1
není	být	k5eNaImIp3nS
obsazena	obsadit	k5eAaPmNgFnS
</s>
<s>
Úřad	úřad	k1gInSc1
generálního	generální	k2eAgMnSc2d1
rady	rada	k1gMnSc2
(	(	kIx(
<g/>
Office	Office	kA
of	of	k?
General	General	k1gMnSc1
Counsel	Counsel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Generální	generální	k2eAgMnSc1d1
rada	rada	k1gMnSc1
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
(	(	kIx(
<g/>
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
civilní	civilní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
(	(	kIx(
<g/>
Office	Office	kA
of	of	k?
Public	publicum	k1gNnPc2
Affairs	Affairs	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Ředitel	ředitel	k1gMnSc1
pro	pro	k7c4
civilní	civilní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
funkce	funkce	k1gFnSc1
není	být	k5eNaImIp3nS
obsazena	obsadit	k5eAaPmNgFnS
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Podle	podle	k7c2
rezoluce	rezoluce	k1gFnSc2
íránského	íránský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
je	být	k5eAaImIp3nS
CIA	CIA	kA
teroristickou	teroristický	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
CIA	CIA	kA
aktualizuje	aktualizovat	k5eAaBmIp3nS
a	a	k8xC
bezplatně	bezplatně	k6eAd1
publikuje	publikovat	k5eAaBmIp3nS
základní	základní	k2eAgFnPc4d1
statistické	statistický	k2eAgFnPc4d1
<g/>
,	,	kIx,
demografické	demografický	k2eAgFnPc4d1
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
světových	světový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
teritoriích	teritorium	k1gNnPc6
<g/>
,	,	kIx,
závislých	závislý	k2eAgNnPc6d1
územích	území	k1gNnPc6
a	a	k8xC
strategicky	strategicky	k6eAd1
významných	významný	k2eAgFnPc6d1
místopisných	místopisný	k2eAgFnPc6d1
entitách	entita	k1gFnPc6
pod	pod	k7c7
souhrnným	souhrnný	k2eAgInSc7d1
názvem	název	k1gInSc7
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc4
tyto	tento	k3xDgFnPc4
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
vlajky	vlajka	k1gFnPc1
a	a	k8xC
generalizované	generalizovaný	k2eAgFnPc1d1
mapy	mapa	k1gFnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
poskytovány	poskytovat	k5eAaImNgFnP
jako	jako	k9
volné	volný	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
CIA	CIA	kA
stála	stát	k5eAaImAgFnS
za	za	k7c7
založením	založení	k1gNnSc7
švýcarské	švýcarský	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Crypto	Crypto	k1gNnSc1
AG	AG	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pak	pak	k6eAd1
po	po	k7c4
mnoho	mnoho	k4c4
desetiletí	desetiletí	k1gNnPc2
dodávala	dodávat	k5eAaImAgFnS
šifrovací	šifrovací	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc4
desítkám	desítka	k1gFnPc3
států	stát	k1gInPc2
včetně	včetně	k7c2
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
odposlouchávala	odposlouchávat	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Belen	Belen	k2eAgInSc4d1
Fernandez	Fernandez	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reappearing	Reappearing	k1gInSc1
the	the	k?
disappeared	disappeared	k1gInSc1
of	of	k?
Operation	Operation	k1gInSc1
Condor	Condor	k1gInSc1
<g/>
.	.	kIx.
www.aljazeera.com	www.aljazeera.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al-Džazíra	Al-Džazír	k1gMnSc2
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Až	až	k9
25	#num#	k4
let	léto	k1gNnPc2
vězení	vězení	k1gNnSc2
<g/>
:	:	kIx,
Soud	soud	k1gInSc1
potrestal	potrestat	k5eAaPmAgInS
jihoamerické	jihoamerický	k2eAgMnPc4d1
důstojníky	důstojník	k1gMnPc4
za	za	k7c4
likvidaci	likvidace	k1gFnSc4
politických	politický	k2eAgMnPc2d1
odpůrců	odpůrce	k1gMnPc2
<g/>
↑	↑	k?
"	"	kIx"
<g/>
1992	#num#	k4
<g/>
:	:	kIx,
Archives	Archives	k1gMnSc1
of	of	k?
Terror	Terror	k1gMnSc1
Discovered	Discovered	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1992	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KLEIN	Klein	k1gMnSc1
<g/>
,	,	kIx,
Naomi	Nao	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Shock	Shock	k1gMnSc1
Doctrine	Doctrin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Picador	Picador	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
312	#num#	k4
<g/>
-	-	kIx~
<g/>
42799	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
126	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.valka.cz	www.valka.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.lidovky.cz/polepseny-ortega-se-opet-dostava-k-moci-fc0-/zpravy-svet.aspx?c=A060926_092420_ln_zahranici_znk	http://www.lidovky.cz/polepseny-ortega-se-opet-dostava-k-moci-fc0-/zpravy-svet.aspx?c=A060926_092420_ln_zahranici_znk	k1gInSc1
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Afghánistán	Afghánistán	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Superkrtek	Superkrtek	k1gInSc1
Ames	Amesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
20	#num#	k4
lety	léto	k1gNnPc7
FBI	FBI	kA
zatkla	zatknout	k5eAaPmAgFnS
nejdražšího	drahý	k2eAgMnSc4d3
vyzvědače	vyzvědač	k1gMnSc4
Moskvy	Moskva	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
I	i	k9
ČR	ČR	kA
nese	nést	k5eAaImIp3nS
vinu	vina	k1gFnSc4
za	za	k7c4
program	program	k1gInSc4
tajných	tajný	k2eAgFnPc2d1
věznic	věznice	k1gFnPc2
CIA	CIA	kA
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
v	v	k7c6
nevládní	vládní	k2eNgFnSc6d1
zprávě	zpráva	k1gFnSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
has	hasit	k5eAaImRp2nS
secretly	secrést	k5eAaPmAgFnP
provided	provided	k1gInSc4
arms	arms	k6eAd1
training	training	k1gInSc1
to	ten	k3xDgNnSc1
Syria	Syria	k1gFnSc1
rebels	rebels	k6eAd1
since	sinec	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Secret	Secret	k1gInSc1
CIA	CIA	kA
effort	effort	k1gInSc1
in	in	k?
Syria	Syria	k1gFnSc1
faces	facesa	k1gFnPc2
large	largat	k5eAaPmIp3nS
funding	funding	k1gInSc1
cut	cut	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
WikiLeaks	WikiLeaksa	k1gFnPc2
zveřejnily	zveřejnit	k5eAaPmAgFnP
hackerský	hackerský	k2eAgInSc4d1
arzenál	arzenál	k1gInSc4
CIA	CIA	kA
<g/>
,	,	kIx,
špiónům	špión	k1gMnPc3
jej	on	k3xPp3gNnSc4
někdo	někdo	k3yInSc1
ukradl	ukradnout	k5eAaPmAgMnS
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CIA	CIA	kA
Budget	budget	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gMnSc1
Unnecessary	Unnecessara	k1gFnSc2
Secret	Secret	k1gMnSc1
<g/>
.	.	kIx.
www.cato.org	www.cato.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
EXKLUZIVNĚ	exkluzivně	k6eAd1
<g/>
:	:	kIx,
Bez	bez	k7c2
kamer	kamera	k1gFnPc2
a	a	k8xC
v	v	k7c6
přísném	přísný	k2eAgNnSc6d1
utajení	utajení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéf	šéf	k1gMnSc1
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
dostal	dostat	k5eAaPmAgInS
medaili	medaile	k1gFnSc4
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šéf	šéf	k1gMnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
Koudelka	Koudelka	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
od	od	k7c2
americké	americký	k2eAgFnSc2d1
CIA	CIA	kA
prestižní	prestižní	k2eAgFnSc4d1
Tenetovu	Tenetův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tichosti	tichost	k1gFnSc6
a	a	k8xC
utajení	utajení	k1gNnSc1
<g/>
,	,	kIx,
iRozhlas	iRozhlas	k1gInSc1
<g/>
,	,	kIx,
4.4	4.4	k4
<g/>
.2019	.2019	k4
<g/>
↑	↑	k?
http://zpravy.idnes.cz/jste-teroriste-vzkazal-iran-armade-usa-a-cia-fzm-/zahranicni.asp?c=A070929_230019_zahranicni_pei	http://zpravy.idnes.cz/jste-teroriste-vzkazal-iran-armade-usa-a-cia-fzm-/zahranicni.asp?c=A070929_230019_zahranicni_pe	k1gFnSc2
<g/>
↑	↑	k?
https://www.washingtonpost.com/graphics/2020/world/national-security/cia-crypto-encryption-machines-espionage/	https://www.washingtonpost.com/graphics/2020/world/national-security/cia-crypto-encryption-machines-espionage/	k4
-	-	kIx~
‘	‘	k?
<g/>
The	The	k1gMnPc1
intelligence	intelligence	k1gFnSc2
coup	coup	k1gInSc1
of	of	k?
the	the	k?
century	centura	k1gFnSc2
<g/>
’	’	k?
</s>
<s>
For	forum	k1gNnPc2
decades	decadesa	k1gFnPc2
<g/>
,	,	kIx,
the	the	k?
CIA	CIA	kA
read	read	k1gMnSc1
the	the	k?
encrypted	encrypted	k1gMnSc1
communications	communications	k6eAd1
of	of	k?
allies	allies	k1gMnSc1
and	and	k?
adversaries	adversaries	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ROEWER	ROEWER	kA
<g/>
,	,	kIx,
Helmut	Helmut	k1gMnSc1
<g/>
;	;	kIx,
SCHÄFER	SCHÄFER	kA
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
<g/>
;	;	kIx,
UHL	UHL	kA
<g/>
,	,	kIx,
Mathias	Mathias	k1gInSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
;	;	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-242-1607-8	80-242-1607-8	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
ředitelů	ředitel	k1gMnPc2
CIA	CIA	kA
</s>
<s>
The	The	k?
World	World	k1gInSc1
Factbook	Factbook	k1gInSc1
</s>
<s>
FBI	FBI	kA
</s>
<s>
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
</s>
<s>
Air	Air	k?
America	America	k1gFnSc1
</s>
<s>
Echelon	Echelon	k1gInSc1
</s>
<s>
Five	Five	k1gFnSc1
Eyes	Eyesa	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Central	Central	k1gFnSc2
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Téma	téma	k1gNnSc1
CIA	CIA	kA
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Ústřední	ústřední	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
</s>
<s>
Secrets	Secrets	k1gInSc1
of	of	k?
the	the	k?
C.I.A.	C.I.A.	k1gFnSc2
<g/>
,	,	kIx,
audiovizuální	audiovizuální	k2eAgInSc4d1
dokument	dokument	k1gInSc4
britské	britský	k2eAgFnSc2d1
televizní	televizní	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
Sky	Sky	k1gMnSc1
Two	Two	k1gMnSc1
o	o	k7c6
vybraných	vybraný	k2eAgFnPc6d1
aférách	aféra	k1gFnPc6
CIA	CIA	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20021205002	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1021698-4	1021698-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2170	#num#	k4
1744	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79099301	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
140094734	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79099301	#num#	k4
</s>
