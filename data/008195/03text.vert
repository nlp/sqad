<p>
<s>
Sojuz	Sojuz	k1gInSc1	Sojuz
15	[number]	k4	15
byl	být	k5eAaImAgInS	být
let	let	k1gInSc4	let
sovětské	sovětský	k2eAgFnSc2d1	sovětská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodě	loď	k1gFnSc2	loď
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
padesátá	padesátý	k4xOgFnSc1	padesátý
loď	loď	k1gFnSc1	loď
vyslaná	vyslaný	k2eAgFnSc1d1	vyslaná
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Posádka	posádka	k1gFnSc1	posádka
==	==	k?	==
</s>
</p>
<p>
<s>
Gennadij	Gennadít	k5eAaPmRp2nS	Gennadít
Sarafanov	Sarafanov	k1gInSc1	Sarafanov
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
lodě	loď	k1gFnSc2	loď
</s>
</p>
<p>
<s>
Lev	Lev	k1gMnSc1	Lev
Stěpanovič	Stěpanovič	k1gMnSc1	Stěpanovič
Ďomin	Ďomin	k1gMnSc1	Ďomin
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
počet	počet	k1gInSc1	počet
letů	let	k1gInPc2	let
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
včetně	včetně	k7c2	včetně
této	tento	k3xDgFnSc2	tento
mise	mise	k1gFnSc2	mise
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Záložní	záložní	k2eAgFnSc1d1	záložní
posádka	posádka	k1gFnSc1	posádka
===	===	k?	===
</s>
</p>
<p>
<s>
Boris	Boris	k1gMnSc1	Boris
Volynov	Volynov	k1gInSc4	Volynov
</s>
</p>
<p>
<s>
Vitalij	Vitalít	k5eAaPmRp2nS	Vitalít
Žolobov	Žolobov	k1gInSc4	Žolobov
</s>
</p>
<p>
<s>
==	==	k?	==
Program	program	k1gInSc1	program
a	a	k8xC	a
průběh	průběh	k1gInSc1	průběh
letu	let	k1gInSc2	let
==	==	k?	==
</s>
</p>
<p>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
pilotovanou	pilotovaný	k2eAgFnSc4d1	pilotovaná
misi	mise	k1gFnSc4	mise
na	na	k7c4	na
orbitální	orbitální	k2eAgFnSc4d1	orbitální
stanici	stanice	k1gFnSc4	stanice
Saljut	Saljut	k1gInSc1	Saljut
3	[number]	k4	3
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vojenského	vojenský	k2eAgInSc2d1	vojenský
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Posádkou	posádka	k1gFnSc7	posádka
byli	být	k5eAaImAgMnP	být
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
palubní	palubní	k2eAgMnPc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
Lev	Lev	k1gMnSc1	Lev
Ďomin	Ďomin	k1gMnSc1	Ďomin
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
lodě	loď	k1gFnSc2	loď
Gennadij	Gennadij	k1gMnSc1	Gennadij
Sarafanov	Sarafanov	k1gInSc1	Sarafanov
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
Saljutem	Saljut	k1gInSc7	Saljut
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
opakovaně	opakovaně	k6eAd1	opakovaně
nezdařilo	zdařit	k5eNaPmAgNnS	zdařit
pro	pro	k7c4	pro
poruchu	porucha	k1gFnSc4	porucha
automatického	automatický	k2eAgInSc2d1	automatický
setkávacího	setkávací	k2eAgInSc2d1	setkávací
systému	systém	k1gInSc2	systém
Igla	Iglum	k1gNnSc2	Iglum
a	a	k8xC	a
protože	protože	k8xS	protože
loď	loď	k1gFnSc1	loď
neměla	mít	k5eNaImAgFnS	mít
rezervní	rezervní	k2eAgInSc4d1	rezervní
nebo	nebo	k8xC	nebo
manuální	manuální	k2eAgInSc4d1	manuální
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
zamýšlený	zamýšlený	k2eAgInSc4d1	zamýšlený
třicetidenní	třicetidenní	k2eAgInSc4d1	třicetidenní
let	let	k1gInSc4	let
výrazně	výrazně	k6eAd1	výrazně
-	-	kIx~	-
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
-	-	kIx~	-
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
<g/>
.	.	kIx.	.
</s>
<s>
Podařila	podařit	k5eAaPmAgFnS	podařit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
vizuální	vizuální	k2eAgFnSc1d1	vizuální
kontrola	kontrola	k1gFnSc1	kontrola
povrchu	povrch	k1gInSc2	povrch
stanice	stanice	k1gFnSc2	stanice
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
přistála	přistát	k5eAaImAgFnS	přistát
na	na	k7c6	na
padáku	padák	k1gInSc6	padák
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
48	[number]	k4	48
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
Celinogradu	Celinograd	k1gInSc2	Celinograd
<g/>
..	..	k?	..
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
první	první	k4xOgNnSc4	první
noční	noční	k2eAgNnSc4d1	noční
přistání	přistání	k1gNnSc4	přistání
Sojuzu	Sojuz	k1gInSc2	Sojuz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Mise	mise	k1gFnSc1	mise
na	na	k7c6	na
webu	web	k1gInSc6	web
Kosmo	Kosma	k1gMnSc5	Kosma
</s>
</p>
