<s>
Cytosol	Cytosol	k1gInSc1	Cytosol
(	(	kIx(	(
<g/>
též	též	k9	též
vnitrobuněčná	vnitrobuněčný	k2eAgFnSc1d1	vnitrobuněčná
či	či	k8xC	či
intracelulární	intracelulární	k2eAgFnSc1d1	intracelulární
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
omývá	omývat	k5eAaImIp3nS	omývat
membránové	membránový	k2eAgInPc4d1	membránový
váčky	váček	k1gInPc4	váček
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
strukturní	strukturní	k2eAgFnPc4d1	strukturní
částice	částice	k1gFnPc4	částice
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
použil	použít	k5eAaPmAgInS	použít
poprvé	poprvé	k6eAd1	poprvé
H.A.	H.A.	k1gFnSc4	H.A.
Lardy	Larda	k1gFnSc2	Larda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
a	a	k8xC	a
nejprve	nejprve	k6eAd1	nejprve
jím	jíst	k5eAaImIp1nS	jíst
myslel	myslet	k5eAaImAgMnS	myslet
tekutý	tekutý	k2eAgInSc4d1	tekutý
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
vyteče	vytéct	k5eAaPmIp3nS	vytéct
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
rozrušení	rozrušení	k1gNnSc6	rozrušení
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
usazení	usazení	k1gNnSc6	usazení
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
částic	částice	k1gFnPc2	částice
ultracentrifugací	ultracentrifugace	k1gFnPc2	ultracentrifugace
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
složka	složka	k1gFnSc1	složka
buněk	buňka	k1gFnPc2	buňka
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
složka	složka	k1gFnSc1	složka
buněčné	buněčný	k2eAgFnSc2d1	buněčná
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
cytoplazmická	cytoplazmický	k2eAgFnSc1d1	cytoplazmický
frakce	frakce	k1gFnSc1	frakce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
cytosol	cytosola	k1gFnPc2	cytosola
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
tekuté	tekutý	k2eAgFnSc2d1	tekutá
fáze	fáze	k1gFnSc2	fáze
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
v	v	k7c6	v
nepoškozené	poškozený	k2eNgFnSc6d1	nepoškozená
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
tedy	tedy	k9	tedy
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
cytoplazmu	cytoplazma	k1gFnSc4	cytoplazma
uvnitř	uvnitř	k7c2	uvnitř
organel	organela	k1gFnPc2	organela
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
cytosolu	cytosol	k1gInSc2	cytosol
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
typu	typ	k1gInSc6	typ
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
převládající	převládající	k2eAgFnSc7d1	převládající
složkou	složka	k1gFnSc7	složka
buněk	buňka	k1gFnPc2	buňka
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
buněk	buňka	k1gFnPc2	buňka
většinu	většina	k1gFnSc4	většina
prostoru	prostor	k1gInSc2	prostor
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
vakuola	vakuola	k1gFnSc1	vakuola
<g/>
.	.	kIx.	.
</s>
<s>
Cytosol	Cytosol	k1gInSc1	Cytosol
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
malých	malý	k2eAgFnPc2d1	malá
molekul	molekula	k1gFnPc2	molekula
i	i	k8xC	i
makromolekul	makromolekula	k1gFnPc2	makromolekula
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
neproteinových	proteinový	k2eNgFnPc2d1	neproteinová
molekul	molekula	k1gFnPc2	molekula
má	mít	k5eAaImIp3nS	mít
molekulovou	molekulový	k2eAgFnSc4d1	molekulová
hmotnost	hmotnost	k1gFnSc4	hmotnost
nepřesahující	přesahující	k2eNgFnSc4d1	nepřesahující
300	[number]	k4	300
Da	Da	k1gFnSc2	Da
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
relativně	relativně	k6eAd1	relativně
nízkou	nízký	k2eAgFnSc4d1	nízká
velikost	velikost	k1gFnSc4	velikost
molekul	molekula	k1gFnPc2	molekula
je	být	k5eAaImIp3nS	být
však	však	k9	však
směs	směs	k1gFnSc1	směs
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
komplexní	komplexní	k2eAgFnSc1d1	komplexní
a	a	k8xC	a
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
metabolitů	metabolit	k1gInPc2	metabolit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
vytvářet	vytvářet	k5eAaImF	vytvářet
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc4	všechen
budou	být	k5eAaImBp3nP	být
přítomné	přítomný	k2eAgInPc1d1	přítomný
v	v	k7c6	v
jednotlivé	jednotlivý	k2eAgFnSc6d1	jednotlivá
buňce	buňka	k1gFnSc6	buňka
u	u	k7c2	u
jediného	jediný	k2eAgInSc2d1	jediný
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
druhu	druh	k1gInSc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněk	buňka	k1gFnPc2	buňka
druhu	druh	k1gInSc2	druh
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
nebo	nebo	k8xC	nebo
u	u	k7c2	u
kvasinky	kvasinka	k1gFnSc2	kvasinka
Saccharomyces	Saccharomyces	k1gInSc1	Saccharomyces
cerevisiae	cerevisia	k1gFnSc2	cerevisia
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc1	počet
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
cytosolu	cytosol	k1gInSc2	cytosol
tvoří	tvořit	k5eAaImIp3nS	tvořit
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
až	až	k9	až
70	[number]	k4	70
<g/>
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
typických	typický	k2eAgFnPc2d1	typická
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrobuněčné	vnitrobuněčný	k2eAgInPc1d1	vnitrobuněčný
pH	ph	kA	ph
cytosolické	cytosolický	k2eAgFnSc2d1	cytosolický
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
7,4	[number]	k4	7,4
<g/>
.	.	kIx.	.
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
rozmezí	rozmezí	k1gNnSc1	rozmezí
7,0	[number]	k4	7,0
<g/>
-	-	kIx~	-
<g/>
7,4	[number]	k4	7,4
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
např.	např.	kA	např.
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
buňka	buňka	k1gFnSc1	buňka
roste	růst	k5eAaImIp3nS	růst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Viskozita	viskozita	k1gFnSc1	viskozita
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
viskozitou	viskozita	k1gFnSc7	viskozita
čisté	čistý	k2eAgFnSc2d1	čistá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
rychlost	rychlost	k1gFnSc4	rychlost
difuze	difuze	k1gFnSc2	difuze
malých	malý	k2eAgFnPc2d1	malá
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
cytosolem	cytosol	k1gMnSc7	cytosol
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
%	%	kIx~	%
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
především	především	k9	především
srážkami	srážka	k1gFnPc7	srážka
s	s	k7c7	s
makromolekulami	makromolekula	k1gFnPc7	makromolekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
koncentraci	koncentrace	k1gFnSc6	koncentrace
a	a	k8xC	a
znesnadňují	znesnadňovat	k5eAaImIp3nP	znesnadňovat
volný	volný	k2eAgInSc4d1	volný
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgNnSc1d1	zásadní
pro	pro	k7c4	pro
buněčné	buněčný	k2eAgInPc4d1	buněčný
děje	děj	k1gInPc4	děj
a	a	k8xC	a
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
i	i	k9	i
její	její	k3xOp3gFnSc1	její
správná	správný	k2eAgFnSc1d1	správná
koncentrace	koncentrace	k1gFnSc1	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
o	o	k7c4	o
20	[number]	k4	20
<g/>
%	%	kIx~	%
se	se	k3xPyFc4	se
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
krevety	kreveta	k1gFnSc2	kreveta
Artemia	Artemium	k1gNnSc2	Artemium
zastaví	zastavit	k5eAaPmIp3nS	zastavit
metabolismus	metabolismus	k1gInSc1	metabolismus
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgInPc1	veškerý
buněčné	buněčný	k2eAgInPc1d1	buněčný
děje	děj	k1gInPc1	děj
ustanou	ustat	k5eAaPmIp3nP	ustat
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
na	na	k7c4	na
30	[number]	k4	30
<g/>
%	%	kIx~	%
normálního	normální	k2eAgInSc2d1	normální
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
často	často	k6eAd1	často
citovaných	citovaný	k2eAgInPc2d1	citovaný
údajů	údaj	k1gInPc2	údaj
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
5	[number]	k4	5
<g/>
%	%	kIx~	%
vody	voda	k1gFnPc4	voda
pevně	pevně	k6eAd1	pevně
navázáno	navázat	k5eAaPmNgNnS	navázat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc2	on
tzv.	tzv.	kA	tzv.
solvatační	solvatační	k2eAgInSc4d1	solvatační
obal	obal	k1gInSc4	obal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
cytosolické	cytosolický	k2eAgFnSc2d1	cytosolický
vody	voda	k1gFnSc2	voda
má	mít	k5eAaImIp3nS	mít
běžnou	běžný	k2eAgFnSc4d1	běžná
strukturu	struktura	k1gFnSc4	struktura
jako	jako	k8xC	jako
čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Solvatační	Solvatační	k2eAgFnSc1d1	Solvatační
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
nepodílí	podílet	k5eNaImIp3nS	podílet
na	na	k7c6	na
osmóze	osmóza	k1gFnSc6	osmóza
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
odlišně	odlišně	k6eAd1	odlišně
i	i	k9	i
při	při	k7c6	při
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
látek	látka	k1gFnPc2	látka
-	-	kIx~	-
některé	některý	k3yIgFnPc4	některý
molekuly	molekula	k1gFnPc4	molekula
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zakoncentrovat	zakoncentrovat	k5eAaImF	zakoncentrovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiné	jiný	k2eAgFnPc1d1	jiná
naopak	naopak	k6eAd1	naopak
ze	z	k7c2	z
solvatačních	solvatační	k2eAgInPc2d1	solvatační
obalů	obal	k1gInPc2	obal
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
jiných	jiný	k2eAgMnPc2d1	jiný
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
makromolekul	makromolekula	k1gFnPc2	makromolekula
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
veškeré	veškerý	k3xTgFnSc2	veškerý
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jen	jen	k9	jen
té	ten	k3xDgFnSc3	ten
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
povrchu	povrch	k1gInSc2	povrch
makromolekul	makromolekula	k1gFnPc2	makromolekula
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
buňky	buňka	k1gFnPc1	buňka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
a	a	k8xC	a
nižší	nízký	k2eAgFnSc7d2	nižší
hustotou	hustota	k1gFnSc7	hustota
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
dalekosáhlé	dalekosáhlý	k2eAgInPc4d1	dalekosáhlý
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
funkci	funkce	k1gFnSc4	funkce
buněk	buňka	k1gFnPc2	buňka
jako	jako	k9	jako
takových	takový	k3xDgFnPc2	takový
<g/>
.	.	kIx.	.
</s>
<s>
Nukleární	nukleární	k2eAgFnSc1d1	nukleární
magnetická	magnetický	k2eAgFnSc1d1	magnetická
rezonance	rezonance	k1gFnSc1	rezonance
tyto	tento	k3xDgInPc4	tento
názory	názor	k1gInPc4	názor
však	však	k9	však
spíše	spíše	k9	spíše
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
(	(	kIx(	(
<g/>
85	[number]	k4	85
%	%	kIx~	%
<g/>
)	)	kIx)	)
vody	voda	k1gFnPc1	voda
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
čistá	čistý	k2eAgFnSc1d1	čistá
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
iontů	ion	k1gInPc2	ion
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
od	od	k7c2	od
koncentrace	koncentrace	k1gFnSc2	koncentrace
iontů	ion	k1gInPc2	ion
mimo	mimo	k7c4	mimo
buňku	buňka	k1gFnSc4	buňka
(	(	kIx(	(
<g/>
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
mimobuněčném	mimobuněčný	k2eAgInSc6d1	mimobuněčný
prostoru	prostor	k1gInSc6	prostor
či	či	k8xC	či
prostě	prostě	k9	prostě
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
vnějškem	vnějšek	k1gInSc7	vnějšek
se	se	k3xPyFc4	se
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
draselných	draselný	k2eAgInPc2d1	draselný
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
sodné	sodný	k2eAgInPc4d1	sodný
ionty	ion	k1gInPc4	ion
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Gradienty	gradient	k1gInPc1	gradient
iontů	ion	k1gInPc2	ion
jsou	být	k5eAaImIp3nP	být
podstatné	podstatný	k2eAgInPc1d1	podstatný
pro	pro	k7c4	pro
osmoregulaci	osmoregulace	k1gFnSc4	osmoregulace
a	a	k8xC	a
mezi	mezi	k7c7	mezi
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
a	a	k8xC	a
vnějším	vnější	k2eAgNnSc7d1	vnější
prostředím	prostředí	k1gNnSc7	prostředí
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
udržuje	udržovat	k5eAaImIp3nS	udržovat
dynamická	dynamický	k2eAgFnSc1d1	dynamická
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
,	,	kIx,	,
např.	např.	kA	např.
díky	díky	k7c3	díky
činnosti	činnost	k1gFnSc3	činnost
sodno-draselné	sodnoraselný	k2eAgFnSc2d1	sodno-draselný
pumpy	pumpa	k1gFnSc2	pumpa
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
ionty	ion	k1gInPc7	ion
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Vápenaté	vápenatý	k2eAgInPc4d1	vápenatý
ionty	ion	k1gInPc4	ion
jsou	být	k5eAaImIp3nP	být
uměle	uměle	k6eAd1	uměle
udržovány	udržovat	k5eAaImNgFnP	udržovat
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
koncentraci	koncentrace	k1gFnSc6	koncentrace
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
totiž	totiž	k9	totiž
důležitou	důležitý	k2eAgFnSc4d1	důležitá
signální	signální	k2eAgFnSc4d1	signální
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
je	on	k3xPp3gFnPc4	on
třeba	třeba	k6eAd1	třeba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
mít	mít	k5eAaImF	mít
jen	jen	k9	jen
krátkodobě	krátkodobě	k6eAd1	krátkodobě
jako	jako	k9	jako
spouštěče	spouštěč	k1gInPc1	spouštěč
různých	různý	k2eAgFnPc2d1	různá
signálních	signální	k2eAgFnPc2d1	signální
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
buňka	buňka	k1gFnSc1	buňka
zabránila	zabránit	k5eAaPmAgFnS	zabránit
šokovým	šokový	k2eAgInPc3d1	šokový
stavům	stav	k1gInPc3	stav
při	při	k7c6	při
náhlých	náhlý	k2eAgFnPc6d1	náhlá
osmotických	osmotický	k2eAgFnPc6d1	osmotická
změnách	změna	k1gFnPc6	změna
<g/>
,	,	kIx,	,
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
přitomny	přitomen	k2eAgMnPc4d1	přitomen
i	i	k8xC	i
osmoprotektanty	osmoprotektant	k1gMnPc4	osmoprotektant
jako	jako	k8xC	jako
betainy	betain	k1gInPc4	betain
či	či	k8xC	či
trehalóza	trehalóza	k1gFnSc1	trehalóza
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
díky	díky	k7c3	díky
nim	on	k3xPp3gInPc3	on
buňky	buňka	k1gFnPc4	buňka
kompletně	kompletně	k6eAd1	kompletně
vyschout	vyschout	k5eAaPmF	vyschout
a	a	k8xC	a
přesto	přesto	k8xC	přesto
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
zvaném	zvaný	k2eAgInSc6d1	zvaný
kryptobióza	kryptobióza	k1gFnSc1	kryptobióza
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrobuněčné	vnitrobuněčný	k2eAgFnPc1d1	vnitrobuněčná
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
navázané	navázaný	k2eAgInPc1d1	navázaný
na	na	k7c4	na
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
membránu	membrána	k1gFnSc4	membrána
nebo	nebo	k8xC	nebo
na	na	k7c4	na
cytoskelet	cytoskelet	k1gInSc4	cytoskelet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozpuštěny	rozpustit	k5eAaPmNgFnP	rozpustit
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
proteinů	protein	k1gInPc2	protein
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
200	[number]	k4	200
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
ml	ml	kA	ml
a	a	k8xC	a
proteiny	protein	k1gInPc1	protein
mohou	moct	k5eAaImIp3nP	moct
zabírat	zabírat	k5eAaImF	zabírat
až	až	k9	až
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
%	%	kIx~	%
objemu	objem	k1gInSc3	objem
cytosolu	cytosola	k1gFnSc4	cytosola
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
stanovení	stanovení	k1gNnSc1	stanovení
množství	množství	k1gNnSc1	množství
proteinů	protein	k1gInPc2	protein
rozpuštěných	rozpuštěný	k2eAgInPc2d1	rozpuštěný
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
poměrně	poměrně	k6eAd1	poměrně
nesnadný	snadný	k2eNgInSc4d1	nesnadný
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
lze	lze	k6eAd1	lze
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
odlišit	odlišit	k5eAaPmF	odlišit
např.	např.	kA	např.
bílkoviny	bílkovina	k1gFnPc4	bílkovina
slabě	slabě	k6eAd1	slabě
asociované	asociovaný	k2eAgInPc1d1	asociovaný
s	s	k7c7	s
membránami	membrána	k1gFnPc7	membrána
či	či	k8xC	či
buněčnými	buněčný	k2eAgFnPc7d1	buněčná
organelami	organela	k1gFnPc7	organela
(	(	kIx(	(
<g/>
a	a	k8xC	a
do	do	k7c2	do
cytosolu	cytosol	k1gInSc2	cytosol
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
až	až	k9	až
při	při	k7c6	při
experimentu	experiment	k1gInSc6	experiment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opatrném	opatrný	k2eAgNnSc6d1	opatrné
narušení	narušení	k1gNnSc6	narušení
buněčné	buněčný	k2eAgFnSc2d1	buněčná
membrány	membrána	k1gFnSc2	membrána
saponinem	saponin	k1gInSc7	saponin
zůstane	zůstat	k5eAaPmIp3nS	zůstat
75	[number]	k4	75
%	%	kIx~	%
buněčných	buněčný	k2eAgInPc2d1	buněčný
proteinů	protein	k1gInPc2	protein
navázáno	navázat	k5eAaPmNgNnS	navázat
na	na	k7c4	na
buněčné	buněčný	k2eAgFnPc4d1	buněčná
membrány	membrána	k1gFnPc4	membrána
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
neuvolní	uvolnit	k5eNaPmIp3nS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
narušené	narušený	k2eAgFnPc1d1	narušená
buňky	buňka	k1gFnPc1	buňka
byly	být	k5eAaImAgFnP	být
schopné	schopný	k2eAgFnPc1d1	schopná
normálně	normálně	k6eAd1	normálně
vytvářet	vytvářet	k5eAaImF	vytvářet
bílkoviny	bílkovina	k1gFnPc4	bílkovina
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
dodáváno	dodávat	k5eAaImNgNnS	dodávat
ATP	atp	kA	atp
a	a	k8xC	a
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
enzymů	enzym	k1gInPc2	enzym
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
navázáno	navázat	k5eAaPmNgNnS	navázat
např.	např.	kA	např.
na	na	k7c4	na
cytoskelet	cytoskelet	k1gInSc4	cytoskelet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
navíc	navíc	k6eAd1	navíc
přítomna	přítomen	k2eAgFnSc1d1	přítomna
i	i	k8xC	i
DNA	DNA	kA	DNA
-	-	kIx~	-
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
uvnitř	uvnitř	k7c2	uvnitř
struktury	struktura	k1gFnSc2	struktura
zvané	zvaný	k2eAgFnSc2d1	zvaná
nukleoid	nukleoid	k1gInSc4	nukleoid
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
koncentrace	koncentrace	k1gFnSc1	koncentrace
makromolekul	makromolekula	k1gFnPc2	makromolekula
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
efektu	efekt	k1gInSc3	efekt
označovanému	označovaný	k2eAgInSc3d1	označovaný
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
literatuře	literatura	k1gFnSc6	literatura
jako	jako	k9	jako
"	"	kIx"	"
<g/>
macromolecular	macromolecular	k1gInSc1	macromolecular
crowding	crowding	k1gInSc1	crowding
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
makromolekulární	makromolekulární	k2eAgNnSc1d1	makromolekulární
nahloučení	nahloučení	k1gNnSc1	nahloučení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
makromolekuly	makromolekula	k1gFnPc1	makromolekula
přítomny	přítomen	k2eAgFnPc1d1	přítomna
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
koncentracích	koncentrace	k1gFnPc6	koncentrace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jejich	jejich	k3xOp3gFnSc1	jejich
efektivní	efektivní	k2eAgFnSc1d1	efektivní
koncentrace	koncentrace	k1gFnSc1	koncentrace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
takových	takový	k3xDgFnPc2	takový
podmínek	podmínka	k1gFnPc2	podmínka
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
reakční	reakční	k2eAgFnSc3d1	reakční
rychlosti	rychlost	k1gFnSc3	rychlost
<g/>
,	,	kIx,	,
vychylují	vychylovat	k5eAaImIp3nP	vychylovat
se	se	k3xPyFc4	se
chemické	chemický	k2eAgFnSc2d1	chemická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
disociačních	disociační	k2eAgFnPc2d1	disociační
konstant	konstanta	k1gFnPc2	konstanta
pro	pro	k7c4	pro
různé	různý	k2eAgNnSc4d1	různé
makromolekulární	makromolekulární	k2eAgNnSc4d1	makromolekulární
uspořádání	uspořádání	k1gNnSc4	uspořádání
(	(	kIx(	(
<g/>
příznivé	příznivý	k2eAgFnPc1d1	příznivá
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
proteinových	proteinový	k2eAgInPc2d1	proteinový
komplexů	komplex	k1gInPc2	komplex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
cytosol	cytosola	k1gFnPc2	cytosola
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
