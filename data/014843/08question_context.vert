<s>
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
portrétní	portrétní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
(	(	kIx(
<g/>
Scottish	Scottish	k1gInSc1
National	National	k1gMnSc1
Portrait	Portrait	k1gInSc1
Gallery	Galler	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
patří	patřit	k5eAaImIp3nS
rovněž	rovněž	k9
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
fotografií	fotografia	k1gFnPc2
(	(	kIx(
<g/>
Scottish	Scottish	k1gInSc1
National	National	k1gFnSc2
Photography	Photographa	k1gFnSc2
Collection	Collection	k1gInSc1
<g/>
)	)	kIx)
sbírá	sbírat	k5eAaImIp3nS
portréty	portrét	k1gInPc4
skotských	skotský	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
</s>