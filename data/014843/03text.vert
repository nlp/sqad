<s>
Skotské	skotský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
galerie	galerie	k1gFnPc1
</s>
<s>
Skotské	skotská	k1gFnPc1
národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
Údaje	údaj	k1gInSc2
o	o	k7c6
muzeu	muzeum	k1gNnSc6
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
(	(	kIx(
<g/>
Dean	Dean	k1gInSc1
Gallery	Galler	k1gInPc1
<g/>
)	)	kIx)
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
1859	#num#	k4
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
55	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
2,58	2,58	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
28,17	28,17	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Skotské	skotský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
galerie	galerie	k1gFnPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
National	National	k1gMnSc1
Galleries	Galleriesa	k1gFnPc2
of	of	k?
Scotland	Scotlanda	k1gFnPc2
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
NGS	NGS	kA
<g/>
,	,	kIx,
gaelsky	gaelsky	k6eAd1
Gailearaidhean	Gailearaidhean	k1gInSc1
Nà	Nà	k1gMnSc2
na	na	k7c6
h-Alba	h-Alba	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1
instituce	instituce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
řídí	řídit	k5eAaImIp3nS
tři	tři	k4xCgInPc4
skotské	skotský	k1gInPc4
národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
v	v	k7c6
Edinburghu	Edinburgh	k1gInSc6
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
jimi	on	k3xPp3gFnPc7
Skotská	skotská	k1gFnSc1
národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
<g/>
,	,	kIx,
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
portrétní	portrétní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
a	a	k8xC
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
dva	dva	k4xCgInPc4
venkovské	venkovský	k2eAgInPc4d1
zámky	zámek	k1gInPc4
zpřístupněné	zpřístupněný	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
vybavené	vybavený	k2eAgInPc4d1
uměleckými	umělecký	k2eAgInPc7d1
předměty	předmět	k1gInPc7
(	(	kIx(
<g/>
Duff	Duff	k1gInSc1
House	house	k1gNnSc1
v	v	k7c6
Banffu	Banff	k1gInSc6
v	v	k7c6
Aberdeenshire	Aberdeenshir	k1gInSc5
a	a	k8xC
Paxton	Paxton	k1gInSc4
House	house	k1gNnSc1
v	v	k7c6
Paxtonu	Paxton	k1gInSc6
v	v	k7c6
Berwickshire	Berwickshir	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Skotských	skotský	k2eAgFnPc2d1
národních	národní	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
a	a	k8xC
jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
péče	péče	k1gFnSc1
o	o	k7c4
národní	národní	k2eAgNnSc4d1
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skotské	skotský	k2eAgFnPc1d1
národní	národní	k2eAgFnPc1d1
galerie	galerie	k1gFnPc1
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
roku	rok	k1gInSc3
1985	#num#	k4
zákonem	zákon	k1gInSc7
National	National	k1gFnPc2
Heritage	Heritage	k1gFnPc2
(	(	kIx(
<g/>
Scotland	Scotland	k1gInSc1
<g/>
)	)	kIx)
Act	Act	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
(	(	kIx(
<g/>
Scottish	Scottish	k1gInSc1
National	National	k1gFnSc1
Gallery	Galler	k1gInPc4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1859	#num#	k4
a	a	k8xC
sídlí	sídlet	k5eAaImIp3nS
na	na	k7c6
pahorku	pahorek	k1gInSc6
The	The	k1gMnSc1
Mound	Mound	k1gMnSc1
v	v	k7c6
neoklasické	neoklasický	k2eAgFnSc6d1
budově	budova	k1gFnSc6
inspirované	inspirovaný	k2eAgFnSc6d1
antickými	antický	k2eAgInPc7d1
chrámy	chrám	k1gInPc7
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
William	William	k1gInSc4
Henry	Henry	k1gMnSc1
Playfair	Playfair	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbírky	sbírka	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
jednak	jednak	k8xC
díla	dílo	k1gNnPc1
předních	přední	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
zahraničních	zahraniční	k2eAgMnPc2d1
(	(	kIx(
<g/>
například	například	k6eAd1
Anthonis	Anthonis	k1gFnSc1
van	vana	k1gFnPc2
Dyck	Dyck	k1gMnSc1
<g/>
,	,	kIx,
Giambattista	Giambattista	k1gMnSc1
Tiepolo	Tiepola	k1gFnSc5
<g/>
,	,	kIx,
Rubens	Rubensa	k1gFnPc2
<g/>
,	,	kIx,
Tizian	Tiziana	k1gFnPc2
<g/>
,	,	kIx,
El	Ela	k1gFnPc2
Greco	Greco	k6eAd1
a	a	k8xC
Monet	moneta	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
sbírku	sbírka	k1gFnSc4
klasických	klasický	k2eAgMnPc2d1
malířů	malíř	k1gMnPc2
skotských	skotský	k1gInPc2
(	(	kIx(
<g/>
Alexander	Alexandra	k1gFnPc2
Nasmyth	Nasmyth	k1gInSc1
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
Raeburn	Raeburn	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
nejznámější	známý	k2eAgInSc1d3
skotský	skotský	k2eAgInSc1d1
obraz	obraz	k1gInSc1
Reverend	reverend	k1gMnSc1
Robert	Robert	k1gMnSc1
Walker	Walker	k1gMnSc1
bruslí	bruslit	k5eAaImIp3nS
na	na	k7c6
jezeře	jezero	k1gNnSc6
Duddingston	Duddingston	k1gInSc1
Loch	loch	k1gInSc1
připisovaný	připisovaný	k2eAgInSc1d1
Raeburnovi	Raeburn	k1gMnSc3
nebo	nebo	k8xC
nověji	nově	k6eAd2
i	i	k9
Danlouxovi	Danlouxův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
portrétní	portrétní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
(	(	kIx(
<g/>
Scottish	Scottish	k1gInSc1
National	National	k1gMnSc1
Portrait	Portrait	k1gInSc1
Gallery	Galler	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
patří	patřit	k5eAaImIp3nS
rovněž	rovněž	k9
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
sbírka	sbírka	k1gFnSc1
fotografií	fotografia	k1gFnPc2
(	(	kIx(
<g/>
Scottish	Scottish	k1gInSc1
National	National	k1gFnSc2
Photography	Photographa	k1gFnSc2
Collection	Collection	k1gInSc1
<g/>
)	)	kIx)
sbírá	sbírat	k5eAaImIp3nS
portréty	portrét	k1gInPc4
skotských	skotský	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sbírku	sbírka	k1gFnSc4
založil	založit	k5eAaPmAgInS
David	David	k1gMnSc1
Steuart	Steuarta	k1gFnPc2
Erskine	Erskin	k1gInSc5
koncem	konec	k1gInSc7
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
samostatná	samostatný	k2eAgFnSc1d1
portrétní	portrétní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
podle	podle	k7c2
vzoru	vzor	k1gInSc2
londýnské	londýnský	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
stejného	stejný	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1889	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novogotickou	novogotický	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
navrhl	navrhnout	k5eAaPmAgInS
Robert	Robert	k1gMnSc1
Rowand	Rowanda	k1gFnPc2
Anderson	Anderson	k1gMnSc1
<g/>
,	,	kIx,
uvnitř	uvnitř	k6eAd1
je	být	k5eAaImIp3nS
asi	asi	k9
3000	#num#	k4
obrazů	obraz	k1gInPc2
a	a	k8xC
soch	socha	k1gFnPc2
<g/>
,	,	kIx,
25	#num#	k4
000	#num#	k4
tisků	tisk	k1gInPc2
a	a	k8xC
grafik	grafika	k1gFnPc2
a	a	k8xC
38	#num#	k4
000	#num#	k4
fotografií	fotografia	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
(	(	kIx(
<g/>
Scottish	Scottish	k1gMnSc1
National	National	k1gFnSc2
Gallery	Galler	k1gInPc4
of	of	k?
Modern	Modern	k1gInSc1
Art	Art	k1gFnSc1
<g/>
)	)	kIx)
sbírá	sbírat	k5eAaImIp3nS
umění	umění	k1gNnSc1
vzniklé	vzniklý	k2eAgFnSc2d1
zhruba	zhruba	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1960	#num#	k4
a	a	k8xC
nyní	nyní	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
ve	v	k7c6
dvou	dva	k4xCgFnPc6
budovách	budova	k1gFnPc6
zvaných	zvaný	k2eAgFnPc2d1
Modern	Moderna	k1gFnPc2
One	One	k1gMnSc1
a	a	k8xC
Modern	Modern	k1gMnSc1
Two	Two	k1gMnSc1
na	na	k7c6
třídě	třída	k1gFnSc6
Belford	Belford	k1gMnSc1
Road	Road	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastoupeni	zastoupit	k5eAaPmNgMnP
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
Salvador	Salvador	k1gMnSc1
Dalí	Dalí	k1gMnSc1
<g/>
,	,	kIx,
René	René	k1gMnSc5
Magritte	Magritt	k1gMnSc5
<g/>
,	,	kIx,
Alberto	Alberta	k1gFnSc5
Giacometti	Giacometť	k1gFnSc5
<g/>
,	,	kIx,
Oskar	Oskar	k1gMnSc1
Kokoschka	Kokoschka	k1gMnSc1
<g/>
,	,	kIx,
Francis	Francis	k1gFnSc1
Bacon	Bacon	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Hockney	Hocknea	k1gFnSc2
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc1
Warhol	Warhol	k1gInSc1
<g/>
,	,	kIx,
Joan	Joan	k1gInSc1
Eardley	Eardlea	k1gFnSc2
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
Davie	Davie	k1gFnSc2
<g/>
,	,	kIx,
Douglas	Douglas	k1gMnSc1
Gordon	Gordon	k1gMnSc1
<g/>
,	,	kIx,
Antony	anton	k1gInPc1
Gormley	Gormlea	k1gFnSc2
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Priseman	Priseman	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Tracey	Tracea	k1gMnSc2
Eminová	Eminová	k1gFnSc1
<g/>
,	,	kIx,
Louise	Louis	k1gMnSc2
Bourgeois	Bourgeois	k1gFnSc2
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Mapplethorpe	Mapplethorp	k1gInSc5
a	a	k8xC
Damien	Damien	k2eAgInSc4d1
Hirst	Hirst	k1gInSc4
<g/>
;	;	kIx,
kolem	kolem	k7c2
budov	budova	k1gFnPc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
sochy	socha	k1gFnPc1
tvůrců	tvůrce	k1gMnPc2
jako	jako	k8xS,k8xC
Ian	Ian	k1gFnSc4
Hamilton	Hamilton	k1gInSc1
Finlay	Finlaa	k1gFnPc1
<g/>
,	,	kIx,
Barbara	Barbara	k1gFnSc1
Hepworth	Hepworth	k1gInSc1
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc5
Moore	Moor	k1gMnSc5
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
Rickey	Rickea	k1gMnSc2
<g/>
,	,	kIx,
Rachel	Rachel	k1gInSc1
Whiteread	Whiteread	k1gInSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Long	Long	k1gMnSc1
a	a	k8xC
Nathan	Nathan	k1gMnSc1
Coley	Colea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Skotské	skotský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ústřední	ústřední	k2eAgInSc1d1
a	a	k8xC
světově	světově	k6eAd1
významná	významný	k2eAgNnPc1d1
muzea	muzeum	k1gNnPc1
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
Itálie	Itálie	k1gFnSc2
a	a	k8xC
Vatikán	Vatikán	k1gInSc1
</s>
<s>
Benátky	Benátky	k1gFnPc1
Akademie	akademie	k1gFnSc2
•	•	k?
Florencie	Florencie	k1gFnSc2
Akademie	akademie	k1gFnSc2
/	/	kIx~
Bargello	Bargello	k1gNnSc1
/	/	kIx~
Pitti	Pitť	k1gFnPc1
/	/	kIx~
Uffizi	Uffize	k1gFnSc4
•	•	k?
Milán	Milán	k1gInSc1
Ambrosiana	Ambrosiana	k1gFnSc1
/	/	kIx~
Brera	Brera	k1gFnSc1
•	•	k?
Neapol	Neapol	k1gFnSc1
Capodimonte	Capodimont	k1gInSc5
•	•	k?
Řím	Řím	k1gInSc1
a	a	k8xC
Vatikán	Vatikán	k1gInSc1
Borghese	Borghese	k1gFnSc2
/	/	kIx~
Galerie	galerie	k1gFnSc1
starého	starý	k2eAgNnSc2d1
umění	umění	k1gNnSc2
/	/	kIx~
Kapitolská	kapitolský	k2eAgNnPc1d1
muzea	muzeum	k1gNnPc1
/	/	kIx~
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
/	/	kIx~
Vatikánská	vatikánský	k2eAgNnPc1d1
muzea	muzeum	k1gNnPc1
Francie	Francie	k1gFnSc2
a	a	k8xC
Benelux	Benelux	k1gInSc4
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1
Rijksmuseum	Rijksmuseum	k1gInSc1
/	/	kIx~
Stedelijk	Stedelijk	k1gInSc1
/	/	kIx~
Van	van	k1gInSc1
Gogh	Gogh	k1gInSc1
•	•	k?
Arnhem	Arnh	k1gInSc7
Muzeum	muzeum	k1gNnSc1
Kröllerové-Müllerové	Kröllerové-Müller	k1gMnPc1
•	•	k?
Brusel	Brusel	k1gInSc4
Královská	královský	k2eAgNnPc4d1
muzea	muzeum	k1gNnPc4
•	•	k?
Gent	Gent	k1gMnSc1
Muzeum	muzeum	k1gNnSc1
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Haag	Haag	k1gInSc1
Mauritshuis	Mauritshuis	k1gInSc1
•	•	k?
Chantilly	Chantilla	k1gFnSc2
Muzeum	muzeum	k1gNnSc1
Condéů	Condé	k1gMnPc2
•	•	k?
Kolmar	Kolmara	k1gFnPc2
Unterlinden	Unterlindno	k1gNnPc2
•	•	k?
Paříž	Paříž	k1gFnSc1
Branly	Branla	k1gFnSc2
/	/	kIx~
Centre	centr	k1gInSc5
Pompidou	Pompida	k1gFnSc7
/	/	kIx~
Cluny	Cluen	k2eAgFnPc4d1
/	/	kIx~
Louvre	Louvre	k1gInSc4
/	/	kIx~
Monet	moneta	k1gFnPc2
/	/	kIx~
Oranžerie	Oranžerie	k1gFnSc1
/	/	kIx~
d	d	k?
<g/>
'	'	kIx"
<g/>
Orsay	Orsaa	k1gMnSc2
/	/	kIx~
Petit	petit	k1gInSc1
Palais	Palais	k1gFnSc2
/	/	kIx~
Picasso	Picassa	k1gFnSc5
/	/	kIx~
Rodin	rodina	k1gFnPc2
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
a	a	k8xC
Irsko	Irsko	k1gNnSc1
</s>
<s>
Cambridge	Cambridge	k1gFnSc1
Fitzwilliamovo	Fitzwilliamův	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
•	•	k?
Dublin	Dublin	k1gInSc1
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
•	•	k?
Edinburgh	Edinburgh	k1gInSc4
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
•	•	k?
Glasgow	Glasgow	k1gInSc1
Kelvingrove	Kelvingrov	k1gInSc5
•	•	k?
Londýn	Londýn	k1gInSc4
Britské	britský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
/	/	kIx~
Courtauld	Courtauld	k1gInSc1
/	/	kIx~
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
/	/	kIx~
Národní	národní	k2eAgFnSc1d1
portrétní	portrétní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
/	/	kIx~
Tate	Tate	k1gFnSc1
/	/	kIx~
Viktorie	Viktorie	k1gFnSc1
a	a	k8xC
Albert	Albert	k1gMnSc1
•	•	k?
Oxford	Oxford	k1gInSc4
Ashmoleovo	Ashmoleův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Německo	Německo	k1gNnSc1
a	a	k8xC
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
Basilej	Basilej	k1gFnSc1
Umělecké	umělecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Berlín	Berlín	k1gInSc4
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
/	/	kIx~
Obrazová	obrazový	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
/	/	kIx~
Ostrov	ostrov	k1gInSc1
muzeí	muzeum	k1gNnPc2
•	•	k?
Brunšvik	Brunšvik	k1gInSc1
HAUM	HAUM	kA
•	•	k?
Curych	Curych	k1gInSc1
Dům	dům	k1gInSc1
umění	umění	k1gNnSc2
•	•	k?
Drážďany	Drážďany	k1gInPc1
Galerie	galerie	k1gFnSc1
mistrů	mistr	k1gMnPc2
<g/>
:	:	kIx,
starých	starý	k2eAgMnPc2d1
/	/	kIx~
nových	nový	k2eAgMnPc2d1
•	•	k?
Frankfurt	Frankfurt	k1gInSc1
n.	n.	k?
M.	M.	kA
Städelovo	Städelův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Hamburk	Hamburk	k1gInSc1
Kunsthalle	Kunsthalle	k1gInSc1
•	•	k?
Kolín	Kolín	k1gInSc1
n.	n.	k?
R.	R.	kA
Muzeum	muzeum	k1gNnSc1
Ludwigových	Ludwigův	k2eAgNnPc6d1
/	/	kIx~
Wallraf	Wallraf	k1gMnSc1
<g/>
–	–	k?
<g/>
Richartz	Richartz	k1gInSc1
•	•	k?
Mnichov	Mnichov	k1gInSc4
Bavorské	bavorský	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
/	/	kIx~
Muzeum	muzeum	k1gNnSc1
Brandhorstových	Brandhorstová	k1gFnPc2
/	/	kIx~
Pinakotéky	pinakotéka	k1gFnPc1
<g/>
:	:	kIx,
Stará	starý	k2eAgFnSc1d1
/	/	kIx~
Nová	Nová	k1gFnSc1
/	/	kIx~
Moderny	Moderna	k1gFnPc1
Severské	severský	k2eAgFnPc1d1
a	a	k8xC
pobaltské	pobaltský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Fredensborg	Fredensborg	k1gInSc1
Louisiana	Louisiana	k1gFnSc1
•	•	k?
Helsinki	Helsinki	k1gNnSc7
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
•	•	k?
Kodaň	Kodaň	k1gFnSc1
Glyptotéka	glyptotéka	k1gFnSc1
/	/	kIx~
Státní	státní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
umění	umění	k1gNnSc2
•	•	k?
Oslo	Oslo	k1gNnSc2
Munchovo	Munchův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
/	/	kIx~
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
umění	umění	k1gNnSc2
•	•	k?
Reykjavík	Reykjavík	k1gInSc4
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
•	•	k?
Riga	Riga	k1gFnSc1
Umělecké	umělecký	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
•	•	k?
Stockholm	Stockholm	k1gInSc1
Muzeum	muzeum	k1gNnSc1
moderny	moderna	k1gFnSc2
/	/	kIx~
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Tallinn	Tallinn	k1gMnSc1
Umělecké	umělecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Vilnius	Vilnius	k1gMnSc1
Umělecké	umělecký	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
Jihozápadní	jihozápadní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
Umělecké	umělecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Bilbao	Bilbao	k1gMnSc1
Guggenheim	Guggenheim	k1gMnSc1
•	•	k?
Figueres	Figueres	k1gMnSc1
Dalí	Dalí	k1gMnSc1
•	•	k?
Lisabon	Lisabon	k1gInSc1
Gulbenkian	Gulbenkian	k1gInSc1
/	/	kIx~
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
starého	starý	k2eAgNnSc2d1
umění	umění	k1gNnSc2
•	•	k?
Madrid	Madrid	k1gInSc1
Muzeum	muzeum	k1gNnSc1
královny	královna	k1gFnSc2
Sofie	Sofia	k1gFnSc2
/	/	kIx~
Muzeum	muzeum	k1gNnSc1
Thyssenů-Bornemiszů	Thyssenů-Bornemisz	k1gInPc2
/	/	kIx~
Prado	Prado	k1gNnSc1
Střední	střední	k2eAgFnSc2d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
•	•	k?
Brno	Brno	k1gNnSc4
Moravská	moravský	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
•	•	k?
Budapešť	Budapešť	k1gFnSc1
Muzeum	muzeum	k1gNnSc1
krásných	krásný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
/	/	kIx~
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
•	•	k?
Krakov	Krakov	k1gInSc4
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Praha	Praha	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
•	•	k?
Štýrský	štýrský	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
Joanneum	Joanneum	k1gInSc1
•	•	k?
Varšava	Varšava	k1gFnSc1
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Vídeň	Vídeň	k1gFnSc1
Albertina	Albertin	k2eAgFnSc1d1
/	/	kIx~
Belvedere	Belveder	k1gInSc5
/	/	kIx~
Muzeum	muzeum	k1gNnSc1
Leopoldových	Leopoldová	k1gFnPc2
/	/	kIx~
mumok	mumok	k1gInSc1
/	/	kIx~
Uměleckohistorické	uměleckohistorický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Vratislav	Vratislav	k1gMnSc1
Národní	národní	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
a	a	k8xC
Balkán	Balkán	k1gInSc1
</s>
<s>
Athény	Athéna	k1gFnPc1
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
•	•	k?
Bělehrad	Bělehrad	k1gInSc1
Národní	národní	k2eAgInSc1d1
muzeum	muzeum	k1gNnSc4
•	•	k?
Bukurešť	Bukurešť	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
umělecké	umělecký	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
•	•	k?
Kyjev	Kyjev	k1gInSc1
Národní	národní	k2eAgNnSc1d1
umělecké	umělecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Minsk	Minsk	k1gInSc4
Národní	národní	k2eAgNnSc1d1
umělecké	umělecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Lublaň	Lublaň	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
•	•	k?
Moskva	Moskva	k1gFnSc1
Puškinovo	Puškinův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
/	/	kIx~
Treťjakovská	Treťjakovský	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
•	•	k?
Petrohrad	Petrohrad	k1gInSc1
Ermitáž	Ermitáž	k1gFnSc1
/	/	kIx~
Ruské	ruský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Sibiň	Sibiň	k1gFnSc1
Brukenthal	Brukenthal	k1gMnSc1
•	•	k?
Sofie	Sofia	k1gFnSc2
Národní	národní	k2eAgFnSc1d1
umělecká	umělecký	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
•	•	k?
Záhřeb	Záhřeb	k1gInSc1
Moderní	moderní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2010612259	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
131536-5	131536-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0606	#num#	k4
7468	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82138771	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131056154	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82138771	#num#	k4
</s>
