<s>
Příběh	příběh	k1gInSc4
Nastagia	Nastagium	k1gNnSc2
degli	degl	k1gMnPc1
Onesti	Onest	k1gFnSc2
</s>
<s>
Příběh	příběh	k1gInSc4
Nastagia	Nastagium	k1gNnSc2
degli	degl	k1gMnPc1
Onesti	Onest	k1gFnSc2
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1483	#num#	k4
<g/>
,	,	kIx,
tempera	tempera	k1gFnSc1
na	na	k7c6
dřevě	dřevo	k1gNnSc6
<g/>
,	,	kIx,
83	#num#	k4
x	x	k?
138	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
Museo	Museo	k6eAd1
del	del	k?
Prado	Prado	k1gNnSc1
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Příběh	příběh	k1gInSc4
Nastagia	Nastagium	k1gNnSc2
degli	degl	k1gMnPc1
Onesti	Onest	k1gFnSc2
(	(	kIx(
<g/>
druhá	druhý	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1483	#num#	k4
<g/>
,	,	kIx,
tempera	tempera	k1gFnSc1
na	na	k7c6
dřevě	dřevo	k1gNnSc6
<g/>
,	,	kIx,
82	#num#	k4
x	x	k?
138	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
Museo	Museo	k6eAd1
del	del	k?
Prado	Prado	k1gNnSc1
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Příběh	příběh	k1gInSc4
Nastagia	Nastagium	k1gNnSc2
degli	degl	k1gMnPc1
Onesti	Onest	k1gFnSc2
(	(	kIx(
<g/>
třetí	třetí	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1483	#num#	k4
<g/>
,	,	kIx,
tempera	tempera	k1gFnSc1
na	na	k7c6
dřevě	dřevo	k1gNnSc6
<g/>
,	,	kIx,
83	#num#	k4
x	x	k?
142	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
Museo	Museo	k6eAd1
del	del	k?
Prado	Prado	k1gNnSc1
<g/>
,	,	kIx,
Madrid	Madrid	k1gInSc1
</s>
<s>
Příběh	příběh	k1gInSc4
Nastagia	Nastagium	k1gNnSc2
degli	degl	k1gMnPc1
Onesti	Onest	k1gFnSc2
(	(	kIx(
<g/>
čtvrtá	čtvrtá	k1gFnSc1
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1483	#num#	k4
<g/>
,	,	kIx,
tempera	tempera	k1gFnSc1
na	na	k7c6
dřevě	dřevo	k1gNnSc6
<g/>
,	,	kIx,
83	#num#	k4
x	x	k?
142	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
Palazzo	Palazza	k1gFnSc5
Pucci	Pucci	k?
<g/>
,	,	kIx,
Florencie	Florencie	k1gFnSc1
</s>
<s>
Příběh	příběh	k1gInSc1
Nastagia	Nastagia	k1gNnSc1
degli	degli	k1gNnSc1
Onesti	Onesti	k1gNnSc1
je	být	k5eAaImIp3nS
společný	společný	k2eAgInSc1d1
název	název	k1gInSc1
čtyř	čtyři	k4xCgInPc2
obrazů	obraz	k1gInPc2
italského	italský	k2eAgMnSc2d1
renesančního	renesanční	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Sandra	Sandro	k1gMnSc2
Botticelliho	Botticelli	k1gMnSc2xF
<g/>
,	,	kIx,
namalovaných	namalovaný	k2eAgNnPc2d1
v	v	k7c6
roce	rok	k1gInSc6
1483	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
obrazy	obraz	k1gInPc4
vyprávějící	vyprávějící	k2eAgInSc1d1
příběh	příběh	k1gInSc1
dvorské	dvorský	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
podle	podle	k7c2
námětu	námět	k1gInSc2
díla	dílo	k1gNnSc2
renesančního	renesanční	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Giovanniho	Giovanni	k1gMnSc2
Boccaccia	Boccaccia	k1gFnSc1
Dekameron	Dekameron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
příběhu	příběh	k1gInSc2
je	být	k5eAaImIp3nS
rytíř	rytíř	k1gMnSc1
Nastagio	Nastagio	k6eAd1
degli	degnout	k5eAaPmAgMnP,k5eAaImAgMnP
Onesti	Onest	k1gMnPc1
z	z	k7c2
Ravenny	Ravenna	k1gFnSc2
zklamaný	zklamaný	k2eAgMnSc1d1
ze	z	k7c2
ztracené	ztracený	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
své	svůj	k3xOyFgFnSc2
milé	milá	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
utišil	utišit	k5eAaPmAgMnS
bolest	bolest	k1gFnSc4
<g/>
,	,	kIx,
hledá	hledat	k5eAaImIp3nS
samotu	samota	k1gFnSc4
v	v	k7c6
piniovém	piniový	k2eAgInSc6d1
lese	les	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
vidí	vidět	k5eAaImIp3nS
utíkající	utíkající	k2eAgFnSc4d1
mladou	mladý	k2eAgFnSc4d1
ženu	žena	k1gFnSc4
pronásledovanou	pronásledovaný	k2eAgFnSc4d1
rytířem	rytíř	k1gMnSc7
na	na	k7c6
koni	kůň	k1gMnSc6
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
psem	pes	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pes	pes	k1gMnSc1
už	už	k9
mladou	mladý	k2eAgFnSc4d1
ženu	žena	k1gFnSc4
dostihl	dostihnout	k5eAaPmAgMnS
a	a	k8xC
zakousl	zakousnout	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
druhém	druhý	k4xOgInSc6
obraze	obraz	k1gInSc6
příběh	příběh	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
zabitím	zabití	k1gNnSc7
bezmocné	bezmocný	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
a	a	k8xC
vytržením	vytržení	k1gNnSc7
srdce	srdce	k1gNnSc2
z	z	k7c2
jejího	její	k3xOp3gNnSc2
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k8xC
rytíř	rytíř	k1gMnSc1
její	její	k3xOp3gNnSc4
srdce	srdce	k1gNnSc4
předhodí	předhodit	k5eAaPmIp3nS
svému	svůj	k3xOyFgMnSc3
psu	pes	k1gMnSc3
(	(	kIx(
<g/>
na	na	k7c6
obraze	obraz	k1gInSc6
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kupodivu	kupodivu	k6eAd1
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
nezraněna	zraněn	k2eNgFnSc1d1
vstává	vstávat	k5eAaImIp3nS
a	a	k8xC
strašný	strašný	k2eAgInSc1d1
koloběh	koloběh	k1gInSc1
událostí	událost	k1gFnPc2
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
třetím	třetí	k4xOgInSc6
obraze	obraz	k1gInSc6
Nastagio	Nastagio	k6eAd1
pozve	pozvat	k5eAaPmIp3nS
svou	svůj	k3xOyFgFnSc4
milou	milá	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ho	on	k3xPp3gMnSc4
opustila	opustit	k5eAaPmAgFnS
<g/>
,	,	kIx,
na	na	k7c4
hostinu	hostina	k1gFnSc4
uprostřed	uprostřed	k7c2
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
hodování	hodování	k1gNnSc2
náhle	náhle	k6eAd1
přibíhá	přibíhat	k5eAaImIp3nS
mladá	mladý	k2eAgFnSc1d1
žena	žena	k1gFnSc1
pronásledována	pronásledovat	k5eAaImNgFnS
rytířem	rytíř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překvapeným	překvapený	k2eAgMnPc3d1
účastníkům	účastník	k1gMnPc3
hostiny	hostina	k1gFnSc2
Nastagio	Nastagio	k6eAd1
vysvětluje	vysvětlovat	k5eAaImIp3nS
příběh	příběh	k1gInSc1
s	s	k7c7
mladou	mladý	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
milá	milá	k1gFnSc1
<g/>
,	,	kIx,
poznávajíc	poznávat	k5eAaImSgFnS
na	na	k7c4
příběhu	příběh	k1gInSc3
neznámé	známý	k2eNgFnSc2d1
ženy	žena	k1gFnSc2
peklo	peklo	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnPc3,k3yIgMnPc3,k3yQgMnPc3
musela	muset	k5eAaImAgFnS
projít	projít	k5eAaPmF
<g/>
,	,	kIx,
si	se	k3xPyFc3
uvědomí	uvědomit	k5eAaPmIp3nS
vlastní	vlastní	k2eAgNnSc4d1
chování	chování	k1gNnSc4
<g/>
,	,	kIx,
vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
k	k	k7c3
Nastagiovi	Nastagius	k1gMnSc3
a	a	k8xC
souhlasí	souhlasit	k5eAaImIp3nP
se	s	k7c7
sňatkem	sňatek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatba	svatba	k1gFnSc1
<g/>
,	,	kIx,
zachycená	zachycený	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
čtvrtá	čtvrtý	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
pod	pod	k7c7
vítězným	vítězný	k2eAgInSc7d1
obloukem	oblouk	k1gInSc7
symbolizujícím	symbolizující	k2eAgInSc7d1
vítězství	vítězství	k1gNnSc4
lásky	láska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obrazy	obraz	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
na	na	k7c4
objednávku	objednávka	k1gFnSc4
florentského	florentský	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
Antonia	Antonio	k1gMnSc2
Pucciho	Pucci	k1gMnSc2
při	při	k7c6
příležitosti	příležitost	k1gFnSc6
svatby	svatba	k1gFnSc2
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
syna	syn	k1gMnSc2
Gianozza	Gianozz	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pucciovci	Pucciovec	k1gInSc6
<g/>
,	,	kIx,
spříznění	spříznění	k1gNnSc6
s	s	k7c7
vládnoucími	vládnoucí	k2eAgInPc7d1
Medicejskými	Medicejský	k2eAgInPc7d1
<g/>
,	,	kIx,
patřili	patřit	k5eAaImAgMnP
k	k	k7c3
významným	významný	k2eAgInPc3d1
Florentským	florentský	k2eAgInPc3d1
rodům	rod	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrazy	obraz	k1gInPc7
původně	původně	k6eAd1
visely	viset	k5eAaImAgInP
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
místností	místnost	k1gFnPc2
rodového	rodový	k2eAgInSc2d1
paláce	palác	k1gInSc2
a	a	k8xC
plnily	plnit	k5eAaImAgFnP
funkce	funkce	k1gFnPc1
nástěnné	nástěnný	k2eAgFnPc1d1
dekorace	dekorace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Botticelli	Botticell	k1gMnPc7
při	při	k7c6
malování	malování	k1gNnSc6
kolekce	kolekce	k1gFnSc2
přibral	přibrat	k5eAaPmAgInS
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
dva	dva	k4xCgMnPc4
umělce	umělec	k1gMnPc4
své	svůj	k3xOyFgFnSc2
dílny	dílna	k1gFnSc2
-	-	kIx~
Bartolomea	Bartolome	k2eAgFnSc1d1
di	di	k?
Giovanni	Giovaneň	k1gFnSc3
a	a	k8xC
Jacopa	Jacopa	k1gFnSc1
del	del	k?
Sellaio	Sellaio	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
epizodách	epizoda	k1gFnPc6
zachycených	zachycený	k2eAgFnPc6d1
na	na	k7c6
třetím	třetí	k4xOgMnSc6
a	a	k8xC
čtvrtém	čtvrtý	k4xOgInSc6
obraze	obraz	k1gInSc6
splývá	splývat	k5eAaImIp3nS
fikce	fikce	k1gFnSc1
a	a	k8xC
realita	realita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vymyšleného	vymyšlený	k2eAgInSc2d1
příběhu	příběh	k1gInSc2
zde	zde	k6eAd1
nacházíme	nacházet	k5eAaImIp1nP
prvky	prvek	k1gInPc4
skutečného	skutečný	k2eAgInSc2d1
světa	svět	k1gInSc2
-	-	kIx~
erby	erb	k1gInPc4
Pucciovců	Pucciovec	k1gMnPc2
a	a	k8xC
Medicejských	Medicejský	k2eAgMnPc2d1
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
portréty	portrét	k1gInPc4
několika	několik	k4yIc7
příslušníků	příslušník	k1gMnPc2
obou	dva	k4xCgFnPc2
rodů	rod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
kolekce	kolekce	k1gFnSc1
obrazů	obraz	k1gInPc2
umístěna	umístit	k5eAaPmNgFnS
ve	v	k7c6
výstavních	výstavní	k2eAgFnPc6d1
expozicích	expozice	k1gFnPc6
Madridského	madridský	k2eAgMnSc2d1
Prada	Prad	k1gMnSc2
(	(	kIx(
<g/>
první	první	k4xOgInPc4
tři	tři	k4xCgInPc4
obrazy	obraz	k1gInPc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
čtvrtý	čtvrtý	k4xOgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
soukromé	soukromý	k2eAgFnSc6d1
sbírce	sbírka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Príbeh	Príbeh	k1gInSc4
Nastagia	Nastagium	k1gNnSc2
degli	degl	k1gMnPc1
Onesti	Onest	k1gMnPc1
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
B.	B.	kA
Deimlingová	Deimlingový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Sandro	Sandra	k1gFnSc5
Botticelli	Botticell	k1gMnSc3
<g/>
,	,	kIx,
Taschen	Taschen	k1gInSc1
/	/	kIx~
Nakladatelství	nakladatelství	k1gNnSc1
Slovart	Slovarta	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7209-515-3	80-7209-515-3	k4
</s>
<s>
B.	B.	kA
Wadiová	Wadiový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Botticelli	Botticell	k1gMnPc1
<g/>
,	,	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1971	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Příběh	příběh	k1gInSc4
Nastagia	Nastagium	k1gNnSc2
degli	degl	k1gMnPc1
Onesti	Onest	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Itálie	Itálie	k1gFnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
