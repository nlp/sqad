<s>
V	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
existovala	existovat	k5eAaImAgFnS	existovat
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
ještě	ještě	k6eAd1	ještě
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
fakulta	fakulta	k1gFnSc1	fakulta
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
FaF	FaF	k1gMnSc1	FaF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
farmacie	farmacie	k1gFnSc1	farmacie
realizované	realizovaný	k2eAgNnSc4d1	realizované
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
.	.	kIx.	.
</s>
