<s>
Modřín	modřín	k1gInSc1	modřín
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
borovicovité	borovicovitý	k2eAgFnSc2d1	borovicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
opadavé	opadavý	k2eAgNnSc4d1	opadavé
<g/>
,	,	kIx,	,
15-50	[number]	k4	15-50
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgInPc4d1	vysoký
jehličnaté	jehličnatý	k2eAgInPc4d1	jehličnatý
stromy	strom	k1gInPc4	strom
s	s	k7c7	s
rovnými	rovný	k2eAgInPc7d1	rovný
kmeny	kmen	k1gInPc7	kmen
a	a	k8xC	a
rovnovážně	rovnovážně	k6eAd1	rovnovážně
odstálými	odstálý	k2eAgFnPc7d1	odstálá
nebo	nebo	k8xC	nebo
převislými	převislý	k2eAgFnPc7d1	převislá
větvemi	větev	k1gFnPc7	větev
a	a	k8xC	a
měkkými	měkký	k2eAgFnPc7d1	měkká
tenkými	tenký	k2eAgFnPc7d1	tenká
jehlicemi	jehlice	k1gFnPc7	jehlice
<g/>
,	,	kIx,	,
vyrůstajícími	vyrůstající	k2eAgFnPc7d1	vyrůstající
jednak	jednak	k8xC	jednak
jednotlivě	jednotlivě	k6eAd1	jednotlivě
po	po	k7c6	po
větvích	větev	k1gFnPc6	větev
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
po	po	k7c6	po
svazečcích	svazeček	k1gInPc6	svazeček
z	z	k7c2	z
brachyblastů	brachyblast	k1gInPc2	brachyblast
<g/>
.	.	kIx.	.
</s>
<s>
Jehlice	jehlice	k1gFnPc1	jehlice
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
,	,	kIx,	,
na	na	k7c6	na
rubu	rub	k1gInSc6	rub
či	či	k8xC	či
oboustranně	oboustranně	k6eAd1	oboustranně
kýlnaté	kýlnatý	k2eAgFnPc1d1	kýlnatá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc4	dva
článkované	článkovaný	k2eAgInPc4d1	článkovaný
pryskyřičné	pryskyřičný	k2eAgInPc4d1	pryskyřičný
kanálky	kanálek	k1gInPc4	kanálek
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
10-14	[number]	k4	10-14
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgInSc1d1	původní
modřín	modřín	k1gInSc1	modřín
opadavý	opadavý	k2eAgInSc1d1	opadavý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
i	i	k9	i
modřín	modřín	k1gInSc1	modřín
japonský	japonský	k2eAgInSc1d1	japonský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
kaempferi	kaempfer	k1gFnSc2	kaempfer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
modřínu	modřín	k1gInSc2	modřín
je	být	k5eAaImIp3nS	být
břem	břem	k?	břem
nebo	nebo	k8xC	nebo
verpán	verpán	k1gInSc1	verpán
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Larix	Larix	k1gInSc1	Larix
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
rozdělován	rozdělovat	k5eAaImNgInS	rozdělovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
podskupiny	podskupina	k1gFnPc4	podskupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buďto	buďto	k8xC	buďto
délky	délka	k1gFnPc1	délka
jehlic	jehlice	k1gFnPc2	jehlice
na	na	k7c4	na
Larix	Larix	k1gInSc4	Larix
(	(	kIx(	(
<g/>
krátké	krátký	k2eAgInPc4d1	krátký
<g/>
)	)	kIx)	)
a	a	k8xC	a
Multiserialis	Multiserialis	k1gFnPc1	Multiserialis
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dle	dle	k7c2	dle
genetického	genetický	k2eAgMnSc2d1	genetický
(	(	kIx(	(
<g/>
Gernandt	Gernandt	k1gInSc1	Gernandt
&	&	k?	&
Liston	Liston	k1gInSc1	Liston
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
a	a	k8xC	a
geografického	geografický	k2eAgNnSc2d1	geografické
rozčlenění	rozčlenění	k1gNnSc2	rozčlenění
na	na	k7c4	na
modříny	modřín	k1gInPc4	modřín
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
možnostem	možnost	k1gFnPc3	možnost
křížení	křížení	k1gNnSc2	křížení
a	a	k8xC	a
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
kultivacím	kultivace	k1gFnPc3	kultivace
jsou	být	k5eAaImIp3nP	být
hranice	hranice	k1gFnPc1	hranice
mezi	mezi	k7c4	mezi
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
poddruhy	poddruh	k1gInPc4	poddruh
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodu	rod	k1gInSc2	rod
někdy	někdy	k6eAd1	někdy
dosti	dosti	k6eAd1	dosti
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Modříny	modřín	k1gInPc1	modřín
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
modřín	modřín	k1gInSc1	modřín
opadavý	opadavý	k2eAgInSc1d1	opadavý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
-	-	kIx~	-
hory	hora	k1gFnPc4	hora
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
modřín	modřín	k1gInSc1	modřín
dahurský	dahurský	k2eAgInSc1d1	dahurský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
gmelinii	gmelinie	k1gFnSc4	gmelinie
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
dahurica	dahurica	k1gFnSc1	dahurica
<g/>
)	)	kIx)	)
-	-	kIx~	-
pláně	pláň	k1gFnPc4	pláň
a	a	k8xC	a
rozdtroušené	rozdtroušený	k2eAgInPc4d1	rozdtroušený
lesy	les	k1gInPc4	les
východní	východní	k2eAgFnSc2d1	východní
Sibiře	Sibiř	k1gFnSc2	Sibiř
modřín	modřín	k1gInSc1	modřín
himálajský	himálajský	k2eAgInSc1d1	himálajský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
griffithii	griffithie	k1gFnSc4	griffithie
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
griffithiana	griffithiana	k1gFnSc1	griffithiana
<g/>
)	)	kIx)	)
-	-	kIx~	-
východní	východní	k2eAgFnSc1d1	východní
Himálaj	Himálaj	k1gFnSc1	Himálaj
Larix	Larix	k1gInSc1	Larix
himalaica	himalaica	k1gFnSc1	himalaica
(	(	kIx(	(
<g/>
modřín	modřín	k1gInSc1	modřín
langtang	langtang	k1gInSc1	langtang
<g/>
)	)	kIx)	)
-	-	kIx~	-
střední	střední	k2eAgFnSc1d1	střední
Himálaj	Himálaj	k1gFnSc1	Himálaj
modřín	modřín	k1gInSc1	modřín
japonský	japonský	k2eAgInSc1d1	japonský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
kaempferi	kaempfer	k1gFnSc2	kaempfer
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
leptolepis	leptolepis	k1gFnSc1	leptolepis
<g/>
)	)	kIx)	)
-	-	kIx~	-
pohoří	pohořet	k5eAaPmIp3nS	pohořet
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
modřín	modřín	k1gInSc1	modřín
tibetský	tibetský	k2eAgInSc1d1	tibetský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
kongboensis	kongboensis	k1gFnSc2	kongboensis
<g/>
)	)	kIx)	)
-	-	kIx~	-
jihovýchodní	jihovýchodní	k2eAgInSc1d1	jihovýchodní
Tibet	Tibet	k1gInSc1	Tibet
modřín	modřín	k1gInSc1	modřín
Mastersův	Mastersův	k2eAgInSc1d1	Mastersův
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
mastersiana	mastersian	k1gMnSc2	mastersian
<g/>
)	)	kIx)	)
-	-	kIx~	-
hory	hora	k1gFnPc1	hora
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Číny	Čína	k1gFnSc2	Čína
modřín	modřín	k1gInSc1	modřín
Potainův	Potainův	k2eAgInSc1d1	Potainův
Larix	Larix	k1gInSc1	Larix
potaninii	potaninie	k1gFnSc3	potaninie
-	-	kIx~	-
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
modřín	modřín	k1gInSc1	modřín
prince	princ	k1gMnSc2	princ
Rupprechta	Rupprecht	k1gMnSc2	Rupprecht
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc4	Larix
principis-rupprechtii	principisupprechtie	k1gFnSc4	principis-rupprechtie
<g/>
)	)	kIx)	)
-	-	kIx~	-
severní	severní	k2eAgFnSc1d1	severní
Čína	Čína	k1gFnSc1	Čína
modřín	modřín	k1gInSc1	modřín
sibiřský	sibiřský	k2eAgInSc1d1	sibiřský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
sibirica	sibiric	k1gInSc2	sibiric
<g/>
)	)	kIx)	)
-	-	kIx~	-
západní	západní	k2eAgFnSc1d1	západní
Sibiř	Sibiř	k1gFnSc1	Sibiř
modřín	modřín	k1gInSc1	modřín
junnanský	junnanský	k2eAgInSc1d1	junnanský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
speciosa	speciosa	k1gFnSc1	speciosa
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejjihozápadnější	jihozápadný	k2eAgFnSc1d3	jihozápadný
Čína	Čína	k1gFnSc1	Čína
Modříny	modřín	k1gInPc4	modřín
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
modřín	modřín	k1gInSc1	modřín
americký	americký	k2eAgInSc1d1	americký
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
modřínovitý	modřínovitý	k2eAgInSc1d1	modřínovitý
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
laricina	laricin	k2eAgFnSc1d1	laricin
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Larix	Larix	k1gInSc1	Larix
americana	americana	k1gFnSc1	americana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pláně	pláň	k1gFnPc1	pláň
a	a	k8xC	a
roztroušené	roztroušený	k2eAgInPc1d1	roztroušený
lesy	les	k1gInPc1	les
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Larix	Larix	k1gInSc1	Larix
lyallii	lyallie	k1gFnSc3	lyallie
(	(	kIx(	(
<g/>
modřín	modřín	k1gInSc1	modřín
subalpinský	subalpinský	k2eAgInSc1d1	subalpinský
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
severozápad	severozápad	k1gInSc1	severozápad
USA	USA	kA	USA
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc4	jihozápad
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
modřín	modřín	k1gInSc4	modřín
západní	západní	k2eAgFnPc1d1	západní
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
occidentalis	occidentalis	k1gFnSc2	occidentalis
<g/>
)	)	kIx)	)
-	-	kIx~	-
severozápad	severozápad	k1gInSc1	severozápad
USA	USA	kA	USA
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc4	jihozápad
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
podhůří	podhůří	k1gNnSc1	podhůří
Květena	květena	k1gFnSc1	květena
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
/	/	kIx~	/
S.	S.	kA	S.
Hejný	Hejný	k1gMnSc1	Hejný
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Slavík	slavík	k1gInSc1	slavík
(	(	kIx(	(
<g/>
Eds	Eds	k1gFnSc1	Eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
-	-	kIx~	-
S.	S.	kA	S.
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
309	[number]	k4	309
<g/>
.	.	kIx.	.
-	-	kIx~	-
ISBN	ISBN	kA	ISBN
80-200-0643-5	[number]	k4	80-200-0643-5
MUSIL	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
;	;	kIx,	;
HAMERNÍK	hamerník	k1gMnSc1	hamerník
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnaté	jehličnatý	k2eAgFnPc4d1	jehličnatá
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1567	[number]	k4	1567
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
168	[number]	k4	168
<g/>
-	-	kIx~	-
<g/>
186	[number]	k4	186
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
modřín	modřín	k1gInSc1	modřín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Modřín	modřín	k1gInSc4	modřín
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Modřín	modřín	k1gInSc4	modřín
na	na	k7c4	na
biolibu	bioliba	k1gFnSc4	bioliba
</s>
