<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
opadavé	opadavý	k2eAgNnSc4d1
<g/>
,	,	kIx,
15-50	[number]	k4
metrů	metr	k1gInPc2
vysoké	vysoký	k2eAgInPc4d1
jehličnaté	jehličnatý	k2eAgInPc4d1
stromy	strom	k1gInPc4
s	s	k7c7
rovnými	rovný	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
a	a	k8xC
rovnovážně	rovnovážně	k6eAd1
odstálými	odstálý	k2eAgFnPc7d1
nebo	nebo	k8xC
převislými	převislý	k2eAgFnPc7d1
větvemi	větev	k1gFnPc7
a	a	k8xC
měkkými	měkký	k2eAgFnPc7d1
tenkými	tenký	k2eAgFnPc7d1
jehlicemi	jehlice	k1gFnPc7
<g/>
,	,	kIx,
vyrůstajícími	vyrůstající	k2eAgFnPc7d1
jednak	jednak	k8xC
jednotlivě	jednotlivě	k6eAd1
po	po	k7c6
větvích	větev	k1gFnPc6
<g/>
,	,	kIx,
jednak	jednak	k8xC
po	po	k7c6
svazečcích	svazeček	k1gInPc6
z	z	k7c2
brachyblastů	brachyblast	k1gInPc2
<g/>
.	.	kIx.
</s>