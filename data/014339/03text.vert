<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
2	#num#	k4
(	(	kIx(
<g/>
model	model	k1gInSc1
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
3	#num#	k4
(	(	kIx(
<g/>
model	model	k1gInSc1
B	B	kA
<g/>
)	)	kIx)
</s>
<s>
Raspberry	Raspberry	k1gFnSc1
Pi	pi	k0
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ˈ	ˈ	k1gNnSc1
pai	pai	k?
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malý	malý	k2eAgInSc1d1
jednodeskový	jednodeskový	k2eAgInSc1d1
počítač	počítač	k1gInSc1
s	s	k7c7
deskou	deska	k1gFnSc7
plošných	plošný	k2eAgInPc2d1
spojů	spoj	k1gInPc2
o	o	k7c6
velikosti	velikost	k1gFnSc6
zhruba	zhruba	k6eAd1
platební	platební	k2eAgFnSc2d1
karty	karta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
britskou	britský	k2eAgFnSc7d1
nadací	nadace	k1gFnSc7
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundantion	Foundantion	k1gInSc1
s	s	k7c7
cílem	cíl	k1gInSc7
podpořit	podpořit	k5eAaPmF
výuku	výuka	k1gFnSc4
informatiky	informatika	k1gFnSc2
ve	v	k7c6
školách	škola	k1gFnPc6
a	a	k8xC
seznámit	seznámit	k5eAaPmF
studenty	student	k1gMnPc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
mohou	moct	k5eAaImIp3nP
počítače	počítač	k1gInPc4
řídit	řídit	k5eAaImF
různá	různý	k2eAgNnPc4d1
zařízení	zařízení	k1gNnPc4
(	(	kIx(
<g/>
např.	např.	kA
mikrovlnná	mikrovlnný	k2eAgFnSc1d1
trouba	trouba	k1gFnSc1
<g/>
,	,	kIx,
automatická	automatický	k2eAgFnSc1d1
pračka	pračka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primárním	primární	k2eAgInSc7d1
operačním	operační	k2eAgInSc7d1
systémem	systém	k1gInSc7
je	být	k5eAaImIp3nS
Raspbian	Raspbian	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
srpnu	srpen	k1gInSc6
2019	#num#	k4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
157	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
536	#num#	k4
Kč	Kč	kA
(	(	kIx(
<g/>
nejlevnější	levný	k2eAgNnSc1d3
je	on	k3xPp3gFnPc4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
Zero	Zero	k6eAd1
<g/>
,	,	kIx,
nejdražší	drahý	k2eAgFnSc7d3
pak	pak	k6eAd1
Raspberry	Raspberr	k1gMnPc7
Pi	pi	k0
4	#num#	k4
Model	model	k1gInSc1
B	B	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
existuje	existovat	k5eAaImIp3nS
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
Pico	Pico	k6eAd1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
mikrokontroléry	mikrokontrolér	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
„	„	k?
<g/>
Raspberry	Raspberra	k1gMnSc2
Pi	pi	k0
<g/>
“	“	k?
je	být	k5eAaImIp3nS
registrovanou	registrovaný	k2eAgFnSc7d1
ochrannou	ochranný	k2eAgFnSc7d1
známkou	známka	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
mají	mít	k5eAaImIp3nP
podobně	podobně	k6eAd1
navržené	navržený	k2eAgInPc4d1
počítače	počítač	k1gInPc4
zdánlivě	zdánlivě	k6eAd1
odvozené	odvozený	k2eAgInPc4d1
názvy	název	k1gInPc4
(	(	kIx(
<g/>
např.	např.	kA
Banana	Banana	k1gFnSc1
Pi	pi	k0
<g/>
,	,	kIx,
Windows	Windows	kA
IoT	IoT	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
je	být	k5eAaImIp3nS
jednodeskový	jednodeskový	k2eAgInSc1d1
počítač	počítač	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
srovnatelný	srovnatelný	k2eAgInSc1d1
se	s	k7c7
(	(	kIx(
<g/>
slabším	slabý	k2eAgInSc7d2
<g/>
)	)	kIx)
stolním	stolní	k2eAgInSc7d1
počítačem	počítač	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
vývod	vývod	k1gInSc4
pro	pro	k7c4
monitor	monitor	k1gInSc4
(	(	kIx(
<g/>
HDMI	HDMI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přes	přes	k7c4
USB	USB	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
připojit	připojit	k5eAaPmF
klávesnici	klávesnice	k1gFnSc4
a	a	k8xC
myš	myš	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyvinuto	vyvinut	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
již	již	k6eAd1
několik	několik	k4yIc1
generací	generace	k1gFnPc2
tohoto	tento	k3xDgInSc2
počítače	počítač	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
výkonem	výkon	k1gInSc7
a	a	k8xC
zamýšleným	zamýšlený	k2eAgNnSc7d1
použitím	použití	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použitý	použitý	k2eAgInSc1d1
mikroprocesor	mikroprocesor	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
rodiny	rodina	k1gFnSc2
ARM	ARM	kA
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
srovnatelný	srovnatelný	k2eAgInSc1d1
s	s	k7c7
běžným	běžný	k2eAgInSc7d1
smartphonem	smartphon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počítači	počítač	k1gInSc6
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
je	on	k3xPp3gMnPc4
možné	možný	k2eAgNnSc1d1
provozovat	provozovat	k5eAaImF
různé	různý	k2eAgFnPc4d1
distribuce	distribuce	k1gFnPc4
Linuxu	linux	k1gInSc2
<g/>
,	,	kIx,
RISC	RISC	kA
OS	OS	kA
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
Microsoft	Microsoft	kA
Windows	Windows	kA
10	#num#	k4
IoT	IoT	k1gFnSc2
Core	Core	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
platformy	platforma	k1gFnSc2
Arduino	Arduino	k1gNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgFnPc4d1
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
použít	použít	k5eAaPmF
nejen	nejen	k6eAd1
k	k	k7c3
ovládání	ovládání	k1gNnSc3
různých	různý	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
pomocí	pomocí	k7c2
GPIO	GPIO	kA
kontaktů	kontakt	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
k	k	k7c3
samotnému	samotný	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
příslušných	příslušný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
ho	on	k3xPp3gMnSc4
též	též	k9
použít	použít	k5eAaPmF
jako	jako	k8xS,k8xC
multimediální	multimediální	k2eAgInSc1d1
přehrávač	přehrávač	k1gInSc1
videa	video	k1gNnSc2
nebo	nebo	k8xC
hudby	hudba	k1gFnSc2
nebo	nebo	k8xC
i	i	k9
jen	jen	k9
pro	pro	k7c4
přístup	přístup	k1gInSc4
k	k	k7c3
Internetu	Internet	k1gInSc3
(	(	kIx(
<g/>
dodávané	dodávaný	k2eAgFnPc1d1
linuxové	linuxový	k2eAgFnPc1d1
distribuce	distribuce	k1gFnPc1
obsahují	obsahovat	k5eAaImIp3nP
webový	webový	k2eAgInSc4d1
prohlížeč	prohlížeč	k1gInSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
potřebné	potřebný	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hardware	hardware	k1gInSc1
</s>
<s>
Blokové	blokový	k2eAgNnSc1d1
schéma	schéma	k1gNnSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
</s>
<s>
Na	na	k7c6
obrázku	obrázek	k1gInSc6
vpravo	vpravo	k6eAd1
je	být	k5eAaImIp3nS
blokové	blokový	k2eAgNnSc1d1
schéma	schéma	k1gNnSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všimněte	všimnout	k5eAaPmRp2nP
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
ethernetový	ethernetový	k2eAgInSc1d1
port	port	k1gInSc1
(	(	kIx(
<g/>
tj.	tj.	kA
síťová	síťový	k2eAgFnSc1d1
karta	karta	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
připojena	připojit	k5eAaPmNgFnS
skrze	skrze	k?
rozhraní	rozhraní	k1gNnSc6
USB	USB	kA
2.0	2.0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
je	být	k5eAaImIp3nS
maximálně	maximálně	k6eAd1
zjednodušen	zjednodušen	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
vysokorychlostní	vysokorychlostní	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
(	(	kIx(
<g/>
např.	např.	kA
SATA	SATA	kA
rozhraní	rozhraní	k1gNnSc4
<g/>
,	,	kIx,
jaké	jaký	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
má	mít	k5eAaImIp3nS
Banana	Banana	k1gFnSc1
Pro	pro	k7c4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Disk	disk	k1gInSc1
je	být	k5eAaImIp3nS
sice	sice	k8xC
možné	možný	k2eAgNnSc1d1
připojit	připojit	k5eAaPmF
pomocí	pomocí	k7c2
USB	USB	kA
Mass	Massa	k1gFnPc2
Storage	Storage	k1gFnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
ze	z	k7c2
sítě	síť	k1gFnSc2
na	na	k7c4
disk	disk	k1gInSc4
a	a	k8xC
zpět	zpět	k6eAd1
bude	být	k5eAaImBp3nS
omezen	omezit	k5eAaPmNgInS
maximální	maximální	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
rozhraní	rozhraní	k1gNnSc2
USB	USB	kA
2.0	2.0	k4
(	(	kIx(
<g/>
tj.	tj.	kA
zhruba	zhruba	k6eAd1
35	#num#	k4
MiB	MiB	k1gFnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
se	se	k3xPyFc4
tedy	tedy	k9
moc	moc	k6eAd1
nehodí	hodit	k5eNaPmIp3nS
pro	pro	k7c4
nasazení	nasazení	k1gNnSc4
jako	jako	k8xS,k8xC
NAS	NAS	kA
(	(	kIx(
<g/>
tj.	tj.	kA
sdílení	sdílení	k1gNnSc1
souborů	soubor	k1gInPc2
z	z	k7c2
disku	disk	k1gInSc2
připojeného	připojený	k2eAgInSc2d1
k	k	k7c3
síti	síť	k1gFnSc3
pomocí	pomocí	k7c2
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
bude	být	k5eAaImBp3nS
v	v	k7c6
přenosových	přenosový	k2eAgFnPc6d1
rychlostech	rychlost	k1gFnPc6
limitováno	limitován	k2eAgNnSc1d1
propustností	propustnost	k1gFnPc2
USB	USB	kA
můstku	můstek	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInPc1d1
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
</s>
<s>
Procesor	procesor	k1gInSc1
BCM2835	BCM2835	k1gFnSc2
z	z	k7c2
rodiny	rodina	k1gFnSc2
ARM	ARM	kA
Cortex-A	Cortex-A	k1gFnSc1
<g/>
6	#num#	k4
taktovaný	taktovaný	k2eAgInSc4d1
na	na	k7c4
700	#num#	k4
MHz	Mhz	kA
</s>
<s>
Grafický	grafický	k2eAgInSc1d1
procesor	procesor	k1gInSc1
VideoCore	VideoCor	k1gInSc5
IV	IV	kA
<g/>
,	,	kIx,
podporující	podporující	k2eAgFnSc1d1
OpenGL	OpenGL	k1gFnSc1
ES	ES	kA
2.0	2.0	k4
<g/>
,	,	kIx,
1080	#num#	k4
<g/>
p	p	k?
<g/>
30	#num#	k4
<g/>
,	,	kIx,
MPEG-4	MPEG-4	k1gFnSc1
</s>
<s>
Obrazový	obrazový	k2eAgInSc1d1
výstup	výstup	k1gInSc1
Composite	Composit	k1gInSc5
RCA	RCA	kA
<g/>
,	,	kIx,
HDMI	HDMI	kA
<g/>
,	,	kIx,
DSI	DSI	kA
</s>
<s>
Zvukový	zvukový	k2eAgInSc1d1
výstup	výstup	k1gInSc1
přes	přes	k7c4
3,5	3,5	k4
<g/>
mm	mm	kA
konektor	konektor	k1gInSc4
<g/>
,	,	kIx,
HDMI	HDMI	kA
</s>
<s>
12	#num#	k4
<g/>
×	×	k?
GPIO	GPIO	kA
<g/>
,	,	kIx,
UART	UART	kA
<g/>
,	,	kIx,
I²	I²	k1gFnSc4
<g/>
,	,	kIx,
sběrnici	sběrnice	k1gFnSc4
SPI	spát	k5eAaImRp2nS
</s>
<s>
Podpora	podpora	k1gFnSc1
JTAG	JTAG	kA
Debug	Debug	k1gInSc4
</s>
<s>
Přidán	přidán	k2eAgMnSc1d1
watchdog	watchdog	k1gMnSc1
timer	timer	k1gMnSc1
</s>
<s>
Původní	původní	k2eAgInPc1d1
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
bylo	být	k5eAaImAgNnS
nabízeno	nabízet	k5eAaImNgNnS
ve	v	k7c6
dvou	dva	k4xCgInPc6
modelech	model	k1gInPc6
<g/>
,	,	kIx,
za	za	k7c4
25	#num#	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
model	model	k1gInSc1
1	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
a	a	k8xC
za	za	k7c4
35	#num#	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
model	model	k1gInSc1
1B	1B	k4
a	a	k8xC
2	#num#	k4
<g/>
B	B	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
dražší	drahý	k2eAgFnSc1d2
verze	verze	k1gFnSc1
měla	mít	k5eAaImAgFnS
navíc	navíc	k6eAd1
síťový	síťový	k2eAgInSc4d1
adaptér	adaptér	k1gInSc4
s	s	k7c7
konektorem	konektor	k1gInSc7
RJ-45	RJ-45	k1gFnSc2
a	a	k8xC
druhý	druhý	k4xOgInSc1
USB	USB	kA
port	port	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
oznámení	oznámení	k1gNnSc2
a	a	k8xC
uvedení	uvedení	k1gNnSc2
na	na	k7c4
trh	trh	k1gInSc4
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
podle	podle	k7c2
recenzí	recenze	k1gFnPc2
o	o	k7c4
dobrý	dobrý	k2eAgInSc4d1
poměr	poměr	k1gInSc4
cena	cena	k1gFnSc1
<g/>
/	/	kIx~
<g/>
výkon	výkon	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Model	model	k1gInSc1
„	„	k?
<g/>
A	a	k8xC
<g/>
“	“	k?
</s>
<s>
dostupný	dostupný	k2eAgInSc1d1
od	od	k7c2
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
256	#num#	k4
MiB	MiB	k1gFnSc1
RWM	RWM	kA
(	(	kIx(
<g/>
RAM	RAM	kA
<g/>
)	)	kIx)
sdílených	sdílený	k2eAgInPc2d1
s	s	k7c7
grafickou	grafický	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
</s>
<s>
Slot	slot	k1gInSc1
pro	pro	k7c4
SD	SD	kA
nebo	nebo	k8xC
MMC	MMC	kA
kartu	karta	k1gFnSc4
</s>
<s>
jeden	jeden	k4xCgMnSc1
USB	USB	kA
port	port	k1gInSc4
</s>
<s>
Model	model	k1gInSc1
„	„	k?
<g/>
A	A	kA
<g/>
+	+	kIx~
<g/>
“	“	k?
</s>
<s>
dostupný	dostupný	k2eAgInSc1d1
od	od	k7c2
listopadu	listopad	k1gInSc2
2014	#num#	k4
</s>
<s>
256	#num#	k4
MiB	MiB	k1gFnSc1
RWM	RWM	kA
(	(	kIx(
<g/>
RAM	RAM	kA
<g/>
)	)	kIx)
sdílených	sdílený	k2eAgInPc2d1
s	s	k7c7
grafickou	grafický	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
</s>
<s>
Slot	slot	k1gInSc1
pro	pro	k7c4
micro	micro	k1gNnSc4
SD	SD	kA
kartu	karta	k1gFnSc4
</s>
<s>
jeden	jeden	k4xCgMnSc1
USB	USB	kA
port	port	k1gInSc4
</s>
<s>
Model	model	k1gInSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
</s>
<s>
dostupný	dostupný	k2eAgInSc1d1
od	od	k7c2
dubna	duben	k1gInSc2
2012	#num#	k4
</s>
<s>
256	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
512	#num#	k4
MiB	MiB	k1gFnPc2
RWM	RWM	kA
(	(	kIx(
<g/>
RAM	RAM	kA
<g/>
)	)	kIx)
sdílených	sdílený	k2eAgInPc2d1
s	s	k7c7
grafickou	grafický	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
</s>
<s>
Slot	slot	k1gInSc1
pro	pro	k7c4
SD	SD	kA
nebo	nebo	k8xC
MMC	MMC	kA
kartu	karta	k1gFnSc4
</s>
<s>
dva	dva	k4xCgMnPc1
USB	USB	kA
porty	port	k1gInPc1
</s>
<s>
ethernetový	ethernetový	k2eAgInSc1d1
adaptér	adaptér	k1gInSc1
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
s	s	k7c7
konektorem	konektor	k1gInSc7
RJ-45	RJ-45	k1gFnSc2
</s>
<s>
Model	model	k1gInSc1
„	„	k?
<g/>
B	B	kA
<g/>
+	+	kIx~
<g/>
“	“	k?
</s>
<s>
dostupný	dostupný	k2eAgInSc1d1
od	od	k7c2
července	červenec	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
512	#num#	k4
MiB	MiB	k1gFnSc1
RWM	RWM	kA
(	(	kIx(
<g/>
RAM	RAM	kA
<g/>
)	)	kIx)
sdílených	sdílený	k2eAgInPc2d1
s	s	k7c7
grafickou	grafický	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
</s>
<s>
Slot	slot	k1gInSc1
pro	pro	k7c4
micro	micro	k1gNnSc4
SD	SD	kA
kartu	karta	k1gFnSc4
</s>
<s>
čtyři	čtyři	k4xCgFnPc4
USB	USB	kA
porty	porta	k1gFnPc1
</s>
<s>
ethernetový	ethernetový	k2eAgInSc1d1
adaptér	adaptér	k1gInSc1
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
s	s	k7c7
konektorem	konektor	k1gInSc7
RJ-45	RJ-45	k1gFnSc2
</s>
<s>
Takto	takto	k6eAd1
upravená	upravený	k2eAgFnSc1d1
deska	deska	k1gFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
vyráběna	vyrábět	k5eAaImNgFnS
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
továrně	továrna	k1gFnSc6
firmy	firma	k1gFnSc2
Sony	Sony	kA
v	v	k7c6
Pencoadu	Pencoad	k1gInSc6
v	v	k7c6
Jižním	jižní	k2eAgInSc6d1
Walesu	Wales	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
dostupná	dostupný	k2eAgFnSc1d1
za	za	k7c4
stejnou	stejný	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadace	nadace	k1gFnSc1
přitom	přitom	k6eAd1
usilovala	usilovat	k5eAaImAgFnS
o	o	k7c4
výrobu	výroba	k1gFnSc4
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
od	od	k7c2
počátku	počátek	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
domluvit	domluvit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
britským	britský	k2eAgMnSc7d1
výrobcem	výrobce	k1gMnSc7
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
podařilo	podařit	k5eAaPmAgNnS
až	až	k9
po	po	k7c6
počátečním	počáteční	k2eAgInSc6d1
úspěchu	úspěch	k1gInSc6
s	s	k7c7
deskami	deska	k1gFnPc7
vyrobenými	vyrobený	k2eAgFnPc7d1
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
2	#num#	k4
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
2	#num#	k4
Model	model	k1gInSc1
B	B	kA
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Model	model	k1gInSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
prodeje	prodej	k1gInSc2
v	v	k7c6
únoru	únor	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
SoC	soc	kA
BCM2836	BCM2836	k1gFnSc4
z	z	k7c2
rodiny	rodina	k1gFnSc2
ARM	ARM	kA
Cortex-A	Cortex-A	k1gFnSc1
<g/>
7	#num#	k4
opět	opět	k6eAd1
od	od	k7c2
firmy	firma	k1gFnSc2
Broadcom	Broadcom	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
obsahuje	obsahovat	k5eAaImIp3nS
čtveřici	čtveřice	k1gFnSc4
procesorových	procesorový	k2eAgNnPc2d1
jader	jádro	k1gNnPc2
s	s	k7c7
taktem	takt	k1gInSc7
900	#num#	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
posílenou	posílený	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
SIMD	SIMD	kA
a	a	k8xC
1	#num#	k4
GiB	GiB	k1gMnPc2
paměti	paměť	k1gFnSc2
RAM	RAM	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Osazen	osazen	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
grafickým	grafický	k2eAgInSc7d1
procesorem	procesor	k1gInSc7
VideoCore	VideoCor	k1gInSc5
IV	IV	kA
<g/>
,	,	kIx,
podporující	podporující	k2eAgFnSc1d1
OpenGL	OpenGL	k1gFnSc1
ES	ES	kA
2.0	2.0	k4
<g/>
,	,	kIx,
1080	#num#	k4
<g/>
p	p	k?
<g/>
30	#num#	k4
<g/>
,	,	kIx,
MPEG-	MPEG-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnPc7
předchůdci	předchůdce	k1gMnPc7
obsahuje	obsahovat	k5eAaImIp3nS
slot	slot	k1gInSc1
pro	pro	k7c4
microSD	microSD	k?
kartu	karta	k1gFnSc4
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc4
porty	port	k1gInPc4
USB	USB	kA
2.0	2.0	k4
<g/>
,	,	kIx,
ethernetový	ethernetový	k2eAgInSc1d1
adaptér	adaptér	k1gInSc1
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
Mbit	Mbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
s	s	k7c7
konektorem	konektor	k1gInSc7
RJ-45	RJ-45	k1gFnSc2
a	a	k8xC
vývody	vývod	k1gInPc1
GPIO	GPIO	kA
<g/>
.	.	kIx.
</s>
<s>
Počítač	počítač	k1gInSc1
je	být	k5eAaImIp3nS
zpětně	zpětně	k6eAd1
kompatibilní	kompatibilní	k2eAgMnSc1d1
s	s	k7c7
původním	původní	k2eAgMnSc7d1
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
3	#num#	k4
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2016	#num#	k4
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
prodeje	prodej	k1gInSc2
model	model	k1gInSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
3	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
vybaven	vybavit	k5eAaPmNgInS
64	#num#	k4
<g/>
bitovým	bitový	k2eAgFnPc3d1
CPU	CPU	kA
o	o	k7c6
taktu	takt	k1gInSc6
1,2	1,2	k4
GHz	GHz	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
čtyřjádro	čtyřjádro	k6eAd1
ARM	ARM	kA
Cortex-A	Cortex-A	k1gFnSc1
<g/>
53	#num#	k4
a	a	k8xC
dle	dle	k7c2
výrobce	výrobce	k1gMnSc2
je	být	k5eAaImIp3nS
o	o	k7c4
50	#num#	k4
%	%	kIx~
rychlejší	rychlý	k2eAgFnSc1d2
než	než	k8xS
to	ten	k3xDgNnSc1
v	v	k7c4
Raspberry	Raspberr	k1gInPc4
Pi	pi	k0
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
Model	model	k1gInSc1
2	#num#	k4
je	být	k5eAaImIp3nS
vybaven	vybavit	k5eAaPmNgInS
1	#num#	k4
GiB	GiB	k1gFnSc7
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinkou	novinka	k1gFnSc7
jsou	být	k5eAaImIp3nP
kromě	kromě	k7c2
64	#num#	k4
<g/>
bitového	bitový	k2eAgInSc2d1
procesoru	procesor	k1gInSc2
i	i	k8xC
integrované	integrovaný	k2eAgNnSc4d1
Wi-Fi	Wi-Fi	k1gNnSc4
a	a	k8xC
Bluetooth	Bluetooth	k1gInSc4
moduly	modul	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozmístění	rozmístění	k1gNnSc1
konektorů	konektor	k1gInPc2
se	se	k3xPyFc4
oproti	oproti	k7c3
předchozímu	předchozí	k2eAgInSc3d1
modelu	model	k1gInSc3
nemění	měnit	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
4	#num#	k4
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
2019	#num#	k4
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
do	do	k7c2
prodeje	prodej	k1gInSc2
model	model	k1gInSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
64	#num#	k4
<g/>
bitový	bitový	k2eAgInSc1d1
čtyřjadrový	čtyřjadrový	k2eAgInSc1d1
procesor	procesor	k1gInSc1
o	o	k7c6
taktu	takt	k1gInSc6
1,5	1,5	k4
<g/>
GHz	GHz	k1gFnSc1
Broadcom	Broadcom	k1gInSc1
BCM	BCM	kA
<g/>
2711	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
3	#num#	k4
se	se	k3xPyFc4
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
4	#num#	k4
prodává	prodávat	k5eAaImIp3nS
ve	v	k7c6
třech	tři	k4xCgFnPc6
variantách	varianta	k1gFnPc6
podle	podle	k7c2
velikosti	velikost	k1gFnSc2
RAM	RAM	kA
-	-	kIx~
1	#num#	k4
GiB	GiB	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
GiB	GiB	k1gFnPc2
<g/>
,	,	kIx,
4	#num#	k4
GiB	GiB	k1gMnPc2
a	a	k8xC
od	od	k7c2
května	květen	k1gInSc2
2020	#num#	k4
i	i	k9
8	#num#	k4
<g/>
GiB	GiB	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napájení	napájení	k1gNnPc1
pomocí	pomocí	k7c2
micro-USB	micro-USB	k?
bylo	být	k5eAaImAgNnS
nahrazeno	nahradit	k5eAaPmNgNnS
napájením	napájení	k1gNnSc7
pomocí	pomoc	k1gFnPc2
USB-	USB-	k1gFnSc2
<g/>
C.	C.	kA
Jeden	jeden	k4xCgMnSc1
HDMI	HDMI	kA
port	port	k1gInSc1
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
dvěma	dva	k4xCgFnPc7
micro-HDMI	micro-HDMI	k?
porty	port	k1gInPc1
a	a	k8xC
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
4	#num#	k4
nyní	nyní	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
i	i	k9
monitory	monitor	k1gInPc4
s	s	k7c7
rozlišením	rozlišení	k1gNnSc7
4K	4K	k4
a	a	k8xC
dva	dva	k4xCgInPc4
USB	USB	kA
2.0	2.0	k4
porty	porta	k1gFnSc2
byly	být	k5eAaImAgInP
nahrazeny	nahradit	k5eAaPmNgInP
USB	USB	kA
3.0	3.0	k4
porty	porta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
400	#num#	k4
</s>
<s>
Eben	eben	k1gInSc1
Upton	Upton	k1gInSc1
oznámil	oznámit	k5eAaPmAgInS
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc6
2020	#num#	k4
existenci	existence	k1gFnSc4
počítače	počítač	k1gInSc2
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
400	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
předchozí	předchozí	k2eAgFnSc2d1
modelu	model	k1gInSc2
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
4	#num#	k4
s	s	k7c7
4	#num#	k4
GB	GB	kA
pamětí	paměť	k1gFnPc2
LPDDR4	LPDDR4	k1gFnPc2
RAM	RAM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změnu	změna	k1gFnSc4
prodělala	prodělat	k5eAaPmAgFnS
deska	deska	k1gFnSc1
plošných	plošný	k2eAgInPc2d1
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
případě	případ	k1gInSc6
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
400	#num#	k4
zabudována	zabudovat	k5eAaPmNgFnS
do	do	k7c2
klávesnice	klávesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
lepšímu	lepší	k1gNnSc3
chlazení	chlazení	k1gNnSc2
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
navýšení	navýšení	k1gNnSc3
frekvence	frekvence	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
procesor	procesor	k1gInSc1
taktován	taktován	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
Pico	Pico	k1gNnSc5
</s>
<s>
Představený	představený	k2eAgMnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
je	být	k5eAaImIp3nS
vývojová	vývojový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
procesoru	procesor	k1gInSc6
RP	RP	kA
<g/>
2040	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
na	na	k7c6
dvoujádrovém	dvoujádrový	k2eAgInSc6d1
procesoru	procesor	k1gInSc6
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
jádra	jádro	k1gNnSc2
jsou	být	k5eAaImIp3nP
ARM	ARM	kA
Cortex-M	Cortex-M	k1gFnSc7
<g/>
0	#num#	k4
<g/>
+	+	kIx~
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
lze	lze	k6eAd1
taktovat	taktovat	k5eAaImF
až	až	k9
na	na	k7c4
133	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
procesoru	procesor	k1gInSc2
je	být	k5eAaImIp3nS
2	#num#	k4
MB	MB	kA
paměť	paměť	k1gFnSc1
typu	typ	k1gInSc2
FLASH	FLASH	kA
a	a	k8xC
264	#num#	k4
kB	kb	kA
paměti	paměť	k1gFnSc2
RAM	RAM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
rozhraní	rozhraní	k1gNnSc4
QSPI	QSPI	kA
lze	lze	k6eAd1
adresovat	adresovat	k5eAaBmF
až	až	k9
16	#num#	k4
MB	MB	kA
externí	externí	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vývojáře	vývojář	k1gMnSc4
existuje	existovat	k5eAaImIp3nS
SDK	SDK	kA
<g/>
,	,	kIx,
nástroje	nástroj	k1gInPc1
založené	založený	k2eAgInPc1d1
na	na	k7c6
GCC	GCC	kA
integrované	integrovaný	k2eAgFnSc2d1
do	do	k7c2
Visual	Visual	k1gInSc1
Studio	studio	k1gNnSc1
Code	Code	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Porovnání	porovnání	k1gNnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
modelů	model	k1gInPc2
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Model	model	k1gInSc1
A	a	k9
</s>
<s>
Model	model	k1gInSc1
B	B	kA
</s>
<s>
Výpočetní	výpočetní	k2eAgInSc1d1
modul	modul	k1gInSc1
<g/>
*	*	kIx~
</s>
<s>
Zero	Zero	k6eAd1
</s>
<s>
Generace	generace	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
lite	lit	k1gInSc5
</s>
<s>
PCB	PCB	kA
verze	verze	k1gFnSc1
1.2	1.2	k4
</s>
<s>
PCB	PCB	kA
verze	verze	k1gFnSc1
1.3	1.3	k4
</s>
<s>
W	W	kA
(	(	kIx(
<g/>
wireless	wireless	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc2
</s>
<s>
únor	únor	k1gInSc1
2012	#num#	k4
</s>
<s>
listopad	listopad	k1gInSc1
2014	#num#	k4
</s>
<s>
duben	duben	k1gInSc1
až	až	k9
červen	červen	k1gInSc1
2012	#num#	k4
</s>
<s>
červenec	červenec	k1gInSc1
2014	#num#	k4
</s>
<s>
únor	únor	k1gInSc1
2015	#num#	k4
</s>
<s>
únor	únor	k1gInSc1
2016	#num#	k4
</s>
<s>
březen	březen	k1gInSc1
2018	#num#	k4
</s>
<s>
červen	červen	k1gInSc1
2019	#num#	k4
</s>
<s>
duben	duben	k1gInSc1
2014	#num#	k4
</s>
<s>
leden	leden	k1gInSc1
2017	#num#	k4
</s>
<s>
listopad	listopad	k1gInSc1
2015	#num#	k4
</s>
<s>
květen	květen	k1gInSc4
2016	#num#	k4
</s>
<s>
únor	únor	k1gInSc1
2017	#num#	k4
</s>
<s>
Cílová	cílový	k2eAgFnSc1d1
cena	cena	k1gFnSc1
</s>
<s>
25	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
20	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
35	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
25	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
35	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
35	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
35	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
35	#num#	k4
<g/>
,	,	kIx,
45	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
55	#num#	k4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
podle	podle	k7c2
modelu	model	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
30	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
30	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
25	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
5	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
5	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
10	#num#	k4
dolarů	dolar	k1gInPc2
</s>
<s>
Architektura	architektura	k1gFnSc1
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
6	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
7	#num#	k4
<g/>
(	(	kIx(
<g/>
32	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
8	#num#	k4
<g/>
(	(	kIx(
<g/>
64	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
8	#num#	k4
<g/>
(	(	kIx(
<g/>
64	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
8	#num#	k4
(	(	kIx(
<g/>
64	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
6	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
8	#num#	k4
(	(	kIx(
<g/>
64	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
ARMv	ARMv	k1gInSc1
<g/>
6	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
<g/>
-bit	-bitum	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
SoC	soc	kA
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2835	BCM2835	k1gFnSc2
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2836	BCM2836	k1gFnSc2
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2837	BCM2837	k1gFnSc2
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2837B0	BCM2837B0	k1gFnSc2
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2711	BCM2711	k1gFnSc2
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2835	BCM2835	k1gFnSc2
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2837	BCM2837	k1gFnSc2
</s>
<s>
Broadcom	Broadcom	k1gInSc1
BCM2835	BCM2835	k1gFnSc2
</s>
<s>
Procesor	procesor	k1gInSc1
</s>
<s>
700	#num#	k4
MHz	Mhz	kA
single-core	single-cor	k1gMnSc5
ARM1176JZF-S	ARM1176JZF-S	k1gFnSc5
</s>
<s>
900	#num#	k4
MHz	Mhz	kA
<g/>
32	#num#	k4
<g/>
-bit	-bit	k2eAgInSc4d1
čtyřjádrový	čtyřjádrový	k2eAgInSc4d1
ARM	ARM	kA
Cortex-A	Cortex-A	k1gFnSc2
<g/>
7	#num#	k4
</s>
<s>
1,2	1,2	k4
GHz	GHz	k1gFnSc1
<g/>
64	#num#	k4
<g/>
-bit	-bit	k2eAgInSc4d1
čtyřjádrový	čtyřjádrový	k2eAgInSc4d1
ARM	ARM	kA
Cortex-A	Cortex-A	k1gFnSc2
<g/>
53	#num#	k4
</s>
<s>
1.4	1.4	k4
GHz	GHz	k1gFnSc1
</s>
<s>
64	#num#	k4
<g/>
-bit	-bit	k1gInSc1
quad-core	quad-cor	k1gMnSc5
</s>
<s>
Cortex-A	Cortex-A	k?
<g/>
53	#num#	k4
</s>
<s>
1,5	1,5	k4
GHz	GHz	k1gFnSc1
64	#num#	k4
<g/>
-bit	-bita	k1gFnPc2
čtyřjádrový	čtyřjádrový	k2eAgMnSc1d1
Cortex-A	Cortex-A	k1gMnSc1
<g/>
72	#num#	k4
</s>
<s>
700	#num#	k4
MHz	Mhz	kA
single-core	single-cor	k1gMnSc5
ARM1176JZF-S	ARM1176JZF-S	k1gMnSc3
</s>
<s>
1,2	1,2	k4
GHz	GHz	k1gFnSc1
64	#num#	k4
<g/>
-bit	-bita	k1gFnPc2
quad-core	quad-cor	k1gInSc5
ARM	ARM	kA
Cortex-A	Cortex-A	k1gFnSc1
<g/>
53	#num#	k4
</s>
<s>
1	#num#	k4
GHz	GHz	k1gMnSc1
ARM1176JZF-S	ARM1176JZF-S	k1gMnSc1
single-core	single-cor	k1gInSc5
</s>
<s>
GPU	GPU	kA
</s>
<s>
Broadcom	Broadcom	k1gInSc1
VideoCore	VideoCor	k1gInSc5
IV	IV	kA
@	@	kIx~
250	#num#	k4
MHz	Mhz	kA
</s>
<s>
OpenGL	OpenGL	k?
ES	ES	kA
2.0	2.0	k4
MPEG-2	MPEG-2	k1gMnPc2
a	a	k8xC
VC-1	VC-1	k1gMnPc2
(	(	kIx(
<g/>
s	s	k7c7
licencí	licence	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
1080	#num#	k4
<g/>
p	p	k?
<g/>
30	#num#	k4
H	H	kA
<g/>
.264	.264	k4
/	/	kIx~
dekodér	dekodér	k1gInSc4
a	a	k8xC
enkodér	enkodér	k1gInSc4
vysokoprofilového	vysokoprofilový	k2eAgMnSc2d1
MPEG-4	MPEG-4	k1gMnSc2
AVC	AVC	kA
</s>
<s>
Paměť	paměť	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
SDRAM	SDRAM	kA
<g/>
)	)	kIx)
</s>
<s>
256	#num#	k4
MiB	MiB	k1gFnSc1
(	(	kIx(
<g/>
sdílenos	sdílenos	k1gInSc1
GPU	GPU	kA
<g/>
)	)	kIx)
</s>
<s>
512	#num#	k4
MiB	MiB	k1gFnPc2
(	(	kIx(
<g/>
sdílené	sdílený	k2eAgFnSc2d1
s	s	k7c7
GPU	GPU	kA
<g/>
)	)	kIx)
od	od	k7c2
května	květen	k1gInSc2
2016	#num#	k4
</s>
<s>
1	#num#	k4
GiB	GiB	k1gFnPc2
(	(	kIx(
<g/>
sdílené	sdílený	k2eAgFnSc2d1
s	s	k7c7
GPU	GPU	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
nebo	nebo	k8xC
8	#num#	k4
GiB	GiB	k1gFnPc2
</s>
<s>
512	#num#	k4
MiB	MiB	k1gFnSc1
(	(	kIx(
<g/>
sdílená	sdílený	k2eAgFnSc1d1
s	s	k7c7
GPU	GPU	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
GiB	GiB	k1gFnPc2
(	(	kIx(
<g/>
sdílené	sdílený	k2eAgFnSc2d1
s	s	k7c7
GPU	GPU	kA
<g/>
)	)	kIx)
</s>
<s>
512	#num#	k4
MiB	MiB	k1gFnSc1
(	(	kIx(
<g/>
sdílená	sdílený	k2eAgFnSc1d1
s	s	k7c7
GPU	GPU	kA
<g/>
)	)	kIx)
</s>
<s>
USB	USB	kA
2.0	2.0	k4
porty	porta	k1gFnSc2
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
přímo	přímo	k6eAd1
z	z	k7c2
BCM2835	BCM2835	k1gFnSc2
čipu	čip	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
prostřednictvímon-board	prostřednictvímon-board	k1gInSc1
USB	USB	kA
hubus	hubus	k1gInSc4
3	#num#	k4
porty	porta	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
(	(	kIx(
<g/>
prostřednictvím	prostřednictvím	k7c2
</s>
<s>
on-board	on-board	k1gMnSc1
USB	USB	kA
hubu	huba	k1gFnSc4
s	s	k7c7
5	#num#	k4
porty	port	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
prostřednictvím	prostřednictvím	k7c2
on-board	on-boarda	k1gFnPc2
USB	USB	kA
hubu	huba	k1gFnSc4
s	s	k7c7
5	#num#	k4
porty	port	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
přímo	přímo	k6eAd1
z	z	k7c2
BCM2835	BCM2835	k1gFnSc2
čipu	čip	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
přímo	přímo	k6eAd1
z	z	k7c2
BCM2837	BCM2837	k1gFnSc2
čipu	čip	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
Micro-USB	Micro-USB	k1gFnSc1
(	(	kIx(
<g/>
přímo	přímo	k6eAd1
z	z	k7c2
BCM2835	BCM2835	k1gFnSc2
čipu	čip	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
USB	USB	kA
3.0	3.0	k4
porty	porta	k1gFnSc2
</s>
<s>
žádný	žádný	k3yNgInSc1
</s>
<s>
2	#num#	k4
(	(	kIx(
<g/>
prostřednictvím	prostřednictvím	k7c2
on-board	on-boarda	k1gFnPc2
USB	USB	kA
hubu	huba	k1gFnSc4
s	s	k7c7
5	#num#	k4
porty	port	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
žádný	žádný	k3yNgInSc1
</s>
<s>
Video	video	k1gNnSc1
vstup	vstup	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
-pin	-pin	k1gInSc1
rozhraní	rozhraní	k1gNnSc2
MIPI	MIPI	kA
kamerový	kamerový	k2eAgMnSc1d1
(	(	kIx(
<g/>
CSI	CSI	kA
<g/>
)	)	kIx)
konektor	konektor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
s	s	k7c7
kamerou	kamera	k1gFnSc7
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
nebos	nebosa	k1gFnPc2
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
noir	noir	k1gMnSc1
kamerou	kamera	k1gFnSc7
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
rozhraní	rozhraní	k1gNnSc6
MIPI	MIPI	kA
(	(	kIx(
<g/>
CSI	CSI	kA
<g/>
)	)	kIx)
</s>
<s>
žádný	žádný	k3yNgInSc1
</s>
<s>
rozhraní	rozhraní	k1gNnSc1
pro	pro	k7c4
připojení	připojení	k1gNnSc4
fotoaparátu	fotoaparát	k1gInSc2
MIPI	MIPI	kA
(	(	kIx(
<g/>
CSI	CSI	kA
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
rev	rev	k?
1.3	1.3	k4
<g/>
)	)	kIx)
</s>
<s>
Video	video	k1gNnSc1
výstupy	výstup	k1gInPc7
</s>
<s>
HDMI	HDMI	kA
(	(	kIx(
<g/>
rev	rev	k?
1.3	1.3	k4
<g/>
)	)	kIx)
kompozitní	kompozitní	k2eAgNnSc1d1
video	video	k1gNnSc1
(	(	kIx(
<g/>
RCA	RCA	kA
konektor	konektor	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
MIPI	MIPI	kA
zobrazovací	zobrazovací	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
(	(	kIx(
<g/>
DSI	DSI	kA
<g/>
)	)	kIx)
</s>
<s>
HDMI	HDMI	kA
<g/>
(	(	kIx(
<g/>
rev	rev	k?
1.3	1.3	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgNnSc1d1
video	video	k1gNnSc1
(	(	kIx(
<g/>
3,5	3,5	k4
<g/>
mm	mm	kA
jack	jacka	k1gFnPc2
TRRS	TRRS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
displej	displej	k1gInSc1
rozhraní	rozhraní	k1gNnSc2
MIPI	MIPI	kA
(	(	kIx(
<g/>
DSI	DSI	kA
<g/>
)	)	kIx)
pro	pro	k7c4
základní	základní	k2eAgNnSc4d1
LCD	LCD	kA
panelů	panel	k1gInPc2
</s>
<s>
HDMI	HDMI	kA
(	(	kIx(
<g/>
rev	rev	k?
1.3	1.3	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgNnSc1d1
video	video	k1gNnSc1
(	(	kIx(
<g/>
RCA	RCA	kA
jack	jack	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
displej	displej	k1gInSc1
rozhraní	rozhraní	k1gNnSc2
MIPI	MIPI	kA
(	(	kIx(
<g/>
DSI	DSI	kA
<g/>
)	)	kIx)
</s>
<s>
HDMI	HDMI	kA
(	(	kIx(
<g/>
rev	rev	k?
1.3	1.3	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgNnSc1d1
video	video	k1gNnSc1
(	(	kIx(
<g/>
3,5	3,5	k4
<g/>
mm	mm	kA
jack	jacka	k1gFnPc2
TRRS	TRRS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
displej	displej	k1gInSc1
rozhraní	rozhraní	k1gNnSc2
MIPI	MIPI	kA
(	(	kIx(
<g/>
DSI	DSI	kA
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
micro-HDMI	micro-HDMI	k?
(	(	kIx(
<g/>
rev	rev	k?
1.3	1.3	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgNnSc1d1
video	video	k1gNnSc1
(	(	kIx(
<g/>
3,5	3,5	k4
<g/>
mm	mm	kA
jack	jacka	k1gFnPc2
TRRS	TRRS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
displej	displej	k1gInSc1
rozhraní	rozhraní	k1gNnSc2
MIPI	MIPI	kA
(	(	kIx(
<g/>
DSI	DSI	kA
<g/>
)	)	kIx)
</s>
<s>
HDMI	HDMI	kA
<g/>
,	,	kIx,
2	#num#	k4
<g/>
×	×	k?
MIPI	MIPI	kA
displej	displej	k1gInSc1
rozhraní	rozhraní	k1gNnSc1
(	(	kIx(
<g/>
DSI	DSI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgNnSc1d1
video	video	k1gNnSc1
</s>
<s>
HDMI	HDMI	kA
Typ	typ	k1gInSc1
C-Mini	C-Min	k2eAgMnPc1d1
1080	#num#	k4
<g/>
p	p	k?
<g/>
60	#num#	k4
<g/>
,	,	kIx,
kompozitní	kompozitní	k2eAgNnSc1d1
video	video	k1gNnSc1
přes	přes	k7c4
GPIO	GPIO	kA
</s>
<s>
Audio	audio	k2eAgInPc1d1
vstupy	vstup	k1gInPc1
</s>
<s>
skrze	skrze	k?
I²	I²	k1gFnSc1
</s>
<s>
Audio	audio	k2eAgInPc1d1
výstupy	výstup	k1gInPc1
</s>
<s>
analog	analog	k1gInSc1
přes	přes	k7c4
3,5	3,5	k4
mm	mm	kA
jack	jacka	k1gFnPc2
<g/>
;	;	kIx,
digitální	digitální	k2eAgFnSc1d1
přes	přes	k7c4
HDMI	HDMI	kA
a	a	k8xC
jako	jako	k9
revize	revize	k1gFnSc2
2	#num#	k4
desky	deska	k1gFnSc2
<g/>
,	,	kIx,
I²	I²	k1gFnSc2
</s>
<s>
analog	analog	k1gInSc1
<g/>
,	,	kIx,
HDMI	HDMI	kA
<g/>
,	,	kIx,
I²	I²	k1gFnSc1
</s>
<s>
HDMI	HDMI	kA
Typ	typ	k1gInSc1
C-Mini	C-Min	k1gMnPc1
<g/>
,	,	kIx,
stereo	stereo	k2eAgInSc1d1
zvuk	zvuk	k1gInSc1
prostřednictvím	prostřednictvím	k7c2
PWM	PWM	kA
na	na	k7c6
GPIO	GPIO	kA
</s>
<s>
On-board	On-board	k1gInSc1
úložný	úložný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
</s>
<s>
SD	SD	kA
<g/>
,	,	kIx,
MMC	MMC	kA
<g/>
,	,	kIx,
slot	slot	k1gInSc1
SDIO	SDIO	kA
karty	karta	k1gFnSc2
</s>
<s>
MicroSDHC	MicroSDHC	k?
slot	slot	k1gInSc1
</s>
<s>
SD	SD	kA
<g/>
,	,	kIx,
MMC	MMC	kA
<g/>
,	,	kIx,
slot	slot	k1gInSc1
SDIO	SDIO	kA
karty	karta	k1gFnSc2
</s>
<s>
MicroSDHC	MicroSDHC	k?
slot	slot	k1gInSc1
</s>
<s>
4	#num#	k4
GiB	GiB	k1gMnSc1
eMMC	eMMC	k?
flash	flash	k1gMnSc1
paměť	paměť	k1gFnSc4
čipů	čip	k1gInPc2
</s>
<s>
MicroSDHC	MicroSDHC	k?
</s>
<s>
Síť	síť	k1gFnSc1
</s>
<s>
žádný	žádný	k3yNgInSc1
</s>
<s>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
Mbit	Mbit	k1gMnSc1
Ethernet	Ethernet	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
P	P	kA
<g/>
8	#num#	k4
<g/>
C	C	kA
<g/>
)	)	kIx)
USB	USB	kA
adaptér	adaptér	k1gInSc1
na	na	k7c4
USB	USB	kA
hubu	huba	k1gFnSc4
</s>
<s>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
Mbit	Mbit	k1gMnSc1
Ethernet	Ethernet	k1gMnSc1
<g/>
,	,	kIx,
802.11	802.11	k4
<g/>
n	n	k0
<g/>
,	,	kIx,
Bluetooth	Bluetooth	k1gMnSc1
4.1	4.1	k4
</s>
<s>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
/	/	kIx~
<g/>
1000	#num#	k4
Mbit	Mbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Ethernet	Etherneta	k1gFnPc2
(	(	kIx(
<g/>
skutečná	skutečný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
maximálně	maximálně	k6eAd1
300	#num#	k4
Mbit	Mbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
802.11	802.11	k4
<g/>
ac	ac	k?
dual	dual	k1gInSc1
band	band	k1gInSc1
2.4	2.4	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
GHz	GHz	k1gFnPc2
wireless	wireless	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
Bluetooth	Bluetooth	k1gInSc1
4.2	4.2	k4
LS	LS	kA
BLE	BLE	kA
</s>
<s>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
/	/	kIx~
<g/>
1000	#num#	k4
Mbit	Mbitum	k1gNnPc2
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Ethernet	Ethernet	k1gInSc1
<g/>
,	,	kIx,
2.4	2.4	k4
<g/>
/	/	kIx~
<g/>
5.0	5.0	k4
GHz	GHz	k1gFnSc2
IEEE	IEEE	kA
802.11	802.11	k4
<g/>
ac	ac	k?
wireless	wireless	k1gInSc1
<g/>
,	,	kIx,
Bluetooth	Bluetooth	k1gInSc1
5.0	5.0	k4
<g/>
,	,	kIx,
BLE	BLE	kA
</s>
<s>
žádný	žádný	k3yNgInSc1
</s>
<s>
802.11	802.11	k4
<g/>
n	n	k0
WiFi	WiF	k1gMnSc5
<g/>
,	,	kIx,
Bluetooth	Bluetooth	k1gInSc1
4.1	4.1	k4
</s>
<s>
Periferní	periferní	k2eAgNnSc1d1
nízkoúrovňové	nízkoúrovňový	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
GPIOa	GPIO	k1gInSc2
doplňky	doplněk	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
také	také	k9
použity	použít	k5eAaPmNgInP
jako	jako	k8xS,k8xC
GPIO	GPIO	kA
<g/>
:	:	kIx,
UART	UART	kA
<g/>
,	,	kIx,
I²	I²	k1gFnSc1
bus	bus	k1gInSc1
<g/>
,	,	kIx,
SPI	spát	k5eAaImRp2nS
bus	bus	k1gInSc4
<g/>
,	,	kIx,
I²	I²	k1gFnSc4
audio	audio	k2eAgFnSc4d1
3,3	3,3	k4
V	V	kA
<g/>
,	,	kIx,
5	#num#	k4
V	V	kA
<g/>
,	,	kIx,
zem	zem	k1gFnSc4
</s>
<s>
17	#num#	k4
<g/>
×	×	k?
GPIOa	GPIO	k1gInSc2
HAT	hat	k0
ID	Ida	k1gFnPc2
bus	bus	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
GPIO	GPIO	kA
</s>
<s>
17	#num#	k4
<g/>
×	×	k?
GPIO	GPIO	kA
</s>
<s>
46	#num#	k4
<g/>
×	×	k?
GPIO	GPIO	kA
</s>
<s>
26	#num#	k4
<g/>
×	×	k?
GPIOa	GPIO	k1gInSc2
HAT	hat	k0
ID	Ida	k1gFnPc2
bus	bus	k1gInSc1
</s>
<s>
Jmenovitý	jmenovitý	k2eAgInSc1d1
výkon	výkon	k1gInSc1
</s>
<s>
300	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
1,5	1,5	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
200	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
1	#num#	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
700	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
3,5	3,5	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
600	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
3,0	3,0	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
800	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
4,0	4,0	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
200	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
1	#num#	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
459	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
2.295	2.295	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
3A	3A	k4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
~	~	kIx~
160	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
0,8	0,8	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
700	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
3.5	3.5	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
~	~	kIx~
160	#num#	k4
mA	mA	k?
(	(	kIx(
<g/>
0,8	0,8	k4
W	W	kA
<g/>
)	)	kIx)
</s>
<s>
Zdroj	zdroj	k1gInSc1
energie	energie	k1gFnSc2
</s>
<s>
5	#num#	k4
V	v	k7c4
přes	přes	k7c4
MicroUSB	MicroUSB	k1gFnSc4
</s>
<s>
5	#num#	k4
V	v	k7c4
přes	přes	k7c4
MicroUSB	MicroUSB	k1gFnSc4
<g/>
,	,	kIx,
GPIO	GPIO	kA
nebo	nebo	k8xC
přes	přes	k7c4
Ethernet	Ethernet	k1gInSc4
(	(	kIx(
<g/>
pomocí	pomocí	k7c2
PoE	PoE	k1gFnSc2
hat	hat	k0
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
V	v	k7c4
přes	přes	k7c4
USB-C	USB-C	k1gFnSc4
<g/>
,	,	kIx,
GPIO	GPIO	kA
nebo	nebo	k8xC
přes	přes	k7c4
Ethernet	Ethernet	k1gInSc4
(	(	kIx(
<g/>
pomocí	pomocí	k7c2
PoE	PoE	k1gFnSc2
hat	hat	k0
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
V	V	kA
přes	přes	k7c4
MicroUSB	MicroUSB	k1gFnPc4
nebo	nebo	k8xC
GPIO	GPIO	kA
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
85,60	85,60	k4
mm	mm	kA
×	×	k?
56,5	56,5	k4
mm	mm	kA
(	(	kIx(
<g/>
3,370	3,370	k4
×	×	k?
2,224	2,224	k4
×	×	k?
v	v	k7c4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
bez	bez	k7c2
vyčnívajících	vyčnívající	k2eAgInPc2d1
konektorů	konektor	k1gInPc2
<g/>
65	#num#	k4
mm	mm	kA
×	×	k?
56,5	56,5	k4
mm	mm	kA
×	×	k?
10	#num#	k4
mm	mm	kA
</s>
<s>
85,60	85,60	k4
mm	mm	kA
×	×	k?
56,5	56,5	k4
mm	mm	kA
<g/>
,	,	kIx,
bez	bez	k7c2
vyčnívajících	vyčnívající	k2eAgInPc2d1
konektorů	konektor	k1gInPc2
</s>
<s>
67,6	67,6	k4
mm	mm	kA
×	×	k?
30	#num#	k4
mm	mm	kA
</s>
<s>
65	#num#	k4
mm	mm	kA
×	×	k?
30	#num#	k4
mm	mm	kA
×	×	k?
5	#num#	k4
mm	mm	kA
</s>
<s>
85,60	85,60	k4
mm	mm	kA
×	×	k?
56,5	56,5	k4
mm	mm	kA
×	×	k?
17	#num#	k4
mm	mm	kA
</s>
<s>
67,6	67,6	k4
mm	mm	kA
×	×	k?
30	#num#	k4
mm	mm	kA
</s>
<s>
67,6	67,6	k4
mm	mm	kA
×	×	k?
31	#num#	k4
mm	mm	kA
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
31	#num#	k4
g	g	kA
(	(	kIx(
<g/>
1,1	1,1	k4
unce	unce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
23	#num#	k4
g	g	kA
(	(	kIx(
<g/>
0,81	0,81	k4
unce	unce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
45	#num#	k4
g	g	kA
(	(	kIx(
<g/>
1,6	1,6	k4
unce	unce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
g	g	kA
(	(	kIx(
<g/>
0,25	0,25	k4
unce	unce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
g	g	kA
</s>
<s>
Konzole	konzola	k1gFnSc3
</s>
<s>
Micro-USB	Micro-USB	k?
kabel	kabel	k1gInSc1
nebo	nebo	k8xC
sériový	sériový	k2eAgInSc1d1
kabel	kabel	k1gInSc1
s	s	k7c7
napájecím	napájecí	k2eAgInSc7d1
konektorem	konektor	k1gInSc7
<g/>
,	,	kIx,
volitelný	volitelný	k2eAgMnSc1d1
GPIO	GPIO	kA
</s>
<s>
USB-C	USB-C	k?
kabel	kabel	k1gInSc1
nebo	nebo	k8xC
sériový	sériový	k2eAgInSc1d1
kabel	kabel	k1gInSc1
s	s	k7c7
napájecím	napájecí	k2eAgInSc7d1
konektorem	konektor	k1gInSc7
<g/>
,	,	kIx,
volitelný	volitelný	k2eAgMnSc1d1
GPIO	GPIO	kA
</s>
<s>
Micro-USB	Micro-USB	k?
kabel	kabel	k1gInSc1
nebo	nebo	k8xC
sériový	sériový	k2eAgInSc1d1
kabel	kabel	k1gInSc1
s	s	k7c7
napájecím	napájecí	k2eAgInSc7d1
konektorem	konektor	k1gInSc7
<g/>
,	,	kIx,
volitelný	volitelný	k2eAgMnSc1d1
GPIO	GPIO	kA
</s>
<s>
Generace	generace	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
lite	lit	k1gInSc5
</s>
<s>
PCB	PCB	kA
verze	verze	k1gFnSc1
1.2	1.2	k4
</s>
<s>
PCB	PCB	kA
verze	verze	k1gFnSc1
1.3	1.3	k4
</s>
<s>
W	W	kA
(	(	kIx(
<g/>
wireless	wireless	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Model	model	k1gInSc1
A	a	k9
</s>
<s>
Model	model	k1gInSc1
B	B	kA
</s>
<s>
Výpočetní	výpočetní	k2eAgInSc1d1
modul	modul	k1gInSc1
</s>
<s>
Zero	Zero	k6eAd1
</s>
<s>
*	*	kIx~
moduly	modul	k1gInPc1
se	se	k3xPyFc4
připojují	připojovat	k5eAaImIp3nP
200	#num#	k4
<g/>
-pinovým	-pinův	k2eAgInSc7d1
DDR2	DDR2	k1gFnSc4
SO-DIMM	SO-DIMM	k1gFnSc2
konektorem	konektor	k1gInSc7
k	k	k7c3
desce	deska	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Konektory	konektor	k1gInPc1
</s>
<s>
Pi	pi	k0
Zero	Zero	k1gMnSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
konektorůa	konektorů	k1gInSc2
hlavních	hlavní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
</s>
<s>
Model	model	k1gInSc1
A	A	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
<g/>
+	+	kIx~
</s>
<s>
Umístění	umístění	k1gNnSc1
konektorůa	konektorů	k1gInSc2
hlavních	hlavní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
na	na	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
1	#num#	k4
Model	model	k1gInSc4
A	a	k9
</s>
<s>
Umístění	umístění	k1gNnSc1
konektorůa	konektorů	k1gInSc2
hlavních	hlavní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
na	na	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
1	#num#	k4
Model	model	k1gInSc1
A	A	kA
<g/>
+	+	kIx~
revize	revize	k1gFnSc1
1.1	1.1	k4
</s>
<s>
Model	model	k1gInSc1
B	B	kA
<g/>
/	/	kIx~
<g/>
B	B	kA
<g/>
+	+	kIx~
</s>
<s>
Umístění	umístění	k1gNnSc1
konektorůa	konektorů	k1gInSc2
hlavních	hlavní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
na	na	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
1	#num#	k4
Model	model	k1gInSc1
B	B	kA
revize	revize	k1gFnSc1
1.2	1.2	k4
</s>
<s>
Umístění	umístění	k1gNnSc1
konektorůa	konektorů	k1gInSc2
hlavních	hlavní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
na	na	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
1	#num#	k4
Model	model	k1gInSc1
B	B	kA
<g/>
+	+	kIx~
revize	revize	k1gFnSc1
1.2	1.2	k4
a	a	k8xC
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
2	#num#	k4
</s>
<s>
Umístění	umístění	k1gNnSc1
konektorůa	konektorů	k1gInSc2
hlavních	hlavní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
na	na	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
3	#num#	k4
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
Výrobce	výrobce	k1gMnSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
nabízí	nabízet	k5eAaImIp3nS
k	k	k7c3
počítači	počítač	k1gInSc3
jako	jako	k8xC,k8xS
operační	operační	k2eAgInSc1d1
systémy	systém	k1gInPc7
ARMové	ARMové	k2eAgFnSc2d1
verze	verze	k1gFnSc2
linuxových	linuxový	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
Debian	Debiana	k1gFnPc2
a	a	k8xC
Arch	archa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobce	výrobce	k1gMnSc1
též	též	k9
ohlásil	ohlásit	k5eAaPmAgMnS
práce	práce	k1gFnPc4
na	na	k7c6
systému	systém	k1gInSc6
Rasdroid	Rasdroid	k1gInSc4
pro	pro	k7c4
Raspberry	Raspberr	k1gInPc4
Pi	pi	k0
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
založeném	založený	k2eAgInSc6d1
na	na	k7c6
systému	systém	k1gInSc6
Android	android	k1gInSc1
4.0	4.0	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2014	#num#	k4
uvolnila	uvolnit	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Broadcom	Broadcom	k1gInSc4
ovladač	ovladač	k1gInSc1
grafického	grafický	k2eAgInSc2d1
procesoru	procesor	k1gInSc2
použitého	použitý	k2eAgInSc2d1
v	v	k7c4
Raspberry	Raspberr	k1gInPc4
Pi	pi	k0
<g/>
,	,	kIx,
pod	pod	k7c4
Open	Open	k1gInSc4
Source	Sourec	k1gInSc2
BSD	BSD	kA
licencí	licence	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
nadace	nadace	k1gFnSc2
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundantion	Foundantion	k1gInSc1
vyhlásila	vyhlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgNnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
na	na	k7c4
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
přizpůsobí	přizpůsobit	k5eAaPmIp3nS
a	a	k8xC
rozběhne	rozběhnout	k5eAaPmIp3nS
hru	hra	k1gFnSc4
Quake	Quak	k1gFnSc2
III	III	kA
<g/>
,	,	kIx,
obdrží	obdržet	k5eAaPmIp3nS
prémii	prémie	k1gFnSc4
10	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
neobsahují	obsahovat	k5eNaImIp3nP
žádné	žádný	k3yNgNnSc4
rozhraní	rozhraní	k1gNnSc4
pro	pro	k7c4
pevný	pevný	k2eAgInSc4d1
disk	disk	k1gInSc4
nebo	nebo	k8xC
SSD	SSD	kA
–	–	k?
pro	pro	k7c4
zavedení	zavedení	k1gNnSc4
systému	systém	k1gInSc2
a	a	k8xC
trvalé	trvalá	k1gFnSc2
uchování	uchování	k1gNnSc2
dat	datum	k1gNnPc2
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
slot	slot	k1gInSc1
na	na	k7c4
microSD	microSD	k?
kartu	karta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInPc1d1
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Raspbian	Raspbian	k1gInSc1
(	(	kIx(
<g/>
Debian	Debian	k1gInSc1
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
<g/>
)	)	kIx)
</s>
<s>
Raspbian	Raspbian	k1gInSc1
2014-09	2014-09	k4
–	–	k?
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
s	s	k7c7
Javou	Java	k1gFnSc7
8	#num#	k4
<g/>
,	,	kIx,
Mathematica	Mathematica	k1gFnSc1
10	#num#	k4
<g/>
,	,	kIx,
Sonic	Sonic	k1gMnSc1
Pi	pi	k0
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
Minecraft	Minecraft	k1gInSc1
Pi	pi	k0
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raspbian	Raspbian	k1gInSc1
2014-12	2014-12	k4
–	–	k?
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
updaty	update	k1gInPc1
<g/>
,	,	kIx,
upgrady	upgrade	k1gInPc1
a	a	k8xC
fixy	fix	k1gInPc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Arch	arch	k1gInSc1
Linux	Linux	kA
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Razdroid	Razdroid	k1gInSc1
(	(	kIx(
<g/>
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
systému	systém	k1gInSc6
Android	android	k1gInSc1
–	–	k?
protože	protože	k8xS
je	být	k5eAaImIp3nS
v	v	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
použit	použit	k2eAgInSc1d1
mikroprocesor	mikroprocesor	k1gInSc4
z	z	k7c2
rodiny	rodina	k1gFnSc2
ARM	ARM	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
srovnatelný	srovnatelný	k2eAgInSc1d1
s	s	k7c7
běžným	běžný	k2eAgInSc7d1
smartphonem	smartphon	k1gInSc7
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Firefox	Firefox	k1gInSc1
OS	OS	kA
byl	být	k5eAaImAgInS
pro	pro	k7c4
Raspberry	Raspberr	k1gMnPc4
Pi	pi	k0
připravován	připravovat	k5eAaImNgInS
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Java	Java	k1gFnSc1
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Python	Python	k1gMnSc1
</s>
<s>
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Wolfram	wolfram	k1gInSc1
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
–	–	k?
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
použitý	použitý	k2eAgInSc1d1
programem	program	k1gInSc7
Mathematica	Mathematic	k1gInSc2
</s>
<s>
Programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
Wolfram	wolfram	k1gInSc1
nyní	nyní	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
nástroj	nástroj	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
pomocí	pomoc	k1gFnSc7
umí	umět	k5eAaImIp3nS
rozpoznávat	rozpoznávat	k5eAaImF
objekty	objekt	k1gInPc4
na	na	k7c6
obrázku	obrázek	k1gInSc6
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
2	#num#	k4
</s>
<s>
Kromě	kromě	k7c2
standardního	standardní	k2eAgInSc2d1
systému	systém	k1gInSc2
Raspbian	Raspbian	k1gInSc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
Raspberry	Raspberr	k1gInPc4
Pi	pi	k0
2	#num#	k4
k	k	k7c3
dispozici	dispozice	k1gFnSc3
i	i	k9
alternativní	alternativní	k2eAgInPc4d1
operační	operační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
pro	pro	k7c4
starší	starý	k2eAgInPc4d2
modely	model	k1gInPc4
nebyly	být	k5eNaImAgInP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
:	:	kIx,
</s>
<s>
linuxová	linuxový	k2eAgFnSc1d1
distribuce	distribuce	k1gFnSc1
Debian	Debiana	k1gFnPc2
Jessie	Jessie	k1gFnSc1
pro	pro	k7c4
RasbperryPi	RasbperryPi	k1gNnSc4
2	#num#	k4
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
NetBSD	NetBSD	k1gFnPc2
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
2	#num#	k4
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Windows	Windows	kA
10	#num#	k4
IoT	IoT	k1gFnSc2
Core	Core	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
nástupce	nástupce	k1gMnSc1
Windows	Windows	kA
Embedded	Embedded	k1gMnSc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
rebrandovaný	rebrandovaný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
jedna	jeden	k4xCgFnSc1
z	z	k7c2
variant	varianta	k1gFnPc2
Windows	Windows	kA
10	#num#	k4
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Windows	Windows	kA
10	#num#	k4
IoT	IoT	k1gFnSc2
Core	Cor	k1gFnSc2
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
2	#num#	k4
nemá	mít	k5eNaImIp3nS
vlastní	vlastní	k2eAgNnSc4d1
grafické	grafický	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ovládat	ovládat	k5eAaImF
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
pomocí	pomocí	k7c2
příkazového	příkazový	k2eAgInSc2d1
řádku	řádek	k1gInSc2
s	s	k7c7
Windows	Windows	kA
PowerShellem	PowerShell	k1gInSc7
nebo	nebo	k8xC
lze	lze	k6eAd1
spustit	spustit	k5eAaPmF
určitou	určitý	k2eAgFnSc4d1
aplikaci	aplikace	k1gFnSc4
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
3	#num#	k4
</s>
<s>
Pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
3	#num#	k4
bylo	být	k5eAaImAgNnS
portováno	portován	k2eAgNnSc1d1
jádro	jádro	k1gNnSc1
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
seL	sít	k5eAaImAgInS
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
mikrojádro	mikrojádro	k6eAd1
je	být	k5eAaImIp3nS
primárně	primárně	k6eAd1
určeno	určit	k5eAaPmNgNnS
pro	pro	k7c4
vestavěné	vestavěný	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
se	s	k7c7
zvláštním	zvláštní	k2eAgNnSc7d1
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
potřebě	potřeba	k1gFnSc3
počítačové	počítačový	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
,	,	kIx,
robustnosti	robustnost	k1gFnSc2
a	a	k8xC
výkonu	výkon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšiřující	rozšiřující	k2eAgFnPc1d1
desky	deska	k1gFnPc1
</s>
<s>
Pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
neoficiálních	neoficiální	k2eAgFnPc2d1,k2eNgFnPc2d1
rozšiřujících	rozšiřující	k2eAgFnPc2d1
desek	deska	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
například	například	k6eAd1
komunikaci	komunikace	k1gFnSc4
po	po	k7c4
RS	RS	kA
<g/>
232	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
třeba	třeba	k6eAd1
pomocí	pomocí	k7c2
RF	RF	kA
modulů	modul	k1gInPc2
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc1
DC	DC	kA
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
UniPi	UniPi	k6eAd1
Board	Board	k1gInSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
rozšiřovací	rozšiřovací	k2eAgInSc4d1
modul	modul	k1gInSc4
pro	pro	k7c4
minipočítač	minipočítač	k1gInSc4
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deska	deska	k1gFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
všechny	všechen	k3xTgFnPc4
důležité	důležitý	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
v	v	k7c6
rámci	rámec	k1gInSc6
řízení	řízení	k1gNnSc2
a	a	k8xC
ovládání	ovládání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
nabízí	nabízet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
mix	mix	k1gInSc4
vstupů	vstup	k1gInPc2
a	a	k8xC
výstupů	výstup	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
pro	pro	k7c4
sledování	sledování	k1gNnSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
umožňují	umožňovat	k5eAaImIp3nP
ovládání	ovládání	k1gNnSc4
sledovaných	sledovaný	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejsou	být	k5eNaImIp3nP
nutné	nutný	k2eAgInPc1d1
kabely	kabel	k1gInPc1
na	na	k7c4
propojení	propojení	k1gNnSc4
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
s	s	k7c7
relátky	relátko	k1gNnPc7
<g/>
,	,	kIx,
vstupy	vstup	k1gInPc7
ani	ani	k8xC
dalšími	další	k2eAgInPc7d1
prvky	prvek	k1gInPc7
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
I2C	I2C	k1gFnSc3
rozhraní	rozhraní	k1gNnSc2
vyvedenému	vyvedený	k2eAgInSc3d1
na	na	k7c4
externí	externí	k2eAgInSc4d1
konektor	konektor	k1gInSc4
lze	lze	k6eAd1
připojit	připojit	k5eAaPmF
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
periferií	periferie	k1gFnPc2
(	(	kIx(
<g/>
teoreticky	teoreticky	k6eAd1
až	až	k9
128	#num#	k4
<g/>
)	)	kIx)
–	–	k?
například	například	k6eAd1
obvody	obvod	k1gInPc1
pro	pro	k7c4
řízení	řízení	k1gNnSc4
relé	relé	k1gNnSc2
<g/>
,	,	kIx,
LED	LED	kA
<g/>
,	,	kIx,
vstupy	vstup	k1gInPc1
apod.	apod.	kA
UART	UART	kA
má	mít	k5eAaImIp3nS
na	na	k7c4
starost	starost	k1gFnSc4
připojení	připojení	k1gNnSc2
NFC	NFC	kA
čtečky	čtečka	k1gFnSc2
karet	kareta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
5	#num#	k4
<g/>
A	A	kA
<g/>
@	@	kIx~
<g/>
250	#num#	k4
<g/>
V	v	k7c6
relé	relé	k1gNnSc6
</s>
<s>
14	#num#	k4
<g/>
×	×	k?
digitální	digitální	k2eAgInSc1d1
vstup	vstup	k1gInSc1
5	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
V	v	k7c6
</s>
<s>
1	#num#	k4
<g/>
wire	wir	k1gInSc2
modul	modul	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
analogový	analogový	k2eAgInSc1d1
vstup	vstup	k1gInSc1
0	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
V	v	k7c6
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
analogový	analogový	k2eAgInSc1d1
výstup	výstup	k1gInSc1
0	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
V	v	k7c6
</s>
<s>
Modul	modul	k1gInSc1
reálného	reálný	k2eAgInSc2d1
času	čas	k1gInSc2
</s>
<s>
Možnost	možnost	k1gFnSc4
připojení	připojení	k1gNnSc2
rozšiřujících	rozšiřující	k2eAgNnPc2d1
relé	relé	k1gNnPc2
modulů	modul	k1gInPc2
a	a	k8xC
vstupů	vstup	k1gInPc2
</s>
<s>
EEPROM	EEPROM	kA
paměť	paměť	k1gFnSc1
</s>
<s>
Notifikační	notifikační	k2eAgFnPc1d1
diody	dioda	k1gFnPc1
<g/>
,	,	kIx,
pro	pro	k7c4
zobrazení	zobrazení	k1gNnSc4
stavu	stav	k1gInSc2
</s>
<s>
Gertboard	Gertboard	k1gMnSc1
</s>
<s>
Gertboard	Gertboard	k1gInSc1
není	být	k5eNaImIp3nS
oficiálním	oficiální	k2eAgInSc7d1
produktem	produkt	k1gInSc7
nadace	nadace	k1gFnSc2
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundantion	Foundantion	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
vyvinut	vyvinut	k2eAgMnSc1d1
Gert	Gert	k1gMnSc1
Van	vana	k1gFnPc2
Loo	Loo	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
i	i	k9
na	na	k7c6
vývoji	vývoj	k1gInSc6
alfa	alfa	k1gNnSc2
verze	verze	k1gFnSc2
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
rozšiřující	rozšiřující	k2eAgFnSc6d1
desce	deska	k1gFnSc6
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
připojeno	připojit	k5eAaPmNgNnS
přes	přes	k7c4
SPI	spát	k5eAaImRp2nS
<g/>
:	:	kIx,
</s>
<s>
MCP4802	MCP4802	k4
–	–	k?
8	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
bitový	bitový	k2eAgInSc4d1
dvoukanálový	dvoukanálový	k2eAgInSc4d1
D	D	kA
<g/>
/	/	kIx~
<g/>
A	A	kA
převodník	převodník	k1gMnSc1
</s>
<s>
MCP3002	MCP3002	k4
–	–	k?
10	#num#	k4
<g/>
bitový	bitový	k2eAgInSc4d1
dvoukanálový	dvoukanálový	k2eAgInSc4d1
A	A	kA
<g/>
/	/	kIx~
<g/>
D	D	kA
převodník	převodník	k1gMnSc1
</s>
<s>
předprogramovaný	předprogramovaný	k2eAgMnSc1d1
Atmel	Atmel	k1gMnSc1
AVR	AVR	kA
ATmega	ATmega	k1gFnSc1
328P	328P	k4
připojená	připojený	k2eAgFnSc1d1
přes	přes	k7c4
UART	UART	kA
</s>
<s>
obvod	obvod	k1gInSc1
pro	pro	k7c4
řízení	řízení	k1gNnSc4
motoru	motor	k1gInSc2
L2603-MW	L2603-MW	k1gFnPc2
připojený	připojený	k2eAgInSc4d1
k	k	k7c3
PWM	PWM	kA
</s>
<s>
3	#num#	k4
tlačítka	tlačítko	k1gNnPc1
</s>
<s>
12	#num#	k4
IO	IO	kA
pinů	pin	k1gInPc2
připojeno	připojit	k5eAaPmNgNnS
přes	přes	k7c4
obvod	obvod	k1gInSc4
74	#num#	k4
<g/>
xx	xx	k?
<g/>
244	#num#	k4
<g/>
,	,	kIx,
pro	pro	k7c4
signalizaci	signalizace	k1gFnSc4
je	být	k5eAaImIp3nS
použito	použít	k5eAaPmNgNnS
12	#num#	k4
LED	LED	kA
diod	dioda	k1gFnPc2
</s>
<s>
6	#num#	k4
výstupů	výstup	k1gInPc2
s	s	k7c7
otevřeným	otevřený	k2eAgInSc7d1
kolektorem	kolektor	k1gInSc7
(	(	kIx(
<g/>
ULN	ULN	kA
<g/>
2803	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Gertboard	Gertboard	k1gInSc1
se	se	k3xPyFc4
připojuje	připojovat	k5eAaImIp3nS
k	k	k7c3
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
přes	přes	k7c4
26	#num#	k4
<g/>
žilový	žilový	k2eAgInSc4d1
plochý	plochý	k2eAgInSc4d1
kabel	kabel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
prodeji	prodej	k1gInSc6
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
dostupný	dostupný	k2eAgMnSc1d1
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
stavebnice	stavebnice	k1gFnSc1
<g/>
,	,	kIx,
zájemce	zájemce	k1gMnSc1
si	se	k3xPyFc3
tedy	tedy	k9
musí	muset	k5eAaImIp3nS
osadit	osadit	k5eAaPmF
desku	deska	k1gFnSc4
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
výpočetní	výpočetní	k2eAgFnSc7d1
modul	modul	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
primárně	primárně	k6eAd1
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
připojení	připojení	k1gNnSc4
k	k	k7c3
vlastní	vlastní	k2eAgFnSc3d1
desce	deska	k1gFnSc3
plošných	plošný	k2eAgInPc2d1
spojů	spoj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Obsahuje	obsahovat	k5eAaImIp3nS
zejména	zejména	k9
<g/>
:	:	kIx,
</s>
<s>
Procesor	procesor	k1gInSc1
BCM2835	BCM2835	k1gFnSc2
z	z	k7c2
rodiny	rodina	k1gFnSc2
ARM11	ARM11	k1gFnPc2
taktovaný	taktovaný	k2eAgMnSc1d1
na	na	k7c4
700	#num#	k4
MHz	Mhz	kA
</s>
<s>
Grafický	grafický	k2eAgInSc1d1
procesor	procesor	k1gInSc1
VideoCore	VideoCor	k1gInSc5
IV	IV	kA
<g/>
,	,	kIx,
podporující	podporující	k2eAgFnSc1d1
OpenGL	OpenGL	k1gFnSc1
ES	ES	kA
2.0	2.0	k4
<g/>
,	,	kIx,
1080	#num#	k4
<g/>
p	p	k?
<g/>
30	#num#	k4
<g/>
,	,	kIx,
MPEG-4	MPEG-4	k1gFnSc1
</s>
<s>
512	#num#	k4
MiB	MiB	k1gFnSc1
RAM	RAM	kA
sdílených	sdílený	k2eAgInPc2d1
s	s	k7c7
grafickou	grafický	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
</s>
<s>
4	#num#	k4
GiB	GiB	k1gMnSc1
eMMC	eMMC	k?
flash	flash	k1gMnSc1
paměti	paměť	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
ekvivalentem	ekvivalent	k1gInSc7
SD	SD	kA
karty	karta	k1gFnSc2
v	v	k7c6
klasickém	klasický	k2eAgInSc6d1
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
tedy	tedy	k9
softwarově	softwarově	k6eAd1
kompatibilní	kompatibilní	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Toto	tento	k3xDgNnSc1
všechno	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
integrováno	integrovat	k5eAaBmNgNnS
na	na	k7c6
malé	malá	k1gFnSc6
<g/>
,	,	kIx,
67,6	67,6	k4
×	×	k?
30	#num#	k4
<g/>
mm	mm	kA
destičce	destička	k1gFnSc6
pasující	pasující	k2eAgFnSc6d1
do	do	k7c2
standardního	standardní	k2eAgInSc2d1
DDR2	DDR2	k1gMnPc3
SODIMM	SODIMM	kA
konektoru	konektor	k1gInSc2
(	(	kIx(
<g/>
jde	jít	k5eAaImIp3nS
o	o	k7c4
stejný	stejný	k2eAgInSc4d1
typ	typ	k1gInSc4
konektoru	konektor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
použit	použít	k5eAaPmNgInS
pro	pro	k7c4
paměť	paměť	k1gFnSc4
laptopů	laptop	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmíněná	zmíněný	k2eAgFnSc1d1
flash	flasha	k1gFnPc2
paměť	paměť	k1gFnSc1
je	být	k5eAaImIp3nS
připojena	připojen	k2eAgFnSc1d1
přímo	přímo	k6eAd1
k	k	k7c3
procesoru	procesor	k1gInSc3
na	na	k7c6
destičce	destička	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
zbývající	zbývající	k2eAgNnPc1d1
procesorová	procesorový	k2eAgNnPc1d1
rozhraní	rozhraní	k1gNnPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
uživateli	uživatel	k1gMnSc3
přes	přes	k7c4
piny	pin	k1gInPc4
konektoru	konektor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Raspberry	Raspberra	k1gFnPc4
výpočetní	výpočetní	k2eAgInSc1d1
mudul	mudul	k1gInSc1
byl	být	k5eAaImAgInS
uvolněn	uvolnit	k5eAaPmNgInS
pod	pod	k7c7
licencí	licence	k1gFnSc7
BSD	BSD	kA
jako	jako	k8xC,k8xS
Open	Open	k1gMnSc1
Hardware	hardware	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
vstupně-výstupní	vstupně-výstupní	k2eAgInPc1d1
modul	modul	k1gInSc1
</s>
<s>
Vstupně	vstupně	k6eAd1
<g/>
/	/	kIx~
<g/>
výstupní	výstupní	k2eAgInSc1d1
modul	modul	k1gInSc1
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
pro	pro	k7c4
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
výpočetním	výpočetní	k2eAgInSc7d1
modulem	modul	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
zejména	zejména	k9
napájení	napájení	k1gNnSc1
<g/>
,	,	kIx,
konektory	konektor	k1gInPc1
a	a	k8xC
rozhraní	rozhraní	k1gNnSc1
klasického	klasický	k2eAgMnSc2d1
Raspberry	Raspberra	k1gMnSc2
Pi	pi	k0
<g/>
,	,	kIx,
jako	jako	k9
<g/>
:	:	kIx,
</s>
<s>
konektor	konektor	k1gInSc1
HDMI	HDMI	kA
</s>
<s>
konektory	konektor	k1gInPc1
USB	USB	kA
</s>
<s>
GPIO	GPIO	kA
konektory	konektor	k1gInPc4
a	a	k8xC
další	další	k2eAgFnPc4d1
</s>
<s>
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
VGA	VGA	kA
Adaptér	adaptér	k1gInSc1
</s>
<s>
Adaptér	adaptér	k1gMnSc1
(	(	kIx(
<g/>
Gert	Gert	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
VGA	VGA	kA
Adapter	Adapter	k1gInSc1
<g/>
)	)	kIx)
pro	pro	k7c4
připojení	připojení	k1gNnSc4
VGA	VGA	kA
monitoru	monitor	k1gInSc2
k	k	k7c3
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
model	model	k1gInSc1
B	B	kA
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
vlastně	vlastně	k9
o	o	k7c4
pasivní	pasivní	k2eAgInSc4d1
digitálně-analogový	digitálně-analogový	k2eAgInSc4d1
převodník	převodník	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
připojuje	připojovat	k5eAaImIp3nS
na	na	k7c6
GPIO	GPIO	kA
konektor	konektor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umí	umět	k5eAaImIp3nS
přenést	přenést	k5eAaPmF
až	až	k6eAd1
rozlišení	rozlišení	k1gNnSc4
1920	#num#	k4
<g/>
×	×	k?
<g/>
1024	#num#	k4
bodů	bod	k1gInPc2
na	na	k7c4
60	#num#	k4
fps	fps	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
na	na	k7c6
GitHubu	GitHub	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
<g/>
:	:	kIx,
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
PCB	PCB	kA
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
GPIO	GPIO	kA
konektor	konektor	k1gInSc4
</s>
<s>
20	#num#	k4
<g/>
×	×	k?
odpor	odpor	k1gInSc1
(	(	kIx(
<g/>
rezistor	rezistor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
VGA	VGA	kA
konektor	konektor	k1gInSc4
</s>
<s>
HDMIPi	HDMIPi	k1gNnSc1
<g/>
,	,	kIx,
displej	displej	k1gInSc1
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
</s>
<s>
HDMIPi	HDMIPi	k6eAd1
je	být	k5eAaImIp3nS
displej	displej	k1gInSc1
primárně	primárně	k6eAd1
určený	určený	k2eAgInSc1d1
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
malých	malý	k2eAgInPc2d1
displejů	displej	k1gInPc2
má	mít	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc1
1280	#num#	k4
<g/>
×	×	k?
<g/>
800	#num#	k4
při	při	k7c6
velikosti	velikost	k1gFnSc6
9	#num#	k4
palců	palec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodává	prodávat	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
75	#num#	k4
liber	libra	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
asi	asi	k9
2	#num#	k4
700	#num#	k4
<g/>
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kamera	kamera	k1gFnSc1
</s>
<s>
Rozšiřující	rozšiřující	k2eAgInSc1d1
modul	modul	k1gInSc1
kamery	kamera	k1gFnSc2
se	se	k3xPyFc4
připojuje	připojovat	k5eAaImIp3nS
pomocí	pomocí	k7c2
plochého	plochý	k2eAgInSc2d1
flexibilního	flexibilní	k2eAgInSc2d1
kabelu	kabel	k1gInSc2
do	do	k7c2
CSI	CSI	kA
konektoru	konektor	k1gInSc2
umístěného	umístěný	k2eAgInSc2d1
mezi	mezi	k7c7
audio-video	audio-video	k6eAd1
výstupem	výstup	k1gInSc7
a	a	k8xC
HDMI	HDMI	kA
portem	port	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamera	kamera	k1gFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
pro	pro	k7c4
video	video	k1gNnSc4
rozlišení	rozlišení	k1gNnSc1
1080	#num#	k4
<g/>
p	p	k?
<g/>
,	,	kIx,
720	#num#	k4
<g/>
p	p	k?
a	a	k8xC
640	#num#	k4
<g/>
×	×	k?
<g/>
480	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozměry	rozměra	k1gFnPc4
modulu	modul	k1gInSc2
jsou	být	k5eAaImIp3nP
25	#num#	k4
<g/>
×	×	k?
<g/>
20	#num#	k4
<g/>
×	×	k?
<g/>
9	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KRČMÁŘ	Krčmář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
<g/>
:	:	kIx,
miniaturní	miniaturní	k2eAgInSc1d1
ARM	ARM	kA
počítač	počítač	k1gInSc1
za	za	k7c4
pár	pár	k4xCyI
stovek	stovka	k1gFnPc2
<g/>
.	.	kIx.
root	root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-9-1	2011-9-1	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
dorazil	dorazit	k5eAaPmAgInS
<g/>
,	,	kIx,
revoluční	revoluční	k2eAgInSc1d1
minipočítač	minipočítač	k1gInSc1
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
<g/>
.	.	kIx.
root	root	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
product	product	k1gMnSc1
launch	launch	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Introducing	Introducing	k1gInSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Model	model	k1gInSc1
B	B	kA
<g/>
+	+	kIx~
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ABCLinuxu	ABCLinux	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nitemedia	Nitemedium	k1gNnSc2
s.	s.	k?
r.	r.	kA
o.	o.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1214	#num#	k4
<g/>
-	-	kIx~
<g/>
1267	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundation	Foundation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Made	Made	k1gFnSc1
in	in	k?
the	the	k?
UK	UK	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-9-6	2012-9-6	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ARM	ARM	kA
Ltd	ltd	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cortex	Cortex	k1gInSc1
<g/>
™	™	k?
<g/>
-A	-A	k?
<g/>
7	#num#	k4
MPCore	MPCor	k1gInSc5
<g/>
™	™	k?
Technical	Technical	k1gFnPc6
Reference	reference	k1gFnSc2
Manual	Manual	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOLČÍK	HOLČÍK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
2	#num#	k4
se	se	k3xPyFc4
nerado	nerad	k2eAgNnSc4d1
fotí	fotit	k5eAaImIp3nS
s	s	k7c7
bleskem	blesk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CN	CN	kA
Invest	Invest	k1gMnSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
2015-02-09	2015-02-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
8991	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
UPTON	UPTON	kA
<g/>
,	,	kIx,
Eben	eben	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
400	#num#	k4
<g/>
:	:	kIx,
the	the	k?
$	$	kIx~
<g/>
70	#num#	k4
desktop	desktop	k1gInSc1
PC	PC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
2020-11-02	2020-11-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
NÝVLT	Nývlt	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolutně	absolutně	k6eAd1
nejlevnější	levný	k2eAgInSc4d3
počítač	počítač	k1gInSc4
na	na	k7c4
práci	práce	k1gFnSc4
i	i	k8xC
výuku	výuka	k1gFnSc4
nás	my	k3xPp1nPc2
nadchnul	nadchnout	k5eAaPmAgMnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IDNES	IDNES	kA
<g/>
,	,	kIx,
2020-11-12	2020-11-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ADAMS	ADAMS	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
2021-01-21	2021-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundation	Foundation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Android	android	k1gInSc1
4.0	4.0	k4
is	is	k?
coming	coming	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
http://sel4.systems	http://sel4.systems	k1gInSc1
<g/>
,	,	kIx,
2012-07-31	2012-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JEŽEK	Ježek	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Broadcomm	Broadcomm	k1gInSc1
uvolnil	uvolnit	k5eAaPmAgInS
dokumentaci	dokumentace	k1gFnSc4
a	a	k8xC
ovladač	ovladač	k1gInSc4
pro	pro	k7c4
GPU	GPU	kA
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
pod	pod	k7c7
BSD	BSD	kA
licencí	licence	k1gFnSc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CDR	CDR	kA
server	server	k1gInSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2014-03-03	2014-03-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyšla	vyjít	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
distribuce	distribuce	k1gFnSc2
Raspbian	Raspbian	k1gInSc1
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Raspbian	Raspbian	k1gInSc1
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
↑	↑	k?
Vánoční	vánoční	k2eAgInSc4d1
Raspbian	Raspbian	k1gInSc4
a	a	k8xC
nový	nový	k2eAgInSc4d1
NOOBS	NOOBS	kA
<g/>
↑	↑	k?
Android	android	k1gInSc1
na	na	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
<g/>
↑	↑	k?
Android	android	k1gInSc1
Pi	pi	k0
Wiki	Wiki	k1gNnPc3
<g/>
.	.	kIx.
androidpi	androidp	k1gMnSc3
<g/>
.	.	kIx.
<g/>
wikia	wikia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Mozilla	Mozilla	k1gFnSc1
chystá	chystat	k5eAaImIp3nS
Firefox	Firefox	k1gInSc4
OS	osa	k1gFnPc2
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
<g/>
↑	↑	k?
Integrating	Integrating	k1gInSc4
NetBeans	NetBeans	k1gInSc4
for	forum	k1gNnPc2
Raspberry	Raspberra	k1gMnSc2
Pi	pi	k0
Java	Jav	k2eAgFnSc1d1
Development	Development	k1gInSc1
<g/>
.	.	kIx.
blogs	blogs	k1gInSc1
<g/>
.	.	kIx.
<g/>
oracle	oracle	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
How	How	k1gFnSc2
to	ten	k3xDgNnSc1
deploy	deploa	k1gFnPc1
<g/>
,	,	kIx,
debug	debug	k1gInSc1
and	and	k?
profile	profil	k1gInSc5
Java	Javum	k1gNnPc1
on	on	k3xPp3gMnSc1
the	the	k?
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
<g/>
↑	↑	k?
Mathematica	Mathematic	k1gInSc2
10	#num#	k4
už	už	k6eAd1
je	být	k5eAaImIp3nS
zdarma	zdarma	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pro	pro	k7c4
Rapsberry	Rapsberra	k1gFnPc4
Pi	pi	k0
<g/>
↑	↑	k?
↑	↑	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
2	#num#	k4
<g/>
:	:	kIx,
Mnohokrát	mnohokrát	k6eAd1
rychlejší	rychlý	k2eAgFnSc1d2
a	a	k8xC
s	s	k7c7
podporou	podpora	k1gFnSc7
Windows	Windows	kA
10	#num#	k4
<g/>
↑	↑	k?
Debian	Debiana	k1gFnPc2
Jessie	Jessie	k1gFnSc1
pro	pro	k7c4
Rasbperry	Rasbperra	k1gFnPc4
Pi	pi	k0
2	#num#	k4
<g/>
↑	↑	k?
HUDSON	HUDSON	kA
<g/>
,	,	kIx,
Nick	Nick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
PI	pi	k0
2	#num#	k4
support	support	k1gInSc1
added	added	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NetBSD	NetBSD	k1gMnSc1
Blog	Blog	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-03-09	2015-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LARABEL	LARABEL	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NetBSD	NetBSD	k1gFnSc1
Now	Now	k1gFnPc2
Supports	Supportsa	k1gFnPc2
The	The	k1gMnSc2
Raspberry	Raspberra	k1gMnSc2
Pi	pi	k0
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoronix	Phoronix	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoronix	Phoronix	k1gInSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
2015-03-10	2015-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
2	#num#	k4
a	a	k8xC
Windows	Windows	kA
10	#num#	k4
IoT	IoT	k1gFnPc6
Core	Cor	k1gInSc2
<g/>
↑	↑	k?
Windows	Windows	kA
compatible	compatible	k6eAd1
hardware	hardware	k1gInSc1
development	development	k1gInSc1
boards	boards	k1gInSc1
–	–	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
2	#num#	k4
<g/>
↑	↑	k?
Introducing	Introducing	k1gInSc1
seL	sít	k5eAaImAgInS
<g/>
4	#num#	k4
4.0	4.0	k4
<g/>
.0	.0	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
http://sel4.systems	http://sel4.systems	k1gInSc1
<g/>
,	,	kIx,
2016-12-13	2016-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
elinux	elinux	k1gInSc1
<g/>
.	.	kIx.
<g/>
org-RPi_Expansion_Boards	org-RPi_Expansion_Boards	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-02-28	2017-02-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gertboard	Gertboard	k1gInSc1
is	is	k?
here	here	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-08-06	2012-08-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FEN	fena	k1gFnPc2
LOGIC	LOGIC	kA
Ltd	ltd	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gertboard	Gertboard	k1gInSc1
user	usrat	k5eAaPmRp2nS
manual	manual	k1gMnSc1
Rev	Rev	k1gMnSc1
1.0	1.0	k4
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
PDF	PDF	kA
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-08-06	2012-08-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
Compute	Comput	k1gInSc5
Module	modul	k1gInSc5
<g/>
:	:	kIx,
new	new	k?
product	product	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-07	2014-04-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VOŘÍŠEK	Voříšek	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raspberry	Raspberro	k1gNnPc7
Pi	pi	k0
bude	být	k5eAaImBp3nS
ještě	ještě	k9
svižnější	svižný	k2eAgNnSc1d2
<g/>
:	:	kIx,
Nově	nově	k6eAd1
si	se	k3xPyFc3
zahrajeme	zahrát	k5eAaPmIp1nP
i	i	k9
Quake	Quake	k1gInSc4
3	#num#	k4
ve	v	k7c4
Full	Full	k1gInSc4
HD	HD	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CDR	CDR	kA
server	server	k1gInSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2014-04-02	2014-04-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOŘÁNEK	BOŘÁNEK	kA
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výpočetní	výpočetní	k2eAgInSc1d1
modul	modul	k1gInSc1
pro	pro	k7c4
Rasbperry	Rasbperra	k1gFnPc4
Pi	pi	k0
je	být	k5eAaImIp3nS
open	open	k1gMnSc1
hardware	hardware	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Info	Info	k6eAd1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CALETKA	CALETKA	k?
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samostatný	samostatný	k2eAgInSc1d1
VGA	VGA	kA
výstup	výstup	k1gInSc1
z	z	k7c2
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
<g/>
.	.	kIx.
www.root.cz	www.root.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Info	Info	k6eAd1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
2014-09-11	2014-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gert	Gert	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
VGA	VGA	kA
Adapter	Adapter	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-09-10	2014-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://github.com/fenlogic/vga666	http://github.com/fenlogic/vga666	k4
Passive	Passiev	k1gFnSc2
VGA	VGA	kA
adapter	adapter	k1gInSc4
666	#num#	k4
for	forum	k1gNnPc2
Raspberry-Pi	Raspberry-Pi	k1gNnSc2
B	B	kA
<g/>
+	+	kIx~
<g/>
↑	↑	k?
HDMIPi	HDMIPi	k1gNnSc1
<g/>
,	,	kIx,
displej	displej	k1gInSc1
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
do	do	k7c2
distribuce	distribuce	k1gFnSc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Roseapple	Roseapple	k6eAd1
Pi	pi	k0
</s>
<s>
Banana	Banana	k1gFnSc1
Pi	pi	k0
</s>
<s>
AMD	AMD	kA
Gizmo	Gizma	k1gFnSc5
Board	Board	k1gMnSc1
</s>
<s>
Intel	Intel	kA
Galileo	Galilea	k1gFnSc5
</s>
<s>
Intel	Intel	kA
Edison	Edison	k1gMnSc1
</s>
<s>
Tinker	Tinker	k1gMnSc1
Board	Board	k1gMnSc1
(	(	kIx(
<g/>
Asus	Asus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
4	#num#	k4
má	mít	k5eAaImIp3nS
8GB	8GB	k4
verzi	verze	k1gFnSc4
<g/>
,	,	kIx,
k	k	k7c3
dispozici	dispozice	k1gFnSc3
je	být	k5eAaImIp3nS
beta	beta	k1gNnSc1
verze	verze	k1gFnSc2
64	#num#	k4
<g/>
bitového	bitový	k2eAgInSc2d1
systému	systém	k1gInSc2
-	-	kIx~
www.root.cz	www.root.cz	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
4B	4B	k4
je	být	k5eAaImIp3nS
venku	venku	k6eAd1
-	-	kIx~
přichází	přicházet	k5eAaImIp3nS
se	se	k3xPyFc4
4K	4K	k4
a	a	k8xC
dekódováním	dekódování	k1gNnSc7
H	H	kA
<g/>
.265	.265	k4
-	-	kIx~
www.root.cz	www.root.cz	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Přichází	přicházet	k5eAaImIp3nS
nový	nový	k2eAgInSc1d1
mini	mini	k2eAgInSc1d1
počítač	počítač	k1gInSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvládne	zvládnout	k5eAaPmIp3nS
dva	dva	k4xCgInPc4
monitory	monitor	k1gInPc4
se	s	k7c7
4	#num#	k4
<g/>
k	k	k7c3
-	-	kIx~
Technet	Technet	k1gMnSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
4	#num#	k4
léčí	léčit	k5eAaImIp3nP
neduhy	neduh	k1gInPc1
předchůdců	předchůdce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
rychlost	rychlost	k1gFnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
video	video	k1gNnSc4
-	-	kIx~
cnews	cnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Home	Home	k1gFnSc1
–	–	k?
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
projektu	projekt	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
CZ	CZ	kA
homepage	homepagat	k5eAaPmIp3nS
–	–	k?
neoficiální	neoficiální	k2eAgFnSc1d1,k2eNgFnSc1d1
stránka	stránka	k1gFnSc1
projektu	projekt	k1gInSc2
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
homepage	homepagat	k5eAaPmIp3nS
–	–	k?
články	článek	k1gInPc4
o	o	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
na	na	k7c4
ROOT	ROOT	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Diit	Diit	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
homepage	homepagat	k5eAaPmIp3nS
–	–	k?
články	článek	k1gInPc4
o	o	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
na	na	k7c4
DIIT	DIIT	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
bude	být	k5eAaImBp3nS
ještě	ještě	k9
svižnější	svižný	k2eAgNnSc1d2
<g/>
:	:	kIx,
Nově	nově	k6eAd1
si	se	k3xPyFc3
zahrajeme	zahrát	k5eAaPmIp1nP
i	i	k9
Quake	Quak	k1gFnPc1
III	III	kA
ve	v	k7c4
Full	Full	k1gInSc4
HD	HD	kA
–	–	k?
článek	článek	k1gInSc1
o	o	k7c4
portování	portování	k1gNnSc4
hry	hra	k1gFnSc2
Quake	Quak	k1gInSc2
III	III	kA
na	na	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Britská	britský	k2eAgFnSc1d1
kráľovná	kráľovný	k2eAgFnSc1d1
vyznamenala	vyznamenat	k5eAaPmAgFnS
tvorcu	tvorcu	k6eAd1
Raspberry	Raspberr	k1gMnPc4
Pi	pi	k0
–	–	k?
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
UniPi	UniP	k1gMnPc7
<g/>
.	.	kIx.
<g/>
technology	technolog	k1gMnPc7
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
PiCore	PiCor	k1gInSc5
5.3	5.3	k4
pro	pro	k7c4
Raspberry	Raspberr	k1gInPc4
Pi	pi	k0
aktualizuje	aktualizovat	k5eAaBmIp3nS
jádro	jádro	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Getting	Getting	k1gInSc1
Involved	Involved	k1gInSc1
With	With	k1gInSc1
The	The	k1gMnSc2
New	New	k1gMnSc2
Raspberry	Raspberra	k1gMnSc2
Pi	pi	k0
Graphics	Graphics	k1gInSc1
Driver	driver	k1gInSc1
–	–	k?
Phoronix	Phoronix	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
–	–	k?
Pi-Top	Pi-Top	k1gInSc1
<g/>
:	:	kIx,
notebook	notebook	k1gInSc1
se	s	k7c7
srdcem	srdce	k1gNnSc7
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
–	–	k?
Root	Rootum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Gallium	Gallium	k1gNnSc1
<g/>
3	#num#	k4
<g/>
D	D	kA
Driver	driver	k1gInSc1
Could	Could	k1gInSc1
Now	Now	k1gFnSc2
Run	Runa	k1gFnPc2
Significantly	Significantly	k1gMnSc1
Faster	Faster	k1gMnSc1
–	–	k?
Phoronix	Phoronix	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Getting	Getting	k1gInSc1
Plan	plan	k1gInSc1
9	#num#	k4
running	running	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
–	–	k?
Nástupce	nástupce	k1gMnSc4
Unixu	Unix	k1gInSc2
pro	pro	k7c4
Raspberry	Raspberra	k1gFnPc4
Pi	pi	k0
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Odroid-C	Odroid-C	k1gFnSc1
<g/>
1	#num#	k4
<g/>
:	:	kIx,
konkurent	konkurent	k1gMnSc1
RPi	RPi	k1gMnSc1
se	s	k7c7
čtyřjádrem	čtyřjádr	k1gInSc7
–	–	k?
ARM	ARM	kA
Cortex-A	Cortex-A	k1gFnSc2
<g/>
5	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
GiB	GiB	k1gMnPc2
RAM	RAM	kA
a	a	k8xC
Ubuntu	Ubunta	k1gFnSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
2	#num#	k4
model	model	k1gInSc4
B	B	kA
–	–	k?
představení	představení	k1gNnSc1
<g/>
,	,	kIx,
sestavení	sestavení	k1gNnSc1
<g/>
,	,	kIx,
instalace	instalace	k1gFnSc1
OS	OS	kA
–	–	k?
Recenze	recenze	k1gFnSc1
a	a	k8xC
instalace	instalace	k1gFnSc1
systému	systém	k1gInSc2
od	od	k7c2
www.linuxexpress.cz	www.linuxexpress.cza	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Vyšel	vyjít	k5eAaPmAgInS
Raspbian	Raspbian	k1gInSc1
2015-11-21	2015-11-21	k4
s	s	k7c7
podporou	podpora	k1gFnSc7
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Zero	Zero	k1gMnSc1
–	–	k?
Raspbian	Raspbian	k1gMnSc1
s	s	k7c7
podporou	podpora	k1gFnSc7
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
Zero	Zero	k1gNnSc5
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Mozilla	Mozilla	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
k	k	k7c3
IoT	IoT	k1gMnSc3
projektem	projekt	k1gInSc7
Things	Things	k1gInSc1
Gateway	Gatewaa	k1gFnSc2
<g/>
,	,	kIx,
bez	bez	k7c2
cloudu	cloud	k1gInSc2
a	a	k8xC
u	u	k7c2
vás	vy	k3xPp2nPc2
doma	doma	k6eAd1
-	-	kIx~
Článek	článek	k1gInSc1
na	na	k7c4
Rootu	Roota	k1gFnSc4
o	o	k7c6
aktivitách	aktivita	k1gFnPc6
Mozilly	Mozilla	k1gFnSc2
v	v	k7c6
IoT	IoT	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Uvedený	uvedený	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
výkonnejší	výkonnejší	k2eAgInSc1d1,k2eAgInSc1d2
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
3	#num#	k4
Model	model	k1gInSc1
B	B	kA
<g/>
+	+	kIx~
s	s	k7c7
gigabitovým	gigabitový	k2eAgInSc7d1
Ethernetom	Ethernetom	k1gInSc1
-	-	kIx~
Raspberry	Raspberra	k1gFnPc1
Pi	pi	k0
Model	model	k1gInSc1
3	#num#	k4
B	B	kA
<g/>
+	+	kIx~
na	na	k7c6
DSL	DSL	kA
<g/>
.	.	kIx.
<g/>
sk	sk	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Raspberry	Raspberr	k1gInPc1
Pi	pi	k0
3	#num#	k4
B	B	kA
<g/>
+	+	kIx~
má	mít	k5eAaImIp3nS
rychlejší	rychlý	k2eAgInSc4d2
Ethernet	Ethernet	k1gInSc4
<g/>
,	,	kIx,
dual-band	dual-band	k1gInSc4
Wi-Fi	Wi-F	k1gFnSc2
a	a	k8xC
PoE	PoE	k1gFnSc2
na	na	k7c4
Root	Root	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Superlevný	Superlevný	k2eAgInSc1d1
minipočítač	minipočítač	k1gInSc1
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
je	být	k5eAaImIp3nS
rychlejší	rychlý	k2eAgMnSc1d2
a	a	k8xC
nepotřebuje	potřebovat	k5eNaImIp3nS
zásuvku	zásuvka	k1gFnSc4
-	-	kIx~
Technet	Technet	k1gMnSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1027903738	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2013000263	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2013000263	#num#	k4
</s>
