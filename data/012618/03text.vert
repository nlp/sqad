<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Hesensko-Kasselský	hesenskoasselský	k2eAgMnSc1d1	hesensko-kasselský
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1787	[number]	k4	1787
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1867	[number]	k4	1867
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
synem	syn	k1gMnSc7	syn
prince	princ	k1gMnSc2	princ
Fridricha	Fridrich	k1gMnSc2	Fridrich
Hesensko-Kasselského	hesenskoasselský	k2eAgMnSc2d1	hesensko-kasselský
a	a	k8xC	a
Karoliny	Karolinum	k1gNnPc7	Karolinum
Nasavsko-Usingenské	Nasavsko-Usingenský	k2eAgFnSc2d1	Nasavsko-Usingenský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
potomci	potomek	k1gMnPc1	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1810	[number]	k4	1810
se	se	k3xPyFc4	se
Vilém	Vilém	k1gMnSc1	Vilém
v	v	k7c6	v
Amalienborgu	Amalienborg	k1gInSc6	Amalienborg
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
Luisou	Luisa	k1gFnSc7	Luisa
Šarlotou	Šarlota	k1gFnSc7	Šarlota
Dánskou	dánský	k2eAgFnSc7d1	dánská
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Frederika	Frederik	k1gMnSc2	Frederik
Dánského	dánský	k2eAgMnSc2d1	dánský
a	a	k8xC	a
Žofie	Žofie	k1gFnPc4	Žofie
Frederiky	Frederika	k1gFnSc2	Frederika
Meklenbursko-Zvěřínské	Meklenbursko-Zvěřínský	k2eAgFnSc2d1	Meklenbursko-Zvěřínský
<g/>
.	.	kIx.	.
</s>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
Luisou	Luisa	k1gFnSc7	Luisa
Šarlotou	Šarlota	k1gFnSc7	Šarlota
několik	několik	k4yIc4	několik
dětíː	dětíː	k?	dětíː
</s>
</p>
<p>
<s>
Karolina	Karolinum	k1gNnSc2	Karolinum
Frederika	Frederika	k1gFnSc1	Frederika
Marie	Marie	k1gFnSc1	Marie
Hesensko-Kasselská	hesenskoasselský	k2eAgFnSc1d1	hesensko-kasselský
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1811	[number]	k4	1811
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Hesensko-Kasselská	hesenskoasselský	k2eAgFnSc1d1	hesensko-kasselský
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1814	[number]	k4	1814
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luisa	Luisa	k1gFnSc1	Luisa
Hesensko-Kasselská	hesenskoasselský	k2eAgFnSc1d1	hesensko-kasselský
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1817-	[number]	k4	1817-
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
Vilém	Vilém	k1gMnSc1	Vilém
Hesensko-Kasselský	hesenskoasselský	k2eAgMnSc1d1	hesensko-kasselský
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1820	[number]	k4	1820
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Augusta	Augusta	k1gMnSc1	Augusta
Žofie	Žofie	k1gFnSc2	Žofie
Frederika	Frederika	k1gFnSc1	Frederika
Hesensko-Kasselská	hesenskoasselský	k2eAgFnSc1d1	hesensko-kasselský
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1823	[number]	k4	1823
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žofie	Žofie	k1gFnSc1	Žofie
Vilemína	Vilemína	k1gFnSc1	Vilemína
Hesensko-Kasselská	hesenskoasselský	k2eAgFnSc1d1	hesensko-kasselský
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Prince	princ	k1gMnSc2	princ
William	William	k1gInSc1	William
of	of	k?	of
Hesse-Kassel	Hesse-Kassela	k1gFnPc2	Hesse-Kassela
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vilém	Vilém	k1gMnSc1	Vilém
Hesensko-Kasselský	hesenskoasselský	k2eAgMnSc1d1	hesensko-kasselský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
