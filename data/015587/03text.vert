<s>
Systemd	Systemd	k6eAd1
</s>
<s>
systemd	systemd	k6eAd1
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Lennart	Lennart	k1gInSc1
Poettering	Poettering	k1gInSc1
<g/>
,	,	kIx,
Kay	Kay	k1gFnSc1
Sievers	Sieversa	k1gFnPc2
a	a	k8xC
další	další	k2eAgNnSc4d1
První	první	k4xOgNnSc4
vydání	vydání	k1gNnSc4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
Aktuální	aktuální	k2eAgFnSc2d1
verze	verze	k1gFnSc2
</s>
<s>
248	#num#	k4
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Linux	Linux	kA
Vyvíjeno	vyvíjet	k5eAaImNgNnS
v	v	k7c6
</s>
<s>
C	C	kA
Typ	typ	k1gInSc1
softwaru	software	k1gInSc2
</s>
<s>
Init	Init	k2eAgMnSc1d1
démon	démon	k1gMnSc1
Licence	licence	k1gFnSc2
</s>
<s>
původně	původně	k6eAd1
GPL	GPL	kA
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
LGPL	LGPL	kA
v	v	k7c6
<g/>
2.1	2.1	k4
<g/>
+	+	kIx~
Web	web	k1gInSc1
</s>
<s>
systemd	systemd	k1gInSc1
<g/>
.	.	kIx.
<g/>
io	io	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
systemd	systemd	k6eAd1
je	být	k5eAaImIp3nS
démon	démon	k1gMnSc1
pro	pro	k7c4
správu	správa	k1gFnSc4
systému	systém	k1gInSc2
navržený	navržený	k2eAgMnSc1d1
a	a	k8xC
vyvinutý	vyvinutý	k2eAgMnSc1d1
exkluzivně	exkluzivně	k6eAd1
pro	pro	k7c4
Linux	linux	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
API	API	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
systémech	systém	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
systemd	systemd	k6eAd1
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
spuštěn	spustit	k5eAaPmNgInS
v	v	k7c6
user	usrat	k5eAaPmRp2nS
space	space	k1gMnSc4
během	během	k7c2
zavádění	zavádění	k1gNnSc2
(	(	kIx(
<g/>
bootování	bootování	k1gNnSc2
<g/>
)	)	kIx)
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systemd	Systemd	k1gInSc1
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
kořenový	kořenový	k2eAgInSc1d1
proces	proces	k1gInSc1
všech	všecek	k3xTgInPc2
ostatních	ostatní	k2eAgInPc2d1
procesů	proces	k1gInPc2
v	v	k7c6
user	usrat	k5eAaPmRp2nS
space	spaec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
systemd	systemd	k6eAd1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
unixové	unixový	k2eAgFnSc2d1
konvence	konvence	k1gFnSc2
pro	pro	k7c4
odlišení	odlišení	k1gNnSc4
démonů	démon	k1gMnPc2
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
procesů	proces	k1gInPc2
přidáním	přidání	k1gNnSc7
písmena	písmeno	k1gNnPc4
d	d	k?
jako	jako	k8xS,k8xC
posledního	poslední	k2eAgNnSc2d1
písmena	písmeno	k1gNnSc2
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
názvu	název	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k8xS,k8xC
systemd	systemd	k6eAd1
se	se	k3xPyFc4
také	také	k9
nazývá	nazývat	k5eAaImIp3nS
kolekce	kolekce	k1gFnSc1
programů	program	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
systemd	systemd	k6eAd1
démon	démon	k1gMnSc1
<g/>
,	,	kIx,
logind	logind	k1gMnSc1
<g/>
,	,	kIx,
udevd	udevd	k6eAd1
a	a	k8xC
několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
nízkoúrovňových	nízkoúrovňový	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Souhrn	souhrn	k1gInSc1
</s>
<s>
systemd	systemd	k1gInSc1
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
pro	pro	k7c4
Linux	linux	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nahradil	nahradit	k5eAaPmAgInS
původní	původní	k2eAgInSc1d1
init	init	k2eAgInSc1d1
systém	systém	k1gInSc1
pocházející	pocházející	k2eAgInSc1d1
z	z	k7c2
UNIX	UNIX	kA
System	Syst	k1gInSc7
V	V	kA
a	a	k8xC
BSD	BSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démon	démon	k1gMnSc1
systemd	systemd	k1gMnSc1
spravuje	spravovat	k5eAaImIp3nS
ostatní	ostatní	k2eAgMnPc4d1
démony	démon	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgMnPc4
démony	démon	k1gMnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
systemd	systemda	k1gFnPc2
<g/>
,	,	kIx,
běží	běžet	k5eAaImIp3nP
jako	jako	k9
procesy	proces	k1gInPc1
na	na	k7c4
pozadí	pozadí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
zavádění	zavádění	k1gNnSc2
systému	systém	k1gInSc2
je	být	k5eAaImIp3nS
systemd	systemd	k6eAd1
spuštěn	spustit	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
démon	démon	k1gMnSc1
a	a	k8xC
během	během	k7c2
vypínání	vypínání	k1gNnSc2
systému	systém	k1gInSc2
ukončen	ukončen	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
poslední	poslední	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Lennart	Lennart	k1gInSc1
Poettering	Poettering	k1gInSc1
a	a	k8xC
Kay	Kay	k1gFnSc1
Sievers	Sieversa	k1gFnPc2
<g/>
,	,	kIx,
původní	původní	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
systemd	systemda	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
chtěli	chtít	k5eAaImAgMnP
v	v	k7c6
mnohém	mnohé	k1gNnSc6
překonat	překonat	k5eAaPmF
schopnosti	schopnost	k1gFnSc2
init	init	k5eAaPmF
démonu	démon	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěli	chtít	k5eAaImAgMnP
zlepšit	zlepšit	k5eAaPmF
framework	framework	k1gInSc4
pro	pro	k7c4
řešení	řešení	k1gNnSc4
závislostí	závislost	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
během	během	k7c2
bootování	bootování	k1gNnSc2
vykonat	vykonat	k5eAaPmF
více	hodně	k6eAd2
úloh	úloha	k1gFnPc2
současně	současně	k6eAd1
a	a	k8xC
redukovat	redukovat	k5eAaBmF
režii	režie	k1gFnSc4
shellu	shell	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
systemd	systemd	k1gInSc1
je	být	k5eAaImIp3nS
publikován	publikovat	k5eAaBmNgInS
jako	jako	k8xC,k8xS
svobodný	svobodný	k2eAgInSc1d1
software	software	k1gInSc1
pod	pod	k7c7
licencí	licence	k1gFnSc7
GNU	gnu	k1gMnSc1
Lesser	Lesser	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	License	k1gFnSc1
verze	verze	k1gFnSc1
2.1	2.1	k4
či	či	k8xC
novější	nový	k2eAgMnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
systemd	systemd	k6eAd1
vyvolává	vyvolávat	k5eAaImIp3nS
vášnivé	vášnivý	k2eAgFnPc4d1
diskuse	diskuse	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
bývá	bývat	k5eAaImIp3nS
kritizováno	kritizovat	k5eAaImNgNnS
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
systemd	systemd	k6eAd1
je	být	k5eAaImIp3nS
čistě	čistě	k6eAd1
pro	pro	k7c4
Linux	linux	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
systemd	systemd	k6eAd1
se	se	k3xPyFc4
nestará	starat	k5eNaImIp3nS
pouze	pouze	k6eAd1
o	o	k7c4
Init	Init	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pohlcuje	pohlcovat	k5eAaImIp3nS
další	další	k2eAgFnPc4d1
různé	různý	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
login	login	k2eAgInSc1d1
<g/>
,	,	kIx,
systém	systém	k1gInSc1
pro	pro	k7c4
synchronizaci	synchronizace	k1gFnSc4
času	čas	k1gInSc2
<g/>
,	,	kIx,
DNS	DNS	kA
<g/>
,	,	kIx,
logování	logování	k1gNnSc1
a	a	k8xC
další	další	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
odklání	odklánět	k5eAaImIp3nS
od	od	k7c2
filozofie	filozofie	k1gFnSc2
Unixu	Unix	k1gInSc2
„	„	k?
<g/>
dělat	dělat	k5eAaImF
jen	jen	k9
jednu	jeden	k4xCgFnSc4
věc	věc	k1gFnSc4
a	a	k8xC
tu	tu	k6eAd1
pořádně	pořádně	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Nestabilní	stabilní	k2eNgNnSc1d1
API	API	kA
měnící	měnící	k2eAgNnSc1d1
se	se	k3xPyFc4
s	s	k7c7
každou	každý	k3xTgFnSc7
verzí	verze	k1gFnSc7
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
aktivní	aktivní	k2eAgNnSc1d1
nasazování	nasazování	k1gNnSc1
do	do	k7c2
hlavních	hlavní	k2eAgFnPc2d1
distribucí	distribuce	k1gFnPc2
na	na	k7c6
straně	strana	k1gFnSc6
druhé	druhý	k4xOgFnSc6
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
kritizováno	kritizovat	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
reimplementací	reimplementace	k1gFnPc2
původně	původně	k6eAd1
samostatných	samostatný	k2eAgFnPc2d1
částí	část	k1gFnPc2
OS	OS	kA
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
logování	logování	k1gNnSc1
<g/>
,	,	kIx,
DNS	DNS	kA
atd.	atd.	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
implementováno	implementovat	k5eAaImNgNnS
neúplně	úplně	k6eNd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
vysloveně	vysloveně	k6eAd1
nekvalitně	kvalitně	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
dosti	dosti	k6eAd1
diskutovaná	diskutovaný	k2eAgFnSc1d1
kvalita	kvalita	k1gFnSc1
implementace	implementace	k1gFnSc2
a	a	k8xC
přístup	přístup	k1gInSc4
Lennarta	Lennarta	k1gFnSc1
Poetteringa	Poetteringa	k1gFnSc1
k	k	k7c3
chybám	chyba	k1gFnPc3
a	a	k8xC
k	k	k7c3
bugreportům	bugreport	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
systemd	systemd	k6eAd1
nemá	mít	k5eNaImIp3nS
dobrou	dobrý	k2eAgFnSc4d1
pověst	pověst	k1gFnSc4
v	v	k7c6
nestandardních	standardní	k2eNgFnPc6d1
situacích	situace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takříkajíc	takříkajíc	k6eAd1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
chování	chování	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
něco	něco	k3yInSc4
pokazí	pokazit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Rozšiřuje	rozšiřovat	k5eAaImIp3nS
se	se	k3xPyFc4
množství	množství	k1gNnSc1
software	software	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
přímo	přímo	k6eAd1
závislé	závislý	k2eAgNnSc1d1
na	na	k7c4
systemd	systemd	k1gInSc4
(	(	kIx(
<g/>
a	a	k8xC
nikoliv	nikoliv	k9
pouze	pouze	k6eAd1
nějakém	nějaký	k3yIgInSc6
rozhraní	rozhraní	k1gNnSc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
je	být	k5eAaImIp3nS
systemd	systemd	k6eAd1
implementací	implementace	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
trnem	trn	k1gInSc7
v	v	k7c6
oku	oko	k1gNnSc6
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
by	by	kYmCp3nP
rádi	rád	k2eAgMnPc1d1
alternativu	alternativa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Linux	linux	k1gInSc1
</s>
<s>
Init	Init	k1gMnSc1
</s>
<s>
Démon	démon	k1gMnSc1
(	(	kIx(
<g/>
software	software	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Systemd	Systemda	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
POETTERING	POETTERING	kA
<g/>
,	,	kIx,
Lennart	Lennart	k1gInSc1
<g/>
;	;	kIx,
SIEVERS	SIEVERS	kA
<g/>
,	,	kIx,
Kay	Kay	k1gFnSc1
<g/>
;	;	kIx,
LEEMHUIS	LEEMHUIS	kA
<g/>
,	,	kIx,
Thorsten	Thorsten	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Control	Control	k1gInSc1
Centre	centr	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gMnSc2
systemd	systemd	k6eAd1
Linux	Linux	kA
init	inita	k1gFnPc2
system	syst	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
H	H	kA
–	–	k?
Open	Open	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heise	Heise	k1gFnSc1
Media	medium	k1gNnSc2
UK	UK	kA
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2.6	2.6	k4
<g/>
.2018	.2018	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
README	README	kA
-	-	kIx~
systemd	systemd	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Root	Root	k1gInSc1
<g/>
/	/	kIx~
<g/>
README	README	kA
<g/>
.	.	kIx.
freedesktop	freedesktop	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
POETTERING	POETTERING	kA
<g/>
,	,	kIx,
Lennart	Lennart	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pid	Pid	k1gFnSc1
Eins	Einsa	k1gFnPc2
Full	Fulla	k1gFnPc2
Atom	atom	k1gInSc1
Feed	Feed	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projects	Projectsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lennart	Lennart	k1gInSc1
Poettering	Poettering	k1gInSc4
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
abclinuxu	abclinux	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
/	/	kIx~
<g/>
serialy	seriala	k1gFnSc2
<g/>
/	/	kIx~
<g/>
systemd	systemd	k1gInSc1
–	–	k?
Seriál	seriál	k1gInSc1
o	o	k7c4
systemd	systemd	k1gInSc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
