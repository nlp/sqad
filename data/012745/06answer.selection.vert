<s>
Erwin	Erwin	k1gMnSc1	Erwin
Johannes	Johannes	k1gMnSc1	Johannes
Eugen	Eugna	k1gFnPc2	Eugna
Rommel	Rommel	k1gMnSc1	Rommel
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Heidenheim	Heidenheim	k1gInSc1	Heidenheim
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Brenz	Brenz	k1gInSc1	Brenz
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Herrlingen	Herrlingen	k1gInSc1	Herrlingen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
též	též	k9	též
jako	jako	k8xS	jako
Pouštní	pouštní	k2eAgFnSc1d1	pouštní
liška	liška	k1gFnSc1	liška
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wüstenfuchs	Wüstenfuchs	k1gInSc1	Wüstenfuchs
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gFnSc1	The
Desert	desert	k1gInSc4	desert
Fox	fox	k1gInSc1	fox
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
polních	polní	k2eAgMnPc2d1	polní
taktiků	taktik	k1gMnPc2	taktik
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
