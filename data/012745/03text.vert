<p>
<s>
Erwin	Erwin	k1gMnSc1	Erwin
Johannes	Johannes	k1gMnSc1	Johannes
Eugen	Eugna	k1gFnPc2	Eugna
Rommel	Rommel	k1gMnSc1	Rommel
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Heidenheim	Heidenheim	k1gInSc1	Heidenheim
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Brenz	Brenz	k1gInSc1	Brenz
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
Herrlingen	Herrlingen	k1gInSc1	Herrlingen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
též	též	k9	též
jako	jako	k8xS	jako
Pouštní	pouštní	k2eAgFnSc1d1	pouštní
liška	liška	k1gFnSc1	liška
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wüstenfuchs	Wüstenfuchs	k1gInSc1	Wüstenfuchs
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gFnSc1	The
Desert	desert	k1gInSc4	desert
Fox	fox	k1gInSc1	fox
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
polních	polní	k2eAgMnPc2d1	polní
taktiků	taktik	k1gMnPc2	taktik
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hodnosti	hodnost	k1gFnSc3	hodnost
generál	generál	k1gMnSc1	generál
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
jakožto	jakožto	k8xS	jakožto
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
generálů	generál	k1gMnPc2	generál
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
jako	jako	k8xC	jako
velitel	velitel	k1gMnSc1	velitel
7	[number]	k4	7
<g/>
.	.	kIx.	.
tankové	tankový	k2eAgFnSc2d1	tanková
divize	divize	k1gFnSc2	divize
během	během	k7c2	během
tažení	tažení	k1gNnPc2	tažení
Francií	Francie	k1gFnSc7	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
velitel	velitel	k1gMnSc1	velitel
Afrikakorpsu	Afrikakorpsa	k1gFnSc4	Afrikakorpsa
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
jako	jako	k9	jako
inspektor	inspektor	k1gMnSc1	inspektor
Atlantického	atlantický	k2eAgInSc2d1	atlantický
valu	val	k1gInSc2	val
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
velitelů	velitel	k1gMnPc2	velitel
čelících	čelící	k2eAgMnPc2d1	čelící
spojeneckému	spojenecký	k2eAgNnSc3d1	spojenecké
vylodění	vylodění	k1gNnSc3	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
stykům	styk	k1gInPc3	styk
s	s	k7c7	s
atentátníky	atentátník	k1gMnPc7	atentátník
<g/>
,	,	kIx,	,
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
visí	viset	k5eAaImIp3nS	viset
na	na	k7c6	na
Stěně	stěna	k1gFnSc6	stěna
největších	veliký	k2eAgMnPc2d3	veliký
tankových	tankový	k2eAgMnPc2d1	tankový
velitelů	velitel	k1gMnPc2	velitel
amerického	americký	k2eAgNnSc2d1	americké
Pattonova	Pattonův	k2eAgNnSc2d1	Pattonovo
muzea	muzeum	k1gNnSc2	muzeum
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
a	a	k8xC	a
tankových	tankový	k2eAgNnPc2d1	tankové
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
pět	pět	k4xCc4	pět
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
tankových	tankový	k2eAgMnPc2d1	tankový
velitelů	velitel	k1gMnPc2	velitel
historie	historie	k1gFnSc2	historie
<g/>
:	:	kIx,	:
Mošeho	Moše	k1gMnSc2	Moše
Peleda	Peled	k1gMnSc2	Peled
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
Pattona	Patton	k1gMnSc2	Patton
<g/>
,	,	kIx,	,
Creightona	Creighton	k1gMnSc2	Creighton
Abramse	Abrams	k1gMnSc2	Abrams
<g/>
,	,	kIx,	,
Israela	Israel	k1gMnSc2	Israel
Tala	Talus	k1gMnSc2	Talus
a	a	k8xC	a
Erwina	Erwin	k1gMnSc2	Erwin
Rommela	Rommel	k1gMnSc2	Rommel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1891	[number]	k4	1891
v	v	k7c6	v
Heidenheimu	Heidenheim	k1gInSc6	Heidenheim
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Brenz	Brenz	k1gInSc1	Brenz
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
ředitele	ředitel	k1gMnSc2	ředitel
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c4	v
Aalen	Aalen	k1gInSc4	Aalen
Erwina	Erwin	k1gMnSc2	Erwin
Rommela	Rommel	k1gMnSc2	Rommel
(	(	kIx(	(
<g/>
†	†	k?	†
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
významného	významný	k2eAgMnSc2d1	významný
württenberského	württenberský	k2eAgMnSc2d1	württenberský
politika	politik	k1gMnSc2	politik
Helene	Helen	k1gInSc5	Helen
von	von	k1gInSc4	von
Luz	luza	k1gFnPc2	luza
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgMnPc4	tři
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
:	:	kIx,	:
Gerharda	Gerhard	k1gMnSc4	Gerhard
<g/>
,	,	kIx,	,
Helene	Helen	k1gInSc5	Helen
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastních	vlastní	k2eAgInPc2d1	vlastní
údajů	údaj	k1gInPc2	údaj
prožil	prožít	k5eAaPmAgMnS	prožít
obzvlášť	obzvlášť	k6eAd1	obzvlášť
příjemné	příjemný	k2eAgNnSc4d1	příjemné
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
mohl	moct	k5eAaImAgInS	moct
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
rozlehlé	rozlehlý	k2eAgFnSc6d1	rozlehlá
rodinné	rodinný	k2eAgFnSc6d1	rodinná
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Rommel	Rommel	k1gMnSc1	Rommel
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k5eAaPmF	stát
leteckým	letecký	k2eAgMnSc7d1	letecký
inženýrem	inženýr	k1gMnSc7	inženýr
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
Rommel	Rommel	k1gInSc1	Rommel
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
kamarádem	kamarád	k1gMnSc7	kamarád
letuschopný	letuschopný	k2eAgInSc4d1	letuschopný
kluzák	kluzák	k1gInSc4	kluzák
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otec	otec	k1gMnSc1	otec
ho	on	k3xPp3gInSc4	on
postavil	postavit	k5eAaPmAgMnS	postavit
před	před	k7c4	před
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
volbu	volba	k1gFnSc4	volba
<g/>
:	:	kIx,	:
buď	buď	k8xC	buď
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
vzoru	vzor	k1gInSc6	vzor
učitelem	učitel	k1gMnSc7	učitel
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
reálném	reálný	k2eAgNnSc6d1	reálné
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
Schwäbisch	Schwäbis	k1gFnPc6	Schwäbis
Gmündu	Gmünd	k1gInSc2	Gmünd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
kadet	kadet	k1gMnSc1	kadet
k	k	k7c3	k
místnímu	místní	k2eAgNnSc3d1	místní
6	[number]	k4	6
<g/>
.	.	kIx.	.
württemberskému	württemberský	k2eAgInSc3d1	württemberský
pěšímu	pěší	k2eAgInSc3d1	pěší
pluku	pluk	k1gInSc3	pluk
König	König	k1gMnSc1	König
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
I.	I.	kA	I.
č.	č.	k?	č.
124	[number]	k4	124
ve	v	k7c6	v
Weingartenu	Weingarten	k1gInSc6	Weingarten
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
byl	být	k5eAaImAgInS	být
povýšen	povýšen	k2eAgInSc1d1	povýšen
na	na	k7c4	na
desátníka	desátník	k1gMnSc4	desátník
a	a	k8xC	a
po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
šesti	šest	k4xCc6	šest
na	na	k7c4	na
četaře	četař	k1gMnPc4	četař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1911	[number]	k4	1911
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
školu	škola	k1gFnSc4	škola
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
dokončil	dokončit	k5eAaPmAgMnS	dokončit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
školu	škola	k1gFnSc4	škola
(	(	kIx(	(
<g/>
Kriegsschule	Kriegsschule	k1gFnSc1	Kriegsschule
<g/>
)	)	kIx)	)
a	a	k8xC	a
následovalo	následovat	k5eAaImAgNnS	následovat
povýšení	povýšení	k1gNnSc1	povýšení
na	na	k7c4	na
poručíka	poručík	k1gMnSc4	poručík
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Gdaňsku	Gdaňsk	k1gInSc6	Gdaňsk
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
i	i	k9	i
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
Lucii	Lucie	k1gFnSc4	Lucie
Marii	Maria	k1gFnSc3	Maria
Mollin	Mollina	k1gFnPc2	Mollina
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
veřejně	veřejně	k6eAd1	veřejně
zasnoubil	zasnoubit	k5eAaPmAgInS	zasnoubit
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
syna	syn	k1gMnSc2	syn
Manfreda	Manfred	k1gMnSc2	Manfred
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
-	-	kIx~	-
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
stuttgartského	stuttgartský	k2eAgMnSc2d1	stuttgartský
starosty	starosta	k1gMnSc2	starosta
-	-	kIx~	-
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Lucií	Lucie	k1gFnPc2	Lucie
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
i	i	k9	i
nemanželskou	manželský	k2eNgFnSc4d1	nemanželská
dceru	dcera	k1gFnSc4	dcera
Gertrud	Gertrud	k1gInSc1	Gertrud
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
další	další	k2eAgFnSc7d1	další
<g/>
,	,	kIx,	,
dvacetiletou	dvacetiletý	k2eAgFnSc7d1	dvacetiletá
láskou	láska	k1gFnSc7	láska
Walburgou	Walburga	k1gFnSc7	Walburga
Stemmer	Stemmra	k1gFnPc2	Stemmra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dlouho	dlouho	k6eAd1	dlouho
držel	držet	k5eAaImAgMnS	držet
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Rommelův	Rommelův	k2eAgInSc1d1	Rommelův
pluk	pluk	k1gInSc1	pluk
povolán	povolán	k2eAgInSc1d1	povolán
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
frontu	fronta	k1gFnSc4	fronta
(	(	kIx(	(
<g/>
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
však	však	k9	však
setrval	setrvat	k5eAaPmAgMnS	setrvat
ještě	ještě	k9	ještě
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
posily	posila	k1gFnPc4	posila
<g/>
,	,	kIx,	,
k	k	k7c3	k
pluku	pluk	k1gInSc3	pluk
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
až	až	k9	až
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
sdělil	sdělit	k5eAaPmAgMnS	sdělit
své	svůj	k3xOyFgFnSc3	svůj
sestře	sestra	k1gFnSc3	sestra
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
nemanželskou	manželský	k2eNgFnSc4d1	nemanželská
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc3	jejíž
matce	matka	k1gFnSc3	matka
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
smrti	smrt	k1gFnSc2	smrt
vyplacena	vyplatit	k5eAaPmNgFnS	vyplatit
jeho	jeho	k3xOp3gFnSc1	jeho
pojistka	pojistka	k1gFnSc1	pojistka
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
padl	padnout	k5eAaImAgMnS	padnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
bojoval	bojovat	k5eAaImAgMnS	bojovat
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Odpočátku	Odpočátko	k1gNnSc6	Odpočátko
projevoval	projevovat	k5eAaImAgInS	projevovat
nekonvenční	konvenční	k2eNgInSc1d1	nekonvenční
myšlení	myšlení	k1gNnSc2	myšlení
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
železným	železný	k2eAgInSc7d1	železný
křížem	kříž	k1gInSc7	kříž
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1914	[number]	k4	1914
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
došly	dojít	k5eAaPmAgFnP	dojít
náboje	náboj	k1gInPc4	náboj
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
s	s	k7c7	s
bajonetem	bajonet	k1gInSc7	bajonet
vrhl	vrhnout	k5eAaImAgMnS	vrhnout
na	na	k7c4	na
tři	tři	k4xCgMnPc4	tři
Francouze	Francouz	k1gMnPc4	Francouz
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
byl	být	k5eAaImAgInS	být
přeložen	přeložit	k5eAaPmNgInS	přeložit
k	k	k7c3	k
Württemberskému	Württemberský	k2eAgInSc3d1	Württemberský
horskému	horský	k2eAgInSc3d1	horský
praporu	prapor	k1gInSc3	prapor
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc4	součást
Deutsches	Deutschesa	k1gFnPc2	Deutschesa
Alpen	Alpen	k2eAgInSc1d1	Alpen
Korps	Korps	k1gInSc1	Korps
(	(	kIx(	(
<g/>
Německého	německý	k2eAgInSc2d1	německý
alpského	alpský	k2eAgInSc2d1	alpský
sboru	sbor	k1gInSc2	sbor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
bojoval	bojovat	k5eAaImAgInS	bojovat
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
proti	proti	k7c3	proti
Rumunům	Rumun	k1gMnPc3	Rumun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1917	[number]	k4	1917
následoval	následovat	k5eAaImAgInS	následovat
přesun	přesun	k1gInSc1	přesun
na	na	k7c4	na
italskou	italský	k2eAgFnSc4d1	italská
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Caporetta	Caporett	k1gInSc2	Caporett
účastnil	účastnit	k5eAaImAgMnS	účastnit
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
klíčový	klíčový	k2eAgInSc4d1	klíčový
bod	bod	k1gInSc4	bod
italských	italský	k2eAgFnPc2d1	italská
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
Monte	Mont	k1gInSc5	Mont
Matajur	Matajur	k1gMnSc1	Matajur
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
muži	muž	k1gMnPc7	muž
i	i	k8xC	i
přes	přes	k7c4	přes
přesilu	přesila	k1gFnSc4	přesila
obránců	obránce	k1gMnPc2	obránce
dobyl	dobýt	k5eAaPmAgMnS	dobýt
a	a	k8xC	a
zajal	zajmout	k5eAaPmAgInS	zajmout
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
Italů	Ital	k1gMnPc2	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
bylo	být	k5eAaImAgNnS	být
předem	předem	k6eAd1	předem
přislíbeno	přislíben	k2eAgNnSc1d1	přislíbeno
udělení	udělení	k1gNnSc1	udělení
nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
německého	německý	k2eAgNnSc2d1	německé
vojenského	vojenský	k2eAgNnSc2d1	vojenské
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
Pour	Pour	k1gMnSc1	Pour
le	le	k?	le
Mérite	Mérit	k1gMnSc5	Mérit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
omylu	omyl	k1gInSc3	omyl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
vyznamenán	vyznamenán	k2eAgMnSc1d1	vyznamenán
jiný	jiný	k2eAgMnSc1d1	jiný
velitel	velitel	k1gMnSc1	velitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dobyl	dobýt	k5eAaPmAgMnS	dobýt
jiný	jiný	k2eAgInSc4d1	jiný
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rommelově	Rommelův	k2eAgInSc6d1	Rommelův
rozhořčeném	rozhořčený	k2eAgInSc6d1	rozhořčený
protestu	protest	k1gInSc6	protest
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
uděleno	udělit	k5eAaPmNgNnS	udělit
i	i	k8xC	i
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
Rommel	Rommel	k1gInSc1	Rommel
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
odvolán	odvolat	k5eAaPmNgMnS	odvolat
a	a	k8xC	a
přidělen	přidělit	k5eAaPmNgInS	přidělit
do	do	k7c2	do
štábní	štábní	k2eAgFnSc2d1	štábní
funkce	funkce	k1gFnSc2	funkce
na	na	k7c6	na
Generálním	generální	k2eAgNnSc6d1	generální
velitelství	velitelství	k1gNnSc6	velitelství
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
nasazení	nasazení	k1gNnSc2	nasazení
č.	č.	k?	č.
64	[number]	k4	64
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Meziválečné	meziválečný	k2eAgNnSc4d1	meziválečné
období	období	k1gNnSc4	období
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Rommel	Rommel	k1gMnSc1	Rommel
u	u	k7c2	u
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
nově	nov	k1gInSc6	nov
formovaném	formovaný	k2eAgInSc6d1	formovaný
Reichswehru	Reichswehra	k1gFnSc4	Reichswehra
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
zabezpečovací	zabezpečovací	k2eAgFnSc2d1	zabezpečovací
roty	rota	k1gFnSc2	rota
č.	č.	k?	č.
32	[number]	k4	32
<g/>
,	,	kIx,	,
nasazené	nasazený	k2eAgMnPc4d1	nasazený
při	při	k7c6	při
nekrvavém	krvavý	k2eNgNnSc6d1	nekrvavé
potlačování	potlačování	k1gNnSc6	potlačování
levicových	levicový	k2eAgMnPc2d1	levicový
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednem	leden	k1gInSc7	leden
1921	[number]	k4	1921
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnem	říjen	k1gInSc7	říjen
1929	[number]	k4	1929
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
střelecké	střelecký	k2eAgFnSc2d1	střelecká
roty	rota	k1gFnSc2	rota
u	u	k7c2	u
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
č.	č.	k?	č.
13	[number]	k4	13
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1929	[number]	k4	1929
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
pěchotní	pěchotní	k2eAgFnSc2d1	pěchotní
taktiky	taktika	k1gFnSc2	taktika
na	na	k7c6	na
pěchotní	pěchotní	k2eAgFnSc6d1	pěchotní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
majora	major	k1gMnSc2	major
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
velitelem	velitel	k1gMnSc7	velitel
horského	horský	k2eAgNnSc2d1	horské
III	III	kA	III
<g/>
.	.	kIx.	.
praporu	prapor	k1gInSc2	prapor
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
č.	č.	k?	č.
17	[number]	k4	17
v	v	k7c6	v
Goslaru	Goslar	k1gInSc6	Goslar
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	s	k7c7	s
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1934	[number]	k4	1934
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
prapor	prapor	k1gInSc1	prapor
účastnil	účastnit	k5eAaImAgInS	účastnit
přehlídky	přehlídka	k1gFnSc2	přehlídka
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
dožínkových	dožínkový	k2eAgFnPc2d1	dožínková
oslav	oslava	k1gFnPc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
jejich	jejich	k3xOp3gInSc1	jejich
kontakt	kontakt	k1gInSc1	kontakt
nepřekročil	překročit	k5eNaPmAgInS	překročit
rámec	rámec	k1gInSc4	rámec
služebního	služební	k2eAgNnSc2d1	služební
setkání	setkání	k1gNnSc2	setkání
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
s	s	k7c7	s
důstojníkem	důstojník	k1gMnSc7	důstojník
zastávajícím	zastávající	k2eAgMnSc7d1	zastávající
relativně	relativně	k6eAd1	relativně
nedůležitou	důležitý	k2eNgFnSc4d1	nedůležitá
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
podplukovníka	podplukovník	k1gMnSc4	podplukovník
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
učitelem	učitel	k1gMnSc7	učitel
taktiky	taktika	k1gFnSc2	taktika
na	na	k7c6	na
pěchotní	pěchotní	k2eAgFnSc6d1	pěchotní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
říšským	říšský	k2eAgNnSc7d1	říšské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
války	válka	k1gFnSc2	válka
pověřen	pověřit	k5eAaPmNgMnS	pověřit
funkcí	funkce	k1gFnSc7	funkce
styčného	styčný	k2eAgMnSc4d1	styčný
důstojníka	důstojník	k1gMnSc4	důstojník
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
u	u	k7c2	u
vedení	vedení	k1gNnSc2	vedení
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
vojenský	vojenský	k2eAgInSc4d1	vojenský
výcvik	výcvik	k1gInSc4	výcvik
mladých	mladý	k2eAgMnPc2d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Infanterie	infanterie	k1gFnSc1	infanterie
greift	greift	k1gInSc1	greift
an	an	k?	an
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Pěchota	pěchota	k1gFnSc1	pěchota
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
)	)	kIx)	)
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
shrnul	shrnout	k5eAaPmAgMnS	shrnout
své	svůj	k3xOyFgFnSc2	svůj
zkušenosti	zkušenost	k1gFnSc2	zkušenost
pěchotního	pěchotní	k2eAgMnSc2d1	pěchotní
taktika	taktik	k1gMnSc2	taktik
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
pozornost	pozornost	k1gFnSc4	pozornost
širokých	široký	k2eAgInPc2d1	široký
čtenářských	čtenářský	k2eAgInPc2d1	čtenářský
kruhů	kruh	k1gInPc2	kruh
včetně	včetně	k7c2	včetně
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
Rommela	Rommel	k1gMnSc4	Rommel
pověřovati	pověřovat	k5eAaImF	pověřovat
občasnými	občasný	k2eAgInPc7d1	občasný
úkoly	úkol	k1gInPc7	úkol
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
povahy	povaha	k1gFnSc2	povaha
při	při	k7c6	při
zajišťování	zajišťování	k1gNnSc6	zajišťování
své	svůj	k3xOyFgFnSc2	svůj
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
září	září	k1gNnSc6	září
1937	[number]	k4	1937
při	při	k7c6	při
sjezdu	sjezd	k1gInSc6	sjezd
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
velení	velení	k1gNnSc2	velení
vůdcovu	vůdcův	k2eAgInSc3d1	vůdcův
doprovodnému	doprovodný	k2eAgInSc3d1	doprovodný
praporu	prapor	k1gInSc3	prapor
během	během	k7c2	během
vstupů	vstup	k1gInPc2	vstup
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
,	,	kIx,	,
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Memelu	Memel	k1gInSc2	Memel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
von	von	k1gInSc1	von
Schiracha	Schiracha	k1gMnSc1	Schiracha
odvolán	odvolat	k5eAaPmNgMnS	odvolat
od	od	k7c2	od
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
nebyl	být	k5eNaImAgMnS	být
žádným	žádný	k3yNgMnSc7	žádný
nacistou	nacista	k1gMnSc7	nacista
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
potlačoval	potlačovat	k5eAaImAgMnS	potlačovat
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
ideologickou	ideologický	k2eAgFnSc4d1	ideologická
výchovu	výchova	k1gFnSc4	výchova
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
vojenského	vojenský	k2eAgInSc2d1	vojenský
výcviku	výcvik	k1gInSc2	výcvik
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
plukovníkem	plukovník	k1gMnSc7	plukovník
a	a	k8xC	a
mezi	mezi	k7c7	mezi
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
1938	[number]	k4	1938
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
1939	[number]	k4	1939
<g />
.	.	kIx.	.
</s>
<s>
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
velitele	velitel	k1gMnSc2	velitel
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
akademie	akademie	k1gFnSc2	akademie
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
ve	v	k7c6	v
Vídeňském	vídeňský	k2eAgNnSc6d1	Vídeňské
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
v	v	k7c6	v
okupovaném	okupovaný	k2eAgNnSc6d1	okupované
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Ostmark	Ostmark	k1gInSc1	Ostmark
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
generálmajora	generálmajor	k1gMnSc2	generálmajor
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
pověřen	pověřit	k5eAaPmNgInS	pověřit
velením	velení	k1gNnSc7	velení
Vůdcova	vůdcův	k2eAgInSc2d1	vůdcův
hlavního	hlavní	k2eAgInSc2d1	hlavní
stanu	stan	k1gInSc2	stan
(	(	kIx(	(
<g/>
funkci	funkce	k1gFnSc4	funkce
krátce	krátce	k6eAd1	krátce
zastával	zastávat	k5eAaImAgMnS	zastávat
již	již	k6eAd1	již
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
četné	četný	k2eAgInPc4d1	četný
kontakty	kontakt	k1gInPc4	kontakt
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
osobnosti	osobnost	k1gFnSc6	osobnost
německého	německý	k2eAgInSc2d1	německý
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Josepha	Joseph	k1gMnSc2	Joseph
Goebbelse	Goebbels	k1gMnSc2	Goebbels
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
velitele	velitel	k1gMnSc2	velitel
vůdcova	vůdcův	k2eAgInSc2d1	vůdcův
hlavního	hlavní	k2eAgInSc2d1	hlavní
stanu	stan	k1gInSc2	stan
ho	on	k3xPp3gMnSc4	on
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
i	i	k9	i
počátek	počátek	k1gInSc4	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
propadal	propadat	k5eAaPmAgMnS	propadat
Hitlerovu	Hitlerův	k2eAgInSc3d1	Hitlerův
vlivu	vliv	k1gInSc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
polského	polský	k2eAgNnSc2d1	polské
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
během	během	k7c2	během
nějž	jenž	k3xRgMnSc4	jenž
dbal	dbát	k5eAaImAgMnS	dbát
na	na	k7c4	na
Vůdcovu	vůdcův	k2eAgFnSc4d1	Vůdcova
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
při	při	k7c6	při
Hitlerových	Hitlerových	k2eAgFnPc6d1	Hitlerových
návštěvách	návštěva	k1gFnPc6	návštěva
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
velení	velení	k1gNnSc6	velení
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
tankové	tankový	k2eAgFnPc1d1	tanková
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
armádní	armádní	k2eAgInSc1d1	armádní
personální	personální	k2eAgInSc1d1	personální
úřad	úřad	k1gInSc1	úřad
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
a	a	k8xC	a
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
absenci	absence	k1gFnSc4	absence
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
Rommelovy	Rommelův	k2eAgFnPc4d1	Rommelova
zkušenosti	zkušenost	k1gFnPc4	zkušenost
s	s	k7c7	s
tankovým	tankový	k2eAgInSc7d1	tankový
bojem	boj	k1gInSc7	boj
a	a	k8xC	a
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
jeho	jeho	k3xOp3gFnPc4	jeho
mimořádné	mimořádný	k2eAgFnPc4d1	mimořádná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
způsobilost	způsobilost	k1gFnSc4	způsobilost
pro	pro	k7c4	pro
velení	velení	k1gNnSc4	velení
divize	divize	k1gFnSc2	divize
horské	horský	k2eAgFnSc2d1	horská
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
mu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1940	[number]	k4	1940
umožnil	umožnit	k5eAaPmAgInS	umožnit
převzít	převzít	k5eAaPmF	převzít
velení	velení	k1gNnSc4	velení
7	[number]	k4	7
<g/>
.	.	kIx.	.
obrněné	obrněný	k2eAgFnSc2d1	obrněná
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
vyzbrojené	vyzbrojený	k2eAgFnSc2d1	vyzbrojená
převážně	převážně	k6eAd1	převážně
československými	československý	k2eAgInPc7d1	československý
tanky	tank	k1gInPc7	tank
Pz	Pz	k1gFnSc2	Pz
38	[number]	k4	38
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
bezvýhradným	bezvýhradný	k2eAgMnSc7d1	bezvýhradný
ctitelem	ctitel	k1gMnSc7	ctitel
Hitlera	Hitler	k1gMnSc2	Hitler
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
kolegy	kolega	k1gMnPc4	kolega
důstojníky	důstojník	k1gMnPc4	důstojník
často	často	k6eAd1	často
zdravil	zdravit	k5eAaImAgInS	zdravit
nacistickým	nacistický	k2eAgInSc7d1	nacistický
pozdravem	pozdrav	k1gInSc7	pozdrav
Heil	Heil	k1gMnSc1	Heil
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
získal	získat	k5eAaPmAgMnS	získat
pověst	pověst	k1gFnSc4	pověst
"	"	kIx"	"
<g/>
nacistického	nacistický	k2eAgMnSc2d1	nacistický
generála	generál	k1gMnSc2	generál
<g/>
"	"	kIx"	"
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
neváhal	váhat	k5eNaImAgMnS	váhat
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
nacistu	nacista	k1gMnSc4	nacista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
tažení	tažení	k1gNnSc1	tažení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
7	[number]	k4	7
<g/>
.	.	kIx.	.
obrněné	obrněný	k2eAgFnSc2d1	obrněná
divize	divize	k1gFnSc2	divize
se	se	k3xPyFc4	se
Rommel	Rommel	k1gInSc1	Rommel
účastnil	účastnit	k5eAaImAgInS	účastnit
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
hrál	hrát	k5eAaImAgInS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
rozkazy	rozkaz	k1gInPc4	rozkaz
nadřízených	nadřízený	k1gMnPc2	nadřízený
brzdící	brzdící	k2eAgInSc4d1	brzdící
postup	postup	k1gInSc4	postup
a	a	k8xC	a
nabádající	nabádající	k2eAgInPc4d1	nabádající
k	k	k7c3	k
opatrnosti	opatrnost	k1gFnSc3	opatrnost
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
německého	německý	k2eAgInSc2d1	německý
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
daleko	daleko	k6eAd1	daleko
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
váhání	váhání	k1gNnSc2	váhání
pronikal	pronikat	k5eAaImAgMnS	pronikat
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
zajištěné	zajištěný	k2eAgNnSc4d1	zajištěné
krytí	krytí	k1gNnSc4	krytí
boků	bok	k1gInPc2	bok
nebo	nebo	k8xC	nebo
týlu	týl	k1gInSc2	týl
a	a	k8xC	a
spoléhal	spoléhat	k5eAaImAgMnS	spoléhat
se	se	k3xPyFc4	se
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
svého	svůj	k3xOyFgInSc2	svůj
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
velení	velení	k1gNnSc1	velení
často	často	k6eAd1	často
nemělo	mít	k5eNaImAgNnS	mít
tušení	tušení	k1gNnSc1	tušení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
divize	divize	k1gFnSc1	divize
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
kde	kde	k6eAd1	kde
udeří	udeřit	k5eAaPmIp3nP	udeřit
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnPc1	jeho
vlastní	vlastní	k2eAgMnPc1d1	vlastní
nadřízení	nadřízený	k1gMnPc1	nadřízený
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byli	být	k5eAaImAgMnP	být
někdy	někdy	k6eAd1	někdy
stejně	stejně	k6eAd1	stejně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jeho	jeho	k3xOp3gFnSc3	jeho
divizi	divize	k1gFnSc3	divize
vyneslo	vynést	k5eAaPmAgNnS	vynést
přezdívku	přezdívka	k1gFnSc4	přezdívka
divize	divize	k1gFnSc2	divize
duchů	duch	k1gMnPc2	duch
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
la	la	k1gNnSc1	la
division	division	k1gInSc1	division
fantôme	fantômat	k5eAaPmIp3nS	fantômat
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Gespenster-Divisionen	Gespenster-Divisionen	k1gInSc1	Gespenster-Divisionen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
hlavní	hlavní	k2eAgInPc4d1	hlavní
úspěchy	úspěch	k1gInPc4	úspěch
patří	patřit	k5eAaImIp3nS	patřit
rychlé	rychlý	k2eAgNnSc1d1	rychlé
překročení	překročení	k1gNnSc1	překročení
řeky	řeka	k1gFnSc2	řeka
Maasy	Maasa	k1gFnSc2	Maasa
<g/>
,	,	kIx,	,
bleskové	bleskový	k2eAgNnSc1d1	bleskové
prolomení	prolomení	k1gNnSc1	prolomení
prodloužení	prodloužení	k1gNnSc2	prodloužení
Maginotovy	Maginotův	k2eAgFnSc2d1	Maginotova
linie	linie	k1gFnSc2	linie
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
ovšem	ovšem	k9	ovšem
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
nedosahovalo	dosahovat	k5eNaImAgNnS	dosahovat
kvalit	kvalita	k1gFnPc2	kvalita
hlavní	hlavní	k2eAgFnSc2d1	hlavní
části	část	k1gFnSc2	část
linie	linie	k1gFnSc2	linie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odražení	odražení	k1gNnSc1	odražení
britského	britský	k2eAgInSc2d1	britský
protiútoku	protiútok	k1gInSc2	protiútok
u	u	k7c2	u
Arrasu	Arras	k1gInSc2	Arras
a	a	k8xC	a
dobytí	dobytí	k1gNnSc2	dobytí
Cherbourgu	Cherbourg	k1gInSc2	Cherbourg
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
výkony	výkon	k1gInPc4	výkon
během	během	k7c2	během
francouzského	francouzský	k2eAgNnSc2d1	francouzské
tažení	tažení	k1gNnSc2	tažení
obdržel	obdržet	k5eAaPmAgInS	obdržet
rytířský	rytířský	k2eAgInSc1d1	rytířský
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Požadavek	požadavek	k1gInSc1	požadavek
generála	generál	k1gMnSc2	generál
Hotha	Hoth	k1gMnSc2	Hoth
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
souzen	soudit	k5eAaImNgInS	soudit
pro	pro	k7c4	pro
neuposlechnutí	neuposlechnutí	k1gNnSc4	neuposlechnutí
rozkazů	rozkaz	k1gInPc2	rozkaz
<g/>
,	,	kIx,	,
smetl	smetnout	k5eAaPmAgInS	smetnout
ze	z	k7c2	z
stolu	stol	k1gInSc2	stol
generál	generál	k1gMnSc1	generál
Kluge	Kluge	k1gNnPc2	Kluge
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
Rommel	Rommel	k1gMnSc1	Rommel
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1941	[number]	k4	1941
generálporučíkem	generálporučík	k1gMnSc7	generálporučík
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
velením	velení	k1gNnSc7	velení
Sperrverband	Sperrverbando	k1gNnPc2	Sperrverbando
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Deutsches	Deutsches	k1gInSc4	Deutsches
Afrika	Afrika	k1gFnSc1	Afrika
Korps	Korps	k1gInSc1	Korps
(	(	kIx(	(
<g/>
DAK	DAK	kA	DAK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
podřízeného	podřízený	k2eAgNnSc2d1	podřízené
italským	italský	k2eAgFnPc3d1	italská
silám	síla	k1gFnPc3	síla
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zastavit	zastavit	k5eAaPmF	zastavit
ústup	ústup	k1gInSc4	ústup
italských	italský	k2eAgFnPc2d1	italská
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
držet	držet	k5eAaImF	držet
se	se	k3xPyFc4	se
aktivní	aktivní	k2eAgFnSc2d1	aktivní
defenzívní	defenzívní	k2eAgFnSc2d1	defenzívní
taktiky	taktika	k1gFnSc2	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
alespoň	alespoň	k9	alespoň
zněly	znět	k5eAaImAgInP	znět
rozkazy	rozkaz	k1gInPc1	rozkaz
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Hitler	Hitler	k1gMnSc1	Hitler
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
podstatně	podstatně	k6eAd1	podstatně
širší	široký	k2eAgInSc4d2	širší
mandát	mandát	k1gInSc4	mandát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
obdržel	obdržet	k5eAaPmAgInS	obdržet
dubové	dubový	k2eAgFnSc3d1	dubová
ratolesti	ratolest	k1gFnSc3	ratolest
k	k	k7c3	k
rytířskému	rytířský	k2eAgInSc3d1	rytířský
kříži	kříž	k1gInSc3	kříž
(	(	kIx(	(
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
možná	možná	k9	možná
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
zastavení	zastavení	k1gNnSc4	zastavení
britské	britský	k2eAgFnSc2d1	britská
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
,	,	kIx,	,
tu	ten	k3xDgFnSc4	ten
však	však	k9	však
zastavili	zastavit	k5eAaPmAgMnP	zastavit
Britové	Brit	k1gMnPc1	Brit
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
odeslat	odeslat	k5eAaPmF	odeslat
síly	síla	k1gFnPc4	síla
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Rommel	Rommel	k1gMnSc1	Rommel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
neměl	mít	k5eNaImAgMnS	mít
většinu	většina	k1gFnSc4	většina
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
nečekanou	čekaný	k2eNgFnSc4d1	nečekaná
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
<g/>
.	.	kIx.	.
</s>
<s>
Zaskočil	zaskočit	k5eAaPmAgMnS	zaskočit
jí	jíst	k5eAaImIp3nS	jíst
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
nadřízené	nadřízený	k1gMnPc4	nadřízený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
Brity	Brit	k1gMnPc4	Brit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
díky	díky	k7c3	díky
odposlechu	odposlech	k1gInSc3	odposlech
rádiové	rádiový	k2eAgFnSc2d1	rádiová
korespondence	korespondence	k1gFnSc2	korespondence
znali	znát	k5eAaImAgMnP	znát
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Rommel	Rommel	k1gInSc1	Rommel
dostal	dostat	k5eAaPmAgInS	dostat
<g/>
,	,	kIx,	,
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
bude	být	k5eAaImBp3nS	být
držet	držet	k5eAaImF	držet
<g/>
.	.	kIx.	.
</s>
<s>
Rommelův	Rommelův	k2eAgInSc1d1	Rommelův
úder	úder	k1gInSc1	úder
vedený	vedený	k2eAgInSc1d1	vedený
poměrně	poměrně	k6eAd1	poměrně
slabými	slabý	k2eAgFnPc7d1	slabá
silami	síla	k1gFnPc7	síla
se	se	k3xPyFc4	se
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
do	do	k7c2	do
netušených	tušený	k2eNgInPc2d1	netušený
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
smetl	smetnout	k5eAaPmAgMnS	smetnout
předsunuté	předsunutý	k2eAgFnSc2d1	předsunutá
britské	britský	k2eAgFnSc2d1	britská
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
až	až	k9	až
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Egypta	Egypt	k1gInSc2	Egypt
u	u	k7c2	u
průsmyku	průsmyk	k1gInSc2	průsmyk
Halfája	Halfáj	k1gInSc2	Halfáj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
však	však	k9	však
měl	mít	k5eAaImAgMnS	mít
jeden	jeden	k4xCgInSc4	jeden
vážný	vážný	k2eAgInSc4d1	vážný
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
čelní	čelní	k2eAgFnPc4d1	čelní
síly	síla	k1gFnPc4	síla
sice	sice	k8xC	sice
byly	být	k5eAaImAgFnP	být
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
však	však	k9	však
zůstal	zůstat	k5eAaPmAgInS	zůstat
Tobrúk	Tobrúk	k1gInSc1	Tobrúk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Britové	Brit	k1gMnPc1	Brit
urputně	urputně	k6eAd1	urputně
drželi	držet	k5eAaImAgMnP	držet
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gInSc1	Rommel
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
podnikl	podniknout	k5eAaPmAgMnS	podniknout
několik	několik	k4yIc4	několik
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
naplánovaných	naplánovaný	k2eAgInPc2d1	naplánovaný
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ho	on	k3xPp3gInSc4	on
stály	stát	k5eAaImAgInP	stát
mnoho	mnoho	k4c4	mnoho
mužů	muž	k1gMnPc2	muž
bez	bez	k7c2	bez
většího	veliký	k2eAgInSc2d2	veliký
efektu	efekt	k1gInSc2	efekt
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Obléhání	obléhání	k1gNnSc4	obléhání
Tobruku	Tobruk	k1gInSc2	Tobruk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
tím	ten	k3xDgNnSc7	ten
ohrozil	ohrozit	k5eAaPmAgMnS	ohrozit
svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
ale	ale	k9	ale
způsobil	způsobit	k5eAaPmAgMnS	způsobit
velké	velký	k2eAgFnPc4d1	velká
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnPc1	jeho
předsunuté	předsunutý	k2eAgFnPc1d1	předsunutá
jednotky	jednotka	k1gFnPc1	jednotka
trpěly	trpět	k5eAaImAgFnP	trpět
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
komplikovaného	komplikovaný	k2eAgNnSc2d1	komplikované
zásobování	zásobování	k1gNnSc2	zásobování
a	a	k8xC	a
Tobrúk	Tobrúka	k1gFnPc2	Tobrúka
v	v	k7c6	v
britských	britský	k2eAgFnPc6d1	britská
rukou	ruka	k1gFnPc6	ruka
tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc1	problém
velice	velice	k6eAd1	velice
zhoršoval	zhoršovat	k5eAaImAgInS	zhoršovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
trochu	trochu	k6eAd1	trochu
napravil	napravit	k5eAaPmAgMnS	napravit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
když	když	k8xS	když
odrazil	odrazit	k5eAaPmAgMnS	odrazit
dva	dva	k4xCgInPc4	dva
uspěchané	uspěchaný	k2eAgInPc4d1	uspěchaný
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
promyšlené	promyšlený	k2eAgInPc1d1	promyšlený
pokusy	pokus	k1gInPc1	pokus
Britů	Brit	k1gMnPc2	Brit
o	o	k7c4	o
osvobození	osvobození	k1gNnSc4	osvobození
Tobrúku	Tobrúk	k1gInSc2	Tobrúk
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc1	operace
Brevity	Brevita	k1gFnSc2	Brevita
a	a	k8xC	a
operace	operace	k1gFnSc2	operace
Battleaxe	Battleaxe	k1gFnSc2	Battleaxe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obratnost	obratnost	k1gFnSc4	obratnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yQgFnSc7	jaký
vedl	vést	k5eAaImAgMnS	vést
složitý	složitý	k2eAgInSc4d1	složitý
manévrovací	manévrovací	k2eAgInSc4d1	manévrovací
boj	boj	k1gInSc4	boj
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
přezdívky	přezdívka	k1gFnSc2	přezdívka
pouštní	pouštní	k2eAgFnSc1d1	pouštní
liška	liška	k1gFnSc1	liška
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
divize	divize	k1gFnSc2	divize
duchů	duch	k1gMnPc2	duch
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
přezdívku	přezdívka	k1gFnSc4	přezdívka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
používaly	používat	k5eAaImAgFnP	používat
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Úcta	úcta	k1gFnSc1	úcta
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nepřítele	nepřítel	k1gMnSc2	nepřítel
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
nejen	nejen	k6eAd1	nejen
Rommelovými	Rommelův	k2eAgFnPc7d1	Rommelova
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dodržoval	dodržovat	k5eAaImAgMnS	dodržovat
Ženevské	ženevský	k2eAgFnPc4d1	Ženevská
konvence	konvence	k1gFnPc4	konvence
a	a	k8xC	a
zacházel	zacházet	k5eAaImAgInS	zacházet
dobře	dobře	k6eAd1	dobře
se	s	k7c7	s
zajatci	zajatec	k1gMnPc7	zajatec
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
etnický	etnický	k2eAgInSc4d1	etnický
původ	původ	k1gInSc4	původ
(	(	kIx(	(
<g/>
týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
i	i	k9	i
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
porobených	porobený	k2eAgInPc2d1	porobený
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
několikrát	několikrát	k6eAd1	několikrát
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
instrukce	instrukce	k1gFnPc4	instrukce
o	o	k7c4	o
zacházení	zacházení	k1gNnSc4	zacházení
se	s	k7c7	s
zajatci	zajatec	k1gMnPc7	zajatec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
konvence	konvence	k1gFnPc1	konvence
porušovaly	porušovat	k5eAaImAgFnP	porušovat
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
rozkazy	rozkaz	k1gInPc4	rozkaz
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
dokonce	dokonce	k9	dokonce
před	před	k7c7	před
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
zatajil	zatajit	k5eAaPmAgMnS	zatajit
a	a	k8xC	a
spálil	spálit	k5eAaPmAgMnS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeho	jeho	k3xOp3gNnSc1	jeho
vedení	vedení	k1gNnSc1	vedení
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
však	však	k9	však
nevyhnulo	vyhnout	k5eNaPmAgNnS	vyhnout
některým	některý	k3yIgInPc3	některý
excesům	exces	k1gInPc3	exces
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	s	k7c7	s
slušným	slušný	k2eAgNnSc7d1	slušné
zacházením	zacházení	k1gNnSc7	zacházení
s	s	k7c7	s
důstojníky	důstojník	k1gMnPc7	důstojník
3	[number]	k4	3
<g/>
.	.	kIx.	.
indické	indický	k2eAgFnSc2d1	indická
mechanizované	mechanizovaný	k2eAgFnSc2d1	mechanizovaná
brigády	brigáda	k1gFnSc2	brigáda
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
o	o	k7c4	o
příděl	příděl	k1gInSc4	příděl
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
příslušníky	příslušník	k1gMnPc4	příslušník
Afrikakorpsu	Afrikakorpsa	k1gFnSc4	Afrikakorpsa
<g/>
,	,	kIx,	,
kontrastuje	kontrastovat	k5eAaImIp3nS	kontrastovat
vyhnání	vyhnání	k1gNnSc1	vyhnání
jejích	její	k3xOp3gMnPc2	její
indických	indický	k2eAgMnPc2d1	indický
příslušníků	příslušník	k1gMnPc2	příslušník
do	do	k7c2	do
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
opakovaně	opakovaně	k6eAd1	opakovaně
využíval	využívat	k5eAaImAgMnS	využívat
neschopnosti	neschopnost	k1gFnPc4	neschopnost
Britů	Brit	k1gMnPc2	Brit
zkoncentrovat	zkoncentrovat	k5eAaPmF	zkoncentrovat
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
zkoordinovat	zkoordinovat	k5eAaPmF	zkoordinovat
své	svůj	k3xOyFgInPc4	svůj
údery	úder	k1gInPc4	úder
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nedostatek	nedostatek	k1gInSc1	nedostatek
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
zásobovací	zásobovací	k2eAgFnPc1d1	zásobovací
linie	linie	k1gFnPc1	linie
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
slabší	slabý	k2eAgFnSc2d2	slabší
síly	síla	k1gFnSc2	síla
ho	on	k3xPp3gMnSc4	on
velice	velice	k6eAd1	velice
omezovaly	omezovat	k5eAaImAgFnP	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
musel	muset	k5eAaImAgMnS	muset
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1941	[number]	k4	1941
<g/>
/	/	kIx~	/
<g/>
1942	[number]	k4	1942
ustoupit	ustoupit	k5eAaPmF	ustoupit
před	před	k7c4	před
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
pečlivěji	pečlivě	k6eAd2	pečlivě
připravenou	připravený	k2eAgFnSc7d1	připravená
ofenzívou	ofenzíva	k1gFnSc7	ofenzíva
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
operace	operace	k1gFnPc4	operace
Crusader	Crusader	k1gInSc1	Crusader
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
Benghází	Bengháze	k1gFnPc2	Bengháze
a	a	k8xC	a
nutnost	nutnost	k1gFnSc4	nutnost
vzdát	vzdát	k5eAaPmF	vzdát
obležení	obležení	k1gNnSc4	obležení
Tobrúku	Tobrúk	k1gInSc2	Tobrúk
pro	pro	k7c4	pro
Rommela	Rommel	k1gMnSc2	Rommel
představovaly	představovat	k5eAaImAgFnP	představovat
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zastavil	zastavit	k5eAaPmAgInS	zastavit
britský	britský	k2eAgInSc1d1	britský
postup	postup	k1gInSc1	postup
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Agheily	Agheila	k1gFnSc2	Agheila
a	a	k8xC	a
naplno	naplno	k6eAd1	naplno
využil	využít	k5eAaPmAgMnS	využít
blízkosti	blízkost	k1gFnPc4	blízkost
skladů	sklad	k1gInPc2	sklad
i	i	k8xC	i
posil	posila	k1gFnPc2	posila
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
nových	nový	k2eAgInPc2d1	nový
tanků	tank	k1gInPc2	tank
<g/>
)	)	kIx)	)
a	a	k8xC	a
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britové	Brit	k1gMnPc1	Brit
byli	být	k5eAaImAgMnP	být
vyčerpáni	vyčerpat	k5eAaPmNgMnP	vyčerpat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
logistické	logistický	k2eAgFnSc2d1	logistická
trasy	trasa	k1gFnSc2	trasa
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
prodloužily	prodloužit	k5eAaPmAgInP	prodloužit
a	a	k8xC	a
část	část	k1gFnSc1	část
britských	britský	k2eAgFnPc2d1	britská
sil	síla	k1gFnPc2	síla
byla	být	k5eAaImAgFnS	být
odeslána	odeslat	k5eAaPmNgFnS	odeslat
na	na	k7c4	na
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
britská	britský	k2eAgFnSc1d1	britská
7	[number]	k4	7
<g/>
.	.	kIx.	.
obrněná	obrněný	k2eAgFnSc1d1	obrněná
brigáda	brigáda	k1gFnSc1	brigáda
(	(	kIx(	(
<g/>
odeslána	odeslat	k5eAaPmNgFnS	odeslat
do	do	k7c2	do
Barmy	Barma	k1gFnSc2	Barma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopustili	dopustit	k5eAaPmAgMnP	dopustit
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
závažné	závažný	k2eAgFnPc1d1	závažná
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
když	když	k8xS	když
příliš	příliš	k6eAd1	příliš
rozptýlili	rozptýlit	k5eAaPmAgMnP	rozptýlit
jednotky	jednotka	k1gFnSc2	jednotka
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
nově	nově	k6eAd1	nově
nasazené	nasazený	k2eAgInPc4d1	nasazený
1	[number]	k4	1
<g/>
.	.	kIx.	.
obrněné	obrněný	k2eAgFnSc2d1	obrněná
divize	divize	k1gFnSc2	divize
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
ploše	plocha	k1gFnSc6	plocha
Kyrenaiky	Kyrenaika	k1gFnSc2	Kyrenaika
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gInSc1	Rommel
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
jako	jako	k8xS	jako
velitel	velitel	k1gMnSc1	velitel
Tankové	tankový	k2eAgFnSc2d1	tanková
armády	armáda	k1gFnSc2	armáda
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
této	tento	k3xDgFnSc2	tento
příznivé	příznivý	k2eAgFnSc2d1	příznivá
situace	situace	k1gFnSc2	situace
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
stažení	stažení	k1gNnSc6	stažení
části	část	k1gFnSc2	část
britských	britský	k2eAgFnPc2d1	britská
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
své	svůj	k3xOyFgFnSc2	svůj
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
radiové	radiový	k2eAgFnSc2d1	radiová
rozvědky	rozvědka	k1gFnSc2	rozvědka
<g/>
,	,	kIx,	,
odposlouchávající	odposlouchávající	k2eAgInSc4d1	odposlouchávající
radiový	radiový	k2eAgInSc4d1	radiový
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
radou	rada	k1gMnSc7	rada
náčelníka	náčelník	k1gMnSc2	náčelník
svého	své	k1gNnSc2	své
štábu	štáb	k1gInSc2	štáb
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Zahnal	zahnat	k5eAaPmAgMnS	zahnat
rozptýlené	rozptýlený	k2eAgMnPc4d1	rozptýlený
Brity	Brit	k1gMnPc4	Brit
<g/>
,	,	kIx,	,
dobyl	dobýt	k5eAaPmAgInS	dobýt
zpět	zpět	k6eAd1	zpět
Benghází	Bengháze	k1gFnPc2	Bengháze
a	a	k8xC	a
stanul	stanout	k5eAaPmAgMnS	stanout
před	před	k7c7	před
Gazalskou	Gazalský	k2eAgFnSc7d1	Gazalský
linií	linie	k1gFnSc7	linie
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zde	zde	k6eAd1	zde
zastavili	zastavit	k5eAaPmAgMnP	zastavit
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
napadl	napadnout	k5eAaPmAgInS	napadnout
jejich	jejich	k3xOp3gInSc1	jejich
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
silnější	silný	k2eAgFnPc1d2	silnější
síly	síla	k1gFnPc1	síla
soustředěné	soustředěný	k2eAgFnPc1d1	soustředěná
okolo	okolo	k7c2	okolo
Gazaly	Gazala	k1gFnSc2	Gazala
k	k	k7c3	k
připravované	připravovaný	k2eAgFnSc3d1	připravovaná
ofensivě	ofensiva	k1gFnSc3	ofensiva
gen.	gen.	kA	gen.
Claude	Claud	k1gInSc5	Claud
Auchinlecka	Auchinlecko	k1gNnPc4	Auchinlecko
a	a	k8xC	a
rozdrtil	rozdrtit	k5eAaPmAgInS	rozdrtit
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Gazaly	Gazala	k1gFnSc2	Gazala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dobyl	dobýt	k5eAaPmAgInS	dobýt
jediným	jediný	k2eAgInSc7d1	jediný
úderem	úder	k1gInSc7	úder
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
nepřipravený	připravený	k2eNgInSc4d1	nepřipravený
Tobrúk	Tobrúk	k1gInSc4	Tobrúk
(	(	kIx(	(
<g/>
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
obratem	obratem	k6eAd1	obratem
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
polního	polní	k2eAgMnSc4d1	polní
maršála	maršál	k1gMnSc4	maršál
<g/>
)	)	kIx)	)
a	a	k8xC	a
postoupil	postoupit	k5eAaPmAgInS	postoupit
až	až	k9	až
k	k	k7c3	k
El	Ela	k1gFnPc2	Ela
Alamejnu	Alamejn	k1gInSc2	Alamejn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
během	během	k7c2	během
střetnutí	střetnutí	k1gNnSc2	střetnutí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vešlo	vejít	k5eAaPmAgNnS	vejít
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alamejnu	Alamejn	k1gInSc2	Alamejn
jeho	jeho	k3xOp3gFnSc4	jeho
unavenou	unavený	k2eAgFnSc4d1	unavená
armádu	armáda	k1gFnSc4	armáda
zastavila	zastavit	k5eAaPmAgFnS	zastavit
obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
následné	následný	k2eAgInPc1d1	následný
protiútoky	protiútok	k1gInPc1	protiútok
britských	britský	k2eAgFnPc2d1	britská
sil	síla	k1gFnPc2	síla
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Auchinlecka	Auchinlecka	k1gFnSc1	Auchinlecka
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
tak	tak	k9	tak
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
nejzazší	zadní	k2eAgFnPc4d3	nejzazší
hranice	hranice	k1gFnPc4	hranice
postupu	postup	k1gInSc2	postup
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
ovšem	ovšem	k9	ovšem
nebyla	být	k5eNaImAgFnS	být
nijak	nijak	k6eAd1	nijak
skvělá	skvělý	k2eAgFnSc1d1	skvělá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zásobování	zásobování	k1gNnSc1	zásobování
jeho	jeho	k3xOp3gFnPc2	jeho
jednotek	jednotka	k1gFnPc2	jednotka
selhávalo	selhávat	k5eAaImAgNnS	selhávat
díky	díky	k7c3	díky
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
logistickým	logistický	k2eAgFnPc3d1	logistická
trasám	trasa	k1gFnPc3	trasa
a	a	k8xC	a
činnosti	činnost	k1gFnSc3	činnost
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
částečně	částečně	k6eAd1	částečně
zaviněno	zavinit	k5eAaPmNgNnS	zavinit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
namísto	namísto	k7c2	namísto
zastavení	zastavení	k1gNnSc2	zastavení
postupu	postup	k1gInSc2	postup
na	na	k7c6	na
egyptských	egyptský	k2eAgFnPc6d1	egyptská
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
mezi	mezi	k7c7	mezi
Hitlerem	Hitler	k1gMnSc7	Hitler
a	a	k8xC	a
Mussolinim	Mussolini	k1gNnSc7	Mussolini
během	během	k7c2	během
dubnového	dubnový	k2eAgNnSc2d1	dubnové
setkání	setkání	k1gNnSc2	setkání
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgMnSc6	jenž
mělo	mít	k5eAaImAgNnS	mít
následovat	následovat	k5eAaImF	následovat
vylodění	vylodění	k1gNnSc1	vylodění
sil	síla	k1gFnPc2	síla
Osy	osa	k1gFnSc2	osa
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
britská	britský	k2eAgFnSc1d1	britská
posádka	posádka	k1gFnSc1	posádka
narušovala	narušovat	k5eAaImAgFnS	narušovat
přísun	přísun	k1gInSc4	přísun
zásob	zásoba	k1gFnPc2	zásoba
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
článek	článek	k1gInSc4	článek
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Maltu	Malta	k1gFnSc4	Malta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Rommel	Rommel	k1gMnSc1	Rommel
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Tobrúku	Tobrúk	k1gInSc2	Tobrúk
Hitlera	Hitler	k1gMnSc2	Hitler
aby	aby	kYmCp3nS	aby
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
postupem	postup	k1gInSc7	postup
jeho	jeho	k3xOp3gNnPc2	jeho
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nutnost	nutnost	k1gFnSc1	nutnost
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
na	na	k7c6	na
posilování	posilování	k1gNnSc6	posilování
a	a	k8xC	a
zásobování	zásobování	k1gNnSc6	zásobování
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
bojů	boj	k1gInPc2	boj
pak	pak	k6eAd1	pak
Ose	osa	k1gFnSc6	osa
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
operaci	operace	k1gFnSc4	operace
Herkules	Herkules	k1gMnSc1	Herkules
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
září	zářit	k5eAaImIp3nS	zářit
se	se	k3xPyFc4	se
Rommel	Rommel	k1gMnSc1	Rommel
opět	opět	k6eAd1	opět
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
prolomení	prolomení	k1gNnSc4	prolomení
britských	britský	k2eAgFnPc2d1	britská
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
útokem	útok	k1gInSc7	útok
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
zásob	zásoba	k1gFnPc2	zásoba
benzínu	benzín	k1gInSc2	benzín
a	a	k8xC	a
Rommel	Rommela	k1gFnPc2	Rommela
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
štábní	štábní	k2eAgMnPc1d1	štábní
důstojníci	důstojník	k1gMnPc1	důstojník
nevěřili	věřit	k5eNaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
Luftwaffe	Luftwaff	k1gInSc5	Luftwaff
a	a	k8xC	a
Italské	italský	k2eAgNnSc4d1	italské
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
dokážou	dokázat	k5eAaPmIp3nP	dokázat
dodat	dodat	k5eAaPmF	dodat
v	v	k7c6	v
dostatečném	dostatečný	k2eAgNnSc6d1	dostatečné
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Kesselring	Kesselring	k1gInSc4	Kesselring
jej	on	k3xPp3gInSc4	on
ubezpečil	ubezpečit	k5eAaPmAgInS	ubezpečit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takovéto	takovýto	k3xDgFnPc1	takovýto
obavy	obava	k1gFnPc1	obava
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
a	a	k8xC	a
že	že	k8xS	že
zásoby	zásoba	k1gFnPc4	zásoba
včas	včas	k6eAd1	včas
dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Rommelova	Rommelův	k2eAgFnSc1d1	Rommelova
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
naplánovaná	naplánovaný	k2eAgFnSc1d1	naplánovaná
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
ukořistěné	ukořistěný	k2eAgFnSc2d1	ukořistěná
britské	britský	k2eAgFnSc2d1	britská
mapy	mapa	k1gFnSc2	mapa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zpravodajským	zpravodajský	k2eAgNnSc7d1	zpravodajské
oddělením	oddělení	k1gNnSc7	oddělení
štábu	štáb	k1gInSc2	štáb
8	[number]	k4	8
<g/>
.	.	kIx.	.
armády	armáda	k1gFnPc4	armáda
úmyslně	úmyslně	k6eAd1	úmyslně
zanechána	zanechán	k2eAgFnSc1d1	zanechána
v	v	k7c6	v
opuštěném	opuštěný	k2eAgInSc6d1	opuštěný
obrněném	obrněný	k2eAgInSc6d1	obrněný
voze	vůz	k1gInSc6	vůz
<g/>
,	,	kIx,	,
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
hřebene	hřeben	k1gInSc2	hřeben
Alam	Alam	k1gMnSc1	Alam
Halfa	halfa	k1gFnSc1	halfa
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
zakreslena	zakreslen	k2eAgFnSc1d1	zakreslena
jako	jako	k8xS	jako
dobře	dobře	k6eAd1	dobře
průjezdná	průjezdný	k2eAgFnSc1d1	průjezdná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
už	už	k6eAd1	už
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
nerozvíjela	rozvíjet	k5eNaImAgFnS	rozvíjet
nijak	nijak	k6eAd1	nijak
dobře	dobře	k6eAd1	dobře
(	(	kIx(	(
<g/>
Montgomery	Montgomer	k1gInPc1	Montgomer
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
zpravodajců	zpravodajce	k1gMnPc2	zpravodajce
kompletní	kompletní	k2eAgInPc1d1	kompletní
plány	plán	k1gInPc1	plán
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgFnSc1d1	definitivní
katastrofa	katastrofa	k1gFnSc1	katastrofa
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
palivo	palivo	k1gNnSc4	palivo
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dost	dost	k6eAd1	dost
dalšího	další	k2eAgNnSc2d1	další
nedostane	dostat	k5eNaPmIp3nS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
proto	proto	k8xC	proto
buďto	buďto	k8xC	buďto
útok	útok	k1gInSc4	útok
ukončit	ukončit	k5eAaPmF	ukončit
a	a	k8xC	a
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
změnit	změnit	k5eAaPmF	změnit
směr	směr	k1gInSc4	směr
úderu	úder	k1gInSc2	úder
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
trasu	trasa	k1gFnSc4	trasa
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
náčelník	náčelník	k1gInSc1	náčelník
štábu	štáb	k1gInSc2	štáb
současně	současně	k6eAd1	současně
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Těžké	těžký	k2eAgInPc1d1	těžký
kobercové	kobercový	k2eAgInPc1d1	kobercový
nálety	nálet	k1gInPc1	nálet
RAF	raf	k0	raf
zpomalily	zpomalit	k5eAaPmAgFnP	zpomalit
náš	náš	k3xOp1gInSc4	náš
postup	postup	k1gInSc4	postup
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
současně	současně	k6eAd1	současně
odhalili	odhalit	k5eAaPmAgMnP	odhalit
disposice	disposice	k1gFnPc4	disposice
našich	náš	k3xOp1gFnPc2	náš
jednotek	jednotka	k1gFnPc2	jednotka
soustředěným	soustředěný	k2eAgFnPc3d1	soustředěná
britským	britský	k2eAgFnPc3d1	britská
obrněným	obrněný	k2eAgFnPc3d1	obrněná
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
,	,	kIx,	,
připraveným	připravený	k2eAgFnPc3d1	připravená
k	k	k7c3	k
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
protiakci	protiakce	k1gFnSc3	protiakce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nás	my	k3xPp1nPc4	my
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
provést	provést	k5eAaPmF	provést
obrat	obrat	k1gInSc4	obrat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
jsme	být	k5eAaImIp1nP	být
plánovali	plánovat	k5eAaImAgMnP	plánovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Zvolil	zvolit	k5eAaPmAgMnS	zvolit
si	se	k3xPyFc3	se
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
a	a	k8xC	a
najel	najet	k5eAaPmAgMnS	najet
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
nejsilněji	silně	k6eAd3	silně
opevněných	opevněný	k2eAgFnPc2d1	opevněná
pozic	pozice	k1gFnPc2	pozice
Britů	Brit	k1gMnPc2	Brit
na	na	k7c4	na
Alam	Alam	k1gInSc4	Alam
Halfě	halfa	k1gFnSc3	halfa
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
skončil	skončit	k5eAaPmAgInS	skončit
katastrofou	katastrofa	k1gFnSc7	katastrofa
a	a	k8xC	a
Kesselring	Kesselring	k1gInSc1	Kesselring
s	s	k7c7	s
Rommelem	Rommel	k1gInSc7	Rommel
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
obvinili	obvinit	k5eAaPmAgMnP	obvinit
z	z	k7c2	z
viny	vina	k1gFnSc2	vina
na	na	k7c6	na
neúspěchu	neúspěch	k1gInSc6	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Kesselring	Kesselring	k1gInSc4	Kesselring
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rommel	Rommel	k1gMnSc1	Rommel
bitvu	bitva	k1gFnSc4	bitva
zbabral	zbabrat	k5eAaPmAgMnS	zbabrat
<g/>
,	,	kIx,	,
Rommel	Rommel	k1gMnSc1	Rommel
zase	zase	k9	zase
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kesselring	Kesselring	k1gInSc1	Kesselring
mu	on	k3xPp3gMnSc3	on
úmyslně	úmyslně	k6eAd1	úmyslně
neposkytl	poskytnout	k5eNaPmAgMnS	poskytnout
všechno	všechen	k3xTgNnSc4	všechen
palivo	palivo	k1gNnSc4	palivo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
mohl	moct	k5eAaImAgMnS	moct
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
mu	on	k3xPp3gMnSc3	on
sebral	sebrat	k5eAaPmAgInS	sebrat
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Kesselring	Kesselring	k1gInSc1	Kesselring
to	ten	k3xDgNnSc4	ten
zuřivě	zuřivě	k6eAd1	zuřivě
popíral	popírat	k5eAaImAgMnS	popírat
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mohl	moct	k5eAaImAgMnS	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
nebylo	být	k5eNaImAgNnS	být
dodáno	dodat	k5eAaPmNgNnS	dodat
všechno	všechen	k3xTgNnSc1	všechen
palivo	palivo	k1gNnSc1	palivo
<g/>
,	,	kIx,	,
co	co	k9	co
dodáno	dodán	k2eAgNnSc1d1	dodáno
být	být	k5eAaImF	být
mohlo	moct	k5eAaImAgNnS	moct
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
-	-	kIx~	-
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dověděl	dovědět	k5eAaPmAgMnS	dovědět
až	až	k9	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
že	že	k8xS	že
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
poválečné	poválečný	k2eAgNnSc4d1	poválečné
zkoumání	zkoumání	k1gNnSc4	zkoumání
záznamů	záznam	k1gInPc2	záznam
o	o	k7c6	o
přepravě	přeprava	k1gFnSc6	přeprava
paliva	palivo	k1gNnSc2	palivo
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
slibované	slibovaný	k2eAgNnSc1d1	slibované
množství	množství	k1gNnSc1	množství
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
dodáno	dodat	k5eAaPmNgNnS	dodat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Tanková	tankový	k2eAgFnSc1d1	tanková
armáda	armáda	k1gFnSc1	armáda
Afrika	Afrika	k1gFnSc1	Afrika
spotřebovala	spotřebovat	k5eAaPmAgFnS	spotřebovat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
než	než	k8xS	než
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
<g/>
,	,	kIx,	,
na	na	k7c4	na
přepravu	přeprava	k1gFnSc4	přeprava
zbytku	zbytek	k1gInSc2	zbytek
do	do	k7c2	do
předních	přední	k2eAgFnPc2d1	přední
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gInSc1	Rommel
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Alam	Alam	k1gInSc4	Alam
Halfu	halfa	k1gFnSc4	halfa
opustil	opustit	k5eAaPmAgMnS	opustit
Afriku	Afrika	k1gFnSc4	Afrika
kvůli	kvůli	k7c3	kvůli
léčení	léčení	k1gNnSc3	léčení
a	a	k8xC	a
také	také	k9	také
návštěvě	návštěva	k1gFnSc3	návštěva
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
převzetí	převzetí	k1gNnSc2	převzetí
maršálské	maršálský	k2eAgFnSc2d1	maršálská
hole	hole	k1gFnSc2	hole
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
velení	velení	k1gNnSc1	velení
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
převzal	převzít	k5eAaPmAgMnS	převzít
generál	generál	k1gMnSc1	generál
Georg	Georg	k1gMnSc1	Georg
Stumme	Stumme	k1gMnSc1	Stumme
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Rommelovy	Rommelův	k2eAgFnSc2d1	Rommelova
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
zahájila	zahájit	k5eAaPmAgNnP	zahájit
britská	britský	k2eAgNnPc1d1	Britské
vojska	vojsko	k1gNnPc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Montgomeryho	Montgomery	k1gMnSc2	Montgomery
útok	útok	k1gInSc1	útok
na	na	k7c4	na
německé	německý	k2eAgFnPc4d1	německá
pozice	pozice	k1gFnPc4	pozice
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alamejnu	Alamejn	k1gInSc2	Alamejn
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
druhá	druhý	k4xOgFnSc1	druhý
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alamejnu	Alamejn	k1gInSc2	Alamejn
byla	být	k5eAaImAgFnS	být
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
bitvou	bitva	k1gFnSc7	bitva
afrického	africký	k2eAgNnSc2d1	africké
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
bojů	boj	k1gInPc2	boj
během	během	k7c2	během
inspekce	inspekce	k1gFnSc2	inspekce
dostal	dostat	k5eAaPmAgMnS	dostat
infarkt	infarkt	k1gInSc4	infarkt
<g/>
,	,	kIx,	,
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
z	z	k7c2	z
auta	auto	k1gNnSc2	auto
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
se	se	k3xPyFc4	se
urychleně	urychleně	k6eAd1	urychleně
musel	muset	k5eAaImAgMnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
moc	moc	k6eAd1	moc
šancí	šance	k1gFnPc2	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Čelil	čelit	k5eAaImAgMnS	čelit
drtivé	drtivý	k2eAgFnSc3d1	drtivá
převaze	převaha	k1gFnSc3	převaha
ve	v	k7c6	v
zbraních	zbraň	k1gFnPc6	zbraň
i	i	k8xC	i
lidech	lid	k1gInPc6	lid
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedostatku	nedostatek	k1gInSc2	nedostatek
paliva	palivo	k1gNnSc2	palivo
téměř	téměř	k6eAd1	téměř
nemobilní	mobilní	k2eNgFnSc1d1	nemobilní
<g/>
.	.	kIx.	.
</s>
<s>
Zoufale	zoufale	k6eAd1	zoufale
zadržoval	zadržovat	k5eAaImAgMnS	zadržovat
nepřítele	nepřítel	k1gMnSc4	nepřítel
promyšlenými	promyšlený	k2eAgInPc7d1	promyšlený
dílčími	dílčí	k2eAgInPc7d1	dílčí
protiútoky	protiútok	k1gInPc7	protiútok
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
nevyhnutelný	vyhnutelný	k2eNgInSc4d1	nevyhnutelný
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
ústup	ústup	k1gInSc4	ústup
zahájil	zahájit	k5eAaPmAgMnS	zahájit
a	a	k8xC	a
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gMnSc3	on
nařídil	nařídit	k5eAaPmAgMnS	nařídit
držet	držet	k5eAaImF	držet
pozice	pozice	k1gFnPc4	pozice
za	za	k7c4	za
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommet	k5eAaImAgInS	Rommet
tedy	tedy	k9	tedy
ústup	ústup	k1gInSc1	ústup
nejprve	nejprve	k6eAd1	nejprve
zrušil	zrušit	k5eAaPmAgInS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
přesto	přesto	k8xC	přesto
provede	provést	k5eAaPmIp3nS	provést
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
setrvat	setrvat	k5eAaPmF	setrvat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
znamená	znamenat	k5eAaImIp3nS	znamenat
zničení	zničení	k1gNnSc1	zničení
jeho	jeho	k3xOp3gFnSc2	jeho
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
poté	poté	k6eAd1	poté
vedl	vést	k5eAaImAgMnS	vést
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Afrika	Afrika	k1gFnSc1	Afrika
je	být	k5eAaImIp3nS	být
ztracena	ztracen	k2eAgFnSc1d1	ztracena
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
jej	on	k3xPp3gMnSc4	on
utvrdilo	utvrdit	k5eAaPmAgNnS	utvrdit
vylodění	vylodění	k1gNnSc1	vylodění
Amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Tunisu	Tunis	k1gInSc2	Tunis
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaně	opakovaně	k6eAd1	opakovaně
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
rozkazy	rozkaz	k1gInPc4	rozkaz
nadřízených	nadřízený	k1gMnPc2	nadřízený
ohledně	ohledně	k7c2	ohledně
držení	držení	k1gNnSc2	držení
různých	různý	k2eAgFnPc2d1	různá
přirozených	přirozený	k2eAgFnPc2d1	přirozená
obraných	obraný	k2eAgFnPc2d1	obraná
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
obranu	obrana	k1gFnSc4	obrana
neměl	mít	k5eNaImAgMnS	mít
dle	dle	k7c2	dle
svého	svůj	k3xOyFgNnSc2	svůj
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
dost	dost	k6eAd1	dost
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
němečtí	německý	k2eAgMnPc1d1	německý
velitelé	velitel	k1gMnPc1	velitel
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vycházející	vycházející	k2eAgFnSc1d1	vycházející
autoři	autor	k1gMnPc1	autor
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgMnSc7	ten
obvinili	obvinit	k5eAaPmAgMnP	obvinit
z	z	k7c2	z
poraženectví	poraženectví	k1gNnPc2	poraženectví
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
Kesselring	Kesselring	k1gInSc1	Kesselring
či	či	k8xC	či
David	David	k1gMnSc1	David
Irving	Irving	k1gInSc1	Irving
<g/>
)	)	kIx)	)
ovšem	ovšem	k9	ovšem
jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Young	Younga	k1gFnPc2	Younga
i	i	k8xC	i
Remy	remy	k1gNnPc2	remy
<g/>
)	)	kIx)	)
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
naprosto	naprosto	k6eAd1	naprosto
přesná	přesný	k2eAgFnSc1d1	přesná
strategická	strategický	k2eAgFnSc1d1	strategická
diagnóza	diagnóza	k1gFnSc1	diagnóza
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
označují	označovat	k5eAaImIp3nP	označovat
Rommelův	Rommelův	k2eAgInSc4d1	Rommelův
ústup	ústup	k1gInSc4	ústup
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejbrilantnějších	brilantní	k2eAgFnPc2d3	nejbrilantnější
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
se	se	k3xPyFc4	se
Rommelovi	Rommelův	k2eAgMnPc1d1	Rommelův
povedla	povést	k5eAaPmAgFnS	povést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gInSc1	Rommel
ještě	ještě	k9	ještě
svedl	svést	k5eAaPmAgInS	svést
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
jednu	jeden	k4xCgFnSc4	jeden
větší	veliký	k2eAgFnSc4d2	veliký
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
bitvu	bitva	k1gFnSc4	bitva
v	v	k7c6	v
průsmyku	průsmyk	k1gInSc6	průsmyk
Kasseríne	Kasserín	k1gMnSc5	Kasserín
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
proti	proti	k7c3	proti
Američanům	Američan	k1gMnPc3	Američan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
labutí	labutí	k2eAgFnSc1d1	labutí
píseň	píseň	k1gFnSc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Zoufale	zoufale	k6eAd1	zoufale
prosil	prosít	k5eAaPmAgMnS	prosít
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
evakuoval	evakuovat	k5eAaBmAgMnS	evakuovat
jeho	jeho	k3xOp3gMnPc4	jeho
muže	muž	k1gMnPc4	muž
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc1	ten
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tuniské	tuniský	k2eAgNnSc1d1	tuniské
předmostí	předmostí	k1gNnSc1	předmostí
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
udržet	udržet	k5eAaPmF	udržet
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
posílal	posílat	k5eAaImAgInS	posílat
tam	tam	k6eAd1	tam
stále	stále	k6eAd1	stále
další	další	k2eAgMnPc4d1	další
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
za	za	k7c4	za
bezprostřední	bezprostřední	k2eAgFnPc4d1	bezprostřední
hrozby	hrozba	k1gFnPc4	hrozba
pádu	pád	k1gInSc2	pád
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
opustil	opustit	k5eAaPmAgMnS	opustit
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
Severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
kvůli	kvůli	k7c3	kvůli
léčení	léčení	k1gNnSc3	léčení
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
i	i	k9	i
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
pokusit	pokusit	k5eAaPmF	pokusit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
k	k	k7c3	k
evakuaci	evakuace	k1gFnSc3	evakuace
jeho	jeho	k3xOp3gFnSc2	jeho
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc7	který
převzal	převzít	k5eAaPmAgInS	převzít
Hans-Jürgen	Hans-Jürgen	k1gInSc1	Hans-Jürgen
von	von	k1gInSc1	von
Arnim	Arnim	k1gInSc1	Arnim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
tvrzení	tvrzení	k1gNnSc2	tvrzení
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Hitler	Hitler	k1gMnSc1	Hitler
evakuaci	evakuace	k1gFnSc4	evakuace
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebylo	být	k5eNaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
povoleno	povolit	k5eAaPmNgNnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
soukromý	soukromý	k2eAgInSc4d1	soukromý
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
žádosti	žádost	k1gFnSc6	žádost
úřední	úřední	k2eAgInSc4d1	úřední
záznamy	záznam	k1gInPc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podezřívavý	podezřívavý	k2eAgMnSc1d1	podezřívavý
Hitler	Hitler	k1gMnSc1	Hitler
nechával	nechávat	k5eAaImAgMnS	nechávat
stenografovat	stenografovat	k5eAaImF	stenografovat
všechny	všechen	k3xTgInPc4	všechen
rozhovory	rozhovor	k1gInPc4	rozhovor
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
vojevůdci	vojevůdce	k1gMnPc7	vojevůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Itálie	Itálie	k1gFnSc2	Itálie
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
zotavování	zotavování	k1gNnSc6	zotavování
byl	být	k5eAaImAgInS	být
Rommel	Rommel	k1gInSc4	Rommel
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
štábu	štáb	k1gInSc2	štáb
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
pověřen	pověřit	k5eAaPmNgMnS	pověřit
přípravou	příprava	k1gFnSc7	příprava
akcí	akce	k1gFnPc2	akce
proti	proti	k7c3	proti
Itálii	Itálie	k1gFnSc3	Itálie
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Itálie	Itálie	k1gFnSc1	Itálie
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Plánovalo	plánovat	k5eAaImAgNnS	plánovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
určen	určit	k5eAaPmNgInS	určit
za	za	k7c4	za
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
německých	německý	k2eAgFnPc2d1	německá
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Hitler	Hitler	k1gMnSc1	Hitler
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
jmenováním	jmenování	k1gNnSc7	jmenování
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
i	i	k9	i
díky	díky	k7c3	díky
odporu	odpor	k1gInSc3	odpor
Göringa	Göring	k1gMnSc2	Göring
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dovedl	dovést	k5eAaPmAgMnS	dovést
Kesselring	Kesselring	k1gInSc4	Kesselring
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
podřízen	podřídit	k5eAaPmNgInS	podřídit
Rommelovi	Rommel	k1gMnSc3	Rommel
<g/>
,	,	kIx,	,
nijak	nijak	k6eAd1	nijak
netěšila	těšit	k5eNaImAgFnS	těšit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
byl	být	k5eAaImAgInS	být
Rommel	Rommel	k1gInSc1	Rommel
pověřen	pověřit	k5eAaPmNgInS	pověřit
funkcí	funkce	k1gFnSc7	funkce
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sotva	sotva	k6eAd1	sotva
tam	tam	k6eAd1	tam
dorazil	dorazit	k5eAaPmAgMnS	dorazit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
okamžitě	okamžitě	k6eAd1	okamžitě
povolán	povolat	k5eAaPmNgMnS	povolat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
byl	být	k5eAaImAgInS	být
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
nařídil	nařídit	k5eAaPmAgMnS	nařídit
připravit	připravit	k5eAaPmF	připravit
vše	všechen	k3xTgNnSc1	všechen
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
provedení	provedení	k1gNnSc3	provedení
operace	operace	k1gFnSc2	operace
Alarich	Alarich	k1gInSc1	Alarich
(	(	kIx(	(
<g/>
obsazení	obsazení	k1gNnSc1	obsazení
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jej	on	k3xPp3gMnSc4	on
Itálie	Itálie	k1gFnSc1	Itálie
ujistila	ujistit	k5eAaPmAgFnS	ujistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nevěřil	věřit	k5eNaImAgMnS	věřit
jí	jíst	k5eAaImIp3nS	jíst
oprávněně	oprávněně	k6eAd1	oprávněně
-	-	kIx~	-
Itálie	Itálie	k1gFnSc1	Itálie
již	již	k6eAd1	již
jednala	jednat	k5eAaImAgFnS	jednat
se	s	k7c7	s
Spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akce	akce	k1gFnSc1	akce
Alarich	Alaricha	k1gFnPc2	Alaricha
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
odposlechnut	odposlechnut	k2eAgInSc1d1	odposlechnut
rozhovor	rozhovor	k1gInSc1	rozhovor
mezi	mezi	k7c7	mezi
Churchillem	Churchill	k1gMnSc7	Churchill
a	a	k8xC	a
Rooseveltem	Roosevelt	k1gMnSc7	Roosevelt
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gInSc1	Rommel
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
lstí	lest	k1gFnPc2	lest
a	a	k8xC	a
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
války	válka	k1gFnSc2	válka
se	s	k7c7	s
Spojenci	spojenec	k1gMnPc7	spojenec
<g/>
)	)	kIx)	)
obsadil	obsadit	k5eAaPmAgMnS	obsadit
severní	severní	k2eAgFnSc4d1	severní
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
spojenecké	spojenecký	k2eAgNnSc4d1	spojenecké
vylodění	vylodění	k1gNnSc4	vylodění
a	a	k8xC	a
převzetí	převzetí	k1gNnSc4	převzetí
klíčových	klíčový	k2eAgFnPc2d1	klíčová
obranných	obranný	k2eAgFnPc2d1	obranná
pozic	pozice	k1gFnPc2	pozice
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Itálie	Itálie	k1gFnSc1	Itálie
odpadne	odpadnout	k5eAaPmIp3nS	odpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
Osa	osa	k1gFnSc1	osa
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
5	[number]	k4	5
dnů	den	k1gInPc2	den
po	po	k7c6	po
spojeneckém	spojenecký	k2eAgNnSc6d1	spojenecké
vylodění	vylodění	k1gNnSc6	vylodění
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
Kesselring	Kesselring	k1gInSc1	Kesselring
udržel	udržet	k5eAaPmAgInS	udržet
linii	linie	k1gFnSc4	linie
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
sjednocovat	sjednocovat	k5eAaImF	sjednocovat
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Rommela	Rommel	k1gMnSc2	Rommel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zatím	zatím	k6eAd1	zatím
zpacifikoval	zpacifikovat	k5eAaPmAgMnS	zpacifikovat
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
bývalého	bývalý	k2eAgMnSc4d1	bývalý
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zacházet	zacházet	k5eAaImF	zacházet
se	s	k7c7	s
zajatci	zajatec	k1gMnPc7	zajatec
a	a	k8xC	a
civilisty	civilista	k1gMnPc7	civilista
korektně	korektně	k6eAd1	korektně
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
však	však	k9	však
též	též	k9	též
sporný	sporný	k2eAgInSc1d1	sporný
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vybízí	vybízet	k5eAaImIp3nS	vybízet
vojáky	voják	k1gMnPc4	voják
k	k	k7c3	k
tvrdosti	tvrdost	k1gFnSc3	tvrdost
vůči	vůči	k7c3	vůči
Italům	Ital	k1gMnPc3	Ital
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
budou	být	k5eAaImBp3nP	být
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdo	kdo	k3yQnSc1	kdo
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
pozdvihne	pozdvihnout	k5eAaPmIp3nS	pozdvihnout
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
tím	ten	k3xDgMnSc7	ten
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
milost	milost	k1gFnSc4	milost
(	(	kIx(	(
<g/>
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádnou	žádný	k3yNgFnSc4	žádný
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
nezákonnému	zákonný	k2eNgNnSc3d1	nezákonné
jednání	jednání	k1gNnSc3	jednání
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
rozkaz	rozkaz	k1gInSc4	rozkaz
obdobného	obdobný	k2eAgInSc2d1	obdobný
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
maršál	maršál	k1gMnSc1	maršál
Kesselring	Kesselring	k1gInSc4	Kesselring
odsouzen	odsouzen	k2eAgInSc4d1	odsouzen
k	k	k7c3	k
patnácti	patnáct	k4xCc3	patnáct
letům	let	k1gInPc3	let
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gInSc1	Rommel
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
brzy	brzy	k6eAd1	brzy
nepohodlný	pohodlný	k2eNgMnSc1d1	nepohodlný
<g/>
:	:	kIx,	:
Jednak	jednak	k8xC	jednak
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
zřízení	zřízení	k1gNnSc4	zřízení
hlavních	hlavní	k2eAgFnPc2d1	hlavní
obranných	obranný	k2eAgFnPc2d1	obranná
pozic	pozice	k1gFnPc2	pozice
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Itálii	Itálie	k1gFnSc6	Itálie
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Spojenci	spojenec	k1gMnPc1	spojenec
mohou	moct	k5eAaImIp3nP	moct
vylodit	vylodit	k5eAaPmF	vylodit
severněji	severně	k6eAd2	severně
a	a	k8xC	a
odříznout	odříznout	k5eAaPmF	odříznout
tak	tak	k6eAd1	tak
bojující	bojující	k2eAgFnPc4d1	bojující
jednotky	jednotka	k1gFnPc4	jednotka
(	(	kIx(	(
<g/>
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
realizaci	realizace	k1gFnSc6	realizace
této	tento	k3xDgFnSc2	tento
vynikající	vynikající	k2eAgFnSc2d1	vynikající
myšlenky	myšlenka	k1gFnSc2	myšlenka
Spojenci	spojenec	k1gMnPc1	spojenec
ukázkovým	ukázkový	k2eAgInSc7d1	ukázkový
způsobem	způsob	k1gInSc7	způsob
pokazili	pokazit	k5eAaPmAgMnP	pokazit
při	při	k7c6	při
vylodění	vylodění	k1gNnSc6	vylodění
u	u	k7c2	u
Anzia	Anzium	k1gNnSc2	Anzium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
požadoval	požadovat	k5eAaImAgMnS	požadovat
prošetření	prošetření	k1gNnSc4	prošetření
zločinů	zločin	k1gInPc2	zločin
SS	SS	kA	SS
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
masové	masový	k2eAgFnSc2d1	masová
vraždy	vražda	k1gFnSc2	vražda
Židů	Žid	k1gMnPc2	Žid
u	u	k7c2	u
Lago	Laga	k1gMnSc5	Laga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rommel	Rommel	k1gInSc1	Rommel
se	s	k7c7	s
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
listopadu	listopad	k1gInSc2	listopad
pak	pak	k6eAd1	pak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
udělá	udělat	k5eAaPmIp3nS	udělat
inspektora	inspektor	k1gMnSc2	inspektor
opevňovacích	opevňovací	k2eAgFnPc2d1	opevňovací
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
dozor	dozor	k1gInSc1	dozor
nad	nad	k7c7	nad
stavbou	stavba	k1gFnSc7	stavba
Atlantického	atlantický	k2eAgInSc2d1	atlantický
valu	val	k1gInSc2	val
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Francie	Francie	k1gFnSc2	Francie
===	===	k?	===
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
byl	být	k5eAaImAgInS	být
Rommel	Rommel	k1gInSc1	Rommel
pověřen	pověřen	k2eAgInSc1d1	pověřen
úkolem	úkol	k1gInSc7	úkol
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
obranného	obranný	k2eAgInSc2d1	obranný
Atlantického	atlantický	k2eAgInSc2d1	atlantický
valu	val	k1gInSc2	val
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velitelem	velitel	k1gMnSc7	velitel
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nyní	nyní	k6eAd1	nyní
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
armádu	armáda	k1gFnSc4	armáda
čelící	čelící	k2eAgFnSc3d1	čelící
spojenecké	spojenecký	k2eAgFnSc3d1	spojenecká
invazi	invaze	k1gFnSc3	invaze
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
dohledem	dohled	k1gInSc7	dohled
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zvýšení	zvýšení	k1gNnSc3	zvýšení
intenzity	intenzita	k1gFnSc2	intenzita
budování	budování	k1gNnSc2	budování
opevnění	opevnění	k1gNnSc2	opevnění
a	a	k8xC	a
Rommel	Rommel	k1gMnSc1	Rommel
osobně	osobně	k6eAd1	osobně
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
zábran	zábrana	k1gFnPc2	zábrana
<g/>
,	,	kIx,	,
zátarasů	zátaras	k1gInPc2	zátaras
a	a	k8xC	a
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
rozmisťovány	rozmisťovat	k5eAaImNgFnP	rozmisťovat
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
však	však	k9	však
narážela	narážet	k5eAaImAgFnS	narážet
na	na	k7c4	na
početní	početní	k2eAgInSc4d1	početní
a	a	k8xC	a
kvalifikační	kvalifikační	k2eAgInSc4d1	kvalifikační
nedostatky	nedostatek	k1gInPc4	nedostatek
u	u	k7c2	u
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
val	val	k1gInSc4	val
budovat	budovat	k5eAaImF	budovat
a	a	k8xC	a
na	na	k7c4	na
nedostatek	nedostatek	k1gInSc4	nedostatek
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
val	val	k1gInSc1	val
při	při	k7c6	při
spojenecké	spojenecký	k2eAgFnSc6d1	spojenecká
invazi	invaze	k1gFnSc6	invaze
ještě	ještě	k9	ještě
daleko	daleko	k6eAd1	daleko
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
a	a	k8xC	a
neplnil	plnit	k5eNaImAgMnS	plnit
svou	svůj	k3xOyFgFnSc4	svůj
úlohu	úloha	k1gFnSc4	úloha
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
též	též	k9	též
prohloubily	prohloubit	k5eAaPmAgInP	prohloubit
spory	spor	k1gInPc1	spor
Rommela	Rommel	k1gMnSc2	Rommel
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
vysokými	vysoký	k2eAgMnPc7d1	vysoký
důstojníky	důstojník	k1gMnPc7	důstojník
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
maršálem	maršál	k1gMnSc7	maršál
Rundstedtem	Rundstedt	k1gInSc7	Rundstedt
si	se	k3xPyFc3	se
příliš	příliš	k6eAd1	příliš
do	do	k7c2	do
noty	nota	k1gFnSc2	nota
nesedl	sednout	k5eNaPmAgInS	sednout
-	-	kIx~	-
jejich	jejich	k3xOp3gInPc1	jejich
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
spíše	spíše	k9	spíše
prohlubovaly	prohlubovat	k5eAaImAgInP	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
Rundstedt	Rundstedt	k2eAgInSc1d1	Rundstedt
Rommelem	Rommel	k1gInSc7	Rommel
pohrdal	pohrdat	k5eAaImAgInS	pohrdat
a	a	k8xC	a
prohlašoval	prohlašovat	k5eAaImAgInS	prohlašovat
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
než	než	k8xS	než
velitel	velitel	k1gMnSc1	velitel
divize	divize	k1gFnSc1	divize
<g/>
,	,	kIx,	,
Rommel	Rommel	k1gInSc1	Rommel
byl	být	k5eAaImAgInS	být
zase	zase	k9	zase
Rundstedtem	Rundstedt	k1gInSc7	Rundstedt
znechucen	znechutit	k5eAaPmNgMnS	znechutit
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
zlomeného	zlomený	k2eAgMnSc4d1	zlomený
a	a	k8xC	a
vyčerpaného	vyčerpaný	k2eAgMnSc4d1	vyčerpaný
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Jak	jak	k8xC	jak
čelit	čelit	k5eAaImF	čelit
invazi	invaze	k1gFnSc3	invaze
<g/>
?	?	kIx.	?
</s>
<s>
====	====	k?	====
</s>
</p>
<p>
<s>
Spory	spor	k1gInPc1	spor
Rundstedta	Rundstedto	k1gNnSc2	Rundstedto
a	a	k8xC	a
Rommela	Rommela	k1gFnSc1	Rommela
vygradovaly	vygradovat	k5eAaPmAgFnP	vygradovat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oba	dva	k4xCgMnPc1	dva
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
diametrálně	diametrálně	k6eAd1	diametrálně
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
představami	představa	k1gFnPc7	představa
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
čelit	čelit	k5eAaImF	čelit
očekávané	očekávaný	k2eAgFnSc3d1	očekávaná
spojenecké	spojenecký	k2eAgFnSc3d1	spojenecká
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Rundstedt	Rundstedt	k1gMnSc1	Rundstedt
a	a	k8xC	a
Leo	Leo	k1gMnSc1	Leo
Geyr	Geyra	k1gFnPc2	Geyra
von	von	k1gInSc1	von
Schweppenburg	Schweppenburg	k1gMnSc1	Schweppenburg
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
klasické	klasický	k2eAgNnSc4d1	klasické
schéma	schéma	k1gNnSc4	schéma
<g/>
:	:	kIx,	:
koncentraci	koncentrace	k1gFnSc4	koncentrace
hlavních	hlavní	k2eAgFnPc2d1	hlavní
tankových	tankový	k2eAgFnPc2d1	tanková
sil	síla	k1gFnPc2	síla
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
po	po	k7c6	po
vylodění	vylodění	k1gNnSc6	vylodění
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
a	a	k8xC	a
smetly	smetnout	k5eAaPmAgInP	smetnout
spojenecká	spojenecký	k2eAgNnPc4d1	spojenecké
předmostí	předmostí	k1gNnSc4	předmostí
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
postup	postup	k1gInSc4	postup
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechtěli	chtít	k5eNaImAgMnP	chtít
vystavovat	vystavovat	k5eAaImF	vystavovat
své	svůj	k3xOyFgFnPc4	svůj
cenné	cenný	k2eAgFnPc4d1	cenná
jednotky	jednotka	k1gFnPc4	jednotka
palbě	palba	k1gFnSc3	palba
těžkých	těžký	k2eAgFnPc2d1	těžká
hladinových	hladinový	k2eAgFnPc2d1	hladinová
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
varoval	varovat	k5eAaImAgMnS	varovat
své	svůj	k3xOyFgMnPc4	svůj
oponenty	oponent	k1gMnPc4	oponent
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vylodění	vylodění	k1gNnSc3	vylodění
<g/>
,	,	kIx,	,
nepřátelské	přátelský	k2eNgNnSc1d1	nepřátelské
letectvo	letectvo	k1gNnSc1	letectvo
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
denní	denní	k2eAgInPc4d1	denní
přesuny	přesun	k1gInPc4	přesun
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
že	že	k8xS	že
jejich	jejich	k3xOp3gInPc1	jejich
pancéřové	pancéřový	k2eAgInPc1d1	pancéřový
svazy	svaz	k1gInPc1	svaz
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
projít	projít	k5eAaPmF	projít
jatkami	jatka	k1gFnPc7	jatka
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
k	k	k7c3	k
plážím	pláž	k1gFnPc3	pláž
dostanou	dostat	k5eAaPmIp3nP	dostat
(	(	kIx(	(
<g/>
dostanou	dostat	k5eAaPmIp3nP	dostat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
<g/>
)	)	kIx)	)
a	a	k8xC	a
trval	trvat	k5eAaImAgMnS	trvat
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
mobilních	mobilní	k2eAgFnPc2d1	mobilní
záloh	záloha	k1gFnPc2	záloha
co	co	k9	co
nejblíž	blízce	k6eAd3	blízce
invazním	invazní	k2eAgFnPc3d1	invazní
plážím	pláž	k1gFnPc3	pláž
pro	pro	k7c4	pro
odražení	odražení	k1gNnSc4	odražení
útoku	útok	k1gInSc2	útok
menšího	malý	k2eAgInSc2d2	menší
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
pouze	pouze	k6eAd1	pouze
odražení	odražení	k1gNnSc1	odražení
invaze	invaze	k1gFnSc2	invaze
malého	malý	k2eAgInSc2d1	malý
rozsahu	rozsah	k1gInSc2	rozsah
již	již	k6eAd1	již
při	při	k7c6	při
vyloďování	vyloďování	k1gNnSc6	vyloďování
dávalo	dávat	k5eAaImAgNnS	dávat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současně	současně	k6eAd1	současně
požadoval	požadovat	k5eAaImAgMnS	požadovat
umístění	umístění	k1gNnSc4	umístění
mobilních	mobilní	k2eAgFnPc2d1	mobilní
záloh	záloha	k1gFnPc2	záloha
o	o	k7c6	o
šesti	šest	k4xCc6	šest
až	až	k9	až
osmi	osm	k4xCc6	osm
tankových	tankový	k2eAgFnPc6d1	tanková
anebo	anebo	k8xC	anebo
mechanizovaných	mechanizovaný	k2eAgFnPc6d1	mechanizovaná
divizích	divize	k1gFnPc6	divize
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rozsáhlejšího	rozsáhlý	k2eAgNnSc2d2	rozsáhlejší
vylodění	vylodění	k1gNnSc2	vylodění
Spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
po	po	k7c6	po
ústupu	ústup	k1gInSc6	ústup
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
provést	provést	k5eAaPmF	provést
protiútok	protiútok	k1gInSc4	protiútok
proti	proti	k7c3	proti
vyloděným	vyloděný	k2eAgFnPc3d1	vyloděná
spojeneckým	spojenecký	k2eAgFnPc3d1	spojenecká
silám	síla	k1gFnPc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
zálohu	záloha	k1gFnSc4	záloha
mu	on	k3xPp3gMnSc3	on
OKW	OKW	kA	OKW
slíbilo	slíbit	k5eAaPmAgNnS	slíbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neposkytlo	poskytnout	k5eNaPmAgNnS	poskytnout
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
zastánci	zastánce	k1gMnPc1	zastánce
obou	dva	k4xCgFnPc2	dva
teorií	teorie	k1gFnPc2	teorie
se	se	k3xPyFc4	se
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
zuřivý	zuřivý	k2eAgInSc1d1	zuřivý
boj	boj	k1gInSc1	boj
a	a	k8xC	a
intrikaření	intrikaření	k1gNnSc1	intrikaření
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pro	pro	k7c4	pro
Třetí	třetí	k4xOgFnSc4	třetí
říši	říše	k1gFnSc4	říše
tragickým	tragický	k2eAgNnSc7d1	tragické
vyústěním	vyústění	k1gNnSc7	vyústění
bylo	být	k5eAaImAgNnS	být
Hitlerovo	Hitlerův	k2eAgNnSc1d1	Hitlerovo
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvolil	zvolit	k5eAaPmAgInS	zvolit
naprosto	naprosto	k6eAd1	naprosto
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
kompromis	kompromis	k1gInSc1	kompromis
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
přístupy	přístup	k1gInPc7	přístup
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
omezil	omezit	k5eAaPmAgMnS	omezit
pravomoce	pravomoc	k1gFnPc4	pravomoc
Rundstedta	Rundstedto	k1gNnSc2	Rundstedto
a	a	k8xC	a
Rommela	Rommel	k1gMnSc4	Rommel
ohledně	ohledně	k7c2	ohledně
disponování	disponování	k1gNnSc2	disponování
s	s	k7c7	s
klíčovými	klíčový	k2eAgFnPc7d1	klíčová
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vylodění	vylodění	k1gNnSc2	vylodění
====	====	k?	====
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1944	[number]	k4	1944
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
polními	polní	k2eAgMnPc7d1	polní
maršály	maršál	k1gMnPc7	maršál
prohlášení	prohlášení	k1gNnSc2	prohlášení
věrnosti	věrnost	k1gFnSc2	věrnost
vůdci	vůdce	k1gMnSc3	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vylodění	vylodění	k1gNnPc2	vylodění
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
Rommel	Rommel	k1gInSc1	Rommel
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemohl	moct	k5eNaImAgMnS	moct
přímo	přímo	k6eAd1	přímo
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
nedaleko	nedaleko	k7c2	nedaleko
fronty	fronta	k1gFnSc2	fronta
byl	být	k5eAaImAgInS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
jeho	jeho	k3xOp3gNnSc1	jeho
automobil	automobil	k1gInSc1	automobil
napaden	napaden	k2eAgInSc1d1	napaden
britskou	britský	k2eAgFnSc7d1	britská
stíhačkou	stíhačka	k1gFnSc7	stíhačka
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gInSc1	Rommel
utrpěl	utrpět	k5eAaPmAgInS	utrpět
četná	četný	k2eAgNnPc4d1	četné
těžká	těžký	k2eAgNnPc4d1	těžké
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
několikanásobnou	několikanásobný	k2eAgFnSc4d1	několikanásobná
frakturu	fraktura	k1gFnSc4	fraktura
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
poranění	poranění	k1gNnSc2	poranění
oka	oko	k1gNnSc2	oko
a	a	k8xC	a
otřes	otřes	k1gInSc1	otřes
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spojenecký	spojenecký	k2eAgMnSc1d1	spojenecký
pilot	pilot	k1gMnSc1	pilot
útok	útok	k1gInSc4	útok
provedl	provést	k5eAaPmAgMnS	provést
<g/>
,	,	kIx,	,
za	za	k7c4	za
nejpravděpodobnějšího	pravděpodobný	k2eAgMnSc4d3	nejpravděpodobnější
z	z	k7c2	z
celé	celá	k1gFnSc2	celá
řady	řada	k1gFnSc2	řada
kandidátů	kandidát	k1gMnPc2	kandidát
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Flight	Flight	k2eAgInSc1d1	Flight
Lieutenant	Lieutenant	k1gInSc1	Lieutenant
kanadského	kanadský	k2eAgNnSc2d1	kanadské
letectva	letectvo	k1gNnSc2	letectvo
Charles	Charles	k1gMnSc1	Charles
Fox	fox	k1gInSc1	fox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Hitlera	Hitler	k1gMnSc4	Hitler
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Rommelovy	Rommelův	k2eAgFnSc2d1	Rommelova
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
<g/>
,	,	kIx,	,
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
Adolfa	Adolf	k1gMnSc4	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
však	však	k8xC	však
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
náčelník	náčelník	k1gMnSc1	náčelník
štábu	štáb	k1gInSc2	štáb
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
generál	generál	k1gMnSc1	generál
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hans	Hans	k1gMnSc1	Hans
Speidel	Speidlo	k1gNnPc2	Speidlo
byl	být	k5eAaImAgMnS	být
účastníkem	účastník	k1gMnSc7	účastník
spiknutí	spiknutí	k1gNnSc2	spiknutí
proti	proti	k7c3	proti
Adolfu	Adolf	k1gMnSc3	Adolf
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
třetího	třetí	k4xOgInSc2	třetí
výslechu	výslech	k1gInSc2	výslech
jiného	jiný	k2eAgMnSc2d1	jiný
účastníka	účastník	k1gMnSc2	účastník
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
plk.	plk.	kA	plk.
Hofackera	Hofackera	k1gFnSc1	Hofackera
<g/>
,	,	kIx,	,
na	na	k7c6	na
gestapu	gestapo	k1gNnSc6	gestapo
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
července	červenec	k1gInSc2	červenec
spolu	spolu	k6eAd1	spolu
jednou	jednou	k6eAd1	jednou
mluvili	mluvit	k5eAaImAgMnP	mluvit
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
politického	politický	k2eAgNnSc2d1	politické
řešení	řešení	k1gNnSc2	řešení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
podporován	podporován	k2eAgMnSc1d1	podporován
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
obavách	obava	k1gFnPc6	obava
tajemníkem	tajemník	k1gMnSc7	tajemník
NSDAP	NSDAP	kA	NSDAP
Martinem	Martin	k1gMnSc7	Martin
Bormannem	Bormann	k1gMnSc7	Bormann
<g/>
,	,	kIx,	,
pojal	pojmout	k5eAaPmAgInS	pojmout
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
Rommel	Rommel	k1gMnSc1	Rommel
byl	být	k5eAaImAgMnS	být
účastníkem	účastník	k1gMnSc7	účastník
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
Maximum	maximum	k1gNnSc1	maximum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
obecně	obecně	k6eAd1	obecně
vedeném	vedený	k2eAgInSc6d1	vedený
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
plk.	plk.	kA	plk.
Hofackerem	Hofackero	k1gNnSc7	Hofackero
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
Hofacker	Hofacker	k1gMnSc1	Hofacker
nejspíše	nejspíše	k9	nejspíše
nezmínil	zmínit	k5eNaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc4	jaký
politické	politický	k2eAgNnSc4d1	politické
řešení	řešení	k1gNnSc4	řešení
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
,	,	kIx,	,
neinformoval	informovat	k5eNaBmAgMnS	informovat
své	svůj	k3xOyFgMnPc4	svůj
nadřízené	nadřízený	k1gMnPc4	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepozoroval	pozorovat	k5eNaImAgMnS	pozorovat
a	a	k8xC	a
neoznámil	oznámit	k5eNaPmAgMnS	oznámit
zapojení	zapojení	k1gNnSc4	zapojení
generála	generál	k1gMnSc2	generál
H.	H.	kA	H.
Speidela	Speidel	k1gMnSc2	Speidel
do	do	k7c2	do
odboje	odboj	k1gInSc2	odboj
mu	on	k3xPp3gMnSc3	on
značně	značně	k6eAd1	značně
přitížila	přitížit	k5eAaPmAgFnS	přitížit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
H.	H.	kA	H.
Speidela	Speidela	k1gFnSc1	Speidela
zachránit	zachránit	k5eAaPmF	zachránit
před	před	k7c7	před
trestním	trestní	k2eAgNnSc7d1	trestní
stíháním	stíhání	k1gNnSc7	stíhání
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
osobní	osobní	k2eAgNnSc4d1	osobní
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
spiknutí	spiknutí	k1gNnSc2	spiknutí
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
namísto	namísto	k7c2	namísto
toho	ten	k3xDgMnSc4	ten
poslal	poslat	k5eAaPmAgMnS	poslat
své	svůj	k3xOyFgMnPc4	svůj
pobočníky	pobočník	k1gMnPc4	pobočník
<g/>
,	,	kIx,	,
generálporučíka	generálporučík	k1gMnSc4	generálporučík
Wilhelma	Wilhelm	k1gMnSc4	Wilhelm
Burgdorfa	Burgdorf	k1gMnSc4	Burgdorf
a	a	k8xC	a
generálmajora	generálmajor	k1gMnSc4	generálmajor
Ernsta	Ernst	k1gMnSc4	Ernst
Maisela	Maisel	k1gMnSc4	Maisel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
buď	buď	k8xC	buď
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
nebo	nebo	k8xC	nebo
spáchání	spáchání	k1gNnSc4	spáchání
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
procesu	proces	k1gInSc6	proces
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
negativním	negativní	k2eAgMnPc3d1	negativní
dopadům	dopad	k1gInPc3	dopad
na	na	k7c6	na
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
<g/>
-li	i	k?	-li
by	by	kYmCp3nS	by
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
,	,	kIx,	,
spáchal	spáchat	k5eAaPmAgInS	spáchat
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
sebevraždu	sebevražda	k1gFnSc4	sebevražda
kyanidovou	kyanidový	k2eAgFnSc7d1	kyanidová
kapslí	kapsle	k1gFnSc7	kapsle
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
označen	označen	k2eAgInSc4d1	označen
infarkt	infarkt	k1gInSc4	infarkt
způsobený	způsobený	k2eAgInSc4d1	způsobený
následky	následek	k1gInPc7	následek
zranění	zranění	k1gNnSc4	zranění
ze	z	k7c2	z
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
a	a	k8xC	a
Rommelovi	Rommel	k1gMnSc3	Rommel
byl	být	k5eAaImAgInS	být
vypraven	vypraven	k2eAgInSc1d1	vypraven
státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřbít	k5eAaPmNgInS	pohřbít
byl	být	k5eAaImAgInS	být
žehem	žeh	k1gInSc7	žeh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
případnému	případný	k2eAgNnSc3d1	případné
pozdějšímu	pozdní	k2eAgNnSc3d2	pozdější
obvinění	obvinění	k1gNnSc3	obvinění
z	z	k7c2	z
travičství	travičství	k1gNnSc2	travičství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rommel	Rommel	k1gMnSc1	Rommel
jako	jako	k8xS	jako
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
==	==	k?	==
</s>
</p>
<p>
<s>
Rommel	Rommel	k1gInSc1	Rommel
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejschopnějších	schopný	k2eAgMnPc2d3	nejschopnější
taktiků	taktik	k1gMnPc2	taktik
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
tankové	tankový	k2eAgMnPc4d1	tankový
velitele	velitel	k1gMnPc4	velitel
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
rozporuplnější	rozporuplný	k2eAgInSc1d2	rozporuplnější
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	on	k3xPp3gNnSc2	on
hodnocení	hodnocení	k1gNnSc2	hodnocení
jakožto	jakožto	k8xS	jakožto
stratéga	stratég	k1gMnSc4	stratég
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysoce	vysoce	k6eAd1	vysoce
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
zhodnocení	zhodnocení	k1gNnSc1	zhodnocení
jeho	jeho	k3xOp3gFnPc2	jeho
strategických	strategický	k2eAgFnPc2d1	strategická
schopností	schopnost	k1gFnPc2	schopnost
pochází	pocházet	k5eAaImIp3nS	pocházet
zejména	zejména	k9	zejména
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
řady	řada	k1gFnSc2	řada
vysokých	vysoká	k1gFnPc2	vysoká
německých	německý	k2eAgMnPc2d1	německý
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
často	často	k6eAd1	často
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgMnS	mít
být	být	k5eAaImF	být
ničím	nic	k3yNnSc7	nic
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
velitelem	velitel	k1gMnSc7	velitel
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
jeho	on	k3xPp3gInSc4	on
vzestup	vzestup	k1gInSc4	vzestup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gNnPc2	on
zapřičiněn	zapřičiněn	k2eAgInSc1d1	zapřičiněn
spíše	spíše	k9	spíše
propagandou	propaganda	k1gFnSc7	propaganda
a	a	k8xC	a
dobrými	dobrý	k2eAgInPc7d1	dobrý
vztahy	vztah	k1gInPc7	vztah
s	s	k7c7	s
Goebbelsem	Goebbelso	k1gNnSc7	Goebbelso
a	a	k8xC	a
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
než	než	k8xS	než
bojovými	bojový	k2eAgInPc7d1	bojový
výsledky	výsledek	k1gInPc7	výsledek
<g/>
,	,	kIx,	,
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vojenského	vojenský	k2eAgNnSc2d1	vojenské
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
nejvíce	hodně	k6eAd3	hodně
projevoval	projevovat	k5eAaImAgInS	projevovat
v	v	k7c6	v
neschopnosti	neschopnost	k1gFnSc6	neschopnost
uznat	uznat	k5eAaPmF	uznat
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
důležitost	důležitost	k1gFnSc4	důležitost
logistických	logistický	k2eAgNnPc2d1	logistické
omezení	omezení	k1gNnPc2	omezení
pro	pro	k7c4	pro
vedení	vedení	k1gNnSc4	vedení
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Těžce	těžce	k6eAd1	těžce
také	také	k9	také
nesli	nést	k5eAaImAgMnP	nést
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Rommel	Rommel	k1gInSc4	Rommel
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
vůdcově	vůdcův	k2eAgFnSc6d1	Vůdcova
přízni	přízeň	k1gFnSc6	přízeň
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
obcházel	obcházet	k5eAaImAgMnS	obcházet
a	a	k8xC	a
jednal	jednat	k5eAaImAgMnS	jednat
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
i	i	k8xC	i
nadřízenými	nadřízený	k2eAgMnPc7d1	nadřízený
byly	být	k5eAaImAgFnP	být
opakovaně	opakovaně	k6eAd1	opakovaně
kritizovány	kritizovat	k5eAaImNgInP	kritizovat
jeho	jeho	k3xOp3gInPc1	jeho
časté	častý	k2eAgInPc1d1	častý
pobyty	pobyt	k1gInPc1	pobyt
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
povzbuzující	povzbuzující	k2eAgFnSc4d1	povzbuzující
morálku	morálka	k1gFnSc4	morálka
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezeným	omezený	k2eAgFnPc3d1	omezená
možnostem	možnost	k1gFnPc3	možnost
spojení	spojení	k1gNnSc2	spojení
často	často	k6eAd1	často
znemožňující	znemožňující	k2eAgFnSc3d1	znemožňující
efektivní	efektivní	k2eAgFnSc3d1	efektivní
komunikaci	komunikace	k1gFnSc3	komunikace
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gNnSc7	on
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
štábem	štáb	k1gInSc7	štáb
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
velení	velení	k1gNnSc1	velení
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
důležitých	důležitý	k2eAgNnPc2d1	důležité
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rommelových	Rommelův	k2eAgFnPc2d1	Rommelova
bitev	bitva	k1gFnPc2	bitva
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
dílem	dílo	k1gNnSc7	dílo
jeho	jeho	k3xOp3gMnPc2	jeho
podřízených	podřízený	k1gMnPc2	podřízený
jednajících	jednající	k2eAgMnPc2d1	jednající
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
.	.	kIx.	.
</s>
<s>
Kritizován	kritizovat	k5eAaImNgInS	kritizovat
byl	být	k5eAaImAgInS	být
též	též	k9	též
za	za	k7c4	za
neschopnost	neschopnost	k1gFnSc4	neschopnost
a	a	k8xC	a
neochotu	neochota	k1gFnSc4	neochota
kooperovat	kooperovat	k5eAaImF	kooperovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
jednotkami	jednotka	k1gFnPc7	jednotka
a	a	k8xC	a
veliteli	velitel	k1gMnPc7	velitel
a	a	k8xC	a
pro	pro	k7c4	pro
ignorování	ignorování	k1gNnSc4	ignorování
a	a	k8xC	a
obcházení	obcházení	k1gNnSc4	obcházení
rozkazů	rozkaz	k1gInPc2	rozkaz
nadřízených	nadřízený	k1gMnPc2	nadřízený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
však	však	k9	však
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
výraznou	výrazný	k2eAgFnSc4d1	výrazná
osobní	osobní	k2eAgFnSc4d1	osobní
zaujatost	zaujatost	k1gFnSc4	zaujatost
Rommelových	Rommelová	k1gFnPc2	Rommelová
největších	veliký	k2eAgMnPc2d3	veliký
kritiků	kritik	k1gMnPc2	kritik
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
vysokých	vysoký	k2eAgMnPc2d1	vysoký
německých	německý	k2eAgMnPc2d1	německý
velitelů	velitel	k1gMnPc2	velitel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byli	být	k5eAaImAgMnP	být
Halder	Halder	k1gInSc4	Halder
<g/>
,	,	kIx,	,
Kesselring	Kesselring	k1gInSc4	Kesselring
a	a	k8xC	a
Rundstedt	Rundstedt	k1gInSc4	Rundstedt
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dotyční	dotyčný	k2eAgMnPc1d1	dotyčný
též	též	k6eAd1	též
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kritice	kritika	k1gFnSc6	kritika
svrhávali	svrhávat	k5eAaImAgMnP	svrhávat
na	na	k7c4	na
Rommela	Rommel	k1gMnSc4	Rommel
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
chyby	chyba	k1gFnSc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Kesselring	Kesselring	k1gInSc1	Kesselring
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
Rommelové	Rommelový	k2eAgFnSc6d1	Rommelový
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Alam	Alam	k1gInSc4	Alam
Halfu	halfa	k1gFnSc4	halfa
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc2	sekce
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rundstedt	Rundstedt	k1gMnSc1	Rundstedt
se	se	k3xPyFc4	se
zase	zase	k9	zase
s	s	k7c7	s
Rommelem	Rommel	k1gMnSc7	Rommel
střetl	střetnout	k5eAaPmAgInS	střetnout
v	v	k7c6	v
zuřivém	zuřivý	k2eAgInSc6d1	zuřivý
sporu	spor	k1gInSc6	spor
ohledně	ohledně	k7c2	ohledně
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
strategii	strategie	k1gFnSc4	strategie
zvolit	zvolit	k5eAaPmF	zvolit
proti	proti	k7c3	proti
očekávané	očekávaný	k2eAgFnSc3d1	očekávaná
spojenecké	spojenecký	k2eAgFnSc3d1	spojenecká
invazi	invaze	k1gFnSc3	invaze
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
brigadier	brigadier	k1gMnSc1	brigadier
Desmond	Desmond	k1gMnSc1	Desmond
Young	Young	k1gMnSc1	Young
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
Rommelovi	Rommel	k1gMnSc6	Rommel
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dílčích	dílčí	k2eAgInPc6d1	dílčí
případech	případ	k1gInPc6	případ
vysoce	vysoce	k6eAd1	vysoce
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
Rommelův	Rommelův	k2eAgInSc4d1	Rommelův
strategický	strategický	k2eAgInSc4d1	strategický
úsudek	úsudek	k1gInSc4	úsudek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
kterak	kterak	k8xS	kterak
nejlépe	dobře	k6eAd3	dobře
čelit	čelit	k5eAaImF	čelit
spojenecké	spojenecký	k2eAgFnSc3d1	spojenecká
invazi	invaze	k1gFnSc3	invaze
a	a	k8xC	a
jaký	jaký	k3yRgInSc1	jaký
bude	být	k5eAaImBp3nS	být
dopad	dopad	k1gInSc1	dopad
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
převahy	převah	k1gInPc1	převah
spojenců	spojenec	k1gMnPc2	spojenec
na	na	k7c4	na
operace	operace	k1gFnPc4	operace
německých	německý	k2eAgFnPc2d1	německá
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
obraz	obraz	k1gInSc1	obraz
Rommelových	Rommelův	k2eAgFnPc2d1	Rommelova
strategických	strategický	k2eAgFnPc2d1	strategická
schopností	schopnost	k1gFnPc2	schopnost
nepodal	podat	k5eNaPmAgMnS	podat
ani	ani	k8xC	ani
spor	spor	k1gInSc4	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
čelit	čelit	k5eAaImF	čelit
vylodění	vylodění	k1gNnSc4	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gInSc1	Rommel
správně	správně	k6eAd1	správně
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
zničující	zničující	k2eAgInPc4d1	zničující
následky	následek	k1gInPc4	následek
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
převahy	převaha	k1gFnSc2	převaha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
předem	předem	k6eAd1	předem
rozvrátily	rozvrátit	k5eAaPmAgFnP	rozvrátit
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
koordinovaný	koordinovaný	k2eAgInSc4d1	koordinovaný
útok	útok	k1gInSc4	útok
větších	veliký	k2eAgInPc2d2	veliký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Nejinak	nejinak	k6eAd1	nejinak
by	by	kYmCp3nS	by
tomu	ten	k3xDgInSc3	ten
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
uskutečnění	uskutečnění	k1gNnSc6	uskutečnění
Rundstedtova	Rundstedtův	k2eAgInSc2d1	Rundstedtův
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Rommelův	Rommelův	k2eAgInSc1d1	Rommelův
plán	plán	k1gInSc1	plán
by	by	kYmCp3nS	by
však	však	k9	však
podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
nepřinesl	přinést	k5eNaPmAgMnS	přinést
<g/>
,	,	kIx,	,
vylodění	vylodění	k1gNnSc4	vylodění
by	by	kYmCp3nS	by
za	za	k7c2	za
stávajícího	stávající	k2eAgInSc2d1	stávající
poměru	poměr	k1gInSc2	poměr
sil	síla	k1gFnPc2	síla
odrazit	odrazit	k5eAaPmF	odrazit
nešlo	jít	k5eNaImAgNnS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Pravdu	pravda	k1gFnSc4	pravda
tak	tak	k6eAd1	tak
měly	mít	k5eAaImAgFnP	mít
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
když	když	k8xS	když
tvrdily	tvrdit	k5eAaImAgFnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
plán	plán	k1gInSc1	plán
nemůže	moct	k5eNaImIp3nS	moct
uspět	uspět	k5eAaPmF	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
historiky	historik	k1gMnPc7	historik
tak	tak	k6eAd1	tak
alespoň	alespoň	k9	alespoň
panuje	panovat	k5eAaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hitlerem	Hitler	k1gMnSc7	Hitler
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
kompromis	kompromis	k1gInSc4	kompromis
byl	být	k5eAaImAgMnS	být
tou	ten	k3xDgFnSc7	ten
nejhorší	zlý	k2eAgFnSc7d3	nejhorší
možnou	možný	k2eAgFnSc7d1	možná
variantou	varianta	k1gFnSc7	varianta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navíc	navíc	k6eAd1	navíc
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
omezení	omezení	k1gNnSc1	omezení
pravomocí	pravomoc	k1gFnPc2	pravomoc
obou	dva	k4xCgMnPc2	dva
velitelů	velitel	k1gMnPc2	velitel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Služební	služební	k2eAgFnSc2d1	služební
chronologie	chronologie	k1gFnSc2	chronologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
19	[number]	k4	19
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1910	[number]	k4	1910
<g/>
:	:	kIx,	:
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
fahnenjunker	fahnenjunker	k1gMnSc1	fahnenjunker
(	(	kIx(	(
<g/>
kadet	kadet	k1gMnSc1	kadet
<g/>
)	)	kIx)	)
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
württemberského	württemberský	k2eAgInSc2d1	württemberský
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
König	König	k1gMnSc1	König
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
I.	I.	kA	I.
č.	č.	k?	č.
124	[number]	k4	124
ve	v	k7c6	v
Weingartenu	Weingarten	k1gInSc6	Weingarten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
březen-listopad	březenistopad	k1gInSc1	březen-listopad
1911	[number]	k4	1911
<g/>
:	:	kIx,	:
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Gdaňsku	Gdaňsk	k1gInSc6	Gdaňsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
leden	leden	k1gInSc1	leden
1912	[number]	k4	1912
<g/>
:	:	kIx,	:
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
poručíka	poručík	k1gMnSc4	poručík
(	(	kIx(	(
<g/>
leutnant	leutnant	k1gMnSc1	leutnant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
rota	rota	k1gFnSc1	rota
124	[number]	k4	124
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
výcvik	výcvik	k1gInSc1	výcvik
odvedenců	odvedenec	k1gMnPc2	odvedenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1914	[number]	k4	1914
<g/>
:	:	kIx,	:
přidělen	přidělit	k5eAaPmNgMnS	přidělit
k	k	k7c3	k
49	[number]	k4	49
<g/>
.	.	kIx.	.
dělostřeleckému	dělostřelecký	k2eAgInSc3d1	dělostřelecký
pluku	pluk	k1gInSc3	pluk
v	v	k7c6	v
Ulmu	Ulmus	k1gInSc6	Ulmus
<g/>
,	,	kIx,	,
dělostřelecký	dělostřelecký	k2eAgInSc1d1	dělostřelecký
výcvik	výcvik	k1gInSc1	výcvik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
-	-	kIx~	-
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
124	[number]	k4	124
<g/>
.	.	kIx.	.
pěšímu	pěší	k1gMnSc3	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
čety	četa	k1gFnSc2	četa
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
červenec	červenec	k1gInSc1	červenec
1915	[number]	k4	1915
<g/>
:	:	kIx,	:
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
nadporučíka	nadporučík	k1gMnSc4	nadporučík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
říjen	říjen	k1gInSc1	říjen
1915	[number]	k4	1915
<g/>
:	:	kIx,	:
velitel	velitel	k1gMnSc1	velitel
pěší	pěší	k2eAgFnSc2d1	pěší
roty	rota	k1gFnSc2	rota
u	u	k7c2	u
Württemberského	Württemberský	k2eAgInSc2d1	Württemberský
horského	horský	k2eAgInSc2d1	horský
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
Alpenkorps	Alpenkorps	k1gInSc1	Alpenkorps
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1918	[number]	k4	1918
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
funkce	funkce	k1gFnSc2	funkce
ve	v	k7c6	v
štábu	štáb	k1gInSc6	štáb
na	na	k7c6	na
generálním	generální	k2eAgNnSc6d1	generální
velitelství	velitelství	k1gNnSc6	velitelství
č.	č.	k?	č.
LXIV	LXIV	kA	LXIV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
říjen	říjen	k1gInSc1	říjen
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
kapitána	kapitán	k1gMnSc4	kapitán
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
pěšímu	pěší	k2eAgInSc3d1	pěší
pluku	pluk	k1gInSc3	pluk
č.	č.	k?	č.
124	[number]	k4	124
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Meziválečné	meziválečný	k2eAgNnSc4d1	meziválečné
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
březen	březen	k1gInSc1	březen
1919	[number]	k4	1919
<g/>
:	:	kIx,	:
velí	velet	k5eAaImIp3nS	velet
zabezpečovací	zabezpečovací	k2eAgFnSc3d1	zabezpečovací
rotě	rota	k1gFnSc3	rota
32	[number]	k4	32
<g/>
,	,	kIx,	,
Friedrichshafen	Friedrichshafen	k1gInSc1	Friedrichshafen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1920	[number]	k4	1920
<g/>
:	:	kIx,	:
velitel	velitel	k1gMnSc1	velitel
střelecké	střelecký	k2eAgFnSc2d1	střelecká
roty	rota	k1gFnSc2	rota
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
č.	č.	k?	č.
13	[number]	k4	13
Reichswehru	Reichswehra	k1gFnSc4	Reichswehra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1929	[number]	k4	1929
<g/>
:	:	kIx,	:
Pěchotní	pěchotní	k2eAgFnSc1d1	pěchotní
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
instruktor	instruktor	k1gMnSc1	instruktor
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1933	[number]	k4	1933
<g/>
:	:	kIx,	:
přebírá	přebírat	k5eAaImIp3nS	přebírat
velení	velení	k1gNnSc4	velení
III	III	kA	III
<g/>
.	.	kIx.	.
praporu	prapor	k1gInSc2	prapor
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
č.	č.	k?	č.
17	[number]	k4	17
v	v	k7c6	v
Goslaru	Goslar	k1gInSc6	Goslar
<g/>
,	,	kIx,	,
major	major	k1gMnSc1	major
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1935	[number]	k4	1935
instruktor	instruktor	k1gMnSc1	instruktor
na	na	k7c6	na
pěchotní	pěchotní	k2eAgFnSc6d1	pěchotní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
,	,	kIx,	,
podplukovník	podplukovník	k1gMnSc1	podplukovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
v	v	k7c4	v
září	září	k1gNnSc4	září
1936	[number]	k4	1936
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
vojenský	vojenský	k2eAgInSc1d1	vojenský
doprovod	doprovod	k1gInSc1	doprovod
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
únor	únor	k1gInSc1	únor
1937	[number]	k4	1937
<g/>
:	:	kIx,	:
styčný	styčný	k2eAgMnSc1d1	styčný
důstojník	důstojník	k1gMnSc1	důstojník
mezi	mezi	k7c7	mezi
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
vedením	vedení	k1gNnSc7	vedení
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
koordinaci	koordinace	k1gFnSc4	koordinace
předvojenského	předvojenský	k2eAgInSc2d1	předvojenský
výcviku	výcvik	k1gInSc2	výcvik
příslušníků	příslušník	k1gMnPc2	příslušník
této	tento	k3xDgFnSc2	tento
nacistické	nacistický	k2eAgFnSc2d1	nacistická
organisace	organisace	k1gFnSc2	organisace
<g/>
,	,	kIx,	,
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
velitel	velitel	k1gMnSc1	velitel
Vůdcova	vůdcův	k2eAgInSc2d1	vůdcův
doprovodného	doprovodný	k2eAgInSc2d1	doprovodný
praporu	prapor	k1gInSc2	prapor
(	(	kIx(	(
<g/>
Führerbegleitbataillon	Führerbegleitbataillon	k1gInSc1	Führerbegleitbataillon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
velitel	velitel	k1gMnSc1	velitel
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
Kriegsschule	Kriegsschule	k1gFnSc1	Kriegsschule
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Vídeňském	vídeňský	k2eAgNnSc6d1	Vídeňské
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
dočasně	dočasně	k6eAd1	dočasně
velí	velet	k5eAaImIp3nS	velet
Vůdcovu	vůdcův	k2eAgInSc3d1	vůdcův
doprovodnému	doprovodný	k2eAgInSc3d1	doprovodný
praporu	prapor	k1gInSc3	prapor
při	při	k7c6	při
obsazování	obsazování	k1gNnSc6	obsazování
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Memelu	Memel	k1gInSc2	Memel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
září	září	k1gNnSc1	září
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
velí	velet	k5eAaImIp3nS	velet
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
doprovodu	doprovod	k1gInSc3	doprovod
Vůdcova	vůdcův	k2eAgInSc2d1	vůdcův
hlavního	hlavní	k2eAgInSc2d1	hlavní
stanu	stan	k1gInSc2	stan
<g/>
,	,	kIx,	,
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
generálmajora	generálmajor	k1gMnSc4	generálmajor
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1940	[number]	k4	1940
<g/>
:	:	kIx,	:
velení	velení	k1gNnSc2	velení
7	[number]	k4	7
<g/>
.	.	kIx.	.
obrněné	obrněný	k2eAgFnSc2d1	obrněná
divize	divize	k1gFnSc2	divize
</s>
</p>
<p>
<s>
leden	leden	k1gInSc1	leden
1941	[number]	k4	1941
<g/>
:	:	kIx,	:
generálporučík	generálporučík	k1gMnSc1	generálporučík
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
:	:	kIx,	:
pověřen	pověřen	k2eAgMnSc1d1	pověřen
velením	velení	k1gNnSc7	velení
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
Afrikakorpsu	Afrikakorps	k1gInSc6	Afrikakorps
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1941	[number]	k4	1941
<g/>
:	:	kIx,	:
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
generála	generál	k1gMnSc4	generál
tankových	tankový	k2eAgNnPc2d1	tankové
vojsk	vojsko	k1gNnPc2	vojsko
(	(	kIx(	(
<g/>
General	General	k1gFnSc1	General
der	drát	k5eAaImRp2nS	drát
panzertruppen	panzertruppen	k2eAgMnSc1d1	panzertruppen
<g/>
)	)	kIx)	)
a	a	k8xC	a
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
Tankové	tankový	k2eAgFnSc2d1	tanková
skupiny	skupina	k1gFnSc2	skupina
Afrika	Afrika	k1gFnSc1	Afrika
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
<g/>
:	:	kIx,	:
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
generálplukovníka	generálplukovník	k1gMnSc4	generálplukovník
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
:	:	kIx,	:
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
generála-polního	generálaolní	k2eAgMnSc4d1	generála-polní
maršála	maršál	k1gMnSc4	maršál
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1942	[number]	k4	1942
<g/>
:	:	kIx,	:
jmenován	jmenovat	k5eAaImNgInS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
Skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vystřídán	vystřídán	k2eAgMnSc1d1	vystřídán
generálem	generál	k1gMnSc7	generál
von	von	k1gInSc1	von
Arnimem	Arnim	k1gMnSc7	Arnim
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1943	[number]	k4	1943
<g/>
:	:	kIx,	:
pověřen	pověřit	k5eAaPmNgInS	pověřit
převzetím	převzetí	k1gNnSc7	převzetí
velitelství	velitelství	k1gNnPc2	velitelství
Skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
B	B	kA	B
<g/>
,	,	kIx,	,
s	s	k7c7	s
velitelstvím	velitelství	k1gNnSc7	velitelství
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1943	[number]	k4	1943
<g/>
:	:	kIx,	:
velitelství	velitelství	k1gNnSc6	velitelství
Skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
B	B	kA	B
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
</s>
</p>
<p>
<s>
listopad	listopad	k1gInSc1	listopad
1943	[number]	k4	1943
<g/>
:	:	kIx,	:
s	s	k7c7	s
velitelstvím	velitelství	k1gNnSc7	velitelství
Skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
B	B	kA	B
pověřen	pověřit	k5eAaPmNgMnS	pověřit
inspekcí	inspekce	k1gFnSc7	inspekce
příprav	příprava	k1gFnPc2	příprava
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
spojenecké	spojenecký	k2eAgFnSc3d1	spojenecká
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
<g/>
:	:	kIx,	:
velení	velení	k1gNnSc2	velení
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
B	B	kA	B
podřízena	podřízen	k2eAgFnSc1d1	podřízena
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Belgie	Belgie	k1gFnSc2	Belgie
(	(	kIx(	(
<g/>
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
zranění	zranění	k1gNnSc4	zranění
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
generál-polní	generálolní	k2eAgMnSc1d1	generál-polní
maršál	maršál	k1gMnSc1	maršál
Günther	Günthra	k1gFnPc2	Günthra
von	von	k1gInSc4	von
Kluge	Kluge	k1gFnSc4	Kluge
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
státní	státní	k2eAgInSc1d1	státní
pohřeb	pohřeb	k1gInSc1	pohřeb
v	v	k7c6	v
Ulmu	Ulmus	k1gInSc6	Ulmus
</s>
</p>
<p>
<s>
==	==	k?	==
Slavné	slavný	k2eAgInPc1d1	slavný
citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Führer	Führer	k1gMnSc1	Führer
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dopis	dopis	k1gInSc1	dopis
manželce	manželka	k1gFnSc3	manželka
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Úplně	úplně	k6eAd1	úplně
mi	já	k3xPp1nSc3	já
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
Vůdce	vůdce	k1gMnSc1	vůdce
spokojený	spokojený	k2eAgMnSc1d1	spokojený
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
k	k	k7c3	k
manželce	manželka	k1gFnSc3	manželka
<g/>
,	,	kIx,	,
září	zářit	k5eAaImIp3nS	zářit
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Půjdu	jít	k5eAaImIp1nS	jít
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
jak	jak	k8xS	jak
jen	jen	k6eAd1	jen
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
málo	málo	k1gNnSc1	málo
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
ovzduší	ovzduší	k1gNnSc6	ovzduší
úspěchu	úspěch	k1gInSc2	úspěch
vojáci	voják	k1gMnPc1	voják
nežádají	žádat	k5eNaImIp3nP	žádat
pití	pití	k1gNnSc4	pití
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
spánek	spánek	k1gInSc4	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Chtějí	chtít	k5eAaImIp3nP	chtít
pouze	pouze	k6eAd1	pouze
jít	jít	k5eAaImF	jít
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
dosáhnu	dosáhnout	k5eAaPmIp1nS	dosáhnout
El	Ela	k1gFnPc2	Ela
Alameinu	Alamein	k1gInSc2	Alamein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cíli	cíl	k1gInSc3	cíl
jsou	být	k5eAaImIp3nP	být
Alexandrie	Alexandrie	k1gFnPc1	Alexandrie
<g/>
,	,	kIx,	,
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc1	údolí
Nilu	Nil	k1gInSc2	Nil
<g/>
...	...	k?	...
Pokud	pokud	k8xS	pokud
bude	být	k5eAaImBp3nS	být
tanková	tankový	k2eAgFnSc1d1	tanková
armáda	armáda	k1gFnSc1	armáda
schopna	schopen	k2eAgFnSc1d1	schopna
projít	projít	k5eAaPmF	projít
úžinou	úžina	k1gFnSc7	úžina
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alameinu	Alamein	k1gInSc2	Alamein
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
budeme	být	k5eAaImBp1nP	být
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Počkám	počkat	k5eAaPmIp1nS	počkat
tam	tam	k6eAd1	tam
na	na	k7c6	na
vás	vy	k3xPp2nPc6	vy
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
budeme	být	k5eAaImBp1nP	být
moct	moct	k5eAaImF	moct
promluvit	promluvit	k5eAaPmF	promluvit
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
pohodlí	pohodlí	k1gNnSc6	pohodlí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Sidi	Sid	k1gFnPc1	Sid
Barrani	Barraň	k1gFnSc3	Barraň
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
Kesselringem	Kesselring	k1gInSc7	Kesselring
a	a	k8xC	a
Cavallerem	Cavaller	k1gInSc7	Cavaller
<g/>
,	,	kIx,	,
zaznamenaný	zaznamenaný	k2eAgMnSc1d1	zaznamenaný
Rommelovým	Rommelův	k2eAgMnSc7d1	Rommelův
pobočníkem	pobočník	k1gMnSc7	pobočník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Kdyby	kdyby	kYmCp3nS	kdyby
úspěch	úspěch	k1gInSc1	úspěch
závisel	záviset	k5eAaImAgInS	záviset
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k9	jako
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
časech	čas	k1gInPc6	čas
<g/>
,	,	kIx,	,
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
vůle	vůle	k1gFnSc2	vůle
mých	můj	k3xOp1gMnPc2	můj
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
překonali	překonat	k5eAaPmAgMnP	překonat
bychom	by	kYmCp1nP	by
Alamein	Alamein	k1gInSc1	Alamein
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
vyschly	vyschnout	k5eAaPmAgInP	vyschnout
zdroje	zdroj	k1gInPc1	zdroj
našich	náš	k3xOp1gFnPc2	náš
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
lenosti	lenost	k1gFnSc3	lenost
a	a	k8xC	a
zmatkářství	zmatkářství	k1gNnSc1	zmatkářství
zásobovacích	zásobovací	k2eAgMnPc2d1	zásobovací
důstojníků	důstojník	k1gMnPc2	důstojník
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
porážky	porážka	k1gFnSc2	porážka
v	v	k7c6	v
první	první	k4xOgFnSc6	první
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alameinu	Alamein	k1gInSc2	Alamein
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Věřte	věřit	k5eAaImRp2nP	věřit
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
Langu	Lang	k1gMnSc6	Lang
<g/>
,	,	kIx,	,
prvních	první	k4xOgNnPc6	první
dvacet	dvacet	k4xCc4	dvacet
čtyři	čtyři	k4xCgMnPc4	čtyři
hodin	hodina	k1gFnPc2	hodina
invaze	invaze	k1gFnPc1	invaze
je	být	k5eAaImIp3nS	být
rozhodujících	rozhodující	k2eAgNnPc2d1	rozhodující
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Spojence	spojenec	k1gMnPc4	spojenec
i	i	k9	i
pro	pro	k7c4	pro
Německo	Německo	k1gNnSc4	Německo
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
ten	ten	k3xDgInSc1	ten
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
k	k	k7c3	k
pobočníkovi	pobočník	k1gMnSc3	pobočník
<g/>
,	,	kIx,	,
květen	květen	k1gInSc1	květen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ale	ale	k9	ale
Vůdce	vůdce	k1gMnSc1	vůdce
mi	já	k3xPp1nSc3	já
důvěřuje	důvěřovat	k5eAaImIp3nS	důvěřovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mi	já	k3xPp1nSc3	já
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
dopis	dopis	k1gInSc1	dopis
manželce	manželka	k1gFnSc3	manželka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
</s>
</p>
<p>
<s>
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
záslužný	záslužný	k2eAgInSc1d1	záslužný
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
</s>
</p>
<p>
<s>
Pour	Pour	k1gMnSc1	Pour
le	le	k?	le
Mérite	Mérit	k1gMnSc5	Mérit
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
cti	čest	k1gFnSc2	čest
,	,	kIx,	,
1934	[number]	k4	1934
</s>
</p>
<p>
<s>
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
</s>
</p>
<p>
<s>
Železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
</s>
</p>
<p>
<s>
Rytířský	rytířský	k2eAgInSc1d1	rytířský
kříž	kříž	k1gInSc1	kříž
Železného	železný	k2eAgInSc2d1	železný
kříže	kříž	k1gInSc2	kříž
</s>
</p>
<p>
<s>
Rytířský	rytířský	k2eAgInSc1d1	rytířský
kříž	kříž	k1gInSc1	kříž
Železného	železný	k2eAgInSc2d1	železný
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
s	s	k7c7	s
dubovou	dubový	k2eAgFnSc4d1	dubová
ratolesti	ratolest	k1gFnPc4	ratolest
,	,	kIx,	,
desátý	desátý	k4xOgMnSc1	desátý
držitel	držitel	k1gMnSc1	držitel
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
</s>
</p>
<p>
<s>
Rytířský	rytířský	k2eAgInSc1d1	rytířský
kříž	kříž	k1gInSc1	kříž
Železného	železný	k2eAgInSc2d1	železný
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
s	s	k7c7	s
dubovou	dubový	k2eAgFnSc4d1	dubová
ratolesti	ratolest	k1gFnPc4	ratolest
a	a	k8xC	a
s	s	k7c7	s
meči	meč	k1gInPc7	meč
<g/>
,	,	kIx,	,
šestý	šestý	k4xOgMnSc1	šestý
držitel	držitel	k1gMnSc1	držitel
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
</s>
</p>
<p>
<s>
Ordine	Ordin	k1gMnSc5	Ordin
militare	militar	k1gMnSc5	militar
di	di	k?	di
Savoia	Savoia	k1gFnSc1	Savoia
,	,	kIx,	,
velkodůstojník	velkodůstojník	k1gInSc1	velkodůstojník
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
</s>
</p>
<p>
<s>
Rytířský	rytířský	k2eAgInSc1d1	rytířský
kříž	kříž	k1gInSc1	kříž
Železného	železný	k2eAgInSc2d1	železný
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
s	s	k7c7	s
dubovou	dubový	k2eAgFnSc4d1	dubová
ratolesti	ratolest	k1gFnPc4	ratolest
<g/>
,	,	kIx,	,
s	s	k7c7	s
meči	meč	k1gInPc7	meč
a	a	k8xC	a
brilianty	briliant	k1gInPc7	briliant
<g/>
,	,	kIx,	,
šestý	šestý	k4xOgMnSc1	šestý
držitel	držitel	k1gMnSc1	držitel
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
</s>
</p>
<p>
<s>
Medaille	Medaille	k1gFnSc1	Medaille
zur	zur	k?	zur
Erinnerung	Erinnerung	k1gInSc1	Erinnerung
an	an	k?	an
die	die	k?	die
Heimkehr	Heimkehr	k1gMnSc1	Heimkehr
des	des	k1gNnSc2	des
Memellandes	Memellandes	k1gMnSc1	Memellandes
</s>
</p>
<p>
<s>
Služební	služební	k2eAgNnSc1d1	služební
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
za	za	k7c4	za
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
službu	služba	k1gFnSc4	služba
4	[number]	k4	4
roky	rok	k1gInPc4	rok
</s>
</p>
<p>
<s>
Služební	služební	k2eAgNnSc1d1	služební
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
za	za	k7c4	za
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
službu	služba	k1gFnSc4	služba
12	[number]	k4	12
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Služební	služební	k2eAgNnSc1d1	služební
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
za	za	k7c4	za
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
službu	služba	k1gFnSc4	služba
18	[number]	k4	18
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Služební	služební	k2eAgNnSc1d1	služební
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
<g/>
,	,	kIx,	,
za	za	k7c4	za
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
službu	služba	k1gFnSc4	služba
25	[number]	k4	25
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
záslužný	záslužný	k2eAgInSc1d1	záslužný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Erwin	Erwin	k1gMnSc1	Erwin
Rommel	Rommel	k1gMnSc1	Rommel
</s>
</p>
<p>
<s>
YOUNG	YOUNG	kA	YOUNG
<g/>
,	,	kIx,	,
Desmond	Desmond	k1gInSc1	Desmond
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gInSc1	Rommel
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
REMY	remy	k1gNnSc4	remy
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc6	Maurika
Philip	Philip	k1gInSc4	Philip
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
Rommel	Rommel	k1gInSc1	Rommel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
1255	[number]	k4	1255
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IRVING	IRVING	kA	IRVING
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gMnSc1	Rommel
-	-	kIx~	-
Liška	Liška	k1gMnSc1	Liška
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
REUTH	REUTH	kA	REUTH
<g/>
,	,	kIx,	,
Ralf	Ralf	k1gMnSc1	Ralf
Georg	Georg	k1gMnSc1	Georg
<g/>
.	.	kIx.	.
</s>
<s>
Rommel	Rommel	k1gInSc1	Rommel
-	-	kIx~	-
Konec	konec	k1gInSc1	konec
jedné	jeden	k4xCgFnSc2	jeden
legendy	legenda	k1gFnSc2	legenda
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Slovanský	slovanský	k2eAgInSc1d1	slovanský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86421	[number]	k4	86421
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BARNETT	BARNETT	kA	BARNETT
<g/>
,	,	kIx,	,
Correli	Correl	k1gInPc7	Correl
<g/>
.	.	kIx.	.
</s>
<s>
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
generálové	generál	k1gMnPc1	generál
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Erwin	Erwin	k1gMnSc1	Erwin
Rommel	Rommel	k1gMnSc1	Rommel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Erwin	Erwin	k1gMnSc1	Erwin
Rommel	Rommela	k1gFnPc2	Rommela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Erwin	Erwin	k1gMnSc1	Erwin
Rommel	Rommel	k1gMnSc1	Rommel
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Tactical	Tacticat	k5eAaPmAgMnS	Tacticat
victory	victor	k1gMnPc4	victor
leading	leading	k1gInSc1	leading
to	ten	k3xDgNnSc1	ten
operational	operationat	k5eAaPmAgInS	operationat
failure	failur	k1gMnSc5	failur
<g/>
:	:	kIx,	:
Rommel	Rommel	k1gMnSc1	Rommel
in	in	k?	in
North	North	k1gMnSc1	North
Africa	Africa	k1gMnSc1	Africa
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
aneb	aneb	k?	aneb
kdo	kdo	k3yQnSc1	kdo
zranil	zranit	k5eAaPmAgMnS	zranit
Erwina	Erwin	k1gMnSc4	Erwin
Rommela	Rommel	k1gMnSc4	Rommel
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Ten	ten	k3xDgMnSc1	ten
Hitler	Hitler	k1gMnSc1	Hitler
snad	snad	k9	snad
není	být	k5eNaImIp3nS	být
normální	normální	k2eAgMnSc1d1	normální
<g/>
?	?	kIx.	?
</s>
<s>
pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Erwinu	Erwin	k1gMnSc3	Erwin
Rommelovi	Rommel	k1gMnSc3	Rommel
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
netradičních	tradiční	k2eNgInPc2d1	netradiční
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
citace	citace	k1gFnPc4	citace
z	z	k7c2	z
málo	málo	k6eAd1	málo
známých	známý	k2eAgInPc2d1	známý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
generál	generál	k1gMnSc1	generál
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
Erwin	Erwin	k1gMnSc1	Erwin
Johannes	Johannes	k1gMnSc1	Johannes
Eugen	Eugen	k2eAgInSc4d1	Eugen
Rommel	Rommel	k1gInSc4	Rommel
</s>
</p>
