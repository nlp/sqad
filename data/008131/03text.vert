<s>
Steak	steak	k1gInSc1	steak
je	být	k5eAaImIp3nS	být
plátek	plátek	k1gInSc1	plátek
masa	maso	k1gNnSc2	maso
ukrojený	ukrojený	k2eAgInSc1d1	ukrojený
přes	přes	k7c4	přes
vlákna	vlákno	k1gNnPc1	vlákno
z	z	k7c2	z
většího	veliký	k2eAgInSc2d2	veliký
kusu	kus	k1gInSc2	kus
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
hovězího	hovězí	k1gNnSc2	hovězí
nebo	nebo	k8xC	nebo
obecně	obecně	k6eAd1	obecně
červeného	červený	k2eAgNnSc2d1	červené
<g/>
.	.	kIx.	.
</s>
<s>
Steaky	steak	k1gInPc1	steak
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
upravují	upravovat	k5eAaImIp3nP	upravovat
grilováním	grilování	k1gNnSc7	grilování
nebo	nebo	k8xC	nebo
pečením	pečení	k1gNnSc7	pečení
na	na	k7c6	na
pánvi	pánev	k1gFnSc6	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
suchého	suchý	k2eAgNnSc2d1	suché
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
podávají	podávat	k5eAaImIp3nP	podávat
se	se	k3xPyFc4	se
v	v	k7c6	v
celku	celek	k1gInSc6	celek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
vždy	vždy	k6eAd1	vždy
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
části	část	k1gFnPc1	část
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Restaurace	restaurace	k1gFnPc1	restaurace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
tohoto	tento	k3xDgInSc2	tento
pokrmu	pokrm	k1gInSc2	pokrm
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
steakhouses	steakhouses	k1gInSc1	steakhouses
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
typické	typický	k2eAgFnSc3d1	typická
steakové	steakový	k2eAgFnSc3d1	steaková
večeři	večeře	k1gFnSc3	večeře
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
obvykle	obvykle	k6eAd1	obvykle
podávají	podávat	k5eAaImIp3nP	podávat
jako	jako	k8xS	jako
příloha	příloha	k1gFnSc1	příloha
nejčastěji	často	k6eAd3	často
brambory	brambora	k1gFnPc4	brambora
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
také	také	k9	také
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
těstoviny	těstovina	k1gFnSc2	těstovina
či	či	k8xC	či
fazole	fazole	k1gFnSc2	fazole
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
přidává	přidávat	k5eAaImIp3nS	přidávat
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
obloha	obloha	k1gFnSc1	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
také	také	k9	také
například	například	k6eAd1	například
ozdoba	ozdoba	k1gFnSc1	ozdoba
z	z	k7c2	z
vařeného	vařený	k2eAgInSc2d1	vařený
humřího	humří	k2eAgInSc2d1	humří
ocasu	ocas	k1gInSc2	ocas
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
surf	surf	k1gInSc1	surf
and	and	k?	and
turf	turf	k1gInSc1	turf
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konzumaci	konzumace	k1gFnSc3	konzumace
steaků	steak	k1gInPc2	steak
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
speciální	speciální	k2eAgInPc1d1	speciální
masivnější	masivní	k2eAgInPc1d2	masivnější
příbory	příbor	k1gInPc1	příbor
s	s	k7c7	s
ostřejším	ostrý	k2eAgInSc7d2	ostřejší
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
a	a	k8xC	a
stupeň	stupeň	k1gInSc1	stupeň
propečení	propečení	k1gNnSc2	propečení
steaku	steak	k1gInSc2	steak
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
osobní	osobní	k2eAgFnSc4d1	osobní
preferenci	preference	k1gFnSc4	preference
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
kratší	krátký	k2eAgInSc1d2	kratší
dobu	doba	k1gFnSc4	doba
je	být	k5eAaImIp3nS	být
steak	steak	k1gInSc1	steak
připravován	připravován	k2eAgInSc1d1	připravován
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
šťavnatější	šťavnatý	k2eAgFnSc1d2	šťavnatější
<g/>
,	,	kIx,	,
jemnější	jemný	k2eAgNnSc1d2	jemnější
a	a	k8xC	a
krvavější	krvavý	k2eAgNnSc1d2	krvavější
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgFnSc1d2	delší
doba	doba	k1gFnSc1	doba
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
znamená	znamenat	k5eAaImIp3nS	znamenat
menší	malý	k2eAgFnPc4d2	menší
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
případných	případný	k2eAgFnPc2d1	případná
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
slovní	slovní	k2eAgFnSc1d1	slovní
stupnice	stupnice	k1gFnSc1	stupnice
pečení	pečení	k1gNnSc2	pečení
steaku	steak	k1gInSc2	steak
od	od	k7c2	od
lehounce	lehounko	k6eAd1	lehounko
opečeného	opečený	k2eAgNnSc2d1	opečené
po	po	k7c4	po
plně	plně	k6eAd1	plně
propečený	propečený	k2eAgInSc4d1	propečený
<g/>
:	:	kIx,	:
Raw	Raw	k1gFnSc4	Raw
<g/>
:	:	kIx,	:
syrový	syrový	k2eAgInSc1d1	syrový
<g/>
,	,	kIx,	,
nepečený	pečený	k2eNgInSc1d1	nepečený
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
speciálních	speciální	k2eAgInPc2d1	speciální
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ale	ale	k9	ale
nejsou	být	k5eNaImIp3nP	být
skutečnými	skutečný	k2eAgInPc7d1	skutečný
steaky	steak	k1gInPc7	steak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Tatarský	tatarský	k2eAgInSc1d1	tatarský
biftek	biftek	k1gInSc1	biftek
či	či	k8xC	či
Carpaccio	Carpaccio	k1gNnSc1	Carpaccio
,	,	kIx,	,
se	se	k3xPyFc4	se
steaky	steak	k1gInPc7	steak
takto	takto	k6eAd1	takto
nepodávají	podávat	k5eNaImIp3nP	podávat
<g/>
.	.	kIx.	.
</s>
<s>
Blue	Blue	k1gInSc1	Blue
rare	rar	k1gMnSc2	rar
nebo	nebo	k8xC	nebo
very	vera	k1gMnSc2	vera
rare	rar	k1gMnSc2	rar
<g/>
:	:	kIx,	:
velmi	velmi	k6eAd1	velmi
mírně	mírně	k6eAd1	mírně
opečený	opečený	k2eAgInSc1d1	opečený
–	–	k?	–
jenom	jenom	k9	jenom
krátce	krátce	k6eAd1	krátce
ošhlehnout	ošhlehnout	k5eAaPmF	ošhlehnout
nad	nad	k7c7	nad
plamenem	plamen	k1gInSc7	plamen
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
strana	strana	k1gFnSc1	strana
cca	cca	kA	cca
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Zvenku	zvenku	k6eAd1	zvenku
je	být	k5eAaImIp3nS	být
opečen	opečen	k2eAgMnSc1d1	opečen
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vevnitř	vevnitř	k6eAd1	vevnitř
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
krvavý	krvavý	k2eAgInSc1d1	krvavý
<g/>
.	.	kIx.	.
</s>
<s>
Rare	Rarat	k5eAaPmIp3nS	Rarat
<g/>
:	:	kIx,	:
jemně	jemně	k6eAd1	jemně
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
opečený	opečený	k2eAgMnSc1d1	opečený
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
středu	střed	k1gInSc6	střed
52	[number]	k4	52
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
cca	cca	kA	cca
1	[number]	k4	1
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
tmavě-hnědý	tmavěnědý	k2eAgMnSc1d1	tmavě-hnědý
a	a	k8xC	a
vnitřek	vnitřek	k1gInSc1	vnitřek
se	se	k3xPyFc4	se
také	také	k9	také
začíná	začínat	k5eAaImIp3nS	začínat
zabarvovat	zabarvovat	k5eAaImF	zabarvovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
rudý	rudý	k2eAgInSc1d1	rudý
<g/>
.	.	kIx.	.
</s>
<s>
Medium	medium	k1gNnSc1	medium
rare	rar	k1gFnSc2	rar
<g/>
:	:	kIx,	:
středně	středně	k6eAd1	středně
opečený	opečený	k2eAgInSc1d1	opečený
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
středu	střed	k1gInSc6	střed
55	[number]	k4	55
°	°	k?	°
<g/>
C.	C.	kA	C.
Z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
je	být	k5eAaImIp3nS	být
tmavě-hnědý	tmavěnědý	k2eAgInSc1d1	tmavě-hnědý
<g/>
,	,	kIx,	,
střed	střed	k1gInSc1	střed
steaku	steak	k1gInSc2	steak
je	být	k5eAaImIp3nS	být
růžový	růžový	k2eAgInSc1d1	růžový
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
vnitřku	vnitřek	k1gInSc2	vnitřek
přechází	přecházet	k5eAaImIp3nS	přecházet
pomalu	pomalu	k6eAd1	pomalu
do	do	k7c2	do
šedo-hnědé	šedonědý	k2eAgFnSc2d1	šedo-hnědá
poblíž	poblíž	k7c2	poblíž
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Medium	medium	k1gNnSc1	medium
<g/>
:	:	kIx,	:
středně	středně	k6eAd1	středně
pečený	pečený	k2eAgInSc1d1	pečený
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
propečený	propečený	k2eAgInSc4d1	propečený
<g/>
;	;	kIx,	;
teplota	teplota	k1gFnSc1	teplota
středu	středa	k1gFnSc4	středa
58	[number]	k4	58
°	°	k?	°
<g/>
C	C	kA	C
–	–	k?	–
60	[number]	k4	60
°	°	k?	°
<g/>
C.	C.	kA	C.
Úplný	úplný	k2eAgInSc1d1	úplný
střed	střed	k1gInSc1	střed
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
růžový	růžový	k2eAgMnSc1d1	růžový
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
zbytek	zbytek	k1gInSc1	zbytek
masa	maso	k1gNnSc2	maso
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
propečen	propéct	k5eAaPmNgInS	propéct
do	do	k7c2	do
šedo-hnědé	šedonědý	k2eAgFnSc2d1	šedo-hnědá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Medium	medium	k1gNnSc1	medium
well	wella	k1gFnPc2	wella
<g/>
:	:	kIx,	:
více	hodně	k6eAd2	hodně
propečený	propečený	k2eAgInSc1d1	propečený
<g/>
;	;	kIx,	;
teplota	teplota	k1gFnSc1	teplota
středu	středa	k1gFnSc4	středa
65	[number]	k4	65
°	°	k?	°
<g/>
C.	C.	kA	C.
Maso	maso	k1gNnSc1	maso
už	už	k9	už
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnPc4d1	celá
hnědé	hnědý	k2eAgFnPc4d1	hnědá
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
náznaky	náznak	k1gInPc7	náznak
růžové	růžový	k2eAgFnSc2d1	růžová
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Šťavnatost	šťavnatost	k1gFnSc1	šťavnatost
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Well	Wellit	k5eAaPmRp2nS	Wellit
done	don	k1gMnSc5	don
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
<g/>
)	)	kIx)	)
propečený	propečený	k2eAgInSc4d1	propečený
<g/>
;	;	kIx,	;
teplota	teplota	k1gFnSc1	teplota
středu	středa	k1gFnSc4	středa
71	[number]	k4	71
°	°	k?	°
<g/>
C.	C.	kA	C.
Maso	maso	k1gNnSc1	maso
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
propečené	propečený	k2eAgInPc1d1	propečený
a	a	k8xC	a
okraje	okraj	k1gInPc1	okraj
mírně	mírně	k6eAd1	mírně
ohořelé	ohořelý	k2eAgInPc1d1	ohořelý
<g/>
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
je	být	k5eAaImIp3nS	být
šedo-hnědý	šedonědý	k2eAgInSc1d1	šedo-hnědý
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Něktěří	Něktěrý	k2eAgMnPc1d1	Něktěrý
kuchaři	kuchař	k1gMnPc1	kuchař
považují	považovat	k5eAaImIp3nP	považovat
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
jako	jako	k8xC	jako
plýtvání	plýtvání	k1gNnSc4	plýtvání
masem	maso	k1gNnSc7	maso
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
existuje	existovat	k5eAaImIp3nS	existovat
speciální	speciální	k2eAgInSc1d1	speciální
"	"	kIx"	"
<g/>
Chicago-style	Chicagotyl	k1gInSc5	Chicago-styl
steak	steak	k1gInSc1	steak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
na	na	k7c4	na
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
stupeň	stupeň	k1gInSc4	stupeň
propečení	propečení	k1gNnSc2	propečení
a	a	k8xC	a
poté	poté	k6eAd1	poté
rychle	rychle	k6eAd1	rychle
povrchově	povrchově	k6eAd1	povrchově
"	"	kIx"	"
<g/>
připálen	připálen	k2eAgMnSc1d1	připálen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
steak	steak	k1gInSc4	steak
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
objednat	objednat	k5eAaPmF	objednat
<g/>
,	,	kIx,	,
když	když	k8xS	když
řeknete	říct	k5eAaPmIp2nP	říct
něco	něco	k3yInSc4	něco
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Chicago-style	Chicagotyl	k1gInSc5	Chicago-styl
rare	rare	k1gFnSc3	rare
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Filet	filet	k1gInSc1	filet
mignon	mignon	k1gInSc1	mignon
–	–	k?	–
Malý	malý	k2eAgInSc1d1	malý
kousek	kousek	k1gInSc1	kousek
pravé	pravý	k2eAgFnSc2d1	pravá
svíčkové	svíčková	k1gFnSc2	svíčková
<g/>
.	.	kIx.	.
</s>
<s>
Flank	flank	k1gInSc1	flank
steak	steak	k1gInSc1	steak
–	–	k?	–
Steak	steak	k1gInSc1	steak
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
strany	strana	k1gFnSc2	strana
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
šťavnatý	šťavnatý	k2eAgMnSc1d1	šťavnatý
jako	jako	k8xS	jako
steaky	steak	k1gInPc1	steak
z	z	k7c2	z
žeber	žebro	k1gNnPc2	žebro
nebo	nebo	k8xC	nebo
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Flat	Flat	k2eAgInSc1d1	Flat
iron	iron	k1gInSc1	iron
steak	steak	k1gInSc1	steak
–	–	k?	–
Z	z	k7c2	z
lopatky	lopatka	k1gFnSc2	lopatka
<g/>
.	.	kIx.	.
</s>
<s>
T-bone	Ton	k1gMnSc5	T-bon
steak	steak	k1gInSc1	steak
a	a	k8xC	a
porterhouse	porterhouse	k1gFnSc1	porterhouse
–	–	k?	–
jemné	jemný	k2eAgNnSc4d1	jemné
maso	maso	k1gNnSc4	maso
ze	z	k7c2	z
zad	záda	k1gNnPc2	záda
zvířete	zvíře	k1gNnSc2	zvíře
s	s	k7c7	s
kostí	kost	k1gFnSc7	kost
tvaru	tvar	k1gInSc2	tvar
písmene	písmeno	k1gNnSc2	písmeno
T.	T.	kA	T.
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
hovězím	hovězí	k2eAgInSc6d1	hovězí
steaku	steak	k1gInSc6	steak
<g/>
.	.	kIx.	.
</s>
