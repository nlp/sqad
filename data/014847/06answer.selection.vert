<s>
Greta	Greta	k1gFnSc1	Greta
Thunbergová	Thunbergový	k2eAgFnSc1d1	Thunbergový
<g/>
,	,	kIx,
nepřechýleně	přechýleně	k6eNd1
plným	plný	k2eAgNnSc7d1	plné
jménem	jméno	k1gNnSc7	jméno
Greta	Gret	k1gInSc2	Gret
Tintin	Tintin	k1gInSc1	Tintin
Eleonora	Eleonora	k1gFnSc1	Eleonora
Ernman	Ernman	k1gMnSc1	Ernman
Thunberg	Thunberg	k1gMnSc1	Thunberg
(	(	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2	leden
2003	#num#	k4
<g/>
,	,	kIx,
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
celosvětově	celosvětově	k6eAd1
známá	známý	k2eAgFnSc1d1	známá
švédská	švédský	k2eAgFnSc1d1	švédská
klimatická	klimatický	k2eAgFnSc1d1	klimatická
aktivistka	aktivistka	k1gFnSc1	aktivistka
<g/>
.	.	kIx.
</s>
