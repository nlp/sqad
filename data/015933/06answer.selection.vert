<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
učenlivé	učenlivý	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
zajetí	zajetí	k1gNnSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nenáročná	náročný	k2eNgFnSc1d1
<g/>
,	,	kIx,
krotká	krotký	k2eAgFnSc1d1
a	a	k8xC
důvěřivá	důvěřivý	k2eAgFnSc1d1
a	a	k8xC
proto	proto	k8xC
byly	být	k5eAaImAgInP
učiněny	učiněn	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
o	o	k7c6
domestikaci	domestikace	k1gFnSc6
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
-	-	kIx~
antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgNnSc1d1
poskytuje	poskytovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
křehkého	křehký	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gNnSc1
mléko	mléko	k1gNnSc1
má	mít	k5eAaImIp3nS
větší	veliký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
bílkovin	bílkovina	k1gFnPc2
a	a	k8xC
mléčného	mléčný	k2eAgInSc2d1
tuku	tuk	k1gInSc2
než	než	k8xS
mléko	mléko	k1gNnSc1
kravské	kravský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>