<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc2d1
</s>
<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
Tragelaphus	Tragelaphus	k1gMnSc1
oryx	oryx	k1gInSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
sudokopytníci	sudokopytník	k1gMnPc1
(	(	kIx(
<g/>
Artiodactyla	Artiodactyla	k1gMnPc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
přežvýkaví	přežvýkavý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Ruminantia	Ruminantia	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
turovití	turovití	k1gMnPc1
(	(	kIx(
<g/>
Bovidae	Bovidae	k1gInSc1
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
tuři	tur	k1gMnPc1
(	(	kIx(
<g/>
Bovinae	Bovinae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
antilopa	antilopa	k1gFnSc1
(	(	kIx(
<g/>
Taurotragus	Taurotragus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Taurotragus	Taurotragus	k1gMnSc1
oryxPallas	oryxPallas	k1gMnSc1
<g/>
,	,	kIx,
1766	#num#	k4
</s>
<s>
mapa	mapa	k1gFnSc1
rozšíření	rozšíření	k1gNnSc2
antilopy	antilopa	k1gFnSc2
losí	losí	k2eAgFnSc2d1
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
mapa	mapa	k1gFnSc1
rozšíření	rozšíření	k1gNnSc2
antilopy	antilopa	k1gFnSc2
losí	losí	k2eAgFnSc2d1
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
(	(	kIx(
<g/>
Taurotragus	Taurotragus	k1gInSc1
oryx	oryx	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
druh	druh	k1gInSc1
přežvýkavého	přežvýkavý	k2eAgMnSc2d1
kopytníka	kopytník	k1gMnSc2
<g/>
,	,	kIx,
obývající	obývající	k2eAgFnSc1d1
africké	africký	k2eAgFnPc4d1
savany	savana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
ve	v	k7c6
stádech	stádo	k1gNnPc6
<g/>
,	,	kIx,
často	často	k6eAd1
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
druhy	druh	k1gInPc7
antilop	antilopa	k1gFnPc2
a	a	k8xC
se	s	k7c7
zebrami	zebra	k1gFnPc7
a	a	k8xC
po	po	k7c6
příbuzné	příbuzný	k2eAgFnSc6d1
antilopě	antilopa	k1gFnSc6
Derbyho	Derby	k1gMnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
největší	veliký	k2eAgInSc1d3
žijící	žijící	k2eAgInSc1d1
druh	druh	k1gInSc1
antilopy	antilopa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
300-950	300-950	k4
kg	kg	kA
</s>
<s>
Délka	délka	k1gFnSc1
těla	tělo	k1gNnSc2
<g/>
:	:	kIx,
200-350	200-350	k4
cm	cm	kA
</s>
<s>
Délka	délka	k1gFnSc1
ocasu	ocas	k1gInSc2
<g/>
:	:	kIx,
50-90	50-90	k4
cm	cm	kA
</s>
<s>
Výška	výška	k1gFnSc1
v	v	k7c6
kohoutku	kohoutek	k1gInSc6
<g/>
:	:	kIx,
120-180	120-180	k4
cm	cm	kA
</s>
<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
a	a	k8xC
mohutné	mohutný	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělí	dospělý	k2eAgMnPc1d1
samci	samec	k1gMnPc1
jsou	být	k5eAaImIp3nP
až	až	k9
dvakrát	dvakrát	k6eAd1
tak	tak	k6eAd1
velcí	velký	k2eAgMnPc1d1
než	než	k8xS
samice	samice	k1gFnPc1
a	a	k8xC
mohou	moct	k5eAaImIp3nP
vážit	vážit	k5eAaImF
až	až	k9
jednu	jeden	k4xCgFnSc4
tunu	tuna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kohoutku	kohoutek	k1gInSc6
mají	mít	k5eAaImIp3nP
tukový	tukový	k2eAgInSc4d1
hrbol	hrbol	k1gInSc4
<g/>
,	,	kIx,
typickým	typický	k2eAgInSc7d1
znakem	znak	k1gInSc7
je	být	k5eAaImIp3nS
také	také	k9
velký	velký	k2eAgInSc4d1
krční	krční	k2eAgInSc4d1
lalok	lalok	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
u	u	k7c2
samců	samec	k1gInPc2
větší	veliký	k2eAgFnSc1d2
a	a	k8xC
výraznější	výrazný	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Srst	srst	k1gFnSc1
na	na	k7c6
těle	tělo	k1gNnSc6
je	být	k5eAaImIp3nS
krátká	krátký	k2eAgFnSc1d1
<g/>
,	,	kIx,
žlutohnědá	žlutohnědý	k2eAgFnSc1d1
<g/>
,	,	kIx,
u	u	k7c2
samců	samec	k1gMnPc2
až	až	k6eAd1
rezavohnědá	rezavohnědý	k2eAgFnSc1d1
nebo	nebo	k8xC
modravě	modravě	k6eAd1
šedá	šedý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc4
pohlaví	pohlaví	k1gNnPc2
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
na	na	k7c6
bocích	bok	k1gInPc6
2-15	2-15	k4
příčných	příčný	k2eAgInPc2d1
světlých	světlý	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	Samek	k1gMnPc1
mají	mít	k5eAaImIp3nP
na	na	k7c6
hlavě	hlava	k1gFnSc6
hustou	hustý	k2eAgFnSc4d1
hnědočernou	hnědočerný	k2eAgFnSc4d1
čupřinu	čupřina	k1gFnSc4
<g/>
,	,	kIx,
obě	dva	k4xCgNnPc1
pohlaví	pohlaví	k1gNnPc1
mají	mít	k5eAaImIp3nP
tmavou	tmavý	k2eAgFnSc4d1
krátkou	krátký	k2eAgFnSc4d1
hřívu	hříva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
od	od	k7c2
krku	krk	k1gInSc2
<g/>
,	,	kIx,
přes	přes	k7c4
kohoutek	kohoutek	k1gInSc4
až	až	k9
po	po	k7c4
kořen	kořen	k1gInSc4
ocasu	ocas	k1gInSc2
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
také	také	k9
zakončen	zakončit	k5eAaPmNgInS
tmavším	tmavý	k2eAgInSc7d2
<g/>
,	,	kIx,
černohnědým	černohnědý	k2eAgInSc7d1
střapcem	střapec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Obě	dva	k4xCgNnPc4
pohlaví	pohlaví	k1gNnPc4
mají	mít	k5eAaImIp3nP
dlouhé	dlouhý	k2eAgInPc1d1
<g/>
,	,	kIx,
rovné	rovný	k2eAgInPc1d1
<g/>
,	,	kIx,
šroubovité	šroubovitý	k2eAgInPc1d1
rohy	roh	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
kolem	kolem	k7c2
65	#num#	k4
cm	cm	kA
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
u	u	k7c2
samců	samec	k1gMnPc2
jsou	být	k5eAaImIp3nP
delší	dlouhý	k2eAgFnPc1d2
a	a	k8xC
mohutnější	mohutný	k2eAgFnPc1d2
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
a	a	k8xC
stanoviště	stanoviště	k1gNnSc1
</s>
<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
obývá	obývat	k5eAaImIp3nS
stromové	stromový	k2eAgNnSc4d1
a	a	k8xC
křovinaté	křovinatý	k2eAgNnSc4d1
savany	savana	k1gFnPc4
<g/>
,	,	kIx,
světlé	světlý	k2eAgInPc4d1
lesy	les	k1gInPc4
<g/>
,	,	kIx,
polopouště	polopoušť	k1gFnPc4
i	i	k8xC
horské	horský	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
až	až	k9
do	do	k7c2
výšky	výška	k1gFnSc2
přes	přes	k7c4
4000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
v	v	k7c6
pásu	pás	k1gInSc6
přes	přes	k7c4
Etiopii	Etiopie	k1gFnSc4
a	a	k8xC
jižní	jižní	k2eAgNnSc4d1
Kongo	Kongo	k1gNnSc4
až	až	k9
do	do	k7c2
Jihoafrické	jihoafrický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biologie	biologie	k1gFnSc1
</s>
<s>
Antilopy	antilopa	k1gFnPc1
losí	losí	k2eAgFnPc1d1
žijí	žít	k5eAaImIp3nP
ve	v	k7c6
stádech	stádo	k1gNnPc6
<g/>
,	,	kIx,
často	často	k6eAd1
do	do	k7c2
25	#num#	k4
kusů	kus	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
dešťů	dešť	k1gInPc2
se	se	k3xPyFc4
samice	samice	k1gFnSc1
s	s	k7c7
telaty	tele	k1gNnPc7
shromažďují	shromažďovat	k5eAaImIp3nP
do	do	k7c2
větších	veliký	k2eAgFnPc2d2
stád	stádo	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
sucha	sucho	k1gNnSc2
pak	pak	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
ještě	ještě	k6eAd1
početnější	početní	k2eAgNnPc1d2
stáda	stádo	k1gNnPc1
<g/>
,	,	kIx,
často	často	k6eAd1
smíšená	smíšený	k2eAgFnSc1d1
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
druhy	druh	k1gInPc7
antilop	antilopa	k1gFnPc2
a	a	k8xC
se	s	k7c7
zebrami	zebra	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
stádě	stádo	k1gNnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
více	hodně	k6eAd2
dospělých	dospělý	k2eAgMnPc2d1
samců	samec	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nárok	nárok	k1gInSc1
na	na	k7c6
páření	páření	k1gNnSc6
se	se	k3xPyFc4
se	s	k7c7
samicemi	samice	k1gFnPc7
má	mít	k5eAaImIp3nS
jen	jen	k9
dominantní	dominantní	k2eAgMnSc1d1
býk	býk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Staří	starý	k2eAgMnPc1d1
samci	samec	k1gMnPc1
jsou	být	k5eAaImIp3nP
teritoriální	teritoriální	k2eAgMnPc1d1
<g/>
,	,	kIx,
žijí	žít	k5eAaImIp3nP
samotářsky	samotářsky	k6eAd1
zpravidla	zpravidla	k6eAd1
na	na	k7c6
zalesněných	zalesněný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
domovský	domovský	k2eAgInSc4d1
okrsek	okrsek	k1gInSc4
samic	samice	k1gFnPc2
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc1d2
<g/>
,	,	kIx,
stádo	stádo	k1gNnSc1
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
starou	starý	k2eAgFnSc7d1
samicí	samice	k1gFnSc7
<g/>
,	,	kIx,
migruje	migrovat	k5eAaImIp3nS
za	za	k7c7
lepší	dobrý	k2eAgFnSc7d2
pastvou	pastva	k1gFnSc7
po	po	k7c6
velkém	velký	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antilopy	antilopa	k1gFnSc2
losí	losí	k2eAgFnSc2d1
spásají	spásat	k5eAaImIp3nP,k5eAaPmIp3nP
vegetaci	vegetace	k1gFnSc4
v	v	k7c6
travnatých	travnatý	k2eAgFnPc6d1
i	i	k8xC
křovinatých	křovinatý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
se	se	k3xPyFc4
pasou	pást	k5eAaImIp3nP
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
potravu	potrava	k1gFnSc4
antilopy	antilopa	k1gFnSc2
losí	losí	k2eAgFnSc4d1
tvoří	tvořit	k5eAaImIp3nP
mladé	mladý	k2eAgFnPc4d1
větvičky	větvička	k1gFnPc4
a	a	k8xC
listí	listí	k1gNnSc4
<g/>
,	,	kIx,
pojídají	pojídat	k5eAaImIp3nP
také	také	k9
ovoce	ovoce	k1gNnPc1
a	a	k8xC
šťavnaté	šťavnatý	k2eAgFnPc1d1
cibulky	cibulka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vyhrabávají	vyhrabávat	k5eAaImIp3nP
kopyty	kopyto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
sucha	sucho	k1gNnSc2
si	se	k3xPyFc3
vystačí	vystačit	k5eAaBmIp3nS
s	s	k7c7
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
přijmou	přijmout	k5eAaPmIp3nP
spolu	spolu	k6eAd1
s	s	k7c7
potravou	potrava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Antilopy	antilopa	k1gFnPc1
losí	losí	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
pohyblivé	pohyblivý	k2eAgFnPc1d1
<g/>
,	,	kIx,
rychlé	rychlý	k2eAgFnPc1d1
antilopy	antilopa	k1gFnPc1
<g/>
,	,	kIx,
při	při	k7c6
útěku	útěk	k1gInSc6
dokážou	dokázat	k5eAaPmIp3nP
přeskočit	přeskočit	k5eAaPmF
i	i	k9
dvoumetrovou	dvoumetrový	k2eAgFnSc4d1
překážku	překážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
napadení	napadení	k1gNnSc6
predátorem	predátor	k1gMnSc7
<g/>
,	,	kIx,
například	například	k6eAd1
lvem	lev	k1gMnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
samice	samice	k1gFnPc1
shluknou	shluknout	k5eAaPmIp3nP
k	k	k7c3
obraně	obrana	k1gFnSc3
mláďat	mládě	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
říje	říje	k1gFnSc2
samci	samec	k1gMnPc1
bojují	bojovat	k5eAaImIp3nP
o	o	k7c4
samice	samice	k1gFnPc4
<g/>
,	,	kIx,
přetlačují	přetlačovat	k5eAaImIp3nP
se	se	k3xPyFc4
rohy	roh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantní	dominantní	k2eAgMnSc1d1
býk	býk	k1gMnSc1
se	se	k3xPyFc4
páří	pářit	k5eAaImIp3nS
s	s	k7c7
více	hodně	k6eAd2
kravami	kráva	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Březost	březost	k1gFnSc1
trvá	trvat	k5eAaImIp3nS
250-277	250-277	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
rodí	rodit	k5eAaImIp3nS
jediné	jediný	k2eAgNnSc1d1
mládě	mládě	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novorozená	novorozený	k2eAgNnPc1d1
mláďata	mládě	k1gNnPc1
nenásledují	následovat	k5eNaImIp3nP
svou	svůj	k3xOyFgFnSc4
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
leží	ležet	k5eAaImIp3nS
v	v	k7c6
úkrytu	úkryt	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
jej	on	k3xPp3gInSc4
matka	matka	k1gFnSc1
chodí	chodit	k5eAaImIp3nS
kojit	kojit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telata	tele	k1gNnPc1
jsou	být	k5eAaImIp3nP
odstavena	odstavit	k5eAaPmNgNnP
v	v	k7c6
6	#num#	k4
měsících	měsíc	k1gInPc6
věku	věk	k1gInSc2
<g/>
,	,	kIx,
pohlavní	pohlavní	k2eAgFnSc1d1
dospělost	dospělost	k1gFnSc1
nastupuje	nastupovat	k5eAaImIp3nS
po	po	k7c6
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
dožít	dožít	k5eAaPmF
až	až	k9
25	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
křížit	křížit	k5eAaImF
s	s	k7c7
domácím	domácí	k2eAgInSc7d1
skotem	skot	k1gInSc7
<g/>
,	,	kIx,
kříženci	kříženec	k1gMnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
neplodní	plodní	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k6eAd1
domácímu	domácí	k2eAgInSc3d1
skotu	skot	k1gInSc3
geneticky	geneticky	k6eAd1
bližší	blízký	k2eAgMnSc1d2
než	než	k8xS
např.	např.	kA
buvol	buvol	k1gMnSc1
africký	africký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
křížit	křížit	k5eAaImF
i	i	k9
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
druhy	druh	k1gInPc7
antilop	antilopa	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	on	k3xPp3gMnPc4
kudu	kudu	k1gMnPc4
velký	velký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Domestikace	domestikace	k1gFnSc1
</s>
<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
učenlivé	učenlivý	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
zajetí	zajetí	k1gNnSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nenáročná	náročný	k2eNgFnSc1d1
<g/>
,	,	kIx,
krotká	krotký	k2eAgFnSc1d1
a	a	k8xC
důvěřivá	důvěřivý	k2eAgFnSc1d1
a	a	k8xC
proto	proto	k8xC
byly	být	k5eAaImAgInP
učiněny	učiněn	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
o	o	k7c6
domestikaci	domestikace	k1gFnSc6
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
-	-	kIx~
antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgNnSc1d1
poskytuje	poskytovat	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
křehkého	křehký	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gNnSc1
mléko	mléko	k1gNnSc1
má	mít	k5eAaImIp3nS
větší	veliký	k2eAgInSc4d2
obsah	obsah	k1gInSc4
bílkovin	bílkovina	k1gFnPc2
a	a	k8xC
mléčného	mléčný	k2eAgInSc2d1
tuku	tuk	k1gInSc2
než	než	k8xS
mléko	mléko	k1gNnSc1
kravské	kravský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
chovat	chovat	k5eAaImF
ji	on	k3xPp3gFnSc4
na	na	k7c6
farmách	farma	k1gFnPc6
jako	jako	k9
polodomestikované	polodomestikovaný	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
antilopy	antilopa	k1gFnSc2
losí	losí	k2eAgFnPc4d1
farmově	farmově	k6eAd1
chovají	chovat	k5eAaImIp3nP
na	na	k7c6
Školním	školní	k2eAgInSc6d1
zemědělském	zemědělský	k2eAgInSc6d1
podniku	podnik	k1gInSc6
Lány	lán	k1gInPc4
České	český	k2eAgFnSc2d1
zemědělské	zemědělský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Chov	chov	k1gInSc1
v	v	k7c6
zoo	zoo	k1gFnSc6
</s>
<s>
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgNnSc1d1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
poměrně	poměrně	k6eAd1
často	často	k6eAd1
chovaným	chovaný	k2eAgMnPc3d1
druhům	druh	k1gMnPc3
antilop	antilopa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
Evropy	Evropa	k1gFnSc2
je	být	k5eAaImIp3nS
vidění	vidění	k1gNnSc3
v	v	k7c6
přibližně	přibližně	k6eAd1
120	#num#	k4
zoo	zoo	k1gFnPc2
zařízeních	zařízení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
Česka	Česko	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
tyto	tento	k3xDgFnPc4
zoologické	zoologický	k2eAgFnPc4d1
zahrady	zahrada	k1gFnPc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
:	:	kIx,
</s>
<s>
Zoo	zoo	k1gFnSc1
Dvorec	dvorec	k1gInSc1
</s>
<s>
Zoo	zoo	k1gFnSc1
Dvůr	Dvůr	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Zoo	zoo	k1gFnSc1
Hodonín	Hodonín	k1gInSc1
</s>
<s>
Zoo	zoo	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Kromě	kromě	k7c2
těchto	tento	k3xDgFnPc2
zoo	zoo	k1gFnPc2
je	být	k5eAaImIp3nS
antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
k	k	k7c3
vidění	vidění	k1gNnSc3
i	i	k8xC
v	v	k7c6
tzv.	tzv.	kA
Safari	safari	k1gNnSc1
pod	pod	k7c7
Jelením	jelení	k2eAgInSc7d1
vrchem	vrch	k1gInSc7
v	v	k7c6
Holčovicích	Holčovice	k1gFnPc6
či	či	k8xC
ve	v	k7c6
Školním	školní	k2eAgInSc6d1
zemědělském	zemědělský	k2eAgInSc6d1
podniku	podnik	k1gInSc6
v	v	k7c6
Lánech	lán	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byl	být	k5eAaImAgMnS
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
k	k	k7c3
vidění	vidění	k1gNnSc3
též	též	k6eAd1
v	v	k7c6
Zoo	zoo	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
a	a	k8xC
Zoo	zoo	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
chová	chovat	k5eAaImIp3nS
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
Zoo	zoo	k1gFnSc2
Košice	Košice	k1gInPc1
<g/>
.	.	kIx.
<g/>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byl	být	k5eAaImAgInS
chován	chovat	k5eAaImNgInS
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
též	též	k9
v	v	k7c6
Zoo	zoo	k1gFnSc6
Bojnice	Bojnice	k1gFnPc4
a	a	k8xC
Zoo	zoo	k1gFnSc1
Bratislava	Bratislava	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chov	chov	k1gInSc1
v	v	k7c6
Zoo	zoo	k1gFnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Antilopy	antilopa	k1gFnPc1
losí	losí	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
v	v	k7c6
Zoo	zoo	k1gNnSc6
Praha	Praha	k1gFnSc1
prvně	prvně	k?
chovány	chovat	k5eAaImNgInP
od	od	k7c2
roku	rok	k1gInSc2
1955	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
dvě	dva	k4xCgFnPc4
samice	samice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
následovala	následovat	k5eAaImAgFnS
takřka	takřka	k6eAd1
půlstoletí	půlstoletí	k1gNnSc4
dlouhá	dlouhý	k2eAgFnSc1d1
pauza	pauza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opět	opět	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
objevil	objevit	k5eAaPmAgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
osídlil	osídlit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
velkorysý	velkorysý	k2eAgInSc4d1
výběh	výběh	k1gInSc4
Africké	africký	k2eAgNnSc4d1
panorama	panorama	k1gNnSc4
<g/>
,	,	kIx,
umístěný	umístěný	k2eAgInSc4d1
severně	severně	k6eAd1
od	od	k7c2
silnice	silnice	k1gFnSc2
do	do	k7c2
Podhoří	podhoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2018	#num#	k4
byly	být	k5eAaImAgFnP
chovány	chovat	k5eAaImNgFnP
dvě	dva	k4xCgFnPc1
samice	samice	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ty	ten	k3xDgInPc1
jsou	být	k5eAaImIp3nP
chovány	chovat	k5eAaImNgInP
dlouhodobě	dlouhodobě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenují	jmenovat	k5eAaImIp3nP,k5eAaBmIp3nP
se	se	k3xPyFc4
Katrin	Katrin	k1gInSc4
(	(	kIx(
<g/>
narozena	narozen	k2eAgFnSc1d1
2007	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Klaudie	Klaudie	k1gFnSc1
(	(	kIx(
<g/>
narozena	narozen	k2eAgFnSc1d1
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
právě	právě	k9
Klaudie	Klaudie	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
tváří	tvář	k1gFnPc2
osvětové	osvětový	k2eAgFnSc2d1
popularizační	popularizační	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
seznamte	seznámit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
představila	představit	k5eAaPmAgFnS
zajímavé	zajímavý	k2eAgNnSc1d1
"	"	kIx"
<g/>
zvířecí	zvířecí	k2eAgFnSc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
"	"	kIx"
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
není	být	k5eNaImIp3nS
chován	chovat	k5eAaImNgInS
za	za	k7c7
účelem	účel	k1gInSc7
záchrany	záchrana	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c7
účelem	účel	k1gInSc7
vzdělávacím	vzdělávací	k2eAgInSc7d1
a	a	k8xC
expozičním	expoziční	k2eAgMnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Mj.	mj.	kA
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
nejvíce	nejvíce	k6eAd1,k6eAd3
podobnou	podobný	k2eAgFnSc4d1
antilopu	antilopa	k1gFnSc4
antilopě	antilopa	k1gFnSc3
Derbyho	Derby	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
ohrožena	ohrozit	k5eAaPmNgFnS
a	a	k8xC
na	na	k7c4
jejíž	jejíž	k3xOyRp3gFnSc4
ochranu	ochrana	k1gFnSc4
existuje	existovat	k5eAaImIp3nS
ochranářský	ochranářský	k2eAgInSc1d1
projekt	projekt	k1gInSc1
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgInSc2
je	být	k5eAaImIp3nS
zapojena	zapojen	k2eAgFnSc1d1
i	i	k8xC
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Zootierliste	Zootierlist	k1gMnSc5
<g/>
.	.	kIx.
zootierliste	zootierlist	k1gMnSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
-	-	kIx~
lexikon	lexikon	k1gInSc1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
www.zoopraha.cz	www.zoopraha.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ročenka	ročenka	k1gFnSc1
Unie	unie	k1gFnSc2
českých	český	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
zoologických	zoologický	k2eAgFnPc2d1
zahrad	zahrada	k1gFnPc2
2018	#num#	k4
<g/>
↑	↑	k?
Nová	nový	k2eAgFnSc1d1
reklamní	reklamní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
SEZNAMTE	seznámit	k5eAaPmRp2nP
SE	se	k3xPyFc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc2d1
–	–	k?
Klaudie	Klaudie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
ZOO	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
-	-	kIx~
Lexikon	lexikon	k1gNnSc1
zvířat	zvíře	k1gNnPc2
-	-	kIx~
antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgNnSc1d1
</s>
<s>
WildAfrica	WildAfrica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Antilopa	antilopa	k1gFnSc1
losí	losí	k2eAgFnSc1d1
(	(	kIx(
<g/>
Taurotragus	Taurotragus	k1gMnSc1
oryx	oryx	k1gInSc4
Pallas	Pallas	k1gMnSc1
<g/>
,	,	kIx,
1766	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Školní	školní	k2eAgInSc1d1
zemědělský	zemědělský	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Lány	lán	k1gInPc1
ČZU	ČZU	kA
<g/>
:	:	kIx,
Farmový	Farmový	k2eAgInSc4d1
chov	chov	k1gInSc4
antilopy	antilopa	k1gFnSc2
losí	losí	k2eAgNnSc1d1
Archivováno	archivován	k2eAgNnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Fahey	Fahey	k1gInPc1
<g/>
,	,	kIx,
B.	B.	kA
1999	#num#	k4
<g/>
.	.	kIx.
„	„	k?
<g/>
Taurotragus	Taurotragus	k1gInSc1
oryx	oryx	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
On-line	On-lin	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Animal	animal	k1gMnSc1
Diversity	Diversit	k1gInPc4
Web	web	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
ZOO	zoo	k1gFnSc1
Dvorec	dvorec	k1gInSc1
,	,	kIx,
www.zoodvorec.cz	www.zoodvorec.cz	k1gInSc1
</s>
