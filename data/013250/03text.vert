<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1965	[number]	k4	1965
Teplice	Teplice	k1gFnPc1	Teplice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Kabát	Kabát	k1gMnSc1	Kabát
<g/>
,	,	kIx,	,
muzikálový	muzikálový	k2eAgMnSc1d1	muzikálový
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Studium	studium	k1gNnSc1	studium
==	==	k?	==
</s>
</p>
<p>
<s>
Vyučil	vyučit	k5eAaPmAgInS	vyučit
se	se	k3xPyFc4	se
opravářem	opravář	k1gMnSc7	opravář
telefonů	telefon	k1gInPc2	telefon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Kabát	kabát	k1gInSc1	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
stala	stát	k5eAaPmAgFnS	stát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
českých	český	k2eAgFnPc2d1	Česká
kapel	kapela	k1gFnPc2	kapela
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
a	a	k8xC	a
současnosti	současnost	k1gFnSc2	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Kabát	kabát	k1gInSc1	kabát
do	do	k7c2	do
Eurovize	Eurovize	k1gFnSc2	Eurovize
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k6eAd1	už
se	se	k3xPyFc4	se
neumístili	umístit	k5eNaPmAgMnP	umístit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Muzikálová	muzikálový	k2eAgFnSc1d1	muzikálová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
mnoho	mnoho	k4c1	mnoho
let	léto	k1gNnPc2	léto
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
muzikálový	muzikálový	k2eAgMnSc1d1	muzikálový
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
např.	např.	kA	např.
v	v	k7c6	v
Krysaři	krysař	k1gMnSc3	krysař
<g/>
,	,	kIx,	,
Hamletovi	Hamlet	k1gMnSc3	Hamlet
<g/>
,	,	kIx,	,
Katu	kat	k1gMnSc3	kat
Mydlářovi	mydlář	k1gMnSc3	mydlář
<g/>
,	,	kIx,	,
Galileovi	Galileus	k1gMnSc3	Galileus
či	či	k8xC	či
ve	v	k7c6	v
Třech	tři	k4xCgMnPc6	tři
mušketýrech	mušketýr	k1gMnPc6	mušketýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
muzikálech	muzikál	k1gInPc6	muzikál
Angelika	Angelika	k1gFnSc1	Angelika
a	a	k8xC	a
Golem	Golem	k1gMnSc1	Golem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
muzikálové	muzikálový	k2eAgFnPc4d1	muzikálová
role	role	k1gFnPc4	role
patří	patřit	k5eAaImIp3nP	patřit
Baron	baron	k1gMnSc1	baron
Prášil	Prášil	k1gMnSc1	Prášil
<g/>
,	,	kIx,	,
Dracula	Dracula	k1gFnSc1	Dracula
<g/>
,	,	kIx,	,
Marcus	Marcus	k1gMnSc1	Marcus
Antonius	Antonius	k1gMnSc1	Antonius
<g/>
,	,	kIx,	,
Joffrey	Joffrea	k1gFnPc1	Joffrea
de	de	k?	de
Peyrac	Peyrac	k1gFnSc1	Peyrac
a	a	k8xC	a
Casanova	Casanův	k2eAgFnSc1d1	Casanova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Cenu	cena	k1gFnSc4	cena
Thálie	Thálie	k1gFnSc1	Thálie
2016	[number]	k4	2016
za	za	k7c4	za
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Mefisto	Mefisto	k1gMnSc1	Mefisto
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Hybernia	Hybernium	k1gNnSc2	Hybernium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herecká	herecký	k2eAgFnSc1d1	herecká
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
hrál	hrát	k5eAaImAgMnS	hrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
krotí	krotit	k5eAaImIp3nP	krotit
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
V	v	k7c6	v
peřině	peřina	k1gFnSc6	peřina
roli	role	k1gFnSc4	role
hospodského	hospodský	k1gMnSc2	hospodský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Porotce	porotce	k1gMnSc1	porotce
==	==	k?	==
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
–	–	k?	–
porotce	porotce	k1gMnSc1	porotce
televizní	televizní	k2eAgFnSc2d1	televizní
soutěže	soutěž	k1gFnSc2	soutěž
Hlas	hlas	k1gInSc1	hlas
ČeskoSlovenska	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
porotce	porotce	k1gMnSc1	porotce
televizní	televizní	k2eAgFnSc2d1	televizní
soutěže	soutěž	k1gFnSc2	soutěž
Hlas	hlas	k1gInSc1	hlas
ČeskoSlovenska	Československo	k1gNnSc2	Československo
</s>
</p>
<p>
<s>
2019	[number]	k4	2019
–	–	k?	–
porotce	porotce	k1gMnSc1	porotce
televizní	televizní	k2eAgFnSc2d1	televizní
soutěže	soutěž	k1gFnSc2	soutěž
The	The	k1gFnSc2	The
Voice	Voiec	k1gInSc2	Voiec
Česko	Česko	k1gNnSc1	Česko
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
==	==	k?	==
Rodinné	rodinný	k2eAgInPc1d1	rodinný
vztahy	vztah	k1gInPc1	vztah
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
potřetí	potřetí	k4xO	potřetí
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
tanečnicí	tanečnice	k1gFnSc7	tanečnice
Jovankou	Jovanka	k1gFnSc7	Jovanka
Skrčeskou	Skrčeska	k1gFnSc7	Skrčeska
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Albert	Albert	k1gMnSc1	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
tanečnicí	tanečnice	k1gFnSc7	tanečnice
Libuškou	Libuška	k1gFnSc7	Libuška
Vojtkovou	Vojtková	k1gFnSc7	Vojtková
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Matyáše	Matyáš	k1gMnSc4	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
vztahu	vztah	k1gInSc2	vztah
se	s	k7c7	s
Sabinou	Sabina	k1gFnSc7	Sabina
Laurinovou	Laurinová	k1gFnSc7	Laurinová
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Valentýnu	Valentýna	k1gFnSc4	Valentýna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Josefa	Josef	k1gMnSc4	Josef
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
</s>
</p>
<p>
<s>
Kabati	Kabat	k1gMnPc1	Kabat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Vojtek	Vojtek	k1gMnSc1	Vojtek
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
