<s>
Sodík	sodík	k1gInSc1	sodík
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Na	na	k7c4	na
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Natrium	natrium	k1gNnSc1	natrium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
zastoupený	zastoupený	k2eAgInSc1d1	zastoupený
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
i	i	k8xC	i
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
lehký	lehký	k2eAgInSc1d1	lehký
a	a	k8xC	a
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1	stříbrolesklý
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
krájet	krájet	k5eAaImF	krájet
nožem	nůž	k1gInSc7	nůž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
má	mít	k5eAaImIp3nS	mít
sodík	sodík	k1gInSc1	sodík
hodnotu	hodnota	k1gFnSc4	hodnota
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
1	[number]	k4	1
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
měkčí	měkký	k2eAgInSc1d2	měkčí
než	než	k8xS	než
mastek	mastek	k1gInSc1	mastek
i	i	k8xC	i
lithium	lithium	k1gNnSc1	lithium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
dobře	dobře	k6eAd1	dobře
vede	vést	k5eAaImIp3nS	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
i	i	k8xC	i
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
plave	plavat	k5eAaImIp3nS	plavat
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parách	para	k1gFnPc6	para
sodíku	sodík	k1gInSc2	sodík
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jednoatomových	jednoatomový	k2eAgFnPc2d1	jednoatomový
částic	částice	k1gFnPc2	částice
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
dvouatomovými	dvouatomový	k2eAgFnPc7d1	dvouatomová
molekulami	molekula	k1gFnPc7	molekula
<g/>
,	,	kIx,	,
páry	pár	k1gInPc1	pár
mají	mít	k5eAaImIp3nP	mít
purpurovou	purpurový	k2eAgFnSc4d1	purpurová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapalném	kapalný	k2eAgInSc6d1	kapalný
amoniaku	amoniak	k1gInSc6	amoniak
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
temně	temně	k6eAd1	temně
modrého	modrý	k2eAgInSc2d1	modrý
roztoku	roztok	k1gInSc2	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
nestabilních	stabilní	k2eNgInPc2d1	nestabilní
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
kovový	kovový	k2eAgInSc1d1	kovový
sodík	sodík	k1gInSc1	sodík
lze	lze	k6eAd1	lze
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
uchovávat	uchovávat	k5eAaImF	uchovávat
např.	např.	kA	např.
překrytý	překrytý	k2eAgInSc1d1	překrytý
vrstvou	vrstva	k1gFnSc7	vrstva
alifatických	alifatický	k2eAgInPc2d1	alifatický
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
jako	jako	k8xS	jako
petrolej	petrolej	k1gInSc1	petrolej
nebo	nebo	k8xC	nebo
nafta	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
pouze	pouze	k6eAd1	pouze
sloučeniny	sloučenina	k1gFnSc2	sloučenina
v	v	k7c4	v
mocenství	mocenství	k1gNnSc4	mocenství
Na	na	k7c4	na
<g/>
+	+	kIx~	+
-	-	kIx~	-
sodné	sodný	k2eAgFnPc1d1	sodná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
lze	lze	k6eAd1	lze
však	však	k9	však
také	také	k9	také
připravit	připravit	k5eAaPmF	připravit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
superbáze	superbáze	k1gFnSc2	superbáze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
sodík	sodík	k1gInSc1	sodík
sodidový	sodidový	k2eAgInSc1d1	sodidový
anion	anion	k1gInSc1	anion
Na	na	k7c6	na
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sodík	sodík	k1gInSc1	sodík
tak	tak	k6eAd1	tak
zaplní	zaplnit	k5eAaPmIp3nS	zaplnit
s-orbital	srbital	k1gMnSc1	s-orbital
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
stabilní	stabilní	k2eAgFnSc4d1	stabilní
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
konfiguraci	konfigurace	k1gFnSc4	konfigurace
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sodík	sodík	k1gInSc1	sodík
má	mít	k5eAaImIp3nS	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
ionizační	ionizační	k2eAgFnSc4d1	ionizační
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
afinitu	afinita	k1gFnSc4	afinita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
dojde	dojít	k5eAaPmIp3nS	dojít
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejsilnější	silný	k2eAgNnPc4d3	nejsilnější
redukční	redukční	k2eAgNnPc4d1	redukční
činidla	činidlo	k1gNnPc4	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
silně	silně	k6eAd1	silně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
i	i	k8xC	i
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
proto	proto	k8xC	proto
setkáváme	setkávat	k5eAaImIp1nP	setkávat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
sodíku	sodík	k1gInSc2	sodík
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
exotermní	exotermní	k2eAgNnSc1d1	exotermní
<g/>
,	,	kIx,	,
že	že	k8xS	že
unikající	unikající	k2eAgInSc1d1	unikající
vodík	vodík	k1gInSc1	vodík
reakčním	reakční	k2eAgNnSc7d1	reakční
teplem	teplo	k1gNnSc7	teplo
obvykle	obvykle	k6eAd1	obvykle
samovolně	samovolně	k6eAd1	samovolně
explozivně	explozivně	k6eAd1	explozivně
vzplane	vzplanout	k5eAaPmIp3nS	vzplanout
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
se	se	k3xPyFc4	se
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
slučuje	slučovat	k5eAaImIp3nS	slučovat
na	na	k7c4	na
peroxid	peroxid	k1gInSc4	peroxid
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
reaguje	reagovat	k5eAaBmIp3nS	reagovat
za	za	k7c2	za
mírného	mírný	k2eAgNnSc2d1	mírné
zahřátí	zahřátí	k1gNnSc2	zahřátí
na	na	k7c4	na
hydrid	hydrid	k1gInSc4	hydrid
sodný	sodný	k2eAgInSc4d1	sodný
NaH	naho	k1gNnPc2	naho
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
se	se	k3xPyFc4	se
sodík	sodík	k1gInSc1	sodík
slučuje	slučovat	k5eAaImIp3nS	slučovat
při	při	k7c6	při
elektrickém	elektrický	k2eAgInSc6d1	elektrický
výboji	výboj	k1gInSc6	výboj
<g/>
,	,	kIx,	,
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
může	moct	k5eAaImIp3nS	moct
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
nitrid	nitrid	k1gInSc4	nitrid
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
N	N	kA	N
nebo	nebo	k8xC	nebo
azid	azid	k1gInSc4	azid
sodný	sodný	k2eAgInSc4d1	sodný
NaN	NaN	k1gFnSc7	NaN
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Sodné	sodný	k2eAgFnPc1d1	sodná
soli	sůl	k1gFnPc1	sůl
barví	barvit	k5eAaImIp3nP	barvit
plamen	plamen	k1gInSc4	plamen
intenzivně	intenzivně	k6eAd1	intenzivně
žlutě	žlutě	k6eAd1	žlutě
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
sodných	sodný	k2eAgFnPc6d1	sodná
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
již	již	k6eAd1	již
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
látku	látka	k1gFnSc4	látka
neter	netra	k1gFnPc2	netra
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
praní	praní	k1gNnSc2	praní
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
samá	samý	k3xTgFnSc1	samý
látka	látka	k1gFnSc1	látka
byla	být	k5eAaImAgFnS	být
dobře	dobře	k6eAd1	dobře
známa	znám	k2eAgFnSc1d1	známa
i	i	k9	i
Egypťanům	Egypťan	k1gMnPc3	Egypťan
<g/>
,	,	kIx,	,
Řekům	Řek	k1gMnPc3	Řek
a	a	k8xC	a
Římanům	Říman	k1gMnPc3	Říman
(	(	kIx(	(
<g/>
Římané	Říman	k1gMnPc1	Říman
ji	on	k3xPp3gFnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
nitrum	nitrum	k1gInSc4	nitrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
nám	my	k3xPp1nPc3	my
dnes	dnes	k6eAd1	dnes
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
soda	soda	k1gFnSc1	soda
-	-	kIx~	-
uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
Na	na	k7c6	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
sodě	soda	k1gFnSc6	soda
byl	být	k5eAaImAgInS	být
přimíchán	přimíchat	k5eAaPmNgInS	přimíchat
i	i	k9	i
potaš	potaš	k1gInSc1	potaš
-	-	kIx~	-
uhličitan	uhličitan	k1gInSc1	uhličitan
draselný	draselný	k2eAgInSc1d1	draselný
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
odlišit	odlišit	k5eAaPmF	odlišit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dal	dát	k5eAaPmAgMnS	dát
alchymista	alchymista	k1gMnSc1	alchymista
Geber	Geber	k1gMnSc1	Geber
této	tento	k3xDgFnSc3	tento
sloučenině	sloučenina	k1gFnSc3	sloučenina
název	název	k1gInSc4	název
alkali	alkat	k5eAaBmAgMnP	alkat
<g/>
.	.	kIx.	.
</s>
<s>
Oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
sebe	se	k3xPyFc2	se
sodu	soda	k1gFnSc4	soda
a	a	k8xC	a
potaš	potaš	k1gFnSc4	potaš
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
povedlo	povést	k5eAaPmAgNnS	povést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1702	[number]	k4	1702
Georg	Georg	k1gMnSc1	Georg
Stahlovi	Stahlův	k2eAgMnPc1d1	Stahlův
a	a	k8xC	a
experimentálně	experimentálně	k6eAd1	experimentálně
to	ten	k3xDgNnSc4	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
roku	rok	k1gInSc2	rok
1736	[number]	k4	1736
Duhamel	Duhamel	k1gMnSc1	Duhamel
de	de	k?	de
Monceau	Monceaa	k1gFnSc4	Monceaa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
Mergraf	Mergraf	k1gMnSc1	Mergraf
odlišil	odlišit	k5eAaPmAgMnS	odlišit
oba	dva	k4xCgInPc4	dva
kovy	kov	k1gInPc4	kov
na	na	k7c6	na
základě	základ	k1gInSc6	základ
plamenových	plamenový	k2eAgFnPc2d1	plamenová
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
kov	kov	k1gInSc1	kov
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
siru	sir	k1gMnSc3	sir
Humphry	Humphra	k1gFnPc4	Humphra
Davymu	Davym	k1gInSc2	Davym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
elektrolyzoval	elektrolyzovat	k5eAaImAgInS	elektrolyzovat
kus	kus	k1gInSc4	kus
roztaveného	roztavený	k2eAgInSc2d1	roztavený
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
v	v	k7c6	v
platinové	platinový	k2eAgFnSc6d1	platinová
misce	miska	k1gFnSc6	miska
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
bohatě	bohatě	k6eAd1	bohatě
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
i	i	k9	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
2,4	[number]	k4	2,4
-	-	kIx~	-
2,6	[number]	k4	2,6
%	%	kIx~	%
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sodík	sodík	k1gInSc1	sodík
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
kation	kation	k1gInSc1	kation
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
10,5	[number]	k4	10,5
g	g	kA	g
Na	na	k7c4	na
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
sodíku	sodík	k1gInSc2	sodík
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
většiny	většina	k1gFnSc2	většina
sloučenin	sloučenina	k1gFnPc2	sloučenina
sodíku	sodík	k1gInSc2	sodík
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
geologických	geologický	k2eAgFnPc2d1	geologická
přeměn	přeměna	k1gFnPc2	přeměna
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
k	k	k7c3	k
vyplavení	vyplavení	k1gNnSc3	vyplavení
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
sodíku	sodík	k1gInSc2	sodík
z	z	k7c2	z
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vrstev	vrstva	k1gFnPc2	vrstva
pevninské	pevninský	k2eAgFnSc2d1	pevninská
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
do	do	k7c2	do
oceánských	oceánský	k2eAgFnPc2d1	oceánská
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
NaCl	NaCl	k1gInSc1	NaCl
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
složku	složka	k1gFnSc4	složka
mořské	mořský	k2eAgFnSc2d1	mořská
soli	sůl	k1gFnSc2	sůl
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
setkáme	setkat	k5eAaPmIp1nP	setkat
prakticky	prakticky	k6eAd1	prakticky
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
prvky	prvek	k1gInPc7	prvek
Mendělejevovy	Mendělejevův	k2eAgFnSc2d1	Mendělejevova
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
obsah	obsah	k1gInSc4	obsah
sodíkových	sodíkový	k2eAgInPc2d1	sodíkový
iontů	ion	k1gInPc2	ion
nalézáme	nalézat	k5eAaImIp1nP	nalézat
také	také	k9	také
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
podzemních	podzemní	k2eAgFnPc6d1	podzemní
minerálních	minerální	k2eAgFnPc6d1	minerální
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
horninami	hornina	k1gFnPc7	hornina
a	a	k8xC	a
sodíkové	sodíkový	k2eAgInPc1d1	sodíkový
ionty	ion	k1gInPc1	ion
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
vyloužily	vyloužit	k5eAaPmAgFnP	vyloužit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
obsažených	obsažený	k2eAgInPc2d1	obsažený
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
halit	halit	k1gInSc1	halit
(	(	kIx(	(
<g/>
sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
NaCl	NaCl	k1gInSc1	NaCl
<g/>
.	.	kIx.	.
</s>
<s>
Ložiska	ložisko	k1gNnSc2	ložisko
tohoto	tento	k3xDgInSc2	tento
minerálu	minerál	k1gInSc2	minerál
mají	mít	k5eAaImIp3nP	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
vyschlých	vyschlý	k2eAgNnPc6d1	vyschlé
jezerech	jezero	k1gNnPc6	jezero
a	a	k8xC	a
mořích	moře	k1gNnPc6	moře
minulých	minulý	k2eAgNnPc2d1	Minulé
geologických	geologický	k2eAgNnPc2d1	geologické
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
minerálů	minerál	k1gInPc2	minerál
biogenního	biogenní	k2eAgInSc2d1	biogenní
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
chilský	chilský	k2eAgInSc4d1	chilský
ledek	ledek	k1gInSc4	ledek
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
dusičnan	dusičnan	k1gInSc4	dusičnan
sodný	sodný	k2eAgInSc4d1	sodný
NaNO	NaNO	k1gFnSc7	NaNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
chilském	chilský	k2eAgNnSc6d1	Chilské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
minerály	minerál	k1gInPc1	minerál
sodíku	sodík	k1gInSc2	sodík
jsou	být	k5eAaImIp3nP	být
kryolit	kryolit	k1gInSc4	kryolit
Na	na	k7c4	na
<g/>
3	[number]	k4	3
<g/>
AlF	alfa	k1gFnPc2	alfa
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
thenardit	thenardit	k5eAaImF	thenardit
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Glauberova	Glauberův	k2eAgFnSc1d1	Glauberova
sůl	sůl	k1gFnSc1	sůl
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
10	[number]	k4	10
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
glauberit	glauberit	k1gInSc1	glauberit
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
CaSO	CaSO	k1gFnPc2	CaSO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
glaserit	glaserit	k1gInSc1	glaserit
NA	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4.3	[number]	k4	4.3
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
solfatarit	solfatarit	k1gInSc1	solfatarit
NaAl	NaAl	k1gInSc1	NaAl
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
12	[number]	k4	12
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
soda	soda	k1gFnSc1	soda
Na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2	[number]	k4	2
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
trona	trona	k6eAd1	trona
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
NaHCO	NaHCO	k1gFnSc1	NaHCO
<g/>
3.2	[number]	k4	3.2
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
borax	borax	k1gInSc1	borax
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
7.10	[number]	k4	7.10
H2O	H2O	k1gFnPc2	H2O
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
mnohé	mnohý	k2eAgInPc4d1	mnohý
živce	živec	k1gInPc4	živec
<g/>
,	,	kIx,	,
slídy	slída	k1gFnPc4	slída
<g/>
,	,	kIx,	,
alkalické	alkalický	k2eAgInPc4d1	alkalický
pyroxeny	pyroxen	k1gInPc4	pyroxen
<g/>
,	,	kIx,	,
alkalické	alkalický	k2eAgInPc4d1	alkalický
amfiboly	amfibol	k1gInPc4	amfibol
a	a	k8xC	a
zeolity	zeolit	k1gInPc4	zeolit
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
buňkách	buňka	k1gFnPc6	buňka
rostlinných	rostlinný	k2eAgFnPc2d1	rostlinná
i	i	k8xC	i
živočišných	živočišný	k2eAgFnPc2d1	živočišná
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
sodík	sodík	k1gInSc1	sodík
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
taveniny	tavenina	k1gFnSc2	tavenina
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgFnSc4d2	nižší
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
než	než	k8xS	než
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
volena	volit	k5eAaImNgFnS	volit
touto	tento	k3xDgFnSc7	tento
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
trochu	trochu	k6eAd1	trochu
komplikovanější	komplikovaný	k2eAgFnSc7d2	komplikovanější
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	s	k7c7	s
vhodnými	vhodný	k2eAgFnPc7d1	vhodná
přísadami	přísada	k1gFnPc7	přísada
podařilo	podařit	k5eAaPmAgNnS	podařit
výrazně	výrazně	k6eAd1	výrazně
snížit	snížit	k5eAaPmF	snížit
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
kovový	kovový	k2eAgInSc1d1	kovový
sodík	sodík	k1gInSc1	sodík
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
průmyslově	průmyslově	k6eAd1	průmyslově
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztavené	roztavený	k2eAgFnSc2d1	roztavená
směsi	směs	k1gFnSc2	směs
60	[number]	k4	60
%	%	kIx~	%
chloridu	chlorid	k1gInSc6	chlorid
vápenatého	vápenatý	k2eAgNnSc2d1	vápenaté
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
580	[number]	k4	580
°	°	k?	°
<g/>
C.	C.	kA	C.
Vápník	vápník	k1gInSc4	vápník
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
ve	v	k7c6	v
sběrné	sběrný	k2eAgFnSc6d1	sběrná
nádobě	nádoba	k1gFnSc6	nádoba
tuhne	tuhnout	k5eAaImIp3nS	tuhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgInSc4d2	vyšší
než	než	k8xS	než
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
od	od	k7c2	od
sodíku	sodík	k1gInSc2	sodík
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
<g/>
.	.	kIx.	.
</s>
<s>
Materiálem	materiál	k1gInSc7	materiál
katody	katoda	k1gFnSc2	katoda
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
anoda	anoda	k1gFnSc1	anoda
je	být	k5eAaImIp3nS	být
grafitová	grafitový	k2eAgFnSc1d1	grafitová
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
produktem	produkt	k1gInSc7	produkt
této	tento	k3xDgFnSc2	tento
elektrolýzy	elektrolýza	k1gFnSc2	elektrolýza
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
ihned	ihned	k6eAd1	ihned
dále	daleko	k6eAd2	daleko
zužitkován	zužitkovat	k5eAaPmNgInS	zužitkovat
pro	pro	k7c4	pro
chemickou	chemický	k2eAgFnSc4d1	chemická
syntézu	syntéza	k1gFnSc4	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
okolo	okolo	k7c2	okolo
200	[number]	k4	200
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
sodíku	sodík	k1gInSc2	sodík
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Na	na	k7c6	na
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
Na	na	k7c6	na
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Reakce	reakce	k1gFnPc1	reakce
na	na	k7c6	na
železné	železný	k2eAgFnSc6d1	železná
katodě	katoda	k1gFnSc6	katoda
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Cl	Cl	k1gMnSc1	Cl
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
Cl_	Cl_	k1gMnSc6	Cl_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Reakce	reakce	k1gFnPc1	reakce
na	na	k7c6	na
grafitové	grafitový	k2eAgFnSc6d1	grafitová
anodě	anoda	k1gFnSc6	anoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
slouží	sloužit	k5eAaImIp3nS	sloužit
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
jako	jako	k8xC	jako
katoda	katoda	k1gFnSc1	katoda
kovová	kovový	k2eAgFnSc1d1	kovová
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
sodíkový	sodíkový	k2eAgInSc4d1	sodíkový
amalgám	amalgám	k1gInSc4	amalgám
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
roztok	roztok	k1gInSc1	roztok
elementárního	elementární	k2eAgInSc2d1	elementární
sodíku	sodík	k1gInSc2	sodík
ve	v	k7c6	v
rtuti	rtuť	k1gFnSc6	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
nalézá	nalézat	k5eAaImIp3nS	nalézat
řadu	řada	k1gFnSc4	řada
uplatnění	uplatnění	k1gNnSc3	uplatnění
především	především	k6eAd1	především
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
jako	jako	k8xS	jako
účinné	účinný	k2eAgNnSc1d1	účinné
redukční	redukční	k2eAgNnSc1d1	redukční
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Sodíkový	sodíkový	k2eAgInSc1d1	sodíkový
amalgám	amalgám	k1gInSc1	amalgám
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
štěpí	štěpit	k5eAaImIp3nS	štěpit
vodou	voda	k1gFnSc7	voda
na	na	k7c4	na
hydroxid	hydroxid	k1gInSc4	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
výrobního	výrobní	k2eAgInSc2d1	výrobní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
výroby	výroba	k1gFnSc2	výroba
sodíku	sodík	k1gInSc2	sodík
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
toxická	toxický	k2eAgFnSc1d1	toxická
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
po	po	k7c6	po
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
uniká	unikat	k5eAaImIp3nS	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
výroby	výroba	k1gFnSc2	výroba
sodíkového	sodíkový	k2eAgInSc2d1	sodíkový
amalgámu	amalgám	k1gInSc2	amalgám
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
stabilní	stabilní	k2eAgFnSc4d1	stabilní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
sodík	sodík	k1gInSc1	sodík
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
nevyrábí	vyrábět	k5eNaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Roztavený	roztavený	k2eAgInSc1d1	roztavený
kovový	kovový	k2eAgInSc1d1	kovový
sodík	sodík	k1gInSc1	sodík
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
a	a	k8xC	a
v	v	k7c6	v
leteckých	letecký	k2eAgInPc6d1	letecký
motorech	motor	k1gInPc6	motor
jako	jako	k8xS	jako
látka	látka	k1gFnSc1	látka
odvádějící	odvádějící	k2eAgNnSc4d1	odvádějící
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
typech	typ	k1gInPc6	typ
reaktoru	reaktor	k1gInSc2	reaktor
vzniká	vznikat	k5eAaImIp3nS	vznikat
teplo	teplo	k6eAd1	teplo
jaderným	jaderný	k2eAgInSc7d1	jaderný
rozpadem	rozpad	k1gInSc7	rozpad
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
primárním	primární	k2eAgInSc6d1	primární
okruhu	okruh	k1gInSc6	okruh
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
využití	využití	k1gNnSc2	využití
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
poměrně	poměrně	k6eAd1	poměrně
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
především	především	k9	především
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
sodík	sodík	k1gInSc1	sodík
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
energetickými	energetický	k2eAgInPc7d1	energetický
neutrony	neutron	k1gInPc7	neutron
nebo	nebo	k8xC	nebo
γ	γ	k?	γ
-	-	kIx~	-
paprsky	paprsek	k1gInPc1	paprsek
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
přeměně	přeměna	k1gFnSc3	přeměna
na	na	k7c4	na
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
β	β	k?	β
nebo	nebo	k8xC	nebo
γ	γ	k?	γ
zářiče	zářič	k1gInPc1	zářič
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
sodík	sodík	k1gInSc1	sodík
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
silné	silný	k2eAgNnSc4d1	silné
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
proto	proto	k8xC	proto
využíván	využívat	k5eAaPmNgInS	využívat
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
organických	organický	k2eAgFnPc2d1	organická
syntetických	syntetický	k2eAgFnPc2d1	syntetická
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
některých	některý	k3yIgInPc2	některý
kovů	kov	k1gInPc2	kov
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
chloridů	chlorid	k1gInPc2	chlorid
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
titan	titan	k1gInSc1	titan
a	a	k8xC	a
zirkonium	zirkonium	k1gNnSc1	zirkonium
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc1	katalyzátor
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pryže	pryž	k1gFnSc2	pryž
a	a	k8xC	a
elastomerů	elastomer	k1gInPc2	elastomer
<g/>
.	.	kIx.	.
</s>
<s>
Elektrickým	elektrický	k2eAgInSc7d1	elektrický
výbojem	výboj	k1gInSc7	výboj
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
sodíkových	sodíkový	k2eAgFnPc2d1	sodíková
par	para	k1gFnPc2	para
o	o	k7c6	o
tlaku	tlak	k1gInSc6	tlak
několika	několik	k4yIc2	několik
torrů	torr	k1gInPc2	torr
vzniká	vznikat	k5eAaImIp3nS	vznikat
velmi	velmi	k6eAd1	velmi
intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
světelné	světelný	k2eAgNnSc1d1	světelné
vyzařování	vyzařování	k1gNnSc1	vyzařování
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
nalézá	nalézat	k5eAaImIp3nS	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
sodíkových	sodíkový	k2eAgFnPc2d1	sodíková
výbojek	výbojka	k1gFnPc2	výbojka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
prakticky	prakticky	k6eAd1	prakticky
setkat	setkat	k5eAaPmF	setkat
ve	v	k7c6	v
svítidlech	svítidlo	k1gNnPc6	svítidlo
pouličního	pouliční	k2eAgNnSc2d1	pouliční
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Neónové	neónový	k2eAgFnPc1d1	neónová
lampy	lampa	k1gFnPc1	lampa
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
Na	na	k7c4	na
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
jasného	jasný	k2eAgNnSc2d1	jasné
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Vysoušejí	vysoušet	k5eAaImIp3nP	vysoušet
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
kapaliny	kapalina	k1gFnPc4	kapalina
a	a	k8xC	a
transformátorový	transformátorový	k2eAgInSc4d1	transformátorový
olej	olej	k1gInSc4	olej
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
sodík	sodík	k1gInSc1	sodík
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
peroxidu	peroxid	k1gInSc2	peroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
pracích	prací	k2eAgInPc2d1	prací
prášků	prášek	k1gInPc2	prášek
a	a	k8xC	a
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
bělících	bělící	k2eAgFnPc2d1	bělící
lázní	lázeň	k1gFnPc2	lázeň
na	na	k7c4	na
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
,	,	kIx,	,
vlnu	vlna	k1gFnSc4	vlna
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgNnSc4d1	umělé
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
,	,	kIx,	,
slámu	sláma	k1gFnSc4	sláma
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
štětiny	štětina	k1gFnPc4	štětina
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnPc4d1	mořská
houby	houba	k1gFnPc4	houba
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc4	kost
a	a	k8xC	a
slonovinu	slonovina	k1gFnSc4	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Sodík	sodík	k1gInSc1	sodík
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
hydridu	hydrid	k1gInSc2	hydrid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgMnSc1d1	sodný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mýdel	mýdlo	k1gNnPc2	mýdlo
reakcí	reakce	k1gFnPc2	reakce
s	s	k7c7	s
vyššími	vysoký	k2eAgNnPc7d2	vyšší
tzv.	tzv.	kA	tzv.
mastnými	mastný	k2eAgFnPc7d1	mastná
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Sodná	sodný	k2eAgNnPc1d1	sodné
mýdla	mýdlo	k1gNnPc1	mýdlo
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
pevná	pevný	k2eAgFnSc1d1	pevná
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
draselných	draselný	k2eAgFnPc2d1	draselná
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
tekutá	tekutý	k2eAgFnSc1d1	tekutá
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
hedvábí	hedvábí	k1gNnSc2	hedvábí
a	a	k8xC	a
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
při	při	k7c6	při
čištění	čištění	k1gNnSc6	čištění
olejů	olej	k1gInPc2	olej
a	a	k8xC	a
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
základní	základní	k2eAgFnSc4d1	základní
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
i	i	k8xC	i
laboratorní	laboratorní	k2eAgFnSc4d1	laboratorní
chemikálii	chemikálie	k1gFnSc4	chemikálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Siřičitan	siřičitan	k1gInSc1	siřičitan
sodný	sodný	k2eAgMnSc1d1	sodný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
fotografickém	fotografický	k2eAgInSc6d1	fotografický
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
ustalovací	ustalovací	k2eAgFnSc6d1	ustalovací
fázi	fáze	k1gFnSc6	fáze
a	a	k8xC	a
u	u	k7c2	u
výbojek	výbojka	k1gFnPc2	výbojka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
farmacii	farmacie	k1gFnSc6	farmacie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
antiseptikum	antiseptikum	k1gNnSc1	antiseptikum
a	a	k8xC	a
jako	jako	k9	jako
konzervační	konzervační	k2eAgInSc4d1	konzervační
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Peroxid	peroxid	k1gInSc1	peroxid
sodný	sodný	k2eAgMnSc1d1	sodný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
poutání	poutání	k1gNnSc4	poutání
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
v	v	k7c6	v
ponorkách	ponorka	k1gFnPc6	ponorka
a	a	k8xC	a
dýchacích	dýchací	k2eAgInPc6d1	dýchací
přístrojích	přístroj	k1gInPc6	přístroj
pro	pro	k7c4	pro
potápěče	potápěč	k1gMnPc4	potápěč
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
oxon	oxona	k1gFnPc2	oxona
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
také	také	k9	také
využití	využití	k1gNnSc1	využití
jako	jako	k8xS	jako
energetické	energetický	k2eAgNnSc1d1	energetické
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
sodný	sodný	k2eAgMnSc1d1	sodný
NaCN	NaCN	k1gMnSc1	NaCN
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyluhování	vyluhování	k1gNnSc3	vyluhování
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgMnSc1d1	sodný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
a	a	k8xC	a
papírenském	papírenský	k2eAgInSc6d1	papírenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
pracích	prací	k2eAgInPc2d1	prací
prášků	prášek	k1gInPc2	prášek
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
solí	sůl	k1gFnPc2	sůl
a	a	k8xC	a
jako	jako	k9	jako
čisticí	čisticí	k2eAgInSc4d1	čisticí
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Hydrogenuhličitan	Hydrogenuhličitan	k1gInSc1	Hydrogenuhličitan
sodný	sodný	k2eAgMnSc1d1	sodný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
kypřících	kypřící	k2eAgInPc2d1	kypřící
prášků	prášek	k1gInPc2	prášek
do	do	k7c2	do
pečiva	pečivo	k1gNnSc2	pečivo
<g/>
,	,	kIx,	,
k	k	k7c3	k
neutralizaci	neutralizace	k1gFnSc3	neutralizace
poleptání	poleptání	k1gNnSc2	poleptání
kyselinou	kyselina	k1gFnSc7	kyselina
či	či	k9wB	či
k	k	k7c3	k
neutralizaci	neutralizace	k1gFnSc3	neutralizace
žaludečních	žaludeční	k2eAgFnPc2d1	žaludeční
šťáv	šťáva	k1gFnPc2	šťáva
při	při	k7c6	při
překyselení	překyselení	k1gNnSc6	překyselení
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
také	také	k9	také
používat	používat	k5eAaImF	používat
jako	jako	k8xC	jako
náplň	náplň	k1gFnSc4	náplň
do	do	k7c2	do
hasicích	hasicí	k2eAgInPc2d1	hasicí
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
sodný	sodný	k2eAgMnSc1d1	sodný
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
organických	organický	k2eAgNnPc2d1	organické
rozpouštědel	rozpouštědlo	k1gNnPc2	rozpouštědlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
pracích	prací	k2eAgInPc2d1	prací
prostředků	prostředek	k1gInPc2	prostředek
a	a	k8xC	a
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
projímadlo	projímadlo	k1gNnSc1	projímadlo
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
sodný	sodný	k2eAgInSc1d1	sodný
NaH	naho	k1gNnPc2	naho
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
a	a	k8xC	a
i	i	k9	i
na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
velmi	velmi	k6eAd1	velmi
živě	živě	k6eAd1	živě
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
sodný	sodný	k2eAgInSc1d1	sodný
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
silným	silný	k2eAgNnSc7d1	silné
redukčním	redukční	k2eAgNnSc7d1	redukční
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
mírně	mírně	k6eAd1	mírně
zahřátého	zahřátý	k2eAgInSc2d1	zahřátý
sodíku	sodík	k1gInSc2	sodík
ve	v	k7c6	v
vodíkové	vodíkový	k2eAgFnSc6d1	vodíková
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
při	při	k7c6	při
hoření	hoření	k1gNnSc6	hoření
sodíku	sodík	k1gInSc2	sodík
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
ho	on	k3xPp3gNnSc2	on
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
zahříváním	zahřívání	k1gNnSc7	zahřívání
peroxidu	peroxid	k1gInSc2	peroxid
sodného	sodný	k2eAgInSc2d1	sodný
se	s	k7c7	s
sodíkem	sodík	k1gInSc7	sodík
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
se	s	k7c7	s
sodíkem	sodík	k1gInSc7	sodík
<g/>
.	.	kIx.	.
</s>
<s>
Peroxid	peroxid	k1gInSc4	peroxid
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bledě	bledě	k6eAd1	bledě
žlutý	žlutý	k2eAgInSc1d1	žlutý
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
redukčními	redukční	k2eAgNnPc7d1	redukční
činidly	činidlo	k1gNnPc7	činidlo
se	se	k3xPyFc4	se
redukuje	redukovat	k5eAaBmIp3nS	redukovat
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
explozivně	explozivně	k6eAd1	explozivně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
pokojové	pokojový	k2eAgFnPc4d1	pokojová
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
a	a	k8xC	a
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
spalováním	spalování	k1gNnSc7	spalování
kovového	kovový	k2eAgInSc2d1	kovový
sodíku	sodík	k1gInSc2	sodík
v	v	k7c6	v
hliníkových	hliníkový	k2eAgFnPc6d1	hliníková
nádobách	nádoba	k1gFnPc6	nádoba
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
+	+	kIx~	+
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Na	na	k7c6	na
<g/>
+	+	kIx~	+
<g/>
O_	O_	k1gFnSc6	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
Na_	Na_	k1gMnSc6	Na_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Sodík	sodík	k1gInSc1	sodík
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
peroxidu	peroxid	k1gInSc2	peroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
sodný	sodný	k2eAgInSc1d1	sodný
NaOH	NaOH	k1gFnSc4	NaOH
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgNnPc1d1	bezbarvé
<g/>
,	,	kIx,	,
hygroskopická	hygroskopický	k2eAgNnPc1d1	hygroskopické
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
leptavá	leptavý	k2eAgFnSc1d1	leptavá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
leptá	leptat	k5eAaImIp3nS	leptat
i	i	k9	i
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
rozpouštění	rozpouštění	k1gNnSc6	rozpouštění
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
roztok	roztok	k1gInSc1	roztok
se	se	k3xPyFc4	se
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
roztoku	roztok	k1gInSc2	roztok
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
nebo	nebo	k8xC	nebo
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
sodného	sodný	k2eAgInSc2d1	sodný
či	či	k8xC	či
peroxidu	peroxid	k1gInSc2	peroxid
sodného	sodný	k2eAgInSc2d1	sodný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sodné	sodný	k2eAgFnPc1d1	sodná
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
obecně	obecně	k6eAd1	obecně
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
a	a	k8xC	a
jen	jen	k9	jen
několik	několik	k4yIc1	několik
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
anion	anion	k1gInSc1	anion
soli	sůl	k1gFnSc2	sůl
barevný	barevný	k2eAgInSc1d1	barevný
(	(	kIx(	(
<g/>
manganistan	manganistan	k1gInSc1	manganistan
<g/>
,	,	kIx,	,
chroman	chroman	k1gInSc1	chroman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sodné	sodný	k2eAgFnPc4d1	sodná
soli	sůl	k1gFnPc4	sůl
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
snadno	snadno	k6eAd1	snadno
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
nesnadno	snadno	k6eNd1	snadno
komplexy	komplex	k1gInPc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
50	[number]	k4	50
lety	léto	k1gNnPc7	léto
nebyly	být	k5eNaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
žádné	žádný	k3yNgInPc1	žádný
komplexy	komplex	k1gInPc1	komplex
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
vůbec	vůbec	k9	vůbec
schopny	schopen	k2eAgFnPc1d1	schopna
tvořit	tvořit	k5eAaImF	tvořit
komplexy	komplex	k1gInPc4	komplex
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
)	)	kIx)	)
Chlorid	chlorid	k1gInSc4	chlorid
sodný	sodný	k2eAgInSc4d1	sodný
NaCl	NaCl	k1gInSc4	NaCl
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
nebo	nebo	k8xC	nebo
kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
sůl	sůl	k1gFnSc1	sůl
patří	patřit	k5eAaImIp3nS	patřit
od	od	k7c2	od
dávných	dávný	k2eAgFnPc2d1	dávná
dob	doba	k1gFnPc2	doba
k	k	k7c3	k
běžně	běžně	k6eAd1	běžně
využívaným	využívaný	k2eAgFnPc3d1	využívaná
chemikáliím	chemikálie	k1gFnPc3	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
součást	součást	k1gFnSc1	součást
lidské	lidský	k2eAgFnSc2d1	lidská
potravy	potrava	k1gFnSc2	potrava
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
mimořádně	mimořádně	k6eAd1	mimořádně
cennou	cenný	k2eAgFnSc7d1	cenná
surovinou	surovina	k1gFnSc7	surovina
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
výnosným	výnosný	k2eAgInPc3d1	výnosný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
značně	značně	k6eAd1	značně
riskantním	riskantní	k2eAgInPc3d1	riskantní
oborům	obor	k1gInPc3	obor
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nalézá	nalézat	k5eAaImIp3nS	nalézat
NaCl	NaCl	k1gMnSc1	NaCl
řadu	řad	k1gInSc2	řad
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
uplatnění	uplatnění	k1gNnPc2	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
s	s	k7c7	s
kuchyňskou	kuchyňský	k2eAgFnSc7d1	kuchyňská
solí	sůl	k1gFnSc7	sůl
setkáme	setkat	k5eAaPmIp1nP	setkat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
dolováním	dolování	k1gNnSc7	dolování
kamenné	kamenný	k2eAgFnSc2d1	kamenná
soli	sůl	k1gFnSc2	sůl
nebo	nebo	k8xC	nebo
odpařováním	odpařování	k1gNnSc7	odpařování
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
sodný	sodný	k2eAgInSc1d1	sodný
neboli	neboli	k8xC	neboli
soda	soda	k1gFnSc1	soda
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
ze	z	k7c2	z
solanky	solanka	k1gFnSc2	solanka
(	(	kIx(	(
<g/>
nasycený	nasycený	k2eAgInSc1d1	nasycený
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
NaCl	NaCl	k1gInSc4	NaCl
<g/>
)	)	kIx)	)
Solvayovým	Solvayův	k2eAgInSc7d1	Solvayův
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
do	do	k7c2	do
solanky	solanka	k1gFnSc2	solanka
nasycené	nasycený	k2eAgFnSc2d1	nasycená
amoniakem	amoniak	k1gInSc7	amoniak
zavádí	zavádět	k5eAaImIp3nS	zavádět
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
hydrogenuhličitan	hydrogenuhličitan	k1gInSc4	hydrogenuhličitan
sodný	sodný	k2eAgInSc4d1	sodný
NaHCO	NaHCO	k1gFnSc7	NaHCO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
pecích	pec	k1gFnPc6	pec
za	za	k7c2	za
teploty	teplota	k1gFnSc2	teplota
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
uhličitan	uhličitan	k1gInSc4	uhličitan
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
C	C	kA	C
l	l	kA	l
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	O	kA	O
+	+	kIx~	+
N	N	kA	N
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
H	H	kA	H
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
N	N	kA	N
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
l	l	kA	l
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
NaCl	NaCl	k1gMnSc1	NaCl
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
<g/>
NH_	NH_	k1gFnSc4	NH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
NaHCO_	NaHCO_	k1gMnSc6	NaHCO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
<g/>
NH_	NH_	k1gFnSc2	NH_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
amoniakem	amoniak	k1gInSc7	amoniak
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
hydrogenuhličitanu	hydrogenuhličitan	k1gInSc2	hydrogenuhličitan
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
N	N	kA	N
a	a	k8xC	a
H	H	kA	H
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
C	C	kA	C
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
H	H	kA	H
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gInSc1	mathsf
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
NaHCO_	NaHCO_	k1gMnSc1	NaHCO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
Na_	Na_	k1gMnSc6	Na_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
CO_	CO_	k1gFnSc2	CO_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Hydrogenuhličitan	Hydrogenuhličitan	k1gInSc4	Hydrogenuhličitan
sodný	sodný	k2eAgInSc4d1	sodný
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
uhličitan	uhličitan	k1gInSc4	uhličitan
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Hydrogenuhličitan	Hydrogenuhličitan	k1gInSc1	Hydrogenuhličitan
sodný	sodný	k2eAgInSc1d1	sodný
neboli	neboli	k8xC	neboli
jedlá	jedlý	k2eAgFnSc1d1	jedlá
soda	soda	k1gFnSc1	soda
NaHCO	NaHCO	k1gFnSc2	NaHCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
se	s	k7c7	s
zásaditou	zásaditý	k2eAgFnSc7d1	zásaditá
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Solvayově	Solvayův	k2eAgInSc6d1	Solvayův
způsobu	způsob	k1gInSc6	způsob
výrobu	výroba	k1gFnSc4	výroba
uhličitanu	uhličitan	k1gInSc2	uhličitan
sodného	sodný	k2eAgNnSc2d1	sodné
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydrogenuhličitan	hydrogenuhličitan	k1gInSc1	hydrogenuhličitan
sodný	sodný	k2eAgInSc1d1	sodný
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
prvním	první	k4xOgInSc6	první
kroku	krok	k1gInSc6	krok
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
také	také	k9	také
získává	získávat	k5eAaImIp3nS	získávat
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
sodný	sodný	k2eAgInSc1d1	sodný
neboli	neboli	k8xC	neboli
sodný	sodný	k2eAgInSc1d1	sodný
nebo	nebo	k8xC	nebo
chilský	chilský	k2eAgInSc1d1	chilský
ledek	ledek	k1gInSc1	ledek
NaNO	NaNO	k1gFnSc1	NaNO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
nahořklou	nahořklý	k2eAgFnSc7d1	nahořklá
chutí	chuť	k1gFnSc7	chuť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc4	dusičnan
sodný	sodný	k2eAgInSc4d1	sodný
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
chilském	chilský	k2eAgNnSc6d1	Chilské
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Zakaspicku	Zakaspick	k1gInSc6	Zakaspick
a	a	k8xC	a
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Hydratovaná	hydratovaný	k2eAgFnSc1d1	hydratovaná
sůl	sůl	k1gFnSc1	sůl
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Glauberova	Glauberův	k2eAgFnSc1d1	Glauberova
sůl	sůl	k1gFnSc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
kyseliny	kyselina	k1gFnSc2	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
z	z	k7c2	z
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
sodného	sodný	k2eAgInSc2d1	sodný
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
chlorid	chlorid	k1gInSc4	chlorid
sodný	sodný	k2eAgInSc4d1	sodný
a	a	k8xC	a
síran	síran	k1gInSc4	síran
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc4	sulfid
sodný	sodný	k2eAgInSc4d1	sodný
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
Mezi	mezi	k7c7	mezi
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
sodíku	sodík	k1gInSc2	sodík
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
sodné	sodný	k2eAgFnPc1d1	sodná
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
-	-	kIx~	-
například	například	k6eAd1	například
benzoan	benzoan	k1gInSc1	benzoan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
hořčice	hořčice	k1gFnSc2	hořčice
jako	jako	k8xC	jako
konzervační	konzervační	k2eAgInSc4d1	konzervační
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
a	a	k8xC	a
sodné	sodný	k2eAgInPc4d1	sodný
alkoholáty	alkoholát	k1gInPc4	alkoholát
například	například	k6eAd1	například
ethoxid	ethoxid	k1gInSc1	ethoxid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
ethanolát	ethanolát	k1gInSc1	ethanolát
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
silná	silný	k2eAgNnPc1d1	silné
organická	organický	k2eAgNnPc1d1	organické
redukční	redukční	k2eAgNnPc1d1	redukční
činidla	činidlo	k1gNnPc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
sodným	sodný	k2eAgFnPc3d1	sodná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
patří	patřit	k5eAaImIp3nS	patřit
organické	organický	k2eAgInPc4d1	organický
komplexy	komplex	k1gInPc4	komplex
sodných	sodný	k2eAgFnPc2d1	sodná
sloučenin	sloučenina	k1gFnPc2	sloučenina
crowny	crowna	k1gFnSc2	crowna
a	a	k8xC	a
kryptáty	kryptát	k1gInPc4	kryptát
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
organických	organický	k2eAgFnPc2d1	organická
sodných	sodný	k2eAgFnPc2d1	sodná
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Jursík	Jursík	k1gMnSc1	Jursík
F.	F.	kA	F.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7080-504-8	[number]	k4	80-7080-504-8
(	(	kIx(	(
<g/>
elektronická	elektronický	k2eAgFnSc1d1	elektronická
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
Jiří	Jiří	k1gMnSc1	Jiří
Hlinka	Hlinka	k1gMnSc1	Hlinka
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
-	-	kIx~	-
studijní	studijní	k2eAgInSc1d1	studijní
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
2003	[number]	k4	2003
ISBN	ISBN	kA	ISBN
80-86376-31-1	[number]	k4	80-86376-31-1
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
<g />
.	.	kIx.	.
</s>
<s>
Earnshaw	Earnshaw	k?	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Jiří	Jiří	k1gMnSc1	Jiří
Vacík	Vacík	k1gMnSc1	Vacík
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
,	,	kIx,	,
Přehled	přehled	k1gInSc1	přehled
středošoklské	středošoklský	k2eAgFnSc2d1	středošoklský
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1999	[number]	k4	1999
ISBN	ISBN	kA	ISBN
80-7235-108-7	[number]	k4	80-7235-108-7
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sodík	sodík	k1gInSc1	sodík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sodík	sodík	k1gInSc1	sodík
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc4d1	chemický
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
portál	portál	k1gInSc4	portál
vliv	vliv	k1gInSc1	vliv
sodíku	sodík	k1gInSc2	sodík
na	na	k7c4	na
organismus	organismus	k1gInSc4	organismus
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
obecné	obecný	k2eAgFnPc1d1	obecná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
The	The	k1gFnSc2	The
Wooden	Woodna	k1gFnPc2	Woodna
Periodic	Periodic	k1gMnSc1	Periodic
Table	tablo	k1gNnSc6	tablo
Table	tablo	k1gNnSc6	tablo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Entry	Entr	k1gInPc7	Entr
on	on	k3xPp3gMnSc1	on
Sodium	Sodium	k1gNnSc1	Sodium
Sodium	Sodium	k1gNnSc1	Sodium
isotopes	isotopesa	k1gFnPc2	isotopesa
data	datum	k1gNnSc2	datum
from	from	k1gMnSc1	from
The	The	k1gMnSc1	The
Berkeley	Berkele	k2eAgInPc4d1	Berkele
Laboratory	Laborator	k1gInPc4	Laborator
Isotopes	Isotopes	k1gMnSc1	Isotopes
Project	Project	k1gMnSc1	Project
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
</s>
