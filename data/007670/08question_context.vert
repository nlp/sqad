<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
Valpurga	Valpurga	k1gFnSc1	Valpurga
Amálie	Amálie	k1gFnSc1	Amálie
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Maria	Mario	k1gMnSc4	Mario
Theresia	Theresius	k1gMnSc4	Theresius
Walburga	Walburg	k1gMnSc4	Walburg
Amalia	Amalius	k1gMnSc4	Amalius
Christiana	Christian	k1gMnSc4	Christian
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1717	[number]	k4	1717
Vídeň	Vídeň	k1gFnSc1	Vídeň
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1780	[number]	k4	1780
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
rodu	rod	k1gInSc6	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
byla	být	k5eAaImAgFnS	být
arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
<g/>
,	,	kIx,	,
královnou	královna	k1gFnSc7	královna
uherskou	uherský	k2eAgFnSc7d1	uherská
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
a	a	k8xC	a
českou	český	k2eAgFnSc7d1	Česká
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
–	–	k?	–
<g/>
1780	[number]	k4	1780
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
markraběnkou	markraběnka	k1gFnSc7	markraběnka
moravskou	moravský	k2eAgFnSc7d1	Moravská
atd.	atd.	kA	atd.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
ženou	žena	k1gFnSc7	žena
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
její	její	k3xOp3gFnSc2	její
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mnohým	mnohý	k2eAgFnPc3d1	mnohá
reformám	reforma	k1gFnPc3	reforma
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
také	také	k9	také
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nemohla	moct	k5eNaImAgFnS	moct
stát	stát	k5eAaImF	stát
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
císařovnou	císařovna	k1gFnSc7	císařovna
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Císařem	Císař	k1gMnSc7	Císař
byl	být	k5eAaImAgInS	být
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1745	[number]	k4	1745
zvolen	zvolit	k5eAaPmNgMnS	zvolit
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
František	František	k1gMnSc1	František
I.	I.	kA	I.
Štěpán	Štěpán	k1gMnSc1	Štěpán
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
císařskou	císařský	k2eAgFnSc7d1	císařská
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
nezůstala	zůstat	k5eNaPmAgFnS	zůstat
jen	jen	k9	jen
dcerou	dcera	k1gFnSc7	dcera
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
císařovnou	císařovna	k1gFnSc7	císařovna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
i	i	k9	i
matkou	matka	k1gFnSc7	matka
dvou	dva	k4xCgMnPc2	dva
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Leopolda	Leopolda	k1gFnSc1	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
byla	být	k5eAaImAgFnS	být
pokřtěna	pokřtít	k5eAaPmNgFnS	pokřtít
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
dnu	den	k1gInSc6	den
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
dcerou	dcera	k1gFnSc7	dcera
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
Alžběty	Alžběta	k1gFnSc2	Alžběta
Kristýny	Kristýna	k1gFnPc4	Kristýna
Brunšvické	brunšvický	k2eAgFnPc4d1	Brunšvická
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
jediný	jediný	k2eAgMnSc1d1	jediný
bratr	bratr	k1gMnSc1	bratr
Leopold	Leopold	k1gMnSc1	Leopold
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
necelých	celý	k2eNgInPc2d1	necelý
sedmi	sedm	k4xCc2	sedm
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
narozením	narození	k1gNnSc7	narození
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1730	[number]	k4	1730
zemřela	zemřít	k5eAaPmAgFnS	zemřít
i	i	k9	i
její	její	k3xOp3gFnSc1	její
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
šestiletá	šestiletý	k2eAgFnSc1d1	šestiletá
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Marie	Marie	k1gFnSc1	Marie
Amálie	Amálie	k1gFnSc1	Amálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pragmatické	pragmatický	k2eAgFnSc2d1	pragmatická
sankce	sankce	k1gFnSc2	sankce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1713	[number]	k4	1713
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
předpokládanou	předpokládaný	k2eAgFnSc7d1	předpokládaná
dědičkou	dědička	k1gFnSc7	dědička
habsburských	habsburský	k2eAgFnPc2d1	habsburská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
rakouského	rakouský	k2eAgNnSc2d1	rakouské
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
ji	on	k3xPp3gFnSc4	on
vychovávali	vychovávat	k5eAaImAgMnP	vychovávat
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgInSc2d1	hlavní
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
bylo	být	k5eAaImAgNnS	být
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
učila	učít	k5eAaPmAgFnS	učít
dějinám	dějiny	k1gFnPc3	dějiny
<g/>
,	,	kIx,	,
latině	latina	k1gFnSc3	latina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
a	a	k8xC	a
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
například	například	k6eAd1	například
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
,	,	kIx,	,
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
tančila	tančit	k5eAaImAgFnS	tančit
společně	společně	k6eAd1	společně
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Marií	Maria	k1gFnSc7	Maria
Annou	Anna	k1gFnSc7	Anna
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
Euristeo	Euristeo	k1gNnSc1	Euristeo
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Zenón	Zenón	k1gMnSc1	Zenón
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
otcových	otcův	k2eAgFnPc2d1	otcova
jmenin	jmeniny	k1gFnPc2	jmeniny
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
zpívala	zpívat	k5eAaImAgFnS	zpívat
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Germania	germanium	k1gNnSc2	germanium
il	il	k?	il
di	di	k?	di
che	che	k0	che
spende	spend	k1gInSc5	spend
Sagro	Sagro	k1gNnSc4	Sagro
all	all	k?	all
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
si	se	k3xPyFc3	se
užila	užít	k5eAaPmAgFnS	užít
i	i	k8xC	i
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hereckou	herecký	k2eAgFnSc4d1	herecká
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Il	Il	k1gMnSc1	Il
cicisbeo	cicisbeo	k1gMnSc1	cicisbeo
consolato	consolato	k6eAd1	consolato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1728	[number]	k4	1728
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
vychovatelkou	vychovatelka	k1gFnSc7	vychovatelka
stala	stát	k5eAaPmAgFnS	stát
říšská	říšský	k2eAgFnSc1d1	říšská
hraběnka	hraběnka	k1gFnSc1	hraběnka
Maria	Maria	k1gFnSc1	Maria
Karolina	Karolinum	k1gNnPc1	Karolinum
von	von	k1gInSc1	von
Fuchs-Mollard	Fuchs-Mollard	k1gInSc1	Fuchs-Mollard
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Charlotte	Charlott	k1gMnSc5	Charlott
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1754	[number]	k4	1754
<g/>
.	.	kIx.	.
</s>
<s>
Charlottu	Charlotta	k1gFnSc4	Charlotta
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
<g/>
,	,	kIx,	,
říkávala	říkávat	k5eAaImAgFnS	říkávat
jí	on	k3xPp3gFnSc2	on
"	"	kIx"	"
<g/>
mami	mami	k1gFnSc2	mami
<g/>
"	"	kIx"	"
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
jediného	jediný	k2eAgMnSc4d1	jediný
nečlena	nečlen	k1gMnSc4	nečlen
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
nechala	nechat	k5eAaPmAgFnS	nechat
pochovat	pochovat	k5eAaPmF	pochovat
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
Císařské	císařský	k2eAgFnSc2d1	císařská
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
leží	ležet	k5eAaImIp3nS	ležet
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
chotě	choť	k1gMnSc2	choť
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
jí	jíst	k5eAaImIp3nS	jíst
pomohla	pomoct	k5eAaPmAgFnS	pomoct
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Františkem	František	k1gMnSc7	František
Štěpánem	Štěpán	k1gMnSc7	Štěpán
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkala	setkat	k5eAaPmAgFnS	setkat
již	již	k6eAd1	již
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
Nancy	Nancy	k1gFnSc2	Nancy
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
viděl	vidět	k5eAaImAgMnS	vidět
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
mu	on	k3xPp3gMnSc3	on
osud	osud	k1gInSc1	osud
odepřel	odepřít	k5eAaPmAgInS	odepřít
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
Františkem	František	k1gMnSc7	František
Štěpánem	Štěpán	k1gMnSc7	Štěpán
byl	být	k5eAaImAgInS	být
věkový	věkový	k2eAgInSc1d1	věkový
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
:	:	kIx,	:
jí	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
šest	šest	k4xCc1	šest
<g/>
,	,	kIx,	,
Františkovi	František	k1gMnSc3	František
Štěpánovi	Štěpán	k1gMnSc3	Štěpán
patnáct	patnáct	k4xCc4	patnáct
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
byl	být	k5eAaImAgMnS	být
velký	velký	k2eAgMnSc1d1	velký
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
oslavách	oslava	k1gFnPc6	oslava
<g/>
,	,	kIx,	,
bálech	bál	k1gInPc6	bál
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
rozděloval	rozdělovat	k5eAaImAgInS	rozdělovat
i	i	k9	i
protokol	protokol	k1gInSc1	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
případné	případný	k2eAgNnSc4d1	případné
spojenectví	spojenectví	k1gNnSc4	spojenectví
mezi	mezi	k7c4	mezi
Habsburky	Habsburk	k1gMnPc4	Habsburk
a	a	k8xC	a
Lotrinčany	Lotrinčan	k1gMnPc4	Lotrinčan
by	by	kYmCp3nP	by
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
popudilo	popudit	k5eAaPmAgNnS	popudit
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
nenarodí	narodit	k5eNaPmIp3nP	narodit
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
také	také	k9	také
později	pozdě	k6eAd2	pozdě
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
)	)	kIx)	)
mužský	mužský	k2eAgMnSc1d1	mužský
následník	následník	k1gMnSc1	následník
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
úvahách	úvaha	k1gFnPc6	úvaha
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
za	za	k7c4	za
koho	kdo	k3yQnSc4	kdo
provdá	provdat	k5eAaPmIp3nS	provdat
svou	svůj	k3xOyFgFnSc4	svůj
nejstarší	starý	k2eAgFnSc4d3	nejstarší
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
existoval	existovat	k5eAaImAgMnS	existovat
mužský	mužský	k2eAgMnSc1d1	mužský
následník	následník	k1gMnSc1	následník
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
přicházely	přicházet	k5eAaImAgInP	přicházet
sňatky	sňatek	k1gInPc1	sňatek
s	s	k7c7	s
wittelsbašským	wittelsbašský	k2eAgMnSc7d1	wittelsbašský
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
(	(	kIx(	(
<g/>
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
k	k	k7c3	k
Rakousku	Rakousko	k1gNnSc6	Rakousko
přidalo	přidat	k5eAaPmAgNnS	přidat
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
ale	ale	k9	ale
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
rozkol	rozkol	k1gInSc4	rozkol
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	s	k7c7	s
španělským	španělský	k2eAgMnSc7d1	španělský
následníkem	následník	k1gMnSc7	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Anglii	Anglie	k1gFnSc4	Anglie
zavázal	zavázat	k5eAaPmAgMnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
provdá	provdat	k5eAaPmIp3nS	provdat
pouze	pouze	k6eAd1	pouze
za	za	k7c4	za
prince	princ	k1gMnPc4	princ
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
rolí	role	k1gFnSc7	role
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zase	zase	k9	zase
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
Františkovi	František	k1gMnSc3	František
Štěpánovi	Štěpán	k1gMnSc3	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1729	[number]	k4	1729
navíc	navíc	k6eAd1	navíc
stal	stát	k5eAaPmAgInS	stát
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
lotrinským	lotrinský	k2eAgMnSc7d1	lotrinský
vévodou	vévoda	k1gMnSc7	vévoda
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
obávala	obávat	k5eAaImAgFnS	obávat
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
usnadnila	usnadnit	k5eAaPmAgFnS	usnadnit
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
polské	polský	k2eAgNnSc4d1	polské
následnictví	následnictví	k1gNnSc4	následnictví
v	v	k7c6	v
letech	let	k1gInPc6	let
1733	[number]	k4	1733
<g/>
–	–	k?	–
<g/>
1735	[number]	k4	1735
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bojovalo	bojovat	k5eAaImAgNnS	bojovat
Rakousko	Rakousko	k1gNnSc1	Rakousko
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
nakonec	nakonec	k6eAd1	nakonec
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc1	Rakousko
získalo	získat	k5eAaPmAgNnS	získat
Parmu	Parma	k1gFnSc4	Parma
a	a	k8xC	a
Piacenzu	Piacenza	k1gFnSc4	Piacenza
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Neapol	Neapol	k1gFnSc4	Neapol
a	a	k8xC	a
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
ztratil	ztratit	k5eAaPmAgMnS	ztratit
Lotrinsko	Lotrinsko	k1gNnSc4	Lotrinsko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ztrátě	ztráta	k1gFnSc3	ztráta
Lotrinska	Lotrinsko	k1gNnSc2	Lotrinsko
a	a	k8xC	a
uznání	uznání	k1gNnSc2	uznání
pragmatické	pragmatický	k2eAgFnSc2d1	pragmatická
sankce	sankce	k1gFnSc2	sankce
Francií	Francie	k1gFnPc2	Francie
již	již	k9	již
nic	nic	k3yNnSc1	nic
nestálo	stát	k5eNaImAgNnS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
svatbě	svatba	k1gFnSc6	svatba
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
udává	udávat	k5eAaImIp3nS	udávat
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc1	Terezie
do	do	k7c2	do
svého	svůj	k3xOyFgMnSc2	svůj
budoucího	budoucí	k2eAgMnSc2d1	budoucí
manžela	manžel	k1gMnSc2	manžel
zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ani	ani	k8xC	ani
odpor	odpor	k1gInSc4	odpor
z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
neočekával	očekávat	k5eNaImAgInS	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
požádal	požádat	k5eAaPmAgMnS	požádat
Marii	Maria	k1gFnSc4	Maria
Terezii	Terezie	k1gFnSc3	Terezie
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1736	[number]	k4	1736
<g/>
,	,	kIx,	,
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
byli	být	k5eAaImAgMnP	být
ale	ale	k9	ale
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
odloučeni	odloučen	k2eAgMnPc1d1	odloučen
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
František	František	k1gMnSc1	František
Štěpán	Štěpán	k1gMnSc1	Štěpán
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
etiketa	etiketa	k1gFnSc1	etiketa
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
byla	být	k5eAaImAgFnS	být
naplánována	naplánovat	k5eAaBmNgFnS	naplánovat
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1736	[number]	k4	1736
v	v	k7c6	v
Augustiniánském	augustiniánský	k2eAgInSc6d1	augustiniánský
kostele	kostel	k1gInSc6	kostel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
