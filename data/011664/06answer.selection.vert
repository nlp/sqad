<s>
Roku	rok	k1gInSc2	rok
1436	[number]	k4	1436
dodal	dodat	k5eAaPmAgMnS	dodat
oltář	oltář	k1gInSc4	oltář
a	a	k8xC	a
vitráže	vitráž	k1gFnPc4	vitráž
pro	pro	k7c4	pro
karmelitánský	karmelitánský	k2eAgInSc4d1	karmelitánský
kostel	kostel	k1gInSc4	kostel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
1443	[number]	k4	1443
pro	pro	k7c4	pro
Freisingského	Freisingský	k1gMnSc4	Freisingský
biskupa	biskup	k1gMnSc2	biskup
Nikodéma	Nikodém	k1gMnSc2	Nikodém
della	dell	k1gMnSc2	dell
Scala	scát	k5eAaImAgFnS	scát
dodal	dodat	k5eAaPmAgInS	dodat
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
<g/>
,	,	kIx,	,
1449	[number]	k4	1449
oltář	oltář	k1gInSc1	oltář
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nezvěstný	zvěstný	k2eNgInSc1d1	nezvěstný
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
<g/>
.	.	kIx.	.
</s>
