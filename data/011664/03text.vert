<p>
<s>
Jakob	Jakob	k1gMnSc1	Jakob
Kaschauer	Kaschauer	k1gMnSc1	Kaschauer
<g/>
,	,	kIx,	,
též	též	k9	též
Jacob	Jacoba	k1gFnPc2	Jacoba
von	von	k1gInSc1	von
Kaschau	Kaschaus	k1gInSc2	Kaschaus
a	a	k8xC	a
Jakub	Jakub	k1gMnSc1	Jakub
z	z	k7c2	z
Košic	Košice	k1gInPc2	Košice
(	(	kIx(	(
<g/>
*	*	kIx~	*
kolem	kolem	k7c2	kolem
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc1	Košice
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Kaschau	Kaschaa	k1gFnSc4	Kaschaa
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1463	[number]	k4	1463
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgMnSc1d1	gotický
řezbář	řezbář	k1gMnSc1	řezbář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
školení	školení	k1gNnSc6	školení
nejsou	být	k5eNaImIp3nP	být
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
uváděn	uváděn	k2eAgInSc1d1	uváděn
roku	rok	k1gInSc2	rok
1429	[number]	k4	1429
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
řídil	řídit	k5eAaImAgMnS	řídit
svatoštěpánskou	svatoštěpánský	k2eAgFnSc4d1	Svatoštěpánská
stavební	stavební	k2eAgFnSc4d1	stavební
huť	huť	k1gFnSc4	huť
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dílna	dílna	k1gFnSc1	dílna
dodala	dodat	k5eAaPmAgFnS	dodat
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
sochu	socha	k1gFnSc4	socha
a	a	k8xC	a
vitráž	vitráž	k1gFnSc4	vitráž
pro	pro	k7c4	pro
dóm	dóm	k1gInSc4	dóm
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1433	[number]	k4	1433
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
dům	dům	k1gInSc1	dům
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1456	[number]	k4	1456
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
dokladů	doklad	k1gInPc2	doklad
o	o	k7c6	o
zakoupení	zakoupení	k1gNnSc6	zakoupení
a	a	k8xC	a
prodeji	prodej	k1gInSc6	prodej
domu	dům	k1gInSc2	dům
Jakobem	Jakob	k1gMnSc7	Jakob
Kaschauerem	Kaschauer	k1gMnSc7	Kaschauer
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1454	[number]	k4	1454
a	a	k8xC	a
1458	[number]	k4	1458
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k8xC	jako
hlava	hlava	k1gFnSc1	hlava
cechu	cech	k1gInSc2	cech
"	"	kIx"	"
<g/>
Bratrstva	bratrstvo	k1gNnSc2	bratrstvo
Božího	boží	k2eAgNnSc2d1	boží
těla	tělo	k1gNnSc2	tělo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Gottsleichnamsbruderschaft	Gottsleichnamsbruderschaft	k1gInSc1	Gottsleichnamsbruderschaft
<g/>
)	)	kIx)	)
při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1436	[number]	k4	1436
dodal	dodat	k5eAaPmAgMnS	dodat
oltář	oltář	k1gInSc4	oltář
a	a	k8xC	a
vitráže	vitráž	k1gFnPc4	vitráž
pro	pro	k7c4	pro
karmelitánský	karmelitánský	k2eAgInSc4d1	karmelitánský
kostel	kostel	k1gInSc4	kostel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
1443	[number]	k4	1443
pro	pro	k7c4	pro
Freisingského	Freisingský	k1gMnSc4	Freisingský
biskupa	biskup	k1gMnSc2	biskup
Nikodéma	Nikodém	k1gMnSc2	Nikodém
della	dell	k1gMnSc2	dell
Scala	scát	k5eAaImAgFnS	scát
dodal	dodat	k5eAaPmAgInS	dodat
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
<g/>
,	,	kIx,	,
1449	[number]	k4	1449
oltář	oltář	k1gInSc1	oltář
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
nezvěstný	zvěstný	k2eNgInSc1d1	nezvěstný
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určení	určení	k1gNnSc6	určení
jeho	jeho	k3xOp3gFnSc2	jeho
identity	identita	k1gFnSc2	identita
panují	panovat	k5eAaImIp3nP	panovat
nejasnosti	nejasnost	k1gFnPc1	nejasnost
-	-	kIx~	-
v	v	k7c6	v
dobových	dobový	k2eAgInPc6d1	dobový
dokumentech	dokument	k1gInPc6	dokument
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
dílna	dílna	k1gFnSc1	dílna
dodávala	dodávat	k5eAaImAgFnS	dodávat
především	především	k9	především
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
vyřezávané	vyřezávaný	k2eAgInPc1d1	vyřezávaný
oltáře	oltář	k1gInPc1	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgNnPc2d1	poslední
zjištění	zjištění	k1gNnPc2	zjištění
ho	on	k3xPp3gInSc4	on
nelze	lze	k6eNd1	lze
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
mistrem	mistr	k1gMnSc7	mistr
označovaným	označovaný	k2eAgNnSc7d1	označované
"	"	kIx"	"
<g/>
Meister	Meister	k1gInSc1	Meister
des	des	k1gNnSc1	des
Albrechtsaltars	Albrechtsaltars	k1gInSc1	Albrechtsaltars
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Kaschauer	Kaschauer	k1gMnSc1	Kaschauer
pracoval	pracovat	k5eAaImAgMnS	pracovat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sochaři	sochař	k1gMnPc7	sochař
Peterem	Peter	k1gMnSc7	Peter
von	von	k1gInSc4	von
Pusica	Pusicum	k1gNnSc2	Pusicum
a	a	k8xC	a
Nicolausem	Nicolaus	k1gMnSc7	Nicolaus
Gerhaertem	Gerhaert	k1gMnSc7	Gerhaert
van	van	k1gInSc4	van
Leyden	Leydna	k1gFnPc2	Leydna
pro	pro	k7c4	pro
rakousko	rakousko	k1gNnSc4	rakousko
-	-	kIx~	-
německého	německý	k2eAgMnSc2d1	německý
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
1440	[number]	k4	1440
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1452	[number]	k4	1452
císaře	císař	k1gMnSc2	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
Friedricha	Friedrich	k1gMnSc2	Friedrich
III	III	kA	III
<g/>
..	..	k?	..
Byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Margaretou	Margareta	k1gFnSc7	Margareta
Hirschvogel	Hirschvogela	k1gFnPc2	Hirschvogela
z	z	k7c2	z
Norimberka	Norimberk	k1gInSc2	Norimberk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
náměstí	náměstí	k1gNnSc1	náměstí
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Donaustadtu	Donaustadt	k1gInSc2	Donaustadt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Kaschauerova	Kaschauerův	k2eAgFnSc1d1	Kaschauerův
postava	postava	k1gFnSc1	postava
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
Freisingu	Freising	k1gInSc2	Freising
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Multscherovou	Multscherův	k2eAgFnSc7d1	Multscherův
sochou	socha	k1gFnSc7	socha
Boží	boží	k2eAgFnSc2d1	boží
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
Landsbergu	Landsberg	k1gInSc6	Landsberg
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
doby	doba	k1gFnSc2	doba
ke	k	k7c3	k
klíčovým	klíčový	k2eAgInPc3d1	klíčový
dílům	díl	k1gInPc3	díl
německého	německý	k2eAgNnSc2d1	německé
sochařství	sochařství	k1gNnSc2	sochařství
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1440	[number]	k4	1440
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
styl	styl	k1gInSc1	styl
řezbářské	řezbářský	k2eAgFnSc2d1	řezbářská
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
patrný	patrný	k2eAgInSc1d1	patrný
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
objemu	objem	k1gInSc2	objem
sochy	socha	k1gFnSc2	socha
<g/>
,	,	kIx,	,
pojednání	pojednání	k1gNnSc1	pojednání
drapérie	drapérie	k1gFnSc2	drapérie
a	a	k8xC	a
posunu	posun	k1gInSc2	posun
k	k	k7c3	k
monumentalitě	monumentalita	k1gFnSc3	monumentalita
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
holandským	holandský	k2eAgNnSc7d1	holandské
uměním	umění	k1gNnSc7	umění
Roberta	Robert	k1gMnSc2	Robert
Campina	Campin	k2eAgMnSc2d1	Campin
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mistra	mistr	k1gMnSc2	mistr
z	z	k7c2	z
Flémale	Flémala	k1gFnSc6	Flémala
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
Kaschauerovy	Kaschauerův	k2eAgFnPc1d1	Kaschauerův
práce	práce	k1gFnPc1	práce
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
zjevně	zjevně	k6eAd1	zjevně
pod	pod	k7c7	pod
přímým	přímý	k2eAgInSc7d1	přímý
vlivem	vliv	k1gInSc7	vliv
Hanse	Hans	k1gMnSc2	Hans
Multschera	Multscher	k1gMnSc2	Multscher
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
panny	panna	k1gFnSc2	panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
sv.	sv.	kA	sv.
Korbiniana	Korbiniana	k1gFnSc1	Korbiniana
z	z	k7c2	z
vyřezávaného	vyřezávaný	k2eAgInSc2d1	vyřezávaný
oltáře	oltář	k1gInSc2	oltář
ve	v	k7c6	v
Freisingu	Freising	k1gInSc6	Freising
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
v	v	k7c4	v
Bayerisches	Bayerisches	k1gInSc4	Bayerisches
Nationalmuseum	Nationalmuseum	k1gNnSc4	Nationalmuseum
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Homolka	Homolka	k1gMnSc1	Homolka
dává	dávat	k5eAaImIp3nS	dávat
Kaschauerovu	Kaschauerův	k2eAgFnSc4d1	Kaschauerův
tvorbu	tvorba	k1gFnSc4	tvorba
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
Mistra	mistr	k1gMnSc2	mistr
ukřižování	ukřižování	k1gNnSc2	ukřižování
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Kutal	kutat	k5eAaImAgMnS	kutat
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
Mistra	mistr	k1gMnSc2	mistr
suchdolské	suchdolský	k2eAgFnSc2d1	suchdolská
Barbory	Barbora	k1gFnSc2	Barbora
</s>
</p>
<p>
<s>
===	===	k?	===
Známá	známý	k2eAgNnPc1d1	známé
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
1436	[number]	k4	1436
Albrecht	Albrecht	k1gMnSc1	Albrecht
Altar	Altar	k1gMnSc1	Altar
<g/>
,	,	kIx,	,
Hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
karmelitánského	karmelitánský	k2eAgInSc2d1	karmelitánský
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
vitráže	vitráž	k1gFnSc2	vitráž
<g/>
,	,	kIx,	,
Am	Am	k1gFnSc1	Am
Hof	Hof	k1gFnSc1	Hof
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
</s>
</p>
<p>
<s>
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
kostela	kostel	k1gInSc2	kostel
ve	v	k7c6	v
Freisingu	Freising	k1gInSc6	Freising
</s>
</p>
<p>
<s>
1445-49	[number]	k4	1445-49
hlavní	hlavní	k2eAgInSc1d1	hlavní
oltář	oltář	k1gInSc1	oltář
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
(	(	kIx(	(
<g/>
Michaelerkirche	Michaelerkirche	k1gInSc1	Michaelerkirche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
</s>
</p>
<p>
<s>
fragment	fragment	k1gInSc1	fragment
Korunování	korunování	k1gNnSc2	korunování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
Bode	bůst	k5eAaImIp3nS	bůst
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
</s>
</p>
<p>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Anna	Anna	k1gFnSc1	Anna
samotřetí	samotřetí	k1gNnSc2	samotřetí
<g/>
,	,	kIx,	,
Wallfahrtskirche	Wallfahrtskirch	k1gInSc2	Wallfahrtskirch
Annaberg	Annaberg	k1gInSc1	Annaberg
u	u	k7c2	u
Sankt	Sankt	k1gInSc1	Sankt
Pölten	Pölten	k2eAgInSc4d1	Pölten
</s>
</p>
<p>
<s>
Madona	Madona	k1gFnSc1	Madona
na	na	k7c6	na
půlměsíci	půlměsíc	k1gInSc6	půlměsíc
<g/>
,	,	kIx,	,
soukromá	soukromý	k2eAgFnSc1d1	soukromá
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
</s>
</p>
<p>
<s>
kamenná	kamenný	k2eAgFnSc1d1	kamenná
socha	socha	k1gFnSc1	socha
vévody	vévoda	k1gMnSc2	vévoda
Fridricha	Fridrich	k1gMnSc2	Fridrich
V.	V.	kA	V.
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
(	(	kIx(	(
<g/>
před	před	k7c7	před
1452	[number]	k4	1452
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kamenná	kamenný	k2eAgFnSc1d1	kamenná
socha	socha	k1gFnSc1	socha
Madony	Madona	k1gFnSc2	Madona
<g/>
,	,	kIx,	,
Württembergischen	Württembergischen	k2eAgInSc1d1	Württembergischen
Landesmuseum	Landesmuseum	k1gInSc1	Landesmuseum
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
</s>
</p>
<p>
<s>
socha	socha	k1gFnSc1	socha
Madony	Madona	k1gFnSc2	Madona
na	na	k7c6	na
kašně	kašna	k1gFnSc6	kašna
<g/>
,	,	kIx,	,
Mariazell	Mariazell	k1gInSc1	Mariazell
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dílna	dílna	k1gFnSc1	dílna
J.	J.	kA	J.
Kaschauera	Kaschauera	k1gFnSc1	Kaschauera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
trůnící	trůnící	k2eAgMnPc1d1	trůnící
sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
jako	jako	k8xS	jako
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
Museum	museum	k1gNnSc1	museum
Mittelalterlicher	Mittelalterlichra	k1gFnPc2	Mittelalterlichra
Österreichischer	Österreichischra	k1gFnPc2	Österreichischra
Kunst	Kunst	k1gInSc1	Kunst
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dílna	dílna	k1gFnSc1	dílna
J.	J.	kA	J.
Kaschauera	Kaschauera	k1gFnSc1	Kaschauera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
boží	boží	k2eAgFnSc1d1	boží
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Purgstall	Purgstall	k1gMnSc1	Purgstall
(	(	kIx(	(
<g/>
Niederösterreich	Niederösterreich	k1gMnSc1	Niederösterreich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
byl	být	k5eAaImAgInS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
v	v	k7c6	v
Deutsche	Deutsche	k1gNnSc6	Deutsche
Biographie	Biographie	k1gFnSc2	Biographie
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
citacím	citace	k1gFnPc3	citace
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jakob	Jakob	k1gMnSc1	Jakob
Kaschauer	Kaschauer	k1gMnSc1	Kaschauer
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Schädler	Schädler	k1gMnSc1	Schädler
<g/>
:	:	kIx,	:
Kaschauer	Kaschauer	k1gMnSc1	Kaschauer
<g/>
,	,	kIx,	,
Jakob	Jakob	k1gMnSc1	Jakob
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Neue	Neue	k1gFnSc1	Neue
Deutsche	Deutsche	k1gFnSc1	Deutsche
Biographie	Biographie	k1gFnSc1	Biographie
(	(	kIx(	(
<g/>
NDB	NDB	kA	NDB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Band	band	k1gInSc1	band
11	[number]	k4	11
<g/>
,	,	kIx,	,
Duncker	Duncker	k1gInSc1	Duncker
&	&	k?	&
Humblot	Humblota	k1gFnPc2	Humblota
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
428	[number]	k4	428
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
192	[number]	k4	192
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
S.	S.	kA	S.
310	[number]	k4	310
<g/>
–	–	k?	–
<g/>
312	[number]	k4	312
</s>
</p>
<p>
<s>
E.	E.	kA	E.
Baum	Baum	k1gInSc1	Baum
<g/>
,	,	kIx,	,
Katalog	katalog	k1gInSc1	katalog
des	des	k1gNnSc1	des
Museums	Museumsa	k1gFnPc2	Museumsa
mittelalterlicher	mittelalterlichra	k1gFnPc2	mittelalterlichra
österreichischer	österreichischra	k1gFnPc2	österreichischra
Kunst	Kunst	k1gInSc1	Kunst
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jakob	Jakob	k1gMnSc1	Jakob
Kaschauer	Kaschaura	k1gFnPc2	Kaschaura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Deutsche	Deutsche	k1gFnSc1	Deutsche
Biographie	Biographie	k1gFnSc1	Biographie
<g/>
:	:	kIx,	:
Kaschauer	Kaschauer	k1gMnSc1	Kaschauer
<g/>
,	,	kIx,	,
Jakob	Jakob	k1gMnSc1	Jakob
</s>
</p>
<p>
<s>
Wien	Wien	k1gMnSc1	Wien
Geschichte	Geschicht	k1gMnSc5	Geschicht
Wiki	Wik	k1gMnSc5	Wik
<g/>
:	:	kIx,	:
Jakob	Jakob	k1gMnSc1	Jakob
Kaschauer	Kaschauer	k1gMnSc1	Kaschauer
</s>
</p>
