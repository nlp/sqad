<s>
Kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnSc2d1
</s>
<s>
Kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnSc2d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
plazi	plaz	k1gMnPc1
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
krokodýli	krokodýl	k1gMnPc1
(	(	kIx(
<g/>
Crocodilia	Crocodilia	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
aligátorovití	aligátorovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Alligatoridae	Alligatoridae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
kajman	kajman	k1gMnSc1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Caiman	Caiman	k1gMnSc1
yacareDaudin	yacareDaudina	k1gFnPc2
<g/>
,	,	kIx,
1802	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
kajmana	kajman	k1gMnSc2
yakaré	yakarý	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
zelená	zelenat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
kajmana	kajman	k1gMnSc2
yakaré	yakarý	k2eAgFnSc2d1
<g/>
(	(	kIx(
<g/>
zelená	zelenat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Caiman	Caiman	k1gMnSc1
yacare	yacar	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
také	také	k9
jako	jako	k9
kajman	kajman	k1gMnSc1
brýlový	brýlový	k2eAgMnSc1d1
paraguayský	paraguayský	k2eAgMnSc1d1
nebo	nebo	k8xC
kajman	kajman	k1gMnSc1
žakaré	žakarý	k2eAgFnSc2d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
kajmana	kajman	k1gMnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
aligátorovitých	aligátorovitý	k2eAgMnPc2d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
v	v	k7c6
hojných	hojný	k2eAgInPc6d1
počtech	počet	k1gInPc6
obývá	obývat	k5eAaImIp3nS
část	část	k1gFnSc1
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
Bolívie	Bolívie	k1gFnSc2
<g/>
,	,	kIx,
Argentiny	Argentina	k1gFnSc2
a	a	k8xC
Paraguaye	Paraguay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpočetnější	početní	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
populace	populace	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
Pantanalu	Pantanal	k1gInSc2
a	a	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
zde	zde	k6eAd1
okolo	okolo	k7c2
10	#num#	k4
milionů	milion	k4xCgInPc2
jedinců	jedinec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
vede	vést	k5eAaImIp3nS
tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
jako	jako	k9
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
</s>
<s>
François	François	k1gFnSc1
Marie	Maria	k1gFnSc2
Daudin	Daudina	k1gFnPc2
ho	on	k3xPp3gMnSc4
popsal	popsat	k5eAaPmAgMnS
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1802	#num#	k4
jako	jako	k8xC,k8xS
Crocodilus	Crocodilus	k1gMnSc1
yacare	yacar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhové	druhový	k2eAgFnPc4d1
jméno	jméno	k1gNnSc4
yacare	yacar	k1gMnSc5
<g/>
,	,	kIx,
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
slova	slovo	k1gNnSc2
jacaré	jacarý	k2eAgInPc4d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
v	v	k7c6
portugalštině	portugalština	k1gFnSc6
"	"	kIx"
<g/>
aligátor	aligátor	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
byl	být	k5eAaImAgInS
považován	považován	k2eAgInSc1d1
za	za	k7c4
poddruh	poddruh	k1gInSc4
kajmana	kajman	k1gMnSc2
brýlového	brýlový	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
druhy	druh	k1gInPc4
jsou	být	k5eAaImIp3nP
vzhledově	vzhledově	k6eAd1
podobní	podobný	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
mají	mít	k5eAaImIp3nP
oddělené	oddělený	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
výskytu	výskyt	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
samostatné	samostatný	k2eAgInPc4d1
taxony	taxon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnSc2d1
je	být	k5eAaImIp3nS
středně	středně	k6eAd1
velký	velký	k2eAgMnSc1d1
krokodýl	krokodýl	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
žluto-hnědou	žluto-hnědý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
dorůst	dorůst	k5eAaPmF
2,5	2,5	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
výjimečně	výjimečně	k6eAd1
až	až	k9
3	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
váhy	váha	k1gFnPc4
maximálně	maximálně	k6eAd1
58	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Samice	samice	k1gFnPc1
jsou	být	k5eAaImIp3nP
menší	malý	k2eAgInSc4d2
než	než	k8xS
samci	samec	k1gMnPc1
a	a	k8xC
dosahují	dosahovat	k5eAaImIp3nP
délky	délka	k1gFnPc4
kolem	kolem	k7c2
1,4	1,4	k4
metru	metr	k1gInSc2
a	a	k8xC
váhy	váha	k1gFnSc2
okolo	okolo	k7c2
20	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s>
Ekologie	ekologie	k1gFnSc1
a	a	k8xC
chování	chování	k1gNnSc1
</s>
<s>
Kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnSc2d1
obývá	obývat	k5eAaImIp3nS
vnitrozemské	vnitrozemský	k2eAgFnSc2d1
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
jezera	jezero	k1gNnPc1
<g/>
,	,	kIx,
mokřady	mokřad	k1gInPc1
a	a	k8xC
řeky	řeka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Životem	život	k1gInSc7
se	se	k3xPyFc4
velmi	velmi	k6eAd1
podobá	podobat	k5eAaImIp3nS
kajmanu	kajman	k1gMnSc3
brýlovému	brýlový	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
jídelníček	jídelníček	k1gInSc4
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
vodních	vodní	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
ryby	ryba	k1gFnPc1
a	a	k8xC
měkkýši	měkkýš	k1gMnPc1
(	(	kIx(
<g/>
především	především	k9
hlemýždi	hlemýžď	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
hadi	had	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loví	lovit	k5eAaImIp3nS
i	i	k9
kapybary	kapybara	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
probíhá	probíhat	k5eAaImIp3nS
mezi	mezi	k7c7
prosincem	prosinec	k1gInSc7
a	a	k8xC
únorem	únor	k1gInSc7
<g/>
,	,	kIx,
uprostřed	uprostřed	k7c2
období	období	k1gNnSc2
dešťů	dešť	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnPc1
budují	budovat	k5eAaImIp3nP
hnízda	hnízdo	k1gNnPc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
kopečků	kopeček	k1gInPc2
<g/>
,	,	kIx,
používají	používat	k5eAaImIp3nP
při	při	k7c6
tom	ten	k3xDgNnSc6
bláto	bláto	k1gNnSc1
a	a	k8xC
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
vegetaci	vegetace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
nich	on	k3xPp3gMnPc2
kladou	klást	k5eAaImIp3nP
22	#num#	k4
až	až	k9
35	#num#	k4
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
až	až	k9
44	#num#	k4
vajec	vejce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
se	se	k3xPyFc4
o	o	k7c4
hnízdo	hnízdo	k1gNnSc4
s	s	k7c7
vejci	vejce	k1gNnPc7
starají	starat	k5eAaImIp3nP
<g/>
,	,	kIx,
po	po	k7c6
vylíhnutí	vylíhnutí	k1gNnSc6
jsou	být	k5eAaImIp3nP
nicméně	nicméně	k8xC
mláďata	mládě	k1gNnPc4
ponechána	ponechán	k2eAgNnPc4d1
svému	svůj	k3xOyFgInSc3
osudu	osud	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepřáteli	nepřítel	k1gMnPc7
malých	malý	k2eAgMnPc2d1
kajmanů	kajman	k1gMnPc2
jsou	být	k5eAaImIp3nP
především	především	k9
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
volavky	volavka	k1gFnPc1
<g/>
,	,	kIx,
čápi	čáp	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohlavně	pohlavně	k6eAd1
dospělými	dospělí	k1gMnPc7
se	se	k3xPyFc4
stávají	stávat	k5eAaImIp3nP
po	po	k7c6
dosažení	dosažení	k1gNnSc6
věku	věk	k1gInSc2
10	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
doba	doba	k1gFnSc1
dožití	dožití	k1gNnSc2
se	se	k3xPyFc4
zřejmě	zřejmě	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
50	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Kajmani	kajman	k1gMnPc1
yakaré	yakarý	k2eAgFnSc2d1
jsou	být	k5eAaImIp3nP
oblíbenou	oblíbený	k2eAgFnSc7d1
kořistí	kořist	k1gFnSc7
anakondy	anakonda	k1gFnSc2
a	a	k8xC
jaguára	jaguár	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hrozby	hrozba	k1gFnPc1
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
byli	být	k5eAaImAgMnP
kajmani	kajman	k1gMnPc1
v	v	k7c6
obrovských	obrovský	k2eAgInPc6d1
počtech	počet	k1gInPc6
loveni	lovit	k5eAaImNgMnP
především	především	k9
pro	pro	k7c4
jejich	jejich	k3xOp3gFnSc4
kůži	kůže	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgInS
nicméně	nicméně	k8xC
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
schválen	schválit	k5eAaPmNgInS
zákaz	zákaz	k1gInSc1
lovu	lov	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
s	s	k7c7
kůžemi	kůže	k1gFnPc7
a	a	k8xC
stavy	stav	k1gInPc7
těchto	tento	k3xDgInPc2
plazů	plaz	k1gInPc2
začaly	začít	k5eAaPmAgFnP
rychle	rychle	k6eAd1
vzrůstat	vzrůstat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInPc1
počty	počet	k1gInPc1
odhadují	odhadovat	k5eAaImIp3nP
na	na	k7c4
10	#num#	k4
milionů	milion	k4xCgInPc2
jedinců	jedinec	k1gMnPc2
jen	jen	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
Pantanalu	Pantanal	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnSc2d1
v	v	k7c6
brazilském	brazilský	k2eAgInSc6d1
Pantanalu	Pantanal	k1gInSc6
</s>
<s>
V	v	k7c6
brazilské	brazilský	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
ve	v	k7c6
městě	město	k1gNnSc6
Cuiabá	Cuiabý	k2eAgFnSc1d1
</s>
<s>
U	u	k7c2
brazilské	brazilský	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
Sã	Sã	k1gMnSc1
Lourenço	Lourenço	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Yacare	Yacar	k1gMnSc5
caiman	caiman	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
https://www.biolib.cz/cz/taxon/id184066/	https://www.biolib.cz/cz/taxon/id184066/	k4
<g/>
↑	↑	k?
Yacaré	Yacarý	k2eAgFnSc2d1
Caiman	Caiman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Crocodiles	Crocodiles	k1gMnSc1
Of	Of	k1gMnSc1
The	The	k1gMnSc1
World	World	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Brazil	Brazil	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Yacare	Yacar	k1gMnSc5
Caiman	Caiman	k1gMnSc1
-	-	kIx~
The	The	k1gMnSc1
Comeback	Comeback	k1gMnSc1
Croc	Croc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
Magazine	Magazin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-07-01	2013-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Igor	Igor	k1gMnSc1
J.	J.	kA
Roberto	Roberta	k1gFnSc5
<g/>
,	,	kIx,
Pedro	Pedra	k1gFnSc5
S.	S.	kA
Bittencourt	Bittencourt	k1gInSc1
<g/>
,	,	kIx,
Fabio	Fabio	k1gMnSc1
L.	L.	kA
Muniz	Muniz	k1gMnSc1
<g/>
,	,	kIx,
Sandra	Sandra	k1gFnSc1
M.	M.	kA
Hernández-Rangel	Hernández-Rangela	k1gFnPc2
<g/>
,	,	kIx,
Yhuri	Yhur	k1gFnPc1
C.	C.	kA
Nóbrega	Nóbreg	k1gMnSc4
<g/>
,	,	kIx,
Robson	Robson	k1gMnSc1
W.	W.	kA
Ávila	Ávila	k1gMnSc1
<g/>
,	,	kIx,
Bruno	Bruno	k1gMnSc1
C.	C.	kA
Souza	Souza	k1gFnSc1
<g/>
,	,	kIx,
Gustavo	Gustava	k1gFnSc5
Alvarez	Alvarez	k1gInSc4
<g/>
,	,	kIx,
Guido	Guido	k1gNnSc4
Miranda-Chumacero	Miranda-Chumacero	k1gNnSc1
<g/>
,	,	kIx,
Zilca	Zilca	k1gMnSc1
Campos	Campos	k1gMnSc1
<g/>
,	,	kIx,
Izeni	Izeen	k2eAgMnPc1d1
P.	P.	kA
Farias	Farias	k1gMnSc1
&	&	k?
Tomas	Tomas	k1gMnSc1
Hrbek	Hrbek	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unexpected	Unexpected	k1gInSc1
but	but	k?
unsurprising	unsurprising	k1gInSc1
lineage	lineagat	k5eAaPmIp3nS
diversity	diversit	k1gInPc4
within	withina	k1gFnPc2
the	the	k?
most	most	k1gInSc1
widespread	widespread	k1gInSc1
Neotropical	Neotropical	k1gFnSc1
crocodilian	crocodilian	k1gMnSc1
genus	genus	k1gMnSc1
Caiman	Caiman	k1gMnSc1
(	(	kIx(
<g/>
Crocodylia	Crocodylia	k1gFnSc1
<g/>
,	,	kIx,
Alligatoridae	Alligatoridae	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systematics	Systematics	k1gInSc4
and	and	k?
Biodiversity	Biodiversit	k1gInPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1080/14772000.2020.1769222	https://doi.org/10.1080/14772000.2020.1769222	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kajman	kajman	k1gMnSc1
yakaré	yakarý	k2eAgFnPc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Caiman	Caiman	k1gMnSc1
yacare	yacar	k1gMnSc5
ve	v	k7c6
Wikidruzích	Wikidruh	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
