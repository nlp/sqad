<p>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
,	,	kIx,	,
též	též	k9	též
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Jeanne	Jeann	k1gInSc5	Jeann
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arc	Arc	k1gMnSc1	Arc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Panna	Panna	k1gFnSc1	Panna
orléanská	orléanský	k2eAgFnSc1d1	orléanská
(	(	kIx(	(
<g/>
La	la	k1gNnSc7	la
Pucelle	Pucelle	k1gFnSc2	Pucelle
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orléans	Orléans	k1gInSc1	Orléans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Panna	Panna	k1gFnSc1	Panna
Jana	Jana	k1gFnSc1	Jana
(	(	kIx(	(
<g/>
Jeanne	Jeann	k1gInSc5	Jeann
la	la	k1gNnPc7	la
Pucelle	Pucelle	k1gNnSc1	Pucelle
<g/>
)	)	kIx)	)
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
Lotrinka	Lotrinka	k1gFnSc1	Lotrinka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
bonne	bonnout	k5eAaPmIp3nS	bonnout
Lorraine	Lorrain	k1gInSc5	Lorrain
<g/>
)	)	kIx)	)
či	či	k8xC	či
jenom	jenom	k9	jenom
Panna	Panna	k1gFnSc1	Panna
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Pucelle	Pucelle	k1gFnSc2	Pucelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1412	[number]	k4	1412
Domrémy-la-Pucelle	Domrémya-Pucelle	k1gFnSc2	Domrémy-la-Pucelle
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1431	[number]	k4	1431
Rouen	Rouna	k1gFnPc2	Rouna
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
hrdinka	hrdinka	k1gFnSc1	hrdinka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanula	stanout	k5eAaPmAgFnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
francouzských	francouzský	k2eAgNnPc2d1	francouzské
vojsk	vojsko	k1gNnPc2	vojsko
za	za	k7c2	za
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Angličanům	Angličan	k1gMnPc3	Angličan
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byla	být	k5eAaImAgFnS	být
upálena	upálit	k5eAaPmNgFnS	upálit
za	za	k7c4	za
kacířství	kacířství	k1gNnSc4	kacířství
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
ospravedlnění	ospravedlnění	k1gNnSc6	ospravedlnění
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
mučednicí	mučednice	k1gFnSc7	mučednice
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
blahořečena	blahořečen	k2eAgFnSc1d1	blahořečena
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
za	za	k7c4	za
svatou	svatý	k2eAgFnSc4d1	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spolupatronkou	spolupatronka	k1gFnSc7	spolupatronka
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
patrně	patrně	k6eAd1	patrně
roku	rok	k1gInSc2	rok
1412	[number]	k4	1412
v	v	k7c6	v
zámožné	zámožný	k2eAgFnSc6d1	zámožná
selské	selský	k2eAgFnSc6d1	selská
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
lotrinské	lotrinský	k2eAgFnSc6d1	lotrinská
vesnici	vesnice	k1gFnSc6	vesnice
Domrémy	Domrém	k1gInPc4	Domrém
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Domrémy-la-Pucelle	Domrémya-Pucelle	k1gFnSc1	Domrémy-la-Pucelle
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
třetí	třetí	k4xOgFnSc1	třetí
etapa	etapa	k1gFnSc1	etapa
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
se	se	k3xPyFc4	se
vylodili	vylodit	k5eAaPmAgMnP	vylodit
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1415	[number]	k4	1415
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
u	u	k7c2	u
Azincourtu	Azincourt	k1gInSc2	Azincourt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
postupně	postupně	k6eAd1	postupně
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
severní	severní	k2eAgFnSc4d1	severní
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
podepsali	podepsat	k5eAaPmAgMnP	podepsat
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
V.	V.	kA	V.
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
smlouvu	smlouva	k1gFnSc4	smlouva
v	v	k7c4	v
Troyes	Troyes	k1gInSc4	Troyes
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
získal	získat	k5eAaPmAgMnS	získat
dědický	dědický	k2eAgInSc4d1	dědický
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
korunu	koruna	k1gFnSc4	koruna
po	po	k7c6	po
Karlově	Karlův	k2eAgFnSc6d1	Karlova
smrti	smrt	k1gFnSc6	smrt
anglický	anglický	k2eAgInSc1d1	anglický
panovnický	panovnický	k2eAgInSc1d1	panovnický
rod	rod	k1gInSc1	rod
Lancasterů	Lancaster	k1gMnPc2	Lancaster
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgInP	být
opominuty	opominut	k2eAgInPc1d1	opominut
dědické	dědický	k2eAgInPc1d1	dědický
nároky	nárok	k1gInPc1	nárok
dauphina	dauphin	k1gMnSc2	dauphin
Karla	Karel	k1gMnSc2	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
brzké	brzký	k2eAgFnSc6d1	brzká
smrti	smrt	k1gFnSc6	smrt
Karla	Karel	k1gMnSc2	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
†	†	k?	†
1422	[number]	k4	1422
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dědicem	dědic	k1gMnSc7	dědic
trůnu	trůn	k1gInSc2	trůn
stal	stát	k5eAaPmAgMnS	stát
nedávno	nedávno	k6eAd1	nedávno
narozený	narozený	k2eAgMnSc1d1	narozený
syn	syn	k1gMnSc1	syn
předčasně	předčasně	k6eAd1	předčasně
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
Jindřicha	Jindřich	k1gMnSc2	Jindřich
V.	V.	kA	V.
a	a	k8xC	a
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
dcery	dcera	k1gFnSc2	dcera
Kateřiny	Kateřina	k1gFnSc2	Kateřina
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Dauphin	dauphin	k1gMnSc1	dauphin
Karel	Karel	k1gMnSc1	Karel
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Loiry	Loira	k1gFnSc2	Loira
a	a	k8xC	a
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
široký	široký	k2eAgInSc4d1	široký
lidový	lidový	k2eAgInSc4d1	lidový
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
anglickým	anglický	k2eAgMnPc3d1	anglický
uchvatitelům	uchvatitel	k1gMnPc3	uchvatitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
14	[number]	k4	14
let	léto	k1gNnPc2	léto
slýchala	slýchat	k5eAaImAgFnS	slýchat
Johanka	Johanka	k1gFnSc1	Johanka
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
výpovědi	výpověď	k1gFnSc2	výpověď
hlasy	hlas	k1gInPc4	hlas
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
vidění	vidění	k1gNnSc4	vidění
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zjevovali	zjevovat	k5eAaImAgMnP	zjevovat
andělé	anděl	k1gMnPc1	anděl
a	a	k8xC	a
světci	světec	k1gMnPc1	světec
<g/>
,	,	kIx,	,
především	především	k9	především
archanděl	archanděl	k1gMnSc1	archanděl
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Kateřina	Kateřina	k1gFnSc1	Kateřina
a	a	k8xC	a
sv.	sv.	kA	sv.
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
modlila	modlit	k5eAaImAgFnS	modlit
za	za	k7c4	za
záchranu	záchrana	k1gFnSc4	záchrana
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žoldnéřská	žoldnéřský	k2eAgNnPc4d1	žoldnéřské
anglická	anglický	k2eAgNnPc4d1	anglické
vojska	vojsko	k1gNnPc4	vojsko
drancovala	drancovat	k5eAaImAgFnS	drancovat
a	a	k8xC	a
vypalovala	vypalovat	k5eAaImAgFnS	vypalovat
její	její	k3xOp3gInSc4	její
rodný	rodný	k2eAgInSc4d1	rodný
kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
se	se	k3xPyFc4	se
blížila	blížit	k5eAaImAgFnS	blížit
k	k	k7c3	k
Orleánsu	Orleáns	k1gInSc3	Orleáns
<g/>
.	.	kIx.	.
</s>
<s>
Údajné	údajný	k2eAgInPc4d1	údajný
hlasy	hlas	k1gInPc4	hlas
světců	světec	k1gMnPc2	světec
jí	jíst	k5eAaImIp3nS	jíst
prý	prý	k9	prý
sdělily	sdělit	k5eAaPmAgInP	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
Bohem	bůh	k1gMnSc7	bůh
vyvolena	vyvolen	k2eAgFnSc1d1	vyvolena
osvobodit	osvobodit	k5eAaPmF	osvobodit
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
a	a	k8xC	a
archanděl	archanděl	k1gMnSc1	archanděl
Michael	Michael	k1gMnSc1	Michael
ji	on	k3xPp3gFnSc4	on
nabádal	nabádat	k5eAaImAgMnS	nabádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vzala	vzít	k5eAaPmAgFnS	vzít
mužský	mužský	k2eAgInSc4d1	mužský
šat	šat	k1gInSc4	šat
a	a	k8xC	a
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
Orléans	Orléans	k1gInSc4	Orléans
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
těmito	tento	k3xDgInPc7	tento
hlasy	hlas	k1gInPc7	hlas
a	a	k8xC	a
díky	díky	k7c3	díky
síle	síla	k1gFnSc3	síla
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
,	,	kIx,	,
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
ve	v	k7c4	v
své	svůj	k3xOyFgNnSc4	svůj
poslání	poslání	k1gNnSc4	poslání
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
veřejně	veřejně	k6eAd1	veřejně
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
úmysly	úmysl	k1gInPc7	úmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
března	březen	k1gInSc2	březen
1429	[number]	k4	1429
vyjela	vyjet	k5eAaPmAgFnS	vyjet
se	se	k3xPyFc4	se
skromným	skromný	k2eAgInSc7d1	skromný
doprovodem	doprovod	k1gInSc7	doprovod
od	od	k7c2	od
guvernéra	guvernér	k1gMnSc2	guvernér
ve	v	k7c4	v
Vaucouleurs	Vaucouleurs	k1gInSc4	Vaucouleurs
za	za	k7c7	za
Karlem	Karel	k1gMnSc7	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
do	do	k7c2	do
Chinonu	chinon	k1gInSc2	chinon
a	a	k8xC	a
za	za	k7c4	za
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
ho	on	k3xPp3gMnSc4	on
přiměla	přimět	k5eAaPmAgFnS	přimět
svou	svůj	k3xOyFgFnSc7	svůj
přímostí	přímost	k1gFnSc7	přímost
a	a	k8xC	a
nezlomným	zlomný	k2eNgNnSc7d1	nezlomné
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
vyšším	vysoký	k2eAgNnSc6d2	vyšší
poslání	poslání	k1gNnSc6	poslání
k	k	k7c3	k
tažení	tažení	k1gNnSc3	tažení
do	do	k7c2	do
Orléansu	Orléans	k1gInSc2	Orléans
<g/>
.	.	kIx.	.
</s>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
dostala	dostat	k5eAaPmAgFnS	dostat
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
nechala	nechat	k5eAaPmAgFnS	nechat
si	se	k3xPyFc3	se
vyšít	vyšít	k5eAaPmF	vyšít
na	na	k7c4	na
korouhev	korouhev	k1gFnSc4	korouhev
květy	květ	k1gInPc4	květ
lilie	lilie	k1gFnSc2	lilie
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
Jesus	Jesus	k1gMnSc1	Jesus
Maria	Mario	k1gMnSc2	Mario
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vítězný	vítězný	k2eAgInSc1d1	vítězný
pochod	pochod	k1gInSc1	pochod
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1429	[number]	k4	1429
vjela	vjet	k5eAaPmAgFnS	vjet
do	do	k7c2	do
osvobozeného	osvobozený	k2eAgInSc2d1	osvobozený
Orléansu	Orléans	k1gInSc2	Orléans
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejprve	nejprve	k6eAd1	nejprve
musela	muset	k5eAaImAgFnS	muset
odrazit	odrazit	k5eAaPmF	odrazit
četné	četný	k2eAgInPc4d1	četný
útoky	útok	k1gInPc4	útok
a	a	k8xC	a
vyčistit	vyčistit	k5eAaPmF	vyčistit
okolí	okolí	k1gNnSc4	okolí
Orléansu	Orléans	k1gInSc2	Orléans
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgNnSc7d1	klíčové
vítězstvím	vítězství	k1gNnSc7	vítězství
pro	pro	k7c4	pro
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
dobytí	dobytí	k1gNnSc4	dobytí
pevnosti	pevnost	k1gFnSc2	pevnost
Tourelles	Tourellesa	k1gFnPc2	Tourellesa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
na	na	k7c4	na
města	město	k1gNnPc4	město
Jargeau	Jargeaus	k1gInSc2	Jargeaus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
obléhala	obléhat	k5eAaImAgFnS	obléhat
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
městu	město	k1gNnSc3	město
Meungu	Meung	k1gInSc2	Meung
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
obléháno	obléhat	k5eAaImNgNnS	obléhat
půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
<g/>
;	;	kIx,	;
až	až	k9	až
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
a	a	k8xC	a
útrpných	útrpný	k2eAgInPc6d1	útrpný
bojích	boj	k1gInPc6	boj
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
Johanka	Johanka	k1gFnSc1	Johanka
anglickou	anglický	k2eAgFnSc4d1	anglická
smetánku	smetánka	k1gFnSc4	smetánka
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
zkušenými	zkušený	k2eAgMnPc7d1	zkušený
anglickými	anglický	k2eAgMnPc7d1	anglický
veliteli	velitel	k1gMnPc7	velitel
<g/>
,	,	kIx,	,
a	a	k8xC	a
město	město	k1gNnSc1	město
nakonec	nakonec	k6eAd1	nakonec
padlo	padnout	k5eAaImAgNnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Johanku	Johanka	k1gFnSc4	Johanka
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
k	k	k7c3	k
Beaugency	Beaugenca	k1gFnPc1	Beaugenca
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
po	po	k7c6	po
dvouměsíčním	dvouměsíční	k2eAgNnSc6d1	dvouměsíční
obléhání	obléhání	k1gNnSc6	obléhání
padlo	padnout	k5eAaImAgNnS	padnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
poslední	poslední	k2eAgInSc4d1	poslední
velký	velký	k2eAgInSc4d1	velký
odpor	odpor	k1gInSc4	odpor
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Loiry	Loira	k1gFnSc2	Loira
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Johanka	Johanka	k1gFnSc1	Johanka
porazila	porazit	k5eAaPmAgFnS	porazit
anglickou	anglický	k2eAgFnSc4d1	anglická
armádu	armáda	k1gFnSc4	armáda
u	u	k7c2	u
Patay	Pataa	k1gFnSc2	Pataa
a	a	k8xC	a
odtáhla	odtáhnout	k5eAaPmAgFnS	odtáhnout
k	k	k7c3	k
Remeši	Remeš	k1gFnSc3	Remeš
<g/>
,	,	kIx,	,
korunovačnímu	korunovační	k2eAgNnSc3d1	korunovační
městu	město	k1gNnSc3	město
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
dobyla	dobýt	k5eAaPmAgFnS	dobýt
města	město	k1gNnSc2	město
Auxerre	Auxerr	k1gInSc5	Auxerr
<g/>
,	,	kIx,	,
Troyes	Troyes	k1gInSc1	Troyes
a	a	k8xC	a
jezerní	jezerní	k2eAgNnSc1d1	jezerní
město	město	k1gNnSc1	město
Chalons	Chalonsa	k1gFnPc2	Chalonsa
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
dauphin	dauphin	k1gMnSc1	dauphin
Karel	Karel	k1gMnSc1	Karel
v	v	k7c6	v
Remeši	Remeš	k1gFnSc6	Remeš
korunován	korunovat	k5eAaBmNgMnS	korunovat
na	na	k7c4	na
Karla	Karel	k1gMnSc4	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
současníků	současník	k1gMnPc2	současník
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
převahu	převaha	k1gFnSc4	převaha
nad	nad	k7c7	nad
svým	svůj	k3xOyFgMnSc7	svůj
anglickým	anglický	k2eAgMnSc7d1	anglický
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
,	,	kIx,	,
nezletilým	zletilý	k2eNgMnSc7d1	nezletilý
Jindřichem	Jindřich	k1gMnSc7	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zajetí	zajetí	k1gNnSc2	zajetí
===	===	k?	===
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
byla	být	k5eAaImAgFnS	být
rozhodnuta	rozhodnut	k2eAgFnSc1d1	rozhodnuta
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
až	až	k9	až
do	do	k7c2	do
vyhnání	vyhnání	k1gNnSc2	vyhnání
Angličanů	Angličan	k1gMnPc2	Angličan
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mocně	mocně	k6eAd1	mocně
působila	působit	k5eAaImAgFnS	působit
na	na	k7c4	na
vojáky	voják	k1gMnPc4	voják
i	i	k8xC	i
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
její	její	k3xOp3gFnPc4	její
popularity	popularita	k1gFnPc4	popularita
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jí	jíst	k5eAaImIp3nS	jíst
svěřoval	svěřovat	k5eAaImAgMnS	svěřovat
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
a	a	k8xC	a
méně	málo	k6eAd2	málo
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
1430	[number]	k4	1430
sice	sice	k8xC	sice
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
její	její	k3xOp3gInPc1	její
oddíly	oddíl	k1gInPc1	oddíl
na	na	k7c4	na
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
neměly	mít	k5eNaImAgFnP	mít
však	však	k9	však
bez	bez	k7c2	bez
královniny	královnin	k2eAgFnSc2d1	Královnina
podpory	podpora	k1gFnSc2	podpora
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obnovení	obnovení	k1gNnSc6	obnovení
válečných	válečný	k2eAgFnPc2d1	válečná
operací	operace	k1gFnPc2	operace
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1431	[number]	k4	1431
hájila	hájit	k5eAaImAgFnS	hájit
Jana	Jana	k1gFnSc1	Jana
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
vojenským	vojenský	k2eAgInSc7d1	vojenský
oddílem	oddíl	k1gInSc7	oddíl
proti	proti	k7c3	proti
anglickým	anglický	k2eAgInPc3d1	anglický
útokům	útok	k1gInPc3	útok
Compiegne	Compiegn	k1gInSc5	Compiegn
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
–	–	k?	–
patrně	patrně	k6eAd1	patrně
zradou	zrada	k1gFnSc7	zrada
–	–	k?	–
zajata	zajat	k2eAgFnSc1d1	zajata
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Burgunďany	Burgunďan	k1gMnPc4	Burgunďan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
spojenci	spojenec	k1gMnPc7	spojenec
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Proces	proces	k1gInSc1	proces
===	===	k?	===
</s>
</p>
<p>
<s>
Kouzlo	kouzlo	k1gNnSc1	kouzlo
Jany	Jana	k1gFnSc2	Jana
však	však	k9	však
nepřestávalo	přestávat	k5eNaImAgNnS	přestávat
na	na	k7c4	na
lid	lid	k1gInSc4	lid
působit	působit	k5eAaImF	působit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Angličané	Angličan	k1gMnPc1	Angličan
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
jednou	jednou	k6eAd1	jednou
provždy	provždy	k6eAd1	provždy
s	s	k7c7	s
žijící	žijící	k2eAgFnSc7d1	žijící
legendou	legenda	k1gFnSc7	legenda
skoncovat	skoncovat	k5eAaPmF	skoncovat
<g/>
.	.	kIx.	.
</s>
<s>
Zajatou	zajatý	k2eAgFnSc4d1	zajatá
dívku	dívka	k1gFnSc4	dívka
od	od	k7c2	od
Burgunďanů	Burgunďan	k1gMnPc2	Burgunďan
koupili	koupit	k5eAaPmAgMnP	koupit
za	za	k7c4	za
10	[number]	k4	10
000	[number]	k4	000
liber	libra	k1gFnPc2	libra
a	a	k8xC	a
odvlekli	odvléct	k5eAaPmAgMnP	odvléct
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
Rouenu	Rouen	k1gInSc2	Rouen
před	před	k7c4	před
anglický	anglický	k2eAgInSc4d1	anglický
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
prokázat	prokázat	k5eAaPmF	prokázat
její	její	k3xOp3gNnSc1	její
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
ďáblem	ďábel	k1gMnSc7	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
řídil	řídit	k5eAaImAgMnS	řídit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
biskup	biskup	k1gMnSc1	biskup
Petr	Petr	k1gMnSc1	Petr
Cauchon	Cauchon	k1gMnSc1	Cauchon
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
jako	jako	k9	jako
čarodějka	čarodějka	k1gFnSc1	čarodějka
a	a	k8xC	a
kacířka	kacířka	k1gFnSc1	kacířka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1431	[number]	k4	1431
byla	být	k5eAaImAgFnS	být
devatenáctiletá	devatenáctiletý	k2eAgFnSc1d1	devatenáctiletá
Jana	Jana	k1gFnSc1	Jana
na	na	k7c6	na
rouenském	rouenský	k2eAgNnSc6d1	rouenský
náměstíčku	náměstíčko	k1gNnSc6	náměstíčko
Vieux	Vieux	k1gInSc4	Vieux
Marché	Marchý	k2eAgFnPc1d1	Marchý
(	(	kIx(	(
<g/>
Starý	starý	k2eAgInSc1d1	starý
trh	trh	k1gInSc1	trh
<g/>
)	)	kIx)	)
upálena	upálen	k2eAgFnSc1d1	upálena
<g/>
.	.	kIx.	.
</s>
<s>
Katolickou	katolický	k2eAgFnSc7d1	katolická
církví	církev	k1gFnSc7	církev
nebyla	být	k5eNaImAgFnS	být
exkomunikována	exkomunikovat	k5eAaBmNgFnS	exkomunikovat
a	a	k8xC	a
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
vyloučených	vyloučený	k2eAgFnPc2d1	vyloučená
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
omylem	omyl	k1gInSc7	omyl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
Karel	Karel	k1gMnSc1	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
zajistil	zajistit	k5eAaPmAgMnS	zajistit
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
takřka	takřka	k6eAd1	takřka
celou	celý	k2eAgFnSc7d1	celá
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
církevního	církevní	k2eAgInSc2d1	církevní
procesu	proces	k1gInSc2	proces
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Janu	Jana	k1gFnSc4	Jana
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
justičního	justiční	k2eAgInSc2d1	justiční
omylu	omyl	k1gInSc2	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
rehabilitována	rehabilitovat	k5eAaBmNgFnS	rehabilitovat
světským	světský	k2eAgInSc7d1	světský
soudem	soud	k1gInSc7	soud
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
Katolické	katolický	k2eAgFnSc2d1	katolická
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Pius	Pius	k1gMnSc1	Pius
X.	X.	kA	X.
ji	on	k3xPp3gFnSc4	on
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
předválečné	předválečný	k2eAgFnSc6d1	předválečná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc3	duben
1909	[number]	k4	1909
za	za	k7c4	za
blahoslavenou	blahoslavený	k2eAgFnSc4d1	blahoslavená
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XV	XV	kA	XV
<g/>
.	.	kIx.	.
ji	on	k3xPp3gFnSc4	on
svatořečil	svatořečit	k5eAaBmAgMnS	svatořečit
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
zemím	zem	k1gFnPc3	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Janě	Jana	k1gFnSc3	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
bývá	bývat	k5eAaImIp3nS	bývat
připisováno	připisován	k2eAgNnSc1d1	připisováno
autorství	autorství	k1gNnSc1	autorství
dopisu	dopis	k1gInSc2	dopis
českým	český	k2eAgMnPc3d1	český
husitům	husita	k1gMnPc3	husita
nalezeného	nalezený	k2eAgInSc2d1	nalezený
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
archívu	archív	k1gInSc6	archív
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
datovaného	datovaný	k2eAgInSc2d1	datovaný
v	v	k7c4	v
Sully	Sull	k1gInPc4	Sull
3	[number]	k4	3
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1430	[number]	k4	1430
–	–	k?	–
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vrcholily	vrcholit	k5eAaImAgFnP	vrcholit
husitské	husitský	k2eAgFnPc1d1	husitská
spanilé	spanilý	k2eAgFnPc1d1	spanilá
jízdy	jízda	k1gFnPc1	jízda
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
jim	on	k3xPp3gMnPc3	on
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
"	"	kIx"	"
<g/>
bezbožným	bezbožný	k2eAgFnPc3d1	bezbožná
a	a	k8xC	a
nedovoleným	dovolený	k2eNgFnPc3d1	nedovolená
pověrám	pověra	k1gFnPc3	pověra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ničí	ničit	k5eAaImIp3nP	ničit
a	a	k8xC	a
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
hrozí	hrozit	k5eAaImIp3nS	hrozit
možností	možnost	k1gFnSc7	možnost
osobního	osobní	k2eAgInSc2d1	osobní
zákroku	zákrok	k1gInSc2	zákrok
(	(	kIx(	(
<g/>
ztíženou	ztížený	k2eAgFnSc4d1	ztížená
však	však	k8xC	však
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nabízí	nabízet	k5eAaImIp3nS	nabízet
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
i	i	k9	i
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jejich	jejich	k3xOp3gInSc2	jejich
zájmu	zájem	k1gInSc2	zájem
o	o	k7c6	o
obrácení	obrácení	k1gNnSc6	obrácení
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yQgFnSc2	jaký
míry	míra	k1gFnSc2	míra
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dopis	dopis	k1gInSc1	dopis
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Jany	Jana	k1gFnSc2	Jana
samé	samý	k3xTgFnPc1	samý
<g/>
,	,	kIx,	,
a	a	k8xC	a
přiklánějí	přiklánět	k5eAaImIp3nP	přiklánět
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
písařem	písař	k1gMnSc7	písař
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
psát	psát	k5eAaImF	psát
neuměla	umět	k5eNaImAgFnS	umět
<g/>
)	)	kIx)	)
napsán	napsat	k5eAaPmNgInS	napsat
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dalších	další	k2eAgNnPc2d1	další
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc1d1	latinský
text	text	k1gInSc1	text
dopisu	dopis	k1gInSc2	dopis
zřejmě	zřejmě	k6eAd1	zřejmě
překladem	překlad	k1gInSc7	překlad
a	a	k8xC	a
původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
složen	složit	k5eAaPmNgInS	složit
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
ovšem	ovšem	k9	ovšem
čeští	český	k2eAgMnPc1d1	český
husité	husita	k1gMnPc1	husita
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
anglických	anglický	k2eAgMnPc2d1	anglický
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
nerozuměli	rozumět	k5eNaImAgMnP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Příštího	příští	k2eAgInSc2d1	příští
roku	rok	k1gInSc2	rok
při	při	k7c6	při
Janině	Janin	k2eAgInSc6d1	Janin
procesu	proces	k1gInSc6	proces
byl	být	k5eAaImAgInS	být
i	i	k9	i
tento	tento	k3xDgInSc4	tento
její	její	k3xOp3gInSc4	její
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
jasně	jasně	k6eAd1	jasně
protikacířské	protikacířský	k2eAgNnSc4d1	protikacířský
ladění	ladění	k1gNnSc4	ladění
<g/>
,	,	kIx,	,
inkvizitorem	inkvizitor	k1gMnSc7	inkvizitor
Johannem	Johann	k1gMnSc7	Johann
Niderem	Nider	k1gMnSc7	Nider
zahrnut	zahrnut	k2eAgMnSc1d1	zahrnut
mezi	mezi	k7c7	mezi
podklady	podklad	k1gInPc7	podklad
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Alternativní	alternativní	k2eAgFnSc2d1	alternativní
historické	historický	k2eAgFnSc2d1	historická
interpretace	interpretace	k1gFnSc2	interpretace
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
Pierre	Pierr	k1gInSc5	Pierr
Caze	Caze	k1gNnSc7	Caze
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
nelegitimní	legitimní	k2eNgFnSc7d1	nelegitimní
dcerou	dcera	k1gFnSc7	dcera
Isabely	Isabela	k1gFnSc2	Isabela
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
přežila	přežít	k5eAaPmAgFnS	přežít
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
jako	jako	k8xS	jako
Jeanne	Jeann	k1gInSc5	Jeann
des	des	k1gNnPc6	des
Armoises	Armoises	k1gInSc4	Armoises
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Antropoložka	antropoložka	k1gFnSc1	antropoložka
Margaret	Margareta	k1gFnPc2	Margareta
Murrayová	Murrayová	k1gFnSc1	Murrayová
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
čarodějnický	čarodějnický	k2eAgInSc4d1	čarodějnický
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpracování	zpracování	k1gNnSc1	zpracování
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Veršované	veršovaný	k2eAgFnSc2d1	veršovaná
satiry	satira	k1gFnSc2	satira
===	===	k?	===
</s>
</p>
<p>
<s>
VOLTAIRE	VOLTAIRE	kA	VOLTAIRE
<g/>
.	.	kIx.	.
</s>
<s>
Panna	Panna	k1gFnSc1	Panna
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Radovan	Radovan	k1gMnSc1	Radovan
Krátký	Krátký	k1gMnSc1	Krátký
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
v	v	k7c6	v
SNKLU	SNKLU	kA	SNKLU
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
338	[number]	k4	338
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
s.	s.	k?	s.
Nesmrtelní	nesmrtelný	k1gMnPc1	nesmrtelný
<g/>
;	;	kIx,	;
Sv.	sv.	kA	sv.
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Drama	drama	k1gNnSc1	drama
===	===	k?	===
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
:	:	kIx,	:
Panna	Panna	k1gFnSc1	Panna
orleánská	orleánský	k2eAgFnSc1d1	Orleánská
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
Jungfrau	Jungfrau	k1gFnSc1	Jungfrau
von	von	k1gInSc4	von
Orleans	Orleans	k1gInSc1	Orleans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1801	[number]	k4	1801
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
:	:	kIx,	:
Svatá	svatý	k2eAgFnSc1d1	svatá
Jana	Jana	k1gFnSc1	Jana
(	(	kIx(	(
<g/>
Saint	Saint	k1gMnSc1	Saint
Joan	Joan	k1gMnSc1	Joan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
Anouilh	Anouilh	k1gMnSc1	Anouilh
<g/>
:	:	kIx,	:
Skřivánek	Skřivánek	k1gMnSc1	Skřivánek
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
́	́	k?	́
<g/>
Alouette	Alouett	k1gMnSc5	Alouett
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS	Giuseppat
Verdi	Verd	k1gMnPc1	Verd
<g/>
:	:	kIx,	:
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
(	(	kIx(	(
<g/>
Giovanna	Giovanna	k1gFnSc1	Giovanna
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Arco	Arco	k1gMnSc1	Arco
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Iljič	Iljič	k1gMnSc1	Iljič
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
<g/>
:	:	kIx,	:
Panna	Panna	k1gFnSc1	Panna
orleánská	orleánský	k2eAgFnSc1d1	Orleánská
(	(	kIx(	(
<g/>
Orleanskaja	Orleanskaj	k2eAgFnSc1d1	Orleanskaj
děva	děva	k1gFnSc1	děva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
</s>
</p>
<p>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Honegger	Honegger	k1gMnSc1	Honegger
<g/>
:	:	kIx,	:
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
(	(	kIx(	(
<g/>
Jeanne	Jeann	k1gMnSc5	Jeann
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Arc	Arc	k1gFnSc2	Arc
au	au	k0	au
bucher	buchra	k1gFnPc2	buchra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
–	–	k?	–
dramatické	dramatický	k2eAgNnSc1d1	dramatické
oratorium	oratorium	k1gNnSc1	oratorium
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Soukup	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Osvaldová	Osvaldová	k1gFnSc1	Osvaldová
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Hubač	hubačit	k5eAaImRp2nS	hubačit
<g/>
:	:	kIx,	:
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
Utrpení	utrpení	k1gNnSc1	utrpení
panny	panna	k1gFnSc2	panna
Orleánské	orleánský	k2eAgNnSc1d1	orleánské
(	(	kIx(	(
<g/>
Passion	Passion	k1gInSc1	Passion
de	de	k?	de
Jeanne	Jeann	k1gInSc5	Jeann
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arc	Arc	k1gFnSc1	Arc
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Carl	Carl	k1gMnSc1	Carl
Theodor	Theodor	k1gMnSc1	Theodor
Dreyer	Dreyer	k1gMnSc1	Dreyer
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Maria	Maria	k1gFnSc1	Maria
Falconetti	Falconetť	k1gFnSc2	Falconetť
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gInSc5	Eugen
Silvain	Silvain	k1gMnSc1	Silvain
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
Berley	Berlea	k1gFnSc2	Berlea
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc3	Maurika
Schutz	Schutza	k1gFnPc2	Schutza
<g/>
,	,	kIx,	,
Antonin	Antonin	k2eAgInSc1d1	Antonin
Artaud	Artaud	k1gInSc1	Artaud
</s>
</p>
<p>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
(	(	kIx(	(
<g/>
Joan	Joan	k1gMnSc1	Joan
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Victor	Victor	k1gMnSc1	Victor
Fleming	Fleming	k1gInSc1	Fleming
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Ingrid	Ingrid	k1gFnSc1	Ingrid
Bergman	Bergman	k1gMnSc1	Bergman
<g/>
,	,	kIx,	,
José	José	k1gNnSc1	José
Ferrer	Ferrra	k1gFnPc2	Ferrra
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Carrol	Carrol	k1gInSc1	Carrol
Naish	Naish	k1gInSc1	Naish
<g/>
,	,	kIx,	,
Ward	Ward	k1gInSc1	Ward
Bond	bond	k1gInSc1	bond
<g/>
,	,	kIx,	,
Shepperd	Shepperd	k1gMnSc1	Shepperd
Strudwick	Strudwick	k1gMnSc1	Strudwick
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
Lockhart	Lockharta	k1gFnPc2	Lockharta
</s>
</p>
<p>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
u	u	k7c2	u
kůlu	kůl	k1gInSc2	kůl
(	(	kIx(	(
<g/>
Giovanna	Giovanna	k1gFnSc1	Giovanna
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arco	Arco	k1gMnSc1	Arco
al	ala	k1gFnPc2	ala
rogo	rogo	k1gMnSc1	rogo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Roberto	Roberta	k1gFnSc5	Roberta
Rossellini	Rossellin	k2eAgMnPc1d1	Rossellin
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Ingrid	Ingrid	k1gFnSc1	Ingrid
Bergman	Bergman	k1gMnSc1	Bergman
<g/>
,	,	kIx,	,
Tullio	Tullia	k1gMnSc5	Tullia
Carminati	Carminati	k1gMnSc5	Carminati
<g/>
,	,	kIx,	,
Fra	Fra	k1gMnSc5	Fra
Domenico	Domenica	k1gMnSc5	Domenica
<g/>
,	,	kIx,	,
Giacinto	Giacinta	k1gMnSc5	Giacinta
Prandelli	Prandell	k1gMnSc5	Prandell
<g/>
,	,	kIx,	,
Augusto	Augusta	k1gMnSc5	Augusta
Romani	Roman	k1gMnPc1	Roman
</s>
</p>
<p>
<s>
Proces	proces	k1gInSc1	proces
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
(	(	kIx(	(
<g/>
Procè	Procè	k1gFnSc1	Procè
de	de	k?	de
Jeanne	Jeann	k1gInSc5	Jeann
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arc	Arc	k1gMnSc1	Arc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Bresson	Bresson	k1gMnSc1	Bresson
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Florence	Florenc	k1gFnPc4	Florenc
Delay	Dela	k2eAgFnPc4d1	Dela
<g/>
,	,	kIx,	,
Jean-Claude	Jean-Claud	k1gInSc5	Jean-Claud
Fourneau	Fourneaum	k1gNnSc6	Fourneaum
<g/>
,	,	kIx,	,
Roger	Roger	k1gMnSc1	Roger
Honorat	Honorat	k1gMnSc1	Honorat
<g/>
,	,	kIx,	,
Marc	Marc	k1gFnSc1	Marc
Jacquier	Jacquier	k1gMnSc1	Jacquier
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Gilibert	Gilibert	k1gMnSc1	Gilibert
</s>
</p>
<p>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
(	(	kIx(	(
<g/>
Joan	Joan	k1gMnSc1	Joan
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Christian	Christian	k1gMnSc1	Christian
Duguay	Duguaa	k1gFnSc2	Duguaa
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Leelee	Leelee	k1gNnSc7	Leelee
Sobieski	Sobiesk	k1gFnSc2	Sobiesk
<g/>
,	,	kIx,	,
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Bisset	Bisset	k1gInSc1	Bisset
<g/>
,	,	kIx,	,
Powers	Powers	k1gInSc1	Powers
Boothe	Booth	k1gFnSc2	Booth
<g/>
,	,	kIx,	,
Neil	Neil	k1gMnSc1	Neil
Patrick	Patrick	k1gMnSc1	Patrick
Harris	Harris	k1gInSc4	Harris
</s>
</p>
<p>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
(	(	kIx(	(
<g/>
Jeanne	Jeann	k1gInSc5	Jeann
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arc	Arc	k1gMnSc1	Arc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Luc	Luc	k1gMnSc1	Luc
Besson	Besson	k1gMnSc1	Besson
<g/>
;	;	kIx,	;
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Milla	Milla	k1gFnSc1	Milla
Jovovich	Jovovich	k1gMnSc1	Jovovich
<g/>
,	,	kIx,	,
Dustin	Dustin	k1gMnSc1	Dustin
Hoffman	Hoffman	k1gMnSc1	Hoffman
<g/>
,	,	kIx,	,
Faye	Faye	k1gFnSc1	Faye
Dunaway	Dunawaa	k1gFnSc2	Dunawaa
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
<g/>
,	,	kIx,	,
Tchéky	Tchéka	k1gFnPc1	Tchéka
Karyo	Karyo	k1gMnSc1	Karyo
<g/>
,	,	kIx,	,
Vincent	Vincent	k1gMnSc1	Vincent
Cassel	Cassel	k1gMnSc1	Cassel
</s>
</p>
<p>
<s>
===	===	k?	===
Videohry	videohra	k1gFnSc2	videohra
===	===	k?	===
</s>
</p>
<p>
<s>
Rock	rock	k1gInSc1	rock
of	of	k?	of
Ages	Ages	k1gInSc1	Ages
bigger	bigger	k1gMnSc1	bigger
a	a	k8xC	a
boulder	boulder	k1gMnSc1	boulder
</s>
</p>
<p>
<s>
Wars	Wars	k1gInSc1	Wars
and	and	k?	and
Warriors	Warriors	k1gInSc1	Warriors
<g/>
:	:	kIx,	:
Joan	Joan	k1gMnSc1	Joan
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Vývojář	vývojář	k1gMnSc1	vývojář
<g/>
:	:	kIx,	:
Enlight	Enlight	k1gMnSc1	Enlight
<g/>
,	,	kIx,	,
single	singl	k1gInSc5	singl
player	playrat	k5eAaPmRp2nS	playrat
RPG	RPG	kA	RPG
<g/>
/	/	kIx~	/
<g/>
strategie	strategie	k1gFnSc1	strategie
na	na	k7c4	na
PC	PC	kA	PC
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FRANCE	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
Anatole	Anatola	k1gFnSc3	Anatola
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
.	.	kIx.	.
</s>
<s>
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zaorálek	Zaorálek	k1gMnSc1	Zaorálek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
522	[number]	k4	522
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
s.	s.	k?	s.
Spisy	spis	k1gInPc1	spis
Anatola	Anatola	k1gFnSc1	Anatola
France	Franc	k1gMnSc2	Franc
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
klasiků	klasik	k1gMnPc2	klasik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
:	:	kIx,	:
(	(	kIx(	(
<g/>
1356	[number]	k4	1356
<g/>
–	–	k?	–
<g/>
1450	[number]	k4	1450
<g/>
)	)	kIx)	)
:	:	kIx,	:
rytířské	rytířský	k2eAgFnPc1d1	rytířská
bitvy	bitva	k1gFnPc1	bitva
a	a	k8xC	a
osudy	osud	k1gInPc1	osud
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
346	[number]	k4	346
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1499	[number]	k4	1499
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NEJEDLÝ	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
8	[number]	k4	8
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
57	[number]	k4	57
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERNOUDOVÁ	PERNOUDOVÁ	kA	PERNOUDOVÁ
<g/>
,	,	kIx,	,
Régine	Régin	k1gMnSc5	Régin
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
:	:	kIx,	:
svědectví	svědectví	k1gNnSc4	svědectví
z	z	k7c2	z
rehabilitačního	rehabilitační	k2eAgInSc2d1	rehabilitační
procesu	proces	k1gInSc2	proces
(	(	kIx(	(
<g/>
1450	[number]	k4	1450
<g/>
-	-	kIx~	-
<g/>
1456	[number]	k4	1456
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Refugium	refugium	k1gNnSc1	refugium
Velehrad-Roma	Velehrad-Roma	k1gFnSc1	Velehrad-Roma
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
272	[number]	k4	272
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7412	[number]	k4	7412
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERNOUDOVÁ	PERNOUDOVÁ	kA	PERNOUDOVÁ
<g/>
,	,	kIx,	,
Régine	Régin	k1gMnSc5	Régin
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
109	[number]	k4	109
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85319	[number]	k4	85319
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
Johance	Johanka	k1gFnSc6	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
1911	[number]	k4	1911
Encyclopæ	Encyclopæ	k1gMnSc1	Encyclopæ
Britannica	Britannica	k1gMnSc1	Britannica
<g/>
/	/	kIx~	/
<g/>
Joan	Joan	k1gMnSc1	Joan
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Johanka	Johanka	k1gFnSc1	Johanka
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
CENTRE	centr	k1gInSc5	centr
</s>
</p>
<p>
<s>
Historical	Historicat	k5eAaPmAgMnS	Historicat
Academy	Academa	k1gFnPc4	Academa
(	(	kIx(	(
<g/>
Association	Association	k1gInSc1	Association
<g/>
)	)	kIx)	)
for	forum	k1gNnPc2	forum
Joan	Joana	k1gFnPc2	Joana
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
Studies	Studies	k1gMnSc1	Studies
</s>
</p>
<p>
<s>
Joan	Joan	k1gMnSc1	Joan
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
Archive	archiv	k1gInSc5	archiv
</s>
</p>
<p>
<s>
St.	st.	kA	st.
Joan	Joan	k1gMnSc1	Joan
of	of	k?	of
Arc	Arc	k1gMnSc1	Arc
</s>
</p>
<p>
<s>
Saint-Joan-of-Arc	Saint-Joanf-Arc	k1gFnSc1	Saint-Joan-of-Arc
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
