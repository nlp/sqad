<s>
Mocně	mocně	k6eAd1	mocně
působila	působit	k5eAaImAgFnS	působit
na	na	k7c4	na
vojáky	voják	k1gMnPc4	voják
i	i	k8xC	i
lid	lid	k1gInSc1	lid
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
její	její	k3xOp3gFnPc4	její
popularity	popularita	k1gFnPc4	popularita
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jí	jíst	k5eAaImIp3nS	jíst
svěřoval	svěřovat	k5eAaImAgMnS	svěřovat
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
a	a	k8xC	a
méně	málo	k6eAd2	málo
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
nebezpečné	bezpečný	k2eNgInPc4d1	nebezpečný
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
