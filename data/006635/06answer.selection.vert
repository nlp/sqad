<s>
Mucha	Mucha	k1gMnSc1	Mucha
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xS	jako
umělec	umělec	k1gMnSc1	umělec
pracující	pracující	k1gMnSc1	pracující
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
reklamních	reklamní	k2eAgInPc2d1	reklamní
plakátů	plakát	k1gInPc2	plakát
tvořil	tvořit	k5eAaImAgMnS	tvořit
i	i	k8xC	i
návrhy	návrh	k1gInPc4	návrh
obalů	obal	k1gInPc2	obal
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
spotřebních	spotřební	k2eAgInPc2d1	spotřební
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
maloval	malovat	k5eAaImAgMnS	malovat
návrhy	návrh	k1gInPc1	návrh
jídelních	jídelní	k2eAgNnPc2d1	jídelní
menu	menu	k1gNnPc2	menu
<g/>
,	,	kIx,	,
kalendářů	kalendář	k1gInPc2	kalendář
a	a	k8xC	a
dekorativních	dekorativní	k2eAgFnPc2d1	dekorativní
zástěn	zástěna	k1gFnPc2	zástěna
<g/>
,	,	kIx,	,
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
knihy	kniha	k1gFnPc4	kniha
svými	svůj	k3xOyFgFnPc7	svůj
ilustracemi	ilustrace	k1gFnPc7	ilustrace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Otčenáš	otčenáš	k1gInSc1	otčenáš
(	(	kIx(	(
<g/>
Le	Le	k1gFnPc1	Le
Pater	patro	k1gNnPc2	patro
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
grafickou	grafický	k2eAgFnSc4d1	grafická
předlohu	předloha	k1gFnSc4	předloha
k	k	k7c3	k
první	první	k4xOgFnSc3	první
československé	československý	k2eAgFnSc3d1	Československá
poštovní	poštovní	k2eAgFnSc3d1	poštovní
známce	známka	k1gFnSc3	známka
a	a	k8xC	a
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
i	i	k9	i
grafickou	grafický	k2eAgFnSc4d1	grafická
podobu	podoba	k1gFnSc4	podoba
československých	československý	k2eAgFnPc2d1	Československá
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
.	.	kIx.	.
</s>
