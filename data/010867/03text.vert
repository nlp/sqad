<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc4d1	významné
město	město	k1gNnSc4	město
a	a	k8xC	a
metropolitní	metropolitní	k2eAgInSc4d1	metropolitní
distrikt	distrikt	k1gInSc4	distrikt
v	v	k7c6	v
regionu	region	k1gInSc6	region
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
historicky	historicky	k6eAd1	historicky
významné	významný	k2eAgFnPc1d1	významná
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
v	v	k7c6	v
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
545	[number]	k4	545
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
zahrnující	zahrnující	k2eAgNnSc4d1	zahrnující
území	území	k1gNnSc4	území
hrabství	hrabství	k1gNnSc2	hrabství
Velký	velký	k2eAgInSc1d1	velký
Manchester	Manchester	k1gInSc1	Manchester
pak	pak	k6eAd1	pak
zhruba	zhruba	k6eAd1	zhruba
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
sousedství	sousedství	k1gNnSc6	sousedství
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Cheshirská	Cheshirský	k2eAgFnSc1d1	Cheshirský
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
hřebeny	hřeben	k1gInPc4	hřeben
Pennin	Pennin	k2eAgInSc1d1	Pennin
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgFnPc7d3	nejbližší
velkými	velký	k2eAgFnPc7d1	velká
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Liverpool	Liverpool	k1gInSc4	Liverpool
(	(	kIx(	(
<g/>
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
56	[number]	k4	56
km	km	kA	km
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sheffield	Sheffield	k1gMnSc1	Sheffield
(	(	kIx(	(
<g/>
56	[number]	k4	56
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
ve	v	k7c6	v
sportovním	sportovní	k2eAgInSc6d1	sportovní
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
fotbalovými	fotbalový	k2eAgInPc7d1	fotbalový
kluby	klub	k1gInPc7	klub
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
City	City	k1gFnSc2	City
a	a	k8xC	a
kriketovým	kriketový	k2eAgInSc7d1	kriketový
klubem	klub	k1gInSc7	klub
Lancashire	Lancashir	k1gInSc5	Lancashir
County	Count	k1gInPc4	Count
Cricket	Cricket	k1gMnSc1	Cricket
Club	club	k1gInSc4	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgFnP	konat
17	[number]	k4	17
<g/>
.	.	kIx.	.
hry	hra	k1gFnSc2	hra
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Centrum	centrum	k1gNnSc1	centrum
Manchesteru	Manchester	k1gInSc2	Manchester
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
památek	památka	k1gFnPc2	památka
chráněných	chráněný	k2eAgInPc2d1	chráněný
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
polohu	poloha	k1gFnSc4	poloha
poblíž	poblíž	k7c2	poblíž
sítě	síť	k1gFnSc2	síť
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
největší	veliký	k2eAgInSc4d3	veliký
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
období	období	k1gNnSc6	období
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
jména	jméno	k1gNnSc2	jméno
keltského	keltský	k2eAgInSc2d1	keltský
původu	původ	k1gInSc2	původ
Mamucium	Mamucium	k1gNnSc4	Mamucium
případně	případně	k6eAd1	případně
Mancunium	Mancunium	k1gNnSc4	Mancunium
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
složením	složení	k1gNnSc7	složení
z	z	k7c2	z
keltského	keltský	k2eAgMnSc2d1	keltský
mamm	mamm	k6eAd1	mamm
(	(	kIx(	(
<g/>
prsa	prsa	k1gNnPc1	prsa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
mamma	mamma	k1gFnSc1	mamma
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
a	a	k8xC	a
staroanglického	staroanglický	k2eAgInSc2d1	staroanglický
ceaster	ceaster	k1gMnSc1	ceaster
(	(	kIx(	(
<g/>
tábor	tábor	k1gInSc1	tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Manchesteru	Manchester	k1gInSc2	Manchester
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
někdy	někdy	k6eAd1	někdy
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
Mancunians	Mancunians	k1gInSc4	Mancunians
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
Manchesteru	Manchester	k1gInSc2	Manchester
byla	být	k5eAaImAgFnS	být
osídlena	osídlen	k2eAgFnSc1d1	osídlena
ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
přišli	přijít	k5eAaPmAgMnP	přijít
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
táhli	táhnout	k5eAaImAgMnP	táhnout
proti	proti	k7c3	proti
Brigantům	brigant	k1gMnPc3	brigant
<g/>
,	,	kIx,	,
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Irwell	Irwell	k1gInSc1	Irwell
nechal	nechat	k5eAaPmAgMnS	nechat
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Agricola	Agricola	k1gFnSc1	Agricola
postavit	postavit	k5eAaPmF	postavit
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dočasná	dočasný	k2eAgFnSc1d1	dočasná
stavba	stavba	k1gFnSc1	stavba
prošla	projít	k5eAaPmAgFnS	projít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
mnoha	mnoho	k4c7	mnoho
přestavbami	přestavba	k1gFnPc7	přestavba
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
významnou	významný	k2eAgFnSc7d1	významná
zastávkou	zastávka	k1gFnSc7	zastávka
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
sever	sever	k1gInSc4	sever
k	k	k7c3	k
pevnostem	pevnost	k1gFnPc3	pevnost
v	v	k7c6	v
Chesteru	Chester	k1gInSc6	Chester
a	a	k8xC	a
Yorku	York	k1gInSc6	York
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
brána	brána	k1gFnSc1	brána
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
část	část	k1gFnSc4	část
hradeb	hradba	k1gFnPc2	hradba
poblíž	poblíž	k7c2	poblíž
Castlefieldu	Castlefield	k1gInSc2	Castlefield
byla	být	k5eAaImAgFnS	být
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
zde	zde	k6eAd1	zde
existovalo	existovat	k5eAaImAgNnS	existovat
opevněné	opevněný	k2eAgNnSc1d1	opevněné
panské	panský	k2eAgNnSc1d1	panské
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
roku	rok	k1gInSc2	rok
1422	[number]	k4	1422
šlechtic	šlechtic	k1gMnSc1	šlechtic
a	a	k8xC	a
kněz	kněz	k1gMnSc1	kněz
Thomas	Thomas	k1gMnSc1	Thomas
De	De	k?	De
La	la	k1gNnPc2	la
Warre	Warr	k1gInSc5	Warr
tento	tento	k3xDgInSc1	tento
statek	statek	k1gInSc1	statek
i	i	k9	i
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
pozemky	pozemek	k1gInPc7	pozemek
věnoval	věnovat	k5eAaImAgMnS	věnovat
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kolej	kolej	k1gFnSc1	kolej
pro	pro	k7c4	pro
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
stavbu	stavba	k1gFnSc4	stavba
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
koleje	kolej	k1gFnSc2	kolej
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
Chetham	Chetham	k1gInSc1	Chetham
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
School	School	k1gInSc1	School
of	of	k?	of
Music	Musice	k1gInPc2	Musice
a	a	k8xC	a
chrám	chrám	k1gInSc1	chrám
se	se	k3xPyFc4	se
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
v	v	k7c4	v
nynější	nynější	k2eAgFnSc4d1	nynější
Manchesterskou	manchesterský	k2eAgFnSc4d1	Manchesterská
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1301	[number]	k4	1301
obdržel	obdržet	k5eAaPmAgInS	obdržet
královský	královský	k2eAgInSc1d1	královský
patent	patent	k1gInSc1	patent
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
trhů	trh	k1gInPc2	trh
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
významným	významný	k2eAgNnSc7d1	významné
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
do	do	k7c2	do
Salfordu	Salford	k1gInSc2	Salford
a	a	k8xC	a
Manchesteru	Manchester	k1gInSc2	Manchester
přistěhovala	přistěhovat	k5eAaPmAgFnS	přistěhovat
komunita	komunita	k1gFnSc1	komunita
vlámských	vlámský	k2eAgMnPc2d1	vlámský
tkalců	tkadlec	k1gMnPc2	tkadlec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
vlněné	vlněný	k2eAgInPc4d1	vlněný
oděvy	oděv	k1gInPc4	oděv
a	a	k8xC	a
započali	započnout	k5eAaPmAgMnP	započnout
tak	tak	k9	tak
tradici	tradice	k1gFnSc4	tradice
tkaní	tkaní	k1gNnSc2	tkaní
sukna	sukno	k1gNnSc2	sukno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proměnila	proměnit	k5eAaPmAgFnS	proměnit
původní	původní	k2eAgNnSc4d1	původní
obchodní	obchodní	k2eAgNnSc4d1	obchodní
městečko	městečko	k1gNnSc4	městečko
ve	v	k7c4	v
významné	významný	k2eAgNnSc4d1	významné
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Vlhké	vlhký	k2eAgNnSc1d1	vlhké
podnebí	podnebí	k1gNnSc1	podnebí
zpracování	zpracování	k1gNnSc1	zpracování
bavlny	bavlna	k1gFnSc2	bavlna
vyhovovalo	vyhovovat	k5eAaImAgNnS	vyhovovat
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
strojů	stroj	k1gInPc2	stroj
poháněných	poháněný	k2eAgInPc2d1	poháněný
parou	para	k1gFnSc7	para
proces	proces	k1gInSc1	proces
předení	předení	k1gNnSc2	předení
a	a	k8xC	a
tkaní	tkaní	k1gNnSc2	tkaní
urychlil	urychlit	k5eAaPmAgInS	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgInS	stát
významným	významný	k2eAgNnSc7d1	významné
distribučním	distribuční	k2eAgNnSc7d1	distribuční
střediskem	středisko	k1gNnSc7	středisko
s	s	k7c7	s
mnoha	mnoho	k4c2	mnoho
sklady	sklad	k1gInPc7	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
Bridgewater	Bridgewater	k1gMnSc1	Bridgewater
Canal	Canal	k1gMnSc1	Canal
<g/>
,	,	kIx,	,
prvního	první	k4xOgInSc2	první
umělého	umělý	k2eAgInSc2d1	umělý
vnitrozemského	vnitrozemský	k2eAgInSc2d1	vnitrozemský
kanálu	kanál	k1gInSc2	kanál
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
podpořila	podpořit	k5eAaPmAgFnS	podpořit
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
díky	díky	k7c3	díky
zajištění	zajištění	k1gNnSc3	zajištění
dostupnosti	dostupnost	k1gFnSc2	dostupnost
levného	levný	k2eAgNnSc2d1	levné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Zprovoznění	zprovoznění	k1gNnSc1	zprovoznění
Liverpool	Liverpool	k1gInSc1	Liverpool
and	and	k?	and
Manchester	Manchester	k1gInSc1	Manchester
Railway	Railwaa	k1gMnSc2	Railwaa
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
železnice	železnice	k1gFnSc2	železnice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
přepravu	přeprava	k1gFnSc4	přeprava
<g/>
,	,	kIx,	,
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
dále	daleko	k6eAd2	daleko
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
rychle	rychle	k6eAd1	rychle
rostl	růst	k5eAaImAgInS	růst
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
lidé	člověk	k1gMnPc1	člověk
zblízka	zblízka	k6eAd1	zblízka
i	i	k9	i
z	z	k7c2	z
dálky	dálka	k1gFnSc2	dálka
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
za	za	k7c7	za
pracovními	pracovní	k2eAgFnPc7d1	pracovní
příležitostmi	příležitost	k1gFnPc7	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
jich	on	k3xPp3gFnPc2	on
sem	sem	k6eAd1	sem
směřoval	směřovat	k5eAaImAgInS	směřovat
především	především	k9	především
z	z	k7c2	z
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
postiženého	postižený	k2eAgMnSc2d1	postižený
tehdy	tehdy	k6eAd1	tehdy
hladomorem	hladomor	k1gMnSc7	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
irské	irský	k2eAgFnSc2d1	irská
komunity	komunita	k1gFnSc2	komunita
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
silný	silný	k2eAgInSc4d1	silný
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
velká	velký	k2eAgFnSc1d1	velká
oslava	oslava	k1gFnSc1	oslava
svátku	svátek	k1gInSc2	svátek
svatého	svatý	k2eAgMnSc2d1	svatý
Patrika	Patrik	k1gMnSc2	Patrik
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
vlna	vlna	k1gFnSc1	vlna
imigrantů	imigrant	k1gMnPc2	imigrant
(	(	kIx(	(
<g/>
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byli	být	k5eAaImAgMnP	být
Židé	Žid	k1gMnPc1	Žid
<g/>
)	)	kIx)	)
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pocházela	pocházet	k5eAaImAgFnS	pocházet
i	i	k9	i
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Manchester	Manchester	k1gInSc1	Manchester
kosmopolitním	kosmopolitní	k2eAgNnSc7d1	kosmopolitní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
motýl	motýl	k1gMnSc1	motýl
drsnokřídlec	drsnokřídlec	k1gMnSc1	drsnokřídlec
březový	březový	k2eAgMnSc1d1	březový
musel	muset	k5eAaImAgMnS	muset
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
změně	změna	k1gFnSc3	změna
kvality	kvalita	k1gFnSc2	kvalita
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
;	;	kIx,	;
během	během	k7c2	během
několika	několik	k4yIc2	několik
generací	generace	k1gFnPc2	generace
se	se	k3xPyFc4	se
barva	barva	k1gFnSc1	barva
jeho	jeho	k3xOp3gNnPc2	jeho
křídel	křídlo	k1gNnPc2	křídlo
zcela	zcela	k6eAd1	zcela
změnila	změnit	k5eAaPmAgFnS	změnit
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
na	na	k7c4	na
černou	černá	k1gFnSc4	černá
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
popis	popis	k1gInSc4	popis
této	tento	k3xDgFnSc2	tento
změny	změna	k1gFnSc2	změna
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
a	a	k8xC	a
posloužil	posloužit	k5eAaPmAgInS	posloužit
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důkazů	důkaz	k1gInPc2	důkaz
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
Manchester	Manchester	k1gInSc1	Manchester
Ship	Ship	k1gInSc1	Ship
Canal	Canal	k1gInSc4	Canal
-	-	kIx~	-
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
kanál	kanál	k1gInSc4	kanál
od	od	k7c2	od
Salfordu	Salford	k1gInSc2	Salford
až	až	k9	až
k	k	k7c3	k
Merseyské	Merseyský	k2eAgFnSc3d1	Merseyský
zátoce	zátoka	k1gFnSc3	zátoka
u	u	k7c2	u
Liverpoolského	liverpoolský	k2eAgInSc2d1	liverpoolský
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
kanálu	kanál	k1gInSc3	kanál
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
doku	dok	k1gInSc6	dok
Manchesterského	manchesterský	k2eAgInSc2d1	manchesterský
přístavu	přístav	k1gInSc2	přístav
přistávat	přistávat	k5eAaImF	přistávat
zaoceánská	zaoceánský	k2eAgNnPc4d1	zaoceánské
plavidla	plavidlo	k1gNnPc4	plavidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
postihla	postihnout	k5eAaPmAgFnS	postihnout
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
především	především	k6eAd1	především
textilní	textilní	k2eAgInSc4d1	textilní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
letadla	letadlo	k1gNnPc4	letadlo
pro	pro	k7c4	pro
Royal	Royal	k1gInSc4	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejznámější	známý	k2eAgInSc1d3	nejznámější
byl	být	k5eAaImAgInS	být
bombardér	bombardér	k1gInSc1	bombardér
Avro	Avro	k1gMnSc1	Avro
Lancaster	Lancaster	k1gMnSc1	Lancaster
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
byl	být	k5eAaImAgInS	být
častým	častý	k2eAgInSc7d1	častý
terčem	terč	k1gInSc7	terč
nacistických	nacistický	k2eAgInPc2d1	nacistický
náletů	nálet	k1gInPc2	nálet
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zničily	zničit	k5eAaPmAgFnP	zničit
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
vážně	vážně	k6eAd1	vážně
poškodily	poškodit	k5eAaPmAgInP	poškodit
katedrálu	katedrála	k1gFnSc4	katedrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
vybuchla	vybuchnout	k5eAaPmAgFnS	vybuchnout
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
bomba	bomba	k1gFnSc1	bomba
nastražená	nastražený	k2eAgFnSc1d1	nastražená
IRA	Ir	k1gMnSc4	Ir
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
zranil	zranit	k5eAaPmAgInS	zranit
asi	asi	k9	asi
200	[number]	k4	200
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc4	žádný
úmrtí	úmrtí	k1gNnSc4	úmrtí
nezpůsobil	způsobit	k5eNaPmAgInS	způsobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
střed	střed	k1gInSc1	střed
města	město	k1gNnSc2	město
vážně	vážně	k6eAd1	vážně
poškodil	poškodit	k5eAaPmAgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
následné	následný	k2eAgFnSc6d1	následná
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
nové	nový	k2eAgInPc1d1	nový
komplexy	komplex	k1gInPc1	komplex
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Printworks	Printworks	k1gInSc1	Printworks
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
stavby	stavba	k1gFnPc1	stavba
pro	pro	k7c4	pro
nákupy	nákup	k1gInPc4	nákup
a	a	k8xC	a
zábavu	zábava	k1gFnSc4	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
město	město	k1gNnSc1	město
hostilo	hostit	k5eAaImAgNnS	hostit
17	[number]	k4	17
<g/>
.	.	kIx.	.
hry	hra	k1gFnSc2	hra
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
se	se	k3xPyFc4	se
také	také	k9	také
dvakrát	dvakrát	k6eAd1	dvakrát
–	–	k?	–
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
a	a	k8xC	a
2000	[number]	k4	2000
–	–	k?	–
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
pořádání	pořádání	k1gNnSc4	pořádání
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
v	v	k7c6	v
Manchester	Manchester	k1gInSc1	Manchester
aréně	aréna	k1gFnSc3	aréna
udál	udát	k5eAaPmAgInS	udát
sebevražedný	sebevražedný	k2eAgInSc1d1	sebevražedný
bombový	bombový	k2eAgInSc1d1	bombový
útok	útok	k1gInSc1	útok
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
život	život	k1gInSc4	život
přišlo	přijít	k5eAaPmAgNnS	přijít
22	[number]	k4	22
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
Salman	Salman	k1gMnSc1	Salman
Abedi	Abed	k1gMnPc1	Abed
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
útoku	útok	k1gInSc3	útok
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
teroristická	teroristický	k2eAgFnSc1d1	teroristická
skupina	skupina	k1gFnSc1	skupina
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
a	a	k8xC	a
podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
Peninským	Peninský	k2eAgNnSc7d1	Peninský
vřesovištěm	vřesoviště	k1gNnSc7	vřesoviště
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc2	jih
Cheshirskou	Cheshirský	k2eAgFnSc7d1	Cheshirský
nížinou	nížina	k1gFnSc7	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
situováno	situován	k2eAgNnSc1d1	situováno
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Irwell	Irwella	k1gFnPc2	Irwella
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
soutoku	soutok	k1gInSc2	soutok
řek	řeka	k1gFnPc2	řeka
Medlock	Medlocka	k1gFnPc2	Medlocka
a	a	k8xC	a
Irk	Irk	k1gFnPc2	Irk
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Mersey	Mersea	k1gFnSc2	Mersea
protéká	protékat	k5eAaImIp3nS	protékat
jižním	jižní	k2eAgInSc7d1	jižní
okrajem	okraj	k1gInSc7	okraj
Manchesteru	Manchester	k1gInSc2	Manchester
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
především	především	k9	především
jeho	jeho	k3xOp3gFnSc1	jeho
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
tak	tak	k9	tak
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
budov	budova	k1gFnPc2	budova
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
vřesoviště	vřesoviště	k1gNnSc4	vřesoviště
<g/>
.	.	kIx.	.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
charakteristika	charakteristika	k1gFnSc1	charakteristika
města	město	k1gNnSc2	město
měla	mít	k5eAaImAgFnS	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
dostupnost	dostupnost	k1gFnSc1	dostupnost
vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
blízkost	blízkost	k1gFnSc4	blízkost
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
má	mít	k5eAaImIp3nS	mít
pověst	pověst	k1gFnSc4	pověst
města	město	k1gNnSc2	město
s	s	k7c7	s
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
podnebím	podnebí	k1gNnSc7	podnebí
a	a	k8xC	a
deštivým	deštivý	k2eAgNnSc7d1	deštivé
počasím	počasí	k1gNnSc7	počasí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
průměrný	průměrný	k2eAgInSc4d1	průměrný
roční	roční	k2eAgInSc4d1	roční
úhrn	úhrn	k1gInSc4	úhrn
srážek	srážka	k1gFnPc2	srážka
809	[number]	k4	809
mm	mm	kA	mm
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
například	například	k6eAd1	například
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Plymouth	Plymouth	k1gInSc1	Plymouth
<g/>
,	,	kIx,	,
Cardiff	Cardiff	k1gInSc1	Cardiff
nebo	nebo	k8xC	nebo
Glasgow	Glasgow	k1gNnSc1	Glasgow
(	(	kIx(	(
<g/>
americký	americký	k2eAgMnSc1d1	americký
New	New	k1gMnSc1	New
York	York	k1gInSc4	York
má	mít	k5eAaImIp3nS	mít
úhrn	úhrn	k1gInSc4	úhrn
srážek	srážka	k1gFnPc2	srážka
1200	[number]	k4	1200
mm	mm	kA	mm
a	a	k8xC	a
italský	italský	k2eAgInSc1d1	italský
Řím	Řím	k1gInSc1	Řím
má	mít	k5eAaImIp3nS	mít
srážek	srážka	k1gFnPc2	srážka
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
množství	množství	k1gNnSc1	množství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
mívají	mívat	k5eAaImIp3nP	mívat
relativně	relativně	k6eAd1	relativně
slabou	slabý	k2eAgFnSc4d1	slabá
intenzitu	intenzita	k1gFnSc4	intenzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trvají	trvat	k5eAaImIp3nP	trvat
většinou	většinou	k6eAd1	většinou
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
vlhkost	vlhkost	k1gFnSc4	vlhkost
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Etnický	etnický	k2eAgInSc1d1	etnický
původ	původ	k1gInSc1	původ
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
66,7	[number]	k4	66,7
<g/>
%	%	kIx~	%
–	–	k?	–
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
59,3	[number]	k4	59,3
<g/>
%	%	kIx~	%
bílí	bílý	k2eAgMnPc1d1	bílý
Britové	Brit	k1gMnPc1	Brit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
17,1	[number]	k4	17,1
<g/>
%	%	kIx~	%
–	–	k?	–
Asiaté	Asiat	k1gMnPc1	Asiat
</s>
</p>
<p>
<s>
8,6	[number]	k4	8,6
<g/>
%	%	kIx~	%
–	–	k?	–
černoši	černoch	k1gMnPc1	černoch
</s>
</p>
<p>
<s>
4,7	[number]	k4	4,7
<g/>
%	%	kIx~	%
–	–	k?	–
míšenci	míšenec	k1gMnPc1	míšenec
</s>
</p>
<p>
<s>
1,9	[number]	k4	1,9
<g/>
%	%	kIx~	%
–	–	k?	–
Arabové	Arab	k1gMnPc1	Arab
</s>
</p>
<p>
<s>
1,2	[number]	k4	1,2
<g/>
%	%	kIx~	%
–	–	k?	–
ostatníNáboženství	ostatníNáboženství	k1gNnSc1	ostatníNáboženství
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
48,7	[number]	k4	48,7
<g/>
%	%	kIx~	%
–	–	k?	–
křesťanství	křesťanství	k1gNnSc2	křesťanství
</s>
</p>
<p>
<s>
15,8	[number]	k4	15,8
<g/>
%	%	kIx~	%
–	–	k?	–
islám	islám	k1gInSc1	islám
</s>
</p>
<p>
<s>
1,1	[number]	k4	1,1
<g/>
%	%	kIx~	%
–	–	k?	–
hinduismus	hinduismus	k1gInSc1	hinduismus
</s>
</p>
<p>
<s>
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
–	–	k?	–
sikhismus	sikhismus	k1gInSc1	sikhismus
</s>
</p>
<p>
<s>
0,8	[number]	k4	0,8
<g/>
%	%	kIx~	%
–	–	k?	–
buddhismus	buddhismus	k1gInSc1	buddhismus
</s>
</p>
<p>
<s>
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
–	–	k?	–
judaismus	judaismus	k1gInSc1	judaismus
</s>
</p>
<p>
<s>
0,4	[number]	k4	0,4
<g/>
%	%	kIx~	%
–	–	k?	–
ostatní	ostatní	k2eAgNnSc4d1	ostatní
náboženství	náboženství	k1gNnSc4	náboženství
</s>
</p>
<p>
<s>
25,3	[number]	k4	25,3
<g/>
%	%	kIx~	%
–	–	k?	–
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
</s>
</p>
<p>
<s>
6,9	[number]	k4	6,9
<g/>
%	%	kIx~	%
–	–	k?	–
neuvedeno	uvést	k5eNaPmNgNnS	uvést
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Manchester	Manchester	k1gInSc4	Manchester
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kancelářských	kancelářský	k2eAgInPc2d1	kancelářský
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
čtvrť	čtvrť	k1gFnSc1	čtvrť
Central	Central	k1gFnSc2	Central
Business	business	k1gInSc1	business
District	District	k1gInSc1	District
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
vedle	vedle	k7c2	vedle
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
se	s	k7c7	s
středem	střed	k1gInSc7	střed
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
Mosley	Moslea	k1gFnSc2	Moslea
Street	Street	k1gInSc1	Street
<g/>
,	,	kIx,	,
Deansgate	Deansgat	k1gMnSc5	Deansgat
<g/>
,	,	kIx,	,
King	King	k1gInSc4	King
Street	Streeta	k1gFnPc2	Streeta
a	a	k8xC	a
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
kanceláře	kancelář	k1gFnPc1	kancelář
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Deansgate	Deansgat	k1gInSc5	Deansgat
ve	v	k7c4	v
Spinningfields	Spinningfields	k1gInSc4	Spinningfields
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
významnou	významný	k2eAgFnSc7d1	významná
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
centrála	centrála	k1gFnSc1	centrála
Royal	Royal	k1gMnSc1	Royal
Bank	bank	k1gInSc1	bank
of	of	k?	of
Scotland	Scotland	k1gInSc1	Scotland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
v	v	k7c4	v
Salford	Salford	k1gInSc4	Salford
Quays	Quaysa	k1gFnPc2	Quaysa
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
regenerace	regenerace	k1gFnSc1	regenerace
oblasti	oblast	k1gFnSc2	oblast
využívané	využívaný	k2eAgFnPc1d1	využívaná
dříve	dříve	k6eAd2	dříve
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
londýnské	londýnský	k2eAgFnSc2d1	londýnská
oblasti	oblast	k1gFnSc2	oblast
Docklands	Docklandsa	k1gFnPc2	Docklandsa
<g/>
)	)	kIx)	)
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
call	calla	k1gFnPc2	calla
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přestěhování	přestěhování	k1gNnSc3	přestěhování
některých	některý	k3yIgFnPc2	některý
redakcí	redakce	k1gFnPc2	redakce
BBC	BBC	kA	BBC
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
právě	právě	k6eAd1	právě
do	do	k7c2	do
manchesterských	manchesterský	k2eAgFnPc2d1	Manchesterská
Salford	Salforda	k1gFnPc2	Salforda
Quays	Quays	k1gInSc1	Quays
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc1	vytvoření
tzv.	tzv.	kA	tzv.
BBC	BBC	kA	BBC
North	North	k1gMnSc1	North
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
nákupním	nákupní	k2eAgNnSc7d1	nákupní
centrem	centrum	k1gNnSc7	centrum
severu	sever	k1gInSc2	sever
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
dvě	dva	k4xCgNnPc4	dva
hlavní	hlavní	k2eAgNnPc4d1	hlavní
obchodní	obchodní	k2eAgNnPc4d1	obchodní
centra	centrum	k1gNnPc4	centrum
–	–	k?	–
Manchester	Manchester	k1gInSc1	Manchester
Arndale	Arndal	k1gMnSc5	Arndal
Centre	centr	k1gInSc5	centr
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
a	a	k8xC	a
Trafford	Trafforda	k1gFnPc2	Trafforda
Centre	centr	k1gInSc5	centr
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
Manchesteru	Manchester	k1gInSc2	Manchester
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
menší	malý	k2eAgNnPc4d2	menší
obchodní	obchodní	k2eAgNnPc4d1	obchodní
centra	centrum	k1gNnPc4	centrum
–	–	k?	–
Triangle	triangl	k1gInSc5	triangl
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
mladou	mladý	k2eAgFnSc4d1	mladá
klientelu	klientela	k1gFnSc4	klientela
<g/>
,	,	kIx,	,
a	a	k8xC	a
Royal	Royal	k1gInSc1	Royal
Exchange	Exchang	k1gFnSc2	Exchang
Centre	centr	k1gInSc5	centr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Shambles	Shamblesa	k1gFnPc2	Shamblesa
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
najít	najít	k5eAaPmF	najít
pobočky	pobočka	k1gFnPc4	pobočka
obchodních	obchodní	k2eAgInPc2d1	obchodní
řetězců	řetězec	k1gInPc2	řetězec
Harvey	Harvea	k1gFnSc2	Harvea
Nichols	Nicholsa	k1gFnPc2	Nicholsa
<g/>
,	,	kIx,	,
Marks	Marksa	k1gFnPc2	Marksa
&	&	k?	&
Spencer	Spencra	k1gFnPc2	Spencra
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
módních	módní	k2eAgInPc2d1	módní
butiků	butik	k1gInPc2	butik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
byl	být	k5eAaImAgInS	být
Manchester	Manchester	k1gInSc1	Manchester
vyčleněn	vyčlenit	k5eAaPmNgInS	vyčlenit
z	z	k7c2	z
hrabství	hrabství	k1gNnSc2	hrabství
Lancashire	Lancashir	k1gInSc5	Lancashir
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
metropolitní	metropolitní	k2eAgInSc1d1	metropolitní
distrikt	distrikt	k1gInSc1	distrikt
Manchester	Manchester	k1gInSc1	Manchester
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
4	[number]	k4	4
úrovní	úroveň	k1gFnPc2	úroveň
správy	správa	k1gFnSc2	správa
–	–	k?	–
Rada	Rada	k1gMnSc1	Rada
města	město	k1gNnSc2	město
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
region	region	k1gInSc1	region
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
Parlament	parlament	k1gInSc1	parlament
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
součástí	součást	k1gFnSc7	součást
metropolitního	metropolitní	k2eAgNnSc2d1	metropolitní
hrabství	hrabství	k1gNnSc2	hrabství
Velký	velký	k2eAgInSc1d1	velký
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
rada	rada	k1gFnSc1	rada
tohoto	tento	k3xDgNnSc2	tento
hrabství	hrabství	k1gNnSc2	hrabství
byla	být	k5eAaImAgNnP	být
zrušena	zrušit	k5eAaPmNgNnP	zrušit
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
a	a	k8xC	a
město	město	k1gNnSc1	město
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
distrikty	distrikt	k1gInPc1	distrikt
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
správními	správní	k2eAgFnPc7d1	správní
jednotkami	jednotka	k1gFnPc7	jednotka
(	(	kIx(	(
<g/>
unitary	unitar	k1gInPc1	unitar
authority	authorita	k1gFnSc2	authorita
<g/>
)	)	kIx)	)
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
činností	činnost	k1gFnPc2	činnost
zajišťovaných	zajišťovaný	k2eAgFnPc2d1	zajišťovaná
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
hrabství	hrabství	k1gNnSc2	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
nemají	mít	k5eNaImIp3nP	mít
úroveň	úroveň	k1gFnSc4	úroveň
správních	správní	k2eAgFnPc2d1	správní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rada	Rada	k1gMnSc1	Rada
města	město	k1gNnSc2	město
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
správním	správní	k2eAgInSc7d1	správní
orgánem	orgán	k1gInSc7	orgán
pro	pro	k7c4	pro
metropolitní	metropolitní	k2eAgInSc4d1	metropolitní
distrikt	distrikt	k1gInSc4	distrikt
Manchester	Manchester	k1gInSc1	Manchester
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
32	[number]	k4	32
volebních	volební	k2eAgInPc2d1	volební
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
je	být	k5eAaImIp3nS	být
voleno	volen	k2eAgNnSc1d1	voleno
96	[number]	k4	96
radních	radní	k1gMnPc2	radní
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volební	volební	k2eAgInPc1d1	volební
obvody	obvod	k1gInPc1	obvod
do	do	k7c2	do
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Parlamentu	parlament	k1gInSc2	parlament
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
je	být	k5eAaImIp3nS	být
Manchester	Manchester	k1gInSc1	Manchester
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
obvodů	obvod	k1gInPc2	obvod
<g/>
:	:	kIx,	:
Manchester	Manchester	k1gInSc1	Manchester
Central	Central	k1gFnSc2	Central
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
–	–	k?	–
Blackley	Blacklea	k1gFnPc1	Blacklea
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
–	–	k?	–
Gorton	Gorton	k1gInSc1	Gorton
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
–	–	k?	–
Withington	Withington	k1gInSc1	Withington
a	a	k8xC	a
Wythenshawe	Wythenshawe	k1gFnSc1	Wythenshawe
a	a	k8xC	a
Sale	Sale	k1gFnSc1	Sale
East	Easta	k1gFnPc2	Easta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
volebního	volební	k2eAgInSc2d1	volební
okrsku	okrsek	k1gInSc2	okrsek
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Anglie	Anglie	k1gFnSc2	Anglie
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Parlamentu	parlament	k1gInSc2	parlament
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
úroveň	úroveň	k1gFnSc4	úroveň
dopravní	dopravní	k2eAgFnSc2d1	dopravní
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
komunikací	komunikace	k1gFnPc2	komunikace
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejhustších	hustý	k2eAgFnPc2d3	nejhustší
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
Velkého	velký	k2eAgInSc2d1	velký
Manchesteru	Manchester	k1gInSc2	Manchester
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgNnSc4d3	veliký
procento	procento	k1gNnSc4	procento
dálnic	dálnice	k1gFnPc2	dálnice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Liverpool	Liverpool	k1gInSc1	Liverpool
and	and	k?	and
Manchester	Manchester	k1gInSc4	Manchester
Railway	Railwaa	k1gFnSc2	Railwaa
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
železniční	železniční	k2eAgFnSc1d1	železniční
tratí	tratit	k5eAaImIp3nS	tratit
na	na	k7c6	na
světě	svět	k1gInSc6	svět
dopravující	dopravující	k2eAgFnSc2d1	dopravující
cestující	cestující	k1gFnSc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
dopravní	dopravní	k2eAgFnSc7d1	dopravní
službou	služba	k1gFnSc7	služba
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
je	být	k5eAaImIp3nS	být
taxislužba	taxislužba	k1gFnSc1	taxislužba
s	s	k7c7	s
typickými	typický	k2eAgInPc7d1	typický
vozy	vůz	k1gInPc7	vůz
Black	Blacka	k1gFnPc2	Blacka
Cabs	Cabsa	k1gFnPc2	Cabsa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
hustou	hustý	k2eAgFnSc7d1	hustá
sítí	síť	k1gFnSc7	síť
stanovišť	stanoviště	k1gNnPc2	stanoviště
taxislužby	taxislužba	k1gFnSc2	taxislužba
protkáno	protkán	k2eAgNnSc4d1	protkáno
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
i	i	k8xC	i
cena	cena	k1gFnSc1	cena
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
přepravy	přeprava	k1gFnSc2	přeprava
přístupná	přístupný	k2eAgFnSc1d1	přístupná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Manchester	Manchester	k1gInSc1	Manchester
Ringway	Ringwaa	k1gFnSc2	Ringwaa
Airport	Airport	k1gInSc4	Airport
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc1	třetí
letiště	letiště	k1gNnSc1	letiště
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
odbavených	odbavený	k2eAgMnPc2d1	odbavený
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
odbavilo	odbavit	k5eAaPmAgNnS	odbavit
22,1	[number]	k4	22,1
miliónů	milión	k4xCgInPc2	milión
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
přímé	přímý	k2eAgNnSc1d1	přímé
spojení	spojení	k1gNnSc1	spojení
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
180	[number]	k4	180
míst	místo	k1gNnPc2	místo
provozovaných	provozovaný	k2eAgMnPc2d1	provozovaný
90	[number]	k4	90
leteckými	letecký	k2eAgFnPc7d1	letecká
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
destinacemi	destinace	k1gFnPc7	destinace
dostupnými	dostupný	k2eAgFnPc7d1	dostupná
přímým	přímý	k2eAgNnSc7d1	přímé
spojením	spojení	k1gNnSc7	spojení
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
New	New	k1gFnSc7	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
<g/>
,	,	kIx,	,
Filadelfie	Filadelfie	k1gFnSc1	Filadelfie
<g/>
,	,	kIx,	,
Atlanta	Atlanta	k1gFnSc1	Atlanta
<g/>
,	,	kIx,	,
Orlando	Orlanda	k1gFnSc5	Orlanda
<g/>
,	,	kIx,	,
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
Houston	Houston	k1gInSc1	Houston
<g/>
,	,	kIx,	,
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Antigua	Antigua	k1gFnSc1	Antigua
<g/>
,	,	kIx,	,
Barbados	Barbados	k1gInSc1	Barbados
<g/>
,	,	kIx,	,
Damašek	Damašek	k1gInSc1	Damašek
<g/>
,	,	kIx,	,
Dubai	Dubai	k1gNnSc1	Dubai
<g/>
,	,	kIx,	,
Abu	Abu	k1gFnSc1	Abu
Dhabi	Dhabi	k1gNnSc2	Dhabi
<g/>
,	,	kIx,	,
Doha	Dohum	k1gNnSc2	Dohum
<g/>
,	,	kIx,	,
Teherán	Teherán	k1gInSc1	Teherán
<g/>
,	,	kIx,	,
Karáčí	Karáčí	k1gNnSc1	Karáčí
<g/>
,	,	kIx,	,
Islamabad	Islamabad	k1gInSc1	Islamabad
<g/>
,	,	kIx,	,
Láhaur	Láhaur	k1gInSc1	Láhaur
<g/>
,	,	kIx,	,
Kuala	Kuala	k1gFnSc1	Kuala
Lumpur	Lumpura	k1gFnPc2	Lumpura
a	a	k8xC	a
Hong	Honga	k1gFnPc2	Honga
Kong	Kongo	k1gNnPc2	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
linka	linka	k1gFnSc1	linka
mezi	mezi	k7c7	mezi
Manchesterem	Manchester	k1gInSc7	Manchester
a	a	k8xC	a
Londýnem	Londýn	k1gInSc7	Londýn
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
rušných	rušný	k2eAgInPc2d1	rušný
vnitrostátních	vnitrostátní	k2eAgInPc2d1	vnitrostátní
spojů	spoj	k1gInPc2	spoj
a	a	k8xC	a
silně	silně	k6eAd1	silně
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
železniční	železniční	k2eAgFnSc3d1	železniční
dopravě	doprava	k1gFnSc3	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
Barton	Barton	k1gInSc1	Barton
Aerodrome	aerodrom	k1gInSc5	aerodrom
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
především	především	k9	především
jako	jako	k9	jako
letiště	letiště	k1gNnSc4	letiště
pro	pro	k7c4	pro
helikoptéry	helikoptéra	k1gFnPc4	helikoptéra
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
krátkou	krátký	k2eAgFnSc4d1	krátká
travnatou	travnatý	k2eAgFnSc4d1	travnatá
dráhu	dráha	k1gFnSc4	dráha
přistávají	přistávat	k5eAaImIp3nP	přistávat
malá	malý	k2eAgNnPc1d1	malé
letadla	letadlo	k1gNnPc1	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
letišti	letiště	k1gNnSc6	letiště
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
nejstarší	starý	k2eAgFnSc1d3	nejstarší
letecká	letecký	k2eAgFnSc1d1	letecká
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
věž	věž	k1gFnSc1	věž
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
silniční	silniční	k2eAgInSc4d1	silniční
okruh	okruh	k1gInSc4	okruh
–	–	k?	–
M	M	kA	M
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
prochází	procházet	k5eAaImIp3nS	procházet
M60	M60	k1gMnPc2	M60
okrajovými	okrajový	k2eAgFnPc7d1	okrajová
částmi	část	k1gFnPc7	část
města	město	k1gNnSc2	město
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
tak	tak	k6eAd1	tak
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
dálnicemi	dálnice	k1gFnPc7	dálnice
spojujícími	spojující	k2eAgFnPc7d1	spojující
Manchester	Manchester	k1gInSc1	Manchester
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
jsou	být	k5eAaImIp3nP	být
M	M	kA	M
<g/>
56	[number]	k4	56
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
M62	M62	k1gFnSc1	M62
a	a	k8xC	a
M	M	kA	M
<g/>
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ústí	ústit	k5eAaImIp3nS	ústit
na	na	k7c6	na
M	M	kA	M
<g/>
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
drží	držet	k5eAaImIp3nS	držet
historické	historický	k2eAgNnSc4d1	historické
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
provozování	provozování	k1gNnSc6	provozování
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
železniční	železniční	k2eAgFnSc1d1	železniční
linka	linka	k1gFnSc1	linka
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
cestujících	cestující	k1gFnPc2	cestující
Liverpool	Liverpool	k1gInSc4	Liverpool
and	and	k?	and
Manchester	Manchester	k1gInSc1	Manchester
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgNnPc2d1	další
50	[number]	k4	50
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
několika	několik	k4yIc7	několik
železničními	železniční	k2eAgFnPc7d1	železniční
stanicemi	stanice	k1gFnPc7	stanice
–	–	k?	–
Manchester	Manchester	k1gInSc1	Manchester
London	London	k1gMnSc1	London
Road	Road	k1gMnSc1	Road
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Manchester	Manchester	k1gInSc1	Manchester
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
Central	Central	k1gFnSc1	Central
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
Mayfield	Mayfield	k1gInSc1	Mayfield
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
Exchange	Exchang	k1gInSc2	Exchang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgNnSc4	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pro	pro	k7c4	pro
cestující	cestující	k1gFnSc4	cestující
uzavřeny	uzavřen	k2eAgFnPc1d1	uzavřena
<g/>
,	,	kIx,	,
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
zůstaly	zůstat	k5eAaPmAgInP	zůstat
pouze	pouze	k6eAd1	pouze
stanice	stanice	k1gFnPc4	stanice
Manchester	Manchester	k1gInSc1	Manchester
Victoria	Victorium	k1gNnSc2	Victorium
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
<g/>
.	.	kIx.	.
</s>
<s>
Expresní	expresní	k2eAgInPc1d1	expresní
vlaky	vlak	k1gInPc1	vlak
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Piccadily	Piccadily	k1gFnSc1	Piccadily
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
menších	malý	k2eAgFnPc2d2	menší
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
–	–	k?	–
Manchester	Manchester	k1gInSc1	Manchester
Oxford	Oxford	k1gInSc1	Oxford
Road	Road	k1gInSc4	Road
<g/>
,	,	kIx,	,
Deansgate	Deansgat	k1gInSc5	Deansgat
a	a	k8xC	a
Salford	Salford	k1gMnSc1	Salford
Central	Central	k1gMnSc1	Central
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
není	být	k5eNaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
systémem	systém	k1gInSc7	systém
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
existuje	existovat	k5eAaImIp3nS	existovat
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
systém	systém	k1gInSc1	systém
dopravy	doprava	k1gFnSc2	doprava
zvaný	zvaný	k2eAgInSc1d1	zvaný
Manchester	Manchester	k1gInSc1	Manchester
Metrolink	Metrolink	k1gInSc1	Metrolink
<g/>
,	,	kIx,	,
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
společností	společnost	k1gFnSc7	společnost
Serco	Serco	k1gNnSc4	Serco
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
spojuje	spojovat	k5eAaImIp3nS	spojovat
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
Altrinchamem	Altrincham	k1gInSc7	Altrincham
<g/>
,	,	kIx,	,
Ecclesem	Eccles	k1gInSc7	Eccles
<g/>
,	,	kIx,	,
Bury	Bury	k?	Bury
<g/>
,	,	kIx,	,
Ashtonem	Ashton	k1gInSc7	Ashton
<g/>
,	,	kIx,	,
East	East	k1gMnSc1	East
Didsbury	Didsbura	k1gFnSc2	Didsbura
a	a	k8xC	a
letištěm	letiště	k1gNnSc7	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
57	[number]	k4	57
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
92	[number]	k4	92
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
třikrát	třikrát	k6eAd1	třikrát
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
ze	z	k7c2	z
znovuotevřených	znovuotevřený	k2eAgInPc2d1	znovuotevřený
britských	britský	k2eAgInPc2d1	britský
tramvajových	tramvajový	k2eAgInPc2d1	tramvajový
systémů	systém	k1gInPc2	systém
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
renesance	renesance	k1gFnSc2	renesance
tramvají	tramvaj	k1gFnPc2	tramvaj
ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
interval	interval	k1gInSc1	interval
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
spoji	spoj	k1gInPc7	spoj
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
tu	tu	k6eAd1	tu
tramvaje	tramvaj	k1gFnPc1	tramvaj
přepraví	přepravit	k5eAaPmIp3nP	přepravit
téměř	téměř	k6eAd1	téměř
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
záměr	záměr	k1gInSc4	záměr
podpořit	podpořit	k5eAaPmF	podpořit
rozšíření	rozšíření	k1gNnSc4	rozšíření
tohoto	tento	k3xDgInSc2	tento
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgNnSc6	jenž
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
schopen	schopen	k2eAgMnSc1d1	schopen
přepravit	přepravit	k5eAaPmF	přepravit
až	až	k9	až
50	[number]	k4	50
miliónů	milión	k4xCgInPc2	milión
cestujících	cestující	k1gFnPc2	cestující
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rychlodrážní	rychlodrážní	k2eAgInSc4d1	rychlodrážní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
provoz	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
jako	jako	k8xS	jako
všechny	všechen	k3xTgInPc4	všechen
kromě	kromě	k7c2	kromě
blackpoolského	blackpoolský	k2eAgInSc2d1	blackpoolský
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
provozoval	provozovat	k5eAaImAgMnS	provozovat
až	až	k6eAd1	až
od	od	k7c2	od
konce	konec	k1gInSc2	konec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
pouliční	pouliční	k2eAgMnPc1d1	pouliční
double-deckerové	doubleecker	k1gMnPc1	double-decker
(	(	kIx(	(
<g/>
dvoupodlažní	dvoupodlažní	k2eAgFnPc4d1	dvoupodlažní
<g/>
)	)	kIx)	)
tramvaje	tramvaj	k1gFnPc4	tramvaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Manchesteru	Manchester	k1gInSc2	Manchester
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
slouží	sloužit	k5eAaImIp3nS	sloužit
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
spojující	spojující	k2eAgNnSc4d1	spojující
město	město	k1gNnSc4	město
se	s	k7c7	s
satelitními	satelitní	k2eAgFnPc7d1	satelitní
částmi	část	k1gFnPc7	část
a	a	k8xC	a
okolními	okolní	k2eAgFnPc7d1	okolní
vesnicemi	vesnice	k1gFnPc7	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
ve	v	k7c6	v
městě	město	k1gNnSc6	město
provozuje	provozovat	k5eAaImIp3nS	provozovat
mnoho	mnoho	k4c4	mnoho
společností	společnost	k1gFnPc2	společnost
–	–	k?	–
First	First	k1gMnSc1	First
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
Stagecoach	Stagecoach	k1gMnSc1	Stagecoach
<g/>
,	,	kIx,	,
Finglands	Finglands	k1gInSc1	Finglands
<g/>
,	,	kIx,	,
UK	UK	kA	UK
North	North	k1gInSc1	North
<g/>
,	,	kIx,	,
Arriva	Arriva	k1gFnSc1	Arriva
a	a	k8xC	a
R.	R.	kA	R.
Bullock	Bullock	k1gInSc1	Bullock
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
vytížených	vytížený	k2eAgFnPc2d1	vytížená
tras	trasa	k1gFnPc2	trasa
je	být	k5eAaImIp3nS	být
spoj	spoj	k1gFnSc4	spoj
Oxford	Oxford	k1gInSc1	Oxford
Road	Road	k1gMnSc1	Road
–	–	k?	–
Wilmslow	Wilmslow	k1gMnSc1	Wilmslow
Road	Road	k1gMnSc1	Road
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejrušnějších	rušný	k2eAgFnPc2d3	nejrušnější
autobusových	autobusový	k2eAgFnPc2d1	autobusová
tras	trasa	k1gFnPc2	trasa
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
dopravují	dopravovat	k5eAaImIp3nP	dopravovat
studenti	student	k1gMnPc1	student
a	a	k8xC	a
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
z	z	k7c2	z
okrajových	okrajový	k2eAgFnPc2d1	okrajová
čtvrtí	čtvrt	k1gFnPc2	čtvrt
města	město	k1gNnSc2	město
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
učeben	učebna	k1gFnPc2	učebna
a	a	k8xC	a
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
First	First	k1gMnSc1	First
Group	Group	k1gMnSc1	Group
také	také	k9	také
provozuje	provozovat	k5eAaImIp3nS	provozovat
bezplatnou	bezplatný	k2eAgFnSc4d1	bezplatná
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
službu	služba	k1gFnSc4	služba
Metroshuttle	Metroshuttle	k1gFnSc2	Metroshuttle
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
důležité	důležitý	k2eAgFnPc4d1	důležitá
oblasti	oblast	k1gFnPc4	oblast
města	město	k1gNnSc2	město
–	–	k?	–
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Manchester	Manchester	k1gInSc1	Manchester
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
Piccadilly	Piccadilla	k1gFnSc2	Piccadilla
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
Oxford	Oxford	k1gInSc1	Oxford
Road	Road	k1gInSc1	Road
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
čtvrtí	čtvrt	k1gFnSc7	čtvrt
<g/>
,	,	kIx,	,
Deansgate	Deansgat	k1gMnSc5	Deansgat
<g/>
,	,	kIx,	,
Salford	Salford	k1gMnSc1	Salford
Central	Central	k1gMnSc1	Central
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Square	square	k1gInSc1	square
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInPc1d1	populární
spoje	spoj	k1gInPc1	spoj
bývají	bývat	k5eAaImIp3nP	bývat
přeplněné	přeplněný	k2eAgInPc1d1	přeplněný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgFnP	provozovat
tři	tři	k4xCgFnPc1	tři
linky	linka	k1gFnPc1	linka
odlišené	odlišený	k2eAgFnPc1d1	odlišená
barevně	barevně	k6eAd1	barevně
–	–	k?	–
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
a	a	k8xC	a
purpurová	purpurový	k2eAgFnSc1d1	purpurová
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
spojů	spoj	k1gInPc2	spoj
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
tak	tak	k6eAd1	tak
dopravní	dopravní	k2eAgFnSc1d1	dopravní
síť	síť	k1gFnSc1	síť
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
železnici	železnice	k1gFnSc4	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
linky	linka	k1gFnPc1	linka
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
jezdí	jezdit	k5eAaImIp3nP	jezdit
kloubové	kloubový	k2eAgInPc1d1	kloubový
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
linky	linka	k1gFnPc1	linka
Bury	Bury	k?	Bury
–	–	k?	–
Manchester	Manchester	k1gInSc1	Manchester
nebo	nebo	k8xC	nebo
Bolton	Bolton	k1gInSc1	Bolton
–	–	k?	–
Manchester	Manchester	k1gInSc1	Manchester
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
stanovištěm	stanoviště	k1gNnSc7	stanoviště
pro	pro	k7c4	pro
dálkové	dálkový	k2eAgInPc4d1	dálkový
autobusy	autobus	k1gInPc4	autobus
(	(	kIx(	(
<g/>
spoje	spoj	k1gFnPc4	spoj
provozuje	provozovat	k5eAaImIp3nS	provozovat
především	především	k9	především
National	National	k1gMnSc1	National
Express	express	k1gInSc1	express
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Manchester	Manchester	k1gInSc4	Manchester
Central	Central	k1gFnSc1	Central
Coach	Coach	k1gInSc1	Coach
Station	station	k1gInSc1	station
na	na	k7c4	na
Chorlton	Chorlton	k1gInSc4	Chorlton
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
moderní	moderní	k2eAgFnSc4d1	moderní
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vybavené	vybavený	k2eAgNnSc1d1	vybavené
stanoviště	stanoviště	k1gNnSc1	stanoviště
(	(	kIx(	(
<g/>
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Říční	říční	k2eAgFnSc1d1	říční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
odkazům	odkaz	k1gInPc3	odkaz
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
patří	patřit	k5eAaImIp3nS	patřit
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
kanálů	kanál	k1gInPc2	kanál
<g/>
:	:	kIx,	:
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
Bolton	Bolton	k1gInSc1	Bolton
&	&	k?	&
Bury	Bury	k?	Bury
Canal	Canal	k1gInSc4	Canal
<g/>
,	,	kIx,	,
Rochdale	Rochdala	k1gFnSc3	Rochdala
Canal	Canal	k1gInSc1	Canal
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
Ship	Ship	k1gInSc1	Ship
Canal	Canal	k1gInSc4	Canal
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
Bridgewater	Bridgewater	k1gMnSc1	Bridgewater
Canal	Canal	k1gMnSc1	Canal
<g/>
,	,	kIx,	,
Ashton	Ashton	k1gInSc1	Ashton
Canal	Canal	k1gInSc1	Canal
a	a	k8xC	a
Leeds	Leeds	k1gInSc1	Leeds
&	&	k?	&
Liverpool	Liverpool	k1gInSc1	Liverpool
Canal	Canal	k1gInSc1	Canal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
pro	pro	k7c4	pro
výletní	výletní	k2eAgFnPc4d1	výletní
plavby	plavba	k1gFnPc4	plavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velkého	velký	k2eAgInSc2d1	velký
Manchesteru	Manchester	k1gInSc2	Manchester
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
galerií	galerie	k1gFnPc2	galerie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lowry	Lowr	k1gInPc1	Lowr
v	v	k7c4	v
Salford	Salford	k1gInSc4	Salford
Quays	Quays	k1gInSc1	Quays
–	–	k?	–
obrazárna	obrazárna	k1gFnSc1	obrazárna
s	s	k7c7	s
díly	díl	k1gInPc1	díl
salfordského	salfordský	k2eAgMnSc2d1	salfordský
malíře	malíř	k1gMnSc2	malíř
L.	L.	kA	L.
S.	S.	kA	S.
Lowryho	Lowry	k1gMnSc2	Lowry
</s>
</p>
<p>
<s>
Athenaeum	Athenaeum	k1gInSc1	Athenaeum
</s>
</p>
<p>
<s>
Salford	Salford	k6eAd1	Salford
Museum	museum	k1gNnSc1	museum
and	and	k?	and
Art	Art	k1gFnSc1	Art
Gallery	Galler	k1gInPc4	Galler
v	v	k7c4	v
Salfordu	Salforda	k1gFnSc4	Salforda
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
Art	Art	k1gFnSc2	Art
Gallery	Galler	k1gInPc7	Galler
</s>
</p>
<p>
<s>
Whitworth	Whitworth	k1gMnSc1	Whitworth
Art	Art	k1gMnSc3	Art
Gallery	Galler	k1gMnPc4	Galler
</s>
</p>
<p>
<s>
Chinese	Chinese	k1gFnSc1	Chinese
Arts	Artsa	k1gFnPc2	Artsa
Centre	centr	k1gInSc5	centr
</s>
</p>
<p>
<s>
Cornerhouse	Cornerhouse	k6eAd1	Cornerhouse
</s>
</p>
<p>
<s>
Castlefield	Castlefield	k6eAd1	Castlefield
Gallery	Galler	k1gInPc1	Galler
</s>
</p>
<p>
<s>
Cube	Cube	k6eAd1	Cube
Gallery	Galler	k1gInPc1	Galler
</s>
</p>
<p>
<s>
Comme	Commat	k5eAaPmIp3nS	Commat
Ca	ca	kA	ca
Art	Art	k1gFnSc1	Art
Gallery	Galler	k1gMnPc4	Galler
</s>
</p>
<p>
<s>
Barn	Barn	k1gInSc1	Barn
Gallery	Galler	k1gInPc1	Galler
</s>
</p>
<p>
<s>
===	===	k?	===
Muzea	muzeum	k1gNnSc2	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
Muzea	muzeum	k1gNnPc1	muzeum
v	v	k7c6	v
Machesteru	Machester	k1gInSc6	Machester
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Greater	Greater	k1gMnSc1	Greater
Manchester	Manchester	k1gInSc4	Manchester
Police	police	k1gFnSc2	police
Museum	museum	k1gNnSc4	museum
</s>
</p>
<p>
<s>
Imperial	Imperial	k1gInSc1	Imperial
War	War	k1gFnPc2	War
Museum	museum	k1gNnSc1	museum
North	North	k1gMnSc1	North
v	v	k7c6	v
Trafford	Trafforda	k1gFnPc2	Trafforda
Parku	park	k1gInSc2	park
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
Jewish	Jewisha	k1gFnPc2	Jewisha
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Science	Science	k1gFnSc1	Science
and	and	k?	and
Industry	Industra	k1gFnSc2	Industra
</s>
</p>
<p>
<s>
Pankhurst	Pankhurst	k1gFnSc1	Pankhurst
Centre	centr	k1gInSc5	centr
</s>
</p>
<p>
<s>
People	People	k6eAd1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
History	Histor	k1gMnPc7	Histor
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
Urbis	Urbis	k1gFnSc1	Urbis
-	-	kIx~	-
muzeum	muzeum	k1gNnSc1	muzeum
života	život	k1gInSc2	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
</s>
</p>
<p>
<s>
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Costume	Costum	k1gInSc5	Costum
</s>
</p>
<p>
<s>
===	===	k?	===
Divadla	divadlo	k1gNnSc2	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
svými	svůj	k3xOyFgNnPc7	svůj
skvělými	skvělý	k2eAgNnPc7d1	skvělé
divadly	divadlo	k1gNnPc7	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
sály	sál	k1gInPc1	sál
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
například	například	k6eAd1	například
Manchester	Manchester	k1gInSc1	Manchester
Opera	opera	k1gFnSc1	opera
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
komerční	komerční	k2eAgNnSc1d1	komerční
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
výjezdní	výjezdní	k2eAgNnPc4d1	výjezdní
představení	představení	k1gNnPc4	představení
divadelních	divadelní	k2eAgFnPc2d1	divadelní
společností	společnost	k1gFnPc2	společnost
z	z	k7c2	z
West	Westa	k1gFnPc2	Westa
Endu	Endus	k1gInSc2	Endus
<g/>
,	,	kIx,	,
Palace	Palace	k1gFnSc2	Palace
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
Royal	Royal	k1gMnSc1	Royal
Exchange	Exchangus	k1gMnSc5	Exchangus
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
,	,	kIx,	,
velká	velká	k1gFnSc1	velká
divadla	divadlo	k1gNnSc2	divadlo
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
burzy	burza	k1gFnSc2	burza
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Library	Librara	k1gFnPc1	Librara
Theatre	Theatr	k1gInSc5	Theatr
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
divadlo	divadlo	k1gNnSc1	divadlo
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
hlavní	hlavní	k2eAgFnSc2d1	hlavní
městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
divadla	divadlo	k1gNnPc4	divadlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
patří	patřit	k5eAaImIp3nP	patřit
Lowry	Lowra	k1gFnPc1	Lowra
a	a	k8xC	a
Studio	studio	k1gNnSc1	studio
Salford	Salforda	k1gFnPc2	Salforda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
menší	malý	k2eAgInPc4d2	menší
divadelní	divadelní	k2eAgInPc4d1	divadelní
sály	sál	k1gInPc4	sál
patří	patřit	k5eAaImIp3nS	patřit
Green	Green	k2eAgInSc1d1	Green
Room	Room	k1gInSc1	Room
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
okrajové	okrajový	k2eAgInPc4d1	okrajový
žánry	žánr	k1gInPc4	žánr
<g/>
,	,	kIx,	,
Contact	Contact	k2eAgInSc4d1	Contact
Theatre	Theatr	k1gInSc5	Theatr
-	-	kIx~	-
divadlo	divadlo	k1gNnSc4	divadlo
především	především	k9	především
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
diváky	divák	k1gMnPc4	divák
a	a	k8xC	a
Dancehouse	Dancehouse	k1gFnPc4	Dancehouse
<g/>
,	,	kIx,	,
s	s	k7c7	s
tanečními	taneční	k2eAgFnPc7d1	taneční
produkcemi	produkce	k1gFnPc7	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
dvou	dva	k4xCgFnPc2	dva
významných	významný	k2eAgFnPc2d1	významná
divadelních	divadelní	k2eAgFnPc2d1	divadelní
škol	škola	k1gFnPc2	škola
–	–	k?	–
School	School	k1gInSc1	School
of	of	k?	of
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
Arden	Ardeny	k1gFnPc2	Ardeny
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
.	.	kIx.	.
</s>
<s>
Royal	Royat	k5eAaImAgMnS	Royat
Northern	Northern	k1gMnSc1	Northern
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
Music	Music	k1gMnSc1	Music
má	mít	k5eAaImIp3nS	mít
vyčleněny	vyčleněn	k2eAgInPc4d1	vyčleněn
speciální	speciální	k2eAgInPc4d1	speciální
prostory	prostor	k1gInPc4	prostor
pro	pro	k7c4	pro
operní	operní	k2eAgFnSc4d1	operní
a	a	k8xC	a
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasická	klasický	k2eAgFnSc1d1	klasická
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
dvou	dva	k4xCgInPc2	dva
symfonických	symfonický	k2eAgMnPc2d1	symfonický
orchestrů	orchestr	k1gInPc2	orchestr
–	–	k?	–
Hallé	Hallý	k2eAgFnPc1d1	Hallý
Orchestra	orchestra	k1gFnSc1	orchestra
a	a	k8xC	a
BBC	BBC	kA	BBC
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
také	také	k9	také
komorní	komorní	k2eAgInSc1d1	komorní
orchestr	orchestr	k1gInSc1	orchestr
–	–	k?	–
Manchester	Manchester	k1gInSc1	Manchester
Camerata	Camerata	k1gFnSc1	Camerata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
sálem	sál	k1gInSc7	sál
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
Free	Fre	k1gFnSc2	Fre
Trade	Trad	k1gInSc5	Trad
Hall	Hall	k1gInSc1	Hall
na	na	k7c4	na
Peter	Peter	k1gMnSc1	Peter
Street	Street	k1gInSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
stal	stát	k5eAaPmAgMnS	stát
moderní	moderní	k2eAgInSc4d1	moderní
koncertní	koncertní	k2eAgInSc4d1	koncertní
sál	sál	k1gInSc4	sál
pro	pro	k7c4	pro
2	[number]	k4	2
000	[number]	k4	000
posluchačů	posluchač	k1gMnPc2	posluchač
Bridgewater	Bridgewater	k1gInSc4	Bridgewater
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
domovský	domovský	k2eAgInSc1d1	domovský
sál	sál	k1gInSc1	sál
Hallé	Hallý	k2eAgFnPc1d1	Hallý
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sál	sál	k1gInSc1	sál
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
nejlépe	dobře	k6eAd3	dobře
technicky	technicky	k6eAd1	technicky
vybavené	vybavený	k2eAgInPc1d1	vybavený
sály	sál	k1gInPc1	sál
pro	pro	k7c4	pro
koncerty	koncert	k1gInPc4	koncert
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
kvalitně	kvalitně	k6eAd1	kvalitně
navrženými	navržený	k2eAgInPc7d1	navržený
akustickými	akustický	k2eAgInPc7d1	akustický
parametry	parametr	k1gInPc7	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
koncertními	koncertní	k2eAgInPc7d1	koncertní
sály	sál	k1gInPc7	sál
jsou	být	k5eAaImIp3nP	být
RNCM	RNCM	kA	RNCM
<g/>
,	,	kIx,	,
Royal	Royal	k1gMnSc1	Royal
Exchange	Exchang	k1gFnSc2	Exchang
Theatre	Theatr	k1gInSc5	Theatr
a	a	k8xC	a
Manchesterská	manchesterský	k2eAgFnSc1d1	Manchesterská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
i	i	k9	i
významným	významný	k2eAgNnSc7d1	významné
centrem	centrum	k1gNnSc7	centrum
hudebního	hudební	k2eAgNnSc2d1	hudební
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
školy	škola	k1gFnPc1	škola
Royal	Royal	k1gMnSc1	Royal
Northern	Northern	k1gMnSc1	Northern
College	College	k1gFnPc2	College
of	of	k?	of
Music	Musice	k1gFnPc2	Musice
a	a	k8xC	a
Chetham	Chetham	k1gInSc1	Chetham
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
School	School	k1gInSc1	School
of	of	k?	of
Music	Musice	k1gInPc2	Musice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Populární	populární	k2eAgFnSc1d1	populární
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
svým	svůj	k3xOyFgNnSc7	svůj
multikulturálním	multikulturální	k2eAgNnSc7d1	multikulturální
prostředím	prostředí	k1gNnSc7	prostředí
vhodné	vhodný	k2eAgFnSc2d1	vhodná
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
populární	populární	k2eAgFnSc4d1	populární
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
bohatou	bohatý	k2eAgFnSc4d1	bohatá
historii	historie	k1gFnSc4	historie
jsou	být	k5eAaImIp3nP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
právem	právem	k6eAd1	právem
hrdi	hrd	k2eAgMnPc1d1	hrd
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bee	Bee	k?	Bee
Gees	Gees	k1gInSc1	Gees
</s>
</p>
<p>
<s>
The	The	k?	The
Hollies	Hollies	k1gInSc1	Hollies
</s>
</p>
<p>
<s>
Mindbenders	Mindbenders	k6eAd1	Mindbenders
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
cc	cc	k?	cc
</s>
</p>
<p>
<s>
Joy	Joy	k?	Joy
Division	Division	k1gInSc1	Division
</s>
</p>
<p>
<s>
New	New	k?	New
Order	Order	k1gInSc1	Order
</s>
</p>
<p>
<s>
Smiths	Smiths	k6eAd1	Smiths
</s>
</p>
<p>
<s>
Oasis	Oasis	k1gFnSc1	Oasis
</s>
</p>
<p>
<s>
The	The	k?	The
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
Simply	Simpnout	k5eAaPmAgFnP	Simpnout
Red	Red	k1gFnPc1	Red
</s>
</p>
<p>
<s>
Take	Take	k6eAd1	Take
That	That	k2eAgInSc1d1	That
</s>
</p>
<p>
<s>
Hurts	Hurts	k6eAd1	Hurts
</s>
</p>
<p>
<s>
The	The	k?	The
Stone	ston	k1gInSc5	ston
Roses	Roses	k1gMnSc1	Roses
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
významný	významný	k2eAgInSc1d1	významný
román	román	k1gInSc1	román
Mary	Mary	k1gFnSc1	Mary
Bartonová	Bartonový	k2eAgFnSc1d1	Bartonová
viktoriánské	viktoriánský	k2eAgFnPc4d1	viktoriánská
spisovatelky	spisovatelka	k1gFnPc4	spisovatelka
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
Gaskell	Gaskella	k1gFnPc2	Gaskella
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
literárních	literární	k2eAgNnPc2d1	literární
zpodobení	zpodobení	k1gNnPc2	zpodobení
tíživých	tíživý	k2eAgFnPc2d1	tíživá
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
proletariátu	proletariát	k1gInSc2	proletariát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
a	a	k8xC	a
rádio	rádio	k1gNnSc1	rádio
===	===	k?	===
</s>
</p>
<p>
<s>
Centrála	centrála	k1gFnSc1	centrála
Granada	Granada	k1gFnSc1	Granada
Television	Television	k1gInSc4	Television
<g/>
,	,	kIx,	,
pobočky	pobočka	k1gFnPc4	pobočka
ITV	ITV	kA	ITV
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
Quay	Qua	k2eAgInPc4d1	Qua
Street	Street	k1gInSc4	Street
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Castlefield	Castlefielda	k1gFnPc2	Castlefielda
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
BBC	BBC	kA	BBC
pro	pro	k7c4	pro
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
Anglii	Anglie	k1gFnSc4	Anglie
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
New	New	k1gFnSc6	New
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
House	house	k1gNnSc1	house
na	na	k7c4	na
Oxford	Oxford	k1gInSc4	Oxford
Road	Roada	k1gFnPc2	Roada
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
produkuje	produkovat	k5eAaImIp3nS	produkovat
zde	zde	k6eAd1	zde
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
lokální	lokální	k2eAgInPc4d1	lokální
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
také	také	k9	také
základnou	základna	k1gFnSc7	základna
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
programů	program	k1gInPc2	program
BBC	BBC	kA	BBC
One	One	k1gMnPc2	One
v	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
BBC	BBC	kA	BBC
existuje	existovat	k5eAaImIp3nS	existovat
záměr	záměr	k1gInSc4	záměr
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
část	část	k1gFnSc4	část
personálu	personál	k1gInSc2	personál
a	a	k8xC	a
technického	technický	k2eAgNnSc2d1	technické
vybavení	vybavení	k1gNnSc2	vybavení
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
Manchesteru	Manchester	k1gInSc2	Manchester
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vysílá	vysílat	k5eAaImIp3nS	vysílat
i	i	k9	i
lokální	lokální	k2eAgInSc4d1	lokální
televizní	televizní	k2eAgInSc4d1	televizní
kanál	kanál	k1gInSc4	kanál
Channel	Channela	k1gFnPc2	Channela
M	M	kA	M
<g/>
,	,	kIx,	,
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
společností	společnost	k1gFnSc7	společnost
Guardian	Guardiana	k1gFnPc2	Guardiana
Media	medium	k1gNnSc2	medium
Group	Group	k1gMnSc1	Group
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
lokální	lokální	k2eAgFnPc4d1	lokální
rádiové	rádiový	k2eAgFnPc4d1	rádiová
stanice	stanice	k1gFnPc4	stanice
patří	patřit	k5eAaImIp3nS	patřit
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
Key	Key	k1gFnSc1	Key
103	[number]	k4	103
<g/>
,	,	kIx,	,
Galaxy	Galax	k1gInPc4	Galax
<g/>
,	,	kIx,	,
Piccadilly	Piccadill	k1gInPc4	Piccadill
Magic	Magice	k1gInPc2	Magice
1152	[number]	k4	1152
<g/>
,	,	kIx,	,
105.4	[number]	k4	105.4
Century	Centura	k1gFnSc2	Centura
FM	FM	kA	FM
,	,	kIx,	,
100.4	[number]	k4	100.4
Smooth	Smooth	k1gInSc1	Smooth
FM	FM	kA	FM
<g/>
,	,	kIx,	,
Capital	Capital	k1gMnSc1	Capital
Gold	Gold	k1gMnSc1	Gold
1458	[number]	k4	1458
a	a	k8xC	a
Xfm	Xfm	k1gMnSc1	Xfm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jižního	jižní	k2eAgInSc2d1	jižní
Manchesteru	Manchester	k1gInSc2	Manchester
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgFnP	provozovat
i	i	k9	i
obecní	obecní	k2eAgFnPc1d1	obecní
rádiové	rádiový	k2eAgFnPc1d1	rádiová
stanice	stanice	k1gFnPc1	stanice
například	například	k6eAd1	například
ALL	ALL	kA	ALL
FM	FM	kA	FM
96.9	[number]	k4	96.9
a	a	k8xC	a
Wythenshawe	Wythenshaw	k1gInPc1	Wythenshaw
FM	FM	kA	FM
97.2	[number]	k4	97.2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Noviny	novina	k1gFnPc1	novina
a	a	k8xC	a
časopisy	časopis	k1gInPc1	časopis
===	===	k?	===
</s>
</p>
<p>
<s>
Noviny	novina	k1gFnPc1	novina
Guardian	Guardiany	k1gInPc2	Guardiany
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
a	a	k8xC	a
vydávány	vydáván	k2eAgInPc1d1	vydáván
jako	jako	k8xS	jako
Manchester	Manchester	k1gInSc1	Manchester
Guardian	Guardiana	k1gFnPc2	Guardiana
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
ústředí	ústředí	k1gNnSc1	ústředí
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
mnoho	mnoho	k4c1	mnoho
řídících	řídící	k2eAgFnPc2d1	řídící
funkcí	funkce	k1gFnPc2	funkce
bylo	být	k5eAaImAgNnS	být
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
prostor	prostora	k1gFnPc2	prostora
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
redakce	redakce	k1gFnSc1	redakce
sesterského	sesterský	k2eAgInSc2d1	sesterský
večerníku	večerník	k1gInSc2	večerník
Manchester	Manchester	k1gInSc1	Manchester
Evening	Evening	k1gInSc1	Evening
News	News	k1gInSc1	News
<g/>
.	.	kIx.	.
</s>
<s>
Nejčtenějším	čtený	k2eAgInSc7d3	nejčtenější
deníkem	deník	k1gInSc7	deník
rozšiřovaným	rozšiřovaný	k2eAgInSc7d1	rozšiřovaný
bezplatně	bezplatně	k6eAd1	bezplatně
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
deník	deník	k1gInSc4	deník
Manchester	Manchester	k1gInSc1	Manchester
Metro	metro	k1gNnSc1	metro
News	News	k1gInSc1	News
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
bezplatně	bezplatně	k6eAd1	bezplatně
dostupným	dostupný	k2eAgInSc7d1	dostupný
deníkem	deník	k1gInSc7	deník
je	být	k5eAaImIp3nS	být
Metro	metro	k1gNnSc1	metro
North	Northa	k1gFnPc2	Northa
West	Westa	k1gFnPc2	Westa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
sídlily	sídlit	k5eAaImAgFnP	sídlit
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
důležité	důležitý	k2eAgFnSc2d1	důležitá
redakce	redakce	k1gFnSc2	redakce
mnoha	mnoho	k4c2	mnoho
celostátních	celostátní	k2eAgInPc2d1	celostátní
deníků	deník	k1gInPc2	deník
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Daily	Daila	k1gFnPc1	Daila
Telegraph	Telegrapha	k1gFnPc2	Telegrapha
<g/>
,	,	kIx,	,
Daily	Daila	k1gFnPc1	Daila
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
Daily	Daila	k1gFnPc1	Daila
Mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
Daily	Dail	k1gInPc1	Dail
Mirror	Mirror	k1gInSc1	Mirror
a	a	k8xC	a
The	The	k1gFnSc1	The
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
kanceláře	kancelář	k1gFnPc4	kancelář
pouze	pouze	k6eAd1	pouze
Daily	Daila	k1gFnPc4	Daila
Sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
rozvoje	rozvoj	k1gInSc2	rozvoj
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
až	až	k6eAd1	až
1	[number]	k4	1
500	[number]	k4	500
novinářů	novinář	k1gMnPc2	novinář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nastalo	nastat	k5eAaPmAgNnS	nastat
postupné	postupný	k2eAgNnSc1d1	postupné
uzavírání	uzavírání	k1gNnSc1	uzavírání
redakcí	redakce	k1gFnPc2	redakce
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
stěhování	stěhování	k1gNnSc2	stěhování
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
označení	označení	k1gNnSc1	označení
druhá	druhý	k4xOgFnSc1	druhý
Fleet	Fleet	k1gMnSc1	Fleet
Street	Street	k1gMnSc1	Street
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náboženství	náboženství	k1gNnSc2	náboženství
==	==	k?	==
</s>
</p>
<p>
<s>
Anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
diecéze	diecéze	k1gFnSc1	diecéze
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
je	být	k5eAaImIp3nS	být
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
diecéze	diecéze	k1gFnSc2	diecéze
Diocese	Diocese	k1gFnSc2	Diocese
of	of	k?	of
Salford	Salford	k1gMnSc1	Salford
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
významné	významný	k2eAgFnPc4d1	významná
komunity	komunita	k1gFnPc4	komunita
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
nejpočetnější	početní	k2eAgFnSc2d3	nejpočetnější
židovské	židovská	k1gFnSc2	židovská
společenství	společenství	k1gNnSc2	společenství
mimo	mimo	k7c4	mimo
Londýn	Londýn	k1gInSc4	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
anglikánská	anglikánský	k2eAgFnSc1d1	anglikánská
gotická	gotický	k2eAgFnSc1d1	gotická
Manchesterská	manchesterský	k2eAgFnSc1d1	Manchesterská
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
stavba	stavba	k1gFnSc1	stavba
trvala	trvat	k5eAaImAgFnS	trvat
asi	asi	k9	asi
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgFnPc4	tři
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Manchester	Manchester	k1gInSc1	Manchester
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
Metropolitan	metropolitan	k1gInSc1	metropolitan
University	universita	k1gFnSc2	universita
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2004	[number]	k4	2004
sloučením	sloučení	k1gNnSc7	sloučení
Victoria	Victorium	k1gNnSc2	Victorium
University	universita	k1gFnSc2	universita
of	of	k?	of
Manchester	Manchester	k1gInSc1	Manchester
a	a	k8xC	a
UMIST	UMIST	kA	UMIST
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
poblíž	poblíž	k7c2	poblíž
Salfordu	Salford	k1gInSc2	Salford
sídlí	sídlet	k5eAaImIp3nS	sídlet
University	universita	k1gFnPc4	universita
of	of	k?	of
Salford	Salford	k1gMnSc1	Salford
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
University	universita	k1gFnSc2	universita
of	of	k?	of
Bolton	Bolton	k1gInSc1	Bolton
,	,	kIx,	,
Royal	Royal	k1gInSc1	Royal
Northern	Northerna	k1gFnPc2	Northerna
College	College	k1gNnSc2	College
of	of	k?	of
Music	Musice	k1gInPc2	Musice
a	a	k8xC	a
University	universita	k1gFnSc2	universita
Centre	centr	k1gInSc5	centr
Oldham	Oldham	k1gInSc1	Oldham
má	můj	k3xOp1gFnSc1	můj
oblast	oblast	k1gFnSc1	oblast
Velkého	velký	k2eAgInSc2d1	velký
Manchesteru	Manchester	k1gInSc2	Manchester
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Manchester	Manchester	k1gInSc1	Manchester
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
Metropolitan	metropolitan	k1gInSc4	metropolitan
University	universita	k1gFnSc2	universita
a	a	k8xC	a
Royal	Royal	k1gInSc1	Royal
Northern	Northerna	k1gFnPc2	Northerna
College	College	k1gNnSc2	College
of	of	k?	of
Music	Musice	k1gInPc2	Musice
jsou	být	k5eAaImIp3nP	být
seskupeny	seskupit	k5eAaPmNgInP	seskupit
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
velký	velký	k2eAgInSc4d1	velký
univerzitní	univerzitní	k2eAgInSc4d1	univerzitní
komplex	komplex	k1gInSc4	komplex
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Oxford	Oxford	k1gInSc4	Oxford
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
Grammar	Grammar	k1gInSc1	Grammar
School	Schoola	k1gFnPc2	Schoola
je	být	k5eAaImIp3nS	být
soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnPc4	chlapec
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Fallowfield	Fallowfielda	k1gFnPc2	Fallowfielda
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
přímo	přímo	k6eAd1	přímo
financována	financovat	k5eAaBmNgFnS	financovat
státem	stát	k1gInSc7	stát
a	a	k8xC	a
neplatilo	platit	k5eNaImAgNnS	platit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
školné	školné	k1gNnSc1	školné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
labouristická	labouristický	k2eAgFnSc1d1	labouristická
vláda	vláda	k1gFnSc1	vláda
ukončila	ukončit	k5eAaPmAgFnS	ukončit
přímé	přímý	k2eAgNnSc4d1	přímé
financování	financování	k1gNnSc4	financování
gymnázií	gymnázium	k1gNnPc2	gymnázium
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
soukromou	soukromý	k2eAgFnSc7d1	soukromá
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
sídlila	sídlit	k5eAaImAgFnS	sídlit
na	na	k7c6	na
prominentním	prominentní	k2eAgNnSc6d1	prominentní
místě	místo	k1gNnSc6	místo
poblíž	poblíž	k7c2	poblíž
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
přestěhována	přestěhovat	k5eAaPmNgFnS	přestěhovat
na	na	k7c4	na
Old	Olda	k1gFnPc2	Olda
Hall	Hall	k1gInSc1	Hall
Lane	Lan	k1gInSc2	Lan
ve	v	k7c6	v
Fallowfieldu	Fallowfield	k1gInSc6	Fallowfield
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
uspokojit	uspokojit	k5eAaPmF	uspokojit
zvyšující	zvyšující	k2eAgNnSc4d1	zvyšující
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
lokalitě	lokalita	k1gFnSc6	lokalita
nyní	nyní	k6eAd1	nyní
sídlí	sídlet	k5eAaImIp3nS	sídlet
Chetham	Chetham	k1gInSc1	Chetham
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
School	School	k1gInSc1	School
of	of	k?	of
Music	Musice	k1gInPc2	Musice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
sídlí	sídlet	k5eAaImIp3nP	sídlet
dva	dva	k4xCgInPc1	dva
významné	významný	k2eAgInPc1d1	významný
fotbalové	fotbalový	k2eAgInPc1d1	fotbalový
kluby	klub	k1gInPc1	klub
<g/>
:	:	kIx,	:
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Domovským	domovský	k2eAgInSc7d1	domovský
stadionem	stadion	k1gInSc7	stadion
Manchester	Manchester	k1gInSc1	Manchester
City	City	k1gFnSc2	City
je	být	k5eAaImIp3nS	být
Etihad	Etihad	k1gInSc1	Etihad
Stadium	stadium	k1gNnSc1	stadium
<g/>
;	;	kIx,	;
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
Old	Olda	k1gFnPc2	Olda
Trafford	Trafford	k1gInSc1	Trafford
<g/>
,	,	kIx,	,
druhém	druhý	k4xOgInSc6	druhý
největším	veliký	k2eAgInSc6d3	veliký
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Trafford	Trafforda	k1gFnPc2	Trafforda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
velkokluby	velkoklub	k1gInPc1	velkoklub
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
manchesterských	manchesterský	k2eAgNnPc2d1	manchesterské
fotbalových	fotbalový	k2eAgNnPc2d1	fotbalové
mužstev	mužstvo	k1gNnPc2	mužstvo
<g/>
,	,	kIx,	,
dalšími	další	k1gNnPc7	další
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Oldham	Oldham	k1gInSc4	Oldham
Athletic	Athletice	k1gFnPc2	Athletice
<g/>
,	,	kIx,	,
Stockport	Stockport	k1gInSc1	Stockport
County	Counta	k1gFnSc2	Counta
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Bury	Bury	k?	Bury
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Wigan	Wigan	k1gInSc4	Wigan
Athletic	Athletice	k1gFnPc2	Athletice
či	či	k8xC	či
Rochdale	Rochdala	k1gFnSc3	Rochdala
AFC	AFC	kA	AFC
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumu	výzkum	k1gInSc2	výzkum
Urbis	Urbis	k1gFnSc2	Urbis
centre	centr	k1gInSc5	centr
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc1	město
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
koncentraci	koncentrace	k1gFnSc4	koncentrace
fotbalových	fotbalový	k2eAgMnPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
moderních	moderní	k2eAgNnPc2d1	moderní
sportovních	sportovní	k2eAgNnPc2d1	sportovní
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Manchester	Manchester	k1gInSc1	Manchester
Velodrome	velodrom	k1gInSc5	velodrom
<g/>
,	,	kIx,	,
City	city	k1gNnSc6	city
of	of	k?	of
Manchester	Manchester	k1gInSc1	Manchester
Stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
National	National	k1gFnSc1	National
Squash	squash	k1gInSc1	squash
Centre	centr	k1gInSc5	centr
a	a	k8xC	a
Manchester	Manchester	k1gInSc1	Manchester
Aquatics	Aquaticsa	k1gFnPc2	Aquaticsa
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
pro	pro	k7c4	pro
Hry	hra	k1gFnPc4	hra
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kriketový	kriketový	k2eAgInSc1d1	kriketový
stadion	stadion	k1gInSc1	stadion
Old	Olda	k1gFnPc2	Olda
Trafford	Trafford	k1gInSc1	Trafford
(	(	kIx(	(
<g/>
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
známý	známý	k2eAgInSc4d1	známý
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
domovským	domovský	k2eAgNnSc7d1	domovské
hřištěm	hřiště	k1gNnSc7	hřiště
kriketového	kriketový	k2eAgInSc2d1	kriketový
klubu	klub	k1gInSc2	klub
Lancashire	Lancashir	k1gInSc5	Lancashir
County	Count	k1gInPc4	Count
Cricket	Cricket	k1gMnSc1	Cricket
Club	club	k1gInSc4	club
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
Velkého	velký	k2eAgInSc2d1	velký
</s>
</p>
<p>
<s>
Manchesteru	Manchester	k1gInSc2	Manchester
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
ragbyové	ragbyový	k2eAgInPc1d1	ragbyový
kluby	klub	k1gInPc1	klub
Sale	Sale	k1gFnSc1	Sale
Sharks	Sharks	k1gInSc1	Sharks
<g/>
,	,	kIx,	,
Wigan	Wigan	k1gInSc1	Wigan
Warriors	Warriors	k1gInSc1	Warriors
<g/>
,	,	kIx,	,
Salford	Salford	k1gInSc1	Salford
City	city	k1gNnSc1	city
Reds	Reds	k1gInSc1	Reds
<g/>
,	,	kIx,	,
Oldham	Oldham	k1gInSc1	Oldham
Roughyeds	Roughyeds	k1gInSc4	Roughyeds
<g/>
,	,	kIx,	,
Rochdale	Rochdala	k1gFnSc3	Rochdala
Hornets	Hornetsa	k1gFnPc2	Hornetsa
a	a	k8xC	a
Swinton	Swinton	k1gInSc1	Swinton
Lions	Lionsa	k1gFnPc2	Lionsa
<g/>
.	.	kIx.	.
</s>
<s>
Belle	bell	k1gInSc5	bell
Vue	Vue	k1gFnSc3	Vue
Stadium	stadium	k1gNnSc1	stadium
v	v	k7c6	v
Gortonu	Gorton	k1gInSc6	Gorton
využívá	využívat	k5eAaImIp3nS	využívat
plochodrážní	plochodrážní	k2eAgInSc1d1	plochodrážní
klub	klub	k1gInSc1	klub
Belle	bell	k1gInSc5	bell
Vue	Vue	k1gMnPc4	Vue
Aces	Aces	k1gInSc4	Aces
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pořádají	pořádat	k5eAaImIp3nP	pořádat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
chrtí	chrtí	k2eAgInPc1d1	chrtí
dostihy	dostih	k1gInPc1	dostih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hala	hala	k1gFnSc1	hala
Manchester	Manchester	k1gInSc1	Manchester
Arena	Arena	k1gFnSc1	Arena
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
pojme	pojmout	k5eAaPmIp3nS	pojmout
21	[number]	k4	21
tisíc	tisíc	k4xCgInPc2	tisíc
sedících	sedící	k2eAgMnPc2d1	sedící
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nP	sloužit
událostem	událost	k1gFnPc3	událost
jak	jak	k6eAd1	jak
sportovním	sportovní	k2eAgFnPc3d1	sportovní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
hudebním	hudební	k2eAgMnSc7d1	hudební
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
výbornou	výborný	k2eAgFnSc4d1	výborná
vybavenost	vybavenost	k1gFnSc4	vybavenost
moderními	moderní	k2eAgInPc7d1	moderní
sportovními	sportovní	k2eAgInPc7d1	sportovní
stánky	stánek	k1gInPc7	stánek
dvakrát	dvakrát	k6eAd1	dvakrát
neúspěšně	úspěšně	k6eNd1	úspěšně
ucházel	ucházet	k5eAaImAgMnS	ucházet
o	o	k7c4	o
pořádání	pořádání	k1gNnSc4	pořádání
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
však	však	k9	však
některé	některý	k3yIgFnPc1	některý
soutěže	soutěž	k1gFnPc1	soutěž
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistické	turistický	k2eAgFnPc1d1	turistická
atrakce	atrakce	k1gFnPc1	atrakce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Architektura	architektura	k1gFnSc1	architektura
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
budov	budova	k1gFnPc2	budova
postavených	postavený	k2eAgMnPc2d1	postavený
od	od	k7c2	od
viktoriánského	viktoriánský	k2eAgNnSc2d1	viktoriánské
období	období	k1gNnSc2	období
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
honosných	honosný	k2eAgFnPc2d1	honosná
budov	budova	k1gFnPc2	budova
odráží	odrážet	k5eAaImIp3nS	odrážet
původní	původní	k2eAgNnSc4d1	původní
postavení	postavení	k1gNnSc4	postavení
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
centra	centrum	k1gNnSc2	centrum
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
bavlnou	bavlna	k1gFnSc7	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
bývalých	bývalý	k2eAgNnPc2d1	bývalé
skladišť	skladiště	k1gNnPc2	skladiště
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
využíváno	využívat	k5eAaPmNgNnS	využívat
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
účely	účel	k1gInPc4	účel
ale	ale	k8xC	ale
původní	původní	k2eAgInSc1d1	původní
charakter	charakter	k1gInSc1	charakter
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Manchester	Manchester	k1gInSc1	Manchester
se	se	k3xPyFc4	se
taky	taky	k6eAd1	taky
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
mrakodrapem	mrakodrap	k1gInSc7	mrakodrap
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Beetham	Beetham	k1gInSc1	Beetham
Tower	Tower	k1gInSc1	Tower
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
by	by	kYmCp3nS	by
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
téměř	téměř	k6eAd1	téměř
169	[number]	k4	169
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bridgewater	Bridgewater	k1gMnSc1	Bridgewater
Hall	Hall	k1gMnSc1	Hall
sídlo	sídlo	k1gNnSc4	sídlo
Hallé	Hallý	k2eAgFnSc2d1	Hallý
Orchestra	orchestra	k1gFnSc1	orchestra
</s>
</p>
<p>
<s>
Corn	Corn	k1gInSc1	Corn
Exchange	Exchang	k1gInSc2	Exchang
nyní	nyní	k6eAd1	nyní
nákupní	nákupní	k2eAgNnSc4d1	nákupní
centrum	centrum	k1gNnSc4	centrum
Triangle	triangl	k1gInSc5	triangl
</s>
</p>
<p>
<s>
G-Mex	G-Mex	k1gInSc1	G-Mex
centrum	centrum	k1gNnSc1	centrum
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Rylands	Rylands	k1gInSc4	Rylands
Library	Librara	k1gFnSc2	Librara
v	v	k7c6	v
Deansgate	Deansgat	k1gMnSc5	Deansgat
</s>
</p>
<p>
<s>
London	London	k1gMnSc1	London
Road	Road	k1gMnSc1	Road
Fire	Fire	k1gFnPc2	Fire
Station	station	k1gInSc4	station
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
Central	Central	k1gFnSc2	Central
Library	Librara	k1gFnSc2	Librara
na	na	k7c4	na
St	St	kA	St
Peter	Peter	k1gMnSc1	Peter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Square	square	k1gInSc1	square
od	od	k7c2	od
E.	E.	kA	E.
Vincenta	Vincent	k1gMnSc2	Vincent
Harrise	Harrise	k1gFnSc2	Harrise
</s>
</p>
<p>
<s>
Manchesterská	manchesterský	k2eAgFnSc1d1	Manchesterská
radnice	radnice	k1gFnSc1	radnice
od	od	k7c2	od
Alfréda	Alfréd	k1gMnSc2	Alfréd
Waterhouse	Waterhouse	k1gFnSc2	Waterhouse
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
Midland	Midlanda	k1gFnPc2	Midlanda
Bank	banka	k1gFnPc2	banka
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
HSBC	HSBC	kA	HSBC
Bank	banka	k1gFnPc2	banka
<g/>
)	)	kIx)	)
na	na	k7c4	na
King	King	k1gInSc4	King
Street	Streeta	k1gFnPc2	Streeta
od	od	k7c2	od
Edwina	Edwino	k1gNnSc2	Edwino
Lutyense	Lutyense	k1gFnSc2	Lutyense
</s>
</p>
<p>
<s>
Midland	Midland	k1gInSc1	Midland
Hotel	hotel	k1gInSc1	hotel
</s>
</p>
<p>
<s>
Piccadilly	Piccadilla	k1gFnPc1	Piccadilla
Gardens	Gardensa	k1gFnPc2	Gardensa
od	od	k7c2	od
Tadaa	Tada	k1gInSc2	Tada
Anda	Anda	k1gFnSc1	Anda
</s>
</p>
<p>
<s>
Palace	Palace	k1gFnSc1	Palace
Hotel	hotel	k1gInSc1	hotel
</s>
</p>
<p>
<s>
Portico	Portico	k1gNnSc1	Portico
Library	Librara	k1gFnSc2	Librara
</s>
</p>
<p>
<s>
Royal	Royal	k1gInSc1	Royal
Exchange	Exchang	k1gFnSc2	Exchang
</s>
</p>
<p>
<s>
South	South	k1gInSc1	South
Manchester	Manchester	k1gInSc1	Manchester
Synagogue	Synagogue	k1gFnSc1	Synagogue
</s>
</p>
<p>
<s>
Strangeways	Strangeways	k6eAd1	Strangeways
Prison	Prison	k1gInSc1	Prison
od	od	k7c2	od
Waterhouse	Waterhouse	k1gFnSc2	Waterhouse
</s>
</p>
<p>
<s>
Sunlight	Sunlight	k1gInSc1	Sunlight
House	house	k1gNnSc1	house
</s>
</p>
<p>
<s>
Trinity	Trinit	k2eAgInPc1d1	Trinit
Bridge	Bridg	k1gInPc1	Bridg
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
River	River	k1gMnSc1	River
Irwell	Irwell	k1gMnSc1	Irwell
od	od	k7c2	od
Santiaga	Santiago	k1gNnSc2	Santiago
Calatravy	Calatrava	k1gFnSc2	Calatrava
</s>
</p>
<p>
<s>
Manchester	Manchester	k1gInSc1	Manchester
Victoria	Victorium	k1gNnSc2	Victorium
Station	station	k1gInSc1	station
</s>
</p>
<p>
<s>
Victoria	Victorium	k1gNnPc4	Victorium
Baths	Baths	k1gInSc1	Baths
</s>
</p>
<p>
<s>
Urbis	Urbis	k1gInSc1	Urbis
Museum	museum	k1gNnSc1	museum
od	od	k7c2	od
Iana	Ianus	k1gMnSc2	Ianus
Simpsona	Simpson	k1gMnSc2	Simpson
</s>
</p>
<p>
<s>
===	===	k?	===
Pomníky	pomník	k1gInPc4	pomník
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
pomníky	pomník	k1gInPc1	pomník
mnoha	mnoho	k4c2	mnoho
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
historii	historie	k1gFnSc4	historie
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
těchto	tento	k3xDgInPc2	tento
památníků	památník	k1gInPc2	památník
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
Albert	Albert	k1gMnSc1	Albert
Square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
městské	městský	k2eAgFnSc2d1	městská
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
v	v	k7c4	v
Piccadilly	Piccadilla	k1gFnPc4	Piccadilla
Gardens	Gardensa	k1gFnPc2	Gardensa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejzajímavější	zajímavý	k2eAgNnSc4d3	nejzajímavější
je	on	k3xPp3gNnSc4	on
možno	možno	k6eAd1	možno
zařadit	zařadit	k5eAaPmF	zařadit
Alan	alan	k1gInSc4	alan
Turing	Turing	k1gInSc1	Turing
Memorial	Memorial	k1gInSc1	Memorial
v	v	k7c6	v
Sackville	Sackvilla	k1gFnSc6	Sackvilla
Park	park	k1gInSc1	park
poblíž	poblíž	k6eAd1	poblíž
Canal	Canal	k1gInSc1	Canal
Street	Street	k1gInSc1	Street
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
památce	památka	k1gFnSc6	památka
otce	otec	k1gMnSc2	otec
moderní	moderní	k2eAgFnSc2d1	moderní
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
památník	památník	k1gInSc4	památník
Abrahama	Abraham	k1gMnSc2	Abraham
Lincolna	Lincoln	k1gMnSc2	Lincoln
připomínající	připomínající	k2eAgFnSc4d1	připomínající
krizi	krize	k1gFnSc4	krize
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
průmyslu	průmysl	k1gInSc6	průmysl
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Anglie	Anglie	k1gFnSc2	Anglie
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1861	[number]	k4	1861
až	až	k8xS	až
1865	[number]	k4	1865
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
Americkou	americký	k2eAgFnSc7d1	americká
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
B	B	kA	B
of	of	k?	of
the	the	k?	the
Bang	Bang	k1gInSc1	Bang
<g/>
,	,	kIx,	,
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
skulptura	skulptura	k1gFnSc1	skulptura
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c4	na
připomínku	připomínka	k1gFnSc4	připomínka
pořádání	pořádání	k1gNnSc4	pořádání
her	hra	k1gFnPc2	hra
Commonwealtu	Commonwealt	k1gInSc2	Commonwealt
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ulice	ulice	k1gFnSc1	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc1	náměstí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
rušných	rušný	k2eAgFnPc2d1	rušná
ulicí	ulice	k1gFnSc7	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc4	část
ulicí	ulice	k1gFnSc7	ulice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
pro	pro	k7c4	pro
pěší	pěší	k2eAgInPc4d1	pěší
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgInPc4d1	ostatní
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
pro	pro	k7c4	pro
Metrolink	Metrolink	k1gInSc4	Metrolink
<g/>
,	,	kIx,	,
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
a	a	k8xC	a
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
dopravu	doprava	k1gFnSc4	doprava
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
přeplněné	přeplněný	k2eAgFnPc1d1	přeplněná
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dopravních	dopravní	k2eAgFnPc2d1	dopravní
tepen	tepna	k1gFnPc2	tepna
je	být	k5eAaImIp3nS	být
Market	market	k1gInSc1	market
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
středověká	středověký	k2eAgFnSc1d1	středověká
výzdoba	výzdoba	k1gFnSc1	výzdoba
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Market	market	k1gInSc4	market
Street	Streeta	k1gFnPc2	Streeta
i	i	k8xC	i
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
ulice	ulice	k1gFnPc1	ulice
jako	jako	k8xS	jako
Smithy	Smith	k1gInPc1	Smith
Door	Doora	k1gFnPc2	Doora
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
původní	původní	k2eAgInSc4d1	původní
charakter	charakter	k1gInSc4	charakter
je	být	k5eAaImIp3nS	být
Long	Long	k1gMnSc1	Long
Millgate	Millgat	k1gInSc5	Millgat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
sever	sever	k1gInSc4	sever
z	z	k7c2	z
Market	market	k1gInSc1	market
Place	plac	k1gInSc6	plac
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
ulici	ulice	k1gFnSc4	ulice
je	být	k5eAaImIp3nS	být
Whitworth	Whitworth	k1gMnSc1	Whitworth
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
ulice	ulice	k1gFnSc1	ulice
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obklopená	obklopený	k2eAgFnSc1d1	obklopená
zahradami	zahrada	k1gFnPc7	zahrada
a	a	k8xC	a
obklopena	obklopit	k5eAaPmNgFnS	obklopit
působivými	působivý	k2eAgFnPc7d1	působivá
cihlovými	cihlový	k2eAgFnPc7d1	cihlová
stavbami	stavba	k1gFnPc7	stavba
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
sloužícími	sloužící	k2eAgInPc7d1	sloužící
jako	jako	k8xS	jako
skladiště	skladiště	k1gNnPc4	skladiště
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
využívány	využívat	k5eAaPmNgInP	využívat
jako	jako	k8xS	jako
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
studentského	studentský	k2eAgInSc2d1	studentský
života	život	k1gInSc2	život
ulice	ulice	k1gFnSc2	ulice
Wilmslow	Wilmslow	k1gMnSc1	Wilmslow
Road	Road	k1gMnSc1	Road
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
místa	místo	k1gNnPc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
Great	Great	k2eAgInSc1d1	Great
Northern	Northern	k1gInSc1	Northern
Square	square	k1gInSc1	square
</s>
</p>
<p>
<s>
Spring	Spring	k1gInSc1	Spring
Gardens	Gardensa	k1gFnPc2	Gardensa
</s>
</p>
<p>
<s>
Cathedral	Cathedrat	k5eAaImAgInS	Cathedrat
Gardens	Gardens	k1gInSc1	Gardens
</s>
</p>
<p>
<s>
New	New	k?	New
Cathedral	Cathedral	k1gMnSc1	Cathedral
Street	Street	k1gMnSc1	Street
</s>
</p>
<p>
<s>
Gay	gay	k1gMnSc1	gay
Village	Villag	k1gFnSc2	Villag
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
–	–	k?	–
největší	veliký	k2eAgFnSc1d3	veliký
čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Manchester	Manchester	k1gInSc1	Manchester
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Manchester	Manchester	k1gInSc4	Manchester
ve	v	k7c4	v
WikislovníkuManchester	WikislovníkuManchester	k1gInSc4	WikislovníkuManchester
City	City	k1gFnSc2	City
Council	Councila	k1gFnPc2	Councila
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
Manchesteru	Manchester	k1gInSc2	Manchester
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
pro	pro	k7c4	pro
barech	bar	k1gInPc6	bar
<g/>
,	,	kIx,	,
hospodách	hospodách	k?	hospodách
<g/>
,	,	kIx,	,
klubech	klub	k1gInPc6	klub
a	a	k8xC	a
restauracích	restaurace	k1gFnPc6	restaurace
Manchesteru	Manchester	k1gInSc2	Manchester
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
muzea	muzeum	k1gNnSc2	muzeum
URBIS	URBIS	kA	URBIS
</s>
</p>
