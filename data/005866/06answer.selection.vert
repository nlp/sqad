<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Antonínem	Antonín	k1gMnSc7	Antonín
ml	ml	kA	ml
<g/>
.	.	kIx.	.
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
obuvnickou	obuvnický	k2eAgFnSc4d1	obuvnická
firmu	firma	k1gFnSc4	firma
Baťa	Baťa	k1gMnSc1	Baťa
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
komplex	komplex	k1gInSc4	komplex
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
podnikatelů	podnikatel	k1gMnPc2	podnikatel
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
