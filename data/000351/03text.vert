<s>
Kosmas	Kosmas	k1gMnSc1	Kosmas
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1045	[number]	k4	1045
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1125	[number]	k4	1125
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
známý	známý	k2eAgMnSc1d1	známý
český	český	k2eAgMnSc1d1	český
kronikář	kronikář	k1gMnSc1	kronikář
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Kroniky	kronika	k1gFnSc2	kronika
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
Chronica	Chronic	k1gInSc2	Chronic
Boemorum	Boemorum	k1gInSc1	Boemorum
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1120	[number]	k4	1120
až	až	k9	až
1125	[number]	k4	1125
děkanem	děkan	k1gMnSc7	děkan
pražské	pražský	k2eAgFnSc2d1	Pražská
kapituly	kapitula	k1gFnSc2	kapitula
při	při	k7c6	při
sv.	sv.	kA	sv.
Vítu	Vít	k1gMnSc6	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
všechny	všechen	k3xTgFnPc4	všechen
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
známé	známý	k2eAgInPc1d1	známý
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
a	a	k8xC	a
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
dožil	dožít	k5eAaPmAgMnS	dožít
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
asi	asi	k9	asi
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
původu	původ	k1gInSc6	původ
toho	ten	k3xDgNnSc2	ten
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
veškeré	veškerý	k3xTgFnPc4	veškerý
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
kroniky	kronika	k1gFnSc2	kronika
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
mimochodem	mimochodem	k9	mimochodem
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
řady	řada	k1gFnSc2	řada
autorů	autor	k1gMnPc2	autor
středověkých	středověký	k2eAgFnPc2d1	středověká
kronik	kronika	k1gFnPc2	kronika
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
pravé	pravý	k2eAgNnSc4d1	pravé
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
jsou	být	k5eAaImIp3nP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
předmluvy	předmluva	k1gFnPc1	předmluva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
k	k	k7c3	k
připsání	připsání	k1gNnSc3	připsání
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
jím	on	k3xPp3gMnSc7	on
psané	psaný	k2eAgFnSc6d1	psaná
kronice	kronika	k1gFnSc6	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
duchovního	duchovní	k1gMnSc2	duchovní
a	a	k8xC	a
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
otcovu	otcův	k2eAgFnSc4d1	otcova
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
dokonce	dokonce	k9	dokonce
i	i	k9	i
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
velmožském	velmožský	k2eAgInSc6d1	velmožský
původu	původ	k1gInSc6	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
přijímána	přijímán	k2eAgFnSc1d1	přijímána
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
neexistuje	existovat	k5eNaImIp3nS	existovat
dost	dost	k6eAd1	dost
dokladů	doklad	k1gInPc2	doklad
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
byla	být	k5eAaImAgFnS	být
vyslovována	vyslovovat	k5eAaImNgFnS	vyslovovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
domnělého	domnělý	k2eAgNnSc2d1	domnělé
příbuzenství	příbuzenství	k1gNnSc2	příbuzenství
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Zdíkem	Zdík	k1gMnSc7	Zdík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
musel	muset	k5eAaImAgMnS	muset
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
dostatečně	dostatečně	k6eAd1	dostatečně
zámožné	zámožný	k2eAgNnSc1d1	zámožné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
studia	studio	k1gNnPc4	studio
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
pražské	pražský	k2eAgFnSc2d1	Pražská
katedrální	katedrální	k2eAgFnSc2d1	katedrální
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
při	při	k7c6	při
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Lamberta	Lambert	k1gMnSc2	Lambert
v	v	k7c6	v
Lutychu	Lutych	k1gInSc6	Lutych
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yRgFnSc7	který
české	český	k2eAgNnSc1d1	české
prostředí	prostředí	k1gNnSc1	prostředí
pěstovalo	pěstovat	k5eAaImAgNnS	pěstovat
styky	styk	k1gInPc4	styk
už	už	k9	už
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
a	a	k8xC	a
která	který	k3yRgFnSc1	který
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgFnPc3d3	nejlepší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ho	on	k3xPp3gMnSc4	on
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
mistr	mistr	k1gMnSc1	mistr
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Lutychu	Lutych	k1gInSc6	Lutych
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1047	[number]	k4	1047
<g/>
-	-	kIx~	-
<g/>
1083	[number]	k4	1083
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
poté	poté	k6eAd1	poté
Kosmas	Kosmas	k1gMnSc1	Kosmas
pobýval	pobývat	k5eAaImAgMnS	pobývat
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
evropských	evropský	k2eAgFnPc6d1	Evropská
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
ukončil	ukončit	k5eAaPmAgMnS	ukončit
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
tedy	tedy	k8xC	tedy
až	až	k9	až
jako	jako	k9	jako
muž	muž	k1gMnSc1	muž
středního	střední	k2eAgInSc2d1	střední
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zvlášť	zvlášť	k6eAd1	zvlášť
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
pražské	pražský	k2eAgFnSc2d1	Pražská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1091	[number]	k4	1091
<g/>
/	/	kIx~	/
<g/>
1092	[number]	k4	1092
odjel	odjet	k5eAaPmAgInS	odjet
s	s	k7c7	s
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
Kosmou	Kosma	k1gMnSc7	Kosma
a	a	k8xC	a
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Ondřejem	Ondřej	k1gMnSc7	Ondřej
za	za	k7c7	za
císařem	císař	k1gMnSc7	císař
Jindřichem	Jindřich	k1gMnSc7	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
do	do	k7c2	do
Mantovy	Mantova	k1gFnSc2	Mantova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1094	[number]	k4	1094
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
jejich	jejich	k3xOp3gNnSc4	jejich
vysvěcení	vysvěcení	k1gNnSc4	vysvěcení
mohučským	mohučský	k2eAgMnSc7d1	mohučský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
kněžské	kněžský	k2eAgFnSc2d1	kněžská
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
už	už	k6eAd1	už
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvořila	tvořit	k5eAaImAgFnS	tvořit
svébytnou	svébytný	k2eAgFnSc4d1	svébytná
skupinu	skupina	k1gFnSc4	skupina
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
kulturou	kultura	k1gFnSc7	kultura
a	a	k8xC	a
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
tvořila	tvořit	k5eAaImAgFnS	tvořit
protipól	protipól	k1gInSc4	protipól
konkurenční	konkurenční	k2eAgFnSc2d1	konkurenční
skupině	skupina	k1gFnSc6	skupina
kněží	kněz	k1gMnPc2	kněz
cizího	cizí	k2eAgInSc2d1	cizí
<g/>
,	,	kIx,	,
především	především	k9	především
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
propojena	propojit	k5eAaPmNgFnS	propojit
nejen	nejen	k6eAd1	nejen
přátelskými	přátelský	k2eAgFnPc7d1	přátelská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
příbuzenskými	příbuzenský	k2eAgFnPc7d1	příbuzenská
vazbami	vazba	k1gFnPc7	vazba
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1099	[number]	k4	1099
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Ostřihomi	Ostřiho	k1gFnPc7	Ostřiho
společně	společně	k6eAd1	společně
s	s	k7c7	s
nedávno	nedávno	k6eAd1	nedávno
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
pražským	pražský	k2eAgMnSc7d1	pražský
biskupem	biskup	k1gMnSc7	biskup
Heřmanem	Heřman	k1gMnSc7	Heřman
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jen	jen	k6eAd1	jen
jáhnem	jáhen	k1gMnSc7	jáhen
<g/>
,	,	kIx,	,
kněžské	kněžský	k2eAgNnSc1d1	kněžské
svěcení	svěcení	k1gNnSc1	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
ne	ne	k9	ne
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1110	[number]	k4	1110
mohl	moct	k5eAaImAgMnS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
děkan	děkan	k1gMnSc1	děkan
pražské	pražský	k2eAgFnSc2d1	Pražská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
vyjednával	vyjednávat	k5eAaImAgMnS	vyjednávat
o	o	k7c4	o
navrácení	navrácení	k1gNnSc4	navrácení
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Sekyřkostele	Sekyřkostel	k1gInSc2	Sekyřkostel
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k6eAd1	jistě
byl	být	k5eAaImAgInS	být
děkanem	děkan	k1gMnSc7	děkan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1120	[number]	k4	1120
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kroniky	kronika	k1gFnSc2	kronika
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
i	i	k9	i
jméno	jméno	k1gNnSc1	jméno
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Božetěše	Božetěcha	k1gFnSc6	Božetěcha
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
úmrtím	úmrtí	k1gNnSc7	úmrtí
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1117	[number]	k4	1117
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
Jindřich	Jindřich	k1gMnSc1	Jindřich
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kronice	kronika	k1gFnSc6	kronika
uveden	uvést	k5eAaPmNgMnS	uvést
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1123	[number]	k4	1123
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
jasná	jasný	k2eAgFnSc1d1	jasná
pasáž	pasáž	k1gFnSc1	pasáž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vykládána	vykládán	k2eAgFnSc1d1	vykládána
dvojím	dvojí	k4xRgInSc7	dvojí
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
dříve	dříve	k6eAd2	dříve
spekulovali	spekulovat	k5eAaImAgMnP	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kosmovým	Kosmův	k2eAgMnSc7d1	Kosmův
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
přímo	přímo	k6eAd1	přímo
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgFnS	zdát
potvrzovat	potvrzovat	k5eAaImF	potvrzovat
i	i	k9	i
přítomnost	přítomnost	k1gFnSc1	přítomnost
Kosmova	Kosmův	k2eAgNnSc2d1	Kosmovo
a	a	k8xC	a
Božetěšina	Božetěšin	k2eAgNnSc2d1	Božetěšin
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
Horologiu	horologium	k1gNnSc6	horologium
olomouckém	olomoucký	k2eAgNnSc6d1	olomoucké
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc2	který
objednal	objednat	k5eAaPmAgMnS	objednat
právě	právě	k9	právě
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
bere	brát	k5eAaImIp3nS	brát
za	za	k7c2	za
pravděpodobnější	pravděpodobný	k2eAgFnSc1d2	pravděpodobnější
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Jindřichy	Jindřich	k1gMnPc4	Jindřich
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
synovi	syn	k1gMnSc3	syn
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
Čechách	Čechy	k1gFnPc6	Čechy
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
otcovu	otcův	k2eAgFnSc4d1	otcova
církevní	církevní	k2eAgFnSc4d1	církevní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
kanovníkem	kanovník	k1gMnSc7	kanovník
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1148	[number]	k4	1148
i	i	k8xC	i
pražským	pražský	k2eAgMnSc7d1	pražský
proboštem	probošt	k1gMnSc7	probošt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Zdíkem	Zdík	k1gMnSc7	Zdík
se	se	k3xPyFc4	se
Kosmův	Kosmův	k2eAgMnSc1d1	Kosmův
syn	syn	k1gMnSc1	syn
dobře	dobře	k6eAd1	dobře
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1147	[number]	k4	1147
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
táhl	táhnout	k5eAaImAgMnS	táhnout
na	na	k7c4	na
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
proti	proti	k7c3	proti
pobaltským	pobaltský	k2eAgMnPc3d1	pobaltský
Prusům	Prus	k1gMnPc3	Prus
<g/>
.	.	kIx.	.
</s>
