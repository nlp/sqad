<s>
Kosmas	Kosmas	k1gMnSc1	Kosmas
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1045	[number]	k4	1045
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1125	[number]	k4	1125
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
známý	známý	k2eAgMnSc1d1	známý
český	český	k2eAgMnSc1d1	český
kronikář	kronikář	k1gMnSc1	kronikář
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Kroniky	kronika	k1gFnSc2	kronika
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
Chronica	Chronic	k1gInSc2	Chronic
Boemorum	Boemorum	k1gInSc1	Boemorum
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1120	[number]	k4	1120
až	až	k9	až
1125	[number]	k4	1125
děkanem	děkan	k1gMnSc7	děkan
pražské	pražský	k2eAgFnSc2d1	Pražská
kapituly	kapitula	k1gFnSc2	kapitula
při	při	k7c6	při
sv.	sv.	kA	sv.
Vítu	Vít	k1gMnSc6	Vít
<g/>
.	.	kIx.	.
</s>
