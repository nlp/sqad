<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
malými	malý	k2eAgFnPc7d1	malá
a	a	k8xC	a
středně	středně	k6eAd1	středně
velkými	velký	k2eAgMnPc7d1	velký
savci	savec	k1gMnPc7	savec
a	a	k8xC	a
ptáky	pták	k1gMnPc7	pták
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
požírá	požírat	k5eAaImIp3nS	požírat
také	také	k9	také
obojživelníky	obojživelník	k1gMnPc4	obojživelník
<g/>
,	,	kIx,	,
plazy	plaz	k1gMnPc4	plaz
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
větší	veliký	k2eAgInSc4d2	veliký
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
