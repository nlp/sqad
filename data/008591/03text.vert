<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1860	[number]	k4	1860
Vysoké	vysoká	k1gFnPc4	vysoká
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
za	za	k7c4	za
Rakouska-Uherska	Rakouska-Uhersek	k1gMnSc4	Rakouska-Uhersek
předák	předák	k1gInSc4	předák
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
účastník	účastník	k1gMnSc1	účastník
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
první	první	k4xOgMnSc1	první
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
předseda	předseda	k1gMnSc1	předseda
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Národního	národní	k2eAgNnSc2d1	národní
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
<g/>
,	,	kIx,	,
studium	studium	k1gNnSc1	studium
a	a	k8xC	a
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
realistickém	realistický	k2eAgNnSc6d1	realistické
hnutí	hnutí	k1gNnSc6	hnutí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
vyučeného	vyučený	k2eAgMnSc4d1	vyučený
zedníka	zedník	k1gMnSc4	zedník
Petra	Petr	k1gMnSc4	Petr
Kramáře	kramář	k1gMnSc4	kramář
ze	z	k7c2	z
Stanového	stanový	k2eAgNnSc2d1	stanové
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
regionálně	regionálně	k6eAd1	regionálně
uznávaného	uznávaný	k2eAgMnSc4d1	uznávaný
zámožného	zámožný	k2eAgMnSc4d1	zámožný
stavitele	stavitel	k1gMnSc4	stavitel
a	a	k8xC	a
majitele	majitel	k1gMnSc4	majitel
cihelny	cihelna	k1gFnSc2	cihelna
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Vodseďálková	Vodseďálková	k1gFnSc1	Vodseďálková
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rolnické	rolnický	k2eAgFnSc2d1	rolnická
rodiny	rodina	k1gFnSc2	rodina
vysockých	vysocký	k2eAgMnPc2d1	vysocký
starousedlíků	starousedlík	k1gMnPc2	starousedlík
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
dědeček	dědeček	k1gMnSc1	dědeček
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
přeživším	přeživší	k2eAgFnPc3d1	přeživší
z	z	k7c2	z
pěti	pět	k4xCc3	pět
dětí	dítě	k1gFnPc2	dítě
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
upjali	upnout	k5eAaPmAgMnP	upnout
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
budoucí	budoucí	k2eAgFnSc4d1	budoucí
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	let	k1gInPc6	let
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
opustil	opustit	k5eAaPmAgMnS	opustit
domov	domov	k1gInSc4	domov
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
reálném	reálný	k2eAgNnSc6d1	reálné
gymnasiu	gymnasion	k1gNnSc6	gymnasion
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
podnájmu	podnájem	k1gInSc6	podnájem
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
začal	začít	k5eAaPmAgMnS	začít
svá	svůj	k3xOyFgNnPc4	svůj
studia	studio	k1gNnPc4	studio
na	na	k7c6	na
malostranském	malostranský	k2eAgNnSc6d1	Malostranské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
mělo	mít	k5eAaImAgNnS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vlasteneckou	vlastenecký	k2eAgFnSc4d1	vlastenecká
pověst	pověst	k1gFnSc4	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
primy	prima	k1gFnSc2	prima
až	až	k9	až
do	do	k7c2	do
oktávy	oktáva	k1gFnSc2	oktáva
studoval	studovat	k5eAaImAgMnS	studovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
,	,	kIx,	,
učil	učit	k5eAaImAgMnS	učit
se	se	k3xPyFc4	se
jazykům	jazyk	k1gInPc3	jazyk
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
piáno	piáno	k?	piáno
a	a	k8xC	a
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
kreslil	kreslit	k5eAaImAgMnS	kreslit
a	a	k8xC	a
maloval	malovat	k5eAaImAgMnS	malovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
širší	široký	k2eAgInSc1d2	širší
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
v	v	k7c6	v
mladých	mladý	k2eAgNnPc6d1	mladé
<g/>
,	,	kIx,	,
studentských	studentský	k2eAgNnPc6d1	studentské
letech	léto	k1gNnPc6	léto
provázel	provázet	k5eAaImAgMnS	provázet
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Kramáře	kramář	k1gMnSc4	kramář
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1879	[number]	k4	1879
maturoval	maturovat	k5eAaBmAgInS	maturovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
a	a	k8xC	a
po	po	k7c6	po
prázdninách	prázdniny	k1gFnPc6	prázdniny
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
zapsat	zapsat	k5eAaPmF	zapsat
na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
letním	letní	k2eAgInSc6d1	letní
semestru	semestr	k1gInSc6	semestr
již	již	k6eAd1	již
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1880	[number]	k4	1880
byl	být	k5eAaImAgMnS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
jako	jako	k9	jako
posluchač	posluchač	k1gMnSc1	posluchač
práv	práv	k2eAgMnSc1d1	práv
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
universitě	universita	k1gFnSc6	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
velmi	velmi	k6eAd1	velmi
živě	živě	k6eAd1	živě
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
studentského	studentský	k2eAgInSc2d1	studentský
národního	národní	k2eAgInSc2d1	národní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1881	[number]	k4	1881
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
doktorem	doktor	k1gMnSc7	doktor
práv	právo	k1gNnPc2	právo
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
právnických	právnický	k2eAgFnPc2d1	právnická
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
nemusel	muset	k5eNaImAgInS	muset
živit	živit	k5eAaImF	živit
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
kariéru	kariéra	k1gFnSc4	kariéra
vysokoškolského	vysokoškolský	k2eAgMnSc4d1	vysokoškolský
pedagoga	pedagog	k1gMnSc4	pedagog
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgMnS	vypravit
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
studia	studio	k1gNnPc4	studio
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
studoval	studovat	k5eAaImAgMnS	studovat
národní	národní	k2eAgNnSc4d1	národní
hospodářství	hospodářství	k1gNnSc4	hospodářství
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
universitě	universita	k1gFnSc6	universita
u	u	k7c2	u
prof.	prof.	kA	prof.
Adolfa	Adolf	k1gMnSc2	Adolf
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
katedrových	katedrový	k2eAgMnPc2d1	katedrový
socialistů	socialist	k1gMnPc2	socialist
<g/>
"	"	kIx"	"
a	a	k8xC	a
odborníka	odborník	k1gMnSc4	odborník
finanční	finanční	k2eAgFnSc2d1	finanční
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
ještě	ještě	k9	ještě
týž	týž	k3xTgInSc1	týž
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
zapsat	zapsat	k5eAaPmF	zapsat
na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
svobodné	svobodný	k2eAgFnSc6d1	svobodná
škole	škola	k1gFnSc6	škola
politických	politický	k2eAgFnPc2d1	politická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gNnSc4	on
přilákala	přilákat	k5eAaPmAgFnS	přilákat
skvělá	skvělý	k2eAgFnSc1d1	skvělá
jména	jméno	k1gNnSc2	jméno
některých	některý	k3yIgMnPc2	některý
profesorů	profesor	k1gMnPc2	profesor
této	tento	k3xDgFnSc2	tento
školy	škola	k1gFnSc2	škola
zvláště	zvláště	k6eAd1	zvláště
Alberta	Albert	k1gMnSc2	Albert
Sorela	Sorel	k1gMnSc2	Sorel
<g/>
,	,	kIx,	,
Emila	Emil	k1gMnSc2	Emil
Boutmy	Boutma	k1gFnSc2	Boutma
<g/>
,	,	kIx,	,
Anatola	Anatola	k1gFnSc1	Anatola
Henry	henry	k1gInSc2	henry
Leroy-Beauliena	Leroy-Beaulieno	k1gNnSc2	Leroy-Beaulieno
a	a	k8xC	a
ruského	ruský	k2eAgMnSc4d1	ruský
státníka	státník	k1gMnSc4	státník
Nik	nika	k1gFnPc2	nika
<g/>
.	.	kIx.	.
</s>
<s>
Milutina	Milutina	k1gFnSc1	Milutina
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
celkem	celkem	k6eAd1	celkem
na	na	k7c6	na
pěti	pět	k4xCc6	pět
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
:	:	kIx,	:
vývoj	vývoj	k1gInSc4	vývoj
rakouského	rakouský	k2eAgNnSc2d1	rakouské
státního	státní	k2eAgNnSc2d1	státní
práva	právo	k1gNnSc2	právo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
národní	národní	k2eAgNnSc1d1	národní
hospodářství	hospodářství	k1gNnSc1	hospodářství
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
politické	politický	k2eAgFnPc4d1	politická
vědy	věda	k1gFnPc4	věda
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
také	také	k9	také
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Doktorský	doktorský	k2eAgInSc1d1	doktorský
diplom	diplom	k1gInSc1	diplom
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
<g/>
Systematicky	systematicky	k6eAd1	systematicky
a	a	k8xC	a
důsledně	důsledně	k6eAd1	důsledně
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
sblížil	sblížit	k5eAaPmAgMnS	sblížit
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bývalým	bývalý	k2eAgMnSc7d1	bývalý
pedagogem	pedagog	k1gMnSc7	pedagog
Josefem	Josef	k1gMnSc7	Josef
Kaizlem	Kaizl	k1gMnSc7	Kaizl
a	a	k8xC	a
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
ovládal	ovládat	k5eAaImAgInS	ovládat
dokonale	dokonale	k6eAd1	dokonale
čtyři	čtyři	k4xCgInPc4	čtyři
jazyky	jazyk	k1gInPc4	jazyk
-	-	kIx~	-
ruštinu	ruština	k1gFnSc4	ruština
<g/>
,	,	kIx,	,
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
němčinu	němčina	k1gFnSc4	němčina
a	a	k8xC	a
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
znal	znát	k5eAaImAgInS	znát
také	také	k9	také
základy	základ	k1gInPc4	základ
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
znamenitý	znamenitý	k2eAgMnSc1d1	znamenitý
debatér	debatér	k1gMnSc1	debatér
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
mladého	mladý	k2eAgNnSc2d1	mladé
politika	politikum	k1gNnSc2	politikum
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
finanční	finanční	k2eAgFnSc4d1	finanční
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
tkalcovnu	tkalcovna	k1gFnSc4	tkalcovna
v	v	k7c6	v
Libštátě	Libštáta	k1gFnSc6	Libštáta
<g/>
,	,	kIx,	,
disponoval	disponovat	k5eAaBmAgMnS	disponovat
akciemi	akcie	k1gFnPc7	akcie
v	v	k7c6	v
Pražské	pražský	k2eAgFnSc6d1	Pražská
akciové	akciový	k2eAgFnSc6d1	akciová
tiskárně	tiskárna	k1gFnSc6	tiskárna
<g/>
,	,	kIx,	,
estonské	estonský	k2eAgFnSc3d1	Estonská
Kränholmanufaktur	Kränholmanufaktura	k1gFnPc2	Kränholmanufaktura
v	v	k7c6	v
Tallinnu	Tallinno	k1gNnSc6	Tallinno
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
podnicích	podnik	k1gInPc6	podnik
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
majitelem	majitel	k1gMnSc7	majitel
cihelny	cihelna	k1gFnSc2	cihelna
v	v	k7c6	v
Semilech	Semily	k1gInPc6	Semily
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
posléze	posléze	k6eAd1	posléze
rozmnožil	rozmnožit	k5eAaPmAgInS	rozmnožit
i	i	k9	i
sňatek	sňatek	k1gInSc1	sňatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
Masarykem	Masaryk	k1gMnSc7	Masaryk
a	a	k8xC	a
Kaizlem	Kaizl	k1gMnSc7	Kaizl
na	na	k7c6	na
formulování	formulování	k1gNnSc6	formulování
nového	nový	k2eAgInSc2d1	nový
politického	politický	k2eAgInSc2d1	politický
směru	směr	k1gInSc2	směr
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
realismus	realismus	k1gInSc1	realismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
Kramář	kramář	k1gMnSc1	kramář
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Herbena	Herben	k2eAgFnSc1d1	Herbena
převzal	převzít	k5eAaPmAgInS	převzít
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
financoval	financovat	k5eAaBmAgInS	financovat
čtrnáctideník	čtrnáctideník	k1gInSc1	čtrnáctideník
Čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
redakci	redakce	k1gFnSc4	redakce
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
příspěvky	příspěvek	k1gInPc4	příspěvek
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
Antonín	Antonín	k1gMnSc1	Antonín
Rezek	Rezek	k1gMnSc1	Rezek
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Masarykem	Masaryk	k1gMnSc7	Masaryk
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
České	český	k2eAgFnSc2d1	Česká
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
–	–	k?	–
realistické	realistický	k2eAgNnSc1d1	realistické
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mladočeským	mladočeský	k2eAgMnSc7d1	mladočeský
politikem	politik	k1gMnSc7	politik
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vrcholící	vrcholící	k2eAgFnSc2d1	vrcholící
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
kampaně	kampaň	k1gFnSc2	kampaň
proti	proti	k7c3	proti
punktacím	punktace	k1gFnPc3	punktace
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Kaizl	Kaizl	k1gMnSc1	Kaizl
a	a	k8xC	a
Masaryk	Masaryk	k1gMnSc1	Masaryk
k	k	k7c3	k
mladočechům	mladočech	k1gMnPc3	mladočech
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
mladočechy	mladočech	k1gMnPc4	mladočech
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
celostátní	celostátní	k2eAgInSc1d1	celostátní
parlament	parlament	k1gInSc1	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
kurii	kurie	k1gFnSc4	kurie
venkovských	venkovský	k2eAgFnPc2d1	venkovská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Paka	Paka	k1gFnSc1	Paka
atd.	atd.	kA	atd.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
rovněž	rovněž	k9	rovněž
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
poslanec	poslanec	k1gMnSc1	poslanec
Českého	český	k2eAgInSc2d1	český
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
.	.	kIx.	.
<g/>
Zatímco	zatímco	k8xS	zatímco
Masaryk	Masaryk	k1gMnSc1	Masaryk
brzy	brzy	k6eAd1	brzy
od	od	k7c2	od
mladočechů	mladočech	k1gMnPc2	mladočech
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
,	,	kIx,	,
Kaizl	Kaizl	k1gMnSc1	Kaizl
s	s	k7c7	s
Kramářem	kramář	k1gMnSc7	kramář
zůstali	zůstat	k5eAaPmAgMnP	zůstat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
realistického	realistický	k2eAgInSc2d1	realistický
<g/>
,	,	kIx,	,
umírněného	umírněný	k2eAgInSc2d1	umírněný
proudu	proud	k1gInSc2	proud
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc6	rok
1894	[number]	k4	1894
byl	být	k5eAaImAgMnS	být
spoluautorem	spoluautor	k1gMnSc7	spoluautor
Nymburské	nymburský	k2eAgFnSc2d1	Nymburská
rezoluce	rezoluce	k1gFnSc2	rezoluce
mladočechů	mladočech	k1gMnPc2	mladočech
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vítězstvím	vítězství	k1gNnSc7	vítězství
umírněného	umírněný	k2eAgInSc2d1	umírněný
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
porážkou	porážka	k1gFnSc7	porážka
radikálně	radikálně	k6eAd1	radikálně
opozičního	opoziční	k2eAgInSc2d1	opoziční
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
opakovaně	opakovaně	k6eAd1	opakovaně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
spojenec	spojenec	k1gMnSc1	spojenec
Kaizla	Kaizla	k1gMnPc2	Kaizla
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
realistických	realistický	k2eAgMnPc2d1	realistický
mladočechů	mladočech	k1gMnPc2	mladočech
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
dekády	dekáda	k1gFnSc2	dekáda
jejich	jejich	k3xOp3gFnSc2	jejich
aktivity	aktivita	k1gFnSc2	aktivita
nasměrovaly	nasměrovat	k5eAaPmAgFnP	nasměrovat
dočasně	dočasně	k6eAd1	dočasně
mladočechy	mladočech	k1gMnPc4	mladočech
k	k	k7c3	k
provládnímu	provládní	k2eAgInSc3d1	provládní
táboru	tábor	k1gInSc3	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
postupný	postupný	k2eAgInSc4d1	postupný
proces	proces	k1gInSc4	proces
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
etapová	etapový	k2eAgFnSc1d1	etapová
politika	politika	k1gFnSc1	politika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
moci	moc	k1gFnSc2	moc
ujala	ujmout	k5eAaPmAgFnS	ujmout
vláda	vláda	k1gFnSc1	vláda
Kazimíra	Kazimír	k1gMnSc2	Kazimír
Badeniho	Badeni	k1gMnSc2	Badeni
<g/>
,	,	kIx,	,
podpořili	podpořit	k5eAaPmAgMnP	podpořit
mladočeši	mladočech	k1gMnPc1	mladočech
její	její	k3xOp3gInSc4	její
návrh	návrh	k1gInSc4	návrh
reformy	reforma	k1gFnSc2	reforma
volebního	volební	k2eAgInSc2d1	volební
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
Badeniho	Badeni	k1gMnSc2	Badeni
volební	volební	k2eAgFnSc1d1	volební
reforma	reforma	k1gFnSc1	reforma
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
dojednali	dojednat	k5eAaPmAgMnP	dojednat
i	i	k9	i
zásadní	zásadní	k2eAgFnSc4d1	zásadní
změnu	změna	k1gFnSc4	změna
úředního	úřední	k2eAgInSc2d1	úřední
statutu	statut	k1gInSc2	statut
češtiny	čeština	k1gFnSc2	čeština
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Badeniho	Badeniha	k1gFnSc5	Badeniha
jazyková	jazykový	k2eAgNnPc4d1	jazykové
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
mladočeská	mladočeský	k2eAgFnSc1d1	mladočeská
strana	strana	k1gFnSc1	strana
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
jako	jako	k9	jako
otevřeně	otevřeně	k6eAd1	otevřeně
provládní	provládní	k2eAgNnSc1d1	provládní
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
napsal	napsat	k5eAaPmAgMnS	napsat
Kaizlovi	Kaizl	k1gMnSc3	Kaizl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
už	už	k6eAd1	už
kousli	kousnout	k5eAaPmAgMnP	kousnout
do	do	k7c2	do
zapovězeného	zapovězený	k2eAgNnSc2d1	zapovězené
jablka	jablko	k1gNnSc2	jablko
–	–	k?	–
a	a	k8xC	a
tu	tu	k6eAd1	tu
nevázanost	nevázanost	k1gFnSc1	nevázanost
na	na	k7c4	na
všecky	všecek	k3xTgFnPc4	všecek
strany	strana	k1gFnPc4	strana
jsme	být	k5eAaImIp1nP	být
ztratili	ztratit	k5eAaPmAgMnP	ztratit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Badeniho	Badeni	k1gMnSc2	Badeni
vláda	vláda	k1gFnSc1	vláda
ale	ale	k9	ale
padla	padnout	k5eAaImAgFnS	padnout
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
německorakouských	německorakouský	k2eAgMnPc2d1	německorakouský
nacionalistů	nacionalista	k1gMnPc2	nacionalista
proti	proti	k7c3	proti
jazykovým	jazykový	k2eAgNnPc3d1	jazykové
nařízením	nařízení	k1gNnPc3	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kaizlem	Kaizl	k1gInSc7	Kaizl
ještě	ještě	k6eAd1	ještě
snažili	snažit	k5eAaImAgMnP	snažit
manévrovat	manévrovat	k5eAaImF	manévrovat
vůči	vůči	k7c3	vůči
následujícímu	následující	k2eAgInSc3d1	následující
vládnímu	vládní	k2eAgInSc3d1	vládní
kabinetu	kabinet	k1gInSc3	kabinet
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Paula	Paula	k1gFnSc1	Paula
Gautsche	Gautsche	k1gFnSc1	Gautsche
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěli	chtít	k5eNaImAgMnP	chtít
převzít	převzít	k5eAaPmF	převzít
radikálně	radikálně	k6eAd1	radikálně
nacionální	nacionální	k2eAgFnSc4d1	nacionální
rétoriku	rétorika	k1gFnSc4	rétorika
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
tehdy	tehdy	k6eAd1	tehdy
například	například	k6eAd1	například
v	v	k7c6	v
německorakouském	německorakouský	k2eAgInSc6d1	německorakouský
táboře	tábor	k1gInSc6	tábor
praktikoval	praktikovat	k5eAaImAgInS	praktikovat
Georg	Georg	k1gInSc1	Georg
von	von	k1gInSc1	von
Schönerer	Schönerer	k1gInSc4	Schönerer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Badeniho	Badeni	k1gMnSc2	Badeni
kabinetu	kabinet	k1gInSc2	kabinet
se	se	k3xPyFc4	se
prostor	prostor	k1gInSc1	prostor
pro	pro	k7c4	pro
výraznější	výrazný	k2eAgNnSc4d2	výraznější
naplnění	naplnění	k1gNnSc4	naplnění
českých	český	k2eAgInPc2d1	český
požadavků	požadavek	k1gInPc2	požadavek
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
pro	pro	k7c4	pro
úspěchy	úspěch	k1gInPc4	úspěch
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
zužuje	zužovat	k5eAaImIp3nS	zužovat
<g/>
.	.	kIx.	.
</s>
<s>
Gautschova	Gautschův	k2eAgNnPc1d1	Gautschův
jazyková	jazykový	k2eAgNnPc1d1	jazykové
nařízení	nařízení	k1gNnPc1	nařízení
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Gautsch	Gautsch	k1gMnSc1	Gautsch
seznámil	seznámit	k5eAaPmAgMnS	seznámit
Kaizla	Kaizla	k1gFnSc4	Kaizla
a	a	k8xC	a
Kramáře	kramář	k1gMnSc4	kramář
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
skromnějším	skromný	k2eAgInSc7d2	skromnější
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
neúspěšným	úspěšný	k2eNgInSc7d1	neúspěšný
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
revizi	revize	k1gFnSc4	revize
Badeniho	Badeni	k1gMnSc2	Badeni
jazykových	jazykový	k2eAgFnPc2d1	jazyková
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
stranyPo	stranyPo	k6eAd1	stranyPo
celá	celý	k2eAgFnSc1d1	celá
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
Kramář	kramář	k1gMnSc1	kramář
blízkým	blízký	k2eAgMnSc7d1	blízký
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Kaizla	Kaizla	k1gMnSc7	Kaizla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
poněkud	poněkud	k6eAd1	poněkud
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc1	jejich
názory	názor	k1gInPc1	názor
rozcházely	rozcházet	k5eAaImAgInP	rozcházet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
když	když	k8xS	když
koncem	koncem	k7c2	koncem
století	století	k1gNnSc2	století
Kramář	kramář	k1gMnSc1	kramář
otevřeně	otevřeně	k6eAd1	otevřeně
na	na	k7c6	na
parlamentní	parlamentní	k2eAgFnSc6d1	parlamentní
půdě	půda	k1gFnSc6	půda
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kaizl	Kaizl	k1gMnSc1	Kaizl
tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgMnS	být
loajálním	loajální	k2eAgMnSc7d1	loajální
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
v	v	k7c6	v
předlitavské	předlitavský	k2eAgFnSc6d1	předlitavská
vládě	vláda	k1gFnSc6	vláda
Franze	Franze	k1gFnSc1	Franze
Thuna-Hohensteina	Thuna-Hohensteina	k1gFnSc1	Thuna-Hohensteina
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
veřejně	veřejně	k6eAd1	veřejně
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
o	o	k7c6	o
Trojspolku	trojspolek	k1gInSc6	trojspolek
(	(	kIx(	(
<g/>
aliance	aliance	k1gFnSc1	aliance
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
starém	staré	k1gNnSc6	staré
<g/>
,	,	kIx,	,
obehraném	obehraný	k2eAgInSc6d1	obehraný
luxusním	luxusní	k2eAgInSc6d1	luxusní
klavíru	klavír	k1gInSc6	klavír
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sice	sice	k8xC	sice
ještě	ještě	k6eAd1	ještě
nemůžeme	moct	k5eNaImIp1nP	moct
odložit	odložit	k5eAaPmF	odložit
do	do	k7c2	do
skladiště	skladiště	k1gNnSc2	skladiště
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
také	také	k6eAd1	také
ale	ale	k9	ale
už	už	k6eAd1	už
současně	současně	k6eAd1	současně
nehrajeme	hrát	k5eNaImIp1nP	hrát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Nadále	nadále	k6eAd1	nadále
zůstával	zůstávat	k5eAaImAgInS	zůstávat
významným	významný	k2eAgMnSc7d1	významný
parlamentním	parlamentní	k2eAgMnSc7d1	parlamentní
politikem	politik	k1gMnSc7	politik
a	a	k8xC	a
řečníkem	řečník	k1gMnSc7	řečník
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
parlamentu	parlament	k1gInSc6	parlament
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
kurie	kurie	k1gFnSc1	kurie
venkovských	venkovský	k2eAgFnPc2d1	venkovská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
Jilemnice	Jilemnice	k1gFnPc1	Jilemnice
<g/>
,	,	kIx,	,
Semily	Semily	k1gInPc1	Semily
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
i	i	k8xC	i
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
městská	městský	k2eAgFnSc1d1	městská
kurie	kurie	k1gFnSc1	kurie
<g/>
,	,	kIx,	,
obvod	obvod	k1gInSc1	obvod
Tábor	Tábor	k1gInSc1	Tábor
<g/>
,	,	kIx,	,
Pacov	Pacov	k1gInSc1	Pacov
<g/>
,	,	kIx,	,
Kamenice	Kamenice	k1gFnSc1	Kamenice
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Josefa	Josef	k1gMnSc2	Josef
Kaizla	Kaizla	k1gMnSc2	Kaizla
postupně	postupně	k6eAd1	postupně
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
vláda	vláda	k1gFnSc1	vláda
Ernesta	Ernest	k1gMnSc2	Ernest
von	von	k1gInSc4	von
Koerbera	Koerbero	k1gNnSc2	Koerbero
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
o	o	k7c4	o
realističtější	realistický	k2eAgFnSc4d2	realističtější
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
usmiřovací	usmiřovací	k2eAgNnPc4d1	usmiřovací
česko-německá	českoěmecký	k2eAgNnPc4d1	česko-německé
jednání	jednání	k1gNnPc4	jednání
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Kramář	kramář	k1gMnSc1	kramář
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
formuloval	formulovat	k5eAaImAgInS	formulovat
český	český	k2eAgInSc1d1	český
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
státoprávním	státoprávní	k2eAgFnPc3d1	státoprávní
otázkám	otázka	k1gFnPc3	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Neodmítl	odmítnout	k5eNaPmAgInS	odmítnout
zcela	zcela	k6eAd1	zcela
požadavek	požadavek	k1gInSc1	požadavek
českých	český	k2eAgMnPc2d1	český
Němců	Němec	k1gMnPc2	Němec
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
krajského	krajský	k2eAgNnSc2d1	krajské
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kraje	kraj	k1gInPc1	kraj
byly	být	k5eAaImAgInP	být
podřízeny	podřídit	k5eAaPmNgInP	podřídit
přímo	přímo	k6eAd1	přímo
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vazby	vazba	k1gFnSc2	vazba
na	na	k7c4	na
celozemský	celozemský	k2eAgInSc4d1	celozemský
správní	správní	k2eAgInSc4d1	správní
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
byl	být	k5eAaImAgInS	být
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Předlitavsku	Předlitavsko	k1gNnSc6	Předlitavsko
začala	začít	k5eAaPmAgFnS	začít
vyostřovat	vyostřovat	k5eAaImF	vyostřovat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
demokratických	demokratický	k2eAgFnPc6d1	demokratická
reformách	reforma	k1gFnPc6	reforma
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
všeobecném	všeobecný	k2eAgNnSc6d1	všeobecné
volebním	volební	k2eAgNnSc6d1	volební
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
letní	letní	k2eAgFnSc2d1	letní
dovolené	dovolená	k1gFnSc2	dovolená
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
mladočeské	mladočeský	k2eAgFnSc6d1	mladočeská
straně	strana	k1gFnSc6	strana
agitovat	agitovat	k5eAaImF	agitovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
reformy	reforma	k1gFnSc2	reforma
a	a	k8xC	a
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
demokratickým	demokratický	k2eAgFnPc3d1	demokratická
tradicím	tradice	k1gFnPc3	tradice
mladočešství	mladočešství	k1gNnSc2	mladočešství
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
tehdy	tehdy	k6eAd1	tehdy
zformuloval	zformulovat	k5eAaPmAgInS	zformulovat
i	i	k9	i
ve	v	k7c6	v
spisku	spisek	k1gInSc6	spisek
Poznámky	poznámka	k1gFnSc2	poznámka
o	o	k7c6	o
české	český	k2eAgFnSc6d1	Česká
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Přihlásil	přihlásit	k5eAaPmAgInS	přihlásit
ke	k	k7c3	k
k	k	k7c3	k
umírněné	umírněný	k2eAgFnSc3d1	umírněná
tradici	tradice	k1gFnSc3	tradice
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
v	v	k7c6	v
mladočeské	mladočeský	k2eAgFnSc6d1	mladočeská
straně	strana	k1gFnSc6	strana
razili	razit	k5eAaImAgMnP	razit
Josef	Josef	k1gMnSc1	Josef
Kaizl	Kaizl	k1gMnSc1	Kaizl
nebo	nebo	k8xC	nebo
Gustav	Gustav	k1gMnSc1	Gustav
Eim	Eim	k1gMnSc1	Eim
<g/>
.	.	kIx.	.
</s>
<s>
Vyzýval	vyzývat	k5eAaImAgInS	vyzývat
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
českých	český	k2eAgMnPc2d1	český
liberálních	liberální	k2eAgMnPc2d1	liberální
politických	politický	k2eAgMnPc2d1	politický
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
aktivní	aktivní	k2eAgFnSc4d1	aktivní
parlamentní	parlamentní	k2eAgFnSc4d1	parlamentní
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
odmítal	odmítat	k5eAaImAgMnS	odmítat
prázdné	prázdný	k2eAgNnSc4d1	prázdné
agitování	agitování	k1gNnSc4	agitování
za	za	k7c4	za
státní	státní	k2eAgNnSc4d1	státní
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
vláda	vláda	k1gFnSc1	vláda
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
veřejnému	veřejný	k2eAgInSc3d1	veřejný
tlaku	tlak	k1gInSc3	tlak
a	a	k8xC	a
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
volební	volební	k2eAgFnSc4d1	volební
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
skutečně	skutečně	k6eAd1	skutečně
zaváděla	zavádět	k5eAaImAgFnS	zavádět
rovné	rovný	k2eAgInPc4d1	rovný
a	a	k8xC	a
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
na	na	k7c4	na
rychlou	rychlý	k2eAgFnSc4d1	rychlá
proměnu	proměna	k1gFnSc4	proměna
fungování	fungování	k1gNnSc2	fungování
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obstála	obstát	k5eAaPmAgFnS	obstát
v	v	k7c6	v
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
komplikovanější	komplikovaný	k2eAgFnSc3d2	komplikovanější
politické	politický	k2eAgFnSc3d1	politická
soutěži	soutěž	k1gFnSc3	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1906	[number]	k4	1906
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
formálně	formálně	k6eAd1	formálně
odsouhlaseny	odsouhlasen	k2eAgFnPc1d1	odsouhlasena
na	na	k7c6	na
stranickém	stranický	k2eAgInSc6d1	stranický
sjezdu	sjezd	k1gInSc6	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mladočechům	mladočech	k1gMnPc3	mladočech
tehdy	tehdy	k6eAd1	tehdy
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
četní	četný	k2eAgMnPc1d1	četný
bývalí	bývalý	k2eAgMnPc1d1	bývalý
pokrokáři	pokrokář	k1gMnPc1	pokrokář
a	a	k8xC	a
realisté	realista	k1gMnPc1	realista
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
integrovat	integrovat	k5eAaBmF	integrovat
nesocialistické	socialistický	k2eNgInPc4d1	nesocialistický
politické	politický	k2eAgInPc4d1	politický
proudy	proud	k1gInPc4	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novoslovanství	novoslovanství	k1gNnPc1	novoslovanství
a	a	k8xC	a
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
začala	začít	k5eAaPmAgFnS	začít
nová	nový	k2eAgFnSc1d1	nová
etapa	etapa	k1gFnSc1	etapa
provládního	provládní	k2eAgInSc2d1	provládní
aktivismu	aktivismus	k1gInSc2	aktivismus
mladočechů	mladočech	k1gMnPc2	mladočech
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
politika	politika	k1gFnSc1	politika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
Kramář	kramář	k1gMnSc1	kramář
odsouhlasil	odsouhlasit	k5eAaPmAgMnS	odsouhlasit
vstup	vstup	k1gInSc4	vstup
mladočecha	mladočech	k1gMnSc2	mladočech
Josefa	Josef	k1gMnSc2	Josef
Fořta	Fořt	k1gMnSc2	Fořt
do	do	k7c2	do
předlitavské	předlitavský	k2eAgFnSc2d1	předlitavská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1906	[number]	k4	1906
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
mladočechů	mladočech	k1gMnPc2	mladočech
na	na	k7c6	na
dosluhující	dosluhující	k2eAgFnSc6d1	dosluhující
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc4	obvod
Čechy	Čechy	k1gFnPc4	Čechy
0	[number]	k4	0
<g/>
33	[number]	k4	33
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
se	se	k3xPyFc4	se
ministrem	ministr	k1gMnSc7	ministr
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
vládě	vláda	k1gFnSc6	vláda
stal	stát	k5eAaPmAgMnS	stát
další	další	k2eAgMnSc1d1	další
mladočech	mladočech	k1gMnSc1	mladočech
František	František	k1gMnSc1	František
Fiedler	Fiedler	k1gMnSc1	Fiedler
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
politické	politický	k2eAgNnSc1d1	politické
spektrum	spektrum	k1gNnSc1	spektrum
(	(	kIx(	(
<g/>
zásadně	zásadně	k6eAd1	zásadně
proměněné	proměněný	k2eAgFnPc1d1	proměněná
volbami	volba	k1gFnPc7	volba
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Mladočeši	mladočech	k1gMnPc1	mladočech
<g/>
,	,	kIx,	,
agrárníci	agrárník	k1gMnPc1	agrárník
a	a	k8xC	a
klerikálové	klerikál	k1gMnPc1	klerikál
se	se	k3xPyFc4	se
stavěli	stavět	k5eAaImAgMnP	stavět
vstřícně	vstřícně	k6eAd1	vstřícně
k	k	k7c3	k
provládní	provládní	k2eAgFnSc3d1	provládní
spolupráci	spolupráce	k1gFnSc3	spolupráce
vůči	vůči	k7c3	vůči
kabinetu	kabinet	k1gInSc3	kabinet
Maxe	Max	k1gMnSc2	Max
Becka	Becek	k1gMnSc2	Becek
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
na	na	k7c6	na
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
frakci	frakce	k1gFnSc4	frakce
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc1d1	národní
klub	klub	k1gInSc1	klub
na	na	k7c6	na
radě	rada	k1gFnSc6	rada
říšské	říšský	k2eAgFnSc6d1	říšská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgFnPc1d1	ostatní
české	český	k2eAgFnPc1d1	Česká
nesocialistické	socialistický	k2eNgFnPc1d1	nesocialistická
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
činnosti	činnost	k1gFnSc6	činnost
nepodílely	podílet	k5eNaImAgFnP	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Provládní	provládní	k2eAgFnSc4d1	provládní
pozici	pozice	k1gFnSc4	pozice
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
Kramář	kramář	k1gMnSc1	kramář
i	i	k9	i
vůči	vůči	k7c3	vůči
následující	následující	k2eAgFnSc3d1	následující
vládě	vláda	k1gFnSc3	vláda
Richarda	Richard	k1gMnSc2	Richard
Bienertha	Bienerth	k1gMnSc2	Bienerth
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
mladočeši	mladočech	k1gMnPc1	mladočech
podpořili	podpořit	k5eAaPmAgMnP	podpořit
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vstřícná	vstřícný	k2eAgFnSc1d1	vstřícná
politika	politika	k1gFnSc1	politika
čelila	čelit	k5eAaImAgFnS	čelit
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
kritice	kritika	k1gFnSc3	kritika
(	(	kIx(	(
<g/>
na	na	k7c4	na
protivládní	protivládní	k2eAgFnPc4d1	protivládní
pozice	pozice	k1gFnPc4	pozice
přešli	přejít	k5eAaPmAgMnP	přejít
i	i	k8xC	i
někteří	některý	k3yIgMnPc1	některý
mladočeši	mladočech	k1gMnPc1	mladočech
a	a	k8xC	a
čeští	český	k2eAgMnPc1d1	český
agrárníci	agrárník	k1gMnPc1	agrárník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Kramář	kramář	k1gMnSc1	kramář
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
významně	významně	k6eAd1	významně
řešil	řešit	k5eAaImAgMnS	řešit
i	i	k9	i
zahraničněpolitické	zahraničněpolitický	k2eAgFnPc4d1	zahraničněpolitická
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
proslovanská	proslovanský	k2eAgFnSc1d1	proslovanský
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
nazývaná	nazývaný	k2eAgNnPc4d1	nazývané
novoslovanství	novoslovanství	k1gNnSc4	novoslovanství
<g/>
,	,	kIx,	,
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
slovanských	slovanský	k2eAgInPc2d1	slovanský
sjezdů	sjezd	k1gInPc2	sjezd
<g/>
.	.	kIx.	.
</s>
<s>
Zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
všeslovanské	všeslovanský	k2eAgInPc4d1	všeslovanský
sjezdy	sjezd	k1gInPc4	sjezd
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Sofii	Sofia	k1gFnSc6	Sofia
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Propagoval	propagovat	k5eAaImAgMnS	propagovat
myšlenky	myšlenka	k1gFnPc4	myšlenka
novoslovanství	novoslovanství	k1gNnSc2	novoslovanství
-	-	kIx~	-
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
sbližování	sbližování	k1gNnSc2	sbližování
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
solidarity	solidarita	k1gFnSc2	solidarita
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
nemělo	mít	k5eNaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
panslovanské	panslovanský	k2eAgFnPc4d1	panslovanská
koncepce	koncepce	k1gFnPc4	koncepce
namířené	namířený	k2eAgFnPc1d1	namířená
proti	proti	k7c3	proti
monarchii	monarchie	k1gFnSc3	monarchie
a	a	k8xC	a
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
Kramář	kramář	k1gMnSc1	kramář
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
soulad	soulad	k1gInSc4	soulad
mezi	mezi	k7c7	mezi
slovanskou	slovanský	k2eAgFnSc7d1	Slovanská
orientací	orientace	k1gFnSc7	orientace
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
zájmy	zájem	k1gInPc1	zájem
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
(	(	kIx(	(
<g/>
posilování	posilování	k1gNnSc3	posilování
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
východní	východní	k2eAgFnSc7d1	východní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc7d1	jihovýchodní
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
kultivace	kultivace	k1gFnSc1	kultivace
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
spojenectví	spojenectví	k1gNnPc2	spojenectví
se	s	k7c7	s
slovanskými	slovanský	k2eAgInPc7d1	slovanský
státy	stát	k1gInPc7	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovanský	slovanský	k2eAgInSc1d1	slovanský
element	element	k1gInSc1	element
v	v	k7c6	v
monarchii	monarchie	k1gFnSc6	monarchie
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
posílen	posílit	k5eAaPmNgInS	posílit
anexí	anexe	k1gFnSc7	anexe
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
(	(	kIx(	(
<g/>
mladočeši	mladočech	k1gMnPc1	mladočech
anexi	anexe	k1gFnSc4	anexe
podpořili	podpořit	k5eAaPmAgMnP	podpořit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
klíčový	klíčový	k2eAgInSc1d1	klíčový
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vojenských	vojenský	k2eAgInPc6d1	vojenský
výdajích	výdaj	k1gInPc6	výdaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
na	na	k7c6	na
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
zároveň	zároveň	k6eAd1	zároveň
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
utvoření	utvoření	k1gNnSc4	utvoření
volného	volný	k2eAgInSc2d1	volný
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
čeští	český	k2eAgMnPc1d1	český
<g/>
,	,	kIx,	,
jihoslovanští	jihoslovanský	k2eAgMnPc1d1	jihoslovanský
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
ukrajinští	ukrajinský	k2eAgMnPc1d1	ukrajinský
nesocialističtí	socialistický	k2eNgMnPc1d1	nesocialistický
poslanci	poslanec	k1gMnPc1	poslanec
<g/>
.	.	kIx.	.
<g/>
Pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
politika	politika	k1gFnSc1	politika
mladočechů	mladočech	k1gMnPc2	mladočech
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
čelila	čelit	k5eAaImAgFnS	čelit
dalším	další	k2eAgInPc3d1	další
otřesům	otřes	k1gInPc3	otřes
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
ministři	ministr	k1gMnPc1	ministr
Albín	Albín	k1gMnSc1	Albín
Bráf	Bráf	k1gMnSc1	Bráf
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Žáček	Žáček	k1gMnSc1	Žáček
opustili	opustit	k5eAaPmAgMnP	opustit
vládu	vláda	k1gFnSc4	vláda
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
krokům	krok	k1gInPc3	krok
některých	některý	k3yIgInPc2	některý
zemských	zemský	k2eAgInPc2d1	zemský
sněmů	sněm	k1gInPc2	sněm
v	v	k7c6	v
alpských	alpský	k2eAgFnPc6d1	alpská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
deklarovaly	deklarovat	k5eAaBmAgFnP	deklarovat
němčinu	němčina	k1gFnSc4	němčina
jako	jako	k8xS	jako
výlučný	výlučný	k2eAgInSc4d1	výlučný
místní	místní	k2eAgInSc4d1	místní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
ale	ale	k9	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
opoziční	opoziční	k2eAgFnSc2d1	opoziční
nálady	nálada	k1gFnSc2	nálada
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
obstrukcí	obstrukce	k1gFnPc2	obstrukce
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
převratnou	převratný	k2eAgFnSc4d1	převratná
změnu	změna	k1gFnSc4	změna
jednacího	jednací	k2eAgInSc2d1	jednací
řádu	řád	k1gInSc2	řád
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
výrazně	výrazně	k6eAd1	výrazně
ztížila	ztížit	k5eAaPmAgNnP	ztížit
obstruování	obstruování	k1gNnPc1	obstruování
<g/>
.	.	kIx.	.
</s>
<s>
Mladočeši	mladočech	k1gMnPc1	mladočech
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
opakovaně	opakovaně	k6eAd1	opakovaně
používali	používat	k5eAaImAgMnP	používat
radikální	radikální	k2eAgFnPc4d1	radikální
obstrukce	obstrukce	k1gFnPc4	obstrukce
jako	jako	k8xS	jako
politický	politický	k2eAgInSc4d1	politický
nástroj	nástroj	k1gInSc4	nástroj
tak	tak	k6eAd1	tak
nyní	nyní	k6eAd1	nyní
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
svého	svůj	k3xOyFgMnSc2	svůj
předáka	předák	k1gMnSc2	předák
tento	tento	k3xDgInSc4	tento
instrument	instrument	k1gInSc4	instrument
zavrhli	zavrhnout	k5eAaPmAgMnP	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
provládní	provládní	k2eAgFnSc1d1	provládní
většina	většina	k1gFnSc1	většina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
vytratila	vytratit	k5eAaPmAgFnS	vytratit
i	i	k9	i
Kramářova	kramářův	k2eAgFnSc1d1	Kramářova
koncepce	koncepce	k1gFnSc1	koncepce
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
selhání	selhání	k1gNnSc2	selhání
této	tento	k3xDgFnSc2	tento
strategie	strategie	k1gFnSc2	strategie
byl	být	k5eAaImAgInS	být
i	i	k9	i
Kramářův	kramářův	k2eAgInSc1d1	kramářův
politický	politický	k2eAgInSc1d1	politický
neúspěch	neúspěch	k1gInSc1	neúspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1910	[number]	k4	1910
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
za	za	k7c4	za
předsedu	předseda	k1gMnSc4	předseda
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
(	(	kIx(	(
<g/>
obnovena	obnoven	k2eAgFnSc1d1	obnovena
střechová	střechový	k2eAgFnSc1d1	střechová
frakce	frakce	k1gFnSc1	frakce
českých	český	k2eAgMnPc2d1	český
nesocialistických	socialistický	k2eNgMnPc2d1	nesocialistický
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
a	a	k8xC	a
místo	místo	k6eAd1	místo
něj	on	k3xPp3gNnSc4	on
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
František	František	k1gMnSc1	František
Fiedler	Fiedler	k1gMnSc1	Fiedler
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
získal	získat	k5eAaPmAgMnS	získat
dominantní	dominantní	k2eAgInSc4d1	dominantní
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
orientovala	orientovat	k5eAaBmAgFnS	orientovat
ryze	ryze	k6eAd1	ryze
opozičně	opozičně	k6eAd1	opozičně
<g/>
.	.	kIx.	.
<g/>
Kramář	kramář	k1gMnSc1	kramář
nicméně	nicméně	k8xC	nicméně
uspěl	uspět	k5eAaPmAgMnS	uspět
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
(	(	kIx(	(
<g/>
zvolen	zvolit	k5eAaPmNgInS	zvolit
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
Čechy	Čechy	k1gFnPc4	Čechy
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
místopředsedů	místopředseda	k1gMnPc2	místopředseda
rakouského	rakouský	k2eAgInSc2d1	rakouský
říšského	říšský	k2eAgInSc2d1	říšský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
vážně	vážně	k6eAd1	vážně
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
i	i	k9	i
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
jmenování	jmenování	k1gNnSc6	jmenování
na	na	k7c4	na
významný	významný	k2eAgInSc4d1	významný
post	post	k1gInSc4	post
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odboj	odboj	k1gInSc4	odboj
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
Kramář	kramář	k1gMnSc1	kramář
zastáncem	zastánce	k1gMnSc7	zastánce
federalizovaného	federalizovaný	k2eAgInSc2d1	federalizovaný
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
by	by	kYmCp3nP	by
úzce	úzko	k6eAd1	úzko
spolupracovaly	spolupracovat	k5eAaImAgInP	spolupracovat
slovanské	slovanský	k2eAgInPc1d1	slovanský
národy	národ	k1gInPc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Kramář	kramář	k1gMnSc1	kramář
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
dokument	dokument	k1gInSc4	dokument
s	s	k7c7	s
názvem	název	k1gInSc7	název
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
již	již	k9	již
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
nepočítal	počítat	k5eNaImAgMnS	počítat
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
plánoval	plánovat	k5eAaImAgMnS	plánovat
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
dále	daleko	k6eAd2	daleko
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
Rusy	Rus	k1gMnPc4	Rus
<g/>
,	,	kIx,	,
Bulhary	Bulhar	k1gMnPc4	Bulhar
<g/>
,	,	kIx,	,
Poláky	Polák	k1gMnPc4	Polák
<g/>
,	,	kIx,	,
Srby	Srb	k1gMnPc4	Srb
a	a	k8xC	a
Černohorce	Černohorec	k1gMnPc4	Černohorec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
odeslal	odeslat	k5eAaPmAgInS	odeslat
carské	carský	k2eAgFnSc3d1	carská
vládě	vláda	k1gFnSc3	vláda
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yQgFnSc2	jaký
míry	míra	k1gFnSc2	míra
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
pouhou	pouhý	k2eAgFnSc7d1	pouhá
<g/>
)	)	kIx)	)
výzvou	výzva	k1gFnSc7	výzva
představitelům	představitel	k1gMnPc3	představitel
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
politicky	politicky	k6eAd1	politicky
angažovat	angažovat	k5eAaBmF	angažovat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
či	či	k8xC	či
nakolik	nakolik	k6eAd1	nakolik
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
vzniku	vznik	k1gInSc3	vznik
přispělo	přispět	k5eAaPmAgNnS	přispět
soužití	soužití	k1gNnSc1	soužití
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Ruskou	ruský	k2eAgFnSc7d1	ruská
Naděždou	Naděžda	k1gFnSc7	Naděžda
Nikolajevnou	Nikolajevný	k2eAgFnSc4d1	Nikolajevný
Abrikosovovou	Abrikosovová	k1gFnSc4	Abrikosovová
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
začal	začít	k5eAaPmAgMnS	začít
Kramář	kramář	k1gMnSc1	kramář
organizovat	organizovat	k5eAaBmF	organizovat
domácí	domácí	k2eAgInSc4d1	domácí
odboj	odboj	k1gInSc4	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1915	[number]	k4	1915
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Aloisem	Alois	k1gMnSc7	Alois
Rašínem	Rašín	k1gMnSc7	Rašín
<g/>
,	,	kIx,	,
Vincencem	Vincenc	k1gMnSc7	Vincenc
Červinkou	Červinka	k1gMnSc7	Červinka
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Zamazalem	Zamazal	k1gMnSc7	Zamazal
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
a	a	k8xC	a
vyzvědačství	vyzvědačství	k1gNnSc4	vyzvědačství
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
jej	on	k3xPp3gInSc4	on
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
Eduard	Eduard	k1gMnSc1	Eduard
Koerner	Koerner	k1gMnSc1	Koerner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatčení	zatčení	k1gNnSc4	zatčení
a	a	k8xC	a
odsouzení	odsouzení	k1gNnSc4	odsouzení
Kramáře	kramář	k1gMnSc2	kramář
bylo	být	k5eAaImAgNnS	být
přelomovým	přelomový	k2eAgInSc7d1	přelomový
krokem	krok	k1gInSc7	krok
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
a	a	k8xC	a
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
rozsudku	rozsudek	k1gInSc2	rozsudek
i	i	k8xC	i
z	z	k7c2	z
fyzické	fyzický	k2eAgFnSc2d1	fyzická
existence	existence	k1gFnSc2	existence
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
zmizet	zmizet	k5eAaPmF	zmizet
přední	přední	k2eAgMnSc1d1	přední
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Uvedených	uvedený	k2eAgInPc2d1	uvedený
skutků	skutek	k1gInPc2	skutek
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
dopustit	dopustit	k5eAaPmF	dopustit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
organizováním	organizování	k1gNnSc7	organizování
všeslovanských	všeslovanský	k2eAgInPc2d1	všeslovanský
sjezdů	sjezd	k1gInPc2	sjezd
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
"	"	kIx"	"
<g/>
spáchání	spáchání	k1gNnSc4	spáchání
<g/>
"	"	kIx"	"
trestné	trestný	k2eAgFnSc2d1	trestná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1917	[number]	k4	1917
ztratil	ztratit	k5eAaPmAgMnS	ztratit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pravomocného	pravomocný	k2eAgInSc2d1	pravomocný
rozsudku	rozsudek	k1gInSc2	rozsudek
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1916	[number]	k4	1916
mandát	mandát	k1gInSc4	mandát
v	v	k7c6	v
Říšské	říšský	k2eAgFnSc6d1	říšská
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
války	válka	k1gFnSc2	válka
měnila	měnit	k5eAaImAgFnS	měnit
vnitropolitická	vnitropolitický	k2eAgFnSc1d1	vnitropolitická
a	a	k8xC	a
zahraničněpolitická	zahraničněpolitický	k2eAgFnSc1d1	zahraničněpolitická
orientace	orientace	k1gFnSc1	orientace
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
oslabení	oslabení	k1gNnSc4	oslabení
vlivu	vliv	k1gInSc2	vliv
generality	generalita	k1gFnSc2	generalita
na	na	k7c4	na
chod	chod	k1gInSc4	chod
státu	stát	k1gInSc2	stát
i	i	k8xC	i
dvora	dvůr	k1gInSc2	dvůr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1917	[number]	k4	1917
změněn	změnit	k5eAaPmNgMnS	změnit
na	na	k7c4	na
20	[number]	k4	20
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
<g />
.	.	kIx.	.
</s>
<s>
odsouzenými	odsouzený	k2eAgInPc7d1	odsouzený
amnestován	amnestovat	k5eAaBmNgMnS	amnestovat
nastoupivším	nastoupivší	k2eAgMnSc7d1	nastoupivší
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
I.	I.	kA	I.
(	(	kIx(	(
<g/>
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
součást	součást	k1gFnSc4	součást
velké	velký	k2eAgFnSc2d1	velká
amnestie	amnestie	k1gFnSc2	amnestie
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
gesto	gesto	k1gNnSc4	gesto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
usnadnit	usnadnit	k5eAaPmF	usnadnit
chystané	chystaný	k2eAgInPc4d1	chystaný
rozhovory	rozhovor	k1gInPc4	rozhovor
s	s	k7c7	s
českými	český	k2eAgMnPc7d1	český
politiky	politik	k1gMnPc7	politik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Historik	historik	k1gMnSc1	historik
Otto	Otto	k1gMnSc1	Otto
Urban	Urban	k1gMnSc1	Urban
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kramář	kramář	k1gMnSc1	kramář
se	se	k3xPyFc4	se
při	při	k7c6	při
případném	případný	k2eAgInSc6d1	případný
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
během	během	k7c2	během
války	válka	k1gFnSc2	válka
býval	bývat	k5eAaImAgInS	bývat
mohl	moct	k5eAaImAgMnS	moct
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
zkušenostem	zkušenost	k1gFnPc3	zkušenost
<g/>
,	,	kIx,	,
renomé	renomé	k1gNnSc3	renomé
a	a	k8xC	a
kontaktům	kontakt	k1gInPc3	kontakt
stát	stát	k5eAaImF	stát
logicky	logicky	k6eAd1	logicky
vůdcem	vůdce	k1gMnSc7	vůdce
českého	český	k2eAgInSc2d1	český
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
zatčení	zatčení	k1gNnSc1	zatčení
a	a	k8xC	a
uvěznění	uvěznění	k1gNnSc1	uvěznění
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
varianta	varianta	k1gFnSc1	varianta
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
a	a	k8xC	a
předákem	předák	k1gMnSc7	předák
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
okruh	okruh	k1gInSc4	okruh
okolo	okolo	k7c2	okolo
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
ale	ale	k9	ale
role	role	k1gFnSc1	role
Karla	Karel	k1gMnSc4	Karel
Kramáře	kramář	k1gMnSc4	kramář
byla	být	k5eAaImAgFnS	být
zásadní	zásadní	k2eAgFnSc1d1	zásadní
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
získal	získat	k5eAaPmAgMnS	získat
již	již	k6eAd1	již
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
pověst	pověst	k1gFnSc1	pověst
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
činnosti	činnost	k1gFnSc3	činnost
pro	pro	k7c4	pro
vlast	vlast	k1gFnSc4	vlast
riskuje	riskovat	k5eAaBmIp3nS	riskovat
vlastní	vlastní	k2eAgInSc1d1	vlastní
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
stanul	stanout	k5eAaPmAgMnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
ovšem	ovšem	k9	ovšem
nejprve	nejprve	k6eAd1	nejprve
pobýval	pobývat	k5eAaImAgMnS	pobývat
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
debat	debata	k1gFnPc2	debata
o	o	k7c4	o
směřování	směřování	k1gNnSc4	směřování
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
předáky	předák	k1gMnPc7	předák
národních	národní	k2eAgMnPc2d1	národní
sociálů	sociál	k1gMnPc2	sociál
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
návrhu	návrh	k1gInSc6	návrh
na	na	k7c4	na
vytvoření	vytvoření	k1gNnSc4	vytvoření
jednotné	jednotný	k2eAgFnSc2d1	jednotná
české	český	k2eAgFnSc2d1	Česká
nesocialistické	socialistický	k2eNgFnSc2d1	nesocialistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1917	[number]	k4	1917
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
zemských	zemský	k2eAgMnPc2d1	zemský
důvěrníků	důvěrník	k1gMnPc2	důvěrník
a	a	k8xC	a
aktivu	aktiv	k1gInSc2	aktiv
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
opět	opět	k6eAd1	opět
instalován	instalovat	k5eAaBmNgInS	instalovat
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Aloisem	Alois	k1gMnSc7	Alois
Rašínem	Rašín	k1gMnSc7	Rašín
<g/>
)	)	kIx)	)
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
o	o	k7c6	o
koncentraci	koncentrace	k1gFnSc6	koncentrace
politické	politický	k2eAgNnSc4d1	politické
spektra	spektrum	k1gNnPc4	spektrum
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1918	[number]	k4	1918
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
fúzi	fúze	k1gFnSc3	fúze
s	s	k7c7	s
několika	několik	k4yIc7	několik
menšími	malý	k2eAgFnPc7d2	menší
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Českou	český	k2eAgFnSc7d1	Česká
stranou	strana	k1gFnSc7	strana
státoprávně	státoprávně	k6eAd1	státoprávně
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Česká	český	k2eAgFnSc1d1	Česká
státoprávní	státoprávní	k2eAgFnSc1d1	státoprávní
demokracie	demokracie	k1gFnSc1	demokracie
(	(	kIx(	(
<g/>
od	od	k7c2	od
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
formace	formace	k1gFnSc2	formace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
přistoupili	přistoupit	k5eAaPmAgMnP	přistoupit
i	i	k9	i
staročeši	staročech	k1gMnPc1	staročech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prvním	první	k4xOgMnSc7	první
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
ČSR	ČSR	kA	ČSR
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
národní	národní	k2eAgMnSc1d1	národní
mučedník	mučedník	k1gMnSc1	mučedník
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1918	[number]	k4	1918
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
střechovým	střechový	k2eAgInSc7d1	střechový
orgánem	orgán	k1gInSc7	orgán
české	český	k2eAgFnSc2d1	Česká
politické	politický	k2eAgFnSc2d1	politická
scény	scéna	k1gFnSc2	scéna
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
zásadní	zásadní	k2eAgFnPc1d1	zásadní
proměny	proměna	k1gFnPc1	proměna
státoprávního	státoprávní	k2eAgNnSc2d1	státoprávní
uspořádání	uspořádání	k1gNnSc2	uspořádání
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
po	po	k7c4	po
25	[number]	k4	25
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
1918	[number]	k4	1918
ho	on	k3xPp3gMnSc4	on
kontaktoval	kontaktovat	k5eAaImAgInS	kontaktovat
nový	nový	k2eAgInSc1d1	nový
(	(	kIx(	(
<g/>
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
)	)	kIx)	)
předlitavský	předlitavský	k2eAgMnSc1d1	předlitavský
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Heinrich	Heinrich	k1gMnSc1	Heinrich
Lammasch	Lammasch	k1gMnSc1	Lammasch
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
získat	získat	k5eAaPmF	získat
Kramářovu	kramářův	k2eAgFnSc4d1	Kramářova
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
českou	český	k2eAgFnSc4d1	Česká
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
kabinet	kabinet	k1gInSc4	kabinet
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
provést	provést	k5eAaPmF	provést
federalizaci	federalizace	k1gFnSc4	federalizace
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
již	již	k6eAd1	již
ale	ale	k8xC	ale
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
taková	takový	k3xDgFnSc1	takový
jednání	jednání	k1gNnSc3	jednání
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.28	.28	k4	.28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Kramář	kramář	k1gMnSc1	kramář
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
představitelů	představitel	k1gMnPc2	představitel
domácího	domácí	k2eAgMnSc2d1	domácí
a	a	k8xC	a
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
stane	stanout	k5eAaPmIp3nS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nové	nový	k2eAgFnSc2d1	nová
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
zprvu	zprvu	k6eAd1	zprvu
propagoval	propagovat	k5eAaImAgMnS	propagovat
představu	představa	k1gFnSc4	představa
monarchistického	monarchistický	k2eAgNnSc2d1	monarchistické
zřízení	zřízení	k1gNnSc2	zřízení
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
republikánské	republikánský	k2eAgFnSc2d1	republikánská
formy	forma	k1gFnSc2	forma
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
změnila	změnit	k5eAaPmAgFnS	změnit
Kramářova	kramářův	k2eAgFnSc1d1	Kramářova
strana	strana	k1gFnSc1	strana
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
na	na	k7c4	na
Československou	československý	k2eAgFnSc4d1	Československá
národní	národní	k2eAgFnSc4d1	národní
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kramář	kramář	k1gMnSc1	kramář
byl	být	k5eAaImAgMnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1918	[number]	k4	1918
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prvním	první	k4xOgMnSc6	první
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
ustavující	ustavující	k2eAgNnSc4d1	ustavující
jednání	jednání	k1gNnSc4	jednání
řídil	řídit	k5eAaImAgMnS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
ústy	ústa	k1gNnPc7	ústa
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
rod	rod	k1gInSc1	rod
habsbursko-lotrinský	habsburskootrinský	k2eAgInSc1d1	habsbursko-lotrinský
zbaven	zbavit	k5eAaPmNgMnS	zbavit
všech	všecek	k3xTgNnPc2	všecek
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
za	za	k7c4	za
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
shromáždění	shromáždění	k1gNnSc6	shromáždění
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
(	(	kIx(	(
<g/>
jednomyslně	jednomyslně	k6eAd1	jednomyslně
<g/>
,	,	kIx,	,
aklamací	aklamace	k1gFnSc7	aklamace
<g/>
)	)	kIx)	)
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
ministerského	ministerský	k2eAgNnSc2d1	ministerské
předsednictví	předsednictví	k1gNnSc2	předsednictví
vedl	vést	k5eAaImAgMnS	vést
Kramář	kramář	k1gMnSc1	kramář
československou	československý	k2eAgFnSc4d1	Československá
delegaci	delegace	k1gFnSc4	delegace
na	na	k7c6	na
Pařížské	pařížský	k2eAgFnSc6d1	Pařížská
mírové	mírový	k2eAgFnSc6d1	mírová
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Prosazoval	prosazovat	k5eAaImAgInS	prosazovat
tam	tam	k6eAd1	tam
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
vítězné	vítězný	k2eAgFnPc4d1	vítězná
mocnosti	mocnost	k1gFnPc4	mocnost
intervenovat	intervenovat	k5eAaImF	intervenovat
proti	proti	k7c3	proti
bolševické	bolševický	k2eAgFnSc3d1	bolševická
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
požadavkem	požadavek	k1gInSc7	požadavek
však	však	k9	však
pro	pro	k7c4	pro
Benešův	Benešův	k2eAgInSc4d1	Benešův
odpor	odpor	k1gInSc4	odpor
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gFnSc1	jeho
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Ruska	Rusko	k1gNnSc2	Rusko
nelze	lze	k6eNd1	lze
zajistit	zajistit	k5eAaPmF	zajistit
trvalejší	trvalý	k2eAgInSc4d2	trvalejší
mír	mír	k1gInSc4	mír
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
prorockou	prorocký	k2eAgFnSc7d1	prorocká
v	v	k7c6	v
době	doba	k1gFnSc6	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
i	i	k8xC	i
války	válka	k1gFnSc2	válka
studené	studený	k2eAgFnSc2d1	studená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
konference	konference	k1gFnSc2	konference
se	se	k3xPyFc4	se
vyostřily	vyostřit	k5eAaPmAgInP	vyostřit
ideové	ideový	k2eAgInPc1d1	ideový
i	i	k8xC	i
osobní	osobní	k2eAgInPc4d1	osobní
rozpory	rozpor	k1gInPc4	rozpor
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
padla	padnout	k5eAaImAgFnS	padnout
po	po	k7c6	po
obecních	obecní	k2eAgFnPc6d1	obecní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
skončily	skončit	k5eAaPmAgFnP	skončit
pro	pro	k7c4	pro
národní	národní	k2eAgInSc4d1	národní
demokraty	demokrat	k1gMnPc7	demokrat
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
další	další	k2eAgFnSc6d1	další
československé	československý	k2eAgFnSc6d1	Československá
vládě	vláda	k1gFnSc6	vláda
již	již	k6eAd1	již
nepůsobil	působit	k5eNaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předsedou	předseda	k1gMnSc7	předseda
národních	národní	k2eAgMnPc2d1	národní
demokratů	demokrat	k1gMnPc2	demokrat
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgMnS	být
Kramář	kramář	k1gMnSc1	kramář
sice	sice	k8xC	sice
předsedou	předseda	k1gMnSc7	předseda
své	svůj	k3xOyFgFnSc2	svůj
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
faktickou	faktický	k2eAgFnSc4d1	faktická
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
měli	mít	k5eAaImAgMnP	mít
jiní	jiný	k1gMnPc1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
zastával	zastávat	k5eAaImAgMnS	zastávat
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
a	a	k8xC	a
nacionalistické	nacionalistický	k2eAgInPc4d1	nacionalistický
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
národního	národní	k2eAgInSc2d1	národní
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
národnostního	národnostní	k2eAgInSc2d1	národnostní
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
–	–	k?	–
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
kritiků	kritik	k1gMnPc2	kritik
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Hradu	hrad	k1gInSc2	hrad
<g/>
"	"	kIx"	"
–	–	k?	–
řadu	řad	k1gInSc2	řad
ideových	ideový	k2eAgInPc2d1	ideový
i	i	k8xC	i
osobních	osobní	k2eAgInPc2d1	osobní
sporů	spor	k1gInPc2	spor
(	(	kIx(	(
<g/>
kupříkladu	kupříkladu	k6eAd1	kupříkladu
o	o	k7c4	o
zásluhy	zásluha	k1gFnPc4	zásluha
domácího	domácí	k2eAgMnSc2d1	domácí
a	a	k8xC	a
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
<g/>
)	)	kIx)	)
s	s	k7c7	s
Benešem	Beneš	k1gMnSc7	Beneš
i	i	k8xC	i
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Národního	národní	k2eAgNnSc2d1	národní
sjednocení	sjednocení	k1gNnSc2	sjednocení
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
Revolučního	revoluční	k2eAgNnSc2d1	revoluční
národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
trvale	trvale	k6eAd1	trvale
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
poslancem	poslanec	k1gMnSc7	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
pravoslavného	pravoslavný	k2eAgInSc2d1	pravoslavný
kostela	kostel	k1gInSc2	kostel
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
Naděždy	Naděžda	k1gFnSc2	Naděžda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
platil	platit	k5eAaImAgMnS	platit
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
za	za	k7c4	za
dobrého	dobrý	k2eAgMnSc4d1	dobrý
společníka	společník	k1gMnSc4	společník
a	a	k8xC	a
přesvědčivého	přesvědčivý	k2eAgMnSc4d1	přesvědčivý
diskutéra	diskutér	k1gMnSc4	diskutér
<g/>
,	,	kIx,	,
stranil	stranit	k5eAaImAgInS	stranit
se	se	k3xPyFc4	se
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
se	se	k3xPyFc4	se
na	na	k7c6	na
studijní	studijní	k2eAgFnSc6d1	studijní
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Rusku	Rusko	k1gNnSc6	Rusko
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
do	do	k7c2	do
salónu	salón	k1gInSc2	salón
tamní	tamní	k2eAgFnSc2d1	tamní
intelektuální	intelektuální	k2eAgFnSc2d1	intelektuální
elity	elita	k1gFnSc2	elita
u	u	k7c2	u
Alexeje	Alexej	k1gMnSc2	Alexej
Alexejeviče	Alexejevič	k1gMnSc2	Alexejevič
Abrikosova	Abrikosův	k2eAgMnSc2d1	Abrikosův
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spolumajitele	spolumajitel	k1gMnSc2	spolumajitel
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
cukrovinky	cukrovinka	k1gFnPc4	cukrovinka
a	a	k8xC	a
konzervárny	konzervárna	k1gFnPc4	konzervárna
na	na	k7c4	na
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Naděždy	Naděžda	k1gFnSc2	Naděžda
Nikolajevny	Nikolajevna	k1gFnSc2	Nikolajevna
Abrikosové	Abrikosový	k2eAgFnSc2d1	Abrikosový
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Chludové	Chludová	k1gFnSc2	Chludová
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
šťastně	šťastně	k6eAd1	šťastně
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Kramář	kramář	k1gMnSc1	kramář
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
cestoval	cestovat	k5eAaImAgMnS	cestovat
také	také	k9	také
do	do	k7c2	do
jejich	jejich	k3xOp3gNnSc2	jejich
letního	letní	k2eAgNnSc2d1	letní
sídla	sídlo	k1gNnSc2	sídlo
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
.	.	kIx.	.
</s>
<s>
Vášnivě	vášnivě	k6eAd1	vášnivě
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
měl	mít	k5eAaImAgInS	mít
příležitost	příležitost	k1gFnSc4	příležitost
pozvat	pozvat	k5eAaPmF	pozvat
manžele	manžel	k1gMnPc4	manžel
Abrikosovy	Abrikosův	k2eAgMnPc4d1	Abrikosův
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
kontaktům	kontakt	k1gInPc3	kontakt
Naděždě	Naděžda	k1gFnSc6	Naděžda
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
gynekologickou	gynekologický	k2eAgFnSc4d1	gynekologická
operaci	operace	k1gFnSc4	operace
u	u	k7c2	u
věhlasného	věhlasný	k2eAgMnSc2d1	věhlasný
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc2	profesor
Eduarda	Eduard	k1gMnSc2	Eduard
Alberta	Albert	k1gMnSc2	Albert
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Naděždině	Naděždin	k2eAgFnSc3d1	Naděždin
rekonvalescenci	rekonvalescence	k1gFnSc3	rekonvalescence
pak	pak	k6eAd1	pak
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
Abrikosov	Abrikosov	k1gInSc1	Abrikosov
domů	dům	k1gInPc2	dům
vrátil	vrátit	k5eAaPmAgInS	vrátit
sám	sám	k3xTgInSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Naděžda	Naděžda	k1gFnSc1	Naděžda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
definitivně	definitivně	k6eAd1	definitivně
opustila	opustit	k5eAaPmAgFnS	opustit
manžela	manžel	k1gMnSc4	manžel
i	i	k8xC	i
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
bydlela	bydlet	k5eAaImAgFnS	bydlet
zprvu	zprvu	k6eAd1	zprvu
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
nedaleko	nedaleko	k7c2	nedaleko
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatbě	svatba	k1gFnSc3	svatba
předcházel	předcházet	k5eAaImAgInS	předcházet
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
poměry	poměr	k1gInPc4	poměr
skandální	skandální	k2eAgInSc4d1	skandální
intimní	intimní	k2eAgInSc4d1	intimní
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Kramář	kramář	k1gMnSc1	kramář
dlouho	dlouho	k6eAd1	dlouho
úspěšně	úspěšně	k6eAd1	úspěšně
tajil	tajit	k5eAaImAgMnS	tajit
i	i	k9	i
před	před	k7c7	před
blízkými	blízký	k2eAgMnPc7d1	blízký
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rozvodu	rozvod	k1gInSc2	rozvod
pro	pro	k7c4	pro
Naděždu	Naděžda	k1gFnSc4	Naděžda
nebylo	být	k5eNaImAgNnS	být
snadné	snadný	k2eAgNnSc1d1	snadné
-	-	kIx~	-
v	v	k7c6	v
pravoslavné	pravoslavný	k2eAgFnSc6d1	pravoslavná
církvi	církev	k1gFnSc6	církev
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
porušil	porušit	k5eAaPmAgInS	porušit
<g/>
-li	i	k?	-li
některý	některý	k3yIgInSc1	některý
z	z	k7c2	z
manželů	manžel	k1gMnPc2	manžel
manželský	manželský	k2eAgInSc4d1	manželský
slib	slib	k1gInSc4	slib
<g/>
;	;	kIx,	;
viník	viník	k1gMnSc1	viník
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
potrestán	potrestat	k5eAaPmNgInS	potrestat
zákazem	zákaz	k1gInSc7	zákaz
dalšího	další	k2eAgInSc2d1	další
sňatku	sňatek	k1gInSc2	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Podvedený	podvedený	k2eAgInSc1d1	podvedený
Abrikosov	Abrikosov	k1gInSc1	Abrikosov
se	se	k3xPyFc4	se
neozýval	ozývat	k5eNaImAgInS	ozývat
<g/>
,	,	kIx,	,
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
Naděžda	Naděžda	k1gFnSc1	Naděžda
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
novou	nový	k2eAgFnSc4d1	nová
partnerku	partnerka	k1gFnSc4	partnerka
a	a	k8xC	a
nemanželské	manželský	k2eNgFnPc4d1	nemanželská
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Podala	podat	k5eAaPmAgFnS	podat
žádost	žádost	k1gFnSc1	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
patriarcha	patriarcha	k1gMnSc1	patriarcha
ruské	ruský	k2eAgFnSc2d1	ruská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
její	její	k3xOp3gNnSc4	její
manželství	manželství	k1gNnSc4	manželství
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
Karla	Karel	k1gMnSc2	Karel
Kramáře	kramář	k1gMnSc2	kramář
a	a	k8xC	a
Naděždy	Naděžda	k1gFnSc2	Naděžda
Abrikosovové	Abrikosovová	k1gFnSc2	Abrikosovová
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1900	[number]	k4	1900
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
v	v	k7c6	v
zámečku	zámeček	k1gInSc6	zámeček
Gaspro	Gaspro	k1gNnSc4	Gaspro
hraběnky	hraběnka	k1gFnSc2	hraběnka
Paniny	Panin	k2eAgFnSc2d1	Panin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
si	se	k3xPyFc3	se
manželé	manžel	k1gMnPc1	manžel
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
přepychovou	přepychový	k2eAgFnSc4d1	přepychová
vilu	vila	k1gFnSc4	vila
Barbo	Barba	k1gFnSc5	Barba
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
projektantem	projektant	k1gMnSc7	projektant
byl	být	k5eAaImAgMnS	být
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Kotěra	Kotěra	k1gFnSc1	Kotěra
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
trávili	trávit	k5eAaImAgMnP	trávit
každé	každý	k3xTgFnPc4	každý
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
prázdniny	prázdniny	k1gFnPc4	prázdniny
až	až	k9	až
do	do	k7c2	do
bolševické	bolševický	k2eAgFnSc2d1	bolševická
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
vila	vila	k1gFnSc1	vila
zkonfiskována	zkonfiskovat	k5eAaPmNgFnS	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1911	[number]	k4	1911
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
si	se	k3xPyFc3	se
Kramářovi	kramář	k1gMnSc3	kramář
postavili	postavit	k5eAaPmAgMnP	postavit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
<g/>
,	,	kIx,	,
na	na	k7c6	na
Baště	bašta	k1gFnSc6	bašta
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
(	(	kIx(	(
<g/>
Kramářova	kramářův	k2eAgFnSc1d1	Kramářova
vila	vila	k1gFnSc1	vila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
architektem	architekt	k1gMnSc7	architekt
byl	být	k5eAaImAgMnS	být
původem	původ	k1gInSc7	původ
Rakušan	Rakušan	k1gMnSc1	Rakušan
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc2d1	uměleckoprůmyslová
školy	škola	k1gFnSc2	škola
Friedrich	Friedrich	k1gMnSc1	Friedrich
Ohmann	Ohmann	k1gMnSc1	Ohmann
<g/>
,	,	kIx,	,
první	první	k4xOgInPc4	první
náčrty	náčrt	k1gInPc4	náčrt
vily	vila	k1gFnSc2	vila
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
paní	paní	k1gFnSc1	paní
Naděžda	Naděžda	k1gFnSc1	Naděžda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
výtvarné	výtvarný	k2eAgFnPc4d1	výtvarná
ambice	ambice	k1gFnPc4	ambice
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
výzdobu	výzdoba	k1gFnSc4	výzdoba
vedl	vést	k5eAaImAgMnS	vést
další	další	k2eAgMnSc1d1	další
pedagog	pedagog	k1gMnSc1	pedagog
UP	UP	kA	UP
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
Celestin	Celestin	k1gMnSc1	Celestin
Klouček	Klouček	k1gMnSc1	Klouček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
šťastné	šťastný	k2eAgNnSc1d1	šťastné
a	a	k8xC	a
plné	plný	k2eAgFnPc1d1	plná
něhy	něha	k1gFnPc1	něha
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
handicapu	handicap	k1gInSc3	handicap
paní	paní	k1gFnSc1	paní
Naděždy	Naděžda	k1gFnSc2	Naděžda
bezdětné	bezdětný	k2eAgFnSc2d1	bezdětná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naděžda	Naděžda	k1gFnSc1	Naděžda
Nikolajevna	Nikolajevna	k1gFnSc1	Nikolajevna
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
zbohatlých	zbohatlý	k2eAgMnPc2d1	zbohatlý
nevolníků	nevolník	k1gMnPc2	nevolník
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
dědeček	dědeček	k1gMnSc1	dědeček
tkadlec	tkadlec	k1gMnSc1	tkadlec
Naran	Naran	k1gMnSc1	Naran
Chludov	Chludovo	k1gNnPc2	Chludovo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
získali	získat	k5eAaPmAgMnP	získat
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
zbohatli	zbohatnout	k5eAaPmAgMnP	zbohatnout
za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
podnikáním	podnikání	k1gNnSc7	podnikání
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Naděžda	Naděžda	k1gFnSc1	Naděžda
byla	být	k5eAaImAgFnS	být
podnikavá	podnikavý	k2eAgFnSc1d1	podnikavá
<g/>
,	,	kIx,	,
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
textilní	textilní	k2eAgFnSc4d1	textilní
továrnu	továrna	k1gFnSc4	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
její	její	k3xOp3gFnSc1	její
nevyrovnaná	vyrovnaný	k2eNgFnSc1d1	nevyrovnaná
povaha	povaha	k1gFnSc1	povaha
ji	on	k3xPp3gFnSc4	on
hnala	hnát	k5eAaImAgFnS	hnát
ke	k	k7c3	k
stále	stále	k6eAd1	stále
silnější	silný	k2eAgFnSc3d2	silnější
touze	touha	k1gFnSc3	touha
po	po	k7c6	po
majetku	majetek	k1gInSc6	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Shromažďovala	shromažďovat	k5eAaImAgFnS	shromažďovat
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
kožichy	kožich	k1gInPc1	kožich
<g/>
,	,	kIx,	,
starožitnosti	starožitnost	k1gFnPc1	starožitnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
chtěla	chtít	k5eAaImAgFnS	chtít
založit	založit	k5eAaPmF	založit
svůj	svůj	k3xOyFgInSc4	svůj
umělecký	umělecký	k2eAgInSc4d1	umělecký
salón	salón	k1gInSc4	salón
<g/>
,	,	kIx,	,
ráda	rád	k2eAgFnSc1d1	ráda
hostila	hostit	k5eAaImAgFnS	hostit
výtvarné	výtvarný	k2eAgMnPc4d1	výtvarný
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
krajináře	krajinář	k1gMnSc4	krajinář
Beneše	Beneš	k1gMnSc4	Beneš
Knüpfera	Knüpfer	k1gMnSc4	Knüpfer
<g/>
.	.	kIx.	.
</s>
<s>
Náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
krymskou	krymský	k2eAgFnSc4d1	Krymská
rezidenci	rezidence	k1gFnSc4	rezidence
si	se	k3xPyFc3	se
Naděžda	Naděžda	k1gFnSc1	Naděžda
umínila	umínit	k5eAaPmAgFnS	umínit
vystavět	vystavět	k5eAaPmF	vystavět
vilu	vila	k1gFnSc4	vila
(	(	kIx(	(
<g/>
Větrov	Větrov	k1gInSc4	Větrov
<g/>
)	)	kIx)	)
v	v	k7c6	v
ruském	ruský	k2eAgInSc6d1	ruský
stylu	styl	k1gInSc6	styl
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
;	;	kIx,	;
megalomanský	megalomanský	k2eAgInSc1d1	megalomanský
projekt	projekt	k1gInSc1	projekt
paní	paní	k1gFnSc2	paní
Kramářové	Kramářové	k?	Kramářové
oba	dva	k4xCgInPc1	dva
stárnoucí	stárnoucí	k2eAgMnPc4d1	stárnoucí
a	a	k8xC	a
nemocné	nemocný	k2eAgMnPc4d1	nemocný
manžele	manžel	k1gMnPc4	manžel
finančně	finančně	k6eAd1	finančně
zruinoval	zruinovat	k5eAaPmAgInS	zruinovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Naděžda	Naděžda	k1gFnSc1	Naděžda
zemřela	zemřít	k5eAaPmAgFnS	zemřít
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1936	[number]	k4	1936
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
do	do	k7c2	do
krypty	krypta	k1gFnSc2	krypta
v	v	k7c6	v
pravoslavném	pravoslavný	k2eAgInSc6d1	pravoslavný
Uspenském	Uspenský	k2eAgInSc6d1	Uspenský
chrámu	chrám	k1gInSc6	chrám
na	na	k7c6	na
Olšanech	Olšany	k1gInPc6	Olšany
(	(	kIx(	(
<g/>
Chrám	chrám	k1gInSc1	chrám
Zesnutí	zesnutí	k1gNnSc2	zesnutí
Přesvaté	přesvatý	k2eAgFnSc2d1	přesvatá
Bohorodice	bohorodice	k1gFnSc2	bohorodice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
pěti	pět	k4xCc6	pět
měsících	měsíc	k1gInPc6	měsíc
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
i	i	k9	i
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výtvarné	výtvarný	k2eAgFnPc4d1	výtvarná
objednávky	objednávka	k1gFnPc4	objednávka
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
měl	mít	k5eAaImAgMnS	mít
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
estetiku	estetika	k1gFnSc4	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
a	a	k8xC	a
vybraně	vybraně	k6eAd1	vybraně
se	se	k3xPyFc4	se
oblékal	oblékat	k5eAaImAgMnS	oblékat
<g/>
,	,	kIx,	,
dbal	dbát	k5eAaImAgMnS	dbát
o	o	k7c4	o
šperky	šperk	k1gInPc4	šperk
a	a	k8xC	a
galanterii	galanterie	k1gFnSc4	galanterie
<g/>
,	,	kIx,	,
rád	rád	k6eAd1	rád
se	se	k3xPyFc4	se
dával	dávat	k5eAaImAgMnS	dávat
zpodobňovat	zpodobňovat	k5eAaImF	zpodobňovat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
portrétních	portrétní	k2eAgFnPc2d1	portrétní
fotografií	fotografia	k1gFnPc2	fotografia
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
z	z	k7c2	z
pražského	pražský	k2eAgInSc2d1	pražský
ateliéru	ateliér	k1gInSc2	ateliér
Jana	Jan	k1gMnSc2	Jan
Langhanse	Langhans	k1gMnSc2	Langhans
<g/>
,	,	kIx,	,
z	z	k7c2	z
vídeňského	vídeňský	k2eAgNnSc2d1	Vídeňské
Luckhardtova	Luckhardtův	k2eAgNnSc2d1	Luckhardtův
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
malovaných	malovaný	k2eAgInPc2d1	malovaný
či	či	k8xC	či
kreslených	kreslený	k2eAgInPc2d1	kreslený
portrétů	portrét	k1gInPc2	portrét
vynikají	vynikat	k5eAaImIp3nP	vynikat
olejomalba	olejomalba	k1gFnSc1	olejomalba
Vratislava	Vratislav	k1gMnSc2	Vratislav
Nechleby	Nechleba	k1gMnSc2	Nechleba
<g/>
,	,	kIx,	,
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Váchy	Vácha	k1gMnSc2	Vácha
<g/>
,	,	kIx,	,
či	či	k8xC	či
Otty	Otta	k1gMnSc2	Otta
Peterse	Peterse	k1gFnSc1	Peterse
a	a	k8xC	a
kresba	kresba	k1gFnSc1	kresba
Maxe	Max	k1gMnSc2	Max
Švabinského	Švabinský	k2eAgMnSc2d1	Švabinský
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
Kramář	kramář	k1gMnSc1	kramář
přátelil	přátelit	k5eAaImAgMnS	přátelit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
objednávku	objednávka	k1gFnSc4	objednávka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nejméně	málo	k6eAd3	málo
šest	šest	k4xCc1	šest
portrétních	portrétní	k2eAgFnPc2d1	portrétní
bust	busta	k1gFnPc2	busta
<g/>
,	,	kIx,	,
od	od	k7c2	od
Vilíma	Vilím	k1gMnSc2	Vilím
Amorta	Amort	k1gMnSc2	Amort
<g/>
,	,	kIx,	,
Bohumila	Bohumil	k1gMnSc2	Bohumil
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
,	,	kIx,	,
Františka	František	k1gMnSc2	František
Rouse	Rous	k1gMnSc2	Rous
<g/>
,	,	kIx,	,
Františka	František	k1gMnSc2	František
Foita	Foit	k1gMnSc2	Foit
<g/>
,	,	kIx,	,
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Šaffa	Šaff	k1gMnSc2	Šaff
<g/>
,	,	kIx,	,
či	či	k8xC	či
brněnského	brněnský	k2eAgMnSc2d1	brněnský
sochaře	sochař	k1gMnSc2	sochař
Josefa	Josef	k1gMnSc2	Josef
Pakosty	pakosta	k1gMnSc2	pakosta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kramář	kramář	k1gMnSc1	kramář
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
salón	salón	k1gInSc1	salón
navštěvovaný	navštěvovaný	k2eAgInSc1d1	navštěvovaný
umělci	umělec	k1gMnPc7	umělec
a	a	k8xC	a
vědci	vědec	k1gMnPc7	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Finančně	finančně	k6eAd1	finančně
podporoval	podporovat	k5eAaImAgMnS	podporovat
například	například	k6eAd1	například
sochaře	sochař	k1gMnSc4	sochař
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
Šaffa	Šaff	k1gMnSc4	Šaff
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
jak	jak	k6eAd1	jak
Kramářovu	kramářův	k2eAgFnSc4d1	Kramářova
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
bustu	busta	k1gFnSc4	busta
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
figurální	figurální	k2eAgInSc4d1	figurální
náhrobek	náhrobek	k1gInSc4	náhrobek
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
množství	množství	k1gNnSc1	množství
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
osobních	osobní	k2eAgFnPc2d1	osobní
památek	památka	k1gFnPc2	památka
přešlo	přejít	k5eAaPmAgNnS	přejít
s	s	k7c7	s
Kramářovou	kramářův	k2eAgFnSc7d1	Kramářova
pozůstalostí	pozůstalost	k1gFnSc7	pozůstalost
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
Společnosti	společnost	k1gFnSc2	společnost
Karla	Karel	k1gMnSc4	Karel
Kramáře	kramář	k1gMnSc4	kramář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
zrušení	zrušení	k1gNnSc6	zrušení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byly	být	k5eAaImAgFnP	být
cennosti	cennost	k1gFnPc1	cennost
z	z	k7c2	z
Kramářovy	kramářův	k2eAgFnSc2d1	Kramářova
vily	vila	k1gFnSc2	vila
uloženy	uložen	k2eAgInPc4d1	uložen
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
a	a	k8xC	a
zatajeny	zatajen	k2eAgInPc1d1	zatajen
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
ostatním	ostatní	k2eAgInSc7d1	ostatní
movitým	movitý	k2eAgInSc7d1	movitý
majetkem	majetek	k1gInSc7	majetek
zkonfiskovány	zkonfiskován	k2eAgFnPc4d1	zkonfiskována
a	a	k8xC	a
tímto	tento	k3xDgNnSc7	tento
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
do	do	k7c2	do
Archivu	archiv	k1gInSc2	archiv
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgInPc1d3	nejcennější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
diamantový	diamantový	k2eAgInSc1d1	diamantový
náhrdelník	náhrdelník	k1gInSc1	náhrdelník
Naděždy	Naděžda	k1gFnSc2	Naděžda
Kramářové	Kramářové	k?	Kramářové
<g/>
,	,	kIx,	,
asi	asi	k9	asi
největší	veliký	k2eAgInSc4d3	veliký
diamantový	diamantový	k2eAgInSc4d1	diamantový
šperk	šperk	k1gInSc4	šperk
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LUSTIGOVÁ	LUSTIGOVÁ	kA	LUSTIGOVÁ
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
:	:	kIx,	:
první	první	k4xOgMnSc1	první
československý	československý	k2eAgMnSc1d1	československý
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
363	[number]	k4	363
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
898	[number]	k4	898
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSATÍK	KOSATÍK	kA	KOSATÍK
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
demokraté	demokrat	k1gMnPc1	demokrat
:	:	kIx,	:
50	[number]	k4	50
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
2307	[number]	k4	2307
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČECHUROVÁ	Čechurová	k1gFnSc1	Čechurová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
STEHLÍKOVÁ	Stehlíková	k1gFnSc1	Stehlíková
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
VANDROVCOVÁ	VANDROVCOVÁ	kA	VANDROVCOVÁ
Miroslava	Miroslava	k1gFnSc1	Miroslava
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
a	a	k8xC	a
Naděžda	Naděžda	k1gFnSc1	Naděžda
Kramářovi	kramářův	k2eAgMnPc1d1	kramářův
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
;	;	kIx,	;
stran	stran	k7c2	stran
142	[number]	k4	142
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-87041-20-8	[number]	k4	978-80-87041-20-8
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
<g/>
STEHLÍKOVÁ	Stehlíková	k1gFnSc1	Stehlíková
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
:	:	kIx,	:
Éra	éra	k1gFnSc1	éra
cylindrů	cylindr	k1gInPc2	cylindr
a	a	k8xC	a
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
k	k	k7c3	k
životnímu	životní	k2eAgInSc3d1	životní
stylu	styl	k1gInSc3	styl
manželů	manžel	k1gMnPc2	manžel
Kramářových	kramářův	k2eAgInPc2d1	kramářův
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
-	-	kIx~	-
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
–	–	k?	–
suplementum	suplementum	k1gNnSc1	suplementum
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Bílek	Bílek	k1gMnSc1	Bílek
a	a	k8xC	a
Luboš	Luboš	k1gMnSc1	Luboš
Velek	Velek	k1gMnSc1	Velek
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Masarykův	Masarykův	k2eAgInSc1d1	Masarykův
ústav	ústav	k1gInSc1	ústav
a	a	k8xC	a
Archiv	archiv	k1gInSc1	archiv
AV	AV	kA	AV
ČR	ČR	kA	ČR
v	v	k7c6	v
Nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
stran	strana	k1gFnPc2	strana
784	[number]	k4	784
<g/>
,	,	kIx,	,
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
s.	s.	k?	s.
616	[number]	k4	616
<g/>
-	-	kIx~	-
<g/>
631	[number]	k4	631
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6860	[number]	k4	6860
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
MÚA	MÚA	kA	MÚA
978-80-86495-58-3	[number]	k4	978-80-86495-58-3
a	a	k8xC	a
HiU	HiU	k1gMnSc3	HiU
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7286	[number]	k4	7286
<g/>
-	-	kIx~	-
<g/>
157	[number]	k4	157
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
MALÍŘ	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
,	,	kIx,	,
s.	s.	k?	s.
139	[number]	k4	139
<g/>
-	-	kIx~	-
<g/>
162	[number]	k4	162
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6860	[number]	k4	6860
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WINKLEROVÁ	Winklerová	k1gFnSc1	Winklerová
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
348	[number]	k4	348
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
348	[number]	k4	348
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČERVINKA	Červinka	k1gMnSc1	Červinka
<g/>
,	,	kIx,	,
Vincenc	Vincenc	k1gMnSc1	Vincenc
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
:	:	kIx,	:
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
a	a	k8xC	a
význam	význam	k1gInSc4	význam
:	:	kIx,	:
k	k	k7c3	k
70	[number]	k4	70
<g/>
tým	tým	k1gInSc1	tým
narozeninám	narozeniny	k1gFnPc3	narozeniny
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
slovanského	slovanský	k2eAgNnSc2d1	slovanské
politika	politikum	k1gNnSc2	politikum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
nár	nár	k?	nár
<g/>
.	.	kIx.	.
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
64	[number]	k4	64
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PRECLÍK	preclík	k1gInSc1	preclík
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
legie	legie	k1gFnSc1	legie
<g/>
,	,	kIx,	,
váz	váza	k1gFnPc2	váza
<g/>
.	.	kIx.	.
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
219	[number]	k4	219
str	str	kA	str
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Paris	Paris	k1gMnSc1	Paris
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
,	,	kIx,	,
Žižkova	Žižkův	k2eAgFnSc1d1	Žižkova
2379	[number]	k4	2379
(	(	kIx(	(
<g/>
734	[number]	k4	734
01	[number]	k4	01
Karviná	Karviná	k1gFnSc1	Karviná
<g/>
)	)	kIx)	)
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
demokratickým	demokratický	k2eAgNnSc7d1	demokratické
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87173	[number]	k4	87173
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
s.	s.	k?	s.
8	[number]	k4	8
-	-	kIx~	-
48	[number]	k4	48
<g/>
,	,	kIx,	,
s.	s.	k?	s.
95	[number]	k4	95
-	-	kIx~	-
116	[number]	k4	116
<g/>
,	,	kIx,	,
s.	s.	k?	s.
125	[number]	k4	125
-	-	kIx~	-
148	[number]	k4	148
<g/>
,	,	kIx,	,
s.	s.	k?	s.
157	[number]	k4	157
<g/>
-	-	kIx~	-
<g/>
160	[number]	k4	160
<g/>
,	,	kIx,	,
s.	s.	k?	s.
165	[number]	k4	165
-	-	kIx~	-
169	[number]	k4	169
</s>
</p>
<p>
<s>
===	===	k?	===
Prameny	pramen	k1gInPc1	pramen
===	===	k?	===
</s>
</p>
<p>
<s>
Vandrovcová	Vandrovcový	k2eAgFnSc1d1	Vandrovcová
<g/>
,	,	kIx,	,
Miroslava	Miroslava	k1gFnSc1	Miroslava
<g/>
:	:	kIx,	:
Fond	fond	k1gInSc1	fond
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
<g/>
,	,	kIx,	,
inventář	inventář	k1gInSc1	inventář
<g/>
;	;	kIx,	;
Archiv	archiv	k1gInSc1	archiv
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Karla	Karel	k1gMnSc2	Karel
Kramáře	kramář	k1gMnSc2	kramář
</s>
</p>
<p>
<s>
Kramářova	kramářův	k2eAgFnSc1d1	Kramářova
vila	vila	k1gFnSc1	vila
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Martina	Martina	k1gFnSc1	Martina
Lustigova	Lustigův	k2eAgFnSc1d1	Lustigova
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
-	-	kIx~	-
osudy	osud	k1gInPc1	osud
prvního	první	k4xOgMnSc4	první
československého	československý	k2eAgMnSc4d1	československý
premiéra	premiér	k1gMnSc4	premiér
</s>
</p>
