<s>
Karel	Karel	k1gMnSc1	Karel
Kramář	kramář	k1gMnSc1	kramář
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1860	[number]	k4	1860
Vysoké	vysoká	k1gFnPc4	vysoká
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1937	[number]	k4	1937
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
za	za	k7c4	za
Rakouska-Uherska	Rakouska-Uhersek	k1gMnSc4	Rakouska-Uhersek
předák	předák	k1gInSc4	předák
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
účastník	účastník	k1gMnSc1	účastník
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
první	první	k4xOgMnSc1	první
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
předseda	předseda	k1gMnSc1	předseda
Československé	československý	k2eAgFnSc2d1	Československá
národní	národní	k2eAgFnSc2d1	národní
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Národního	národní	k2eAgNnSc2d1	národní
sjednocení	sjednocení	k1gNnSc2	sjednocení
<g/>
.	.	kIx.	.
</s>
