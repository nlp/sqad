<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
Pitt	Pitt	k1gInSc1	Pitt
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
jako	jako	k8xS	jako
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
Voight	Voight	k1gMnSc1	Voight
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1975	[number]	k4	1975
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
vyslankyně	vyslankyně	k1gFnSc1	vyslankyně
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
amerického	americký	k2eAgMnSc2d1	americký
herce	herec	k1gMnSc2	herec
Jona	Jonus	k1gMnSc2	Jonus
Voighta	Voight	k1gMnSc2	Voight
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
pracovala	pracovat	k5eAaImAgFnS	pracovat
také	také	k9	také
jako	jako	k9	jako
modelka	modelka	k1gFnSc1	modelka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
žen	žena	k1gFnPc2	žena
světa	svět	k1gInSc2	svět
a	a	k8xC	a
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
soukromém	soukromý	k2eAgInSc6d1	soukromý
životě	život	k1gInSc6	život
informují	informovat	k5eAaBmIp3nP	informovat
nejen	nejen	k6eAd1	nejen
bulvární	bulvární	k2eAgNnPc1d1	bulvární
média	médium	k1gNnPc1	médium
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelkou	držitelka	k1gFnSc7	držitelka
tří	tři	k4xCgFnPc2	tři
cen	cena	k1gFnPc2	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
filmového	filmový	k2eAgMnSc4d1	filmový
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
zahájila	zahájit	k5eAaPmAgFnS	zahájit
rolí	role	k1gFnSc7	role
v	v	k7c6	v
nízkorozpočtovém	nízkorozpočtový	k2eAgInSc6d1	nízkorozpočtový
filmu	film	k1gInSc6	film
Cyborg	Cyborg	k1gMnSc1	Cyborg
II	II	kA	II
<g/>
:	:	kIx,	:
Skleněný	skleněný	k2eAgInSc1d1	skleněný
stín	stín	k1gInSc1	stín
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
síť	síť	k1gFnSc1	síť
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
životopisných	životopisný	k2eAgInPc6d1	životopisný
filmech	film	k1gInPc6	film
George	Georg	k1gInSc2	Georg
Wallace	Wallace	k1gFnSc1	Wallace
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gia	Gia	k1gMnSc1	Gia
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
kladné	kladný	k2eAgInPc4d1	kladný
ohlasy	ohlas	k1gInPc4	ohlas
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Oscara	Oscara	k1gFnSc1	Oscara
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Narušení	narušení	k1gNnSc2	narušení
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
věhlasu	věhlas	k1gInSc2	věhlas
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
díky	díky	k7c3	díky
roli	role	k1gFnSc3	role
Lary	Lara	k1gFnSc2	Lara
Croft	Crofta	k1gFnPc2	Crofta
<g/>
,	,	kIx,	,
hrdinky	hrdinka	k1gFnSc2	hrdinka
série	série	k1gFnSc2	série
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Lara	Lara	k1gMnSc1	Lara
Croft	Croft	k1gMnSc1	Croft
–	–	k?	–
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgFnPc2d1	placená
hvězd	hvězda	k1gFnPc2	hvězda
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
doposud	doposud	k6eAd1	doposud
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
s	s	k7c7	s
akční	akční	k2eAgFnSc7d1	akční
komedií	komedie	k1gFnSc7	komedie
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
&	&	k?	&
Mrs	Mrs	k1gFnSc1	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
animovanými	animovaný	k2eAgInPc7d1	animovaný
filmy	film	k1gInPc7	film
Kung	Kunga	k1gFnPc2	Kunga
Fu	fu	k0	fu
Panda	panda	k1gFnSc1	panda
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kung	Kung	k1gMnSc1	Kung
Fu	fu	k0	fu
Panda	panda	k1gFnSc1	panda
2	[number]	k4	2
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
herci	herec	k1gMnPc7	herec
Jonnym	Jonnym	k1gInSc4	Jonnym
Lee	Lea	k1gFnSc3	Lea
Millerem	Miller	k1gMnSc7	Miller
a	a	k8xC	a
Billym	Billym	k1gInSc4	Billym
Bobem	Bob	k1gMnSc7	Bob
Thorntonem	Thornton	k1gInSc7	Thornton
skončila	skončit	k5eAaPmAgFnS	skončit
rozvodem	rozvod	k1gInSc7	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Bradem	Bradem	k?	Bradem
Pittem	Pitt	k1gMnSc7	Pitt
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
jsou	být	k5eAaImIp3nP	být
manželé	manžel	k1gMnPc1	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Pittem	Pitto	k1gNnSc7	Pitto
mají	mít	k5eAaImIp3nP	mít
dcery	dcera	k1gFnPc4	dcera
Shiloh	Shiloha	k1gFnPc2	Shiloha
a	a	k8xC	a
Vivienne	Vivienn	k1gInSc5	Vivienn
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Knoxe	Knoxe	k1gFnSc2	Knoxe
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
adoptovali	adoptovat	k5eAaPmAgMnP	adoptovat
děti	dítě	k1gFnPc4	dítě
Maddoxe	Maddoxe	k1gFnSc2	Maddoxe
<g/>
,	,	kIx,	,
Zaharu	Zahar	k1gInSc2	Zahar
a	a	k8xC	a
Paxe	Pax	k1gFnSc2	Pax
<g/>
.	.	kIx.	.
</s>
<s>
Angažuje	angažovat	k5eAaBmIp3nS	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
humanitárních	humanitární	k2eAgInPc6d1	humanitární
projektech	projekt	k1gInPc6	projekt
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
problematiky	problematika	k1gFnSc2	problematika
uprchlíků	uprchlík	k1gMnPc2	uprchlík
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
UNHCR	UNHCR	kA	UNHCR
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
povýšila	povýšit	k5eAaPmAgFnS	povýšit
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Angelinu	Angelin	k2eAgFnSc4d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
herců	herc	k1gInPc2	herc
Jona	Jonum	k1gNnSc2	Jonum
Voighta	Voight	k1gInSc2	Voight
a	a	k8xC	a
Marcheline	Marchelin	k1gInSc5	Marchelin
Bertrandové	Bertrandový	k2eAgInPc4d1	Bertrandový
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
bratrem	bratr	k1gMnSc7	bratr
je	být	k5eAaImIp3nS	být
herec	herec	k1gMnSc1	herec
James	James	k1gMnSc1	James
Haven	Havna	k1gFnPc2	Havna
<g/>
,	,	kIx,	,
strýcem	strýc	k1gMnSc7	strýc
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
Chip	Chip	k1gMnSc1	Chip
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kmotřenkou	kmotřenka	k1gFnSc7	kmotřenka
herců	herc	k1gInPc2	herc
Maximiliana	Maximilian	k1gMnSc4	Maximilian
Schella	Schell	k1gMnSc4	Schell
a	a	k8xC	a
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Bissetové	Bissetový	k2eAgInPc5d1	Bissetový
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
zdědila	zdědit	k5eAaPmAgFnS	zdědit
německou	německý	k2eAgFnSc7d1	německá
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
smíšeného	smíšený	k2eAgInSc2d1	smíšený
francouzsko-kanadského	francouzskoanadský	k2eAgInSc2d1	francouzsko-kanadský
<g/>
,	,	kIx,	,
holandského	holandský	k2eAgInSc2d1	holandský
a	a	k8xC	a
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
cítí	cítit	k5eAaImIp3nP	cítit
být	být	k5eAaImF	být
částečnou	částečný	k2eAgFnSc7d1	částečná
Irokézkou	Irokézka	k1gFnSc7	Irokézka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
jediný	jediný	k2eAgMnSc1d1	jediný
známý	známý	k1gMnSc1	známý
domorodý	domorodý	k2eAgInSc4d1	domorodý
předek	předek	k1gInSc4	předek
byla	být	k5eAaImAgFnS	být
žena	žena	k1gFnSc1	žena
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Huronů	Huron	k1gMnPc2	Huron
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1649	[number]	k4	1649
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
rodičů	rodič	k1gMnPc2	rodič
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
žila	žít	k5eAaImAgFnS	žít
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
svých	svůj	k3xOyFgFnPc2	svůj
hereckých	herecký	k2eAgFnPc2d1	herecká
ambicí	ambice	k1gFnPc2	ambice
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
výchovy	výchova	k1gFnSc2	výchova
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
Jolie	Jolie	k1gFnSc1	Jolie
často	často	k6eAd1	často
sledovala	sledovat	k5eAaImAgFnS	sledovat
filmy	film	k1gInPc4	film
společně	společně	k6eAd1	společně
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jak	jak	k8xC	jak
později	pozdě	k6eAd2	pozdě
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
podnětů	podnět	k1gInPc2	podnět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ji	on	k3xPp3gFnSc4	on
přivedly	přivést	k5eAaPmAgInP	přivést
k	k	k7c3	k
herectví	herectví	k1gNnSc3	herectví
<g/>
;	;	kIx,	;
nebyla	být	k5eNaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
producentem	producent	k1gMnSc7	producent
Billem	Bill	k1gMnSc7	Bill
Dayem	Day	k1gMnSc7	Day
<g/>
)	)	kIx)	)
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
;	;	kIx,	;
do	do	k7c2	do
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c6	na
Lee	Lea	k1gFnSc6	Lea
Strasberg	Strasberg	k1gMnSc1	Strasberg
Theatre	Theatr	k1gInSc5	Theatr
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávila	strávit	k5eAaPmAgFnS	strávit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
divadelních	divadelní	k2eAgNnPc6d1	divadelní
představeních	představení	k1gNnPc6	představení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
opustila	opustit	k5eAaPmAgFnS	opustit
hereckou	herecký	k2eAgFnSc4d1	herecká
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
stát	stát	k5eAaPmF	stát
ředitelkou	ředitelka	k1gFnSc7	ředitelka
pohřebního	pohřební	k2eAgInSc2d1	pohřební
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
především	především	k9	především
na	na	k7c6	na
přehlídkách	přehlídka	k1gFnPc6	přehlídka
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
chodila	chodit	k5eAaImAgFnS	chodit
v	v	k7c6	v
černém	černý	k2eAgInSc6d1	černý
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
přítelem	přítel	k1gMnSc7	přítel
experimentovala	experimentovat	k5eAaImAgFnS	experimentovat
s	s	k7c7	s
BDSM	BDSM	kA	BDSM
(	(	kIx(	(
<g/>
hrátky	hrátky	k1gFnPc1	hrátky
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vztah	vztah	k1gInSc4	vztah
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
skončil	skončit	k5eAaPmAgMnS	skončit
<g/>
,	,	kIx,	,
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
si	se	k3xPyFc3	se
byt	byt	k1gInSc4	byt
nad	nad	k7c7	nad
garáží	garáž	k1gFnSc7	garáž
několik	několik	k4yIc4	několik
bloků	blok	k1gInPc2	blok
od	od	k7c2	od
domu	dům	k1gInSc2	dům
její	její	k3xOp3gFnSc2	její
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Dokončila	dokončit	k5eAaPmAgFnS	dokončit
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
herectví	herectví	k1gNnSc2	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
komentovala	komentovat	k5eAaBmAgFnS	komentovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
stále	stále	k6eAd1	stále
jsem	být	k5eAaImIp1nS	být
–	–	k?	–
a	a	k8xC	a
vždycky	vždycky	k6eAd1	vždycky
budu	být	k5eAaImBp1nS	být
–	–	k?	–
mladá	mladá	k1gFnSc1	mladá
punkerka	punkerka	k1gFnSc1	punkerka
s	s	k7c7	s
tetováním	tetování	k1gNnSc7	tetování
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
dospívání	dospívání	k1gNnSc2	dospívání
trpěla	trpět	k5eAaImAgFnS	trpět
vážnými	vážný	k2eAgFnPc7d1	vážná
depresemi	deprese	k1gFnPc7	deprese
se	s	k7c7	s
sebevražednými	sebevražedný	k2eAgInPc7d1	sebevražedný
sklony	sklon	k1gInPc7	sklon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Beverly	Beverla	k1gFnPc4	Beverla
Hills	Hills	k1gInSc4	Hills
High	High	k1gInSc1	High
School	School	k1gInSc1	School
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
z	z	k7c2	z
bohatých	bohatý	k2eAgFnPc2d1	bohatá
rodin	rodina	k1gFnPc2	rodina
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
cítila	cítit	k5eAaImAgFnS	cítit
osamělá	osamělý	k2eAgFnSc1d1	osamělá
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
terčem	terč	k1gInSc7	terč
posměšků	posměšek	k1gInPc2	posměšek
kvůli	kvůli	k7c3	kvůli
velmi	velmi	k6eAd1	velmi
hubené	hubený	k2eAgFnSc3d1	hubená
postavě	postava	k1gFnSc3	postava
a	a	k8xC	a
nošení	nošení	k1gNnSc3	nošení
brýlí	brýle	k1gFnPc2	brýle
a	a	k8xC	a
rovnátek	rovnátko	k1gNnPc2	rovnátko
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
obtížné	obtížný	k2eAgNnSc1d1	obtížné
navázat	navázat	k5eAaPmF	navázat
emocionální	emocionální	k2eAgInSc4d1	emocionální
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
okolními	okolní	k2eAgMnPc7d1	okolní
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
k	k	k7c3	k
sebepoškozování	sebepoškozování	k1gNnSc3	sebepoškozování
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sbírala	sbírat	k5eAaImAgFnS	sbírat
jsem	být	k5eAaImIp1nS	být
nože	nůž	k1gInPc4	nůž
a	a	k8xC	a
vždycky	vždycky	k6eAd1	vždycky
jsem	být	k5eAaImIp1nS	být
měla	mít	k5eAaImAgFnS	mít
okolo	okolo	k6eAd1	okolo
sebe	sebe	k3xPyFc4	sebe
jisté	jistý	k2eAgFnPc1d1	jistá
věci	věc	k1gFnPc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Rituál	rituál	k1gInSc1	rituál
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
řízla	říznout	k5eAaPmAgFnS	říznout
a	a	k8xC	a
cítila	cítit	k5eAaImAgFnS	cítit
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
mi	já	k3xPp1nSc3	já
dával	dávat	k5eAaImAgMnS	dávat
pocit	pocit	k1gInSc4	pocit
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
přinášel	přinášet	k5eAaImAgInS	přinášet
jakési	jakýsi	k3yIgNnSc4	jakýsi
uvolnění	uvolnění	k1gNnSc4	uvolnění
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
léčivý	léčivý	k2eAgInSc1d1	léčivý
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
;	;	kIx,	;
ve	v	k7c6	v
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
měla	mít	k5eAaImAgFnS	mít
již	již	k6eAd1	již
za	za	k7c7	za
sebou	se	k3xPyFc7	se
zkušenost	zkušenost	k1gFnSc4	zkušenost
s	s	k7c7	s
"	"	kIx"	"
<g/>
téměř	téměř	k6eAd1	téměř
každou	každý	k3xTgFnSc7	každý
dostupnou	dostupný	k2eAgFnSc7d1	dostupná
drogou	droga	k1gFnSc7	droga
<g/>
"	"	kIx"	"
včetně	včetně	k7c2	včetně
heroinu	heroin	k1gInSc2	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
měla	mít	k5eAaImAgFnS	mít
vždy	vždy	k6eAd1	vždy
problematický	problematický	k2eAgInSc4d1	problematický
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
Voightově	Voightův	k2eAgFnSc3d1	Voightův
nevěře	nevěra	k1gFnSc3	nevěra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
rozpadu	rozpad	k1gInSc2	rozpad
manželství	manželství	k1gNnSc2	manželství
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
otci	otec	k1gMnSc6	otec
odcizila	odcizit	k5eAaPmAgFnS	odcizit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
vztahů	vztah	k1gInPc2	vztah
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
si	se	k3xPyFc3	se
zahráli	zahrát	k5eAaPmAgMnP	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Lara	Lara	k1gMnSc1	Lara
Croft	Croft	k1gMnSc1	Croft
–	–	k?	–
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
opět	opět	k6eAd1	opět
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
požádala	požádat	k5eAaPmAgFnS	požádat
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
používala	používat	k5eAaImAgFnS	používat
střední	střední	k2eAgNnSc4d1	střední
jméno	jméno	k1gNnSc4	jméno
Jolie	Jolie	k1gFnSc2	Jolie
<g/>
,	,	kIx,	,
k	k	k7c3	k
ustavení	ustavení	k1gNnSc3	ustavení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
identity	identita	k1gFnSc2	identita
jako	jako	k8xS	jako
herečky	herečka	k1gFnSc2	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
oficiálně	oficiálně	k6eAd1	oficiálně
požádala	požádat	k5eAaPmAgFnS	požádat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
jména	jméno	k1gNnSc2	jméno
odstranit	odstranit	k5eAaPmF	odstranit
příjmení	příjmení	k1gNnSc4	příjmení
Voight	Voighta	k1gFnPc2	Voighta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
Voight	Voight	k1gMnSc1	Voight
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Access	Access	k1gInSc1	Access
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
má	mít	k5eAaImIp3nS	mít
"	"	kIx"	"
<g/>
vážné	vážný	k2eAgInPc4d1	vážný
mentální	mentální	k2eAgInPc4d1	mentální
problémy	problém	k1gInPc4	problém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dcera	dcera	k1gFnSc1	dcera
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nepřeje	přát	k5eNaImIp3nS	přát
žádný	žádný	k3yNgInSc4	žádný
další	další	k2eAgInSc4d1	další
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
adopce	adopce	k1gFnSc2	adopce
syna	syn	k1gMnSc2	syn
Maddoxe	Maddoxe	k1gFnSc2	Maddoxe
si	se	k3xPyFc3	se
nemyslí	myslet	k5eNaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
bylo	být	k5eAaImAgNnS	být
zdravé	zdravý	k2eAgNnSc1d1	zdravé
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
stycích	styk	k1gInPc6	styk
s	s	k7c7	s
Voightem	Voighto	k1gNnSc7	Voighto
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
smrti	smrt	k1gFnSc2	smrt
její	její	k3xOp3gFnSc2	její
milované	milovaný	k2eAgFnSc2d1	milovaná
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
Angelinou	Angelin	k2eAgFnSc7d1	Angelina
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
k	k	k7c3	k
alespoň	alespoň	k9	alespoň
dočasnému	dočasný	k2eAgNnSc3d1	dočasné
usmíření	usmíření	k1gNnSc3	usmíření
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
malou	malý	k2eAgFnSc4d1	malá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Lookin	Lookina	k1gFnPc2	Lookina
<g/>
'	'	kIx"	'
to	ten	k3xDgNnSc1	ten
Get	Get	k1gFnSc3	Get
Out	Out	k1gFnSc2	Out
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
hrál	hrát	k5eAaImAgMnS	hrát
její	její	k3xOp3gNnPc4	její
otec	otec	k1gMnSc1	otec
Jon	Jon	k1gMnSc1	Jon
Voight	Voight	k1gMnSc1	Voight
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
spoluautorem	spoluautor	k1gMnSc7	spoluautor
scénáře	scénář	k1gInSc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
problémy	problém	k1gInPc4	problém
u	u	k7c2	u
kamerových	kamerový	k2eAgFnPc2d1	kamerová
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
vytýkáno	vytýkán	k2eAgNnSc1d1	vytýkáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
temná	temnat	k5eAaImIp3nS	temnat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
si	se	k3xPyFc3	se
v	v	k7c6	v
pěti	pět	k4xCc6	pět
studentských	studentský	k2eAgNnPc6d1	studentské
filmech	film	k1gInPc6	film
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
natáčel	natáčet	k5eAaImAgMnS	natáčet
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
USC	USC	kA	USC
School	School	k1gInSc1	School
of	of	k?	of
Cinema-Television	Cinema-Television	k1gInSc1	Cinema-Television
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
rovněž	rovněž	k9	rovněž
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
hudebních	hudební	k2eAgInPc6d1	hudební
klipech	klip	k1gInPc6	klip
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Stand	Standa	k1gFnPc2	Standa
by	by	kYmCp3nS	by
My	my	k3xPp1nPc1	my
Woman	Womana	k1gFnPc2	Womana
<g/>
"	"	kIx"	"
od	od	k7c2	od
Lennyho	Lennyho	k?	Lennyho
Kravitze	Kravitze	k1gFnSc2	Kravitze
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Alta	Alt	k2eAgFnSc1d1	Alta
Marea	Marea	k1gFnSc1	Marea
<g/>
"	"	kIx"	"
od	od	k7c2	od
Antonella	Antonell	k1gMnSc2	Antonell
Vendittiho	Venditti	k1gMnSc2	Venditti
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
About	About	k1gInSc1	About
Time	Tim	k1gInPc1	Tim
<g/>
"	"	kIx"	"
od	od	k7c2	od
The	The	k1gFnSc2	The
Lemonheads	Lemonheads	k1gInSc1	Lemonheads
či	či	k8xC	či
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Dreams	Dreamsa	k1gFnPc2	Dreamsa
Come	Com	k1gInSc2	Com
Through	Througha	k1gFnPc2	Througha
<g/>
"	"	kIx"	"
od	od	k7c2	od
Meat	Meatum	k1gNnPc2	Meatum
Loafa	Loaf	k1gMnSc2	Loaf
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgFnSc4d1	profesionální
filmovou	filmový	k2eAgFnSc4d1	filmová
kariéru	kariéra	k1gFnSc4	kariéra
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
když	když	k8xS	když
získala	získat	k5eAaPmAgFnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
roli	role	k1gFnSc4	role
v	v	k7c6	v
nízkorozpočtovém	nízkorozpočtový	k2eAgNnSc6d1	nízkorozpočtové
sci-fi	scii	k1gNnSc6	sci-fi
snímku	snímek	k1gInSc2	snímek
Cyborg	Cyborg	k1gInSc1	Cyborg
2	[number]	k4	2
–	–	k?	–
Skleněný	skleněný	k2eAgInSc4d1	skleněný
stín	stín	k1gInSc4	stín
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
filmová	filmový	k2eAgFnSc1d1	filmová
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
Casella	Casella	k1gFnSc1	Casella
"	"	kIx"	"
<g/>
Cash	cash	k1gFnSc1	cash
<g/>
"	"	kIx"	"
Reeseová	Reeseová	k1gFnSc1	Reeseová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
humanoidní	humanoidní	k2eAgInSc1d1	humanoidní
robot	robot	k1gInSc1	robot
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
i	i	k9	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
sex-appealu	sexppeal	k1gInSc2	sex-appeal
<g/>
)	)	kIx)	)
proniknout	proniknout	k5eAaPmF	proniknout
na	na	k7c4	na
ústředí	ústředí	k1gNnSc4	ústředí
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
korporace	korporace	k1gFnSc2	korporace
a	a	k8xC	a
tam	tam	k6eAd1	tam
explodovat	explodovat	k5eAaBmF	explodovat
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
byla	být	k5eAaImAgFnS	být
filmem	film	k1gInSc7	film
tak	tak	k6eAd1	tak
zklamaná	zklamaný	k2eAgFnSc1d1	zklamaná
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
přihlášením	přihlášení	k1gNnSc7	přihlášení
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
konkurz	konkurz	k1gInSc4	konkurz
otálela	otálet	k5eAaImAgFnS	otálet
skoro	skoro	k6eAd1	skoro
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
v	v	k7c6	v
nezávislém	závislý	k2eNgInSc6d1	nezávislý
filmu	film	k1gInSc6	film
Bez	bez	k7c2	bez
důkazů	důkaz	k1gInPc2	důkaz
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
filmu	film	k1gInSc6	film
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
síť	síť	k1gFnSc1	síť
roli	role	k1gFnSc4	role
hackerky	hackerka	k1gFnSc2	hackerka
Kate	kat	k1gInSc5	kat
"	"	kIx"	"
<g/>
Acid	Acid	k1gMnSc1	Acid
Burn	Burn	k1gMnSc1	Burn
<g/>
"	"	kIx"	"
Libbyové	Libbyové	k2eAgMnSc1d1	Libbyové
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc4	Times
napsaly	napsat	k5eAaPmAgFnP	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kate	kat	k1gMnSc5	kat
(	(	kIx(	(
<g/>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
<g/>
)	)	kIx)	)
vyniká	vynikat	k5eAaImIp3nS	vynikat
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tváří	tvářet	k5eAaImIp3nS	tvářet
ještě	ještě	k9	ještě
kyseleji	kysele	k6eAd2	kysele
<g/>
,	,	kIx,	,
než	než	k8xS	než
její	její	k3xOp3gMnPc1	její
herečtí	herecký	k2eAgMnPc1d1	herecký
kolegové	kolega	k1gMnPc1	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
upoutá	upoutat	k5eAaPmIp3nS	upoutat
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
vzácná	vzácný	k2eAgFnSc1d1	vzácná
žena-hackerka	ženaackerka	k1gFnSc1	žena-hackerka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sedí	sedit	k5eAaImIp3nS	sedit
u	u	k7c2	u
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
zírá	zírat	k5eAaImIp3nS	zírat
na	na	k7c4	na
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
její	její	k3xOp3gFnSc4	její
zasmušilou	zasmušilý	k2eAgFnSc4d1	zasmušilá
pózu	póza	k1gFnSc4	póza
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
co	co	k9	co
její	její	k3xOp3gFnSc1	její
role	role	k1gFnSc1	role
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
znát	znát	k5eAaImF	znát
sladce	sladko	k6eAd1	sladko
andělský	andělský	k2eAgInSc4d1	andělský
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
zdědila	zdědit	k5eAaPmAgFnS	zdědit
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
Jonu	Jon	k1gMnSc6	Jon
Voightovi	Voight	k1gMnSc6	Voight
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Snímek	snímek	k1gInSc1	snímek
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgInS	být
komerčně	komerčně	k6eAd1	komerčně
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
uvedení	uvedení	k1gNnSc6	uvedení
na	na	k7c6	na
videokazetách	videokazeta	k1gFnPc6	videokazeta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kultovním	kultovní	k2eAgInSc7d1	kultovní
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
si	se	k3xPyFc3	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
roli	role	k1gFnSc4	role
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Utajená	utajený	k2eAgFnSc1d1	utajená
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
volné	volný	k2eAgInPc1d1	volný
moderní	moderní	k2eAgFnSc4d1	moderní
adaptaci	adaptace	k1gFnSc4	adaptace
Romea	Romeo	k1gMnSc2	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc2	Julie
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgFnSc2d1	umístěná
do	do	k7c2	do
prostředí	prostředí	k1gNnSc6	prostředí
dvou	dva	k4xCgFnPc2	dva
znepřátelených	znepřátelený	k2eAgFnPc2d1	znepřátelená
italských	italský	k2eAgFnPc2d1	italská
rodin	rodina	k1gFnPc2	rodina
vlastnících	vlastnící	k2eAgFnPc2d1	vlastnící
restaurace	restaurace	k1gFnPc4	restaurace
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Měsíc	měsíc	k1gInSc1	měsíc
nad	nad	k7c7	nad
pouští	poušť	k1gFnSc7	poušť
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
mladou	mladý	k2eAgFnSc4d1	mladá
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
staršího	starý	k2eAgMnSc2d2	starší
muže	muž	k1gMnSc2	muž
(	(	kIx(	(
<g/>
Danny	Dann	k1gInPc1	Dann
Aiello	Aiello	k1gNnSc1	Aiello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
on	on	k3xPp3gMnSc1	on
projevuje	projevovat	k5eAaImIp3nS	projevovat
city	city	k1gFnSc4	city
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
matce	matka	k1gFnSc3	matka
(	(	kIx(	(
<g/>
Anne	Anne	k1gFnSc1	Anne
Archerová	Archerová	k1gFnSc1	Archerová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
si	se	k3xPyFc3	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Foxfire	Foxfir	k1gInSc5	Foxfir
Margret	Margret	k1gInSc4	Margret
"	"	kIx"	"
<g/>
Legs	Legs	k1gInSc4	Legs
<g/>
"	"	kIx"	"
Sadovskou	Sadovský	k2eAgFnSc4d1	Sadovský
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dospívajících	dospívající	k2eAgFnPc2d1	dospívající
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spojí	spojit	k5eAaPmIp3nP	spojit
neobvyklé	obvyklý	k2eNgNnSc4d1	neobvyklé
pouto	pouto	k1gNnSc4	pouto
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
zbijí	zbít	k5eAaPmIp3nP	zbít
učitele	učitel	k1gMnSc4	učitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
sexuálně	sexuálně	k6eAd1	sexuálně
obtěžoval	obtěžovat	k5eAaImAgMnS	obtěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
Times	Times	k1gInSc4	Times
napsaly	napsat	k5eAaPmAgFnP	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dosti	dosti	k6eAd1	dosti
obehrané	obehraný	k2eAgNnSc1d1	obehrané
téma	téma	k1gNnSc1	téma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jolie	Jolie	k1gFnSc1	Jolie
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Johna	John	k1gMnSc2	John
Voighta	Voight	k1gMnSc2	Voight
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vymyká	vymykat	k5eAaImIp3nS	vymykat
stereotypu	stereotyp	k1gInSc2	stereotyp
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Maddy	Madda	k1gFnSc2	Madda
<g/>
,	,	kIx,	,
Legs	Legs	k1gInSc1	Legs
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
prvkem	prvek	k1gInSc7	prvek
a	a	k8xC	a
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Davida	David	k1gMnSc2	David
Duchovnyho	Duchovnyho	k?	Duchovnyho
v	v	k7c6	v
thrilleru	thriller	k1gInSc6	thriller
Ruce	ruka	k1gFnPc1	ruka
od	od	k7c2	od
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
losangeleském	losangeleský	k2eAgNnSc6d1	losangeleské
podsvětí	podsvětí	k1gNnSc6	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
film	film	k1gInSc4	film
propadl	propadnout	k5eAaPmAgInS	propadnout
<g/>
;	;	kIx,	;
Roger	Roger	k1gInSc1	Roger
Ebert	Eberta	k1gFnPc2	Eberta
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
dává	dávat	k5eAaImIp3nS	dávat
jisté	jistý	k2eAgNnSc4d1	jisté
teplo	teplo	k1gNnSc4	teplo
do	do	k7c2	do
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
a	a	k8xC	a
agresivní	agresivní	k2eAgFnSc1d1	agresivní
<g/>
;	;	kIx,	;
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
měkká	měkký	k2eAgFnSc1d1	měkká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
Blossomovou	Blossomový	k2eAgFnSc7d1	Blossomový
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
možná	možná	k9	možná
skutečně	skutečně	k6eAd1	skutečně
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Následně	následně	k6eAd1	následně
Angelina	Angelin	k2eAgFnSc1d1	Angelina
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
TV	TV	kA	TV
filmu	film	k1gInSc3	film
Pravé	pravý	k2eAgFnSc2d1	pravá
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
historickém	historický	k2eAgNnSc6d1	historické
romantickém	romantický	k2eAgNnSc6d1	romantické
dramatu	drama	k1gNnSc6	drama
z	z	k7c2	z
Divokého	divoký	k2eAgInSc2d1	divoký
Západu	západ	k1gInSc2	západ
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Janice	Janice	k1gFnPc1	Janice
Woods	Woods	k1gInSc4	Woods
Windleové	Windleová	k1gFnSc2	Windleová
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
jako	jako	k9	jako
striptérka	striptérka	k1gFnSc1	striptérka
v	v	k7c6	v
hudebním	hudební	k2eAgNnSc6d1	hudební
videu	video	k1gNnSc6	video
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Anybody	Anybod	k1gInPc4	Anybod
Seen	Seena	k1gFnPc2	Seena
My	my	k3xPp1nPc1	my
Baby	baby	k1gNnSc3	baby
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
od	od	k7c2	od
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
strmě	strmě	k6eAd1	strmě
stoupat	stoupat	k5eAaImF	stoupat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
získala	získat	k5eAaPmAgFnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
roli	role	k1gFnSc4	role
v	v	k7c6	v
TV	TV	kA	TV
filmu	film	k1gInSc2	film
George	Georg	k1gInSc2	Georg
Wallace	Wallace	k1gFnSc2	Wallace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
zde	zde	k6eAd1	zde
Cornelii	Cornelie	k1gFnSc4	Cornelie
Wallaceovou	Wallaceův	k2eAgFnSc7d1	Wallaceova
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
manželku	manželka	k1gFnSc4	manželka
alabamského	alabamský	k2eAgMnSc2d1	alabamský
guvernéra	guvernér	k1gMnSc2	guvernér
George	Georg	k1gMnSc2	Georg
Wallace	Wallace	k1gFnSc2	Wallace
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Gary	Gar	k2eAgFnPc4d1	Gara
Sinise	Sinise	k1gFnPc4	Sinise
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
velmi	velmi	k6eAd1	velmi
dobrá	dobrá	k9	dobrá
hodnocení	hodnocení	k1gNnSc1	hodnocení
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
cen	cena	k1gFnPc2	cena
i	i	k9	i
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
minisérie	minisérie	k1gFnSc1	minisérie
či	či	k8xC	či
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
byla	být	k5eAaImAgFnS	být
roli	role	k1gFnSc4	role
rovněž	rovněž	k9	rovněž
nominována	nominován	k2eAgFnSc1d1	nominována
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
životopisném	životopisný	k2eAgInSc6d1	životopisný
filmu	film	k1gInSc6	film
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
HBO	HBO	kA	HBO
Gia	Gia	k1gMnSc2	Gia
<g/>
.	.	kIx.	.
</s>
<s>
Ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
zde	zde	k6eAd1	zde
supermodelku	supermodelka	k1gFnSc4	supermodelka
Giu	Giu	k1gFnSc2	Giu
Carangi	Carang	k1gFnSc2	Carang
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
dokumentoval	dokumentovat	k5eAaBmAgInS	dokumentovat
kariéru	kariéra	k1gFnSc4	kariéra
slavné	slavný	k2eAgFnSc2d1	slavná
modelky	modelka	k1gFnSc2	modelka
a	a	k8xC	a
především	především	k9	především
její	její	k3xOp3gInSc1	její
pád	pád	k1gInSc1	pád
zaviněný	zaviněný	k2eAgInSc1d1	zaviněný
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
heroinu	heroin	k1gInSc6	heroin
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
smrt	smrt	k1gFnSc4	smrt
na	na	k7c4	na
AIDS	aids	k1gInSc4	aids
uprostřed	uprostřed	k7c2	uprostřed
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vanessa	Vanessa	k1gFnSc1	Vanessa
Vance	Vance	k1gFnSc1	Vance
z	z	k7c2	z
Reel	Reela	k1gFnPc2	Reela
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
získala	získat	k5eAaPmAgFnS	získat
široké	široký	k2eAgNnSc4d1	široké
uznání	uznání	k1gNnSc4	uznání
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
roli	role	k1gFnSc6	role
Gii	Gii	k1gFnSc2	Gii
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
pochopit	pochopit	k5eAaPmF	pochopit
proč	proč	k6eAd1	proč
<g/>
.	.	kIx.	.
</s>
<s>
Jolie	Jolie	k1gFnSc1	Jolie
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
roli	role	k1gFnSc6	role
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
,	,	kIx,	,
nespoutaná	spoutaný	k2eNgFnSc1d1	nespoutaná
-	-	kIx~	-
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
plátno	plátno	k1gNnSc1	plátno
drzostí	drzost	k1gFnSc7	drzost
<g/>
,	,	kIx,	,
šarmem	šarm	k1gInSc7	šarm
a	a	k8xC	a
zoufalstvím	zoufalství	k1gNnSc7	zoufalství
-	-	kIx~	-
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
postava	postava	k1gFnSc1	postava
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejkrásnější	krásný	k2eAgFnSc7d3	nejkrásnější
lidskou	lidský	k2eAgFnSc7d1	lidská
troskou	troska	k1gFnSc7	troska
filmové	filmový	k2eAgFnSc2d1	filmová
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guild	k1gMnSc1	Guild
Award	Award	k1gMnSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
konceptem	koncept	k1gInSc7	koncept
metodického	metodický	k2eAgNnSc2d1	metodické
herectví	herectví	k1gNnSc2	herectví
Lee	Lea	k1gFnSc6	Lea
Strasberga	Strasberg	k1gMnSc2	Strasberg
během	během	k7c2	během
většiny	většina	k1gFnSc2	většina
svých	svůj	k3xOyFgInPc2	svůj
raných	raný	k2eAgInPc2d1	raný
filmů	film	k1gInPc2	film
obvykle	obvykle	k6eAd1	obvykle
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
v	v	k7c6	v
roli	role	k1gFnSc6	role
i	i	k9	i
mezi	mezi	k7c7	mezi
natáčecími	natáčecí	k2eAgFnPc7d1	natáčecí
scénami	scéna	k1gFnPc7	scéna
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
získala	získat	k5eAaPmAgFnS	získat
reputaci	reputace	k1gFnSc4	reputace
herečky	herečka	k1gFnSc2	herečka
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
Gii	Gii	k1gFnSc1	Gii
řekla	říct	k5eAaPmAgFnS	říct
svému	svůj	k3xOyFgMnSc3	svůj
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
manželovi	manžel	k1gMnSc3	manžel
Jonnymu	Jonnym	k1gInSc2	Jonnym
Lee	Lea	k1gFnSc3	Lea
Millerovi	Millerův	k2eAgMnPc1d1	Millerův
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nebude	být	k5eNaImBp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zavolat	zavolat	k5eAaPmF	zavolat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Řeknu	říct	k5eAaPmIp1nS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Jsem	být	k5eAaImIp1nS	být
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Umírám	umírat	k5eAaImIp1nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
lesba	lesba	k1gFnSc1	lesba
<g/>
.	.	kIx.	.
</s>
<s>
Neuvidím	uvidět	k5eNaPmIp1nS	uvidět
tě	ty	k3xPp2nSc4	ty
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
tell	tell	k1gInSc1	tell
him	him	k?	him
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
alone	alonout	k5eAaPmIp3nS	alonout
<g/>
;	;	kIx,	;
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
dying	dying	k1gMnSc1	dying
<g/>
;	;	kIx,	;
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
gay	gay	k1gMnSc1	gay
<g/>
;	;	kIx,	;
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
not	nota	k1gFnPc2	nota
going	going	k1gMnSc1	going
to	ten	k3xDgNnSc4	ten
see	see	k?	see
you	you	k?	you
for	forum	k1gNnPc2	forum
weeks	weeks	k1gInSc1	weeks
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
natáčení	natáčení	k1gNnSc2	natáčení
Gii	Gii	k1gFnSc2	Gii
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
končí	končit	k5eAaImIp3nS	končit
s	s	k7c7	s
herectvím	herectví	k1gNnSc7	herectví
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
filmu	film	k1gInSc3	film
co	co	k3yInSc4	co
dát	dát	k5eAaPmF	dát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rozešla	rozejít	k5eAaPmAgFnS	rozejít
se	se	k3xPyFc4	se
s	s	k7c7	s
Millerem	Miller	k1gMnSc7	Miller
a	a	k8xC	a
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc4	York
University	universita	k1gFnSc2	universita
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
filmové	filmový	k2eAgFnSc2d1	filmová
režie	režie	k1gFnSc2	režie
a	a	k8xC	a
na	na	k7c4	na
kurzy	kurz	k1gInPc4	kurz
scenáristiky	scenáristika	k1gFnSc2	scenáristika
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
popsala	popsat	k5eAaPmAgFnS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dobré	dobrá	k1gFnSc2	dobrá
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zisku	zisk	k1gInSc6	zisk
Zlatého	zlatý	k2eAgInSc2d1	zlatý
glóbu	glóbus	k1gInSc2	glóbus
za	za	k7c4	za
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
George	Georg	k1gMnSc2	Georg
Wallace	Wallace	k1gFnSc2	Wallace
a	a	k8xC	a
příznivých	příznivý	k2eAgFnPc6d1	příznivá
kritikách	kritika	k1gFnPc6	kritika
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Gia	Gia	k1gMnPc2	Gia
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
herečky	herečka	k1gFnSc2	herečka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filmu	film	k1gInSc3	film
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
gangsterkou	gangsterka	k1gFnSc7	gangsterka
Zločin	zločin	k1gInSc1	zločin
a	a	k8xC	a
trest	trest	k1gInSc1	trest
v	v	k7c6	v
NYC	NYC	kA	NYC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Podoby	podoba	k1gFnSc2	podoba
lásky	láska	k1gFnSc2	láska
po	po	k7c6	po
boku	bok	k1gInSc6	bok
hvězd	hvězda	k1gFnPc2	hvězda
jako	jako	k8xC	jako
Sean	Seana	k1gFnPc2	Seana
Connery	Conner	k1gMnPc4	Conner
<g/>
,	,	kIx,	,
Gillian	Gillian	k1gMnSc1	Gillian
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
Ryan	Ryan	k1gMnSc1	Ryan
Phillippe	Phillipp	k1gMnSc5	Phillipp
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jon	Jon	k1gMnSc1	Jon
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
většinou	většina	k1gFnSc7	většina
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
zvlášť	zvlášť	k6eAd1	zvlášť
byla	být	k5eAaImAgFnS	být
vychvalována	vychvalován	k2eAgFnSc1d1	vychvalována
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Francisco	Francisco	k1gMnSc1	Francisco
Chronicle	Chronicle	k1gNnSc2	Chronicle
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jolie	Jolie	k1gFnSc1	Jolie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
role	role	k1gFnSc1	role
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
scénáři	scénář	k1gInSc6	scénář
přepisována	přepisován	k2eAgFnSc1d1	přepisována
<g/>
,	,	kIx,	,
září	zářit	k5eAaImIp3nS	zářit
jako	jako	k9	jako
zoufalá	zoufalý	k2eAgFnSc1d1	zoufalá
žena	žena	k1gFnSc1	žena
vymetající	vymetající	k2eAgInPc1d1	vymetající
noční	noční	k2eAgInPc1d1	noční
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
vše	všechen	k3xTgNnSc4	všechen
je	být	k5eAaImIp3nS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
riskovat	riskovat	k5eAaBmF	riskovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
získala	získat	k5eAaPmAgFnS	získat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
cenu	cena	k1gFnSc4	cena
Breakthrough	Breakthrougha	k1gFnPc2	Breakthrougha
Performance	performance	k1gFnSc2	performance
Award	Awarda	k1gFnPc2	Awarda
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
National	National	k1gMnSc1	National
Board	Board	k1gMnSc1	Board
of	of	k?	of
Review	Review	k1gMnSc1	Review
of	of	k?	of
Motion	Motion	k1gInSc1	Motion
Pictures	Pictures	k1gInSc1	Pictures
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
komediálním	komediální	k2eAgNnSc6d1	komediální
dramatu	drama	k1gNnSc6	drama
Bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
Runway	runway	k1gFnSc1	runway
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Cusackem	Cusack	k1gInSc7	Cusack
<g/>
,	,	kIx,	,
Billy	Bill	k1gMnPc7	Bill
Bobem	bob	k1gInSc7	bob
Thorntonem	Thornton	k1gInSc7	Thornton
a	a	k8xC	a
Jonem	Jon	k1gMnSc7	Jon
Stewartem	Stewart	k1gMnSc7	Stewart
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
smíšené	smíšený	k2eAgInPc4d1	smíšený
ohlasy	ohlas	k1gInPc4	ohlas
a	a	k8xC	a
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
byla	být	k5eAaImAgFnS	být
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgNnPc1	sám
<g/>
,	,	kIx,	,
za	za	k7c4	za
roli	role	k1gFnSc4	role
Thorntonovy	Thorntonův	k2eAgFnSc2d1	Thorntonova
svůdné	svůdný	k2eAgFnSc2d1	svůdná
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mary	Mary	k1gFnSc1	Mary
(	(	kIx(	(
<g/>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
absurdní	absurdní	k2eAgInSc1d1	absurdní
výtvor	výtvor	k1gInSc1	výtvor
scenáristy	scenárista	k1gMnSc2	scenárista
<g/>
,	,	kIx,	,
svobodomyslná	svobodomyslný	k2eAgFnSc1d1	svobodomyslná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pláče	plakat	k5eAaImIp3nS	plakat
nad	nad	k7c7	nad
usychajícími	usychající	k2eAgInPc7d1	usychající
ibišky	ibišek	k1gInPc7	ibišek
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
tyrkysové	tyrkysový	k2eAgInPc4d1	tyrkysový
prsteny	prsten	k1gInPc4	prsten
a	a	k8xC	a
pláče	pláč	k1gInPc4	pláč
<g/>
,	,	kIx,	,
když	když	k8xS	když
Russell	Russell	k1gMnSc1	Russell
(	(	kIx(	(
<g/>
Thornton	Thornton	k1gInSc1	Thornton
<g/>
)	)	kIx)	)
tráví	trávit	k5eAaImIp3nP	trávit
celé	celý	k2eAgFnPc4d1	celá
noci	noc	k1gFnPc4	noc
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Denzela	Denzela	k1gMnSc4	Denzela
Washingtona	Washington	k1gMnSc4	Washington
v	v	k7c6	v
thrilleru	thriller	k1gInSc6	thriller
Sběratel	sběratel	k1gMnSc1	sběratel
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
natočeném	natočený	k2eAgInSc6d1	natočený
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
detektivního	detektivní	k2eAgInSc2d1	detektivní
románu	román	k1gInSc2	román
Jefferyho	Jeffery	k1gMnSc4	Jeffery
Deavera	Deaver	k1gMnSc4	Deaver
<g/>
.	.	kIx.	.
</s>
<s>
Jolie	Jolie	k1gFnSc1	Jolie
hrála	hrát	k5eAaImAgFnS	hrát
psychicky	psychicky	k6eAd1	psychicky
nevyrovnanou	vyrovnaný	k2eNgFnSc4d1	nevyrovnaná
policistku	policistka	k1gFnSc4	policistka
pomáhající	pomáhající	k2eAgFnSc2d1	pomáhající
ochrnutému	ochrnutý	k2eAgMnSc3d1	ochrnutý
kriminalistovi	kriminalista	k1gMnSc3	kriminalista
(	(	kIx(	(
<g/>
Washington	Washington	k1gInSc1	Washington
<g/>
)	)	kIx)	)
dopadnout	dopadnout	k5eAaPmF	dopadnout
sériového	sériový	k2eAgMnSc4d1	sériový
vraha	vrah	k1gMnSc4	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
celosvětově	celosvětově	k6eAd1	celosvětově
151	[number]	k4	151
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
Free	Free	k1gInSc1	Free
Press	Press	k1gInSc4	Press
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jolie	Jolie	k1gFnSc1	Jolie
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
jako	jako	k9	jako
vždy	vždy	k6eAd1	vždy
krásná	krásný	k2eAgFnSc1d1	krásná
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
roli	role	k1gFnSc4	role
se	se	k3xPyFc4	se
ale	ale	k9	ale
prostě	prostě	k6eAd1	prostě
a	a	k8xC	a
očividně	očividně	k6eAd1	očividně
nehodí	hodit	k5eNaPmIp3nS	hodit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Angelina	Angelin	k2eAgFnSc1d1	Angelina
dále	daleko	k6eAd2	daleko
přijala	přijmout	k5eAaPmAgFnS	přijmout
roli	role	k1gFnSc4	role
sociopatické	sociopatický	k2eAgFnSc2d1	sociopatická
dívky	dívka	k1gFnSc2	dívka
Lisy	lis	k1gInPc1	lis
Roweové	Roweová	k1gFnSc2	Roweová
léčící	léčící	k2eAgInPc1d1	léčící
se	se	k3xPyFc4	se
v	v	k7c6	v
psychiatrické	psychiatrický	k2eAgFnSc6d1	psychiatrická
léčebně	léčebna	k1gFnSc6	léčebna
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Narušení	narušení	k1gNnSc2	narušení
<g/>
,	,	kIx,	,
adaptaci	adaptace	k1gFnSc4	adaptace
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
knihy	kniha	k1gFnSc2	kniha
Susanny	Susanna	k1gFnSc2	Susanna
Kaysenové	Kaysenová	k1gFnSc2	Kaysenová
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Winona	Winona	k1gFnSc1	Winona
Ryderová	Ryderová	k1gFnSc1	Ryderová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
doufala	doufat	k5eAaImAgFnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
bude	být	k5eAaImBp3nS	být
velkým	velký	k2eAgInSc7d1	velký
comebackem	comeback	k1gInSc7	comeback
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
film	film	k1gInSc1	film
znamenal	znamenat	k5eAaImAgInS	znamenat
zásadní	zásadní	k2eAgInSc1d1	zásadní
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
ji	on	k3xPp3gFnSc4	on
povznesl	povznést	k5eAaPmAgInS	povznést
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
hollywoodské	hollywoodský	k2eAgFnPc4d1	hollywoodská
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
získala	získat	k5eAaPmAgFnS	získat
třetí	třetí	k4xOgInSc4	třetí
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
cenu	cena	k1gFnSc4	cena
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guild	k1gMnSc1	Guild
Award	Award	k1gMnSc1	Award
a	a	k8xC	a
především	především	k9	především
Cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
)	)	kIx)	)
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Variety	varieta	k1gFnSc2	varieta
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jolie	Jolie	k1gFnSc1	Jolie
exceluje	excelovat	k5eAaImIp3nS	excelovat
jako	jako	k9	jako
ohnivá	ohnivý	k2eAgFnSc1d1	ohnivá
<g/>
,	,	kIx,	,
nezodpovědná	zodpovědný	k2eNgFnSc1d1	nezodpovědná
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
Susan	Susany	k1gInPc2	Susany
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
daleko	daleko	k6eAd1	daleko
přínosnější	přínosný	k2eAgFnSc1d2	přínosnější
než	než	k8xS	než
metody	metoda	k1gFnPc1	metoda
doktorů	doktor	k1gMnPc2	doktor
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
letním	letní	k2eAgInSc6d1	letní
trháku	trhák	k1gInSc6	trhák
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
zde	zde	k6eAd1	zde
Sarah	Sarah	k1gFnSc1	Sarah
"	"	kIx"	"
<g/>
Sway	Swaa	k1gFnSc2	Swaa
<g/>
"	"	kIx"	"
Waylendovou	Waylendový	k2eAgFnSc4d1	Waylendový
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc4d1	bývalá
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
zloděje	zloděj	k1gMnSc2	zloděj
aut	auto	k1gNnPc2	auto
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Cage	Cage	k1gNnSc6	Cage
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
to	ten	k3xDgNnSc4	ten
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
The	The	k1gFnSc1	The
Washington	Washington	k1gInSc1	Washington
Post	post	k1gInSc1	post
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Všechno	všechen	k3xTgNnSc1	všechen
co	co	k9	co
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
postávání	postávání	k1gNnSc1	postávání
<g/>
,	,	kIx,	,
chladné	chladný	k2eAgInPc1d1	chladný
pohledy	pohled	k1gInPc1	pohled
a	a	k8xC	a
předvádění	předvádění	k1gNnSc1	předvádění
masitých	masitý	k2eAgInPc2d1	masitý
polštářků	polštářek	k1gInPc2	polštářek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tak	tak	k6eAd1	tak
provokativně	provokativně	k6eAd1	provokativně
vroubí	vroubit	k5eAaImIp3nS	vroubit
její	její	k3xOp3gNnPc4	její
ústa	ústa	k1gNnPc4	ústa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Později	pozdě	k6eAd2	pozdě
Angelina	Angelin	k2eAgFnSc1d1	Angelina
objasnila	objasnit	k5eAaPmAgFnS	objasnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
role	role	k1gFnSc1	role
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byla	být	k5eAaImAgFnS	být
vítanou	vítaný	k2eAgFnSc7d1	vítaná
úlevou	úleva	k1gFnSc7	úleva
po	po	k7c6	po
emocionálně	emocionálně	k6eAd1	emocionálně
náročné	náročný	k2eAgFnSc6d1	náročná
roli	role	k1gFnSc6	role
Lisy	Lisa	k1gFnSc2	Lisa
Roweové	Roweové	k2eAgFnSc2d1	Roweové
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Narušení	narušení	k1gNnSc2	narušení
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gInSc7	její
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
když	když	k8xS	když
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
tržby	tržba	k1gFnSc2	tržba
čítaly	čítat	k5eAaImAgInP	čítat
237	[number]	k4	237
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
její	její	k3xOp3gFnPc1	její
herecké	herecký	k2eAgFnPc1d1	herecká
schopnosti	schopnost	k1gFnPc1	schopnost
byly	být	k5eAaImAgFnP	být
vysoce	vysoce	k6eAd1	vysoce
oceňovány	oceňovat	k5eAaImNgInP	oceňovat
kritikou	kritika	k1gFnSc7	kritika
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
filmy	film	k1gInPc1	film
zatím	zatím	k6eAd1	zatím
nepoutaly	poutat	k5eNaImAgInP	poutat
zájem	zájem	k1gInSc4	zájem
široké	široký	k2eAgFnSc2d1	široká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
film	film	k1gInSc1	film
Lara	Lara	k1gFnSc1	Lara
Croft	Croft	k1gMnSc1	Croft
-	-	kIx~	-
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
z	z	k7c2	z
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
udělal	udělat	k5eAaPmAgMnS	udělat
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
hvězdu	hvězda	k1gFnSc4	hvězda
první	první	k4xOgFnSc2	první
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
archeoložky	archeoložka	k1gFnSc2	archeoložka
Lary	Lara	k1gMnSc2	Lara
Croft	Crofta	k1gFnPc2	Crofta
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
populární	populární	k2eAgFnSc2d1	populární
herní	herní	k2eAgFnSc2d1	herní
série	série	k1gFnSc2	série
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
si	se	k3xPyFc3	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
musela	muset	k5eAaImAgFnS	muset
osvojit	osvojit	k5eAaPmF	osvojit
anglický	anglický	k2eAgInSc4d1	anglický
přízvuk	přízvuk	k1gInSc4	přízvuk
a	a	k8xC	a
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
náročný	náročný	k2eAgInSc4d1	náročný
výcvik	výcvik	k1gInSc4	výcvik
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
výkon	výkon	k1gInSc1	výkon
ve	v	k7c6	v
filmu	film	k1gInSc6	film
byl	být	k5eAaImAgInS	být
chválen	chválen	k2eAgInSc1d1	chválen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
film	film	k1gInSc1	film
samotný	samotný	k2eAgInSc1d1	samotný
byl	být	k5eAaImAgInS	být
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Slant	Slanta	k1gFnPc2	Slanta
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
Lary	Lara	k1gFnSc2	Lara
Croft	Croft	k1gInSc1	Croft
narodila	narodit	k5eAaPmAgFnS	narodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
<g/>
)	)	kIx)	)
Simon	Simon	k1gMnSc1	Simon
West	West	k1gMnSc1	West
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
primitivní	primitivní	k2eAgFnSc4d1	primitivní
arkádu	arkáda	k1gFnSc4	arkáda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
komerčně	komerčně	k6eAd1	komerčně
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
vydělal	vydělat	k5eAaPmAgInS	vydělat
275	[number]	k4	275
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
a	a	k8xC	a
Angelinu	Angelin	k2eAgFnSc4d1	Angelina
nasměroval	nasměrovat	k5eAaPmAgMnS	nasměrovat
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
hvězdy	hvězda	k1gFnSc2	hvězda
akčních	akční	k2eAgInPc2d1	akční
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sedmý	sedmý	k4xOgInSc4	sedmý
hřích	hřích	k1gInSc4	hřích
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
"	"	kIx"	"
<g/>
nevěstu	nevěsta	k1gFnSc4	nevěsta
na	na	k7c4	na
inzerát	inzerát	k1gInSc4	inzerát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skrývající	skrývající	k2eAgNnPc4d1	skrývající
temná	temný	k2eAgNnPc4d1	temné
tajemství	tajemství	k1gNnPc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Jejího	její	k3xOp3gMnSc4	její
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
bohatého	bohatý	k2eAgMnSc2d1	bohatý
kubánského	kubánský	k2eAgMnSc2d1	kubánský
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
Antonio	Antonio	k1gMnSc1	Antonio
Banderas	Banderas	k1gMnSc1	Banderas
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Cornella	Cornello	k1gNnSc2	Cornello
Woolriche	Woolrich	k1gFnSc2	Woolrich
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
zcela	zcela	k6eAd1	zcela
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
filmu	film	k1gInSc6	film
Život	život	k1gInSc4	život
nebo	nebo	k8xC	nebo
něco	něco	k3yInSc1	něco
takového	takový	k3xDgNnSc2	takový
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
Angelina	Angelin	k2eAgFnSc1d1	Angelina
roli	role	k1gFnSc3	role
ambiciózní	ambiciózní	k2eAgFnPc4d1	ambiciózní
televizní	televizní	k2eAgFnPc4d1	televizní
reportérky	reportérka	k1gFnPc4	reportérka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
předpovězeno	předpovězen	k2eAgNnSc1d1	předpovězeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
týden	týden	k1gInSc4	týden
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
většinou	většina	k1gFnSc7	většina
negativní	negativní	k2eAgFnSc2d1	negativní
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
výkon	výkon	k1gInSc4	výkon
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
byl	být	k5eAaImAgMnS	být
chválen	chválit	k5eAaImNgMnS	chválit
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Clinton	Clinton	k1gMnSc1	Clinton
na	na	k7c4	na
CNN	CNN	kA	CNN
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
<g/>
)	)	kIx)	)
Jolie	Jolie	k1gFnSc1	Jolie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
roli	role	k1gFnSc6	role
vynikající	vynikající	k2eAgMnSc1d1	vynikající
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
pár	pár	k4xCyI	pár
zádrhelů	zádrhel	k1gInPc2	zádrhel
v	v	k7c6	v
ději	děj	k1gInSc6	děj
uprostřed	uprostřed	k7c2	uprostřed
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
držitelka	držitelka	k1gFnSc1	držitelka
Ceny	cena	k1gFnSc2	cena
Akademie	akademie	k1gFnSc2	akademie
zcela	zcela	k6eAd1	zcela
přesvědčivá	přesvědčivý	k2eAgFnSc1d1	přesvědčivá
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
sebepoznání	sebepoznání	k1gNnSc3	sebepoznání
a	a	k8xC	a
pochopení	pochopení	k1gNnSc3	pochopení
pravého	pravý	k2eAgInSc2d1	pravý
smyslu	smysl	k1gInSc2	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roli	role	k1gFnSc4	role
archeoložky	archeoložka	k1gFnSc2	archeoložka
Lary	Lara	k1gFnSc2	Lara
Croft	Crofta	k1gFnPc2	Crofta
si	se	k3xPyFc3	se
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Lara	Lara	k1gMnSc1	Lara
Croft	Croft	k1gMnSc1	Croft
-	-	kIx~	-
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
<g/>
:	:	kIx,	:
Kolébka	kolébka	k1gFnSc1	kolébka
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
filmem	film	k1gInSc7	film
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
nejlépe	dobře	k6eAd3	dobře
placené	placený	k2eAgFnPc4d1	placená
hollywoodské	hollywoodský	k2eAgFnPc4d1	hollywoodská
herečky	herečka	k1gFnPc4	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
nebylo	být	k5eNaImAgNnS	být
tak	tak	k6eAd1	tak
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
jako	jako	k8xS	jako
původní	původní	k2eAgInSc1d1	původní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
však	však	k9	však
vydělalo	vydělat	k5eAaPmAgNnS	vydělat
solidních	solidní	k2eAgInPc2d1	solidní
156	[number]	k4	156
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
také	také	k9	také
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
v	v	k7c6	v
hudebním	hudební	k2eAgNnSc6d1	hudební
videu	video	k1gNnSc6	video
"	"	kIx"	"
<g/>
Did	Did	k1gMnSc1	Did
My	my	k3xPp1nPc1	my
Time	Tim	k1gFnPc1	Tim
<g/>
"	"	kIx"	"
skupiny	skupina	k1gFnPc1	skupina
Korn	Korno	k1gNnPc2	Korno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hranice	hranice	k1gFnSc2	hranice
zlomu	zlom	k1gInSc2	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
mladé	mladý	k2eAgFnSc2d1	mladá
ženy	žena	k1gFnSc2	žena
z	z	k7c2	z
vyšších	vysoký	k2eAgInPc2d2	vyšší
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
charitativní	charitativní	k2eAgFnPc4d1	charitativní
organizace	organizace	k1gFnPc4	organizace
v	v	k7c6	v
postižených	postižený	k2eAgFnPc6d1	postižená
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
odrážela	odrážet	k5eAaImAgFnS	odrážet
její	její	k3xOp3gInSc4	její
skutečný	skutečný	k2eAgInSc4d1	skutečný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
angažuje	angažovat	k5eAaBmIp3nS	angažovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
humanitárních	humanitární	k2eAgFnPc2d1	humanitární
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
ovšem	ovšem	k9	ovšem
vlažnou	vlažný	k2eAgFnSc4d1	vlažná
odezvu	odezva	k1gFnSc4	odezva
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
ani	ani	k8xC	ani
komerčně	komerčně	k6eAd1	komerčně
nebyl	být	k5eNaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Ethana	Ethan	k1gMnSc2	Ethan
Hawka	Hawek	k1gMnSc2	Hawek
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Zloděj	zloděj	k1gMnSc1	zloděj
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
zde	zde	k6eAd1	zde
agentku	agentka	k1gFnSc4	agentka
FBI	FBI	kA	FBI
na	na	k7c6	na
stopě	stopa	k1gFnSc6	stopa
sériového	sériový	k2eAgMnSc2d1	sériový
vraha	vrah	k1gMnSc2	vrah
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přebírá	přebírat	k5eAaImIp3nS	přebírat
identitu	identita	k1gFnSc4	identita
svých	svůj	k3xOyFgFnPc2	svůj
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Ohlasy	ohlas	k1gInPc1	ohlas
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
byly	být	k5eAaImAgFnP	být
smíšené	smíšený	k2eAgFnPc1d1	smíšená
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Hollywood	Hollywood	k1gInSc1	Hollywood
Reporter	Reporter	k1gMnSc1	Reporter
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
dojmem	dojem	k1gInSc7	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
jste	být	k5eAaImIp2nP	být
to	ten	k3xDgNnSc1	ten
už	už	k9	už
někdy	někdy	k6eAd1	někdy
někde	někde	k6eAd1	někde
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
s	s	k7c7	s
nezaměnitelnou	zaměnitelný	k2eNgFnSc7d1	nezaměnitelná
směsí	směs	k1gFnSc7	směs
šarmu	šarm	k1gInSc2	šarm
a	a	k8xC	a
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Angelina	Angelin	k2eAgFnSc1d1	Angelina
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
hlas	hlas	k1gInSc4	hlas
rybce	rybka	k1gFnSc3	rybka
Lole	Lol	k1gInPc4	Lol
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Příběh	příběh	k1gInSc1	příběh
žraloka	žralok	k1gMnSc2	žralok
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
menší	malý	k2eAgFnSc4d2	menší
roli	role	k1gFnSc4	role
v	v	k7c6	v
komiksové	komiksový	k2eAgFnSc6d1	komiksová
adaptaci	adaptace	k1gFnSc6	adaptace
Svět	svět	k1gInSc1	svět
zítřka	zítřek	k1gInSc2	zítřek
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
Olympias	Olympias	k1gInSc4	Olympias
<g/>
,	,	kIx,	,
matku	matka	k1gFnSc4	matka
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
filmu	film	k1gInSc2	film
Alexander	Alexandra	k1gFnPc2	Alexandra
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
kinech	kino	k1gNnPc6	kino
film	film	k1gInSc4	film
propadl	propadnout	k5eAaPmAgInS	propadnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
vydělal	vydělat	k5eAaPmAgInS	vydělat
jen	jen	k9	jen
34	[number]	k4	34
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Oliver	Oliver	k1gMnSc1	Oliver
Stone	ston	k1gInSc5	ston
to	ten	k3xDgNnSc1	ten
přičítal	přičítat	k5eAaImAgMnS	přičítat
Alexanderově	Alexanderův	k2eAgFnSc3d1	Alexanderova
bisexualitě	bisexualita	k1gFnSc3	bisexualita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
filmu	film	k1gInSc6	film
vyobrazena	vyobrazit	k5eAaPmNgFnS	vyobrazit
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
však	však	k9	však
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
vydělal	vydělat	k5eAaPmAgInS	vydělat
133	[number]	k4	133
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ženský	ženský	k2eAgInSc4d1	ženský
protějšek	protějšek	k1gInSc4	protějšek
Brada	Brada	k1gMnSc1	Brada
Pitta	Pitta	k1gMnSc1	Pitta
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
&	&	k?	&
Mrs	Mrs	k1gFnSc1	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
manželů	manžel	k1gMnPc2	manžel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
svazek	svazek	k1gInSc1	svazek
nemá	mít	k5eNaImIp3nS	mít
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
však	však	k9	však
rapidně	rapidně	k6eAd1	rapidně
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zabijáci	zabiják	k1gMnPc1	zabiják
pracující	pracující	k2eAgMnPc1d1	pracující
pro	pro	k7c4	pro
konkurenční	konkurenční	k2eAgFnPc4d1	konkurenční
agentury	agentura	k1gFnPc4	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
smíšené	smíšený	k2eAgMnPc4d1	smíšený
kritiky	kritik	k1gMnPc4	kritik
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
chválen	chválit	k5eAaImNgInS	chválit
především	především	k9	především
za	za	k7c4	za
výborně	výborně	k6eAd1	výborně
fungující	fungující	k2eAgFnSc4d1	fungující
chemii	chemie	k1gFnSc4	chemie
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
hlavními	hlavní	k2eAgMnPc7d1	hlavní
hrdiny	hrdina	k1gMnPc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Star	Star	kA	Star
Tribune	tribun	k1gMnSc5	tribun
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zatímco	zatímco	k8xS	zatímco
příběh	příběh	k1gInSc1	příběh
působí	působit	k5eAaImIp3nS	působit
chaoticky	chaoticky	k6eAd1	chaoticky
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
nese	nést	k5eAaImIp3nS	nést
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
šarmu	šarm	k1gInSc2	šarm
<g/>
,	,	kIx,	,
pulsující	pulsující	k2eAgFnSc2d1	pulsující
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
výbušné	výbušný	k2eAgFnSc2d1	výbušná
chemie	chemie	k1gFnSc2	chemie
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Celosvětově	celosvětově	k6eAd1	celosvětově
film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
478	[number]	k4	478
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sedmým	sedmý	k4xOgInSc7	sedmý
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Roberta	Roberta	k1gFnSc1	Roberta
de	de	k?	de
Nira	Nir	k2eAgFnSc1d1	Nira
Kauza	kauza	k1gFnSc1	kauza
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
raná	raný	k2eAgNnPc4d1	rané
léta	léto	k1gNnPc4	léto
CIA	CIA	kA	CIA
očima	oko	k1gNnPc7	oko
Edwarda	Edward	k1gMnSc2	Edward
Wilsona	Wilson	k1gMnSc2	Wilson
<g/>
,	,	kIx,	,
ctižádostivého	ctižádostivý	k2eAgMnSc4d1	ctižádostivý
důstojníka	důstojník	k1gMnSc4	důstojník
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
hraje	hrát	k5eAaImIp3nS	hrát
Matt	Matt	k1gMnSc1	Matt
Damon	Damon	k1gMnSc1	Damon
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
role	role	k1gFnSc1	role
Margaret	Margareta	k1gFnPc2	Margareta
"	"	kIx"	"
<g/>
Clover	Clover	k1gInSc1	Clover
<g/>
"	"	kIx"	"
Russellové	Russellová	k1gFnPc1	Russellová
<g/>
,	,	kIx,	,
Wilsonovy	Wilsonův	k2eAgFnPc1d1	Wilsonova
zanedbávané	zanedbávaný	k2eAgFnPc1d1	zanedbávaná
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Chicago	Chicago	k1gNnSc1	Chicago
Tribune	tribun	k1gMnSc5	tribun
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jolie	Jolie	k1gFnSc1	Jolie
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
filmu	film	k1gInSc2	film
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
stárne	stárnout	k5eAaImIp3nS	stárnout
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bezstarostně	bezstarostně	k6eAd1	bezstarostně
lhostejná	lhostejný	k2eAgFnSc1d1	lhostejná
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
její	její	k3xOp3gInSc1	její
křehký	křehký	k2eAgInSc1d1	křehký
charakter	charakter	k1gInSc1	charakter
vnímají	vnímat	k5eAaImIp3nP	vnímat
diváci	divák	k1gMnPc1	divák
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roli	role	k1gFnSc4	role
režisérky	režisérka	k1gFnSc2	režisérka
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
vyzkoušela	vyzkoušet	k5eAaPmAgFnS	vyzkoušet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
s	s	k7c7	s
dokumentárním	dokumentární	k2eAgInSc7d1	dokumentární
filmem	film	k1gInSc7	film
A	a	k8xC	a
Place	plac	k1gInSc6	plac
in	in	k?	in
Time	Time	k1gInSc1	Time
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
běžný	běžný	k2eAgInSc1d1	běžný
každodenní	každodenní	k2eAgInSc1d1	každodenní
život	život	k1gInSc1	život
na	na	k7c6	na
27	[number]	k4	27
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
světa	svět	k1gInSc2	svět
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
film	film	k1gInSc4	film
určený	určený	k2eAgInSc4d1	určený
především	především	k6eAd1	především
k	k	k7c3	k
promítání	promítání	k1gNnSc3	promítání
na	na	k7c6	na
amerických	americký	k2eAgFnPc6d1	americká
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
roli	role	k1gFnSc4	role
Mariane	Marian	k1gMnSc5	Marian
Pearlové	Pearlové	k2eAgNnSc6d1	Pearlové
v	v	k7c6	v
dokumentárním	dokumentární	k2eAgNnSc6d1	dokumentární
dramatu	drama	k1gNnSc6	drama
Síla	síla	k1gFnSc1	síla
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
stejnojmenných	stejnojmenný	k2eAgFnPc2d1	stejnojmenná
pamětí	paměť	k1gFnPc2	paměť
skutečné	skutečný	k2eAgFnSc2d1	skutečná
Mariane	Marian	k1gMnSc5	Marian
Pearlové	Pearlový	k2eAgFnSc3d1	Pearlový
<g/>
,	,	kIx,	,
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
únos	únos	k1gInSc4	únos
a	a	k8xC	a
vraždu	vražda	k1gFnSc4	vražda
jejího	její	k3xOp3gMnSc2	její
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
novináře	novinář	k1gMnSc2	novinář
Daniela	Daniel	k1gMnSc2	Daniel
Pearla	Pearl	k1gMnSc2	Pearl
<g/>
,	,	kIx,	,
pákistánskými	pákistánský	k2eAgMnPc7d1	pákistánský
teroristy	terorista	k1gMnPc7	terorista
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hollywood	Hollywood	k1gInSc1	Hollywood
Reporter	Reporter	k1gMnSc1	Reporter
popsal	popsat	k5eAaPmAgMnS	popsat
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Role	role	k1gFnSc1	role
jí	on	k3xPp3gFnSc3	on
sedí	sedit	k5eAaImIp3nS	sedit
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
dojemně	dojemně	k6eAd1	dojemně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
respekt	respekt	k1gInSc4	respekt
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
zvládá	zvládat	k5eAaImIp3nS	zvládat
i	i	k9	i
obtížné	obtížný	k2eAgInPc1d1	obtížný
dialogy	dialog	k1gInPc1	dialog
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Angelina	Angelin	k2eAgFnSc1d1	Angelina
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guild	k1gMnSc1	Guild
Award	Award	k1gMnSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
také	také	k9	také
dorazil	dorazit	k5eAaPmAgInS	dorazit
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
film	film	k1gInSc1	film
Beowulf	Beowulf	k1gInSc4	Beowulf
<g/>
,	,	kIx,	,
vytvářený	vytvářený	k2eAgInSc4d1	vytvářený
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
technikou	technika	k1gFnSc7	technika
motion	motion	k1gInSc1	motion
capture	captur	k1gMnSc5	captur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Angelina	Angelin	k2eAgFnSc1d1	Angelina
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
roli	role	k1gFnSc4	role
Grendelovy	Grendelův	k2eAgFnSc2d1	Grendelova
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Jamese	Jamese	k1gFnSc2	Jamese
McAvoye	McAvoy	k1gFnSc2	McAvoy
a	a	k8xC	a
Morgana	morgan	k1gMnSc2	morgan
Freemana	Freeman	k1gMnSc2	Freeman
v	v	k7c6	v
akčním	akční	k2eAgInSc6d1	akční
filmu	film	k1gInSc6	film
Wanted	Wanted	k1gInSc1	Wanted
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
příznivé	příznivý	k2eAgFnPc4d1	příznivá
kritiky	kritika	k1gFnPc4	kritika
a	a	k8xC	a
i	i	k9	i
po	po	k7c6	po
komerční	komerční	k2eAgFnSc6d1	komerční
stránce	stránka	k1gFnSc6	stránka
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
když	když	k8xS	když
celosvětově	celosvětově	k6eAd1	celosvětově
vydělal	vydělat	k5eAaPmAgInS	vydělat
342	[number]	k4	342
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
dále	daleko	k6eAd2	daleko
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
hlas	hlas	k1gInSc4	hlas
Mistryni	mistryně	k1gFnSc4	mistryně
Tygřici	tygřice	k1gFnSc4	tygřice
v	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
filmu	film	k1gInSc6	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
Kung	Kunga	k1gFnPc2	Kunga
Fu	fu	k0	fu
Panda	panda	k1gFnSc1	panda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
výdělkem	výdělek	k1gInSc7	výdělek
632	[number]	k4	632
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
stal	stát	k5eAaPmAgInS	stát
třetím	třetí	k4xOgInSc7	třetí
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
si	se	k3xPyFc3	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
zahrála	zahrát	k5eAaPmAgFnS	zahrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Clinta	Clinta	k1gFnSc1	Clinta
Eastwooda	Eastwooda	k1gFnSc1	Eastwooda
Výměna	výměna	k1gFnSc1	výměna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
skutečných	skutečný	k2eAgInPc6d1	skutečný
únosech	únos	k1gInPc6	únos
a	a	k8xC	a
vraždách	vražda	k1gFnPc6	vražda
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Angelina	Angelin	k2eAgFnSc1d1	Angelina
hrála	hrát	k5eAaImAgFnS	hrát
Christine	Christin	k1gInSc5	Christin
Collinsovou	Collinsová	k1gFnSc4	Collinsová
<g/>
,	,	kIx,	,
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
po	po	k7c6	po
pěti	pět	k4xCc6	pět
měsících	měsíc	k1gInPc6	měsíc
znovu	znovu	k6eAd1	znovu
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
uneseným	unesený	k2eAgMnSc7d1	unesený
synem	syn	k1gMnSc7	syn
-	-	kIx~	-
jen	jen	k6eAd1	jen
aby	aby	kYmCp3nS	aby
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
policie	policie	k1gFnSc1	policie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podvodník	podvodník	k1gMnSc1	podvodník
<g/>
.	.	kIx.	.
</s>
<s>
Chicago	Chicago	k1gNnSc1	Chicago
Tribune	tribun	k1gMnSc5	tribun
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jolie	Jolie	k1gFnSc1	Jolie
opravdu	opravdu	k6eAd1	opravdu
září	zářit	k5eAaImIp3nS	zářit
během	během	k7c2	během
klidu	klid	k1gInSc2	klid
před	před	k7c7	před
bouří	bouř	k1gFnSc7	bouř
<g/>
,	,	kIx,	,
ve	v	k7c6	v
scénách	scéna	k1gFnPc6	scéna
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
jedna	jeden	k4xCgFnSc1	jeden
povýšená	povýšený	k2eAgFnSc1d1	povýšená
autorita	autorita	k1gFnSc1	autorita
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
odmítá	odmítat	k5eAaImIp3nS	odmítat
a	a	k8xC	a
ponižuje	ponižovat	k5eAaImIp3nS	ponižovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Angelina	Angelin	k2eAgFnSc1d1	Angelina
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
(	(	kIx(	(
<g/>
Oskara	Oskar	k1gMnSc2	Oskar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
,	,	kIx,	,
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guild	k1gMnSc1	Guild
Award	Award	k1gMnSc1	Award
a	a	k8xC	a
na	na	k7c4	na
Cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
roční	roční	k2eAgFnSc6d1	roční
pauze	pauza	k1gFnSc6	pauza
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Salt	salto	k1gNnPc2	salto
agentku	agentka	k1gFnSc4	agentka
CIA	CIA	kA	CIA
Evelyn	Evelyn	k1gNnSc1	Evelyn
Saltovou	saltový	k2eAgFnSc7d1	Saltová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dát	dát	k5eAaPmF	dát
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ji	on	k3xPp3gFnSc4	on
spáč	spáč	k1gMnSc1	spáč
KGB	KGB	kA	KGB
obviní	obvinit	k5eAaPmIp3nS	obvinit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
agentkou	agentka	k1gFnSc7	agentka
také	také	k9	také
pracující	pracující	k2eAgMnPc1d1	pracující
pro	pro	k7c4	pro
Rusy	Rus	k1gMnPc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
psána	psát	k5eAaImNgFnS	psát
jako	jako	k8xS	jako
mužská	mužský	k2eAgFnSc1d1	mužská
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vedení	vedení	k1gNnSc2	vedení
studia	studio	k1gNnSc2	studio
Columbia	Columbia	k1gFnSc1	Columbia
Pictures	Pictures	k1gMnSc1	Pictures
doporučilo	doporučit	k5eAaPmAgNnS	doporučit
režisérovi	režisér	k1gMnSc3	režisér
Phillipovi	Phillip	k1gMnSc3	Phillip
Noycemu	Noycema	k1gFnSc4	Noycema
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
Angelinu	Angelin	k2eAgFnSc4d1	Angelina
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
úspěch	úspěch	k1gInSc4	úspěch
s	s	k7c7	s
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
výdělkem	výdělek	k1gInSc7	výdělek
294	[number]	k4	294
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritika	k1gFnPc1	kritika
byly	být	k5eAaImAgFnP	být
smíšené	smíšený	k2eAgInPc1d1	smíšený
až	až	k6eAd1	až
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
byl	být	k5eAaImAgMnS	být
spíše	spíše	k9	spíše
chválen	chválen	k2eAgMnSc1d1	chválen
<g/>
;	;	kIx,	;
časopis	časopis	k1gInSc1	časopis
Empire	empir	k1gInSc5	empir
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
prodat	prodat	k5eAaPmF	prodat
neuvěřitelné	uvěřitelný	k2eNgFnPc4d1	neuvěřitelná
<g/>
,	,	kIx,	,
šílené	šílený	k2eAgFnPc4d1	šílená
<g/>
,	,	kIx,	,
dechberoucí	dechberoucí	k2eAgFnPc4d1	dechberoucí
kašpařiny	kašpařina	k1gFnPc4	kašpařina
<g/>
,	,	kIx,	,
Jolie	Jolie	k1gFnSc1	Jolie
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
akčním	akční	k2eAgInSc6d1	akční
byznyse	byznys	k1gInSc6	byznys
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Angelina	Angelin	k2eAgFnSc1d1	Angelina
si	se	k3xPyFc3	se
dále	daleko	k6eAd2	daleko
zahrála	zahrát	k5eAaPmAgFnS	zahrát
vedle	vedle	k7c2	vedle
Johnnyho	Johnny	k1gMnSc2	Johnny
Deppa	Depp	k1gMnSc2	Depp
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
u	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
zcela	zcela	k6eAd1	zcela
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Peter	Peter	k1gMnSc1	Peter
Travers	travers	k1gInSc4	travers
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Depp	Depp	k1gInSc4	Depp
a	a	k8xC	a
Jolie	Jolie	k1gFnPc4	Jolie
spadli	spadnout	k5eAaPmAgMnP	spadnout
na	na	k7c4	na
samé	samý	k3xTgNnSc4	samý
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
dojmem	dojem	k1gInSc7	dojem
dobře	dobře	k6eAd1	dobře
oblečených	oblečený	k2eAgFnPc2d1	oblečená
zombií	zombie	k1gFnPc2	zombie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slabém	slabý	k2eAgInSc6d1	slabý
začátku	začátek	k1gInSc6	začátek
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
kinech	kino	k1gNnPc6	kino
však	však	k8xC	však
film	film	k1gInSc4	film
nakonec	nakonec	k6eAd1	nakonec
celosvětově	celosvětově	k6eAd1	celosvětově
vydělal	vydělat	k5eAaPmAgMnS	vydělat
slušných	slušný	k2eAgFnPc2d1	slušná
278	[number]	k4	278
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
obdržela	obdržet	k5eAaPmAgFnS	obdržet
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejím	její	k3xOp3gInSc7	její
jediným	jediný	k2eAgInSc7d1	jediný
důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
zajistit	zajistit	k5eAaPmF	zajistit
přítomnost	přítomnost	k1gFnSc4	přítomnost
mediálně	mediálně	k6eAd1	mediálně
atraktivní	atraktivní	k2eAgFnPc1d1	atraktivní
osobnosti	osobnost	k1gFnPc1	osobnost
na	na	k7c6	na
předávacím	předávací	k2eAgInSc6d1	předávací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
znovu	znovu	k6eAd1	znovu
namluvila	namluvit	k5eAaBmAgFnS	namluvit
Mistryni	mistryně	k1gFnSc4	mistryně
Tygřici	tygřice	k1gFnSc4	tygřice
v	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
filmu	film	k1gInSc6	film
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
Kung	Kunga	k1gFnPc2	Kunga
Fu	fu	k0	fu
Panda	panda	k1gFnSc1	panda
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
s	s	k7c7	s
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
výdělkem	výdělek	k1gInSc7	výdělek
666	[number]	k4	666
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
vytváření	vytváření	k1gNnSc6	vytváření
se	se	k3xPyFc4	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
podílela	podílet	k5eAaImAgFnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
herecké	herecký	k2eAgInPc4d1	herecký
kostýmy	kostým	k1gInPc4	kostým
za	za	k7c4	za
režisérskou	režisérský	k2eAgFnSc4d1	režisérská
židli	židle	k1gFnSc4	židle
a	a	k8xC	a
natočila	natočit	k5eAaBmAgFnS	natočit
film	film	k1gInSc4	film
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
medu	med	k1gInSc2	med
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
milostný	milostný	k2eAgInSc1d1	milostný
příběh	příběh	k1gInSc1	příběh
mezi	mezi	k7c7	mezi
srbským	srbský	k2eAgMnSc7d1	srbský
vojákem	voják	k1gMnSc7	voják
a	a	k8xC	a
bosenskou	bosenský	k2eAgFnSc7d1	bosenská
válečnou	válečný	k2eAgFnSc7d1	válečná
zajatkyní	zajatkyně	k1gFnSc7	zajatkyně
během	během	k7c2	během
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
v	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Bosnu	Bosna	k1gFnSc4	Bosna
navštívila	navštívit	k5eAaPmAgFnS	navštívit
dvakrát	dvakrát	k6eAd1	dvakrát
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
Vyslankyně	vyslankyně	k1gFnSc2	vyslankyně
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
UNHCR	UNHCR	kA	UNHCR
<g/>
,	,	kIx,	,
objasnila	objasnit	k5eAaPmAgFnS	objasnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmem	film	k1gInSc7	film
chtěla	chtít	k5eAaImAgFnS	chtít
znovu	znovu	k6eAd1	znovu
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
vypořádávají	vypořádávat	k5eAaImIp3nP	vypořádávat
s	s	k7c7	s
následky	následek	k1gInPc7	následek
této	tento	k3xDgFnSc2	tento
nedávné	dávný	k2eNgFnSc2d1	nedávná
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
Angelina	Angelin	k2eAgFnSc1d1	Angelina
také	také	k9	také
napsala	napsat	k5eAaBmAgFnS	napsat
scénář	scénář	k1gInSc4	scénář
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
jej	on	k3xPp3gMnSc4	on
produkovala	produkovat	k5eAaImAgFnS	produkovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
sklidil	sklidit	k5eAaPmAgMnS	sklidit
chválu	chvála	k1gFnSc4	chvála
i	i	k8xC	i
kritiku	kritika	k1gFnSc4	kritika
<g/>
;	;	kIx,	;
odezvy	odezva	k1gFnSc2	odezva
z	z	k7c2	z
Bosny	Bosna	k1gFnSc2	Bosna
byly	být	k5eAaImAgInP	být
"	"	kIx"	"
<g/>
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
pozitivní	pozitivní	k2eAgFnPc1d1	pozitivní
<g/>
"	"	kIx"	"
<g/>
,,	,,	k?	,,
zatímco	zatímco	k8xS	zatímco
Srbové	Srbové	k2eAgInSc4d1	Srbové
film	film	k1gInSc4	film
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
kvůli	kvůli	k7c3	kvůli
údajnému	údajný	k2eAgNnSc3d1	údajné
protisrbskému	protisrbský	k2eAgNnSc3d1	protisrbský
zaměření	zaměření	k1gNnSc3	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Angelině	Angelin	k2eAgFnSc3d1	Angelina
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
znovuoživení	znovuoživení	k1gNnSc4	znovuoživení
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
povědomí	povědomí	k1gNnSc2	povědomí
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
uděleno	udělen	k2eAgNnSc1d1	uděleno
čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
Stanley	Stanlea	k1gFnSc2	Stanlea
Kramer	Kramer	k1gMnSc1	Kramer
Award	Award	k1gMnSc1	Award
od	od	k7c2	od
producentské	producentský	k2eAgFnSc2d1	producentská
organizace	organizace	k1gFnSc2	organizace
Producers	Producersa	k1gFnPc2	Producersa
Guild	Guild	k1gMnSc1	Guild
of	of	k?	of
America	America	k1gFnSc1	America
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
filmům	film	k1gInPc3	film
zaměřeným	zaměřený	k2eAgInPc3d1	zaměřený
na	na	k7c4	na
provokativní	provokativní	k2eAgNnPc4d1	provokativní
sociální	sociální	k2eAgNnPc4d1	sociální
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
rovněž	rovněž	k9	rovněž
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
a	a	k8xC	a
půl	půl	k1xP	půl
letech	léto	k1gNnPc6	léto
pauzy	pauza	k1gFnSc2	pauza
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Maleficent	Maleficent	k1gInSc1	Maleficent
<g/>
,	,	kIx,	,
inspirovaném	inspirovaný	k2eAgInSc6d1	inspirovaný
postavou	postava	k1gFnSc7	postava
z	z	k7c2	z
filmů	film	k1gInPc2	film
Walta	Walt	k1gMnSc2	Walt
Disneye	Disney	k1gMnSc2	Disney
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
víkend	víkend	k1gInSc4	víkend
film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
přes	přes	k7c4	přes
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
role	role	k1gFnSc1	role
režisérky	režisérka	k1gFnSc2	režisérka
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
filmu	film	k1gInSc3	film
Nezlomný	zlomný	k2eNgMnSc1d1	nezlomný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
naplánovaná	naplánovaný	k2eAgFnSc1d1	naplánovaná
premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
By	by	k9	by
the	the	k?	the
Sea	Sea	k1gFnPc2	Sea
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
hraje	hrát	k5eAaImIp3nS	hrát
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Bradem	Bradem	k?	Bradem
Pittem	Pitt	k1gMnSc7	Pitt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
vážnou	vážný	k2eAgFnSc4d1	vážná
známost	známost	k1gFnSc4	známost
navázala	navázat	k5eAaPmAgFnS	navázat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
vydržel	vydržet	k5eAaPmAgInS	vydržet
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
jim	on	k3xPp3gMnPc3	on
povolila	povolit	k5eAaPmAgFnS	povolit
scházet	scházet	k5eAaImF	scházet
se	se	k3xPyFc4	se
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
později	pozdě	k6eAd2	pozdě
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Buď	buď	k8xC	buď
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
toulat	toulat	k5eAaImF	toulat
někde	někde	k6eAd1	někde
po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsme	být	k5eAaImIp1nP	být
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
mé	můj	k3xOp1gFnSc6	můj
ložnici	ložnice	k1gFnSc6	ložnice
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
ve	v	k7c6	v
vedlejším	vedlejší	k2eAgInSc6d1	vedlejší
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
zvolila	zvolit	k5eAaPmAgFnS	zvolit
druhou	druhý	k4xOgFnSc4	druhý
možnost	možnost	k1gFnSc4	možnost
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
jsem	být	k5eAaImIp1nS	být
stále	stále	k6eAd1	stále
vstávala	vstávat	k5eAaImAgFnS	vstávat
každé	každý	k3xTgNnSc4	každý
ráno	ráno	k1gNnSc4	ráno
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
můj	můj	k3xOp1gInSc1	můj
první	první	k4xOgInSc1	první
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
vztah	vztah	k1gInSc4	vztah
přirovnala	přirovnat	k5eAaPmAgFnS	přirovnat
jeho	jeho	k3xOp3gFnSc4	jeho
emoční	emoční	k2eAgFnSc4d1	emoční
intenzitou	intenzita	k1gFnSc7	intenzita
k	k	k7c3	k
manželství	manželství	k1gNnSc3	manželství
a	a	k8xC	a
dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozchod	rozchod	k1gInSc1	rozchod
ji	on	k3xPp3gFnSc4	on
přiměl	přimět	k5eAaPmAgInS	přimět
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
herecké	herecký	k2eAgFnSc3d1	herecká
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
síť	síť	k1gFnSc1	síť
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
prožila	prožít	k5eAaPmAgFnS	prožít
krátký	krátký	k2eAgInSc4d1	krátký
románek	románek	k1gInSc4	románek
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
hercem	herec	k1gMnSc7	herec
Jonnym	Jonnymum	k1gNnPc2	Jonnymum
Lee	Lea	k1gFnSc3	Lea
Millerem	Miller	k1gMnSc7	Miller
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gMnSc1	její
první	první	k4xOgMnSc1	první
milenec	milenec	k1gMnSc1	milenec
od	od	k7c2	od
rozpadu	rozpad	k1gInSc2	rozpad
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
natáčení	natáčení	k1gNnSc2	natáčení
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
přerušili	přerušit	k5eAaPmAgMnP	přerušit
kontakt	kontakt	k1gInSc4	kontakt
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
dali	dát	k5eAaPmAgMnP	dát
znovu	znovu	k6eAd1	znovu
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
následovala	následovat	k5eAaImAgFnS	následovat
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgNnPc1d1	Angelino
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
dostavila	dostavit	k5eAaPmAgFnS	dostavit
v	v	k7c6	v
černých	černý	k2eAgFnPc6d1	černá
kožených	kožený	k2eAgFnPc6d1	kožená
kalhotách	kalhoty	k1gFnPc6	kalhoty
a	a	k8xC	a
bílé	bílý	k2eAgFnSc6d1	bílá
košili	košile	k1gFnSc6	košile
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
měla	mít	k5eAaImAgFnS	mít
napsáno	napsán	k2eAgNnSc4d1	napsáno
ženichovo	ženichův	k2eAgNnSc4d1	ženichovo
jméno	jméno	k1gNnSc4	jméno
svojí	svojit	k5eAaImIp3nS	svojit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgMnSc2d1	Angelin
a	a	k8xC	a
Jonny	Jonna	k1gMnSc2	Jonna
se	se	k3xPyFc4	se
rozešli	rozejít	k5eAaPmAgMnP	rozejít
již	již	k6eAd1	již
v	v	k7c6	v
září	září	k1gNnSc6	září
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
rozvedeno	rozvést	k5eAaPmNgNnS	rozvést
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
zůstali	zůstat	k5eAaPmAgMnP	zůstat
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
později	pozdě	k6eAd2	pozdě
objasnila	objasnit	k5eAaPmAgFnS	objasnit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
o	o	k7c6	o
načasování	načasování	k1gNnSc6	načasování
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
Jonny	Jonna	k1gFnSc2	Jonna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
jakého	jaký	k3yQgMnSc4	jaký
si	se	k3xPyFc3	se
dívka	dívka	k1gFnSc1	dívka
může	moct	k5eAaImIp3nS	moct
přát	přát	k5eAaImF	přát
<g/>
.	.	kIx.	.
</s>
<s>
Vždycky	vždycky	k6eAd1	vždycky
ho	on	k3xPp3gMnSc4	on
budu	být	k5eAaImBp1nS	být
milovat	milovat	k5eAaImF	milovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
prostě	prostě	k9	prostě
příliš	příliš	k6eAd1	příliš
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Angelina	Angelin	k2eAgFnSc1d1	Angelina
prožila	prožít	k5eAaPmAgFnS	prožít
krátký	krátký	k2eAgInSc4d1	krátký
lesbický	lesbický	k2eAgInSc4d1	lesbický
románek	románek	k1gInSc4	románek
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
a	a	k8xC	a
modelkou	modelka	k1gFnSc7	modelka
Jenny	Jenna	k1gFnSc2	Jenna
Shimizu	Shimiz	k1gInSc2	Shimiz
během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
Foxfire	Foxfir	k1gInSc5	Foxfir
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Jenny	Jenn	k1gMnPc4	Jenn
<g/>
,	,	kIx,	,
kdybych	kdyby	kYmCp1nS	kdyby
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
za	za	k7c4	za
mého	můj	k3xOp1gMnSc4	můj
manžela	manžel	k1gMnSc4	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
láska	láska	k1gFnSc1	láska
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Shimizu	Shimiz	k1gInSc2	Shimiz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
trval	trvat	k5eAaImAgInS	trvat
roky	rok	k1gInPc4	rok
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Angelina	Angelin	k2eAgFnSc1d1	Angelina
měla	mít	k5eAaImAgFnS	mít
romantické	romantický	k2eAgInPc4d1	romantický
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
bisexuální	bisexuální	k2eAgNnSc1d1	bisexuální
<g/>
,	,	kIx,	,
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zítra	zítra	k6eAd1	zítra
zamiluji	zamilovat	k5eAaPmIp1nS	zamilovat
do	do	k7c2	do
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
budu	být	k5eAaImBp1nS	být
líbat	líbat	k5eAaImF	líbat
a	a	k8xC	a
dotýkat	dotýkat	k5eAaImF	dotýkat
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
<g/>
?	?	kIx.	?
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
budu	být	k5eAaImBp1nS	být
milovat	milovat	k5eAaImF	milovat
<g/>
?	?	kIx.	?
</s>
<s>
Určitě	určitě	k6eAd1	určitě
<g/>
!	!	kIx.	!
</s>
<s>
Ano	ano	k9	ano
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Po	po	k7c6	po
dvouměsíčních	dvouměsíční	k2eAgFnPc6d1	dvouměsíční
námluvách	námluva	k1gFnPc6	námluva
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
za	za	k7c4	za
herce	herec	k1gMnSc4	herec
Billyho	Billy	k1gMnSc4	Billy
Boba	Bob	k1gMnSc4	Bob
Thorntona	Thornton	k1gMnSc4	Thornton
<g/>
.	.	kIx.	.
</s>
<s>
Potkali	potkat	k5eAaPmAgMnP	potkat
se	se	k3xPyFc4	se
na	na	k7c4	na
natáčení	natáčení	k1gNnSc4	natáčení
filmu	film	k1gInSc2	film
Bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
Runway	runway	k1gFnSc1	runway
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
nerozvinul	rozvinout	k5eNaPmAgMnS	rozvinout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Thornton	Thornton	k1gInSc1	Thornton
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Laurou	Laura	k1gFnSc7	Laura
Dernovou	Dernová	k1gFnSc7	Dernová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
s	s	k7c7	s
Billym	Billym	k1gInSc1	Billym
Bobem	bob	k1gInSc7	bob
otevřeně	otevřeně	k6eAd1	otevřeně
a	a	k8xC	a
vášnivě	vášnivě	k6eAd1	vášnivě
projevovali	projevovat	k5eAaImAgMnP	projevovat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
náklonnost	náklonnost	k1gFnSc4	náklonnost
(	(	kIx(	(
<g/>
známo	znám	k2eAgNnSc1d1	známo
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nosil	nosit	k5eAaImAgInS	nosit
na	na	k7c6	na
krku	krk	k1gInSc6	krk
ampulku	ampulka	k1gFnSc4	ampulka
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
toho	ten	k3xDgNnSc2	ten
druhého	druhý	k4xOgMnSc4	druhý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
stalo	stát	k5eAaPmAgNnS	stát
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
námětem	námět	k1gInSc7	námět
bulvárního	bulvární	k2eAgInSc2d1	bulvární
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2002	[number]	k4	2002
Angelina	Angelin	k2eAgFnSc1d1	Angelina
a	a	k8xC	a
Billy	Bill	k1gMnPc4	Bill
Bob	bob	k1gInSc4	bob
oznámili	oznámit	k5eAaPmAgMnP	oznámit
adopci	adopce	k1gFnSc4	adopce
syna	syn	k1gMnSc2	syn
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Kambodže	Kambodža	k1gFnSc2	Kambodža
<g/>
,	,	kIx,	,
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
však	však	k9	však
následoval	následovat	k5eAaImAgInS	následovat
náhlý	náhlý	k2eAgInSc1d1	náhlý
rozchod	rozchod	k1gInSc1	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
rozvedeni	rozveden	k2eAgMnPc1d1	rozveden
byli	být	k5eAaImAgMnP	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Angeliny	Angelin	k2eAgMnPc4d1	Angelin
později	pozdě	k6eAd2	pozdě
ptali	ptat	k5eAaImAgMnP	ptat
na	na	k7c4	na
náhlý	náhlý	k2eAgInSc4d1	náhlý
rozpad	rozpad	k1gInSc4	rozpad
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
úplně	úplně	k6eAd1	úplně
změnili	změnit	k5eAaPmAgMnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
jsme	být	k5eAaImIp1nP	být
neměli	mít	k5eNaImAgMnP	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Zní	znět	k5eAaImIp3nS	znět
to	ten	k3xDgNnSc1	ten
děsivě	děsivě	k6eAd1	děsivě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
...	...	k?	...
Myslím	myslet	k5eAaImIp1nS	myslet
že	že	k8xS	že
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
vrhnete	vrhnout	k5eAaPmIp2nP	vrhnout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
do	do	k7c2	do
vztahu	vztah	k1gInSc2	vztah
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
se	se	k3xPyFc4	se
dost	dost	k6eAd1	dost
dobře	dobře	k6eAd1	dobře
neznáte	znát	k5eNaImIp2nP	znát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
zapletla	zaplést	k5eAaPmAgFnS	zaplést
do	do	k7c2	do
hollywoodského	hollywoodský	k2eAgInSc2d1	hollywoodský
skandálu	skandál	k1gInSc2	skandál
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
obviněna	obvinit	k5eAaPmNgFnS	obvinit
ze	z	k7c2	z
zavinění	zavinění	k1gNnSc2	zavinění
rozpadu	rozpad	k1gInSc3	rozpad
manželství	manželství	k1gNnSc2	manželství
herců	herec	k1gMnPc2	herec
Brada	Brada	k1gMnSc1	Brada
Pitta	Pitta	k1gMnSc1	Pitta
a	a	k8xC	a
Jennifer	Jennifer	k1gMnSc1	Jennifer
Anistonové	Aniston	k1gMnPc1	Aniston
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
a	a	k8xC	a
Brad	brada	k1gFnPc2	brada
byli	být	k5eAaImAgMnP	být
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
románku	románek	k1gInSc2	románek
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
&	&	k?	&
Mrs	Mrs	k1gFnSc1	Mrs
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
to	ten	k3xDgNnSc1	ten
několikrát	několikrát	k6eAd1	několikrát
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
příležitostech	příležitost	k1gFnPc6	příležitost
popřela	popřít	k5eAaPmAgFnS	popřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
připustila	připustit	k5eAaPmAgFnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
s	s	k7c7	s
Bradem	Bradem	k?	Bradem
během	běh	k1gInSc7	běh
natáčení	natáčení	k1gNnSc2	natáčení
zamilovali	zamilovat	k5eAaPmAgMnP	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
ženatým	ženatý	k2eAgMnSc7d1	ženatý
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
když	když	k8xS	když
můj	můj	k3xOp1gMnSc1	můj
vlastní	vlastní	k2eAgMnSc1d1	vlastní
otec	otec	k1gMnSc1	otec
podvedl	podvést	k5eAaPmAgMnS	podvést
mou	můj	k3xOp1gFnSc4	můj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nemohu	moct	k5eNaImIp1nS	moct
odpustit	odpustit	k5eAaPmF	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Nemohla	moct	k5eNaImAgFnS	moct
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
ráno	ráno	k6eAd1	ráno
podívat	podívat	k5eAaImF	podívat
do	do	k7c2	do
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nepřitahoval	přitahovat	k5eNaImAgInS	přitahovat
by	by	kYmCp3nS	by
mě	já	k3xPp1nSc4	já
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podvádí	podvádět	k5eAaImIp3nS	podvádět
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Angelina	Angelin	k2eAgFnSc1d1	Angelina
a	a	k8xC	a
Brad	brada	k1gFnPc2	brada
nekomentovali	komentovat	k5eNaBmAgMnP	komentovat
veřejně	veřejně	k6eAd1	veřejně
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
až	až	k9	až
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Angelina	Angelin	k2eAgFnSc1d1	Angelina
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
časopisu	časopis	k1gInSc2	časopis
People	People	k1gFnPc7	People
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Bradem	Bradem	k?	Bradem
čeká	čekat	k5eAaImIp3nS	čekat
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
Angelina	Angelin	k2eAgNnSc2d1	Angelino
a	a	k8xC	a
Brad	brada	k1gFnPc2	brada
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	let	k1gInPc6	let
vztahu	vztah	k1gInSc2	vztah
oficiálně	oficiálně	k6eAd1	oficiálně
oznámili	oznámit	k5eAaPmAgMnP	oznámit
zasnoubení	zasnoubení	k1gNnSc4	zasnoubení
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
ale	ale	k9	ale
až	až	k9	až
po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
na	na	k7c6	na
francouzském	francouzský	k2eAgInSc6d1	francouzský
zámku	zámek	k1gInSc6	zámek
Miraval	Miraval	k1gFnSc2	Miraval
<g/>
,	,	kIx,	,
v	v	k7c6	v
úzkem	úzko	k1gNnSc7	úzko
kruhu	kruh	k1gInSc2	kruh
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k1gInSc1	pár
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
též	též	k9	též
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Brangelina	Brangelina	k1gFnSc1	Brangelina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
velkému	velký	k2eAgInSc3d1	velký
zájmu	zájem	k1gInSc6	zájem
médií	médium	k1gNnPc2	médium
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
podala	podat	k5eAaPmAgFnS	podat
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
a	a	k8xC	a
svěření	svěření	k1gNnSc4	svěření
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
výlučné	výlučný	k2eAgFnSc2d1	výlučná
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
sedmiměsíčního	sedmiměsíční	k2eAgMnSc2d1	sedmiměsíční
chlapce	chlapec	k1gMnSc2	chlapec
Maddoxe	Maddoxe	k1gFnSc2	Maddoxe
Chivana	Chivan	k1gMnSc2	Chivan
ze	z	k7c2	z
sirotčince	sirotčinec	k1gInSc2	sirotčinec
v	v	k7c4	v
Phnom	Phnom	k1gInSc4	Phnom
Penhu	Penh	k1gInSc2	Penh
v	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Rath	Ratha	k1gFnPc2	Ratha
Vibol	Vibola	k1gFnPc2	Vibola
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
adopci	adopce	k1gFnSc4	adopce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dvakrát	dvakrát	k6eAd1	dvakrát
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Kambodžu	Kambodža	k1gFnSc4	Kambodža
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Lara	Lara	k1gMnSc1	Lara
Croft	Croft	k1gMnSc1	Croft
–	–	k?	–
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
na	na	k7c6	na
misi	mise	k1gFnSc6	mise
UNHCR	UNHCR	kA	UNHCR
<g/>
.	.	kIx.	.
</s>
<s>
Adopční	adopční	k2eAgInSc1d1	adopční
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
čas	čas	k1gInSc4	čas
přerušen	přerušit	k5eAaPmNgInS	přerušit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
když	když	k8xS	když
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
zakázala	zakázat	k5eAaPmAgFnS	zakázat
adopce	adopce	k1gFnSc1	adopce
z	z	k7c2	z
Kambodži	Kambodža	k1gFnSc6	Kambodža
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c4	o
obchodování	obchodování	k1gNnSc4	obchodování
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
adopce	adopce	k1gFnSc2	adopce
se	se	k3xPyFc4	se
Angelina	Angelin	k2eAgFnSc1d1	Angelina
i	i	k8xC	i
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
do	do	k7c2	do
Namibie	Namibie	k1gFnSc2	Namibie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
natáčela	natáčet	k5eAaImAgFnS	natáčet
film	film	k1gInSc4	film
Hranice	hranice	k1gFnSc2	hranice
zlomu	zlom	k1gInSc2	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Angelina	Angelin	k2eAgFnSc1d1	Angelina
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Billym	Billym	k1gInSc4	Billym
Bobem	Bob	k1gMnSc7	Bob
Thorntonem	Thornton	k1gInSc7	Thornton
oznámili	oznámit	k5eAaPmAgMnP	oznámit
adopci	adopce	k1gFnSc3	adopce
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
Maddoxe	Maddoxe	k1gFnSc1	Maddoxe
pouze	pouze	k6eAd1	pouze
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
holčičku	holčička	k1gFnSc4	holčička
jménem	jméno	k1gNnSc7	jméno
Zahara	Zahar	k1gMnSc2	Zahar
Marley	Marlea	k1gMnSc2	Marlea
<g/>
,	,	kIx,	,
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
ze	z	k7c2	z
sirotčince	sirotčinec	k1gInSc2	sirotčinec
v	v	k7c6	v
Addis	Addis	k1gFnSc6	Addis
Abebě	Abeba	k1gFnSc6	Abeba
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Zahara	Zahara	k1gFnSc1	Zahara
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
v	v	k7c6	v
Awase	Awasa	k1gFnSc6	Awasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
adopce	adopce	k1gFnSc2	adopce
panovala	panovat	k5eAaImAgFnS	panovat
mylná	mylný	k2eAgFnSc1d1	mylná
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zahara	Zahara	k1gFnSc1	Zahara
je	být	k5eAaImIp3nS	být
sirotek	sirotek	k1gMnSc1	sirotek
po	po	k7c6	po
rodičích	rodič	k1gMnPc6	rodič
zemřelých	zemřelý	k2eAgMnPc6d1	zemřelý
na	na	k7c4	na
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
sama	sám	k3xTgFnSc1	sám
není	být	k5eNaImIp3nS	být
AIDS	aids	k1gInSc4	aids
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
testy	test	k1gInPc1	test
však	však	k9	však
byly	být	k5eAaImAgInP	být
negativní	negativní	k2eAgInPc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
příletu	přílet	k1gInSc6	přílet
do	do	k7c2	do
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
Zahara	Zahara	k1gFnSc1	Zahara
hospitalizována	hospitalizován	k2eAgFnSc1d1	hospitalizována
kvůli	kvůli	k7c3	kvůli
dehydrataci	dehydratace	k1gFnSc3	dehydratace
a	a	k8xC	a
podvýživě	podvýživa	k1gFnSc3	podvýživa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
médii	médium	k1gNnPc7	médium
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
biologická	biologický	k2eAgFnSc1d1	biologická
matka	matka	k1gFnSc1	matka
chce	chtít	k5eAaImIp3nS	chtít
Zaharu	Zahara	k1gFnSc4	Zahara
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
to	ten	k3xDgNnSc4	ten
však	však	k8xC	však
popřela	popřít	k5eAaPmAgFnS	popřít
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Zahara	Zahara	k1gFnSc1	Zahara
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc4d1	velké
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgNnP	být
adoptována	adoptován	k2eAgNnPc1d1	adoptováno
někým	někdo	k3yInSc7	někdo
jako	jako	k9	jako
Angelina	Angelin	k2eAgMnSc4d1	Angelin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cestovala	cestovat	k5eAaImAgFnS	cestovat
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
Zaharu	Zahara	k1gFnSc4	Zahara
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Brad	brada	k1gFnPc2	brada
Pitt	Pitt	k2eAgMnSc1d1	Pitt
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
naznačila	naznačit	k5eAaPmAgFnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
adopce	adopce	k1gFnSc1	adopce
z	z	k7c2	z
Etiopie	Etiopie	k1gFnSc2	Etiopie
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gNnSc7	jejich
společným	společný	k2eAgNnSc7d1	společné
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
Bradův	Bradův	k2eAgMnSc1d1	Bradův
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brad	brada	k1gFnPc2	brada
hodlá	hodlat	k5eAaImIp3nS	hodlat
adoptovat	adoptovat	k5eAaPmF	adoptovat
Maddoxe	Maddoxe	k1gFnPc4	Maddoxe
i	i	k8xC	i
Zaharu	Zahara	k1gFnSc4	Zahara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
Angelina	Angelin	k2eAgFnSc1d1	Angelina
požádala	požádat	k5eAaPmAgFnS	požádat
úřady	úřad	k1gInPc4	úřad
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
příjmení	příjmení	k1gNnSc2	příjmení
dětí	dítě	k1gFnPc2	dítě
na	na	k7c4	na
Jolie-Pitt	Jolie-Pitt	k1gInSc4	Jolie-Pitt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
oficiálně	oficiálně	k6eAd1	oficiálně
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
obě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
uniknout	uniknout	k5eAaPmF	uniknout
mediálnímu	mediální	k2eAgNnSc3d1	mediální
šílenství	šílenství	k1gNnSc3	šílenství
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Bradem	Bradem	k?	Bradem
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
jejich	jejich	k3xOp3gMnSc2	jejich
prvního	první	k4xOgMnSc2	první
biologického	biologický	k2eAgMnSc2d1	biologický
potomka	potomek	k1gMnSc2	potomek
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
do	do	k7c2	do
Namibie	Namibie	k1gFnSc2	Namibie
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
Swakopmundu	Swakopmund	k1gInSc6	Swakopmund
narodila	narodit	k5eAaPmAgFnS	narodit
dcerka	dcerka	k1gFnSc1	dcerka
Shiloh	Shiloh	k1gMnSc1	Shiloh
Nouvel	Nouvel	k1gMnSc1	Nouvel
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
namibijský	namibijský	k2eAgInSc4d1	namibijský
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
prodat	prodat	k5eAaPmF	prodat
první	první	k4xOgInPc4	první
snímky	snímek	k1gInPc4	snímek
Shiloh	Shiloh	k1gMnSc1	Shiloh
Nouvel	Nouvel	k1gMnSc1	Nouvel
vybraným	vybraný	k2eAgMnPc3d1	vybraný
časopisům	časopis	k1gInPc3	časopis
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
z	z	k7c2	z
cenných	cenný	k2eAgInPc2d1	cenný
snímků	snímek	k1gInPc2	snímek
profitovali	profitovat	k5eAaBmAgMnP	profitovat
paparazzi	paparazh	k1gMnPc1	paparazh
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
People	People	k1gFnSc2	People
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
4	[number]	k4	4
milióny	milión	k4xCgInPc1	milión
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
pro	pro	k7c4	pro
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
časopis	časopis	k1gInSc1	časopis
Hello	Hello	k1gNnSc1	Hello
<g/>
!	!	kIx.	!
</s>
<s>
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
3,5	[number]	k4	3,5
miliónu	milión	k4xCgInSc2	milión
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
pro	pro	k7c4	pro
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
Angelina	Angelin	k2eAgNnSc2d1	Angelino
a	a	k8xC	a
Brad	brada	k1gFnPc2	brada
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
fotografií	fotografia	k1gFnPc2	fotografia
získali	získat	k5eAaPmAgMnP	získat
<g/>
,	,	kIx,	,
věnovali	věnovat	k5eAaImAgMnP	věnovat
nadacím	nadace	k1gFnPc3	nadace
zachraňujícím	zachraňující	k2eAgMnSc6d1	zachraňující
africké	africký	k2eAgFnPc4d1	africká
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
druhého	druhý	k4xOgMnSc4	druhý
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
,	,	kIx,	,
tříletého	tříletý	k2eAgMnSc4d1	tříletý
Paxe	Pax	k1gMnSc4	Pax
Thiena	Thien	k1gMnSc4	Thien
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sirotčince	sirotčinec	k1gInSc2	sirotčinec
v	v	k7c4	v
Ho	on	k3xPp3gNnSc4	on
Či	či	k8xC	či
Minově	minově	k6eAd1	minově
Městě	město	k1gNnSc6	město
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Pham	Pham	k1gMnSc1	Pham
Quang	Quang	k1gMnSc1	Quang
Sang	Sang	k1gMnSc1	Sang
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
v	v	k7c4	v
Ho	on	k3xPp3gNnSc4	on
Či	či	k8xC	či
Minově	minově	k6eAd1	minově
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
byl	být	k5eAaImAgInS	být
opuštěn	opuštěn	k2eAgInSc1d1	opuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
Paxe	Paxe	k1gFnSc1	Paxe
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vietnamské	vietnamský	k2eAgInPc1d1	vietnamský
zákony	zákon	k1gInPc1	zákon
nepovolují	povolovat	k5eNaImIp3nP	povolovat
společnou	společný	k2eAgFnSc4d1	společná
adopci	adopce	k1gFnSc4	adopce
nesezdaným	sezdaný	k2eNgInPc3d1	nesezdaný
párům	pár	k1gInPc3	pár
<g/>
.	.	kIx.	.
</s>
<s>
Práva	práv	k2eAgNnPc1d1	právo
na	na	k7c4	na
první	první	k4xOgFnPc4	první
fotografie	fotografia	k1gFnPc4	fotografia
Paxe	Pax	k1gFnSc2	Pax
po	po	k7c6	po
adopci	adopce	k1gFnSc6	adopce
byly	být	k5eAaImAgFnP	být
opět	opět	k6eAd1	opět
prodány	prodat	k5eAaPmNgFnP	prodat
časopisům	časopis	k1gInPc3	časopis
People	People	k1gFnPc2	People
za	za	k7c2	za
2	[number]	k4	2
milióny	milión	k4xCgInPc4	milión
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
Hello	Hello	k1gNnSc4	Hello
<g/>
!	!	kIx.	!
</s>
<s>
za	za	k7c4	za
nezveřejněnou	zveřejněný	k2eNgFnSc4d1	nezveřejněná
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
Angelina	Angelin	k2eAgFnSc1d1	Angelina
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
oficiální	oficiální	k2eAgFnSc4d1	oficiální
změnu	změna	k1gFnSc4	změna
příjmení	příjmení	k1gNnSc2	příjmení
Paxe	Pax	k1gMnSc2	Pax
Thiena	Thien	k1gMnSc2	Thien
z	z	k7c2	z
Jolie	Jolie	k1gFnSc2	Jolie
na	na	k7c4	na
Jolie-Pitt	Jolie-Pitt	k1gInSc4	Jolie-Pitt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
oficiálně	oficiálně	k6eAd1	oficiálně
adoptoval	adoptovat	k5eAaPmAgInS	adoptovat
Paxe	Paxe	k1gInSc1	Paxe
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Bradem	Bradem	k?	Bradem
čeká	čekat	k5eAaImIp3nS	čekat
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c4	na
porod	porod	k1gInSc4	porod
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Nice	Nice	k1gFnSc6	Nice
<g/>
,	,	kIx,	,
reportéři	reportér	k1gMnPc1	reportér
tábořili	tábořit	k5eAaImAgMnP	tábořit
venku	venku	k6eAd1	venku
na	na	k7c6	na
promenádě	promenáda	k1gFnSc6	promenáda
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
Knoxe	Knoxe	k1gFnSc2	Knoxe
Léona	Léona	k1gFnSc1	Léona
a	a	k8xC	a
dcerku	dcerka	k1gFnSc4	dcerka
Vivienne	Vivienn	k1gInSc5	Vivienn
Marcheline	Marchelin	k1gInSc5	Marchelin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
fotografie	fotografia	k1gFnPc1	fotografia
dětí	dítě	k1gFnPc2	dítě
byly	být	k5eAaImAgFnP	být
prodány	prodán	k2eAgFnPc1d1	prodána
časopisům	časopis	k1gInPc3	časopis
People	People	k1gFnPc4	People
and	and	k?	and
Hello	Hello	k1gNnSc1	Hello
<g/>
!	!	kIx.	!
</s>
<s>
celkem	celkem	k6eAd1	celkem
za	za	k7c4	za
14	[number]	k4	14
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Zisk	zisk	k1gInSc1	zisk
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaImNgInS	věnovat
společně	společně	k6eAd1	společně
založené	založený	k2eAgFnSc3d1	založená
nadaci	nadace	k1gFnSc3	nadace
Jolie-Pitt	Jolie-Pitt	k2eAgInSc1d1	Jolie-Pitt
Foundation	Foundation	k1gInSc1	Foundation
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
Angelina	Angelin	k2eAgFnSc1d1	Angelina
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
svěření	svěření	k1gNnSc4	svěření
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
výlučné	výlučný	k2eAgFnSc2d1	výlučná
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
nechala	nechat	k5eAaPmAgFnS	nechat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prevence	prevence	k1gFnSc2	prevence
rakoviny	rakovina	k1gFnSc2	rakovina
odstranit	odstranit	k5eAaPmF	odstranit
obě	dva	k4xCgNnPc1	dva
prsa	prsa	k1gNnPc1	prsa
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c4	po
sdělení	sdělení	k1gNnPc4	sdělení
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
87	[number]	k4	87
<g/>
%	%	kIx~	%
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
prsu	prs	k1gInSc2	prs
kvůli	kvůli	k7c3	kvůli
mutaci	mutace	k1gFnSc3	mutace
genu	gen	k1gInSc2	gen
BRCA	BRCA	kA	BRCA
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Rakovinou	rakovina	k1gFnSc7	rakovina
prsu	prs	k1gInSc2	prs
i	i	k8xC	i
vaječníků	vaječník	k1gInPc2	vaječník
trpěla	trpět	k5eAaImAgFnS	trpět
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
56	[number]	k4	56
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rakovina	rakovina	k1gFnSc1	rakovina
vaječníků	vaječník	k1gInPc2	vaječník
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
její	její	k3xOp3gFnSc2	její
babičky	babička	k1gFnSc2	babička
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
45	[number]	k4	45
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jolie	Jolie	k1gFnSc1	Jolie
si	se	k3xPyFc3	se
proto	proto	k6eAd1	proto
nechala	nechat	k5eAaPmAgFnS	nechat
operativně	operativně	k6eAd1	operativně
odstranit	odstranit	k5eAaPmF	odstranit
i	i	k9	i
vaječníky	vaječník	k1gInPc1	vaječník
<g/>
;	;	kIx,	;
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
u	u	k7c2	u
ní	on	k3xPp3gFnSc7	on
lékaři	lékař	k1gMnPc1	lékař
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
na	na	k7c4	na
50	[number]	k4	50
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
oznámení	oznámení	k1gNnSc6	oznámení
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
teta	teta	k1gFnSc1	teta
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
Jolie	Jolie	k1gFnSc2	Jolie
Debbie	Debbie	k1gFnSc1	Debbie
Martinová	Martinová	k1gFnSc1	Martinová
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
hereččiny	hereččin	k2eAgFnSc2d1	hereččina
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jí	on	k3xPp3gFnSc3	on
61	[number]	k4	61
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
rakovina	rakovina	k1gFnSc1	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
osobně	osobně	k6eAd1	osobně
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
rozsah	rozsah	k1gInSc4	rozsah
světové	světový	k2eAgFnSc2d1	světová
humanitární	humanitární	k2eAgFnSc2d1	humanitární
krize	krize	k1gFnSc2	krize
během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
Lara	Lara	k1gMnSc1	Lara
Croft	Croft	k1gMnSc1	Croft
–	–	k?	–
Tomb	Tomb	k1gMnSc1	Tomb
Raider	Raider	k1gMnSc1	Raider
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
v	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Kontaktovala	kontaktovat	k5eAaImAgFnS	kontaktovat
Úřad	úřad	k1gInSc4	úřad
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
(	(	kIx(	(
<g/>
UNHCR	UNHCR	kA	UNHCR
<g/>
)	)	kIx)	)
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
problémových	problémový	k2eAgFnPc6d1	problémová
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
poznala	poznat	k5eAaPmAgFnS	poznat
podmínky	podmínka	k1gFnPc4	podmínka
na	na	k7c6	na
postižených	postižený	k2eAgNnPc6d1	postižené
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
uprchlické	uprchlický	k2eAgInPc4d1	uprchlický
tábory	tábor	k1gInPc4	tábor
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2001	[number]	k4	2001
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
misi	mise	k1gFnSc4	mise
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
-denní	enní	k2eAgFnSc4d1	-denní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Sierry	Sierra	k1gFnSc2	Sierra
Leone	Leo	k1gMnSc5	Leo
a	a	k8xC	a
Tanzanie	Tanzanie	k1gFnPc4	Tanzanie
<g/>
;	;	kIx,	;
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
zděšení	zděšení	k1gNnSc4	zděšení
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
na	na	k7c4	na
14	[number]	k4	14
dní	den	k1gInPc2	den
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Kambodži	Kambodža	k1gFnSc6	Kambodža
a	a	k8xC	a
navštívila	navštívit	k5eAaPmAgFnS	navštívit
afghánské	afghánský	k2eAgMnPc4d1	afghánský
uprchlíky	uprchlík	k1gMnPc4	uprchlík
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Hradila	hradit	k5eAaImAgFnS	hradit
si	se	k3xPyFc3	se
sama	sám	k3xTgFnSc1	sám
veškeré	veškerý	k3xTgInPc4	veškerý
výdaje	výdaj	k1gInPc4	výdaj
a	a	k8xC	a
sdílela	sdílet	k5eAaImAgFnS	sdílet
stejné	stejný	k2eAgFnPc4d1	stejná
polní	polní	k2eAgFnPc4d1	polní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
další	další	k2eAgMnPc1d1	další
pracovníci	pracovník	k1gMnPc1	pracovník
UNHCR	UNHCR	kA	UNHCR
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
Angelina	Angelin	k2eAgFnSc1d1	Angelina
na	na	k7c6	na
ústředí	ústředí	k1gNnSc6	ústředí
UNHCR	UNHCR	kA	UNHCR
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
oficiálně	oficiálně	k6eAd1	oficiálně
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
Vyslankyní	vyslankyně	k1gFnSc7	vyslankyně
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
při	při	k7c6	při
UNHCR	UNHCR	kA	UNHCR
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
četné	četný	k2eAgFnPc4d1	četná
humanitární	humanitární	k2eAgFnPc4d1	humanitární
mise	mise	k1gFnPc4	mise
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Setkala	setkat	k5eAaPmAgFnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
uprchlíky	uprchlík	k1gMnPc7	uprchlík
a	a	k8xC	a
vyslídlenci	vyslídlence	k1gFnSc4	vyslídlence
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
čeho	co	k3yInSc2	co
hodlá	hodlat	k5eAaImIp3nS	hodlat
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vědomí	vědomí	k1gNnSc1	vědomí
o	o	k7c4	o
situaci	situace	k1gFnSc4	situace
těchto	tento	k3xDgMnPc2	tento
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
mělo	mít	k5eAaImAgNnS	mít
dostat	dostat	k5eAaPmF	dostat
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
prošli	projít	k5eAaPmAgMnP	projít
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
přehlíženi	přehlížet	k5eAaImNgMnP	přehlížet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
zapomenuté	zapomenutý	k2eAgFnSc2d1	zapomenutá
krize	krize	k1gFnSc2	krize
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
pozornost	pozornost	k1gFnSc1	pozornost
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebojí	bát	k5eNaImIp3nP	bát
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
navštívila	navštívit	k5eAaPmAgFnS	navštívit
súdánský	súdánský	k2eAgInSc4d1	súdánský
Darfúr	Darfúr	k1gInSc4	Darfúr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zuřila	zuřit	k5eAaImAgFnS	zuřit
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
hladomor	hladomor	k1gInSc1	hladomor
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Čad	Čad	k1gInSc4	Čad
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tamní	tamní	k2eAgFnSc2d1	tamní
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
až	až	k9	až
2009	[number]	k4	2009
opakovaně	opakovaně	k6eAd1	opakovaně
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Irák	Irák	k1gInSc4	Irák
<g/>
;	;	kIx,	;
taktež	taktež	k6eAd1	taktež
opakovaně	opakovaně	k6eAd1	opakovaně
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2008	[number]	k4	2008
až	až	k8xS	až
2011	[number]	k4	2011
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
<g/>
;	;	kIx,	;
navštívila	navštívit	k5eAaPmAgFnS	navštívit
i	i	k9	i
Libyi	Libye	k1gFnSc4	Libye
během	během	k7c2	během
tamní	tamní	k2eAgFnSc2d1	tamní
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
Vyslankyně	vyslankyně	k1gFnSc2	vyslankyně
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
byla	být	k5eAaImAgFnS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
Speciální	speciální	k2eAgFnSc7d1	speciální
vyslankyní	vyslankyně	k1gFnSc7	vyslankyně
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
UNHCR	UNHCR	kA	UNHCR
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
Speciální	speciální	k2eAgFnSc1d1	speciální
vyslankyně	vyslankyně	k1gFnSc1	vyslankyně
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
UNHCR	UNHCR	kA	UNHCR
a	a	k8xC	a
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
(	(	kIx(	(
<g/>
António	António	k1gMnSc1	António
Guterres	Guterres	k1gMnSc1	Guterres
<g/>
)	)	kIx)	)
na	na	k7c6	na
diplomatické	diplomatický	k2eAgFnSc6d1	diplomatická
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
projektech	projekt	k1gInPc6	projekt
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
řešení	řešení	k1gNnSc2	řešení
pro	pro	k7c4	pro
velké	velká	k1gFnPc4	velká
humanitární	humanitární	k2eAgFnSc2d1	humanitární
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
či	či	k8xC	či
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
pozice	pozice	k1gFnSc1	pozice
odrážející	odrážející	k2eAgFnSc4d1	odrážející
vynikající	vynikající	k2eAgFnSc4d1	vynikající
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
odvádí	odvádět	k5eAaImIp3nS	odvádět
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
mluvčí	mluvčí	k1gMnSc1	mluvčí
UNHCR	UNHCR	kA	UNHCR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
pro	pro	k7c4	pro
UNHCR	UNHCR	kA	UNHCR
využívá	využívat	k5eAaPmIp3nS	využívat
svoji	svůj	k3xOyFgFnSc4	svůj
popularitu	popularita	k1gFnSc4	popularita
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upozornila	upozornit	k5eAaPmAgFnS	upozornit
na	na	k7c4	na
humanitární	humanitární	k2eAgNnSc4d1	humanitární
krize	krize	k1gFnSc1	krize
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
rané	raný	k2eAgFnPc1d1	raná
mise	mise	k1gFnPc1	mise
jsou	být	k5eAaImIp3nP	být
zdokumentovány	zdokumentovat	k5eAaPmNgFnP	zdokumentovat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Notes	notes	k1gInSc1	notes
from	from	k6eAd1	from
My	my	k3xPp1nPc1	my
Travels	Travelsa	k1gFnPc2	Travelsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
současně	současně	k6eAd1	současně
s	s	k7c7	s
premiérou	premiéra	k1gFnSc7	premiéra
jejího	její	k3xOp3gInSc2	její
filmu	film	k1gInSc2	film
Hranice	hranice	k1gFnSc2	hranice
zlomu	zlom	k1gInSc2	zlom
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
MTV	MTV	kA	MTV
uvedla	uvést	k5eAaPmAgFnS	uvést
film	film	k1gInSc4	film
The	The	k1gFnSc2	The
Diary	Diara	k1gFnSc2	Diara
of	of	k?	of
Angelina	Angelin	k2eAgFnSc1d1	Angelina
Jolie	Jolie	k1gFnSc1	Jolie
&	&	k?	&
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jeffrey	Jeffrea	k1gMnSc2	Jeffrea
Sachs	Sachs	k1gInSc1	Sachs
in	in	k?	in
Africa	Afric	k1gInSc2	Afric
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokumentoval	dokumentovat	k5eAaBmAgInS	dokumentovat
cestu	cesta	k1gFnSc4	cesta
Angeliny	Angelin	k2eAgFnSc2d1	Angelina
a	a	k8xC	a
známého	známý	k1gMnSc2	známý
ekonoma	ekonom	k1gMnSc2	ekonom
Jeffreyho	Jeffrey	k1gMnSc2	Jeffrey
Sachse	Sachs	k1gMnSc2	Sachs
do	do	k7c2	do
odloučených	odloučený	k2eAgFnPc2d1	odloučená
vesnic	vesnice	k1gFnPc2	vesnice
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Keni	Keňa	k1gFnSc6	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Angelina	Angelin	k2eAgFnSc1d1	Angelina
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
propaguje	propagovat	k5eAaImIp3nS	propagovat
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
uprchlíků	uprchlík	k1gMnPc2	uprchlík
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
času	čas	k1gInSc2	čas
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
propagovala	propagovat	k5eAaImAgFnS	propagovat
humanitární	humanitární	k2eAgFnSc1d1	humanitární
krize	krize	k1gFnSc1	krize
i	i	k9	i
na	na	k7c6	na
politické	politický	k2eAgFnSc6d1	politická
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
akce	akce	k1gFnPc4	akce
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Světového	světový	k2eAgInSc2d1	světový
dne	den	k1gInSc2	den
uprchlíků	uprchlík	k1gMnPc2	uprchlík
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc5	D.C.
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pozvána	pozvat	k5eAaPmNgFnS	pozvat
jako	jako	k9	jako
řečník	řečník	k1gMnSc1	řečník
na	na	k7c4	na
Světové	světový	k2eAgNnSc4d1	světové
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
forum	forum	k1gNnSc4	forum
v	v	k7c6	v
Davosu	Davos	k1gInSc6	Davos
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
lobbuje	lobbovat	k5eAaImIp3nS	lobbovat
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
humanitárních	humanitární	k2eAgFnPc2d1	humanitární
organizací	organizace	k1gFnPc2	organizace
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
například	například	k6eAd1	například
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2003	[number]	k4	2003
a	a	k8xC	a
2006	[number]	k4	2006
nejméně	málo	k6eAd3	málo
20	[number]	k4	20
<g/>
x	x	k?	x
hovořila	hovořit	k5eAaImAgFnS	hovořit
se	s	k7c7	s
senátory	senátor	k1gMnPc7	senátor
i	i	k8xC	i
kongresmany	kongresman	k1gMnPc7	kongresman
a	a	k8xC	a
zasadila	zasadit	k5eAaPmAgFnS	zasadit
se	se	k3xPyFc4	se
za	za	k7c2	za
schválení	schválení	k1gNnSc2	schválení
několika	několik	k4yIc2	několik
dotací	dotace	k1gFnPc2	dotace
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
a	a	k8xC	a
ohroženým	ohrožený	k2eAgFnPc3d1	ohrožená
dětem	dítě	k1gFnPc3	dítě
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
světě	svět	k1gInSc6	svět
a	a	k8xC	a
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
řekla	říct	k5eAaPmAgFnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přestože	přestože	k8xS	přestože
bych	by	kYmCp1nS	by
dala	dát	k5eAaPmAgFnS	dát
přednost	přednost	k1gFnSc4	přednost
tomu	ten	k3xDgNnSc3	ten
Washington	Washington	k1gInSc1	Washington
nikdy	nikdy	k6eAd1	nikdy
nenavštívit	navštívit	k5eNaPmF	navštívit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
něco	něco	k3yInSc4	něco
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členkou	členka	k1gFnSc7	členka
americké	americký	k2eAgFnSc2d1	americká
neziskové	ziskový	k2eNgFnSc2d1	nezisková
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
(	(	kIx(	(
<g/>
Council	Council	k1gInSc4	Council
on	on	k3xPp3gMnSc1	on
Foreign	Foreign	k1gMnSc1	Foreign
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
vzniku	vznik	k1gInSc2	vznik
několika	několik	k4yIc2	několik
charitativních	charitativní	k2eAgFnPc2d1	charitativní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
založila	založit	k5eAaPmAgFnS	založit
nadaci	nadace	k1gFnSc4	nadace
Maddox	Maddox	k1gInSc1	Maddox
Jolie-Pitt	Jolie-Pitt	k1gMnSc1	Jolie-Pitt
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Maddox	Maddox	k1gInSc1	Maddox
Jolie	Jolie	k1gFnSc1	Jolie
Project	Project	k1gMnSc1	Project
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
rozvoj	rozvoj	k1gInSc4	rozvoj
komunit	komunita	k1gFnPc2	komunita
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
kambodžské	kambodžský	k2eAgFnSc6d1	kambodžská
provincii	provincie	k1gFnSc6	provincie
Bát	bát	k5eAaImF	bát
Dambang	Dambang	k1gInSc4	Dambang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
organizací	organizace	k1gFnSc7	organizace
Global	globat	k5eAaImAgMnS	globat
Health	Health	k1gMnSc1	Health
Committee	Committee	k1gNnSc1	Committee
založila	založit	k5eAaPmAgFnS	založit
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Kambodži	Kambodža	k1gFnSc6	Kambodža
Phnom	Phnom	k1gInSc1	Phnom
Penhu	Penh	k1gInSc6	Penh
zařízení	zařízení	k1gNnSc1	zařízení
Maddox	Maddox	k1gInSc1	Maddox
Chivan	Chivan	k1gMnSc1	Chivan
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
péči	péče	k1gFnSc4	péče
HIV	HIV	kA	HIV
nakaženým	nakažený	k2eAgFnPc3d1	nakažená
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
Angelina	Angelin	k2eAgFnSc1d1	Angelina
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
životním	životní	k2eAgMnSc7d1	životní
partnerem	partner	k1gMnSc7	partner
Bradem	Bradem	k?	Bradem
Pittem	Pitt	k1gMnSc7	Pitt
založila	založit	k5eAaPmAgFnS	založit
nadaci	nadace	k1gFnSc4	nadace
Jolie-Pitt	Jolie-Pitt	k2eAgInSc4d1	Jolie-Pitt
Foundation	Foundation	k1gInSc4	Foundation
poskytující	poskytující	k2eAgInSc4d1	poskytující
humanitární	humanitární	k2eAgFnSc4d1	humanitární
podporu	podpora	k1gFnSc4	podpora
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Angelina	Angelin	k2eAgFnSc1d1	Angelina
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ekonomem	ekonom	k1gMnSc7	ekonom
Gene	gen	k1gInSc5	gen
Sperlingem	Sperling	k1gInSc7	Sperling
založila	založit	k5eAaPmAgFnS	založit
organizaci	organizace	k1gFnSc4	organizace
Education	Education	k1gInSc1	Education
Partnership	Partnership	k1gMnSc1	Partnership
for	forum	k1gNnPc2	forum
Children	Childrno	k1gNnPc2	Childrno
of	of	k?	of
Conflict	Conflict	k1gMnSc1	Conflict
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dotuje	dotovat	k5eAaBmIp3nS	dotovat
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
programy	program	k1gInPc4	program
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
zasažené	zasažený	k2eAgFnPc4d1	zasažená
přírodními	přírodní	k2eAgInPc7d1	přírodní
nebo	nebo	k8xC	nebo
člověkem	člověk	k1gMnSc7	člověk
způsobenými	způsobený	k2eAgFnPc7d1	způsobená
katastrofami	katastrofa	k1gFnPc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
na	na	k7c6	na
ustavení	ustavení	k1gNnSc6	ustavení
organizace	organizace	k1gFnSc2	organizace
Kids	Kids	k1gInSc4	Kids
in	in	k?	in
Need	Need	k1gInSc1	Need
of	of	k?	of
Defense	defense	k1gFnSc1	defense
<g/>
,	,	kIx,	,
sdružení	sdružení	k1gNnSc1	sdružení
advokátních	advokátní	k2eAgFnPc2d1	advokátní
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
firemních	firemní	k2eAgFnPc2d1	firemní
právnických	právnický	k2eAgFnPc2d1	právnická
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
,	,	kIx,	,
nevládních	vládní	k2eNgFnPc2d1	nevládní
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
zdarma	zdarma	k6eAd1	zdarma
právnické	právnický	k2eAgFnPc1d1	právnická
služby	služba	k1gFnPc1	služba
osamělým	osamělý	k2eAgFnPc3d1	osamělá
dětem	dítě	k1gFnPc3	dítě
<g/>
–	–	k?	–
<g/>
imigrantům	imigrant	k1gMnPc3	imigrant
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
založila	založit	k5eAaPmAgFnS	založit
projekt	projekt	k1gInSc4	projekt
Jolie	Jolie	k1gFnSc1	Jolie
Legal	Legal	k1gInSc1	Legal
Fellows	Fellows	k1gInSc1	Fellows
Programme	Programme	k1gFnSc1	Programme
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yRgInSc2	který
právníci	právník	k1gMnPc1	právník
podporují	podporovat	k5eAaImIp3nP	podporovat
vladní	vladní	k2eAgFnPc4d1	vladní
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
humanitární	humanitární	k2eAgFnSc4d1	humanitární
činnost	činnost	k1gFnSc4	činnost
si	se	k3xPyFc3	se
vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
široké	široký	k2eAgNnSc4d1	široké
uznání	uznání	k1gNnSc4	uznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
Humanitarian	Humanitariana	k1gFnPc2	Humanitariana
Award	Award	k1gInSc4	Award
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
Church	Church	k1gMnSc1	Church
World	World	k1gMnSc1	World
Service	Service	k1gFnSc1	Service
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
laureátkou	laureátka	k1gFnSc7	laureátka
ceny	cena	k1gFnSc2	cena
Citizen	Citizen	kA	Citizen
of	of	k?	of
the	the	k?	the
World	Worldo	k1gNnPc2	Worldo
Award	Award	k1gInSc1	Award
udělované	udělovaný	k2eAgFnSc2d1	udělovaná
organizací	organizace	k1gFnSc7	organizace
United	United	k1gInSc4	United
Nations	Nations	k1gInSc1	Nations
Correspondents	Correspondents	k1gInSc1	Correspondents
Association	Association	k1gInSc1	Association
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
obdržela	obdržet	k5eAaPmAgFnS	obdržet
cenu	cena	k1gFnSc4	cena
Global	globat	k5eAaImAgMnS	globat
Humanitarian	Humanitarian	k1gMnSc1	Humanitarian
Award	Award	k1gMnSc1	Award
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
Association	Association	k1gInSc1	Association
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
jí	jíst	k5eAaImIp3nS	jíst
král	král	k1gMnSc1	král
Norodom	Norodom	k1gInSc1	Norodom
Sihamoni	Sihamoň	k1gFnSc3	Sihamoň
udělil	udělit	k5eAaPmAgInS	udělit
kambodžské	kambodžský	k2eAgNnSc4d1	Kambodžské
občanství	občanství	k1gNnSc4	občanství
za	za	k7c4	za
humanitární	humanitární	k2eAgFnSc4d1	humanitární
činnost	činnost	k1gFnSc4	činnost
ve	v	k7c6	v
propěch	propě	k1gFnPc6	propě
jeho	jeho	k3xOp3gFnSc2	jeho
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
obdržela	obdržet	k5eAaPmAgFnS	obdržet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
úřadujícím	úřadující	k2eAgMnSc7d1	úřadující
Vysokým	vysoký	k2eAgMnSc7d1	vysoký
komisařem	komisař	k1gMnSc7	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
António	António	k6eAd1	António
Guterresem	Guterres	k1gInSc7	Guterres
prestižní	prestižní	k2eAgFnSc4d1	prestižní
Cenu	cena	k1gFnSc4	cena
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
Freedom	Freedom	k1gInSc1	Freedom
Award	Awarda	k1gFnPc2	Awarda
<g/>
)	)	kIx)	)
od	od	k7c2	od
organizace	organizace	k1gFnSc2	organizace
International	International	k1gMnSc2	International
Rescue	Rescu	k1gMnSc2	Rescu
Committee	Committe	k1gMnSc2	Committe
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
laureáty	laureát	k1gMnPc4	laureát
této	tento	k3xDgFnSc2	tento
ceny	cena	k1gFnSc2	cena
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
obdržela	obdržet	k5eAaPmAgFnS	obdržet
od	od	k7c2	od
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
UNHCR	UNHCR	kA	UNHCR
Antónia	Antónium	k1gNnSc2	Antónium
Guterrese	Guterrese	k1gFnSc2	Guterrese
zlatou	zlatý	k2eAgFnSc4d1	zlatá
jehlici	jehlice	k1gFnSc4	jehlice
určenou	určený	k2eAgFnSc4d1	určená
nejdéle	dlouho	k6eAd3	dlouho
sloužícím	sloužící	k2eAgMnPc3d1	sloužící
pracovníkům	pracovník	k1gMnPc3	pracovník
UNHCR	UNHCR	kA	UNHCR
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
práce	práce	k1gFnSc2	práce
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
Vyslankyně	vyslankyně	k1gFnSc2	vyslankyně
dobré	dobrý	k2eAgFnSc2d1	dobrá
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
povýšena	povýšit	k5eAaPmNgFnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
