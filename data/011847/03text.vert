<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
je	být	k5eAaImIp3nS	být
pořad	pořad	k1gInSc4	pořad
z	z	k7c2	z
produkce	produkce	k1gFnSc2	produkce
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
je	být	k5eAaImIp3nS	být
dvanáctidílný	dvanáctidílný	k2eAgInSc4d1	dvanáctidílný
cyklus	cyklus	k1gInSc4	cyklus
(	(	kIx(	(
<g/>
dokument	dokument	k1gInSc4	dokument
propojený	propojený	k2eAgInSc4d1	propojený
s	s	k7c7	s
reality	realita	k1gFnSc2	realita
show	show	k1gNnSc4	show
<g/>
)	)	kIx)	)
o	o	k7c6	o
životech	život	k1gInPc6	život
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
probléch	problé	k1gFnPc6	problé
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
,	,	kIx,	,
penězi	peníze	k1gInPc7	peníze
či	či	k8xC	či
je	on	k3xPp3gFnPc4	on
postihly	postihnout	k5eAaPmAgInP	postihnout
jiné	jiný	k2eAgInPc1d1	jiný
závažné	závažný	k2eAgInPc1d1	závažný
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
změnách	změna	k1gFnPc6	změna
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
podstoupili	podstoupit	k5eAaPmAgMnP	podstoupit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pořadu	pořad	k1gInSc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
že	že	k8xS	že
přijali	přijmout	k5eAaPmAgMnP	přijmout
pomoc	pomoc	k1gFnSc4	pomoc
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
povolili	povolit	k5eAaPmAgMnP	povolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
představen	představit	k5eAaPmNgInS	představit
divákům	divák	k1gMnPc3	divák
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
účastník	účastník	k1gMnSc1	účastník
poté	poté	k6eAd1	poté
prodělal	prodělat	k5eAaPmAgMnS	prodělat
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
vedli	vést	k5eAaImAgMnP	vést
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
jeho	jeho	k3xOp3gFnSc2	jeho
životní	životní	k2eAgFnSc2d1	životní
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
například	například	k6eAd1	například
se	s	k7c7	s
zaplacením	zaplacení	k1gNnSc7	zaplacení
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
plastické	plastický	k2eAgFnSc2d1	plastická
operace	operace	k1gFnSc2	operace
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
MF	MF	kA	MF
DNES	dnes	k6eAd1	dnes
tento	tento	k3xDgInSc4	tento
televizní	televizní	k2eAgInSc4d1	televizní
pořad	pořad	k1gInSc4	pořad
hodnotilo	hodnotit	k5eAaImAgNnS	hodnotit
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
Novy	nova	k1gFnSc2	nova
léčí	léčit	k5eAaImIp3nP	léčit
těžké	těžký	k2eAgInPc1d1	těžký
životy	život	k1gInPc1	život
plastikou	plastika	k1gFnSc7	plastika
a	a	k8xC	a
kadeřníkem	kadeřník	k1gMnSc7	kadeřník
recenze	recenze	k1gFnSc2	recenze
pořadu	pořad	k1gInSc2	pořad
na	na	k7c4	na
idnes	idnes	k1gInSc4	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Mirka	Mirka	k1gFnSc1	Mirka
Spáčilová	Spáčilová	k1gFnSc1	Spáčilová
</s>
</p>
<p>
<s>
Nova	nova	k1gFnSc1	nova
rozdává	rozdávat	k5eAaImIp3nS	rozdávat
Druhou	druhý	k4xOgFnSc4	druhý
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
<g/>
Nový	nový	k2eAgInSc4d1	nový
tv	tv	k?	tv
pořad	pořad	k1gInSc1	pořad
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
lidem	člověk	k1gMnPc3	člověk
zvednout	zvednout	k5eAaPmF	zvednout
se	se	k3xPyFc4	se
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
blesk	blesk	k1gInSc1	blesk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Saša	Saša	k1gFnSc1	Saša
Šeflová	Šeflová	k1gFnSc1	Šeflová
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
-	-	kIx~	-
Vrací	vracet	k5eAaImIp3nS	vracet
lidem	člověk	k1gMnPc3	člověk
radost	radost	k1gFnSc1	radost
-	-	kIx~	-
Šíp	šíp	k1gInSc1	šíp
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
TV	TV	kA	TV
program	program	k1gInSc1	program
-	-	kIx~	-
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
-	-	kIx~	-
shrnutí	shrnutí	k1gNnSc4	shrnutí
obsahu	obsah	k1gInSc2	obsah
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc4d1	nový
původní	původní	k2eAgInSc4d1	původní
pořad	pořad	k1gInSc4	pořad
televize	televize	k1gFnSc2	televize
Nova	nova	k1gFnSc1	nova
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
vzhůru	vzhůru	k6eAd1	vzhůru
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
propagační	propagační	k2eAgFnSc1d1	propagační
znělka	znělka	k1gFnSc1	znělka
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
díly	díl	k1gInPc4	díl
<g/>
,	,	kIx,	,
archiv	archiv	k1gInSc1	archiv
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nova	nova	k1gFnSc1	nova
přichází	přicházet	k5eAaImIp3nS	přicházet
s	s	k7c7	s
Druhou	druhý	k4xOgFnSc7	druhý
šancí	šance	k1gFnSc7	šance
denik	denik	k1gMnSc1	denik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
startuje	startovat	k5eAaBmIp3nS	startovat
na	na	k7c4	na
Nově	nově	k6eAd1	nově
už	už	k6eAd1	už
na	na	k7c6	na
konci	konec	k1gInSc6	konec
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
příběhů	příběh	k1gInPc2	příběh
ze	z	k7c2	z
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
mediashow	mediashow	k?	mediashow
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Nova	nova	k1gFnSc1	nova
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
lidské	lidský	k2eAgNnSc4d1	lidské
dno	dno	k1gNnSc4	dno
i	i	k8xC	i
nový	nový	k2eAgInSc4d1	nový
Comeback	Comeback	k1gInSc4	Comeback
od	od	k7c2	od
odstavce	odstavec	k1gInSc2	odstavec
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
nabízí	nabízet	k5eAaImIp3nS	nabízet
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bleskove	bleskov	k1gInSc5	bleskov
<g/>
.	.	kIx.	.
<g/>
aktualne	aktualnout	k5eAaPmIp3nS	aktualnout
<g/>
.	.	kIx.	.
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Jitka	Jitka	k1gFnSc1	Jitka
Gráfová	Gráfový	k2eAgFnSc1d1	Gráfová
</s>
</p>
