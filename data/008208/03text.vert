<p>
<s>
Kandela	kandela	k1gFnSc1	kandela
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
cd	cd	kA	cd
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
název	název	k1gInSc1	název
jednotky	jednotka	k1gFnSc2	jednotka
je	být	k5eAaImIp3nS	být
candela	candela	k1gFnSc1	candela
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
svítivosti	svítivost	k1gFnSc2	svítivost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
definice	definice	k1gFnPc1	definice
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
Kandela	kandela	k1gFnSc1	kandela
je	být	k5eAaImIp3nS	být
svítivost	svítivost	k1gFnSc4	svítivost
světelného	světelný	k2eAgInSc2d1	světelný
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
směru	směr	k1gInSc6	směr
emituje	emitovat	k5eAaBmIp3nS	emitovat
(	(	kIx(	(
<g/>
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
)	)	kIx)	)
monochromatické	monochromatický	k2eAgNnSc1d1	monochromatické
záření	záření	k1gNnSc1	záření
o	o	k7c6	o
frekvenci	frekvence	k1gFnSc6	frekvence
540	[number]	k4	540
<g/>
×	×	k?	×
<g/>
1012	[number]	k4	1012
hertzů	hertz	k1gInPc2	hertz
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
zářivost	zářivost	k1gFnSc1	zářivost
(	(	kIx(	(
<g/>
zářivá	zářivý	k2eAgFnSc1d1	zářivá
intenzita	intenzita	k1gFnSc1	intenzita
<g/>
)	)	kIx)	)
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
683	[number]	k4	683
wattů	watt	k1gInPc2	watt
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
steradián	steradián	k1gInSc4	steradián
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komentář	komentář	k1gInSc1	komentář
definice	definice	k1gFnSc2	definice
===	===	k?	===
</s>
</p>
<p>
<s>
Svítivost	svítivost	k1gFnSc1	svítivost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
fotometrických	fotometrický	k2eAgFnPc2d1	fotometrická
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
respektují	respektovat	k5eAaImIp3nP	respektovat
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
citlivost	citlivost	k1gFnSc4	citlivost
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
vlnovým	vlnový	k2eAgFnPc3d1	vlnová
délkám	délka	k1gFnPc3	délka
světelného	světelný	k2eAgNnSc2d1	světelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
definici	definice	k1gFnSc6	definice
zvolená	zvolený	k2eAgFnSc1d1	zvolená
frekvence	frekvence	k1gFnSc1	frekvence
540	[number]	k4	540
THz	THz	k1gMnSc1	THz
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
uprostřed	uprostřed	k7c2	uprostřed
oblasti	oblast	k1gFnSc2	oblast
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
blízká	blízký	k2eAgFnSc1d1	blízká
světlu	světlo	k1gNnSc3	světlo
zelené	zelený	k2eAgFnSc2d1	zelená
barvy	barva	k1gFnSc2	barva
při	při	k7c6	při
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
555	[number]	k4	555
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
při	při	k7c6	při
denním	denní	k2eAgNnSc6d1	denní
vidění	vidění	k1gNnSc6	vidění
nejcitlivější	citlivý	k2eAgInSc1d3	nejcitlivější
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
nám	my	k3xPp1nPc3	my
tedy	tedy	k9	tedy
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
monochromatické	monochromatický	k2eAgNnSc4d1	monochromatické
světlo	světlo	k1gNnSc4	světlo
platí	platit	k5eAaImIp3nS	platit
přepočet	přepočet	k1gInSc1	přepočet
<g/>
,	,	kIx,	,
že	že	k8xS	že
1	[number]	k4	1
cd	cd	kA	cd
=	=	kIx~	=
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
683	[number]	k4	683
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
sr	sr	k?	sr
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
ještě	ještě	k9	ještě
jednodušeji	jednoduše	k6eAd2	jednoduše
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
pro	pro	k7c4	pro
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
1	[number]	k4	1
W	W	kA	W
=	=	kIx~	=
683	[number]	k4	683
lm	lm	k?	lm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
jiné	jiný	k2eAgFnPc4d1	jiná
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
pak	pak	k6eAd1	pak
tento	tento	k3xDgInSc1	tento
koeficient	koeficient	k1gInSc1	koeficient
683	[number]	k4	683
lm	lm	k?	lm
<g/>
/	/	kIx~	/
<g/>
W	W	kA	W
musíme	muset	k5eAaImIp1nP	muset
vynásobit	vynásobit	k5eAaPmF	vynásobit
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
hodnotou	hodnota	k1gFnSc7	hodnota
podle	podle	k7c2	podle
křivky	křivka	k1gFnSc2	křivka
poměrné	poměrný	k2eAgFnSc2d1	poměrná
světelné	světelný	k2eAgFnSc2d1	světelná
účinnosti	účinnost	k1gFnSc2	účinnost
(	(	kIx(	(
<g/>
křivky	křivka	k1gFnSc2	křivka
spektrální	spektrální	k2eAgFnSc2d1	spektrální
citlivosti	citlivost	k1gFnSc3	citlivost
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
fotometrických	fotometrický	k2eAgInPc2d1	fotometrický
výpočtů	výpočet	k1gInPc2	výpočet
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
standardní	standardní	k2eAgFnSc1d1	standardní
definice	definice	k1gFnSc1	definice
křivky	křivka	k1gFnSc2	křivka
poměrné	poměrný	k2eAgFnSc2d1	poměrná
světelné	světelný	k2eAgFnSc2d1	světelná
účinnosti	účinnost	k1gFnSc2	účinnost
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
záření	záření	k1gNnSc2	záření
stejně	stejně	k6eAd1	stejně
důležitá	důležitý	k2eAgFnSc1d1	důležitá
jako	jako	k8xS	jako
definiční	definiční	k2eAgInSc1d1	definiční
koeficient	koeficient	k1gInSc1	koeficient
683	[number]	k4	683
lm	lm	k?	lm
<g/>
/	/	kIx~	/
<g/>
W.	W.	kA	W.
</s>
</p>
<p>
<s>
==	==	k?	==
Starší	starý	k2eAgFnPc1d2	starší
definice	definice	k1gFnPc1	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xS	jako
svítivost	svítivost	k1gFnSc1	svítivost
svíčky	svíčka	k1gFnSc2	svíčka
definovaného	definovaný	k2eAgNnSc2d1	definované
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Typů	typ	k1gInPc2	typ
referenčních	referenční	k2eAgFnPc2d1	referenční
svíček	svíčka	k1gFnPc2	svíčka
však	však	k9	však
existovalo	existovat	k5eAaImAgNnS	existovat
několik	několik	k4yIc4	několik
(	(	kIx(	(
<g/>
a	a	k8xC	a
tomu	ten	k3xDgNnSc3	ten
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
několik	několik	k4yIc1	několik
mírně	mírně	k6eAd1	mírně
různých	různý	k2eAgFnPc2d1	různá
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
složité	složitý	k2eAgNnSc1d1	složité
zachovat	zachovat	k5eAaPmF	zachovat
přesně	přesně	k6eAd1	přesně
stejné	stejný	k2eAgFnPc4d1	stejná
podmínky	podmínka	k1gFnPc4	podmínka
hoření	hoření	k1gNnSc2	hoření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
jednotka	jednotka	k1gFnSc1	jednotka
předefinována	předefinován	k2eAgFnSc1d1	předefinován
jako	jako	k8xS	jako
svítivost	svítivost	k1gFnSc1	svítivost
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
600	[number]	k4	600
000	[number]	k4	000
m2	m2	k4	m2
povrchu	povrch	k1gInSc2	povrch
absolutně	absolutně	k6eAd1	absolutně
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
kolmém	kolmý	k2eAgInSc6d1	kolmý
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
povrchu	povrch	k1gInSc3	povrch
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
platiny	platina	k1gFnSc2	platina
(	(	kIx(	(
<g/>
1768	[number]	k4	1768
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
při	při	k7c6	při
normálním	normální	k2eAgInSc6d1	normální
tlaku	tlak	k1gInSc6	tlak
(	(	kIx(	(
<g/>
101	[number]	k4	101
325	[number]	k4	325
Pa	Pa	kA	Pa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
na	na	k7c4	na
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
generální	generální	k2eAgFnSc4d1	generální
konferenci	konference	k1gFnSc4	konference
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
definice	definice	k1gFnSc1	definice
platí	platit	k5eAaImIp3nS	platit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
<g/>
Všechny	všechen	k3xTgFnPc1	všechen
definice	definice	k1gFnPc1	definice
popisují	popisovat	k5eAaImIp3nP	popisovat
prakticky	prakticky	k6eAd1	prakticky
stejnou	stejný	k2eAgFnSc4d1	stejná
jednotkovou	jednotkový	k2eAgFnSc4d1	jednotková
svítivost	svítivost	k1gFnSc4	svítivost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stále	stále	k6eAd1	stále
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
svítivosti	svítivost	k1gFnSc2	svítivost
plamene	plamen	k1gInSc2	plamen
jedné	jeden	k4xCgFnSc2	jeden
běžné	běžný	k2eAgFnSc2d1	běžná
svíčky	svíčka	k1gFnSc2	svíčka
ve	v	k7c6	v
vodorovném	vodorovný	k2eAgInSc6d1	vodorovný
směru	směr	k1gInSc6	směr
(	(	kIx(	(
<g/>
plamen	plamen	k1gInSc1	plamen
je	být	k5eAaImIp3nS	být
vertikálně	vertikálně	k6eAd1	vertikálně
protáhlý	protáhlý	k2eAgMnSc1d1	protáhlý
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svislém	svislý	k2eAgInSc6d1	svislý
směru	směr	k1gInSc6	směr
jeho	jeho	k3xOp3gFnSc4	jeho
svítivost	svítivost	k1gFnSc4	svítivost
menší	malý	k2eAgInPc1d2	menší
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
<g/>
:	:	kIx,	:
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
žárovka	žárovka	k1gFnSc1	žárovka
100	[number]	k4	100
W	W	kA	W
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
cd	cd	kA	cd
<g/>
;	;	kIx,	;
běžná	běžný	k2eAgFnSc1d1	běžná
indikační	indikační	k2eAgFnSc1d1	indikační
světelná	světelný	k2eAgFnSc1d1	světelná
(	(	kIx(	(
<g/>
LED	LED	kA	LED
<g/>
)	)	kIx)	)
dioda	dioda	k1gFnSc1	dioda
jen	jen	k9	jen
asi	asi	k9	asi
0,5	[number]	k4	0,5
cd	cd	kA	cd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
jednotky	jednotka	k1gFnSc2	jednotka
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
candela	candel	k1gMnSc2	candel
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svíce	svíce	k1gFnSc1	svíce
<g/>
,	,	kIx,	,
svíčka	svíčka	k1gFnSc1	svíčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
lumenem	lumen	k1gInSc7	lumen
a	a	k8xC	a
kandelou	kandela	k1gFnSc7	kandela
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
zdrojů	zdroj	k1gInPc2	zdroj
světla	světlo	k1gNnSc2	světlo
s	s	k7c7	s
usměrněným	usměrněný	k2eAgInSc7d1	usměrněný
svitem	svit	k1gInSc7	svit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
LED	LED	kA	LED
<g/>
)	)	kIx)	)
výrobci	výrobce	k1gMnPc1	výrobce
uvádějí	uvádět	k5eAaImIp3nP	uvádět
svítivost	svítivost	k1gFnSc4	svítivost
právě	právě	k9	právě
v	v	k7c6	v
cd	cd	kA	cd
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
však	však	k9	však
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
přímo	přímo	k6eAd1	přímo
porovnávatelná	porovnávatelný	k2eAgFnSc1d1	porovnávatelný
<g/>
.	.	kIx.	.
</s>
<s>
Musíme	muset	k5eAaImIp1nP	muset
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
znát	znát	k5eAaImF	znát
ještě	ještě	k9	ještě
další	další	k2eAgInSc1d1	další
údaj	údaj	k1gInSc1	údaj
–	–	k?	–
úhel	úhel	k1gInSc4	úhel
rotačního	rotační	k2eAgInSc2d1	rotační
kužele	kužel	k1gInSc2	kužel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
zdroj	zdroj	k1gInSc1	zdroj
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
lm-cd	lmd	k6eAd1	lm-cd
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
dán	dát	k5eAaPmNgInS	dát
poměrem	poměr	k1gInSc7	poměr
plochy	plocha	k1gFnSc2	plocha
kruhové	kruhový	k2eAgFnSc2d1	kruhová
úseče	úseč	k1gFnSc2	úseč
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tento	tento	k3xDgInSc4	tento
kužel	kužel	k1gInSc1	kužel
vytne	vytnout	k5eAaPmIp3nS	vytnout
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
koule	koule	k1gFnSc2	koule
a	a	k8xC	a
plochy	plocha	k1gFnSc2	plocha
kruhové	kruhový	k2eAgFnSc2d1	kruhová
úseče	úseč	k1gFnSc2	úseč
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vytne	vytnout	k5eAaPmIp3nS	vytnout
prostorový	prostorový	k2eAgInSc4d1	prostorový
úhel	úhel	k1gInSc4	úhel
1	[number]	k4	1
steradián	steradián	k1gInSc4	steradián
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
můžeme	moct	k5eAaImIp1nP	moct
přepočítat	přepočítat	k5eAaPmF	přepočítat
svítivost	svítivost	k1gFnSc4	svítivost
[	[	kIx(	[
<g/>
cd	cd	kA	cd
<g/>
]	]	kIx)	]
na	na	k7c4	na
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
[	[	kIx(	[
<g/>
lm	lm	k?	lm
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
přímou	přímý	k2eAgFnSc4d1	přímá
návaznost	návaznost	k1gFnSc4	návaznost
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
všesměrového	všesměrový	k2eAgMnSc2d1	všesměrový
<g/>
,	,	kIx,	,
bodového	bodový	k2eAgInSc2d1	bodový
zdroje	zdroj	k1gInSc2	zdroj
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gInSc4	on
porovnávat	porovnávat	k5eAaImF	porovnávat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
–	–	k?	–
když	když	k8xS	když
mají	mít	k5eAaImIp3nP	mít
2	[number]	k4	2
LED	LED	kA	LED
stejnou	stejný	k2eAgFnSc4d1	stejná
svítivost	svítivost	k1gFnSc4	svítivost
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
stejný	stejný	k2eAgInSc4d1	stejný
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ani	ani	k9	ani
výkon	výkon	k1gInSc1	výkon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
-li	i	k?	-li
zdroj	zdroj	k1gInSc4	zdroj
určitou	určitý	k2eAgFnSc7d1	určitá
svítivostí	svítivost	k1gFnSc7	svítivost
Iv	Iva	k1gFnPc2	Iva
(	(	kIx(	(
<g/>
v	v	k7c6	v
kandelách	kandela	k1gFnPc6	kandela
<g/>
)	)	kIx)	)
v	v	k7c6	v
definovaném	definovaný	k2eAgInSc6d1	definovaný
prostorovém	prostorový	k2eAgInSc6d1	prostorový
úhlu	úhel	k1gInSc6	úhel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
světelný	světelný	k2eAgInSc1d1	světelný
tok	tok	k1gInSc1	tok
Φ	Φ	k?	Φ
v	v	k7c6	v
lumenech	lumen	k1gInPc6	lumen
dán	dát	k5eAaPmNgInS	dát
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
[	[	kIx(	[
</s>
</p>
<p>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
]	]	kIx)	]
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
cos	cos	k3yInSc1	cos
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
β	β	k?	β
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
lm	lm	k?	lm
<g/>
}	}	kIx)	}
]	]	kIx)	]
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
cd	cd	kA	cd
<g/>
}	}	kIx)	}
]	]	kIx)	]
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
cdot	cdotum	k1gNnPc2	cdotum
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
))	))	k?	))
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
β	β	k?	β
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc1	úhel
vyzařování	vyzařování	k1gNnSc2	vyzařování
svítidla	svítidlo	k1gNnSc2	svítidlo
–	–	k?	–
<g/>
celkový	celkový	k2eAgInSc4d1	celkový
vrcholový	vrcholový	k2eAgInSc4d1	vrcholový
úhel	úhel	k1gInSc4	úhel
světelného	světelný	k2eAgInSc2d1	světelný
kužele	kužel	k1gInSc2	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lampa	lampa	k1gFnSc1	lampa
se	s	k7c7	s
svítivostí	svítivost	k1gFnSc7	svítivost
590	[number]	k4	590
cd	cd	kA	cd
a	a	k8xC	a
vrcholovým	vrcholový	k2eAgInSc7d1	vrcholový
úhlem	úhel	k1gInSc7	úhel
40	[number]	k4	40
<g/>
°	°	k?	°
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
224	[number]	k4	224
lumenů	lumen	k1gInPc2	lumen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zdroj	zdroj	k1gInSc1	zdroj
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
světlo	světlo	k1gNnSc4	světlo
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tok	tok	k1gInSc1	tok
vypočten	vypočíst	k5eAaPmNgInS	vypočíst
násobením	násobení	k1gNnSc7	násobení
svítivosti	svítivost	k1gFnSc2	svítivost
konstantou	konstanta	k1gFnSc7	konstanta
4	[number]	k4	4
<g/>
π	π	k?	π
<g/>
:	:	kIx,	:
všesměrový	všesměrový	k2eAgInSc4d1	všesměrový
zdroj	zdroj	k1gInSc4	zdroj
1	[number]	k4	1
kandela	kandela	k1gFnSc1	kandela
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
12,6	[number]	k4	12,6
lumenů	lumen	k1gInPc2	lumen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kandela	kandela	k1gFnSc1	kandela
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
