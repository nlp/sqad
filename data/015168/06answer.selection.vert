<s desamb="1">
Doba	doba	k1gFnSc1
její	její	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
trvala	trvat	k5eAaImAgFnS
63	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
7	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc4
nejdelší	dlouhý	k2eAgNnSc4d3
období	období	k1gNnSc4
vlády	vláda	k1gFnSc2
britského	britský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>