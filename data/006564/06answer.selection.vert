<s>
Během	během	k7c2	během
translace	translace	k1gFnSc2	translace
je	být	k5eAaImIp3nS	být
informace	informace	k1gFnSc1	informace
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
v	v	k7c6	v
mRNA	mRNA	k?	mRNA
podle	podle	k7c2	podle
přesných	přesný	k2eAgNnPc2d1	přesné
pravidel	pravidlo	k1gNnPc2	pravidlo
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
dekódována	dekódován	k2eAgFnSc1d1	dekódována
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
sestaven	sestavit	k5eAaPmNgInS	sestavit
řetězec	řetězec	k1gInSc1	řetězec
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
