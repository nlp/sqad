<s>
Translace	translace	k1gFnSc1	translace
je	být	k5eAaImIp3nS	být
sekundární	sekundární	k2eAgInSc4d1	sekundární
proces	proces	k1gInSc4	proces
syntézy	syntéza	k1gFnSc2	syntéza
bílkovin	bílkovina	k1gFnPc2	bílkovina
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
procesu	proces	k1gInSc2	proces
genové	genový	k2eAgFnSc2d1	genová
exprese	exprese	k1gFnSc2	exprese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sestavení	sestavení	k1gNnSc4	sestavení
primární	primární	k2eAgFnSc2d1	primární
struktury	struktura	k1gFnSc2	struktura
bílkoviny	bílkovina	k1gFnSc2	bílkovina
podle	podle	k7c2	podle
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
transkripci	transkripce	k1gFnSc6	transkripce
vytvořené	vytvořený	k2eAgFnSc6d1	vytvořená
mediátorovou	mediátorův	k2eAgFnSc7d1	mediátorův
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
mRNA	mRNA	k?	mRNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
translace	translace	k1gFnSc2	translace
je	být	k5eAaImIp3nS	být
informace	informace	k1gFnSc1	informace
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
v	v	k7c6	v
mRNA	mRNA	k?	mRNA
podle	podle	k7c2	podle
přesných	přesný	k2eAgNnPc2d1	přesné
pravidel	pravidlo	k1gNnPc2	pravidlo
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
dekódována	dekódován	k2eAgFnSc1d1	dekódována
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
sestaven	sestavit	k5eAaPmNgInS	sestavit
řetězec	řetězec	k1gInSc1	řetězec
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Translaci	translace	k1gFnSc4	translace
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
fází	fáze	k1gFnPc2	fáze
<g/>
:	:	kIx,	:
iniciace	iniciace	k1gFnSc1	iniciace
<g/>
,	,	kIx,	,
elongace	elongace	k1gFnSc1	elongace
a	a	k8xC	a
terminace	terminace	k1gFnSc1	terminace
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
translace	translace	k1gFnSc2	translace
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
konzervovaný	konzervovaný	k2eAgInSc1d1	konzervovaný
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgFnPc7	všecek
základními	základní	k2eAgFnPc7d1	základní
větvemi	větev	k1gFnPc7	větev
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
názvy	název	k1gInPc1	název
faktorů	faktor	k1gInPc2	faktor
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nP	lišit
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
proteiny	protein	k1gInPc1	protein
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dodatečně	dodatečně	k6eAd1	dodatečně
až	až	k9	až
u	u	k7c2	u
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
organismů	organismus	k1gInPc2	organismus
s	s	k7c7	s
pravým	pravý	k2eAgNnSc7d1	pravé
jádrem	jádro	k1gNnSc7	jádro
-	-	kIx~	-
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
živočichy	živočich	k1gMnPc4	živočich
či	či	k8xC	či
např.	např.	kA	např.
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
iniciační	iniciační	k2eAgFnSc2d1	iniciační
fáze	fáze	k1gFnSc2	fáze
translace	translace	k1gFnSc2	translace
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kontaktu	kontakt	k1gInSc3	kontakt
AUG	AUG	kA	AUG
startkodonu	startkodon	k1gInSc2	startkodon
na	na	k7c6	na
mRNA	mRNA	k?	mRNA
s	s	k7c7	s
iniciační	iniciační	k2eAgFnSc7d1	iniciační
tRNA	trnout	k5eAaImSgInS	trnout
nesoucí	nesoucí	k2eAgFnSc1d1	nesoucí
methionin	methionin	k1gInSc1	methionin
(	(	kIx(	(
<g/>
tRNAMet	tRNAMet	k1gInSc1	tRNAMet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
koordinují	koordinovat	k5eAaBmIp3nP	koordinovat
tzv.	tzv.	kA	tzv.
iniciační	iniciační	k2eAgInPc1d1	iniciační
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
kterých	který	k3yRgMnPc2	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
až	až	k8xS	až
desítky	desítka	k1gFnPc4	desítka
(	(	kIx(	(
<g/>
u	u	k7c2	u
bakterií	bakterium	k1gNnPc2	bakterium
mají	mít	k5eAaImIp3nP	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
tolik	tolik	k6eAd1	tolik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kroku	krok	k1gInSc6	krok
se	se	k3xPyFc4	se
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
ribozomální	ribozomální	k2eAgFnSc4d1	ribozomální
podjednotku	podjednotka	k1gFnSc4	podjednotka
naváže	navázat	k5eAaPmIp3nS	navázat
iniciační	iniciační	k2eAgInSc1d1	iniciační
tRNA	trnout	k5eAaImSgInS	trnout
a	a	k8xC	a
iniciační	iniciační	k2eAgInSc1d1	iniciační
faktor	faktor	k1gInSc1	faktor
eIF	eIF	k?	eIF
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
IF2	IF2	k1gMnPc2	IF2
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
)	)	kIx)	)
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
aktivní	aktivní	k2eAgFnSc6d1	aktivní
formě	forma	k1gFnSc6	forma
s	s	k7c7	s
GTP	GTP	kA	GTP
(	(	kIx(	(
<g/>
eIF	eIF	k?	eIF
<g/>
2	[number]	k4	2
<g/>
-GTP	-GTP	k?	-GTP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
komplex	komplex	k1gInSc1	komplex
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
preiniciační	preiniciační	k2eAgInSc4d1	preiniciační
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
guanosinové	guanosinový	k2eAgFnSc3d1	guanosinový
čepičce	čepička	k1gFnSc3	čepička
mRNA	mRNA	k?	mRNA
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
naváže	navázat	k5eAaPmIp3nS	navázat
helikáza	helikáza	k1gFnSc1	helikáza
eIF	eIF	k?	eIF
<g/>
4	[number]	k4	4
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
rozmotat	rozmotat	k5eAaPmF	rozmotat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
sekundární	sekundární	k2eAgFnPc4d1	sekundární
struktury	struktura	k1gFnPc4	struktura
přítomné	přítomný	k2eAgFnPc4d1	přítomná
na	na	k7c4	na
v	v	k7c4	v
5	[number]	k4	5
<g/>
'	'	kIx"	'
konci	konec	k1gInSc6	konec
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
komplexy	komplex	k1gInPc1	komplex
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
asociují	asociovat	k5eAaBmIp3nP	asociovat
s	s	k7c7	s
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
koncem	koncem	k7c2	koncem
této	tento	k3xDgFnSc2	tento
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
připravená	připravený	k2eAgFnSc1d1	připravená
mRNA	mRNA	k?	mRNA
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
preiniciační	preiniciační	k2eAgInSc4d1	preiniciační
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
malá	malý	k2eAgFnSc1d1	malá
ribosomální	ribosomální	k2eAgFnSc1d1	ribosomální
podjednotka	podjednotka	k1gFnSc1	podjednotka
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
správné	správný	k2eAgNnSc4d1	správné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
start	start	k1gInSc4	start
kodon	kodona	k1gFnPc2	kodona
AUG	AUG	kA	AUG
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
skenování	skenování	k1gNnSc1	skenování
mRNA	mRNA	k?	mRNA
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
tRNAMet	tRNAMet	k1gInSc1	tRNAMet
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
start	start	k1gInSc4	start
kodon	kodona	k1gFnPc2	kodona
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
první	první	k4xOgInSc4	první
kodon	kodon	k1gInSc4	kodon
od	od	k7c2	od
5	[number]	k4	5
<g/>
'	'	kIx"	'
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoznání	rozpoznání	k1gNnPc4	rozpoznání
start	start	k1gInSc1	start
kodonu	kodon	k1gInSc2	kodon
AUG	AUG	kA	AUG
od	od	k7c2	od
běžných	běžný	k2eAgInPc2d1	běžný
AUG	AUG	kA	AUG
je	být	k5eAaImIp3nS	být
usnadněno	usnadnit	k5eAaPmNgNnS	usnadnit
dalšími	další	k2eAgInPc7d1	další
nukleotidy	nukleotid	k1gInPc7	nukleotid
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
-	-	kIx~	-
celá	celý	k2eAgFnSc1d1	celá
sekvence	sekvence	k1gFnSc1	sekvence
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tzv.	tzv.	kA	tzv.
sekvenci	sekvence	k1gFnSc4	sekvence
Kozakové	Kozakový	k2eAgInPc1d1	Kozakový
-	-	kIx~	-
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
přibližně	přibližně	k6eAd1	přibližně
RCCAUGG	RCCAUGG	kA	RCCAUGG
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpoznání	rozpoznání	k1gNnSc6	rozpoznání
startovního	startovní	k2eAgInSc2d1	startovní
kodonu	kodon	k1gInSc2	kodon
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
GTP	GTP	kA	GTP
na	na	k7c6	na
faktoru	faktor	k1gInSc6	faktor
eIF	eIF	k?	eIF
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hydrolýze	hydrolýza	k1gFnSc6	hydrolýza
GTP	GTP	kA	GTP
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
disociaci	disociace	k1gFnSc3	disociace
všech	všecek	k3xTgInPc2	všecek
zbývajících	zbývající	k2eAgInPc2d1	zbývající
faktorů	faktor	k1gInPc2	faktor
z	z	k7c2	z
komplexu	komplex	k1gInSc2	komplex
a	a	k8xC	a
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
60S	[number]	k4	60S
podjednotky	podjednotka	k1gFnPc4	podjednotka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
GTP	GTP	kA	GTP
v	v	k7c6	v
iniciačním	iniciační	k2eAgInSc6d1	iniciační
faktoru	faktor	k1gInSc6	faktor
eIF	eIF	k?	eIF
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odstartuje	odstartovat	k5eAaPmIp3nS	odstartovat
proces	proces	k1gInSc4	proces
elongace	elongace	k1gFnSc2	elongace
<g/>
.	.	kIx.	.
</s>
<s>
Ribozom	Ribozom	k1gInSc1	Ribozom
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
kompletní	kompletní	k2eAgMnSc1d1	kompletní
a	a	k8xC	a
připravený	připravený	k2eAgMnSc1d1	připravený
vytvářet	vytvářet	k5eAaImF	vytvářet
polypeptid	polypeptid	k1gInSc1	polypeptid
postupným	postupný	k2eAgNnSc7d1	postupné
přidáváním	přidávání	k1gNnSc7	přidávání
jedné	jeden	k4xCgFnSc2	jeden
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
za	za	k7c7	za
druhou	druhý	k4xOgFnSc7	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Přidání	přidání	k1gNnSc1	přidání
každé	každý	k3xTgFnSc2	každý
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
krocích	krok	k1gInPc6	krok
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
příslušná	příslušný	k2eAgFnSc1d1	příslušná
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
přiřazena	přiřadit	k5eAaPmNgFnS	přiřadit
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
-	-	kIx~	-
ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
trojici	trojice	k1gFnSc3	trojice
(	(	kIx(	(
<g/>
tripletu	triplet	k1gInSc3	triplet
<g/>
)	)	kIx)	)
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bazí	baz	k1gFnPc2	baz
je	být	k5eAaImIp3nS	být
přiřazena	přiřadit	k5eAaPmNgFnS	přiřadit
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
proteinogenních	proteinogenní	k2eAgFnPc2d1	proteinogenní
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kroky	krok	k1gInPc1	krok
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
uvnitř	uvnitř	k7c2	uvnitř
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
A-místě	Aíst	k1gInSc6	A-míst
a	a	k8xC	a
P-místě	Píst	k1gInSc6	P-míst
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
i	i	k9	i
další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgInPc1d1	zvaný
tentokrát	tentokrát	k6eAd1	tentokrát
elongační	elongační	k2eAgInPc1d1	elongační
(	(	kIx(	(
<g/>
EF	EF	kA	EF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
ribozomu	ribozom	k1gInSc2	ribozom
doručovány	doručovat	k5eAaImNgFnP	doručovat
v	v	k7c6	v
aktivované	aktivovaný	k2eAgFnSc6d1	aktivovaná
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
připojené	připojený	k2eAgNnSc1d1	připojené
na	na	k7c4	na
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
tRNA	trnout	k5eAaImSgInS	trnout
pomocí	pomocí	k7c2	pomocí
aminoacyl-tRNA	aminoacylRNA	k?	aminoacyl-tRNA
syntetázy	syntetáza	k1gFnSc2	syntetáza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
přichází	přicházet	k5eAaImIp3nS	přicházet
aminoacyl-tRNA	aminoacylRNA	k?	aminoacyl-tRNA
(	(	kIx(	(
<g/>
tRNA	trnout	k5eAaImSgInS	trnout
s	s	k7c7	s
navázanou	navázaný	k2eAgFnSc7d1	navázaná
aminokyselinou	aminokyselina	k1gFnSc7	aminokyselina
<g/>
)	)	kIx)	)
do	do	k7c2	do
A-místa	Aíst	k1gMnSc2	A-míst
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
elongační	elongační	k2eAgInSc1d1	elongační
faktor	faktor	k1gInSc1	faktor
eEF	eEF	k?	eEF
<g/>
1	[number]	k4	1
<g/>
A	A	kA	A
(	(	kIx(	(
<g/>
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
EF-Tu	EF-Tus	k1gInSc2	EF-Tus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přinesená	přinesený	k2eAgFnSc1d1	přinesená
aminoacyl-tRNA	aminoacylRNA	k?	aminoacyl-tRNA
musí	muset	k5eAaImIp3nS	muset
nést	nést	k5eAaImF	nést
právě	právě	k9	právě
tu	ten	k3xDgFnSc4	ten
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kodonu	kodona	k1gFnSc4	kodona
v	v	k7c6	v
právě	právě	k6eAd1	právě
překládané	překládaný	k2eAgFnSc6d1	překládaná
části	část	k1gFnSc6	část
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
přinesená	přinesený	k2eAgFnSc1d1	přinesená
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
z	z	k7c2	z
ribozomu	ribozom	k1gInSc2	ribozom
vypuzena	vypuzen	k2eAgFnSc1d1	vypuzena
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
kodon	kodon	k1gInSc1	kodon
na	na	k7c4	na
mRNA	mRNA	k?	mRNA
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tzv.	tzv.	kA	tzv.
antikodonu	antikodona	k1gFnSc4	antikodona
v	v	k7c6	v
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
,	,	kIx,	,
eEF	eEF	k?	eEF
<g/>
1	[number]	k4	1
<g/>
A	a	k8xC	a
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
své	svůj	k3xOyFgNnSc1	svůj
GTP	GTP	kA	GTP
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přechod	přechod	k1gInSc4	přechod
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
fáze	fáze	k1gFnSc2	fáze
elongace	elongace	k1gFnSc2	elongace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
je	být	k5eAaImIp3nS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
vazba	vazba	k1gFnSc1	vazba
mezi	mezi	k7c7	mezi
peptidovým	peptidový	k2eAgInSc7d1	peptidový
řetězcem	řetězec	k1gInSc7	řetězec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
P-místě	Písta	k1gFnSc6	P-místa
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
,	,	kIx,	,
a	a	k8xC	a
aminoskupinou	aminoskupina	k1gFnSc7	aminoskupina
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
A-místa	Aíst	k1gMnSc2	A-míst
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samotný	samotný	k2eAgInSc4d1	samotný
počátek	počátek	k1gInSc4	počátek
elongace	elongace	k1gFnSc2	elongace
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
jevu	jev	k1gInSc3	jev
<g/>
,	,	kIx,	,
jen	jen	k9	jen
v	v	k7c6	v
P-místě	Písta	k1gFnSc6	P-místa
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
peptidový	peptidový	k2eAgInSc1d1	peptidový
řetězec	řetězec	k1gInSc1	řetězec
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
iniciační	iniciační	k2eAgFnSc1d1	iniciační
tRNA	trnout	k5eAaImSgInS	trnout
s	s	k7c7	s
navázaným	navázaný	k2eAgInSc7d1	navázaný
methioninem	methionin	k1gInSc7	methionin
-	-	kIx~	-
první	první	k4xOgFnSc7	první
aminokyselinou	aminokyselina	k1gFnSc7	aminokyselina
každého	každý	k3xTgInSc2	každý
polypeptidu	polypeptid	k1gInSc2	polypeptid
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
translokace	translokace	k1gFnSc1	translokace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
peptidový	peptidový	k2eAgInSc1d1	peptidový
řetězec	řetězec	k1gInSc1	řetězec
a	a	k8xC	a
mRNA	mRNA	k?	mRNA
posune	posunout	k5eAaPmIp3nS	posunout
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
triplet	triplet	k1gInSc4	triplet
bazí	bazí	k6eAd1	bazí
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
P-místa	Píst	k1gMnSc2	P-míst
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
prostor	prostor	k1gInSc1	prostor
v	v	k7c6	v
A-místě	Aíst	k1gInSc6	A-míst
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
aminoacyl-tRNA	aminoacylRNA	k?	aminoacyl-tRNA
<g/>
.	.	kIx.	.
</s>
<s>
Procesu	proces	k1gInSc3	proces
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
elongační	elongační	k2eAgInSc4d1	elongační
faktor	faktor	k1gInSc4	faktor
eEF	eEF	k?	eEF
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
EF-G	EF-G	k1gFnSc2	EF-G
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vmezeří	vmezeřit	k5eAaPmIp3nS	vmezeřit
do	do	k7c2	do
A-místa	Aíst	k1gMnSc2	A-míst
a	a	k8xC	a
translokuje	translokovat	k5eAaPmIp3nS	translokovat
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
peptidový	peptidový	k2eAgInSc1d1	peptidový
řetězec	řetězec	k1gInSc1	řetězec
dál	daleko	k6eAd2	daleko
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
východu	východ	k1gInSc3	východ
z	z	k7c2	z
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
GTP	GTP	kA	GTP
v	v	k7c6	v
eEF	eEF	k?	eEF
<g/>
2	[number]	k4	2
hydrolyzováno	hydrolyzován	k2eAgNnSc4d1	hydrolyzován
<g/>
,	,	kIx,	,
eEF	eEF	k?	eEF
<g/>
2	[number]	k4	2
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
A-místa	Aíst	k1gMnSc2	A-míst
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
spustit	spustit	k5eAaPmF	spustit
další	další	k2eAgNnSc4d1	další
kolo	kolo	k1gNnSc4	kolo
elongace	elongace	k1gFnSc2	elongace
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
A-místa	Aíst	k1gMnSc2	A-míst
opět	opět	k6eAd1	opět
navazuje	navazovat	k5eAaImIp3nS	navazovat
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
posouváním	posouvání	k1gNnSc7	posouvání
ribozomu	ribozom	k1gInSc2	ribozom
do	do	k7c2	do
A-místa	Aíst	k1gMnSc2	A-míst
dostane	dostat	k5eAaPmIp3nS	dostat
kodon	kodon	k1gNnSc4	kodon
UAA	UAA	kA	UAA
<g/>
,	,	kIx,	,
UAG	UAG	kA	UAG
nebo	nebo	k8xC	nebo
UGA	UGA	kA	UGA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
polypeptid	polypeptid	k1gInSc1	polypeptid
hotový	hotový	k2eAgInSc1d1	hotový
a	a	k8xC	a
proteosyntéza	proteosyntéza	k1gFnSc1	proteosyntéza
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
o	o	k7c4	o
terminační	terminační	k2eAgInPc4d1	terminační
kodony	kodon	k1gInPc4	kodon
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesignalizují	signalizovat	k5eNaImIp3nP	signalizovat
žádnou	žádný	k3yNgFnSc4	žádný
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
nemají	mít	k5eNaImIp3nP	mít
jiný	jiný	k2eAgInSc4d1	jiný
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
polypeptid	polypeptid	k1gInSc4	polypeptid
<g/>
,	,	kIx,	,
uspořádaný	uspořádaný	k2eAgInSc4d1	uspořádaný
do	do	k7c2	do
primární	primární	k2eAgFnSc2d1	primární
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Procesu	proces	k1gInSc3	proces
napomáhají	napomáhat	k5eAaBmIp3nP	napomáhat
tzv.	tzv.	kA	tzv.
release	release	k6eAd1	release
faktory	faktor	k1gInPc1	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
polypeptid	polypeptid	k1gInSc4	polypeptid
to	ten	k3xDgNnSc1	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
konec	konec	k1gInSc1	konec
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
skládání	skládání	k1gNnSc3	skládání
(	(	kIx(	(
<g/>
foldingu	folding	k1gInSc2	folding
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
posttranslačních	posttranslační	k2eAgFnPc2d1	posttranslační
úprav	úprava	k1gFnPc2	úprava
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
teprve	teprve	k6eAd1	teprve
vzniká	vznikat	k5eAaImIp3nS	vznikat
zralý	zralý	k2eAgInSc1d1	zralý
protein	protein	k1gInSc1	protein
(	(	kIx(	(
<g/>
bílkovina	bílkovina	k1gFnSc1	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Translace	translace	k1gFnSc1	translace
je	být	k5eAaImIp3nS	být
energeticky	energeticky	k6eAd1	energeticky
velice	velice	k6eAd1	velice
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bakterie	bakterie	k1gFnSc1	bakterie
E.	E.	kA	E.
coli	coli	k6eAd1	coli
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
90	[number]	k4	90
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
celkové	celkový	k2eAgFnSc2d1	celková
potřeby	potřeba	k1gFnSc2	potřeba
energie	energie	k1gFnSc2	energie
právě	právě	k9	právě
na	na	k7c4	na
syntézu	syntéza	k1gFnSc4	syntéza
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pouhou	pouhý	k2eAgFnSc4d1	pouhá
jednu	jeden	k4xCgFnSc4	jeden
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
se	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
uvádí	uvádět	k5eAaImIp3nS	uvádět
spotřeba	spotřeba	k1gFnSc1	spotřeba
4	[number]	k4	4
molekul	molekula	k1gFnPc2	molekula
ATP	atp	kA	atp
<g/>
:	:	kIx,	:
2	[number]	k4	2
ATP	atp	kA	atp
–	–	k?	–
aktivace	aktivace	k1gFnSc1	aktivace
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
navázáním	navázání	k1gNnSc7	navázání
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
na	na	k7c4	na
tRNA	trnout	k5eAaImSgInS	trnout
2	[number]	k4	2
ATP	atp	kA	atp
–	–	k?	–
vznik	vznik	k1gInSc4	vznik
peptidové	peptidový	k2eAgFnSc2d1	peptidová
vazby	vazba	k1gFnSc2	vazba
Ze	z	k7c2	z
studií	studie	k1gFnPc2	studie
srovnávajících	srovnávající	k2eAgFnPc2d1	srovnávající
normální	normální	k2eAgFnSc4d1	normální
energetickou	energetický	k2eAgFnSc4d1	energetická
potřebu	potřeba	k1gFnSc4	potřeba
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
zablokována	zablokován	k2eAgFnSc1d1	zablokována
syntéza	syntéza	k1gFnSc1	syntéza
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgNnPc4d2	vyšší
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
asi	asi	k9	asi
7,5	[number]	k4	7,5
ATP	atp	kA	atp
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
zařazenou	zařazený	k2eAgFnSc4d1	zařazená
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
(	(	kIx(	(
<g/>
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
translačního	translační	k2eAgInSc2d1	translační
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
různé	různý	k2eAgInPc1d1	různý
elongační	elongační	k2eAgInPc1d1	elongační
faktory	faktor	k1gInPc1	faktor
(	(	kIx(	(
<g/>
EF-T	EF-T	k1gFnSc1	EF-T
<g/>
,	,	kIx,	,
EF-G	EF-G	k1gFnSc1	EF-G
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
posttranslační	posttranslační	k2eAgFnSc2d1	posttranslační
modifikace	modifikace	k1gFnSc2	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
syntézy	syntéza	k1gFnSc2	syntéza
polypeptidového	polypeptidový	k2eAgInSc2d1	polypeptidový
řetězce	řetězec	k1gInSc2	řetězec
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
bílkovin	bílkovina	k1gFnPc2	bílkovina
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
následným	následný	k2eAgFnPc3d1	následná
úpravám	úprava	k1gFnPc3	úprava
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
posttranslačním	posttranslační	k2eAgFnPc3d1	posttranslační
modifikacím	modifikace	k1gFnPc3	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
modifikace	modifikace	k1gFnPc1	modifikace
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
finální	finální	k2eAgFnSc3d1	finální
podobě	podoba	k1gFnSc3	podoba
nativní	nativní	k2eAgFnSc2d1	nativní
bílkoviny	bílkovina	k1gFnSc2	bílkovina
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgFnPc1d1	zásadní
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
posttranslační	posttranslační	k2eAgFnPc4d1	posttranslační
modifikace	modifikace	k1gFnPc4	modifikace
patří	patřit	k5eAaImIp3nS	patřit
částečné	částečný	k2eAgNnSc1d1	částečné
proteolytické	proteolytický	k2eAgNnSc1d1	proteolytické
štěpení	štěpení	k1gNnSc1	štěpení
<g/>
,	,	kIx,	,
tvorba	tvorba	k1gFnSc1	tvorba
disulfidových	disulfidový	k2eAgFnPc2d1	disulfidový
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
glykosylace	glykosylace	k1gFnSc2	glykosylace
<g/>
,	,	kIx,	,
γ	γ	k?	γ
<g/>
,	,	kIx,	,
hydroxylace	hydroxylace	k1gFnSc1	hydroxylace
<g/>
,	,	kIx,	,
fosforylace	fosforylace	k1gFnSc1	fosforylace
<g/>
,	,	kIx,	,
biotinylace	biotinylace	k1gFnSc1	biotinylace
či	či	k8xC	či
acylace	acylace	k1gFnSc1	acylace
<g/>
.	.	kIx.	.
</s>
