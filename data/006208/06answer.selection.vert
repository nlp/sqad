<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
<g/>
,	,	kIx,	,
miliardáře	miliardář	k1gMnSc4	miliardář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
obrněný	obrněný	k2eAgInSc4d1	obrněný
oblek	oblek	k1gInSc4	oblek
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
mírové	mírový	k2eAgInPc4d1	mírový
účely	účel	k1gInPc4	účel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
slavnou	slavný	k2eAgFnSc7d1	slavná
celebritou	celebrita	k1gFnSc7	celebrita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
představil	představit	k5eAaPmAgMnS	představit
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
snímku	snímek	k1gInSc6	snímek
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
navazujícím	navazující	k2eAgInSc6d1	navazující
filmu	film	k1gInSc6	film
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
3	[number]	k4	3
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
