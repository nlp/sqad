<s>
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
akční	akční	k2eAgInSc1d1	akční
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Jon	Jon	k1gMnSc2	Jon
Favreau	Favreaus	k1gInSc2	Favreaus
podle	podle	k7c2	podle
komiksů	komiks	k1gInPc2	komiks
o	o	k7c4	o
Iron	iron	k1gInSc4	iron
Manovi	Manův	k2eAgMnPc1d1	Manův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
<g/>
,	,	kIx,	,
miliardáře	miliardář	k1gMnSc4	miliardář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
obrněný	obrněný	k2eAgInSc4d1	obrněný
oblek	oblek	k1gInSc4	oblek
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
mírové	mírový	k2eAgInPc4d1	mírový
účely	účel	k1gInPc4	účel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
slavnou	slavný	k2eAgFnSc7d1	slavná
celebritou	celebrita	k1gFnSc7	celebrita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
představil	představit	k5eAaPmAgMnS	představit
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
snímku	snímek	k1gInSc6	snímek
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
navazujícím	navazující	k2eAgInSc6d1	navazující
filmu	film	k1gInSc6	film
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
3	[number]	k4	3
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
třetí	třetí	k4xOgInSc4	třetí
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
snímek	snímek	k1gInSc4	snímek
z	z	k7c2	z
filmové	filmový	k2eAgFnSc2d1	filmová
série	série	k1gFnSc2	série
Marvel	Marvela	k1gFnPc2	Marvela
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgNnPc1d1	ruské
média	médium	k1gNnPc1	médium
informují	informovat	k5eAaBmIp3nP	informovat
o	o	k7c4	o
odhalení	odhalení	k1gNnSc4	odhalení
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
Iron	iron	k1gInSc4	iron
Manem	Man	k1gMnSc7	Man
<g/>
.	.	kIx.	.
</s>
<s>
Anton	anton	k1gInSc1	anton
Vanko	Vanko	k1gNnSc1	Vanko
zrovna	zrovna	k6eAd1	zrovna
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zprávy	zpráva	k1gFnPc4	zpráva
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
stavět	stavět	k5eAaImF	stavět
miniaturní	miniaturní	k2eAgInSc1d1	miniaturní
obloukový	obloukový	k2eAgInSc1d1	obloukový
reaktor	reaktor	k1gInSc1	reaktor
podobným	podobný	k2eAgInSc7d1	podobný
tomu	ten	k3xDgNnSc3	ten
Starkovu	Starkův	k2eAgMnSc3d1	Starkův
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
Stark	Stark	k1gInSc4	Stark
obletovanou	obletovaný	k2eAgFnSc7d1	obletovaná
celebritou	celebrita	k1gFnSc7	celebrita
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
svůj	svůj	k3xOyFgInSc4	svůj
oblek	oblek	k1gInSc4	oblek
Iron	iron	k1gInSc1	iron
Mana	mana	k1gFnSc1	mana
k	k	k7c3	k
mírumilovným	mírumilovný	k2eAgInPc3d1	mírumilovný
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
odolává	odolávat	k5eAaImIp3nS	odolávat
tlaku	tlak	k1gInSc3	tlak
americké	americký	k2eAgFnSc2d1	americká
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prodal	prodat	k5eAaPmAgMnS	prodat
jeho	jeho	k3xOp3gNnPc4	jeho
konstrukční	konstrukční	k2eAgNnPc4d1	konstrukční
schémata	schéma	k1gNnPc4	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Obnoví	obnovit	k5eAaPmIp3nP	obnovit
veletrh	veletrh	k1gInSc4	veletrh
Stark	Stark	k1gInSc1	Stark
Expo	Expo	k1gNnSc1	Expo
ve	v	k7c4	v
Flushing	Flushing	k1gInSc4	Flushing
Meadows	Meadowsa	k1gFnPc2	Meadowsa
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
odkazu	odkaz	k1gInSc6	odkaz
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Howarda	Howard	k1gMnSc2	Howard
<g/>
.	.	kIx.	.
</s>
<s>
Palladiové	palladiový	k2eAgNnSc1d1	palladiový
jádro	jádro	k1gNnSc1	jádro
obloukového	obloukový	k2eAgInSc2d1	obloukový
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
udržuje	udržovat	k5eAaImIp3nS	udržovat
Starka	starka	k1gFnSc1	starka
naživu	naživu	k6eAd1	naživu
a	a	k8xC	a
také	také	k9	také
pohání	pohánět	k5eAaImIp3nS	pohánět
jeho	on	k3xPp3gInSc4	on
oblek	oblek	k1gInSc4	oblek
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
však	však	k9	však
také	také	k9	také
tráví	trávit	k5eAaImIp3nS	trávit
a	a	k8xC	a
Tony	Tony	k1gMnSc1	Tony
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
za	za	k7c4	za
něj	on	k3xPp3gNnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
náhradu	náhrada	k1gFnSc4	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
očekávané	očekávaný	k2eAgFnSc3d1	očekávaná
smrti	smrt	k1gFnSc3	smrt
je	být	k5eAaImIp3nS	být
deprimovaný	deprimovaný	k2eAgInSc1d1	deprimovaný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
lehkomyslný	lehkomyslný	k2eAgMnSc1d1	lehkomyslný
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
neříct	říct	k5eNaPmF	říct
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
nikomu	nikdo	k3yNnSc3	nikdo
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
stávající	stávající	k2eAgFnSc4d1	stávající
osobní	osobní	k2eAgFnSc4d1	osobní
asistentku	asistentka	k1gFnSc4	asistentka
Pepper	Pepper	k1gMnSc1	Pepper
Pottsovou	Pottsová	k1gFnSc7	Pottsová
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
ředitelkou	ředitelka	k1gFnSc7	ředitelka
Stark	Stark	k1gInSc4	Stark
Industries	Industriesa	k1gFnPc2	Industriesa
a	a	k8xC	a
najme	najmout	k5eAaPmIp3nS	najmout
zaměstnankyni	zaměstnankyně	k1gFnSc4	zaměstnankyně
Natalii	Natalie	k1gFnSc4	Natalie
Rushmanovou	Rushmanová	k1gFnSc4	Rushmanová
jako	jako	k8xS	jako
svoji	svůj	k3xOyFgFnSc4	svůj
novou	nový	k2eAgFnSc4d1	nová
asistentku	asistentka	k1gFnSc4	asistentka
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
odletí	odletět	k5eAaPmIp3nP	odletět
do	do	k7c2	do
Monaka	Monako	k1gNnSc2	Monako
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Stark	Stark	k1gInSc1	Stark
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
historické	historický	k2eAgNnSc4d1	historické
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
závodu	závod	k1gInSc6	závod
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
napaden	napadnout	k5eAaPmNgInS	napadnout
Ivanem	Ivan	k1gMnSc7	Ivan
Vankem	Vanek	k1gMnSc7	Vanek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
elektrické	elektrický	k2eAgInPc4d1	elektrický
biče	bič	k1gInPc4	bič
<g/>
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
si	se	k3xPyFc3	se
nasadí	nasadit	k5eAaPmIp3nS	nasadit
oblek	oblek	k1gInSc1	oblek
Iron	iron	k1gInSc1	iron
Mana	Man	k1gMnSc2	Man
a	a	k8xC	a
ruského	ruský	k2eAgMnSc2d1	ruský
vědce	vědec	k1gMnSc2	vědec
porazí	porazit	k5eAaPmIp3nS	porazit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
výstroj	výstroj	k1gFnSc1	výstroj
utrpí	utrpět	k5eAaPmIp3nS	utrpět
vážné	vážný	k2eAgFnPc4d1	vážná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Vanko	Vanko	k6eAd1	Vanko
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
dokázat	dokázat	k5eAaPmF	dokázat
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
není	být	k5eNaImIp3nS	být
neporazitelný	porazitelný	k2eNgInSc1d1	neporazitelný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
udělá	udělat	k5eAaPmIp3nS	udělat
dojem	dojem	k1gInSc4	dojem
na	na	k7c4	na
Justina	Justin	k1gMnSc4	Justin
Hammera	Hammer	k1gMnSc4	Hammer
<g/>
,	,	kIx,	,
Starkova	Starkův	k2eAgMnSc4d1	Starkův
rivala	rival	k1gMnSc4	rival
v	v	k7c6	v
podnikání	podnikání	k1gNnSc6	podnikání
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nafinguje	nafingovat	k5eAaBmIp3nS	nafingovat
Vankovu	Vankův	k2eAgFnSc4d1	Vankova
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
dostane	dostat	k5eAaPmIp3nS	dostat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
požádá	požádat	k5eAaPmIp3nS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mnoha	mnoho	k4c2	mnoho
obrněných	obrněný	k2eAgInPc2d1	obrněný
obleků	oblek	k1gInPc2	oblek
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
chce	chtít	k5eAaImIp3nS	chtít
zastínit	zastínit	k5eAaPmF	zastínit
Tonyho	Tony	k1gMnSc4	Tony
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
narozeninového	narozeninový	k2eAgInSc2d1	narozeninový
večírku	večírek	k1gInSc2	večírek
doma	doma	k6eAd1	doma
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
(	(	kIx(	(
<g/>
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
poslední	poslední	k2eAgFnSc4d1	poslední
<g/>
)	)	kIx)	)
v	v	k7c4	v
Iron	iron	k1gInSc4	iron
Manově	manově	k6eAd1	manově
obleku	oblek	k1gInSc2	oblek
opije	opít	k5eAaPmIp3nS	opít
<g/>
.	.	kIx.	.
</s>
<s>
Znechucený	znechucený	k2eAgMnSc1d1	znechucený
Starkův	Starkův	k2eAgMnSc1d1	Starkův
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
,	,	kIx,	,
podplukovník	podplukovník	k1gMnSc1	podplukovník
Rhodes	Rhodes	k1gMnSc1	Rhodes
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
proto	proto	k6eAd1	proto
oblékne	obléknout	k5eAaPmIp3nS	obléknout
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
verzí	verze	k1gFnPc2	verze
a	a	k8xC	a
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
zkrotit	zkrotit	k5eAaPmF	zkrotit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
boj	boj	k1gInSc1	boj
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Rhodes	Rhodes	k1gMnSc1	Rhodes
svůj	svůj	k3xOyFgInSc4	svůj
oblek	oblek	k1gInSc4	oblek
zabaví	zabavit	k5eAaPmIp3nP	zabavit
<g/>
,	,	kIx,	,
odletí	odletět	k5eAaPmIp3nP	odletět
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
přenechá	přenechat	k5eAaPmIp3nS	přenechat
ho	on	k3xPp3gInSc4	on
americkému	americký	k2eAgNnSc3d1	americké
letectvu	letectvo	k1gNnSc3	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Nick	Nick	k1gInSc1	Nick
Fury	Fura	k1gFnSc2	Fura
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
agentury	agentura	k1gFnSc2	agentura
S.	S.	kA	S.
<g/>
H.I.E.L.D.	H.I.E.L.D.	k1gMnSc2	H.I.E.L.D.
<g/>
,	,	kIx,	,
osloví	oslovit	k5eAaPmIp3nS	oslovit
Starka	starka	k1gFnSc1	starka
<g/>
,	,	kIx,	,
odhalí	odhalit	k5eAaPmIp3nS	odhalit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rushmanová	Rushmanová	k1gFnSc1	Rushmanová
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
agentka	agentka	k1gFnSc1	agentka
Nataša	Nataša	k1gFnSc1	Nataša
Romanovová	Romanovová	k1gFnSc1	Romanovová
a	a	k8xC	a
že	že	k8xS	že
Howard	Howard	k1gMnSc1	Howard
Stark	Stark	k1gInSc1	Stark
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
znal	znát	k5eAaImAgMnS	znát
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
S.	S.	kA	S.
<g/>
H.I.E.L.D.u.	H.I.E.L.D.u.	k1gMnSc7	H.I.E.L.D.u.
Také	také	k9	také
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vankův	Vankův	k2eAgMnSc1d1	Vankův
otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
Howardem	Howard	k1gInSc7	Howard
na	na	k7c6	na
vynálezu	vynález	k1gInSc6	vynález
obloukového	obloukový	k2eAgInSc2d1	obloukový
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ho	on	k3xPp3gInSc4	on
Anton	Anton	k1gMnSc1	Anton
zkusil	zkusit	k5eAaPmAgMnS	zkusit
prodat	prodat	k5eAaPmF	prodat
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
jej	on	k3xPp3gInSc4	on
Stark	Stark	k1gInSc4	Stark
deportovat	deportovat	k5eAaBmF	deportovat
a	a	k8xC	a
Sověti	Sovět	k1gMnPc1	Sovět
ho	on	k3xPp3gMnSc4	on
uvěznili	uvěznit	k5eAaPmAgMnP	uvěznit
v	v	k7c6	v
gulagu	gulag	k1gInSc6	gulag
<g/>
.	.	kIx.	.
</s>
<s>
Fury	Fur	k1gMnPc4	Fur
rovněž	rovněž	k9	rovněž
předá	předat	k5eAaPmIp3nS	předat
Tonymu	Tony	k1gMnSc3	Tony
některé	některý	k3yIgFnPc1	některý
staré	starý	k2eAgFnPc1d1	stará
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
zkoumání	zkoumání	k1gNnSc6	zkoumání
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
diorama	diorama	k1gFnSc1	diorama
výstavy	výstava	k1gFnSc2	výstava
Stark	Stark	k1gInSc1	Stark
Expo	Expo	k1gNnSc1	Expo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
skrývá	skrývat	k5eAaImIp3nS	skrývat
v	v	k7c6	v
sobě	se	k3xPyFc3	se
strukturu	struktura	k1gFnSc4	struktura
nového	nový	k2eAgInSc2d1	nový
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
svého	svůj	k3xOyFgInSc2	svůj
počítače	počítač	k1gInSc2	počítač
J.A.	J.A.	k1gFnPc2	J.A.
<g/>
R.	R.	kA	R.
<g/>
V.I.	V.I.	k1gFnSc2	V.I.
<g/>
S.	S.	kA	S.
<g/>
e	e	k0	e
ho	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vanko	Vanko	k1gNnSc1	Vanko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
živý	živý	k2eAgMnSc1d1	živý
<g/>
,	,	kIx,	,
vloží	vložit	k5eAaPmIp3nS	vložit
nový	nový	k2eAgInSc1d1	nový
prvek	prvek	k1gInSc1	prvek
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
obloukového	obloukový	k2eAgInSc2d1	obloukový
reaktoru	reaktor	k1gInSc2	reaktor
v	v	k7c6	v
hrudníku	hrudník	k1gInSc6	hrudník
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ukončí	ukončit	k5eAaPmIp3nS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
palladiu	palladion	k1gNnSc6	palladion
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Expu	Expo	k1gNnSc6	Expo
mezitím	mezitím	k6eAd1	mezitím
Hammer	Hammer	k1gMnSc1	Hammer
představí	představit	k5eAaPmIp3nS	představit
Vankovy	Vankův	k2eAgInPc4d1	Vankův
ozbrojené	ozbrojený	k2eAgInPc4d1	ozbrojený
drony	dron	k1gInPc4	dron
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vede	vést	k5eAaImIp3nS	vést
Rhodes	Rhodes	k1gInSc1	Rhodes
ve	v	k7c6	v
Starkově	Starkův	k2eAgInSc6d1	Starkův
obleku	oblek	k1gInSc6	oblek
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
těžce	těžce	k6eAd1	těžce
vyzbrojil	vyzbrojit	k5eAaPmAgMnS	vyzbrojit
právě	právě	k9	právě
Hammer	Hammer	k1gMnSc1	Hammer
<g/>
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
dorazí	dorazit	k5eAaPmIp3nS	dorazit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vylepšené	vylepšený	k2eAgFnSc6d1	vylepšená
výstroji	výstroj	k1gFnSc6	výstroj
a	a	k8xC	a
varuje	varovat	k5eAaImIp3nS	varovat
Rhodese	Rhodese	k1gFnSc1	Rhodese
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Vanko	Vanko	k1gNnSc1	Vanko
dálkově	dálkově	k6eAd1	dálkově
převezme	převzít	k5eAaPmIp3nS	převzít
ovládání	ovládání	k1gNnSc1	ovládání
nad	nad	k7c7	nad
drony	dron	k1gInPc7	dron
i	i	k8xC	i
Rhodesovým	Rhodesový	k2eAgInSc7d1	Rhodesový
oblekem	oblek	k1gInSc7	oblek
a	a	k8xC	a
na	na	k7c4	na
Iron	iron	k1gInSc4	iron
Mana	Man	k1gMnSc2	Man
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Hammer	Hammer	k1gMnSc1	Hammer
je	být	k5eAaImIp3nS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
Romanovová	Romanovová	k1gFnSc1	Romanovová
společně	společně	k6eAd1	společně
se	s	k7c7	s
Starkovým	Starkův	k2eAgMnSc7d1	Starkův
bodyguardem	bodyguard	k1gMnSc7	bodyguard
Happy	Happa	k1gFnSc2	Happa
Hoganem	Hogan	k1gMnSc7	Hogan
zamíří	zamířit	k5eAaPmIp3nS	zamířit
pro	pro	k7c4	pro
ruského	ruský	k2eAgMnSc4d1	ruský
vědce	vědec	k1gMnSc4	vědec
do	do	k7c2	do
Hammerovy	Hammerův	k2eAgFnSc2d1	Hammerova
továrny	továrna	k1gFnSc2	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Vanko	Vanko	k6eAd1	Vanko
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
agentka	agentka	k1gFnSc1	agentka
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vrátit	vrátit	k5eAaPmF	vrátit
ovládání	ovládání	k1gNnSc4	ovládání
Rhodesova	Rhodesův	k2eAgInSc2d1	Rhodesův
obleku	oblek	k1gInSc2	oblek
jeho	jeho	k3xOp3gMnSc7	jeho
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
boji	boj	k1gInSc6	boj
Stark	Stark	k1gInSc1	Stark
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kamarádem	kamarád	k1gMnSc7	kamarád
Vanka	Vanko	k1gNnSc2	Vanko
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc2	jeho
drony	drona	k1gFnSc2	drona
porazí	porazit	k5eAaPmIp3nS	porazit
a	a	k8xC	a
Rus	Rus	k1gMnSc1	Rus
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
odpálením	odpálení	k1gNnSc7	odpálení
své	svůj	k3xOyFgFnSc2	svůj
výstroje	výstroj	k1gFnSc2	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
běží	běžet	k5eAaImIp3nS	běžet
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c4	o
řádění	řádění	k1gNnSc4	řádění
Hulka	Hulka	k1gMnSc1	Hulka
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Fury	Fura	k1gFnSc2	Fura
informuje	informovat	k5eAaBmIp3nS	informovat
Tonyho	Tony	k1gMnSc4	Tony
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
složité	složitý	k2eAgFnPc4d1	složitá
osobnosti	osobnost	k1gFnPc4	osobnost
se	se	k3xPyFc4	se
S.	S.	kA	S.
<g/>
H.I.E.L.D.	H.I.E.L.D.	k1gMnSc1	H.I.E.L.D.
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
využít	využít	k5eAaPmF	využít
jej	on	k3xPp3gMnSc4	on
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
konzultanta	konzultant	k1gMnSc4	konzultant
<g/>
.	.	kIx.	.
</s>
<s>
Stark	Stark	k1gInSc4	Stark
a	a	k8xC	a
Rhodes	Rhodes	k1gInSc4	Rhodes
poté	poté	k6eAd1	poté
dostanou	dostat	k5eAaPmIp3nP	dostat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
hrdinství	hrdinství	k1gNnSc4	hrdinství
od	od	k7c2	od
senátora	senátor	k1gMnSc2	senátor
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Agent	agent	k1gMnSc1	agent
Phil	Phil	k1gMnSc1	Phil
Coulson	Coulson	k1gMnSc1	Coulson
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
Starkovi	Starek	k1gMnSc3	Starek
také	také	k6eAd1	také
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
<g/>
,	,	kIx,	,
dorazí	dorazit	k5eAaPmIp3nS	dorazit
do	do	k7c2	do
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ohlásí	ohlásit	k5eAaPmIp3nS	ohlásit
nalezení	nalezení	k1gNnSc4	nalezení
velkého	velký	k2eAgNnSc2d1	velké
kladiva	kladivo	k1gNnSc2	kladivo
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Radovan	Radovan	k1gMnSc1	Radovan
Vaculík	Vaculík	k1gMnSc1	Vaculík
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Tony	Tony	k1gMnSc1	Tony
Stark	Stark	k1gInSc1	Stark
/	/	kIx~	/
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
Gwyneth	Gwyneth	k1gMnSc1	Gwyneth
Paltrowová	Paltrowová	k1gFnSc1	Paltrowová
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Simona	Simona	k1gFnSc1	Simona
Vrbická	Vrbická	k1gFnSc1	Vrbická
<g/>
)	)	kIx)	)
jako	jako	k9	jako
Virginia	Virginium	k1gNnSc2	Virginium
"	"	kIx"	"
<g/>
Pepper	Pepper	k1gInSc1	Pepper
<g/>
"	"	kIx"	"
Pottsová	Pottsová	k1gFnSc1	Pottsová
Don	dona	k1gFnPc2	dona
Cheadle	Cheadlo	k1gNnSc6	Cheadlo
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Bohdan	Bohdan	k1gMnSc1	Bohdan
<g />
.	.	kIx.	.
</s>
<s>
Tůma	Tůma	k1gMnSc1	Tůma
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
podplukovník	podplukovník	k1gMnSc1	podplukovník
James	James	k1gMnSc1	James
"	"	kIx"	"
<g/>
Rhodey	Rhodey	k1gInPc1	Rhodey
<g/>
"	"	kIx"	"
Rhodes	Rhodes	k1gInSc1	Rhodes
Scarlett	Scarlett	k1gInSc1	Scarlett
Johanssonová	Johanssonová	k1gFnSc1	Johanssonová
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Jitka	Jitka	k1gFnSc1	Jitka
Moučková	Moučková	k1gFnSc1	Moučková
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Natalie	Natalie	k1gFnSc1	Natalie
Rushmanová	Rushmanová	k1gFnSc1	Rushmanová
/	/	kIx~	/
agentka	agentka	k1gFnSc1	agentka
Nataša	Nataša	k1gFnSc1	Nataša
Romanovová	Romanovová	k1gFnSc1	Romanovová
/	/	kIx~	/
Black	Black	k1gMnSc1	Black
Widow	Widow	k1gMnSc1	Widow
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Natasha	Natasha	k1gMnSc1	Natasha
Romanoff	Romanoff	k1gMnSc1	Romanoff
<g/>
)	)	kIx)	)
Sam	Sam	k1gMnSc1	Sam
Rockwell	Rockwell	k1gMnSc1	Rockwell
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Justin	Justin	k1gMnSc1	Justin
Hammer	Hammra	k1gFnPc2	Hammra
Clark	Clark	k1gInSc1	Clark
Gregg	Gregg	k1gMnSc1	Gregg
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Mahdal	Mahdal	k1gMnSc1	Mahdal
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
agent	agent	k1gMnSc1	agent
Phil	Phil	k1gMnSc1	Phil
Coulson	Coulson	k1gMnSc1	Coulson
John	John	k1gMnSc1	John
Slattery	Slatter	k1gInPc4	Slatter
(	(	kIx(	(
<g/>
český	český	k2eAgInSc4d1	český
dabing	dabing	k1gInSc4	dabing
<g/>
:	:	kIx,	:
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hruška	Hruška	k1gMnSc1	Hruška
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Howard	Howard	k1gInSc1	Howard
Stark	Stark	k1gInSc1	Stark
Mickey	Micke	k2eAgInPc1d1	Micke
Rourke	Rourk	k1gInPc1	Rourk
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Lukáš	Lukáš	k1gMnSc1	Lukáš
Hlavica	Hlavica	k1gMnSc1	Hlavica
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xC	jako
Ivan	Ivan	k1gMnSc1	Ivan
Vanko	Vanko	k1gNnSc1	Vanko
Samuel	Samuel	k1gMnSc1	Samuel
L.	L.	kA	L.
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Rímský	Rímský	k1gMnSc1	Rímský
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
ředitel	ředitel	k1gMnSc1	ředitel
Nick	Nick	k1gMnSc1	Nick
Fury	Fura	k1gFnSc2	Fura
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
také	také	k9	také
Paul	Paul	k1gMnSc1	Paul
Bettany	Bettan	k1gInPc1	Bettan
(	(	kIx(	(
<g/>
hlas	hlas	k1gInSc1	hlas
J.A.	J.A.	k1gFnPc2	J.A.
<g/>
R.	R.	kA	R.
<g/>
V.I.	V.I.	k1gFnSc2	V.I.
<g/>
S.	S.	kA	S.
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jon	Jon	k1gMnSc1	Jon
Favreau	Favreaus	k1gInSc2	Favreaus
(	(	kIx(	(
<g/>
Happy	Happ	k1gInPc4	Happ
Hogan	Hogan	k1gInSc1	Hogan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leslie	Leslie	k1gFnSc1	Leslie
Bibbová	Bibbová	k1gFnSc1	Bibbová
(	(	kIx(	(
<g/>
Christine	Christin	k1gInSc5	Christin
Everhartová	Everhartový	k2eAgNnPc4d1	Everhartový
<g/>
)	)	kIx)	)
a	a	k8xC	a
Garry	Garra	k1gFnPc1	Garra
Shandling	Shandling	k1gInSc1	Shandling
(	(	kIx(	(
<g/>
senátor	senátor	k1gMnSc1	senátor
Stern	sternum	k1gNnPc2	sternum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cameo	cameo	k6eAd1	cameo
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
Stan	stan	k1gInSc4	stan
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Elon	Elon	k1gInSc4	Elon
Musk	Muska	k1gFnPc2	Muska
a	a	k8xC	a
Larry	Larra	k1gFnSc2	Larra
Ellison	Ellisona	k1gFnPc2	Ellisona
(	(	kIx(	(
<g/>
zahráli	zahrát	k5eAaPmAgMnP	zahrát
si	se	k3xPyFc3	se
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
filmu	film	k1gInSc2	film
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
oznámila	oznámit	k5eAaPmAgFnS	oznámit
společnost	společnost	k1gFnSc1	společnost
Marvel	Marvel	k1gMnSc1	Marvel
Studios	Studios	k?	Studios
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
sequelu	sequel	k1gInSc2	sequel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
plátna	plátno	k1gNnPc4	plátno
zamířit	zamířit	k5eAaPmF	zamířit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgMnS	získat
post	post	k1gInSc4	post
režiséra	režisér	k1gMnSc2	režisér
opět	opět	k6eAd1	opět
Jon	Jon	k1gMnSc4	Jon
Favreau	Favreaa	k1gMnSc4	Favreaa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
novým	nový	k2eAgMnSc7d1	nový
scenáristou	scenárista	k1gMnSc7	scenárista
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Justin	Justin	k1gMnSc1	Justin
Theroux	Theroux	k1gInSc4	Theroux
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
role	role	k1gFnSc2	role
Tonyho	Tony	k1gMnSc2	Tony
Starka	starka	k1gFnSc1	starka
byl	být	k5eAaImAgInS	být
opětovně	opětovně	k6eAd1	opětovně
obsazen	obsazen	k2eAgMnSc1d1	obsazen
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postava	postava	k1gFnSc1	postava
Jamese	Jamese	k1gFnSc1	Jamese
Rhodese	Rhodese	k1gFnSc1	Rhodese
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
přeobsazena	přeobsazen	k2eAgFnSc1d1	přeobsazen
–	–	k?	–
místo	místo	k1gNnSc4	místo
Terrence	Terrence	k1gFnSc2	Terrence
Howarda	Howarda	k1gFnSc1	Howarda
jej	on	k3xPp3gMnSc4	on
nově	nově	k6eAd1	nově
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Don	Don	k1gMnSc1	Don
Cheadle	Cheadlo	k1gNnSc6	Cheadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byli	být	k5eAaImAgMnP	být
oznámeni	oznámen	k2eAgMnPc1d1	oznámen
další	další	k2eAgMnPc1d1	další
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Samuela	Samuel	k1gMnSc2	Samuel
L.	L.	kA	L.
Jacksona	Jackson	k1gMnSc2	Jackson
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
devět	devět	k4xCc4	devět
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
filmu	film	k1gInSc2	film
s	s	k7c7	s
rozpočtem	rozpočet	k1gInSc7	rozpočet
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
probíhalo	probíhat	k5eAaImAgNnS	probíhat
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
potitulkovou	potitulkový	k2eAgFnSc4d1	potitulková
scénu	scéna	k1gFnSc4	scéna
režíroval	režírovat	k5eAaImAgMnS	režírovat
Kenneth	Kenneth	k1gMnSc1	Kenneth
Branagh	Branagh	k1gMnSc1	Branagh
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
filmu	film	k1gInSc2	film
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
2	[number]	k4	2
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kin	kino	k1gNnPc2	kino
byl	být	k5eAaImAgInS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
v	v	k7c6	v
kinodistribuci	kinodistribuce	k1gFnSc6	kinodistribuce
objevil	objevit	k5eAaPmAgInS	objevit
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
utržil	utržit	k5eAaPmAgInS	utržit
snímek	snímek	k1gInSc1	snímek
312	[number]	k4	312
433	[number]	k4	433
331	[number]	k4	331
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
zemích	zem	k1gFnPc6	zem
dalších	další	k2eAgInPc2d1	další
311	[number]	k4	311
500	[number]	k4	500
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětové	celosvětový	k2eAgFnPc1d1	celosvětová
tržby	tržba	k1gFnPc1	tržba
tak	tak	k6eAd1	tak
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
623	[number]	k4	623
933	[number]	k4	933
331	[number]	k4	331
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
o	o	k7c4	o
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
překonal	překonat	k5eAaPmAgInS	překonat
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Kinobox	Kinobox	k1gInSc1	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
26	[number]	k4	26
recenzí	recenze	k1gFnPc2	recenze
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
ohodnotil	ohodnotit	k5eAaPmAgInS	ohodnotit
film	film	k1gInSc1	film
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
2	[number]	k4	2
63	[number]	k4	63
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Rotten	Rotten	k2eAgMnSc1d1	Rotten
Tomatoes	Tomatoes	k1gMnSc1	Tomatoes
udělil	udělit	k5eAaPmAgMnS	udělit
snímku	snímka	k1gFnSc4	snímka
známku	známka	k1gFnSc4	známka
6,5	[number]	k4	6,5
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
277	[number]	k4	277
recenzí	recenze	k1gFnPc2	recenze
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
201	[number]	k4	201
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
spokojených	spokojený	k2eAgMnPc2d1	spokojený
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
72	[number]	k4	72
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
serveru	server	k1gInSc2	server
Metacritic	Metacritice	k1gFnPc2	Metacritice
získal	získat	k5eAaPmAgMnS	získat
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
40	[number]	k4	40
recenzí	recenze	k1gFnPc2	recenze
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
57	[number]	k4	57
ze	z	k7c2	z
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
2	[number]	k4	2
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepších	dobrý	k2eAgInPc2d3	nejlepší
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
žánrové	žánrový	k2eAgFnPc4d1	žánrová
ceny	cena	k1gFnPc4	cena
Saturn	Saturn	k1gInSc1	Saturn
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
kategorie	kategorie	k1gFnSc2	kategorie
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
sci-fi	scii	k1gNnSc1	sci-fi
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
komerčnímu	komerční	k2eAgInSc3d1	komerční
úspěchu	úspěch	k1gInSc3	úspěch
snímku	snímek	k1gInSc2	snímek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
filmový	filmový	k2eAgMnSc1d1	filmový
sequel	sequet	k5eAaPmAgMnS	sequet
Iron	iron	k1gInSc4	iron
Man	mana	k1gFnPc2	mana
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
součástí	součást	k1gFnSc7	součást
série	série	k1gFnSc2	série
Marvel	Marvela	k1gFnPc2	Marvela
Cinematic	Cinematice	k1gFnPc2	Cinematice
Universe	Universe	k1gFnSc2	Universe
(	(	kIx(	(
<g/>
MCU	MCU	kA	MCU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
Robert	Robert	k1gMnSc1	Robert
Downey	Downea	k1gFnSc2	Downea
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
Tony	Tony	k1gMnSc1	Tony
Stark	Stark	k1gInSc1	Stark
/	/	kIx~	/
Iron	iron	k1gInSc1	iron
Man	mana	k1gFnPc2	mana
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
celovečerních	celovečerní	k2eAgInPc6d1	celovečerní
filmech	film	k1gInPc6	film
MCU	MCU	kA	MCU
<g/>
.	.	kIx.	.
</s>
