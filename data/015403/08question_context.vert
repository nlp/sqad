<s>
Manus	Manus	k1gInSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
ruka	ruka	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
právní	právní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
měl	mít	k5eAaImAgInS
v	v	k7c6
antickém	antický	k2eAgInSc6d1
Římě	Řím	k1gInSc6
otec	otec	k1gMnSc1
rodiny	rodina	k1gFnSc2
(	(	kIx(
<g/>
pater	patro	k1gNnPc2
familias	familiasa	k1gFnPc2
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
manžel	manžel	k1gMnSc1
nad	nad	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
<g/>
-li	-li	k?
uzavřeno	uzavřít	k5eAaPmNgNnS
„	„	k?
<g/>
matrimonium	matrimonium	k1gNnSc1
cum	cum	k?
in	in	k?
manum	manum	k?
conventione	convention	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
manželství	manželství	k1gNnSc1
s	s	k7c7
manželskou	manželský	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
nad	nad	k7c7
ženou	žena	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>