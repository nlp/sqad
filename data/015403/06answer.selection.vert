<s>
Nutno	nutno	k6eAd1
ovšem	ovšem	k9
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
přísné	přísný	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
bylo	být	k5eAaImAgNnS
už	už	k6eAd1
za	za	k7c2
doby	doba	k1gFnSc2
republiky	republika	k1gFnSc2
nahrazováno	nahrazovat	k5eAaImNgNnS
méně	málo	k6eAd2
přísným	přísný	k2eAgNnPc3d1
manželstvím	manželství	k1gNnPc3
„	„	k?
<g/>
sine	sinus	k1gInSc5
in	in	k?
manum	manum	k?
conventione	convention	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
bez	bez	k7c2
manželské	manželský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
již	již	k6eAd1
na	na	k7c6
sklonku	sklonek	k1gInSc6
republiky	republika	k1gFnSc2
bylo	být	k5eAaImAgNnS
častější	častý	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>