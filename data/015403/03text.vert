<s>
Manus	Manus	k1gInSc1
(	(	kIx(
<g/>
římské	římský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Manus	Manus	k1gInSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
ruka	ruka	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
právní	právní	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
měl	mít	k5eAaImAgInS
v	v	k7c6
antickém	antický	k2eAgInSc6d1
Římě	Řím	k1gInSc6
otec	otec	k1gMnSc1
rodiny	rodina	k1gFnSc2
(	(	kIx(
<g/>
pater	patro	k1gNnPc2
familias	familiasa	k1gFnPc2
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
manžel	manžel	k1gMnSc1
nad	nad	k7c7
svou	svůj	k3xOyFgFnSc7
manželkou	manželka	k1gFnSc7
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
<g/>
-li	-li	k?
uzavřeno	uzavřít	k5eAaPmNgNnS
„	„	k?
<g/>
matrimonium	matrimonium	k1gNnSc1
cum	cum	k?
in	in	k?
manum	manum	k?
conventione	convention	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
manželství	manželství	k1gNnSc1
s	s	k7c7
manželskou	manželský	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
nad	nad	k7c7
ženou	žena	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žena	žena	k1gFnSc1
podrobená	podrobený	k2eAgFnSc1d1
této	tento	k3xDgFnSc3
moci	moc	k1gFnSc3
měla	mít	k5eAaImAgFnS
v	v	k7c6
rodině	rodina	k1gFnSc6
stejné	stejný	k2eAgNnSc1d1
právní	právní	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
jako	jako	k8xS,k8xC
dcera	dcera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
tak	tak	k6eAd1
podrobena	podrobit	k5eAaPmNgFnS
domácímu	domácí	k2eAgInSc3d1
soudu	soud	k1gInSc3
svého	svůj	k3xOyFgMnSc4
manžela	manžel	k1gMnSc4
<g/>
,	,	kIx,
nebyla	být	k5eNaImAgFnS
způsobilá	způsobilý	k2eAgFnSc1d1
mít	mít	k5eAaImF
jakýkoli	jakýkoli	k3yIgInSc4
vlastní	vlastní	k2eAgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
cokoli	cokoli	k3yInSc4
nabývala	nabývat	k5eAaImAgFnS
<g/>
,	,	kIx,
nabývala	nabývat	k5eAaImAgFnS
pro	pro	k7c4
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Uzavření	uzavření	k1gNnSc1
manželství	manželství	k1gNnSc2
in	in	k?
manum	manum	k?
conventione	convention	k1gInSc5
mohlo	moct	k5eAaImAgNnS
proběhnout	proběhnout	k5eAaPmF
třemi	tři	k4xCgInPc7
způsoby	způsob	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
confarreatio	confarreatio	k6eAd1
byla	být	k5eAaImAgFnS
prastará	prastarý	k2eAgFnSc1d1
forma	forma	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
hlavně	hlavně	k9
v	v	k7c6
kruzích	kruh	k1gInPc6
kněžských	kněžský	k2eAgFnPc2d1
<g/>
,	,	kIx,
při	při	k7c6
ní	on	k3xPp3gFnSc6
ženich	ženich	k1gMnSc1
s	s	k7c7
nevěstou	nevěsta	k1gFnSc7
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
10	#num#	k4
svědků	svědek	k1gMnPc2
a	a	k8xC
dvou	dva	k4xCgNnPc2
kněží	kněz	k1gMnPc2
podstupovali	podstupovat	k5eAaImAgMnP
různé	různý	k2eAgInPc4d1
ceremoniální	ceremoniální	k2eAgInPc4d1
úkony	úkon	k1gInPc4
a	a	k8xC
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k8xC
obětovali	obětovat	k5eAaBmAgMnP
Jupiterovi	Jupiter	k1gMnSc3
špaldový	špaldový	k2eAgInSc4d1
chléb	chléb	k1gInSc4
(	(	kIx(
<g/>
panis	panis	k1gFnSc1
farreus	farreus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
dal	dát	k5eAaPmAgMnS
celému	celý	k2eAgInSc3d1
obřadu	obřad	k1gInSc6
název	název	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
coemptio	coemptio	k6eAd1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
prodej	prodej	k1gInSc1
nevěsty	nevěsta	k1gFnSc2
ženichovi	ženichův	k2eAgMnPc1d1
(	(	kIx(
<g/>
manželství	manželství	k1gNnPc1
koupí	koupě	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
vznikl	vzniknout	k5eAaPmAgInS
rituál	rituál	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
udávána	udávat	k5eAaImNgFnS
již	již	k9
cena	cena	k1gFnSc1
symbolická	symbolický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
usus	usus	k1gInSc4
bylo	být	k5eAaImAgNnS
jakési	jakýsi	k3yIgNnSc1
„	„	k?
<g/>
vydržení	vydržení	k1gNnPc2
<g/>
“	“	k?
manželské	manželský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
nad	nad	k7c7
ženou	žena	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žil	žít	k5eAaImAgMnS
<g/>
-li	-li	k?
muž	muž	k1gMnSc1
se	s	k7c7
ženou	žena	k1gFnSc7
rok	rok	k1gInSc1
jako	jako	k8xS,k8xC
manžel	manžel	k1gMnSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
mu	on	k3xPp3gMnSc3
nad	nad	k7c4
ní	on	k3xPp3gFnSc3
automaticky	automaticky	k6eAd1
manus	manus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nutno	nutno	k6eAd1
ovšem	ovšem	k9
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc1
přísné	přísný	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
bylo	být	k5eAaImAgNnS
už	už	k6eAd1
za	za	k7c2
doby	doba	k1gFnSc2
republiky	republika	k1gFnSc2
nahrazováno	nahrazovat	k5eAaImNgNnS
méně	málo	k6eAd2
přísným	přísný	k2eAgNnPc3d1
manželstvím	manželství	k1gNnPc3
„	„	k?
<g/>
sine	sinus	k1gInSc5
in	in	k?
manum	manum	k?
conventione	convention	k1gInSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
bez	bez	k7c2
manželské	manželský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
již	již	k6eAd1
na	na	k7c6
sklonku	sklonek	k1gInSc6
republiky	republika	k1gFnSc2
bylo	být	k5eAaImAgNnS
častější	častý	k2eAgNnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Manus	Manus	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověký	starověký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
</s>
