<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgInSc4d1	přezdívaný
kronikářem	kronikář	k1gMnSc7	kronikář
Kosmou	Kosma	k1gMnSc7	Kosma
"	"	kIx"	"
<g/>
český	český	k2eAgMnSc1d1	český
Achilles	Achilles	k1gMnSc1	Achilles
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
1002	[number]	k4	1002
až	až	k9	až
1005	[number]	k4	1005
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1055	[number]	k4	1055
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1029	[number]	k4	1029
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
letech	let	k1gInPc6	let
1034	[number]	k4	1034
<g/>
–	–	k?	–
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1002	[number]	k4	1002
jako	jako	k8xS	jako
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
družky	družka	k1gFnSc2	družka
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
snad	snad	k9	snad
druhé	druhý	k4xOgFnPc1	druhý
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
Boženy	Božena	k1gFnPc1	Božena
<g/>
.	.	kIx.	.
</s>
<s>
Břetislavův	Břetislavův	k2eAgMnSc1d1	Břetislavův
otec	otec	k1gMnSc1	otec
Oldřich	Oldřich	k1gMnSc1	Oldřich
zřejmě	zřejmě	k6eAd1	zřejmě
roku	rok	k1gInSc2	rok
1029	[number]	k4	1029
dobyl	dobýt	k5eAaPmAgInS	dobýt
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgMnS	porazit
polsko-moravské	polskooravský	k2eAgFnPc4d1	polsko-moravský
posádky	posádka	k1gFnPc4	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
,	,	kIx,	,
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
Oldřichův	Oldřichův	k2eAgMnSc1d1	Oldřichův
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
unesl	unést	k5eAaPmAgMnS	unést
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
ve	v	k7c6	v
Schweinfurtu	Schweinfurt	k1gInSc6	Schweinfurt
dceru	dcera	k1gFnSc4	dcera
bavorského	bavorský	k2eAgMnSc4d1	bavorský
velmože	velmož	k1gMnSc4	velmož
<g/>
,	,	kIx,	,
markraběte	markrabě	k1gMnSc4	markrabě
z	z	k7c2	z
Nordgau	Nordgaus	k1gInSc2	Nordgaus
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
Jitku	Jitka	k1gFnSc4	Jitka
ze	z	k7c2	z
Schweinfurtu	Schweinfurt	k1gInSc2	Schweinfurt
(	(	kIx(	(
<g/>
též	též	k9	též
Juditu	Judita	k1gFnSc4	Judita
<g/>
,	,	kIx,	,
Gutu	Guta	k1gFnSc4	Guta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
odjeli	odjet	k5eAaPmAgMnP	odjet
spolu	spolu	k6eAd1	spolu
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Břetislav	Břetislav	k1gMnSc1	Břetislav
z	z	k7c2	z
Oldřichovy	Oldřichův	k2eAgFnSc2d1	Oldřichova
vůle	vůle	k1gFnSc2	vůle
údělným	údělný	k2eAgMnSc7d1	údělný
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
s	s	k7c7	s
Jitkou	Jitka	k1gFnSc7	Jitka
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
také	také	k9	také
roku	rok	k1gInSc2	rok
1031	[number]	k4	1031
narodil	narodit	k5eAaPmAgMnS	narodit
jejich	jejich	k3xOp3gMnSc1	jejich
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
Spytihněv	Spytihněv	k1gMnSc1	Spytihněv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1033	[number]	k4	1033
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
předvolal	předvolat	k5eAaPmAgMnS	předvolat
(	(	kIx(	(
<g/>
důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
neochota	neochota	k1gFnSc1	neochota
podporovat	podporovat	k5eAaImF	podporovat
Konráda	Konrád	k1gMnSc4	Konrád
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvinil	obvinit	k5eAaPmAgMnS	obvinit
jej	on	k3xPp3gInSc4	on
z	z	k7c2	z
úkladů	úklad	k1gInPc2	úklad
<g/>
,	,	kIx,	,
sesadil	sesadit	k5eAaPmAgInS	sesadit
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
Oldřichův	Oldřichův	k2eAgMnSc1d1	Oldřichův
bratr	bratr	k1gMnSc1	bratr
a	a	k8xC	a
Břetislavův	Břetislavův	k2eAgMnSc1d1	Břetislavův
strýc	strýc	k1gMnSc1	strýc
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1034	[number]	k4	1034
však	však	k9	však
císař	císař	k1gMnSc1	císař
Oldřicha	Oldřich	k1gMnSc2	Oldřich
propustil	propustit	k5eAaPmAgInS	propustit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
mu	on	k3xPp3gMnSc3	on
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
knížectví	knížectví	k1gNnSc6	knížectví
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jaromír	Jaromír	k1gMnSc1	Jaromír
získá	získat	k5eAaPmIp3nS	získat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
úděl	úděl	k1gInSc1	úděl
a	a	k8xC	a
Břetislav	Břetislav	k1gMnSc1	Břetislav
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jaromíra	Jaromír	k1gMnSc4	Jaromír
dal	dát	k5eAaPmAgMnS	dát
ale	ale	k9	ale
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
oslepit	oslepit	k5eAaPmF	oslepit
a	a	k8xC	a
Břetislav	Břetislav	k1gMnSc1	Břetislav
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
snad	snad	k9	snad
přiostřilo	přiostřit	k5eAaPmAgNnS	přiostřit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Břetislav	Břetislav	k1gMnSc1	Břetislav
zřejmě	zřejmě	k6eAd1	zřejmě
Jaromíra	Jaromír	k1gMnSc4	Jaromír
podporoval	podporovat	k5eAaImAgInS	podporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Oldřicha	Oldřich	k1gMnSc2	Oldřich
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1034	[number]	k4	1034
byl	být	k5eAaImAgMnS	být
Břetislav	Břetislav	k1gMnSc1	Břetislav
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
Jaromíra	Jaromír	k1gMnSc2	Jaromír
jediným	jediný	k2eAgMnSc7d1	jediný
žijícím	žijící	k2eAgMnSc7d1	žijící
mužským	mužský	k2eAgMnSc7d1	mužský
Přemyslovcem	Přemyslovec	k1gMnSc7	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
trůn	trůn	k1gInSc4	trůn
usedl	usednout	k5eAaPmAgMnS	usednout
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzápětí	vzápětí	k6eAd1	vzápětí
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
v	v	k7c4	v
Břetislavův	Břetislavův	k2eAgInSc4d1	Břetislavův
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
nezachránilo	zachránit	k5eNaPmAgNnS	zachránit
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
Vršovci	Vršovec	k1gMnPc1	Vršovec
<g/>
,	,	kIx,	,
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výboje	výboj	k1gInSc2	výboj
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
===	===	k?	===
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
velice	velice	k6eAd1	velice
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
sebevědomý	sebevědomý	k2eAgMnSc1d1	sebevědomý
panovník	panovník	k1gMnSc1	panovník
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
upevnil	upevnit	k5eAaPmAgMnS	upevnit
a	a	k8xC	a
pojistil	pojistit	k5eAaPmAgInS	pojistit
svoji	svůj	k3xOyFgFnSc4	svůj
domácí	domácí	k2eAgFnSc4d1	domácí
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
prvního	první	k4xOgMnSc2	první
následníka	následník	k1gMnSc2	následník
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
své	svůj	k3xOyFgMnPc4	svůj
moci	moct	k5eAaImF	moct
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
polské	polský	k2eAgNnSc4d1	polské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1038	[number]	k4	1038
<g/>
–	–	k?	–
<g/>
1039	[number]	k4	1039
obsadil	obsadit	k5eAaPmAgInS	obsadit
Hnězdno	Hnězdno	k1gNnSc4	Hnězdno
<g/>
,	,	kIx,	,
v	v	k7c6	v
hnězdenské	hnězdenský	k2eAgFnSc6d1	Hnězdenská
katedrále	katedrála	k1gFnSc6	katedrála
nad	nad	k7c7	nad
hrobem	hrob	k1gInSc7	hrob
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
vydání	vydání	k1gNnSc4	vydání
prvního	první	k4xOgMnSc2	první
známého	známý	k1gMnSc2	známý
českého	český	k2eAgInSc2d1	český
zákoníku	zákoník	k1gInSc2	zákoník
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Břetislavova	Břetislavův	k2eAgFnSc1d1	Břetislavova
dekreta	dekreta	k1gFnSc1	dekreta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěchovy	Vojtěchův	k2eAgInPc1d1	Vojtěchův
ostatky	ostatek	k1gInPc1	ostatek
(	(	kIx(	(
<g/>
i	i	k8xC	i
ostatky	ostatek	k1gInPc1	ostatek
Svatého	svatý	k2eAgMnSc2d1	svatý
Radima	Radim	k1gMnSc2	Radim
a	a	k8xC	a
svatých	svatý	k2eAgMnPc2d1	svatý
Pěti	pět	k4xCc2	pět
bratří	bratr	k1gMnPc2	bratr
<g/>
)	)	kIx)	)
následně	následně	k6eAd1	následně
odvezl	odvézt	k5eAaPmAgInS	odvézt
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Doufal	doufat	k5eAaImAgMnS	doufat
zřejmě	zřejmě	k6eAd1	zřejmě
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
pomohly	pomoct	k5eAaPmAgFnP	pomoct
zajistit	zajistit	k5eAaPmF	zajistit
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
v	v	k7c6	v
Hnězdně	Hnězdno	k1gNnSc6	Hnězdno
<g/>
,	,	kIx,	,
prokážou	prokázat	k5eAaPmIp3nP	prokázat
stejnou	stejný	k2eAgFnSc4d1	stejná
službu	služba	k1gFnSc4	služba
i	i	k8xC	i
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jenže	jenže	k8xC	jenže
jeho	jeho	k3xOp3gNnSc1	jeho
protipolské	protipolský	k2eAgNnSc1d1	protipolský
tažení	tažení	k1gNnSc1	tažení
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
císaře	císař	k1gMnSc2	císař
<g/>
)	)	kIx)	)
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vzestup	vzestup	k1gInSc1	vzestup
moci	moc	k1gFnSc2	moc
sousedního	sousední	k2eAgMnSc2d1	sousední
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
svého	svůj	k3xOyFgMnSc4	svůj
vazala	vazal	k1gMnSc4	vazal
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
mladý	mladý	k2eAgMnSc1d1	mladý
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
připustit	připustit	k5eAaPmF	připustit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
povznesení	povznesení	k1gNnSc4	povznesení
Čech	Čechy	k1gFnPc2	Čechy
zřízením	zřízení	k1gNnSc7	zřízení
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
povýšením	povýšení	k1gNnSc7	povýšení
na	na	k7c6	na
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
vyhnaného	vyhnaný	k2eAgMnSc2d1	vyhnaný
piastovského	piastovský	k2eAgMnSc2d1	piastovský
knížete	kníže	k1gMnSc2	kníže
Kazimíra	Kazimír	k1gMnSc4	Kazimír
I.	I.	kA	I.
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
svých	svůj	k3xOyFgMnPc2	svůj
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
žádal	žádat	k5eAaImAgMnS	žádat
na	na	k7c6	na
Břetislavovi	Břetislav	k1gMnSc6	Břetislav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vrátil	vrátit	k5eAaPmAgMnS	vrátit
polskou	polský	k2eAgFnSc4d1	polská
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
vojáci	voják	k1gMnPc1	voják
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
předtím	předtím	k6eAd1	předtím
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
odporu	odpor	k1gInSc2	odpor
dobyl	dobýt	k5eAaPmAgInS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
ultimátem	ultimátum	k1gNnSc7	ultimátum
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
knížete	kníže	k1gNnSc4wR	kníže
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgInSc4	jenž
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
připravil	připravit	k5eAaPmAgInS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
konflikt	konflikt	k1gInSc4	konflikt
odvrátit	odvrátit	k5eAaPmF	odvrátit
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
jako	jako	k9	jako
rukojmí	rukojmí	k1gNnSc4	rukojmí
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Spytihněva	Spytihněv	k1gMnSc2	Spytihněv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konflikt	konflikt	k1gInSc1	konflikt
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Jindřichem	Jindřich	k1gMnSc7	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1040	[number]	k4	1040
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
s	s	k7c7	s
říšským	říšský	k2eAgNnSc7d1	říšské
vojskem	vojsko	k1gNnSc7	vojsko
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
již	již	k9	již
jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
česká	český	k2eAgFnSc1d1	Česká
výprava	výprava	k1gFnSc1	výprava
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
sem	sem	k6eAd1	sem
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
roku	rok	k1gInSc2	rok
1033	[number]	k4	1033
a	a	k8xC	a
porazil	porazit	k5eAaPmAgInS	porazit
knížete	kníže	k1gMnSc4	kníže
Oldřicha	Oldřich	k1gMnSc4	Oldřich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
postupovala	postupovat	k5eAaImAgNnP	postupovat
Jindřichova	Jindřichův	k2eAgNnPc1d1	Jindřichovo
vojska	vojsko	k1gNnPc1	vojsko
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
proudech	proud	k1gInPc6	proud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
vpád	vpád	k1gInSc1	vpád
byl	být	k5eAaImAgInS	být
odražen	odrazit	k5eAaPmNgInS	odrazit
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Brůdku	brůdek	k1gInSc2	brůdek
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
Chlumce	Chlumec	k1gInSc2	Chlumec
pod	pod	k7c7	pod
Krušnými	krušný	k2eAgFnPc7d1	krušná
horami	hora	k1gFnPc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
musel	muset	k5eAaImAgMnS	muset
uznat	uznat	k5eAaPmF	uznat
daný	daný	k2eAgInSc4d1	daný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Břetislavovým	Břetislavův	k2eAgMnSc7d1	Břetislavův
spojencem	spojenec	k1gMnSc7	spojenec
byl	být	k5eAaImAgMnS	být
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Petr	Petr	k1gMnSc1	Petr
Orseolo	Orseola	k1gFnSc5	Orseola
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obsadil	obsadit	k5eAaPmAgMnS	obsadit
jižní	jižní	k2eAgNnSc4d1	jižní
pomezí	pomezí	k1gNnSc4	pomezí
českého	český	k2eAgMnSc2d1	český
souseda	soused	k1gMnSc2	soused
a	a	k8xC	a
dovolil	dovolit	k5eAaPmAgMnS	dovolit
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
vyslat	vyslat	k5eAaPmF	vyslat
moravskou	moravský	k2eAgFnSc4d1	Moravská
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
hotovost	hotovost	k1gFnSc4	hotovost
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS	Spytihnět
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
bude	být	k5eAaImBp3nS	být
jednat	jednat	k5eAaImF	jednat
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
toužil	toužit	k5eAaImAgMnS	toužit
pomstít	pomstít	k5eAaPmF	pomstít
za	za	k7c4	za
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
pohraničním	pohraniční	k2eAgInPc3d1	pohraniční
zásekům	zásek	k1gInPc3	zásek
<g/>
,	,	kIx,	,
pronikl	proniknout	k5eAaPmAgMnS	proniknout
celkem	celkem	k6eAd1	celkem
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
země	zem	k1gFnSc2	zem
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
Břetislava	Břetislav	k1gMnSc4	Břetislav
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
biskup	biskup	k1gMnSc1	biskup
Šebíř	Šebíř	k1gMnSc1	Šebíř
i	i	k8xC	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
velmoži	velmož	k1gMnPc1	velmož
jsou	být	k5eAaImIp3nP	být
přístupni	přístupen	k2eAgMnPc1d1	přístupen
kompromisům	kompromis	k1gInPc3	kompromis
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
se	se	k3xPyFc4	se
Jindřichovi	Jindřichův	k2eAgMnPc1d1	Jindřichův
III	III	kA	III
<g/>
.	.	kIx.	.
podrobil	podrobit	k5eAaPmAgMnS	podrobit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgMnS	zabránit
dalšímu	další	k2eAgNnSc3d1	další
pustošení	pustošení	k1gNnSc3	pustošení
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
dostavil	dostavit	k5eAaPmAgMnS	dostavit
na	na	k7c4	na
říšský	říšský	k2eAgInSc4d1	říšský
sněm	sněm	k1gInSc4	sněm
do	do	k7c2	do
Řezna	Řezno	k1gNnSc2	Řezno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgNnP	být
přítomna	přítomen	k2eAgNnPc1d1	přítomno
přední	přední	k2eAgNnPc1d1	přední
knížata	kníže	k1gNnPc1	kníže
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
musel	muset	k5eAaImAgMnS	muset
Břetislav	Břetislav	k1gMnSc1	Břetislav
bosý	bosý	k2eAgMnSc1d1	bosý
a	a	k8xC	a
v	v	k7c6	v
rouše	roucho	k1gNnSc6	roucho
kajícníka	kajícník	k1gMnSc4	kajícník
padnout	padnout	k5eAaImF	padnout
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
k	k	k7c3	k
nohám	noha	k1gFnPc3	noha
<g/>
,	,	kIx,	,
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
žádat	žádat	k5eAaImF	žádat
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
také	také	k9	také
zříct	zříct	k5eAaPmF	zříct
moravského	moravský	k2eAgNnSc2d1	Moravské
území	území	k1gNnSc2	území
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Východní	východní	k2eAgFnSc2d1	východní
marky	marka	k1gFnSc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
udělena	udělen	k2eAgFnSc1d1	udělena
milost	milost	k1gFnSc1	milost
<g/>
,	,	kIx,	,
složil	složit	k5eAaPmAgMnS	složit
lenní	lenní	k2eAgInSc4d1	lenní
hold	hold	k1gInSc4	hold
a	a	k8xC	a
přísahu	přísaha	k1gFnSc4	přísaha
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
přijal	přijmout	k5eAaPmAgMnS	přijmout
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
dvě	dva	k4xCgNnPc1	dva
území	území	k1gNnPc1	území
(	(	kIx(	(
<g/>
soudí	soudit	k5eAaImIp3nP	soudit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
slezské	slezský	k2eAgNnSc4d1	Slezské
Vratislavsko	Vratislavsko	k1gNnSc4	Vratislavsko
a	a	k8xC	a
Hlohovsko	Hlohovsko	k1gNnSc4	Hlohovsko
<g/>
)	)	kIx)	)
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
formální	formální	k2eAgNnSc4d1	formální
podrobení	podrobení	k1gNnSc4	podrobení
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
Břetislava	Břetislav	k1gMnSc4	Břetislav
jako	jako	k9	jako
významného	významný	k2eAgMnSc4d1	významný
spojence	spojenec	k1gMnSc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
Břetislav	Břetislav	k1gMnSc1	Břetislav
již	již	k6eAd1	již
pevným	pevný	k2eAgMnSc7d1	pevný
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
ceněným	ceněný	k2eAgMnSc7d1	ceněný
spojencem	spojenec	k1gMnSc7	spojenec
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
podporoval	podporovat	k5eAaImAgInS	podporovat
zejména	zejména	k9	zejména
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
roku	rok	k1gInSc2	rok
1042	[number]	k4	1042
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Břetislavem	Břetislav	k1gMnSc7	Břetislav
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
sesadit	sesadit	k5eAaPmF	sesadit
z	z	k7c2	z
trůnu	trůn	k1gInSc2	trůn
Samuela	Samuel	k1gMnSc2	Samuel
Abu	Abu	k1gMnSc2	Abu
a	a	k8xC	a
dosadit	dosadit	k5eAaPmF	dosadit
zpět	zpět	k6eAd1	zpět
Petra	Petra	k1gFnSc1	Petra
Orseola	Orseola	k1gFnSc1	Orseola
pobývajícího	pobývající	k2eAgInSc2d1	pobývající
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1041	[number]	k4	1041
v	v	k7c6	v
bavorském	bavorský	k2eAgInSc6d1	bavorský
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
byla	být	k5eAaImAgFnS	být
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
výprava	výprava	k1gFnSc1	výprava
roku	rok	k1gInSc2	rok
1043	[number]	k4	1043
nepřinesla	přinést	k5eNaPmAgFnS	přinést
změnu	změna	k1gFnSc4	změna
na	na	k7c6	na
uherském	uherský	k2eAgInSc6d1	uherský
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1044	[number]	k4	1044
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Menfö	Menfö	k1gFnSc2	Menfö
Samuela	Samuel	k1gMnSc4	Samuel
Abu	Abu	k1gMnSc4	Abu
porazili	porazit	k5eAaPmAgMnP	porazit
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
uherští	uherský	k2eAgMnPc1d1	uherský
velmoži	velmož	k1gMnPc1	velmož
začali	začít	k5eAaPmAgMnP	začít
opět	opět	k6eAd1	opět
masivně	masivně	k6eAd1	masivně
přebíhat	přebíhat	k5eAaImF	přebíhat
k	k	k7c3	k
Petrovi	Petr	k1gMnSc3	Petr
Benátčanovi	Benátčan	k1gMnSc3	Benátčan
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Orseolo	Orseola	k1gFnSc5	Orseola
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Székesfehérváru	Székesfehérvár	k1gInSc6	Székesfehérvár
znovu	znovu	k6eAd1	znovu
uveden	uvést	k5eAaPmNgMnS	uvést
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Aba	Aba	k1gMnSc1	Aba
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1054	[number]	k4	1054
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
Břetislav	Břetislav	k1gMnSc1	Břetislav
komplikovanou	komplikovaný	k2eAgFnSc4d1	komplikovaná
otázku	otázka	k1gFnSc4	otázka
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
prosazením	prosazení	k1gNnSc7	prosazení
seniorátu	seniorát	k1gInSc2	seniorát
(	(	kIx(	(
<g/>
stařešinského	stařešinský	k2eAgInSc2d1	stařešinský
řádu	řád	k1gInSc2	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zavázal	zavázat	k5eAaPmAgMnS	zavázat
svých	svůj	k3xOyFgMnPc2	svůj
pět	pět	k4xCc4	pět
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
zemské	zemský	k2eAgMnPc4d1	zemský
předáky	předák	k1gMnPc4	předák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInPc3d1	další
členům	člen	k1gInPc3	člen
dynastie	dynastie	k1gFnSc2	dynastie
pak	pak	k6eAd1	pak
měly	mít	k5eAaImAgFnP	mít
náležet	náležet	k5eAaImF	náležet
menší	malý	k2eAgInPc4d2	menší
úděly	úděl	k1gInPc4	úděl
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
Břetislav	Břetislav	k1gMnSc1	Břetislav
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Chrudim	Chrudim	k1gFnSc1	Chrudim
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nového	nový	k2eAgNnSc2d1	nové
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Jitkou	Jitka	k1gFnSc7	Jitka
ze	z	k7c2	z
Svinibrodu	Svinibrod	k1gInSc2	Svinibrod
(	(	kIx(	(
<g/>
1003	[number]	k4	1003
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
1058	[number]	k4	1058
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
Břetislav	Břetislav	k1gMnSc1	Břetislav
pět	pět	k4xCc1	pět
synů	syn	k1gMnPc2	syn
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS	Spytihnět
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1031	[number]	k4	1031
<g/>
–	–	k?	–
<g/>
1061	[number]	k4	1061
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
<g/>
∞	∞	k?	∞
Ida	ido	k1gNnSc2	ido
WettinskáVratislav	WettinskáVratislava	k1gFnPc2	WettinskáVratislava
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
†	†	k?	†
1092	[number]	k4	1092
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
<g/>
∞	∞	k?	∞
N.	N.	kA	N.
<g/>
N.	N.	kA	N.
</s>
</p>
<p>
<s>
∞	∞	k?	∞
1057	[number]	k4	1057
Adléta	Adléto	k1gNnPc4	Adléto
Uherská	uherský	k2eAgNnPc4d1	Uherské
</s>
</p>
<p>
<s>
∞	∞	k?	∞
1062	[number]	k4	1062
Svatava	Svatava	k1gFnSc1	Svatava
PolskáKonrád	PolskáKonráda	k1gFnPc2	PolskáKonráda
I.	I.	kA	I.
Brněnský	brněnský	k2eAgMnSc1d1	brněnský
(	(	kIx(	(
<g/>
1035	[number]	k4	1035
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
1092	[number]	k4	1092
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
<g/>
∞	∞	k?	∞
1054	[number]	k4	1054
Virpirka	Virpirka	k1gFnSc1	Virpirka
z	z	k7c2	z
TenglinguJaromír	TenglinguJaromíra	k1gFnPc2	TenglinguJaromíra
(	(	kIx(	(
<g/>
1040	[number]	k4	1040
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
1090	[number]	k4	1090
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
biskup	biskup	k1gMnSc1	biskup
</s>
</p>
<p>
<s>
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
Olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
(	(	kIx(	(
<g/>
†	†	k?	†
1087	[number]	k4	1087
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
údělník	údělník	k1gMnSc1	údělník
<g/>
∞	∞	k?	∞
Eufemie	eufemie	k1gFnSc1	eufemie
Uherská	uherský	k2eAgFnSc1d1	uherská
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KRZEMIEŃSKA	KRZEMIEŃSKA	kA	KRZEMIEŃSKA
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
knížete	kníže	k1gMnSc2	kníže
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
o	o	k7c4	o
upevnění	upevnění	k1gNnSc4	upevnění
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
1039	[number]	k4	1039
<g/>
-	-	kIx~	-
<g/>
1041	[number]	k4	1041
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
75	[number]	k4	75
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KRZEMIEŃSKA	KRZEMIEŃSKA	kA	KRZEMIEŃSKA
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
.	.	kIx.	.
</s>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
polovině	polovina	k1gFnSc6	polovina
XI	XI	kA	XI
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
390	[number]	k4	390
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901760	[number]	k4	901760
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
a	a	k8xC	a
obrana	obrana	k1gFnSc1	obrana
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Oldřicha	Oldřich	k1gMnSc2	Oldřich
po	po	k7c4	po
Břetislava	Břetislav	k1gMnSc4	Břetislav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
282	[number]	k4	282
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
365	[number]	k4	365
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MATLA-KOZŁOWSKA	MATLA-KOZŁOWSKA	k?	MATLA-KOZŁOWSKA
<g/>
,	,	kIx,	,
Marzena	Marzen	k2eAgFnSc1d1	Marzena
<g/>
.	.	kIx.	.
</s>
<s>
Pierwsi	Pierwse	k1gFnSc4	Pierwse
Przemyślidzi	Przemyślidze	k1gFnSc4	Przemyślidze
i	i	k9	i
ich	ich	k?	ich
państwo	państwo	k6eAd1	państwo
(	(	kIx(	(
<g/>
od	od	k7c2	od
X	X	kA	X
do	do	k7c2	do
połowy	połowa	k1gFnSc2	połowa
XI	XI	kA	XI
wieku	wieku	k6eAd1	wieku
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekspansja	Ekspansj	k2eAgFnSc1d1	Ekspansj
terytorialna	terytorialna	k1gFnSc1	terytorialna
i	i	k8xC	i
jej	on	k3xPp3gMnSc4	on
polityczne	politycznout	k5eAaPmIp3nS	politycznout
uwarunkowania	uwarunkowanium	k1gNnPc5	uwarunkowanium
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
<g/>
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k1gMnSc1	Wydawnictwo
Poznańskie	Poznańskie	k1gFnSc2	Poznańskie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7177	[number]	k4	7177
<g/>
-	-	kIx~	-
<g/>
547	[number]	k4	547
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
do	do	k7c2	do
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
1214	[number]	k4	1214
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
PECHAR	PECHAR	kA	PECHAR
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Králové	Král	k1gMnPc1	Král
a	a	k8xC	a
knížata	kníže	k1gMnPc1wR	kníže
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	Rybka	k1gMnSc1	Rybka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
413	[number]	k4	413
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
8	[number]	k4	8
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Brun	Brun	k1gMnSc1	Brun
<g/>
–	–	k?	–
<g/>
By	by	k9	by
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
225	[number]	k4	225
<g/>
–	–	k?	–
<g/>
368	[number]	k4	368
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
247	[number]	k4	247
<g/>
–	–	k?	–
<g/>
248	[number]	k4	248
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
1034	[number]	k4	1034
<g/>
–	–	k?	–
<g/>
1198	[number]	k4	1198
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
712	[number]	k4	712
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
905	[number]	k4	905
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Břetislavova	Břetislavův	k2eAgFnSc1d1	Břetislavova
dekreta	dekreta	k1gFnSc1	dekreta
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
na	na	k7c4	na
www.e-stredovek.cz	www.etredovek.cz	k1gInSc4	www.e-stredovek.cz
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
–	–	k?	–
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
–	–	k?	–
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
</s>
</p>
<p>
<s>
Božena	Božena	k1gFnSc1	Božena
a	a	k8xC	a
Jitka	Jitka	k1gFnSc1	Jitka
</s>
</p>
<p>
<s>
Břetislavova	Břetislavův	k2eAgFnSc1d1	Břetislavova
dekreta	dekreta	k1gFnSc1	dekreta
</s>
</p>
<p>
<s>
Břetislavovo	Břetislavův	k2eAgNnSc1d1	Břetislavovo
ustanovení	ustanovení	k1gNnSc1	ustanovení
o	o	k7c6	o
seniorátě	seniorát	k1gInSc6	seniorát
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
v	v	k7c6	v
Kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
</s>
</p>
