<p>
<s>
Centimetr	centimetr	k1gInSc1	centimetr
(	(	kIx(	(
<g/>
značení	značení	k1gNnSc1	značení
cm	cm	kA	cm
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
délky	délka	k1gFnSc2	délka
v	v	k7c6	v
metrickém	metrický	k2eAgInSc6d1	metrický
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Centimetr	centimetr	k1gInSc1	centimetr
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
setině	setina	k1gFnSc3	setina
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
centi	cenť	k1gFnSc2	cenť
totiž	totiž	k9	totiž
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
předponě	předpona	k1gFnSc6	předpona
soustavy	soustava	k1gFnSc2	soustava
SI	se	k3xPyFc3	se
jednu	jeden	k4xCgFnSc4	jeden
setinu	setina	k1gFnSc4	setina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
soustavy	soustava	k1gFnSc2	soustava
CGS	CGS	kA	CGS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
fyzických	fyzický	k2eAgFnPc2d1	fyzická
veličin	veličina	k1gFnPc2	veličina
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgFnPc4d1	používaná
jednotky	jednotka	k1gFnPc4	jednotka
s	s	k7c7	s
SI	si	k1gNnSc7	si
předponami	předpona	k1gFnPc7	předpona
pro	pro	k7c4	pro
103	[number]	k4	103
jako	jako	k8xS	jako
mili-	mili-	k?	mili-
nebo	nebo	k8xC	nebo
kilo-	kilo-	k?	kilo-
centimetr	centimetr	k1gInSc1	centimetr
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
praktickou	praktický	k2eAgFnSc7d1	praktická
jednotkou	jednotka	k1gFnSc7	jednotka
délky	délka	k1gFnSc2	délka
pro	pro	k7c4	pro
každodenní	každodenní	k2eAgNnSc4d1	každodenní
měření	měření	k1gNnSc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Centimetr	centimetr	k1gInSc1	centimetr
je	být	k5eAaImIp3nS	být
přibliženě	přibliženě	k6eAd1	přibliženě
stejně	stejně	k6eAd1	stejně
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xC	jako
šířka	šířka	k1gFnSc1	šířka
průměrného	průměrný	k2eAgInSc2d1	průměrný
nehtu	nehet	k1gInSc2	nehet
průměrného	průměrný	k2eAgMnSc4d1	průměrný
dospělého	dospělý	k1gMnSc4	dospělý
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Převedení	převedení	k1gNnSc1	převedení
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
jednotky	jednotka	k1gFnPc4	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Unicode	Unicod	k1gInSc5	Unicod
symboly	symbol	k1gInPc7	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
s	s	k7c7	s
čínskými	čínský	k2eAgInPc7d1	čínský
<g/>
,	,	kIx,	,
japonskými	japonský	k2eAgInPc7d1	japonský
a	a	k8xC	a
korejskými	korejský	k2eAgInPc7d1	korejský
(	(	kIx(	(
<g/>
CJK	CJK	kA	CJK
<g/>
)	)	kIx)	)
znaky	znak	k1gInPc1	znak
má	mít	k5eAaImIp3nS	mít
Unicode	Unicod	k1gInSc5	Unicod
symboly	symbol	k1gInPc7	symbol
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Centimetr	centimetr	k1gInSc1	centimetr
(	(	kIx(	(
<g/>
cm	cm	kA	cm
<g/>
)	)	kIx)	)
-	-	kIx~	-
kód	kód	k1gInSc4	kód
339D	[number]	k4	339D
</s>
</p>
<p>
<s>
Centimetr	centimetr	k1gInSc4	centimetr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
(	(	kIx(	(
<g/>
cm2	cm2	k4	cm2
<g/>
)	)	kIx)	)
-	-	kIx~	-
kód	kód	k1gInSc4	kód
33A0	[number]	k4	33A0
</s>
</p>
<p>
<s>
Centimetr	centimetr	k1gInSc1	centimetr
kubický	kubický	k2eAgInSc1d1	kubický
(	(	kIx(	(
<g/>
cm3	cm3	k4	cm3
<g/>
)	)	kIx)	)
-	-	kIx~	-
kód	kód	k1gInSc4	kód
33	[number]	k4	33
<g/>
A	a	k8xC	a
<g/>
4	[number]	k4	4
<g/>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
východoasijskými	východoasijský	k2eAgInPc7d1	východoasijský
fonty	font	k1gInPc7	font
CJK	CJK	kA	CJK
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
šířkou	šířka	k1gFnSc7	šířka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
jako	jako	k8xS	jako
čínský	čínský	k2eAgInSc4d1	čínský
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Centimetre	centimetr	k1gInSc5	centimetr
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
