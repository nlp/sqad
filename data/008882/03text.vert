<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Macaa	Macao	k1gNnSc2	Macao
má	mít	k5eAaImIp3nS	mít
zelený	zelený	k2eAgInSc1d1	zelený
list	list	k1gInSc1	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc1d1	umístěný
bílý	bílý	k2eAgInSc1d1	bílý
stylizovaný	stylizovaný	k2eAgInSc1d1	stylizovaný
květ	květ	k1gInSc1	květ
lotosu	lotos	k1gInSc2	lotos
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
lotosem	lotos	k1gInSc7	lotos
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
most	most	k1gInSc1	most
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gInSc7	on
vodní	vodní	k2eAgFnSc1d1	vodní
hladina	hladina	k1gFnSc1	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tímto	tento	k3xDgInSc7	tento
emblémem	emblém	k1gInSc7	emblém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblouku	oblouk	k1gInSc6	oblouk
pět	pět	k4xCc4	pět
žlutých	žlutý	k2eAgFnPc2d1	žlutá
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
prostřední	prostřední	k2eAgNnSc1d1	prostřední
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgNnSc1d2	veliký
<g/>
)	)	kIx)	)
z	z	k7c2	z
čínské	čínský	k2eAgFnSc2d1	čínská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Macao	Macao	k1gNnSc1	Macao
je	být	k5eAaImIp3nS	být
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
součástí	součást	k1gFnSc7	součást
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
Macaa	Macao	k1gNnSc2	Macao
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
přistoupením	přistoupení	k1gNnSc7	přistoupení
Macaa	Macao	k1gNnSc2	Macao
k	k	k7c3	k
Číně	Čína	k1gFnSc3	Čína
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
používalo	používat	k5eAaImAgNnS	používat
Macao	Macao	k1gNnSc1	Macao
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
portugalských	portugalský	k2eAgFnPc2d1	portugalská
kolonií	kolonie	k1gFnPc2	kolonie
oficiálně	oficiálně	k6eAd1	oficiálně
vlajku	vlajka	k1gFnSc4	vlajka
portugalskou	portugalský	k2eAgFnSc4d1	portugalská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Macaa	Macao	k1gNnPc4	Macao
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Znak	znak	k1gInSc1	znak
Macaa	Macao	k1gNnSc2	Macao
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Macaa	Macao	k1gNnSc2	Macao
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
