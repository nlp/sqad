<s>
Samuel	Samuel	k1gMnSc1	Samuel
Barclay	Barclaa	k1gFnSc2	Barclaa
Beckett	Beckett	k1gMnSc1	Beckett
[	[	kIx(	[
<g/>
Bekit	Bekit	k1gMnSc1	Bekit
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
irský	irský	k2eAgMnSc1d1	irský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
absurdního	absurdní	k2eAgNnSc2d1	absurdní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
Foxrocku	Foxrock	k1gInSc6	Foxrock
nedaleko	nedaleko	k7c2	nedaleko
Dublinu	Dublin	k1gInSc2	Dublin
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
situované	situovaný	k2eAgFnSc6d1	situovaná
protestantské	protestantský	k2eAgFnSc6d1	protestantská
rodině	rodina	k1gFnSc6	rodina
zeměměřiče	zeměměřič	k1gMnSc2	zeměměřič
Williama	William	k1gMnSc2	William
Becketta	Beckett	k1gMnSc2	Beckett
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
sestry	sestra	k1gFnSc2	sestra
Mary	Mary	k1gFnSc2	Mary
Roeové	Roeová	k1gFnSc2	Roeová
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
prestižních	prestižní	k2eAgFnPc6d1	prestižní
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c4	na
Trinity	Trinit	k2eAgInPc4d1	Trinit
College	Colleg	k1gInPc4	Colleg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
evropské	evropský	k2eAgInPc4d1	evropský
jazyky	jazyk	k1gInPc4	jazyk
(	(	kIx(	(
<g/>
francouzštinu	francouzština	k1gFnSc4	francouzština
a	a	k8xC	a
italštinu	italština	k1gFnSc4	italština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
v	v	k7c6	v
Belfastu	Belfast	k1gInSc6	Belfast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
odjel	odjet	k5eAaPmAgMnS	odjet
přednášet	přednášet	k5eAaImF	přednášet
angličtinu	angličtina	k1gFnSc4	angličtina
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
působil	působit	k5eAaImAgMnS	působit
i	i	k8xC	i
na	na	k7c6	na
prestižní	prestižní	k2eAgFnSc6d1	prestižní
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
převzal	převzít	k5eAaPmAgInS	převzít
svůj	svůj	k3xOyFgInSc4	svůj
magisterský	magisterský	k2eAgInSc4d1	magisterský
diplom	diplom	k1gInSc4	diplom
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
vyučování	vyučování	k1gNnSc2	vyučování
francouzského	francouzský	k2eAgInSc2d1	francouzský
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c4	na
dublinské	dublinský	k2eAgFnPc4d1	Dublinská
Trinity	Trinit	k2eAgFnPc4d1	Trinita
College	Colleg	k1gFnPc4	Colleg
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
věnovat	věnovat	k5eAaPmF	věnovat
výlučně	výlučně	k6eAd1	výlučně
spisovatelské	spisovatelský	k2eAgFnSc3d1	spisovatelská
dráze	dráha	k1gFnSc3	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Dědictví	dědictví	k1gNnSc1	dědictví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
připadlo	připadnout	k5eAaPmAgNnS	připadnout
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
usadit	usadit	k5eAaPmF	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
neutuchajícím	utuchající	k2eNgFnPc3d1	neutuchající
depresím	deprese	k1gFnPc3	deprese
podrobil	podrobit	k5eAaPmAgMnS	podrobit
psychiatrické	psychiatrický	k2eAgFnSc3d1	psychiatrická
léčbě	léčba	k1gFnSc3	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
léčebny	léčebna	k1gFnSc2	léčebna
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
básnického	básnický	k2eAgInSc2d1	básnický
světa	svět	k1gInSc2	svět
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
svou	svůj	k3xOyFgFnSc4	svůj
98	[number]	k4	98
<g/>
veršovou	veršový	k2eAgFnSc7d1	veršová
sbírkou	sbírka	k1gFnSc7	sbírka
Děvkoskop	Děvkoskop	k1gInSc1	Děvkoskop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
dramatickém	dramatický	k2eAgInSc6d1	dramatický
monologu	monolog	k1gInSc6	monolog
si	se	k3xPyFc3	se
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
krátí	krátit	k5eAaImIp3nS	krátit
čekání	čekání	k1gNnSc4	čekání
na	na	k7c4	na
omeletu	omeleta	k1gFnSc4	omeleta
ze	z	k7c2	z
zkažených	zkažený	k2eAgNnPc2d1	zkažené
vajec	vejce	k1gNnPc2	vejce
rozjímáním	rozjímání	k1gNnSc7	rozjímání
nad	nad	k7c7	nad
obskurností	obskurnost	k1gFnSc7	obskurnost
teologických	teologický	k2eAgFnPc2d1	teologická
záhad	záhada	k1gFnPc2	záhada
<g/>
,	,	kIx,	,
plynutím	plynutí	k1gNnSc7	plynutí
času	čas	k1gInSc2	čas
a	a	k8xC	a
blížící	blížící	k2eAgNnSc4d1	blížící
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sbírce	sbírka	k1gFnSc6	sbírka
následoval	následovat	k5eAaImAgInS	následovat
soubor	soubor	k1gInSc4	soubor
esejí	esej	k1gFnPc2	esej
Proust	Proust	k1gMnSc1	Proust
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
a	a	k8xC	a
román	román	k1gInSc4	román
Víc	hodně	k6eAd2	hodně
píchanců	píchanec	k1gInPc2	píchanec
než	než	k8xS	než
kopanců	kopanec	k1gInPc2	kopanec
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
žil	žít	k5eAaImAgMnS	žít
Beckett	Beckett	k1gMnSc1	Beckett
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
Suzanne	Suzann	k1gInSc5	Suzann
Dechevaux-Dumesnilovou	Dechevaux-Dumesnilová	k1gFnSc7	Dechevaux-Dumesnilová
<g/>
,	,	kIx,	,
studentkou	studentka	k1gFnSc7	studentka
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
románová	románový	k2eAgFnSc1d1	románová
kariéra	kariéra	k1gFnSc1	kariéra
naplno	naplno	k6eAd1	naplno
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
knihou	kniha	k1gFnSc7	kniha
Murphy	Murpha	k1gFnSc2	Murpha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
líčí	líčit	k5eAaImIp3nS	líčit
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
boj	boj	k1gInSc1	boj
hrdiny	hrdina	k1gMnSc2	hrdina
zmítajícího	zmítající	k2eAgMnSc2d1	zmítající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
milence-prostitutce	milencerostitutka	k1gFnSc6	milence-prostitutka
a	a	k8xC	a
naprostém	naprostý	k2eAgInSc6d1	naprostý
útěku	útěk	k1gInSc6	útěk
do	do	k7c2	do
temných	temný	k2eAgFnPc2d1	temná
zákoutí	zákoutí	k1gNnPc2	zákoutí
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
protifašistickém	protifašistický	k2eAgInSc6d1	protifašistický
odboji	odboj	k1gInSc6	odboj
-	-	kIx~	-
francouzském	francouzský	k2eAgNnSc6d1	francouzské
hnutí	hnutí	k1gNnSc6	hnutí
odporu	odpor	k1gInSc3	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
pronásledováním	pronásledování	k1gNnSc7	pronásledování
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
nucen	nutit	k5eAaImNgMnS	nutit
prchnout	prchnout	k5eAaPmF	prchnout
do	do	k7c2	do
malé	malý	k2eAgFnSc2d1	malá
vesničky	vesnička	k1gFnSc2	vesnička
Roussillon	Roussillon	k1gInSc1	Roussillon
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
napsal	napsat	k5eAaPmAgMnS	napsat
svůj	svůj	k3xOyFgMnSc1	svůj
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
druhý	druhý	k4xOgInSc1	druhý
román	román	k1gInSc1	román
Watt	watt	k1gInSc1	watt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
poslední	poslední	k2eAgNnPc4d1	poslední
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
románových	románový	k2eAgNnPc2d1	románové
děl	dělo	k1gNnPc2	dělo
napsaných	napsaný	k2eAgNnPc2d1	napsané
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
řeči	řeč	k1gFnSc6	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
krátce	krátce	k6eAd1	krátce
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
irským	irský	k2eAgInSc7d1	irský
Červeným	červený	k2eAgInSc7d1	červený
křížem	kříž	k1gInSc7	kříž
v	v	k7c6	v
St.	st.	kA	st.
<g/>
Lo	Lo	k1gMnPc2	Lo
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
už	už	k6eAd1	už
natrvalo	natrvalo	k6eAd1	natrvalo
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1949	[number]	k4	1949
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
své	svůj	k3xOyFgNnSc4	svůj
největší	veliký	k2eAgNnSc4d3	veliký
prozaické	prozaický	k2eAgNnSc4d1	prozaické
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
trilogii	trilogie	k1gFnSc4	trilogie
Molloy	Molloa	k1gFnSc2	Molloa
<g/>
,	,	kIx,	,
Malone	Malon	k1gInSc5	Malon
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
Nepojmenovatelný	pojmenovatelný	k2eNgMnSc1d1	nepojmenovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
prý	prý	k9	prý
tvořilo	tvořit	k5eAaImAgNnS	tvořit
mnohem	mnohem	k6eAd1	mnohem
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
psal	psát	k5eAaImAgInS	psát
"	"	kIx"	"
<g/>
beze	beze	k7c2	beze
stylu	styl	k1gInSc2	styl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
eleganci	elegance	k1gFnSc4	elegance
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
posedlý	posedlý	k2eAgInSc1d1	posedlý
touhou	touha	k1gFnSc7	touha
vytvořit	vytvořit	k5eAaPmF	vytvořit
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
sám	sám	k3xTgMnSc1	sám
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
literatura	literatura	k1gFnSc1	literatura
neslova	slův	k2eNgFnSc1d1	neslova
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
slovům	slovo	k1gNnPc3	slovo
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
Čekání	čekání	k1gNnSc2	čekání
na	na	k7c6	na
Godota	Godota	k1gFnSc1	Godota
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
proslulost	proslulost	k1gFnSc4	proslulost
a	a	k8xC	a
katapultovala	katapultovat	k5eAaBmAgFnS	katapultovat
jej	on	k3xPp3gMnSc4	on
mezi	mezi	k7c7	mezi
přední	přední	k2eAgFnSc7d1	přední
představitele	představitel	k1gMnSc2	představitel
absurdního	absurdní	k2eAgNnSc2d1	absurdní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
dílo	dílo	k1gNnSc1	dílo
Konec	konec	k1gInSc4	konec
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
a	a	k8xC	a
série	série	k1gFnSc1	série
divadelních	divadelní	k2eAgFnPc2d1	divadelní
a	a	k8xC	a
krátkých	krátký	k2eAgFnPc2d1	krátká
rádiových	rádiový	k2eAgFnPc2d1	rádiová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Poslední	poslední	k2eAgFnSc1d1	poslední
páska	páska	k1gFnSc1	páska
popisující	popisující	k2eAgFnSc1d1	popisující
starého	starý	k1gMnSc4	starý
osamělého	osamělý	k2eAgMnSc4d1	osamělý
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sedí	sedit	k5eAaImIp3nS	sedit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pokoji	pokoj	k1gInSc6	pokoj
a	a	k8xC	a
po	po	k7c6	po
nocích	noc	k1gFnPc6	noc
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
nahrávky	nahrávka	k1gFnPc4	nahrávka
různých	různý	k2eAgFnPc2d1	různá
etap	etapa	k1gFnPc2	etapa
své	svůj	k3xOyFgFnSc2	svůj
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
rodné	rodný	k2eAgFnSc3d1	rodná
angličtině	angličtina	k1gFnSc3	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
dílech	díl	k1gInPc6	díl
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgFnPc1d1	přítomna
stopy	stopa	k1gFnPc1	stopa
černého	černý	k2eAgInSc2d1	černý
humoru	humor	k1gInSc2	humor
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
se	se	k3xPyFc4	se
spisovatel	spisovatel	k1gMnSc1	spisovatel
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
zmírnil	zmírnit	k5eAaPmAgInS	zmírnit
dopad	dopad	k1gInSc1	dopad
svých	svůj	k3xOyFgInPc2	svůj
pochmurných	pochmurný	k2eAgInPc2d1	pochmurný
námětů	námět	k1gInPc2	námět
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
jeho	on	k3xPp3gInSc2	on
posledního	poslední	k2eAgInSc2d1	poslední
kompletního	kompletní	k2eAgInSc2d1	kompletní
románu	román	k1gInSc2	román
Jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
líčí	líčit	k5eAaImIp3nS	líčit
osud	osud	k1gInSc1	osud
hrdiny	hrdina	k1gMnSc2	hrdina
brodícího	brodící	k2eAgMnSc2d1	brodící
se	se	k3xPyFc4	se
bahnem	bahno	k1gNnSc7	bahno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
dohání	dohánět	k5eAaImIp3nS	dohánět
dalšího	další	k2eAgMnSc4d1	další
brodícího	brodící	k2eAgMnSc4d1	brodící
se	se	k3xPyFc4	se
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
násilím	násilí	k1gNnSc7	násilí
nutí	nutit	k5eAaImIp3nS	nutit
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
čekal	čekat	k5eAaImAgMnS	čekat
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
bude	být	k5eAaImBp3nS	být
trápit	trápit	k5eAaImF	trápit
jeho	jeho	k3xOp3gNnSc2	jeho
samotného	samotný	k2eAgNnSc2d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
psal	psát	k5eAaImAgInS	psát
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
rozhlas	rozhlas	k1gInSc4	rozhlas
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
70	[number]	k4	70
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
sbírky	sbírka	k1gFnPc1	sbírka
krátkých	krátký	k2eAgFnPc2d1	krátká
básní	báseň	k1gFnPc2	báseň
Mirlitonnades	Mirlitonnadesa	k1gFnPc2	Mirlitonnadesa
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
All	All	k1gMnSc2	All
Strange	Strang	k1gMnSc2	Strang
Away	Awaa	k1gMnSc2	Awaa
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
Katastrofa	katastrofa	k1gFnSc1	katastrofa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
tématem	téma	k1gNnSc7	téma
výslechu	výslech	k1gInSc2	výslech
disidentů	disident	k1gMnPc2	disident
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
Václava	Václav	k1gMnSc4	Václav
Havla	Havel	k1gMnSc4	Havel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
mu	on	k3xPp3gNnSc3	on
zemřela	zemřít	k5eAaPmAgFnS	zemřít
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
nastěhoval	nastěhovat	k5eAaPmAgMnS	nastěhovat
do	do	k7c2	do
menšího	malý	k2eAgInSc2d2	menší
pečovatelského	pečovatelský	k2eAgInSc2d1	pečovatelský
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
doma	doma	k6eAd1	doma
ošklivě	ošklivě	k6eAd1	ošklivě
upadl	upadnout	k5eAaPmAgMnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
dny	den	k1gInPc4	den
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
holém	holý	k2eAgInSc6d1	holý
pokoji	pokoj	k1gInSc6	pokoj
přijímáním	přijímání	k1gNnSc7	přijímání
návštěv	návštěva	k1gFnPc2	návštěva
a	a	k8xC	a
psaním	psaní	k1gNnSc7	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
kniha	kniha	k1gFnSc1	kniha
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
Stirring	Stirring	k1gInSc4	Stirring
Still	Stilla	k1gFnPc2	Stilla
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Beckett	Beckett	k1gMnSc1	Beckett
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
potíží	potíž	k1gFnPc2	potíž
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Proslýchá	proslýchat	k5eAaImIp3nS	proslýchat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinu	většina	k1gFnSc4	většina
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
daroval	darovat	k5eAaPmAgMnS	darovat
chudým	chudý	k2eAgMnSc7d1	chudý
umělcům	umělec	k1gMnPc3	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
'	'	kIx"	'
<g/>
Cimetiè	Cimetiè	k1gFnSc1	Cimetiè
du	du	k?	du
Montparnasse	Montparnasse	k1gFnSc2	Montparnasse
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
francouzským	francouzský	k2eAgNnSc7d1	francouzské
prostředím	prostředí	k1gNnSc7	prostředí
značně	značně	k6eAd1	značně
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
psal	psát	k5eAaImAgMnS	psát
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
ale	ale	k8xC	ale
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
díla	dílo	k1gNnSc2	dílo
kreativně	kreativně	k6eAd1	kreativně
převáděl	převádět	k5eAaImAgInS	převádět
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
jazyku	jazyk	k1gInSc3	jazyk
do	do	k7c2	do
druhého	druhý	k4xOgMnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
(	(	kIx(	(
<g/>
En	En	k1gMnSc1	En
attendant	attendant	k1gMnSc1	attendant
Godot	Godot	k1gMnSc1	Godot
<g/>
)	)	kIx)	)
-	-	kIx~	-
1949	[number]	k4	1949
Konec	konec	k1gInSc1	konec
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
Fin	Fin	k1gMnSc1	Fin
de	de	k?	de
partie	partie	k1gFnSc1	partie
<g/>
)	)	kIx)	)
-	-	kIx~	-
1957	[number]	k4	1957
Akt	akta	k1gNnPc2	akta
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
-	-	kIx~	-
1957	[number]	k4	1957
Poslední	poslední	k2eAgFnSc1d1	poslední
páska	páska	k1gFnSc1	páska
(	(	kIx(	(
<g/>
Krapp	Krapp	k1gInSc1	Krapp
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Last	Last	k1gInSc1	Last
Tape	Tap	k1gInPc1	Tap
<g/>
)	)	kIx)	)
-	-	kIx~	-
1958	[number]	k4	1958
Akt	akta	k1gNnPc2	akta
beze	beze	k7c2	beze
slov	slovo	k1gNnPc2	slovo
II	II	kA	II
-	-	kIx~	-
1959	[number]	k4	1959
Šťastné	Šťastná	k1gFnSc2	Šťastná
<g />
.	.	kIx.	.
</s>
<s>
dny	den	k1gInPc1	den
(	(	kIx(	(
<g/>
Happy	Happ	k1gInPc1	Happ
Days	Daysa	k1gFnPc2	Daysa
<g/>
)	)	kIx)	)
-	-	kIx~	-
1961	[number]	k4	1961
Hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
Play	play	k0	play
<g/>
)	)	kIx)	)
-	-	kIx~	-
1963	[number]	k4	1963
Jde	jít	k5eAaImIp3nS	jít
a	a	k8xC	a
přijde	přijít	k5eAaPmIp3nS	přijít
(	(	kIx(	(
<g/>
Come	Come	k1gFnSc1	Come
and	and	k?	and
Go	Go	k1gFnSc1	Go
<g/>
)	)	kIx)	)
-	-	kIx~	-
1966	[number]	k4	1966
Dech	dech	k1gInSc1	dech
(	(	kIx(	(
<g/>
Breath	Breath	k1gInSc1	Breath
<g/>
)	)	kIx)	)
-	-	kIx~	-
1969	[number]	k4	1969
Ne	ne	k9	ne
já	já	k3xPp1nSc1	já
(	(	kIx(	(
<g/>
Not	nota	k1gFnPc2	nota
I	i	k8xC	i
<g/>
)	)	kIx)	)
-	-	kIx~	-
1973	[number]	k4	1973
Tenkrát	tenkrát	k6eAd1	tenkrát
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
That	That	k2eAgInSc1d1	That
Time	Time	k1gInSc1	Time
<g/>
)	)	kIx)	)
-	-	kIx~	-
1975	[number]	k4	1975
Nyní	nyní	k6eAd1	nyní
-	-	kIx~	-
1982	[number]	k4	1982
Katastrofa	katastrofa	k1gFnSc1	katastrofa
(	(	kIx(	(
<g/>
Catastrophe	Catastrophe	k1gFnSc1	Catastrophe
<g/>
)	)	kIx)	)
-	-	kIx~	-
1984	[number]	k4	1984
Všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kdož	kdož	k3yRnSc1	kdož
padají	padat	k5eAaImIp3nP	padat
(	(	kIx(	(
<g/>
All	All	k1gMnPc1	All
That	That	k1gMnSc1	That
Fall	Fall	k1gMnSc1	Fall
<g/>
)	)	kIx)	)
-	-	kIx~	-
1957	[number]	k4	1957
Víc	hodně	k6eAd2	hodně
píchanců	píchanec	k1gInPc2	píchanec
než	než	k8xS	než
kopanců	kopanec	k1gInPc2	kopanec
(	(	kIx(	(
<g/>
More	mor	k1gInSc5	mor
Pricks	Pricks	k1gInSc1	Pricks
Than	Than	k1gNnSc1	Than
Kicks	Kicks	k1gInSc1	Kicks
<g/>
)	)	kIx)	)
-	-	kIx~	-
1934	[number]	k4	1934
Murphy	Murpha	k1gFnPc1	Murpha
-	-	kIx~	-
1938	[number]	k4	1938
Watt	watt	k1gInSc1	watt
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Tso	Tso	k1gMnSc2	Tso
-	-	kIx~	-
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Molloy	Molloa	k1gFnSc2	Molloa
-	-	kIx~	-
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
následujícími	následující	k2eAgFnPc7d1	následující
tvoří	tvořit	k5eAaImIp3nP	tvořit
trilogii	trilogie	k1gFnSc3	trilogie
Malone	Malon	k1gInSc5	Malon
umírá	umírat	k5eAaImIp3nS	umírat
-	-	kIx~	-
1951	[number]	k4	1951
Nepojmenovatelný	pojmenovatelný	k2eNgMnSc1d1	nepojmenovatelný
-	-	kIx~	-
1953	[number]	k4	1953
Novely	novela	k1gFnPc1	novela
a	a	k8xC	a
texty	text	k1gInPc1	text
pro	pro	k7c4	pro
nic	nic	k6eAd1	nic
-	-	kIx~	-
1955	[number]	k4	1955
a	a	k8xC	a
1958	[number]	k4	1958
Jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
1961	[number]	k4	1961
Děvkoskop	Děvkoskop	k1gInSc4	Děvkoskop
(	(	kIx(	(
<g/>
Whoroscope	Whoroscop	k1gMnSc5	Whoroscop
<g/>
)	)	kIx)	)
-	-	kIx~	-
1930	[number]	k4	1930
Společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
Company	Compan	k1gInPc1	Compan
<g/>
)	)	kIx)	)
-	-	kIx~	-
1979	[number]	k4	1979
</s>
