<s>
Lovčičky	Lovčička	k1gFnPc1
</s>
<s>
Lovčičky	Lovčička	k1gFnPc1
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0646	CZ0646	k4
593265	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vyškov	Vyškov	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
646	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
64	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
710	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
4,04	4,04	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Lovčičky	Lovčička	k1gFnPc4
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
260	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
683	#num#	k4
54	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
1	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Lovčičky	Lovčička	k1gFnPc4
14868354	#num#	k4
Otnice	Otnice	k1gFnSc2
oulovcicky@politavi.cz	oulovcicky@politavi.cza	k1gFnPc2
Starosta	Starosta	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Martin	Martin	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.lovcicky.cz	www.lovcicky.cz	k1gInSc1
</s>
<s>
Lovčičky	Lovčička	k1gFnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
593265	#num#	k4
Kód	kód	k1gInSc1
části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
87653	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lovčičky	Lovčička	k1gFnPc1
jsou	být	k5eAaImIp3nP
obcí	obec	k1gFnPc2
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Vyškov	Vyškov	k1gInSc1
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
12	#num#	k4
km	km	kA
od	od	k7c2
Slavkova	Slavkov	k1gInSc2
u	u	k7c2
Brna	Brno	k1gNnSc2
v	v	k7c6
údolí	údolí	k1gNnSc6
o	o	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
260	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
710	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1141	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sweydiger	Sweydiger	k1gMnSc1
Haugvic	Haugvic	k1gMnSc1
z	z	k7c2
Biskupic	Biskupice	k1gFnPc2
koupil	koupit	k5eAaPmAgInS
Lovčičky	Lovčička	k1gFnSc2
roku	rok	k1gInSc2
1376	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
působí	působit	k5eAaImIp3nS
sokolská	sokolský	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
obec	obec	k1gFnSc1
začala	začít	k5eAaPmAgFnS
(	(	kIx(
<g/>
z	z	k7c2
dotace	dotace	k1gFnSc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
i	i	k8xC
obce	obec	k1gFnSc2
<g/>
)	)	kIx)
opravovat	opravovat	k5eAaImF
sokolovnu	sokolovna	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jí	on	k3xPp3gFnSc3
vrátila	vrátit	k5eAaPmAgFnS
původní	původní	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
zejména	zejména	k9
o	o	k7c4
opravu	oprava	k1gFnSc4
střechy	střecha	k1gFnSc2
a	a	k8xC
výměnu	výměna	k1gFnSc4
oken	okno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
V	v	k7c6
obci	obec	k1gFnSc6
k	k	k7c3
počátku	počátek	k1gInSc3
roku	rok	k1gInSc2
2016	#num#	k4
žilo	žít	k5eAaImAgNnS
celkem	celek	k1gInSc7
663	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
322	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
341	#num#	k4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
věk	věk	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
dosahoval	dosahovat	k5eAaImAgInS
39,8	39,8	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
domů	dům	k1gInPc2
a	a	k8xC
bytů	byt	k1gInPc2
provedeném	provedený	k2eAgInSc6d1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
obci	obec	k1gFnSc6
žilo	žít	k5eAaImAgNnS
586	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
z	z	k7c2
nich	on	k3xPp3gInPc2
bylo	být	k5eAaImAgNnS
(	(	kIx(
<g/>
15,2	15,2	k4
%	%	kIx~
<g/>
)	)	kIx)
obyvatel	obyvatel	k1gMnPc2
ve	v	k7c6
věku	věk	k1gInSc6
od	od	k7c2
20	#num#	k4
do	do	k7c2
29	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
do	do	k7c2
14	#num#	k4
let	léto	k1gNnPc2
věku	věk	k1gInSc2
tvořily	tvořit	k5eAaImAgFnP
15	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
senioři	senior	k1gMnPc1
nad	nad	k7c4
70	#num#	k4
let	léto	k1gNnPc2
úhrnem	úhrnem	k6eAd1
7,2	7,2	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkem	celkem	k6eAd1
498	#num#	k4
občanů	občan	k1gMnPc2
obce	obec	k1gFnSc2
starších	starší	k1gMnPc2
15	#num#	k4
let	léto	k1gNnPc2
mělo	mít	k5eAaImAgNnS
vzdělání	vzdělání	k1gNnSc1
38,2	38,2	k4
%	%	kIx~
střední	střední	k2eAgNnSc1d1
vč.	vč.	k?
vyučení	vyučení	k1gNnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
maturity	maturita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
vysokoškoláků	vysokoškolák	k1gMnPc2
dosahoval	dosahovat	k5eAaImAgInS
6	#num#	k4
%	%	kIx~
a	a	k8xC
bez	bez	k7c2
vzdělání	vzdělání	k1gNnSc2
bylo	být	k5eAaImAgNnS
naopak	naopak	k6eAd1
0,2	0,2	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
cenzu	cenzus	k1gInSc2
dále	daleko	k6eAd2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
městě	město	k1gNnSc6
žilo	žít	k5eAaImAgNnS
281	#num#	k4
ekonomicky	ekonomicky	k6eAd1
aktivních	aktivní	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
94	#num#	k4
%	%	kIx~
z	z	k7c2
nich	on	k3xPp3gFnPc2
se	se	k3xPyFc4
řadilo	řadit	k5eAaImAgNnS
mezi	mezi	k7c4
zaměstnané	zaměstnaný	k1gMnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgNnPc2
67,6	67,6	k4
%	%	kIx~
patřilo	patřit	k5eAaImAgNnS
mezi	mezi	k7c4
zaměstnance	zaměstnanec	k1gMnPc4
<g/>
,	,	kIx,
2,1	2,1	k4
%	%	kIx~
k	k	k7c3
zaměstnavatelům	zaměstnavatel	k1gMnPc3
a	a	k8xC
zbytek	zbytek	k1gInSc4
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c4
vlastní	vlastní	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
celých	celý	k2eAgNnPc2d1
47,3	47,3	k4
%	%	kIx~
občanů	občan	k1gMnPc2
nebylo	být	k5eNaImAgNnS
ekonomicky	ekonomicky	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
nepracující	pracující	k2eNgMnPc1d1
důchodci	důchodce	k1gMnPc1
či	či	k8xC
žáci	žák	k1gMnPc1
<g/>
,	,	kIx,
studenti	student	k1gMnPc1
nebo	nebo	k8xC
učni	učeň	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
zbytek	zbytek	k1gInSc1
svou	svůj	k3xOyFgFnSc4
ekonomickou	ekonomický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
uvést	uvést	k5eAaPmF
nechtěl	chtít	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Úhrnem	úhrnem	k6eAd1
235	#num#	k4
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
40,1	40,1	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	se	k3xPyFc4
hlásilo	hlásit	k5eAaImAgNnS
k	k	k7c3
české	český	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
147	#num#	k4
obyvatel	obyvatel	k1gMnPc2
bylo	být	k5eAaImAgNnS
Moravanů	Moravan	k1gMnPc2
a	a	k8xC
1	#num#	k4
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celých	celý	k2eAgMnPc2d1
294	#num#	k4
obyvatel	obyvatel	k1gMnPc2
obce	obec	k1gFnSc2
však	však	k9
svou	svůj	k3xOyFgFnSc4
národnost	národnost	k1gFnSc4
neuvedlo	uvést	k5eNaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
za	za	k7c4
celou	celý	k2eAgFnSc4d1
obec	obec	k1gFnSc4
i	i	k9
za	za	k7c4
jeho	jeho	k3xOp3gFnPc4
jednotlivé	jednotlivý	k2eAgFnPc4d1
části	část	k1gFnPc4
uvádí	uvádět	k5eAaImIp3nS
tabulka	tabulka	k1gFnSc1
níže	níže	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
zobrazuje	zobrazovat	k5eAaImIp3nS
i	i	k9
příslušnost	příslušnost	k1gFnSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
k	k	k7c3
obci	obec	k1gFnSc3
či	či	k8xC
následné	následný	k2eAgNnSc4d1
odtržení	odtržení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Místní	místní	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
1791183418691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
část	část	k1gFnSc1
Lovčičky	Lovčička	k1gFnSc2
<g/>
366534649672683759955910954790840723614512539586	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
domů	dům	k1gInPc2
</s>
<s>
část	část	k1gFnSc1
Lovčičky	Lovčička	k1gFnSc2
<g/>
6787123129132142171178218233217206197221240253	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatelData	obyvatelDat	k1gMnSc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
Wikidat	Wikidat	k1gFnSc2
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Vyškov	Vyškov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Filiální	filiální	k2eAgInSc1d1
kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
ze	z	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Kaplička	kaplička	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Anny	Anna	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1863	#num#	k4
na	na	k7c6
okraji	okraj	k1gInSc6
obce	obec	k1gFnSc2
</s>
<s>
Kaplička	kaplička	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Floriána	Florián	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1817	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
obecní	obecní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Floriána	Florián	k1gMnSc2
</s>
<s>
pohled	pohled	k1gInSc1
od	od	k7c2
silnice	silnice	k1gFnSc2
do	do	k7c2
Otnic	Otnice	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
luk	luk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sokolovna	sokolovna	k1gFnSc1
v	v	k7c6
Lovčičkách	Lovčička	k1gFnPc6
dostane	dostat	k5eAaPmIp3nS
lepší	dobrý	k2eAgInSc1d2
strop	strop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispěje	přispět	k5eAaPmIp3nS
na	na	k7c4
něj	on	k3xPp3gInSc4
kraj	kraj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyškovský	vyškovský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2017	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Lovčičky	Lovčička	k1gFnPc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Vyškov	Vyškov	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2011-03-26	2011-03-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lovčičky	Lovčička	k1gFnSc2
-	-	kIx~
obec	obec	k1gFnSc1
<g/>
/	/	kIx~
<g/>
město	město	k1gNnSc1
(	(	kIx(
<g/>
okr	okr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyškov	Vyškov	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2011-03-26	2011-03-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
900	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
2394	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Lovčičky	Lovčička	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
489	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
760	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1310	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Okres	okres	k1gInSc1
Vyškov	Vyškov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
640	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Lovčičky	Lovčička	k1gFnSc2
<g/>
:	:	kIx,
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
-	-	kIx~
informační	informační	k2eAgFnSc2d1
tabule	tabule	k1gFnSc2
v	v	k7c6
obci	obec	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
NEKUDA	Nekuda	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastivěda	vlastivěda	k1gFnSc1
moravská	moravský	k2eAgNnPc4d1
Vyškovsko	Vyškovsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Muzejní	muzejní	k2eAgInSc4d1
spolek	spolek	k1gInSc4
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Otnice	Otnice	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lovčičky	Lovčička	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Lovčičky	Lovčička	k1gFnPc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
Lovčičky	Lovčička	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lovčičky	Lovčička	k1gFnPc1
<g/>
:	:	kIx,
Obec	obec	k1gFnSc1
Lovčičky	Lovčička	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
<g/>
,	,	kIx,
obce	obec	k1gFnSc2
a	a	k8xC
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
okresu	okres	k1gInSc2
Vyškov	Vyškov	k1gInSc1
</s>
<s>
Bohaté	bohatý	k2eAgFnPc4d1
Málkovice	Málkovice	k1gFnPc4
•	•	k?
Bohdalice-Pavlovice	Bohdalice-Pavlovice	k1gFnPc4
•	•	k?
Bošovice	Bošovice	k1gFnPc4
•	•	k?
Brankovice	Brankovice	k1gFnPc4
•	•	k?
Bučovice	Bučovice	k1gFnPc4
•	•	k?
Dětkovice	Dětkovice	k1gFnSc2
•	•	k?
Dobročkovice	Dobročkovice	k1gFnSc2
•	•	k?
Dražovice	Dražovice	k1gFnSc1
•	•	k?
Drnovice	Drnovice	k1gInPc1
•	•	k?
Drysice	Drysice	k1gFnSc2
•	•	k?
Habrovany	Habrovan	k1gMnPc4
•	•	k?
Heršpice	Heršpice	k1gFnPc4
•	•	k?
Hlubočany	Hlubočan	k1gMnPc4
•	•	k?
Hodějice	Hodějice	k1gFnSc1
•	•	k?
Holubice	holubice	k1gFnSc2
•	•	k?
Hostěrádky-Rešov	Hostěrádky-Rešov	k1gInSc1
•	•	k?
Hoštice-Heroltice	Hoštice-Heroltice	k1gFnSc2
•	•	k?
Hrušky	hruška	k1gFnSc2
•	•	k?
Hvězdlice	Hvězdlice	k1gFnSc2
•	•	k?
Chvalkovice	Chvalkovice	k1gFnSc2
•	•	k?
Ivanovice	Ivanovice	k1gFnPc1
na	na	k7c6
Hané	Haná	k1gFnSc6
•	•	k?
Ježkovice	Ježkovice	k1gFnSc2
•	•	k?
Kobeřice	Kobeřice	k1gFnSc2
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kojátky	Kojátka	k1gFnSc2
•	•	k?
Komořany	Komořan	k1gMnPc4
•	•	k?
Kozlany	Kozlana	k1gFnPc4
•	•	k?
Kožušice	Kožušice	k1gFnPc4
•	•	k?
Krásensko	Krásensko	k1gNnSc1
•	•	k?
Křenovice	Křenovice	k1gFnSc2
•	•	k?
Křižanovice	Křižanovice	k1gFnSc2
•	•	k?
Křižanovice	Křižanovice	k1gFnSc2
u	u	k7c2
Vyškova	Vyškov	k1gInSc2
•	•	k?
Kučerov	Kučerov	k1gInSc1
•	•	k?
Letonice	Letonice	k1gFnSc2
•	•	k?
Lovčičky	Lovčička	k1gFnSc2
•	•	k?
Luleč	Luleč	k1gMnSc1
•	•	k?
Lysovice	Lysovice	k1gFnSc2
•	•	k?
Malínky	Malínka	k1gFnSc2
•	•	k?
Medlovice	Medlovice	k1gFnSc2
•	•	k?
Milešovice	Milešovice	k1gFnSc2
•	•	k?
Milonice	Milonice	k1gFnSc2
•	•	k?
Moravské	moravský	k2eAgFnSc2d1
Málkovice	Málkovice	k1gFnSc2
•	•	k?
Mouřínov	Mouřínov	k1gInSc1
•	•	k?
Němčany	Němčan	k1gMnPc4
•	•	k?
Nemochovice	Nemochovice	k1gFnSc1
•	•	k?
Nemojany	Nemojana	k1gFnSc2
•	•	k?
Nemotice	Nemotice	k1gFnSc2
•	•	k?
Nesovice	Nesovice	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nevojice	Nevojice	k1gFnSc2
•	•	k?
Nížkovice	Nížkovice	k1gFnSc2
•	•	k?
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
•	•	k?
Olšany	Olšany	k1gInPc4
•	•	k?
Orlovice	Orlovice	k1gFnSc2
•	•	k?
Otnice	Otnice	k1gFnSc2
•	•	k?
Podbřežice	Podbřežice	k1gFnSc2
•	•	k?
Podivice	Podivice	k1gFnSc1
•	•	k?
Podomí	Podomí	k1gNnPc2
•	•	k?
Prusy-Boškůvky	Prusy-Boškůvka	k1gFnSc2
•	•	k?
Pustiměř	Pustiměř	k1gMnSc1
•	•	k?
Račice-Pístovice	Račice-Pístovice	k1gFnSc2
•	•	k?
Radslavice	Radslavice	k1gFnSc2
•	•	k?
Rašovice	Rašovice	k1gFnSc2
•	•	k?
Rostěnice-Zvonovice	Rostěnice-Zvonovice	k1gFnSc2
•	•	k?
Rousínov	Rousínov	k1gInSc1
•	•	k?
Ruprechtov	Ruprechtov	k1gInSc1
•	•	k?
Rybníček	rybníček	k1gInSc1
•	•	k?
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
•	•	k?
Snovídky	Snovídka	k1gFnSc2
•	•	k?
Studnice	studnice	k1gFnSc2
•	•	k?
Šaratice	šaratice	k1gFnSc2
•	•	k?
Švábenice	Švábenice	k1gFnSc2
•	•	k?
Topolany	Topolana	k1gFnSc2
•	•	k?
Tučapy	Tučapa	k1gFnSc2
•	•	k?
Uhřice	Uhřice	k1gFnSc2
•	•	k?
Vážany	Vážana	k1gFnSc2
•	•	k?
Vážany	Vážana	k1gFnSc2
nad	nad	k7c7
Litavou	Litava	k1gFnSc7
•	•	k?
Velešovice	Velešovice	k1gFnSc2
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zbýšov	Zbýšov	k1gInSc1
•	•	k?
Zelená	zelenat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
vojenský	vojenský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
Březina	Březina	k1gFnSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
