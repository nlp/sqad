<p>
<s>
Záhlinické	Záhlinický	k2eAgInPc4d1	Záhlinický
rybníky	rybník	k1gInPc4	rybník
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
čtyř	čtyři	k4xCgInPc2	čtyři
rybníků	rybník	k1gInPc2	rybník
<g/>
,	,	kIx,	,
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
Moravě	Morava	k1gFnSc6	Morava
asi	asi	k9	asi
200	[number]	k4	200
m	m	kA	m
západně	západně	k6eAd1	západně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Záhlinice	Záhlinice	k1gFnSc2	Záhlinice
a	a	k8xC	a
Hulína	Hulín	k1gInSc2	Hulín
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
ve	v	k7c6	v
Zlínském	zlínský	k2eAgInSc6d1	zlínský
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
Hornomoravském	hornomoravský	k2eAgInSc6d1	hornomoravský
úvalu	úval	k1gInSc6	úval
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
Rusavy	Rusava	k1gFnSc2	Rusava
do	do	k7c2	do
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
významné	významný	k2eAgNnSc4d1	významné
stanoviště	stanoviště	k1gNnSc4	stanoviště
a	a	k8xC	a
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
vodních	vodní	k2eAgMnPc2d1	vodní
a	a	k8xC	a
tažných	tažný	k2eAgMnPc2d1	tažný
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
rybníků	rybník	k1gInPc2	rybník
==	==	k?	==
</s>
</p>
<p>
<s>
Rybníky	rybník	k1gInPc1	rybník
v	v	k7c6	v
Záhlinicích	Záhlinik	k1gInPc6	Záhlinik
byly	být	k5eAaImAgFnP	být
budovány	budovat	k5eAaImNgFnP	budovat
již	již	k6eAd1	již
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1547	[number]	k4	1547
-	-	kIx~	-
1573	[number]	k4	1573
je	být	k5eAaImIp3nS	být
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
Dubravius	Dubravius	k1gMnSc1	Dubravius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
rybníky	rybník	k1gInPc1	rybník
vysušeny	vysušit	k5eAaPmNgInP	vysušit
a	a	k8xC	a
přeměněny	přeměnit	k5eAaPmNgInP	přeměnit
na	na	k7c4	na
pastviny	pastvina	k1gFnPc4	pastvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rybníky	rybník	k1gInPc1	rybník
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
==	==	k?	==
</s>
</p>
<p>
<s>
Rybníky	rybník	k1gInPc1	rybník
byly	být	k5eAaImAgInP	být
obnoveny	obnovit	k5eAaPmNgInP	obnovit
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
1953	[number]	k4	1953
–	–	k?	–
1981	[number]	k4	1981
Státním	státní	k2eAgNnSc7d1	státní
rybářstvím	rybářství	k1gNnSc7	rybářství
Přerov	Přerov	k1gInSc1	Přerov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svárovský	svárovský	k2eAgInSc1d1	svárovský
rybník	rybník	k1gInSc1	rybník
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
105	[number]	k4	105
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pláňavský	Pláňavský	k2eAgInSc1d1	Pláňavský
rybník	rybník	k1gInSc1	rybník
zřízen	zřízen	k2eAgInSc1d1	zřízen
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
44	[number]	k4	44
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doubravický	Doubravický	k2eAgInSc1d1	Doubravický
rybník	rybník	k1gInSc1	rybník
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
54	[number]	k4	54
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Němčický	němčický	k2eAgInSc1d1	němčický
rybník	rybník	k1gInSc1	rybník
zřízen	zřízen	k2eAgInSc1d1	zřízen
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
36	[number]	k4	36
ha	ha	kA	ha
<g/>
.	.	kIx.	.
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
rybníků	rybník	k1gInPc2	rybník
je	být	k5eAaImIp3nS	být
239	[number]	k4	239
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byly	být	k5eAaImAgFnP	být
Svárovský	svárovský	k2eAgInSc4d1	svárovský
a	a	k8xC	a
Doubravický	Doubravický	k2eAgInSc4d1	Doubravický
rybník	rybník	k1gInSc4	rybník
rozdělen	rozdělen	k2eAgInSc4d1	rozdělen
na	na	k7c4	na
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Svárovský	svárovský	k2eAgInSc1d1	svárovský
rybník	rybník	k1gInSc1	rybník
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
Doubravický	Doubravický	k2eAgInSc1d1	Doubravický
na	na	k7c4	na
pět	pět	k4xCc4	pět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
má	mít	k5eAaImIp3nS	mít
rybníky	rybník	k1gInPc4	rybník
v	v	k7c6	v
pronájmu	pronájem	k1gInSc6	pronájem
firma	firma	k1gFnSc1	firma
Rybářství	rybářství	k1gNnSc2	rybářství
Hulín	Hulín	k1gInSc1	Hulín
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chová	chovat	k5eAaImIp3nS	chovat
kapry	kapr	k1gMnPc4	kapr
<g/>
,	,	kIx,	,
amury	amur	k1gMnPc4	amur
<g/>
,	,	kIx,	,
tolstolobiky	tolstolobik	k1gMnPc4	tolstolobik
<g/>
,	,	kIx,	,
líny	lín	k1gMnPc4	lín
<g/>
,	,	kIx,	,
štiky	štika	k1gFnPc4	štika
a	a	k8xC	a
sumce	sumec	k1gMnPc4	sumec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
chová	chovat	k5eAaImIp3nS	chovat
vodní	vodní	k2eAgFnSc4d1	vodní
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1995	[number]	k4	1995
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
500	[number]	k4	500
ha	ha	kA	ha
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
samotných	samotný	k2eAgInPc2d1	samotný
rybníků	rybník	k1gInPc2	rybník
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
starý	starý	k2eAgInSc1d1	starý
dubový	dubový	k2eAgInSc1d1	dubový
porost	porost	k1gInSc1	porost
Zámeček	zámeček	k1gInSc1	zámeček
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
od	od	k7c2	od
rybníků	rybník	k1gInPc2	rybník
západně	západně	k6eAd1	západně
až	až	k9	až
ke	k	k7c3	k
Kroměříži	Kroměříž	k1gFnSc3	Kroměříž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
myslivna	myslivna	k1gFnSc1	myslivna
Zámeček	zámeček	k1gInSc1	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Stolička	stolička	k1gFnSc1	stolička
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1838	[number]	k4	1838
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
<g/>
1874	[number]	k4	1874
Murghí	Murgh	k1gFnPc2	Murgh
<g/>
,	,	kIx,	,
Ladak	Ladak	k1gInSc1	Ladak
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
geolog	geolog	k1gMnSc1	geolog
a	a	k8xC	a
paleontolog	paleontolog	k1gMnSc1	paleontolog
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Himálaji	Himálaj	k1gFnSc6	Himálaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
částí	část	k1gFnSc7	část
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
Filena	Filena	k1gFnSc1	Filena
<g/>
,	,	kIx,	,
lužní	lužní	k2eAgInSc1d1	lužní
les	les	k1gInSc1	les
četnými	četný	k2eAgFnPc7d1	četná
tůněmi	tůně	k1gFnPc7	tůně
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Svárovského	Svárovského	k2eAgInSc2d1	Svárovského
rybníka	rybník	k1gInSc2	rybník
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Moravě	Morava	k1gFnSc3	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Východně	východně	k6eAd1	východně
a	a	k8xC	a
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Svárova	Svárov	k1gInSc2	Svárov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
podmáčené	podmáčený	k2eAgFnPc1d1	podmáčená
louky	louka	k1gFnPc1	louka
Přední	přední	k2eAgFnPc4d1	přední
louky	louka	k1gFnPc4	louka
a	a	k8xC	a
Bahnovský	Bahnovský	k2eAgInSc4d1	Bahnovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flóra	Flóra	k1gFnSc1	Flóra
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
česnek	česnek	k1gInSc4	česnek
hranatý	hranatý	k2eAgInSc4d1	hranatý
<g/>
,	,	kIx,	,
starček	starček	k1gInSc1	starček
poříční	poříční	k2eAgInSc1d1	poříční
<g/>
,	,	kIx,	,
pryšec	pryšec	k1gInSc1	pryšec
bahenní	bahenní	k2eAgInSc1d1	bahenní
<g/>
,	,	kIx,	,
pryskyřník	pryskyřník	k1gInSc1	pryskyřník
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
hrachor	hrachor	k1gInSc1	hrachor
bahenní	bahenní	k2eAgInSc1d1	bahenní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lužních	lužní	k2eAgInPc6d1	lužní
lesích	les	k1gInPc6	les
jsou	být	k5eAaImIp3nP	být
zachované	zachovaný	k2eAgInPc1d1	zachovaný
staré	starý	k2eAgInPc1d1	starý
porosty	porost	k1gInPc1	porost
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
dubu	dub	k1gInSc2	dub
letního	letní	k2eAgInSc2d1	letní
a	a	k8xC	a
jasanu	jasan	k1gInSc2	jasan
ztepilého	ztepilý	k2eAgInSc2d1	ztepilý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zamokřených	zamokřený	k2eAgFnPc6d1	zamokřená
loukách	louka	k1gFnPc6	louka
jsou	být	k5eAaImIp3nP	být
rákosiny	rákosina	k1gFnPc1	rákosina
a	a	k8xC	a
rozptýlené	rozptýlený	k2eAgFnPc1d1	rozptýlená
vrby	vrba	k1gFnPc1	vrba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Ptáci	pták	k1gMnPc5	pták
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
živočichy	živočich	k1gMnPc7	živočich
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
nejbohatší	bohatý	k2eAgNnSc4d3	nejbohatší
zastoupení	zastoupení	k1gNnSc4	zastoupení
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
270	[number]	k4	270
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
135	[number]	k4	135
druhů	druh	k1gInPc2	druh
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obojživelníci	obojživelník	k1gMnPc5	obojživelník
===	===	k?	===
</s>
</p>
<p>
<s>
Početnou	početný	k2eAgFnSc7d1	početná
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
11	[number]	k4	11
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plazi	plaz	k1gMnPc5	plaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
3	[number]	k4	3
druhy	druh	k1gInPc1	druh
chráněných	chráněný	k2eAgMnPc2d1	chráněný
plazů	plaz	k1gMnPc2	plaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Savci	savec	k1gMnPc5	savec
===	===	k?	===
</s>
</p>
<p>
<s>
Savci	savec	k1gMnPc1	savec
jsou	být	k5eAaImIp3nP	být
zastoupení	zastoupení	k1gNnSc4	zastoupení
několika	několik	k4yIc7	několik
druhy	druh	k1gInPc4	druh
netopýrů	netopýr	k1gMnPc2	netopýr
či	či	k8xC	či
bobrem	bobr	k1gMnSc7	bobr
evropským	evropský	k2eAgMnSc7d1	evropský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bezobratlí	bezobratlí	k1gMnPc5	bezobratlí
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
chráněných	chráněný	k2eAgMnPc2d1	chráněný
vodních	vodní	k2eAgMnPc2d1	vodní
bezobratlých	bezobratlí	k1gMnPc2	bezobratlí
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
raci	rak	k1gMnPc1	rak
a	a	k8xC	a
mlži	mlž	k1gMnPc1	mlž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hmyzu	hmyz	k1gInSc2	hmyz
zde	zde	k6eAd1	zde
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
vzácné	vzácný	k2eAgInPc1d1	vzácný
druhy	druh	k1gInPc1	druh
brouků	brouk	k1gMnPc2	brouk
<g/>
,	,	kIx,	,
motýlů	motýl	k1gMnPc2	motýl
či	či	k8xC	či
vážek	vážka	k1gFnPc2	vážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geologie	geologie	k1gFnSc2	geologie
==	==	k?	==
</s>
</p>
<p>
<s>
Podloží	podloží	k1gNnSc1	podloží
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
kenozickými	kenozický	k2eAgInPc7d1	kenozický
<g/>
,	,	kIx,	,
neocénními	océnní	k2eNgInPc7d1	océnní
a	a	k8xC	a
pliocénními	pliocénní	k2eAgInPc7d1	pliocénní
sladkovodními	sladkovodní	k2eAgInPc7d1	sladkovodní
a	a	k8xC	a
brakickými	brakický	k2eAgInPc7d1	brakický
sedimenty	sediment	k1gInPc7	sediment
(	(	kIx(	(
<g/>
písky	písek	k1gInPc7	písek
<g/>
,	,	kIx,	,
štěrky	štěrk	k1gInPc7	štěrk
a	a	k8xC	a
jíly	jíl	k1gInPc7	jíl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písky	Písek	k1gInPc1	Písek
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
pestře	pestro	k6eAd1	pestro
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
<g/>
,	,	kIx,	,
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
s	s	k7c7	s
polohami	poloha	k1gFnPc7	poloha
křemičitých	křemičitý	k2eAgMnPc2d1	křemičitý
štěrků	štěrk	k1gInPc2	štěrk
a	a	k8xC	a
jílovitých	jílovitý	k2eAgInPc2d1	jílovitý
písků	písek	k1gInPc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Horniny	hornina	k1gFnPc4	hornina
místy	místy	k6eAd1	místy
obsahuji	obsahovat	k5eAaImIp1nS	obsahovat
až	až	k9	až
desítky	desítka	k1gFnSc2	desítka
centimetrů	centimetr	k1gInPc2	centimetr
mocné	mocný	k2eAgFnSc2d1	mocná
vrstvy	vrstva	k1gFnSc2	vrstva
lignitu	lignit	k1gInSc2	lignit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Blízká	blízký	k2eAgNnPc1d1	blízké
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
přírodním	přírodní	k2eAgInSc7d1	přírodní
parkem	park	k1gInSc7	park
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nS	sousedit
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Stonáč	Stonáč	k1gInSc1	Stonáč
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1955	[number]	k4	1955
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
4,61	[number]	k4	4,61
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
500	[number]	k4	500
m	m	kA	m
od	od	k7c2	od
Záhlinických	Záhlinický	k2eAgInPc2d1	Záhlinický
rybníků	rybník	k1gInPc2	rybník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Skalky	skalka	k1gFnSc2	skalka
u	u	k7c2	u
Hulína	Hulín	k1gInSc2	Hulín
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
0,74	[number]	k4	0,74
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přírodním	přírodní	k2eAgInSc7d1	přírodní
parkem	park	k1gInSc7	park
sousedí	sousedit	k5eAaImIp3nS	sousedit
Evropsky	evropsky	k6eAd1	evropsky
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
Stonáč	Stonáč	k1gInSc1	Stonáč
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
5,5	[number]	k4	5,5
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
s	s	k7c7	s
parkem	park	k1gInSc7	park
sousedí	sousedit	k5eAaImIp3nS	sousedit
Evropsky	evropsky	k6eAd1	evropsky
významná	významný	k2eAgFnSc1d1	významná
lokalita	lokalita	k1gFnSc1	lokalita
Skalky	skalka	k1gFnSc2	skalka
vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
11,14	[number]	k4	11,14
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Filena	Filena	k1gFnSc1	Filena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
při	při	k7c6	při
rozloze	rozloha	k1gFnSc6	rozloha
105	[number]	k4	105
ha	ha	kA	ha
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
jihovýchodní	jihovýchodní	k2eAgFnSc4d1	jihovýchodní
část	část	k1gFnSc4	část
Svárovského	Svárovského	k2eAgInSc2d1	Svárovského
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
lužní	lužní	k2eAgInPc1d1	lužní
lesy	les	k1gInPc1	les
a	a	k8xC	a
podmáčené	podmáčený	k2eAgFnPc1d1	podmáčená
louky	louka	k1gFnPc1	louka
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
nebyla	být	k5eNaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
les	les	k1gInSc4	les
Zámeček	zámeček	k1gInSc4	zámeček
<g/>
,	,	kIx,	,
od	od	k7c2	od
západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
Svárovského	Svárovského	k2eAgInSc2d1	Svárovského
rybníka	rybník	k1gInSc2	rybník
až	až	k9	až
na	na	k7c4	na
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Planorbis	Planorbis	k1gFnSc1	Planorbis
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
4	[number]	k4	4
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cykloturistika	cykloturistika	k1gFnSc1	cykloturistika
==	==	k?	==
</s>
</p>
<p>
<s>
Přírodním	přírodní	k2eAgInSc7d1	přírodní
parkem	park	k1gInSc7	park
prochází	procházet	k5eAaImIp3nS	procházet
cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
trasa	trasa	k1gFnSc1	trasa
číslo	číslo	k1gNnSc1	číslo
5034	[number]	k4	5034
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
–	–	k?	–
<g/>
Kelč	Kelč	k1gFnSc1	Kelč
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
66	[number]	k4	66
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Přírodní	přírodní	k2eAgInSc4d1	přírodní
park	park	k1gInSc4	park
Záhlinické	Záhlinický	k2eAgInPc4d1	Záhlinický
rybníky	rybník	k1gInPc4	rybník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Záhlinické	Záhlinický	k2eAgInPc1d1	Záhlinický
rybníky	rybník	k1gInPc1	rybník
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Tlumačova	tlumačův	k2eAgInSc2d1	tlumačův
</s>
</p>
<p>
<s>
Záhlinické	Záhlinický	k2eAgInPc1d1	Záhlinický
rybníky	rybník	k1gInPc1	rybník
</s>
</p>
