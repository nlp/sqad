<p>
<s>
Enver	Enver	k1gMnSc1	Enver
Paša	paša	k1gMnSc1	paša
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
Enver	Enver	k1gMnSc1	Enver
Paşa	Paşa	k1gMnSc1	Paşa
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Enver	Enver	k1gMnSc1	Enver
Pascha	Pascha	k1gMnSc1	Pascha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
İ	İ	k?	İ
Enver	Enver	k1gInSc1	Enver
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
Istanbul	Istanbul	k1gInSc1	Istanbul
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
osmanský	osmanský	k2eAgMnSc1d1	osmanský
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
a	a	k8xC	a
hlavní	hlavní	k2eAgMnSc1d1	hlavní
vojenský	vojenský	k2eAgMnSc1d1	vojenský
vůdce	vůdce	k1gMnSc1	vůdce
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
lídrům	lídr	k1gMnPc3	lídr
mladoturecké	mladoturecký	k2eAgFnSc2d1	mladoturecká
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
země	zem	k1gFnSc2	zem
měl	mít	k5eAaImAgMnS	mít
Enver	Enver	k1gMnSc1	Enver
Paša	paša	k1gMnSc1	paša
hlavní	hlavní	k2eAgFnSc4d1	hlavní
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
vstup	vstup	k1gInSc4	vstup
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
balkánských	balkánský	k2eAgFnPc2d1	balkánská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluvládci	spoluvládce	k1gMnPc7	spoluvládce
Talatem	Talat	k1gMnSc7	Talat
a	a	k8xC	a
Džamalem	Džamal	k1gMnSc7	Džamal
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
strůjce	strůjce	k1gMnSc4	strůjce
arménské	arménský	k2eAgFnSc2d1	arménská
genocidy	genocida	k1gFnSc2	genocida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
si	se	k3xPyFc3	se
neustále	neustále	k6eAd1	neustále
navyšoval	navyšovat	k5eAaImAgMnS	navyšovat
své	svůj	k3xOyFgFnPc4	svůj
hodnosti	hodnost	k1gFnPc4	hodnost
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
"	"	kIx"	"
<g/>
Enver	Enver	k1gMnSc1	Enver
Paša	paša	k1gMnSc1	paša
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
titul	titul	k1gInSc1	titul
paša	paša	k1gMnSc1	paša
(	(	kIx(	(
<g/>
paşa	paşa	k1gMnSc1	paşa
<g/>
)	)	kIx)	)
znamenal	znamenat	k5eAaImAgInS	znamenat
v	v	k7c6	v
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
faktického	faktický	k2eAgMnSc4d1	faktický
vládce	vládce	k1gMnSc4	vládce
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
Enver	Enver	k1gMnSc1	Enver
Paša	paša	k1gMnSc1	paša
stal	stát	k5eAaPmAgMnS	stát
tureckým	turecký	k2eAgMnSc7d1	turecký
ministrem	ministr	k1gMnSc7	ministr
války	válka	k1gFnSc2	válka
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
členem	člen	k1gMnSc7	člen
triumvirátu	triumvirát	k1gInSc2	triumvirát
"	"	kIx"	"
<g/>
Tří	tři	k4xCgInPc2	tři
pašů	paš	k1gInPc2	paš
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Talatem	Talat	k1gMnSc7	Talat
a	a	k8xC	a
Džamalem	Džamal	k1gMnSc7	Džamal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
triumvirát	triumvirát	k1gInSc1	triumvirát
vládl	vládnout	k5eAaImAgInS	vládnout
zemi	zem	k1gFnSc4	zem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
a	a	k8xC	a
de	de	k?	de
facto	facto	k1gNnSc4	facto
vrchní	vrchní	k2eAgMnSc1d1	vrchní
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
ovládal	ovládat	k5eAaImAgMnS	ovládat
osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
armádu	armáda	k1gFnSc4	armáda
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
vojenským	vojenský	k2eAgMnSc7d1	vojenský
velitelem	velitel	k1gMnSc7	velitel
turecký	turecký	k2eAgMnSc1d1	turecký
sultán	sultán	k1gMnSc1	sultán
<g/>
,	,	kIx,	,
Enver	Enver	k1gMnSc1	Enver
měl	mít	k5eAaImAgMnS	mít
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
hodnost	hodnost	k1gFnSc1	hodnost
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
Turecka	Turecko	k1gNnSc2	Turecko
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
musel	muset	k5eAaImAgMnS	muset
Enver	Enver	k1gMnSc1	Enver
Paša	paša	k1gMnSc1	paša
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Rudé	rudý	k2eAgFnSc3d1	rudá
armádě	armáda	k1gFnSc3	armáda
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
poblíž	poblíž	k6eAd1	poblíž
Dušanbe	Dušanb	k1gInSc5	Dušanb
<g/>
,	,	kIx,	,
současného	současný	k2eAgNnSc2d1	současné
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
nositelem	nositel	k1gMnSc7	nositel
německého	německý	k2eAgNnSc2d1	německé
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
Pour	Pour	k1gMnSc1	Pour
le	le	k?	le
Mérite	Mérit	k1gInSc5	Mérit
vojenské	vojenský	k2eAgFnSc2d1	vojenská
třídy	třída	k1gFnPc1	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Enver	Enver	k1gMnSc1	Enver
Pasha	Pasha	k1gMnSc1	Pasha
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
