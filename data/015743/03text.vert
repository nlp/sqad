<s>
Falun	Falun	k1gMnSc1
</s>
<s>
Falun	Falun	k1gMnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
60	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+1	+1	k4
Stát	stát	k1gInSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Dalarna	Dalarna	k1gFnSc1
Komuna	komuna	k1gFnSc1
</s>
<s>
Falun	Falun	k1gMnSc1
</s>
<s>
Falun	Falun	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
25,16	25,16	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
38	#num#	k4
039	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1511	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Světové	světový	k2eAgFnSc2d1
dědictví	dědictví	k1gNnSc6
UNESCO	Unesco	k1gNnSc1
Název	název	k1gInSc4
lokality	lokalita	k1gFnSc2
</s>
<s>
těžební	těžební	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
měděného	měděný	k2eAgInSc2d1
dolu	dol	k1gInSc2
ve	v	k7c6
Falunu	Falun	k1gInSc6
Typ	typ	k1gInSc1
</s>
<s>
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kritérium	kritérium	k1gNnSc1
</s>
<s>
ii	ii	k?
<g/>
,	,	kIx,
iii	iii	k?
<g/>
,	,	kIx,
v	v	k7c4
Odkaz	odkaz	k1gInSc4
</s>
<s>
1027	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
</s>
<s>
2001	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc2
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.falun.se	www.falun.se	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+46	+46	k4
23	#num#	k4
PSČ	PSČ	kA
</s>
<s>
791	#num#	k4
XX	XX	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Falun	Falun	k1gMnSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
v	v	k7c6
centrálním	centrální	k2eAgNnSc6d1
Švédsku	Švédsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
správním	správní	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
kraje	kraj	k1gInSc2
Dalarna	Dalarno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžba	těžba	k1gFnSc1
mědi	měď	k1gFnSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
a	a	k8xC
dělnické	dělnický	k2eAgFnSc3d1
čtvrti	čtvrt	k1gFnSc3
města	město	k1gNnSc2
jsou	být	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
na	na	k7c6
seznamu	seznam	k1gInSc6
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Město	město	k1gNnSc1
Falun	Faluna	k1gFnPc2
vyrostlo	vyrůst	k5eAaPmAgNnS
kolem	kolem	k7c2
dolů	dol	k1gInPc2
na	na	k7c4
měď	měď	k1gFnSc4
a	a	k8xC
obživa	obživa	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
města	město	k1gNnSc2
byla	být	k5eAaImAgFnS
dlouho	dlouho	k6eAd1
na	na	k7c6
těžbě	těžba	k1gFnSc6
mědi	měď	k1gFnSc2
závislá	závislý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžilo	těžit	k5eAaImAgNnS
se	s	k7c7
zde	zde	k6eAd1
nepřerušeně	přerušeně	k6eNd1
přes	přes	k7c4
tisíc	tisíc	k4xCgInSc4
let	léto	k1gNnPc2
a	a	k8xC
podle	podle	k7c2
některých	některý	k3yIgInPc2
pramenů	pramen	k1gInPc2
se	se	k3xPyFc4
zde	zde	k6eAd1
těžilo	těžit	k5eAaImAgNnS
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
legendy	legenda	k1gFnSc2
naleziště	naleziště	k1gNnSc2
mědi	měď	k1gFnSc2
objevil	objevit	k5eAaPmAgMnS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
kozel	kozel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
zatoulal	zatoulat	k5eAaPmAgInS
do	do	k7c2
míst	místo	k1gNnPc2
ložiska	ložisko	k1gNnSc2
<g/>
,	,	kIx,
vyválel	vyválet	k5eAaPmAgInS
se	se	k3xPyFc4
na	na	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
když	když	k8xS
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
rohy	roh	k1gInPc4
obarvené	obarvený	k2eAgInPc4d1
mědí	měď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
vytvořilo	vytvořit	k5eAaPmAgNnS
malé	malý	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
<g/>
.	.	kIx.
1641	#num#	k4
dostal	dostat	k5eAaPmAgMnS
Falun	Falun	k1gInSc4
městská	městský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
rozvoj	rozvoj	k1gInSc1
města	město	k1gNnSc2
začal	začít	k5eAaPmAgInS
po	po	k7c6
výstavbě	výstavba	k1gFnSc6
železnice	železnice	k1gFnSc2
a	a	k8xC
továren	továrna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
město	město	k1gNnSc1
vyvinulo	vyvinout	k5eAaPmAgNnS
na	na	k7c4
administrativní	administrativní	k2eAgNnSc4d1
a	a	k8xC
vzdělávací	vzdělávací	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgInS
zavřen	zavřen	k2eAgInSc1d1
poslední	poslední	k2eAgInSc1d1
důl	důl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžba	těžba	k1gFnSc1
mědi	měď	k1gFnSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
a	a	k8xC
dělnické	dělnický	k2eAgFnSc3d1
čtvrti	čtvrt	k1gFnSc3
města	město	k1gNnSc2
jsou	být	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
na	na	k7c6
seznamu	seznam	k1gInSc6
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Ložisko	ložisko	k1gNnSc1
Falun	Faluna	k1gFnPc2
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejbohatším	bohatý	k2eAgNnPc3d3
sulfidickým	sulfidický	k2eAgNnPc3d1
ložiskům	ložisko	k1gNnPc3
Švédska	Švédsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
sulfidické	sulfidický	k2eAgInPc4d1
typy	typ	k1gInPc4
rud	ruda	k1gFnPc2
<g/>
,	,	kIx,
přítomnost	přítomnost	k1gFnSc1
magnetitových	magnetitový	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
rud	ruda	k1gFnPc2
v	v	k7c6
ložisku	ložisko	k1gNnSc6
indikuje	indikovat	k5eAaBmIp3nS
souvislost	souvislost	k1gFnSc1
s	s	k7c7
železnorudnými	železnorudný	k2eAgInPc7d1
horizonty	horizont	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rudy	ruda	k1gFnPc1
jsou	být	k5eAaImIp3nP
masivní	masivní	k2eAgFnPc1d1
<g/>
,	,	kIx,
vtroušené	vtroušený	k2eAgFnPc1d1
a	a	k8xC
další	další	k2eAgInSc1d1
typ	typ	k1gInSc1
je	být	k5eAaImIp3nS
lokálně	lokálně	k6eAd1
zván	zván	k2eAgInSc1d1
"	"	kIx"
<g/>
sköl	sköl	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
vtroušených	vtroušený	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
a	a	k8xC
žilek	žilka	k1gFnPc2
chalkopyritu	chalkopyrit	k1gInSc2
<g/>
,	,	kIx,
pyrhotinu	pyrhotin	k1gInSc2
<g/>
,	,	kIx,
sfaleritu	sfalerit	k1gInSc2
a	a	k8xC
galenitu	galenit	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turistická	turistický	k2eAgNnPc1d1
lákadla	lákadlo	k1gNnPc1
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
sportovní	sportovní	k2eAgInSc1d1
areál	areál	k1gInSc1
Lugnet	Lugneta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
součástí	součást	k1gFnPc2
jsou	být	k5eAaImIp3nP
dva	dva	k4xCgInPc4
skokanské	skokanský	k2eAgInPc4d1
můstky	můstek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konala	konat	k5eAaImAgNnP
se	se	k3xPyFc4
zde	zde	k6eAd1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
v	v	k7c6
letech	léto	k1gNnPc6
1954	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1993	#num#	k4
a	a	k8xC
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konají	konat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
i	i	k9
soutěže	soutěž	k1gFnSc2
v	v	k7c6
severské	severský	k2eAgFnSc6d1
kombinaci	kombinace	k1gFnSc6
<g/>
,	,	kIx,
biatlon	biatlon	k1gInSc4
<g/>
,	,	kIx,
skoky	skok	k1gInPc4
<g/>
,	,	kIx,
lyžování	lyžování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
kandidovalo	kandidovat	k5eAaImAgNnS
i	i	k9
na	na	k7c4
zimní	zimní	k2eAgFnPc4d1
Olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
lákadlem	lákadlo	k1gNnSc7
jsou	být	k5eAaImIp3nP
samozřejmě	samozřejmě	k6eAd1
doly	dol	k1gInPc1
zapsané	zapsaný	k2eAgInPc1d1
v	v	k7c6
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
nedaleko	daleko	k6eNd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
rodiště	rodiště	k1gNnSc1
Carla	Carl	k1gMnSc2
Larssona	Larsson	k1gMnSc2
-	-	kIx~
Sundborn	Sundborn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
sídlí	sídlet	k5eAaImIp3nS
florbalový	florbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
IBF	IBF	kA
Falun	Falun	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vyhrál	vyhrát	k5eAaPmAgInS
švédskou	švédský	k2eAgFnSc4d1
superligu	superliga	k1gFnSc4
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
a	a	k8xC
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
S	s	k7c7
městem	město	k1gNnSc7
Falun	Falun	k1gInSc1
se	se	k3xPyFc4
pojí	pojit	k5eAaImIp3nS,k5eAaPmIp3nS
ještě	ještě	k9
dva	dva	k4xCgInPc4
pojmy	pojem	k1gInPc4
-	-	kIx~
Faluröd	Faluröd	k1gMnSc1
a	a	k8xC
Falukorv	Falukorv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faluröd	Faluröda	k1gFnPc2
je	být	k5eAaImIp3nS
tmavočervená	tmavočervený	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
z	z	k7c2
typických	typický	k2eAgInPc2d1
švédských	švédský	k2eAgInPc2d1
domků	domek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
barva	barva	k1gFnSc1
byla	být	k5eAaImAgFnS
používána	používat	k5eAaImNgFnS
na	na	k7c4
domy	dům	k1gInPc4
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
po	po	k7c6
celém	celý	k2eAgNnSc6d1
Švédsku	Švédsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
národní	národní	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
domů	dům	k1gInPc2
a	a	k8xC
dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
oblíbená	oblíbený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falukorv	Falukorv	k1gInSc1
je	být	k5eAaImIp3nS
typický	typický	k2eAgInSc1d1
švédský	švédský	k2eAgInSc1d1
"	"	kIx"
<g/>
párek	párek	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Falunu	Falun	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
v	v	k7c6
dolech	dol	k1gInPc6
zaměstnáno	zaměstnat	k5eAaPmNgNnS
mnoho	mnoho	k4c1
německých	německý	k2eAgMnPc2d1
horníků	horník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
přišli	přijít	k5eAaPmAgMnP
na	na	k7c4
tento	tento	k3xDgInSc4
nápad	nápad	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zpracovat	zpracovat	k5eAaPmF
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
Falukorv	Falukorv	k1gInSc4
dostal	dostat	k5eAaPmAgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
českému	český	k2eAgInSc3d1
točenému	točený	k2eAgInSc3d1
salámu	salám	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jí	jíst	k5eAaImIp3nS
se	se	k3xPyFc4
nahrubo	nahrubo	k6eAd1
nakrájený	nakrájený	k2eAgInSc1d1
na	na	k7c4
chleba	chléb	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
těstovinami	těstovina	k1gFnPc7
<g/>
,	,	kIx,
k	k	k7c3
bramborové	bramborový	k2eAgFnSc3d1
kaši	kaša	k1gFnSc3
nebo	nebo	k8xC
jako	jako	k9
"	"	kIx"
<g/>
Korv	Korv	k1gMnSc1
Stroganoff	Stroganoff	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Sesterská	sesterský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Grudziądz	Grudziądz	k1gInSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Gütersloh	Gütersloh	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Hamina	Hamina	k1gFnSc1
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
</s>
<s>
Rø	Rø	k1gFnPc2
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
</s>
<s>
Vordingborg	Vordingborg	k1gInSc1
<g/>
,	,	kIx,
Dánsko	Dánsko	k1gNnSc1
</s>
<s>
Fotografie	fotografia	k1gFnPc1
</s>
<s>
Měděný	měděný	k2eAgInSc1d1
důl	důl	k1gInSc1
Falu	falos	k1gInSc2
koppargruva	koppargruva	k6eAd1
</s>
<s>
Měděný	měděný	k2eAgInSc1d1
důl	důl	k1gInSc1
Falu	falos	k1gInSc2
koppargruva	koppargruva	k6eAd1
</s>
<s>
Östanforså	Östanforså	k1gMnSc1
</s>
<s>
Kristinegymnasiet	Kristinegymnasiet	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Vaněček	Vaněček	k1gMnSc1
M.	M.	kA
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
:	:	kIx,
Nerostné	nerostný	k2eAgFnSc2d1
suroviny	surovina	k1gFnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudy	Ruda	k1gMnPc7
a	a	k8xC
nerudy	neruda	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
544	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
str	str	kA
<g/>
.	.	kIx.
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Geijer	Geijer	k1gInSc1
P.	P.	kA
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
:	:	kIx,
On	on	k3xPp3gMnSc1
the	the	k?
origin	origin	k1gInSc1
of	of	k?
the	the	k?
Falun	Falun	k1gInSc1
type	typ	k1gInSc5
sulfide	sulfid	k1gInSc5
mineralization	mineralization	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geol.	Geol.	k1gFnSc1
Fören	Förna	k1gFnPc2
Förth	Förtha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
86	#num#	k4
str	str	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
3	#num#	k4
-	-	kIx~
27	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Falun	Faluna	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Falun	Faluna	k1gFnPc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Švédsko	Švédsko	k1gNnSc1
–	–	k?
Sverige	Sverig	k1gInSc2
–	–	k?
(	(	kIx(
<g/>
S	s	k7c7
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
län	län	k?
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnPc1
správní	správní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
</s>
<s>
Blekinge	Blekinge	k1gFnSc1
(	(	kIx(
<g/>
Karlskrona	Karlskrona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dalarna	Dalarna	k1gFnSc1
(	(	kIx(
<g/>
Falun	Falun	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Gotland	Gotland	k1gInSc1
(	(	kIx(
<g/>
Visby	Visba	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Gävleborg	Gävleborg	k1gInSc1
(	(	kIx(
<g/>
Gävle	Gävle	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Halland	Halland	k1gInSc1
(	(	kIx(
<g/>
Halmstad	Halmstad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Jämtland	Jämtland	k1gInSc1
(	(	kIx(
<g/>
Östersund	Östersund	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Jönköping	Jönköping	k1gInSc1
(	(	kIx(
<g/>
Jönköping	Jönköping	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kalmar	Kalmar	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Kalmar	Kalmar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kronoberg	Kronoberg	k1gInSc1
(	(	kIx(
<g/>
Växjö	Växjö	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Norrbotten	Norrbottno	k1gNnPc2
(	(	kIx(
<g/>
Luleå	Luleå	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Skå	Skå	k1gMnSc5
(	(	kIx(
<g/>
Malmö	Malmö	k1gMnSc6
<g/>
)	)	kIx)
•	•	k?
Stockholm	Stockholm	k1gInSc1
(	(	kIx(
<g/>
Stockholm	Stockholm	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Södermanland	Södermanland	k1gInSc1
(	(	kIx(
<g/>
Nyköping	Nyköping	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Uppsala	Uppsala	k1gFnSc1
(	(	kIx(
<g/>
Uppsala	Uppsala	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Värmland	Värmland	k1gInSc1
(	(	kIx(
<g/>
Karlstad	Karlstad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Västerbotten	Västerbottno	k1gNnPc2
(	(	kIx(
<g/>
Umeå	Umeå	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Västernorrland	Västernorrland	k1gInSc1
(	(	kIx(
<g/>
Härnösand	Härnösand	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Västmanland	Västmanland	k1gInSc1
(	(	kIx(
<g/>
Västerå	Västerå	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Västra	Västra	k1gFnSc1
Götaland	Götaland	k1gInSc1
(	(	kIx(
<g/>
Göteborg	Göteborg	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Örebro	Örebro	k1gNnSc1
(	(	kIx(
<g/>
Örebro	Örebro	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Östergötland	Östergötland	k1gInSc1
(	(	kIx(
<g/>
Linköping	Linköping	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Švédské	švédský	k2eAgFnPc1d1
památky	památka	k1gFnPc1
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Drottningholmský	Drottningholmský	k2eAgInSc1d1
palác	palác	k1gInSc1
•	•	k?
Birka	Birek	k1gInSc2
a	a	k8xC
Hovgå	Hovgå	k1gNnPc2
•	•	k?
železárny	železárna	k1gFnSc2
Engelsberg	Engelsberg	k1gMnSc1
•	•	k?
skalní	skalní	k2eAgFnPc1d1
rytiny	rytina	k1gFnPc1
v	v	k7c4
Tanum	Tanum	k1gNnSc4
•	•	k?
Skogskyrkogå	Skogskyrkogå	k1gFnPc2
•	•	k?
hanzovní	hanzovní	k2eAgNnSc1d1
město	město	k1gNnSc1
Visby	Visba	k1gFnSc2
•	•	k?
kostelní	kostelní	k2eAgNnSc1d1
město	město	k1gNnSc1
Gammelstad	Gammelstad	k1gInSc1
•	•	k?
laponské	laponský	k2eAgNnSc4d1
území	území	k1gNnSc4
•	•	k?
námořní	námořní	k2eAgInSc1d1
přístav	přístav	k1gInSc1
Karlskrona	Karlskrona	k1gFnSc1
•	•	k?
zemědělská	zemědělský	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
ostrova	ostrov	k1gInSc2
Öland	Öland	k1gMnSc1
•	•	k?
Höga	Höga	k1gMnSc1
kusten	kusten	k2eAgMnSc1d1
/	/	kIx~
Kvarken	Kvarken	k1gInSc1
•	•	k?
těžba	těžba	k1gFnSc1
mědi	měď	k1gFnSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
Falun	Faluno	k1gNnPc2
•	•	k?
rádiová	rádiový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Grimeton	Grimeton	k1gInSc1
•	•	k?
Struveho	Struve	k1gMnSc2
geodetický	geodetický	k2eAgInSc4d1
oblouk	oblouk	k1gInSc4
•	•	k?
zdobené	zdobený	k2eAgFnSc2d1
farmy	farma	k1gFnSc2
v	v	k7c6
Hälsinglandu	Hälsingland	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4016392-1	4016392-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79060238	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
145402557	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79060238	#num#	k4
</s>
