<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
ÚNMZ	ÚNMZ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
správní	správní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
podřízen	podřídit	k5eAaPmNgInS
Ministerstvu	ministerstvo	k1gNnSc3
průmyslu	průmysl	k1gInSc3
a	a	k8xC
obchodu	obchod	k1gInSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>