<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
Český	český	k2eAgInSc1d1
normalizační	normalizační	k2eAgInSc1d1
institut	institut	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1993	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Biskupský	biskupský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
1148	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
110	#num#	k4
00	#num#	k4
Praha	Praha	k1gFnSc1
1	#num#	k4
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
124	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.unmz.cz	www.unmz.cz	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
unmz@unmz.cz	unmz@unmz.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
zdkaa	zdkaa	k1gFnSc1
<g/>
2	#num#	k4
<g/>
i	i	k8xC
IČO	IČO	kA
</s>
<s>
48135267	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
ÚNMZ	ÚNMZ	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
správní	správní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
podřízen	podřídit	k5eAaPmNgInS
Ministerstvu	ministerstvo	k1gNnSc3
průmyslu	průmysl	k1gInSc3
a	a	k8xC
obchodu	obchod	k1gInSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
zákonem	zákon	k1gInSc7
č.	č.	k?
20	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c4
zabezpečení	zabezpečení	k1gNnSc4
výkonu	výkon	k1gInSc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
technické	technický	k2eAgFnSc2d1
normalizace	normalizace	k1gFnSc2
<g/>
,	,	kIx,
metrologie	metrologie	k1gFnSc2
a	a	k8xC
státního	státní	k2eAgNnSc2d1
zkušebnictví	zkušebnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zabezpečuje	zabezpečovat	k5eAaImIp3nS
úkoly	úkol	k1gInPc4
vyplývající	vyplývající	k2eAgInPc4d1
z	z	k7c2
usnesení	usnesení	k1gNnSc2
vlády	vláda	k1gFnSc2
č.	č.	k?
631	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1994	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
zajištění	zajištění	k1gNnSc6
procesu	proces	k1gInSc2
integrace	integrace	k1gFnSc2
ČR	ČR	kA
do	do	k7c2
EU	EU	kA
včetně	včetně	k7c2
harmonizace	harmonizace	k1gFnSc2
právního	právní	k2eAgInSc2d1
řádu	řád	k1gInSc2
s	s	k7c7
EU	EU	kA
a	a	k8xC
sbližování	sbližování	k1gNnSc2
technický	technický	k2eAgInSc4d1
předpisů	předpis	k1gInPc2
a	a	k8xC
norem	norma	k1gFnPc2
z	z	k7c2
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
Zodpovídá	zodpovídat	k5eAaImIp3nS,k5eAaPmIp3nS
za	za	k7c4
zabezpečování	zabezpečování	k1gNnSc4
tvorby	tvorba	k1gFnSc2
<g/>
,	,	kIx,
vydávání	vydávání	k1gNnSc2
a	a	k8xC
řádné	řádný	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
českých	český	k2eAgFnPc2d1
technických	technický	k2eAgFnPc2d1
norem	norma	k1gFnPc2
<g/>
,	,	kIx,
normalizačních	normalizační	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
a	a	k8xC
publikací	publikace	k1gFnPc2
</s>
<s>
řídí	řídit	k5eAaImIp3nS
a	a	k8xC
zabezpečuje	zabezpečovat	k5eAaImIp3nS
státní	státní	k2eAgFnSc4d1
metrologii	metrologie	k1gFnSc4
podle	podle	k7c2
zákona	zákon	k1gInSc2
o	o	k7c4
metrologii	metrologie	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
autorizuje	autorizovat	k5eAaBmIp3nS
organizace	organizace	k1gFnSc1
k	k	k7c3
výkonům	výkon	k1gInPc3
v	v	k7c6
oblasti	oblast	k1gFnSc6
státní	státní	k2eAgFnSc2d1
metrologie	metrologie	k1gFnSc2
a	a	k8xC
k	k	k7c3
úřednímu	úřední	k2eAgNnSc3d1
měření	měření	k1gNnSc3
<g/>
,	,	kIx,
</s>
<s>
uznává	uznávat	k5eAaImIp3nS
zahraniční	zahraniční	k2eAgInPc4d1
certifikáty	certifikát	k1gInPc4
o	o	k7c4
schválení	schválení	k1gNnSc4
typu	typ	k1gInSc2
<g/>
,	,	kIx,
uznává	uznávat	k5eAaImIp3nS
ověření	ověření	k1gNnSc4
měřidla	měřidlo	k1gNnSc2
pro	pro	k7c4
ČR	ČR	kA
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
ověřeno	ověřit	k5eAaPmNgNnS
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
</s>
<s>
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c6
pokutách	pokuta	k1gFnPc6
za	za	k7c4
porušení	porušení	k1gNnSc4
předpisů	předpis	k1gInPc2
o	o	k7c4
metrologii	metrologie	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c6
vyjmutí	vyjmutí	k1gNnSc6
měřidla	měřidlo	k1gNnSc2
z	z	k7c2
působnosti	působnost	k1gFnSc2
metrologické	metrologický	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
vyhlašuje	vyhlašovat	k5eAaImIp3nS
a	a	k8xC
schvaluje	schvalovat	k5eAaImIp3nS
státní	státní	k2eAgInPc4d1
etalony	etalon	k1gInPc4
<g/>
,	,	kIx,
</s>
<s>
schvaluje	schvalovat	k5eAaImIp3nS
předpisy	předpis	k1gInPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
při	při	k7c6
měření	měření	k1gNnSc6
a	a	k8xC
také	také	k9
schvaluje	schvalovat	k5eAaImIp3nS
metrologické	metrologický	k2eAgInPc4d1
technické	technický	k2eAgInPc4d1
předpisy	předpis	k1gInPc4
<g/>
,	,	kIx,
</s>
<s>
stanovuje	stanovovat	k5eAaImIp3nS
seznam	seznam	k1gInSc4
měřidel	měřidlo	k1gNnPc2
podléhající	podléhající	k2eAgInSc1d1
státní	státní	k2eAgFnSc3d1
metrologické	metrologický	k2eAgFnSc3d1
kontrole	kontrola	k1gFnSc3
<g/>
,	,	kIx,
tzv.	tzv.	kA
Výměr	výměr	k1gInSc1
o	o	k7c6
stanovených	stanovený	k2eAgNnPc6d1
měřidlech	měřidlo	k1gNnPc6
<g/>
,	,	kIx,
</s>
<s>
přiděluje	přidělovat	k5eAaImIp3nS
kalibrační	kalibrační	k2eAgFnSc4d1
značku	značka	k1gFnSc4
střediskům	středisko	k1gNnPc3
kalibrační	kalibrační	k2eAgNnSc4d1
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
</s>
<s>
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c6
opravných	opravný	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
proti	proti	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
Českého	český	k2eAgInSc2d1
metrologického	metrologický	k2eAgInSc2d1
institutu	institut	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Metrologie	metrologie	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
metrologický	metrologický	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
normalizační	normalizační	k2eAgInSc1d1
institut	institut	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
technickou	technický	k2eAgFnSc4d1
normalizaci	normalizace	k1gFnSc4
<g/>
,	,	kIx,
metrologii	metrologie	k1gFnSc4
a	a	k8xC
státní	státní	k2eAgNnSc4d1
zkušebnictví	zkušebnictví	k1gNnSc4
za	za	k7c4
rok	rok	k1gInSc4
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
úřadu	úřad	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
158760655	#num#	k4
</s>
