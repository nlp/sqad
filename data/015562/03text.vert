<s>
Tabor	Tabor	k1gInSc1
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tabor	Tabor	k1gInSc1
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
VáclavaPoloha	VáclavaPoloh	k1gMnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
42	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
97	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
410	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
USA	USA	kA
USA	USA	kA
Stát	stát	k1gInSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
Okres	okres	k1gInSc1
</s>
<s>
Bon	bon	k1gInSc1
Homme	Homm	k1gInSc5
County	Count	k1gInPc5
</s>
<s>
Tabor	Tabor	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1,0	1,0	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
423	#num#	k4
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
434	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1871	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.taborsd.com	www.taborsd.com	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
605	#num#	k4
PSČ	PSČ	kA
</s>
<s>
57063	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tabor	Tabor	k1gMnSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
ve	v	k7c6
státě	stát	k1gInSc6
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
a	a	k8xC
v	v	k7c6
okrese	okres	k1gInSc6
Bon	bona	k1gFnPc2
Homme	Homm	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
hranice	hranice	k1gFnSc2
s	s	k7c7
Nebraskou	Nebraska	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
423	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
0.98	0.98	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Město	město	k1gNnSc4
založili	založit	k5eAaPmAgMnP
osadníci	osadník	k1gMnPc1
pocházející	pocházející	k2eAgMnPc1d1
z	z	k7c2
území	území	k1gNnPc2
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
a	a	k8xC
pojmenovali	pojmenovat	k5eAaPmAgMnP
ho	on	k3xPp3gMnSc4
po	po	k7c6
jihočeském	jihočeský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Tábor	Tábor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
slavné	slavný	k2eAgMnPc4d1
rodáky	rodák	k1gMnPc4
patří	patřit	k5eAaImIp3nS
Lou	Lou	k1gMnSc4
Koupal	koupat	k5eAaImAgMnS
(	(	kIx(
1898	#num#	k4
<g/>
–	–	k?
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
americký	americký	k2eAgMnSc1d1
baseballista	baseballista	k1gMnSc1
s	s	k7c7
českými	český	k2eAgInPc7d1
kořeny	kořen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
1910	#num#	k4
</s>
<s>
273	#num#	k4
</s>
<s>
1920	#num#	k4
</s>
<s>
428	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
397	#num#	k4
</s>
<s>
1940	#num#	k4
</s>
<s>
391	#num#	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
373	#num#	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
378	#num#	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
388	#num#	k4
</s>
<s>
1980	#num#	k4
</s>
<s>
460	#num#	k4
</s>
<s>
1990	#num#	k4
</s>
<s>
403	#num#	k4
</s>
<s>
2000	#num#	k4
</s>
<s>
417	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
423	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
odhad	odhad	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
408	#num#	k4
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Tabora	Tabora	k1gFnSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Jižní	jižní	k2eAgFnSc2d1
Dakoty	Dakota	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
</s>
<s>
Tabor	Tabor	k1gInSc1
Log	log	kA
school	school	k1gInSc1
</s>
<s>
Historický	historický	k2eAgInSc1d1
dům	dům	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tabor	Tabor	k1gMnSc1
<g/>
,	,	kIx,
South	South	k1gMnSc1
Dakota	Dakota	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Tabor	Tabora	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
