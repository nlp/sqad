<p>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Ciconia	Ciconium	k1gNnSc2	Ciconium
nigra	nigr	k1gMnSc2	nigr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zástupcem	zástupce	k1gMnSc7	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
čápovitých	čápovitý	k2eAgNnPc2d1	čápovitý
(	(	kIx(	(
<g/>
Ciconiidae	Ciconiidae	k1gNnPc2	Ciconiidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
černý	černý	k2eAgMnSc1d1	černý
pták	pták	k1gMnSc1	pták
s	s	k7c7	s
bronzovým	bronzový	k2eAgInSc7d1	bronzový
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
červeným	červený	k2eAgInSc7d1	červený
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
bílým	bílý	k2eAgNnSc7d1	bílé
břichem	břicho	k1gNnSc7	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
samcem	samec	k1gInSc7	samec
a	a	k8xC	a
samicí	samice	k1gFnSc7	samice
téměř	téměř	k6eAd1	téměř
není	být	k5eNaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
matně	matně	k6eAd1	matně
hnědá	hnědat	k5eAaImIp3nS	hnědat
s	s	k7c7	s
šedozeleným	šedozelený	k2eAgInSc7d1	šedozelený
zobákem	zobák	k1gInSc7	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
věku	věk	k1gInSc3	věk
přes	přes	k7c4	přes
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
plachý	plachý	k2eAgMnSc1d1	plachý
<g/>
,	,	kIx,	,
vzácnější	vzácný	k2eAgMnSc1d2	vzácnější
obyvatel	obyvatel	k1gMnSc1	obyvatel
horských	horský	k2eAgInPc2d1	horský
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
pahorkatin	pahorkatina	k1gFnPc2	pahorkatina
a	a	k8xC	a
lužních	lužní	k2eAgInPc2d1	lužní
lesů	les	k1gInPc2	les
žije	žít	k5eAaImIp3nS	žít
jednotlivě	jednotlivě	k6eAd1	jednotlivě
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
skrytě	skrytě	k6eAd1	skrytě
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
z	z	k7c2	z
klacků	klacek	k1gInPc2	klacek
a	a	k8xC	a
větví	větev	k1gFnPc2	větev
bývá	bývat	k5eAaImIp3nS	bývat
schováno	schovat	k5eAaPmNgNnS	schovat
v	v	k7c6	v
lesích	les	k1gInPc6	les
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
starých	starý	k2eAgInPc2d1	starý
stromů	strom	k1gInPc2	strom
nebo	nebo	k8xC	nebo
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
v	v	k7c6	v
mírnějších	mírný	k2eAgFnPc6d2	mírnější
oblastech	oblast	k1gFnPc6	oblast
Asie	Asie	k1gFnSc2	Asie
až	až	k9	až
po	po	k7c4	po
Dálný	dálný	k2eAgInSc4d1	dálný
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
a	a	k8xC	a
západoasijské	západoasijský	k2eAgFnPc1d1	západoasijský
populace	populace	k1gFnPc1	populace
táhnou	táhnout	k5eAaImIp3nP	táhnout
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
drobnými	drobný	k2eAgMnPc7d1	drobný
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
menších	malý	k2eAgInPc6d2	menší
vodních	vodní	k2eAgInPc6d1	vodní
tocích	tok	k1gInPc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Snáší	snášet	k5eAaImIp3nS	snášet
obvykle	obvykle	k6eAd1	obvykle
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
líhnou	líhnout	k5eAaImIp3nP	líhnout
mláďata	mládě	k1gNnPc4	mládě
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
prachovým	prachový	k2eAgNnSc7d1	prachové
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
černý	černý	k1gMnSc1	černý
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
rodu	rod	k1gInSc2	rod
Ciconia	Ciconium	k1gNnSc2	Ciconium
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
6	[number]	k4	6
dalších	další	k2eAgInPc2d1	další
recentních	recentní	k2eAgInPc2d1	recentní
(	(	kIx(	(
<g/>
žijících	žijící	k2eAgInPc2d1	žijící
<g/>
)	)	kIx)	)
druhů	druh	k1gInPc2	druh
čápů	čáp	k1gMnPc2	čáp
<g/>
:	:	kIx,	:
především	především	k9	především
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
čáp	čáp	k1gMnSc1	čáp
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
<g/>
,	,	kIx,	,
africký	africký	k2eAgMnSc1d1	africký
čáp	čáp	k1gMnSc1	čáp
simbil	simbil	k1gMnSc1	simbil
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příbuznými	příbuzný	k2eAgMnPc7d1	příbuzný
velkými	velký	k2eAgMnPc7d1	velký
ptáky	pták	k1gMnPc7	pták
jako	jako	k8xS	jako
např.	např.	kA	např.
marabu	marabu	k1gMnSc1	marabu
africkým	africký	k2eAgInPc3d1	africký
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgMnS	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
čápovitých	čápovitý	k2eAgFnPc2d1	čápovitý
(	(	kIx(	(
<g/>
Ciconiidae	Ciconiidae	k1gFnPc2	Ciconiidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
do	do	k7c2	do
řádu	řád	k1gInSc2	řád
brodivých	brodivý	k2eAgFnPc2d1	brodivá
(	(	kIx(	(
<g/>
Ciconiiformes	Ciconiiformesa	k1gFnPc2	Ciconiiformesa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
černý	černý	k1gMnSc1	černý
je	být	k5eAaImIp3nS	být
monotypický	monotypický	k2eAgInSc4d1	monotypický
druh	druh	k1gInSc4	druh
–	–	k?	–
netvoří	tvořit	k5eNaImIp3nS	tvořit
tedy	tedy	k9	tedy
žádné	žádný	k3yNgInPc4	žádný
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
černý	černý	k1gMnSc1	černý
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
běžnější	běžný	k2eAgMnSc1d2	běžnější
čáp	čáp	k1gMnSc1	čáp
bílý	bílý	k1gMnSc1	bílý
<g/>
:	:	kIx,	:
délka	délka	k1gFnSc1	délka
jeho	jeho	k3xOp3gNnSc2	jeho
těla	tělo	k1gNnSc2	tělo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
90	[number]	k4	90
<g/>
–	–	k?	–
<g/>
105	[number]	k4	105
cm	cm	kA	cm
a	a	k8xC	a
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
173	[number]	k4	173
<g/>
–	–	k?	–
<g/>
205	[number]	k4	205
cm	cm	kA	cm
<g/>
.	.	kIx.	.
<g/>
Váží	vážit	k5eAaImIp3nS	vážit
2,4	[number]	k4	2,4
až	až	k9	až
3,2	[number]	k4	3,2
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
bílého	bílý	k2eAgNnSc2d1	bílé
břicha	břicho	k1gNnSc2	břicho
je	být	k5eAaImIp3nS	být
tělo	tělo	k1gNnSc4	tělo
celé	celý	k2eAgFnSc2d1	celá
leskle	leskle	k6eAd1	leskle
černé	černý	k2eAgFnSc2d1	černá
(	(	kIx(	(
<g/>
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
samců	samec	k1gMnPc2	samec
jde	jít	k5eAaImIp3nS	jít
kovový	kovový	k2eAgInSc4d1	kovový
lesk	lesk	k1gInSc4	lesk
do	do	k7c2	do
zelena	zeleno	k1gNnSc2	zeleno
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
nebo	nebo	k8xC	nebo
purpuru	purpur	k1gInSc2	purpur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
bývají	bývat	k5eAaImIp3nP	bývat
matnější	matný	k2eAgFnPc1d2	matnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
narozená	narozený	k2eAgNnPc1d1	narozené
mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
prakticky	prakticky	k6eAd1	prakticky
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
;	;	kIx,	;
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
pak	pak	k6eAd1	pak
zpočátku	zpočátku	k6eAd1	zpočátku
hnědí	hnědit	k5eAaImIp3nS	hnědit
bez	bez	k7c2	bez
lesku	lesk	k1gInSc2	lesk
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
spíš	spíš	k9	spíš
šedočerní	šedočerný	k2eAgMnPc1d1	šedočerný
s	s	k7c7	s
jen	jen	k6eAd1	jen
slabým	slabý	k2eAgInSc7d1	slabý
nádechem	nádech	k1gInSc7	nádech
lesku	lesk	k1gInSc2	lesk
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
a	a	k8xC	a
na	na	k7c6	na
krovkách	krovka	k1gFnPc6	krovka
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
hnědočervené	hnědočervený	k2eAgFnPc4d1	hnědočervená
(	(	kIx(	(
<g/>
při	při	k7c6	při
hnízdění	hnízdění	k1gNnSc6	hnízdění
svítivě	svítivě	k6eAd1	svítivě
červené	červený	k2eAgFnSc2d1	červená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mláďata	mládě	k1gNnPc4	mládě
zprvu	zprvu	k6eAd1	zprvu
zobák	zobák	k1gInSc1	zobák
citrónově	citrónově	k6eAd1	citrónově
žlutý	žlutý	k2eAgInSc1d1	žlutý
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
přebarvují	přebarvovat	k5eAaImIp3nP	přebarvovat
–	–	k?	–
dospívají	dospívat	k5eAaImIp3nP	dospívat
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
dobarvují	dobarvovat	k5eAaImIp3nP	dobarvovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
nejvýše	vysoce	k6eAd3	vysoce
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
byl	být	k5eAaImAgInS	být
největší	veliký	k2eAgInSc1d3	veliký
naměřený	naměřený	k2eAgInSc1d1	naměřený
věk	věk	k1gInSc1	věk
30	[number]	k4	30
let	léto	k1gNnPc2	léto
u	u	k7c2	u
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
<g/>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
hnízda	hnízdo	k1gNnSc2	hnízdo
vydává	vydávat	k5eAaImIp3nS	vydávat
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
syčivé	syčivý	k2eAgInPc4d1	syčivý
a	a	k8xC	a
hvízdavé	hvízdavý	k2eAgInPc4d1	hvízdavý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
zobákem	zobák	k1gInSc7	zobák
klape	klapat	k5eAaImIp3nS	klapat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
klapání	klapání	k1gNnSc1	klapání
je	být	k5eAaImIp3nS	být
slabší	slabý	k2eAgFnSc1d2	slabší
než	než	k8xS	než
u	u	k7c2	u
čápa	čáp	k1gMnSc2	čáp
bílého	bílý	k1gMnSc2	bílý
a	a	k8xC	a
také	také	k9	také
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Čápi	čáp	k1gMnPc1	čáp
černí	čerň	k1gFnPc2	čerň
staví	stavit	k5eAaImIp3nP	stavit
svá	svůj	k3xOyFgNnPc4	svůj
hnízda	hnízdo	k1gNnPc4	hnízdo
na	na	k7c6	na
vysokých	vysoký	k2eAgInPc6d1	vysoký
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
použijí	použít	k5eAaPmIp3nP	použít
a	a	k8xC	a
dostaví	dostavit	k5eAaPmIp3nP	dostavit
i	i	k9	i
stará	starý	k2eAgNnPc1d1	staré
hnízda	hnízdo	k1gNnPc1	hnízdo
dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
většinou	většinou	k6eAd1	většinou
jednotlivě	jednotlivě	k6eAd1	jednotlivě
a	a	k8xC	a
dvě	dva	k4xCgNnPc1	dva
hnízda	hnízdo	k1gNnPc1	hnízdo
nebývají	bývat	k5eNaImIp3nP	bývat
blíž	blízce	k6eAd2	blízce
než	než	k8xS	než
500	[number]	k4	500
m	m	kA	m
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
však	však	k9	však
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
i	i	k8xC	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhnízdili	vyhnízdit	k5eAaPmAgMnP	vyhnízdit
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
čápa	čáp	k1gMnSc2	čáp
bílého	bílý	k1gMnSc2	bílý
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
používají	používat	k5eAaImIp3nP	používat
především	především	k9	především
klacky	klacek	k1gInPc1	klacek
a	a	k8xC	a
větve	větev	k1gFnPc1	větev
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
spojené	spojený	k2eAgNnSc1d1	spojené
blátem	bláto	k1gNnSc7	bláto
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
čápi	čáp	k1gMnPc1	čáp
bílí	bílý	k2eAgMnPc1d1	bílý
používají	používat	k5eAaImIp3nP	používat
hnízdo	hnízdo	k1gNnSc4	hnízdo
opakovaně	opakovaně	k6eAd1	opakovaně
a	a	k8xC	a
pokaždé	pokaždé	k6eAd1	pokaždé
ho	on	k3xPp3gInSc4	on
kus	kus	k1gInSc4	kus
přistaví	přistavit	k5eAaPmIp3nS	přistavit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
časem	čas	k1gInSc7	čas
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
obrovské	obrovský	k2eAgInPc4d1	obrovský
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
průměr	průměr	k1gInSc1	průměr
1,5	[number]	k4	1,5
m	m	kA	m
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
2	[number]	k4	2
m.	m.	k?	m.
Hnízdo	hnízdo	k1gNnSc1	hnízdo
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
blízko	blízko	k7c2	blízko
lesní	lesní	k2eAgFnSc2d1	lesní
paseky	paseka	k1gFnSc2	paseka
nebo	nebo	k8xC	nebo
mýtiny	mýtina	k1gFnSc2	mýtina
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
po	po	k7c6	po
prohřátí	prohřátý	k2eAgMnPc1d1	prohřátý
země	zem	k1gFnSc2	zem
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
stoupavé	stoupavý	k2eAgInPc1d1	stoupavý
proudy	proud	k1gInPc1	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
čáp	čáp	k1gMnSc1	čáp
využívá	využívat	k5eAaImIp3nS	využívat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
vzletu	vzlet	k1gInSc3	vzlet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
snáší	snášet	k5eAaImIp3nS	snášet
obvykle	obvykle	k6eAd1	obvykle
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
bílých	bílé	k1gNnPc2	bílé
slabě	slabě	k6eAd1	slabě
nazelenalých	nazelenalý	k2eAgNnPc2d1	nazelenalé
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
kolem	kolem	k6eAd1	kolem
65	[number]	k4	65
mm	mm	kA	mm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
přes	přes	k7c4	přes
80	[number]	k4	80
g.	g.	k?	g.
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
v	v	k7c4	v
jejich	jejich	k3xOp3gNnSc4	jejich
zahřívání	zahřívání	k1gNnSc4	zahřívání
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
vajec	vejce	k1gNnPc2	vejce
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
rodiče	rodič	k1gMnPc1	rodič
sedí	sedit	k5eAaImIp3nP	sedit
obvykle	obvykle	k6eAd1	obvykle
již	již	k9	již
od	od	k7c2	od
prvního	první	k4xOgNnSc2	první
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
doba	doba	k1gFnSc1	doba
sezení	sezení	k1gNnSc1	sezení
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
také	také	k6eAd1	také
mláďata	mládě	k1gNnPc4	mládě
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
krmí	krmit	k5eAaImIp3nS	krmit
nejméně	málo	k6eAd3	málo
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
(	(	kIx(	(
<g/>
až	až	k9	až
73	[number]	k4	73
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
mláďata	mládě	k1gNnPc1	mládě
schopná	schopný	k2eAgNnPc1d1	schopné
letu	let	k1gInSc3	let
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ještě	ještě	k9	ještě
přibližně	přibližně	k6eAd1	přibližně
další	další	k2eAgMnPc4d1	další
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gInPc4	on
rodiče	rodič	k1gMnPc1	rodič
přikrmují	přikrmovat	k5eAaImIp3nP	přikrmovat
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívají	dospívat	k5eAaImIp3nP	dospívat
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
udáváno	udáván	k2eAgNnSc1d1	udáváno
rozmezí	rozmezí	k1gNnSc1	rozmezí
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Čáp	Čáp	k1gMnSc1	Čáp
černý	černý	k1gMnSc1	černý
je	být	k5eAaImIp3nS	být
monogamní	monogamní	k2eAgInSc4d1	monogamní
druh	druh	k1gInSc4	druh
–	–	k?	–
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
stálého	stálý	k2eAgMnSc4d1	stálý
partnera	partner	k1gMnSc4	partner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
letech	léto	k1gNnPc6	léto
však	však	k9	však
své	svůj	k3xOyFgMnPc4	svůj
partnery	partner	k1gMnPc4	partner
střídá	střídat	k5eAaImIp3nS	střídat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etologie	etologie	k1gFnSc2	etologie
==	==	k?	==
</s>
</p>
<p>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
černý	černý	k1gMnSc1	černý
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
především	především	k6eAd1	především
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
lesy	les	k1gInPc4	les
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
nebo	nebo	k8xC	nebo
stojatých	stojatý	k2eAgFnPc2d1	stojatá
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
převažuje	převažovat	k5eAaImIp3nS	převažovat
v	v	k7c6	v
lesích	lese	k1gFnPc6	lese
listnatých	listnatý	k2eAgFnPc6d1	listnatá
a	a	k8xC	a
smíšených	smíšený	k2eAgFnPc6d1	smíšená
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
prameny	pramen	k1gInPc1	pramen
však	však	k9	však
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jeho	on	k3xPp3gInSc4	on
výskyt	výskyt	k1gInSc4	výskyt
spíše	spíše	k9	spíše
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
porostech	porost	k1gInPc6	porost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezlesé	bezlesý	k2eAgFnSc6d1	bezlesá
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
kulturní	kulturní	k2eAgFnSc6d1	kulturní
krajině	krajina	k1gFnSc6	krajina
s	s	k7c7	s
ojedinělými	ojedinělý	k2eAgInPc7d1	ojedinělý
stromy	strom	k1gInPc7	strom
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
;	;	kIx,	;
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
však	však	k9	však
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhnízdění	vyhnízdění	k1gNnSc6	vyhnízdění
a	a	k8xC	a
za	za	k7c2	za
tahu	tah	k1gInSc2	tah
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
na	na	k7c6	na
otevřených	otevřený	k2eAgFnPc6d1	otevřená
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
velmi	velmi	k6eAd1	velmi
skrytě	skrytě	k6eAd1	skrytě
–	–	k?	–
jeho	jeho	k3xOp3gNnSc1	jeho
hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
najít	najít	k5eAaPmF	najít
i	i	k9	i
pro	pro	k7c4	pro
zkušenější	zkušený	k2eAgMnPc4d2	zkušenější
ornitology	ornitolog	k1gMnPc4	ornitolog
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
pomocí	pomocí	k7c2	pomocí
satelitního	satelitní	k2eAgNnSc2d1	satelitní
sledování	sledování	k1gNnSc2	sledování
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Africká	africký	k2eAgFnSc1d1	africká
odysea	odysea	k1gFnSc1	odysea
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
přichází	přicházet	k5eAaImIp3nS	přicházet
až	až	k9	až
k	k	k7c3	k
okrajům	okraj	k1gInPc3	okraj
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
a	a	k8xC	a
zde	zde	k6eAd1	zde
skrytě	skrytě	k6eAd1	skrytě
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
potrava	potrava	k1gFnSc1	potrava
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
živočišná	živočišný	k2eAgFnSc1d1	živočišná
–	–	k?	–
hlavní	hlavní	k2eAgInSc1d1	hlavní
podíl	podíl	k1gInSc1	podíl
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
u	u	k7c2	u
čápa	čáp	k1gMnSc2	čáp
bílého	bílý	k1gMnSc2	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
ale	ale	k8xC	ale
také	také	k9	také
žáby	žába	k1gFnPc4	žába
<g/>
,	,	kIx,	,
mloky	mlok	k1gMnPc4	mlok
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
hady	had	k1gMnPc4	had
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pak	pak	k6eAd1	pak
drobné	drobný	k2eAgMnPc4d1	drobný
savce	savec	k1gMnPc4	savec
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
hledá	hledat	k5eAaImIp3nS	hledat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
vodách	voda	k1gFnPc6	voda
–	–	k?	–
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
lesních	lesní	k2eAgInPc6d1	lesní
potocích	potok	k1gInPc6	potok
a	a	k8xC	a
horských	horský	k2eAgFnPc6d1	horská
bystřinách	bystřina	k1gFnPc6	bystřina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
letět	letět	k5eAaImF	letět
i	i	k9	i
dvacet	dvacet	k4xCc4	dvacet
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Predátoři	predátor	k1gMnPc5	predátor
===	===	k?	===
</s>
</p>
<p>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
čáp	čáp	k1gMnSc1	čáp
černý	černý	k1gMnSc1	černý
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgMnPc4	žádný
přirozené	přirozený	k2eAgMnPc4d1	přirozený
predátory	predátor	k1gMnPc4	predátor
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
platit	platit	k5eAaImF	platit
jen	jen	k9	jen
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
rozšíření	rozšíření	k1gNnSc4	rozšíření
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
a	a	k8xC	a
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
čáp	čáp	k1gMnSc1	čáp
projektu	projekt	k1gInSc2	projekt
Africká	africký	k2eAgFnSc1d1	africká
odysea	odysea	k1gFnSc1	odysea
–	–	k?	–
samice	samice	k1gFnSc1	samice
Kristýna	Kristýna	k1gFnSc1	Kristýna
–	–	k?	–
se	se	k3xPyFc4	se
na	na	k7c6	na
zimovišti	zimoviště	k1gNnSc6	zimoviště
v	v	k7c6	v
Senegalu	Senegal	k1gInSc6	Senegal
stal	stát	k5eAaPmAgInS	stát
zřejmě	zřejmě	k6eAd1	zřejmě
obětí	oběť	k1gFnSc7	oběť
nějakého	nějaký	k3yIgMnSc2	nějaký
dravce	dravec	k1gMnSc2	dravec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Čápi	čáp	k1gMnPc1	čáp
černí	čerň	k1gFnPc2	čerň
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
na	na	k7c4	na
západ	západ	k1gInSc4	západ
zde	zde	k6eAd1	zde
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
části	část	k1gFnPc4	část
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
Beneluxu	Benelux	k1gInSc2	Benelux
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
severně	severně	k6eAd1	severně
od	od	k7c2	od
Himálaje	Himálaj	k1gFnSc2	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
–	–	k?	–
zdejší	zdejší	k2eAgFnSc2d1	zdejší
populace	populace	k1gFnSc2	populace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
patrně	patrně	k6eAd1	patrně
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
ze	z	k7c2	z
zde	zde	k6eAd1	zde
zimujících	zimující	k2eAgMnPc2d1	zimující
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
čáp	čáp	k1gMnSc1	čáp
černý	černý	k1gMnSc1	černý
vymizel	vymizet	k5eAaPmAgMnS	vymizet
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
vlivem	vliv	k1gInSc7	vliv
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
,	,	kIx,	,
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgInPc1d3	nejnižší
stavy	stav	k1gInPc1	stav
byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
nejdále	daleko	k6eAd3	daleko
do	do	k7c2	do
východních	východní	k2eAgFnPc2d1	východní
částí	část	k1gFnPc2	část
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
polského	polský	k2eAgNnSc2d1	polské
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
části	část	k1gFnSc3	část
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
nejjižnějšího	jižní	k2eAgInSc2d3	nejjižnější
okraje	okraj	k1gInSc2	okraj
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
maďarského	maďarský	k2eAgNnSc2d1	Maďarské
Podunají	Podunají	k1gNnSc2	Podunají
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
expanze	expanze	k1gFnSc1	expanze
stále	stále	k6eAd1	stále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
i	i	k9	i
do	do	k7c2	do
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
jižního	jižní	k2eAgNnSc2d1	jižní
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hnízdili	hnízdit	k5eAaImAgMnP	hnízdit
čápi	čáp	k1gMnPc1	čáp
černí	čerň	k1gFnPc2	čerň
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
<g/>
Celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
čápů	čáp	k1gMnPc2	čáp
černých	černý	k2eAgMnPc2d1	černý
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
24	[number]	k4	24
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
44	[number]	k4	44
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
asi	asi	k9	asi
15	[number]	k4	15
500	[number]	k4	500
<g/>
)	)	kIx)	)
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
jeho	on	k3xPp3gInSc2	on
hnízdního	hnízdní	k2eAgInSc2d1	hnízdní
areálu	areál	k1gInSc2	areál
na	na	k7c4	na
18	[number]	k4	18
800	[number]	k4	800
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tah	tah	k1gInSc1	tah
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
a	a	k8xC	a
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
pyrenejské	pyrenejský	k2eAgFnSc2d1	pyrenejská
populace	populace	k1gFnSc2	populace
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tažné	tažný	k2eAgMnPc4d1	tažný
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přezimují	přezimovat	k5eAaBmIp3nP	přezimovat
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
<g/>
.	.	kIx.	.
</s>
<s>
Tažné	tažný	k2eAgFnPc1d1	tažná
cesty	cesta	k1gFnPc1	cesta
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
hlavně	hlavně	k9	hlavně
podle	podle	k7c2	podle
hnízdní	hnízdní	k2eAgFnSc2d1	hnízdní
lokality	lokalita	k1gFnSc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
táhne	táhnout	k5eAaImIp3nS	táhnout
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
a	a	k8xC	a
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
populace	populace	k1gFnPc1	populace
z	z	k7c2	z
východu	východ	k1gInSc2	východ
zimují	zimovat	k5eAaImIp3nP	zimovat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Odlétají	odlétat	k5eAaImIp3nP	odlétat
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
už	už	k9	už
na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
od	od	k7c2	od
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
až	až	k9	až
po	po	k7c6	po
bílých	bílý	k2eAgMnPc6d1	bílý
čápech	čáp	k1gMnPc6	čáp
<g/>
.	.	kIx.	.
<g/>
Čápi	čáp	k1gMnPc1	čáp
z	z	k7c2	z
východních	východní	k2eAgFnPc2d1	východní
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
táhnou	táhnout	k5eAaImIp3nP	táhnout
obvykle	obvykle	k6eAd1	obvykle
přes	přes	k7c4	přes
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
do	do	k7c2	do
středních	střední	k2eAgFnPc2d1	střední
nebo	nebo	k8xC	nebo
východních	východní	k2eAgFnPc2d1	východní
oblastí	oblast	k1gFnPc2	oblast
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
ostatních	ostatní	k2eAgNnPc2d1	ostatní
českých	český	k2eAgNnPc2d1	české
území	území	k1gNnPc2	území
letí	letět	k5eAaImIp3nS	letět
většinou	většina	k1gFnSc7	většina
přes	přes	k7c4	přes
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
částí	část	k1gFnPc2	část
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tahu	tah	k1gInSc6	tah
uletí	uletět	k5eAaPmIp3nS	uletět
za	za	k7c4	za
den	den	k1gInSc4	den
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
km	km	kA	km
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
páru	pár	k1gInSc2	pár
mohou	moct	k5eAaImIp3nP	moct
létat	létat	k5eAaImF	létat
do	do	k7c2	do
zimovišť	zimoviště	k1gNnPc2	zimoviště
různými	různý	k2eAgInPc7d1	různý
směry	směr	k1gInPc7	směr
nebo	nebo	k8xC	nebo
volit	volit	k5eAaImF	volit
i	i	k9	i
odlišná	odlišný	k2eAgNnPc4d1	odlišné
zimoviště	zimoviště	k1gNnPc4	zimoviště
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
zimovištím	zimoviště	k1gNnPc3	zimoviště
však	však	k9	však
bývají	bývat	k5eAaImIp3nP	bývat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
věrní	věrný	k2eAgMnPc1d1	věrný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čápi	čáp	k1gMnPc1	čáp
táhnou	táhnout	k5eAaImIp3nP	táhnout
vždy	vždy	k6eAd1	vždy
jednotlivě	jednotlivě	k6eAd1	jednotlivě
–	–	k?	–
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
hejnech	hejno	k1gNnPc6	hejno
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
formacích	formace	k1gFnPc6	formace
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
létají	létat	k5eAaImIp3nP	létat
do	do	k7c2	do
zimovišť	zimoviště	k1gNnPc2	zimoviště
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
svých	svůj	k3xOyFgMnPc6	svůj
rodičích	rodič	k1gMnPc6	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tahem	tah	k1gInSc7	tah
někdy	někdy	k6eAd1	někdy
podnikají	podnikat	k5eAaImIp3nP	podnikat
tzv.	tzv.	kA	tzv.
předtahové	předtahový	k2eAgFnSc2d1	předtahový
potulky	potulka	k1gFnSc2	potulka
(	(	kIx(	(
<g/>
přelety	přelet	k1gInPc1	přelet
mimo	mimo	k7c4	mimo
směr	směr	k1gInSc4	směr
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
tahů	tah	k1gInPc2	tah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
běžné	běžný	k2eAgNnSc1d1	běžné
např.	např.	kA	např.
u	u	k7c2	u
volavek	volavka	k1gFnPc2	volavka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nebylo	být	k5eNaImAgNnS	být
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
doloženo	doložen	k2eAgNnSc1d1	doloženo
jeho	jeho	k3xOp3gNnSc4	jeho
hnízdění	hnízdění	k1gNnSc4	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
pravidelně	pravidelně	k6eAd1	pravidelně
hnízdit	hnízdit	k5eAaImF	hnízdit
v	v	k7c6	v
lužních	lužní	k2eAgInPc6d1	lužní
lesích	les	k1gInPc6	les
u	u	k7c2	u
soutoku	soutok	k1gInSc2	soutok
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgMnS	šířit
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
;	;	kIx,	;
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc4d1	zaznamenáno
první	první	k4xOgNnSc4	první
hnízdění	hnízdění	k1gNnSc4	hnízdění
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nyní	nyní	k6eAd1	nyní
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
roztroušeně	roztroušeně	k6eAd1	roztroušeně
v	v	k7c6	v
příhodných	příhodný	k2eAgInPc6d1	příhodný
biotopech	biotop	k1gInPc6	biotop
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
až	až	k9	až
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
horní	horní	k2eAgFnSc6d1	horní
hranici	hranice	k1gFnSc6	hranice
lesa	les	k1gInSc2	les
–	–	k?	–
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
zahnízdění	zahnízdění	k1gNnSc1	zahnízdění
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1050	[number]	k4	1050
m.	m.	k?	m.
Počet	počet	k1gInSc1	počet
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
:	:	kIx,	:
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nejvýše	vysoce	k6eAd3	vysoce
desítky	desítka	k1gFnPc1	desítka
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
77	[number]	k4	77
už	už	k9	už
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
minimálně	minimálně	k6eAd1	minimálně
288	[number]	k4	288
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
stavy	stav	k1gInPc7	stav
čápa	čáp	k1gMnSc2	čáp
černého	černý	k1gMnSc2	černý
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
na	na	k7c4	na
asi	asi	k9	asi
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
hnízdících	hnízdící	k2eAgInPc2d1	hnízdící
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Projekt	projekt	k1gInSc1	projekt
Africká	africký	k2eAgFnSc1d1	africká
odysea	odysea	k1gFnSc1	odysea
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
se	se	k3xPyFc4	se
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
projekt	projekt	k1gInSc1	projekt
Africká	africký	k2eAgFnSc1d1	africká
odysea	odysea	k1gFnSc1	odysea
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
sledováni	sledován	k2eAgMnPc1d1	sledován
čápi	čáp	k1gMnPc1	čáp
černí	černit	k5eAaImIp3nP	černit
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
na	na	k7c4	na
zimoviště	zimoviště	k1gNnSc4	zimoviště
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Čápi	čáp	k1gMnPc1	čáp
byli	být	k5eAaImAgMnP	být
při	při	k7c6	při
těchto	tento	k3xDgNnPc6	tento
sledováních	sledování	k1gNnPc6	sledování
vybaveni	vybaven	k2eAgMnPc1d1	vybaven
"	"	kIx"	"
<g/>
batůžky	batůžky	k?	batůžky
<g/>
"	"	kIx"	"
s	s	k7c7	s
vysílačkami	vysílačka	k1gFnPc7	vysílačka
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
sledování	sledování	k1gNnSc4	sledování
satelitní	satelitní	k2eAgNnSc4d1	satelitní
i	i	k8xC	i
pozemní	pozemní	k2eAgNnSc4d1	pozemní
telemetrií	telemetrie	k1gFnSc7	telemetrie
<g/>
.	.	kIx.	.
</s>
<s>
Posluchači	posluchač	k1gMnPc1	posluchač
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
čápů	čáp	k1gMnPc2	čáp
dozvídali	dozvídat	k5eAaImAgMnP	dozvídat
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
pořadech	pořad	k1gInPc6	pořad
Dobré	dobrá	k1gFnSc2	dobrá
jitro	jitro	k1gNnSc4	jitro
a	a	k8xC	a
Meteor	meteor	k1gInSc4	meteor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
organizátorem	organizátor	k1gMnSc7	organizátor
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgMnS	být
Miroslav	Miroslav	k1gMnSc1	Miroslav
Bobek	Bobek	k1gMnSc1	Bobek
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
redaktor	redaktor	k1gMnSc1	redaktor
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
ředitel	ředitel	k1gMnSc1	ředitel
Zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
čápem	čáp	k1gMnSc7	čáp
<g/>
,	,	kIx,	,
sledovaným	sledovaný	k2eAgMnSc7d1	sledovaný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
samice	samice	k1gFnSc1	samice
Kristýna	Kristýna	k1gFnSc1	Kristýna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
vysílačkami	vysílačka	k1gFnPc7	vysílačka
poprvé	poprvé	k6eAd1	poprvé
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1995	[number]	k4	1995
v	v	k7c6	v
lesích	les	k1gInPc6	les
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Brd	Brdy	k1gInPc2	Brdy
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
sledovat	sledovat	k5eAaImF	sledovat
3	[number]	k4	3
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
nepřetržitého	přetržitý	k2eNgNnSc2d1	nepřetržité
sledování	sledování	k1gNnSc2	sledování
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
živočicha	živočich	k1gMnSc4	živočich
pomocí	pomocí	k7c2	pomocí
vysílačů	vysílač	k1gInPc2	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
čápi	čáp	k1gMnPc1	čáp
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
sledování	sledování	k1gNnSc1	sledování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Viktor	Viktor	k1gMnSc1	Viktor
a	a	k8xC	a
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
,	,	kIx,	,
odchycení	odchycení	k1gNnSc1	odchycení
na	na	k7c6	na
Rakovnicku	Rakovnicko	k1gNnSc6	Rakovnicko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
červeného	červený	k2eAgInSc2d1	červený
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
je	být	k5eAaImIp3nS	být
čáp	čáp	k1gMnSc1	čáp
černý	černý	k1gMnSc1	černý
tzv.	tzv.	kA	tzv.
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
LC	LC	kA	LC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
nebo	nebo	k8xC	nebo
žádné	žádný	k3yNgFnPc1	žádný
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hodnocení	hodnocení	k1gNnSc1	hodnocení
je	být	k5eAaImIp3nS	být
však	však	k9	však
posuzováno	posuzovat	k5eAaImNgNnS	posuzovat
z	z	k7c2	z
celosvětového	celosvětový	k2eAgNnSc2d1	celosvětové
hlediska	hledisko	k1gNnSc2	hledisko
–	–	k?	–
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
řazen	řadit	k5eAaImNgInS	řadit
mezi	mezi	k7c4	mezi
silně	silně	k6eAd1	silně
ohrožené	ohrožený	k2eAgMnPc4d1	ohrožený
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
ornitologická	ornitologický	k2eAgFnSc1d1	ornitologická
–	–	k?	–
Pták	pták	k1gMnSc1	pták
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Čáp	Čáp	k1gMnSc1	Čáp
bílý	bílý	k1gMnSc1	bílý
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čáp	čáp	k1gMnSc1	čáp
černý	černý	k2eAgMnSc1d1	černý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Ciconia	Ciconium	k1gNnSc2	Ciconium
nigra	nigr	k1gMnSc2	nigr
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
BioLib	BioLiba	k1gFnPc2	BioLiba
</s>
</p>
<p>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
čápů	čáp	k1gMnPc2	čáp
ptákem	pták	k1gMnSc7	pták
roku	rok	k1gInSc2	rok
</s>
</p>
<p>
<s>
Hlas	hlas	k1gInSc1	hlas
čápa	čáp	k1gMnSc2	čáp
černého	černý	k1gMnSc2	černý
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hlas	hlas	k1gInSc1	hlas
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
</s>
</p>
