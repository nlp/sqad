<p>
<s>
Radvanovice	Radvanovice	k1gFnSc1	Radvanovice
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Schillerberg	Schillerberg	k1gInSc1	Schillerberg
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
šumavská	šumavský	k2eAgFnSc1d1	Šumavská
vesnice	vesnice	k1gFnSc1	vesnice
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Stožec	Stožec	k1gInSc1	Stožec
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vsi	ves	k1gFnSc2	ves
České	český	k2eAgInPc1d1	český
Žleby	žleb	k1gInPc1	žleb
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
existovala	existovat	k5eAaImAgFnS	existovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1752	[number]	k4	1752
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
završilo	završit	k5eAaPmAgNnS	završit
vysidlování	vysidlování	k1gNnSc1	vysidlování
započaté	započatý	k2eAgNnSc1d1	započaté
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vesnice	vesnice	k1gFnSc1	vesnice
stávala	stávat	k5eAaImAgFnS	stávat
<g/>
,	,	kIx,	,
umístěn	umístit	k5eAaPmNgInS	umístit
kamenný	kamenný	k2eAgInSc1d1	kamenný
památník	památník	k1gInSc1	památník
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
zánku	zánk	k1gInSc2	zánk
vsi	ves	k1gFnSc2	ves
<g/>
,	,	kIx,	,
a	a	k8xC	a
patrné	patrný	k2eAgFnPc1d1	patrná
jsou	být	k5eAaImIp3nP	být
rozvaliny	rozvalina	k1gFnPc1	rozvalina
původních	původní	k2eAgNnPc2d1	původní
stavení	stavení	k1gNnPc2	stavení
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
kromě	kromě	k7c2	kromě
vlastního	vlastní	k2eAgNnSc2d1	vlastní
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
v	v	k7c6	v
názvu	název	k1gInSc6	název
pahorku	pahorek	k1gInSc2	pahorek
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
vesnice	vesnice	k1gFnSc2	vesnice
(	(	kIx(	(
<g/>
Radvanovský	Radvanovský	k2eAgInSc1d1	Radvanovský
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
1012	[number]	k4	1012
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc1	její
jméno	jméno	k1gNnSc4	jméno
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
památný	památný	k2eAgInSc1d1	památný
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
30	[number]	k4	30
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
Radvanovická	Radvanovický	k2eAgFnSc1d1	Radvanovický
lípa	lípa	k1gFnSc1	lípa
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
památníku	památník	k1gInSc2	památník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hstorických	hstorický	k2eAgInPc2d1	hstorický
záznamů	záznam	k1gInPc2	záznam
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
žilo	žít	k5eAaImAgNnS	žít
191	[number]	k4	191
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
jednatřiceti	jednatřicet	k4xCc6	jednatřicet
staveních	stavení	k1gNnPc6	stavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Radvanovice	Radvanovice	k1gFnSc2	Radvanovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
