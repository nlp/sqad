<s>
Jméno	jméno	k1gNnSc1	jméno
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
kromě	kromě	k7c2	kromě
vlastního	vlastní	k2eAgNnSc2d1	vlastní
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
v	v	k7c6	v
názvu	název	k1gInSc6	název
pahorku	pahorek	k1gInSc2	pahorek
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
původní	původní	k2eAgFnSc2d1	původní
vesnice	vesnice	k1gFnSc2	vesnice
(	(	kIx(	(
<g/>
Radvanovský	Radvanovský	k2eAgInSc1d1	Radvanovský
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
1012	[number]	k4	1012
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc1	její
jméno	jméno	k1gNnSc4	jméno
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
památný	památný	k2eAgInSc1d1	památný
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
30	[number]	k4	30
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
Radvanovická	Radvanovický	k2eAgFnSc1d1	Radvanovický
lípa	lípa	k1gFnSc1	lípa
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
výše	výše	k1gFnSc2	výše
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
památníku	památník	k1gInSc2	památník
<g/>
.	.	kIx.	.
</s>
