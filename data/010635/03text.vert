<p>
<s>
Messier	Messier	k1gMnSc1	Messier
M21	M21	k1gMnSc1	M21
(	(	kIx(	(
<g/>
také	také	k9	také
M21	M21	k1gFnSc1	M21
nebo	nebo	k8xC	nebo
NGC	NGC	kA	NGC
6531	[number]	k4	6531
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Střelce	Střelec	k1gMnSc2	Střelec
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
900	[number]	k4	900
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
ji	on	k3xPp3gFnSc4	on
Charles	Charles	k1gMnSc1	Charles
Messier	Messier	k1gMnSc1	Messier
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1764	[number]	k4	1764
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozorování	pozorování	k1gNnSc2	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
M21	M21	k4	M21
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
°	°	k?	°
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
Polis	Polis	k1gFnSc2	Polis
(	(	kIx(	(
<g/>
μ	μ	k?	μ
Sgr	Sgr	k1gFnSc1	Sgr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
jeden	jeden	k4xCgInSc4	jeden
stupeň	stupeň	k1gInSc4	stupeň
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
mlhoviny	mlhovina	k1gFnSc2	mlhovina
Trifid	Trifida	k1gFnPc2	Trifida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
i	i	k9	i
pomocí	pomocí	k7c2	pomocí
triedru	triedr	k1gInSc2	triedr
10	[number]	k4	10
<g/>
x	x	k?	x
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
rozliší	rozlišit	k5eAaPmIp3nS	rozlišit
pouze	pouze	k6eAd1	pouze
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
114	[number]	k4	114
mm	mm	kA	mm
ovšem	ovšem	k9	ovšem
ukáže	ukázat	k5eAaPmIp3nS	ukázat
různé	různý	k2eAgFnPc4d1	různá
hvězdy	hvězda	k1gFnPc4	hvězda
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
200	[number]	k4	200
mm	mm	kA	mm
hvězdokupu	hvězdokupa	k1gFnSc4	hvězdokupa
rozloží	rozložit	k5eAaPmIp3nS	rozložit
úplně	úplně	k6eAd1	úplně
a	a	k8xC	a
okrajové	okrajový	k2eAgFnSc3d1	okrajová
oblasti	oblast	k1gFnSc3	oblast
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
vypadají	vypadat	k5eAaImIp3nP	vypadat
velice	velice	k6eAd1	velice
nepravidelně	pravidelně	k6eNd1	pravidelně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
část	část	k1gFnSc1	část
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
zdobí	zdobit	k5eAaImIp3nS	zdobit
asi	asi	k9	asi
10	[number]	k4	10
slabých	slabý	k2eAgFnPc2d1	slabá
hvězd	hvězda	k1gFnPc2	hvězda
seřazených	seřazený	k2eAgFnPc2d1	seřazená
do	do	k7c2	do
kroužku	kroužek	k1gInSc2	kroužek
<g/>
.	.	kIx.	.
<g/>
Mlhovina	mlhovina	k1gFnSc1	mlhovina
leží	ležet	k5eAaImIp3nS	ležet
blízko	blízko	k7c2	blízko
ekliptiky	ekliptika	k1gFnSc2	ekliptika
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
často	často	k6eAd1	často
přechází	přecházet	k5eAaImIp3nP	přecházet
tělesa	těleso	k1gNnPc1	těleso
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
Messierova	Messierův	k2eAgInSc2d1	Messierův
katalogu	katalog	k1gInSc2	katalog
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
2	[number]	k4	2
<g/>
°	°	k?	°
jižně	jižně	k6eAd1	jižně
leží	ležet	k5eAaImIp3nS	ležet
mlhovina	mlhovina	k1gFnSc1	mlhovina
Laguna	laguna	k1gFnSc1	laguna
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
°	°	k?	°
severozápadně	severozápadně	k6eAd1	severozápadně
otevřená	otevřený	k2eAgFnSc1d1	otevřená
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
M23	M23	k1gFnSc2	M23
a	a	k8xC	a
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
další	další	k2eAgInPc1d1	další
objekty	objekt	k1gInPc1	objekt
mezi	mezi	k7c7	mezi
kulovou	kulový	k2eAgFnSc7d1	kulová
hvězdokupou	hvězdokupa	k1gFnSc7	hvězdokupa
M22	M22	k1gFnSc7	M22
a	a	k8xC	a
Orlí	orlí	k2eAgFnSc7d1	orlí
mlhovinou	mlhovina	k1gFnSc7	mlhovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M21	M21	k4	M21
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jednoduše	jednoduše	k6eAd1	jednoduše
pozorovat	pozorovat	k5eAaImF	pozorovat
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
obydlených	obydlený	k2eAgFnPc2d1	obydlená
oblastí	oblast	k1gFnPc2	oblast
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
dostatečně	dostatečně	k6eAd1	dostatečně
nízkou	nízký	k2eAgFnSc4d1	nízká
jižní	jižní	k2eAgFnSc4d1	jižní
deklinaci	deklinace	k1gFnSc4	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
není	být	k5eNaImIp3nS	být
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
blízko	blízko	k7c2	blízko
polárního	polární	k2eAgInSc2d1	polární
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
poměrně	poměrně	k6eAd1	poměrně
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
dobře	dobře	k6eAd1	dobře
viditelná	viditelný	k2eAgFnSc1d1	viditelná
vysoko	vysoko	k6eAd1	vysoko
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
během	během	k7c2	během
jižních	jižní	k2eAgFnPc2d1	jižní
zimních	zimní	k2eAgFnPc2d1	zimní
nocí	noc	k1gFnPc2	noc
a	a	k8xC	a
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
blízko	blízko	k7c2	blízko
obratníku	obratník	k1gInSc2	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
vidět	vidět	k5eAaImF	vidět
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnPc1d3	nejvhodnější
období	období	k1gNnPc1	období
pro	pro	k7c4	pro
její	její	k3xOp3gNnPc4	její
pozorování	pozorování	k1gNnPc4	pozorování
na	na	k7c6	na
večerní	večerní	k2eAgFnSc6d1	večerní
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
pozorování	pozorování	k1gNnSc2	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
Hvězdokupu	hvězdokupa	k1gFnSc4	hvězdokupa
M21	M21	k1gFnSc2	M21
objevil	objevit	k5eAaPmAgMnS	objevit
Charles	Charles	k1gMnSc1	Charles
Messier	Messier	k1gMnSc1	Messier
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1764	[number]	k4	1764
během	během	k7c2	během
hledání	hledání	k1gNnSc2	hledání
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
malou	malý	k2eAgFnSc4d1	malá
hvězdokupu	hvězdokupa	k1gFnSc4	hvězdokupa
a	a	k8xC	a
zařadil	zařadit	k5eAaPmAgMnS	zařadit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
slavného	slavný	k2eAgInSc2d1	slavný
katalogu	katalog	k1gInSc2	katalog
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Herschel	Herschel	k1gMnSc1	Herschel
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
hvězdokupě	hvězdokupa	k1gFnSc6	hvězdokupa
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
žádné	žádný	k3yNgFnPc4	žádný
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
oblohu	obloha	k1gFnSc4	obloha
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Admirál	admirál	k1gMnSc1	admirál
Smyth	Smyth	k1gMnSc1	Smyth
ji	on	k3xPp3gFnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
hvězdokupu	hvězdokupa	k1gFnSc4	hvězdokupa
umístěnou	umístěný	k2eAgFnSc7d1	umístěná
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
bohatém	bohatý	k2eAgNnSc6d1	bohaté
poli	pole	k1gNnSc6	pole
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
M21	M21	k4	M21
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
hvězdokupu	hvězdokupa	k1gFnSc4	hvězdokupa
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
zhuštěné	zhuštěný	k2eAgNnSc1d1	zhuštěné
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
jejími	její	k3xOp3gFnPc7	její
hvězdami	hvězda	k1gFnPc7	hvězda
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
jednomu	jeden	k4xCgNnSc3	jeden
světelnému	světelný	k2eAgInSc3d1	světelný
roku	rok	k1gInSc3	rok
<g/>
.	.	kIx.	.
<g/>
Hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
okolo	okolo	k7c2	okolo
57	[number]	k4	57
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gFnPc1	její
nejjasnější	jasný	k2eAgFnPc1d3	nejjasnější
hvězdy	hvězda	k1gFnPc1	hvězda
osmé	osmý	k4xOgFnSc2	osmý
magnitudy	magnituda	k1gFnSc2	magnituda
jsou	být	k5eAaImIp3nP	být
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
B0	B0	k1gFnSc2	B0
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
mladé	mladý	k2eAgFnPc1d1	mladá
<g/>
;	;	kIx,	;
její	její	k3xOp3gNnSc1	její
stáří	stáří	k1gNnSc1	stáří
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
její	její	k3xOp3gFnSc2	její
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uznává	uznávat	k5eAaImIp3nS	uznávat
se	se	k3xPyFc4	se
hodnota	hodnota	k1gFnSc1	hodnota
kolem	kolem	k7c2	kolem
3	[number]	k4	3
900	[number]	k4	900
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
M21	M21	k1gFnSc2	M21
(	(	kIx(	(
<g/>
astronomia	astronomia	k1gFnSc1	astronomia
<g/>
)	)	kIx)	)
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Messier	Messier	k1gInSc1	Messier
21	[number]	k4	21
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Simbad	Simbad	k6eAd1	Simbad
–	–	k?	–
Messier	Messier	k1gMnSc1	Messier
21	[number]	k4	21
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc1	Universe
<g/>
:	:	kIx,	:
seznam	seznam	k1gInSc1	seznam
otevřených	otevřený	k2eAgFnPc2d1	otevřená
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
</s>
</p>
