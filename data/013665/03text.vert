<s>
Integrovaný	integrovaný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Typický	typický	k2eAgInSc1d1
označník	označník	k1gInSc1
autobusové	autobusový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
IDS	IDS	kA
JMK	JMK	kA
<g/>
:	:	kIx,
zastávka	zastávka	k1gFnSc1
Moravský	moravský	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
železniční	železniční	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
</s>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
IDS	IDS	kA
JMK	JMK	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
integrovaný	integrovaný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnPc1d1
tramvaje	tramvaj	k1gFnPc1
<g/>
,	,	kIx,
trolejbusy	trolejbus	k1gInPc1
<g/>
,	,	kIx,
vlaky	vlak	k1gInPc1
<g/>
,	,	kIx,
městské	městský	k2eAgFnSc2d1
a	a	k8xC
příměstské	příměstský	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
linky	linka	k1gFnSc2
a	a	k8xC
lodní	lodní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
na	na	k7c6
Brněnské	brněnský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
IDS	IDS	kA
JMK	JMK	kA
obsluhuje	obsluhovat	k5eAaImIp3nS
po	po	k7c6
ukončení	ukončení	k1gNnSc6
6	#num#	k4
<g/>
.	.	kIx.
etapy	etapa	k1gFnSc2
v	v	k7c6
červenci	červenec	k1gInSc6
2010	#num#	k4
celé	celá	k1gFnSc2
území	území	k1gNnPc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
místně	místně	k6eAd1
zasahuje	zasahovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
okolních	okolní	k2eAgInPc2d1
krajů	kraj	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
Pardubický	pardubický	k2eAgInSc1d1
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgMnSc1d1
a	a	k8xC
Zlínský	zlínský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Autobusové	autobusový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
IDS	IDS	kA
JMK	JMK	kA
zajíždí	zajíždět	k5eAaImIp3nS
i	i	k9
do	do	k7c2
několika	několik	k4yIc2
rakouských	rakouský	k2eAgMnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
vlaková	vlakový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
vedena	vést	k5eAaImNgFnS
na	na	k7c4
Slovensko	Slovensko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Koordinátorem	koordinátor	k1gMnSc7
IDS	IDS	kA
JMK	JMK	kA
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc4
KORDIS	KORDIS	kA
JMK	JMK	kA
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
o.	o.	k?
založená	založený	k2eAgNnPc4d1
Jihomoravským	jihomoravský	k2eAgInSc7d1
krajem	kraj	k1gInSc7
a	a	k8xC
Statutárním	statutární	k2eAgNnSc7d1
městem	město	k1gNnSc7
Brnem	Brno	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
rozvoje	rozvoj	k1gInSc2
</s>
<s>
Autobus	autobus	k1gInSc4
Irisbus	Irisbus	k1gInSc1
Crossway	Crosswaa	k1gFnSc2
12M	12M	k4
dopravce	dopravce	k1gMnSc2
ČSAD	ČSAD	kA
Tišnov	Tišnov	k1gInSc1
vyčkává	vyčkávat	k5eAaImIp3nS
v	v	k7c6
terminálu	terminál	k1gInSc6
před	před	k7c7
železniční	železniční	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
Kuřim	Kuřim	k1gInSc1
na	na	k7c4
pravidelný	pravidelný	k2eAgInSc4d1
odjezd	odjezd	k1gInSc4
regionální	regionální	k2eAgFnSc2d1
linky	linka	k1gFnSc2
311	#num#	k4
</s>
<s>
Plány	plán	k1gInPc1
na	na	k7c4
integraci	integrace	k1gFnSc4
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c4
území	území	k1gNnPc4
nově	nově	k6eAd1
vzniklého	vzniklý	k2eAgInSc2d1
kraje	kraj	k1gInSc2
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgFnP
na	na	k7c6
začátku	začátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřízení	zřízení	k1gNnPc1
„	„	k?
<g/>
Koordinátora	koordinátor	k1gMnSc2
integrovaného	integrovaný	k2eAgInSc2d1
systému	systém	k1gInSc2
veřejné	veřejný	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
“	“	k?
bylo	být	k5eAaImAgNnS
Statutárním	statutární	k2eAgFnPc3d1
městem	město	k1gNnSc7
Brnem	Brno	k1gNnSc7
a	a	k8xC
Jihomoravským	jihomoravský	k2eAgInSc7d1
krajem	krajem	k6eAd1
schváleno	schválit	k5eAaPmNgNnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
KORDIS	KORDIS	kA
JMK	JMK	kA
zahájila	zahájit	k5eAaPmAgFnS
přípravné	přípravný	k2eAgFnPc4d1
práce	práce	k1gFnPc4
na	na	k7c6
IDS	IDS	kA
v	v	k7c6
říjnu	říjen	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
příprav	příprava	k1gFnPc2
na	na	k7c6
spuštění	spuštění	k1gNnSc6
IDS	IDS	kA
byly	být	k5eAaImAgFnP
v	v	k7c6
Brně	Brno	k1gNnSc6
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
zavedeny	zaveden	k2eAgFnPc4d1
tarifní	tarifní	k2eAgFnPc4d1
zóny	zóna	k1gFnPc4
a	a	k8xC
přečíslovány	přečíslován	k2eAgFnPc4d1
trolejbusové	trolejbusový	k2eAgFnPc4d1
linky	linka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
etapa	etapa	k1gFnSc1
IDS	IDS	kA
JMK	JMK	kA
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IDS	IDS	kA
zahrnoval	zahrnovat	k5eAaImAgInS
110	#num#	k4
obcí	obec	k1gFnPc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
MHD	MHD	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
v	v	k7c6
Blansku	Blansko	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
jedné	jeden	k4xCgFnSc6
obci	obec	k1gFnSc6
z	z	k7c2
krajů	kraj	k1gInPc2
Olomouckého	olomoucký	k2eAgNnSc2d1
a	a	k8xC
Vysočina	vysočina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Etapa	etapa	k1gFnSc1
2A	2A	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
<g/>
)	)	kIx)
zahrnula	zahrnout	k5eAaPmAgFnS
49	#num#	k4
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
především	především	k6eAd1
na	na	k7c6
Tišnovsku	Tišnovsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povětšinou	povětšinou	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
obce	obec	k1gFnPc4
převedené	převedený	k2eAgFnPc1d1
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2005	#num#	k4
z	z	k7c2
kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
do	do	k7c2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2005	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zapojení	zapojení	k1gNnSc3
Popovic	Popovice	k1gFnPc2
a	a	k8xC
linky	linka	k1gFnSc2
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Adamově	Adamov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Etapa	etapa	k1gFnSc1
2B	2B	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2005	#num#	k4
<g/>
)	)	kIx)
rozšířila	rozšířit	k5eAaPmAgFnS
IDS	IDS	kA
JMK	JMK	kA
na	na	k7c4
Sokolnicko	Sokolnicko	k1gNnSc4
a	a	k8xC
Zbraslavsko	Zbraslavsko	k1gNnSc4
(	(	kIx(
<g/>
24	#num#	k4
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Etapa	etapa	k1gFnSc1
3A	3A	k4
rozšířila	rozšířit	k5eAaPmAgFnS
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2005	#num#	k4
IDS	IDS	kA
JMK	JMK	kA
do	do	k7c2
22	#num#	k4
obcí	obec	k1gFnPc2
Slavkovska	Slavkovsko	k1gNnSc2
<g/>
,	,	kIx,
Bučovicka	Bučovicko	k1gNnSc2
a	a	k8xC
Vyškovska	Vyškovsko	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
MHD	MHD	kA
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Etapa	etapa	k1gFnSc1
3B	3B	k4
rozšířila	rozšířit	k5eAaPmAgFnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2006	#num#	k4
obsluhované	obsluhovaný	k2eAgFnSc2d1
území	území	k1gNnSc4
o	o	k7c4
Ivančicko	Ivančicko	k1gNnSc4
(	(	kIx(
<g/>
21	#num#	k4
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Etapa	etapa	k1gFnSc1
3C	3C	k4
rozšířila	rozšířit	k5eAaPmAgFnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2006	#num#	k4
obsluhované	obsluhovaný	k2eAgFnSc2d1
území	území	k1gNnSc4
o	o	k7c4
Židlochovicko	Židlochovicko	k1gNnSc4
(	(	kIx(
<g/>
29	#num#	k4
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Etapa	etapa	k1gFnSc1
4A	4A	k4
obsluhuje	obsluhovat	k5eAaImIp3nS
od	od	k7c2
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
Boskovicko	Boskovicko	k1gNnSc1
(	(	kIx(
<g/>
110	#num#	k4
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Náměšť	Náměšť	k1gFnSc1
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Etapa	etapa	k1gFnSc1
4B	4B	k4
rozšířila	rozšířit	k5eAaPmAgFnS
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2008	#num#	k4
obsluhované	obsluhovaný	k2eAgFnSc2d1
území	území	k1gNnSc4
o	o	k7c4
Vyškovsko	Vyškovsko	k1gNnSc4
a	a	k8xC
část	část	k1gFnSc4
Kyjovska	Kyjovsko	k1gNnSc2
(	(	kIx(
<g/>
62	#num#	k4
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
etapou	etapa	k1gFnSc7
5	#num#	k4
se	se	k3xPyFc4
IDS	IDS	kA
JMK	JMK	kA
od	od	k7c2
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
rozšířil	rozšířit	k5eAaPmAgInS
na	na	k7c4
Břeclavsko	Břeclavsko	k1gNnSc4
<g/>
,	,	kIx,
Hodonínsko	Hodonínsko	k1gNnSc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
slovenských	slovenský	k2eAgNnPc2d1
měst	město	k1gNnPc2
Holíč	Holíč	k1gMnSc1
a	a	k8xC
Skalica	Skalica	k1gMnSc1
a	a	k8xC
obcí	obec	k1gFnPc2
Kátov	Kátovo	k1gNnPc2
a	a	k8xC
Vrádište	Vrádište	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
obsluhuje	obsluhovat	k5eAaImIp3nS
linka	linka	k1gFnSc1
č.	č.	k?
910	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kyjovsko	Kyjovsko	k1gNnSc1
<g/>
,	,	kIx,
Hustopečsko	Hustopečsko	k1gNnSc1
a	a	k8xC
Veselsko	Veselsko	k1gNnSc1
(	(	kIx(
<g/>
124	#num#	k4
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
včetně	včetně	k7c2
MHD	MHD	kA
v	v	k7c6
Břeclavi	Břeclav	k1gFnSc6
<g/>
,	,	kIx,
Hodoníně	Hodonín	k1gInSc6
<g/>
,	,	kIx,
Kyjově	Kyjov	k1gInSc6
a	a	k8xC
Mikulově	Mikulov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2010	#num#	k4
byla	být	k5eAaImAgFnS
zaintegrována	zaintegrovat	k5eAaBmNgFnS,k5eAaPmNgFnS,k5eAaImNgFnS
MHD	MHD	kA
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
bylo	být	k5eAaImAgNnS
do	do	k7c2
IDS	IDS	kA
JMK	JMK	kA
etapou	etapa	k1gFnSc7
6	#num#	k4
začleněno	začleněn	k2eAgNnSc1d1
Znojemsko	Znojemsko	k1gNnSc1
<g/>
,	,	kIx,
Jevišovicko	Jevišovicko	k1gNnSc1
<g/>
,	,	kIx,
Hrušovansko	Hrušovansko	k1gNnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
rakouského	rakouský	k2eAgNnSc2d1
města	město	k1gNnSc2
Laa	Laa	k1gMnSc1
an	an	k?
der	drát	k5eAaImRp2nS
Thaya	Thay	k2eAgNnPc4d1
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
zajíždí	zajíždět	k5eAaImIp3nS
linka	linka	k1gFnSc1
č.	č.	k?
104	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Miroslavsko	Miroslavsko	k1gNnSc4
(	(	kIx(
<g/>
161	#num#	k4
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
bylo	být	k5eAaImAgNnS
integrovaným	integrovaný	k2eAgInSc7d1
dopravním	dopravní	k2eAgInSc7d1
systémem	systém	k1gInSc7
pokryto	pokryt	k2eAgNnSc1d1
celé	celý	k2eAgNnSc1d1
území	území	k1gNnSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2012	#num#	k4
byly	být	k5eAaImAgFnP
na	na	k7c6
vybraných	vybraný	k2eAgFnPc6d1
regionálních	regionální	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
uvedeny	uvést	k5eAaPmNgInP
do	do	k7c2
provozu	provoz	k1gInSc2
první	první	k4xOgInPc1
cyklobusy	cyklobus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
byla	být	k5eAaImAgFnS
zaintegrována	zaintegrován	k2eAgFnSc1d1
obec	obec	k1gFnSc1
Sejřek	Sejřka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
IDS	IDS	kA
JMK	JMK	kA
rozšířil	rozšířit	k5eAaPmAgInS
do	do	k7c2
rakouského	rakouský	k2eAgNnSc2d1
města	město	k1gNnSc2
Drosendorf-Zissersdorf	Drosendorf-Zissersdorf	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
nicméně	nicméně	k8xC
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
začala	začít	k5eAaPmAgFnS
oprava	oprava	k1gFnSc1
přeshraniční	přeshraniční	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
tak	tak	k6eAd1
nebyla	být	k5eNaImAgFnS
průjezdná	průjezdný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Linka	linka	k1gFnSc1
č.	č.	k?
816	#num#	k4
začala	začít	k5eAaPmAgFnS
do	do	k7c2
Drosendorfu	Drosendorf	k1gInSc2
fakticky	fakticky	k6eAd1
poprvé	poprvé	k6eAd1
jezdit	jezdit	k5eAaImF
až	až	k9
v	v	k7c6
letní	letní	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
byl	být	k5eAaImAgMnS
IDS	IDS	kA
JMK	JMK	kA
rozšířen	rozšířit	k5eAaPmNgInS
o	o	k7c4
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
zóny	zóna	k1gFnPc4
na	na	k7c6
slovenském	slovenský	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
kterými	který	k3yRgInPc7,k3yIgInPc7,k3yQgInPc7
vede	vést	k5eAaImIp3nS
vlaková	vlakový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
S91	S91	k1gFnSc1
z	z	k7c2
Javorníku	Javorník	k1gInSc2
do	do	k7c2
Myjavy	Myjava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
dalšímu	další	k2eAgNnSc3d1
rozšíření	rozšíření	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
vlaková	vlakový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
S31	S31	k1gFnSc2
prodloužena	prodloužit	k5eAaPmNgFnS
do	do	k7c2
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
Nedvědice	Nedvědice	k1gFnSc2
přes	přes	k7c4
Bystřici	Bystřice	k1gFnSc4
nad	nad	k7c7
Pernštejnem	Pernštejn	k1gInSc7
do	do	k7c2
zastávky	zastávka	k1gFnSc2
Rovné-Divišov	Rovné-Divišov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
trase	trasa	k1gFnSc6
vznikly	vzniknout	k5eAaPmAgFnP
tři	tři	k4xCgFnPc1
nové	nový	k2eAgFnPc1d1
zóny	zóna	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MHD	MHD	kA
v	v	k7c6
Bystřici	Bystřice	k1gFnSc6
nad	nad	k7c7
Pernštejnem	Pernštejn	k1gInSc7
byla	být	k5eAaImAgFnS
do	do	k7c2
IDS	IDS	kA
JMK	JMK	kA
začleněna	začleněn	k2eAgMnSc4d1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
se	se	k3xPyFc4
IDS	IDS	kA
JMK	JMK	kA
rozšířil	rozšířit	k5eAaPmAgInS
do	do	k7c2
dalších	další	k2eAgFnPc2d1
pěti	pět	k4xCc2
obcí	obec	k1gFnPc2
Dolního	dolní	k2eAgNnSc2d1
Rakouska	Rakousko	k1gNnSc2
(	(	kIx(
<g/>
Schrattenberg	Schrattenberg	k1gMnSc1
<g/>
,	,	kIx,
Herrnbaumgarten	Herrnbaumgarten	k2eAgMnSc1d1
<g/>
,	,	kIx,
Poysdorf	Poysdorf	k1gMnSc1
<g/>
,	,	kIx,
Falkenstein	Falkenstein	k1gMnSc1
a	a	k8xC
Drasenhofen	Drasenhofen	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
vznikly	vzniknout	k5eAaPmAgInP
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
zóny	zóna	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
obsluhuje	obsluhovat	k5eAaImIp3nS
turistický	turistický	k2eAgInSc4d1
autobus	autobus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
byla	být	k5eAaImAgFnS
zavedena	zaveden	k2eAgFnSc1d1
linka	linka	k1gFnSc1
R50	R50	k1gFnSc2
v	v	k7c6
úseku	úsek	k1gInSc6
Brno	Brno	k1gNnSc1
–	–	k?
Břeclav	Břeclav	k1gFnSc1
provozovaná	provozovaný	k2eAgFnSc1d1
soukromým	soukromý	k2eAgMnSc7d1
dopravcem	dopravce	k1gMnSc7
RegioJet	RegioJeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
zasahují	zasahovat	k5eAaImIp3nP
linky	linka	k1gFnPc4
z	z	k7c2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
v	v	k7c6
několika	několik	k4yIc6
oblastech	oblast	k1gFnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
od	od	k7c2
vzniku	vznik	k1gInSc2
IDS	IDS	kA
JMK	JMK	kA
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
Velká	velký	k2eAgFnSc1d1
Bíteš	Bíteš	k1gFnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
Ivančicko	Ivančicko	k1gNnSc1
(	(	kIx(
<g/>
Mohelno	Mohelna	k1gFnSc5
<g/>
,	,	kIx,
Kralice	Kralice	k1gFnPc1
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
<g/>
…	…	k?
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
Náměšť	Náměšť	k1gFnSc1
nad	nad	k7c7
Oslavou	oslava	k1gFnSc7
a	a	k8xC
Kunštátsko	Kunštátsko	k1gNnSc1
(	(	kIx(
<g/>
Nyklovice	Nyklovice	k1gFnPc1
<g/>
,	,	kIx,
Rovečné	Rovečný	k2eAgFnPc1d1
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
Moravské	moravský	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
Hrotovice	Hrotovice	k1gFnPc1
<g/>
,	,	kIx,
Dukovany	Dukovany	k1gInPc1
<g/>
…	…	k?
</s>
<s>
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
byly	být	k5eAaImAgFnP
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
6	#num#	k4
obcí	obec	k1gFnPc2
po	po	k7c6
trase	trasa	k1gFnSc6
výrazné	výrazný	k2eAgFnSc2d1
snahy	snaha	k1gFnSc2
o	o	k7c6
začlenění	začlenění	k1gNnSc6
trasy	trasa	k1gFnSc2
Níhov	Níhov	k1gInSc4
–	–	k?
Křižanov	Křižanov	k1gInSc1
</s>
<s>
od	od	k7c2
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
byl	být	k5eAaImAgInS
začleněn	začleněn	k2eAgInSc1d1
Sejřek	Sejřek	k1gInSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
byly	být	k5eAaImAgFnP
začleněny	začleněn	k2eAgFnPc1d1
železniční	železniční	k2eAgFnPc1d1
stanice	stanice	k1gFnPc1
a	a	k8xC
zastávky	zastávka	k1gFnPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
Bystřice	Bystřice	k1gFnSc2
nad	nad	k7c7
Pernštejnem	Pernštejn	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
město	město	k1gNnSc1
se	se	k3xPyFc4
o	o	k7c6
zaintegrování	zaintegrování	k1gNnSc6
trasy	trasa	k1gFnSc2
Nedvědice	Nedvědice	k1gFnSc2
–	–	k?
Rovné-Divišov	Rovné-Divišov	k1gInSc1
snažilo	snažit	k5eAaImAgNnS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
plánovalo	plánovat	k5eAaImAgNnS
i	i	k8xC
začlenění	začlenění	k1gNnSc1
MHD	MHD	kA
Bystřice	Bystřice	k1gFnSc1
nad	nad	k7c7
Pernštejnem	Pernštejn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2015	#num#	k4
byla	být	k5eAaImAgFnS
do	do	k7c2
systému	systém	k1gInSc2
zahrnuta	zahrnut	k2eAgFnSc1d1
i	i	k8xC
MHD	MHD	kA
v	v	k7c6
Bystřici	Bystřice	k1gFnSc6
nad	nad	k7c7
Pernštejnem	Pernštejn	k1gInSc7
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tarifní	tarifní	k2eAgFnPc1d1
zóny	zóna	k1gFnPc1
a	a	k8xC
linky	linka	k1gFnPc1
</s>
<s>
IDS	IDS	kA
JMK	JMK	kA
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
většího	veliký	k2eAgNnSc2d2
množství	množství	k1gNnSc2
relativně	relativně	k6eAd1
malých	malý	k2eAgFnPc2d1
tarifních	tarifní	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
požadavku	požadavek	k1gInSc3
cestujících	cestující	k1gMnPc2
na	na	k7c4
přepravu	přeprava	k1gFnSc4
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgNnPc7d1
zapojenými	zapojený	k2eAgNnPc7d1
sídly	sídlo	k1gNnPc7
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
cestovat	cestovat	k5eAaImF
přes	přes	k7c4
centrální	centrální	k2eAgNnSc4d1
město	město	k1gNnSc4
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
celkovém	celkový	k2eAgInSc6d1
počtu	počet	k1gInSc6
721	#num#	k4
obsluhovaných	obsluhovaný	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
674	#num#	k4
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
)	)	kIx)
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
na	na	k7c4
jednu	jeden	k4xCgFnSc4
zónu	zóna	k1gFnSc4
zhruba	zhruba	k6eAd1
4	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
Brno	Brno	k1gNnSc1
je	být	k5eAaImIp3nS
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
dvou	dva	k4xCgFnPc2
zón	zóna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tarifní	tarifní	k2eAgFnPc1d1
zóny	zóna	k1gFnPc1
IDS	IDS	kA
JMK	JMK	kA
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
trojmístnými	trojmístný	k2eAgInPc7d1
čísly	číslo	k1gNnPc7
–	–	k?
samo	sám	k3xTgNnSc1
Brno	Brno	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
zóny	zóna	k1gFnSc2
100	#num#	k4
(	(	kIx(
<g/>
vnitřní	vnitřní	k2eAgFnSc2d1
<g/>
)	)	kIx)
a	a	k8xC
101	#num#	k4
(	(	kIx(
<g/>
vnější	vnější	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
okolní	okolní	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
pak	pak	k6eAd1
mají	mít	k5eAaImIp3nP
trojmístná	trojmístný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
v	v	k7c6
řadách	řada	k1gFnPc6
2	#num#	k4
<g/>
xx	xx	k?
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
xx	xx	k?
(	(	kIx(
<g/>
např.	např.	kA
Adamov	Adamov	k1gInSc4
225	#num#	k4
<g/>
,	,	kIx,
Kuřim	Kuřim	k1gInSc1
310	#num#	k4
<g/>
,	,	kIx,
Modřice	Modřice	k1gFnSc1
510	#num#	k4
<g/>
,	,	kIx,
Vyškov	Vyškov	k1gInSc1
740	#num#	k4
<g/>
,	,	kIx,
Znojmo	Znojmo	k1gNnSc1
800	#num#	k4
<g/>
,	,	kIx,
Hodonín	Hodonín	k1gInSc1
900	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
číslice	číslice	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
směr	směr	k1gInSc4
od	od	k7c2
Brna	Brno	k1gNnSc2
<g/>
:	:	kIx,
1	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2	#num#	k4
–	–	k?
sever	sever	k1gInSc1
až	až	k8xS
severovýchod	severovýchod	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
–	–	k?
sever	sever	k1gInSc1
až	až	k8xS
severozápad	severozápad	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
–	–	k?
západ	západ	k1gInSc1
až	až	k8xS
jihozápad	jihozápad	k1gInSc1
<g/>
,	,	kIx,
5	#num#	k4
–	–	k?
jih	jih	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
–	–	k?
jihovýchod	jihovýchod	k1gInSc1
<g/>
,	,	kIx,
7	#num#	k4
–	–	k?
východ	východ	k1gInSc1
<g/>
,	,	kIx,
8	#num#	k4
–	–	k?
okolí	okolí	k1gNnSc1
Znojma	Znojmo	k1gNnSc2
<g/>
,	,	kIx,
9	#num#	k4
–	–	k?
okolí	okolí	k1gNnSc6
Hodonína	Hodonín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
číslice	číslice	k1gFnSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
od	od	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
Znojma	Znojmo	k1gNnSc2
nebo	nebo	k8xC
Hodonína	Hodonín	k1gInSc2
<g/>
:	:	kIx,
0	#num#	k4
–	–	k?
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Znojmo	Znojmo	k1gNnSc1
nebo	nebo	k8xC
Hodonín	Hodonín	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
–	–	k?
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
Brnem	Brno	k1gNnSc7
<g/>
,	,	kIx,
Znojmem	Znojmo	k1gNnSc7
nebo	nebo	k8xC
Hodonínem	Hodonín	k1gInSc7
<g/>
,	,	kIx,
2	#num#	k4
–	–	k?
druhá	druhý	k4xOgFnSc1
zóna	zóna	k1gFnSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
od	od	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
Znojma	Znojmo	k1gNnSc2
nebo	nebo	k8xC
Hodonína	Hodonín	k1gInSc2
<g/>
,	,	kIx,
atd.	atd.	kA
Samostatné	samostatný	k2eAgNnSc1d1
číslování	číslování	k1gNnSc1
zón	zóna	k1gFnPc2
je	být	k5eAaImIp3nS
použito	použít	k5eAaPmNgNnS
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
kraje	kraj	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Znojma	Znojmo	k1gNnSc2
a	a	k8xC
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
kraje	kraj	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Hodonína	Hodonín	k1gInSc2
a	a	k8xC
Velké	velká	k1gFnSc2
nad	nad	k7c7
Veličkou	Velička	k1gFnSc7
z	z	k7c2
důvodu	důvod	k1gInSc2
velké	velký	k2eAgFnSc2d1
vzdálenosti	vzdálenost	k1gFnSc2
od	od	k7c2
krajského	krajský	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
číslice	číslice	k1gFnSc1
pak	pak	k6eAd1
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
to	ten	k3xDgNnSc1
třeba	třeba	k9
<g/>
)	)	kIx)
upřesňuje	upřesňovat	k5eAaImIp3nS
směr	směr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
vzdálenosti	vzdálenost	k1gFnSc6
od	od	k7c2
Brna	Brno	k1gNnSc2
(	(	kIx(
<g/>
Znojma	Znojmo	k1gNnSc2
nebo	nebo	k8xC
Hodonína	Hodonín	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
rozdělit	rozdělit	k5eAaPmF
oblast	oblast	k1gFnSc4
v	v	k7c6
přibližně	přibližně	k6eAd1
stejném	stejný	k2eAgInSc6d1
směru	směr	k1gInSc6
a	a	k8xC
vzdálenosti	vzdálenost	k1gFnPc4
do	do	k7c2
více	hodně	k6eAd2
zón	zóna	k1gFnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
jsou	být	k5eAaImIp3nP
jim	on	k3xPp3gFnPc3
přiděleny	přidělen	k2eAgInPc1d1
různé	různý	k2eAgFnPc4d1
třetí	třetí	k4xOgFnPc4
číslice	číslice	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
Ochoz	ochoz	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
210	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
sousední	sousední	k2eAgInPc1d1
Bílovice	Bílovice	k1gInPc1
nad	nad	k7c7
Svitavou	Svitava	k1gFnSc7
jsou	být	k5eAaImIp3nP
v	v	k7c6
zóně	zóna	k1gFnSc6
215	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc1
linky	linka	k1gFnPc1
zapojené	zapojený	k2eAgFnPc1d1
do	do	k7c2
IDS	IDS	kA
JMK	JMK	kA
používají	používat	k5eAaImIp3nP
jednotné	jednotný	k2eAgInPc1d1
dvou-	dvou-	k?
(	(	kIx(
<g/>
brněnské	brněnský	k2eAgFnSc2d1
linky	linka	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
tříciferné	tříciferný	k2eAgNnSc4d1
(	(	kIx(
<g/>
ostatní	ostatní	k2eAgNnSc4d1
<g/>
)	)	kIx)
označení	označení	k1gNnSc4
<g/>
,	,	kIx,
vlaky	vlak	k1gInPc4
Českých	český	k2eAgFnPc2d1
drah	draha	k1gFnPc2
jsou	být	k5eAaImIp3nP
označeny	označit	k5eAaPmNgInP
písmenem	písmeno	k1gNnSc7
S	s	k7c7
nebo	nebo	k8xC
R	R	kA
následovaným	následovaný	k2eAgNnSc7d1
jednou	jeden	k4xCgFnSc7
nebo	nebo	k8xC
dvěma	dva	k4xCgFnPc7
číslicemi	číslice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
doprava	doprava	k1gFnSc1
zajišťována	zajišťovat	k5eAaImNgFnS
na	na	k7c6
těchto	tento	k3xDgFnPc6
pravidelných	pravidelný	k2eAgFnPc6d1
linkách	linka	k1gFnPc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autobus	autobus	k1gInSc1
Irisbus	Irisbus	k1gMnSc1
Ares	Ares	k1gMnSc1
15M	15M	k4
(	(	kIx(
<g/>
firmy	firma	k1gFnSc2
VYDOS	VYDOS	kA
BUS	bus	k1gInSc1
<g/>
)	)	kIx)
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
na	na	k7c6
meziregionální	meziregionální	k2eAgFnSc6d1
lince	linka	k1gFnSc6
č.	č.	k?
107	#num#	k4
do	do	k7c2
Brna	Brno	k1gNnSc2
</s>
<s>
Digitální	digitální	k2eAgInPc1d1
informační	informační	k2eAgInPc1d1
panely	panel	k1gInPc1
na	na	k7c6
nádraží	nádraží	k1gNnSc6
v	v	k7c6
Tišnově	Tišnov	k1gInSc6
</s>
<s>
typ	typ	k1gInSc1
dopravyrozmezí	dopravyrozmeze	k1gFnPc2
čísel	číslo	k1gNnPc2
linekpočet	linekpočet	k1gInSc1
linek	linka	k1gFnPc2
</s>
<s>
vlaky	vlak	k1gInPc1
–	–	k?
dálkové	dálkový	k2eAgFnSc2d1
</s>
<s>
R	R	kA
<g/>
8	#num#	k4
<g/>
–	–	k?
<g/>
R	R	kA
<g/>
56	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
vlaky	vlak	k1gInPc1
–	–	k?
místní	místní	k2eAgInPc1d1
</s>
<s>
S	s	k7c7
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
S	s	k7c7
<g/>
91	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
vlaky	vlak	k1gInPc1
–	–	k?
turistické	turistický	k2eAgFnSc2d1
</s>
<s>
Expres	expres	k2eAgFnSc1d1
Pálava	Pálava	k1gFnSc1
<g/>
–	–	k?
<g/>
Podyjí	Podyjí	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
tramvaje	tramvaj	k1gFnPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
trolejbusy	trolejbus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
25	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
denní	denní	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
40	#num#	k4
<g/>
–	–	k?
<g/>
84	#num#	k4
<g/>
,	,	kIx,
E	E	kA
<g/>
50	#num#	k4
<g/>
–	–	k?
<g/>
E	E	kA
<g/>
76	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
školní	školní	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
š	š	k?
<g/>
85	#num#	k4
<g/>
–	–	k?
<g/>
š	š	k?
<g/>
88	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
noční	noční	k2eAgFnSc2d1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
N	N	kA
<g/>
89	#num#	k4
<g/>
–	–	k?
<g/>
N	N	kA
<g/>
99	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
zvláštních	zvláštní	k2eAgFnPc2d1
linek	linka	k1gFnPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Avion	avion	k1gInSc1
<g/>
,	,	kIx,
Olympia	Olympia	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
lodě	loď	k1gFnPc1
na	na	k7c6
Brněnské	brněnský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
</s>
<s>
bez	bez	k7c2
označení	označení	k1gNnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Adamově	Adamov	k1gInSc6
</s>
<s>
215	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Blansku	Blansko	k1gNnSc6
</s>
<s>
221	#num#	k4
<g/>
–	–	k?
<g/>
226	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Bystřici	Bystřice	k1gFnSc6
nad	nad	k7c7
Pernštejnem	Pernštejn	k1gInSc7
</s>
<s>
370	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Břeclavi	Břeclav	k1gFnSc6
</s>
<s>
561	#num#	k4
<g/>
–	–	k?
<g/>
569	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Kyjově	Kyjov	k1gInSc6
</s>
<s>
671	#num#	k4
<g/>
–	–	k?
<g/>
673	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
ve	v	k7c6
Vyškově	Vyškov	k1gInSc6
</s>
<s>
741	#num#	k4
<g/>
–	–	k?
<g/>
744	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
</s>
<s>
801	#num#	k4
<g/>
–	–	k?
<g/>
809	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
autobusy	autobus	k1gInPc1
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Hodoníně	Hodonín	k1gInSc6
</s>
<s>
901	#num#	k4
<g/>
–	–	k?
<g/>
904	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
meziregionální	meziregionální	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
</s>
<s>
104	#num#	k4
<g/>
–	–	k?
<g/>
174	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
regionální	regionální	k2eAgInPc1d1
autobusy	autobus	k1gInPc1
</s>
<s>
201	#num#	k4
<g/>
–	–	k?
<g/>
940	#num#	k4
</s>
<s>
175	#num#	k4
</s>
<s>
Do	do	k7c2
prosince	prosinec	k1gInSc2
2013	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
provozu	provoz	k1gInSc6
také	také	k9
autobusová	autobusový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
č.	č.	k?
581	#num#	k4
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
Mikulově	Mikulov	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
změnou	změna	k1gFnSc7
trasy	trasa	k1gFnSc2
regionální	regionální	k2eAgFnSc2d1
linky	linka	k1gFnSc2
č.	č.	k?
585	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Významné	významný	k2eAgInPc1d1
přestupní	přestupní	k2eAgInPc1d1
uzly	uzel	k1gInPc1
</s>
<s>
Brno	Brno	k1gNnSc1
–	–	k?
zóny	zóna	k1gFnSc2
100	#num#	k4
a	a	k8xC
101	#num#	k4
</s>
<s>
Blansko	Blansko	k1gNnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
235	#num#	k4
</s>
<s>
Boskovice	Boskovice	k1gInPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
265	#num#	k4
</s>
<s>
Břeclav	Břeclav	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
575	#num#	k4
</s>
<s>
Bučovice	Bučovice	k1gFnPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
645	#num#	k4
</s>
<s>
Čejč	Čejč	k6eAd1
v	v	k7c6
zóně	zóna	k1gFnSc6
660	#num#	k4
</s>
<s>
Hodonín	Hodonín	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
900	#num#	k4
</s>
<s>
Hustopeče	Hustopeč	k1gFnPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
545	#num#	k4
</s>
<s>
Ivančice	Ivančice	k1gFnPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
447	#num#	k4
</s>
<s>
Klobouky	Klobouky	k1gInPc1
u	u	k7c2
Brna	Brno	k1gNnSc2
v	v	k7c6
zóně	zóna	k1gFnSc6
640	#num#	k4
</s>
<s>
Kunštát	Kunštát	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
266	#num#	k4
</s>
<s>
Kuřim	Kuřim	k6eAd1
v	v	k7c6
zóně	zóna	k1gFnSc6
310	#num#	k4
</s>
<s>
Kyjov	Kyjov	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
675	#num#	k4
</s>
<s>
Letovice	Letovice	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
275	#num#	k4
</s>
<s>
Lomnice	Lomnice	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
340	#num#	k4
</s>
<s>
Lysice	Lysice	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
256	#num#	k4
</s>
<s>
Mikulov	Mikulov	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
571	#num#	k4
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
459	#num#	k4
</s>
<s>
Modřice	Modřice	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
510	#num#	k4
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
448	#num#	k4
</s>
<s>
Oslavany	Oslavany	k1gInPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
447	#num#	k4
</s>
<s>
Pohořelice	Pohořelice	k1gFnPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
530	#num#	k4
</s>
<s>
Rajhrad	Rajhrad	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
510	#num#	k4
</s>
<s>
Rousínov	Rousínov	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
730	#num#	k4
</s>
<s>
Slavkov	Slavkov	k1gInSc1
u	u	k7c2
Brna	Brno	k1gNnSc2
v	v	k7c6
zóně	zóna	k1gFnSc6
635	#num#	k4
</s>
<s>
Sokolnice	sokolnice	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
610	#num#	k4
</s>
<s>
Strážnice	Strážnice	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
925	#num#	k4
</s>
<s>
Šakvice	Šakvice	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
545	#num#	k4
</s>
<s>
Tišnov	Tišnov	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
330	#num#	k4
</s>
<s>
Valtice	Valtice	k1gFnPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
582	#num#	k4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Bíteš	Bíteš	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
440	#num#	k4
</s>
<s>
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
v	v	k7c6
zóně	zóna	k1gFnSc6
935	#num#	k4
</s>
<s>
Vyškov	Vyškov	k1gInSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
740	#num#	k4
</s>
<s>
Zaječí	zaječet	k5eAaPmIp3nS
v	v	k7c6
zóně	zóna	k1gFnSc6
555	#num#	k4
</s>
<s>
Zastávka	zastávka	k1gFnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
425	#num#	k4
</s>
<s>
Znojmo	Znojmo	k1gNnSc1
v	v	k7c6
zóně	zóna	k1gFnSc6
800	#num#	k4
</s>
<s>
Židlochovice	Židlochovice	k1gFnPc1
v	v	k7c6
zóně	zóna	k1gFnSc6
525	#num#	k4
</s>
<s>
IDS	IDS	kA
JMK	JMK	kA
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
</s>
<s>
Autobusové	autobusový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
IDS	IDS	kA
JMK	JMK	kA
zajíždí	zajíždět	k5eAaImIp3nS
také	také	k9
do	do	k7c2
rakouských	rakouský	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
obcí	obec	k1gFnPc2
Laa	Laa	k1gFnSc2
an	an	k?
der	drát	k5eAaImRp2nS
Thaya	Thaya	k1gMnSc1
<g/>
,	,	kIx,
Drosendorf-Zissersdorf	Drosendorf-Zissersdorf	k1gMnSc1
<g/>
,	,	kIx,
Schrattenberg	Schrattenberg	k1gMnSc1
<g/>
,	,	kIx,
Herrnbaumgarten	Herrnbaumgarten	k2eAgMnSc1d1
<g/>
,	,	kIx,
Poysdorf	Poysdorf	k1gMnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
části	část	k1gFnSc2
Poysbrunn	Poysbrunn	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Falkenstein	Falkenstein	k1gInSc1
a	a	k8xC
Drasehofen	Drasehofen	k1gInSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
části	část	k1gFnSc2
Stützenhofen	Stützenhofen	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
obsluhují	obsluhovat	k5eAaImIp3nP
autobusy	autobus	k1gInPc1
IDS	IDS	kA
JMK	JMK	kA
Holíč	Holíč	k1gInSc4
<g/>
,	,	kIx,
Kátov	Kátov	k1gInSc4
<g/>
,	,	kIx,
Vrádište	Vrádište	k1gFnSc4
a	a	k8xC
Skalici	Skalice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
slovenské	slovenský	k2eAgNnSc4d1
území	území	k1gNnSc4
jezdí	jezdit	k5eAaImIp3nS
také	také	k9
jedna	jeden	k4xCgFnSc1
vlaková	vlakový	k2eAgFnSc1d1
linka	linka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
přes	přes	k7c4
Vrbovce	vrbovka	k1gFnSc6
a	a	k8xC
Brestovec	Brestovec	k1gInSc1
vede	vést	k5eAaImIp3nS
do	do	k7c2
Myjavy	Myjava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zapojené	zapojený	k2eAgFnSc3d1
dopravní	dopravní	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
</s>
<s>
Přeprava	přeprava	k1gFnSc1
osob	osoba	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
IDS	IDS	kA
JMK	JMK	kA
byla	být	k5eAaImAgFnS
v	v	k7c6
etapě	etapa	k1gFnSc6
4A	4A	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
zajišťována	zajišťovat	k5eAaImNgFnS
těmito	tento	k3xDgFnPc7
společnostmi	společnost	k1gFnPc7
<g/>
:	:	kIx,
</s>
<s>
České	český	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
(	(	kIx(
<g/>
železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
(	(	kIx(
<g/>
tramvaje	tramvaj	k1gFnPc1
<g/>
,	,	kIx,
trolejbusy	trolejbus	k1gInPc1
<g/>
,	,	kIx,
autobusy	autobus	k1gInPc1
<g/>
,	,	kIx,
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
a	a	k8xC
autobusovými	autobusový	k2eAgMnPc7d1
dopravci	dopravce	k1gMnPc7
</s>
<s>
ADOSA	ADOSA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
Bítešská	Bítešský	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
r.	r.	kA
o.	o.	k?
</s>
<s>
BODOS	BODOS	kA
bus	bus	k1gInSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
BORS	BORS	kA
Břeclav	Břeclav	k1gFnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
ČAD	Čad	k1gInSc4
Blansko	Blansko	k1gNnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
ČSAD	ČSAD	kA
Tišnov	Tišnov	k1gInSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
ČSAD	ČSAD	kA
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
DOPAZ	DOPAZ	kA
<g/>
,	,	kIx,
s.	s.	k?
r.	r.	kA
o.	o.	k?
</s>
<s>
FTL	FTL	kA
-	-	kIx~
First	First	k1gMnSc1
Transport	transporta	k1gFnPc2
Lines	Lines	k1gMnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
Jarmila	Jarmila	k1gFnSc1
Brtníková	Brtníková	k1gFnSc1
-	-	kIx~
Cestovní	cestovní	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
BTC	BTC	kA
</s>
<s>
SEBUS	SEBUS	kA
<g/>
,	,	kIx,
s.	s.	k?
r.	r.	kA
o.	o.	k?
</s>
<s>
Tourbus	Tourbus	k1gMnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
VYDOS	VYDOS	kA
BUS	bus	k1gInSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
ZDAR	zdar	k1gInSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
konečné	konečný	k2eAgFnSc2d1
etapy	etapa	k1gFnSc2
6	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
na	na	k7c6
provozu	provoz	k1gInSc6
IDS	IDS	kA
JMK	JMK	kA
podílelo	podílet	k5eAaImAgNnS
21	#num#	k4
dopravců	dopravce	k1gMnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgFnPc1d1
dráhy	dráha	k1gFnPc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
(	(	kIx(
<g/>
železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
(	(	kIx(
<g/>
tramvaje	tramvaj	k1gFnPc1
<g/>
,	,	kIx,
trolejbusy	trolejbus	k1gInPc1
<g/>
,	,	kIx,
autobusy	autobus	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
a	a	k8xC
autobusoví	autobusový	k2eAgMnPc1d1
dopravci	dopravce	k1gMnPc1
</s>
<s>
ADOSA	ADOSA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
BDS-BUS	BDS-BUS	k?
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
BK	BK	kA
BUS	bus	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
BusLine	BusLinout	k5eAaPmIp3nS
a.s.	a.s.	k?
</s>
<s>
BORS	BORS	kA
Břeclav	Břeclav	k1gFnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
Břežanská	Břežanský	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
ČAD	Čad	k1gInSc4
Blansko	Blansko	k1gNnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
ČSAD	ČSAD	kA
Hodonín	Hodonín	k1gInSc1
a.s.	a.s.	k?
</s>
<s>
ČSAD	ČSAD	kA
Kyjov	Kyjov	k1gInSc1
a.s.	a.s.	k?
</s>
<s>
ČSAD	ČSAD	kA
Tišnov	Tišnov	k1gInSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
DOPAZ	DOPAZ	kA
<g/>
,	,	kIx,
s.	s.	k?
r.	r.	kA
o.	o.	k?
</s>
<s>
FTL	FTL	kA
-	-	kIx~
First	First	k1gMnSc1
Transport	transporta	k1gFnPc2
Lines	Lines	k1gMnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
ICOM	ICOM	kA
transport	transport	k1gInSc1
a.s.	a.s.	k?
</s>
<s>
SEBUS	SEBUS	kA
<g/>
,	,	kIx,
s.	s.	k?
r.	r.	kA
o.	o.	k?
</s>
<s>
Tourbus	Tourbus	k1gMnSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
TRADO-BUS	TRADO-BUS	k?
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
TREDOS	TREDOS	kA
<g/>
,	,	kIx,
spol	spol	k1gInSc1
<g/>
.	.	kIx.
s	s	k7c7
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
VYDOS	VYDOS	kA
BUS	bus	k1gInSc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
Znojemská	znojemský	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
-	-	kIx~
PSOTA	Psota	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
</s>
<s>
Telematické	Telematický	k2eAgFnPc1d1
služby	služba	k1gFnPc1
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2009	#num#	k4
byla	být	k5eAaImAgFnS
zprovozněna	zprovozněn	k2eAgFnSc1d1
služba	služba	k1gFnSc1
informací	informace	k1gFnPc2
o	o	k7c6
odjezdech	odjezd	k1gInPc6
z	z	k7c2
konkrétní	konkrétní	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
pomocí	pomocí	k7c2
WAPu	WAPus	k1gInSc2
nebo	nebo	k8xC
webu	web	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jsou	být	k5eAaImIp3nP
využity	využit	k2eAgInPc4d1
nejen	nejen	k6eAd1
jízdní	jízdní	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
aktuální	aktuální	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
skutečné	skutečný	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
vozidla	vozidlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
IDS	IDS	kA
JMK	JMK	kA
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
svém	svůj	k3xOyFgInSc6
webu	web	k1gInSc6
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
spuštěná	spuštěný	k2eAgFnSc1d1
v	v	k7c6
září	září	k1gNnSc6
2009	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Pilotně	Pilotně	k1gFnSc1
byla	být	k5eAaImAgFnS
podobná	podobný	k2eAgFnSc1d1
služba	služba	k1gFnSc1
pouze	pouze	k6eAd1
pro	pro	k7c4
služební	služební	k2eAgFnSc4d1
potřebu	potřeba	k1gFnSc4
řidičů	řidič	k1gMnPc2
střídajících	střídající	k2eAgMnPc2d1
na	na	k7c6
trati	trať	k1gFnSc6
pro	pro	k7c4
vybraných	vybraný	k2eAgFnPc2d1
38	#num#	k4
zastávek	zastávka	k1gFnPc2
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
zavedena	zavést	k5eAaPmNgFnS
u	u	k7c2
pražských	pražský	k2eAgFnPc2d1
tramvají	tramvaj	k1gFnPc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
–	–	k?
v	v	k7c4
den	den	k1gInSc4
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2007	#num#	k4
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
dne	den	k1gInSc2
otevřených	otevřený	k2eAgFnPc2d1
dveří	dveře	k1gFnPc2
byla	být	k5eAaImAgFnS
přístupná	přístupný	k2eAgFnSc1d1
i	i	k8xC
veřejnosti	veřejnost	k1gFnSc3
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
http://www.idsjmk.cz/strucne.aspx	http://www.idsjmk.cz/strucne.aspx	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
18	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Stručně	stručně	k6eAd1
o	o	k7c4
IDS	IDS	kA
JMK	JMK	kA
<g/>
↑	↑	k?
Kronika	kronika	k1gFnSc1
IDS	IDS	kA
JMK	JMK	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idsjmk	Idsjmk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Informace	informace	k1gFnPc1
k	k	k7c3
prodloužení	prodloužení	k1gNnSc3
linky	linka	k1gFnSc2
816	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kordis-jmk	Kordis-jmk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Novinky	novinka	k1gFnPc1
a	a	k8xC
změny	změna	k1gFnPc1
v	v	k7c6
dopravě	doprava	k1gFnSc6
<g/>
:	:	kIx,
Rozšíření	rozšíření	k1gNnSc1
IDS	IDS	kA
JMK	JMK	kA
na	na	k7c4
Slovensko	Slovensko	k1gNnSc4
do	do	k7c2
Myjavy	Myjava	k1gFnSc2
-	-	kIx~
linka	linka	k1gFnSc1
S91	S91	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idsjmk	Idsjmk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MAŠOVÁ	MAŠOVÁ	kA
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
července	červenec	k1gInSc2
bude	být	k5eAaImBp3nS
stačit	stačit	k5eAaBmF
Bystřickým	Bystřický	k1gMnPc3
jeden	jeden	k4xCgInSc4
lístek	lístek	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdarsky	Zdarsko	k1gNnPc7
<g/>
.	.	kIx.
<g/>
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-06-06	2014-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rozšíření	rozšíření	k1gNnSc1
IDS	IDS	kA
JMK	JMK	kA
na	na	k7c4
Bystřicko	Bystřicko	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idsjmk	Idsjmk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MAŠOVÁ	MAŠOVÁ	kA
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Bystřici	Bystřice	k1gFnSc6
se	se	k3xPyFc4
připojí	připojit	k5eAaPmIp3nP
k	k	k7c3
Brnu	Brno	k1gNnSc3
už	už	k9
od	od	k7c2
ledna	leden	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdarsky	Zdarsko	k1gNnPc7
<g/>
.	.	kIx.
<g/>
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-09-29	2014-09-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SOLAŘÍKOVÁ	SOLAŘÍKOVÁ	kA
<g/>
,	,	kIx,
Ivana	Ivana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turistbus	Turistbus	k1gInSc1
sveze	svézt	k5eAaPmIp3nS
výletníky	výletník	k1gMnPc4
po	po	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
za	za	k7c4
hranice	hranice	k1gFnPc4
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016-06-27	2016-06-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vojtěch	Vojtěch	k1gMnSc1
Říha	Říha	k1gMnSc1
<g/>
:	:	kIx,
Integrace	integrace	k1gFnSc1
veřejné	veřejný	k2eAgFnSc2d1
osobní	osobní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
v	v	k7c6
kraji	kraj	k1gInSc6
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc4
<g/>
,	,	kIx,
Dopravní	dopravní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Jana	Jan	k1gMnSc2
Pernera	Perner	k1gMnSc2
<g/>
,	,	kIx,
prezentace	prezentace	k1gFnSc2
s	s	k7c7
podporou	podpora	k1gFnSc7
vědeckovýzkumného	vědeckovýzkumný	k2eAgInSc2d1
projektu	projekt	k1gInSc2
Univerzity	univerzita	k1gFnSc2
Pardubice	Pardubice	k1gInPc4
č.	č.	k?
51030	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
SG	SG	kA
<g/>
520001	#num#	k4
(	(	kIx(
<g/>
studentská	studentský	k2eAgFnSc1d1
grantová	grantový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nedatováno	datován	k2eNgNnSc1d1
<g/>
↑	↑	k?
Brněnský	brněnský	k2eAgInSc1d1
integrovaný	integrovaný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
ukousl	ukousnout	k5eAaPmAgInS
další	další	k2eAgFnSc4d1
část	část	k1gFnSc4
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
↑	↑	k?
Jízdní	jízdní	k2eAgInPc1d1
řády	řád	k1gInPc1
IDS	IDS	kA
JMK	JMK	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idsjmk	Idsjmk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
HLOUŠEK	Hloušek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikulovskou	mikulovský	k2eAgFnSc4d1
linku	linka	k1gFnSc4
581	#num#	k4
nahradí	nahradit	k5eAaPmIp3nP
v	v	k7c6
půli	půle	k1gFnSc6
prosince	prosinec	k1gInSc2
"	"	kIx"
<g/>
valtická	valtický	k2eAgFnSc1d1
<g/>
"	"	kIx"
585	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
plus	plus	k1gInSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-11-22	2013-11-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyhledání	vyhledání	k1gNnSc2
odjezdů	odjezd	k1gInPc2
na	na	k7c4
mobile	mobile	k1gNnSc4
v	v	k7c6
reálném	reálný	k2eAgInSc6d1
čase	čas	k1gInSc6
Archivováno	archivován	k2eAgNnSc1d1
10	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
IDS	IDS	kA
JMK	JMK	kA
<g/>
↑	↑	k?
IDS	IDS	kA
JMK	JMK	kA
<g/>
:	:	kIx,
Aktuální	aktuální	k2eAgInPc1d1
odjezdy	odjezd	k1gInPc1
MHD	MHD	kA
v	v	k7c6
mobilu	mobil	k1gInSc6
<g/>
,	,	kIx,
BUSportál	BUSportál	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
9	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Šurovský	Šurovský	k1gMnSc1
<g/>
:	:	kIx,
Nový	nový	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
pro	pro	k7c4
řidiče	řidič	k1gMnPc4
tramvají	tramvaj	k1gFnPc2
<g/>
,	,	kIx,
DP	DP	kA
kontakt	kontakt	k1gInSc4
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc1
2006	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
otestuje	otestovat	k5eAaPmIp3nS
za	za	k7c7
pomocí	pomoc	k1gFnSc7
veřejnosti	veřejnost	k1gFnSc2
22	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
informační	informační	k2eAgFnSc4d1
SMS	SMS	kA
službu	služba	k1gFnSc4
Archivováno	archivovat	k5eAaBmNgNnS
31	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČUMA	Čuma	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
10	#num#	k4
let	léto	k1gNnPc2
IDS	IDS	kA
JMK	JMK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
KORDIS	KORDIS	kA
JMK	JMK	kA
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
128	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Integrovaném	integrovaný	k2eAgInSc6d1
dopravním	dopravní	k2eAgInSc6d1
systému	systém	k1gInSc6
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Integrovaném	integrovaný	k2eAgInSc6d1
dopravním	dopravní	k2eAgInSc6d1
systému	systém	k1gInSc6
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
IDS	IDS	kA
JMK	JMK	kA
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
fotogalerie	fotogalerie	k1gFnSc1
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Brně	Brno	k1gNnSc6
a	a	k8xC
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
</s>
<s>
Mapová	mapový	k2eAgFnSc1d1
aplikace	aplikace	k1gFnSc1
zobrazující	zobrazující	k2eAgFnSc4d1
aktuální	aktuální	k2eAgFnSc4d1
polohu	poloha	k1gFnSc4
vozidel	vozidlo	k1gNnPc2
IDS	IDS	kA
JMK	JMK	kA
</s>
