<s>
Integrovaný	integrovaný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
IDS	IDS	kA
JMK	JMK	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
integrovaný	integrovaný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnPc1d1
tramvaje	tramvaj	k1gFnPc1
<g/>
,	,	kIx,
trolejbusy	trolejbus	k1gInPc1
<g/>
,	,	kIx,
vlaky	vlak	k1gInPc1
<g/>
,	,	kIx,
městské	městský	k2eAgFnSc2d1
a	a	k8xC
příměstské	příměstský	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
linky	linka	k1gFnSc2
a	a	k8xC
lodní	lodní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
na	na	k7c6
Brněnské	brněnský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
<g/>
.	.	kIx.
</s>