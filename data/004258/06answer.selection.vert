<s>
Nová	nový	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
v	v	k7c6
secesním	secesní	k2eAgInSc6d1
slohu	sloh	k1gInSc6
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1901	[number]	k4
<g/>
–	–	k?
<g/>
1909	[number]	k4
podle	podle	k7c2
vítězného	vítězný	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
Josefa	Josef	k1gMnSc2
Fanty	Fanta	k1gMnSc2
na	na	k7c6
základě	základ	k1gInSc6
architektonické	architektonický	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>