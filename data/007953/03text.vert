<s>
Perestrojka	perestrojka	k1gFnSc1	perestrojka
(	(	kIx(	(
п	п	k?	п
<g/>
́	́	k?	́
<g/>
й	й	k?	й
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
reformních	reformní	k2eAgInPc2d1	reformní
kroků	krok	k1gInPc2	krok
zahájených	zahájený	k2eAgInPc2d1	zahájený
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
KSSS	KSSS	kA	KSSS
Michailem	Michail	k1gMnSc7	Michail
Gorbačovem	Gorbačov	k1gInSc7	Gorbačov
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
perestrojky	perestrojka	k1gFnSc2	perestrojka
byla	být	k5eAaImAgFnS	být
především	především	k9	především
restrukturalizace	restrukturalizace	k1gFnSc1	restrukturalizace
sovětské	sovětský	k2eAgFnSc2d1	sovětská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
reformy	reforma	k1gFnPc4	reforma
perestrojky	perestrojka	k1gFnSc2	perestrojka
navazovaly	navazovat	k5eAaImAgFnP	navazovat
také	také	k9	také
širší	široký	k2eAgFnPc1d2	širší
změny	změna	k1gFnPc1	změna
k	k	k7c3	k
demokratizaci	demokratizace	k1gFnSc3	demokratizace
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
soustředěné	soustředěný	k2eAgFnSc2d1	soustředěná
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
tzv.	tzv.	kA	tzv.
glasnosti	glasnost	k1gFnSc2	glasnost
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
otevřenosti	otevřenost	k1gFnSc2	otevřenost
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
změny	změna	k1gFnPc1	změna
ale	ale	k9	ale
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
prováděny	provádět	k5eAaImNgInP	provádět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
úloha	úloha	k1gFnSc1	úloha
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
socialistického	socialistický	k2eAgInSc2d1	socialistický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
hovořilo	hovořit	k5eAaImAgNnS	hovořit
o	o	k7c4	o
demokratizaci	demokratizace	k1gFnSc4	demokratizace
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
glasnosti	glasnost	k1gFnSc6	glasnost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nikoli	nikoli	k9	nikoli
svobodě	svoboda	k1gFnSc3	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
se	se	k3xPyFc4	se
k	k	k7c3	k
volbám	volba	k1gFnPc3	volba
mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
KSSS	KSSS	kA	KSSS
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sovětů	sovět	k1gInPc2	sovět
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
povoleny	povolen	k2eAgFnPc1d1	povolena
strany	strana	k1gFnPc1	strana
nové	nový	k2eAgFnPc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Perestrojka	perestrojka	k1gFnSc1	perestrojka
skončila	skončit	k5eAaPmAgFnS	skončit
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
nezdarem	nezdar	k1gInSc7	nezdar
a	a	k8xC	a
rozpadem	rozpad	k1gInSc7	rozpad
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
(	(	kIx(	(
<g/>
opuštění	opuštění	k1gNnSc1	opuštění
Brežněvovy	Brežněvův	k2eAgFnSc2d1	Brežněvova
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgInS	vést
její	její	k3xOp3gInSc1	její
nezdar	nezdar	k1gInSc1	nezdar
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
všech	všecek	k3xTgInPc2	všecek
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
ohledech	ohled	k1gInPc6	ohled
je	být	k5eAaImIp3nS	být
perestrojka	perestrojka	k1gFnSc1	perestrojka
podobná	podobný	k2eAgFnSc1d1	podobná
demokratizačnímu	demokratizační	k2eAgInSc3d1	demokratizační
procesu	proces	k1gInSc3	proces
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc4	ten
zase	zase	k9	zase
umožnila	umožnit	k5eAaPmAgFnS	umožnit
doba	doba	k1gFnSc1	doba
Chruščovova	Chruščovův	k2eAgNnSc2d1	Chruščovovo
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
započatá	započatý	k2eAgFnSc1d1	započatá
koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
nebyla	být	k5eNaImAgFnS	být
perestrojka	perestrojka	k1gFnSc1	perestrojka
dostatečně	dostatečně	k6eAd1	dostatečně
radikální	radikální	k2eAgFnSc1d1	radikální
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
chřadnoucí	chřadnoucí	k2eAgFnSc4d1	chřadnoucí
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Neuvěřitelná	uvěřitelný	k2eNgFnSc1d1	neuvěřitelná
zastaralost	zastaralost	k1gFnSc1	zastaralost
sovětských	sovětský	k2eAgInPc2d1	sovětský
podniků	podnik	k1gInPc2	podnik
znamenala	znamenat	k5eAaImAgNnP	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
domácí	domácí	k2eAgInPc1d1	domácí
výrobky	výrobek	k1gInPc1	výrobek
tradičních	tradiční	k2eAgFnPc2d1	tradiční
firem	firma	k1gFnPc2	firma
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
trzích	trh	k1gInPc6	trh
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
uplatnitelné	uplatnitelný	k2eAgNnSc1d1	uplatnitelné
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
omezilo	omezit	k5eAaPmAgNnS	omezit
možnost	možnost	k1gFnSc4	možnost
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
režim	režim	k1gInSc4	režim
potřebné	potřebný	k2eAgFnSc2d1	potřebná
valuty	valuta	k1gFnSc2	valuta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
problém	problém	k1gInSc4	problém
bylo	být	k5eAaImAgNnS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
řešit	řešit	k5eAaImF	řešit
a	a	k8xC	a
odpovědí	odpověď	k1gFnSc7	odpověď
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
právě	právě	k9	právě
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
připravila	připravit	k5eAaPmAgFnS	připravit
perestrojka	perestrojka	k1gFnSc1	perestrojka
<g/>
.	.	kIx.	.
</s>
<s>
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
jistých	jistý	k2eAgInPc2d1	jistý
úspěchů	úspěch	k1gInPc2	úspěch
v	v	k7c6	v
decentralizaci	decentralizace	k1gFnSc6	decentralizace
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
a	a	k8xC	a
1988	[number]	k4	1988
byly	být	k5eAaImAgFnP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
nové	nový	k2eAgInPc1d1	nový
zákony	zákon	k1gInPc1	zákon
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
redefinovaly	redefinovat	k5eAaImAgFnP	redefinovat
úlohu	úloha	k1gFnSc4	úloha
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
vymezily	vymezit	k5eAaPmAgFnP	vymezit
jejich	jejich	k3xOp3gFnSc4	jejich
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
zajistily	zajistit	k5eAaPmAgFnP	zajistit
jim	on	k3xPp3gMnPc3	on
samostatné	samostatný	k2eAgNnSc1d1	samostatné
financování	financování	k1gNnSc1	financování
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
tak	tak	k9	tak
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
nemohl	moct	k5eNaImAgMnS	moct
zachraňovat	zachraňovat	k5eAaImF	zachraňovat
ty	ten	k3xDgInPc4	ten
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vinou	vinout	k5eAaImIp3nP	vinout
vlastní	vlastní	k2eAgFnPc1d1	vlastní
neefektivity	neefektivita	k1gFnPc1	neefektivita
byly	být	k5eAaImAgFnP	být
silně	silně	k6eAd1	silně
nerentabilní	rentabilní	k2eNgFnPc1d1	nerentabilní
a	a	k8xC	a
ždímaly	ždímat	k5eAaImAgInP	ždímat
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
pokladnu	pokladna	k1gFnSc4	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
krokem	krok	k1gInSc7	krok
byl	být	k5eAaImAgInS	být
družstevní	družstevní	k2eAgInSc1d1	družstevní
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
povolil	povolit	k5eAaPmAgInS	povolit
-	-	kIx~	-
byť	byť	k8xS	byť
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
-	-	kIx~	-
soukromé	soukromý	k2eAgNnSc1d1	soukromé
podnikání	podnikání	k1gNnSc1	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nyní	nyní	k6eAd1	nyní
možné	možný	k2eAgNnSc1d1	možné
zakládat	zakládat	k5eAaImF	zakládat
i	i	k9	i
společné	společný	k2eAgInPc4d1	společný
podniky	podnik	k1gInPc4	podnik
se	s	k7c7	s
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
účastí	účast	k1gFnSc7	účast
(	(	kIx(	(
<g/>
kontrolovány	kontrolován	k2eAgFnPc1d1	kontrolována
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
ovšem	ovšem	k9	ovšem
-	-	kIx~	-
alespoň	alespoň	k9	alespoň
formálně	formálně	k6eAd1	formálně
-	-	kIx~	-
sovětskými	sovětský	k2eAgMnPc7d1	sovětský
občany	občan	k1gMnPc7	občan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
i	i	k8xC	i
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
státu	stát	k1gInSc2	stát
objevily	objevit	k5eAaPmAgInP	objevit
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
ale	ale	k8xC	ale
zůstaly	zůstat	k5eAaPmAgInP	zůstat
jiné	jiný	k2eAgInPc1d1	jiný
atributy	atribut	k1gInPc1	atribut
centrálně	centrálně	k6eAd1	centrálně
řízeného	řízený	k2eAgNnSc2d1	řízené
komunistického	komunistický	k2eAgNnSc2d1	komunistické
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
systémově	systémově	k6eAd1	systémově
změnit	změnit	k5eAaPmF	změnit
možné	možný	k2eAgNnSc1d1	možné
nebylo	být	k5eNaImAgNnS	být
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
pevně	pevně	k6eAd1	pevně
<g/>
,	,	kIx,	,
nemožnost	nemožnost	k1gFnSc1	nemožnost
volné	volný	k2eAgFnSc2d1	volná
směnitelnosti	směnitelnost	k1gFnSc2	směnitelnost
rublu	rubl	k1gInSc2	rubl
<g/>
,	,	kIx,	,
socialistická	socialistický	k2eAgFnSc1d1	socialistická
struktura	struktura	k1gFnSc1	struktura
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
neexistence	neexistence	k1gFnSc2	neexistence
<g/>
,	,	kIx,	,
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgNnSc1d1	omezené
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
soukromé	soukromý	k2eAgNnSc1d1	soukromé
a	a	k8xC	a
silný	silný	k2eAgInSc1d1	silný
státní	státní	k2eAgInSc1d1	státní
a	a	k8xC	a
družstevní	družstevní	k2eAgInSc1d1	družstevní
sektor	sektor	k1gInSc1	sektor
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračující	pokračující	k2eAgInSc1d1	pokračující
státní	státní	k2eAgInSc1d1	státní
dozor	dozor	k1gInSc1	dozor
nad	nad	k7c7	nad
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
byly	být	k5eAaImAgFnP	být
změny	změna	k1gFnPc1	změna
provedené	provedený	k2eAgFnPc1d1	provedená
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
politickou	politický	k2eAgFnSc7d1	politická
garniturou	garnitura	k1gFnSc7	garnitura
pro	pro	k7c4	pro
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
společnost	společnost	k1gFnSc4	společnost
značně	značně	k6eAd1	značně
radikální	radikální	k2eAgInPc1d1	radikální
<g/>
.	.	kIx.	.
</s>
<s>
Konzervativní	konzervativní	k2eAgNnSc1d1	konzervativní
křídlo	křídlo	k1gNnSc1	křídlo
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
však	však	k9	však
příliš	příliš	k6eAd1	příliš
spokojeno	spokojen	k2eAgNnSc1d1	spokojeno
nebylo	být	k5eNaImAgNnS	být
a	a	k8xC	a
reformní	reformní	k2eAgInPc4d1	reformní
kroky	krok	k1gInPc4	krok
chtělo	chtít	k5eAaImAgNnS	chtít
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
liberálové	liberál	k1gMnPc1	liberál
požadovali	požadovat	k5eAaImAgMnP	požadovat
jejich	jejich	k3xOp3gNnSc4	jejich
urychlení	urychlení	k1gNnSc4	urychlení
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
otevřenost	otevřenost	k1gFnSc1	otevřenost
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
diskutovat	diskutovat	k5eAaImF	diskutovat
o	o	k7c6	o
mnohých	mnohý	k2eAgInPc6d1	mnohý
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tabu	tabu	k1gNnPc2	tabu
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
kritická	kritický	k2eAgNnPc4d1	kritické
místa	místo	k1gNnPc4	místo
sovětských	sovětský	k2eAgFnPc2d1	sovětská
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
drogová	drogový	k2eAgFnSc1d1	drogová
problematika	problematika	k1gFnSc1	problematika
nebo	nebo	k8xC	nebo
národnostní	národnostní	k2eAgInPc1d1	národnostní
rozpory	rozpor	k1gInPc1	rozpor
<g/>
,	,	kIx,	,
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
systém	systém	k1gInSc4	systém
nahlodávat	nahlodávat	k5eAaImF	nahlodávat
natolik	natolik	k6eAd1	natolik
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
rozpadl	rozpadnout	k5eAaPmAgMnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Satelitní	satelitní	k2eAgInPc1d1	satelitní
státy	stát	k1gInPc1	stát
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
na	na	k7c4	na
příznivce	příznivec	k1gMnPc4	příznivec
a	a	k8xC	a
odpůrce	odpůrce	k1gMnPc4	odpůrce
perestrojky	perestrojka	k1gFnSc2	perestrojka
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
se	se	k3xPyFc4	se
komunisté	komunista	k1gMnPc1	komunista
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
o	o	k7c4	o
podobné	podobný	k2eAgFnPc4d1	podobná
reformy	reforma	k1gFnPc4	reforma
jako	jako	k8xS	jako
Gorbačov	Gorbačov	k1gInSc4	Gorbačov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
byla	být	k5eAaImAgFnS	být
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
vyčerpána	vyčerpat	k5eAaPmNgFnS	vyčerpat
dlouhodobou	dlouhodobý	k2eAgFnSc7d1	dlouhodobá
krizí	krize	k1gFnSc7	krize
a	a	k8xC	a
soubojem	souboj	k1gInSc7	souboj
s	s	k7c7	s
opozičním	opoziční	k2eAgNnSc7d1	opoziční
hnutím	hnutí	k1gNnSc7	hnutí
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
zase	zase	k9	zase
postupně	postupně	k6eAd1	postupně
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
generační	generační	k2eAgFnSc3d1	generační
obměně	obměna	k1gFnSc3	obměna
vedení	vedení	k1gNnSc2	vedení
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
souviselo	souviset	k5eAaImAgNnS	souviset
přehodnocení	přehodnocení	k1gNnSc4	přehodnocení
událostí	událost	k1gFnPc2	událost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
(	(	kIx(	(
<g/>
krvavé	krvavý	k2eAgNnSc1d1	krvavé
potlačení	potlačení	k1gNnSc1	potlačení
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
povstání	povstání	k1gNnSc2	povstání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Méně	málo	k6eAd2	málo
nadšeni	nadšen	k2eAgMnPc1d1	nadšen
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
perestrojky	perestrojka	k1gFnSc2	perestrojka
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
představitelé	představitel	k1gMnPc1	představitel
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
a	a	k8xC	a
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tyto	tento	k3xDgInPc1	tento
režimy	režim	k1gInPc1	režim
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
byly	být	k5eAaImAgFnP	být
natolik	natolik	k6eAd1	natolik
existenčně	existenčně	k6eAd1	existenčně
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nemohly	moct	k5eNaImAgFnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
přestavbu	přestavba	k1gFnSc4	přestavba
zcela	zcela	k6eAd1	zcela
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
připojily	připojit	k5eAaPmAgInP	připojit
ke	k	k7c3	k
Gorbačovovu	Gorbačovův	k2eAgNnSc3d1	Gorbačovovo
reformnímu	reformní	k2eAgNnSc3d1	reformní
úsilí	úsilí	k1gNnSc3	úsilí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
reformy	reforma	k1gFnPc1	reforma
zaváděly	zavádět	k5eAaImAgFnP	zavádět
spíše	spíše	k9	spíše
méně	málo	k6eAd2	málo
důsledně	důsledně	k6eAd1	důsledně
a	a	k8xC	a
pomaleji	pomale	k6eAd2	pomale
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Skutečný	skutečný	k2eAgInSc4d1	skutečný
odpor	odpor	k1gInSc4	odpor
pak	pak	k6eAd1	pak
Gorbačovova	Gorbačovův	k2eAgFnSc1d1	Gorbačovova
politika	politika	k1gFnSc1	politika
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
u	u	k7c2	u
konzervativních	konzervativní	k2eAgInPc2d1	konzervativní
komunistických	komunistický	k2eAgInPc2d1	komunistický
režimů	režim	k1gInPc2	režim
v	v	k7c6	v
NDR	NDR	kA	NDR
a	a	k8xC	a
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Místní	místní	k2eAgMnPc1d1	místní
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
vůdci	vůdce	k1gMnPc1	vůdce
(	(	kIx(	(
<g/>
E.	E.	kA	E.
Honecker	Honecker	k1gMnSc1	Honecker
a	a	k8xC	a
N.	N.	kA	N.
Ceausescu	Ceausescus	k1gInSc2	Ceausescus
<g/>
)	)	kIx)	)
otevřeně	otevřeně	k6eAd1	otevřeně
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
Moskvu	Moskva	k1gFnSc4	Moskva
za	za	k7c4	za
politiku	politika	k1gFnSc4	politika
přestavby	přestavba	k1gFnSc2	přestavba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
ohrožení	ohrožení	k1gNnSc4	ohrožení
stability	stabilita	k1gFnSc2	stabilita
celého	celý	k2eAgInSc2d1	celý
komunistického	komunistický	k2eAgInSc2d1	komunistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
přestavbě	přestavba	k1gFnSc3	přestavba
se	se	k3xPyFc4	se
nepřipojily	připojit	k5eNaPmAgInP	připojit
ani	ani	k8xC	ani
Kuba	Kuba	k1gFnSc1	Kuba
a	a	k8xC	a
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Specifickou	specifický	k2eAgFnSc4d1	specifická
podobu	podoba	k1gFnSc4	podoba
měla	mít	k5eAaImAgFnS	mít
přestavba	přestavba	k1gFnSc1	přestavba
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
Laosu	Laos	k1gInSc6	Laos
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
čínského	čínský	k2eAgMnSc2d1	čínský
vůdce	vůdce	k1gMnSc2	vůdce
Teng	Teng	k1gMnSc1	Teng
Siao-pchinga	Siaochinga	k1gFnSc1	Siao-pchinga
<g/>
,	,	kIx,	,
vietnamské	vietnamský	k2eAgFnPc1d1	vietnamská
reformy	reforma	k1gFnPc1	reforma
"	"	kIx"	"
<g/>
Doi	Doi	k1gFnSc1	Doi
moi	moi	k?	moi
<g/>
"	"	kIx"	"
a	a	k8xC	a
laoské	laoský	k2eAgNnSc1d1	laoské
"	"	kIx"	"
<g/>
tin	tin	k?	tin
tanakán	tanakán	k2eAgInSc4d1	tanakán
maj	maj	k?	maj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nové	nový	k2eAgNnSc1d1	nové
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
kladly	klást	k5eAaImAgFnP	klást
důraz	důraz	k1gInSc4	důraz
především	především	k6eAd1	především
na	na	k7c4	na
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
transformaci	transformace	k1gFnSc4	transformace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc6d1	kulturní
rovině	rovina	k1gFnSc6	rovina
měla	mít	k5eAaImAgFnS	mít
zůstat	zůstat	k5eAaPmF	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
absolutní	absolutní	k2eAgFnSc1d1	absolutní
moc	moc	k1gFnSc1	moc
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
nad	nad	k7c7	nad
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
transformace	transformace	k1gFnSc1	transformace
Reforma	reforma	k1gFnSc1	reforma
Tranzice	Tranzice	k1gFnSc1	Tranzice
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Perestrojka	perestrojka	k1gFnSc1	perestrojka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Perestrojka	perestrojka	k1gFnSc1	perestrojka
<g/>
,	,	kIx,	,
pořad	pořad	k1gInSc1	pořad
Historie	historie	k1gFnSc1	historie
<g/>
.	.	kIx.	.
<g/>
cs	cs	k?	cs
<g/>
,	,	kIx,	,
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
Z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
slavné	slavný	k2eAgFnSc2d1	slavná
přestavby	přestavba	k1gFnSc2	přestavba
zbyl	zbýt	k5eAaPmAgInS	zbýt
nedostatek	nedostatek	k1gInSc1	nedostatek
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
unikátních	unikátní	k2eAgFnPc2d1	unikátní
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
nahrávek	nahrávka	k1gFnPc2	nahrávka
z	z	k7c2	z
vysílání	vysílání	k1gNnSc2	vysílání
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Kol	kol	k7c2	kol
<g/>
.	.	kIx.	.
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Východ	východ	k1gInSc1	východ
(	(	kIx(	(
<g/>
vznik	vznik	k1gInSc1	vznik
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
rozpad	rozpad	k1gInSc1	rozpad
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Trento	Trento	k1gNnSc1	Trento
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Castro	Castro	k1gNnSc1	Castro
a	a	k8xC	a
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
k	k	k7c3	k
dnešku	dnešek	k1gInSc3	dnešek
<g/>
.	.	kIx.	.
</s>
<s>
Weber	Weber	k1gMnSc1	Weber
<g/>
,	,	kIx,	,
H.	H.	kA	H.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
NDR	NDR	kA	NDR
Fairbanks	Fairbanks	k1gInSc1	Fairbanks
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Číny	Čína	k1gFnSc2	Čína
Hlavatá	Hlavatá	k1gFnSc1	Hlavatá
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Ičo	Ičo	k1gFnSc1	Ičo
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Karlová	Karlová	k1gFnSc1	Karlová
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Vietnamu	Vietnam	k1gInSc2	Vietnam
Nožina	Nožin	k2eAgInSc2d1	Nožin
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Laosu	Laos	k1gInSc2	Laos
Pullmann	Pullmann	k1gInSc1	Pullmann
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
Konec	konec	k1gInSc1	konec
experimentu	experiment	k1gInSc2	experiment
<g/>
,	,	kIx,	,
Scriptorium	Scriptorium	k1gNnSc1	Scriptorium
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
prvnímu	první	k4xOgNnSc3	první
a	a	k8xC	a
poslednímu	poslední	k2eAgMnSc3d1	poslední
sovětskému	sovětský	k2eAgMnSc3d1	sovětský
prezidentovi	prezident	k1gMnSc3	prezident
impérium	impérium	k1gNnSc1	impérium
vymklo	vymknout	k5eAaPmAgNnS	vymknout
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
<g/>
;	;	kIx,	;
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
nástup	nástup	k1gInSc4	nástup
Michaila	Michail	k1gMnSc4	Michail
Gorbačova	Gorbačův	k2eAgMnSc4d1	Gorbačův
do	do	k7c2	do
Kremlu	Kreml	k1gInSc2	Kreml
a	a	k8xC	a
na	na	k7c4	na
krach	krach	k1gInSc4	krach
přestavby	přestavba	k1gFnSc2	přestavba
</s>
