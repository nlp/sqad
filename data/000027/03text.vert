<s>
Inzulin	inzulin	k1gInSc1	inzulin
(	(	kIx(	(
<g/>
též	též	k9	též
insulin	insulin	k1gInSc1	insulin
<g/>
,	,	kIx,	,
inzulín	inzulín	k1gInSc1	inzulín
<g/>
,	,	kIx,	,
z	z	k7c2	z
lat.	lat.	k?	lat.
insula	insula	k1gFnSc1	insula
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hormon	hormon	k1gInSc1	hormon
produkovaný	produkovaný	k2eAgInSc1d1	produkovaný
B	B	kA	B
buňkami	buňka	k1gFnPc7	buňka
Langerhansových	Langerhansová	k1gFnPc2	Langerhansová
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgMnSc1d1	břišní
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
hladinu	hladina	k1gFnSc4	hladina
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Inzulin	inzulin	k1gInSc1	inzulin
má	mít	k5eAaImIp3nS	mít
opačnou	opačný	k2eAgFnSc4d1	opačná
funkci	funkce	k1gFnSc4	funkce
než	než	k8xS	než
glukagon	glukagon	k1gInSc4	glukagon
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jeho	jeho	k3xOp3gMnSc1	jeho
antagonista	antagonista	k1gMnSc1	antagonista
<g/>
.	.	kIx.	.
</s>
<s>
Inzulinem	inzulin	k1gInSc7	inzulin
se	se	k3xPyFc4	se
také	také	k9	také
léčí	léčit	k5eAaImIp3nP	léčit
diabetici	diabetik	k1gMnPc1	diabetik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
vlastního	vlastní	k2eAgInSc2d1	vlastní
inzulinu	inzulin	k1gInSc2	inzulin
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
se	se	k3xPyFc4	se
podkožně	podkožně	k6eAd1	podkožně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
injekcí	injekce	k1gFnPc2	injekce
<g/>
,	,	kIx,	,
inzulinových	inzulinový	k2eAgNnPc2d1	inzulinové
per	pero	k1gNnPc2	pero
nebo	nebo	k8xC	nebo
inzulinových	inzulinový	k2eAgFnPc2d1	inzulinová
pump	pumpa	k1gFnPc2	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Inzulin	inzulin	k1gInSc4	inzulin
nelze	lze	k6eNd1	lze
podávat	podávat	k5eAaImF	podávat
ústy	ústa	k1gNnPc7	ústa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bílkovinná	bílkovinný	k2eAgFnSc1d1	bílkovinná
molekula	molekula	k1gFnSc1	molekula
inzulinu	inzulin	k1gInSc2	inzulin
se	se	k3xPyFc4	se
činností	činnost	k1gFnSc7	činnost
enzymů	enzym	k1gInPc2	enzym
v	v	k7c6	v
trávicím	trávicí	k2eAgNnSc6d1	trávicí
ústrojí	ústrojí	k1gNnSc6	ústrojí
rozštěpí	rozštěpit	k5eAaPmIp3nS	rozštěpit
na	na	k7c4	na
kratší	krátký	k2eAgInPc4d2	kratší
peptidy	peptid	k1gInPc4	peptid
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
katalytický	katalytický	k2eAgInSc1d1	katalytický
účinek	účinek	k1gInSc1	účinek
deaktivuje	deaktivovat	k5eAaImIp3nS	deaktivovat
<g/>
.	.	kIx.	.
</s>
<s>
Inzulin	inzulin	k1gInSc1	inzulin
je	být	k5eAaImIp3nS	být
makromolekula	makromolekula	k1gFnSc1	makromolekula
bílkovinné	bílkovinný	k2eAgFnSc2d1	bílkovinná
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
polypeptidických	polypeptidický	k2eAgInPc2d1	polypeptidický
řetězců	řetězec	k1gInPc2	řetězec
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
disulfidickými	disulfidický	k2eAgInPc7d1	disulfidický
můstky	můstek	k1gInPc7	můstek
a	a	k8xC	a
které	který	k3yRgInPc4	který
dohromady	dohromady	k6eAd1	dohromady
mají	mít	k5eAaImIp3nP	mít
51	[number]	k4	51
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
-	-	kIx~	-
řetězec	řetězec	k1gInSc1	řetězec
A	A	kA	A
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
21	[number]	k4	21
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
a	a	k8xC	a
řetězec	řetězec	k1gInSc1	řetězec
B	B	kA	B
30	[number]	k4	30
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
Paul	Paula	k1gFnPc2	Paula
Langerhans	Langerhansa	k1gFnPc2	Langerhansa
<g/>
,	,	kIx,	,
student	student	k1gMnSc1	student
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
Langerhansovy	Langerhansův	k2eAgInPc4d1	Langerhansův
ostrůvky	ostrůvek	k1gInPc4	ostrůvek
ve	v	k7c6	v
slinivce	slinivka	k1gFnSc6	slinivka
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
německý	německý	k2eAgMnSc1d1	německý
doktor	doktor	k1gMnSc1	doktor
Oscar	Oscar	k1gMnSc1	Oscar
Minkowski	Minkowsk	k1gFnSc2	Minkowsk
dělal	dělat	k5eAaImAgMnS	dělat
pokusy	pokus	k1gInPc4	pokus
na	na	k7c6	na
psech	pes	k1gMnPc6	pes
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
zjistil	zjistit	k5eAaPmAgInS	zjistit
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
slinivkou	slinivka	k1gFnSc7	slinivka
a	a	k8xC	a
diabetem	diabetes	k1gInSc7	diabetes
(	(	kIx(	(
<g/>
vyoperoval	vyoperovat	k5eAaPmAgMnS	vyoperovat
psovi	pes	k1gMnSc3	pes
pankreas	pankreas	k1gInSc4	pankreas
a	a	k8xC	a
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
cukr	cukr	k1gInSc4	cukr
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
doktor	doktor	k1gMnSc1	doktor
Eugene	Eugen	k1gInSc5	Eugen
Opie	Opi	k1gMnPc4	Opi
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
pokusech	pokus	k1gInPc6	pokus
přišel	přijít	k5eAaPmAgMnS	přijít
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
Langerhansovými	Langerhansův	k2eAgInPc7d1	Langerhansův
ostrůvky	ostrůvek	k1gInPc7	ostrůvek
a	a	k8xC	a
diabetem	diabetes	k1gInSc7	diabetes
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
George	Georg	k1gInSc2	Georg
Ludwig	Ludwig	k1gMnSc1	Ludwig
Zuelzer	Zuelzer	k1gMnSc1	Zuelzer
téměř	téměř	k6eAd1	téměř
vyléčil	vyléčit	k5eAaPmAgMnS	vyléčit
diabetického	diabetický	k2eAgMnSc4d1	diabetický
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
podával	podávat	k5eAaImAgMnS	podávat
extrakt	extrakt	k1gInSc4	extrakt
z	z	k7c2	z
pankreatu	pankreas	k1gInSc2	pankreas
(	(	kIx(	(
<g/>
svůj	svůj	k3xOyFgInSc4	svůj
pokus	pokus	k1gInSc4	pokus
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
Frederick	Frederick	k1gInSc1	Frederick
Banting	Banting	k1gInSc4	Banting
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
izolovat	izolovat	k5eAaBmF	izolovat
účinnou	účinný	k2eAgFnSc4d1	účinná
látku	látka	k1gFnSc4	látka
z	z	k7c2	z
pankreatické	pankreatický	k2eAgFnSc2d1	pankreatická
šťávy	šťáva	k1gFnSc2	šťáva
→	→	k?	→
Banting	Banting	k1gInSc1	Banting
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
jet	jet	k5eAaImF	jet
do	do	k7c2	do
Toronta	Toronto	k1gNnSc2	Toronto
za	za	k7c7	za
profesorem	profesor	k1gMnSc7	profesor
torontské	torontský	k2eAgFnSc2d1	Torontská
univerzity	univerzita	k1gFnSc2	univerzita
John	John	k1gMnSc1	John
James	James	k1gMnSc1	James
Richard	Richard	k1gMnSc1	Richard
Macleodem	Macleod	k1gMnSc7	Macleod
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
poskytnout	poskytnout	k5eAaPmF	poskytnout
Bantingovi	Bantingův	k2eAgMnPc1d1	Bantingův
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc3	jeho
asistentu	asistent	k1gMnSc3	asistent
(	(	kIx(	(
<g/>
studentu	student	k1gMnSc3	student
medicíny	medicína	k1gFnSc2	medicína
Charles	Charles	k1gMnSc1	Charles
Best	Best	k1gMnSc1	Best
<g/>
)	)	kIx)	)
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
laboratoře	laboratoř	k1gFnSc2	laboratoř
na	na	k7c6	na
dělání	dělání	k1gNnSc6	dělání
pokusů	pokus	k1gInPc2	pokus
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g />
.	.	kIx.	.
</s>
<s>
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Bantingovi	Bantingův	k2eAgMnPc1d1	Bantingův
a	a	k8xC	a
Bestovi	Bestův	k2eAgMnPc1d1	Bestův
extrahovat	extrahovat	k5eAaBmF	extrahovat
"	"	kIx"	"
<g/>
čistý	čistý	k2eAgInSc4d1	čistý
<g/>
"	"	kIx"	"
inzulin	inzulin	k1gInSc4	inzulin
z	z	k7c2	z
krav	kráva	k1gFnPc2	kráva
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
podvázali	podvázat	k5eAaPmAgMnP	podvázat
vývod	vývod	k1gInSc4	vývod
pankreatu	pankreas	k1gInSc2	pankreas
a	a	k8xC	a
inzulin	inzulin	k1gInSc4	inzulin
extrahovali	extrahovat	k5eAaBmAgMnP	extrahovat
-	-	kIx~	-
extrahovanou	extrahovaný	k2eAgFnSc4d1	extrahovaná
látku	látka	k1gFnSc4	látka
zprvu	zprvu	k6eAd1	zprvu
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
"	"	kIx"	"
<g/>
isletin	isletin	k2eAgInSc4d1	isletin
<g/>
"	"	kIx"	"
→	→	k?	→
tento	tento	k3xDgInSc4	tento
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
bezúplatně	bezúplatně	k6eAd1	bezúplatně
předali	předat	k5eAaPmAgMnP	předat
Torontské	torontský	k2eAgFnSc3d1	Torontská
univerzitě	univerzita	k1gFnSc3	univerzita
za	za	k7c4	za
pronájem	pronájem	k1gInSc4	pronájem
laboratoře	laboratoř	k1gFnSc2	laboratoř
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
dostal	dostat	k5eAaPmAgMnS	dostat
<g />
.	.	kIx.	.
</s>
<s>
první	první	k4xOgInPc1	první
inzulinové	inzulinový	k2eAgInPc1d1	inzulinový
vzorky	vzorek	k1gInPc1	vzorek
k	k	k7c3	k
použití	použití	k1gNnSc2	použití
bostonský	bostonský	k2eAgMnSc1d1	bostonský
diabetolog	diabetolog	k1gMnSc1	diabetolog
E.	E.	kA	E.
P.	P.	kA	P.
Joslin	Joslin	k1gInSc1	Joslin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
14	[number]	k4	14
<g/>
letému	letý	k2eAgInSc3d1	letý
chlapci	chlapec	k1gMnSc6	chlapec
jménem	jméno	k1gNnSc7	jméno
Leonard	Leonard	k1gMnSc1	Leonard
Thompson	Thompson	k1gMnSc1	Thompson
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
léčený	léčený	k2eAgMnSc1d1	léčený
diabetik	diabetik	k1gMnSc1	diabetik
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
přežil	přežít	k5eAaPmAgMnS	přežít
ještě	ještě	k6eAd1	ještě
dalších	další	k2eAgNnPc2d1	další
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
velmi	velmi	k6eAd1	velmi
stoupala	stoupat	k5eAaImAgFnS	stoupat
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
inzulinu	inzulin	k1gInSc6	inzulin
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
inzulín	inzulín	k1gInSc4	inzulín
už	už	k6eAd1	už
nestačila	stačit	k5eNaBmAgFnS	stačit
vyrábět	vyrábět	k5eAaImF	vyrábět
Torontská	torontský	k2eAgFnSc1d1	Torontská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
výroby	výroba	k1gFnSc2	výroba
ujala	ujmout	k5eAaPmAgFnS	ujmout
firma	firma	k1gFnSc1	firma
Eli	Eli	k1gMnSc2	Eli
Lilly	Lilla	k1gMnSc2	Lilla
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Indianopolis	Indianopolis	k1gFnSc2	Indianopolis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
Bantingovi	Bantingův	k2eAgMnPc1d1	Bantingův
a	a	k8xC	a
Macleodovi	Macleodův	k2eAgMnPc1d1	Macleodův
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gFnSc7	jehož
záštitou	záštita	k1gFnSc7	záštita
prováděl	provádět	k5eAaImAgInS	provádět
Banting	Banting	k1gInSc1	Banting
své	svůj	k3xOyFgInPc4	svůj
pokusy	pokus	k1gInPc4	pokus
<g/>
)	)	kIx)	)
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
John	John	k1gMnSc1	John
Jacob	Jacoba	k1gFnPc2	Jacoba
Abel	Abel	k1gInSc4	Abel
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
inzulin	inzulin	k1gInSc4	inzulin
vykrystalizoval	vykrystalizovat	k5eAaPmAgMnS	vykrystalizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
inzulín	inzulín	k1gInSc1	inzulín
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
byla	být	k5eAaImAgFnS	být
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
biochemická	biochemický	k2eAgFnSc1d1	biochemická
podstata	podstata	k1gFnSc1	podstata
inzulinu	inzulin	k1gInSc2	inzulin
Frederickem	Frederick	k1gInSc7	Frederick
Sangerem	Sanger	k1gMnSc7	Sanger
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
odměněn	odměnit	k5eAaPmNgInS	odměnit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Nejprve	nejprve	k6eAd1	nejprve
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
B	B	kA	B
buňkách	buňka	k1gFnPc6	buňka
zhotoven	zhotoven	k2eAgInSc4d1	zhotoven
proinzulin	proinzulin	k2eAgInSc4d1	proinzulin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
polypeptidických	polypeptidický	k2eAgInPc2d1	polypeptidický
řetězců	řetězec	k1gInPc2	řetězec
označovaných	označovaný	k2eAgInPc2d1	označovaný
jako	jako	k9	jako
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
řetězec	řetězec	k1gInSc1	řetězec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgFnPc1d1	spojená
C-peptidem	Ceptid	k1gInSc7	C-peptid
a	a	k8xC	a
disulfidickými	disulfidický	k2eAgInPc7d1	disulfidický
můstky	můstek	k1gInPc7	můstek
zajišťujícími	zajišťující	k2eAgInPc7d1	zajišťující
soudržnost	soudržnost	k1gFnSc4	soudržnost
makromolekuly	makromolekula	k1gFnSc2	makromolekula
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
proinzulin	proinzulin	k2eAgMnSc1d1	proinzulin
se	se	k3xPyFc4	se
kumuluje	kumulovat	k5eAaImIp3nS	kumulovat
v	v	k7c6	v
sekrečních	sekreční	k2eAgFnPc6d1	sekreční
granulích	granule	k1gFnPc6	granule
B	B	kA	B
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
inzulin	inzulin	k1gInSc1	inzulin
(	(	kIx(	(
<g/>
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
řetězec	řetězec	k1gInSc1	řetězec
<g/>
)	)	kIx)	)
a	a	k8xC	a
C-peptid	Ceptid	k1gInSc4	C-peptid
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
C-peptid	Ceptid	k1gInSc1	C-peptid
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
ve	v	k7c6	v
vyšetřeních	vyšetření	k1gNnPc6	vyšetření
jako	jako	k8xS	jako
ukazatel	ukazatel	k1gInSc1	ukazatel
endogenní	endogenní	k2eAgFnSc2d1	endogenní
syntézy	syntéza	k1gFnSc2	syntéza
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
již	již	k6eAd1	již
aktivní	aktivní	k2eAgFnSc1d1	aktivní
molekula	molekula	k1gFnSc1	molekula
inzulinu	inzulin	k1gInSc2	inzulin
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
stimulaci	stimulace	k1gFnSc4	stimulace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vyloučit	vyloučit	k5eAaPmF	vyloučit
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Sekrece	sekrece	k1gFnSc1	sekrece
insulinu	insulin	k1gInSc2	insulin
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
hodnotě	hodnota	k1gFnSc6	hodnota
glykémie	glykémie	k1gFnSc2	glykémie
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
více	hodně	k6eAd2	hodně
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
sekrece	sekrece	k1gFnSc1	sekrece
insulinu	insulin	k1gInSc2	insulin
<g/>
.	.	kIx.	.
</s>
<s>
Glukóza	glukóza	k1gFnSc1	glukóza
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
β	β	k?	β
pankreatu	pankreas	k1gInSc2	pankreas
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
metabolizována	metabolizován	k2eAgFnSc1d1	metabolizována
na	na	k7c4	na
ATP.	atp.	kA	atp.
ATP	atp	kA	atp
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
ATP-dependentní	ATPependentní	k2eAgInPc4d1	ATP-dependentní
receptory	receptor	k1gInPc4	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
receptory	receptor	k1gInPc1	receptor
zavřou	zavřít	k5eAaPmIp3nP	zavřít
draslíkové	draslíkový	k2eAgInPc4d1	draslíkový
kanály	kanál	k1gInPc4	kanál
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
depolarizaci	depolarizace	k1gFnSc3	depolarizace
<g/>
.	.	kIx.	.
</s>
<s>
Napěťově	napěťově	k6eAd1	napěťově
řízené	řízený	k2eAgInPc1d1	řízený
vápníkové	vápníkový	k2eAgInPc1d1	vápníkový
kanály	kanál	k1gInPc1	kanál
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
otevírají	otevírat	k5eAaImIp3nP	otevírat
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
proudí	proudit	k5eAaImIp3nS	proudit
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
intracelulární	intracelulární	k2eAgFnSc1d1	intracelulární
koncentrace	koncentrace	k1gFnSc1	koncentrace
vápníku	vápník	k1gInSc2	vápník
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
aktivaci	aktivace	k1gFnSc4	aktivace
Ca	ca	kA	ca
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
-dependentních	ependentní	k2eAgFnPc2d1	-dependentní
proteinkinas	proteinkinasa	k1gFnPc2	proteinkinasa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spouští	spouštět	k5eAaImIp3nP	spouštět
exocytózu	exocytóza	k1gFnSc4	exocytóza
sekrečních	sekreční	k2eAgFnPc2d1	sekreční
granul	granula	k1gFnPc2	granula
inzulinu	inzulin	k1gInSc2	inzulin
a	a	k8xC	a
otevření	otevření	k1gNnSc2	otevření
draslíkových	draslíkův	k2eAgInPc2d1	draslíkův
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
repolarizaci	repolarizace	k1gFnSc3	repolarizace
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
hodnota	hodnota	k1gFnSc1	hodnota
potenciálu	potenciál	k1gInSc2	potenciál
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
klidového	klidový	k2eAgInSc2d1	klidový
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
sekrece	sekrece	k1gFnPc1	sekrece
inzulinu	inzulin	k1gInSc2	inzulin
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
inzulinem	inzulin	k1gInSc7	inzulin
se	se	k3xPyFc4	se
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
dostává	dostávat	k5eAaImIp3nS	dostávat
i	i	k9	i
C-peptid	Ceptid	k1gInSc1	C-peptid
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
denní	denní	k2eAgFnSc1d1	denní
sekrece	sekrece	k1gFnSc1	sekrece
inzulinu	inzulin	k1gInSc2	inzulin
u	u	k7c2	u
nediabetika	nediabetik	k1gMnSc2	nediabetik
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
IU	IU	kA	IU
<g/>
.	.	kIx.	.
</s>
<s>
Stimulačními	stimulační	k2eAgMnPc7d1	stimulační
regulátory	regulátor	k1gMnPc7	regulátor
jsou	být	k5eAaImIp3nP	být
manóza	manóza	k1gFnSc1	manóza
<g/>
,	,	kIx,	,
parasympatikus	parasympatikus	k1gInSc1	parasympatikus
(	(	kIx(	(
<g/>
cholinergní	cholinergní	k2eAgFnSc1d1	cholinergní
vlákna	vlákna	k1gFnSc1	vlákna
vagu	vagus	k1gInSc2	vagus
<g/>
)	)	kIx)	)
a	a	k8xC	a
acetylcholin	acetylcholin	k1gInSc1	acetylcholin
<g/>
,	,	kIx,	,
gastrin	gastrin	k1gInSc1	gastrin
<g/>
,	,	kIx,	,
sekretin	sekretin	k1gInSc1	sekretin
<g/>
,	,	kIx,	,
GIP	GIP	kA	GIP
(	(	kIx(	(
<g/>
gastrický	gastrický	k2eAgInSc1d1	gastrický
inhibiční	inhibiční	k2eAgInSc1d1	inhibiční
peptid	peptid	k1gInSc1	peptid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cholecystokinin	cholecystokinin	k2eAgInSc4d1	cholecystokinin
<g/>
,	,	kIx,	,
ketolátky	ketolátko	k1gNnPc7	ketolátko
<g/>
,	,	kIx,	,
cirkulující	cirkulující	k2eAgFnPc1d1	cirkulující
částice	částice	k1gFnPc1	částice
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
jako	jako	k8xC	jako
volné	volný	k2eAgFnPc1d1	volná
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
arginin	arginin	k2eAgInSc1d1	arginin
a	a	k8xC	a
leucin	leucin	k1gInSc1	leucin
<g/>
)	)	kIx)	)
a	a	k8xC	a
sympatikus	sympatikus	k1gInSc4	sympatikus
<g/>
.	.	kIx.	.
</s>
<s>
Sympatikus	sympatikus	k1gInSc1	sympatikus
působí	působit	k5eAaImIp3nS	působit
stimulačně	stimulačně	k6eAd1	stimulačně
i	i	k9	i
inhibičně	inhibičně	k6eAd1	inhibičně
<g/>
.	.	kIx.	.
α	α	k?	α
<g/>
2	[number]	k4	2
<g/>
-adrenergní	drenergní	k2eAgInSc1d1	-adrenergní
receptor	receptor	k1gInSc1	receptor
snižuje	snižovat	k5eAaImIp3nS	snižovat
sekreci	sekrece	k1gFnSc4	sekrece
<g/>
,	,	kIx,	,
β	β	k?	β
<g/>
1	[number]	k4	1
a	a	k8xC	a
β	β	k?	β
<g/>
2	[number]	k4	2
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Inhibičně	inhibičně	k6eAd1	inhibičně
působí	působit	k5eAaImIp3nS	působit
i	i	k9	i
adrenalin	adrenalin	k1gInSc1	adrenalin
a	a	k8xC	a
noradrenalin	noradrenalin	k1gInSc1	noradrenalin
z	z	k7c2	z
dřeně	dřeň	k1gFnSc2	dřeň
nadledvin	nadledvina	k1gFnPc2	nadledvina
<g/>
,	,	kIx,	,
somatostatin	somatostatina	k1gFnPc2	somatostatina
z	z	k7c2	z
γ	γ	k?	γ
pankreatu	pankreas	k1gInSc2	pankreas
a	a	k8xC	a
neuropeptid	neuropeptid	k1gInSc4	neuropeptid
galanin	galanina	k1gFnPc2	galanina
<g/>
.	.	kIx.	.
</s>
<s>
Inzulin	inzulin	k1gInSc1	inzulin
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
vylučován	vylučovat	k5eAaImNgInS	vylučovat
kontinuálně	kontinuálně	k6eAd1	kontinuálně
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
příjem	příjem	k1gInSc4	příjem
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bazální	bazální	k2eAgFnSc1d1	bazální
sekrece	sekrece	k1gFnSc1	sekrece
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bazální	bazální	k2eAgFnSc1d1	bazální
sekrece	sekrece	k1gFnSc1	sekrece
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
50	[number]	k4	50
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
vyloučeného	vyloučený	k2eAgInSc2d1	vyloučený
inzulinu	inzulin	k1gInSc2	inzulin
během	během	k7c2	během
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
50	[number]	k4	50
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
spotřeby	spotřeba	k1gFnSc2	spotřeba
inzulinu	inzulin	k1gInSc2	inzulin
připadá	připadat	k5eAaImIp3nS	připadat
jídlem	jídlo	k1gNnSc7	jídlo
stimulována	stimulovat	k5eAaImNgFnS	stimulovat
sekrece	sekrece	k1gFnSc1	sekrece
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
bolusová	bolusový	k2eAgFnSc1d1	bolusová
neboli	neboli	k8xC	neboli
prandiální	prandiální	k2eAgFnSc1d1	prandiální
sekrece	sekrece	k1gFnSc1	sekrece
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgMnSc1d1	ústní
jsou	být	k5eAaImIp3nP	být
polysacharidy	polysacharid	k1gInPc1	polysacharid
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
natráveny	natrávit	k5eAaPmNgFnP	natrávit
enzymem	enzym	k1gInSc7	enzym
amylasou	amylasa	k1gFnSc7	amylasa
na	na	k7c4	na
oligosacharidy	oligosacharid	k1gInPc4	oligosacharid
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
potrava	potrava	k1gFnSc1	potrava
přes	přes	k7c4	přes
žaludek	žaludek	k1gInSc4	žaludek
do	do	k7c2	do
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sacharidy	sacharid	k1gInPc1	sacharid
štěpí	štěpit	k5eAaImIp3nP	štěpit
pomocí	pomocí	k7c2	pomocí
pankreatické	pankreatický	k2eAgFnSc2d1	pankreatická
amylázy	amyláza	k1gFnSc2	amyláza
až	až	k9	až
na	na	k7c4	na
disacharidy	disacharid	k1gInPc4	disacharid
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
specifickými	specifický	k2eAgFnPc7d1	specifická
disacharasami	disacharasa	k1gFnPc7	disacharasa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
maltasou	maltasa	k1gFnSc7	maltasa
<g/>
,	,	kIx,	,
sacharasou	sacharasa	k1gFnSc7	sacharasa
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
ve	v	k7c6	v
sliznici	sliznice	k1gFnSc6	sliznice
střevní	střevní	k2eAgInPc4d1	střevní
štěpeny	štěpen	k2eAgInPc4d1	štěpen
na	na	k7c4	na
monosacharidy	monosacharid	k1gInPc4	monosacharid
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
monosacharid	monosacharid	k1gInSc1	monosacharid
glukóza	glukóza	k1gFnSc1	glukóza
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
střevní	střevní	k2eAgFnSc7d1	střevní
stěnou	stěna	k1gFnSc7	stěna
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
podráždí	podráždit	k5eAaPmIp3nS	podráždit
regulační	regulační	k2eAgInSc1d1	regulační
systém	systém	k1gInSc1	systém
β	β	k?	β
mechanismem	mechanismus	k1gInSc7	mechanismus
negativní	negativní	k2eAgFnSc2d1	negativní
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vyloučení	vyloučení	k1gNnSc4	vyloučení
inzulinu	inzulin	k1gInSc2	inzulin
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Inzulin	inzulin	k1gInSc1	inzulin
putuje	putovat	k5eAaImIp3nS	putovat
krevním	krevní	k2eAgInSc7d1	krevní
oběhem	oběh	k1gInSc7	oběh
k	k	k7c3	k
buňkám	buňka	k1gFnPc3	buňka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
inzulinové	inzulinový	k2eAgInPc4d1	inzulinový
receptory	receptor	k1gInPc4	receptor
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Navázáním	navázání	k1gNnSc7	navázání
inzulinu	inzulin	k1gInSc2	inzulin
na	na	k7c4	na
receptor	receptor	k1gInSc4	receptor
se	se	k3xPyFc4	se
uvnitř	uvnitř	k7c2	uvnitř
buňky	buňka	k1gFnSc2	buňka
rozpoutá	rozpoutat	k5eAaPmIp3nS	rozpoutat
kaskáda	kaskáda	k1gFnSc1	kaskáda
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
glukózového	glukózový	k2eAgInSc2d1	glukózový
transportéru	transportér	k1gInSc2	transportér
4	[number]	k4	4
(	(	kIx(	(
<g/>
zkr.	zkr.	kA	zkr.
GLUT	GLUT	kA	GLUT
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
glukóza	glukóza	k1gFnSc1	glukóza
dostane	dostat	k5eAaPmIp3nS	dostat
dovnitř	dovnitř	k6eAd1	dovnitř
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
přijaté	přijatý	k2eAgFnSc2d1	přijatá
glukózy	glukóza	k1gFnSc2	glukóza
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
na	na	k7c4	na
energetický	energetický	k2eAgInSc4d1	energetický
metabolismus	metabolismus	k1gInSc4	metabolismus
a	a	k8xC	a
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
zásobní	zásobní	k2eAgInSc4d1	zásobní
cukr	cukr	k1gInSc4	cukr
-	-	kIx~	-
glykogen	glykogen	k1gInSc1	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Glykogen	glykogen	k1gInSc1	glykogen
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
ukládá	ukládat	k5eAaImIp3nS	ukládat
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
příčně	příčně	k6eAd1	příčně
pruhované	pruhovaný	k2eAgFnSc6d1	pruhovaná
svalovině	svalovina	k1gFnSc6	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
Vychytáváním	vychytávání	k1gNnSc7	vychytávání
glukózy	glukóza	k1gFnSc2	glukóza
buňkami	buňka	k1gFnPc7	buňka
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
její	její	k3xOp3gFnSc1	její
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
zákonitě	zákonitě	k6eAd1	zákonitě
inhibována	inhibován	k2eAgFnSc1d1	inhibována
produkce	produkce	k1gFnSc1	produkce
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
organismus	organismus	k1gInSc1	organismus
nemá	mít	k5eNaImIp3nS	mít
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
přísun	přísun	k1gInSc1	přísun
glukózy	glukóza	k1gFnSc2	glukóza
(	(	kIx(	(
<g/>
fyzická	fyzický	k2eAgFnSc1d1	fyzická
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
,	,	kIx,	,
hladovění	hladovění	k1gNnSc1	hladovění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přirozeně	přirozeně	k6eAd1	přirozeně
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
štěpení	štěpení	k1gNnSc3	štěpení
zásobního	zásobní	k2eAgInSc2d1	zásobní
glykogenu	glykogen	k1gInSc2	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
těžší	těžký	k2eAgFnSc6d2	těžší
hypoglykémii	hypoglykémie	k1gFnSc6	hypoglykémie
je	být	k5eAaImIp3nS	být
stimulována	stimulován	k2eAgFnSc1d1	stimulována
sekrece	sekrece	k1gFnSc1	sekrece
hormonu	hormon	k1gInSc2	hormon
glukagonu	glukagon	k1gInSc2	glukagon
-	-	kIx~	-
antagonisty	antagonista	k1gMnSc2	antagonista
inzulínu	inzulín	k1gInSc2	inzulín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
putuje	putovat	k5eAaImIp3nS	putovat
ze	z	k7c2	z
slinivky	slinivka	k1gFnSc2	slinivka
krví	krev	k1gFnSc7	krev
do	do	k7c2	do
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
a	a	k8xC	a
katalyzuje	katalyzovat	k5eAaBmIp3nS	katalyzovat
rozklad	rozklad	k1gInSc4	rozklad
makromolekuly	makromolekula	k1gFnSc2	makromolekula
glykogenu	glykogen	k1gInSc2	glykogen
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
molekuly	molekula	k1gFnPc4	molekula
glukózy	glukóza	k1gFnSc2	glukóza
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
glykogenolýza	glykogenolýza	k1gFnSc1	glykogenolýza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
rychle	rychle	k6eAd1	rychle
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
hladinu	hladina	k1gFnSc4	hladina
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Aktivita	aktivita	k1gFnSc1	aktivita
inzulínu	inzulín	k1gInSc2	inzulín
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
standardní	standardní	k2eAgInPc1d1	standardní
preparáty	preparát	k1gInPc1	preparát
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
průměrně	průměrně	k6eAd1	průměrně
25	[number]	k4	25
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c4	v
1	[number]	k4	1
mg	mg	kA	mg
krystalického	krystalický	k2eAgInSc2d1	krystalický
inzulínu	inzulín	k1gInSc2	inzulín
<g/>
.	.	kIx.	.
</s>
<s>
Aplikačními	aplikační	k2eAgFnPc7d1	aplikační
formami	forma	k1gFnPc7	forma
jsou	být	k5eAaImIp3nP	být
naředěné	naředěný	k2eAgInPc1d1	naředěný
roztoky	roztok	k1gInPc1	roztok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zaznamenáváme	zaznamenávat	k5eAaImIp1nP	zaznamenávat
výrazný	výrazný	k2eAgInSc4d1	výrazný
pokrok	pokrok	k1gInSc4	pokrok
zejména	zejména	k9	zejména
u	u	k7c2	u
aplikačních	aplikační	k2eAgFnPc2d1	aplikační
forem	forma	k1gFnPc2	forma
inzulínu	inzulín	k1gInSc2	inzulín
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
diabetiky	diabetik	k1gMnPc4	diabetik
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
předností	přednost	k1gFnSc7	přednost
těchto	tento	k3xDgInPc2	tento
nových	nový	k2eAgInPc2d1	nový
preparátů	preparát	k1gInPc2	preparát
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
a	a	k8xC	a
chráněný	chráněný	k2eAgInSc4d1	chráněný
inzulín	inzulín	k1gInSc4	inzulín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jednak	jednak	k8xC	jednak
pomaleji	pomale	k6eAd2	pomale
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
tak	tak	k6eAd1	tak
rychlé	rychlý	k2eAgFnSc3d1	rychlá
přeměně	přeměna	k1gFnSc3	přeměna
a	a	k8xC	a
inaktivaci	inaktivace	k1gFnSc3	inaktivace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
játrech	játra	k1gNnPc6	játra
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
vychytávání	vychytávání	k1gNnSc1	vychytávání
glukózy	glukóza	k1gFnSc2	glukóza
z	z	k7c2	z
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
tvorbu	tvorba	k1gFnSc4	tvorba
zásobního	zásobní	k2eAgInSc2d1	zásobní
glykogenu	glykogen	k1gInSc2	glykogen
<g/>
;	;	kIx,	;
blokuje	blokovat	k5eAaImIp3nS	blokovat
ketogenezi	ketogeneze	k1gFnSc4	ketogeneze
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vzniku	vznik	k1gInSc3	vznik
ketoacidózy	ketoacidóza	k1gFnSc2	ketoacidóza
<g/>
,	,	kIx,	,
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
rozvratu	rozvrat	k1gInSc2	rozvrat
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
GLUT	GLUT	kA	GLUT
4	[number]	k4	4
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
podporuje	podporovat	k5eAaImIp3nS	podporovat
vychytávání	vychytávání	k1gNnSc1	vychytávání
glukózy	glukóza	k1gFnSc2	glukóza
(	(	kIx(	(
<g/>
primární	primární	k2eAgFnSc1d1	primární
energie	energie	k1gFnSc1	energie
svalů	sval	k1gInPc2	sval
<g/>
)	)	kIx)	)
V	v	k7c6	v
tukové	tukový	k2eAgFnSc6d1	tuková
tkáni	tkáň	k1gFnSc6	tkáň
inzulin	inzulin	k1gInSc1	inzulin
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tvorbu	tvorba	k1gFnSc4	tvorba
tuků	tuk	k1gInPc2	tuk
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
při	při	k7c6	při
diabetu	diabetes	k1gInSc6	diabetes
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
nebo	nebo	k8xC	nebo
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
spotřebě	spotřeba	k1gFnSc6	spotřeba
inzulinu	inzulin	k1gInSc2	inzulin
tloustne	tloustnout	k5eAaImIp3nS	tloustnout
<g/>
)	)	kIx)	)
Molekula	molekula	k1gFnSc1	molekula
inzulinu	inzulin	k1gInSc2	inzulin
je	být	k5eAaImIp3nS	být
degradována	degradován	k2eAgFnSc1d1	degradována
především	především	k9	především
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
vytvoření	vytvoření	k1gNnSc6	vytvoření
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
molekuly	molekula	k1gFnSc2	molekula
je	být	k5eAaImIp3nS	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
degradačními	degradační	k2eAgInPc7d1	degradační
enzymy	enzym	k1gInPc7	enzym
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
endocytózou	endocytóza	k1gFnSc7	endocytóza
cílové	cílový	k2eAgFnSc2d1	cílová
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Syntetický	syntetický	k2eAgInSc1d1	syntetický
inzulin	inzulin	k1gInSc1	inzulin
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
čištěný	čištěný	k2eAgInSc1d1	čištěný
<g/>
,	,	kIx,	,
neutrální	neutrální	k2eAgInSc1d1	neutrální
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vázaný	vázaný	k2eAgInSc1d1	vázaný
se	s	k7c7	s
zinkem	zinek	k1gInSc7	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
látky	látka	k1gFnPc4	látka
ovlivňující	ovlivňující	k2eAgFnPc4d1	ovlivňující
délku	délka	k1gFnSc4	délka
účinku	účinek	k1gInSc2	účinek
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
,	,	kIx,	,
konzervační	konzervační	k2eAgFnPc1d1	konzervační
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
stabilizující	stabilizující	k2eAgFnPc1d1	stabilizující
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Synteticky	synteticky	k6eAd1	synteticky
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
inzulin	inzulin	k1gInSc1	inzulin
je	být	k5eAaImIp3nS	být
lék	lék	k1gInSc1	lék
pro	pro	k7c4	pro
diabetiky	diabetik	k1gMnPc4	diabetik
I.	I.	kA	I.
typu	typ	k1gInSc3	typ
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
častěji	často	k6eAd2	často
pro	pro	k7c4	pro
diabetiky	diabetik	k1gMnPc4	diabetik
II	II	kA	II
<g/>
.	.	kIx.	.
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
dostává	dostávat	k5eAaImIp3nS	dostávat
subkutánně	subkutánně	k6eAd1	subkutánně
(	(	kIx(	(
<g/>
podkožně	podkožně	k6eAd1	podkožně
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
inzulinových	inzulinový	k2eAgFnPc2d1	inzulinová
stříkaček	stříkačka	k1gFnPc2	stříkačka
<g/>
,	,	kIx,	,
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
pump	pumpa	k1gFnPc2	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
je	být	k5eAaImIp3nS	být
inhalační	inhalační	k2eAgInSc1d1	inhalační
inzulin	inzulin	k1gInSc1	inzulin
(	(	kIx(	(
<g/>
Exubera	Exubera	k1gFnSc1	Exubera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
množství	množství	k1gNnSc1	množství
inhalovaného	inhalovaný	k2eAgInSc2d1	inhalovaný
inzulinu	inzulin	k1gInSc2	inzulin
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
při	při	k7c6	při
"	"	kIx"	"
<g/>
klasickém	klasický	k2eAgNnSc6d1	klasické
<g/>
"	"	kIx"	"
podání	podání	k1gNnSc6	podání
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
především	především	k9	především
z	z	k7c2	z
vepřových	vepřový	k2eAgInPc2d1	vepřový
nebo	nebo	k8xC	nebo
hovězích	hovězí	k2eAgInPc2d1	hovězí
pankreatů	pankreas	k1gInPc2	pankreas
<g/>
.	.	kIx.	.
</s>
<s>
Vepřový	vepřový	k2eAgInSc1d1	vepřový
inzulin	inzulin	k1gInSc1	inzulin
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
aminokyselině	aminokyselina	k1gFnSc6	aminokyselina
<g/>
,	,	kIx,	,
hovězí	hovězí	k2eAgInSc1d1	hovězí
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
<g/>
,	,	kIx,	,
od	od	k7c2	od
humánního	humánní	k2eAgInSc2d1	humánní
inzulínu	inzulín	k1gInSc2	inzulín
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
minimální	minimální	k2eAgInSc4d1	minimální
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
účinnost	účinnost	k1gFnSc4	účinnost
preparátu	preparát	k1gInSc2	preparát
nebo	nebo	k8xC	nebo
jeho	on	k3xPp3gNnSc2	on
tolerování	tolerování	k1gNnSc2	tolerování
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
inzulin	inzulin	k1gInSc1	inzulin
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
především	především	k9	především
z	z	k7c2	z
jatečních	jateční	k2eAgMnPc2d1	jateční
vepřů	vepř	k1gMnPc2	vepř
a	a	k8xC	a
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
poražených	poražený	k2eAgNnPc2d1	poražené
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
vyňal	vynít	k5eAaPmAgInS	vynít
pankreas	pankreas	k1gInSc1	pankreas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
posílal	posílat	k5eAaImAgInS	posílat
do	do	k7c2	do
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
inzulinu	inzulin	k1gInSc2	inzulin
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zpracovával	zpracovávat	k5eAaImAgMnS	zpracovávat
a	a	k8xC	a
čistil	čistit	k5eAaImAgMnS	čistit
do	do	k7c2	do
konečné	konečný	k2eAgFnSc2d1	konečná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Zvířecí	zvířecí	k2eAgInSc1d1	zvířecí
inzulin	inzulin	k1gInSc1	inzulin
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
vyspělém	vyspělý	k2eAgInSc6d1	vyspělý
světě	svět	k1gInSc6	svět
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
nahrazen	nahradit	k5eAaPmNgInS	nahradit
inzulinem	inzulin	k1gInSc7	inzulin
humánním	humánní	k2eAgInSc7d1	humánní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
inzulinem	inzulin	k1gInSc7	inzulin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pomocí	pomocí	k7c2	pomocí
genového	genový	k2eAgNnSc2d1	genové
inženýrství	inženýrství	k1gNnSc2	inženýrství
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nS	lišit
od	od	k7c2	od
inzulinu	inzulin	k1gInSc2	inzulin
produkovaného	produkovaný	k2eAgInSc2d1	produkovaný
lidským	lidský	k2eAgNnSc7d1	lidské
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
ohledně	ohledně	k6eAd1	ohledně
uspořádání	uspořádání	k1gNnSc4	uspořádání
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
s	s	k7c7	s
inzulinem	inzulin	k1gInSc7	inzulin
produkovaným	produkovaný	k2eAgInSc7d1	produkovaný
lidským	lidský	k2eAgInSc7d1	lidský
pankreatem	pankreas	k1gInSc7	pankreas
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	s	k7c7	s
zkratkou	zkratka	k1gFnSc7	zkratka
HM	HM	k?	HM
inzulin	inzulin	k1gInSc4	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
také	také	k9	také
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
semisynteticky	semisynteticky	k6eAd1	semisynteticky
z	z	k7c2	z
vepřového	vepřový	k2eAgInSc2d1	vepřový
inzulinu	inzulin	k1gInSc2	inzulin
záměnou	záměna	k1gFnSc7	záměna
odlišné	odlišný	k2eAgFnSc2d1	odlišná
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
v	v	k7c6	v
humánním	humánní	k2eAgInSc6d1	humánní
inzulinu	inzulin	k1gInSc6	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
biosyntetické	biosyntetický	k2eAgFnSc3d1	biosyntetická
přípravě	příprava	k1gFnSc3	příprava
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
bakterie	bakterie	k1gFnSc2	bakterie
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
nebo	nebo	k8xC	nebo
kvasinky	kvasinka	k1gFnSc2	kvasinka
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
cerevisiae	cerevisia	k1gFnSc2	cerevisia
se	se	k3xPyFc4	se
vpraví	vpravit	k5eAaPmIp3nS	vpravit
lidský	lidský	k2eAgInSc1d1	lidský
gen	gen	k1gInSc1	gen
z	z	k7c2	z
krátkého	krátký	k2eAgNnSc2d1	krátké
ramene	rameno	k1gNnSc2	rameno
11	[number]	k4	11
<g/>
.	.	kIx.	.
chromozomu	chromozom	k1gInSc2	chromozom
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
lidského	lidský	k2eAgInSc2d1	lidský
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
či	či	k8xC	či
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
)	)	kIx)	)
izoluje	izolovat	k5eAaBmIp3nS	izolovat
čistý	čistý	k2eAgInSc1d1	čistý
humánní	humánní	k2eAgInSc1d1	humánní
inzulin	inzulin	k1gInSc1	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
fyzikálně-chemických	fyzikálněhemický	k2eAgFnPc2d1	fyzikálně-chemická
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
zpomalením	zpomalení	k1gNnSc7	zpomalení
absorpce	absorpce	k1gFnSc2	absorpce
inzulínu	inzulín	k1gInSc2	inzulín
z	z	k7c2	z
podkoží	podkoží	k1gNnSc2	podkoží
dosáhneme	dosáhnout	k5eAaPmIp1nP	dosáhnout
zpomalení	zpomalení	k1gNnSc4	zpomalení
vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
inzulinu	inzulin	k1gInSc2	inzulin
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Označujeme	označovat	k5eAaImIp1nP	označovat
je	on	k3xPp3gFnPc4	on
podle	podle	k7c2	podle
technologie	technologie	k1gFnSc2	technologie
výroby	výroba	k1gFnSc2	výroba
jako	jako	k8xS	jako
NPH	NPH	kA	NPH
(	(	kIx(	(
<g/>
Neutral	Neutral	k1gFnSc1	Neutral
Protamin	protamin	k1gInSc1	protamin
Hagedorn	Hagedorn	k1gInSc1	Hagedorn
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
depotní	depotní	k2eAgInPc4d1	depotní
inzuliny	inzulin	k1gInPc4	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
inzulinu	inzulin	k1gInSc2	inzulin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ampuli	ampule	k1gFnSc6	ampule
mléčně	mléčně	k6eAd1	mléčně
zabarvený	zabarvený	k2eAgMnSc1d1	zabarvený
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
suspenzi	suspenze	k1gFnSc6	suspenze
<g/>
,	,	kIx,	,
před	před	k7c7	před
upotřebením	upotřebení	k1gNnSc7	upotřebení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
léčivo	léčivo	k1gNnSc4	léčivo
protřepat	protřepat	k5eAaPmF	protřepat
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
krátkodobé	krátkodobý	k2eAgInPc1d1	krátkodobý
inzuliny	inzulin	k1gInPc1	inzulin
jsou	být	k5eAaImIp3nP	být
čiré	čirý	k2eAgInPc1d1	čirý
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
roztoky	roztok	k1gInPc1	roztok
bez	bez	k7c2	bez
přídavků	přídavek	k1gInPc2	přídavek
pomocných	pomocný	k2eAgFnPc2d1	pomocná
látek	látka	k1gFnPc2	látka
zpomalujících	zpomalující	k2eAgFnPc2d1	zpomalující
absorpci	absorpce	k1gFnSc4	absorpce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
typů	typ	k1gInPc2	typ
inzulinů	inzulin	k1gInPc2	inzulin
zvolili	zvolit	k5eAaPmAgMnP	zvolit
výrobci	výrobce	k1gMnPc1	výrobce
odlišné	odlišný	k2eAgFnSc2d1	odlišná
barvy	barva	k1gFnSc2	barva
sekundárních	sekundární	k2eAgInPc2d1	sekundární
obalů	obal	k1gInPc2	obal
<g/>
:	:	kIx,	:
rychle	rychle	k6eAd1	rychle
působící	působící	k2eAgInPc1d1	působící
inzulíny	inzulín	k1gInPc1	inzulín
žluté	žlutý	k2eAgInPc1d1	žlutý
<g/>
,	,	kIx,	,
NPH	NPH	kA	NPH
insulíny	insulína	k1gFnSc2	insulína
zelené	zelená	k1gFnSc2	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštně	zvláštně	k6eAd1	zvláštně
upravený	upravený	k2eAgInSc1d1	upravený
inzulin	inzulin	k1gInSc1	inzulin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
molekuly	molekula	k1gFnPc1	molekula
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
HM	HM	k?	HM
inzulinu	inzulin	k1gInSc2	inzulin
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Změnou	změna	k1gFnSc7	změna
struktury	struktura	k1gFnSc2	struktura
inzulinu	inzulin	k1gInSc2	inzulin
je	být	k5eAaImIp3nS	být
dosažena	dosáhnout	k5eAaPmNgFnS	dosáhnout
změna	změna	k1gFnSc1	změna
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
účinku	účinek	k1gInSc2	účinek
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
jsou	být	k5eAaImIp3nP	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
rychle	rychle	k6eAd1	rychle
působící	působící	k2eAgFnSc1d1	působící
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
krátkodobě	krátkodobě	k6eAd1	krátkodobě
působící	působící	k2eAgNnPc4d1	působící
analoga	analogon	k1gNnPc4	analogon
a	a	k8xC	a
dlouze	dlouho	k6eAd1	dlouho
působící	působící	k2eAgNnPc4d1	působící
analoga	analogon	k1gNnPc4	analogon
<g/>
.	.	kIx.	.
</s>
<s>
Krátkodobě	krátkodobě	k6eAd1	krátkodobě
působící	působící	k2eAgInSc1d1	působící
(	(	kIx(	(
<g/>
insulin	insulin	k1gInSc1	insulin
aspart	aspart	k1gInSc1	aspart
<g/>
,	,	kIx,	,
insulin	insulin	k1gInSc1	insulin
lispro	lispro	k6eAd1	lispro
<g/>
,	,	kIx,	,
insulin	insulin	k1gInSc1	insulin
glulisin	glulisin	k1gInSc1	glulisin
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
kratší	krátký	k2eAgFnSc4d2	kratší
dobu	doba	k1gFnSc4	doba
působení	působení	k1gNnSc2	působení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
dobu	doba	k1gFnSc4	doba
nástupu	nástup	k1gInSc2	nástup
než	než	k8xS	než
klasický	klasický	k2eAgInSc4d1	klasický
humánní	humánní	k2eAgInSc4d1	humánní
inzulin	inzulin	k1gInSc4	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
injekčně	injekčně	k6eAd1	injekčně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
do	do	k7c2	do
inzulinových	inzulinový	k2eAgFnPc2d1	inzulinová
pump	pumpa	k1gFnPc2	pumpa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podání	podání	k1gNnSc4	podání
inzulinovým	inzulinový	k2eAgNnSc7d1	inzulinové
perem	pero	k1gNnSc7	pero
je	být	k5eAaImIp3nS	být
výhodou	výhoda	k1gFnSc7	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
analoga	analogon	k1gNnSc2	analogon
inzulinu	inzulin	k1gInSc2	inzulin
lze	lze	k6eAd1	lze
podat	podat	k5eAaPmF	podat
až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jídlem	jídlo	k1gNnSc7	jídlo
nebo	nebo	k8xC	nebo
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
jídla	jídlo	k1gNnSc2	jídlo
(	(	kIx(	(
<g/>
humánní	humánní	k2eAgInSc1d1	humánní
inzulín	inzulín	k1gInSc1	inzulín
se	se	k3xPyFc4	se
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
půl	půl	k6eAd1	půl
hodiny	hodina	k1gFnPc4	hodina
před	před	k7c7	před
plánovaným	plánovaný	k2eAgNnSc7d1	plánované
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
působící	působící	k2eAgNnPc1d1	působící
analoga	analogon	k1gNnPc1	analogon
(	(	kIx(	(
<g/>
insulin	insulin	k1gInSc1	insulin
glargin	glargin	k1gInSc1	glargin
<g/>
,	,	kIx,	,
insulin	insulin	k1gInSc1	insulin
detemir	detemir	k1gInSc1	detemir
<g/>
,	,	kIx,	,
insulin	insulin	k1gInSc1	insulin
degludec	degludec	k1gMnSc1	degludec
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
výhodu	výhoda	k1gFnSc4	výhoda
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
působení	působení	k1gNnSc6	působení
(	(	kIx(	(
<g/>
až	až	k9	až
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
krátkodobého	krátkodobý	k2eAgInSc2d1	krátkodobý
peaku	peak	k1gInSc2	peak
typického	typický	k2eAgMnSc4d1	typický
pro	pro	k7c4	pro
HM	HM	k?	HM
inzuliny	inzulin	k1gInPc4	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužení	prodloužení	k1gNnSc1	prodloužení
účinku	účinek	k1gInSc2	účinek
je	být	k5eAaImIp3nS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
u	u	k7c2	u
insulinu	insulin	k1gInSc2	insulin
glarginu	glargin	k1gInSc2	glargin
po	po	k7c6	po
podkožním	podkožní	k2eAgNnSc6d1	podkožní
podání	podání	k1gNnSc6	podání
-	-	kIx~	-
změna	změna	k1gFnSc1	změna
pH	ph	kA	ph
-	-	kIx~	-
tvorbou	tvorba	k1gFnSc7	tvorba
hexamerů	hexamer	k1gMnPc2	hexamer
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
inzulín	inzulín	k1gInSc1	inzulín
postupně	postupně	k6eAd1	postupně
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
<g/>
,	,	kIx,	,
u	u	k7c2	u
insulinu	insulin	k1gInSc2	insulin
detemiru	detemir	k1gInSc2	detemir
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
kyseliny	kyselina	k1gFnSc2	kyselina
myristové	myristová	k1gFnSc2	myristová
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dlouhodobých	dlouhodobý	k2eAgNnPc2d1	dlouhodobé
analog	analogon	k1gNnPc2	analogon
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
inzulin	inzulin	k1gInSc1	inzulin
čirý	čirý	k2eAgInSc1d1	čirý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
standardní	standardní	k2eAgFnSc6d1	standardní
léčbě	léčba	k1gFnSc6	léčba
diabetu	diabetes	k1gInSc2	diabetes
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
užívají	užívat	k5eAaImIp3nP	užívat
lidské	lidský	k2eAgInPc1d1	lidský
inzuliny	inzulin	k1gInPc1	inzulin
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
Inzulin	inzulin	k1gInSc1	inzulin
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
subkutánně	subkutánně	k6eAd1	subkutánně
(	(	kIx(	(
<g/>
podkožně	podkožně	k6eAd1	podkožně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnicích	nemocnice	k1gFnPc6	nemocnice
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
inzulin	inzulin	k1gInSc1	inzulin
namíchat	namíchat	k5eAaBmF	namíchat
do	do	k7c2	do
infuze	infuze	k1gFnSc2	infuze
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
intravenózně	intravenózně	k6eAd1	intravenózně
(	(	kIx(	(
<g/>
nitrožilně	nitrožilně	k6eAd1	nitrožilně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
užívané	užívaný	k2eAgFnSc2d1	užívaná
inzulinové	inzulinový	k2eAgFnSc2d1	inzulinová
lahvičky	lahvička	k1gFnSc2	lahvička
a	a	k8xC	a
cartridge	cartridg	k1gFnSc2	cartridg
(	(	kIx(	(
<g/>
náplně	náplň	k1gFnPc1	náplň
do	do	k7c2	do
pera	pero	k1gNnSc2	pero
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
100	[number]	k4	100
IU	IU	kA	IU
(	(	kIx(	(
<g/>
100	[number]	k4	100
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
inzulinu	inzulin	k1gInSc2	inzulin
v	v	k7c6	v
1	[number]	k4	1
ml	ml	kA	ml
<g/>
,	,	kIx,	,
od	od	k7c2	od
05-2015	[number]	k4	05-2015
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
Humalog	Humaloga	k1gFnPc2	Humaloga
200	[number]	k4	200
IU	IU	kA	IU
<g/>
/	/	kIx~	/
<g/>
ml	ml	kA	ml
<g/>
,	,	kIx,	,
od	od	k7c2	od
09-2015	[number]	k4	09-2015
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
insulin	insulin	k1gInSc1	insulin
glargin	glargin	k2eAgInSc1d1	glargin
300	[number]	k4	300
IU	IU	kA	IU
<g/>
/	/	kIx~	/
<g/>
ml	ml	kA	ml
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Toujeo	Toujeo	k1gNnSc1	Toujeo
<g/>
.	.	kIx.	.
</s>
<s>
Inzulinové	inzulinový	k2eAgNnSc4d1	inzulinové
pero	pero	k1gNnSc4	pero
Inzulinová	inzulinový	k2eAgFnSc1d1	inzulinová
pumpa	pumpa	k1gFnSc1	pumpa
Inhalační	inhalační	k2eAgFnSc1d1	inhalační
inzulin	inzulin	k1gInSc4	inzulin
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
inzulín	inzulín	k1gInSc1	inzulín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Výrobci	výrobce	k1gMnPc1	výrobce
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
:	:	kIx,	:
Novo	nova	k1gFnSc5	nova
Nordisk	Nordisk	k1gInSc1	Nordisk
-	-	kIx~	-
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Actrapid	Actrapid	k1gInSc1	Actrapid
<g/>
,	,	kIx,	,
Insulatard	Insulatard	k1gMnSc1	Insulatard
<g/>
,	,	kIx,	,
Levemir	Levemir	k1gMnSc1	Levemir
(	(	kIx(	(
<g/>
detemir	detemir	k1gMnSc1	detemir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NovoRapid	NovoRapid	k1gInSc1	NovoRapid
(	(	kIx(	(
<g/>
aspart	aspart	k1gInSc1	aspart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NovoMix	NovoMix	k1gInSc1	NovoMix
30	[number]	k4	30
(	(	kIx(	(
<g/>
dvoufázový	dvoufázový	k2eAgInSc1d1	dvoufázový
inzulin	inzulin	k1gInSc1	inzulin
aspart	aspart	k1gInSc1	aspart
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tresiba	Tresiba	k1gFnSc1	Tresiba
(	(	kIx(	(
<g/>
insulin	insulin	k1gInSc1	insulin
degludec	degludec	k1gMnSc1	degludec
<g/>
)	)	kIx)	)
Eli	Eli	k1gMnSc1	Eli
Lilly	Lilla	k1gFnSc2	Lilla
-	-	kIx~	-
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Humulin	Humulin	k2eAgMnSc1d1	Humulin
R	R	kA	R
<g/>
,	,	kIx,	,
Humulin	Humulin	k2eAgMnSc1d1	Humulin
N	N	kA	N
<g/>
,	,	kIx,	,
Humalog	Humalog	k1gInSc1	Humalog
(	(	kIx(	(
<g/>
lispro	lispro	k1gNnSc1	lispro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Humalog	Humalog	k1gInSc1	Humalog
200	[number]	k4	200
(	(	kIx(	(
<g/>
koncentrovaný	koncentrovaný	k2eAgInSc1d1	koncentrovaný
inzulin	inzulin	k1gInSc1	inzulin
lispro	lispro	k6eAd1	lispro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Humalog	Humalog	k1gInSc1	Humalog
Mix	mix	k1gInSc1	mix
<g/>
25	[number]	k4	25
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
dvoufázový	dvoufázový	k2eAgInSc1d1	dvoufázový
inzulin	inzulin	k1gInSc1	inzulin
lispro	lispro	k6eAd1	lispro
-	-	kIx~	-
25	[number]	k4	25
<g/>
%	%	kIx~	%
lispro	lispro	k6eAd1	lispro
a	a	k8xC	a
75	[number]	k4	75
<g/>
%	%	kIx~	%
protaminované	protaminovaný	k2eAgNnSc1d1	protaminovaný
lispro	lispro	k1gNnSc1	lispro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Humalog	Humalog	k1gInSc1	Humalog
Mix	mix	k1gInSc1	mix
<g/>
50	[number]	k4	50
(	(	kIx(	(
<g/>
dvoufázový	dvoufázový	k2eAgInSc1d1	dvoufázový
inzulin	inzulin	k1gInSc1	inzulin
lispro	lispro	k6eAd1	lispro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Abasaglar	Abasaglar	k1gInSc1	Abasaglar
(	(	kIx(	(
<g/>
glargin	glargin	k1gInSc1	glargin
<g/>
)	)	kIx)	)
Sanofi	Sanofi	k1gNnSc1	Sanofi
-	-	kIx~	-
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Insuman	Insuman	k1gMnSc1	Insuman
Rapid	rapid	k1gInSc1	rapid
<g/>
,	,	kIx,	,
Insuman	Insuman	k1gMnSc1	Insuman
Basal	Basal	k1gMnSc1	Basal
<g/>
,	,	kIx,	,
Lantus	Lantus	k1gMnSc1	Lantus
(	(	kIx(	(
<g/>
glargin	glargin	k1gMnSc1	glargin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Apidra	Apidra	k1gFnSc1	Apidra
(	(	kIx(	(
<g/>
glulisin	glulisin	k1gInSc1	glulisin
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Inzulin	inzulin	k1gInSc1	inzulin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
