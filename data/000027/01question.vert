<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
nedostatek	nedostatek	k1gInSc4	nedostatek
vlastního	vlastní	k2eAgInSc2d1	vlastní
inzulínu	inzulín	k1gInSc2	inzulín
a	a	k8xC	a
léčí	léčit	k5eAaImIp3nS	léčit
se	se	k3xPyFc4	se
inzulinem	inzulin	k1gInSc7	inzulin
<g/>
?	?	kIx.	?
</s>
