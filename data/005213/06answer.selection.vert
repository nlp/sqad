<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
476	[number]	k4	476
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
posledního	poslední	k2eAgMnSc2d1	poslední
císaře	císař	k1gMnSc2	císař
Romula	Romulus	k1gMnSc2	Romulus
Augusta	August	k1gMnSc2	August
západořímská	západořímský	k2eAgFnSc1d1	Západořímská
říše	říše	k1gFnSc1	říše
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
antické	antický	k2eAgFnPc1d1	antická
tradice	tradice	k1gFnPc1	tradice
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
vzdělanost	vzdělanost	k1gFnSc1	vzdělanost
přetrvaly	přetrvat	k5eAaPmAgFnP	přetrvat
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
až	až	k6eAd1	až
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
