<s>
Christian	Christian	k1gMnSc1	Christian
Matthias	Matthias	k1gMnSc1	Matthias
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
Garding	Garding	k1gInSc1	Garding
<g/>
,	,	kIx,	,
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1	Šlesvicko-Holštýnsko
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Charlottenburg	Charlottenburg	k1gInSc1	Charlottenburg
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
Vratislavi	Vratislav	k1gFnSc6	Vratislav
a	a	k8xC	a
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Mommsen	Mommsen	k1gInSc1	Mommsen
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
německého	německý	k2eAgMnSc2d1	německý
protestantského	protestantský	k2eAgMnSc2d1	protestantský
kněze	kněz	k1gMnSc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1838	[number]	k4	1838
až	až	k9	až
1843	[number]	k4	1843
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
právní	právní	k2eAgFnSc4d1	právní
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
práva	právo	k1gNnSc2	právo
u	u	k7c2	u
B.	B.	kA	B.
G.	G.	kA	G.
Niebuhra	Niebuhra	k1gFnSc1	Niebuhra
a	a	k8xC	a
J.	J.	kA	J.
G.	G.	kA	G.
Droysena	Droysen	k2eAgFnSc1d1	Droysen
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Kielu	Kiel	k1gInSc6	Kiel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
studia	studio	k1gNnSc2	studio
získal	získat	k5eAaPmAgMnS	získat
stipendium	stipendium	k1gNnSc4	stipendium
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
vědecké	vědecký	k2eAgNnSc4d1	vědecké
studium	studium	k1gNnSc4	studium
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
především	především	k9	především
antickými	antický	k2eAgInPc7d1	antický
nápisy	nápis	k1gInPc7	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
jich	on	k3xPp3gInPc2	on
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1853	[number]	k4	1853
je	být	k5eAaImIp3nS	být
využil	využít	k5eAaPmAgMnS	využít
pro	pro	k7c4	pro
vydání	vydání	k1gNnSc4	vydání
souboru	soubor	k1gInSc2	soubor
latinských	latinský	k2eAgInPc2d1	latinský
nápisů	nápis	k1gInPc2	nápis
Corpus	corpus	k1gNnSc2	corpus
inscriptionum	inscriptionum	k1gNnSc1	inscriptionum
Latinarum	Latinarum	k1gInSc1	Latinarum
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
nápisů	nápis	k1gInPc2	nápis
z	z	k7c2	z
území	území	k1gNnSc2	území
celé	celý	k2eAgFnSc2d1	celá
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
odborníci	odborník	k1gMnPc1	odborník
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
každé	každý	k3xTgFnSc2	každý
epigrafické	epigrafický	k2eAgFnSc2d1	epigrafická
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsno	k1gNnPc2	Mommsno
připravil	připravit	k5eAaPmAgMnS	připravit
osm	osm	k4xCc1	osm
svazků	svazek	k1gInPc2	svazek
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
patnácti	patnáct	k4xCc2	patnáct
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
asi	asi	k9	asi
400	[number]	k4	400
000	[number]	k4	000
těchto	tento	k3xDgInPc2	tento
nápisů	nápis	k1gInPc2	nápis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
ciziny	cizina	k1gFnSc2	cizina
působil	působit	k5eAaImAgMnS	působit
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
liberální	liberální	k2eAgInPc4d1	liberální
názory	názor	k1gInPc4	názor
a	a	k8xC	a
aktivní	aktivní	k2eAgFnSc4d1	aktivní
podporu	podpora	k1gFnSc4	podpora
revoluce	revoluce	k1gFnSc2	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
(	(	kIx(	(
<g/>
stal	stát	k5eAaPmAgMnS	stát
sa	sa	k?	sa
poslancem	poslanec	k1gMnSc7	poslanec
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
psal	psát	k5eAaImAgInS	psát
články	článek	k1gInPc4	článek
do	do	k7c2	do
revolučních	revoluční	k2eAgFnPc2d1	revoluční
novin	novina	k1gFnPc2	novina
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
donucen	donucen	k2eAgInSc1d1	donucen
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
profesorské	profesorský	k2eAgNnSc4d1	profesorské
místo	místo	k1gNnSc4	místo
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
Římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
usídlil	usídlit	k5eAaPmAgMnS	usídlit
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
pruské	pruský	k2eAgFnSc2d1	pruská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
profesorem	profesor	k1gMnSc7	profesor
římských	římský	k2eAgFnPc2d1	římská
dějin	dějiny	k1gFnPc2	dějiny
na	na	k7c6	na
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1873	[number]	k4	1873
až	až	k9	až
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
poslancem	poslanec	k1gMnSc7	poslanec
německého	německý	k2eAgInSc2d1	německý
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
velice	velice	k6eAd1	velice
ostře	ostro	k6eAd1	ostro
proti	proti	k7c3	proti
českým	český	k2eAgFnPc3d1	Česká
národním	národní	k2eAgFnPc3d1	národní
a	a	k8xC	a
státoprávním	státoprávní	k2eAgFnPc3d1	státoprávní
snahám	snaha	k1gFnPc3	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
spise	spis	k1gInSc6	spis
Poslání	poslání	k1gNnSc2	poslání
Němcům	Němec	k1gMnPc3	Němec
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
obvinil	obvinit	k5eAaPmAgInS	obvinit
Čechy	Čech	k1gMnPc4	Čech
z	z	k7c2	z
rozbíjení	rozbíjení	k1gNnSc2	rozbíjení
jednoty	jednota	k1gFnSc2	jednota
německého	německý	k2eAgInSc2d1	německý
národa	národ	k1gInSc2	národ
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
přijmout	přijmout	k5eAaPmF	přijmout
logické	logický	k2eAgInPc4d1	logický
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
použity	použit	k2eAgFnPc1d1	použita
násilné	násilný	k2eAgFnPc1d1	násilná
praktiky	praktika	k1gFnPc1	praktika
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Buďte	budit	k5eAaImRp2nP	budit
tvrdými	tvrdý	k2eAgFnPc7d1	tvrdá
<g/>
.	.	kIx.	.
</s>
<s>
Rozum	rozum	k1gInSc1	rozum
lebka	lebka	k1gFnSc1	lebka
Čechů	Čech	k1gMnPc2	Čech
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ranám	rána	k1gFnPc3	rána
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
porozumí	porozumět	k5eAaPmIp3nS	porozumět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
postavil	postavit	k5eAaPmAgInS	postavit
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
spise	spis	k1gInSc6	spis
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Poláky	Polák	k1gMnPc7	Polák
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
evropských	evropský	k2eAgFnPc2d1	Evropská
kulturních	kulturní	k2eAgFnPc2d1	kulturní
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
nazval	nazvat	k5eAaBmAgMnS	nazvat
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
apoštoly	apoštol	k1gMnPc4	apoštol
barbarství	barbarství	k1gNnPc4	barbarství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výpad	výpad	k1gInSc1	výpad
proti	proti	k7c3	proti
Slovanům	Slovan	k1gMnPc3	Slovan
nezůstal	zůstat	k5eNaPmAgInS	zůstat
bez	bez	k7c2	bez
odezvy	odezva	k1gFnSc2	odezva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Sorbonně	Sorbonna	k1gFnSc6	Sorbonna
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
Mommsenovu	Mommsenův	k2eAgInSc3d1	Mommsenův
postoji	postoj	k1gInSc3	postoj
francouzský	francouzský	k2eAgMnSc1d1	francouzský
profesor	profesor	k1gMnSc1	profesor
slavistiky	slavistika	k1gFnSc2	slavistika
Ernest	Ernest	k1gMnSc1	Ernest
Denis	Denisa	k1gFnPc2	Denisa
<g/>
,	,	kIx,	,
z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
historiků	historik	k1gMnPc2	historik
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
prudký	prudký	k2eAgInSc1d1	prudký
odpor	odpor	k1gInSc1	odpor
zejména	zejména	k9	zejména
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Goll	Goll	k1gMnSc1	Goll
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
rektor	rektor	k1gMnSc1	rektor
univerzity	univerzita	k1gFnSc2	univerzita
ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
doktor	doktor	k1gMnSc1	doktor
Oswald	Oswald	k1gMnSc1	Oswald
Marian	Marian	k1gMnSc1	Marian
Balzer	Balzer	k1gMnSc1	Balzer
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
obdržel	obdržet	k5eAaPmAgInS	obdržet
Mommsen	Mommsen	k1gInSc4	Mommsen
především	především	k9	především
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc1d1	hlavní
dílo	dílo	k1gNnSc1	dílo
Římské	římský	k2eAgFnPc1d1	římská
dějiny	dějiny	k1gFnPc1	dějiny
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Römische	Römischus	k1gMnSc5	Römischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
)	)	kIx)	)
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
pouze	pouze	k6eAd1	pouze
psaním	psaní	k1gNnSc7	psaní
odborné	odborný	k2eAgFnSc2d1	odborná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
tím	ten	k3xDgNnSc7	ten
oceněn	ocenit	k5eAaPmNgInS	ocenit
přínos	přínos	k1gInSc4	přínos
Theodora	Theodor	k1gMnSc2	Theodor
Mommsena	Mommsen	k2eAgMnSc4d1	Mommsen
při	při	k7c6	při
popularizaci	popularizace	k1gFnSc6	popularizace
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Zdůvodnění	zdůvodnění	k1gNnSc1	zdůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
bylo	být	k5eAaImAgNnS	být
následující	následující	k2eAgMnSc1d1	následující
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
největšímu	veliký	k2eAgMnSc3d3	veliký
žijícímu	žijící	k2eAgMnSc3d1	žijící
mistru	mistr	k1gMnSc3	mistr
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
historiografie	historiografie	k1gFnSc2	historiografie
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zřetelem	zřetel	k1gInSc7	zřetel
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
monumentální	monumentální	k2eAgNnSc1d1	monumentální
dílo	dílo	k1gNnSc1	dílo
Římské	římský	k2eAgFnPc1d1	římská
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
udělení	udělení	k1gNnSc6	udělení
ceny	cena	k1gFnSc2	cena
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
kritické	kritický	k2eAgInPc1d1	kritický
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
nedá	dát	k5eNaPmIp3nS	dát
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
ceně	cena	k1gFnSc6	cena
za	za	k7c4	za
krásnou	krásný	k2eAgFnSc4d1	krásná
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
akademie	akademie	k1gFnSc1	akademie
však	však	k9	však
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
příslušných	příslušný	k2eAgFnPc6d1	příslušná
stanovách	stanova	k1gFnPc6	stanova
k	k	k7c3	k
Nobelově	Nobelův	k2eAgFnSc3d1	Nobelova
ceně	cena	k1gFnSc3	cena
je	být	k5eAaImIp3nS	být
určeno	určen	k2eAgNnSc1d1	určeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
sem	sem	k6eAd1	sem
spadají	spadat	k5eAaImIp3nP	spadat
také	také	k9	také
jiné	jiný	k2eAgInPc4d1	jiný
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
formou	forma	k1gFnSc7	forma
a	a	k8xC	a
pojetím	pojetí	k1gNnSc7	pojetí
mají	mít	k5eAaImIp3nP	mít
literární	literární	k2eAgFnSc4d1	literární
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
Mommsenovým	Mommsenův	k2eAgInSc7d1	Mommsenův
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
Římské	římský	k2eAgFnPc4d1	římská
dějiny	dějiny	k1gFnPc4	dějiny
(	(	kIx(	(
<g/>
Römische	Römischus	k1gMnSc5	Römischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyšly	vyjít	k5eAaPmAgInP	vyjít
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
dílech	dílo	k1gNnPc6	dílo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1854	[number]	k4	1854
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Mommsen	Mommsen	k1gInSc1	Mommsen
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
popsal	popsat	k5eAaPmAgMnS	popsat
dějiny	dějiny	k1gFnPc4	dějiny
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
Římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
po	po	k7c4	po
vládu	vláda	k1gFnSc4	vláda
Gaia	Gaius	k1gMnSc2	Gaius
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
byl	být	k5eAaImAgInS	být
Mommsen	Mommsen	k1gInSc1	Mommsen
velkým	velký	k2eAgMnSc7d1	velký
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
dějin	dějiny	k1gFnPc2	dějiny
projevil	projevit	k5eAaPmAgInS	projevit
Mommsen	Mommsen	k1gInSc1	Mommsen
vysoké	vysoká	k1gFnSc2	vysoká
literární	literární	k2eAgNnSc4d1	literární
mistrovství	mistrovství	k1gNnSc4	mistrovství
při	při	k7c6	při
současném	současný	k2eAgInSc6d1	současný
zachovaní	zachovaný	k2eAgMnPc1d1	zachovaný
vědeckého	vědecký	k2eAgInSc2d1	vědecký
charakteru	charakter	k1gInSc2	charakter
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgMnS	představit
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
vývoj	vývoj	k1gInSc4	vývoj
římského	římský	k2eAgInSc2d1	římský
národa	národ	k1gInSc2	národ
od	od	k7c2	od
malého	malý	k2eAgInSc2d1	malý
městského	městský	k2eAgInSc2d1	městský
státu	stát	k1gInSc2	stát
k	k	k7c3	k
rozsáhlé	rozsáhlý	k2eAgFnSc3d1	rozsáhlá
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
prostým	prostý	k2eAgInSc7d1	prostý
popisem	popis	k1gInSc7	popis
politického	politický	k2eAgInSc2d1	politický
<g/>
,	,	kIx,	,
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
a	a	k8xC	a
náboženského	náboženský	k2eAgInSc2d1	náboženský
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
autor	autor	k1gMnSc1	autor
za	za	k7c7	za
vším	všecek	k3xTgNnSc7	všecek
hledal	hledat	k5eAaImAgInS	hledat
nějakou	nějaký	k3yIgFnSc4	nějaký
vyšší	vysoký	k2eAgFnSc4d2	vyšší
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zastával	zastávat	k5eAaImAgMnS	zastávat
tezi	teze	k1gFnSc4	teze
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
příkladech	příklad	k1gInPc6	příklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobro	dobro	k1gNnSc1	dobro
nakonec	nakonec	k6eAd1	nakonec
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
také	také	k9	také
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
formou	forma	k1gFnSc7	forma
vlády	vláda	k1gFnSc2	vláda
v	v	k7c4	v
starověkého	starověký	k2eAgMnSc4d1	starověký
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
demokratická	demokratický	k2eAgFnSc1d1	demokratická
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
příkladem	příklad	k1gInSc7	příklad
byla	být	k5eAaImAgFnS	být
vláda	vláda	k1gFnSc1	vláda
Gaia	Gaius	k1gMnSc2	Gaius
Julia	Julius	k1gMnSc2	Julius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
popisované	popisovaný	k2eAgFnSc2d1	popisovaná
pak	pak	k6eAd1	pak
Mommsen	Mommsen	k1gInSc1	Mommsen
srovnával	srovnávat	k5eAaImAgInS	srovnávat
s	s	k7c7	s
politickým	politický	k2eAgInSc7d1	politický
vývojem	vývoj	k1gInSc7	vývoj
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
Caesarovi	Caesar	k1gMnSc3	Caesar
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
nikterak	nikterak	k6eAd1	nikterak
nezakrýval	zakrývat	k5eNaImAgMnS	zakrývat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
vlády	vláda	k1gFnSc2	vláda
silného	silný	k2eAgMnSc4d1	silný
politika	politik	k1gMnSc4	politik
a	a	k8xC	a
státníka	státník	k1gMnSc4	státník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
německé	německý	k2eAgInPc4d1	německý
státy	stát	k1gInPc4	stát
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
osobnost	osobnost	k1gFnSc4	osobnost
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
pruském	pruský	k2eAgMnSc6d1	pruský
králi	král	k1gMnSc6	král
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jehož	jehož	k3xOyRp3gFnSc7	jehož
vládou	vláda	k1gFnSc7	vláda
mělo	mít	k5eAaImAgNnS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
silné	silný	k2eAgNnSc1d1	silné
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mommsenovy	Mommsenův	k2eAgFnPc1d1	Mommsenův
Římské	římský	k2eAgFnPc1d1	římská
dějiny	dějiny	k1gFnPc1	dějiny
vychovávaly	vychovávat	k5eAaImAgFnP	vychovávat
a	a	k8xC	a
vychovávají	vychovávat	k5eAaImIp3nP	vychovávat
celé	celý	k2eAgFnPc4d1	celá
generace	generace	k1gFnPc4	generace
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
výrazně	výrazně	k6eAd1	výrazně
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
i	i	k9	i
široké	široký	k2eAgFnPc1d1	široká
vrstvy	vrstva	k1gFnPc1	vrstva
čtenářů	čtenář	k1gMnPc2	čtenář
a	a	k8xC	a
probudily	probudit	k5eAaPmAgFnP	probudit
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
starověké	starověký	k2eAgFnPc4d1	starověká
dějiny	dějiny	k1gFnPc4	dějiny
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
žádné	žádný	k3yNgNnSc4	žádný
jiné	jiný	k2eAgNnSc4d1	jiné
historické	historický	k2eAgNnSc4d1	historické
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
vycházel	vycházet	k5eAaImAgMnS	vycházet
i	i	k9	i
výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
udělování	udělování	k1gNnSc4	udělování
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Theodora	Theodor	k1gMnSc4	Theodor
Mommsena	Mommsen	k2eAgMnSc4d1	Mommsen
jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
vědce	vědec	k1gMnSc2	vědec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
psal	psát	k5eAaImAgMnS	psát
čitelně	čitelně	k6eAd1	čitelně
a	a	k8xC	a
srozumitelně	srozumitelně	k6eAd1	srozumitelně
i	i	k9	i
pro	pro	k7c4	pro
široké	široký	k2eAgFnPc4d1	široká
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
se	se	k3xPyFc4	se
k	k	k7c3	k
římským	římský	k2eAgFnPc3d1	římská
dějinám	dějiny	k1gFnPc3	dějiny
ještě	ještě	k9	ještě
vrátil	vrátit	k5eAaPmAgInS	vrátit
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydal	vydat	k5eAaPmAgInS	vydat
jejich	jejich	k3xOp3gInSc4	jejich
pátý	pátý	k4xOgInSc4	pátý
díl	díl	k1gInSc4	díl
(	(	kIx(	(
<g/>
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
nikdy	nikdy	k6eAd1	nikdy
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
)	)	kIx)	)
z	z	k7c2	z
období	období	k1gNnSc2	období
Římského	římský	k2eAgNnSc2d1	římské
císařství	císařství	k1gNnSc2	císařství
s	s	k7c7	s
názvem	název	k1gInSc7	název
Die	Die	k1gFnPc2	Die
Provinzen	Provinzna	k1gFnPc2	Provinzna
<g/>
,	,	kIx,	,
von	von	k1gInSc1	von
Caesar	Caesar	k1gMnSc1	Caesar
bis	bis	k?	bis
Diocletian	Diocletian	k1gMnSc1	Diocletian
(	(	kIx(	(
<g/>
Římské	římský	k2eAgFnSc2d1	římská
provincie	provincie	k1gFnSc2	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
již	již	k9	již
tak	tak	k6eAd1	tak
komplexní	komplexní	k2eAgNnSc1d1	komplexní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
římské	římský	k2eAgFnPc4d1	římská
provincie	provincie	k1gFnPc4	provincie
v	v	k7c6	v
období	období	k1gNnSc6	období
raného	raný	k2eAgNnSc2d1	rané
císařství	císařství	k1gNnSc2	císařství
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
jejich	jejich	k3xOp3gInSc4	jejich
politický	politický	k2eAgInSc4d1	politický
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
další	další	k2eAgInSc4d1	další
dva	dva	k4xCgInPc4	dva
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
monumentální	monumentální	k2eAgFnPc4d1	monumentální
dějiny	dějiny	k1gFnPc4	dějiny
politických	politický	k2eAgFnPc2d1	politická
institucí	instituce	k1gFnPc2	instituce
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
Římské	římský	k2eAgNnSc1d1	římské
státní	státní	k2eAgNnSc1d1	státní
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
Römisches	Römisches	k1gMnSc1	Römisches
Staatsrecht	Staatsrecht	k1gMnSc1	Staatsrecht
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnSc2d1	vydaná
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
svazích	svah	k1gInPc6	svah
v	v	k7c6	v
letech	let	k1gInPc6	let
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
a	a	k8xC	a
Římské	římský	k2eAgNnSc1d1	římské
trestní	trestní	k2eAgNnSc1d1	trestní
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
Römisches	Römisches	k1gMnSc1	Römisches
Strafrecht	Strafrecht	k1gMnSc1	Strafrecht
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
jsou	být	k5eAaImIp3nP	být
pracemi	práce	k1gFnPc7	práce
průkopnickými	průkopnický	k2eAgFnPc7d1	průkopnická
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
právní	právní	k2eAgNnPc1d1	právní
a	a	k8xC	a
historická	historický	k2eAgNnPc1d1	historické
hlediska	hledisko	k1gNnPc1	hledisko
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
pomocné	pomocný	k2eAgFnPc1d1	pomocná
vědy	věda	k1gFnPc1	věda
historické	historický	k2eAgFnPc1d1	historická
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
numismatiku	numismatika	k1gFnSc4	numismatika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
Mommsen	Mommsen	k1gInSc1	Mommsen
napsal	napsat	k5eAaBmAgInS	napsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1500	[number]	k4	1500
dalších	další	k2eAgFnPc2d1	další
studií	studie	k1gFnPc2	studie
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
např	např	kA	např
o	o	k7c4	o
archeologii	archeologie	k1gFnSc4	archeologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
vydány	vydán	k2eAgFnPc1d1	vydána
v	v	k7c6	v
osmi	osm	k4xCc6	osm
svazcích	svazek	k1gInPc6	svazek
a	a	k8xC	a
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
stránek	stránka	k1gFnPc2	stránka
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
Římanů	Říman	k1gMnPc2	Říman
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
jeho	jeho	k3xOp3gFnPc6	jeho
sférách	sféra	k1gFnPc6	sféra
-	-	kIx~	-
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgFnSc6d1	náboženská
i	i	k8xC	i
kulturní	kulturní	k2eAgFnSc6d1	kulturní
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
vyšel	vyjít	k5eAaPmAgInS	vyjít
z	z	k7c2	z
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
Mommsenova	Mommsenův	k2eAgNnSc2d1	Mommsenův
díla	dílo	k1gNnSc2	dílo
třikrát	třikrát	k6eAd1	třikrát
jen	jen	k6eAd1	jen
malý	malý	k2eAgInSc1d1	malý
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
Římských	římský	k2eAgFnPc2d1	římská
dějin	dějiny	k1gFnPc2	dějiny
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Diktátoři	diktátor	k1gMnPc1	diktátor
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Pavla	Pavel	k1gMnSc4	Pavel
Eisnera	Eisner	k1gMnSc4	Eisner
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
vydalo	vydat	k5eAaPmAgNnS	vydat
výbor	výbor	k1gInSc4	výbor
pražské	pražský	k2eAgNnSc4d1	Pražské
Nakladatelské	nakladatelský	k2eAgNnSc4d1	nakladatelské
družstvo	družstvo	k1gNnSc4	družstvo
Máje	máj	k1gInSc2	máj
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
vyšel	vyjít	k5eAaPmAgInS	vyjít
výbor	výbor	k1gInSc4	výbor
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
s	s	k7c7	s
úvodem	úvod	k1gInSc7	úvod
Augustina	Augustin	k1gMnSc2	Augustin
Nováka	Novák	k1gMnSc2	Novák
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
vydalo	vydat	k5eAaPmAgNnS	vydat
výbor	výbor	k1gInSc4	výbor
olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Votobia	Votobium	k1gNnSc2	Votobium
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k1gInSc1	Mommsen
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsna	k1gFnPc2	Mommsna
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
The	The	k1gMnSc1	The
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
Bio	Bio	k?	Bio
on	on	k3xPp3gMnSc1	on
Mommsen	Mommsen	k1gInSc4	Mommsen
A	a	k9	a
Mommsen	Mommsen	k1gInSc4	Mommsen
biography	biographa	k1gFnSc2	biographa
Theodor	Theodora	k1gFnPc2	Theodora
Mommsen	Mommsna	k1gFnPc2	Mommsna
biography	biographa	k1gFnSc2	biographa
from	from	k1gMnSc1	from
the	the	k?	the
Mommsen	Mommsen	k1gInSc1	Mommsen
family	famila	k1gFnSc2	famila
website	websit	k1gInSc5	websit
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k1gInSc1	Mommsen
History	Histor	k1gInPc1	Histor
of	of	k?	of
Rome	Rom	k1gMnSc5	Rom
Römische	Römischus	k1gMnSc5	Römischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
at	at	k?	at
German	German	k1gMnSc1	German
Project	Project	k1gMnSc1	Project
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
-	-	kIx~	-
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
http://www.britannica.com/nobel/micro/400_7.html	[url]	k5eAaPmAgMnS	http://www.britannica.com/nobel/micro/400_7.html
</s>
