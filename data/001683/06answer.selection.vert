<s>
Christian	Christian	k1gMnSc1	Christian
Matthias	Matthias	k1gMnSc1	Matthias
Theodor	Theodor	k1gMnSc1	Theodor
Mommsen	Mommsen	k2eAgMnSc1d1	Mommsen
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
Garding	Garding	k1gInSc1	Garding
<g/>
,	,	kIx,	,
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1	Šlesvicko-Holštýnsko
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Charlottenburg	Charlottenburg	k1gInSc1	Charlottenburg
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
Vratislavi	Vratislav	k1gFnSc6	Vratislav
a	a	k8xC	a
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
