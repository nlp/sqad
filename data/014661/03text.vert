<s>
Bezodtoká	bezodtoký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Oblast	oblast	k1gFnSc1
Kaspického	kaspický	k2eAgNnSc2d1
moře	moře	k1gNnSc2
je	být	k5eAaImIp3nS
příkladem	příklad	k1gInSc7
bezodtoké	bezodtoký	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Bezodtoká	bezodtoký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
území	území	k1gNnSc4
na	na	k7c6
zemském	zemský	k2eAgInSc6d1
povrchu	povrch	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
buď	buď	k8xC
není	být	k5eNaImIp3nS
odvodňováno	odvodňován	k2eAgNnSc1d1
vůbec	vůbec	k9
(	(	kIx(
<g/>
bezvodá	bezvodý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
např.	např.	kA
poušť	poušť	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
sice	sice	k8xC
je	být	k5eAaImIp3nS
odvodňováno	odvodňován	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
voda	voda	k1gFnSc1
neodtéká	odtékat	k5eNaImIp3nS
do	do	k7c2
žádného	žádný	k3yNgNnSc2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
částí	část	k1gFnPc2
světového	světový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
definovat	definovat	k5eAaBmF
i	i	k9
jako	jako	k9
území	území	k1gNnPc1
nepatřící	patřící	k2eNgNnPc1d1
k	k	k7c3
úmoří	úmoří	k1gNnSc3
žádného	žádný	k3yNgInSc2
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
charakteristika	charakteristika	k1gFnSc1
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
mezi	mezi	k7c4
bezodtoké	bezodtoký	k2eAgNnSc4d1
oblasti	oblast	k1gFnSc6
počítáno	počítat	k5eAaImNgNnS
přibližně	přibližně	k6eAd1
32	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
km²	km²	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
21,5	21,5	k4
%	%	kIx~
z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
rozlohy	rozloha	k1gFnSc2
souše	souš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházejí	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
zejména	zejména	k9
v	v	k7c6
suchých	suchý	k2eAgFnPc6d1
centrálních	centrální	k2eAgFnPc6d1
částech	část	k1gFnPc6
kontinentů	kontinent	k1gInPc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
kolem	kolem	k7c2
obratníků	obratník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3
souvislou	souvislý	k2eAgFnSc7d1
bezodtokou	bezodtoký	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
je	být	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc1d1
Eurasie	Eurasie	k1gFnSc1
<g/>
,	,	kIx,
další	další	k2eAgFnPc1d1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
na	na	k7c6
Sahaře	Sahara	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
Austrálii	Austrálie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
pouště	poušť	k1gFnSc2
Kalahari	Kalahar	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
pánvi	pánev	k1gFnSc6
a	a	k8xC
ve	v	k7c6
středních	střední	k2eAgFnPc6d1
Andách	Anda	k1gFnPc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
V	v	k7c6
jistém	jistý	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
jsou	být	k5eAaImIp3nP
bezodtoké	bezodtoký	k2eAgFnPc1d1
také	také	k6eAd1
zaledněné	zaledněný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
(	(	kIx(
<g/>
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
Antarktidy	Antarktida	k1gFnSc2
a	a	k8xC
Grónska	Grónsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nemající	mající	k2eNgInSc1d1
povrchový	povrchový	k2eAgInSc4d1
odtok	odtok	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Zdaleka	zdaleka	k6eAd1
největší	veliký	k2eAgInSc1d3
„	„	k?
<g/>
bezodtokou	bezodtoký	k2eAgFnSc7d1
<g/>
“	“	k?
řekou	řeka	k1gFnSc7
je	být	k5eAaImIp3nS
Volha	Volha	k1gFnSc1
<g/>
,	,	kIx,
odvodňující	odvodňující	k2eAgFnSc4d1
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
Ruska	Rusko	k1gNnSc2
a	a	k8xC
ústící	ústící	k2eAgInPc4d1
do	do	k7c2
Kaspického	kaspický	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
toky	toka	k1gFnPc1
bezodtokých	bezodtoký	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
jsou	být	k5eAaImIp3nP
Ural	Ural	k1gInSc1
<g/>
,	,	kIx,
Těrek	těrka	k1gFnPc2
<g/>
,	,	kIx,
Kura	kura	k1gFnSc1
<g/>
,	,	kIx,
Emba	Emba	k1gFnSc1
(	(	kIx(
<g/>
vše	všechen	k3xTgNnSc1
též	též	k9
do	do	k7c2
Kaspického	kaspický	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Amudarja	Amudarja	k1gFnSc1
<g/>
,	,	kIx,
Syrdarja	Syrdarja	k1gFnSc1
(	(	kIx(
<g/>
obě	dva	k4xCgFnPc1
do	do	k7c2
Aralského	aralský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tarim	Tarim	k1gMnSc1
(	(	kIx(
<g/>
do	do	k7c2
jezera	jezero	k1gNnSc2
Lobnor	Lobnora	k1gFnPc2
nebo	nebo	k8xC
Karakošun	Karakošuna	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
Okavango	Okavango	k1gMnSc1
(	(	kIx(
<g/>
do	do	k7c2
stejnojmenných	stejnojmenný	k2eAgFnPc2d1
bažin	bažina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Šari	Šar	k1gMnSc5
<g/>
,	,	kIx,
Komaduru-Jobe	Komaduru-Job	k1gMnSc5
(	(	kIx(
<g/>
obě	dva	k4xCgFnPc1
do	do	k7c2
jezera	jezero	k1gNnSc2
Čad	Čad	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Medvědí	medvědí	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
Velkého	velký	k2eAgNnSc2d1
Solného	solný	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Humboldtova	Humboldtův	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
Humboldtova	Humboldtův	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jordán	Jordán	k1gMnSc1
(	(	kIx(
<g/>
do	do	k7c2
Mrtvého	mrtvý	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
Desaguadero	Desaguadero	k1gNnSc4
(	(	kIx(
<g/>
do	do	k7c2
jezera	jezero	k1gNnSc2
Poopó	Poopó	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bezodtoké	bezodtoký	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
patří	patřit	k5eAaImIp3nP
vlivem	vlivem	k7c2
své	svůj	k3xOyFgFnSc2
obecné	obecný	k2eAgFnSc2d1
suchosti	suchost	k1gFnSc2
a	a	k8xC
odlehlosti	odlehlost	k1gFnSc2
od	od	k7c2
moře	moře	k1gNnSc2
k	k	k7c3
málo	málo	k6eAd1
(	(	kIx(
<g/>
až	až	k9
nejméně	málo	k6eAd3
<g/>
)	)	kIx)
zalidněným	zalidněný	k2eAgInSc7d1
<g/>
,	,	kIx,
periferním	periferní	k2eAgFnPc3d1
částem	část	k1gFnPc3
světa	svět	k1gInSc2
<g/>
;	;	kIx,
výjimkou	výjimka	k1gFnSc7
je	být	k5eAaImIp3nS
především	především	k9
povodí	povodí	k1gNnSc1
Volhy	Volha	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
má	mít	k5eAaImIp3nS
mírné	mírný	k2eAgNnSc4d1
klima	klima	k1gNnSc4
a	a	k8xC
vláhy	vláha	k1gFnPc4
relativní	relativní	k2eAgInSc4d1
dostatek	dostatek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
aridních	aridní	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
při	při	k7c6
přílišném	přílišný	k2eAgInSc6d1
výparu	výpar	k1gInSc6
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
zvyšování	zvyšování	k1gNnSc3
salinity	salinita	k1gFnSc2
jezerních	jezerní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
se	se	k3xPyFc4
postupně	postupně	k6eAd1
začínají	začínat	k5eAaImIp3nP
tvořit	tvořit	k5eAaImF
solné	solný	k2eAgInPc1d1
útvary	útvar	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
travertiny	travertin	k1gInPc4
<g/>
,	,	kIx,
pisoidy	pisoida	k1gFnPc4
a	a	k8xC
solná	solný	k2eAgFnSc1d1
krusta	krusta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takováto	takovýto	k3xDgNnPc1
bezodtoká	bezodtoký	k2eAgNnPc1d1
jezera	jezero	k1gNnPc1
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
playas	playas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
se	se	k3xPyFc4
playas	playas	k1gMnSc1
ekonomicky	ekonomicky	k6eAd1
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
pro	pro	k7c4
těžbu	těžba	k1gFnSc4
přírodní	přírodní	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
vzniklé	vzniklý	k2eAgFnPc4d1
odpařováním	odpařování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Environmentální	environmentální	k2eAgFnPc1d1
hrozby	hrozba	k1gFnPc1
</s>
<s>
Plocha	plocha	k1gFnSc1
bezodtokých	bezodtoký	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
se	se	k3xPyFc4
vinou	vinout	k5eAaImIp3nP
desertifikace	desertifikace	k1gFnPc1
ve	v	k7c6
světě	svět	k1gInSc6
zvětšuje	zvětšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
především	především	k9
o	o	k7c4
oblast	oblast	k1gFnSc4
subsaharské	subsaharský	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
jev	jev	k1gInSc1
projevuje	projevovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
nápadně	nápadně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
problémovou	problémový	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
je	být	k5eAaImIp3nS
Aralské	aralský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc4
přítoky	přítok	k1gInPc4
Syrdarja	Syrdarja	k1gFnSc1
a	a	k8xC
Amudarja	Amudarja	k1gFnSc1
jsou	být	k5eAaImIp3nP
podél	podél	k7c2
svých	svůj	k3xOyFgInPc2
toků	tok	k1gInPc2
masivně	masivně	k6eAd1
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
na	na	k7c4
zemědělské	zemědělský	k2eAgNnSc4d1
zavlažování	zavlažování	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
výrazné	výrazný	k2eAgNnSc4d1
snížení	snížení	k1gNnSc4
(	(	kIx(
<g/>
až	až	k9
vymizení	vymizení	k1gNnSc2
<g/>
)	)	kIx)
přítoku	přítok	k1gInSc2
do	do	k7c2
jezera	jezero	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
vysychání	vysychání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Úmoří	úmoří	k1gNnSc1
</s>
<s>
Rozvodí	rozvodí	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
bezodtoká	bezodtoký	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
