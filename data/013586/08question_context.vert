<s>
Rudolf	Rudolf	k1gMnSc1
von	von	k1gMnSc1
Jhering	Jhering	k1gMnSc1
(	(	kIx(
<g/>
křestní	křestní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
psáno	psát	k5eAaImNgNnS
i	i	k8xC
Rudolph	Rudolph	k1gMnSc1
a	a	k8xC
příjmení	příjmení	k1gNnSc1
Ihering	Ihering	k1gMnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
ˈ	ˈ	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
;	;	kIx,
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1818	#num#	k4
<g/>
,	,	kIx,
Aurich	Aurich	k1gInSc1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1892	#num#	k4
<g/>
,	,	kIx,
Göttingen	Göttingen	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
právník	právník	k1gMnSc1
a	a	k8xC
právní	právní	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
průkopníkem	průkopník	k1gMnSc7
sociologického	sociologický	k2eAgNnSc2d1
chápání	chápání	k1gNnSc2
práva	právo	k1gNnSc2
a	a	k8xC
měl	mít	k5eAaImAgInS
vliv	vliv	k1gInSc4
především	především	k9
na	na	k7c4
německé	německý	k2eAgNnSc4d1
soukromé	soukromý	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
.	.	kIx.
</s>