<s>
Rudolf	Rudolf	k1gMnSc1
von	von	k1gInSc4
Jhering	Jhering	k1gInSc4
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
von	von	k1gInSc4
Jhering	Jhering	k1gInSc4
Narození	narození	k1gNnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1818	#num#	k4
Aurich	Auricha	k1gFnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1892	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
74	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Göttingen	Göttingen	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
GöttingenuUniverzita	GöttingenuUniverzita	k1gFnSc1
HeidelbergHumboldtova	HeidelbergHumboldtův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Pracoviště	pracoviště	k1gNnSc2
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1
univerzitaHumboldtova	univerzitaHumboldtův	k2eAgFnSc1d1
univerzitaLipská	univerzitaLipský	k2eAgFnSc1d1
univerzitaUniverzita	univerzitaUniverzita	k1gFnSc1
HeidelbergUniverzita	HeidelbergUniverzita	k1gFnSc1
GiessenUniverzita	GiessenUniverzita	k1gFnSc1
v	v	k7c6
GöttingenuUniverzita	GöttingenuUniverzita	k1gFnSc1
v	v	k7c4
BasilejiKielská	BasilejiKielský	k2eAgNnPc4d1
univerzitaRostocká	univerzitaRostocký	k2eAgNnPc4d1
univerzita	univerzita	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Rodolpho	Rodolpze	k6eAd1
von	von	k1gInSc1
Ihering	Ihering	k1gInSc1
(	(	kIx(
<g/>
vnuk	vnuk	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
von	von	k1gMnSc1
Jhering	Jhering	k1gMnSc1
(	(	kIx(
<g/>
křestní	křestní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
psáno	psát	k5eAaImNgNnS
i	i	k8xC
Rudolph	Rudolph	k1gMnSc1
a	a	k8xC
příjmení	příjmení	k1gNnSc1
Ihering	Ihering	k1gMnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
ˈ	ˈ	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
;	;	kIx,
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1818	#num#	k4
<g/>
,	,	kIx,
Aurich	Aurich	k1gInSc1
–	–	k?
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1892	#num#	k4
<g/>
,	,	kIx,
Göttingen	Göttingen	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
právník	právník	k1gMnSc1
a	a	k8xC
právní	právní	k2eAgMnSc1d1
teoretik	teoretik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
průkopníkem	průkopník	k1gMnSc7
sociologického	sociologický	k2eAgNnSc2d1
chápání	chápání	k1gNnSc2
práva	právo	k1gNnSc2
a	a	k8xC
měl	mít	k5eAaImAgInS
vliv	vliv	k1gInSc4
především	především	k9
na	na	k7c4
německé	německý	k2eAgNnSc4d1
soukromé	soukromý	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Jhering	Jhering	k1gInSc1
pocházel	pocházet	k5eAaImAgInS
z	z	k7c2
právnické	právnický	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
usazené	usazený	k2eAgFnSc2d1
ve	v	k7c6
Východním	východní	k2eAgNnSc6d1
Frísku	Frísko	k1gNnSc6
od	od	k7c2
roku	rok	k1gInSc2
1522	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
rodiče	rodič	k1gMnPc1
byli	být	k5eAaImAgMnP
Georg	Georg	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
Jhering	Jhering	k1gInSc4
z	z	k7c2
Aurichu	Aurich	k1gInSc2
(	(	kIx(
<g/>
1779	#num#	k4
<g/>
–	–	k?
<g/>
1825	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Anna	Anna	k1gFnSc1
Maria	Maria	k1gFnSc1
Schwersová	Schwersová	k1gFnSc1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
–	–	k?
<g/>
1861	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
Leeru	Leer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
v	v	k7c6
Heidelbergu	Heidelberg	k1gInSc6
<g/>
,	,	kIx,
Göttingenu	Göttingen	k1gInSc6
<g/>
,	,	kIx,
Mnichově	Mnichov	k1gInSc6
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1838	#num#	k4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
v	v	k7c6
roce	rok	k1gInSc6
1842	#num#	k4
získal	získat	k5eAaPmAgInS
doktorát	doktorát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
třikrát	třikrát	k6eAd1
ženatý	ženatý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
první	první	k4xOgFnSc7
manželkou	manželka	k1gFnSc7
byla	být	k5eAaImAgNnP
Helene	Helen	k1gInSc5
Hofmannová	Hofmannová	k1gFnSc1
(	(	kIx(
<g/>
†	†	k?
1848	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Idou	Ida	k1gFnSc7
Frölichovou	Frölichová	k1gFnSc7
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1826	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1867	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
měl	mít	k5eAaImAgMnS
pět	pět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
její	její	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
létě	léto	k1gNnSc6
1869	#num#	k4
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
vychovatelkou	vychovatelka	k1gFnSc7
svých	svůj	k3xOyFgFnPc2
dětí	dítě	k1gFnPc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
Luisou	Luisa	k1gFnSc7
Wildersovou	Wildersová	k1gFnSc7
(	(	kIx(
<g/>
1840	#num#	k4
<g/>
–	–	k?
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Hermann	Hermann	k1gMnSc1
von	von	k1gInSc4
Ihering	Ihering	k1gInSc1
(	(	kIx(
<g/>
1850	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
známý	známý	k2eAgMnSc1d1
biolog	biolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
profesorských	profesorský	k2eAgNnPc6d1
místech	místo	k1gNnPc6
v	v	k7c6
Basileji	Basilej	k1gFnSc6
<g/>
,	,	kIx,
Rostocku	Rostock	k1gInSc6
<g/>
,	,	kIx,
Kielu	Kiel	k1gInSc2
a	a	k8xC
Gießenu	Gießen	k1gInSc2
přišel	přijít	k5eAaPmAgInS
Jhering	Jhering	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1868	#num#	k4
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
přednesl	přednést	k5eAaPmAgMnS
slavnou	slavný	k2eAgFnSc4d1
přednášku	přednáška	k1gFnSc4
„	„	k?
<g/>
Boj	boj	k1gInSc1
o	o	k7c4
právo	právo	k1gNnSc4
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
„	„	k?
<g/>
Der	drát	k5eAaImRp2nS
Kampf	Kampf	k1gInSc1
ums	ums	k?
Recht	Recht	k?
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
za	za	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
dosáhla	dosáhnout	k5eAaPmAgFnS
dvanáct	dvanáct	k4xCc4
vydání	vydání	k1gNnPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
přeložena	přeložit	k5eAaPmNgFnS
do	do	k7c2
26	#num#	k4
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Životem	život	k1gInSc7
práva	právo	k1gNnSc2
je	být	k5eAaImIp3nS
boj	boj	k1gInSc1
–	–	k?
boj	boj	k1gInSc1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
tříd	třída	k1gFnPc2
a	a	k8xC
jednotlivců	jednotlivec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
smysl	smysl	k1gInSc1
jen	jen	k9
jako	jako	k9
projev	projev	k1gInSc4
konfliktů	konflikt	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
projevem	projev	k1gInSc7
snahy	snaha	k1gFnSc2
lidstva	lidstvo	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
samo	sám	k3xTgNnSc1
sebe	sebe	k3xPyFc4
zkrotilo	zkrotit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
bohužel	bohužel	k6eAd1
se	se	k3xPyFc4
právo	právo	k1gNnSc1
snažilo	snažit	k5eAaImAgNnS
násilí	násilí	k1gNnSc4
a	a	k8xC
bezpráví	bezpráví	k1gNnSc4
potírat	potírat	k5eAaImF
prostředky	prostředek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jednou	jednou	k6eAd1
v	v	k7c6
rozumném	rozumný	k2eAgInSc6d1
světě	svět	k1gInSc6
budou	být	k5eAaImBp3nP
považovány	považován	k2eAgFnPc1d1
za	za	k7c2
podivné	podivný	k2eAgFnSc2d1
a	a	k8xC
zároveň	zároveň	k6eAd1
ohavné	ohavný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neboť	neboť	k8xC
právo	právo	k1gNnSc4
se	se	k3xPyFc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
nikdy	nikdy	k6eAd1
nepokusilo	pokusit	k5eNaPmAgNnS
konflikty	konflikt	k1gInPc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
vyřešit	vyřešit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k6eAd1
zmírnit	zmírnit	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
stanovilo	stanovit	k5eAaPmAgNnS
pravidla	pravidlo	k1gNnPc4
<g/>
,	,	kIx,
podle	podle	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
vybojovat	vybojovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Jheringovi	Jhering	k1gMnSc3
udělil	udělit	k5eAaPmAgInS
dědičný	dědičný	k2eAgInSc1d1
šlechtický	šlechtický	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1872	#num#	k4
Jhering	Jhering	k1gInSc1
přijal	přijmout	k5eAaPmAgInS
profesuru	profesura	k1gFnSc4
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Adolf	Adolf	k1gMnSc1
Exner	Exner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jhering	Jhering	k1gInSc1
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
odmítl	odmítnout	k5eAaPmAgMnS
pozvání	pozvání	k1gNnSc4
do	do	k7c2
Lipska	Lipsko	k1gNnSc2
a	a	k8xC
Heidelbergu	Heidelberg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Rudolf	Rudolfa	k1gFnPc2
von	von	k1gInSc1
Jhering	Jhering	k1gInSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Rudolf	Rudolf	k1gMnSc1
von	von	k1gInSc1
Jhering	Jhering	k1gInSc4
<g/>
,	,	kIx,
Der	drát	k5eAaImRp2nS
Kampf	Kampf	k1gInSc1
ums	ums	k?
Recht	Recht	k?
<g/>
,	,	kIx,
Wien	Wien	k1gMnSc1
<g/>
,	,	kIx,
1872	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rudolf	Rudolf	k1gMnSc1
von	von	k1gInSc4
Jhering	Jhering	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
skuk	skuk	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
641	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118555367	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2141	#num#	k4
6556	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83033048	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84101514	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83033048	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Právo	právo	k1gNnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
