<s>
Odcházení	odcházení	k1gNnSc1
</s>
<s>
Odcházení	odcházení	k1gNnSc1
je	být	k5eAaImIp3nS
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
od	od	k7c2
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
o	o	k7c6
pěti	pět	k4xCc6
dějstvích	dějství	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nese	nést	k5eAaImIp3nS
výrazné	výrazný	k2eAgInPc4d1
autobiografické	autobiografický	k2eAgInPc4d1
rysy	rys	k1gInPc4
vztahující	vztahující	k2eAgInPc4d1
se	se	k3xPyFc4
k	k	k7c3
období	období	k1gNnSc3
po	po	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
funkci	funkce	k1gFnSc4
prezidenta	prezident	k1gMnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hru	hra	k1gFnSc4
dokončil	dokončit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
během	během	k7c2
svého	svůj	k3xOyFgInSc2
pobytu	pobyt	k1gInSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Pozadí	pozadí	k1gNnSc1
vzniku	vznik	k1gInSc2
</s>
<s>
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
začal	začít	k5eAaPmAgMnS
na	na	k7c6
hře	hra	k1gFnSc6
pracovat	pracovat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vytvořil	vytvořit	k5eAaPmAgMnS
první	první	k4xOgInPc4
dialogy	dialog	k1gInPc4
a	a	k8xC
bohatý	bohatý	k2eAgInSc4d1
poznámkový	poznámkový	k2eAgInSc4d1
aparát	aparát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
však	však	k9
projekt	projekt	k1gInSc4
opustil	opustit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
po	po	k7c6
skončení	skončení	k1gNnSc6
výkonu	výkon	k1gInSc2
prezidentské	prezidentský	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
,	,	kIx,
nalezl	nalézt	k5eAaBmAgInS,k5eAaPmAgInS
zpracované	zpracovaný	k2eAgFnPc4d1
poznámky	poznámka	k1gFnPc4
na	na	k7c6
Hrádečku	hrádeček	k1gInSc6
v	v	k7c6
šesti	šest	k4xCc6
školních	školní	k2eAgInPc6d1
sešitech	sešit	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkušenosti	zkušenost	k1gFnSc2
z	z	k7c2
vysoké	vysoký	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
tak	tak	k9
mohl	moct	k5eAaImAgInS
využít	využít	k5eAaPmF
k	k	k7c3
rozvinutí	rozvinutí	k1gNnSc3
dialogů	dialog	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
průvodních	průvodní	k2eAgInPc2d1
symptomů	symptom	k1gInPc2
ztráty	ztráta	k1gFnSc2
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hra	hra	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
z	z	k7c2
variací	variace	k1gFnPc2
Shakespearova	Shakespearův	k2eAgMnSc2d1
Krále	Král	k1gMnSc2
Leara	Lear	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
byl	být	k5eAaImAgInS
přítomen	přítomen	k2eAgInSc1d1
vinohradskému	vinohradský	k2eAgNnSc3d1
představení	představení	k1gNnSc3
manželky	manželka	k1gFnSc2
Dagmar	Dagmar	k1gFnSc2
Havlové	Havlová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zvárnila	zvárnit	k5eAaPmAgFnS,k5eAaImAgFnS
postavu	postava	k1gFnSc4
Ljubov	Ljubov	k1gInSc4
Raněvské	Raněvský	k2eAgNnSc1d1
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
hru	hra	k1gFnSc4
vtěsnat	vtěsnat	k5eAaPmF
do	do	k7c2
mantinelů	mantinel	k1gInPc2
Čechovova	Čechovův	k2eAgInSc2d1
Višňového	višňový	k2eAgInSc2d1
sadu	sad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celé	celý	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
bohaté	bohatý	k2eAgNnSc1d1
na	na	k7c4
narážky	narážka	k1gFnPc4
<g/>
,	,	kIx,
asociace	asociace	k1gFnPc4
i	i	k8xC
citace	citace	k1gFnPc4
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
přímo	přímo	k6eAd1
z	z	k7c2
uvedených	uvedený	k2eAgFnPc2d1
her	hra	k1gFnPc2
předlohy	předloha	k1gFnSc2
<g/>
,	,	kIx,
samotných	samotný	k2eAgInPc2d1
Havlových	Havlových	k2eAgInPc2d1
projevů	projev	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
i	i	k8xC
odkazů	odkaz	k1gInPc2
na	na	k7c4
další	další	k2eAgMnPc4d1
autory	autor	k1gMnPc4
(	(	kIx(
<g/>
např.	např.	kA
Samuela	Samuela	k1gFnSc1
Becketta	Becketta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obsah	obsah	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
Vilém	Vilém	k1gMnSc1
Rieger	Rieger	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
obývá	obývat	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
částí	část	k1gFnSc7
své	svůj	k3xOyFgFnSc2
rodiny	rodina	k1gFnSc2
(	(	kIx(
<g/>
jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
-	-	kIx~
Babička	babička	k1gFnSc1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
Zuzana	Zuzana	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnSc4d1
<g/>
)	)	kIx)
služební	služební	k2eAgFnSc4d1
vilu	vila	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
soukromí	soukromý	k2eAgMnPc1d1
částečně	částečně	k6eAd1
narušují	narušovat	k5eAaImIp3nP
novináři	novinář	k1gMnPc1
z	z	k7c2
bulvárního	bulvární	k2eAgInSc2d1
deníku	deník	k1gInSc2
Fuj	fuj	k0
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
Rieger	Rieger	k1gMnSc1
poskytuje	poskytovat	k5eAaImIp3nS
interview	interview	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Riegerovo	Riegerův	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
přebírá	přebírat	k5eAaImIp3nS
nově	nově	k6eAd1
jmenovaný	jmenovaný	k2eAgMnSc1d1
náměstek	náměstek	k1gMnSc1
Vlastimil	Vlastimil	k1gMnSc1
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
byl	být	k5eAaImAgInS
Rieger	Rieger	k1gInSc4
dříve	dříve	k6eAd2
v	v	k7c6
konfliktu	konflikt	k1gInSc6
v	v	k7c6
neurčené	určený	k2eNgFnSc6d1
záležitosti	záležitost	k1gFnSc6
korupce	korupce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
z	z	k7c2
pozice	pozice	k1gFnSc2
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
prosazuje	prosazovat	k5eAaImIp3nS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
bývalý	bývalý	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
má	mít	k5eAaImIp3nS
vládní	vládní	k2eAgFnSc4d1
vilu	vila	k1gFnSc4
opustit	opustit	k5eAaPmF
<g/>
;	;	kIx,
přitom	přitom	k6eAd1
však	však	k9
není	být	k5eNaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
najít	najít	k5eAaPmF
nové	nový	k2eAgNnSc4d1
ubytování	ubytování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
padne	padnout	k5eAaPmIp3nS,k5eAaImIp3nS
definitivní	definitivní	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
o	o	k7c4
stěhování	stěhování	k1gNnSc4
<g/>
,	,	kIx,
probíhají	probíhat	k5eAaImIp3nP
přípravy	příprava	k1gFnPc4
odjezdu	odjezd	k1gInSc2
<g/>
;	;	kIx,
na	na	k7c6
scéně	scéna	k1gFnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
několik	několik	k4yIc4
darů	dar	k1gInPc2
od	od	k7c2
nejvyšších	vysoký	k2eAgMnPc2d3
politických	politický	k2eAgMnPc2d1
reprezentantů	reprezentant	k1gMnPc2
různých	různý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přicházejí	přicházet	k5eAaImIp3nP
zprávy	zpráva	k1gFnPc1
o	o	k7c6
budoucím	budoucí	k2eAgInSc6d1
osudu	osud	k1gInSc6
vily	vila	k1gFnSc2
-	-	kIx~
stát	stát	k5eAaImF,k5eAaPmF
ji	on	k3xPp3gFnSc4
prodal	prodat	k5eAaPmAgMnS
čerstvě	čerstvě	k6eAd1
jmenovanému	jmenovaný	k1gMnSc3
vicepředsedovi	vicepředsed	k1gMnSc3
Kleinovi	Klein	k1gMnSc3
a	a	k8xC
ta	ten	k3xDgFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
využita	využít	k5eAaPmNgFnS
pro	pro	k7c4
komerční	komerční	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
oběť	oběť	k1gFnSc4
této	tento	k3xDgFnSc3
změně	změna	k1gFnSc3
jako	jako	k8xS,k8xC
první	první	k4xOgFnSc4
padá	padat	k5eAaImIp3nS
višňový	višňový	k2eAgInSc1d1
sad	sad	k1gInSc1
přiléhající	přiléhající	k2eAgInSc1d1
k	k	k7c3
vile	vila	k1gFnSc3
-	-	kIx~
na	na	k7c4
scénu	scéna	k1gFnSc4
doléhá	doléhat	k5eAaImIp3nS
zvuk	zvuk	k1gInSc1
motorových	motorový	k2eAgFnPc2d1
pil	pila	k1gFnPc2
a	a	k8xC
kácení	kácení	k1gNnSc6
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rieger	Rieger	k1gInSc1
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
zdiskreditován	zdiskreditován	k2eAgInSc4d1
detaily	detail	k1gInPc4
z	z	k7c2
intimního	intimní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
zveřejnil	zveřejnit	k5eAaPmAgInS
deník	deník	k1gInSc1
Fuj	fuj	k0
<g/>
,	,	kIx,
jako	jako	k9
jediné	jediný	k2eAgNnSc1d1
reálné	reálný	k2eAgNnSc1d1
uplatnění	uplatnění	k1gNnSc1
se	se	k3xPyFc4
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
jeví	jevit	k5eAaImIp3nS
místo	místo	k7c2
poradce	poradce	k1gMnSc1
bývalému	bývalý	k2eAgMnSc3d1
tajemníkovi	tajemník	k1gMnSc3
Kleina	Klein	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
místy	místy	k6eAd1
přerušována	přerušovat	k5eAaImNgFnS
hlasem	hlasem	k6eAd1
z	z	k7c2
reproduktoru	reproduktor	k1gInSc2
komentujícím	komentující	k2eAgInSc6d1
hru	hra	k1gFnSc4
z	z	k7c2
pohledu	pohled	k1gInSc2
autora	autor	k1gMnSc2
<g/>
;	;	kIx,
po	po	k7c4
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
herci	herec	k1gMnPc7
zůstávají	zůstávat	k5eAaImIp3nP
nehybní	hybný	k2eNgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Text	text	k1gInSc1
hry	hra	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
nepřímé	přímý	k2eNgFnSc2d1
narážky	narážka	k1gFnSc2
na	na	k7c4
Višňový	višňový	k2eAgInSc4d1
sad	sad	k1gInSc4
A.	A.	kA
P.	P.	kA
Čechova	Čechov	k1gMnSc2
a	a	k8xC
na	na	k7c4
Shakespearova	Shakespearův	k2eAgMnSc4d1
Krále	Král	k1gMnSc4
Leara	Lear	k1gMnSc4
<g/>
;	;	kIx,
lze	lze	k6eAd1
pozorovat	pozorovat	k5eAaImF
i	i	k9
určitou	určitý	k2eAgFnSc4d1
dávku	dávka	k1gFnSc4
</s>
<s>
sebeironie	sebeironie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Osoby	osoba	k1gFnPc1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Rieger	Rieger	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
kancléř	kancléř	k1gMnSc1
</s>
<s>
Babička	babička	k1gFnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
Riegera	Riegera	k1gFnSc1
</s>
<s>
Vlasta	Vlasta	k1gFnSc1
<g/>
,	,	kIx,
starší	starý	k2eAgFnSc1d2
dcera	dcera	k1gFnSc1
Riegera	Riegero	k1gNnSc2
</s>
<s>
Albín	Albín	k1gMnSc1
<g/>
,	,	kIx,
manžel	manžel	k1gMnSc1
Vlasty	Vlasta	k1gFnSc2
</s>
<s>
Zuzana	Zuzana	k1gFnSc1
<g/>
,	,	kIx,
mladší	mladý	k2eAgFnSc1d2
dcera	dcera	k1gFnSc1
Riegera	Riegero	k1gNnSc2
</s>
<s>
Irena	Irena	k1gFnSc1
<g/>
,	,	kIx,
Riegerova	Riegerův	k2eAgFnSc1d1
dlouholetá	dlouholetý	k2eAgFnSc1d1
přítelkyně	přítelkyně	k1gFnSc1
</s>
<s>
Monika	Monika	k1gFnSc1
<g/>
,	,	kIx,
Irenina	Irenin	k2eAgFnSc1d1
přítelkyně	přítelkyně	k1gFnSc1
</s>
<s>
Hanuš	Hanuš	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
Riegerův	Riegerův	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
Hanuše	Hanuš	k1gMnSc2
</s>
<s>
Osvald	Osvald	k1gMnSc1
<g/>
,	,	kIx,
pomocník	pomocník	k1gMnSc1
v	v	k7c6
domácnosti	domácnost	k1gFnSc6
Riegera	Riegero	k1gNnSc2
</s>
<s>
Knobloch	Knobloch	k1gMnSc1
<g/>
,	,	kIx,
zahradník	zahradník	k1gMnSc1
</s>
<s>
Vlastík	Vlastík	k1gMnSc1
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
náměstek	náměstek	k1gMnSc1
a	a	k8xC
posléze	posléze	k6eAd1
vicepředseda	vicepředseda	k1gMnSc1
</s>
<s>
Bea	Bea	k?
Weissenmütelhofová	Weissenmütelhofový	k2eAgFnSc1d1
<g/>
,	,	kIx,
studentka	studentka	k1gFnSc1
politologie	politologie	k1gFnSc1
a	a	k8xC
multikulturní	multikulturní	k2eAgFnSc1d1
sociopsychologie	sociopsychologie	k1gFnSc1
</s>
<s>
Jack	Jack	k1gMnSc1
<g/>
,	,	kIx,
novinář	novinář	k1gMnSc1
</s>
<s>
Bob	Bob	k1gMnSc1
<g/>
,	,	kIx,
fotograf	fotograf	k1gMnSc1
</s>
<s>
Strážníci	strážník	k1gMnPc1
</s>
<s>
Hlas	hlas	k1gInSc1
z	z	k7c2
reproduktoru	reproduktor	k1gInSc2
</s>
<s>
Uvedení	uvedení	k1gNnSc1
</s>
<s>
Česká	český	k2eAgNnPc1d1
nastudování	nastudování	k1gNnPc1
</s>
<s>
Text	text	k1gInSc1
hry	hra	k1gFnSc2
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
knižně	knižně	k6eAd1
koncem	koncem	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
neoficiální	oficiální	k2eNgFnSc1d1,k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
derniéra	derniéra	k1gFnSc1
na	na	k7c6
půdě	půda	k1gFnSc6
ostravského	ostravský	k2eAgNnSc2d1
Wichterlova	Wichterlův	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hru	hra	k1gFnSc4
nastudoval	nastudovat	k5eAaBmAgInS
amatérský	amatérský	k2eAgInSc1d1
divadelní	divadelní	k2eAgInSc1d1
soubor	soubor	k1gInSc1
složený	složený	k2eAgInSc1d1
ze	z	k7c2
studentů	student	k1gMnPc2
a	a	k8xC
pedagogů	pedagog	k1gMnPc2
gymnázia	gymnázium	k1gNnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
režiséra	režisér	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
Sosny	Sosna	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgNnSc4
oficiální	oficiální	k2eAgNnSc4d1
provedení	provedení	k1gNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
vedl	vést	k5eAaImAgMnS
režisér	režisér	k1gMnSc1
David	David	k1gMnSc1
Radok	Radok	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
hlavních	hlavní	k2eAgFnPc6d1
rolích	role	k1gFnPc6
vystoupili	vystoupit	k5eAaPmAgMnP
Zuzana	Zuzana	k1gFnSc1
Stivínová	Stivínová	k1gFnSc1
<g/>
,	,	kIx,
Vlasta	Vlasta	k1gFnSc1
Chramostová	Chramostová	k1gFnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Tříska	Tříska	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Neúspěšná	úspěšný	k2eNgNnPc1d1
jednání	jednání	k1gNnPc1
zastupující	zastupující	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
Aura-Pont	Aura-Ponta	k1gFnPc2
nejdříve	dříve	k6eAd3
vedla	vést	k5eAaImAgFnS
s	s	k7c7
vedením	vedení	k1gNnSc7
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
zkrachovala	zkrachovat	k5eAaPmAgFnS
představa	představa	k1gFnSc1
realizace	realizace	k1gFnSc1
v	v	k7c6
Divadle	divadlo	k1gNnSc6
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
odmítnutí	odmítnutí	k1gNnSc3
Jana	Jan	k1gMnSc2
Třísky	Tříska	k1gMnSc2
a	a	k8xC
externího	externí	k2eAgMnSc2d1
režiséra	režisér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastudování	nastudování	k1gNnSc1
tak	tak	k6eAd1
proběhlo	proběhnout	k5eAaPmAgNnS
na	na	k7c6
scéně	scéna	k1gFnSc6
Divadla	divadlo	k1gNnSc2
Archa	archa	k1gFnSc1
v	v	k7c6
režii	režie	k1gFnSc6
Davida	David	k1gMnSc2
Radoka	Radoek	k1gMnSc2
<g/>
;	;	kIx,
premiéra	premiéra	k1gFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
patří	patřit	k5eAaImIp3nS
dílo	dílo	k1gNnSc4
k	k	k7c3
základu	základ	k1gInSc3
repertoáru	repertoár	k1gInSc2
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
se	se	k3xPyFc4
režisér	režisér	k1gMnSc1
držel	držet	k5eAaImAgMnS
představ	představ	k1gInSc4
autora	autor	k1gMnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
Tříska	Tříska	k1gMnSc1
uplatnil	uplatnit	k5eAaPmAgMnS
výraznou	výrazný	k2eAgFnSc4d1
gestikulaci	gestikulace	k1gFnSc4
hraníčí	hraníčí	k2eAgFnSc4d1
s	s	k7c7
karikováním	karikování	k1gNnSc7
Riegera	Riegero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
problému	problém	k1gInSc2
byla	být	k5eAaImAgFnS
stále	stále	k6eAd1
během	během	k7c2
zkoušení	zkoušení	k1gNnSc2
Dagmar	Dagmar	k1gFnSc1
Havlová	Havlová	k1gFnSc1
nahrazena	nahradit	k5eAaPmNgFnS
Zuzanou	Zuzana	k1gFnSc7
Stivínovou	Stivínový	k2eAgFnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
taktéž	taktéž	k?
filmově	filmově	k6eAd1
zpracována	zpracován	k2eAgFnSc1d1
<g/>
,	,	kIx,
pod	pod	k7c7
režijním	režijní	k2eAgNnSc7d1
vedením	vedení	k1gNnSc7
Jaroslava	Jaroslav	k1gMnSc2
Brabce	Brabec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgNnSc1d1
nastudování	nastudování	k1gNnSc1
</s>
<s>
V	v	k7c6
zahraničí	zahraničí	k1gNnSc6
byla	být	k5eAaImAgFnS
hra	hra	k1gFnSc1
poprvé	poprvé	k6eAd1
uvedena	uveden	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2008	#num#	k4
v	v	k7c6
Londýnském	londýnský	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
Orange	Orangus	k1gMnSc5
Tree	Treus	k1gMnSc5
Theatre	Theatr	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
angličtiny	angličtina	k1gFnSc2
hru	hra	k1gFnSc4
přeložil	přeložit	k5eAaPmAgMnS
Paul	Paul	k1gMnSc1
Wilson	Wilson	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
jako	jako	k8xS,k8xC
člen	člen	k1gInSc1
hudební	hudební	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
The	The	k1gMnSc1
Plastic	Plastice	k1gFnPc2
People	People	k1gMnSc1
of	of	k?
the	the	k?
Universe	Universe	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Premiéra	premiéra	k1gFnSc1
německého	německý	k2eAgNnSc2d1
uvedení	uvedení	k1gNnSc2
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
v	v	k7c6
Cáchách	Cáchy	k1gFnPc6
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
Václava	Václav	k1gMnSc2
a	a	k8xC
Dagmar	Dagmar	k1gFnSc4
Havlových	Havlová	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
s	s	k7c7
českým	český	k2eAgNnSc7d1
hereckým	herecký	k2eAgNnSc7d1
obsazením	obsazení	k1gNnSc7
(	(	kIx(
<g/>
zpracování	zpracování	k1gNnSc1
Andreje	Andrej	k1gMnSc2
Kroba	Krob	k1gMnSc2
z	z	k7c2
Klicperova	Klicperův	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
v	v	k7c6
Divadle	divadlo	k1gNnSc6
na	na	k7c6
Tagance	Taganka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představení	představení	k1gNnSc6
se	se	k3xPyFc4
účastnili	účastnit	k5eAaImAgMnP
např.	např.	kA
bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Milan	Milan	k1gMnSc1
Uhde	Uhde	k1gInSc1
nebo	nebo	k8xC
někdejší	někdejší	k2eAgMnSc1d1
vicepremiér	vicepremiér	k1gMnSc1
Martin	Martin	k1gMnSc1
Jahn	Jahn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
ruské	ruský	k2eAgMnPc4d1
diváky	divák	k1gMnPc4
byl	být	k5eAaImAgInS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
simultánní	simultánní	k2eAgInSc4d1
překlad	překlad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
slovenštiny	slovenština	k1gFnSc2
hru	hra	k1gFnSc4
pro	pro	k7c4
Slovenské	slovenský	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
přeložil	přeložit	k5eAaPmAgMnS
Milan	Milan	k1gMnSc1
Lasica	Lasica	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Korejské	korejský	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
počínaje	počínaje	k7c7
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2010	#num#	k4
konala	konat	k5eAaImAgFnS
celkem	celkem	k6eAd1
čtyři	čtyři	k4xCgNnPc1
představení	představení	k1gNnPc2
Divadla	divadlo	k1gNnSc2
Archa	archa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Originální	originální	k2eAgFnSc1d1
české	český	k2eAgNnSc4d1
nastudování	nastudování	k1gNnSc4
zhlédli	zhlédnout	k5eAaPmAgMnP
jihokorejští	jihokorejský	k2eAgMnPc1d1
diváce	diváko	k6eAd1
v	v	k7c6
soulském	soulský	k2eAgNnSc6d1
LG	LG	kA
Artcentre	Artcentr	k1gInSc5
v	v	k7c6
původním	původní	k2eAgNnSc6d1
obsazení	obsazení	k1gNnSc6
v	v	k7c6
českém	český	k2eAgNnSc6d1
znění	znění	k1gNnSc6
s	s	k7c7
korejskými	korejský	k2eAgInPc7d1
titulky	titulek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hru	hra	k1gFnSc4
do	do	k7c2
korejštiny	korejština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
divadelní	divadelní	k2eAgMnSc1d1
režisér	režisér	k1gMnSc1
a	a	k8xC
absolvent	absolvent	k1gMnSc1
pražské	pražský	k2eAgInPc4d1
DAMU	DAMU	kA
Sin	sin	kA
Ho	on	k3xPp3gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
překlad	překlad	k1gInSc4
u	u	k7c2
této	tento	k3xDgFnSc2
příležitosti	příležitost	k1gFnSc2
péčí	péče	k1gFnPc2
Velvyslanectví	velvyslanectví	k1gNnSc2
ČR	ČR	kA
v	v	k7c6
Soulu	Soul	k1gInSc6
vyšel	vyjít	k5eAaPmAgInS
také	také	k9
knižně	knižně	k6eAd1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
více	hodně	k6eAd2
na	na	k7c4
www.mzv.cz/seoul	www.mzv.cz/seoul	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Knižní	knižní	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
textu	text	k1gInSc2
hry	hra	k1gFnSc2
</s>
<s>
Odcházení	odcházení	k1gNnSc1
<g/>
,	,	kIx,
nakladatelství	nakladatelství	k1gNnSc1
Respekt	respekt	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-903766-0-1	978-80-903766-0-1	k4
</s>
<s>
v	v	k7c6
rámci	rámec	k1gInSc6
svazku	svazek	k1gInSc2
Spisy	spis	k1gInPc7
VIII	VIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Torst	Torst	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-7215-320-6	978-80-7215-320-6	k4
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Odcházení	odcházení	k1gNnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
hry	hra	k1gFnSc2
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
natočil	natočit	k5eAaBmAgMnS
celovečerní	celovečerní	k2eAgInSc4d1
stejnojmenný	stejnojmenný	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnPc4d1
role	role	k1gFnPc4
ztvárnili	ztvárnit	k5eAaPmAgMnP
Josef	Josefa	k1gFnPc2
Abrhám	Abrha	k1gFnPc3
a	a	k8xC
Dagmar	Dagmar	k1gFnSc1
Havlová	Havlová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéru	premiéra	k1gFnSc4
v	v	k7c6
kinech	kino	k1gNnPc6
měl	mít	k5eAaImAgInS
film	film	k1gInSc1
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
ŽANTOVSKÝ	Žantovský	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Havel	Havel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Odcházení	odcházení	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
527	#num#	k4
<g/>
–	–	k?
<g/>
535	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Světová	světový	k2eAgFnSc1d1
premiéra	premiéra	k1gFnSc1
hry	hra	k1gFnSc2
Odcházení	odcházení	k1gNnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
Archivováno	archivovat	k5eAaBmNgNnS
26	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Indrová	Indrová	k1gFnSc1
Jaroslava	Jaroslava	k1gFnSc1
<g/>
,	,	kIx,
solokapr	solokapr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Havlovo	Havlův	k2eAgNnSc1d1
Odcházení	odcházení	k1gNnSc1
poprvé	poprvé	k6eAd1
za	za	k7c7
hranicemi	hranice	k1gFnPc7
<g/>
:	:	kIx,
v	v	k7c6
Londýně	Londýn	k1gInSc6
mělo	mít	k5eAaImAgNnS
úspěch	úspěch	k1gInSc4
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Odcházení	odcházení	k1gNnSc1
na	na	k7c6
Kinoboxu	Kinobox	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Odcházení	odcházení	k1gNnPc1
ke	k	k7c3
stažení	stažení	k1gNnSc3
na	na	k7c6
webu	web	k1gInSc6
Knihovny	knihovna	k1gFnSc2
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Odcházení	odcházení	k1gNnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Archivu	archiv	k1gInSc2
Národního	národní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
