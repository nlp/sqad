<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1578	[number]	k4	1578
jako	jako	k8xS	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
Štýrského	štýrský	k2eAgInSc2d1	štýrský
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Anny	Anna	k1gFnSc2	Anna
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
(	(	kIx(	(
<g/>
1551	[number]	k4	1551
<g/>
–	–	k?	–
<g/>
1608	[number]	k4	1608
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
