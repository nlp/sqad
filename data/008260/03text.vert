<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
Štýrský	štýrský	k2eAgMnSc1d1	štýrský
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1578	[number]	k4	1578
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1637	[number]	k4	1637
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
římský	římský	k2eAgMnSc1d1	římský
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
a	a	k8xC	a
chorvatský	chorvatský	k2eAgMnSc1d1	chorvatský
<g/>
,	,	kIx,	,
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
v	v	k7c6	v
letech	let	k1gInPc6	let
1620	[number]	k4	1620
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
rakouský	rakouský	k2eAgMnSc1d1	rakouský
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1590	[number]	k4	1590
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
rovněž	rovněž	k9	rovněž
vévoda	vévoda	k1gMnSc1	vévoda
štýrský	štýrský	k2eAgMnSc1d1	štýrský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1578	[number]	k4	1578
jako	jako	k8xS	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
Štýrského	štýrský	k2eAgInSc2d1	štýrský
(	(	kIx(	(
<g/>
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1590	[number]	k4	1590
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Anny	Anna	k1gFnSc2	Anna
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
(	(	kIx(	(
<g/>
1551	[number]	k4	1551
<g/>
–	–	k?	–
<g/>
1608	[number]	k4	1608
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vychováván	vychovávat	k5eAaImNgInS	vychovávat
v	v	k7c6	v
přísné	přísný	k2eAgFnSc6d1	přísná
katolické	katolický	k2eAgFnSc6d1	katolická
víře	víra	k1gFnSc6	víra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
osobně	osobně	k6eAd1	osobně
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
i	i	k9	i
jeho	jeho	k3xOp3gNnSc4	jeho
pozdější	pozdní	k2eAgNnSc4d2	pozdější
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1608	[number]	k4	1608
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1590	[number]	k4	1590
byl	být	k5eAaImAgMnS	být
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
v	v	k7c6	v
bavorském	bavorský	k2eAgInSc6d1	bavorský
Ingolstadtu	Ingolstadt	k1gInSc6	Ingolstadt
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
však	však	k9	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
a	a	k8xC	a
regentskou	regentský	k2eAgFnSc4d1	regentská
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
Rakousích	Rakousy	k1gInPc6	Rakousy
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
které	který	k3yIgFnPc1	který
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
Štýrsko	Štýrsko	k1gNnSc4	Štýrsko
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc4	Korutany
a	a	k8xC	a
Kraňsko	Kraňsko	k1gNnSc4	Kraňsko
<g/>
)	)	kIx)	)
za	za	k7c4	za
nezletilého	zletilý	k2eNgMnSc4d1	nezletilý
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratranec	bratranec	k1gMnSc1	bratranec
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinandovi	Ferdinandův	k2eAgMnPc1d1	Ferdinandův
katoličtí	katolický	k2eAgMnPc1d1	katolický
učitelé	učitel	k1gMnPc1	učitel
mu	on	k3xPp3gMnSc3	on
vštípili	vštípit	k5eAaPmAgMnP	vštípit
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
panovník	panovník	k1gMnSc1	panovník
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
duchovní	duchovní	k2eAgFnSc4d1	duchovní
spásu	spása	k1gFnSc4	spása
svých	svůj	k3xOyFgMnPc2	svůj
poddaných	poddaný	k1gMnPc2	poddaný
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
povinností	povinnost	k1gFnPc2	povinnost
je	být	k5eAaImIp3nS	být
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
odklonem	odklon	k1gInSc7	odklon
od	od	k7c2	od
"	"	kIx"	"
<g/>
správné	správný	k2eAgFnSc2d1	správná
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
katolické	katolický	k2eAgFnSc2d1	katolická
<g/>
)	)	kIx)	)
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
později	pozdě	k6eAd2	pozdě
nestrpěl	strpět	k5eNaPmAgMnS	strpět
nekatolíky	nekatolík	k1gMnPc4	nekatolík
na	na	k7c6	na
ovládaných	ovládaný	k2eAgNnPc6d1	ovládané
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
politika	politika	k1gFnSc1	politika
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
tímto	tento	k3xDgInSc7	tento
postojem	postoj	k1gInSc7	postoj
a	a	k8xC	a
spočívala	spočívat	k5eAaImAgFnS	spočívat
především	především	k9	především
v	v	k7c6	v
nekompromisním	kompromisní	k2eNgInSc6d1	nekompromisní
postupu	postup	k1gInSc6	postup
proti	proti	k7c3	proti
příslušníkům	příslušník	k1gMnPc3	příslušník
nekatolických	katolický	k2eNgFnPc2d1	nekatolická
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátek	počátek	k1gInSc1	počátek
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
dědičných	dědičný	k2eAgFnPc6d1	dědičná
zemích	zem	k1gFnPc6	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
dědičných	dědičný	k2eAgFnPc2d1	dědičná
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
navrátil	navrátit	k5eAaPmAgInS	navrátit
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
svých	svůj	k3xOyFgFnPc2	svůj
studií	studie	k1gFnPc2	studie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1595	[number]	k4	1595
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Loreta	Loreto	k1gNnSc2	Loreto
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
kavalírskou	kavalírský	k2eAgFnSc4d1	kavalírská
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
mladí	mladý	k2eAgMnPc1d1	mladý
šlechtici	šlechtic	k1gMnPc1	šlechtic
získávali	získávat	k5eAaImAgMnP	získávat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
habsburském	habsburský	k2eAgInSc6d1	habsburský
rodě	rod	k1gInSc6	rod
však	však	k9	však
byla	být	k5eAaImAgFnS	být
taková	takový	k3xDgFnSc1	takový
cesta	cesta	k1gFnSc1	cesta
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
příslušníkem	příslušník	k1gMnSc7	příslušník
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
něco	něco	k3yInSc1	něco
takového	takový	k3xDgMnSc4	takový
podnikl	podniknout	k5eAaPmAgMnS	podniknout
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
začal	začít	k5eAaPmAgInS	začít
tvrdě	tvrdě	k6eAd1	tvrdě
potlačovat	potlačovat	k5eAaImF	potlačovat
vyznavače	vyznavač	k1gMnPc4	vyznavač
nekatolické	katolický	k2eNgFnSc2d1	nekatolická
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1598	[number]	k4	1598
například	například	k6eAd1	například
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
nekatolické	katolický	k2eNgMnPc4d1	nekatolický
kazatele	kazatel	k1gMnPc4	kazatel
a	a	k8xC	a
duchovní	duchovní	k2eAgMnPc1d1	duchovní
ze	z	k7c2	z
Štýrského	štýrský	k2eAgInSc2d1	štýrský
Hradce	Hradec	k1gInSc2	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
kroky	krok	k1gInPc1	krok
vyvolávaly	vyvolávat	k5eAaImAgInP	vyvolávat
odpor	odpor	k1gInSc4	odpor
mezi	mezi	k7c7	mezi
protestantskou	protestantský	k2eAgFnSc7d1	protestantská
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
nejednotnost	nejednotnost	k1gFnSc1	nejednotnost
však	však	k9	však
mu	on	k3xPp3gMnSc3	on
nakonec	nakonec	k6eAd1	nakonec
umožnila	umožnit	k5eAaPmAgFnS	umožnit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
již	již	k9	již
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
vnitrorakouských	vnitrorakouský	k2eAgFnPc2d1	vnitrorakouský
zemí	zem	k1gFnPc2	zem
alespoň	alespoň	k9	alespoň
formálně	formálně	k6eAd1	formálně
katolická	katolický	k2eAgFnSc1d1	katolická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
Matyáš	Matyáš	k1gMnSc1	Matyáš
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgMnPc4	žádný
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
přijat	přijmout	k5eAaPmNgInS	přijmout
českými	český	k2eAgInPc7d1	český
stavy	stav	k1gInPc7	stav
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1617	[number]	k4	1617
<g/>
)	)	kIx)	)
a	a	k8xC	a
korunován	korunovat	k5eAaBmNgInS	korunovat
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Volbu	volba	k1gFnSc4	volba
mu	on	k3xPp3gMnSc3	on
zaručila	zaručit	k5eAaPmAgFnS	zaručit
tzv.	tzv.	kA	tzv.
Oňatova	Oňatův	k2eAgFnSc1d1	Oňatův
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
a	a	k8xC	a
španělskými	španělský	k2eAgMnPc7d1	španělský
Habsburky	Habsburk	k1gMnPc7	Habsburk
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
španělská	španělský	k2eAgFnSc1d1	španělská
větev	větev	k1gFnSc1	větev
rodu	rod	k1gInSc2	rod
vzdala	vzdát	k5eAaPmAgFnS	vzdát
nároku	nárok	k1gInSc3	nárok
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
stavové	stavový	k2eAgFnPc1d1	stavová
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
věděli	vědět	k5eAaImAgMnP	vědět
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
protireformačním	protireformační	k2eAgNnSc6d1	protireformační
tažení	tažení	k1gNnSc6	tažení
v	v	k7c6	v
Korutanech	Korutany	k1gInPc6	Korutany
a	a	k8xC	a
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
zvolili	zvolit	k5eAaPmAgMnP	zvolit
za	za	k7c4	za
příslib	příslib	k1gInSc4	příslib
respektování	respektování	k1gNnSc2	respektování
Rudolfova	Rudolfův	k2eAgInSc2d1	Rudolfův
majestátu	majestát	k1gInSc2	majestát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zaručoval	zaručovat	k5eAaImAgInS	zaručovat
náboženské	náboženský	k2eAgFnPc4d1	náboženská
svobody	svoboda	k1gFnPc4	svoboda
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
měšťanů	měšťan	k1gMnPc2	měšťan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgNnSc1d1	české
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
povstání	povstání	k1gNnSc1	povstání
===	===	k?	===
</s>
</p>
<p>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
absolutistického	absolutistický	k2eAgMnSc4d1	absolutistický
vládce	vládce	k1gMnSc4	vládce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
popíral	popírat	k5eAaImAgInS	popírat
světská	světský	k2eAgNnPc4d1	světské
práva	právo	k1gNnPc4	právo
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nově	nově	k6eAd1	nově
ovládnutých	ovládnutý	k2eAgFnPc6d1	ovládnutá
zemích	zem	k1gFnPc6	zem
dosud	dosud	k6eAd1	dosud
těšila	těšit	k5eAaImAgFnS	těšit
značným	značný	k2eAgFnPc3d1	značná
výsadám	výsada	k1gFnPc3	výsada
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysokému	vysoký	k2eAgInSc3d1	vysoký
počtu	počet	k1gInSc3	počet
protestantů	protestant	k1gMnPc2	protestant
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
mezi	mezi	k7c7	mezi
šlechtou	šlechta	k1gFnSc7	šlechta
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
stal	stát	k5eAaPmAgMnS	stát
brzy	brzy	k6eAd1	brzy
nepopulárním	populární	k2eNgInPc3d1	nepopulární
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jeho	jeho	k3xOp3gMnPc1	jeho
odpůrci	odpůrce	k1gMnPc1	odpůrce
zahájili	zahájit	k5eAaPmAgMnP	zahájit
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1618	[number]	k4	1618
Pražskou	pražský	k2eAgFnSc7d1	Pražská
defenestrací	defenestrace	k1gFnSc7	defenestrace
české	český	k2eAgNnSc1d1	české
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
českých	český	k2eAgInPc2d1	český
nekatolických	katolický	k2eNgInPc2d1	nekatolický
stavů	stav	k1gInPc2	stav
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
také	také	k9	také
protestantská	protestantský	k2eAgFnSc1d1	protestantská
šlechta	šlechta	k1gFnSc1	šlechta
z	z	k7c2	z
Horních	horní	k2eAgInPc2d1	horní
a	a	k8xC	a
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nechtěla	chtít	k5eNaImAgFnS	chtít
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
přijmout	přijmout	k5eAaPmF	přijmout
za	za	k7c4	za
arcivévodu	arcivévoda	k1gMnSc4	arcivévoda
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
císařovým	císařův	k2eAgMnSc7d1	císařův
kancléřem	kancléř	k1gMnSc7	kancléř
kardinálem	kardinál	k1gMnSc7	kardinál
Kleslem	Klesl	k1gMnSc7	Klesl
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
situaci	situace	k1gFnSc4	situace
řešit	řešit	k5eAaImF	řešit
diplomatickou	diplomatický	k2eAgFnSc7d1	diplomatická
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
ke	k	k7c3	k
kompromisům	kompromis	k1gInPc3	kompromis
a	a	k8xC	a
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
radikální	radikální	k2eAgInPc4d1	radikální
kroky	krok	k1gInPc4	krok
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nechal	nechat	k5eAaPmAgMnS	nechat
Khlesla	Khlesla	k1gFnSc4	Khlesla
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1618	[number]	k4	1618
zatknout	zatknout	k5eAaPmF	zatknout
a	a	k8xC	a
internovat	internovat	k5eAaBmF	internovat
<g/>
.	.	kIx.	.
</s>
<s>
Nemocný	mocný	k2eNgMnSc1d1	nemocný
císař	císař	k1gMnSc1	císař
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
situaci	situace	k1gFnSc6	situace
zůstal	zůstat	k5eAaPmAgInS	zůstat
pasivní	pasivní	k2eAgInSc1d1	pasivní
a	a	k8xC	a
nijak	nijak	k6eAd1	nijak
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
císaře	císař	k1gMnSc2	císař
Matyáše	Matyáš	k1gMnSc2	Matyáš
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1619	[number]	k4	1619
se	se	k3xPyFc4	se
nemohl	moct	k5eNaImAgInS	moct
fakticky	fakticky	k6eAd1	fakticky
ujmout	ujmout	k5eAaPmF	ujmout
vlády	vláda	k1gFnPc4	vláda
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
a	a	k8xC	a
rakouských	rakouský	k2eAgFnPc6d1	rakouská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgNnPc1d1	české
stavovská	stavovský	k2eAgNnPc1d1	Stavovské
vojska	vojsko	k1gNnPc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Matyáše	Matyáš	k1gMnSc2	Matyáš
Thurna	Thurno	k1gNnSc2	Thurno
oblehla	oblehnout	k5eAaPmAgFnS	oblehnout
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
dobytí	dobytí	k1gNnSc4	dobytí
se	se	k3xPyFc4	se
nepokusila	pokusit	k5eNaPmAgFnS	pokusit
<g/>
.	.	kIx.	.
</s>
<s>
Stavovští	stavovský	k2eAgMnPc1d1	stavovský
předáci	předák	k1gMnPc1	předák
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
o	o	k7c4	o
diplomatické	diplomatický	k2eAgNnSc4d1	diplomatické
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
jej	on	k3xPp3gNnSc4	on
donutit	donutit	k5eAaPmF	donutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
dřívější	dřívější	k2eAgFnPc4d1	dřívější
stavovské	stavovský	k2eAgFnPc4d1	stavovská
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc4d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
a	a	k8xC	a
Horních	horní	k2eAgInPc6d1	horní
Rakousích	Rakousy	k1gInPc6	Rakousy
a	a	k8xC	a
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1619	[number]	k4	1619
pronikli	proniknout	k5eAaPmAgMnP	proniknout
zástupci	zástupce	k1gMnPc1	zástupce
rakouské	rakouský	k2eAgFnSc2d1	rakouská
protestantské	protestantský	k2eAgFnSc2d1	protestantská
šlechty	šlechta	k1gFnSc2	šlechta
do	do	k7c2	do
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
Hofburgu	Hofburg	k1gInSc2	Hofburg
a	a	k8xC	a
předložili	předložit	k5eAaPmAgMnP	předložit
mu	on	k3xPp3gMnSc3	on
své	svůj	k3xOyFgInPc4	svůj
požadavky	požadavek	k1gInPc4	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
donucen	donucen	k2eAgMnSc1d1	donucen
tyto	tento	k3xDgInPc4	tento
požadavky	požadavek	k1gInPc4	požadavek
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
rychlým	rychlý	k2eAgInSc7d1	rychlý
zásahem	zásah	k1gInSc7	zásah
vojenského	vojenský	k2eAgInSc2d1	vojenský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
to	ten	k3xDgNnSc4	ten
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
zásah	zásah	k1gInSc4	zásah
boží	boží	k2eAgFnSc2d1	boží
Prozřetelnosti	prozřetelnost	k1gFnSc2	prozřetelnost
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
ho	on	k3xPp3gMnSc4	on
utvrdila	utvrdit	k5eAaPmAgFnS	utvrdit
v	v	k7c6	v
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
jinověrcům	jinověrec	k1gMnPc3	jinověrec
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jednat	jednat	k5eAaImF	jednat
rázně	rázně	k6eAd1	rázně
<g/>
,	,	kIx,	,
a	a	k8xC	a
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
politické	politický	k2eAgInPc4d1	politický
kroky	krok	k1gInPc4	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgInS	být
generálním	generální	k2eAgInSc7d1	generální
sněmem	sněm	k1gInSc7	sněm
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
sesazeného	sesazený	k2eAgMnSc4d1	sesazený
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgMnSc7d1	nový
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Zimní	zimní	k2eAgMnSc1d1	zimní
král	král	k1gMnSc1	král
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stál	stát	k5eAaImAgInS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Protestantské	protestantský	k2eAgFnSc2d1	protestantská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
===	===	k?	===
</s>
</p>
<p>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
satisfakci	satisfakce	k1gFnSc4	satisfakce
obdržel	obdržet	k5eAaPmAgMnS	obdržet
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgInS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
ROk	rok	k1gInSc1	rok
?	?	kIx.	?
</s>
<s>
říšskými	říšský	k2eAgMnPc7d1	říšský
kurfiřty	kurfiřt	k1gMnPc7	kurfiřt
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
a	a	k8xC	a
korunován	korunovat	k5eAaBmNgMnS	korunovat
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
vládl	vládnout	k5eAaImAgInS	vládnout
pouze	pouze	k6eAd1	pouze
sjednoceným	sjednocený	k2eAgFnPc3d1	sjednocená
rakouským	rakouský	k2eAgFnPc3d1	rakouská
zemím	zem	k1gFnPc3	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neovládal	ovládat	k5eNaImAgMnS	ovládat
žádnou	žádný	k3yNgFnSc4	žádný
z	z	k7c2	z
královských	královský	k2eAgFnPc2d1	královská
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
kurfiřtským	kurfiřtský	k2eAgInSc7d1	kurfiřtský
hlasem	hlas	k1gInSc7	hlas
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebylo	být	k5eNaImAgNnS	být
úplně	úplně	k6eAd1	úplně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
tímto	tento	k3xDgInSc7	tento
hlasem	hlas	k1gInSc7	hlas
disponuje	disponovat	k5eAaBmIp3nS	disponovat
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
stavové	stavový	k2eAgNnSc4d1	stavové
považovali	považovat	k5eAaImAgMnP	považovat
sesazení	sesazení	k1gNnSc4	sesazení
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
za	za	k7c4	za
legální	legální	k2eAgInSc4d1	legální
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
vyslali	vyslat	k5eAaPmAgMnP	vyslat
do	do	k7c2	do
Frankfurtu	Frankfurt	k1gInSc2	Frankfurt
své	svůj	k3xOyFgNnSc4	svůj
poselstvo	poselstvo	k1gNnSc4	poselstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgMnS	být
sesazen	sesadit	k5eAaPmNgMnS	sesadit
podle	podle	k7c2	podle
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
hlas	hlas	k1gInSc1	hlas
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
komu	kdo	k3yInSc3	kdo
hlas	hlas	k1gInSc1	hlas
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
,	,	kIx,	,
a	a	k8xC	a
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
již	již	k9	již
sedmý	sedmý	k4xOgInSc1	sedmý
hlas	hlas	k1gInSc1	hlas
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
zajímavostí	zajímavost	k1gFnSc7	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
pověst	pověst	k1gFnSc1	pověst
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
stižen	stihnout	k5eAaPmNgInS	stihnout
mrtvicí	mrtvice	k1gFnSc7	mrtvice
<g/>
,	,	kIx,	,
ochrnul	ochrnout	k5eAaPmAgMnS	ochrnout
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
oslepl	oslepnout	k5eAaPmAgMnS	oslepnout
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
oko	oko	k1gNnSc4	oko
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potlačení	potlačení	k1gNnSc1	potlačení
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
===	===	k?	===
</s>
</p>
<p>
<s>
Cestou	cesta	k1gFnSc7	cesta
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
tajnou	tajný	k2eAgFnSc4d1	tajná
dohodu	dohoda	k1gFnSc4	dohoda
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
příbuzným	příbuzný	k2eAgMnSc7d1	příbuzný
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
I.	I.	kA	I.
Bavorským	bavorský	k2eAgFnPc3d1	bavorská
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
polských	polský	k2eAgMnPc2d1	polský
a	a	k8xC	a
španělských	španělský	k2eAgMnPc2d1	španělský
katolických	katolický	k2eAgMnPc2d1	katolický
spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
již	již	k6eAd1	již
v	v	k7c6	v
září	září	k1gNnSc6	září
podařilo	podařit	k5eAaPmAgNnS	podařit
uklidnit	uklidnit	k5eAaPmF	uklidnit
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
rakouských	rakouský	k2eAgFnPc6d1	rakouská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odměna	odměna	k1gFnSc1	odměna
za	za	k7c4	za
pomoc	pomoc	k1gFnSc4	pomoc
byly	být	k5eAaImAgInP	být
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
zastaveny	zastaven	k2eAgInPc4d1	zastaven
Horní	horní	k2eAgInPc4d1	horní
Rakousy	Rakousy	k1gInPc4	Rakousy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
pak	pak	k6eAd1	pak
porazili	porazit	k5eAaPmAgMnP	porazit
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
české	český	k2eAgNnSc4d1	české
stavovské	stavovský	k2eAgNnSc4d1	Stavovské
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
nepříliš	příliš	k6eNd1	příliš
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
opustil	opustit	k5eAaPmAgMnS	opustit
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgInSc1d1	falcký
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgInS	moct
opět	opět	k6eAd1	opět
ujmout	ujmout	k5eAaPmF	ujmout
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Místodržícím	místodržící	k1gMnPc3	místodržící
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Karla	Karel	k1gMnSc2	Karel
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
a	a	k8xC	a
zrušil	zrušit	k5eAaPmAgMnS	zrušit
Rudolfův	Rudolfův	k2eAgInSc4d1	Rudolfův
Majestát	majestát	k1gInSc4	majestát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
jednoduše	jednoduše	k6eAd1	jednoduše
přestřihl	přestřihnout	k5eAaPmAgMnS	přestřihnout
nůžkami	nůžky	k1gFnPc7	nůžky
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
vlna	vlna	k1gFnSc1	vlna
zatýkání	zatýkání	k1gNnSc2	zatýkání
<g/>
,	,	kIx,	,
konfiskací	konfiskace	k1gFnPc2	konfiskace
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
tvrdých	tvrdý	k2eAgInPc2d1	tvrdý
trestů	trest	k1gInPc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1621	[number]	k4	1621
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
vůdce	vůdce	k1gMnPc4	vůdce
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1622	[number]	k4	1622
vydal	vydat	k5eAaPmAgInS	vydat
tzv.	tzv.	kA	tzv.
generální	generální	k2eAgInSc4d1	generální
pardon	pardon	k1gInSc4	pardon
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
omilostnil	omilostnit	k5eAaPmAgMnS	omilostnit
další	další	k2eAgMnPc4d1	další
účastníky	účastník	k1gMnPc4	účastník
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Milost	milost	k1gFnSc1	milost
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
zachování	zachování	k1gNnSc2	zachování
osobních	osobní	k2eAgNnPc2d1	osobní
práv	právo	k1gNnPc2	právo
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
a	a	k8xC	a
půl	půl	k6eAd1	půl
nastalo	nastat	k5eAaPmAgNnS	nastat
další	další	k2eAgNnSc1d1	další
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
zabavování	zabavování	k1gNnSc1	zabavování
majetku	majetek	k1gInSc2	majetek
české	český	k2eAgFnSc2d1	Česká
protestantské	protestantský	k2eAgFnSc2d1	protestantská
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
královských	královský	k2eAgNnPc2d1	královské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následně	následně	k6eAd1	následně
výhodně	výhodně	k6eAd1	výhodně
získaly	získat	k5eAaPmAgInP	získat
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
,	,	kIx,	,
katolické	katolický	k2eAgInPc1d1	katolický
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
cizí	cizí	k2eAgInPc4d1	cizí
šlechtické	šlechtický	k2eAgInPc4d1	šlechtický
rody	rod	k1gInPc4	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potlačení	potlačení	k1gNnSc1	potlačení
povstání	povstání	k1gNnSc2	povstání
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nečekanou	čekaný	k2eNgFnSc4d1	nečekaná
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
rekatolizačních	rekatolizační	k2eAgFnPc2d1	rekatolizační
snah	snaha	k1gFnPc2	snaha
a	a	k8xC	a
provedení	provedení	k1gNnSc6	provedení
kroků	krok	k1gInPc2	krok
omezujících	omezující	k2eAgInPc2d1	omezující
moc	moc	k1gFnSc4	moc
šlechty	šlechta	k1gFnSc2	šlechta
v	v	k7c6	v
přímo	přímo	k6eAd1	přímo
ovládaných	ovládaný	k2eAgFnPc6d1	ovládaná
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obnovené	obnovený	k2eAgNnSc4d1	obnovené
zřízení	zřízení	k1gNnSc4	zřízení
zemské	zemský	k2eAgFnSc2d1	zemská
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
povstání	povstání	k1gNnSc2	povstání
začal	začít	k5eAaPmAgInS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
rekatolizační	rekatolizační	k2eAgFnSc4d1	rekatolizační
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
upevňovat	upevňovat	k5eAaImF	upevňovat
centrální	centrální	k2eAgFnSc4d1	centrální
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
začlenění	začlenění	k1gNnSc4	začlenění
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
do	do	k7c2	do
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
době	doba	k1gFnSc6	doba
válečného	válečný	k2eAgNnSc2d1	válečné
příměří	příměří	k1gNnSc2	příměří
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1622	[number]	k4	1622
a	a	k8xC	a
1623	[number]	k4	1623
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
několik	několik	k4yIc4	několik
nařízení	nařízení	k1gNnPc2	nařízení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
dílčí	dílčí	k2eAgNnPc1d1	dílčí
opatření	opatření	k1gNnPc1	opatření
nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgNnP	být
zahrnuta	zahrnout	k5eAaPmNgNnP	zahrnout
do	do	k7c2	do
jednotného	jednotný	k2eAgInSc2d1	jednotný
právního	právní	k2eAgInSc2d1	právní
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1627	[number]	k4	1627
vydal	vydat	k5eAaPmAgMnS	vydat
tzv.	tzv.	kA	tzv.
Obnovené	obnovený	k2eAgNnSc1d1	obnovené
zřízení	zřízení	k1gNnSc1	zřízení
zemské	zemský	k2eAgNnSc1d1	zemské
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
České	český	k2eAgNnSc4d1	české
království	království	k1gNnSc4	království
za	za	k7c4	za
dědičné	dědičný	k2eAgInPc4d1	dědičný
v	v	k7c6	v
Habsburské	habsburský	k2eAgFnSc6d1	habsburská
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
zaváděla	zavádět	k5eAaImAgFnS	zavádět
absolutismus	absolutismus	k1gInSc4	absolutismus
namísto	namísto	k7c2	namísto
dosavadního	dosavadní	k2eAgNnSc2d1	dosavadní
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
zřízení	zřízení	k1gNnSc2	zřízení
(	(	kIx(	(
<g/>
zemské	zemský	k2eAgInPc1d1	zemský
sněmy	sněm	k1gInPc1	sněm
ztratily	ztratit	k5eAaPmAgInP	ztratit
právo	právo	k1gNnSc4	právo
volby	volba	k1gFnSc2	volba
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zrovnoprávnila	zrovnoprávnit	k5eAaPmAgFnS	zrovnoprávnit
němčinu	němčina	k1gFnSc4	němčina
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
v	v	k7c6	v
úředním	úřední	k2eAgInSc6d1	úřední
styku	styk	k1gInSc6	styk
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1615	[number]	k4	1615
byla	být	k5eAaImAgFnS	být
jednacím	jednací	k2eAgInSc7d1	jednací
jazykem	jazyk	k1gInSc7	jazyk
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
pouze	pouze	k6eAd1	pouze
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
jako	jako	k9	jako
jediné	jediný	k2eAgNnSc4d1	jediné
povolené	povolený	k2eAgNnSc4d1	povolené
katolické	katolický	k2eAgNnSc4d1	katolické
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgInS	vydat
stejný	stejný	k2eAgInSc1d1	stejný
zákoník	zákoník	k1gInSc1	zákoník
i	i	k9	i
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovení	ustanovení	k1gNnSc1	ustanovení
Obnoveného	obnovený	k2eAgNnSc2d1	obnovené
zřízení	zřízení	k1gNnSc2	zřízení
zemského	zemský	k2eAgNnSc2d1	zemské
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rekatolizace	rekatolizace	k1gFnSc2	rekatolizace
a	a	k8xC	a
emigrace	emigrace	k1gFnSc2	emigrace
českých	český	k2eAgMnPc2d1	český
protestantů	protestant	k1gMnPc2	protestant
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
protireformační	protireformační	k2eAgFnSc2d1	protireformační
politiky	politika	k1gFnSc2	politika
musela	muset	k5eAaImAgFnS	muset
zemi	zem	k1gFnSc3	zem
opustit	opustit	k5eAaPmF	opustit
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
české	český	k2eAgFnSc2d1	Česká
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
přestoupit	přestoupit	k5eAaPmF	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Stránský	Stránský	k1gMnSc1	Stránský
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Skála	Skála	k1gMnSc1	Skála
ze	z	k7c2	z
Zhoře	Zhoř	k1gFnSc2	Zhoř
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
znamenalo	znamenat	k5eAaImAgNnS	znamenat
postupný	postupný	k2eAgInSc4d1	postupný
kulturní	kulturní	k2eAgInSc4d1	kulturní
a	a	k8xC	a
jazykový	jazykový	k2eAgInSc4d1	jazykový
úpadek	úpadek	k1gInSc4	úpadek
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
stavů	stav	k1gInPc2	stav
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přestoupí	přestoupit	k5eAaPmIp3nS	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
emigrují	emigrovat	k5eAaBmIp3nP	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
Poddaný	poddaný	k2eAgInSc1d1	poddaný
lid	lid	k1gInSc1	lid
však	však	k9	však
byl	být	k5eAaImAgInS	být
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
nucen	nutit	k5eAaImNgMnS	nutit
přestoupit	přestoupit	k5eAaPmF	přestoupit
povinně	povinně	k6eAd1	povinně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
podobných	podobný	k2eAgInPc2d1	podobný
příslibů	příslib	k1gInPc2	příslib
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
dal	dát	k5eAaPmAgInS	dát
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
zvolen	zvolit	k5eAaPmNgMnS	zvolit
také	také	k6eAd1	také
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Uherské	uherský	k2eAgNnSc1d1	Uherské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
ovládali	ovládat	k5eAaImAgMnP	ovládat
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
královské	královský	k2eAgFnPc1d1	královská
Uhry	Uhry	k1gFnPc1	Uhry
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
rozkládaly	rozkládat	k5eAaImAgFnP	rozkládat
na	na	k7c6	na
území	území	k1gNnSc6	území
Horní	horní	k2eAgFnSc2d1	horní
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
úzkého	úzký	k2eAgInSc2d1	úzký
pásu	pás	k1gInSc2	pás
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
Uher	uher	k1gInSc4	uher
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
turecké	turecký	k2eAgFnSc2d1	turecká
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
samostatné	samostatný	k2eAgNnSc1d1	samostatné
Sedmihradské	sedmihradský	k2eAgNnSc1d1	sedmihradské
knížectví	knížectví	k1gNnSc1	knížectví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
politicky	politicky	k6eAd1	politicky
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
častými	častý	k2eAgInPc7d1	častý
válečnými	válečný	k2eAgInPc7d1	válečný
konflikty	konflikt	k1gInPc7	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
propuknutí	propuknutí	k1gNnSc6	propuknutí
českého	český	k2eAgInSc2d1	český
stavovské	stavovský	k2eAgInPc1d1	stavovský
povstání	povstání	k1gNnSc6	povstání
využil	využít	k5eAaPmAgMnS	využít
situace	situace	k1gFnPc4	situace
sedmihradský	sedmihradský	k2eAgMnSc1d1	sedmihradský
kníže	kníže	k1gMnSc1	kníže
Gabriel	Gabriel	k1gMnSc1	Gabriel
Bethlen	Bethlen	k2eAgMnSc1d1	Bethlen
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
snil	snít	k5eAaImAgMnS	snít
o	o	k7c4	o
znovusjednocení	znovusjednocení	k1gNnSc4	znovusjednocení
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
důležitým	důležitý	k2eAgMnSc7d1	důležitý
spojencem	spojenec	k1gMnSc7	spojenec
protestantů	protestant	k1gMnPc2	protestant
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Protestantské	protestantský	k2eAgInPc1d1	protestantský
stavy	stav	k1gInPc1	stav
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
dojednaly	dojednat	k5eAaPmAgInP	dojednat
spojenectví	spojenectví	k1gNnSc4	spojenectví
mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
vzbouřenci	vzbouřenec	k1gMnPc7	vzbouřenec
a	a	k8xC	a
Bethlenem	Bethlen	k1gInSc7	Bethlen
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1619	[number]	k4	1619
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
obsadit	obsadit	k5eAaPmF	obsadit
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnPc4d1	celá
Horní	horní	k2eAgFnPc4d1	horní
Uhry	Uhry	k1gFnPc4	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
moravskými	moravský	k2eAgInPc7d1	moravský
a	a	k8xC	a
českými	český	k2eAgInPc7d1	český
oddíly	oddíl	k1gInPc7	oddíl
následně	následně	k6eAd1	následně
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
<g/>
,	,	kIx,	,
udržel	udržet	k5eAaPmAgMnS	udržet
si	se	k3xPyFc3	se
v	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
příměří	příměří	k1gNnSc6	příměří
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
dobytými	dobytý	k2eAgFnPc7d1	dobytá
oblastmi	oblast	k1gFnPc7	oblast
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1620	[number]	k4	1620
nechal	nechat	k5eAaPmAgInS	nechat
uherským	uherský	k2eAgInSc7d1	uherský
sněmem	sněm	k1gInSc7	sněm
zvolit	zvolit	k5eAaPmF	zvolit
za	za	k7c4	za
krále	král	k1gMnPc4	král
namísto	namísto	k7c2	namísto
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
by	by	kYmCp3nS	by
bývala	bývat	k5eAaImAgFnS	bývat
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
i	i	k9	i
turecká	turecký	k2eAgFnSc1d1	turecká
Porta	porta	k1gFnSc1	porta
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
orgán	orgán	k1gInSc1	orgán
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdá	vzdát	k5eAaPmIp3nS	vzdát
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
nevýhodné	výhodný	k2eNgNnSc1d1	nevýhodné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
poraženo	poražen	k2eAgNnSc4d1	poraženo
stavovské	stavovský	k2eAgNnSc4d1	Stavovské
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mezi	mezi	k7c7	mezi
uherskou	uherský	k2eAgFnSc7d1	uherská
šlechtou	šlechta	k1gFnSc7	šlechta
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
paniku	panika	k1gFnSc4	panika
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
spěchala	spěchat	k5eAaImAgFnS	spěchat
ujistit	ujistit	k5eAaPmF	ujistit
jej	on	k3xPp3gMnSc4	on
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
loajalitě	loajalita	k1gFnSc6	loajalita
<g/>
.	.	kIx.	.
</s>
<s>
Bethlen	Bethlen	k2eAgMnSc1d1	Bethlen
zůstal	zůstat	k5eAaPmAgMnS	zůstat
osamocen	osamocen	k2eAgMnSc1d1	osamocen
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
protiútoku	protiútok	k1gInSc2	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1622	[number]	k4	1622
byl	být	k5eAaImAgInS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
mikulovský	mikulovský	k2eAgInSc1d1	mikulovský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
se	se	k3xPyFc4	se
Bethlen	Bethlen	k2eAgInSc1d1	Bethlen
vzdal	vzdát	k5eAaPmAgInS	vzdát
nároků	nárok	k1gInPc2	nárok
na	na	k7c4	na
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
dobytých	dobytý	k2eAgNnPc2d1	dobyté
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
sedmi	sedm	k4xCc2	sedm
stolic	stolice	k1gFnPc2	stolice
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Horních	horní	k2eAgFnPc2d1	horní
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
vévodství	vévodství	k1gNnPc2	vévodství
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
míru	mír	k1gInSc2	mír
pak	pak	k6eAd1	pak
ujistil	ujistit	k5eAaPmAgMnS	ujistit
uherské	uherský	k2eAgInPc4d1	uherský
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
jejich	jejich	k3xOp3gNnPc4	jejich
privilegia	privilegium	k1gNnPc4	privilegium
a	a	k8xC	a
náboženské	náboženský	k2eAgFnPc4d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
1637	[number]	k4	1637
<g/>
)	)	kIx)	)
nemusel	muset	k5eNaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
útokům	útok	k1gInPc3	útok
sedmihradských	sedmihradský	k2eAgMnPc2d1	sedmihradský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byla	být	k5eAaImAgFnS	být
protestantskými	protestantský	k2eAgMnPc7d1	protestantský
spojenci	spojenec	k1gMnPc7	spojenec
vybízena	vybízen	k2eAgFnSc1d1	vybízena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozšíření	rozšíření	k1gNnSc1	rozšíření
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
napadla	napadnout	k5eAaPmAgFnS	napadnout
císařská	císařský	k2eAgFnSc1d1	císařská
a	a	k8xC	a
španělská	španělský	k2eAgFnSc1d1	španělská
vojska	vojsko	k1gNnSc2	vojsko
porýnskou	porýnský	k2eAgFnSc4d1	porýnská
Falc	Falc	k1gFnSc4	Falc
(	(	kIx(	(
<g/>
říšské	říšský	k2eAgNnSc1d1	říšské
panství	panství	k1gNnSc1	panství
uprchlého	uprchlý	k2eAgMnSc2d1	uprchlý
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Fridricha	Fridrich	k1gMnSc2	Fridrich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1622	[number]	k4	1622
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
císař	císař	k1gMnSc1	císař
získal	získat	k5eAaPmAgMnS	získat
zpět	zpět	k6eAd1	zpět
zastavené	zastavený	k2eAgInPc4d1	zastavený
Horní	horní	k2eAgInPc4d1	horní
Rakousy	Rakousy	k1gInPc4	Rakousy
<g/>
,	,	kIx,	,
odebral	odebrat	k5eAaPmAgMnS	odebrat
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
bulou	bula	k1gFnSc7	bula
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Fridrichovi	Fridrichův	k2eAgMnPc1d1	Fridrichův
kurfiřtskou	kurfiřtský	k2eAgFnSc7d1	kurfiřtská
hodnost	hodnost	k1gFnSc4	hodnost
(	(	kIx(	(
<g/>
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
volit	volit	k5eAaImF	volit
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
privilegii	privilegium	k1gNnPc7	privilegium
<g/>
)	)	kIx)	)
a	a	k8xC	a
převedl	převést	k5eAaPmAgMnS	převést
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
bavorského	bavorský	k2eAgMnSc4d1	bavorský
vévodu	vévoda	k1gMnSc4	vévoda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
stal	stát	k5eAaPmAgMnS	stát
regentem	regens	k1gMnSc7	regens
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
odpor	odpor	k1gInSc4	odpor
u	u	k7c2	u
protestantských	protestantský	k2eAgNnPc2d1	protestantské
knížat	kníže	k1gNnPc2	kníže
a	a	k8xC	a
zformování	zformování	k1gNnSc1	zformování
protikatolické	protikatolický	k2eAgFnSc2d1	protikatolická
koalice	koalice	k1gFnSc2	koalice
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
této	tento	k3xDgFnSc2	tento
koalice	koalice	k1gFnSc2	koalice
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
Kristián	Kristián	k1gMnSc1	Kristián
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
říšským	říšský	k2eAgNnSc7d1	říšské
knížetem	kníže	k1gNnSc7wR	kníže
v	v	k7c6	v
Šlesvicku	Šlesvicko	k1gNnSc6	Šlesvicko
a	a	k8xC	a
Holštýnsku	Holštýnsko	k1gNnSc6	Holštýnsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
===	===	k?	===
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
přijal	přijmout	k5eAaPmAgMnS	přijmout
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
Albrechta	Albrecht	k1gMnSc2	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
půjčoval	půjčovat	k5eAaImAgInS	půjčovat
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
postavení	postavení	k1gNnSc4	postavení
armády	armáda	k1gFnSc2	armáda
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
císařských	císařský	k2eAgFnPc2d1	císařská
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
si	se	k3xPyFc3	se
císaře	císař	k1gMnSc4	císař
zavázal	zavázat	k5eAaPmAgInS	zavázat
a	a	k8xC	a
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
ekonomicky	ekonomicky	k6eAd1	ekonomicky
profitoval	profitovat	k5eAaBmAgMnS	profitovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
schopný	schopný	k2eAgMnSc1d1	schopný
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
svedl	svést	k5eAaPmAgInS	svést
řadu	řada	k1gFnSc4	řada
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
bitev	bitva	k1gFnPc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
zásluhou	zásluha	k1gFnSc7	zásluha
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
bojů	boj	k1gInPc2	boj
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
za	za	k7c2	za
Ferdinandova	Ferdinandův	k2eAgInSc2d1	Ferdinandův
života	život	k1gInSc2	život
vedla	vést	k5eAaImAgFnS	vést
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Restituční	restituční	k2eAgInSc1d1	restituční
edikt	edikt	k1gInSc1	edikt
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
úspěchů	úspěch	k1gInPc2	úspěch
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
rekatolizaci	rekatolizace	k1gFnSc4	rekatolizace
na	na	k7c6	na
území	území	k1gNnSc6	území
celé	celý	k2eAgFnSc2d1	celá
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
vydal	vydat	k5eAaPmAgInS	vydat
tzv.	tzv.	kA	tzv.
restituční	restituční	k2eAgInSc4d1	restituční
edikt	edikt	k1gInSc4	edikt
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
požadoval	požadovat	k5eAaImAgMnS	požadovat
navrácení	navrácení	k1gNnSc4	navrácení
náboženských	náboženský	k2eAgInPc2d1	náboženský
a	a	k8xC	a
územních	územní	k2eAgInPc2d1	územní
poměrů	poměr	k1gInPc2	poměr
před	před	k7c4	před
rok	rok	k1gInSc4	rok
1555	[number]	k4	1555
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
uzavřen	uzavřen	k2eAgInSc4d1	uzavřen
tzv.	tzv.	kA	tzv.
augšpurský	augšpurský	k2eAgInSc4d1	augšpurský
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozpoutání	rozpoutání	k1gNnSc3	rozpoutání
výpadů	výpad	k1gInPc2	výpad
protestantského	protestantský	k2eAgNnSc2d1	protestantské
Švédska	Švédsko	k1gNnSc2	Švédsko
proti	proti	k7c3	proti
Habsburkům	Habsburk	k1gInPc3	Habsburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zavraždění	zavraždění	k1gNnSc4	zavraždění
Albrechta	Albrecht	k1gMnSc2	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
===	===	k?	===
</s>
</p>
<p>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
postavení	postavení	k1gNnSc3	postavení
a	a	k8xC	a
schopnostem	schopnost	k1gFnPc3	schopnost
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pochopitelně	pochopitelně	k6eAd1	pochopitelně
budilo	budit	k5eAaImAgNnS	budit
závist	závist	k1gFnSc4	závist
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
odpůrci	odpůrce	k1gMnPc7	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1634	[number]	k4	1634
uvěřil	uvěřit	k5eAaPmAgMnS	uvěřit
zprávám	zpráva	k1gFnPc3	zpráva
o	o	k7c6	o
údajné	údajný	k2eAgFnSc6d1	údajná
Valdštejnově	Valdštejnův	k2eAgFnSc6d1	Valdštejnova
zradě	zrada	k1gFnSc6	zrada
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ho	on	k3xPp3gMnSc4	on
proto	proto	k8xC	proto
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
zavraždit	zavraždit	k5eAaPmF	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
duši	duše	k1gFnSc4	duše
frýdlantského	frýdlantský	k2eAgMnSc2d1	frýdlantský
vévody	vévoda	k1gMnSc2	vévoda
nechal	nechat	k5eAaPmAgMnS	nechat
sloužit	sloužit	k5eAaImF	sloužit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
tisíce	tisíc	k4xCgInPc4	tisíc
mší	mše	k1gFnPc2	mše
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Postoupení	postoupení	k1gNnSc4	postoupení
Lužice	Lužice	k1gFnSc2	Lužice
Sasku	Saska	k1gFnSc4	Saska
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
přízeň	přízeň	k1gFnSc4	přízeň
saského	saský	k2eAgMnSc2d1	saský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Jana	Jan	k1gMnSc2	Jan
Jiřího	Jiří	k1gMnSc2	Jiří
postoupil	postoupit	k5eAaPmAgInS	postoupit
roku	rok	k1gInSc2	rok
1635	[number]	k4	1635
Sasku	Sasko	k1gNnSc3	Sasko
území	území	k1gNnSc2	území
Horní	horní	k2eAgFnSc2d1	horní
i	i	k8xC	i
Dolní	dolní	k2eAgFnSc2d1	dolní
Lužice	Lužice	k1gFnSc2	Lužice
uzavřením	uzavření	k1gNnSc7	uzavření
separátního	separátní	k2eAgInSc2d1	separátní
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pražský	pražský	k2eAgInSc1d1	pražský
mír	mír	k1gInSc1	mír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
trvalou	trvalý	k2eAgFnSc4d1	trvalá
ztrátu	ztráta	k1gFnSc4	ztráta
tohoto	tento	k3xDgNnSc2	tento
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
prosadil	prosadit	k5eAaPmAgMnS	prosadit
korunovaci	korunovace	k1gFnSc4	korunovace
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
1636	[number]	k4	1636
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1637	[number]	k4	1637
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
svoji	svůj	k3xOyFgFnSc4	svůj
koncepci	koncepce	k1gFnSc4	koncepce
centralizované	centralizovaný	k2eAgFnSc2d1	centralizovaná
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
zanechal	zanechat	k5eAaPmAgInS	zanechat
však	však	k9	však
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
v	v	k7c6	v
ničivé	ničivý	k2eAgFnSc6d1	ničivá
smršti	smršť	k1gFnSc6	smršť
bojů	boj	k1gInPc2	boj
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
zdaleka	zdaleka	k6eAd1	zdaleka
nebyla	být	k5eNaImAgFnS	být
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mauzoleu	mauzoleum	k1gNnSc6	mauzoleum
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sám	sám	k3xTgMnSc1	sám
založil	založit	k5eAaPmAgMnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
srdce	srdce	k1gNnSc1	srdce
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
Hrobce	hrobka	k1gFnSc6	hrobka
srdcí	srdce	k1gNnPc2	srdce
v	v	k7c6	v
augustiniánském	augustiniánský	k2eAgInSc6d1	augustiniánský
kostele	kostel	k1gInSc6	kostel
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
ve	v	k7c6	v
Vévodské	vévodský	k2eAgFnSc6d1	vévodská
hrobce	hrobka	k1gFnSc6	hrobka
svatoštěpánské	svatoštěpánský	k2eAgFnSc2d1	Svatoštěpánská
katedrály	katedrála	k1gFnSc2	katedrála
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgNnPc1d1	osobní
charakteristika	charakteristikon	k1gNnPc1	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
historik	historik	k1gMnSc1	historik
Ernest	Ernest	k1gMnSc1	Ernest
Denis	Denisa	k1gFnPc2	Denisa
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
měl	mít	k5eAaImAgInS	mít
všecky	všecek	k3xTgFnPc4	všecek
vlastnosti	vlastnost	k1gFnPc4	vlastnost
výborného	výborný	k2eAgMnSc4d1	výborný
venkovského	venkovský	k2eAgMnSc4d1	venkovský
faráře	farář	k1gMnSc4	farář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
veselý	veselý	k2eAgInSc1d1	veselý
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgFnSc2d1	dobrá
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
omezeného	omezený	k2eAgNnSc2d1	omezené
vědění	vědění	k1gNnSc2	vědění
<g/>
,	,	kIx,	,
bázlivý	bázlivý	k2eAgInSc1d1	bázlivý
a	a	k8xC	a
úzkostlivý	úzkostlivý	k2eAgInSc1d1	úzkostlivý
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejkrvavějších	krvavý	k2eAgFnPc2d3	nejkrvavější
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
kapku	kapka	k1gFnSc4	kapka
žluči	žluč	k1gFnSc2	žluč
...	...	k?	...
Jsa	být	k5eAaImSgInS	být
povahy	povaha	k1gFnSc2	povaha
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
ctnostný	ctnostný	k2eAgInSc1d1	ctnostný
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
život	život	k1gInSc1	život
jednotvárný	jednotvárný	k2eAgInSc1d1	jednotvárný
a	a	k8xC	a
pokojný	pokojný	k2eAgMnSc1d1	pokojný
uprostřed	uprostřed	k7c2	uprostřed
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
něžný	něžný	k2eAgInSc4d1	něžný
<g/>
,	,	kIx,	,
laskavý	laskavý	k2eAgInSc4d1	laskavý
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
sluhům	sluha	k1gMnPc3	sluha
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
rád	rád	k6eAd1	rád
odpouštěl	odpouštět	k5eAaImAgInS	odpouštět
jejich	jejich	k3xOp3gInPc4	jejich
poklesky	poklesek	k1gInPc4	poklesek
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k6eAd1	okolo
sebe	sebe	k3xPyFc4	sebe
rád	rád	k6eAd1	rád
viděl	vidět	k5eAaImAgMnS	vidět
tváře	tvář	k1gFnPc4	tvář
spokojené	spokojený	k2eAgFnPc4d1	spokojená
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
štědrý	štědrý	k2eAgInSc1d1	štědrý
<g/>
,	,	kIx,	,
ba	ba	k9	ba
až	až	k9	až
marnotratný	marnotratný	k2eAgMnSc1d1	marnotratný
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
charakteristiku	charakteristika	k1gFnSc4	charakteristika
doplnil	doplnit	k5eAaPmAgMnS	doplnit
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
Josef	Josef	k1gMnSc1	Josef
Pekař	Pekař	k1gMnSc1	Pekař
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Císař	Císař	k1gMnSc1	Císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zvítěziv	zvítězit	k5eAaPmDgInS	zvítězit
potrestal	potrestat	k5eAaPmAgInS	potrestat
tak	tak	k9	tak
hrozně	hrozně	k6eAd1	hrozně
odboj	odboj	k1gInSc1	odboj
stavovský	stavovský	k2eAgInSc1d1	stavovský
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
osobně	osobně	k6eAd1	osobně
panovník	panovník	k1gMnSc1	panovník
malého	malý	k2eAgInSc2d1	malý
významu	význam	k1gInSc2	význam
a	a	k8xC	a
nadání	nadání	k1gNnSc4	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jeho	jeho	k3xOp3gFnSc1	jeho
pevná	pevný	k2eAgFnSc1d1	pevná
víra	víra	k1gFnSc1	víra
naplňovala	naplňovat	k5eAaImAgFnS	naplňovat
jej	on	k3xPp3gInSc4	on
důvěrou	důvěra	k1gFnSc7	důvěra
v	v	k7c4	v
budoucnost	budoucnost	k1gFnSc4	budoucnost
i	i	k9	i
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
nebezpečných	bezpečný	k2eNgFnPc6d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Nestaral	starat	k5eNaImAgMnS	starat
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
o	o	k7c4	o
státní	státní	k2eAgFnPc4d1	státní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
cele	cele	k6eAd1	cele
jeho	jeho	k3xOp3gMnPc1	jeho
důvěrníci	důvěrník	k1gMnPc1	důvěrník
političtí	politický	k2eAgMnPc1d1	politický
i	i	k8xC	i
duchovní	duchovní	k2eAgMnPc1d1	duchovní
<g/>
,	,	kIx,	,
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
zejména	zejména	k6eAd1	zejména
zpovědníci	zpovědník	k1gMnPc1	zpovědník
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
Nejraději	rád	k6eAd3	rád
hověl	hovět	k5eAaImAgMnS	hovět
lovu	lov	k1gInSc3	lov
a	a	k8xC	a
jiným	jiný	k2eAgFnPc3d1	jiná
zábavám	zábava	k1gFnPc3	zábava
<g/>
,	,	kIx,	,
vydávaje	vydávat	k5eAaImSgInS	vydávat
peníze	peníz	k1gInPc4	peníz
nešetrně	šetrně	k6eNd1	šetrně
a	a	k8xC	a
neznaje	neznat	k5eAaImSgInS	neznat
míry	míra	k1gFnSc2	míra
v	v	k7c6	v
rozdílení	rozdílení	k1gNnSc6	rozdílení
statků	statek	k1gInPc2	statek
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
důvěřoval	důvěřovat	k5eAaImAgInS	důvěřovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Rozhodování	rozhodování	k1gNnSc1	rozhodování
o	o	k7c6	o
důležitých	důležitý	k2eAgFnPc6d1	důležitá
věcech	věc	k1gFnPc6	věc
nechával	nechávat	k5eAaImAgMnS	nechávat
na	na	k7c4	na
svých	svůj	k3xOyFgNnPc6	svůj
schopných	schopný	k2eAgMnPc6d1	schopný
rádcích	rádce	k1gMnPc6	rádce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
skutečnými	skutečný	k2eAgMnPc7d1	skutečný
vládci	vládce	k1gMnPc7	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
měli	mít	k5eAaImAgMnP	mít
jeho	jeho	k3xOp3gInSc4	jeho
rádce	rádce	k1gMnSc1	rádce
Jan	Jan	k1gMnSc1	Jan
Oldřich	Oldřich	k1gMnSc1	Oldřich
z	z	k7c2	z
Eggenberku	Eggenberk	k1gInSc2	Eggenberk
(	(	kIx(	(
<g/>
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
"	"	kIx"	"
<g/>
neomezený	omezený	k2eNgMnSc1d1	neomezený
pán	pán	k1gMnSc1	pán
císařovy	císařův	k2eAgFnSc2d1	císařova
vůle	vůle	k1gFnSc2	vůle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpovědník	zpovědník	k1gMnSc1	zpovědník
<g/>
,	,	kIx,	,
jezuitský	jezuitský	k2eAgMnSc1d1	jezuitský
páter	páter	k1gMnSc1	páter
Lamormain	Lamormain	k1gMnSc1	Lamormain
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
ze	z	k7c2	z
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
císařovy	císařův	k2eAgFnSc2d1	císařova
hlavy	hlava	k1gFnSc2	hlava
údajně	údajně	k6eAd1	údajně
nebyla	být	k5eNaImAgNnP	být
ani	ani	k8xC	ani
drastická	drastický	k2eAgNnPc1d1	drastické
opatření	opatření	k1gNnPc1	opatření
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
českého	český	k2eAgNnSc2d1	české
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
pro	pro	k7c4	pro
27	[number]	k4	27
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
nespal	spát	k5eNaImAgMnS	spát
a	a	k8xC	a
radil	radit	k5eAaImAgMnS	radit
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
zpovědníkem	zpovědník	k1gMnSc7	zpovědník
<g/>
.	.	kIx.	.
</s>
<s>
Rozsudky	rozsudek	k1gInPc4	rozsudek
nakonec	nakonec	k6eAd1	nakonec
podepsal	podepsat	k5eAaPmAgInS	podepsat
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgMnS	zavázat
vykonat	vykonat	k5eAaPmF	vykonat
kajícnou	kajícný	k2eAgFnSc4d1	kajícná
pouť	pouť	k1gFnSc4	pouť
k	k	k7c3	k
zázračnému	zázračný	k2eAgInSc3d1	zázračný
obrazu	obraz	k1gInSc3	obraz
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
do	do	k7c2	do
Mariazellu	Mariazell	k1gInSc2	Mariazell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postoje	postoj	k1gInPc1	postoj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zastával	zastávat	k5eAaImAgMnS	zastávat
a	a	k8xC	a
které	který	k3yQgFnPc1	který
ovlivňovaly	ovlivňovat	k5eAaImAgFnP	ovlivňovat
jeho	jeho	k3xOp3gFnSc4	jeho
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
vycházely	vycházet	k5eAaImAgFnP	vycházet
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
jeho	jeho	k3xOp3gMnSc2	jeho
jezuitského	jezuitský	k2eAgMnSc2d1	jezuitský
vychovatele	vychovatel	k1gMnSc2	vychovatel
a	a	k8xC	a
zpovědníka	zpovědník	k1gMnSc2	zpovědník
Bartholomea	Bartholomeus	k1gMnSc2	Bartholomeus
Villera	Viller	k1gMnSc2	Viller
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
později	pozdě	k6eAd2	pozdě
mnohokrát	mnohokrát	k6eAd1	mnohokrát
citoval	citovat	k5eAaBmAgMnS	citovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kníže	kníže	k1gMnSc1	kníže
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
tělesné	tělesný	k2eAgNnSc4d1	tělesné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
za	za	k7c4	za
duševní	duševní	k2eAgNnSc4d1	duševní
blaho	blaho	k1gNnSc4	blaho
svých	svůj	k3xOyFgMnPc2	svůj
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
takové	takový	k3xDgInPc4	takový
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
poddaných	poddaná	k1gFnPc2	poddaná
odvrátilo	odvrátit	k5eAaPmAgNnS	odvrátit
od	od	k7c2	od
spásonosného	spásonosný	k2eAgNnSc2d1	spásonosné
učení	učení	k1gNnSc2	učení
římské	římský	k2eAgFnSc2d1	římská
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
provinilo	provinit	k5eAaPmAgNnS	provinit
se	se	k3xPyFc4	se
kacířstvím	kacířství	k1gNnSc7	kacířství
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
cíl	cíl	k1gInSc1	cíl
pravého	pravý	k2eAgNnSc2d1	pravé
knížete	kníže	k1gNnSc2wR	kníže
musí	muset	k5eAaImIp3nP	muset
tedy	tedy	k9	tedy
být	být	k5eAaImF	být
takové	takový	k3xDgNnSc4	takový
kacířství	kacířství	k1gNnSc4	kacířství
potlačit	potlačit	k5eAaPmF	potlačit
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
tím	ten	k3xDgNnSc7	ten
spásu	spása	k1gFnSc4	spása
duší	duše	k1gFnPc2	duše
svých	svůj	k3xOyFgFnPc2	svůj
poddaných	poddaná	k1gFnPc2	poddaná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tak	tak	k6eAd1	tak
nečiní	činit	k5eNaImIp3nS	činit
<g/>
,	,	kIx,	,
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
tím	ten	k3xDgNnSc7	ten
spásu	spása	k1gFnSc4	spása
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
duše	duše	k1gFnPc1	duše
<g/>
.	.	kIx.	.
</s>
<s>
Boží	boží	k2eAgMnSc1d1	boží
oblíbenec	oblíbenec	k1gMnSc1	oblíbenec
musí	muset	k5eAaImIp3nS	muset
vždy	vždy	k6eAd1	vždy
sledovat	sledovat	k5eAaImF	sledovat
Boží	boží	k2eAgInSc4d1	boží
zájem	zájem	k1gInSc4	zájem
<g/>
,	,	kIx,	,
ba	ba	k9	ba
i	i	k9	i
být	být	k5eAaImF	být
připraven	připravit	k5eAaPmNgMnS	připravit
vzít	vzít	k5eAaPmF	vzít
žebráckou	žebrácký	k2eAgFnSc4d1	Žebrácká
hůl	hůl	k1gFnSc4	hůl
než	než	k8xS	než
Boží	božit	k5eAaImIp3nS	božit
věc	věc	k1gFnSc1	věc
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
ohrozit	ohrozit	k5eAaPmF	ohrozit
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
princeznou	princezna	k1gFnSc7	princezna
Marií	Maria	k1gFnSc7	Maria
Annou	Anna	k1gFnSc7	Anna
(	(	kIx(	(
<g/>
1574	[number]	k4	1574
<g/>
–	–	k?	–
<g/>
1616	[number]	k4	1616
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
bavorského	bavorský	k2eAgInSc2d1	bavorský
vévodského	vévodský	k2eAgInSc2d1	vévodský
páru	pár	k1gInSc2	pár
Viléma	Vilém	k1gMnSc2	Vilém
V.	V.	kA	V.
a	a	k8xC	a
Renaty	Renata	k1gFnSc2	Renata
Lotrinské	lotrinský	k2eAgFnSc2d1	lotrinská
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1600	[number]	k4	1600
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
7	[number]	k4	7
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
3	[number]	k4	3
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zemřely	zemřít	k5eAaPmAgFnP	zemřít
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kristýna	Kristýna	k1gFnSc1	Kristýna
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1601	[number]	k4	1601
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
†	†	k?	†
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Karel	Karel	k1gMnSc1	Karel
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
–	–	k?	–
1619	[number]	k4	1619
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1608	[number]	k4	1608
–	–	k?	–
1657	[number]	k4	1657
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
<g/>
∞	∞	k?	∞
1631	[number]	k4	1631
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Španělská	španělský	k2eAgFnSc1d1	španělská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Markéty	Markéta	k1gFnSc2	Markéta
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
</s>
</p>
<p>
<s>
∞	∞	k?	∞
1648	[number]	k4	1648
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Marie	Marie	k1gFnSc1	Marie
Leopoldina	Leopoldina	k1gFnSc1	Leopoldina
Tyrolská	tyrolský	k2eAgFnSc1d1	tyrolská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Leopolda	Leopold	k1gMnSc2	Leopold
V.	V.	kA	V.
a	a	k8xC	a
Klaudie	Klaudie	k1gFnSc2	Klaudie
Toskánské	toskánský	k2eAgFnSc2d1	Toskánská
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Medicejských	Medicejský	k2eAgFnPc2d1	Medicejská
</s>
</p>
<p>
<s>
∞	∞	k?	∞
1651	[number]	k4	1651
princezna	princezna	k1gFnSc1	princezna
Eleonora	Eleonora	k1gFnSc1	Eleonora
Magdalena	Magdalena	k1gFnSc1	Magdalena
Gonzagová	Gonzagový	k2eAgFnSc1d1	Gonzagový
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
vévody	vévoda	k1gMnSc2	vévoda
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
manželky	manželka	k1gFnPc1	manželka
Marie	Maria	k1gFnSc2	Maria
de	de	k?	de
GonzagaMarie	GonzagaMarie	k1gFnSc1	GonzagaMarie
Anna	Anna	k1gFnSc1	Anna
(	(	kIx(	(
<g/>
1610	[number]	k4	1610
–	–	k?	–
1665	[number]	k4	1665
<g/>
)	)	kIx)	)
∞	∞	k?	∞
1635	[number]	k4	1635
bavorský	bavorský	k2eAgMnSc1d1	bavorský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
vévody	vévoda	k1gMnSc2	vévoda
Viléma	Vilém	k1gMnSc2	Vilém
V.	V.	kA	V.
a	a	k8xC	a
Renaty	Renata	k1gFnSc2	Renata
Lotrinské	lotrinský	k2eAgFnSc2d1	lotrinská
</s>
</p>
<p>
<s>
Cecílie	Cecílie	k1gFnSc1	Cecílie
Renata	Renata	k1gFnSc1	Renata
(	(	kIx(	(
<g/>
1611	[number]	k4	1611
–	–	k?	–
1644	[number]	k4	1644
<g/>
)	)	kIx)	)
<g/>
∞	∞	k?	∞
1637	[number]	k4	1637
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Vasa	Vasa	k1gMnSc1	Vasa
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Zikmunda	Zikmund	k1gMnSc2	Zikmund
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
Vilém	Vilém	k1gMnSc1	Vilém
(	(	kIx(	(
<g/>
1614	[number]	k4	1614
–	–	k?	–
1662	[number]	k4	1662
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
místodržitelPo	místodržitelPo	k1gMnSc1	místodržitelPo
smrti	smrt	k1gFnSc2	smrt
Marie	Maria	k1gFnSc2	Maria
Anny	Anna	k1gFnSc2	Anna
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
let	léto	k1gNnPc2	léto
vdovcem	vdovec	k1gMnSc7	vdovec
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
podruhé	podruhé	k6eAd1	podruhé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
svého	svůj	k3xOyFgNnSc2	svůj
vdovectví	vdovectví	k1gNnSc2	vdovectví
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
bránil	bránit	k5eAaImAgMnS	bránit
svým	svůj	k3xOyFgFnPc3	svůj
sexuálním	sexuální	k2eAgFnPc3d1	sexuální
potřebám	potřeba	k1gFnPc3	potřeba
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
oblékal	oblékat	k5eAaImAgMnS	oblékat
žíněné	žíněný	k2eAgNnSc4d1	žíněné
roucho	roucho	k1gNnSc4	roucho
a	a	k8xC	a
bičoval	bičovat	k5eAaImAgMnS	bičovat
se	se	k3xPyFc4	se
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
důtkami	důtka	k1gFnPc7	důtka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
princezna	princezna	k1gFnSc1	princezna
Eleonora	Eleonora	k1gFnSc1	Eleonora
de	de	k?	de
Gonzaga	Gonzaga	k1gFnSc1	Gonzaga
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
vévody	vévoda	k1gMnSc2	vévoda
Vincenta	Vincent	k1gMnSc2	Vincent
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
druhé	druhý	k4xOgFnPc1	druhý
manželky	manželka	k1gFnPc1	manželka
Eleonory	Eleonora	k1gFnSc2	Eleonora
de	de	k?	de
Medici	medik	k1gMnPc1	medik
<g/>
.	.	kIx.	.
</s>
<s>
Sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1622	[number]	k4	1622
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
manželství	manželství	k1gNnSc1	manželství
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc4	vývod
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
16	[number]	k4	16
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Ep	Ep	k1gMnSc1	Ep
<g/>
–	–	k?	–
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
;	;	kIx,	;
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-200-2292-9	[number]	k4	978-80-200-2292-9
(	(	kIx(	(
<g/>
Academia	academia	k1gFnSc1	academia
<g/>
)	)	kIx)	)
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
978-80-7286-215-3	[number]	k4	978-80-7286-215-3
(	(	kIx(	(
<g/>
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
122	[number]	k4	122
<g/>
–	–	k?	–
<g/>
124	[number]	k4	124
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1584	[number]	k4	1584
<g/>
-	-	kIx~	-
<g/>
1620	[number]	k4	1620
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
Habsburkové	Habsburk	k1gMnPc1	Habsburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
496	[number]	k4	496
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
388	[number]	k4	388
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
385	[number]	k4	385
<g/>
-	-	kIx~	-
<g/>
397	[number]	k4	397
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
1618	[number]	k4	1618
<g/>
-	-	kIx~	-
<g/>
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
947	[number]	k4	947
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
RAK	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
tvých	tvůj	k3xOp2gNnPc2	tvůj
křídel	křídlo	k1gNnPc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grafoprint-Neubert	Grafoprint-Neubert	k1gMnSc1	Grafoprint-Neubert
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85785	[number]	k4	85785
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
:	:	kIx,	:
životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
a	a	k8xC	a
Milan	Milan	k1gMnSc1	Milan
Kouřimských	kouřimský	k2eAgMnPc2d1	kouřimský
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7243	[number]	k4	7243
<g/>
-	-	kIx~	-
<g/>
455	[number]	k4	455
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
Přeloženo	přeložit	k5eAaPmNgNnS	přeložit
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgMnSc2	čtvrtý
<g/>
,	,	kIx,	,
upraveného	upravený	k2eAgNnSc2d1	upravené
vydání	vydání	k1gNnSc2	vydání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KONTLER	KONTLER	kA	KONTLER
<g/>
,	,	kIx,	,
László	László	k1gFnSc1	László
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dopl	dopl	k1gMnSc1	dopl
<g/>
..	..	k?	..
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
613	[number]	k4	613
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
616	[number]	k4	616
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
152	[number]	k4	152
<g/>
–	–	k?	–
<g/>
153	[number]	k4	153
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VÁCHA	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Der	drát	k5eAaImRp2nS	drát
Herrscher	Herrschra	k1gFnPc2	Herrschra
auf	auf	k?	auf
dem	dem	k?	dem
Sakralbild	Sakralbild	k1gMnSc1	Sakralbild
zur	zur	k?	zur
Zeit	Zeit	k1gMnSc1	Zeit
der	drát	k5eAaImRp2nS	drát
Gegenreformation	Gegenreformation	k1gInSc1	Gegenreformation
und	und	k?	und
des	des	k1gNnSc1	des
Barock	Barock	k1gMnSc1	Barock
<g/>
:	:	kIx,	:
Eine	Eine	k1gInSc1	Eine
ikonologische	ikonologische	k1gNnSc1	ikonologische
Untersuchung	Untersuchung	k1gInSc1	Untersuchung
zur	zur	k?	zur
herrscherlichen	herrscherlichen	k2eAgInSc1d1	herrscherlichen
Repräsentation	Repräsentation	k1gInSc1	Repräsentation
Kaiser	Kaiser	k1gMnSc1	Kaiser
Ferdinands	Ferdinandsa	k1gFnPc2	Ferdinandsa
II	II	kA	II
<g/>
.	.	kIx.	.
in	in	k?	in
Böhmen	Böhmen	k1gInSc1	Böhmen
<g/>
.	.	kIx.	.
</s>
<s>
Prag	Prag	k1gInSc1	Prag
<g/>
:	:	kIx,	:
Artefactum	Artefactum	k1gNnSc1	Artefactum
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
327	[number]	k4	327
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86890	[number]	k4	86890
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VEBER	VEBER	kA	VEBER
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
doplněné	doplněný	k2eAgInPc4d1	doplněný
a	a	k8xC	a
aktualizované	aktualizovaný	k2eAgInPc4d1	aktualizovaný
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOCELKA	VOCELKA	kA	VOCELKA
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
<g/>
;	;	kIx,	;
HELLER	HELLER	kA	HELLER
<g/>
,	,	kIx,	,
Lynne	Lynn	k1gInSc5	Lynn
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1	soukromý
svět	svět	k1gInSc1	svět
Habsburků	Habsburk	k1gMnPc2	Habsburk
:	:	kIx,	:
život	život	k1gInSc4	život
a	a	k8xC	a
všední	všední	k2eAgInPc4d1	všední
dny	den	k1gInPc4	den
jednoho	jeden	k4xCgInSc2	jeden
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Lucie	Lucie	k1gFnSc1	Lucie
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Plejáda	Plejáda	k1gFnSc1	Plejáda
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
344	[number]	k4	344
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87374	[number]	k4	87374
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VONDRA	Vondra	k1gMnSc1	Vondra
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1637	[number]	k4	1637
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WINKELBAUER	WINKELBAUER	kA	WINKELBAUER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Österreichische	Österreichischus	k1gMnSc5	Österreichischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
1522	[number]	k4	1522
-	-	kIx~	-
1699	[number]	k4	1699
:	:	kIx,	:
Ständefreiheit	Ständefreiheit	k1gInSc1	Ständefreiheit
und	und	k?	und
Fürstenmacht	Fürstenmacht	k1gInSc1	Fürstenmacht
;	;	kIx,	;
Länder	Länder	k1gInSc1	Länder
und	und	k?	und
Untertanen	Untertanen	k1gInSc1	Untertanen
des	des	k1gNnSc1	des
Hauses	Hauses	k1gInSc1	Hauses
Habsburg	Habsburg	k1gMnSc1	Habsburg
im	im	k?	im
konfessionellen	konfessionellen	k2eAgMnSc1d1	konfessionellen
Zeitalter	Zeitalter	k1gMnSc1	Zeitalter
<g/>
.	.	kIx.	.
</s>
<s>
Teil	Teil	k1gInSc1	Teil
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
Ueberreuter	Ueberreuter	k1gMnSc1	Ueberreuter
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
621	[number]	k4	621
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
3528	[number]	k4	3528
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WINKELBAUER	WINKELBAUER	kA	WINKELBAUER
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Österreichische	Österreichischus	k1gMnSc5	Österreichischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
1522	[number]	k4	1522
-	-	kIx~	-
1699	[number]	k4	1699
:	:	kIx,	:
Ständefreiheit	Ständefreiheit	k1gInSc1	Ständefreiheit
und	und	k?	und
Fürstenmacht	Fürstenmacht	k1gInSc1	Fürstenmacht
;	;	kIx,	;
Länder	Länder	k1gInSc1	Länder
und	und	k?	und
Untertanen	Untertanen	k1gInSc1	Untertanen
des	des	k1gNnSc1	des
Hauses	Hauses	k1gInSc1	Hauses
Habsburg	Habsburg	k1gMnSc1	Habsburg
im	im	k?	im
konfessionellen	konfessionellen	k2eAgMnSc1d1	konfessionellen
Zeitalter	Zeitalter	k1gMnSc1	Zeitalter
<g/>
.	.	kIx.	.
</s>
<s>
Teil	Teil	k1gInSc1	Teil
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
<g/>
:	:	kIx,	:
Ueberreuter	Ueberreuter	k1gMnSc1	Ueberreuter
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
567	[number]	k4	567
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
3987	[number]	k4	3987
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hurter	Hurter	k1gInSc1	Hurter
F.	F.	kA	F.
<g/>
,	,	kIx,	,
Geschichte	Geschicht	k1gInSc5	Geschicht
Ferdinands	Ferdinands	k1gInSc1	Ferdinands
II	II	kA	II
<g/>
.	.	kIx.	.
und	und	k?	und
seiner	seiner	k1gMnSc1	seiner
Eltern	Eltern	k1gMnSc1	Eltern
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
11	[number]	k4	11
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
až	až	k8xS	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
volby	volba	k1gFnSc2	volba
císařem	císař	k1gMnSc7	císař
(	(	kIx(	(
<g/>
díly	díl	k1gInPc7	díl
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gInPc4	jeho
osudy	osud	k1gInPc4	osud
po	po	k7c6	po
císařské	císařský	k2eAgFnSc6d1	císařská
volbě	volba	k1gFnSc6	volba
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
(	(	kIx(	(
<g/>
díly	díl	k1gInPc7	díl
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
nachází	nacházet	k5eAaImIp3nS	nacházet
edice	edice	k1gFnSc1	edice
příslušných	příslušný	k2eAgInPc2d1	příslušný
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dílů	díl	k1gInPc2	díl
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
díl	díl	k1gInSc1	díl
3	[number]	k4	3
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
díl	díl	k1gInSc1	díl
4	[number]	k4	4
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
díl	díl	k1gInSc1	díl
5	[number]	k4	5
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
díl	díl	k1gInSc1	díl
7	[number]	k4	7
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
díl	díl	k1gInSc1	díl
8	[number]	k4	8
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
díl	díl	k1gInSc1	díl
9	[number]	k4	9
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
díly	díl	k1gInPc1	díl
10-11	[number]	k4	10-11
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrský	štýrský	k2eAgMnSc1d1	štýrský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrský	štýrský	k2eAgMnSc1d1	štýrský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Štýrský	štýrský	k2eAgInSc1d1	štýrský
</s>
</p>
