<s>
Paul	Paul	k1gMnSc1
Reynaud	Reynaud	k1gMnSc1
</s>
<s>
Paul	Paul	k1gMnSc1
Reynaud	Reynaud	k1gMnSc1
</s>
<s>
118	#num#	k4
<g/>
.	.	kIx.
premiér	premiér	k1gMnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
21	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1940	#num#	k4
–	–	k?
16	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc4
1940	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Édouard	Édouard	k1gMnSc1
Daladier	Daladier	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Philippe	Philippat	k5eAaPmIp3nS
Pétain	Pétain	k2eAgMnSc1d1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Alliance	Alliance	k1gFnSc1
démocratique	démocratique	k1gFnSc1
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
-	-	kIx~
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
Centre	centr	k1gInSc5
National	National	k1gFnSc3
des	des	k1gNnPc2
Indépendants	Indépendants	k1gInSc1
et	et	k?
Paysans	Paysans	k1gInSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
-	-	kIx~
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1878	#num#	k4
Barcelonnette	Barcelonnett	k1gInSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1966	#num#	k4
Neuilly-sur-Seine	Neuilly-sur-Sein	k1gInSc5
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc1
</s>
<s>
Montparnasse	Montparnasse	k1gFnSc1
Choť	choť	k1gFnSc1
</s>
<s>
Jeanne	Jeannout	k5eAaImIp3nS,k5eAaPmIp3nS
Henri-Robert	Henri-Robert	k1gInSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Hélè	Hélè	k5eAaPmIp3nS,k5eAaImIp3nS
de	de	k?
Portes	Portes	k1gInSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Évelyne	Évelynout	k5eAaPmIp3nS
ReynaudováPaul-Serge	ReynaudováPaul-Serge	k1gFnSc4
Reynaud	Reynauda	k1gFnPc2
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1
univerzitaÉcole	univerzitaÉcole	k1gFnSc1
des	des	k1gNnSc2
hautes	hautesa	k1gFnPc2
études	études	k1gMnSc1
commerciales	commerciales	k1gMnSc1
de	de	k?
Paris	Paris	k1gMnSc1
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
advokát	advokát	k1gMnSc1
a	a	k8xC
novinář	novinář	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
rytíř	rytíř	k1gMnSc1
Čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
Commons	Commonsa	k1gFnPc2
</s>
<s>
Paul	Paul	k1gMnSc1
Reynaud	Reynaud	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Paul	Paul	k1gMnSc1
Reynaud	Reynaud	k1gMnSc1
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1878	#num#	k4
Barcelonnette	Barcelonnett	k1gInSc5
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
-	-	kIx~
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1966	#num#	k4
Neuilly-sur-Seine	Neuilly-sur-Sein	k1gInSc5
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
prominentní	prominentní	k2eAgMnSc1d1
právník	právník	k1gMnSc1
meziválečného	meziválečný	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
předposledním	předposlední	k2eAgMnSc7d1
premiérem	premiér	k1gMnSc7
Třetí	třetí	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
války	válka	k1gFnSc2
odmítl	odmítnout	k5eAaPmAgMnS
podporovat	podporovat	k5eAaImF
příměří	příměří	k1gNnSc4
s	s	k7c7
Německem	Německo	k1gNnSc7
a	a	k8xC
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neúspěšně	úspěšně	k6eNd1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
o	o	k7c4
útěk	útěk	k1gInSc4
z	z	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgMnS
zadržen	zadržet	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vězněn	věznit	k5eAaImNgMnS
v	v	k7c4
Sachsenhausenu	Sachsenhausen	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
přesunut	přesunout	k5eAaPmNgInS
do	do	k7c2
věznice	věznice	k1gFnSc2
v	v	k7c6
hradu	hrad	k1gInSc6
Itter	Ittra	k1gFnPc2
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
dočkal	dočkat	k5eAaPmAgInS
osvobození	osvobození	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
opět	opět	k6eAd1
prominentním	prominentní	k2eAgMnSc7d1
politikem	politik	k1gMnSc7
a	a	k8xC
postupně	postupně	k6eAd1
vystřídal	vystřídat	k5eAaPmAgMnS
několik	několik	k4yIc4
ministerských	ministerský	k2eAgInPc2d1
postů	post	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporoval	podporovat	k5eAaImAgMnS
vytvoření	vytvoření	k1gNnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
evropských	evropský	k2eAgFnPc2d1
a	a	k8xC
podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
ústavy	ústava	k1gFnSc2
pro	pro	k7c4
Pátou	pátý	k4xOgFnSc4
republiku	republika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
po	po	k7c6
neshodách	neshoda	k1gFnPc6
s	s	k7c7
prezidentem	prezident	k1gMnSc7
de	de	k?
Gaullem	Gaull	k1gInSc7
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
působení	působení	k1gNnSc2
ve	v	k7c6
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Paul	Paul	k1gMnSc1
Reynaud	Reynaud	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Paul	Paula	k1gFnPc2
Reynaud	Reynaudo	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
skuk	skuk	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4780	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118788558	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2122	#num#	k4
2268	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
93006286	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
17228684	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
93006286	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
