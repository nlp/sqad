<s>
Kanduš	Kanduš	k?
</s>
<s>
Kanduš	Kanduš	k?
(	(	kIx(
<g/>
též	též	k9
kaftan	kaftan	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
živůtková	živůtkový	k2eAgFnSc1d1
sukně	sukně	k1gFnSc1
z	z	k7c2
kanafasu	kanafas	k1gInSc2
<g/>
,	,	kIx,
mezulánu	mezulán	k1gInSc2
nebo	nebo	k8xC
plátna	plátno	k1gNnSc2
upraveného	upravený	k2eAgInSc2d1
modrotiskem	modrotisk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
krojů	kroj	k1gInPc2
severních	severní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
oděv	oděv	k1gInSc1
byl	být	k5eAaImAgInS
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
plátěného	plátěný	k2eAgInSc2d1
rubáče	rubáče	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
nosíval	nosívat	k5eAaImAgMnS
jako	jako	k9
spodní	spodní	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
letním	letní	k2eAgNnSc6d1
období	období	k1gNnSc6
kanduš	kanduš	k?
nosívaly	nosívat	k5eAaImAgFnP
ženy	žena	k1gFnPc4
a	a	k8xC
dívky	dívka	k1gFnPc4
jako	jako	k8xC,k8xS
jednoduché	jednoduchý	k2eAgInPc4d1
šaty	šat	k1gInPc4
bez	bez	k7c2
rukávů	rukáv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakou	jaký	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
součást	součást	k1gFnSc4
krojů	kroj	k1gInPc2
se	se	k3xPyFc4
dochoval	dochovat	k5eAaPmAgInS
zejména	zejména	k9
u	u	k7c2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VONDRUŠKOVÁ	Vondrušková	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jařmo	jařmo	k1gNnSc1
<g/>
,	,	kIx,
parkán	parkán	k1gInSc1
<g/>
,	,	kIx,
trdlice	trdlice	k1gFnSc1
<g/>
,	,	kIx,
aneb	aneb	k?
<g/>
,	,	kIx,
Výkladový	výkladový	k2eAgInSc1d1
slovník	slovník	k1gInSc1
historických	historický	k2eAgInPc2d1
pojmů	pojem	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
upadají	upadat	k5eAaPmIp3nP,k5eAaImIp3nP
v	v	k7c6
zapomnění	zapomnění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
199	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
3946	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Chodský	chodský	k2eAgInSc1d1
kroj	kroj	k1gInSc1
a	a	k8xC
obrázek	obrázek	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
