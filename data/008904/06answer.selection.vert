<s>
Vodka	vodka	k1gFnSc1	vodka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
nápoje	nápoj	k1gInSc2	nápoj
obsahující	obsahující	k2eAgFnSc1d1	obsahující
obvykle	obvykle	k6eAd1	obvykle
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
(	(	kIx(	(
<g/>
nejčastější	častý	k2eAgMnSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
