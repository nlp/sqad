<p>
<s>
Vodka	vodka	k1gFnSc1	vodka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
alkoholického	alkoholický	k2eAgInSc2d1	alkoholický
nápoje	nápoj	k1gInSc2	nápoj
obsahující	obsahující	k2eAgFnSc1d1	obsahující
obvykle	obvykle	k6eAd1	obvykle
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
%	%	kIx~	%
alkoholu	alkohol	k1gInSc2	alkohol
(	(	kIx(	(
<g/>
nejčastější	častý	k2eAgMnSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodka	vodka	k1gFnSc1	vodka
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
čiré	čirý	k2eAgFnSc2d1	čirá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
alkoholické	alkoholický	k2eAgFnSc2d1	alkoholická
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
pochází	pocházet	k5eAaImIp3nS	pocházet
buď	buď	k8xC	buď
ze	z	k7c2	z
slovanského	slovanský	k2eAgNnSc2d1	slovanské
slova	slovo	k1gNnSc2	slovo
pro	pro	k7c4	pro
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
od	od	k7c2	od
в	в	k?	в
(	(	kIx(	(
<g/>
rozpustit	rozpustit	k5eAaPmF	rozpustit
léčivo	léčivo	k1gNnSc4	léčivo
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
jako	jako	k8xS	jako
destilát	destilát	k1gInSc1	destilát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
středověkého	středověký	k2eAgInSc2d1	středověký
nápoje	nápoj	k1gInSc2	nápoj
aqua	aquus	k1gMnSc4	aquus
vitae	vita	k1gMnSc4	vita
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
čistá	čistý	k2eAgFnSc1d1	čistá
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
chlazená	chlazený	k2eAgFnSc1d1	chlazená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
také	také	k9	také
používáná	používáný	k2eAgFnSc1d1	používáná
v	v	k7c6	v
koktejlech	koktejl	k1gInPc6	koktejl
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Screwdriver	Screwdrivra	k1gFnPc2	Screwdrivra
<g/>
,	,	kIx,	,
Yorsh	Yorsha	k1gFnPc2	Yorsha
<g/>
,	,	kIx,	,
Bloody	Blooda	k1gFnSc2	Blooda
Mary	Mary	k1gFnSc2	Mary
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
on	on	k3xPp3gInSc1	on
the	the	k?	the
Beach	Beach	k1gInSc1	Beach
či	či	k8xC	či
Long	Long	k1gInSc1	Long
Island	Island	k1gInSc1	Island
Iced	Iced	k1gInSc1	Iced
Tea	Tea	k1gFnSc1	Tea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodka	vodka	k1gFnSc1	vodka
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
či	či	k8xC	či
rýže	rýže	k1gFnSc1	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgInSc4d1	výrobní
postup	postup	k1gInSc4	postup
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
fází	fáze	k1gFnPc2	fáze
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
kvašení	kvašení	k1gNnSc1	kvašení
<g/>
,	,	kIx,	,
destilace	destilace	k1gFnSc1	destilace
<g/>
,	,	kIx,	,
filtrace	filtrace	k1gFnSc1	filtrace
a	a	k8xC	a
ředění	ředění	k1gNnSc1	ředění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodka	vodka	k1gFnSc1	vodka
z	z	k7c2	z
bramborového	bramborový	k2eAgInSc2d1	bramborový
nebo	nebo	k8xC	nebo
řepného	řepný	k2eAgInSc2d1	řepný
lihu	líh	k1gInSc2	líh
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dochucuje	dochucovat	k5eAaImIp3nS	dochucovat
kapsaicinem	kapsaicin	k1gInSc7	kapsaicin
(	(	kIx(	(
<g/>
paprikovým	paprikový	k2eAgInSc7d1	paprikový
výtažkem	výtažek	k1gInSc7	výtažek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cukrem	cukr	k1gInSc7	cukr
a	a	k8xC	a
kuchyňskou	kuchyňský	k2eAgFnSc7d1	kuchyňská
solí	sůl	k1gFnSc7	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
čisté	čistý	k2eAgFnSc2d1	čistá
vodky	vodka	k1gFnSc2	vodka
existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
ochucených	ochucený	k2eAgFnPc2d1	ochucená
vodek	vodka	k1gFnPc2	vodka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
jemnější	jemný	k2eAgFnPc1d2	jemnější
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
příchutě	příchuť	k1gFnPc4	příchuť
patří	patřit	k5eAaImIp3nP	patřit
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
broskev	broskev	k1gFnSc1	broskev
<g/>
,	,	kIx,	,
švestka	švestka	k1gFnSc1	švestka
<g/>
,	,	kIx,	,
višeň	višeň	k1gFnSc1	višeň
nebo	nebo	k8xC	nebo
meloun	meloun	k1gInSc1	meloun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Známé	známý	k2eAgFnPc1d1	známá
značky	značka	k1gFnPc1	značka
==	==	k?	==
</s>
</p>
<p>
<s>
Absolut	Absolut	k1gInSc1	Absolut
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Amundsen	Amundsen	k1gInSc1	Amundsen
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beluga	Beluga	k1gFnSc1	Beluga
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Belvedere	Belvedrat	k5eAaPmIp3nS	Belvedrat
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Berentzen	Berentzen	k2eAgMnSc1d1	Berentzen
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bismarck	Bismarck	k6eAd1	Bismarck
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bols	Bols	k6eAd1	Bols
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Boris	Boris	k1gMnSc1	Boris
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Božkov	Božkov	k1gInSc1	Božkov
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cîroc	Cîroc	k1gFnSc1	Cîroc
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Danzka	Danzka	k1gFnSc1	Danzka
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finlandia	Finlandium	k1gNnPc1	Finlandium
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Goral	goral	k1gMnSc1	goral
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gorbatschow	Gorbatschow	k?	Gorbatschow
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grasovka	Grasovka	k1gFnSc1	Grasovka
Vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
/	/	kIx~	/
<g/>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grey	Grea	k1gFnPc1	Grea
Goose	Goose	k1gFnSc2	Goose
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hanácká	hanácký	k2eAgFnSc1d1	Hanácká
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chingis	Chingis	k1gFnSc1	Chingis
(	(	kIx(	(
<g/>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chopin	Chopin	k1gMnSc1	Chopin
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jelzin	Jelzin	k1gInSc1	Jelzin
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kalašnikov	kalašnikov	k1gInSc1	kalašnikov
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koskenkorva	Koskenkorva	k1gFnSc1	Koskenkorva
Vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Level	level	k1gInSc1	level
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moskovskaya	Moskovskay	k2eAgFnSc1d1	Moskovskay
osobaya	osobay	k2eAgFnSc1d1	osobay
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nemiroff	Nemiroff	k1gInSc1	Nemiroff
(	(	kIx(	(
<g/>
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Puschkin	Puschkin	k1gInSc1	Puschkin
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Russian	Russian	k1gInSc1	Russian
Standard	standard	k1gInSc1	standard
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skyy	Skyy	k1gInPc1	Skyy
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smirnoff	Smirnoff	k1gMnSc1	Smirnoff
Red	Red	k1gMnSc1	Red
<g/>
,	,	kIx,	,
Blue	Blue	k1gFnSc1	Blue
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
/	/	kIx~	/
<g/>
USA	USA	kA	USA
<g/>
/	/	kIx~	/
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sobieski	Sobieski	k6eAd1	Sobieski
wódka	wódka	k1gFnSc1	wódka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Soplica	Soplic	k2eAgFnSc1d1	Soplic
wódka	wódka	k1gFnSc1	wódka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
St.	st.	kA	st.
Nicolaus	Nicolaus	k1gInSc1	Nicolaus
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Starogardzka	Starogardzka	k1gFnSc1	Starogardzka
wódka	wódka	k1gFnSc1	wódka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stock	Stock	k6eAd1	Stock
Plzeň	Plzeň	k1gFnSc1	Plzeň
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stolichnaya	Stolichnay	k2eAgFnSc1d1	Stolichnay
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ultimat	ultimatum	k1gNnPc2	ultimatum
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Villa	Villa	k1gMnSc1	Villa
Lobos	Lobos	k1gMnSc1	Lobos
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vodka	vodka	k1gFnSc1	vodka
42	[number]	k4	42
Blended	Blended	k1gInSc1	Blended
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vodka	vodka	k1gFnSc1	vodka
Velikopol	Velikopol	k1gInSc1	Velikopol
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vodka	vodka	k1gFnSc1	vodka
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Wyborowa	Wyborow	k2eAgFnSc1d1	Wyborow
wódka	wódka	k1gFnSc1	wódka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Xellent	Xellent	k1gInSc1	Xellent
vodka	vodka	k1gFnSc1	vodka
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Żubrówka	Żubrówka	k1gFnSc1	Żubrówka
wódka	wódka	k1gFnSc1	wódka
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vodka	vodka	k1gFnSc1	vodka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vodka	vodka	k1gFnSc1	vodka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
