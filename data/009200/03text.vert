<p>
<s>
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Havířov	Havířov	k1gInSc1	Havířov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Pasta	pasta	k1gFnSc1	pasta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
momentálně	momentálně	k6eAd1	momentálně
hrající	hrající	k2eAgMnSc1d1	hrající
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
draftován	draftovat	k5eAaImNgInS	draftovat
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
draftu	draft	k1gInSc2	draft
2014	[number]	k4	2014
jako	jako	k9	jako
25	[number]	k4	25
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
týmem	tým	k1gInSc7	tým
Boston	Boston	k1gInSc1	Boston
Bruins	Bruinsa	k1gFnPc2	Bruinsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svých	svůj	k3xOyFgNnPc2	svůj
prvních	první	k4xOgNnPc6	první
2	[number]	k4	2
branek	branka	k1gFnPc2	branka
v	v	k7c6	v
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Philadelphii	Philadelphia	k1gFnSc3	Philadelphia
Flyers	Flyersa	k1gFnPc2	Flyersa
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
překonal	překonat	k5eAaPmAgMnS	překonat
kanadského	kanadský	k2eAgMnSc4d1	kanadský
brankáře	brankář	k1gMnSc4	brankář
Raye	Ray	k1gMnSc2	Ray
Emeryho	Emery	k1gMnSc2	Emery
bekhendem	bekhend	k1gInSc7	bekhend
po	po	k7c6	po
dorážce	dorážka	k1gFnSc6	dorážka
střely	střela	k1gFnSc2	střela
švédského	švédský	k2eAgMnSc2d1	švédský
hokejisty	hokejista	k1gMnSc2	hokejista
Marcuse	Marcuse	k1gFnSc1	Marcuse
Krügera	Krügera	k1gFnSc1	Krügera
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
gól	gól	k1gInSc4	gól
pak	pak	k6eAd1	pak
přidal	přidat	k5eAaPmAgMnS	přidat
zakončením	zakončení	k1gNnSc7	zakončení
bez	bez	k7c2	bez
přípravy	příprava	k1gFnSc2	příprava
po	po	k7c6	po
přihrávce	přihrávka	k1gFnSc6	přihrávka
od	od	k7c2	od
krajana	krajan	k1gMnSc2	krajan
Davida	David	k1gMnSc2	David
Krejčího	Krejčí	k1gMnSc2	Krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mohl	moct	k5eAaImAgInS	moct
zkompletovat	zkompletovat	k5eAaPmF	zkompletovat
hattrick	hattrick	k1gInSc4	hattrick
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
prázdnou	prázdný	k2eAgFnSc4d1	prázdná
klec	klec	k1gFnSc4	klec
soupeře	soupeř	k1gMnSc4	soupeř
trefil	trefit	k5eAaPmAgMnS	trefit
až	až	k6eAd1	až
po	po	k7c6	po
konci	konec	k1gInSc6	konec
vypršení	vypršení	k1gNnSc1	vypršení
základní	základní	k2eAgFnSc2d1	základní
hrací	hrací	k2eAgFnSc2d1	hrací
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
zásahům	zásah	k1gInPc3	zásah
mu	on	k3xPp3gMnSc3	on
navíc	navíc	k6eAd1	navíc
stačily	stačit	k5eAaBmAgInP	stačit
pouhé	pouhý	k2eAgFnPc4d1	pouhá
dvě	dva	k4xCgFnPc4	dva
střely	střela	k1gFnPc4	střela
na	na	k7c4	na
branku	branka	k1gFnSc4	branka
za	za	k7c4	za
celé	celý	k2eAgNnSc4d1	celé
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
českých	český	k2eAgMnPc2d1	český
hokejistů	hokejista	k1gMnPc2	hokejista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
kategorii	kategorie	k1gFnSc4	kategorie
Junior	junior	k1gMnSc1	junior
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hokejka	hokejka	k1gFnSc1	hokejka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
a	a	k8xC	a
2019	[number]	k4	2019
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hokejka	hokejka	k1gFnSc1	hokejka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
výhercem	výherce	k1gMnSc7	výherce
této	tento	k3xDgFnSc2	tento
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Česko	Česko	k1gNnSc1	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
kariéru	kariéra	k1gFnSc4	kariéra
načal	načít	k5eAaPmAgMnS	načít
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Havířově	Havířov	k1gInSc6	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
hokejové	hokejový	k2eAgInPc4d1	hokejový
krůčky	krůček	k1gInPc4	krůček
udělal	udělat	k5eAaPmAgMnS	udělat
na	na	k7c6	na
havířovském	havířovský	k2eAgInSc6d1	havířovský
zimním	zimní	k2eAgInSc6d1	zimní
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
září	září	k1gNnSc6	září
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
zde	zde	k6eAd1	zde
ve	v	k7c6	v
3	[number]	k4	3
letech	léto	k1gNnPc6	léto
přivedla	přivést	k5eAaPmAgFnS	přivést
maminka	maminka	k1gFnSc1	maminka
Marcela	Marcela	k1gFnSc1	Marcela
<g/>
.	.	kIx.	.
</s>
<s>
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
začínal	začínat	k5eAaImAgMnS	začínat
hrát	hrát	k5eAaImF	hrát
hokej	hokej	k1gInSc4	hokej
v	v	k7c6	v
dívčích	dívčí	k2eAgFnPc6d1	dívčí
krasobruslařských	krasobruslařský	k2eAgFnPc6d1	krasobruslařská
bruslích	brusle	k1gFnPc6	brusle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
zvládl	zvládnout	k5eAaPmAgMnS	zvládnout
potřebné	potřebný	k2eAgInPc4d1	potřebný
základy	základ	k1gInPc4	základ
bruslení	bruslení	k1gNnSc2	bruslení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
už	už	k9	už
jeho	jeho	k3xOp3gFnPc1	jeho
hokejové	hokejový	k2eAgFnPc1d1	hokejová
dovednosti	dovednost	k1gFnPc1	dovednost
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
jako	jako	k9	jako
předškolák	předškolák	k1gMnSc1	předškolák
hrával	hrávat	k5eAaImAgMnS	hrávat
zápasy	zápas	k1gInPc4	zápas
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
za	za	k7c4	za
3	[number]	k4	3
<g/>
.	.	kIx.	.
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
v	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
za	za	k7c4	za
A	A	kA	A
tým	tým	k1gInSc4	tým
havířovského	havířovský	k2eAgInSc2d1	havířovský
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zámoří	zámoří	k1gNnSc2	zámoří
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
jako	jako	k9	jako
25	[number]	k4	25
<g/>
.	.	kIx.	.
celkově	celkově	k6eAd1	celkově
týmem	tým	k1gInSc7	tým
Boston	Boston	k1gInSc1	Boston
Bruins	Bruinsa	k1gFnPc2	Bruinsa
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
draftu	draft	k1gInSc2	draft
NHL	NHL	kA	NHL
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
tříletý	tříletý	k2eAgInSc1d1	tříletý
nováčkovský	nováčkovský	k2eAgInSc1d1	nováčkovský
kontrakt	kontrakt	k1gInSc1	kontrakt
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
týmem	tým	k1gInSc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Pastrňák	Pastrňák	k1gInSc4	Pastrňák
předsezónní	předsezónní	k2eAgInSc4d1	předsezónní
tréninkový	tréninkový	k2eAgInSc4d1	tréninkový
kemp	kemp	k1gInSc4	kemp
Bruins	Bruinsa	k1gFnPc2	Bruinsa
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
si	se	k3xPyFc3	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	zářit	k5eAaImIp3nS	zářit
poranil	poranit	k5eAaPmAgMnS	poranit
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
farmářského	farmářský	k2eAgInSc2d1	farmářský
týmu	tým	k1gInSc2	tým
Providence	providence	k1gFnSc2	providence
Bruins	Bruinsa	k1gFnPc2	Bruinsa
v	v	k7c4	v
American	American	k1gInSc4	American
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
(	(	kIx(	(
<g/>
AHL	AHL	kA	AHL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
listopadu	listopad	k1gInSc2	listopad
připsal	připsat	k5eAaPmAgMnS	připsat
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
zápasech	zápas	k1gInPc6	zápas
devět	devět	k4xCc4	devět
kanadských	kanadský	k2eAgInPc2d1	kanadský
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
dvě	dva	k4xCgFnPc4	dva
branky	branka	k1gFnPc4	branka
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
asistencí	asistence	k1gFnPc2	asistence
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
útočníkem	útočník	k1gMnSc7	útočník
a	a	k8xC	a
nováčkem	nováček	k1gMnSc7	nováček
měsíce	měsíc	k1gInSc2	měsíc
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
AHL	AHL	kA	AHL
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
potřebovalo	potřebovat	k5eAaImAgNnS	potřebovat
vedení	vedení	k1gNnSc1	vedení
Bostonu	Boston	k1gInSc2	Boston
doplnit	doplnit	k5eAaPmF	doplnit
kádr	kádr	k1gInSc4	kádr
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
Davida	David	k1gMnSc2	David
Krejčího	Krejčí	k1gMnSc2	Krejčí
a	a	k8xC	a
Chrise	Chrise	k1gFnSc1	Chrise
Kellyho	Kelly	k1gMnSc2	Kelly
<g/>
,	,	kIx,	,
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
se	se	k3xPyFc4	se
sáhnout	sáhnout	k5eAaPmF	sáhnout
z	z	k7c2	z
Providence	providence	k1gFnSc2	providence
po	po	k7c6	po
Pastrňákovi	Pastrňák	k1gMnSc6	Pastrňák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
17	[number]	k4	17
zápasech	zápas	k1gInPc6	zápas
AHL	AHL	kA	AHL
5	[number]	k4	5
gólů	gól	k1gInPc2	gól
a	a	k8xC	a
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
13	[number]	k4	13
asistencí	asistence	k1gFnPc2	asistence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
poprvé	poprvé	k6eAd1	poprvé
debutoval	debutovat	k5eAaBmAgInS	debutovat
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Pittsburghu	Pittsburgh	k1gInSc3	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
prohrál	prohrát	k5eAaPmAgInS	prohrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
utkání	utkání	k1gNnSc4	utkání
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
deset	deset	k4xCc4	deset
střídání	střídání	k1gNnPc2	střídání
a	a	k8xC	a
celkem	celkem	k6eAd1	celkem
na	na	k7c6	na
ledě	led	k1gInSc6	led
strávil	strávit	k5eAaPmAgMnS	strávit
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
53	[number]	k4	53
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
1	[number]	k4	1
<g/>
.	.	kIx.	.
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
první	první	k4xOgInSc4	první
bod	bod	k1gInSc4	bod
v	v	k7c6	v
NHL	NHL	kA	NHL
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Philadelphii	Philadelphia	k1gFnSc3	Philadelphia
Flyers	Flyersa	k1gFnPc2	Flyersa
<g/>
,	,	kIx,	,
když	když	k8xS	když
překoval	překovat	k5eAaPmAgInS	překovat
Raye	Ray	k1gMnSc4	Ray
Emeryho	Emery	k1gMnSc4	Emery
<g/>
,	,	kIx,	,
pomohl	pomoct	k5eAaPmAgMnS	pomoct
k	k	k7c3	k
výhře	výhra	k1gFnSc3	výhra
svého	svůj	k3xOyFgNnSc2	svůj
mužstva	mužstvo	k1gNnSc2	mužstvo
poměrem	poměr	k1gInSc7	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
první	první	k4xOgFnSc7	první
hvězdou	hvězda	k1gFnSc7	hvězda
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
stejný	stejný	k2eAgInSc1d1	stejný
počin	počin	k1gInSc1	počin
i	i	k9	i
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
Tampou	Tampa	k1gFnSc7	Tampa
Bay	Bay	k1gFnSc2	Bay
Lightning	Lightning	k1gInSc1	Lightning
<g/>
,	,	kIx,	,
když	když	k8xS	když
dvakrát	dvakrát	k6eAd1	dvakrát
překonal	překonat	k5eAaPmAgInS	překonat
Bena	Bena	k?	Bena
Bishopa	Bishop	k1gMnSc2	Bishop
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
první	první	k4xOgInSc4	první
teenegerem	teeneger	k1gInSc7	teeneger
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Bostonu	Boston	k1gInSc2	Boston
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
vítězný	vítězný	k2eAgInSc4d1	vítězný
gól	gól	k1gInSc4	gól
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
když	když	k8xS	když
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Carolinou	Carolina	k1gFnSc7	Carolina
Hurricanes	Hurricanesa	k1gFnPc2	Hurricanesa
při	při	k7c6	při
vítězství	vítězství	k1gNnSc6	vítězství
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
je	být	k5eAaImIp3nS	být
mladším	mladý	k2eAgMnSc7d2	mladší
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
synů	syn	k1gMnPc2	syn
Marcely	Marcela	k1gFnSc2	Marcela
Ziembové	Ziembová	k1gFnSc2	Ziembová
a	a	k8xC	a
Milana	Milan	k1gMnSc2	Milan
Pastrňáka	Pastrňák	k1gMnSc2	Pastrňák
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
–	–	k?	–
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
v	v	k7c6	v
Havířově	Havířov	k1gInSc6	Havířov
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
Jakuba	Jakub	k1gMnSc2	Jakub
(	(	kIx(	(
<g/>
*	*	kIx~	*
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
sportem	sport	k1gInSc7	sport
jako	jako	k8xS	jako
on	on	k3xPp3gMnSc1	on
neživí	živit	k5eNaImIp3nS	živit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
členem	člen	k1gInSc7	člen
týmu	tým	k1gInSc2	tým
Real	Real	k1gInSc1	Real
TOP	topit	k5eAaImRp2nS	topit
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
dresu	dres	k1gInSc6	dres
se	se	k3xPyFc4	se
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
charitativních	charitativní	k2eAgInPc2d1	charitativní
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Tváří	tvář	k1gFnSc7	tvář
roku	rok	k1gInSc2	rok
organizace	organizace	k1gFnSc2	organizace
Champions	Championsa	k1gFnPc2	Championsa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
for	forum	k1gNnPc2	forum
Children	Childrna	k1gFnPc2	Childrna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
dětským	dětský	k2eAgMnPc3d1	dětský
pacientům	pacient	k1gMnPc3	pacient
v	v	k7c6	v
bostonských	bostonský	k2eAgFnPc6d1	Bostonská
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
juniorka	juniorka	k1gFnSc1	juniorka
(	(	kIx(	(
<g/>
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
do	do	k7c2	do
18	[number]	k4	18
letMistrovství	letMistrovství	k1gNnSc2	letMistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokejiAmerican	hokejiAmerican	k1gInSc1	hokejiAmerican
Hockey	Hocke	k2eAgInPc1d1	Hocke
League	Leagu	k1gInPc1	Leagu
</s>
</p>
<p>
<s>
==	==	k?	==
Klubové	klubový	k2eAgFnPc1d1	klubová
statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
2018	[number]	k4	2018
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
celkem	celkem	k6eAd1	celkem
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
NHL	NHL	kA	NHL
+	+	kIx~	+
Play	play	k0	play
OFF	OFF	kA	OFF
<g/>
)	)	kIx)	)
za	za	k7c4	za
94	[number]	k4	94
zápasů	zápas	k1gInPc2	zápas
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
2019	[number]	k4	2019
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
celkem	celkem	k6eAd1	celkem
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
NHL	NHL	kA	NHL
+	+	kIx~	+
Play	play	k0	play
OFF	OFF	kA	OFF
<g/>
)	)	kIx)	)
za	za	k7c4	za
90	[number]	k4	90
zápasů	zápas	k1gInPc2	zápas
100	[number]	k4	100
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Juniorská	juniorský	k2eAgFnSc1d1	juniorská
reprezentace	reprezentace	k1gFnSc1	reprezentace
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Seniorská	seniorský	k2eAgFnSc1d1	seniorská
reprezentace	reprezentace	k1gFnSc1	reprezentace
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gInSc4	Pastrňák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
na	na	k7c6	na
Instagramu	Instagram	k1gInSc6	Instagram
</s>
</p>
<p>
<s>
====	====	k?	====
Profily	profil	k1gInPc4	profil
====	====	k?	====
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Pastrňák	Pastrňák	k1gMnSc1	Pastrňák
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hráčský	hráčský	k2eAgInSc1d1	hráčský
profil	profil	k1gInSc1	profil
Real	Real	k1gInSc1	Real
TOP	topit	k5eAaImRp2nS	topit
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
