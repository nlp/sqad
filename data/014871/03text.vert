<s>
Matica	Matic	k2eAgFnSc1d1
slovenská	slovenský	k2eAgFnSc1d1
</s>
<s>
Matice	matice	k1gFnSc1
slovenská	slovenský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečať	Pečať	k1gFnSc1
<g/>
.	.	kIx.
1848	#num#	k4
</s>
<s>
První	první	k4xOgNnSc1
sídlo	sídlo	k1gNnSc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
</s>
<s>
Matica	Matica	k6eAd1
slovenská	slovenský	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
slovenské	slovenský	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
kulturní	kulturní	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Martině	Martina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
je	být	k5eAaImIp3nS
předsedou	předseda	k1gMnSc7
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
advokát	advokát	k1gMnSc1
Marián	Marián	k1gMnSc1
Gešper	Gešper	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Přípravy	příprava	k1gFnPc1
vzniku	vznik	k1gInSc2
</s>
<s>
Zakládající	zakládající	k2eAgMnPc1d1
členové	člen	k1gMnPc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kreslil	kreslit	k5eAaImAgMnS
Josef	Josef	k1gMnSc1
Rupert	Rupert	k1gMnSc1
Maria	Mario	k1gMnSc4
Přecechtěl	Přecechtěl	k1gMnSc4
</s>
<s>
První	první	k4xOgFnSc4
zmínku	zmínka	k1gFnSc4
o	o	k7c6
potřebě	potřeba	k1gFnSc6
založit	založit	k5eAaPmF
Matici	matice	k1gFnSc4
slovenskou	slovenský	k2eAgFnSc4d1
vyslovil	vyslovit	k5eAaPmAgMnS
v	v	k7c6
listě	list	k1gInSc6
Jánu	Ján	k1gMnSc3
Kollárovi	Kollár	k1gMnSc3
roku	rok	k1gInSc2
1827	#num#	k4
Pavel	Pavel	k1gMnSc1
Josef	Josef	k1gMnSc1
Šafařík	Šafařík	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
nedávno	nedávno	k6eAd1
založenou	založený	k2eAgFnSc4d1
Matici	matice	k1gFnSc4
srbskou	srbský	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
jako	jako	k9
první	první	k4xOgFnSc1
slovanská	slovanský	k2eAgFnSc1d1
matice	matice	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
o	o	k7c4
rok	rok	k1gInSc4
dříve	dříve	k6eAd2
(	(	kIx(
<g/>
1826	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1831	#num#	k4
existovala	existovat	k5eAaImAgFnS
Matice	matice	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dalo	dát	k5eAaPmAgNnS
Slovákům	Slovák	k1gMnPc3
konkrétní	konkrétní	k2eAgFnSc4d1
představu	představa	k1gFnSc4
ohledně	ohledně	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
Matice	matice	k1gFnSc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myšlenky	myšlenka	k1gFnSc2
se	se	k3xPyFc4
chopili	chopit	k5eAaPmAgMnP
revolučně	revolučně	k6eAd1
naladění	naladěný	k2eAgMnPc1d1
štúrovci	štúrovec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
programu	program	k1gInSc6
svého	svůj	k3xOyFgInSc2
kroužku	kroužek	k1gInSc2
Vzájemnost	vzájemnost	k1gFnSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
i	i	k9
založení	založení	k1gNnSc2
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
plány	plán	k1gInPc4
se	se	k3xPyFc4
však	však	k9
neuskutečnily	uskutečnit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
slovenské	slovenský	k2eAgFnSc2d1
Matice	matice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1863	#num#	k4
byl	být	k5eAaImAgInS
výsledkem	výsledek	k1gInSc7
snah	snaha	k1gFnPc2
slovensky	slovensky	k6eAd1
orientovaných	orientovaný	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
na	na	k7c6
martinském	martinský	k2eAgNnSc6d1
Memorandovém	memorandový	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
roku	rok	k1gInSc2
1861	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přípravný	přípravný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
</s>
<s>
Obal	obal	k1gInSc1
prvních	první	k4xOgFnPc2
stanov	stanova	k1gFnPc2
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
</s>
<s>
Zde	zde	k6eAd1
byl	být	k5eAaImAgInS
ustanoven	ustanoven	k2eAgInSc1d1
dočasný	dočasný	k2eAgInSc1d1
přípravný	přípravný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
poměrně	poměrně	k6eAd1
pružně	pružně	k6eAd1
vypracoval	vypracovat	k5eAaPmAgMnS
návrh	návrh	k1gInSc4
stanov	stanova	k1gFnPc2
spolku	spolek	k1gInSc2
a	a	k8xC
již	již	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1861	#num#	k4
je	být	k5eAaImIp3nS
předložil	předložit	k5eAaPmAgMnS
ke	k	k7c3
schválení	schválení	k1gNnSc3
uherské	uherský	k2eAgFnSc3d1
místodržitelské	místodržitelský	k2eAgFnSc3d1
radě	rada	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výbor	výbor	k1gInSc1
inicioval	iniciovat	k5eAaBmAgInS
i	i	k9
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
národní	národní	k2eAgFnSc4d1
sbírku	sbírka	k1gFnSc4
na	na	k7c4
matiční	matiční	k2eAgInSc4d1
fond	fond	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
probíhala	probíhat	k5eAaImAgNnP
velmi	velmi	k6eAd1
úspěšně	úspěšně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
dárců	dárce	k1gMnPc2
sice	sice	k8xC
byli	být	k5eAaImAgMnP
chudí	chudý	k2eAgMnPc1d1
zemědělci	zemědělec	k1gMnPc1
či	či	k8xC
řemeslníci	řemeslník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
obvykle	obvykle	k6eAd1
dávali	dávat	k5eAaImAgMnP
jen	jen	k9
po	po	k7c6
jednom	jeden	k4xCgInSc6
krejcaru	krejcar	k1gInSc6
<g/>
,	,	kIx,
časem	časem	k6eAd1
jich	on	k3xPp3gMnPc2
však	však	k9
byly	být	k5eAaImAgInP
tisíce	tisíc	k4xCgInPc4
<g/>
,	,	kIx,
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
dary	dar	k1gInPc1
tak	tak	k9
za	za	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc1
narostly	narůst	k5eAaPmAgInP
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
padesáti	padesát	k4xCc2
tisíce	tisíc	k4xCgInSc2
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgFnPc4d2
starosti	starost	k1gFnPc4
měl	mít	k5eAaImAgInS
výbor	výbor	k1gInSc1
s	s	k7c7
hledáním	hledání	k1gNnSc7
sídla	sídlo	k1gNnSc2
Matice	matice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
bylo	být	k5eAaImAgNnS
vybráno	vybrat	k5eAaPmNgNnS
Brezno	Brezna	k1gFnSc5
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
městská	městský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
dala	dát	k5eAaPmAgFnS
přípravnému	přípravný	k2eAgInSc3d1
výboru	výbor	k1gInSc3
i	i	k9
předběžný	předběžný	k2eAgInSc4d1
souhlas	souhlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
zásah	zásah	k1gInSc4
maďarského	maďarský	k2eAgMnSc2d1
komisaře	komisař	k1gMnSc2
Zvolenské	zvolenský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
Jozefa	Jozef	k1gMnSc2
Havase	Havasa	k1gFnSc6
však	však	k9
své	svůj	k3xOyFgNnSc1
rozhodnutí	rozhodnutí	k1gNnSc1
odvolalo	odvolat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
delším	dlouhý	k2eAgNnSc6d2
hledání	hledání	k1gNnSc6
nakonec	nakonec	k6eAd1
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
volba	volba	k1gFnSc1
na	na	k7c4
memorandový	memorandový	k2eAgInSc4d1
Martin	Martin	k1gInSc4
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
Turčiansky	Turčiansky	k1gFnPc2
Svätý	Svätý	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
1862	#num#	k4
ohlásil	ohlásit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
přijme	přijmout	k5eAaPmIp3nS
Matici	matice	k1gFnSc4
slovenskou	slovenský	k2eAgFnSc4d1
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
lůna	lůno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
konečné	konečný	k2eAgNnSc4d1
schválení	schválení	k1gNnSc4
stanov	stanova	k1gFnPc2
však	však	k9
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
čekat	čekat	k5eAaImF
ještě	ještě	k9
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Cyrilometodějské	cyrilometodějský	k2eAgFnPc1d1
oslavy	oslava	k1gFnPc1
</s>
<s>
Spolu	spolu	k6eAd1
s	s	k7c7
matiční	matiční	k2eAgFnSc7d1
akcí	akce	k1gFnSc7
probíhaly	probíhat	k5eAaImAgFnP
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
i	i	k9
velké	velká	k1gFnSc3
přípravy	příprava	k1gFnSc2
oslav	oslava	k1gFnPc2
tisíciletého	tisíciletý	k2eAgNnSc2d1
jubilea	jubileum	k1gNnSc2
příchodu	příchod	k1gInSc2
Cyrila	Cyril	k1gMnSc2
a	a	k8xC
Metoděje	Metoděj	k1gMnSc2
na	na	k7c4
Velkou	velký	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přípravě	příprava	k1gFnSc6
oslav	oslava	k1gFnPc2
se	se	k3xPyFc4
angažovali	angažovat	k5eAaBmAgMnP
hlavně	hlavně	k9
Andrej	Andrej	k1gMnSc1
Ľudovít	Ľudovít	k1gMnSc1
Radlinský	Radlinský	k2eAgMnSc1d1
a	a	k8xC
Štefan	Štefan	k1gMnSc1
Moyzes	Moyzes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovenští	slovenský	k2eAgMnPc1d1
biskupové	biskup	k1gMnPc1
byli	být	k5eAaImAgMnP
vyzváni	vyzvat	k5eAaPmNgMnP
k	k	k7c3
aktivní	aktivní	k2eAgFnSc3d1
účasti	účast	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
ostřihomský	ostřihomský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
Jan	Jan	k1gMnSc1
Scitovský	Scitovský	k2eAgMnSc1d1
udělal	udělat	k5eAaPmAgMnS
vše	všechen	k3xTgNnSc4
pro	pro	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
oslavy	oslava	k1gFnPc1
neuskutečnily	uskutečnit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nitranský	nitranský	k2eAgInSc1d1
biskup	biskup	k1gInSc1
byl	být	k5eAaImAgInS
zastrašen	zastrašit	k5eAaPmNgInS
(	(	kIx(
<g/>
centrem	centrum	k1gNnSc7
národních	národní	k2eAgFnPc2d1
oslav	oslava	k1gFnPc2
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
stát	stát	k5eAaImF,k5eAaPmF
Nitra	Nitra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
větší	veliký	k2eAgFnPc1d2
oslavy	oslava	k1gFnPc1
konaly	konat	k5eAaImAgFnP
jen	jen	k9
v	v	k7c6
bystrické	bystrický	k2eAgFnSc6d1
a	a	k8xC
spišské	spišský	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jubileum	jubileum	k1gNnSc1
nakonec	nakonec	k6eAd1
nebylo	být	k5eNaImAgNnS
tak	tak	k6eAd1
slavné	slavný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
plánovalo	plánovat	k5eAaImAgNnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
velkou	velký	k2eAgFnSc7d1
manifestací	manifestace	k1gFnSc7
národního	národní	k2eAgNnSc2d1
uvědomění	uvědomění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přispěly	přispět	k5eAaPmAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
i	i	k9
velké	velký	k2eAgFnPc1d1
poutě	pouť	k1gFnPc1
na	na	k7c4
Moravu	Morava	k1gFnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c4
Velehrad	Velehrad	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
oslavy	oslava	k1gFnPc1
nabraly	nabrat	k5eAaPmAgFnP
skutečný	skutečný	k2eAgInSc4d1
česko-slovenský	česko-slovenský	k2eAgInSc4d1
a	a	k8xC
slovanský	slovanský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
Velehrad	Velehrad	k1gInSc1
stal	stát	k5eAaPmAgInS
mekkou	mekká	k1gFnSc4
slovenských	slovenský	k2eAgMnPc2d1
poutníků	poutník	k1gMnPc2
a	a	k8xC
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
utužovalo	utužovat	k5eAaImAgNnS
bratrství	bratrství	k1gNnSc1
Čechů	Čech	k1gMnPc2
<g/>
,	,	kIx,
Moravanů	Moravan	k1gMnPc2
a	a	k8xC
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
</s>
<s>
Vyvrcholením	vyvrcholení	k1gNnSc7
cyrilometodějských	cyrilometodějský	k2eAgFnPc2d1
oslav	oslava	k1gFnPc2
bylo	být	k5eAaImAgNnS
první	první	k4xOgNnSc1
valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1863	#num#	k4
v	v	k7c6
Martine	Martin	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
koncem	koncem	k7c2
května	květen	k1gInSc2
potvrdil	potvrdit	k5eAaPmAgMnS
stanovy	stanova	k1gFnPc4
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakládající	zakládající	k2eAgFnSc1d1
valné	valný	k2eAgNnSc4d1
shromáždění	shromáždění	k1gNnSc4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
velkou	velký	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
manifestací	manifestace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moyzesova	Moyzesův	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
z	z	k7c2
Banské	banský	k2eAgFnSc2d1
Bystrice	Bystrica	k1gFnSc2
do	do	k7c2
Martina	Martin	k1gInSc2
připomínala	připomínat	k5eAaImAgFnS
triumfální	triumfální	k2eAgInSc4d1
pochod	pochod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yRgMnPc7,k3yIgMnPc7,k3yQgMnPc7
procházel	procházet	k5eAaImAgMnS
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
připravovalo	připravovat	k5eAaImAgNnS
nadšené	nadšený	k2eAgNnSc1d1
uvítání	uvítání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martinských	martinský	k2eAgFnPc2d1
oslav	oslava	k1gFnPc2
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
účastnilo	účastnit	k5eAaImAgNnS
ještě	ještě	k6eAd1
víc	hodně	k6eAd2
lidí	člověk	k1gMnPc2
než	než	k8xS
memorandového	memorandový	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
prvním	první	k4xOgNnSc6
pracovním	pracovní	k2eAgNnSc6d1
zasedání	zasedání	k1gNnSc6
členů	člen	k1gInPc2
Matice	matice	k1gFnSc2
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
první	první	k4xOgInSc1
matiční	matiční	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Štefan	Štefan	k1gMnSc1
Moyzes	Moyzes	k1gMnSc1
<g/>
,	,	kIx,
místopředsedové	místopředseda	k1gMnPc1
Karol	Karola	k1gFnPc2
Kuzmány	Kuzmán	k1gMnPc7
a	a	k8xC
Jan	Jan	k1gMnSc1
Országh	Országh	k1gMnSc1
<g/>
,	,	kIx,
tajemníkem	tajemník	k1gMnSc7
Pavel	Pavel	k1gMnSc1
Mudroň	Mudroň	k1gMnSc1
a	a	k8xC
Michal	Michal	k1gMnSc1
Chrastek	Chrastka	k1gFnPc2
<g/>
,	,	kIx,
pokladníkem	pokladník	k1gMnSc7
Tomáš	Tomáš	k1gMnSc1
Červeňa	Červeňa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Francisci	Francisek	k1gMnPc1
byl	být	k5eAaImAgMnS
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
při	při	k7c6
získávání	získávání	k1gNnSc6
povolení	povolení	k1gNnSc2
ke	k	k7c3
vzniku	vznik	k1gInSc3
Matice	matice	k1gFnSc2
vybaven	vybaven	k2eAgInSc1d1
titulem	titul	k1gInSc7
čestného	čestný	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
Matice	matice	k1gFnSc2
</s>
<s>
Podle	podle	k7c2
stanov	stanova	k1gFnPc2
měla	mít	k5eAaImAgFnS
Matice	matice	k1gFnSc1
sdružovat	sdružovat	k5eAaImF
slovenské	slovenský	k2eAgInPc4d1
kulturní	kulturní	k2eAgInPc4d1
<g/>
,	,	kIx,
osvětové	osvětový	k2eAgInPc4d1
a	a	k8xC
vědecké	vědecký	k2eAgMnPc4d1
pracovníky	pracovník	k1gMnPc4
<g/>
,	,	kIx,
podporovat	podporovat	k5eAaImF
rozvoj	rozvoj	k1gInSc4
slovenské	slovenský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
organizovat	organizovat	k5eAaBmF
osvětovou	osvětový	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
<g/>
,	,	kIx,
starat	starat	k5eAaImF
se	se	k3xPyFc4
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
vzdělání	vzdělání	k1gNnSc2
slovenského	slovenský	k2eAgInSc2d1
lidu	lid	k1gInSc2
a	a	k8xC
pozdvihovat	pozdvihovat	k5eAaImF
národní	národní	k2eAgNnSc4d1
sebevědomí	sebevědomí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
ač	ač	k8xS
jen	jen	k9
částečně	částečně	k6eAd1
dle	dle	k7c2
daných	daný	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
okolností	okolnost	k1gFnPc2
<g/>
,	,	kIx,
také	také	k9
MS	MS	kA
plnila	plnit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc7d1
brzdou	brzda	k1gFnSc7
její	její	k3xOp3gFnSc2
činnosti	činnost	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
stalo	stát	k5eAaPmAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
nesměla	smět	k5eNaImAgFnS
zakládat	zakládat	k5eAaImF
místní	místní	k2eAgFnPc4d1
pobočky	pobočka	k1gFnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
předpokládalo	předpokládat	k5eAaImAgNnS
v	v	k7c6
původním	původní	k2eAgInSc6d1
návrhu	návrh	k1gInSc6
stanov	stanova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tak	tak	k6eAd1
si	se	k3xPyFc3
však	však	k9
Matice	matice	k1gFnPc1
za	za	k7c4
krátký	krátký	k2eAgInSc4d1
čas	čas	k1gInSc4
získala	získat	k5eAaPmAgFnS
autoritu	autorita	k1gFnSc4
vrcholné	vrcholný	k2eAgFnSc2d1
celonárodní	celonárodní	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
mohla	moct	k5eAaImAgFnS
reprezentovat	reprezentovat	k5eAaImF
národ	národ	k1gInSc4
i	i	k9
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
a	a	k8xC
navazovat	navazovat	k5eAaImF
styky	styk	k1gInPc4
s	s	k7c7
kulturními	kulturní	k2eAgFnPc7d1
a	a	k8xC
vědeckými	vědecký	k2eAgFnPc7d1
institucemi	instituce	k1gFnPc7
dalších	další	k2eAgNnPc2d1
<g/>
,	,	kIx,
hlavně	hlavně	k9
slovanských	slovanský	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zhodnocení	zhodnocení	k1gNnSc1
prvních	první	k4xOgMnPc2
12	#num#	k4
let	léto	k1gNnPc2
činnosti	činnost	k1gFnSc2
</s>
<s>
Za	za	k7c4
prvních	první	k4xOgInPc2
12	#num#	k4
let	léto	k1gNnPc2
svého	svůj	k3xOyFgInSc2
působení	působení	k1gNnSc6
si	se	k3xPyFc3
Matice	matice	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
téměř	téměř	k6eAd1
stotisícovou	stotisícový	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
dárců	dárce	k1gMnPc2
<g/>
,	,	kIx,
příznivců	příznivec	k1gMnPc2
a	a	k8xC
spolupracovníků	spolupracovník	k1gMnPc2
a	a	k8xC
vybudovala	vybudovat	k5eAaPmAgFnS
z	z	k7c2
Martina	Martin	k1gMnSc2
kulturní	kulturní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1874	#num#	k4
měla	mít	k5eAaImAgFnS
Matice	matice	k1gFnSc1
1200	#num#	k4
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterými	který	k3yIgMnPc7,k3yRgMnPc7,k3yQgMnPc7
bylo	být	k5eAaImAgNnS
66	#num#	k4
kolektivních	kolektivní	k2eAgInPc2d1
členů	člen	k1gInPc2
jako	jako	k8xC,k8xS
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
města	město	k1gNnSc2
<g/>
,	,	kIx,
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
sdružení	sdružení	k1gNnSc1
apod.	apod.	kA
Po	po	k7c6
celém	celý	k2eAgNnSc6d1
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
i	i	k8xC
v	v	k7c6
Uhersku	Uhersko	k1gNnSc6
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
vybudovanou	vybudovaný	k2eAgFnSc4d1
širokou	široký	k2eAgFnSc4d1
síť	síť	k1gFnSc4
místních	místní	k2eAgMnPc2d1
„	„	k?
<g/>
jednatelů	jednatel	k1gMnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
starali	starat	k5eAaImAgMnP
o	o	k7c4
rozšiřování	rozšiřování	k1gNnSc4
jejího	její	k3xOp3gInSc2
tisku	tisk	k1gInSc2
a	a	k8xC
kulturně-osvětového	kulturně-osvětový	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
v	v	k7c6
daném	daný	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1875	#num#	k4
vydala	vydat	k5eAaPmAgFnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
F.	F.	kA
V.	V.	kA
Sasinka	Sasinka	k1gFnSc1
12	#num#	k4
ročníků	ročník	k1gInPc2
"	"	kIx"
<g/>
Letopisu	letopis	k1gInSc2
Matice	matice	k1gFnSc2
slovenskej	slovenskej	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgInS
v	v	k7c6
podstatě	podstata	k1gFnSc6
první	první	k4xOgMnSc1
vědecký	vědecký	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
to	ten	k3xDgNnSc4
vydala	vydat	k5eAaPmAgFnS
41	#num#	k4
knižních	knižní	k2eAgInPc2d1
titulů	titul	k1gInPc2
nejrozličnějšího	rozličný	k2eAgNnSc2d3
zaměření	zaměření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
též	též	k9
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
rozvoji	rozvoj	k1gInSc3
ochotnického	ochotnický	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
a	a	k8xC
sborového	sborový	k2eAgInSc2d1
zpěvu	zpěv	k1gInSc2
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
prostřednictvím	prostřednictvím	k7c2
místních	místní	k2eAgFnPc2d1
"	"	kIx"
<g/>
jednatelů	jednatel	k1gMnPc2
<g/>
"	"	kIx"
založit	založit	k5eAaPmF
řadu	řada	k1gFnSc4
ochotnických	ochotnický	k2eAgInPc2d1
souborů	soubor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
významnému	významný	k2eAgInSc3d1
okruhu	okruh	k1gInSc3
působení	působení	k1gNnSc2
MS	MS	kA
patřila	patřit	k5eAaImAgFnS
i	i	k9
sběratelská	sběratelský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
nahrazovala	nahrazovat	k5eAaImAgFnS
úlohu	úloha	k1gFnSc4
národního	národní	k2eAgNnSc2d1
muzejnictví	muzejnictví	k1gNnSc2
<g/>
,	,	kIx,
archivnictví	archivnictví	k1gNnSc2
<g/>
,	,	kIx,
galerie	galerie	k1gFnSc2
a	a	k8xC
knihovny	knihovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fond	fond	k1gInSc4
její	její	k3xOp3gFnSc2
knihovny	knihovna	k1gFnSc2
se	se	k3xPyFc4
rozrostl	rozrůst	k5eAaPmAgInS
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
10	#num#	k4
000	#num#	k4
svazků	svazek	k1gInPc2
<g/>
;	;	kIx,
v	v	k7c6
literárním	literární	k2eAgInSc6d1
archivu	archiv	k1gInSc6
opatrovala	opatrovat	k5eAaImAgFnS
332	#num#	k4
rukopisů	rukopis	k1gInPc2
a	a	k8xC
velmi	velmi	k6eAd1
se	se	k3xPyFc4
rozrostly	rozrůst	k5eAaPmAgFnP
sbírky	sbírka	k1gFnPc1
jejich	jejich	k3xOp3gInPc2
archeologických	archeologický	k2eAgInPc2d1
<g/>
,	,	kIx,
numismatických	numismatický	k2eAgInPc2d1
<g/>
,	,	kIx,
přírodovědných	přírodovědný	k2eAgNnPc2d1
a	a	k8xC
výtvarných	výtvarný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
vyvinula	vyvinout	k5eAaPmAgFnS
velké	velký	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
i	i	k9
na	na	k7c4
poli	pole	k1gFnSc4
organizování	organizování	k1gNnSc2
vědecké	vědecký	k2eAgFnSc2d1
výzkumné	výzkumný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
jediná	jediný	k2eAgFnSc1d1
celonárodní	celonárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
udržovala	udržovat	k5eAaImAgFnS
čilé	čilý	k2eAgInPc4d1
kulturní	kulturní	k2eAgInPc4d1
a	a	k8xC
vědecké	vědecký	k2eAgInPc4d1
styky	styk	k1gInPc4
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
slovanskými	slovanský	k2eAgInPc7d1
ústavy	ústav	k1gInPc7
podobného	podobný	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Matice	matice	k1gFnSc1
byla	být	k5eAaImAgFnS
financována	financovat	k5eAaBmNgFnS
výlučně	výlučně	k6eAd1
z	z	k7c2
vlastních	vlastní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
získávala	získávat	k5eAaImAgFnS
z	z	k7c2
členských	členský	k2eAgInPc2d1
příspěvků	příspěvek	k1gInPc2
a	a	k8xC
ze	z	k7c2
sbírek	sbírka	k1gFnPc2
mezi	mezi	k7c7
slovenským	slovenský	k2eAgInSc7d1
lidem	lid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
maďarské	maďarský	k2eAgFnPc1d1
celonárodní	celonárodní	k2eAgFnPc1d1
vědecké	vědecký	k2eAgFnPc1d1
a	a	k8xC
kulturní	kulturní	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
dostávaly	dostávat	k5eAaImAgFnP
od	od	k7c2
vlády	vláda	k1gFnSc2
i	i	k9
sto	sto	k4xCgNnSc1
či	či	k8xC
dvěstětisícové	dvěstětisícový	k2eAgFnPc1d1
dotace	dotace	k1gFnPc1
ročně	ročně	k6eAd1
<g/>
,	,	kIx,
MS	MS	kA
za	za	k7c4
celý	celý	k2eAgInSc4d1
čas	čas	k1gInSc4
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
nedostala	dostat	k5eNaPmAgFnS
vůbec	vůbec	k9
nic	nic	k3yNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
však	však	k9
byla	být	k5eAaImAgFnS
již	již	k6eAd1
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
vystavena	vystavit	k5eAaPmNgFnS
všlikému	všliký	k2eAgNnSc3d1
příkoří	příkoří	k1gNnSc3
ze	z	k7c2
strany	strana	k1gFnSc2
vládnoucí	vládnoucí	k2eAgFnSc2d1
maďarské	maďarský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
–	–	k?
a	a	k8xC
roku	rok	k1gInSc2
1875	#num#	k4
byl	být	k5eAaImAgInS
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
vynesen	vynesen	k2eAgInSc1d1
definitivní	definitivní	k2eAgInSc1d1
ortel	ortel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zánik	zánik	k1gInSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
nařízením	nařízení	k1gNnSc7
z	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1875	#num#	k4
nejenže	nejenže	k6eAd1
zakázalo	zakázat	k5eAaPmAgNnS
činnost	činnost	k1gFnSc4
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
zabavilo	zabavit	k5eAaPmAgNnS
i	i	k9
všechen	všechen	k3xTgInSc4
její	její	k3xOp3gInSc4
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obranné	obranný	k2eAgFnPc4d1
akce	akce	k1gFnPc4
ze	z	k7c2
strany	strana	k1gFnSc2
jejího	její	k3xOp3gNnSc2
vedení	vedení	k1gNnSc2
i	i	k9
ze	z	k7c2
se	se	k3xPyFc4
strany	strana	k1gFnPc1
celé	celý	k2eAgFnSc2d1
slovenské	slovenský	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
byly	být	k5eAaImAgFnP
bezvýsledné	bezvýsledný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
interpelaci	interpelace	k1gFnSc6
srbského	srbský	k2eAgMnSc2d1
poslance	poslanec	k1gMnSc2
v	v	k7c6
uherském	uherský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
Svetozára	Svetozár	k1gMnSc2
Miletića	Miletićus	k1gMnSc2
ve	v	k7c6
věci	věc	k1gFnSc6
zastavení	zastavení	k1gNnSc1
činnosti	činnost	k1gFnSc2
MS	MS	kA
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Koloman	Koloman	k1gMnSc1
Tisza	Tisz	k1gMnSc4
odpověděl	odpovědět	k5eAaPmAgMnS
velmi	velmi	k6eAd1
výstižně	výstižně	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
on	on	k3xPp3gMnSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
žádný	žádný	k3yNgInSc4
slovenský	slovenský	k2eAgInSc4d1
národ	národ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prohlásil	prohlásit	k5eAaPmAgMnS
též	též	k9
<g/>
,	,	kIx,
že	že	k8xS
činnost	činnost	k1gFnSc1
Matice	matice	k1gFnSc1
neodpovídala	odpovídat	k5eNaImAgFnS
jejím	její	k3xOp3gInSc7
stanovám	stanova	k1gFnPc3
a	a	k8xC
byla	být	k5eAaImAgFnS
protivlastenecká	protivlastenecký	k2eAgFnSc1d1
a	a	k8xC
protistátní	protistátní	k2eAgFnSc1d1
a	a	k8xC
své	svůj	k3xOyFgNnSc4
vyjádření	vyjádření	k1gNnSc4
zakončil	zakončit	k5eAaPmAgMnS
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Dokud	dokud	k8xS
budu	být	k5eAaImBp1nS
na	na	k7c6
tomto	tento	k3xDgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
s	s	k7c7
každým	každý	k3xTgInSc7
spolkem	spolek	k1gInSc7
podobného	podobný	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
zúčtuji	zúčtovat	k5eAaPmIp1nS
vždy	vždy	k6eAd1
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Obnovení	obnovení	k1gNnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
Matica	Matic	k2eAgFnSc1d1
slovenská	slovenský	k2eAgFnSc1d1
obnovila	obnovit	k5eAaPmAgFnS
až	až	k9
po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1919	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
rozhodnutí	rozhodnutí	k1gNnSc2
Vavra	Vavr	k1gMnSc2
Šrobára	Šrobár	k1gMnSc2
<g/>
,	,	kIx,
ministra	ministr	k1gMnSc2
pro	pro	k7c4
správu	správa	k1gFnSc4
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sídla	sídlo	k1gNnPc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
budova	budova	k1gFnSc1
</s>
<s>
Pamětní	pamětní	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
budově	budova	k1gFnSc6
MS	MS	kA
</s>
<s>
Původní	původní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1863	#num#	k4
<g/>
–	–	k?
<g/>
1875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
podle	podle	k7c2
projektu	projekt	k1gInSc2
Jana	Jan	k1gMnSc2
Nepomuka	Nepomuk	k1gMnSc2
Bobule	bobule	k1gFnSc2
z	z	k7c2
darů	dar	k1gInPc2
slovenského	slovenský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastavěná	zastavěný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
1	#num#	k4
189	#num#	k4
m²	m²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
pozdně	pozdně	k6eAd1
klasicistní	klasicistní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
byla	být	k5eAaImAgFnS
začátkem	začátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
dočasně	dočasně	k6eAd1
upravena	upravit	k5eAaPmNgFnS
pro	pro	k7c4
státní	státní	k2eAgFnPc4d1
instituce	instituce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1962	#num#	k4
národní	národní	k2eAgFnSc7d1
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
areálu	areál	k1gInSc6
budovy	budova	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
socha	socha	k1gFnSc1
S.	S.	kA
H.	H.	kA
Vajanského	Vajanský	k2eAgMnSc4d1
od	od	k7c2
Františka	František	k1gMnSc2
Úprky	úprk	k1gInPc4
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
umístěna	umístit	k5eAaPmNgFnS
před	před	k7c7
2	#num#	k4
<g/>
.	.	kIx.
budovou	budova	k1gFnSc7
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budově	budova	k1gFnSc6
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
umístěna	umístit	k5eAaPmNgFnS
stálá	stálý	k2eAgFnSc1d1
expozice	expozice	k1gFnSc1
Slovenského	slovenský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
literárního	literární	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
budova	budova	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
budova	budova	k1gFnSc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1924	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
je	být	k5eAaImIp3nS
bohatě	bohatě	k6eAd1
členěná	členěný	k2eAgFnSc1d1
ve	v	k7c6
hmotě	hmota	k1gFnSc6
třemi	tři	k4xCgInPc7
hlubokými	hluboký	k2eAgInPc7d1
rizality	rizalit	k1gInPc7
na	na	k7c6
fasádách	fasáda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
podle	podle	k7c2
projektu	projekt	k1gInSc2
Jana	Jan	k1gMnSc2
Palkoviče	Palkovič	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastavěná	zastavěný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
1	#num#	k4
737	#num#	k4
m²	m²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
je	být	k5eAaImIp3nS
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
budovou	budova	k1gFnSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
socha	socha	k1gFnSc1
národního	národní	k2eAgMnSc2d1
umělce	umělec	k1gMnSc2
Jána	Ján	k1gMnSc2
Kulicha	Kulich	k1gMnSc2
"	"	kIx"
<g/>
Matice	matice	k1gFnSc1
slovenská	slovenský	k2eAgFnSc1d1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
ze	z	k7c2
svařované	svařovaný	k2eAgFnSc2d1
oceli	ocel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
budova	budova	k1gFnSc1
</s>
<s>
Třetí	třetí	k4xOgFnSc1
budova	budova	k1gFnSc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1963	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
sídlo	sídlo	k1gNnSc4
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vymezení	vymezení	k1gNnSc6
majetku	majetek	k1gInSc2
mezi	mezi	k7c7
Maticí	matice	k1gFnSc7
slovenskou	slovenský	k2eAgFnSc7d1
a	a	k8xC
Slovenskou	slovenský	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
<g/>
,	,	kIx,
slouží	sloužit	k5eAaImIp3nS
3	#num#	k4
<g/>
.	.	kIx.
budova	budova	k1gFnSc1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
Slovenské	slovenský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matice	matice	k1gFnSc1
slovenská	slovenský	k2eAgFnSc1d1
momentálně	momentálně	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
opět	opět	k6eAd1
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
budově	budova	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Předsedové	předseda	k1gMnPc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
</s>
<s>
1863	#num#	k4
<g/>
–	–	k?
<g/>
1869	#num#	k4
Štefan	Štefan	k1gMnSc1
Moyzes	Moyzes	k1gMnSc1
</s>
<s>
1870	#num#	k4
<g/>
–	–	k?
<g/>
1875	#num#	k4
Jozef	Jozef	k1gMnSc1
Kozáček	kozáček	k1gMnSc1
</s>
<s>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
Matúš	Matúš	k1gInSc1
Dula	dout	k5eAaImAgFnS
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
Pavol	Pavola	k1gFnPc2
Országh	Országh	k1gMnSc1
Hviezdoslav	Hviezdoslav	k1gMnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
František	František	k1gMnSc1
Richard	Richard	k1gMnSc1
Osvald	Osvald	k1gMnSc1
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
Vavro	Vavro	k1gNnSc4
Šrobár	Šrobár	k1gInSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
Juraj	Juraj	k1gMnSc1
Janoška	Janoška	k1gMnSc1
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
Marián	Marián	k1gMnSc1
Blaha	Blaha	k1gMnSc1
(	(	kIx(
<g/>
1869	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
Ján	Ján	k1gMnSc1
Vanovič	Vanovič	k1gMnSc1
(	(	kIx(
<g/>
1856	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
Jozef	Jozef	k1gMnSc1
Országh	Országh	k1gMnSc1
(	(	kIx(
<g/>
1883	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
Jur	jura	k1gFnPc2
Hronec	Hronec	k1gInSc4
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
1968	#num#	k4
<g/>
–	–	k?
<g/>
19731974	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
čestný	čestný	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Laco	Laco	k1gMnSc1
Novomeský	Novomeský	k2eAgMnSc1d1
</s>
<s>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Mináč	Mináč	k1gMnSc1
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
leden	leden	k1gInSc1
<g/>
–	–	k?
<g/>
srpen	srpen	k1gInSc1
<g/>
)	)	kIx)
Viliam	Viliam	k1gMnSc1
Gruska	Grusek	k1gMnSc2
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
Jozef	Jozef	k1gMnSc1
Markuš	Markuš	k1gMnSc1
</s>
<s>
2010-2017	2010-2017	k4
Marián	Marián	k1gMnSc1
Tkáč	tkáč	k1gMnSc1
</s>
<s>
2017-	2017-	k4
Marián	Marián	k1gMnSc1
Gešper	Gešper	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matičiari	Matičiar	k1gMnPc1
si	se	k3xPyFc3
zvolili	zvolit	k5eAaPmAgMnP
nového	nový	k2eAgMnSc4d1
predsedu	predseda	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuality	aktualita	k1gFnPc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-11-11	2017-11-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Matica	Matica	k1gFnSc1
slovenská	slovenský	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Matica	Matic	k2eAgFnSc1d1
slovenská	slovenský	k2eAgFnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010710540	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1006268-3	1006268-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0860	#num#	k4
4106	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80113700	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
3386156012434149700008	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80113700	#num#	k4
</s>
