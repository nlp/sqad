<s>
Vyvrcholením	vyvrcholení	k1gNnSc7
cyrilometodějských	cyrilometodějský	k2eAgFnPc2d1
oslav	oslava	k1gFnPc2
bylo	být	k5eAaImAgNnS
první	první	k4xOgNnSc1
valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
Matice	matice	k1gFnSc2
slovenské	slovenský	k2eAgFnSc2d1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1863	#num#	k4
v	v	k7c6
Martine	Martin	k1gInSc5
<g/>
,	,	kIx,
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
císař	císař	k1gMnSc1
František	František	k1gMnSc1
Josef	Josef	k1gMnSc1
I.	I.	kA
koncem	koncem	k7c2
května	květen	k1gInSc2
potvrdil	potvrdit	k5eAaPmAgMnS
stanovy	stanova	k1gFnPc4
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>