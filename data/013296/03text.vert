<p>
<s>
CPython	CPython	k1gInSc1	CPython
je	být	k5eAaImIp3nS	být
referenční	referenční	k2eAgFnSc1d1	referenční
implementace	implementace	k1gFnSc1	implementace
interpretu	interpret	k1gMnSc3	interpret
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Pythonu	Python	k1gMnSc3	Python
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
zároveň	zároveň	k6eAd1	zároveň
o	o	k7c6	o
implementaci	implementace	k1gFnSc6	implementace
nejrozšířejnější	rozšířejný	k2eAgFnSc6d3	rozšířejný
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
nazývána	nazývat	k5eAaImNgFnS	nazývat
stručně	stručně	k6eAd1	stručně
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
podobný	podobný	k2eAgInSc1d1	podobný
název	název	k1gInSc1	název
Cython	Cythona	k1gFnPc2	Cythona
označuje	označovat	k5eAaImIp3nS	označovat
jiný	jiný	k2eAgInSc1d1	jiný
projekt	projekt	k1gInSc1	projekt
–	–	k?	–
interpret	interpret	k1gMnSc1	interpret
Pythonu	Python	k1gMnSc3	Python
rozšířeného	rozšířený	k2eAgNnSc2d1	rozšířené
o	o	k7c4	o
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
jazyka	jazyk	k1gInSc2	jazyk
C.	C.	kA	C.
Sám	sám	k3xTgMnSc1	sám
CPython	CPython	k1gMnSc1	CPython
je	být	k5eAaImIp3nS	být
naprogramovaný	naprogramovaný	k2eAgInSc1d1	naprogramovaný
v	v	k7c6	v
C	C	kA	C
a	a	k8xC	a
před	před	k7c7	před
spuštěním	spuštění	k1gNnSc7	spuštění
si	se	k3xPyFc3	se
pythonový	pythonový	k2eAgInSc1d1	pythonový
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
překládá	překládat	k5eAaImIp3nS	překládat
do	do	k7c2	do
bajtkódu	bajtkód	k1gInSc2	bajtkód
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
obecně	obecně	k6eAd1	obecně
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
–	–	k?	–
v	v	k7c4	v
některým	některý	k3yIgNnPc3	některý
testech	test	k1gInPc6	test
ho	on	k3xPp3gNnSc4	on
překonává	překonávat	k5eAaImIp3nS	překonávat
RPython	RPython	k1gInSc1	RPython
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Jython	Jython	k1gInSc1	Jython
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
u	u	k7c2	u
krátkých	krátký	k2eAgInPc2d1	krátký
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgNnPc2	který
může	moct	k5eAaImIp3nS	moct
nahrání	nahrání	k1gNnSc4	nahrání
javovského	javovský	k2eAgInSc2d1	javovský
virtuálního	virtuální	k2eAgInSc2d1	virtuální
stroje	stroj	k1gInSc2	stroj
zabrat	zabrat	k5eAaPmF	zabrat
čas	čas	k1gInSc4	čas
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
výrazné	výrazný	k2eAgFnPc4d1	výrazná
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
CPythonu	CPython	k1gInSc2	CPython
patří	patřit	k5eAaImIp3nS	patřit
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
využívá	využívat	k5eAaPmIp3nS	využívat
globální	globální	k2eAgInSc1d1	globální
zámek	zámek	k1gInSc1	zámek
interpretu	interpret	k1gMnSc3	interpret
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
interní	interní	k2eAgFnSc2d1	interní
proměnné	proměnná	k1gFnSc2	proměnná
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
běžet	běžet	k5eAaImF	běžet
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
okamžik	okamžik	k1gInSc4	okamžik
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
vlákno	vlákno	k1gNnSc1	vlákno
–	–	k?	–
interpret	interpret	k1gMnSc1	interpret
tedy	tedy	k9	tedy
neumí	umět	k5eNaImIp3nS	umět
sám	sám	k3xTgMnSc1	sám
efektivně	efektivně	k6eAd1	efektivně
plně	plně	k6eAd1	plně
využít	využít	k5eAaPmF	využít
moderní	moderní	k2eAgInPc1d1	moderní
vícejádrové	vícejádrový	k2eAgInPc1d1	vícejádrový
procesory	procesor	k1gInPc1	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
interprety	interpret	k1gMnPc7	interpret
Pythonu	Python	k1gMnSc3	Python
tím	ten	k3xDgNnSc7	ten
CPython	CPython	k1gNnSc1	CPython
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
(	(	kIx(	(
<g/>
stejné	stejný	k2eAgNnSc1d1	stejné
omezení	omezení	k1gNnSc1	omezení
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
Stackless	Stackless	k1gInSc1	Stackless
Python	Python	k1gMnSc1	Python
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
interprety	interpret	k1gMnPc4	interpret
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
omezení	omezení	k1gNnSc2	omezení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jython	Jython	k1gInSc1	Jython
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
CPython	CPythona	k1gFnPc2	CPythona
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Společná	společný	k2eAgFnSc1d1	společná
domácí	domácí	k2eAgFnSc1d1	domácí
stránka	stránka	k1gFnSc1	stránka
Pythonu	Python	k1gMnSc3	Python
a	a	k8xC	a
CPythonu	CPython	k1gMnSc3	CPython
</s>
</p>
