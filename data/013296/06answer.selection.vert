<s>
Naopak	naopak	k6eAd1	naopak
podobný	podobný	k2eAgInSc1d1	podobný
název	název	k1gInSc1	název
Cython	Cythona	k1gFnPc2	Cythona
označuje	označovat	k5eAaImIp3nS	označovat
jiný	jiný	k2eAgInSc1d1	jiný
projekt	projekt	k1gInSc1	projekt
–	–	k?	–
interpret	interpret	k1gMnSc1	interpret
Pythonu	Python	k1gMnSc3	Python
rozšířeného	rozšířený	k2eAgNnSc2d1	rozšířené
o	o	k7c4	o
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
jazyka	jazyk	k1gInSc2	jazyk
C.	C.	kA	C.
Sám	sám	k3xTgMnSc1	sám
CPython	CPython	k1gMnSc1	CPython
je	být	k5eAaImIp3nS	být
naprogramovaný	naprogramovaný	k2eAgInSc1d1	naprogramovaný
v	v	k7c6	v
C	C	kA	C
a	a	k8xC	a
před	před	k7c7	před
spuštěním	spuštění	k1gNnSc7	spuštění
si	se	k3xPyFc3	se
pythonový	pythonový	k2eAgInSc1d1	pythonový
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
překládá	překládat	k5eAaImIp3nS	překládat
do	do	k7c2	do
bajtkódu	bajtkód	k1gInSc2	bajtkód
<g/>
.	.	kIx.	.
</s>
