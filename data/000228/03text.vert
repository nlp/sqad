<s>
Slunečnice	slunečnice	k1gFnSc1	slunečnice
je	být	k5eAaImIp3nS	být
šesté	šestý	k4xOgNnSc1	šestý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
české	český	k2eAgFnSc2d1	Česká
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Lucie	Lucie	k1gFnSc2	Lucie
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
autorských	autorský	k2eAgNnPc2d1	autorské
(	(	kIx(	(
<g/>
jen	jen	k9	jen
u	u	k7c2	u
tří	tři	k4xCgFnPc2	tři
písní	píseň	k1gFnPc2	píseň
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
spoluautoři	spoluautor	k1gMnPc1	spoluautor
uvedeni	uveden	k2eAgMnPc1d1	uveden
externí	externí	k2eAgMnPc1d1	externí
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kompilační	kompilační	k2eAgNnSc4d1	kompilační
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Lucie	Lucie	k1gFnSc1	Lucie
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
tého	tého	k6eAd1	tého
desky	deska	k1gFnPc1	deska
zařazeny	zařazen	k2eAgFnPc1d1	zařazena
písně	píseň	k1gFnSc2	píseň
Ona	onen	k3xDgFnSc1	onen
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
Zakousnutej	Zakousnutej	k?	Zakousnutej
do	do	k7c2	do
tebe	ty	k3xPp2nSc2	ty
a	a	k8xC	a
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pojď	jít	k5eAaImRp2nS	jít
se	se	k3xPyFc4	se
mnou	já	k3xPp1nSc7	já
do	do	k7c2	do
chrámu	chrám	k1gInSc2	chrám
ticha	ticho	k1gNnSc2	ticho
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodym	k1gInSc4	Kodym
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodym	k1gInSc1	Kodym
<g/>
)	)	kIx)	)
Ona	onen	k3xDgFnSc1	onen
ví	vědět	k5eAaImIp3nS	vědět
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodym	k1gInSc1	Kodym
<g/>
)	)	kIx)	)
Zakousnutej	Zakousnutej	k?	Zakousnutej
do	do	k7c2	do
tebe	ty	k3xPp2nSc2	ty
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
P.	P.	kA	P.
<g/>
B.	B.	kA	B.
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodym	k1gInSc1	Kodym
<g/>
)	)	kIx)	)
Hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Miloš	Miloš	k1gMnSc1	Miloš
Hájíček	hájíček	k1gInSc4	hájíček
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Miloš	Miloš	k1gMnSc1	Miloš
Hájíček	hájíček	k1gInSc1	hájíček
<g/>
)	)	kIx)	)
Veverka	veverka	k1gFnSc1	veverka
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodymum	k1gNnPc2	Kodymum
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Slunečnice	slunečnice	k1gFnSc1	slunečnice
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Karlík	Karlík	k1gMnSc1	Karlík
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
)	)	kIx)	)
Karibi	Karibi	k1gNnSc1	Karibi
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodym	k1gInSc4	Kodym
<g/>
,	,	kIx,	,
text	text	k1gInSc4	text
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Mládek	Mládek	k1gMnSc1	Mládek
<g/>
)	)	kIx)	)
Mohu	moct	k5eAaImIp1nS	moct
tě	ty	k3xPp2nSc4	ty
jenom	jenom	k9	jenom
milovat	milovat	k5eAaImF	milovat
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodym	k1gInSc1	Kodym
<g/>
)	)	kIx)	)
Noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Tomáš	Tomáš	k1gMnSc1	Tomáš
Belko	Belko	k1gNnSc4	Belko
<g/>
)	)	kIx)	)
Vůně	vůně	k1gFnSc1	vůně
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
P.	P.	kA	P.
<g/>
B.	B.	kA	B.
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
Kodym	Kodym	k1gInSc1	Kodym
<g/>
)	)	kIx)	)
Touha	touha	k1gFnSc1	touha
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
<g/>
)	)	kIx)	)
Michal	Michal	k1gMnSc1	Michal
Dvořák	Dvořák	k1gMnSc1	Dvořák
Robert	Roberta	k1gFnPc2	Roberta
Kodym	Kodym	k1gInSc1	Kodym
David	David	k1gMnSc1	David
Koller	Koller	k1gMnSc1	Koller
P.	P.	kA	P.
<g/>
B.	B.	kA	B.
<g/>
Ch	Ch	kA	Ch
<g/>
.	.	kIx.	.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Belko	Belko	k1gNnSc4	Belko
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kurzweil	Kurzweil	k1gMnSc1	Kurzweil
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
(	(	kIx(	(
<g/>
dechy	dech	k1gInPc1	dech
<g/>
)	)	kIx)	)
Milan	Milan	k1gMnSc1	Milan
Cimfe	Cimf	k1gInSc5	Cimf
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
el.	el.	k?	el.
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
)	)	kIx)	)
Miloš	Miloš	k1gMnSc1	Miloš
Hájíčekl	Hájíčekl	k1gMnSc1	Hájíčekl
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
samply	sampnout	k5eAaPmAgFnP	sampnout
<g/>
,	,	kIx,	,
aranžmá	aranžmá	k1gNnSc1	aranžmá
<g/>
)	)	kIx)	)
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanákl	Hanákl	k1gInSc1	Hanákl
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
Pavel	Pavel	k1gMnSc1	Pavel
Karlík	Karlík	k1gMnSc1	Karlík
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
Vráťa	Vráťa	k1gMnSc1	Vráťa
Kocourekl	Kocourekl	k1gMnSc1	Kocourekl
(	(	kIx(	(
<g/>
samply	sampnout	k5eAaPmAgFnP	sampnout
<g/>
)	)	kIx)	)
Adam	Adam	k1gMnSc1	Adam
Koller	Koller	k1gMnSc1	Koller
(	(	kIx(	(
<g/>
congo	congo	k1gMnSc1	congo
<g/>
)	)	kIx)	)
Luboš	Luboš	k1gMnSc1	Luboš
Krtička	Krtička	k1gMnSc1	Krtička
(	(	kIx(	(
<g/>
trubka	trubka	k1gFnSc1	trubka
<g/>
)	)	kIx)	)
Libor	Libor	k1gMnSc1	Libor
Mikoška	Mikošek	k1gMnSc2	Mikošek
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Mládek	Mládek	k1gMnSc1	Mládek
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
banjo	banjo	k1gNnSc1	banjo
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
P.	P.	kA	P.
Muchow	Muchow	k1gMnSc1	Muchow
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
aranžmá	aranžmá	k1gNnSc1	aranžmá
<g/>
)	)	kIx)	)
Vláďa	Vláďa	k1gMnSc1	Vláďa
Pecha	Pecha	k1gMnSc1	Pecha
(	(	kIx(	(
<g/>
programování	programování	k1gNnSc1	programování
<g/>
)	)	kIx)	)
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
didgeridoo	didgeridoo	k1gMnSc1	didgeridoo
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
přehledu	přehled	k1gInSc2	přehled
prodejnosti	prodejnost	k1gFnSc2	prodejnost
zveřejněného	zveřejněný	k2eAgMnSc2d1	zveřejněný
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
dne	den	k1gInSc2	den
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
<g/>
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
na	na	k7c6	na
s.	s.	k?	s.
2	[number]	k4	2
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
vydání	vydání	k1gNnSc2	vydání
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
cca	cca	kA	cca
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
alba	album	k1gNnSc2	album
Slunečnice	slunečnice	k1gFnSc2	slunečnice
prodalo	prodat	k5eAaPmAgNnS	prodat
62	[number]	k4	62
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
českou	český	k2eAgFnSc7d1	Česká
deskou	deska	k1gFnSc7	deska
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
místo	místo	k7c2	místo
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Těžkej	těžkat	k5eAaImRp2nS	těžkat
Pokondr	Pokondr	k1gInSc4	Pokondr
s	s	k7c7	s
albem	album	k1gNnSc7	album
Ježek	Ježek	k1gMnSc1	Ježek
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
69	[number]	k4	69
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
skupina	skupina	k1gFnSc1	skupina
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
publiku	publikum	k1gNnSc3	publikum
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Slunečnice	slunečnice	k1gFnSc2	slunečnice
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
odehrála	odehrát	k5eAaPmAgFnS	odehrát
během	během	k7c2	během
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
2001	[number]	k4	2001
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
koncertů	koncert	k1gInPc2	koncert
vesměs	vesměs	k6eAd1	vesměs
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
arénách	aréna	k1gFnPc6	aréna
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
