<s>
Cep	cep	k1gInSc1	cep
je	být	k5eAaImIp3nS	být
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
nářadí	nářadí	k1gNnSc4	nářadí
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
až	až	k6eAd1	až
do	do	k7c2	do
novověku	novověk	k1gInSc2	novověk
k	k	k7c3	k
mlácení	mlácení	k1gNnSc3	mlácení
obilí	obilí	k1gNnSc2	obilí
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
oddělení	oddělení	k1gNnSc2	oddělení
zrna	zrno	k1gNnSc2	zrno
od	od	k7c2	od
plev	pleva	k1gFnPc2	pleva
<g/>
.	.	kIx.	.
</s>
