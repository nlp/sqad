<p>
<s>
Cep	cep	k1gInSc1	cep
je	být	k5eAaImIp3nS	být
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
nářadí	nářadí	k1gNnSc4	nářadí
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
až	až	k6eAd1	až
do	do	k7c2	do
novověku	novověk	k1gInSc2	novověk
k	k	k7c3	k
mlácení	mlácení	k1gNnSc3	mlácení
obilí	obilí	k1gNnSc2	obilí
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
oddělení	oddělení	k1gNnSc2	oddělení
zrna	zrno	k1gNnSc2	zrno
od	od	k7c2	od
plev	pleva	k1gFnPc2	pleva
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
násady	násada	k1gFnSc2	násada
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
závěsu	závěs	k1gInSc6	závěs
umístěno	umístit	k5eAaPmNgNnS	umístit
asi	asi	k9	asi
40	[number]	k4	40
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
břevno	břevno	k1gNnSc1	břevno
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
hlava	hlava	k1gFnSc1	hlava
cepu	cep	k1gInSc2	cep
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okovaný	okovaný	k2eAgInSc1d1	okovaný
cep	cep	k1gInSc1	cep
sloužil	sloužit	k5eAaImAgInS	sloužit
také	také	k9	také
jako	jako	k9	jako
zbraň	zbraň	k1gFnSc1	zbraň
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
husitských	husitský	k2eAgFnPc2d1	husitská
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
selských	selský	k2eAgFnPc2d1	selská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technologie	technologie	k1gFnSc1	technologie
sklizně	sklizeň	k1gFnSc2	sklizeň
obilí	obilí	k1gNnSc2	obilí
==	==	k?	==
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
sklizně	sklizeň	k1gFnSc2	sklizeň
obilí	obilí	k1gNnSc2	obilí
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
žně	žeň	k1gFnSc2	žeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žňová	žňový	k2eAgFnSc1d1	žňová
technologie	technologie	k1gFnSc1	technologie
sklizně	sklizeň	k1gFnSc2	sklizeň
byla	být	k5eAaImAgFnS	být
zhruba	zhruba	k6eAd1	zhruba
následující	následující	k2eAgFnSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Obilí	obilí	k1gNnSc1	obilí
bylo	být	k5eAaImAgNnS	být
nejprve	nejprve	k6eAd1	nejprve
pokoseno	pokosit	k5eAaPmNgNnS	pokosit
srpem	srp	k1gInSc7	srp
<g/>
,	,	kIx,	,
kosou	kosý	k2eAgFnSc7d1	kosá
či	či	k8xC	či
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
sekačkou	sekačka	k1gFnSc7	sekačka
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
bývala	bývat	k5eAaImAgFnS	bývat
obvykle	obvykle	k6eAd1	obvykle
tažena	táhnout	k5eAaImNgFnS	táhnout
koňmi	kůň	k1gMnPc7	kůň
nebo	nebo	k8xC	nebo
kravkou	kravka	k1gFnSc7	kravka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pokosené	pokosený	k2eAgNnSc1d1	pokosené
obilí	obilí	k1gNnSc1	obilí
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
svázáno	svázat	k5eAaPmNgNnS	svázat
do	do	k7c2	do
snopů	snop	k1gInPc2	snop
(	(	kIx(	(
<g/>
t.j.	t.j.	k?	t.j.
<g/>
,	,	kIx,	,
svazků	svazek	k1gInPc2	svazek
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
stébel	stéblo	k1gNnPc2	stéblo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
snopů	snop	k1gInPc2	snop
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
postaveny	postavit	k5eAaPmNgInP	postavit
tzv.	tzv.	kA	tzv.
panáky	panák	k1gInPc1	panák
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obilí	obilí	k1gNnSc1	obilí
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
dosoušelo	dosoušet	k5eAaImAgNnS	dosoušet
</s>
</p>
<p>
<s>
Panáky	panák	k1gMnPc4	panák
byl	být	k5eAaImAgInS	být
rozebrány	rozebrán	k2eAgMnPc4d1	rozebrán
a	a	k8xC	a
snopy	snop	k1gInPc1	snop
byly	být	k5eAaImAgInP	být
vidlemi	vidle	k1gFnPc7	vidle
naloženy	naložit	k5eAaPmNgInP	naložit
na	na	k7c4	na
vůz	vůz	k1gInSc4	vůz
(	(	kIx(	(
<g/>
žebřiňák	žebřiňák	k1gInSc4	žebřiňák
<g/>
)	)	kIx)	)
a	a	k8xC	a
odvezeny	odvezen	k2eAgInPc4d1	odvezen
ke	k	k7c3	k
stodole	stodola	k1gFnSc3	stodola
</s>
</p>
<p>
<s>
Sklizené	sklizený	k2eAgNnSc1d1	sklizené
obilí	obilí	k1gNnSc1	obilí
(	(	kIx(	(
<g/>
klasy	klas	k1gInPc4	klas
svázané	svázaný	k2eAgInPc4d1	svázaný
do	do	k7c2	do
snopů	snop	k1gInPc2	snop
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
uskladněno	uskladnit	k5eAaPmNgNnS	uskladnit
ve	v	k7c6	v
stodole	stodola	k1gFnSc6	stodola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
snopů	snop	k1gInPc2	snop
bylo	být	k5eAaImAgNnS	být
položeno	položit	k5eAaPmNgNnS	položit
na	na	k7c4	na
mlat	mlat	k1gInSc4	mlat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
zadaném	zadaný	k2eAgInSc6d1	zadaný
rytmu	rytmus	k1gInSc6	rytmus
(	(	kIx(	(
<g/>
k	k	k7c3	k
dodržení	dodržení	k1gNnSc3	dodržení
rytmu	rytmus	k1gInSc2	rytmus
byly	být	k5eAaImAgFnP	být
deklamovány	deklamován	k2eAgFnPc1d1	deklamován
říkačky	říkačka	k1gFnPc1	říkačka
–	–	k?	–
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
mlatců	mlatec	k1gInPc2	mlatec
<g/>
:	:	kIx,	:
čtyřcepní	čtyřcepnit	k5eAaPmIp3nS	čtyřcepnit
<g/>
,	,	kIx,	,
pěticepní	pěticepnit	k5eAaPmIp3nS	pěticepnit
<g/>
,	,	kIx,	,
šesticepní	šesticepnit	k5eAaPmIp3nS	šesticepnit
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
lehce	lehko	k6eAd1	lehko
bušilo	bušit	k5eAaImAgNnS	bušit
cepy	cep	k1gInPc4	cep
–	–	k?	–
krouživým	krouživý	k2eAgInSc7d1	krouživý
pohybem	pohyb	k1gInSc7	pohyb
hlavy	hlava	k1gFnSc2	hlava
cepu	cep	k1gInSc2	cep
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nárazy	náraz	k1gInPc1	náraz
hlavy	hlava	k1gFnSc2	hlava
cepu	cep	k1gInSc2	cep
na	na	k7c4	na
obilní	obilní	k2eAgFnPc4d1	obilní
klasy	klasa	k1gFnPc4	klasa
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
zrní	zrní	k1gNnSc2	zrní
a	a	k8xC	a
plev	pleva	k1gFnPc2	pleva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
propadly	propadnout	k5eAaPmAgFnP	propadnout
vymlácenou	vymlácený	k2eAgFnSc7d1	vymlácená
slámou	sláma	k1gFnSc7	sláma
na	na	k7c4	na
mlat	mlat	k1gInSc4	mlat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sláma	sláma	k1gFnSc1	sláma
byla	být	k5eAaImAgFnS	být
uskladněna	uskladnit	k5eAaPmNgFnS	uskladnit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
jako	jako	k8xC	jako
stelivo	stelivo	k1gNnSc4	stelivo
a	a	k8xC	a
směs	směs	k1gFnSc4	směs
plev	pleva	k1gFnPc2	pleva
a	a	k8xC	a
zrní	zrní	k1gNnSc2	zrní
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
oddělována	oddělovat	k5eAaImNgFnS	oddělovat
přehazováním	přehazování	k1gNnSc7	přehazování
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgInSc1d1	dopravní
prostředek	prostředek	k1gInSc1	prostředek
k	k	k7c3	k
pneumatické	pneumatický	k2eAgFnSc3d1	pneumatická
dopravě	doprava	k1gFnSc3	doprava
sena	seno	k1gNnSc2	seno
a	a	k8xC	a
slámy	sláma	k1gFnSc2	sláma
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
fukar	fukar	k1gInSc1	fukar
<g/>
.	.	kIx.	.
<g/>
Cep	cep	k1gInSc1	cep
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
mnohem	mnohem	k6eAd1	mnohem
efektivnějších	efektivní	k2eAgFnPc2d2	efektivnější
strojních	strojní	k2eAgFnPc2d1	strojní
mechanických	mechanický	k2eAgFnPc2d1	mechanická
mlátiček	mlátička	k1gFnPc2	mlátička
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
obilních	obilní	k2eAgInPc2d1	obilní
kombajnů	kombajn	k1gInPc2	kombajn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bojové	bojový	k2eAgNnSc1d1	bojové
použití	použití	k1gNnSc1	použití
cepu	cep	k1gInSc2	cep
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byly	být	k5eAaImAgInP	být
okované	okovaný	k2eAgInPc1d1	okovaný
cepy	cep	k1gInPc1	cep
používány	používat	k5eAaImNgInP	používat
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
efektivních	efektivní	k2eAgFnPc2d1	efektivní
zbraní	zbraň	k1gFnPc2	zbraň
husitskými	husitský	k2eAgNnPc7d1	husitské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvořili	tvořit	k5eAaImAgMnP	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
součást	součást	k1gFnSc4	součást
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
uměli	umět	k5eAaImAgMnP	umět
dobře	dobře	k6eAd1	dobře
zacházet	zacházet	k5eAaImF	zacházet
a	a	k8xC	a
zbraň	zbraň	k1gFnSc4	zbraň
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
bojovat	bojovat	k5eAaImF	bojovat
mimo	mimo	k7c4	mimo
dosah	dosah	k1gInSc4	dosah
mečů	meč	k1gInPc2	meč
útočníků	útočník	k1gMnPc2	útočník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cepy	cep	k1gInPc4	cep
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kosami	kosa	k1gFnPc7	kosa
a	a	k8xC	a
vidlemi	vidle	k1gFnPc7	vidle
tvořily	tvořit	k5eAaImAgFnP	tvořit
improvizovanou	improvizovaný	k2eAgFnSc4d1	improvizovaná
výzbroj	výzbroj	k1gFnSc4	výzbroj
sedláků	sedlák	k1gMnPc2	sedlák
i	i	k8xC	i
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
selských	selský	k2eAgFnPc2d1	selská
bouří	bouř	k1gFnPc2	bouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cep	cep	k1gInSc1	cep
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cep	cep	k1gInSc1	cep
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
