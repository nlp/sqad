<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Bláha	Bláha	k1gMnSc1	Bláha
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Venda	Venda	k1gMnSc1	Venda
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupin	skupina	k1gFnPc2	skupina
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
a	a	k8xC	a
Medvěd	medvěd	k1gMnSc1	medvěd
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Povoláním	povolání	k1gNnSc7	povolání
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgMnSc1d1	umělecký
truhlář	truhlář	k1gMnSc1	truhlář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Bláha	Bláha	k1gMnSc1	Bláha
(	(	kIx(	(
<g/>
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
)	)	kIx)	)
</s>
</p>
