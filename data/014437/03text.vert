<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Tramvaj	tramvaj	k1gFnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
</s>
<s>
městský	městský	k2eAgInSc1d1
autobus	autobus	k1gInSc1
typu	typ	k1gInSc2
MAN	mana	k1gFnPc2
</s>
<s>
Kloubový	kloubový	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
autobus	autobus	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
(	(	kIx(
<g/>
MHD	MHD	kA
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
zkráceně	zkráceně	k6eAd1
jen	jen	k9
městská	městský	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
linek	linka	k1gFnPc2
osobní	osobní	k2eAgFnSc2d1
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
určených	určený	k2eAgFnPc2d1
k	k	k7c3
zajišťování	zajišťování	k1gNnSc3
dopravní	dopravní	k2eAgFnSc2d1
obsluhy	obsluha	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
hromadnými	hromadný	k2eAgInPc7d1
dopravními	dopravní	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
mívají	mívat	k5eAaImIp3nP
města	město	k1gNnPc1
vlastní	vlastní	k2eAgInPc4d1
integrovaný	integrovaný	k2eAgInSc4d1
dopravní	dopravní	k2eAgInSc4d1
systém	systém	k1gInSc4
(	(	kIx(
<g/>
buď	buď	k8xC
pro	pro	k7c4
samotné	samotný	k2eAgNnSc4d1
území	území	k1gNnSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
i	i	k9
pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
okolí	okolí	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
platí	platit	k5eAaImIp3nP
jednotné	jednotný	k2eAgFnPc1d1
přepravní	přepravní	k2eAgFnPc1d1
a	a	k8xC
tarifní	tarifní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
provoz	provoz	k1gInSc1
linek	linka	k1gFnPc2
MHD	MHD	kA
je	být	k5eAaImIp3nS
dotován	dotovat	k5eAaBmNgInS
městem	město	k1gNnSc7
jednotným	jednotný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
však	však	k9
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
linky	linka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nejsou	být	k5eNaImIp3nP
součástí	součást	k1gFnSc7
takového	takový	k3xDgInSc2
integrovaného	integrovaný	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
mají	mít	k5eAaImIp3nP
vlastní	vlastní	k2eAgFnPc1d1
<g/>
,	,	kIx,
odlišné	odlišný	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
nebo	nebo	k8xC
nejsou	být	k5eNaImIp3nP
dotované	dotovaný	k2eAgFnPc4d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1
systém	systém	k1gInSc1
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
může	moct	k5eAaImIp3nS
zahrnovat	zahrnovat	k5eAaImF
autobusovou	autobusový	k2eAgFnSc4d1
<g/>
,	,	kIx,
tramvajovou	tramvajový	k2eAgFnSc4d1
či	či	k8xC
trolejbusovou	trolejbusový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
městech	město	k1gNnPc6
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
miliónem	milión	k4xCgInSc7
obyvatel	obyvatel	k1gMnPc2
zpravidla	zpravidla	k6eAd1
také	také	k9
metro	metro	k1gNnSc4
a	a	k8xC
městskou	městský	k2eAgFnSc4d1
nebo	nebo	k8xC
příměstskou	příměstský	k2eAgFnSc4d1
železnici	železnice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
i	i	k9
lanovky	lanovka	k1gFnPc1
<g/>
,	,	kIx,
nekonvenční	konvenční	k2eNgFnPc1d1
dráhy	dráha	k1gFnPc1
(	(	kIx(
<g/>
visuté	visutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
na	na	k7c6
magnetickém	magnetický	k2eAgInSc6d1
polštáři	polštář	k1gInSc6
apod.	apod.	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
přívozy	přívoz	k1gInPc1
či	či	k8xC
jiné	jiný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
vodní	vodní	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
v	v	k7c6
některých	některý	k3yIgInPc6
krajích	kraj	k1gInPc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
převládá	převládat	k5eAaImIp3nS
snaha	snaha	k1gFnSc1
propojit	propojit	k5eAaPmF
městskou	městský	k2eAgFnSc4d1
a	a	k8xC
regionální	regionální	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
a	a	k8xC
zmenšit	zmenšit	k5eAaPmF
rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
nimi	on	k3xPp3gNnPc7
vytvářením	vytváření	k1gNnSc7
a	a	k8xC
rozvojem	rozvoj	k1gInSc7
integrovaných	integrovaný	k2eAgInPc2d1
dopravních	dopravní	k2eAgInPc2d1
systémů	systém	k1gInPc2
(	(	kIx(
<g/>
IDS	IDS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
doplněna	doplnit	k5eAaPmNgFnS
též	též	k6eAd1
příměstskou	příměstský	k2eAgFnSc4d1
<g/>
,	,	kIx,
meziměstskou	meziměstský	k2eAgFnSc4d1
a	a	k8xC
nehromadnou	hromadný	k2eNgFnSc4d1
dopravou	doprava	k1gFnSc7
a	a	k8xC
provázána	provázán	k2eAgFnSc1d1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc4d1
přepravní	přepravní	k2eAgInPc4d1
uzly	uzel	k1gInPc4
MHD	MHD	kA
se	se	k3xPyFc4
zřizují	zřizovat	k5eAaImIp3nP
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
přepravních	přepravní	k2eAgInPc2d1
uzlů	uzel	k1gInPc2
meziměstské	meziměstský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
(	(	kIx(
<g/>
nádraží	nádraží	k1gNnSc1
<g/>
,	,	kIx,
autobusová	autobusový	k2eAgNnPc4d1
nádraží	nádraží	k1gNnPc4
<g/>
,	,	kIx,
letiště	letiště	k1gNnPc4
<g/>
,	,	kIx,
přístavy	přístav	k1gInPc1
významné	významný	k2eAgInPc1d1
pro	pro	k7c4
osobní	osobní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
blízkosti	blízkost	k1gFnSc6
přepravních	přepravní	k2eAgInPc2d1
uzlů	uzel	k1gInPc2
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
nebo	nebo	k8xC
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
rámci	rámec	k1gInSc6
se	se	k3xPyFc4
zřizují	zřizovat	k5eAaImIp3nP
stanoviště	stanoviště	k1gNnPc1
taxislužby	taxislužba	k1gFnSc2
<g/>
,	,	kIx,
záchytná	záchytný	k2eAgNnPc1d1
parkoviště	parkoviště	k1gNnPc1
(	(	kIx(
<g/>
P	P	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
místa	místo	k1gNnSc2
pro	pro	k7c4
přestup	přestup	k1gInSc4
z	z	k7c2
automobilové	automobilový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
na	na	k7c4
hromadnou	hromadný	k2eAgFnSc4d1
(	(	kIx(
<g/>
K	K	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
úložiště	úložiště	k1gNnSc2
jízdních	jízdní	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
(	(	kIx(
<g/>
B	B	kA
<g/>
+	+	kIx~
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
nemotorovými	motorový	k2eNgNnPc7d1,k2eAgNnPc7d1
vozidly	vozidlo	k1gNnPc7
na	na	k7c6
bázi	báze	k1gFnSc6
potahových	potahový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
nebo	nebo	k8xC
jízdních	jízdní	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
(	(	kIx(
<g/>
rikša	rikša	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rovněž	rovněž	k9
nemívá	mívat	k5eNaImIp3nS
dnes	dnes	k6eAd1
povahu	povaha	k1gFnSc4
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
českých	český	k2eAgNnPc6d1
městech	město	k1gNnPc6
jen	jen	k6eAd1
jako	jako	k8xS,k8xC
turistická	turistický	k2eAgFnSc1d1
atrakce	atrakce	k1gFnSc1
a	a	k8xC
nemá	mít	k5eNaImIp3nS
klíčový	klíčový	k2eAgInSc4d1
dopravní	dopravní	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozvojových	rozvojový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
však	však	k9
i	i	k9
dopravní	dopravní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
s	s	k7c7
animální	animální	k2eAgFnSc7d1
trakcí	trakce	k1gFnSc7
jsou	být	k5eAaImIp3nP
dodnes	dodnes	k6eAd1
významnou	významný	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
ostatně	ostatně	k6eAd1
tak	tak	k6eAd1
začínala	začínat	k5eAaImAgFnS
i	i	k9
evropská	evropský	k2eAgFnSc1d1
veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
(	(	kIx(
<g/>
omnibus	omnibus	k1gInSc1
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
i	i	k8xC
dálková	dálkový	k2eAgFnSc1d1
koněspřežná	koněspřežný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
pojmu	pojem	k1gInSc2
MHD	MHD	kA
v	v	k7c6
Československu	Československo	k1gNnSc6
</s>
<s>
Ve	v	k7c6
větších	veliký	k2eAgNnPc6d2
městech	město	k1gNnPc6
provozovali	provozovat	k5eAaImAgMnP
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
začátkem	začátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
veřejnou	veřejný	k2eAgFnSc4d1
hromadnou	hromadný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
jak	jak	k8xS,k8xC
soukromí	soukromý	k2eAgMnPc1d1
dopravci	dopravce	k1gMnPc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
podniky	podnik	k1gInPc1
vlastněné	vlastněný	k2eAgInPc1d1
obcemi	obec	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
striktnímu	striktní	k2eAgNnSc3d1
oddělení	oddělení	k1gNnSc3
městské	městský	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
od	od	k7c2
příměstské	příměstský	k2eAgFnSc2d1
a	a	k8xC
meziměstské	meziměstský	k2eAgFnSc2d1
došlo	dojít	k5eAaPmAgNnS
zejména	zejména	k9
znárodňovacím	znárodňovací	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
č.	č.	k?
311	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
národních	národní	k2eAgInPc6d1
dopravních	dopravní	k2eAgInPc6d1
podnicích	podnik	k1gInPc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
monopolizoval	monopolizovat	k5eAaBmAgInS
veřejnou	veřejný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
provozování	provozování	k1gNnSc4
městské	městský	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
vyhradil	vyhradit	k5eAaPmAgMnS
komunálním	komunální	k2eAgMnSc7d1
(	(	kIx(
<g/>
obecním	obecní	k2eAgInPc3d1
<g/>
)	)	kIx)
podnikům	podnik	k1gInPc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ostatní	ostatní	k2eAgFnSc4d1
autobusovou	autobusový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
podnikům	podnik	k1gInPc3
ČSAD	ČSAD	kA
<g/>
.	.	kIx.
</s>
<s>
Krokem	krokem	k6eAd1
k	k	k7c3
integraci	integrace	k1gFnSc3
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
byla	být	k5eAaImAgFnS
vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
364	#num#	k4
<g/>
/	/	kIx~
<g/>
1953	#num#	k4
Ú.	Ú.	kA
l.	l.	k?
(	(	kIx(
<g/>
novela	novela	k1gFnSc1
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1963	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
se	se	k3xPyFc4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
přepravní	přepravní	k2eAgInSc1d1
řád	řád	k1gInSc1
pro	pro	k7c4
městskou	městský	k2eAgFnSc4d1
hromadnou	hromadný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
osob	osoba	k1gFnPc2
(	(	kIx(
<g/>
městský	městský	k2eAgInSc1d1
přepravní	přepravní	k2eAgInSc1d1
řád	řád	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
vyhláška	vyhláška	k1gFnSc1
127	#num#	k4
<g/>
/	/	kIx~
<g/>
1964	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
městském	městský	k2eAgInSc6d1
přepravním	přepravní	k2eAgInSc6d1
řádu	řád	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
však	však	k9
městskou	městský	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
ještě	ještě	k9
více	hodně	k6eAd2
vzdálily	vzdálit	k5eAaPmAgInP
dopravě	doprava	k1gFnSc3
ČSAD	ČSAD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technické	technický	k2eAgFnSc2d1
oborové	oborový	k2eAgFnSc2d1
normy	norma	k1gFnSc2
ON	on	k3xPp3gMnSc1
73	#num#	k4
6425	#num#	k4
a	a	k8xC
ON	on	k3xPp3gMnSc1
73	#num#	k4
6426	#num#	k4
z	z	k7c2
počátku	počátek	k1gInSc2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
dokonce	dokonce	k9
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
zakazovaly	zakazovat	k5eAaImAgInP
zřizovat	zřizovat	k5eAaImF
společné	společný	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
pro	pro	k7c4
MHD	MHD	kA
a	a	k8xC
ČSAD	ČSAD	kA
(	(	kIx(
<g/>
nová	nový	k2eAgFnSc1d1
ČSN	ČSN	kA
73	#num#	k4
6425	#num#	k4
z	z	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
to	ten	k3xDgNnSc1
naopak	naopak	k6eAd1
doporučuje	doporučovat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
123	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
městské	městský	k2eAgFnSc6d1
hromadné	hromadný	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
,	,	kIx,
též	též	k9
jako	jako	k9
prováděcí	prováděcí	k2eAgInSc1d1
předpis	předpis	k1gInSc1
k	k	k7c3
tehdy	tehdy	k6eAd1
novému	nový	k2eAgInSc3d1
Zákonu	zákon	k1gInSc3
o	o	k7c6
silniční	silniční	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
a	a	k8xC
vnitrostátním	vnitrostátní	k2eAgNnSc6d1
zasilatelství	zasilatelství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
MHD	MHD	kA
definován	definovat	k5eAaBmNgInS
v	v	k7c6
§	§	k?
2	#num#	k4
vyhlášky	vyhláška	k1gFnSc2
č.	č.	k?
175	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
přepravním	přepravní	k2eAgInSc6d1
řádu	řád	k1gInSc6
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
drážní	drážní	k2eAgFnSc4d1
a	a	k8xC
silniční	silniční	k2eAgFnSc4d1
osobní	osobní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
použit	použít	k5eAaPmNgInS
i	i	k9
například	například	k6eAd1
ve	v	k7c6
vyhlášce	vyhláška	k1gFnSc6
č.	č.	k?
30	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
v	v	k7c6
popisu	popis	k1gInSc6
dopravní	dopravní	k2eAgFnSc2d1
značky	značka	k1gFnSc2
a	a	k8xC
v	v	k7c6
ČSN	ČSN	kA
73	#num#	k4
6425	#num#	k4
Autobusové	autobusový	k2eAgInPc1d1
<g/>
,	,	kIx,
tramvajové	tramvajový	k2eAgFnSc2d1
a	a	k8xC
trolejbusové	trolejbusový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
historii	historie	k1gFnSc6
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
v	v	k7c6
článcích	článek	k1gInPc6
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
a	a	k8xC
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
</s>
<s>
Důležitost	důležitost	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
města	město	k1gNnPc4
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
50	#num#	k4
000	#num#	k4
obyvateli	obyvatel	k1gMnPc7
je	být	k5eAaImIp3nS
pravidelná	pravidelný	k2eAgFnSc1d1
městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
nezbytností	nezbytnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Dnešní	dnešní	k2eAgNnPc1d1
velkoměsta	velkoměsto	k1gNnPc1
by	by	k9
bez	bez	k7c2
městské	městský	k2eAgFnSc2d1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
nemohla	moct	k5eNaImAgFnS
existovat	existovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
je	být	k5eAaImIp3nS
většinou	většina	k1gFnSc7
městských	městský	k2eAgFnPc2d1
správ	správa	k1gFnPc2
podporována	podporován	k2eAgFnSc1d1
<g/>
,	,	kIx,
hlavně	hlavně	k9
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
nejen	nejen	k6eAd1
finančními	finanční	k2eAgFnPc7d1
dotacemi	dotace	k1gFnPc7
(	(	kIx(
<g/>
zakázkami	zakázka	k1gFnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
omezováním	omezování	k1gNnSc7
automobilové	automobilový	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
mýtným	mýtné	k1gNnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
pěšími	pěší	k2eAgFnPc7d1
zónami	zóna	k1gFnPc7
v	v	k7c6
centrech	centrum	k1gNnPc6
velkých	velký	k2eAgFnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
studie	studie	k1gFnSc2
amerických	americký	k2eAgFnPc2d1
universit	universita	k1gFnPc2
se	se	k3xPyFc4
ale	ale	k9
ulice	ulice	k1gFnSc1
měst	město	k1gNnPc2
méně	málo	k6eAd2
stávají	stávat	k5eAaImIp3nP
globálně	globálně	k6eAd1
méně	málo	k6eAd2
propojené	propojený	k2eAgFnPc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znevýhodňuje	znevýhodňovat	k5eAaImIp3nS
hromadnou	hromadný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výhody	výhoda	k1gFnPc1
</s>
<s>
Výhodou	výhoda	k1gFnSc7
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
oproti	oproti	k7c3
automobilové	automobilový	k2eAgFnSc3d1
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
míra	míra	k1gFnSc1
znečištění	znečištění	k1gNnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
hluku	hluk	k1gInSc2
a	a	k8xC
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
všem	všecek	k3xTgInPc3
(	(	kIx(
<g/>
dětem	dítě	k1gFnPc3
<g/>
,	,	kIx,
méně	málo	k6eAd2
majetným	majetný	k2eAgMnPc3d1
<g/>
,	,	kIx,
zdravotně	zdravotně	k6eAd1
postiženým	postižený	k2eAgMnPc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
je	být	k5eAaImIp3nS
2,5	2,5	k4
<g/>
krát	krát	k6eAd1
méně	málo	k6eAd2
energeticky	energeticky	k6eAd1
náročná	náročný	k2eAgFnSc1d1
než	než	k8xS
individuální	individuální	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Přeprava	přeprava	k1gFnSc1
MHD	MHD	kA
je	být	k5eAaImIp3nS
bezpečnější	bezpečný	k2eAgFnSc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7
MHD	MHD	kA
je	být	k5eAaImIp3nS
především	především	k9
nižší	nízký	k2eAgFnSc1d2
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
prostředky	prostředek	k1gInPc1
MHD	MHD	kA
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
individuální	individuální	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
zastavují	zastavovat	k5eAaImIp3nP
v	v	k7c6
zastávkách	zastávka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hustém	hustý	k2eAgInSc6d1
městském	městský	k2eAgInSc6d1
provozu	provoz	k1gInSc6
je	být	k5eAaImIp3nS
často	často	k6eAd1
MHD	MHD	kA
omezována	omezovat	k5eAaImNgFnS
zejména	zejména	k6eAd1
automobilovou	automobilový	k2eAgFnSc4d1
dopravou	doprava	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
prostorově	prostorově	k6eAd1
náročnější	náročný	k2eAgMnSc1d2
a	a	k8xC
častěji	často	k6eAd2
v	v	k7c6
ní	on	k3xPp3gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
zácpám	zácpa	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
MHD	MHD	kA
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
často	často	k6eAd1
nespravedlivě	spravedlivě	k6eNd1
zdržována	zdržován	k2eAgFnSc1d1
auty	aut	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
tramvajová	tramvajový	k2eAgFnSc1d1
trať	trať	k1gFnSc1
může	moct	k5eAaImIp3nS
přepravit	přepravit	k5eAaPmF
stejně	stejně	k6eAd1
lidí	člověk	k1gMnPc2
jako	jako	k8xS,k8xC
čtyřpruhová	čtyřpruhový	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
oddělení	oddělení	k1gNnSc1
tramvajového	tramvajový	k2eAgInSc2d1
pásu	pás	k1gInSc2
od	od	k7c2
aut	auto	k1gNnPc2
může	moct	k5eAaImIp3nS
pro	pro	k7c4
MHD	MHD	kA
přinést	přinést	k5eAaPmF
jako	jako	k9
výhodu	výhoda	k1gFnSc4
zvýšenou	zvýšený	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
méně	málo	k6eAd2
osídlených	osídlený	k2eAgFnPc6d1
částech	část	k1gFnPc6
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgNnPc6
obdobích	období	k1gNnPc6
dne	den	k1gInSc2
a	a	k8xC
týdne	týden	k1gInSc2
buď	buď	k8xC
k	k	k7c3
nedostatečnému	dostatečný	k2eNgNnSc3d1
zajištění	zajištění	k1gNnSc3
dopravní	dopravní	k2eAgFnSc2d1
obslužnosti	obslužnost	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
naopak	naopak	k6eAd1
ke	k	k7c3
zbytečnému	zbytečný	k2eAgNnSc3d1
plýtvání	plýtvání	k1gNnSc1
veřejnými	veřejný	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
a	a	k8xC
zatěžování	zatěžování	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
provozem	provoz	k1gInSc7
prázdných	prázdný	k2eAgNnPc2d1
nebo	nebo	k8xC
poloprázdných	poloprázdný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Prostředky	prostředek	k1gInPc1
hromadné	hromadný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
terčem	terč	k1gInSc7
teroristických	teroristický	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
města	město	k1gNnPc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgNnP
snížit	snížit	k5eAaPmF
riziko	riziko	k1gNnSc4
stavebním	stavební	k2eAgNnSc7d1
řešením	řešení	k1gNnSc7
(	(	kIx(
<g/>
metro	metro	k1gNnSc1
v	v	k7c6
Kodani	Kodaň	k1gFnSc6
<g/>
)	)	kIx)
či	či	k8xC
doplňkovým	doplňkový	k2eAgNnPc3d1
zařízením	zařízení	k1gNnPc3
(	(	kIx(
<g/>
odpadkové	odpadkový	k2eAgInPc1d1
koše	koš	k1gInPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
lépe	dobře	k6eAd2
odolávající	odolávající	k2eAgInSc4d1
výbuchu	výbuch	k1gInSc3
<g/>
,	,	kIx,
turnikety	turniket	k1gInPc4
odhalující	odhalující	k2eAgFnSc2d1
útočníka	útočník	k1gMnSc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<	<	kIx(
</s>
<s>
Hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
místem	místo	k1gNnSc7
přenosu	přenos	k1gInSc2
nemocí	nemoc	k1gFnPc2
v	v	k7c6
případě	případ	k1gInSc6
epidemie	epidemie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Financování	financování	k1gNnSc1
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
většinou	většinou	k6eAd1
dotována	dotovat	k5eAaBmNgFnS
z	z	k7c2
veřejných	veřejný	k2eAgInPc2d1
rozpočtů	rozpočet	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
státem	stát	k1gInSc7
<g/>
,	,	kIx,
územními	územní	k2eAgInPc7d1
samosprávnými	samosprávný	k2eAgInPc7d1
celky	celek	k1gInPc7
nebo	nebo	k8xC
městy	město	k1gNnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnosti	společnost	k1gFnSc6
provozující	provozující	k2eAgFnSc4d1
veřejnou	veřejný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
zakládány	zakládat	k5eAaImNgInP
veřejnoprávními	veřejnoprávní	k2eAgFnPc7d1
korporacemi	korporace	k1gFnPc7
(	(	kIx(
<g/>
státem	stát	k1gInSc7
<g/>
,	,	kIx,
územními	územní	k2eAgInPc7d1
samosprávnými	samosprávný	k2eAgInPc7d1
celky	celek	k1gInPc7
nebo	nebo	k8xC
městy	město	k1gNnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
někde	někde	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
i	i	k9
soukromá	soukromý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
Glasgow	Glasgow	k1gInSc4
či	či	k8xC
některá	některý	k3yIgNnPc4
města	město	k1gNnPc4
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výdaje	výdaj	k1gInSc2
na	na	k7c4
městskou	městský	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
největší	veliký	k2eAgFnPc4d3
položky	položka	k1gFnPc4
městských	městský	k2eAgInPc2d1
rozpočtů	rozpočet	k1gInPc2
a	a	k8xC
rostou	růst	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
<g/>
,	,	kIx,
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
větší	veliký	k2eAgFnSc2d2
<g/>
;	;	kIx,
u	u	k7c2
velkoměst	velkoměsto	k1gNnPc2
tyto	tento	k3xDgInPc1
náklady	náklad	k1gInPc1
představují	představovat	k5eAaImIp3nP
astronomické	astronomický	k2eAgFnPc4d1
sumy	suma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
má	mít	k5eAaImIp3nS
městskou	městský	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
většina	většina	k1gFnSc1
okresních	okresní	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
měst	město	k1gNnPc2
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
15	#num#	k4
000	#num#	k4
obyvateli	obyvatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://phys.org/news/2020-01-street-network-patterns-reveal-worldwide.html	https://phys.org/news/2020-01-street-network-patterns-reveal-worldwide.html	k1gInSc1
-	-	kIx~
Street	Street	k1gInSc1
network	network	k1gInSc2
patterns	patterns	k6eAd1
reveal	reveat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
worrying	worrying	k1gInSc1
worldwide	worldwid	k1gInSc5
trend	trend	k1gInSc1
towards	towardsa	k1gFnPc2
urban	urban	k1gMnSc1
sprawl	sprawl	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Městská	městský	k2eAgFnSc1d1
autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
aglomeraci	aglomerace	k1gFnSc6
</s>
<s>
Příměstská	příměstský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
(	(	kIx(
<g/>
S-Bahn	S-Bahn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Trolejbusová	trolejbusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Metro	metro	k1gNnSc1
</s>
<s>
Integrovaný	integrovaný	k2eAgInSc1d1
dopravní	dopravní	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
MHD	MHD	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Diskusní	diskusní	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
o	o	k7c6
městské	městský	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
městské	městský	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Stránky	stránka	k1gFnPc1
o	o	k7c6
MHD	MHD	kA
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Citytrans	Citytrans	k1gInSc1
-	-	kIx~
informace	informace	k1gFnPc1
zejména	zejména	k9
o	o	k7c6
městské	městský	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
</s>
<s>
Stránky	stránka	k1gFnPc1
o	o	k7c6
MHD	MHD	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
i	i	k8xC
jiných	jiný	k2eAgInPc6d1
českých	český	k2eAgInPc6d1
a	a	k8xC
slovenských	slovenský	k2eAgInPc6d1
městech	město	k1gNnPc6
Archivováno	archivován	k2eAgNnSc4d1
9	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnPc1
z	z	k7c2
některých	některý	k3yIgInPc2
provozů	provoz	k1gInPc2
MHD	MHD	kA
</s>
<s>
MHD	MHD	kA
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Městská	městský	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
-	-	kIx~
IBM	IBM	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Veřejná	veřejný	k2eAgFnSc1d1
a	a	k8xC
městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
silniční	silniční	k2eAgFnSc2d1
</s>
<s>
omnibus	omnibus	k1gInSc1
•	•	k?
autobusová	autobusový	k2eAgFnSc1d1
(	(	kIx(
<g/>
autobus	autobus	k1gInSc1
•	•	k?
metrobus	metrobus	k1gMnSc1
•	•	k?
Ekobus	Ekobus	k1gMnSc1
•	•	k?
cyklobus	cyklobus	k1gMnSc1
•	•	k?
skibus	skibus	k1gMnSc1
•	•	k?
radiobus	radiobus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
trolejbus	trolejbus	k1gInSc1
kolejová	kolejový	k2eAgNnPc5d1
</s>
<s>
tramvajová	tramvajový	k2eAgFnSc1d1
</s>
<s>
tramvaj	tramvaj	k1gFnSc1
(	(	kIx(
<g/>
koňská	koňský	k2eAgFnSc1d1
•	•	k?
parní	parní	k2eAgFnSc1d1
•	•	k?
plynová	plynový	k2eAgFnSc1d1
•	•	k?
pneumatická	pneumatický	k2eAgFnSc1d1
•	•	k?
dieselová	dieselový	k2eAgFnSc1d1
•	•	k?
elektrická	elektrický	k2eAgFnSc1d1
•	•	k?
vodíková	vodíkový	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
rychlodrážní	rychlodrážní	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
•	•	k?
vlakotramvaj	vlakotramvaj	k1gFnSc1
•	•	k?
podpovrchová	podpovrchový	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
</s>
<s>
metro	metro	k1gNnSc1
(	(	kIx(
<g/>
lehké	lehký	k2eAgNnSc1d1
<g/>
)	)	kIx)
•	•	k?
meziměstská	meziměstský	k2eAgFnSc1d1
tramvaj	tramvaj	k1gFnSc1
•	•	k?
příměstská	příměstský	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
(	(	kIx(
<g/>
Esko	eska	k1gFnSc5
•	•	k?
S-Bahn	S-Bahn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
vodní	vodní	k2eAgFnSc1d1
</s>
<s>
městský	městský	k2eAgInSc1d1
přívoz	přívoz	k1gInSc1
•	•	k?
vodní	vodní	k2eAgFnSc4d1
tramvaj	tramvaj	k1gFnSc4
městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
•	•	k?
linková	linkový	k2eAgFnSc1d1
(	(	kIx(
<g/>
příměstská	příměstský	k2eAgFnSc1d1
•	•	k?
soukromá	soukromý	k2eAgFnSc1d1
•	•	k?
regionální	regionální	k2eAgFnSc1d1
•	•	k?
kyvadlová	kyvadlový	k2eAgFnSc1d1
•	•	k?
náhradní	náhradní	k2eAgInPc1d1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Doprava	doprava	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4043176-9	4043176-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85141331	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85141331	#num#	k4
</s>
