<p>
<s>
Ethyl-acetát	Ethylcetát	k1gInSc1	Ethyl-acetát
nebo	nebo	k8xC	nebo
ethylester	ethylester	k1gInSc1	ethylester
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
zvaný	zvaný	k2eAgInSc1d1	zvaný
i	i	k8xC	i
octan	octan	k1gInSc1	octan
ethylnatý	ethylnatý	k2eAgInSc1d1	ethylnatý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
s	s	k7c7	s
funkčním	funkční	k2eAgInSc7d1	funkční
vzorcem	vzorec	k1gInSc7	vzorec
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COOCH	COOCH	kA	COOCH
<g/>
2	[number]	k4	2
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ester	ester	k1gInSc4	ester
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
kapalina	kapalina	k1gFnSc1	kapalina
má	mít	k5eAaImIp3nS	mít
charakteristickou	charakteristický	k2eAgFnSc7d1	charakteristická
sladkou	sladký	k2eAgFnSc7d1	sladká
vůní	vůně	k1gFnSc7	vůně
připomínající	připomínající	k2eAgFnSc2d1	připomínající
některá	některý	k3yIgNnPc1	některý
lepidla	lepidlo	k1gNnPc1	lepidlo
a	a	k8xC	a
odstraňovače	odstraňovač	k1gInPc1	odstraňovač
laku	lak	k1gInSc2	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
obsažena	obsažen	k2eAgFnSc1d1	obsažena
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
jako	jako	k8xS	jako
rozpouštědlo	rozpouštědlo	k1gNnSc4	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
dohromady	dohromady	k6eAd1	dohromady
ročně	ročně	k6eAd1	ročně
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
cca	cca	kA	cca
400	[number]	k4	400
tisíc	tisíc	k4xCgInSc1	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
výroba	výroba	k1gFnSc1	výroba
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
na	na	k7c4	na
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
octová	octový	k2eAgFnSc1d1	octová
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
ethanolem	ethanol	k1gInSc7	ethanol
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
ethylacetátu	ethylacetát	k1gInSc2	ethylacetát
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CH3COOH	CH3COOH	k4	CH3COOH
+	+	kIx~	+
CH3CH2OH	CH3CH2OH	k1gMnSc1	CH3CH2OH
⇌	⇌	k?	⇌
CH3COOCH2CH3	CH3COOCH2CH3	k1gMnSc1	CH3COOCH2CH3
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
OVýsledný	OVýsledný	k2eAgInSc1d1	OVýsledný
produkt	produkt	k1gInSc1	produkt
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
odebírat	odebírat	k5eAaImF	odebírat
protože	protože	k8xS	protože
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
rovnovážná	rovnovážný	k2eAgFnSc1d1	rovnovážná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
odebírat	odebírat	k5eAaImF	odebírat
například	například	k6eAd1	například
destilací	destilace	k1gFnSc7	destilace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Ethyl-acetát	Ethylcetát	k1gInSc1	Ethyl-acetát
se	se	k3xPyFc4	se
primárně	primárně	k6eAd1	primárně
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
a	a	k8xC	a
ředidlo	ředidlo	k1gNnSc1	ředidlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
upřednostňován	upřednostňovat	k5eAaImNgInS	upřednostňovat
díky	díky	k7c3	díky
nízké	nízký	k2eAgFnSc3d1	nízká
ceně	cena	k1gFnSc3	cena
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnSc3d1	malá
toxicitě	toxicita	k1gFnSc3	toxicita
a	a	k8xC	a
snesitelnému	snesitelný	k2eAgInSc3d1	snesitelný
zápachu	zápach	k1gInSc3	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
čištění	čištění	k1gNnSc4	čištění
desek	deska	k1gFnPc2	deska
s	s	k7c7	s
elektronickými	elektronický	k2eAgInPc7d1	elektronický
obvody	obvod	k1gInPc7	obvod
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
odstraňovačích	odstraňovač	k1gInPc6	odstraňovač
laku	lak	k1gInSc2	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
aceton	aceton	k1gInSc1	aceton
a	a	k8xC	a
acetonitril	acetonitril	k1gInSc1	acetonitril
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
dekofeinizaci	dekofeinizace	k1gFnSc6	dekofeinizace
kávových	kávový	k2eAgNnPc2d1	kávové
zrn	zrno	k1gNnPc2	zrno
a	a	k8xC	a
čajových	čajový	k2eAgInPc2d1	čajový
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
nátěrových	nátěrový	k2eAgFnPc6d1	nátěrová
hmotách	hmota	k1gFnPc6	hmota
jako	jako	k8xS	jako
aktivátor	aktivátor	k1gInSc4	aktivátor
nebo	nebo	k8xC	nebo
tvrdidlo	tvrdidlo	k1gNnSc4	tvrdidlo
<g/>
.	.	kIx.	.
</s>
<s>
Ethyl-acetát	Ethylcetát	k1gInSc1	Ethyl-acetát
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
cukrovinkách	cukrovinka	k1gFnPc6	cukrovinka
<g/>
,	,	kIx,	,
parfémech	parfém	k1gInPc6	parfém
a	a	k8xC	a
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parfémech	parfém	k1gInPc6	parfém
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
a	a	k8xC	a
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
vůni	vůně	k1gFnSc4	vůně
parfému	parfém	k1gInSc2	parfém
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Laboratorní	laboratorní	k2eAgNnSc1d1	laboratorní
použití	použití	k1gNnSc1	použití
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
se	se	k3xPyFc4	se
směsi	směs	k1gFnPc4	směs
obsahující	obsahující	k2eAgInSc4d1	obsahující
ethyl-acetát	ethylcetát	k1gInSc4	ethyl-acetát
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
sloupcové	sloupcový	k2eAgFnSc6d1	sloupcová
chromatografii	chromatografie	k1gFnSc6	chromatografie
a	a	k8xC	a
při	při	k7c6	při
extrakcích	extrakce	k1gFnPc6	extrakce
<g/>
.	.	kIx.	.
</s>
<s>
Málokdy	málokdy	k6eAd1	málokdy
se	se	k3xPyFc4	se
však	však	k9	však
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
reakční	reakční	k2eAgNnSc1d1	reakční
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
náchylný	náchylný	k2eAgInSc1d1	náchylný
na	na	k7c4	na
hydrolýzu	hydrolýza	k1gFnSc4	hydrolýza
a	a	k8xC	a
transesterifikaci	transesterifikace	k1gFnSc4	transesterifikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
experimentech	experiment	k1gInPc6	experiment
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
stlačeným	stlačený	k2eAgInSc7d1	stlačený
vzduchem	vzduch	k1gInSc7	vzduch
z	z	k7c2	z
horké	horký	k2eAgFnSc2d1	horká
vodní	vodní	k2eAgFnSc2d1	vodní
lázně	lázeň	k1gFnSc2	lázeň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těkavý	těkavý	k2eAgMnSc1d1	těkavý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc4d1	nízký
bod	bod	k1gInSc4	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnSc1d1	další
použití	použití	k1gNnSc1	použití
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
entomologie	entomologie	k1gFnSc2	entomologie
se	se	k3xPyFc4	se
ethyl-acetát	ethylcetát	k1gInSc1	ethyl-acetát
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
účinný	účinný	k2eAgInSc1d1	účinný
asfyxant	asfyxant	k1gInSc1	asfyxant
při	při	k7c6	při
sběru	sběr	k1gInSc6	sběr
a	a	k8xC	a
studiu	studio	k1gNnSc6	studio
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
smrtičce	smrtička	k1gFnSc6	smrtička
naplněné	naplněný	k2eAgFnSc6d1	naplněná
ethyl-acetátem	ethylcetát	k1gInSc7	ethyl-acetát
jeho	jeho	k3xOp3gFnSc2	jeho
páry	pára	k1gFnSc2	pára
rychle	rychle	k6eAd1	rychle
usmrcují	usmrcovat	k5eAaImIp3nP	usmrcovat
sbíraný	sbíraný	k2eAgMnSc1d1	sbíraný
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
dospělý	dospělý	k2eAgMnSc1d1	dospělý
<g/>
)	)	kIx)	)
hmyz	hmyz	k1gInSc1	hmyz
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
ho	on	k3xPp3gInSc4	on
poškodil	poškodit	k5eAaPmAgInS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ethyl-acetát	ethylcetát	k1gInSc1	ethyl-acetát
není	být	k5eNaImIp3nS	být
hygroskopický	hygroskopický	k2eAgInSc1d1	hygroskopický
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
hmyz	hmyz	k1gInSc1	hmyz
dostatečně	dostatečně	k6eAd1	dostatečně
měkký	měkký	k2eAgInSc1d1	měkký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
přidán	přidat	k5eAaPmNgInS	přidat
do	do	k7c2	do
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
ve	v	k7c6	v
vínech	víno	k1gNnPc6	víno
==	==	k?	==
</s>
</p>
<p>
<s>
Ethylester	Ethylester	k1gInSc1	Ethylester
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
je	být	k5eAaImIp3nS	být
nejhojnějším	hojný	k2eAgInSc7d3	nejhojnější
esterem	ester	k1gInSc7	ester
ve	v	k7c6	v
víně	víno	k1gNnSc6	víno
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
se	se	k3xPyFc4	se
reakcí	reakce	k1gFnSc7	reakce
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
a	a	k8xC	a
ethanolu	ethanol	k1gInSc2	ethanol
během	během	k7c2	během
fermentace	fermentace	k1gFnSc2	fermentace
<g/>
.	.	kIx.	.
</s>
<s>
Aroma	aroma	k1gNnSc1	aroma
ethyl-acetátu	ethylcetát	k1gInSc2	ethyl-acetát
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
v	v	k7c6	v
mladých	mladý	k2eAgNnPc6d1	mladé
vínech	víno	k1gNnPc6	víno
a	a	k8xC	a
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
obecnému	obecný	k2eAgNnSc3d1	obecné
vnímání	vnímání	k1gNnSc3	vnímání
"	"	kIx"	"
<g/>
ovocnosti	ovocnost	k1gFnSc3	ovocnost
<g/>
"	"	kIx"	"
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
různých	různý	k2eAgMnPc2d1	různý
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
má	mít	k5eAaImIp3nS	mít
práh	práh	k1gInSc1	práh
vnímání	vnímání	k1gNnSc2	vnímání
okolo	okolo	k7c2	okolo
120	[number]	k4	120
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Nadměrná	nadměrný	k2eAgNnPc4d1	nadměrné
množství	množství	k1gNnSc4	množství
ethyl-acetátu	ethylcetát	k1gInSc2	ethyl-acetát
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
vadu	vada	k1gFnSc4	vada
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
kyslíku	kyslík	k1gInSc2	kyslík
může	moct	k5eAaImIp3nS	moct
vadu	vada	k1gFnSc4	vada
zjitřit	zjitřit	k5eAaPmF	zjitřit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
ethanolu	ethanol	k1gInSc2	ethanol
na	na	k7c4	na
acetaldehyd	acetaldehyd	k1gInSc4	acetaldehyd
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
ve	v	k7c6	v
víně	víno	k1gNnSc6	víno
ostrou	ostrý	k2eAgFnSc4d1	ostrá
octovou	octový	k2eAgFnSc4d1	octová
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
Ethyl-acetát	Ethylcetát	k1gInSc4	Ethyl-acetát
lze	lze	k6eAd1	lze
hydrolyzovat	hydrolyzovat	k5eAaBmF	hydrolyzovat
v	v	k7c6	v
kyselém	kyselý	k2eAgNnSc6d1	kyselé
nebo	nebo	k8xC	nebo
zásaditém	zásaditý	k2eAgNnSc6d1	zásadité
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
kyselinu	kyselina	k1gFnSc4	kyselina
octovou	octový	k2eAgFnSc4d1	octová
a	a	k8xC	a
ethanol	ethanol	k1gInSc4	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
kyselého	kyselý	k2eAgInSc2d1	kyselý
katalyzátoru	katalyzátor	k1gInSc2	katalyzátor
hydrolýzu	hydrolýza	k1gFnSc4	hydrolýza
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Fischerovo	Fischerův	k2eAgNnSc4d1	Fischerovo
ekvilibrium	ekvilibrium	k1gNnSc4	ekvilibrium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
(	(	kIx(	(
<g/>
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
pro	pro	k7c4	pro
ilustrativní	ilustrativní	k2eAgInPc4d1	ilustrativní
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
ethyl-acetát	ethylcetát	k1gInSc1	ethyl-acetát
typicky	typicky	k6eAd1	typicky
hydrolyzuje	hydrolyzovat	k5eAaBmIp3nS	hydrolyzovat
ve	v	k7c6	v
dvoufázovém	dvoufázový	k2eAgInSc6d1	dvoufázový
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
stechiometrickým	stechiometrický	k2eAgNnSc7d1	stechiometrické
množstvím	množství	k1gNnSc7	množství
silné	silný	k2eAgFnSc2d1	silná
zásady	zásada	k1gFnSc2	zásada
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hydroxidu	hydroxid	k1gInSc3	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ethanol	ethanol	k1gInSc4	ethanol
a	a	k8xC	a
octan	octan	k1gInSc4	octan
sodný	sodný	k2eAgInSc4d1	sodný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
s	s	k7c7	s
ethanolem	ethanol	k1gInSc7	ethanol
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
COOCH_	COOCH_	k1gFnSc2	COOCH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
CH_	CH_	k1gFnSc2	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
NaOH	NaOH	k1gFnSc1	NaOH
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
CH_	CH_	k1gMnSc6	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
CH_	CH_	k1gFnSc2	CH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
OH	OH	kA	OH
<g/>
+	+	kIx~	+
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
COONa	COON	k1gInSc2	COON
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
Ethylacetát	Ethylacetát	k1gInSc1	Ethylacetát
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
sodným	sodný	k2eAgInSc7d1	sodný
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
ethanolu	ethanol	k1gInSc2	ethanol
a	a	k8xC	a
octanu	octan	k1gInSc2	octan
sodného	sodný	k2eAgNnSc2d1	sodné
<g/>
.	.	kIx.	.
<g/>
Reaguje	reagovat	k5eAaBmIp3nS	reagovat
také	také	k9	také
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
⇌	⇌	k?	⇌
</s>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
COOCH_	COOCH_	k1gFnSc2	COOCH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
CH_	CH_	k1gFnSc2	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
H_	H_	k1gMnSc1	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O	o	k7c4	o
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
rightleftharpoons	rightleftharpoons	k1gInSc1	rightleftharpoons
\	\	kIx~	\
CH_	CH_	k1gFnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
COOH	COOH	kA	COOH
<g/>
+	+	kIx~	+
<g/>
CH_	CH_	k1gMnSc1	CH_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
CH_	CH_	k1gFnSc2	CH_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
OH	OH	kA	OH
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
Ethylacetát	Ethylacetát	k1gInSc1	Ethylacetát
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
a	a	k8xC	a
ethanolu	ethanol	k1gInSc2	ethanol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
LD50	LD50	k1gMnPc2	LD50
u	u	k7c2	u
potkanů	potkan	k1gMnPc2	potkan
činí	činit	k5eAaImIp3nS	činit
11,3	[number]	k4	11,3
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc3d1	nízká
toxicitě	toxicita	k1gFnSc3	toxicita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ethyl	ethyl	k1gInSc1	ethyl
acetate	acetat	k1gInSc5	acetat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
