<s>
Současný	současný	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
mužů	muž	k1gMnPc2
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
9,58	9,58	k4
s	s	k7c7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
jamajský	jamajský	k2eAgMnSc1d1
sprinter	sprinter	k1gMnSc1
Usain	Usain	k1gMnSc1
Bolt	Bolt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ho	on	k3xPp3gInSc4
zaběhl	zaběhnout	k5eAaPmAgMnS
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
2009	#num#	k4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>