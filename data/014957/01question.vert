<s>
Jakou	jaký	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
hodnotu	hodnota	k1gFnSc4
má	mít	k5eAaImIp3nS
světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
mužů	muž	k1gMnPc2
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
100	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
jamajský	jamajský	k2eAgMnSc1d1
sprintér	sprintér	k1gMnSc1
Usain	Usain	k1gMnSc1
Bolt	Bolt	k1gMnSc1
<g/>
?	?	kIx.
</s>