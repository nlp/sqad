<s>
Běh	běh	k1gInSc1
na	na	k7c4
100	#num#	k4
metrů	metr	k1gInPc2
</s>
<s>
Justin	Justin	k1gMnSc1
Gatlin	Gatlin	k2eAgMnSc1d1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
ve	v	k7c6
sprintu	sprint	k1gInSc6
na	na	k7c4
100	#num#	k4
<g/>
m	m	kA
(	(	kIx(
<g/>
Helsinky	Helsinky	k1gFnPc1
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Finále	finále	k1gNnSc1
letní	letní	k2eAgFnSc2d1
Univerziády	univerziáda	k1gFnSc2
(	(	kIx(
<g/>
Kwangdžu	Kwangdžu	k1gFnSc1
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Běh	běh	k1gInSc1
na	na	k7c4
100	#num#	k4
metrů	metr	k1gInPc2
je	být	k5eAaImIp3nS
lehkoatletický	lehkoatletický	k2eAgInSc4d1
sprint	sprint	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
běží	běžet	k5eAaImIp3nS
maximálním	maximální	k2eAgNnSc7d1
úsilím	úsilí	k1gNnSc7
od	od	k7c2
startu	start	k1gInSc2
do	do	k7c2
cíle	cíl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítěz	vítěz	k1gMnSc1
této	tento	k3xDgFnSc2
disciplíny	disciplína	k1gFnSc2
na	na	k7c6
vrcholných	vrcholný	k2eAgFnPc6d1
světových	světový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
světový	světový	k2eAgMnSc1d1
rekordman	rekordman	k1gMnSc1
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
nejrychlejšího	rychlý	k2eAgMnSc4d3
muže	muž	k1gMnSc4
a	a	k8xC
ženu	žena	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stovce	stovka	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
přezdívá	přezdívat	k5eAaImIp3nS
"	"	kIx"
<g/>
královská	královský	k2eAgFnSc1d1
<g/>
"	"	kIx"
atletická	atletický	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžecká	běžecký	k2eAgFnSc1d1
trať	trať	k1gFnSc1
na	na	k7c4
100	#num#	k4
m	m	kA
se	se	k3xPyFc4
běhá	běhat	k5eAaImIp3nS
v	v	k7c6
oddělených	oddělený	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
širokých	široký	k2eAgFnPc2d1
122	#num#	k4
–	–	k?
125	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trať	trať	k1gFnSc1
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c6
jedné	jeden	k4xCgFnSc6
ze	z	k7c2
dvou	dva	k4xCgFnPc2
rovinek	rovinka	k1gFnPc2
na	na	k7c6
atletickém	atletický	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závodník	Závodník	k1gMnSc1
musí	muset	k5eAaImIp3nS
startovat	startovat	k5eAaBmF
z	z	k7c2
nízkého	nízký	k2eAgInSc2d1
startu	start	k1gInSc2
a	a	k8xC
ze	z	k7c2
startovních	startovní	k2eAgInPc2d1
bloků	blok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
uznán	uznat	k5eAaPmNgInS
<g/>
,	,	kIx,
pokud	pokud	k8xS
rychlost	rychlost	k1gFnSc1
větru	vítr	k1gInSc2
vanoucího	vanoucí	k2eAgInSc2d1
do	do	k7c2
zad	záda	k1gNnPc2
běžce	běžec	k1gMnSc2
je	být	k5eAaImIp3nS
maximálně	maximálně	k6eAd1
2	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
</s>
<s>
Rychlost	rychlost	k1gFnSc1
</s>
<s>
Oficiálně	oficiálně	k6eAd1
změřená	změřený	k2eAgFnSc1d1
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
při	při	k7c6
sprintu	sprint	k1gInSc6
je	být	k5eAaImIp3nS
44,72	44,72	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
u	u	k7c2
mužů	muž	k1gMnPc2
(	(	kIx(
<g/>
Usain	Usain	k1gMnSc1
Bolt	Bolt	k1gMnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
39,56	39,56	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
u	u	k7c2
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
Florence	Florenc	k1gFnSc2
Griffith	Griffith	k1gMnSc1
Joynerová	Joynerová	k1gFnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranicí	hranice	k1gFnSc7
světové	světový	k2eAgFnSc2d1
extratřídy	extratřída	k1gFnSc2
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
bariéra	bariéra	k1gFnSc1
10	#num#	k4
sekund	sekunda	k1gFnPc2
u	u	k7c2
mužů	muž	k1gMnPc2
a	a	k8xC
11	#num#	k4
sekund	sekunda	k1gFnPc2
u	u	k7c2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
10	#num#	k4
sekund	sekunda	k1gFnPc2
už	už	k6eAd1
dokázalo	dokázat	k5eAaPmAgNnS
běžet	běžet	k5eAaImF
přes	přes	k7c4
80	#num#	k4
sprinterů	sprinter	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
září	září	k1gNnSc3
roku	rok	k1gInSc2
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
donedávna	donedávna	k6eAd1
všichni	všechen	k3xTgMnPc1
vesměs	vesměs	k6eAd1
černé	černý	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
pleti	pleť	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Nejrychlejším	rychlý	k2eAgInSc7d3
„	„	k?
<g/>
bílým	bílý	k1gMnSc7
<g/>
“	“	k?
sprinterem	sprinter	k1gMnSc7
je	být	k5eAaImIp3nS
Francouz	Francouz	k1gMnSc1
Christophe	Christophus	k1gMnSc5
Lemaitre	Lemaitr	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
výkonu	výkon	k1gInSc2
9,92	9,92	k4
s.	s.	k?
Před	před	k7c7
ním	on	k3xPp3gInSc7
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgMnS
Ital	Ital	k1gMnSc1
Pietro	Pietro	k1gNnSc4
Mennea	Menneus	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zaběhl	zaběhnout	k5eAaPmAgMnS
tuto	tento	k3xDgFnSc4
trať	trať	k1gFnSc4
za	za	k7c4
10,01	10,01	k4
s	s	k7c7
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gMnSc6
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
povedlo	povést	k5eAaPmAgNnS
už	už	k9
jen	jen	k9
Poláku	Polák	k1gMnSc3
Marianu	Marian	k1gMnSc3
Woroninovi	Woronin	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
využil	využít	k5eAaPmAgMnS
max	max	kA
<g/>
.	.	kIx.
podpory	podpora	k1gFnSc2
větru	vítr	k1gInSc2
2	#num#	k4
<g/>
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
a	a	k8xC
zaběhl	zaběhnout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
neoficiálně	neoficiálně	k6eAd1,k6eNd1
rovných	rovný	k2eAgFnPc2d1
10,00	10,00	k4
s.	s.	k?
Obecný	obecný	k2eAgInSc1d1
předpoklad	předpoklad	k1gInSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
černí	černý	k2eAgMnPc1d1
sprinteři	sprinter	k1gMnPc1
mají	mít	k5eAaImIp3nP
víc	hodně	k6eAd2
rychlých	rychlý	k2eAgNnPc2d1
svalových	svalový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
v	v	k7c6
motorickém	motorický	k2eAgNnSc6d1
svalstvu	svalstvo	k1gNnSc6
<g/>
,	,	kIx,
proto	proto	k8xC
dokážou	dokázat	k5eAaPmIp3nP
běhat	běhat	k5eAaImF
rychleji	rychle	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schopnost	schopnost	k1gFnSc1
zaběhnout	zaběhnout	k5eAaPmF
stometrový	stometrový	k2eAgInSc4d1
sprint	sprint	k1gInSc4
v	v	k7c6
čase	čas	k1gInSc6
pod	pod	k7c4
10	#num#	k4
sekund	sekunda	k1gFnPc2
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
Hranice	hranice	k1gFnSc2
deseti	deset	k4xCc2
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
</s>
<s>
Nejrychlejším	rychlý	k2eAgMnSc7d3
českým	český	k2eAgMnSc7d1
sprinterem	sprinter	k1gMnSc7
na	na	k7c6
této	tento	k3xDgFnSc6
trati	trať	k1gFnSc6
je	být	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
Zdeněk	Zdeněk	k1gMnSc1
Stromšík	Stromšík	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
zaběhl	zaběhnout	k5eAaPmAgMnS
stovku	stovka	k1gFnSc4
za	za	k7c4
10,16	10,16	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
dříve	dříve	k6eAd2
však	však	k9
běžel	běžet	k5eAaImAgMnS
s	s	k7c7
nedovolenou	dovolený	k2eNgFnSc7d1
podporou	podpora	k1gFnSc7
větru	vítr	k1gInSc2
3	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
stovku	stovka	k1gFnSc4
za	za	k7c4
10,11	10,11	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Předchozí	předchozí	k2eAgInSc1d1
rekord	rekord	k1gInSc1
10,23	10,23	k4
sekundy	sekunda	k1gFnSc2
zaběhl	zaběhnout	k5eAaPmAgMnS
roku	rok	k1gInSc2
2010	#num#	k4
sprinter	sprinter	k1gMnSc1
Jan	Jan	k1gMnSc1
Veleba	Veleba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekord	rekord	k1gInSc1
v	v	k7c6
české	český	k2eAgFnSc6d1
ženské	ženský	k2eAgFnSc6d1
kategorii	kategorie	k1gFnSc6
drží	držet	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
běžkyně	běžkyně	k1gFnSc1
Jarmila	Jarmila	k1gFnSc1
Kratochvílová	Kratochvílová	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
časem	časem	k6eAd1
11,09	11,09	k4
sekundy	sekunda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Světové	světový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
</s>
<s>
Současný	současný	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
mužů	muž	k1gMnPc2
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
9,58	9,58	k4
s	s	k7c7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
jamajský	jamajský	k2eAgMnSc1d1
sprinter	sprinter	k1gMnSc1
Usain	Usain	k1gMnSc1
Bolt	Bolt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ho	on	k3xPp3gInSc4
zaběhl	zaběhnout	k5eAaPmAgMnS
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
2009	#num#	k4
v	v	k7c6
Berlíně	Berlín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
přitom	přitom	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
lidské	lidský	k2eAgNnSc4d1
maximum	maximum	k1gNnSc4
na	na	k7c6
této	tento	k3xDgFnSc6
trati	trať	k1gFnSc6
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
9,25	9,25	k4
až	až	k9
9,40	9,40	k4
s.	s.	k?
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ženský	ženský	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
drží	držet	k5eAaImIp3nS
Florence	Florenc	k1gFnPc4
Griffith-Joyner	Griffith-Joynero	k1gNnPc2
a	a	k8xC
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
10,49	10,49	k4
s.	s.	k?
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Závod	závod	k1gInSc1
byl	být	k5eAaImAgInS
zpočátku	zpočátku	k6eAd1
provozován	provozovat	k5eAaImNgInS
na	na	k7c6
trávě	tráva	k1gFnSc6
nebo	nebo	k8xC
na	na	k7c6
škvárové	škvárový	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
100	#num#	k4
yardů	yard	k1gInPc2
(	(	kIx(
<g/>
91,44	91,44	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vlivem	vliv	k1gInSc7
pevninské	pevninský	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
v	v	k7c4
závod	závod	k1gInSc4
na	na	k7c4
100	#num#	k4
m.	m.	k?
Závodníci	Závodník	k1gMnPc1
startovali	startovat	k5eAaBmAgMnP
ze	z	k7c2
stojící	stojící	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1887	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
Charles	Charles	k1gMnSc1
Scherrill	Scherrill	k1gMnSc1
vykopal	vykopat	k5eAaPmAgMnS
v	v	k7c6
dráze	dráha	k1gFnSc6
malé	malý	k2eAgFnSc2d1
dírky	dírka	k1gFnSc2
k	k	k7c3
lepšímu	dobrý	k2eAgInSc3d2
odrazu	odraz	k1gInSc3
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
dal	dát	k5eAaPmAgMnS
podnět	podnět	k1gInSc4
ke	k	k7c3
vzniku	vznik	k1gInSc3
startovních	startovní	k2eAgInPc2d1
bloků	blok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1928	#num#	k4
–	–	k?
1929	#num#	k4
George	George	k1gNnPc2
Breshnahan	Breshnahan	k1gInSc4
a	a	k8xC
William	William	k1gInSc4
Tuttle	Tuttle	k1gFnSc2
vynalezli	vynaleznout	k5eAaPmAgMnP
startovní	startovní	k2eAgInPc4d1
bloky	blok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
byly	být	k5eAaImAgInP
startovní	startovní	k2eAgInPc1d1
bloky	blok	k1gInPc1
oficiálně	oficiálně	k6eAd1
schváleny	schválit	k5eAaPmNgInP
IAAF	IAAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
je	být	k5eAaImIp3nS
stanoveno	stanovit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
uznán	uznat	k5eAaPmNgInS
<g/>
,	,	kIx,
pokud	pokud	k8xS
rychlost	rychlost	k1gFnSc1
větru	vítr	k1gInSc2
vanoucího	vanoucí	k2eAgInSc2d1
do	do	k7c2
zad	záda	k1gNnPc2
běžce	běžec	k1gMnSc2
je	být	k5eAaImIp3nS
maximálně	maximálně	k6eAd1
2	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s.	s.	k?
První	první	k4xOgInPc4
experimenty	experiment	k1gInPc4
s	s	k7c7
elektrickou	elektrický	k2eAgFnSc7d1
časomírou	časomíra	k1gFnSc7
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
první	první	k4xOgFnSc2
čtvrtiny	čtvrtina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
je	být	k5eAaImIp3nS
užívána	užívat	k5eAaImNgFnS
až	až	k6eAd1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1977	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Současní	současný	k2eAgMnPc1d1
světoví	světový	k2eAgMnPc1d1
rekordmani	rekordman	k1gMnPc1
</s>
<s>
Muži	muž	k1gMnPc1
-	-	kIx~
Usain	Usain	k2eAgInSc4d1
Bolt	Bolt	k1gInSc4
9,58	9,58	k4
s	s	k7c7
</s>
<s>
Ženy	žena	k1gFnPc1
-	-	kIx~
Florence	Florenc	k1gFnPc1
Griffith-Joyner	Griffith-Joyner	k1gInSc4
10,49	10,49	k4
s	s	k7c7
</s>
<s>
Současné	současný	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
</s>
<s>
Rekord	rekord	k1gInSc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
Atlet	atlet	k1gMnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Světový	světový	k2eAgMnSc1d1
(	(	kIx(
<g/>
WR	WR	kA
<g/>
)	)	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
<g/>
9,58	9,58	k4
<g/>
Usain	Usaina	k1gFnPc2
BoltJamajka	BoltJamajka	k1gFnSc1
JamajkaBerlín	JamajkaBerlín	k1gInSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,49	10,49	k4
<g/>
Florence	Florenc	k1gFnSc2
Griffith-Joynerová	Griffith-Joynerová	k1gFnSc1
USAIndianapolis	USAIndianapolis	k1gFnSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
</s>
<s>
Olympijský	olympijský	k2eAgMnSc1d1
(	(	kIx(
<g/>
OR	OR	kA
<g/>
)	)	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
<g/>
9,63	9,63	k4
<g/>
Usain	Usaina	k1gFnPc2
BoltJamajka	BoltJamajka	k1gFnSc1
JamajkaLondýn	JamajkaLondýn	k1gInSc1
<g/>
5	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,62	10,62	k4
<g/>
Florence	Florenc	k1gFnSc2
Griffith-Joynerová	Griffith-Joynerová	k1gFnSc1
USASoul	USASoul	k1gInSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
MS	MS	kA
<g/>
)	)	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
<g/>
9,58	9,58	k4
<g/>
Usain	Usaina	k1gFnPc2
BoltJamajka	BoltJamajka	k1gFnSc1
JamajkaBerlín	JamajkaBerlín	k1gInSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,70	10,70	k4
<g/>
Marion	Marion	k1gInSc1
Jonesová	Jonesový	k2eAgFnSc1d1
USASevilla	USASevilla	k1gFnSc1
<g/>
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
</s>
<s>
Český	český	k2eAgMnSc1d1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
<g/>
10,16	10,16	k4
<g/>
Zdeněk	Zdeňka	k1gFnPc2
StromšíkČesko	StromšíkČesko	k1gNnSc4
ČeskoTábor	ČeskoTábor	k1gInSc1
<g/>
18	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
11,09	11,09	k4
<g/>
Jarmila	Jarmila	k1gFnSc1
KratochvílováČeskoslovensko	KratochvílováČeskoslovensko	k1gNnSc4
ČeskoslovenskoBratislava	ČeskoslovenskoBratislav	k1gMnSc2
<g/>
6	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1981	#num#	k4
</s>
<s>
Současné	současný	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
podle	podle	k7c2
kontinentů	kontinent	k1gInPc2
</s>
<s>
Rekord	rekord	k1gInSc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Výkon	výkon	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
Atlet	atlet	k1gMnSc1
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Město	město	k1gNnSc1
</s>
<s>
Datum	datum	k1gNnSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Muži	muž	k1gMnSc3
<g/>
9,86	9,86	k4
<g/>
Francis	Francis	k1gFnSc2
ObikweluPortugalsko	ObikweluPortugalsko	k1gNnSc1
PortugalskoAthény	PortugalskoAthéna	k1gFnSc2
<g/>
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,73	10,73	k4
<g/>
Christine	Christin	k1gInSc5
ArronováFrancie	ArronováFrancie	k1gFnPc4
FrancieBudapešť	FrancieBudapešť	k1gFnSc4
<g/>
19	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Muži	muž	k1gMnSc3
<g/>
9,85	9,85	k4
<g/>
Olusoji	Olusoj	k1gInSc6
FasubaNigérie	FasubaNigérie	k1gFnSc2
NigérieDauhá	NigérieDauhý	k2eAgFnSc1d1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,78	10,78	k4
<g/>
Murielle	Murielle	k1gNnPc4
Ahouréová	Ahouréový	k2eAgNnPc4d1
Pobřeží	pobřeží	k1gNnPc4
slonovinyMontverde	slonovinyMontverde	k6eAd1
<g/>
11	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
Muži	muž	k1gMnSc3
<g/>
9,91	9,91	k4
<g/>
Femi	Fem	k1gFnSc2
Ogunode	Ogunod	k1gInSc5
KatarWu-chan	KatarWu-chan	k1gInSc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,79	10,79	k4
<g/>
Li	li	k8xS
Xuemei	Xuemei	k1gNnSc4
ČínaŠanghaj	ČínaŠanghaj	k1gInSc1
<g/>
18.10	18.10	k4
<g/>
.	.	kIx.
1997	#num#	k4
</s>
<s>
Severní	severní	k2eAgFnSc1d1
a	a	k8xC
střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Muži	muž	k1gMnSc3
<g/>
9,58	9,58	k4
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k2eAgInSc1d1
JamajkaBerlín	JamajkaBerlín	k1gInSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,49	10,49	k4
Florence	Florenc	k1gFnSc2
Griffith-Joynerová	Griffith-Joynerová	k1gFnSc1
USAIndianapolis	USAIndianapolis	k1gFnSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Muži	muž	k1gMnSc3
<g/>
10,00	10,00	k4
<g/>
Robson	Robson	k1gNnSc4
da	da	k?
Silva	Silva	k1gFnSc1
BrazílieCiudad	BrazílieCiudad	k1gInSc1
de	de	k?
México	México	k6eAd1
<g/>
22	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
10,91	10,91	k4
<g/>
Rosângela	Rosângela	k1gFnSc1
Santosová	Santosová	k1gFnSc1
BrazílieLondýn	BrazílieLondýn	k1gInSc1
<g/>
6	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
</s>
<s>
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
Muži	muž	k1gMnSc3
<g/>
9,93	9,93	k4
<g/>
Patrick	Patrick	k1gMnSc1
Johnson	Johnson	k1gMnSc1
AustrálieMito	AustrálieMit	k2eAgNnSc4d1
<g/>
5	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2003	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
<g/>
11,11	11,11	k4
<g/>
Melissa	Melissa	k1gFnSc1
Breenová	Breenová	k1gFnSc1
AustrálieCanberra	AustrálieCanberra	k1gFnSc1
<g/>
9	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
</s>
<s>
Muži	muž	k1gMnPc1
na	na	k7c4
iaaf	iaaf	k1gInSc4
<g/>
.	.	kIx.
<g/>
orgŽeny	orgŽen	k1gInPc1
na	na	k7c4
iaaf	iaaf	k1gInSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Vývoj	vývoj	k1gInSc1
světového	světový	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
(	(	kIx(
<g/>
el.	el.	k?
měření	měření	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
ČasJménoDatumMísto	ČasJménoDatumMísto	k6eAd1
</s>
<s>
10,64	10,64	k4
Ralph	Ralph	k1gInSc1
Metcalfe	Metcalf	k1gInSc5
<g/>
16.07	16.07	k4
<g/>
.1932	.1932	k4
<g/>
Stanford	Stanforda	k1gFnPc2
</s>
<s>
10,53	10,53	k4
Eddie	Eddie	k1gFnSc1
Tolan	Tolany	k1gInPc2
<g/>
31.07	31.07	k4
<g/>
.1932	.1932	k4
<g/>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
</s>
<s>
10,38	10,38	k4
Eddie	Eddie	k1gFnSc1
Tolan	Tolany	k1gInPc2
<g/>
0	#num#	k4
<g/>
1.08	1.08	k4
<g/>
.1932	.1932	k4
<g/>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
</s>
<s>
10,38	10,38	k4
Ralph	Ralph	k1gInSc1
Metcalfe	Metcalf	k1gInSc5
<g/>
0	#num#	k4
<g/>
1.08	1.08	k4
<g/>
.1932	.1932	k4
<g/>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
</s>
<s>
10,34	10,34	k4
Barney	Barney	k1gInPc4
Ewell	Ewell	k1gInSc1
<g/>
0	#num#	k4
<g/>
9.07	9.07	k4
<g/>
.1948	.1948	k4
<g/>
Evanston	Evanston	k1gInSc1
</s>
<s>
10,32	10,32	k4
Ray	Ray	k1gMnSc1
Norton	Norton	k1gInSc4
<g/>
10.08	10.08	k4
<g/>
.1958	.1958	k4
<g/>
Thonon-les-Bains	Thonon-les-Bainsa	k1gFnPc2
</s>
<s>
10,32	10,32	k4
Jocelyn	Jocelyn	k1gMnSc1
Delecour	Delecour	k1gMnSc1
<g/>
10.08	10.08	k4
<g/>
.1958	.1958	k4
<g/>
Thonon-les-Bains	Thonon-les-Bainsa	k1gFnPc2
</s>
<s>
10,29	10,29	k4
Peter	Peter	k1gMnSc1
Radford	Radford	k1gMnSc1
<g/>
13.09	13.09	k4
<g/>
.1958	.1958	k4
<g/>
Colombes	Colombesa	k1gFnPc2
</s>
<s>
10,25	10,25	k4
Armin	Armin	k1gInSc1
Hary	Hara	k1gFnSc2
<g/>
21.06	21.06	k4
<g/>
.1960	.1960	k4
<g/>
Curych	Curych	k1gInSc1
</s>
<s>
10,06	10,06	k4
Bob	Bob	k1gMnSc1
Hayes	Hayes	k1gMnSc1
<g/>
15.10	15.10	k4
<g/>
.1964	.1964	k4
<g/>
Tokio	Tokio	k1gNnSc4
</s>
<s>
10,03	10,03	k4
Jim	on	k3xPp3gMnPc3
Hines	Hines	k1gMnSc1
<g/>
20.06	20.06	k4
<g/>
.1968	.1968	k4
<g/>
Sacramento	Sacramento	k1gNnSc4
</s>
<s>
10,02	10,02	k4
Charles	Charles	k1gMnSc1
Greene	Green	k1gInSc5
<g/>
13.10	13.10	k4
<g/>
.1968	.1968	k4
<g/>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
</s>
<s>
9,95	9,95	k4
Jim	on	k3xPp3gMnPc3
Hines	Hines	k1gMnSc1
<g/>
14.10	14.10	k4
<g/>
.1968	.1968	k4
<g/>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
</s>
<s>
9,93	9,93	k4
Calvin	Calvina	k1gFnPc2
Smith	Smith	k1gMnSc1
<g/>
0	#num#	k4
<g/>
3.07	3.07	k4
<g/>
.1983	.1983	k4
<g/>
Colorado	Colorado	k1gNnSc1
Springs	Springsa	k1gFnPc2
</s>
<s>
9,93	9,93	k4
Carl	Carl	k1gInSc1
Lewis	Lewis	k1gFnSc2
<g/>
30.08	30.08	k4
<g/>
.1987	.1987	k4
<g/>
Řím	Řím	k1gInSc1
</s>
<s>
9,93	9,93	k4
Carl	Carl	k1gInSc1
Lewis	Lewis	k1gFnSc2
<g/>
17.08	17.08	k4
<g/>
.1988	.1988	k4
<g/>
Curych	Curych	k1gInSc1
</s>
<s>
9,92	9,92	k4
Carl	Carl	k1gInSc1
Lewis	Lewis	k1gFnSc2
<g/>
24.09	24.09	k4
<g/>
.1988	.1988	k4
<g/>
Soul	Soul	k1gInSc1
</s>
<s>
9,90	9,90	k4
Leroy	Leroy	k1gInPc4
Burrell	Burrell	k1gInSc1
<g/>
14.06	14.06	k4
<g/>
.1991	.1991	k4
<g/>
New	New	k1gMnPc2
York	York	k1gInSc4
</s>
<s>
9,86	9,86	k4
Carl	Carl	k1gInSc1
Lewis	Lewis	k1gFnSc2
<g/>
25.08	25.08	k4
<g/>
.1991	.1991	k4
<g/>
Tokio	Tokio	k1gNnSc4
</s>
<s>
9,85	9,85	k4
Leroy	Leroy	k1gInPc4
Burrell	Burrell	k1gInSc1
<g/>
0	#num#	k4
<g/>
6.07	6.07	k4
<g/>
.1994	.1994	k4
<g/>
Lausanne	Lausanne	k1gNnPc2
</s>
<s>
9,84	9,84	k4
Donovan	Donovan	k1gMnSc1
Bailey	Bailea	k1gFnSc2
<g/>
27.07	27.07	k4
<g/>
.1996	.1996	k4
<g/>
Atlanta	Atlanta	k1gFnSc1
</s>
<s>
9,79	9,79	k4
Maurice	Maurika	k1gFnSc3
Greene	Green	k1gInSc5
<g/>
16.06	16.06	k4
<g/>
.1999	.1999	k4
<g/>
Athény	Athéna	k1gFnPc1
</s>
<s>
9,77	9,77	k4
Asafa	Asaf	k1gMnSc4
Powell	Powell	k1gInSc4
<g/>
14.06	14.06	k4
<g/>
.2005	.2005	k4
<g/>
Athény	Athéna	k1gFnPc1
</s>
<s>
9,77	9,77	k4
Asafa	Asaf	k1gMnSc4
Powell	Powell	k1gInSc4
<g/>
11.06	11.06	k4
<g/>
.2006	.2006	k4
<g/>
Gateshead	Gateshead	k1gInSc1
</s>
<s>
9,77	9,77	k4
Asafa	Asaf	k1gMnSc4
Powell	Powell	k1gInSc4
<g/>
18.08	18.08	k4
<g/>
.2006	.2006	k4
<g/>
Curych	Curych	k1gInSc1
</s>
<s>
9,74	9,74	k4
Asafa	Asaf	k1gMnSc4
Powell	Powell	k1gInSc4
<g/>
0	#num#	k4
<g/>
9.09	9.09	k4
<g/>
.2007	.2007	k4
<g/>
Rieti	Rieti	k1gNnPc2
</s>
<s>
9,72	9,72	k4
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
<g/>
31.05	31.05	k4
<g/>
.2008	.2008	k4
<g/>
New	New	k1gMnPc2
York	York	k1gInSc4
</s>
<s>
9,69	9,69	k4
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
<g/>
16.08	16.08	k4
<g/>
.2008	.2008	k4
<g/>
Peking	Peking	k1gInSc1
</s>
<s>
9,58	9,58	k4
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
<g/>
16.08	16.08	k4
<g/>
.2009	.2009	k4
<g/>
Berlín	Berlín	k1gInSc1
</s>
<s>
Ženy	žena	k1gFnPc1
</s>
<s>
ČasJménoDatumMísto	ČasJménoDatumMísto	k6eAd1
</s>
<s>
11,08	11,08	k4
Wyomia	Wyomia	k1gFnSc1
Tyusová	Tyusová	k1gFnSc1
<g/>
15.10	15.10	k4
<g/>
.1968	.1968	k4
<g/>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
</s>
<s>
11,07	11,07	k4
Renate	Renat	k1gInSc5
Stecherová	Stecherová	k1gFnSc1
<g/>
0	#num#	k4
<g/>
2.09	2.09	k4
<g/>
.1972	.1972	k4
<g/>
Mnichov	Mnichov	k1gInSc1
</s>
<s>
11,04	11,04	k4
Inge	Inge	k1gFnSc1
Heltenová	Heltenová	k1gFnSc1
<g/>
13.06	13.06	k4
<g/>
.1976	.1976	k4
<g/>
Fürth	Fürtha	k1gFnPc2
</s>
<s>
11,01	11,01	k4
Annegret	Annegret	k1gInSc1
Richterová	Richterová	k1gFnSc1
<g/>
25.07	25.07	k4
<g/>
.1976	.1976	k4
<g/>
Montréal	Montréal	k1gMnSc1
</s>
<s>
10,88	10,88	k4
Marlies	Marlies	k1gInSc1
Göhrová	Göhrová	k1gFnSc1
<g/>
0	#num#	k4
<g/>
1.07	1.07	k4
<g/>
.1977	.1977	k4
<g/>
Drážďany	Drážďany	k1gInPc7
</s>
<s>
10,88	10,88	k4
Marlies	Marlies	k1gInSc1
Göhrová	Göhrová	k1gFnSc1
<g/>
0	#num#	k4
<g/>
9.07	9.07	k4
<g/>
.1982	.1982	k4
<g/>
Karl-Marx-Stadt	Karl-Marx-Stadt	k1gInSc1
</s>
<s>
10,81	10,81	k4
Marlies	Marlies	k1gInSc1
Göhrová	Göhrová	k1gFnSc1
<g/>
0	#num#	k4
<g/>
8.06	8.06	k4
<g/>
.1983	.1983	k4
<g/>
Berlín	Berlín	k1gInSc1
</s>
<s>
10,79	10,79	k4
Evelyn	Evelyn	k1gInSc1
Ashfordová	Ashfordová	k1gFnSc1
<g/>
0	#num#	k4
<g/>
3.07	3.07	k4
<g/>
.1983	.1983	k4
<g/>
Colorado	Colorado	k1gNnSc1
Springs	Springsa	k1gFnPc2
</s>
<s>
10,76	10,76	k4
Evelyn	Evelyn	k1gInSc1
Ashfordová	Ashfordová	k1gFnSc1
<g/>
22.08	22.08	k4
<g/>
.1984	.1984	k4
<g/>
Curych	Curych	k1gInSc1
</s>
<s>
10,49	10,49	k4
Florence	Florenc	k1gFnSc2
Griffith-Joynerová	Griffith-Joynerová	k1gFnSc1
<g/>
16.07	16.07	k4
<g/>
.1988	.1988	k4
<g/>
Indianapolis	Indianapolis	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
běžci	běžec	k1gMnPc1
a	a	k8xC
běžkyně	běžkyně	k1gFnSc1
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
Hranici	hranice	k1gFnSc4
10	#num#	k4
sekund	sekunda	k1gFnPc2
doposud	doposud	k6eAd1
pokořilo	pokořit	k5eAaPmAgNnS
139	#num#	k4
atletů	atlet	k1gMnPc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hranici	hranice	k1gFnSc4
9,90	9,90	k4
sekund	sekunda	k1gFnPc2
34	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
a	a	k8xC
hranici	hranice	k1gFnSc6
9,80	9,80	k4
sekund	sekunda	k1gFnPc2
8	#num#	k4
běžců	běžec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
sprinterem	sprinter	k1gMnSc7
Asafa	Asaf	k1gMnSc2
Powell	Powell	k1gMnSc1
z	z	k7c2
Jamajky	Jamajka	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
čas	čas	k1gInSc1
pod	pod	k7c4
10	#num#	k4
sekund	sekunda	k1gFnPc2
zaběhl	zaběhnout	k5eAaPmAgInS
92	#num#	k4
<g/>
×	×	k?
<g/>
,	,	kIx,
pod	pod	k7c4
9,90	9,90	k4
sekund	sekunda	k1gFnPc2
potom	potom	k6eAd1
43	#num#	k4
<g/>
×	×	k?
a	a	k8xC
8	#num#	k4
<g/>
×	×	k?
běžel	běžet	k5eAaImAgInS
pod	pod	k7c4
9,80	9,80	k4
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
Američan	Američan	k1gMnSc1
Maurice	Maurika	k1gFnSc3
Greene	Green	k1gInSc5
běžel	běžet	k5eAaImAgMnS
53	#num#	k4
<g/>
×	×	k?
pod	pod	k7c4
10	#num#	k4
(	(	kIx(
<g/>
11	#num#	k4
<g/>
×	×	k?
pod	pod	k7c4
9,90	9,90	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světový	světový	k2eAgMnSc1d1
rekordman	rekordman	k1gMnSc1
Usain	Usain	k1gMnSc1
Bolt	Bolt	k1gMnSc1
z	z	k7c2
Jamajky	Jamajka	k1gFnSc2
zaběhl	zaběhnout	k5eAaPmAgMnS
pod	pod	k7c4
10	#num#	k4
sekund	sekunda	k1gFnPc2
44	#num#	k4
závodů	závod	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
ale	ale	k9
běžel	běžet	k5eAaImAgMnS
31	#num#	k4
<g/>
×	×	k?
pod	pod	k7c7
9,90	9,90	k4
a	a	k8xC
11	#num#	k4
<g/>
×	×	k?
pod	pod	k7c4
9,80	9,80	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
ČasVítr	ČasVítr	k1gMnSc1
(	(	kIx(
<g/>
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
AtletStátDatumMěsto	AtletStátDatumMěsta	k1gFnSc5
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.58	9.58	k4
<g/>
+	+	kIx~
<g/>
0.9	0.9	k4
<g/>
Usain	Usain	k2eAgInSc1d1
Bolt	Bolt	k1gInSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
Berlín	Berlín	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.69	9.69	k4
<g/>
+	+	kIx~
<g/>
2.0	2.0	k4
<g/>
Tyson	Tyson	k1gMnSc1
Gay	gay	k1gMnSc1
<g/>
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
Šanghaj	Šanghaj	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
-0.1	-0.1	k4
<g/>
Yohan	Yohan	k1gInSc1
Blake	Blak	k1gFnSc2
<g/>
23	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
Lausanne	Lausanne	k1gNnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.72	9.72	k4
<g/>
+	+	kIx~
<g/>
0.2	0.2	k4
<g/>
Asafa	Asaf	k1gMnSc2
Powell	Powell	k1gInSc4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
Lausanne	Lausanne	k1gNnPc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.74	9.74	k4
<g/>
+	+	kIx~
<g/>
0.9	0.9	k4
<g/>
Justin	Justin	k1gMnSc1
Gatlin	Gatlin	k1gInSc4
<g/>
15	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
Dauhá	Dauhý	k2eAgFnSc1d1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.78	9.78	k4
<g/>
+	+	kIx~
<g/>
0.9	0.9	k4
<g/>
Nesta	Nesta	k1gMnSc1
Carter	Carter	k1gMnSc1
<g/>
29	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
Rieti	Rieti	k1gNnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.79	9.79	k4
<g/>
+	+	kIx~
<g/>
0.1	0.1	k4
<g/>
Maurice	Maurika	k1gFnSc3
Greene	Green	k1gInSc5
<g/>
16	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
Athény	Athéna	k1gFnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.80	9.80	k4
<g/>
+	+	kIx~
<g/>
1.3	1.3	k4
<g/>
Steve	Steve	k1gMnSc1
Mullings	Mullings	k1gInSc4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
Eugene	Eugen	k1gInSc5
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.82	9.82	k4
<g/>
+	+	kIx~
<g/>
1.7	1.7	k4
<g/>
Richard	Richard	k1gMnSc1
Thompson	Thompson	k1gMnSc1
<g/>
21	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
Port	port	k1gInSc1
of	of	k?
Spain	Spain	k1gInSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.84	9.84	k4
<g/>
+	+	kIx~
<g/>
0.7	0.7	k4
<g/>
Donovan	Donovan	k1gMnSc1
Bailey	Bailea	k1gFnSc2
<g/>
27	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
Atlanta	Atlanta	k1gFnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+0.2	+0.2	k4
<g/>
Bruny	Bruna	k1gMnSc2
Surin	Surin	k1gInSc4
<g/>
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
Sevilla	Sevilla	k1gFnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.3	+1.3	k4
<g/>
Trayvon	Trayvon	k1gMnSc1
Bromell	Bromell	k1gMnSc1
<g/>
25	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
Eugene	Eugen	k1gMnSc5
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.85	9.85	k4
<g/>
+	+	kIx~
<g/>
1.2	1.2	k4
<g/>
Leroy	Leroy	k1gInPc4
Burrell	Burrell	k1gInSc1
<g/>
6	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
Lausanne	Lausanne	k1gNnPc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.7	+1.7	k4
<g/>
Olusoji	Olusoj	k1gInPc7
Fasuba	Fasub	k1gMnSc2
<g/>
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
Dauhá	Dauhý	k2eAgFnSc1d1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.3	+1.3	k4
<g/>
Michael	Michael	k1gMnSc1
Rodgers	Rodgers	k1gInSc4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
Eugene	Eugen	k1gMnSc5
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.86	9.86	k4
<g/>
+	+	kIx~
<g/>
1.2	1.2	k4
<g/>
Carl	Carlum	k1gNnPc2
Lewis	Lewis	k1gFnSc1
<g/>
25	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1991	#num#	k4
<g/>
Tokio	Tokio	k1gNnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
−	−	k?
<g/>
Frankie	Frankie	k1gFnSc1
Fredericks	Fredericks	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
Lausanne	Lausanne	k1gNnPc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.8	+1.8	k4
<g/>
Ato	Ato	k1gMnSc1
Boldon	Boldon	k1gMnSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
Walnut	Walnut	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+0.6	+0.6	k4
<g/>
Francis	Francis	k1gFnSc1
Obikwelu	Obikwel	k1gInSc2
<g/>
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
Athény	Athéna	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.8	+1.8	k4
<g/>
Keston	Keston	k1gInSc1
Bledman	Bledman	k1gMnSc1
<g/>
23	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
Port	port	k1gInSc1
of	of	k?
Spain	Spain	k1gInSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.3	+1.3	k4
<g/>
Jimmy	Jimm	k1gInPc4
Vicaut	Vicaut	k1gInSc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
Saint-Denis	Saint-Denis	k1gFnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.87	9.87	k4
<g/>
+	+	kIx~
<g/>
0.3	0.3	k4
<g/>
Linford	Linfordo	k1gNnPc2
Christie	Christie	k1gFnSc1
<g/>
15	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
Stuttgart	Stuttgart	k1gInSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
-0.2	-0.2	k4
<g/>
Obadele	Obadel	k1gInSc2
Thompson	Thompson	k1gNnSc4
<g/>
11	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
Johannesburg	Johannesburg	k1gInSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.88	9.88	k4
<g/>
+	+	kIx~
<g/>
1.8	1.8	k4
<g/>
Shawn	Shawn	k1gMnSc1
Crawford	Crawford	k1gMnSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
Eugene	Eugen	k1gMnSc5
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.0	+1.0	k4
<g/>
Walter	Walter	k1gMnSc1
Dix	Dix	k1gMnSc1
<g/>
8	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
Nottwil	Nottwila	k1gFnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+0.9	+0.9	k4
<g/>
Ryan	Ryan	k1gInSc1
Bailey	Bailea	k1gFnSc2
<g/>
29	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
Rieti	Rieti	k1gNnPc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.0	+1.0	k4
<g/>
Michael	Michael	k1gMnSc1
Frater	Frater	k1gMnSc1
<g/>
30	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
Lausanne	Lausanne	k1gNnPc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.89	9.89	k4
<g/>
+	+	kIx~
<g/>
1.6	1.6	k4
<g/>
Travis	Travis	k1gInSc1
Padgett	Padgett	k2eAgInSc1d1
<g/>
28	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
Eugene	Eugen	k1gMnSc5
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.6	+1.6	k4
<g/>
Darvis	Darvis	k1gFnSc1
Patton	Patton	k1gInSc1
<g/>
28	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
Eugene	Eugen	k1gMnSc5
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.3	+1.3	k4
<g/>
Ngonidzashe	Ngonidzashe	k1gFnSc1
Makusha	Makusha	k1gFnSc1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
Des	des	k1gNnPc2
Moines	Moinesa	k1gFnPc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
9.90	9.90	k4
<g/>
+	+	kIx~
<g/>
0.4	0.4	k4
<g/>
Nickel	Nicklo	k1gNnPc2
Ashmeade	Ashmead	k1gInSc5
<g/>
11	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
Moskva	Moskva	k1gFnSc1
</s>
<s>
Ženy	žena	k1gFnPc1
</s>
<s>
Současný	současný	k2eAgInSc1d1
světový	světový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
však	však	k9
poslední	poslední	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
bývá	bývat	k5eAaImIp3nS
stále	stále	k6eAd1
více	hodně	k6eAd2
napadán	napadat	k5eAaPmNgMnS,k5eAaBmNgMnS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
možného	možný	k2eAgNnSc2d1
užití	užití	k1gNnSc2
dopingu	doping	k1gInSc2
americkou	americký	k2eAgFnSc7d1
sprinterkou	sprinterka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Florence	Florenc	k1gFnSc2
Griffith-Joynerová	Griffith-Joynerová	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
v	v	k7c6
38	#num#	k4
letech	let	k1gInPc6
na	na	k7c4
následky	následek	k1gInPc4
zadušení	zadušení	k1gNnSc2
ve	v	k7c6
spánku	spánek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevily	objevit	k5eAaPmAgInP
se	se	k3xPyFc4
spekulace	spekulace	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
mohlo	moct	k5eAaImAgNnS
jít	jít	k5eAaImF
o	o	k7c4
důsledek	důsledek	k1gInSc4
dlouhodobého	dlouhodobý	k2eAgNnSc2d1
užívání	užívání	k1gNnSc2
dopingu	doping	k1gInSc2
v	v	k7c6
době	doba	k1gFnSc6
vrcholné	vrcholný	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
(	(	kIx(
<g/>
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
navíc	navíc	k6eAd1
podezřele	podezřele	k6eAd1
rychle	rychle	k6eAd1
skončila	skončit	k5eAaPmAgFnS
téměř	téměř	k6eAd1
ihned	ihned	k6eAd1
po	po	k7c6
rekordních	rekordní	k2eAgInPc6d1
výkonech	výkon	k1gInPc6
z	z	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
výkonu	výkon	k1gInSc2
je	být	k5eAaImIp3nS
na	na	k7c4
věrohodnost	věrohodnost	k1gFnSc4
příliš	příliš	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnPc1d1
sprinterky	sprinterka	k1gFnPc1
se	se	k3xPyFc4
totiž	totiž	k9
ani	ani	k9
po	po	k7c6
dvaceti	dvacet	k4xCc6
letech	let	k1gInPc6
nedokážou	dokázat	k5eNaPmIp3nP
až	až	k9
na	na	k7c4
vzácné	vzácný	k2eAgFnPc4d1
výjimky	výjimka	k1gFnPc4
dostat	dostat	k5eAaPmF
pod	pod	k7c4
hranici	hranice	k1gFnSc4
10,75	10,75	k4
s.	s.	k?
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
ČasVítr	ČasVítr	k1gMnSc1
(	(	kIx(
<g/>
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
AtletkaStátDatumMěsto	AtletkaStátDatumMěsta	k1gFnSc5
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.490.0	10.490.0	k4
<g/>
Florence	Florenc	k1gFnSc2
Griffith-Joynerová	Griffith-Joynerová	k1gFnSc1
<g/>
16	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1988	#num#	k4
<g/>
Indianapolis	Indianapolis	k1gFnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.64	10.64	k4
<g/>
+	+	kIx~
<g/>
1.2	1.2	k4
<g/>
Carmelita	Carmelit	k2eAgFnSc1d1
Jeterová	Jeterová	k1gFnSc1
<g/>
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
Šanghaj	Šanghaj	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.65	10.65	k4
<g/>
+	+	kIx~
<g/>
1.1	1.1	k4
<g/>
Marion	Marion	k1gInSc1
Jonesová	Jonesový	k2eAgFnSc1d1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
Johannesburg	Johannesburg	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.70	10.70	k4
<g/>
+	+	kIx~
<g/>
0.6	0.6	k4
<g/>
Shelly-Ann	Shelly-Anno	k1gNnPc2
Fraserová	Fraserová	k1gFnSc1
<g/>
29	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
Kingston	Kingston	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.73	10.73	k4
<g/>
+	+	kIx~
<g/>
2.0	2.0	k4
<g/>
Christine	Christin	k1gInSc5
Arronová	Arronová	k1gFnSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
Budapešť	Budapešť	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.74	10.74	k4
<g/>
+	+	kIx~
<g/>
1.3	1.3	k4
<g/>
Merlene	Merlen	k1gInSc5
Otteyová	Otteyová	k1gFnSc1
<g/>
7	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
Milán	Milán	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.75	10.75	k4
<g/>
+	+	kIx~
<g/>
0.4	0.4	k4
<g/>
Kerron	Kerron	k1gInSc1
Stewartová	Stewartová	k1gFnSc1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
Řím	Řím	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.76	10.76	k4
<g/>
+	+	kIx~
<g/>
1.7	1.7	k4
<g/>
Evelyn	Evelyno	k1gNnPc2
Ashfordová	Ashfordová	k1gFnSc1
<g/>
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1984	#num#	k4
<g/>
Curych	Curych	k1gInSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.1	+1.1	k4
<g/>
Veronica	Veronica	k1gFnSc1
Campbellová-Brownová	Campbellová-Brownová	k1gFnSc1
<g/>
31	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.77	10.77	k4
<g/>
+	+	kIx~
<g/>
0.9	0.9	k4
<g/>
Irina	Irina	k1gFnSc1
Privalovová	Privalovová	k1gFnSc1
<g/>
6	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
Lausanne	Lausanne	k1gNnPc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+0.7	+0.7	k4
<g/>
Ivet	Iveta	k1gFnPc2
Lalovová	Lalovová	k1gFnSc1
<g/>
19	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
Plovdiv	Plovdiv	k1gInSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.78	10.78	k4
<g/>
+	+	kIx~
<g/>
1.0	1.0	k4
<g/>
Dawn	Dawn	k1gMnSc1
Sowell	Sowell	k1gMnSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
Provo	Provo	k1gNnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.5	+1.5	k4
<g/>
Torri	Torri	k1gNnSc7
Edwardsová	Edwardsová	k1gFnSc1
<g/>
28	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
Eugene	Eugen	k1gMnSc5
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.790.0	10.790.0	k4
<g/>
Xuemei	Xuemei	k1gNnSc1
Li	li	k8xS
<g/>
18	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
Šanghaj	Šanghaj	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
-0.1	-0.1	k4
<g/>
Inger	Inger	k1gInSc1
Millerová	Millerová	k1gFnSc1
<g/>
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
Sevilla	Sevilla	k1gFnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.1	+1.1	k4
<g/>
Blessing	Blessing	k1gInSc1
Okagbareová	Okagbareová	k1gFnSc1
<g/>
27	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
Londýn	Londýn	k1gInSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
+1.8	+1.8	k4
<g/>
English	English	k1gInSc1
Gardnerová	Gardnerová	k1gFnSc1
<g/>
26	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
Eugene	Eugen	k1gMnSc5
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10.80	10.80	k4
<g/>
+	+	kIx~
<g/>
0.8	0.8	k4
<g/>
Tori	Tori	k1gNnPc2
Bowieová	Bowieová	k1gFnSc1
<g/>
18	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
Monako	Monako	k1gNnSc1
</s>
<s>
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
muži	muž	k1gMnPc1
na	na	k7c6
olympijské	olympijský	k2eAgFnSc6d1
trati	trať	k1gFnSc6
jsou	být	k5eAaImIp3nP
Usain	Usain	k2eAgInSc4d1
Bolt	Bolt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc3
z	z	k7c2
LOH	LOH	kA
v	v	k7c6
Pekingu	Peking	k1gInSc6
2008	#num#	k4
dokázal	dokázat	k5eAaPmAgInS
o	o	k7c4
4	#num#	k4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
v	v	k7c6
Londýně	Londýn	k1gInSc6
obhájit	obhájit	k5eAaPmF
a	a	k8xC
Carl	Carl	k1gMnSc1
Lewis	Lewis	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
zvítězit	zvítězit	k5eAaPmF
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angeles	k1gInSc4
1984	#num#	k4
a	a	k8xC
po	po	k7c6
diskvalifikaci	diskvalifikace	k1gFnSc6
Bena	Bena	k?
Johnsona	Johnson	k1gMnSc2
získal	získat	k5eAaPmAgMnS
zlato	zlato	k1gNnSc4
i	i	k8xC
v	v	k7c6
Soulu	Soul	k1gInSc6
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženy	žena	k1gFnSc2
poprvé	poprvé	k6eAd1
běžely	běžet	k5eAaImAgInP
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
v	v	k7c6
Amsterodamu	Amsterodam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepší	dobrý	k2eAgFnSc7d3
ženou	žena	k1gFnSc7
je	být	k5eAaImIp3nS
Wyomia	Wyomia	k1gFnSc1
Tyusová	Tyusová	k1gFnSc1
a	a	k8xC
Gail	Gail	k1gMnSc1
Deversová	Deversová	k1gFnSc1
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
získaly	získat	k5eAaPmAgFnP
zlaté	zlatý	k2eAgFnPc1d1
medaile	medaile	k1gFnPc1
na	na	k7c6
dvou	dva	k4xCgFnPc6
hrách	hra	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Nejúspěšnější	úspěšný	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
jsou	být	k5eAaImIp3nP
USA	USA	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
sportovci	sportovec	k1gMnPc1
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
100	#num#	k4
m	m	kA
získali	získat	k5eAaPmAgMnP
zatím	zatím	k6eAd1
53	#num#	k4
medailí	medaile	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
USA	USA	kA
-	-	kIx~
100	#num#	k4
m	m	kA
</s>
<s>
Zlato	zlato	k1gNnSc1
</s>
<s>
Stříbro	stříbro	k1gNnSc1
</s>
<s>
Bronz	bronz	k1gInSc1
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
16	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
Ženy	žena	k1gFnPc1
</s>
<s>
9	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
25	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
si	se	k3xPyFc3
odnesli	odnést	k5eAaPmAgMnP
Carl	Carl	k1gInSc4
Lewis	Lewis	k1gFnSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Maurice	Maurika	k1gFnSc3
Greene	Green	k1gInSc5
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
)	)	kIx)
po	po	k7c6
třech	tři	k4xCgInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
ženami	žena	k1gFnPc7
je	být	k5eAaImIp3nS
nejúspěšnější	úspěšný	k2eAgInSc4d3
Marion	Marion	k1gInSc4
Jonesová	Jonesový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
získala	získat	k5eAaPmAgFnS
dva	dva	k4xCgInPc4
tituly	titul	k1gInPc4
mistryně	mistryně	k1gFnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
titulů	titul	k1gInPc2
získali	získat	k5eAaPmAgMnP
sportovci	sportovec	k1gMnPc1
USA	USA	kA
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
11	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
5	#num#	k4
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
zvítězila	zvítězit	k5eAaPmAgFnS
Američanka	Američanka	k1gFnSc1
Kelli	Kell	k1gMnSc5
White	Whit	k1gMnSc5
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
usvědčena	usvědčit	k5eAaPmNgFnS
z	z	k7c2
dopingu	doping	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
musela	muset	k5eAaImAgFnS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepšími	dobrý	k2eAgMnPc7d3
Evropany	Evropan	k1gMnPc7
jsou	být	k5eAaImIp3nP
Brit	Brit	k1gMnSc1
Linford	Linford	k1gMnSc1
Christie	Christie	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
získal	získat	k5eAaPmAgInS
zlato	zlato	k1gNnSc4
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
1992	#num#	k4
<g/>
,	,	kIx,
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
ve	v	k7c6
Stuttgartu	Stuttgart	k1gInSc6
1993	#num#	k4
a	a	k8xC
tři	tři	k4xCgInPc1
tituly	titul	k1gInPc1
mistra	mistr	k1gMnSc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trojnásobný	trojnásobný	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
Evropy	Evropa	k1gFnSc2
je	být	k5eAaImIp3nS
i	i	k9
Ukrajinec	Ukrajinec	k1gMnSc1
Valerij	Valerij	k1gMnSc1
Borzov	Borzov	k1gInSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
získal	získat	k5eAaPmAgInS
i	i	k9
zlato	zlato	k1gNnSc4
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ženách	žena	k1gFnPc6
kraluje	kralovat	k5eAaImIp3nS
Němka	Němka	k1gFnSc1
Marlies	Marlies	k1gInSc1
Göhrová	Göhrová	k1gFnSc1
<g/>
,	,	kIx,
mistryně	mistryně	k1gFnSc1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
mistryně	mistryně	k1gFnSc1
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
a	a	k8xC
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
při	při	k7c6
současném	současný	k2eAgInSc6d1
světovém	světový	k2eAgInSc6d1
rekordu	rekord	k1gInSc6
mužů	muž	k1gMnPc2
(	(	kIx(
<g/>
čas	čas	k1gInSc4
9,58	9,58	k4
s	s	k7c7
<g/>
)	)	kIx)
činí	činit	k5eAaImIp3nP
10,44	10,44	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
37,58	37,58	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
žen	žena	k1gFnPc2
(	(	kIx(
<g/>
čas	čas	k1gInSc1
10,49	10,49	k4
s	s	k7c7
<g/>
)	)	kIx)
pak	pak	k6eAd1
9,53	9,53	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
34,32	34,32	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekordman	rekordman	k1gMnSc1
-	-	kIx~
muž	muž	k1gMnSc1
tedy	tedy	k9
běžel	běžet	k5eAaImAgMnS
průměrně	průměrně	k6eAd1
každou	každý	k3xTgFnSc4
sekundu	sekunda	k1gFnSc4
o	o	k7c4
91	#num#	k4
cm	cm	kA
dále	daleko	k6eAd2
než	než	k8xS
žena	žena	k1gFnSc1
-	-	kIx~
rekordmanka	rekordmanka	k1gFnSc1
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
rozdílu	rozdíl	k1gInSc2
3,26	3,26	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
cíli	cíl	k1gInSc6
by	by	kYmCp3nS
rozdíl	rozdíl	k1gInSc1
znamenal	znamenat	k5eAaImAgInS
přibližně	přibližně	k6eAd1
8,7	8,7	k4
metru	metr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
však	však	k9
třeba	třeba	k6eAd1
zmínit	zmínit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
obvykle	obvykle	k6eAd1
běhají	běhat	k5eAaImIp3nP
nejrychlejší	rychlý	k2eAgFnPc1d3
ženy	žena	k1gFnPc1
podstatně	podstatně	k6eAd1
pomaleji	pomale	k6eAd2
než	než	k8xS
rekordmanka	rekordmanka	k1gFnSc1
Florence	Florenc	k1gFnSc2
Griffith	Griffitha	k1gFnPc2
Jonesová	Jonesový	k2eAgFnSc1d1
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
kolem	kolem	k7c2
10,80	10,80	k4
s.	s.	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
mužském	mužský	k2eAgInSc6d1
sprintu	sprint	k1gInSc6
se	se	k3xPyFc4
nejvyšší	vysoký	k2eAgFnSc1d3
dosažená	dosažený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
posledního	poslední	k2eAgNnSc2d1
století	století	k1gNnSc2
výrazně	výrazně	k6eAd1
zvyšovala	zvyšovat	k5eAaImAgFnS
(	(	kIx(
<g/>
od	od	k7c2
hodnoty	hodnota	k1gFnSc2
36,9	36,9	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
až	až	k9
po	po	k7c4
44,7	44,7	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.sportsscientists.com	www.sportsscientists.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2011	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
23	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.iaaf.org	http://www.iaaf.org	k1gInSc1
<g/>
↑	↑	k?
http://www.atletika.cz/aktuality/stromsik-novym-rekordmanem-na-stovce/	http://www.atletika.cz/aktuality/stromsik-novym-rekordmanem-na-stovce/	k?
<g/>
↑	↑	k?
http://www.atletika.cz/aktuality/stromsik-zabehl-vetrnych-1011/	http://www.atletika.cz/aktuality/stromsik-zabehl-vetrnych-1011/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2016/12/09/je-usain-bolt-skutecne-nejrychlejsi/	http://dinosaurusblog.com/2016/12/09/je-usain-bolt-skutecne-nejrychlejsi/	k4
<g/>
↑	↑	k?
http://www.iaaf.org/records/toplists/sprints/100-metres/outdoor/men/senior	http://www.iaaf.org/records/toplists/sprints/100-metres/outdoor/men/senior	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2008/08/31/718667-jak-je-to-s-nejvyssi-rychlosti-cloveka/	https://dinosaurusblog.com/2008/08/31/718667-jak-je-to-s-nejvyssi-rychlosti-cloveka/	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
rychlosti	rychlost	k1gFnSc2
lidského	lidský	k2eAgInSc2d1
sprintu	sprint	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaurusblog	Dinosaurusblog	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
medailistů	medailista	k1gMnPc2
na	na	k7c4
mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
100	#num#	k4
m	m	kA
</s>
<s>
Seznam	seznam	k1gInSc1
medailistů	medailista	k1gMnPc2
na	na	k7c4
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
100	#num#	k4
m	m	kA
</s>
<s>
Seznam	seznam	k1gInSc1
medailistů	medailista	k1gMnPc2
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
100	#num#	k4
m	m	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
asociace	asociace	k1gFnSc2
atletických	atletický	k2eAgFnPc2d1
federací	federace	k1gFnPc2
(	(	kIx(
<g/>
IAAF	IAAF	kA
<g/>
)	)	kIx)
</s>
<s>
Atletické	atletický	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
(	(	kIx(
<g/>
IAAF	IAAF	kA
<g/>
)	)	kIx)
</s>
<s>
Stránky	stránka	k1gFnPc1
Českého	český	k2eAgInSc2d1
atletického	atletický	k2eAgInSc2d1
svazu	svaz	k1gInSc2
</s>
<s>
Veteranska	Veteransko	k1gNnPc1
Atletika	atletika	k1gFnSc1
</s>
<s>
Video	video	k1gNnSc1
světového	světový	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
9,74	9,74	k4
s	s	k7c7
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Atletika	atletika	k1gFnSc1
Sprinty	sprint	k1gInPc5
</s>
<s>
50	#num#	k4
m	m	kA
•	•	k?
60	#num#	k4
m	m	kA
•	•	k?
100	#num#	k4
m	m	kA
•	•	k?
200	#num#	k4
m	m	kA
•	•	k?
400	#num#	k4
m	m	kA
Překážky	překážka	k1gFnSc2
</s>
<s>
60	#num#	k4
m	m	kA
•	•	k?
100	#num#	k4
m	m	kA
•	•	k?
110	#num#	k4
m	m	kA
•	•	k?
400	#num#	k4
m	m	kA
Střední	střední	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
</s>
<s>
800	#num#	k4
m	m	kA
•	•	k?
1000	#num#	k4
m	m	kA
•	•	k?
1500	#num#	k4
m	m	kA
•	•	k?
3000	#num#	k4
m	m	kA
•	•	k?
steeplechase	steeplechase	k1gFnSc2
Dlouhé	Dlouhé	k2eAgFnSc2d1
tratě	trať	k1gFnSc2
</s>
<s>
5000	#num#	k4
m	m	kA
•	•	k?
10	#num#	k4
000	#num#	k4
m	m	kA
•	•	k?
půlmaraton	půlmaraton	k1gInSc1
•	•	k?
maraton	maraton	k1gInSc1
Štafety	štafeta	k1gFnSc2
</s>
<s>
4	#num#	k4
×	×	k?
100	#num#	k4
m	m	kA
•	•	k?
4	#num#	k4
×	×	k?
200	#num#	k4
m	m	kA
•	•	k?
4	#num#	k4
×	×	k?
400	#num#	k4
m	m	kA
•	•	k?
4	#num#	k4
×	×	k?
800	#num#	k4
m	m	kA
•	•	k?
4	#num#	k4
×	×	k?
1500	#num#	k4
m	m	kA
Sportovní	sportovní	k2eAgFnSc2d1
chůze	chůze	k1gFnSc2
</s>
<s>
3	#num#	k4
km	km	kA
•	•	k?
5	#num#	k4
km	km	kA
•	•	k?
10	#num#	k4
km	km	kA
•	•	k?
20	#num#	k4
km	km	kA
•	•	k?
50	#num#	k4
km	km	kA
Vrhy	vrh	k1gInPc4
a	a	k8xC
hody	hod	k1gInPc4
</s>
<s>
Hod	hod	k1gInSc4
diskem	disk	k1gInSc7
•	•	k?
Hod	hod	k1gInSc1
kladivem	kladivo	k1gNnSc7
•	•	k?
Hod	hod	k1gInSc4
oštěpem	oštěp	k1gInSc7
•	•	k?
Vrh	vrh	k1gInSc1
koulí	koulet	k5eAaImIp3nS
Skoky	skok	k1gInPc4
</s>
<s>
Skok	skok	k1gInSc1
vysoký	vysoký	k2eAgInSc1d1
•	•	k?
Skok	skok	k1gInSc1
o	o	k7c6
tyči	tyč	k1gFnSc6
•	•	k?
Skok	skok	k1gInSc4
daleký	daleký	k2eAgInSc4d1
•	•	k?
Trojskok	trojskok	k1gInSc4
Víceboje	víceboj	k1gInSc2
</s>
<s>
Desetiboj	desetiboj	k1gInSc1
•	•	k?
Sedmiboj	sedmiboj	k1gInSc1
•	•	k?
Halový	halový	k2eAgInSc1d1
sedmiboj	sedmiboj	k1gInSc1
•	•	k?
Halový	halový	k2eAgInSc1d1
pětiboj	pětiboj	k1gInSc1
</s>
<s>
Běh	běh	k1gInSc1
na	na	k7c4
100	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
muži	muž	k1gMnSc6
</s>
<s>
1896	#num#	k4
•	•	k?
1900	#num#	k4
•	•	k?
1904	#num#	k4
•	•	k?
1908	#num#	k4
•	•	k?
1912	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1924	#num#	k4
•	•	k?
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2020	#num#	k4
ženy	žena	k1gFnSc2
</s>
<s>
1928	#num#	k4
•	•	k?
1932	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1948	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1960	#num#	k4
•	•	k?
1964	#num#	k4
•	•	k?
1968	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1988	#num#	k4
•	•	k?
1992	#num#	k4
•	•	k?
1996	#num#	k4
•	•	k?
2000	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2008	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2016	#num#	k4
•	•	k?
2020	#num#	k4
Seznam	seznam	k1gInSc4
medailistů	medailista	k1gMnPc2
</s>
