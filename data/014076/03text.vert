<s>
Protaktinium	protaktinium	k1gNnSc1
</s>
<s>
Protaktinium	protaktinium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
2	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
213	#num#	k4
</s>
<s>
Pa	Pa	kA
</s>
<s>
91	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
atom	atom	k1gInSc1
protaktinia	protaktinium	k1gNnSc2
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Protaktinium	protaktinium	k1gNnSc1
<g/>
,	,	kIx,
Pa	Pa	kA
<g/>
,	,	kIx,
91	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Protactinium	Protactinium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
ppm	ppm	k?
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbrný	stříbrný	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-13-3	7440-13-3	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
231,035	231,035	k4
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
161	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
Pa	Pa	kA
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
106	#num#	k4
pm	pm	k?
<g/>
(	(	kIx(
<g/>
Pa	Pa	kA
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
91	#num#	k4
pm	pm	k?
<g/>
(	(	kIx(
<g/>
Pa	Pa	kA
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
88	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
2	#num#	k4
6	#num#	k4
<g/>
d	d	k?
<g/>
1	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
II	II	kA
<g/>
,	,	kIx,
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
V	v	k7c6
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,5	1,5	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
5,60	5,60	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
11,3	11,3	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
20,5	20,5	k4
eV	eV	k?
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
</s>
<s>
36,4	36,4	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
α	α	k1gFnSc1
čtverečná	čtverečný	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
a	a	k8xC
<g/>
=	=	kIx~
388,9	388,9	k4
pm	pm	k?
c	c	k0
<g/>
=	=	kIx~
341,7	341,7	k4
pm	pm	k?
β	β	k1gFnSc1
krychlová	krychlový	k2eAgFnSc1d1
a	a	k8xC
=	=	kIx~
381	#num#	k4
pm	pm	k?
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
15,18	15,18	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
změny	změna	k1gFnSc2
modifikace	modifikace	k1gFnSc2
</s>
<s>
1	#num#	k4
170	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
α	α	k?
→	→	k?
β	β	k?
<g/>
)	)	kIx)
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
443,15	443,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
15,374	15,374	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
α	α	k?
<g/>
,	,	kIx,
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
13,87	13,87	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
47	#num#	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Součinitel	součinitel	k1gInSc1
délkové	délkový	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
</s>
<s>
99	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
α	α	k?
<g/>
)	)	kIx)
</s>
<s>
Molární	molární	k2eAgFnSc1d1
atomizační	atomizační	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
</s>
<s>
607,2	607,2	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
51,9	51,9	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
přibližně	přibližně	k6eAd1
1	#num#	k4
600	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
1	#num#	k4
600	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
přibližně	přibližně	k6eAd1
3	#num#	k4
300	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
3	#num#	k4
300	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
14,65	14,65	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
varu	var	k1gInSc2
</s>
<s>
460,5	460,5	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
0,121	0,121	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
0,099	0,099	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
(	(	kIx(
<g/>
plyn	plyn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
5,56	5,56	k4
<g/>
×	×	k?
<g/>
106	#num#	k4
S	s	k7c7
<g/>
/	/	kIx~
<g/>
m	m	kA
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
177	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
9	#num#	k4
Ω	Ω	k1gFnPc2
</s>
<s>
Teplota	teplota	k1gFnSc1
přechodu	přechod	k1gInSc2
do	do	k7c2
supravodivého	supravodivý	k2eAgInSc2d1
stavu	stav	k1gInSc2
</s>
<s>
1,5	1,5	k4
K	k	k7c3
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pr	pr	k0
<g/>
⋏	⋏	k?
</s>
<s>
Thorium	thorium	k1gNnSc1
≺	≺	k?
<g/>
Pa	Pa	kA
<g/>
≻	≻	k?
Uran	Uran	k1gInSc1
</s>
<s>
Protaktinium	protaktinium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Pa	Pa	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Protactinium	Protactinium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třetím	třetí	k4xOgInSc7
členem	člen	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc4d1
kovový	kovový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Protaktinium	protaktinium	k1gNnSc1
je	být	k5eAaImIp3nS
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
stříbřitě	stříbřitě	k6eAd1
bílé	bílý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
působením	působení	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
mění	měnit	k5eAaImIp3nS
na	na	k7c4
šedavou	šedavý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
protaktinia	protaktinium	k1gNnSc2
231	#num#	k4
<g/>
Pa	Pa	kA
je	být	k5eAaImIp3nS
α	α	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c4
mocenství	mocenství	k1gNnSc4
od	od	k7c2
Pa	Pa	kA
<g/>
+	+	kIx~
<g/>
3	#num#	k4
po	po	k7c6
Pa	Pa	kA
<g/>
+	+	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejstálejší	stálý	k2eAgFnPc1d3
jsou	být	k5eAaImIp3nP
sloučeniny	sloučenina	k1gFnPc1
s	s	k7c7
oxidačním	oxidační	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
Pa	Pa	kA
<g/>
+	+	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
chemickým	chemický	k2eAgNnSc7d1
chováním	chování	k1gNnSc7
podobají	podobat	k5eAaImIp3nP
sloučeninám	sloučenina	k1gFnPc3
tantalu	tantal	k1gInSc2
nebo	nebo	k8xC
niobu	niob	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1
kov	kov	k1gInSc1
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
redukcí	redukce	k1gFnSc7
fluoridu	fluorid	k1gInSc2
protaktiničného	protaktiničný	k2eAgInSc2d1
kovovým	kovový	k2eAgNnSc7d1
baryem	baryum	k1gNnSc7
při	při	k7c6
teplotě	teplota	k1gFnSc6
kolem	kolem	k7c2
1400	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc7
identifikovali	identifikovat	k5eAaBmAgMnP
protaktinium	protaktinium	k1gNnSc4
(	(	kIx(
<g/>
izotop	izotop	k1gInSc1
234	#num#	k4
<g/>
Pa	Pa	kA
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
1,17	1,17	k4
minuty	minuta	k1gFnSc2
<g/>
)	)	kIx)
Kasimir	Kasimir	k1gInSc1
Fajans	Fajans	k1gInSc1
a	a	k8xC
O.	O.	kA
H.	H.	kA
Göhring	Göhring	k1gInSc1
jako	jako	k8xC,k8xS
produkt	produkt	k1gInSc1
rozpadu	rozpad	k1gInSc2
uranu	uran	k1gInSc2
238	#num#	k4
<g/>
U.	U.	kA
Pojmenovali	pojmenovat	k5eAaPmAgMnP
jej	on	k3xPp3gNnSc4
brevium	brevium	k1gNnSc4
podle	podle	k7c2
krátké	krátký	k2eAgFnSc2d1
doby	doba	k1gFnSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
brevis	brevis	k1gInSc1
–	–	k?
krátký	krátký	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
objevitele	objevitel	k1gMnSc2
protaktinia	protaktinium	k1gNnSc2
jsou	být	k5eAaImIp3nP
však	však	k9
obvykle	obvykle	k6eAd1
označováni	označován	k2eAgMnPc1d1
Otto	Otto	k1gMnSc1
Hahn	Hahn	k1gMnSc1
a	a	k8xC
Lise	lis	k1gInSc6
Meitner	Meitnra	k1gFnPc2
z	z	k7c2
Německa	Německo	k1gNnSc2
a	a	k8xC
Frederick	Frederick	k1gMnSc1
Soddy	Sodda	k1gFnSc2
a	a	k8xC
John	John	k1gMnSc1
Cranston	Cranston	k1gInSc4
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
roku	rok	k1gInSc2
1918	#num#	k4
nezávisle	závisle	k6eNd1
na	na	k7c4
sobě	se	k3xPyFc3
oznámili	oznámit	k5eAaPmAgMnP
objev	objev	k1gInSc4
izotopu	izotop	k1gInSc2
231	#num#	k4
<g/>
Pa	Pa	kA
s	s	k7c7
mnohem	mnohem	k6eAd1
delším	dlouhý	k2eAgInSc7d2
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
prvku	prvek	k1gInSc2
bylo	být	k5eAaImAgNnS
změněno	změnit	k5eAaPmNgNnS
na	na	k7c4
protaktinium	protaktinium	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
<g/>
,	,	kIx,
izotopy	izotop	k1gInPc1
a	a	k8xC
využití	využití	k1gNnSc1
</s>
<s>
V	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
se	se	k3xPyFc4
můžeme	moct	k5eAaImIp1nP
setkat	setkat	k5eAaPmF
pouze	pouze	k6eAd1
s	s	k7c7
velmi	velmi	k6eAd1
nízkými	nízký	k2eAgInPc7d1
obsahy	obsah	k1gInPc7
izotopu	izotop	k1gInSc2
231	#num#	k4
<g/>
Pa	Pa	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
produktem	produkt	k1gInSc7
radioaktivního	radioaktivní	k2eAgInSc2d1
rozpadu	rozpad	k1gInSc2
uranu	uran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc2
tohoto	tento	k3xDgInSc2
izotopu	izotop	k1gInSc2
je	být	k5eAaImIp3nS
32	#num#	k4
760	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
proto	proto	k8xC
i	i	k9
v	v	k7c6
nejbohatších	bohatý	k2eAgFnPc6d3
uranových	uranový	k2eAgFnPc6d1
rudách	ruda	k1gFnPc6
nacházíme	nacházet	k5eAaImIp1nP
protaktinium	protaktinium	k1gNnSc4
v	v	k7c6
množství	množství	k1gNnSc6
maximálně	maximálně	k6eAd1
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
ppm	ppm	k?
(	(	kIx(
<g/>
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
dalších	další	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
stojí	stát	k5eAaImIp3nS
za	za	k7c4
zmínku	zmínka	k1gFnSc4
např.	např.	kA
230	#num#	k4
<g/>
Pa	Pa	kA
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
17,4	17,4	k4
dne	den	k1gInSc2
nebo	nebo	k8xC
233	#num#	k4
<g/>
Pa	Pa	kA
s	s	k7c7
poločasem	poločas	k1gInSc7
26,975	26,975	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
30	#num#	k4
izotopů	izotop	k1gInPc2
protaktinia	protaktinium	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
IzotopPoločas	IzotopPoločas	k1gMnSc1
přeměnyDruh	přeměnyDruh	k1gMnSc1
rozpaduProdukt	rozpaduProdukt	k1gInSc4
rozpadu	rozpad	k1gInSc2
</s>
<s>
211	#num#	k4
<g/>
Pa	Pa	kA
<g/>
>	>	kIx)
<g/>
300	#num#	k4
nsα	nsα	k?
<g/>
207	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
212	#num#	k4
<g/>
Pa	Pa	kA
<g/>
5,1	5,1	k4
msα	msα	k?
<g/>
208	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
213	#num#	k4
<g/>
Pa	Pa	kA
<g/>
5,3	5,3	k4
msα	msα	k?
<g/>
209	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
214	#num#	k4
<g/>
Pa	Pa	kA
<g/>
17	#num#	k4
msα	msα	k?
<g/>
210	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
215	#num#	k4
<g/>
Pa	Pa	kA
<g/>
14	#num#	k4
msα	msα	k?
<g/>
211	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
216	#num#	k4
<g/>
Pa	Pa	kA
<g/>
150	#num#	k4
msα	msα	k?
(	(	kIx(
<g/>
98	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
2	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
212	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
<g/>
/	/	kIx~
216	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
217	#num#	k4
<g/>
Pa	Pa	kA
<g/>
3,6	3,6	k4
msα	msα	k?
<g/>
213	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
218	#num#	k4
<g/>
Pa	Pa	kA
<g/>
113	#num#	k4
μ	μ	k?
<g/>
214	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
219	#num#	k4
<g/>
Pa	Pa	kA
<g/>
53	#num#	k4
nsα	nsα	k?
<g/>
215	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
220	#num#	k4
<g/>
Pa	Pa	kA
<g/>
0,78	0,78	k4
μ	μ	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
3,0	3,0	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
216	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
<g/>
/	/	kIx~
220	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
221	#num#	k4
<g/>
Pa	Pa	kA
<g/>
5,9	5,9	k4
μ	μ	k?
<g/>
217	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
222	#num#	k4
<g/>
Pa	Pa	kA
<g/>
2,9	2,9	k4
msα	msα	k?
<g/>
218	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
223	#num#	k4
<g/>
Pa	Pa	kA
<g/>
5,1	5,1	k4
msα	msα	k?
<g/>
219	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
224	#num#	k4
<g/>
Pa	Pa	kA
<g/>
846	#num#	k4
msα	msα	k?
<g/>
220	#num#	k4
<g/>
Ac	Ac	k1gFnSc1
</s>
<s>
225	#num#	k4
<g/>
Pa	Pa	kA
<g/>
1,7	1,7	k4
sα	sα	k?
<g/>
221	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
226	#num#	k4
<g/>
Pa	Pa	kA
<g/>
1,8	1,8	k4
minα	minα	k?
(	(	kIx(
<g/>
74	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
26	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
222	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
<g/>
/	/	kIx~
226	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
227	#num#	k4
<g/>
Pa	Pa	kA
<g/>
38,3	38,3	k4
minα	minα	k?
(	(	kIx(
<g/>
85	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
15	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
223	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
<g/>
/	/	kIx~
227	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
228	#num#	k4
<g/>
Pa	Pa	kA
<g/>
22,4	22,4	k4
hε	hε	k?
(	(	kIx(
<g/>
98,15	98,15	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
1,85	1,85	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
228	#num#	k4
<g/>
Th	Th	k1gFnPc2
/	/	kIx~
224	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
229	#num#	k4
<g/>
Pa	Pa	kA
<g/>
1,5	1,5	k4
dε	dε	k?
(	(	kIx(
<g/>
99,52	99,52	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
0,48	0,48	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
229	#num#	k4
<g/>
Th	Th	k1gFnPc2
<g/>
/	/	kIx~
225	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
230	#num#	k4
<g/>
Pa	Pa	kA
<g/>
17,4	17,4	k4
dε	dε	k?
(	(	kIx(
<g/>
92,20	92,20	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
β	β	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
7,80	7,80	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
3,2	3,2	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
230	#num#	k4
<g/>
Th	Th	k1gFnPc2
<g/>
/	/	kIx~
230	#num#	k4
<g/>
U	U	kA
<g/>
/	/	kIx~
226	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
</s>
<s>
231	#num#	k4
<g/>
Pa	Pa	kA
<g/>
32	#num#	k4
760	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
<	<	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
227	#num#	k4
<g/>
Ac	Ac	k1gFnPc2
/	/	kIx~
různé	různý	k2eAgInPc1d1
</s>
<s>
232	#num#	k4
<g/>
Pa	Pa	kA
<g/>
1,32	1,32	k4
dβ	dβ	k?
<g/>
−	−	k?
/	/	kIx~
ε	ε	k?
<g/>
232	#num#	k4
<g/>
U	U	kA
/	/	kIx~
232	#num#	k4
<g/>
Th	Th	k1gFnPc2
</s>
<s>
233	#num#	k4
<g/>
Pa	Pa	kA
<g/>
26,975	26,975	k4
dβ	dβ	k?
<g/>
−	−	k?
<g/>
233	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
234	#num#	k4
<g/>
Pa	Pa	kA
<g/>
6,70	6,70	k4
hβ	hβ	k?
<g/>
−	−	k?
<g/>
234	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
235	#num#	k4
<g/>
Pa	Pa	kA
<g/>
24,4	24,4	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
235	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
236	#num#	k4
<g/>
Pa	Pa	kA
<g/>
9,1	9,1	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
236	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
237	#num#	k4
<g/>
Pa	Pa	kA
<g/>
8,7	8,7	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
237	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
238	#num#	k4
<g/>
Pa	Pa	kA
<g/>
2,28	2,28	k4
minβ	minβ	k?
<g/>
−	−	k?
<g/>
238	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
239	#num#	k4
<g/>
Pa	Pa	kA
<g/>
1,8	1,8	k4
hβ	hβ	k?
<g/>
−	−	k?
<g/>
239	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
240	#num#	k4
<g/>
Pa	Pa	kA
?	?	kIx.
</s>
<s desamb="1">
<g/>
β	β	k?
<g/>
−	−	k?
<g/>
240	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
První	první	k4xOgFnSc1
izolace	izolace	k1gFnSc1
oxidu	oxid	k1gInSc2
protaktinia	protaktinium	k1gNnSc2
Pa	Pa	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
5	#num#	k4
byla	být	k5eAaImAgFnS
uskutečněna	uskutečněn	k2eAgFnSc1d1
roku	rok	k1gInSc2
1927	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Aristid	Aristid	k1gInSc1
V.	V.	kA
Grosse	Gross	k1gMnSc2
připravil	připravit	k5eAaPmAgInS
přibližně	přibližně	k6eAd1
2	#num#	k4
mg	mg	kA
látky	látka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elementární	elementární	k2eAgInSc1d1
kov	kov	k1gInSc1
byl	být	k5eAaImAgInS
získán	získán	k2eAgInSc1d1
roku	rok	k1gInSc2
1934	#num#	k4
termickým	termický	k2eAgInSc7d1
rozkladem	rozklad	k1gInSc7
jodidu	jodid	k1gInSc2
protaktinia	protaktinium	k1gNnSc2
na	na	k7c6
elektricky	elektricky	k6eAd1
zahřívaném	zahřívaný	k2eAgNnSc6d1
kovovém	kovový	k2eAgNnSc6d1
vlákně	vlákno	k1gNnSc6
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
PaI	PaI	k1gFnSc1
<g/>
5	#num#	k4
→	→	k?
2	#num#	k4
Pa	Pa	kA
+	+	kIx~
5	#num#	k4
I2	I2	k1gFnPc2
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
množství	množství	k1gNnSc1
čistého	čistý	k2eAgInSc2d1
prvku	prvek	k1gInSc2
bylo	být	k5eAaImAgNnS
připraveno	připravit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
pod	pod	k7c7
patronací	patronace	k1gFnSc7
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
atomovou	atomový	k2eAgFnSc4d1
energii	energie	k1gFnSc4
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
přitom	přitom	k6eAd1
zpracováváno	zpracovávat	k5eAaImNgNnS
asi	asi	k9
60	#num#	k4
tun	tuna	k1gFnPc2
kalů	kal	k1gInPc2
zbylých	zbylý	k2eAgInPc2d1
po	po	k7c6
extrakci	extrakce	k1gFnSc6
uranu	uran	k1gInSc2
z	z	k7c2
konžských	konžský	k2eAgFnPc2d1
rud	ruda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Separační	separační	k2eAgInSc1d1
proces	proces	k1gInSc1
sestával	sestávat	k5eAaImAgInS
z	z	k7c2
dvanácti	dvanáct	k4xCc2
kroků	krok	k1gInPc2
(	(	kIx(
<g/>
loužení	loužení	k1gNnSc1
kyselinami	kyselina	k1gFnPc7
<g/>
,	,	kIx,
kapalinová	kapalinový	k2eAgFnSc1d1
extrakce	extrakce	k1gFnSc1
<g/>
,	,	kIx,
separace	separace	k1gFnSc1
na	na	k7c6
ionexech	ionex	k1gInPc6
atd.	atd.	kA
<g/>
)	)	kIx)
a	a	k8xC
výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
125	#num#	k4
g	g	kA
kovového	kovový	k2eAgNnSc2d1
protaktinia	protaktinium	k1gNnSc2
o	o	k7c6
čistotě	čistota	k1gFnSc6
99,9	99,9	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
náklady	náklad	k1gInPc4
na	na	k7c4
tento	tento	k3xDgInSc4
proces	proces	k1gInSc4
se	se	k3xPyFc4
pohybovaly	pohybovat	k5eAaImAgInP
kolem	kolem	k6eAd1
půl	půl	k1xP
milionu	milion	k4xCgInSc2
amerických	americký	k2eAgMnPc2d1
dolarů	dolar	k1gInPc2
a	a	k8xC
získané	získaný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
protaktinia	protaktinium	k1gNnSc2
dodnes	dodnes	k6eAd1
uspokojuje	uspokojovat	k5eAaImIp3nS
celosvětovou	celosvětový	k2eAgFnSc4d1
poptávku	poptávka	k1gFnSc4
po	po	k7c6
tomto	tento	k3xDgInSc6
prvku	prvek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jasně	jasně	k6eAd1
ukazuje	ukazovat	k5eAaImIp3nS
i	i	k9
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
praktický	praktický	k2eAgInSc1d1
význam	význam	k1gInSc1
protaktinia	protaktinium	k1gNnSc2
je	být	k5eAaImIp3nS
zanedbatelný	zanedbatelný	k2eAgInSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
vyžití	vyžití	k1gNnSc1
se	se	k3xPyFc4
omezuje	omezovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c4
speciální	speciální	k2eAgInPc4d1
vědecké	vědecký	k2eAgInPc4d1
experimenty	experiment	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Budoucí	budoucí	k2eAgInSc1d1
význam	význam	k1gInSc1
protaktinia	protaktinium	k1gNnSc2
a	a	k8xC
především	především	k6eAd1
izotopů	izotop	k1gInPc2
233	#num#	k4
<g/>
Pa	Pa	kA
a	a	k8xC
234	#num#	k4
<g/>
Pa	Pa	kA
záleží	záležet	k5eAaImIp3nS
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
solných	solný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
233	#num#	k4
<g/>
Pa	Pa	kA
vznikajícího	vznikající	k2eAgInSc2d1
záchytem	záchyt	k1gInSc7
neutronu	neutron	k1gInSc2
jádrem	jádro	k1gNnSc7
thoria	thorium	k1gNnSc2
232	#num#	k4
<g/>
Th	Th	k1gFnPc2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc7
rozpadem	rozpad	k1gInSc7
získává	získávat	k5eAaImIp3nS
izotop	izotop	k1gInSc1
uranu	uran	k1gInSc2
233	#num#	k4
<g/>
U	U	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
perspektivní	perspektivní	k2eAgFnSc7d1
náhradou	náhrada	k1gFnSc7
235	#num#	k4
<g/>
U.	U.	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
protaktinium	protaktinium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
protaktinium	protaktinium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4175981-3	4175981-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85107634	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85107634	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
