<s>
Redukcionismus	redukcionismus	k1gInSc1
</s>
<s>
Redukcionismus	redukcionismus	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
reductio	reductio	k1gNnSc1
<g/>
,	,	kIx,
přivedení	přivedení	k1gNnSc1
nazpět	nazpět	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
výkladový	výkladový	k2eAgInSc4d1
postup	postup	k1gInSc4
a	a	k8xC
myšlenkový	myšlenkový	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
vysvětlovat	vysvětlovat	k5eAaImF
složité	složitý	k2eAgFnPc4d1
skutečnosti	skutečnost	k1gFnPc4
převedením	převedení	k1gNnSc7
na	na	k7c4
jednoduché	jednoduchý	k2eAgFnPc4d1
části	část	k1gFnPc4
zejména	zejména	k9
rozkladem	rozklad	k1gInSc7
a	a	k8xC
tvrzením	tvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
celek	celek	k1gInSc1
není	být	k5eNaImIp3nS
„	„	k?
<g/>
nic	nic	k6eAd1
než	než	k8xS
<g/>
“	“	k?
soubor	soubor	k1gInSc1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mechanická	mechanický	k2eAgFnSc1d1
kachna	kachna	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Jacques	Jacques	k1gMnSc1
de	de	k?
Vaucanson	Vaucanson	k1gMnSc1
<g/>
,	,	kIx,
1739	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1
</s>
<s>
Snaha	snaha	k1gFnSc1
vyložit	vyložit	k5eAaPmF
složité	složitý	k2eAgFnPc4d1
skutečnosti	skutečnost	k1gFnPc4
pomocí	pomocí	k7c2
jednodušších	jednoduchý	k2eAgInPc2d2
je	být	k5eAaImIp3nS
podstatou	podstata	k1gFnSc7
každého	každý	k3xTgInSc2
výkladu	výklad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnPc4
řečtí	řecký	k2eAgMnPc1d1
filosofové	filosof	k1gMnPc1
<g/>
,	,	kIx,
předsókratici	předsókratik	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
najít	najít	k5eAaPmF
počátek	počátek	k1gInSc4
nebo	nebo	k8xC
původ	původ	k1gInSc4
světa	svět	k1gInSc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
ze	z	k7c2
živlů	živel	k1gInPc2
<g/>
,	,	kIx,
neznamená	znamenat	k5eNaImIp3nS
ještě	ještě	k6eAd1
úplné	úplný	k2eAgNnSc4d1
převedení	převedení	k1gNnSc4
čili	čili	k8xC
redukci	redukce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchůdcem	předchůdce	k1gMnSc7
redukcionismu	redukcionismus	k1gInSc2
–	–	k?
pokud	pokud	k8xS
lze	lze	k6eAd1
z	z	k7c2
dochovaných	dochovaný	k2eAgInPc2d1
zlomků	zlomek	k1gInPc2
spolehlivě	spolehlivě	k6eAd1
soudit	soudit	k5eAaImF
–	–	k?
je	být	k5eAaImIp3nS
teprve	teprve	k6eAd1
Démokritos	Démokritos	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP
sice	sice	k8xC
sladké	sladký	k2eAgFnPc1d1
a	a	k8xC
hořké	hořký	k2eAgFnPc1d1
<g/>
,	,	kIx,
teplé	teplý	k2eAgFnPc1d1
a	a	k8xC
studené	studený	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
jsou	být	k5eAaImIp3nP
jen	jen	k9
atomy	atom	k1gInPc1
a	a	k8xC
prázdno	prázdno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Démokritos	Démokritos	k1gMnSc1
<g/>
,	,	kIx,
zl	zl	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
DK	DK	kA
68	#num#	k4
B	B	kA
9	#num#	k4
</s>
<s>
U	u	k7c2
Démokrita	Démokritos	k1gMnSc2
provází	provázet	k5eAaImIp3nS
ovšem	ovšem	k9
toto	tento	k3xDgNnSc1
přesvědčení	přesvědčení	k1gNnSc1
i	i	k8xC
hluboká	hluboký	k2eAgFnSc1d1
skepse	skepse	k1gFnSc1
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
do	do	k7c2
možností	možnost	k1gFnPc2
lidského	lidský	k2eAgNnSc2d1
poznání	poznání	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
nevíme	vědět	k5eNaImIp1nP
nic	nic	k3yNnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
pravda	pravda	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
hlubině	hlubina	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
propasti	propast	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Démokritos	Démokritos	k1gMnSc1
<g/>
,	,	kIx,
zl	zl	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
DK	DK	kA
68	#num#	k4
B	B	kA
117	#num#	k4
</s>
<s>
Descartes	Descartes	k1gMnSc1
a	a	k8xC
novověká	novověký	k2eAgFnSc1d1
věda	věda	k1gFnSc1
</s>
<s>
Novověká	novověký	k2eAgFnSc1d1
věda	věda	k1gFnSc1
si	se	k3xPyFc3
klade	klást	k5eAaImIp3nS
jiný	jiný	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
:	:	kIx,
smyslem	smysl	k1gInSc7
poznání	poznání	k1gNnSc2
není	být	k5eNaImIp3nS
skutečnosti	skutečnost	k1gFnPc4
rozumět	rozumět	k5eAaImF
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
ji	on	k3xPp3gFnSc4
účinně	účinně	k6eAd1
ovládnout	ovládnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
výborně	výborně	k6eAd1
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
reduktivní	reduktivní	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
skutečnost	skutečnost	k1gFnSc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c4
jednoduché	jednoduchý	k2eAgFnPc4d1
součásti	součást	k1gFnPc4
a	a	k8xC
složky	složka	k1gFnPc4
a	a	k8xC
zanedbává	zanedbávat	k5eAaImIp3nS
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
takovému	takový	k3xDgMnSc3
rozložení	rozložený	k2eAgMnPc1d1
příčí	příčit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
se	se	k3xPyFc4
rozklad	rozklad	k1gInSc1
na	na	k7c4
jednoduché	jednoduchý	k2eAgFnPc4d1
složky	složka	k1gFnPc4
podaří	podařit	k5eAaPmIp3nS
<g/>
,	,	kIx,
lze	lze	k6eAd1
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
najít	najít	k5eAaPmF
i	i	k9
jednoduché	jednoduchý	k2eAgInPc1d1
příčinné	příčinný	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
(	(	kIx(
<g/>
kauzalita	kauzalita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
vyjádřit	vyjádřit	k5eAaPmF
matematicky	matematicky	k6eAd1
a	a	k8xC
s	s	k7c7
jejich	jejich	k3xOp3gFnSc7
pomocí	pomoc	k1gFnSc7
s	s	k7c7
jistotou	jistota	k1gFnSc7
předvídat	předvídat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
metodu	metoda	k1gFnSc4
vypracoval	vypracovat	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
a	a	k8xC
vědec	vědec	k1gMnSc1
René	René	k1gMnSc1
Descartes	Descartes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reduktivní	reduktivní	k2eAgInSc1d1
postup	postup	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
u	u	k7c2
Descarta	Descart	k1gMnSc2
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
skutečnost	skutečnost	k1gFnSc1
chápe	chápat	k5eAaImIp3nS
jako	jako	k9
neživou	živý	k2eNgFnSc4d1
<g/>
,	,	kIx,
složenou	složený	k2eAgFnSc4d1
z	z	k7c2
neživých	živý	k2eNgFnPc2d1
částí	část	k1gFnPc2
a	a	k8xC
ovládanou	ovládaný	k2eAgFnSc4d1
zákony	zákon	k1gInPc1
mechaniky	mechanika	k1gFnSc2
<g/>
:	:	kIx,
i	i	k9
živé	živý	k2eAgInPc1d1
organismy	organismus	k1gInPc1
jsou	být	k5eAaImIp3nP
stroje	stroj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
rozebrat	rozebrat	k5eAaPmF
a	a	k8xC
opět	opět	k6eAd1
složit	složit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k8xS
člověk	člověk	k1gMnSc1
má	mít	k5eAaImIp3nS
kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
nehmotnou	hmotný	k2eNgFnSc4d1
duši	duše	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
nelze	lze	k6eNd1
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
rozkládat	rozkládat	k5eAaImF
a	a	k8xC
skládat	skládat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Descartův	Descartův	k2eAgInSc1d1
mechanistický	mechanistický	k2eAgInSc1d1
postup	postup	k1gInSc1
se	se	k3xPyFc4
skvěle	skvěle	k6eAd1
osvědčil	osvědčit	k5eAaPmAgInS
při	při	k7c6
zkoumání	zkoumání	k1gNnSc6
a	a	k8xC
ovládání	ovládání	k1gNnSc6
neživé	živý	k2eNgFnSc2d1
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
narazil	narazit	k5eAaPmAgMnS
však	však	k9
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
musel	muset	k5eAaImAgInS
obětovat	obětovat	k5eAaBmF
příliš	příliš	k6eAd1
mnoho	mnoho	k4c4
–	–	k?
už	už	k6eAd1
při	při	k7c6
výkladu	výklad	k1gInSc6
meteorologických	meteorologický	k2eAgInPc2d1
jevů	jev	k1gInPc2
a	a	k8xC
zejména	zejména	k9
při	při	k7c6
zkoumání	zkoumání	k1gNnSc6
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Descartův	Descartův	k2eAgInSc4d1
výklad	výklad	k1gInSc4
fysiologie	fysiologie	k1gFnSc2
lidského	lidský	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
neprosadil	prosadit	k5eNaPmAgInS
a	a	k8xC
švédská	švédský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
Kristýna	Kristýna	k1gFnSc1
I.	I.	kA
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
knihu	kniha	k1gFnSc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
odepsala	odepsat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
výklad	výklad	k1gInSc1
líbí	líbit	k5eAaImIp3nS
<g/>
,	,	kIx,
nedovede	dovést	k5eNaPmIp3nS
si	se	k3xPyFc3
však	však	k9
představit	představit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
dvěma	dva	k4xCgInPc3
hodinovým	hodinový	k2eAgInPc3d1
strojům	stroj	k1gInPc3
narodily	narodit	k5eAaPmAgFnP
malé	malý	k2eAgFnPc4d1
hodinky	hodinka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
těšily	těšit	k5eAaImAgFnP
velké	velký	k2eAgFnPc1d1
pozornosti	pozornost	k1gFnPc1
mechanické	mechanický	k2eAgFnPc1d1
automaty	automat	k1gInPc4
<g/>
,	,	kIx,
napodobující	napodobující	k2eAgNnSc4d1
chování	chování	k1gNnSc4
živočichů	živočich	k1gMnPc2
i	i	k9
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
</s>
<s>
Od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
si	se	k3xPyFc3
pokusná	pokusný	k2eAgFnSc1d1
(	(	kIx(
<g/>
empirická	empirický	k2eAgFnSc1d1
<g/>
)	)	kIx)
věda	věda	k1gFnSc1
postupně	postupně	k6eAd1
uvědomuje	uvědomovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
objevování	objevování	k1gNnSc1
přírodních	přírodní	k2eAgFnPc2d1
zákonitostí	zákonitost	k1gFnPc2
vyžaduje	vyžadovat	k5eAaImIp3nS
umělé	umělý	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
experimentu	experiment	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pokud	pokud	k8xS
možno	možno	k6eAd1
vyloučí	vyloučit	k5eAaPmIp3nS
všechny	všechen	k3xTgInPc4
rušivé	rušivý	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
:	:	kIx,
tak	tak	k6eAd1
volný	volný	k2eAgInSc1d1
pád	pád	k1gInSc1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
měřit	měřit	k5eAaImF
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
nástrojem	nástroj	k1gInSc7
tohoto	tento	k3xDgNnSc2
poznávání	poznávání	k1gNnSc2
stávají	stávat	k5eAaImIp3nP
modely	model	k1gInPc4
a	a	k8xC
modelování	modelování	k1gNnSc4
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
mechanické	mechanický	k2eAgInPc1d1
<g/>
,	,	kIx,
matematické	matematický	k2eAgInPc1d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
s	s	k7c7
pomocí	pomoc	k1gFnSc7
počítačů	počítač	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redukcionismus	redukcionismus	k1gInSc1
pak	pak	k6eAd1
znamená	znamenat	k5eAaImIp3nS
přesvědčení	přesvědčení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
složité	složitý	k2eAgFnPc4d1
skutečnosti	skutečnost	k1gFnPc4
lze	lze	k6eAd1
ztotožnit	ztotožnit	k5eAaPmF
s	s	k7c7
jejich	jejich	k3xOp3gInPc7
zjednodušenými	zjednodušený	k2eAgInPc7d1
modely	model	k1gInPc7
a	a	k8xC
vyložit	vyložit	k5eAaPmF
beze	beze	k7c2
zbytku	zbytek	k1gInSc2
ze	z	k7c2
zákonitostí	zákonitost	k1gFnPc2
jejich	jejich	k3xOp3gFnPc2
jednodušších	jednoduchý	k2eAgFnPc2d2
složek	složka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redukcionismus	redukcionismus	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
postupů	postup	k1gInPc2
positivismu	positivismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
chce	chtít	k5eAaImIp3nS
poznání	poznání	k1gNnSc4
redukovat	redukovat	k5eAaBmF
na	na	k7c4
vykazatelná	vykazatelný	k2eAgNnPc4d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
měřitelná	měřitelný	k2eAgNnPc4d1
fakta	faktum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
redukcionismu	redukcionismus	k1gInSc2
</s>
<s>
Námitky	námitka	k1gFnPc1
proti	proti	k7c3
redukcionismu	redukcionismus	k1gInSc3
jsou	být	k5eAaImIp3nP
několika	několik	k4yIc2
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
shrnout	shrnout	k5eAaPmF
do	do	k7c2
zásady	zásada	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
celek	celek	k1gInSc1
je	být	k5eAaImIp3nS
víc	hodně	k6eAd2
než	než	k8xS
souhrn	souhrn	k1gInSc1
jeho	jeho	k3xOp3gFnPc2
částí	část	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Aristotelés	Aristotelés	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Živé	živý	k2eAgInPc1d1
organismy	organismus	k1gInPc1
nelze	lze	k6eNd1
rozebrat	rozebrat	k5eAaPmF
a	a	k8xC
opět	opět	k6eAd1
složit	složit	k5eAaPmF
jako	jako	k8xC,k8xS
stroje	stroj	k1gInPc4
<g/>
;	;	kIx,
</s>
<s>
kromě	kromě	k7c2
částí	část	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nP
celek	celek	k1gInSc1
i	i	k8xC
různé	různý	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
,	,	kIx,
struktura	struktura	k1gFnSc1
celku	celek	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
na	na	k7c6
úrovní	úroveň	k1gFnPc2
složitých	složitý	k2eAgInPc2d1
systémů	systém	k1gInPc2
lze	lze	k6eAd1
pozorovat	pozorovat	k5eAaImF
různé	různý	k2eAgInPc4d1
emergentní	emergentní	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
nelze	lze	k6eNd1
vysvětlit	vysvětlit	k5eAaPmF
jen	jen	k9
z	z	k7c2
vlastností	vlastnost	k1gFnPc2
jejich	jejich	k3xOp3gFnPc2
částí	část	k1gFnPc2
<g/>
;	;	kIx,
</s>
<s>
u	u	k7c2
složitých	složitý	k2eAgNnPc2d1
(	(	kIx(
<g/>
zejména	zejména	k9
živých	živý	k2eAgNnPc2d1
<g/>
)	)	kIx)
celků	celek	k1gInPc2
pozorujeme	pozorovat	k5eAaImIp1nP
i	i	k9
ovlivnění	ovlivnění	k1gNnSc4
částí	část	k1gFnPc2
celkem	celkem	k6eAd1
(	(	kIx(
<g/>
tzv.	tzv.	kA
downward	downward	k1gInSc1
causation	causation	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
adaptace	adaptace	k1gFnSc1
<g/>
,	,	kIx,
evoluce	evoluce	k1gFnSc1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
bychom	by	kYmCp1nP
chtěli	chtít	k5eAaImAgMnP
živý	živý	k2eAgInSc4d1
organismus	organismus	k1gInSc4
vysvětlit	vysvětlit	k5eAaPmF
pouze	pouze	k6eAd1
z	z	k7c2
atomů	atom	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
vlastností	vlastnost	k1gFnPc2
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
bychom	by	kYmCp1nP
u	u	k7c2
nich	on	k3xPp3gInPc2
předpokládat	předpokládat	k5eAaImF
nějaký	nějaký	k3yIgInSc1
zárodek	zárodek	k1gInSc1
schopnosti	schopnost	k1gFnSc2
reprodukce	reprodukce	k1gFnSc2
a	a	k8xC
dokonce	dokonce	k9
i	i	k8xC
vývoje	vývoj	k1gInSc2
a	a	k8xC
vědomí	vědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
vědeckého	vědecký	k2eAgInSc2d1
redukcionismu	redukcionismus	k1gInSc2
vysvětluje	vysvětlovat	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
evolucionista	evolucionista	k1gMnSc1
Richard	Richard	k1gMnSc1
Boyd	Boyd	k1gMnSc1
takto	takto	k6eAd1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Žádný	žádný	k3yNgMnSc1
rozumný	rozumný	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
si	se	k3xPyFc3
nemyslí	myslet	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
složitost	složitost	k1gFnSc1
organického	organický	k2eAgInSc2d1
či	či	k8xC
kulturního	kulturní	k2eAgInSc2d1
světa	svět	k1gInSc2
dala	dát	k5eAaPmAgFnS
shrnout	shrnout	k5eAaPmF
pod	pod	k7c4
pár	pár	k4xCyI
základních	základní	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
nebo	nebo	k8xC
vtěsnat	vtěsnat	k5eAaPmF
do	do	k7c2
pár	pár	k4xCyI
experimentů	experiment	k1gInPc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Redukcionismus	redukcionismus	k1gInSc1
<g/>
“	“	k?
evoluční	evoluční	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
je	být	k5eAaImIp3nS
ryze	ryze	k6eAd1
taktický	taktický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děláme	dělat	k5eAaImIp1nP
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
umíme	umět	k5eAaImIp1nP
<g/>
,	,	kIx,
tváří	tvářet	k5eAaImIp3nP
v	v	k7c4
tvář	tvář	k1gFnSc4
děsivé	děsivý	k2eAgFnSc2d1
rozmanitosti	rozmanitost	k1gFnSc2
a	a	k8xC
složitosti	složitost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Reductionism	Reductionisma	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
Descartes	Descartes	k1gMnSc1
<g/>
,	,	kIx,
Rozprava	rozprava	k1gFnSc1
o	o	k7c6
metodě	metoda	k1gFnSc6
II	II	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
Descartes	Descartes	k1gMnSc1
<g/>
,	,	kIx,
Regulae	Regulae	k1gFnSc1
ad	ad	k7c4
directionem	direction	k1gInSc7
ingenii	ingenium	k1gNnPc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Descartes	Descartes	k1gMnSc1
<g/>
,	,	kIx,
Rozprava	rozprava	k1gFnSc1
o	o	k7c6
metodě	metoda	k1gFnSc6
V.	V.	kA
↑	↑	k?
P.	P.	kA
J.	J.	kA
Richerson	Richerson	k1gMnSc1
–	–	k?
R.	R.	kA
Boyd	Boyd	k1gInSc1
<g/>
,	,	kIx,
Not	nota	k1gFnPc2
by	by	k9
genes	genes	k1gMnSc1
alone	alonout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
98	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
R.	R.	kA
Dawkins	Dawkins	k1gInSc1
<g/>
,	,	kIx,
Slepý	slepý	k2eAgMnSc1d1
hodinář	hodinář	k1gMnSc1
<g/>
:	:	kIx,
zázrak	zázrak	k1gInSc1
života	život	k1gInSc2
očima	oko	k1gNnPc7
evoluční	evoluční	k2eAgFnSc1d1
biologie	biologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
–	–	k?
357	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-7185-445-X	80-7185-445-X	k4
</s>
<s>
R.	R.	kA
Descartes	Descartes	k1gMnSc1
<g/>
,	,	kIx,
Rozprava	rozprava	k1gFnSc1
o	o	k7c6
metodě	metoda	k1gFnSc6
(	(	kIx(
<g/>
1637	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
–	–	k?
67	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-205-0216-5	80-205-0216-5	k4
</s>
<s>
R.	R.	kA
Descartes	Descartes	k1gMnSc1
<g/>
,	,	kIx,
Regulae	Regulae	k1gFnSc1
ad	ad	k7c4
directionem	direction	k1gInSc7
ingenii	ingenium	k1gNnPc7
(	(	kIx(
<g/>
Pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
vedení	vedení	k1gNnSc4
rozumu	rozum	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2000	#num#	k4
–	–	k?
314	#num#	k4
s.	s.	k?
;	;	kIx,
21	#num#	k4
cm	cm	kA
ISBN	ISBN	kA
80-7298-000-9	80-7298-000-9	k4
</s>
<s>
A.	A.	kA
Koyré	Koyrý	k2eAgFnPc1d1
<g/>
,	,	kIx,
Rozhovory	rozhovor	k1gInPc1
nad	nad	k7c7
Descartem	Descart	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
–	–	k?
99	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-7021-835-5	80-7021-835-5	k4
</s>
<s>
P.	P.	kA
J.	J.	kA
Richerson	Richerson	k1gMnSc1
–	–	k?
R.	R.	kA
Boyd	Boyd	k1gInSc1
<g/>
,	,	kIx,
Not	nota	k1gFnPc2
by	by	k9
genes	genes	k1gInSc4
alone	alonout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc4
University	universita	k1gFnSc2
Press	Press	k1gInSc4
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0-226-71212-5	0-226-71212-5	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Atomismus	atomismus	k1gInSc1
</s>
<s>
Holismus	holismus	k1gInSc1
</s>
<s>
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Bertalanffy	Bertalanff	k1gInPc4
</s>
<s>
Positivismus	positivismus	k1gInSc1
</s>
<s>
René	René	k1gMnSc1
Descartes	Descartes	k1gMnSc1
</s>
<s>
Teorie	teorie	k1gFnSc1
systémů	systém	k1gInPc2
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Heslo	heslo	k1gNnSc4
„	„	k?
<g/>
Reductionism	Reductionism	k1gInSc1
in	in	k?
Biology	biolog	k1gMnPc7
<g/>
“	“	k?
ve	v	k7c6
The	The	k1gFnSc6
Stanford	Stanford	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosopha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Dupré	Duprý	k2eAgFnSc2d1
<g/>
:	:	kIx,
The	The	k1gFnSc2
Disunity	Disunita	k1gFnSc2
of	of	k?
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interview	interview	k1gNnSc1
o	o	k7c6
vědách	věda	k1gFnPc6
a	a	k8xC
redukcionismu	redukcionismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4135105-8	4135105-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85112145	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85112145	#num#	k4
</s>
