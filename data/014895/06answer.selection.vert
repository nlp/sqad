<s>
Redukcionismus	redukcionismus	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
reductio	reductio	k1gNnSc1
<g/>
,	,	kIx,
přivedení	přivedení	k1gNnSc1
nazpět	nazpět	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
výkladový	výkladový	k2eAgInSc4d1
postup	postup	k1gInSc4
a	a	k8xC
myšlenkový	myšlenkový	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
vysvětlovat	vysvětlovat	k5eAaImF
složité	složitý	k2eAgFnPc4d1
skutečnosti	skutečnost	k1gFnPc4
převedením	převedení	k1gNnSc7
na	na	k7c4
jednoduché	jednoduchý	k2eAgFnPc4d1
části	část	k1gFnPc4
zejména	zejména	k9
rozkladem	rozklad	k1gInSc7
a	a	k8xC
tvrzením	tvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
celek	celek	k1gInSc1
není	být	k5eNaImIp3nS
„	„	k?
<g/>
nic	nic	k6eAd1
než	než	k8xS
<g/>
“	“	k?
soubor	soubor	k1gInSc1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>