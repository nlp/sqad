<s>
Při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
umlčováním	umlčování	k1gNnSc7	umlčování
genů	gen	k1gInPc2	gen
u	u	k7c2	u
háďátka	háďátko	k1gNnSc2	háďátko
obecného	obecný	k2eAgNnSc2d1	obecné
Caenorhabditis	Caenorhabditis	k1gFnSc1	Caenorhabditis
elegans	elegans	k6eAd1	elegans
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
biologem	biolog	k1gMnSc7	biolog
Andrew	Andrew	k1gFnSc2	Andrew
Firem	firma	k1gFnPc2	firma
fenomén	fenomén	k1gInSc1	fenomén
RNA	RNA	kA	RNA
interference	interference	k1gFnSc2	interference
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
oba	dva	k4xCgMnPc1	dva
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
