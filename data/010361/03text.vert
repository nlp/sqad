<p>
<s>
Craig	Craig	k1gInSc4	Craig
Cameron	Cameron	k1gInSc1	Cameron
Mello	Mello	k1gNnSc1	Mello
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Haven	Haven	k1gInSc1	Haven
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
molekulární	molekulární	k2eAgMnSc1d1	molekulární
biolog	biolog	k1gMnSc1	biolog
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
molekulárního	molekulární	k2eAgNnSc2d1	molekulární
lékařství	lékařství	k1gNnSc2	lékařství
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
Medical	Medical	k1gFnSc2	Medical
School	Schoola	k1gFnPc2	Schoola
ve	v	k7c6	v
Worcesteru	Worcester	k1gInSc6	Worcester
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
jeho	jeho	k3xOp3gFnSc2	jeho
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
činnosti	činnost	k1gFnSc2	činnost
jsou	být	k5eAaImIp3nP	být
experimenty	experiment	k1gInPc1	experiment
s	s	k7c7	s
"	"	kIx"	"
<g/>
vypínáním	vypínání	k1gNnSc7	vypínání
<g/>
"	"	kIx"	"
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
genů	gen	k1gInPc2	gen
u	u	k7c2	u
hlístic	hlístice	k1gFnPc2	hlístice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
umlčováním	umlčování	k1gNnSc7	umlčování
genů	gen	k1gInPc2	gen
u	u	k7c2	u
háďátka	háďátko	k1gNnSc2	háďátko
obecného	obecný	k2eAgNnSc2d1	obecné
Caenorhabditis	Caenorhabditis	k1gFnSc1	Caenorhabditis
elegans	elegans	k6eAd1	elegans
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
biologem	biolog	k1gMnSc7	biolog
Andrew	Andrew	k1gFnSc2	Andrew
Firem	firma	k1gFnPc2	firma
fenomén	fenomén	k1gInSc1	fenomén
RNA	RNA	kA	RNA
interference	interference	k1gFnSc2	interference
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
oba	dva	k4xCgMnPc1	dva
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Craig	Craiga	k1gFnPc2	Craiga
C.	C.	kA	C.
Mello	Mello	k1gNnSc4	Mello
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
