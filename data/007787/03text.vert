<s>
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
jako	jako	k8xC	jako
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
umělecké	umělecký	k2eAgFnPc1d1	umělecká
formy	forma	k1gFnPc1	forma
začínají	začínat	k5eAaImIp3nP	začínat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ohlasem	ohlas	k1gInSc7	ohlas
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
předveden	předvést	k5eAaPmNgInS	předvést
kinematograf	kinematograf	k1gInSc1	kinematograf
bratří	bratr	k1gMnPc2	bratr
Lumiè	Lumiè	k1gFnSc2	Lumiè
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xS	jako
technický	technický	k2eAgInSc4d1	technický
zázrak	zázrak	k1gInSc4	zázrak
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgInSc4d3	nejmladší
<g/>
,	,	kIx,	,
syntetický	syntetický	k2eAgInSc4d1	syntetický
a	a	k8xC	a
audiovizuální	audiovizuální	k2eAgInSc4d1	audiovizuální
druh	druh	k1gInSc4	druh
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
hlavním	hlavní	k2eAgInSc7d1	hlavní
atributem	atribut	k1gInSc7	atribut
je	být	k5eAaImIp3nS	být
ikoničnost	ikoničnost	k1gFnSc1	ikoničnost
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezcem	vynálezce	k1gMnSc7	vynálezce
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
T.	T.	kA	T.
A.	A.	kA	A.
Edison	Edisona	k1gFnPc2	Edisona
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prapočátky	prapočátek	k1gInPc1	prapočátek
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
najít	najít	k5eAaPmF	najít
už	už	k6eAd1	už
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
oživené	oživený	k2eAgFnSc2d1	oživená
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc4	dva
základní	základní	k2eAgNnPc4d1	základní
paradigmata	paradigma	k1gNnPc4	paradigma
<g/>
:	:	kIx,	:
pasivní	pasivní	k2eAgNnSc4d1	pasivní
zachycování	zachycování	k1gNnSc4	zachycování
reality	realita	k1gFnSc2	realita
(	(	kIx(	(
<g/>
dokumentaristická	dokumentaristický	k2eAgFnSc1d1	dokumentaristická
linie	linie	k1gFnSc1	linie
–	–	k?	–
Lumiére	Lumiér	k1gMnSc5	Lumiér
<g/>
)	)	kIx)	)
a	a	k8xC	a
sci-fi	scii	k1gFnSc1	sci-fi
<g/>
,	,	kIx,	,
triková	trikový	k2eAgFnSc1d1	triková
linie	linie	k1gFnSc1	linie
(	(	kIx(	(
<g/>
Mélies	Mélies	k1gInSc1	Mélies
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Lumiérové	Lumiérový	k2eAgFnSc2d1	Lumiérový
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
dokumentárními	dokumentární	k2eAgInPc7d1	dokumentární
a	a	k8xC	a
cestopisnými	cestopisný	k2eAgInPc7d1	cestopisný
filmy	film	k1gInPc7	film
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	on	k3xPp3gNnPc4	on
chtěli	chtít	k5eAaImAgMnP	chtít
přivézt	přivézt	k5eAaPmF	přivézt
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
krátký	krátký	k2eAgInSc4d1	krátký
dějový	dějový	k2eAgInSc4d1	dějový
film	film	k1gInSc4	film
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
jejich	jejich	k3xOp3gInSc4	jejich
Pokropený	pokropený	k2eAgMnSc1d1	pokropený
kropič	kropič	k1gMnSc1	kropič
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgMnPc4	první
představitele	představitel	k1gMnPc4	představitel
filmu	film	k1gInSc2	film
patří	patřit	k5eAaImIp3nS	patřit
Georges	Georges	k1gMnSc1	Georges
Méliè	Méliè	k1gMnSc1	Méliè
(	(	kIx(	(
<g/>
vynález	vynález	k1gInSc1	vynález
filmového	filmový	k2eAgInSc2d1	filmový
triku	trik	k1gInSc2	trik
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
filmové	filmový	k2eAgFnPc1d1	filmová
spoločnosti	spoločnost	k1gFnPc1	spoločnost
(	(	kIx(	(
<g/>
Pathé	Pathý	k2eAgFnPc1d1	Pathý
a	a	k8xC	a
Gaumont	Gaumont	k1gInSc1	Gaumont
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
Společnosť	Společnosť	k1gFnSc2	Společnosť
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Societé	Societý	k2eAgFnSc2d1	Societá
film	film	k1gInSc4	film
d	d	k?	d
<g/>
́	́	k?	́
<g/>
art	art	k?	art
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
Charles	Charles	k1gMnSc1	Charles
Pathé	Pathý	k2eAgFnSc2d1	Pathý
zahájil	zahájit	k5eAaPmAgMnS	zahájit
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
expanzi	expanze	k1gFnSc4	expanze
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Prapočátky	prapočátek	k1gInPc1	prapočátek
filmu	film	k1gInSc2	film
tvoří	tvořit	k5eAaImIp3nP	tvořit
němý	němý	k2eAgInSc4d1	němý
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Promítal	promítat	k5eAaImAgMnS	promítat
se	se	k3xPyFc4	se
s	s	k7c7	s
hudebním	hudební	k2eAgInSc7d1	hudební
doprovodem	doprovod	k1gInSc7	doprovod
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
gramofonu	gramofon	k1gInSc2	gramofon
<g/>
,	,	kIx,	,
klavíru	klavír	k1gInSc2	klavír
apod.	apod.	kA	apod.
Optické	optický	k2eAgFnPc4d1	optická
hračky	hračka	k1gFnPc4	hračka
<g/>
,	,	kIx,	,
hrátky	hrátky	k1gFnPc4	hrátky
se	s	k7c7	s
stínem	stín	k1gInSc7	stín
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
magické	magický	k2eAgFnSc2d1	magická
lucerny	lucerna	k1gFnSc2	lucerna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
vizuální	vizuální	k2eAgInPc1d1	vizuální
triky	trik	k1gInPc1	trik
existují	existovat	k5eAaImIp3nP	existovat
již	již	k9	již
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vynálezců	vynálezce	k1gMnPc2	vynálezce
<g/>
,	,	kIx,	,
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
výrobců	výrobce	k1gMnPc2	výrobce
pozorovalo	pozorovat	k5eAaImAgNnS	pozorovat
vizuální	vizuální	k2eAgInSc4d1	vizuální
fenomén	fenomén	k1gInSc4	fenomén
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
individuálních	individuální	k2eAgInPc2d1	individuální
statických	statický	k2eAgInPc2d1	statický
snímků	snímek	k1gInPc2	snímek
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
–	–	k?	–
koncept	koncept	k1gInSc1	koncept
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
jako	jako	k8xC	jako
Persistence	persistence	k1gFnSc1	persistence
of	of	k?	of
Vision	vision	k1gInSc1	vision
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
iluzi	iluze	k1gFnSc4	iluze
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
britský	britský	k2eAgMnSc1d1	britský
lékař	lékař	k1gMnSc1	lékař
Peter	Peter	k1gMnSc1	Peter
Roget	Roget	k1gMnSc1	Roget
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
technologií	technologie	k1gFnPc2	technologie
–	–	k?	–
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
optické	optický	k2eAgFnPc4d1	optická
a	a	k8xC	a
mechanické	mechanický	k2eAgFnPc4d1	mechanická
hračky	hračka	k1gFnPc4	hračka
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
vize	vize	k1gFnSc1	vize
–	–	k?	–
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
předcházela	předcházet	k5eAaImAgFnS	předcházet
tak	tak	k6eAd1	tak
zrození	zrození	k1gNnSc4	zrození
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
:	:	kIx,	:
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
vynalezen	vynalezen	k2eAgInSc4d1	vynalezen
Athanasiusem	Athanasius	k1gMnSc7	Athanasius
Kircherem	Kircher	k1gMnSc7	Kircher
první	první	k4xOgFnSc7	první
primitivní	primitivní	k2eAgFnSc1d1	primitivní
"	"	kIx"	"
<g/>
magická	magický	k2eAgFnSc1d1	magická
lucerna	lucerna	k1gFnSc1	lucerna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
přístroj	přístroj	k1gInSc1	přístroj
s	s	k7c7	s
objektivem	objektiv	k1gInSc7	objektiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
promítal	promítat	k5eAaImAgInS	promítat
obraz	obraz	k1gInSc4	obraz
z	z	k7c2	z
fólie	fólie	k1gFnSc2	fólie
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
s	s	k7c7	s
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
zdrojem	zdroj	k1gInSc7	zdroj
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
svíčkou	svíčka	k1gFnSc7	svíčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1824	[number]	k4	1824
–	–	k?	–
vynález	vynález	k1gInSc1	vynález
Thaumatropu	Thaumatrop	k1gInSc2	Thaumatrop
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
optického	optický	k2eAgInSc2d1	optický
klamu	klam	k1gInSc2	klam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
využíval	využívat	k5eAaPmAgInS	využívat
konceptu	koncept	k1gInSc3	koncept
Persistence	persistence	k1gFnSc2	persistence
of	of	k?	of
Vision	vision	k1gInSc1	vision
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
ve	v	k7c6	v
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
<g />
.	.	kIx.	.
</s>
<s>
článku	článek	k1gInSc3	článek
Petera	Peter	k1gMnSc2	Peter
Rogeta	Roget	k1gMnSc2	Roget
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
anglický	anglický	k2eAgMnSc1d1	anglický
lékař	lékař	k1gMnSc1	lékař
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
John	John	k1gMnSc1	John
Ayrton	Ayrton	k1gInSc4	Ayrton
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
1831	[number]	k4	1831
–	–	k?	–
objev	objev	k1gInSc1	objev
zákonu	zákon	k1gInSc3	zákon
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
indukce	indukce	k1gFnSc2	indukce
anglickým	anglický	k2eAgMnSc7d1	anglický
vědcem	vědec	k1gMnSc7	vědec
Michaelem	Michael	k1gMnSc7	Michael
Faradayem	Faraday	k1gInSc7	Faraday
<g/>
,	,	kIx,	,
princip	princip	k1gInSc1	princip
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
pohánění	pohánění	k1gNnSc2	pohánění
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
filmových	filmový	k2eAgFnPc2d1	filmová
technik	technika	k1gFnPc2	technika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
1832	[number]	k4	1832
–	–	k?	–
vynález	vynález	k1gInSc1	vynález
Fantaskopu	Fantaskop	k1gInSc2	Fantaskop
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Phenakistiscop	Phenakistiscop	k1gInSc1	Phenakistiscop
<g/>
)	)	kIx)	)
vytvořeným	vytvořený	k2eAgMnSc7d1	vytvořený
belgickým	belgický	k2eAgMnSc7d1	belgický
vynálezcem	vynálezce	k1gMnSc7	vynálezce
Josephem	Joseph	k1gInSc7	Joseph
Plateau	Plateaus	k1gInSc2	Plateaus
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
simulovalo	simulovat	k5eAaImAgNnS	simulovat
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
nebo	nebo	k8xC	nebo
sekvence	sekvence	k1gFnSc1	sekvence
samostatných	samostatný	k2eAgInPc2d1	samostatný
obrázků	obrázek	k1gInPc2	obrázek
zobrazující	zobrazující	k2eAgFnSc2d1	zobrazující
různé	různý	k2eAgFnSc2d1	různá
fáze	fáze	k1gFnSc2	fáze
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
žonglování	žonglování	k1gNnSc4	žonglování
nebo	nebo	k8xC	nebo
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
nebo	nebo	k8xC	nebo
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
drážkovaného	drážkovaný	k2eAgInSc2d1	drážkovaný
kotouče	kotouč	k1gInSc2	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
kotouč	kotouč	k1gInSc1	kotouč
umístěn	umístit	k5eAaPmNgInS	umístit
před	před	k7c4	před
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
a	a	k8xC	a
točil	točit	k5eAaImAgMnS	točit
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
divák	divák	k1gMnSc1	divák
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
skrz	skrz	k7c4	skrz
otvory	otvor	k1gInPc4	otvor
vidět	vidět	k5eAaImF	vidět
"	"	kIx"	"
<g/>
pohybující	pohybující	k2eAgMnSc1d1	pohybující
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
1834	[number]	k4	1834
–	–	k?	–
vynález	vynález	k1gInSc1	vynález
a	a	k8xC	a
patent	patent	k1gInSc1	patent
dalšího	další	k2eAgInSc2d1	další
stroboskopického	stroboskopický	k2eAgInSc2d1	stroboskopický
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
Daedalum	Daedalum	k1gInSc1	Daedalum
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
Američanem	Američan	k1gMnSc7	Američan
Williamem	William	k1gInSc7	William
Lincolnem	Lincoln	k1gMnSc7	Lincoln
přejmenovaným	přejmenovaný	k2eAgFnPc3d1	přejmenovaná
jako	jako	k8xC	jako
Zoetrope	Zoetrop	k1gMnSc5	Zoetrop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britským	britský	k2eAgMnSc7d1	britský
vynálezcem	vynálezce	k1gMnSc7	vynálezce
Williamem	William	k1gInSc7	William
George	George	k1gNnSc7	George
Hornerem	Horner	k1gMnSc7	Horner
<g/>
.	.	kIx.	.
</s>
<s>
Představoval	představovat	k5eAaImAgInS	představovat
dutý	dutý	k2eAgInSc1d1	dutý
rotační	rotační	k2eAgInSc1d1	rotační
válec	válec	k1gInSc1	válec
(	(	kIx(	(
<g/>
či	či	k8xC	či
válec	válec	k1gInSc1	válec
s	s	k7c7	s
klikou	klika	k1gFnSc7	klika
<g/>
)	)	kIx)	)
se	s	k7c7	s
sekvenčními	sekvenční	k2eAgFnPc7d1	sekvenční
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
,	,	kIx,	,
kresbami	kresba	k1gFnPc7	kresba
nebo	nebo	k8xC	nebo
ilustracemi	ilustrace	k1gFnPc7	ilustrace
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
pravidelně	pravidelně	k6eAd1	pravidelně
rozmístěnými	rozmístěný	k2eAgMnPc7d1	rozmístěný
do	do	k7c2	do
úzkých	úzký	k2eAgNnPc2d1	úzké
drážek	drážka	k1gNnPc2	drážka
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
divák	divák	k1gMnSc1	divák
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
"	"	kIx"	"
<g/>
pohybující	pohybující	k2eAgMnSc1d1	pohybující
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
kresby	kresba	k1gFnPc1	kresba
<g/>
.	.	kIx.	.
1839	[number]	k4	1839
–	–	k?	–
zrození	zrození	k1gNnSc4	zrození
fotografie	fotografia	k1gFnSc2	fotografia
s	s	k7c7	s
komerčně	komerčně	k6eAd1	komerčně
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
daguerrotypie	daguerrotypie	k1gFnSc2	daguerrotypie
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
způsob	způsob	k1gInSc1	způsob
fotografování	fotografování	k1gNnSc2	fotografování
na	na	k7c4	na
stříbrné	stříbrný	k2eAgInPc4d1	stříbrný
či	či	k8xC	či
měděné	měděný	k2eAgInPc4d1	měděný
plechy	plech	k1gInPc4	plech
<g/>
)	)	kIx)	)
zásluhou	zásluhou	k7c2	zásluhou
francouzského	francouzský	k2eAgMnSc2d1	francouzský
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc2	vynálezce
Jacques-Louis	Jacques-Louis	k1gFnSc2	Jacques-Louis
Daguerre	Daguerr	k1gMnSc5	Daguerr
Mandeeho	Mandeeha	k1gMnSc5	Mandeeha
<g/>
.	.	kIx.	.
</s>
<s>
1841	[number]	k4	1841
–	–	k?	–
patentování	patentování	k1gNnSc4	patentování
kalotypie	kalotypie	k1gFnSc2	kalotypie
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
talbotypie	talbotypie	k1gFnSc1	talbotypie
<g/>
,	,	kIx,	,
proces	proces	k1gInSc1	proces
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
fotografií	fotografia	k1gFnPc2	fotografia
na	na	k7c4	na
negativní	negativní	k2eAgInPc4d1	negativní
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
papír	papír	k1gInSc4	papír
<g/>
)	)	kIx)	)
britským	britský	k2eAgMnSc7d1	britský
vynálezcem	vynálezce	k1gMnSc7	vynálezce
Williamem	William	k1gInSc7	William
Henry	henry	k1gInSc2	henry
Fox	fox	k1gInSc1	fox
Talbotem	Talbot	k1gInSc7	Talbot
<g/>
.	.	kIx.	.
</s>
<s>
1861	[number]	k4	1861
–	–	k?	–
vynález	vynález	k1gInSc1	vynález
Kinematoskopu	Kinematoskop	k1gInSc2	Kinematoskop
<g/>
,	,	kIx,	,
patentovaným	patentovaný	k2eAgMnSc7d1	patentovaný
Američanem	Američan	k1gMnSc7	Američan
Sellersem	Sellers	k1gMnSc7	Sellers
Colemanem	Coleman	k1gMnSc7	Coleman
<g/>
,	,	kIx,	,
zlepšující	zlepšující	k2eAgInPc1d1	zlepšující
rotující	rotující	k2eAgInPc1d1	rotující
lopatkové	lopatkový	k2eAgInPc1d1	lopatkový
stroje	stroj	k1gInPc1	stroj
zobrazující	zobrazující	k2eAgInPc1d1	zobrazující
(	(	kIx(	(
<g/>
ručně	ručně	k6eAd1	ručně
zalomenou	zalomený	k2eAgFnSc4d1	zalomená
<g/>
)	)	kIx)	)
sérii	série	k1gFnSc4	série
stereoskopických	stereoskopický	k2eAgInPc2d1	stereoskopický
snímků	snímek	k1gInPc2	snímek
na	na	k7c4	na
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
byl	být	k5eAaImAgInS	být
namontovaný	namontovaný	k2eAgInSc1d1	namontovaný
na	na	k7c4	na
do	do	k7c2	do
bedny	bedna	k1gFnSc2	bedna
<g/>
.	.	kIx.	.
</s>
<s>
1869	[number]	k4	1869
–	–	k?	–
rozvoj	rozvoj	k1gInSc1	rozvoj
celuloidu	celuloid	k1gInSc2	celuloid
Johnem	John	k1gMnSc7	John
Wesley	Weslea	k1gFnSc2	Weslea
Hyattem	Hyatt	k1gMnSc7	Hyatt
<g/>
,	,	kIx,	,
patentovaném	patentovaný	k2eAgInSc6d1	patentovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
a	a	k8xC	a
ochranou	ochrana	k1gFnSc7	ochrana
známkou	známka	k1gFnSc7	známka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
–	–	k?	–
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
jako	jako	k8xC	jako
základ	základ	k1gInSc1	základ
fotografického	fotografický	k2eAgInSc2d1	fotografický
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
1870	[number]	k4	1870
–	–	k?	–
První	první	k4xOgFnSc1	první
demonstrace	demonstrace	k1gFnSc1	demonstrace
Phasmotropu	Phasmotrop	k1gInSc2	Phasmotrop
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Phasmatropu	Phasmatropa	k1gFnSc4	Phasmatropa
<g/>
)	)	kIx)	)
Henrym	Henry	k1gMnSc6	Henry
Renno	Renno	k1gNnSc1	Renno
Heylem	Heyl	k1gInSc7	Heyl
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
představil	představit	k5eAaPmAgInS	představit
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
statické	statický	k2eAgFnSc2d1	statická
fotografie	fotografia	k1gFnSc2	fotografia
nebo	nebo	k8xC	nebo
fotografie	fotografia	k1gFnSc2	fotografia
tanečníků	tanečník	k1gMnPc2	tanečník
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tak	tak	k6eAd1	tak
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
1877	[number]	k4	1877
–	–	k?	–
vynález	vynález	k1gInSc1	vynález
praxinoskopu	praxinoskop	k1gInSc2	praxinoskop
francouzským	francouzský	k2eAgMnSc7d1	francouzský
vynálezcem	vynálezce	k1gMnSc7	vynálezce
Charlesem	Charles	k1gMnSc7	Charles
Emile	Emil	k1gMnSc5	Emil
Reynaudem	Reynaud	k1gInSc7	Reynaud
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
projektor	projektor	k1gInSc1	projektor
<g/>
"	"	kIx"	"
se	s	k7c7	s
zrcadlícím	zrcadlící	k2eAgMnSc7d1	zrcadlící
se	se	k3xPyFc4	se
bubnem	buben	k1gInSc7	buben
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
podobným	podobný	k2eAgNnSc7d1	podobné
zařízením	zařízení	k1gNnSc7	zařízení
jako	jako	k8xC	jako
Zoetrope	Zoetrop	k1gInSc5	Zoetrop
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1	veřejná
demonstrace	demonstrace	k1gFnPc1	demonstrace
praxinoscopu	praxinoscop	k1gInSc2	praxinoscop
byly	být	k5eAaImAgFnP	být
promítány	promítat	k5eAaImNgFnP	promítat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
s	s	k7c7	s
projekcí	projekce	k1gFnSc7	projekce
"	"	kIx"	"
<g/>
filmů	film	k1gInPc2	film
<g/>
"	"	kIx"	"
trvajících	trvající	k2eAgFnPc2d1	trvající
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
pařížském	pařížský	k2eAgNnSc6d1	pařížské
Theatre	Theatr	k1gInSc5	Theatr
Optique	Optique	k1gNnPc1	Optique
<g/>
.	.	kIx.	.
1879	[number]	k4	1879
–	–	k?	–
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
první	první	k4xOgFnSc4	první
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
výstavu	výstava	k1gFnSc4	výstava
účinné	účinný	k2eAgFnSc2d1	účinná
žárovky	žárovka	k1gFnSc2	žárovka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
využitou	využitý	k2eAgFnSc4d1	využitá
ve	v	k7c6	v
filmových	filmový	k2eAgInPc6d1	filmový
projektorech	projektor	k1gInPc6	projektor
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
nejsrozumitelnější	srozumitelný	k2eAgInSc1d3	nejsrozumitelnější
způsob	způsob	k1gInSc1	způsob
sdílení	sdílení	k1gNnSc2	sdílení
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
zážitků	zážitek	k1gInPc2	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nejranějších	raný	k2eAgInPc2d3	nejranější
časů	čas	k1gInPc2	čas
lidé	člověk	k1gMnPc1	člověk
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
zobrazení	zobrazení	k1gNnSc4	zobrazení
nejen	nejen	k6eAd1	nejen
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
světa	svět	k1gInSc2	svět
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jeskynní	jeskynní	k2eAgFnPc1d1	jeskynní
malby	malba	k1gFnPc1	malba
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
ohromují	ohromovat	k5eAaImIp3nP	ohromovat
přesností	přesnost	k1gFnSc7	přesnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
jsou	být	k5eAaImIp3nP	být
zachycena	zachycen	k2eAgNnPc4d1	zachyceno
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
tanečníci	tanečník	k1gMnPc1	tanečník
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Obrazové	obrazový	k2eAgInPc4d1	obrazový
příběhy	příběh	k1gInPc4	příběh
na	na	k7c6	na
vlysech	vlys	k1gInPc6	vlys
egyptských	egyptský	k2eAgInPc2d1	egyptský
hrobů	hrob	k1gInPc2	hrob
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
zřetelně	zřetelně	k6eAd1	zřetelně
opakování	opakování	k1gNnSc4	opakování
stále	stále	k6eAd1	stále
stejných	stejný	k2eAgFnPc2d1	stejná
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
při	při	k7c6	při
pečlivém	pečlivý	k2eAgInSc6d1	pečlivý
pohledu	pohled	k1gInSc6	pohled
vyklubou	vyklubat	k5eAaPmIp3nP	vyklubat
studie	studie	k1gFnPc1	studie
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
fází	fáze	k1gFnPc2	fáze
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
za	za	k7c7	za
sebou	se	k3xPyFc7	se
řazené	řazený	k2eAgInPc1d1	řazený
reliéfy	reliéf	k1gInPc1	reliéf
běžícího	běžící	k2eAgMnSc2d1	běžící
válečníka	válečník	k1gMnSc2	válečník
<g/>
,	,	kIx,	,
zdvihajícího	zdvihající	k2eAgMnSc2d1	zdvihající
svůj	svůj	k3xOyFgInSc4	svůj
oštěp	oštěp	k1gInSc4	oštěp
stále	stále	k6eAd1	stále
výše	vysoce	k6eAd2	vysoce
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
úžasnou	úžasný	k2eAgFnSc4d1	úžasná
podobnost	podobnost	k1gFnSc4	podobnost
se	se	k3xPyFc4	se
za	za	k7c7	za
sebou	se	k3xPyFc7	se
jdoucími	jdoucí	k2eAgInPc7d1	jdoucí
políčky	políček	k1gInPc7	políček
filmového	filmový	k2eAgInSc2d1	filmový
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
pohyblivé	pohyblivý	k2eAgInPc1d1	pohyblivý
obrázky	obrázek	k1gInPc1	obrázek
promítají	promítat	k5eAaImIp3nP	promítat
již	již	k6eAd1	již
nejméně	málo	k6eAd3	málo
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
doklady	doklad	k1gInPc4	doklad
stínohry	stínohra	k1gFnSc2	stínohra
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
"	"	kIx"	"
<g/>
vykouzleni	vykouzlen	k2eAgMnPc1d1	vykouzlen
<g/>
"	"	kIx"	"
bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
bájná	bájný	k2eAgNnPc1d1	bájné
zvířata	zvíře	k1gNnPc1	zvíře
i	i	k8xC	i
démoni	démon	k1gMnPc1	démon
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Dvourozměrné	dvourozměrný	k2eAgFnPc1d1	dvourozměrná
filigránové	filigránový	k2eAgFnPc1d1	filigránová
figurky	figurka	k1gFnPc1	figurka
z	z	k7c2	z
vydělané	vydělaný	k2eAgFnSc2d1	vydělaná
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
kůže	kůže	k1gFnSc2	kůže
byly	být	k5eAaImAgFnP	být
umělecky	umělecky	k6eAd1	umělecky
vybarveny	vybarven	k2eAgFnPc1d1	vybarvena
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
tak	tak	k6eAd1	tak
tenké	tenký	k2eAgInPc1d1	tenký
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
prozářit	prozářit	k5eAaPmF	prozářit
světlem	světlo	k1gNnSc7	světlo
olejové	olejový	k2eAgFnSc2d1	olejová
lampy	lampa	k1gFnSc2	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
barevné	barevný	k2eAgInPc1d1	barevný
stíny	stín	k1gInPc1	stín
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
po	po	k7c6	po
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
je	být	k5eAaImIp3nS	být
stínohra	stínohra	k1gFnSc1	stínohra
wayang	wayang	k1gInSc4	wayang
kulit	kulit	k5eAaImF	kulit
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
dřívější	dřívější	k2eAgInPc4d1	dřívější
pokusy	pokus	k1gInPc4	pokus
rozpohybovat	rozpohybovat	k5eAaPmF	rozpohybovat
a	a	k8xC	a
promítat	promítat	k5eAaImF	promítat
obrázky	obrázek	k1gInPc4	obrázek
sloužily	sloužit	k5eAaImAgInP	sloužit
k	k	k7c3	k
předstírání	předstírání	k1gNnSc3	předstírání
působení	působení	k1gNnSc2	působení
nadpřirozených	nadpřirozený	k2eAgFnPc2d1	nadpřirozená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
senzacechtivé	senzacechtivý	k2eAgNnSc1d1	senzacechtivé
publikum	publikum	k1gNnSc1	publikum
v	v	k7c4	v
hrůzu	hrůza	k1gFnSc4	hrůza
a	a	k8xC	a
strach	strach	k1gInSc4	strach
pomocí	pomocí	k7c2	pomocí
podivuhodné	podivuhodný	k2eAgFnSc2d1	podivuhodná
trikové	trikový	k2eAgFnSc2d1	triková
bedny	bedna	k1gFnSc2	bedna
potulný	potulný	k2eAgMnSc1d1	potulný
Ital	Ital	k1gMnSc1	Ital
Giovanni	Giovann	k1gMnPc1	Giovann
Battista	Battista	k1gMnSc1	Battista
della	della	k1gMnSc1	della
Porta	porta	k1gFnSc1	porta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zatemněné	zatemněný	k2eAgFnSc6d1	zatemněná
místnosti	místnost	k1gFnSc6	místnost
"	"	kIx"	"
<g/>
vyčaroval	vyčarovat	k5eAaPmAgInS	vyčarovat
<g/>
"	"	kIx"	"
užaslému	užaslý	k2eAgNnSc3d1	užaslé
publiku	publikum	k1gNnSc3	publikum
čerta	čert	k1gMnSc2	čert
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
Porta	porta	k1gFnSc1	porta
použil	použít	k5eAaPmAgInS	použít
princip	princip	k1gInSc1	princip
"	"	kIx"	"
<g/>
temné	temný	k2eAgFnPc1d1	temná
komory	komora	k1gFnPc1	komora
<g/>
"	"	kIx"	"
–	–	k?	–
camery	camer	k1gMnPc4	camer
obscury	obscura	k1gFnSc2	obscura
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgInSc1d1	popsaný
poprvé	poprvé	k6eAd1	poprvé
již	již	k6eAd1	již
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Odhalil	odhalit	k5eAaPmAgMnS	odhalit
jej	on	k3xPp3gNnSc4	on
arabský	arabský	k2eAgMnSc1d1	arabský
učenec	učenec	k1gMnSc1	učenec
Abu	Abu	k1gMnSc1	Abu
Ali	Ali	k1gMnSc1	Ali
al-Hasan	al-Hasan	k1gMnSc1	al-Hasan
<g/>
,	,	kIx,	,
když	když	k8xS	když
hledal	hledat	k5eAaImAgMnS	hledat
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
zatmění	zatmění	k1gNnSc4	zatmění
Slunce	slunce	k1gNnSc2	slunce
bez	bez	k7c2	bez
poškození	poškození	k1gNnSc2	poškození
zraku	zrak	k1gInSc2	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Využil	využít	k5eAaPmAgMnS	využít
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
padá	padat	k5eAaImIp3nS	padat
malým	malý	k2eAgInSc7d1	malý
prostorem	prostor	k1gInSc7	prostor
do	do	k7c2	do
jinak	jinak	k6eAd1	jinak
naprosto	naprosto	k6eAd1	naprosto
temného	temný	k2eAgInSc2d1	temný
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
promítá	promítat	k5eAaImIp3nS	promítat
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
obraz	obraz	k1gInSc4	obraz
předmětu	předmět	k1gInSc2	předmět
a	a	k8xC	a
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
před	před	k7c7	před
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Porta	porto	k1gNnSc2	porto
umístil	umístit	k5eAaPmAgMnS	umístit
své	svůj	k3xOyFgNnSc4	svůj
publikum	publikum	k1gNnSc4	publikum
do	do	k7c2	do
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
zády	záda	k1gNnPc7	záda
k	k	k7c3	k
otvoru	otvor	k1gInSc3	otvor
ve	v	k7c6	v
zdi	zeď	k1gFnSc6	zeď
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vně	vně	k7c2	vně
pokoje	pokoj	k1gInSc2	pokoj
se	se	k3xPyFc4	se
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
pomocník	pomocník	k1gMnSc1	pomocník
v	v	k7c6	v
kostýmu	kostým	k1gInSc6	kostým
čerta	čert	k1gMnSc2	čert
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
camera	camera	k1gFnSc1	camera
obscura	obscura	k1gFnSc1	obscura
promítá	promítat	k5eAaImIp3nS	promítat
obráceně	obráceně	k6eAd1	obráceně
a	a	k8xC	a
čert	čert	k1gMnSc1	čert
zdánlivě	zdánlivě	k6eAd1	zdánlivě
stál	stát	k5eAaImAgMnS	stát
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
,	,	kIx,	,
neminul	minout	k5eNaImAgMnS	minout
se	se	k3xPyFc4	se
účinkem	účinek	k1gInSc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
hračka	hračka	k1gFnSc1	hračka
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pouťoví	pouťový	k2eAgMnPc1d1	pouťový
umělci	umělec	k1gMnPc1	umělec
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
princip	princip	k1gInSc4	princip
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
účely	účel	k1gInPc4	účel
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jej	on	k3xPp3gMnSc4	on
vyvíjeli	vyvíjet	k5eAaImAgMnP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
čočka	čočka	k1gFnSc1	čočka
zasazená	zasazený	k2eAgFnSc1d1	zasazená
do	do	k7c2	do
otvoru	otvor	k1gInSc2	otvor
pro	pro	k7c4	pro
světlo	světlo	k1gNnSc4	světlo
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
promítaný	promítaný	k2eAgInSc1d1	promítaný
obraz	obraz	k1gInSc1	obraz
a	a	k8xC	a
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
převrácen	převrátit	k5eAaPmNgInS	převrátit
do	do	k7c2	do
správné	správný	k2eAgFnSc2d1	správná
pozice	pozice	k1gFnSc2	pozice
pomocí	pomocí	k7c2	pomocí
našikmo	našikmo	k6eAd1	našikmo
postaveného	postavený	k2eAgNnSc2d1	postavené
nebo	nebo	k8xC	nebo
dutého	dutý	k2eAgNnSc2d1	duté
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
používali	používat	k5eAaImAgMnP	používat
malíři	malíř	k1gMnPc1	malíř
a	a	k8xC	a
kreslíři	kreslíř	k1gMnPc1	kreslíř
podstatně	podstatně	k6eAd1	podstatně
zmenšenou	zmenšený	k2eAgFnSc4d1	zmenšená
přenosnou	přenosný	k2eAgFnSc4d1	přenosná
cameru	camera	k1gFnSc4	camera
obscuru	obscur	k1gInSc2	obscur
k	k	k7c3	k
vyhotovení	vyhotovení	k1gNnSc3	vyhotovení
co	co	k9	co
možná	možná	k9	možná
nejvěrnějších	věrný	k2eAgInPc2d3	nejvěrnější
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Promítali	promítat	k5eAaImAgMnP	promítat
zobrazení	zobrazení	k1gNnSc3	zobrazení
předmětů	předmět	k1gInPc2	předmět
nebo	nebo	k8xC	nebo
krajin	krajina	k1gFnPc2	krajina
na	na	k7c4	na
kreslící	kreslící	k2eAgInSc4d1	kreslící
papír	papír	k1gInSc4	papír
nebo	nebo	k8xC	nebo
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
a	a	k8xC	a
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
obkreslili	obkreslit	k5eAaPmAgMnP	obkreslit
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
takové	takový	k3xDgNnSc4	takový
zobrazení	zobrazení	k1gNnSc4	zobrazení
přímo	přímo	k6eAd1	přímo
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
vynalezení	vynalezení	k1gNnSc3	vynalezení
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
kuriozit	kuriozita	k1gFnPc2	kuriozita
a	a	k8xC	a
zázračných	zázračný	k2eAgInPc2d1	zázračný
přístrojů	přístroj	k1gInPc2	přístroj
slavila	slavit	k5eAaImAgFnS	slavit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
předchůdkyně	předchůdkyně	k1gFnSc2	předchůdkyně
moderního	moderní	k2eAgInSc2d1	moderní
projektoru	projektor	k1gInSc2	projektor
<g/>
,	,	kIx,	,
laterna	laterna	k1gFnSc1	laterna
magica	magica	k1gFnSc1	magica
<g/>
.	.	kIx.	.
</s>
<s>
Současník	současník	k1gMnSc1	současník
ji	on	k3xPp3gFnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
malého	malý	k2eAgMnSc4d1	malý
pomocníka	pomocník	k1gMnSc4	pomocník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
bílé	bílý	k2eAgFnSc6d1	bílá
stěně	stěna	k1gFnSc6	stěna
zviditelňoval	zviditelňovat	k5eAaImAgMnS	zviditelňovat
duchy	duch	k1gMnPc4	duch
a	a	k8xC	a
strašlivé	strašlivý	k2eAgInPc4d1	strašlivý
obludy	oblud	k1gInPc4	oblud
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
neznají	neznat	k5eAaImIp3nP	neznat
jeho	jeho	k3xOp3gNnSc4	jeho
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaPmIp3nS	připadat
jako	jako	k9	jako
čáry	čára	k1gFnPc4	čára
a	a	k8xC	a
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tajemství	tajemství	k1gNnSc1	tajemství
kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
lucerny	lucerna	k1gFnSc2	lucerna
ovšem	ovšem	k9	ovšem
odhalil	odhalit	k5eAaPmAgMnS	odhalit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1671	[number]	k4	1671
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
traktátu	traktát	k1gInSc6	traktát
Ars	Ars	k1gFnSc1	Ars
magna	magna	k1gFnSc1	magna
lucis	lucis	k1gFnSc1	lucis
et	et	k?	et
umbrae	umbraat	k5eAaPmIp3nS	umbraat
učenec	učenec	k1gMnSc1	učenec
Athanasius	Athanasius	k1gMnSc1	Athanasius
Kircher	Kirchra	k1gFnPc2	Kirchra
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
na	na	k7c4	na
skleněné	skleněný	k2eAgFnPc4d1	skleněná
destičky	destička	k1gFnPc4	destička
namalované	namalovaný	k2eAgFnPc4d1	namalovaná
obrázky	obrázek	k1gInPc4	obrázek
promítat	promítat	k5eAaImF	promítat
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
člověk	člověk	k1gMnSc1	člověk
uložil	uložit	k5eAaPmAgInS	uložit
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
účinek	účinek	k1gInSc4	účinek
zesilovalo	zesilovat	k5eAaImAgNnS	zesilovat
duté	dutý	k2eAgNnSc1d1	duté
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
do	do	k7c2	do
malé	malý	k2eAgFnSc2d1	malá
krabice	krabice	k1gFnSc2	krabice
<g/>
.	.	kIx.	.
</s>
<s>
Pomalované	pomalovaný	k2eAgFnPc1d1	pomalovaná
skleněné	skleněný	k2eAgFnPc1d1	skleněná
destičky	destička	k1gFnPc1	destička
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
hlavou	hlava	k1gFnSc7	hlava
dolů	dolů	k6eAd1	dolů
vsunovaly	vsunovat	k5eAaImAgInP	vsunovat
mezi	mezi	k7c4	mezi
svíčku	svíčka	k1gFnSc4	svíčka
a	a	k8xC	a
jediný	jediný	k2eAgInSc4d1	jediný
otvor	otvor	k1gInSc4	otvor
v	v	k7c6	v
krabici	krabice	k1gFnSc6	krabice
<g/>
.	.	kIx.	.
</s>
<s>
Spojná	spojný	k2eAgFnSc1d1	spojná
čočka	čočka	k1gFnSc1	čočka
před	před	k7c7	před
otvorem	otvor	k1gInSc7	otvor
spojovala	spojovat	k5eAaImAgFnS	spojovat
světlo	světlo	k1gNnSc1	světlo
procházející	procházející	k2eAgInSc1d1	procházející
skleněným	skleněný	k2eAgInSc7d1	skleněný
obrázkem	obrázek	k1gInSc7	obrázek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
ve	v	k7c6	v
ztemnělé	ztemnělý	k2eAgFnSc6d1	ztemnělá
místnosti	místnost	k1gFnSc6	místnost
byl	být	k5eAaImAgInS	být
vržen	vržen	k2eAgInSc1d1	vržen
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
nevzdělaný	vzdělaný	k2eNgInSc1d1	nevzdělaný
lid	lid	k1gInSc1	lid
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
na	na	k7c6	na
trzích	trh	k1gInPc6	trh
podvádět	podvádět	k5eAaImF	podvádět
různými	různý	k2eAgMnPc7d1	různý
šarlatány	šarlatán	k1gMnPc7	šarlatán
a	a	k8xC	a
kouzelníky	kouzelník	k1gMnPc7	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
v	v	k7c6	v
elegantních	elegantní	k2eAgInPc6d1	elegantní
salonech	salon	k1gInPc6	salon
pobláznili	pobláznit	k5eAaPmAgMnP	pobláznit
obchodně	obchodně	k6eAd1	obchodně
zdatní	zdatný	k2eAgMnPc1d1	zdatný
zaklínači	zaklínač	k1gMnPc1	zaklínač
duchů	duch	k1gMnPc2	duch
své	svůj	k3xOyFgFnSc3	svůj
vzdělané	vzdělaný	k2eAgNnSc1d1	vzdělané
publikum	publikum	k1gNnSc1	publikum
domnělými	domnělý	k2eAgInPc7d1	domnělý
výstupy	výstup	k1gInPc7	výstup
čertů	čert	k1gMnPc2	čert
<g/>
,	,	kIx,	,
strašidel	strašidlo	k1gNnPc2	strašidlo
a	a	k8xC	a
vyvolanými	vyvolaný	k2eAgMnPc7d1	vyvolaný
mrtvými	mrtvý	k1gMnPc7	mrtvý
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
ve	v	k7c6	v
skvěle	skvěle	k6eAd1	skvěle
zinscenovaném	zinscenovaný	k2eAgNnSc6d1	zinscenované
představení	představení	k1gNnSc6	představení
promítaly	promítat	k5eAaImAgFnP	promítat
efektně	efektně	k6eAd1	efektně
na	na	k7c4	na
bílý	bílý	k2eAgInSc4d1	bílý
kouř	kouř	k1gInSc4	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Laterna	laterna	k1gFnSc1	laterna
magica	magica	k6eAd1	magica
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
při	při	k7c6	při
takovém	takový	k3xDgNnSc6	takový
zjevení	zjevení	k1gNnSc6	zjevení
samozřejmě	samozřejmě	k6eAd1	samozřejmě
skrytá	skrytý	k2eAgFnSc1d1	skrytá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osvíceném	osvícený	k2eAgMnSc6d1	osvícený
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
využívali	využívat	k5eAaImAgMnP	využívat
vědci	vědec	k1gMnPc1	vědec
tento	tento	k3xDgInSc4	tento
praktický	praktický	k2eAgInSc4d1	praktický
přístroj	přístroj	k1gInSc4	přístroj
ke	k	k7c3	k
zpříjemnění	zpříjemnění	k1gNnSc3	zpříjemnění
svých	svůj	k3xOyFgFnPc2	svůj
přednášek	přednáška	k1gFnPc2	přednáška
promítáním	promítání	k1gNnSc7	promítání
vysvětlujícího	vysvětlující	k2eAgInSc2d1	vysvětlující
obrazového	obrazový	k2eAgInSc2d1	obrazový
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
hračkářský	hračkářský	k2eAgInSc1d1	hračkářský
průmysl	průmysl	k1gInSc1	průmysl
úspěšně	úspěšně	k6eAd1	úspěšně
nabízel	nabízet	k5eAaImAgInS	nabízet
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
lucernu	lucerna	k1gFnSc4	lucerna
jako	jako	k8xS	jako
senzaci	senzace	k1gFnSc4	senzace
do	do	k7c2	do
dětských	dětský	k2eAgInPc2d1	dětský
pokojů	pokoj	k1gInPc2	pokoj
zámožných	zámožný	k2eAgMnPc2d1	zámožný
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Vynález	vynález	k1gInSc1	vynález
kinematografu	kinematograf	k1gInSc2	kinematograf
vytlačil	vytlačit	k5eAaPmAgInS	vytlačit
nakonec	nakonec	k6eAd1	nakonec
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
lucernu	lucerna	k1gFnSc4	lucerna
z	z	k7c2	z
trhu	trh	k1gInSc2	trh
kuriozit	kuriozita	k1gFnPc2	kuriozita
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
však	však	k9	však
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1895	[number]	k4	1895
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
Café	café	k1gNnSc2	café
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
žádná	žádný	k3yNgFnSc1	žádný
filmová	filmový	k2eAgFnSc1d1	filmová
premiéra	premiéra	k1gFnSc1	premiéra
nekonala	konat	k5eNaImAgFnS	konat
<g/>
:	:	kIx,	:
bratři	bratr	k1gMnPc1	bratr
Lumiérové	Lumiérový	k2eAgFnSc6d1	Lumiérový
použili	použít	k5eAaPmAgMnP	použít
laternu	laterna	k1gFnSc4	laterna
magicu	magica	k1gFnSc4	magica
jako	jako	k8xC	jako
světelný	světelný	k2eAgInSc4d1	světelný
zdroj	zdroj	k1gInSc4	zdroj
pro	pro	k7c4	pro
promítání	promítání	k1gNnSc4	promítání
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
laternou	laterna	k1gFnSc7	laterna
magicou	magicou	k6eAd1	magicou
byl	být	k5eAaImAgInS	být
splněn	splnit	k5eAaPmNgInS	splnit
jeden	jeden	k4xCgInSc1	jeden
podstatný	podstatný	k2eAgInSc1d1	podstatný
předpoklad	předpoklad	k1gInSc1	předpoklad
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
:	:	kIx,	:
možnost	možnost	k1gFnSc4	možnost
promítat	promítat	k5eAaImF	promítat
obrazy	obraz	k1gInPc4	obraz
před	před	k7c7	před
větším	veliký	k2eAgNnSc7d2	veliký
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
obrazy	obraz	k1gInPc1	obraz
však	však	k9	však
byly	být	k5eAaImAgInP	být
ještě	ještě	k9	ještě
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prolínání	prolínání	k1gNnSc2	prolínání
posunovacích	posunovací	k2eAgInPc2d1	posunovací
obrázků	obrázek	k1gInPc2	obrázek
v	v	k7c6	v
kouzelné	kouzelný	k2eAgFnSc6d1	kouzelná
lucerně	lucerna	k1gFnSc6	lucerna
vývoj	vývoj	k1gInSc4	vývoj
ke	k	k7c3	k
skutečné	skutečný	k2eAgFnSc3d1	skutečná
iluzi	iluze	k1gFnSc3	iluze
pohybu	pohyb	k1gInSc2	pohyb
nevedl	vést	k5eNaImAgInS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
obrázků	obrázek	k1gInPc2	obrázek
stále	stále	k6eAd1	stále
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
skutečném	skutečný	k2eAgInSc6d1	skutečný
<g/>
,	,	kIx,	,
s	s	k7c7	s
promítáním	promítání	k1gNnSc7	promítání
spojeném	spojený	k2eAgInSc6d1	spojený
pohybu	pohyb	k1gInSc6	pohyb
<g/>
:	:	kIx,	:
figurky	figurka	k1gFnSc2	figurka
stínohry	stínohra	k1gFnSc2	stínohra
vodili	vodit	k5eAaImAgMnP	vodit
loutkaři	loutkař	k1gMnPc1	loutkař
a	a	k8xC	a
della	della	k6eAd1	della
Portovi	Portův	k2eAgMnPc1d1	Portův
čerti	čert	k1gMnPc1	čert
museli	muset	k5eAaImAgMnP	muset
před	před	k7c7	před
otvorem	otvor	k1gInSc7	otvor
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
pochodovat	pochodovat	k5eAaImF	pochodovat
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
fotograficky	fotograficky	k6eAd1	fotograficky
nebo	nebo	k8xC	nebo
kreslířsky	kreslířsky	k6eAd1	kreslířsky
zachycené	zachycený	k2eAgInPc4d1	zachycený
obrazy	obraz	k1gInPc4	obraz
rozpohybovat	rozpohybovat	k5eAaPmF	rozpohybovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
objevit	objevit	k5eAaPmF	objevit
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
optický	optický	k2eAgInSc4d1	optický
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnímáme	vnímat	k5eAaImIp1nP	vnímat
promítání	promítání	k1gNnSc4	promítání
filmového	filmový	k2eAgInSc2d1	filmový
pásu	pás	k1gInSc2	pás
jako	jako	k8xS	jako
"	"	kIx"	"
živý	živý	k2eAgInSc1d1	živý
obraz	obraz	k1gInSc1	obraz
<g/>
"	"	kIx"	"
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
optickém	optický	k2eAgInSc6d1	optický
klamu	klam	k1gInSc6	klam
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
pás	pás	k1gInSc1	pás
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
následujících	následující	k2eAgInPc2d1	následující
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
maličkých	maličký	k2eAgInPc6d1	maličký
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tyto	tento	k3xDgInPc4	tento
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
obrázky	obrázek	k1gInPc4	obrázek
lidské	lidský	k2eAgNnSc4d1	lidské
oko	oko	k1gNnSc4	oko
vnímá	vnímat	k5eAaImIp3nS	vnímat
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
–	–	k?	–
optimální	optimální	k2eAgInSc4d1	optimální
je	být	k5eAaImIp3nS	být
24	[number]	k4	24
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
-	-	kIx~	-
splynou	splynout	k5eAaPmIp3nP	splynout
do	do	k7c2	do
"	"	kIx"	"
<g/>
živého	živý	k2eAgInSc2d1	živý
obrazu	obraz	k1gInSc2	obraz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
věda	věda	k1gFnSc1	věda
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
vzniká	vznikat	k5eAaImIp3nS	vznikat
díky	díky	k7c3	díky
setrvačnosti	setrvačnost	k1gFnSc3	setrvačnost
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
následkem	následek	k1gInSc7	následek
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
doznívání	doznívání	k1gNnSc1	doznívání
zrakového	zrakový	k2eAgInSc2d1	zrakový
vjemu	vjem	k1gInSc2	vjem
–	–	k?	–
oko	oko	k1gNnSc1	oko
podrží	podržet	k5eAaPmIp3nS	podržet
vjem	vjem	k1gInSc4	vjem
obrazu	obraz	k1gInSc2	obraz
ještě	ještě	k9	ještě
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
již	již	k6eAd1	již
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
vjem	vjem	k1gInSc4	vjem
prvního	první	k4xOgNnSc2	první
splynout	splynout	k5eAaPmF	splynout
s	s	k7c7	s
vjemem	vjem	k1gInSc7	vjem
druhého	druhý	k4xOgNnSc2	druhý
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následujícího	následující	k2eAgInSc2d1	následující
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
působení	působení	k1gNnSc1	působení
záblesků	záblesk	k1gInPc2	záblesk
<g/>
,	,	kIx,	,
stroboskopický	stroboskopický	k2eAgInSc1d1	stroboskopický
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
sled	sled	k1gInSc1	sled
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
obrazů	obraz	k1gInPc2	obraz
splyne	splynout	k5eAaPmIp3nS	splynout
do	do	k7c2	do
pohyblivého	pohyblivý	k2eAgInSc2d1	pohyblivý
toku	tok	k1gInSc2	tok
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
promítání	promítání	k1gNnSc1	promítání
obrazů	obraz	k1gInPc2	obraz
přerušováno	přerušovat	k5eAaImNgNnS	přerušovat
krátkými	krátký	k2eAgFnPc7d1	krátká
fázemi	fáze	k1gFnPc7	fáze
tmy	tma	k1gFnSc2	tma
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
objevil	objevit	k5eAaPmAgInS	objevit
anglický	anglický	k2eAgMnSc1d1	anglický
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Michael	Michael	k1gMnSc1	Michael
Faraday	Faradaa	k1gFnSc2	Faradaa
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
loukoťové	loukoťový	k2eAgNnSc4d1	loukoťové
kolo	kolo	k1gNnSc4	kolo
projíždějící	projíždějící	k2eAgNnSc4d1	projíždějící
za	za	k7c7	za
plaňkovým	plaňkový	k2eAgInSc7d1	plaňkový
plotem	plot	k1gInSc7	plot
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
stálo	stát	k5eAaImAgNnS	stát
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
couvalo	couvat	k5eAaImAgNnS	couvat
<g/>
.	.	kIx.	.
</s>
<s>
Došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
oko	oko	k1gNnSc1	oko
spojuje	spojovat	k5eAaImIp3nS	spojovat
přerušované	přerušovaný	k2eAgInPc4d1	přerušovaný
vjemy	vjem	k1gInPc4	vjem
do	do	k7c2	do
zkreslených	zkreslený	k2eAgInPc2d1	zkreslený
nebo	nebo	k8xC	nebo
chybných	chybný	k2eAgInPc2d1	chybný
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
stroboskopický	stroboskopický	k2eAgInSc1d1	stroboskopický
efekt	efekt	k1gInSc1	efekt
nemění	měnit	k5eNaImIp3nS	měnit
jen	jen	k9	jen
vnímání	vnímání	k1gNnSc1	vnímání
skutečného	skutečný	k2eAgInSc2d1	skutečný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
podmínek	podmínka	k1gFnPc2	podmínka
může	moct	k5eAaImIp3nS	moct
naopak	naopak	k6eAd1	naopak
simulovat	simulovat	k5eAaImF	simulovat
pohyb	pohyb	k1gInSc4	pohyb
reálně	reálně	k6eAd1	reálně
neexistující	existující	k2eNgInSc4d1	neexistující
<g/>
.	.	kIx.	.
</s>
<s>
Belgičan	Belgičan	k1gMnSc1	Belgičan
Joseph	Joseph	k1gInSc4	Joseph
Plateau	Plateaus	k1gInSc2	Plateaus
a	a	k8xC	a
Rakušan	Rakušan	k1gMnSc1	Rakušan
Simon	Simon	k1gMnSc1	Simon
Stampfer	Stampfer	k1gMnSc1	Stampfer
využili	využít	k5eAaPmAgMnP	využít
Faradayův	Faradayův	k2eAgInSc4d1	Faradayův
objev	objev	k1gInSc4	objev
a	a	k8xC	a
nezávise	záviset	k5eNaImSgInS	záviset
na	na	k7c4	na
sobě	se	k3xPyFc3	se
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
hračku	hračka	k1gFnSc4	hračka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
skutečně	skutečně	k6eAd1	skutečně
pohyblivé	pohyblivý	k2eAgInPc4d1	pohyblivý
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Stampferův	Stampferův	k2eAgInSc4d1	Stampferův
stroboskop	stroboskop	k1gInSc4	stroboskop
a	a	k8xC	a
Plateauův	Plateauův	k2eAgInSc4d1	Plateauův
fenakistiskop	fenakistiskop	k1gInSc4	fenakistiskop
sestávaly	sestávat	k5eAaImAgFnP	sestávat
z	z	k7c2	z
kulatého	kulatý	k2eAgInSc2d1	kulatý
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
vnějším	vnější	k2eAgInSc6d1	vnější
okraji	okraj	k1gInSc6	okraj
byly	být	k5eAaImAgInP	být
namalovány	namalován	k2eAgInPc1d1	namalován
fázové	fázový	k2eAgInPc1d1	fázový
obrázky	obrázek	k1gInPc1	obrázek
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
manželský	manželský	k2eAgInSc1d1	manželský
pár	pár	k1gInSc1	pár
tančící	tančící	k2eAgInSc1d1	tančící
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
kruhu	kruh	k1gInSc6	kruh
pod	pod	k7c7	pod
nimi	on	k3xPp3gFnPc7	on
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgInP	umístit
průzory	průzor	k1gInPc1	průzor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
člověk	člověk	k1gMnSc1	člověk
postavil	postavit	k5eAaPmAgMnS	postavit
s	s	k7c7	s
přístrojem	přístroj	k1gInSc7	přístroj
před	před	k7c4	před
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
a	a	k8xC	a
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
skrze	skrze	k?	skrze
průzory	průzor	k1gInPc1	průzor
obraz	obraz	k1gInSc1	obraz
rotujícího	rotující	k2eAgInSc2d1	rotující
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
skutečně	skutečně	k6eAd1	skutečně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Tmavé	tmavý	k2eAgFnPc1d1	tmavá
plochy	plocha	k1gFnPc1	plocha
mezi	mezi	k7c7	mezi
průzory	průzor	k1gInPc7	průzor
přerušovaly	přerušovat	k5eAaImAgInP	přerušovat
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
obrázky	obrázek	k1gInPc4	obrázek
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
a	a	k8xC	a
vyvolávaly	vyvolávat	k5eAaImAgFnP	vyvolávat
tak	tak	k9	tak
stroboskopický	stroboskopický	k2eAgInSc4d1	stroboskopický
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
první	první	k4xOgNnSc1	první
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
obrazů	obraz	k1gInPc2	obraz
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
příslušně	příslušně	k6eAd1	příslušně
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
<g/>
:	:	kIx,	:
kolo	kolo	k1gNnSc1	kolo
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
snadno	snadno	k6eAd1	snadno
pochopitelnou	pochopitelný	k2eAgFnSc4d1	pochopitelná
myšlenku	myšlenka	k1gFnSc4	myšlenka
promítat	promítat	k5eAaImF	promítat
pohyblivé	pohyblivý	k2eAgInPc4d1	pohyblivý
obrazy	obraz	k1gInPc4	obraz
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zpřístupnily	zpřístupnit	k5eAaPmAgFnP	zpřístupnit
většímu	veliký	k2eAgNnSc3d2	veliký
publiku	publikum	k1gNnSc3	publikum
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
konečně	konečně	k6eAd1	konečně
rakouský	rakouský	k2eAgMnSc1d1	rakouský
důstojník	důstojník	k1gMnSc1	důstojník
a	a	k8xC	a
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
vynálezce	vynálezce	k1gMnSc1	vynálezce
Franz	Franz	k1gMnSc1	Franz
von	von	k1gInSc4	von
Uchatius	Uchatius	k1gInSc1	Uchatius
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
zkombinoval	zkombinovat	k5eAaPmAgInS	zkombinovat
kolo	kolo	k1gNnSc4	kolo
života	život	k1gInSc2	život
s	s	k7c7	s
principem	princip	k1gInSc7	princip
laterny	laterna	k1gFnSc2	laterna
magicy	magica	k1gFnSc2	magica
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
vlastnoručně	vlastnoručně	k6eAd1	vlastnoručně
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
promítacího	promítací	k2eAgInSc2d1	promítací
přístroje	přístroj	k1gInSc2	přístroj
zasadil	zasadit	k5eAaPmAgMnS	zasadit
disk	disk	k1gInSc4	disk
se	s	k7c7	s
dvanácti	dvanáct	k4xCc7	dvanáct
průhlednými	průhledný	k2eAgInPc7d1	průhledný
obrázky	obrázek	k1gInPc7	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
každý	každý	k3xTgInSc4	každý
obrázek	obrázek	k1gInSc4	obrázek
umístil	umístit	k5eAaPmAgMnS	umístit
čočku	čočka	k1gFnSc4	čočka
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
promítací	promítací	k2eAgNnSc4d1	promítací
světlo	světlo	k1gNnSc4	světlo
otáčet	otáčet	k5eAaImF	otáčet
se	se	k3xPyFc4	se
za	za	k7c4	za
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc1	jeden
za	za	k7c7	za
druhým	druhý	k4xOgInSc7	druhý
<g/>
,	,	kIx,	,
s	s	k7c7	s
potřebnou	potřebný	k2eAgFnSc7d1	potřebná
krátkou	krátký	k2eAgFnSc7d1	krátká
tmavou	tmavý	k2eAgFnSc7d1	tmavá
fází	fáze	k1gFnSc7	fáze
<g/>
,	,	kIx,	,
promítaly	promítat	k5eAaImAgFnP	promítat
na	na	k7c4	na
stejné	stejný	k2eAgNnSc4d1	stejné
místo	místo	k1gNnSc4	místo
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ještě	ještě	k9	ještě
skutečně	skutečně	k6eAd1	skutečně
složitá	složitý	k2eAgFnSc1d1	složitá
aparatura	aparatura	k1gFnSc1	aparatura
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prodejním	prodejní	k2eAgInSc7d1	prodejní
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
promítat	promítat	k5eAaImF	promítat
jen	jen	k9	jen
skutečně	skutečně	k6eAd1	skutečně
krátké	krátký	k2eAgFnSc2d1	krátká
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Uchatiův	Uchatiův	k2eAgInSc1d1	Uchatiův
návrh	návrh	k1gInSc1	návrh
postavit	postavit	k5eAaPmF	postavit
promítací	promítací	k2eAgInSc4d1	promítací
přístroj	přístroj	k1gInSc4	přístroj
s	s	k7c7	s
čočkami	čočka	k1gFnPc7	čočka
pro	pro	k7c4	pro
sto	sto	k4xCgNnSc4	sto
obrázků	obrázek	k1gInPc2	obrázek
nebyl	být	k5eNaImAgInS	být
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
nákladný	nákladný	k2eAgInSc1d1	nákladný
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnSc3	Francouz
Emilu	Emil	k1gMnSc3	Emil
Reynaudovi	Reynauda	k1gMnSc3	Reynauda
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
promítnou	promítnout	k5eAaPmIp3nP	promítnout
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
krátké	krátká	k1gFnSc2	krátká
kreslené	kreslený	k2eAgInPc1d1	kreslený
animované	animovaný	k2eAgInPc1d1	animovaný
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
nejprve	nejprve	k6eAd1	nejprve
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
praxinoskop	praxinoskop	k1gInSc1	praxinoskop
–	–	k?	–
nahradil	nahradit	k5eAaPmAgInS	nahradit
průzory	průzor	k1gInPc4	průzor
zázračného	zázračný	k2eAgInSc2d1	zázračný
bubnu	buben	k1gInSc2	buben
hranolovitým	hranolovitý	k2eAgInSc7d1	hranolovitý
věncem	věnec	k1gInSc7	věnec
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
umístěným	umístěný	k2eAgInSc7d1	umístěný
kolem	kolem	k7c2	kolem
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
se	se	k3xPyFc4	se
díval	dívat	k5eAaImAgMnS	dívat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
věnce	věnec	k1gInSc2	věnec
pohybujícího	pohybující	k2eAgInSc2d1	pohybující
se	se	k3xPyFc4	se
bubnu	buben	k1gInSc2	buben
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
zajišťovalo	zajišťovat	k5eAaImAgNnS	zajišťovat
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
stroboskopické	stroboskopický	k2eAgNnSc1d1	stroboskopické
přerušování	přerušování	k1gNnSc1	přerušování
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pohyblivý	pohyblivý	k2eAgInSc4d1	pohyblivý
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
si	se	k3xPyFc3	se
Reynaud	Reynaud	k1gMnSc1	Reynaud
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
"	"	kIx"	"
<g/>
optické	optický	k2eAgNnSc1d1	optické
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
přístrojem	přístroj	k1gInSc7	přístroj
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
promítat	promítat	k5eAaImF	promítat
potažené	potažený	k2eAgInPc4d1	potažený
pásy	pás	k1gInPc4	pás
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
vyryty	vyryt	k2eAgFnPc1d1	vyryta
a	a	k8xC	a
následně	následně	k6eAd1	následně
vybarveny	vybarven	k2eAgFnPc4d1	vybarvena
fáze	fáze	k1gFnPc4	fáze
pohybu	pohyb	k1gInSc2	pohyb
malých	malý	k2eAgFnPc2d1	malá
scének	scénka	k1gFnPc2	scénka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
průsvitné	průsvitný	k2eAgInPc1d1	průsvitný
filmové	filmový	k2eAgInPc1d1	filmový
pásy	pás	k1gInPc1	pás
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
opatřeny	opatřen	k2eAgInPc1d1	opatřen
poutky	poutko	k1gNnPc7	poutko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
odvíjení	odvíjení	k1gNnSc3	odvíjení
filmu	film	k1gInSc2	film
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
laterny	laterna	k1gFnSc2	laterna
magicy	magica	k1gFnSc2	magica
prozařovalo	prozařovat	k5eAaImAgNnS	prozařovat
obrázky	obrázek	k1gInPc4	obrázek
fází	fáze	k1gFnPc2	fáze
a	a	k8xC	a
promítalo	promítat	k5eAaImAgNnS	promítat
je	on	k3xPp3gFnPc4	on
na	na	k7c4	na
věnec	věnec	k1gInSc4	věnec
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhé	druhý	k4xOgNnSc1	druhý
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
je	on	k3xPp3gMnPc4	on
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výčtu	výčet	k1gInSc6	výčet
nezbytných	zbytný	k2eNgInPc2d1	zbytný
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
předcházely	předcházet	k5eAaImAgFnP	předcházet
zrození	zrození	k1gNnSc4	zrození
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
chybět	chybět	k5eAaImF	chybět
objev	objev	k1gInSc4	objev
jednoho	jeden	k4xCgMnSc2	jeden
francouzského	francouzský	k2eAgMnSc2d1	francouzský
chemika	chemik	k1gMnSc2	chemik
<g/>
.	.	kIx.	.
</s>
<s>
Optický	optický	k2eAgInSc1d1	optický
postup	postup	k1gInSc1	postup
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
obsažený	obsažený	k2eAgInSc4d1	obsažený
v	v	k7c6	v
cameře	camera	k1gFnSc6	camera
obscuře	obscura	k1gFnSc6	obscura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
znám	znát	k5eAaImIp1nS	znát
již	již	k6eAd1	již
stovky	stovka	k1gFnPc4	stovka
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
Joseph	Joseph	k1gInSc1	Joseph
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépec	k1gInPc4	Niépec
<g/>
,	,	kIx,	,
podnícený	podnícený	k2eAgInSc4d1	podnícený
svým	svůj	k1gMnSc7	svůj
krajanem	krajan	k1gMnSc7	krajan
<g/>
,	,	kIx,	,
malířem	malíř	k1gMnSc7	malíř
Louisem	Louis	k1gMnSc7	Louis
Jacquesem	Jacques	k1gMnSc7	Jacques
Mandé	Mandý	k2eAgNnSc1d1	Mandý
Daguerrem	Daguerr	k1gMnSc7	Daguerr
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgMnS	najít
chemický	chemický	k2eAgInSc4d1	chemický
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
světelný	světelný	k2eAgInSc1d1	světelný
obraz	obraz	k1gInSc1	obraz
promítaný	promítaný	k2eAgInSc1d1	promítaný
v	v	k7c6	v
cameře	camera	k1gFnSc6	camera
obscuře	obscura	k1gFnSc6	obscura
zachytit	zachytit	k5eAaPmF	zachytit
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
teprve	teprve	k6eAd1	teprve
pomocí	pomocí	k7c2	pomocí
tužky	tužka	k1gFnSc2	tužka
<g/>
.	.	kIx.	.
</s>
<s>
Niépce	Niépec	k1gInPc1	Niépec
proto	proto	k8xC	proto
vsadil	vsadit	k5eAaPmAgInS	vsadit
do	do	k7c2	do
kamery	kamera	k1gFnSc2	kamera
desku	deska	k1gFnSc4	deska
potřenou	potřený	k2eAgFnSc4d1	potřená
dusičnanem	dusičnan	k1gInSc7	dusičnan
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
roztok	roztok	k1gInSc1	roztok
z	z	k7c2	z
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
prachu	prach	k1gInSc2	prach
po	po	k7c6	po
osvícení	osvícení	k1gNnSc6	osvícení
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Niépcemu	Niépcema	k1gFnSc4	Niépcema
vytvořit	vytvořit	k5eAaPmF	vytvořit
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
první	první	k4xOgFnSc4	první
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
expozice	expozice	k1gFnSc1	expozice
avšak	avšak	k8xC	avšak
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
příliš	příliš	k6eAd1	příliš
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vytvořit	vytvořit	k5eAaPmF	vytvořit
zřetelný	zřetelný	k2eAgInSc4d1	zřetelný
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Daguerre	Daguerr	k1gMnSc5	Daguerr
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
skrytě	skrytě	k6eAd1	skrytě
připraven	připravit	k5eAaPmNgInS	připravit
i	i	k9	i
po	po	k7c6	po
kratší	krátký	k2eAgFnSc6d2	kratší
době	doba	k1gFnSc6	doba
expozice	expozice	k1gFnSc2	expozice
a	a	k8xC	a
vyjeví	vyjevit	k5eAaPmIp3nS	vyjevit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
exponovaná	exponovaný	k2eAgFnSc1d1	exponovaná
deska	deska	k1gFnSc1	deska
ponoří	ponořit	k5eAaPmIp3nS	ponořit
v	v	k7c6	v
temnu	temno	k1gNnSc6	temno
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
zafixoval	zafixovat	k5eAaPmAgInS	zafixovat
v	v	k7c6	v
solném	solný	k2eAgInSc6d1	solný
roztoku	roztok	k1gInSc6	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Niépce	Niépec	k1gMnSc4	Niépec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
před	před	k7c7	před
zveřejněním	zveřejnění	k1gNnSc7	zveřejnění
postupu	postup	k1gInSc2	postup
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
před	před	k7c7	před
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
Akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
bez	bez	k7c2	bez
uznání	uznání	k1gNnSc2	uznání
tohoto	tento	k3xDgMnSc2	tento
partnera	partner	k1gMnSc2	partner
jako	jako	k8xS	jako
daguerrotypie	daguerrotypie	k1gFnSc2	daguerrotypie
<g/>
.	.	kIx.	.
</s>
<s>
Daguerrotypie	daguerrotypie	k1gFnPc1	daguerrotypie
byly	být	k5eAaImAgFnP	být
ovšem	ovšem	k9	ovšem
ještě	ještě	k6eAd1	ještě
stranově	stranově	k6eAd1	stranově
převrácené	převrácený	k2eAgInPc4d1	převrácený
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
unikáty	unikát	k1gInPc4	unikát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
prosadil	prosadit	k5eAaPmAgInS	prosadit
postup	postup	k1gInSc1	postup
anglického	anglický	k2eAgMnSc2d1	anglický
konkurenta	konkurent	k1gMnSc2	konkurent
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
Daguerrova	Daguerrův	k2eAgInSc2d1	Daguerrův
triumfu	triumf	k1gInSc2	triumf
podařilo	podařit	k5eAaPmAgNnS	podařit
osvítit	osvítit	k5eAaPmF	osvítit
potažený	potažený	k2eAgInSc4d1	potažený
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Fox	fox	k1gInSc4	fox
Talbot	Talbota	k1gFnPc2	Talbota
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
ještě	ještě	k6eAd1	ještě
dnes	dnes	k6eAd1	dnes
běžnou	běžný	k2eAgFnSc4d1	běžná
technologii	technologie	k1gFnSc4	technologie
negativu	negativ	k1gInSc2	negativ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
papírových	papírový	k2eAgFnPc2d1	papírová
fotografií	fotografia	k1gFnPc2	fotografia
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
dělat	dělat	k5eAaImF	dělat
četné	četný	k2eAgInPc4d1	četný
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
stranově	stranově	k6eAd1	stranově
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
kopie	kopie	k1gFnPc1	kopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
americký	americký	k2eAgInSc1d1	americký
duchovní	duchovní	k2eAgInSc1d1	duchovní
Hannibal	Hannibal	k1gInSc1	Hannibal
Goodwin	Goodwin	k1gInSc1	Goodwin
první	první	k4xOgInPc4	první
filmové	filmový	k2eAgInPc4d1	filmový
pásy	pás	k1gInPc4	pás
z	z	k7c2	z
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
celuloid	celuloid	k1gInSc1	celuloid
byl	být	k5eAaImAgInS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
tenoučkou	tenoučký	k2eAgFnSc7d1	tenoučká
exponovatelnou	exponovatelný	k2eAgFnSc7d1	exponovatelný
vrstvou	vrstva	k1gFnSc7	vrstva
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jej	on	k3xPp3gNnSc4	on
možno	možno	k6eAd1	možno
transportovat	transportovat	k5eAaBmF	transportovat
srolovaný	srolovaný	k2eAgInSc1d1	srolovaný
v	v	k7c6	v
kameře	kamera	k1gFnSc6	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Eastman	Eastman	k1gMnSc1	Eastman
Kodak	Kodak	kA	Kodak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
experimentovala	experimentovat	k5eAaImAgFnS	experimentovat
s	s	k7c7	s
celuloidem	celuloid	k1gInSc7	celuloid
jako	jako	k8xC	jako
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
fotografické	fotografický	k2eAgInPc4d1	fotografický
snímky	snímek	k1gInPc4	snímek
téměř	téměř	k6eAd1	téměř
současně	současně	k6eAd1	současně
a	a	k8xC	a
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Goodwinovi	Goodwin	k1gMnSc6	Goodwin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgInSc2	tento
objevu	objev	k1gInSc2	objev
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
filmových	filmový	k2eAgFnPc2d1	filmová
továren	továrna	k1gFnPc2	továrna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
fotografie	fotografie	k1gFnSc1	fotografie
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
<g/>
,	,	kIx,	,
předváděla	předvádět	k5eAaImAgFnS	předvádět
kola	kola	k1gFnSc1	kola
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zázračné	zázračný	k2eAgInPc1d1	zázračný
bubínky	bubínek	k1gInPc1	bubínek
<g/>
,	,	kIx,	,
palcová	palcový	k2eAgNnPc1d1	palcové
kina	kino	k1gNnPc1	kino
<g/>
,	,	kIx,	,
mutoskopy	mutoskop	k1gInPc1	mutoskop
<g/>
,	,	kIx,	,
praxinoskopy	praxinoskop	k1gInPc1	praxinoskop
a	a	k8xC	a
optické	optický	k2eAgNnSc4d1	optické
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c4	v
živé	živý	k2eAgInPc4d1	živý
obrazy	obraz	k1gInPc4	obraz
jen	jen	k6eAd1	jen
kresby	kresba	k1gFnSc2	kresba
a	a	k8xC	a
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Raná	raný	k2eAgFnSc1d1	raná
fotografie	fotografie	k1gFnSc1	fotografie
neumožňovala	umožňovat	k5eNaImAgFnS	umožňovat
zachytit	zachytit	k5eAaPmF	zachytit
na	na	k7c4	na
přesně	přesně	k6eAd1	přesně
za	za	k7c7	za
sebou	se	k3xPyFc7	se
následující	následující	k2eAgInPc1d1	následující
fázové	fázový	k2eAgInPc1d1	fázový
obrázky	obrázek	k1gInPc1	obrázek
nejmenší	malý	k2eAgInPc1d3	nejmenší
úseky	úsek	k1gInPc1	úsek
fotografované	fotografovaný	k2eAgFnSc2d1	fotografovaná
akce	akce	k1gFnSc2	akce
nezbytné	nezbytný	k2eAgFnSc2d1	nezbytná
pro	pro	k7c4	pro
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
trvala	trvat	k5eAaImAgFnS	trvat
expozice	expozice	k1gFnSc1	expozice
příliš	příliš	k6eAd1	příliš
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
splynutí	splynutí	k1gNnSc4	splynutí
fotografií	fotografia	k1gFnPc2	fotografia
do	do	k7c2	do
pohyblivého	pohyblivý	k2eAgInSc2d1	pohyblivý
obrazu	obraz	k1gInSc2	obraz
v	v	k7c6	v
zázračném	zázračný	k2eAgInSc6d1	zázračný
bubnu	buben	k1gInSc6	buben
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
se	s	k7c7	s
značným	značný	k2eAgNnSc7d1	značné
trápením	trápení	k1gNnSc7	trápení
fotomodelů	fotomodel	k1gMnPc2	fotomodel
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
museli	muset	k5eAaImAgMnP	muset
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
prvních	první	k4xOgNnPc6	první
"	"	kIx"	"
<g/>
oživlých	oživlý	k2eAgFnPc6d1	oživlá
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
"	"	kIx"	"
Henry	henry	k1gInSc4	henry
R.	R.	kA	R.
Heyl	Heyl	k1gInSc1	Heyl
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
fantasmatropu	fantasmatrop	k1gInSc6	fantasmatrop
–	–	k?	–
promítacím	promítací	k2eAgNnSc6d1	promítací
kole	kolo	k1gNnSc6	kolo
života	život	k1gInSc2	život
s	s	k7c7	s
trhavým	trhavý	k2eAgInSc7d1	trhavý
transportním	transportní	k2eAgInSc7d1	transportní
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
,	,	kIx,	,
na	na	k7c6	na
nekonečných	konečný	k2eNgNnPc6d1	nekonečné
sezeních	sezení	k1gNnPc6	sezení
pózovat	pózovat	k5eAaImF	pózovat
kvůli	kvůli	k7c3	kvůli
době	doba	k1gFnSc3	doba
expozice	expozice	k1gFnSc2	expozice
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
jednotlivé	jednotlivý	k2eAgFnSc6d1	jednotlivá
fázi	fáze	k1gFnSc6	fáze
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
sotva	sotva	k6eAd1	sotva
simulován	simulován	k2eAgInSc4d1	simulován
přirozený	přirozený	k2eAgInSc4d1	přirozený
průběh	průběh	k1gInSc4	průběh
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nasnadě	nasnadě	k6eAd1	nasnadě
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
pionýři	pionýr	k1gMnPc1	pionýr
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
Edison	Edison	k1gNnSc1	Edison
<g/>
,	,	kIx,	,
Skladanowští	Skladanowský	k1gMnPc1	Skladanowský
a	a	k8xC	a
Lumiérové	Lumiérový	k2eAgFnPc1d1	Lumiérový
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
ohromit	ohromit	k5eAaPmF	ohromit
a	a	k8xC	a
vyděsit	vyděsit	k5eAaPmF	vyděsit
publikum	publikum	k1gNnSc1	publikum
ucelenými	ucelený	k2eAgInPc7d1	ucelený
výjevy	výjev	k1gInPc7	výjev
a	a	k8xC	a
dokumentárními	dokumentární	k2eAgInPc7d1	dokumentární
snímky	snímek	k1gInPc7	snímek
<g/>
,	,	kIx,	,
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
nejprve	nejprve	k6eAd1	nejprve
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
technické	technický	k2eAgInPc1d1	technický
předpoklady	předpoklad	k1gInPc1	předpoklad
pro	pro	k7c4	pro
momentovou	momentový	k2eAgFnSc4d1	momentová
a	a	k8xC	a
sekvenční	sekvenční	k2eAgFnSc4d1	sekvenční
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
průkopník	průkopník	k1gMnSc1	průkopník
Eadweard	Eadwearda	k1gFnPc2	Eadwearda
Muybridge	Muybridge	k1gFnPc2	Muybridge
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stal	stát	k5eAaPmAgMnS	stát
slavným	slavný	k2eAgInSc7d1	slavný
díky	dík	k1gInPc7	dík
jeho	jeho	k3xOp3gFnSc2	jeho
fotografické	fotografický	k2eAgFnSc2d1	fotografická
loco-motion	locootion	k1gInSc4	loco-motion
studie	studie	k1gFnPc4	studie
(	(	kIx(	(
<g/>
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
např.	např.	kA	např.
1882	[number]	k4	1882
je	být	k5eAaImIp3nS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
"	"	kIx"	"
<g/>
Kůň	kůň	k1gMnSc1	kůň
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
Muybridge	Muybridg	k1gFnSc2	Muybridg
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
okamžitým	okamžitý	k2eAgInSc7d1	okamžitý
záznamem	záznam	k1gInSc7	záznam
sprintujícího	sprintující	k2eAgMnSc4d1	sprintující
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
na	na	k7c6	na
dostizích	dostih	k1gInPc6	dostih
v	v	k7c6	v
Sacramentu	Sacrament	k1gInSc6	Sacrament
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1878	[number]	k4	1878
úspěšně	úspěšně	k6eAd1	úspěšně
provedli	provést	k5eAaPmAgMnP	provést
chronofotografii	chronofotografie	k1gFnSc4	chronofotografie
experimentu	experiment	k1gInSc2	experiment
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gMnSc5	Alta
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
pro	pro	k7c4	pro
bohatého	bohatý	k2eAgMnSc4d1	bohatý
politika	politik	k1gMnSc4	politik
Lelanda	Lelando	k1gNnSc2	Lelando
Stanforda	Stanforda	k1gFnSc1	Stanforda
pomocí	pomocí	k7c2	pomocí
několika	několik	k4yIc2	několik
kamer	kamera	k1gFnPc2	kamera
snímajících	snímající	k2eAgInPc2d1	snímající
koňské	koňský	k2eAgInPc4d1	koňský
dostihy	dostih	k1gInPc4	dostih
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
kůň	kůň	k1gMnSc1	kůň
zvedne	zvednout	k5eAaPmIp3nS	zvednout
všechny	všechen	k3xTgFnPc4	všechen
čtyři	čtyři	k4xCgFnPc4	čtyři
končetiny	končetina	k1gFnPc4	končetina
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
Muybridga	Muybridga	k1gFnSc1	Muybridga
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
publikovaly	publikovat	k5eAaBmAgInP	publikovat
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
nastříhány	nastříhán	k2eAgInPc4d1	nastříhán
na	na	k7c4	na
proužky	proužek	k1gInPc4	proužek
a	a	k8xC	a
používané	používaný	k2eAgInPc4d1	používaný
v	v	k7c6	v
praxinoskopu	praxinoskop	k1gInSc6	praxinoskop
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
potomka	potomek	k1gMnSc4	potomek
<g/>
"	"	kIx"	"
stroboskopického	stroboskopický	k2eAgInSc2d1	stroboskopický
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Charles	Charles	k1gMnSc1	Charles
Emile	Emil	k1gMnSc5	Emil
Reynaud	Reynaudo	k1gNnPc2	Reynaudo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Praxinoskop	Praxinoskop	k1gInSc1	Praxinoskop
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc4	první
filmový	filmový	k2eAgInSc4d1	filmový
přístroj	přístroj	k1gInSc4	přístroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zobrazoval	zobrazovat	k5eAaImAgInS	zobrazovat
sérii	série	k1gFnSc4	série
snímků	snímek	k1gInPc2	snímek
na	na	k7c4	na
stěnu	stěna	k1gFnSc4	stěna
(	(	kIx(	(
<g/>
či	či	k8xC	či
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
svislou	svislý	k2eAgFnSc4d1	svislá
plochu	plocha	k1gFnSc4	plocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muybridgovi	Muybridgův	k2eAgMnPc1d1	Muybridgův
jeho	jeho	k3xOp3gFnSc2	jeho
fotografické	fotografický	k2eAgFnSc2d1	fotografická
série	série	k1gFnSc2	série
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
pomohly	pomoct	k5eAaPmAgInP	pomoct
vytvořit	vytvořit	k5eAaPmF	vytvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
vynález	vynález	k1gInSc4	vynález
Zoopraxiskop	Zoopraxiskop	k1gInSc1	Zoopraxiskop
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
zoogyroscope	zoogyroscop	k1gInSc5	zoogyroscop
či	či	k8xC	či
kolo	kolo	k1gNnSc1	kolo
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
primitivní	primitivní	k2eAgInSc1d1	primitivní
filmový	filmový	k2eAgInSc1d1	filmový
projektor	projektor	k1gInSc1	projektor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
animaci	animace	k1gFnSc4	animace
<g/>
)	)	kIx)	)
a	a	k8xC	a
promítal	promítat	k5eAaImAgMnS	promítat
rychle	rychle	k6eAd1	rychle
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucí	jdoucí	k2eAgInPc4d1	jdoucí
obrázky	obrázek	k1gInPc4	obrázek
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgInPc1d1	skutečný
filmy	film	k1gInPc1	film
vznikaly	vznikat	k5eAaImAgInP	vznikat
až	až	k9	až
po	po	k7c6	po
vývoji	vývoj	k1gInSc6	vývoj
fotografického	fotografický	k2eAgInSc2d1	fotografický
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
pružný	pružný	k2eAgInSc1d1	pružný
a	a	k8xC	a
průhledný	průhledný	k2eAgInSc1d1	průhledný
celuloid	celuloid	k1gInSc1	celuloid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mohl	moct	k5eAaImAgMnS	moct
nahrát	nahrát	k5eAaBmF	nahrát
záznam	záznam	k1gInSc4	záznam
i	i	k9	i
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
pokusů	pokus	k1gInPc2	pokus
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
pařížským	pařížský	k2eAgMnSc7d1	pařížský
inovátorem	inovátor	k1gMnSc7	inovátor
a	a	k8xC	a
fyziologem	fyziolog	k1gMnSc7	fyziolog
Étienne-Jules	Étienne-Jules	k1gMnSc1	Étienne-Jules
Mareyem	Marey	k1gMnSc7	Marey
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
a	a	k8xC	a
nahrával	nahrávat	k5eAaImAgMnS	nahrávat
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
především	především	k9	především
létající	létající	k2eAgMnPc4d1	létající
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pelikány	pelikán	k1gInPc7	pelikán
v	v	k7c6	v
letu	let	k1gInSc6	let
<g/>
)	)	kIx)	)
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
pomocí	pomocí	k7c2	pomocí
fotografických	fotografický	k2eAgInPc2d1	fotografický
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
nápadu	nápad	k1gInSc3	nápad
"	"	kIx"	"
<g/>
revolvingové	revolvingový	k2eAgFnSc2d1	revolvingová
fotografické	fotografický	k2eAgFnSc2d1	fotografická
desky	deska	k1gFnSc2	deska
<g/>
"	"	kIx"	"
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
francouzským	francouzský	k2eAgMnSc7d1	francouzský
astronomem	astronom	k1gMnSc7	astronom
Pierre-Jules	Pierre-Jules	k1gMnSc1	Pierre-Jules
César	César	k1gMnSc1	César
Janssenem	Janssen	k1gMnSc7	Janssen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
Marey	Marea	k1gFnSc2	Marea
často	často	k6eAd1	často
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
vynálezcem	vynálezce	k1gMnSc7	vynálezce
kina	kino	k1gNnSc2	kino
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
kameru	kamera	k1gFnSc4	kamera
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
mohl	moct	k5eAaImAgInS	moct
vyfotit	vyfotit	k5eAaPmF	vyfotit
více	hodně	k6eAd2	hodně
fotografií	fotografia	k1gFnPc2	fotografia
(	(	kIx(	(
<g/>
až	až	k9	až
12	[number]	k4	12
<g/>
)	)	kIx)	)
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
a	a	k8xC	a
zachytit	zachytit	k5eAaPmF	zachytit
pohybující	pohybující	k2eAgNnPc4d1	pohybující
se	se	k3xPyFc4	se
zvířata	zvíře	k1gNnPc4	zvíře
nebo	nebo	k8xC	nebo
lidi	člověk	k1gMnPc4	člověk
–	–	k?	–
tzv.	tzv.	kA	tzv.
chronofotografii	chronofotografie	k1gFnSc4	chronofotografie
neboli	neboli	k8xC	neboli
sériové	sériový	k2eAgNnSc4d1	sériové
fotografování	fotografování	k1gNnSc4	fotografování
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
Muybridgeho	Muybridge	k1gMnSc4	Muybridge
exponované	exponovaný	k2eAgFnSc2d1	exponovaná
snímky	snímka	k1gFnSc2	snímka
běžících	běžící	k2eAgMnPc2d1	běžící
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Termín	termín	k1gInSc1	termín
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
Mareyeho	Mareye	k1gMnSc2	Mareye
vynálezu	vynález	k1gInSc2	vynález
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
více	hodně	k6eAd2	hodně
snímků	snímek	k1gInPc2	snímek
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
subjektu	subjekt	k1gInSc2	subjekt
než	než	k8xS	než
stačil	stačit	k5eAaBmAgInS	stačit
Muybridge	Muybridge	k1gInSc4	Muybridge
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
obrázky	obrázek	k1gInPc1	obrázek
vyfotit	vyfotit	k5eAaPmF	vyfotit
<g/>
.	.	kIx.	.
</s>
<s>
Mayereho	Mayereze	k6eAd1	Mayereze
chronofotografie	chronofotografie	k1gFnSc1	chronofotografie
byla	být	k5eAaImAgFnS	být
revoluční	revoluční	k2eAgFnSc1d1	revoluční
<g/>
.	.	kIx.	.
</s>
<s>
Brzo	brzo	k6eAd1	brzo
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k6eAd1	až
30	[number]	k4	30
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
experimenty	experiment	k1gInPc1	experiment
byly	být	k5eAaImAgInP	být
provedeny	provést	k5eAaPmNgInP	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
rodákem	rodák	k1gMnSc7	rodák
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
Louis	Louis	k1gMnSc1	Louis
Aimé	Aimé	k1gNnSc2	Aimé
Augustin	Augustin	k1gMnSc1	Augustin
Le	Le	k1gMnSc7	Le
Princem	princ	k1gMnSc7	princ
<g/>
.	.	kIx.	.
</s>
<s>
Le	Le	k?	Le
Prince	princ	k1gMnSc2	princ
používal	používat	k5eAaImAgMnS	používat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
roli	role	k1gFnSc4	role
papíru	papír	k1gInSc2	papír
pokrytou	pokrytý	k2eAgFnSc7d1	pokrytá
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
emulzí	emulze	k1gFnSc7	emulze
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
a	a	k8xC	a
patentoval	patentovat	k5eAaBmAgMnS	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
krátké	krátký	k2eAgInPc1d1	krátký
fragmenty	fragment	k1gInPc1	fragment
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
i	i	k9	i
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
křižovatka	křižovatka	k1gFnSc1	křižovatka
na	na	k7c6	na
mostě	most	k1gInSc6	most
v	v	k7c4	v
Leeds	Leeds	k1gInSc4	Leeds
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
Muybridgeho	Muybridge	k1gMnSc2	Muybridge
<g/>
,	,	kIx,	,
Mareyeho	Mareye	k1gMnSc2	Mareye
a	a	k8xC	a
Le	Le	k1gMnSc2	Le
Prince	princ	k1gMnSc2	princ
položila	položit	k5eAaPmAgFnS	položit
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
filmové	filmový	k2eAgFnSc2d1	filmová
kamery	kamera	k1gFnSc2	kamera
<g/>
,	,	kIx,	,
projektoru	projektor	k1gInSc2	projektor
a	a	k8xC	a
transparentního	transparentní	k2eAgInSc2d1	transparentní
celuloidového	celuloidový	k2eAgInSc2d1	celuloidový
filmu	film	k1gInSc2	film
–	–	k?	–
tedy	tedy	k8xC	tedy
rozvoji	rozvoj	k1gInSc3	rozvoj
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
vynálezce	vynálezce	k1gMnSc1	vynálezce
George	George	k1gNnSc2	George
Eastman	Eastman	k1gMnSc1	Eastman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
suchou	suchý	k2eAgFnSc4d1	suchá
desku	deska	k1gFnSc4	deska
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
vzniku	vznik	k1gInSc2	vznik
stabilnějšího	stabilní	k2eAgInSc2d2	stabilnější
typu	typ	k1gInSc2	typ
celuloidového	celuloidový	k2eAgInSc2d1	celuloidový
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
také	také	k9	také
malou	malý	k2eAgFnSc4d1	malá
kamerovou	kamerový	k2eAgFnSc4d1	kamerová
(	(	kIx(	(
<g/>
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
<g/>
)	)	kIx)	)
krabičku	krabička	k1gFnSc4	krabička
"	"	kIx"	"
<g/>
Kodak	Kodak	kA	Kodak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
používala	používat	k5eAaImAgFnS	používat
srolovaný	srolovaný	k2eAgInSc4d1	srolovaný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
filmovou	filmový	k2eAgFnSc4d1	filmová
roli	role	k1gFnSc4	role
papíru	papír	k1gInSc2	papír
svým	svůj	k3xOyFgInSc7	svůj
vynálezem	vynález	k1gInSc7	vynález
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
–	–	k?	–
perforovaný	perforovaný	k2eAgInSc1d1	perforovaný
celuloidový	celuloidový	k2eAgInSc1d1	celuloidový
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
syntetický	syntetický	k2eAgInSc1d1	syntetický
plast	plast	k1gInSc1	plast
potažený	potažený	k2eAgInSc1d1	potažený
želatinou	želatina	k1gFnSc7	želatina
<g/>
)	)	kIx)	)
s	s	k7c7	s
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
emulzí	emulze	k1gFnSc7	emulze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
slavný	slavný	k2eAgMnSc1d1	slavný
americký	americký	k2eAgMnSc1d1	americký
vynálezce	vynálezce	k1gMnSc1	vynálezce
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
mladý	mladý	k2eAgMnSc1d1	mladý
britský	britský	k2eAgMnSc1d1	britský
asistent	asistent	k1gMnSc1	asistent
William	William	k1gInSc4	William
Kennedy	Kenneda	k1gMnSc2	Kenneda
Laurie	Laurie	k1gFnSc2	Laurie
Dickson	Dicksona	k1gFnPc2	Dicksona
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
laboratoři	laboratoř	k1gFnSc6	laboratoř
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Orange	Orange	k1gInSc4	Orange
(	(	kIx(	(
<g/>
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
vypůjčili	vypůjčit	k5eAaPmAgMnP	vypůjčit
dřívější	dřívější	k2eAgFnSc4d1	dřívější
práci	práce	k1gFnSc4	práce
Muybridgeho	Muybridge	k1gMnSc4	Muybridge
<g/>
,	,	kIx,	,
Mareyho	Marey	k1gMnSc4	Marey
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc4	Le
Princeho	Prince	k1gMnSc4	Prince
a	a	k8xC	a
Eastmana	Eastman	k1gMnSc4	Eastman
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
pohybu	pohyb	k1gInSc2	pohyb
na	na	k7c4	na
film	film	k1gInSc4	film
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
tvůrčího	tvůrčí	k2eAgMnSc2d1	tvůrčí
a	a	k8xC	a
inovačního	inovační	k2eAgInSc2d1	inovační
vývoje	vývoj	k1gInSc2	vývoj
provedl	provést	k5eAaPmAgMnS	provést
Dickson	Dickson	k1gMnSc1	Dickson
<g/>
,	,	kIx,	,
Edison	Edison	k1gMnSc1	Edison
pouze	pouze	k6eAd1	pouze
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Edisonovi	Edison	k1gMnSc3	Edison
připisovaná	připisovaný	k2eAgFnSc1d1	připisovaná
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
filmových	filmový	k2eAgFnPc2d1	filmová
kamer	kamera	k1gFnPc2	kamera
a	a	k8xC	a
projektorů	projektor	k1gInPc2	projektor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
Dickson	Dickson	k1gInSc1	Dickson
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1890	[number]	k4	1890
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
motorem	motor	k1gInSc7	motor
poháněnou	poháněný	k2eAgFnSc4d1	poháněná
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
filmovat	filmovat	k5eAaImF	filmovat
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kinetograph	Kinetograph	k1gInSc1	Kinetograph
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
filmů	film	k1gInPc2	film
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gNnSc1	Edison
Studios	Studios	k?	Studios
bylo	být	k5eAaImAgNnS	být
formálně	formálně	k6eAd1	formálně
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k9	jako
Manufacturing	Manufacturing	k1gInSc4	Manufacturing
Company	Compana	k1gFnSc2	Compana
Edison	Edison	k1gNnSc4	Edison
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
inovacemi	inovace	k1gFnPc7	inovace
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
práci	práce	k1gFnSc3	práce
asistenta	asistent	k1gMnSc2	asistent
Dicksona	Dickson	k1gMnSc2	Dickson
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Motorem	motor	k1gInSc7	motor
poháněná	poháněný	k2eAgFnSc1d1	poháněná
kamera	kamera	k1gFnSc1	kamera
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
k	k	k7c3	k
zachycení	zachycení	k1gNnSc3	zachycení
pohybu	pohyb	k1gInSc2	pohyb
se	s	k7c7	s
synchronizovanou	synchronizovaný	k2eAgFnSc7d1	synchronizovaná
clonou	clona	k1gFnSc7	clona
a	a	k8xC	a
systémem	systém	k1gInSc7	systém
ozubených	ozubený	k2eAgNnPc2d1	ozubené
kol	kolo	k1gNnPc2	kolo
(	(	kIx(	(
<g/>
Dicksonův	Dicksonův	k2eAgInSc1d1	Dicksonův
unikátní	unikátní	k2eAgInSc1d1	unikátní
vynález	vynález	k1gInSc1	vynález
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kinetographu	kinetograph	k1gInSc6	kinetograph
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
35	[number]	k4	35
mm	mm	kA	mm
široký	široký	k2eAgInSc1d1	široký
perforovaný	perforovaný	k2eAgInSc1d1	perforovaný
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1892	[number]	k4	1892
úředně	úředně	k6eAd1	úředně
zavedený	zavedený	k2eAgInSc4d1	zavedený
kinetograph	kinetograph	k1gInSc4	kinetograph
jako	jako	k8xS	jako
soubor	soubor	k1gInSc4	soubor
standardních	standardní	k2eAgFnPc2d1	standardní
kamer	kamera	k1gFnPc2	kamera
pro	pro	k7c4	pro
divadelní	divadelní	k2eAgFnPc4d1	divadelní
hry	hra	k1gFnPc4	hra
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Rukou	ruka	k1gFnSc7	ruka
poháněná	poháněný	k2eAgFnSc1d1	poháněná
kamera	kamera	k1gFnSc1	kamera
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stala	stát	k5eAaPmAgFnS	stát
populárnější	populární	k2eAgFnSc1d2	populárnější
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
motorem	motor	k1gInSc7	motor
poháněné	poháněný	k2eAgFnSc2d1	poháněná
kamery	kamera	k1gFnSc2	kamera
byly	být	k5eAaImAgInP	být
těžké	těžký	k2eAgInPc1d1	těžký
a	a	k8xC	a
neskladné	skladný	k2eNgInPc1d1	neskladný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
Dickson	Dickson	k1gMnSc1	Dickson
také	také	k9	také
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
verzi	verze	k1gFnSc4	verze
filmového	filmový	k2eAgInSc2d1	filmový
projektoru	projektor	k1gInSc2	projektor
na	na	k7c4	na
Zoetrope	Zoetrop	k1gInSc5	Zoetrop
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Kinetoskop	kinetoskop	k1gInSc1	kinetoskop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
nebo	nebo	k8xC	nebo
1890	[number]	k4	1890
natočil	natočit	k5eAaBmAgInS	natočit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
na	na	k7c6	na
experimentálním	experimentální	k2eAgInSc6d1	experimentální
kinetoskopu	kinetoskop	k1gInSc6	kinetoskop
<g/>
,	,	kIx,	,
machinace	machinace	k1gFnSc1	machinace
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgInSc1	první
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
veřejná	veřejný	k2eAgFnSc1d1	veřejná
prezentace	prezentace	k1gFnSc1	prezentace
filmu	film	k1gInSc2	film
v	v	k7c6	v
USA	USA	kA	USA
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
kinetoskopu	kinetoskop	k1gInSc2	kinetoskop
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
Edisonově	Edisonův	k2eAgFnSc6d1	Edisonova
laboratoři	laboratoř	k1gFnSc6	laboratoř
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1891	[number]	k4	1891
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dicksonův	Dicksonův	k2eAgInSc1d1	Dicksonův
pozdrav	pozdrav	k1gInSc1	pozdrav
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
krátkém	krátký	k2eAgInSc6d1	krátký
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
Dickson	Dickson	k1gMnSc1	Dickson
objevil	objevit	k5eAaPmAgMnS	objevit
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
uklonil	uklonit	k5eAaPmAgInS	uklonit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
usmál	usmát	k5eAaPmAgMnS	usmát
se	se	k3xPyFc4	se
a	a	k8xC	a
ceremoniálně	ceremoniálně	k6eAd1	ceremoniálně
si	se	k3xPyFc3	se
sundal	sundat	k5eAaPmAgMnS	sundat
klobouk	klobouk	k1gInSc4	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Edison	Edison	k1gInSc1	Edison
zahájil	zahájit	k5eAaPmAgInS	zahájit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
upraveným	upravený	k2eAgInSc7d1	upravený
kinetoskopem	kinetoskop	k1gInSc7	kinetoskop
komerční	komerční	k2eAgInSc4d1	komerční
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Kinetoskop	kinetoskop	k1gInSc4	kinetoskop
byl	být	k5eAaImAgMnS	být
předchůdce	předchůdce	k1gMnSc1	předchůdce
filmového	filmový	k2eAgInSc2d1	filmový
projektoru	projektor	k1gInSc2	projektor
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
zvuku	zvuk	k1gInSc2	zvuk
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
patentován	patentovat	k5eAaBmNgInS	patentovat
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1897	[number]	k4	1897
(	(	kIx(	(
<g/>
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
patent	patent	k1gInSc4	patent
Edison	Edison	k1gMnSc1	Edison
podal	podat	k5eAaPmAgMnS	podat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stroj	stroj	k1gInSc1	stroj
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
na	na	k7c6	na
karnevalech	karneval	k1gInPc6	karneval
<g/>
,	,	kIx,	,
v	v	k7c6	v
salónech	salón	k1gInPc6	salón
nebo	nebo	k8xC	nebo
v	v	k7c6	v
hernách	herna	k1gFnPc6	herna
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
první	první	k4xOgNnSc4	první
filmové	filmový	k2eAgNnSc4d1	filmové
produkční	produkční	k2eAgNnSc4d1	produkční
studio	studio	k1gNnSc4	studio
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
"	"	kIx"	"
<g/>
Černá	černý	k2eAgFnSc1d1	černá
Marie	Marie	k1gFnSc1	Marie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
Black	Black	k1gInSc1	Black
Maria	Maria	k1gFnSc1	Maria
<g/>
"	"	kIx"	"
či	či	k8xC	či
samotným	samotný	k2eAgInSc7d1	samotný
Edisonem	Edison	k1gInSc7	Edison
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Psí	psí	k2eAgFnSc1d1	psí
bouda	bouda	k1gFnSc1	bouda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
Edisonovy	Edisonův	k2eAgFnSc2d1	Edisonova
laboratoře	laboratoř	k1gFnSc2	laboratoř
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Orange	Orange	k1gInSc4	Orange
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
637,67	[number]	k4	637,67
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
tvorby	tvorba	k1gFnSc2	tvorba
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
kinetoskop	kinetoskop	k1gInSc4	kinetoskop
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
černě	černě	k6eAd1	černě
obložená	obložený	k2eAgFnSc1d1	obložená
plechová	plechový	k2eAgFnSc1d1	plechová
chata	chata	k1gFnSc1	chata
s	s	k7c7	s
odklopitelnou	odklopitelný	k2eAgFnSc7d1	odklopitelná
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
točit	točit	k5eAaImF	točit
kolem	kolem	k7c2	kolem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
sluneční	sluneční	k2eAgNnSc1d1	sluneční
světlo	světlo	k1gNnSc1	světlo
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
řízeno	řídit	k5eAaImNgNnS	řídit
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
května	květen	k1gInSc2	květen
1893	[number]	k4	1893
v	v	k7c6	v
brooklynském	brooklynský	k2eAgInSc6d1	brooklynský
Institutu	institut	k1gInSc6	institut
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
Edison	Edison	k1gNnSc4	Edison
provedl	provést	k5eAaPmAgInS	provést
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc4	první
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
demonstraci	demonstrace	k1gFnSc4	demonstrace
filmů	film	k1gInPc2	film
přes	přes	k7c4	přes
kinetoskop	kinetoskop	k1gInSc4	kinetoskop
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
Černé	Černé	k2eAgFnSc6d1	Černé
Marii	Maria	k1gFnSc6	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgInS	představit
sérii	série	k1gFnSc4	série
filmů	film	k1gInPc2	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kovářové	Kovářové	k2eAgNnPc2d1	Kovářové
představení	představení	k1gNnPc2	představení
a	a	k8xC	a
ukázal	ukázat	k5eAaPmAgInS	ukázat
tři	tři	k4xCgMnPc4	tři
lidi	člověk	k1gMnPc4	člověk
předstírající	předstírající	k2eAgMnPc4d1	předstírající
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
kováři	kovář	k1gMnPc1	kovář
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
filmy	film	k1gInPc1	film
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
v	v	k7c6	v
Černé	Černé	k2eAgFnSc6d1	Černé
Marii	Maria	k1gFnSc6	Maria
byly	být	k5eAaImAgInP	být
pod	pod	k7c7	pod
autorským	autorský	k2eAgNnSc7d1	autorské
právem	právo	k1gNnSc7	právo
Dicksona	Dickson	k1gMnSc2	Dickson
a	a	k8xC	a
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
Knihovně	knihovna	k1gFnSc6	knihovna
Kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
1894	[number]	k4	1894
byl	být	k5eAaImAgInS	být
Edisonův	Edisonův	k2eAgInSc1d1	Edisonův
kinetoskopický	kinetoskopický	k2eAgInSc1d1	kinetoskopický
záznam	záznam	k1gInSc1	záznam
kýchnutí	kýchnutí	k1gNnSc2	kýchnutí
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Kýchnutí	kýchnutí	k1gNnSc1	kýchnutí
Freda	Freda	k1gMnSc1	Freda
Otta	Otta	k1gMnSc1	Otta
<g/>
)	)	kIx)	)
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
Dickson	Dickson	k1gInSc1	Dickson
představil	představit	k5eAaPmAgInS	představit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
v	v	k7c6	v
edisonově	edisonův	k2eAgNnSc6d1	edisonův
studiu	studio	k1gNnSc6	studio
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
asistentem	asistent	k1gMnSc7	asistent
Fredem	Fred	k1gMnSc7	Fred
Ottem	Ottem	k?	Ottem
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
pět	pět	k4xCc1	pět
krátkých	krátký	k2eAgInPc2d1	krátký
filmů	film	k1gInPc2	film
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
pro	pro	k7c4	pro
reklamní	reklamní	k2eAgInPc4d1	reklamní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
dříve	dříve	k6eAd2	dříve
patřily	patřit	k5eAaImAgInP	patřit
Fredu	Fred	k1gMnSc3	Fred
Ottovi	Otta	k1gMnSc3	Otta
<g/>
,	,	kIx,	,
zaměstnanci	zaměstnanec	k1gMnSc3	zaměstnanec
Edisona	Edison	k1gMnSc4	Edison
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
komicky	komicky	k6eAd1	komicky
kýchal	kýchat	k5eAaImAgMnS	kýchat
na	na	k7c4	na
kameru	kamera	k1gFnSc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prvních	první	k4xOgInPc2	první
filmů	film	k1gInPc2	film
natočených	natočený	k2eAgInPc2d1	natočený
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
byly	být	k5eAaImAgFnP	být
kouzelnické	kouzelnický	k2eAgNnSc4d1	kouzelnické
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
varieté	varieté	k1gNnSc4	varieté
<g/>
,	,	kIx,	,
herci	herec	k1gMnPc1	herec
z	z	k7c2	z
Buffalo	Buffalo	k1gFnSc2	Buffalo
Bill	Bill	k1gMnSc1	Bill
Wild	Wild	k1gMnSc1	Wild
West	West	k1gMnSc1	West
Show	show	k1gFnSc4	show
<g/>
,	,	kIx,	,
boxerské	boxerský	k2eAgInPc4d1	boxerský
zápasy	zápas	k1gInPc4	zápas
či	či	k8xC	či
kohoutí	kohoutí	k2eAgInPc4d1	kohoutí
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
"	"	kIx"	"
<g/>
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
obrázků	obrázek	k1gInPc2	obrázek
<g/>
"	"	kIx"	"
však	však	k9	však
byly	být	k5eAaImAgInP	být
neupravené	upravený	k2eNgInPc1d1	neupravený
primitivní	primitivní	k2eAgInPc1d1	primitivní
dokumenty	dokument	k1gInPc1	dokument
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgNnPc1d1	domácí
videa	video	k1gNnPc1	video
a	a	k8xC	a
ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
–	–	k?	–
pouliční	pouliční	k2eAgFnSc2d1	pouliční
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc2	práce
policie	policie	k1gFnSc2	policie
nebo	nebo	k8xC	nebo
hasičů	hasič	k1gMnPc2	hasič
či	či	k8xC	či
záběry	záběr	k1gInPc1	záběr
projíždějícího	projíždějící	k2eAgInSc2d1	projíždějící
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Edison	Edisona	k1gFnPc2	Edisona
a	a	k8xC	a
Dickson	Dicksona	k1gFnPc2	Dicksona
také	také	k9	také
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
prototyp	prototyp	k1gInSc4	prototyp
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Kinetofonograf	Kinetofonograf	k1gInSc1	Kinetofonograf
nebo	nebo	k8xC	nebo
Kinetofon	Kinetofon	k1gInSc1	Kinetofon
–	–	k?	–
předchůdce	předchůdce	k1gMnSc1	předchůdce
kinetoskopu	kinetoskop	k1gInSc2	kinetoskop
poskytující	poskytující	k2eAgInSc4d1	poskytující
nesynchronní	synchronní	k2eNgInSc4d1	nesynchronní
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Projektor	projektor	k1gInSc1	projektor
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
ke	k	k7c3	k
gramofonu	gramofon	k1gInSc3	gramofon
s	s	k7c7	s
kladkovým	kladkový	k2eAgInSc7d1	kladkový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stroj	stroj	k1gInSc1	stroj
často	často	k6eAd1	často
nefungoval	fungovat	k5eNaImAgInS	fungovat
správně	správně	k6eAd1	správně
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
ho	on	k3xPp3gNnSc4	on
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
jako	jako	k9	jako
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
u	u	k7c2	u
konkurence	konkurence	k1gFnSc2	konkurence
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
objevovat	objevovat	k5eAaImF	objevovat
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
lepší	dobrý	k2eAgFnSc4d2	lepší
a	a	k8xC	a
spolehlivější	spolehlivý	k2eAgFnSc4d2	spolehlivější
synchronizaci	synchronizace	k1gFnSc4	synchronizace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
známý	známý	k1gMnSc1	známý
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
jediný	jediný	k2eAgInSc1d1	jediný
přeživší	přeživší	k2eAgInSc1d1	přeživší
<g/>
)	)	kIx)	)
film	film	k1gInSc1	film
se	s	k7c7	s
živě	živě	k6eAd1	živě
zaznamenaným	zaznamenaný	k2eAgInSc7d1	zaznamenaný
zvukem	zvuk	k1gInSc7	zvuk
byl	být	k5eAaImAgInS	být
test	test	k1gInSc1	test
kinetofonu	kinetofon	k1gInSc2	kinetofon
–	–	k?	–
celkově	celkově	k6eAd1	celkově
17	[number]	k4	17
<g/>
.	.	kIx.	.
pokus	pokus	k1gInSc1	pokus
Dicksona	Dickson	k1gMnSc2	Dickson
o	o	k7c4	o
zvukový	zvukový	k2eAgInSc4d1	zvukový
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
í	í	k0	í
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
první	první	k4xOgNnSc4	první
komerční	komerční	k2eAgNnSc4d1	komerční
promítání	promítání	k1gNnSc4	promítání
na	na	k7c4	na
newyorské	newyorský	k2eAgFnPc4d1	newyorská
Broadway	Broadwaa	k1gFnPc4	Broadwaa
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvníci	návštěvník	k1gMnPc1	návštěvník
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
25	[number]	k4	25
centů	cent	k1gInPc2	cent
za	za	k7c4	za
vstupné	vstupné	k1gNnSc4	vstupné
a	a	k8xC	a
posadili	posadit	k5eAaPmAgMnP	posadit
se	se	k3xPyFc4	se
do	do	k7c2	do
salónu	salón	k1gInSc2	salón
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
umístěno	umístit	k5eAaPmNgNnS	umístit
pět	pět	k4xCc1	pět
kinetoskopů	kinetoskop	k1gInPc2	kinetoskop
<g/>
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
Griffo	Griffo	k1gMnSc1	Griffo
v.	v.	k?	v.
Battling	Battling	k1gInSc1	Battling
Charles	Charles	k1gMnSc1	Charles
Barnett	Barnetta	k1gFnPc2	Barnetta
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
film	film	k1gInSc1	film
promítaný	promítaný	k2eAgInSc1d1	promítaný
publiku	publikum	k1gNnSc3	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřminutový	čtyřminutový	k2eAgInSc1d1	čtyřminutový
černobílý	černobílý	k2eAgInSc1d1	černobílý
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
Woodvillem	Woodvill	k1gInSc7	Woodvill
Lathamem	Latham	k1gInSc7	Latham
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
Otwayem	Otway	k1gMnSc7	Otway
a	a	k8xC	a
Greyem	Grey	k1gMnSc7	Grey
<g/>
.	.	kIx.	.
</s>
<s>
Hraný	hraný	k2eAgInSc1d1	hraný
boj	boj	k1gInSc1	boj
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
kamerou	kamera	k1gFnSc7	kamera
Eidoloskop	Eidoloskop	k1gInSc1	Eidoloskop
ze	z	k7c2	z
střechy	střecha	k1gFnSc2	střecha
Madison	Madison	k1gInSc1	Madison
Square	square	k1gInSc1	square
Garden	Gardna	k1gFnPc2	Gardna
a	a	k8xC	a
zachycoval	zachycovat	k5eAaImAgInS	zachycovat
australské	australský	k2eAgInPc4d1	australský
boxery	boxer	k1gInPc4	boxer
Alberta	Albert	k1gMnSc2	Albert
Griffithse	Griffiths	k1gMnSc2	Griffiths
a	a	k8xC	a
Charlese	Charles	k1gMnSc2	Charles
Barnetta	Barnett	k1gMnSc2	Barnett
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
přišlo	přijít	k5eAaPmAgNnS	přijít
dohromady	dohromady	k6eAd1	dohromady
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
diváků	divák	k1gMnPc2	divák
na	na	k7c4	na
promítání	promítání	k1gNnSc4	promítání
filmů	film	k1gInPc2	film
Holičství	holičství	k1gNnPc2	holičství
<g/>
,	,	kIx,	,
Kováři	kovář	k1gMnPc1	kovář
<g/>
,	,	kIx,	,
Kohoutí	kohoutí	k2eAgInPc1d1	kohoutí
zápasy	zápas	k1gInPc1	zápas
a	a	k8xC	a
Wrestling	Wrestling	k1gInSc1	Wrestling
<g/>
.	.	kIx.	.
</s>
<s>
Edisonovo	Edisonův	k2eAgNnSc1d1	Edisonovo
filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
filmů	film	k1gInPc2	film
–	–	k?	–
populární	populární	k2eAgFnSc4d1	populární
novou	nový	k2eAgFnSc4d1	nová
formu	forma	k1gFnSc4	forma
zábavy	zábava	k1gFnSc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Kinetoskop	kinetoskop	k1gInSc1	kinetoskop
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
San	San	k1gFnSc4	San
Francisco	Francisco	k6eAd1	Francisco
<g/>
,	,	kIx,	,
Atlantic	Atlantice	k1gFnPc2	Atlantice
City	city	k1gNnSc1	city
a	a	k8xC	a
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kiss	Kissa	k1gFnPc2	Kissa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
se	se	k3xPyFc4	se
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
scéně	scéna	k1gFnSc6	scéna
políbili	políbit	k5eAaPmAgMnP	políbit
May	May	k1gMnSc1	May
Irwinová	Irwinová	k1gFnSc1	Irwinová
a	a	k8xC	a
John	John	k1gMnSc1	John
C.	C.	kA	C.
Rice	Rice	k1gNnSc2	Rice
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
se	se	k3xPyFc4	se
tak	tak	k9	tak
radovat	radovat	k5eAaImF	radovat
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
polibku	polibek	k1gInSc2	polibek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Inovační	inovační	k2eAgMnPc1d1	inovační
francouzští	francouzský	k2eAgMnPc1d1	francouzský
bratři	bratr	k1gMnPc1	bratr
Lumiérové	Lumiérový	k2eAgFnSc2d1	Lumiérový
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
a	a	k8xC	a
Auguste	August	k1gMnSc5	August
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
nazýváni	nazývat	k5eAaImNgMnP	nazývat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otcové	otec	k1gMnPc1	otec
moderního	moderní	k2eAgInSc2d1	moderní
filmu	film	k1gInSc2	film
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
fotografické	fotografický	k2eAgNnSc4d1	fotografické
vybavení	vybavení	k1gNnSc4	vybavení
a	a	k8xC	a
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
se	s	k7c7	s
prací	práce	k1gFnSc7	práce
Edisona	Edison	k1gMnSc2	Edison
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
společnou	společný	k2eAgFnSc4d1	společná
filmovou	filmový	k2eAgFnSc4d1	filmová
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
projektor	projektor	k1gInSc4	projektor
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
ručně	ručně	k6eAd1	ručně
přenosné	přenosný	k2eAgNnSc1d1	přenosné
a	a	k8xC	a
lehké	lehký	k2eAgNnSc1d1	lehké
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mohlo	moct	k5eAaImAgNnS	moct
promítat	promítat	k5eAaImF	promítat
film	film	k1gInSc4	film
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednomu	jeden	k4xCgNnSc3	jeden
diváku	divák	k1gMnSc5	divák
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
jako	jako	k8xC	jako
kinematograf	kinematograf	k1gInSc1	kinematograf
a	a	k8xC	a
patentován	patentován	k2eAgMnSc1d1	patentován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Víceúčelové	víceúčelový	k2eAgNnSc1d1	víceúčelové
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
kombinace	kombinace	k1gFnSc1	kombinace
snímání	snímání	k1gNnSc2	snímání
<g/>
,	,	kIx,	,
kopírování	kopírování	k1gNnSc2	kopírování
a	a	k8xC	a
promítání	promítání	k1gNnSc2	promítání
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
výhodnější	výhodný	k2eAgMnSc1d2	výhodnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgMnSc1	jeden
divák	divák	k1gMnSc1	divák
mohl	moct	k5eAaImAgMnS	moct
sledovat	sledovat	k5eAaImF	sledovat
film	film	k1gInSc4	film
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
plátnu	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Použit	použít	k5eAaPmNgInS	použít
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
šířky	šířka	k1gFnSc2	šířka
35	[number]	k4	35
mm	mm	kA	mm
a	a	k8xC	a
rychlost	rychlost	k1gFnSc1	rychlost
16	[number]	k4	16
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
fps	fps	k?	fps
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
24	[number]	k4	24
fps	fps	k?	fps
stala	stát	k5eAaPmAgFnS	stát
standardem	standard	k1gInSc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
veřejná	veřejný	k2eAgFnSc1d1	veřejná
zkouška	zkouška	k1gFnSc1	zkouška
a	a	k8xC	a
demonstrace	demonstrace	k1gFnSc1	demonstrace
lumiérové	lumiérový	k2eAgFnSc2d1	lumiérový
kamery	kamera	k1gFnSc2	kamera
(	(	kIx(	(
<g/>
kinematografu	kinematograf	k1gInSc2	kinematograf
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1895	[number]	k4	1895
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
Lumiérů	Lumiér	k1gInPc2	Lumiér
<g/>
.	.	kIx.	.
</s>
<s>
Způsobili	způsobit	k5eAaPmAgMnP	způsobit
senzaci	senzace	k1gFnSc4	senzace
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
prvnímu	první	k4xOgInSc3	první
filmu	film	k1gInSc2	film
Dělníci	dělník	k1gMnPc1	dělník
opouštějí	opouštět	k5eAaImIp3nP	opouštět
továrnu	továrna	k1gFnSc4	továrna
Lumiére	Lumiér	k1gMnSc5	Lumiér
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Sortie	Sortie	k1gFnSc2	Sortie
des	des	k1gNnSc2	des
Ouviers	Ouviers	k1gInSc1	Ouviers
de	de	k?	de
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Usine	Usin	k1gMnSc5	Usin
Lumiere	Lumier	k1gMnSc5	Lumier
Lyon	Lyon	k1gInSc1	Lyon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ten	ten	k3xDgMnSc1	ten
zobrazoval	zobrazovat	k5eAaImAgMnS	zobrazovat
pouze	pouze	k6eAd1	pouze
každodenní	každodenní	k2eAgFnSc4d1	každodenní
rutinu	rutina	k1gFnSc4	rutina
–	–	k?	–
dělníci	dělník	k1gMnPc1	dělník
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
opouštějí	opouštět	k5eAaImIp3nP	opouštět
bránou	brána	k1gFnSc7	brána
továrnu	továrna	k1gFnSc4	továrna
Lumiére	Lumiér	k1gMnSc5	Lumiér
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgInSc1d1	známý
pojem	pojem	k1gInSc1	pojem
kino	kino	k1gNnSc1	kino
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
odvozené	odvozený	k2eAgNnSc1d1	odvozené
z	z	k7c2	z
kinematograf	kinematograf	k1gInSc1	kinematograf
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zrodil	zrodit	k5eAaPmAgInS	zrodit
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1895	[number]	k4	1895
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Lumiérové	Lumiérový	k2eAgFnSc2d1	Lumiérový
představili	představit	k5eAaPmAgMnP	představit
první	první	k4xOgNnPc4	první
komerční	komerční	k2eAgNnPc4d1	komerční
představení	představení	k1gNnPc4	představení
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
biografu	biograf	k1gInSc6	biograf
–	–	k?	–
ve	v	k7c6	v
sklepních	sklepní	k2eAgFnPc6d1	sklepní
prostorách	prostora	k1gFnPc6	prostora
Grand	grand	k1gMnSc1	grand
Café	café	k1gNnSc2	café
na	na	k7c6	na
Bulváru	bulvár	k1gInSc6	bulvár
Kapucínů	kapucín	k1gMnPc2	kapucín
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Dvacetiminutový	dvacetiminutový	k2eAgInSc1d1	dvacetiminutový
program	program	k1gInSc1	program
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
deset	deset	k4xCc4	deset
krátkých	krátký	k2eAgInPc2d1	krátký
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
filmů	film	k1gInPc2	film
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
dvaceti	dvacet	k4xCc7	dvacet
reprízami	repríza	k1gFnPc7	repríza
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
krátkometrážní	krátkometrážní	k2eAgInPc1d1	krátkometrážní
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
mini-dokumenty	miniokument	k1gInPc1	mini-dokument
<g/>
)	)	kIx)	)
nazvali	nazvat	k5eAaPmAgMnP	nazvat
jako	jako	k9	jako
aktuality	aktualita	k1gFnSc2	aktualita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
"	"	kIx"	"
<g/>
domácí	domácí	k2eAgInPc4d1	domácí
<g/>
"	"	kIx"	"
filmy	film	k1gInPc1	film
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
La	la	k1gNnSc4	la
Sortie	Sortie	k1gFnSc2	Sortie
des	des	k1gNnSc2	des
Ouviers	Ouviers	k1gInSc1	Ouviers
de	de	k?	de
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Usine	Usin	k1gMnSc5	Usin
Lumiè	Lumiè	k1gMnSc5	Lumiè
à	à	k?	à
Lyon	Lyon	k1gInSc1	Lyon
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Dělníci	dělník	k1gMnPc1	dělník
opouštějí	opouštět	k5eAaImIp3nP	opouštět
továrnu	továrna	k1gFnSc4	továrna
Lumiére	Lumiér	k1gMnSc5	Lumiér
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
46	[number]	k4	46
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
La	la	k1gNnPc1	la
Voltige	Voltige	k1gFnPc2	Voltige
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Skok	skok	k1gInSc1	skok
na	na	k7c4	na
koně	kůň	k1gMnSc4	kůň
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
46	[number]	k4	46
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
La	la	k1gNnSc1	la
Pê	Pê	k1gFnSc2	Pê
aux	aux	k?	aux
Poissons	Poissons	k1gInSc1	Poissons
Rouges	Rouges	k1gInSc1	Rouges
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Rybolov	rybolov	k1gInSc1	rybolov
zlaté	zlatý	k2eAgFnSc2d1	zlatá
rybky	rybka	k1gFnSc2	rybka
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
42	[number]	k4	42
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
Le	Le	k1gMnSc1	Le
Débarquement	Débarquement	k1gMnSc1	Débarquement
du	du	k?	du
Congrè	Congrè	k1gMnSc1	Congrè
de	de	k?	de
Photographie	Photographie	k1gFnSc2	Photographie
à	à	k?	à
Lyon	Lyon	k1gInSc1	Lyon
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Vylodění	vylodění	k1gNnSc1	vylodění
kongresu	kongres	k1gInSc2	kongres
fotografů	fotograf	k1gMnPc2	fotograf
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
48	[number]	k4	48
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
Forgerons	Forgerons	k1gInSc1	Forgerons
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Kováři	kovář	k1gMnPc1	kovář
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
49	[number]	k4	49
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
Le	Le	k1gMnSc1	Le
Jardinier	Jardinier	k1gMnSc1	Jardinier
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Pokropený	pokropený	k2eAgMnSc1d1	pokropený
kropič	kropič	k1gMnSc1	kropič
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Zahradník	Zahradník	k1gMnSc1	Zahradník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
49	[number]	k4	49
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
Le	Le	k1gMnSc1	Le
Repas	Repas	k1gMnSc1	Repas
(	(	kIx(	(
<g/>
de	de	k?	de
Bébé	Bébá	k1gFnSc2	Bébá
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Krmení	krmení	k1gNnSc1	krmení
dítěte	dítě	k1gNnSc2	dítě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
41	[number]	k4	41
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
Le	Le	k1gFnSc1	Le
Saut	Sauta	k1gFnPc2	Sauta
à	à	k?	à
la	la	k1gNnSc4	la
Couverture	Couvertur	k1gMnSc5	Couvertur
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Salta	salto	k1gNnPc4	salto
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
dece	deka	k1gFnSc6	deka
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
41	[number]	k4	41
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
La	la	k1gNnSc1	la
Place	plac	k1gInSc6	plac
des	des	k1gNnSc1	des
Cordeliers	Cordeliers	k1gInSc1	Cordeliers
à	à	k?	à
Lyon	Lyon	k1gInSc1	Lyon
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Náměstí	náměstí	k1gNnSc1	náměstí
Cordeliers	Cordeliersa	k1gFnPc2	Cordeliersa
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
44	[number]	k4	44
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
La	la	k1gNnPc1	la
Mer	Mer	k1gFnPc2	Mer
(	(	kIx(	(
<g/>
Baignade	Baignad	k1gInSc5	Baignad
en	en	k?	en
Mer	Mer	k1gMnPc7	Mer
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Koupání	koupání	k1gNnSc1	koupání
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
38	[number]	k4	38
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
Nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
první	první	k4xOgFnSc2	první
komedie	komedie	k1gFnSc2	komedie
se	s	k7c7	s
zahradníkem	zahradník	k1gMnSc7	zahradník
a	a	k8xC	a
zavlažovací	zavlažovací	k2eAgFnSc7d1	zavlažovací
hadicí	hadice	k1gFnSc7	hadice
(	(	kIx(	(
<g/>
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
Pokropený	pokropený	k2eAgMnSc1d1	pokropený
kropič	kropič	k1gMnSc1	kropič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dělníci	dělník	k1gMnPc1	dělník
opouštějí	opouštět	k5eAaImIp3nP	opouštět
továrnu	továrna	k1gFnSc4	továrna
Lumiére	Lumiér	k1gMnSc5	Lumiér
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
(	(	kIx(	(
<g/>
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
táhnoucí	táhnoucí	k2eAgInSc4d1	táhnoucí
kočár	kočár	k1gInSc4	kočár
přibližující	přibližující	k2eAgNnSc4d1	přibližující
se	se	k3xPyFc4	se
ke	k	k7c3	k
kameře	kamera	k1gFnSc3	kamera
(	(	kIx(	(
<g/>
č.	č.	k?	č.
9	[number]	k4	9
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
krmení	krmení	k1gNnSc1	krmení
dítěte	dítě	k1gNnSc2	dítě
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
č.	č.	k?	č.
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lumiérové	Lumiérový	k2eAgNnSc1d1	Lumiérový
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
stali	stát	k5eAaPmAgMnP	stát
slavnými	slavný	k2eAgInPc7d1	slavný
díky	dík	k1gInPc7	dík
jejich	jejich	k3xOp3gInSc1	jejich
52	[number]	k4	52
<g/>
.	.	kIx.	.
dokumentu	dokument	k1gInSc2	dokument
s	s	k7c7	s
názvem	název	k1gInSc7	název
Arrivée	Arrivé	k1gInSc2	Arrivé
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
train	train	k1gInSc1	train
en	en	k?	en
gare	garat	k5eAaPmIp3nS	garat
La	la	k1gNnSc4	la
Ciotat	Ciotat	k1gFnSc2	Ciotat
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Příjezd	příjezd	k1gInSc1	příjezd
vlaku	vlak	k1gInSc2	vlak
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
la	la	k1gNnSc2	la
Ciotat	Ciotat	k1gFnSc2	Ciotat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
zděšení	zděšení	k1gNnPc2	zděšení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vlak	vlak	k1gInSc1	vlak
zdánlivě	zdánlivě	k6eAd1	zdánlivě
najížděl	najíždět	k5eAaImAgInS	najíždět
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgInPc1d1	moderní
výzkumy	výzkum	k1gInPc1	výzkum
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
"	"	kIx"	"
<g/>
vyděšených	vyděšený	k2eAgMnPc2d1	vyděšený
diváků	divák	k1gMnPc2	divák
<g/>
"	"	kIx"	"
vyvracejí	vyvracet	k5eAaImIp3nP	vyvracet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
veleúspěšných	veleúspěšný	k2eAgMnPc2d1	veleúspěšný
bratrů	bratr	k1gMnPc2	bratr
Lumiérů	Lumiér	k1gInPc2	Lumiér
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
proslavil	proslavit	k5eAaPmAgMnS	proslavit
také	také	k9	také
filmař	filmař	k1gMnSc1	filmař
Georges	Georges	k1gMnSc1	Georges
Méliè	Méliè	k1gMnSc1	Méliè
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pomohl	pomoct	k5eAaPmAgMnS	pomoct
rozšířit	rozšířit	k5eAaPmF	rozšířit
filmovou	filmový	k2eAgFnSc4d1	filmová
kinematografii	kinematografie	k1gFnSc4	kinematografie
svými	svůj	k3xOyFgInPc7	svůj
imaginativními	imaginativní	k2eAgInPc7d1	imaginativní
fantasy	fantas	k1gInPc7	fantas
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Lumiérové	Lumiérový	k2eAgMnPc4d1	Lumiérový
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
prodat	prodat	k5eAaPmF	prodat
jejich	jejich	k3xOp3gInSc4	jejich
kinematograf	kinematograf	k1gInSc4	kinematograf
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vyrobit	vyrobit	k5eAaPmF	vyrobit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kameru	kamera	k1gFnSc4	kamera
a	a	k8xC	a
poté	poté	k6eAd1	poté
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
první	první	k4xOgMnSc1	první
filmový	filmový	k2eAgInSc1d1	filmový
ateliér	ateliér	k1gInSc1	ateliér
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgNnPc2d1	další
15	[number]	k4	15
let	léto	k1gNnPc2	léto
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
asi	asi	k9	asi
500	[number]	k4	500
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
jen	jen	k9	jen
pár	pár	k4xCyI	pár
"	"	kIx"	"
<g/>
přežilo	přežít	k5eAaPmAgNnS	přežít
<g/>
"	"	kIx"	"
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
promítal	promítat	k5eAaImAgMnS	promítat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
filmovou	filmový	k2eAgFnSc7d1	filmová
společností	společnost	k1gFnSc7	společnost
Pathe	Path	k1gFnSc2	Path
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
financovala	financovat	k5eAaBmAgFnS	financovat
a	a	k8xC	a
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
skončil	skončit	k5eAaPmAgInS	skončit
s	s	k7c7	s
podnikáním	podnikání	k1gNnSc7	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Iluzionista	iluzionista	k1gMnSc1	iluzionista
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
mág	mág	k1gMnSc1	mág
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
na	na	k7c4	na
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
<g/>
,	,	kIx,	,
Meliés	Meliés	k1gInSc1	Meliés
využil	využít	k5eAaPmAgInS	využít
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
14	[number]	k4	14
<g/>
minutovou	minutový	k2eAgFnSc4d1	minutová
sci-fi	scii	k1gFnSc4	sci-fi
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc4	Le
Voyage	Voyag	k1gInSc2	Voyag
dans	dans	k1gInSc4	dans
la	la	k1gNnSc2	la
Lune	Lun	k1gFnSc2	Lun
–	–	k?	–
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
neslavnější	slavný	k2eNgFnSc1d2	neslavnější
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
byly	být	k5eAaImAgInP	být
surrealistické	surrealistický	k2eAgInPc1d1	surrealistický
speciální	speciální	k2eAgInPc1d1	speciální
efekty	efekt	k1gInPc1	efekt
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
památného	památný	k2eAgInSc2d1	památný
obrazu	obraz	k1gInSc2	obraz
Měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zapíchnutou	zapíchnutý	k2eAgFnSc7d1	zapíchnutá
<g/>
"	"	kIx"	"
kapslí	kapsle	k1gFnSc7	kapsle
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
<g/>
.	.	kIx.	.
</s>
<s>
Meliés	Meliés	k6eAd1	Meliés
také	také	k9	také
do	do	k7c2	do
filmu	film	k1gInSc2	film
představil	představit	k5eAaPmAgInS	představit
myšlenku	myšlenka	k1gFnSc4	myšlenka
příběhového	příběhový	k2eAgInSc2d1	příběhový
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
zápletky	zápletka	k1gFnSc2	zápletka
<g/>
,	,	kIx,	,
vývoje	vývoj	k1gInSc2	vývoj
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
iluze	iluze	k1gFnSc2	iluze
a	a	k8xC	a
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
fotografických	fotografický	k2eAgInPc2d1	fotografický
triků	trik	k1gInPc2	trik
(	(	kIx(	(
<g/>
počátek	počátek	k1gInSc4	počátek
speciálních	speciální	k2eAgInPc2d1	speciální
efektů	efekt	k1gInPc2	efekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvojitých	dvojitý	k2eAgFnPc2d1	dvojitá
expozicí	expozice	k1gFnPc2	expozice
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
,	,	kIx,	,
zastavení	zastavení	k1gNnSc1	zastavení
či	či	k8xC	či
zpomalení	zpomalení	k1gNnSc1	zpomalení
pohybu	pohyb	k1gInSc2	pohyb
nebo	nebo	k8xC	nebo
také	také	k9	také
ztlumení	ztlumení	k1gNnSc2	ztlumení
<g/>
/	/	kIx~	/
<g/>
zesílení	zesílení	k1gNnSc2	zesílení
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
použití	použití	k1gNnSc1	použití
jeho	jeho	k3xOp3gFnSc2	jeho
kamery	kamera	k1gFnSc2	kamera
inovativní	inovativní	k2eAgFnSc1d1	inovativní
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
kamera	kamera	k1gFnSc1	kamera
stále	stále	k6eAd1	stále
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
postupný	postupný	k2eAgInSc4d1	postupný
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgNnSc7d1	klíčové
obdobím	období	k1gNnSc7	období
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
kinematografie	kinematografie	k1gFnSc2	kinematografie
v	v	k7c6	v
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
začátek	začátek	k1gInSc4	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Edisonova	Edisonův	k2eAgFnSc1d1	Edisonova
společnost	společnost	k1gFnSc1	společnost
soupeřila	soupeřit	k5eAaImAgFnS	soupeřit
s	s	k7c7	s
konkurenty	konkurent	k1gMnPc7	konkurent
–	–	k?	–
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
filmové	filmový	k2eAgFnSc2d1	filmová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnSc2d1	velká
průkopnické	průkopnický	k2eAgFnSc2d1	průkopnická
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
pod	pod	k7c4	pod
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
těchto	tento	k3xDgFnPc2	tento
společností	společnost	k1gFnPc2	společnost
<g/>
:	:	kIx,	:
Edison	Edison	k1gInSc1	Edison
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Company	Compana	k1gFnSc2	Compana
–	–	k?	–
začala	začít	k5eAaPmAgFnS	začít
produkovat	produkovat	k5eAaImF	produkovat
filmy	film	k1gInPc4	film
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
pro	pro	k7c4	pro
Edisonův	Edisonův	k2eAgInSc4d1	Edisonův
vynález	vynález	k1gInSc4	vynález
kinetoskop	kinetoskop	k1gInSc1	kinetoskop
<g/>
.	.	kIx.	.
</s>
<s>
Centrála	centrála	k1gFnSc1	centrála
a	a	k8xC	a
výrobní	výrobní	k2eAgNnPc1d1	výrobní
zařízení	zařízení	k1gNnPc1	zařízení
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
ve	v	k7c6	v
West	Westa	k1gFnPc2	Westa
Orange	Orang	k1gInSc2	Orang
<g/>
,	,	kIx,	,
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Edison	Edison	k1gMnSc1	Edison
intenzivně	intenzivně	k6eAd1	intenzivně
bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c4	za
monopol	monopol	k1gInSc4	monopol
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gMnPc2	jeho
<g/>
"	"	kIx"	"
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
nebo	nebo	k8xC	nebo
vykupoval	vykupovat	k5eAaImAgInS	vykupovat
patenty	patent	k1gInPc4	patent
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Selig	Selig	k1gMnSc1	Selig
Polyscope	Polyscop	k1gInSc5	Polyscop
Company	Compana	k1gFnPc4	Compana
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
WN	WN	kA	WN
Selig	Selig	k1gInSc1	Selig
Company	Compan	k1gInPc1	Compan
<g/>
)	)	kIx)	)
–	–	k?	–
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc7	Illinois
plukovníkem	plukovník	k1gMnSc7	plukovník
Williamem	William	k1gInSc7	William
Seligem	Selig	k1gMnSc7	Selig
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
specializovala	specializovat	k5eAaBmAgFnS	specializovat
na	na	k7c4	na
grotesky	grotesk	k1gInPc4	grotesk
<g/>
,	,	kIx,	,
historické	historický	k2eAgInPc4d1	historický
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc1	cestování
<g/>
,	,	kIx,	,
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
westernech	western	k1gInPc6	western
objevila	objevit	k5eAaPmAgFnS	objevit
filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
Tom	Tom	k1gMnSc1	Tom
Mix	mix	k1gInSc1	mix
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gMnSc1	American
Vitagraph	Vitagraph	k1gMnSc1	Vitagraph
Company	Compana	k1gFnSc2	Compana
–	–	k?	–
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
rodáky	rodák	k1gMnPc7	rodák
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
J.	J.	kA	J.
Stuart	Stuart	k1gInSc4	Stuart
Blacktonem	Blackton	k1gInSc7	Blackton
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
E.	E.	kA	E.
Smithem	Smith	k1gInSc7	Smith
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
fikční	fikční	k2eAgInSc4d1	fikční
film	film	k1gInSc4	film
společnosti	společnost	k1gFnSc2	společnost
byl	být	k5eAaImAgMnS	být
The	The	k1gMnSc1	The
Burglar	Burglar	k1gMnSc1	Burglar
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Roof	Roof	k1gInSc1	Roof
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
natočen	natočit	k5eAaBmNgInS	natočit
a	a	k8xC	a
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
největší	veliký	k2eAgFnSc7d3	veliký
filmovou	filmový	k2eAgFnSc7d1	filmová
společností	společnost	k1gFnSc7	společnost
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
200	[number]	k4	200
filmů	film	k1gInPc2	film
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gMnSc1	American
Mutoscope	Mutoscop	k1gInSc5	Mutoscop
Company	Compan	k1gMnPc7	Compan
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
City	City	k1gFnPc2	City
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Edisonovým	Edisonův	k2eAgMnSc7d1	Edisonův
pracovníkem	pracovník	k1gMnSc7	pracovník
Williamem	William	k1gInSc7	William
Dicksonem	Dickson	k1gMnSc7	Dickson
<g/>
,	,	kIx,	,
Hermanem	Herman	k1gMnSc7	Herman
Caslerem	Casler	k1gMnSc7	Casler
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Marvinem	Marvin	k1gInSc7	Marvin
a	a	k8xC	a
vynálezcem	vynálezce	k1gMnSc7	vynálezce
kapesního	kapesní	k2eAgInSc2d1	kapesní
zapalovače	zapalovač	k1gInSc2	zapalovač
Eliasem	Elias	k1gMnSc7	Elias
Koopmanem	Koopman	k1gMnSc7	Koopman
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgInSc1	první
filmový	filmový	k2eAgInSc1d1	filmový
stroj	stroj	k1gInSc1	stroj
byl	být	k5eAaImAgInS	být
Mutoskop	Mutoskop	k1gInSc4	Mutoskop
–	–	k?	–
zařízení	zařízení	k1gNnSc2	zařízení
velikostí	velikost	k1gFnPc2	velikost
podobné	podobný	k2eAgFnSc2d1	podobná
kinetoskopu	kinetoskop	k1gInSc6	kinetoskop
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
použití	použití	k1gNnSc2	použití
filmu	film	k1gInSc2	film
využili	využít	k5eAaPmAgMnP	využít
rotujících	rotující	k2eAgFnPc2d1	rotující
fotografií	fotografia	k1gFnPc2	fotografia
namontovaných	namontovaný	k2eAgFnPc2d1	namontovaná
na	na	k7c6	na
bubnu	buben	k1gInSc6	buben
uvnitř	uvnitř	k7c2	uvnitř
skříně	skříň	k1gFnSc2	skříň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyvolávaly	vyvolávat	k5eAaImAgFnP	vyvolávat
dojem	dojem	k1gInSc4	dojem
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
zkonstruování	zkonstruování	k1gNnSc1	zkonstruování
projektoru	projektor	k1gInSc2	projektor
–	–	k?	–
projektor	projektor	k1gInSc1	projektor
Biograf	biograf	k1gInSc1	biograf
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
demonstrován	demonstrovat	k5eAaBmNgInS	demonstrovat
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
promítala	promítat	k5eAaImAgFnS	promítat
svůj	svůj	k3xOyFgInSc4	svůj
film	film	k1gInSc4	film
pro	pro	k7c4	pro
americké	americký	k2eAgMnPc4d1	americký
diváky	divák	k1gMnPc4	divák
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
také	také	k9	také
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kameru	kamera	k1gFnSc4	kamera
s	s	k7c7	s
názvem	název	k1gInSc7	název
Mutograf	Mutograf	k1gInSc1	Mutograf
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
nazvaným	nazvaný	k2eAgInSc7d1	nazvaný
Biograf	biograf	k1gMnSc1	biograf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nevyužívala	využívat	k5eNaImAgFnS	využívat
ozubených	ozubený	k2eAgNnPc2d1	ozubené
kol	kolo	k1gNnPc2	kolo
nebo	nebo	k8xC	nebo
perforací	perforace	k1gFnPc2	perforace
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
film	film	k1gInSc4	film
poprvé	poprvé	k6eAd1	poprvé
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
Mutoskope	Mutoskop	k1gInSc5	Mutoskop
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stala	stát	k5eAaPmAgFnS	stát
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
filmovou	filmový	k2eAgFnSc7d1	filmová
společností	společnost	k1gFnSc7	společnost
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
American	American	k1gInSc4	American
Mutoscope	Mutoscop	k1gInSc5	Mutoscop
and	and	k?	and
Biograph	Biograph	k1gInSc4	Biograph
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Slavnými	slavný	k2eAgInPc7d1	slavný
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
především	především	k9	především
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
První	první	k4xOgNnSc1	první
natáčení	natáčení	k1gNnSc1	natáčení
papeže	papež	k1gMnSc2	papež
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
První	první	k4xOgFnSc1	první
výrobní	výrobní	k2eAgFnSc1d1	výrobní
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
kontrakt	kontrakt	k1gInSc4	kontrakt
s	s	k7c7	s
Bílým	bílý	k2eAgInSc7d1	bílý
domem	dům	k1gInSc7	dům
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
a	a	k8xC	a
první	první	k4xOgNnSc4	první
studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
film	film	k1gInSc4	film
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Williamem	William	k1gInSc7	William
McKinleyem	McKinleyem	k1gInSc1	McKinleyem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
založili	založit	k5eAaPmAgMnP	založit
první	první	k4xOgNnSc4	první
filmové	filmový	k2eAgNnSc4d1	filmové
studio	studio	k1gNnSc4	studio
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spoléhající	spoléhající	k2eAgMnSc1d1	spoléhající
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
umělé	umělý	k2eAgNnSc4d1	umělé
světlo	světlo	k1gNnSc4	světlo
Jsou	být	k5eAaImIp3nP	být
tvůrci	tvůrce	k1gMnPc1	tvůrce
prvního	první	k4xOgInSc2	první
westernového	westernový	k2eAgInSc2d1	westernový
filmu	film	k1gInSc2	film
natočeného	natočený	k2eAgInSc2d1	natočený
a	a	k8xC	a
představeného	představený	k1gMnSc2	představený
na	na	k7c6	na
západě	západ	k1gInSc6	západ
USA	USA	kA	USA
<g/>
,	,	kIx,	,
California	Californium	k1gNnSc2	Californium
Hold	hold	k1gInSc4	hold
Up	Up	k1gFnSc2	Up
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
se	se	k3xPyFc4	se
Florence	Florenc	k1gFnPc1	Florenc
<g />
.	.	kIx.	.
</s>
<s>
Lawrence	Lawrence	k1gFnSc1	Lawrence
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgInSc4	první
"	"	kIx"	"
<g/>
filmovou	filmový	k2eAgFnSc7d1	filmová
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
"	"	kIx"	"
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
první	první	k4xOgFnSc7	první
hlavní	hlavní	k2eAgFnSc7d1	hlavní
filmovou	filmový	k2eAgFnSc7d1	filmová
společností	společnost	k1gFnSc7	společnost
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
natočila	natočit	k5eAaBmAgFnS	natočit
film	film	k1gInSc4	film
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
–	–	k?	–
Daring	Daring	k1gInSc1	Daring
Hold-Up	Hold-Up	k1gMnSc1	Hold-Up
in	in	k?	in
Southern	Southern	k1gMnSc1	Southern
California	Californium	k1gNnSc2	Californium
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Tvůrci	tvůrce	k1gMnPc1	tvůrce
prvního	první	k4xOgInSc2	první
filmu	film	k1gInSc2	film
natočeném	natočený	k2eAgMnSc6d1	natočený
konkrétně	konkrétně	k6eAd1	konkrétně
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
severně	severně	k6eAd1	severně
od	od	k7c2	od
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
"	"	kIx"	"
–	–	k?	–
latinské	latinský	k2eAgNnSc1d1	latinské
melodrama	melodrama	k?	melodrama
s	s	k7c7	s
názvem	název	k1gInSc7	název
In	In	k1gFnPc2	In
Old	Olda	k1gFnPc2	Olda
California	Californium	k1gNnSc2	Californium
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
Tvůrci	tvůrce	k1gMnPc1	tvůrce
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
epos	epos	k1gInSc4	epos
Judith	Juditha	k1gFnPc2	Juditha
z	z	k7c2	z
Bethulia	Bethulium	k1gNnSc2	Bethulium
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
americké	americký	k2eAgFnSc2d1	americká
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
byly	být	k5eAaImAgFnP	být
doprovázeny	doprovázet	k5eAaImNgFnP	doprovázet
záplavou	záplava	k1gFnSc7	záplava
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
brzdil	brzdit	k5eAaImAgInS	brzdit
vývoj	vývoj	k1gInSc1	vývoj
boj	boj	k1gInSc4	boj
o	o	k7c4	o
patenty	patent	k1gInPc4	patent
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
desetileté	desetiletý	k2eAgNnSc1d1	desetileté
kolo	kolo	k1gNnSc1	kolo
právních	právní	k2eAgInPc2d1	právní
sporů	spor	k1gInPc2	spor
probíhalo	probíhat	k5eAaImAgNnS	probíhat
mezi	mezi	k7c7	mezi
Edisonem	Edison	k1gMnSc7	Edison
vedeným	vedený	k2eAgMnSc7d1	vedený
filmovým	filmový	k2eAgInSc7d1	filmový
kartelem	kartel	k1gInSc7	kartel
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Patents	Patents	k1gInSc1	Patents
Company	Compan	k1gInPc1	Compan
(	(	kIx(	(
<g/>
MPPC	MPPC	kA	MPPC
<g/>
)	)	kIx)	)
a	a	k8xC	a
nezávislými	závislý	k2eNgMnPc7d1	nezávislý
producenty	producent	k1gMnPc7	producent
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
monopolizaci	monopolizace	k1gFnSc4	monopolizace
však	však	k9	však
ztroskotaly	ztroskotat	k5eAaPmAgFnP	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
provozovatelů	provozovatel	k1gMnPc2	provozovatel
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
znali	znát	k5eAaImAgMnP	znát
lépe	dobře	k6eAd2	dobře
vkus	vkus	k1gInSc4	vkus
publika	publikum	k1gNnSc2	publikum
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
přechodem	přechod	k1gInSc7	přechod
k	k	k7c3	k
dlouhým	dlouhý	k2eAgInPc3d1	dlouhý
hlavním	hlavní	k2eAgInPc3d1	hlavní
filmům	film	k1gInPc3	film
a	a	k8xC	a
zavedením	zavedení	k1gNnSc7	zavedení
systému	systém	k1gInSc2	systém
filmových	filmový	k2eAgFnPc2d1	filmová
hvězd	hvězda	k1gFnPc2	hvězda
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
účinnější	účinný	k2eAgInSc4d2	účinnější
koncept	koncept	k1gInSc4	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Produkovali	produkovat	k5eAaImAgMnP	produkovat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
první	první	k4xOgFnSc2	první
americké	americký	k2eAgFnSc2d1	americká
gagové	gagový	k2eAgFnSc2d1	Gagová
komedie	komedie	k1gFnSc2	komedie
a	a	k8xC	a
filmové	filmový	k2eAgInPc4d1	filmový
eposy	epos	k1gInPc4	epos
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kartel	kartel	k1gInSc1	kartel
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
dobru	dobro	k1gNnSc3	dobro
přičíst	přičíst	k5eAaPmF	přičíst
vytvoření	vytvoření	k1gNnSc4	vytvoření
westernu	western	k1gInSc2	western
<g/>
.	.	kIx.	.
</s>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nejprve	nejprve	k6eAd1	nejprve
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
svou	svůj	k3xOyFgFnSc4	svůj
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vzdálili	vzdálit	k5eAaPmAgMnP	vzdálit
kontrole	kontrola	k1gFnSc3	kontrola
MPPC	MPPC	kA	MPPC
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
etabloval	etablovat	k5eAaBmAgMnS	etablovat
jako	jako	k9	jako
nové	nový	k2eAgNnSc4d1	nové
produkční	produkční	k2eAgNnSc4d1	produkční
centrum	centrum	k1gNnSc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgNnSc1d1	dobré
počasí	počasí	k1gNnSc1	počasí
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
krajiny	krajina	k1gFnSc2	krajina
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
nabízely	nabízet	k5eAaImAgFnP	nabízet
optimální	optimální	k2eAgFnPc1d1	optimální
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
vyrobeno	vyroben	k2eAgNnSc1d1	vyrobeno
50	[number]	k4	50
<g/>
%	%	kIx~	%
světově	světově	k6eAd1	světově
provozovaných	provozovaný	k2eAgInPc2d1	provozovaný
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Edisonovy	Edisonův	k2eAgFnSc2d1	Edisonova
společnosti	společnost	k1gFnSc2	společnost
obchodník	obchodník	k1gMnSc1	obchodník
Edwin	Edwin	k1gMnSc1	Edwin
S.	S.	kA	S.
Porter	porter	k1gInSc1	porter
<g/>
.	.	kIx.	.
</s>
<s>
Následcích	následek	k1gInPc6	následek
jedenáct	jedenáct	k4xCc4	jedenáct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
filmařem	filmař	k1gMnSc7	filmař
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Porter	porter	k1gInSc1	porter
se	se	k3xPyFc4	se
zaučil	zaučit	k5eAaPmAgInS	zaučit
natáčením	natáčení	k1gNnSc7	natáčení
zpravodajských	zpravodajský	k2eAgMnPc2d1	zpravodajský
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
natočil	natočit	k5eAaBmAgMnS	natočit
dva	dva	k4xCgInPc4	dva
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
amerického	americký	k2eAgMnSc2d1	americký
hasiče	hasič	k1gMnSc2	hasič
a	a	k8xC	a
Velkou	velký	k2eAgFnSc4d1	velká
vlakovou	vlakový	k2eAgFnSc4d1	vlaková
loupež	loupež	k1gFnSc4	loupež
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
milníky	milník	k1gInPc4	milník
filmových	filmový	k2eAgFnPc2d1	filmová
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Porter	porter	k1gInSc1	porter
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
plynulým	plynulý	k2eAgInSc7d1	plynulý
paralelním	paralelní	k2eAgInSc7d1	paralelní
střihem	střih	k1gInSc7	střih
(	(	kIx(	(
<g/>
pochopitelně	pochopitelně	k6eAd1	pochopitelně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
"	"	kIx"	"
<g/>
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
"	"	kIx"	"
filmového	filmový	k2eAgInSc2d1	filmový
střihu	střih	k1gInSc2	střih
<g/>
)	)	kIx)	)
a	a	k8xC	a
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgInPc4	dva
tyto	tento	k3xDgInPc4	tento
filmy	film	k1gInPc4	film
jsou	být	k5eAaImIp3nP	být
invenční	invenční	k2eAgMnPc1d1	invenční
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
pověst	pověst	k1gFnSc4	pověst
jako	jako	k8xC	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
otců	otec	k1gMnPc2	otec
filmové	filmový	k2eAgFnSc2d1	filmová
techniky	technika	k1gFnSc2	technika
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
první	první	k4xOgMnSc1	první
vážný	vážný	k2eAgInSc1d1	vážný
problém	problém	k1gInSc1	problém
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
filmové	filmový	k2eAgFnSc2d1	filmová
estetiky	estetika	k1gFnSc2	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Porterovy	Porterův	k2eAgInPc1d1	Porterův
pozdější	pozdní	k2eAgInPc1d2	pozdější
filmy	film	k1gInPc1	film
nikdy	nikdy	k6eAd1	nikdy
nenaplnily	naplnit	k5eNaPmAgInP	naplnit
slibné	slibný	k2eAgInPc1d1	slibný
začátky	začátek	k1gInPc1	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jiní	jiný	k2eAgMnPc1d1	jiný
filmaři	filmař	k1gMnPc1	filmař
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
projevili	projevit	k5eAaPmAgMnP	projevit
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
právě	právě	k9	právě
takovou	takový	k3xDgFnSc4	takový
plynulost	plynulost	k1gFnSc4	plynulost
<g/>
.	.	kIx.	.
</s>
<s>
Vlastně	vlastně	k9	vlastně
památný	památný	k2eAgInSc4d1	památný
paralelní	paralelní	k2eAgInSc4d1	paralelní
střih	střih	k1gInSc4	střih
v	v	k7c6	v
Životě	život	k1gInSc6	život
amerického	americký	k2eAgMnSc2d1	americký
hasiče	hasič	k1gMnSc2	hasič
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
částečně	částečně	k6eAd1	částečně
dílem	dílo	k1gNnSc7	dílo
náhody	náhoda	k1gFnSc2	náhoda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
Porterových	Porterův	k2eAgInPc2d1	Porterův
cílů	cíl	k1gInPc2	cíl
při	při	k7c6	při
natáčení	natáčení	k1gNnSc4	natáčení
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
bylo	být	k5eAaImAgNnS	být
použít	použít	k5eAaPmF	použít
zbytkovou	zbytkový	k2eAgFnSc4d1	zbytková
metráž	metráž	k1gFnSc4	metráž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
Edisonově	Edisonův	k2eAgFnSc6d1	Edisonova
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ve	v	k7c6	v
standardních	standardní	k2eAgFnPc6d1	standardní
filmových	filmový	k2eAgFnPc6d1	filmová
dějinách	dějiny	k1gFnPc6	dějiny
je	být	k5eAaImIp3nS	být
Porter	porter	k1gInSc1	porter
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
inovátor	inovátor	k1gMnSc1	inovátor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Life	Life	k1gInSc1	Life
of	of	k?	of
an	an	k?	an
American	American	k1gMnSc1	American
Fireman	Fireman	k1gMnSc1	Fireman
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
Porter	porter	k1gInSc1	porter
ve	v	k7c6	v
filmu	film	k1gInSc6	film
využil	využít	k5eAaPmAgInS	využít
archivních	archivní	k2eAgInPc2d1	archivní
záběrů	záběr	k1gInPc2	záběr
z	z	k7c2	z
požárního	požární	k2eAgNnSc2d1	požární
cvičení	cvičení	k1gNnSc2	cvičení
<g/>
,	,	kIx,	,
doplnil	doplnit	k5eAaPmAgMnS	doplnit
je	být	k5eAaImIp3nS	být
dotáčkami	dotáčka	k1gFnPc7	dotáčka
nahranými	nahraný	k2eAgMnPc7d1	nahraný
herci	herec	k1gMnPc7	herec
a	a	k8xC	a
pospojoval	pospojovat	k5eAaPmAgMnS	pospojovat
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
vyprávějící	vyprávějící	k2eAgInSc4d1	vyprávějící
o	o	k7c6	o
záchraně	záchrana	k1gFnSc6	záchrana
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
dítěte	dítě	k1gNnSc2	dítě
z	z	k7c2	z
hořícího	hořící	k2eAgInSc2d1	hořící
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
okamžiku	okamžik	k1gInSc6	okamžik
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
zabíraný	zabíraný	k2eAgInSc1d1	zabíraný
zvenku	zvenku	k6eAd1	zvenku
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
kamera	kamera	k1gFnSc1	kamera
pronikla	proniknout	k5eAaPmAgFnS	proniknout
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ještě	ještě	k6eAd1	ještě
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
kamera	kamera	k1gFnSc1	kamera
tam	tam	k6eAd1	tam
hasiče	hasič	k1gMnPc4	hasič
již	již	k6eAd1	již
nesledovala	sledovat	k5eNaImAgFnS	sledovat
a	a	k8xC	a
dramatická	dramatický	k2eAgFnSc1d1	dramatická
pauza	pauza	k1gFnSc1	pauza
zesilovala	zesilovat	k5eAaImAgFnS	zesilovat
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahynul	zahynout	k5eAaPmAgMnS	zahynout
i	i	k9	i
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
v	v	k7c6	v
plamenech	plamen	k1gInPc6	plamen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dokázala	dokázat	k5eAaPmAgFnS	dokázat
montáž	montáž	k1gFnSc1	montáž
vhodně	vhodně	k6eAd1	vhodně
vybraných	vybraný	k2eAgInPc2d1	vybraný
kousků	kousek	k1gInPc2	kousek
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
umně	umně	k6eAd1	umně
slepených	slepený	k2eAgFnPc2d1	slepená
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
slavnější	slavný	k2eAgMnSc1d2	slavnější
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
kinematografii	kinematografie	k1gFnSc4	kinematografie
typičtější	typický	k2eAgFnSc4d2	typičtější
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Porterova	Porterův	k2eAgFnSc1d1	Porterova
Velká	velký	k2eAgFnSc1d1	velká
železniční	železniční	k2eAgFnSc1d1	železniční
loupež	loupež	k1gFnSc1	loupež
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc2	The
Great	Great	k2eAgInSc4d1	Great
Train	Train	k1gInSc4	Train
Robbery	Robbera	k1gFnSc2	Robbera
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
gangsterka	gangsterka	k1gFnSc1	gangsterka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
první	první	k4xOgFnSc1	první
kovbojka	kovbojka	k1gFnSc1	kovbojka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
opíral	opírat	k5eAaImAgInS	opírat
o	o	k7c4	o
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
událost	událost	k1gFnSc4	událost
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
děj	děj	k1gInSc1	děj
měl	mít	k5eAaImAgInS	mít
několik	několik	k4yIc4	několik
akčních	akční	k2eAgInPc2d1	akční
momentů	moment	k1gInPc2	moment
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
neměl	mít	k5eNaImAgInS	mít
žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
film	film	k1gInSc1	film
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celé	celý	k2eAgFnSc2d1	celá
osmileté	osmiletý	k2eAgFnSc2d1	osmiletá
existence	existence	k1gFnSc2	existence
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Porter	porter	k1gInSc1	porter
předváděl	předvádět	k5eAaImAgInS	předvádět
pozornost	pozornost	k1gFnSc4	pozornost
diváka	divák	k1gMnSc4	divák
vynalézavou	vynalézavý	k2eAgFnSc7d1	vynalézavá
montáží	montáž	k1gFnSc7	montáž
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
a	a	k8xC	a
dbal	dbát	k5eAaImAgMnS	dbát
na	na	k7c4	na
logickou	logický	k2eAgFnSc4d1	logická
posloupnost	posloupnost	k1gFnSc4	posloupnost
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
divák	divák	k1gMnSc1	divák
dokázal	dokázat	k5eAaPmAgMnS	dokázat
orientovat	orientovat	k5eAaBmF	orientovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
dějových	dějový	k2eAgInPc6d1	dějový
zvratech	zvrat	k1gInPc6	zvrat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgInSc1	první
ukázal	ukázat	k5eAaPmAgInS	ukázat
detail	detail	k1gInSc4	detail
revolveru	revolver	k1gInSc2	revolver
střílejícího	střílející	k2eAgInSc2d1	střílející
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
publika	publikum	k1gNnSc2	publikum
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
využil	využít	k5eAaPmAgInS	využít
též	též	k9	též
filmovou	filmový	k2eAgFnSc4d1	filmová
působivost	působivost	k1gFnSc4	působivost
honičky	honička	k1gFnSc2	honička
<g/>
.	.	kIx.	.
</s>
<s>
Přepadení	přepadení	k1gNnSc1	přepadení
dostavníku	dostavník	k1gInSc2	dostavník
je	být	k5eAaImIp3nS	být
plné	plný	k2eAgFnPc4d1	plná
pohybu	pohyb	k1gInSc2	pohyb
–	–	k?	–
skoků	skok	k1gInPc2	skok
<g/>
,	,	kIx,	,
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
klusu	klus	k1gInSc2	klus
i	i	k8xC	i
cvalu	cval	k1gInSc2	cval
<g/>
.	.	kIx.	.
</s>
<s>
Cabiria	Cabirium	k1gNnPc1	Cabirium
měla	mít	k5eAaImAgNnP	mít
veliký	veliký	k2eAgInSc4d1	veliký
vliv	vliv	k1gInSc4	vliv
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
však	však	k9	však
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
centrum	centrum	k1gNnSc4	centrum
filmové	filmový	k2eAgFnSc2d1	filmová
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
Zrození	zrození	k1gNnSc1	zrození
národa	národ	k1gInSc2	národ
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnSc1	The
Birth	Birth	k1gMnSc1	Birth
of	of	k?	of
a	a	k8xC	a
Nation	Nation	k1gInSc1	Nation
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
a	a	k8xC	a
Intolerance	intolerance	k1gFnSc1	intolerance
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Davida	David	k1gMnSc2	David
W.	W.	kA	W.
Griffitha	Griffith	k1gMnSc2	Griffith
<g/>
,	,	kIx,	,
nezapomenutelného	zapomenutelný	k2eNgMnSc2d1	nezapomenutelný
Homéra	Homér	k1gMnSc2	Homér
filmu	film	k1gInSc2	film
a	a	k8xC	a
prvního	první	k4xOgMnSc2	první
tvůrce	tvůrce	k1gMnSc2	tvůrce
pravidel	pravidlo	k1gNnPc2	pravidlo
filmové	filmový	k2eAgFnSc2d1	filmová
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Griffith	Griffith	k1gMnSc1	Griffith
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
dráhu	dráha	k1gFnSc4	dráha
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
toužil	toužit	k5eAaImAgInS	toužit
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
točit	točit	k5eAaImF	točit
"	"	kIx"	"
<g/>
uznávané	uznávaný	k2eAgInPc4d1	uznávaný
<g/>
"	"	kIx"	"
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
chtěl	chtít	k5eAaImAgInS	chtít
osvobodit	osvobodit	k5eAaPmF	osvobodit
od	od	k7c2	od
statutu	statut	k1gInSc2	statut
nižší	nízký	k2eAgFnSc2d2	nižší
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
něčeho	něco	k3yInSc2	něco
nevhodného	vhodný	k2eNgNnSc2d1	nevhodné
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
většina	většina	k1gFnSc1	většina
vitality	vitalita	k1gFnSc2	vitalita
a	a	k8xC	a
síly	síla	k1gFnSc2	síla
filmu	film	k1gInSc2	film
spočívá	spočívat	k5eAaImIp3nS	spočívat
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
odpadlickém	odpadlický	k2eAgInSc6d1	odpadlický
statutu	statut	k1gInSc6	statut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
Mack	Mack	k1gMnSc1	Mack
Sennett	Sennett	k1gMnSc1	Sennett
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
pro	pro	k7c4	pro
Griffitha	Griffith	k1gMnSc4	Griffith
<g/>
,	,	kIx,	,
točil	točit	k5eAaImAgMnS	točit
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
komedie	komedie	k1gFnPc4	komedie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
projevují	projevovat	k5eAaImIp3nP	projevovat
ducha	duch	k1gMnSc4	duch
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
nadšení	nadšení	k1gNnSc2	nadšení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
patrné	patrný	k2eAgInPc1d1	patrný
u	u	k7c2	u
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
režiséra	režisér	k1gMnSc2	režisér
Griffitha	Griffith	k1gMnSc2	Griffith
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Griffithovo	Griffithův	k2eAgNnSc1d1	Griffithovo
umění	umění	k1gNnSc1	umění
se	se	k3xPyFc4	se
dívalo	dívat	k5eAaImAgNnS	dívat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
Sennettovo	Sennettův	k2eAgNnSc1d1	Sennettův
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
k	k	k7c3	k
dadaismu	dadaismus	k1gInSc3	dadaismus
a	a	k8xC	a
surrealismu	surrealismus	k1gInSc3	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
již	již	k6eAd1	již
jmenované	jmenovaný	k2eAgInPc1d1	jmenovaný
americké	americký	k2eAgInPc1d1	americký
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
Zrození	zrození	k1gNnSc1	zrození
národa	národ	k1gInSc2	národ
a	a	k8xC	a
Intolerance	intolerance	k1gFnSc2	intolerance
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
monumentálních	monumentální	k2eAgNnPc2d1	monumentální
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
obrovské	obrovský	k2eAgFnPc4d1	obrovská
dekorace	dekorace	k1gFnPc4	dekorace
z	z	k7c2	z
Intolerance	intolerance	k1gFnSc2	intolerance
se	se	k3xPyFc4	se
tyčily	tyčit	k5eAaImAgFnP	tyčit
nad	nad	k7c7	nad
Hollywoodem	Hollywood	k1gInSc7	Hollywood
přes	přes	k7c4	přes
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gNnSc1	jejich
rozebrání	rozebrání	k1gNnSc1	rozebrání
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
být	být	k5eAaImF	být
příliš	příliš	k6eAd1	příliš
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Griffithova	Griffithův	k2eAgFnSc1d1	Griffithova
individualita	individualita	k1gFnSc1	individualita
překračovala	překračovat	k5eAaImAgFnS	překračovat
řadu	řada	k1gFnSc4	řada
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
samouk	samouk	k1gMnSc1	samouk
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zátěže	zátěž	k1gFnSc2	zátěž
literárních	literární	k2eAgFnPc2d1	literární
či	či	k8xC	či
divadelních	divadelní	k2eAgFnPc2d1	divadelní
konvencí	konvence	k1gFnPc2	konvence
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
senzačními	senzační	k2eAgInPc7d1	senzační
filmy	film	k1gInPc7	film
a	a	k8xC	a
společenskými	společenský	k2eAgFnPc7d1	společenská
melodramaty	melodramaty	k?	melodramaty
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Klobouk	klobouk	k1gInSc1	klobouk
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gMnPc1	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Hat	hat	k0	hat
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
))	))	k?	))
typickými	typický	k2eAgFnPc7d1	typická
pro	pro	k7c4	pro
americkou	americký	k2eAgFnSc4d1	americká
mentalitu	mentalita	k1gFnSc4	mentalita
<g/>
.	.	kIx.	.
</s>
<s>
Světovou	světový	k2eAgFnSc4d1	světová
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
trvalé	trvalý	k2eAgNnSc4d1	trvalé
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
si	se	k3xPyFc3	se
Griffith	Griffith	k1gMnSc1	Griffith
získal	získat	k5eAaPmAgMnS	získat
dvěma	dva	k4xCgInPc7	dva
velkými	velký	k2eAgInPc7d1	velký
opusy	opus	k1gInPc7	opus
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
shrnul	shrnout	k5eAaPmAgMnS	shrnout
prakticky	prakticky	k6eAd1	prakticky
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
ve	v	k7c6	v
filmu	film	k1gInSc6	film
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historici	historik	k1gMnPc1	historik
naivně	naivně	k6eAd1	naivně
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Griffith	Griffith	k1gMnSc1	Griffith
objevil	objevit	k5eAaPmAgMnS	objevit
takové	takový	k3xDgInPc4	takový
výrazové	výrazový	k2eAgInPc4d1	výrazový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
např.	např.	kA	např.
velký	velký	k2eAgInSc4d1	velký
detail	detail	k1gInSc4	detail
(	(	kIx(	(
<g/>
o	o	k7c4	o
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
znám	znát	k5eAaImIp1nS	znát
již	již	k9	již
Angličanům	Angličan	k1gMnPc3	Angličan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Griffith	Griffith	k1gMnSc1	Griffith
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
režisérem	režisér	k1gMnSc7	režisér
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
originální	originální	k2eAgInPc4d1	originální
výrazové	výrazový	k2eAgInPc4d1	výrazový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
výstřední	výstřední	k2eAgFnPc4d1	výstřední
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgInS	přijmout
za	za	k7c4	za
vlastní	vlastní	k2eAgInSc4d1	vlastní
a	a	k8xC	a
zbavil	zbavit	k5eAaPmAgInS	zbavit
je	on	k3xPp3gFnPc4	on
jejich	jejich	k3xOp3gFnPc4	jejich
samoúčelnosti	samoúčelnost	k1gFnPc4	samoúčelnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
"	"	kIx"	"
<g/>
zázraky	zázrak	k1gInPc1	zázrak
<g/>
"	"	kIx"	"
filmového	filmový	k2eAgNnSc2d1	filmové
plátna	plátno	k1gNnSc2	plátno
uměl	umět	k5eAaImAgInS	umět
uplatnit	uplatnit	k5eAaPmF	uplatnit
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
ohromení	ohromení	k1gNnSc4	ohromení
diváka	divák	k1gMnSc2	divák
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
cíle	cíl	k1gInSc2	cíl
tvůrců	tvůrce	k1gMnPc2	tvůrce
Cabirie	Cabirie	k1gFnSc2	Cabirie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
dojetí	dojetí	k1gNnSc2	dojetí
a	a	k8xC	a
propagaci	propagace	k1gFnSc4	propagace
vlastních	vlastní	k2eAgInPc2d1	vlastní
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zrození	zrození	k1gNnSc6	zrození
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
epopeji	epopeje	k1gFnSc4	epopeje
z	z	k7c2	z
války	válka	k1gFnSc2	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
bylo	být	k5eAaImAgNnS	být
takovýmto	takovýto	k3xDgInSc7	takovýto
názorem	názor	k1gInSc7	názor
zotavení	zotavení	k1gNnSc2	zotavení
poraženého	poražený	k2eAgInSc2d1	poražený
Jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
jižanského	jižanský	k2eAgMnSc2d1	jižanský
plukovníka	plukovník	k1gMnSc2	plukovník
zatratil	zatratit	k5eAaPmAgMnS	zatratit
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
silou	síla	k1gFnSc7	síla
"	"	kIx"	"
<g/>
opilé	opilý	k2eAgInPc1d1	opilý
negry	negr	k1gInPc1	negr
<g/>
"	"	kIx"	"
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
"	"	kIx"	"
<g/>
špinavou	špinavý	k2eAgFnSc4d1	špinavá
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
"	"	kIx"	"
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
kapucí	kapuce	k1gFnSc7	kapuce
zahaleným	zahalený	k2eAgMnSc7d1	zahalený
jezdcem	jezdec	k1gMnSc7	jezdec
Ku-klux-klanu	Kuluxlan	k1gInSc2	Ku-klux-klan
<g/>
.	.	kIx.	.
</s>
<s>
Griffith	Griffith	k1gMnSc1	Griffith
mistrovsky	mistrovsky	k6eAd1	mistrovsky
sestříhal	sestříhat	k5eAaPmAgMnS	sestříhat
zejména	zejména	k9	zejména
scénu	scéna	k1gFnSc4	scéna
příchodu	příchod	k1gInSc2	příchod
posily	posila	k1gFnSc2	posila
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
podobě	podoba	k1gFnSc6	podoba
jako	jako	k9	jako
"	"	kIx"	"
<g/>
záchrana	záchrana	k1gFnSc1	záchrana
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
potom	potom	k6eAd1	potom
mnohokrát	mnohokrát	k6eAd1	mnohokrát
kopírovaná	kopírovaný	k2eAgFnSc1d1	kopírovaná
-	-	kIx~	-
zahalení	zahalený	k2eAgMnPc1d1	zahalený
jezdci	jezdec	k1gMnPc1	jezdec
uhánějí	uhánět	k5eAaImIp3nP	uhánět
s	s	k7c7	s
větrem	vítr	k1gInSc7	vítr
o	o	k7c4	o
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgFnPc1d1	jiná
scény	scéna	k1gFnPc1	scéna
paralelně	paralelně	k6eAd1	paralelně
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
rodinu	rodina	k1gFnSc4	rodina
Cameronů	Cameron	k1gInPc2	Cameron
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nS	bránit
před	před	k7c4	před
útoky	útok	k1gInPc4	útok
tlupy	tlupa	k1gFnSc2	tlupa
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Propojení	propojení	k1gNnSc1	propojení
těchto	tento	k3xDgFnPc2	tento
scén	scéna	k1gFnPc2	scéna
metodou	metoda	k1gFnSc7	metoda
"	"	kIx"	"
<g/>
synchronní	synchronní	k2eAgFnSc2d1	synchronní
montáže	montáž	k1gFnSc2	montáž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
různé	různý	k2eAgFnPc1d1	různá
dějové	dějový	k2eAgFnPc1d1	dějová
sekvence	sekvence	k1gFnPc1	sekvence
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
pocit	pocit	k1gInSc4	pocit
ohrožení	ohrožení	k1gNnSc2	ohrožení
a	a	k8xC	a
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
dramatické	dramatický	k2eAgNnSc1d1	dramatické
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Zrození	zrození	k1gNnSc2	zrození
národa	národ	k1gInSc2	národ
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
rekordního	rekordní	k2eAgInSc2d1	rekordní
finančního	finanční	k2eAgInSc2d1	finanční
úspěchu	úspěch	k1gInSc2	úspěch
(	(	kIx(	(
<g/>
zisk	zisk	k1gInSc1	zisk
ze	z	k7c2	z
vstupného	vstupné	k1gNnSc2	vstupné
činil	činit	k5eAaImAgInS	činit
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
jednoznačně	jednoznačně	k6eAd1	jednoznačně
reakční	reakční	k2eAgNnSc1d1	reakční
vyznění	vyznění	k1gNnSc1	vyznění
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
protesty	protest	k1gInPc4	protest
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
přímo	přímo	k6eAd1	přímo
potyčky	potyčka	k1gFnSc2	potyčka
před	před	k7c7	před
kiny	kino	k1gNnPc7	kino
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgInPc4d1	získaný
peníze	peníz	k1gInPc4	peníz
investoval	investovat	k5eAaBmAgInS	investovat
Griffith	Griffith	k1gInSc1	Griffith
do	do	k7c2	do
následného	následný	k2eAgInSc2d1	následný
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgMnSc4	jenž
zvolil	zvolit	k5eAaPmAgMnS	zvolit
humanistické	humanistický	k2eAgNnSc4d1	humanistické
téma	téma	k1gNnSc4	téma
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
nesnášenlivosti	nesnášenlivost	k1gFnSc6	nesnášenlivost
<g/>
,	,	kIx,	,
odvážně	odvážně	k6eAd1	odvážně
namířené	namířený	k2eAgFnPc1d1	namířená
proti	proti	k7c3	proti
předsudkům	předsudek	k1gInPc3	předsudek
a	a	k8xC	a
nespravedlnosti	nespravedlnost	k1gFnSc6	nespravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Intolerance	intolerance	k1gFnSc1	intolerance
byla	být	k5eAaImAgFnS	být
svým	svůj	k3xOyFgNnSc7	svůj
pojetím	pojetí	k1gNnSc7	pojetí
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yQgFnSc7	jaký
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
kinematografie	kinematografie	k1gFnSc2	kinematografie
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
ještě	ještě	k6eAd1	ještě
nesetkala	setkat	k5eNaPmAgFnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
realizaci	realizace	k1gFnSc4	realizace
filmu	film	k1gInSc2	film
pracovalo	pracovat	k5eAaImAgNnS	pracovat
60	[number]	k4	60
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
trvala	trvat	k5eAaImAgFnS	trvat
72	[number]	k4	72
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
film	film	k1gInSc1	film
zredukoval	zredukovat	k5eAaPmAgInS	zredukovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
odehrávajících	odehrávající	k2eAgInPc6d1	odehrávající
příbězích	příběh	k1gInPc6	příběh
-	-	kIx~	-
o	o	k7c6	o
vyloupení	vyloupení	k1gNnSc6	vyloupení
Babylónu	babylón	k1gInSc2	babylón
<g/>
,	,	kIx,	,
utrpení	utrpení	k1gNnSc4	utrpení
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
pařížském	pařížský	k2eAgInSc6d1	pařížský
masakru	masakr	k1gInSc6	masakr
hugenotů	hugenot	k1gMnPc2	hugenot
a	a	k8xC	a
současné	současný	k2eAgFnSc2d1	současná
oběti	oběť	k1gFnSc2	oběť
soudního	soudní	k2eAgInSc2d1	soudní
omylu	omyl	k1gInSc2	omyl
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
fanatismus	fanatismus	k1gInSc1	fanatismus
v	v	k7c6	v
nejrozmanitějších	rozmanitý	k2eAgFnPc6d3	nejrozmanitější
podobách	podoba	k1gFnPc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
intoleranci	intolerance	k1gFnSc3	intolerance
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
stavěno	stavěn	k2eAgNnSc1d1	stavěno
dobro	dobro	k1gNnSc1	dobro
a	a	k8xC	a
milosrdenství	milosrdenství	k1gNnSc1	milosrdenství
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
naivnost	naivnost	k1gFnSc4	naivnost
a	a	k8xC	a
špatný	špatný	k2eAgInSc4d1	špatný
vkus	vkus	k1gInSc4	vkus
nelze	lze	k6eNd1	lze
neobdivovat	obdivovat	k5eNaImF	obdivovat
režijní	režijní	k2eAgFnSc4d1	režijní
obratnost	obratnost	k1gFnSc4	obratnost
při	při	k7c6	při
realizaci	realizace	k1gFnSc6	realizace
filmového	filmový	k2eAgNnSc2d1	filmové
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
i	i	k9	i
dnes	dnes	k6eAd1	dnes
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
řadu	řada	k1gFnSc4	řada
potíží	potíž	k1gFnPc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
nebyly	být	k5eNaImAgInP	být
tak	tak	k6eAd1	tak
jednoduše	jednoduše	k6eAd1	jednoduše
porušeny	porušit	k5eAaPmNgInP	porušit
tři	tři	k4xCgInPc1	tři
pravidla	pravidlo	k1gNnPc4	pravidlo
klasického	klasický	k2eAgNnSc2d1	klasické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
a	a	k8xC	a
děje	děj	k1gInPc4	děj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
zbrzdily	zbrzdit	k5eAaPmAgFnP	zbrzdit
svobodný	svobodný	k2eAgInSc4d1	svobodný
rozvoj	rozvoj	k1gInSc4	rozvoj
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
roky	rok	k1gInPc4	rok
byla	být	k5eAaImAgFnS	být
Intolerance	intolerance	k1gFnSc2	intolerance
skutečným	skutečný	k2eAgInSc7d1	skutečný
výčtem	výčet	k1gInSc7	výčet
filmových	filmový	k2eAgFnPc2d1	filmová
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Filmoví	filmový	k2eAgMnPc1d1	filmový
tvůrci	tvůrce	k1gMnPc1	tvůrce
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
sedmi	sedm	k4xCc2	sedm
nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
společnostem	společnost	k1gFnPc3	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1908	[number]	k4	1908
spojily	spojit	k5eAaPmAgInP	spojit
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Pathé	Pathý	k2eAgFnSc2d1	Pathý
Frè	Frè	k1gFnSc2	Frè
a	a	k8xC	a
Star	star	k1gFnSc2	star
Film	film	k1gInSc1	film
Company	Compana	k1gFnSc2	Compana
do	do	k7c2	do
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Patents	Patents	k1gInSc1	Patents
Company	Compan	k1gInPc1	Compan
(	(	kIx(	(
<g/>
MPPC	MPPC	kA	MPPC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
začínaly	začínat	k5eAaImAgInP	začínat
vystupovat	vystupovat	k5eAaImF	vystupovat
některé	některý	k3yIgFnPc1	některý
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Stěhovaly	stěhovat	k5eAaImAgInP	stěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
vymkly	vymknout	k5eAaPmAgFnP	vymknout
mocenskému	mocenský	k2eAgInSc3d1	mocenský
vlivu	vliv	k1gInSc3	vliv
MPPC	MPPC	kA	MPPC
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
MPPC	MPPC	kA	MPPC
obhospodařovala	obhospodařovat	k5eAaImAgFnS	obhospodařovat
veškeré	veškerý	k3xTgInPc4	veškerý
patenty	patent	k1gInPc4	patent
na	na	k7c4	na
kamery	kamera	k1gFnPc4	kamera
a	a	k8xC	a
promítací	promítací	k2eAgInPc4d1	promítací
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
podle	podle	k7c2	podle
exkluzivní	exkluzivní	k2eAgFnSc2d1	exkluzivní
smlouvy	smlouva	k1gFnSc2	smlouva
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Eastman	Eastman	k1gMnSc1	Eastman
Kodak	Kodak	kA	Kodak
byla	být	k5eAaImAgNnP	být
MPPC	MPPC	kA	MPPC
jediným	jediný	k2eAgMnSc7d1	jediný
odběratelem	odběratel	k1gMnSc7	odběratel
filmového	filmový	k2eAgInSc2d1	filmový
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
využívala	využívat	k5eAaImAgFnS	využívat
svého	svůj	k3xOyFgNnSc2	svůj
monopolního	monopolní	k2eAgNnSc2d1	monopolní
postavení	postavení	k1gNnSc2	postavení
k	k	k7c3	k
vybírání	vybírání	k1gNnSc3	vybírání
licenčních	licenční	k2eAgInPc2d1	licenční
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
užití	užití	k1gNnSc4	užití
kamer	kamera	k1gFnPc2	kamera
a	a	k8xC	a
filmových	filmový	k2eAgInPc2d1	filmový
projektorů	projektor	k1gInPc2	projektor
a	a	k8xC	a
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
program	program	k1gInSc4	program
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
MPPC	MPPC	kA	MPPC
daří	dařit	k5eAaImIp3nS	dařit
podřídit	podřídit	k5eAaPmF	podřídit
si	se	k3xPyFc3	se
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
trh	trh	k1gInSc4	trh
půjčoven	půjčovna	k1gFnPc2	půjčovna
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
si	se	k3xPyFc3	se
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
společnosti	společnost	k1gFnPc1	společnost
budovaly	budovat	k5eAaImAgFnP	budovat
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
majitel	majitel	k1gMnSc1	majitel
půjčovny	půjčovna	k1gFnSc2	půjčovna
William	William	k1gInSc1	William
Fox	fox	k1gInSc1	fox
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
s	s	k7c7	s
MPPC	MPPC	kA	MPPC
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
nadvláda	nadvláda	k1gFnSc1	nadvláda
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
již	již	k6eAd1	již
prolomena	prolomen	k2eAgFnSc1d1	prolomena
<g/>
.	.	kIx.	.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
jako	jako	k8xS	jako
Jesse	Jesse	k1gFnPc1	Jesse
Lasky	Laska	k1gFnSc2	Laska
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Goldwyn	Goldwyn	k1gMnSc1	Goldwyn
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Laemmle	Laemmle	k1gFnSc2	Laemmle
<g/>
,	,	kIx,	,
Adolph	Adolph	k1gMnSc1	Adolph
Zukor	Zukor	k1gMnSc1	Zukor
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Ince	Inka	k1gFnSc6	Inka
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
B.	B.	kA	B.
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Hollywood	Hollywood	k1gInSc1	Hollywood
stal	stát	k5eAaPmAgInS	stát
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
filmových	filmový	k2eAgMnPc2d1	filmový
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
stále	stále	k6eAd1	stále
nových	nový	k2eAgFnPc2d1	nová
atrakcí	atrakce	k1gFnPc2	atrakce
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnPc4	jejich
varietní	varietní	k2eAgInSc1d1	varietní
program	program	k1gInSc1	program
přiměl	přimět	k5eAaPmAgInS	přimět
bratry	bratr	k1gMnPc4	bratr
Maxe	Max	k1gMnSc4	Max
a	a	k8xC	a
Emila	Emil	k1gMnSc4	Emil
Skladanowské	Skladanowská	k1gFnSc2	Skladanowská
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
investovali	investovat	k5eAaBmAgMnP	investovat
čas	čas	k1gInSc4	čas
i	i	k8xC	i
peníze	peníz	k1gInPc4	peníz
do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
nové	nový	k2eAgFnSc2d1	nová
aparatury	aparatura	k1gFnSc2	aparatura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
předstírala	předstírat	k5eAaImAgFnS	předstírat
"	"	kIx"	"
<g/>
živé	živý	k2eAgInPc4d1	živý
obrazy	obraz	k1gInPc4	obraz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
"	"	kIx"	"
<g/>
laterna-magica-show	laternaagicahow	k?	laterna-magica-show
<g/>
"	"	kIx"	"
úspěšně	úspěšně	k6eAd1	úspěšně
cestovali	cestovat	k5eAaImAgMnP	cestovat
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
publikum	publikum	k1gNnSc4	publikum
ohromovali	ohromovat	k5eAaImAgMnP	ohromovat
očividně	očividně	k6eAd1	očividně
pohyblivými	pohyblivý	k2eAgInPc7d1	pohyblivý
mlhavými	mlhavý	k2eAgInPc7d1	mlhavý
obrazy	obraz	k1gInPc7	obraz
<g/>
,	,	kIx,	,
vytvořenými	vytvořený	k2eAgInPc7d1	vytvořený
šikovným	šikovný	k2eAgNnPc3d1	šikovné
prolínáním	prolínání	k1gNnPc3	prolínání
více	hodně	k6eAd2	hodně
projekcí	projekce	k1gFnSc7	projekce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
natočil	natočit	k5eAaBmAgMnS	natočit
puntičkář	puntičkář	k1gMnSc1	puntičkář
Max	Max	k1gMnSc1	Max
Skladanowsky	Skladanowska	k1gFnSc2	Skladanowska
první	první	k4xOgMnSc1	první
německý	německý	k2eAgInSc1d1	německý
film	film	k1gInSc1	film
s	s	k7c7	s
přestavěnou	přestavěný	k2eAgFnSc7d1	přestavěná
kamerou	kamera	k1gFnSc7	kamera
Kodak	Kodak	kA	Kodak
na	na	k7c4	na
celuloidový	celuloidový	k2eAgInSc4d1	celuloidový
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
berlínské	berlínský	k2eAgMnPc4d1	berlínský
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
bratra	bratr	k1gMnSc2	bratr
Emila	Emil	k1gMnSc2	Emil
při	při	k7c6	při
cvičení	cvičení	k1gNnSc6	cvičení
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
rodičovského	rodičovský	k2eAgInSc2d1	rodičovský
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chybějícímu	chybějící	k2eAgInSc3d1	chybějící
projektoru	projektor	k1gInSc3	projektor
se	se	k3xPyFc4	se
promítání	promítání	k1gNnSc6	promítání
těchto	tento	k3xDgInPc2	tento
filmů	film	k1gInPc2	film
konalo	konat	k5eAaImAgNnS	konat
nejprve	nejprve	k6eAd1	nejprve
pomocí	pomocí	k7c2	pomocí
palcového	palcový	k2eAgNnSc2d1	palcové
kina	kino	k1gNnSc2	kino
<g/>
,	,	kIx,	,
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
malých	malý	k2eAgFnPc2d1	malá
listovacích	listovací	k2eAgFnPc2d1	listovací
knížeček	knížečka	k1gFnPc2	knížečka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
varietními	varietní	k2eAgInPc7d1	varietní
výstupy	výstup	k1gInPc7	výstup
však	však	k9	však
Max	Max	k1gMnSc1	Max
horečně	horečně	k6eAd1	horečně
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
přerušovaném	přerušovaný	k2eAgInSc6d1	přerušovaný
mechanismu	mechanismus	k1gInSc6	mechanismus
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
promítací	promítací	k2eAgInSc4d1	promítací
přístroj	přístroj	k1gInSc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1895	[number]	k4	1895
byl	být	k5eAaImAgInS	být
dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
projektor	projektor	k1gInSc1	projektor
<g/>
,	,	kIx,	,
pokřtěný	pokřtěný	k2eAgInSc1d1	pokřtěný
jako	jako	k8xS	jako
bioskop	bioskop	k1gInSc1	bioskop
<g/>
,	,	kIx,	,
hotov	hotov	k2eAgInSc1d1	hotov
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelé	ředitel	k1gMnPc1	ředitel
slavného	slavný	k2eAgNnSc2d1	slavné
Varieté	varieté	k1gNnSc2	varieté
Wintergarden	Wintergardna	k1gFnPc2	Wintergardna
<g/>
,	,	kIx,	,
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zábavní	zábavní	k2eAgFnSc2d1	zábavní
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
namáhali	namáhat	k5eAaImAgMnP	namáhat
do	do	k7c2	do
dílny	dílna	k1gFnSc2	dílna
pouťových	pouťový	k2eAgMnPc2d1	pouťový
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
posoudili	posoudit	k5eAaPmAgMnP	posoudit
novou	nový	k2eAgFnSc4d1	nová
senzaci	senzace	k1gFnSc4	senzace
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1895	[number]	k4	1895
směly	smět	k5eAaImAgFnP	smět
krátké	krátká	k1gFnPc1	krátká
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
se	se	k3xPyFc4	se
mihotající	mihotající	k2eAgInPc1d1	mihotající
filmy	film	k1gInPc1	film
tvořit	tvořit	k5eAaImF	tvořit
vrchol	vrchol	k1gInSc4	vrchol
a	a	k8xC	a
zakončení	zakončení	k1gNnSc4	zakončení
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Publikum	publikum	k1gNnSc1	publikum
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
vytržení	vytržení	k1gNnSc2	vytržení
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
putovalo	putovat	k5eAaImAgNnS	putovat
do	do	k7c2	do
domu	dům	k1gInSc2	dům
hvězd	hvězda	k1gFnPc2	hvězda
večera	večer	k1gInSc2	večer
pozvání	pozvánět	k5eAaImIp3nP	pozvánět
světově	světově	k6eAd1	světově
proslulého	proslulý	k2eAgNnSc2d1	proslulé
pařížského	pařížský	k2eAgNnSc2d1	pařížské
varieté	varieté	k1gNnSc2	varieté
Follies	Follies	k1gMnSc1	Follies
Bergere	Berger	k1gMnSc5	Berger
<g/>
.	.	kIx.	.
</s>
<s>
Skladanowským	Skladanowské	k1gNnSc7	Skladanowské
náleží	náležet	k5eAaImIp3nS	náležet
sláva	sláva	k1gFnSc1	sláva
za	za	k7c4	za
první	první	k4xOgNnSc4	první
veřejné	veřejný	k2eAgNnSc4d1	veřejné
představení	představení	k1gNnSc4	představení
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
bioskopu	bioskop	k1gInSc2	bioskop
byla	být	k5eAaImAgFnS	být
nicméně	nicméně	k8xC	nicméně
příliš	příliš	k6eAd1	příliš
nepohodlná	pohodlný	k2eNgFnSc1d1	nepohodlná
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Skladanowští	Skladanowský	k1gMnPc1	Skladanowský
přijeli	přijet	k5eAaPmAgMnP	přijet
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
mluvilo	mluvit	k5eAaImAgNnS	mluvit
již	již	k6eAd1	již
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
o	o	k7c6	o
kinematografu	kinematograf	k1gInSc6	kinematograf
bratří	bratr	k1gMnPc2	bratr
Lumiérů	Lumiér	k1gMnPc2	Lumiér
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
své	svůj	k3xOyFgInPc4	svůj
"	"	kIx"	"
<g/>
živé	živý	k2eAgInPc4d1	živý
obrazy	obraz	k1gInPc4	obraz
<g/>
"	"	kIx"	"
veřejně	veřejně	k6eAd1	veřejně
představili	představit	k5eAaPmAgMnP	představit
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Follies	Follies	k1gMnSc1	Follies
Bergere	Berger	k1gMnSc5	Berger
bez	bez	k7c2	bez
okolků	okolek	k1gInPc2	okolek
odřeklo	odřeknout	k5eAaPmAgNnS	odřeknout
představení	představení	k1gNnSc1	představení
tímto	tento	k3xDgInSc7	tento
již	již	k6eAd1	již
překonaného	překonaný	k2eAgInSc2d1	překonaný
bioskopu	bioskop	k1gInSc2	bioskop
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
rozčarovaní	rozčarovaný	k2eAgMnPc1d1	rozčarovaný
jako	jako	k8xC	jako
zvědaví	zvědavý	k2eAgMnPc1d1	zvědavý
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Skladanowští	Skladanowský	k2eAgMnPc1d1	Skladanowský
hned	hned	k6eAd1	hned
příští	příští	k2eAgNnPc4d1	příští
představení	představení	k1gNnPc4	představení
konkurentů	konkurent	k1gMnPc2	konkurent
a	a	k8xC	a
uznali	uznat	k5eAaPmAgMnP	uznat
technickou	technický	k2eAgFnSc4d1	technická
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
pařížského	pařížský	k2eAgInSc2d1	pařížský
vynálezu	vynález	k1gInSc2	vynález
<g/>
.	.	kIx.	.
</s>
<s>
Skladanowských	Skladanowských	k2eAgInSc1d1	Skladanowských
bioskop	bioskop	k1gInSc1	bioskop
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
promítacích	promítací	k2eAgInPc2d1	promítací
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
patentovány	patentován	k2eAgMnPc4d1	patentován
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Prokazatelně	prokazatelně	k6eAd1	prokazatelně
se	se	k3xPyFc4	se
také	také	k9	také
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
konala	konat	k5eAaImAgFnS	konat
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
první	první	k4xOgNnPc4	první
veřejná	veřejný	k2eAgNnPc4d1	veřejné
filmová	filmový	k2eAgNnPc4d1	filmové
představení	představení	k1gNnPc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Prosadit	prosadit	k5eAaPmF	prosadit
se	se	k3xPyFc4	se
však	však	k9	však
mohly	moct	k5eAaImAgInP	moct
jen	jen	k9	jen
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
objevitelé	objevitel	k1gMnPc1	objevitel
disponovali	disponovat	k5eAaBmAgMnP	disponovat
finančními	finanční	k2eAgInPc7d1	finanční
prostředky	prostředek	k1gInPc7	prostředek
nezbytnými	nezbytný	k2eAgInPc7d1	nezbytný
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
dalšímu	další	k2eAgInSc3d1	další
technickému	technický	k2eAgInSc3d1	technický
vývoji	vývoj	k1gInSc3	vývoj
a	a	k8xC	a
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
uvedení	uvedení	k1gNnSc3	uvedení
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
skupina	skupina	k1gFnSc1	skupina
filmových	filmový	k2eAgMnPc2d1	filmový
tvůrců	tvůrce	k1gMnPc2	tvůrce
z	z	k7c2	z
Brightonu	Brighton	k1gInSc2	Brighton
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
specifické	specifický	k2eAgFnPc4d1	specifická
součástí	součást	k1gFnSc7	součást
filmové	filmový	k2eAgFnSc3d1	filmová
řeči	řeč	k1gFnSc3	řeč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
svá	svůj	k3xOyFgNnPc4	svůj
malí	malý	k1gMnPc1	malý
filmová	filmový	k2eAgNnPc4d1	filmové
pojednání	pojednání	k1gNnPc4	pojednání
učinili	učinit	k5eAaImAgMnP	učinit
srozumitelnějšími	srozumitelný	k2eAgMnPc7d2	srozumitelnější
<g/>
,	,	kIx,	,
napínavějšími	napínavý	k2eAgMnPc7d2	napínavější
a	a	k8xC	a
zábavnějšími	zábavní	k2eAgMnPc7d2	zábavnější
<g/>
.	.	kIx.	.
</s>
<s>
Portrétní	portrétní	k2eAgMnSc1d1	portrétní
fotograf	fotograf	k1gMnSc1	fotograf
George	George	k1gNnSc2	George
Albert	Albert	k1gMnSc1	Albert
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
jako	jako	k9	jako
první	první	k4xOgInSc1	první
s	s	k7c7	s
technicko-vypravěčskými	technickoypravěčský	k2eAgFnPc7d1	technicko-vypravěčský
možnostmi	možnost	k1gFnPc7	možnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
zkomponování	zkomponování	k1gNnSc4	zkomponování
scény	scéna	k1gFnSc2	scéna
sestříháním	sestříhání	k1gNnSc7	sestříhání
různých	různý	k2eAgInPc2d1	různý
obrazových	obrazový	k2eAgInPc2d1	obrazový
záběrů	záběr	k1gInPc2	záběr
a	a	k8xC	a
perspektiv	perspektiva	k1gFnPc2	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
filmu	film	k1gInSc6	film
Babiččina	babiččin	k2eAgFnSc1d1	babiččina
lupa	lupa	k1gFnSc1	lupa
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
například	například	k6eAd1	například
rané	raný	k2eAgNnSc4d1	rané
využití	využití	k1gNnSc4	využití
insertního	insertní	k2eAgInSc2d1	insertní
střihu	střih	k1gInSc2	střih
<g/>
,	,	kIx,	,
střídání	střídání	k1gNnSc1	střídání
extrémně	extrémně	k6eAd1	extrémně
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
velikostí	velikost	k1gFnPc2	velikost
záběrů	záběr	k1gInPc2	záběr
a	a	k8xC	a
"	"	kIx"	"
<g/>
hlediskového	hlediskový	k2eAgInSc2d1	hlediskový
záběru	záběr	k1gInSc2	záběr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
point-of-view-shot	pointfiewhot	k1gInSc1	point-of-view-shot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
reprodukuje	reprodukovat	k5eAaBmIp3nS	reprodukovat
subjektivní	subjektivní	k2eAgFnSc4d1	subjektivní
perspektivu	perspektiva	k1gFnSc4	perspektiva
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
opakovaně	opakovaně	k6eAd1	opakovaně
stříhá	stříhat	k5eAaImIp3nS	stříhat
obraz	obraz	k1gInSc4	obraz
dítěte	dítě	k1gNnSc2	dítě
dívajícího	dívající	k2eAgNnSc2d1	dívající
se	se	k3xPyFc4	se
skrz	skrz	k6eAd1	skrz
lupu	lupat	k5eAaImIp1nS	lupat
proti	proti	k7c3	proti
velkým	velký	k2eAgInPc3d1	velký
detailům	detail	k1gInPc3	detail
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Babiččino	babiččin	k2eAgNnSc1d1	Babiččino
oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
v	v	k7c6	v
kulatém	kulatý	k2eAgInSc6d1	kulatý
výřezu	výřez	k1gInSc6	výřez
na	na	k7c6	na
černém	černý	k2eAgNnSc6d1	černé
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Smith	Smith	k1gMnSc1	Smith
našel	najít	k5eAaPmAgMnS	najít
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
na	na	k7c6	na
divadelní	divadelní	k2eAgFnSc6d1	divadelní
scéně	scéna	k1gFnSc6	scéna
byl	být	k5eAaImAgInS	být
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
jen	jen	k9	jen
slovně	slovně	k6eAd1	slovně
<g/>
,	,	kIx,	,
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
filmově	filmově	k6eAd1	filmově
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
význam	význam	k1gInSc4	význam
pomocí	pomocí	k7c2	pomocí
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
obrazy	obraz	k1gInPc7	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
lékárník	lékárník	k1gMnSc1	lékárník
James	James	k1gMnSc1	James
Williamson	Williamson	k1gMnSc1	Williamson
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
objevil	objevit	k5eAaPmAgMnS	objevit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
při	při	k7c6	při
filmové	filmový	k2eAgFnSc6d1	filmová
aktualitě	aktualita	k1gFnSc6	aktualita
o	o	k7c6	o
veslařských	veslařský	k2eAgInPc6d1	veslařský
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmové	filmový	k2eAgNnSc1d1	filmové
vyprávění	vyprávění	k1gNnSc1	vyprávění
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
ukazovat	ukazovat	k5eAaImF	ukazovat
celý	celý	k2eAgInSc4d1	celý
průběh	průběh	k1gInSc4	průběh
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
srozumitelné	srozumitelný	k2eAgNnSc1d1	srozumitelné
<g/>
.	.	kIx.	.
</s>
<s>
Wiliamson	Wiliamson	k1gMnSc1	Wiliamson
namontoval	namontovat	k5eAaPmAgMnS	namontovat
mezi	mezi	k7c4	mezi
start	start	k1gInSc4	start
a	a	k8xC	a
cílový	cílový	k2eAgInSc4d1	cílový
finiš	finiš	k1gInSc4	finiš
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
záběr	záběr	k1gInSc4	záběr
ze	z	k7c2	z
závodu	závod	k1gInSc2	závod
a	a	k8xC	a
použil	použít	k5eAaPmAgInS	použít
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
základní	základní	k2eAgFnSc4d1	základní
formu	forma	k1gFnSc4	forma
eliptické	eliptický	k2eAgFnSc2d1	eliptická
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
montáže	montáž	k1gFnSc2	montáž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Útok	útok	k1gInSc4	útok
na	na	k7c4	na
čínskou	čínský	k2eAgFnSc4d1	čínská
misii	misie	k1gFnSc4	misie
šel	jít	k5eAaImAgMnS	jít
ještě	ještě	k9	ještě
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
konfrontoval	konfrontovat	k5eAaBmAgMnS	konfrontovat
střídající	střídající	k2eAgMnSc1d1	střídající
se	se	k3xPyFc4	se
obrazy	obraz	k1gInPc7	obraz
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
volající	volající	k2eAgFnSc2d1	volající
misionářovy	misionářův	k2eAgFnSc2d1	misionářův
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
před	před	k7c7	před
útočníky	útočník	k1gMnPc7	útočník
na	na	k7c4	na
balkon	balkon	k1gInSc4	balkon
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejího	její	k3xOp3gMnSc4	její
spěšně	spěšně	k6eAd1	spěšně
se	se	k3xPyFc4	se
blížícího	blížící	k2eAgMnSc2d1	blížící
zachránce	zachránce	k1gMnSc2	zachránce
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
tvoří	tvořit	k5eAaImIp3nS	tvořit
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
střihu	střih	k1gInSc2	střih
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
Last	Last	k1gInSc1	Last
Minute	Minut	k1gMnSc5	Minut
Rescue	Rescuus	k1gMnSc5	Rescuus
(	(	kIx(	(
<g/>
záchrana	záchrana	k1gFnSc1	záchrana
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
minutě	minuta	k1gFnSc6	minuta
<g/>
)	)	kIx)	)
nepostradatelný	postradatelný	k2eNgInSc1d1	nepostradatelný
dramaturgický	dramaturgický	k2eAgInSc1d1	dramaturgický
vrchol	vrchol	k1gInSc1	vrchol
mnoha	mnoho	k4c2	mnoho
westernů	western	k1gInPc2	western
<g/>
,	,	kIx,	,
thrillerů	thriller	k1gInPc2	thriller
či	či	k8xC	či
akčních	akční	k2eAgInPc2d1	akční
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
filmaři	filmař	k1gMnPc1	filmař
z	z	k7c2	z
Brightonu	Brighton	k1gInSc2	Brighton
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
použili	použít	k5eAaPmAgMnP	použít
první	první	k4xOgInPc4	první
specificky	specificky	k6eAd1	specificky
filmové	filmový	k2eAgInPc4d1	filmový
výrazové	výrazový	k2eAgInPc4d1	výrazový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
nedostali	dostat	k5eNaPmAgMnP	dostat
příležitost	příležitost	k1gFnSc4	příležitost
své	svůj	k3xOyFgFnSc2	svůj
inovace	inovace	k1gFnSc2	inovace
dále	daleko	k6eAd2	daleko
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Narůstajícímu	narůstající	k2eAgInSc3d1	narůstající
konkurenčnímu	konkurenční	k2eAgInSc3d1	konkurenční
tlaku	tlak	k1gInSc3	tlak
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
nemohli	moct	k5eNaImAgMnP	moct
nesrovnatelně	srovnatelně	k6eNd1	srovnatelně
chudší	chudý	k2eAgMnPc1d2	chudší
"	"	kIx"	"
<g/>
Brightonští	Brightonský	k2eAgMnPc1d1	Brightonský
<g/>
"	"	kIx"	"
čelit	čelit	k5eAaImF	čelit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
začala	začít	k5eAaPmAgFnS	začít
samostatná	samostatný	k2eAgFnSc1d1	samostatná
výroba	výroba	k1gFnSc1	výroba
filmu	film	k1gInSc2	film
teprve	teprve	k6eAd1	teprve
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
však	však	k9	však
nemuselo	muset	k5eNaImAgNnS	muset
vandrovat	vandrovat	k5eAaImF	vandrovat
po	po	k7c6	po
vesnicích	vesnice	k1gFnPc6	vesnice
jako	jako	k9	jako
laciná	laciný	k2eAgFnSc1d1	laciná
jarmareční	jarmareční	k2eAgFnSc1d1	jarmareční
atrakce	atrakce	k1gFnSc1	atrakce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
táhlo	táhnout	k5eAaImAgNnS	táhnout
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
publikum	publikum	k1gNnSc4	publikum
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
do	do	k7c2	do
kamenných	kamenný	k2eAgNnPc2d1	kamenné
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
Italové	Ital	k1gMnPc1	Ital
byli	být	k5eAaImAgMnP	být
spíše	spíše	k9	spíše
ochotni	ochoten	k2eAgMnPc1d1	ochoten
akceptovat	akceptovat	k5eAaBmF	akceptovat
a	a	k8xC	a
využívat	využívat	k5eAaPmF	využívat
film	film	k1gInSc4	film
jako	jako	k8xS	jako
umělecké	umělecký	k2eAgNnSc4d1	umělecké
médium	médium	k1gNnSc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
recept	recept	k1gInSc1	recept
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
se	se	k3xPyFc4	se
však	však	k9	však
neorientoval	orientovat	k5eNaBmAgMnS	orientovat
na	na	k7c4	na
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
objevili	objevit	k5eAaPmAgMnP	objevit
vypravěčský	vypravěčský	k2eAgInSc4d1	vypravěčský
a	a	k8xC	a
vizuální	vizuální	k2eAgInSc4d1	vizuální
potenciál	potenciál	k1gInSc4	potenciál
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrovali	demonstrovat	k5eAaBmAgMnP	demonstrovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
film	film	k1gInSc1	film
může	moct	k5eAaImIp3nS	moct
překonat	překonat	k5eAaPmF	překonat
čas	čas	k1gInSc4	čas
i	i	k8xC	i
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
velké	velký	k2eAgFnPc4d1	velká
souvislosti	souvislost	k1gFnPc4	souvislost
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
epickým	epický	k2eAgMnSc7d1	epický
než	než	k8xS	než
dramatickým	dramatický	k2eAgNnSc7d1	dramatické
médiem	médium	k1gNnSc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
založené	založený	k2eAgInPc1d1	založený
koncerny	koncern	k1gInPc1	koncern
Cines	Cinesa	k1gFnPc2	Cinesa
<g/>
,	,	kIx,	,
Itala	Ital	k1gMnSc2	Ital
a	a	k8xC	a
Ambrosio	Ambrosio	k6eAd1	Ambrosio
se	se	k3xPyFc4	se
specializovaly	specializovat	k5eAaBmAgInP	specializovat
na	na	k7c4	na
nákladná	nákladný	k2eAgNnPc4d1	nákladné
filmová	filmový	k2eAgNnPc4d1	filmové
zpracování	zpracování	k1gNnPc4	zpracování
historických	historický	k2eAgNnPc2d1	historické
témat	téma	k1gNnPc2	téma
–	–	k?	–
nejčastěji	často	k6eAd3	často
podle	podle	k7c2	podle
populárních	populární	k2eAgFnPc2d1	populární
literárních	literární	k2eAgFnPc2d1	literární
předloh	předloha	k1gFnPc2	předloha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
kapitálovým	kapitálový	k2eAgNnSc7d1	kapitálové
nasazením	nasazení	k1gNnSc7	nasazení
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
odehrával	odehrávat	k5eAaImAgInS	odehrávat
<g/>
,	,	kIx,	,
točili	točit	k5eAaImAgMnP	točit
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
v	v	k7c6	v
gigantických	gigantický	k2eAgFnPc6d1	gigantická
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
"	"	kIx"	"
<g/>
natvrdo	natvrdo	k6eAd1	natvrdo
<g/>
"	"	kIx"	"
postavených	postavený	k2eAgFnPc6d1	postavená
kulisách	kulisa	k1gFnPc6	kulisa
a	a	k8xC	a
na	na	k7c6	na
skutečných	skutečný	k2eAgNnPc6d1	skutečné
dějištích	dějiště	k1gNnPc6	dějiště
národní	národní	k2eAgFnSc2d1	národní
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Pompézně	pompézně	k6eAd1	pompézně
vyzdobené	vyzdobený	k2eAgFnPc1d1	vyzdobená
armády	armáda	k1gFnPc1	armáda
statistů	statista	k1gMnPc2	statista
zalidňovaly	zalidňovat	k5eAaImAgFnP	zalidňovat
scenérie	scenérie	k1gFnPc1	scenérie
a	a	k8xC	a
dělaly	dělat	k5eAaImAgFnP	dělat
dojem	dojem	k1gInSc4	dojem
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
světovém	světový	k2eAgInSc6d1	světový
úspěchu	úspěch	k1gInSc6	úspěch
dvacetiminutového	dvacetiminutový	k2eAgNnSc2d1	dvacetiminutové
historického	historický	k2eAgNnSc2d1	historické
dramatu	drama	k1gNnSc2	drama
Poslední	poslední	k2eAgFnSc2d1	poslední
dny	dna	k1gFnSc2	dna
Pompejí	Pompeje	k1gFnPc2	Pompeje
vsadili	vsadit	k5eAaPmAgMnP	vsadit
producenti	producent	k1gMnPc1	producent
na	na	k7c4	na
stále	stále	k6eAd1	stále
delší	dlouhý	k2eAgInPc4d2	delší
a	a	k8xC	a
náročnější	náročný	k2eAgInPc4d2	náročnější
filmové	filmový	k2eAgInPc4d1	filmový
eposy	epos	k1gInPc4	epos
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc1	film
s	s	k7c7	s
tituly	titul	k1gInPc7	titul
jako	jako	k8xC	jako
Pád	Pád	k1gInSc1	Pád
Tróji	Trója	k1gFnSc3	Trója
(	(	kIx(	(
<g/>
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
800	[number]	k4	800
komparzistů	komparzista	k1gMnPc2	komparzista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Agrippina	Agrippina	k1gFnSc1	Agrippina
(	(	kIx(	(
<g/>
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
2000	[number]	k4	2000
komparzistů	komparzista	k1gMnPc2	komparzista
<g/>
)	)	kIx)	)
a	a	k8xC	a
Quo	Quo	k1gFnSc1	Quo
Vadis	Vadis	k1gFnSc1	Vadis
(	(	kIx(	(
<g/>
120	[number]	k4	120
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
5000	[number]	k4	5000
komparzistů	komparzista	k1gMnPc2	komparzista
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
rizikové	rizikový	k2eAgFnPc1d1	riziková
investice	investice	k1gFnPc1	investice
obřích	obří	k2eAgFnPc2d1	obří
dimenzí	dimenze	k1gFnPc2	dimenze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
brzy	brzy	k6eAd1	brzy
vyplatit	vyplatit	k5eAaPmF	vyplatit
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
během	během	k7c2	během
několika	několik	k4yIc2	několik
málo	málo	k4c1	málo
let	léto	k1gNnPc2	léto
národem	národ	k1gInSc7	národ
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
exportním	exportní	k2eAgInSc7d1	exportní
podílem	podíl	k1gInSc7	podíl
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
hned	hned	k6eAd1	hned
po	po	k7c6	po
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
produkovala	produkovat	k5eAaImAgFnS	produkovat
s	s	k7c7	s
rozpočtem	rozpočet	k1gInSc7	rozpočet
jednoho	jeden	k4xCgInSc2	jeden
miliónu	milión	k4xCgInSc2	milión
zlatých	zlatá	k1gFnPc2	zlatá
lir	lira	k1gFnPc2	lira
dosud	dosud	k6eAd1	dosud
nejdražší	drahý	k2eAgFnSc7d3	nejdražší
a	a	k8xC	a
s	s	k7c7	s
promítací	promítací	k2eAgFnSc7d1	promítací
dobou	doba	k1gFnSc7	doba
tři	tři	k4xCgFnPc1	tři
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
čtvrtě	čtvrt	k1gFnPc1	čtvrt
hodiny	hodina	k1gFnSc2	hodina
zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
film	film	k1gInSc1	film
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
–	–	k?	–
Cabiria	Cabirium	k1gNnSc2	Cabirium
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Giovanni	Giovanň	k1gMnSc3	Giovanň
Pastrone	Pastron	k1gInSc5	Pastron
nelitoval	litovat	k5eNaImAgInS	litovat
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
vysokohorského	vysokohorský	k2eAgInSc2d1	vysokohorský
sněhu	sníh	k1gInSc2	sníh
k	k	k7c3	k
pouštím	poušť	k1gFnPc3	poušť
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
lidských	lidský	k2eAgNnPc2d1	lidské
mas	maso	k1gNnPc2	maso
<g/>
,	,	kIx,	,
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
velbloudích	velbloudí	k2eAgFnPc2d1	velbloudí
karavan	karavana	k1gFnPc2	karavana
a	a	k8xC	a
fantastických	fantastický	k2eAgFnPc2d1	fantastická
dekorací	dekorace	k1gFnPc2	dekorace
hojnost	hojnost	k1gFnSc1	hojnost
historických	historický	k2eAgInPc2d1	historický
prvků	prvek	k1gInPc2	prvek
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Cabiria	Cabirium	k1gNnSc2	Cabirium
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
příkladem	příklad	k1gInSc7	příklad
bohatství	bohatství	k1gNnSc2	bohatství
filmové	filmový	k2eAgFnSc2d1	filmová
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
italský	italský	k2eAgInSc1d1	italský
velkofilm	velkofilm	k1gInSc1	velkofilm
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Pastrone	Pastron	k1gInSc5	Pastron
pracoval	pracovat	k5eAaImAgInS	pracovat
s	s	k7c7	s
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
velikostmi	velikost	k1gFnPc7	velikost
záběru	záběr	k1gInSc2	záběr
<g/>
,	,	kIx,	,
různými	různý	k2eAgFnPc7d1	různá
délkami	délka	k1gFnPc7	délka
záběru	záběr	k1gInSc2	záběr
a	a	k8xC	a
sledy	sled	k1gInPc4	sled
střihů	střih	k1gInPc2	střih
<g/>
,	,	kIx,	,
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
paralelní	paralelní	k2eAgFnSc7d1	paralelní
montáží	montáž	k1gFnSc7	montáž
a	a	k8xC	a
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
nasadil	nasadit	k5eAaPmAgMnS	nasadit
umělé	umělý	k2eAgNnSc4d1	umělé
světlo	světlo	k1gNnSc4	světlo
k	k	k7c3	k
estetickým	estetický	k2eAgInPc3d1	estetický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
venkovní	venkovní	k2eAgInPc4d1	venkovní
snímky	snímek	k1gInPc4	snímek
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
kamerový	kamerový	k2eAgInSc1d1	kamerový
vůz	vůz	k1gInSc1	vůz
a	a	k8xC	a
vyzkoumal	vyzkoumat	k5eAaPmAgMnS	vyzkoumat
možnosti	možnost	k1gFnPc4	možnost
kamerové	kamerový	k2eAgFnSc2d1	kamerová
jízdy	jízda	k1gFnSc2	jízda
jako	jako	k8xC	jako
nového	nový	k2eAgInSc2d1	nový
filmového	filmový	k2eAgInSc2d1	filmový
výrazového	výrazový	k2eAgInSc2d1	výrazový
prostředku	prostředek	k1gInSc2	prostředek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Carello	Carello	k1gNnSc1	Carello
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
Pastrone	Pastron	k1gInSc5	Pastron
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
filmu	film	k1gInSc2	film
dokonce	dokonce	k9	dokonce
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
,	,	kIx,	,
otevřel	otevřít	k5eAaPmAgInS	otevřít
kinematografii	kinematografie	k1gFnSc3	kinematografie
novou	nový	k2eAgFnSc4d1	nová
dimenzi	dimenze	k1gFnSc4	dimenze
–	–	k?	–
hloubku	hloubek	k1gInSc3	hloubek
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
obdobích	období	k1gNnPc6	období
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
trhu	trh	k1gInSc6	trh
profilovat	profilovat	k5eAaImF	profilovat
i	i	k9	i
producenti	producent	k1gMnPc1	producent
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
zemích	zem	k1gFnPc6	zem
–	–	k?	–
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozuměli	rozumět	k5eAaImAgMnP	rozumět
obchodu	obchod	k1gInSc3	obchod
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
nos	nos	k1gInSc4	nos
na	na	k7c4	na
senzace	senzace	k1gFnPc4	senzace
<g/>
.	.	kIx.	.
</s>
<s>
Dánská	dánský	k2eAgFnSc1d1	dánská
Nordisk	Nordisk	k1gInSc1	Nordisk
Films	Films	k1gInSc1	Films
Kompagni	Kompagň	k1gMnPc7	Kompagň
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
bývalým	bývalý	k2eAgMnSc7d1	bývalý
cirkusovým	cirkusový	k2eAgMnSc7d1	cirkusový
akrobatem	akrobat	k1gMnSc7	akrobat
Ole	Ola	k1gFnSc6	Ola
Olsenem	Olsen	k1gInSc7	Olsen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
nechal	nechat	k5eAaPmAgMnS	nechat
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
film	film	k1gInSc4	film
ze	z	k7c2	z
safari	safari	k1gNnSc2	safari
Lov	lov	k1gInSc1	lov
lvů	lev	k1gInPc2	lev
zastřelit	zastřelit	k5eAaPmF	zastřelit
před	před	k7c7	před
běžící	běžící	k2eAgFnSc7d1	běžící
kamerou	kamera	k1gFnSc7	kamera
dvě	dva	k4xCgFnPc1	dva
živé	živý	k2eAgFnPc1d1	živá
divoké	divoký	k2eAgFnPc1d1	divoká
šelmy	šelma	k1gFnPc1	šelma
<g/>
.	.	kIx.	.
</s>
<s>
Zuřivé	zuřivý	k2eAgInPc1d1	zuřivý
protesty	protest	k1gInPc1	protest
zvedaly	zvedat	k5eAaImAgInP	zvedat
obrat	obrat	k1gInSc4	obrat
a	a	k8xC	a
Olsen	Olsen	k1gInSc4	Olsen
prodal	prodat	k5eAaPmAgMnS	prodat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
260	[number]	k4	260
kopií	kopie	k1gFnPc2	kopie
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
rozruch	rozruch	k1gInSc1	rozruch
svým	svůj	k3xOyFgInSc7	svůj
prvním	první	k4xOgInSc7	první
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
filmem	film	k1gInSc7	film
Bílá	bílý	k2eAgFnSc1d1	bílá
otrokyně	otrokyně	k1gFnSc1	otrokyně
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
Evropanky	Evropanka	k1gFnSc2	Evropanka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
zavlečena	zavleknout	k5eAaPmNgFnS	zavleknout
do	do	k7c2	do
nevěstince	nevěstinec	k1gInSc2	nevěstinec
v	v	k7c6	v
cizí	cizí	k2eAgFnSc6d1	cizí
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnPc4	svůj
"	"	kIx"	"
<g/>
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
smyslnosti	smyslnost	k1gFnPc4	smyslnost
<g/>
"	"	kIx"	"
opět	opět	k6eAd1	opět
veřejný	veřejný	k2eAgInSc1d1	veřejný
pokřik	pokřik	k1gInSc1	pokřik
a	a	k8xC	a
volání	volání	k1gNnSc1	volání
po	po	k7c6	po
filmové	filmový	k2eAgFnSc6d1	filmová
cenzuře	cenzura	k1gFnSc6	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
erotických	erotický	k2eAgInPc2d1	erotický
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
produkce	produkce	k1gFnSc1	produkce
v	v	k7c6	v
předválečném	předválečný	k2eAgNnSc6d1	předválečné
období	období	k1gNnSc6	období
měla	mít	k5eAaImAgFnS	mít
proslavit	proslavit	k5eAaPmF	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
se	se	k3xPyFc4	se
dánští	dánský	k2eAgMnPc1d1	dánský
pionýři	pionýr	k1gMnPc1	pionýr
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
umění	umění	k1gNnSc2	umění
filmového	filmový	k2eAgNnSc2d1	filmové
herectví	herectví	k1gNnSc2	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hvězda	hvězda	k1gFnSc1	hvězda
Asta	Asta	k1gFnSc1	Asta
Nielsenová	Nielsenová	k1gFnSc1	Nielsenová
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
všeodhalující	všeodhalující	k2eAgFnSc1d1	všeodhalující
čočka	čočka	k1gFnSc1	čočka
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
nutí	nutit	k5eAaImIp3nS	nutit
k	k	k7c3	k
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
pravdivosti	pravdivost	k1gFnSc3	pravdivost
výrazu	výraz	k1gInSc2	výraz
<g/>
"	"	kIx"	"
a	a	k8xC	a
filmové	filmový	k2eAgNnSc1d1	filmové
herectví	herectví	k1gNnSc1	herectví
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zcela	zcela	k6eAd1	zcela
jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
hraní	hraní	k1gNnSc2	hraní
než	než	k8xS	než
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
režií	režie	k1gFnSc7	režie
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
Petera	Peter	k1gMnSc2	Peter
Urbana	Urban	k1gMnSc2	Urban
Gada	Gadus	k1gMnSc2	Gadus
<g/>
,	,	kIx,	,
ukázala	ukázat	k5eAaPmAgFnS	ukázat
Nielsenová	Nielsenová	k1gFnSc1	Nielsenová
světu	svět	k1gInSc3	svět
již	již	k9	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
filmu	film	k1gInSc6	film
Nad	nad	k7c7	nad
propastí	propast	k1gFnSc7	propast
co	co	k3yQnSc1	co
obnáší	obnášet	k5eAaImIp3nS	obnášet
umění	umění	k1gNnSc4	umění
filmového	filmový	k2eAgNnSc2d1	filmové
herectví	herectví	k1gNnSc2	herectví
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
minimálními	minimální	k2eAgInPc7d1	minimální
gesty	gest	k1gInPc7	gest
<g/>
,	,	kIx,	,
pohledem	pohled	k1gInSc7	pohled
<g/>
,	,	kIx,	,
pohybem	pohyb	k1gInSc7	pohyb
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
smyslnou	smyslný	k2eAgFnSc7d1	smyslná
přítomností	přítomnost	k1gFnSc7	přítomnost
celého	celý	k2eAgNnSc2d1	celé
svého	své	k1gNnSc2	své
těla	tělo	k1gNnSc2	tělo
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zahrát	zahrát	k5eAaPmF	zahrát
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
pocitů	pocit	k1gInPc2	pocit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Královna	královna	k1gFnSc1	královna
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
obsazována	obsazován	k2eAgFnSc1d1	obsazována
jako	jako	k8xS	jako
tragicky	tragicky	k6eAd1	tragicky
milující	milující	k2eAgFnSc1d1	milující
nebo	nebo	k8xC	nebo
pronásledovaná	pronásledovaný	k2eAgFnSc1d1	pronásledovaná
nevinná	vinný	k2eNgFnSc1d1	nevinná
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
osud	osud	k1gInSc1	osud
neomylně	omylně	k6eNd1	omylně
spěl	spět	k5eAaImAgInS	spět
ke	k	k7c3	k
smutnému	smutný	k2eAgInSc3d1	smutný
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Dánové	Dán	k1gMnPc1	Dán
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zpopularizovali	zpopularizovat	k5eAaPmAgMnP	zpopularizovat
odhalování	odhalování	k1gNnSc4	odhalování
duše	duše	k1gFnSc2	duše
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
ovládali	ovládat	k5eAaImAgMnP	ovládat
německý	německý	k2eAgInSc4d1	německý
filmový	filmový	k2eAgInSc4d1	filmový
trh	trh	k1gInSc4	trh
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
svými	svůj	k3xOyFgMnPc7	svůj
melodramaty	melodramaty	k?	melodramaty
skandinávskou	skandinávský	k2eAgFnSc4d1	skandinávská
tradici	tradice	k1gFnSc4	tradice
psychologického	psychologický	k2eAgInSc2d1	psychologický
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
filmová	filmový	k2eAgFnSc1d1	filmová
projekce	projekce	k1gFnSc1	projekce
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
českým	český	k2eAgMnPc3d1	český
průkopníkům	průkopník	k1gMnPc3	průkopník
kinematografie	kinematografie	k1gFnSc2	kinematografie
patří	patřit	k5eAaImIp3nS	patřit
Viktor	Viktor	k1gMnSc1	Viktor
Ponrepo	Ponrepa	k1gFnSc5	Ponrepa
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Dismas	Dismas	k1gMnSc1	Dismas
Šlambor	Šlambor	k1gMnSc1	Šlambor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
prvního	první	k4xOgNnSc2	první
stálého	stálý	k2eAgNnSc2d1	stálé
kina	kino	k1gNnSc2	kino
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
Karlově	Karlův	k2eAgFnSc6d1	Karlova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Modré	modrý	k2eAgFnSc2d1	modrá
štiky	štika	k1gFnSc2	štika
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
konala	konat	k5eAaImAgFnS	konat
Světová	světový	k2eAgFnSc1d1	světová
výstava	výstava	k1gFnSc1	výstava
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
promítaly	promítat	k5eAaImAgInP	promítat
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
umění	umění	k1gNnSc6	umění
měl	mít	k5eAaImAgMnS	mít
Jan	Jan	k1gMnSc1	Jan
Kříženecký	kříženecký	k2eAgMnSc1d1	kříženecký
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
filmař	filmař	k1gMnSc1	filmař
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
filmování	filmování	k1gNnSc4	filmování
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
několik	několik	k4yIc4	několik
kinematografických	kinematografický	k2eAgInPc2d1	kinematografický
přístrojů	přístroj	k1gInPc2	přístroj
pro	pro	k7c4	pro
výstavu	výstava	k1gFnSc4	výstava
a	a	k8xC	a
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
kopie	kopie	k1gFnPc4	kopie
některých	některý	k3yIgInPc2	některý
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
např.	např.	kA	např.
Smích	smích	k1gInSc1	smích
a	a	k8xC	a
pláč	pláč	k1gInSc1	pláč
či	či	k8xC	či
Pokropený	pokropený	k2eAgMnSc1d1	pokropený
kropič	kropič	k1gMnSc1	kropič
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
chlapec	chlapec	k1gMnSc1	chlapec
škádlí	škádlit	k5eAaImIp3nS	škádlit
zahradníka	zahradník	k1gMnSc4	zahradník
s	s	k7c7	s
hadicí	hadice	k1gFnSc7	hadice
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
vytryskne	vytrysknout	k5eAaPmIp3nS	vytrysknout
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
hadice	hadice	k1gFnSc2	hadice
do	do	k7c2	do
obličeje	obličej	k1gInSc2	obličej
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
první	první	k4xOgFnSc1	první
groteska	groteska	k1gFnSc1	groteska
<g/>
.	.	kIx.	.
</s>
<s>
Rozmach	rozmach	k1gInSc1	rozmach
filmového	filmový	k2eAgNnSc2d1	filmové
podnikání	podnikání	k1gNnSc2	podnikání
nastal	nastat	k5eAaPmAgInS	nastat
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
až	až	k6eAd1	až
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
začalo	začít	k5eAaPmAgNnS	začít
začátkem	začátkem	k7c2	začátkem
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
produkovat	produkovat	k5eAaImF	produkovat
velké	velká	k1gFnPc4	velká
množství	množství	k1gNnSc2	množství
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
odlišovaly	odlišovat	k5eAaImAgFnP	odlišovat
od	od	k7c2	od
ostatních	ostatní	k1gNnPc2	ostatní
především	především	k9	především
využíváním	využívání	k1gNnSc7	využívání
svého	svůj	k3xOyFgNnSc2	svůj
přírodního	přírodní	k2eAgNnSc2d1	přírodní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Kinematografie	kinematografie	k1gFnSc1	kinematografie
se	se	k3xPyFc4	se
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
severskou	severský	k2eAgFnSc4d1	severská
krajinu	krajina	k1gFnSc4	krajina
a	a	k8xC	a
využívala	využívat	k5eAaImAgFnS	využívat
současně	současně	k6eAd1	současně
i	i	k9	i
domácí	domácí	k2eAgFnSc2d1	domácí
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
kostýmů	kostým	k1gInPc2	kostým
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
švédské	švédský	k2eAgInPc4d1	švédský
filmy	film	k1gInPc4	film
proslavili	proslavit	k5eAaPmAgMnP	proslavit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgMnPc1	tři
režiséři	režisér	k1gMnPc1	režisér
<g/>
:	:	kIx,	:
Georg	Georg	k1gMnSc1	Georg
af	af	k?	af
Klercker	Klercker	k1gMnSc1	Klercker
<g/>
,	,	kIx,	,
Mauritz	Mauritz	k1gMnSc1	Mauritz
Stiller	Stiller	k1gMnSc1	Stiller
a	a	k8xC	a
Victor	Victor	k1gMnSc1	Victor
Sjöström	Sjöström	k1gMnSc1	Sjöström
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
díla	dílo	k1gNnPc1	dílo
přinesla	přinést	k5eAaPmAgNnP	přinést
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
samotný	samotný	k2eAgMnSc1d1	samotný
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
značný	značný	k2eAgInSc1d1	značný
věhlas	věhlas	k1gInSc1	věhlas
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
kinematografické	kinematografický	k2eAgFnSc6d1	kinematografická
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
desátých	desátý	k4xOgNnPc6	desátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
společnost	společnost	k1gFnSc1	společnost
Svenska	Svensko	k1gNnSc2	Svensko
Biografteatern	Biografteaterna	k1gFnPc2	Biografteaterna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
existuje	existovat	k5eAaImIp3nS	existovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Charles	Charles	k1gMnSc1	Charles
Magnusson	Magnusson	k1gMnSc1	Magnusson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
z	z	k7c2	z
ní	on	k3xPp3gFnSc7	on
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
významnou	významný	k2eAgFnSc4d1	významná
produkční	produkční	k2eAgFnSc4d1	produkční
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
zdejším	zdejší	k2eAgMnSc7d1	zdejší
režisérem	režisér	k1gMnSc7	režisér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
af	af	k?	af
Klercker	Klercker	k1gMnSc1	Klercker
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šéfem	šéf	k1gMnSc7	šéf
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
byli	být	k5eAaImAgMnP	být
najati	najat	k2eAgMnPc1d1	najat
i	i	k8xC	i
Stiller	Stiller	k1gMnSc1	Stiller
a	a	k8xC	a
Sjöström	Sjöström	k1gMnSc1	Sjöström
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
švédská	švédský	k2eAgFnSc1d1	švédská
kinematografie	kinematografie	k1gFnSc1	kinematografie
byla	být	k5eAaImAgFnS	být
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
se	se	k3xPyFc4	se
režiséři	režisér	k1gMnPc1	režisér
spokojit	spokojit	k5eAaPmF	spokojit
se	s	k7c7	s
skromným	skromný	k2eAgInSc7d1	skromný
rozpočtem	rozpočet	k1gInSc7	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Af	Af	k?	Af
Klercker	Klercker	k1gMnSc1	Klercker
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
začal	začít	k5eAaPmAgMnS	začít
režírovat	režírovat	k5eAaImF	režírovat
<g/>
.	.	kIx.	.
</s>
<s>
Točil	točit	k5eAaImAgMnS	točit
komedie	komedie	k1gFnPc4	komedie
<g/>
,	,	kIx,	,
kriminální	kriminální	k2eAgInPc4d1	kriminální
thillery	thiller	k1gInPc4	thiller
<g/>
,	,	kIx,	,
válečné	válečný	k2eAgInPc4d1	válečný
filmy	film	k1gInPc4	film
a	a	k8xC	a
dramata	drama	k1gNnPc4	drama
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
výraznou	výrazný	k2eAgFnSc4d1	výrazná
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
osvětlení	osvětlení	k1gNnSc4	osvětlení
a	a	k8xC	a
rámování	rámování	k1gNnSc4	rámování
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
se	se	k3xPyFc4	se
nepohodl	pohodnout	k5eNaPmAgMnS	pohodnout
s	s	k7c7	s
Magnussonem	Magnusson	k1gMnSc7	Magnusson
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
menší	malý	k2eAgFnPc4d2	menší
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
Hasselbladfilm	Hasselbladfilm	k1gInSc4	Hasselbladfilm
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
prošel	projít	k5eAaPmAgInS	projít
několika	několik	k4yIc7	několik
fúzemi	fúze	k1gFnPc7	fúze
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
součástí	součást	k1gFnSc7	součást
Svenska	Svensko	k1gNnSc2	Svensko
Bio	Bio	k?	Bio
přejmenované	přejmenovaný	k2eAgFnPc4d1	přejmenovaná
na	na	k7c4	na
Svensk	Svensk	k1gInSc4	Svensk
Filmindustri	Filmindustr	k1gFnSc2	Filmindustr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
společnost	společnost	k1gFnSc1	společnost
uchovala	uchovat	k5eAaPmAgFnS	uchovat
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
af	af	k?	af
Klercker	Klercker	k1gInSc1	Klercker
již	již	k6eAd1	již
přestal	přestat	k5eAaPmAgInS	přestat
režírovat	režírovat	k5eAaImF	režírovat
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
na	na	k7c4	na
obchodování	obchodování	k1gNnSc4	obchodování
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgMnPc1d1	zbývající
dva	dva	k4xCgMnPc1	dva
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
Stiller	Stiller	k1gMnSc1	Stiller
a	a	k8xC	a
Sjöström	Sjöström	k1gMnSc1	Sjöström
<g/>
,	,	kIx,	,
setrvali	setrvat	k5eAaPmAgMnP	setrvat
u	u	k7c2	u
Magnussovy	Magnussův	k2eAgFnSc2d1	Magnussův
firmy	firma	k1gFnSc2	firma
Svenska	Svensko	k1gNnSc2	Svensko
<g/>
.	.	kIx.	.
</s>
<s>
Stiller	Stiller	k1gMnSc1	Stiller
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
adaptacím	adaptace	k1gFnPc3	adaptace
románů	román	k1gInPc2	román
Selmy	Selma	k1gFnSc2	Selma
Lagerlöfové	Lagerlöfový	k2eAgFnPc1d1	Lagerlöfová
<g/>
,	,	kIx,	,
nositelky	nositelka	k1gFnPc1	nositelka
Nobelové	Nobelové	k2eAgFnSc2d1	Nobelové
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
nejznámější	známý	k2eAgInPc1d3	nejznámější
filmy	film	k1gInPc1	film
patří	patřit	k5eAaImIp3nP	patřit
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
Tomáše	Tomáš	k1gMnSc2	Tomáš
Graala	Graal	k1gMnSc2	Graal
<g/>
,	,	kIx,	,
Poklad	poklad	k1gInSc4	poklad
pana	pan	k1gMnSc2	pan
Arna	Arne	k1gMnSc2	Arne
a	a	k8xC	a
Za	za	k7c7	za
štěstím	štěstí	k1gNnSc7	štěstí
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
film	film	k1gInSc1	film
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
erotickou	erotický	k2eAgFnSc4d1	erotická
komedii	komedie	k1gFnSc4	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Victor	Victor	k1gMnSc1	Victor
Sjöström	Sjöström	k1gMnSc1	Sjöström
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
režisérů	režisér	k1gMnPc2	režisér
němé	němý	k2eAgFnSc2d1	němá
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
se	se	k3xPyFc4	se
detailními	detailní	k2eAgInPc7d1	detailní
záběry	záběr	k1gInPc7	záběr
a	a	k8xC	a
využíváním	využívání	k1gNnSc7	využívání
krajiny	krajina	k1gFnSc2	krajina
jako	jako	k8xS	jako
expresivní	expresivní	k2eAgInSc4d1	expresivní
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Sjöström	Sjöström	k1gMnSc1	Sjöström
natočil	natočit	k5eAaBmAgMnS	natočit
řadu	řada	k1gFnSc4	řada
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
nejvýznamnější	významný	k2eAgNnPc1d3	nejvýznamnější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Ingeborg	Ingeborg	k1gInSc4	Ingeborg
Holmová	Holmová	k1gFnSc1	Holmová
<g/>
,	,	kIx,	,
Terje	Terje	k1gFnSc1	Terje
Vigen	Vigen	k1gInSc1	Vigen
nebo	nebo	k8xC	nebo
snímek	snímek	k1gInSc1	snímek
Psanci	psanec	k1gMnSc3	psanec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
omezení	omezení	k1gNnSc6	omezení
vývozu	vývoz	k1gInSc2	vývoz
švédských	švédský	k2eAgInPc2d1	švédský
filmů	film	k1gInPc2	film
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
snímkem	snímek	k1gInSc7	snímek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
přijat	přijmout	k5eAaPmNgInS	přijmout
velmi	velmi	k6eAd1	velmi
pozitivně	pozitivně	k6eAd1	pozitivně
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Louis	Louis	k1gMnSc1	Louis
Deluc	Deluc	k1gFnSc1	Deluc
ho	on	k3xPp3gNnSc4	on
náležité	náležitý	k2eAgNnSc4d1	náležité
pochválil	pochválit	k5eAaPmAgMnS	pochválit
<g/>
:	:	kIx,	:
Publikum	publikum	k1gNnSc1	publikum
je	být	k5eAaImIp3nS	být
zaplaveno	zaplavit	k5eAaPmNgNnS	zaplavit
emocemi	emoce	k1gFnPc7	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
jsou	být	k5eAaImIp3nP	být
jati	jmout	k5eAaPmNgMnP	jmout
posvátnou	posvátný	k2eAgFnSc7d1	posvátná
hrůzou	hrůza	k1gFnSc7	hrůza
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
neúrodné	úrodný	k2eNgFnPc4d1	neúrodná
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
selské	selský	k2eAgInPc4d1	selský
oděvy	oděv	k1gInPc4	oděv
a	a	k8xC	a
na	na	k7c4	na
prostou	prostý	k2eAgFnSc4d1	prostá
ošklivost	ošklivost	k1gFnSc4	ošklivost
i	i	k8xC	i
silný	silný	k2eAgInSc4d1	silný
lyrismus	lyrismus	k1gInSc4	lyrismus
zblízka	zblízka	k6eAd1	zblízka
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
pravdivost	pravdivost	k1gFnSc4	pravdivost
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
scén	scéna	k1gFnPc2	scéna
<g/>
,	,	kIx,	,
soustředěných	soustředěný	k2eAgFnPc2d1	soustředěná
na	na	k7c4	na
ústředí	ústředí	k1gNnSc4	ústředí
dvojici	dvojice	k1gFnSc4	dvojice
<g/>
,	,	kIx,	,
na	na	k7c4	na
násilné	násilný	k2eAgInPc4d1	násilný
boje	boj	k1gInPc4	boj
i	i	k8xC	i
výsostně	výsostně	k6eAd1	výsostně
tragický	tragický	k2eAgInSc4d1	tragický
konec	konec	k1gInSc4	konec
dvou	dva	k4xCgMnPc2	dva
starších	starý	k2eAgMnPc2d2	starší
milenců	milenec	k1gMnPc2	milenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
unikají	unikat	k5eAaImIp3nP	unikat
životu	život	k1gInSc3	život
v	v	k7c6	v
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
objetí	objetí	k1gNnSc6	objetí
uprostřed	uprostřed	k7c2	uprostřed
sněžné	sněžný	k2eAgFnSc2d1	sněžná
pustiny	pustina	k1gFnSc2	pustina
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
význačným	význačný	k2eAgInSc7d1	význačný
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgMnS	být
Vozka	vozka	k1gMnSc1	vozka
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
víceexpozice	víceexpozice	k1gFnPc1	víceexpozice
byly	být	k5eAaImAgFnP	být
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
dosud	dosud	k6eAd1	dosud
nejpropracovanějších	propracovaný	k2eAgFnPc2d3	nejpropracovanější
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stal	stát	k5eAaPmAgInS	stát
klasickým	klasický	k2eAgInSc7d1	klasický
dílem	díl	k1gInSc7	díl
filmové	filmový	k2eAgFnSc2d1	filmová
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
švédské	švédský	k2eAgFnSc2d1	švédská
kinematografie	kinematografie	k1gFnSc2	kinematografie
započal	započnout	k5eAaPmAgInS	započnout
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
odchodem	odchod	k1gInSc7	odchod
režiséru	režisér	k1gMnSc3	režisér
Stillera	Stiller	k1gMnSc2	Stiller
a	a	k8xC	a
Sjöströma	Sjöström	k1gMnSc2	Sjöström
a	a	k8xC	a
excelentních	excelentní	k2eAgMnPc2d1	excelentní
herců	herec	k1gMnPc2	herec
Grety	Greta	k1gFnSc2	Greta
Garbo	Garba	k1gFnSc5	Garba
a	a	k8xC	a
Larsa	Lars	k1gMnSc2	Lars
Hansona	Hanson	k1gMnSc2	Hanson
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Nastaly	nastat	k5eAaPmAgFnP	nastat
finanční	finanční	k2eAgFnPc1d1	finanční
potíže	potíž	k1gFnPc1	potíž
a	a	k8xC	a
Svenska	Svensko	k1gNnPc1	Svensko
z	z	k7c2	z
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
scény	scéna	k1gFnSc2	scéna
téměř	téměř	k6eAd1	téměř
vymizela	vymizet	k5eAaPmAgFnS	vymizet
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
produkce	produkce	k1gFnSc1	produkce
upadala	upadat	k5eAaImAgFnS	upadat
závratným	závratný	k2eAgNnSc7d1	závratné
tempem	tempo	k1gNnSc7	tempo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Německo	Německo	k1gNnSc1	Německo
ekonomicky	ekonomicky	k6eAd1	ekonomicky
trpělo	trpět	k5eAaImAgNnS	trpět
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
zažíval	zažívat	k5eAaImAgInS	zažívat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
ér	éra	k1gFnPc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
až	až	k6eAd1	až
po	po	k7c4	po
nástup	nástup	k1gInSc4	nástup
nacistů	nacista	k1gMnPc2	nacista
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
německé	německý	k2eAgFnSc3d1	německá
kinematografii	kinematografie	k1gFnSc3	kinematografie
v	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
vyspělosti	vyspělost	k1gFnSc6	vyspělost
mohl	moct	k5eAaImAgInS	moct
rovnat	rovnat	k5eAaImF	rovnat
pouze	pouze	k6eAd1	pouze
Hollywood	Hollywood	k1gInSc4	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
expanzi	expanze	k1gFnSc4	expanze
způsobila	způsobit	k5eAaPmAgFnS	způsobit
především	především	k9	především
izolace	izolace	k1gFnSc1	izolace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
omezení	omezení	k1gNnSc1	omezení
importu	import	k1gInSc2	import
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
filmů	film	k1gInPc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Zadlužené	zadlužený	k2eAgNnSc1d1	zadlužené
Německo	Německo	k1gNnSc1	Německo
si	se	k3xPyFc3	se
uvědomilo	uvědomit	k5eAaPmAgNnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaPmF	stát
velmi	velmi	k6eAd1	velmi
výnosným	výnosný	k2eAgNnSc7d1	výnosné
podnikáním	podnikání	k1gNnSc7	podnikání
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
plně	plně	k6eAd1	plně
podporován	podporovat	k5eAaImNgInS	podporovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
větší	veliký	k2eAgFnPc1d2	veliký
společnosti	společnost	k1gFnPc1	společnost
slučovat	slučovat	k5eAaImF	slučovat
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
společnost	společnost	k1gFnSc4	společnost
Universum-Film-Aktiengesellschaft	Universum-Film-Aktiengesellschafta	k1gFnPc2	Universum-Film-Aktiengesellschafta
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc7d1	známá
obecně	obecně	k6eAd1	obecně
po	po	k7c6	po
svými	svůj	k3xOyFgInPc7	svůj
iniciály	iniciála	k1gFnPc1	iniciála
jako	jako	k8xC	jako
UFA	UFA	kA	UFA
<g/>
.	.	kIx.	.
</s>
<s>
UFA	UFA	kA	UFA
byla	být	k5eAaImAgFnS	být
kartel	kartel	k1gInSc4	kartel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
seskupoval	seskupovat	k5eAaImAgMnS	seskupovat
všechny	všechen	k3xTgMnPc4	všechen
velké	velký	k2eAgMnPc4d1	velký
výrobce	výrobce	k1gMnPc4	výrobce
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
učinil	učinit	k5eAaImAgMnS	učinit
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
Edison	Edisona	k1gFnPc2	Edisona
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
německých	německý	k2eAgInPc2d1	německý
kartelů	kartel	k1gInPc2	kartel
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nelišil	lišit	k5eNaImAgInS	lišit
od	od	k7c2	od
metod	metoda	k1gFnPc2	metoda
amerických	americký	k2eAgInPc2d1	americký
trustů	trust	k1gInPc2	trust
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
monopolistické	monopolistický	k2eAgInPc1d1	monopolistický
cíle	cíl	k1gInPc1	cíl
byly	být	k5eAaImAgInP	být
totožné	totožný	k2eAgInPc1d1	totožný
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc4	tento
finanční	finanční	k2eAgFnPc4d1	finanční
kombinace	kombinace	k1gFnPc4	kombinace
takzvaným	takzvaný	k2eAgInSc7d1	takzvaný
"	"	kIx"	"
<g/>
Shermanovým	Shermanův	k2eAgInSc7d1	Shermanův
zákonem	zákon	k1gInSc7	zákon
<g/>
"	"	kIx"	"
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
zakázány	zakázat	k5eAaPmNgInP	zakázat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
vychvalovány	vychvalován	k2eAgFnPc1d1	vychvalována
a	a	k8xC	a
chráněny	chránit	k5eAaImNgFnP	chránit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
vládních	vládní	k2eAgNnPc2d1	vládní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnSc1	izolace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
silně	silně	k6eAd1	silně
omezila	omezit	k5eAaPmAgFnS	omezit
import	import	k1gInSc4	import
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
minimální	minimální	k2eAgFnSc4d1	minimální
konkurenci	konkurence	k1gFnSc4	konkurence
na	na	k7c6	na
domácím	domácí	k2eAgInSc6d1	domácí
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
k	k	k7c3	k
spolupráci	spolupráce	k1gFnSc6	spolupráce
technické	technický	k2eAgMnPc4d1	technický
pracovníky	pracovník	k1gMnPc4	pracovník
i	i	k9	i
herce	herec	k1gMnPc4	herec
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
proudu	proud	k1gInSc3	proud
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přidávali	přidávat	k5eAaImAgMnP	přidávat
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přicházeli	přicházet	k5eAaImAgMnP	přicházet
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Varšavy	Varšava	k1gFnSc2	Varšava
a	a	k8xC	a
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgNnPc7d1	jiné
např.	např.	kA	např.
Asta	Ast	k2eAgFnSc1d1	Asta
Nielsenová	Nielsenová	k1gFnSc1	Nielsenová
<g/>
,	,	kIx,	,
Pola	pola	k1gFnSc1	pola
Negri	Negr	k1gFnSc2	Negr
<g/>
,	,	kIx,	,
Olaf	Olaf	k1gInSc4	Olaf
Fönss	Fönssa	k1gFnPc2	Fönssa
nebo	nebo	k8xC	nebo
Lupu	lupa	k1gFnSc4	lupa
Pick	Picka	k1gFnPc2	Picka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
zklidnily	zklidnit	k5eAaPmAgInP	zklidnit
nepřátelské	přátelský	k2eNgInPc1d1	nepřátelský
postoje	postoj	k1gInPc1	postoj
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
kinematografie	kinematografie	k1gFnSc1	kinematografie
začala	začít	k5eAaPmAgFnS	začít
sklízet	sklízet	k5eAaImF	sklízet
úspěchy	úspěch	k1gInPc4	úspěch
i	i	k9	i
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
následnou	následný	k2eAgFnSc4d1	následná
hyperinflaci	hyperinflace	k1gFnSc4	hyperinflace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
průmysl	průmysl	k1gInSc1	průmysl
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
začal	začít	k5eAaPmAgInS	začít
těžit	těžit	k5eAaImF	těžit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
utrácet	utrácet	k5eAaImF	utrácet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
šetřily	šetřit	k5eAaImAgInP	šetřit
<g/>
,	,	kIx,	,
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
den	den	k1gInSc4	den
ze	z	k7c2	z
dne	den	k1gInSc2	den
svou	svůj	k3xOyFgFnSc4	svůj
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Surovin	surovina	k1gFnPc2	surovina
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
nedostatek	nedostatek	k1gInSc4	nedostatek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
filmů	film	k1gInPc2	film
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
dost	dost	k6eAd1	dost
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
i	i	k9	i
stavěla	stavět	k5eAaImAgNnP	stavět
další	další	k2eAgNnPc1d1	další
kina	kino	k1gNnPc1	kino
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vybavené	vybavený	k2eAgInPc1d1	vybavený
ateliéry	ateliér	k1gInPc1	ateliér
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
neměly	mít	k5eNaImAgInP	mít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
navíc	navíc	k6eAd1	navíc
prospívala	prospívat	k5eAaImAgFnS	prospívat
exportu	export	k1gInSc3	export
než	než	k8xS	než
importu	import	k1gInSc3	import
a	a	k8xC	a
tak	tak	k9	tak
německé	německý	k2eAgFnSc2d1	německá
společnosti	společnost	k1gFnSc2	společnost
mohli	moct	k5eAaImAgMnP	moct
využívat	využívat	k5eAaPmF	využívat
své	svůj	k3xOyFgFnPc4	svůj
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
konkurence	konkurence	k1gFnPc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
republika	republika	k1gFnSc1	republika
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
finančně	finančně	k6eAd1	finančně
účastnit	účastnit	k5eAaImF	účastnit
koncernu	koncern	k1gInSc2	koncern
UFA	UFA	kA	UFA
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
soukromou	soukromý	k2eAgFnSc4d1	soukromá
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
postupně	postupně	k6eAd1	postupně
zrušila	zrušit	k5eAaPmAgFnS	zrušit
své	svůj	k3xOyFgInPc4	svůj
pobočné	pobočný	k2eAgInPc4d1	pobočný
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgFnSc3d1	sloužící
státní	státní	k2eAgFnSc3d1	státní
propagandě	propaganda	k1gFnSc3	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Velkokapitál	velkokapitál	k1gInSc1	velkokapitál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
i	i	k9	i
nadále	nadále	k6eAd1	nadále
koncern	koncern	k1gInSc1	koncern
ovládal	ovládat	k5eAaImAgInS	ovládat
<g/>
,	,	kIx,	,
nezměnil	změnit	k5eNaPmAgMnS	změnit
však	však	k9	však
nic	nic	k3yNnSc1	nic
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
politické	politický	k2eAgFnSc6d1	politická
orientaci	orientace	k1gFnSc6	orientace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nejlépe	dobře	k6eAd3	dobře
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
další	další	k2eAgInPc4d1	další
Lubitschovy	Lubitschův	k2eAgInPc4d1	Lubitschův
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
nadaný	nadaný	k2eAgMnSc1d1	nadaný
režisér	režisér	k1gMnSc1	režisér
natočil	natočit	k5eAaBmAgMnS	natočit
totiž	totiž	k9	totiž
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
velmi	velmi	k6eAd1	velmi
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
protifrancouzskou	protifrancouzský	k2eAgFnSc4d1	protifrancouzská
Madame	madame	k1gFnSc4	madame
DuBarry	DuBarra	k1gFnSc2	DuBarra
a	a	k8xC	a
protiamerickou	protiamerický	k2eAgFnSc4d1	protiamerická
Ústřicovou	ústřicový	k2eAgFnSc4d1	ústřicová
princeznu	princezna	k1gFnSc4	princezna
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Austernprinzessin	Austernprinzessin	k1gMnSc1	Austernprinzessin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Madame	madame	k1gFnSc2	madame
DuBarry	DuBarra	k1gFnSc2	DuBarra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
za	za	k7c4	za
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
američtí	americký	k2eAgMnPc1d1	americký
experti	expert	k1gMnPc1	expert
odhadovali	odhadovat	k5eAaImAgMnP	odhadovat
za	za	k7c4	za
náklady	náklad	k1gInPc4	náklad
nejméně	málo	k6eAd3	málo
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
drahý	drahý	k2eAgInSc4d1	drahý
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
Lubitsch	Lubitsch	k1gMnSc1	Lubitsch
prvním	první	k4xOgMnSc6	první
významným	významný	k2eAgMnSc7d1	významný
režisérem	režisér	k1gMnSc7	režisér
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
natáčet	natáčet	k5eAaImF	natáčet
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Negativem	negativ	k1gInSc7	negativ
izolace	izolace	k1gFnSc2	izolace
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
stejný	stejný	k2eAgInSc1d1	stejný
typ	typ	k1gInSc1	typ
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
komedií	komedie	k1gFnPc2	komedie
a	a	k8xC	a
dramat	drama	k1gNnPc2	drama
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dominoval	dominovat	k5eAaImAgInS	dominovat
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
pomalu	pomalu	k6eAd1	pomalu
formovat	formovat	k5eAaImF	formovat
nové	nový	k2eAgInPc4d1	nový
trendy	trend	k1gInPc4	trend
–	–	k?	–
německé	německý	k2eAgNnSc1d1	německé
expresionistické	expresionistický	k2eAgNnSc1d1	expresionistické
hnutí	hnutí	k1gNnSc1	hnutí
a	a	k8xC	a
"	"	kIx"	"
<g/>
kammerspiel	kammerspiel	k1gInSc1	kammerspiel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
expresionismu	expresionismus	k1gInSc6	expresionismus
převládala	převládat	k5eAaImAgFnS	převládat
hrůza	hrůza	k1gFnSc1	hrůza
<g/>
,	,	kIx,	,
fantastičnost	fantastičnost	k1gFnSc1	fantastičnost
a	a	k8xC	a
zločin	zločin	k1gInSc1	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
originalita	originalita	k1gFnSc1	originalita
sklidila	sklidit	k5eAaPmAgFnS	sklidit
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
těšit	těšit	k5eAaImF	těšit
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Rakušan	Rakušan	k1gMnSc1	Rakušan
Carl	Carl	k1gMnSc1	Carl
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaBmAgMnS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Janowitzem	Janowitz	k1gMnSc7	Janowitz
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc4	scénář
filmu	film	k1gInSc2	film
Kabinet	kabinet	k1gInSc1	kabinet
doktora	doktor	k1gMnSc2	doktor
Caligariho	Caligari	k1gMnSc2	Caligari
(	(	kIx(	(
<g/>
Kabinett	Kabinett	k1gInSc1	Kabinett
des	des	k1gNnSc2	des
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Caligari	Caligar	k1gMnSc6	Caligar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
osobností	osobnost	k1gFnSc7	osobnost
německého	německý	k2eAgInSc2d1	německý
filmu	film	k1gInSc2	film
němé	němý	k2eAgFnSc2d1	němá
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Caligari	Caligari	k1gNnPc2	Caligari
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
postavou	postava	k1gFnSc7	postava
obecně	obecně	k6eAd1	obecně
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xC	jako
Harpagon	Harpagon	k1gMnSc1	Harpagon
nebo	nebo	k8xC	nebo
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
tragický	tragický	k2eAgInSc1d1	tragický
"	"	kIx"	"
<g/>
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
bez	bez	k7c2	bez
literární	literární	k2eAgFnSc2d1	literární
předlohy	předloha	k1gFnSc2	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Conrad	Conrad	k1gInSc1	Conrad
Veidt	Veidta	k1gFnPc2	Veidta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
Cesara	Cesar	k1gMnSc4	Cesar
v	v	k7c4	v
Caligarim	Caligarim	k1gInSc4	Caligarim
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
dekorace	dekorace	k1gFnPc1	dekorace
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
duševnímu	duševní	k2eAgNnSc3d1	duševní
rozpoložení	rozpoložení	k1gNnSc3	rozpoložení
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
herci	herec	k1gMnPc1	herec
neocenitelnou	ocenitelný	k2eNgFnSc4d1	neocenitelná
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
a	a	k8xC	a
prožívaní	prožívaný	k2eAgMnPc1d1	prožívaný
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Splyne	splynout	k5eAaPmIp3nS	splynout
se	s	k7c7	s
zobrazeným	zobrazený	k2eAgNnSc7d1	zobrazené
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
pohybovat	pohybovat	k5eAaImF	pohybovat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
rytmu	rytmus	k1gInSc6	rytmus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výraznou	výrazný	k2eAgFnSc7d1	výrazná
osobností	osobnost	k1gFnSc7	osobnost
rané	raný	k2eAgFnSc2d1	raná
německé	německý	k2eAgFnSc2d1	německá
kinematografie	kinematografie	k1gFnSc2	kinematografie
byl	být	k5eAaImAgMnS	být
Fritz	Fritz	k1gMnSc1	Fritz
Lang	Lang	k1gMnSc1	Lang
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc2	Hollywood
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
natočit	natočit	k5eAaBmF	natočit
desítky	desítka	k1gFnPc4	desítka
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
návštěvě	návštěva	k1gFnSc6	návštěva
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
byl	být	k5eAaImAgMnS	být
natolik	natolik	k6eAd1	natolik
překvapen	překvapit	k5eAaPmNgMnS	překvapit
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
natočil	natočit	k5eAaBmAgInS	natočit
film	film	k1gInSc1	film
Metropolis	Metropolis	k1gFnSc2	Metropolis
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
děsivou	děsivý	k2eAgFnSc4d1	děsivá
vizi	vize	k1gFnSc4	vize
budoucího	budoucí	k2eAgNnSc2d1	budoucí
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
vládců	vládce	k1gMnPc2	vládce
a	a	k8xC	a
dělníky	dělník	k1gMnPc7	dělník
otroky	otrok	k1gMnPc4	otrok
pracující	pracující	k2eAgMnPc1d1	pracující
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
využito	využit	k2eAgNnSc1d1	využito
silné	silný	k2eAgNnSc1d1	silné
promyšlené	promyšlený	k2eAgNnSc1d1	promyšlené
estetiky	estetika	k1gFnPc4	estetika
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
záběrů	záběr	k1gInPc2	záběr
<g/>
,	,	kIx,	,
geometrie	geometrie	k1gFnSc2	geometrie
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
opakujících	opakující	k2eAgFnPc2d1	opakující
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
mas	masa	k1gFnPc2	masa
dělníků	dělník	k1gMnPc2	dělník
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
obrazcích	obrazec	k1gInPc6	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
rysy	rys	k1gInPc7	rys
expresionismu	expresionismus	k1gInSc2	expresionismus
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
kinematografii	kinematografie	k1gFnSc6	kinematografie
byly	být	k5eAaImAgFnP	být
deformace	deformace	k1gFnPc1	deformace
a	a	k8xC	a
zveličení	zveličení	k1gNnPc1	zveličení
<g/>
,	,	kIx,	,
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
špičaté	špičatý	k2eAgInPc1d1	špičatý
a	a	k8xC	a
pokroucené	pokroucený	k2eAgInPc1d1	pokroucený
<g/>
,	,	kIx,	,
židle	židle	k1gFnPc1	židle
vysoké	vysoká	k1gFnSc2	vysoká
a	a	k8xC	a
schodiště	schodiště	k1gNnSc2	schodiště
křivá	křivý	k2eAgFnSc1d1	křivá
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
se	se	k3xPyFc4	se
nesnažili	snažit	k5eNaImAgMnP	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
přirozeného	přirozený	k2eAgInSc2d1	přirozený
dojmu	dojem	k1gInSc2	dojem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dělali	dělat	k5eAaImAgMnP	dělat
trhané	trhaný	k2eAgInPc4d1	trhaný
pohyby	pohyb	k1gInPc4	pohyb
a	a	k8xC	a
nečekaně	nečekaně	k6eAd1	nečekaně
prudká	prudký	k2eAgNnPc1d1	prudké
gesta	gesto	k1gNnPc1	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
nasvícení	nasvícení	k1gNnSc2	nasvícení
zepředu	zepředu	k6eAd1	zepředu
a	a	k8xC	a
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
sloučení	sloučení	k1gNnSc1	sloučení
mezi	mezi	k7c7	mezi
hercem	herec	k1gMnSc7	herec
a	a	k8xC	a
pozadím	pozadí	k1gNnSc7	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
i	i	k9	i
stínů	stín	k1gInPc2	stín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
deformace	deformace	k1gFnSc1	deformace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
trendem	trend	k1gInSc7	trend
byl	být	k5eAaImAgMnS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kammerspiel	kammerspiel	k1gMnSc1	kammerspiel
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
komorního	komorní	k2eAgNnSc2d1	komorní
dramatu	drama	k1gNnSc2	drama
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
cílem	cíl	k1gInSc7	cíl
zobrazit	zobrazit	k5eAaPmF	zobrazit
několik	několik	k4yIc4	několik
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
zkoumat	zkoumat	k5eAaImF	zkoumat
jejich	jejich	k3xOp3gFnPc4	jejich
životní	životní	k2eAgFnPc4d1	životní
krize	krize	k1gFnPc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Kladl	klást	k5eAaImAgMnS	klást
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
výrazné	výrazný	k2eAgInPc4d1	výrazný
detaily	detail	k1gInPc4	detail
a	a	k8xC	a
psychologii	psychologie	k1gFnSc4	psychologie
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
"	"	kIx"	"
<g/>
konkurenčního	konkurenční	k2eAgInSc2d1	konkurenční
<g/>
"	"	kIx"	"
expresionismu	expresionismus	k1gInSc2	expresionismus
byl	být	k5eAaImAgInS	být
děj	děj	k1gInSc1	děj
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
současného	současný	k2eAgNnSc2d1	současné
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zůstával	zůstávat	k5eAaImAgInS	zůstávat
stále	stále	k6eAd1	stále
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
Závěry	závěr	k1gInPc1	závěr
filmů	film	k1gInPc2	film
typu	typ	k1gInSc2	typ
kammerspiel	kammerspiela	k1gFnPc2	kammerspiela
pak	pak	k6eAd1	pak
končily	končit	k5eAaImAgInP	končit
vždy	vždy	k6eAd1	vždy
nešťastně	šťastně	k6eNd1	šťastně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
žánru	žánr	k1gInSc6	žánr
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
jen	jen	k9	jen
několik	několik	k4yIc1	několik
filmů	film	k1gInPc2	film
<g/>
:	:	kIx,	:
Střepy	střep	k1gInPc1	střep
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
a	a	k8xC	a
Silvestrovská	silvestrovský	k2eAgFnSc1d1	silvestrovská
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
Lupu	lupa	k1gFnSc4	lupa
Picka	Picek	k1gMnSc2	Picek
<g/>
,	,	kIx,	,
Zadní	zadní	k2eAgNnSc1d1	zadní
schodiště	schodiště	k1gNnSc1	schodiště
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
Leopolda	Leopold	k1gMnSc2	Leopold
Jessnera	Jessner	k1gMnSc2	Jessner
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
štace	štace	k1gFnSc1	štace
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Friedricha	Friedrich	k1gMnSc4	Friedrich
Murnaua	Murnauus	k1gMnSc4	Murnauus
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
Carla	Carl	k1gMnSc4	Carl
Dreyera	Dreyer	k1gMnSc4	Dreyer
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
žádaným	žádaný	k2eAgInSc7d1	žádaný
žánrem	žánr	k1gInSc7	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Říjnová	říjnový	k2eAgFnSc1d1	říjnová
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
chtěla	chtít	k5eAaImAgFnS	chtít
mít	mít	k5eAaImF	mít
neustálý	neustálý	k2eAgInSc4d1	neustálý
dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
filmovou	filmový	k2eAgFnSc7d1	filmová
produkcí	produkce	k1gFnSc7	produkce
a	a	k8xC	a
tak	tak	k6eAd1	tak
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Lidový	lidový	k2eAgInSc4d1	lidový
komisariát	komisariát	k1gInSc4	komisariát
osvěty	osvěta	k1gFnSc2	osvěta
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
Narkompros	Narkomprosa	k1gFnPc2	Narkomprosa
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
filmovou	filmový	k2eAgFnSc7d1	filmová
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
distribucí	distribuce	k1gFnSc7	distribuce
a	a	k8xC	a
promítáním	promítání	k1gNnSc7	promítání
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
revolucí	revoluce	k1gFnSc7	revoluce
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
natočil	natočit	k5eAaBmAgMnS	natočit
nadaný	nadaný	k2eAgMnSc1d1	nadaný
ruský	ruský	k2eAgMnSc1d1	ruský
režisér	režisér	k1gMnSc1	režisér
Lev	Lev	k1gMnSc1	Lev
Kulešov	Kulešov	k1gInSc4	Kulešov
film	film	k1gInSc1	film
Projekt	projekt	k1gInSc1	projekt
inženýra	inženýr	k1gMnSc2	inženýr
Praita	Prait	k1gMnSc2	Prait
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
využil	využít	k5eAaPmAgInS	využít
prvky	prvek	k1gInPc4	prvek
kontinuity	kontinuita	k1gFnSc2	kontinuita
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
přes	přes	k7c4	přes
menší	malý	k2eAgInSc4d2	menší
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
rozvoj	rozvoj	k1gInSc4	rozvoj
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
kinematografii	kinematografie	k1gFnSc6	kinematografie
úpadek	úpadek	k1gInSc4	úpadek
<g/>
,	,	kIx,	,
chyběl	chybět	k5eAaImAgInS	chybět
totiž	totiž	k9	totiž
filmový	filmový	k2eAgInSc1d1	filmový
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Květnu	květen	k1gInSc6	květen
1918	[number]	k4	1918
svěřila	svěřit	k5eAaPmAgFnS	svěřit
vláda	vláda	k1gFnSc1	vláda
jeden	jeden	k4xCgInSc1	jeden
milion	milion	k4xCgInSc1	milion
dolarů	dolar	k1gInPc2	dolar
filmovému	filmový	k2eAgInSc3d1	filmový
distributoru	distributor	k1gMnSc3	distributor
Jacquesu	Jacques	k1gMnSc5	Jacques
Cibrariovi	Cibrarius	k1gMnSc6	Cibrarius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
za	za	k7c2	za
války	válka	k1gFnSc2	válka
působil	působit	k5eAaImAgMnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
obchodní	obchodní	k2eAgFnSc6d1	obchodní
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Cibrario	Cibrario	k6eAd1	Cibrario
nakoupil	nakoupit	k5eAaPmAgInS	nakoupit
bezcenný	bezcenný	k2eAgInSc4d1	bezcenný
použitý	použitý	k2eAgInSc4d1	použitý
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
peněz	peníze	k1gInPc2	peníze
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
režim	režim	k1gInSc1	režim
pokusil	pokusit	k5eAaPmAgInS	pokusit
veškerý	veškerý	k3xTgInSc4	veškerý
zbylý	zbylý	k2eAgInSc4d1	zbylý
materiál	materiál	k1gInSc4	materiál
znárodnit	znárodnit	k5eAaPmF	znárodnit
<g/>
,	,	kIx,	,
soukromí	soukromý	k2eAgMnPc1d1	soukromý
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
distributoři	distributor	k1gMnPc1	distributor
skryly	skrýt	k5eAaPmAgFnP	skrýt
své	svůj	k3xOyFgFnPc4	svůj
poslední	poslední	k2eAgFnPc4d1	poslední
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
nastal	nastat	k5eAaPmAgInS	nastat
tak	tak	k9	tak
jeho	jeho	k3xOp3gInSc1	jeho
absolutní	absolutní	k2eAgInSc1d1	absolutní
nedostatek	nedostatek	k1gInSc1	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
Narkompros	Narkomprosa	k1gFnPc2	Narkomprosa
založil	založit	k5eAaPmAgInS	založit
Státní	státní	k2eAgFnSc4d1	státní
filmovou	filmový	k2eAgFnSc4d1	filmová
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
připojil	připojit	k5eAaPmAgInS	připojit
Kulešov	Kulešov	k1gInSc1	Kulešov
i	i	k8xC	i
jiní	jiný	k2eAgMnPc1d1	jiný
významní	významný	k2eAgMnPc1d1	významný
režiséři	režisér	k1gMnPc1	režisér
a	a	k8xC	a
herci	herec	k1gMnPc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
započala	započnout	k5eAaPmAgFnS	započnout
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
země	zem	k1gFnSc2	zem
a	a	k8xC	a
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Lenina	Lenin	k1gMnSc2	Lenin
začala	začít	k5eAaPmAgFnS	začít
vzkvétat	vzkvétat	k5eAaImF	vzkvétat
i	i	k9	i
ruská	ruský	k2eAgFnSc1d1	ruská
kinematografie	kinematografie	k1gFnSc1	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
filmové	filmový	k2eAgInPc4d1	filmový
ateliéry	ateliér	k1gInPc4	ateliér
a	a	k8xC	a
umělci	umělec	k1gMnPc1	umělec
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
začali	začít	k5eAaPmAgMnP	začít
seskupovat	seskupovat	k5eAaImF	seskupovat
v	v	k7c6	v
hlavních	hlavní	k2eAgInPc6d1	hlavní
centrech	centr	k1gInPc6	centr
výroby	výroba	k1gFnSc2	výroba
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Lenin	Lenin	k1gMnSc1	Lenin
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
umění	umění	k1gNnPc2	umění
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
film	film	k1gInSc1	film
uměním	umění	k1gNnSc7	umění
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
pokusila	pokusit	k5eAaPmAgFnS	pokusit
sjednotit	sjednotit	k5eAaPmF	sjednotit
filmový	filmový	k2eAgInSc4d1	filmový
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
distribuční	distribuční	k2eAgInSc4d1	distribuční
monopol	monopol	k1gInSc4	monopol
Goskino	Goskino	k1gNnSc1	Goskino
(	(	kIx(	(
<g/>
Státní	státní	k2eAgInSc1d1	státní
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vytvoření	vytvoření	k1gNnSc1	vytvoření
institutu	institut	k1gInSc2	institut
nesplnilo	splnit	k5eNaPmAgNnS	splnit
očekávání	očekávání	k1gNnSc1	očekávání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
několik	několik	k4yIc1	několik
společností	společnost	k1gFnPc2	společnost
bylo	být	k5eAaImAgNnS	být
natolik	natolik	k6eAd1	natolik
silných	silný	k2eAgNnPc2d1	silné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohly	moct	k5eAaImAgFnP	moct
Goskinu	Goskina	k1gFnSc4	Goskina
konkurovat	konkurovat	k5eAaImF	konkurovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
silnou	silný	k2eAgFnSc4d1	silná
konkurenci	konkurence	k1gFnSc4	konkurence
Goskino	Goskino	k1gNnSc4	Goskino
natočil	natočit	k5eAaBmAgInS	natočit
asi	asi	k9	asi
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
ruský	ruský	k2eAgInSc1d1	ruský
film	film	k1gInSc1	film
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Křižník	křižník	k1gInSc4	křižník
Potěmkin	Potěmkin	k1gMnSc1	Potěmkin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
natočil	natočit	k5eAaBmAgMnS	natočit
nadaný	nadaný	k2eAgMnSc1d1	nadaný
ruský	ruský	k2eAgMnSc1d1	ruský
režisér	režisér	k1gMnSc1	režisér
Sergej	Sergej	k1gMnSc1	Sergej
Ejzenštejn	Ejzenštejno	k1gNnPc2	Ejzenštejno
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgMnPc3d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
byly	být	k5eAaImAgFnP	být
bohatě	bohatě	k6eAd1	bohatě
využity	využít	k5eAaPmNgFnP	využít
dramatické	dramatický	k2eAgFnPc1d1	dramatická
montáže	montáž	k1gFnPc1	montáž
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
záběrově	záběrově	k6eAd1	záběrově
a	a	k8xC	a
fotograficky	fotograficky	k6eAd1	fotograficky
snímané	snímaný	k2eAgNnSc1d1	snímané
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
kameramanů	kameraman	k1gMnPc2	kameraman
<g/>
,	,	kIx,	,
Eduardem	Eduard	k1gMnSc7	Eduard
Tissém	Tissý	k2eAgNnSc6d1	Tissý
<g/>
.	.	kIx.	.
</s>
<s>
Ejzenštejn	Ejzenštejn	k1gMnSc1	Ejzenštejn
zachytil	zachytit	k5eAaPmAgMnS	zachytit
osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
předměty	předmět	k1gInPc4	předmět
"	"	kIx"	"
<g/>
nestřežené	střežený	k2eNgInPc4d1	nestřežený
<g/>
"	"	kIx"	"
–	–	k?	–
vysoké	vysoký	k2eAgFnPc4d1	vysoká
boty	bota	k1gFnPc4	bota
<g/>
,	,	kIx,	,
schody	schod	k1gInPc4	schod
<g/>
,	,	kIx,	,
mříže	mříž	k1gFnPc4	mříž
<g/>
,	,	kIx,	,
šavle	šavle	k1gFnPc4	šavle
<g/>
,	,	kIx,	,
kamenného	kamenný	k2eAgInSc2d1	kamenný
lva	lev	k1gInSc2	lev
apod.	apod.	kA	apod.
Dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
filmu	film	k1gInSc6	film
využil	využít	k5eAaPmAgMnS	využít
svých	svůj	k3xOyFgFnPc2	svůj
střihových	střihový	k2eAgFnPc2d1	střihová
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
rytmickou	rytmický	k2eAgFnSc7d1	rytmická
montáží	montáž	k1gFnSc7	montáž
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
stále	stále	k6eAd1	stále
rychlejších	rychlý	k2eAgInPc6d2	rychlejší
střizích	střih	k1gInPc6	střih
ukázal	ukázat	k5eAaPmAgInS	ukázat
střídající	střídající	k2eAgInPc1d1	střídající
se	se	k3xPyFc4	se
detaily	detail	k1gInPc7	detail
otáčejícího	otáčející	k2eAgNnSc2d1	otáčející
se	se	k3xPyFc4	se
soukolí	soukolí	k1gNnSc2	soukolí
křižníku	křižník	k1gInSc6	křižník
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
firma	firma	k1gFnSc1	firma
Goskino	Goskino	k1gNnSc4	Goskino
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vést	vést	k5eAaImF	vést
filmový	filmový	k2eAgInSc4d1	filmový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
vláda	vláda	k1gFnSc1	vláda
novou	nový	k2eAgFnSc4d1	nová
společnost	společnost	k1gFnSc4	společnost
Sovkino	Sovkin	k2eAgNnSc1d1	Sovkin
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
vlastnily	vlastnit	k5eAaImAgFnP	vlastnit
a	a	k8xC	a
financovaly	financovat	k5eAaBmAgFnP	financovat
němečtí	německý	k2eAgMnPc1d1	německý
komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
,	,	kIx,	,
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgMnPc2d1	významný
sovětských	sovětský	k2eAgMnPc2d1	sovětský
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
experimenty	experiment	k1gInPc1	experiment
však	však	k9	však
nevydržely	vydržet	k5eNaPmAgInP	vydržet
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
si	se	k3xPyFc3	se
začala	začít	k5eAaPmAgFnS	začít
stěžovat	stěžovat	k5eAaImF	stěžovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
intelektuální	intelektuální	k2eAgMnPc1d1	intelektuální
a	a	k8xC	a
prostí	prostý	k2eAgMnPc1d1	prostý
rolníci	rolník	k1gMnPc1	rolník
a	a	k8xC	a
dělníci	dělník	k1gMnPc1	dělník
nemají	mít	k5eNaImIp3nP	mít
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
pochopení	pochopení	k1gNnSc4	pochopení
<g/>
.	.	kIx.	.
</s>
<s>
Stalinistická	stalinistický	k2eAgFnSc1d1	stalinistická
totalita	totalita	k1gFnSc1	totalita
donutila	donutit	k5eAaPmAgFnS	donutit
ruskou	ruský	k2eAgFnSc4d1	ruská
kinematografii	kinematografie	k1gFnSc4	kinematografie
k	k	k7c3	k
natáčení	natáčení	k1gNnSc3	natáčení
filmů	film	k1gInPc2	film
typu	typ	k1gInSc2	typ
lehké	lehký	k2eAgFnSc2d1	lehká
zábavy	zábava	k1gFnSc2	zábava
nebo	nebo	k8xC	nebo
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
pobídkou	pobídka	k1gFnSc7	pobídka
k	k	k7c3	k
budování	budování	k1gNnSc3	budování
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Umělecký	umělecký	k2eAgInSc1d1	umělecký
film	film	k1gInSc1	film
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
tak	tak	k9	tak
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
ideologický	ideologický	k2eAgInSc4d1	ideologický
i	i	k8xC	i
praktický	praktický	k2eAgInSc4d1	praktický
zmatek	zmatek	k1gInSc4	zmatek
a	a	k8xC	a
obrazem	obraz	k1gInSc7	obraz
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
nových	nový	k2eAgInPc2d1	nový
uměleckých	umělecký	k2eAgInPc2d1	umělecký
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nenavazovaly	navazovat	k5eNaImAgInP	navazovat
na	na	k7c4	na
stará	starý	k2eAgNnPc4d1	staré
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
důležité	důležitý	k2eAgFnPc1d1	důležitá
myšlenky	myšlenka	k1gFnPc1	myšlenka
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
přepadu	přepad	k1gInSc6	přepad
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
několik	několik	k4yIc1	několik
umělců	umělec	k1gMnPc2	umělec
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
centrem	centr	k1gMnSc7	centr
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
filmaři	filmař	k1gMnPc1	filmař
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
již	již	k9	již
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mohli	moct	k5eAaImAgMnP	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pokusech	pokus	k1gInPc6	pokus
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Pařížských	pařížský	k2eAgFnPc2d1	Pařížská
CAFÉs	CAFÉsa	k1gFnPc2	CAFÉsa
se	se	k3xPyFc4	se
scházela	scházet	k5eAaImAgFnS	scházet
spousta	spousta	k1gFnSc1	spousta
mladých	mladý	k2eAgMnPc2d1	mladý
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
plni	pln	k2eAgMnPc1d1	pln
kreativity	kreativita	k1gFnSc2	kreativita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiální	oficiální	k2eAgFnSc1d1	oficiální
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
neuznávala	uznávat	k5eNaImAgFnS	uznávat
(	(	kIx(	(
<g/>
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Georges	Georges	k1gMnSc1	Georges
Braque	Braqu	k1gFnSc2	Braqu
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gInSc4	Salvador
Dalí	Dal	k1gFnPc2	Dal
<g/>
,	,	kIx,	,
Luis	Luisa	k1gFnPc2	Luisa
Buñ	Buñ	k1gFnPc2	Buñ
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgMnPc1	první
filmoví	filmový	k2eAgMnPc1d1	filmový
kritici	kritik	k1gMnPc1	kritik
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Louis	Louis	k1gMnSc1	Louis
Delluc	Delluc	k1gInSc1	Delluc
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgMnSc1	tento
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
vznik	vznik	k1gInSc4	vznik
časopisu	časopis	k1gInSc2	časopis
Ciné-club	Cinéluba	k1gFnPc2	Ciné-cluba
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sice	sice	k8xC	sice
fungoval	fungovat	k5eAaImAgInS	fungovat
pouze	pouze	k6eAd1	pouze
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
existencí	existence	k1gFnSc7	existence
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
několik	několik	k4yIc4	několik
projekcí	projekce	k1gFnPc2	projekce
–	–	k?	–
počátky	počátek	k1gInPc4	počátek
tradic	tradice	k1gFnPc2	tradice
filmových	filmový	k2eAgInPc2d1	filmový
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Delluc	Delluc	k6eAd1	Delluc
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
režiséry	režisér	k1gMnPc7	režisér
(	(	kIx(	(
<g/>
Germaine	Germain	k1gInSc5	Germain
Dulacová	Dulacová	k1gFnSc5	Dulacová
<g/>
,	,	kIx,	,
Abel	Abel	k1gMnSc1	Abel
Gance	Gance	k1gMnSc1	Gance
<g/>
,	,	kIx,	,
Marcel	Marcel	k1gMnSc1	Marcel
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Herbier	Herbier	k1gMnSc1	Herbier
a	a	k8xC	a
Jean	Jean	k1gMnSc1	Jean
Epstein	Epstein	k1gMnSc1	Epstein
<g/>
)	)	kIx)	)
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
impresionistickému	impresionistický	k2eAgNnSc3d1	impresionistické
filmovému	filmový	k2eAgNnSc3d1	filmové
umění	umění	k1gNnSc3	umění
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
nepřenáší	přenášet	k5eNaImIp3nS	přenášet
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zkušenost	zkušenost	k1gFnSc1	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1	osobní
pohled	pohled	k1gInSc1	pohled
umělce	umělec	k1gMnSc2	umělec
a	a	k8xC	a
"	"	kIx"	"
<g/>
pocity	pocit	k1gInPc4	pocit
místo	místo	k7c2	místo
příběhů	příběh	k1gInPc2	příběh
<g/>
"	"	kIx"	"
měly	mít	k5eAaImAgInP	mít
tvořit	tvořit	k5eAaImF	tvořit
jádro	jádro	k1gNnSc1	jádro
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
nechat	nechat	k5eAaPmF	nechat
jej	on	k3xPp3gInSc4	on
stát	stát	k1gInSc4	stát
s	s	k7c7	s
"	"	kIx"	"
<g/>
poetickým	poetický	k2eAgInSc7d1	poetický
výrazem	výraz	k1gInSc7	výraz
duše	duše	k1gFnSc2	duše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Delluc	Delluc	k1gInSc1	Delluc
definoval	definovat	k5eAaBmAgInS	definovat
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
konceptem	koncept	k1gInSc7	koncept
"	"	kIx"	"
<g/>
Photogenie	Photogenie	k1gFnSc2	Photogenie
<g/>
"	"	kIx"	"
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
filmový	filmový	k2eAgInSc4d1	filmový
obraz	obraz	k1gInSc4	obraz
od	od	k7c2	od
zobrazovaného	zobrazovaný	k2eAgInSc2d1	zobrazovaný
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
Delluc	Delluc	k1gFnSc4	Delluc
<g/>
,	,	kIx,	,
propůjčuje	propůjčovat	k5eAaImIp3nS	propůjčovat
předmětu	předmět	k1gInSc3	předmět
nový	nový	k2eAgInSc1d1	nový
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
diváku	divák	k1gMnSc3	divák
skrze	skrze	k?	skrze
pohled	pohled	k1gInSc1	pohled
filmaře	filmař	k1gMnSc2	filmař
otvírá	otvírat	k5eAaImIp3nS	otvírat
nový	nový	k2eAgInSc4d1	nový
rozhled	rozhled	k1gInSc4	rozhled
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
různými	různý	k2eAgFnPc7d1	různá
formami	forma	k1gFnPc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
hlavních	hlavní	k2eAgNnPc6d1	hlavní
městech	město	k1gNnPc6	město
byla	být	k5eAaImAgNnP	být
otevřena	otevřen	k2eAgNnPc1d1	otevřeno
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
kina	kino	k1gNnPc1	kino
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgNnP	nazývat
někdy	někdy	k6eAd1	někdy
"	"	kIx"	"
<g/>
studia	studio	k1gNnPc1	studio
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
avantgardní	avantgardní	k2eAgNnPc1d1	avantgardní
kina	kino	k1gNnPc1	kino
<g/>
"	"	kIx"	"
a	a	k8xC	a
tvořila	tvořit	k5eAaImAgFnS	tvořit
obdobu	obdoba	k1gFnSc4	obdoba
avantgardních	avantgardní	k2eAgNnPc2d1	avantgardní
divadel	divadlo	k1gNnPc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
například	například	k6eAd1	například
Uršulinky	Uršulinka	k1gFnSc2	Uršulinka
(	(	kIx(	(
<g/>
Ursulines	Ursulines	k1gInSc1	Ursulines
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dvojice	dvojice	k1gFnSc1	dvojice
herců	herec	k1gMnPc2	herec
Armand	Armand	k1gInSc4	Armand
Tallier	Tallira	k1gFnPc2	Tallira
a	a	k8xC	a
Laurence	Laurence	k1gFnSc1	Laurence
Myrga	Myrga	k1gFnSc1	Myrga
formulovala	formulovat	k5eAaImAgFnS	formulovat
svoje	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
při	při	k7c6	při
otevření	otevření	k1gNnSc6	otevření
studia	studio	k1gNnSc2	studio
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Získávat	získávat	k5eAaImF	získávat
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
mezi	mezi	k7c7	mezi
elitou	elita	k1gFnSc7	elita
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
intelektuálů	intelektuál	k1gMnPc2	intelektuál
z	z	k7c2	z
Latinské	latinský	k2eAgFnSc2d1	Latinská
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
...	...	k?	...
Na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
plátně	plátno	k1gNnSc6	plátno
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
místo	místo	k1gNnSc4	místo
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
znamená	znamenat	k5eAaImIp3nS	znamenat
původnost	původnost	k1gFnSc4	původnost
<g/>
,	,	kIx,	,
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Impresionističtí	impresionistický	k2eAgMnPc1d1	impresionistický
režiséři	režisér	k1gMnPc1	režisér
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zobrazit	zobrazit	k5eAaPmF	zobrazit
obraz	obraz	k1gInSc4	obraz
samotný	samotný	k2eAgInSc4d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Využívali	využívat	k5eAaPmAgMnP	využívat
optické	optický	k2eAgInPc4d1	optický
triky	trik	k1gInPc4	trik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ukázali	ukázat	k5eAaPmAgMnP	ukázat
pocity	pocit	k1gInPc4	pocit
postav	postava	k1gFnPc2	postava
-	-	kIx~	-
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
,	,	kIx,	,
vize	vize	k1gFnPc1	vize
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc1	myšlenka
<g/>
..	..	k?	..
Také	také	k9	také
používali	používat	k5eAaImAgMnP	používat
"	"	kIx"	"
<g/>
subjektivní	subjektivní	k2eAgFnSc4d1	subjektivní
kameru	kamera	k1gFnSc4	kamera
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
extrémní	extrémní	k2eAgFnSc4d1	extrémní
perspektivu	perspektiva	k1gFnSc4	perspektiva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
scenérii	scenérie	k1gFnSc4	scenérie
očima	oko	k1gNnPc7	oko
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
váhu	váha	k1gFnSc4	váha
kladli	klást	k5eAaImAgMnP	klást
impresionisté	impresionista	k1gMnPc1	impresionista
na	na	k7c6	na
"	"	kIx"	"
<g/>
mise-en-scéne	misencén	k1gInSc5	mise-en-scén
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
inscenaci	inscenace	k1gFnSc4	inscenace
filmového	filmový	k2eAgInSc2d1	filmový
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
avantgardisty	avantgardista	k1gMnPc4	avantgardista
patřil	patřit	k5eAaImAgMnS	patřit
Marcel	Marcel	k1gMnSc1	Marcel
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Herbier	Herbier	k1gMnSc1	Herbier
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
navodit	navodit	k5eAaBmF	navodit
působivou	působivý	k2eAgFnSc4d1	působivá
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
asi	asi	k9	asi
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
dílem	díl	k1gInSc7	díl
byl	být	k5eAaImAgInS	být
Eldorado	Eldorada	k1gFnSc5	Eldorada
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
plný	plný	k2eAgInSc4d1	plný
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
u	u	k7c2	u
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
úspěch	úspěch	k1gInSc1	úspěch
a	a	k8xC	a
francouzský	francouzský	k2eAgInSc1d1	francouzský
Ciné-club	Cinélub	k1gInSc1	Ciné-club
pak	pak	k6eAd1	pak
ocenil	ocenit	k5eAaPmAgInS	ocenit
především	především	k9	především
sloh	sloh	k1gInSc1	sloh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
především	především	k6eAd1	především
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
subjektivismem	subjektivismus	k1gInSc7	subjektivismus
<g/>
.	.	kIx.	.
</s>
<s>
Jean	Jean	k1gMnSc1	Jean
Epstein	Epstein	k1gMnSc1	Epstein
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
prosadil	prosadit	k5eAaPmAgMnS	prosadit
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
avantgardě	avantgarda	k1gFnSc6	avantgarda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
Červené	Červené	k2eAgFnSc6d1	Červené
krčmě	krčma	k1gFnSc6	krčma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vynikala	vynikat	k5eAaImAgFnS	vynikat
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
montáží	montáž	k1gFnSc7	montáž
<g/>
,	,	kIx,	,
natočil	natočit	k5eAaBmAgMnS	natočit
ještě	ještě	k6eAd1	ještě
úspěšnější	úspěšný	k2eAgInSc4d2	úspěšnější
film	film	k1gInSc4	film
Věrné	věrný	k2eAgNnSc1d1	věrné
srdce	srdce	k1gNnSc1	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Estetické	estetický	k2eAgInPc4d1	estetický
objevy	objev	k1gInPc4	objev
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
snímku	snímek	k1gInSc6	snímek
družily	družit	k5eAaImAgInP	družit
ke	k	k7c3	k
klasickému	klasický	k2eAgInSc3d1	klasický
naturalistickému	naturalistický	k2eAgInSc3d1	naturalistický
námětu	námět	k1gInSc3	námět
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
bodem	bod	k1gInSc7	bod
a	a	k8xC	a
skutečným	skutečný	k2eAgInSc7d1	skutečný
příspěvkem	příspěvek	k1gInSc7	příspěvek
do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
antologie	antologie	k1gFnSc2	antologie
byla	být	k5eAaImAgFnS	být
venkovská	venkovský	k2eAgFnSc1d1	venkovská
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Epstein	Epstein	k1gMnSc1	Epstein
zde	zde	k6eAd1	zde
použil	použít	k5eAaPmAgMnS	použít
zrychlené	zrychlený	k2eAgFnSc2d1	zrychlená
montáže	montáž	k1gFnSc2	montáž
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
estetika	estetika	k1gFnSc1	estetika
<g/>
,	,	kIx,	,
na	na	k7c4	na
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
ustupovat	ustupovat	k5eAaImF	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Přešlo	přejít	k5eAaPmAgNnS	přejít
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
od	od	k7c2	od
subjektivismu	subjektivismus	k1gInSc2	subjektivismus
a	a	k8xC	a
expresionismu	expresionismus	k1gInSc2	expresionismus
k	k	k7c3	k
abstrakci	abstrakce	k1gFnSc3	abstrakce
<g/>
,	,	kIx,	,
dadaismu	dadaismus	k1gInSc3	dadaismus
nebo	nebo	k8xC	nebo
surrealismu	surrealismus	k1gInSc3	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
dokumentárním	dokumentární	k2eAgInPc3d1	dokumentární
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
výrazným	výrazný	k2eAgMnSc7d1	výrazný
režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgMnS	být
René	René	k1gMnSc1	René
Clair	Clair	k1gMnSc1	Clair
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
debutoval	debutovat	k5eAaBmAgMnS	debutovat
svým	svůj	k3xOyFgInSc7	svůj
filmem	film	k1gInSc7	film
Paříž	Paříž	k1gFnSc1	Paříž
spí	spát	k5eAaImIp3nS	spát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
exteriérech	exteriér	k1gInPc6	exteriér
s	s	k7c7	s
omezenými	omezený	k2eAgInPc7d1	omezený
prostředky	prostředek	k1gInPc7	prostředek
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
hodnotu	hodnota	k1gFnSc4	hodnota
snímku	snímek	k1gInSc2	snímek
a	a	k8xC	a
pořídit	pořídit	k5eAaPmF	pořídit
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
záběry	záběr	k1gInPc4	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Mezihra	mezihra	k1gFnSc1	mezihra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
snímku	snímek	k1gInSc2	snímek
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
avantgardy	avantgarda	k1gFnSc2	avantgarda
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
se	se	k3xPyFc4	se
rozvinu	rozvinout	k5eAaPmIp1nS	rozvinout
předválečný	předválečný	k2eAgInSc1d1	předválečný
styl	styl	k1gInSc1	styl
<g/>
.	.	kIx.	.
</s>
<s>
Využíval	využívat	k5eAaPmAgMnS	využívat
formální	formální	k2eAgFnPc4d1	formální
vyumělkovanosti	vyumělkovanost	k1gFnPc4	vyumělkovanost
-	-	kIx~	-
dvojexpozice	dvojexpozice	k1gFnPc4	dvojexpozice
<g/>
,	,	kIx,	,
deformace	deformace	k1gFnPc4	deformace
obrazů	obraz	k1gInPc2	obraz
nebo	nebo	k8xC	nebo
rychlé	rychlý	k2eAgFnSc2d1	rychlá
montáže	montáž	k1gFnSc2	montáž
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
při	při	k7c6	při
sestavování	sestavování	k1gNnSc6	sestavování
fantasticky	fantasticky	k6eAd1	fantasticky
nereálných	reálný	k2eNgInPc2d1	nereálný
obrazů	obraz	k1gInPc2	obraz
této	tento	k3xDgFnSc2	tento
bláznivé	bláznivý	k2eAgFnSc2d1	bláznivá
koláže	koláž	k1gFnSc2	koláž
nechal	nechat	k5eAaPmAgMnS	nechat
vést	vést	k5eAaImF	vést
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
pro	pro	k7c4	pro
rytmus	rytmus	k1gInSc4	rytmus
filmu	film	k1gInSc2	film
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
hodnotou	hodnota	k1gFnSc7	hodnota
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
avantgardisté	avantgardista	k1gMnPc1	avantgardista
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
snažili	snažit	k5eAaImAgMnP	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
"	"	kIx"	"
<g/>
cinéma	cinéma	k1gFnSc1	cinéma
pur	pur	k?	pur
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
ryzí	ryzí	k2eAgInSc1d1	ryzí
film	film	k1gInSc1	film
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
čistý	čistý	k2eAgInSc1d1	čistý
film	film	k1gInSc1	film
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
znemožnit	znemožnit	k5eAaPmF	znemožnit
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
logické	logický	k2eAgNnSc4d1	logické
uchopení	uchopení	k1gNnSc4	uchopení
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prvořadý	prvořadý	k2eAgInSc1d1	prvořadý
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
rytmus	rytmus	k1gInSc1	rytmus
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
vytvoření	vytvoření	k1gNnSc2	vytvoření
proudu	proud	k1gInSc2	proud
volných	volný	k2eAgFnPc2d1	volná
asociací	asociace	k1gFnPc2	asociace
Svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
a	a	k8xC	a
největší	veliký	k2eAgFnSc2d3	veliký
veřejné	veřejný	k2eAgFnSc2d1	veřejná
pozornosti	pozornost	k1gFnSc2	pozornost
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
avantgardnímu	avantgardní	k2eAgInSc3d1	avantgardní
filmu	film	k1gInSc2	film
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
s	s	k7c7	s
díly	díl	k1gInPc7	díl
Luise	Luisa	k1gFnSc3	Luisa
Bunuela	Bunuela	k1gFnSc1	Bunuela
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
malířem	malíř	k1gMnSc7	malíř
Salvadorem	Salvador	k1gMnSc7	Salvador
Dalím	Dalí	k1gMnSc7	Dalí
inscenovali	inscenovat	k5eAaBmAgMnP	inscenovat
šokující	šokující	k2eAgInPc4d1	šokující
obrazy	obraz	k1gInPc4	obraz
a	a	k8xC	a
montovali	montovat	k5eAaImAgMnP	montovat
je	on	k3xPp3gInPc4	on
<g/>
,	,	kIx,	,
ovlivněni	ovlivnit	k5eAaPmNgMnP	ovlivnit
nastupující	nastupující	k2eAgFnSc7d1	nastupující
psychoanalýzou	psychoanalýza	k1gFnSc7	psychoanalýza
<g/>
,	,	kIx,	,
do	do	k7c2	do
nelogických	logický	k2eNgInPc2d1	nelogický
<g/>
,	,	kIx,	,
snově	snově	k6eAd1	snově
asociačních	asociační	k2eAgInPc2d1	asociační
sledů	sled	k1gInPc2	sled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Andaluský	andaluský	k2eAgMnSc1d1	andaluský
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vynořují	vynořovat	k5eAaImIp3nP	vynořovat
obrazy	obraz	k1gInPc1	obraz
milostného	milostný	k2eAgInSc2d1	milostný
příběhu	příběh	k1gInSc2	příběh
jako	jako	k8xS	jako
z	z	k7c2	z
podvědomí	podvědomí	k1gNnSc2	podvědomí
protagonistů	protagonista	k1gMnPc2	protagonista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
detailu	detail	k1gInSc6	detail
rozřeže	rozřezat	k5eAaPmIp3nS	rozřezat
muž	muž	k1gMnSc1	muž
břitvou	břitva	k1gFnSc7	břitva
na	na	k7c4	na
holení	holení	k1gNnSc4	holení
oko	oko	k1gNnSc4	oko
mladé	mladý	k2eAgFnSc2d1	mladá
dívky	dívka	k1gFnSc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
sled	sled	k1gInSc1	sled
obrazů	obraz	k1gInPc2	obraz
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
melouny	meloun	k1gInPc4	meloun
<g/>
,	,	kIx,	,
klavíry	klavír	k1gInPc4	klavír
a	a	k8xC	a
oslí	oslí	k2eAgFnSc4d1	oslí
mršinu	mršina	k1gFnSc4	mršina
přivázané	přivázaný	k2eAgFnSc2d1	přivázaná
k	k	k7c3	k
mladému	mladý	k1gMnSc3	mladý
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
brání	bránit	k5eAaImIp3nS	bránit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
k	k	k7c3	k
dívce	dívka	k1gFnSc3	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
se	s	k7c7	s
surrealistickým	surrealistický	k2eAgNnSc7d1	surrealistické
heslem	heslo	k1gNnSc7	heslo
stal	stát	k5eAaPmAgInS	stát
Lautréamontův	Lautréamontův	k2eAgInSc1d1	Lautréamontův
citát	citát	k1gInSc1	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Krásný	krásný	k2eAgMnSc1d1	krásný
jako	jako	k8xC	jako
setkání	setkání	k1gNnSc1	setkání
deštníku	deštník	k1gInSc2	deštník
a	a	k8xC	a
šicího	šicí	k2eAgInSc2d1	šicí
stroje	stroj	k1gInSc2	stroj
na	na	k7c6	na
pitevním	pitevní	k2eAgInSc6d1	pitevní
stole	stol	k1gInSc6	stol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
První	první	k4xOgFnSc6	první
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
filmového	filmový	k2eAgInSc2d1	filmový
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
soustřeďovala	soustřeďovat	k5eAaImAgFnS	soustřeďovat
všechen	všechen	k3xTgInSc4	všechen
filmový	filmový	k2eAgInSc4d1	filmový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kinech	kino	k1gNnPc6	kino
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
promítat	promítat	k5eAaImF	promítat
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pocházely	pocházet	k5eAaImAgFnP	pocházet
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Československu	Československo	k1gNnSc6	Československo
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
válkou	válka	k1gFnSc7	válka
uzavřeny	uzavřen	k2eAgFnPc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byla	být	k5eAaImAgFnS	být
takřka	takřka	k6eAd1	takřka
neomezená	omezený	k2eNgFnSc1d1	neomezená
nabídka	nabídka	k1gFnSc1	nabídka
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
filmového	filmový	k2eAgInSc2d1	filmový
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
vznik	vznik	k1gInSc4	vznik
nových	nový	k2eAgInPc2d1	nový
kinematografických	kinematografický	k2eAgInPc2d1	kinematografický
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
republika	republika	k1gFnSc1	republika
trpěla	trpět	k5eAaImAgFnS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
dobře	dobře	k6eAd1	dobře
vybavených	vybavený	k2eAgInPc2d1	vybavený
ateliéru	ateliér	k1gInSc6	ateliér
nutil	nutit	k5eAaImAgInS	nutit
výrobce	výrobce	k1gMnSc4	výrobce
jezdit	jezdit	k5eAaImF	jezdit
do	do	k7c2	do
berlínských	berlínský	k2eAgInPc2d1	berlínský
nebo	nebo	k8xC	nebo
vídeňských	vídeňský	k2eAgInPc2d1	vídeňský
ateliérů	ateliér	k1gInPc2	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
ještě	ještě	k6eAd1	ještě
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
a	a	k8xC	a
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
domácích	domácí	k2eAgInPc6d1	domácí
ateliérech	ateliér	k1gInPc6	ateliér
rostla	růst	k5eAaImAgNnP	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
založil	založit	k5eAaPmAgMnS	založit
Miloš	Miloš	k1gMnSc1	Miloš
Havel	Havel	k1gMnSc1	Havel
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
A-B	A-B	k1gFnPc2	A-B
spojením	spojení	k1gNnSc7	spojení
distribučních	distribuční	k2eAgFnPc2d1	distribuční
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
počátek	počátek	k1gInSc1	počátek
systematičtější	systematický	k2eAgFnSc2d2	systematičtější
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pronikavé	pronikavý	k2eAgFnSc3d1	pronikavá
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
zřízená	zřízený	k2eAgFnSc1d1	zřízená
československá	československý	k2eAgFnSc1d1	Československá
cenzura	cenzura	k1gFnSc1	cenzura
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
platilo	platit	k5eAaImAgNnS	platit
nařízení	nařízení	k1gNnSc1	nařízení
o	o	k7c6	o
filmové	filmový	k2eAgFnSc6d1	filmová
cenzuře	cenzura	k1gFnSc6	cenzura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
platilo	platit	k5eAaImAgNnS	platit
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
bývalé	bývalý	k2eAgNnSc4d1	bývalé
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
podklady	podklad	k1gInPc1	podklad
pro	pro	k7c4	pro
filmové	filmový	k2eAgFnPc4d1	filmová
statistiky	statistika	k1gFnPc4	statistika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
početní	početní	k2eAgNnSc4d1	početní
zachycení	zachycení	k1gNnSc4	zachycení
filmové	filmový	k2eAgFnSc2d1	filmová
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
uveřejňování	uveřejňování	k1gNnSc1	uveřejňování
úředních	úřední	k2eAgInPc2d1	úřední
cenzurních	cenzurní	k2eAgInPc2d1	cenzurní
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Českoslovenští	československý	k2eAgMnPc1d1	československý
producenti	producent	k1gMnPc1	producent
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
natočili	natočit	k5eAaBmAgMnP	natočit
celkem	celkem	k6eAd1	celkem
183	[number]	k4	183
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
bylo	být	k5eAaImAgNnS	být
postaveno	postaven	k2eAgNnSc4d1	postaveno
první	první	k4xOgNnSc4	první
muzeum	muzeum	k1gNnSc4	muzeum
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Brychtou	Brychta	k1gMnSc7	Brychta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
začala	začít	k5eAaPmAgFnS	začít
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
přicházet	přicházet	k5eAaImF	přicházet
ostrá	ostrý	k2eAgFnSc1d1	ostrá
konkurence	konkurence	k1gFnSc1	konkurence
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
technicky	technicky	k6eAd1	technicky
vyspělejší	vyspělý	k2eAgInPc4d2	vyspělejší
americké	americký	k2eAgInPc4d1	americký
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
československý	československý	k2eAgInSc4d1	československý
film	film	k1gInSc4	film
to	ten	k3xDgNnSc1	ten
měl	mít	k5eAaImAgInS	mít
drtivý	drtivý	k2eAgInSc1d1	drtivý
dopad	dopad	k1gInSc1	dopad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
brzo	brzo	k6eAd1	brzo
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
hlad	hlad	k1gInSc1	hlad
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
po	po	k7c6	po
cizích	cizí	k2eAgInPc6d1	cizí
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c6	v
přesycení	přesycení	k1gNnSc6	přesycení
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
po	po	k7c6	po
dvouleté	dvouletý	k2eAgFnSc6d1	dvouletá
pauze	pauza	k1gFnSc6	pauza
získal	získat	k5eAaPmAgMnS	získat
film	film	k1gInSc4	film
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
opět	opět	k6eAd1	opět
ztracené	ztracený	k2eAgFnPc4d1	ztracená
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
udržel	udržet	k5eAaPmAgMnS	udržet
až	až	k6eAd1	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
herci	herec	k1gMnPc1	herec
Charles	Charles	k1gMnSc1	Charles
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
<g/>
,	,	kIx,	,
Douglas	Douglas	k1gMnSc1	Douglas
Fairbanks	Fairbanks	k1gInSc1	Fairbanks
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
Pickfordová	Pickfordová	k1gFnSc1	Pickfordová
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
David	David	k1gMnSc1	David
Wark	Wark	k1gMnSc1	Wark
Griffith	Griffith	k1gMnSc1	Griffith
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgFnPc1	čtyři
známé	známá	k1gFnPc1	známá
osobnosti	osobnost	k1gFnSc2	osobnost
amerického	americký	k2eAgInSc2d1	americký
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
zakládají	zakládat	k5eAaImIp3nP	zakládat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
produkční	produkční	k2eAgFnSc4d1	produkční
a	a	k8xC	a
distribuční	distribuční	k2eAgFnSc4d1	distribuční
společnost	společnost	k1gFnSc4	společnost
United	United	k1gMnSc1	United
Artists	Artistsa	k1gFnPc2	Artistsa
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
společnosti	společnost	k1gFnSc2	společnost
bylo	být	k5eAaImAgNnS	být
finančně	finančně	k6eAd1	finančně
podporovat	podporovat	k5eAaImF	podporovat
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
filmy	film	k1gInPc4	film
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
filmových	filmový	k2eAgMnPc2d1	filmový
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
postarat	postarat	k5eAaPmF	postarat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
distribuci	distribuce	k1gFnSc4	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zakladatelé	zakladatel	k1gMnPc1	zakladatel
zajistili	zajistit	k5eAaPmAgMnP	zajistit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
vlastním	vlastní	k2eAgInSc7d1	vlastní
ziskem	zisk	k1gInSc7	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
společnosti	společnost	k1gFnSc2	společnost
bylo	být	k5eAaImAgNnS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c6	na
bezprostředně	bezprostředně	k6eAd1	bezprostředně
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
dohodu	dohoda	k1gFnSc4	dohoda
dvou	dva	k4xCgInPc2	dva
největších	veliký	k2eAgInPc2d3	veliký
filmových	filmový	k2eAgInPc2d1	filmový
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
First	First	k1gFnSc1	First
National	National	k1gFnSc1	National
a	a	k8xC	a
Famous	Famous	k1gInSc1	Famous
Players	Playersa	k1gFnPc2	Playersa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
drastickému	drastický	k2eAgNnSc3d1	drastické
omezení	omezení	k1gNnSc3	omezení
neúměrně	úměrně	k6eNd1	úměrně
vysokých	vysoký	k2eAgInPc2d1	vysoký
platů	plat	k1gInPc2	plat
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
velké	velký	k2eAgFnPc1d1	velká
firmy	firma	k1gFnPc1	firma
se	se	k3xPyFc4	se
chtěly	chtít	k5eAaImAgFnP	chtít
touto	tento	k3xDgFnSc7	tento
dohodou	dohoda	k1gFnSc7	dohoda
postavit	postavit	k5eAaPmF	postavit
proti	proti	k7c3	proti
běžné	běžný	k2eAgFnSc3d1	běžná
praxi	praxe	k1gFnSc3	praxe
svých	svůj	k3xOyFgFnPc2	svůj
filmových	filmový	k2eAgFnPc2d1	filmová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
rozeštvávat	rozeštvávat	k5eAaImF	rozeštvávat
společnosti	společnost	k1gFnPc1	společnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
střídat	střídat	k5eAaImF	střídat
různá	různý	k2eAgNnPc4d1	různé
studia	studio	k1gNnPc4	studio
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
stále	stále	k6eAd1	stále
své	svůj	k3xOyFgInPc4	svůj
platy	plat	k1gInPc4	plat
šroubovat	šroubovat	k5eAaImF	šroubovat
co	co	k9	co
nejvýš	nejvýš	k6eAd1	nejvýš
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
slavným	slavný	k2eAgNnPc3d1	slavné
jménům	jméno	k1gNnPc3	jméno
sdruženým	sdružený	k2eAgMnSc7d1	sdružený
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
se	se	k3xPyFc4	se
United	United	k1gInSc1	United
Artists	Artistsa	k1gFnPc2	Artistsa
skutečně	skutečně	k6eAd1	skutečně
podařilo	podařit	k5eAaPmAgNnS	podařit
prolomit	prolomit	k5eAaPmF	prolomit
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
hráz	hráz	k1gFnSc4	hráz
mezi	mezi	k7c7	mezi
výrobnou	výrobna	k1gFnSc7	výrobna
<g/>
,	,	kIx,	,
půjčovnou	půjčovna	k1gFnSc7	půjčovna
filmů	film	k1gInPc2	film
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
filmy	film	k1gInPc1	film
promítaly	promítat	k5eAaImAgInP	promítat
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
neměla	mít	k5eNaImAgFnS	mít
žádné	žádný	k3yNgInPc4	žádný
vlastní	vlastní	k2eAgInPc4d1	vlastní
ateliéry	ateliér	k1gInPc4	ateliér
a	a	k8xC	a
tak	tak	k6eAd1	tak
využívala	využívat	k5eAaImAgFnS	využívat
pouze	pouze	k6eAd1	pouze
filmových	filmový	k2eAgFnPc2d1	filmová
studií	studie	k1gFnPc2	studie
svých	svůj	k3xOyFgMnPc2	svůj
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gInSc4	United
Artists	Artistsa	k1gFnPc2	Artistsa
nevedla	vést	k5eNaImAgFnS	vést
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
režiséry	režisér	k1gMnPc7	režisér
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
žádný	žádný	k3yNgInSc4	žádný
složitý	složitý	k2eAgInSc4d1	složitý
smluvní	smluvní	k2eAgInSc4d1	smluvní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
neusilovala	usilovat	k5eNaImAgFnS	usilovat
o	o	k7c4	o
zřetězení	zřetězení	k1gNnSc4	zřetězení
kin	kino	k1gNnPc2	kino
-	-	kIx~	-
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
dal	dát	k5eAaPmAgMnS	dát
očekávat	očekávat	k5eAaImF	očekávat
u	u	k7c2	u
filmů	film	k1gInPc2	film
společnosti	společnost	k1gFnSc2	společnost
finanční	finanční	k2eAgInSc4d1	finanční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
majitelé	majitel	k1gMnPc1	majitel
kin	kino	k1gNnPc2	kino
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
promítali	promítat	k5eAaImAgMnP	promítat
i	i	k9	i
bez	bez	k7c2	bez
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k1gInSc1	Chaplin
své	svůj	k3xOyFgInPc4	svůj
velké	velký	k2eAgInPc4d1	velký
úspěchy	úspěch	k1gInPc4	úspěch
slaví	slavit	k5eAaImIp3nS	slavit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
založení	založení	k1gNnSc2	založení
United	United	k1gInSc1	United
Artists	Artists	k1gInSc1	Artists
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgMnPc2d1	placený
umělců	umělec	k1gMnPc2	umělec
filmové	filmový	k2eAgFnSc2d1	filmová
branže	branže	k1gFnSc2	branže
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1	filmová
společnosti	společnost	k1gFnPc1	společnost
mu	on	k3xPp3gNnSc3	on
nechávaly	nechávat	k5eAaImAgFnP	nechávat
volnou	volný	k2eAgFnSc7d1	volná
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
šmajdavého	šmajdavý	k2eAgMnSc4d1	šmajdavý
tuláka	tulák	k1gMnSc4	tulák
<g/>
,	,	kIx,	,
oblečeného	oblečený	k2eAgMnSc2d1	oblečený
do	do	k7c2	do
ošuntěného	ošuntěný	k2eAgInSc2d1	ošuntěný
obleku	oblek	k1gInSc2	oblek
<g/>
,	,	kIx,	,
pamatující	pamatující	k2eAgInPc4d1	pamatující
lepší	dobrý	k2eAgInPc4d2	lepší
časy	čas	k1gInPc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
raných	raný	k2eAgInPc6d1	raný
filmech	film	k1gInPc6	film
pokaždé	pokaždé	k6eAd1	pokaždé
hrál	hrát	k5eAaImAgInS	hrát
sociálně	sociálně	k6eAd1	sociálně
deklasované	deklasovaný	k2eAgInPc4d1	deklasovaný
charaktery	charakter	k1gInPc4	charakter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
lstí	lest	k1gFnSc7	lest
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
proti	proti	k7c3	proti
krásným	krásný	k2eAgMnPc3d1	krásný
a	a	k8xC	a
úspěšným	úspěšný	k2eAgMnPc3d1	úspěšný
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Intelektuálové	intelektuál	k1gMnPc1	intelektuál
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Psí	psí	k2eAgInSc1d1	psí
život	život	k1gInSc1	život
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
jasnou	jasný	k2eAgFnSc4d1	jasná
obžalobu	obžaloba	k1gFnSc4	obžaloba
kapitalistického	kapitalistický	k2eAgInSc2d1	kapitalistický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Skvělý	skvělý	k2eAgMnSc1d1	skvělý
filmový	filmový	k2eAgMnSc1d1	filmový
komik	komik	k1gMnSc1	komik
Harold	Harold	k1gMnSc1	Harold
Lloyd	Lloyd	k1gMnSc1	Lloyd
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgMnSc1d1	proslulý
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgInPc7	svůj
akrobatickými	akrobatický	k2eAgInPc7d1	akrobatický
kousky	kousek	k1gInPc7	kousek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
zejména	zejména	k9	zejména
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
O	o	k7c4	o
patro	patro	k1gNnSc4	patro
výš	vysoce	k6eAd2	vysoce
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
režiséru	režisér	k1gMnSc3	režisér
Freda	Fred	k1gMnSc4	Fred
C.	C.	kA	C.
Newmeyera	Newmeyer	k1gMnSc4	Newmeyer
a	a	k8xC	a
Sama	Sam	k1gMnSc4	Sam
Taylora	Taylor	k1gMnSc4	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
O	o	k7c4	o
patro	patro	k1gNnSc4	patro
výš	vysoce	k6eAd2	vysoce
byla	být	k5eAaImAgFnS	být
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
filmem	film	k1gInSc7	film
Lloyda	Lloyda	k1gFnSc1	Lloyda
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zdolat	zdolat	k5eAaPmF	zdolat
výškovou	výškový	k2eAgFnSc4d1	výšková
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
vystaven	vystavit	k5eAaPmNgMnS	vystavit
nástrahám	nástraha	k1gFnPc3	nástraha
a	a	k8xC	a
protivenstvím	protivenství	k1gNnPc3	protivenství
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
tají	tajit	k5eAaImIp3nP	tajit
dech	dech	k1gInSc1	dech
a	a	k8xC	a
vstávají	vstávat	k5eAaImIp3nP	vstávat
vlasy	vlas	k1gInPc1	vlas
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
dva	dva	k4xCgInPc1	dva
následující	následující	k2eAgInPc1d1	následující
filmy	film	k1gInPc1	film
jsou	být	k5eAaImIp3nP	být
tematicky	tematicky	k6eAd1	tematicky
i	i	k9	i
žánrově	žánrově	k6eAd1	žánrově
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vhodně	vhodně	k6eAd1	vhodně
voleným	volený	k2eAgInPc3d1	volený
úhlům	úhel	k1gInPc3	úhel
záběrů	záběr	k1gInPc2	záběr
působí	působit	k5eAaImIp3nP	působit
mnohé	mnohý	k2eAgFnPc1d1	mnohá
scény	scéna	k1gFnPc1	scéna
ve	v	k7c6	v
filmu	film	k1gInSc6	film
hrozivěji	hrozivě	k6eAd2	hrozivě
než	než	k8xS	než
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
záběrů	záběr	k1gInPc2	záběr
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
Harold	Harold	k1gMnSc1	Harold
Lloyd	Lloyd	k1gMnSc1	Lloyd
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Dvojníkem	dvojník	k1gMnSc7	dvojník
se	se	k3xPyFc4	se
nechával	nechávat	k5eAaImAgMnS	nechávat
nahradit	nahradit	k5eAaPmF	nahradit
jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
velkých	velký	k2eAgInPc6d1	velký
celcích	celek	k1gInPc6	celek
<g/>
.	.	kIx.	.
</s>
<s>
Šplhání	šplhání	k1gNnSc1	šplhání
po	po	k7c6	po
mrakodrapech	mrakodrap	k1gInPc6	mrakodrap
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
kritiky	kritika	k1gFnSc2	kritika
interpretováno	interpretován	k2eAgNnSc1d1	interpretováno
jako	jako	k9	jako
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
bijící	bijící	k2eAgInSc1d1	bijící
symbol	symbol	k1gInSc1	symbol
společenského	společenský	k2eAgInSc2d1	společenský
vzestupu	vzestup	k1gInSc2	vzestup
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgMnSc6	jenž
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
vskrytu	vskrytu	k6eAd1	vskrytu
touží	toužit	k5eAaImIp3nS	toužit
<g/>
.	.	kIx.	.
</s>
<s>
Lloyd	Lloyd	k1gMnSc1	Lloyd
často	často	k6eAd1	často
ztělesnil	ztělesnit	k5eAaPmAgMnS	ztělesnit
optimistického	optimistický	k2eAgMnSc4d1	optimistický
kariéristického	kariéristický	k2eAgMnSc4d1	kariéristický
maloměšťáka	maloměšťák	k1gMnSc4	maloměšťák
<g/>
,	,	kIx,	,
obdařeného	obdařený	k2eAgMnSc2d1	obdařený
komicky	komicky	k6eAd1	komicky
směšnou	směšný	k2eAgFnSc7d1	směšná
snahou	snaha	k1gFnSc7	snaha
po	po	k7c4	po
přizpůsobení	přizpůsobení	k1gNnSc4	přizpůsobení
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
nalézt	nalézt	k5eAaBmF	nalézt
navzdory	navzdory	k7c3	navzdory
protivenstvím	protivenství	k1gNnPc3	protivenství
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc3d1	vlastní
nešikovnosti	nešikovnost	k1gFnSc3	nešikovnost
uplatnění	uplatnění	k1gNnSc1	uplatnění
ve	v	k7c6	v
velkoměstské	velkoměstský	k2eAgFnSc6d1	velkoměstská
džungli	džungle	k1gFnSc6	džungle
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
útrapách	útrapa	k1gFnPc6	útrapa
nakonec	nakonec	k6eAd1	nakonec
hrdina	hrdina	k1gMnSc1	hrdina
přece	přece	k9	přece
jen	jen	k6eAd1	jen
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
kýženého	kýžený	k2eAgInSc2d1	kýžený
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Lloyd	Lloyd	k1gMnSc1	Lloyd
mnohdy	mnohdy	k6eAd1	mnohdy
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
prospěchářských	prospěchářský	k2eAgFnPc2d1	prospěchářská
hrdinou	hrdina	k1gMnSc7	hrdina
u	u	k7c2	u
amerického	americký	k2eAgNnSc2d1	americké
publika	publikum	k1gNnSc2	publikum
větší	veliký	k2eAgFnSc2d2	veliký
obliby	obliba	k1gFnSc2	obliba
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaké	jaký	k3yIgFnPc1	jaký
se	se	k3xPyFc4	se
těšily	těšit	k5eAaImAgFnP	těšit
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
hvězdy	hvězda	k1gFnPc1	hvězda
němých	němý	k2eAgFnPc2d1	němá
grotesek	groteska	k1gFnPc2	groteska
-	-	kIx~	-
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
a	a	k8xC	a
Buster	Buster	k1gMnSc1	Buster
Keaton	Keaton	k1gInSc1	Keaton
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gInSc1	Joseph
"	"	kIx"	"
<g/>
Buster	Buster	k1gInSc1	Buster
<g/>
"	"	kIx"	"
Keaton	Keaton	k1gInSc1	Keaton
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
přezdívka	přezdívka	k1gFnSc1	přezdívka
"	"	kIx"	"
<g/>
Stoneface	Stoneface	k1gFnSc1	Stoneface
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kamenná	kamenný	k2eAgFnSc1d1	kamenná
tvář	tvář	k1gFnSc1	tvář
<g/>
)	)	kIx)	)
přesně	přesně	k6eAd1	přesně
vystihovala	vystihovat	k5eAaImAgFnS	vystihovat
stoický	stoický	k2eAgInSc4d1	stoický
výraz	výraz	k1gInSc4	výraz
jeho	jeho	k3xOp3gFnSc2	jeho
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Charliem	Charlie	k1gMnSc7	Charlie
Chaplinem	Chaplin	k1gInSc7	Chaplin
a	a	k8xC	a
Haroldem	Harold	k1gMnSc7	Harold
Lloydem	Lloyd	k1gMnSc7	Lloyd
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dokázali	dokázat	k5eAaPmAgMnP	dokázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
osobitý	osobitý	k2eAgInSc4d1	osobitý
typ	typ	k1gInSc4	typ
filmové	filmový	k2eAgFnSc2d1	filmová
komiky	komika	k1gFnSc2	komika
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečností	jedinečnost	k1gFnSc7	jedinečnost
komického	komický	k2eAgMnSc2d1	komický
hrdiny	hrdina	k1gMnSc2	hrdina
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
Busterem	Buster	k1gInSc7	Buster
Keatonem	Keaton	k1gInSc7	Keaton
je	být	k5eAaImIp3nS	být
vážnost	vážnost	k1gFnSc1	vážnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
<g/>
,	,	kIx,	,
i	i	k9	i
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
nejabsurdnějších	absurdní	k2eAgFnPc6d3	nejabsurdnější
a	a	k8xC	a
nejneočekávanějších	očekávaný	k2eNgFnPc6d3	nejneočekávanější
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
prvenství	prvenství	k1gNnSc2	prvenství
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
bojem	boj	k1gInSc7	boj
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
natolik	natolik	k6eAd1	natolik
silní	silný	k2eAgMnPc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
prosadit	prosadit	k5eAaPmF	prosadit
proti	proti	k7c3	proti
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Patents	Patents	k1gInSc4	Patents
Company	Compan	k1gInPc7	Compan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
sdružovali	sdružovat	k5eAaImAgMnP	sdružovat
do	do	k7c2	do
stále	stále	k6eAd1	stále
větších	veliký	k2eAgFnPc2d2	veliký
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
začínala	začínat	k5eAaImAgFnS	začínat
vznikat	vznikat	k5eAaImF	vznikat
velká	velká	k1gFnSc1	velká
studia	studio	k1gNnSc2	studio
<g/>
:	:	kIx,	:
Paramount	Paramount	k1gMnSc1	Paramount
Pictures	Pictures	k1gMnSc1	Pictures
<g/>
,	,	kIx,	,
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Metro-Goldwyn-Mayer	Metro-Goldwyn-Mayer	k1gInSc1	Metro-Goldwyn-Mayer
<g/>
,	,	kIx,	,
Fox	fox	k1gInSc1	fox
Film	film	k1gInSc1	film
Corporation	Corporation	k1gInSc4	Corporation
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
<g/>
)	)	kIx)	)
a	a	k8xC	a
Independent	independent	k1gMnSc1	independent
Moving	Moving	k1gInSc4	Moving
Pictures	Pictures	k1gInSc4	Pictures
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc2	předchůdce
Universal	Universal	k1gMnSc2	Universal
Studios	Studios	k?	Studios
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
podniky	podnik	k1gInPc1	podnik
si	se	k3xPyFc3	se
pečlivě	pečlivě	k6eAd1	pečlivě
hlídaly	hlídat	k5eAaImAgInP	hlídat
úspěchy	úspěch	k1gInPc1	úspěch
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
-	-	kIx~	-
jako	jako	k8xC	jako
hvězdy	hvězda	k1gFnPc1	hvězda
vystupovaly	vystupovat	k5eAaImAgFnP	vystupovat
jen	jen	k9	jen
určité	určitý	k2eAgInPc4d1	určitý
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
znovu	znovu	k6eAd1	znovu
obměňovaly	obměňovat	k5eAaImAgInP	obměňovat
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
témata	téma	k1gNnPc4	téma
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
omezovaly	omezovat	k5eAaImAgFnP	omezovat
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohl	moct	k5eAaImAgMnS	moct
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Filmem	film	k1gInSc7	film
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
filmem	film	k1gInSc7	film
firmou	firma	k1gFnSc7	firma
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
fakticky	fakticky	k6eAd1	fakticky
zahájena	zahájen	k2eAgFnSc1d1	zahájena
éra	éra	k1gFnSc1	éra
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
od	od	k7c2	od
základu	základ	k1gInSc2	základ
změnila	změnit	k5eAaPmAgFnS	změnit
veškerá	veškerý	k3xTgFnSc1	veškerý
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
přístrojové	přístrojový	k2eAgNnSc4d1	přístrojové
vybavení	vybavení	k1gNnSc4	vybavení
i	i	k8xC	i
tvář	tvář	k1gFnSc4	tvář
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
premiéra	premiéra	k1gFnSc1	premiéra
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
pouze	pouze	k6eAd1	pouze
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
hraná	hraný	k2eAgFnSc1d1	hraná
ze	z	k7c2	z
speciálního	speciální	k2eAgInSc2d1	speciální
gramofonu	gramofon	k1gInSc2	gramofon
systému	systém	k1gInSc2	systém
Vitaphone	Vitaphon	k1gInSc5	Vitaphon
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
zpěvák	zpěvák	k1gMnSc1	zpěvák
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc1	slovo
-	-	kIx~	-
monolog	monolog	k1gInSc1	monolog
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
postavy	postava	k1gFnSc2	postava
broadwayských	broadwayský	k2eAgInPc2d1	broadwayský
muzikálů	muzikál	k1gInPc2	muzikál
Ala	ala	k1gFnSc1	ala
Jolsona	Jolsona	k1gFnSc1	Jolsona
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
na	na	k7c4	na
250	[number]	k4	250
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Počkej	počkat	k5eAaPmRp2nS	počkat
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
!	!	kIx.	!
</s>
<s>
Počkej	počkat	k5eAaPmRp2nS	počkat
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
!	!	kIx.	!
</s>
<s>
Ještě	ještě	k6eAd1	ještě
jsi	být	k5eAaImIp2nS	být
nic	nic	k3yNnSc1	nic
neslyšel	slyšet	k5eNaImAgMnS	slyšet
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgNnPc1	první
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zazněla	zaznít	k5eAaPmAgFnS	zaznít
z	z	k7c2	z
filmového	filmový	k2eAgNnSc2d1	filmové
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
finanční	finanční	k2eAgFnSc6d1	finanční
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
bratři	bratr	k1gMnPc1	bratr
tedy	tedy	k9	tedy
obrátili	obrátit	k5eAaPmAgMnP	obrátit
svoji	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
vynález	vynález	k1gInSc4	vynález
firmy	firma	k1gFnSc2	firma
Western	Western	kA	Western
Electric	Electric	k1gMnSc1	Electric
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zachránit	zachránit	k5eAaPmF	zachránit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
svoji	svůj	k3xOyFgFnSc4	svůj
zruinovanou	zruinovaný	k2eAgFnSc4d1	zruinovaná
filmovou	filmový	k2eAgFnSc4d1	filmová
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vší	všecek	k3xTgFnSc6	všecek
tajnosti	tajnost	k1gFnSc6	tajnost
byl	být	k5eAaImAgInS	být
ozvučen	ozvučen	k2eAgInSc1d1	ozvučen
film	film	k1gInSc1	film
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
koncipovaný	koncipovaný	k2eAgMnSc1d1	koncipovaný
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
němý	němý	k2eAgInSc1d1	němý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
senzací	senzace	k1gFnSc7	senzace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přinesl	přinést	k5eAaPmAgMnS	přinést
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
mluvené	mluvený	k2eAgNnSc1d1	mluvené
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
úspěch	úspěch	k1gInSc1	úspěch
Dona	dona	k1gFnSc1	dona
Juana	Juan	k1gMnSc2	Juan
jednoznačně	jednoznačně	k6eAd1	jednoznačně
předčil	předčít	k5eAaPmAgMnS	předčít
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
nadšení	nadšení	k1gNnSc3	nadšení
trvalo	trvat	k5eAaImAgNnS	trvat
celých	celý	k2eAgNnPc2d1	celé
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
zvukový	zvukový	k2eAgInSc1d1	zvukový
film	film	k1gInSc1	film
definitivně	definitivně	k6eAd1	definitivně
prosadil	prosadit	k5eAaPmAgInS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
musela	muset	k5eAaImAgFnS	muset
nakoupit	nakoupit	k5eAaPmF	nakoupit
zvuková	zvukový	k2eAgFnSc1d1	zvuková
zařízená	zařízený	k2eAgFnSc1d1	zařízená
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
od	od	k7c2	od
čtvrt	čtvrt	k1xP	čtvrt
do	do	k7c2	do
půl	půl	k1xP	půl
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
kus	kus	k1gInSc4	kus
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
instalaci	instalace	k1gFnSc4	instalace
zvukové	zvukový	k2eAgFnSc2d1	zvuková
aparatury	aparatura	k1gFnSc2	aparatura
činily	činit	k5eAaImAgInP	činit
15	[number]	k4	15
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zařízení	zařízení	k1gNnSc1	zařízení
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
majetkem	majetek	k1gInSc7	majetek
firmy	firma	k1gFnSc2	firma
Western	Western	kA	Western
Electric	Electric	k1gMnSc1	Electric
a	a	k8xC	a
vlastníci	vlastník	k1gMnPc1	vlastník
kin	kino	k1gNnPc2	kino
museli	muset	k5eAaImAgMnP	muset
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
splácet	splácet	k5eAaImF	splácet
jeho	jeho	k3xOp3gInSc4	jeho
pronájem	pronájem	k1gInSc4	pronájem
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
značné	značný	k2eAgFnSc3d1	značná
finanční	finanční	k2eAgFnSc3d1	finanční
nákladnosti	nákladnost	k1gFnSc3	nákladnost
se	se	k3xPyFc4	se
však	však	k9	však
počet	počet	k1gInSc1	počet
zvukových	zvukový	k2eAgNnPc2d1	zvukové
kin	kino	k1gNnPc2	kino
rychle	rychle	k6eAd1	rychle
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgNnPc1	tento
kina	kino	k1gNnPc1	kino
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pro	pro	k7c4	pro
filmové	filmový	k2eAgNnSc4d1	filmové
publikum	publikum	k1gNnSc4	publikum
velkou	velký	k2eAgFnSc7d1	velká
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
500	[number]	k4	500
zvukových	zvukový	k2eAgNnPc2d1	zvukové
kin	kino	k1gNnPc2	kino
a	a	k8xC	a
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
němých	němý	k2eAgMnPc2d1	němý
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
1300	[number]	k4	1300
zvukových	zvukový	k2eAgFnPc2d1	zvuková
a	a	k8xC	a
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc6	rok
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
na	na	k7c4	na
5200	[number]	k4	5200
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInSc1d1	počáteční
záměr	záměr	k1gInSc1	záměr
většiny	většina	k1gFnSc2	většina
filmových	filmový	k2eAgFnPc2d1	filmová
společností	společnost	k1gFnPc2	společnost
ignorovat	ignorovat	k5eAaImF	ignorovat
snahu	snaha	k1gFnSc4	snaha
firmy	firma	k1gFnSc2	firma
Warner	Warner	k1gInSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
však	však	k9	však
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
pronikl	proniknout	k5eAaPmAgInS	proniknout
zvuk	zvuk	k1gInSc1	zvuk
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
filmových	filmový	k2eAgFnPc2d1	filmová
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
prvních	první	k4xOgInPc2	první
zvukových	zvukový	k2eAgInPc2d1	zvukový
filmů	film	k1gInPc2	film
byli	být	k5eAaImAgMnP	být
herci	herec	k1gMnPc1	herec
nuceni	nutit	k5eAaImNgMnP	nutit
mluvit	mluvit	k5eAaImF	mluvit
stále	stále	k6eAd1	stále
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
mikrofonu	mikrofon	k1gInSc3	mikrofon
<g/>
,	,	kIx,	,
ukrytému	ukrytý	k2eAgMnSc3d1	ukrytý
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
květináči	květináč	k1gInSc6	květináč
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnPc2	součást
dekorace	dekorace	k1gFnSc2	dekorace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značně	značně	k6eAd1	značně
omezovalo	omezovat	k5eAaImAgNnS	omezovat
jejich	jejich	k3xOp3gInSc4	jejich
pohyb	pohyb	k1gInSc4	pohyb
a	a	k8xC	a
hereckou	herecký	k2eAgFnSc4d1	herecká
akci	akce	k1gFnSc4	akce
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgNnSc4d1	další
zpracování	zpracování	k1gNnSc4	zpracování
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
zaznamenaného	zaznamenaný	k2eAgMnSc2d1	zaznamenaný
na	na	k7c6	na
speciálních	speciální	k2eAgFnPc6d1	speciální
gramofonových	gramofonový	k2eAgFnPc6d1	gramofonová
deskách	deska	k1gFnPc6	deska
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
omezené	omezený	k2eAgNnSc1d1	omezené
<g/>
,	,	kIx,	,
natáčely	natáčet	k5eAaImAgFnP	natáčet
se	se	k3xPyFc4	se
scény	scéna	k1gFnPc1	scéna
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
záběru	záběr	k1gInSc6	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vzaly	vzít	k5eAaPmAgFnP	vzít
definitivně	definitivně	k6eAd1	definitivně
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
veškeré	veškerý	k3xTgFnPc4	veškerý
montážní	montážní	k2eAgFnPc4d1	montážní
koncepce	koncepce	k1gFnPc4	koncepce
němého	němý	k2eAgInSc2d1	němý
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
režiséři	režisér	k1gMnPc1	režisér
Sergej	Sergej	k1gMnSc1	Sergej
Ejzenštejn	Ejzenštejn	k1gMnSc1	Ejzenštejn
a	a	k8xC	a
Vsevolod	Vsevolod	k1gInSc1	Vsevolod
Pudovkin	Pudovkin	k1gMnSc1	Pudovkin
vydali	vydat	k5eAaPmAgMnP	vydat
dokonce	dokonce	k9	dokonce
svůj	svůj	k3xOyFgInSc4	svůj
slavný	slavný	k2eAgInSc4d1	slavný
manifest	manifest	k1gInSc4	manifest
proti	proti	k7c3	proti
zvukovému	zvukový	k2eAgInSc3d1	zvukový
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
v	v	k7c6	v
exteriéru	exteriér	k1gInSc6	exteriér
bylo	být	k5eAaImAgNnS	být
prakticky	prakticky	k6eAd1	prakticky
úplně	úplně	k6eAd1	úplně
nemožné	možný	k2eNgNnSc1d1	nemožné
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
množství	množství	k1gNnSc3	množství
různých	různý	k2eAgInPc2d1	různý
parazitních	parazitní	k2eAgInPc2d1	parazitní
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
katastrofálně	katastrofálně	k6eAd1	katastrofálně
narušovaly	narušovat	k5eAaImAgFnP	narušovat
srozumitelnost	srozumitelnost	k1gFnSc4	srozumitelnost
dialogů	dialog	k1gInPc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
systém	systém	k1gInSc1	systém
tzv.	tzv.	kA	tzv.
zadní	zadní	k2eAgFnSc2d1	zadní
projekce	projekce	k1gFnSc2	projekce
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
na	na	k7c4	na
speciální	speciální	k2eAgNnSc4d1	speciální
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
před	před	k7c7	před
kterým	který	k3yIgInSc7	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
herecká	herecký	k2eAgFnSc1d1	herecká
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
promítala	promítat	k5eAaImAgFnS	promítat
exteriérové	exteriérový	k2eAgInPc4d1	exteriérový
záběry	záběr	k1gInPc4	záběr
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
herců	herec	k1gMnPc2	herec
se	se	k3xPyFc4	se
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
muselo	muset	k5eAaImAgNnS	muset
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
prací	práce	k1gFnSc7	práce
rozloučit	rozloučit	k5eAaPmF	rozloučit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gInSc1	jejich
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
značně	značně	k6eAd1	značně
zkreslený	zkreslený	k2eAgInSc1d1	zkreslený
technickou	technický	k2eAgFnSc7d1	technická
aparaturou	aparatura	k1gFnSc7	aparatura
<g/>
,	,	kIx,	,
vyzníval	vyznívat	k5eAaImAgMnS	vyznívat
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
zcela	zcela	k6eAd1	zcela
směšně	směšně	k6eAd1	směšně
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
většina	většina	k1gFnSc1	většina
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
herců	herec	k1gMnPc2	herec
bylo	být	k5eAaImAgNnS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jazykové	jazykový	k2eAgFnSc3d1	jazyková
bariéře	bariéra	k1gFnSc3	bariéra
nucena	nutit	k5eAaImNgFnS	nutit
opustit	opustit	k5eAaPmF	opustit
hollywoodská	hollywoodský	k2eAgNnPc1d1	hollywoodské
studia	studio	k1gNnPc1	studio
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
i	i	k9	i
např.	např.	kA	např.
Emil	Emil	k1gMnSc1	Emil
Jannings	Jannings	k1gInSc1	Jannings
a	a	k8xC	a
Pola	pola	k1gFnSc1	pola
Negri	Negr	k1gFnSc2	Negr
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
ztratila	ztratit	k5eAaPmAgFnS	ztratit
i	i	k9	i
malá	malý	k2eAgNnPc1d1	malé
hudební	hudební	k2eAgNnPc1d1	hudební
tělesa	těleso	k1gNnPc1	těleso
a	a	k8xC	a
pianisté	pianista	k1gMnPc1	pianista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
v	v	k7c6	v
němých	němý	k2eAgNnPc6d1	němé
kinech	kino	k1gNnPc6	kino
zvukový	zvukový	k2eAgInSc1d1	zvukový
doprovod	doprovod	k1gInSc1	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhou	zásluhou	k7c2	zásluhou
zvuku	zvuk	k1gInSc2	zvuk
se	se	k3xPyFc4	se
také	také	k9	také
podstatně	podstatně	k6eAd1	podstatně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
zájem	zájem	k1gInSc1	zájem
publika	publikum	k1gNnSc2	publikum
o	o	k7c4	o
filmové	filmový	k2eAgInPc4d1	filmový
týdeníky	týdeník	k1gInPc4	týdeník
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
William	William	k1gInSc4	William
Fox	fox	k1gInSc1	fox
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
od	od	k7c2	od
Western	Western	kA	Western
Electric	Electrice	k1gFnPc2	Electrice
jejich	jejich	k3xOp3gInSc4	jejich
systém	systém	k1gInSc4	systém
Movietone	Movieton	k1gInSc5	Movieton
a	a	k8xC	a
použil	použít	k5eAaPmAgInS	použít
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
ozvučení	ozvučení	k1gNnSc3	ozvučení
svých	svůj	k3xOyFgInPc2	svůj
týdenníků	týdenník	k1gInPc2	týdenník
<g/>
,	,	kIx,	,
natáčených	natáčený	k2eAgInPc2d1	natáčený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
byli	být	k5eAaImAgMnP	být
zvukovou	zvukový	k2eAgFnSc7d1	zvuková
kulisou	kulisa	k1gFnSc7	kulisa
exotických	exotický	k2eAgFnPc2d1	exotická
krajin	krajina	k1gFnPc2	krajina
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
a	a	k8xC	a
přicházely	přicházet	k5eAaImAgInP	přicházet
další	další	k2eAgInPc1d1	další
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
peníze	peníz	k1gInPc1	peníz
do	do	k7c2	do
pokladen	pokladna	k1gFnPc2	pokladna
biografů	biograf	k1gInPc2	biograf
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
v	v	k7c6	v
"	"	kIx"	"
<g/>
Černý	černý	k2eAgInSc4d1	černý
pátek	pátek	k1gInSc4	pátek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
znamenal	znamenat	k5eAaImAgInS	znamenat
krach	krach	k1gInSc1	krach
newyorské	newyorský	k2eAgFnSc2d1	newyorská
burzy	burza	k1gFnSc2	burza
začátek	začátek	k1gInSc1	začátek
světové	světový	k2eAgFnSc2d1	světová
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
byly	být	k5eAaImAgFnP	být
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
banky	banka	k1gFnPc1	banka
krachovaly	krachovat	k5eAaBmAgFnP	krachovat
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
produktivita	produktivita	k1gFnSc1	produktivita
klesla	klesnout	k5eAaPmAgFnS	klesnout
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
</s>
<s>
inflace	inflace	k1gFnSc1	inflace
oslabila	oslabit	k5eAaPmAgFnS	oslabit
kupní	kupní	k2eAgFnSc4d1	kupní
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
nebyl	být	k5eNaImAgInS	být
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
utrpením	utrpení	k1gNnSc7	utrpení
světové	světový	k2eAgFnSc2d1	světová
krize	krize	k1gFnSc2	krize
příliš	příliš	k6eAd1	příliš
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
nabízelo	nabízet	k5eAaImAgNnS	nabízet
lidem	člověk	k1gMnPc3	člověk
nejdostupnější	dostupný	k2eAgFnSc4d3	nejdostupnější
a	a	k8xC	a
nejprostší	prostý	k2eAgFnSc4d3	nejprostší
formu	forma	k1gFnSc4	forma
úniku	únik	k1gInSc2	únik
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
se	se	k3xPyFc4	se
nuceně	nuceně	k6eAd1	nuceně
začala	začít	k5eAaPmAgFnS	začít
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
zvukovému	zvukový	k2eAgInSc3d1	zvukový
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
asi	asi	k9	asi
20	[number]	k4	20
500	[number]	k4	500
amerických	americký	k2eAgNnPc2d1	americké
kin	kino	k1gNnPc2	kino
a	a	k8xC	a
novinka	novinka	k1gFnSc1	novinka
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
i	i	k9	i
v	v	k7c6	v
evropských	evropský	k2eAgNnPc6d1	Evropské
hlavních	hlavní	k2eAgNnPc6d1	hlavní
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Přestavba	přestavba	k1gFnSc1	přestavba
kina	kino	k1gNnSc2	kino
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
náklady	náklad	k1gInPc7	náklad
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
získat	získat	k5eAaPmF	získat
během	během	k7c2	během
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
byly	být	k5eAaImAgInP	být
značně	značně	k6eAd1	značně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
rostly	růst	k5eAaImAgInP	růst
i	i	k9	i
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
zvukových	zvukový	k2eAgInPc2d1	zvukový
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
trhu	trh	k1gInSc6	trh
prodávány	prodávat	k5eAaImNgInP	prodávat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
mnohé	mnohý	k2eAgInPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
natáčeny	natáčen	k2eAgFnPc1d1	natáčena
hned	hned	k6eAd1	hned
víckrát	víckrát	k6eAd1	víckrát
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
německými	německý	k2eAgFnPc7d1	německá
<g/>
,	,	kIx,	,
anglickými	anglický	k2eAgInPc7d1	anglický
a	a	k8xC	a
francouzskými	francouzský	k2eAgInPc7d1	francouzský
herci	herc	k1gInPc7	herc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
natáčení	natáčení	k1gNnSc4	natáčení
více	hodně	k6eAd2	hodně
verzí	verze	k1gFnSc7	verze
vzácnost	vzácnost	k1gFnSc1	vzácnost
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
titulky	titulek	k1gInPc4	titulek
a	a	k8xC	a
dabing	dabing	k1gInSc4	dabing
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
němé	němý	k2eAgFnSc2d1	němá
éry	éra	k1gFnSc2	éra
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
německý	německý	k2eAgInSc4d1	německý
film	film	k1gInSc4	film
obdobím	období	k1gNnSc7	období
úpadku	úpadek	k1gInSc2	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
francouzský	francouzský	k2eAgMnSc1d1	francouzský
kritik	kritik	k1gMnSc1	kritik
Georges	Georges	k1gMnSc1	Georges
Charensol	Charensol	k1gInSc4	Charensol
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vývoj	vývoj	k1gInSc1	vývoj
německé	německý	k2eAgFnSc2d1	německá
kinematografie	kinematografie	k1gFnSc2	kinematografie
spěl	spět	k5eAaImAgInS	spět
rychle	rychle	k6eAd1	rychle
k	k	k7c3	k
umění	umění	k1gNnSc3	umění
hloubce	hloubka	k1gFnSc6	hloubka
komerčnímu	komerční	k2eAgInSc3d1	komerční
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kromě	kromě	k7c2	kromě
jednoho	jeden	k4xCgInSc2	jeden
či	či	k8xC	či
dvou	dva	k4xCgInPc2	dva
filmů	film	k1gInPc2	film
hodnotných	hodnotný	k2eAgMnPc2d1	hodnotný
jsme	být	k5eAaImIp1nP	být
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
vidět	vidět	k5eAaImF	vidět
jen	jen	k9	jen
záplavu	záplava	k1gFnSc4	záplava
výrobků	výrobek	k1gInPc2	výrobek
krajně	krajně	k6eAd1	krajně
podprůměrných	podprůměrný	k2eAgInPc2d1	podprůměrný
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
také	také	k9	také
začínali	začínat	k5eAaImAgMnP	začínat
němečtí	německý	k2eAgMnPc1d1	německý
režiséři	režisér	k1gMnPc1	režisér
emigrovat	emigrovat	k5eAaBmF	emigrovat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
slavných	slavný	k2eAgMnPc2d1	slavný
filmových	filmový	k2eAgMnPc2d1	filmový
režisérů	režisér	k1gMnPc2	režisér
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
1925	[number]	k4	1925
zůstal	zůstat	k5eAaPmAgMnS	zůstat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
pouze	pouze	k6eAd1	pouze
Fritz	Fritz	k1gMnSc1	Fritz
Lang	Lang	k1gMnSc1	Lang
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc1	jeho
poslední	poslední	k2eAgInPc1d1	poslední
němé	němý	k2eAgInPc1d1	němý
filmy	film	k1gInPc1	film
už	už	k6eAd1	už
nedosahovaly	dosahovat	k5eNaImAgInP	dosahovat
takového	takový	k3xDgInSc2	takový
úspěchu	úspěch	k1gInSc2	úspěch
jako	jako	k8xC	jako
Metropolis	Metropolis	k1gFnSc1	Metropolis
nebo	nebo	k8xC	nebo
Unavená	unavený	k2eAgFnSc1d1	unavená
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Inovace	inovace	k1gFnSc1	inovace
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vrátila	vrátit	k5eAaPmAgFnS	vrátit
většina	většina	k1gFnSc1	většina
emigrovaných	emigrovaný	k2eAgMnPc2d1	emigrovaný
režisérů	režisér	k1gMnPc2	režisér
a	a	k8xC	a
herců	herec	k1gMnPc2	herec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
cizí	cizí	k2eAgInSc4d1	cizí
přízvuk	přízvuk	k1gInSc4	přízvuk
své	svůj	k3xOyFgFnSc2	svůj
anglické	anglický	k2eAgFnSc2d1	anglická
výslovnosti	výslovnost	k1gFnSc2	výslovnost
či	či	k8xC	či
pro	pro	k7c4	pro
úplnou	úplný	k2eAgFnSc4d1	úplná
neznalost	neznalost	k1gFnSc4	neznalost
jazyka	jazyk	k1gInSc2	jazyk
stali	stát	k5eAaPmAgMnP	stát
pro	pro	k7c4	pro
Hollywood	Hollywood	k1gInSc4	Hollywood
nepotřební	potřební	k2eNgInSc4d1	nepotřební
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
vzplanul	vzplanout	k5eAaPmAgMnS	vzplanout
požár	požár	k1gInSc4	požár
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
vláda	vláda	k1gFnSc1	vláda
využila	využít	k5eAaPmAgFnS	využít
jako	jako	k8xC	jako
záminky	záminka	k1gFnSc2	záminka
k	k	k7c3	k
masivnímu	masivní	k2eAgNnSc3d1	masivní
omezování	omezování	k1gNnSc3	omezování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
emigrace	emigrace	k1gFnSc1	emigrace
německých	německý	k2eAgMnPc2d1	německý
filmových	filmový	k2eAgMnPc2d1	filmový
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tři	tři	k4xCgMnPc1	tři
nejvýraznější	výrazný	k2eAgMnPc1d3	nejvýraznější
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Pabst	Pabst	k1gMnSc1	Pabst
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Lang	Lang	k1gMnSc1	Lang
a	a	k8xC	a
Erich	Erich	k1gMnSc1	Erich
Pommer	Pommer	k1gMnSc1	Pommer
<g/>
,	,	kIx,	,
opustili	opustit	k5eAaPmAgMnP	opustit
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Lang	Lang	k1gMnSc1	Lang
tu	tu	k6eAd1	tu
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Pommera	Pommero	k1gNnSc2	Pommero
film	film	k1gInSc1	film
Liliom	Liliom	k1gInSc4	Liliom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
nesklidil	sklidit	k5eNaPmAgInS	sklidit
velký	velký	k2eAgInSc1d1	velký
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
dokončení	dokončení	k1gNnSc6	dokončení
odjeli	odjet	k5eAaPmAgMnP	odjet
Pommer	Pommer	k1gMnSc1	Pommer
i	i	k8xC	i
Lang	Lang	k1gMnSc1	Lang
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Pabst	Pabst	k1gMnSc1	Pabst
odjel	odjet	k5eAaPmAgMnS	odjet
také	také	k9	také
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pobyt	pobyt	k1gInSc1	pobyt
neměl	mít	k5eNaImAgInS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
zklamáním	zklamání	k1gNnSc7	zklamání
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
Hitlera	Hitler	k1gMnSc2	Hitler
odjel	odjet	k5eAaPmAgMnS	odjet
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
Joseph	Joseph	k1gInSc4	Joseph
Goebbels	Goebbelsa	k1gFnPc2	Goebbelsa
velmistrem	velmistr	k1gMnSc7	velmistr
německého	německý	k2eAgInSc2d1	německý
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
udělat	udělat	k5eAaPmF	udělat
z	z	k7c2	z
UFY	UFY	kA	UFY
distribuční	distribuční	k2eAgInSc1d1	distribuční
monopol	monopol	k1gInSc1	monopol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
dosud	dosud	k6eAd1	dosud
ovládali	ovládat	k5eAaImAgMnP	ovládat
zástupci	zástupce	k1gMnPc1	zástupce
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
propaganda	propaganda	k1gFnSc1	propaganda
mimořádně	mimořádně	k6eAd1	mimořádně
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
politiky	politika	k1gFnSc2	politika
všech	všecek	k3xTgFnPc2	všecek
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
běžnou	běžný	k2eAgFnSc7d1	běžná
praxí	praxe	k1gFnSc7	praxe
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
monitorování	monitorování	k1gNnSc4	monitorování
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
vyhodnocování	vyhodnocování	k1gNnSc3	vyhodnocování
denního	denní	k2eAgInSc2d1	denní
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
filmového	filmový	k2eAgNnSc2d1	filmové
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
apod.	apod.	kA	apod.
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnSc2	jejich
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
za	za	k7c2	za
současného	současný	k2eAgNnSc2d1	současné
sledování	sledování	k1gNnSc2	sledování
reakcí	reakce	k1gFnPc2	reakce
veřejnosti	veřejnost	k1gFnSc2	veřejnost
pak	pak	k6eAd1	pak
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
vliv	vliv	k1gInSc4	vliv
jak	jak	k8xC	jak
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
protivníkovy	protivníkův	k2eAgFnPc4d1	protivníkova
propagandy	propaganda	k1gFnPc4	propaganda
a	a	k8xC	a
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
kontrapropagandu	kontrapropaganda	k1gFnSc4	kontrapropaganda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
vyvracet	vyvracet	k5eAaImF	vyvracet
protichůdné	protichůdný	k2eAgInPc4d1	protichůdný
propagandistické	propagandistický	k2eAgInPc4d1	propagandistický
koncepty	koncept	k1gInPc4	koncept
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
usilovala	usilovat	k5eAaImAgFnS	usilovat
nacistická	nacistický	k2eAgFnSc1d1	nacistická
propaganda	propaganda	k1gFnSc1	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
propagandy	propaganda	k1gFnSc2	propaganda
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
osvěty	osvěta	k1gFnSc2	osvěta
<g/>
,	,	kIx,	,
šéf	šéf	k1gMnSc1	šéf
propagandistického	propagandistický	k2eAgInSc2d1	propagandistický
aparátu	aparát	k1gInSc2	aparát
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
filmové	filmový	k2eAgFnSc2d1	filmová
komory	komora	k1gFnSc2	komora
Goebbles	Goebblesa	k1gFnPc2	Goebblesa
<g/>
,	,	kIx,	,
co	co	k9	co
by	by	kYmCp3nS	by
fanoušek	fanoušek	k1gMnSc1	fanoušek
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
domáhal	domáhat	k5eAaImAgMnS	domáhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
velkolepý	velkolepý	k2eAgInSc1d1	velkolepý
film	film	k1gInSc1	film
podobný	podobný	k2eAgInSc1d1	podobný
ruskému	ruský	k2eAgInSc3d1	ruský
Křižníku	křižník	k1gInSc3	křižník
Potěmkinovi	Potěmkin	k1gMnSc3	Potěmkin
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
úžasně	úžasně	k6eAd1	úžasně
dobře	dobře	k6eAd1	dobře
udělaný	udělaný	k2eAgInSc1d1	udělaný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
filmařské	filmařský	k2eAgNnSc4d1	filmařské
mistrovství	mistrovství	k1gNnSc4	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
k	k	k7c3	k
bolševikům	bolševik	k1gMnPc3	bolševik
přivést	přivést	k5eAaPmF	přivést
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
bez	bez	k7c2	bez
pevného	pevný	k2eAgNnSc2d1	pevné
ideologického	ideologický	k2eAgNnSc2d1	ideologické
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
může	moct	k5eAaImIp3nS	moct
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
politickým	politický	k2eAgInPc3d1	politický
požadavkům	požadavek	k1gInPc3	požadavek
a	a	k8xC	a
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
sdělován	sdělovat	k5eAaImNgInS	sdělovat
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
dokonce	dokonce	k9	dokonce
i	i	k9	i
ten	ten	k3xDgInSc4	ten
nejodpornější	odporný	k2eAgInSc4d3	nejodpornější
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřen	k2eAgMnSc1d1	vyjádřen
skrze	skrze	k?	skrze
médium	médium	k1gNnSc1	médium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vynikající	vynikající	k2eAgFnSc4d1	vynikající
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
"	"	kIx"	"
<g/>
..	..	k?	..
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
měl	mít	k5eAaImAgMnS	mít
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
<g/>
,	,	kIx,	,
však	však	k9	však
natolik	natolik	k6eAd1	natolik
zklamal	zklamat	k5eAaPmAgMnS	zklamat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Hitler	Hitler	k1gMnSc1	Hitler
při	při	k7c6	při
zřizování	zřizování	k1gNnSc6	zřizování
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
propagandy	propaganda	k1gFnSc2	propaganda
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
osvěty	osvěta	k1gFnSc2	osvěta
důrazně	důrazně	k6eAd1	důrazně
požadoval	požadovat	k5eAaImAgMnS	požadovat
provádět	provádět	k5eAaImF	provádět
velkorysou	velkorysý	k2eAgFnSc4d1	velkorysá
propagační	propagační	k2eAgFnSc4d1	propagační
a	a	k8xC	a
osvětovou	osvětový	k2eAgFnSc4d1	osvětová
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
letargii	letargie	k1gFnSc3	letargie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
společnost	společnost	k1gFnSc4	společnost
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
spíše	spíše	k9	spíše
apatie	apatie	k1gFnSc1	apatie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
té	ten	k3xDgFnSc3	ten
přispívala	přispívat	k5eAaImAgFnS	přispívat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
i	i	k8xC	i
únava	únava	k1gFnSc1	únava
z	z	k7c2	z
přemíry	přemíra	k1gFnSc2	přemíra
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
navenek	navenek	k6eAd1	navenek
projevovaný	projevovaný	k2eAgInSc1d1	projevovaný
souhlas	souhlas	k1gInSc1	souhlas
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
motivován	motivovat	k5eAaBmNgInS	motivovat
strachem	strach	k1gInSc7	strach
z	z	k7c2	z
možné	možný	k2eAgFnSc2d1	možná
represe	represe	k1gFnSc2	represe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1942	[number]	k4	1942
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
firma	firma	k1gFnSc1	firma
UFA	UFA	kA	UFA
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
sdružovala	sdružovat	k5eAaImAgFnS	sdružovat
všechny	všechen	k3xTgInPc4	všechen
německé	německý	k2eAgFnSc3d1	německá
produkční	produkční	k2eAgFnSc3d1	produkční
společnosti	společnost	k1gFnSc3	společnost
-	-	kIx~	-
Bavaria	Bavarium	k1gNnSc2	Bavarium
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Berlin-Film	Berlin-Film	k1gInSc1	Berlin-Film
<g/>
,	,	kIx,	,
Terra	Terra	k1gFnSc1	Terra
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Tobis	Tobis	k1gInSc1	Tobis
AG	AG	kA	AG
<g/>
,	,	kIx,	,
Prag-Film	Prag-Filma	k1gFnPc2	Prag-Filma
a	a	k8xC	a
Wien-Film	Wien-Filma	k1gFnPc2	Wien-Filma
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kinematografické	kinematografický	k2eAgInPc1d1	kinematografický
podniky	podnik	k1gInPc1	podnik
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
sama	sám	k3xTgFnSc1	sám
strana	strana	k1gFnSc1	strana
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Deutsche	Deutsche	k1gFnSc1	Deutsche
Filmherstellungs-	Filmherstellungs-	k1gFnSc2	Filmherstellungs-
und	und	k?	und
Verwertungs-GmbH	Verwertungs-GmbH	k1gFnSc2	Verwertungs-GmbH
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
DFG	DFG	kA	DFG
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
DFG	DFG	kA	DFG
postupně	postupně	k6eAd1	postupně
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
sérii	série	k1gFnSc4	série
filmů	film	k1gInPc2	film
líčících	líčící	k2eAgMnPc2d1	líčící
teritoriální	teritoriální	k2eAgFnSc4d1	teritoriální
expanzi	expanze	k1gFnSc4	expanze
Třetí	třetí	k4xOgFnSc2	třetí
Říše	říš	k1gFnSc2	říš
-	-	kIx~	-
prvním	první	k4xOgMnSc6	první
byl	být	k5eAaImAgMnS	být
třicetiminutový	třicetiminutový	k2eAgInSc4d1	třicetiminutový
Osudový	osudový	k2eAgInSc4d1	osudový
obrat	obrat	k1gInSc4	obrat
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
podat	podat	k5eAaPmF	podat
členům	člen	k1gMnPc3	člen
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
i	i	k8xC	i
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
shrnující	shrnující	k2eAgInSc4d1	shrnující
obraz	obraz	k1gInSc4	obraz
politicko-vojenské	politickoojenský	k2eAgFnSc2d1	politicko-vojenská
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
rozbití	rozbití	k1gNnSc3	rozbití
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
morálně	morálně	k6eAd1	morálně
<g/>
"	"	kIx"	"
ji	on	k3xPp3gFnSc4	on
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vznikal	vznikat	k5eAaImAgInS	vznikat
pod	pod	k7c7	pod
značným	značný	k2eAgInSc7d1	značný
časovým	časový	k2eAgInSc7d1	časový
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
ohledu	ohled	k1gInSc6	ohled
relativně	relativně	k6eAd1	relativně
skromný	skromný	k2eAgMnSc1d1	skromný
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
filmem	film	k1gInSc7	film
DFG	DFG	kA	DFG
byl	být	k5eAaImAgInS	být
Polské	polský	k2eAgNnSc4d1	polské
tažení	tažení	k1gNnSc4	tažení
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
líčilo	líčit	k5eAaImAgNnS	líčit
okolnosti	okolnost	k1gFnPc4	okolnost
německého	německý	k2eAgInSc2d1	německý
vpádu	vpád	k1gInSc2	vpád
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgInS	následovat
snímek	snímek	k1gInSc1	snímek
Vítězství	vítězství	k1gNnSc2	vítězství
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
popisující	popisující	k2eAgNnSc1d1	popisující
německé	německý	k2eAgNnSc1d1	německé
tažení	tažení	k1gNnSc1	tažení
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
posledním	poslední	k2eAgInSc7d1	poslední
filmem	film	k1gInSc7	film
série	série	k1gFnSc2	série
byl	být	k5eAaImAgInS	být
Věčný	věčný	k2eAgMnSc1d1	věčný
Žid	Žid	k1gMnSc1	Žid
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
antisemitský	antisemitský	k2eAgInSc1d1	antisemitský
pseudodokument	pseudodokument	k1gInSc1	pseudodokument
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaPmNgInP	využívat
především	především	k9	především
snímky	snímek	k1gInPc4	snímek
z	z	k7c2	z
polských	polský	k2eAgNnPc2d1	polské
ghett	ghetto	k1gNnPc2	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Nacistický	nacistický	k2eAgInSc1d1	nacistický
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
plnit	plnit	k5eAaImF	plnit
mnoha	mnoho	k4c7	mnoho
průměrnými	průměrný	k2eAgInPc7d1	průměrný
a	a	k8xC	a
podprůměrnými	podprůměrný	k2eAgInPc7d1	podprůměrný
talenty	talent	k1gInPc7	talent
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
označili	označit	k5eAaPmAgMnP	označit
kammerspiel	kammerspiel	k1gInSc4	kammerspiel
a	a	k8xC	a
expresionismus	expresionismus	k1gInSc4	expresionismus
za	za	k7c4	za
projevy	projev	k1gInPc4	projev
"	"	kIx"	"
<g/>
zvrhlého	zvrhlý	k2eAgNnSc2d1	zvrhlé
umění	umění	k1gNnSc2	umění
<g/>
"	"	kIx"	"
a	a	k8xC	a
kteří	který	k3yIgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
natáčet	natáčet	k5eAaImF	natáčet
pro	pro	k7c4	pro
UFU	UFU	kA	UFU
výpravné	výpravný	k2eAgInPc4d1	výpravný
historické	historický	k2eAgInPc4d1	historický
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
film	film	k1gInSc1	film
Flétnový	flétnový	k2eAgInSc1d1	flétnový
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Sanssouci	Sanssouci	k1gNnSc6	Sanssouci
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
i	i	k9	i
když	když	k8xS	když
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
mělo	mít	k5eAaImAgNnS	mít
plné	plný	k2eAgInPc4d1	plný
sklady	sklad	k1gInPc4	sklad
kinematografického	kinematografický	k2eAgInSc2d1	kinematografický
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vybavené	vybavený	k2eAgInPc1d1	vybavený
ateliéry	ateliér	k1gInPc1	ateliér
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
filmy	film	k1gInPc1	film
neměly	mít	k5eNaImAgFnP	mít
žádnou	žádný	k3yNgFnSc4	žádný
významnou	významný	k2eAgFnSc4d1	významná
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgInP	sloužit
především	především	k9	především
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohly	moct	k5eAaImAgInP	moct
vzbudit	vzbudit	k5eAaPmF	vzbudit
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
ostře	ostro	k6eAd1	ostro
kritizovány	kritizován	k2eAgFnPc1d1	kritizována
<g/>
,	,	kIx,	,
cenzurovány	cenzurován	k2eAgFnPc1d1	cenzurována
nebo	nebo	k8xC	nebo
zakázány	zakázán	k2eAgFnPc1d1	zakázána
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
zvuku	zvuk	k1gInSc2	zvuk
přinesl	přinést	k5eAaPmAgInS	přinést
francouzské	francouzský	k2eAgFnSc3d1	francouzská
kinematografii	kinematografie	k1gFnSc3	kinematografie
optimismus	optimismus	k1gInSc4	optimismus
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
filmů	film	k1gInPc2	film
z	z	k7c2	z
ročních	roční	k2eAgInPc2d1	roční
padesáti	padesát	k4xCc2	padesát
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
padesát	padesát	k4xCc4	padesát
<g/>
.	.	kIx.	.
</s>
<s>
Trh	trh	k1gInSc1	trh
ovládaly	ovládat	k5eAaImAgInP	ovládat
dva	dva	k4xCgInPc1	dva
filmové	filmový	k2eAgInPc1d1	filmový
monopoly	monopol	k1gInPc1	monopol
-	-	kIx~	-
Pathé-Nathan	Pathé-Nathan	k1gInSc1	Pathé-Nathan
a	a	k8xC	a
Gaumont-Franco-Film-Aubert	Gaumont-Franco-Film-Aubert	k1gInSc1	Gaumont-Franco-Film-Aubert
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
neměla	mít	k5eNaImAgFnS	mít
možnost	možnost	k1gFnSc4	možnost
využívat	využívat	k5eAaImF	využívat
komerční	komerční	k2eAgInSc4d1	komerční
zvukový	zvukový	k2eAgInSc4d1	zvukový
systém	systém	k1gInSc4	systém
a	a	k8xC	a
tak	tak	k6eAd1	tak
musela	muset	k5eAaImAgFnS	muset
platit	platit	k5eAaImF	platit
za	za	k7c4	za
zvukové	zvukový	k2eAgNnSc4d1	zvukové
zařízení	zařízení	k1gNnSc4	zařízení
kin	kino	k1gNnPc2	kino
a	a	k8xC	a
ateliérů	ateliér	k1gInPc2	ateliér
obrovské	obrovský	k2eAgInPc1d1	obrovský
poplatky	poplatek	k1gInPc1	poplatek
Americe	Amerika	k1gFnSc3	Amerika
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k8xC	i
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
náklady	náklad	k1gInPc1	náklad
brzdily	brzdit	k5eAaImAgInP	brzdit
dovoz	dovoz	k1gInSc4	dovoz
cizích	cizí	k2eAgInPc2d1	cizí
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Bernard	Bernard	k1gMnSc1	Bernard
Natan	Natan	k1gMnSc1	Natan
<g/>
,	,	kIx,	,
spoluvlastník	spoluvlastník	k1gMnSc1	spoluvlastník
Pathé	Pathý	k2eAgFnSc2d1	Pathý
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgInS	obvinit
z	z	k7c2	z
podvodů	podvod	k1gInPc2	podvod
a	a	k8xC	a
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
krach	krach	k1gInSc4	krach
jeho	jeho	k3xOp3gFnSc2	jeho
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
a	a	k8xC	a
zanedlouho	zanedlouho	k6eAd1	zanedlouho
i	i	k9	i
Gaumont-Franco-Film-Aubert	Gaumont-Franco-Film-Aubert	k1gMnSc1	Gaumont-Franco-Film-Aubert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
oněch	onen	k3xDgNnPc2	onen
černých	černý	k2eAgNnPc2d1	černé
let	léto	k1gNnPc2	léto
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kinematografie	kinematografie	k1gFnSc2	kinematografie
stačilo	stačit	k5eAaBmAgNnS	stačit
trochu	trocha	k1gFnSc4	trocha
upřímnosti	upřímnost	k1gFnSc2	upřímnost
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dětských	dětský	k2eAgFnPc2d1	dětská
tváří	tvář	k1gFnPc2	tvář
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
všední	všední	k2eAgInSc4d1	všední
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
počalo	počnout	k5eAaPmAgNnS	počnout
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
mistrovském	mistrovský	k2eAgNnSc6d1	mistrovské
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
toto	tento	k3xDgNnSc1	tento
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
francouzská	francouzský	k2eAgFnSc1d1	francouzská
produkce	produkce	k1gFnSc1	produkce
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
rapidním	rapidní	k2eAgInSc6d1	rapidní
poklesu	pokles	k1gInSc6	pokles
a	a	k8xC	a
jen	jen	k9	jen
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
filmů	film	k1gInPc2	film
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Krachující	krachující	k2eAgInPc1d1	krachující
filmové	filmový	k2eAgInPc1d1	filmový
monopoly	monopol	k1gInPc1	monopol
však	však	k9	však
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
volné	volný	k2eAgFnSc3d1	volná
konkurenci	konkurence	k1gFnSc3	konkurence
-	-	kIx~	-
"	"	kIx"	"
<g/>
Naštěstí	naštěstí	k6eAd1	naštěstí
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
úpadek	úpadek	k1gInSc1	úpadek
hlavních	hlavní	k2eAgFnPc2d1	hlavní
velkých	velký	k2eAgFnPc2d1	velká
společností	společnost	k1gFnPc2	společnost
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
nezávislým	závislý	k2eNgMnPc3d1	nezávislý
producentům	producent	k1gMnPc3	producent
a	a	k8xC	a
režisérům	režisér	k1gMnPc3	režisér
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
natáčet	natáčet	k5eAaImF	natáčet
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
tvořily	tvořit	k5eAaImAgFnP	tvořit
slavnou	slavný	k2eAgFnSc4d1	slavná
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
lze	lze	k6eAd1	lze
nazvat	nazvat	k5eAaPmF	nazvat
poeticko-realistickou	poetickoealistický	k2eAgFnSc4d1	poeticko-realistický
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Začala	začít	k5eAaPmAgFnS	začít
tak	tak	k6eAd1	tak
nová	nový	k2eAgFnSc1d1	nová
éra	éra	k1gFnSc1	éra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
příchodem	příchod	k1gInSc7	příchod
Jacquesa	Jacques	k1gMnSc2	Jacques
Feydera	Feyder	k1gMnSc2	Feyder
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
natočil	natočit	k5eAaBmAgMnS	natočit
známé	známý	k2eAgInPc4d1	známý
filmy	film	k1gInPc4	film
jako	jako	k8xS	jako
např.	např.	kA	např.
Velká	velká	k1gFnSc1	velká
hra	hra	k1gFnSc1	hra
nebo	nebo	k8xC	nebo
Penzión	penzión	k1gInSc1	penzión
Memosa	Memosa	k1gFnSc1	Memosa
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
druhý	druhý	k4xOgInSc1	druhý
zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
určité	určitý	k2eAgFnPc4d1	určitá
společenské	společenský	k2eAgFnPc4d1	společenská
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
hrou	hra	k1gFnSc7	hra
-	-	kIx~	-
krupiéra	krupiér	k1gMnSc2	krupiér
z	z	k7c2	z
Monte	Mont	k1gInSc5	Mont
Carla	Carl	k1gMnSc4	Carl
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
režisér	režisér	k1gMnSc1	režisér
film	film	k1gInSc4	film
komentoval	komentovat	k5eAaBmAgMnS	komentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
"	"	kIx"	"
<g/>
konfliktu	konflikt	k1gInSc2	konflikt
mezi	mezi	k7c7	mezi
dělníky	dělník	k1gMnPc7	dělník
a	a	k8xC	a
zaměstnavateli	zaměstnavatel	k1gMnPc7	zaměstnavatel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgFnPc7	dva
filmy	film	k1gInPc4	film
se	se	k3xPyFc4	se
Feyder	Feyder	k1gInSc1	Feyder
stal	stát	k5eAaPmAgInS	stát
první	první	k4xOgFnSc7	první
osobností	osobnost	k1gFnSc7	osobnost
nové	nový	k2eAgFnSc2d1	nová
francouzské	francouzský	k2eAgFnSc2d1	francouzská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
seskupovala	seskupovat	k5eAaImAgFnS	seskupovat
Jeana	Jean	k1gMnSc4	Jean
Renoira	Renoir	k1gMnSc4	Renoir
<g/>
,	,	kIx,	,
Marcela	Marcel	k1gMnSc2	Marcel
Carného	Carný	k1gMnSc2	Carný
<g/>
,	,	kIx,	,
Jeana	Jean	k1gMnSc2	Jean
Viga	Vigus	k1gMnSc2	Vigus
<g/>
,	,	kIx,	,
Marcela	Marcel	k1gMnSc2	Marcel
Pagnola	Pagnola	k1gFnSc1	Pagnola
a	a	k8xC	a
Juliena	Julien	k2eAgFnSc1d1	Juliena
Duviviera	Duviviera	k1gFnSc1	Duviviera
<g/>
.	.	kIx.	.
</s>
<s>
Renoir	Renoir	k1gInSc1	Renoir
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
filmy	film	k1gInPc7	film
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
celou	celý	k2eAgFnSc4d1	celá
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
kinematografii	kinematografie	k1gFnSc4	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
totiž	totiž	k9	totiž
využít	využít	k5eAaPmF	využít
přirozených	přirozený	k2eAgInPc2d1	přirozený
zdrojů	zdroj	k1gInPc2	zdroj
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
a	a	k8xC	a
zapojení	zapojení	k1gNnSc2	zapojení
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
pak	pak	k6eAd1	pak
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
hloubku	hloubka	k1gFnSc4	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
zobrazit	zobrazit	k5eAaPmF	zobrazit
stejně	stejně	k6eAd1	stejně
ostře	ostro	k6eAd1	ostro
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
prostorovém	prostorový	k2eAgInSc6d1	prostorový
odstupu	odstup	k1gInSc6	odstup
<g/>
.	.	kIx.	.
</s>
<s>
Úběžný	úběžný	k2eAgInSc4d1	úběžný
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
vše	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
směřuje	směřovat	k5eAaImIp3nS	směřovat
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
částmi	část	k1gFnPc7	část
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
smysl	smysl	k1gInSc4	smysl
i	i	k9	i
nejmenším	malý	k2eAgInPc3d3	nejmenší
detailům	detail	k1gInPc3	detail
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
optiky	optika	k1gFnSc2	optika
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgMnS	moct
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
střihu	střih	k1gInSc2	střih
přecházet	přecházet	k5eAaImF	přecházet
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
plánů	plán	k1gInPc2	plán
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohl	moct	k5eAaImAgInS	moct
naplno	naplno	k6eAd1	naplno
uplatit	uplatit	k5eAaPmF	uplatit
své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Pravidla	pravidlo	k1gNnPc4	pravidlo
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
odhalil	odhalit	k5eAaPmAgMnS	odhalit
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
orientovala	orientovat	k5eAaBmAgFnS	orientovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
srovnal	srovnat	k5eAaPmAgMnS	srovnat
svět	svět	k1gInSc4	svět
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
sloužících	sloužící	k1gFnPc2	sloužící
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
provokativní	provokativní	k2eAgFnSc1d1	provokativní
směs	směs	k1gFnSc1	směs
melodramatu	melodramatu	k?	melodramatu
a	a	k8xC	a
frašky	fraška	k1gFnSc2	fraška
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
premiéře	premiéra	k1gFnSc6	premiéra
zakázán	zakázán	k2eAgInSc4d1	zakázán
coby	coby	k?	coby
"	"	kIx"	"
<g/>
demoralizující	demoralizující	k2eAgFnSc4d1	demoralizující
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Podivná	podivný	k2eAgFnSc1d1	podivná
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
téměř	téměř	k6eAd1	téměř
přerušila	přerušit	k5eAaPmAgFnS	přerušit
filmovou	filmový	k2eAgFnSc4d1	filmová
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
již	již	k6eAd1	již
brzdil	brzdit	k5eAaImAgInS	brzdit
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Duvivier	Duvivier	k1gMnSc1	Duvivier
<g/>
,	,	kIx,	,
Renoir	Renoir	k1gMnSc1	Renoir
<g/>
,	,	kIx,	,
Gabin	Gabin	k1gMnSc1	Gabin
a	a	k8xC	a
Morganová	Morganová	k1gFnSc1	Morganová
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Feyder	Feyder	k1gMnSc1	Feyder
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
i	i	k9	i
když	když	k8xS	když
počet	počet	k1gInSc4	počet
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
klesal	klesat	k5eAaImAgInS	klesat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
dvě	dva	k4xCgFnPc4	dva
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
,	,	kIx,	,
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
a	a	k8xC	a
kreslený	kreslený	k2eAgInSc4d1	kreslený
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
pomalu	pomalu	k6eAd1	pomalu
úspěšně	úspěšně	k6eAd1	úspěšně
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
zvukový	zvukový	k2eAgInSc1d1	zvukový
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
zpoždění	zpoždění	k1gNnSc4	zpoždění
způsobila	způsobit	k5eAaPmAgFnS	způsobit
tvorba	tvorba	k1gFnSc1	tvorba
vlastního	vlastní	k2eAgInSc2d1	vlastní
kinematografického	kinematografický	k2eAgInSc2d1	kinematografický
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
nemuseli	muset	k5eNaImAgMnP	muset
Sověti	Sovět	k1gMnPc1	Sovět
platit	platit	k5eAaImF	platit
poplatky	poplatek	k1gInPc4	poplatek
za	za	k7c4	za
materiál	materiál	k1gInSc4	materiál
z	z	k7c2	z
ciziny	cizina	k1gFnSc2	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
byly	být	k5eAaImAgInP	být
natáčeny	natáčen	k2eAgInPc1d1	natáčen
němé	němý	k2eAgInPc1d1	němý
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Kulička	kulička	k1gFnSc1	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgInSc7	první
filmem	film	k1gInSc7	film
natočeným	natočený	k2eAgInSc7d1	natočený
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
sovětský	sovětský	k2eAgInSc4d1	sovětský
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
a	a	k8xC	a
negativní	negativní	k2eAgInSc4d1	negativní
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
významnější	významný	k2eAgMnSc1d2	významnější
režisér	režisér	k1gMnSc1	režisér
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
vrátil	vrátit	k5eAaPmAgInS	vrátit
Vsevolod	Vsevolod	k1gInSc1	Vsevolod
Pudovkin	Pudovkin	k2eAgInSc1d1	Pudovkin
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
následoval	následovat	k5eAaImAgInS	následovat
Ejzenštejn	Ejzenštejn	k1gInSc1	Ejzenštejn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
původně	původně	k6eAd1	původně
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
žádného	žádný	k3yNgNnSc2	žádný
uskutečnění	uskutečnění	k1gNnSc2	uskutečnění
z	z	k7c2	z
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
postupně	postupně	k6eAd1	postupně
přicházel	přicházet	k5eAaImAgInS	přicházet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
záměrně	záměrně	k6eAd1	záměrně
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
nečinnost	nečinnost	k1gFnSc1	nečinnost
ho	on	k3xPp3gInSc4	on
donutila	donutit	k5eAaPmAgFnS	donutit
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
skončil	skončit	k5eAaPmAgInS	skončit
neúspěšně	úspěšně	k6eNd1	úspěšně
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
rozmach	rozmach	k1gInSc1	rozmach
sovětské	sovětský	k2eAgFnSc2d1	sovětská
školy	škola	k1gFnSc2	škola
</s>
<s>
přilákal	přilákat	k5eAaPmAgInS	přilákat
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
cizí	cizit	k5eAaImIp3nS	cizit
režiséry	režisér	k1gMnPc4	režisér
<g/>
:	:	kIx,	:
Hanse	Hans	k1gMnSc4	Hans
Richtera	Richter	k1gMnSc4	Richter
<g/>
,	,	kIx,	,
Bélu	Béla	k1gMnSc4	Béla
Balázse	Balázs	k1gMnSc4	Balázs
<g/>
,	,	kIx,	,
Jorise	Jorise	k1gFnSc1	Jorise
Ivense	Ivense	k1gFnSc1	Ivense
a	a	k8xC	a
známého	známý	k2eAgMnSc2d1	známý
divadelníka	divadelník	k1gMnSc2	divadelník
Erwina	Erwin	k1gMnSc2	Erwin
Piscatora	Piscator	k1gMnSc2	Piscator
<g/>
,	,	kIx,	,
vyhnaného	vyhnaný	k2eAgMnSc2d1	vyhnaný
nacisty	nacista	k1gMnSc2	nacista
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pětiletý	pětiletý	k2eAgInSc1d1	pětiletý
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
kulturních	kulturní	k2eAgInPc2d1	kulturní
klubů	klub	k1gInPc2	klub
v	v	k7c6	v
továrnách	továrna	k1gFnPc6	továrna
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
řízených	řízený	k2eAgInPc2d1	řízený
samotnými	samotný	k2eAgMnPc7d1	samotný
dělníky	dělník	k1gMnPc7	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
velkého	velký	k2eAgInSc2d1	velký
rozvoje	rozvoj	k1gInSc2	rozvoj
sovětská	sovětský	k2eAgFnSc1d1	sovětská
kinematografie	kinematografie	k1gFnSc1	kinematografie
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
filmem	film	k1gInSc7	film
Čapajev	Čapajev	k1gMnSc7	Čapajev
<g/>
,	,	kIx,	,
dílem	díl	k1gInSc7	díl
režisérské	režisérský	k2eAgFnSc2d1	režisérská
dvojice	dvojice	k1gFnSc2	dvojice
Sergeje	Sergej	k1gMnSc2	Sergej
a	a	k8xC	a
Georgije	Georgije	k1gMnSc2	Georgije
Vasiljevových	Vasiljevových	k2eAgMnSc2d1	Vasiljevových
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
především	především	k9	především
díky	díky	k7c3	díky
kolektivní	kolektivní	k2eAgFnSc3d1	kolektivní
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Herci	herec	k1gMnPc1	herec
studovali	studovat	k5eAaImAgMnP	studovat
své	svůj	k3xOyFgFnPc4	svůj
role	role	k1gFnPc4	role
s	s	k7c7	s
právě	právě	k6eAd1	právě
takovou	takový	k3xDgFnSc7	takový
svědomitostí	svědomitost	k1gFnSc7	svědomitost
jako	jako	k9	jako
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
žili	žít	k5eAaImAgMnP	žít
dlouho	dlouho	k6eAd1	dlouho
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tu	tu	k6eAd1	tu
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
bojových	bojový	k2eAgFnPc6d1	bojová
scénách	scéna	k1gFnPc6	scéna
<g/>
,	,	kIx,	,
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
okolnosti	okolnost	k1gFnPc1	okolnost
usnadnily	usnadnit	k5eAaPmAgFnP	usnadnit
jejich	jejich	k3xOp3gFnSc4	jejich
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
snímku	snímek	k1gInSc2	snímek
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
především	především	k9	především
zásluhou	zásluhou	k7c2	zásluhou
moderního	moderní	k2eAgMnSc2d1	moderní
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
žil	žít	k5eAaImAgInS	žít
vlastním	vlastní	k2eAgInSc7d1	vlastní
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
nevšední	všední	k2eNgInSc4d1	nevšední
věhlas	věhlas	k1gInSc4	věhlas
svou	svůj	k3xOyFgFnSc4	svůj
silnou	silný	k2eAgFnSc4d1	silná
a	a	k8xC	a
pravdivou	pravdivý	k2eAgFnSc7d1	pravdivá
lidskostí	lidskost	k1gFnSc7	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
úspěch	úspěch	k1gInSc1	úspěch
podnítil	podnítit	k5eAaPmAgInS	podnítit
k	k	k7c3	k
natáčení	natáčení	k1gNnSc3	natáčení
dalších	další	k2eAgFnPc2d1	další
historických	historický	k2eAgFnPc2d1	historická
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
hrdinských	hrdinský	k2eAgInPc6d1	hrdinský
činech	čin	k1gInPc6	čin
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Profesor	profesor	k1gMnSc1	profesor
Mamlock	Mamlock	k1gMnSc1	Mamlock
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
od	od	k7c2	od
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Rapoporta	Rapoport	k1gMnSc2	Rapoport
o	o	k7c6	o
životě	život	k1gInSc6	život
antifašisty	antifašista	k1gMnSc2	antifašista
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
sovětská	sovětský	k2eAgFnSc1d1	sovětská
kinematografie	kinematografie	k1gFnSc1	kinematografie
bohatá	bohatý	k2eAgFnSc1d1	bohatá
a	a	k8xC	a
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
ale	ale	k9	ale
málokdo	málokdo	k3yInSc1	málokdo
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k4c1	mnoho
západních	západní	k2eAgInPc2d1	západní
států	stát	k1gInPc2	stát
zavedlo	zavést	k5eAaPmAgNnS	zavést
cenzuru	cenzura	k1gFnSc4	cenzura
a	a	k8xC	a
zabránily	zabránit	k5eAaPmAgFnP	zabránit
tak	tak	k8xC	tak
přístupu	přístup	k1gInSc6	přístup
sovětských	sovětský	k2eAgInPc2d1	sovětský
filmů	film	k1gInPc2	film
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
trh	trh	k1gInSc4	trh
(	(	kIx(	(
<g/>
francouzská	francouzský	k2eAgFnSc1d1	francouzská
cenzura	cenzura	k1gFnSc1	cenzura
nepřipustila	připustit	k5eNaPmAgFnS	připustit
například	například	k6eAd1	například
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
ani	ani	k8xC	ani
jediný	jediný	k2eAgInSc1d1	jediný
sovětský	sovětský	k2eAgInSc1d1	sovětský
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zákazy	zákaz	k1gInPc1	zákaz
vznikaly	vznikat	k5eAaImAgInP	vznikat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
film	film	k1gInSc1	film
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
nových	nový	k2eAgInPc2d1	nový
vrcholů	vrchol	k1gInPc2	vrchol
metodou	metoda	k1gFnSc7	metoda
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
soutěžního	soutěžní	k2eAgMnSc2d1	soutěžní
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgInPc1d1	vyvolaný
pětiletými	pětiletý	k2eAgInPc7d1	pětiletý
plány	plán	k1gInPc7	plán
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
kinematografie	kinematografie	k1gFnSc1	kinematografie
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
natočených	natočený	k2eAgInPc2d1	natočený
filmů	film	k1gInPc2	film
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
okolo	okolo	k7c2	okolo
třiceti	třicet	k4xCc2	třicet
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
však	však	k9	však
přišla	přijít	k5eAaPmAgFnS	přijít
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
odhlasován	odhlasovat	k5eAaPmNgInS	odhlasovat
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Filmový	filmový	k2eAgInSc1d1	filmový
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
Cinematograph	Cinematograph	k1gInSc1	Cinematograph
Films	Filmsa	k1gFnPc2	Filmsa
Act	Act	k1gFnPc2	Act
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgNnSc2	jenž
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
kina	kino	k1gNnPc1	kino
nucena	nucen	k2eAgNnPc1d1	nuceno
dodržovat	dodržovat	k5eAaImF	dodržovat
pětiprocentní	pětiprocentní	k2eAgFnSc4d1	pětiprocentní
kvótu	kvóta	k1gFnSc4	kvóta
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
5	[number]	k4	5
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hrací	hrací	k2eAgFnSc2d1	hrací
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
vyhradit	vyhradit	k5eAaPmF	vyhradit
filmům	film	k1gInPc3	film
domácí	domácí	k2eAgFnSc2d1	domácí
výroby	výroba	k1gFnSc2	výroba
-	-	kIx~	-
a	a	k8xC	a
rozsah	rozsah	k1gInSc1	rozsah
domácí	domácí	k2eAgFnSc2d1	domácí
výroby	výroba	k1gFnSc2	výroba
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
minimálním	minimální	k2eAgInSc7d1	minimální
počtem	počet	k1gInSc7	počet
50	[number]	k4	50
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příštích	příští	k2eAgNnPc6d1	příští
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
kvóta	kvóta	k1gFnSc1	kvóta
postupně	postupně	k6eAd1	postupně
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
na	na	k7c4	na
20	[number]	k4	20
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
vedl	vést	k5eAaImAgInS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
průměr	průměr	k1gInSc1	průměr
rapidně	rapidně	k6eAd1	rapidně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
<g/>
,	,	kIx,	,
na	na	k7c6	na
150	[number]	k4	150
filmů	film	k1gInPc2	film
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc4	první
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
úspěch	úspěch	k1gInSc4	úspěch
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
Herbert	Herbert	k1gInSc1	Herbert
Wilcox	Wilcox	k1gInSc1	Wilcox
svým	svůj	k3xOyFgInSc7	svůj
filmem	film	k1gInSc7	film
Úsvit	úsvit	k1gInSc1	úsvit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
filmové	filmový	k2eAgFnSc6d1	filmová
scéně	scéna	k1gFnSc6	scéna
objevují	objevovat	k5eAaImIp3nP	objevovat
talentovaní	talentovaný	k2eAgMnPc1d1	talentovaný
režiséři	režisér	k1gMnPc1	režisér
Anthony	Anthona	k1gFnSc2	Anthona
Asquith	Asquith	k1gMnSc1	Asquith
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
anglický	anglický	k2eAgInSc1d1	anglický
zvukový	zvukový	k2eAgInSc1d1	zvukový
film	film	k1gInSc1	film
Její	její	k3xOp3gFnSc4	její
zpověď	zpověď	k1gFnSc4	zpověď
natočil	natočit	k5eAaBmAgMnS	natočit
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
jeho	jeho	k3xOp3gInSc4	jeho
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
dobře	dobře	k6eAd1	dobře
postavený	postavený	k2eAgInSc4d1	postavený
scénář	scénář	k1gInSc4	scénář
kriminálního	kriminální	k2eAgInSc2d1	kriminální
filmu	film	k1gInSc2	film
a	a	k8xC	a
technickou	technický	k2eAgFnSc4d1	technická
vynalézavost	vynalézavost	k1gFnSc4	vynalézavost
v	v	k7c6	v
působivých	působivý	k2eAgInPc6d1	působivý
záběrech	záběr	k1gInPc6	záběr
kamery	kamera	k1gFnSc2	kamera
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střihu	střih	k1gInSc6	střih
a	a	k8xC	a
ve	v	k7c6	v
využití	využití	k1gNnSc6	využití
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
filmový	filmový	k2eAgInSc1d1	filmový
úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
půdě	půda	k1gFnSc6	půda
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
snímek	snímek	k1gInSc1	snímek
Šest	šest	k4xCc1	šest
žen	žena	k1gFnPc2	žena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
režiséra	režisér	k1gMnSc2	režisér
Alexandera	Alexander	k1gMnSc2	Alexander
Kordy	Korda	k1gMnSc2	Korda
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
úspěchem	úspěch	k1gInSc7	úspěch
Lubitschova	Lubitschův	k2eAgInSc2d1	Lubitschův
berlínského	berlínský	k2eAgInSc2d1	berlínský
filmu	film	k1gInSc2	film
Anna	Anna	k1gFnSc1	Anna
Boleynová	Boleynová	k1gFnSc1	Boleynová
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
žen	žena	k1gFnPc2	žena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
anglickou	anglický	k2eAgFnSc4d1	anglická
kinematografii	kinematografie	k1gFnSc4	kinematografie
začátek	začátek	k1gInSc1	začátek
nového	nový	k2eAgInSc2d1	nový
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Korda	Korda	k1gMnSc1	Korda
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
filmu	film	k1gInSc3	film
stal	stát	k5eAaPmAgMnS	stát
významným	významný	k2eAgMnSc7d1	významný
anglickým	anglický	k2eAgMnSc7d1	anglický
režisérem	režisér	k1gMnSc7	režisér
a	a	k8xC	a
samostatným	samostatný	k2eAgMnSc7d1	samostatný
podnikatelem	podnikatel	k1gMnSc7	podnikatel
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
kupit	kupit	k5eAaImF	kupit
výpravné	výpravný	k2eAgInPc4d1	výpravný
velkofilmy	velkofilm	k1gInPc4	velkofilm
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
rázu	ráz	k1gInSc2	ráz
-	-	kIx~	-
Poslední	poslední	k2eAgFnPc1d1	poslední
lásky	láska	k1gFnPc1	láska
dona	don	k1gMnSc2	don
Juana	Juan	k1gMnSc2	Juan
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
;	;	kIx,	;
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
poslední	poslední	k2eAgInSc4d1	poslední
film	film	k1gInSc4	film
Douglase	Douglas	k1gInSc6	Douglas
Fairbankse	Fairbanksa	k1gFnSc3	Fairbanksa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Miláček	miláček	k1gMnSc1	miláček
slonů	slon	k1gMnPc2	slon
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
neměla	mít	k5eNaImAgFnS	mít
dostatek	dostatek	k1gInSc4	dostatek
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1	kvalifikovaný
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ve	v	k7c6	v
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
míře	míra	k1gFnSc6	míra
obracela	obracet	k5eAaImAgFnS	obracet
na	na	k7c4	na
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
režiséry	režisér	k1gMnPc4	režisér
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
francouzského	francouzský	k2eAgMnSc4d1	francouzský
Claira	Clair	k1gMnSc4	Clair
<g/>
,	,	kIx,	,
na	na	k7c4	na
německého	německý	k2eAgMnSc4d1	německý
Paula	Paul	k1gMnSc4	Paul
Czinnera	Czinner	k1gMnSc4	Czinner
a	a	k8xC	a
Bertholda	Berthold	k1gMnSc4	Berthold
Viertela	Viertel	k1gMnSc4	Viertel
nebo	nebo	k8xC	nebo
na	na	k7c4	na
režiséra	režisér	k1gMnSc4	režisér
prvního	první	k4xOgInSc2	první
českého	český	k2eAgInSc2d1	český
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
Když	když	k8xS	když
struny	struna	k1gFnSc2	struna
lkají	lkát	k5eAaImIp3nP	lkát
Friedricha	Friedrich	k1gMnSc4	Friedrich
Féhera	Féhera	k1gFnSc1	Féhera
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
John	John	k1gMnSc1	John
Grierson	Grierson	k1gMnSc1	Grierson
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
nashromáždil	nashromáždit	k5eAaPmAgMnS	nashromáždit
skupinu	skupina	k1gFnSc4	skupina
mladých	mladý	k2eAgMnPc2d1	mladý
dokumentaristů	dokumentarista	k1gMnPc2	dokumentarista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
přiblížit	přiblížit	k5eAaPmF	přiblížit
rozličné	rozličný	k2eAgFnSc2d1	rozličná
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
především	především	k9	především
ale	ale	k8xC	ale
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
prostých	prostý	k2eAgMnPc2d1	prostý
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
rybářů	rybář	k1gMnPc2	rybář
<g/>
,	,	kIx,	,
horníků	horník	k1gMnPc2	horník
a	a	k8xC	a
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
bydlením	bydlení	k1gNnSc7	bydlení
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Loděnice	loděnice	k1gFnSc1	loděnice
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
i	i	k9	i
na	na	k7c4	na
sociální	sociální	k2eAgInPc4d1	sociální
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
režisérem	režisér	k1gMnSc7	režisér
v	v	k7c6	v
předválečném	předválečný	k2eAgNnSc6d1	předválečné
období	období	k1gNnSc6	období
Anglie	Anglie	k1gFnSc2	Anglie
byl	být	k5eAaImAgMnS	být
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
dílem	díl	k1gInSc7	díl
bylo	být	k5eAaImAgNnS	být
Třicet	třicet	k4xCc1	třicet
devět	devět	k4xCc1	devět
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
kriminální	kriminální	k2eAgInSc1d1	kriminální
román	román	k1gInSc1	román
z	z	k7c2	z
prostřední	prostřední	k2eAgFnSc2d1	prostřední
realistické	realistický	k2eAgFnSc2d1	realistická
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
a	a	k8xC	a
talent	talent	k1gInSc1	talent
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mladí	mladý	k2eAgMnPc1d1	mladý
britští	britský	k2eAgMnPc1d1	britský
režiséři	režisér	k1gMnPc1	režisér
se	se	k3xPyFc4	se
zmítali	zmítat	k5eAaImAgMnP	zmítat
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
protilehlými	protilehlý	k2eAgInPc7d1	protilehlý
póly	pól	k1gInPc7	pól
<g/>
:	:	kIx,	:
dokumentarismem	dokumentarismus	k1gInSc7	dokumentarismus
a	a	k8xC	a
"	"	kIx"	"
<g/>
kordovským	kordovský	k2eAgInSc7d1	kordovský
<g/>
"	"	kIx"	"
slohem	sloh	k1gInSc7	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Dokumentaristé	dokumentarista	k1gMnPc1	dokumentarista
totiž	totiž	k9	totiž
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
sovětského	sovětský	k2eAgInSc2d1	sovětský
revolučního	revoluční	k2eAgInSc2d1	revoluční
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
rytmické	rytmický	k2eAgFnSc2d1	rytmická
montážní	montážní	k2eAgFnSc2d1	montážní
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
převzali	převzít	k5eAaPmAgMnP	převzít
metody	metoda	k1gFnPc4	metoda
inscenace	inscenace	k1gFnSc2	inscenace
z	z	k7c2	z
hraného	hraný	k2eAgInSc2d1	hraný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
kreativní	kreativní	k2eAgMnPc1d1	kreativní
se	se	k3xPyFc4	se
ukázali	ukázat	k5eAaPmAgMnP	ukázat
být	být	k5eAaImF	být
v	v	k7c6	v
kombinovaném	kombinovaný	k2eAgNnSc6d1	kombinované
nasazení	nasazení	k1gNnSc6	nasazení
ruchů	ruch	k1gInPc2	ruch
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
mluveného	mluvený	k2eAgInSc2d1	mluvený
komentáře	komentář	k1gInSc2	komentář
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
byl	být	k5eAaImAgInS	být
britský	britský	k2eAgInSc1d1	britský
film	film	k1gInSc1	film
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zahájení	zahájení	k1gNnSc1	zahájení
nepřátelství	nepřátelství	k1gNnPc2	nepřátelství
ještě	ještě	k9	ještě
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
padesát	padesát	k4xCc4	padesát
filmů	film	k1gInPc2	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
éra	éra	k1gFnSc1	éra
klasického	klasický	k2eAgInSc2d1	klasický
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikaly	vznikat	k5eAaImAgFnP	vznikat
četné	četný	k2eAgInPc4d1	četný
nové	nový	k2eAgInPc4d1	nový
žánry	žánr	k1gInPc4	žánr
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
"	"	kIx"	"
<g/>
továrnou	továrna	k1gFnSc7	továrna
na	na	k7c4	na
sny	sen	k1gInPc4	sen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
napínavé	napínavý	k2eAgInPc1d1	napínavý
či	či	k8xC	či
veselé	veselý	k2eAgInPc1d1	veselý
filmy	film	k1gInPc1	film
dovolovaly	dovolovat	k5eAaImAgInP	dovolovat
lidem	člověk	k1gMnPc3	člověk
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
únik	únik	k1gInSc4	únik
z	z	k7c2	z
deprimující	deprimující	k2eAgFnSc2d1	deprimující
reality	realita	k1gFnSc2	realita
jejich	jejich	k3xOp3gInSc2	jejich
všedního	všední	k2eAgInSc2d1	všední
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
žánry	žánr	k1gInPc1	žánr
reprodukovaly	reprodukovat	k5eAaBmAgInP	reprodukovat
mýtus	mýtus	k1gInSc4	mýtus
o	o	k7c6	o
zemi	zem	k1gFnSc6	zem
neomezených	omezený	k2eNgFnPc2d1	neomezená
možností	možnost	k1gFnPc2	možnost
a	a	k8xC	a
přísahaly	přísahat	k5eAaImAgFnP	přísahat
v	v	k7c6	v
nekonečných	konečný	k2eNgFnPc6d1	nekonečná
variantách	varianta	k1gFnPc6	varianta
na	na	k7c4	na
"	"	kIx"	"
<g/>
americký	americký	k2eAgInSc4d1	americký
sen	sen	k1gInSc4	sen
<g/>
"	"	kIx"	"
velké	velký	k2eAgFnSc2d1	velká
kariéry	kariéra	k1gFnSc2	kariéra
"	"	kIx"	"
<g/>
od	od	k7c2	od
umývače	umývač	k1gInSc2	umývač
talířů	talíř	k1gInPc2	talíř
k	k	k7c3	k
milionáři	milionář	k1gMnPc7	milionář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1930	[number]	k4	1930
začal	začít	k5eAaPmAgInS	začít
promítat	promítat	k5eAaImF	promítat
Milestonův	Milestonův	k2eAgInSc1d1	Milestonův
protiválečný	protiválečný	k2eAgInSc1d1	protiválečný
film	film	k1gInSc1	film
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
premiéru	premiéra	k1gFnSc4	premiéra
další	další	k2eAgInSc4d1	další
film	film	k1gInSc4	film
o	o	k7c6	o
hrůzách	hrůza	k1gFnPc6	hrůza
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc1d1	západní
fronta	fronta	k1gFnSc1	fronta
1918	[number]	k4	1918
Georga	Georga	k1gFnSc1	Georga
Wilhelma	Wilhelma	k1gFnSc1	Wilhelma
Pabsta	Pabsta	k1gFnSc1	Pabsta
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
filmy	film	k1gInPc1	film
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
každodenní	každodenní	k2eAgNnSc4d1	každodenní
umírání	umírání	k1gNnSc4	umírání
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
v	v	k7c6	v
letech	let	k1gInPc6	let
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Odmítání	odmítání	k1gNnSc1	odmítání
"	"	kIx"	"
<g/>
hurávlastenectví	hurávlastenectví	k1gNnSc1	hurávlastenectví
<g/>
"	"	kIx"	"
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
odpor	odpor	k1gInSc4	odpor
pravicových	pravicový	k2eAgInPc2d1	pravicový
kruhů	kruh	k1gInPc2	kruh
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
frontě	fronta	k1gFnSc6	fronta
klid	klid	k1gInSc1	klid
dokonce	dokonce	k9	dokonce
zakázán	zakázán	k2eAgInSc1d1	zakázán
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
gangstera	gangster	k1gMnSc2	gangster
od	od	k7c2	od
drobného	drobný	k2eAgMnSc2d1	drobný
vykrádače	vykrádač	k1gMnSc2	vykrádač
benzinových	benzinový	k2eAgFnPc2d1	benzinová
pump	pumpa	k1gFnPc2	pumpa
ke	k	k7c3	k
králi	král	k1gMnPc7	král
podsvětí	podsvětí	k1gNnSc1	podsvětí
bylo	být	k5eAaImAgNnS	být
námětem	námět	k1gInSc7	námět
filmu	film	k1gInSc2	film
Malý	Malý	k1gMnSc1	Malý
Caesar	Caesar	k1gMnSc1	Caesar
(	(	kIx(	(
<g/>
Little	Little	k1gFnSc1	Little
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
poprvé	poprvé	k6eAd1	poprvé
ukázal	ukázat	k5eAaPmAgInS	ukázat
Hollywood	Hollywood	k1gInSc1	Hollywood
otevřeně	otevřeně	k6eAd1	otevřeně
realitu	realita	k1gFnSc4	realita
zločinu	zločin	k1gInSc2	zločin
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Odkláněl	odklánět	k5eAaImAgInS	odklánět
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
od	od	k7c2	od
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
kriminálních	kriminální	k2eAgInPc2d1	kriminální
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byl	být	k5eAaImAgInS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
pachatel	pachatel	k1gMnSc1	pachatel
nebo	nebo	k8xC	nebo
detektiv	detektiv	k1gMnSc1	detektiv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
obrovské	obrovský	k2eAgFnSc2d1	obrovská
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Malý	Malý	k1gMnSc1	Malý
Caesar	Caesar	k1gMnSc1	Caesar
obraz	obraz	k1gInSc4	obraz
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
dřívější	dřívější	k2eAgFnSc7d1	dřívější
vírou	víra	k1gFnSc7	víra
v	v	k7c4	v
pokrok	pokrok	k1gInSc4	pokrok
-	-	kIx~	-
k	k	k7c3	k
blahobytu	blahobyt	k1gInSc3	blahobyt
nevede	vést	k5eNaImIp3nS	vést
poctivost	poctivost	k1gFnSc1	poctivost
a	a	k8xC	a
zdatnost	zdatnost	k1gFnSc1	zdatnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dravý	dravý	k2eAgInSc1d1	dravý
egoismus	egoismus	k1gInSc1	egoismus
a	a	k8xC	a
brutalita	brutalita	k1gFnSc1	brutalita
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
průměrný	průměrný	k2eAgMnSc1d1	průměrný
Američan	Američan	k1gMnSc1	Američan
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
většinou	většinou	k6eAd1	většinou
vítězí	vítězit	k5eAaImIp3nS	vítězit
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
bezohlednost	bezohlednost	k1gFnSc1	bezohlednost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgInPc6	tento
obdobných	obdobný	k2eAgInPc6d1	obdobný
filmech	film	k1gInPc6	film
je	být	k5eAaImIp3nS	být
zločin	zločin	k1gInSc1	zločin
interpretován	interpretován	k2eAgInSc1d1	interpretován
jako	jako	k8xS	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
naprosté	naprostý	k2eAgNnSc4d1	naprosté
selhání	selhání	k1gNnSc4	selhání
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
ospravedlňován	ospravedlňován	k2eAgInSc1d1	ospravedlňován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
scéně	scéna	k1gFnSc6	scéna
objevují	objevovat	k5eAaImIp3nP	objevovat
populární	populární	k2eAgInSc4d1	populární
Stan	stan	k1gInSc4	stan
Laurel	Laurel	k1gInSc1	Laurel
(	(	kIx(	(
<g/>
vl	vl	k?	vl
<g/>
.	.	kIx.	.
jménem	jméno	k1gNnSc7	jméno
Artur	Artur	k1gMnSc1	Artur
Stanley	Stanlea	k1gFnSc2	Stanlea
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oliver	Oliver	k1gInSc4	Oliver
Hardy	Harda	k1gFnSc2	Harda
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
vzorovou	vzorový	k2eAgFnSc7d1	vzorová
komickou	komický	k2eAgFnSc7d1	komická
dvojicí	dvojice	k1gFnSc7	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
naprosto	naprosto	k6eAd1	naprosto
protikladné	protikladný	k2eAgFnSc3d1	protikladná
osobnosti	osobnost	k1gFnSc3	osobnost
už	už	k6eAd1	už
kontrast	kontrast	k1gInSc4	kontrast
jejich	jejich	k3xOp3gInSc2	jejich
zevnějšku	zevnějšek	k1gInSc2	zevnějšek
budí	budit	k5eAaImIp3nS	budit
smích	smích	k1gInSc1	smích
<g/>
.	.	kIx.	.
</s>
<s>
Laurel	Laurel	k1gMnSc1	Laurel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
komiku	komika	k1gFnSc4	komika
Freda	Fred	k1gMnSc2	Fred
Karna	Karn	k1gMnSc2	Karn
do	do	k7c2	do
USA	USA	kA	USA
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
Charliem	Charlie	k1gMnSc7	Charlie
Chaplinem	Chaplin	k1gInSc7	Chaplin
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
drobný	drobný	k2eAgMnSc1d1	drobný
<g/>
,	,	kIx,	,
nesmělý	smělý	k2eNgMnSc1d1	nesmělý
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpláče	rozplakat	k5eAaPmIp3nS	rozplakat
a	a	k8xC	a
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
dítě	dítě	k1gNnSc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Rozvážný	rozvážný	k2eAgInSc4d1	rozvážný
Hardy	Hard	k1gInPc4	Hard
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
zdánlivě	zdánlivě	k6eAd1	zdánlivě
suverénní	suverénní	k2eAgFnSc1d1	suverénní
<g/>
,	,	kIx,	,
toleruje	tolerovat	k5eAaImIp3nS	tolerovat
však	však	k9	však
vlastnosti	vlastnost	k1gFnPc4	vlastnost
svého	svůj	k3xOyFgMnSc2	svůj
parťáka	parťák	k1gMnSc2	parťák
a	a	k8xC	a
bezvýchodné	bezvýchodný	k2eAgFnSc2d1	bezvýchodná
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Stan	stan	k1gInSc1	stan
už	už	k6eAd1	už
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
pláče	plakat	k5eAaImIp3nS	plakat
a	a	k8xC	a
kvituje	kvitovat	k5eAaBmIp3nS	kvitovat
rezignovaným	rezignovaný	k2eAgNnSc7d1	rezignované
pokrčením	pokrčení	k1gNnSc7	pokrčení
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
dokonale	dokonale	k6eAd1	dokonale
přizpůsobila	přizpůsobit	k5eAaPmAgFnS	přizpůsobit
zvukovému	zvukový	k2eAgInSc3d1	zvukový
filmu	film	k1gInSc3	film
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc4	jejich
pisklavý	pisklavý	k2eAgInSc4d1	pisklavý
hlas	hlas	k1gInSc4	hlas
dodal	dodat	k5eAaPmAgMnS	dodat
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
komičnosti	komičnost	k1gFnSc2	komičnost
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
Hal	hala	k1gFnPc2	hala
Roach	Roach	k1gMnSc1	Roach
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
jenom	jenom	k6eAd1	jenom
průměrně	průměrně	k6eAd1	průměrně
úspěšně	úspěšně	k6eAd1	úspěšně
herce	herc	k1gInSc2	herc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
bezpočet	bezpočet	k1gInSc4	bezpočet
krátkých	krátký	k2eAgInPc2d1	krátký
i	i	k8xC	i
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
velmi	velmi	k6eAd1	velmi
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
pohádek	pohádka	k1gFnPc2	pohádka
bratří	bratr	k1gMnPc2	bratr
Grimmů	Grimm	k1gMnPc2	Grimm
první	první	k4xOgMnSc1	první
kreslený	kreslený	k2eAgInSc1d1	kreslený
film	film	k1gInSc1	film
Walta	Walt	k1gMnSc2	Walt
Disneye	Disney	k1gMnSc2	Disney
<g/>
,	,	kIx,	,
populární	populární	k2eAgFnSc1d1	populární
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
předstihly	předstihnout	k5eAaPmAgFnP	předstihnout
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vloženo	vložit	k5eAaPmNgNnS	vložit
do	do	k7c2	do
sektoru	sektor	k1gInSc2	sektor
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
proslulém	proslulý	k2eAgInSc6d1	proslulý
filmu	film	k1gInSc6	film
trvala	trvat	k5eAaImAgFnS	trvat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
namísto	namísto	k7c2	namísto
předpokládaných	předpokládaný	k2eAgInPc2d1	předpokládaný
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
výdaje	výdaj	k1gInPc1	výdaj
vzrostly	vzrůst	k5eAaPmAgInP	vzrůst
až	až	k9	až
šestinásobně	šestinásobně	k6eAd1	šestinásobně
-	-	kIx~	-
z	z	k7c2	z
250	[number]	k4	250
000	[number]	k4	000
na	na	k7c4	na
1,5	[number]	k4	1,5
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Štáb	štáb	k1gInSc1	štáb
570	[number]	k4	570
kreslířů	kreslíř	k1gMnPc2	kreslíř
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
podle	podle	k7c2	podle
přesných	přesný	k2eAgFnPc2d1	přesná
předloh	předloha	k1gFnPc2	předloha
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milión	milión	k4xCgInSc4	milión
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
obrázků	obrázek	k1gInPc2	obrázek
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
pasáže	pasáž	k1gFnPc1	pasáž
-	-	kIx~	-
jako	jako	k8xS	jako
například	například	k6eAd1	například
cesta	cesta	k1gFnSc1	cesta
sedmi	sedm	k4xCc2	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInSc1d1	trvající
ve	v	k7c6	v
filmu	film	k1gInSc6	film
pouhou	pouhý	k2eAgFnSc4d1	pouhá
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
pět	pět	k4xCc4	pět
kreslířů	kreslíř	k1gMnPc2	kreslíř
celého	celý	k2eAgInSc2d1	celý
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
výdaje	výdaj	k1gInPc1	výdaj
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
trpaslík	trpaslík	k1gMnSc1	trpaslík
musel	muset	k5eAaImAgMnS	muset
mít	mít	k5eAaImF	mít
své	svůj	k3xOyFgInPc4	svůj
nezaměnitelné	zaměnitelný	k2eNgInPc4d1	nezaměnitelný
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
divák	divák	k1gMnSc1	divák
nemohl	moct	k5eNaImAgMnS	moct
splést	splést	k5eAaPmF	splést
třeba	třeba	k9	třeba
Prófu	prófa	k1gMnSc4	prófa
se	s	k7c7	s
Stydlínem	Stydlín	k1gInSc7	Stydlín
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Sněhurce	Sněhurka	k1gFnSc6	Sněhurka
se	se	k3xPyFc4	se
zúročily	zúročit	k5eAaPmAgFnP	zúročit
bohaté	bohatý	k2eAgFnPc1d1	bohatá
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
získával	získávat	k5eAaImAgMnS	získávat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
svými	svůj	k3xOyFgInPc7	svůj
sedmiminutovými	sedmiminutový	k2eAgInPc7d1	sedmiminutový
krátkometrážními	krátkometrážní	k2eAgInPc7d1	krátkometrážní
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kasovní	kasovní	k2eAgInSc1d1	kasovní
trhák	trhák	k1gInSc1	trhák
vynesl	vynést	k5eAaPmAgInS	vynést
hodně	hodně	k6eAd1	hodně
přes	přes	k7c4	přes
8	[number]	k4	8
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
v	v	k7c6	v
Mánesu	Mánes	k1gMnSc6	Mánes
sešlo	sejít	k5eAaPmAgNnS	sejít
asi	asi	k9	asi
padesát	padesát	k4xCc1	padesát
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
herců	herec	k1gMnPc2	herec
a	a	k8xC	a
filmových	filmový	k2eAgMnPc2d1	filmový
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaBmAgMnP	shodnout
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
postupu	postup	k1gInSc6	postup
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc1d1	značný
počet	počet	k1gInSc1	počet
spisovatelů	spisovatel	k1gMnPc2	spisovatel
zasával	zasávat	k5eAaPmAgInS	zasávat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
českého	český	k2eAgInSc2d1	český
filmu	film	k1gInSc2	film
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
užitečné	užitečný	k2eAgNnSc1d1	užitečné
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
nacisté	nacista	k1gMnPc1	nacista
zlikvidovali	zlikvidovat	k5eAaPmAgMnP	zlikvidovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
porážce	porážka	k1gFnSc6	porážka
mohlo	moct	k5eAaImAgNnS	moct
začít	začít	k5eAaPmF	začít
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Stanovisko	stanovisko	k1gNnSc1	stanovisko
filmařů	filmař	k1gMnPc2	filmař
a	a	k8xC	a
části	část	k1gFnSc2	část
herců	herc	k1gInPc2	herc
bylo	být	k5eAaImAgNnS	být
odlišné	odlišný	k2eAgNnSc1d1	odlišné
<g/>
,	,	kIx,	,
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nacistického	nacistický	k2eAgInSc2d1	nacistický
teroru	teror	k1gInSc2	teror
je	být	k5eAaImIp3nS	být
každé	každý	k3xTgNnSc1	každý
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zazní	zaznět	k5eAaImIp3nS	zaznět
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
z	z	k7c2	z
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
nedocenitelnou	docenitelný	k2eNgFnSc7d1	nedocenitelná
oporou	opora	k1gFnSc7	opora
pro	pro	k7c4	pro
morálku	morálka	k1gFnSc4	morálka
a	a	k8xC	a
psychiku	psychika	k1gFnSc4	psychika
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgInP	být
dohodnuty	dohodnut	k2eAgInPc1d1	dohodnut
tři	tři	k4xCgInPc1	tři
principy	princip	k1gInPc1	princip
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
měla	mít	k5eAaImAgFnS	mít
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
tvorba	tvorba	k1gFnSc1	tvorba
stát	stát	k1gInSc1	stát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nepřipustit	připustit	k5eNaPmF	připustit
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
ústupek	ústupek	k1gInSc1	ústupek
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
kolaboraci	kolaborace	k1gFnSc3	kolaborace
s	s	k7c7	s
okupanty	okupant	k1gMnPc7	okupant
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
volit	volit	k5eAaImF	volit
jen	jen	k9	jen
velice	velice	k6eAd1	velice
opatrně	opatrně	k6eAd1	opatrně
současnou	současný	k2eAgFnSc4d1	současná
tematiku	tematika	k1gFnSc4	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Podporovat	podporovat	k5eAaImF	podporovat
u	u	k7c2	u
diváka	divák	k1gMnSc2	divák
národní	národní	k2eAgNnSc4d1	národní
vědomí	vědomí	k1gNnSc4	vědomí
a	a	k8xC	a
vůli	vůle	k1gFnSc4	vůle
k	k	k7c3	k
rezistenci	rezistence	k1gFnSc4	rezistence
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
vrátíme	vrátit	k5eAaPmIp1nP	vrátit
k	k	k7c3	k
minulosti	minulost	k1gFnSc3	minulost
-	-	kIx~	-
ke	k	k7c3	k
klasickým	klasický	k2eAgMnPc3d1	klasický
autorům	autor	k1gMnPc3	autor
<g/>
,	,	kIx,	,
k	k	k7c3	k
době	doba	k1gFnSc3	doba
národnostních	národnostní	k2eAgInPc2d1	národnostní
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
třetice	třetice	k1gFnSc2	třetice
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
usilovat	usilovat	k5eAaImF	usilovat
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
o	o	k7c4	o
postupné	postupný	k2eAgNnSc4d1	postupné
zvyšování	zvyšování	k1gNnSc4	zvyšování
celkové	celkový	k2eAgFnSc2d1	celková
kvality	kvalita	k1gFnSc2	kvalita
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
zlikvidovány	zlikvidovat	k5eAaPmNgFnP	zlikvidovat
všechny	všechen	k3xTgFnPc1	všechen
české	český	k2eAgFnPc1d1	Česká
výrobní	výrobní	k2eAgFnPc1d1	výrobní
firmy	firma	k1gFnPc1	firma
a	a	k8xC	a
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
produkční	produkční	k2eAgFnPc1d1	produkční
společnosti	společnost	k1gFnPc1	společnost
-	-	kIx~	-
Lucernafilm	Lucernafilm	k1gInSc1	Lucernafilm
a	a	k8xC	a
Nationalfilm	Nationalfilm	k1gInSc1	Nationalfilm
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
půjčovny	půjčovna	k1gFnPc1	půjčovna
byly	být	k5eAaImAgFnP	být
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
do	do	k7c2	do
monopolního	monopolní	k2eAgInSc2d1	monopolní
podniku	podnik	k1gInSc2	podnik
Kosmos	kosmos	k1gInSc1	kosmos
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tak	tak	k6eAd1	tak
měl	mít	k5eAaImAgInS	mít
vedle	vedle	k7c2	vedle
Lucernafilmu	Lucernafilm	k1gInSc2	Lucernafilm
a	a	k8xC	a
Nationalfilmu	Nationalfilm	k1gInSc3	Nationalfilm
jediný	jediný	k2eAgMnSc1d1	jediný
právo	právo	k1gNnSc4	právo
k	k	k7c3	k
distribuci	distribuce	k1gFnSc3	distribuce
českých	český	k2eAgInPc2d1	český
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1	filmová
organizace	organizace	k1gFnPc1	organizace
se	se	k3xPyFc4	se
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
okupace	okupace	k1gFnSc2	okupace
sdružily	sdružit	k5eAaPmAgInP	sdružit
do	do	k7c2	do
Ústředí	ústředí	k1gNnSc2	ústředí
filmového	filmový	k2eAgInSc2d1	filmový
oboru	obor	k1gInSc2	obor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgMnSc4	jenž
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Filmové	filmový	k2eAgNnSc1d1	filmové
ústředí	ústředí	k1gNnSc1	ústředí
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ovšem	ovšem	k9	ovšem
neměli	mít	k5eNaImAgMnP	mít
Němci	Němec	k1gMnPc1	Němec
zastoupení	zastoupení	k1gNnSc2	zastoupení
a	a	k8xC	a
organizace	organizace	k1gFnSc1	organizace
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
hájit	hájit	k5eAaImF	hájit
autonomii	autonomie	k1gFnSc4	autonomie
české	český	k2eAgFnSc2d1	Česká
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zřídil	zřídit	k5eAaPmAgMnS	zřídit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
říšský	říšský	k2eAgInSc4d1	říšský
protektor	protektor	k1gInSc4	protektor
Českomoravské	českomoravský	k2eAgNnSc1d1	Českomoravské
filmové	filmový	k2eAgNnSc1d1	filmové
ústředí	ústředí	k1gNnSc1	ústředí
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ČMFÚ	ČMFÚ	kA	ČMFÚ
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obchodu	obchod	k1gInSc2	obchod
převedena	převést	k5eAaPmNgFnS	převést
veškerá	veškerý	k3xTgFnSc1	veškerý
kompetence	kompetence	k1gFnSc1	kompetence
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
filmy	film	k1gInPc4	film
Hej	hej	k6eAd1	hej
rup	rupět	k5eAaImRp2nS	rupět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
patří	patřit	k5eAaImIp3nS	patřit
nám	my	k3xPp1nPc3	my
<g/>
,	,	kIx,	,
Třetí	třetí	k4xOgFnSc1	třetí
rota	rota	k1gFnSc1	rota
<g/>
,	,	kIx,	,
M.	M.	kA	M.
R.	R.	kA	R.
Štefánik	Štefánik	k1gMnSc1	Štefánik
<g/>
,	,	kIx,	,
Poručík	poručík	k1gMnSc1	poručík
Alexandr	Alexandr	k1gMnSc1	Alexandr
Rjepkin	Rjepkin	k1gMnSc1	Rjepkin
<g/>
,	,	kIx,	,
Jízdní	jízdní	k2eAgFnSc1d1	jízdní
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
Neporažená	poražený	k2eNgFnSc1d1	neporažená
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
C.	C.	kA	C.
a	a	k8xC	a
k.	k.	k?	k.
polní	polní	k2eAgMnSc1d1	polní
maršálek	maršálek	k1gMnSc1	maršálek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939-1944	[number]	k4	1939-1944
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
113	[number]	k4	113
českých	český	k2eAgInPc2d1	český
hraných	hraný	k2eAgInPc2d1	hraný
filmů	film	k1gInPc2	film
předválečného	předválečný	k2eAgNnSc2d1	předválečné
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nějakým	nějaký	k3yIgInPc3	nějaký
způsobem	způsob	k1gInSc7	způsob
mohly	moct	k5eAaImAgInP	moct
podporovat	podporovat	k5eAaImF	podporovat
národní	národní	k2eAgNnSc4d1	národní
vědomí	vědomí	k1gNnSc4	vědomí
či	či	k8xC	či
obhajovat	obhajovat	k5eAaImF	obhajovat
ideu	idea	k1gFnSc4	idea
české	český	k2eAgFnSc2d1	Česká
nebo	nebo	k8xC	nebo
československé	československý	k2eAgFnSc2d1	Československá
státnosti	státnost	k1gFnSc2	státnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1941	[number]	k4	1941
byla	být	k5eAaImAgFnS	být
cenzura	cenzura	k1gFnSc1	cenzura
ještě	ještě	k6eAd1	ještě
zpřísněna	zpřísněn	k2eAgFnSc1d1	zpřísněna
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
pak	pak	k6eAd1	pak
filmová	filmový	k2eAgFnSc1d1	filmová
cenzura	cenzura	k1gFnSc1	cenzura
přešla	přejít	k5eAaPmAgFnS	přejít
z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
gestapa	gestapo	k1gNnSc2	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
kinematografie	kinematografie	k1gFnSc1	kinematografie
projevila	projevit	k5eAaPmAgFnS	projevit
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
velkou	velký	k2eAgFnSc4d1	velká
životnost	životnost	k1gFnSc1	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vyniknout	vyniknout	k5eAaPmF	vyniknout
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc4d1	schopen
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
uznání	uznání	k1gNnSc2	uznání
(	(	kIx(	(
<g/>
a	a	k8xC	a
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Gustav	Gustav	k1gMnSc1	Gustav
Machatý	Machatý	k1gMnSc1	Machatý
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc1	film
Erotikon	Erotikona	k1gFnPc2	Erotikona
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
a	a	k8xC	a
Extáze	extáze	k1gFnPc1	extáze
jsou	být	k5eAaImIp3nP	být
nepříliš	příliš	k6eNd1	příliš
komplikované	komplikovaný	k2eAgInPc4d1	komplikovaný
příběhy	příběh	k1gInPc4	příběh
manželské	manželský	k2eAgFnSc2d1	manželská
nevěry	nevěra	k1gFnSc2	nevěra
zpracovány	zpracován	k2eAgFnPc4d1	zpracována
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
atmosféry	atmosféra	k1gFnSc2	atmosféra
místa	místo	k1gNnSc2	místo
děje	dít	k5eAaImIp3nS	dít
-	-	kIx~	-
zejména	zejména	k9	zejména
ve	v	k7c6	v
scénách	scéna	k1gFnPc6	scéna
z	z	k7c2	z
nádražního	nádražní	k2eAgNnSc2d1	nádražní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
Erotikonu	Erotikon	k1gInSc6	Erotikon
a	a	k8xC	a
ve	v	k7c6	v
scénách	scéna	k1gFnPc6	scéna
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
v	v	k7c6	v
Extázi	extáze	k1gFnSc6	extáze
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Extáze	extáze	k1gFnSc2	extáze
byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
také	také	k9	také
především	především	k9	především
malým	malý	k2eAgInSc7d1	malý
počtem	počet	k1gInSc7	počet
dialogů	dialog	k1gInPc2	dialog
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
době	doba	k1gFnSc6	doba
teprve	teprve	k6eAd1	teprve
se	se	k3xPyFc4	se
rozvíjejícího	rozvíjející	k2eAgInSc2d1	rozvíjející
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
sklidil	sklidit	k5eAaPmAgInS	sklidit
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
ohlas	ohlas	k1gInSc1	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Rovenský	Rovenský	k2eAgMnSc1d1	Rovenský
patřil	patřit	k5eAaImAgInS	patřit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mezi	mezi	k7c4	mezi
nejtalentovanější	talentovaný	k2eAgMnPc4d3	nejtalentovanější
české	český	k2eAgMnPc4d1	český
filmové	filmový	k2eAgMnPc4d1	filmový
tvůrce	tvůrce	k1gMnPc4	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
nejznámější	známý	k2eAgInPc1d3	nejznámější
snímky	snímek	k1gInPc1	snímek
patří	patřit	k5eAaImIp3nP	patřit
dramatický	dramatický	k2eAgInSc4d1	dramatický
příběh	příběh	k1gInSc4	příběh
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
dvou	dva	k4xCgFnPc2	dva
dospívajících	dospívající	k2eAgFnPc2d1	dospívající
dětí	dítě	k1gFnPc2	dítě
Řeka	Řek	k1gMnSc2	Řek
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmové	filmový	k2eAgNnSc1d1	filmové
přepracování	přepracování	k1gNnSc1	přepracování
dramatu	drama	k1gNnSc2	drama
bratří	bratřit	k5eAaImIp3nS	bratřit
Mrštíků	Mrštík	k1gMnPc2	Mrštík
Maryša	Maryša	k1gFnSc1	Maryša
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Hlídač	hlídač	k1gInSc1	hlídač
č.	č.	k?	č.
47	[number]	k4	47
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Josefa	Josefa	k1gFnSc1	Josefa
Kopty	kopt	k1gInPc4	kopt
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
pak	pak	k6eAd1	pak
připravil	připravit	k5eAaPmAgInS	připravit
i	i	k9	i
filmový	filmový	k2eAgInSc1d1	filmový
přepis	přepis	k1gInSc1	přepis
románu	román	k1gInSc2	román
Marie	Maria	k1gFnSc2	Maria
Majerové	Majerové	k2eAgNnSc2d1	Majerové
Panenství	panenství	k1gNnSc2	panenství
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
dílo	dílo	k1gNnSc4	dílo
stačil	stačit	k5eAaBmAgMnS	stačit
natočit	natočit	k5eAaBmF	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ujal	ujmout	k5eAaPmAgMnS	ujmout
začínající	začínající	k2eAgMnSc1d1	začínající
režisér	režisér	k1gMnSc1	režisér
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
do	do	k7c2	do
podvědomí	podvědomí	k1gNnSc2	podvědomí
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
zapsal	zapsat	k5eAaPmAgMnS	zapsat
také	také	k9	také
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejplodnějších	plodný	k2eAgMnPc2d3	nejplodnější
českých	český	k2eAgMnPc2d1	český
režisérů	režisér	k1gMnPc2	režisér
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
film	film	k1gInSc1	film
Jánošík	Jánošík	k1gMnSc1	Jánošík
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úspěch	úspěch	k1gInSc1	úspěch
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
se	se	k3xPyFc4	se
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
především	především	k9	především
na	na	k7c4	na
veseloherní	veseloherní	k2eAgInSc4d1	veseloherní
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
spjato	spjat	k2eAgNnSc1d1	spjato
i	i	k9	i
s	s	k7c7	s
adaptacemi	adaptace	k1gFnPc7	adaptace
vážně	vážně	k6eAd1	vážně
zaměřených	zaměřený	k2eAgNnPc2d1	zaměřené
děl	dělo	k1gNnPc2	dělo
literárních	literární	k2eAgInPc2d1	literární
(	(	kIx(	(
<g/>
Hordubalové	Hordubal	k1gMnPc5	Hordubal
<g/>
)	)	kIx)	)
a	a	k8xC	a
dramatické	dramatický	k2eAgFnSc2d1	dramatická
tvorby	tvorba	k1gFnSc2	tvorba
(	(	kIx(	(
<g/>
Lidé	člověk	k1gMnPc1	člověk
na	na	k7c6	na
kře	kra	k1gFnSc6	kra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
natáčení	natáčení	k1gNnSc6	natáčení
veseloher	veselohra	k1gFnPc2	veselohra
získal	získat	k5eAaPmAgInS	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
hned	hned	k6eAd1	hned
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
zvukové	zvukový	k2eAgFnSc2d1	zvuková
éry	éra	k1gFnSc2	éra
při	při	k7c6	při
realizaci	realizace	k1gFnSc6	realizace
série	série	k1gFnSc2	série
filmů	film	k1gInPc2	film
se	s	k7c7	s
známým	známý	k2eAgInSc7d1	známý
českým	český	k2eAgInSc7d1	český
divadelním	divadelní	k2eAgInSc7d1	divadelní
a	a	k8xC	a
filmovým	filmový	k2eAgMnSc7d1	filmový
komikem	komik	k1gMnSc7	komik
Vlastou	Vlasta	k1gMnSc7	Vlasta
Burianem	Burian	k1gMnSc7	Burian
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žánru	žánr	k1gInSc6	žánr
moderních	moderní	k2eAgFnPc2d1	moderní
společenských	společenský	k2eAgFnPc2d1	společenská
komedií	komedie	k1gFnPc2	komedie
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
jeho	jeho	k3xOp3gNnSc4	jeho
filmy	film	k1gInPc1	film
s	s	k7c7	s
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
jevištním	jevištní	k2eAgInSc7d1	jevištní
veseloherním	veseloherní	k2eAgInSc7d1	veseloherní
herce	herc	k1gInSc2	herc
Oldřichem	Oldřich	k1gMnSc7	Oldřich
Novým	Nový	k1gMnSc7	Nový
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Frič	Frič	k1gMnSc1	Frič
první	první	k4xOgFnSc7	první
přivedl	přivést	k5eAaPmAgMnS	přivést
před	před	k7c4	před
kameru	kamera	k1gFnSc4	kamera
(	(	kIx(	(
<g/>
Kristián	Kristián	k1gMnSc1	Kristián
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
tropí	tropit	k5eAaImIp3nS	tropit
hlouposti	hloupost	k1gFnSc2	hloupost
<g/>
,	,	kIx,	,
Roztomilý	roztomilý	k2eAgMnSc1d1	roztomilý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
Katakomby	katakomby	k1gFnPc1	katakomby
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
režiséry	režisér	k1gMnPc7	režisér
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
patří	patřit	k5eAaImIp3nS	patřit
Miroslav	Miroslav	k1gMnSc1	Miroslav
Cikán	cikán	k1gMnSc1	cikán
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Čáp	Čáp	k1gMnSc1	Čáp
<g/>
,	,	kIx,	,
J.	J.	kA	J.
A.	A.	kA	A.
Holman	Holman	k1gMnSc1	Holman
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Slavínský	Slavínský	k2eAgMnSc1d1	Slavínský
a	a	k8xC	a
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
protektorátních	protektorátní	k2eAgNnPc6d1	protektorátní
letech	léto	k1gNnPc6	léto
režírovalo	režírovat	k5eAaImAgNnS	režírovat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
na	na	k7c6	na
režii	režie	k1gFnSc6	režie
podílelo	podílet	k5eAaImAgNnS	podílet
23	[number]	k4	23
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
natočili	natočit	k5eAaBmAgMnP	natočit
114	[number]	k4	114
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1940	[number]	k4	1940
v	v	k7c6	v
listu	list	k1gInSc6	list
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
Herald	Herald	k1gMnSc1	Herald
Tribute	tribut	k1gInSc5	tribut
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nedostižný	Nedostižný	k?	Nedostižný
Charles	Charles	k1gMnSc1	Charles
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
v	v	k7c6	v
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Diktátor	diktátor	k1gMnSc1	diktátor
je	být	k5eAaImIp3nS	být
ostrým	ostrý	k2eAgInSc7d1	ostrý
komickým	komický	k2eAgInSc7d1	komický
komentářem	komentář	k1gInSc7	komentář
ke	k	k7c3	k
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zešílel	zešílet	k5eAaPmAgMnS	zešílet
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
solidní	solidní	k2eAgFnSc4d1	solidní
strukturu	struktura	k1gFnSc4	struktura
neodolatelného	odolatelný	k2eNgInSc2d1	neodolatelný
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
plane	planout	k5eAaImIp3nS	planout
hněvem	hněv	k1gInSc7	hněv
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Chaplinův	Chaplinův	k2eAgInSc4d1	Chaplinův
první	první	k4xOgInSc4	první
mluvený	mluvený	k2eAgInSc4d1	mluvený
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
chabě	chabě	k6eAd1	chabě
zakrývaná	zakrývaný	k2eAgFnSc1d1	zakrývaná
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
nacistické	nacistický	k2eAgNnSc4d1	nacistické
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
vydělal	vydělat	k5eAaPmAgInS	vydělat
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
než	než	k8xS	než
jeho	jeho	k3xOp3gInPc4	jeho
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
ostatní	ostatní	k2eAgInPc4d1	ostatní
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
i	i	k8xC	i
svět	svět	k1gInSc4	svět
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
hněvu	hněv	k1gInSc3	hněv
pádné	pádný	k2eAgMnPc4d1	pádný
důvody	důvod	k1gInPc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
USA	USA	kA	USA
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
neutrální	neutrální	k2eAgFnPc4d1	neutrální
až	až	k9	až
do	do	k7c2	do
bombardování	bombardování	k1gNnSc2	bombardování
Pearl	Pearl	k1gInSc1	Pearl
Harboru	Harbor	k1gInSc2	Harbor
Japonskem	Japonsko	k1gNnSc7	Japonsko
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
Hollywood	Hollywood	k1gInSc1	Hollywood
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
předválečné	předválečný	k2eAgFnSc6d1	předválečná
pohotovosti	pohotovost	k1gFnSc6	pohotovost
několika	několik	k4yIc2	několik
filmy	film	k1gInPc1	film
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
natočenými	natočený	k2eAgMnPc7d1	natočený
před	před	k7c7	před
náletem	nálet	k1gInSc7	nálet
<g/>
.	.	kIx.	.
</s>
<s>
York	York	k1gInSc1	York
Četař	četař	k1gMnSc1	četař
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Howarda	Howarda	k1gFnSc1	Howarda
Hawkse	Hawkse	k1gFnSc1	Hawkse
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hrál	hrát	k5eAaImAgMnS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
Gary	Gara	k1gFnSc2	Gara
Cooper	Coopero	k1gNnPc2	Coopero
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
odehrával	odehrávat	k5eAaImAgInS	odehrávat
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
útokem	útok	k1gInSc7	útok
proti	proti	k7c3	proti
izolacionismu	izolacionismus	k1gInSc3	izolacionismus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyzývaly	vyzývat	k5eAaImAgFnP	vyzývat
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
chopila	chopit	k5eAaPmAgFnS	chopit
zbraní	zbraň	k1gFnSc7	zbraň
a	a	k8xC	a
vyzdvihovaly	vyzdvihovat	k5eAaImAgFnP	vyzdvihovat
ctnosti	ctnost	k1gFnPc1	ctnost
demokracie	demokracie	k1gFnSc2	demokracie
proti	proti	k7c3	proti
brutalitě	brutalita	k1gFnSc3	brutalita
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgFnP	patřit
Paní	paní	k1gFnPc1	paní
Miniverová	Miniverová	k1gFnSc1	Miniverová
Williama	Williama	k1gFnSc1	Williama
Wylera	Wylera	k1gFnSc1	Wylera
<g/>
,	,	kIx,	,
Casablanca	Casablanca	k1gFnSc1	Casablanca
Michaela	Michael	k1gMnSc2	Michael
Curtize	Curtize	k1gFnSc2	Curtize
odehrávající	odehrávající	k2eAgFnSc2d1	odehrávající
se	se	k3xPyFc4	se
ve	v	k7c6	v
válečném	válečný	k2eAgNnSc6d1	válečné
Maroku	Maroko	k1gNnSc6	Maroko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
a	a	k8xC	a
Být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
či	či	k8xC	či
nebýt	být	k5eNaImF	být
Ernsta	Ernst	k1gMnSc4	Ernst
Lubitshe	Lubitsh	k1gMnSc4	Lubitsh
<g/>
.	.	kIx.	.
</s>
<s>
Vypuknutí	vypuknutí	k1gNnSc1	vypuknutí
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ohrožovalo	ohrožovat	k5eAaImAgNnS	ohrožovat
životně	životně	k6eAd1	životně
důležitý	důležitý	k2eAgInSc4d1	důležitý
hollywoodský	hollywoodský	k2eAgInSc4d1	hollywoodský
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Vývoz	vývoz	k1gInSc1	vývoz
studií	studie	k1gFnPc2	studie
do	do	k7c2	do
států	stát	k1gInPc2	stát
Osy	osa	k1gFnSc2	osa
-	-	kIx~	-
hlavně	hlavně	k6eAd1	hlavně
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
-	-	kIx~	-
klesl	klesnout	k5eAaPmAgInS	klesnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
.	.	kIx.	.
</s>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
však	však	k9	však
přesto	přesto	k6eAd1	přesto
čerpal	čerpat	k5eAaImAgInS	čerpat
třetinu	třetina	k1gFnSc4	třetina
svých	svůj	k3xOyFgInPc2	svůj
celkových	celkový	k2eAgInPc2d1	celkový
příjmů	příjem	k1gInPc2	příjem
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
trhů	trh	k1gInPc2	trh
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Británie	Británie	k1gFnSc1	Británie
jediným	jediný	k2eAgNnSc7d1	jediné
význačným	význačný	k2eAgNnSc7d1	význačné
zahraničním	zahraniční	k2eAgNnSc7d1	zahraniční
útočištěm	útočiště	k1gNnSc7	útočiště
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
se	se	k3xPyFc4	se
studia	studio	k1gNnPc1	studio
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
pokrýt	pokrýt	k5eAaPmF	pokrýt
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
kvalitních	kvalitní	k2eAgInPc6d1	kvalitní
filmech	film	k1gInPc6	film
buď	buď	k8xC	buď
obracela	obracet	k5eAaImAgFnS	obracet
na	na	k7c4	na
nezávislé	závislý	k2eNgMnPc4d1	nezávislý
producenty	producent	k1gMnPc4	producent
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
řady	řada	k1gFnPc4	řada
se	se	k3xPyFc4	se
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
rychle	rychle	k6eAd1	rychle
rozrůstaly	rozrůstat	k5eAaImAgFnP	rozrůstat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
svým	svůj	k3xOyFgInPc3	svůj
vlastním	vlastní	k2eAgInPc3d1	vlastní
talentům	talent	k1gInPc3	talent
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
pracovaly	pracovat	k5eAaImAgFnP	pracovat
na	na	k7c4	na
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc4d2	veliký
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Paramount	Paramount	k1gMnSc1	Paramount
získal	získat	k5eAaPmAgMnS	získat
Cecil	Cecil	k1gMnSc1	Cecil
B.	B.	kA	B.
DeMille	DeMille	k1gInSc1	DeMille
status	status	k1gInSc1	status
"	"	kIx"	"
<g/>
kmenového	kmenový	k2eAgNnSc2d1	kmenové
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
<g/>
"	"	kIx"	"
producenta	producent	k1gMnSc2	producent
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
podílu	podíl	k1gInSc6	podíl
na	na	k7c6	na
zisku	zisk	k1gInSc2	zisk
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
sílu	síla	k1gFnSc4	síla
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
filmových	filmový	k2eAgMnPc2d1	filmový
tvůrců	tvůrce	k1gMnPc2	tvůrce
a	a	k8xC	a
vrcholných	vrcholný	k2eAgInPc2d1	vrcholný
talentů	talent	k1gInPc2	talent
pracujících	pracující	k1gMnPc2	pracující
na	na	k7c4	na
smlouvu	smlouva	k1gFnSc4	smlouva
počátku	počátek	k1gInSc2	počátek
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
posílil	posílit	k5eAaPmAgInS	posílit
vzestup	vzestup	k1gInSc4	vzestup
profesních	profesní	k2eAgMnPc2d1	profesní
svazů	svaz	k1gInPc2	svaz
-	-	kIx~	-
Svaz	svaz	k1gInSc1	svaz
scenáristů	scenárista	k1gMnPc2	scenárista
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
režisérů	režisér	k1gMnPc2	režisér
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
filmových	filmový	k2eAgInPc2d1	filmový
herců	herc	k1gInPc2	herc
-	-	kIx~	-
což	což	k3yRnSc1	což
vážně	vážně	k6eAd1	vážně
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
kontrolu	kontrola	k1gFnSc4	kontrola
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pravomoci	pravomoc	k1gFnSc2	pravomoc
umělců	umělec	k1gMnPc2	umělec
k	k	k7c3	k
rozhodování	rozhodování	k1gNnSc3	rozhodování
o	o	k7c6	o
vlastní	vlastní	k2eAgFnSc6d1	vlastní
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
talenty	talent	k1gInPc1	talent
navíc	navíc	k6eAd1	navíc
odcházely	odcházet	k5eAaImAgInP	odcházet
na	na	k7c4	na
volnou	volný	k2eAgFnSc4d1	volná
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dále	daleko	k6eAd2	daleko
podrylo	podrýt	k5eAaPmAgNnS	podrýt
zavedený	zavedený	k2eAgInSc4d1	zavedený
systém	systém	k1gInSc4	systém
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
nadvládu	nadvláda	k1gFnSc4	nadvláda
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
úspěchu	úspěch	k1gInSc3	úspěch
snímku	snímek	k1gInSc2	snímek
Fantasia	Fantasius	k1gMnSc2	Fantasius
Walta	Walt	k1gMnSc2	Walt
Disneye	Disney	k1gMnSc2	Disney
a	a	k8xC	a
prvního	první	k4xOgInSc2	první
amerického	americký	k2eAgInSc2d1	americký
filmu	film	k1gInSc2	film
Alfreda	Alfred	k1gMnSc2	Alfred
Hitchcocka	Hitchcocka	k1gFnSc1	Hitchcocka
Mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
a	a	k8xC	a
živá	živý	k2eAgFnSc1d1	živá
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
se	s	k7c7	s
RKO	RKO	kA	RKO
(	(	kIx(	(
<g/>
Radio-Keith-Orpheum	Radio-Keith-Orpheum	k1gInSc1	Radio-Keith-Orpheum
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
logem	log	k1gInSc7	log
byl	být	k5eAaImAgInS	být
stožár	stožár	k1gInSc4	stožár
přenášející	přenášející	k2eAgInPc4d1	přenášející
vlnové	vlnový	k2eAgInPc4d1	vlnový
signály	signál	k1gInPc4	signál
stojící	stojící	k2eAgInPc4d1	stojící
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
)	)	kIx)	)
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
vážnými	vážný	k2eAgInPc7d1	vážný
finančními	finanční	k2eAgInPc7d1	finanční
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Distribuční	distribuční	k2eAgInSc1d1	distribuční
systém	systém	k1gInSc1	systém
RKO	RKO	kA	RKO
těžce	těžce	k6eAd1	těžce
postihlo	postihnout	k5eAaPmAgNnS	postihnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
O.	O.	kA	O.
Selznick	Selznick	k1gMnSc1	Selznick
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
přivedl	přivést	k5eAaPmAgInS	přivést
Hitchcocka	Hitchcocka	k1gFnSc1	Hitchcocka
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
)	)	kIx)	)
a	a	k8xC	a
Samuel	Samuel	k1gMnSc1	Samuel
Goldwyn	Goldwyn	k1gMnSc1	Goldwyn
založili	založit	k5eAaPmAgMnP	založit
vlastní	vlastní	k2eAgInSc4d1	vlastní
distribuční	distribuční	k2eAgFnSc4d1	distribuční
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
RKO	RKO	kA	RKO
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
rychlou	rychlý	k2eAgFnSc4d1	rychlá
návratnost	návratnost	k1gFnSc4	návratnost
<g/>
,	,	kIx,	,
vsadila	vsadit	k5eAaPmAgFnS	vsadit
na	na	k7c4	na
26	[number]	k4	26
<g/>
letého	letý	k2eAgMnSc4d1	letý
Orsona	Orson	k1gMnSc4	Orson
Wellese	Wellese	k1gFnSc2	Wellese
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
Občana	občan	k1gMnSc2	občan
Kanea	Kaneus	k1gMnSc2	Kaneus
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
jí	jíst	k5eAaImIp3nS	jíst
sice	sice	k8xC	sice
vynesl	vynést	k5eAaPmAgMnS	vynést
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
extrémně	extrémně	k6eAd1	extrémně
plodný	plodný	k2eAgInSc4d1	plodný
a	a	k8xC	a
produktivní	produktivní	k2eAgInSc4d1	produktivní
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
evropská	evropský	k2eAgFnSc1d1	Evropská
filmová	filmový	k2eAgFnSc1d1	filmová
produkce	produkce	k1gFnSc1	produkce
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
války	válka	k1gFnSc2	válka
trpěla	trpět	k5eAaImAgFnS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Hollywoodská	hollywoodský	k2eAgFnSc1d1	hollywoodská
produkce	produkce	k1gFnSc1	produkce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
až	až	k9	až
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
před	před	k7c4	před
velkou	velký	k2eAgFnSc4d1	velká
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
krizi	krize	k1gFnSc4	krize
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
návštěvnost	návštěvnost	k1gFnSc4	návštěvnost
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
připadalo	připadat	k5eAaImAgNnS	připadat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
domácnost	domácnost	k1gFnSc4	domácnost
2,52	[number]	k4	2,52
návštěvy	návštěva	k1gFnSc2	návštěva
kina	kino	k1gNnSc2	kino
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
2,33	[number]	k4	2,33
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
2,34	[number]	k4	2,34
návštěvy	návštěva	k1gFnPc4	návštěva
na	na	k7c4	na
domácnost	domácnost	k1gFnSc4	domácnost
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
0,23	[number]	k4	0,23
návštěvy	návštěva	k1gFnPc4	návštěva
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Dějové	dějový	k2eAgInPc1d1	dějový
filmy	film	k1gInPc1	film
nabízely	nabízet	k5eAaImAgInP	nabízet
masám	masa	k1gFnPc3	masa
snadný	snadný	k2eAgInSc4d1	snadný
<g/>
,	,	kIx,	,
nenákladný	nákladný	k2eNgInSc4d1	nenákladný
a	a	k8xC	a
dostupný	dostupný	k2eAgInSc4d1	dostupný
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uniknout	uniknout	k5eAaPmF	uniknout
od	od	k7c2	od
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
pracovní	pracovní	k2eAgFnSc2d1	pracovní
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
uskrovňování	uskrovňování	k1gNnSc2	uskrovňování
a	a	k8xC	a
děsivých	děsivý	k2eAgFnPc2d1	děsivá
zpráv	zpráva	k1gFnPc2	zpráva
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Westerny	western	k1gInPc1	western
<g/>
,	,	kIx,	,
technicolorové	technicolorový	k2eAgInPc1d1	technicolorový
muzikály	muzikál	k1gInPc1	muzikál
a	a	k8xC	a
propracované	propracovaný	k2eAgFnPc1d1	propracovaná
komedie	komedie	k1gFnPc1	komedie
byly	být	k5eAaImAgFnP	být
dokonalým	dokonalý	k2eAgNnSc7d1	dokonalé
uklidňujícími	uklidňující	k2eAgInPc7d1	uklidňující
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Amerika	Amerika	k1gFnSc1	Amerika
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
modely	model	k1gInPc1	model
pro	pro	k7c4	pro
vyjadřování	vyjadřování	k1gNnSc4	vyjadřování
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
ženou	žena	k1gFnSc7	žena
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
střetávaly	střetávat	k5eAaImAgFnP	střetávat
s	s	k7c7	s
realitami	realita	k1gFnPc7	realita
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ženy	žena	k1gFnPc1	žena
vykonávaly	vykonávat	k5eAaImAgFnP	vykonávat
mužské	mužský	k2eAgFnPc4d1	mužská
profese	profes	k1gFnPc4	profes
a	a	k8xC	a
samy	sám	k3xTgFnPc1	sám
se	se	k3xPyFc4	se
staraly	starat	k5eAaImAgFnP	starat
o	o	k7c4	o
domácnost	domácnost	k1gFnSc4	domácnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
muži	muž	k1gMnPc1	muž
bojovali	bojovat	k5eAaImAgMnP	bojovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odráželo	odrážet	k5eAaImAgNnS	odrážet
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dobových	dobový	k2eAgInPc6d1	dobový
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
hrály	hrát	k5eAaImAgFnP	hrát
energické	energický	k2eAgFnPc1d1	energická
ženské	ženský	k2eAgFnPc1d1	ženská
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Barbara	Barbara	k1gFnSc1	Barbara
Stanwyck	Stanwycka	k1gFnPc2	Stanwycka
<g/>
,	,	kIx,	,
Bette	Bett	k1gInSc5	Bett
Davis	Davis	k1gFnSc1	Davis
a	a	k8xC	a
Joan	Joan	k1gMnSc1	Joan
Crawford	Crawford	k1gMnSc1	Crawford
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
v	v	k7c6	v
působivých	působivý	k2eAgInPc6d1	působivý
melodramatech	melodramatech	k?	melodramatech
<g/>
,	,	kIx,	,
k	k	k7c3	k
jakým	jaký	k3yIgFnPc3	jaký
patří	patřit	k5eAaImIp3nS	patřit
Now	Now	k1gMnSc1	Now
<g/>
,	,	kIx,	,
Voyager	Voyager	k1gMnSc1	Voyager
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mildred	Mildred	k1gMnSc1	Mildred
Pierceová	Pierceová	k1gFnSc1	Pierceová
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ochota	ochota	k1gFnSc1	ochota
angažovat	angažovat	k5eAaBmF	angažovat
se	se	k3xPyFc4	se
v	v	k7c6	v
seriózních	seriózní	k2eAgFnPc6d1	seriózní
konfrontacích	konfrontace	k1gFnPc6	konfrontace
se	s	k7c7	s
sociálními	sociální	k2eAgInPc7d1	sociální
problémy	problém	k1gInPc7	problém
s	s	k7c7	s
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
a	a	k8xC	a
rasovou	rasový	k2eAgFnSc7d1	rasová
bigotností	bigotnost	k1gFnSc7	bigotnost
se	se	k3xPyFc4	se
však	však	k9	však
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
neamerickou	americký	k2eNgFnSc4d1	neamerická
činnost	činnost	k1gFnSc4	činnost
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
HUAC	HUAC	kA	HUAC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podnícený	podnícený	k2eAgInSc1d1	podnícený
senátorem	senátor	k1gMnSc7	senátor
Josephem	Joseph	k1gInSc7	Joseph
McCarthym	McCarthymum	k1gNnPc2	McCarthymum
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgInS	zahájit
své	svůj	k3xOyFgNnSc4	svůj
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
údajné	údajný	k2eAgFnSc2d1	údajná
komunistické	komunistický	k2eAgFnSc2d1	komunistická
infiltrace	infiltrace	k1gFnSc2	infiltrace
do	do	k7c2	do
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
HUAC	HUAC	kA	HUAC
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hollywoodští	hollywoodský	k2eAgMnPc1d1	hollywoodský
filmoví	filmový	k2eAgMnPc1d1	filmový
tvůrci	tvůrce	k1gMnPc1	tvůrce
"	"	kIx"	"
<g/>
využívají	využívat	k5eAaImIp3nP	využívat
rafinované	rafinovaný	k2eAgFnPc1d1	rafinovaná
metody	metoda	k1gFnPc1	metoda
a	a	k8xC	a
ve	v	k7c6	v
filmech	film	k1gInPc6	film
glorifikují	glorifikovat	k5eAaBmIp3nP	glorifikovat
komunistický	komunistický	k2eAgInSc4d1	komunistický
systém	systém	k1gInSc4	systém
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1947	[number]	k4	1947
zahájil	zahájit	k5eAaPmAgInS	zahájit
veřejná	veřejný	k2eAgNnPc4d1	veřejné
slyšení	slyšení	k1gNnSc4	slyšení
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
vyslýchal	vyslýchat	k5eAaImAgInS	vyslýchat
"	"	kIx"	"
<g/>
přátelské	přátelský	k2eAgNnSc4d1	přátelské
<g/>
"	"	kIx"	"
svědky	svědek	k1gMnPc7	svědek
(	(	kIx(	(
<g/>
vstřícné	vstřícný	k2eAgFnPc1d1	vstřícná
k	k	k7c3	k
účelům	účel	k1gInPc3	účel
výboru	výbor	k1gInSc2	výbor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
patřili	patřit	k5eAaImAgMnP	patřit
Adolphe	Adolphe	k1gFnSc4	Adolphe
Menjou	Menjá	k1gFnSc4	Menjá
<g/>
,	,	kIx,	,
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Taylor	Taylor	k1gMnSc1	Taylor
a	a	k8xC	a
Gary	Gara	k1gFnPc1	Gara
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc4	deset
takzvaně	takzvaně	k6eAd1	takzvaně
"	"	kIx"	"
<g/>
nevstřícných	vstřícný	k2eNgMnPc2d1	nevstřícný
<g/>
"	"	kIx"	"
svědků	svědek	k1gMnPc2	svědek
bylo	být	k5eAaImAgNnS	být
ke	k	k7c3	k
slyšení	slyšení	k1gNnSc6	slyšení
předvoláno	předvolat	k5eAaPmNgNnS	předvolat
soudně	soudně	k6eAd1	soudně
<g/>
.	.	kIx.	.
</s>
<s>
Hollywoodská	hollywoodský	k2eAgFnSc1d1	hollywoodská
desítka	desítka	k1gFnSc1	desítka
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
<g/>
,	,	kIx,	,
když	když	k8xS	když
její	její	k3xOp3gMnPc1	její
příslušníci	příslušník	k1gMnPc1	příslušník
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
pátý	pátý	k4xOgInSc1	pátý
dodatek	dodatek	k1gInSc1	dodatek
americké	americký	k2eAgFnSc2d1	americká
ústavy	ústava	k1gFnSc2	ústava
dává	dávat	k5eAaImIp3nS	dávat
právo	práv	k2eAgNnSc1d1	právo
odmítnout	odmítnout	k5eAaPmF	odmítnout
odpovídat	odpovídat	k5eAaImF	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
či	či	k8xC	či
nejsou	být	k5eNaImIp3nP	být
komunisté	komunista	k1gMnPc1	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Edward	Edward	k1gMnSc1	Edward
Dmytryk	Dmytryk	k1gMnSc1	Dmytryk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
odvolal	odvolat	k5eAaPmAgInS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
filmových	filmový	k2eAgMnPc2d1	filmový
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
techniků	technik	k1gMnPc2	technik
ocitlo	ocitnout	k5eAaPmAgNnS	ocitnout
na	na	k7c6	na
černé	černý	k2eAgFnSc6d1	černá
listině	listina	k1gFnSc6	listina
<g/>
,	,	kIx,	,
smlouvy	smlouva	k1gFnPc1	smlouva
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
kariéra	kariéra	k1gFnSc1	kariéra
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pracovali	pracovat	k5eAaImAgMnP	pracovat
pod	pod	k7c7	pod
falešnými	falešný	k2eAgNnPc7d1	falešné
jmény	jméno	k1gNnPc7	jméno
nebo	nebo	k8xC	nebo
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
pomalu	pomalu	k6eAd1	pomalu
zotavovala	zotavovat	k5eAaImAgFnS	zotavovat
z	z	k7c2	z
úpadku	úpadek	k1gInSc2	úpadek
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
tvorbu	tvorba	k1gFnSc4	tvorba
filmů	film	k1gInPc2	film
zajišťovaly	zajišťovat	k5eAaImAgInP	zajišťovat
především	především	k9	především
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
střední	střední	k2eAgFnPc1d1	střední
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
velké	velký	k2eAgFnPc1d1	velká
filmové	filmový	k2eAgFnPc1d1	filmová
korporace	korporace	k1gFnPc1	korporace
jako	jako	k8xC	jako
Pathé	Pathý	k2eAgNnSc1d1	Pathé
nebo	nebo	k8xC	nebo
Gaumost	Gaumost	k1gFnSc1	Gaumost
zaměřily	zaměřit	k5eAaPmAgInP	zaměřit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
distribuci	distribuce	k1gFnSc4	distribuce
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
poloviny	polovina	k1gFnSc2	polovina
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
domácí	domácí	k2eAgFnSc1d1	domácí
výroba	výroba	k1gFnSc1	výroba
klesala	klesat	k5eAaImAgFnS	klesat
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
velkého	velký	k2eAgInSc2d1	velký
importu	import	k1gInSc2	import
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
<g/>
,	,	kIx,	,
především	především	k9	především
amerických	americký	k2eAgMnPc2d1	americký
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
filmový	filmový	k2eAgInSc4d1	filmový
průmysl	průmysl	k1gInSc4	průmysl
více	hodně	k6eAd2	hodně
podporovat	podporovat	k5eAaImF	podporovat
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1946	[number]	k4	1946
proto	proto	k8xC	proto
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Centre	centr	k1gInSc5	centr
national	nationat	k5eAaPmAgInS	nationat
de	de	k?	de
la	la	k1gNnSc7	la
cinématographie	cinématographie	k1gFnSc2	cinématographie
(	(	kIx(	(
<g/>
CNC	CNC	kA	CNC
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Centrum	centrum	k1gNnSc1	centrum
národní	národní	k2eAgFnSc2d1	národní
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
primární	primární	k2eAgInSc4d1	primární
úkol	úkol	k1gInSc4	úkol
zajistit	zajistit	k5eAaPmF	zajistit
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
podporu	podpora	k1gFnSc4	podpora
francouzské	francouzský	k2eAgFnSc2d1	francouzská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
centrum	centrum	k1gNnSc1	centrum
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
zavedlo	zavést	k5eAaPmAgNnS	zavést
legislativu	legislativa	k1gFnSc4	legislativa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
kvóty	kvóta	k1gFnPc4	kvóta
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
cizích	cizí	k2eAgInPc2d1	cizí
filmů	film	k1gInPc2	film
a	a	k8xC	a
daň	daň	k1gFnSc4	daň
ze	z	k7c2	z
vstupného	vstupné	k1gNnSc2	vstupné
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
měl	mít	k5eAaImAgMnS	mít
film	film	k1gInSc4	film
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
produkce	produkce	k1gFnSc2	produkce
průměrný	průměrný	k2eAgInSc1d1	průměrný
rozpočet	rozpočet	k1gInSc1	rozpočet
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
finanční	finanční	k2eAgFnSc1d1	finanční
podpora	podpora	k1gFnSc1	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vlády	vláda	k1gFnSc2	vláda
postupně	postupně	k6eAd1	postupně
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
silná	silný	k2eAgFnSc1d1	silná
americká	americký	k2eAgFnSc1d1	americká
konkurence	konkurence	k1gFnSc1	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
nad	nad	k7c7	nad
propastí	propast	k1gFnSc7	propast
krachu	krach	k1gInSc2	krach
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
využívali	využívat	k5eAaPmAgMnP	využívat
koprodukce	koprodukce	k1gFnPc4	koprodukce
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
kinematografie	kinematografie	k1gFnSc1	kinematografie
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
Francii	Francie	k1gFnSc6	Francie
vládl	vládnout	k5eAaImAgInS	vládnout
styl	styl	k1gInSc1	styl
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
kritiky	kritik	k1gMnPc7	kritik
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
tradice	tradice	k1gFnSc1	tradice
kvality	kvalita	k1gFnSc2	kvalita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
adaptace	adaptace	k1gFnPc4	adaptace
klasických	klasický	k2eAgFnPc2d1	klasická
literárních	literární	k2eAgFnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nešetřily	šetřit	k5eNaImAgFnP	šetřit
bohatých	bohatý	k2eAgFnPc2d1	bohatá
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
složitým	složitý	k2eAgNnSc7d1	složité
osvětlením	osvětlení	k1gNnSc7	osvětlení
či	či	k8xC	či
extravagantními	extravagantní	k2eAgInPc7d1	extravagantní
kostýmy	kostým	k1gInPc7	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Výrazné	výrazný	k2eAgFnPc1d1	výrazná
osobnosti	osobnost	k1gFnPc1	osobnost
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
byli	být	k5eAaImAgMnP	být
především	především	k9	především
scenáristické	scenáristický	k2eAgNnSc4d1	scenáristické
duo	duo	k1gNnSc4	duo
Jean	Jean	k1gMnSc1	Jean
Aurenche	Aurench	k1gInSc2	Aurench
a	a	k8xC	a
Pierre	Pierr	k1gInSc5	Pierr
Bost	Bostum	k1gNnPc2	Bostum
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
adaptacemi	adaptace	k1gFnPc7	adaptace
děl	dělo	k1gNnPc2	dělo
Georgese	Georgese	k1gFnSc1	Georgese
Feydeaua	Feydeaua	k1gFnSc1	Feydeaua
<g/>
,	,	kIx,	,
Colette	Colett	k1gMnSc5	Colett
<g/>
,	,	kIx,	,
Stendhala	Stendhal	k1gMnSc2	Stendhal
a	a	k8xC	a
Émile	Émil	k1gMnSc2	Émil
Zoly	Zola	k1gMnSc2	Zola
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
snad	snad	k9	snad
nejznámějším	známý	k2eAgInSc7d3	nejznámější
filmem	film	k1gInSc7	film
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
Zakázané	zakázaný	k2eAgFnPc1d1	zakázaná
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režírované	režírovaný	k2eAgFnSc2d1	režírovaná
René	René	k1gFnSc2	René
Clémentem	Clément	k1gInSc7	Clément
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
demonstrují	demonstrovat	k5eAaBmIp3nP	demonstrovat
morbidní	morbidní	k2eAgInPc1d1	morbidní
dětské	dětský	k2eAgInPc1d1	dětský
rituály	rituál	k1gInPc1	rituál
satirizující	satirizující	k2eAgFnSc4d1	satirizující
církevní	církevní	k2eAgFnSc4d1	církevní
posedlost	posedlost	k1gFnSc4	posedlost
smrtí	smrt	k1gFnPc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
známou	známý	k2eAgFnSc7d1	známá
scenáristicko-režijní	scenáristickoežijní	k2eAgFnSc7d1	scenáristicko-režijní
dvojicí	dvojice	k1gFnSc7	dvojice
byli	být	k5eAaImAgMnP	být
Charles	Charles	k1gMnSc1	Charles
Spaak	Spaak	k1gMnSc1	Spaak
a	a	k8xC	a
André	André	k1gMnSc5	André
Cayatte	Cayatt	k1gMnSc5	Cayatt
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
tvorbou	tvorba	k1gFnSc7	tvorba
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
především	především	k6eAd1	především
na	na	k7c4	na
kritiku	kritika	k1gFnSc4	kritika
francouzské	francouzský	k2eAgFnSc2d1	francouzská
justice	justice	k1gFnSc2	justice
a	a	k8xC	a
občas	občas	k6eAd1	občas
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k6eAd1	až
moralizujícího	moralizující	k2eAgInSc2d1	moralizující
postoje	postoj	k1gInSc2	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Henri-Georges	Henri-Georges	k1gMnSc1	Henri-Georges
Clouzot	Clouzot	k1gMnSc1	Clouzot
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
klasického	klasický	k2eAgInSc2d1	klasický
romantismu	romantismus	k1gInSc2	romantismus
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
sarkastických	sarkastický	k2eAgInPc2d1	sarkastický
thrillerů	thriller	k1gInPc2	thriller
popisující	popisující	k2eAgFnSc4d1	popisující
měšťáckou	měšťácký	k2eAgFnSc4d1	měšťácká
společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Nábřeží	nábřeží	k1gNnSc1	nábřeží
zlatníků	zlatník	k1gInPc2	zlatník
<g/>
,	,	kIx,	,
Mzda	mzda	k1gFnSc1	mzda
strachu	strach	k1gInSc2	strach
nebo	nebo	k8xC	nebo
Ďábelské	ďábelský	k2eAgFnPc1d1	ďábelská
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
navíc	navíc	k6eAd1	navíc
poprvé	poprvé	k6eAd1	poprvé
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
tradiční	tradiční	k2eAgInSc1d1	tradiční
Filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
představovat	představovat	k5eAaImF	představovat
hlavního	hlavní	k2eAgMnSc4d1	hlavní
konkurenta	konkurent	k1gMnSc4	konkurent
italského	italský	k2eAgInSc2d1	italský
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
italská	italský	k2eAgFnSc1d1	italská
kinematografie	kinematografie	k1gFnSc1	kinematografie
pod	pod	k7c7	pod
přísným	přísný	k2eAgInSc7d1	přísný
dohledem	dohled	k1gInSc7	dohled
diktátora	diktátor	k1gMnSc4	diktátor
Benitem	Benitum	k1gNnSc7	Benitum
Mussolinim	Mussolinim	k1gMnSc1	Mussolinim
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
filmové	filmový	k2eAgNnSc4d1	filmové
studio	studio	k1gNnSc4	studio
Cinecittà	Cinecittà	k1gFnSc2	Cinecittà
a	a	k8xC	a
italský	italský	k2eAgInSc1d1	italský
film	film	k1gInSc1	film
dostatečně	dostatečně	k6eAd1	dostatečně
financoval	financovat	k5eAaBmAgInS	financovat
<g/>
.	.	kIx.	.
</s>
<s>
Natáčely	natáčet	k5eAaImAgInP	natáčet
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
propagandistické	propagandistický	k2eAgInPc1d1	propagandistický
fašistické	fašistický	k2eAgInPc1d1	fašistický
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnPc1	komedie
nebo	nebo	k8xC	nebo
melodramata	melodramata	k?	melodramata
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
obecně	obecně	k6eAd1	obecně
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
bílé	bílý	k2eAgInPc1d1	bílý
telefony	telefon	k1gInPc1	telefon
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
typické	typický	k2eAgFnSc2d1	typická
buržoazní	buržoazní	k2eAgFnSc2d1	buržoazní
dekorace	dekorace	k1gFnSc2	dekorace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
vládní	vládní	k2eAgFnSc3d1	vládní
cenzuře	cenzura	k1gFnSc3	cenzura
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
redakce	redakce	k1gFnSc2	redakce
časopisu	časopis	k1gInSc2	časopis
Cinema	Cinemum	k1gNnSc2	Cinemum
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vedl	vést	k5eAaImAgMnS	vést
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
syn	syn	k1gMnSc1	syn
Vittorio	Vittorio	k1gMnSc1	Vittorio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
nový	nový	k2eAgInSc1d1	nový
styl	styl	k1gInSc1	styl
neorealismus	neorealismus	k1gInSc1	neorealismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
především	především	k9	především
na	na	k7c4	na
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
prostou	prostý	k2eAgFnSc4d1	prostá
osobu	osoba	k1gFnSc4	osoba
z	z	k7c2	z
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
představoval	představovat	k5eAaImAgMnS	představovat
neherec	neherec	k1gMnSc1	neherec
a	a	k8xC	a
prožíval	prožívat	k5eAaImAgMnS	prožívat
všední	všední	k2eAgInPc4d1	všední
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
natáčené	natáčený	k2eAgFnPc4d1	natáčená
ve	v	k7c6	v
skutečných	skutečný	k2eAgInPc6d1	skutečný
exteriérech	exteriér	k1gInPc6	exteriér
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
snímek	snímek	k1gInSc4	snímek
natočený	natočený	k2eAgInSc4d1	natočený
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
neorealismu	neorealismus	k1gInSc2	neorealismus
je	být	k5eAaImIp3nS	být
Posedlost	posedlost	k1gFnSc1	posedlost
od	od	k7c2	od
režiséra	režisér	k1gMnSc2	režisér
Luchina	Luchin	k1gMnSc2	Luchin
Viscontiho	Visconti	k1gMnSc2	Visconti
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
známým	známý	k2eAgMnSc7d1	známý
režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgMnS	být
Roberto	Roberta	k1gFnSc5	Roberta
Rossellini	Rossellin	k2eAgMnPc1d1	Rossellin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
umožnil	umožnit	k5eAaPmAgMnS	umožnit
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
filmové	filmový	k2eAgFnSc3d1	filmová
trilogie	trilogie	k1gFnPc1	trilogie
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
otevřené	otevřený	k2eAgNnSc1d1	otevřené
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
-	-	kIx~	-
Paisá	Paisý	k2eAgFnSc1d1	Paisý
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
nula	nula	k1gFnSc1	nula
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
prosadit	prosadit	k5eAaPmF	prosadit
nový	nový	k2eAgInSc4d1	nový
italský	italský	k2eAgInSc4d1	italský
film	film	k1gInSc4	film
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Neorealismu	neorealismus	k1gInSc3	neorealismus
nejlépe	dobře	k6eAd3	dobře
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
filmy	film	k1gInPc1	film
Vittoria	Vittorium	k1gNnSc2	Vittorium
de	de	k?	de
Sicy	Sicy	k?	Sicy
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
buduje	budovat	k5eAaImIp3nS	budovat
drama	drama	k1gNnSc4	drama
a	a	k8xC	a
napětí	napětí	k1gNnSc4	napětí
z	z	k7c2	z
drobných	drobný	k2eAgInPc2d1	drobný
všednodenních	všednodenní	k2eAgInPc2d1	všednodenní
detailů	detail	k1gInPc2	detail
ze	z	k7c2	z
života	život	k1gInSc2	život
obyčejných	obyčejný	k2eAgMnPc2d1	obyčejný
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Děti	dítě	k1gFnPc1	dítě
ulice	ulice	k1gFnSc2	ulice
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Zloději	zloděj	k1gMnPc1	zloděj
kol	kolo	k1gNnPc2	kolo
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
filmy	film	k1gInPc1	film
mapovaly	mapovat	k5eAaImAgInP	mapovat
problémy	problém	k1gInPc4	problém
vesnického	vesnický	k2eAgInSc2d1	vesnický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nP	patřit
nepochybně	pochybně	k6eNd1	pochybně
snímek	snímek	k1gInSc4	snímek
Hořká	hořká	k1gFnSc1	hořká
rýže	rýže	k1gFnSc1	rýže
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ukazoval	ukazovat	k5eAaImAgInS	ukazovat
využívání	využívání	k1gNnSc4	využívání
mladých	mladý	k2eAgFnPc2d1	mladá
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
donucených	donucený	k2eAgInPc2d1	donucený
ke	k	k7c3	k
špatně	špatně	k6eAd1	špatně
placené	placený	k2eAgFnSc3d1	placená
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
a	a	k8xC	a
nahnaných	nahnaný	k2eAgMnPc2d1	nahnaný
do	do	k7c2	do
ubytoven	ubytovna	k1gFnPc2	ubytovna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
podobaly	podobat	k5eAaImAgFnP	podobat
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
