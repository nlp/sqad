<p>
<s>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
jednoslovně	jednoslovně	k6eAd1	jednoslovně
Atlantik	Atlantik	k1gInSc4	Atlantik
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
mezi	mezi	k7c7	mezi
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
,	,	kIx,	,
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
po	po	k7c4	po
Antarktidu	Antarktida	k1gFnSc4	Antarktida
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podle	podle	k7c2	podle
Národní	národní	k2eAgFnSc2d1	národní
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
hydrografické	hydrografický	k2eAgFnSc2d1	hydrografická
organizace	organizace	k1gFnSc2	organizace
<g/>
)	)	kIx)	)
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
jižní	jižní	k2eAgNnSc4d1	jižní
60	[number]	k4	60
<g/>
.	.	kIx.	.
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Jižním	jižní	k2eAgInSc7d1	jižní
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
z	z	k7c2	z
pěti	pět	k4xCc2	pět
světových	světový	k2eAgInPc2d1	světový
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
přibližně	přibližně	k6eAd1	přibližně
pětinu	pětina	k1gFnSc4	pětina
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
včetně	včetně	k7c2	včetně
okrajových	okrajový	k2eAgNnPc2d1	okrajové
moří	moře	k1gNnPc2	moře
činí	činit	k5eAaImIp3nS	činit
cca	cca	kA	cca
106,5	[number]	k4	106,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
82,4	[number]	k4	82,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
2	[number]	k4	2
bez	bez	k7c2	bez
okrajových	okrajový	k2eAgNnPc2d1	okrajové
moří	moře	k1gNnPc2	moře
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
76,8	[number]	k4	76,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
2	[number]	k4	2
při	při	k7c6	při
uznání	uznání	k1gNnSc6	uznání
Jižního	jižní	k2eAgInSc2d1	jižní
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Atlantiku	Atlantik	k1gInSc2	Atlantik
přitéká	přitékat	k5eAaImIp3nS	přitékat
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
asi	asi	k9	asi
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
většího	veliký	k2eAgNnSc2d2	veliký
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
Tichého	Tichého	k2eAgInSc2d1	Tichého
nebo	nebo	k8xC	nebo
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
Atlantiku	Atlantik	k1gInSc2	Atlantik
je	být	k5eAaImIp3nS	být
i	i	k9	i
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
přilehlých	přilehlý	k2eAgNnPc2d1	přilehlé
moří	moře	k1gNnPc2	moře
354,7	[number]	k4	354,7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
323,6	[number]	k4	323,6
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
3	[number]	k4	3
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	jeho	k3xOp3gNnSc2	jeho
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
uváděna	uváděn	k2eAgFnSc1d1	uváděna
111	[number]	k4	111
866	[number]	k4	866
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
východ	východ	k1gInSc4	východ
<g/>
–	–	k?	–
<g/>
západ	západ	k1gInSc1	západ
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2848	[number]	k4	2848
km	km	kA	km
mezi	mezi	k7c7	mezi
Brazílií	Brazílie	k1gFnSc7	Brazílie
a	a	k8xC	a
Libérií	Libérie	k1gFnSc7	Libérie
do	do	k7c2	do
4830	[number]	k4	4830
km	km	kA	km
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
severní	severní	k2eAgFnSc7d1	severní
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlubší	hluboký	k2eAgNnSc4d3	nejhlubší
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
je	být	k5eAaImIp3nS	být
prohlubeň	prohlubeň	k1gFnSc4	prohlubeň
Milwaukee	Milwaukee	k1gFnPc2	Milwaukee
v	v	k7c6	v
Portorickém	portorický	k2eAgInSc6d1	portorický
příkopu	příkop	k1gInSc6	příkop
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
8648	[number]	k4	8648
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
3332	[number]	k4	3332
m	m	kA	m
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
3926	[number]	k4	3926
m	m	kA	m
bez	bez	k7c2	bez
moří	moře	k1gNnPc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středem	středem	k7c2	středem
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
největší	veliký	k2eAgInSc1d3	veliký
podmořský	podmořský	k2eAgInSc1d1	podmořský
horský	horský	k2eAgInSc1d1	horský
hřeben	hřeben	k1gInSc1	hřeben
–	–	k?	–
Středoatlantský	Středoatlantský	k2eAgInSc1d1	Středoatlantský
hřbet	hřbet	k1gInSc1	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
S	s	k7c7	s
od	od	k7c2	od
Islandu	Island	k1gInSc2	Island
až	až	k6eAd1	až
k	k	k7c3	k
58	[number]	k4	58
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
11	[number]	k4	11
300	[number]	k4	300
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
jsou	být	k5eAaImIp3nP	být
pevninského	pevninský	k2eAgMnSc4d1	pevninský
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
ostrovy	ostrov	k1gInPc1	ostrov
leží	ležet	k5eAaImIp3nP	ležet
blízko	blízko	k7c2	blízko
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
oddělily	oddělit	k5eAaPmAgInP	oddělit
–	–	k?	–
např.	např.	kA	např.
Grónsko	Grónsko	k1gNnSc1	Grónsko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
stycích	styk	k1gInPc6	styk
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
stovky	stovka	k1gFnPc1	stovka
sopečných	sopečný	k2eAgInPc2d1	sopečný
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Malé	Malé	k2eAgFnPc4d1	Malé
Antily	Antily	k1gFnPc4	Antily
<g/>
,	,	kIx,	,
Azory	Azory	k1gFnPc4	Azory
<g/>
,	,	kIx,	,
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Madeira	Madeira	k1gFnSc1	Madeira
a	a	k8xC	a
Kapverdy	Kapverda	k1gFnPc1	Kapverda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
severního	severní	k2eAgInSc2d1	severní
Atlantiku	Atlantik	k1gInSc2	Atlantik
leží	ležet	k5eAaImIp3nS	ležet
většina	většina	k1gFnSc1	většina
nejvyspělejších	vyspělý	k2eAgFnPc2d3	nejvyspělejší
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
protkán	protkat	k5eAaPmNgInS	protkat
sítí	síť	k1gFnSc7	síť
celosvětově	celosvětově	k6eAd1	celosvětově
nejpoužívanějších	používaný	k2eAgFnPc2d3	nejpoužívanější
mořských	mořský	k2eAgFnPc2d1	mořská
tras	trasa	k1gFnPc2	trasa
mezi	mezi	k7c7	mezi
východní	východní	k2eAgFnSc7d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc7d1	západní
polokoulí	polokoule	k1gFnSc7	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
významné	významný	k2eAgFnPc1d1	významná
světové	světový	k2eAgFnPc1d1	světová
oblasti	oblast	k1gFnPc1	oblast
rybolovu	rybolov	k1gInSc2	rybolov
a	a	k8xC	a
těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
v	v	k7c6	v
Karibském	karibský	k2eAgNnSc6d1	Karibské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
Mexickém	mexický	k2eAgInSc6d1	mexický
zálivu	záliv	k1gInSc6	záliv
a	a	k8xC	a
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgNnPc4d1	vnitřní
a	a	k8xC	a
okrajová	okrajový	k2eAgNnPc4d1	okrajové
moře	moře	k1gNnPc4	moře
===	===	k?	===
</s>
</p>
<p>
<s>
Grónské	grónský	k2eAgNnSc1d1	grónské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Irmingerovo	Irmingerův	k2eAgNnSc1d1	Irmingerův
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Labradorské	labradorský	k2eAgNnSc1d1	labradorský
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Sargasové	sargasový	k2eAgNnSc1d1	Sargasové
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Yukatánského	Yukatánský	k2eAgNnSc2d1	Yukatánský
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
</s>
</p>
<p>
<s>
Norské	norský	k2eAgNnSc1d1	norské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
</s>
</p>
<p>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Irské	irský	k2eAgNnSc1d1	irské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Lamanšský	lamanšský	k2eAgInSc1d1	lamanšský
průliv	průliv	k1gInSc1	průliv
</s>
</p>
<p>
<s>
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
</s>
</p>
<p>
<s>
Weddellovo	Weddellův	k2eAgNnSc1d1	Weddellovo
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
moře	moře	k1gNnSc1	moře
krále	král	k1gMnSc2	král
Haakona	Haakon	k1gMnSc2	Haakon
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c6	o
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
souostroví	souostroví	k1gNnSc6	souostroví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oceánské	oceánský	k2eAgFnSc2d1	oceánská
pánve	pánev	k1gFnSc2	pánev
===	===	k?	===
</s>
</p>
<p>
<s>
Africko-antarktická	africkontarktický	k2eAgFnSc1d1	africko-antarktický
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Angolská	angolský	k2eAgFnSc1d1	angolská
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Argentinská	argentinský	k2eAgFnSc1d1	Argentinská
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
Kanárská	kanárský	k2eAgFnSc1d1	Kanárská
pánev	pánev	k1gFnSc1	pánev
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgMnS	být
Atlas	Atlas	k1gMnSc1	Atlas
obr	obr	k1gMnSc1	obr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podpíral	podpírat	k5eAaImAgMnS	podpírat
nebeskou	nebeský	k2eAgFnSc4d1	nebeská
klenbu	klenba	k1gFnSc4	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1	řecké
jméno	jméno	k1gNnSc1	jméno
Atlantis	Atlantis	k1gFnSc1	Atlantis
thallasa	thallas	k1gMnSc2	thallas
znamená	znamenat	k5eAaImIp3nS	znamenat
Atlantovo	Atlantův	k2eAgNnSc1d1	Atlantův
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikali	pronikat	k5eAaImAgMnP	pronikat
na	na	k7c4	na
oceán	oceán	k1gInSc4	oceán
přes	přes	k7c4	přes
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc4	Grónsko
až	až	k9	až
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
severští	severský	k2eAgMnPc1d1	severský
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
podél	podél	k7c2	podél
západních	západní	k2eAgInPc2d1	západní
břehů	břeh	k1gInPc2	břeh
Afriky	Afrika	k1gFnSc2	Afrika
plavili	plavit	k5eAaImAgMnP	plavit
portugalští	portugalský	k2eAgMnPc1d1	portugalský
mořeplavci	mořeplavec	k1gMnPc1	mořeplavec
Bartolomeo	Bartolomeo	k1gMnSc1	Bartolomeo
Diaz	Diaz	k1gMnSc1	Diaz
a	a	k8xC	a
Vasco	Vasco	k1gMnSc1	Vasco
da	da	k?	da
Gama	gama	k1gNnSc2	gama
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
španělská	španělský	k2eAgFnSc1d1	španělská
výprava	výprava	k1gFnSc1	výprava
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Itala	Ital	k1gMnSc2	Ital
ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
službách	služba	k1gFnPc6	služba
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
(	(	kIx(	(
<g/>
Cristoforo	Cristofora	k1gFnSc5	Cristofora
Colombo	Colomba	k1gFnSc5	Colomba
<g/>
)	)	kIx)	)
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
plavbou	plavba	k1gFnSc7	plavba
přes	přes	k7c4	přes
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
Bahamských	bahamský	k2eAgInPc2d1	bahamský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okrajové	okrajový	k2eAgFnSc6d1	okrajová
části	část	k1gFnSc6	část
==	==	k?	==
</s>
</p>
<p>
<s>
Sargasové	sargasový	k2eAgNnSc1d1	Sargasové
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Guinejský	guinejský	k2eAgInSc1d1	guinejský
záliv	záliv	k1gInSc1	záliv
</s>
</p>
<p>
<s>
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
</s>
</p>
<p>
<s>
Karibské	karibský	k2eAgNnSc1d1	Karibské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
</s>
</p>
<p>
<s>
Baltské	baltský	k2eAgNnSc1d1	Baltské
moře	moře	k1gNnSc1	moře
</s>
</p>
<p>
<s>
Severní	severní	k2eAgNnSc4d1	severní
moře	moře	k1gNnSc4	moře
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
CRUMMENERL	CRUMMENERL	kA	CRUMMENERL
<g/>
,	,	kIx,	,
Rainer	Rainer	k1gInSc1	Rainer
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
a	a	k8xC	a
oceány	oceán	k1gInPc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Fraus	fraus	k1gFnSc1	fraus
<g/>
,	,	kIx,	,
©	©	k?	©
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Edice	edice	k1gFnSc1	edice
Co-jak-proč	Coakroč	k1gFnSc2	Co-jak-proč
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7238	[number]	k4	7238
<g/>
-	-	kIx~	-
<g/>
477	[number]	k4	477
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JANSKÝ	janský	k2eAgMnSc1d1	janský
<g/>
,	,	kIx,	,
Bohumír	Bohumír	k1gMnSc1	Bohumír
<g/>
.	.	kIx.	.
</s>
<s>
Geografie	geografie	k1gFnSc1	geografie
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7066	[number]	k4	7066
<g/>
-	-	kIx~	-
<g/>
678	[number]	k4	678
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moře	moře	k1gNnSc1	moře
a	a	k8xC	a
oceány	oceán	k1gInPc1	oceán
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
©	©	k?	©
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
8302	[number]	k4	8302
<g/>
-	-	kIx~	-
<g/>
2297	[number]	k4	2297
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
:	:	kIx,	:
pro	pro	k7c4	pro
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
včetně	včetně	k7c2	včetně
škol	škola	k1gFnPc2	škola
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
podle	podle	k7c2	podle
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
programu	program	k1gInSc2	program
Obecná	obecný	k2eAgFnSc1d1	obecná
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Naps	Naps	k1gInSc1	Naps
<g/>
.	.	kIx.	.
prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Petr	Petr	k1gMnSc1	Petr
Chalupa	Chalupa	k1gMnSc1	Chalupa
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPL	SPL	kA	SPL
–	–	k?	–
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86287	[number]	k4	86287
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc2	galerie
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
