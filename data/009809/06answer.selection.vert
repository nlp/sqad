<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
je	být	k5eAaImIp3nS	být
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k8xC	i
rozlohou	rozloha	k1gFnSc7	rozloha
třetí	třetí	k4xOgNnSc4	třetí
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
Slezsku	Slezsko	k1gNnSc6	Slezsko
(	(	kIx(	(
<g/>
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
