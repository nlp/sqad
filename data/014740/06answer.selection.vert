<s>
V	v	k7c6
letech	léto	k1gNnPc6
1931	#num#	k4
až	až	k9
1934	#num#	k4
studoval	studovat	k5eAaImAgInS
Turing	Turing	k1gInSc1
matematiku	matematika	k1gFnSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
na	na	k7c4
King	King	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gFnSc7
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
členem	člen	k1gMnSc7
univerzitní	univerzitní	k2eAgFnSc2d1
koleje	kolej	k1gFnSc2
(	(	kIx(
<g/>
fellow	fellow	k?
<g/>
)	)	kIx)
na	na	k7c6
základě	základ	k1gInSc6
své	svůj	k3xOyFgFnSc2
disertace	disertace	k1gFnSc2
o	o	k7c6
centrální	centrální	k2eAgFnSc6d1
limitní	limitní	k2eAgFnSc6d1
větě	věta	k1gFnSc6
<g/>
.	.	kIx.
</s>