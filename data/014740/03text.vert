<s>
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc4
</s>
<s>
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc4
cca	cca	kA
v	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
<g/>
Narození	narození	k1gNnPc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1912	#num#	k4
Maida	Maido	k1gNnSc2
Vale	val	k1gInSc6
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1954	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
41	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Wilmslow	Wilmslow	k1gMnSc5
<g/>
,	,	kIx,
Cheshire	Cheshir	k1gMnSc5
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
britská	britský	k2eAgFnSc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Sherborne	Sherbornout	k5eAaPmIp3nS
School	School	k1gInSc1
<g/>
,	,	kIx,
King	King	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gFnSc7
(	(	kIx(
<g/>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Princetonská	Princetonský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Pracoviště	pracoviště	k1gNnSc2
</s>
<s>
University	universita	k1gFnPc1
of	of	k?
Manchester	Manchester	k1gInSc1
<g/>
,	,	kIx,
Government	Government	k1gInSc1
Communications	Communicationsa	k1gFnPc2
Headquarters	Headquarters	k1gInSc1
<g/>
,	,	kIx,
National	National	k1gMnSc1
Physical	Physical	k1gFnSc2
Laboratory	Laborator	k1gInPc4
<g/>
,	,	kIx,
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Cambridgi	Cambridge	k1gFnSc6
Obory	obora	k1gFnSc2
</s>
<s>
matematika	matematika	k1gFnSc1
<g/>
,	,	kIx,
kryptoanalýza	kryptoanalýza	k1gFnSc1
<g/>
,	,	kIx,
matematická	matematický	k2eAgFnSc1d1
informatika	informatika	k1gFnSc1
<g/>
,	,	kIx,
biologie	biologie	k1gFnSc1
Známý	známý	k1gMnSc1
díky	díky	k7c3
</s>
<s>
kryptoanalýza	kryptoanalýza	k1gFnSc1
Enigmy	enigma	k1gFnSc2
<g/>
,	,	kIx,
Turingův	Turingův	k2eAgInSc1d1
stroj	stroj	k1gInSc1
<g/>
,	,	kIx,
Turingův	Turingův	k2eAgInSc1d1
test	test	k1gInSc1
<g/>
,	,	kIx,
Turingova	Turingův	k2eAgFnSc1d1
Bomba	bomba	k1gFnSc1
<g/>
,	,	kIx,
problém	problém	k1gInSc1
zastavení	zastavení	k1gNnSc6
Významná	významný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
On	on	k3xPp3gMnSc1
computable	computable	k6eAd1
numbers	numbers	k6eAd1
<g/>
,	,	kIx,
with	with	k1gInSc1
an	an	k?
application	application	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Entscheidungsproblem	Entscheidungsprobl	k1gInSc7
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Smith	Smith	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Prize	Priza	k1gFnSc6
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Řád	řád	k1gInSc1
britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
,	,	kIx,
Člen	člen	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
Julius	Julius	k1gMnSc1
Mathison	Mathisona	k1gFnPc2
Turing	Turing	k1gInSc4
a	a	k8xC
Ethel	Ethel	k1gInSc4
Sara	Sarum	k1gNnSc2
Stoney	Stonea	k1gFnSc2
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alan	Alan	k1gMnSc1
Mathison	Mathison	k1gMnSc1
Turing	Turing	k1gInSc4
<g/>
,	,	kIx,
OBE	Ob	k1gInSc5
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1912	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1954	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
logik	logik	k1gMnSc1
<g/>
,	,	kIx,
kryptoanalytik	kryptoanalytik	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
moderní	moderní	k2eAgFnSc2d1
informatiky	informatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejně	veřejně	k6eAd1
známý	známý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
zásluhy	zásluha	k1gFnPc4
o	o	k7c6
dešifrování	dešifrování	k1gNnSc6
nacistických	nacistický	k2eAgInPc2d1
tajných	tajný	k2eAgInPc2d1
kódů	kód	k1gInPc2
během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
–	–	k?
Enigmy	enigma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
byl	být	k5eAaImAgInS
odsouzen	odsoudit	k5eAaPmNgInS,k5eAaImNgInS
za	za	k7c4
homosexuální	homosexuální	k2eAgInSc4d1
akt	akt	k1gInSc4
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1885	#num#	k4
a	a	k8xC
posmrtně	posmrtně	k6eAd1
rehabilitován	rehabilitován	k2eAgInSc1d1
královnou	královna	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc4
narodil	narodit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
z	z	k7c2
Anglie	Anglie	k1gFnSc2
zpátky	zpátky	k6eAd1
do	do	k7c2
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
malého	malý	k2eAgMnSc4d1
Alana	Alan	k1gMnSc4
s	s	k7c7
sebou	se	k3xPyFc7
nevzali	vzít	k5eNaPmAgMnP
<g/>
,	,	kIx,
vychovávali	vychovávat	k5eAaImAgMnP
ho	on	k3xPp3gNnSc4
příbuzní	příbuzný	k1gMnPc1
a	a	k8xC
chůvy	chůva	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alan	Alan	k1gMnSc1
ani	ani	k9
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
dětství	dětství	k1gNnSc6
nevykazoval	vykazovat	k5eNaImAgMnS
výjimečnou	výjimečný	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
průměrným	průměrný	k2eAgMnSc7d1
žákem	žák	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavily	bavit	k5eAaImAgInP
ho	on	k3xPp3gMnSc4
šachy	šach	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
nebyl	být	k5eNaImAgMnS
zvlášť	zvlášť	k6eAd1
dobrým	dobrý	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Plastika	plastika	k1gFnSc1
vědce	vědec	k1gMnSc2
v	v	k7c6
pracovně	pracovna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
měl	mít	k5eAaImAgMnS
Alan	Alan	k1gMnSc1
nastoupit	nastoupit	k5eAaPmF
na	na	k7c4
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Sherborne	Sherborn	k1gInSc5
<g/>
,	,	kIx,
ochromila	ochromit	k5eAaPmAgFnS
Británii	Británie	k1gFnSc4
devítidenní	devítidenní	k2eAgFnSc1d1
všeobecná	všeobecný	k2eAgFnSc1d1
stávka	stávka	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Alan	Alan	k1gMnSc1
vzal	vzít	k5eAaPmAgMnS
kolo	kolo	k1gNnSc4
a	a	k8xC
během	během	k7c2
dvou	dva	k4xCgInPc2
dnů	den	k1gInPc2
dojel	dojet	k5eAaPmAgInS
do	do	k7c2
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
vzdálena	vzdálit	k5eAaPmNgFnS
asi	asi	k9
100	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
Christopherem	Christopher	k1gMnSc7
Morcomem	Morcom	k1gMnSc7
<g/>
,	,	kIx,
bavili	bavit	k5eAaImAgMnP
se	se	k3xPyFc4
spolu	spolu	k6eAd1
o	o	k7c6
vědeckých	vědecký	k2eAgFnPc6d1
novinkách	novinka	k1gFnPc6
a	a	k8xC
prováděli	provádět	k5eAaImAgMnP
vlastní	vlastní	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morcomova	Morcomův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
Alana	Alan	k1gMnSc2
těžce	těžce	k6eAd1
zasáhla	zasáhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1931	#num#	k4
až	až	k9
1934	#num#	k4
studoval	studovat	k5eAaImAgInS
Turing	Turing	k1gInSc1
matematiku	matematika	k1gFnSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
na	na	k7c4
King	King	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
College	College	k1gFnSc7
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
členem	člen	k1gMnSc7
univerzitní	univerzitní	k2eAgFnSc2d1
koleje	kolej	k1gFnSc2
(	(	kIx(
<g/>
fellow	fellow	k?
<g/>
)	)	kIx)
na	na	k7c6
základě	základ	k1gInSc6
své	svůj	k3xOyFgFnSc2
disertace	disertace	k1gFnSc2
o	o	k7c6
centrální	centrální	k2eAgFnSc6d1
limitní	limitní	k2eAgFnSc6d1
větě	věta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Turingovy	Turingův	k2eAgFnPc1d1
největší	veliký	k2eAgFnPc1d3
vědecké	vědecký	k2eAgFnPc1d1
zásluhy	zásluha	k1gFnPc1
tkví	tkvět	k5eAaImIp3nP
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
článku	článek	k1gInSc6
„	„	k?
<g/>
On	on	k3xPp3gMnSc1
Computable	Computable	k1gMnSc1
Numbers	Numbersa	k1gFnPc2
<g/>
,	,	kIx,
with	witha	k1gFnPc2
an	an	k?
Application	Application	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Entscheidungsproblem	Entscheidungsprobl	k1gInSc7
<g/>
“	“	k?
z	z	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavádí	zavádět	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
pojem	pojem	k1gInSc4
Turingova	Turingův	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
,	,	kIx,
teoretického	teoretický	k2eAgInSc2d1
modelu	model	k1gInSc2
obecného	obecný	k2eAgInSc2d1
výpočetního	výpočetní	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
základů	základ	k1gInPc2
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
dokázal	dokázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
problém	problém	k1gInSc1
zastavení	zastavení	k1gNnSc2
Turingova	Turingův	k2eAgInSc2d1
stroje	stroj	k1gInSc2
není	být	k5eNaImIp3nS
rozhodnutelný	rozhodnutelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
Churchovy-Turingovy	Churchovy-Turingův	k2eAgFnSc2d1
teze	teze	k1gFnSc2
pak	pak	k6eAd1
lze	lze	k6eAd1
toto	tento	k3xDgNnSc4
zjištění	zjištění	k1gNnSc4
aplikovat	aplikovat	k5eAaBmF
na	na	k7c6
Hilbertem	Hilbert	k1gInSc7
formulovaný	formulovaný	k2eAgInSc1d1
tzv.	tzv.	kA
Entscheidungsproblem	Entscheidungsprobl	k1gMnSc7
neboli	neboli	k8xC
problém	problém	k1gInSc4
rozhodnutelnosti	rozhodnutelnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1937	#num#	k4
a	a	k8xC
1938	#num#	k4
studoval	studovat	k5eAaImAgInS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Princetonu	Princeton	k1gInSc6
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Alonza	Alonz	k1gMnSc2
Churche	Church	k1gMnSc2
a	a	k8xC
získal	získat	k5eAaPmAgInS
zde	zde	k6eAd1
doktorát	doktorát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
Turing	Turing	k1gInSc1
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgMnPc2d3
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
v	v	k7c4
Bletchley	Bletchley	k1gInPc4
Parku	park	k1gInSc2
luštili	luštit	k5eAaImAgMnP
německé	německý	k2eAgInPc4d1
tajné	tajný	k2eAgInPc4d1
kódy	kód	k1gInPc4
šifrované	šifrovaný	k2eAgInPc4d1
stroji	stroj	k1gInSc6
Enigma	enigma	k1gFnSc1
a	a	k8xC
Tunny	Tunny	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
úsilí	úsilí	k1gNnSc1
bylo	být	k5eAaImAgNnS
velice	velice	k6eAd1
úspěšné	úspěšný	k2eAgNnSc1d1
a	a	k8xC
Angličané	Angličan	k1gMnPc1
měli	mít	k5eAaImAgMnP
po	po	k7c4
větší	veliký	k2eAgFnSc4d2
část	část	k1gFnSc4
války	válka	k1gFnSc2
k	k	k7c3
dispozici	dispozice	k1gFnSc3
„	„	k?
<g/>
tajné	tajný	k2eAgFnSc2d1
<g/>
“	“	k?
nepřátelské	přátelský	k2eNgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
po	po	k7c6
válce	válka	k1gFnSc6
ovšem	ovšem	k9
o	o	k7c6
této	tento	k3xDgFnSc6
své	svůj	k3xOyFgFnSc6
práci	práce	k1gFnSc6
nemohl	moct	k5eNaImAgMnS
mluvit	mluvit	k5eAaImF
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
by	by	kYmCp3nS
tím	ten	k3xDgNnSc7
porušil	porušit	k5eAaPmAgMnS
státní	státní	k2eAgNnSc4d1
tajemství	tajemství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesprávně	správně	k6eNd1
je	být	k5eAaImIp3nS
zmiňován	zmiňován	k2eAgInSc1d1
přímý	přímý	k2eAgInSc1d1
podíl	podíl	k1gInSc1
Turinga	Turing	k1gMnSc2
na	na	k7c4
sestrojení	sestrojení	k1gNnSc4
počítače	počítač	k1gInSc2
Colossus	Colossus	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
k	k	k7c3
němu	on	k3xPp3gMnSc3
nepřímo	přímo	k6eNd1
přispěl	přispět	k5eAaPmAgInS
výzkumem	výzkum	k1gInSc7
kryptoanalýzy	kryptoanalýza	k1gFnSc2
Lorenzovy	Lorenzův	k2eAgFnSc2d1
šifry	šifra	k1gFnSc2
pomocí	pomocí	k7c2
statistických	statistický	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Turing	Turing	k1gInSc1
dlouhodobě	dlouhodobě	k6eAd1
uvažoval	uvažovat	k5eAaImAgInS
o	o	k7c6
možnostech	možnost	k1gFnPc6
inteligentních	inteligentní	k2eAgInPc2d1
strojů	stroj	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
myšlenky	myšlenka	k1gFnSc2
tzv.	tzv.	kA
Turingova	Turingův	k2eAgInSc2d1
testu	test	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
inteligentní	inteligentní	k2eAgFnSc4d1
můžeme	moct	k5eAaImIp1nP
stroj	stroj	k1gInSc1
považovat	považovat	k5eAaImF
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
nejsme	být	k5eNaImIp1nP
schopni	schopen	k2eAgMnPc1d1
odlišit	odlišit	k5eAaPmF
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
výstup	výstup	k1gInSc4
(	(	kIx(
<g/>
například	například	k6eAd1
jeho	jeho	k3xOp3gFnPc4
odpovědi	odpověď	k1gFnPc4
<g/>
)	)	kIx)
od	od	k7c2
výstupu	výstup	k1gInSc2
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byly	být	k5eAaImAgFnP
myšlenky	myšlenka	k1gFnPc1
Turingova	Turingův	k2eAgInSc2d1
stroje	stroj	k1gInSc2
využity	využít	k5eAaPmNgInP
při	při	k7c6
konstrukci	konstrukce	k1gFnSc6
prvních	první	k4xOgMnPc2
počítačů	počítač	k1gMnPc2
řízených	řízený	k2eAgMnPc2d1
programem	program	k1gInSc7
uloženým	uložený	k2eAgInSc7d1
ve	v	k7c6
vnitřní	vnitřní	k2eAgFnSc6d1
paměti	paměť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
počítače	počítač	k1gInSc2
Turing	Turing	k1gInSc1
prakticky	prakticky	k6eAd1
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
teoretickém	teoretický	k2eAgNnSc6d1
vysvětlení	vysvětlení	k1gNnSc6
morfogeneze	morfogeneze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odsouzení	odsouzení	k1gNnSc1
a	a	k8xC
smrt	smrt	k1gFnSc1
</s>
<s>
Busta	busta	k1gFnSc1
Alana	Alan	k1gMnSc2
Turinga	Turinga	k1gFnSc1
<g/>
,	,	kIx,
Univerzita	univerzita	k1gFnSc1
v	v	k7c4
Surrey	Surrea	k1gFnPc4
</s>
<s>
O	o	k7c4
Turingově	Turingově	k1gFnSc4
osobním	osobní	k2eAgInSc6d1
životě	život	k1gInSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
málo	málo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
1952	#num#	k4
se	se	k3xPyFc4
Turing	Turing	k1gInSc1
seznámil	seznámit	k5eAaPmAgInS
s	s	k7c7
devatenáctiletým	devatenáctiletý	k2eAgMnSc7d1
nezaměstnaným	nezaměstnaný	k1gMnSc7
Arnoldem	Arnold	k1gMnSc7
Murrayem	Murray	k1gMnSc7
a	a	k8xC
pozval	pozvat	k5eAaPmAgInS
jej	on	k3xPp3gMnSc4
k	k	k7c3
sobě	se	k3xPyFc3
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
byl	být	k5eAaImAgMnS
dům	dům	k1gInSc4
vykraden	vykraden	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murray	Murraum	k1gNnPc7
sdělil	sdělit	k5eAaPmAgMnS
Turingovi	Turing	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
zlodějem	zloděj	k1gMnSc7
byl	být	k5eAaImAgMnS
pravděpodobně	pravděpodobně	k6eAd1
jeho	jeho	k3xOp3gMnSc1
známý	známý	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
Turing	Turing	k1gInSc1
nahlásil	nahlásit	k5eAaPmAgInS
vloupání	vloupání	k1gNnSc3
na	na	k7c4
policii	policie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
vyšetřování	vyšetřování	k1gNnSc2
se	se	k3xPyFc4
přiznal	přiznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
Murrayem	Murrayum	k1gNnSc7
měl	mít	k5eAaImAgInS
sexuální	sexuální	k2eAgInSc1d1
vztah	vztah	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
byl	být	k5eAaImAgMnS
obviněn	obvinit	k5eAaPmNgMnS
ze	z	k7c2
sexuálního	sexuální	k2eAgInSc2d1
deliktu	delikt	k1gInSc2
(	(	kIx(
<g/>
gross	grossit	k5eAaPmRp2nS
indecency	indecenca	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
čelil	čelit	k5eAaImAgMnS
soudnímu	soudní	k2eAgNnSc3d1
procesu	proces	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgInS
odepřen	odepřen	k2eAgInSc4d1
další	další	k2eAgInSc4d1
přístup	přístup	k1gInSc4
k	k	k7c3
utajovaným	utajovaný	k2eAgFnPc3d1
informacím	informace	k1gFnPc3
a	a	k8xC
tedy	tedy	k9
i	i	k9
jeho	jeho	k3xOp3gFnSc4
účast	účast	k1gFnSc4
na	na	k7c4
šifrování	šifrování	k1gNnSc4
ve	v	k7c6
Vládním	vládní	k2eAgNnSc6d1
komunikačním	komunikační	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
(	(	kIx(
<g/>
GCHQ	GCHQ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišel	přijít	k5eAaPmAgMnS
rovněž	rovněž	k9
o	o	k7c4
možnost	možnost	k1gFnSc4
cestovat	cestovat	k5eAaImF
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
místo	místo	k1gNnSc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
si	se	k3xPyFc3
ale	ale	k9
udržel	udržet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
byl	být	k5eAaImAgMnS
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
a	a	k8xC
musel	muset	k5eAaImAgMnS
volit	volit	k5eAaImF
mezi	mezi	k7c7
(	(	kIx(
<g/>
až	až	k6eAd1
dvouletým	dvouletý	k2eAgMnSc7d1
<g/>
)	)	kIx)
vězením	vězení	k1gNnSc7
a	a	k8xC
probací	probace	k1gFnSc7
–	–	k?
podmíněným	podmíněný	k2eAgNnSc7d1
prominutím	prominutí	k1gNnSc7
trestu	trest	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ovšem	ovšem	k9
bylo	být	k5eAaImAgNnS
vázáno	vázat	k5eAaImNgNnS
na	na	k7c4
podstoupení	podstoupení	k1gNnSc4
hormonální	hormonální	k2eAgFnSc2d1
„	„	k?
<g/>
léčby	léčba	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
organo-therapic	organo-therapic	k1gMnSc1
treatment	treatment	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
pro	pro	k7c4
druhou	druhý	k4xOgFnSc4
možnost	možnost	k1gFnSc4
<g/>
:	:	kIx,
po	po	k7c4
dobu	doba	k1gFnSc4
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
dostával	dostávat	k5eAaImAgMnS
ke	k	k7c3
snížení	snížení	k1gNnSc3
libida	libido	k1gNnSc2
dávky	dávka	k1gFnSc2
syntetického	syntetický	k2eAgInSc2d1
ženského	ženský	k2eAgInSc2d1
hormonu	hormon	k1gInSc2
estrogenu	estrogen	k1gInSc2
diethylstilbestrolu	diethylstilbestrol	k1gInSc2
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
navíc	navíc	k6eAd1
běžně	běžně	k6eAd1
způsoboval	způsobovat	k5eAaImAgInS
gynekomastii	gynekomastie	k1gFnSc4
(	(	kIx(
<g/>
růst	růst	k1gInSc4
prsů	prs	k1gInPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
nevítaný	vítaný	k2eNgInSc1d1
příklad	příklad	k1gInSc1
morfogeneze	morfogeneze	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
zrovna	zrovna	k6eAd1
zabýval	zabývat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1954	#num#	k4
Turing	Turing	k1gInSc1
zemřel	zemřít	k5eAaPmAgInS
na	na	k7c4
otravu	otrava	k1gFnSc4
kyanidem	kyanid	k1gInSc7
draselným	draselný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
napuštěno	napuštěn	k2eAgNnSc4d1
jablko	jablko	k1gNnSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgMnSc2,k3yQgMnSc2,k3yIgMnSc2
trochu	trochu	k6eAd1
snědl	snědnout	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přítomnost	přítomnost	k1gFnSc1
kyanidu	kyanid	k1gInSc2
v	v	k7c6
jablku	jablko	k1gNnSc6
nebyla	být	k5eNaImAgFnS
testována	testovat	k5eAaImNgFnS
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
příčina	příčina	k1gFnSc1
smrti	smrt	k1gFnSc2
byl	být	k5eAaImAgInS
kyanid	kyanid	k1gInSc1
určen	určit	k5eAaPmNgInS
až	až	k6eAd1
při	při	k7c6
pitvě	pitva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
oficiálního	oficiální	k2eAgNnSc2d1
stanoviska	stanovisko	k1gNnSc2
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
sebevraždu	sebevražda	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byly	být	k5eAaImAgFnP
odmítnuty	odmítnut	k2eAgFnPc4d1
spekulace	spekulace	k1gFnPc4
o	o	k7c6
náhodě	náhoda	k1gFnSc6
(	(	kIx(
<g/>
neopatrné	opatrný	k2eNgNnSc1d1
zacházení	zacházení	k1gNnSc1
s	s	k7c7
chemikáliemi	chemikálie	k1gFnPc7
<g/>
)	)	kIx)
nebo	nebo	k8xC
o	o	k7c6
vraždě	vražda	k1gFnSc6
(	(	kIx(
<g/>
politické	politický	k2eAgInPc1d1
<g/>
,	,	kIx,
špionážní	špionážní	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1
omluva	omluva	k1gFnSc1
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2009	#num#	k4
se	se	k3xPyFc4
britský	britský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Gordon	Gordon	k1gMnSc1
Brown	Brown	k1gMnSc1
jménem	jméno	k1gNnSc7
vlády	vláda	k1gFnSc2
omluvil	omluvit	k5eAaPmAgInS
Alanu	Alan	k1gMnSc3
Turingovi	Turing	k1gMnSc3
za	za	k7c2
příkoří	příkoří	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
způsobeno	způsobit	k5eAaPmNgNnS
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgMnS
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
pro	pro	k7c4
homosexualitu	homosexualita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omluvu	omluva	k1gFnSc4
zveřejnil	zveřejnit	k5eAaPmAgInS
list	list	k1gInSc1
The	The	k1gMnSc2
Daily	Daila	k1gMnSc2
Telegraph	Telegraph	k1gMnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Jménem	jméno	k1gNnSc7
britské	britský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
a	a	k8xC
všech	všecek	k3xTgInPc2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
díky	díky	k7c3
Alanově	Alanův	k2eAgFnSc3d1
práci	práce	k1gFnSc3
žijí	žít	k5eAaImIp3nP
svobodně	svobodně	k6eAd1
<g/>
,	,	kIx,
říkám	říkat	k5eAaImIp1nS
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
nám	my	k3xPp1nPc3
to	ten	k3xDgNnSc1
líto	líto	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasloužil	zasloužit	k5eAaPmAgMnS
jste	být	k5eAaImIp2nP
si	se	k3xPyFc3
něco	něco	k3yInSc1
lepšího	dobrý	k2eAgNnSc2d2
<g/>
,	,	kIx,
<g/>
“	“	k?
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
Brown	Brown	k1gInSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Není	být	k5eNaImIp3nS
přehnané	přehnaný	k2eAgNnSc1d1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
bez	bez	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
mimořádného	mimořádný	k2eAgNnSc2d1
přispění	přispění	k1gNnSc2
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
dějiny	dějiny	k1gFnPc1
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
velice	velice	k6eAd1
odlišné	odlišný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
<g/>
,	,	kIx,
za	za	k7c4
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
mu	on	k3xPp3gMnSc3
musíme	muset	k5eAaImIp1nP
být	být	k5eAaImF
vděčni	vděčen	k2eAgMnPc1d1
<g/>
,	,	kIx,
staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
ještě	ještě	k6eAd1
hroznějšího	hrozný	k2eAgNnSc2d2
světla	světlo	k1gNnSc2
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gNnSc7
jednalo	jednat	k5eAaImAgNnS
tak	tak	k9
nelidsky	lidsky	k6eNd1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
udělila	udělit	k5eAaPmAgFnS
britská	britský	k2eAgFnSc1d1
královna	královna	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alanu	alan	k1gInSc2
Turingovi	Turing	k1gMnSc6
královskou	královský	k2eAgFnSc4d1
posmrtnou	posmrtný	k2eAgFnSc4d1
milost	milost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Britský	britský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
spravedlnosti	spravedlnost	k1gFnSc2
Grayling	Grayling	k1gInSc1
k	k	k7c3
milosti	milost	k1gFnSc3
uvedl	uvést	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Jeho	jeho	k3xOp3gFnSc4
genialita	genialita	k1gFnSc1
pomohla	pomoct	k5eAaPmAgFnS
ukončit	ukončit	k5eAaPmF
válku	válka	k1gFnSc4
a	a	k8xC
zachránila	zachránit	k5eAaPmAgFnS
tisíce	tisíc	k4xCgInPc4
životů	život	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
pozdější	pozdní	k2eAgInSc1d2
život	život	k1gInSc1
byl	být	k5eAaImAgInS
zastíněn	zastínit	k5eAaPmNgInS
jeho	jeho	k3xOp3gInPc7
odsouzením	odsouzení	k1gNnSc7
za	za	k7c4
homosexualitu	homosexualita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
rozsudek	rozsudek	k1gInSc4
bychom	by	kYmCp1nP
nyní	nyní	k6eAd1
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
nespravedlivý	spravedlivý	k2eNgInSc4d1
a	a	k8xC
diskriminační	diskriminační	k2eAgInSc4d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgInS
rozsudek	rozsudek	k1gInSc4
odvolán	odvolán	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
si	se	k3xPyFc3
zaslouží	zasloužit	k5eAaPmIp3nS
být	být	k5eAaImF
uznáván	uznávat	k5eAaImNgInS
za	za	k7c4
jeho	jeho	k3xOp3gInPc4
přínosy	přínos	k1gInPc4
ve	v	k7c6
válečném	válečný	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
a	a	k8xC
ve	v	k7c6
vědě	věda	k1gFnSc6
o	o	k7c6
počítačích	počítač	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milost	milost	k1gFnSc1
od	od	k7c2
královny	královna	k1gFnSc2
je	být	k5eAaImIp3nS
adekvátní	adekvátní	k2eAgInSc4d1
hold	hold	k1gInSc4
tomuto	tento	k3xDgMnSc3
skvělému	skvělý	k2eAgMnSc3d1
muži	muž	k1gMnSc3
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
britská	britský	k2eAgFnSc1d1
centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
novou	nový	k2eAgFnSc4d1
bankovku	bankovka	k1gFnSc4
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
nominální	nominální	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
<g/>
,	,	kIx,
tj.	tj.	kA
50	#num#	k4
liber	libra	k1gFnPc2
<g/>
,	,	kIx,
zvolila	zvolit	k5eAaPmAgFnS
Turingův	Turingův	k2eAgInSc4d1
portrét	portrét	k1gInSc4
<g/>
,	,	kIx,
doplněný	doplněný	k2eAgInSc1d1
jeho	jeho	k3xOp3gInSc7
citátem	citát	k1gInSc7
„	„	k?
<g/>
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
předzvěst	předzvěst	k1gFnSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
přijde	přijít	k5eAaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
stín	stín	k1gInSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
bude	být	k5eAaImBp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
Vybrán	vybrán	k2eAgInSc1d1
byl	být	k5eAaImAgInS
ze	z	k7c2
seznamu	seznam	k1gInSc2
200	#num#	k4
tisíc	tisíc	k4xCgInPc2
vědeckých	vědecký	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
britské	britský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
nahradil	nahradit	k5eAaPmAgMnS
vynálezce	vynálezce	k1gMnSc1
parního	parní	k2eAgInSc2d1
stroje	stroj	k1gInSc2
Jamese	Jamese	k1gFnSc1
Watta	Watta	k1gFnSc1
a	a	k8xC
Matthewa	Matthew	k2eAgFnSc1d1
Boultona	Boultona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
plastová	plastový	k2eAgFnSc1d1
a	a	k8xC
hůře	zle	k6eAd2
padělatelná	padělatelný	k2eAgFnSc1d1
bankovka	bankovka	k1gFnSc1
byla	být	k5eAaImAgFnS
bankou	banka	k1gFnSc7
připravována	připravován	k2eAgFnSc1d1
k	k	k7c3
vypuštění	vypuštění	k1gNnSc3
do	do	k7c2
oběhu	oběh	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
počest	počest	k1gFnSc4
Alana	Alan	k1gMnSc2
Turinga	Turing	k1gMnSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1966	#num#	k4
udílena	udílen	k2eAgFnSc1d1
Turingova	Turingův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
informatických	informatický	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
mezinárodní	mezinárodní	k2eAgInPc4d1
svátky	svátek	k1gInPc4
byl	být	k5eAaImAgInS
zařazen	zařazen	k2eAgInSc1d1
Mezinárodní	mezinárodní	k2eAgInSc1d1
den	den	k1gInSc1
počítačů	počítač	k1gMnPc2
-	-	kIx~
25	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1
den	den	k1gInSc1
počítačů	počítač	k1gInPc2
byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
na	na	k7c4
popud	popud	k1gInSc4
anglického	anglický	k2eAgMnSc2d1
matematika	matematik	k1gMnSc2
Alana	Alan	k1gMnSc2
Mathisona	Mathison	k1gMnSc2
Turinga	Turing	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
publikoval	publikovat	k5eAaBmAgMnS
článek	článek	k1gInSc4
„	„	k?
<g/>
On	on	k3xPp3gMnSc1
Computable	Computable	k1gMnSc1
Numbers	Numbersa	k1gFnPc2
<g/>
,	,	kIx,
with	witha	k1gFnPc2
an	an	k?
Application	Application	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Entscheidungsproblem	Entscheidungsprobl	k1gInSc7
<g/>
“	“	k?
dopomohl	dopomoct	k5eAaPmAgMnS
a	a	k8xC
podpořil	podpořit	k5eAaPmAgMnS
výrobu	výroba	k1gFnSc4
dnešních	dnešní	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgFnPc7,k3yRgFnPc7,k3yQgFnPc7
se	se	k3xPyFc4
dnes	dnes	k6eAd1
setkáme	setkat	k5eAaPmIp1nP
na	na	k7c6
každém	každý	k3xTgInSc6
kroku	krok	k1gInSc6
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Newman	Newman	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
H.	H.	kA
A.	A.	kA
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Alan	alan	k1gInSc1
Mathison	Mathison	k1gNnSc1
Turing	Turing	k1gInSc1
<g/>
.	.	kIx.
1912	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biographical	Biographical	k1gFnSc1
Memoirs	Memoirsa	k1gFnPc2
of	of	k?
Fellows	Fellows	k1gInSc1
of	of	k?
the	the	k?
Royal	Royal	k1gInSc1
Society	societa	k1gFnSc2
1	#num#	k4
<g/>
:	:	kIx,
253	#num#	k4
<g/>
–	–	k?
<g/>
226	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rsbm	rsbm	k6eAd1
<g/>
.1955	.1955	k4
<g/>
.0019	.0019	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
JSTOR	JSTOR	kA
769256.1	769256.1	k4
2	#num#	k4
MAREŠ	Mareš	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slova	slovo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
hodí	hodit	k5eAaImIp3nP,k5eAaPmIp3nP
aneb	aneb	k?
jak	jak	k6eAd1
si	se	k3xPyFc3
povídat	povídat	k5eAaImF
o	o	k7c6
matematice	matematika	k1gFnSc6
<g/>
,	,	kIx,
kybernetice	kybernetika	k1gFnSc6
a	a	k8xC
informatice	informatika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1445	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Se	s	k7c7
složitostí	složitost	k1gFnSc7
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
jednoduché	jednoduchý	k2eAgNnSc1d1
<g/>
,	,	kIx,
s.	s.	k?
303	#num#	k4
<g/>
-	-	kIx~
<g/>
309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GANNON	GANNON	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Colossus	Colossus	k1gInSc1
<g/>
:	:	kIx,
Bletchley	Bletchlea	k1gFnSc2
Park	park	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Greatest	Greatest	k1gMnSc1
Secret	Secret	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Atlantic	Atlantice	k1gFnPc2
Books	Books	k1gInSc1
<g/>
,	,	kIx,
c	c	k0
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84354	#num#	k4
<g/>
-	-	kIx~
<g/>
331	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KAPOUN	kapoun	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průkopníci	průkopník	k1gMnPc1
informačního	informační	k2eAgInSc2d1
věku	věk	k1gInSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
19.8	19.8	k4
<g/>
.2010	.2010	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
A.	A.	kA
Hodges	Hodges	k1gInSc1
<g/>
:	:	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
472	#num#	k4
<g/>
↑	↑	k?
http://www.turing.org.uk/bio/part8.html	http://www.turing.org.uk/bio/part8.html	k1gMnSc1
<g/>
↑	↑	k?
Když	když	k8xS
se	se	k3xPyFc4
DES	des	k1gNnSc1
změní	změnit	k5eAaPmIp3nS
v	v	k7c4
děs	děs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chyba	chyba	k1gFnSc1
lékařů	lékař	k1gMnPc2
ohrozila	ohrozit	k5eAaPmAgFnS
generace	generace	k1gFnSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2020	#num#	k4
<g/>
,	,	kIx,
Zdraví	zdraví	k1gNnSc1
„	„	k?
<g/>
v	v	k7c6
cajku	cajk	k1gInSc6
<g/>
“	“	k?
<g/>
↑	↑	k?
Britský	britský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
se	se	k3xPyFc4
omluvil	omluvit	k5eAaPmAgMnS
kastrovanému	kastrovaný	k2eAgMnSc3d1
hrdinovi	hrdina	k1gMnSc3
války	válka	k1gFnSc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Gordon	Gordon	k1gMnSc1
Brown	Brown	k1gMnSc1
<g/>
:	:	kIx,
I	I	kA
<g/>
'	'	kIx"
<g/>
m	m	kA
proud	proud	k1gInSc1
to	ten	k3xDgNnSc1
say	say	k?
sorry	sorry	k9
to	ten	k3xDgNnSc4
a	a	k8xC
real	real	k1gMnSc1
war	war	k?
hero	hero	k1gMnSc1
↑	↑	k?
Pozdní	pozdní	k2eAgInSc1d1
vánoční	vánoční	k2eAgInSc1d1
dárek	dárek	k1gInSc1
–	–	k?
posmrtný	posmrtný	k2eAgInSc1d1
pardon	pardon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Literární	literární	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BARET	baret	k1gInSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnému	slavný	k2eAgMnSc3d1
Turingovi	Turing	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
rozluštil	rozluštit	k5eAaPmAgMnS
Enigma	enigma	k1gNnSc4
kód	kód	k1gInSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
udělena	udělit	k5eAaPmNgFnS
až	až	k9
teď	teď	k6eAd1
milost	milost	k1gFnSc4
za	za	k7c4
jeho	jeho	k3xOp3gFnSc4
„	„	k?
<g/>
homosexualitu	homosexualita	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-12-25	2013-12-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
8991	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zakladatel	zakladatel	k1gMnSc1
počítačové	počítačový	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
válečný	válečný	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nové	nový	k2eAgFnSc6d1
britské	britský	k2eAgFnSc6d1
bankovce	bankovka	k1gFnSc6
bude	být	k5eAaImBp3nS
podobizna	podobizna	k1gFnSc1
Alana	Alan	k1gMnSc2
Turinga	Turing	k1gMnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LAUDER	LAUDER	kA
<g/>
,	,	kIx,
Silvie	Silvie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
padesátilibrovce	padesátilibrovka	k1gFnSc6
bude	být	k5eAaImBp3nS
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
<g/>
,	,	kIx,
hrdina	hrdina	k1gMnSc1
dohnaný	dohnaný	k2eAgMnSc1d1
k	k	k7c3
sebevraždě	sebevražda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týdeník	týdeník	k1gInSc1
Respekt	respekt	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2019-07-15	2019-07-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
David	David	k1gMnSc1
Leavitt	Leavitt	k1gMnSc1
<g/>
:	:	kIx,
Muž	muž	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
věděl	vědět	k5eAaImAgMnS
příliš	příliš	k6eAd1
mnoho	mnoho	k6eAd1
:	:	kIx,
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
a	a	k8xC
vynález	vynález	k1gInSc1
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argo	Argo	k6eAd1
a	a	k8xC
Dokořán	dokořán	k6eAd1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
900	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Andrew	Andrew	k1gMnSc1
Hodges	Hodges	k1gMnSc1
<g/>
:	:	kIx,
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Enigma	enigma	k1gFnSc1
<g/>
,	,	kIx,
Vintage	Vintage	k1gFnSc1
edition	edition	k1gInSc1
1992	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
:	:	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Burnett	Burnett	k2eAgInSc1d1
Books	Books	k1gInSc1
Ltd	ltd	kA
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
911641	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
David	David	k1gMnSc1
Leavitt	Leavitt	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Man	Man	k1gMnSc1
Who	Who	k1gMnSc1
Knew	Knew	k1gMnSc1
Too	Too	k1gMnSc1
Much	moucha	k1gFnPc2
:	:	kIx,
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
and	and	k?
the	the	k?
Invetion	Invetion	k1gInSc1
of	of	k?
the	the	k?
Computer	computer	k1gInSc1
<g/>
,	,	kIx,
Phoenix	Phoenix	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
<g/>
:	:	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7538	#num#	k4
<g/>
-	-	kIx~
<g/>
2200	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
audiokniha	audiokniha	k1gMnSc1
Přiznání	přiznání	k1gNnSc2
Alana	Alan	k1gMnSc2
Turinga	Turing	k1gMnSc2
<g/>
,	,	kIx,
vydala	vydat	k5eAaPmAgFnS
Audiotéka	Audiotéka	k1gFnSc1
<g/>
,	,	kIx,
načetl	načíst	k5eAaPmAgMnS,k5eAaBmAgMnS
Miroslav	Miroslav	k1gMnSc1
Táborský	Táborský	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
informatiky	informatika	k1gFnSc2
</s>
<s>
Turingův	Turingův	k2eAgInSc1d1
stroj	stroj	k1gInSc1
</s>
<s>
Turingův	Turingův	k2eAgInSc1d1
test	test	k1gInSc1
</s>
<s>
Turingova	Turingův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
</s>
<s>
Bomba	bomba	k1gFnSc1
(	(	kIx(
<g/>
počítač	počítač	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Marian	Marian	k1gMnSc1
Rejewski	Rejewsk	k1gFnSc2
–	–	k?
polský	polský	k2eAgMnSc1d1
kryptoanalytik	kryptoanalytik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
prolomil	prolomit	k5eAaPmAgMnS
Enigmu	enigma	k1gFnSc4
před	před	k7c7
vypuknutím	vypuknutí	k1gNnSc7
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Wilhelm	Wilhelm	k1gMnSc1
Tranow	Tranow	k1gMnSc1
–	–	k?
německý	německý	k2eAgMnSc1d1
kryptoanalytik	kryptoanalytik	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
za	za	k7c7
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
prolomil	prolomit	k5eAaPmAgMnS
britské	britský	k2eAgFnPc4d1
šifry	šifra	k1gFnPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
otec	otec	k1gMnSc1
moderních	moderní	k2eAgInPc2d1
počítačů	počítač	k1gMnPc2
Alan	Alan	k1gMnSc1
Turing	Turing	k1gInSc1
<g/>
,	,	kIx,
ČT24	ČT24	k1gFnSc1
23	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
</s>
<s>
FENCL	Fencl	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
:	:	kIx,
OSOBNOST	osobnost	k1gFnSc1
<g/>
:	:	kIx,
55	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
sebevraždy	sebevražda	k1gFnSc2
matematika	matematik	k1gMnSc2
<g/>
,	,	kIx,
Neviditelný	viditelný	k2eNgMnSc1d1
pes	pes	k1gMnSc1
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2009	#num#	k4
</s>
<s>
TRONNER	TRONNER	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
<g/>
:	:	kIx,
Genius	genius	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
matematicky	matematicky	k6eAd1
stvořil	stvořit	k5eAaPmAgMnS
počítač	počítač	k1gInSc4
<g/>
,	,	kIx,
Živě	živě	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Enigma	enigma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Website	Websit	k1gInSc5
maintained	maintained	k1gInSc1
by	by	kYmCp3nS
biographer	biographra	k1gFnPc2
Andrew	Andrew	k1gMnSc1
Hodges	Hodges	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Alan	alan	k1gInSc1
Turing	Turing	k1gInSc1
-	-	kIx~
Bibliography	Bibliographa	k1gFnPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInPc4d1
svátky	svátek	k1gInPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990008646	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118802976	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1058	#num#	k4
9902	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83171546	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
41887917	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83171546	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Matematika	matematika	k1gFnSc1
</s>
