<s>
Osmiridium	osmiridium	k1gNnSc1	osmiridium
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
iridosmium	iridosmium	k1gNnSc4	iridosmium
<g/>
,	,	kIx,	,
zastarale	zastarale	k6eAd1	zastarale
něvjanskit	něvjanskit	k5eAaPmF	něvjanskit
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácná	vzácný	k2eAgFnSc1d1	vzácná
přírodní	přírodní	k2eAgFnSc3d1	přírodní
korozi	koroze	k1gFnSc3	koroze
odolná	odolný	k2eAgFnSc1d1	odolná
slitina	slitina	k1gFnSc1	slitina
osmia	osmium	k1gNnSc2	osmium
a	a	k8xC	a
iridia	iridium	k1gNnSc2	iridium
se	s	k7c7	s
stopami	stopa	k1gFnPc7	stopa
dalších	další	k2eAgMnPc2d1	další
kovových	kovový	k2eAgMnPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
platinové	platinový	k2eAgFnSc2d1	platinová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
prvky	prvek	k1gInPc1	prvek
ruthenium	ruthenium	k1gNnSc1	ruthenium
<g/>
,	,	kIx,	,
rhodium	rhodium	k1gNnSc1	rhodium
<g/>
,	,	kIx,	,
palladium	palladium	k1gNnSc1	palladium
<g/>
,	,	kIx,	,
osmium	osmium	k1gNnSc1	osmium
<g/>
,	,	kIx,	,
iridium	iridium	k1gNnSc1	iridium
a	a	k8xC	a
platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
přirozené	přirozený	k2eAgNnSc1d1	přirozené
<g/>
,	,	kIx,	,
či	či	k8xC	či
uměle	uměle	k6eAd1	uměle
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
slitinu	slitina	k1gFnSc4	slitina
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
hustotou	hustota	k1gFnSc7	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Osmiridium	osmiridium	k1gNnSc1	osmiridium
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
rozpoznáno	rozpoznat	k5eAaPmNgNnS	rozpoznat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
coby	coby	k?	coby
nežádoucí	žádoucí	k2eNgFnSc1d1	nežádoucí
příměs	příměs	k1gFnSc1	příměs
ve	v	k7c6	v
zlatonosných	zlatonosný	k2eAgFnPc6d1	zlatonosná
půdách	půda	k1gFnPc6	půda
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
.	.	kIx.	.
</s>
<s>
Osmium	osmium	k1gNnSc1	osmium
a	a	k8xC	a
iridium	iridium	k1gNnSc1	iridium
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
kovy	kov	k1gInPc1	kov
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dva	dva	k4xCgInPc4	dva
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
hustotou	hustota	k1gFnSc7	hustota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Osmiridium	osmiridium	k1gNnSc1	osmiridium
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
50	[number]	k4	50
%	%	kIx~	%
iridia	iridium	k1gNnSc2	iridium
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
iridosmium	iridosmium	k1gNnSc1	iridosmium
ho	on	k3xPp3gMnSc4	on
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
70	[number]	k4	70
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Osmiridium	osmiridium	k1gNnSc1	osmiridium
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
však	však	k9	však
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nP	těžet
jiné	jiný	k2eAgInPc1d1	jiný
kovy	kov	k1gInPc1	kov
platinové	platinový	k2eAgFnSc2d1	platinová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
izolovat	izolovat	k5eAaBmF	izolovat
pomocí	pomocí	k7c2	pomocí
lučavky	lučavka	k1gFnSc2	lučavka
královské	královský	k2eAgFnSc2d1	královská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
platinu	platina	k1gFnSc4	platina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoli	nikoli	k9	nikoli
osmiridium	osmiridium	k1gNnSc1	osmiridium
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malých	malý	k2eAgInPc2d1	malý
<g/>
,	,	kIx,	,
nesmírně	smírně	k6eNd1	smírně
tvrdých	tvrdý	k2eAgNnPc2d1	tvrdé
kovových	kovový	k2eAgNnPc2d1	kovové
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
šesterečnou	šesterečný	k2eAgFnSc4d1	šesterečná
krystalovou	krystalový	k2eAgFnSc4d1	krystalová
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
Iridium	iridium	k1gNnSc4	iridium
Osmium	osmium	k1gNnSc4	osmium
Mindat	Mindat	k1gFnSc2	Mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
-	-	kIx~	-
Osmiridium	osmiridium	k1gNnSc1	osmiridium
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Osmiridium	osmiridium	k1gNnSc4	osmiridium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
