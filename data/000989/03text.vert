<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
Bosco	Bosco	k1gMnSc1	Bosco
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Don	Don	k1gMnSc1	Don
Bosco	Bosco	k1gMnSc1	Bosco
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
česky	česky	k6eAd1	česky
psán	psán	k2eAgInSc1d1	psán
jako	jako	k8xC	jako
Bosko	bosko	k6eAd1	bosko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Giovanni	Giovanň	k1gMnSc5	Giovanň
Melchiorre	Melchiorr	k1gMnSc5	Melchiorr
Bosco	Bosca	k1gMnSc5	Bosca
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
Castelnuovo	Castelnuovo	k1gNnSc1	Castelnuovo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Asti	Ast	k1gFnSc2	Ast
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Castelnuovo	Castelnuovo	k1gNnSc1	Castelnuovo
Don	dona	k1gFnPc2	dona
Bosco	Bosco	k1gNnSc1	Bosco
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Turín	Turín	k1gInSc1	Turín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
světec	světec	k1gMnSc1	světec
<g/>
,	,	kIx,	,
vychovatel	vychovatel	k1gMnSc1	vychovatel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
první	první	k4xOgFnSc1	první
hlava	hlava	k1gFnSc1	hlava
Salesiánů	salesián	k1gMnPc2	salesián
Dona	Don	k1gMnSc4	Don
Bosca	Boscus	k1gMnSc4	Boscus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
prohlášen	prohlášen	k2eAgMnSc1d1	prohlášen
za	za	k7c4	za
Otce	otec	k1gMnSc4	otec
a	a	k8xC	a
učitele	učitel	k1gMnSc4	učitel
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgInSc2d1	oficiální
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
matrice	matrika	k1gFnSc6	matrika
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Bosco	Bosco	k1gMnSc1	Bosco
narodil	narodit	k5eAaPmAgMnS	narodit
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1815	[number]	k4	1815
v	v	k7c6	v
Becchi	Becch	k1gFnSc6	Becch
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
kantonů	kanton	k1gInPc2	kanton
Morialda	Morialdo	k1gNnSc2	Morialdo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
Castelnuova	Castelnuovo	k1gNnSc2	Castelnuovo
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Asti	Ast	k1gFnSc2	Ast
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Castelnuovo	Castelnuovo	k1gNnSc1	Castelnuovo
Don	dona	k1gFnPc2	dona
Bosco	Bosco	k6eAd1	Bosco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
však	však	k9	však
vždy	vždy	k6eAd1	vždy
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
-	-	kIx~	-
tedy	tedy	k9	tedy
v	v	k7c4	v
den	den	k1gInSc4	den
slavnosti	slavnost	k1gFnSc2	slavnost
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Boscův	Boscův	k2eAgMnSc1d1	Boscův
otec	otec	k1gMnSc1	otec
František	František	k1gMnSc1	František
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Markéta	Markéta	k1gFnSc1	Markéta
byli	být	k5eAaImAgMnP	být
rolníci	rolník	k1gMnPc1	rolník
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k6eAd1	Bosco
měl	mít	k5eAaImAgInS	mít
ještě	ještě	k9	ještě
dva	dva	k4xCgInPc4	dva
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
-	-	kIx~	-
Antonína	Antonín	k1gMnSc4	Antonín
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
měl	mít	k5eAaImAgMnS	mít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
a	a	k8xC	a
Josefa	Josef	k1gMnSc4	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
Bosca	Bosca	k1gFnSc1	Bosca
tak	tak	k6eAd1	tak
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
jen	jen	k9	jen
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
otce	otec	k1gMnSc2	otec
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
rodinu	rodina	k1gFnSc4	rodina
velkou	velký	k2eAgFnSc7d1	velká
tragédií	tragédie	k1gFnSc7	tragédie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
navíc	navíc	k6eAd1	navíc
sucho	sucho	k6eAd1	sucho
zničilo	zničit	k5eAaPmAgNnS	zničit
úrodu	úroda	k1gFnSc4	úroda
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
uživit	uživit	k5eAaPmF	uživit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
chtěla	chtít	k5eAaImAgFnS	chtít
ho	on	k3xPp3gInSc4	on
matka	matka	k1gFnSc1	matka
poslat	poslat	k5eAaPmF	poslat
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejstarší	starý	k2eAgMnSc1d3	nejstarší
bratr	bratr	k1gMnSc1	bratr
Antonín	Antonín	k1gMnSc1	Antonín
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
Bosco	Bosco	k1gMnSc1	Bosco
školu	škola	k1gFnSc4	škola
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
chodil	chodit	k5eAaImAgMnS	chodit
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měl	mít	k5eAaImAgMnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
mystický	mystický	k2eAgInSc4d1	mystický
zážitek	zážitek	k1gInSc4	zážitek
-	-	kIx~	-
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
zjevil	zjevit	k5eAaPmAgMnS	zjevit
muž	muž	k1gMnSc1	muž
zahalený	zahalený	k2eAgMnSc1d1	zahalený
v	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
plášti	plášť	k1gInSc6	plášť
s	s	k7c7	s
oslnivě	oslnivě	k6eAd1	oslnivě
zářícím	zářící	k2eAgInSc7d1	zářící
obličejem	obličej	k1gInSc7	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
matka	matka	k1gFnSc1	matka
toho	ten	k3xDgMnSc2	ten
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
se	se	k3xPyFc4	se
s	s	k7c7	s
Boscem	Bosec	k1gInSc7	Bosec
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
vysvětlili	vysvětlit	k5eAaPmAgMnP	vysvětlit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nP	muset
krotit	krotit	k5eAaImF	krotit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
chlapce	chlapec	k1gMnPc4	chlapec
a	a	k8xC	a
vychovávat	vychovávat	k5eAaImF	vychovávat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
chlapce	chlapec	k1gMnSc4	chlapec
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
Bosco	Bosco	k6eAd1	Bosco
různá	různý	k2eAgNnPc4d1	různé
akrobatická	akrobatický	k2eAgNnPc4d1	akrobatické
čísla	číslo	k1gNnPc4	číslo
a	a	k8xC	a
pořádal	pořádat	k5eAaImAgInS	pořádat
pro	pro	k7c4	pro
chlapce	chlapec	k1gMnSc4	chlapec
představení	představení	k1gNnSc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
však	však	k9	však
představení	představení	k1gNnSc1	představení
začalo	začít	k5eAaPmAgNnS	začít
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
společné	společný	k2eAgFnSc3d1	společná
modlitbě	modlitba	k1gFnSc3	modlitba
a	a	k8xC	a
zpěvu	zpěv	k1gInSc3	zpěv
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
kázání	kázání	k1gNnSc4	kázání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
slyšel	slyšet	k5eAaImAgMnS	slyšet
na	na	k7c6	na
mši	mše	k1gFnSc6	mše
svaté	svatý	k2eAgFnSc6d1	svatá
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
díky	dík	k1gInPc1	dík
své	svůj	k3xOyFgFnSc2	svůj
výborné	výborný	k2eAgFnSc2d1	výborná
paměti	paměť	k1gFnSc2	paměť
slovo	slovo	k1gNnSc1	slovo
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
pamatoval	pamatovat	k5eAaImAgInS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k6eAd1	Bosco
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
účastnil	účastnit	k5eAaImAgMnS	účastnit
programu	program	k1gInSc3	program
misionářů	misionář	k1gMnPc2	misionář
v	v	k7c6	v
Buttiglieře	Buttigliera	k1gFnSc6	Buttigliera
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
dal	dát	k5eAaPmAgMnS	dát
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
kněz	kněz	k1gMnSc1	kněz
Don	Don	k1gMnSc1	Don
Calosso	Calossa	k1gFnSc5	Calossa
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
si	se	k3xPyFc3	se
Bosco	Bosco	k6eAd1	Bosco
z	z	k7c2	z
kázání	kázání	k1gNnSc2	kázání
něco	něco	k3yInSc4	něco
zapamatoval	zapamatovat	k5eAaPmAgMnS	zapamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
Bosco	Bosco	k6eAd1	Bosco
odříkal	odříkat	k5eAaPmAgInS	odříkat
přesně	přesně	k6eAd1	přesně
celé	celý	k2eAgNnSc4d1	celé
kázání	kázání	k1gNnSc4	kázání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Calosso	Calossa	k1gFnSc5	Calossa
ohromen	ohromit	k5eAaPmNgMnS	ohromit
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zajistí	zajistit	k5eAaPmIp3nS	zajistit
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
školy	škola	k1gFnSc2	škola
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
dvacet	dvacet	k4xCc4	dvacet
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
domova	domov	k1gInSc2	domov
a	a	k8xC	a
docházení	docházení	k1gNnSc1	docházení
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
učil	učít	k5eAaPmAgMnS	učít
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
již	již	k6eAd1	již
uměl	umět	k5eAaImAgMnS	umět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
do	do	k7c2	do
Chieri	Chier	k1gFnSc2	Chier
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
studovat	studovat	k5eAaImF	studovat
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
rovnou	rovnou	k6eAd1	rovnou
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
spolužáků	spolužák	k1gMnPc2	spolužák
nejstarší	starý	k2eAgInSc1d3	nejstarší
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
studia	studio	k1gNnSc2	studio
měl	mít	k5eAaImAgMnS	mít
školu	škola	k1gFnSc4	škola
vždy	vždy	k6eAd1	vždy
bezplatně	bezplatně	k6eAd1	bezplatně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dostával	dostávat	k5eAaImAgMnS	dostávat
stipendium	stipendium	k1gNnSc4	stipendium
-	-	kIx~	-
jako	jako	k8xS	jako
žák	žák	k1gMnSc1	žák
s	s	k7c7	s
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
prospěchem	prospěch	k1gInSc7	prospěch
ze	z	k7c2	z
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gNnSc2	on
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
skupina	skupina	k1gFnSc1	skupina
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yQgFnPc7	který
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
modlil	modlit	k5eAaImAgMnS	modlit
a	a	k8xC	a
rozmlouval	rozmlouvat	k5eAaImAgMnS	rozmlouvat
nad	nad	k7c7	nad
věroučnými	věroučný	k2eAgFnPc7d1	věroučná
otázkami	otázka	k1gFnPc7	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
stát	stát	k1gInSc1	stát
se	s	k7c7	s
knězem	kněz	k1gMnSc7	kněz
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysvěcení	vysvěcení	k1gNnSc6	vysvěcení
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
začala	začít	k5eAaPmAgFnS	začít
Boscova	Boscův	k2eAgFnSc1d1	Boscova
hlavní	hlavní	k2eAgFnSc1d1	hlavní
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
duchovním	duchovní	k2eAgMnSc7d1	duchovní
patronem	patron	k1gMnSc7	patron
donem	don	k1gMnSc7	don
Cafassem	Cafass	k1gMnSc7	Cafass
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
blahořečeným	blahořečený	k2eAgMnPc3d1	blahořečený
<g/>
)	)	kIx)	)
započal	započnout	k5eAaPmAgInS	započnout
svou	svůj	k3xOyFgFnSc4	svůj
kněžskou	kněžský	k2eAgFnSc4d1	kněžská
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
a	a	k8xC	a
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
útulku	útulek	k1gInSc6	útulek
a	a	k8xC	a
nemocnici	nemocnice	k1gFnSc6	nemocnice
svaté	svatý	k2eAgFnSc2d1	svatá
Filomény	Filoména	k1gFnSc2	Filoména
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
chlapci	chlapec	k1gMnPc1	chlapec
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
jich	on	k3xPp3gInPc2	on
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
skupině	skupina	k1gFnSc6	skupina
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc1	sto
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
těsná	těsný	k2eAgFnSc1d1	těsná
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
Bosco	Bosco	k6eAd1	Bosco
hledal	hledat	k5eAaImAgMnS	hledat
nové	nový	k2eAgMnPc4d1	nový
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
oratoř	oratoř	k1gInSc1	oratoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měl	mít	k5eAaImAgInS	mít
další	další	k2eAgInSc4d1	další
mystický	mystický	k2eAgInSc4d1	mystický
zážitek	zážitek	k1gInSc4	zážitek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ho	on	k3xPp3gMnSc4	on
nasměroval	nasměrovat	k5eAaPmAgInS	nasměrovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
dalším	další	k2eAgNnSc6d1	další
působení	působení	k1gNnSc6	působení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
našel	najít	k5eAaPmAgMnS	najít
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
kapli	kaple	k1gFnSc4	kaple
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1844	[number]	k4	1844
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
oratoř	oratoř	k1gFnSc4	oratoř
-	-	kIx~	-
oratoř	oratoř	k1gInSc1	oratoř
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
Saleského	Saleský	k2eAgMnSc2d1	Saleský
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k6eAd1	Bosco
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
především	především	k9	především
na	na	k7c4	na
problematické	problematický	k2eAgMnPc4d1	problematický
mladíky	mladík	k1gMnPc4	mladík
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
apod.	apod.	kA	apod.
Přijal	přijmout	k5eAaPmAgInS	přijmout
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
však	však	k9	však
i	i	k9	i
několik	několik	k4yIc1	několik
vzorných	vzorný	k2eAgMnPc2d1	vzorný
a	a	k8xC	a
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
ostatním	ostatní	k2eAgInSc7d1	ostatní
příkladem	příklad	k1gInSc7	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
chlapců	chlapec	k1gMnPc2	chlapec
bylo	být	k5eAaImAgNnS	být
negramotných	gramotný	k2eNgFnPc2d1	negramotná
a	a	k8xC	a
málo	málo	k4c1	málo
vzdělaných	vzdělaný	k2eAgMnPc2d1	vzdělaný
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Bosco	Bosco	k6eAd1	Bosco
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
bude	být	k5eAaImBp3nS	být
sám	sám	k3xTgMnSc1	sám
vyučovat	vyučovat	k5eAaImF	vyučovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
napsal	napsat	k5eAaBmAgInS	napsat
i	i	k9	i
několik	několik	k4yIc4	několik
učebnic	učebnice	k1gFnPc2	učebnice
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
i	i	k9	i
večerní	večerní	k2eAgFnSc4d1	večerní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gMnSc1	Bosco
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
několik	několik	k4yIc4	několik
pomocníků	pomocník	k1gMnPc2	pomocník
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pečovateli	pečovatel	k1gMnPc7	pečovatel
stávali	stávat	k5eAaImAgMnP	stávat
i	i	k9	i
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
oratoř	oratoř	k1gFnSc1	oratoř
vychovala	vychovat	k5eAaPmAgFnS	vychovat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chlapců	chlapec	k1gMnPc2	chlapec
stále	stále	k6eAd1	stále
narůstal	narůstat	k5eAaImAgInS	narůstat
a	a	k8xC	a
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
založeny	založit	k5eAaPmNgInP	založit
další	další	k2eAgInPc1d1	další
oratoře	oratoř	k1gInPc1	oratoř
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
svatému	svatý	k1gMnSc3	svatý
Aloisovi	Alois	k1gMnSc3	Alois
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
třetí	třetí	k4xOgNnSc1	třetí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
Andělu	Anděl	k1gMnSc3	Anděl
strážnému	strážný	k1gMnSc3	strážný
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
se	se	k3xPyFc4	se
oratoř	oratoř	k1gFnSc1	oratoř
setkávala	setkávat	k5eAaImAgFnS	setkávat
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
donutily	donutit	k5eAaPmAgFnP	donutit
změnit	změnit	k5eAaPmF	změnit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
hned	hned	k6eAd1	hned
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gMnSc1	Bosco
musel	muset	k5eAaImAgMnS	muset
dokonce	dokonce	k9	dokonce
pronajmout	pronajmout	k5eAaPmF	pronajmout
louku	louka	k1gFnSc4	louka
a	a	k8xC	a
scházeli	scházet	k5eAaImAgMnP	scházet
se	se	k3xPyFc4	se
alespoň	alespoň	k9	alespoň
venku	venku	k6eAd1	venku
<g/>
.	.	kIx.	.
</s>
<s>
Chlapců	chlapec	k1gMnPc2	chlapec
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
vyvolání	vyvolání	k1gNnSc2	vyvolání
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgMnSc1d1	policejní
náčelník	náčelník	k1gMnSc1	náčelník
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Boscovi	Bosec	k1gMnSc3	Bosec
oratoř	oratoř	k1gInSc4	oratoř
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
,	,	kIx,	,
majitelé	majitel	k1gMnPc1	majitel
louky	louka	k1gFnSc2	louka
přestali	přestat	k5eAaPmAgMnP	přestat
Boscovi	Boscův	k2eAgMnPc1d1	Boscův
louku	louka	k1gFnSc4	louka
pronajímat	pronajímat	k5eAaImF	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
oratoři	oratoř	k1gInSc6	oratoř
se	se	k3xPyFc4	se
však	však	k9	však
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
Francesko	Francesko	k1gNnSc4	Francesko
Pinardi	Pinard	k1gMnPc1	Pinard
a	a	k8xC	a
prodal	prodat	k5eAaPmAgMnS	prodat
Boscovi	Bosec	k1gMnSc3	Bosec
kostelík	kostelík	k1gInSc4	kostelík
<g/>
.	.	kIx.	.
</s>
<s>
Malou	malý	k2eAgFnSc4d1	malá
kapli	kaple	k1gFnSc4	kaple
vysvětili	vysvětit	k5eAaPmAgMnP	vysvětit
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgMnSc1d1	městský
markýz	markýz	k1gMnSc1	markýz
Cavour	Cavour	k1gMnSc1	Cavour
však	však	k9	však
považoval	považovat	k5eAaImAgMnS	považovat
shromáždění	shromáždění	k1gNnSc4	shromáždění
za	za	k7c4	za
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
příkaz	příkaz	k1gInSc1	příkaz
ho	on	k3xPp3gMnSc4	on
rozpustit	rozpustit	k5eAaPmF	rozpustit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
příkaz	příkaz	k1gInSc4	příkaz
však	však	k9	však
zrušil	zrušit	k5eAaPmAgMnS	zrušit
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
Albert	Albert	k1gMnSc1	Albert
Sardinský	sardinský	k2eAgMnSc1d1	sardinský
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
pouze	pouze	k6eAd1	pouze
příkaz	příkaz	k1gInSc4	příkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
shromáždění	shromáždění	k1gNnSc4	shromáždění
hlídali	hlídat	k5eAaImAgMnP	hlídat
strážníci	strážník	k1gMnPc1	strážník
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
markýz	markýz	k1gMnSc1	markýz
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
strážníci	strážník	k1gMnPc1	strážník
byli	být	k5eAaImAgMnP	být
odvoláni	odvolat	k5eAaPmNgMnP	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
vydal	vydat	k5eAaPmAgMnS	vydat
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
Albert	Albert	k1gMnSc1	Albert
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
udělovala	udělovat	k5eAaImAgFnS	udělovat
svobodu	svoboda	k1gFnSc4	svoboda
židům	žid	k1gMnPc3	žid
a	a	k8xC	a
protestantům	protestant	k1gMnPc3	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gMnSc1	Bosco
popsal	popsat	k5eAaPmAgMnS	popsat
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
situaci	situace	k1gFnSc4	situace
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
je	být	k5eAaImIp3nS	být
poskytnuta	poskytnut	k2eAgFnSc1d1	poskytnuta
i	i	k9	i
svoboda	svoboda	k1gFnSc1	svoboda
dělat	dělat	k5eAaImF	dělat
dobré	dobrý	k2eAgNnSc1d1	dobré
či	či	k8xC	či
zlé	zlý	k2eAgNnSc1d1	zlé
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
komu	kdo	k3yInSc3	kdo
zlíbí	zlíbit	k5eAaPmIp3nS	zlíbit
<g/>
...	...	k?	...
V	v	k7c6	v
těch	ten	k3xDgInPc6	ten
dnech	den	k1gInPc6	den
mladé	mladý	k2eAgFnSc2d1	mladá
lidi	člověk	k1gMnPc4	člověk
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
jakési	jakýsi	k3yIgNnSc1	jakýsi
šílenství	šílenství	k1gNnSc1	šílenství
<g/>
.	.	kIx.	.
</s>
<s>
Shromažďovali	shromažďovat	k5eAaImAgMnP	shromažďovat
se	se	k3xPyFc4	se
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
a	a	k8xC	a
napadali	napadat	k5eAaBmAgMnP	napadat
kněze	kněz	k1gMnSc4	kněz
a	a	k8xC	a
kostely	kostel	k1gInPc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
urážka	urážka	k1gFnSc1	urážka
náboženství	náboženství	k1gNnSc2	náboženství
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
"	"	kIx"	"
<g/>
vydařený	vydařený	k2eAgInSc4d1	vydařený
kousek	kousek	k1gInSc4	kousek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Bosco	Bosco	k1gMnSc1	Bosco
byl	být	k5eAaImAgMnS	být
několikrát	několikrát	k6eAd1	několikrát
napaden	napadnout	k5eAaPmNgMnS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
byly	být	k5eAaImAgInP	být
i	i	k9	i
páchány	páchán	k2eAgInPc1d1	páchán
atentáty	atentát	k1gInPc1	atentát
(	(	kIx(	(
<g/>
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
zastřelení	zastřelení	k1gNnSc4	zastřelení
<g/>
,	,	kIx,	,
otrávení	otrávení	k1gNnSc1	otrávení
<g/>
,	,	kIx,	,
umlácení	umlácení	k1gNnSc1	umlácení
<g/>
,	,	kIx,	,
ubodání	ubodání	k1gNnSc1	ubodání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
však	však	k9	však
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
Piemontu	Piemont	k1gInSc2	Piemont
proti	proti	k7c3	proti
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
r.	r.	kA	r.
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
otřes	otřes	k1gInSc4	otřes
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1	veřejná
školy	škola	k1gFnPc1	škola
byly	být	k5eAaImAgFnP	být
zavřeny	zavřít	k5eAaPmNgFnP	zavřít
<g/>
.	.	kIx.	.
</s>
<s>
Semináře	seminář	k1gInPc1	seminář
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Chieri	Chier	k1gInSc6	Chier
a	a	k8xC	a
Turíně	Turín	k1gInSc6	Turín
<g/>
,	,	kIx,	,
obsadili	obsadit	k5eAaPmAgMnP	obsadit
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
seminaristé	seminarista	k1gMnPc1	seminarista
diecéze	diecéze	k1gFnSc2	diecéze
zůstali	zůstat	k5eAaPmAgMnP	zůstat
bez	bez	k7c2	bez
domova	domov	k1gInSc2	domov
a	a	k8xC	a
bez	bez	k7c2	bez
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
oratoře	oratoř	k1gFnSc2	oratoř
se	se	k3xPyFc4	se
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
stal	stát	k5eAaPmAgInS	stát
diecézním	diecézní	k2eAgInSc7d1	diecézní
seminářem	seminář	k1gInSc7	seminář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
prošla	projít	k5eAaPmAgFnS	projít
oratoř	oratoř	k1gFnSc1	oratoř
asi	asi	k9	asi
největší	veliký	k2eAgFnSc7d3	veliký
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
nekatolických	katolický	k2eNgFnPc2d1	nekatolická
myšlenek	myšlenka	k1gFnPc2	myšlenka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
mnoho	mnoho	k4c4	mnoho
chlapců	chlapec	k1gMnPc2	chlapec
z	z	k7c2	z
oratoře	oratoř	k1gInSc2	oratoř
odcházelo	odcházet	k5eAaImAgNnS	odcházet
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
sice	sice	k8xC	sice
opět	opět	k6eAd1	opět
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
už	už	k9	už
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Boscova	Boscův	k2eAgFnSc1d1	Boscova
oratoř	oratoř	k1gFnSc1	oratoř
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
nabízela	nabízet	k5eAaImAgFnS	nabízet
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
co	co	k9	co
budou	být	k5eAaImBp3nP	být
chlapci	chlapec	k1gMnPc1	chlapec
dělat	dělat	k5eAaImF	dělat
dále	daleko	k6eAd2	daleko
a	a	k8xC	a
jak	jak	k6eAd1	jak
bude	být	k5eAaImBp3nS	být
dílo	dílo	k1gNnSc1	dílo
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gMnSc1	Bosco
chtěl	chtít	k5eAaImAgMnS	chtít
docílit	docílit	k5eAaPmF	docílit
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
sám	sám	k3xTgMnSc1	sám
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
ústavu	ústav	k1gInSc6	ústav
vychovávat	vychovávat	k5eAaImF	vychovávat
kněze	kněz	k1gMnPc4	kněz
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
by	by	kYmCp3nP	by
později	pozdě	k6eAd2	pozdě
zůstávali	zůstávat	k5eAaImAgMnP	zůstávat
a	a	k8xC	a
neodcházeli	odcházet	k5eNaImAgMnP	odcházet
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
dlouho	dlouho	k6eAd1	dlouho
nedařilo	dařit	k5eNaImAgNnS	dařit
a	a	k8xC	a
kněží	kněz	k1gMnPc1	kněz
odcházeli	odcházet	k5eAaImAgMnP	odcházet
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
seminářů	seminář	k1gInPc2	seminář
a	a	k8xC	a
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
biskupy	biskup	k1gInPc4	biskup
posíláni	posílat	k5eAaImNgMnP	posílat
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Bosca	Boscum	k1gNnPc4	Boscum
naléhali	naléhat	k5eAaBmAgMnP	naléhat
i	i	k8xC	i
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
Dokonce	dokonce	k9	dokonce
i	i	k8xC	i
Urban	Urban	k1gMnSc1	Urban
Rattazzi	Rattazh	k1gMnPc1	Rattazh
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prosadil	prosadit	k5eAaPmAgMnS	prosadit
protiřeholní	protiřeholní	k2eAgInPc4d1	protiřeholní
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
mu	on	k3xPp3gInSc3	on
roku	rok	k1gInSc3	rok
1847	[number]	k4	1847
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Milý	milý	k1gMnSc5	milý
Done	Don	k1gMnSc5	Don
Bosco	Bosca	k1gMnSc5	Bosca
<g/>
,	,	kIx,	,
přeji	přát	k5eAaImIp1nS	přát
vám	vy	k3xPp2nPc3	vy
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
mohl	moct	k5eAaImAgInS	moct
vyučovat	vyučovat	k5eAaImF	vyučovat
a	a	k8xC	a
vychovávat	vychovávat	k5eAaImF	vychovávat
tolik	tolik	k6eAd1	tolik
chudých	chudý	k2eAgMnPc2d1	chudý
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ani	ani	k9	ani
vy	vy	k3xPp2nPc1	vy
nejste	být	k5eNaImIp2nP	být
nesmrtelný	smrtelný	k2eNgInSc4d1	nesmrtelný
<g/>
!	!	kIx.	!
</s>
<s>
Co	co	k3yRnSc1	co
bude	být	k5eAaImBp3nS	být
z	z	k7c2	z
vašeho	váš	k3xOp2gInSc2	váš
podniku	podnik	k1gInSc2	podnik
po	po	k7c6	po
vás	vy	k3xPp2nPc6	vy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
aby	aby	kYmCp3nS	aby
zajistil	zajistit	k5eAaPmAgMnS	zajistit
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
a	a	k8xC	a
vychoval	vychovat	k5eAaPmAgMnS	vychovat
si	se	k3xPyFc3	se
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
Bosco	Bosco	k6eAd1	Bosco
začíná	začínat	k5eAaImIp3nS	začínat
psát	psát	k5eAaImF	psát
stanovy	stanova	k1gFnPc4	stanova
nové	nový	k2eAgFnSc2d1	nová
kongregace	kongregace	k1gFnSc2	kongregace
zasvěcené	zasvěcený	k2eAgFnSc2d1	zasvěcená
sv.	sv.	kA	sv.
Františku	František	k1gMnSc3	František
Saleskému	Saleský	k2eAgMnSc3d1	Saleský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
poprvé	poprvé	k6eAd1	poprvé
papeže	papež	k1gMnSc4	papež
Pia	Pius	k1gMnSc4	Pius
IX	IX	kA	IX
<g/>
.	.	kIx.	.
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
mu	on	k3xPp3gMnSc3	on
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
ho	on	k3xPp3gNnSc4	on
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
sepsal	sepsat	k5eAaPmAgMnS	sepsat
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
požadavek	požadavek	k1gInSc4	požadavek
během	během	k7c2	během
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
ještě	ještě	k9	ještě
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
až	až	k9	až
paměti	paměť	k1gFnSc2	paměť
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
Bosco	Bosco	k6eAd1	Bosco
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1859	[number]	k4	1859
úředně	úředně	k6eAd1	úředně
vzniká	vznikat	k5eAaImIp3nS	vznikat
salesiánská	salesiánský	k2eAgFnSc1d1	Salesiánská
kongregace	kongregace	k1gFnSc1	kongregace
-	-	kIx~	-
Společnost	společnost	k1gFnSc1	společnost
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Saleského	Saleský	k2eAgMnSc2d1	Saleský
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Salesiáni	salesián	k1gMnPc1	salesián
Dona	dona	k1gFnSc1	dona
Bosca	Bosca	k1gFnSc1	Bosca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
Boscem	Bosec	k1gInSc7	Bosec
má	mít	k5eAaImIp3nS	mít
18	[number]	k4	18
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Kongregace	kongregace	k1gFnSc1	kongregace
však	však	k9	však
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
církevně	církevně	k6eAd1	církevně
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gNnSc1	Bosco
schválení	schválení	k1gNnSc2	schválení
považuje	považovat	k5eAaImIp3nS	považovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
za	za	k7c4	za
formalitu	formalita	k1gFnSc4	formalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
však	však	k9	však
proti	proti	k7c3	proti
Boscovi	Bosec	k1gMnSc3	Bosec
ozývají	ozývat	k5eAaImIp3nP	ozývat
i	i	k9	i
kritické	kritický	k2eAgInPc4d1	kritický
hlasy	hlas	k1gInPc4	hlas
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
představitelé	představitel	k1gMnPc1	představitel
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nový	nový	k2eAgMnSc1d1	nový
turínský	turínský	k2eAgMnSc1d1	turínský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Gastaldi	Gastald	k1gMnPc1	Gastald
<g/>
,	,	kIx,	,
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
dílem	díl	k1gInSc7	díl
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k6eAd1	Bosco
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
několikrát	několikrát	k6eAd1	několikrát
vydává	vydávat	k5eAaPmIp3nS	vydávat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letitých	letitý	k2eAgFnPc6d1	letitá
snahách	snaha	k1gFnPc6	snaha
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
přerušeny	přerušit	k5eAaPmNgInP	přerušit
i	i	k9	i
prvním	první	k4xOgInSc7	první
vatikánským	vatikánský	k2eAgInSc7d1	vatikánský
koncilem	koncil	k1gInSc7	koncil
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
Bosco	Bosco	k6eAd1	Bosco
dočkal	dočkat	k5eAaPmAgMnS	dočkat
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1869	[number]	k4	1869
je	být	k5eAaImIp3nS	být
schválena	schválit	k5eAaPmNgFnS	schválit
tzv.	tzv.	kA	tzv.
Zbožná	zbožný	k2eAgFnSc1d1	zbožná
salesiánská	salesiánský	k2eAgFnSc1d1	Salesiánská
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1874	[number]	k4	1874
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
definitivně	definitivně	k6eAd1	definitivně
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
Stanovy	stanova	k1gFnPc4	stanova
Salesiánské	salesiánský	k2eAgFnSc2d1	Salesiánská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
již	již	k9	již
15	[number]	k4	15
ústavů	ústav	k1gInPc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgInP	začít
vznikat	vznikat	k5eAaImF	vznikat
další	další	k2eAgInPc1d1	další
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
nadále	nadále	k6eAd1	nadále
rychle	rychle	k6eAd1	rychle
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
Bosco	Bosco	k6eAd1	Bosco
odmítal	odmítat	k5eAaImAgMnS	odmítat
zakládání	zakládání	k1gNnSc4	zakládání
podobných	podobný	k2eAgFnPc2d1	podobná
institucí	instituce	k1gFnPc2	instituce
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1872	[number]	k4	1872
dal	dát	k5eAaPmAgMnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
Kongregaci	kongregace	k1gFnSc4	kongregace
Dcer	dcera	k1gFnPc2	dcera
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocnice	pomocnice	k1gFnSc2	pomocnice
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
nakonec	nakonec	k6eAd1	nakonec
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
poslední	poslední	k2eAgFnSc4d1	poslední
třetí	třetí	k4xOgFnSc4	třetí
větev	větev	k1gFnSc4	větev
-	-	kIx~	-
Salesiáni	salesián	k1gMnPc1	salesián
pomocníci	pomocník	k1gMnPc1	pomocník
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeni	tvořen	k2eAgMnPc1d1	tvořen
laiky	laik	k1gMnPc7	laik
(	(	kIx(	(
<g/>
terciáři	terciář	k1gMnPc7	terciář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
celkem	celkem	k6eAd1	celkem
730	[number]	k4	730
výchovných	výchovný	k2eAgInPc2d1	výchovný
ústavů	ústav	k1gInPc2	ústav
řízených	řízený	k2eAgInPc2d1	řízený
salesiány	salesián	k1gMnPc7	salesián
<g/>
,	,	kIx,	,
725	[number]	k4	725
salesiánkami	salesiánka	k1gFnPc7	salesiánka
a	a	k8xC	a
přes	přes	k7c4	přes
5000	[number]	k4	5000
různých	různý	k2eAgNnPc2d1	různé
školských	školský	k2eAgNnPc2d1	školské
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
komunit	komunita	k1gFnPc2	komunita
salesiánů	salesián	k1gMnPc2	salesián
1809	[number]	k4	1809
<g/>
,	,	kIx,	,
salesiánek	salesiánka	k1gFnPc2	salesiánka
pak	pak	k6eAd1	pak
1603	[number]	k4	1603
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
;	;	kIx,	;
salesiáni	salesián	k1gMnPc1	salesián
působili	působit	k5eAaImAgMnP	působit
ve	v	k7c6	v
122	[number]	k4	122
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
97	[number]	k4	97
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
5	[number]	k4	5
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Salesiáni	salesián	k1gMnPc1	salesián
brzy	brzy	k6eAd1	brzy
započali	započnout	k5eAaPmAgMnP	započnout
také	také	k9	také
činnost	činnost	k1gFnSc4	činnost
misionářskou	misionářský	k2eAgFnSc4d1	misionářská
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dále	daleko	k6eAd2	daleko
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
-	-	kIx~	-
zejména	zejména	k9	zejména
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc4	první
misionáře	misionář	k1gMnPc4	misionář
Bosco	Bosco	k6eAd1	Bosco
vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
,	,	kIx,	,
připravil	připravit	k5eAaPmAgMnS	připravit
a	a	k8xC	a
vyslal	vyslat	k5eAaPmAgMnS	vyslat
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Argentiny	Argentina	k1gFnSc2	Argentina
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
divošských	divošský	k2eAgInPc2d1	divošský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Misie	misie	k1gFnSc1	misie
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozrůstaly	rozrůstat	k5eAaImAgInP	rozrůstat
a	a	k8xC	a
salesiáni	salesián	k1gMnPc1	salesián
se	se	k3xPyFc4	se
zařadili	zařadit	k5eAaPmAgMnP	zařadit
mezi	mezi	k7c4	mezi
velké	velká	k1gFnPc4	velká
misionářské	misionářský	k2eAgFnSc2d1	misionářská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokřesťanštili	pokřesťanštit	k5eAaPmAgMnP	pokřesťanštit
Ekvádor	Ekvádor	k1gInSc4	Ekvádor
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
i	i	k9	i
k	k	k7c3	k
indiánským	indiánský	k2eAgInPc3d1	indiánský
kmenům	kmen	k1gInPc3	kmen
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Bororos	Bororosa	k1gFnPc2	Bororosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Působili	působit	k5eAaImAgMnP	působit
také	také	k9	také
kolem	kolem	k7c2	kolem
řeky	řeka	k1gFnSc2	řeka
Rio	Rio	k1gFnSc2	Rio
Negro	Negro	k1gNnSc1	Negro
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
k	k	k7c3	k
indiánským	indiánský	k2eAgInPc3d1	indiánský
kmenům	kmen	k1gInPc3	kmen
Paraguaye	Paraguay	k1gFnSc2	Paraguay
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Amazonskou	amazonský	k2eAgFnSc4d1	Amazonská
nížinu	nížina	k1gFnSc4	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
působili	působit	k5eAaImAgMnP	působit
také	také	k9	také
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gMnSc1	Bosco
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc1d1	známý
a	a	k8xC	a
populární	populární	k2eAgInSc1d1	populární
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1888	[number]	k4	1888
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
72	[number]	k4	72
let	léto	k1gNnPc2	léto
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
veliký	veliký	k2eAgInSc4d1	veliký
rozruch	rozruch	k1gInSc4	rozruch
<g/>
.	.	kIx.	.
</s>
<s>
Obchodníci	obchodník	k1gMnPc1	obchodník
zavírali	zavírat	k5eAaImAgMnP	zavírat
své	svůj	k3xOyFgInPc4	svůj
podniky	podnik	k1gInPc4	podnik
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
smrt	smrt	k1gFnSc4	smrt
Jana	Jan	k1gMnSc2	Jan
Bosca	Boscus	k1gMnSc2	Boscus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
pohřeb	pohřeb	k1gInSc4	pohřeb
přišlo	přijít	k5eAaPmAgNnS	přijít
okolo	okolo	k7c2	okolo
40	[number]	k4	40
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ozývaly	ozývat	k5eAaImAgInP	ozývat
hlasy	hlas	k1gInPc1	hlas
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
svatořečení	svatořečení	k1gNnSc4	svatořečení
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
s	s	k7c7	s
vyšetřováním	vyšetřování	k1gNnSc7	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
však	však	k9	však
byli	být	k5eAaImAgMnP	být
někteří	některý	k3yIgMnPc1	některý
vysocí	vysoký	k2eAgMnPc1d1	vysoký
představitelé	představitel	k1gMnPc1	představitel
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
i	i	k8xC	i
někteří	některý	k3yIgMnPc1	některý
kardinálové	kardinál	k1gMnPc1	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
pro	pro	k7c4	pro
Bosca	Boscus	k1gMnSc4	Boscus
kladně	kladně	k6eAd1	kladně
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
Piem	Pius	k1gMnSc7	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
beatifikován	beatifikován	k2eAgMnSc1d1	beatifikován
a	a	k8xC	a
na	na	k7c4	na
velikonoční	velikonoční	k2eAgFnSc4d1	velikonoční
neděli	neděle	k1gFnSc4	neděle
1934	[number]	k4	1934
kanonizován	kanonizovat	k5eAaBmNgInS	kanonizovat
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
ho	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
Otce	otec	k1gMnSc4	otec
a	a	k8xC	a
učitele	učitel	k1gMnSc4	učitel
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Boscovo	Boscův	k2eAgNnSc1d1	Boscův
dílo	dílo	k1gNnSc1	dílo
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednak	jednak	k8xC	jednak
mnoho	mnoho	k4c4	mnoho
článků	článek	k1gInPc2	článek
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
a	a	k8xC	a
pak	pak	k6eAd1	pak
asi	asi	k9	asi
130	[number]	k4	130
různých	různý	k2eAgInPc2d1	různý
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
byla	být	k5eAaImAgNnP	být
díla	dílo	k1gNnPc1	dílo
jak	jak	k6eAd1	jak
katechetická	katechetický	k2eAgNnPc1d1	Katechetické
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
populárně-vědecká	populárněědecký	k2eAgFnSc1d1	populárně-vědecká
<g/>
,	,	kIx,	,
životopisná	životopisný	k2eAgFnSc1d1	životopisná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
napsal	napsat	k5eAaBmAgInS	napsat
paměti	paměť	k1gFnSc2	paměť
prvních	první	k4xOgNnPc6	první
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gMnSc1	Bosco
původně	původně	k6eAd1	původně
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyvážil	vyvážit	k5eAaPmAgMnS	vyvážit
tiskoviny	tiskovina	k1gFnPc4	tiskovina
nepřátelské	přátelský	k2eNgNnSc1d1	nepřátelské
katolictví	katolictví	k1gNnSc1	katolictví
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
začaly	začít	k5eAaPmAgInP	začít
vycházet	vycházet	k5eAaImF	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vydával	vydávat	k5eAaImAgMnS	vydávat
např.	např.	kA	např.
měsíčník	měsíčník	k1gMnSc1	měsíčník
Katolické	katolický	k2eAgNnSc4d1	katolické
čtení	čtení	k1gNnSc4	čtení
či	či	k8xC	či
letáky	leták	k1gInPc4	leták
Co	co	k9	co
má	mít	k5eAaImIp3nS	mít
vědět	vědět	k5eAaImF	vědět
katolík	katolík	k1gMnSc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
jeho	jeho	k3xOp3gNnPc7	jeho
literárními	literární	k2eAgNnPc7d1	literární
díly	dílo	k1gNnPc7	dílo
byly	být	k5eAaImAgInP	být
životopisy	životopis	k1gInPc1	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
napsal	napsat	k5eAaPmAgMnS	napsat
např.	např.	kA	např.
o	o	k7c6	o
papežích	papeží	k2eAgInPc6d1	papeží
od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
až	až	k9	až
po	po	k7c4	po
Silvestra	Silvestr	k1gMnSc4	Silvestr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
o	o	k7c6	o
Dominiku	Dominik	k1gMnSc6	Dominik
Saviovi	Savius	k1gMnSc6	Savius
<g/>
,	,	kIx,	,
o	o	k7c4	o
Piu	Pius	k1gMnSc3	Pius
IX	IX	kA	IX
<g/>
.	.	kIx.	.
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
metrické	metrický	k2eAgFnSc2d1	metrická
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
vydal	vydat	k5eAaPmAgMnS	vydat
Bosco	Bosco	k6eAd1	Bosco
učebnici	učebnice	k1gFnSc3	učebnice
Desítková	desítkový	k2eAgFnSc1d1	desítková
metrická	metrický	k2eAgFnSc1d1	metrická
soustava	soustava	k1gFnSc1	soustava
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
ještě	ještě	k6eAd1	ještě
divadelní	divadelní	k2eAgFnSc4d1	divadelní
frašku	fraška	k1gFnSc4	fraška
Desetinná	desetinný	k2eAgFnSc1d1	desetinná
soustava	soustava	k1gFnSc1	soustava
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
napsal	napsat	k5eAaPmAgInS	napsat
učebnici	učebnice	k1gFnSc3	učebnice
Dějepis	dějepis	k1gInSc4	dějepis
italský	italský	k2eAgInSc4d1	italský
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
dokonce	dokonce	k9	dokonce
ministr	ministr	k1gMnSc1	ministr
Lanza	Lanz	k1gMnSc4	Lanz
zavést	zavést	k5eAaPmF	zavést
na	na	k7c4	na
státní	státní	k2eAgFnPc4d1	státní
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Požadoval	požadovat	k5eAaImAgMnS	požadovat
však	však	k9	však
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Bosco	Bosco	k1gMnSc1	Bosco
změnil	změnit	k5eAaPmAgMnS	změnit
několik	několik	k4yIc4	několik
míst	místo	k1gNnPc2	místo
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebudu	být	k5eNaImBp1nS	být
otrocky	otrocky	k6eAd1	otrocky
chválit	chválit	k5eAaImF	chválit
a	a	k8xC	a
zbaběle	zbaběle	k6eAd1	zbaběle
tupit	tupit	k5eAaImF	tupit
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
mi	já	k3xPp1nSc3	já
o	o	k7c4	o
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
ne	ne	k9	ne
o	o	k7c4	o
pochvalu	pochvala	k1gFnSc4	pochvala
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
velkým	velký	k2eAgInSc7d1	velký
dílem	díl	k1gInSc7	díl
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
šestidílné	šestidílný	k2eAgFnPc1d1	šestidílná
Církevní	církevní	k2eAgFnPc1d1	církevní
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k1gMnSc1	Bosco
také	také	k6eAd1	také
založil	založit	k5eAaPmAgMnS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozrůstala	rozrůstat	k5eAaImAgFnS	rozrůstat
<g/>
,	,	kIx,	,
i	i	k8xC	i
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
a	a	k8xC	a
salesiáni	salesián	k1gMnPc1	salesián
stále	stále	k6eAd1	stále
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
mnoho	mnoho	k4c4	mnoho
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
např.	např.	kA	např.
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Portál	portál	k1gInSc1	portál
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
metodu	metoda	k1gFnSc4	metoda
výchovy	výchova	k1gFnSc2	výchova
nazval	nazvat	k5eAaBmAgMnS	nazvat
Bosco	Bosco	k6eAd1	Bosco
preventivní	preventivní	k2eAgInSc4d1	preventivní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pak	pak	k6eAd1	pak
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
pilířích	pilíř	k1gInPc6	pilíř
<g/>
:	:	kIx,	:
rozum	rozum	k1gInSc1	rozum
-	-	kIx~	-
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
domýšlet	domýšlet	k5eAaImF	domýšlet
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
jeho	jeho	k3xOp3gNnSc2	jeho
jednání	jednání	k1gNnSc2	jednání
důsledky	důsledek	k1gInPc4	důsledek
na	na	k7c4	na
daného	daný	k2eAgMnSc4d1	daný
jednotlivce	jednotlivec	k1gMnSc4	jednotlivec
laskavost	laskavost	k1gFnSc1	laskavost
-	-	kIx~	-
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
<g/>
,	,	kIx,	,
zajímat	zajímat	k5eAaImF	zajímat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gInPc4	jejich
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
hry	hra	k1gFnPc4	hra
apod.	apod.	kA	apod.
náboženství	náboženství	k1gNnSc2	náboženství
-	-	kIx~	-
vychovatelská	vychovatelský	k2eAgFnSc1d1	vychovatelská
činnost	činnost	k1gFnSc1	činnost
má	mít	k5eAaImIp3nS	mít
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vztahu	vztah	k1gInSc2	vztah
vychovatele	vychovatel	k1gMnSc2	vychovatel
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vést	vést	k5eAaImF	vést
své	svůj	k3xOyFgMnPc4	svůj
svěřence	svěřenec	k1gMnPc4	svěřenec
<g/>
.	.	kIx.	.
</s>
<s>
Bosco	Bosco	k6eAd1	Bosco
s	s	k7c7	s
chlapci	chlapec	k1gMnPc7	chlapec
hrál	hrát	k5eAaImAgMnS	hrát
mnoho	mnoho	k4c4	mnoho
her	hra	k1gFnPc2	hra
a	a	k8xC	a
provozoval	provozovat	k5eAaImAgMnS	provozovat
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
modlili	modlit	k5eAaImAgMnP	modlit
<g/>
,	,	kIx,	,
účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
mše	mše	k1gFnSc2	mše
svaté	svatá	k1gFnSc2	svatá
apod.	apod.	kA	apod.
Některé	některý	k3yIgFnPc4	některý
výpovědi	výpověď	k1gFnPc4	výpověď
dona	don	k1gMnSc2	don
Bosca	Boscus	k1gMnSc2	Boscus
o	o	k7c6	o
výchově	výchova	k1gFnSc6	výchova
<g/>
:	:	kIx,	:
Salesián	salesián	k1gMnSc1	salesián
musí	muset	k5eAaImIp3nS	muset
svým	svůj	k3xOyFgInSc7	svůj
ustavičným	ustavičný	k2eAgInSc7d1	ustavičný
dohledem	dohled	k1gInSc7	dohled
a	a	k8xC	a
především	především	k6eAd1	především
pozornou	pozorný	k2eAgFnSc7d1	pozorná
péčí	péče	k1gFnSc7	péče
takřka	takřka	k6eAd1	takřka
znemožnit	znemožnit	k5eAaPmF	znemožnit
dětem	dítě	k1gFnPc3	dítě
hřích	hřích	k1gInSc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
pořád	pořád	k6eAd1	pořád
žít	žít	k5eAaImF	žít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jakým	jaký	k3yQgInSc7	jaký
názvem	název	k1gInSc7	název
<g/>
?	?	kIx.	?
</s>
<s>
Jako	jako	k9	jako
představený	představený	k1gMnSc1	představený
<g/>
?	?	kIx.	?
</s>
<s>
Jako	jako	k8xS	jako
špehoun	špehoun	k?	špehoun
<g/>
?	?	kIx.	?
</s>
<s>
Nikoliv	nikoliv	k9	nikoliv
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jako	jako	k8xS	jako
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nenechá	nechat	k5eNaPmIp3nS	nechat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
nikdy	nikdy	k6eAd1	nikdy
samy	sám	k3xTgFnPc1	sám
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jejich	jejich	k3xOp3gFnSc1	jejich
svoboda	svoboda	k1gFnSc1	svoboda
není	být	k5eNaImIp3nS	být
výchovou	výchova	k1gFnSc7	výchova
zabezpečena	zabezpečit	k5eAaPmNgFnS	zabezpečit
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
lásky	láska	k1gFnSc2	láska
není	být	k5eNaImIp3nS	být
důvěry	důvěra	k1gFnSc2	důvěra
a	a	k8xC	a
bez	bez	k7c2	bez
důvěry	důvěra	k1gFnSc2	důvěra
není	být	k5eNaImIp3nS	být
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Učinit	učinit	k5eAaImF	učinit
se	se	k3xPyFc4	se
milovanými	milovaný	k2eAgMnPc7d1	milovaný
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
milovaným	milovaný	k1gMnPc3	milovaný
učinili	učinit	k5eAaPmAgMnP	učinit
Pána	pán	k1gMnSc4	pán
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Heslem	heslo	k1gNnSc7	heslo
Dona	Don	k1gMnSc2	Don
Bosca	Boscus	k1gMnSc2	Boscus
bylo	být	k5eAaImAgNnS	být
(	(	kIx(	(
<g/>
a	a	k8xC	a
heslem	heslo	k1gNnSc7	heslo
salesiánů	salesián	k1gMnPc2	salesián
dosud	dosud	k6eAd1	dosud
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
latinské	latinský	k2eAgFnPc1d1	Latinská
Da	Da	k1gFnPc1	Da
mihi	mihi	k1gNnSc2	mihi
animas	animasa	k1gFnPc2	animasa
<g/>
,	,	kIx,	,
cetera	cetera	k1gFnSc1	cetera
tolle	tolle	k1gFnSc1	tolle
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
duše	duše	k1gFnPc1	duše
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
si	se	k3xPyFc3	se
vezmi	vzít	k5eAaPmRp2nS	vzít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Don	Don	k1gMnSc1	Don
Bosco	Bosco	k1gMnSc1	Bosco
jím	jíst	k5eAaImIp1nS	jíst
chtěl	chtít	k5eAaImAgMnS	chtít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
spasit	spasit	k5eAaPmF	spasit
lidské	lidský	k2eAgFnPc4d1	lidská
duše	duše	k1gFnPc4	duše
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zanedbaných	zanedbaný	k2eAgMnPc2d1	zanedbaný
chlapců	chlapec	k1gMnPc2	chlapec
z	z	k7c2	z
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
je	být	k5eAaImIp3nS	být
podřadné	podřadný	k2eAgNnSc1d1	podřadné
<g/>
.	.	kIx.	.
</s>
<s>
Pomyslně	pomyslně	k6eAd1	pomyslně
oslovován	oslovován	k2eAgInSc1d1	oslovován
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Ďábel	ďábel	k1gMnSc1	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
věta	věta	k1gFnSc1	věta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
(	(	kIx(	(
<g/>
Gn	Gn	k1gFnSc1	Gn
14	[number]	k4	14
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dixit	Dixit	k1gInSc1	Dixit
autem	aut	k1gInSc7	aut
rex	rex	k?	rex
Sodomorum	Sodomorum	k1gInSc1	Sodomorum
ad	ad	k7c4	ad
Abram	Abram	k1gInSc4	Abram
da	da	k?	da
mihi	mih	k1gFnSc2	mih
animas	animas	k1gMnSc1	animas
cetera	cetera	k1gFnSc1	cetera
tolle	toll	k1gMnSc2	toll
tibi	tib	k1gFnSc2	tib
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vulgáta	Vulgáta	k1gFnSc1	Vulgáta
<g/>
;	;	kIx,	;
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
ČEPu	čep	k1gInSc2	čep
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pak	pak	k6eAd1	pak
řekl	říct	k5eAaPmAgMnS	říct
Abramovi	Abram	k1gMnSc3	Abram
král	král	k1gMnSc1	král
Sodomy	Sodoma	k1gFnSc2	Sodoma
<g/>
:	:	kIx,	:
,	,	kIx,	,
<g/>
Dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
jmění	jmění	k1gNnSc1	jmění
si	se	k3xPyFc3	se
nech	nechat	k5eAaPmRp2nS	nechat
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
don	don	k1gMnSc1	don
Bosco	Bosco	k6eAd1	Bosco
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
několik	několik	k4yIc1	několik
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
těší	těšit	k5eAaImIp3nS	těšit
zájmu	zájem	k1gInSc2	zájem
právě	právě	k6eAd1	právě
díky	díky	k7c3	díky
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
<g/>
;	;	kIx,	;
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Lva	Lev	k1gMnSc2	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
dostavěl	dostavět	k5eAaPmAgInS	dostavět
kostel	kostel	k1gInSc4	kostel
Božského	božský	k2eAgNnSc2d1	božské
Srdce	srdce	k1gNnSc2	srdce
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
socha	socha	k1gFnSc1	socha
Pia	Pius	k1gMnSc2	Pius
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
držící	držící	k2eAgFnSc1d1	držící
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
listinu	listina	k1gFnSc4	listina
schvalující	schvalující	k2eAgFnSc4d1	schvalující
salesiánskou	salesiánský	k2eAgFnSc4d1	Salesiánská
kongregaci	kongregace	k1gFnSc4	kongregace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
je	být	k5eAaImIp3nS	být
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Papeži	Papež	k1gMnSc3	Papež
Piovi	Pius	k1gMnSc3	Pius
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
druhému	druhý	k4xOgMnSc3	druhý
otci	otec	k1gMnSc3	otec
salesiánskému	salesiánský	k2eAgMnSc3d1	salesiánský
<g/>
,	,	kIx,	,
postavili	postavit	k5eAaPmAgMnP	postavit
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zejména	zejména	k9	zejména
však	však	k9	však
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
chrám	chrám	k1gInSc1	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocnice	pomocnice	k1gFnSc2	pomocnice
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
i	i	k9	i
Boscův	Boscův	k2eAgInSc1d1	Boscův
hrob	hrob	k1gInSc1	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Památka	památka	k1gFnSc1	památka
Jana	Jan	k1gMnSc2	Jan
Bosca	Boscus	k1gMnSc2	Boscus
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
<g/>
.	.	kIx.	.
</s>
<s>
Znázorňován	znázorňován	k2eAgMnSc1d1	znázorňován
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kněžském	kněžský	k2eAgNnSc6d1	kněžské
rouchu	roucho	k1gNnSc6	roucho
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Bosco	Bosco	k1gMnSc1	Bosco
je	být	k5eAaImIp3nS	být
světec	světec	k1gMnSc1	světec
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
svědectví	svědectví	k1gNnPc2	svědectví
zázračných	zázračný	k2eAgInPc2d1	zázračný
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
měl	mít	k5eAaImAgMnS	mít
vykonat	vykonat	k5eAaPmF	vykonat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
popisuje	popisovat	k5eAaImIp3nS	popisovat
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc4	sám
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rozmnožení	rozmnožení	k1gNnSc1	rozmnožení
hostií	hostie	k1gFnPc2	hostie
<g/>
,	,	kIx,	,
čtení	čtení	k1gNnSc1	čtení
ve	v	k7c6	v
svědomí	svědomí	k1gNnSc6	svědomí
druhých	druhý	k4xOgFnPc2	druhý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
popisují	popisovat	k5eAaImIp3nP	popisovat
členové	člen	k1gMnPc1	člen
ústavu	ústav	k1gInSc2	ústav
či	či	k8xC	či
životopisci	životopisec	k1gMnPc1	životopisec
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
např.	např.	kA	např.
chovanci	chovanec	k1gMnPc1	chovanec
popisovali	popisovat	k5eAaImAgMnP	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
zpovědnici	zpovědnice	k1gFnSc6	zpovědnice
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
nevzpomínali	vzpomínat	k5eNaImAgMnP	vzpomínat
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
hříchy	hřích	k1gInPc4	hřích
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
hříchy	hřích	k1gInPc1	hřích
všechny	všechen	k3xTgInPc4	všechen
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
byl	být	k5eAaImAgInS	být
spatřen	spatřit	k5eAaPmNgInS	spatřit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
v	v	k7c4	v
danou	daný	k2eAgFnSc4d1	daná
dobu	doba	k1gFnSc4	doba
nenacházel	nacházet	k5eNaImAgInS	nacházet
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
bilokace	bilokace	k1gFnSc2	bilokace
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
prý	prý	k9	prý
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
i	i	k9	i
brzkou	brzký	k2eAgFnSc4d1	brzká
nečekanou	čekaný	k2eNgFnSc4d1	nečekaná
smrt	smrt	k1gFnSc4	smrt
některého	některý	k3yIgMnSc2	některý
chlapce	chlapec	k1gMnSc2	chlapec
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gNnSc3	on
přisuzovány	přisuzovat	k5eAaImNgFnP	přisuzovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
jasnovidecké	jasnovidecký	k2eAgFnPc1d1	jasnovidecká
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
modlitbu	modlitba	k1gFnSc4	modlitba
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
náhle	náhle	k6eAd1	náhle
a	a	k8xC	a
nečekaně	nečekaně	k6eAd1	nečekaně
vyléčilo	vyléčit	k5eAaPmAgNnS	vyléčit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zapříčiňovalo	zapříčiňovat	k5eAaImAgNnS	zapříčiňovat
i	i	k9	i
jejich	jejich	k3xOp3gInPc1	jejich
následné	následný	k2eAgInPc1d1	následný
štědré	štědrý	k2eAgInPc1d1	štědrý
dary	dar	k1gInPc1	dar
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
kongregace	kongregace	k1gFnSc2	kongregace
<g/>
.	.	kIx.	.
</s>
