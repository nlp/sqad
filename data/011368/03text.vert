<p>
<s>
Redundance	redundance	k1gFnSc1	redundance
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
redundare	redundar	k1gMnSc5	redundar
<g/>
,	,	kIx,	,
přetékat	přetékat	k5eAaImF	přetékat
<g/>
,	,	kIx,	,
přebývat	přebývat	k5eAaImF	přebývat
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
informační	informační	k2eAgInSc1d1	informační
nebo	nebo	k8xC	nebo
funkční	funkční	k2eAgInSc1d1	funkční
nadbytek	nadbytek	k1gInSc1	nadbytek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
nebo	nebo	k8xC	nebo
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
dvojí	dvojí	k4xRgNnSc4	dvojí
odlišné	odlišný	k2eAgNnSc4d1	odlišné
užití	užití	k1gNnSc4	užití
slova	slovo	k1gNnSc2	slovo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Redundance	redundance	k1gFnSc1	redundance
jako	jako	k8xC	jako
zbytečnost	zbytečnost	k1gFnSc1	zbytečnost
<g/>
,	,	kIx,	,
plýtvání	plýtvání	k1gNnSc1	plýtvání
slovy	slovo	k1gNnPc7	slovo
a	a	k8xC	a
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Redundantní	redundantní	k2eAgNnSc1d1	redundantní
je	být	k5eAaImIp3nS	být
příslovečné	příslovečný	k2eAgNnSc1d1	příslovečné
"	"	kIx"	"
<g/>
páté	pátý	k4xOgNnSc4	pátý
kolo	kolo	k1gNnSc4	kolo
u	u	k7c2	u
vozu	vůz	k1gInSc2	vůz
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
i	i	k8xC	i
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
je	být	k5eAaImIp3nS	být
však	však	k9	však
redundance	redundance	k1gFnSc1	redundance
důležitá	důležitý	k2eAgFnSc1d1	důležitá
jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
a	a	k8xC	a
odolnosti	odolnost	k1gFnSc2	odolnost
proti	proti	k7c3	proti
chybám	chyba	k1gFnPc3	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Redundance	redundance	k1gFnSc2	redundance
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gMnSc1	jazyk
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
informatického	informatický	k2eAgNnSc2d1	informatické
hlediska	hledisko	k1gNnSc2	hledisko
silně	silně	k6eAd1	silně
redundantní	redundantní	k2eAgInSc1d1	redundantní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc4	všechen
kombinace	kombinace	k1gFnPc4	kombinace
hlásek	hláska	k1gFnPc2	hláska
a	a	k8xC	a
písmen	písmeno	k1gNnPc2	písmeno
dávají	dávat	k5eAaImIp3nP	dávat
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
smysl	smysl	k1gInSc1	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jen	jen	k9	jen
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
čtenář	čtenář	k1gMnSc1	čtenář
rozeznat	rozeznat	k5eAaPmF	rozeznat
překlepy	překlep	k1gInPc4	překlep
<g/>
,	,	kIx,	,
tiskové	tiskový	k2eAgFnPc4d1	tisková
chyby	chyba	k1gFnPc4	chyba
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Automatické	automatický	k2eAgInPc1d1	automatický
jazykové	jazykový	k2eAgInPc1d1	jazykový
korektory	korektor	k1gInPc1	korektor
využívají	využívat	k5eAaPmIp3nP	využívat
právě	právě	k9	právě
této	tento	k3xDgFnSc2	tento
"	"	kIx"	"
<g/>
přirozené	přirozený	k2eAgFnSc2d1	přirozená
<g/>
"	"	kIx"	"
redundance	redundance	k1gFnSc2	redundance
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
telefonní	telefonní	k2eAgNnPc4d1	telefonní
čísla	číslo	k1gNnPc4	číslo
mají	mít	k5eAaImIp3nP	mít
redundanci	redundance	k1gFnSc3	redundance
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
při	při	k7c6	při
chybě	chyba	k1gFnSc6	chyba
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
často	často	k6eAd1	často
dovolá	dovolat	k5eAaPmIp3nS	dovolat
jinému	jiný	k1gMnSc3	jiný
účastníkovi	účastník	k1gMnSc3	účastník
než	než	k8xS	než
chtěl	chtít	k5eAaImAgInS	chtít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
hlasové	hlasový	k2eAgFnSc6d1	hlasová
komunikaci	komunikace	k1gFnSc6	komunikace
a	a	k8xC	a
za	za	k7c2	za
ztížených	ztížený	k2eAgFnPc2d1	ztížená
podmínek	podmínka	k1gFnPc2	podmínka
nebo	nebo	k8xC	nebo
u	u	k7c2	u
obtížných	obtížný	k2eAgNnPc2d1	obtížné
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
zkratek	zkratka	k1gFnPc2	zkratka
atd.	atd.	kA	atd.
se	se	k3xPyFc4	se
redundance	redundance	k1gFnSc1	redundance
uměle	uměle	k6eAd1	uměle
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hláskováním	hláskování	k1gNnSc7	hláskování
<g/>
:	:	kIx,	:
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
pac	pac	k1gInSc1	pac
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
hláskuje	hláskovat	k5eAaImIp3nS	hláskovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
pé-á-cé	pé-áé	k6eAd1	pé-á-cé
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Petr	Petr	k1gMnSc1	Petr
–	–	k?	–
Adam	Adam	k1gMnSc1	Adam
–	–	k?	–
Cyril	Cyril	k1gMnSc1	Cyril
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zvlášť	zvlášť	k6eAd1	zvlášť
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
(	(	kIx(	(
<g/>
spelling	spelling	k1gInSc1	spelling
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
odchyluje	odchylovat	k5eAaImIp3nS	odchylovat
od	od	k7c2	od
psané	psaný	k2eAgFnSc2d1	psaná
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
redundance	redundance	k1gFnSc1	redundance
záměrně	záměrně	k6eAd1	záměrně
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
rétorice	rétorika	k1gFnSc6	rétorika
<g/>
:	:	kIx,	:
opakovaná	opakovaný	k2eAgNnPc1d1	opakované
slova	slovo	k1gNnPc1	slovo
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc1	myšlenka
si	se	k3xPyFc3	se
posluchač	posluchač	k1gMnSc1	posluchač
lépe	dobře	k6eAd2	dobře
zapamatuje	zapamatovat	k5eAaPmIp3nS	zapamatovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
básních	báseň	k1gFnPc6	báseň
se	se	k3xPyFc4	se
myšlenka	myšlenka	k1gFnSc1	myšlenka
často	často	k6eAd1	často
opakuje	opakovat	k5eAaImIp3nS	opakovat
jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
(	(	kIx(	(
<g/>
hendiadys	hendiadys	k1gNnSc1	hendiadys
<g/>
,	,	kIx,	,
paralelismus	paralelismus	k1gInSc1	paralelismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
==	==	k?	==
</s>
</p>
<p>
<s>
Zejména	zejména	k9	zejména
zápisy	zápis	k1gInPc1	zápis
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
kódů	kód	k1gInPc2	kód
a	a	k8xC	a
programů	program	k1gInPc2	program
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
redundanci	redundance	k1gFnSc4	redundance
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
plánovitě	plánovitě	k6eAd1	plánovitě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
například	například	k6eAd1	například
paritou	parita	k1gFnSc7	parita
<g/>
,	,	kIx,	,
kontrolní	kontrolní	k2eAgFnSc7d1	kontrolní
číslicí	číslice	k1gFnSc7	číslice
nebo	nebo	k8xC	nebo
kontrolním	kontrolní	k2eAgInSc7d1	kontrolní
součtem	součet	k1gInSc7	součet
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
aspoň	aspoň	k9	aspoň
odhalení	odhalení	k1gNnSc4	odhalení
části	část	k1gFnSc2	část
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
daleko	daleko	k6eAd1	daleko
složitější	složitý	k2eAgMnSc1d2	složitější
a	a	k8xC	a
nákladnější	nákladný	k2eAgFnPc1d2	nákladnější
redundance	redundance	k1gFnPc1	redundance
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
samoopravného	samoopravný	k2eAgNnSc2d1	samoopravné
kódování	kódování	k1gNnSc2	kódování
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
samoopravný	samoopravný	k2eAgInSc4d1	samoopravný
kód	kód	k1gInSc4	kód
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
automatickou	automatický	k2eAgFnSc4d1	automatická
opravu	oprava	k1gFnSc4	oprava
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
i	i	k9	i
více	hodně	k6eAd2	hodně
chyb	chyba	k1gFnPc2	chyba
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
bitů	bit	k1gInPc2	bit
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
technice	technika	k1gFnSc6	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
technické	technický	k2eAgFnSc2d1	technická
redundance	redundance	k1gFnSc2	redundance
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zdvojování	zdvojování	k1gNnSc1	zdvojování
výtahových	výtahový	k2eAgNnPc2d1	výtahové
lan	lano	k1gNnPc2	lano
<g/>
,	,	kIx,	,
souběžné	souběžný	k2eAgNnSc4d1	souběžné
ukládání	ukládání	k1gNnSc4	ukládání
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
diskových	diskový	k2eAgFnPc6d1	disková
polích	pole	k1gFnPc6	pole
RAID	raid	k1gInSc4	raid
<g/>
,	,	kIx,	,
souběžné	souběžný	k2eAgNnSc4d1	souběžné
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
u	u	k7c2	u
spolehlivostních	spolehlivostní	k2eAgInPc2d1	spolehlivostní
počítačových	počítačový	k2eAgInPc2d1	počítačový
systémů	systém	k1gInPc2	systém
nebo	nebo	k8xC	nebo
ztrojení	ztrojení	k1gNnSc1	ztrojení
řídících	řídící	k2eAgInPc2d1	řídící
a	a	k8xC	a
komunikačních	komunikační	k2eAgInPc2d1	komunikační
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
raketoplánu	raketoplán	k1gInSc6	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
první	první	k4xOgInSc4	první
československý	československý	k2eAgInSc4d1	československý
číslicový	číslicový	k2eAgInSc4d1	číslicový
počítač	počítač	k1gInSc4	počítač
SAPO	sapa	k1gFnSc5	sapa
měl	mít	k5eAaImAgInS	mít
ztrojenou	ztrojený	k2eAgFnSc4d1	ztrojená
výpočetní	výpočetní	k2eAgFnSc4d1	výpočetní
jednotku	jednotka	k1gFnSc4	jednotka
a	a	k8xC	a
při	při	k7c6	při
nesouhlasném	souhlasný	k2eNgInSc6d1	nesouhlasný
výsledku	výsledek	k1gInSc6	výsledek
operace	operace	k1gFnPc1	operace
jednotky	jednotka	k1gFnPc1	jednotka
"	"	kIx"	"
<g/>
hlasovaly	hlasovat	k5eAaImAgFnP	hlasovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
uvážit	uvážit	k5eAaPmF	uvážit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
redundance	redundance	k1gFnSc2	redundance
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
součástek	součástka	k1gFnPc2	součástka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
také	také	k9	také
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
nahodilé	nahodilý	k2eAgFnSc2d1	nahodilá
chyby	chyba	k1gFnSc2	chyba
i	i	k8xC	i
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
</s>
</p>
