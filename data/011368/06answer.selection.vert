<s>
Jazyk	jazyk	k1gMnSc1	jazyk
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
informatického	informatický	k2eAgNnSc2d1	informatické
hlediska	hledisko	k1gNnSc2	hledisko
silně	silně	k6eAd1	silně
redundantní	redundantní	k2eAgInSc1d1	redundantní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc4	všechen
kombinace	kombinace	k1gFnPc4	kombinace
hlásek	hláska	k1gFnPc2	hláska
a	a	k8xC	a
písmen	písmeno	k1gNnPc2	písmeno
dávají	dávat	k5eAaImIp3nP	dávat
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
jazyce	jazyk	k1gInSc6	jazyk
smysl	smysl	k1gInSc1	smysl
<g/>
.	.	kIx.	.
</s>
