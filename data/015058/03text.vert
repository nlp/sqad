<s>
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lokomotíva	Lokomotíva	k1gFnSc1
KošiceNázev	KošiceNázev	k1gFnSc2
</s>
<s>
Basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc4
Země	zem	k1gFnSc2
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Sever	sever	k1gInSc1
<g/>
)	)	kIx)
Založení	založení	k1gNnSc1
</s>
<s>
1946	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zánik	zánik	k1gInSc1
</s>
<s>
2001	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Asociace	asociace	k1gFnSc2
</s>
<s>
SBA	SBA	kA
Barvy	barva	k1gFnPc1
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
Stadion	stadion	k1gNnSc4
Název	název	k1gInSc1
</s>
<s>
Basketbalová	basketbalový	k2eAgFnSc1d1
hala	hala	k1gFnSc1
Lokomotíva	Lokomotíva	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Československá	československý	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
extraliga	extraliga	k1gFnSc1
žen	žena	k1gFnPc2
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
</s>
<s>
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotív	k1gMnSc2
Košice	Košice	k1gInPc1
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
</s>
<s>
Delta	delta	k1gFnSc1
Management	management	k1gInSc1
Košice	Košice	k1gInPc1
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
</s>
<s>
Delta	delta	k1gFnSc1
Domex	Domex	k1gInSc1
Košice	Košice	k1gInPc1
</s>
<s>
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
slovenský	slovenský	k2eAgInSc1d1
basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
sídlil	sídlit	k5eAaImAgInS
v	v	k7c6
košické	košický	k2eAgFnSc6d1
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Sever	sever	k1gInSc1
ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založen	založen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
fotbalovým	fotbalový	k2eAgInSc7d1
a	a	k8xC
stolnotenisovým	stolnotenisový	k2eAgInSc7d1
oddílem	oddíl	k1gInSc7
do	do	k7c2
nově	nově	k6eAd1
založeného	založený	k2eAgInSc2d1
Športového	Športový	k2eAgInSc2d1
klubu	klub	k1gInSc2
Železničiari	Železničiar	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ženský	ženský	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
býval	bývat	k5eAaImAgMnS
dlouholetým	dlouholetý	k2eAgMnSc7d1
účastníkem	účastník	k1gMnSc7
československé	československý	k2eAgFnSc2d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
slovenské	slovenský	k2eAgFnSc2d1
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
ovšem	ovšem	k9
Lokomotíva	Lokomotíva	k1gFnSc1
nikdy	nikdy	k6eAd1
nedosáhla	dosáhnout	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Zanikla	zaniknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
po	po	k7c6
přetvoření	přetvoření	k1gNnSc6
původního	původní	k2eAgInSc2d1
klubu	klub	k1gInSc2
do	do	k7c2
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Delta	delta	k1gNnSc2
VODS	VODS	kA
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Good	Good	k1gInSc1
Angels	Angelsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Klubové	klubový	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
byly	být	k5eAaImAgInP
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrával	odehrávat	k5eAaImAgInS
v	v	k7c6
basketbalové	basketbalový	k2eAgFnSc6d1
hale	hala	k1gFnSc6
Lokomotíva	Lokomotívo	k1gNnSc2
v	v	k7c4
košickom	košickom	k1gInSc4
Čermeli	Čermel	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1946	#num#	k4
–	–	k?
ŠK	ŠK	kA
Železničiari	Železničiar	k1gFnSc2
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Športový	Športový	k2eAgInSc1d1
klub	klub	k1gInSc1
Železničiari	Železničiar	k1gFnSc2
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1946	#num#	k4
–	–	k?
fúze	fúze	k1gFnSc1
s	s	k7c7
ŠK	ŠK	kA
Sparta	Sparta	k1gFnSc1
Košice	Košice	k1gInPc1
=	=	kIx~
<g/>
>	>	kIx)
ŠK	ŠK	kA
Železničiari	Železničiare	k1gFnSc4
Sparta	Sparta	k1gFnSc1
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Športový	Športový	k2eAgInSc1d1
klub	klub	k1gInSc1
Železničiari	Železničiari	k1gNnSc2
Sparta	Sparta	k1gFnSc1
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1949	#num#	k4
–	–	k?
fúze	fúze	k1gFnSc1
s	s	k7c7
Sokol	Sokol	k1gMnSc1
Jednota	jednota	k1gFnSc1
Dynamo	dynamo	k1gNnSc1
Košice	Košice	k1gInPc1
=	=	kIx~
<g/>
>	>	kIx)
ZSJ	ZSJ	kA
Dynamo	dynamo	k1gNnSc1
ČSD	ČSD	kA
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Závodná	Závodný	k2eAgFnSc1d1
sokolská	sokolský	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Dynamo	dynamo	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
štátne	štátnout	k5eAaPmIp3nS
dráhy	dráha	k1gFnSc2
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1952	#num#	k4
–	–	k?
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Telovýchovná	Telovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1963	#num#	k4
–	–	k?
fúze	fúze	k1gFnSc1
s	s	k7c7
TJ	tj	kA
VSŽ	VSŽ	kA
Košice	Košice	k1gInPc1
=	=	kIx~
<g/>
>	>	kIx)
TJ	tj	kA
Lokomotíva	Lokomotívo	k1gNnSc2
VSŽ	VSŽ	kA
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Telovýchovná	Telovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Východoslovenské	východoslovenský	k2eAgNnSc1d1
železiarne	železiarnout	k5eAaPmIp3nS
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1967	#num#	k4
–	–	k?
znovu	znovu	k6eAd1
oddělení	oddělení	k1gNnSc1
=	=	kIx~
<g/>
>	>	kIx)
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Telovýchovná	Telovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
–	–	k?
BK	BK	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
–	–	k?
fúze	fúze	k1gFnSc1
s	s	k7c7
TJ	tj	kA
Jednota	jednota	k1gFnSc1
VSS	VSS	kA
Unimex	Unimex	k1gInSc1
Košice	Košice	k1gInPc1
=	=	kIx~
<g/>
>	>	kIx)
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
(	(	kIx(
<g/>
Basketbalový	basketbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
Východoslovenské	východoslovenský	k2eAgFnSc6d1
strojárne	strojárnout	k5eAaPmIp3nS
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
–	–	k?
Delta	delta	k1gFnSc1
Management	management	k1gInSc1
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1998	#num#	k4
–	–	k?
Delta	delta	k1gFnSc1
Management	management	k1gInSc1
Košice	Košice	k1gInPc1
</s>
<s>
1999	#num#	k4
–	–	k?
Delta	delta	k1gFnSc1
Domex	Domex	k1gInSc1
Košice	Košice	k1gInPc1
</s>
<s>
2000	#num#	k4
–	–	k?
Delta	delta	k1gFnSc1
Termostav	Termostav	k1gMnSc1
Mráz	Mráz	k1gMnSc1
Košice	Košice	k1gInPc1
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
–	–	k?
ženy	žena	k1gFnSc2
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
ZČ	ZČ	kA
-	-	kIx~
základní	základní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1959	#num#	k4
–	–	k?
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
v	v	k7c6
ročníku	ročník	k1gInSc6
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
ZČ	ZČ	kA
</s>
<s>
Play-off	Play-off	k1gMnSc1
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Sestup	sestup	k1gInSc1
</s>
<s>
...	...	k?
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Sestup	sestup	k1gInSc1
</s>
<s>
...	...	k?
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotívo	k1gNnSc2
VSŽ	VSŽ	kA
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Sestup	sestup	k1gInSc1
</s>
<s>
...	...	k?
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Play-out	Play-out	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
TJ	tj	kA
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
...	...	k?
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotív	k1gMnSc2
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
1993	#num#	k4
–	–	k?
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
v	v	k7c6
ročníku	ročník	k1gInSc6
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
ZČ	ZČ	kA
</s>
<s>
Play-off	Play-off	k1gMnSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotív	k1gMnSc2
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotív	k1gMnSc2
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotív	k1gMnSc2
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotív	k1gMnSc2
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
BK	BK	kA
VSS	VSS	kA
Lokomotíva	Lokomotív	k1gMnSc2
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Delta	delta	k1gFnSc1
Management	management	k1gInSc1
Lokomotíva	Lokomotíva	k1gFnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Delta	delta	k1gFnSc1
Management	management	k1gInSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Delta	delta	k1gFnSc1
Domex	Domex	k1gInSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Delta	delta	k1gFnSc1
Termostav	Termostav	k1gMnSc1
Mráz	Mráz	k1gMnSc1
Košice	Košice	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
úroveň	úroveň	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
Zápas	zápas	k1gInSc1
o	o	k7c4
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
mezinárodních	mezinárodní	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výkonnostní	výkonnostní	k2eAgFnPc1d1
úrovně	úroveň	k1gFnPc1
</s>
<s>
superpohár	superpohár	k1gInSc1
–	–	k?
Superpohár	superpohár	k1gInSc4
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
–	–	k?
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
Euroliga	Euroliga	k1gFnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
výkonnostní	výkonnostní	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
–	–	k?
Pohár	pohár	k1gInSc1
Ronchettiové	Ronchettius	k1gMnPc1
<g/>
,	,	kIx,
EuroCup	EuroCup	k1gMnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
EL	Ela	k1gFnPc2
-	-	kIx~
Euroliga	Euroliga	k1gFnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
-	-	kIx~
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
EC	EC	kA
-	-	kIx~
EuroCup	EuroCup	k1gMnSc1
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
SP	SP	kA
-	-	kIx~
Superpohár	superpohár	k1gInSc4
v	v	k7c6
basketbalu	basketbal	k1gInSc6
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
PR	pr	k0
-	-	kIx~
Pohár	pohár	k1gInSc4
Ronchettiové	Ronchettiový	k2eAgFnSc2d1
</s>
<s>
PR	pr	k0
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
PR	pr	k0
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
–	–	k?
Základní	základní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
D	D	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
PR	pr	k0
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PR	pr	k0
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Krátko	krátko	k6eAd1
z	z	k7c2
histórie	histórie	k1gFnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fclokomotiva	fclokomotiva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
Klubová	klubový	k2eAgNnPc4d1
história	histórium	k1gNnPc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
stklokomotiva	stklokomotiva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
GOOD	GOOD	kA
ANGELS	ANGELS	kA
Košice	Košice	k1gInPc4
-	-	kIx~
História	Histórium	k1gNnSc2
klubu	klub	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
exz	exz	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
14.10	14.10	k4
<g/>
.2014	.2014	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Basketbalová	basketbalový	k2eAgFnSc1d1
hala	hala	k1gFnSc1
Lokomotíva	Lokomotíva	k1gFnSc1
-	-	kIx~
Basketbal	basketbal	k1gInSc1
<g/>
,	,	kIx,
Košice	Košice	k1gInPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sportoviska	sportovisko	k1gNnPc4
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Juraj	Juraj	k1gMnSc1
Gacík	Gacík	k1gMnSc1
<g/>
:	:	kIx,
Kronika	kronika	k1gFnSc1
československého	československý	k2eAgInSc2d1
a	a	k8xC
slovenského	slovenský	k2eAgInSc2d1
basketbalu	basketbal	k1gInSc2
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
-	-	kIx~
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
2000	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
slovensky	slovensky	k6eAd1
<g/>
,	,	kIx,
BADEM	BADEM	kA
<g/>
,	,	kIx,
Žilina	Žilina	k1gFnSc1
<g/>
,	,	kIx,
943	#num#	k4
stran	strana	k1gFnPc2
<g/>
↑	↑	k?
"	"	kIx"
<g/>
TJ	tj	kA
VSŽ	VSŽ	kA
-	-	kIx~
história	histórium	k1gNnPc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
forevervszkosice	forevervszkosice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Historický	historický	k2eAgInSc1d1
prehľad	prehľad	k1gInSc1
súťaží	súťažit	k5eAaPmIp3nS
po	po	k7c4
jednotlivých	jednotlivý	k2eAgInPc2d1
ročníkoch	ročníkoch	k1gInSc4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
msbasket	msbasket	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Új	Új	k1gFnSc1
Szó	Szó	k1gFnSc1
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
library	librar	k1gInPc1
<g/>
.	.	kIx.
<g/>
hungaricana	hungaricana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
hu	hu	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Šimák	Šimák	k1gMnSc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
československého	československý	k2eAgInSc2d1
basketbalu	basketbal	k1gInSc2
v	v	k7c6
číslech	číslo	k1gNnPc6
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
-	-	kIx~
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Basketbalový	basketbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ÚV	ÚV	kA
ČSTV	ČSTV	kA
<g/>
,	,	kIx,
květen	květen	k1gInSc4
1985	#num#	k4
<g/>
,	,	kIx,
174	#num#	k4
stran	strana	k1gFnPc2
<g/>
↑	↑	k?
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Šimák	Šimák	k1gMnSc1
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Historie	historie	k1gFnSc1
československého	československý	k2eAgInSc2d1
basketbalu	basketbal	k1gInSc2
v	v	k7c6
číslech	číslo	k1gNnPc6
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
část	část	k1gFnSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
-	-	kIx~
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
a	a	k8xC
slovenská	slovenský	k2eAgFnSc1d1
basketbalová	basketbalový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
<g/>
,	,	kIx,
březen	březen	k1gInSc1
1993	#num#	k4
<g/>
,	,	kIx,
130	#num#	k4
stran	strana	k1gFnPc2
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Women	Women	k2eAgInSc1d1
Basketball	Basketball	k1gInSc1
European	Europeana	k1gFnPc2
Champions	Championsa	k1gFnPc2
Cup	cup	k1gInSc1
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
todor	todor	k1gInSc1
<g/>
66	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Women	Women	k2eAgInSc1d1
Basketball	Basketball	k1gInSc1
European	Europeana	k1gFnPc2
Ronchetti	Ronchetť	k1gFnSc2
Cup	cup	k1gInSc1
Archive	archiv	k1gInSc5
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
todor	todor	k1gInSc1
<g/>
66	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
History	Histor	k1gInPc4
(	(	kIx(
<g/>
Ronchetti	Ronchetti	k1gNnSc3
Cup	cup	k1gInSc1
1972	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fibaeurope	fibaeurop	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
History	Histor	k1gInPc1
(	(	kIx(
<g/>
FIBA	FIBA	kA
Euroleague	Euroleague	k1gFnSc1
2001	#num#	k4
<g/>
–	–	k?
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
archive	archiv	k1gInSc5
<g/>
.	.	kIx.
<g/>
fiba	fibum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
History	Histor	k1gInPc1
(	(	kIx(
<g/>
FIBA	FIBA	kA
Europe	Europ	k1gInSc5
Cup	cup	k1gInSc1
2003	#num#	k4
<g/>
–	–	k?
)	)	kIx)
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fibaeurope	fibaeurop	k1gMnSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Ročníky	ročník	k1gInPc1
slovenských	slovenský	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
sezóny	sezóna	k1gFnSc2
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
včetně	včetně	k7c2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
