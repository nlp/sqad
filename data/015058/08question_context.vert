<s desamb="1">
Založen	založen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
fotbalovým	fotbalový	k2eAgInSc7d1
a	a	k8xC
stolnotenisovým	stolnotenisový	k2eAgInSc7d1
oddílem	oddíl	k1gInSc7
do	do	k7c2
nově	nově	k6eAd1
založeného	založený	k2eAgInSc2d1
Športového	Športový	k2eAgInSc2d1
klubu	klub	k1gInSc2
Železničiari	Železničiar	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ženský	ženský	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
býval	bývat	k5eAaImAgMnS
dlouholetým	dlouholetý	k2eAgMnSc7d1
účastníkem	účastník	k1gMnSc7
československé	československý	k2eAgFnSc2d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
slovenské	slovenský	k2eAgFnSc2d1
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>