<p>
<s>
Penicilin	penicilin	k1gInSc1	penicilin
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
penicilín	penicilín	k1gInSc1	penicilín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc3	smysl
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
tradicí	tradice	k1gFnSc7	tradice
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
beta-laktamová	betaaktamový	k2eAgNnPc4d1	beta-laktamový
antibiotika	antibiotikum	k1gNnPc4	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
plísně	plíseň	k1gFnSc2	plíseň
Penicillium	Penicillium	k1gNnSc1	Penicillium
(	(	kIx(	(
<g/>
štětičkovec	štětičkovec	k1gInSc1	štětičkovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
poprvé	poprvé	k6eAd1	poprvé
identifikována	identifikován	k2eAgFnSc1d1	identifikována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Penicilin	penicilin	k1gInSc1	penicilin
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hlediska	hledisko	k1gNnSc2	hledisko
bicyklická	bicyklický	k2eAgFnSc1d1	bicyklický
organická	organický	k2eAgFnSc1d1	organická
kyselina	kyselina	k1gFnSc1	kyselina
se	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
C	C	kA	C
<g/>
5	[number]	k4	5
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
NSO	NSO	kA	NSO
<g/>
(	(	kIx(	(
<g/>
COOH	COOH	kA	COOH
<g/>
)	)	kIx)	)
<g/>
NHCOR	NHCOR	kA	NHCOR
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
R	R	kA	R
znamená	znamenat	k5eAaImIp3nS	znamenat
funkční	funkční	k2eAgInSc1d1	funkční
zbytek	zbytek	k1gInSc1	zbytek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
lépe	dobře	k6eAd2	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
,	,	kIx,	,
etheru	ether	k1gInSc6	ether
<g/>
,	,	kIx,	,
chloroformu	chloroform	k1gInSc6	chloroform
a	a	k8xC	a
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
penicilinu	penicilin	k1gInSc2	penicilin
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
spojených	spojený	k2eAgFnPc2d1	spojená
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
cysteinu	cystein	k1gInSc2	cystein
a	a	k8xC	a
valinu	valin	k1gInSc2	valin
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
navěšena	navěšet	k5eAaBmNgFnS	navěšet
na	na	k7c4	na
některá	některý	k3yIgNnPc4	některý
místa	místo	k1gNnPc4	místo
penicilinové	penicilinový	k2eAgFnPc4d1	penicilinová
molekuly	molekula	k1gFnPc4	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
základě	základ	k1gInSc6	základ
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
například	například	k6eAd1	například
penicilin	penicilin	k1gInSc1	penicilin
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
V	V	kA	V
či	či	k8xC	či
X.	X.	kA	X.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinky	účinek	k1gInPc4	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Peniciliny	penicilin	k1gInPc1	penicilin
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
beta-laktamová	betaaktamový	k2eAgNnPc1d1	beta-laktamový
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nP	působit
baktericidně	baktericidně	k6eAd1	baktericidně
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
účinná	účinný	k2eAgFnSc1d1	účinná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
toxicitu	toxicita	k1gFnSc4	toxicita
<g/>
.	.	kIx.	.
</s>
<s>
Užívají	užívat	k5eAaImIp3nP	užívat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
proti	proti	k7c3	proti
grampozitivním	grampozitivní	k2eAgFnPc3d1	grampozitivní
bakteriím	bakterie	k1gFnPc3	bakterie
<g/>
,	,	kIx,	,
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
např.	např.	kA	např.
na	na	k7c4	na
streptokoky	streptokok	k1gMnPc4	streptokok
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc1d1	mnohý
stafylokoky	stafylokok	k1gInPc1	stafylokok
<g/>
,	,	kIx,	,
listerie	listerie	k1gFnPc1	listerie
<g/>
,	,	kIx,	,
bacily	bacil	k1gInPc1	bacil
<g/>
,	,	kIx,	,
klostridia	klostridium	k1gNnPc1	klostridium
<g/>
,	,	kIx,	,
aktinomycety	aktinomycet	k1gInPc1	aktinomycet
<g/>
.	.	kIx.	.
</s>
<s>
Vykazují	vykazovat	k5eAaImIp3nP	vykazovat
však	však	k9	však
i	i	k9	i
účinek	účinek	k1gInSc4	účinek
proti	proti	k7c3	proti
jistým	jistý	k2eAgFnPc3d1	jistá
gramnegativním	gramnegativní	k2eAgFnPc3d1	gramnegativní
bakteriím	bakterie	k1gFnPc3	bakterie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
neisserie	neisserie	k1gFnPc1	neisserie
<g/>
,	,	kIx,	,
borrelie	borrelie	k1gFnPc1	borrelie
<g/>
,	,	kIx,	,
treponemy	treponema	k1gFnPc1	treponema
či	či	k8xC	či
leptospiry	leptospira	k1gFnPc1	leptospira
<g/>
.	.	kIx.	.
</s>
<s>
Beta-laktamová	Betaaktamový	k2eAgNnPc1d1	Beta-laktamový
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
včetně	včetně	k7c2	včetně
penicilinu	penicilin	k1gInSc2	penicilin
obecně	obecně	k6eAd1	obecně
brzdí	brzdit	k5eAaImIp3nP	brzdit
syntézu	syntéza	k1gFnSc4	syntéza
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněčných	buněčný	k2eAgFnPc2d1	buněčná
stěn	stěna	k1gFnPc2	stěna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vážou	vázat	k5eAaImIp3nP	vázat
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
významné	významný	k2eAgInPc4d1	významný
enzymy	enzym	k1gInPc4	enzym
(	(	kIx(	(
<g/>
transpeptidázy	transpeptidáza	k1gFnPc4	transpeptidáza
a	a	k8xC	a
karboxypeptidázy	karboxypeptidáza	k1gFnPc4	karboxypeptidáza
<g/>
)	)	kIx)	)
účastnící	účastnící	k2eAgFnSc2d1	účastnící
se	se	k3xPyFc4	se
syntézy	syntéza	k1gFnSc2	syntéza
peptidoglykanu	peptidoglykan	k1gInSc2	peptidoglykan
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
nepřímo	přímo	k6eNd1	přímo
aktivují	aktivovat	k5eAaBmIp3nP	aktivovat
autolyziny	autolyzin	k1gInPc1	autolyzin
<g/>
,	,	kIx,	,
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
způsobí	způsobit	k5eAaPmIp3nS	způsobit
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
smrt	smrt	k1gFnSc4	smrt
(	(	kIx(	(
<g/>
lýzi	lýze	k1gFnSc4	lýze
<g/>
)	)	kIx)	)
bakterií	bakterie	k1gFnSc7	bakterie
<g/>
.	.	kIx.	.
<g/>
Určité	určitý	k2eAgInPc1d1	určitý
kmeny	kmen	k1gInPc1	kmen
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
bakterií	bakterie	k1gFnPc2	bakterie
si	se	k3xPyFc3	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
proti	proti	k7c3	proti
penicilinu	penicilin	k1gInSc6	penicilin
rezistenci	rezistence	k1gFnSc4	rezistence
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
několika	několik	k4yIc2	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
bakterie	bakterie	k1gFnPc1	bakterie
mají	mít	k5eAaImIp3nP	mít
pozměněnou	pozměněný	k2eAgFnSc4d1	pozměněná
strukturu	struktura	k1gFnSc4	struktura
jmenovaných	jmenovaný	k2eAgInPc2d1	jmenovaný
enzymů	enzym	k1gInPc2	enzym
transpeptidáz	transpeptidáza	k1gFnPc2	transpeptidáza
a	a	k8xC	a
karboxypeptidáz	karboxypeptidáza	k1gFnPc2	karboxypeptidáza
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zamezí	zamezit	k5eAaPmIp3nP	zamezit
navázání	navázání	k1gNnSc3	navázání
penicilinu	penicilin	k1gInSc2	penicilin
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnPc1	bakterie
schopné	schopný	k2eAgFnPc1d1	schopná
tvořit	tvořit	k5eAaImF	tvořit
penicilinázu	penicilináza	k1gFnSc4	penicilináza
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
beta-laktamázy	betaaktamáza	k1gFnSc2	beta-laktamáza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
penicilin	penicilin	k1gInSc4	penicilin
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
ho	on	k3xPp3gMnSc4	on
zneškodňuje	zneškodňovat	k5eAaImIp3nS	zneškodňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
plísni	plíseň	k1gFnSc6	plíseň
Penicillium	Penicillium	k1gNnSc1	Penicillium
chrysogenum	chrysogenum	k1gNnSc1	chrysogenum
(	(	kIx(	(
<g/>
též	též	k9	též
P.	P.	kA	P.
notatum	notatum	k1gNnSc4	notatum
<g/>
)	)	kIx)	)
toto	tento	k3xDgNnSc4	tento
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
objevil	objevit	k5eAaPmAgMnS	objevit
Alexander	Alexandra	k1gFnPc2	Alexandra
Fleming	Fleming	k1gInSc4	Fleming
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Všiml	všimnout	k5eAaPmAgInS	všimnout
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Petriho	Petri	k1gMnSc2	Petri
misce	miska	k1gFnSc6	miska
s	s	k7c7	s
bakteriemi	bakterie	k1gFnPc7	bakterie
roste	růst	k5eAaImIp3nS	růst
plíseň	plíseň	k1gFnSc1	plíseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
okolní	okolní	k2eAgFnPc4d1	okolní
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
však	však	k9	však
angličtí	anglický	k2eAgMnPc1d1	anglický
vědci	vědec	k1gMnPc1	vědec
Howard	Howard	k1gMnSc1	Howard
Walter	Walter	k1gMnSc1	Walter
Florey	Florea	k1gFnSc2	Florea
a	a	k8xC	a
Ernst	Ernst	k1gMnSc1	Ernst
Boris	Boris	k1gMnSc1	Boris
Chain	Chain	k1gMnSc1	Chain
využili	využít	k5eAaPmAgMnP	využít
Flemingových	Flemingový	k2eAgNnPc2d1	Flemingové
pozorování	pozorování	k1gNnPc2	pozorování
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
izolovat	izolovat	k5eAaBmF	izolovat
penicilin	penicilin	k1gInSc4	penicilin
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
tito	tento	k3xDgMnPc1	tento
vědci	vědec	k1gMnPc1	vědec
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
penicilin	penicilin	k1gInSc1	penicilin
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
BF	BF	kA	BF
Mykoin	Mykoin	k1gInSc4	Mykoin
510	[number]	k4	510
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
připraven	připravit	k5eAaPmNgInS	připravit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
v	v	k7c6	v
chemicko-farmaceutické	chemickoarmaceutický	k2eAgFnSc6d1	chemicko-farmaceutická
továrně	továrna	k1gFnSc6	továrna
Benjamin	Benjamin	k1gMnSc1	Benjamin
Fragner	Fragner	k1gMnSc1	Fragner
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Měcholupech	Měcholup	k1gInPc6	Měcholup
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Zentiva	Zentiva	k1gFnSc1	Zentiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
penicilin	penicilin	k1gInSc1	penicilin
zachránil	zachránit	k5eAaPmAgInS	zachránit
82	[number]	k4	82
milionů	milion	k4xCgInPc2	milion
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
penicilin	penicilin	k1gInSc1	penicilin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
penicilin	penicilin	k1gInSc1	penicilin
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
penicilinu	penicilin	k1gInSc2	penicilin
</s>
</p>
