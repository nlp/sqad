<s>
První	první	k4xOgFnSc7	první
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
symphonic	symphonice	k1gFnPc2	symphonice
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nejspíše	nejspíše	k9	nejspíše
Therion	Therion	k1gInSc4	Therion
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
zapojili	zapojit	k5eAaPmAgMnP	zapojit
symfonický	symfonický	k2eAgInSc4d1	symfonický
orchestr	orchestr	k1gInSc4	orchestr
jako	jako	k8xS	jako
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
součást	součást	k1gFnSc4	součást
svého	svůj	k3xOyFgInSc2	svůj
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
odklonili	odklonit	k5eAaPmAgMnP	odklonit
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
předchozí	předchozí	k2eAgFnSc2d1	předchozí
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
