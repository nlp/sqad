<p>
<s>
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
je	být	k5eAaImIp3nS	být
právní	právní	k2eAgInSc4d1	právní
nástroj	nástroj	k1gInSc4	nástroj
ochrany	ochrana	k1gFnSc2	ochrana
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yRgFnSc2	který
firmy	firma	k1gFnSc2	firma
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
samy	sám	k3xTgFnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgInPc4	svůj
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zákazníky	zákazník	k1gMnPc4	zákazník
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c4	na
trh	trh	k1gInSc4	trh
přinášejí	přinášet	k5eAaImIp3nP	přinášet
stejné	stejný	k2eAgInPc1d1	stejný
nebo	nebo	k8xC	nebo
podobné	podobný	k2eAgInPc1d1	podobný
výrobky	výrobek	k1gInPc1	výrobek
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
za	za	k7c4	za
splnění	splnění	k1gNnSc4	splnění
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgFnPc2d1	stanovená
příslušným	příslušný	k2eAgInSc7d1	příslušný
právním	právní	k2eAgInSc7d1	právní
předpisem	předpis	k1gInSc7	předpis
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
označení	označení	k1gNnSc4	označení
schopné	schopný	k2eAgNnSc4d1	schopné
grafického	grafický	k2eAgNnSc2d1	grafické
znázornění	znázornění	k1gNnSc2	znázornění
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
například	například	k6eAd1	například
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
fráze	fráze	k1gFnPc4	fráze
nebo	nebo	k8xC	nebo
logo	logo	k1gNnSc4	logo
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
výrobku	výrobek	k1gInSc2	výrobek
či	či	k8xC	či
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kombinace	kombinace	k1gFnSc1	kombinace
předchozích	předchozí	k2eAgInPc2d1	předchozí
způsobů	způsob	k1gInPc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
známkové	známkový	k2eAgFnSc2d1	známková
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
explicitní	explicitní	k2eAgFnSc1d1	explicitní
ochrana	ochrana	k1gFnSc1	ochrana
obchodních	obchodní	k2eAgFnPc2d1	obchodní
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
jejich	jejich	k3xOp3gInPc2	jejich
symbolů	symbol	k1gInPc2	symbol
<g/>
)	)	kIx)	)
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
obecných	obecný	k2eAgInPc2d1	obecný
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
obchodní	obchodní	k2eAgInSc1d1	obchodní
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgFnSc1d1	obchodní
značka	značka	k1gFnSc1	značka
ani	ani	k8xC	ani
brand	brand	k1gInSc1	brand
se	se	k3xPyFc4	se
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
nestávají	stávat	k5eNaImIp3nP	stávat
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
zápisem	zápis	k1gInSc7	zápis
do	do	k7c2	do
příslušného	příslušný	k2eAgInSc2d1	příslušný
rejstříku	rejstřík	k1gInSc2	rejstřík
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
jako	jako	k8xS	jako
ochranné	ochranný	k2eAgFnPc4d1	ochranná
známky	známka	k1gFnPc4	známka
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
zapsány	zapsat	k5eAaPmNgInP	zapsat
i	i	k9	i
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
obchodní	obchodní	k2eAgFnSc4d1	obchodní
značku	značka	k1gFnSc4	značka
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
pak	pak	k6eAd1	pak
brand	branda	k1gFnPc2	branda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Registrace	registrace	k1gFnPc4	registrace
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
==	==	k?	==
</s>
</p>
<p>
<s>
Registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
v	v	k7c6	v
rejstříku	rejstřík	k1gInSc6	rejstřík
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
Úřad	úřad	k1gInSc1	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zápisem	zápis	k1gInSc7	zápis
nové	nový	k2eAgFnSc2d1	nová
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
tento	tento	k3xDgInSc1	tento
úřad	úřad	k1gInSc1	úřad
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
všechny	všechen	k3xTgFnPc4	všechen
předepsané	předepsaný	k2eAgFnPc4d1	předepsaná
náležitosti	náležitost	k1gFnPc4	náležitost
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
není	být	k5eNaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
nebo	nebo	k8xC	nebo
zaměnitelná	zaměnitelný	k2eAgFnSc1d1	zaměnitelná
s	s	k7c7	s
jinou	jiný	k2eAgFnSc7d1	jiná
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
a	a	k8xC	a
přihlášku	přihláška	k1gFnSc4	přihláška
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
také	také	k9	také
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
mohla	moct	k5eAaImAgFnS	moct
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
připomínek	připomínka	k1gFnPc2	připomínka
a	a	k8xC	a
námitek	námitka	k1gFnPc2	námitka
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
dotčená	dotčený	k2eAgFnSc1d1	dotčená
veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
podány	podán	k2eAgFnPc4d1	podána
námitky	námitka	k1gFnPc4	námitka
nebo	nebo	k8xC	nebo
připomínky	připomínka	k1gFnPc4	připomínka
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
přihlášeného	přihlášený	k2eAgNnSc2d1	přihlášené
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
zamítnuty	zamítnut	k2eAgFnPc1d1	zamítnuta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
zapsána	zapsat	k5eAaPmNgFnS	zapsat
do	do	k7c2	do
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
schopné	schopný	k2eAgNnSc1d1	schopné
zápisu	zápis	k1gInSc6	zápis
do	do	k7c2	do
rejstříku	rejstřík	k1gInSc2	rejstřík
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
označení	označení	k1gNnSc4	označení
vyhovující	vyhovující	k2eAgNnSc4d1	vyhovující
podmínkám	podmínka	k1gFnPc3	podmínka
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
rejstříku	rejstřík	k1gInSc2	rejstřík
ochranných	ochranný	k2eAgFnPc2d1	ochranná
</s>
</p>
<p>
<s>
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
schopnou	schopný	k2eAgFnSc7d1	schopná
zápisu	zápis	k1gInSc6	zápis
do	do	k7c2	do
rejstříku	rejstřík	k1gInSc2	rejstřík
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
může	moct	k5eAaImIp3nS	moct
</s>
</p>
<p>
<s>
být	být	k5eAaImF	být
<g/>
:	:	kIx,	:
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
označení	označení	k1gNnSc4	označení
schopné	schopný	k2eAgNnSc4d1	schopné
grafického	grafický	k2eAgNnSc2d1	grafické
znázornění	znázornění	k1gNnSc2	znázornění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
</s>
</p>
<p>
<s>
osobních	osobní	k2eAgNnPc2d1	osobní
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc4	kresba
<g/>
,	,	kIx,	,
písmena	písmeno	k1gNnPc4	písmeno
a	a	k8xC	a
číslice	číslice	k1gFnPc4	číslice
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
výrobku	výrobek	k1gInSc2	výrobek
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
obal	obal	k1gInSc4	obal
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
způsobilé	způsobilý	k2eAgNnSc1d1	způsobilé
odlišit	odlišit	k5eAaPmF	odlišit
výrobky	výrobek	k1gInPc4	výrobek
nebo	nebo	k8xC	nebo
služby	služba	k1gFnPc1	služba
jedné	jeden	k4xCgFnSc2	jeden
osoby	osoba	k1gFnSc2	osoba
od	od	k7c2	od
výrobků	výrobek	k1gInPc2	výrobek
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
služeb	služba	k1gFnPc2	služba
jiné	jiný	k2eAgFnSc2d1	jiná
osoby	osoba	k1gFnSc2	osoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povinnosti	povinnost	k1gFnSc2	povinnost
majitele	majitel	k1gMnSc2	majitel
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastník	vlastník	k1gMnSc1	vlastník
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
známku	známka	k1gFnSc4	známka
řádně	řádně	k6eAd1	řádně
užívat	užívat	k5eAaImF	užívat
a	a	k8xC	a
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vlastník	vlastník	k1gMnSc1	vlastník
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
nezačal	začít	k5eNaPmAgInS	začít
do	do	k7c2	do
5	[number]	k4	5
let	léto	k1gNnPc2	léto
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
zápisu	zápis	k1gInSc2	zápis
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
do	do	k7c2	do
rejstříku	rejstřík	k1gInSc2	rejstřík
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
řádně	řádně	k6eAd1	řádně
užívat	užívat	k5eAaImF	užívat
pro	pro	k7c4	pro
výrobky	výrobek	k1gInPc4	výrobek
nebo	nebo	k8xC	nebo
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
toto	tento	k3xDgNnSc1	tento
užívání	užívání	k1gNnSc1	užívání
bylo	být	k5eAaImAgNnS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
nejméně	málo	k6eAd3	málo
na	na	k7c4	na
nepřetržitou	přetržitý	k2eNgFnSc4d1	nepřetržitá
dobu	doba	k1gFnSc4	doba
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
známka	známka	k1gFnSc1	známka
zrušena	zrušen	k2eAgFnSc1d1	zrušena
na	na	k7c6	na
základě	základ	k1gInSc6	základ
§	§	k?	§
10	[number]	k4	10
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
31	[number]	k4	31
nebo	nebo	k8xC	nebo
32	[number]	k4	32
<g/>
c	c	k0	c
Zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
441	[number]	k4	441
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
,	,	kIx,	,
ledaže	ledaže	k8xS	ledaže
pro	pro	k7c4	pro
neužívání	neužívání	k1gNnSc4	neužívání
existují	existovat	k5eAaImIp3nP	existovat
řádné	řádný	k2eAgInPc1d1	řádný
důvody	důvod	k1gInPc1	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Řádným	řádný	k2eAgNnSc7d1	řádné
užíváním	užívání	k1gNnSc7	užívání
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
užívání	užívání	k1gNnSc1	užívání
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
základní	základní	k2eAgFnSc7d1	základní
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
zaručit	zaručit	k5eAaPmF	zaručit
identitu	identita	k1gFnSc4	identita
původu	původ	k1gInSc2	původ
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
případném	případný	k2eAgNnSc6d1	případné
prokazování	prokazování	k1gNnSc6	prokazování
užívání	užívání	k1gNnSc1	užívání
Úřad	úřad	k1gInSc1	úřad
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
zejména	zejména	k9	zejména
rozsah	rozsah	k1gInSc4	rozsah
a	a	k8xC	a
frekvenci	frekvence	k1gFnSc4	frekvence
užívání	užívání	k1gNnSc2	užívání
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
se	s	k7c7	s
zohledněním	zohlednění	k1gNnSc7	zohlednění
povahy	povaha	k1gFnSc2	povaha
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
současně	současně	k6eAd1	současně
charakteristiky	charakteristika	k1gFnPc4	charakteristika
trhu	trh	k1gInSc2	trh
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
již	již	k6eAd1	již
nebudou	být	k5eNaImBp3nP	být
konfliktní	konfliktní	k2eAgNnPc1d1	konfliktní
označení	označení	k1gNnPc1	označení
z	z	k7c2	z
úřední	úřední	k2eAgFnSc2d1	úřední
povinnosti	povinnost	k1gFnSc2	povinnost
zamítány	zamítán	k2eAgFnPc1d1	zamítána
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
Úřad	úřad	k1gInSc1	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
může	moct	k5eAaImIp3nS	moct
zapsat	zapsat	k5eAaPmF	zapsat
podobnou	podobný	k2eAgFnSc4d1	podobná
nebo	nebo	k8xC	nebo
totožnou	totožný	k2eAgFnSc4d1	totožná
známku	známka	k1gFnSc4	známka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
vlastníkovi	vlastník	k1gMnSc6	vlastník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sledoval	sledovat	k5eAaImAgInS	sledovat
rejstřík	rejstřík	k1gInSc1	rejstřík
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kolizní	kolizní	k2eAgFnSc2d1	kolizní
přihlášky	přihláška	k1gFnSc2	přihláška
ve	v	k7c6	v
lhůtě	lhůta	k1gFnSc6	lhůta
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
podal	podat	k5eAaPmAgMnS	podat
námitky	námitka	k1gFnSc2	námitka
proti	proti	k7c3	proti
zápisu	zápis	k1gInSc3	zápis
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
budou	být	k5eAaImBp3nP	být
tato	tento	k3xDgNnPc4	tento
konfliktní	konfliktní	k2eAgNnPc4d1	konfliktní
označení	označení	k1gNnPc4	označení
platně	platně	k6eAd1	platně
zapsána	zapsat	k5eAaPmNgNnP	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
povinnost	povinnost	k1gFnSc1	povinnost
je	být	k5eAaImIp3nS	být
zákonné	zákonný	k2eAgNnSc4d1	zákonné
vtělení	vtělení	k1gNnSc4	vtělení
zásady	zásada	k1gFnSc2	zásada
vigitanbilus	vigitanbilus	k1gMnSc1	vigitanbilus
iura	iura	k1gMnSc1	iura
scripta	scripta	k1gMnSc1	scripta
sunt	sunt	k1gMnSc1	sunt
neboli	neboli	k8xC	neboli
práva	právo	k1gNnPc4	právo
náleží	náležet	k5eAaImIp3nS	náležet
bdělým	bdělý	k2eAgMnSc7d1	bdělý
(	(	kIx(	(
<g/>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
j.	j.	k?	j.
7	[number]	k4	7
As	as	k9	as
140	[number]	k4	140
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
monitoringu	monitoring	k1gInSc3	monitoring
slouží	sloužit	k5eAaImIp3nS	sloužit
pravidelný	pravidelný	k2eAgMnSc1d1	pravidelný
věstník	věstník	k1gMnSc1	věstník
Úřadu	úřad	k1gInSc2	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
v	v	k7c6	v
papírové	papírový	k2eAgFnSc6d1	papírová
podobě	podoba	k1gFnSc6	podoba
nebo	nebo	k8xC	nebo
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
služby	služba	k1gFnPc4	služba
on-line	onin	k1gInSc5	on-lin
monitoringu	monitoring	k1gInSc6	monitoring
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Používané	používaný	k2eAgFnPc4d1	používaná
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
národní	národní	k2eAgFnPc1d1	národní
ochranné	ochranný	k2eAgFnPc1d1	ochranná
známky	známka	k1gFnPc1	známka
(	(	kIx(	(
<g/>
rejstřík	rejstřík	k1gInSc1	rejstřík
ÚPV	ÚPV	kA	ÚPV
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
ochranné	ochranný	k2eAgFnPc1d1	ochranná
známky	známka	k1gFnPc1	známka
(	(	kIx(	(
<g/>
rejstřík	rejstřík	k1gInSc1	rejstřík
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ochranné	ochranný	k2eAgFnPc1d1	ochranná
známky	známka	k1gFnPc1	známka
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
rejstřík	rejstřík	k1gInSc1	rejstřík
EUIPO	EUIPO	kA	EUIPO
v	v	k7c6	v
Alicante	Alicant	k1gMnSc5	Alicant
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
všeobecně	všeobecně	k6eAd1	všeobecně
známé	známý	k2eAgFnPc4d1	známá
ochranné	ochranný	k2eAgFnPc4d1	ochranná
známky	známka	k1gFnPc4	známka
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
TRIPS	TRIPS	kA	TRIPS
a	a	k8xC	a
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
)	)	kIx)	)
<g/>
Platnost	platnost	k1gFnSc1	platnost
<g/>
:	:	kIx,	:
10	[number]	k4	10
let	léto	k1gNnPc2	léto
od	od	k7c2	od
podání	podání	k1gNnSc2	podání
přihlášky	přihláška	k1gFnSc2	přihláška
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
prodloužení	prodloužení	k1gNnSc2	prodloužení
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
platnosti	platnost	k1gFnSc2	platnost
</s>
</p>
<p>
<s>
-ochranná	chranný	k2eAgFnSc1d1	-ochranný
známka	známka	k1gFnSc1	známka
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
na	na	k7c4	na
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
firmy	firma	k1gFnPc1	firma
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
subjekt	subjekt	k1gInSc1	subjekt
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
subjekt	subjekt	k1gInSc1	subjekt
podniká	podnikat	k5eAaImIp3nS	podnikat
jenom	jenom	k9	jenom
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
exportuje	exportovat	k5eAaBmIp3nS	exportovat
například	například	k6eAd1	například
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pořídit	pořídit	k5eAaPmF	pořídit
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
má	mít	k5eAaImIp3nS	mít
subjekt	subjekt	k1gInSc4	subjekt
větší	veliký	k2eAgFnSc2d2	veliký
ambice	ambice	k1gFnSc2	ambice
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
obchodovat	obchodovat	k5eAaImF	obchodovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pořídit	pořídit	k5eAaPmF	pořídit
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
přímo	přímo	k6eAd1	přímo
pro	pro	k7c4	pro
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
podmíněná	podmíněný	k2eAgFnSc1d1	podmíněná
existencí	existence	k1gFnSc7	existence
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Značení	značení	k1gNnSc4	značení
obchodních	obchodní	k2eAgFnPc2d1	obchodní
značek	značka	k1gFnPc2	značka
===	===	k?	===
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgFnPc1d1	obchodní
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
označovány	označován	k2eAgInPc4d1	označován
následující	následující	k2eAgInPc4d1	následující
symboly	symbol	k1gInPc4	symbol
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
TM	TM	kA	TM
–	–	k?	–
neregistrovaná	registrovaný	k2eNgFnSc1d1	neregistrovaná
obchodní	obchodní	k2eAgFnSc1d1	obchodní
značka	značka	k1gFnSc1	značka
</s>
</p>
<p>
<s>
SM	SM	kA	SM
–	–	k?	–
neregistrovaná	registrovaný	k2eNgFnSc1d1	neregistrovaná
značka	značka	k1gFnSc1	značka
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
značka	značka	k1gFnSc1	značka
používaná	používaný	k2eAgFnSc1d1	používaná
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
značkové	značkový	k2eAgFnPc4d1	značková
služby	služba	k1gFnPc4	služba
</s>
</p>
<p>
<s>
®	®	k?	®
–	–	k?	–
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
</s>
</p>
<p>
<s>
==	==	k?	==
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
značka	značka	k1gFnSc1	značka
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
==	==	k?	==
</s>
</p>
<p>
<s>
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Vlastník	vlastník	k1gMnSc1	vlastník
registrované	registrovaný	k2eAgFnSc2d1	registrovaná
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jejího	její	k3xOp3gNnSc2	její
porušení	porušení	k1gNnSc2	porušení
započít	započít	k5eAaPmF	započít
právní	právní	k2eAgInPc4d1	právní
úkony	úkon	k1gInPc4	úkon
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
neoprávněného	oprávněný	k2eNgNnSc2d1	neoprávněné
použití	použití	k1gNnSc2	použití
této	tento	k3xDgFnSc2	tento
známky	známka	k1gFnSc2	známka
<g/>
,	,	kIx,	,
registrace	registrace	k1gFnSc2	registrace
známky	známka	k1gFnSc2	známka
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
nevyžaduje	vyžadovat	k5eNaImIp3nS	vyžadovat
<g/>
.	.	kIx.	.
</s>
<s>
Vlastník	vlastník	k1gMnSc1	vlastník
neregistrované	registrovaný	k2eNgFnSc2d1	neregistrovaná
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
též	též	k9	též
může	moct	k5eAaImIp3nS	moct
podniknout	podniknout	k5eAaPmF	podniknout
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
svého	svůj	k3xOyFgNnSc2	svůj
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
započatých	započatý	k2eAgNnPc6d1	započaté
řízeních	řízení	k1gNnPc6	řízení
prokazovat	prokazovat	k5eAaImF	prokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
své	svůj	k3xOyFgNnSc4	svůj
označení	označení	k1gNnSc4	označení
užívá	užívat	k5eAaImIp3nS	užívat
a	a	k8xC	a
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gMnPc7	druh
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
novelizace	novelizace	k1gFnSc2	novelizace
1.1	[number]	k4	1.1
<g/>
.2019	.2019	k4	.2019
lze	lze	k6eAd1	lze
nyní	nyní	k6eAd1	nyní
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
následující	následující	k2eAgInPc4d1	následující
druhy	druh	k1gInPc4	druh
OZ	OZ	kA	OZ
</s>
</p>
<p>
<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
výlučně	výlučně	k6eAd1	výlučně
slovy	slovo	k1gNnPc7	slovo
nebo	nebo	k8xC	nebo
písmeny	písmeno	k1gNnPc7	písmeno
v	v	k7c6	v
latince	latinka	k1gFnSc6	latinka
<g/>
,	,	kIx,	,
arabskými	arabský	k2eAgFnPc7d1	arabská
nebo	nebo	k8xC	nebo
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
obvyklými	obvyklý	k2eAgInPc7d1	obvyklý
typografickými	typografický	k2eAgInPc7d1	typografický
znaky	znak	k1gInPc7	znak
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc7	jejich
kombinací	kombinace	k1gFnSc7	kombinace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
reprodukce	reprodukce	k1gFnSc2	reprodukce
označení	označení	k1gNnSc2	označení
s	s	k7c7	s
obvyklým	obvyklý	k2eAgNnSc7d1	obvyklé
písmem	písmo	k1gNnSc7	písmo
a	a	k8xC	a
v	v	k7c6	v
obvyklém	obvyklý	k2eAgNnSc6d1	obvyklé
rozvržení	rozvržení	k1gNnSc6	rozvržení
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
grafických	grafický	k2eAgInPc2d1	grafický
prvků	prvek	k1gInPc2	prvek
nebo	nebo	k8xC	nebo
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrazová	obrazový	k2eAgFnSc1d1	obrazová
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
styly	styl	k1gInPc1	styl
nebo	nebo	k8xC	nebo
rozvržením	rozvržení	k1gNnSc7	rozvržení
<g/>
,	,	kIx,	,
grafickým	grafický	k2eAgInSc7d1	grafický
prvkem	prvek	k1gInSc7	prvek
nebo	nebo	k8xC	nebo
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
výlučně	výlučně	k6eAd1	výlučně
obrazovými	obrazový	k2eAgInPc7d1	obrazový
prvky	prvek	k1gInPc7	prvek
nebo	nebo	k8xC	nebo
kombinací	kombinace	k1gFnSc7	kombinace
slovních	slovní	k2eAgInPc2d1	slovní
a	a	k8xC	a
obrazových	obrazový	k2eAgInPc2d1	obrazový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
reprodukce	reprodukce	k1gFnSc2	reprodukce
označení	označení	k1gNnSc2	označení
<g/>
,	,	kIx,	,
o	o	k7c4	o
jehož	jehož	k3xOyRp3gInSc4	jehož
zápis	zápis	k1gInSc4	zápis
se	se	k3xPyFc4	se
žádá	žádat	k5eAaImIp3nS	žádat
<g/>
,	,	kIx,	,
znázorňující	znázorňující	k2eAgFnSc1d1	znázorňující
veškeré	veškerý	k3xTgInPc4	veškerý
její	její	k3xOp3gInPc4	její
prvky	prvek	k1gInPc4	prvek
a	a	k8xC	a
případně	případně	k6eAd1	případně
také	také	k9	také
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostorová	prostorový	k2eAgFnSc1d1	prostorová
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
nebo	nebo	k8xC	nebo
doplněna	doplnit	k5eAaPmNgFnS	doplnit
trojrozměrným	trojrozměrný	k2eAgInSc7d1	trojrozměrný
tvarem	tvar	k1gInSc7	tvar
<g/>
,	,	kIx,	,
znázorňujícím	znázorňující	k2eAgInSc7d1	znázorňující
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
,	,	kIx,	,
obal	obal	k1gInSc1	obal
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
výrobek	výrobek	k1gInSc1	výrobek
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gNnSc1	jeho
ztvárnění	ztvárnění	k1gNnSc1	ztvárnění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
grafické	grafický	k2eAgFnSc2d1	grafická
reprodukce	reprodukce	k1gFnSc2	reprodukce
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
i	i	k8xC	i
obrazy	obraz	k1gInPc1	obraz
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
počítačem	počítač	k1gInSc7	počítač
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
fotografické	fotografický	k2eAgFnSc2d1	fotografická
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
.	.	kIx.	.
</s>
<s>
Grafická	grafický	k2eAgFnSc1d1	grafická
nebo	nebo	k8xC	nebo
fotografická	fotografický	k2eAgFnSc1d1	fotografická
reprodukce	reprodukce	k1gFnSc1	reprodukce
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
různé	různý	k2eAgInPc4d1	různý
náhledy	náhled	k1gInPc4	náhled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poziční	poziční	k2eAgFnSc1d1	poziční
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
je	být	k5eAaImIp3nS	být
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
na	na	k7c6	na
výrobku	výrobek	k1gInSc6	výrobek
umístěna	umístit	k5eAaPmNgFnS	umístit
nebo	nebo	k8xC	nebo
jakým	jaký	k3yQgNnSc7	jaký
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
připevněna	připevněn	k2eAgFnSc1d1	připevněna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
reprodukce	reprodukce	k1gFnSc2	reprodukce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
náležitě	náležitě	k6eAd1	náležitě
určuje	určovat	k5eAaImIp3nS	určovat
pozici	pozice	k1gFnSc4	pozice
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
nebo	nebo	k8xC	nebo
proporce	proporce	k1gFnPc1	proporce
u	u	k7c2	u
příslušných	příslušný	k2eAgInPc2d1	příslušný
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
netvoří	tvořit	k5eNaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
předmětu	předmět	k1gInSc2	předmět
zápisu	zápis	k1gInSc2	zápis
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vizuálně	vizuálně	k6eAd1	vizuálně
označí	označit	k5eAaPmIp3nS	označit
nejlépe	dobře	k6eAd3	dobře
přerušovanými	přerušovaný	k2eAgFnPc7d1	přerušovaná
nebo	nebo	k8xC	nebo
tečkovanými	tečkovaný	k2eAgFnPc7d1	tečkovaná
čarami	čára	k1gFnPc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádření	vyjádření	k1gNnSc1	vyjádření
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
popisem	popis	k1gInSc7	popis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podrobně	podrobně	k6eAd1	podrobně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
k	k	k7c3	k
výrobku	výrobek	k1gInSc3	výrobek
připevněna	připevněn	k2eAgFnSc1d1	připevněna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
se	s	k7c7	s
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
výlučně	výlučně	k6eAd1	výlučně
sadou	sada	k1gFnSc7	sada
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
reprodukce	reprodukce	k1gFnSc2	reprodukce
znázorňující	znázorňující	k2eAgFnSc2d1	znázorňující
opakující	opakující	k2eAgInSc4d1	opakující
se	se	k3xPyFc4	se
vzor	vzor	k1gInSc4	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádření	vyjádření	k1gNnSc1	vyjádření
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
popisem	popis	k1gInSc7	popis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podrobně	podrobně	k6eAd1	podrobně
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
pravidelně	pravidelně	k6eAd1	pravidelně
opakují	opakovat	k5eAaImIp3nP	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barevná	barevný	k2eAgFnSc1d1	barevná
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
výlučně	výlučně	k6eAd1	výlučně
barvou	barva	k1gFnSc7	barva
bez	bez	k7c2	bez
obrysů	obrys	k1gInPc2	obrys
nebo	nebo	k8xC	nebo
výlučně	výlučně	k6eAd1	výlučně
barevnou	barevný	k2eAgFnSc7d1	barevná
kombinací	kombinace	k1gFnSc7	kombinace
bez	bez	k7c2	bez
obrysů	obrys	k1gInPc2	obrys
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
-	-	kIx~	-
reprodukce	reprodukce	k1gFnSc2	reprodukce
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
označením	označení	k1gNnSc7	označení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
kód	kód	k1gInSc4	kód
barvy	barva	k1gFnPc4	barva
(	(	kIx(	(
<g/>
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
například	například	k6eAd1	například
CMYK	CMYK	kA	CMYK
<g/>
,	,	kIx,	,
PANTONE	PANTONE	kA	PANTONE
<g/>
,	,	kIx,	,
RAL	RAL	kA	RAL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
-	-	kIx~	-
reprodukce	reprodukce	k1gFnSc1	reprodukce
znázorňující	znázorňující	k2eAgFnSc1d1	znázorňující
systematické	systematický	k2eAgNnSc4d1	systematické
uspořádání	uspořádání	k1gNnSc4	uspořádání
barevné	barevný	k2eAgFnSc2d1	barevná
kombinace	kombinace	k1gFnSc2	kombinace
jednotným	jednotný	k2eAgInPc3d1	jednotný
a	a	k8xC	a
předem	předem	k6eAd1	předem
stanoveným	stanovený	k2eAgInSc7d1	stanovený
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
označením	označení	k1gNnSc7	označení
těchto	tento	k3xDgFnPc2	tento
barev	barva	k1gFnPc2	barva
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
odkazu	odkaz	k1gInSc2	odkaz
na	na	k7c4	na
všeobecně	všeobecně	k6eAd1	všeobecně
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
kód	kód	k1gInSc4	kód
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvuková	zvukový	k2eAgFnSc1d1	zvuková
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
výlučně	výlučně	k6eAd1	výlučně
zvukem	zvuk	k1gInSc7	zvuk
nebo	nebo	k8xC	nebo
kombinací	kombinace	k1gFnSc7	kombinace
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
audionahrávky	audionahrávka	k1gFnSc2	audionahrávka
reprodukující	reprodukující	k2eAgInSc1d1	reprodukující
zvuk	zvuk	k1gInSc1	zvuk
nebo	nebo	k8xC	nebo
přesným	přesný	k2eAgNnSc7d1	přesné
zachycením	zachycení	k1gNnSc7	zachycení
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
notovém	notový	k2eAgInSc6d1	notový
zápisu	zápis	k1gInSc6	zápis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohybová	pohybový	k2eAgFnSc1d1	pohybová
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
nebo	nebo	k8xC	nebo
doplněna	doplnit	k5eAaPmNgFnS	doplnit
pohybem	pohyb	k1gInSc7	pohyb
nebo	nebo	k8xC	nebo
změnou	změna	k1gFnSc7	změna
pozice	pozice	k1gFnSc2	pozice
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
ochranné	ochranný	k2eAgFnSc6d1	ochranná
známce	známka	k1gFnSc6	známka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
videonahrávky	videonahrávka	k1gFnSc2	videonahrávka
nebo	nebo	k8xC	nebo
řady	řada	k1gFnSc2	řada
statických	statický	k2eAgInPc2d1	statický
obrazů	obraz	k1gInPc2	obraz
v	v	k7c6	v
příslušném	příslušný	k2eAgNnSc6d1	příslušné
pořadí	pořadí	k1gNnSc6	pořadí
znázorňujících	znázorňující	k2eAgInPc2d1	znázorňující
pohyb	pohyb	k1gInSc4	pohyb
nebo	nebo	k8xC	nebo
změnu	změna	k1gFnSc4	změna
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
li	li	k8xS	li
použity	použit	k2eAgInPc1d1	použit
statické	statický	k2eAgInPc1d1	statický
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
očíslovány	očíslovat	k5eAaPmNgFnP	očíslovat
nebo	nebo	k8xC	nebo
doprovázeny	doprovázet	k5eAaImNgFnP	doprovázet
popisem	popis	k1gInSc7	popis
vysvětlujícím	vysvětlující	k2eAgInSc7d1	vysvětlující
jejich	jejich	k3xOp3gNnSc3	jejich
pořadí	pořadí	k1gNnSc3	pořadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Multimediální	multimediální	k2eAgFnSc1d1	multimediální
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
nebo	nebo	k8xC	nebo
doplněna	doplnit	k5eAaPmNgFnS	doplnit
kombinací	kombinace	k1gFnSc7	kombinace
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
audiovizuální	audiovizuální	k2eAgFnSc2d1	audiovizuální
nahrávky	nahrávka	k1gFnSc2	nahrávka
obsahující	obsahující	k2eAgFnSc4d1	obsahující
kombinaci	kombinace	k1gFnSc4	kombinace
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Holografická	holografický	k2eAgFnSc1d1	holografická
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
prvky	prvek	k1gInPc4	prvek
s	s	k7c7	s
holografickými	holografický	k2eAgInPc7d1	holografický
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
předložením	předložení	k1gNnSc7	předložení
videonahrávky	videonahrávka	k1gFnSc2	videonahrávka
nebo	nebo	k8xC	nebo
grafické	grafický	k2eAgFnSc2d1	grafická
nebo	nebo	k8xC	nebo
fotografické	fotografický	k2eAgFnSc2d1	fotografická
reprodukce	reprodukce	k1gFnSc2	reprodukce
obsahující	obsahující	k2eAgInPc1d1	obsahující
náhledy	náhled	k1gInPc1	náhled
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
pro	pro	k7c4	pro
náležité	náležitý	k2eAgNnSc4d1	náležité
rozpoznání	rozpoznání	k1gNnSc4	rozpoznání
holografického	holografický	k2eAgInSc2d1	holografický
efektu	efekt	k1gInSc2	efekt
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
druh	druh	k1gInSc1	druh
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
žádnému	žádný	k3yNgMnSc3	žádný
z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
druhů	druh	k1gInPc2	druh
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
v	v	k7c6	v
jakékoli	jakýkoli	k3yIgFnSc6	jakýkoli
vhodné	vhodný	k2eAgFnSc6d1	vhodná
formě	forma	k1gFnSc6	forma
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
všeobecně	všeobecně	k6eAd1	všeobecně
dostupných	dostupný	k2eAgFnPc2d1	dostupná
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
jasně	jasně	k6eAd1	jasně
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
dostupným	dostupný	k2eAgInSc7d1	dostupný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
srozumitelně	srozumitelně	k6eAd1	srozumitelně
<g/>
,	,	kIx,	,
trvanlivě	trvanlivě	k6eAd1	trvanlivě
a	a	k8xC	a
objektivně	objektivně	k6eAd1	objektivně
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
v	v	k7c6	v
rejstříku	rejstřík	k1gInSc6	rejstřík
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
příslušným	příslušný	k2eAgInSc7d1	příslušný
orgánům	orgán	k1gInPc3	orgán
i	i	k8xC	i
veřejnosti	veřejnost	k1gFnSc6	veřejnost
umožněno	umožnit	k5eAaPmNgNnS	umožnit
jasně	jasně	k6eAd1	jasně
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
předmět	předmět	k1gInSc4	předmět
ochrany	ochrana	k1gFnSc2	ochrana
poskytovaný	poskytovaný	k2eAgInSc4d1	poskytovaný
vlastníkovi	vlastník	k1gMnSc3	vlastník
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklady	příklad	k1gInPc1	příklad
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
===	===	k?	===
</s>
</p>
<p>
<s>
SlovaNejčastější	SlovaNejčastý	k2eAgFnSc1d2	SlovaNejčastý
forma	forma	k1gFnSc1	forma
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
je	být	k5eAaImIp3nS	být
slovní	slovní	k2eAgNnSc1d1	slovní
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
</s>
</p>
<p>
<s>
ČKD	ČKD	kA	ČKD
</s>
</p>
<p>
<s>
Škoda	škoda	k1gFnSc1	škoda
</s>
</p>
<p>
<s>
BudweiserLogoJe	BudweiserLogoJe	k6eAd1	BudweiserLogoJe
druhá	druhý	k4xOgFnSc1	druhý
nejčastější	častý	k2eAgFnSc1d3	nejčastější
forma	forma	k1gFnSc1	forma
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Logo	logo	k1gNnSc1	logo
je	být	k5eAaImIp3nS	být
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
stává	stávat	k5eAaImIp3nS	stávat
symbolem	symbol	k1gInSc7	symbol
spojeným	spojený	k2eAgInSc7d1	spojený
se	s	k7c7	s
zbožím	zboží	k1gNnSc7	zboží
nebo	nebo	k8xC	nebo
službou	služba	k1gFnSc7	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
McDonald	McDonald	k1gInSc1	McDonald
–	–	k?	–
dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
oblouk	oblouk	k1gInSc1	oblouk
</s>
</p>
<p>
<s>
Apple	Apple	kA	Apple
–	–	k?	–
nakouslé	nakouslý	k2eAgNnSc4d1	nakouslé
jablko	jablko	k1gNnSc4	jablko
</s>
</p>
<p>
<s>
Škoda	škoda	k1gFnSc1	škoda
–	–	k?	–
okřídlený	okřídlený	k2eAgInSc4d1	okřídlený
šípKombinace	šípKombinace	k1gFnPc4	šípKombinace
písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
grafikyIBM	grafikyIBM	k?	grafikyIBM
</s>
</p>
<p>
<s>
Sun	Sun	kA	Sun
</s>
</p>
<p>
<s>
Digital	Digital	kA	Digital
</s>
</p>
<p>
<s>
JavaTvar	JavaTvar	k1gInSc1	JavaTvar
výrobku	výrobek	k1gInSc2	výrobek
nebo	nebo	k8xC	nebo
obaluláhev	obaluláhev	k1gFnSc4	obaluláhev
Coca-ColaNejčastěji	Coca-ColaNejčastěji	k1gFnSc2	Coca-ColaNejčastěji
registrované	registrovaný	k2eAgFnSc2d1	registrovaná
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známkyŠkoda	známkyŠkoda	k1gMnSc1	známkyŠkoda
<g/>
,	,	kIx,	,
Omega	omega	k1gFnSc1	omega
<g/>
,	,	kIx,	,
Titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
Cobra	Cobra	k1gMnSc1	Cobra
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
Matrix	Matrix	k1gInSc1	Matrix
<g/>
,	,	kIx,	,
Evolution	Evolution	k1gInSc1	Evolution
<g/>
,	,	kIx,	,
Eclipse	Eclipse	k1gFnSc1	Eclipse
<g/>
,	,	kIx,	,
Optima	optimum	k1gNnSc2	optimum
<g/>
,	,	kIx,	,
Fusion	Fusion	k1gInSc1	Fusion
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
Vision	vision	k1gInSc1	vision
<g/>
,	,	kIx,	,
Delta	delta	k1gFnSc1	delta
<g/>
,	,	kIx,	,
Smart	Smart	k1gInSc1	Smart
<g/>
,	,	kIx,	,
Star	Star	kA	Star
<g/>
,	,	kIx,	,
Prima	prima	k6eAd1	prima
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnota	hodnota	k1gFnSc1	hodnota
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
==	==	k?	==
</s>
</p>
<p>
<s>
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
práva	právo	k1gNnPc4	právo
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hodnota	hodnota	k1gFnSc1	hodnota
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
firma	firma	k1gFnSc1	firma
kupuje	kupovat	k5eAaImIp3nS	kupovat
jinou	jiný	k2eAgFnSc4d1	jiná
firmu	firma	k1gFnSc4	firma
pouze	pouze	k6eAd1	pouze
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
jejich	jejich	k3xOp3gFnPc2	jejich
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Absence	absence	k1gFnSc1	absence
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
==	==	k?	==
</s>
</p>
<p>
<s>
Nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
ochrana	ochrana	k1gFnSc1	ochrana
obchodního	obchodní	k2eAgInSc2d1	obchodní
názvu	název	k1gInSc2	název
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
už	už	k9	už
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
název	název	k1gInSc4	název
i	i	k8xC	i
napodobeninu	napodobenina	k1gFnSc4	napodobenina
produktu	produkt	k1gInSc2	produkt
převzala	převzít	k5eAaPmAgFnS	převzít
konkurence	konkurence	k1gFnSc1	konkurence
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
původní	původní	k2eAgMnSc1d1	původní
vlastník	vlastník	k1gMnSc1	vlastník
"	"	kIx"	"
<g/>
nápadu	nápad	k1gInSc2	nápad
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gMnPc1	jeho
dědicové	dědic	k1gMnPc1	dědic
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
neměli	mít	k5eNaImAgMnP	mít
jak	jak	k9	jak
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
<g/>
Příklady	příklad	k1gInPc4	příklad
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
šunka	šunka	k1gFnSc1	šunka
</s>
</p>
<p>
<s>
Talián	talián	k1gInSc1	talián
</s>
</p>
<p>
<s>
==	==	k?	==
Obnova	obnova	k1gFnSc1	obnova
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
má	mít	k5eAaImIp3nS	mít
platnost	platnost	k1gFnSc4	platnost
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
si	se	k3xPyFc3	se
tuto	tento	k3xDgFnSc4	tento
platnost	platnost	k1gFnSc4	platnost
prodloužit	prodloužit	k5eAaPmF	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochranných	ochranný	k2eAgFnPc6d1	ochranná
známkách	známka	k1gFnPc6	známka
se	se	k3xPyFc4	se
platnost	platnost	k1gFnSc1	platnost
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zažádat	zažádat	k5eAaPmF	zažádat
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
lze	lze	k6eAd1	lze
jenom	jenom	k9	jenom
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
platnosti	platnost	k1gFnSc2	platnost
zápisu	zápis	k1gInSc2	zápis
ochranné	ochranný	k2eAgFnPc4d1	ochranná
známky	známka	k1gFnPc4	známka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zaplatit	zaplatit	k5eAaPmF	zaplatit
správní	správní	k2eAgInSc4d1	správní
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
obnovu	obnova	k1gFnSc4	obnova
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
500	[number]	k4	500
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastník	vlastník	k1gMnSc1	vlastník
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
zapomene	zapomnět	k5eAaImIp3nS	zapomnět
ve	v	k7c6	v
stanovené	stanovený	k2eAgFnSc6d1	stanovená
lhůtě	lhůta	k1gFnSc6	lhůta
podat	podat	k5eAaPmF	podat
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ještě	ještě	k9	ještě
možnost	možnost	k1gFnSc4	možnost
zažádat	zažádat	k5eAaPmF	zažádat
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
vypršení	vypršení	k1gNnSc2	vypršení
platnosti	platnost	k1gFnSc2	platnost
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
poplatek	poplatek	k1gInSc1	poplatek
vyšší	vysoký	k2eAgInSc1d2	vyšší
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
5	[number]	k4	5
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
majitel	majitel	k1gMnSc1	majitel
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
o	o	k7c6	o
prodloužení	prodloužení	k1gNnSc6	prodloužení
nezažádá	zažádat	k5eNaPmIp3nS	zažádat
<g/>
,	,	kIx,	,
zápis	zápis	k1gInSc1	zápis
známky	známka	k1gFnSc2	známka
se	se	k3xPyFc4	se
zruší	zrušit	k5eAaPmIp3nS	zrušit
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
známku	známka	k1gFnSc4	známka
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
prodloužení	prodloužení	k1gNnSc4	prodloužení
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
vyplnit	vyplnit	k5eAaPmF	vyplnit
potřebný	potřebný	k2eAgInSc4d1	potřebný
formulář	formulář	k1gInSc4	formulář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
podá	podat	k5eAaPmIp3nS	podat
na	na	k7c4	na
Úřad	úřad	k1gInSc4	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KOL	kol	k6eAd1	kol
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vytvořit	vytvořit	k5eAaPmF	vytvořit
atraktivní	atraktivní	k2eAgInSc4d1	atraktivní
obchodní	obchodní	k2eAgInSc4d1	obchodní
název	název	k1gInSc4	název
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
značky	značka	k1gFnSc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NZB	NZB	kA	NZB
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
340	[number]	k4	340
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
904272	[number]	k4	904272
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
441	[number]	k4	441
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochranných	ochranný	k2eAgFnPc6d1	ochranná
známkách	známka	k1gFnPc6	známka
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
nahradil	nahradit	k5eAaPmAgInS	nahradit
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
137	[number]	k4	137
<g/>
/	/	kIx~	/
<g/>
1995	[number]	k4	1995
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ochranných	ochranný	k2eAgFnPc6d1	ochranná
známkách	známka	k1gFnPc6	známka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Madridská	madridský	k2eAgFnSc1d1	Madridská
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
zápisu	zápis	k1gInSc6	zápis
továrních	tovární	k2eAgFnPc2d1	tovární
nebo	nebo	k8xC	nebo
obchodních	obchodní	k2eAgFnPc2d1	obchodní
známek	známka	k1gFnPc2	známka
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
revidovaná	revidovaný	k2eAgFnSc1d1	revidovaná
<g/>
;	;	kIx,	;
v	v	k7c6	v
ČR	ČR	kA	ČR
vyhl	vyhl	k1gMnSc1	vyhl
<g/>
.	.	kIx.	.
65	[number]	k4	65
<g/>
/	/	kIx~	/
<g/>
1975	[number]	k4	1975
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Protokol	protokol	k1gInSc1	protokol
k	k	k7c3	k
Madridské	madridský	k2eAgFnSc3d1	Madridská
dohodě	dohoda	k1gFnSc3	dohoda
ze	z	k7c2	z
dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
třídění	třídění	k1gNnSc2	třídění
obrazových	obrazový	k2eAgInPc2d1	obrazový
prvků	prvek	k1gInPc2	prvek
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
(	(	kIx(	(
<g/>
ČR	ČR	kA	ČR
není	být	k5eNaImIp3nS	být
smluvní	smluvní	k2eAgFnSc7d1	smluvní
stranou	strana	k1gFnSc7	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Niceská	niceský	k2eAgFnSc1d1	Niceská
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
třídění	třídění	k1gNnSc6	třídění
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
zápisu	zápis	k1gInSc2	zápis
známek	známka	k1gFnPc2	známka
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
TM-XML	TM-XML	k?	TM-XML
</s>
</p>
<p>
<s>
obchodní	obchodní	k2eAgFnSc1d1	obchodní
značka	značka	k1gFnSc1	značka
</s>
</p>
<p>
<s>
zeměpisné	zeměpisný	k2eAgNnSc1d1	zeměpisné
označení	označení	k1gNnSc1	označení
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
a	a	k8xC	a
komunitární	komunitární	k2eAgFnSc1d1	komunitární
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
</s>
</p>
<p>
<s>
ÚPV	ÚPV	kA	ÚPV
Databáze	databáze	k1gFnSc1	databáze
českých	český	k2eAgFnPc2d1	Česká
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
</s>
</p>
<p>
<s>
EUIPO	EUIPO	kA	EUIPO
Úřad	úřad	k1gInSc1	úřad
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
pro	pro	k7c4	pro
duševní	duševní	k2eAgNnSc4d1	duševní
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
</s>
</p>
<p>
<s>
WIPO	WIPO	kA	WIPO
Světová	světový	k2eAgFnSc1d1	světová
organizace	organizace	k1gFnSc1	organizace
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
</s>
</p>
