<s>
Lásky	láska	k1gFnPc1	láska
jedné	jeden	k4xCgFnSc2	jeden
plavovlásky	plavovláska	k1gFnSc2	plavovláska
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
černobílý	černobílý	k2eAgInSc1d1	černobílý
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
filmový	filmový	k2eAgInSc1d1	filmový
časopis	časopis	k1gInSc1	časopis
Empire	empir	k1gInSc5	empir
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
cizojazyčných	cizojazyčný	k2eAgInPc2d1	cizojazyčný
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
mladá	mladý	k2eAgFnSc1d1	mladá
dělnice	dělnice	k1gFnSc1	dělnice
z	z	k7c2	z
malého	malý	k2eAgNnSc2d1	malé
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Hana	Hana	k1gFnSc1	Hana
Brejchová	Brejchová	k1gFnSc1	Brejchová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hledá	hledat	k5eAaImIp3nS	hledat
pravou	pravý	k2eAgFnSc4d1	pravá
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
zábavě	zábava	k1gFnSc6	zábava
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
mladý	mladý	k2eAgMnSc1d1	mladý
muzikant	muzikant	k1gMnSc1	muzikant
(	(	kIx(	(
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Pucholt	Pucholt	k1gMnSc1	Pucholt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
ještě	ještě	k9	ještě
téže	týž	k3xTgFnSc2	týž
noci	noc	k1gFnSc2	noc
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
přijela	přijet	k5eAaPmAgFnS	přijet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
ubytovala	ubytovat	k5eAaPmAgFnS	ubytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
jeho	jeho	k3xOp3gMnPc2	jeho
překvapených	překvapený	k2eAgMnPc2d1	překvapený
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
milý	milý	k1gMnSc1	milý
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
však	však	k9	však
už	už	k6eAd1	už
nejevil	jevit	k5eNaImAgInS	jevit
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Zruč	Zruč	k1gFnSc1	Zruč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
