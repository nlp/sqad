<s>
Vopli	Vopl	k1gMnPc1
</s>
<s>
Vopli	Vopli	k1gFnSc1
Vidopliassova	Vidopliassova	k1gFnSc1
nebo	nebo	k8xC
VV	VV	kA
(	(	kIx(
<g/>
ukrajinsky	ukrajinsky	k6eAd1
В	В	k?
В	В	k?
<g/>
,	,	kIx,
В	В	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
populární	populární	k2eAgFnSc1d1
ukrajinská	ukrajinský	k2eAgFnSc1d1
rocková	rockový	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
v	v	k7c6
Kyjevě	Kyjev	k1gInSc6
<g/>
,	,	kIx,
Ukrajinská	ukrajinský	k2eAgFnSc1d1
SSR	SSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
působí	působit	k5eAaImIp3nS
v	v	k7c6
hudebních	hudební	k2eAgInPc6d1
stylech	styl	k1gInPc6
hard-rock	hard-rock	k1gInSc1
<g/>
,	,	kIx,
folk	folk	k1gInSc1
<g/>
,	,	kIx,
lidové	lidový	k2eAgFnPc1d1
a	a	k8xC
vlastenecké	vlastenecký	k2eAgFnPc1d1
písně	píseň	k1gFnPc1
<g/>
,	,	kIx,
punk	punk	k1gInSc1
<g/>
,	,	kIx,
heavy	heava	k1gFnPc1
metal	metal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
1991-1996	1991-1996	k4
kapela	kapela	k1gFnSc1
působila	působit	k5eAaImAgFnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frontman	Frontman	k1gMnSc1
skupiny	skupina	k1gFnSc2
Oleh	Oleh	k1gMnSc1
Skrypka	Skrypek	k1gMnSc2
koncertuje	koncertovat	k5eAaImIp3nS
také	také	k9
ve	v	k7c6
stylu	styl	k1gInSc6
jazz	jazz	k1gInSc1
a	a	k8xC
elektronická	elektronický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
a	a	k8xC
produkoval	produkovat	k5eAaImAgMnS
několik	několik	k4yIc4
sólových	sólový	k2eAgNnPc2d1
alb	album	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
zakladatelem	zakladatel	k1gMnSc7
řady	řada	k1gFnSc2
festivalů	festival	k1gInPc2
alternativní	alternativní	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
Krajina	Krajina	k1gFnSc1
Mrij	Mrij	k1gFnSc1
a	a	k8xC
Rock-Sič	Rock-Sič	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Oleh	Oleh	k1gInSc1
Skrypka	Skrypka	k1gFnSc1
-	-	kIx~
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
akordeon	akordeon	k1gInSc1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
</s>
<s>
Oleksij	Oleksít	k5eAaPmRp2nS
Melčenko	Melčenka	k1gFnSc5
-	-	kIx~
basa	basa	k1gFnSc1
</s>
<s>
Jevhen	Jevhen	k1gInSc1
Rohačevský	Rohačevský	k2eAgInSc1d1
-	-	kIx~
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
</s>
<s>
Serhij	Serhít	k5eAaPmRp2nS
Sakhno	Sakhno	k6eAd1
-	-	kIx~
bicí	bicí	k2eAgFnSc1d1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Oleksandr	Oleksandr	k1gInSc1
Pipa	pipa	k1gFnSc1
</s>
<s>
Jurij	Jurít	k5eAaPmRp2nS
Zdorenko	Zdorenka	k1gFnSc5
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
1991	#num#	k4
-	-	kIx~
Abo	Abo	k1gFnSc2
ABO	ABO	kA
</s>
<s>
1993	#num#	k4
-	-	kIx~
Zakustyka	Zakustyek	k1gInSc2
</s>
<s>
1994	#num#	k4
-	-	kIx~
Krajina	Krajina	k1gFnSc1
Mrij	Mrij	k1gFnSc1
</s>
<s>
1996	#num#	k4
-	-	kIx~
Muzika	muzika	k1gFnSc1
(	(	kIx(
<g/>
single	singl	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
-	-	kIx~
Muzika	muzika	k1gFnSc1
</s>
<s>
1998	#num#	k4
-	-	kIx~
Ljubov	Ljubov	k1gInSc1
(	(	kIx(
<g/>
single	singl	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
-	-	kIx~
Chvyli	Chvyli	k1gMnSc1
Amura	Amura	k1gMnSc1
</s>
<s>
2001	#num#	k4
-	-	kIx~
Den	den	k1gInSc1
NaroDJennja	NaroDJennj	k1gInSc2
</s>
<s>
2001	#num#	k4
-	-	kIx~
Mamaj	Mamaj	k1gFnSc1
(	(	kIx(
<g/>
single	singl	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
-	-	kIx~
Fajno	Fajno	k6eAd1
</s>
<s>
2006	#num#	k4
-	-	kIx~
Buly	buly	k1gNnSc4
Deňky	deňka	k1gFnSc2
</s>
<s>
2007	#num#	k4
-	-	kIx~
Video	video	k1gNnSc1
Collection	Collection	k1gInSc1
(	(	kIx(
<g/>
DVD	DVD	kA
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
-	-	kIx~
VV	VV	kA
na	na	k7c4
Rock-Sič	Rock-Sič	k1gInSc4
festivalové	festivalový	k2eAgNnSc4d1
pódium	pódium	k1gNnSc4
</s>
<s>
2008	#num#	k4
-	-	kIx~
Hymna	hymna	k1gFnSc1
Ukrajiny	Ukrajina	k1gFnSc2
</s>
<s>
2008	#num#	k4
-	-	kIx~
Video	video	k1gNnSc1
Collection	Collection	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2	#num#	k4
DVD	DVD	kA
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
-	-	kIx~
Lado	lado	k1gNnSc1
</s>
<s>
2008	#num#	k4
-	-	kIx~
Buly	bula	k1gFnSc2
Deňky	deňka	k1gFnSc2
(	(	kIx(
<g/>
vinyl	vinyl	k1gInSc1
LP	LP	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vopli	Vople	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
VV	VV	kA
</s>
<s>
Stránky	stránka	k1gFnPc1
festivalů	festival	k1gInPc2
Krajina	Krajina	k1gFnSc1
Mrij	Mrij	k1gFnSc1
a	a	k8xC
Rock-Sič	Rock-Sič	k1gInSc1
</s>
<s>
Koncert	koncert	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
2011	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2006059136	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
138611917	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2006059136	#num#	k4
</s>
