<s>
Základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
nepokrývá	pokrývat	k5eNaImIp3nS
téma	téma	k1gNnSc4
dostatečně	dostatečně	k6eAd1
z	z	k7c2
celosvětového	celosvětový	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokuste	pokusit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
článek	článek	k1gInSc4
vylepšit	vylepšit	k5eAaPmF
a	a	k8xC
doplnit	doplnit	k5eAaPmF
nebo	nebo	k8xC
k	k	k7c3
možnému	možný	k2eAgNnSc3d1
zdokonalení	zdokonalení	k1gNnSc3
přispět	přispět	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Faktická	faktický	k2eAgFnSc1d1
přesnost	přesnost	k1gFnSc1
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
byla	být	k5eAaImAgFnS
zpochybněna	zpochybnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgNnPc1d2
zdůvodnění	zdůvodnění	k1gNnPc1
najdete	najít	k5eAaPmIp2nP
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
neodstraňujte	odstraňovat	k5eNaImRp2nP
tuto	tento	k3xDgFnSc4
zprávu	zpráva	k1gFnSc4
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
nebudou	být	k5eNaImBp3nP
pochybnosti	pochybnost	k1gFnPc1
vyřešeny	vyřešen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
vkladatele	vkladatel	k1gMnPc4
šablony	šablona	k1gFnSc2
<g/>
:	:	kIx,
Na	na	k7c6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
zdůvodněte	zdůvodnit	k5eAaPmRp2nP
vložení	vložení	k1gNnSc4
šablony	šablona	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
základních	základní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
obecně	obecně	k6eAd1
a	a	k8xC
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
(	(	kIx(
<g/>
o	o	k7c6
českých	český	k2eAgFnPc6d1
základních	základní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
-	-	kIx~
Brno	Brno	k1gNnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
vzdělávací	vzdělávací	k2eAgFnSc1d1
škola	škola	k1gFnSc1
primárního	primární	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
1	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
stupni	stupeň	k1gInPc7
v	v	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
klasifikaci	klasifikace	k1gFnSc6
ISCED	ISCED	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žáci	Žák	k1gMnPc1
obvykle	obvykle	k6eAd1
zahajují	zahajovat	k5eAaImIp3nP
povinnou	povinný	k2eAgFnSc4d1
školní	školní	k2eAgFnSc4d1
docházku	docházka	k1gFnSc4
nástupem	nástup	k1gInSc7
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
ve	v	k7c6
věku	věk	k1gInSc6
6	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgInPc6
vyspělých	vyspělý	k2eAgInPc6d1
a	a	k8xC
ve	v	k7c6
většině	většina	k1gFnSc6
rozvojových	rozvojový	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
je	být	k5eAaImIp3nS
docházka	docházka	k1gFnSc1
do	do	k7c2
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
povinná	povinný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
různě	různě	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
existuje	existovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
povolení	povolení	k1gNnSc2
individuálního	individuální	k2eAgNnSc2d1
domácího	domácí	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
na	na	k7c4
žádost	žádost	k1gFnSc4
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecným	obecný	k2eAgInSc7d1
cílem	cíl	k1gInSc7
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
je	být	k5eAaImIp3nS
poskytovat	poskytovat	k5eAaImF
žákům	žák	k1gMnPc3
takové	takový	k3xDgNnSc4
základy	základ	k1gInPc1
všeobecného	všeobecný	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
je	být	k5eAaImIp3nS
připraví	připravit	k5eAaPmIp3nS
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
sekundárního	sekundární	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
učí	učit	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
konfrontovat	konfrontovat	k5eAaBmF
s	s	k7c7
vrstevníky	vrstevník	k1gMnPc7
a	a	k8xC
naučit	naučit	k5eAaPmF
se	se	k3xPyFc4
řešit	řešit	k5eAaImF
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
jsou	být	k5eAaImIp3nP
stupně	stupeň	k1gInPc1
ISCED	ISCED	kA
2	#num#	k4
a	a	k8xC
3	#num#	k4
(	(	kIx(
<g/>
tj.	tj.	kA
2	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc4
ZŠ	ZŠ	kA
a	a	k8xC
střední	střední	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
)	)	kIx)
sloučeny	sloučen	k2eAgInPc1d1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
škole	škola	k1gFnSc6
včetně	včetně	k7c2
počítání	počítání	k1gNnSc2
ročníků	ročník	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
Gymnasium	gymnasium	k1gNnSc4
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
víceleté	víceletý	k2eAgInPc1d1
gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
USA	USA	kA
apod.	apod.	kA
Obdobný	obdobný	k2eAgInSc4d1
systém	systém	k1gInSc4
fungoval	fungovat	k5eAaImAgInS
také	také	k9
v	v	k7c6
Československu	Československo	k1gNnSc6
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
i	i	k8xC
v	v	k7c6
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
bývají	bývat	k5eAaImIp3nP
stupně	stupeň	k1gInPc1
ISCED	ISCED	kA
1	#num#	k4
a	a	k8xC
2	#num#	k4
spojené	spojený	k2eAgFnPc1d1
organizačně	organizačně	k6eAd1
i	i	k9
budovou	budova	k1gFnSc7
(	(	kIx(
<g/>
a	a	k8xC
počítáním	počítání	k1gNnSc7
ročníků	ročník	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Srovnání	srovnání	k1gNnSc1
stupňů	stupeň	k1gInPc2
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
jiných	jiný	k2eAgInPc6d1
státech	stát	k1gInPc6
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
ISCED	ISCED	kA
1	#num#	k4
</s>
<s>
ISCED	ISCED	kA
2	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
ZŠ	ZŠ	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
ZŠ	ZŠ	kA
</s>
<s>
NěmeckoGrundschuleRealschule	NěmeckoGrundschuleRealschule	k1gFnSc1
<g/>
,	,	kIx,
Gesamtschule	Gesamtschule	k1gFnSc1
</s>
<s>
RakouskoVolksschuleAHS	RakouskoVolksschuleAHS	k?
(	(	kIx(
<g/>
Gymnasium	gymnasium	k1gNnSc1
<g/>
)	)	kIx)
Unterstufe	Unterstuf	k1gMnSc5
<g/>
,	,	kIx,
Neue	Neuus	k1gMnSc5
Mittelschule	Mittelschul	k1gMnSc5
<g/>
,	,	kIx,
Hauptschule	Hauptschul	k1gMnSc5
</s>
<s>
ItálieScuola	ItálieScuola	k1gFnSc1
primariaScuola	primariaScuola	k1gFnSc1
secondaria	secondarium	k1gNnSc2
di	di	k?
l	l	kA
grado	grado	k1gNnSc1
</s>
<s>
NizozemskoBasisschoolVMBO	NizozemskoBasisschoolVMBO	k?
</s>
<s>
FrancieÉcole	FrancieÉcole	k1gFnSc1
primaireCollége	primaireCollégat	k5eAaPmIp3nS
</s>
<s>
Velká	velká	k1gFnSc1
BritániePrimary	BritániePrimara	k1gFnSc2
SchoolSecondary	SchoolSecondara	k1gFnSc2
School	Schoola	k1gFnPc2
</s>
<s>
USAElementary	USAElementar	k1gInPc1
SchoolJunior	SchoolJuniora	k1gFnPc2
High	Higha	k1gFnPc2
School	School	k1gInSc1
<g/>
,	,	kIx,
Middle	Middle	k1gFnSc1
School	Schoola	k1gFnPc2
</s>
<s>
Historie	historie	k1gFnSc1
základního	základní	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
</s>
<s>
Základního	základní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
probíhalo	probíhat	k5eAaImAgNnS
již	již	k6eAd1
ve	v	k7c6
starověkém	starověký	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
a	a	k8xC
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
typické	typický	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
sedmi	sedm	k4xCc2
let	léto	k1gNnPc2
matky	matka	k1gFnSc2
vzdělávaly	vzdělávat	k5eAaImAgFnP
své	svůj	k3xOyFgMnPc4
syny	syn	k1gMnPc4
doma	doma	k6eAd1
a	a	k8xC
ti	ten	k3xDgMnPc1
poté	poté	k6eAd1
nastoupili	nastoupit	k5eAaPmAgMnP
do	do	k7c2
formálního	formální	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgInPc6d1
časech	čas	k1gInPc6
a	a	k8xC
oblastech	oblast	k1gFnPc6
lišilo	lišit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
se	se	k3xPyFc4
do	do	k7c2
12	#num#	k4
let	léto	k1gNnPc2
chlapci	chlapec	k1gMnSc6
učili	učít	k5eAaPmAgMnP,k5eAaImAgMnP
bojovým	bojový	k2eAgFnPc3d1
dovednostem	dovednost	k1gFnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k6eAd1
čtení	čtení	k1gNnSc3
<g/>
,	,	kIx,
psaní	psaní	k1gNnSc3
a	a	k8xC
aritmetice	aritmetika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Aténách	Atény	k1gFnPc6
zase	zase	k9
kladli	klást	k5eAaImAgMnP
důraz	důraz	k1gInSc4
na	na	k7c4
porozumění	porozumění	k1gNnSc4
základům	základ	k1gInPc3
polis	polis	k1gFnSc2
<g/>
,	,	kIx,
čtení	čtení	k1gNnSc2
<g/>
,	,	kIx,
psaní	psaní	k1gNnSc2
<g/>
,	,	kIx,
aritmetiky	aritmetika	k1gFnSc2
<g/>
,	,	kIx,
vyučována	vyučovat	k5eAaImNgFnS
byla	být	k5eAaImAgFnS
i	i	k9
hudba	hudba	k1gFnSc1
a	a	k8xC
gymnastika	gymnastika	k1gFnSc1
s	s	k7c7
atletikou	atletika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívky	dívka	k1gFnPc4
jakékoliv	jakýkoliv	k3yIgNnSc4
vzdělávání	vzdělávání	k1gNnSc4
získávaly	získávat	k5eAaImAgFnP
pouze	pouze	k6eAd1
doma	doma	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
Římě	Řím	k1gInSc6
nazývána	nazýván	k2eAgFnSc1d1
ludus	ludus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
90	#num#	k4
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
Marcus	Marcus	k1gMnSc1
Fabius	Fabius	k1gMnSc1
Quintilianus	Quintilianus	k1gMnSc1
dílo	dílo	k1gNnSc1
Institutio	Institutio	k1gMnSc1
oratoria	oratorium	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
základech	základ	k1gInPc6
rétoriky	rétorika	k1gFnSc2
a	a	k8xC
výchově	výchova	k1gFnSc6
řečníků	řečník	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
díle	dílo	k1gNnSc6
rozlišil	rozlišit	k5eAaPmAgInS
učení	učení	k1gNnSc4
a	a	k8xC
výuku	výuka	k1gFnSc4
a	a	k8xC
položil	položit	k5eAaPmAgInS
základ	základ	k1gInSc4
základního	základní	k2eAgNnSc2d1
vyučování	vyučování	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
díle	dílo	k1gNnSc6
zmiňuje	zmiňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
dítě	dítě	k1gNnSc1
mělo	mít	k5eAaImAgNnS
od	od	k7c2
7	#num#	k4
do	do	k7c2
14	#num#	k4
let	léto	k1gNnPc2
naučit	naučit	k5eAaPmF
smyslovým	smyslový	k2eAgFnPc3d1
zkušenostem	zkušenost	k1gFnPc3
<g/>
,	,	kIx,
naučit	naučit	k5eAaPmF
tvořit	tvořit	k5eAaImF
myšlenky	myšlenka	k1gFnPc4
<g/>
,	,	kIx,
rozvíjet	rozvíjet	k5eAaImF
jazyk	jazyk	k1gInSc4
a	a	k8xC
paměť	paměť	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
zmiňuje	zmiňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
fyzické	fyzický	k2eAgNnSc1d1
trestání	trestání	k1gNnSc1
žáků	žák	k1gMnPc2
neslouží	sloužit	k5eNaImIp3nS
jako	jako	k9
jejich	jejich	k3xOp3gFnSc1
motivace	motivace	k1gFnSc1
k	k	k7c3
učení	učení	k1gNnSc3
a	a	k8xC
doporučil	doporučit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
žáci	žák	k1gMnPc1
měli	mít	k5eAaImAgMnP
motivovat	motivovat	k5eAaBmF
pomocí	pomocí	k7c2
zajímavé	zajímavý	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
ústupem	ústup	k1gInSc7
římského	římský	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
zřizovat	zřizovat	k5eAaImF
velké	velký	k2eAgFnPc4d1
školy	škola	k1gFnPc4
při	při	k7c6
katedrálách	katedrála	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
poskytovaly	poskytovat	k5eAaImAgInP
vzdělání	vzdělání	k1gNnSc4
hlavně	hlavně	k9
v	v	k7c6
náboženských	náboženský	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
853	#num#	k4
Římské	římský	k2eAgInPc4d1
rada	rada	k1gFnSc1
stanovila	stanovit	k5eAaPmAgFnS
povinnost	povinnost	k1gFnSc4
zajišťovat	zajišťovat	k5eAaImF
základní	základní	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
každé	každý	k3xTgFnSc2
farnosti	farnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgNnSc7d1
vzděláním	vzdělání	k1gNnSc7
byla	být	k5eAaImAgFnS
míněna	míněn	k2eAgFnSc1d1
výuka	výuka	k1gFnSc1
čtení	čtení	k1gNnSc2
a	a	k8xC
psaní	psaní	k1gNnSc2
latiny	latina	k1gFnSc2
a	a	k8xC
osvojení	osvojení	k1gNnSc2
náboženských	náboženský	k2eAgInPc2d1
rituálů	rituál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Církev	církev	k1gFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c6
vzdělávání	vzdělávání	k1gNnSc6
monopol	monopol	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
neměla	mít	k5eNaImAgFnS
povinnost	povinnost	k1gFnSc4
ho	on	k3xPp3gMnSc4
poskytovat	poskytovat	k5eAaImF
každému	každý	k3xTgNnSc3
dítěti	dítě	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
vyvinul	vyvinout	k5eAaPmAgInS
výukový	výukový	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c4
opakování	opakování	k1gNnSc4
a	a	k8xC
pamatování	pamatování	k1gNnSc4
si	se	k3xPyFc3
frází	fráze	k1gFnPc2
<g/>
,	,	kIx,
otázek	otázka	k1gFnPc2
a	a	k8xC
odpovědí	odpověď	k1gFnPc2
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
pochopení	pochopení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozdělení	rozdělení	k1gNnSc3
na	na	k7c4
farní	farní	k2eAgFnPc4d1
<g/>
,	,	kIx,
církevní	církevní	k2eAgFnPc4d1
<g/>
,	,	kIx,
klášterní	klášterní	k2eAgFnPc4d1
a	a	k8xC
katedrální	katedrální	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
především	především	k9
podle	podle	k7c2
velikosti	velikost	k1gFnSc2
církve	církev	k1gFnSc2
v	v	k7c6
dané	daný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
vstupem	vstup	k1gInSc7
dívek	dívka	k1gFnPc2
do	do	k7c2
škol	škola	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
rozdělení	rozdělení	k1gNnSc3
a	a	k8xC
zřízení	zřízení	k1gNnSc3
klášterů	klášter	k1gInPc2
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
byly	být	k5eAaImAgFnP
vyučovány	vyučován	k2eAgFnPc1d1
pouze	pouze	k6eAd1
dívky	dívka	k1gFnPc4
od	od	k7c2
8	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
latinské	latinský	k2eAgFnSc6d1
gramatice	gramatika	k1gFnSc6
<g/>
,	,	kIx,
náboženské	náboženský	k2eAgFnSc6d1
nauce	nauka	k1gFnSc6
<g/>
,	,	kIx,
tkaní	tkaní	k1gNnSc6
atd	atd	kA
<g/>
…	…	k?
</s>
<s>
Dalším	další	k2eAgInSc7d1
postupem	postup	k1gInSc7
času	čas	k1gInSc2
se	se	k3xPyFc4
základní	základní	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
moc	moc	k6eAd1
nezměnilo	změnit	k5eNaPmAgNnS
<g/>
,	,	kIx,
vliv	vliv	k1gInSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
neměl	mít	k5eNaImAgInS
ani	ani	k8xC
humanismus	humanismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
přinesl	přinést	k5eAaPmAgInS
velkou	velký	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
do	do	k7c2
sekundárního	sekundární	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
renesance	renesance	k1gFnSc2
se	se	k3xPyFc4
chlapci	chlapec	k1gMnPc1
učili	učit	k5eAaImAgMnP,k5eAaPmAgMnP
od	od	k7c2
5	#num#	k4
let	léto	k1gNnPc2
latinské	latinský	k2eAgFnSc2d1
gramatice	gramatika	k1gFnSc3
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
stejných	stejný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
jako	jako	k8xC,k8xS
děti	dítě	k1gFnPc4
v	v	k7c6
době	doba	k1gFnSc6
římské	římský	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děti	dítě	k1gFnPc1
ze	z	k7c2
zámožných	zámožný	k2eAgFnPc2d1
rodin	rodina	k1gFnPc2
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
vyučovány	vyučovat	k5eAaImNgFnP
soukromými	soukromý	k2eAgFnPc7d1
učiteli	učitel	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vznikly	vzniknout	k5eAaPmAgFnP
předchůdci	předchůdce	k1gMnPc1
dnešních	dnešní	k2eAgNnPc2d1
gymnázií	gymnázium	k1gNnPc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zavedení	zavedení	k1gNnSc3
podpory	podpora	k1gFnSc2
pro	pro	k7c4
výuku	výuka	k1gFnSc4
gramatiky	gramatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
rozvíjet	rozvíjet	k5eAaImF
například	například	k6eAd1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
i	i	k9
vzdělávací	vzdělávací	k2eAgFnSc1d1
politika	politika	k1gFnSc1
a	a	k8xC
to	ten	k3xDgNnSc1
poskytnutím	poskytnutí	k1gNnSc7
dotací	dotace	k1gFnPc2
i	i	k8xC
průběžným	průběžný	k2eAgNnSc7d1
financováním	financování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
gymnázia	gymnázium	k1gNnPc1
byla	být	k5eAaImAgNnP
zřízena	zřídit	k5eAaPmNgNnP
pro	pro	k7c4
poskytování	poskytování	k1gNnSc4
základního	základní	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
vyžadovala	vyžadovat	k5eAaImAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jejich	jejich	k3xOp3gMnPc1
žáci	žák	k1gMnPc1
již	již	k6eAd1
měli	mít	k5eAaImAgMnP
určité	určitý	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
a	a	k8xC
znalosti	znalost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Očekávalo	očekávat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
schopni	schopen	k2eAgMnPc1d1
číst	číst	k5eAaImF
a	a	k8xC
psát	psát	k5eAaImF
v	v	k7c6
lidovém	lidový	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
proto	proto	k6eAd1
potřeba	potřeba	k6eAd1
zajistit	zajistit	k5eAaPmF
základní	základní	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
před	před	k7c7
gymnáziem	gymnázium	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
našem	náš	k3xOp1gInSc6
území	území	k1gNnSc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
základní	základní	k2eAgFnPc4d1
školy	škola	k1gFnPc4
důležitá	důležitý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
za	za	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1774	#num#	k4
jejími	její	k3xOp3gNnPc7
reformami	reforma	k1gFnPc7
zavedena	zavést	k5eAaPmNgFnS
povinná	povinný	k2eAgFnSc1d1
školní	školní	k2eAgFnSc1d1
docházka	docházka	k1gFnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
od	od	k7c2
6	#num#	k4
do	do	k7c2
12	#num#	k4
let	léto	k1gNnPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
letních	letní	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
pracovaly	pracovat	k5eAaImAgInP
v	v	k7c6
hospodářství	hospodářství	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Školy	škola	k1gFnPc1
byly	být	k5eAaImAgFnP
podle	podle	k7c2
tereziánské	tereziánský	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
rozděleny	rozdělen	k2eAgFnPc1d1
na	na	k7c4
systém	systém	k1gInSc4
tří	tři	k4xCgFnPc2
škol	škola	k1gFnPc2
<g/>
:	:	kIx,
triviální	triviální	k2eAgFnSc2d1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc2d1
a	a	k8xC
normální	normální	k2eAgFnSc2d1
školy	škola	k1gFnSc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
osnovou	osnova	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
běžely	běžet	k5eAaImAgInP
tzv.	tzv.	kA
preparandy	preparanda	k1gFnSc2
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
učitelů	učitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
o	o	k7c6
historii	historie	k1gFnSc6
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
ČR	ČR	kA
ve	v	k7c6
článku	článek	k1gInSc6
Základní	základní	k2eAgFnPc4d1
školy	škola	k1gFnPc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Začlenění	začlenění	k1gNnSc1
do	do	k7c2
vzdělávacího	vzdělávací	k2eAgInSc2d1
systému	systém	k1gInSc2
</s>
<s>
Základnímu	základní	k2eAgNnSc3d1
vzdělávání	vzdělávání	k1gNnSc3
často	často	k6eAd1
předchází	předcházet	k5eAaImIp3nS
předškolním	předškolní	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc1
(	(	kIx(
<g/>
dětské	dětský	k2eAgFnPc1d1
jesle	jesle	k1gFnPc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
od	od	k7c2
6	#num#	k4
měsíců	měsíc	k1gInPc2
do	do	k7c2
3	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
do	do	k7c2
6	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
navzájem	navzájem	k6eAd1
splývajících	splývající	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předškolní	předškolní	k2eAgFnSc1d1
docházka	docházka	k1gFnSc1
není	být	k5eNaImIp3nS
zpravidla	zpravidla	k6eAd1
považována	považován	k2eAgNnPc1d1
vzdělávání	vzdělávání	k1gNnPc1
<g/>
,	,	kIx,
byť	byť	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
často	často	k6eAd1
provozován	provozovat	k5eAaImNgMnS
školami	škola	k1gFnPc7
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
jako	jako	k8xC,k8xS
předstupeň	předstupeň	k1gInSc1
školní	školní	k2eAgFnSc2d1
docházky	docházka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
musí	muset	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
děti	dítě	k1gFnPc4
příslušného	příslušný	k2eAgInSc2d1
věku	věk	k1gInSc2
nastoupit	nastoupit	k5eAaPmF
k	k	k7c3
povinnému	povinný	k2eAgNnSc3d1
jednomu	jeden	k4xCgInSc3
roku	rok	k1gInSc3
předškolního	předškolní	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
předchází	předcházet	k5eAaImIp3nS
nástupu	nástup	k1gInSc6
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
hrazeno	hradit	k5eAaImNgNnS
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
rok	rok	k1gInSc4
však	však	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
plněn	plněn	k2eAgInSc1d1
individuálním	individuální	k2eAgNnSc7d1
vzděláváním	vzdělávání	k1gNnSc7
(	(	kIx(
<g/>
např.	např.	kA
domácím	domácí	k2eAgMnPc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
základní	základní	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
(	(	kIx(
<g/>
primární	primární	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navazuje	navazovat	k5eAaImIp3nS
sekundární	sekundární	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
a	a	k8xC
poté	poté	k6eAd1
terciární	terciární	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Informace	informace	k1gFnPc1
o	o	k7c6
povinném	povinný	k2eAgNnSc6d1
předškolním	předškolní	k2eAgNnSc6d1
vzdělávání	vzdělávání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
©	©	k?
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.msmt.cz/vzdelavani/predskolni-vzdelavani/informace-o-povinnem-predskolnim-vzdelavani	http://www.msmt.cz/vzdelavani/predskolni-vzdelavani/informace-o-povinnem-predskolnim-vzdelavaň	k1gFnSc3
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Obor	obor	k1gInSc1
vzdělání	vzdělání	k1gNnSc2
</s>
<s>
Základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Folkeskole	Folkeskole	k1gFnSc1
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1
školní	školní	k2eAgFnSc1d1
docházka	docházka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
základní	základní	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Základní	základní	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
(	(	kIx(
<g/>
Rejstřík	rejstřík	k1gInSc1
škol	škola	k1gFnPc2
a	a	k8xC
školských	školský	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
MŠMT	MŠMT	kA
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
Český	český	k2eAgInSc1d1
školský	školský	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
https://web.archive.org/web/20101202000924/http://www.klubspoluzaku.cz/vzdelavani/zakladni-skoly/databaze-zakladnich-skol/	https://web.archive.org/web/20101202000924/http://www.klubspoluzaku.cz/vzdelavani/zakladni-skoly/databaze-zakladnich-skol/	k4
–	–	k?
Databáze	databáze	k1gFnSc2
základních	základní	k2eAgFnPc2d1
škol	škola	k1gFnPc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
neziskové	ziskový	k2eNgFnSc2d1
organizace	organizace	k1gFnSc2
Klubu	klub	k1gInSc2
Spolužáků	spolužák	k1gMnPc2
</s>
<s>
http://www.czso.cz/csu/klasifik.nsf/i/metodika_mezinarodni_klasifikace_vzdelani_isced_97	http://www.czso.cz/csu/klasifik.nsf/i/metodika_mezinarodni_klasifikace_vzdelani_isced_97	k4
-	-	kIx~
UNESCO	Unesco	k1gNnSc1
klasifikace	klasifikace	k1gFnSc2
ISCED	ISCED	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4022349-8	4022349-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
8248	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85042512	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85042512	#num#	k4
</s>
