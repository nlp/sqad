<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
On	on	k3xPp3gInSc1	on
the	the	k?	the
Origin	Origin	k1gInSc1	Origin
of	of	k?	of
Species	species	k1gFnSc4	species
by	by	k9	by
Means	Means	k1gInSc4	Means
of	of	k?	of
Natural	Natural	k?	Natural
Selection	Selection	k1gInSc1	Selection
<g/>
,	,	kIx,	,
or	or	k?	or
the	the	k?	the
Preservation	Preservation	k1gInSc1	Preservation
of	of	k?	of
Favoured	Favoured	k1gInSc1	Favoured
Races	Races	k1gInSc1	Races
in	in	k?	in
the	the	k?	the
Struggle	Struggle	k1gNnSc4	Struggle
for	forum	k1gNnPc2	forum
Life	Lif	k1gFnSc2	Lif
(	(	kIx(	(
<g/>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
druhů	druh	k1gInPc2	druh
přírodním	přírodní	k2eAgInSc7d1	přírodní
výběrem	výběr	k1gInSc7	výběr
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
uchováním	uchování	k1gNnSc7	uchování
prospěšných	prospěšný	k2eAgNnPc2d1	prospěšné
plemen	plemeno	k1gNnPc2	plemeno
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
život	život	k1gInSc4	život
-	-	kIx~	-
většinou	většinou	k6eAd1	většinou
zkracovaná	zkracovaný	k2eAgFnSc1d1	zkracovaná
na	na	k7c4	na
<g/>
:	:	kIx,	:
O	o	k7c6	o
původu	původ	k1gInSc6	původ
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
představila	představit	k5eAaPmAgFnS	představit
vývoj	vývoj	k1gInSc4	vývoj
organismů	organismus	k1gInPc2	organismus
od	od	k7c2	od
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
a	a	k8xC	a
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
ji	on	k3xPp3gFnSc4	on
jako	jako	k9	jako
komplexní	komplexní	k2eAgFnSc4d1	komplexní
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
teorii	teorie	k1gFnSc4	teorie
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
