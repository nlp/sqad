<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc4	systém
sloužící	sloužící	k2eAgInSc4d1	sloužící
jako	jako	k8xC	jako
základní	základní	k2eAgInSc4d1	základní
prostředek	prostředek	k1gInSc4	prostředek
lidské	lidský	k2eAgFnSc2d1	lidská
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
funkce	funkce	k1gFnSc2	funkce
dorozumívací	dorozumívací	k2eAgNnSc1d1	dorozumívací
může	moct	k5eAaImIp3nS	moct
plnit	plnit	k5eAaImF	plnit
další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
apelovou	apelový	k2eAgFnSc7d1	apelová
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
předávání	předávání	k1gNnSc3	předávání
příkazů	příkaz	k1gInPc2	příkaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
referenční	referenční	k2eAgFnSc1d1	referenční
(	(	kIx(	(
<g/>
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
se	se	k3xPyFc4	se
na	na	k7c4	na
časové	časový	k2eAgInPc4d1	časový
nebo	nebo	k8xC	nebo
prostorové	prostorový	k2eAgInPc4d1	prostorový
vztahy	vztah	k1gInPc4	vztah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kontaktovou	kontaktový	k2eAgFnSc4d1	kontaktová
<g/>
,	,	kIx,	,
expresivní	expresivní	k2eAgFnSc4d1	expresivní
(	(	kIx(	(
<g/>
emotivní	emotivní	k2eAgFnSc4d1	emotivní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
estetickou	estetický	k2eAgFnSc7d1	estetická
(	(	kIx(	(
<g/>
poetickou	poetický	k2eAgFnSc7d1	poetická
<g/>
)	)	kIx)	)
a	a	k8xC	a
metajazykovou	metajazykový	k2eAgFnSc4d1	metajazyková
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
počet	počet	k1gInSc1	počet
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c7	mezi
pěti	pět	k4xCc7	pět
a	a	k8xC	a
sedmi	sedm	k4xCc7	sedm
tisíci	tisíc	k4xCgInPc7	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
rozlišit	rozlišit	k5eAaPmF	rozlišit
mezi	mezi	k7c7	mezi
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
dialektem	dialekt	k1gInSc7	dialekt
<g/>
;	;	kIx,	;
přesný	přesný	k2eAgInSc1d1	přesný
odhad	odhad	k1gInSc1	odhad
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
použitých	použitý	k2eAgFnPc6d1	použitá
metodách	metoda	k1gFnPc6	metoda
či	či	k8xC	či
stanovených	stanovený	k2eAgNnPc6d1	stanovené
kritériích	kritérion	k1gNnPc6	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Akademikové	akademik	k1gMnPc1	akademik
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
konce	konec	k1gInSc2	konec
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
vymizí	vymizet	k5eAaPmIp3nP	vymizet
na	na	k7c4	na
padesát	padesát	k4xCc4	padesát
až	až	k9	až
devadesát	devadesát	k4xCc4	devadesát
procent	procento	k1gNnPc2	procento
současných	současný	k2eAgInPc2d1	současný
živých	živý	k2eAgInPc2d1	živý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
jazyk	jazyk	k1gInSc1	jazyk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
praslovanského	praslovanský	k2eAgInSc2d1	praslovanský
základu	základ	k1gInSc2	základ
*	*	kIx~	*
<g/>
językъ	językъ	k?	językъ
<g/>
,	,	kIx,	,
kteréžto	kteréžto	k?	kteréžto
slovo	slovo	k1gNnSc1	slovo
má	mít	k5eAaImIp3nS	mít
nejblíže	blízce	k6eAd3	blízce
ke	k	k7c3	k
staroperskému	staroperský	k2eAgNnSc3d1	staroperské
slovu	slovo	k1gNnSc3	slovo
insuwis	insuwis	k1gFnSc2	insuwis
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
indoevropský	indoevropský	k2eAgInSc1d1	indoevropský
kořen	kořen	k1gInSc1	kořen
slova	slovo	k1gNnSc2	slovo
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
*	*	kIx~	*
<g/>
n	n	k0	n
<g/>
̥	̥	k?	̥
<g/>
-ghú-	hú-	k?	-ghú-
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
*	*	kIx~	*
<g/>
dnghu-	dnghu-	k?	dnghu-
či	či	k8xC	či
*	*	kIx~	*
<g/>
dnghwa-	dnghwa-	k?	dnghwa-
<g/>
,	,	kIx,	,
s	s	k7c7	s
významem	význam	k1gInSc7	význam
jazyk	jazyk	k1gInSc1	jazyk
(	(	kIx(	(
<g/>
orgán	orgán	k1gInSc1	orgán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
mluva	mluva	k1gFnSc1	mluva
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
předpona	předpona	k1gFnSc1	předpona
*	*	kIx~	*
<g/>
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
<g/>
n-	n-	k?	n-
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
kořen	kořen	k1gInSc4	kořen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
promítl	promítnout	k5eAaPmAgInS	promítnout
i	i	k9	i
do	do	k7c2	do
ostatních	ostatní	k2eAgInPc2d1	ostatní
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
language	language	k1gFnSc1	language
či	či	k8xC	či
tongue	tongue	k1gFnSc1	tongue
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Zunge	Zunge	k1gFnSc1	Zunge
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Я	Я	k?	Я
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
langage	langage	k6eAd1	langage
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
lengua	lengua	k6eAd1	lengua
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
liežù	liežù	k?	liežù
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
lingua	lingua	k6eAd1	lingua
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kořen	kořen	k1gInSc1	kořen
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
také	také	k6eAd1	také
kontaminován	kontaminovat	k5eAaBmNgInS	kontaminovat
kořenem	kořen	k1gInSc7	kořen
slovem	slovem	k6eAd1	slovem
lízat	lízat	k5eAaImF	lízat
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
hlásky	hláska	k1gFnSc2	hláska
d	d	k?	d
v	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
gótsky	gótsky	k6eAd1	gótsky
tuggo	tuggo	k6eAd1	tuggo
<g/>
,	,	kIx,	,
staroirsky	staroirsky	k6eAd1	staroirsky
tege	tege	k6eAd1	tege
či	či	k8xC	či
velšsky	velšsky	k6eAd1	velšsky
tafod	tafoda	k1gFnPc2	tafoda
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
o	o	k7c4	o
zdvojení	zdvojení	k1gNnSc4	zdvojení
kořene	kořen	k1gInSc2	kořen
slova	slovo	k1gNnSc2	slovo
nebo	nebo	k8xC	nebo
o	o	k7c4	o
tabuové	tabuový	k2eAgInPc4d1	tabuový
důvody	důvod	k1gInPc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
zkoumáním	zkoumání	k1gNnSc7	zkoumání
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Otázky	otázka	k1gFnPc1	otázka
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
filosofie	filosofie	k1gFnSc2	filosofie
jazyka	jazyk	k1gInSc2	jazyk
byly	být	k5eAaImAgFnP	být
pokládány	pokládat	k5eAaImNgFnP	pokládat
již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zde	zde	k6eAd1	zde
otázky	otázka	k1gFnPc4	otázka
typu	typ	k1gInSc2	typ
zda	zda	k8xS	zda
mohou	moct	k5eAaImIp3nP	moct
slova	slovo	k1gNnPc4	slovo
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
zkušenost	zkušenost	k1gFnSc1	zkušenost
nebo	nebo	k8xC	nebo
motivovanost	motivovanost	k1gFnSc1	motivovanost
obsahu	obsah	k1gInSc2	obsah
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
konkrétními	konkrétní	k2eAgFnPc7d1	konkrétní
památkami	památka	k1gFnPc7	památka
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Platónovy	Platónův	k2eAgInPc1d1	Platónův
dialogy	dialog	k1gInPc1	dialog
Gorgias	Gorgiasa	k1gFnPc2	Gorgiasa
a	a	k8xC	a
Faidros	Faidrosa	k1gFnPc2	Faidrosa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
časnějším	časný	k2eAgInSc7d2	časnější
dokladem	doklad	k1gInSc7	doklad
uvažování	uvažování	k1gNnSc2	uvažování
o	o	k7c6	o
jazyku	jazyk	k1gInSc6	jazyk
jsou	být	k5eAaImIp3nP	být
gramatiky	gramatika	k1gFnPc1	gramatika
sanskrtu	sanskrt	k1gInSc2	sanskrt
<g/>
,	,	kIx,	,
nejranější	raný	k2eAgInPc1d3	nejranější
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
kodifikaci	kodifikace	k1gFnSc4	kodifikace
liturgického	liturgický	k2eAgInSc2d1	liturgický
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc2	uspořádání
znaků	znak	k1gInPc2	znak
do	do	k7c2	do
abecedy	abeceda	k1gFnSc2	abeceda
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
období	období	k1gNnSc2	období
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Zásadním	zásadní	k2eAgInSc7d1	zásadní
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
Pā	Pā	k1gFnSc1	Pā
gramatika	gramatika	k1gFnSc1	gramatika
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Významní	významný	k2eAgMnPc1d1	významný
myslitelé	myslitel	k1gMnPc1	myslitel
vyjadřující	vyjadřující	k2eAgMnSc1d1	vyjadřující
se	se	k3xPyFc4	se
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
Rousseau	Rousseau	k1gMnSc1	Rousseau
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
emocí	emoce	k1gFnPc2	emoce
zatímco	zatímco	k8xS	zatímco
Kant	Kant	k1gMnSc1	Kant
je	být	k5eAaImIp3nS	být
zastáncem	zastánce	k1gMnSc7	zastánce
názoru	názor	k1gInSc2	názor
o	o	k7c6	o
původu	původ	k1gInSc6	původ
racionálním	racionální	k2eAgInSc6d1	racionální
(	(	kIx(	(
<g/>
rozumovém	rozumový	k2eAgInSc6d1	rozumový
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
logickém	logický	k2eAgNnSc6d1	logické
myšlení	myšlení	k1gNnSc6	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Filosofové	filosof	k1gMnPc1	filosof
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
samotná	samotný	k2eAgFnSc1d1	samotná
filosofie	filosofie	k1gFnSc1	filosofie
je	být	k5eAaImIp3nS	být
studiem	studio	k1gNnSc7	studio
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
lingvistiky	lingvistika	k1gFnSc2	lingvistika
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
zapsali	zapsat	k5eAaPmAgMnP	zapsat
například	například	k6eAd1	například
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
svou	svůj	k3xOyFgFnSc7	svůj
teorií	teorie	k1gFnSc7	teorie
jazykového	jazykový	k2eAgInSc2d1	jazykový
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
příklonem	příklon	k1gInSc7	příklon
ke	k	k7c3	k
struktuře	struktura	k1gFnSc3	struktura
a	a	k8xC	a
Noam	Noam	k1gInSc1	Noam
Chomsky	Chomsky	k1gFnSc2	Chomsky
teorií	teorie	k1gFnPc2	teorie
generativní	generativní	k2eAgFnSc2d1	generativní
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Přirozené	přirozený	k2eAgInPc1d1	přirozený
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
mluveny	mluven	k2eAgInPc1d1	mluven
<g/>
,	,	kIx,	,
znakovány	znakovat	k5eAaImNgInP	znakovat
nebo	nebo	k8xC	nebo
psány	psát	k5eAaImNgInP	psát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
jako	jako	k8xC	jako
audio	audio	k2eAgInSc1d1	audio
záznam	záznam	k1gInSc1	záznam
<g/>
,	,	kIx,	,
video	video	k1gNnSc1	video
nebo	nebo	k8xC	nebo
taktilní	taktilní	k2eAgInSc1d1	taktilní
záznam	záznam	k1gInSc1	záznam
Braillovým	Braillův	k2eAgNnSc7d1	Braillovo
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
modální	modální	k2eAgFnSc7d1	modální
nezávislostí	nezávislost	k1gFnSc7	nezávislost
(	(	kIx(	(
<g/>
způsobu	způsob	k1gInSc2	způsob
předání	předání	k1gNnSc1	předání
samotného	samotný	k2eAgNnSc2d1	samotné
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
perspektivy	perspektiva	k1gFnSc2	perspektiva
filosofie	filosofie	k1gFnSc2	filosofie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jazyk	jazyk	k1gInSc1	jazyk
definován	definovat	k5eAaBmNgInS	definovat
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
kognitivní	kognitivní	k2eAgFnSc1d1	kognitivní
schopnost	schopnost	k1gFnSc1	schopnost
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
a	a	k8xC	a
požívat	požívat	k5eAaImF	požívat
komplexní	komplexní	k2eAgInPc4d1	komplexní
systémy	systém	k1gInPc4	systém
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
médium	médium	k1gNnSc4	médium
k	k	k7c3	k
popsání	popsání	k1gNnSc3	popsání
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kterých	který	k3yRgFnPc2	který
tyto	tento	k3xDgInPc1	tento
systémy	systém	k1gInPc1	systém
fungují	fungovat	k5eAaImIp3nP	fungovat
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
souhrn	souhrn	k1gInSc1	souhrn
výpovědí	výpověď	k1gFnPc2	výpověď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vypovězeny	vypovědět	k5eAaPmNgInP	vypovědět
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jazyky	jazyk	k1gInPc1	jazyk
procházejí	procházet	k5eAaImIp3nP	procházet
procesem	proces	k1gInSc7	proces
sémiózy	sémióza	k1gFnSc2	sémióza
–	–	k?	–
přiřazování	přiřazování	k1gNnSc2	přiřazování
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
významům	význam	k1gInPc3	význam
<g/>
.	.	kIx.	.
</s>
<s>
Orální	orální	k2eAgInPc1d1	orální
<g/>
,	,	kIx,	,
manuální	manuální	k2eAgInPc1d1	manuální
a	a	k8xC	a
taktilní	taktilní	k2eAgInPc1d1	taktilní
jazyky	jazyk	k1gInPc1	jazyk
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
fonologický	fonologický	k2eAgInSc4d1	fonologický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
souborem	soubor	k1gInSc7	soubor
pravidel	pravidlo	k1gNnPc2	pravidlo
podle	podle	k7c2	podle
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
jazykové	jazykový	k2eAgInPc4d1	jazykový
znaky	znak	k1gInPc4	znak
(	(	kIx(	(
<g/>
nejmenší	malý	k2eAgFnPc4d3	nejmenší
jednotky	jednotka	k1gFnPc4	jednotka
nesoucí	nesoucí	k2eAgFnSc1d1	nesoucí
význam	význam	k1gInSc1	význam
<g/>
(	(	kIx(	(
<g/>
fonémy	foném	k1gInPc1	foném
<g/>
))	))	k?	))
řadí	řadit	k5eAaImIp3nP	řadit
do	do	k7c2	do
sekvencí	sekvence	k1gFnPc2	sekvence
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
jsou	být	k5eAaImIp3nP	být
významově	významově	k6eAd1	významově
větší	veliký	k2eAgInPc1d2	veliký
slovotvorné	slovotvorný	k2eAgInPc1d1	slovotvorný
celky	celek	k1gInPc1	celek
(	(	kIx(	(
<g/>
morfémy	morfém	k1gInPc1	morfém
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
slova	slovo	k1gNnPc1	slovo
(	(	kIx(	(
<g/>
lexémy	lexém	k1gInPc1	lexém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntaktický	syntaktický	k2eAgInSc1d1	syntaktický
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
stará	starat	k5eAaImIp3nS	starat
o	o	k7c6	o
kombinaci	kombinace	k1gFnSc6	kombinace
slov	slovo	k1gNnPc2	slovo
do	do	k7c2	do
frází	fráze	k1gFnPc2	fráze
a	a	k8xC	a
výpovědí	výpověď	k1gFnPc2	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
jazyk	jazyk	k1gInSc1	jazyk
má	mít	k5eAaImIp3nS	mít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
produktivity	produktivita	k1gFnSc2	produktivita
<g/>
,	,	kIx,	,
rekurzivity	rekurzivita	k1gFnSc2	rekurzivita
a	a	k8xC	a
umístění	umístění	k1gNnSc2	umístění
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
potom	potom	k6eAd1	potom
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
společenské	společenský	k2eAgFnSc6d1	společenská
konvenci	konvence	k1gFnSc6	konvence
a	a	k8xC	a
učení	učení	k1gNnSc6	učení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
komplexní	komplexní	k2eAgFnSc1d1	komplexní
struktura	struktura	k1gFnSc1	struktura
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgFnSc4d2	širší
zásobu	zásoba	k1gFnSc4	zásoba
významů	význam	k1gInPc2	význam
než	než	k8xS	než
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
známý	známý	k2eAgInSc1d1	známý
systém	systém	k1gInSc1	systém
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
počáteční	počáteční	k2eAgInSc4d1	počáteční
stav	stav	k1gInSc4	stav
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
raní	raný	k2eAgMnPc1d1	raný
hominidé	hominid	k1gMnPc1	hominid
začali	začít	k5eAaPmAgMnP	začít
postupně	postupně	k6eAd1	postupně
měnit	měnit	k5eAaImF	měnit
své	svůj	k3xOyFgInPc4	svůj
primární	primární	k2eAgInPc4d1	primární
dorozumívací	dorozumívací	k2eAgInPc4d1	dorozumívací
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
týkají	týkat	k5eAaImIp3nP	týkat
teorie	teorie	k1gFnPc1	teorie
tzv.	tzv.	kA	tzv.
jiných	jiný	k2eAgFnPc2d1	jiná
myslí	mysl	k1gFnPc2	mysl
a	a	k8xC	a
sdílené	sdílený	k2eAgFnSc2d1	sdílená
intence	intence	k1gFnSc2	intence
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
dáván	dáván	k2eAgInSc1d1	dáván
také	také	k9	také
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
narůstající	narůstající	k2eAgFnSc7d1	narůstající
kapacitou	kapacita	k1gFnSc7	kapacita
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
různých	různý	k2eAgFnPc2d1	různá
struktur	struktura	k1gFnPc2	struktura
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
spojován	spojován	k2eAgInSc1d1	spojován
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
komunikačními	komunikační	k2eAgFnPc7d1	komunikační
a	a	k8xC	a
společenskými	společenský	k2eAgFnPc7d1	společenská
funkcemi	funkce	k1gFnPc7	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
zpracováván	zpracovávat	k5eAaImNgInS	zpracovávat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
za	za	k7c4	za
jazyková	jazykový	k2eAgNnPc4d1	jazykové
centra	centrum	k1gNnPc4	centrum
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
považována	považován	k2eAgFnSc1d1	považována
Brockova	Brockův	k2eAgFnSc1d1	Brockova
a	a	k8xC	a
Wernickeho	Wernicke	k1gMnSc2	Wernicke
oblast	oblast	k1gFnSc1	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
plynně	plynně	k6eAd1	plynně
mluví	mluvit	k5eAaImIp3nS	mluvit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
hluboce	hluboko	k6eAd1	hluboko
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jazyk	jazyk	k1gInSc4	jazyk
také	také	k9	také
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
společenských	společenský	k2eAgInPc6d1	společenský
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc6d1	kulturní
ohledech	ohled	k1gInPc6	ohled
jako	jako	k9	jako
například	například	k6eAd1	například
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
jisté	jistý	k2eAgFnSc3d1	jistá
sociální	sociální	k2eAgFnSc3d1	sociální
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc1d1	sociální
stratifikace	stratifikace	k1gFnSc1	stratifikace
či	či	k8xC	či
zábava	zábava	k1gFnSc1	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
a	a	k8xC	a
rozrůzňují	rozrůzňovat	k5eAaImIp3nP	rozrůzňovat
průběhem	průběh	k1gInSc7	průběh
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
jejich	jejich	k3xOp3gFnSc2	jejich
evoluce	evoluce	k1gFnSc2	evoluce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rekonstruována	rekonstruovat	k5eAaBmNgFnS	rekonstruovat
například	například	k6eAd1	například
srovnávací	srovnávací	k2eAgFnSc1d1	srovnávací
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
při	při	k7c6	při
porovnávání	porovnávání	k1gNnSc6	porovnávání
moderních	moderní	k2eAgInPc2d1	moderní
jazyků	jazyk	k1gInPc2	jazyk
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prvek	prvek	k1gInSc1	prvek
musel	muset	k5eAaImAgInS	muset
mít	mít	k5eAaImF	mít
společný	společný	k2eAgInSc1d1	společný
prajazyk	prajazyk	k1gInSc1	prajazyk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
došlo	dojít	k5eAaPmAgNnS	dojít
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
následujících	následující	k2eAgInPc2d1	následující
stupňů	stupeň	k1gInPc2	stupeň
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ze	z	k7c2	z
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejšířeji	šíro	k6eAd3	šíro
používané	používaný	k2eAgInPc4d1	používaný
patří	patřit	k5eAaImIp3nP	patřit
Indoevropská	indoevropský	k2eAgFnSc1d1	indoevropská
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
zástupci	zástupce	k1gMnPc7	zástupce
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
nebo	nebo	k8xC	nebo
hindština	hindština	k1gFnSc1	hindština
<g/>
;	;	kIx,	;
Sino-Tibetská	Sino-Tibetský	k2eAgFnSc1d1	Sino-Tibetský
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tibetštinu	tibetština	k1gFnSc4	tibetština
<g/>
,	,	kIx,	,
mandarinštinu	mandarinština	k1gFnSc4	mandarinština
a	a	k8xC	a
dialekty	dialekt	k1gInPc4	dialekt
čínštiny	čínština	k1gFnSc2	čínština
<g/>
;	;	kIx,	;
Afro-Asijská	Afro-Asijský	k2eAgFnSc1d1	Afro-Asijský
jazyková	jazykový	k2eAgFnSc1d1	jazyková
rodina	rodina	k1gFnSc1	rodina
–	–	k?	–
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
somálština	somálština	k1gFnSc1	somálština
<g/>
,	,	kIx,	,
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
;	;	kIx,	;
Bantuské	bantuský	k2eAgInPc1d1	bantuský
jazyky	jazyk	k1gInPc1	jazyk
–	–	k?	–
swahilština	swahilština	k1gFnSc1	swahilština
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
zulu	zulus	k1gInSc2	zulus
a	a	k8xC	a
stovky	stovka	k1gFnSc2	stovka
dalších	další	k2eAgInPc2d1	další
živých	živý	k2eAgInPc2d1	živý
afrických	africký	k2eAgInPc2d1	africký
jazyků	jazyk	k1gInPc2	jazyk
<g/>
;	;	kIx,	;
Malajsko-Polynezské	Malajsko-Polynezský	k2eAgInPc4d1	Malajsko-Polynezský
jazyky	jazyk	k1gInPc4	jazyk
–	–	k?	–
indonézština	indonézština	k1gFnSc1	indonézština
<g/>
,	,	kIx,	,
malajština	malajština	k1gFnSc1	malajština
a	a	k8xC	a
stovky	stovka	k1gFnPc1	stovka
pacifických	pacifický	k2eAgInPc2d1	pacifický
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Drávidská	drávidský	k2eAgFnSc1d1	drávidská
rodina	rodina	k1gFnSc1	rodina
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
převážně	převážně	k6eAd1	převážně
jižní	jižní	k2eAgFnSc6d1	jižní
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tamilštinou	tamilština	k1gFnSc7	tamilština
a	a	k8xC	a
telugštinou	telugština	k1gFnSc7	telugština
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc4	kód
či	či	k8xC	či
šifra	šifra	k1gFnSc1	šifra
sloužící	sloužící	k1gFnSc2	sloužící
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
a	a	k8xC	a
přenosu	přenos	k1gInSc3	přenos
sdělení	sdělení	k1gNnPc2	sdělení
<g/>
;	;	kIx,	;
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
však	však	k9	však
od	od	k7c2	od
jazyků	jazyk	k1gInPc2	jazyk
umělých	umělý	k2eAgInPc2d1	umělý
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
slouží	sloužit	k5eAaImIp3nS	sloužit
například	například	k6eAd1	například
k	k	k7c3	k
programování	programování	k1gNnSc3	programování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lidský	lidský	k2eAgInSc4d1	lidský
jazyk	jazyk	k1gInSc4	jazyk
přirozeně	přirozeně	k6eAd1	přirozeně
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
a	a	k8xC	a
dynamicky	dynamicky	k6eAd1	dynamicky
se	se	k3xPyFc4	se
vyvíjející	vyvíjející	k2eAgFnPc1d1	vyvíjející
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
přirozenými	přirozený	k2eAgInPc7d1	přirozený
a	a	k8xC	a
umělými	umělý	k2eAgInPc7d1	umělý
jazyky	jazyk	k1gInPc7	jazyk
je	být	k5eAaImIp3nS	být
především	především	k9	především
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
formální	formální	k2eAgInPc1d1	formální
(	(	kIx(	(
<g/>
umělé	umělý	k2eAgInPc1d1	umělý
<g/>
)	)	kIx)	)
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
systémy	systém	k1gInPc4	systém
znaků	znak	k1gInPc2	znak
ke	k	k7c3	k
kódování	kódování	k1gNnSc3	kódování
a	a	k8xC	a
dekódování	dekódování	k1gNnSc3	dekódování
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
přirozené	přirozený	k2eAgInPc1d1	přirozený
jazyky	jazyk	k1gInPc1	jazyk
plní	plnit	k5eAaImIp3nP	plnit
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
úloh	úloha	k1gFnPc2	úloha
<g/>
,	,	kIx,	,
např.	např.	kA	např.
společenskou	společenský	k2eAgFnSc4d1	společenská
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
především	především	k6eAd1	především
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
přirozeném	přirozený	k2eAgInSc6d1	přirozený
lidském	lidský	k2eAgInSc6d1	lidský
jazyce	jazyk	k1gInSc6	jazyk
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gInSc4	on
studuje	studovat	k5eAaImIp3nS	studovat
obor	obor	k1gInSc4	obor
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
předmět	předmět	k1gInSc1	předmět
studia	studio	k1gNnSc2	studio
jazykovědy	jazykověda	k1gFnSc2	jazykověda
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gMnSc1	jazyk
nahlížen	nahlížen	k2eAgMnSc1d1	nahlížen
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
abstraktní	abstraktní	k2eAgInSc1d1	abstraktní
koncept	koncept	k1gInSc1	koncept
a	a	k8xC	a
jako	jako	k9	jako
specifické	specifický	k2eAgInPc1d1	specifický
systémy	systém	k1gInPc1	systém
-	-	kIx~	-
např.	např.	kA	např.
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lingvista	lingvista	k1gMnSc1	lingvista
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
byl	být	k5eAaImAgMnS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
definoval	definovat	k5eAaBmAgInS	definovat
moderní	moderní	k2eAgFnSc4d1	moderní
lingvistiku	lingvistika	k1gFnSc4	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgInS	použít
francouzských	francouzský	k2eAgInPc2d1	francouzský
termínů	termín	k1gInPc2	termín
langage	langagat	k5eAaPmIp3nS	langagat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
jazyka	jazyk	k1gInSc2	jazyk
jako	jako	k8xC	jako
konceptu	koncept	k1gInSc2	koncept
společného	společný	k2eAgNnSc2d1	společné
všem	všecek	k3xTgNnPc3	všecek
<g/>
,	,	kIx,	,
langue	langue	k1gFnSc4	langue
jako	jako	k8xC	jako
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
parole	parole	k1gFnSc4	parole
jakožto	jakožto	k8xS	jakožto
specifický	specifický	k2eAgInSc1d1	specifický
akt	akt	k1gInSc1	akt
komunikace	komunikace	k1gFnSc2	komunikace
proběhlý	proběhlý	k2eAgInSc1d1	proběhlý
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
jazyku	jazyk	k1gInSc6	jazyk
jako	jako	k8xS	jako
o	o	k7c6	o
obecném	obecný	k2eAgInSc6d1	obecný
konceptu	koncept	k1gInSc6	koncept
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použit	k2eAgFnPc1d1	použita
definice	definice	k1gFnPc1	definice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
různé	různý	k2eAgFnPc4d1	různá
stránky	stránka	k1gFnPc4	stránka
tohoto	tento	k3xDgInSc2	tento
fenoménu	fenomén	k1gInSc2	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Rozličné	rozličný	k2eAgFnSc2d1	rozličná
definice	definice	k1gFnSc2	definice
s	s	k7c7	s
sebou	se	k3xPyFc7	se
také	také	k6eAd1	také
přinášejí	přinášet	k5eAaImIp3nP	přinášet
mnohost	mnohost	k1gFnSc4	mnohost
přístupů	přístup	k1gInPc2	přístup
a	a	k8xC	a
chápání	chápání	k1gNnSc2	chápání
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
nuance	nuance	k1gFnPc1	nuance
nastávají	nastávat	k5eAaImIp3nP	nastávat
i	i	k9	i
u	u	k7c2	u
lingvistických	lingvistický	k2eAgFnPc2d1	lingvistická
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
teorií	teorie	k1gFnPc2	teorie
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spjatých	spjatý	k2eAgInPc2d1	spjatý
nebo	nebo	k8xC	nebo
jimi	on	k3xPp3gMnPc7	on
zastávaných	zastávaný	k2eAgInPc2d1	zastávaný
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgInSc1d1	hlavní
dorozumívací	dorozumívací	k2eAgInSc1d1	dorozumívací
prostředek	prostředek	k1gInSc1	prostředek
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
znakový	znakový	k2eAgInSc1d1	znakový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
popisují	popisovat	k5eAaImIp3nP	popisovat
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
myšlenky	myšlenka	k1gFnPc1	myšlenka
a	a	k8xC	a
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
lidé	člověk	k1gMnPc1	člověk
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
svých	svůj	k3xOyFgFnPc2	svůj
představ	představa	k1gFnPc2	představa
reality	realita	k1gFnSc2	realita
a	a	k8xC	a
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
následné	následný	k2eAgNnSc1d1	následné
sdělení	sdělení	k1gNnSc1	sdělení
jiným	jiný	k2eAgInSc7d1	jiný
lidem	lid	k1gInSc7	lid
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
významů	význam	k1gInPc2	význam
sdílený	sdílený	k2eAgMnSc1d1	sdílený
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
gramaticky	gramaticky	k6eAd1	gramaticky
správných	správný	k2eAgNnPc2d1	správné
vyjádření	vyjádření	k1gNnPc2	vyjádření
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
věty	věta	k1gFnPc4	věta
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
významu	význam	k1gInSc6	význam
i	i	k9	i
další	další	k2eAgInPc4d1	další
kódovací	kódovací	k2eAgInPc4d1	kódovací
nebo	nebo	k8xC	nebo
dorozumívací	dorozumívací	k2eAgInPc4d1	dorozumívací
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jazyk	jazyk	k1gInSc1	jazyk
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
(	(	kIx(	(
<g/>
znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
zvukové	zvukový	k2eAgInPc4d1	zvukový
projevy	projev	k1gInPc4	projev
zvířat	zvíře	k1gNnPc2	zvíře
apod.	apod.	kA	apod.
Debaty	debata	k1gFnSc2	debata
o	o	k7c6	o
přirozenosti	přirozenost	k1gFnSc6	přirozenost
a	a	k8xC	a
původu	původ	k1gInSc6	původ
jazyka	jazyk	k1gInSc2	jazyk
byly	být	k5eAaImAgFnP	být
vedeny	vést	k5eAaImNgInP	vést
od	od	k7c2	od
starověkých	starověký	k2eAgFnPc2d1	starověká
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Řečtí	řecký	k2eAgMnPc1d1	řecký
filosofové	filosof	k1gMnPc1	filosof
Gorgias	Gorgias	k1gInSc4	Gorgias
a	a	k8xC	a
Platón	platón	k1gInSc4	platón
polemizovali	polemizovat	k5eAaImAgMnP	polemizovat
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
univerzáliích	univerzálie	k1gFnPc6	univerzálie
a	a	k8xC	a
motivovanosti	motivovanost	k1gFnSc2	motivovanost
slov	slovo	k1gNnPc2	slovo
jsoucny	jsoucno	k1gNnPc7	jsoucno
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
zde	zde	k6eAd1	zde
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunikace	komunikace	k1gFnSc1	komunikace
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jazyk	jazyk	k1gInSc1	jazyk
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
představy	představ	k1gInPc7	představ
a	a	k8xC	a
koncepty	koncept	k1gInPc7	koncept
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
existují	existovat	k5eAaImIp3nP	existovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
zásadní	zásadní	k2eAgInSc4d1	zásadní
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
chápány	chápat	k5eAaImNgInP	chápat
jako	jako	k9	jako
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jazyk	jazyk	k1gInSc4	jazyk
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
osvícenských	osvícenský	k2eAgFnPc2d1	osvícenská
debat	debata	k1gFnPc2	debata
o	o	k7c6	o
původu	původ	k1gInSc6	původ
člověka	člověk	k1gMnSc2	člověk
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
módní	módní	k2eAgNnSc1d1	módní
polemizovat	polemizovat	k5eAaImF	polemizovat
též	též	k9	též
o	o	k7c6	o
původu	původ	k1gInSc6	původ
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Myslitelé	myslitel	k1gMnPc1	myslitel
jako	jako	k9	jako
Rousseau	Rousseau	k1gMnSc1	Rousseau
a	a	k8xC	a
Herder	Herdra	k1gFnPc2	Herdra
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
názor	názor	k1gInSc4	názor
o	o	k7c6	o
původu	původ	k1gInSc6	původ
jazyka	jazyk	k1gInSc2	jazyk
z	z	k7c2	z
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
instinktivní	instinktivní	k2eAgFnSc6d1	instinktivní
potřebě	potřeba	k1gFnSc6	potřeba
se	se	k3xPyFc4	se
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
jev	jev	k1gInSc4	jev
blízký	blízký	k2eAgInSc4d1	blízký
hudbě	hudba	k1gFnSc3	hudba
nebo	nebo	k8xC	nebo
poezii	poezie	k1gFnSc3	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
racionalisté	racionalist	k1gMnPc1	racionalist
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Kant	Kant	k1gMnSc1	Kant
nebo	nebo	k8xC	nebo
Descartes	Descartes	k1gMnSc1	Descartes
viděli	vidět	k5eAaImAgMnP	vidět
zdroj	zdroj	k1gInSc4	zdroj
v	v	k7c6	v
racionálním	racionální	k2eAgNnSc6d1	racionální
uvažování	uvažování	k1gNnSc6	uvažování
a	a	k8xC	a
pragmatické	pragmatický	k2eAgFnSc6d1	pragmatická
potřebě	potřeba	k1gFnSc6	potřeba
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
začali	začít	k5eAaPmAgMnP	začít
badatelé	badatel	k1gMnPc1	badatel
jako	jako	k8xS	jako
Wittgenstein	Wittgenstein	k1gInSc4	Wittgenstein
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
jazyk	jazyk	k1gInSc1	jazyk
při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
prožívání	prožívání	k1gNnSc6	prožívání
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
jazyk	jazyk	k1gInSc1	jazyk
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
objektivní	objektivní	k2eAgFnSc4d1	objektivní
skutečnost	skutečnost	k1gFnSc4	skutečnost
světa	svět	k1gInSc2	svět
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
médiem	médium	k1gNnSc7	médium
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
chápeme	chápat	k5eAaImIp1nP	chápat
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
úvahy	úvaha	k1gFnPc1	úvaha
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
přehodnocení	přehodnocení	k1gNnSc3	přehodnocení
otázek	otázka	k1gFnPc2	otázka
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
filosofie	filosofie	k1gFnSc2	filosofie
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
lingvistické	lingvistický	k2eAgFnPc4d1	lingvistická
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
definicí	definice	k1gFnPc2	definice
vidí	vidět	k5eAaImIp3nS	vidět
jazyk	jazyk	k1gInSc1	jazyk
primárně	primárně	k6eAd1	primárně
jako	jako	k9	jako
proprietu	proprieta	k1gFnSc4	proprieta
lidské	lidský	k2eAgFnSc2d1	lidská
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lidem	člověk	k1gMnPc3	člověk
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
:	:	kIx,	:
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
produkovat	produkovat	k5eAaImF	produkovat
a	a	k8xC	a
chápat	chápat	k5eAaImF	chápat
výpovědi	výpověď	k1gFnPc4	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
směřuje	směřovat	k5eAaImIp3nS	směřovat
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
univerzalitě	univerzalita	k1gFnSc3	univerzalita
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
biologickou	biologický	k2eAgFnSc4d1	biologická
bázi	báze	k1gFnSc4	báze
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
unikátní	unikátní	k2eAgInSc1d1	unikátní
vývoj	vývoj	k1gInSc1	vývoj
lidského	lidský	k2eAgInSc2d1	lidský
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
akvizice	akvizice	k1gFnSc1	akvizice
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
imanentní	imanentní	k2eAgFnSc1d1	imanentní
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
podpírají	podpírat	k5eAaImIp3nP	podpírat
svá	svůj	k3xOyFgNnPc4	svůj
tvrzení	tvrzení	k1gNnPc4	tvrzení
o	o	k7c4	o
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
kognitivně	kognitivně	k6eAd1	kognitivně
normální	normální	k2eAgFnPc4d1	normální
děti	dítě	k1gFnPc4	dítě
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
naučí	naučit	k5eAaPmIp3nS	naučit
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
formálních	formální	k2eAgFnPc2d1	formální
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
mohou	moct	k5eAaImIp3nP	moct
dokonce	dokonce	k9	dokonce
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
spontánně	spontánně	k6eAd1	spontánně
na	na	k7c6	na
takovýchto	takovýto	k3xDgNnPc6	takovýto
místech	místo	k1gNnPc6	místo
spolu	spolu	k6eAd1	spolu
žijí	žít	k5eAaImIp3nP	žít
lidé	člověk	k1gMnPc1	člověk
mluvící	mluvící	k2eAgInPc4d1	mluvící
různými	různý	k2eAgInPc7d1	různý
jazyky	jazyk	k1gInPc7	jazyk
většinou	většinou	k6eAd1	většinou
dvěma	dva	k4xCgFnPc7	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spolu	spolu	k6eAd1	spolu
navzájem	navzájem	k6eAd1	navzájem
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
;	;	kIx,	;
takto	takto	k6eAd1	takto
vzniklým	vzniklý	k2eAgMnPc3d1	vzniklý
jazykům	jazyk	k1gMnPc3	jazyk
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
také	také	k9	také
jaz	jaz	k?	jaz
<g/>
.	.	kIx.	.
kreolské	kreolský	k2eAgFnPc1d1	kreolská
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
náhledy	náhled	k1gInPc1	náhled
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sledovány	sledovat	k5eAaImNgInP	sledovat
až	až	k9	až
k	k	k7c3	k
filosofům	filosof	k1gMnPc3	filosof
Kantovi	Kantův	k2eAgMnPc1d1	Kantův
a	a	k8xC	a
Descartesovi	Descartesův	k2eAgMnPc1d1	Descartesův
<g/>
,	,	kIx,	,
chápou	chápat	k5eAaImIp3nP	chápat
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
niterně	niterně	k6eAd1	niterně
imanentní	imanentní	k2eAgFnSc1d1	imanentní
člověku	člověk	k1gMnSc3	člověk
<g/>
;	;	kIx,	;
promítají	promítat	k5eAaImIp3nP	promítat
se	se	k3xPyFc4	se
též	též	k9	též
v	v	k7c4	v
Chomského	Chomský	k2eAgMnSc4d1	Chomský
teorii	teorie	k1gFnSc3	teorie
Univerzální	univerzální	k2eAgFnSc2d1	univerzální
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
definice	definice	k1gFnPc1	definice
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
uplatňovány	uplatňován	k2eAgInPc1d1	uplatňován
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
kognitivně	kognitivně	k6eAd1	kognitivně
vědeckém	vědecký	k2eAgInSc6d1	vědecký
rámci	rámec	k1gInSc6	rámec
a	a	k8xC	a
v	v	k7c6	v
neurolingvistice	neurolingvistika	k1gFnSc6	neurolingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
filosofii	filosofie	k1gFnSc4	filosofie
ale	ale	k8xC	ale
nesdílejí	sdílet	k5eNaImIp3nP	sdílet
všichni	všechen	k3xTgMnPc1	všechen
jazykovědci	jazykovědec	k1gMnPc1	jazykovědec
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
sociální	sociální	k2eAgInSc4d1	sociální
konstrukt	konstrukt	k1gInSc4	konstrukt
Jiná	jiný	k2eAgFnSc1d1	jiná
definice	definice	k1gFnSc1	definice
vidí	vidět	k5eAaImIp3nS	vidět
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
formální	formální	k2eAgInSc4d1	formální
systém	systém	k1gInSc4	systém
znaků	znak	k1gInPc2	znak
zpravovaný	zpravovaný	k2eAgInSc4d1	zpravovaný
gramatickými	gramatický	k2eAgNnPc7d1	gramatické
pravidly	pravidlo	k1gNnPc7	pravidlo
pro	pro	k7c4	pro
formulaci	formulace	k1gFnSc4	formulace
a	a	k8xC	a
přenos	přenos	k1gInSc4	přenos
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
popisuje	popisovat	k5eAaImIp3nS	popisovat
jazyk	jazyk	k1gInSc1	jazyk
jako	jako	k8xS	jako
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
strukturovaný	strukturovaný	k2eAgInSc1d1	strukturovaný
systém	systém	k1gInSc1	systém
znaků	znak	k1gInPc2	znak
sestávajících	sestávající	k2eAgInPc2d1	sestávající
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
označujícího	označující	k2eAgMnSc2d1	označující
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
např.	např.	kA	např.
stůl	stůl	k1gInSc4	stůl
<g/>
)	)	kIx)	)
a	a	k8xC	a
označovaného	označovaný	k2eAgInSc2d1	označovaný
(	(	kIx(	(
<g/>
entita	entita	k1gFnSc1	entita
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
výraz	výraz	k1gInSc1	výraz
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stůl	stůl	k1gInSc4	stůl
jako	jako	k8xC	jako
obraz	obraz	k1gInSc4	obraz
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
koncept	koncept	k1gInSc4	koncept
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
vybaví	vybavit	k5eAaPmIp3nS	vybavit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
strukturní	strukturní	k2eAgInSc1d1	strukturní
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
je	být	k5eAaImIp3nS	být
odkazem	odkaz	k1gInSc7	odkaz
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
de	de	k?	de
Saussura	Saussur	k1gMnSc2	Saussur
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
podmětné	podmětný	k2eAgNnSc1d1	podmětné
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Formální	formální	k2eAgInSc1d1	formální
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
svým	svůj	k3xOyFgInSc7	svůj
postupem	postup	k1gInSc7	postup
od	od	k7c2	od
nejelementárnějších	elementární	k2eAgFnPc2d3	nejelementárnější
částí	část	k1gFnPc2	část
–	–	k?	–
fonémů	foném	k1gInPc2	foném
<g/>
,	,	kIx,	,
po	po	k7c4	po
celky	celek	k1gInPc4	celek
větší	veliký	k2eAgFnSc1d2	veliký
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
slova	slovo	k1gNnPc1	slovo
nebo	nebo	k8xC	nebo
věty	věta	k1gFnPc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
představitelem	představitel	k1gMnSc7	představitel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
touto	tento	k3xDgFnSc7	tento
problematikou	problematika	k1gFnSc7	problematika
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
generativní	generativní	k2eAgFnSc6d1	generativní
gramatice	gramatika	k1gFnSc6	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Definoval	definovat	k5eAaBmAgMnS	definovat
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xC	jako
množinu	množina	k1gFnSc4	množina
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vypovězeny	vypovědět	k5eAaPmNgInP	vypovědět
na	na	k7c6	na
základě	základ	k1gInSc6	základ
principu	princip	k1gInSc2	princip
transformační	transformační	k2eAgFnSc2d1	transformační
gramatiky	gramatika	k1gFnSc2	gramatika
(	(	kIx(	(
<g/>
proces	proces	k1gInSc1	proces
generace	generace	k1gFnSc2	generace
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
transformační	transformační	k2eAgFnSc1d1	transformační
gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
soubor	soubor	k1gInSc1	soubor
platných	platný	k2eAgNnPc2d1	platné
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
užití	užití	k1gNnSc6	užití
formulují	formulovat	k5eAaImIp3nP	formulovat
gramaticky	gramaticky	k6eAd1	gramaticky
správné	správný	k2eAgFnPc1d1	správná
věty	věta	k1gFnPc1	věta
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
považuje	považovat	k5eAaImIp3nS	považovat
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
mozku	mozek	k1gInSc2	mozek
za	za	k7c4	za
imanentní	imanentní	k2eAgFnSc4d1	imanentní
a	a	k8xC	a
za	za	k7c4	za
samotný	samotný	k2eAgInSc4d1	samotný
základ	základ	k1gInSc4	základ
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
teorie	teorie	k1gFnSc1	teorie
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
formální	formální	k2eAgFnSc3d1	formální
logice	logika	k1gFnSc3	logika
a	a	k8xC	a
formální	formální	k2eAgFnSc3d1	formální
teorii	teorie	k1gFnSc3	teorie
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
,	,	kIx,	,
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
v	v	k7c6	v
aplikované	aplikovaný	k2eAgFnSc6d1	aplikovaná
počítačové	počítačový	k2eAgFnSc6d1	počítačová
lingvistice	lingvistika	k1gFnSc6	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Strukturní	strukturní	k2eAgInSc1d1	strukturní
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
přehledem	přehled	k1gInSc7	přehled
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
úrovní	úroveň	k1gFnPc2	úroveň
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
jsou	být	k5eAaImIp3nP	být
studovány	studován	k2eAgFnPc1d1	studována
disciplínami	disciplína	k1gFnPc7	disciplína
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoliv	jakýkoliv	k3yIgFnPc1	jakýkoliv
jednotky	jednotka	k1gFnPc1	jednotka
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
na	na	k7c4	na
jednotky	jednotka	k1gFnPc4	jednotka
émické	émické	k2eAgMnSc2d1	émické
(	(	kIx(	(
<g/>
přípona	přípona	k1gFnSc1	přípona
-ém	-ém	k?	-ém
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
s	s	k7c7	s
předponou	předpona	k1gFnSc7	předpona
alo-	alo-	k?	alo-
<g/>
.	.	kIx.	.
</s>
<s>
Émické	Émický	k2eAgFnPc1d1	Émický
jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
konceptu	koncept	k1gInSc2	koncept
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
př	př	kA	př
<g/>
.	.	kIx.	.
lexém	lexém	k1gInSc1	lexém
(	(	kIx(	(
<g/>
koncept	koncept	k1gInSc1	koncept
slova	slovo	k1gNnSc2	slovo
společný	společný	k2eAgInSc1d1	společný
všem	všecek	k3xTgNnPc3	všecek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alo	alo	k0wR	alo
jednotky	jednotka	k1gFnPc4	jednotka
jsou	být	k5eAaImIp3nP	být
konkrétně	konkrétně	k6eAd1	konkrétně
aktualizované	aktualizovaný	k2eAgFnPc4d1	aktualizovaná
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
aktu	akt	k1gInSc6	akt
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
př	př	kA	př
<g/>
.	.	kIx.	.
alolex	alolex	k1gInSc1	alolex
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
vyslovené	vyslovený	k2eAgNnSc1d1	vyslovené
slovo	slovo	k1gNnSc1	slovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úrovně	úroveň	k1gFnPc1	úroveň
seřazeny	seřadit	k5eAaPmNgFnP	seřadit
od	od	k7c2	od
nejmenších	malý	k2eAgFnPc2d3	nejmenší
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
:	:	kIx,	:
fonetická	fonetický	k2eAgFnSc1d1	fonetická
–	–	k?	–
zvuková	zvukový	k2eAgFnSc1d1	zvuková
stránka	stránka	k1gFnSc1	stránka
řeči	řeč	k1gFnSc2	řeč
fonologická	fonologický	k2eAgFnSc1d1	fonologická
–	–	k?	–
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
rozlišujícími	rozlišující	k2eAgInPc7d1	rozlišující
prvky	prvek	k1gInPc7	prvek
mezi	mezi	k7c4	mezi
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
hierarchizuje	hierarchizovat	k5eAaBmIp3nS	hierarchizovat
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
třídí	třídit	k5eAaImIp3nS	třídit
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
fón	fón	k1gInSc1	fón
<g/>
)	)	kIx)	)
morfologická	morfologický	k2eAgFnSc1d1	morfologická
–	–	k?	–
studuje	studovat	k5eAaImIp3nS	studovat
vytváření	vytváření	k1gNnSc4	vytváření
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
jak	jak	k8xC	jak
celých	celý	k2eAgNnPc2d1	celé
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
významových	významový	k2eAgFnPc2d1	významová
částí	část	k1gFnPc2	část
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
též	též	k9	též
třídí	třídit	k5eAaImIp3nS	třídit
slova	slovo	k1gNnPc4	slovo
do	do	k7c2	do
gramatických	gramatický	k2eAgFnPc2d1	gramatická
kategorií	kategorie	k1gFnPc2	kategorie
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
morfém	morfém	k1gInSc1	morfém
<g/>
)	)	kIx)	)
lexikální	lexikální	k2eAgFnSc1d1	lexikální
–	–	k?	–
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
lex	lex	k?	lex
<g/>
)	)	kIx)	)
syntaktická	syntaktický	k2eAgFnSc1d1	syntaktická
–	–	k?	–
popisuje	popisovat	k5eAaImIp3nS	popisovat
pravidla	pravidlo	k1gNnPc4	pravidlo
utváření	utváření	k1gNnSc2	utváření
frází	fráze	k1gFnPc2	fráze
nebo	nebo	k8xC	nebo
vět	věta	k1gFnPc2	věta
Nejpragmatičtější	pragmatický	k2eAgFnSc1d3	pragmatický
z	z	k7c2	z
definic	definice	k1gFnPc2	definice
vážící	vážící	k2eAgNnSc1d1	vážící
se	se	k3xPyFc4	se
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
ta	ten	k3xDgFnSc1	ten
směřující	směřující	k2eAgNnPc1d1	směřující
k	k	k7c3	k
sociální	sociální	k2eAgFnSc3d1	sociální
(	(	kIx(	(
<g/>
společenské	společenský	k2eAgFnSc3d1	společenská
<g/>
)	)	kIx)	)
funkci	funkce	k1gFnSc3	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vidí	vidět	k5eAaImIp3nS	vidět
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xC	jako
komunikační	komunikační	k2eAgInSc4d1	komunikační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lidem	člověk	k1gMnPc3	člověk
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
verbální	verbální	k2eAgFnPc4d1	verbální
nebo	nebo	k8xC	nebo
znakové	znakový	k2eAgFnPc4d1	znaková
výpovědi	výpověď	k1gFnPc4	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
se	se	k3xPyFc4	se
a	a	k8xC	a
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
tak	tak	k9	tak
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1	funkční
teorie	teorie	k1gFnSc1	teorie
gramatiky	gramatika	k1gFnSc2	gramatika
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
gramatické	gramatický	k2eAgFnSc2d1	gramatická
struktury	struktura	k1gFnSc2	struktura
jejich	jejich	k3xOp3gFnSc7	jejich
komunikativní	komunikativní	k2eAgFnSc7d1	komunikativní
funkčností	funkčnost	k1gFnSc7	funkčnost
a	a	k8xC	a
chápou	chápat	k5eAaImIp3nP	chápat
gramatickou	gramatický	k2eAgFnSc4d1	gramatická
strukturu	struktura	k1gFnSc4	struktura
jazyka	jazyk	k1gInSc2	jazyk
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc4	výsledek
adaptivního	adaptivní	k2eAgInSc2d1	adaptivní
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
byla	být	k5eAaImAgFnS	být
gramatika	gramatika	k1gFnSc1	gramatika
"	"	kIx"	"
<g/>
ušita	ušit	k2eAgFnSc1d1	ušita
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sloužila	sloužit	k5eAaImAgFnS	sloužit
komunikativním	komunikativní	k2eAgFnPc3d1	komunikativní
potřebám	potřeba	k1gFnPc3	potřeba
jeho	jeho	k3xOp3gMnPc3	jeho
uživatelům	uživatel	k1gMnPc3	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
náhled	náhled	k1gInSc1	náhled
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
kognitivním	kognitivní	k2eAgInSc7d1	kognitivní
a	a	k8xC	a
interakčním	interakční	k2eAgInSc7d1	interakční
rámcem	rámec	k1gInSc7	rámec
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojována	spojován	k2eAgFnSc1d1	spojována
sociolingvistika	sociolingvistika	k1gFnSc1	sociolingvistika
a	a	k8xC	a
lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
antropologie	antropologie	k1gFnSc1	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgFnPc1d1	funkční
teorie	teorie	k1gFnPc1	teorie
tíhnou	tíhnout	k5eAaImIp3nP	tíhnout
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
studovat	studovat	k5eAaImF	studovat
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
dynamický	dynamický	k2eAgInSc1d1	dynamický
fenomén	fenomén	k1gInSc1	fenomén
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
struktury	struktura	k1gFnSc2	struktura
podléhající	podléhající	k2eAgFnSc2d1	podléhající
neustálému	neustálý	k2eAgInSc3d1	neustálý
procesu	proces	k1gInSc2	proces
změn	změna	k1gFnPc2	změna
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
svými	svůj	k3xOyFgMnPc7	svůj
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
důležitost	důležitost	k1gFnSc4	důležitost
lingvistické	lingvistický	k2eAgFnSc2d1	lingvistická
typologie	typologie	k1gFnSc2	typologie
klasifikace	klasifikace	k1gFnSc2	klasifikace
jazyků	jazyk	k1gInPc2	jazyk
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
struktury	struktura	k1gFnSc2	struktura
<g/>
;	;	kIx,	;
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vidět	vidět	k5eAaImF	vidět
proces	proces	k1gInSc4	proces
gramatikalizace	gramatikalizace	k1gFnSc2	gramatikalizace
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
daného	daný	k2eAgInSc2d1	daný
systému	systém	k1gInSc2	systém
a	a	k8xC	a
ze	z	k7c2	z
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
údajů	údaj	k1gInPc2	údaj
zobecňovat	zobecňovat	k5eAaImF	zobecňovat
na	na	k7c4	na
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
filosofie	filosofie	k1gFnSc2	filosofie
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
akcentován	akcentován	k2eAgInSc4d1	akcentován
pragmatický	pragmatický	k2eAgInSc4d1	pragmatický
pohled	pohled	k1gInSc4	pohled
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojována	spojovat	k5eAaImNgFnS	spojovat
pozdní	pozdní	k2eAgFnSc1d1	pozdní
práce	práce	k1gFnSc1	práce
Wittgensteinova	Wittgensteinův	k2eAgFnSc1d1	Wittgensteinova
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
filosofové	filosof	k1gMnPc1	filosof
jazyka	jazyk	k1gInSc2	jazyk
jako	jako	k8xC	jako
P.	P.	kA	P.
Grice	Grice	k1gMnSc1	Grice
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Searl	Searl	k1gInSc1	Searl
a	a	k8xC	a
W.	W.	kA	W.
O.	O.	kA	O.
Quine	Quin	k1gInSc5	Quin
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
jazyk	jazyk	k1gInSc1	jazyk
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
před	před	k7c4	před
řádově	řádově	k6eAd1	řádově
sto	sto	k4xCgNnSc4	sto
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
gramatika	gramatika	k1gFnSc1	gramatika
není	být	k5eNaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
v	v	k7c6	v
útlém	útlý	k2eAgNnSc6d1	útlé
dětství	dětství	k1gNnSc6	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Jazyku	jazyk	k1gMnSc3	jazyk
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
učí	učit	k5eAaImIp3nS	učit
již	již	k6eAd1	již
v	v	k7c6	v
prenatálním	prenatální	k2eAgNnSc6d1	prenatální
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
technicky	technicky	k6eAd1	technicky
spoluvyvíjet	spoluvyvíjet	k5eAaImF	spoluvyvíjet
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
k	k	k7c3	k
počítání	počítání	k1gNnSc3	počítání
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgInPc1d1	jistý
aspekty	aspekt	k1gInPc1	aspekt
jazyka	jazyk	k1gInSc2	jazyk
také	také	k9	také
částečně	částečně	k6eAd1	částečně
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
parametry	parametr	k1gInPc7	parametr
prostředí	prostředí	k1gNnSc2	prostředí
jako	jako	k8xS	jako
například	například	k6eAd1	například
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc4d1	různý
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
písma	písmo	k1gNnPc4	písmo
však	však	k9	však
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
určovat	určovat	k5eAaImF	určovat
význam	význam	k1gInSc4	význam
ze	z	k7c2	z
zapsaného	zapsaný	k2eAgNnSc2d1	zapsané
stejnou	stejná	k1gFnSc4	stejná
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
živých	živý	k2eAgInPc2d1	živý
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
necelých	celý	k2eNgInPc2d1	necelý
7	[number]	k4	7
000	[number]	k4	000
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
jich	on	k3xPp3gMnPc2	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
polovina	polovina	k1gFnSc1	polovina
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgMnSc1d1	unikátní
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
formami	forma	k1gFnPc7	forma
komunikace	komunikace	k1gFnSc2	komunikace
používané	používaný	k2eAgFnSc2d1	používaná
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Systémy	systém	k1gInPc1	systém
používané	používaný	k2eAgInPc1d1	používaný
například	například	k6eAd1	například
včelami	včela	k1gFnPc7	včela
nebo	nebo	k8xC	nebo
primáty	primát	k1gInPc7	primát
jsou	být	k5eAaImIp3nP	být
systémy	systém	k1gInPc4	systém
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezené	omezený	k2eAgNnSc1d1	omezené
množství	množství	k1gNnSc1	množství
interakcí	interakce	k1gFnPc2	interakce
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
lidský	lidský	k2eAgInSc1d1	lidský
jazyk	jazyk	k1gInSc1	jazyk
otevřený	otevřený	k2eAgInSc1d1	otevřený
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
vyvíjející	vyvíjející	k2eAgFnSc1d1	vyvíjející
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vlastnost	vlastnost	k1gFnSc4	vlastnost
produktivity	produktivita	k1gFnSc2	produktivita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
omezeného	omezený	k2eAgNnSc2d1	omezené
množství	množství	k1gNnSc2	množství
znaků	znak	k1gInPc2	znak
vzniká	vznikat	k5eAaImIp3nS	vznikat
neomezené	omezený	k2eNgNnSc1d1	neomezené
množství	množství	k1gNnSc1	množství
výpovědí	výpověď	k1gFnPc2	výpověď
a	a	k8xC	a
nová	nový	k2eAgNnPc4d1	nové
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
díky	díky	k7c3	díky
dualitě	dualita	k1gFnSc3	dualita
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
množina	množina	k1gFnSc1	množina
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
význam	význam	k1gInSc4	význam
nemají	mít	k5eNaImIp3nP	mít
(	(	kIx(	(
<g/>
zvuky	zvuk	k1gInPc1	zvuk
<g/>
,	,	kIx,	,
písmena	písmeno	k1gNnPc1	písmeno
<g/>
,	,	kIx,	,
gesta	gesto	k1gNnPc1	gesto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
takřka	takřka	k6eAd1	takřka
neomezeně	omezeně	k6eNd1	omezeně
kombinována	kombinovat	k5eAaImNgFnS	kombinovat
s	s	k7c7	s
významy	význam	k1gInPc7	význam
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nových	nový	k2eAgInPc2d1	nový
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc4	znak
a	a	k8xC	a
gramatická	gramatický	k2eAgNnPc4d1	gramatické
pravidla	pravidlo	k1gNnPc4	pravidlo
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
široce	široko	k6eAd1	široko
arbitrární	arbitrární	k2eAgFnPc1d1	arbitrární
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
motivovanost	motivovanost	k1gFnSc1	motivovanost
spojení	spojení	k1gNnSc2	spojení
výrazu	výraz	k1gInSc2	výraz
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
konotace	konotace	k1gFnPc4	konotace
ovlivněné	ovlivněný	k2eAgInPc1d1	ovlivněný
nesčetnými	sčetný	k2eNgFnPc7d1	nesčetná
okolnostmi	okolnost	k1gFnPc7	okolnost
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
také	také	k9	také
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
motivován	motivován	k2eAgMnSc1d1	motivován
vůbec	vůbec	k9	vůbec
ničím	ničit	k5eAaImIp1nS	ničit
<g/>
.	.	kIx.	.
</s>
<s>
Interakce	interakce	k1gFnPc1	interakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
komunikační	komunikační	k2eAgInPc4d1	komunikační
prostředky	prostředek	k1gInPc4	prostředek
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
geneticky	geneticky	k6eAd1	geneticky
determinované	determinovaný	k2eAgNnSc1d1	determinované
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
zvířat	zvíře	k1gNnPc2	zvíře
si	se	k3xPyFc3	se
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sociálního	sociální	k2eAgNnSc2d1	sociální
učení	učení	k1gNnSc2	učení
osvojit	osvojit	k5eAaPmF	osvojit
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
sebevyjádření	sebevyjádření	k1gNnPc2	sebevyjádření
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
lexigramů	lexigram	k1gInPc2	lexigram
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
velryb	velryba	k1gFnPc2	velryba
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
své	svůj	k3xOyFgFnSc2	svůj
písně	píseň	k1gFnSc2	píseň
imitací	imitace	k1gFnPc2	imitace
členů	člen	k1gMnPc2	člen
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
některá	některý	k3yIgNnPc1	některý
zvířata	zvíře	k1gNnPc1	zvíře
mohou	moct	k5eAaImIp3nP	moct
naučit	naučit	k5eAaPmF	naučit
mnoho	mnoho	k4c4	mnoho
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgNnSc1	žádný
si	se	k3xPyFc3	se
ale	ale	k9	ale
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
osvojit	osvojit	k5eAaPmF	osvojit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
cokoliv	cokoliv	k3yInSc4	cokoliv
získaného	získaný	k2eAgNnSc2d1	získané
ani	ani	k8xC	ani
vzdáleně	vzdáleně	k6eAd1	vzdáleně
nepřipomínalo	připomínat	k5eNaImAgNnS	připomínat
komplexní	komplexní	k2eAgFnSc4d1	komplexní
gramatiku	gramatika	k1gFnSc4	gramatika
či	či	k8xC	či
lexikální	lexikální	k2eAgFnSc4d1	lexikální
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
lidského	lidský	k2eAgInSc2d1	lidský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
především	především	k6eAd1	především
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
druhu	druh	k1gInSc2	druh
zvířecího	zvířecí	k2eAgInSc2d1	zvířecí
komunikačního	komunikační	k2eAgInSc2d1	komunikační
systému	systém	k1gInSc2	systém
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
gramatické	gramatický	k2eAgFnPc4d1	gramatická
a	a	k8xC	a
sémantické	sémantický	k2eAgFnPc4d1	sémantická
kategorie	kategorie	k1gFnPc4	kategorie
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
nebo	nebo	k8xC	nebo
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
,	,	kIx,	,
minulost	minulost	k1gFnSc1	minulost
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
užity	užít	k5eAaPmNgInP	užít
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
komplexním	komplexní	k2eAgNnPc3d1	komplexní
sdělením	sdělení	k1gNnPc3	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
známým	známý	k2eAgInSc7d1	známý
přirozeným	přirozený	k2eAgInSc7d1	přirozený
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
modálně	modálně	k6eAd1	modálně
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
ne	ne	k9	ne
pouze	pouze	k6eAd1	pouze
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jednoho	jeden	k4xCgInSc2	jeden
kanálu	kanál	k1gInSc2	kanál
či	či	k8xC	či
media	medium	k1gNnPc4	medium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skrze	skrze	k?	skrze
několik	několik	k4yIc4	několik
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mluvený	mluvený	k2eAgInSc1d1	mluvený
jazyk	jazyk	k1gInSc1	jazyk
užívá	užívat	k5eAaImIp3nS	užívat
modality	modalita	k1gFnSc2	modalita
auditivní	auditivní	k2eAgNnSc1d1	auditivní
<g/>
,	,	kIx,	,
znakové	znakový	k2eAgInPc1d1	znakový
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
zápis	zápis	k1gInSc1	zápis
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
vizuální	vizuální	k2eAgFnSc6d1	vizuální
modalitě	modalita	k1gFnSc6	modalita
a	a	k8xC	a
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
taktilní	taktilní	k2eAgFnSc4d1	taktilní
modalitu	modalita	k1gFnSc4	modalita
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
také	také	k9	také
unikátní	unikátní	k2eAgNnSc1d1	unikátní
tím	ten	k3xDgInSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
může	moct	k5eAaImIp3nS	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c6	na
abstraktní	abstraktní	k2eAgFnSc6d1	abstraktní
skutečnosti	skutečnost	k1gFnSc6	skutečnost
a	a	k8xC	a
na	na	k7c6	na
smyšlené	smyšlený	k2eAgFnSc6d1	smyšlená
či	či	k8xC	či
hypotetické	hypotetický	k2eAgFnSc6d1	hypotetická
události	událost	k1gFnSc6	událost
,	,	kIx,	,
<g/>
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
na	na	k7c4	na
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
udály	udát	k5eAaPmAgFnP	udát
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
referovat	referovat	k5eAaBmF	referovat
ke	k	k7c3	k
skutečnostem	skutečnost	k1gFnPc3	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
neváží	vážit	k5eNaImIp3nP	vážit
na	na	k7c4	na
aktuální	aktuální	k2eAgInSc4d1	aktuální
čas	čas	k1gInSc4	čas
a	a	k8xC	a
prostoru	prostora	k1gFnSc4	prostora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
displacement	displacement	k1gInSc1	displacement
(	(	kIx(	(
<g/>
dislokace	dislokace	k1gFnSc1	dislokace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
komunikace	komunikace	k1gFnSc2	komunikace
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
známky	známka	k1gFnPc1	známka
dispacementu	dispacement	k1gInSc2	dispacement
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
komunikace	komunikace	k1gFnSc1	komunikace
včel	včela	k1gFnPc2	včela
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
nektaru	nektar	k1gInSc2	nektar
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
této	tento	k3xDgFnSc2	tento
vlastnosti	vlastnost	k1gFnSc2	vlastnost
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
řeči	řeč	k1gFnSc6	řeč
je	být	k5eAaImIp3nS	být
však	však	k9	však
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
unikátní	unikátní	k2eAgNnPc4d1	unikátní
<g/>
.	.	kIx.	.
</s>
