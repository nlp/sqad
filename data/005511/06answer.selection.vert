<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
především	především	k6eAd1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
přirozeném	přirozený	k2eAgInSc6d1
lidském	lidský	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS
ho	on	k3xPp3gInSc4
studuje	studovat	k5eAaImIp3nS
obor	obor	k1gInSc4
lingvistika	lingvistika	k1gFnSc1
<g/>
.	.	kIx.
</s>