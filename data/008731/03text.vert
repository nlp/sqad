<p>
<s>
Ještěnice	Ještěnice	k1gFnSc1	Ještěnice
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
ev.	ev.	k?	ev.
č.	č.	k?	č.
838	[number]	k4	838
poblíž	poblíž	k7c2	poblíž
obce	obec	k1gFnSc2	obec
Horní	horní	k2eAgFnSc2d1	horní
Dubenky	Dubenka	k1gFnSc2	Dubenka
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jihlava	Jihlava	k1gFnSc1	Jihlava
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
697	[number]	k4	697
-	-	kIx~	-
704	[number]	k4	704
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
spravuje	spravovat	k5eAaImIp3nS	spravovat
AOPK	AOPK	kA	AOPK
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
zachování	zachování	k1gNnSc1	zachování
vzácných	vzácný	k2eAgNnPc2d1	vzácné
a	a	k8xC	a
chráněných	chráněný	k2eAgNnPc2d1	chráněné
rostlinných	rostlinný	k2eAgNnPc2d1	rostlinné
a	a	k8xC	a
živočišných	živočišný	k2eAgNnPc2d1	živočišné
společenstev	společenstvo	k1gNnPc2	společenstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PP	PP	kA	PP
Ještěnice	Ještěnice	k1gFnSc1	Ještěnice
tvoří	tvořit	k5eAaImIp3nS	tvořit
enkláva	enkláva	k1gFnSc1	enkláva
prameništní	prameništní	k2eAgFnSc2d1	prameništní
louky	louka	k1gFnSc2	louka
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
lesa	les	k1gInSc2	les
asi	asi	k9	asi
1,5	[number]	k4	1,5
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Horní	horní	k2eAgFnSc2d1	horní
Dubenky	Dubenka	k1gFnSc2	Dubenka
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
svahu	svah	k1gInSc6	svah
Skelného	skelný	k2eAgInSc2d1	skelný
vrchu	vrch	k1gInSc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavní	hlavní	k2eAgNnSc4d1	hlavní
evropské	evropský	k2eAgNnSc4d1	Evropské
rozvodí	rozvodí	k1gNnSc4	rozvodí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
s	s	k7c7	s
o	o	k7c4	o
zbytek	zbytek	k1gInSc4	zbytek
prameništního	prameništní	k2eAgNnSc2d1	prameništní
svahového	svahový	k2eAgNnSc2d1	svahové
rašeliniště	rašeliniště	k1gNnSc2	rašeliniště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
fytogeograficky	fytogeograficky	k6eAd1	fytogeograficky
významnou	významný	k2eAgFnSc7d1	významná
lokalitou	lokalita	k1gFnSc7	lokalita
borůvky	borůvka	k1gFnSc2	borůvka
bažinné	bažinný	k2eAgNnSc1d1	bažinné
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
uliginosum	uliginosum	k1gNnSc1	uliginosum
<g/>
)	)	kIx)	)
v	v	k7c6	v
Jihlavských	jihlavský	k2eAgInPc6d1	jihlavský
vrších	vrch	k1gInPc6	vrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
<s>
Moldanubický	Moldanubický	k2eAgInSc1d1	Moldanubický
pluton	pluton	k1gInSc1	pluton
–	–	k?	–
dvojslídný	dvojslídný	k2eAgInSc1d1	dvojslídný
granit	granit	k1gInSc1	granit
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
až	až	k9	až
drobně	drobně	k6eAd1	drobně
zrnitý	zrnitý	k2eAgMnSc1d1	zrnitý
adamelit	adamelit	k5eAaImF	adamelit
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
drobně	drobně	k6eAd1	drobně
porfyrický	porfyrický	k2eAgInSc1d1	porfyrický
<g/>
,	,	kIx,	,
mrákotínského	mrákotínský	k2eAgInSc2d1	mrákotínský
typu	typ	k1gInSc2	typ
tvoří	tvořit	k5eAaImIp3nS	tvořit
horninové	horninový	k2eAgNnSc1d1	horninové
podloží	podloží	k1gNnSc1	podloží
této	tento	k3xDgFnSc2	tento
lokality	lokalita	k1gFnSc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Pramennou	pramenný	k2eAgFnSc4d1	pramenná
část	část	k1gFnSc4	část
Švábovského	Švábovský	k2eAgInSc2d1	Švábovský
potoka	potok	k1gInSc2	potok
překrývají	překrývat	k5eAaImIp3nP	překrývat
převážně	převážně	k6eAd1	převážně
hlinitopísčité	hlinitopísčitý	k2eAgInPc1d1	hlinitopísčitý
deluviální	deluviální	k2eAgInPc1d1	deluviální
kongeflukční	kongeflukční	k2eAgInPc1d1	kongeflukční
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydrologické	hydrologický	k2eAgInPc1d1	hydrologický
poměry	poměr	k1gInPc1	poměr
daly	dát	k5eAaPmAgInP	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
převážně	převážně	k6eAd1	převážně
pseudoglejím	pseudoglej	k1gFnPc3	pseudoglej
typickým	typický	k2eAgFnPc3d1	typická
a	a	k8xC	a
organozemním	organozemní	k2eAgFnPc3d1	organozemní
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pramene	pramen	k1gInSc2	pramen
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
organozemní	organozemní	k2eAgInPc4d1	organozemní
a	a	k8xC	a
typické	typický	k2eAgInPc4d1	typický
gleje	glej	k1gInPc4	glej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flora	Flora	k1gFnSc1	Flora
===	===	k?	===
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgFnSc4d1	centrální
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc4d1	severozápadní
část	část	k1gFnSc4	část
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
ostřicovo-rašeliníková	ostřicovoašeliníkový	k2eAgNnPc1d1	ostřicovo-rašeliníkový
společenstva	společenstvo	k1gNnPc1	společenstvo
Sphagnorecurvi-Caricion	Sphagnorecurvi-Caricion	k1gInSc1	Sphagnorecurvi-Caricion
canescentis	canescentis	k1gFnPc4	canescentis
s	s	k7c7	s
následujícím	následující	k2eAgNnSc7d1	následující
charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
druhovým	druhový	k2eAgNnSc7d1	druhové
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
kniha	kniha	k1gFnSc1	kniha
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
referenších	referenše	k1gFnPc6	referenše
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
ostřice	ostřice	k1gFnSc1	ostřice
šedavá	šedavý	k2eAgFnSc1d1	šedavá
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
canescens	canescens	k1gInSc1	canescens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zábělník	zábělník	k1gInSc1	zábělník
bahenní	bahenní	k2eAgInSc1d1	bahenní
(	(	kIx(	(
<g/>
Comarum	Comarum	k1gInSc1	Comarum
palustre	palustr	k1gInSc5	palustr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrbovka	vrbovka	k1gFnSc1	vrbovka
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Epilobium	Epilobium	k1gNnSc1	Epilobium
palustre	palustr	k1gInSc5	palustr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klikva	klikva	k1gFnSc1	klikva
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Oxycoccus	Oxycoccus	k1gMnSc1	Oxycoccus
palustris	palustris	k1gFnSc2	palustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
violka	violka	k1gFnSc1	violka
bahenní	bahenní	k2eAgFnSc1d1	bahenní
(	(	kIx(	(
<g/>
Viola	Viola	k1gFnSc1	Viola
pallustris	pallustris	k1gFnSc2	pallustris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starček	starček	k1gInSc1	starček
potoční	potoční	k2eAgInSc1d1	potoční
(	(	kIx(	(
<g/>
Tephroseris	Tephroseris	k1gInSc1	Tephroseris
crispa	crisp	k1gMnSc2	crisp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
suchopýr	suchopýr	k1gInSc1	suchopýr
úzkolistý	úzkolistý	k2eAgInSc1d1	úzkolistý
(	(	kIx(	(
<g/>
Eriophorum	Eriophorum	k1gInSc1	Eriophorum
angustifolium	angustifolium	k1gNnSc1	angustifolium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kozlík	kozlík	k1gInSc4	kozlík
dvoudomý	dvoudomý	k2eAgInSc4d1	dvoudomý
(	(	kIx(	(
<g/>
Valeriana	Valeriana	k1gFnSc1	Valeriana
dioica	dioica	k1gMnSc1	dioica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
zde	zde	k6eAd1	zde
můžete	moct	k5eAaImIp2nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
prstnatce	prstnatka	k1gFnSc3	prstnatka
májového	májový	k2eAgInSc2d1	májový
(	(	kIx(	(
<g/>
Dactylorhiza	Dactylorhiza	k1gFnSc1	Dactylorhiza
majalis	majalis	k1gFnSc2	majalis
<g/>
)	)	kIx)	)
a	a	k8xC	a
rosnatku	rosnatka	k1gFnSc4	rosnatka
okrouhlolistou	okrouhlolistý	k2eAgFnSc4d1	okrouhlolistá
(	(	kIx(	(
<g/>
Drosera	Droser	k1gMnSc4	Droser
rotundifolia	rotundifolius	k1gMnSc4	rotundifolius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedená	uvedený	k2eAgNnPc1d1	uvedené
společenstva	společenstvo	k1gNnPc1	společenstvo
přecházejí	přecházet	k5eAaImIp3nP	přecházet
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
lokality	lokalita	k1gFnSc2	lokalita
k	k	k7c3	k
ostřicovým	ostřicový	k2eAgInPc3d1	ostřicový
porostům	porost	k1gInPc3	porost
svazu	svaz	k1gInSc2	svaz
Caricion	Caricion	k1gInSc1	Caricion
fuscae	fuscaat	k5eAaPmIp3nS	fuscaat
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
sušších	suchý	k2eAgNnPc6d2	sušší
místech	místo	k1gNnPc6	místo
ke	k	k7c3	k
krátkostébelným	krátkostébelný	k2eAgNnPc3d1	krátkostébelný
společenstvům	společenstvo	k1gNnPc3	společenstvo
svazu	svaz	k1gInSc3	svaz
Violion	Violion	k1gInSc4	Violion
caninae	canina	k1gMnSc2	canina
<g/>
.	.	kIx.	.
</s>
<s>
PP	PP	kA	PP
je	být	k5eAaImIp3nS	být
také	také	k9	také
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
plošně	plošně	k6eAd1	plošně
nevelkými	velký	k2eNgFnPc7d1	nevelká
formacemi	formace	k1gFnPc7	formace
plotníkových	plotníkův	k2eAgInPc2d1	plotníkův
bultů	bult	k1gInPc2	bult
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
)	)	kIx)	)
s	s	k7c7	s
keříčky	keříček	k1gInPc7	keříček
brusnicovitých	brusnicovitý	k2eAgMnPc2d1	brusnicovitý
<g/>
.	.	kIx.	.
</s>
<s>
Plotníkové	Plotníkový	k2eAgFnPc1d1	Plotníkový
bulty	bulta	k1gFnPc1	bulta
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
luční	luční	k2eAgFnPc1d1	luční
enklávy	enkláva	k1gFnPc1	enkláva
a	a	k8xC	a
připomínají	připomínat	k5eAaImIp3nP	připomínat
společenstva	společenstvo	k1gNnPc1	společenstvo
vrchovišť	vrchoviště	k1gNnPc2	vrchoviště
třídy	třída	k1gFnSc2	třída
Oxycocco-Sphagnetea	Oxycocco-Sphagnetea	k1gFnSc1	Oxycocco-Sphagnetea
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
také	také	k9	také
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
výskyt	výskyt	k1gInSc1	výskyt
vlochyně	vlochyně	k1gFnSc2	vlochyně
bahenní	bahenní	k2eAgFnSc2d1	bahenní
(	(	kIx(	(
<g/>
borůvky	borůvka	k1gFnSc2	borůvka
bažinné	bažinný	k2eAgFnPc1d1	bažinná
<g/>
,	,	kIx,	,
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
uliginosum	uliginosum	k1gInSc1	uliginosum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
PP	PP	kA	PP
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ještěrka	ještěrka	k1gFnSc1	ještěrka
živorodá	živorodý	k2eAgFnSc1d1	živorodá
(	(	kIx(	(
<g/>
Zootoca	Zootoc	k2eAgFnSc1d1	Zootoca
vivipara	vivipara	k1gFnSc1	vivipara
<g/>
)	)	kIx)	)
a	a	k8xC	a
zmije	zmije	k1gFnSc1	zmije
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Vipera	Vipera	k1gFnSc1	Vipera
berus	berus	k1gMnSc1	berus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
linduška	linduška	k1gFnSc1	linduška
luční	luční	k2eAgFnSc1d1	luční
(	(	kIx(	(
<g/>
Anthis	Anthis	k1gFnSc1	Anthis
pratensis	pratensis	k1gFnSc2	pratensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bramborníček	bramborníček	k1gMnSc1	bramborníček
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Saxicola	Saxicola	k1gFnSc1	Saxicola
rubetra	rubetra	k1gFnSc1	rubetra
<g/>
)	)	kIx)	)
a	a	k8xC	a
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
šedý	šedý	k2eAgMnSc1d1	šedý
(	(	kIx(	(
<g/>
Lanius	Lanius	k1gMnSc1	Lanius
excubitor	excubitor	k1gMnSc1	excubitor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyhledával	vyhledávat	k5eAaImAgInS	vyhledávat
tuto	tento	k3xDgFnSc4	tento
lokalitu	lokalita	k1gFnSc4	lokalita
jako	jako	k8xS	jako
tokaniště	tokaniště	k1gNnSc4	tokaniště
tetřívek	tetřívek	k1gMnSc1	tetřívek
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Tetrao	Tetrao	k6eAd1	Tetrao
tetrix	tetrix	k1gInSc1	tetrix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
nevyužívané	využívaný	k2eNgFnSc2d1	nevyužívaná
a	a	k8xC	a
louky	louka	k1gFnSc2	louka
je	být	k5eAaImIp3nS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
ladem	lad	k1gInSc7	lad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byly	být	k5eAaImAgInP	být
zahájeny	zahájen	k2eAgInPc1d1	zahájen
ochranářské	ochranářský	k2eAgInPc1d1	ochranářský
zásahy	zásah	k1gInPc1	zásah
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kosení	kosení	k1gNnSc6	kosení
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc6	odstraňování
náletových	náletový	k2eAgFnPc2d1	náletová
dřevin	dřevina	k1gFnPc2	dřevina
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zachování	zachování	k1gNnSc2	zachování
světlomilných	světlomilný	k2eAgNnPc2d1	světlomilné
společenstev	společenstvo	k1gNnPc2	společenstvo
rašelinných	rašelinný	k2eAgFnPc2d1	rašelinná
luk	louka	k1gFnPc2	louka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jihlava	Jihlava	k1gFnSc1	Jihlava
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ještěnice	Ještěnice	k1gFnSc2	Ještěnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dědictví	dědictví	k1gNnSc1	dědictví
Vysočiny	vysočina	k1gFnSc2	vysočina
-	-	kIx~	-
PP	PP	kA	PP
Ještěnice	Ještěnice	k1gFnSc1	Ještěnice
</s>
</p>
