<s>
Johannes	Johannes	k1gMnSc1	Johannes
Diderik	Diderik	k1gMnSc1	Diderik
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Waals	Waals	k1gInSc1	Waals
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
Leiden	Leidna	k1gFnPc2	Leidna
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
holandský	holandský	k2eAgMnSc1d1	holandský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
stavové	stavový	k2eAgFnSc6d1	stavová
rovnici	rovnice	k1gFnSc6	rovnice
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1862	[number]	k4	1862
-	-	kIx~	-
1865	[number]	k4	1865
studoval	studovat	k5eAaImAgMnS	studovat
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Leidenu	Leiden	k1gInSc6	Leiden
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1864	[number]	k4	1864
učil	učít	k5eAaPmAgMnS	učít
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Deventeru	Deventer	k1gInSc6	Deventer
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
.	.	kIx.	.
</s>
<s>
Doktorskou	doktorský	k2eAgFnSc4d1	doktorská
disertaci	disertace	k1gFnSc4	disertace
s	s	k7c7	s
názvem	název	k1gInSc7	název
Over	Over	k1gInSc4	Over
de	de	k?	de
Continuiteit	Continuiteit	k1gInSc1	Continuiteit
van	van	k1gInSc1	van
den	den	k1gInSc4	den
Gas	Gas	k1gFnSc2	Gas
-	-	kIx~	-
en	en	k?	en
Vloeistoftoestand	Vloeistoftoestand	k1gInSc1	Vloeistoftoestand
obhájil	obhájit	k5eAaPmAgInS	obhájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Amsterodamu	Amsterodam	k1gInSc6	Amsterodam
<g/>
.	.	kIx.	.
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
se	s	k7c7	s
problémem	problém	k1gInSc7	problém
spojitosti	spojitost	k1gFnPc4	spojitost
plynného	plynný	k2eAgMnSc2d1	plynný
a	a	k8xC	a
kapalného	kapalný	k2eAgInSc2d1	kapalný
stavu	stav	k1gInSc2	stav
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
nalézt	nalézt	k5eAaPmF	nalézt
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
objemem	objem	k1gInSc7	objem
<g/>
,	,	kIx,	,
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Prokázal	prokázat	k5eAaPmAgMnS	prokázat
existenci	existence	k1gFnSc4	existence
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
molekul	molekula	k1gFnPc2	molekula
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
kapalinách	kapalina	k1gFnPc6	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
jako	jako	k8xS	jako
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waalsovy	Waalsův	k2eAgFnSc2d1	Waalsova
síly	síla	k1gFnPc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
též	též	k9	též
tzv.	tzv.	kA	tzv.
van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Waalsovou	Waalsový	k2eAgFnSc4d1	Waalsový
rovnici	rovnice	k1gFnSc4	rovnice
platnou	platný	k2eAgFnSc4d1	platná
pro	pro	k7c4	pro
kapaliny	kapalina	k1gFnPc4	kapalina
i	i	k8xC	i
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
změnu	změna	k1gFnSc4	změna
skupenství	skupenství	k1gNnSc2	skupenství
s	s	k7c7	s
platnosti	platnost	k1gFnSc3	platnost
pro	pro	k7c4	pro
kapaliny	kapalina	k1gFnPc4	kapalina
stejného	stejný	k2eAgNnSc2d1	stejné
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Siru	sir	k1gMnSc3	sir
Jamesi	Jamese	k1gFnSc4	Jamese
Dewarovi	Dewar	k1gMnSc3	Dewar
a	a	k8xC	a
Heike	Heike	k1gFnSc4	Heike
Kamerlinghovi	Kamerlinghův	k2eAgMnPc1d1	Kamerlinghův
Onnesovi	Onnesův	k2eAgMnPc1d1	Onnesův
údaje	údaj	k1gInPc4	údaj
potřebné	potřebný	k2eAgInPc4d1	potřebný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kapalného	kapalný	k2eAgNnSc2d1	kapalné
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
publikoval	publikovat	k5eAaBmAgMnS	publikovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
myšlenky	myšlenka	k1gFnPc4	myšlenka
termodynamické	termodynamický	k2eAgFnSc2d1	termodynamická
teorie	teorie	k1gFnSc2	teorie
kapilarity	kapilarita	k1gFnSc2	kapilarita
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
hlavním	hlavní	k2eAgInSc7d1	hlavní
předpokladem	předpoklad	k1gInSc7	předpoklad
byla	být	k5eAaImAgFnS	být
existence	existence	k1gFnSc1	existence
pozvolné	pozvolný	k2eAgFnSc2d1	pozvolná
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
velmi	velmi	k6eAd1	velmi
prudké	prudký	k2eAgInPc1d1	prudký
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
hustoty	hustota	k1gFnSc2	hustota
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
mezi	mezi	k7c7	mezi
kapalinou	kapalina	k1gFnSc7	kapalina
a	a	k8xC	a
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
oceněny	ocenit	k5eAaPmNgFnP	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
