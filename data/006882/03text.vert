<s>
Regelinda	Regelinda	k1gFnSc1	Regelinda
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Reglindis	Reglindis	k1gFnSc1	Reglindis
nebo	nebo	k8xC	nebo
Regelindis	Regelindis	k1gFnSc1	Regelindis
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
míšeňská	míšeňský	k2eAgFnSc1d1	Míšeňská
markraběnka	markraběnka	k1gFnSc1	markraběnka
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
polského	polský	k2eAgMnSc2d1	polský
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Emnildy	Emnilda	k1gFnSc2	Emnilda
z	z	k7c2	z
Lužice	Lužice	k1gFnSc2	Lužice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1002	[number]	k4	1002
byla	být	k5eAaImAgFnS	být
provdána	provdat	k5eAaPmNgFnS	provdat
za	za	k7c4	za
Heřmana	Heřman	k1gMnSc4	Heřman
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
zavražděného	zavražděný	k2eAgMnSc4d1	zavražděný
míšeňského	míšeňský	k2eAgMnSc4d1	míšeňský
markraběte	markrabě	k1gMnSc4	markrabě
Ekkerharda	Ekkerhard	k1gMnSc4	Ekkerhard
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Heřmanovi	Heřman	k1gMnSc3	Heřman
podařilo	podařit	k5eAaPmAgNnS	podařit
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
císaře	císař	k1gMnSc2	císař
silou	síla	k1gFnSc7	síla
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
strýce	strýc	k1gMnSc2	strýc
Gunzelina	Gunzelin	k2eAgMnSc2d1	Gunzelin
markrabský	markrabský	k2eAgInSc4d1	markrabský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Regelinda	Regelinda	k1gFnSc1	Regelinda
zemřela	zemřít	k5eAaPmAgFnS	zemřít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1014	[number]	k4	1014
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
známá	známý	k2eAgFnSc1d1	známá
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
půvabné	půvabný	k2eAgFnSc3d1	půvabná
gotické	gotický	k2eAgFnSc3d1	gotická
soše	socha	k1gFnSc3	socha
zvané	zvaný	k2eAgFnSc2d1	zvaná
smějící	smějící	k2eAgFnSc2d1	smějící
se	se	k3xPyFc4	se
Polka	Polka	k1gFnSc1	Polka
dochované	dochovaný	k2eAgNnSc4d1	dochované
v	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
zakladatelských	zakladatelský	k2eAgInPc2d1	zakladatelský
párů	pár	k1gInPc2	pár
naumburského	naumburský	k2eAgInSc2d1	naumburský
dómu	dóm	k1gInSc2	dóm
od	od	k7c2	od
neznámého	známý	k2eNgMnSc2d1	neznámý
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Naumburského	Naumburský	k2eAgMnSc4d1	Naumburský
mistra	mistr	k1gMnSc4	mistr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Regelinda	Regelind	k1gMnSc2	Regelind
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Regelinda	Regelind	k1gMnSc2	Regelind
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
