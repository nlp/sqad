<s>
JavaFX	JavaFX	k?
Script	Script	k1gInSc1
</s>
<s>
JavaFX	JavaFX	k?
Script	Script	k1gInSc1
</s>
<s>
Paradigma	paradigma	k1gNnSc1
</s>
<s>
Objektově	objektově	k6eAd1
orientovaný	orientovaný	k2eAgInSc1d1
<g/>
,	,	kIx,
Deklarativní	deklarativní	k2eAgMnSc1d1
Autor	autor	k1gMnSc1
</s>
<s>
Sun	Sun	kA
Microsystems	Microsystems	k1gInSc1
Typová	typový	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
</s>
<s>
silná	silný	k2eAgFnSc1d1
OS	OS	kA
</s>
<s>
multiplatformní	multiplatformní	k2eAgFnSc1d1
Licence	licence	k1gFnSc1
</s>
<s>
GNU	gnu	k1gMnSc1
General	General	k1gMnSc1
Public	publicum	k1gNnPc2
License	Licensa	k1gFnSc3
Web	web	k1gInSc4
</s>
<s>
javafx	javafx	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
JavaFX	JavaFX	k1gFnSc1
Script	Script	k1gInSc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
pojmenovaný	pojmenovaný	k2eAgMnSc1d1
F3	F3	k1gMnSc1
(	(	kIx(
<g/>
Form	Form	k1gMnSc1
Follows	Followsa	k5eAp3nS
Function	Function	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
staticky	staticky	k6eAd1
typovaný	typovaný	k2eAgInSc1d1
deklarativní	deklarativní	k2eAgInSc1d1
skriptovací	skriptovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
platformě	platforma	k1gFnSc6
Java	Jav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
pro	pro	k7c4
programování	programování	k1gNnSc4
na	na	k7c6
platformě	platforma	k1gFnSc6
JavaFX	JavaFX	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
JavaFX	JavaFX	k1gFnSc2
Scriptu	Script	k1gInSc2
je	být	k5eAaImIp3nS
údajně	údajně	k6eAd1
až	až	k9
5	#num#	k4
<g/>
x	x	k?
úspornější	úsporný	k2eAgInPc1d2
než	než	k8xS
Javy	Jav	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syntaxe	syntaxe	k1gFnSc1
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
JavaScriptu	JavaScript	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
poskytuje	poskytovat	k5eAaImIp3nS
výhodu	výhoda	k1gFnSc4
automatického	automatický	k2eAgNnSc2d1
data	datum	k1gNnSc2
bindingu	binding	k1gInSc2
i	i	k8xC
plné	plný	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
2D	2D	k4
grafiky	grafika	k1gFnSc2
<g/>
,	,	kIx,
swingovských	swingovský	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
a	a	k8xC
deklarativních	deklarativní	k2eAgFnPc2d1
animací	animace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
verze	verze	k1gFnSc2
JavaFX	JavaFX	k1gFnSc2
2.0	2.0	k4
<g/>
,	,	kIx,
JavaFX	JavaFX	k1gMnSc1
Script	Script	k1gMnSc1
už	už	k6eAd1
není	být	k5eNaImIp3nS
podporován	podporovat	k5eAaImNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oracle	Oracle	k1gInSc1
to	ten	k3xDgNnSc4
vysvětluje	vysvětlovat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
můžeme	moct	k5eAaImIp1nP
použít	použít	k5eAaPmF
ostatní	ostatní	k2eAgInPc1d1
skriptovací	skriptovací	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
podporující	podporující	k2eAgInPc1d1
JVM	JVM	kA
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Groovy	Groův	k2eAgInPc1d1
nebo	nebo	k8xC
Scala	scát	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
jazyka	jazyk	k1gInSc2
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgMnS
do	do	k7c2
projektu	projekt	k1gInSc2
Visage	Visag	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Staticky	staticky	k6eAd1
typovaný	typovaný	k2eAgInSc1d1
–	–	k?
To	to	k9
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
datový	datový	k2eAgInSc1d1
typ	typ	k1gInSc1
každé	každý	k3xTgFnSc2
proměnné	proměnná	k1gFnSc2
<g/>
,	,	kIx,
parametru	parametr	k1gInSc2
i	i	k8xC
návratové	návratový	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
znám	znám	k2eAgMnSc1d1
již	již	k6eAd1
při	při	k7c6
překladu	překlad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Deklarativní	deklarativní	k2eAgInSc1d1
–	–	k?
Jazyk	jazyk	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
čeho	co	k3yRnSc2,k3yInSc2,k3yQnSc2
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
jak	jak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
výsledek	výsledek	k1gInSc1
je	být	k5eAaImIp3nS
jednodušší	jednoduchý	k2eAgFnPc4d2
syntaxe	syntax	k1gFnPc4
oproti	oproti	k7c3
Javě	Java	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomuto	tento	k3xDgMnSc3
rysu	rys	k1gMnSc3
je	být	k5eAaImIp3nS
JavaFX	JavaFX	k1gMnSc1
Script	Script	k1gMnSc1
vhodný	vhodný	k2eAgInSc4d1
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
komplexních	komplexní	k2eAgInPc2d1
GUI	GUI	kA
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Data	datum	k1gNnSc2
binding	binding	k1gInSc1
–	–	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
obecnou	obecný	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
spojující	spojující	k2eAgFnSc4d1
logiku	logika	k1gFnSc4
a	a	k8xC
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
správně	správně	k6eAd1
nastaveny	nastaven	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
<g/>
,	,	kIx,
změní	změnit	k5eAaPmIp3nS
<g/>
-li	-li	k?
data	datum	k1gNnSc2
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k9
elementy	element	k1gInPc4
vázané	vázaný	k2eAgInPc4d1
k	k	k7c3
těmto	tento	k3xDgNnPc3
datům	datum	k1gNnPc3
se	se	k3xPyFc4
automaticky	automaticky	k6eAd1
změní	změnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
platí	platit	k5eAaImIp3nS
i	i	k9
opačná	opačný	k2eAgFnSc1d1
závislost	závislost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
změna	změna	k1gFnSc1
stavu	stav	k1gInSc2
jedné	jeden	k4xCgFnSc2
GUI	GUI	kA
komponenty	komponenta	k1gFnSc2
synchronizovaně	synchronizovaně	k6eAd1
změní	změnit	k5eAaPmIp3nS
jinou	jiný	k2eAgFnSc4d1
komponentu	komponenta	k1gFnSc4
či	či	k8xC
datový	datový	k2eAgInSc4d1
model	model	k1gInSc4
bez	bez	k7c2
potřeby	potřeba	k1gFnSc2
jakéhokoliv	jakýkoliv	k3yIgInSc2
listeneru	listener	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
třeba	třeba	k6eAd1
v	v	k7c6
Javě	Java	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
v	v	k7c6
JavaFX	JavaFX	k1gFnSc6
Scriptu	Script	k1gInSc2
ulehčuje	ulehčovat	k5eAaImIp3nS
podpora	podpora	k1gFnSc1
většiny	většina	k1gFnSc2
používaných	používaný	k2eAgInPc2d1
IDE	IDE	kA
(	(	kIx(
<g/>
Netbeans	Netbeans	k1gInSc1
<g/>
,	,	kIx,
Eclipse	Eclipse	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syntaktické	syntaktický	k2eAgFnPc4d1
a	a	k8xC
další	další	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
jsou	být	k5eAaImIp3nP
programátorovi	programátorův	k2eAgMnPc1d1
předávány	předávat	k5eAaImNgFnP
při	při	k7c6
překladu	překlad	k1gInSc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
compile-time	compile-timat	k5eAaPmIp3nS
errors	errors	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncovka	koncovka	k1gFnSc1
zdrojových	zdrojový	k2eAgInPc2d1
souborů	soubor	k1gInPc2
v	v	k7c6
jazyce	jazyk	k1gInSc6
JavaFX	JavaFX	k1gMnSc1
Script	Script	k1gMnSc1
je	být	k5eAaImIp3nS
fx	fx	k?
<g/>
.	.	kIx.
</s>
<s>
JavaFX	JavaFX	k?
kombinuje	kombinovat	k5eAaImIp3nS
skriptovací	skriptovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
JavaFX	JavaFX	k1gFnPc2
Script	Script	k1gInSc4
s	s	k7c7
množstvím	množství	k1gNnSc7
vývojových	vývojový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
grafických	grafický	k2eAgFnPc2d1
<g/>
,	,	kIx,
audio	audio	k2eAgFnPc2d1
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
knihoven	knihovna	k1gFnPc2
médií	médium	k1gNnPc2
včetně	včetně	k7c2
runtime	runtime	k1gInSc4
prostředí	prostředí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
sjednocený	sjednocený	k2eAgInSc1d1
look	look	k1gInSc1
and	and	k?
feel	feel	k1gInSc1
(	(	kIx(
<g/>
vzhled	vzhled	k1gInSc1
a	a	k8xC
pocit	pocit	k1gInSc1
<g/>
)	)	kIx)
napříč	napříč	k7c7
různými	různý	k2eAgNnPc7d1
zařízeními	zařízení	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
syntaxe	syntaxe	k1gFnSc1
</s>
<s>
Proměnné	proměnná	k1gFnPc4
a	a	k8xC
primitivní	primitivní	k2eAgInPc4d1
typy	typ	k1gInPc4
</s>
<s>
JavaFX	JavaFX	k?
Script	Script	k1gInSc1
poskytuje	poskytovat	k5eAaImIp3nS
4	#num#	k4
primitivní	primitivní	k2eAgInPc4d1
typy	typ	k1gInPc4
<g/>
:	:	kIx,
String	String	k1gInSc1
<g/>
,	,	kIx,
Boolean	Boolean	k1gMnSc1
<g/>
,	,	kIx,
Number	Number	k1gMnSc1
a	a	k8xC
Integer	Integer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Proměnné	proměnná	k1gFnPc1
jakéhokoliv	jakýkoliv	k3yIgInSc2
typu	typ	k1gInSc2
je	být	k5eAaImIp3nS
uvedena	uveden	k2eAgFnSc1d1
s	s	k7c7
klíčovým	klíčový	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
var.	var.	k?
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Javy	Java	k1gFnSc2
zde	zde	k6eAd1
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
uvést	uvést	k5eAaPmF
datový	datový	k2eAgInSc4d1
typ	typ	k1gInSc4
při	při	k7c6
deklaraci	deklarace	k1gFnSc6
<g/>
,	,	kIx,
datový	datový	k2eAgMnSc1d1
typ	typ	k1gInSc4
si	se	k3xPyFc3
interpret	interpret	k1gMnSc1
odvodí	odvodit	k5eAaPmIp3nS
z	z	k7c2
jejího	její	k3xOp3gNnSc2
použití	použití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
protože	protože	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
staticky	staticky	k6eAd1
typový	typový	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
nelze	lze	k6eNd1
v	v	k7c6
průběhu	průběh	k1gInSc6
životního	životní	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
datový	datový	k2eAgInSc4d1
typ	typ	k1gInSc4
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměnné	proměnná	k1gFnPc1
musí	muset	k5eAaImIp3nP
být	být	k5eAaImF
pojmenovány	pojmenovat	k5eAaPmNgInP
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
pravidly	pravidlo	k1gNnPc7
Javy	Java	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
var	var	k1gInSc1
mojePromenna	mojePromenn	k1gInSc2
=	=	kIx~
"	"	kIx"
<g/>
obsah	obsah	k1gInSc1
promenne	promennout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
mojePromenna	mojePromenna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
String	String	k1gInSc1
=	=	kIx~
"	"	kIx"
<g/>
obsah	obsah	k1gInSc1
promenne	promennout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
Operátory	operátor	k1gInPc1
</s>
<s>
JavaFX	JavaFX	k?
Script	Script	k1gInSc1
poskytuje	poskytovat	k5eAaImIp3nS
de	de	k?
facto	facto	k1gNnSc4
stejné	stejný	k2eAgInPc4d1
operátory	operátor	k1gInPc4
jako	jako	k8xS,k8xC
Java	Javum	k1gNnPc4
<g/>
,	,	kIx,
plus	plus	k6eAd1
některé	některý	k3yIgFnPc1
unikátní	unikátní	k2eAgFnPc1d1
navíc	navíc	k6eAd1
jako	jako	k9
např.	např.	kA
reverse	reverse	k1gFnSc1
(	(	kIx(
<g/>
opačná	opačný	k2eAgFnSc1d1
sekvence	sekvence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
indexof	indexof	k1gInSc1
(	(	kIx(
<g/>
index	index	k1gInSc1
sekvence	sekvence	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
as	as	k1gNnSc1
(	(	kIx(
<g/>
přetypování	přetypování	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
atd.	atd.	kA
</s>
<s>
Funkce	funkce	k1gFnSc1
a	a	k8xC
operace	operace	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
funkcí	funkce	k1gFnSc7
definuje	definovat	k5eAaBmIp3nS
JavaFX	JavaFX	k1gMnSc1
Script	Script	k1gMnSc1
klíčové	klíčový	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
function	function	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
znovupoužitelný	znovupoužitelný	k2eAgInSc4d1
blok	blok	k1gInSc4
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
deklarace	deklarace	k1gFnSc2
proměnných	proměnná	k1gFnPc2
i	i	k8xC
návratový	návratový	k2eAgInSc1d1
typ	typ	k1gInSc1
<g/>
,	,	kIx,
obsah	obsah	k1gInSc1
funkce	funkce	k1gFnSc2
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
do	do	k7c2
složených	složený	k2eAgFnPc2d1
závorek	závorka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnSc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
zejména	zejména	k9
pro	pro	k7c4
účely	účel	k1gInPc4
matematické	matematický	k2eAgInPc4d1
nebo	nebo	k8xC
jako	jako	k9
gettery	getter	k1gInPc1
a	a	k8xC
settery	setter	k1gInPc1
tříd	třída	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kontrastu	kontrast	k1gInSc6
s	s	k7c7
Javou	Java	k1gFnSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
deklarovat	deklarovat	k5eAaBmF
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
proměnné	proměnná	k1gFnPc1
mimo	mimo	k7c4
třídy	třída	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Procedury	procedura	k1gFnSc2
v	v	k7c6
JavaFX	JavaFX	k1gFnSc6
Scriptu	Script	k1gInSc2
mají	mít	k5eAaImIp3nP
vyhrazeno	vyhrazen	k2eAgNnSc4d1
klíčové	klíčový	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
operation	operation	k1gInSc1
a	a	k8xC
mohou	moct	k5eAaImIp3nP
obsahovat	obsahovat	k5eAaImF
libovolné	libovolný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
příkazů	příkaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
operation	operation	k1gInSc1
vypisAhoj	vypisAhoj	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
{	{	kIx(
</s>
<s>
System	Syst	k1gInSc7
<g/>
.	.	kIx.
<g/>
out	out	k?
<g/>
.	.	kIx.
<g/>
println	println	k1gMnSc1
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Ahoj	ahoj	k0
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
}	}	kIx)
</s>
<s>
function	function	k1gInSc1
secti	secť	k1gFnSc2
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
<g/>
)	)	kIx)
{	{	kIx(
</s>
<s>
var	var	k1gInSc1
c	c	k0
=	=	kIx~
a	a	k8xC
+	+	kIx~
b	b	k?
<g/>
;	;	kIx,
</s>
<s>
return	return	k1gMnSc1
c	c	k0
<g/>
;	;	kIx,
</s>
<s>
}	}	kIx)
</s>
<s>
Podmínky	podmínka	k1gFnPc1
a	a	k8xC
cykly	cyklus	k1gInPc1
</s>
<s>
Jsou	být	k5eAaImIp3nP
obdobné	obdobný	k2eAgInPc1d1
jako	jako	k8xC,k8xS
v	v	k7c6
Javě	Java	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Výraz	výraz	k1gInSc1
rozsahu	rozsah	k1gInSc2
</s>
<s>
JavaFX	JavaFX	k?
Script	Script	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
novinku	novinka	k1gFnSc4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
výraz	výraz	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
deklarovat	deklarovat	k5eAaBmF
sekvenci	sekvence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výraz	výraz	k1gInSc1
používá	používat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
integery	integer	k1gInPc4
oddělené	oddělený	k2eAgInPc4d1
notací	notace	k1gFnSc7
„	„	k?
<g/>
..	..	k?
<g/>
“	“	k?
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
.10	.10	k4
step	step	k1gInSc1
2	#num#	k4
<g/>
]	]	kIx)
výraz	výraz	k1gInSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
sekvenci	sekvence	k1gFnSc4
lichých	lichý	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
od	od	k7c2
1	#num#	k4
do	do	k7c2
9	#num#	k4
</s>
<s>
Výjimky	výjimka	k1gFnPc1
</s>
<s>
Jazyk	jazyk	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
vyhazování	vyhazování	k1gNnSc4
výjimek	výjimka	k1gFnPc2
prostřednictvím	prostřednictvím	k7c2
výrazu	výraz	k1gInSc2
throw	throw	k?
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
zachycování	zachycování	k1gNnSc4
bloky	blok	k1gInPc1
try	try	k?
<g/>
,	,	kIx,
catch	catch	k1gInSc1
<g/>
,	,	kIx,
finally	finall	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
stejné	stejný	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
v	v	k7c6
Javě	Java	k1gFnSc6
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
pořadí	pořadí	k1gNnSc2
výrazů	výraz	k1gInPc2
catch	catcha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
objevit	objevit	k5eAaPmF
v	v	k7c6
jakémkoliv	jakýkoliv	k3yIgNnSc6
pořadí	pořadí	k1gNnSc6
<g/>
,	,	kIx,
oproti	oproti	k7c3
Javě	Java	k1gFnSc3
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
uvést	uvést	k5eAaPmF
nejdřív	dříve	k6eAd3
hierarchicky	hierarchicky	k6eAd1
nižší	nízký	k2eAgFnPc4d2
podtřídy	podtřída	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Třídy	třída	k1gFnPc1
a	a	k8xC
objekty	objekt	k1gInPc1
</s>
<s>
Třídy	třída	k1gFnPc1
se	se	k3xPyFc4
vytvářejí	vytvářet	k5eAaImIp3nP
s	s	k7c7
použitím	použití	k1gNnSc7
klíčového	klíčový	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
class	class	k6eAd1
následovaného	následovaný	k2eAgInSc2d1
platným	platný	k2eAgInSc7d1
identifikátorem	identifikátor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
třídy	třída	k1gFnPc1
je	být	k5eAaImIp3nS
ve	v	k7c6
složených	složený	k2eAgFnPc6d1
závorkách	závorka	k1gFnPc6
<g/>
,	,	kIx,
atributy	atribut	k1gInPc4
začínají	začínat	k5eAaImIp3nP
klíčovým	klíčový	k2eAgInSc7d1
slovem	slovo	k1gNnSc7
atribute	atribut	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chování	chování	k1gNnSc1
poskytované	poskytovaný	k2eAgFnSc2d1
třídou	třída	k1gFnSc7
je	být	k5eAaImIp3nS
poskytováno	poskytovat	k5eAaImNgNnS
funkcemi	funkce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
k	k	k7c3
třídám	třída	k1gFnPc3
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgInS
modifikátory	modifikátor	k1gInPc4
public	publicum	k1gNnPc2
<g/>
,	,	kIx,
private	privat	k1gMnSc5
<g/>
,	,	kIx,
protected	protected	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
konstrukci	konstrukce	k1gFnSc4
static	statice	k1gFnPc2
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
třídních	třídní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
atributů	atribut	k1gInPc2
<g/>
,	,	kIx,
nabízí	nabízet	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
možnost	možnost	k1gFnSc1
definovat	definovat	k5eAaBmF
tzv.	tzv.	kA
utility	utilita	k1gFnPc4
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
např.	např.	kA
Math	Math	k1gMnSc1
poskytující	poskytující	k2eAgFnSc2d1
různé	různý	k2eAgFnSc2d1
matematické	matematický	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instance	instance	k1gFnSc1
tříd	třída	k1gFnPc2
se	se	k3xPyFc4
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Javy	Java	k1gFnSc2
nevytváří	vytvářit	k5eNaPmIp3nS,k5eNaImIp3nS
pomocí	pomocí	k7c2
operátoru	operátor	k1gInSc2
new	new	k?
<g/>
,	,	kIx,
ale	ale	k8xC
pomocí	pomocí	k7c2
objektových	objektový	k2eAgInPc2d1
literálů	literál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
vytvoření	vytvoření	k1gNnSc1
objektu	objekt	k1gInSc2
Adresa	adresa	k1gFnSc1
</s>
<s>
def	def	k?
mojeAdresa	mojeAdresa	k1gFnSc1
=	=	kIx~
Adresa	adresa	k1gFnSc1
{	{	kIx(
</s>
<s>
ulice	ulice	k1gFnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
1	#num#	k4
Main	Main	k1gMnSc1
Street	Street	k1gMnSc1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
mesto	mesto	k1gNnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Santa	Santa	k1gFnSc1
Clara	Clara	k1gFnSc1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
stat	stat	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
CA	ca	kA
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
zip	zip	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
95050	#num#	k4
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
}	}	kIx)
</s>
<s>
Dědičnost	dědičnost	k1gFnSc1
a	a	k8xC
vícenásobná	vícenásobný	k2eAgFnSc1d1
dědičnost	dědičnost	k1gFnSc1
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
dědění	dědění	k1gNnSc2
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
zdědí	zdědit	k5eAaPmIp3nP
atributy	atribut	k1gInPc1
a	a	k8xC
funkcionalita	funkcionalita	k1gFnSc1
nadtřídy	nadtřída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgInSc3
slouží	sloužit	k5eAaImIp3nS
klíčové	klíčový	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
extends	extendsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
JavaFX	JavaFX	k1gFnSc4
Script	Scriptum	k1gNnPc2
má	mít	k5eAaImIp3nS
speciální	speciální	k2eAgInPc1d1
bloky	blok	k1gInPc1
zajišťující	zajišťující	k2eAgFnSc4d1
bezpečnou	bezpečný	k2eAgFnSc4d1
inicializaci	inicializace	k1gFnSc4
a	a	k8xC
post	post	k1gInSc4
inicializaci	inicializace	k1gFnSc4
hierarchie	hierarchie	k1gFnSc2
(	(	kIx(
<g/>
bloky	blok	k1gInPc1
init	initum	k1gNnPc2
a	a	k8xC
postinit	postinit	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Javy	Java	k1gFnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
vícenásobná	vícenásobný	k2eAgFnSc1d1
dědičnost	dědičnost	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
třída	třída	k1gFnSc1
může	moct	k5eAaImIp3nS
dědit	dědit	k5eAaImF
od	od	k7c2
více	hodně	k6eAd2
tříd	třída	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vytváření	vytváření	k1gNnSc1
Java	Javum	k1gNnSc2
objektů	objekt	k1gInPc2
</s>
<s>
Operátor	operátor	k1gInSc1
new	new	k?
má	mít	k5eAaImIp3nS
opodstatnění	opodstatnění	k1gNnSc4
při	při	k7c6
vytváření	vytváření	k1gNnSc6
Java	Jav	k1gInSc2
instancí	instance	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
var	var	k1gInSc1
date	dat	k1gInSc2
=	=	kIx~
new	new	k?
java	java	k1gFnSc1
<g/>
.	.	kIx.
<g/>
util	util	k1gInSc1
<g/>
.	.	kIx.
<g/>
Date	Date	k1gFnSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
Data	datum	k1gNnPc1
binding	binding	k1gInSc1
</s>
<s>
Pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
data	datum	k1gNnSc2
bindingu	binding	k1gInSc2
obsahuje	obsahovat	k5eAaImIp3nS
JavaFX	JavaFX	k1gMnSc1
Script	Script	k1gMnSc1
výrazy	výraz	k1gInPc1
let	léto	k1gNnPc2
a	a	k8xC
bind	binda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčové	klíčový	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
let	léto	k1gNnPc2
označuje	označovat	k5eAaImIp3nS
závislá	závislý	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
,	,	kIx,
bind	bind	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
var	var	k1gInSc1
text	text	k1gInSc1
<g/>
1	#num#	k4
=	=	kIx~
"	"	kIx"
<g/>
text	text	k1gInSc1
<g/>
1	#num#	k4
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
let	let	k1gInSc4
text	text	k1gInSc1
<g/>
2	#num#	k4
=	=	kIx~
bind	bind	k1gInSc1
text	text	k1gInSc1
<g/>
1	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
System	Syst	k1gInSc7
<g/>
.	.	kIx.
<g/>
out	out	k?
<g/>
.	.	kIx.
<g/>
println	println	k1gMnSc1
<g/>
(	(	kIx(
<g/>
text	text	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
//	//	k?
vypíše	vypsat	k5eAaPmIp3nS
<g/>
:	:	kIx,
text	text	k1gInSc1
<g/>
1	#num#	k4
</s>
<s>
text	text	k1gInSc1
<g/>
1	#num#	k4
=	=	kIx~
"	"	kIx"
<g/>
nový	nový	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
System	Syst	k1gInSc7
<g/>
.	.	kIx.
<g/>
out	out	k?
<g/>
.	.	kIx.
<g/>
println	println	k1gMnSc1
<g/>
(	(	kIx(
<g/>
text	text	k1gInSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
//	//	k?
vypíše	vypsat	k5eAaPmIp3nS
<g/>
:	:	kIx,
nový	nový	k2eAgInSc4d1
text	text	k1gInSc4
</s>
<s>
Ukázka	ukázka	k1gFnSc1
jednoduché	jednoduchý	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
</s>
<s>
Alternativa	alternativa	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
import	import	k1gInSc1
javafx	javafx	k1gInSc1
<g/>
.	.	kIx.
<g/>
ext	ext	k?
<g/>
.	.	kIx.
<g/>
swing	swing	k1gInSc1
<g/>
.	.	kIx.
<g/>
*	*	kIx~
<g/>
;	;	kIx,
</s>
<s>
SwingFrame	SwingFram	k1gInSc5
{	{	kIx(
</s>
<s>
title	titla	k1gFnSc3
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Hello	Hello	k1gNnSc1
world	worlda	k1gFnPc2
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
width	width	k1gInSc1
<g/>
:	:	kIx,
100	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
height	height	k1gInSc1
<g/>
:	:	kIx,
50	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
content	content	k1gMnSc1
<g/>
:	:	kIx,
Label	Label	k1gMnSc1
{	{	kIx(
</s>
<s>
text	text	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Hello	Hello	k1gNnSc1
world	worlda	k1gFnPc2
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
}	}	kIx)
</s>
<s>
visible	visible	k6eAd1
<g/>
:	:	kIx,
true	true	k6eAd1
<g/>
;	;	kIx,
</s>
<s>
}	}	kIx)
</s>
<s>
Alternativa	alternativa	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
import	import	k1gInSc1
javafx	javafx	k1gInSc1
<g/>
.	.	kIx.
<g/>
ext	ext	k?
<g/>
.	.	kIx.
<g/>
swing	swing	k1gInSc1
<g/>
.	.	kIx.
<g/>
*	*	kIx~
<g/>
;	;	kIx,
</s>
<s>
var	var	k1gInSc1
myFrame	myFram	k1gInSc5
<g/>
:	:	kIx,
<g/>
SwingFrame	SwingFram	k1gInSc5
=	=	kIx~
new	new	k?
SwingFrame	SwingFram	k1gInSc5
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
var	var	k1gInSc1
myLabel	myLabel	k1gInSc1
<g/>
:	:	kIx,
<g/>
Label	Label	k1gInSc1
=	=	kIx~
new	new	k?
Label	Label	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
myLabel	myLabel	k1gInSc1
<g/>
.	.	kIx.
<g/>
text	text	k1gInSc1
=	=	kIx~
"	"	kIx"
<g/>
Hello	Hello	k1gNnSc1
World	Worlda	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
;	;	kIx,
</s>
<s>
myFrame	myFram	k1gInSc5
<g/>
.	.	kIx.
<g/>
width	width	k1gMnSc1
=	=	kIx~
200	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
myFrame	myFram	k1gInSc5
<g/>
.	.	kIx.
<g/>
height	heightum	k1gNnPc2
=	=	kIx~
50	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
myFrame	myFram	k1gInSc5
<g/>
.	.	kIx.
<g/>
visible	visible	k6eAd1
=	=	kIx~
true	true	k6eAd1
<g/>
;	;	kIx,
</s>
<s>
myFrame	myFram	k1gInSc5
<g/>
.	.	kIx.
<g/>
content	content	k1gMnSc1
=	=	kIx~
myLabel	myLabel	k1gMnSc1
<g/>
;	;	kIx,
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
JavaFX	JavaFX	k1gMnSc1
Frequently	Frequently	k1gFnSc2
Asked	Asked	k1gMnSc1
Questions	Questions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
oracle	oracle	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Does	Does	k1gInSc1
JavaFX	JavaFX	k1gFnSc2
2	#num#	k4
support	support	k1gInSc1
JavaFX	JavaFX	k1gFnSc2
Script	Scripta	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
visage	visage	k1gInSc1
–	–	k?
Declarative	Declarativ	k1gInSc5
language	language	k1gFnSc3
for	forum	k1gNnPc2
expressing	expressing	k1gInSc1
user	usrat	k5eAaPmRp2nS
interfaces	interfaces	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
JavaFX	JavaFX	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
JavaFX	JavaFX	k1gFnSc2
</s>
<s>
Server	server	k1gInSc1
Javaworld	Javaworlda	k1gFnPc2
</s>
