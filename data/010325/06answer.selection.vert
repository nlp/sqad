<s>
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
řídkou	řídký	k2eAgFnSc4d1	řídká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
zadržovat	zadržovat	k5eAaImF	zadržovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
výměnu	výměna	k1gFnSc4	výměna
mezi	mezi	k7c7	mezi
povrchem	povrch	k1gInSc7	povrch
a	a	k8xC	a
okolním	okolní	k2eAgInSc7d1	okolní
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
velké	velký	k2eAgInPc4d1	velký
teplotní	teplotní	k2eAgInPc4d1	teplotní
rozdíly	rozdíl	k1gInPc4	rozdíl
během	během	k7c2	během
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
