<p>
<s>
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
přímořském	přímořský	k2eAgNnSc6d1	přímořské
městě	město	k1gNnSc6	město
Soči	Soči	k1gNnSc2	Soči
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
hostitelském	hostitelský	k2eAgNnSc6d1	hostitelské
městě	město	k1gNnSc6	město
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
hlasování	hlasování	k1gNnSc1	hlasování
členů	člen	k1gMnPc2	člen
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
byly	být	k5eAaImAgFnP	být
organizovány	organizovat	k5eAaBmNgFnP	organizovat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
centrech	centrum	k1gNnPc6	centrum
<g/>
,	,	kIx,	,
v	v	k7c6	v
pobřežním	pobřežní	k2eAgNnSc6d1	pobřežní
městě	město	k1gNnSc6	město
Soči	Soči	k1gNnSc2	Soči
a	a	k8xC	a
v	v	k7c6	v
horském	horský	k2eAgNnSc6d1	horské
středisku	středisko	k1gNnSc6	středisko
Krasnaja	Krasnajus	k1gMnSc2	Krasnajus
Poljana	Poljan	k1gMnSc2	Poljan
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhé	druhý	k4xOgFnPc4	druhý
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
pořádané	pořádaný	k2eAgFnPc4d1	pořádaná
na	na	k7c6	na
území	území	k1gNnSc6	území
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
první	první	k4xOgFnSc4	první
zimní	zimní	k2eAgFnSc4d1	zimní
olympiádu	olympiáda	k1gFnSc4	olympiáda
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
olympiádu	olympiáda	k1gFnSc4	olympiáda
se	se	k3xPyFc4	se
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS	kvalifikovat
rekordních	rekordní	k2eAgInPc2d1	rekordní
88	[number]	k4	88
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
předchozími	předchozí	k2eAgFnPc7d1	předchozí
hrami	hra	k1gFnPc7	hra
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
2010	[number]	k4	2010
se	se	k3xPyFc4	se
rozdalo	rozdat	k5eAaPmAgNnS	rozdat
98	[number]	k4	98
sad	sada	k1gFnPc2	sada
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
12	[number]	k4	12
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
nových	nový	k2eAgFnPc2d1	nová
soutěží	soutěž	k1gFnPc2	soutěž
bylo	být	k5eAaImAgNnS	být
<g/>
:	:	kIx,	:
smíšená	smíšený	k2eAgFnSc1d1	smíšená
štafeta	štafeta	k1gFnSc1	štafeta
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
<g/>
,	,	kIx,	,
ženské	ženský	k2eAgInPc4d1	ženský
skoky	skok	k1gInPc4	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
smíšené	smíšený	k2eAgNnSc1d1	smíšené
týmové	týmový	k2eAgNnSc1d1	týmové
krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
<g/>
,	,	kIx,	,
smíšené	smíšený	k2eAgFnPc1d1	smíšená
týmové	týmový	k2eAgFnPc1d1	týmová
saně	saně	k1gFnPc1	saně
<g/>
,	,	kIx,	,
lyžařský	lyžařský	k2eAgMnSc1d1	lyžařský
half-pipe	halfipat	k5eAaPmIp3nS	half-pipat
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
slopestyle	slopestyl	k1gInSc5	slopestyl
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snowboardový	snowboardový	k2eAgInSc1d1	snowboardový
slopestyle	slopestyl	k1gInSc5	slopestyl
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snowboardový	snowboardový	k2eAgInSc1d1	snowboardový
paralelní	paralelní	k2eAgInSc4d1	paralelní
slalom	slalom	k1gInSc4	slalom
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc4d1	mužský
a	a	k8xC	a
ženský	ženský	k2eAgInSc4d1	ženský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
hry	hra	k1gFnPc1	hra
stály	stát	k5eAaImAgInP	stát
51	[number]	k4	51
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
původní	původní	k2eAgInPc4d1	původní
odhady	odhad	k1gInPc4	odhad
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Stály	stát	k5eAaImAgFnP	stát
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
nákladů	náklad	k1gInPc2	náklad
předchozích	předchozí	k2eAgFnPc2d1	předchozí
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
o	o	k7c4	o
11	[number]	k4	11
miliard	miliarda	k4xCgFnPc2	miliarda
překonaly	překonat	k5eAaPmAgFnP	překonat
hry	hra	k1gFnPc1	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
byly	být	k5eAaImAgFnP	být
tedy	tedy	k9	tedy
nejdražšími	drahý	k2eAgFnPc7d3	nejdražší
olympijskými	olympijský	k2eAgFnPc7d1	olympijská
hrami	hra	k1gFnPc7	hra
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
měly	mít	k5eAaImAgFnP	mít
rekordních	rekordní	k2eAgInPc2d1	rekordní
2,1	[number]	k4	2,1
miliardy	miliarda	k4xCgFnSc2	miliarda
diváků	divák	k1gMnPc2	divák
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Volba	volba	k1gFnSc1	volba
pořadatele	pořadatel	k1gMnSc2	pořadatel
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Soči	Soči	k1gNnSc2	Soči
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
konečný	konečný	k2eAgInSc4d1	konečný
seznam	seznam	k1gInSc4	seznam
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
pořadatelství	pořadatelství	k1gNnSc4	pořadatelství
zimních	zimní	k2eAgFnPc2d1	zimní
her	hra	k1gFnPc2	hra
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městy	město	k1gNnPc7	město
Pchjongčchang	Pchjongčchang	k1gMnSc1	Pchjongčchang
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
a	a	k8xC	a
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
Soči	Soči	k1gNnSc2	Soči
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
na	na	k7c4	na
119	[number]	k4	119
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
MOV	MOV	kA	MOV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
v	v	k7c4	v
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Oznámil	oznámit	k5eAaPmAgMnS	oznámit
jej	on	k3xPp3gNnSc4	on
prezident	prezident	k1gMnSc1	prezident
MOV	MOV	kA	MOV
Jacques	Jacques	k1gMnSc1	Jacques
Rogge	Rogg	k1gInSc2	Rogg
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
hlasování	hlasování	k1gNnSc6	hlasování
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
musela	muset	k5eAaImAgNnP	muset
kandidátská	kandidátský	k2eAgNnPc1d1	kandidátské
města	město	k1gNnPc1	město
po	po	k7c4	po
schválení	schválení	k1gNnSc4	schválení
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
poslat	poslat	k5eAaPmF	poslat
přihlášku	přihláška	k1gFnSc4	přihláška
a	a	k8xC	a
vyplněný	vyplněný	k2eAgInSc4d1	vyplněný
dotazník	dotazník	k1gInSc4	dotazník
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
města	město	k1gNnSc2	město
Soči	Soči	k1gNnSc1	Soči
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaca	Jaca	k1gFnSc1	Jaca
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Almaty	Almat	k1gInPc1	Almat
(	(	kIx(	(
<g/>
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pchjongčchang	Pchjongčchang	k1gInSc1	Pchjongčchang
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sofie	Sofie	k1gFnSc1	Sofie
(	(	kIx(	(
<g/>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bordžomi	Bordžo	k1gFnPc7	Bordžo
(	(	kIx(	(
<g/>
Gruzie	Gruzie	k1gFnPc1	Gruzie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
finální	finální	k2eAgMnPc1d1	finální
kandidáti	kandidát	k1gMnPc1	kandidát
byli	být	k5eAaImAgMnP	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
radou	rada	k1gFnSc7	rada
MOV	MOV	kA	MOV
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
Organizační	organizační	k2eAgInSc1d1	organizační
výbor	výbor	k1gInSc1	výbor
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Olympijský	olympijský	k2eAgInSc1d1	olympijský
park	park	k1gInSc1	park
==	==	k?	==
</s>
</p>
<p>
<s>
Hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
nově	nově	k6eAd1	nově
postaveném	postavený	k2eAgInSc6d1	postavený
Olympijském	olympijský	k2eAgInSc6d1	olympijský
parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
stadionů	stadion	k1gInPc2	stadion
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Soči	Soči	k1gNnPc3	Soči
===	===	k?	===
</s>
</p>
<p>
<s>
Fišt	Fišt	k2eAgInSc1d1	Fišt
stadion	stadion	k1gInSc1	stadion
–	–	k?	–
ceremonie	ceremonie	k1gFnSc1	ceremonie
<g/>
,	,	kIx,	,
40	[number]	k4	40
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
</s>
</p>
<p>
<s>
Bolšoj	Bolšoj	k1gInSc1	Bolšoj
stadion	stadion	k1gInSc1	stadion
–	–	k?	–
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
12	[number]	k4	12
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
</s>
</p>
<p>
<s>
Šajba	šajba	k1gFnSc1	šajba
arena	aren	k1gInSc2	aren
–	–	k?	–
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
7	[number]	k4	7
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
</s>
</p>
<p>
<s>
Adler-Arena	Adler-Arena	k1gFnSc1	Adler-Arena
–	–	k?	–
rychlobruslení	rychlobruslení	k1gNnSc1	rychlobruslení
<g/>
,	,	kIx,	,
8	[number]	k4	8
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
</s>
</p>
<p>
<s>
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
Ajsberg	Ajsberg	k1gInSc1	Ajsberg
–	–	k?	–
krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
<g/>
,	,	kIx,	,
short	short	k1gInSc1	short
track	tracka	k1gFnPc2	tracka
<g/>
,	,	kIx,	,
12	[number]	k4	12
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
</s>
</p>
<p>
<s>
Leďanoj	Leďanoj	k1gInSc1	Leďanoj
kub	kubo	k1gNnPc2	kubo
–	–	k?	–
curling	curling	k1gInSc1	curling
<g/>
,	,	kIx,	,
3	[number]	k4	3
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vesnice	vesnice	k1gFnSc1	vesnice
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
vysílací	vysílací	k2eAgNnSc1d1	vysílací
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
tiskové	tiskový	k2eAgNnSc1d1	tiskové
středisko	středisko	k1gNnSc1	středisko
</s>
</p>
<p>
<s>
===	===	k?	===
Horské	Horské	k2eAgNnSc1d1	Horské
středisko	středisko	k1gNnSc1	středisko
Krasnaja	Krasnajus	k1gMnSc2	Krasnajus
Poljana	Poljan	k1gMnSc2	Poljan
===	===	k?	===
</s>
</p>
<p>
<s>
Lyžařský	lyžařský	k2eAgInSc1d1	lyžařský
areál	areál	k1gInSc1	areál
Laura	Laura	k1gFnSc1	Laura
–	–	k?	–
biatlon	biatlon	k1gInSc1	biatlon
a	a	k8xC	a
běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
</s>
</p>
<p>
<s>
Park	park	k1gInSc1	park
extrémních	extrémní	k2eAgInPc2d1	extrémní
sportů	sport	k1gInPc2	sport
Roza	Roza	k1gFnSc1	Roza
Chutor	chutor	k1gInSc1	chutor
–	–	k?	–
akrobatické	akrobatický	k2eAgNnSc4d1	akrobatické
lyžování	lyžování	k1gNnSc4	lyžování
a	a	k8xC	a
snowboarding	snowboarding	k1gInSc4	snowboarding
</s>
</p>
<p>
<s>
Lyžařský	lyžařský	k2eAgInSc1d1	lyžařský
areál	areál	k1gInSc1	areál
Roza	Roza	k1gMnSc1	Roza
Chutor	chutor	k1gInSc1	chutor
–	–	k?	–
alpské	alpský	k2eAgNnSc1d1	alpské
lyžování	lyžování	k1gNnSc1	lyžování
</s>
</p>
<p>
<s>
Bobová	bobový	k2eAgFnSc1d1	bobová
a	a	k8xC	a
sáňkařská	sáňkařský	k2eAgFnSc1d1	sáňkařská
dráha	dráha	k1gFnSc1	dráha
Sanki	Sanki	k1gNnSc2	Sanki
–	–	k?	–
boby	bob	k1gInPc1	bob
<g/>
,	,	kIx,	,
saně	saně	k1gFnPc1	saně
a	a	k8xC	a
skeleton	skeleton	k1gInSc1	skeleton
</s>
</p>
<p>
<s>
Russkije	Russkít	k5eAaPmIp3nS	Russkít
Gorki	Gorki	k1gFnPc4	Gorki
–	–	k?	–
skoky	skok	k1gInPc4	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
severská	severský	k2eAgFnSc1d1	severská
kombinace	kombinace	k1gFnSc1	kombinace
</s>
</p>
<p>
<s>
Plošina	plošina	k1gFnSc1	plošina
Roza	Roz	k2eAgFnSc1d1	Roza
Chutor	chutor	k1gInSc4	chutor
–	–	k?	–
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vesnice	vesnice	k1gFnSc1	vesnice
</s>
</p>
<p>
<s>
===	===	k?	===
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
využití	využití	k1gNnSc1	využití
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
olympijského	olympijský	k2eAgInSc2d1	olympijský
parku	park	k1gInSc2	park
konaly	konat	k5eAaImAgFnP	konat
i	i	k9	i
Zimní	zimní	k2eAgFnPc1d1	zimní
paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Soči	Soči	k1gNnPc6	Soči
poté	poté	k6eAd1	poté
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
po	po	k7c4	po
sto	sto	k4xCgNnSc4	sto
letech	léto	k1gNnPc6	léto
závody	závod	k1gInPc4	závod
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
série	série	k1gFnSc1	série
Formule	formule	k1gFnSc1	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
dohoda	dohoda	k1gFnSc1	dohoda
je	být	k5eAaImIp3nS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
10	[number]	k4	10
ruských	ruský	k2eAgInPc6d1	ruský
městech	město	k1gNnPc6	město
naplánováno	naplánován	k2eAgNnSc1d1	naplánováno
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aréně	aréna	k1gFnSc6	aréna
Bolšoj	Bolšoj	k1gInSc1	Bolšoj
také	také	k6eAd1	také
hraje	hrát	k5eAaImIp3nS	hrát
nový	nový	k2eAgInSc1d1	nový
hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
KHL	KHL	kA	KHL
<g/>
,	,	kIx,	,
Sočinskije	Sočinskije	k1gFnPc2	Sočinskije
Leopardy	leopard	k1gMnPc4	leopard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Symboly	symbol	k1gInPc1	symbol
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
pochodeň	pochodeň	k1gFnSc1	pochodeň
===	===	k?	===
</s>
</p>
<p>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
štafeta	štafeta	k1gFnSc1	štafeta
s	s	k7c7	s
pochodní	pochodeň	k1gFnSc7	pochodeň
byla	být	k5eAaImAgFnS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
pouť	pouť	k1gFnSc1	pouť
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zimních	zimní	k2eAgFnPc2d1	zimní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
měřila	měřit	k5eAaImAgFnS	měřit
65	[number]	k4	65
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Olympijskou	olympijský	k2eAgFnSc4d1	olympijská
pochodeň	pochodeň	k1gFnSc4	pochodeň
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
předali	předat	k5eAaPmAgMnP	předat
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1	ruský
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
Oleg	Oleg	k1gMnSc1	Oleg
Kotov	Kotov	k1gInSc1	Kotov
a	a	k8xC	a
Sergej	Sergej	k1gMnSc1	Sergej
Rjazanskij	Rjazanskij	k1gMnSc1	Rjazanskij
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
předali	předat	k5eAaPmAgMnP	předat
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
z	z	k7c2	z
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
pochodeň	pochodeň	k1gFnSc1	pochodeň
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
podívala	podívat	k5eAaImAgFnS	podívat
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
atomového	atomový	k2eAgInSc2d1	atomový
ledoborce	ledoborec	k1gInSc2	ledoborec
(	(	kIx(	(
<g/>
50	[number]	k4	50
let	léto	k1gNnPc2	léto
Pobedy	Pobeda	k1gMnSc2	Pobeda
<g/>
)	)	kIx)	)
na	na	k7c4	na
severní	severní	k2eAgInSc4d1	severní
pól	pól	k1gInSc4	pól
<g/>
,	,	kIx,	,
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Evropy	Evropa	k1gFnSc2	Evropa
Elbrus	Elbrus	k1gInSc1	Elbrus
i	i	k8xC	i
do	do	k7c2	do
nejhlubšího	hluboký	k2eAgNnSc2d3	nejhlubší
světového	světový	k2eAgNnSc2d1	světové
jezera	jezero	k1gNnSc2	jezero
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
zažehli	zažehnout	k5eAaPmAgMnP	zažehnout
olympijský	olympijský	k2eAgInSc4d1	olympijský
oheň	oheň	k1gInSc4	oheň
hokejista	hokejista	k1gMnSc1	hokejista
Vladislav	Vladislav	k1gMnSc1	Vladislav
Treťjak	Treťjak	k1gMnSc1	Treťjak
a	a	k8xC	a
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Irina	Irina	k1gFnSc1	Irina
Rodninová	Rodninový	k2eAgFnSc1d1	Rodninový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Maskoti	Maskot	k1gMnPc1	Maskot
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
výběru	výběr	k1gInSc6	výběr
maskotů	maskot	k1gInPc2	maskot
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
podílet	podílet	k5eAaImF	podílet
všichni	všechen	k3xTgMnPc1	všechen
ruští	ruský	k2eAgMnPc1d1	ruský
občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
v	v	k7c4	v
11.20	[number]	k4	11.20
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
GMT	GMT	kA	GMT
+3	+3	k4	+3
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
konečné	konečný	k2eAgInPc1d1	konečný
výsledky	výsledek	k1gInPc1	výsledek
veřejného	veřejný	k2eAgNnSc2d1	veřejné
hlasování	hlasování	k1gNnSc2	hlasování
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
"	"	kIx"	"
<g/>
Talismaniya	Talismaniya	k1gFnSc1	Talismaniya
Soči	Soči	k1gNnSc1	Soči
2014	[number]	k4	2014
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc1d1	volební
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
tři	tři	k4xCgInPc4	tři
návrhy	návrh	k1gInPc4	návrh
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
hlasů	hlas	k1gInPc2	hlas
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
maskoty	maskot	k1gInPc1	maskot
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězily	zvítězit	k5eAaPmAgInP	zvítězit
tyto	tento	k3xDgInPc1	tento
návrhy	návrh	k1gInPc1	návrh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lední	lední	k2eAgMnSc1d1	lední
medvěd	medvěd	k1gMnSc1	medvěd
(	(	kIx(	(
<g/>
Oleg	Oleg	k1gMnSc1	Oleg
Serdečnij	Serdečnij	k1gMnSc1	Serdečnij
<g/>
,	,	kIx,	,
Soči	Soči	k1gNnSc1	Soči
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zajíc	Zajíc	k1gMnSc1	Zajíc
bělák	bělák	k1gMnSc1	bělák
(	(	kIx(	(
<g/>
Silvija	Silvij	k2eAgFnSc1d1	Silvij
Petrovová	Petrovová	k1gFnSc1	Petrovová
<g/>
,	,	kIx,	,
Čuvašsko	Čuvašsko	k1gNnSc1	Čuvašsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sněžný	sněžný	k2eAgMnSc1d1	sněžný
levhart	levhart	k1gMnSc1	levhart
(	(	kIx(	(
<g/>
Vadim	Vadim	k?	Vadim
Pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
Nachodka	Nachodka	k1gFnSc1	Nachodka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Účast	účast	k1gFnSc1	účast
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
OH	OH	kA	OH
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
se	se	k3xPyFc4	se
kvalifikovalo	kvalifikovat	k5eAaBmAgNnS	kvalifikovat
rekordních	rekordní	k2eAgInPc2d1	rekordní
88	[number]	k4	88
národních	národní	k2eAgInPc2d1	národní
olympijských	olympijský	k2eAgInPc2d1	olympijský
výborů	výbor	k1gInPc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
nezúčastní	zúčastnit	k5eNaPmIp3nS	zúčastnit
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
a	a	k8xC	a
Senegal	Senegal	k1gInSc1	Senegal
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
přibyly	přibýt	k5eAaPmAgInP	přibýt
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc1d1	britský
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
<g/>
,	,	kIx,	,
Portoriko	Portoriko	k1gNnSc1	Portoriko
a	a	k8xC	a
Thajsko	Thajsko	k1gNnSc1	Thajsko
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
šest	šest	k4xCc4	šest
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
poprvé	poprvé	k6eAd1	poprvé
<g/>
:	:	kIx,	:
Malta	Malta	k1gFnSc1	Malta
<g/>
,	,	kIx,	,
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
,	,	kIx,	,
Togo	Togo	k1gNnSc1	Togo
<g/>
,	,	kIx,	,
Tonga	Tonga	k1gFnSc1	Tonga
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
.	.	kIx.	.
</s>
<s>
Sportovci	sportovec	k1gMnPc1	sportovec
Indie	Indie	k1gFnSc2	Indie
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
pod	pod	k7c7	pod
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
byla	být	k5eAaImAgFnS	být
suspendována	suspendovat	k5eAaPmNgFnS	suspendovat
po	po	k7c6	po
volebním	volební	k2eAgInSc6d1	volební
procesu	proces	k1gInSc6	proces
Indické	indický	k2eAgFnSc2d1	indická
olympijské	olympijský	k2eAgFnSc2d1	olympijská
asociace	asociace	k1gFnSc2	asociace
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
reprezentovalo	reprezentovat	k5eAaImAgNnS	reprezentovat
88	[number]	k4	88
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Marketing	marketing	k1gInSc1	marketing
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc7d1	oficiální
videohrou	videohra	k1gFnSc7	videohra
ZOH	ZOH	kA	ZOH
Soči	Soči	k1gNnSc6	Soči
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Mario	Mario	k1gMnSc1	Mario
&	&	k?	&
Sonic	Sonic	k1gMnSc1	Sonic
at	at	k?	at
the	the	k?	the
Sochi	Soch	k1gFnSc2	Soch
2014	[number]	k4	2014
Olympic	Olympic	k1gMnSc1	Olympic
Winter	Winter	k1gMnSc1	Winter
Games	Games	k1gMnSc1	Games
<g/>
"	"	kIx"	"
od	od	k7c2	od
Nintenda	Nintend	k1gMnSc2	Nintend
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
vydaná	vydaný	k2eAgFnSc1d1	vydaná
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Sochi	Sochi	k1gNnSc4	Sochi
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Ski	ski	k1gFnSc1	ski
Slopestyle	Slopestyl	k1gInSc5	Slopestyl
Challenge	Challenge	k1gNnPc4	Challenge
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Android	android	k1gInSc1	android
a	a	k8xC	a
"	"	kIx"	"
<g/>
Sochi	Sochi	k1gNnSc1	Sochi
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Olympic	Olympice	k1gFnPc2	Olympice
Games	Gamesa	k1gFnPc2	Gamesa
Resort	resort	k1gInSc1	resort
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
on-line	onin	k1gInSc5	on-lin
sociální	sociální	k2eAgFnSc1d1	sociální
síť	síť	k1gFnSc1	síť
Facebook	Facebook	k1gInSc1	Facebook
<g/>
.	.	kIx.	.
<g/>
Vstupenky	vstupenka	k1gFnPc1	vstupenka
na	na	k7c4	na
Olympiádu	olympiáda	k1gFnSc4	olympiáda
stály	stát	k5eAaImAgFnP	stát
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
550	[number]	k4	550
do	do	k7c2	do
26000	[number]	k4	26000
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
Škoda	škoda	k6eAd1	škoda
Auto	auto	k1gNnSc1	auto
je	být	k5eAaImIp3nS	být
dodavatelem	dodavatel	k1gMnSc7	dodavatel
vozů	vůz	k1gInPc2	vůz
pro	pro	k7c4	pro
OH	OH	kA	OH
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sponzoringu	sponzoring	k1gInSc2	sponzoring
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrakt	kontrakt	k1gInSc1	kontrakt
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Flotilu	flotila	k1gFnSc4	flotila
tvořilo	tvořit	k5eAaImAgNnS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
set	sto	k4xCgNnPc2	sto
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
modely	model	k1gInPc7	model
Superb	Superba	k1gFnPc2	Superba
<g/>
,	,	kIx,	,
Yeti	yeti	k1gMnSc1	yeti
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
Octavia	octavia	k1gFnSc1	octavia
třetí	třetí	k4xOgFnSc2	třetí
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Auta	auto	k1gNnPc1	auto
jsou	být	k5eAaImIp3nP	být
určená	určený	k2eAgNnPc1d1	určené
pro	pro	k7c4	pro
sportovce	sportovec	k1gMnSc4	sportovec
<g/>
,	,	kIx,	,
VIP	VIP	kA	VIP
hosty	host	k1gMnPc7	host
a	a	k8xC	a
funkcionáře	funkcionář	k1gMnPc4	funkcionář
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
českými	český	k2eAgInPc7d1	český
puky	puk	k1gInPc7	puk
firmy	firma	k1gFnSc2	firma
Rubena	rubit	k5eAaImNgFnS	rubit
z	z	k7c2	z
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
hrách	hra	k1gFnPc6	hra
od	od	k7c2	od
Nagana	Nagano	k1gNnSc2	Nagano
1998	[number]	k4	1998
se	se	k3xPyFc4	se
také	také	k9	také
používaly	používat	k5eAaImAgInP	používat
české	český	k2eAgInPc4d1	český
puky	puk	k1gInPc4	puk
firmy	firma	k1gFnSc2	firma
Gufex	Gufex	k1gInSc1	Gufex
z	z	k7c2	z
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
Vancouver	Vancouver	k1gInSc1	Vancouver
2010	[number]	k4	2010
s	s	k7c7	s
puky	puk	k1gInPc7	puk
kanadskými	kanadský	k2eAgInPc7d1	kanadský
<g/>
.	.	kIx.	.
<g/>
Hodinářská	hodinářský	k2eAgFnSc1d1	hodinářská
firma	firma	k1gFnSc1	firma
Prim	prima	k1gFnPc2	prima
z	z	k7c2	z
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
člena	člen	k1gMnSc4	člen
olympijské	olympijský	k2eAgFnSc2d1	olympijská
delegace	delegace	k1gFnSc2	delegace
speciální	speciální	k2eAgInSc4d1	speciální
model	model	k1gInSc4	model
hodinek	hodinka	k1gFnPc2	hodinka
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
získal	získat	k5eAaPmAgMnS	získat
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
hodinky	hodinka	k1gFnPc4	hodinka
modelu	model	k1gInSc2	model
Diplomat	diplomat	k1gInSc1	diplomat
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
93	[number]	k4	93
tisíc	tisíc	k4xCgInSc1	tisíc
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
sportovce	sportovec	k1gMnSc2	sportovec
<g/>
.	.	kIx.	.
<g/>
Olympionici	olympionik	k1gMnPc1	olympionik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
zlato	zlato	k1gNnSc4	zlato
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
odměněni	odměnit	k5eAaPmNgMnP	odměnit
zlatými	zlatý	k2eAgFnPc7d1	zlatá
medailemi	medaile	k1gFnPc7	medaile
s	s	k7c7	s
úlomky	úlomek	k1gInPc7	úlomek
Čeljabinského	čeljabinský	k2eAgInSc2d1	čeljabinský
meteoritu	meteorit	k1gInSc2	meteorit
–	–	k?	–
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
přesně	přesně	k6eAd1	přesně
rok	rok	k1gInSc4	rok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
kusy	kus	k1gInPc7	kus
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
dopadly	dopadnout	k5eAaPmAgInP	dopadnout
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Čebarkul	Čebarkula	k1gFnPc2	Čebarkula
na	na	k7c6	na
Urale	Ural	k1gInSc6	Ural
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sponzoři	sponzor	k1gMnPc5	sponzor
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
partnery	partner	k1gMnPc7	partner
Olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Kalendář	kalendář	k1gInSc1	kalendář
soutěží	soutěž	k1gFnPc2	soutěž
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Medailové	medailový	k2eAgNnSc4d1	medailové
pořadí	pořadí	k1gNnSc4	pořadí
zemí	zem	k1gFnPc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
medailí	medaile	k1gFnPc2	medaile
získala	získat	k5eAaPmAgFnS	získat
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Ireen	Irena	k1gFnPc2	Irena
Wüstová	Wüstový	k2eAgFnSc1d1	Wüstová
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
získala	získat	k5eAaPmAgFnS	získat
pět	pět	k4xCc4	pět
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
stříbrné	stříbrná	k1gFnPc4	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Viktor	Viktor	k1gMnSc1	Viktor
An	An	k1gMnSc1	An
<g/>
,	,	kIx,	,
norská	norský	k2eAgFnSc1d1	norská
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Marit	Marit	k1gMnSc1	Marit
Bjø	Bjø	k1gMnSc1	Bjø
a	a	k8xC	a
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
biatlonistka	biatlonistka	k1gFnSc1	biatlonistka
Darja	Darja	k1gFnSc1	Darja
Domračevová	Domračevová	k1gFnSc1	Domračevová
získali	získat	k5eAaPmAgMnP	získat
nerozhodně	rozhodně	k6eNd1	rozhodně
nejvíce	nejvíce	k6eAd1	nejvíce
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
tři	tři	k4xCgFnPc4	tři
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
lyžařce	lyžařka	k1gFnSc3	lyžařka
Tině	Tina	k1gFnSc3	Tina
Mazeové	Mazeová	k1gFnSc2	Mazeová
získalo	získat	k5eAaPmAgNnS	získat
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
ze	z	k7c2	z
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Sáňkař	sáňkař	k1gMnSc1	sáňkař
Armin	Armin	k1gMnSc1	Armin
Zöggeler	Zöggeler	k1gMnSc1	Zöggeler
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
sportovcem	sportovec	k1gMnSc7	sportovec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
šest	šest	k4xCc4	šest
medailí	medaile	k1gFnPc2	medaile
v	v	k7c6	v
šesti	šest	k4xCc6	šest
po	po	k7c4	po
sobě	se	k3xPyFc3	se
jdoucích	jdoucí	k2eAgFnPc6d1	jdoucí
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některým	některý	k3yIgMnPc3	některý
ruským	ruský	k2eAgMnPc3d1	ruský
sportovcům	sportovec	k1gMnPc3	sportovec
byly	být	k5eAaImAgInP	být
odebrány	odebrat	k5eAaPmNgInP	odebrat
jejich	jejich	k3xOp3gFnSc7	jejich
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
kvůli	kvůli	k7c3	kvůli
obvinění	obvinění	k1gNnSc3	obvinění
z	z	k7c2	z
dopingu	doping	k1gInSc2	doping
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
odvolání	odvolání	k1gNnSc6	odvolání
bylo	být	k5eAaImAgNnS	být
28	[number]	k4	28
rusů	rus	k1gMnPc2	rus
očištěno	očistit	k5eAaPmNgNnS	očistit
a	a	k8xC	a
medaile	medaile	k1gFnPc4	medaile
byly	být	k5eAaImAgFnP	být
znovu	znovu	k6eAd1	znovu
navráceny	navrácen	k2eAgInPc1d1	navrácen
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
tak	tak	k9	tak
znovu	znovu	k6eAd1	znovu
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
medailovém	medailový	k2eAgNnSc6d1	medailové
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česko	Česko	k1gNnSc1	Česko
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
2014	[number]	k4	2014
==	==	k?	==
</s>
</p>
<p>
<s>
Českou	český	k2eAgFnSc4d1	Česká
výpravu	výprava	k1gFnSc4	výprava
na	na	k7c4	na
Zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
2014	[number]	k4	2014
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
tvořilo	tvořit	k5eAaImAgNnS	tvořit
88	[number]	k4	88
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnPc4	medaile
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snowboardistka	snowboardistka	k1gFnSc1	snowboardistka
Eva	Eva	k1gFnSc1	Eva
Samková	Samková	k1gFnSc1	Samková
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc1	zlato
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
biatlonisté	biatlonista	k1gMnPc1	biatlonista
Ondřej	Ondřej	k1gMnSc1	Ondřej
Moravec	Moravec	k1gMnSc1	Moravec
(	(	kIx(	(
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc1	bronz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Koukalová	Koukalová	k1gFnSc1	Koukalová
(	(	kIx(	(
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Soukup	Soukup	k1gMnSc1	Soukup
(	(	kIx(	(
<g/>
bronz	bronz	k1gInSc1	bronz
<g/>
)	)	kIx)	)
a	a	k8xC	a
smíšená	smíšený	k2eAgFnSc1d1	smíšená
biatlonová	biatlonový	k2eAgFnSc1d1	biatlonová
štafeta	štafeta	k1gFnSc1	štafeta
(	(	kIx(	(
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
nechal	nechat	k5eAaPmAgInS	nechat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
postavit	postavit	k5eAaPmF	postavit
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
Olympijský	olympijský	k2eAgInSc1d1	olympijský
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
fungoval	fungovat	k5eAaImAgInS	fungovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
velkoplošné	velkoplošný	k2eAgInPc1d1	velkoplošný
LED	led	k1gInSc1	led
obrazovky	obrazovka	k1gFnSc2	obrazovka
<g/>
,	,	kIx,	,
přenosy	přenos	k1gInPc4	přenos
na	na	k7c4	na
100	[number]	k4	100
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
bruslařská	bruslařský	k2eAgFnSc1d1	bruslařská
plocha	plocha	k1gFnSc1	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
6500	[number]	k4	6500
m2	m2	k4	m2
<g/>
,	,	kIx,	,
curlingová	curlingový	k2eAgFnSc1d1	curlingová
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
běžkařský	běžkařský	k2eAgInSc1d1	běžkařský
ovál	ovál	k1gInSc1	ovál
<g/>
,	,	kIx,	,
snow	snow	k?	snow
park	park	k1gInSc1	park
na	na	k7c4	na
snowboarding	snowboarding	k1gInSc4	snowboarding
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc1	dva
hokejová	hokejový	k2eAgNnPc1d1	hokejové
hřiště	hřiště	k1gNnPc1	hřiště
a	a	k8xC	a
laserová	laserový	k2eAgFnSc1d1	laserová
střelnice	střelnice	k1gFnSc1	střelnice
pro	pro	k7c4	pro
biatlon	biatlon	k1gInSc4	biatlon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kontroverze	kontroverze	k1gFnSc1	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Olympiáda	olympiáda	k1gFnSc1	olympiáda
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
několik	několik	k4yIc4	několik
kontroverzí	kontroverze	k1gFnPc2	kontroverze
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kritiky	kritika	k1gFnSc2	kritika
dodržování	dodržování	k1gNnSc2	dodržování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
obavy	obava	k1gFnPc1	obava
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
sportovců	sportovec	k1gMnPc2	sportovec
na	na	k7c4	na
dopad	dopad	k1gInSc4	dopad
zákazu	zákaz	k1gInSc2	zákaz
propagace	propagace	k1gFnSc2	propagace
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
kvůli	kvůli	k7c3	kvůli
hrozbám	hrozba	k1gFnPc3	hrozba
džihádistů	džihádista	k1gMnPc2	džihádista
a	a	k8xC	a
údajné	údajný	k2eAgNnSc1d1	údajné
porušování	porušování	k1gNnSc1	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
při	při	k7c6	při
budování	budování	k1gNnSc6	budování
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
tématu	téma	k1gNnSc2	téma
věnuje	věnovat	k5eAaPmIp3nS	věnovat
projekt	projekt	k1gInSc1	projekt
Utajené	utajený	k2eAgFnSc2d1	utajená
disciplíny	disciplína	k1gFnSc2	disciplína
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
her	hra	k1gFnPc2	hra
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
dvěma	dva	k4xCgFnPc3	dva
politicky	politicky	k6eAd1	politicky
motivovaným	motivovaný	k2eAgInPc3d1	motivovaný
incidentům	incident	k1gInPc3	incident
<g/>
.	.	kIx.	.
</s>
<s>
Transsexuální	Transsexuální	k2eAgFnSc1d1	Transsexuální
bývalá	bývalý	k2eAgFnSc1d1	bývalá
politička	politička	k1gFnSc1	politička
Vladimir	Vladimir	k1gInSc4	Vladimir
Luxuria	Luxurium	k1gNnSc2	Luxurium
byla	být	k5eAaImAgFnS	být
zadržena	zadržet	k5eAaPmNgFnS	zadržet
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Olympijském	olympijský	k2eAgInSc6d1	olympijský
parku	park	k1gInSc6	park
krátce	krátce	k6eAd1	krátce
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
duhový	duhový	k2eAgInSc4d1	duhový
symbol	symbol	k1gInSc4	symbol
LGBT	LGBT	kA	LGBT
<g/>
.	.	kIx.	.
</s>
<s>
Členky	členka	k1gFnPc1	členka
punkové	punkový	k2eAgFnSc2d1	punková
skupiny	skupina	k1gFnSc2	skupina
Pussy	Pussa	k1gFnPc1	Pussa
Riots	Riots	k1gInSc4	Riots
byly	být	k5eAaImAgFnP	být
násilím	násilí	k1gNnSc7	násilí
rozehnány	rozehnat	k5eAaPmNgFnP	rozehnat
přímo	přímo	k6eAd1	přímo
pod	pod	k7c7	pod
olympijskými	olympijský	k2eAgInPc7d1	olympijský
kruhy	kruh	k1gInPc7	kruh
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Soči	Soči	k1gNnSc2	Soči
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
olympijská	olympijský	k2eAgNnPc4d1	Olympijské
sportoviště	sportoviště	k1gNnPc4	sportoviště
<g/>
)	)	kIx)	)
při	při	k7c6	při
nahrávání	nahrávání	k1gNnSc6	nahrávání
videoklipu	videoklip	k1gInSc2	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Putin	putin	k2eAgInSc1d1	putin
nás	my	k3xPp1nPc4	my
naučí	naučit	k5eAaPmIp3nS	naučit
milovat	milovat	k5eAaImF	milovat
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
demonstrace	demonstrace	k1gFnPc1	demonstrace
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
mohly	moct	k5eAaImAgInP	moct
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
konat	konat	k5eAaImF	konat
výhradně	výhradně	k6eAd1	výhradně
po	po	k7c6	po
souhlasu	souhlas	k1gInSc6	souhlas
místních	místní	k2eAgInPc2d1	místní
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
incident	incident	k1gInSc4	incident
reagoval	reagovat	k5eAaBmAgInS	reagovat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
věc	věc	k1gFnSc4	věc
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
znepokojivou	znepokojivý	k2eAgFnSc4d1	znepokojivá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
civilní	civilní	k2eAgNnPc4d1	civilní
a	a	k8xC	a
nesouvisející	související	k2eNgNnPc4d1	nesouvisející
s	s	k7c7	s
olympijským	olympijský	k2eAgNnSc7d1	Olympijské
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
také	také	k9	také
přemrštěné	přemrštěný	k2eAgInPc1d1	přemrštěný
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
uspořádání	uspořádání	k1gNnSc4	uspořádání
her	hra	k1gFnPc2	hra
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
51	[number]	k4	51
miliard	miliarda	k4xCgFnPc2	miliarda
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
staly	stát	k5eAaPmAgFnP	stát
nejnákladnějšími	nákladný	k2eAgFnPc7d3	nejnákladnější
olympijskými	olympijský	k2eAgFnPc7d1	olympijská
hrami	hra	k1gFnPc7	hra
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
podávání	podávání	k1gNnSc6	podávání
kandidatury	kandidatura	k1gFnSc2	kandidatura
přitom	přitom	k6eAd1	přitom
byly	být	k5eAaImAgInP	být
náklady	náklad	k1gInPc1	náklad
vyčísleny	vyčíslen	k2eAgInPc1d1	vyčíslen
na	na	k7c4	na
12	[number]	k4	12
miliard	miliarda	k4xCgFnPc2	miliarda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
zvýšení	zvýšení	k1gNnSc3	zvýšení
způsobila	způsobit	k5eAaPmAgFnS	způsobit
také	také	k9	také
mimořádná	mimořádný	k2eAgNnPc4d1	mimořádné
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
ve	v	k7c6	v
Volgogradu	Volgograd	k1gInSc3	Volgograd
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Komentátoři	komentátor	k1gMnPc1	komentátor
ale	ale	k9	ale
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
zvlášť	zvlášť	k6eAd1	zvlášť
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
míru	míra	k1gFnSc4	míra
korupce	korupce	k1gFnSc2	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgInPc1d1	ruský
úřady	úřad	k1gInPc1	úřad
ale	ale	k9	ale
žádaly	žádat	k5eAaImAgInP	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
nezapočítávaly	započítávat	k5eNaImAgFnP	započítávat
do	do	k7c2	do
celkové	celkový	k2eAgFnSc2d1	celková
ceny	cena	k1gFnSc2	cena
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
jen	jen	k9	jen
zhruba	zhruba	k6eAd1	zhruba
pět	pět	k4xCc4	pět
až	až	k9	až
sedm	sedm	k4xCc4	sedm
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
vynaložilo	vynaložit	k5eAaPmAgNnS	vynaložit
na	na	k7c4	na
organizaci	organizace	k1gFnSc4	organizace
nesouvisející	související	k2eNgFnSc4d1	nesouvisející
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
silniční	silniční	k2eAgFnSc1d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
ze	z	k7c2	z
Soči	Soči	k1gNnSc2	Soči
do	do	k7c2	do
Krásné	krásný	k2eAgFnSc2d1	krásná
Poljany	Poljana	k1gFnSc2	Poljana
stálo	stát	k5eAaImAgNnS	stát
8,7	[number]	k4	8,7
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
uspořádání	uspořádání	k1gNnSc4	uspořádání
celých	celý	k2eAgFnPc2d1	celá
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2010	[number]	k4	2010
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
byla	být	k5eAaImAgFnS	být
kritizována	kritizován	k2eAgFnSc1d1	kritizována
nepřipravenost	nepřipravenost	k1gFnSc1	nepřipravenost
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nedokončené	dokončený	k2eNgFnPc4d1	nedokončená
ubytovací	ubytovací	k2eAgFnPc4d1	ubytovací
kapacity	kapacita	k1gFnPc4	kapacita
pro	pro	k7c4	pro
novináře	novinář	k1gMnPc4	novinář
ve	v	k7c6	v
vysokohorském	vysokohorský	k2eAgNnSc6d1	vysokohorské
středisku	středisko	k1gNnSc6	středisko
Krásná	krásný	k2eAgFnSc1d1	krásná
Poljana	Poljana	k1gFnSc1	Poljana
<g/>
.	.	kIx.	.
</s>
<s>
Týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
tří	tři	k4xCgNnPc2	tři
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
kapacity	kapacita	k1gFnSc2	kapacita
41	[number]	k4	41
tisíc	tisíc	k4xCgInPc2	tisíc
pokojů	pokoj	k1gInPc2	pokoj
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zakončení	zakončení	k1gNnSc6	zakončení
her	hra	k1gFnPc2	hra
předseda	předseda	k1gMnSc1	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
Thomas	Thomas	k1gMnSc1	Thomas
Bach	Bach	k1gMnSc1	Bach
mj.	mj.	kA	mj.
zhodnotil	zhodnotit	k5eAaPmAgMnS	zhodnotit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rusko	Rusko	k1gNnSc1	Rusko
slíbilo	slíbit	k5eAaPmAgNnS	slíbit
výborná	výborná	k1gFnSc1	výborná
sportoviště	sportoviště	k1gNnSc2	sportoviště
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgFnSc4d1	vynikající
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
vesnici	vesnice	k1gFnSc4	vesnice
a	a	k8xC	a
bezvadnou	bezvadný	k2eAgFnSc4d1	bezvadná
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
sliby	slib	k1gInPc4	slib
splnilo	splnit	k5eAaPmAgNnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
her	hra	k1gFnPc2	hra
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
média	médium	k1gNnPc4	médium
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
novinář	novinář	k1gMnSc1	novinář
využila	využít	k5eAaPmAgFnS	využít
olympijský	olympijský	k2eAgInSc4d1	olympijský
projekt	projekt	k1gInSc4	projekt
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
cíle	cíl	k1gInPc4	cíl
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
protiruské	protiruský	k2eAgFnSc2d1	protiruská
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
opačného	opačný	k2eAgInSc2d1	opačný
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
než	než	k8xS	než
zamýšleli	zamýšlet	k5eAaImAgMnP	zamýšlet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doping	doping	k1gInSc1	doping
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
her	hra	k1gFnPc2	hra
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
případům	případ	k1gInPc3	případ
porušení	porušení	k1gNnSc1	porušení
dopingových	dopingový	k2eAgNnPc2d1	dopingové
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
her	hra	k1gFnPc2	hra
byli	být	k5eAaImAgMnP	být
vyloučeni	vyloučen	k2eAgMnPc1d1	vyloučen
německá	německý	k2eAgFnSc1d1	německá
biatlonistka	biatlonistka	k1gFnSc1	biatlonistka
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Evi	Evi	k1gMnSc1	Evi
Sachenbacherová-Stehleová	Sachenbacherová-Stehleová	k1gFnSc1	Sachenbacherová-Stehleová
a	a	k8xC	a
italský	italský	k2eAgMnSc1d1	italský
bobista	bobista	k1gMnSc1	bobista
a	a	k8xC	a
dřívější	dřívější	k2eAgMnSc1d1	dřívější
atlet	atlet	k1gMnSc1	atlet
William	William	k1gInSc1	William
Frullani	Frullan	k1gMnPc1	Frullan
<g/>
,	,	kIx,	,
pozitivní	pozitivní	k2eAgMnPc1d1	pozitivní
testy	testa	k1gFnSc2	testa
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
lotyšský	lotyšský	k2eAgMnSc1d1	lotyšský
hokejista	hokejista	k1gMnSc1	hokejista
Vitalijs	Vitalijsa	k1gFnPc2	Vitalijsa
Pavlovs	Pavlovs	k1gInSc1	Pavlovs
<g/>
,	,	kIx,	,
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
běžkyně	běžkyně	k1gFnSc1	běžkyně
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Marina	Marina	k1gFnSc1	Marina
Lisogorová	Lisogorová	k1gFnSc1	Lisogorová
a	a	k8xC	a
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
finále	finále	k1gNnSc7	finále
hokejového	hokejový	k2eAgInSc2d1	hokejový
turnaje	turnaj	k1gInSc2	turnaj
proti	proti	k7c3	proti
Kanadě	Kanada	k1gFnSc3	Kanada
Švéd	Švéd	k1gMnSc1	Švéd
Nicklas	Nicklas	k1gMnSc1	Nicklas
Bäckström	Bäckström	k1gMnSc1	Bäckström
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
WDR	WDR	kA	WDR
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
her	hra	k1gFnPc2	hra
podezření	podezření	k1gNnSc2	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruští	ruský	k2eAgMnPc1d1	ruský
sportovci	sportovec	k1gMnPc1	sportovec
si	se	k3xPyFc3	se
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
spornou	sporný	k2eAgFnSc7d1	sporná
metodou	metoda	k1gFnSc7	metoda
inhalace	inhalace	k1gFnSc2	inhalace
xenonu	xenon	k1gInSc2	xenon
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
organismus	organismus	k1gInSc1	organismus
reaguje	reagovat	k5eAaBmIp3nS	reagovat
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
produkcí	produkce	k1gFnSc7	produkce
erythropoetinu	erythropoetin	k2eAgFnSc4d1	erythropoetin
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2016	[number]	k4	2016
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
Světová	světový	k2eAgFnSc1d1	světová
antidopingová	antidopingový	k2eAgFnSc1d1	antidopingová
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
WADA	WADA	kA	WADA
<g/>
)	)	kIx)	)
svou	svůj	k3xOyFgFnSc4	svůj
vyšetřovací	vyšetřovací	k2eAgFnSc4d1	vyšetřovací
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnPc4	který
ruské	ruský	k2eAgInPc1d1	ruský
státní	státní	k2eAgInPc1d1	státní
orgány	orgán	k1gInPc1	orgán
systematicky	systematicky	k6eAd1	systematicky
kryly	krýt	k5eAaImAgInP	krýt
doping	doping	k1gInSc4	doping
ruských	ruský	k2eAgMnPc2d1	ruský
olympioniků	olympionik	k1gMnPc2	olympionik
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
WADA	WADA	kA	WADA
ruský	ruský	k2eAgInSc1d1	ruský
dopingový	dopingový	k2eAgInSc4d1	dopingový
program	program	k1gInSc4	program
řídilo	řídit	k5eAaImAgNnS	řídit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
sportu	sport	k1gInSc2	sport
vedené	vedený	k2eAgNnSc1d1	vedené
Vitalijem	Vitalij	k1gMnSc7	Vitalij
Mutkem	Mutek	k1gMnSc7	Mutek
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
tajnou	tajný	k2eAgFnSc7d1	tajná
službou	služba	k1gFnSc7	služba
FSB	FSB	kA	FSB
<g/>
,	,	kIx,	,
středisky	středisko	k1gNnPc7	středisko
vrcholového	vrcholový	k2eAgInSc2d1	vrcholový
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
laboratořemi	laboratoř	k1gFnPc7	laboratoř
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
debaklu	debakl	k1gInSc6	debakl
výsledků	výsledek	k1gInPc2	výsledek
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
ruská	ruský	k2eAgFnSc1d1	ruská
výprava	výprava	k1gFnSc1	výprava
přivezla	přivézt	k5eAaPmAgFnS	přivézt
tři	tři	k4xCgInPc4	tři
zlaté	zlatý	k2eAgInPc4d1	zlatý
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
zorganizovala	zorganizovat	k5eAaPmAgFnS	zorganizovat
rozjela	rozjet	k5eAaPmAgFnS	rozjet
tajný	tajný	k2eAgInSc4d1	tajný
program	program	k1gInSc4	program
před	před	k7c7	před
hrami	hra	k1gFnPc7	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
systém	systém	k1gInSc1	systém
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
manipulaci	manipulace	k1gFnSc4	manipulace
se	s	k7c7	s
vzorky	vzorek	k1gInPc7	vzorek
ruských	ruský	k2eAgMnPc2d1	ruský
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
dopingového	dopingový	k2eAgInSc2d1	dopingový
programu	program	k1gInSc2	program
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
20	[number]	k4	20
ruských	ruský	k2eAgMnPc2d1	ruský
olympioniků	olympionik	k1gMnPc2	olympionik
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
včetně	včetně	k7c2	včetně
15	[number]	k4	15
medailistů	medailista	k1gMnPc2	medailista
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
spekulací	spekulace	k1gFnPc2	spekulace
ruští	ruský	k2eAgMnPc1d1	ruský
olympionici	olympionik	k1gMnPc1	olympionik
používali	používat	k5eAaImAgMnP	používat
xenonový	xenonový	k2eAgInSc4d1	xenonový
plyn	plyn	k1gInSc4	plyn
během	během	k7c2	během
příprav	příprava	k1gFnPc2	příprava
na	na	k7c6	na
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
xenonového	xenonový	k2eAgInSc2d1	xenonový
plynu	plyn	k1gInSc2	plyn
není	být	k5eNaImIp3nS	být
přímo	přímo	k6eAd1	přímo
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
Světovou	světový	k2eAgFnSc7d1	světová
antidopingovou	antidopingový	k2eAgFnSc7d1	antidopingová
agenturou	agentura	k1gFnSc7	agentura
(	(	kIx(	(
<g/>
WADA	WADA	kA	WADA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
výkonnosti	výkonnost	k1gFnSc2	výkonnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
debatu	debata	k1gFnSc4	debata
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
dopingem	doping	k1gInSc7	doping
a	a	k8xC	a
jaké	jaký	k3yRgInPc1	jaký
postupy	postup	k1gInPc1	postup
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
a	a	k8xC	a
neměly	mít	k5eNaImAgInP	mít
by	by	kYmCp3nP	by
být	být	k5eAaImF	být
povoleny	povolit	k5eAaPmNgInP	povolit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
listopadu	listopad	k1gInSc3	listopad
2017	[number]	k4	2017
odebral	odebrat	k5eAaPmAgInS	odebrat
MOV	MOV	kA	MOV
Rusku	Ruska	k1gFnSc4	Ruska
kvůli	kvůli	k7c3	kvůli
dopingové	dopingový	k2eAgFnSc3d1	dopingová
aféře	aféra	k1gFnSc3	aféra
14	[number]	k4	14
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
4	[number]	k4	4
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
propadlo	propadnout	k5eAaPmAgNnS	propadnout
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
se	s	k7c7	s
42	[number]	k4	42
ze	z	k7c2	z
43	[number]	k4	43
potrestaných	potrestaný	k2eAgMnPc2d1	potrestaný
sportovců	sportovec	k1gMnPc2	sportovec
odvolalo	odvolat	k5eAaPmAgNnS	odvolat
k	k	k7c3	k
Mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
sportovní	sportovní	k2eAgFnSc3d1	sportovní
arbitráži	arbitráž	k1gFnSc3	arbitráž
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
(	(	kIx(	(
<g/>
CAS	CAS	kA	CAS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
očistila	očistit	k5eAaPmAgFnS	očistit
28	[number]	k4	28
sportovů	sportov	k1gInPc2	sportov
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgMnPc2	dva
olympijských	olympijský	k2eAgMnPc2d1	olympijský
vítězů	vítěz	k1gMnPc2	vítěz
-	-	kIx~	-
běžce	běžec	k1gMnSc2	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Alexandra	Alexandr	k1gMnSc2	Alexandr
Legkova	Legkův	k2eAgMnSc2d1	Legkův
a	a	k8xC	a
skeletonisty	skeletonista	k1gMnSc2	skeletonista
Alexandra	Alexandr	k1gMnSc2	Alexandr
Treťjakova	Treťjakův	k2eAgMnSc2d1	Treťjakův
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tak	tak	k6eAd1	tak
získali	získat	k5eAaPmAgMnP	získat
ruští	ruský	k2eAgMnPc1d1	ruský
sportovci	sportovec	k1gMnPc1	sportovec
zpět	zpět	k6eAd1	zpět
9	[number]	k4	9
ze	z	k7c2	z
13	[number]	k4	13
odebraných	odebraný	k2eAgFnPc2d1	odebraná
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dalších	další	k2eAgMnPc2d1	další
11	[number]	k4	11
sportovců	sportovec	k1gMnPc2	sportovec
včetně	včetně	k7c2	včetně
držitele	držitel	k1gMnSc2	držitel
dvou	dva	k4xCgInPc2	dva
zlatých	zlatý	k2eAgFnPc2d1	zlatá
medailí	medaile	k1gFnPc2	medaile
Aleksandra	Aleksandr	k1gMnSc2	Aleksandr
Zubkova	Zubkov	k1gInSc2	Zubkov
byla	být	k5eAaImAgFnS	být
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
diskvalifikace	diskvalifikace	k1gFnSc1	diskvalifikace
z	z	k7c2	z
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
.	.	kIx.	.
</s>
<s>
Doživotní	doživotní	k2eAgInSc1d1	doživotní
zákaz	zákaz	k1gInSc1	zákaz
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
OH	OH	kA	OH
však	však	k9	však
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
jednorázový	jednorázový	k2eAgInSc4d1	jednorázový
trest	trest	k1gInSc4	trest
pro	pro	k7c4	pro
Zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
2018	[number]	k4	2018
v	v	k7c6	v
Pchjongčchangu	Pchjongčchang	k1gInSc6	Pchjongčchang
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
zbývajících	zbývající	k2eAgFnPc6d1	zbývající
třech	tři	k4xCgFnPc6	tři
sportovkyních	sportovkyně	k1gFnPc6	sportovkyně
-	-	kIx~	-
biatlonistkách	biatlonistka	k1gFnPc6	biatlonistka
a	a	k8xC	a
držitelkách	držitelka	k1gFnPc6	držitelka
dvou	dva	k4xCgInPc2	dva
stříbrných	stříbrná	k1gFnPc2	stříbrná
medailí	medaile	k1gFnPc2	medaile
Olza	Olzus	k1gMnSc2	Olzus
Zaitsevové	Zaitsevová	k1gFnSc2	Zaitsevová
<g/>
,	,	kIx,	,
Olze	Olga	k1gFnSc3	Olga
Viljuchinové	Viljuchinová	k1gFnSc3	Viljuchinová
a	a	k8xC	a
Janě	Jana	k1gFnSc3	Jana
Romanovové	Romanovové	k2eAgFnPc2d1	Romanovové
bude	být	k5eAaImBp3nS	být
CAS	CAS	kA	CAS
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
předchozímu	předchozí	k2eAgNnSc3d1	předchozí
ukončení	ukončení	k1gNnSc3	ukončení
kariéry	kariéra	k1gFnSc2	kariéra
jednat	jednat	k5eAaImF	jednat
až	až	k9	až
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
ZOH	ZOH	kA	ZOH
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Navrácením	navrácení	k1gNnSc7	navrácení
těchto	tento	k3xDgFnPc2	tento
medailí	medaile	k1gFnPc2	medaile
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
dostává	dostávat	k5eAaImIp3nS	dostávat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
oznámili	oznámit	k5eAaPmAgMnP	oznámit
kavkazští	kavkazský	k2eAgMnPc1d1	kavkazský
extremisté	extremista	k1gMnPc1	extremista
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodlají	hodlat	k5eAaImIp3nP	hodlat
hry	hra	k1gFnPc4	hra
narušit	narušit	k5eAaPmF	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrtonosných	smrtonosný	k2eAgInPc6d1	smrtonosný
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
ve	v	k7c6	v
Volgogradu	Volgograd	k1gInSc3	Volgograd
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
přes	přes	k7c4	přes
tři	tři	k4xCgFnPc4	tři
desítky	desítka	k1gFnPc4	desítka
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
teroristé	terorista	k1gMnPc1	terorista
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
opět	opět	k6eAd1	opět
ozvat	ozvat	k5eAaPmF	ozvat
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
dostaly	dostat	k5eAaPmAgInP	dostat
olympijské	olympijský	k2eAgInPc1d1	olympijský
výbory	výbor	k1gInPc1	výbor
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Česka	Česko	k1gNnSc2	Česko
e-mail	eail	k1gInSc1	e-mail
s	s	k7c7	s
teroristickou	teroristický	k2eAgFnSc7d1	teroristická
výhrůžkou	výhrůžka	k1gFnSc7	výhrůžka
napsanou	napsaný	k2eAgFnSc7d1	napsaná
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
ruštině	ruština	k1gFnSc6	ruština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
MOV	MOV	kA	MOV
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
<g/>
)	)	kIx)	)
nepředstavovaly	představovat	k5eNaImAgFnP	představovat
výhrůžky	výhrůžka	k1gFnPc1	výhrůžka
vážnou	vážný	k2eAgFnSc4d1	vážná
reálnou	reálný	k2eAgFnSc4d1	reálná
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
oběžníkový	oběžníkový	k2eAgInSc1d1	oběžníkový
e-mail	eail	k1gInSc1	e-mail
<g/>
.	.	kIx.	.
<g/>
Vedení	vedení	k1gNnSc1	vedení
MOV	MOV	kA	MOV
bere	brát	k5eAaImIp3nS	brát
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
velmi	velmi	k6eAd1	velmi
vážně	vážně	k6eAd1	vážně
a	a	k8xC	a
potvrzovalo	potvrzovat	k5eAaImAgNnS	potvrzovat
schopnost	schopnost	k1gFnSc4	schopnost
Ruska	Rusko	k1gNnSc2	Rusko
poskytnout	poskytnout	k5eAaPmF	poskytnout
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
olympiádu	olympiáda	k1gFnSc4	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
potření	potření	k1gNnSc4	potření
hrozby	hrozba	k1gFnSc2	hrozba
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
islámských	islámský	k2eAgMnPc2d1	islámský
militantů	militant	k1gMnPc2	militant
ze	z	k7c2	z
severního	severní	k2eAgInSc2d1	severní
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
bylo	být	k5eAaImAgNnS	být
prý	prý	k9	prý
postaráno	postarán	k2eAgNnSc1d1	postaráno
na	na	k7c6	na
podobné	podobný	k2eAgFnSc6d1	podobná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c4	po
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opatření	opatření	k1gNnPc1	opatření
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
vyčlenily	vyčlenit	k5eAaPmAgInP	vyčlenit
ruské	ruský	k2eAgInPc1d1	ruský
úřady	úřad	k1gInPc1	úřad
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
policistů	policista	k1gMnPc2	policista
a	a	k8xC	a
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
svého	svůj	k3xOyFgInSc2	svůj
mateřského	mateřský	k2eAgInSc2d1	mateřský
jazyka	jazyk	k1gInSc2	jazyk
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
s	s	k7c7	s
rusky	rusky	k6eAd1	rusky
nemluvícími	mluvící	k2eNgMnPc7d1	nemluvící
diváky	divák	k1gMnPc7	divák
dorozumívat	dorozumívat	k5eAaImF	dorozumívat
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
nebo	nebo	k8xC	nebo
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
vznášely	vznášet	k5eAaImAgInP	vznášet
bezpilotní	bezpilotní	k2eAgInPc4d1	bezpilotní
letouny	letoun	k1gInPc4	letoun
<g/>
,	,	kIx,	,
situaci	situace	k1gFnSc4	situace
monitorovalo	monitorovat	k5eAaImAgNnS	monitorovat
5500	[number]	k4	5500
kamer	kamera	k1gFnPc2	kamera
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
v	v	k7c6	v
pohotovosti	pohotovost	k1gFnSc6	pohotovost
byly	být	k5eAaImAgInP	být
protiletadlové	protiletadlový	k2eAgInPc1d1	protiletadlový
systémy	systém	k1gInPc1	systém
S-400	S-400	k1gFnSc3	S-400
a	a	k8xC	a
Pancir-S	Pancir-S	k1gFnSc6	Pancir-S
<g/>
1	[number]	k4	1
protivzdušné	protivzdušný	k2eAgFnSc2d1	protivzdušná
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
na	na	k7c6	na
Černém	černý	k2eAgNnSc6d1	černé
moři	moře	k1gNnSc6	moře
čtyři	čtyři	k4xCgInPc1	čtyři
protiteroristické	protiteroristický	k2eAgInPc1d1	protiteroristický
dělové	dělový	k2eAgInPc1d1	dělový
čluny	člun	k1gInPc1	člun
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
železniční	železniční	k2eAgNnSc4d1	železniční
nádraží	nádraží	k1gNnSc4	nádraží
a	a	k8xC	a
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
sportoviště	sportoviště	k1gNnSc4	sportoviště
museli	muset	k5eAaImAgMnP	muset
návštěvníci	návštěvník	k1gMnPc1	návštěvník
projít	projít	k5eAaPmF	projít
přes	přes	k7c4	přes
kontrolní	kontrolní	k2eAgInPc4d1	kontrolní
body	bod	k1gInPc4	bod
vybavené	vybavený	k2eAgInPc4d1	vybavený
rentgeny	rentgen	k1gInPc7	rentgen
<g/>
,	,	kIx,	,
detektory	detektor	k1gInPc7	detektor
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
výbušného	výbušný	k2eAgInSc2d1	výbušný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
58	[number]	k4	58
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
ruských	ruský	k2eAgFnPc2d1	ruská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
asi	asi	k9	asi
70	[number]	k4	70
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlídala	hlídat	k5eAaImAgFnS	hlídat
blízké	blízký	k2eAgFnPc4d1	blízká
jižní	jižní	k2eAgFnPc4d1	jižní
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Gruzií	Gruzie	k1gFnSc7	Gruzie
<g/>
.	.	kIx.	.
</s>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
budou	být	k5eAaImBp3nP	být
"	"	kIx"	"
<g/>
nejbezpečnějšími	bezpečný	k2eAgFnPc7d3	nejbezpečnější
olympijskými	olympijský	k2eAgFnPc7d1	olympijská
hrami	hra	k1gFnPc7	hra
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
podepsal	podepsat	k5eAaPmAgMnS	podepsat
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechna	všechen	k3xTgNnPc1	všechen
shromáždění	shromáždění	k1gNnPc1	shromáždění
<g/>
,	,	kIx,	,
manifestace	manifestace	k1gFnPc1	manifestace
<g/>
,	,	kIx,	,
demonstrace	demonstrace	k1gFnPc1	demonstrace
a	a	k8xC	a
pochody	pochod	k1gInPc1	pochod
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
přímo	přímo	k6eAd1	přímo
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
olympijskými	olympijský	k2eAgFnPc7d1	olympijská
a	a	k8xC	a
paralympijskými	paralympijský	k2eAgFnPc7d1	paralympijská
hrami	hra	k1gFnPc7	hra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
konat	konat	k5eAaImF	konat
výhradně	výhradně	k6eAd1	výhradně
po	po	k7c6	po
souhlasu	souhlas	k1gInSc6	souhlas
místních	místní	k2eAgInPc2d1	místní
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Zóna	zóna	k1gFnSc1	zóna
pro	pro	k7c4	pro
demonstrace	demonstrace	k1gFnPc4	demonstrace
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
v	v	k7c6	v
parku	park	k1gInSc6	park
ve	v	k7c6	v
dvacetitisícovém	dvacetitisícový	k2eAgNnSc6d1	dvacetitisícové
městě	město	k1gNnSc6	město
Chosta	Chosta	k1gFnSc1	Chosta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
sedmnáct	sedmnáct	k4xCc4	sedmnáct
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Soči	Soči	k1gNnSc2	Soči
a	a	k8xC	a
dvanáct	dvanáct	k4xCc4	dvanáct
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
sportoviště	sportoviště	k1gNnSc2	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
však	však	k9	však
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
zde	zde	k6eAd1	zde
pořádané	pořádaný	k2eAgInPc4d1	pořádaný
protesty	protest	k1gInPc4	protest
musela	muset	k5eAaImAgFnS	muset
schválit	schválit	k5eAaPmF	schválit
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
místní	místní	k2eAgInPc4d1	místní
úřady	úřad	k1gInPc4	úřad
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
znění	znění	k1gNnSc1	znění
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
veškeré	veškerý	k3xTgInPc1	veškerý
protesty	protest	k1gInPc1	protest
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
olympiády	olympiáda	k1gFnSc2	olympiáda
zakazovalo	zakazovat	k5eAaImAgNnS	zakazovat
<g/>
,	,	kIx,	,
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
obhájci	obhájce	k1gMnPc1	obhájce
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
jako	jako	k8xS	jako
neústavní	ústavní	k2eNgMnSc1d1	neústavní
<g/>
.	.	kIx.	.
<g/>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
ruskému	ruský	k2eAgMnSc3d1	ruský
prezidentu	prezident	k1gMnSc3	prezident
Putinovi	Putin	k1gMnSc3	Putin
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Pentagon	Pentagon	k1gInSc4	Pentagon
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
dvě	dva	k4xCgFnPc4	dva
lodě	loď	k1gFnPc4	loď
v	v	k7c6	v
pohotovosti	pohotovost	k1gFnSc6	pohotovost
v	v	k7c6	v
Černém	černý	k2eAgNnSc6d1	černé
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
2014	[number]	k4	2014
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sochi	Sochi	k6eAd1	Sochi
<g/>
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
Olympic	Olympic	k1gMnSc1	Olympic
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Český	český	k2eAgInSc1d1	český
olympijský	olympijský	k2eAgInSc1d1	olympijský
tým	tým	k1gInSc1	tým
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
Olympijských	olympijský	k2eAgInPc2d1	olympijský
stadionů	stadion	k1gInPc2	stadion
a	a	k8xC	a
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
zahajovací	zahajovací	k2eAgInSc1d1	zahajovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
ZOH	ZOH	kA	ZOH
2014	[number]	k4	2014
–	–	k?	–
Youtube	Youtub	k1gInSc5	Youtub
video	video	k1gNnSc4	video
</s>
</p>
