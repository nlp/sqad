<p>
<s>
Potápky	potápka	k1gFnPc1	potápka
(	(	kIx(	(
<g/>
Podicipediformes	Podicipediformes	k1gInSc1	Podicipediformes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řád	řád	k1gInSc1	řád
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jedinou	jediný	k2eAgFnSc4d1	jediná
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
,	,	kIx,	,
potápkovití	potápkovitý	k2eAgMnPc1d1	potápkovitý
<g/>
,	,	kIx,	,
s	s	k7c7	s
20	[number]	k4	20
žijícími	žijící	k2eAgInPc7d1	žijící
druhy	druh	k1gInPc7	druh
v	v	k7c6	v
šesti	šest	k4xCc2	šest
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
kostrční	kostrční	k2eAgFnSc4d1	kostrční
žlázu	žláza	k1gFnSc4	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k9	až
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Potápky	potápka	k1gFnPc1	potápka
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
velikosti	velikost	k1gFnSc3	velikost
od	od	k7c2	od
nejmenších	malý	k2eAgInPc2d3	nejmenší
27	[number]	k4	27
cm	cm	kA	cm
až	až	k9	až
po	po	k7c6	po
největší	veliký	k2eAgFnSc6d3	veliký
48	[number]	k4	48
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
krátký	krátký	k2eAgInSc4d1	krátký
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
krk	krk	k1gInSc4	krk
a	a	k8xC	a
kratičký	kratičký	k2eAgInSc4d1	kratičký
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Prsty	prst	k1gInPc1	prst
mají	mít	k5eAaImIp3nP	mít
opatřené	opatřený	k2eAgInPc1d1	opatřený
plovacími	plovací	k2eAgInPc7d1	plovací
lemy	lem	k1gInPc7	lem
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
posunuty	posunout	k5eAaPmNgFnP	posunout
dozadu	dozadu	k6eAd1	dozadu
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ptáci	pták	k1gMnPc1	pták
prakticky	prakticky	k6eAd1	prakticky
nemohou	moct	k5eNaImIp3nP	moct
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
vypadají	vypadat	k5eAaImIp3nP	vypadat
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
==	==	k?	==
</s>
</p>
<p>
<s>
Řadu	řada	k1gFnSc4	řada
let	let	k1gInSc1	let
byly	být	k5eAaImAgFnP	být
potápky	potápka	k1gFnPc1	potápka
považované	považovaný	k2eAgFnPc1d1	považovaná
za	za	k7c4	za
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
skupinu	skupina	k1gFnSc4	skupina
potáplic	potáplice	k1gFnPc2	potáplice
(	(	kIx(	(
<g/>
Gaviiformes	Gaviiformes	k1gInSc1	Gaviiformes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
výzkumy	výzkum	k1gInPc1	výzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
konvergentní	konvergentní	k2eAgInSc4d1	konvergentní
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Sesterskou	sesterský	k2eAgFnSc7d1	sesterská
skupinou	skupina	k1gFnSc7	skupina
potápek	potápka	k1gFnPc2	potápka
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
plameňáci	plameňák	k1gMnPc1	plameňák
(	(	kIx(	(
<g/>
Phoenicopteriformes	Phoenicopteriformes	k1gMnSc1	Phoenicopteriformes
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
řada	řada	k1gFnSc1	řada
společných	společný	k2eAgInPc2d1	společný
znaků	znak	k1gInPc2	znak
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
nejméně	málo	k6eAd3	málo
23	[number]	k4	23
presakrálních	presakrální	k2eAgInPc2d1	presakrální
obratlů	obratel	k1gInPc2	obratel
(	(	kIx(	(
<g/>
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
ptáků	pták	k1gMnPc2	pták
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
notarium	notarium	k1gNnSc1	notarium
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
srůstem	srůst	k1gInSc7	srůst
čtyř	čtyři	k4xCgFnPc2	čtyři
nebo	nebo	k8xC	nebo
pěti	pět	k4xCc2	pět
hrudních	hrudní	k2eAgInPc2d1	hrudní
obratlů	obratel	k1gInPc2	obratel
</s>
</p>
<p>
<s>
11	[number]	k4	11
ručních	ruční	k2eAgFnPc2d1	ruční
letek	letka	k1gFnPc2	letka
(	(	kIx(	(
<g/>
jediná	jediný	k2eAgFnSc1d1	jediná
další	další	k2eAgFnSc1d1	další
skupina	skupina	k1gFnSc1	skupina
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
počtem	počet	k1gInSc7	počet
jsou	být	k5eAaImIp3nP	být
čápovití	čápovitý	k2eAgMnPc1d1	čápovitý
(	(	kIx(	(
<g/>
Ciconiidae	Ciconiidae	k1gNnSc7	Ciconiidae
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
potravu	potrava	k1gFnSc4	potrava
se	se	k3xPyFc4	se
potápějí	potápět	k5eAaImIp3nP	potápět
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
loví	lovit	k5eAaImIp3nP	lovit
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
měkkýše	měkkýš	k1gMnPc4	měkkýš
a	a	k8xC	a
korýše	korýš	k1gMnPc4	korýš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mláďata	mládě	k1gNnPc4	mládě
==	==	k?	==
</s>
</p>
<p>
<s>
Potápky	potápka	k1gFnPc1	potápka
si	se	k3xPyFc3	se
budují	budovat	k5eAaImIp3nP	budovat
své	svůj	k3xOyFgNnSc4	svůj
hnízdo	hnízdo	k1gNnSc4	hnízdo
z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
plošina	plošina	k1gFnSc1	plošina
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
či	či	k8xC	či
chuchvalec	chuchvalec	k1gInSc4	chuchvalec
ukotvený	ukotvený	k2eAgInSc4d1	ukotvený
vodními	vodní	k2eAgFnPc7d1	vodní
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
rákosí	rákosí	k1gNnSc6	rákosí
<g/>
,	,	kIx,	,
na	na	k7c6	na
větších	veliký	k2eAgNnPc6d2	veliký
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
zatopených	zatopený	k2eAgInPc6d1	zatopený
lomech	lom	k1gInPc6	lom
i	i	k8xC	i
umělých	umělý	k2eAgFnPc6d1	umělá
vodních	vodní	k2eAgFnPc6d1	vodní
nádržích	nádrž	k1gFnPc6	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
párech	pár	k1gInPc6	pár
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
potápka	potápka	k1gMnSc1	potápka
černokrká	černokrkat	k5eAaPmIp3nS	černokrkat
<g/>
)	)	kIx)	)
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnPc1	samice
snáší	snášet	k5eAaImIp3nP	snášet
jednou	jednou	k6eAd1	jednou
či	či	k8xC	či
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
<g/>
)	)	kIx)	)
do	do	k7c2	do
roka	rok	k1gInSc2	rok
4-6	[number]	k4	4-6
bílých	bílý	k2eAgNnPc2d1	bílé
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
krmí	krmit	k5eAaImIp3nP	krmit
nejčastěji	často	k6eAd3	často
rybami	ryba	k1gFnPc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
často	často	k6eAd1	často
polykají	polykat	k5eAaImIp3nP	polykat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
peří	peří	k1gNnSc4	peří
a	a	k8xC	a
pak	pak	k6eAd1	pak
jím	on	k3xPp3gInSc7	on
krmí	krmit	k5eAaImIp3nS	krmit
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnPc3	který
tím	ten	k3xDgNnSc7	ten
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
trávení	trávení	k1gNnSc6	trávení
<g/>
,	,	kIx,	,
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
je	on	k3xPp3gInPc4	on
cizopasníků	cizopasník	k1gMnPc2	cizopasník
v	v	k7c6	v
žaludku	žaludek	k1gInSc6	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
potáplic	potáplice	k1gFnPc2	potáplice
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
vozí	vozit	k5eAaImIp3nP	vozit
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Podiceps	Podicepsa	k1gFnPc2	Podicepsa
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
Podiceps	Podiceps	k1gInSc4	Podiceps
gallardoi	gallardo	k1gMnPc7	gallardo
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
černokrká	černokrkat	k5eAaPmIp3nS	černokrkat
Podiceps	Podiceps	k1gInSc4	Podiceps
nigricollis	nigricollis	k1gFnSc2	nigricollis
</s>
</p>
<p>
<s>
potápka	potápka	k1gMnSc1	potápka
roháč	roháč	k1gMnSc1	roháč
Podiceps	Podicepsa	k1gFnPc2	Podicepsa
cristatus	cristatus	k1gMnSc1	cristatus
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
rudokrká	rudokrkat	k5eAaPmIp3nS	rudokrkat
Podiceps	Podiceps	k1gInSc4	Podiceps
griseigena	griseigeno	k1gNnSc2	griseigeno
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
stříbřitá	stříbřitý	k2eAgFnSc1d1	stříbřitá
Podiceps	Podiceps	k1gInSc4	Podiceps
occipitalis	occipitalis	k1gFnSc5	occipitalis
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
Taczanovského	Taczanovský	k2eAgInSc2d1	Taczanovský
Podiceps	Podicepsa	k1gFnPc2	Podicepsa
taczanowskii	taczanowskie	k1gFnSc4	taczanowskie
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
velká	velká	k1gFnSc1	velká
Podiceps	Podiceps	k1gInSc1	Podiceps
major	major	k1gMnSc1	major
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
žlutorohá	žlutorohat	k5eAaImIp3nS	žlutorohat
Podiceps	Podiceps	k1gInSc4	Podiceps
auritus	auritus	k1gInSc4	auritus
</s>
</p>
<p>
<s>
Podiceps	Podiceps	k6eAd1	Podiceps
andinus	andinus	k1gInSc1	andinus
–	–	k?	–
vyhynulá	vyhynulý	k2eAgFnSc5d1	vyhynulá
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Aechmophorus	Aechmophorus	k1gInSc1	Aechmophorus
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
Clarkova	Clarkův	k2eAgFnSc1d1	Clarkova
Aechmophorus	Aechmophorus	k1gInSc4	Aechmophorus
clarkii	clarkie	k1gFnSc3	clarkie
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	(
<g/>
potápka	potápka	k1gFnSc1	potápka
velká	velká	k1gFnSc1	velká
<g/>
)	)	kIx)	)
Aechmophorus	Aechmophorus	k1gInSc1	Aechmophorus
occidentalis	occidentalis	k1gFnSc2	occidentalis
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Podilymbus	Podilymbus	k1gInSc1	Podilymbus
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
šedá	šedá	k1gFnSc1	šedá
(	(	kIx(	(
<g/>
potápka	potápka	k1gFnSc1	potápka
americká	americký	k2eAgFnSc1d1	americká
<g/>
)	)	kIx)	)
Podilymbus	Podilymbus	k1gMnSc1	Podilymbus
podiceps	podiceps	k6eAd1	podiceps
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
obrovská	obrovský	k2eAgFnSc1d1	obrovská
Podilymbus	Podilymbus	k1gInSc4	Podilymbus
gigas	gigas	k1gInSc1	gigas
–	–	k?	–
vyhynulá	vyhynulý	k2eAgFnSc5d1	vyhynulá
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Poliocephalus	Poliocephalus	k1gInSc1	Poliocephalus
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
šedohlavá	šedohlavý	k2eAgFnSc1d1	šedohlavá
Poliocephalus	Poliocephalus	k1gInSc4	Poliocephalus
poliocephalus	poliocephalus	k1gMnSc1	poliocephalus
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
Poliocephalus	Poliocephalus	k1gInSc4	Poliocephalus
rufopectus	rufopectus	k1gMnSc1	rufopectus
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Rollandia	Rollandium	k1gNnSc2	Rollandium
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
krátkokřídlá	krátkokřídlý	k2eAgFnSc1d1	krátkokřídlý
Rollandia	Rollandium	k1gNnPc4	Rollandium
microptera	micropter	k1gMnSc4	micropter
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
Rollandova	Rollandův	k2eAgNnSc2d1	Rollandův
Rollandia	Rollandium	k1gNnSc2	Rollandium
rolland	rollanda	k1gFnPc2	rollanda
</s>
</p>
<p>
<s>
rod	rod	k1gInSc1	rod
Tachybaptus	Tachybaptus	k1gInSc1	Tachybaptus
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
australská	australský	k2eAgFnSc1d1	australská
(	(	kIx(	(
<g/>
potápka	potápka	k1gFnSc1	potápka
australasijská	australasijský	k2eAgFnSc1d1	australasijský
<g/>
)	)	kIx)	)
Tachybaptus	Tachybaptus	k1gMnSc1	Tachybaptus
novaehollandiae	novaehollandiaat	k5eAaPmIp3nS	novaehollandiaat
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
madagaskarská	madagaskarský	k2eAgFnSc1d1	madagaskarská
Tachybaptus	Tachybaptus	k1gInSc4	Tachybaptus
pelzelnii	pelzelnie	k1gFnSc6	pelzelnie
</s>
</p>
<p>
<s>
potápka	potápka	k1gFnSc1	potápka
malá	malý	k2eAgFnSc1d1	malá
Tachybaptus	Tachybaptus	k1gInSc4	Tachybaptus
ruficollis	ruficollis	k1gFnPc4	ruficollis
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
řazena	řazen	k2eAgFnSc1d1	řazena
k	k	k7c3	k
rodu	rod	k1gInSc3	rod
Podiceps	Podicepsa	k1gFnPc2	Podicepsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
potápka	potápka	k1gMnSc1	potápka
nejmenší	malý	k2eAgMnSc1d3	nejmenší
Tachybaptus	Tachybaptus	k1gMnSc1	Tachybaptus
dominicus	dominicus	k1gMnSc1	dominicus
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Podiceps	Podicepsa	k1gFnPc2	Podicepsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
potápka	potápka	k1gMnSc1	potápka
skořicovohrdlá	skořicovohrdlat	k5eAaImIp3nS	skořicovohrdlat
Tachybaptus	Tachybaptus	k1gMnSc1	Tachybaptus
rufolavatus	rufolavatus	k1gMnSc1	rufolavatus
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
