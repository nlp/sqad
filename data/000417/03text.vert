<s>
Zeus	Zeus	k1gInSc1	Zeus
(	(	kIx(	(
<g/>
starověký	starověký	k2eAgMnSc1d1	starověký
řecký	řecký	k2eAgMnSc1d1	řecký
Ζ	Ζ	k?	Ζ
[	[	kIx(	[
<g/>
Zeús	Zeús	k1gInSc1	Zeús
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
moderní	moderní	k2eAgMnSc1d1	moderní
řecký	řecký	k2eAgMnSc1d1	řecký
Δ	Δ	k?	Δ
[	[	kIx(	[
<g/>
Dias	Dias	k1gInSc1	Dias
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
skloňování	skloňování	k1gNnSc1	skloňování
Dia	Dia	k1gFnSc2	Dia
<g/>
,	,	kIx,	,
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
,	,	kIx,	,
Die	Die	k1gMnSc1	Die
<g/>
,	,	kIx,	,
Diem	Diem	k1gMnSc1	Diem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
z	z	k7c2	z
bohů	bůh	k1gMnPc2	bůh
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k6eAd1	Zeus
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
indoevropské	indoevropský	k2eAgNnSc4d1	indoevropské
božstvo	božstvo	k1gNnSc4	božstvo
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
indoevropského	indoevropský	k2eAgInSc2d1	indoevropský
kořene	kořen	k1gInSc2	kořen
Djaus	Djaus	k1gMnSc1	Djaus
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
paralelu	paralela	k1gFnSc4	paralela
ve	v	k7c6	v
jménech	jméno	k1gNnPc6	jméno
Deus	Deusa	k1gFnPc2	Deusa
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
(	(	kIx(	(
<g/>
řím	řím	k?	řím
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zeus	Zeus	k1gInSc1	Zeus
pater	patro	k1gNnPc2	patro
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Djaus	Djaus	k1gMnSc1	Djaus
Pitar	Pitar	k1gMnSc1	Pitar
(	(	kIx(	(
<g/>
védský	védský	k2eAgMnSc1d1	védský
Nebeský	nebeský	k2eAgMnSc1d1	nebeský
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tyr	Tyr	k1gFnSc1	Tyr
(	(	kIx(	(
<g/>
germ.	germ.	k?	germ.
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Zeus	Zeus	k1gInSc1	Zeus
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
bohem	bůh	k1gMnSc7	bůh
Olympského	olympský	k2eAgInSc2d1	olympský
panteonu	panteon	k1gInSc2	panteon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
etymologicky	etymologicky	k6eAd1	etymologicky
jasný	jasný	k2eAgInSc4d1	jasný
indoevropský	indoevropský	k2eAgInSc4d1	indoevropský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Nenacházíme	nacházet	k5eNaImIp1nP	nacházet
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
ovšem	ovšem	k9	ovšem
atribut	atribut	k1gInSc4	atribut
ztracené	ztracený	k2eAgFnSc2d1	ztracená
ruky	ruka	k1gFnSc2	ruka
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
u	u	k7c2	u
jeho	jeho	k3xOp3gFnSc2	jeho
germánské	germánský	k2eAgFnSc2d1	germánská
podoby	podoba	k1gFnSc2	podoba
Tyra	Tyr	k1gInSc2	Tyr
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
frýžského	frýžský	k2eAgMnSc4d1	frýžský
boha	bůh	k1gMnSc4	bůh
Sabazia	Sabazius	k1gMnSc4	Sabazius
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
ztotožněn	ztotožněn	k2eAgMnSc1d1	ztotožněn
a	a	k8xC	a
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
zobrazován	zobrazovat	k5eAaImNgMnS	zobrazovat
s	s	k7c7	s
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
zdobenou	zdobený	k2eAgFnSc7d1	zdobená
symboly	symbol	k1gInPc7	symbol
dobrotivosti	dobrotivost	k1gFnSc2	dobrotivost
a	a	k8xC	a
laskavosti	laskavost	k1gFnSc2	laskavost
<g/>
,	,	kIx,	,
a	a	k8xC	a
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
jemu	on	k3xPp3gMnSc3	on
věnovaných	věnovaný	k2eAgFnPc6d1	věnovaná
oslavách	oslava	k1gFnPc6	oslava
představován	představován	k2eAgInSc1d1	představován
mužem	muž	k1gMnSc7	muž
jedoucím	jedoucí	k2eAgMnSc7d1	jedoucí
na	na	k7c6	na
voze	vůz	k1gInSc6	vůz
s	s	k7c7	s
pravým	pravý	k2eAgNnSc7d1	pravé
předloktím	předloktí	k1gNnSc7	předloktí
zakrytým	zakrytý	k2eAgNnSc7d1	zakryté
v	v	k7c6	v
hávu	háv	k1gInSc6	háv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
védách	véda	k1gFnPc6	véda
se	se	k3xPyFc4	se
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Djaus	Djaus	k1gMnSc1	Djaus
Pitar	Pitar	k1gMnSc1	Pitar
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
původně	původně	k6eAd1	původně
nejstarší	starý	k2eAgNnSc4d3	nejstarší
božstvo	božstvo	k1gNnSc4	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
indoevropských	indoevropský	k2eAgNnPc6d1	indoevropské
náboženstvích	náboženství	k1gNnPc6	náboženství
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
buď	buď	k8xC	buď
zastíněn	zastíněn	k2eAgInSc1d1	zastíněn
původně	původně	k6eAd1	původně
nižšími	nízký	k2eAgNnPc7d2	nižší
božstvy	božstvo	k1gNnPc7	božstvo
(	(	kIx(	(
<g/>
Odinem	Odin	k1gInSc7	Odin
u	u	k7c2	u
severních	severní	k2eAgMnPc2d1	severní
germánských	germánský	k2eAgMnPc2d1	germánský
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Indrou	Indra	k1gMnSc7	Indra
u	u	k7c2	u
Indů	Indus	k1gInPc2	Indus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
udržel	udržet	k5eAaPmAgMnS	udržet
vůdčí	vůdčí	k2eAgNnSc4d1	vůdčí
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
trojici	trojice	k1gFnSc6	trojice
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
božstev	božstvo	k1gNnPc2	božstvo
(	(	kIx(	(
<g/>
keltský	keltský	k2eAgInSc1d1	keltský
Teutatis	Teutatis	k1gInSc1	Teutatis
<g/>
,	,	kIx,	,
baltský	baltský	k2eAgInSc1d1	baltský
Dievs	Dievs	k1gInSc1	Dievs
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
řecký	řecký	k2eAgInSc1d1	řecký
Zeus	Zeus	k1gInSc1	Zeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovanském	slovanský	k2eAgInSc6d1	slovanský
panteonu	panteon	k1gInSc6	panteon
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
projevil	projevit	k5eAaPmAgMnS	projevit
jako	jako	k9	jako
Svarog	Svarog	k1gMnSc1	Svarog
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
jehož	jenž	k3xRgNnSc2	jenž
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
indoevropské	indoevropský	k2eAgInPc4d1	indoevropský
svar	svar	k1gInSc4	svar
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc4	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
se	s	k7c7	s
sanskrtským	sanskrtský	k2eAgInSc7d1	sanskrtský
výrazem	výraz	k1gInSc7	výraz
svarga	svarg	k1gMnSc2	svarg
(	(	kIx(	(
<g/>
nebe	nebe	k1gNnSc2	nebe
<g/>
)	)	kIx)	)
a	a	k8xC	a
íránským	íránský	k2eAgMnSc7d1	íránský
hvar	hvar	k1gInSc1	hvar
(	(	kIx(	(
<g/>
slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k1gInSc1	Zeus
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
relativně	relativně	k6eAd1	relativně
nedůležitý	důležitý	k2eNgMnSc1d1	nedůležitý
indoevropský	indoevropský	k2eAgMnSc1d1	indoevropský
bůh	bůh	k1gMnSc1	bůh
jasného	jasný	k2eAgNnSc2d1	jasné
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
splynul	splynout	k5eAaPmAgInS	splynout
s	s	k7c7	s
několika	několik	k4yIc7	několik
jinými	jiný	k2eAgMnPc7d1	jiný
bohy	bůh	k1gMnPc7	bůh
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
bohem	bůh	k1gMnSc7	bůh
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
Krétu	Kréta	k1gFnSc4	Kréta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
ochráncem	ochránce	k1gMnSc7	ochránce
mykénských	mykénský	k2eAgMnPc2d1	mykénský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
vyplynula	vyplynout	k5eAaPmAgFnS	vyplynout
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
původní	původní	k2eAgFnSc2d1	původní
funkce	funkce	k1gFnSc2	funkce
boha	bůh	k1gMnSc2	bůh
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
jasné	jasný	k2eAgFnSc2d1	jasná
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
námořně	námořně	k6eAd1	námořně
založenou	založený	k2eAgFnSc4d1	založená
Krétu	Kréta	k1gFnSc4	Kréta
mělo	mít	k5eAaImAgNnS	mít
počasí	počasí	k1gNnSc4	počasí
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ochráncem	ochránce	k1gMnSc7	ochránce
řeckých	řecký	k2eAgMnPc2d1	řecký
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
bůh	bůh	k1gMnSc1	bůh
počasí	počasí	k1gNnSc4	počasí
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
bohem	bůh	k1gMnSc7	bůh
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
,	,	kIx,	,
nejpravděpodobnější	pravděpodobný	k2eAgFnSc1d3	nejpravděpodobnější
teorie	teorie	k1gFnSc1	teorie
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
proměnlivosti	proměnlivost	k1gFnSc3	proměnlivost
počasí	počasí	k1gNnSc1	počasí
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
se	se	k3xPyFc4	se
Řekové	Řek	k1gMnPc1	Řek
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládce	vládce	k1gMnSc1	vládce
počasí	počasí	k1gNnSc2	počasí
vládne	vládnout	k5eAaImIp3nS	vládnout
i	i	k9	i
horám	hora	k1gFnPc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
ho	on	k3xPp3gMnSc4	on
Řekové	Řek	k1gMnPc1	Řek
začali	začít	k5eAaPmAgMnP	začít
umisťovat	umisťovat	k5eAaImF	umisťovat
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
Olymp	Olymp	k1gInSc1	Olymp
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgMnSc2	tento
boha	bůh	k1gMnSc2	bůh
začal	začít	k5eAaPmAgMnS	začít
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
lokálně	lokálně	k6eAd1	lokálně
<g/>
,	,	kIx,	,
stávat	stávat	k5eAaImF	stávat
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přerod	přerod	k1gInSc1	přerod
byl	být	k5eAaImAgInS	být
patrně	patrně	k6eAd1	patrně
nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
a	a	k8xC	a
nejrychlejší	rychlý	k2eAgMnSc1d3	nejrychlejší
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Smyrny	Smyrna	k1gFnSc2	Smyrna
(	(	kIx(	(
<g/>
Izmir	Izmir	k1gInSc1	Izmir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
dynamický	dynamický	k2eAgInSc1d1	dynamický
a	a	k8xC	a
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Homéra	Homér	k1gMnSc4	Homér
(	(	kIx(	(
<g/>
cca	cca	kA	cca
750	[number]	k4	750
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
dokončený	dokončený	k2eAgInSc1d1	dokončený
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
Homér	Homér	k1gMnSc1	Homér
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Hésiodos	Hésiodosa	k1gFnPc2	Hésiodosa
dali	dát	k5eAaPmAgMnP	dát
tomuto	tento	k3xDgMnSc3	tento
bohu	bůh	k1gMnSc3	bůh
základní	základní	k2eAgFnPc4d1	základní
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Hésiodos	Hésiodos	k1gMnSc1	Hésiodos
psal	psát	k5eAaImAgMnS	psát
Zrození	zrození	k1gNnPc4	zrození
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
již	již	k9	již
Zeus	Zeus	k1gInSc1	Zeus
všeobecně	všeobecně	k6eAd1	všeobecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
alternativu	alternativa	k1gFnSc4	alternativa
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Homéra	Homér	k1gMnSc2	Homér
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
silně	silně	k6eAd1	silně
patrný	patrný	k2eAgInSc1d1	patrný
Diův	Diův	k2eAgInSc1d1	Diův
vznik	vznik	k1gInSc1	vznik
z	z	k7c2	z
boha	bůh	k1gMnSc2	bůh
počasí	počasí	k1gNnSc2	počasí
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
takový	takový	k3xDgInSc4	takový
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
prchlivý	prchlivý	k2eAgInSc1d1	prchlivý
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
snadno	snadno	k6eAd1	snadno
oklamatelný	oklamatelný	k2eAgMnSc1d1	oklamatelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Iliadě	Iliada	k1gFnSc6	Iliada
se	se	k3xPyFc4	se
však	však	k9	však
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
nová	nový	k2eAgFnSc1d1	nová
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
stává	stávat	k5eAaImIp3nS	stávat
ochráncem	ochránce	k1gMnSc7	ochránce
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zesílena	zesílit	k5eAaPmNgFnS	zesílit
Hésiodem	Hésiod	k1gInSc7	Hésiod
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
evidentně	evidentně	k6eAd1	evidentně
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
primární	primární	k2eAgNnSc4d1	primární
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
jeho	jeho	k3xOp3gInSc1	jeho
přerod	přerod	k1gInSc1	přerod
v	v	k7c4	v
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
vlastnosti	vlastnost	k1gFnSc2	vlastnost
boha	bůh	k1gMnSc2	bůh
počasí	počasí	k1gNnSc2	počasí
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ho	on	k3xPp3gNnSc4	on
ještě	ještě	k6eAd1	ještě
Aischylos	Aischylos	k1gMnSc1	Aischylos
v	v	k7c6	v
Prosebnicích	prosebnice	k1gFnPc6	prosebnice
(	(	kIx(	(
<g/>
Prosebnice	prosebnice	k1gFnSc1	prosebnice
<g/>
,	,	kIx,	,
Hiketides	Hiketides	k1gInSc1	Hiketides
<g/>
,	,	kIx,	,
psáno	psán	k2eAgNnSc1d1	psáno
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Marathonu	Marathon	k1gInSc2	Marathon
-	-	kIx~	-
490	[number]	k4	490
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
Upoutaném	upoutaný	k2eAgMnSc6d1	upoutaný
Prométheovi	Prométheus	k1gMnSc6	Prométheus
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
boha	bůh	k1gMnSc4	bůh
značně	značně	k6eAd1	značně
rozporuplného	rozporuplný	k2eAgNnSc2d1	rozporuplné
a	a	k8xC	a
stále	stále	k6eAd1	stále
nezpochybňuje	zpochybňovat	k5eNaImIp3nS	zpochybňovat
jeho	jeho	k3xOp3gFnSc1	jeho
některá	některý	k3yIgNnPc4	některý
primitivní	primitivní	k2eAgNnPc4d1	primitivní
jednání	jednání	k1gNnPc4	jednání
či	či	k8xC	či
negativní	negativní	k2eAgFnPc4d1	negativní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
právě	právě	k9	právě
z	z	k7c2	z
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
bohova	bohův	k2eAgInSc2d1	bohův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
Xenofanea	Xenofaneus	k1gMnSc2	Xenofaneus
objevuje	objevovat	k5eAaImIp3nS	objevovat
první	první	k4xOgMnSc1	první
významnější	významný	k2eAgInSc1d2	významnější
pokus	pokus	k1gInSc1	pokus
proměnit	proměnit	k5eAaPmF	proměnit
Dia	Dia	k1gMnSc4	Dia
v	v	k7c4	v
boha	bůh	k1gMnSc4	bůh
bez	bez	k7c2	bez
těchto	tento	k3xDgFnPc2	tento
negativních	negativní	k2eAgFnPc2d1	negativní
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
a	a	k8xC	a
především	především	k6eAd1	především
pozdější	pozdní	k2eAgFnSc1d2	pozdější
polemika	polemika	k1gFnSc1	polemika
o	o	k7c6	o
příliš	příliš	k6eAd1	příliš
lidských	lidský	k2eAgFnPc6d1	lidská
vlastnostech	vlastnost	k1gFnPc6	vlastnost
Dia	Dia	k1gFnSc1	Dia
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
pomalu	pomalu	k6eAd1	pomalu
stával	stávat	k5eAaImAgMnS	stávat
všemocný	všemocný	k2eAgMnSc1d1	všemocný
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
někteří	některý	k3yIgMnPc1	některý
filosofové	filosof	k1gMnPc1	filosof
naznačovali	naznačovat	k5eAaImAgMnP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
jediného	jediný	k2eAgMnSc4d1	jediný
boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
že	že	k8xS	že
ostatní	ostatní	k2eAgMnPc1d1	ostatní
bohové	bůh	k1gMnPc1	bůh
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
jeho	jeho	k3xOp3gNnSc4	jeho
vtělení	vtělení	k1gNnSc4	vtělení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
pomocníci	pomocník	k1gMnPc1	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
vývoj	vývoj	k1gInSc4	vývoj
by	by	kYmCp3nS	by
nepochybně	pochybně	k6eNd1	pochybně
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
monoteismu	monoteismus	k1gInSc3	monoteismus
a	a	k8xC	a
k	k	k7c3	k
představě	představa	k1gFnSc3	představa
jakéhosi	jakýsi	k3yIgNnSc2	jakýsi
universálního	universální	k2eAgNnSc2d1	universální
<g/>
,	,	kIx,	,
dokonalého	dokonalý	k2eAgMnSc2d1	dokonalý
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
byl	být	k5eAaImAgInS	být
nastolen	nastolit	k5eAaPmNgInS	nastolit
několika	několik	k4yIc7	několik
významnými	významný	k2eAgMnPc7d1	významný
stoiky	stoik	k1gMnPc7	stoik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
myšlenka	myšlenka	k1gFnSc1	myšlenka
rovnosti	rovnost	k1gFnSc2	rovnost
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
podmínek	podmínka	k1gFnPc2	podmínka
vytvoření	vytvoření	k1gNnSc2	vytvoření
monoteismu	monoteismus	k1gInSc6	monoteismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
představy	představa	k1gFnPc1	představa
byly	být	k5eAaImAgFnP	být
nepochybně	pochybně	k6eNd1	pochybně
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
stylem	styl	k1gInSc7	styl
vlády	vláda	k1gFnSc2	vláda
Alexandra	Alexandr	k1gMnSc2	Alexandr
Makedonského	makedonský	k2eAgMnSc2d1	makedonský
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
další	další	k2eAgInSc1d1	další
rozvoj	rozvoj	k1gInSc1	rozvoj
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gMnSc2	Dia
nebyl	být	k5eNaImAgInS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ten	ten	k3xDgMnSc1	ten
již	již	k6eAd1	již
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
začínal	začínat	k5eAaImAgMnS	začínat
splývat	splývat	k5eAaImF	splývat
s	s	k7c7	s
římským	římský	k2eAgMnSc7d1	římský
bohem	bůh	k1gMnSc7	bůh
Jovem	Jovem	k?	Jovem
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
Dia	Dia	k1gFnSc2	Dia
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vývoje	vývoj	k1gInSc2	vývoj
bohů	bůh	k1gMnPc2	bůh
je	být	k5eAaImIp3nS	být
vcelku	vcelku	k6eAd1	vcelku
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
společnosti	společnost	k1gFnSc2	společnost
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
počtu	počet	k1gInSc2	počet
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
boha	bůh	k1gMnSc2	bůh
něčeho	něco	k3yInSc2	něco
dobrého	dobrý	k2eAgMnSc4d1	dobrý
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Dia	Dia	k1gFnSc2	Dia
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
<g/>
)	)	kIx)	)
stane	stanout	k5eAaPmIp3nS	stanout
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přijme	přijmout	k5eAaPmIp3nS	přijmout
i	i	k8xC	i
aspekty	aspekt	k1gInPc4	aspekt
boha	bůh	k1gMnSc4	bůh
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
opačného	opačný	k2eAgInSc2d1	opačný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
nastal	nastat	k5eAaPmAgInS	nastat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
raně	raně	k6eAd1	raně
mykénské	mykénský	k2eAgNnSc1d1	mykénské
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
Zeus	Zeus	k1gInSc4	Zeus
zažil	zažít	k5eAaPmAgMnS	zažít
velké	velký	k2eAgNnSc4d1	velké
povýšení	povýšení	k1gNnSc4	povýšení
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
ochráncem	ochránce	k1gMnSc7	ochránce
králů	král	k1gMnPc2	král
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
jeho	jeho	k3xOp3gFnSc4	jeho
důležitost	důležitost	k1gFnSc4	důležitost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
právě	právě	k6eAd1	právě
výjimečnou	výjimečný	k2eAgFnSc7d1	výjimečná
důležitostí	důležitost	k1gFnSc7	důležitost
dobrého	dobrý	k2eAgNnSc2d1	dobré
počasí	počasí	k1gNnSc2	počasí
na	na	k7c4	na
krétskou	krétský	k2eAgFnSc4d1	krétská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Mykén	Mykény	k1gFnPc2	Mykény
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
dostalo	dostat	k5eAaPmAgNnS	dostat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k6eAd1	až
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
U	u	k7c2	u
Dia	Dia	k1gFnSc2	Dia
poté	poté	k6eAd1	poté
nastal	nastat	k5eAaPmAgInS	nastat
relativně	relativně	k6eAd1	relativně
nezvyklý	zvyklý	k2eNgInSc4d1	nezvyklý
přechod	přechod	k1gInSc4	přechod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jako	jako	k8xS	jako
bůh	bůh	k1gMnSc1	bůh
počasí	počasí	k1gNnSc2	počasí
přijal	přijmout	k5eAaPmAgMnS	přijmout
funkci	funkce	k1gFnSc4	funkce
boha	bůh	k1gMnSc2	bůh
hor.	hor.	k?	hor.
Tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
je	být	k5eAaImIp3nS	být
nezvyklý	zvyklý	k2eNgMnSc1d1	nezvyklý
a	a	k8xC	a
u	u	k7c2	u
Dia	Dia	k1gFnSc2	Dia
je	být	k5eAaImIp3nS	být
i	i	k9	i
problematické	problematický	k2eAgNnSc1d1	problematické
ho	on	k3xPp3gNnSc4	on
časově	časově	k6eAd1	časově
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
vydržel	vydržet	k5eAaPmAgMnS	vydržet
až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
dórských	dórský	k2eAgInPc2d1	dórský
kmenů	kmen	k1gInPc2	kmen
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1100	[number]	k4	1100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
si	se	k3xPyFc3	se
však	však	k9	však
podržel	podržet	k5eAaPmAgMnS	podržet
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
ochránce	ochránce	k1gMnSc2	ochránce
řeckých	řecký	k2eAgMnPc2d1	řecký
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
lokálních	lokální	k2eAgMnPc2d1	lokální
vládců	vládce	k1gMnPc2	vládce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
ustálila	ustálit	k5eAaPmAgFnS	ustálit
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
potřeba	potřeba	k1gFnSc1	potřeba
hlavního	hlavní	k2eAgMnSc2d1	hlavní
boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
právě	právě	k9	právě
Zeus	Zeus	k1gInSc1	Zeus
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
zaostalost	zaostalost	k1gFnSc4	zaostalost
řeckých	řecký	k2eAgFnPc2d1	řecká
horských	horský	k2eAgFnPc2d1	horská
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
nečekaným	čekaný	k2eNgInSc7d1	nečekaný
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
se	se	k3xPyFc4	se
Zeus	Zeus	k1gInSc1	Zeus
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
bůh	bůh	k1gMnSc1	bůh
prosadil	prosadit	k5eAaPmAgMnS	prosadit
-	-	kIx~	-
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
776	[number]	k4	776
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
konaly	konat	k5eAaImAgFnP	konat
první	první	k4xOgFnPc1	první
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
vyvodit	vyvodit	k5eAaBmF	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
dórském	dórský	k2eAgNnSc6d1	dórské
pojetí	pojetí	k1gNnSc6	pojetí
měl	mít	k5eAaImAgMnS	mít
tento	tento	k3xDgMnSc1	tento
bůh	bůh	k1gMnSc1	bůh
nějakou	nějaký	k3yIgFnSc4	nějaký
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
a	a	k8xC	a
představa	představa	k1gFnSc1	představa
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tento	tento	k3xDgMnSc1	tento
bůh	bůh	k1gMnSc1	bůh
byl	být	k5eAaImAgMnS	být
ochránce	ochránce	k1gMnSc4	ochránce
panovníka	panovník	k1gMnSc4	panovník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
nebyla	být	k5eNaImAgFnS	být
nepřijatelná	přijatelný	k2eNgFnSc1d1	nepřijatelná
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
moc	moc	k1gFnSc1	moc
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
lokálních	lokální	k2eAgMnPc2d1	lokální
vládců	vládce	k1gMnPc2	vládce
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
stoupat	stoupat	k5eAaImF	stoupat
i	i	k9	i
moc	moc	k1gFnSc4	moc
jejich	jejich	k3xOp3gMnSc2	jejich
ochránce	ochránce	k1gMnSc2	ochránce
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
vývoj	vývoj	k1gInSc1	vývoj
boha-ochránce	bohachránec	k1gMnSc2	boha-ochránec
panovníka	panovník	k1gMnSc2	panovník
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
společenství	společenství	k1gNnPc2	společenství
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
egyptském	egyptský	k2eAgNnSc6d1	egyptské
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Dalším	další	k2eAgInSc7d1	další
nezanedbatelným	zanedbatelný	k2eNgInSc7d1	nezanedbatelný
faktorem	faktor	k1gInSc7	faktor
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Dórové	Dór	k1gMnPc1	Dór
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
klimaticky	klimaticky	k6eAd1	klimaticky
příznivé	příznivý	k2eAgFnSc2d1	příznivá
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
původním	původní	k2eAgMnPc3d1	původní
řeckým	řecký	k2eAgMnPc3d1	řecký
kmenům	kmen	k1gInPc3	kmen
zatlačeným	zatlačený	k2eAgInPc3d1	zatlačený
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
znám	znát	k5eAaImIp1nS	znát
i	i	k9	i
jako	jako	k9	jako
bůh	bůh	k1gMnSc1	bůh
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
všemi	všecek	k3xTgInPc7	všecek
ochotně	ochotně	k6eAd1	ochotně
přijat	přijmout	k5eAaPmNgInS	přijmout
za	za	k7c4	za
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
měl	mít	k5eAaImAgMnS	mít
Zeus	Zeus	k1gInSc4	Zeus
příliš	příliš	k6eAd1	příliš
lidské	lidský	k2eAgFnSc2d1	lidská
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jeho	jeho	k3xOp3gInSc3	jeho
rychlému	rychlý	k2eAgInSc3d1	rychlý
vzniku	vznik	k1gInSc3	vznik
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
mýty	mýtus	k1gInPc1	mýtus
a	a	k8xC	a
báje	báj	k1gFnPc1	báj
nestačily	stačit	k5eNaBmAgFnP	stačit
posunout	posunout	k5eAaPmF	posunout
z	z	k7c2	z
vypravování	vypravování	k1gNnSc2	vypravování
bardů	bard	k1gMnPc2	bard
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
odlidštění	odlidštění	k1gNnSc1	odlidštění
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
sklon	sklon	k1gInSc1	sklon
se	se	k3xPyFc4	se
nejvýrazněji	výrazně	k6eAd3	výrazně
projevila	projevit	k5eAaPmAgFnS	projevit
u	u	k7c2	u
stoiků	stoik	k1gMnPc2	stoik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
byl	být	k5eAaImAgInS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
potřebou	potřeba	k1gFnSc7	potřeba
vytvořit	vytvořit	k5eAaPmF	vytvořit
boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nP	by
lidi	člověk	k1gMnPc4	člověk
morálně	morálně	k6eAd1	morálně
převyšoval	převyšovat	k5eAaImAgInS	převyšovat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Tyto	tento	k3xDgFnPc1	tento
tendence	tendence	k1gFnPc1	tendence
sílily	sílit	k5eAaImAgFnP	sílit
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
;	;	kIx,	;
protože	protože	k8xS	protože
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
morálku	morálka	k1gFnSc4	morálka
se	se	k3xPyFc4	se
kladly	klást	k5eAaImAgFnP	klást
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInPc1d2	veliký
nároky	nárok	k1gInPc1	nárok
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
zmoralizovat	zmoralizovat	k5eAaPmF	zmoralizovat
i	i	k9	i
bohy	bůh	k1gMnPc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
funkce	funkce	k1gFnSc2	funkce
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zeus	Zeus	k1gInSc4	Zeus
byl	být	k5eAaImAgMnS	být
vládce	vládce	k1gMnSc1	vládce
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
takový	takový	k3xDgInSc4	takový
ovládal	ovládat	k5eAaImAgInS	ovládat
i	i	k9	i
blesky	blesk	k1gInPc4	blesk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
shromažďoval	shromažďovat	k5eAaImAgMnS	shromažďovat
mraky	mrak	k1gInPc4	mrak
<g/>
,	,	kIx,	,
zařizoval	zařizovat	k5eAaImAgMnS	zařizovat
déšť	déšť	k1gInSc4	déšť
<g/>
,	,	kIx,	,
sníh	sníh	k1gInSc4	sníh
<g/>
,	,	kIx,	,
duhu	duha	k1gFnSc4	duha
a	a	k8xC	a
ostatní	ostatní	k2eAgNnSc4d1	ostatní
počasí	počasí	k1gNnSc4	počasí
-	-	kIx~	-
např.	např.	kA	např.
"	"	kIx"	"
<g/>
probouzel	probouzet	k5eAaImAgInS	probouzet
vítr	vítr	k1gInSc1	vítr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
z	z	k7c2	z
Íliady	Íliada	k1gFnSc2	Íliada
lze	lze	k6eAd1	lze
usoudit	usoudit	k5eAaPmF	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
všemocný	všemocný	k2eAgMnSc1d1	všemocný
ani	ani	k8xC	ani
vševědoucí	vševědoucí	k2eAgMnSc1d1	vševědoucí
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
proměňovat	proměňovat	k5eAaImF	proměňovat
v	v	k7c4	v
cokoli	cokoli	k3yInSc4	cokoli
a	a	k8xC	a
kohokoli	kdokoli	k3yInSc2	kdokoli
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
proměňovat	proměňovat	k5eAaImF	proměňovat
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Diova	Diův	k2eAgFnSc1d1	Diova
funkce	funkce	k1gFnSc1	funkce
ochránce	ochránce	k1gMnSc2	ochránce
vladaře	vladař	k1gMnSc2	vladař
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zcela	zcela	k6eAd1	zcela
vytratila	vytratit	k5eAaPmAgFnS	vytratit
a	a	k8xC	a
přenesla	přenést	k5eAaPmAgFnS	přenést
se	se	k3xPyFc4	se
na	na	k7c4	na
ochránce	ochránce	k1gMnSc1	ochránce
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
způsobené	způsobený	k2eAgNnSc1d1	způsobené
rozvojem	rozvoj	k1gInSc7	rozvoj
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neměla	mít	k5eNaImAgFnS	mít
přímého	přímý	k2eAgMnSc4d1	přímý
vládce	vládce	k1gMnSc4	vládce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
cítil	cítit	k5eAaImAgMnS	cítit
povinnost	povinnost	k1gFnSc4	povinnost
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
někomu	někdo	k3yInSc3	někdo
děkovat	děkovat	k5eAaImF	děkovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mu	on	k3xPp3gMnSc3	on
podléhalo	podléhat	k5eAaImAgNnS	podléhat
nejen	nejen	k6eAd1	nejen
lidstvo	lidstvo	k1gNnSc1	lidstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jednotlivec	jednotlivec	k1gMnSc1	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
od	od	k7c2	od
pojetí	pojetí	k1gNnSc2	pojetí
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
pouze	pouze	k6eAd1	pouze
plní	plnit	k5eAaImIp3nS	plnit
Diovu	Diův	k2eAgFnSc4d1	Diova
vůli	vůle	k1gFnSc4	vůle
až	až	k9	až
po	po	k7c4	po
silné	silný	k2eAgNnSc4d1	silné
omezení	omezení	k1gNnSc4	omezení
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
připadla	připadnout	k5eAaPmAgNnP	připadnout
Moiře	Moiř	k1gInPc4	Moiř
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
dohlížel	dohlížet	k5eAaImAgMnS	dohlížet
nad	nad	k7c7	nad
řádem	řád	k1gInSc7	řád
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
střídání	střídání	k1gNnSc1	střídání
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
dne	den	k1gInSc2	den
a	a	k8xC	a
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
vyplynula	vyplynout	k5eAaPmAgFnS	vyplynout
jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
ochránce	ochránce	k1gMnSc2	ochránce
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
,	,	kIx,	,
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
,	,	kIx,	,
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
rodinného	rodinný	k2eAgInSc2d1	rodinný
krbu	krb	k1gInSc2	krb
<g/>
)	)	kIx)	)
a	a	k8xC	a
pohostinnosti	pohostinnost	k1gFnSc2	pohostinnost
<g/>
.	.	kIx.	.
</s>
<s>
Společensky	společensky	k6eAd1	společensky
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
ochránce	ochránce	k1gMnSc1	ochránce
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
věštěním	věštění	k1gNnSc7	věštění
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
zasvěcena	zasvěcen	k2eAgNnPc4d1	zasvěceno
druhá	druhý	k4xOgFnSc1	druhý
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
řecká	řecký	k2eAgFnSc1d1	řecká
věštírna	věštírna	k1gFnSc1	věštírna
v	v	k7c6	v
Dódóně	Dódóna	k1gFnSc6	Dódóna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věštilo	věštit	k5eAaImAgNnS	věštit
podle	podle	k7c2	podle
šumu	šum	k1gInSc2	šum
listů	list	k1gInPc2	list
posvátného	posvátný	k2eAgInSc2d1	posvátný
dubu	dub	k1gInSc2	dub
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
věštění	věštění	k1gNnSc2	věštění
mohl	moct	k5eAaImAgInS	moct
svoji	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
lidem	člověk	k1gMnPc3	člověk
zjevovat	zjevovat	k5eAaImF	zjevovat
pomocí	pomocí	k7c2	pomocí
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
hromem	hrom	k1gInSc7	hrom
<g/>
,	,	kIx,	,
bleskem	blesk	k1gInSc7	blesk
<g/>
,	,	kIx,	,
či	či	k8xC	či
letem	letem	k6eAd1	letem
orla	orel	k1gMnSc4	orel
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
podání	podání	k1gNnSc6	podání
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
i	i	k9	i
jako	jako	k8xC	jako
ochránce	ochránce	k1gMnSc1	ochránce
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
častěji	často	k6eAd2	často
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
bohyně	bohyně	k1gFnSc1	bohyně
Diké	Diká	k1gFnSc2	Diká
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hérou	Héra	k1gFnSc7	Héra
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k8xS	jako
ochránce	ochránce	k1gMnSc2	ochránce
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
byl	být	k5eAaImAgMnS	být
spíše	spíše	k9	spíše
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Héry	Héra	k1gFnSc2	Héra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Iliady	Iliada	k1gFnSc2	Iliada
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
domě	dům	k1gInSc6	dům
dva	dva	k4xCgInPc4	dva
sudy	sud	k1gInPc4	sud
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
posílá	posílat	k5eAaImIp3nS	posílat
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
lidem	člověk	k1gMnPc3	člověk
dobré	dobrý	k2eAgFnSc2d1	dobrá
nebo	nebo	k8xC	nebo
špatné	špatný	k2eAgFnSc2d1	špatná
věci	věc	k1gFnSc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
jen	jen	k9	jen
na	na	k7c6	na
něm.	něm.	k?	něm.
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
mýtů	mýtus	k1gInPc2	mýtus
byl	být	k5eAaImAgInS	být
Zeus	Zeus	k1gInSc1	Zeus
i	i	k8xC	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
však	však	k9	však
byla	být	k5eAaImAgFnS	být
limitována	limitován	k2eAgFnSc1d1	limitována
ostatními	ostatní	k2eAgMnPc7d1	ostatní
bohy	bůh	k1gMnPc7	bůh
a	a	k8xC	a
Zeus	Zeus	k1gInSc1	Zeus
si	se	k3xPyFc3	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
rozhodně	rozhodně	k6eAd1	rozhodně
nemohl	moct	k5eNaImAgMnS	moct
počínat	počínat	k5eAaImF	počínat
suverénně	suverénně	k6eAd1	suverénně
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
lidem	člověk	k1gMnPc3	člověk
dává	dávat	k5eAaImIp3nS	dávat
nejen	nejen	k6eAd1	nejen
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
spokojenost	spokojenost	k1gFnSc1	spokojenost
a	a	k8xC	a
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
.	.	kIx.	.
</s>
<s>
Diovy	Diův	k2eAgFnPc1d1	Diova
morální	morální	k2eAgFnPc1d1	morální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
mají	mít	k5eAaImIp3nP	mít
velice	velice	k6eAd1	velice
nestálý	stálý	k2eNgInSc4d1	nestálý
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
každopádně	každopádně	k6eAd1	každopádně
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c2	za
mstivého	mstivý	k2eAgMnSc2d1	mstivý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
záludného	záludný	k2eAgInSc2d1	záludný
a	a	k8xC	a
intrikářského	intrikářský	k2eAgInSc2d1	intrikářský
<g/>
.	.	kIx.	.
</s>
<s>
Manželce	manželka	k1gFnSc3	manželka
byl	být	k5eAaImAgMnS	být
nevěrný	věrný	k2eNgMnSc1d1	nevěrný
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
hádal	hádat	k5eAaImAgMnS	hádat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
silně	silně	k6eAd1	silně
bisexuální	bisexuální	k2eAgInPc4d1	bisexuální
sklony	sklon	k1gInPc4	sklon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc1	jeho
homosexuální	homosexuální	k2eAgFnSc1d1	homosexuální
tendence	tendence	k1gFnSc1	tendence
se	se	k3xPyFc4	se
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
pedofilie	pedofilie	k1gFnSc2	pedofilie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
postavě	postava	k1gFnSc6	postava
tohoto	tento	k3xDgMnSc4	tento
boha	bůh	k1gMnSc4	bůh
si	se	k3xPyFc3	se
děláme	dělat	k5eAaImIp1nP	dělat
představy	představa	k1gFnPc4	představa
z	z	k7c2	z
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
sepsali	sepsat	k5eAaPmAgMnP	sepsat
řečtí	řecký	k2eAgMnPc1d1	řecký
autoři	autor	k1gMnPc1	autor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
mýtech	mýtus	k1gInPc6	mýtus
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc4d1	patrný
silný	silný	k2eAgInSc4d1	silný
neřecký	řecký	k2eNgInSc4d1	řecký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pojetí	pojetí	k1gNnSc4	pojetí
tohoto	tento	k3xDgMnSc2	tento
boha	bůh	k1gMnSc2	bůh
značně	značně	k6eAd1	značně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
řecké	řecký	k2eAgInPc1d1	řecký
mýty	mýtus	k1gInPc1	mýtus
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
chronologicky	chronologicky	k6eAd1	chronologicky
pojatý	pojatý	k2eAgInSc4d1	pojatý
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
sestavit	sestavit	k5eAaPmF	sestavit
něco	něco	k3yInSc4	něco
jako	jako	k8xS	jako
Diův	Diův	k2eAgInSc4d1	Diův
životopis	životopis	k1gInSc4	životopis
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k6eAd1	Zeus
byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
Titána	Titán	k1gMnSc2	Titán
Krona	Kron	k1gMnSc2	Kron
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Rheie	Rheie	k1gFnSc2	Rheie
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Ída	Ída	k1gFnSc2	Ída
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
v	v	k7c6	v
Aigaiském	Aigaiský	k2eAgNnSc6d1	Aigaiský
pohoří	pohoří	k1gNnSc6	pohoří
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
dalších	další	k2eAgFnPc2d1	další
verzí	verze	k1gFnPc2	verze
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
kopec	kopec	k1gInSc4	kopec
Dikté	Diktý	k2eAgFnPc1d1	Diktý
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
uschovala	uschovat	k5eAaPmAgFnS	uschovat
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
před	před	k7c7	před
Kronem	Kron	k1gMnSc7	Kron
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
vzpoury	vzpoura	k1gFnSc2	vzpoura
a	a	k8xC	a
každé	každý	k3xTgNnSc4	každý
její	její	k3xOp3gNnSc4	její
dítě	dítě	k1gNnSc4	dítě
preventivně	preventivně	k6eAd1	preventivně
pozřel	pozřít	k5eAaPmAgInS	pozřít
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
obava	obava	k1gFnSc1	obava
vycházela	vycházet	k5eAaImAgFnS	vycházet
z	z	k7c2	z
proroctví	proroctví	k1gNnSc2	proroctví
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dětí	dítě	k1gFnPc2	dítě
svrhne	svrhnout	k5eAaPmIp3nS	svrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Živil	živit	k5eAaImAgMnS	živit
se	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
Amaltheie	Amaltheie	k1gFnSc2	Amaltheie
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
verzí	verze	k1gFnPc2	verze
koza	koza	k1gFnSc1	koza
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiná	k1gFnPc2	jiná
nymfa	nymfa	k1gFnSc1	nymfa
a	a	k8xC	a
medem	med	k1gInSc7	med
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
ho	on	k3xPp3gMnSc4	on
krmily	krmit	k5eAaImAgFnP	krmit
včely	včela	k1gFnPc1	včela
a	a	k8xC	a
pečovaly	pečovat	k5eAaImAgFnP	pečovat
o	o	k7c4	o
něj	on	k3xPp3gInSc4	on
kúréti	kúrét	k5eAaPmF	kúrét
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
verzí	verze	k1gFnPc2	verze
i	i	k8xC	i
nymfy	nymfa	k1gFnSc2	nymfa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
Dia	Dia	k1gFnSc2	Dia
dala	dát	k5eAaPmAgFnS	dát
Rheia	Rheia	k1gFnSc1	Rheia
Kronovi	Kron	k1gMnSc3	Kron
spolknout	spolknout	k5eAaPmF	spolknout
kámen	kámen	k1gInSc4	kámen
zabalený	zabalený	k2eAgInSc4d1	zabalený
do	do	k7c2	do
dětských	dětský	k2eAgNnPc2d1	dětské
plen	pleno	k1gNnPc2	pleno
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
kámen	kámen	k1gInSc4	kámen
Zeus	Zeusa	k1gFnPc2	Zeusa
později	pozdě	k6eAd2	pozdě
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
Delfách	Delfy	k1gFnPc6	Delfy
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
má	mít	k5eAaImIp3nS	mít
evidentně	evidentně	k6eAd1	evidentně
krétské	krétský	k2eAgInPc4d1	krétský
kořeny	kořen	k1gInPc4	kořen
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
Dia	Dia	k1gMnPc2	Dia
s	s	k7c7	s
některým	některý	k3yIgMnSc7	některý
z	z	k7c2	z
vegetačních	vegetační	k2eAgNnPc2d1	vegetační
božstev	božstvo	k1gNnPc2	božstvo
mykénské	mykénský	k2eAgFnSc2d1	mykénská
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Zeus	Zeus	k1gInSc1	Zeus
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
vedl	vést	k5eAaImAgInS	vést
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
staršími	starý	k2eAgMnPc7d2	starší
sourozenci	sourozenec	k1gMnPc7	sourozenec
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc4	ten
Kronos	Kronos	k1gMnSc1	Kronos
také	také	k6eAd1	také
původně	původně	k6eAd1	původně
spolkl	spolknout	k5eAaPmAgMnS	spolknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
ho	on	k3xPp3gInSc4	on
tlačil	tlačit	k5eAaImAgInS	tlačit
kámen	kámen	k1gInSc1	kámen
spolknutý	spolknutý	k2eAgInSc1d1	spolknutý
místo	místo	k7c2	místo
Dia	Dia	k1gMnPc2	Dia
<g/>
,	,	kIx,	,
vyvrhl	vyvrhnout	k5eAaPmAgInS	vyvrhnout
je	být	k5eAaImIp3nS	být
<g/>
)	)	kIx)	)
vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
Kronovi	Kron	k1gMnSc3	Kron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
většina	většina	k1gFnSc1	většina
Titánů	Titán	k1gMnPc2	Titán
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Krona	Kron	k1gMnSc2	Kron
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Dia	Dia	k1gMnPc2	Dia
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
sourozenců	sourozenec	k1gMnPc2	sourozenec
bojovali	bojovat	k5eAaImAgMnP	bojovat
jednoocí	jednooký	k2eAgMnPc1d1	jednooký
obři	obr	k1gMnPc1	obr
Kyklopové	Kyklop	k1gMnPc1	Kyklop
a	a	k8xC	a
storucí	storuký	k2eAgMnPc1d1	storuký
obři	obr	k1gMnPc1	obr
Hekatoncheirové	Hekatoncheirová	k1gFnSc2	Hekatoncheirová
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k6eAd1	Zeus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
spojenci	spojenec	k1gMnPc1	spojenec
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
po	po	k7c6	po
desetiletém	desetiletý	k2eAgInSc6d1	desetiletý
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
mýtu	mýtus	k1gInSc2	mýtus
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
a	a	k8xC	a
krétský	krétský	k2eAgInSc4d1	krétský
vliv	vliv	k1gInSc4	vliv
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zcela	zcela	k6eAd1	zcela
nepatrný	patrný	k2eNgInSc4d1	patrný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
losem	los	k1gInSc7	los
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
světem	svět	k1gInSc7	svět
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
bratry	bratr	k1gMnPc7	bratr
-	-	kIx~	-
Diovi	Diův	k2eAgMnPc1d1	Diův
připadla	připadnout	k5eAaPmAgFnS	připadnout
vláda	vláda	k1gFnSc1	vláda
nad	nad	k7c7	nad
nebem	nebe	k1gNnSc7	nebe
a	a	k8xC	a
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
Dia	Dia	k1gFnPc2	Dia
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
pohoří	pohoří	k1gNnSc2	pohoří
Olymp	Olymp	k1gInSc1	Olymp
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlil	sídlit	k5eAaImAgInS	sídlit
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
vybranými	vybraný	k2eAgMnPc7d1	vybraný
bohy	bůh	k1gMnPc7	bůh
nazývanými	nazývaný	k2eAgFnPc7d1	nazývaná
Olympané	Olympan	k1gMnPc5	Olympan
<g/>
.	.	kIx.	.
</s>
<s>
Diovu	Diův	k2eAgFnSc4d1	Diova
vládu	vláda	k1gFnSc4	vláda
vážně	vážně	k6eAd1	vážně
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
povstání	povstání	k1gNnSc1	povstání
obrů	obr	k1gMnPc2	obr
Gigantů	gigant	k1gInPc2	gigant
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
ostatních	ostatní	k2eAgMnPc2d1	ostatní
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
pozemského	pozemský	k2eAgMnSc2d1	pozemský
syna	syn	k1gMnSc2	syn
Hérakla	Hérakles	k1gMnSc2	Hérakles
potlačil	potlačit	k5eAaPmAgMnS	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
mýty	mýtus	k1gInPc1	mýtus
pak	pak	k6eAd1	pak
popisují	popisovat	k5eAaImIp3nP	popisovat
různé	různý	k2eAgInPc4d1	různý
Diovy	Diův	k2eAgInPc4d1	Diův
zálety	zálet	k1gInPc4	zálet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gNnSc4	jeho
zasahování	zasahování	k1gNnSc4	zasahování
do	do	k7c2	do
lidských	lidský	k2eAgFnPc2d1	lidská
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
nutně	nutně	k6eAd1	nutně
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
člověku	člověk	k1gMnSc3	člověk
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Prométheus	Prométheus	k1gMnSc1	Prométheus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
dohlížitel	dohlížitel	k1gMnSc1	dohlížitel
na	na	k7c4	na
řád	řád	k1gInSc4	řád
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgMnS	projevit
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
když	když	k8xS	když
bleskem	blesk	k1gInSc7	blesk
srazil	srazit	k5eAaPmAgMnS	srazit
Faethónta	Faethónta	k1gMnSc1	Faethónta
<g/>
.	.	kIx.	.
</s>
<s>
Diovým	Diův	k2eAgInSc7d1	Diův
symbolem	symbol	k1gInSc7	symbol
byl	být	k5eAaImAgMnS	být
orel	orel	k1gMnSc1	orel
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
blesků	blesk	k1gInPc2	blesk
a	a	k8xC	a
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posvátným	posvátný	k2eAgInSc7d1	posvátný
stromem	strom	k1gInSc7	strom
je	být	k5eAaImIp3nS	být
dub	dub	k1gInSc1	dub
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnil	vlastnit	k5eAaImAgInS	vlastnit
rovněž	rovněž	k9	rovněž
nezničitelný	zničitelný	k2eNgInSc1d1	nezničitelný
štít	štít	k1gInSc1	štít
aigis	aigis	k1gInSc1	aigis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
Héfaistos	Héfaistos	k1gInSc1	Héfaistos
z	z	k7c2	z
kozí	kozí	k2eAgFnSc2d1	kozí
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
dětí	dítě	k1gFnPc2	dítě
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgMnPc4	některý
si	se	k3xPyFc3	se
vynutila	vynutit	k5eAaPmAgFnS	vynutit
mytologická	mytologický	k2eAgFnSc1d1	mytologická
logika	logika	k1gFnSc1	logika
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovýchto	takovýto	k3xDgInPc6	takovýto
případech	případ	k1gInPc6	případ
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
u	u	k7c2	u
tohoto	tento	k3xDgMnSc2	tento
potomka	potomek	k1gMnSc2	potomek
jistou	jistý	k2eAgFnSc4d1	jistá
příbuznost	příbuznost	k1gFnSc4	příbuznost
s	s	k7c7	s
jasným	jasný	k2eAgNnSc7d1	jasné
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
snahu	snaha	k1gFnSc4	snaha
logicky	logicky	k6eAd1	logicky
uspořádat	uspořádat	k5eAaPmF	uspořádat
mytologii	mytologie	k1gFnSc4	mytologie
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
příbuzenskou	příbuzenský	k2eAgFnSc4d1	příbuzenská
linii	linie	k1gFnSc4	linie
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
královskými	královský	k2eAgInPc7d1	královský
rody	rod	k1gInPc7	rod
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnSc4	rodič
Kronos	Kronos	k1gMnSc1	Kronos
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
Rheia	Rheia	k1gFnSc1	Rheia
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
Sourozenci	sourozenec	k1gMnPc7	sourozenec
Sestry	sestra	k1gFnSc2	sestra
Hestiá	Hestiá	k1gFnSc1	Hestiá
Démétér	Démétér	k1gFnSc1	Démétér
Héra	Héra	k1gFnSc1	Héra
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
manželka	manželka	k1gFnSc1	manželka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Hádés	Hádésa	k1gFnPc2	Hádésa
Poseidón	Poseidón	k1gMnSc1	Poseidón
</s>
<s>
Manželky	manželka	k1gFnSc2	manželka
Métis	Métis	k1gFnSc1	Métis
Héra	Héra	k1gFnSc1	Héra
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
sestra	sestra	k1gFnSc1	sestra
<g/>
)	)	kIx)	)
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
Manželské	manželský	k2eAgFnPc1d1	manželská
(	(	kIx(	(
<g/>
s	s	k7c7	s
Hérou	Héra	k1gFnSc7	Héra
<g/>
)	)	kIx)	)
Áres	Áres	k1gMnSc1	Áres
(	(	kIx(	(
<g/>
bůh	bůh	k1gMnSc1	bůh
<g />
.	.	kIx.	.
</s>
<s>
války	válka	k1gFnPc1	válka
<g/>
)	)	kIx)	)
Hébé	Hébé	k1gFnSc1	Hébé
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
věčné	věčný	k2eAgFnSc2d1	věčná
mladosti	mladost	k1gFnSc2	mladost
<g/>
)	)	kIx)	)
Eileithýia	Eileithýia	k1gFnSc1	Eileithýia
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
porodu	porod	k1gInSc2	porod
<g/>
)	)	kIx)	)
Héfaistos	Héfaistos	k1gMnSc1	Héfaistos
(	(	kIx(	(
<g/>
bůh	bůh	k1gMnSc1	bůh
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
kovářství	kovářství	k1gNnSc2	kovářství
<g/>
)	)	kIx)	)
Nemanželské	manželský	k2eNgInPc1d1	nemanželský
S	s	k7c7	s
bohyněmi	bohyně	k1gFnPc7	bohyně
(	(	kIx(	(
<g/>
řazeno	řazen	k2eAgNnSc1d1	řazeno
abecedně	abecedně	k6eAd1	abecedně
podle	podle	k7c2	podle
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
Persefona	Persefona	k1gFnSc1	Persefona
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Démétér	Démétér	k1gFnSc2	Démétér
-	-	kIx~	-
zároveň	zároveň	k6eAd1	zároveň
sestra	sestra	k1gFnSc1	sestra
<g/>
)	)	kIx)	)
Afrodíté	Afrodítý	k2eAgNnSc1d1	Afrodíté
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
matka	matka	k1gFnSc1	matka
Dióna	Dión	k1gInSc2	Dión
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
Homéra	Homér	k1gMnSc2	Homér
<g/>
)	)	kIx)	)
Charitky	Charitka	k1gFnPc1	Charitka
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Eurynomé	Eurynomý	k2eAgFnPc1d1	Eurynomý
<g/>
)	)	kIx)	)
Apollón	Apollón	k1gMnSc1	Apollón
a	a	k8xC	a
Artemis	Artemis	k1gFnSc1	Artemis
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Létó	Létó	k1gFnSc1	Létó
<g/>
)	)	kIx)	)
Hermés	Hermés	k1gInSc1	Hermés
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Maia	Maia	k1gFnSc1	Maia
<g/>
)	)	kIx)	)
Múzy	Múza	k1gFnPc1	Múza
(	(	kIx(	(
<g/>
devět	devět	k4xCc1	devět
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
devíti	devět	k4xCc2	devět
nocí	noc	k1gFnPc2	noc
strávených	strávený	k2eAgFnPc2d1	strávená
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
po	po	k7c4	po
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Mnémosyné	Mnémosyná	k1gFnSc2	Mnémosyná
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Helena	Helena	k1gFnSc1	Helena
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Nemesis	Nemesis	k1gFnSc1	Nemesis
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
matku	matka	k1gFnSc4	matka
označována	označovat	k5eAaImNgFnS	označovat
Léda	Léda	k1gFnSc1	Léda
<g/>
)	)	kIx)	)
Moiry	Moir	k1gInPc1	Moir
a	a	k8xC	a
Hóry	Hóra	k1gFnPc1	Hóra
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Themis	Themis	k1gFnSc1	Themis
<g/>
)	)	kIx)	)
S	s	k7c7	s
nebohyní	nebohyně	k1gFnSc7	nebohyně
(	(	kIx(	(
<g/>
řazeno	řazen	k2eAgNnSc1d1	řazeno
abecedně	abecedně	k6eAd1	abecedně
podle	podle	k7c2	podle
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
Aiakos	Aiakos	k1gInSc1	Aiakos
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Aigína	Aigína	k1gFnSc1	Aigína
<g/>
)	)	kIx)	)
Héraklés	Héraklés	k1gInSc1	Héraklés
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Alkména	Alkména	k1gFnSc1	Alkména
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
velmi	velmi	k6eAd1	velmi
výjimečně	výjimečně	k6eAd1	výjimečně
za	za	k7c4	za
matku	matka	k1gFnSc4	matka
považována	považován	k2eAgFnSc1d1	považována
Semelé	Semelý	k2eAgFnPc1d1	Semelý
<g/>
)	)	kIx)	)
Amfíón	Amfíón	k1gMnSc1	Amfíón
a	a	k8xC	a
Zéthos	Zéthos	k1gMnSc1	Zéthos
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Antiopa	Antiopa	k1gFnSc1	Antiopa
<g/>
)	)	kIx)	)
Perseus	Perseus	k1gMnSc1	Perseus
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Danaé	Danaá	k1gFnSc2	Danaá
<g/>
)	)	kIx)	)
Mínós	Mínós	k1gInSc1	Mínós
<g/>
,	,	kIx,	,
Rhadamanthys	Rhadamanthys	k1gInSc1	Rhadamanthys
a	a	k8xC	a
Sarpédón	Sarpédón	k1gInSc1	Sarpédón
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Európa	Európa	k1gFnSc1	Európa
<g/>
)	)	kIx)	)
Epafos	Epafos	k1gInSc1	Epafos
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Íó	Íó	k1gMnSc1	Íó
<g/>
)	)	kIx)	)
Arkas	Arkas	k1gMnSc1	Arkas
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Kallistó	Kallistó	k1gFnSc1	Kallistó
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Polydeukés	Polydeukés	k6eAd1	Polydeukés
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Dioskúrové	Dioskúrový	k2eAgNnSc4d1	Dioskúrový
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Léda	Léda	k1gMnSc1	Léda
<g/>
)	)	kIx)	)
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
(	(	kIx(	(
<g/>
matka	matka	k1gFnSc1	matka
Semelé	Semelý	k2eAgFnSc2d1	Semelý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
matka	matka	k1gFnSc1	matka
Hérakla	Hérakles	k1gMnSc2	Hérakles
<g/>
)	)	kIx)	)
Tantalos	Tantalos	k1gMnSc1	Tantalos
Bez	bez	k7c2	bez
matky	matka	k1gFnSc2	matka
Athéna	Athéna	k1gFnSc1	Athéna
(	(	kIx(	(
<g/>
vyskočila	vyskočit	k5eAaPmAgFnS	vyskočit
mu	on	k3xPp3gNnSc3	on
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plod	plod	k1gInSc1	plod
pocházel	pocházet	k5eAaImAgInS	pocházet
od	od	k7c2	od
první	první	k4xOgFnSc2	první
manželky	manželka	k1gFnSc2	manželka
Métis	Métis	k1gFnSc2	Métis
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pojal	pojmout	k5eAaPmAgMnS	pojmout
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
Kolem	kolem	k7c2	kolem
Dia	Dia	k1gFnSc2	Dia
se	se	k3xPyFc4	se
jako	jako	k9	jako
kolem	kolem	k7c2	kolem
každého	každý	k3xTgMnSc2	každý
významnějšího	významný	k2eAgMnSc2d2	významnější
boha	bůh	k1gMnSc2	bůh
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
docela	docela	k6eAd1	docela
silný	silný	k2eAgInSc1d1	silný
kult	kult	k1gInSc1	kult
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
neměl	mít	k5eNaImAgInS	mít
ustálená	ustálený	k2eAgNnPc4d1	ustálené
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
nijak	nijak	k6eAd1	nijak
nevybočoval	vybočovat	k5eNaImAgMnS	vybočovat
z	z	k7c2	z
rámce	rámec	k1gInSc2	rámec
rituálů	rituál	k1gInPc2	rituál
věštných	věštný	k2eAgInPc2d1	věštný
řeckých	řecký	k2eAgInPc2d1	řecký
respektive	respektive	k9	respektive
indoevropských	indoevropský	k2eAgMnPc2d1	indoevropský
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
obětí	oběť	k1gFnSc7	oběť
týče	týkat	k5eAaImIp3nS	týkat
tak	tak	k9	tak
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kultu	kult	k1gInSc6	kult
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
několik	několik	k4yIc1	několik
lidských	lidský	k2eAgFnPc2d1	lidská
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
klasicky	klasicky	k6eAd1	klasicky
řeckému	řecký	k2eAgInSc3d1	řecký
způsobu	způsob	k1gInSc3	způsob
obětování	obětování	k1gNnSc2	obětování
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
poctě	pocta	k1gFnSc3	pocta
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
závody	závod	k1gInPc1	závod
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
(	(	kIx(	(
<g/>
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Nemei	Neme	k1gFnSc6	Neme
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existovala	existovat	k5eAaImAgFnS	existovat
řada	řada	k1gFnSc1	řada
běžných	běžný	k2eAgFnPc2d1	běžná
oslav	oslava	k1gFnPc2	oslava
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
mimořádných	mimořádný	k2eAgFnPc2d1	mimořádná
oslav	oslava	k1gFnPc2	oslava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k9	třeba
naklonit	naklonit	k5eAaPmF	naklonit
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k1gInSc1	Zeus
měl	mít	k5eAaImAgInS	mít
relativně	relativně	k6eAd1	relativně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
očistných	očistný	k2eAgInPc6d1	očistný
a	a	k8xC	a
smírných	smírný	k2eAgInPc6d1	smírný
obřadech	obřad	k1gInPc6	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k1gInSc1	Zeus
samozřejmě	samozřejmě	k6eAd1	samozřejmě
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
do	do	k7c2	do
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
zasvěceno	zasvětit	k5eAaPmNgNnS	zasvětit
několik	několik	k4yIc1	několik
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
jednak	jednak	k8xC	jednak
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chrámů	chrám	k1gInPc2	chrám
se	se	k3xPyFc4	se
nejznámějším	známý	k2eAgInPc3d3	nejznámější
stal	stát	k5eAaPmAgInS	stát
Diův	Diův	k2eAgInSc1d1	Diův
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
největším	veliký	k2eAgInSc7d3	veliký
řeckým	řecký	k2eAgInSc7d1	řecký
chrámem	chrám	k1gInSc7	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
chrámu	chrám	k1gInSc6	chrám
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Diova	Diův	k2eAgFnSc1d1	Diova
socha	socha	k1gFnSc1	socha
tzv.	tzv.	kA	tzv.
Feidiova	Feidiův	k2eAgFnSc1d1	Feidiova
socha	socha	k1gFnSc1	socha
Dia	Dia	k1gFnSc2	Dia
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
z	z	k7c2	z
roku	rok	k1gInSc2	rok
453	[number]	k4	453
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
sochař	sochař	k1gMnSc1	sochař
Feidiás	Feidiása	k1gFnPc2	Feidiása
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
velmi	velmi	k6eAd1	velmi
známý	známý	k2eAgInSc1d1	známý
chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
Olympion	Olympion	k1gInSc4	Olympion
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
věštírna	věštírna	k1gFnSc1	věštírna
v	v	k7c6	v
Dódóně	Dódóna	k1gFnSc6	Dódóna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zasvěcena	zasvětit	k5eAaPmNgFnS	zasvětit
Diovi	Diův	k2eAgMnPc5d1	Diův
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Řecko	Řecko	k1gNnSc4	Řecko
byl	být	k5eAaImAgInS	být
nejznámějším	známý	k2eAgInSc7d3	nejznámější
chrámem	chrám	k1gInSc7	chrám
patrně	patrně	k6eAd1	patrně
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Akragantu	Akragant	k1gInSc6	Akragant
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
byl	být	k5eAaImAgInS	být
Zeus	Zeus	k1gInSc1	Zeus
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
nejčastěji	často	k6eAd3	často
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vrhá	vrhat	k5eAaImIp3nS	vrhat
blesky	blesk	k1gInPc4	blesk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
některých	některý	k3yIgInPc6	některý
výjevech	výjev	k1gInPc6	výjev
z	z	k7c2	z
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
nejvděčnějším	vděčný	k2eAgNnSc7d3	nejvděčnější
tématem	téma	k1gNnSc7	téma
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
svržení	svržení	k1gNnSc1	svržení
Titánů	Titán	k1gMnPc2	Titán
do	do	k7c2	do
Tartaru	Tartar	k1gInSc2	Tartar
<g/>
,	,	kIx,	,
či	či	k8xC	či
boj	boj	k1gInSc1	boj
s	s	k7c7	s
Giganty	gigant	k1gInPc7	gigant
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Feidiovy	Feidiův	k2eAgFnPc4d1	Feidiova
sochy	socha	k1gFnPc4	socha
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
slavné	slavný	k2eAgNnSc1d1	slavné
sousoší	sousoší	k1gNnSc1	sousoší
Dia	Dia	k1gMnSc2	Dia
s	s	k7c7	s
Ganymédem	Ganyméd	k1gMnSc7	Ganyméd
<g/>
,	,	kIx,	,
či	či	k8xC	či
socha	socha	k1gFnSc1	socha
Zeus	Zeusa	k1gFnPc2	Zeusa
s	s	k7c7	s
Oticoli	Oticole	k1gFnSc3	Oticole
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
novější	nový	k2eAgFnSc2d2	novější
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
známa	znám	k2eAgNnPc1d1	známo
zobrazení	zobrazení	k1gNnPc1	zobrazení
především	především	k9	především
jeho	jeho	k3xOp3gFnPc2	jeho
milostných	milostný	k2eAgFnPc2d1	milostná
afér	aféra	k1gFnPc2	aféra
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Léda	Léda	k1gFnSc1	Léda
s	s	k7c7	s
labutí	labuť	k1gFnSc7	labuť
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
scény	scéna	k1gFnPc1	scéna
zobrazující	zobrazující	k2eAgFnPc1d1	zobrazující
všechny	všechen	k3xTgMnPc4	všechen
Olympské	olympský	k2eAgMnPc4d1	olympský
bohy	bůh	k1gMnPc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Souboj	souboj	k1gInSc4	souboj
Titánů	Titán	k1gMnPc2	Titán
Robert	Robert	k1gMnSc1	Robert
Graves	Graves	k1gMnSc1	Graves
<g/>
,	,	kIx,	,
Řecké	řecký	k2eAgInPc4d1	řecký
mýty	mýtus	k1gInPc4	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zeus	Zeusa	k1gFnPc2	Zeusa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Zeus	Zeusa	k1gFnPc2	Zeusa
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Film	film	k1gInSc1	film
Souboj	souboj	k1gInSc1	souboj
titánů	titán	k1gMnPc2	titán
na	na	k7c6	na
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Film	film	k1gInSc1	film
Souboj	souboj	k1gInSc1	souboj
titánů	titán	k1gMnPc2	titán
na	na	k7c4	na
IMDb	IMDb	k1gInSc4	IMDb
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
</s>
