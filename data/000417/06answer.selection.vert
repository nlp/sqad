<s>
Zeus	Zeus	k1gInSc1	Zeus
(	(	kIx(	(
<g/>
starověký	starověký	k2eAgMnSc1d1	starověký
řecký	řecký	k2eAgMnSc1d1	řecký
Ζ	Ζ	k?	Ζ
[	[	kIx(	[
<g/>
Zeús	Zeús	k1gInSc1	Zeús
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
moderní	moderní	k2eAgMnSc1d1	moderní
řecký	řecký	k2eAgMnSc1d1	řecký
Δ	Δ	k?	Δ
[	[	kIx(	[
<g/>
Dias	Dias	k1gInSc1	Dias
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
skloňování	skloňování	k1gNnSc1	skloňování
Dia	Dia	k1gFnSc2	Dia
<g/>
,	,	kIx,	,
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
,	,	kIx,	,
Die	Die	k1gMnSc1	Die
<g/>
,	,	kIx,	,
Diem	Diem	k1gMnSc1	Diem
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
z	z	k7c2	z
bohů	bůh	k1gMnPc2	bůh
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
