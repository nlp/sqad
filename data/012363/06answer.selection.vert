<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
druhem	druh	k1gInSc7	druh
chráněným	chráněný	k2eAgInSc7d1	chráněný
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
druhů	druh	k1gInPc2	druh
ještěrek	ještěrka	k1gFnPc2	ještěrka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
<g/>
.	.	kIx.	.
</s>
