<s>
Praxeologie	Praxeologie	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Praxeologie	Praxeologie	k1gFnSc1
je	být	k5eAaImIp3nS
věda	věda	k1gFnSc1
o	o	k7c6
lidském	lidský	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
<g/>
,	,	kIx,
či	či	k8xC
jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
logika	logika	k1gFnSc1
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k9
ekonomie	ekonomie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
však	však	k9
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
podání	podání	k1gNnSc6
rakouské	rakouský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
je	být	k5eAaImIp3nS
ekonomie	ekonomie	k1gFnSc1
praxeologií	praxeologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praxeologii	Praxeologie	k1gFnSc6
vnesl	vnést	k5eAaPmAgMnS
do	do	k7c2
vědeckého	vědecký	k2eAgInSc2d1
světa	svět	k1gInSc2
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ji	on	k3xPp3gFnSc4
označil	označit	k5eAaPmAgInS
za	za	k7c4
teoretickou	teoretický	k2eAgFnSc4d1
část	část	k1gFnSc4
věd	věda	k1gFnPc2
o	o	k7c6
lidském	lidský	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
protipólem	protipól	k1gInSc7
je	být	k5eAaImIp3nS
historie	historie	k1gFnSc1
se	s	k7c7
svojí	svůj	k3xOyFgFnSc7
specifickou	specifický	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
zkoumání	zkoumání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Právě	právě	k9
metoda	metoda	k1gFnSc1
zkoumání	zkoumání	k1gNnSc2
je	být	k5eAaImIp3nS
výchozím	výchozí	k2eAgInSc7d1
bodem	bod	k1gInSc7
praxeologie	praxeologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murray	Murraa	k1gFnSc2
Rothbard	Rothbarda	k1gFnPc2
dokonce	dokonce	k9
praxeologii	praxeologie	k1gFnSc4
označil	označit	k5eAaPmAgInS
za	za	k7c4
metodu	metoda	k1gFnSc4
zkoumání	zkoumání	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Mises	Mises	k1gInSc1
ji	on	k3xPp3gFnSc4
považoval	považovat	k5eAaImAgInS
za	za	k7c4
ucelený	ucelený	k2eAgInSc4d1
komplex	komplex	k1gInSc4
teoretické	teoretický	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
o	o	k7c6
lidském	lidský	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
praxeologie	praxeologie	k1gFnSc1
je	být	k5eAaImIp3nS
jak	jak	k6eAd1
metoda	metoda	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
teorie	teorie	k1gFnSc1
na	na	k7c6
ní	on	k3xPp3gFnSc6
vybudované	vybudovaný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Praxeologie	Praxeologie	k1gFnSc1
je	být	k5eAaImIp3nS
axiomatickou-deduktivní	axiomatickou-deduktivní	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
apriorní	apriorní	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
nikoli	nikoli	k9
vědu	věda	k1gFnSc4
empirickou	empirický	k2eAgFnSc4d1
<g/>
,	,	kIx,
jakou	jaký	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
jsou	být	k5eAaImIp3nP
přírodní	přírodní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
plyne	plynout	k5eAaImIp3nS
z	z	k7c2
objevu	objev	k1gInSc2
konceptu	koncept	k1gInSc2
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
ukázal	ukázat	k5eAaPmAgInS
Mises	Mises	k1gInSc1
<g/>
,	,	kIx,
nabývá	nabývat	k5eAaImIp3nS
hodnoty	hodnota	k1gFnPc4
axiomu	axiom	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
nevyvratitelný	vyvratitelný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apriorismus	apriorismus	k1gInSc1
praxeologie	praxeologie	k1gFnSc2
je	být	k5eAaImIp3nS
však	však	k9
diametrálně	diametrálně	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
od	od	k7c2
apriorismu	apriorismus	k1gInSc2
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matematika	matematika	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
volbě	volba	k1gFnSc6
určitých	určitý	k2eAgInPc2d1
axiomů	axiom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praxeologie	Praxeologie	k1gFnSc1
je	být	k5eAaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
jednom	jeden	k4xCgInSc6
samozřejmém	samozřejmý	k2eAgInSc6d1
axiomu	axiom	k1gInSc6
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
nutně	nutně	k6eAd1
obsažen	obsažen	k2eAgMnSc1d1
v	v	k7c6
každé	každý	k3xTgFnSc6
lidské	lidský	k2eAgFnSc6d1
mysli	mysl	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
axiom	axiom	k1gInSc4
v	v	k7c6
sobě	sebe	k3xPyFc6
zahrnuje	zahrnovat	k5eAaImIp3nS
charakteristické	charakteristický	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
kategorie	kategorie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnSc7
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
odhalování	odhalování	k1gNnSc1
za	za	k7c7
pomocí	pomoc	k1gFnSc7
deduktivní	deduktivní	k2eAgFnSc2d1
logiky	logika	k1gFnSc2
je	být	k5eAaImIp3nS
náplní	náplň	k1gFnSc7
praxeologie	praxeologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
vytváření	vytváření	k1gNnSc1
dalších	další	k2eAgNnPc2d1
tvrzení	tvrzení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
musejí	muset	k5eAaImIp3nP
být	být	k5eAaImF
apriori	apriori	k6eAd1
pravdivá	pravdivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatou	podstata	k1gFnSc7
praxeologie	praxeologie	k1gFnSc2
je	být	k5eAaImIp3nS
tak	tak	k9
objevování	objevování	k1gNnSc1
kauzálních	kauzální	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
v	v	k7c6
teleologickém	teleologický	k2eAgInSc6d1
sledu	sled	k1gInSc6
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
praxeologie	praxeologie	k1gFnSc2
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
imaginárních	imaginární	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
dedukce	dedukce	k1gFnPc1
z	z	k7c2
fundamentální	fundamentální	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
jednání	jednání	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
abstrahování	abstrahování	k1gNnSc3
od	od	k7c2
určitých	určitý	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
přítomných	přítomný	k2eAgFnPc2d1
v	v	k7c6
reálném	reálný	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
například	například	k6eAd1
docházíme	docházet	k5eAaImIp1nP
k	k	k7c3
objevu	objev	k1gInSc3
podstaty	podstata	k1gFnSc2
zisku	zisk	k1gInSc2
a	a	k8xC
ztráty	ztráta	k1gFnSc2
na	na	k7c6
základě	základ	k1gInSc6
modelu	model	k1gInSc2
donekonečna	donekonečna	k6eAd1
se	se	k3xPyFc4
opakujících	opakující	k2eAgInPc2d1
ekonomických	ekonomický	k2eAgInPc2d1
procesů	proces	k1gInPc2
(	(	kIx(
<g/>
evenly	evenl	k1gInPc4
rotating	rotating	k1gInSc1
economy	econom	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
podstatou	podstata	k1gFnSc7
je	být	k5eAaImIp3nS
abstrahování	abstrahování	k1gNnSc1
od	od	k7c2
toku	tok	k1gInSc2
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
však	však	k9
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
nikdy	nikdy	k6eAd1
aplikována	aplikovat	k5eAaBmNgFnS
na	na	k7c4
realitu	realita	k1gFnSc4
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
pouze	pouze	k6eAd1
k	k	k7c3
objasnění	objasnění	k1gNnSc3
jednoho	jeden	k4xCgInSc2
ekonomického	ekonomický	k2eAgInSc2d1
problému	problém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
z	z	k7c2
podstaty	podstata	k1gFnSc2
nerealistická	realistický	k2eNgFnSc1d1
a	a	k8xC
umělá	umělý	k2eAgFnSc1d1
a	a	k8xC
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
za	za	k7c7
konkrétním	konkrétní	k2eAgInSc7d1
účelem	účel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
fakt	fakt	k1gInSc4
ekonom	ekonom	k1gMnSc1
nikdy	nikdy	k6eAd1
nesmí	smět	k5eNaImIp3nS
ztratit	ztratit	k5eAaPmF
ze	z	k7c2
zřetele	zřetel	k1gInSc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
jeho	jeho	k3xOp3gNnSc4
zkoumání	zkoumání	k1gNnSc4
k	k	k7c3
zásadním	zásadní	k2eAgInPc3d1
omylům	omyl	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Praxeologie	Praxeologie	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
alternativou	alternativa	k1gFnSc7
k	k	k7c3
metodologii	metodologie	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
používá	používat	k5eAaImIp3nS
ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tou	ten	k3xDgFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
metodologický	metodologický	k2eAgInSc1d1
pozitivismus	pozitivismus	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
největším	veliký	k2eAgMnSc7d3
obhájcem	obhájce	k1gMnSc7
je	být	k5eAaImIp3nS
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
Milton	Milton	k1gInSc4
Friedman	Friedman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozitivismus	pozitivismus	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c4
konstruování	konstruování	k1gNnSc4
hypotéz	hypotéza	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
musejí	muset	k5eAaImIp3nP
být	být	k5eAaImF
následně	následně	k6eAd1
testovány	testovat	k5eAaImNgInP
na	na	k7c6
skutečných	skutečný	k2eAgInPc6d1
jevech	jev	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praxeologie	Praxeologie	k1gFnSc1
takové	takový	k3xDgNnSc4
testování	testování	k1gNnSc4
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
její	její	k3xOp3gNnPc1
tvrzení	tvrzení	k1gNnPc1
nabývají	nabývat	k5eAaImIp3nP
jinou	jiný	k2eAgFnSc4d1
pravdivostní	pravdivostní	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
než	než	k8xS
výroky	výrok	k1gInPc4
pozitivistické	pozitivistický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
jednání	jednání	k1gNnSc2
různých	různý	k2eAgMnPc2d1
jednotlivců	jednotlivec	k1gMnPc2
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
komplexní	komplexní	k2eAgInPc1d1
jevy	jev	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pouhého	pouhý	k2eAgNnSc2d1
pozorování	pozorování	k1gNnSc2
těchto	tento	k3xDgInPc2
jevů	jev	k1gInPc2
nemůžeme	moct	k5eNaImIp1nP
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
faktory	faktor	k1gInPc1
je	on	k3xPp3gNnSc4
způsobily	způsobit	k5eAaPmAgFnP
<g/>
,	,	kIx,
protože	protože	k8xS
jsou	být	k5eAaImIp3nP
výsledkem	výsledek	k1gInSc7
působení	působení	k1gNnSc4
mnoha	mnoho	k4c2
různých	různý	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
různě	různě	k6eAd1
a	a	k8xC
stejný	stejný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
různě	různě	k6eAd1
v	v	k7c6
různém	různý	k2eAgInSc6d1
časovém	časový	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomie	ekonomie	k1gFnSc1
nedisponuje	disponovat	k5eNaBmIp3nS
laboratorními	laboratorní	k2eAgInPc7d1
pokusy	pokus	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
by	by	kYmCp3nP
umožnily	umožnit	k5eAaPmAgInP
izolovat	izolovat	k5eAaBmF
působení	působení	k1gNnSc2
jiných	jiný	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
a	a	k8xC
zjistit	zjistit	k5eAaPmF
tak	tak	k9
přínos	přínos	k1gInSc4
jednotlivých	jednotlivý	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
na	na	k7c4
výsledný	výsledný	k2eAgInSc4d1
jev	jev	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
vracíme	vracet	k5eAaImIp1nP
ke	k	k7c3
specifické	specifický	k2eAgFnSc3d1
metodě	metoda	k1gFnSc3
praxeologie	praxeologie	k1gFnSc2
<g/>
,	,	kIx,
metodě	metoda	k1gFnSc3
spekulativních	spekulativní	k2eAgFnPc2d1
konstrukcí	konstrukce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
tuto	tento	k3xDgFnSc4
metodologii	metodologie	k1gFnSc4
nepřijímá	přijímat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
mezi	mezi	k7c7
„	„	k?
<g/>
Rakušany	Rakušan	k1gMnPc7
<g/>
“	“	k?
nebyla	být	k5eNaImAgFnS
tato	tento	k3xDgFnSc1
metodologie	metodologie	k1gFnSc1
vždy	vždy	k6eAd1
důsledně	důsledně	k6eAd1
uplatňována	uplatňován	k2eAgFnSc1d1
a	a	k8xC
mnohými	mnohý	k2eAgMnPc7d1
byla	být	k5eAaImAgFnS
opuštěna	opuštěn	k2eAgNnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
Hayekem	Hayek	k1gInSc7
nebo	nebo	k8xC
Reismanem	Reisman	k1gInSc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
o	o	k7c4
jakousi	jakýsi	k3yIgFnSc4
syntézu	syntéza	k1gFnSc4
rakouské	rakouský	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
praxeologie	praxeologie	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
pozitivistické	pozitivistický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
ekonomie	ekonomie	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
</s>
<s>
Murray	Murraa	k1gFnPc1
Rothbard	Rothbarda	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
</s>
