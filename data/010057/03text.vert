<p>
<s>
Galatský	Galatský	k2eAgInSc1d1	Galatský
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Galata	Galat	k2eAgFnSc1d1	Galata
Köprüsü	Köprüsü	k1gFnSc1	Köprüsü
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Zlatý	zlatý	k2eAgInSc4d1	zlatý
roh	roh	k1gInSc4	roh
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
čtvrti	čtvrt	k1gFnPc4	čtvrt
Galata	Galat	k1gMnSc2	Galat
a	a	k8xC	a
Eminonü	Eminonü	k1gMnSc2	Eminonü
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
490	[number]	k4	490
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc4d1	široký
42	[number]	k4	42
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
patra	patro	k1gNnPc4	patro
<g/>
,	,	kIx,	,
po	po	k7c6	po
horním	horní	k2eAgInSc6d1	horní
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnPc1	silnice
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
tvoří	tvořit	k5eAaImIp3nS	tvořit
promenáda	promenáda	k1gFnSc1	promenáda
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
restauracemi	restaurace	k1gFnPc7	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
zvedací	zvedací	k2eAgMnSc1d1	zvedací
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
lodě	loď	k1gFnPc1	loď
z	z	k7c2	z
Bosporu	Bospor	k1gInSc2	Bospor
vplouvat	vplouvat	k5eAaImF	vplouvat
do	do	k7c2	do
Zlatého	zlatý	k2eAgInSc2d1	zlatý
rohu	roh	k1gInSc2	roh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
most	most	k1gInSc1	most
přes	přes	k7c4	přes
zlatý	zlatý	k2eAgInSc4d1	zlatý
roh	roh	k1gInSc4	roh
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
císař	císař	k1gMnSc1	císař
Justinián	Justinián	k1gMnSc1	Justinián
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
vypsal	vypsat	k5eAaPmAgMnS	vypsat
sultán	sultán	k1gMnSc1	sultán
Bajezid	Bajezida	k1gFnPc2	Bajezida
II	II	kA	II
<g/>
.	.	kIx.	.
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
nového	nový	k2eAgInSc2d1	nový
mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
i	i	k9	i
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
a	a	k8xC	a
Michelangelo	Michelangela	k1gFnSc5	Michelangela
Buonarotti	Buonarott	k2eAgMnPc1d1	Buonarott
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
realizace	realizace	k1gFnSc2	realizace
návrhů	návrh	k1gInPc2	návrh
nakonec	nakonec	k6eAd1	nakonec
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
druhý	druhý	k4xOgInSc4	druhý
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
železný	železný	k2eAgInSc1d1	železný
most	most	k1gInSc1	most
postavený	postavený	k2eAgInSc1d1	postavený
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
staviteli	stavitel	k1gMnPc7	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
most	most	k1gInSc4	most
postavili	postavit	k5eAaPmAgMnP	postavit
Němci	Němec	k1gMnPc1	Němec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
poničen	poničit	k5eAaPmNgInS	poničit
požárem	požár	k1gInSc7	požár
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
mostu	most	k1gInSc2	most
současného	současný	k2eAgInSc2d1	současný
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
domácí	domácí	k2eAgFnSc1d1	domácí
firma	firma	k1gFnSc1	firma
STFA	STFA	kA	STFA
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Galatský	Galatský	k2eAgInSc4d1	Galatský
most	most	k1gInSc4	most
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.allaboutturkey.com/galata.htm	[url]	k6eAd1	http://www.allaboutturkey.com/galata.htm
</s>
</p>
<p>
<s>
http://structurae.net/structures/data/index.cfm?ID=s0005859	[url]	k4	http://structurae.net/structures/data/index.cfm?ID=s0005859
</s>
</p>
<p>
<s>
http://turecko.svetadily.cz/clanky/Zlaty-roh-istanbulska-zatoka	[url]	k6eAd1	http://turecko.svetadily.cz/clanky/Zlaty-roh-istanbulska-zatoka
</s>
</p>
