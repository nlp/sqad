<s>
Letní	letní	k2eAgInSc1d1	letní
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
je	být	k5eAaImIp3nS	být
asterismus	asterismus	k1gInSc1	asterismus
tvořený	tvořený	k2eAgInSc1d1	tvořený
hvězdami	hvězda	k1gFnPc7	hvězda
Deneb	Denba	k1gFnPc2	Denba
(	(	kIx(	(
<g/>
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Labutě	labuť	k1gFnSc2	labuť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vega	Vega	k1gMnSc1	Vega
(	(	kIx(	(
<g/>
v	v	k7c6	v
Lyře	lyra	k1gFnSc6	lyra
<g/>
)	)	kIx)	)
a	a	k8xC	a
Altair	Altair	k1gMnSc1	Altair
(	(	kIx(	(
<g/>
v	v	k7c6	v
Orlu	Orel	k1gMnSc6	Orel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnSc1d1	letní
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
hvězdy	hvězda	k1gFnPc4	hvězda
tvořící	tvořící	k2eAgInSc4d1	tvořící
letní	letní	k2eAgInSc4d1	letní
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
57	[number]	k4	57
tradičních	tradiční	k2eAgFnPc2d1	tradiční
navigačních	navigační	k2eAgFnPc2d1	navigační
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nejjasnější	jasný	k2eAgFnSc7d3	nejjasnější
hvězdou	hvězda	k1gFnSc7	hvězda
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Vega	Vega	k1gFnSc1	Vega
(	(	kIx(	(
<g/>
0,03	[number]	k4	0,03
mag	mag	k?	mag
<g/>
,	,	kIx,	,
nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
po	po	k7c4	po
Arcturu	Arctura	k1gFnSc4	Arctura
druhá	druhý	k4xOgFnSc1	druhý
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
severní	severní	k2eAgFnSc2d1	severní
nebeské	nebeský	k2eAgFnSc2d1	nebeská
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
25	[number]	k4	25
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
slabší	slabý	k2eAgMnSc1d2	slabší
je	být	k5eAaImIp3nS	být
Altair	Altair	k1gInSc1	Altair
(	(	kIx(	(
<g/>
0,77	[number]	k4	0,77
mag	mag	k?	mag
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
16,7	[number]	k4	16,7
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejslabší	slabý	k2eAgFnSc7d3	nejslabší
hvězdou	hvězda	k1gFnSc7	hvězda
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
trojice	trojice	k1gFnSc2	trojice
je	být	k5eAaImIp3nS	být
Deneb	Denba	k1gFnPc2	Denba
(	(	kIx(	(
<g/>
1,25	[number]	k4	1,25
mag	mag	k?	mag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
dvou	dva	k4xCgFnPc2	dva
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
Deneb	Denba	k1gFnPc2	Denba
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
opravdu	opravdu	k6eAd1	opravdu
daleko	daleko	k6eAd1	daleko
–	–	k?	–
asi	asi	k9	asi
1	[number]	k4	1
500	[number]	k4	500
světelných	světelný	k2eAgInPc2d1	světelný
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
bílý	bílý	k1gMnSc1	bílý
veleobr	veleobr	k1gMnSc1	veleobr
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejzářivějším	zářivý	k2eAgFnPc3d3	nejzářivější
hvězdám	hvězda	k1gFnPc3	hvězda
oblohy	obloha	k1gFnSc2	obloha
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
80	[number]	k4	80
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
stejně	stejně	k6eAd1	stejně
daleko	daleko	k6eAd1	daleko
jako	jako	k8xS	jako
Vega	Vega	k1gMnSc1	Vega
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
by	by	k9	by
16	[number]	k4	16
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
planeta	planeta	k1gFnSc1	planeta
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k8xC	i
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Jarní	jarní	k2eAgInSc1d1	jarní
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
Zimní	zimní	k2eAgInSc1d1	zimní
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
Zimní	zimní	k2eAgInSc4d1	zimní
šestiúhelník	šestiúhelník	k1gInSc4	šestiúhelník
</s>
