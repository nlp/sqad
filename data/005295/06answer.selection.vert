<s>
Letní	letní	k2eAgInSc1d1	letní
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
je	být	k5eAaImIp3nS	být
asterismus	asterismus	k1gInSc1	asterismus
tvořený	tvořený	k2eAgInSc1d1	tvořený
hvězdami	hvězda	k1gFnPc7	hvězda
Deneb	Denba	k1gFnPc2	Denba
(	(	kIx(	(
<g/>
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Labutě	labuť	k1gFnSc2	labuť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vega	Vega	k1gMnSc1	Vega
(	(	kIx(	(
<g/>
v	v	k7c6	v
Lyře	lyra	k1gFnSc6	lyra
<g/>
)	)	kIx)	)
a	a	k8xC	a
Altair	Altair	k1gMnSc1	Altair
(	(	kIx(	(
<g/>
v	v	k7c6	v
Orlu	Orel	k1gMnSc6	Orel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
