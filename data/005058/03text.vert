<s>
Aeneis	Aeneis	k1gFnSc1	Aeneis
<g/>
,	,	kIx,	,
vyslov	vyslovit	k5eAaPmRp2nS	vyslovit
énéjis	énéjis	k1gInSc1	énéjis
(	(	kIx(	(
<g/>
genitiv	genitiv	k1gInSc1	genitiv
Aeneidy	Aeneida	k1gFnSc2	Aeneida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
Aeneida	Aeneida	k1gFnSc1	Aeneida
je	být	k5eAaImIp3nS	být
Vergiliův	Vergiliův	k2eAgInSc4d1	Vergiliův
epos	epos	k1gInSc4	epos
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
latinsky	latinsky	k6eAd1	latinsky
v	v	k7c6	v
hexametrech	hexametr	k1gInPc6	hexametr
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
v	v	k7c6	v
letech	let	k1gInPc6	let
29	[number]	k4	29
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
jako	jako	k8xC	jako
poslední	poslední	k2eAgNnSc4d1	poslední
básníkovo	básníkův	k2eAgNnSc4d1	básníkovo
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
metrické	metrický	k2eAgInPc1d1	metrický
a	a	k8xC	a
dějové	dějový	k2eAgInPc1d1	dějový
nedostatky	nedostatek	k1gInPc1	nedostatek
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
nedokončenosti	nedokončenost	k1gFnSc6	nedokončenost
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
9896	[number]	k4	9896
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Aeneis	Aeneis	k1gFnSc1	Aeneis
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
homérskými	homérský	k2eAgInPc7d1	homérský
eposy	epos	k1gInPc7	epos
<g/>
,	,	kIx,	,
Íliadou	Íliada	k1gFnSc7	Íliada
a	a	k8xC	a
Odysseiou	Odysseia	k1gFnSc7	Odysseia
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
do	do	k7c2	do
stylu	styl	k1gInSc2	styl
i	i	k8xC	i
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
šest	šest	k4xCc1	šest
knih	kniha	k1gFnPc2	kniha
eposu	epos	k1gInSc2	epos
líčí	líčit	k5eAaImIp3nS	líčit
příběh	příběh	k1gInSc1	příběh
bájného	bájný	k2eAgMnSc2d1	bájný
hrdiny	hrdina	k1gMnSc2	hrdina
Aenea	Aeneas	k1gMnSc2	Aeneas
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
hořící	hořící	k2eAgFnSc2d1	hořící
Tróje	Trója	k1gFnSc2	Trója
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tak	tak	k6eAd1	tak
paralelu	paralela	k1gFnSc4	paralela
k	k	k7c3	k
příběhu	příběh	k1gInSc3	příběh
Odysseie	Odysseie	k1gFnSc2	Odysseie
<g/>
;	;	kIx,	;
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
pak	pak	k6eAd1	pak
popisuje	popisovat	k5eAaImIp3nS	popisovat
příchod	příchod	k1gInSc1	příchod
Aenea	Aeneas	k1gMnSc2	Aeneas
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
druhů	druh	k1gInPc2	druh
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
boje	boj	k1gInSc2	boj
s	s	k7c7	s
italskými	italský	k2eAgInPc7d1	italský
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
paralelou	paralela	k1gFnSc7	paralela
k	k	k7c3	k
vyprávění	vyprávění	k1gNnSc3	vyprávění
Íliady	Íliada	k1gFnSc2	Íliada
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
epizod	epizoda	k1gFnPc2	epizoda
a	a	k8xC	a
vedlejších	vedlejší	k2eAgNnPc2d1	vedlejší
vyprávění	vyprávění	k1gNnPc2	vyprávění
(	(	kIx(	(
<g/>
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
kartáginské	kartáginský	k2eAgFnSc6d1	kartáginská
královně	královna	k1gFnSc6	královna
Didoně	Didoň	k1gFnSc2	Didoň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Aenea	Aeneas	k1gMnSc2	Aeneas
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
statečné	statečný	k2eAgFnSc6d1	statečná
Amazonce	Amazonka	k1gFnSc6	Amazonka
Camille	Camille	k1gFnSc2	Camille
či	či	k8xC	či
Aeneovo	Aeneův	k2eAgNnSc1d1	Aeneův
retrospektivní	retrospektivní	k2eAgNnSc1d1	retrospektivní
vyprávění	vyprávění	k1gNnSc1	vyprávění
o	o	k7c6	o
trojské	trojský	k2eAgFnSc6d1	Trojská
válce	válka	k1gFnSc6	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
<g/>
,	,	kIx,	,
pojímaný	pojímaný	k2eAgMnSc1d1	pojímaný
jako	jako	k8xC	jako
praotec	praotec	k1gMnSc1	praotec
římského	římský	k2eAgInSc2d1	římský
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
a	a	k8xC	a
vzor	vzor	k1gInSc4	vzor
římských	římský	k2eAgFnPc2d1	římská
ctností	ctnost	k1gFnPc2	ctnost
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
eposu	epos	k1gInSc2	epos
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
národní	národní	k2eAgInSc4d1	národní
epos	epos	k1gInSc4	epos
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
srovnatelný	srovnatelný	k2eAgMnSc1d1	srovnatelný
s	s	k7c7	s
Homérem	Homér	k1gMnSc7	Homér
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
měl	mít	k5eAaImAgInS	mít
epos	epos	k1gInSc1	epos
funkci	funkce	k1gFnSc4	funkce
didaktickou	didaktický	k2eAgFnSc4d1	didaktická
jako	jako	k8xS	jako
poučení	poučení	k1gNnSc1	poučení
o	o	k7c6	o
"	"	kIx"	"
<g/>
římských	římský	k2eAgFnPc6d1	římská
ctnostech	ctnost	k1gFnPc6	ctnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
jinotajnou	jinotajný	k2eAgFnSc4d1	jinotajná
funkci	funkce	k1gFnSc4	funkce
politickou	politický	k2eAgFnSc4d1	politická
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ideologicky	ideologicky	k6eAd1	ideologicky
podpořil	podpořit	k5eAaPmAgInS	podpořit
politiku	politika	k1gFnSc4	politika
císaře	císař	k1gMnSc2	císař
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
údajného	údajný	k2eAgMnSc2d1	údajný
potomka	potomek	k1gMnSc2	potomek
bájného	bájný	k2eAgMnSc2d1	bájný
Aenea	Aeneas	k1gMnSc2	Aeneas
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
s	s	k7c7	s
Trójany	Trójan	k1gMnPc7	Trójan
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedorazí	dorazit	k5eNaPmIp3nS	dorazit
k	k	k7c3	k
jejím	její	k3xOp3gInPc3	její
břehům	břeh	k1gInPc3	břeh
kvůli	kvůli	k7c3	kvůli
větrné	větrný	k2eAgFnSc3d1	větrná
bouři	bouř	k1gFnSc3	bouř
způsobené	způsobený	k2eAgFnSc2d1	způsobená
pánem	pán	k1gMnSc7	pán
větrů	vítr	k1gInPc2	vítr
Aeolem	Aeolus	k1gMnSc7	Aeolus
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
požádala	požádat	k5eAaPmAgFnS	požádat
bohyně	bohyně	k1gFnSc1	bohyně
Juno	Juno	k1gFnSc1	Juno
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
jsou	být	k5eAaImIp3nP	být
nuceny	nutit	k5eAaImNgFnP	nutit
přistát	přistát	k5eAaImF	přistát
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
Libye	Libye	k1gFnSc2	Libye
<g/>
.	.	kIx.	.
</s>
<s>
Aeneova	Aeneův	k2eAgFnSc1d1	Aeneův
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
bohyně	bohyně	k1gFnSc1	bohyně
lásky	láska	k1gFnSc2	láska
Venuše	Venuše	k1gFnSc2	Venuše
přichází	přicházet	k5eAaImIp3nS	přicházet
za	za	k7c7	za
Jupiterem	Jupiter	k1gMnSc7	Jupiter
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
prosila	prosit	k5eAaImAgFnS	prosit
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
pro	pro	k7c4	pro
Aenea	Aeneas	k1gMnSc4	Aeneas
<g/>
.	.	kIx.	.
</s>
<s>
Vládce	vládce	k1gMnSc1	vládce
bohů	bůh	k1gMnPc2	bůh
jí	jíst	k5eAaImIp3nS	jíst
vyhoví	vyhovit	k5eAaPmIp3nS	vyhovit
<g/>
,	,	kIx,	,
odhalí	odhalit	k5eAaPmIp3nP	odhalit
jí	on	k3xPp3gFnSc3	on
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
směrem	směr	k1gInSc7	směr
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
ubírat	ubírat	k5eAaImF	ubírat
hrdinův	hrdinův	k2eAgInSc4d1	hrdinův
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vydává	vydávat	k5eAaPmIp3nS	vydávat
za	za	k7c7	za
Aeneem	Aeneas	k1gMnSc7	Aeneas
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mladé	mladý	k2eAgFnSc2d1	mladá
lovkyně	lovkyně	k1gFnSc2	lovkyně
<g/>
,	,	kIx,	,
převypráví	převyprávět	k5eAaPmIp3nS	převyprávět
mu	on	k3xPp3gMnSc3	on
pohnutou	pohnutý	k2eAgFnSc4d1	pohnutá
historii	historie	k1gFnSc4	historie
Didony	Didona	k1gFnSc2	Didona
<g/>
,	,	kIx,	,
vládkyně	vládkyně	k1gFnSc1	vládkyně
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Dido	Dido	k6eAd1	Dido
žila	žít	k5eAaImAgFnS	žít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mužem	muž	k1gMnSc7	muž
Sycheem	Sycheus	k1gMnSc7	Sycheus
ve	v	k7c6	v
přímořském	přímořský	k2eAgNnSc6d1	přímořské
městě	město	k1gNnSc6	město
Tyru	Tyrus	k1gInSc2	Tyrus
ve	v	k7c6	v
Foinikii	Foinikie	k1gFnSc6	Foinikie
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
její	její	k3xOp3gMnSc1	její
bratr	bratr	k1gMnSc1	bratr
Pygmalion	Pygmalion	k1gInSc4	Pygmalion
nezabil	zabít	k5eNaPmAgMnS	zabít
Sychea	Sychea	k1gMnSc1	Sychea
a	a	k8xC	a
nezmoncnil	zmoncnit	k5eNaPmAgInS	zmoncnit
se	se	k3xPyFc4	se
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dido	Dido	k6eAd1	Dido
varovaná	varovaný	k2eAgFnSc1d1	varovaná
přízrakem	přízrak	k1gInSc7	přízrak
zabitého	zabitý	k2eAgInSc2d1	zabitý
Sychea	Syche	k1gInSc2	Syche
<g/>
,	,	kIx,	,
prchá	prchat	k5eAaImIp3nS	prchat
Tyru	Tyra	k1gFnSc4	Tyra
a	a	k8xC	a
kupuje	kupovat	k5eAaImIp3nS	kupovat
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
nyní	nyní	k6eAd1	nyní
buduje	budovat	k5eAaImIp3nS	budovat
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
zasvěceno	zasvětit	k5eAaPmNgNnS	zasvětit
bohyni	bohyně	k1gFnSc4	bohyně
Iuno	Iuno	k1gFnSc2	Iuno
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
do	do	k7c2	do
samotného	samotný	k2eAgNnSc2d1	samotné
Kartága	Kartágo	k1gNnSc2	Kartágo
nepozorovaně	pozorovaně	k6eNd1	pozorovaně
<g/>
,	,	kIx,	,
obestřen	obestřen	k2eAgInSc1d1	obestřen
mlhou	mlha	k1gFnSc7	mlha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
neviditelnost	neviditelnost	k1gFnSc4	neviditelnost
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
královninu	královnin	k2eAgInSc3d1	královnin
paláci	palác	k1gInSc3	palác
Aeneas	Aeneas	k1gMnSc1	Aeneas
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
výstavbu	výstavba	k1gFnSc4	výstavba
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
již	již	k6eAd1	již
dorazí	dorazit	k5eAaPmIp3nP	dorazit
část	část	k1gFnSc4	část
Trójanů	Trójan	k1gMnPc2	Trójan
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
u	u	k7c2	u
Didony	Didona	k1gFnSc2	Didona
slyšení	slyšení	k1gNnSc2	slyšení
<g/>
,	,	kIx,	,
převyprávějí	převyprávět	k5eAaPmIp3nP	převyprávět
své	svůj	k3xOyFgFnPc4	svůj
útrapy	útrapa	k1gFnPc4	útrapa
<g/>
.	.	kIx.	.
</s>
<s>
Aneas	Aneas	k1gInSc1	Aneas
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
z	z	k7c2	z
mlhy	mlha	k1gFnSc2	mlha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
halila	halit	k5eAaImAgFnS	halit
a	a	k8xC	a
promluví	promluvit	k5eAaPmIp3nS	promluvit
ke	k	k7c3	k
královně	královna	k1gFnSc3	královna
<g/>
.	.	kIx.	.
</s>
<s>
Dido	Dido	k6eAd1	Dido
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
Trójany	Trójan	k1gMnPc7	Trójan
soucit	soucit	k1gInSc4	soucit
a	a	k8xC	a
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
jejich	jejich	k3xOp3gFnSc2	jejich
záchrany	záchrana	k1gFnSc2	záchrana
uspořádá	uspořádat	k5eAaPmIp3nS	uspořádat
hostinu	hostina	k1gFnSc4	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
sesílá	sesílat	k5eAaImIp3nS	sesílat
bůžka	bůžek	k1gMnSc4	bůžek
lásky	láska	k1gFnSc2	láska
Kupida	Kupida	k1gFnSc1	Kupida
s	s	k7c7	s
podobou	podoba	k1gFnSc7	podoba
Aeneova	Aeneův	k2eAgMnSc2d1	Aeneův
malého	malý	k1gMnSc2	malý
syna	syn	k1gMnSc4	syn
Iula	Iulus	k1gMnSc2	Iulus
Askania	Askanium	k1gNnSc2	Askanium
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznítí	vznítit	k5eAaPmIp3nS	vznítit
v	v	k7c6	v
Didoně	Didona	k1gFnSc6	Didona
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
Aeneovi	Aeneas	k1gMnSc3	Aeneas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
hostiny	hostina	k1gFnSc2	hostina
Dido	Dido	k6eAd1	Dido
žádá	žádat	k5eAaImIp3nS	žádat
Aenea	Aeneas	k1gMnSc4	Aeneas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
vypravoval	vypravovat	k5eAaImAgInS	vypravovat
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
od	od	k7c2	od
vyvrácení	vyvrácení	k1gNnSc2	vyvrácení
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
s	s	k7c7	s
bolestí	bolest	k1gFnSc7	bolest
líčí	líčit	k5eAaImIp3nS	líčit
pád	pád	k1gInSc1	pád
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
postavili	postavit	k5eAaPmAgMnP	postavit
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
Tróje	Trója	k1gFnSc2	Trója
dřevěného	dřevěný	k2eAgMnSc4d1	dřevěný
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jehož	k3xOyRp3gFnPc2	jehož
útrob	útroba	k1gFnPc2	útroba
se	se	k3xPyFc4	se
ukryjí	ukrýt	k5eAaPmIp3nP	ukrýt
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
vojsk	vojsko	k1gNnPc2	vojsko
předstírá	předstírat	k5eAaImIp3nS	předstírat
ústup	ústup	k1gInSc4	ústup
a	a	k8xC	a
odpluje	odplout	k5eAaPmIp3nS	odplout
na	na	k7c4	na
blízký	blízký	k2eAgInSc4d1	blízký
ostrov	ostrov	k1gInSc4	ostrov
Tenedos	Tenedosa	k1gFnPc2	Tenedosa
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaPmIp3nP	vydávat
před	před	k7c4	před
hradby	hradba	k1gFnPc4	hradba
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
na	na	k7c4	na
dřevěného	dřevěný	k2eAgMnSc4d1	dřevěný
koně	kůň	k1gMnSc4	kůň
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nS	různit
<g/>
,	,	kIx,	,
Láokoón	Láokoón	k1gMnSc1	Láokoón
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
boha	bůh	k1gMnSc2	bůh
moří	mořit	k5eAaImIp3nS	mořit
Neptuna	Neptun	k1gMnSc4	Neptun
<g/>
,	,	kIx,	,
varuje	varovat	k5eAaImIp3nS	varovat
Trójany	Trójan	k1gMnPc4	Trójan
před	před	k7c7	před
možnou	možný	k2eAgFnSc7d1	možná
lstí	lest	k1gFnSc7	lest
a	a	k8xC	a
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
oštěp	oštěp	k1gInSc4	oštěp
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
zabodne	zabodnout	k5eAaPmIp3nS	zabodnout
do	do	k7c2	do
koně	kůň	k1gMnSc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Sinón	Sinón	k1gMnSc1	Sinón
<g/>
,	,	kIx,	,
zrádný	zrádný	k2eAgMnSc1d1	zrádný
voják	voják	k1gMnSc1	voják
z	z	k7c2	z
řeckých	řecký	k2eAgFnPc2d1	řecká
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
od	od	k7c2	od
Řeků	Řek	k1gMnPc2	Řek
svázat	svázat	k5eAaPmF	svázat
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
při	při	k7c6	při
při	pře	k1gFnSc6	pře
přemlouvání	přemlouvání	k1gNnSc2	přemlouvání
na	na	k7c4	na
Trójany	Trójan	k1gMnPc4	Trójan
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
<g/>
.	.	kIx.	.
</s>
<s>
Priamos	Priamos	k1gMnSc1	Priamos
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Tróje	Trója	k1gFnSc2	Trója
mu	on	k3xPp3gMnSc3	on
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
jej	on	k3xPp3gMnSc4	on
osvobodit	osvobodit	k5eAaPmF	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Láokoón	Láokoón	k1gInSc1	Láokoón
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
obětovat	obětovat	k5eAaBmF	obětovat
Neptunovi	Neptun	k1gMnSc3	Neptun
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
i	i	k9	i
jeho	jeho	k3xOp3gMnPc4	jeho
syny	syn	k1gMnPc4	syn
napadnou	napadnout	k5eAaPmIp3nP	napadnout
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
dva	dva	k4xCgMnPc1	dva
hadi	had	k1gMnPc1	had
a	a	k8xC	a
zabijí	zabít	k5eAaPmIp3nP	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
to	ten	k3xDgNnSc4	ten
chápou	chápat	k5eAaImIp3nP	chápat
jako	jako	k9	jako
trest	trest	k1gInSc4	trest
za	za	k7c4	za
vrhnutí	vrhnutí	k1gNnSc4	vrhnutí
oštěpu	oštěp	k1gInSc2	oštěp
a	a	k8xC	a
koně	kůň	k1gMnPc1	kůň
vtahují	vtahovat	k5eAaImIp3nP	vtahovat
za	za	k7c4	za
hradby	hradba	k1gFnPc4	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
vyplouvají	vyplouvat	k5eAaImIp3nP	vyplouvat
z	z	k7c2	z
Tenedu	Teneda	k1gMnSc4	Teneda
<g/>
,	,	kIx,	,
Sinón	Sinón	k1gMnSc1	Sinón
otvírá	otvírat	k5eAaImIp3nS	otvírat
městské	městský	k2eAgFnPc4d1	městská
brány	brána	k1gFnPc4	brána
<g/>
,	,	kIx,	,
z	z	k7c2	z
koně	kůň	k1gMnSc2	kůň
vycházejí	vycházet	k5eAaImIp3nP	vycházet
vojáci	voják	k1gMnPc1	voják
a	a	k8xC	a
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
stáže	stáž	k1gFnPc4	stáž
<g/>
.	.	kIx.	.
</s>
<s>
Aeneovi	Aeneas	k1gMnSc3	Aeneas
se	se	k3xPyFc4	se
zjevil	zjevit	k5eAaPmAgMnS	zjevit
duch	duch	k1gMnSc1	duch
Hektóra	Hektóra	k1gMnSc1	Hektóra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
vyzval	vyzvat	k5eAaPmAgInS	vyzvat
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
z	z	k7c2	z
Tróji	Trója	k1gFnSc6	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Aneas	Aneas	k1gInSc1	Aneas
se	se	k3xPyFc4	se
chopí	chopit	k5eAaPmIp3nS	chopit
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
od	od	k7c2	od
Apollonova	Apollonův	k2eAgMnSc2d1	Apollonův
kněze	kněz	k1gMnSc2	kněz
Panthúse	Panthús	k1gMnSc2	Panthús
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastává	nastávat	k5eAaImIp3nS	nastávat
poslední	poslední	k2eAgFnSc1d1	poslední
hodina	hodina	k1gFnSc1	hodina
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
sesbírá	sesbírat	k5eAaPmIp3nS	sesbírat
oddíly	oddíl	k1gInPc1	oddíl
oděné	oděný	k2eAgInPc1d1	oděný
řeckou	řecký	k2eAgFnSc7d1	řecká
zbrojí	zbroj	k1gFnSc7	zbroj
a	a	k8xC	a
s	s	k7c7	s
prvotním	prvotní	k2eAgNnSc7d1	prvotní
štěstím	štěstí	k1gNnSc7	štěstí
se	se	k3xPyFc4	se
pouští	pouštět	k5eAaImIp3nS	pouštět
do	do	k7c2	do
boje	boj	k1gInSc2	boj
s	s	k7c7	s
Řeky	Řek	k1gMnPc7	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Bojuje	bojovat	k5eAaImIp3nS	bojovat
se	se	k3xPyFc4	se
o	o	k7c4	o
královský	královský	k2eAgInSc4d1	královský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
vítězí	vítězit	k5eAaImIp3nP	vítězit
díky	díky	k7c3	díky
přesile	přesila	k1gFnSc3	přesila
a	a	k8xC	a
palác	palác	k1gInSc1	palác
ničí	ničit	k5eAaImIp3nS	ničit
<g/>
,	,	kIx,	,
pronikají	pronikat	k5eAaImIp3nP	pronikat
do	do	k7c2	do
královské	královský	k2eAgFnSc2d1	královská
komnaty	komnata	k1gFnSc2	komnata
a	a	k8xC	a
řecký	řecký	k2eAgMnSc1d1	řecký
náčelník	náčelník	k1gMnSc1	náčelník
Pyrrhos	Pyrrhos	k1gMnSc1	Pyrrhos
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
krále	král	k1gMnSc4	král
Priama	Priamos	k1gMnSc4	Priamos
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
propadá	propadat	k5eAaPmIp3nS	propadat
hněvu	hněv	k1gInSc2	hněv
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
Helenu	Helena	k1gFnSc4	Helena
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xS	jako
příčinu	příčina	k1gFnSc4	příčina
zkázy	zkáza	k1gFnSc2	zkáza
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
zjeví	zjevit	k5eAaPmIp3nS	zjevit
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
jej	on	k3xPp3gMnSc4	on
zachránit	zachránit	k5eAaPmF	zachránit
otce	otec	k1gMnSc4	otec
Anchíse	Anchís	k1gMnSc4	Anchís
<g/>
,	,	kIx,	,
ženu	žena	k1gFnSc4	žena
Kreúsu	Kreús	k1gInSc2	Kreús
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
Iula	Iulus	k1gMnSc2	Iulus
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
jde	jít	k5eAaImIp3nS	jít
zachránit	zachránit	k5eAaPmF	zachránit
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc4	ten
odmítá	odmítat	k5eAaImIp3nS	odmítat
opustit	opustit	k5eAaPmF	opustit
Tróju	Trója	k1gFnSc4	Trója
<g/>
,	,	kIx,	,
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
jej	on	k3xPp3gNnSc4	on
teprve	teprve	k6eAd1	teprve
zjevení	zjevení	k1gNnSc1	zjevení
kouzelných	kouzelný	k2eAgInPc2d1	kouzelný
plamenů	plamen	k1gInPc2	plamen
nad	nad	k7c7	nad
Iulovou	Iulový	k2eAgFnSc7d1	Iulový
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
let	let	k1gInSc4	let
ohnivé	ohnivý	k2eAgFnSc2d1	ohnivá
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
bere	brát	k5eAaImIp3nS	brát
otce	otec	k1gMnSc4	otec
Anchísa	Anchís	k1gMnSc4	Anchís
do	do	k7c2	do
náručí	náručí	k1gNnSc2	náručí
<g/>
,	,	kIx,	,
Iula	Iul	k1gInSc2	Iul
za	za	k7c4	za
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
je	být	k5eAaImIp3nS	být
manželka	manželka	k1gFnSc1	manželka
Kreúsa	Kreúsa	k1gFnSc1	Kreúsa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Cereřina	Cereřin	k2eAgInSc2d1	Cereřin
chrámu	chrám	k1gInSc2	chrám
ještě	ještě	k6eAd1	ještě
vezmou	vzít	k5eAaPmIp3nP	vzít
ochranné	ochranný	k2eAgMnPc4d1	ochranný
bohy	bůh	k1gMnPc4	bůh
Penáty	penát	k1gMnPc4	penát
a	a	k8xC	a
utíkají	utíkat	k5eAaImIp3nP	utíkat
z	z	k7c2	z
hořící	hořící	k2eAgFnSc2d1	hořící
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
Kreúsu	Kreúsa	k1gFnSc4	Kreúsa
<g/>
,	,	kIx,	,
Aeneas	Aeneas	k1gMnSc1	Aeneas
zanechá	zanechat	k5eAaPmIp3nS	zanechat
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
údolí	údolí	k1gNnSc2	údolí
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zemřelá	zemřelý	k2eAgFnSc1d1	zemřelá
Kreúsa	Kreúsa	k1gFnSc1	Kreúsa
a	a	k8xC	a
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
mu	on	k3xPp3gInSc3	on
náročné	náročný	k2eAgFnPc4d1	náročná
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
dosažení	dosažení	k1gNnSc4	dosažení
Itále	Itále	k1gNnSc1	Itále
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
a	a	k8xC	a
sňatek	sňatek	k1gInSc1	sňatek
s	s	k7c7	s
princeznou	princezna	k1gFnSc7	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Aneas	Aneas	k1gInSc1	Aneas
odchází	odcházet	k5eAaImIp3nS	odcházet
k	k	k7c3	k
uprchlým	uprchlý	k2eAgMnPc3d1	uprchlý
Trójanům	Trójan	k1gMnPc3	Trójan
do	do	k7c2	do
ídských	ídská	k1gFnPc2	ídská
hor.	hor.	k?	hor.
Aeneas	Aeneas	k1gMnSc1	Aeneas
líčí	líčit	k5eAaImIp3nS	líčit
události	událost	k1gFnPc4	událost
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Přeživší	přeživší	k2eAgMnPc1d1	přeživší
Trójané	Trójan	k1gMnPc1	Trójan
staví	stavit	k5eAaPmIp3nP	stavit
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
vydávají	vydávat	k5eAaPmIp3nP	vydávat
se	se	k3xPyFc4	se
k	k	k7c3	k
Thrákii	Thrákie	k1gFnSc3	Thrákie
<g/>
,	,	kIx,	,
Aeneas	Aeneas	k1gMnSc1	Aeneas
obětuje	obětovat	k5eAaBmIp3nS	obětovat
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
nalezne	nalézt	k5eAaBmIp3nS	nalézt
hrob	hrob	k1gInSc1	hrob
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
Trójana	Trójan	k1gMnSc2	Trójan
Polydóra	Polydór	k1gMnSc2	Polydór
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
svěřil	svěřit	k5eAaPmAgInS	svěřit
Priam	Priam	k1gInSc4	Priam
thráckému	thrácký	k2eAgMnSc3d1	thrácký
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
mrtvý	mrtvý	k1gMnSc1	mrtvý
Polydóros	Polydórosa	k1gFnPc2	Polydórosa
převypráví	převyprávět	k5eAaPmIp3nS	převyprávět
Aeneovi	Aeneův	k2eAgMnPc1d1	Aeneův
svůj	svůj	k3xOyFgInSc4	svůj
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Polydóra	Polydóra	k6eAd1	Polydóra
náležitě	náležitě	k6eAd1	náležitě
pohřbí	pohřbít	k5eAaPmIp3nS	pohřbít
a	a	k8xC	a
opouští	opouštět	k5eAaImIp3nS	opouštět
Thrákii	Thrákie	k1gFnSc4	Thrákie
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
připlouvají	připlouvat	k5eAaImIp3nP	připlouvat
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Délos	Délos	k1gInSc1	Délos
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
Apollo	Apollo	k1gMnSc1	Apollo
dává	dávat	k5eAaImIp3nS	dávat
Trójanům	Trójan	k1gMnPc3	Trójan
věštbu	věštba	k1gFnSc4	věštba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prorokuje	prorokovat	k5eAaImIp3nS	prorokovat
jejich	jejich	k3xOp3gInSc2	jejich
rodu	rod	k1gInSc2	rod
velkou	velký	k2eAgFnSc4d1	velká
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
ale	ale	k8xC	ale
hledat	hledat	k5eAaImF	hledat
svou	svůj	k3xOyFgFnSc4	svůj
starou	starý	k2eAgFnSc4d1	stará
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Odplují	odplout	k5eAaPmIp3nP	odplout
na	na	k7c4	na
Krétu	Kréta	k1gFnSc4	Kréta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chtějí	chtít	k5eAaImIp3nP	chtít
postavit	postavit	k5eAaPmF	postavit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
Trójany	Trójan	k1gMnPc4	Trójan
stíhá	stíhat	k5eAaImIp3nS	stíhat
mor	mor	k1gInSc1	mor
a	a	k8xC	a
neúroda	neúroda	k1gFnSc1	neúroda
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
díky	díky	k7c3	díky
Penátům	penát	k1gMnPc3	penát
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
plavit	plavit	k5eAaImF	plavit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
kolem	kolem	k6eAd1	kolem
Strofad	Strofad	k1gInSc4	Strofad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zabijí	zabít	k5eAaPmIp3nP	zabít
jalovici	jalovice	k1gFnSc4	jalovice
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
ji	on	k3xPp3gFnSc4	on
sníst	sníst	k5eAaPmF	sníst
<g/>
,	,	kIx,	,
na	na	k7c4	na
Trójany	Trójan	k1gMnPc4	Trójan
nečekaně	nečekaně	k6eAd1	nečekaně
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
harpyje	harpyje	k1gFnSc1	harpyje
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgMnPc7	který
chtějí	chtít	k5eAaImIp3nP	chtít
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Harpyje	Harpyje	k1gFnSc1	Harpyje
Kelainó	Kelainó	k1gFnSc1	Kelainó
jim	on	k3xPp3gMnPc3	on
dává	dávat	k5eAaImIp3nS	dávat
kvůli	kvůli	k7c3	kvůli
zabití	zabití	k1gNnSc3	zabití
jalovice	jalovice	k1gFnSc2	jalovice
a	a	k8xC	a
souboji	souboj	k1gInSc6	souboj
zlou	zlý	k2eAgFnSc4d1	zlá
věštbu	věštba	k1gFnSc4	věštba
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Trójané	Trójan	k1gMnPc1	Trójan
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
město	město	k1gNnSc1	město
nepostaví	postavit	k5eNaPmIp3nS	postavit
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nesnědí	sníst	k5eNaPmIp3nP	sníst
své	svůj	k3xOyFgInPc4	svůj
stoly	stol	k1gInPc4	stol
<g/>
.	.	kIx.	.
</s>
<s>
Plavba	plavba	k1gFnSc1	plavba
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
k	k	k7c3	k
ostraovu	ostraovo	k1gNnSc3	ostraovo
Epiros	Epirosa	k1gFnPc2	Epirosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Aeneas	Aeneas	k1gMnSc1	Aeneas
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Andromaché	Andromachý	k2eAgMnPc4d1	Andromachý
<g/>
,	,	kIx,	,
ženou	hnát	k5eAaImIp3nP	hnát
Hetkora	Hetkor	k1gMnSc4	Hetkor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
potkává	potkávat	k5eAaImIp3nS	potkávat
i	i	k9	i
Priamova	Priamův	k2eAgMnSc2d1	Priamův
syna	syn	k1gMnSc2	syn
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
Apollonův	Apollonův	k2eAgMnSc1d1	Apollonův
věštec	věštec	k1gMnSc1	věštec
a	a	k8xC	a
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
Aeneovi	Aeneas	k1gMnSc3	Aeneas
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
putování	putování	k1gNnSc1	putování
k	k	k7c3	k
Sicílii	Sicílie	k1gFnSc3	Sicílie
<g/>
,	,	kIx,	,
Tyrrhénským	Tyrrhénský	k2eAgNnSc7d1	Tyrrhénský
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
také	také	k9	také
vydat	vydat	k5eAaPmF	vydat
k	k	k7c3	k
podsvětní	podsvětní	k2eAgFnSc3d1	podsvětní
řece	řeka	k1gFnSc3	řeka
<g/>
,	,	kIx,	,
než	než	k8xS	než
najde	najít	k5eAaPmIp3nS	najít
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
znamení	znamení	k1gNnSc6	znamení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
prasnice	prasnice	k1gFnSc2	prasnice
s	s	k7c7	s
třiceti	třicet	k4xCc2	třicet
prasátky	prasátko	k1gNnPc7	prasátko
<g/>
.	.	kIx.	.
</s>
<s>
Varuje	varovat	k5eAaImIp3nS	varovat
Aenea	Aeneas	k1gMnSc4	Aeneas
před	před	k7c7	před
Skyllou	Skylla	k1gFnSc7	Skylla
a	a	k8xC	a
Charybdou	Charybda	k1gFnSc7	Charybda
<g/>
,	,	kIx,	,
nabádá	nabádat	k5eAaImIp3nS	nabádat
k	k	k7c3	k
obětem	oběť	k1gFnPc3	oběť
bohyni	bohyně	k1gFnSc4	bohyně
Junoně	Juno	k1gFnSc3	Juno
<g/>
,	,	kIx,	,
manželce	manželka	k1gFnSc3	manželka
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Radí	radit	k5eAaImIp3nP	radit
Aenenovi	Aenenův	k2eAgMnPc1d1	Aenenův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
Kúmách	Kúma	k1gNnPc6	Kúma
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
věštkyní	věštkyně	k1gFnSc7	věštkyně
Sibylou	Sibyla	k1gFnSc7	Sibyla
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
plují	plout	k5eAaImIp3nP	plout
kolem	kolem	k7c2	kolem
břehů	břeh	k1gInPc2	břeh
jiní	jiný	k1gMnPc1	jiný
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Míjí	míjet	k5eAaImIp3nS	míjet
sopku	sopka	k1gFnSc4	sopka
Etnu	Etna	k1gFnSc4	Etna
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
svrhl	svrhnout	k5eAaPmAgInS	svrhnout
Jupiter	Jupiter	k1gInSc1	Jupiter
na	na	k7c4	na
obra	obr	k1gMnSc4	obr
Enkelada	Enkelada	k1gFnSc1	Enkelada
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
zpod	zpod	k7c2	zpod
hory	hora	k1gFnSc2	hora
plive	plive	k?	plive
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Setkávají	setkávat	k5eAaImIp3nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
Řekem	Řek	k1gMnSc7	Řek
Achaimenidem	Achaimenid	k1gMnSc7	Achaimenid
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jim	on	k3xPp3gMnPc3	on
vylíčí	vylíčit	k5eAaPmIp3nS	vylíčit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jeho	jeho	k3xOp3gMnSc1	jeho
druh	druh	k1gMnSc1	druh
Odysseus	Odysseus	k1gMnSc1	Odysseus
unikl	uniknout	k5eAaPmAgMnS	uniknout
ze	z	k7c2	z
spárů	spár	k1gInPc2	spár
Polyféma	Polyfém	k1gMnSc2	Polyfém
<g/>
,	,	kIx,	,
jednookého	jednooký	k2eAgMnSc2d1	jednooký
obra	obr	k1gMnSc2	obr
lidojeda	lidojed	k1gMnSc2	lidojed
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
uvidí	uvidět	k5eAaPmIp3nS	uvidět
obra	obr	k1gMnSc4	obr
<g/>
,	,	kIx,	,
Aeneas	Aeneas	k1gMnSc1	Aeneas
velí	velet	k5eAaImIp3nS	velet
k	k	k7c3	k
překotnému	překotný	k2eAgNnSc3d1	překotné
odplutí	odplutí	k1gNnSc3	odplutí
<g/>
,	,	kIx,	,
k	k	k7c3	k
břehu	břeh	k1gInSc3	břeh
přibíhají	přibíhat	k5eAaImIp3nP	přibíhat
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
Kyklópové	Kyklóp	k1gMnPc1	Kyklóp
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
loď	loď	k1gFnSc4	loď
u	u	k7c2	u
nedostihnou	dostihnout	k5eNaPmIp3nP	dostihnout
<g/>
.	.	kIx.	.
</s>
<s>
Doráží	dorážet	k5eAaImIp3nS	dorážet
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Drepanos	Drepanosa	k1gFnPc2	Drepanosa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Aeneas	Aeneas	k1gMnSc1	Aeneas
pohřbívá	pohřbívat	k5eAaImIp3nS	pohřbívat
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Anchísa	Anchís	k1gMnSc2	Anchís
<g/>
.	.	kIx.	.
</s>
<s>
Vítr	vítr	k1gInSc1	vítr
odvane	odvanout	k5eAaPmIp3nS	odvanout
Trójany	Trójan	k1gMnPc4	Trójan
od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
k	k	k7c3	k
africkým	africký	k2eAgInPc3d1	africký
břehům	břeh	k1gInPc3	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Dídó	Dídó	k?	Dídó
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
planoucí	planoucí	k2eAgFnSc6d1	planoucí
lásce	láska	k1gFnSc6	láska
k	k	k7c3	k
Aeneovi	Aeneas	k1gMnSc3	Aeneas
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
líčí	líčit	k5eAaImIp3nS	líčit
počet	počet	k1gInSc4	počet
afrických	africký	k2eAgMnPc2d1	africký
ženichů	ženich	k1gMnPc2	ženich
<g/>
,	,	kIx,	,
Aeneas	Aeneas	k1gMnSc1	Aeneas
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
příznivým	příznivý	k2eAgNnSc7d1	příznivé
znamením	znamení	k1gNnSc7	znamení
od	od	k7c2	od
Iunony	Iuno	k1gFnSc2	Iuno
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
Dídóně	Dídóna	k1gFnSc3	Dídóna
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
<g/>
.	.	kIx.	.
</s>
<s>
Dídó	Dídó	k?	Dídó
koná	konat	k5eAaImIp3nS	konat
oběť	oběť	k1gFnSc4	oběť
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Iuno	Iuno	k1gFnSc1	Iuno
se	se	k3xPyFc4	se
domlouvá	domlouvat	k5eAaImIp3nS	domlouvat
s	s	k7c7	s
Venuší	Venuše	k1gFnSc7	Venuše
na	na	k7c6	na
sňatku	sňatek	k1gInSc6	sňatek
Dídóny	Dídóna	k1gFnSc2	Dídóna
s	s	k7c7	s
Aeneem	Aeneas	k1gMnSc7	Aeneas
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc1	Venuše
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
vidí	vidět	k5eAaImIp3nS	vidět
lest	lest	k1gFnSc1	lest
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ale	ale	k9	ale
dohodnou	dohodnout	k5eAaPmIp3nP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
lovu	lov	k1gInSc2	lov
se	se	k3xPyFc4	se
strhne	strhnout	k5eAaPmIp3nS	strhnout
bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zažene	zahnat	k5eAaPmIp3nS	zahnat
dvojici	dvojice	k1gFnSc4	dvojice
do	do	k7c2	do
jeskyně	jeskyně	k1gFnSc2	jeskyně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
stráví	strávit	k5eAaPmIp3nP	strávit
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
tím	ten	k3xDgNnSc7	ten
manželství	manželství	k1gNnSc4	manželství
nevznikne	vzniknout	k5eNaPmIp3nS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Punové	Pun	k1gMnPc1	Pun
i	i	k8xC	i
Trójané	Trójan	k1gMnPc1	Trójan
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaImIp3nP	vydávat
na	na	k7c4	na
společný	společný	k2eAgInSc4d1	společný
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
se	se	k3xPyFc4	se
strhne	strhnout	k5eAaPmIp3nS	strhnout
bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
Aeneas	Aeneas	k1gMnSc1	Aeneas
skončí	skončit	k5eAaPmIp3nS	skončit
s	s	k7c7	s
Dídó	Dídó	k1gFnSc7	Dídó
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Zosobněná	zosobněný	k2eAgFnSc1d1	zosobněná
Pověst	pověst	k1gFnSc1	pověst
létá	létat	k5eAaImIp3nS	létat
po	po	k7c6	po
Libyi	Libye	k1gFnSc6	Libye
a	a	k8xC	a
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
Aeneovi	Aeneas	k1gMnSc6	Aeneas
a	a	k8xC	a
Didoně	Didona	k1gFnSc6	Didona
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yQgMnPc3	který
upadá	upadat	k5eAaImIp3nS	upadat
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
mísí	mísit	k5eAaImIp3nP	mísit
ale	ale	k9	ale
pravdu	pravda	k1gFnSc4	pravda
se	s	k7c7	s
lží	lež	k1gFnSc7	lež
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
pobouřený	pobouřený	k2eAgMnSc1d1	pobouřený
maretánský	maretánský	k2eAgMnSc1d1	maretánský
král	král	k1gMnSc1	král
Iarbas	Iarbas	k1gMnSc1	Iarbas
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Dídó	Dídó	k1gFnSc1	Dídó
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
vysílá	vysílat	k5eAaImIp3nS	vysílat
prosby	prosba	k1gFnPc4	prosba
k	k	k7c3	k
Iovovi	Iova	k1gMnSc3	Iova
vládci	vládce	k1gMnSc3	vládce
bohů	bůh	k1gMnPc2	bůh
na	na	k7c6	na
zjednání	zjednání	k1gNnSc6	zjednání
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Iupiter	Iupiter	k1gInSc1	Iupiter
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
posla	posel	k1gMnSc4	posel
bohů	bůh	k1gMnPc2	bůh
Merkura	Merkur	k1gMnSc4	Merkur
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzkázal	vzkázat	k5eAaPmAgInS	vzkázat
Aeneovi	Aeneas	k1gMnSc3	Aeneas
odplout	odplout	k5eAaPmF	odplout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
a	a	k8xC	a
především	především	k9	především
syna	syn	k1gMnSc4	syn
čeká	čekat	k5eAaImIp3nS	čekat
osud	osud	k1gInSc1	osud
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
zmatku	zmatek	k1gInSc2	zmatek
vydá	vydat	k5eAaPmIp3nS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
svým	svůj	k3xOyFgInSc7	svůj
lidem	lid	k1gInSc7	lid
ke	k	k7c3	k
skryté	skrytý	k2eAgFnSc3d1	skrytá
přípravě	příprava	k1gFnSc3	příprava
lodí	loď	k1gFnPc2	loď
na	na	k7c4	na
odchod	odchod	k1gInSc4	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Dídó	Dídó	k?	Dídó
tuto	tento	k3xDgFnSc4	tento
lest	lest	k1gFnSc4	lest
prohlédne	prohlédnout	k5eAaPmIp3nS	prohlédnout
<g/>
.	.	kIx.	.
</s>
<s>
Dídó	Dídó	k?	Dídó
vyčítá	vyčítat	k5eAaImIp3nS	vyčítat
<g/>
,	,	kIx,	,
přemlouvá	přemlouvat	k5eAaImIp3nS	přemlouvat
<g/>
,	,	kIx,	,
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
zapřísahá	zapřísahat	k5eAaImIp3nS	zapřísahat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
musí	muset	k5eAaImIp3nS	muset
odejít	odejít	k5eAaPmF	odejít
Aeneas	Aeneas	k1gMnSc1	Aeneas
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
jí	on	k3xPp3gFnSc3	on
nechá	nechat	k5eAaPmIp3nS	nechat
alespoň	alespoň	k9	alespoň
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
uznává	uznávat	k5eAaImIp3nS	uznávat
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
nechtěl	chtít	k5eNaImAgMnS	chtít
odcházet	odcházet	k5eAaImF	odcházet
ani	ani	k8xC	ani
z	z	k7c2	z
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
odchází	odcházet	k5eAaImIp3nS	odcházet
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
city	city	k1gNnSc6	city
vřou	vřít	k5eAaImIp3nP	vřít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhněvaná	rozhněvaný	k2eAgFnSc1d1	rozhněvaná
Dídó	Dídó	k1gFnSc1	Dídó
řekne	říct	k5eAaPmIp3nS	říct
Aeneovi	Aeneas	k1gMnSc3	Aeneas
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
tedy	tedy	k9	tedy
jde	jít	k5eAaImIp3nS	jít
<g/>
,	,	kIx,	,
hádku	hádka	k1gFnSc4	hádka
končí	končit	k5eAaImIp3nS	končit
kletbou	kletba	k1gFnSc7	kletba
a	a	k8xC	a
umdlelou	umdlelý	k2eAgFnSc7d1	umdlelý
ji	on	k3xPp3gFnSc4	on
služky	služka	k1gFnPc4	služka
odnesou	odnést	k5eAaPmIp3nP	odnést
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholí	vrcholí	k1gNnSc1	vrcholí
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c6	na
odplutí	odplutí	k1gNnSc6	odplutí
<g/>
.	.	kIx.	.
</s>
<s>
Dídó	Dídó	k?	Dídó
posílá	posílat	k5eAaImIp3nS	posílat
sestru	sestra	k1gFnSc4	sestra
Annu	Anna	k1gFnSc4	Anna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Aeneas	Aeneas	k1gMnSc1	Aeneas
na	na	k7c4	na
čas	čas	k1gInSc4	čas
odložil	odložit	k5eAaPmAgInS	odložit
odjezd	odjezd	k1gInSc1	odjezd
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
nevyslyší	vyslyšet	k5eNaPmIp3nS	vyslyšet
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholí	vrcholit	k5eAaImIp3nP	vrcholit
královnino	královnin	k2eAgNnSc4d1	královnino
zoufalství	zoufalství	k1gNnSc4	zoufalství
<g/>
,	,	kIx,	,
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
věštby	věštba	k1gFnPc4	věštba
a	a	k8xC	a
sny	sen	k1gInPc4	sen
jí	on	k3xPp3gFnSc3	on
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
zlá	zlé	k1gNnPc1	zlé
znamení	znamení	k1gNnSc1	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Nechává	nechávat	k5eAaImIp3nS	nechávat
vystavět	vystavět	k5eAaPmF	vystavět
hranici	hranice	k1gFnSc4	hranice
z	z	k7c2	z
manželského	manželský	k2eAgNnSc2d1	manželské
lůžka	lůžko	k1gNnSc2	lůžko
a	a	k8xC	a
Aeneovy	Aeneův	k2eAgFnSc2d1	Aeneův
zbraně	zbraň	k1gFnSc2	zbraň
jako	jako	k8xS	jako
magický	magický	k2eAgInSc4d1	magický
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zastřela	zastřít	k5eAaPmAgFnS	zastřít
sebevražedné	sebevražedný	k2eAgInPc4d1	sebevražedný
úmysly	úmysl	k1gInPc4	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Obětuje	obětovat	k5eAaBmIp3nS	obětovat
podsvětním	podsvětní	k2eAgMnPc3d1	podsvětní
bohům	bůh	k1gMnPc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenkami	myšlenka	k1gFnPc7	myšlenka
těká	těkat	k5eAaImIp3nS	těkat
nad	nad	k7c7	nad
Trójany	Trójan	k1gMnPc7	Trójan
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
svým	svůj	k3xOyFgInSc7	svůj
osudem	osud	k1gInSc7	osud
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
porušenou	porušený	k2eAgFnSc7d1	porušená
věrností	věrnost	k1gFnSc7	věrnost
k	k	k7c3	k
Sychaeovi	Sychaeus	k1gMnSc3	Sychaeus
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
budí	budit	k5eAaImIp3nS	budit
Aenea	Aeneas	k1gMnSc4	Aeneas
ze	z	k7c2	z
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
varuje	varovat	k5eAaImIp3nS	varovat
před	před	k7c7	před
hrozící	hrozící	k2eAgFnSc7d1	hrozící
zkázou	zkáza	k1gFnSc7	zkáza
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
probudí	probudit	k5eAaPmIp3nS	probudit
Trójany	Trójan	k1gMnPc4	Trójan
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
odvázat	odvázat	k5eAaPmF	odvázat
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
spěšně	spěšně	k6eAd1	spěšně
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Dídó	Dídó	k?	Dídó
propadá	propadat	k5eAaPmIp3nS	propadat
pomstychtivému	pomstychtivý	k2eAgInSc3d1	pomstychtivý
hněvu	hněv	k1gInSc3	hněv
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chtěla	chtít	k5eAaImAgFnS	chtít
nechat	nechat	k5eAaPmF	nechat
zapálit	zapálit	k5eAaPmF	zapálit
trójské	trójský	k2eAgFnPc4d1	Trójská
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
zabít	zabít	k5eAaPmF	zabít
Askania	Askanium	k1gNnPc4	Askanium
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
vzývá	vzývat	k5eAaImIp3nS	vzývat
podsvětní	podsvětní	k2eAgNnPc4d1	podsvětní
božstva	božstvo	k1gNnPc4	božstvo
a	a	k8xC	a
sesílá	sesílat	k5eAaImIp3nS	sesílat
na	na	k7c4	na
Trójany	Trójan	k1gMnPc4	Trójan
kletbu	kletba	k1gFnSc4	kletba
a	a	k8xC	a
ukládá	ukládat	k5eAaImIp3nS	ukládat
Kartágu	Kartágo	k1gNnSc3	Kartágo
smtelnou	smtelný	k2eAgFnSc4d1	smtelný
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
budoucímu	budoucí	k2eAgInSc3d1	budoucí
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Nechá	nechat	k5eAaPmIp3nS	nechat
poslat	poslat	k5eAaPmF	poslat
pro	pro	k7c4	pro
sestru	sestra	k1gFnSc4	sestra
Annu	Anna	k1gFnSc4	Anna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravila	připravit	k5eAaPmAgFnS	připravit
očistný	očistný	k2eAgInSc4d1	očistný
obřad	obřad	k1gInSc4	obřad
a	a	k8xC	a
probodne	probodnout	k5eAaPmIp3nS	probodnout
se	se	k3xPyFc4	se
Aeneovým	Aeneův	k2eAgInSc7d1	Aeneův
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Iuno	Iuno	k1gFnSc1	Iuno
posílá	posílat	k5eAaImIp3nS	posílat
poselkyni	poselkyně	k1gFnSc4	poselkyně
bohů	bůh	k1gMnPc2	bůh
Írys	Írys	k1gInSc4	Írys
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zbavila	zbavit	k5eAaPmAgFnS	zbavit
Dídónu	Dídóna	k1gFnSc4	Dídóna
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Iupiterova	Iupiterův	k2eAgFnSc1d1	Iupiterův
bouře	bouře	k1gFnSc1	bouře
zažene	zahnat	k5eAaPmIp3nS	zahnat
Aenea	Aeneas	k1gMnSc4	Aeneas
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
druhy	druh	k1gInPc1	druh
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
uvítá	uvítat	k5eAaPmIp3nS	uvítat
král	král	k1gMnSc1	král
Acestes	Acestes	k1gMnSc1	Acestes
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
Trójan	Trójan	k1gMnSc1	Trójan
<g/>
.	.	kIx.	.
</s>
<s>
Aneas	Aneas	k1gInSc1	Aneas
se	s	k7c7	s
shromážděným	shromážděný	k2eAgNnSc7d1	shromážděné
lidem	lido	k1gNnSc7	lido
koná	konat	k5eAaImIp3nS	konat
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
oběti	oběť	k1gFnPc4	oběť
na	na	k7c6	na
hrobě	hrob	k1gInSc6	hrob
otce	otec	k1gMnSc2	otec
Anchísa	Anchís	k1gMnSc2	Anchís
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Aeneas	Aeneas	k1gMnSc1	Aeneas
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
počest	počest	k1gFnSc4	počest
budou	být	k5eAaImBp3nP	být
konat	konat	k5eAaImF	konat
hry	hra	k1gFnPc4	hra
–	–	k?	–
závody	závod	k1gInPc1	závod
lodí	loď	k1gFnSc7	loď
<g/>
,	,	kIx,	,
hod	hod	k1gInSc1	hod
kopí	kopí	k1gNnSc2	kopí
<g/>
,	,	kIx,	,
střelba	střelba	k1gFnSc1	střelba
lukem	luk	k1gInSc7	luk
a	a	k8xC	a
pěstní	pěstní	k2eAgInSc4d1	pěstní
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obětech	oběť	k1gFnPc6	oběť
vyleze	vylézt	k5eAaPmIp3nS	vylézt
zpoza	zpoza	k7c2	zpoza
hrobu	hrob	k1gInSc2	hrob
pestrobarevný	pestrobarevný	k2eAgMnSc1d1	pestrobarevný
had	had	k1gMnSc1	had
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stáčel	stáčet	k5eAaImAgMnS	stáčet
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
kruhů	kruh	k1gInPc2	kruh
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
naplnil	naplnit	k5eAaPmAgInS	naplnit
sedmý	sedmý	k4xOgInSc1	sedmý
rok	rok	k1gInSc1	rok
bloudění	bloudění	k1gNnSc2	bloudění
<g/>
.	.	kIx.	.
</s>
<s>
Trojané	Trojaný	k2eAgNnSc1d1	Trojaný
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ducha	duch	k1gMnSc4	duch
tohoto	tento	k3xDgInSc2	tento
místa	místo	k1gNnPc4	místo
nebo	nebo	k8xC	nebo
o	o	k7c4	o
Anchísova	Anchísův	k2eAgMnSc4d1	Anchísův
posla	posel	k1gMnSc4	posel
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
začínají	začínat	k5eAaImIp3nP	začínat
závodem	závod	k1gInSc7	závod
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
Aeneas	Aeneas	k1gMnSc1	Aeneas
odměňuje	odměňovat	k5eAaImIp3nS	odměňovat
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
,	,	kIx,	,
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
běžeckým	běžecký	k2eAgInSc7d1	běžecký
závodem	závod	k1gInSc7	závod
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
pěstní	pěstní	k2eAgInPc1d1	pěstní
souboje	souboj	k1gInPc1	souboj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zápasníci	zápasník	k1gMnPc1	zápasník
zprvu	zprvu	k6eAd1	zprvu
váhají	váhat	k5eAaImIp3nP	váhat
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ale	ale	k9	ale
dají	dát	k5eAaPmIp3nP	dát
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skončí	skončit	k5eAaPmIp3nS	skončit
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
úkazem	úkaz	k1gInSc7	úkaz
–	–	k?	–
naplano	naplano	k6eAd1	naplano
vystřelený	vystřelený	k2eAgInSc1d1	vystřelený
šíp	šíp	k1gInSc1	šíp
se	se	k3xPyFc4	se
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
vznítí	vznítit	k5eAaPmIp3nS	vznítit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Aeneas	Aeneas	k1gMnSc1	Aeneas
to	ten	k3xDgNnSc4	ten
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jako	jako	k8xS	jako
dobré	dobrý	k2eAgNnSc1d1	dobré
znamení	znamení	k1gNnSc1	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
trójská	trójský	k2eAgFnSc1d1	Trójská
branná	branný	k2eAgFnSc1d1	Branná
mládež	mládež	k1gFnSc1	mládež
za	za	k7c4	za
vedení	vedení	k1gNnSc4	vedení
Iula	Iulum	k1gNnSc2	Iulum
Askania	Askanium	k1gNnSc2	Askanium
<g/>
.	.	kIx.	.
</s>
<s>
Iuno	Iuno	k1gFnSc1	Iuno
přikazuje	přikazovat	k5eAaImIp3nS	přikazovat
Íris	Íris	k1gInSc4	Íris
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
za	za	k7c7	za
trójskými	trójský	k2eAgFnPc7d1	Trójská
ženami	žena	k1gFnPc7	žena
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Beroe	Beroe	k1gFnPc2	Beroe
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
zasela	zasít	k5eAaPmAgFnS	zasít
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
plavbám	plavba	k1gFnPc3	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
stařena	stařena	k1gFnSc1	stařena
Pyyrgó	Pyyrgó	k1gMnPc2	Pyyrgó
odhalí	odhalit	k5eAaPmIp3nS	odhalit
lest	lest	k1gFnSc4	lest
Íris	Írisa	k1gFnPc2	Írisa
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
přesto	přesto	k6eAd1	přesto
vydají	vydat	k5eAaPmIp3nP	vydat
zapálit	zapálit	k5eAaPmF	zapálit
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
dozvídají	dozvídat	k5eAaImIp3nP	dozvídat
o	o	k7c6	o
požáru	požár	k1gInSc6	požár
<g/>
,	,	kIx,	,
Iulus	Iulus	k1gInSc1	Iulus
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
k	k	k7c3	k
lodím	loď	k1gFnPc3	loď
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
rozutečou	rozutéct	k5eAaPmIp3nP	rozutéct
a	a	k8xC	a
ukryjí	ukrýt	k5eAaPmIp3nP	ukrýt
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
hasí	hasit	k5eAaImIp3nP	hasit
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
Aeneas	Aeneas	k1gMnSc1	Aeneas
prosí	prosit	k5eAaImIp3nS	prosit
Iupitera	Iupiter	k1gMnSc4	Iupiter
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
sešle	sešle	k6eAd1	sešle
déšť	déšť	k1gInSc4	déšť
na	na	k7c4	na
uhašení	uhašení	k1gNnSc4	uhašení
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
lodě	loď	k1gFnPc1	loď
unikly	uniknout	k5eAaPmAgFnP	uniknout
zničení	zničení	k1gNnSc4	zničení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Aenea	Aeneas	k1gMnSc4	Aeneas
padají	padat	k5eAaImIp3nP	padat
kvůli	kvůli	k7c3	kvůli
oné	onen	k3xDgFnSc3	onen
pohromě	pohroma	k1gFnSc3	pohroma
pochyby	pochyba	k1gFnSc2	pochyba
<g/>
,	,	kIx,	,
rozvažuje	rozvažovat	k5eAaImIp3nS	rozvažovat
zdali	zdali	k9	zdali
zůstat	zůstat	k5eAaPmF	zůstat
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
město	město	k1gNnSc4	město
tady	tady	k6eAd1	tady
nebo	nebo	k8xC	nebo
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Moudrý	moudrý	k2eAgInSc1d1	moudrý
Nautés	Nautés	k1gInSc1	Nautés
mu	on	k3xPp3gMnSc3	on
poradí	poradit	k5eAaPmIp3nS	poradit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nechal	nechat	k5eAaPmAgMnS	nechat
starce	stařec	k1gMnPc4	stařec
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
tady	tady	k6eAd1	tady
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
k	k	k7c3	k
italským	italský	k2eAgInPc3d1	italský
břehům	břeh	k1gInPc3	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Aeneovi	Aeneas	k1gMnSc3	Aeneas
se	se	k3xPyFc4	se
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
Anchíses	Anchíses	k1gInSc1	Anchíses
z	z	k7c2	z
dovolení	dovolení	k1gNnSc2	dovolení
Iupitera	Iupiter	k1gMnSc2	Iupiter
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dal	dát	k5eAaPmAgMnS	dát
na	na	k7c4	na
rady	rada	k1gFnPc4	rada
<g/>
,	,	kIx,	,
sesbíral	sesbírat	k5eAaPmAgMnS	sesbírat
nejodvážnější	odvážný	k2eAgMnSc1d3	nejodvážnější
a	a	k8xC	a
vyplul	vyplout	k5eAaPmAgInS	vyplout
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
sestoupit	sestoupit	k5eAaPmF	sestoupit
s	s	k7c7	s
věštkyní	věštkyně	k1gFnSc7	věštkyně
Sibyllou	Sibyllý	k2eAgFnSc7d1	Sibyllý
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
svolává	svolávat	k5eAaImIp3nS	svolávat
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
nechává	nechávat	k5eAaImIp3nS	nechávat
opravit	opravit	k5eAaPmF	opravit
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
Acestovi	Acesta	k1gMnSc6	Acesta
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
nad	nad	k7c7	nad
místem	místo	k1gNnSc7	místo
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
město	město	k1gNnSc1	město
Acestu	Acest	k1gInSc2	Acest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
devítidenních	devítidenní	k2eAgInPc6d1	devítidenní
obřadech	obřad	k1gInPc6	obřad
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
<g/>
.	.	kIx.	.
</s>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
přichází	přicházet	k5eAaImIp3nS	přicházet
za	za	k7c7	za
Neptunem	Neptun	k1gInSc7	Neptun
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Aeneovi	Aeneas	k1gMnSc3	Aeneas
klidnou	klidný	k2eAgFnSc4d1	klidná
plavbu	plavba	k1gFnSc4	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gMnSc1	Neptun
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
plavbě	plavba	k1gFnSc6	plavba
padne	padnout	k5eAaImIp3nS	padnout
jeden	jeden	k4xCgMnSc1	jeden
námořník	námořník	k1gMnSc1	námořník
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
noční	noční	k2eAgFnSc2d1	noční
plavby	plavba	k1gFnSc2	plavba
se	se	k3xPyFc4	se
námořníci	námořník	k1gMnPc1	námořník
usnuli	usnout	k5eAaPmAgMnP	usnout
<g/>
,	,	kIx,	,
Palinúra	Palinúra	k1gMnSc1	Palinúra
uspal	uspat	k5eAaPmAgMnS	uspat
sám	sám	k3xTgMnSc1	sám
bůh	bůh	k1gMnSc1	bůh
Spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
a	a	k8xC	a
svrhl	svrhnout	k5eAaPmAgInS	svrhnout
jej	on	k3xPp3gMnSc4	on
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
umírá	umírat	k5eAaImIp3nS	umírat
nepohřben	pohřben	k2eNgMnSc1d1	pohřben
ve	v	k7c6	v
vlnách	vlna	k1gFnPc6	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
přistává	přistávat	k5eAaImIp3nS	přistávat
na	na	k7c4	na
italské	italský	k2eAgInPc4d1	italský
břehy	břeh	k1gInPc4	břeh
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
se	se	k3xPyFc4	se
k	k	k7c3	k
Apollonovu	Apollonův	k2eAgInSc3d1	Apollonův
chrámu	chrám	k1gInSc3	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
druhové	druh	k1gMnPc1	druh
si	se	k3xPyFc3	se
prohlíží	prohlížet	k5eAaImIp3nP	prohlížet
vyobrazení	vyobrazení	k1gNnSc4	vyobrazení
na	na	k7c6	na
branách	brána	k1gFnPc6	brána
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
vylíčen	vylíčen	k2eAgInSc1d1	vylíčen
příběh	příběh	k1gInSc1	příběh
Daidala	Daidal	k1gMnSc2	Daidal
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
obětuje	obětovat	k5eAaBmIp3nS	obětovat
sedm	sedm	k4xCc1	sedm
jhem	jho	k1gNnSc7	jho
netknutých	tknutý	k2eNgMnPc2d1	netknutý
býků	býk	k1gMnPc2	býk
<g/>
,	,	kIx,	,
volá	volat	k5eAaImIp3nS	volat
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
věštkyně	věštkyně	k1gFnPc1	věštkyně
Sibylla	Sibyll	k1gMnSc2	Sibyll
do	do	k7c2	do
ochrámu	ochrám	k1gInSc2	ochrám
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
pronáší	pronášet	k5eAaImIp3nS	pronášet
modlitbu	modlitba	k1gFnSc4	modlitba
k	k	k7c3	k
Apollonovi	Apollo	k1gMnSc3	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
žádá	žádat	k5eAaImIp3nS	žádat
od	od	k7c2	od
Sibylly	Sibylla	k1gFnSc2	Sibylla
věštbu	věštba	k1gFnSc4	věštba
<g/>
.	.	kIx.	.
</s>
<s>
Prostoupena	prostoupen	k2eAgFnSc1d1	prostoupena
Apollonem	Apollo	k1gMnSc7	Apollo
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
Aeneovi	Aeneas	k1gMnSc3	Aeneas
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
cizinkou	cizinka	k1gFnSc7	cizinka
a	a	k8xC	a
nečekanou	čekaný	k2eNgFnSc4d1	nečekaná
naději	naděje	k1gFnSc4	naděje
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
žádá	žádat	k5eAaImIp3nS	žádat
po	po	k7c6	po
Sibylle	Sibylla	k1gFnSc6	Sibylla
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
podsvětí	podsvětí	k1gNnSc2	podsvětí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
otci	otec	k1gMnSc3	otec
Anchísovi	Anchís	k1gMnSc3	Anchís
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
získat	získat	k5eAaPmF	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
ratolest	ratolest	k1gFnSc4	ratolest
–	–	k?	–
dar	dar	k1gInSc1	dar
pro	pro	k7c4	pro
podsvětní	podsvětní	k2eAgFnSc4d1	podsvětní
bohyni	bohyně	k1gFnSc4	bohyně
Proserpinu	Proserpin	k1gInSc2	Proserpin
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
tam	tam	k6eAd1	tam
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
také	také	k9	také
pohřbít	pohřbít	k5eAaPmF	pohřbít
druha	druh	k1gMnSc4	druh
Míséna	Mísén	k1gMnSc4	Mísén
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
poměřovat	poměřovat	k5eAaImF	poměřovat
s	s	k7c7	s
mořským	mořský	k2eAgMnSc7d1	mořský
bohem	bůh	k1gMnSc7	bůh
Trítónem	Trítón	k1gMnSc7	Trítón
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
trest	trest	k1gInSc4	trest
utopil	utopit	k5eAaPmAgMnS	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
jej	on	k3xPp3gNnSc4	on
nechává	nechávat	k5eAaImIp3nS	nechávat
pohřbít	pohřbít	k5eAaPmF	pohřbít
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
dva	dva	k4xCgMnPc4	dva
holoubky	holoubek	k1gMnPc4	holoubek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
zlaté	zlatý	k2eAgFnSc3d1	zlatá
ratolesti	ratolest	k1gFnSc3	ratolest
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
utrhl	utrhnout	k5eAaPmAgInS	utrhnout
<g/>
,	,	kIx,	,
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
k	k	k7c3	k
Sibylle	Sibylla	k1gFnSc3	Sibylla
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
podsvětí	podsvětí	k1gNnSc1	podsvětí
náležité	náležitý	k2eAgFnSc2d1	náležitá
oběti	oběť	k1gFnSc2	oběť
podsvětním	podsvětní	k2eAgNnPc3d1	podsvětní
božstvům	božstvo	k1gNnPc3	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Sibylla	Sibylla	k1gFnSc1	Sibylla
odesílá	odesílat	k5eAaImIp3nS	odesílat
všechny	všechen	k3xTgInPc4	všechen
kromě	kromě	k7c2	kromě
Aenea	Aeneas	k1gMnSc2	Aeneas
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
musí	muset	k5eAaImIp3nS	muset
tasit	tasit	k5eAaPmF	tasit
meč	meč	k1gInSc1	meč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
podsvětí	podsvětí	k1gNnSc2	podsvětí
sídlí	sídlet	k5eAaImIp3nP	sídlet
démoni	démon	k1gMnPc1	démon
zhouby	zhouba	k1gFnSc2	zhouba
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
obludy	obluda	k1gFnSc2	obluda
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Acheron	Acheron	k1gMnSc1	Acheron
<g/>
,	,	kIx,	,
Kókytos	Kókytos	k1gMnSc1	Kókytos
vidí	vidět	k5eAaImIp3nS	vidět
starce	stařec	k1gMnSc2	stařec
–	–	k?	–
převozníka	převozník	k1gMnSc2	převozník
duší	duše	k1gFnPc2	duše
Charóna	Charón	k1gMnSc2	Charón
<g/>
.	.	kIx.	.
</s>
<s>
Aenas	Aenas	k1gInSc1	Aenas
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
čekající	čekající	k2eAgFnPc4d1	čekající
duše	duše	k1gFnPc4	duše
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nP	prosit
Sibyllu	Sibylla	k1gFnSc4	Sibylla
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Přeplavit	přeplavit	k5eAaPmF	přeplavit
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Styx	Styx	k1gInSc1	Styx
smí	smět	k5eAaImIp3nS	smět
řádně	řádně	k6eAd1	řádně
pohřbené	pohřbený	k2eAgFnPc4d1	pohřbená
duše	duše	k1gFnPc4	duše
<g/>
,	,	kIx,	,
nepohřbení	nepohřbení	k1gNnSc4	nepohřbení
musí	muset	k5eAaImIp3nP	muset
čekat	čekat	k5eAaImF	čekat
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
potkává	potkávat	k5eAaImIp3nS	potkávat
své	svůj	k3xOyFgInPc4	svůj
zemřelé	zemřelý	k2eAgInPc4d1	zemřelý
druhy	druh	k1gInPc4	druh
i	i	k8xC	i
kormidelníka	kormidelník	k1gMnSc4	kormidelník
Palinúra	Palinúr	k1gMnSc4	Palinúr
a	a	k8xC	a
promlouvá	promlouvat	k5eAaImIp3nS	promlouvat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Charón	Charón	k1gMnSc1	Charón
pozná	poznat	k5eAaPmIp3nS	poznat
živého	živý	k2eAgMnSc4d1	živý
Aenea	Aeneas	k1gMnSc4	Aeneas
a	a	k8xC	a
prvně	prvně	k?	prvně
jej	on	k3xPp3gMnSc4	on
nechce	chtít	k5eNaImIp3nS	chtít
převést	převést	k5eAaPmF	převést
<g/>
,	,	kIx,	,
po	po	k7c6	po
ukázání	ukázání	k1gNnSc6	ukázání
zlaté	zlatý	k2eAgFnSc2d1	zlatá
ratolesti	ratolest	k1gFnSc2	ratolest
svolí	svolit	k5eAaPmIp3nS	svolit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
břehu	břeh	k1gInSc6	břeh
se	se	k3xPyFc4	se
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
k	k	k7c3	k
pekelnému	pekelný	k2eAgMnSc3d1	pekelný
psu	pes	k1gMnSc3	pes
Kerberovi	Kerber	k1gMnSc3	Kerber
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
štěkat	štěkat	k5eAaImF	štěkat
a	a	k8xC	a
nechce	chtít	k5eNaImIp3nS	chtít
je	on	k3xPp3gInPc4	on
pustit	pustit	k5eAaPmF	pustit
<g/>
.	.	kIx.	.
</s>
<s>
Sibylla	Sibylla	k6eAd1	Sibylla
mu	on	k3xPp3gMnSc3	on
předhodí	předhodit	k5eAaPmIp3nP	předhodit
koláč	koláč	k1gInSc4	koláč
s	s	k7c7	s
uspávajícími	uspávající	k2eAgInPc7d1	uspávající
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
dál	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
volná	volný	k2eAgFnSc1d1	volná
<g/>
,	,	kIx,	,
míří	mířit	k5eAaImIp3nS	mířit
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dlí	dlít	k5eAaImIp3nP	dlít
předčasně	předčasně	k6eAd1	předčasně
zesnulá	zesnulý	k2eAgNnPc1d1	zesnulé
nemluvňata	nemluvně	k1gNnPc1	nemluvně
<g/>
,	,	kIx,	,
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
odsouzení	odsouzený	k2eAgMnPc1d1	odsouzený
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
soudí	soudit	k5eAaImIp3nS	soudit
soudce	soudce	k1gMnSc1	soudce
Mínos	Mínos	k1gMnSc1	Mínos
<g/>
,	,	kIx,	,
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
místům	místo	k1gNnPc3	místo
patří	patřit	k5eAaImIp3nS	patřit
také	také	k6eAd1	také
sebevrazi	sebevrah	k1gMnPc1	sebevrah
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
Pole	pole	k1gNnSc4	pole
smutku	smutek	k1gInSc2	smutek
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obývají	obývat	k5eAaImIp3nP	obývat
nešťastní	šťastný	k2eNgMnPc1d1	nešťastný
milenci	milenec	k1gMnPc1	milenec
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
Aeneas	Aeneas	k1gMnSc1	Aeneas
potkává	potkávat	k5eAaImIp3nS	potkávat
Dídó	Dídó	k1gFnSc4	Dídó
<g/>
,	,	kIx,	,
obrací	obracet	k5eAaImIp3nS	obracet
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
s	s	k7c7	s
omluvou	omluva	k1gFnSc7	omluva
a	a	k8xC	a
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
odvrací	odvracet	k5eAaImIp3nS	odvracet
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
a	a	k8xC	a
prchá	prchat	k5eAaImIp3nS	prchat
za	za	k7c7	za
manželem	manžel	k1gMnSc7	manžel
Sychaem	Sycha	k1gMnSc7	Sycha
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
kráčí	kráčet	k5eAaImIp3nS	kráčet
místy	místy	k6eAd1	místy
padlých	padlý	k2eAgMnPc2d1	padlý
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
ho	on	k3xPp3gNnSc4	on
Trójané	Trójan	k1gMnPc1	Trójan
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
utíkají	utíkat	k5eAaImIp3nP	utíkat
strachy	strach	k1gInPc7	strach
<g/>
,	,	kIx,	,
potkává	potkávat	k5eAaImIp3nS	potkávat
Priamova	Priamův	k2eAgMnSc4d1	Priamův
syna	syn	k1gMnSc4	syn
Déifoba	Déifoba	k1gFnSc1	Déifoba
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
zažil	zažít	k5eAaPmAgMnS	zažít
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
že	že	k8xS	že
Helena	Helena	k1gFnSc1	Helena
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
po	po	k7c6	po
Paridově	Paridův	k2eAgFnSc6d1	Paridova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Déifoba	Déifoba	k1gFnSc1	Déifoba
odzbrojila	odzbrojit	k5eAaPmAgFnS	odzbrojit
a	a	k8xC	a
Meneláos	Meneláos	k1gInSc4	Meneláos
jej	on	k3xPp3gInSc4	on
zabil	zabít	k5eAaPmAgInS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Sibylla	Sibylla	k6eAd1	Sibylla
řeč	řeč	k1gFnSc1	řeč
utíná	utínat	k5eAaImIp3nS	utínat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
mají	mít	k5eAaImIp3nP	mít
vydat	vydat	k5eAaPmF	vydat
nalevo	nalevo	k6eAd1	nalevo
do	do	k7c2	do
Tartaru	Tartar	k1gInSc2	Tartar
trestnice	trestnice	k1gFnSc2	trestnice
provinilých	provinilý	k2eAgInPc2d1	provinilý
a	a	k8xC	a
napravo	napravo	k6eAd1	napravo
do	do	k7c2	do
sídla	sídlo	k1gNnSc2	sídlo
blažených	blažený	k2eAgFnPc2d1	blažená
–	–	k?	–
Élysia	Élysium	k1gNnSc2	Élysium
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
vidí	vidět	k5eAaImIp3nS	vidět
nedobytnou	dobytný	k2eNgFnSc4d1	nedobytná
pevnost	pevnost	k1gFnSc4	pevnost
obehnanou	obehnaný	k2eAgFnSc4d1	obehnaná
třemi	tři	k4xCgFnPc7	tři
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc4	vstup
beze	beze	k7c2	beze
spánku	spánek	k1gInSc2	spánek
střeží	střežit	k5eAaImIp3nS	střežit
bohyně	bohyně	k1gFnSc1	bohyně
pomsty	pomsta	k1gFnSc2	pomsta
Tísifoné	Tísifoná	k1gFnSc2	Tísifoná
<g/>
,	,	kIx,	,
kolem	kolem	k6eAd1	kolem
plyne	plynout	k5eAaImIp3nS	plynout
plamenná	plamenný	k2eAgFnSc1d1	plamenná
řeka	řeka	k1gFnSc1	řeka
Flegethón	Flegethón	k1gInSc1	Flegethón
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
slyší	slyšet	k5eAaImIp3nS	slyšet
ozývající	ozývající	k2eAgMnSc1d1	ozývající
se	se	k3xPyFc4	se
nářek	nářek	k1gInSc1	nářek
a	a	k8xC	a
bití	bití	k1gNnSc1	bití
<g/>
,	,	kIx,	,
ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
Sibylly	Sibylla	k1gMnSc2	Sibylla
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
soudci	soudce	k1gMnSc6	soudce
Rhadamanthovi	Rhadamanth	k1gMnSc6	Rhadamanth
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
vede	vést	k5eAaImIp3nS	vést
výslech	výslech	k1gInSc1	výslech
<g/>
,	,	kIx,	,
nutí	nutit	k5eAaImIp3nS	nutit
k	k	k7c3	k
doznání	doznání	k1gNnSc3	doznání
<g/>
,	,	kIx,	,
předává	předávat	k5eAaImIp3nS	předávat
Erínyím	Erínyí	k1gNnSc7	Erínyí
táhnoucím	táhnoucí	k2eAgNnSc7d1	táhnoucí
odsouzence	odsouzenec	k1gMnSc4	odsouzenec
do	do	k7c2	do
jámy	jáma	k1gFnSc2	jáma
Tartaru	Tartar	k1gInSc2	Tartar
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
provinilce	provinilec	k1gMnPc4	provinilec
Titány	Titán	k1gMnPc4	Titán
<g/>
,	,	kIx,	,
obry	obr	k1gMnPc4	obr
Ótose	Ótos	k1gMnSc4	Ótos
a	a	k8xC	a
Efialta	Efialt	k1gMnSc4	Efialt
<g/>
,	,	kIx,	,
krále	král	k1gMnSc4	král
Salomónea	Salomóneus	k1gMnSc4	Salomóneus
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Titya	Tityus	k1gMnSc4	Tityus
<g/>
,	,	kIx,	,
Pertithoa	Pertithous	k1gMnSc4	Pertithous
<g/>
,	,	kIx,	,
Ixiona	Ixion	k1gMnSc4	Ixion
<g/>
,	,	kIx,	,
Peirithoa	Peirithous	k1gMnSc4	Peirithous
<g/>
,	,	kIx,	,
Thésea	Théseus	k1gMnSc4	Théseus
a	a	k8xC	a
Flegya	Flegyus	k1gMnSc4	Flegyus
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Sibylla	Sibylla	k1gFnSc1	Sibylla
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počet	počet	k1gInSc1	počet
trestů	trest	k1gInPc2	trest
i	i	k8xC	i
provinilců	provinilec	k1gMnPc2	provinilec
je	být	k5eAaImIp3nS	být
bezpočet	bezpočet	k1gInSc1	bezpočet
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
má	mít	k5eAaImIp3nS	mít
splnit	splnit	k5eAaPmF	splnit
úkol	úkol	k1gInSc4	úkol
–	–	k?	–
odevzdat	odevzdat	k5eAaPmF	odevzdat
dar	dar	k1gInSc4	dar
Proserpině	Proserpina	k1gFnSc3	Proserpina
<g/>
,	,	kIx,	,
omývá	omývat	k5eAaImIp3nS	omývat
se	se	k3xPyFc4	se
očistnou	očistný	k2eAgFnSc7d1	očistná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
vsadil	vsadit	k5eAaPmAgInS	vsadit
zlatou	zlatý	k2eAgFnSc4d1	zlatá
ratolest	ratolest	k1gFnSc4	ratolest
u	u	k7c2	u
prahu	práh	k1gInSc2	práh
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Élysia	Élysium	k1gNnSc2	Élysium
<g/>
,	,	kIx,	,
místa	místo	k1gNnSc2	místo
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
tance	tanec	k1gInSc2	tanec
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
vidí	vidět	k5eAaImIp3nS	vidět
pěvce	pěvec	k1gMnSc4	pěvec
Orfea	Orfeus	k1gMnSc4	Orfeus
<g/>
,	,	kIx,	,
bezúhonné	bezúhonný	k2eAgMnPc4d1	bezúhonný
a	a	k8xC	a
zasloužilé	zasloužilý	k2eAgMnPc4d1	zasloužilý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
básníky	básník	k1gMnPc4	básník
a	a	k8xC	a
umělce	umělec	k1gMnPc4	umělec
-	-	kIx~	-
všechny	všechen	k3xTgFnPc4	všechen
zdobené	zdobený	k2eAgFnPc4d1	zdobená
bělostnými	bělostný	k2eAgInPc7d1	bělostný
vínky	vínek	k1gInPc7	vínek
<g/>
.	.	kIx.	.
</s>
<s>
Básníka	básník	k1gMnSc2	básník
Músáia	Músáius	k1gMnSc2	Músáius
ptají	ptat	k5eAaImIp3nP	ptat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
Anchísem	Anchís	k1gMnSc7	Anchís
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
snaží	snažit	k5eAaImIp3nP	snažit
objemout	objemout	k5eAaPmF	objemout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nehmotného	hmotný	k2eNgMnSc4d1	nehmotný
ducha	duch	k1gMnSc4	duch
to	ten	k3xDgNnSc1	ten
nejde	jít	k5eNaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Zahlédne	zahlédnout	k5eAaPmIp3nS	zahlédnout
údolí	údolí	k1gNnPc4	údolí
řeky	řeka	k1gFnSc2	řeka
zapomnění	zapomnění	k1gNnSc2	zapomnění
Léthé	Léthé	k1gFnSc2	Léthé
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
celé	celý	k2eAgFnSc2d1	celá
zástupy	zástup	k1gInPc1	zástup
lidí	člověk	k1gMnPc2	člověk
předurčených	předurčený	k2eAgFnPc2d1	předurčená
osudem	osud	k1gInSc7	osud
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
jiného	jiný	k2eAgNnSc2d1	jiné
těla	tělo	k1gNnSc2	tělo
pijí	pít	k5eAaImIp3nP	pít
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Anchíses	Anchíses	k1gInSc4	Anchíses
vypráví	vyprávět	k5eAaImIp3nP	vyprávět
Aeneovi	Aeneův	k2eAgMnPc1d1	Aeneův
o	o	k7c6	o
světové	světový	k2eAgFnSc6d1	světová
duši	duše	k1gFnSc6	duše
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
všichni	všechen	k3xTgMnPc1	všechen
živí	živý	k2eAgMnPc1d1	živý
tvorové	tvor	k1gMnPc1	tvor
mající	mající	k2eAgFnSc4d1	mající
ohnivou	ohnivý	k2eAgFnSc4d1	ohnivá
duši	duše	k1gFnSc4	duše
nebeského	nebeský	k2eAgInSc2d1	nebeský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
odívá	odívat	k5eAaImIp3nS	odívat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Duše	duše	k1gFnSc1	duše
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
prochází	procházet	k5eAaImIp3nS	procházet
očistou	očista	k1gFnSc7	očista
živlů	živel	k1gInPc2	živel
od	od	k7c2	od
tělesnosti	tělesnost	k1gFnSc2	tělesnost
a	a	k8xC	a
provinění	provinění	k1gNnSc2	provinění
<g/>
,	,	kIx,	,
jen	jen	k9	jen
málo	málo	k4c1	málo
duší	duše	k1gFnPc2	duše
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
Élysiu	Élysium	k1gNnSc6	Élysium
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnSc1d1	ostatní
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oběh	oběh	k1gInSc1	oběh
trvá	trvat	k5eAaImIp3nS	trvat
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Anchísés	Anchísés	k1gInSc1	Anchísés
přivádí	přivádět	k5eAaImIp3nS	přivádět
Aenea	Aeneas	k1gMnSc4	Aeneas
k	k	k7c3	k
shluku	shluk	k1gInSc3	shluk
duší	duše	k1gFnPc2	duše
budoucích	budoucí	k2eAgInPc6d1	budoucí
pokoleních	pokolení	k1gNnPc6	pokolení
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
nacházející	nacházející	k2eAgMnSc1d1	nacházející
vládce	vládce	k1gMnSc1	vládce
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Říma	Řím	k1gInSc2	Řím
–	–	k?	–
Romula	Romulus	k1gMnSc2	Romulus
<g/>
.	.	kIx.	.
</s>
<s>
Prorokuje	prorokovat	k5eAaImIp3nS	prorokovat
slávu	sláva	k1gFnSc4	sláva
rodu	rod	k1gInSc2	rod
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
Iula	Iul	k1gInSc2	Iul
Askania	Askanium	k1gNnSc2	Askanium
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vzejde	vzejít	k5eAaPmIp3nS	vzejít
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Augustus	Augustus	k1gMnSc1	Augustus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
přinést	přinést	k5eAaPmF	přinést
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
<s>
Předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
světovládu	světovláda	k1gFnSc4	světovláda
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
římské	římský	k2eAgMnPc4d1	římský
krále	král	k1gMnPc4	král
a	a	k8xC	a
Bruta	Bruta	k1gMnSc1	Bruta
zakladatele	zakladatel	k1gMnSc2	zakladatel
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
velké	velký	k2eAgFnSc3d1	velká
osobnosti	osobnost	k1gFnSc3	osobnost
časů	čas	k1gInPc2	čas
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
slavné	slavný	k2eAgFnSc6d1	slavná
velitele	velitel	k1gMnSc4	velitel
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Poslání	poslání	k1gNnSc1	poslání
Říma	Řím	k1gInSc2	Řím
je	být	k5eAaImIp3nS	být
podrobit	podrobit	k5eAaPmF	podrobit
si	se	k3xPyFc3	se
svět	svět	k1gInSc4	svět
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
udržel	udržet	k5eAaPmAgInS	udržet
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
smutný	smutný	k2eAgMnSc1d1	smutný
Marcus	Marcus	k1gMnSc1	Marcus
Claudius	Claudius	k1gMnSc1	Claudius
Marcellus	Marcellus	k1gMnSc1	Marcellus
(	(	kIx(	(
<g/>
zeť	zeť	k1gMnSc1	zeť
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přes	přes	k7c4	přes
vynikající	vynikající	k2eAgFnPc4d1	vynikající
vlastnosti	vlastnost	k1gFnPc4	vlastnost
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
rozkvětu	rozkvět	k1gInSc6	rozkvět
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Anchíses	Anchíses	k1gInSc1	Anchíses
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
v	v	k7c6	v
Aeneovi	Aeneův	k2eAgMnPc1d1	Aeneův
nadšení	nadšení	k1gNnSc2	nadšení
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
mu	on	k3xPp3gMnSc3	on
boje	boj	k1gInPc1	boj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ho	on	k3xPp3gInSc4	on
čekají	čekat	k5eAaImIp3nP	čekat
i	i	k9	i
budoucí	budoucí	k2eAgFnSc4d1	budoucí
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
a	a	k8xC	a
Sibylla	Sibylla	k1gMnSc1	Sibylla
procházejí	procházet	k5eAaImIp3nP	procházet
kolem	kolem	k7c2	kolem
dvojí	dvojí	k4xRgFnSc2	dvojí
brány	brána	k1gFnSc2	brána
boha	bůh	k1gMnSc4	bůh
Snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
rohu	roh	k1gInSc2	roh
pro	pro	k7c4	pro
skutečné	skutečný	k2eAgMnPc4d1	skutečný
duchy	duch	k1gMnPc4	duch
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
ze	z	k7c2	z
slonoviny	slonovina	k1gFnSc2	slonovina
pro	pro	k7c4	pro
klamné	klamný	k2eAgFnPc4d1	klamná
vidiny	vidina	k1gFnPc4	vidina
<g/>
.	.	kIx.	.
</s>
<s>
Aeneas	Aeneas	k1gMnSc1	Aeneas
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
slonovinovou	slonovinový	k2eAgFnSc7d1	slonovinová
branou	brána	k1gFnSc7	brána
spěchá	spěchat	k5eAaImIp3nS	spěchat
k	k	k7c3	k
lodím	loď	k1gFnPc3	loď
a	a	k8xC	a
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Aeneis	Aeneis	k1gFnSc1	Aeneis
hluboce	hluboko	k6eAd1	hluboko
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
celou	celý	k2eAgFnSc7d1	celá
římskou	římska	k1gFnSc7	římska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
středověkou	středověký	k2eAgFnSc4d1	středověká
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
homérskými	homérský	k2eAgInPc7d1	homérský
eposy	epos	k1gInPc7	epos
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
jejím	její	k3xOp3gNnPc3	její
nejzákladnějším	základní	k2eAgNnPc3d3	nejzákladnější
dílům	dílo	k1gNnPc3	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
klasického	klasický	k2eAgInSc2d1	klasický
kánonu	kánon	k1gInSc2	kánon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
staletí	staletí	k1gNnSc4	staletí
základem	základ	k1gInSc7	základ
studia	studio	k1gNnSc2	studio
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
filologie	filologie	k1gFnSc2	filologie
<g/>
.	.	kIx.	.
</s>
<s>
Aeneis	Aeneis	k1gFnSc1	Aeneis
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
vzorem	vzor	k1gInSc7	vzor
i	i	k8xC	i
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
Dantovu	Dantův	k2eAgFnSc4d1	Dantova
Božskou	božský	k2eAgFnSc4d1	božská
komedii	komedie	k1gFnSc4	komedie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Vergilius	Vergilius	k1gMnSc1	Vergilius
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
duševní	duševní	k2eAgFnSc2d1	duševní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
překlady	překlad	k1gInPc1	překlad
do	do	k7c2	do
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
také	také	k9	také
hudební	hudební	k2eAgNnSc4d1	hudební
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
:	:	kIx,	:
opera	opera	k1gFnSc1	opera
La	la	k1gNnSc2	la
Didone	Didon	k1gInSc5	Didon
od	od	k7c2	od
Francesco	Francesco	k1gMnSc1	Francesco
Cavalliho	Cavalli	k1gMnSc2	Cavalli
(	(	kIx(	(
<g/>
1641	[number]	k4	1641
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
anglická	anglický	k2eAgFnSc1d1	anglická
opera	opera	k1gFnSc1	opera
Dido	Dido	k1gMnSc1	Dido
and	and	k?	and
Aeneas	Aeneas	k1gMnSc1	Aeneas
(	(	kIx(	(
<g/>
1689	[number]	k4	1689
<g/>
)	)	kIx)	)
Henry	Henry	k1gMnSc1	Henry
Purcella	Purcella	k1gMnSc1	Purcella
a	a	k8xC	a
opera	opera	k1gFnSc1	opera
Les	les	k1gInSc1	les
Troyens	Troyens	k1gInSc1	Troyens
Hectora	Hector	k1gMnSc2	Hector
Berlioze	Berlioz	k1gMnSc2	Berlioz
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
parodie	parodie	k1gFnSc1	parodie
Aeneidy	Aeneida	k1gFnSc2	Aeneida
Ivana	Ivana	k1gFnSc1	Ivana
Kotljarevského	Kotljarevský	k2eAgInSc2d1	Kotljarevský
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
její	její	k3xOp3gNnSc4	její
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
operní	operní	k2eAgNnSc4d1	operní
zpracování	zpracování	k1gNnSc4	zpracování
Mykoly	Mykola	k1gFnSc2	Mykola
Lysenka	lysenka	k1gFnSc1	lysenka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
byla	být	k5eAaImAgFnS	být
Aeneis	Aeneis	k1gFnSc1	Aeneis
přeložena	přeložit	k5eAaPmNgFnS	přeložit
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
časoměrně	časoměrně	k6eAd1	časoměrně
Karlem	Karel	k1gMnSc7	Karel
Vinařickým	Vinařický	k2eAgMnSc7d1	Vinařický
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
a	a	k8xC	a
Antonínem	Antonín	k1gMnSc7	Antonín
Škodou	Škoda	k1gMnSc7	Škoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
v	v	k7c6	v
přízvučném	přízvučný	k2eAgInSc6d1	přízvučný
hexametru	hexametr	k1gInSc6	hexametr
pak	pak	k8xC	pak
Otmarem	Otmar	k1gMnSc7	Otmar
Vaňorným	Vaňorný	k2eAgMnSc7d1	Vaňorný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
VERGILIUS	Vergilius	k1gMnSc1	Vergilius
<g/>
.	.	kIx.	.
</s>
<s>
Aeneis	Aeneis	k1gFnSc1	Aeneis
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
479	[number]	k4	479
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc4	Mertlík
na	na	k7c6	na
základě	základ	k1gInSc6	základ
překladu	překlad	k1gInSc2	překlad
Otmara	Otmar	k1gMnSc2	Otmar
Vaňorného	Vaňorný	k2eAgMnSc2d1	Vaňorný
<g/>
;	;	kIx,	;
doslov	doslov	k1gInSc1	doslov
napsal	napsat	k5eAaBmAgMnS	napsat
Martin	Martin	k1gMnSc1	Martin
C.	C.	kA	C.
Putna	putna	k1gFnSc1	putna
<g/>
)	)	kIx)	)
VERGILIUS	Vergilius	k1gMnSc1	Vergilius
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Virgilia	Virgilia	k1gFnSc1	Virgilia
Maróna	Maróna	k1gFnSc1	Maróna
Spisy	spis	k1gInPc1	spis
básnické	básnický	k2eAgInPc1d1	básnický
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Alois	Alois	k1gMnSc1	Alois
Vinařický	Vinařický	k2eAgMnSc1d1	Vinařický
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
[	[	kIx(	[
<g/>
České	český	k2eAgNnSc1d1	české
museum	museum	k1gNnSc1	museum
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
297,143	[number]	k4	297,143
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Spis	spis	k1gInSc1	spis
Aeneida	Aeneida	k1gFnSc1	Aeneida
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
297	[number]	k4	297
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
