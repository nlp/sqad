<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
(	(	kIx(	(
<g/>
moldavsky	moldavsky	k6eAd1	moldavsky
Republica	Republica	k1gFnSc1	Republica
Moldova	Moldův	k2eAgFnSc1d1	Moldova
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
М	М	k?	М
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
М	М	k?	М
i	i	k8xC	i
М	М	k?	М
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
východoevropský	východoevropský	k2eAgInSc1d1	východoevropský
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
mezi	mezi	k7c7	mezi
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
a	a	k8xC	a
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
necelé	celý	k2eNgInPc4d1	necelý
4	[number]	k4	4
milióny	milión	k4xCgInPc1	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Kišiněv	Kišiněv	k1gFnSc1	Kišiněv
(	(	kIx(	(
<g/>
Chiș	Chiș	k1gFnSc1	Chiș
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úřední	úřední	k2eAgInSc1d1	úřední
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nazýval	nazývat	k5eAaImAgInS	nazývat
moldavština	moldavština	k1gFnSc1	moldavština
<g/>
,	,	kIx,	,
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
dialekt	dialekt	k1gInSc4	dialekt
rumunštiny	rumunština	k1gFnSc2	rumunština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
rumunština	rumunština	k1gFnSc1	rumunština
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
též	též	k9	též
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
a	a	k8xC	a
gagauzština	gagauzština	k1gFnSc1	gagauzština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
republika	republika	k1gFnSc1	republika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
Moldavského	moldavský	k2eAgNnSc2d1	moldavské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
knížectví	knížectví	k1gNnSc2	knížectví
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
záboru	zábor	k1gInSc6	zábor
Rakouskem	Rakousko	k1gNnSc7	Rakousko
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
Bukovina	Bukovina	k1gFnSc1	Bukovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
si	se	k3xPyFc3	se
zbylé	zbylý	k2eAgNnSc4d1	zbylé
území	území	k1gNnSc4	území
rozporcovali	rozporcovat	k5eAaPmAgMnP	rozporcovat
Rusové	Rus	k1gMnPc1	Rus
a	a	k8xC	a
Osmani	Osman	k1gMnPc1	Osman
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
poté	poté	k6eAd1	poté
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
Besarábie	Besarábie	k1gFnSc1	Besarábie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
krátce	krátce	k6eAd1	krátce
existovala	existovat	k5eAaImAgFnS	existovat
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
včleněna	včlenit	k5eAaPmNgFnS	včlenit
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
kontrolované	kontrolovaný	k2eAgFnPc4d1	kontrolovaná
Sověty	Sovět	k1gMnPc7	Sovět
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
autonomní	autonomní	k2eAgFnSc1d1	autonomní
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
Sověti	Sovět	k1gMnPc1	Sovět
i	i	k8xC	i
zbytek	zbytek	k1gInSc1	zbytek
Moldavska	Moldavsko	k1gNnPc4	Moldavsko
obsadili	obsadit	k5eAaPmAgMnP	obsadit
a	a	k8xC	a
začlenili	začlenit	k5eAaPmAgMnP	začlenit
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
Moldavskou	moldavský	k2eAgFnSc4d1	Moldavská
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Znovupřipojení	Znovupřipojení	k1gNnPc1	Znovupřipojení
k	k	k7c3	k
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
Moldavané	Moldavan	k1gMnPc1	Moldavan
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Moldávie	Moldávie	k1gFnSc1	Moldávie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
historické	historický	k2eAgFnSc2d1	historická
země	zem	k1gFnSc2	zem
Moldávie	Moldávie	k1gFnSc2	Moldávie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
Gagauzsko	Gagauzsko	k1gNnSc4	Gagauzsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
turkického	turkický	k2eAgInSc2d1	turkický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
je	být	k5eAaImIp3nS	být
autonomní	autonomní	k2eAgFnSc4d1	autonomní
oblast	oblast	k1gFnSc4	oblast
a	a	k8xC	a
mezinárodně	mezinárodně	k6eAd1	mezinárodně
neuznávaný	uznávaný	k2eNgInSc1d1	neuznávaný
stát	stát	k1gInSc1	stát
Podněstří	Podněstří	k1gFnSc2	Podněstří
<g/>
,	,	kIx,	,
obývané	obývaný	k2eAgFnSc2d1	obývaná
zejména	zejména	k9	zejména
etnickými	etnický	k2eAgMnPc7d1	etnický
Rusy	Rus	k1gMnPc7	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS	Podněstřit
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
<g/>
,	,	kIx,	,
netvoří	tvořit	k5eNaImIp3nP	tvořit
historickou	historický	k2eAgFnSc4d1	historická
součást	součást	k1gFnSc4	součást
Moldávie	Moldávie	k1gFnSc2	Moldávie
ani	ani	k8xC	ani
předválečného	předválečný	k2eAgNnSc2d1	předválečné
"	"	kIx"	"
<g/>
Velkého	velký	k2eAgNnSc2d1	velké
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
je	být	k5eAaImIp3nS	být
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
a	a	k8xC	a
Organizace	organizace	k1gFnSc2	organizace
černomořské	černomořský	k2eAgFnSc2d1	černomořská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
per	pero	k1gNnPc2	pero
capita	capita	k1gFnSc1	capita
<g/>
)	)	kIx)	)
a	a	k8xC	a
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
nejnižší	nízký	k2eAgInSc1d3	nejnižší
index	index	k1gInSc1	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
navštěvovanou	navštěvovaný	k2eAgFnSc7d1	navštěvovaná
zemí	zem	k1gFnSc7	zem
turisty	turista	k1gMnSc2	turista
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
až	až	k6eAd1	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
ale	ale	k9	ale
na	na	k7c4	na
historické	historický	k2eAgNnSc4d1	historické
království	království	k1gNnSc4	království
Moldávie	Moldávie	k1gFnSc2	Moldávie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
a	a	k8xC	a
na	na	k7c6	na
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
a	a	k8xC	a
osmanské	osmanský	k2eAgNnSc4d1	osmanské
období	období	k1gNnSc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Moldavský	moldavský	k2eAgInSc1d1	moldavský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgInS	zformovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1359	[number]	k4	1359
se	se	k3xPyFc4	se
moldavský	moldavský	k2eAgMnSc1d1	moldavský
kníže	kníže	k1gMnSc1	kníže
Bohdan	Bohdana	k1gFnPc2	Bohdana
I.	I.	kA	I.
uherského	uherský	k2eAgInSc2d1	uherský
vlivu	vliv	k1gInSc2	vliv
zbavil	zbavit	k5eAaPmAgInS	zbavit
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
knížetem	kníže	k1gMnSc7	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Moldávie	Moldávie	k1gFnSc1	Moldávie
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
lenní	lenní	k2eAgFnSc2d1	lenní
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
moldavským	moldavský	k2eAgMnSc7d1	moldavský
vládcem	vládce	k1gMnSc7	vládce
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
Štěpán	Štěpán	k1gMnSc1	Štěpán
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
1457	[number]	k4	1457
<g/>
–	–	k?	–
<g/>
1504	[number]	k4	1504
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
knížectví	knížectví	k1gNnSc1	knížectví
ohroženo	ohrožen	k2eAgNnSc1d1	ohroženo
agresí	agrese	k1gFnSc7	agrese
sousedního	sousední	k2eAgNnSc2d1	sousední
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
Štěpán	Štěpán	k1gMnSc1	Štěpán
snažil	snažit	k5eAaImAgMnS	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
alianci	aliance	k1gFnSc4	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1489	[number]	k4	1489
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
Turky	Turek	k1gMnPc4	Turek
donucen	donucen	k2eAgInSc1d1	donucen
platit	platit	k5eAaImF	platit
tribut	tribut	k1gInSc4	tribut
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
poté	poté	k6eAd1	poté
uznal	uznat	k5eAaPmAgMnS	uznat
lenní	lenní	k2eAgFnSc4d1	lenní
závislost	závislost	k1gFnSc4	závislost
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
na	na	k7c6	na
osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
získala	získat	k5eAaPmAgFnS	získat
Moldávie	Moldávie	k1gFnSc1	Moldávie
od	od	k7c2	od
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
Besarábie	Besarábie	k1gFnSc2	Besarábie
–	–	k?	–
historické	historický	k2eAgFnSc2d1	historická
ruské	ruský	k2eAgFnSc2d1	ruská
gubernie	gubernie	k1gFnSc2	gubernie
mezi	mezi	k7c7	mezi
Dněstrem	Dněstr	k1gInSc7	Dněstr
<g/>
,	,	kIx,	,
Prutem	prut	k1gInSc7	prut
a	a	k8xC	a
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
připojení	připojení	k1gNnSc4	připojení
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Moldavského	moldavský	k2eAgNnSc2d1	moldavské
knížectví	knížectví	k1gNnSc2	knížectví
k	k	k7c3	k
Ruskému	ruský	k2eAgNnSc3d1	ruské
impériu	impérium	k1gNnSc3	impérium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbylé	zbylý	k2eAgNnSc1d1	zbylé
území	území	k1gNnSc1	území
Moldávie	Moldávie	k1gFnSc2	Moldávie
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
Besarábie	Besarábie	k1gFnSc2	Besarábie
s	s	k7c7	s
městy	město	k1gNnPc7	město
Bolhrad	Bolhrada	k1gFnPc2	Bolhrada
<g/>
,	,	kIx,	,
Izmajl	Izmajla	k1gFnPc2	Izmajla
a	a	k8xC	a
Cahul	Cahula	k1gFnPc2	Cahula
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
Moldavskému	moldavský	k2eAgNnSc3d1	moldavské
knížectví	knížectví	k1gNnSc3	knížectví
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
spojení	spojení	k1gNnSc2	spojení
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
a	a	k8xC	a
Valašska	Valašsko	k1gNnSc2	Valašsko
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Spojených	spojený	k2eAgFnPc2d1	spojená
knížectví	knížectví	k1gNnPc2	knížectví
Moldávie	Moldávie	k1gFnSc2	Moldávie
a	a	k8xC	a
Valašska	Valašsko	k1gNnSc2	Valašsko
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
Principatele	Principatel	k1gMnSc2	Principatel
Unite	Unit	k1gInSc5	Unit
ale	ale	k9	ale
Moldovei	Moldovei	k1gNnSc6	Moldovei
ș	ș	k?	ș
Ț	Ț	k?	Ț
Româneș	Româneș	k1gFnPc2	Româneș
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
Rumunské	rumunský	k2eAgNnSc4d1	rumunské
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
přičleněna	přičlenit	k5eAaPmNgFnS	přičlenit
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
byla	být	k5eAaImAgFnS	být
Besarábie	Besarábie	k1gFnSc1	Besarábie
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
dějinách	dějiny	k1gFnPc6	dějiny
v	v	k7c6	v
celku	celek	k1gInSc6	celek
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
Rumunsku	Rumunsko	k1gNnSc3	Rumunsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
však	však	k9	však
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
neuznával	uznávat	k5eNaImAgInS	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
rumunské	rumunský	k2eAgFnSc2d1	rumunská
administrace	administrace	k1gFnSc2	administrace
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Besarábie	Besarábie	k1gFnSc2	Besarábie
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
13	[number]	k4	13
žup	župa	k1gFnPc2	župa
<g/>
.	.	kIx.	.
</s>
<s>
Hotin	Hotin	k1gMnSc1	Hotin
(	(	kIx(	(
<g/>
Chotim	Chotim	k1gMnSc1	Chotim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Soroca	Soroca	k1gFnSc1	Soroca
<g/>
,	,	kIx,	,
Bălț	Bălț	k1gFnSc1	Bălț
<g/>
,	,	kIx,	,
Orhei	Orhei	k1gNnSc1	Orhei
<g/>
,	,	kIx,	,
Lăpuș	Lăpuș	k1gFnSc1	Lăpuș
<g/>
,	,	kIx,	,
Tighina	Tighina	k1gFnSc1	Tighina
<g/>
,	,	kIx,	,
Cahul	Cahul	k1gInSc1	Cahul
<g/>
,	,	kIx,	,
Cetatea	Cetatea	k1gMnSc1	Cetatea
Albă	Albă	k1gMnSc1	Albă
<g/>
,	,	kIx,	,
Ismail	Ismail	k1gMnSc1	Ismail
(	(	kIx(	(
<g/>
Izmail	Izmail	k1gMnSc1	Izmail
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fălciu	Fălcium	k1gNnSc6	Fălcium
<g/>
,	,	kIx,	,
Iaș	Iaș	k1gFnSc1	Iaș
(	(	kIx(	(
<g/>
Jasy	Jasy	k1gInPc1	Jasy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Botoș	Botoș	k1gFnSc2	Botoș
a	a	k8xC	a
Dorohoi	Doroho	k1gFnSc2	Doroho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
obsadil	obsadit	k5eAaPmAgInS	obsadit
celou	celý	k2eAgFnSc4d1	celá
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
správou	správa	k1gFnSc7	správa
byla	být	k5eAaImAgFnS	být
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Besarábie	Besarábie	k1gFnSc2	Besarábie
(	(	kIx(	(
<g/>
Budžak	Budžak	k1gInSc4	Budžak
<g/>
)	)	kIx)	)
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
Ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
sloučena	sloučit	k5eAaPmNgFnS	sloučit
s	s	k7c7	s
Moldavskou	moldavský	k2eAgFnSc7d1	Moldavská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
SSR	SSR	kA	SSR
(	(	kIx(	(
<g/>
Podněstřím	Podněstří	k1gNnSc7	Podněstří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sloučených	sloučený	k2eAgFnPc2d1	sloučená
oblastí	oblast	k1gFnPc2	oblast
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
válku	válka	k1gFnSc4	válka
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
obsadily	obsadit	k5eAaPmAgFnP	obsadit
Besarábii	Besarábie	k1gFnSc4	Besarábie
vojska	vojsko	k1gNnSc2	vojsko
fašistické	fašistický	k2eAgFnSc2d1	fašistická
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Besarábie	Besarábie	k1gFnSc1	Besarábie
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
pod	pod	k7c7	pod
rumunskou	rumunský	k2eAgFnSc7d1	rumunská
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
trval	trvat	k5eAaImAgInS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
německá	německý	k2eAgFnSc1d1	německá
a	a	k8xC	a
rumunská	rumunský	k2eAgNnPc1d1	rumunské
vojska	vojsko	k1gNnPc1	vojsko
z	z	k7c2	z
Besarábie	Besarábie	k1gFnSc2	Besarábie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
bylo	být	k5eAaImAgNnS	být
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
součástí	součást	k1gFnPc2	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jako	jako	k8xC	jako
Moldavská	moldavský	k2eAgNnPc4d1	moldavské
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Besarábie	Besarábie	k1gFnSc2	Besarábie
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
získala	získat	k5eAaPmAgFnS	získat
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
SSR	SSR	kA	SSR
samostatnost	samostatnost	k1gFnSc1	samostatnost
jako	jako	k8xS	jako
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
referendum	referendum	k1gNnSc1	referendum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
současného	současný	k2eAgNnSc2d1	současné
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
je	být	k5eAaImIp3nS	být
Podněstří	Podněstří	k1gFnSc1	Podněstří
(	(	kIx(	(
<g/>
moldavsky	moldavsky	k6eAd1	moldavsky
Nistreană	Nistreană	k1gFnSc1	Nistreană
<g/>
,	,	kIx,	,
rumunsky	rumunsky	k6eAd1	rumunsky
Transnistria	Transnistrium	k1gNnSc2	Transnistrium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
východního	východní	k2eAgInSc2d1	východní
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
Dněstr	Dněstr	k1gInSc4	Dněstr
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
rusky	rusky	k6eAd1	rusky
mluvícího	mluvící	k2eAgNnSc2d1	mluvící
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS	Podněstřit
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
SSSR	SSSR	kA	SSSR
již	již	k6eAd1	již
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
jako	jako	k8xC	jako
Podněsterská	podněsterský	k2eAgFnSc1d1	Podněsterská
moldavská	moldavský	k2eAgFnSc1d1	Moldavská
republika	republika	k1gFnSc1	republika
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Moldavské	moldavský	k2eAgFnSc6d1	Moldavská
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Podněsterské	podněsterský	k2eAgFnSc2d1	Podněsterská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
značně	značně	k6eAd1	značně
destabilizuje	destabilizovat	k5eAaBmIp3nS	destabilizovat
celý	celý	k2eAgInSc4d1	celý
region	region	k1gInSc4	region
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávána	uznáván	k2eAgFnSc1d1	uznávána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
tvoří	tvořit	k5eAaImIp3nS	tvořit
mírně	mírně	k6eAd1	mírně
vyvýšená	vyvýšený	k2eAgFnSc1d1	vyvýšená
a	a	k8xC	a
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
rovina	rovina	k1gFnSc1	rovina
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
Dealul	Dealul	k1gInSc4	Dealul
Bălăneşti	Bălăneşť	k1gFnSc2	Bălăneşť
(	(	kIx(	(
<g/>
430	[number]	k4	430
m.	m.	k?	m.
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
,	,	kIx,	,
Kodra	Kodra	k1gFnSc1	Kodra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
nejzápadnější	západní	k2eAgInPc1d3	nejzápadnější
výběžky	výběžek	k1gInPc1	výběžek
eurasijské	eurasijský	k2eAgFnSc2d1	eurasijská
stepi	step	k1gFnSc2	step
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgInPc7d1	typický
prvky	prvek	k1gInPc7	prvek
reliéfu	reliéf	k1gInSc2	reliéf
jsou	být	k5eAaImIp3nP	být
četné	četný	k2eAgFnPc1d1	četná
úžlabiny	úžlabina	k1gFnPc1	úžlabina
a	a	k8xC	a
doliny	dolina	k1gFnPc1	dolina
–	–	k?	–
výsledek	výsledek	k1gInSc1	výsledek
erozní	erozní	k2eAgInSc1d1	erozní
činnosti	činnost	k1gFnSc3	činnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozoruhodností	pozoruhodnost	k1gFnSc7	pozoruhodnost
reliéfu	reliéf	k1gInSc2	reliéf
jsou	být	k5eAaImIp3nP	být
vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
tovtre	tovtr	k1gInSc5	tovtr
(	(	kIx(	(
<g/>
toltre	toltr	k1gMnSc5	toltr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
okolím	okolí	k1gNnSc7	okolí
zvedají	zvedat	k5eAaImIp3nP	zvedat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
cca	cca	kA	cca
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevznikly	vzniknout	k5eNaPmAgFnP	vzniknout
tektonickou	tektonický	k2eAgFnSc7d1	tektonická
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rify	rif	k1gInPc4	rif
<g/>
,	,	kIx,	,
podmořské	podmořský	k2eAgInPc4d1	podmořský
útesy	útes	k1gInPc4	útes
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
zdejších	zdejší	k2eAgNnPc6d1	zdejší
mořích	moře	k1gNnPc6	moře
před	před	k7c7	před
20	[number]	k4	20
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
tovtrů	tovtr	k1gMnPc2	tovtr
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ukrajinsko-moldavském	ukrajinskooldavský	k2eAgNnSc6d1	ukrajinsko-moldavský
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
,	,	kIx,	,
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
Prutu	prut	k1gInSc6	prut
(	(	kIx(	(
<g/>
cca	cca	kA	cca
200	[number]	k4	200
km	km	kA	km
od	od	k7c2	od
Kišiněva	Kišiněvo	k1gNnSc2	Kišiněvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
protkána	protkat	k5eAaPmNgFnS	protkat
dolinami	dolina	k1gFnPc7	dolina
a	a	k8xC	a
kaňony	kaňon	k1gInPc4	kaňon
přítoků	přítok	k1gInPc2	přítok
horního	horní	k2eAgInSc2d1	horní
Prutu	prut	k1gInSc2	prut
(	(	kIx(	(
<g/>
k	k	k7c3	k
nejkrásnějším	krásný	k2eAgFnPc3d3	nejkrásnější
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
průlom	průlom	k1gInSc4	průlom
řeky	řeka	k1gFnSc2	řeka
Zbruč	Zbruč	k1gFnSc2	Zbruč
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
reliéfu	reliéf	k1gInSc2	reliéf
je	být	k5eAaImIp3nS	být
i	i	k9	i
oblast	oblast	k1gFnSc1	oblast
Sto	sto	k4xCgNnSc4	sto
pahorků	pahorek	k1gInPc2	pahorek
mezi	mezi	k7c7	mezi
Prutem	prut	k1gInSc7	prut
a	a	k8xC	a
Dněstrem	Dněstr	k1gInSc7	Dněstr
(	(	kIx(	(
<g/>
asi	asi	k9	asi
200	[number]	k4	200
km	km	kA	km
od	od	k7c2	od
Kišiněva	Kišiněvo	k1gNnSc2	Kišiněvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
asi	asi	k9	asi
3500	[number]	k4	3500
pahorků	pahorek	k1gInPc2	pahorek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
výše	výše	k1gFnSc1	výše
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
od	od	k7c2	od
1,5	[number]	k4	1,5
do	do	k7c2	do
cca	cca	kA	cca
30	[number]	k4	30
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Až	až	k9	až
75	[number]	k4	75
%	%	kIx~	%
země	zem	k1gFnSc2	zem
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
černozem	černozem	k1gFnSc1	černozem
<g/>
;	;	kIx,	;
lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
činnosti	činnost	k1gFnSc2	činnost
vykácené	vykácený	k2eAgFnSc2d1	vykácená
(	(	kIx(	(
<g/>
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
není	být	k5eNaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
vodní	vodní	k2eAgInPc4d1	vodní
zdroje	zdroj	k1gInPc4	zdroj
–	–	k?	–
ani	ani	k8xC	ani
povrchové	povrchový	k2eAgFnPc1d1	povrchová
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
podzemní	podzemní	k2eAgFnPc1d1	podzemní
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
východoevropské	východoevropský	k2eAgFnPc1d1	východoevropská
řeky	řeka	k1gFnPc1	řeka
–	–	k?	–
Dněstr	Dněstr	k1gInSc1	Dněstr
a	a	k8xC	a
Prut	prut	k1gInSc1	prut
–	–	k?	–
tvoří	tvořit	k5eAaImIp3nS	tvořit
její	její	k3xOp3gFnSc4	její
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
i	i	k9	i
k	k	k7c3	k
Dunaji	Dunaj	k1gInSc3	Dunaj
–	–	k?	–
asi	asi	k9	asi
0,5	[number]	k4	0,5
km	km	kA	km
úsek	úsek	k1gInSc4	úsek
břehu	břeh	k1gInSc2	břeh
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Prutu	prut	k1gInSc2	prut
a	a	k8xC	a
Dunaje	Dunaj	k1gInSc2	Dunaj
u	u	k7c2	u
vesnice	vesnice	k1gFnSc2	vesnice
Giurgiuleşti	Giurgiuleşť	k1gFnSc2	Giurgiuleşť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přilehlých	přilehlý	k2eAgFnPc6d1	přilehlá
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
bažinatá	bažinatý	k2eAgNnPc4d1	bažinaté
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgNnSc1d1	mírné
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
klima	klima	k1gNnSc1	klima
s	s	k7c7	s
teplým	teplý	k2eAgNnSc7d1	teplé
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
chladnou	chladný	k2eAgFnSc7d1	chladná
zimou	zima	k1gFnSc7	zima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teploty	teplota	k1gFnPc1	teplota
klesají	klesat	k5eAaImIp3nP	klesat
pod	pod	k7c4	pod
bod	bod	k1gInSc4	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Prší	pršet	k5eAaImIp3nS	pršet
zejména	zejména	k9	zejména
v	v	k7c6	v
teplejších	teplý	k2eAgInPc6d2	teplejší
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
dešťové	dešťový	k2eAgInPc1d1	dešťový
přívaly	příval	k1gInPc1	příval
přicházejí	přicházet	k5eAaImIp3nP	přicházet
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Kišiněvu	Kišiněv	k1gInSc6	Kišiněv
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
-5	-5	k4	-5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
23	[number]	k4	23
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgNnSc1d1	unitární
<g/>
,	,	kIx,	,
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
dokumentem	dokument	k1gInSc7	dokument
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
měněna	měnit	k5eAaImNgFnS	měnit
pouze	pouze	k6eAd1	pouze
dvoutřetinovou	dvoutřetinový	k2eAgFnSc7d1	dvoutřetinová
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
změny	změna	k1gFnSc2	změna
nemůžou	nemůžou	k?	nemůžou
být	být	k5eAaImF	být
provedeny	provést	k5eAaPmNgInP	provést
během	během	k7c2	během
stavu	stav	k1gInSc2	stav
nouze	nouze	k1gFnSc2	nouze
nebo	nebo	k8xC	nebo
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInPc1d1	ústavní
dodatky	dodatek	k1gInPc1	dodatek
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc2d1	státní
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
,	,	kIx,	,
nezávislostí	nezávislost	k1gFnPc2	nezávislost
nebo	nebo	k8xC	nebo
státní	státní	k2eAgFnSc2d1	státní
jednoty	jednota	k1gFnSc2	jednota
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
prosazeny	prosadit	k5eAaPmNgInP	prosadit
pouze	pouze	k6eAd1	pouze
vůli	vůle	k1gFnSc3	vůle
většiny	většina	k1gFnSc2	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
legislativním	legislativní	k2eAgInSc7d1	legislativní
orgánem	orgán	k1gInSc7	orgán
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
101	[number]	k4	101
<g/>
členný	členný	k2eAgInSc1d1	členný
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
poslanci	poslanec	k1gMnPc1	poslanec
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
většinu	většina	k1gFnSc4	většina
59	[number]	k4	59
křesel	křeslo	k1gNnPc2	křeslo
Aliance	aliance	k1gFnSc2	aliance
pro	pro	k7c4	pro
Evropskou	evropský	k2eAgFnSc4d1	Evropská
integraci	integrace	k1gFnSc4	integrace
tvořená	tvořený	k2eAgNnPc4d1	tvořené
Liberálně	liberálně	k6eAd1	liberálně
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
Liberální	liberální	k2eAgFnSc7d1	liberální
stranou	strana	k1gFnSc7	strana
a	a	k8xC	a
Demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
opoziční	opoziční	k2eAgFnSc7d1	opoziční
stranou	strana	k1gFnSc7	strana
je	být	k5eAaImIp3nS	být
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Moldavské	moldavský	k2eAgFnSc2d1	Moldavská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
drží	držet	k5eAaImIp3nS	držet
42	[number]	k4	42
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
moldavský	moldavský	k2eAgMnSc1d1	moldavský
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zvolení	zvolení	k1gNnSc3	zvolení
prezidenta	prezident	k1gMnSc2	prezident
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
většina	většina	k1gFnSc1	většina
tří	tři	k4xCgFnPc2	tři
pětin	pětina	k1gFnPc2	pětina
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
je	být	k5eAaImIp3nS	být
premiér	premiér	k1gMnSc1	premiér
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
ústava	ústava	k1gFnSc1	ústava
také	také	k9	také
ustanovuje	ustanovovat	k5eAaImIp3nS	ustanovovat
nezávislý	závislý	k2eNgInSc1d1	nezávislý
šestičlenný	šestičlenný	k2eAgInSc1d1	šestičlenný
ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
největších	veliký	k2eAgFnPc2d3	veliký
bank	banka	k1gFnPc2	banka
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
ukradena	ukraden	k2eAgFnSc1d1	ukradena
jedna	jeden	k4xCgFnSc1	jeden
miliarda	miliarda	k4xCgFnSc1	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
celou	celý	k2eAgFnSc4d1	celá
jednu	jeden	k4xCgFnSc4	jeden
osminu	osmina	k1gFnSc4	osmina
moldavského	moldavský	k2eAgInSc2d1	moldavský
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
korupce	korupce	k1gFnSc2	korupce
zatčen	zatčen	k2eAgMnSc1d1	zatčen
starosta	starosta	k1gMnSc1	starosta
Kišiněva	Kišiněvo	k1gNnSc2	Kišiněvo
a	a	k8xC	a
místopředseda	místopředseda	k1gMnSc1	místopředseda
proevropské	proevropský	k2eAgFnSc2d1	proevropská
Liberální	liberální	k2eAgFnSc2d1	liberální
strany	strana	k1gFnSc2	strana
Dorin	Dorin	k1gMnSc1	Dorin
Chirtoacă	Chirtoacă	k1gMnSc1	Chirtoacă
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moldavsko-české	moldavsko-český	k2eAgInPc1d1	moldavsko-český
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
je	být	k5eAaImIp3nS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
mezi	mezi	k7c4	mezi
prioritní	prioritní	k2eAgFnPc4d1	prioritní
země	zem	k1gFnPc4	zem
české	český	k2eAgFnSc2d1	Česká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
především	především	k9	především
v	v	k7c6	v
sektorech	sektor	k1gInPc6	sektor
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
zásobování	zásobování	k1gNnSc2	zásobování
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
sanitace	sanitace	k1gFnSc1	sanitace
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc2	lesnictví
a	a	k8xC	a
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
nevládní	vládní	k2eNgFnSc1d1	nevládní
humanitární	humanitární	k2eAgFnSc1d1	humanitární
organizace	organizace	k1gFnSc1	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
soustředí	soustředit	k5eAaPmIp3nS	soustředit
zejména	zejména	k9	zejména
na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
zkušeností	zkušenost	k1gFnPc2	zkušenost
představitelům	představitel	k1gMnPc3	představitel
samosprávy	samospráva	k1gFnSc2	samospráva
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
občanské	občanský	k2eAgFnSc2d1	občanská
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnSc2	organizace
Adra	Adr	k1gInSc2	Adr
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
děti	dítě	k1gFnPc4	dítě
žijící	žijící	k2eAgFnPc4d1	žijící
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
realizovány	realizován	k2eAgInPc1d1	realizován
projekty	projekt	k1gInPc1	projekt
Aid	Aida	k1gFnPc2	Aida
for	forum	k1gNnPc2	forum
Trade	Trad	k1gInSc5	Trad
a	a	k8xC	a
moldavským	moldavský	k2eAgMnPc3d1	moldavský
studentům	student	k1gMnPc3	student
jsou	být	k5eAaImIp3nP	být
nabízena	nabízen	k2eAgNnPc1d1	nabízeno
česká	český	k2eAgNnPc1d1	české
vládní	vládní	k2eAgNnPc1d1	vládní
stipendia	stipendium	k1gNnPc1	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
v	v	k7c6	v
ČR	ČR	kA	ČR
studovalo	studovat	k5eAaImAgNnS	studovat
celkem	celkem	k6eAd1	celkem
36	[number]	k4	36
moldavských	moldavský	k2eAgMnPc2d1	moldavský
vládních	vládní	k2eAgMnPc2d1	vládní
stipendistů	stipendista	k1gMnPc2	stipendista
<g/>
.	.	kIx.	.
<g/>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
zrušila	zrušit	k5eAaPmAgFnS	zrušit
vízovou	vízový	k2eAgFnSc4d1	vízová
povinnost	povinnost	k1gFnSc4	povinnost
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vstup	vstup	k1gInSc4	vstup
a	a	k8xC	a
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c6	na
území	území	k1gNnSc6	území
Moldavské	moldavský	k2eAgFnSc2d1	Moldavská
republiky	republika	k1gFnSc2	republika
bez	bez	k7c2	bez
víz	vízo	k1gNnPc2	vízo
maximálně	maximálně	k6eAd1	maximálně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
90	[number]	k4	90
dnů	den	k1gInPc2	den
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
data	datum	k1gNnSc2	datum
prvního	první	k4xOgMnSc2	první
vstupu	vstup	k1gInSc2	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
hraničních	hraniční	k2eAgInPc6d1	hraniční
přechodech	přechod	k1gInPc6	přechod
registrace	registrace	k1gFnSc2	registrace
o	o	k7c6	o
pobytu	pobyt	k1gInSc6	pobyt
cizinců	cizinec	k1gMnPc2	cizinec
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
registru	registr	k1gInSc2	registr
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
dřívější	dřívější	k2eAgFnSc1d1	dřívější
přihlašovací	přihlašovací	k2eAgFnSc1d1	přihlašovací
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
32	[number]	k4	32
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
městské	městský	k2eAgInPc1d1	městský
autonomní	autonomní	k2eAgInPc1d1	autonomní
celky	celek	k1gInPc1	celek
(	(	kIx(	(
<g/>
Kišiněv	Kišiněv	k1gMnSc1	Kišiněv
<g/>
,	,	kIx,	,
Bălț	Bălț	k1gMnSc1	Bălț
a	a	k8xC	a
Tighina	Tighina	k1gMnSc1	Tighina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
částečně	částečně	k6eAd1	částečně
autonomní	autonomní	k2eAgInPc1d1	autonomní
regiony	region	k1gInPc1	region
(	(	kIx(	(
<g/>
Gagauzsko	Gagauzsko	k1gNnSc1	Gagauzsko
a	a	k8xC	a
Podněstří	Podněstří	k1gFnSc1	Podněstří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
jistý	jistý	k2eAgInSc4d1	jistý
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
nejchudší	chudý	k2eAgNnSc1d3	nejchudší
zemí	zem	k1gFnSc7	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
drobného	drobný	k2eAgNnSc2d1	drobné
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
příznivé	příznivý	k2eAgNnSc4d1	příznivé
klima	klima	k1gNnSc4	klima
pro	pro	k7c4	pro
intenzivní	intenzivní	k2eAgNnSc4d1	intenzivní
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
pěstování	pěstování	k1gNnSc4	pěstování
některých	některý	k3yIgFnPc2	některý
subtropických	subtropický	k2eAgFnPc2d1	subtropická
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
tabáku	tabák	k1gInSc2	tabák
a	a	k8xC	a
také	také	k9	také
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nedisponuje	disponovat	k5eNaBmIp3nS	disponovat
žádným	žádný	k3yNgNnSc7	žádný
významnějším	významný	k2eAgNnSc7d2	významnější
nerostným	nerostný	k2eAgNnSc7d1	nerostné
bohatstvím	bohatství	k1gNnSc7	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
členem	člen	k1gInSc7	člen
Středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
zóny	zóna	k1gFnSc2	zóna
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Indexu	index	k1gInSc2	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
je	být	k5eAaImIp3nS	být
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
zdaleka	zdaleka	k6eAd1	zdaleka
nejméně	málo	k6eAd3	málo
rozvinutou	rozvinutý	k2eAgFnSc7d1	rozvinutá
evropskou	evropský	k2eAgFnSc7d1	Evropská
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
HDI	HDI	kA	HDI
podobně	podobně	k6eAd1	podobně
vysokým	vysoká	k1gFnPc3	vysoká
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
či	či	k8xC	či
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
paliva	palivo	k1gNnPc1	palivo
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
dovážet	dovážet	k5eAaImF	dovážet
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
energetických	energetický	k2eAgFnPc6d1	energetická
otázkách	otázka	k1gFnPc6	otázka
zcela	zcela	k6eAd1	zcela
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vlastní	vlastní	k2eAgFnSc2d1	vlastní
důležité	důležitý	k2eAgFnSc2d1	důležitá
elektrické	elektrický	k2eAgFnSc2d1	elektrická
rozvodny	rozvodna	k1gFnSc2	rozvodna
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
spolu	spolu	k6eAd1	spolu
mají	mít	k5eAaImIp3nP	mít
vleklé	vleklý	k2eAgInPc1d1	vleklý
spory	spor	k1gInPc1	spor
ohledně	ohledně	k7c2	ohledně
cen	cena	k1gFnPc2	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
těchto	tento	k3xDgInPc2	tento
sporů	spor	k1gInPc2	spor
je	být	k5eAaImIp3nS	být
ruský	ruský	k2eAgInSc1d1	ruský
zákaz	zákaz	k1gInSc1	zákaz
prodeje	prodej	k1gInSc2	prodej
moldavského	moldavský	k2eAgNnSc2d1	moldavské
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
výrobků	výrobek	k1gInPc2	výrobek
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
také	také	k9	také
zdvojnásobení	zdvojnásobení	k1gNnSc4	zdvojnásobení
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
dodávky	dodávka	k1gFnPc4	dodávka
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
kolísání	kolísání	k1gNnSc6	kolísání
cen	cena	k1gFnPc2	cena
těchto	tento	k3xDgFnPc2	tento
komodit	komodita	k1gFnPc2	komodita
velmi	velmi	k6eAd1	velmi
citlivá	citlivý	k2eAgNnPc1d1	citlivé
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
výrazné	výrazný	k2eAgNnSc1d1	výrazné
zpomalení	zpomalení	k1gNnSc1	zpomalení
růstu	růst	k1gInSc2	růst
HDP	HDP	kA	HDP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
země	země	k1gFnSc1	země
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
vykázala	vykázat	k5eAaPmAgFnS	vykázat
pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
růst	růst	k1gInSc4	růst
ekonomiky	ekonomika	k1gFnSc2	ekonomika
o	o	k7c4	o
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
si	se	k3xPyFc3	se
ponechává	ponechávat	k5eAaImIp3nS	ponechávat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
velkou	velký	k2eAgFnSc7d1	velká
částí	část	k1gFnSc7	část
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
trpí	trpět	k5eAaImIp3nS	trpět
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc7d1	velká
inflací	inflace	k1gFnSc7	inflace
a	a	k8xC	a
korupcí	korupce	k1gFnSc7	korupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
pouze	pouze	k6eAd1	pouze
100	[number]	k4	100
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Odhadem	odhad	k1gInSc7	odhad
až	až	k9	až
25	[number]	k4	25
%	%	kIx~	%
práceschopného	práceschopný	k2eAgNnSc2d1	práceschopné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgFnSc1d1	oficiální
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
pouhé	pouhý	k2eAgFnSc2d1	pouhá
2,3	[number]	k4	2,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
je	být	k5eAaImIp3nS	být
prioritní	prioritní	k2eAgFnSc7d1	prioritní
zemí	zem	k1gFnSc7	zem
české	český	k2eAgFnSc2d1	Česká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
některých	některý	k3yIgInPc2	některý
problémů	problém	k1gInPc2	problém
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
prevence	prevence	k1gFnSc2	prevence
migrace	migrace	k1gFnSc2	migrace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
12	[number]	k4	12
300	[number]	k4	300
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
vyasfaltovaná	vyasfaltovaný	k2eAgFnSc1d1	vyasfaltovaná
<g/>
.	.	kIx.	.
</s>
<s>
Moldavské	moldavský	k2eAgNnSc1d1	moldavské
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
Dněstru	Dněstr	k1gInSc3	Dněstr
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
Černému	černý	k2eAgNnSc3d1	černé
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
přes	přes	k7c4	přes
Prut	prut	k1gInSc4	prut
k	k	k7c3	k
Dunaji	Dunaj	k1gInSc3	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
vnitrozemskými	vnitrozemský	k2eAgInPc7d1	vnitrozemský
přístavy	přístav	k1gInPc7	přístav
jsou	být	k5eAaImIp3nP	být
Tighina	Tighina	k1gMnSc1	Tighina
a	a	k8xC	a
Rybnica	Rybnica	k1gMnSc1	Rybnica
na	na	k7c6	na
Dněstru	Dněstr	k1gInSc6	Dněstr
a	a	k8xC	a
Ungheni	Unghen	k2eAgMnPc1d1	Unghen
na	na	k7c6	na
Prutu	prut	k1gInSc6	prut
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kišiněvě	Kišiněva	k1gFnSc6	Kišiněva
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
přímé	přímý	k2eAgNnSc1d1	přímé
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
střediskem	středisko	k1gNnSc7	středisko
slabě	slabě	k6eAd1	slabě
rozvinutého	rozvinutý	k2eAgInSc2d1	rozvinutý
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodné	pozoruhodný	k2eAgInPc1d1	pozoruhodný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
pevnosti	pevnost	k1gFnPc1	pevnost
rozmístěné	rozmístěný	k2eAgFnPc1d1	rozmístěná
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kruhové	kruhový	k2eAgNnSc1d1	kruhové
opevnění	opevnění	k1gNnSc1	opevnění
Soroca	Soroc	k1gInSc2	Soroc
na	na	k7c6	na
Dněstru	Dněstr	k1gInSc6	Dněstr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýhodnějším	výhodný	k2eAgNnSc7d3	nejvýhodnější
obdobím	období	k1gNnSc7	období
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
vinobraní	vinobraní	k1gNnSc1	vinobraní
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
mnohé	mnohý	k2eAgFnPc1d1	mnohá
slavnosti	slavnost	k1gFnPc1	slavnost
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
(	(	kIx(	(
<g/>
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
kontrolovaných	kontrolovaný	k2eAgInPc2d1	kontrolovaný
ústřední	ústřední	k2eAgFnSc7d1	ústřední
vládou	vláda	k1gFnSc7	vláda
<g/>
)	)	kIx)	)
2	[number]	k4	2
998	[number]	k4	998
235	[number]	k4	235
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pokles	pokles	k1gInSc4	pokles
o	o	k7c4	o
11,3	[number]	k4	11,3
<g/>
%	%	kIx~	%
oproti	oproti	k7c3	oproti
údajům	údaj	k1gInPc3	údaj
zjištěným	zjištěný	k2eAgInPc3d1	zjištěný
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
urbanizace	urbanizace	k1gFnSc2	urbanizace
je	být	k5eAaImIp3nS	být
45	[number]	k4	45
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Podněstří	Podněstří	k1gFnSc6	Podněstří
(	(	kIx(	(
<g/>
k	k	k7c3	k
říjnu	říjen	k1gInSc3	říjen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
žilo	žít	k5eAaImAgNnS	žít
475	[number]	k4	475
665	[number]	k4	665
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
pokles	pokles	k1gInSc1	pokles
<g/>
,	,	kIx,	,
o	o	k7c6	o
14,3	[number]	k4	14,3
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
číslu	číslo	k1gNnSc3	číslo
zaznamenanému	zaznamenaný	k2eAgInSc3d1	zaznamenaný
při	pře	k1gFnSc3	pře
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
urbanizace	urbanizace	k1gFnSc2	urbanizace
v	v	k7c6	v
Podněstří	Podněstří	k1gFnSc6	Podněstří
byla	být	k5eAaImAgFnS	být
69,9	[number]	k4	69,9
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Emigrace	emigrace	k1gFnSc1	emigrace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
masový	masový	k2eAgInSc1d1	masový
jev	jev	k1gInSc1	jev
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
demografickou	demografický	k2eAgFnSc4d1	demografická
situaci	situace	k1gFnSc4	situace
a	a	k8xC	a
ekonomiku	ekonomika	k1gFnSc4	ekonomika
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
služba	služba	k1gFnSc1	služba
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
600	[number]	k4	600
000	[number]	k4	000
až	až	k9	až
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
moldavských	moldavský	k2eAgMnPc2d1	moldavský
občanů	občan	k1gMnPc2	občan
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
25	[number]	k4	25
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
<g/>
Panují	panovat	k5eAaImIp3nP	panovat
spory	spor	k1gInPc1	spor
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
mluví	mluvit	k5eAaImIp3nS	mluvit
většina	většina	k1gFnSc1	většina
Moldavanů	Moldavan	k1gMnPc2	Moldavan
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
moldavštinou	moldavština	k1gFnSc7	moldavština
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rumunštinou	rumunština	k1gFnSc7	rumunština
<g/>
.	.	kIx.	.
</s>
<s>
Deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
rumunštinu	rumunština	k1gFnSc4	rumunština
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
uváděla	uvádět	k5eAaImAgFnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
národní	národní	k2eAgInSc1d1	národní
jazyk	jazyk	k1gInSc1	jazyk
Moldavské	moldavský	k2eAgFnSc2d1	Moldavská
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
moldavština	moldavština	k1gFnSc1	moldavština
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vláda	vláda	k1gFnSc1	vláda
přijala	přijmout	k5eAaPmAgFnS	přijmout
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
priorit	priorita	k1gFnPc2	priorita
národní	národní	k2eAgFnSc2d1	národní
politiky	politika	k1gFnSc2	politika
je	být	k5eAaImIp3nS	být
zachování	zachování	k1gNnSc4	zachování
moldavského	moldavský	k2eAgInSc2d1	moldavský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
však	však	k9	však
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
deklarace	deklarace	k1gFnSc1	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc1d2	vyšší
právní	právní	k2eAgFnSc1d1	právní
závaznost	závaznost	k1gFnSc1	závaznost
než	než	k8xS	než
ústava	ústava	k1gFnSc1	ústava
a	a	k8xC	a
státním	státní	k2eAgInSc7d1	státní
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
rumunština	rumunština	k1gFnSc1	rumunština
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
nezahrnovalo	zahrnovat	k5eNaImAgNnS	zahrnovat
Podněstří	Podněstří	k1gFnSc7	Podněstří
<g/>
)	)	kIx)	)
54,6	[number]	k4	54,6
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
jazyk	jazyk	k1gInSc4	jazyk
označilo	označit	k5eAaPmAgNnS	označit
moldavštinu	moldavština	k1gFnSc4	moldavština
<g/>
,	,	kIx,	,
24	[number]	k4	24
%	%	kIx~	%
rumunštinu	rumunština	k1gFnSc4	rumunština
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
pouze	pouze	k6eAd1	pouze
4,1	[number]	k4	4,1
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
jsou	být	k5eAaImIp3nP	být
etničtí	etnický	k2eAgMnPc1d1	etnický
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
ruštinu	ruština	k1gFnSc4	ruština
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
hlavní	hlavní	k2eAgInSc4d1	hlavní
jazyk	jazyk	k1gInSc4	jazyk
označilo	označit	k5eAaPmAgNnS	označit
14,5	[number]	k4	14,5
<g/>
%	%	kIx~	%
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
ji	on	k3xPp3gFnSc4	on
hlavně	hlavně	k9	hlavně
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Gagauzové	Gagauz	k1gMnPc1	Gagauz
a	a	k8xC	a
Bulhaři	Bulhar	k1gMnPc1	Bulhar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	s	k7c7	s
60	[number]	k4	60
<g/>
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
učilo	učít	k5eAaPmAgNnS	učít
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
cizí	cizí	k2eAgInSc4d1	cizí
jazyk	jazyk	k1gInSc4	jazyk
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
nejčastější	častý	k2eAgFnSc7d3	nejčastější
volbou	volba	k1gFnSc7	volba
byla	být	k5eAaImAgFnS	být
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vazby	vazba	k1gFnPc1	vazba
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
silné	silný	k2eAgInPc1d1	silný
kvůli	kvůli	k7c3	kvůli
tradiční	tradiční	k2eAgFnSc3d1	tradiční
francouzské	francouzský	k2eAgFnSc3d1	francouzská
sounáležitosti	sounáležitost	k1gFnSc3	sounáležitost
s	s	k7c7	s
národy	národ	k1gInPc7	národ
mluvícími	mluvící	k2eAgInPc7d1	mluvící
románskými	románský	k2eAgInPc7d1	románský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
tvoří	tvořit	k5eAaImIp3nS	tvořit
93,3	[number]	k4	93,3
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působí	působit	k5eAaImIp3nP	působit
dvě	dva	k4xCgFnPc1	dva
pravoslavné	pravoslavný	k2eAgFnPc1d1	pravoslavná
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
podřízená	podřízený	k2eAgFnSc1d1	podřízená
ruské	ruský	k2eAgFnSc3d1	ruská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
a	a	k8xC	a
Pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
Besarábie	Besarábie	k1gFnSc1	Besarábie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
podřízená	podřízený	k2eAgFnSc1d1	podřízená
Rumunské	rumunský	k2eAgFnSc3d1	rumunská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
církve	církev	k1gFnPc1	církev
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
národní	národní	k2eAgFnSc7d1	národní
církví	církev	k1gFnSc7	církev
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
1,9	[number]	k4	1,9
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
tvoří	tvořit	k5eAaImIp3nP	tvořit
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
množství	množství	k1gNnSc1	množství
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Národnostní	národnostní	k2eAgNnSc4d1	národnostní
složení	složení	k1gNnSc4	složení
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Cahulu	Cahul	k1gInSc2	Cahul
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jediná	jediný	k2eAgFnSc1d1	jediná
česká	český	k2eAgFnSc1d1	Česká
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
Huluboaia	Huluboaia	k1gFnSc1	Huluboaia
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Holubinka	holubinka	k1gFnSc1	holubinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
větších	veliký	k2eAgNnPc2d2	veliký
měst	město	k1gNnPc2	město
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
vylidňování	vylidňování	k1gNnPc4	vylidňování
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
podává	podávat	k5eAaImIp3nS	podávat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
slavný	slavný	k2eAgMnSc1d1	slavný
básník	básník	k1gMnSc1	básník
Mihai	Mihae	k1gFnSc4	Mihae
Eminescu	Eminesca	k1gFnSc4	Eminesca
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
klavíristů	klavírista	k1gMnPc2	klavírista
historie	historie	k1gFnSc2	historie
Anton	Anton	k1gMnSc1	Anton
Rubinstein	Rubinstein	k1gMnSc1	Rubinstein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Emil	Emil	k1gMnSc1	Emil
Loteanu	Loteana	k1gFnSc4	Loteana
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
básník	básník	k1gMnSc1	básník
Grigore	Grigor	k1gInSc5	Grigor
Vieru	Viera	k1gFnSc4	Viera
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřekládanějším	překládaný	k2eAgMnSc7d3	nejpřekládanější
současným	současný	k2eAgMnSc7d1	současný
moldavským	moldavský	k2eAgMnSc7d1	moldavský
autorem	autor	k1gMnSc7	autor
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
Ion	ion	k1gInSc1	ion
Druce	Druce	k1gFnSc2	Druce
<g/>
.	.	kIx.	.
</s>
<s>
Pěvkyní	pěvkyně	k1gFnSc7	pěvkyně
milánské	milánský	k2eAgNnSc4d1	Milánské
La	la	k1gNnSc4	la
Scaly	scát	k5eAaImAgInP	scát
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Maria	Maria	k1gFnSc1	Maria
Bieș	Bieș	k1gFnSc7	Bieș
<g/>
,	,	kIx,	,
meziválečnou	meziválečný	k2eAgFnSc7d1	meziválečná
operní	operní	k2eAgFnSc7d1	operní
hvězdou	hvězda	k1gFnSc7	hvězda
byla	být	k5eAaImAgFnS	být
Maria	Maria	k1gFnSc1	Maria
Cebotari	Cebotar	k1gFnSc2	Cebotar
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
zahajovacímu	zahajovací	k2eAgMnSc3d1	zahajovací
i	i	k8xC	i
závěrečnému	závěrečný	k2eAgInSc3d1	závěrečný
ceremoniálu	ceremoniál	k1gInSc3	ceremoniál
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
Eugen	Eugen	k1gInSc1	Eugen
Doga	doga	k1gFnSc1	doga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
disco	disco	k1gNnSc2	disco
kapela	kapela	k1gFnSc1	kapela
O-Zone	O-Zon	k1gInSc5	O-Zon
tvořená	tvořený	k2eAgFnSc1d1	tvořená
trojicí	trojice	k1gFnSc7	trojice
Dan	Dan	k1gMnSc1	Dan
Bălan	Bălan	k1gMnSc1	Bălan
<g/>
,	,	kIx,	,
Radu	rada	k1gFnSc4	rada
Sîrbu	Sîrb	k1gInSc2	Sîrb
a	a	k8xC	a
Arsenie	Arsenie	k1gFnSc2	Arsenie
Todiraş	Todiraş	k1gFnSc2	Todiraş
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pop-music	popusic	k1gFnSc4	pop-music
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
byla	být	k5eAaImAgFnS	být
mimořádně	mimořádně	k6eAd1	mimořádně
populární	populární	k2eAgFnSc1d1	populární
moldavská	moldavský	k2eAgFnSc1d1	Moldavská
rodačka	rodačka	k1gFnSc1	rodačka
Sofia	Sofia	k1gFnSc1	Sofia
Rotaru	Rotar	k1gInSc2	Rotar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
kišiněvský	kišiněvský	k2eAgMnSc1d1	kišiněvský
rodák	rodák	k1gMnSc1	rodák
Andrew	Andrew	k1gMnSc1	Andrew
Rayel	Rayel	k1gMnSc1	Rayel
<g/>
.	.	kIx.	.
</s>
<s>
Dětskou	dětský	k2eAgFnSc7d1	dětská
pěveckou	pěvecký	k2eAgFnSc7d1	pěvecká
hvězdou	hvězda	k1gFnSc7	hvězda
je	být	k5eAaImIp3nS	být
Cleopatra	Cleopatra	k1gFnSc1	Cleopatra
Stratanová	Stratanový	k2eAgFnSc1d1	Stratanový
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
raper	raper	k1gMnSc1	raper
Calin	Calin	k1gMnSc1	Calin
Panfili	Panfili	k1gMnSc1	Panfili
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
významný	významný	k2eAgMnSc1d1	významný
ruský	ruský	k2eAgMnSc1d1	ruský
architekt	architekt	k1gMnSc1	architekt
Alexej	Alexej	k1gMnSc1	Alexej
Ščusev	Ščusev	k1gFnSc1	Ščusev
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
představitelem	představitel	k1gMnSc7	představitel
architektury	architektura	k1gFnSc2	architektura
stalinské	stalinský	k2eAgFnSc2d1	stalinská
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
nazývána	nazývat	k5eAaImNgFnS	nazývat
stalinský	stalinský	k2eAgInSc4d1	stalinský
imperiální	imperiální	k2eAgInSc4d1	imperiální
styl	styl	k1gInSc4	styl
či	či	k8xC	či
stalinský	stalinský	k2eAgInSc4d1	stalinský
klasicismus	klasicismus	k1gInSc4	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejznámější	známý	k2eAgFnSc7d3	nejznámější
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
Leninovo	Leninův	k2eAgNnSc1d1	Leninovo
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
památka	památka	k1gFnSc1	památka
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Struveho	Struve	k1gMnSc4	Struve
geodetický	geodetický	k2eAgInSc1d1	geodetický
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
řetězcem	řetězec	k1gInSc7	řetězec
triangulačních	triangulační	k2eAgInPc2d1	triangulační
bodů	bod	k1gInPc2	bod
vybudovaným	vybudovaný	k2eAgMnPc3d1	vybudovaný
napříč	napříč	k7c7	napříč
deseti	deset	k4xCc7	deset
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
od	od	k7c2	od
norského	norský	k2eAgInSc2d1	norský
Hammerfestu	Hammerfest	k1gInSc2	Hammerfest
až	až	k9	až
k	k	k7c3	k
Černému	černý	k2eAgNnSc3d1	černé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
2820	[number]	k4	2820
kilometrů	kilometr	k1gInPc2	kilometr
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
ruským	ruský	k2eAgMnSc7d1	ruský
astronomem	astronom	k1gMnSc7	astronom
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Georgem	Georg	k1gMnSc7	Georg
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
von	von	k1gInSc4	von
Struve	Struev	k1gFnSc2	Struev
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
tento	tento	k3xDgInSc4	tento
zápis	zápis	k1gInSc1	zápis
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
,	,	kIx,	,
Estonskem	Estonsko	k1gNnSc7	Estonsko
<g/>
,	,	kIx,	,
Finskem	Finsko	k1gNnSc7	Finsko
<g/>
,	,	kIx,	,
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
,	,	kIx,	,
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
Norskem	Norsko	k1gNnSc7	Norsko
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
Švédskem	Švédsko	k1gNnSc7	Švédsko
a	a	k8xC	a
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
významným	významný	k2eAgFnPc3d1	významná
historickým	historický	k2eAgFnPc3d1	historická
a	a	k8xC	a
architektonickým	architektonický	k2eAgFnPc3d1	architektonická
památkám	památka	k1gFnPc3	památka
patří	patřit	k5eAaImIp3nS	patřit
jeskynní	jeskynní	k2eAgInSc4d1	jeskynní
klášter	klášter	k1gInSc4	klášter
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Tipova	Tipov	k1gInSc2	Tipov
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
ortodoxní	ortodoxní	k2eAgInSc4d1	ortodoxní
klášter	klášter	k1gInSc4	klášter
celé	celý	k2eAgFnSc2d1	celá
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
důležitý	důležitý	k2eAgInSc1d1	důležitý
klášter	klášter	k1gInSc1	klášter
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Cipriana	Ciprian	k1gMnSc2	Ciprian
<g/>
,	,	kIx,	,
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
moldavská	moldavský	k2eAgFnSc1d1	Moldavská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Pověstmi	pověst	k1gFnPc7	pověst
je	být	k5eAaImIp3nS	být
opředen	opříst	k5eAaPmNgInS	opříst
klášter	klášter	k1gInSc1	klášter
Svaté	svatý	k2eAgFnSc2d1	svatá
Trojice	trojice	k1gFnSc2	trojice
v	v	k7c4	v
Saharna	Saharno	k1gNnPc4	Saharno
<g/>
.	.	kIx.	.
</s>
<s>
Kišiněv	Kišinět	k5eAaPmDgInS	Kišinět
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
památkou	památka	k1gFnSc7	památka
Alexandra	Alexandr	k1gMnSc2	Alexandr
Sergejeviče	Sergejevič	k1gMnSc2	Sergejevič
Puškina	Puškin	k1gMnSc2	Puškin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sem	sem	k6eAd1	sem
rád	rád	k6eAd1	rád
jezdíval	jezdívat	k5eAaImAgMnS	jezdívat
a	a	k8xC	a
trávil	trávit	k5eAaImAgMnS	trávit
zde	zde	k6eAd1	zde
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodným	pozoruhodný	k2eAgInSc7d1	pozoruhodný
dokladem	doklad	k1gInSc7	doklad
střídání	střídání	k1gNnSc2	střídání
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
mísení	mísení	k1gNnSc1	mísení
vlivů	vliv	k1gInPc2	vliv
na	na	k7c6	na
území	území	k1gNnSc6	území
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Orhei	Orhe	k1gFnSc2	Orhe
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
nejvíce	nejvíce	k6eAd1	nejvíce
historických	historický	k2eAgFnPc2d1	historická
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
od	od	k7c2	od
muslimských	muslimský	k2eAgFnPc2d1	muslimská
mešit	mešita	k1gFnPc2	mešita
až	až	k9	až
po	po	k7c4	po
pravoslavné	pravoslavný	k2eAgInPc4d1	pravoslavný
kláštery	klášter	k1gInPc4	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moldavské	moldavský	k2eAgFnSc2d1	Moldavská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Grigore	Grigor	k1gMnSc5	Grigor
Ureche	Urechus	k1gMnSc5	Urechus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kronice	kronika	k1gFnSc6	kronika
Letopiseţul	Letopiseţula	k1gFnPc2	Letopiseţula
Ţării	Ţărie	k1gFnSc4	Ţărie
Moldovei	Moldovei	k1gNnSc2	Moldovei
poprvé	poprvé	k6eAd1	poprvé
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
existenci	existence	k1gFnSc4	existence
moldavského	moldavský	k2eAgInSc2d1	moldavský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
románského	románský	k2eAgInSc2d1	románský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Rumunskou	rumunský	k2eAgFnSc4d1	rumunská
filologii	filologie	k1gFnSc4	filologie
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
povýšil	povýšit	k5eAaPmAgMnS	povýšit
Bogdan	Bogdan	k1gMnSc1	Bogdan
Petriceicu	Petriceic	k2eAgFnSc4d1	Petriceic
Hasdeu	Hasdea	k1gFnSc4	Hasdea
(	(	kIx(	(
<g/>
rodák	rodák	k1gMnSc1	rodák
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Besarábie	Besarábie	k1gFnSc2	Besarábie
<g/>
,	,	kIx,	,
o	o	k7c4	o
niž	jenž	k3xRgFnSc4	jenž
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
přišlo	přijít	k5eAaPmAgNnS	přijít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
Berg	Berg	k1gMnSc1	Berg
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
geografem	geograf	k1gMnSc7	geograf
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
Ruské	ruský	k2eAgFnSc2d1	ruská
geografické	geografický	k2eAgFnSc2d1	geografická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
také	také	k9	také
slavný	slavný	k2eAgMnSc1d1	slavný
polský	polský	k2eAgMnSc1d1	polský
statistik	statistik	k1gMnSc1	statistik
Jerzy	Jerza	k1gFnSc2	Jerza
Neyman	Neyman	k1gMnSc1	Neyman
či	či	k8xC	či
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
teorie	teorie	k1gFnSc2	teorie
organické	organický	k2eAgFnSc2d1	organická
katalýzy	katalýza	k1gFnSc2	katalýza
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Zelinskij	Zelinskij	k1gMnSc1	Zelinskij
<g/>
.	.	kIx.	.
</s>
<s>
Politologem	politolog	k1gMnSc7	politolog
a	a	k8xC	a
odborníkem	odborník	k1gMnSc7	odborník
na	na	k7c4	na
geopolitiku	geopolitika	k1gFnSc4	geopolitika
je	být	k5eAaImIp3nS	být
Oleg	Oleg	k1gMnSc1	Oleg
Serebrian	Serebrian	k1gMnSc1	Serebrian
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
institucemi	instituce	k1gFnPc7	instituce
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
(	(	kIx(	(
<g/>
Biblioteca	Bibliotecus	k1gMnSc4	Bibliotecus
Naţională	Naţională	k1gMnSc4	Naţională
a	a	k8xC	a
Republicii	Republicie	k1gFnSc4	Republicie
Moldova	Moldův	k2eAgMnSc2d1	Moldův
<g/>
)	)	kIx)	)
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Kišiněvě	Kišiněva	k1gFnSc6	Kišiněva
(	(	kIx(	(
<g/>
Universitatea	Universitatea	k1gFnSc1	Universitatea
de	de	k?	de
Stat	Stat	k2eAgInSc1d1	Stat
din	din	k1gInSc1	din
Moldova	Moldův	k2eAgMnSc2d1	Moldův
<g/>
)	)	kIx)	)
a	a	k8xC	a
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
Academia	academia	k1gFnSc1	academia
de	de	k?	de
Ştiinţe	Ştiinţe	k1gFnPc2	Ştiinţe
a	a	k8xC	a
Moldovei	Moldovei	k1gNnPc2	Moldovei
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
posledně	posledně	k6eAd1	posledně
jmenované	jmenovaná	k1gFnPc1	jmenovaná
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
roku	rok	k1gInSc3	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
se	se	k3xPyFc4	se
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
představilo	představit	k5eAaPmAgNnS	představit
prvně	prvně	k?	prvně
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
hned	hned	k6eAd1	hned
si	se	k3xPyFc3	se
přivezlo	přivézt	k5eAaPmAgNnS	přivézt
dvě	dva	k4xCgFnPc4	dva
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
kanoistů	kanoista	k1gMnPc2	kanoista
Nicolae	Nicolae	k1gNnSc2	Nicolae
Juravschi	Juravsch	k1gFnSc2	Juravsch
a	a	k8xC	a
Viktor	Viktor	k1gMnSc1	Viktor
Reneysky	Reneyska	k1gFnSc2	Reneyska
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
stříbro	stříbro	k1gNnSc4	stříbro
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
dvě	dva	k4xCgFnPc4	dva
zlata	zlato	k1gNnSc2	zlato
ze	z	k7c2	z
sovětské	sovětský	k2eAgFnSc2d1	sovětská
éry	éra	k1gFnSc2	éra
<g/>
)	)	kIx)	)
a	a	k8xC	a
zápasník	zápasník	k1gMnSc1	zápasník
Sergej	Sergej	k1gMnSc1	Sergej
Murejko	Murejka	k1gFnSc5	Murejka
přivezl	přivézt	k5eAaPmAgMnS	přivézt
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
Moldavané	Moldavan	k1gMnPc1	Moldavan
tuto	tento	k3xDgFnSc4	tento
bilanci	bilance	k1gFnSc4	bilance
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
získal	získat	k5eAaPmAgMnS	získat
střelec	střelec	k1gMnSc1	střelec
Oleg	Oleg	k1gMnSc1	Oleg
Moldovan	Moldovan	k1gMnSc1	Moldovan
a	a	k8xC	a
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
boxer	boxer	k1gInSc4	boxer
Vitalie	Vitalie	k1gFnPc1	Vitalie
Gruș	Gruș	k1gFnSc2	Gruș
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
medaili	medaile	k1gFnSc4	medaile
přivezl	přivézt	k5eAaPmAgMnS	přivézt
opět	opět	k6eAd1	opět
boxer	boxer	k1gMnSc1	boxer
<g/>
,	,	kIx,	,
Veaceslav	Veaceslav	k1gMnSc1	Veaceslav
Gojan	Gojan	k1gMnSc1	Gojan
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
znovu	znovu	k6eAd1	znovu
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
medaile	medaile	k1gFnSc1	medaile
z	z	k7c2	z
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
v	v	k7c6	v
moldavské	moldavský	k2eAgFnSc6d1	Moldavská
sbírce	sbírka	k1gFnSc6	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Gojan	Gojan	k1gInSc1	Gojan
je	být	k5eAaImIp3nS	být
i	i	k9	i
mistrem	mistr	k1gMnSc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
ovšem	ovšem	k9	ovšem
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
vzpěrač	vzpěrač	k1gMnSc1	vzpěrač
Tudor	tudor	k1gInSc4	tudor
Casapu	Casap	k1gInSc2	Casap
<g/>
.	.	kIx.	.
<g/>
Lukostřelkyně	lukostřelkyně	k1gFnSc1	lukostřelkyně
Natalia	Natalia	k1gFnSc1	Natalia
Valeeva	Valeeva	k1gFnSc1	Valeeva
je	být	k5eAaImIp3nS	být
mistryní	mistryně	k1gFnSc7	mistryně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
získala	získat	k5eAaPmAgFnS	získat
pro	pro	k7c4	pro
samostatné	samostatný	k2eAgNnSc4d1	samostatné
Moldavsko	Moldavsko	k1gNnSc4	Moldavsko
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
tituly	titul	k1gInPc4	titul
už	už	k6eAd1	už
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
i	i	k9	i
dva	dva	k4xCgInPc4	dva
bronzy	bronz	k1gInPc4	bronz
z	z	k7c2	z
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Moldavci	Moldavec	k1gMnPc1	Moldavec
startovali	startovat	k5eAaBmAgMnP	startovat
ještě	ještě	k6eAd1	ještě
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
Sjednocený	sjednocený	k2eAgInSc4d1	sjednocený
tým	tým	k1gInSc4	tým
zemí	zem	k1gFnPc2	zem
právě	právě	k6eAd1	právě
rozpadlého	rozpadlý	k2eAgInSc2d1	rozpadlý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zápasník	zápasník	k1gMnSc1	zápasník
Lukman	Lukman	k1gMnSc1	Lukman
Džabrajilov	Džabrajilov	k1gInSc4	Džabrajilov
má	mít	k5eAaImIp3nS	mít
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
na	na	k7c6	na
istanbulském	istanbulský	k2eAgInSc6d1	istanbulský
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
lidovou	lidový	k2eAgFnSc7d1	lidová
verzí	verze	k1gFnSc7	verze
zápasu	zápas	k1gInSc2	zápas
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
je	být	k5eAaImIp3nS	být
trânta	trânta	k1gFnSc1	trânta
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
další	další	k2eAgInSc4d1	další
bojový	bojový	k2eAgInSc4d1	bojový
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
judo	judo	k1gNnSc4	judo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
slušnou	slušný	k2eAgFnSc4d1	slušná
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Florescu	Florescus	k1gInSc2	Florescus
kupříkladu	kupříkladu	k6eAd1	kupříkladu
bral	brát	k5eAaImAgMnS	brát
stříbro	stříbro	k1gNnSc4	stříbro
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Moldavané	Moldavan	k1gMnPc1	Moldavan
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzpírání	vzpírání	k1gNnSc4	vzpírání
<g/>
,	,	kIx,	,
Oleg	Oleg	k1gMnSc1	Oleg
Sîrghi	Sîrgh	k1gFnSc2	Sîrgh
je	být	k5eAaImIp3nS	být
trojnásobným	trojnásobný	k2eAgMnSc7d1	trojnásobný
mistrem	mistr	k1gMnSc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
evropských	evropský	k2eAgMnPc2d1	evropský
šampionů	šampion	k1gMnPc2	šampion
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
Igor	Igor	k1gMnSc1	Igor
Bour	boura	k1gFnPc2	boura
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anatolie	Anatolie	k1gFnSc1	Anatolie
Cîrîcu	Cîrîcus	k1gInSc2	Cîrîcus
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
a	a	k8xC	a
Cristina	Cristin	k2eAgInSc2d1	Cristin
Iovu	Iovus	k1gInSc2	Iovus
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
druhý	druhý	k4xOgInSc1	druhý
titul	titul	k1gInSc1	titul
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
již	již	k6eAd1	již
za	za	k7c4	za
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
evropské	evropský	k2eAgNnSc4d1	Evropské
zlato	zlato	k1gNnSc4	zlato
dvakrát	dvakrát	k6eAd1	dvakrát
Ghenadie	Ghenadie	k1gFnPc4	Ghenadie
Tulbea	Tulbe	k1gInSc2	Tulbe
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taekwondista	Taekwondista	k1gMnSc1	Taekwondista
Stepan	Stepan	k1gMnSc1	Stepan
Dimitrov	Dimitrov	k1gInSc4	Dimitrov
je	být	k5eAaImIp3nS	být
mistrem	mistr	k1gMnSc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moldavský	moldavský	k2eAgInSc1d1	moldavský
původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
cyklista	cyklista	k1gMnSc1	cyklista
Andrei	Andrea	k1gFnSc3	Andrea
Tchmil	Tchmil	k1gFnPc7	Tchmil
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Besarábie	Besarábie	k1gFnSc1	Besarábie
</s>
</p>
<p>
<s>
Gagauzsko	Gagauzsko	k6eAd1	Gagauzsko
</s>
</p>
<p>
<s>
Moldávie	Moldávie	k1gFnSc1	Moldávie
</s>
</p>
<p>
<s>
Podněstří	Podněstřit	k5eAaPmIp3nS	Podněstřit
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Moldavsko	Moldavsko	k1gNnSc4	Moldavsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Moldavské	moldavský	k2eAgFnSc2d1	Moldavská
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rumunsky	rumunsky	k6eAd1	rumunsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
moldavské	moldavský	k2eAgFnSc2d1	Moldavská
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Moldavsku	Moldavsko	k1gNnSc6	Moldavsko
</s>
</p>
<p>
<s>
Moldova	Moldův	k2eAgFnSc1d1	Moldova
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moldova	Moldův	k2eAgFnSc1d1	Moldova
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Moldova	Moldův	k2eAgFnSc1d1	Moldova
Country	country	k2eAgFnSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Moldova	Moldův	k2eAgFnSc1d1	Moldova
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-01	[number]	k4	2011-07-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Moldova	Moldův	k2eAgFnSc1d1	Moldova
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Kišiněvě	Kišiněva	k1gFnSc6	Kišiněva
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-06-27	[number]	k4	2011-06-27
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BUCKMASTER	BUCKMASTER	kA	BUCKMASTER
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Moldova	Moldův	k2eAgFnSc1d1	Moldova
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
