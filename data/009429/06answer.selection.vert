<s>
Hořčice	hořčice	k1gFnSc1	hořčice
je	být	k5eAaImIp3nS	být
dochucovací	dochucovací	k2eAgInSc4d1	dochucovací
přípravek	přípravek	k1gInSc4	přípravek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
husté	hustý	k2eAgFnSc3d1	hustá
žluté	žlutý	k2eAgFnPc4d1	žlutá
nebo	nebo	k8xC	nebo
žlutohnědé	žlutohnědý	k2eAgFnPc4d1	žlutohnědá
pasty	pasta	k1gFnPc4	pasta
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
z	z	k7c2	z
mletých	mletý	k2eAgNnPc2d1	mleté
hořčičných	hořčičný	k2eAgNnPc2d1	hořčičné
semínek	semínko	k1gNnPc2	semínko
smíchaných	smíchaný	k2eAgInPc2d1	smíchaný
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
octem	ocet	k1gInSc7	ocet
<g/>
,	,	kIx,	,
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
cukrem	cukr	k1gInSc7	cukr
<g/>
,	,	kIx,	,
mletým	mletý	k2eAgNnSc7d1	mleté
kořením	koření	k1gNnSc7	koření
<g/>
,	,	kIx,	,
olejem	olej	k1gInSc7	olej
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc7d1	jiná
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
.	.	kIx.	.
</s>
