<p>
<s>
Hořčice	hořčice	k1gFnSc1	hořčice
je	být	k5eAaImIp3nS	být
dochucovací	dochucovací	k2eAgInSc4d1	dochucovací
přípravek	přípravek	k1gInSc4	přípravek
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
husté	hustý	k2eAgFnSc3d1	hustá
žluté	žlutý	k2eAgFnPc4d1	žlutá
nebo	nebo	k8xC	nebo
žlutohnědé	žlutohnědý	k2eAgFnPc4d1	žlutohnědá
pasty	pasta	k1gFnPc4	pasta
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
z	z	k7c2	z
mletých	mletý	k2eAgNnPc2d1	mleté
hořčičných	hořčičný	k2eAgNnPc2d1	hořčičné
semínek	semínko	k1gNnPc2	semínko
smíchaných	smíchaný	k2eAgInPc2d1	smíchaný
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
octem	ocet	k1gInSc7	ocet
<g/>
,	,	kIx,	,
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
cukrem	cukr	k1gInSc7	cukr
<g/>
,	,	kIx,	,
mletým	mletý	k2eAgNnSc7d1	mleté
kořením	koření	k1gNnSc7	koření
<g/>
,	,	kIx,	,
olejem	olej	k1gInSc7	olej
nebo	nebo	k8xC	nebo
jinou	jiný	k2eAgFnSc7d1	jiná
kapalinou	kapalina	k1gFnSc7	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přibarvená	přibarvený	k2eAgFnSc1d1	přibarvená
kurkumou	kurkuma	k1gFnSc7	kurkuma
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ostrou	ostrý	k2eAgFnSc4d1	ostrá
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
až	až	k9	až
silně	silně	k6eAd1	silně
pálivou	pálivý	k2eAgFnSc4d1	pálivá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
konzervována	konzervovat	k5eAaBmNgFnS	konzervovat
kyselinou	kyselina	k1gFnSc7	kyselina
benzoovou	benzoový	k2eAgFnSc7d1	benzoová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
hořčici	hořčice	k1gFnSc4	hořčice
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
směs	směs	k1gFnSc1	směs
rozemletých	rozemletý	k2eAgNnPc2d1	rozemleté
semínek	semínko	k1gNnPc2	semínko
s	s	k7c7	s
vinným	vinný	k2eAgInSc7d1	vinný
moštem	mošt	k1gInSc7	mošt
<g/>
)	)	kIx)	)
používali	používat	k5eAaImAgMnP	používat
starověcí	starověký	k2eAgMnPc1d1	starověký
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
hořčice	hořčice	k1gFnSc2	hořčice
==	==	k?	==
</s>
</p>
<p>
<s>
Hořčice	hořčice	k1gFnSc1	hořčice
kremžská	kremžský	k2eAgFnSc1d1	kremžská
–	–	k?	–
původem	původ	k1gInSc7	původ
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
rakouského	rakouský	k2eAgNnSc2d1	rakouské
města	město	k1gNnSc2	město
Kremže	Kremže	k1gFnSc2	Kremže
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
semen	semeno	k1gNnPc2	semeno
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
(	(	kIx(	(
<g/>
hnědé	hnědý	k2eAgFnSc2d1	hnědá
<g/>
)	)	kIx)	)
hořčice	hořčice	k1gFnSc2	hořčice
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
viditelně	viditelně	k6eAd1	viditelně
hrubě	hrubě	k6eAd1	hrubě
mleté	mletý	k2eAgFnPc1d1	mletá
černé	černý	k2eAgFnPc1d1	černá
slupky	slupka	k1gFnPc1	slupka
a	a	k8xC	a
vyšší	vysoký	k2eAgNnSc1d2	vyšší
procento	procento	k1gNnSc1	procento
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Přibarvená	přibarvený	k2eAgFnSc1d1	přibarvená
kremžská	kremžský	k2eAgFnSc1d1	kremžská
hořčice	hořčice	k1gFnSc1	hořčice
má	mít	k5eAaImIp3nS	mít
žlutohnědou	žlutohnědý	k2eAgFnSc4d1	žlutohnědá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
nepřibarvená	přibarvený	k2eNgFnSc1d1	nepřibarvená
má	mít	k5eAaImIp3nS	mít
šedohnědou	šedohnědý	k2eAgFnSc7d1	šedohnědá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dijonská	Dijonský	k2eAgFnSc1d1	Dijonská
hořčice	hořčice	k1gFnSc1	hořčice
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hořčice	hořčice	k1gFnSc2	hořčice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hořčice	hořčice	k1gFnSc2	hořčice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
