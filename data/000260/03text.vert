<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šlégr	Šlégr	k1gMnSc1	Šlégr
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1971	[number]	k4	1971
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
prestižního	prestižní	k2eAgInSc2d1	prestižní
spolku	spolek	k1gInSc2	spolek
Triple	tripl	k1gInSc5	tripl
Gold	Gold	k1gMnSc1	Gold
Club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
vítězství	vítězství	k1gNnSc1	vítězství
na	na	k7c4	na
MS	MS	kA	MS
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
<g/>
,	,	kIx,	,
OH	OH	kA	OH
a	a	k8xC	a
zisk	zisk	k1gInSc4	zisk
Stanleyova	Stanleyův	k2eAgInSc2d1	Stanleyův
poháru	pohár	k1gInSc2	pohár
<g/>
)	)	kIx)	)
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
člen	člen	k1gInSc1	člen
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
do	do	k7c2	do
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
místopředseda	místopředseda	k1gMnSc1	místopředseda
strany	strana	k1gFnSc2	strana
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
-	-	kIx~	-
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
PSP	PSP	kA	PSP
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
mandátu	mandát	k1gInSc6	mandát
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
působí	působit	k5eAaImIp3nP	působit
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
hokejového	hokejový	k2eAgInSc2d1	hokejový
klubu	klub	k1gInSc2	klub
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
funkci	funkce	k1gFnSc4	funkce
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
realizačního	realizační	k2eAgInSc2d1	realizační
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
Jiří	Jiří	k1gMnSc1	Jiří
Bubla	Bubla	k1gMnSc1	Bubla
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
zámoří	zámoří	k1gNnSc2	zámoří
hrál	hrát	k5eAaImAgInS	hrát
za	za	k7c4	za
Litvínov	Litvínov	k1gInSc4	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Vancouver	Vancouver	k1gInSc1	Vancouver
Canucks	Canucks	k1gInSc1	Canucks
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
však	však	k9	však
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
dresu	dres	k1gInSc6	dres
nezahrál	zahrát	k5eNaPmAgMnS	zahrát
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
v	v	k7c6	v
play	play	k0	play
off	off	k?	off
ani	ani	k8xC	ani
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gMnPc1	jeho
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
finálových	finálový	k2eAgInPc2d1	finálový
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Stanleyův	Stanleyův	k2eAgInSc4d1	Stanleyův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
pomohl	pomoct	k5eAaPmAgMnS	pomoct
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
klubu	klub	k1gInSc2	klub
Södertälje	Södertälj	k1gFnSc2	Södertälj
SK	Sk	kA	Sk
k	k	k7c3	k
postupu	postup	k1gInSc3	postup
do	do	k7c2	do
Elitserien	Elitserina	k1gFnPc2	Elitserina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
konec	konec	k1gInSc4	konec
své	svůj	k3xOyFgFnSc2	svůj
hokejové	hokejový	k2eAgFnSc2d1	hokejová
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
hokeji	hokej	k1gInSc3	hokej
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ještě	ještě	k9	ještě
během	během	k7c2	během
play-out	playut	k1gMnSc1	play-out
sezóny	sezóna	k1gFnSc2	sezóna
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pomohl	pomoct	k5eAaPmAgMnS	pomoct
svému	svůj	k3xOyFgInSc3	svůj
domovskému	domovský	k2eAgInSc3d1	domovský
týmu	tým	k1gInSc3	tým
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc1	Litvínov
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zachránění	zachránění	k1gNnSc4	zachránění
svého	svůj	k3xOyFgInSc2	svůj
týmu	tým	k1gInSc2	tým
od	od	k7c2	od
baráže	baráž	k1gFnSc2	baráž
o	o	k7c4	o
extraligu	extraliga	k1gFnSc4	extraliga
ukončil	ukončit	k5eAaPmAgMnS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
vedení	vedení	k1gNnSc1	vedení
HC	HC	kA	HC
Verva	verva	k1gFnSc1	verva
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
že	že	k8xS	že
opět	opět	k6eAd1	opět
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
trénuje	trénovat	k5eAaImIp3nS	trénovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
litvínovský	litvínovský	k2eAgInSc4d1	litvínovský
klub	klub	k1gInSc4	klub
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
získal	získat	k5eAaPmAgInS	získat
extraligový	extraligový	k2eAgInSc1d1	extraligový
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mu	on	k3xPp3gMnSc3	on
nedovolily	dovolit	k5eNaPmAgFnP	dovolit
nastoupit	nastoupit	k5eAaPmF	nastoupit
v	v	k7c4	v
play-off	playff	k1gInSc4	play-off
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgInS	oznámit
definitivní	definitivní	k2eAgInSc1d1	definitivní
konec	konec	k1gInSc1	konec
aktivní	aktivní	k2eAgFnSc2d1	aktivní
hokejové	hokejový	k2eAgFnSc2d1	hokejová
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
tělesné	tělesný	k2eAgFnSc3d1	tělesná
odolnosti	odolnost	k1gFnSc3	odolnost
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
Guma	guma	k1gFnSc1	guma
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
reprezentaci	reprezentace	k1gFnSc4	reprezentace
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
poprvé	poprvé	k6eAd1	poprvé
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
při	při	k7c6	při
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
proti	proti	k7c3	proti
Švédsku	Švédsko	k1gNnSc3	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Statistiky	statistika	k1gFnPc1	statistika
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
:	:	kIx,	:
Juniorská	juniorský	k2eAgFnSc1d1	juniorská
reprezentace	reprezentace	k1gFnSc1	reprezentace
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
SNK-ED	SNK-ED	k1gFnSc4	SNK-ED
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Litvínova	Litvínov	k1gInSc2	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
městským	městský	k2eAgMnSc7d1	městský
zastupitelem	zastupitel	k1gMnSc7	zastupitel
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
radním	radní	k1gMnPc3	radní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
začal	začít	k5eAaPmAgMnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
tváří	tvář	k1gFnSc7	tvář
v	v	k7c6	v
předvolební	předvolební	k2eAgFnSc6d1	předvolební
kampani	kampaň	k1gFnSc6	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Natočil	natočit	k5eAaBmAgInS	natočit
pro	pro	k7c4	pro
ČSSD	ČSSD	kA	ČSSD
volební	volební	k2eAgInSc4d1	volební
klip	klip	k1gInSc4	klip
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgMnSc6	který
v	v	k7c6	v
oranžovém	oranžový	k2eAgInSc6d1	oranžový
dresu	dres	k1gInSc6	dres
najíždí	najíždět	k5eAaImIp3nS	najíždět
na	na	k7c4	na
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
brankář	brankář	k1gMnSc1	brankář
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
dresu	dres	k1gInSc6	dres
se	s	k7c7	s
jmenovkou	jmenovka	k1gFnSc7	jmenovka
Topolánek	Topolánka	k1gFnPc2	Topolánka
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
mu	on	k3xPp3gMnSc3	on
gól	gól	k1gInSc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zrušených	zrušený	k2eAgFnPc6d1	zrušená
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
měl	mít	k5eAaImAgMnS	mít
kandidovat	kandidovat	k5eAaImF	kandidovat
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
hned	hned	k6eAd1	hned
za	za	k7c7	za
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
už	už	k9	už
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
ČSSD	ČSSD	kA	ČSSD
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
její	její	k3xOp3gFnSc2	její
kandidátky	kandidátka	k1gFnSc2	kandidátka
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
mandát	mandát	k1gInSc4	mandát
poslance	poslanec	k1gMnSc2	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
opustil	opustit	k5eAaPmAgInS	opustit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Paroubkem	Paroubek	k1gInSc7	Paroubek
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
nového	nový	k2eAgNnSc2d1	nové
politického	politický	k2eAgNnSc2d1	politické
uskupení	uskupení	k1gNnSc2	uskupení
-	-	kIx~	-
strany	strana	k1gFnPc4	strana
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
-	-	kIx~	-
levice	levice	k1gFnSc1	levice
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
-	-	kIx~	-
Národní	národní	k2eAgMnPc1d1	národní
socialisté	socialist	k1gMnPc1	socialist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
ustavujícím	ustavující	k2eAgInSc6d1	ustavující
sjezdu	sjezd	k1gInSc6	sjezd
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jejím	její	k3xOp3gMnSc7	její
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
LEV	lev	k1gInSc4	lev
21	[number]	k4	21
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Ústeckého	ústecký	k2eAgInSc2d1	ústecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
za	za	k7c4	za
LEV	lev	k1gInSc4	lev
21	[number]	k4	21
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
29	[number]	k4	29
-	-	kIx~	-
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
5,65	[number]	k4	5,65
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
však	však	k9	však
skončil	skončit	k5eAaPmAgInS	skončit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
nepostoupil	postoupit	k5eNaPmAgMnS	postoupit
ani	ani	k9	ani
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
na	na	k7c6	na
mimořádném	mimořádný	k2eAgNnSc6d1	mimořádné
jednání	jednání	k1gNnSc6	jednání
Sněmovny	sněmovna	k1gFnSc2	sněmovna
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
policejní	policejní	k2eAgFnSc6d1	policejní
razii	razie	k1gFnSc6	razie
na	na	k7c6	na
Úřadu	úřad	k1gInSc6	úřad
vlády	vláda	k1gFnSc2	vláda
ČR	ČR	kA	ČR
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
mandátu	mandát	k1gInSc2	mandát
a	a	k8xC	a
ukončí	ukončit	k5eAaPmIp3nS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
čistého	čistý	k2eAgNnSc2d1	čisté
prostředí	prostředí	k1gNnSc2	prostředí
mezi	mezi	k7c4	mezi
sportovce	sportovec	k1gMnSc4	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
LEV	lev	k1gInSc4	lev
21	[number]	k4	21
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
kandidátky	kandidátka	k1gFnSc2	kandidátka
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
(	(	kIx(	(
<g/>
LEV	lev	k1gInSc1	lev
21	[number]	k4	21
se	se	k3xPyFc4	se
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kandidátky	kandidátka	k1gFnSc2	kandidátka
získal	získat	k5eAaPmAgInS	získat
nejvíc	hodně	k6eAd3	hodně
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
12,50	[number]	k4	12,50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
na	na	k7c4	na
post	post	k1gInSc4	post
místopředsedy	místopředseda	k1gMnSc2	místopředseda
LEV	Lev	k1gMnSc1	Lev
21	[number]	k4	21
a	a	k8xC	a
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
SNK-ED	SNK-ED	k1gFnSc4	SNK-ED
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
Litvínova	Litvínov	k1gInSc2	Litvínov
a	a	k8xC	a
uspěl	uspět	k5eAaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
když	když	k8xS	když
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
jako	jako	k8xS	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
4	[number]	k4	4
-	-	kIx~	-
Most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
16,42	[number]	k4	16,42
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
postoupil	postoupit	k5eAaPmAgMnS	postoupit
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
však	však	k9	však
prohrál	prohrát	k5eAaPmAgMnS	prohrát
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
29,18	[number]	k4	29,18
%	%	kIx~	%
:	:	kIx,	:
70,81	[number]	k4	70,81
%	%	kIx~	%
s	s	k7c7	s
kandidátkou	kandidátka	k1gFnSc7	kandidátka
hnutí	hnutí	k1gNnSc3	hnutí
Severočeši	Severočech	k1gMnPc1	Severočech
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Alenou	Alena	k1gFnSc7	Alena
Dernerovou	Dernerová	k1gFnSc7	Dernerová
<g/>
.	.	kIx.	.
</s>
<s>
Senátorem	senátor	k1gMnSc7	senátor
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nestal	stát	k5eNaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
však	však	k9	však
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
volby	volba	k1gFnSc2	volba
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obvodu	obvod	k1gInSc6	obvod
za	za	k7c4	za
neplatné	platný	k2eNgFnPc4d1	neplatná
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jejich	jejich	k3xOp3gNnSc4	jejich
opakování	opakování	k1gNnSc4	opakování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
tak	tak	k6eAd1	tak
za	za	k7c4	za
ČSSD	ČSSD	kA	ČSSD
kandiduje	kandidovat	k5eAaImIp3nS	kandidovat
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g />
.	.	kIx.	.
</s>
<s>
Vancouver	Vancouver	k1gInSc1	Vancouver
Canucks	Canucks	k1gInSc1	Canucks
<g/>
,	,	kIx,	,
Hamilton	Hamilton	k1gInSc1	Hamilton
Canucks	Canucks	k1gInSc1	Canucks
(	(	kIx(	(
<g/>
AHL	AHL	kA	AHL
<g/>
)	)	kIx)	)
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1994	[number]	k4	1994
Vancouver	Vancouver	k1gInSc1	Vancouver
Canucks	Canucks	k1gInSc1	Canucks
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
Vancouver	Vancouver	k1gInSc1	Vancouver
Canucks	Canucks	k1gInSc1	Canucks
<g/>
,	,	kIx,	,
Edmonton	Edmonton	k1gInSc1	Edmonton
Oilers	Oilers	k1gInSc1	Oilers
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
Edmonton	Edmonton	k1gInSc1	Edmonton
Oilers	Oilers	k1gInSc4	Oilers
<g/>
,	,	kIx,	,
Cape-Breton	Cape-Breton	k1gInSc1	Cape-Breton
Oilers	Oilers	k1gInSc1	Oilers
(	(	kIx(	(
<g/>
AHL	AHL	kA	AHL
<g/>
)	)	kIx)	)
1996	[number]	k4	1996
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
Södertälje	Södertälje	k1gFnSc1	Södertälje
SK	Sk	kA	Sk
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc4	Penguins
<g/>
,	,	kIx,	,
Atlanta	Atlanta	k1gFnSc1	Atlanta
Thrashers	Thrashers	k1gInSc1	Thrashers
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2002	[number]	k4	2002
Atlanta	Atlanta	k1gFnSc1	Atlanta
Thrashers	Thrashersa	k1gFnPc2	Thrashersa
<g/>
,	,	kIx,	,
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
Vítěz	vítěz	k1gMnSc1	vítěz
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
,	,	kIx,	,
Avangard	Avangard	k1gInSc1	Avangard
Omsk	Omsk	k1gInSc1	Omsk
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
Vancouver	Vancouvero	k1gNnPc2	Vancouvero
Canucks	Canucksa	k1gFnPc2	Canucksa
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
HC	HC	kA	HC
Litvínov	Litvínov	k1gInSc4	Litvínov
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
HC	HC	kA	HC
BENZINA	BENZINA	kA	BENZINA
Litvínov	Litvínov	k1gInSc4	Litvínov
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
nehrál	hrát	k5eNaImAgMnS	hrát
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
HC	HC	kA	HC
Verva	verva	k1gFnSc1	verva
Litvínov	Litvínov	k1gInSc1	Litvínov
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2013	[number]	k4	2013
nehrál	hrát	k5eNaImAgMnS	hrát
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
HC	HC	kA	HC
Verva	verva	k1gFnSc1	verva
Litvínov	Litvínov	k1gInSc1	Litvínov
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
HC	HC	kA	HC
Verva	verva	k1gFnSc1	verva
Litvínov	Litvínov	k1gInSc1	Litvínov
Mistr	mistr	k1gMnSc1	mistr
extraligy	extraliga	k1gFnSc2	extraliga
konec	konec	k1gInSc1	konec
hokejové	hokejový	k2eAgFnSc2d1	hokejová
kariéry	kariéra	k1gFnSc2	kariéra
Ano	ano	k9	ano
V	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
:	:	kIx,	:
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
start	start	k1gInSc4	start
v	v	k7c6	v
NHL	NHL	kA	NHL
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
6.10	[number]	k4	6.10
<g/>
.1992	.1992	k4	.1992
proti	proti	k7c3	proti
Edmontonu	Edmonton	k1gInSc3	Edmonton
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
bod	bod	k1gInSc1	bod
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
také	také	k6eAd1	také
proti	proti	k7c3	proti
Edmontonu	Edmonton	k1gInSc3	Edmonton
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
gól	gól	k1gInSc1	gól
dal	dát	k5eAaPmAgInS	dát
Patriku	Patrik	k1gMnSc3	Patrik
Royovi	Roy	k1gMnSc3	Roy
z	z	k7c2	z
Montrealu	Montreal	k1gInSc2	Montreal
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Edmontonu	Edmonton	k1gInSc2	Edmonton
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1995	[number]	k4	1995
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
Romana	Roman	k1gMnSc4	Roman
Oksiutu	Oksiut	k1gInSc2	Oksiut
<g/>
.	.	kIx.	.
</s>
<s>
Pittsburgh	Pittsburgh	k1gInSc4	Pittsburgh
získal	získat	k5eAaPmAgMnS	získat
Šlégra	Šlégra	k1gMnSc1	Šlégra
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
možnost	možnost	k1gFnSc4	možnost
výběru	výběr	k1gInSc2	výběr
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
draftu	draft	k1gInSc2	draft
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Atlanty	Atlanta	k1gFnSc2	Atlanta
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2001	[number]	k4	2001
za	za	k7c4	za
možnost	možnost	k1gFnSc4	možnost
volby	volba	k1gFnSc2	volba
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
vstupního	vstupní	k2eAgInSc2d1	vstupní
draftu	draft	k1gInSc2	draft
<g/>
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
získal	získat	k5eAaPmAgInS	získat
Jiřího	Jiří	k1gMnSc4	Jiří
Šlégra	Šlégr	k1gMnSc4	Šlégr
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2002	[number]	k4	2002
za	za	k7c4	za
Jurije	Jurije	k1gMnSc4	Jurije
Bucajeva	Bucajev	k1gMnSc4	Bucajev
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
výběru	výběr	k1gInSc2	výběr
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolem	kolem	k7c2	kolem
draftu	draft	k1gInSc2	draft
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jiří	Jiří	k1gMnSc1	Jiří
Šlégr	Šlégra	k1gFnPc2	Šlégra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Jiří	Jiří	k1gMnSc1	Jiří
Šlégr	Šlégr	k1gMnSc1	Šlégr
-	-	kIx~	-
statistiky	statistika	k1gFnPc1	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Statistika	statistik	k1gMnSc2	statistik
Jiřího	Jiří	k1gMnSc2	Jiří
Šlégra	Šlégr	k1gMnSc2	Šlégr
v	v	k7c6	v
NHL	NHL	kA	NHL
na	na	k7c6	na
nhl	nhl	k?	nhl
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Jiří	Jiří	k1gMnSc1	Jiří
Šlégr	Šlégr	k1gMnSc1	Šlégr
na	na	k7c6	na
nhl	nhl	k?	nhl
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Jiří	Jiří	k1gMnSc1	Jiří
Šlégr	Šlégr	k1gMnSc1	Šlégr
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
