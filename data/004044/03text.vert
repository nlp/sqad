<s>
Jahoda	jahoda	k1gFnSc1	jahoda
je	být	k5eAaImIp3nS	být
souplodí	souplodí	k1gNnSc4	souplodí
nažek	nažka	k1gFnPc2	nažka
na	na	k7c6	na
šťavnatém	šťavnatý	k2eAgNnSc6d1	šťavnaté
zdužnatělém	zdužnatělý	k2eAgNnSc6d1	zdužnatělé
květním	květní	k2eAgNnSc6d1	květní
lůžku	lůžko	k1gNnSc6	lůžko
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgFnSc6d1	rostoucí
na	na	k7c6	na
jahodníku	jahodník	k1gInSc6	jahodník
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
souplodí	souplodí	k1gNnSc1	souplodí
nažek	nažka	k1gFnPc2	nažka
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
nepravý	pravý	k2eNgInSc4d1	nepravý
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Jahoda	jahoda	k1gFnSc1	jahoda
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
drobné	drobný	k2eAgNnSc4d1	drobné
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
bohatým	bohatý	k2eAgInSc7d1	bohatý
zdrojem	zdroj	k1gInSc7	zdroj
vitamínu	vitamín	k1gInSc2	vitamín
C.	C.	kA	C.
Jeho	jeho	k3xOp3gInSc4	jeho
obsah	obsah	k1gInSc4	obsah
je	být	k5eAaImIp3nS	být
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
citrusovými	citrusový	k2eAgInPc7d1	citrusový
plody	plod	k1gInPc7	plod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
odrůdě	odrůda	k1gFnSc6	odrůda
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
od	od	k7c2	od
40	[number]	k4	40
do	do	k7c2	do
90	[number]	k4	90
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
100	[number]	k4	100
g	g	kA	g
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
vysokého	vysoký	k2eAgInSc2d1	vysoký
podílu	podíl	k1gInSc2	podíl
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
bohatým	bohatý	k2eAgInSc7d1	bohatý
zdrojem	zdroj	k1gInSc7	zdroj
vitamínu	vitamín	k1gInSc2	vitamín
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
E	E	kA	E
a	a	k8xC	a
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
kyseliny	kyselina	k1gFnPc1	kyselina
jablečné	jablečný	k2eAgFnPc1d1	jablečná
<g/>
,	,	kIx,	,
citrónové	citrónový	k2eAgFnPc1d1	citrónová
<g/>
,	,	kIx,	,
chininové	chininový	k2eAgFnPc1d1	chininová
<g/>
,	,	kIx,	,
šťavelové	šťavelový	k2eAgFnPc1d1	šťavelová
<g/>
,	,	kIx,	,
salicylové	salicylový	k2eAgFnPc1d1	salicylová
či	či	k8xC	či
ellagová	ellagový	k2eAgFnSc1d1	ellagová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
prevenci	prevence	k1gFnSc6	prevence
nádorových	nádorový	k2eAgFnPc2d1	nádorová
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
kyseliny	kyselina	k1gFnPc1	kyselina
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
udávající	udávající	k2eAgFnSc4d1	udávající
typickou	typický	k2eAgFnSc4d1	typická
a	a	k8xC	a
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
vůni	vůně	k1gFnSc4	vůně
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Jahoda	jahoda	k1gFnSc1	jahoda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
lehce	lehko	k6eAd1	lehko
stravitelnou	stravitelný	k2eAgFnSc4d1	stravitelná
vlákninu	vláknina	k1gFnSc4	vláknina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadné	snadný	k2eAgFnPc4d1	snadná
další	další	k2eAgFnPc4d1	další
tepelné	tepelný	k2eAgFnPc4d1	tepelná
úpravy	úprava	k1gFnPc4	úprava
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
marmelád	marmeláda	k1gFnPc2	marmeláda
<g/>
,	,	kIx,	,
či	či	k8xC	či
džemů	džem	k1gInPc2	džem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jahodě	jahoda	k1gFnSc6	jahoda
zastoupen	zastoupen	k2eAgMnSc1d1	zastoupen
převážně	převážně	k6eAd1	převážně
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
a	a	k8xC	a
pak	pak	k6eAd1	pak
některé	některý	k3yIgInPc4	některý
netypické	typický	k2eNgInPc4d1	netypický
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
fluor	fluor	k1gInSc1	fluor
<g/>
,	,	kIx,	,
kobalt	kobalt	k1gInSc1	kobalt
či	či	k8xC	či
molybden	molybden	k1gInSc1	molybden
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
také	také	k9	také
výrazným	výrazný	k2eAgInSc7d1	výrazný
zdrojem	zdroj	k1gInSc7	zdroj
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
z	z	k7c2	z
87	[number]	k4	87
%	%	kIx~	%
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
druhům	druh	k1gInPc3	druh
ovoce	ovoce	k1gNnSc2	ovoce
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
malý	malý	k2eAgInSc4d1	malý
podíl	podíl	k1gInSc4	podíl
přírodních	přírodní	k2eAgInPc2d1	přírodní
cukrů	cukr	k1gInPc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Konzumace	konzumace	k1gFnSc1	konzumace
jahod	jahoda	k1gFnPc2	jahoda
má	mít	k5eAaImIp3nS	mít
údajně	údajně	k6eAd1	údajně
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
účinek	účinek	k1gInSc1	účinek
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
testosteronu	testosteron	k1gInSc2	testosteron
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
spermie	spermie	k1gFnPc4	spermie
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
v	v	k7c6	v
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
orgánech	orgán	k1gInPc6	orgán
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
antioxidantů	antioxidant	k1gInPc2	antioxidant
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podporují	podporovat	k5eAaImIp3nP	podporovat
metabolismus	metabolismus	k1gInSc4	metabolismus
<g/>
,	,	kIx,	,
vylučování	vylučování	k1gNnSc4	vylučování
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
vzniku	vznik	k1gInSc2	vznik
močových	močový	k2eAgMnPc2d1	močový
a	a	k8xC	a
žlučových	žlučový	k2eAgMnPc2d1	žlučový
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
snižují	snižovat	k5eAaImIp3nP	snižovat
nachlazení	nachlazení	k1gNnSc4	nachlazení
<g/>
,	,	kIx,	,
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
chorob	choroba	k1gFnPc2	choroba
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Jahoda	jahoda	k1gFnSc1	jahoda
není	být	k5eNaImIp3nS	být
bobule	bobule	k1gFnSc1	bobule
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
za	za	k7c4	za
bobuli	bobule	k1gFnSc4	bobule
nebo	nebo	k8xC	nebo
plod	plod	k1gInSc4	plod
považována	považován	k2eAgFnSc1d1	považována
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bobulí	bobule	k1gFnPc2	bobule
jsou	být	k5eAaImIp3nP	být
semena	semeno	k1gNnPc1	semeno
obklopena	obklopit	k5eAaPmNgNnP	obklopit
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
šťavnatou	šťavnatý	k2eAgFnSc7d1	šťavnatá
dužninou	dužnina	k1gFnSc7	dužnina
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
angrešt	angrešt	k1gInSc4	angrešt
<g/>
,	,	kIx,	,
rybíz	rybíz	k1gInSc1	rybíz
nebo	nebo	k8xC	nebo
borůvka	borůvka	k1gFnSc1	borůvka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
také	také	k9	také
okurka	okurka	k1gFnSc1	okurka
<g/>
,	,	kIx,	,
řepa	řepa	k1gFnSc1	řepa
nebo	nebo	k8xC	nebo
dýně	dýně	k1gFnSc1	dýně
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	Jahoda	k1gMnPc4	Jahoda
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
konzumován	konzumován	k2eAgInSc4d1	konzumován
plod	plod	k1gInSc4	plod
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
u	u	k7c2	u
jahodníku	jahodník	k1gInSc2	jahodník
nejde	jít	k5eNaImIp3nS	jít
botanicky	botanicky	k6eAd1	botanicky
o	o	k7c4	o
plod	plod	k1gInSc4	plod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zbytnělé	zbytnělý	k2eAgNnSc1d1	zbytnělé
květní	květní	k2eAgNnSc1d1	květní
lůžko	lůžko	k1gNnSc1	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInPc1d1	vlastní
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
malá	malý	k2eAgNnPc4d1	malé
zelená	zelený	k2eAgNnPc4d1	zelené
zrníčka	zrníčko	k1gNnPc4	zrníčko
na	na	k7c6	na
červené	červený	k2eAgFnSc6d1	červená
slupce	slupka	k1gFnSc6	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
již	již	k9	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
konzumovány	konzumovat	k5eAaBmNgFnP	konzumovat
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnSc6d1	kamenná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
konzumaci	konzumace	k1gFnSc4	konzumace
se	se	k3xPyFc4	se
ale	ale	k9	ale
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
až	až	k9	až
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
pěstovány	pěstovat	k5eAaImNgInP	pěstovat
převážně	převážně	k6eAd1	převážně
mnichy	mnich	k1gInPc1	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Zahradní	zahradní	k2eAgFnPc1d1	zahradní
jahody	jahoda	k1gFnPc1	jahoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
konzumovány	konzumovat	k5eAaBmNgInP	konzumovat
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
křížením	křížení	k1gNnSc7	křížení
několika	několik	k4yIc2	několik
druhů	druh	k1gInPc2	druh
jahod	jahoda	k1gFnPc2	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
přivezeny	přivezen	k2eAgFnPc1d1	přivezena
až	až	k6eAd1	až
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
korzáry	korzár	k1gMnPc7	korzár
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
současných	současný	k2eAgFnPc2d1	současná
jahod	jahoda	k1gFnPc2	jahoda
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
křížení	křížení	k1gNnSc1	křížení
druhů	druh	k1gInPc2	druh
zahradníkem	zahradník	k1gMnSc7	zahradník
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XV	XV	kA	XV
jménem	jméno	k1gNnSc7	jméno
Antoino	Antoino	k1gNnSc4	Antoino
Duchesne	Duchesne	k1gFnSc1	Duchesne
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
práci	práce	k1gFnSc3	práce
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zkřížení	zkřížení	k1gNnSc3	zkřížení
jahodníku	jahodník	k1gInSc2	jahodník
virginského	virginský	k2eAgInSc2d1	virginský
a	a	k8xC	a
jahodníku	jahodník	k1gInSc2	jahodník
chilského	chilský	k2eAgMnSc2d1	chilský
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
nové	nový	k2eAgFnSc2d1	nová
odrůdy	odrůda	k1gFnSc2	odrůda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jahodníku	jahodník	k1gInSc2	jahodník
ananasového	ananasový	k2eAgInSc2d1	ananasový
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
považovány	považován	k2eAgFnPc1d1	považována
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
chuť	chuť	k1gFnSc4	chuť
za	za	k7c4	za
královské	královský	k2eAgNnSc4d1	královské
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
či	či	k8xC	či
za	za	k7c4	za
ovoce	ovoce	k1gNnSc4	ovoce
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jejich	jejich	k3xOp3gFnSc1	jejich
konzumace	konzumace	k1gFnSc1	konzumace
měla	mít	k5eAaImAgFnS	mít
podporovat	podporovat	k5eAaImF	podporovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
sexu	sex	k1gInSc3	sex
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
jahody	jahoda	k1gFnPc1	jahoda
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
míře	míra	k1gFnSc6	míra
pěstovat	pěstovat	k5eAaImF	pěstovat
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Neveklovska	Neveklovsko	k1gNnSc2	Neveklovsko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jahodník	jahodník	k1gInSc1	jahodník
velkoplodý	velkoplodý	k2eAgInSc1d1	velkoplodý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgFnPc4d1	pěstována
různé	různý	k2eAgFnPc4d1	různá
odrůdy	odrůda	k1gFnPc4	odrůda
jahodníku	jahodník	k1gInSc2	jahodník
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
křížence	kříženka	k1gFnSc6	kříženka
více	hodně	k6eAd2	hodně
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgFnPc4d1	pěstována
odrůdy	odrůda	k1gFnPc4	odrůda
křížence	kříženec	k1gMnSc2	kříženec
jahodník	jahodník	k1gInSc1	jahodník
velkoplodý	velkoplodý	k2eAgInSc1d1	velkoplodý
(	(	kIx(	(
<g/>
Fragaria	Fragarium	k1gNnSc2	Fragarium
×	×	k?	×
ananassa	ananassa	k1gFnSc1	ananassa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
jahody	jahoda	k1gFnPc1	jahoda
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
rostliny	rostlina	k1gFnPc4	rostlina
větší	veliký	k2eAgFnPc4d2	veliký
vzrůstem	vzrůst	k1gInSc7	vzrůst
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInPc1d2	veliký
plody	plod	k1gInPc1	plod
než	než	k8xS	než
plané	planý	k2eAgInPc1d1	planý
druhy	druh	k1gInPc1	druh
jahod	jahoda	k1gFnPc2	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
více	hodně	k6eAd2	hodně
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
sklizně	sklizeň	k1gFnSc2	sklizeň
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
prodejci	prodejce	k1gMnPc7	prodejce
rozdělovány	rozdělován	k2eAgInPc1d1	rozdělován
odrůdy	odrůda	k1gFnPc4	odrůda
jahodníku	jahodník	k1gInSc2	jahodník
velkoplodého	velkoplodý	k2eAgInSc2d1	velkoplodý
na	na	k7c6	na
<g/>
:	:	kIx,	:
jednouplodicí	jednouplodicí	k2eAgFnSc2d1	jednouplodicí
odrůdy	odrůda	k1gFnSc2	odrůda
které	který	k3yRgInPc1	který
dávají	dávat	k5eAaImIp3nP	dávat
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc4	dva
sklizně	sklizeň	k1gFnPc4	sklizeň
do	do	k7c2	do
roka	rok	k1gInSc2	rok
"	"	kIx"	"
<g/>
stáleplodicí	stáleplodicí	k2eAgMnSc1d1	stáleplodicí
<g/>
"	"	kIx"	"
-	-	kIx~	-
mezi	mezi	k7c4	mezi
odrůdy	odrůda	k1gFnPc4	odrůda
"	"	kIx"	"
<g/>
stáleplodících	stáleplodící	k2eAgInPc2d1	stáleplodící
<g/>
"	"	kIx"	"
jahodníků	jahodník	k1gInPc2	jahodník
zařazujeme	zařazovat	k5eAaImIp1nP	zařazovat
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
ČR	ČR	kA	ČR
plodí	plodit	k5eAaImIp3nP	plodit
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tyto	tento	k3xDgMnPc4	tento
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
odrůd	odrůda	k1gFnPc2	odrůda
plodnost	plodnost	k1gFnSc1	plodnost
spíše	spíše	k9	spíše
žádná	žádný	k3yNgFnSc1	žádný
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
měsíční	měsíční	k2eAgFnPc1d1	měsíční
<g/>
"	"	kIx"	"
jahody	jahoda	k1gFnPc1	jahoda
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
druhu	druh	k1gInSc2	druh
Fragaria	Fragarium	k1gNnSc2	Fragarium
vesca	vescum	k1gNnSc2	vescum
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
jahodník	jahodník	k1gInSc4	jahodník
velkoplodý	velkoplodý	k2eAgInSc4d1	velkoplodý
<g/>
.	.	kIx.	.
</s>
<s>
Pěstované	pěstovaný	k2eAgInPc1d1	pěstovaný
kultivary	kultivar	k1gInPc1	kultivar
plodí	plodit	k5eAaImIp3nP	plodit
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
,	,	kIx,	,
až	až	k9	až
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
menší	malý	k2eAgInPc4d2	menší
plody	plod	k1gInPc4	plod
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc1d1	malý
plody	plod	k1gInPc1	plod
v	v	k7c6	v
nevelkém	velký	k2eNgNnSc6d1	nevelké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
dekorativní	dekorativní	k2eAgNnSc4d1	dekorativní
ovoce	ovoce	k1gNnSc4	ovoce
(	(	kIx(	(
<g/>
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Day	Day	k1gMnSc1	Day
neutral	utrat	k5eNaPmAgMnS	utrat
<g/>
"	"	kIx"	"
-	-	kIx~	-
jahodníky	jahodník	k1gInPc1	jahodník
plodící	plodící	k2eAgInSc1d1	plodící
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jahodníky	jahodník	k1gInPc1	jahodník
ovšem	ovšem	k9	ovšem
dávají	dávat	k5eAaImIp3nP	dávat
velkou	velký	k2eAgFnSc4d1	velká
úrodu	úroda	k1gFnSc4	úroda
také	také	k9	také
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
se	se	k3xPyFc4	se
sklízejí	sklízet	k5eAaImIp3nP	sklízet
ručním	ruční	k2eAgInSc7d1	ruční
sběrem	sběr	k1gInSc7	sběr
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
trhány	trhán	k2eAgInPc1d1	trhán
z	z	k7c2	z
jahodníku	jahodník	k1gInSc2	jahodník
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
období	období	k1gNnSc1	období
sklizně	sklizeň	k1gFnSc2	sklizeň
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Sklízené	sklízený	k2eAgFnPc1d1	sklízená
jahody	jahoda	k1gFnPc1	jahoda
mají	mít	k5eAaImIp3nP	mít
svěže	svěže	k6eAd1	svěže
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
dozrálé	dozrálý	k2eAgFnPc1d1	dozrálá
<g/>
.	.	kIx.	.
</s>
<s>
Nezralé	zralý	k2eNgInPc1d1	nezralý
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
či	či	k8xC	či
nažloutlé	nažloutlý	k2eAgNnSc1d1	nažloutlé
a	a	k8xC	a
přezrále	přezrále	k6eAd1	přezrále
pak	pak	k6eAd1	pak
tmavě	tmavě	k6eAd1	tmavě
rudé	rudý	k2eAgFnPc1d1	rudá
a	a	k8xC	a
na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
již	již	k9	již
měkké	měkký	k2eAgNnSc1d1	měkké
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
sklízet	sklízet	k5eAaImF	sklízet
jahody	jahoda	k1gFnPc4	jahoda
dopoledne	dopoledne	k1gNnSc2	dopoledne
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
oschlé	oschlý	k2eAgFnPc1d1	oschlá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
stále	stále	k6eAd1	stále
lehce	lehko	k6eAd1	lehko
podchlazené	podchlazený	k2eAgFnPc1d1	podchlazená
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
udržuje	udržovat	k5eAaImIp3nS	udržovat
jejich	jejich	k3xOp3gInSc4	jejich
stav	stav	k1gInSc4	stav
déle	dlouho	k6eAd2	dlouho
čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
<g/>
.	.	kIx.	.
</s>
<s>
Natrhané	natrhaný	k2eAgFnPc1d1	natrhaná
jahody	jahoda	k1gFnPc1	jahoda
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
umístit	umístit	k5eAaPmF	umístit
do	do	k7c2	do
ledničky	lednička	k1gFnSc2	lednička
<g/>
,	,	kIx,	,
či	či	k8xC	či
chladné	chladný	k2eAgFnSc2d1	chladná
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
déle	dlouho	k6eAd2	dlouho
vydržely	vydržet	k5eAaPmAgFnP	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jahod	jahoda	k1gFnPc2	jahoda
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
pěstována	pěstován	k2eAgFnSc1d1	pěstována
na	na	k7c6	na
obrovských	obrovský	k2eAgFnPc6d1	obrovská
jahodových	jahodový	k2eAgFnPc6d1	jahodová
plantážích	plantáž	k1gFnPc6	plantáž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozesety	rozesít	k5eAaPmNgFnP	rozesít
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
pěstitele	pěstitel	k1gMnPc4	pěstitel
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
či	či	k8xC	či
severské	severský	k2eAgInPc1d1	severský
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgFnSc1d1	roční
produkce	produkce	k1gFnSc1	produkce
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
2	[number]	k4	2
milióny	milión	k4xCgInPc1	milión
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výraznou	výrazný	k2eAgFnSc4d1	výrazná
vývozní	vývozní	k2eAgFnSc4d1	vývozní
komoditu	komodita	k1gFnSc4	komodita
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
zahradních	zahradní	k2eAgFnPc6d1	zahradní
jahodách	jahoda	k1gFnPc6	jahoda
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
uváděny	uvádět	k5eAaImNgFnP	uvádět
jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
alergických	alergický	k2eAgFnPc2d1	alergická
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
však	však	k9	však
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
alergickou	alergický	k2eAgFnSc4d1	alergická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tzv.	tzv.	kA	tzv.
pseudoalergickou	pseudoalergický	k2eAgFnSc4d1	pseudoalergický
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
podobnými	podobný	k2eAgInPc7d1	podobný
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
kopřivka	kopřivka	k1gFnSc1	kopřivka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vyvolávány	vyvoláván	k2eAgMnPc4d1	vyvoláván
jiným	jiný	k2eAgInSc7d1	jiný
mechanismem	mechanismus	k1gInSc7	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
se	se	k3xPyFc4	se
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
jak	jak	k6eAd1	jak
za	za	k7c2	za
syrova	syrov	k1gInSc2	syrov
po	po	k7c6	po
utržení	utržení	k1gNnSc6	utržení
z	z	k7c2	z
keříku	keřík	k1gInSc2	keřík
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
po	po	k7c6	po
tepelné	tepelný	k2eAgFnSc6d1	tepelná
úpravě	úprava	k1gFnSc6	úprava
<g/>
,	,	kIx,	,
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
jako	jako	k9	jako
náplň	náplň	k1gFnSc1	náplň
například	například	k6eAd1	například
do	do	k7c2	do
ovocných	ovocný	k2eAgInPc2d1	ovocný
(	(	kIx(	(
<g/>
jahodových	jahodový	k2eAgInPc2d1	jahodový
<g/>
)	)	kIx)	)
knedlíků	knedlík	k1gInPc2	knedlík
<g/>
,	,	kIx,	,
do	do	k7c2	do
pečiva	pečivo	k1gNnSc2	pečivo
(	(	kIx(	(
<g/>
jahodový	jahodový	k2eAgInSc1d1	jahodový
koláč	koláč	k1gInSc1	koláč
<g/>
,	,	kIx,	,
jahodová	jahodový	k2eAgFnSc1d1	jahodová
bublanina	bublanina	k1gFnSc1	bublanina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
zavařují	zavařovat	k5eAaImIp3nP	zavařovat
do	do	k7c2	do
kompotů	kompot	k1gInPc2	kompot
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
moštů	mošt	k1gInPc2	mošt
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášená	vyhlášený	k2eAgFnSc1d1	vyhlášená
je	být	k5eAaImIp3nS	být
konzumace	konzumace	k1gFnSc1	konzumace
jahod	jahoda	k1gFnPc2	jahoda
se	se	k3xPyFc4	se
šlehačkou	šlehačka	k1gFnSc7	šlehačka
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jako	jako	k9	jako
dekorační	dekorační	k2eAgInSc1d1	dekorační
doplněk	doplněk	k1gInSc1	doplněk
pro	pro	k7c4	pro
míchané	míchaný	k2eAgInPc4d1	míchaný
nápoje	nápoj	k1gInPc4	nápoj
a	a	k8xC	a
značné	značný	k2eAgFnPc4d1	značná
obliby	obliba	k1gFnPc4	obliba
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
zastoupení	zastoupení	k1gNnSc6	zastoupení
má	mít	k5eAaImIp3nS	mít
konzumace	konzumace	k1gFnSc1	konzumace
jahod	jahoda	k1gFnPc2	jahoda
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sektem	sekt	k1gInSc7	sekt
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
mají	mít	k5eAaImIp3nP	mít
výrazné	výrazný	k2eAgFnPc4d1	výrazná
aromatické	aromatický	k2eAgFnPc4d1	aromatická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
u	u	k7c2	u
celé	celý	k2eAgFnSc2d1	celá
škály	škála	k1gFnSc2	škála
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Jahodová	jahodový	k2eAgFnSc1d1	jahodová
příchuť	příchuť	k1gFnSc1	příchuť
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
známá	známý	k2eAgFnSc1d1	známá
u	u	k7c2	u
žvýkaček	žvýkačka	k1gFnPc2	žvýkačka
<g/>
,	,	kIx,	,
bonbónů	bonbón	k1gInPc2	bonbón
<g/>
,	,	kIx,	,
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
,	,	kIx,	,
sušenek	sušenka	k1gFnPc2	sušenka
<g/>
,	,	kIx,	,
vůní	vůně	k1gFnPc2	vůně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
například	například	k6eAd1	například
u	u	k7c2	u
kondomů	kondom	k1gInPc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Jahody	jahoda	k1gFnPc1	jahoda
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
také	také	k9	také
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
prospěšný	prospěšný	k2eAgInSc1d1	prospěšný
pro	pro	k7c4	pro
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
růst	růst	k1gInSc4	růst
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
jahody	jahoda	k1gFnPc1	jahoda
využívaly	využívat	k5eAaPmAgFnP	využívat
i	i	k9	i
pro	pro	k7c4	pro
bělení	bělení	k1gNnSc4	bělení
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
