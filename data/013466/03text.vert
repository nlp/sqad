<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
Eriksen	Eriksna	k1gFnPc2	Eriksna
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
norský	norský	k2eAgMnSc1d1	norský
antropolog	antropolog	k1gMnSc1	antropolog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
sociální	sociální	k2eAgFnSc2d1	sociální
antropologie	antropologie	k1gFnSc2	antropologie
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
hlavní	hlavní	k2eAgInPc4d1	hlavní
okruhy	okruh	k1gInPc4	okruh
zájmu	zájem	k1gInSc2	zájem
patří	patřit	k5eAaImIp3nP	patřit
etnicita	etnicita	k1gFnSc1	etnicita
<g/>
,	,	kIx,	,
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
<g/>
,	,	kIx,	,
globalizace	globalizace	k1gFnSc1	globalizace
a	a	k8xC	a
multikulturalismus	multikulturalismus	k1gInSc1	multikulturalismus
<g/>
.	.	kIx.	.
</s>
<s>
Terénní	terénní	k2eAgInSc1d1	terénní
výzkum	výzkum	k1gInSc1	výzkum
prováděl	provádět	k5eAaImAgInS	provádět
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Trinidadu	Trinidad	k1gInSc6	Trinidad
<g/>
,	,	kIx,	,
Mauriciu	Mauricium	k1gNnSc6	Mauricium
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Nø	Nø	k1gFnSc6	Nø
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
gymnázium	gymnázium	k1gNnSc4	gymnázium
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Tø	Tø	k1gFnSc2	Tø
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Eriksen	Eriksen	k1gInSc1	Eriksen
věnoval	věnovat	k5eAaImAgInS	věnovat
výzkumu	výzkum	k1gInSc3	výzkum
etnicity	etnicita	k1gFnSc2	etnicita
na	na	k7c6	na
Trinidadu	Trinidad	k1gInSc6	Trinidad
a	a	k8xC	a
Mauriciu	Mauricium	k1gNnSc6	Mauricium
<g/>
.	.	kIx.	.
</s>
<s>
Profesorem	profesor	k1gMnSc7	profesor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
popularizaci	popularizace	k1gFnSc3	popularizace
sociální	sociální	k2eAgFnSc2d1	sociální
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
veřejné	veřejný	k2eAgFnPc4d1	veřejná
debaty	debata	k1gFnPc4	debata
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c4	za
Norskou	norský	k2eAgFnSc4d1	norská
stranu	strana	k1gFnSc4	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
let	let	k1gInSc1	let
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
magazínu	magazín	k1gInSc6	magazín
Samtiden	Samtidna	k1gFnPc2	Samtidna
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vedl	vést	k5eAaImAgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
kritikům	kritik	k1gMnPc3	kritik
norského	norský	k2eAgInSc2d1	norský
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
komentoval	komentovat	k5eAaBmAgMnS	komentovat
proces	proces	k1gInSc4	proces
s	s	k7c7	s
norským	norský	k2eAgMnSc7d1	norský
masovým	masový	k2eAgMnSc7d1	masový
vrahem	vrah	k1gMnSc7	vrah
Andersem	Anders	k1gMnSc7	Anders
Behringem	Behring	k1gInSc7	Behring
Breivikem	Breivik	k1gMnSc7	Breivik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
pachatelem	pachatel	k1gMnSc7	pachatel
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Breivik	Breivik	k1gInSc1	Breivik
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
Eriksena	Eriksen	k2eAgFnSc1d1	Eriksena
citoval	citovat	k5eAaBmAgMnS	citovat
jako	jako	k9	jako
představitele	představitel	k1gMnPc4	představitel
multikulturalismu	multikulturalismus	k1gInSc2	multikulturalismus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
dekonstrukci	dekonstrukce	k1gFnSc4	dekonstrukce
norského	norský	k2eAgNnSc2d1	norské
etnika	etnikum	k1gNnSc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Eriksen	Eriksen	k1gInSc1	Eriksen
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
také	také	k9	také
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
jako	jako	k8xC	jako
svědek	svědek	k1gMnSc1	svědek
obhajoby	obhajoba	k1gFnSc2	obhajoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
bibliografie	bibliografie	k1gFnSc2	bibliografie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
dosud	dosud	k6eAd1	dosud
vyšly	vyjít	k5eAaPmAgFnP	vyjít
tyto	tento	k3xDgFnPc1	tento
práce	práce	k1gFnPc1	práce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Odpady	odpad	k1gInPc1	odpad
<g/>
:	:	kIx,	:
Odpad	odpad	k1gInSc1	odpad
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nechtěných	chtěný	k2eNgInPc2d1	nechtěný
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
účinků	účinek	k1gInPc2	účinek
Eriksen	Eriksna	k1gFnPc2	Eriksna
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
Etnicita	etnicita	k1gFnSc1	etnicita
a	a	k8xC	a
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
<g/>
:	:	kIx,	:
antropologické	antropologický	k2eAgFnSc2d1	antropologická
perspektivy	perspektiva	k1gFnSc2	perspektiva
Eriksen	Eriksen	k1gInSc1	Eriksen
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Syndrom	syndrom	k1gInSc1	syndrom
velkého	velký	k2eAgMnSc2d1	velký
vlka	vlk	k1gMnSc2	vlk
<g/>
:	:	kIx,	:
hledání	hledání	k1gNnSc1	hledání
štěstí	štěstí	k1gNnSc2	štěstí
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
nadbytku	nadbytek	k1gInSc2	nadbytek
Eriksen	Eriksen	k1gInSc1	Eriksen
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
antropologie	antropologie	k1gFnSc1	antropologie
<g/>
:	:	kIx,	:
příbuzenství	příbuzenství	k1gNnSc1	příbuzenství
<g/>
,	,	kIx,	,
národnostní	národnostní	k2eAgFnSc4d1	národnostní
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
,	,	kIx,	,
rituál	rituál	k1gInSc4	rituál
Eriksen	Eriksna	k1gFnPc2	Eriksna
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Antropologie	antropologie	k1gFnSc1	antropologie
multikulturních	multikulturní	k2eAgFnPc2d1	multikulturní
společností	společnost	k1gFnPc2	společnost
<g/>
:	:	kIx,	:
rozumět	rozumět	k5eAaImF	rozumět
identitě	identita	k1gFnSc3	identita
Eriksen	Eriksna	k1gFnPc2	Eriksna
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
</s>
</p>
<p>
<s>
Tyranie	tyranie	k1gFnSc1	tyranie
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
:	:	kIx,	:
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
pomalý	pomalý	k2eAgInSc1d1	pomalý
čas	čas	k1gInSc1	čas
v	v	k7c6	v
informačním	informační	k2eAgInSc6d1	informační
věku	věk	k1gInSc6	věk
Eriksen	Eriksen	k1gInSc1	Eriksen
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
Eriksen	Eriksna	k1gFnPc2	Eriksna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hyllanda	k1gFnPc2	Hyllanda
Eriksen	Eriksna	k1gFnPc2	Eriksna
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Thomas	Thomas	k1gMnSc1	Thomas
Hylland	Hylland	k1gInSc1	Hylland
Eriksen	Eriksen	k2eAgMnSc1d1	Eriksen
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Thomad	Thomad	k1gInSc1	Thomad
Hylland	Hylland	k1gInSc1	Hylland
Eriksen	Eriksen	k1gInSc1	Eriksen
–	–	k?	–
profil	profil	k1gInSc4	profil
na	na	k7c6	na
Katedře	katedra	k1gFnSc6	katedra
sociální	sociální	k2eAgFnSc2d1	sociální
antropologie	antropologie	k1gFnSc2	antropologie
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Eriksen	Eriksen	k1gInSc1	Eriksen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
site	site	k1gNnSc7	site
-	-	kIx~	-
osobní	osobní	k2eAgFnSc2d1	osobní
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Eriksen	Eriksen	k1gInSc1	Eriksen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
blog	bloga	k1gFnPc2	bloga
-	-	kIx~	-
blog	blog	k1gInSc1	blog
</s>
</p>
