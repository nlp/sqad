<s>
V	v	k7c6
antické	antický	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
bývá	bývat	k5eAaImIp3nS
popisován	popisován	k2eAgMnSc1d1
a	a	k8xC
zpodobován	zpodobován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
černý	černý	k2eAgMnSc1d1
pes	pes	k1gMnSc1
extrémní	extrémní	k2eAgFnSc2d1
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
dvěma	dva	k4xCgInPc7
nebo	nebo	k8xC
třemi	tři	k4xCgFnPc7
hlavami	hlava	k1gFnPc7
<g/>
,	,	kIx,
dračím	dračí	k2eAgInSc7d1
ohonem	ohon	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
krku	krk	k1gInSc6
a	a	k8xC
na	na	k7c6
nohách	noha	k1gFnPc6
mívá	mívat	k5eAaImIp3nS
kromě	kromě	k7c2
srsti	srst	k1gFnSc2
hady	had	k1gMnPc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc4
sliny	slina	k1gFnPc1
jsou	být	k5eAaImIp3nP
jedovaté	jedovatý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>