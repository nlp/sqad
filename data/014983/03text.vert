<s>
Kerberos	Kerberos	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
postavě	postava	k1gFnSc6
z	z	k7c2
mytologie	mytologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Kerberos	Kerberos	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kerberos	Kerberos	k1gMnSc1
Kerberos	Kerberos	k1gMnSc1
na	na	k7c6
obraze	obraz	k1gInSc6
od	od	k7c2
Williama	William	k1gMnSc2
Blakea	Blakeus	k1gMnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Týfón	Týfón	k1gInSc1
a	a	k8xC
Echidna	Echidna	k1gFnSc1
Příbuzní	příbuzný	k2eAgMnPc1d1
</s>
<s>
Orthos	Orthos	k1gInSc1
<g/>
,	,	kIx,
Hydra	hydra	k1gFnSc1
<g/>
,	,	kIx,
nemejský	mejský	k2eNgInSc1d1
lev	lev	k1gInSc1
a	a	k8xC
Chiméra	chiméra	k1gFnSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eurystheus	Eurystheus	k1gMnSc1
sleduje	sledovat	k5eAaImIp3nS
dvouhlavého	dvouhlavý	k2eAgMnSc4d1
Kerbera	Kerber	k1gMnSc4
ukryt	ukryt	k2eAgInSc4d1
v	v	k7c6
nádobě	nádoba	k1gFnSc6
na	na	k7c4
obilí	obilí	k1gNnSc4
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
př.n.l.	př.n.l.	k?
<g/>
,	,	kIx,
Louvre	Louvre	k1gInSc1
</s>
<s>
Kerberos	Kerberos	k1gMnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Κ	Κ	k?
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Cerberus	Cerberus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
antické	antický	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
trojhlavý	trojhlavý	k2eAgMnSc1d1
černý	černý	k2eAgMnSc1d1
pes	pes	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
střeží	střežit	k5eAaImIp3nS
vchod	vchod	k1gInSc4
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovnitř	dovnitř	k6eAd1
pustí	pustit	k5eAaPmIp3nS
každého	každý	k3xTgMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ven	ven	k6eAd1
nikdo	nikdo	k3yNnSc1
živý	živý	k2eAgMnSc1d1
nevyjde	vyjít	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgFnPc2d1
představ	představa	k1gFnPc2
střežil	střežit	k5eAaImAgInS
především	především	k6eAd1
podsvětí	podsvětí	k1gNnSc4
před	před	k7c7
vstupem	vstup	k1gInSc7
živých	živý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
V	v	k7c6
antické	antický	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
bývá	bývat	k5eAaImIp3nS
popisován	popisován	k2eAgMnSc1d1
a	a	k8xC
zpodobován	zpodobován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
černý	černý	k2eAgMnSc1d1
pes	pes	k1gMnSc1
extrémní	extrémní	k2eAgFnSc2d1
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
dvěma	dva	k4xCgInPc7
nebo	nebo	k8xC
třemi	tři	k4xCgFnPc7
hlavami	hlava	k1gFnPc7
<g/>
,	,	kIx,
dračím	dračí	k2eAgInSc7d1
ohonem	ohon	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
krku	krk	k1gInSc6
a	a	k8xC
na	na	k7c6
nohách	noha	k1gFnPc6
mívá	mívat	k5eAaImIp3nS
kromě	kromě	k7c2
srsti	srst	k1gFnSc2
hady	had	k1gMnPc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc4
sliny	slina	k1gFnPc1
jsou	být	k5eAaImIp3nP
jedovaté	jedovatý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hésiodos	Hésiodos	k1gMnSc1
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hlav	hlava	k1gFnPc2
je	být	k5eAaImIp3nS
nespočetně	spočetně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
Kerberových	Kerberův	k2eAgFnPc2d1
hlav	hlava	k1gFnPc2
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
nejasný	jasný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
trojhlavý	trojhlavý	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
uváděn	uvádět	k5eAaImNgInS
až	až	k9
v	v	k7c6
římské	římský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
popisoval	popisovat	k5eAaImAgMnS
jako	jako	k9
stohlavý	stohlavý	k2eAgMnSc1d1
či	či	k8xC
mnohohlavý	mnohohlavý	k2eAgMnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
jedovatým	jedovatý	k2eAgInSc7d1
dechem	dech	k1gInSc7
a	a	k8xC
množstvím	množství	k1gNnSc7
hadích	hadí	k2eAgFnPc2d1
hlav	hlava	k1gFnPc2
na	na	k7c6
šíjích	šíj	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
Kerberos	Kerberos	k1gMnSc1
spatřil	spatřit	k5eAaPmAgMnS
světlo	světlo	k1gNnSc4
světa	svět	k1gInSc2
jenom	jenom	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
když	když	k8xS
ho	on	k3xPp3gNnSc4
vyvedl	vyvést	k5eAaPmAgMnS
z	z	k7c2
podsvětí	podsvětí	k1gNnSc2
Héraklés	Héraklésa	k1gFnPc2
při	při	k7c6
plnění	plnění	k1gNnSc6
dvanáctého	dvanáctý	k4xOgInSc2
úkolu	úkol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mykénský	mykénský	k2eAgMnSc1d1
král	král	k1gMnSc1
Eurystheus	Eurystheus	k1gMnSc1
neuznal	uznat	k5eNaPmAgMnS
Héraklovi	Hérakles	k1gMnSc3
splnění	splnění	k1gNnSc3
dvou	dva	k4xCgInPc2
z	z	k7c2
deseti	deset	k4xCc2
úkolů	úkol	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
plnil	plnit	k5eAaImAgInS
s	s	k7c7
pomocníky	pomocník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uložil	uložit	k5eAaPmAgMnS
proto	proto	k8xC
Héraklovi	Héraklův	k2eAgMnPc1d1
dva	dva	k4xCgInPc4
další	další	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
dvanáctým	dvanáctý	k4xOgFnPc3
bylo	být	k5eAaImAgNnS
chytit	chytit	k5eAaPmF
v	v	k7c6
podsvětí	podsvětí	k1gNnSc6
hlídače	hlídač	k1gInSc2
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
psa	pes	k1gMnSc2
Kerbera	Kerber	k1gMnSc2
a	a	k8xC
přivést	přivést	k5eAaPmF
ho	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hérakles	Hérakles	k1gMnSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
se	se	k3xPyFc4
podsvětí	podsvětí	k1gNnSc1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
bohů	bůh	k1gMnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
tam	tam	k6eAd1
vlídně	vlídně	k6eAd1
přijat	přijmout	k5eAaPmNgMnS
samotným	samotný	k2eAgMnSc7d1
králem	král	k1gMnSc7
Hádem	Hádes	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
mu	on	k3xPp3gMnSc3
dovolil	dovolit	k5eAaPmAgMnS
Kerbera	Kerber	k1gMnSc4
chytit	chytit	k5eAaPmF
za	za	k7c4
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
dokáže	dokázat	k5eAaPmIp3nS
holýma	holý	k2eAgFnPc7d1
rukama	ruka	k1gFnPc7
<g/>
,	,	kIx,
beze	beze	k7c2
zbraně	zbraň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kerberos	Kerberos	k1gMnSc1
se	se	k3xPyFc4
zuřivě	zuřivě	k6eAd1
bránil	bránit	k5eAaImAgMnS
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gInSc4
však	však	k9
Hérakles	Hérakles	k1gMnSc1
stiskl	stisknout	k5eAaPmAgMnS
tak	tak	k6eAd1
silně	silně	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
přidusil	přidusit	k5eAaPmAgMnS
<g/>
,	,	kIx,
slíbil	slíbit	k5eAaPmAgMnS
poslušnost	poslušnost	k1gFnSc4
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
se	se	k3xPyFc4
spoutat	spoutat	k5eAaPmF
a	a	k8xC
následoval	následovat	k5eAaImAgMnS
Hérakla	Hérakles	k1gMnSc4
do	do	k7c2
Mykén	Mykény	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
král	král	k1gMnSc1
Eurystheus	Eurystheus	k1gMnSc1
spatřil	spatřit	k5eAaPmAgMnS
strašného	strašný	k2eAgMnSc4d1
strážce	strážce	k1gMnSc4
podsvětí	podsvětí	k1gNnSc2
Kerbera	Kerber	k1gMnSc4
<g/>
,	,	kIx,
vyděsil	vyděsit	k5eAaPmAgMnS
se	se	k3xPyFc4
a	a	k8xC
schoval	schovat	k5eAaPmAgMnS
do	do	k7c2
nádoby	nádoba	k1gFnSc2
na	na	k7c4
obilí	obilí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
padl	padnout	k5eAaImAgMnS,k5eAaPmAgMnS
před	před	k7c7
Héraklem	Hérakles	k1gMnSc7
na	na	k7c4
kolena	koleno	k1gNnPc4
a	a	k8xC
prosil	prosít	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
příšeru	příšera	k1gFnSc4
odvedl	odvést	k5eAaPmAgMnS
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Vergilia	Vergilius	k1gMnSc2
(	(	kIx(
<g/>
Aeneis	Aeneis	k1gFnSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
pronikl	proniknout	k5eAaPmAgInS
Aeneas	Aeneas	k1gMnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Hérakles	Hérakles	k1gMnSc1
<g/>
,	,	kIx,
do	do	k7c2
podsvětí	podsvětí	k1gNnSc2
díky	díky	k7c3
uspávacímu	uspávací	k2eAgInSc3d1
účinku	účinek	k1gInSc3
medového	medový	k2eAgInSc2d1
koláče	koláč	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Kerberovi	Kerber	k1gMnSc3
hodila	hodit	k5eAaImAgFnS,k5eAaPmAgFnS
Sibyla	Sibyla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Orfeus	Orfeus	k1gMnSc1
očaroval	očarovat	k5eAaPmAgMnS
Kerbera	Kerber	k1gMnSc4
svým	svůj	k3xOyFgInSc7
zpěvem	zpěv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Rodinné	rodinný	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Matka	matka	k1gFnSc1
<g/>
:	:	kIx,
Echidna	Echidna	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
<g/>
:	:	kIx,
Týfón	Týfón	k1gMnSc1
</s>
<s>
Sourozenci	sourozenec	k1gMnPc1
<g/>
:	:	kIx,
Orthos	Orthos	k1gMnSc1
<g/>
,	,	kIx,
Hydra	hydra	k1gFnSc1
<g/>
,	,	kIx,
Chiméra	chiméra	k1gFnSc1
<g/>
,	,	kIx,
Sfinx	sfinx	k1gInSc1
<g/>
,	,	kIx,
nemejský	mejský	k2eNgInSc1d1
lev	lev	k1gInSc1
a	a	k8xC
Skylla	Skylla	k1gFnSc1
</s>
<s>
Odraz	odraz	k1gInSc1
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
</s>
<s>
Kerberos	Kerberos	k1gMnSc1
bývá	bývat	k5eAaImIp3nS
vyobrazen	vyobrazen	k2eAgMnSc1d1
ve	v	k7c6
scénách	scéna	k1gFnPc6
chytání	chytání	k1gNnSc2
<g/>
,	,	kIx,
krocení	krocení	k1gNnSc4
Kerbera	Kerber	k1gMnSc2
<g/>
,	,	kIx,
konfrontace	konfrontace	k1gFnPc1
Kerbera	Kerber	k1gMnSc2
s	s	k7c7
Eurysteuem	Eurysteuum	k1gNnSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
vyobrazení	vyobrazení	k1gNnSc6
Hádovy	Hádův	k2eAgFnSc2d1
podsvětní	podsvětní	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgInPc6
obrazech	obraz	k1gInPc6
bývá	bývat	k5eAaImIp3nS
jen	jen	k9
rekvizitou	rekvizita	k1gFnSc7
v	v	k7c6
Héraklově	Héraklův	k2eAgNnSc6d1
pozadí	pozadí	k1gNnSc6
<g/>
,	,	kIx,
jindy	jindy	k6eAd1
Hérakles	Hérakles	k1gMnSc1
na	na	k7c6
zkrocení	zkrocení	k1gNnSc6
Kerbera	Kerber	k1gMnSc2
používá	používat	k5eAaImIp3nS
zakázanou	zakázaný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podoba	podoba	k1gFnSc1
Kerbera	Kerber	k1gMnSc2
se	se	k3xPyFc4
podle	podle	k7c2
fantazie	fantazie	k1gFnSc2
umělce	umělec	k1gMnSc2
může	moct	k5eAaImIp3nS
měnit	měnit	k5eAaImF
<g/>
,	,	kIx,
nejdále	daleko	k6eAd3
došel	dojít	k5eAaPmAgInS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
skice	skica	k1gFnSc6
dvorní	dvorní	k2eAgMnSc1d1
rudolfinský	rudolfinský	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
Giuseppe	Giusepp	k1gMnSc5
Arcimboldo	Arcimbolda	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1
archaická	archaický	k2eAgFnSc1d1
keramika	keramika	k1gFnSc1
<g/>
:	:	kIx,
černofigurová	černofigurový	k2eAgFnSc1d1
nebo	nebo	k8xC
červenofigurová	červenofigurový	k2eAgFnSc1d1
malba	malba	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
pás	pás	k1gInSc1
nádoby	nádoba	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
prohlížet	prohlížet	k5eAaImF
výjev	výjev	k1gInSc4
z	z	k7c2
různých	různý	k2eAgFnPc2d1
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
příklady	příklad	k1gInPc7
v	v	k7c6
Louvru	Louvre	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
nebo	nebo	k8xC
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Renesanční	renesanční	k2eAgFnSc1d1
malba	malba	k1gFnSc1
<g/>
,	,	kIx,
grafika	grafika	k1gFnSc1
a	a	k8xC
sochařství	sochařství	k1gNnSc1
<g/>
:	:	kIx,
silácké	silácký	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
Hérakla	Hérakles	k1gMnSc2
jako	jako	k8xC,k8xS
zápasníka	zápasník	k1gMnSc2
při	při	k7c6
krocení	krocení	k1gNnSc6
Kerbera	Kerber	k1gMnSc2
<g/>
;	;	kIx,
například	například	k6eAd1
Paolo	Paolo	k1gNnSc1
della	dell	k1gMnSc2
Stella	Stella	k1gFnSc1
<g/>
:	:	kIx,
reliéf	reliéf	k1gInSc1
na	na	k7c6
letohrádku	letohrádek	k1gInSc6
královny	královna	k1gFnSc2
Anny	Anna	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Historismus	historismus	k1gInSc1
si	se	k3xPyFc3
libuje	libovat	k5eAaImIp3nS
v	v	k7c6
popisu	popis	k1gInSc6
prostor	prostor	k1gInSc1
podsvětí	podsvětí	k1gNnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
Gustav	Gustav	k1gMnSc1
Doré	Dorá	k1gFnSc2
nebo	nebo	k8xC
William	William	k1gInSc4
Blake	Blak	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Dante	Dante	k1gMnSc1
Alighieri	Alighier	k1gFnSc2
<g/>
,	,	kIx,
Peklo	peklo	k1gNnSc1
<g/>
:	:	kIx,
barvitý	barvitý	k2eAgInSc1d1
popis	popis	k1gInSc1
obludy	obluda	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
Vergilius	Vergilius	k1gMnSc1
uklidní	uklidnit	k5eAaPmIp3nS
hozenou	hozený	k2eAgFnSc7d1
hrstí	hrst	k1gFnSc7
hlíny	hlína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Kerberos	Kerberos	k1gMnSc1
byl	být	k5eAaImAgMnS
předlohou	předloha	k1gFnSc7
pro	pro	k7c4
trojhlavého	trojhlavý	k2eAgMnSc4d1
psa	pes	k1gMnSc4
Chloupka	Chloupek	k1gMnSc2
J.	J.	kA
K.	K.	kA
Rowlingové	Rowlingový	k2eAgNnSc1d1
v	v	k7c4
Harry	Harr	k1gMnPc4
Potterovi	Potterův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Kerberos	Kerberos	k1gMnSc1
je	být	k5eAaImIp3nS
monstrum	monstrum	k1gNnSc4
ze	z	k7c2
seriálu	seriál	k1gInSc2
Lost	Lost	k1gMnSc1
(	(	kIx(
<g/>
Cerberus	Cerberus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Hérakles	Hérakles	k1gMnSc1
napájí	napájet	k5eAaImIp3nS
dvouhlavého	dvouhlavý	k2eAgMnSc4d1
Kerbera	Kerber	k1gMnSc4
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc4
<g/>
,	,	kIx,
530-520	530-520	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
Louvre	Louvre	k1gInSc1
</s>
<s>
Hans	Hans	k1gMnSc1
Sebald	Sebald	k1gMnSc1
Beham	Beham	k1gInSc1
<g/>
:	:	kIx,
Hérakles	Hérakles	k1gMnSc1
krotí	krotit	k5eAaImIp3nS
Kerbera	Kerber	k1gMnSc2
<g/>
,	,	kIx,
Norimberk	Norimberk	k1gInSc1
<g/>
,	,	kIx,
1545	#num#	k4
</s>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS
Arcimboldo	Arcimboldo	k1gNnSc1
<g/>
:	:	kIx,
Skica	skica	k1gFnSc1
pro	pro	k7c4
karnevalovou	karnevalový	k2eAgFnSc4d1
masku	maska	k1gFnSc4
kombinuje	kombinovat	k5eAaImIp3nS
Kerbera	Kerber	k1gMnSc4
s	s	k7c7
lernskou	lernský	k2eAgFnSc7d1
hydrou	hydra	k1gFnSc7
</s>
<s>
Hérakles	Hérakles	k1gMnSc1
se	s	k7c7
zkroceným	zkrocený	k2eAgMnSc7d1
Kerberem	Kerber	k1gMnSc7
na	na	k7c6
barokní	barokní	k2eAgFnSc6d1
kašně	kašna	k1gFnSc6
Parnas	Parnas	k1gInSc1
<g/>
,	,	kIx,
Zelný	zelný	k2eAgInSc1d1
trh	trh	k1gInSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
</s>
<s>
Francesco	Francesco	k6eAd1
Zurbarán	Zurbarán	k2eAgInSc1d1
<g/>
:	:	kIx,
Héraklés	Héraklés	k1gInSc1
táhne	táhnout	k5eAaImIp3nS
Kerbera	Kerber	k1gMnSc4
na	na	k7c6
řemeni	řemen	k1gInSc6
a	a	k8xC
ohání	ohánět	k5eAaImIp3nS
se	se	k3xPyFc4
kyjem	kyj	k1gInSc7
<g/>
;	;	kIx,
Madrid	Madrid	k1gInSc1
1634	#num#	k4
</s>
<s>
Jiné	jiný	k2eAgInPc1d1
významy	význam	k1gInPc1
</s>
<s>
V	v	k7c6
přeneseném	přenesený	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
nebo	nebo	k8xC
ironicky	ironicky	k6eAd1
se	se	k3xPyFc4
tak	tak	k9
označuje	označovat	k5eAaImIp3nS
<g/>
:	:	kIx,
přísný	přísný	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
hlídač	hlídač	k1gMnSc1
<g/>
,	,	kIx,
domovník	domovník	k1gMnSc1
<g/>
,	,	kIx,
fotbalový	fotbalový	k2eAgMnSc1d1
brankář	brankář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1
přírodovědec	přírodovědec	k1gMnSc1
Georges	Georges	k1gMnSc1
Cuvier	Cuvier	k1gMnSc1
roku	rok	k1gInSc2
1829	#num#	k4
pojmenoval	pojmenovat	k5eAaPmAgMnS
Cerberus	Cerberus	k1gMnSc1
nově	nově	k6eAd1
objevený	objevený	k2eAgInSc4d1
rod	rod	k1gInSc4
asijského	asijský	k2eAgMnSc2d1
hada	had	k1gMnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Kerbera	Kerber	k1gMnSc2
je	být	k5eAaImIp3nS
pojmenována	pojmenován	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Cerberus	Cerberus	k1gMnSc1
v	v	k7c6
herní	herní	k2eAgFnSc6d1
sérii	série	k1gFnSc6
Mass	Massa	k1gFnPc2
Effect	Effecta	k1gFnPc2
</s>
<s>
Podle	podle	k7c2
Kerbera	Kerber	k1gMnSc2
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
měsíc	měsíc	k1gInSc1
trpasličí	trpasličí	k2eAgFnSc2d1
planety	planeta	k1gFnSc2
Pluto	Pluto	k1gMnSc1
Kerberos	Kerberos	k1gMnSc1
a	a	k8xC
planetka	planetka	k1gFnSc1
1865	#num#	k4
Cerberus	Cerberus	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Beolens	Beolens	k1gInSc1
<g/>
,	,	kIx,
Bo	Bo	k?
<g/>
;	;	kIx,
Watkins	Watkins	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
;	;	kIx,
Grayson	Grayson	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
The	The	k1gMnSc1
Eponym	eponym	k1gMnSc1
Dictionary	Dictionara	k1gFnSc2
of	of	k?
Reptiles	Reptiles	k1gInSc1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
"	"	kIx"
<g/>
Cerberus	Cerberus	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Johns	Johns	k1gInSc1
Hopkins	Hopkins	k1gInSc4
University	universita	k1gFnSc2
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Baltimore	Baltimore	k1gInSc1
2011	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
50	#num#	k4
<g/>
:	:	kIx,
xiii	xiii	k1gNnSc6
+	+	kIx~
296	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-1-4214-0135-5	978-1-4214-0135-5	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Gerhard	Gerhard	k1gMnSc1
Löwe	Löw	k1gInSc2
<g/>
,	,	kIx,
Heindrich	Heindrich	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Stoll	Stoll	k1gMnSc1
<g/>
,	,	kIx,
ABC	ABC	kA
antiky	antika	k1gFnSc2
</s>
<s>
Publius	Publius	k1gMnSc1
Ovidius	Ovidius	k1gMnSc1
Naso	Naso	k1gMnSc1
<g/>
,	,	kIx,
Proměny	proměna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovna	knihovna	k1gFnSc1
klasiků	klasik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odeon	odeon	k1gInSc1
1959	#num#	k4
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
IX	IX	kA
<g/>
,	,	kIx,
s.	s.	k?
200	#num#	k4
<g/>
:	:	kIx,
<g/>
"	"	kIx"
<g/>
Ó	ó	k0
Kerbere	Kerber	k1gMnSc5
s	s	k7c7
trojitou	trojitý	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Mertlík	Mertlík	k1gInSc1
<g/>
,	,	kIx,
Starověké	starověký	k2eAgFnPc1d1
báje	báj	k1gFnPc1
a	a	k8xC
pověsti	pověst	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	svoboda	k1gFnSc1
Praha	Praha	k1gFnSc1
1972	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
146	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kerberos	Kerberos	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Portál	portál	k1gInSc1
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
:	:	kIx,
https://www.theoi.com/Ther/KuonKerberos.html	https://www.theoi.com/Ther/KuonKerberos.html	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Řecká	řecký	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
Prvotní	prvotní	k2eAgFnSc1d1
tvořitelská	tvořitelský	k2eAgFnSc1d1
božstva	božstvo	k1gNnPc4
</s>
<s>
Achlys	Achlysa	k1gFnPc2
•	•	k?
Aión	Aióna	k1gFnPc2
/	/	kIx~
Chronos	Chronosa	k1gFnPc2
•	•	k?
Eurynomé	Eurynomý	k2eAgFnSc2d1
•	•	k?
Ofión	Ofión	k1gMnSc1
•	•	k?
Chaos	chaos	k1gInSc1
•	•	k?
Erebos	Erebos	k1gInSc1
+	+	kIx~
Nyx	Nyx	k1gFnSc1
>	>	kIx)
Aithér	Aithér	k1gInSc1
•	•	k?
Hémerá	Hémerý	k2eAgFnSc1d1
•	•	k?
Gaia	Gaia	k1gFnSc1
•	•	k?
Tartaros	Tartaros	k1gInSc1
•	•	k?
Erós	erós	k1gInSc1
/	/	kIx~
Fanes	Fanes	k1gInSc1
•	•	k?
Pýthón	Pýthón	k1gInSc1
•	•	k?
Úranos	Úranos	k1gInSc1
•	•	k?
Pontos	Pontos	k1gInSc1
•	•	k?
Týfón	Týfón	k1gInSc1
Pontos	Pontos	k1gInSc1
+	+	kIx~
Gaia	Gaia	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Néreus	Néreus	k1gInSc1
•	•	k?
Thaumás	Thaumás	k1gInSc1
•	•	k?
Forkýs	Forkýs	k1gInSc1
•	•	k?
Kétó	Kétó	k1gMnSc2
•	•	k?
Eurybia	Eurybius	k1gMnSc2
•	•	k?
Harpyje	Harpyje	k1gFnSc2
•	•	k?
Chrýsáór	Chrýsáór	k1gMnSc1
•	•	k?
Néreovny	Néreovna	k1gFnSc2
Forkidy	Forkida	k1gFnSc2
</s>
<s>
Echidna	Echidna	k1gFnSc1
(	(	kIx(
<g/>
Kerberos	Kerberos	k1gMnSc1
<g/>
,	,	kIx,
Orthos	Orthos	k1gMnSc1
<g/>
,	,	kIx,
Hydra	hydra	k1gFnSc1
<g/>
,	,	kIx,
Chiméra	chiméra	k1gFnSc1
<g/>
,	,	kIx,
Sfinx	sfinx	k1gInSc1
<g/>
,	,	kIx,
Nemejský	Nemejský	k2eAgInSc1d1
lev	lev	k1gInSc1
<g/>
,	,	kIx,
Skylla	Skylla	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Ládón	Ládón	k1gInSc1
•	•	k?
Gorgony	Gorgo	k1gFnSc2
•	•	k?
Graie	Graie	k1gFnSc2
•	•	k?
Sirény	Siréna	k1gFnSc2
•	•	k?
Hesperidky	Hesperidka	k1gFnSc2
</s>
<s>
Úranos	Úranos	k1gMnSc1
+	+	kIx~
Gaia	Gaia	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Titáni	Titán	k1gMnPc1
<g/>
12	#num#	k4
</s>
<s>
Okeanos	Okeanos	k1gMnSc1
•	•	k?
Koios	Koios	k1gMnSc1
•	•	k?
Kríos	Kríos	k1gMnSc1
•	•	k?
Hyperión	Hyperión	k1gMnSc1
•	•	k?
Iapetos	Iapetos	k1gMnSc1
•	•	k?
Mnémosyné	Mnémosyná	k1gFnSc2
•	•	k?
Foibé	Foibý	k2eAgNnSc1d1
•	•	k?
Rheia	Rheius	k1gMnSc2
•	•	k?
Téthys	Téthys	k1gInSc1
•	•	k?
Theia	Theia	k1gFnSc1
•	•	k?
Themis	Themis	k1gFnSc1
•	•	k?
Kronos	Kronos	k1gMnSc1
•	•	k?
Ókeanovny	Ókeanovna	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnPc1
<g/>
,	,	kIx,
<g/>
také	také	k9
Olympané	Olympan	k1gMnPc1
</s>
<s>
Seléné	Seléné	k1gNnSc1
<g/>
,	,	kIx,
Éós	Éós	k1gMnSc1
<g/>
,	,	kIx,
Hélios	Hélios	k1gMnSc1
•	•	k?
Astraios	Astraios	k1gMnSc1
•	•	k?
Aithra	Aithra	k1gFnSc1
•	•	k?
Asteria	Asterium	k1gNnSc2
•	•	k?
Dióna	Dióno	k1gNnSc2
•	•	k?
Klymené	Klymený	k2eAgFnSc2d1
•	•	k?
Métis	Métis	k1gFnSc2
•	•	k?
Pallás	Pallás	k1gInSc1
•	•	k?
Persés	Persés	k1gInSc1
•	•	k?
Kallirhoé	Kallirhoá	k1gFnSc2
•	•	k?
Peithó	Peithó	k1gMnSc1
•	•	k?
Létó	Létó	k1gMnSc1
•	•	k?
Asteria	Asterium	k1gNnSc2
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnPc1
<g/>
,	,	kIx,
Iapetovi	Iapetův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
</s>
<s>
Prométheus	Prométheus	k1gMnSc1
•	•	k?
Epimétheus	Epimétheus	k1gMnSc1
•	•	k?
Atlás	Atlás	k1gInSc1
•	•	k?
Menoitios	Menoitios	k1gInSc1
•	•	k?
Plejády	Plejáda	k1gFnSc2
•	•	k?
Hyády	Hyáda	k1gFnSc2
</s>
<s>
Kyklópové	Kyklópové	k2eAgFnSc1d1
</s>
<s>
Brontés	Brontés	k1gInSc1
(	(	kIx(
<g/>
hrom	hrom	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Steropés	Steropés	k1gInSc1
(	(	kIx(
<g/>
blesk	blesk	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Argés	Argés	k1gInSc1
(	(	kIx(
<g/>
jas	jas	k1gInSc1
<g/>
)	)	kIx)
Hekatoncheirové	Hekatoncheirové	k2eAgFnSc1d1
</s>
<s>
Briareós	Briareós	k1gInSc1
•	•	k?
Kottos	Kottos	k1gInSc1
•	•	k?
Gyés	Gyés	k1gInSc1
při	při	k7c6
kastraci	kastrace	k1gFnSc6
</s>
<s>
Afrodita	Afrodita	k1gFnSc1
•	•	k?
Erínye	Eríny	k1gFnSc2
<g/>
,	,	kIx,
tři	tři	k4xCgFnPc1
Fúrie	Fúrie	k1gFnPc1
(	(	kIx(
<g/>
Alléktó	Alléktó	k1gFnPc1
•	•	k?
Megaira	Megaira	k1gFnSc1
•	•	k?
Tísifoné	Tísifoné	k1gNnSc1
<g/>
)	)	kIx)
Giganti	gigant	k1gMnPc1
<g/>
,	,	kIx,
z	z	k7c2
krve	krev	k1gFnSc2
</s>
<s>
Alkyoneus	Alkyoneus	k1gMnSc1
•	•	k?
Athos	Athos	k1gMnSc1
•	•	k?
Klytios	Klytios	k1gMnSc1
•	•	k?
Enkelados	Enkelados	k1gMnSc1
•	•	k?
Echión	Echión	k1gMnSc1
>	>	kIx)
pro	pro	k7c4
pomstu	pomsta	k1gFnSc4
<g/>
:	:	kIx,
gigantomachie	gigantomachie	k1gFnSc1
</s>
<s>
Kronos	Kronos	k1gMnSc1
+	+	kIx~
Rheia	Rheia	k1gFnSc1
<g/>
:	:	kIx,
<g/>
6	#num#	k4
olympských	olympský	k2eAgFnPc2d1
bohůa	bohůa	k6eAd1
další	další	k2eAgMnPc1d1
Olympané	Olympan	k1gMnPc1
</s>
<s>
Hestiá	Hestiá	k1gFnSc1
•	•	k?
Démétér	Démétér	k1gFnSc1
•	•	k?
Héra	Héra	k1gFnSc1
•	•	k?
Hádés	Hádés	k1gInSc1
•	•	k?
Poseidón	Poseidón	k1gInSc1
•	•	k?
Zeus	Zeus	k1gInSc1
bozi	bůh	k1gMnPc1
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Hermés	Hermés	k6eAd1
•	•	k?
Apollón	Apollón	k1gMnSc1
<g/>
+	+	kIx~
<g/>
Artemis	Artemis	k1gFnSc1
•	•	k?
Athéna	Athéna	k1gFnSc1
•	•	k?
Héfaistos	Héfaistos	k1gInSc1
•	•	k?
Arés	Arés	k1gInSc1
•	•	k?
Hébé	Hébé	k1gFnSc2
•	•	k?
Eileithýia	Eileithýia	k1gFnSc1
•	•	k?
Pandia	Pandium	k1gNnSc2
<g/>
,	,	kIx,
Ersa	Ers	k2eAgFnSc1d1
•	•	k?
Persefona	Persefona	k1gFnSc1
•	•	k?
Acherón	Acherón	k1gInSc1
•	•	k?
Moiry	Moira	k1gFnSc2
•	•	k?
Hóry	Hóra	k1gFnSc2
Múzy	Múza	k1gFnSc2
</s>
<s>
Kalliopé	Kalliopé	k1gFnSc7
•	•	k?
Euterpé	Euterpý	k2eAgFnSc2d1
•	•	k?
Erató	Erató	k1gFnSc2
•	•	k?
Thaleia	Thaleia	k1gFnSc1
•	•	k?
Melpomené	Melpomený	k2eAgNnSc4d1
•	•	k?
Terpsichoré	Terpsichorý	k2eAgFnSc2d1
•	•	k?
Kleió	Kleió	k1gMnSc1
•	•	k?
Úrania	Úranium	k1gNnSc2
•	•	k?
Polyhymnia	Polyhymnia	k1gFnSc1
</s>
<s>
bozi	bůh	k1gMnPc1
3	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Anteros	Anterosa	k1gFnPc2
•	•	k?
Erós	Erós	k1gMnSc1
•	•	k?
Harmonia	harmonium	k1gNnSc2
•	•	k?
Himeros	Himerosa	k1gFnPc2
•	•	k?
Eunomia	Eunomius	k1gMnSc2
•	•	k?
Hermaphroditus	Hermaphroditus	k1gInSc1
•	•	k?
Peitho	Peit	k1gMnSc2
•	•	k?
Rhodos	Rhodos	k1gInSc1
•	•	k?
Tyché	Tychý	k2eAgFnPc1d1
polobozi	polobůh	k1gMnPc1
na	na	k7c6
Olympu	Olymp	k1gInSc6
</s>
<s>
Dionýsos	Dionýsos	k1gMnSc1
•	•	k?
Héraklés	Héraklés	k1gInSc1
</s>
<s>
rasy	rasa	k1gFnPc1
a	a	k8xC
bytosti	bytost	k1gFnPc1
</s>
<s>
Nymfy	nymfa	k1gFnPc1
•	•	k?
Pan	Pan	k1gMnSc1
•	•	k?
Satyr	satyr	k1gMnSc1
•	•	k?
Silénos	Silénos	k1gMnSc1
•	•	k?
Argos	Argos	k1gMnSc1
•	•	k?
Kentaur	kentaur	k1gMnSc1
•	•	k?
Mínotaurus	Mínotaurus	k1gMnSc1
•	•	k?
Charybda	Charybda	k1gFnSc1
•	•	k?
Fénix	fénix	k1gMnSc1
polobozi	polobůh	k1gMnPc1
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
:	:	kIx,
hrdinové	hrdina	k1gMnPc1
a	a	k8xC
oběti	oběť	k1gFnPc1
</s>
<s>
Héraklés	Héraklés	k6eAd1
•	•	k?
Théseus	Théseus	k1gMnSc1
•	•	k?
Kallistó	Kallistó	k1gMnSc1
•	•	k?
Kirké	Kirká	k1gFnSc2
•	•	k?
Antaios	Antaios	k1gMnSc1
•	•	k?
Glaukos	Glaukos	k1gMnSc1
•	•	k?
Iásón	Iásón	k1gMnSc1
•	•	k?
Perseus	Perseus	k1gMnSc1
události	událost	k1gFnSc2
a	a	k8xC
příběhy	příběh	k1gInPc4
</s>
<s>
Amazonomachie	Amazonomachie	k1gFnSc1
•	•	k?
Kalydónský	Kalydónský	k2eAgInSc4d1
lov	lov	k1gInSc4
•	•	k?
Trójská	trójský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Odysseia	Odysseia	k1gFnSc1
•	•	k?
Sedm	sedm	k4xCc1
proti	proti	k7c3
Thébám	Théby	k1gFnPc3
</s>
