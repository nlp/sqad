<s>
Obec	obec	k1gFnSc1	obec
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Pardubický	pardubický	k2eAgInSc1d1	pardubický
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Přelouče	Přelouč	k1gFnSc2	Přelouč
a	a	k8xC	a
22	[number]	k4	22
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
646	[number]	k4	646
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
především	především	k9	především
díky	díky	k7c3	díky
místnímu	místní	k2eAgInSc3d1	místní
Národnímu	národní	k2eAgInSc3d1	národní
hřebčínu	hřebčín	k1gInSc3	hřebčín
<g/>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
již	již	k6eAd1	již
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Starokladrubský	Starokladrubský	k2eAgMnSc1d1	Starokladrubský
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
Bílé	bílý	k2eAgFnSc2d1	bílá
Vchynice	Vchynice	k1gFnSc2	Vchynice
Kolesa	koleso	k1gNnSc2	koleso
Komárov	Komárov	k1gInSc1	Komárov
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1350	[number]	k4	1350
<g/>
,	,	kIx,	,
nepodložená	podložený	k2eNgFnSc1d1	nepodložená
z	z	k7c2	z
konce	konec	k1gInSc2	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
náležely	náležet	k5eAaImAgInP	náležet
Kladruby	Kladruby	k1gInPc1	Kladruby
menším	malý	k2eAgMnPc3d2	menší
vlastníkům	vlastník	k1gMnPc3	vlastník
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
je	být	k5eAaImIp3nS	být
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
Vilém	Vilém	k1gMnSc1	Vilém
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1560	[number]	k4	1560
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
prodal	prodat	k5eAaPmAgMnS	prodat
královské	královský	k2eAgFnSc3d1	královská
komoře	komora	k1gFnSc3	komora
pardubické	pardubický	k2eAgNnSc1d1	pardubické
panství	panství	k1gNnSc1	panství
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgMnSc3	jenž
náležel	náležet	k5eAaImAgInS	náležet
kladrubský	kladrubský	k2eAgInSc1d1	kladrubský
dvůr	dvůr	k1gInSc1	dvůr
se	s	k7c7	s
zámečkem	zámeček	k1gInSc7	zámeček
a	a	k8xC	a
oborou	obora	k1gFnSc7	obora
s	s	k7c7	s
chovem	chov	k1gInSc7	chov
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
vzešlé	vzešlý	k2eAgNnSc1d1	vzešlé
z	z	k7c2	z
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vede	vést	k5eAaImIp3nS	vést
první	první	k4xOgNnSc4	první
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
Lenka	Lenka	k1gFnSc1	Lenka
Gotthardová	Gotthardový	k2eAgFnSc1d1	Gotthardová
<g/>
.	.	kIx.	.
</s>
<s>
Starostkou	starostka	k1gFnSc7	starostka
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
ČSNS	ČSNS	kA	ČSNS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
57,24	[number]	k4	57,24
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
Lenka	Lenka	k1gFnSc1	Lenka
Gottgardová	Gottgardový	k2eAgFnSc1d1	Gottgardový
získala	získat	k5eAaPmAgFnS	získat
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
kandidátů	kandidát	k1gMnPc2	kandidát
v	v	k7c6	v
Kladrubech	Kladruby	k1gInPc6	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
největší	veliký	k2eAgFnSc4d3	veliký
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
237	[number]	k4	237
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
při	při	k7c6	při
volební	volební	k2eAgFnSc6d1	volební
účasti	účast	k1gFnSc6	účast
329	[number]	k4	329
voličů	volič	k1gMnPc2	volič
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
72	[number]	k4	72
<g/>
%	%	kIx~	%
důvěře	důvěra	k1gFnSc6	důvěra
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
Anděla	Anděl	k1gMnSc2	Anděl
Strážce	strážce	k1gMnSc2	strážce
-	-	kIx~	-
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
ke	k	k7c3	k
Kolesům	koleso	k1gNnPc3	koleso
Areál	areál	k1gInSc1	areál
hřebčína	hřebčín	k1gInSc2	hřebčín
(	(	kIx(	(
<g/>
zámek	zámek	k1gInSc1	zámek
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Leopolda	Leopold	k1gMnSc2	Leopold
<g/>
,	,	kIx,	,
hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Kříže	Kříž	k1gMnSc2	Kříž
<g/>
,	,	kIx,	,
stáje	stáj	k1gFnSc2	stáj
<g/>
,	,	kIx,	,
kmenové	kmenový	k2eAgNnSc1d1	kmenové
stádo	stádo	k1gNnSc1	stádo
starokladrubského	starokladrubský	k2eAgInSc2d1	starokladrubský
bělouše	bělouš	k1gInSc2	bělouš
<g/>
)	)	kIx)	)
-	-	kIx~	-
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
Myslivna	myslivna	k1gFnSc1	myslivna
Galerie	galerie	k1gFnSc1	galerie
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Hřebčín	hřebčín	k1gInSc1	hřebčín
Kladruby	Kladruby	k1gInPc4	Kladruby
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
