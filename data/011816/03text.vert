<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Gabriel	Gabriel	k1gMnSc1	Gabriel
Verne	Vern	k1gInSc5	Vern
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
:	:	kIx,	:
žil	žít	k5eAaImAgMnS	žít
vern	vern	k1gMnSc1	vern
<g/>
,	,	kIx,	,
v	v	k7c6	v
IPA	IPA	kA	IPA
ʒ	ʒ	k?	ʒ
vɛ	vɛ	k?	vɛ
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1828	[number]	k4	1828
Nantes	Nantes	k1gInSc1	Nantes
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1905	[number]	k4	1905
Amiens	Amiens	k1gInSc1	Amiens
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
dobrodružné	dobrodružný	k2eAgFnSc2d1	dobrodružná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
žánru	žánr	k1gInSc2	žánr
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
oblíbeny	oblíben	k2eAgInPc1d1	oblíben
hlavně	hlavně	k9	hlavně
mezi	mezi	k7c7	mezi
mládeží	mládež	k1gFnSc7	mládež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
kráter	kráter	k1gInSc1	kráter
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1828	[number]	k4	1828
v	v	k7c6	v
domě	dům	k1gInSc6	dům
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Olivier-de-Clisson	Oliviere-Clissona	k1gFnPc2	Olivier-de-Clissona
na	na	k7c6	na
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
Île	Île	k1gFnSc2	Île
Feydeau	Feydeaus	k1gInSc2	Feydeaus
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nantes	Nantesa	k1gFnPc2	Nantesa
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
pařížského	pařížský	k2eAgMnSc2d1	pařížský
právníka	právník	k1gMnSc2	právník
Pierra	Pierra	k1gFnSc1	Pierra
Vernea	Vernea	k1gFnSc1	Vernea
a	a	k8xC	a
Sophie	Sophie	k1gFnSc1	Sophie
Allote	Allot	k1gInSc5	Allot
de	de	k?	de
la	la	k1gNnSc3	la
Fuÿ	Fuÿ	k1gFnSc2	Fuÿ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
nantské	nantský	k2eAgFnSc2d1	nantský
rodiny	rodina	k1gFnSc2	rodina
námořníků	námořník	k1gMnPc2	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Jules	Jules	k1gMnSc1	Jules
měl	mít	k5eAaImAgMnS	mít
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Paula	Paul	k1gMnSc2	Paul
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
<g/>
:	:	kIx,	:
Annu	Anna	k1gFnSc4	Anna
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mathildu	Mathilda	k1gFnSc4	Mathilda
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
a	a	k8xC	a
Marii	Maria	k1gFnSc3	Maria
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
penzionátu	penzionát	k1gInSc2	penzionát
při	při	k7c6	při
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedla	vést	k5eAaImAgFnS	vést
paní	paní	k1gFnSc1	paní
Sambinová	Sambinová	k1gFnSc1	Sambinová
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
Saint-Stanislas	Saint-Stanislas	k1gInSc1	Saint-Stanislas
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vedena	vést	k5eAaImNgFnS	vést
v	v	k7c6	v
přísném	přísný	k2eAgMnSc6d1	přísný
katolickém	katolický	k2eAgMnSc6d1	katolický
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
nadaným	nadaný	k2eAgMnSc7d1	nadaný
žákem	žák	k1gMnSc7	žák
–	–	k?	–
obdržel	obdržet	k5eAaPmAgMnS	obdržet
několik	několik	k4yIc4	několik
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
např.	např.	kA	např.
za	za	k7c4	za
zeměpis	zeměpis	k1gInSc4	zeměpis
<g/>
,	,	kIx,	,
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
...	...	k?	...
Další	další	k2eAgInPc4d1	další
roky	rok	k1gInPc4	rok
školy	škola	k1gFnSc2	škola
strávil	strávit	k5eAaPmAgInS	strávit
v	v	k7c6	v
semináři	seminář	k1gInSc6	seminář
Saint-Donatien	Saint-Donatina	k1gFnPc2	Saint-Donatina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nedokončeném	dokončený	k2eNgInSc6d1	nedokončený
románu	román	k1gInSc6	román
Kněz	kněz	k1gMnSc1	kněz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
(	(	kIx(	(
<g/>
Un	Un	k1gFnSc1	Un
prê	prê	k?	prê
en	en	k?	en
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
–	–	k?	–
dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
–	–	k?	–
najmout	najmout	k5eAaPmF	najmout
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
jako	jako	k8xS	jako
plavčík	plavčík	k1gMnSc1	plavčík
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
to	ten	k3xDgNnSc4	ten
však	však	k9	však
zjistili	zjistit	k5eAaPmAgMnP	zjistit
a	a	k8xC	a
cestě	cesta	k1gFnSc6	cesta
zabránili	zabránit	k5eAaPmAgMnP	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
chtěl	chtít	k5eAaImAgMnS	chtít
splnit	splnit	k5eAaPmF	splnit
slib	slib	k1gInSc4	slib
a	a	k8xC	a
dovézt	dovézt	k5eAaPmF	dovézt
sestřenici	sestřenice	k1gFnSc4	sestřenice
Caroline	Carolin	k1gInSc5	Carolin
Tronsonové	Tronsonové	k2eAgInSc4d1	Tronsonové
korálový	korálový	k2eAgInSc4d1	korálový
náhrdelník	náhrdelník	k1gInSc4	náhrdelník
<g/>
.	.	kIx.	.
</s>
<s>
Otci	otec	k1gMnSc3	otec
musel	muset	k5eAaImAgMnS	muset
slíbit	slíbit	k5eAaPmF	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
bude	být	k5eAaImBp3nS	být
cestovat	cestovat	k5eAaImF	cestovat
jen	jen	k9	jen
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
snech	sen	k1gInPc6	sen
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jeho	jeho	k3xOp3gFnPc2	jeho
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
legenda	legenda	k1gFnSc1	legenda
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
úplně	úplně	k6eAd1	úplně
mimo	mimo	k7c4	mimo
realitu	realita	k1gFnSc4	realita
–	–	k?	–
údajně	údajně	k6eAd1	údajně
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
plachetnice	plachetnice	k1gFnSc2	plachetnice
<g/>
,	,	kIx,	,
důkladně	důkladně	k6eAd1	důkladně
ji	on	k3xPp3gFnSc4	on
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
si	se	k3xPyFc3	se
"	"	kIx"	"
<g/>
osahal	osahat	k5eAaPmAgMnS	osahat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1844	[number]	k4	1844
a	a	k8xC	a
1846	[number]	k4	1846
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Paulem	Paul	k1gMnSc7	Paul
Lycée	Lycée	k1gFnPc2	Lycée
Royal	Royal	k1gInSc1	Royal
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Clemenceau	Clemenceaus	k1gInSc3	Clemenceaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maturitu	maturita	k1gFnSc4	maturita
složil	složit	k5eAaPmAgMnS	složit
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1846	[number]	k4	1846
v	v	k7c4	v
Rennes	Rennes	k1gInSc4	Rennes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Studia	studio	k1gNnSc2	studio
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
odjel	odjet	k5eAaPmAgInS	odjet
Jules	Jules	k1gInSc1	Jules
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
otce	otec	k1gMnSc2	otec
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Rose	Ros	k1gMnSc2	Ros
Herminie	Herminie	k1gFnSc2	Herminie
Arnaud	Arnaud	k1gMnSc1	Arnaud
Grossetiè	Grossetiè	k1gMnSc1	Grossetiè
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
skládat	skládat	k5eAaImF	skládat
první	první	k4xOgFnPc1	první
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
ji	on	k3xPp3gFnSc4	on
opěvoval	opěvovat	k5eAaImAgInS	opěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Idylka	idylka	k1gFnSc1	idylka
měla	mít	k5eAaImAgFnS	mít
bohužel	bohužel	k9	bohužel
krátké	krátký	k2eAgNnSc4d1	krátké
trvání	trvání	k1gNnSc4	trvání
–	–	k?	–
rodiče	rodič	k1gMnPc4	rodič
Herminie	Herminie	k1gFnSc2	Herminie
těžce	těžce	k6eAd1	těžce
snášeli	snášet	k5eAaImAgMnP	snášet
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
mladý	mladý	k2eAgMnSc1d1	mladý
student	student	k1gMnSc1	student
bez	bez	k7c2	bez
jisté	jistý	k2eAgFnSc2d1	jistá
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Zaslíbili	zaslíbit	k5eAaPmAgMnP	zaslíbit
ji	on	k3xPp3gFnSc4	on
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yQgMnSc4	který
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1848	[number]	k4	1848
<g/>
.	.	kIx.	.
</s>
<s>
Jules	Jules	k1gMnSc1	Jules
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
vztek	vztek	k1gInSc4	vztek
vybíjel	vybíjet	k5eAaImAgMnS	vybíjet
psaním	psaní	k1gNnSc7	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
odrazil	odrazit	k5eAaPmAgInS	odrazit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
jeho	jeho	k3xOp3gFnPc2	jeho
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
literárních	literární	k2eAgFnPc2d1	literární
postav	postava	k1gFnPc2	postava
–	–	k?	–
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
provdány	provdán	k2eAgInPc4d1	provdán
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
projevoval	projevovat	k5eAaImAgMnS	projevovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
určitou	určitý	k2eAgFnSc4d1	určitá
zášť	zášť	k1gFnSc4	zášť
vůči	vůči	k7c3	vůči
rodnému	rodný	k2eAgInSc3d1	rodný
kraji	kraj	k1gInSc3	kraj
a	a	k8xC	a
tamním	tamní	k2eAgMnPc3d1	tamní
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vzal	vzít	k5eAaPmAgMnS	vzít
ještě	ještě	k9	ještě
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc1	román
Kněz	kněz	k1gMnSc1	kněz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgFnPc2	dva
tragédií	tragédie	k1gFnPc2	tragédie
a	a	k8xC	a
své	svůj	k3xOyFgFnPc4	svůj
básně	báseň	k1gFnPc4	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Nastěhoval	nastěhovat	k5eAaPmAgInS	nastěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
nájemního	nájemní	k2eAgInSc2d1	nájemní
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Ancienne	Ancienn	k1gInSc5	Ancienn
Comédie	Comédie	k1gFnSc2	Comédie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sdílel	sdílet	k5eAaImAgMnS	sdílet
s	s	k7c7	s
Édouardem	Édouard	k1gMnSc7	Édouard
Bonamym	Bonamym	k1gInSc4	Bonamym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
on	on	k3xPp3gMnSc1	on
student	student	k1gMnSc1	student
a	a	k8xC	a
pocházel	pocházet	k5eAaImAgMnS	pocházet
také	také	k9	také
z	z	k7c2	z
Nantes	Nantesa	k1gFnPc2	Nantesa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
přijel	přijet	k5eAaPmAgMnS	přijet
Verne	Vern	k1gInSc5	Vern
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vrcholící	vrcholící	k2eAgFnSc2d1	vrcholící
únorové	únorový	k2eAgFnSc2d1	únorová
revoluce	revoluce	k1gFnSc2	revoluce
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
stavěly	stavět	k5eAaImAgInP	stavět
barikády	barikáda	k1gFnPc4	barikáda
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
dojmy	dojem	k1gInPc4	dojem
popisoval	popisovat	k5eAaImAgMnS	popisovat
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Châteaubourg	Châteaubourg	k1gMnSc1	Châteaubourg
jej	on	k3xPp3gMnSc4	on
brzo	brzo	k6eAd1	brzo
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
literárních	literární	k2eAgInPc2d1	literární
salónů	salón	k1gInPc2	salón
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pak	pak	k6eAd1	pak
s	s	k7c7	s
gustem	gusto	k1gNnSc7	gusto
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Nadchnul	nadchnout	k5eAaPmAgMnS	nadchnout
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
pustil	pustit	k5eAaPmAgMnS	pustit
se	se	k3xPyFc4	se
do	do	k7c2	do
psaní	psaní	k1gNnSc2	psaní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ale	ale	k9	ale
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
nevydané	vydaný	k2eNgNnSc1d1	nevydané
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Vrhl	vrhnout	k5eAaImAgMnS	vrhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
četbu	četba	k1gFnSc4	četba
Victora	Victor	k1gMnSc4	Victor
Huga	Hugo	k1gMnSc4	Hugo
<g/>
,	,	kIx,	,
Alexandra	Alexandr	k1gMnSc4	Alexandr
Dumase	Dumasa	k1gFnSc6	Dumasa
<g/>
,	,	kIx,	,
Alfreda	Alfred	k1gMnSc2	Alfred
de	de	k?	de
Vigny	Vigna	k1gMnSc2	Vigna
<g/>
,	,	kIx,	,
Alfreda	Alfred	k1gMnSc2	Alfred
de	de	k?	de
Musset	Musset	k1gInSc1	Musset
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
dramatiků	dramatik	k1gMnPc2	dramatik
preferoval	preferovat	k5eAaImAgMnS	preferovat
zejména	zejména	k9	zejména
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
a	a	k8xC	a
Moliè	Moliè	k1gMnSc4	Moliè
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
ho	on	k3xPp3gMnSc4	on
nejvíce	nejvíce	k6eAd1	nejvíce
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
navýsost	navýsost	k6eAd1	navýsost
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Victora	Victor	k1gMnSc2	Victor
Huga	Hugo	k1gMnSc2	Hugo
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
nadšený	nadšený	k2eAgInSc1d1	nadšený
opakovanou	opakovaný	k2eAgFnSc7d1	opakovaná
četbou	četba	k1gFnSc7	četba
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jsem	být	k5eAaImIp1nS	být
mohl	moct	k5eAaImAgMnS	moct
recitovat	recitovat	k5eAaImF	recitovat
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
celé	celý	k2eAgFnSc2d1	celá
pasáže	pasáž	k1gFnSc2	pasáž
z	z	k7c2	z
Notre	Notr	k1gInSc5	Notr
Dame	Dame	k1gNnPc7	Dame
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc2	jeho
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mě	já	k3xPp1nSc4	já
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
nejvíce	hodně	k6eAd3	hodně
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
vlivem	vliv	k1gInSc7	vliv
<g/>
,	,	kIx,	,
asi	asi	k9	asi
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmnácti	sedmnáct	k4xCc2	sedmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
napsal	napsat	k5eAaPmAgMnS	napsat
nemálo	málo	k6eNd1	málo
tragédií	tragédie	k1gFnPc2	tragédie
a	a	k8xC	a
komedií	komedie	k1gFnPc2	komedie
<g/>
,	,	kIx,	,
nepočítaje	nepočítaje	k7c4	nepočítaje
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
romány	román	k1gInPc1	román
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
psal	psát	k5eAaImAgMnS	psát
rodičům	rodič	k1gMnPc3	rodič
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
finančních	finanční	k2eAgInPc6d1	finanční
problémech	problém	k1gInPc6	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1849	[number]	k4	1849
upoutala	upoutat	k5eAaPmAgFnS	upoutat
jeho	jeho	k3xOp3gFnSc1	jeho
pozornost	pozornost	k1gFnSc1	pozornost
cholera	cholera	k1gFnSc1	cholera
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
povolán	povolat	k5eAaPmNgMnS	povolat
do	do	k7c2	do
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nesl	nést	k5eAaImAgInS	nést
velmi	velmi	k6eAd1	velmi
nelibě	libě	k6eNd1	libě
a	a	k8xC	a
projevil	projevit	k5eAaPmAgInS	projevit
tak	tak	k9	tak
svůj	svůj	k3xOyFgInSc4	svůj
antimilitaristický	antimilitaristický	k2eAgInSc4d1	antimilitaristický
postoj	postoj	k1gInSc4	postoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gInSc4	on
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
neopustil	opustit	k5eNaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Verne	Vern	k1gInSc5	Vern
nějak	nějak	k6eAd1	nějak
uživil	uživit	k5eAaPmAgMnS	uživit
<g/>
,	,	kIx,	,
dával	dávat	k5eAaImAgMnS	dávat
hodiny	hodina	k1gFnPc4	hodina
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
nesl	nést	k5eAaImAgMnS	nést
nelibě	libě	k6eNd1	libě
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
)	)	kIx)	)
a	a	k8xC	a
vypomáhal	vypomáhat	k5eAaImAgMnS	vypomáhat
u	u	k7c2	u
přítele	přítel	k1gMnSc2	přítel
advokáta	advokát	k1gMnSc2	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
také	také	k9	také
datovalo	datovat	k5eAaImAgNnS	datovat
Verneovo	Verneův	k2eAgNnSc4d1	Verneovo
špatné	špatný	k2eAgNnSc4d1	špatné
zdraví	zdraví	k1gNnSc4	zdraví
–	–	k?	–
časté	častý	k2eAgNnSc1d1	časté
hladovění	hladovění	k1gNnSc1	hladovění
mu	on	k3xPp3gMnSc3	on
působilo	působit	k5eAaImAgNnS	působit
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
zažíváním	zažívání	k1gNnSc7	zažívání
a	a	k8xC	a
žaludkem	žaludek	k1gInSc7	žaludek
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
přičíst	přičíst	k5eAaPmF	přičíst
dědičné	dědičný	k2eAgInPc4d1	dědičný
předpoklady	předpoklad	k1gInPc4	předpoklad
k	k	k7c3	k
gastrickým	gastrický	k2eAgFnPc3d1	gastrická
obtížím	obtíž	k1gFnPc3	obtíž
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
patologickou	patologický	k2eAgFnSc4d1	patologická
bulimii	bulimie	k1gFnSc4	bulimie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
postižen	postihnout	k5eAaPmNgInS	postihnout
paralýzou	paralýza	k1gFnSc7	paralýza
obličejových	obličejový	k2eAgInPc2d1	obličejový
nervů	nerv	k1gInPc2	nerv
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
život	život	k1gInSc4	život
vrátila	vrátit	k5eAaPmAgFnS	vrátit
celkem	celkem	k6eAd1	celkem
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
zapříčiněná	zapříčiněný	k2eAgNnPc4d1	zapříčiněné
zánětem	zánět	k1gInSc7	zánět
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
své	svůj	k3xOyFgFnPc4	svůj
zdravotní	zdravotní	k2eAgFnPc4d1	zdravotní
obtíže	obtíž	k1gFnPc4	obtíž
složil	složit	k5eAaPmAgMnS	složit
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
právnické	právnický	k2eAgFnPc4d1	právnická
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tak	tak	k6eAd1	tak
začít	začít	k5eAaPmF	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
advokát	advokát	k1gMnSc1	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
začal	začít	k5eAaPmAgMnS	začít
organizovat	organizovat	k5eAaBmF	organizovat
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
večeře	večeře	k1gFnSc2	večeře
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
"	"	kIx"	"
<g/>
Jedenáct	jedenáct	k4xCc1	jedenáct
bez	bez	k7c2	bez
ženy	žena	k1gFnSc2	žena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgFnSc1d1	literární
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
Verne	Vern	k1gInSc5	Vern
o	o	k7c4	o
literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
většího	veliký	k2eAgInSc2d2	veliký
ohlasu	ohlas	k1gInSc2	ohlas
<g/>
)	)	kIx)	)
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
libreta	libreto	k1gNnPc1	libreto
a	a	k8xC	a
divadelní	divadelní	k2eAgFnPc1d1	divadelní
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Zlomená	zlomený	k2eAgNnPc1d1	zlomené
stébla	stéblo	k1gNnPc1	stéblo
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
návštěvám	návštěva	k1gFnPc3	návštěva
literárních	literární	k2eAgMnPc2d1	literární
salónů	salón	k1gInPc2	salón
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
kontaktů	kontakt	k1gInPc2	kontakt
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Dumasem	Dumas	k1gMnSc7	Dumas
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Představil	představit	k5eAaPmAgMnS	představit
jim	on	k3xPp3gMnPc3	on
rukopis	rukopis	k1gInSc4	rukopis
komedie	komedie	k1gFnSc1	komedie
Les	les	k1gInSc1	les
Pailles	Pailles	k1gMnSc1	Pailles
rompues	rompues	k1gMnSc1	rompues
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c4	na
prkna	prkno	k1gNnPc4	prkno
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
revue	revue	k1gFnSc2	revue
Musée	Musée	k1gNnSc2	Musée
des	des	k1gNnSc2	des
familles	familles	k1gMnSc1	familles
Pitre-Chevalierem	Pitre-Chevalier	k1gMnSc7	Pitre-Chevalier
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
vydal	vydat	k5eAaPmAgInS	vydat
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
je	on	k3xPp3gInPc4	on
podroboval	podrobovat	k5eAaImAgMnS	podrobovat
cenzuře	cenzura	k1gFnSc3	cenzura
či	či	k8xC	či
žádal	žádat	k5eAaImAgMnS	žádat
jejich	jejich	k3xOp3gNnSc4	jejich
přepisování	přepisování	k1gNnSc4	přepisování
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiná	k1gFnSc3	jiná
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
vydavatele	vydavatel	k1gMnSc2	vydavatel
Pierra-Julese	Pierra-Julese	k1gFnSc2	Pierra-Julese
Hetzela	Hetzela	k1gMnSc2	Hetzela
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
stačí	stačit	k5eAaBmIp3nS	stačit
porovnat	porovnat	k5eAaPmF	porovnat
vydání	vydání	k1gNnSc4	vydání
v	v	k7c6	v
revue	revue	k1gFnSc6	revue
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pak	pak	k6eAd1	pak
vyšla	vyjít	k5eAaPmAgFnS	vyjít
u	u	k7c2	u
Hetzela	Hetzela	k1gFnSc2	Hetzela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
také	také	k9	také
angažován	angažován	k2eAgMnSc1d1	angažován
jako	jako	k8xC	jako
sekretář	sekretář	k1gMnSc1	sekretář
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedostával	dostávat	k5eNaImAgInS	dostávat
plat	plat	k1gInSc1	plat
–	–	k?	–
odměnou	odměna	k1gFnSc7	odměna
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
uváděny	uvádět	k5eAaImNgFnP	uvádět
jeho	jeho	k3xOp3gFnPc1	jeho
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Michelem	Michel	k1gInSc7	Michel
Carré	Carrý	k2eAgNnSc1d1	Carré
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
1855	[number]	k4	1855
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
plodným	plodný	k2eAgMnSc7d1	plodný
dramatikem	dramatik	k1gMnSc7	dramatik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
nadchl	nadchnout	k5eAaPmAgMnS	nadchnout
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
geografii	geografie	k1gFnSc4	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
proslulým	proslulý	k2eAgMnSc7d1	proslulý
Jacquem	Jacqu	k1gMnSc7	Jacqu
Aragem	Arag	k1gMnSc7	Arag
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
objevování	objevování	k1gNnSc6	objevování
světa	svět	k1gInSc2	svět
i	i	k8xC	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
slepotu	slepota	k1gFnSc4	slepota
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jej	on	k3xPp3gNnSc4	on
také	také	k9	také
přivedl	přivést	k5eAaPmAgMnS	přivést
k	k	k7c3	k
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc2	on
novému	nový	k2eAgMnSc3d1	nový
literární	literární	k2eAgInPc4d1	literární
žánru	žánr	k1gInSc3	žánr
–	–	k?	–
cestopisu	cestopis	k1gInSc6	cestopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
nakladatelem	nakladatel	k1gMnSc7	nakladatel
Pierrem-Julesem	Pierrem-Jules	k1gMnSc7	Pierrem-Jules
Hetzelem	Hetzel	k1gMnSc7	Hetzel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vydal	vydat	k5eAaPmAgMnS	vydat
jeho	on	k3xPp3gInSc4	on
dobrodružný	dobrodružný	k2eAgInSc4d1	dobrodružný
román	román	k1gInSc4	román
Pět	pět	k4xCc1	pět
neděl	neděle	k1gFnPc2	neděle
v	v	k7c6	v
baloně	balon	k1gInSc6	balon
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
tak	tak	k6eAd1	tak
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
že	že	k8xS	že
nakladatel	nakladatel	k1gMnSc1	nakladatel
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Verneovi	Verneus	k1gMnSc3	Verneus
exkluzivní	exkluzivní	k2eAgFnSc4d1	exkluzivní
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Verne	Vern	k1gInSc5	Vern
zavázal	zavázat	k5eAaPmAgMnS	zavázat
napsat	napsat	k5eAaBmF	napsat
dva	dva	k4xCgInPc4	dva
svazky	svazek	k1gInPc4	svazek
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
počet	počet	k1gInSc4	počet
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
některé	některý	k3yIgInPc1	některý
romány	román	k1gInPc1	román
byly	být	k5eAaImAgInP	být
několikasvazkové	několikasvazkový	k2eAgInPc1d1	několikasvazkový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
smlouva	smlouva	k1gFnSc1	smlouva
Vernea	Vernea	k1gFnSc1	Vernea
finančně	finančně	k6eAd1	finančně
zajistila	zajistit	k5eAaPmAgFnS	zajistit
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
mu	on	k3xPp3gMnSc3	on
živit	živit	k5eAaImF	živit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
psaním	psaní	k1gNnSc7	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Pierre-Jules	Pierre-Jules	k1gMnSc1	Pierre-Jules
Hetzel	Hetzel	k1gMnSc1	Hetzel
také	také	k9	také
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
souhrnný	souhrnný	k2eAgInSc4d1	souhrnný
název	název	k1gInSc4	název
pro	pro	k7c4	pro
Verneovo	Verneův	k2eAgNnSc4d1	Verneovo
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Podivuhodné	podivuhodný	k2eAgFnPc1d1	podivuhodná
cesty	cesta	k1gFnPc1	cesta
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Voyages	Voyages	k1gMnSc1	Voyages
extraordinaires	extraordinaires	k1gMnSc1	extraordinaires
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
"	"	kIx"	"
<g/>
shrnout	shrnout	k5eAaPmF	shrnout
všechny	všechen	k3xTgFnPc4	všechen
znalosti	znalost	k1gFnPc4	znalost
zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
<g/>
,	,	kIx,	,
geologické	geologický	k2eAgFnPc4d1	geologická
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
<g/>
,	,	kIx,	,
astronomické	astronomický	k2eAgFnPc4d1	astronomická
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
shromáždila	shromáždit	k5eAaPmAgFnS	shromáždit
moderní	moderní	k2eAgFnSc1d1	moderní
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpracovat	zpracovat	k5eAaPmF	zpracovat
tak	tak	k6eAd1	tak
barvitým	barvitý	k2eAgInSc7d1	barvitý
a	a	k8xC	a
poutavým	poutavý	k2eAgInSc7d1	poutavý
způsobem	způsob	k1gInSc7	způsob
...	...	k?	...
dějiny	dějiny	k1gFnPc1	dějiny
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
Hetzel	Hetzel	k1gMnPc1	Hetzel
a	a	k8xC	a
Verne	Vern	k1gInSc5	Vern
druhý	druhý	k4xOgInSc1	druhý
kontrakt	kontrakt	k1gInSc1	kontrakt
na	na	k7c6	na
napsání	napsání	k1gNnSc6	napsání
dějin	dějiny	k1gFnPc2	dějiny
velkých	velký	k2eAgInPc2d1	velký
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
Verne	Vern	k1gInSc5	Vern
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Gabrielem	Gabriel	k1gMnSc7	Gabriel
Marcelem	Marcel	k1gMnSc7	Marcel
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
knihovníkem	knihovník	k1gMnSc7	knihovník
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Národní	národní	k2eAgFnSc6d1	národní
knihovně	knihovna	k1gFnSc6	knihovna
(	(	kIx(	(
<g/>
Bibliothè	Bibliothè	k1gFnSc1	Bibliothè
Nationale	Nationale	k1gMnSc1	Nationale
<g/>
)	)	kIx)	)
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
spolupráce	spolupráce	k1gFnSc2	spolupráce
bylo	být	k5eAaImAgNnS	být
především	především	k9	především
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Verne	Vern	k1gInSc5	Vern
uměl	umět	k5eAaImAgMnS	umět
mluvit	mluvit	k5eAaImF	mluvit
a	a	k8xC	a
číst	číst	k5eAaImF	číst
pouze	pouze	k6eAd1	pouze
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
cizích	cizí	k2eAgInPc6d1	cizí
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Marcel	Marcel	k1gMnSc1	Marcel
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ovládal	ovládat	k5eAaImAgInS	ovládat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželství	manželství	k1gNnSc1	manželství
<g/>
,	,	kIx,	,
obchodování	obchodování	k1gNnSc1	obchodování
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
a	a	k8xC	a
cesty	cesta	k1gFnSc2	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Verne	Vern	k1gInSc5	Vern
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
"	"	kIx"	"
<g/>
Jedenáct	jedenáct	k4xCc1	jedenáct
bez	bez	k7c2	bez
ženy	žena	k1gFnSc2	žena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
oženili	oženit	k5eAaPmAgMnP	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
i	i	k9	i
on	on	k3xPp3gMnSc1	on
začal	začít	k5eAaPmAgInS	začít
poohlížet	poohlížet	k5eAaImF	poohlížet
po	po	k7c6	po
vhodné	vhodný	k2eAgFnSc6d1	vhodná
partnerce	partnerka	k1gFnSc6	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
svateb	svatba	k1gFnPc2	svatba
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
letou	letý	k2eAgFnSc7d1	letá
vdovou	vdova	k1gFnSc7	vdova
Honorií	Honorie	k1gFnSc7	Honorie
Morelovou	Morelová	k1gFnSc7	Morelová
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
-	-	kIx~	-
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matkou	matka	k1gFnSc7	matka
dvou	dva	k4xCgInPc6	dva
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
pak	pak	k6eAd1	pak
spolu	spolu	k6eAd1	spolu
syna	syn	k1gMnSc4	syn
Michela	Michel	k1gMnSc4	Michel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
dopsal	dopsat	k5eAaPmAgMnS	dopsat
některá	některý	k3yIgNnPc1	některý
jeho	jeho	k3xOp3gMnPc3	jeho
nedokončená	dokončený	k2eNgNnPc4d1	nedokončené
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
psaní	psaní	k1gNnSc1	psaní
jej	on	k3xPp3gMnSc4	on
natrvalo	natrvalo	k6eAd1	natrvalo
neuživí	uživit	k5eNaPmIp3nS	uživit
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
Verne	Vern	k1gInSc5	Vern
investovat	investovat	k5eAaBmF	investovat
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
a	a	k8xC	a
obchodovat	obchodovat	k5eAaImF	obchodovat
s	s	k7c7	s
akciemi	akcie	k1gFnPc7	akcie
<g/>
.	.	kIx.	.
</s>
<s>
Půjčku	půjčka	k1gFnSc4	půjčka
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
mu	on	k3xPp3gMnSc3	on
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nebyl	být	k5eNaImAgMnS	být
zrovna	zrovna	k6eAd1	zrovna
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
<g/>
,	,	kIx,	,
zavíral	zavírat	k5eAaImAgInS	zavírat
se	se	k3xPyFc4	se
do	do	k7c2	do
pracovny	pracovna	k1gFnSc2	pracovna
a	a	k8xC	a
usilovně	usilovně	k6eAd1	usilovně
psal	psát	k5eAaImAgInS	psát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
Verne	Vern	k1gInSc5	Vern
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
unikl	uniknout	k5eAaPmAgMnS	uniknout
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
pominutí	pominutí	k1gNnSc6	pominutí
smyslů	smysl	k1gInPc2	smysl
vystřelil	vystřelit	k5eAaPmAgInS	vystřelit
z	z	k7c2	z
revolveru	revolver	k1gInSc2	revolver
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
Gaston	Gaston	k1gInSc1	Gaston
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
jeho	on	k3xPp3gMnSc2	on
bratra	bratr	k1gMnSc2	bratr
Paula	Paul	k1gMnSc2	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Verneovi	Verneus	k1gMnSc3	Verneus
se	se	k3xPyFc4	se
ale	ale	k9	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
srazit	srazit	k5eAaPmF	srazit
synovcovi	synovec	k1gMnSc3	synovec
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jenom	jenom	k6eAd1	jenom
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
cest	cesta	k1gFnPc2	cesta
zapracoval	zapracovat	k5eAaPmAgMnS	zapracovat
i	i	k9	i
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
Skotsko	Skotsko	k1gNnSc4	Skotsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
navštívil	navštívit	k5eAaPmAgMnS	navštívit
např.	např.	kA	např.
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
radním	radní	k1gMnSc7	radní
města	město	k1gNnSc2	město
Amiens	Amiens	k1gInSc1	Amiens
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
památka	památka	k1gFnSc1	památka
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
působení	působení	k1gNnSc6	působení
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c4	v
Amiens	Amiens	k1gInSc4	Amiens
technicky	technicky	k6eAd1	technicky
zajímavé	zajímavý	k2eAgNnSc4d1	zajímavé
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
nosná	nosný	k2eAgFnSc1d1	nosná
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
řešena	řešit	k5eAaImNgFnS	řešit
originálně	originálně	k6eAd1	originálně
použitými	použitý	k2eAgInPc7d1	použitý
komíny	komín	k1gInPc7	komín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otázky	otázka	k1gFnPc1	otázka
autorství	autorství	k1gNnSc2	autorství
Verneových	Verneův	k2eAgNnPc2d1	Verneovo
děl	dělo	k1gNnPc2	dělo
==	==	k?	==
</s>
</p>
<p>
<s>
Autorství	autorství	k1gNnSc1	autorství
některých	některý	k3yIgFnPc2	některý
knih	kniha	k1gFnPc2	kniha
Julese	Julese	k1gFnSc2	Julese
Vernea	Verneum	k1gNnSc2	Verneum
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
román	román	k1gInSc4	román
Trosečník	trosečník	k1gMnSc1	trosečník
z	z	k7c2	z
Cynthie	Cynthie	k1gFnSc2	Cynthie
napsal	napsat	k5eAaPmAgMnS	napsat
jiný	jiný	k2eAgMnSc1d1	jiný
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
André	André	k1gMnSc1	André
Laurie	Laurie	k1gFnSc1	Laurie
(	(	kIx(	(
<g/>
Verne	Vern	k1gInSc5	Vern
provedl	provést	k5eAaPmAgInS	provést
pouze	pouze	k6eAd1	pouze
kontrolní	kontrolní	k2eAgFnPc4d1	kontrolní
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
z	z	k7c2	z
komerčních	komerční	k2eAgInPc2d1	komerční
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
Verne	Vern	k1gInSc5	Vern
upravil	upravit	k5eAaPmAgMnS	upravit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
podstatně	podstatně	k6eAd1	podstatně
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
jiné	jiný	k2eAgInPc4d1	jiný
Laurieho	Laurieha	k1gFnSc5	Laurieha
rukopisy	rukopis	k1gInPc1	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
základem	základ	k1gInSc7	základ
dvou	dva	k4xCgInPc2	dva
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
vydaných	vydaný	k2eAgFnPc2d1	vydaná
rovněž	rovněž	k9	rovněž
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
<g/>
:	:	kIx,	:
Ocelové	ocelový	k2eAgNnSc1d1	ocelové
město	město	k1gNnSc1	město
a	a	k8xC	a
Hvězda	hvězda	k1gFnSc1	hvězda
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
tak	tak	k6eAd1	tak
romány	román	k1gInPc1	román
dokončené	dokončený	k2eAgFnSc2d1	dokončená
nebo	nebo	k8xC	nebo
přepracované	přepracovaný	k2eAgFnSc2d1	přepracovaná
jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
Michelem	Michel	k1gMnSc7	Michel
Vernem	Vern	k1gMnSc7	Vern
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Lodivod	lodivod	k1gMnSc1	lodivod
dunajský	dunajský	k2eAgMnSc1d1	dunajský
<g/>
,	,	kIx,	,
Honba	honba	k1gFnSc1	honba
za	za	k7c7	za
meteorem	meteor	k1gInSc7	meteor
nebo	nebo	k8xC	nebo
Podivuhodná	podivuhodný	k2eAgNnPc1d1	podivuhodné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
výpravy	výprava	k1gFnSc2	výprava
Barsacovy	Barsacův	k2eAgFnSc2d1	Barsacův
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
výzkumů	výzkum	k1gInPc2	výzkum
dosti	dosti	k6eAd1	dosti
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
původních	původní	k2eAgInPc2d1	původní
Verneových	Verneův	k2eAgInPc2d1	Verneův
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stálým	stálý	k2eAgMnPc3d1	stálý
Verneovým	Verneův	k2eAgMnPc3d1	Verneův
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
patřil	patřit	k5eAaImAgMnS	patřit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Paul	Paul	k1gMnSc1	Paul
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
dálkové	dálkový	k2eAgFnSc2d1	dálková
plavby	plavba	k1gFnSc2	plavba
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
jeho	jeho	k3xOp3gFnPc6	jeho
knihách	kniha	k1gFnPc6	kniha
týkajících	týkající	k2eAgFnPc6d1	týkající
se	se	k3xPyFc4	se
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
námořnictví	námořnictví	k1gNnSc1	námořnictví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pravidelného	pravidelný	k2eAgMnSc2d1	pravidelný
Verneova	Verneův	k2eAgMnSc2d1	Verneův
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
musíme	muset	k5eAaImIp1nP	muset
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
nakladatele	nakladatel	k1gMnSc4	nakladatel
Hetzela	Hetzela	k1gMnSc4	Hetzela
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
pravidelně	pravidelně	k6eAd1	pravidelně
prvním	první	k4xOgMnSc7	první
čtenářem	čtenář	k1gMnSc7	čtenář
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
korespondence	korespondence	k1gFnSc1	korespondence
a	a	k8xC	a
analýza	analýza	k1gFnSc1	analýza
původních	původní	k2eAgInPc2d1	původní
rukopisů	rukopis	k1gInPc2	rukopis
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hetzel	Hetzel	k1gMnSc1	Hetzel
nejen	nejen	k6eAd1	nejen
přispíval	přispívat	k5eAaImAgMnS	přispívat
mnoha	mnoho	k4c2	mnoho
nápady	nápad	k1gInPc4	nápad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prováděl	provádět	k5eAaImAgInS	provádět
také	také	k9	také
v	v	k7c6	v
rukopisech	rukopis	k1gInPc6	rukopis
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
života	život	k1gInSc2	život
a	a	k8xC	a
odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
společně	společně	k6eAd1	společně
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Herbertem	Herbert	k1gMnSc7	Herbert
Georgem	Georg	k1gMnSc7	Georg
Wellsem	Wells	k1gInSc7	Wells
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
žánru	žánr	k1gInSc2	žánr
vědeckofantastické	vědeckofantastický	k2eAgFnSc2d1	vědeckofantastická
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
science-fiction	scienceiction	k1gInSc1	science-fiction
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Verne	Vern	k1gInSc5	Vern
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
vědeckých	vědecký	k2eAgInPc2d1	vědecký
a	a	k8xC	a
technických	technický	k2eAgInPc2d1	technický
objevů	objev	k1gInPc2	objev
a	a	k8xC	a
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dálkové	dálkový	k2eAgInPc4d1	dálkový
lety	let	k1gInPc4	let
řiditelným	řiditelný	k2eAgInSc7d1	řiditelný
balónem	balón	k1gInSc7	balón
<g/>
,	,	kIx,	,
velkorážní	velkorážní	k2eAgNnSc4d1	velkorážní
dělo	dělo	k1gNnSc4	dělo
<g/>
,	,	kIx,	,
videofon	videofon	k1gInSc4	videofon
<g/>
,	,	kIx,	,
skafandr	skafandr	k1gInSc4	skafandr
<g/>
,	,	kIx,	,
kosmické	kosmický	k2eAgFnPc4d1	kosmická
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc4d1	elektrický
motor	motor	k1gInSc4	motor
<g/>
,	,	kIx,	,
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
vrtulník	vrtulník	k1gInSc4	vrtulník
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
zemřel	zemřít	k5eAaPmAgMnS	zemřít
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1905	[number]	k4	1905
v	v	k7c6	v
Amiensu	Amiens	k1gInSc6	Amiens
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Michel	Michel	k1gMnSc1	Michel
Verne	Vern	k1gInSc5	Vern
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
o	o	k7c4	o
vydání	vydání	k1gNnSc4	vydání
jeho	on	k3xPp3gInSc2	on
posledního	poslední	k2eAgInSc2d1	poslední
originálního	originální	k2eAgInSc2d1	originální
románu	román	k1gInSc2	román
Zatopená	zatopený	k2eAgFnSc1d1	zatopená
Sahara	Sahara	k1gFnSc1	Sahara
a	a	k8xC	a
poté	poté	k6eAd1	poté
knihou	kniha	k1gFnSc7	kniha
Maják	maják	k1gInSc1	maják
na	na	k7c6	na
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
zahájil	zahájit	k5eAaPmAgMnS	zahájit
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
vydávání	vydávání	k1gNnSc4	vydávání
dalších	další	k2eAgInPc2d1	další
svazků	svazek	k1gInPc2	svazek
Podivuhodných	podivuhodný	k2eAgFnPc2d1	podivuhodná
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dílech	díl	k1gInPc6	díl
však	však	k9	však
Michel	Michel	k1gMnSc1	Michel
prováděl	provádět	k5eAaImAgMnS	provádět
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
)	)	kIx)	)
velké	velký	k2eAgFnPc4d1	velká
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
originální	originální	k2eAgFnPc1d1	originální
verze	verze	k1gFnPc1	verze
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
někteří	některý	k3yIgMnPc1	některý
Verneovi	Verneův	k2eAgMnPc1d1	Verneův
životopisci	životopisec	k1gMnPc1	životopisec
uváděli	uvádět	k5eAaImAgMnP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Verne	Vern	k1gInSc5	Vern
je	být	k5eAaImIp3nS	být
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Oleskiewicz	Oleskiewicz	k1gInSc1	Oleskiewicz
(	(	kIx(	(
<g/>
verne	vernout	k5eAaPmIp3nS	vernout
je	on	k3xPp3gNnSc4	on
francouzsky	francouzsky	k6eAd1	francouzsky
olše	olše	k1gFnSc1	olše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
fámu	fáma	k1gFnSc4	fáma
o	o	k7c4	o
sobě	se	k3xPyFc3	se
patrně	patrně	k6eAd1	patrně
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
sám	sám	k3xTgMnSc1	sám
Verne	Vern	k1gInSc5	Vern
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
historkami	historka	k1gFnPc7	historka
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
často	často	k6eAd1	často
bavil	bavit	k5eAaImAgMnS	bavit
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podivuhodné	podivuhodný	k2eAgFnPc4d1	podivuhodná
cesty	cesta	k1gFnPc4	cesta
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Verneova	Verneův	k2eAgInSc2d1	Verneův
života	život	k1gInSc2	život
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
cyklus	cyklus	k1gInSc1	cyklus
Podivuhodné	podivuhodný	k2eAgFnSc2d1	podivuhodná
cesty	cesta	k1gFnSc2	cesta
padesát	padesát	k4xCc4	padesát
čtyři	čtyři	k4xCgFnPc4	čtyři
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
cyklus	cyklus	k1gInSc4	cyklus
stála	stát	k5eAaImAgFnS	stát
kniha	kniha	k1gFnSc1	kniha
povídek	povídka	k1gFnPc2	povídka
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
a	a	k8xC	a
román	román	k1gInSc1	román
Trosečník	trosečník	k1gMnSc1	trosečník
z	z	k7c2	z
Cynthie	Cynthie	k1gFnSc2	Cynthie
<g/>
,	,	kIx,	,
napsaný	napsaný	k2eAgInSc4d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
Andrém	André	k1gMnSc7	André
Lauriem	Laurium	k1gNnSc7	Laurium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorův	autorův	k2eAgMnSc1d1	autorův
syn	syn	k1gMnSc1	syn
Michel	Michel	k1gMnSc1	Michel
Verne	Vern	k1gInSc5	Vern
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
cyklus	cyklus	k1gInSc4	cyklus
ještě	ještě	k9	ještě
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
osm	osm	k4xCc4	osm
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
Majákem	maják	k1gInSc7	maják
na	na	k7c6	na
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
neděl	neděle	k1gFnPc2	neděle
v	v	k7c6	v
baloně	balon	k1gInSc6	balon
(	(	kIx(	(
<g/>
Cinq	Cinq	k1gMnSc1	Cinq
semaines	semaines	k1gMnSc1	semaines
en	en	k?	en
ballon	ballon	k1gInSc1	ballon
<g/>
,	,	kIx,	,
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
kapitána	kapitán	k1gMnSc2	kapitán
Hatterase	Hatterasa	k1gFnSc3	Hatterasa
(	(	kIx(	(
<g/>
Voyages	Voyages	k1gInSc1	Voyages
et	et	k?	et
aventures	aventures	k1gInSc1	aventures
du	du	k?	du
capitaine	capitainout	k5eAaPmIp3nS	capitainout
Hatteras	Hatteras	k1gInSc1	Hatteras
<g/>
,	,	kIx,	,
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
voyage	voyag	k1gInSc2	voyag
au	au	k0	au
centre	centr	k1gInSc5	centr
de	de	k?	de
la	la	k0	la
terre	terr	k1gMnSc5	terr
<g/>
,	,	kIx,	,
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
(	(	kIx(	(
<g/>
De	De	k?	De
la	la	k1gNnSc1	la
Terre	Terr	k1gInSc5	Terr
à	à	k?	à
la	la	k1gNnSc2	la
Lune	Lune	k1gFnPc2	Lune
<g/>
,	,	kIx,	,
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Grant	k1gMnSc2	Grant
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
enfants	enfants	k1gInSc1	enfants
du	du	k?	du
capitaine	capitainout	k5eAaPmIp3nS	capitainout
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
3	[number]	k4	3
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
(	(	kIx(	(
<g/>
Vingt	Vingt	k2eAgInSc4d1	Vingt
mille	mille	k1gInSc4	mille
lieues	lieuesa	k1gFnPc2	lieuesa
sous	sous	k6eAd1	sous
les	les	k1gInSc1	les
mers	mersa	k1gFnPc2	mersa
<g/>
,	,	kIx,	,
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
Autour	Autoura	k1gFnPc2	Autoura
de	de	k?	de
la	la	k1gNnSc4	la
Lune	Lun	k1gFnSc2	Lun
<g/>
,	,	kIx,	,
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Plující	plující	k2eAgNnSc1d1	plující
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Une	Une	k1gMnSc5	Une
ville	vill	k1gMnSc5	vill
flottante	flottant	k1gMnSc5	flottant
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
současně	současně	k6eAd1	současně
Vernova	Vernův	k2eAgFnSc1d1	Vernova
historická	historický	k2eAgFnSc1d1	historická
novela	novela	k1gFnSc1	novela
Prorazili	prorazit	k5eAaPmAgMnP	prorazit
blokádu	blokáda	k1gFnSc4	blokáda
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Forceurs	Forceurs	k1gInSc1	Forceurs
de	de	k?	de
blocus	blocus	k1gInSc1	blocus
<g/>
)	)	kIx)	)
z	z	k7c2	z
období	období	k1gNnSc2	období
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
tří	tři	k4xCgMnPc2	tři
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
tří	tři	k4xCgMnPc2	tři
Angličanů	Angličan	k1gMnPc2	Angličan
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
,	,	kIx,	,
Aventures	Aventures	k1gInSc1	Aventures
de	de	k?	de
trois	trois	k1gInSc1	trois
Russes	Russes	k1gInSc1	Russes
et	et	k?	et
de	de	k?	de
trois	trois	k1gFnSc1	trois
Anglais	Anglais	k1gFnSc1	Anglais
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
kožešin	kožešina	k1gFnPc2	kožešina
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
pays	pays	k6eAd1	pays
des	des	k1gNnSc7	des
fourrures	fourruresa	k1gFnPc2	fourruresa
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
osmdesát	osmdesát	k4xCc4	osmdesát
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
tour	tour	k1gMnSc1	tour
du	du	k?	du
monde	mond	k1gInSc5	mond
en	en	k?	en
quatre	quatr	k1gInSc5	quatr
<g/>
–	–	k?	–
<g/>
vingts	vingts	k6eAd1	vingts
jours	jours	k6eAd1	jours
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tajuplný	tajuplný	k2eAgInSc1d1	tajuplný
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
île	île	k?	île
mystérieuse	mystérieuse	k1gFnSc1	mystérieuse
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
3	[number]	k4	3
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Chancellor	Chancellor	k1gMnSc1	Chancellor
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
Chancellor	Chancellor	k1gMnSc1	Chancellor
<g/>
,	,	kIx,	,
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
současně	současně	k6eAd1	současně
Vernova	Vernův	k2eAgFnSc1d1	Vernova
historická	historický	k2eAgFnSc1d1	historická
novela	novela	k1gFnSc1	novela
Martin	Martin	k1gMnSc1	Martin
Paz	Paz	k1gMnSc1	Paz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Carův	carův	k2eAgMnSc1d1	carův
kurýr	kurýr	k1gMnSc1	kurýr
(	(	kIx(	(
<g/>
Michel	Michel	k1gMnSc1	Michel
Strogoff	Strogoff	k1gMnSc1	Strogoff
<g/>
,	,	kIx,	,
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
současně	současně	k6eAd1	současně
Vernova	Vernův	k2eAgFnSc1d1	Vernova
historická	historický	k2eAgFnSc1d1	historická
novela	novela	k1gFnSc1	novela
Drama	dramo	k1gNnSc2	dramo
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
(	(	kIx(	(
<g/>
Un	Un	k1gMnSc1	Un
drame	dramat	k5eAaPmIp3nS	dramat
au	au	k0	au
Mexique	Mexique	k1gNnPc6	Mexique
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kometě	kometa	k1gFnSc6	kometa
(	(	kIx(	(
<g/>
Hector	Hector	k1gMnSc1	Hector
Servadac	Servadac	k1gFnSc1	Servadac
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Světem	svět	k1gInSc7	svět
slunečním	sluneční	k2eAgInSc7d1	sluneční
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
indie	indie	k1gFnPc1	indie
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Indes	Indes	k1gMnSc1	Indes
noires	noires	k1gMnSc1	noires
<g/>
,	,	kIx,	,
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Patnáctiletý	patnáctiletý	k2eAgMnSc1d1	patnáctiletý
kapitán	kapitán	k1gMnSc1	kapitán
(	(	kIx(	(
<g/>
Un	Un	k1gMnSc1	Un
capitaine	capitainout	k5eAaPmIp3nS	capitainout
de	de	k?	de
quinze	quinze	k1gFnSc1	quinze
ans	ans	k?	ans
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ocelové	ocelový	k2eAgNnSc1d1	ocelové
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
cinq	cinq	k?	cinq
cents	cents	k1gInSc1	cents
millions	millions	k1gInSc1	millions
de	de	k?	de
la	la	k1gNnPc2	la
Bégum	Bégum	k1gInSc1	Bégum
–	–	k?	–
"	"	kIx"	"
<g/>
Pět	pět	k4xCc4	pět
set	sto	k4xCgNnPc2	sto
miliónů	milión	k4xCgInPc2	milión
Begumy	Begum	k1gInPc4	Begum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
vyšel	vyjít	k5eAaPmAgInS	vyjít
současně	současně	k6eAd1	současně
kratší	krátký	k2eAgInSc1d2	kratší
Vernův	Vernův	k2eAgInSc1d1	Vernův
historický	historický	k2eAgInSc1d1	historický
příběh	příběh	k1gInSc1	příběh
Vzbouřenci	vzbouřenec	k1gMnSc3	vzbouřenec
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Bounty	Bounta	k1gFnSc2	Bounta
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Révoltés	Révoltés	k1gInSc1	Révoltés
de	de	k?	de
la	la	k1gNnSc7	la
Bounty	Bounta	k1gFnSc2	Bounta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číňanovy	Číňanův	k2eAgFnPc1d1	Číňanova
trampoty	trampota	k1gFnPc1	trampota
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
tribulations	tribulations	k1gInSc1	tribulations
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
Chinois	Chinois	k1gInSc1	Chinois
en	en	k?	en
Chine	Chin	k1gMnSc5	Chin
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
O	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zemí	zem	k1gFnSc7	zem
šelem	šelma	k1gFnPc2	šelma
(	(	kIx(	(
<g/>
La	la	k1gNnSc7	la
maison	maison	k1gMnSc1	maison
à	à	k?	à
vapeur	vapeur	k1gMnSc1	vapeur
–	–	k?	–
"	"	kIx"	"
<g/>
Dům	dům	k1gInSc1	dům
na	na	k7c4	na
páru	pára	k1gFnSc4	pára
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Nana	Nana	k1gMnSc1	Nana
Sahib	sahib	k1gMnSc1	sahib
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
Ocelový	ocelový	k2eAgMnSc1d1	ocelový
olbřím	olbřím	k1gMnSc1	olbřím
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
pralesa	prales	k1gInSc2	prales
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Jangada	Jangada	k1gFnSc1	Jangada
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Osm	osm	k4xCc1	osm
set	sto	k4xCgNnPc2	sto
mil	míle	k1gFnPc2	míle
po	po	k7c6	po
Amazonce	Amazonka	k1gFnSc6	Amazonka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Škola	škola	k1gFnSc1	škola
robinsonů	robinson	k1gMnPc2	robinson
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
école	école	k1gInSc1	école
des	des	k1gNnSc1	des
robinsons	robinsons	k1gInSc1	robinsons
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Dva	dva	k4xCgMnPc1	dva
Robinsoni	Robinson	k1gMnPc1	Robinson
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
paprsek	paprsek	k1gInSc1	paprsek
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
rayon	rayon	k1gMnSc1	rayon
vert	vert	k?	vert
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
–	–	k?	–
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
současně	současně	k6eAd1	současně
Vernova	Vernův	k2eAgFnSc1d1	Vernova
krátká	krátký	k2eAgFnSc1d1	krátká
satirická	satirický	k2eAgFnSc1d1	satirická
povídka	povídka	k1gFnSc1	povídka
Deset	deset	k4xCc4	deset
hodin	hodina	k1gFnPc2	hodina
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
(	(	kIx(	(
<g/>
Dix	Dix	k1gMnSc1	Dix
Heures	Heures	k1gMnSc1	Heures
en	en	k?	en
chasse	chasse	k1gFnSc2	chasse
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvrdohlavý	tvrdohlavý	k2eAgInSc1d1	tvrdohlavý
Turek	turek	k1gInSc1	turek
(	(	kIx(	(
<g/>
Kéraban	Kéraban	k1gInSc1	Kéraban
le	le	k?	le
tê	tê	k?	tê
–	–	k?	–
"	"	kIx"	"
<g/>
Paličatý	paličatý	k2eAgInSc1d1	paličatý
Kéraban	Kéraban	k1gInSc1	Kéraban
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
jihu	jih	k1gInSc2	jih
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
étoile	étoile	k6eAd1	étoile
du	du	k?	du
sud	suda	k1gFnPc2	suda
<g/>
,	,	kIx,	,
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Archipel	archipel	k1gInSc1	archipel
v	v	k7c6	v
plamenech	plamen	k1gInPc6	plamen
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
archipel	archipel	k1gInSc1	archipel
en	en	k?	en
feu	feu	k?	feu
<g/>
,	,	kIx,	,
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Sandorf	Sandorf	k1gMnSc1	Sandorf
(	(	kIx(	(
<g/>
Nový	Nový	k1gMnSc5	Nový
hrabě	hrabě	k1gMnSc5	hrabě
Monte	Mont	k1gMnSc5	Mont
Christo	Christa	k1gMnSc5	Christa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Mathias	Mathias	k1gMnSc1	Mathias
Sandorf	Sandorf	k1gMnSc1	Sandorf
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
3	[number]	k4	3
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Robur	Robur	k1gMnSc1	Robur
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
(	(	kIx(	(
<g/>
Robur	Robur	k1gMnSc1	Robur
le	le	k?	le
Conquérant	Conquérant	k1gMnSc1	Conquérant
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Vzducholodí	vzducholoď	k1gFnSc7	vzducholoď
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
číslo	číslo	k1gNnSc1	číslo
9672	[number]	k4	9672
(	(	kIx(	(
<g/>
Un	Un	k1gFnSc1	Un
billet	billet	k1gInSc1	billet
de	de	k?	de
loterie	loterie	k1gFnSc1	loterie
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
número	número	k1gNnSc1	número
9672	[number]	k4	9672
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
současně	současně	k6eAd1	současně
Vernova	Vernův	k2eAgFnSc1d1	Vernova
fantastická	fantastický	k2eAgFnSc1d1	fantastická
novela	novela	k1gFnSc1	novela
Fffff	Fffff	k1gMnSc1	Fffff
<g/>
...	...	k?	...
<g/>
plesk	plesk	k1gInSc1	plesk
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Frritt-Flacc	Frritt-Flacc	k1gFnSc4	Frritt-Flacc
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sever	sever	k1gInSc1	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
(	(	kIx(	(
<g/>
Nord	Nord	k1gInSc1	Nord
contre	contr	k1gInSc5	contr
Sud	suda	k1gFnPc2	suda
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
chemin	chemin	k1gInSc1	chemin
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
románem	román	k1gInSc7	román
vyšla	vyjít	k5eAaPmAgFnS	vyjít
současně	současně	k6eAd1	současně
Vernova	Vernův	k2eAgFnSc1d1	Vernova
satirická	satirický	k2eAgFnSc1d1	satirická
novela	novela	k1gFnSc1	novela
na	na	k7c4	na
britský	britský	k2eAgInSc4d1	britský
kolonialismus	kolonialismus	k1gInSc4	kolonialismus
Gil	Gil	k1gMnSc1	Gil
Braltar	Braltar	k1gMnSc1	Braltar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
prázdnin	prázdniny	k1gFnPc2	prázdniny
(	(	kIx(	(
<g/>
Deux	Deux	k1gInSc1	Deux
ans	ans	k?	ans
de	de	k?	de
vacances	vacances	k1gMnSc1	vacances
<g/>
,	,	kIx,	,
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bezejmenná	bezejmenný	k2eAgFnSc1d1	bezejmenná
rodina	rodina	k1gFnSc1	rodina
(	(	kIx(	(
<g/>
Famille	Famille	k1gInSc1	Famille
sans	sans	k1gInSc1	sans
nom	nom	k?	nom
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zmatek	zmatek	k1gInSc1	zmatek
nad	nad	k7c4	nad
zmatek	zmatek	k1gInSc4	zmatek
(	(	kIx(	(
<g/>
Sans	Sans	k1gInSc1	Sans
dessus	dessus	k1gMnSc1	dessus
dessous	dessous	k1gMnSc1	dessous
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Oceánem	oceán	k1gInSc7	oceán
na	na	k7c6	na
kře	kra	k1gFnSc6	kra
ledové	ledový	k2eAgFnPc1d1	ledová
(	(	kIx(	(
<g/>
César	César	k1gMnSc1	César
Cascabel	Cascabel	k1gMnSc1	Cascabel
<g/>
,	,	kIx,	,
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
V	v	k7c6	v
pustinách	pustina	k1gFnPc6	pustina
australských	australský	k2eAgMnPc2d1	australský
(	(	kIx(	(
<g/>
Mistress	mistress	k1gFnSc1	mistress
Branican	Branican	k1gInSc1	Branican
–	–	k?	–
"	"	kIx"	"
<g/>
Paní	paní	k1gFnSc1	paní
Branicanová	Branicanová	k1gFnSc1	Branicanová
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Zajatec	zajatec	k1gMnSc1	zajatec
pustin	pustina	k1gFnPc2	pustina
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tajemný	tajemný	k2eAgInSc1d1	tajemný
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
(	(	kIx(	(
<g/>
Le	Le	k1gMnPc1	Le
château	château	k1gNnSc2	château
des	des	k1gNnSc2	des
Carpathes	Carpathes	k1gMnSc1	Carpathes
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Claudius	Claudius	k1gMnSc1	Claudius
Bombarnak	Bombarnak	k1gMnSc1	Bombarnak
(	(	kIx(	(
<g/>
Claudius	Claudius	k1gMnSc1	Claudius
Bombarnac	Bombarnac	k1gFnSc1	Bombarnac
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Malý	Malý	k1gMnSc5	Malý
Dobráček	dobráček	k1gMnSc1	dobráček
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
'	'	kIx"	'
<g/>
tit-Bonhomme	tit-Bonhomit	k5eAaPmRp1nP	tit-Bonhomit
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Neobyčejná	obyčejný	k2eNgNnPc1d1	neobyčejné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
mistra	mistr	k1gMnSc2	mistr
Antifera	Antifer	k1gMnSc2	Antifer
(	(	kIx(	(
<g/>
Mirifiques	Mirifiques	k1gMnSc1	Mirifiques
aventures	aventures	k1gMnSc1	aventures
de	de	k?	de
maître	maîtr	k1gInSc5	maîtr
Antifer	Antifer	k1gMnSc1	Antifer
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
závěť	závěť	k1gFnSc1	závěť
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Plovoucí	plovoucí	k2eAgInSc1d1	plovoucí
ostrov	ostrov	k1gInSc1	ostrov
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
île	île	k?	île
hélice	hélice	k1gFnSc1	hélice
<g/>
,	,	kIx,	,
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Vynález	vynález	k1gInSc1	vynález
zkázy	zkáza	k1gFnSc2	zkáza
(	(	kIx(	(
<g/>
Face	Face	k1gInSc1	Face
au	au	k0	au
drapeau	drapeaus	k1gInSc3	drapeaus
–	–	k?	–
"	"	kIx"	"
<g/>
Tváří	tvářet	k5eAaImIp3nS	tvářet
k	k	k7c3	k
praporu	prapor	k1gInSc3	prapor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
praporu	prapor	k1gInSc2	prapor
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Milionář	milionář	k1gMnSc1	milionář
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
(	(	kIx(	(
<g/>
Clovis	Clovis	k1gFnPc2	Clovis
Dardentor	Dardentor	k1gInSc1	Dardentor
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ledová	ledový	k2eAgFnSc1d1	ledová
sfinga	sfinga	k1gFnSc1	sfinga
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
sphinx	sphinx	k1gInSc4	sphinx
des	des	k1gNnSc2	des
glaces	glacesa	k1gFnPc2	glacesa
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vlnách	vlna	k1gFnPc6	vlna
Orinoka	Orinoko	k1gNnSc2	Orinoko
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
superbe	superbat	k5eAaPmIp3nS	superbat
Orénoque	Orénoque	k1gInSc4	Orénoque
–	–	k?	–
"	"	kIx"	"
<g/>
Nádherné	nádherný	k2eAgNnSc1d1	nádherné
Orinoko	Orinoko	k1gNnSc1	Orinoko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Závěť	závěť	k1gFnSc1	závěť
výstředníka	výstředník	k1gMnSc2	výstředník
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc2	Le
testament	testament	k1gInSc1	testament
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
un	un	k?	un
excentrique	excentrique	k1gInSc1	excentrique
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlast	vlast	k1gFnSc1	vlast
(	(	kIx(	(
<g/>
Seconde	Second	k1gInSc5	Second
patrie	patrie	k1gFnSc2	patrie
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ves	ves	k1gFnSc1	ves
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
village	villagat	k5eAaPmIp3nS	villagat
aérien	aérien	k2eAgMnSc1d1	aérien
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Podivuhodné	podivuhodný	k2eAgNnSc1d1	podivuhodné
setkání	setkání	k1gNnSc1	setkání
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Záhadné	záhadný	k2eAgNnSc1d1	záhadné
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
velrybářské	velrybářský	k2eAgFnSc2d1	velrybářská
lodi	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
histoires	histoires	k1gMnSc1	histoires
de	de	k?	de
Jean	Jean	k1gMnSc1	Jean
<g/>
–	–	k?	–
<g/>
Marie	Maria	k1gFnSc2	Maria
Cabidoulin	Cabidoulin	k2eAgMnSc1d1	Cabidoulin
–	–	k?	–
"	"	kIx"	"
<g/>
Příběhy	příběh	k1gInPc1	příběh
Jeana-Marie	Jeana-Marie	k1gFnSc2	Jeana-Marie
Cabidoulina	Cabidoulin	k2eAgFnSc1d1	Cabidoulin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bratři	bratr	k1gMnPc1	bratr
Kipové	Kipová	k1gFnSc2	Kipová
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
frè	frè	k?	frè
Kip	Kip	k1gFnSc2	Kip
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgNnPc1d1	cestovní
stipendia	stipendium	k1gNnPc1	stipendium
(	(	kIx(	(
<g/>
Bourses	Bourses	k1gInSc1	Bourses
de	de	k?	de
voyage	voyage	k1gInSc1	voyage
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Drama	drama	k1gNnSc1	drama
v	v	k7c6	v
Livonsku	Livonsko	k1gNnSc6	Livonsko
(	(	kIx(	(
<g/>
Un	Un	k1gFnSc6	Un
drame	dramat	k5eAaPmIp3nS	dramat
en	en	k?	en
Livonie	Livonie	k1gFnSc1	Livonie
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pán	pán	k1gMnSc1	pán
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Maître	Maîtr	k1gInSc5	Maîtr
du	du	k?	du
monde	mond	k1gInSc5	mond
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zatopená	zatopený	k2eAgFnSc1d1	zatopená
Sahara	Sahara	k1gFnSc1	Sahara
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
invasion	invasion	k1gInSc1	invasion
de	de	k?	de
la	la	k1gNnSc2	la
mer	mer	k?	mer
–	–	k?	–
"	"	kIx"	"
<g/>
Vpád	vpád	k1gInSc1	vpád
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Maják	maják	k1gInSc1	maják
na	na	k7c6	na
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc5	Le
Phare	Phar	k1gMnSc5	Phar
du	du	k?	du
bout	bout	k1gInSc1	bout
du	du	k?	du
monde	mond	k1gInSc5	mond
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
sopka	sopka	k1gFnSc1	sopka
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
volcan	volcan	k1gMnSc1	volcan
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
or	or	k?	or
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Trampoty	trampota	k1gFnPc1	trampota
páně	páně	k2eAgFnSc2d1	páně
Thompsonovy	Thompsonův	k2eAgFnSc2d1	Thompsonova
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
agence	agence	k1gFnSc1	agence
Thompson	Thompson	k1gMnSc1	Thompson
and	and	k?	and
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Honba	honba	k1gFnSc1	honba
za	za	k7c7	za
meteorem	meteor	k1gInSc7	meteor
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
chasse	chasse	k1gFnSc2	chasse
au	au	k0	au
météore	météor	k1gMnSc5	météor
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lodivod	lodivod	k1gMnSc1	lodivod
dunajský	dunajský	k2eAgMnSc1d1	dunajský
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc5	Le
pilote	pilot	k1gMnSc5	pilot
du	du	k?	du
Danube	danub	k1gInSc5	danub
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Trosečníci	trosečník	k1gMnPc1	trosečník
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
Jonathan	Jonathan	k1gMnSc1	Jonathan
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
naufragés	naufragésa	k1gFnPc2	naufragésa
du	du	k?	du
Jonathan	Jonathan	k1gMnSc1	Jonathan
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
Viléma	Vilém	k1gMnSc2	Vilém
Storitze	Storitze	k1gFnSc2	Storitze
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
secret	secret	k1gMnSc1	secret
de	de	k?	de
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Storitz	Storitz	k1gMnSc1	Storitz
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Podivuhodná	podivuhodný	k2eAgNnPc1d1	podivuhodné
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
výpravy	výprava	k1gFnSc2	výprava
Barsacovy	Barsacův	k2eAgFnSc2d1	Barsacův
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
étonnante	étonnant	k1gMnSc5	étonnant
aventure	aventur	k1gMnSc5	aventur
de	de	k?	de
la	la	k1gNnPc6	la
mission	mission	k1gInSc4	mission
Barsac	Barsac	k1gFnSc1	Barsac
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
1	[number]	k4	1
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnPc1d1	další
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Kněz	kněz	k1gMnSc1	kněz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
(	(	kIx(	(
<g/>
Un	Un	k1gFnSc1	Un
Prê	Prê	k1gFnSc2	Prê
en	en	k?	en
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
Vernova	Vernův	k2eAgFnSc1d1	Vernova
próza	próza	k1gFnSc1	próza
<g/>
,	,	kIx,	,
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
prvně	prvně	k?	prvně
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
pozpátku	pozpátku	k6eAd1	pozpátku
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Skotska	Skotsko	k1gNnSc2	Skotsko
(	(	kIx(	(
<g/>
Voyage	Voyage	k1gInSc1	Voyage
à	à	k?	à
reculons	reculons	k1gInSc1	reculons
en	en	k?	en
Angleterre	Angleterr	k1gInSc5	Angleterr
et	et	k?	et
en	en	k?	en
Écosse	Écosse	k1gFnSc2	Écosse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Verne	Vern	k1gInSc5	Vern
napsal	napsat	k5eAaPmAgInS	napsat
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInSc1	jehož
námět	námět	k1gInSc1	námět
vytěžil	vytěžit	k5eAaPmAgInS	vytěžit
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
dvou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Prvně	Prvně	k?	Prvně
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
až	až	k9	až
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
au	au	k0	au
XXe	XXe	k1gFnPc6	XXe
siè	siè	k?	siè
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Hetzel	Hetzel	k1gMnSc1	Hetzel
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vydat	vydat	k5eAaPmF	vydat
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
přílišný	přílišný	k2eAgInSc4d1	přílišný
pesimismus	pesimismus	k1gInSc4	pesimismus
<g/>
.	.	kIx.	.
</s>
<s>
Verne	Vernout	k5eAaPmIp3nS	Vernout
pak	pak	k6eAd1	pak
rukopis	rukopis	k1gInSc4	rukopis
založil	založit	k5eAaPmAgMnS	založit
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgInPc4	svůj
ostatní	ostatní	k2eAgInPc4d1	ostatní
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
objeven	objevit	k5eAaPmNgMnS	objevit
Vernovým	Vernův	k2eAgMnSc7d1	Vernův
pravnukem	pravnuk	k1gMnSc7	pravnuk
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
de	de	k?	de
Chanteleine	Chantelein	k1gMnSc5	Chantelein
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc5	Le
Comte	Comt	k1gMnSc5	Comt
de	de	k?	de
Chanteleine	Chantelein	k1gMnSc5	Chantelein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
publikoval	publikovat	k5eAaBmAgMnS	publikovat
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Musée	Musé	k1gFnSc2	Musé
des	des	k1gNnSc2	des
Familles	Famillesa	k1gFnPc2	Famillesa
a	a	k8xC	a
jehož	jenž	k3xRgNnSc2	jenž
knižního	knižní	k2eAgNnSc2d1	knižní
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
nikdy	nikdy	k6eAd1	nikdy
nedočkal	dočkat	k5eNaPmAgInS	dočkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Edgard	Edgard	k1gMnSc1	Edgard
Poe	Poe	k1gMnSc1	Poe
et	et	k?	et
ses	ses	k?	ses
œ	œ	k?	œ
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Poe	Poe	k1gMnSc1	Poe
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Musée	Musé	k1gFnSc2	Musé
des	des	k1gNnSc2	des
Familles	Famillesa	k1gFnPc2	Famillesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
zeměpis	zeměpis	k1gInSc1	zeměpis
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
Géographie	Géographie	k1gFnSc1	Géographie
illustrée	illustré	k1gFnSc2	illustré
de	de	k?	de
la	la	k1gNnSc1	la
France	Franc	k1gMnSc2	Franc
et	et	k?	et
de	de	k?	de
ses	ses	k?	ses
colonies	colonies	k1gMnSc1	colonies
<g/>
,	,	kIx,	,
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
započaté	započatý	k2eAgNnSc1d1	započaté
profesorem	profesor	k1gMnSc7	profesor
Théophilem	Théophil	k1gMnSc7	Théophil
Lavalléem	Lavallé	k1gMnSc7	Lavallé
(	(	kIx(	(
<g/>
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Hetzela	Hetzela	k1gFnSc2	Hetzela
Vernem	Vern	k1gInSc7	Vern
dokončené	dokončený	k2eAgNnSc1d1	dokončené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Strýček	strýček	k1gMnSc1	strýček
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Oncle	Oncl	k1gMnSc2	Oncl
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
nakladatel	nakladatel	k1gMnSc1	nakladatel
Hetzel	Hetzel	k1gMnSc1	Hetzel
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vydat	vydat	k5eAaPmF	vydat
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc4	jehož
většinu	většina	k1gFnSc4	většina
textu	text	k1gInSc2	text
Verne	Vern	k1gInSc5	Vern
využil	využít	k5eAaPmAgMnS	využít
pro	pro	k7c4	pro
napsání	napsání	k1gNnSc4	napsání
prvního	první	k4xOgMnSc2	první
dílu	dílo	k1gNnSc3	dílo
Tajuplného	tajuplný	k2eAgInSc2d1	tajuplný
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
samostatně	samostatně	k6eAd1	samostatně
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
velkých	velký	k2eAgInPc2d1	velký
objevů	objev	k1gInPc2	objev
(	(	kIx(	(
<g/>
Découverte	Découvert	k1gInSc5	Découvert
de	de	k?	de
la	la	k0	la
Terre	Terr	k1gInSc5	Terr
(	(	kIx(	(
<g/>
Histoire	Histoir	k1gMnSc5	Histoir
générale	général	k1gMnSc5	général
des	des	k1gNnSc7	des
grands	grands	k1gInSc1	grands
voyages	voyages	k1gInSc1	voyages
et	et	k?	et
des	des	k1gNnSc2	des
grands	grands	k6eAd1	grands
voyageurs	voyageurs	k6eAd1	voyageurs
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několikadílný	několikadílný	k2eAgInSc4d1	několikadílný
geografický	geografický	k2eAgInSc4d1	geografický
a	a	k8xC	a
historický	historický	k2eAgInSc4d1	historický
spis	spis	k1gInSc4	spis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
docteur	docteur	k1gMnSc1	docteur
Ox	Ox	k1gMnSc1	Ox
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
autorových	autorův	k2eAgFnPc2d1	autorova
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
titulní	titulní	k2eAgFnSc2d1	titulní
novely	novela	k1gFnSc2	novela
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kniha	kniha	k1gFnSc1	kniha
ještě	ještě	k6eAd1	ještě
Drama	drama	k1gNnSc1	drama
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
(	(	kIx(	(
<g/>
Un	Un	k1gMnSc5	Un
Drame	Dram	k1gMnSc5	Dram
dans	dans	k1gInSc4	dans
les	les	k1gInSc4	les
Airs	Airsa	k1gFnPc2	Airsa
<g/>
,	,	kIx,	,
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
Zachariáš	Zachariáš	k1gFnSc2	Zachariáš
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Maître	Maîtr	k1gInSc5	Maîtr
Zacharius	Zacharius	k1gMnSc1	Zacharius
<g/>
,	,	kIx,	,
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zajatci	zajatec	k1gMnPc1	zajatec
polárního	polární	k2eAgNnSc2d1	polární
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
Un	Un	k1gFnSc1	Un
Hivernage	Hivernag	k1gFnSc2	Hivernag
dans	dans	k6eAd1	dans
les	les	k1gInSc4	les
Glaces	Glacesa	k1gFnPc2	Glacesa
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
a	a	k8xC	a
cestopisnou	cestopisný	k2eAgFnSc4d1	cestopisná
črtu	črta	k1gFnSc4	črta
Vernova	Vernův	k2eAgMnSc2d1	Vernův
bratra	bratr	k1gMnSc2	bratr
Paula	Paul	k1gMnSc2	Paul
Čtyřicátý	čtyřicátý	k4xOgInSc4	čtyřicátý
francouzský	francouzský	k2eAgInSc4d1	francouzský
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
Mont	Mont	k1gInSc4	Mont
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
La	la	k1gNnPc1	la
quarantieme	quarantiat	k5eAaBmIp1nP	quarantiat
ascension	ascension	k1gInSc4	ascension
française	française	k1gFnSc2	française
du	du	k?	du
Mont-Blanc	Mont-Blanc	k1gInSc1	Mont-Blanc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesty	cesta	k1gFnPc4	cesta
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Voyages	Voyagesa	k1gFnPc2	Voyagesa
au	au	k0	au
théâtre	théâtr	k1gMnSc5	théâtr
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společné	společný	k2eAgNnSc4d1	společné
vydání	vydání	k1gNnSc4	vydání
tří	tři	k4xCgFnPc2	tři
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
osmdesát	osmdesát	k4xCc4	osmdesát
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
Děti	dítě	k1gFnPc1	dítě
kapitána	kapitán	k1gMnSc2	kapitán
Granta	Granta	k1gFnSc1	Granta
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Strogov	Strogov	k1gInSc1	Strogov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Verne	Vern	k1gInSc5	Vern
napsal	napsat	k5eAaPmAgInS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
dramatikem	dramatik	k1gMnSc7	dramatik
Adolphem	Adolph	k1gInSc7	Adolph
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Ennerym	Ennerym	k1gInSc1	Ennerym
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Trosečník	trosečník	k1gMnSc1	trosečník
z	z	k7c2	z
Cynthie	Cynthie	k1gFnSc2	Cynthie
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Épave	Épaev	k1gFnPc1	Épaev
du	du	k?	du
Cynthia	Cynthia	k1gFnSc1	Cynthia
<g/>
,	,	kIx,	,
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
společně	společně	k6eAd1	společně
s	s	k7c7	s
André	André	k1gMnSc7	André
Lauriem	Laurium	k1gNnSc7	Laurium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Včera	včera	k6eAd1	včera
a	a	k8xC	a
zítra	zítra	k6eAd1	zítra
(	(	kIx(	(
<g/>
Hier	Hier	k1gMnSc1	Hier
et	et	k?	et
demain	demain	k1gMnSc1	demain
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
posmrtně	posmrtně	k6eAd1	posmrtně
vydaných	vydaný	k2eAgFnPc2d1	vydaná
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
Verneovým	Verneův	k2eAgMnSc7d1	Verneův
synem	syn	k1gMnSc7	syn
Michelem	Michel	k1gMnSc7	Michel
<g/>
.	.	kIx.	.
</s>
<s>
Verneovo	Verneův	k2eAgNnSc1d1	Verneovo
autorství	autorství	k1gNnSc1	autorství
těchto	tento	k3xDgNnPc2	tento
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
však	však	k9	však
dosti	dosti	k6eAd1	dosti
problematické	problematický	k2eAgNnSc1d1	problematické
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
povídky	povídka	k1gFnPc4	povídka
napsal	napsat	k5eAaPmAgMnS	napsat
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
původně	původně	k6eAd1	původně
napsal	napsat	k5eAaPmAgMnS	napsat
otec	otec	k1gMnSc1	otec
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
syn	syn	k1gMnSc1	syn
Michel	Michel	k1gMnSc1	Michel
Verne	Vern	k1gInSc5	Vern
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
úpravy	úprava	k1gFnPc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tyto	tento	k3xDgFnPc4	tento
povídky	povídka	k1gFnPc4	povídka
a	a	k8xC	a
novely	novela	k1gFnPc4	novela
<g/>
:	:	kIx,	:
Rodina	rodina	k1gFnSc1	rodina
Krysáků	Krysák	k1gInPc2	Krysák
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
famille	famille	k1gFnPc1	famille
Raton	Raton	k1gMnSc1	Raton
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
Dis	dis	k1gNnSc2	dis
a	a	k8xC	a
slečna	slečna	k1gFnSc1	slečna
Es	es	k1gNnSc2	es
(	(	kIx(	(
<g/>
Monsieur	Monsieur	k1gMnSc1	Monsieur
Ré-Dieze	Ré-Dieze	k1gFnSc2	Ré-Dieze
et	et	k?	et
Mademoiselle	Mademoiselle	k1gFnSc2	Mademoiselle
Mi-Bémol	Mi-Bémola	k1gFnPc2	Mi-Bémola
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Osud	osud	k1gInSc4	osud
Jeana	Jean	k1gMnSc2	Jean
Morénase	Morénas	k1gInSc6	Morénas
(	(	kIx(	(
<g/>
La	la	k1gNnSc6	la
Destinée	Destiné	k1gInSc2	Destiné
de	de	k?	de
Jean	Jean	k1gMnSc1	Jean
<g />
.	.	kIx.	.
</s>
<s>
Morénas	Morénas	k1gInSc1	Morénas
<g/>
)	)	kIx)	)
a	a	k8xC	a
Humbuk	humbuk	k1gInSc1	humbuk
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Humbug	Humbuga	k1gFnPc2	Humbuga
<g/>
)	)	kIx)	)
z	z	k7c2	z
Verneovy	Verneův	k2eAgFnSc2d1	Verneova
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
díla	dílo	k1gNnPc4	dílo
Michela	Michel	k1gMnSc2	Michel
Vernea	Verneus	k1gMnSc2	Verneus
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
XXIX	XXIX	kA	XXIX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
(	(	kIx(	(
<g/>
Au	au	k0	au
XXIXe	XXIXus	k1gMnSc5	XXIXus
siecle	siecl	k1gMnSc5	siecl
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
a	a	k8xC	a
Věčný	věčný	k2eAgMnSc1d1	věčný
Adam	Adam	k1gMnSc1	Adam
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Éternel	Éternel	k1gMnSc1	Éternel
Adam	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ilustrátoři	ilustrátor	k1gMnPc5	ilustrátor
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
ilustrátoři	ilustrátor	k1gMnPc1	ilustrátor
===	===	k?	===
</s>
</p>
<p>
<s>
Édouard	Édouard	k1gInSc1	Édouard
Riou	Rious	k1gInSc2	Rious
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
–	–	k?	–
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	let	k1gInPc6	let
1863	[number]	k4	1863
–	–	k?	–
1881	[number]	k4	1881
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
osm	osm	k4xCc4	osm
Verneových	Verneův	k2eAgNnPc2d1	Verneovo
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gInSc1	Jules
Férat	Férat	k1gInSc1	Férat
(	(	kIx(	(
<g/>
1819	[number]	k4	1819
–	–	k?	–
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	let	k1gInPc6	let
1871	[number]	k4	1871
–	–	k?	–
1881	[number]	k4	1881
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
šest	šest	k4xCc4	šest
Verneových	Verneův	k2eAgInPc2d1	Verneův
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
jeho	jeho	k3xOp3gNnSc1	jeho
novely	novela	k1gFnPc4	novela
a	a	k8xC	a
knihu	kniha	k1gFnSc4	kniha
jeho	jeho	k3xOp3gFnPc2	jeho
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
Cesty	cesta	k1gFnSc2	cesta
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Léon	Léon	k1gMnSc1	Léon
Benett	Benett	k1gMnSc1	Benett
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
–	–	k?	–
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	let	k1gInPc6	let
1873	[number]	k4	1873
–	–	k?	–
1910	[number]	k4	1910
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
dvacet	dvacet	k4xCc4	dvacet
osm	osm	k4xCc1	osm
Verneových	Verneův	k2eAgNnPc2d1	Verneovo
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
George	George	k1gInSc1	George
Roux	Roux	k1gInSc1	Roux
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
–	–	k?	–
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
v	v	k7c6	v
letech	let	k1gInPc6	let
1885	[number]	k4	1885
–	–	k?	–
1919	[number]	k4	1919
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
dvacet	dvacet	k4xCc4	dvacet
Verneových	Verneův	k2eAgInPc2d1	Verneův
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
jeho	jeho	k3xOp3gFnSc2	jeho
novely	novela	k1gFnSc2	novela
a	a	k8xC	a
podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
ilustracích	ilustrace	k1gFnPc6	ilustrace
knihy	kniha	k1gFnSc2	kniha
Verneových	Verneův	k2eAgFnPc2d1	Verneova
povídek	povídka	k1gFnPc2	povídka
Včera	včera	k6eAd1	včera
a	a	k8xC	a
zítra	zítra	k6eAd1	zítra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ilustrátoři	ilustrátor	k1gMnPc1	ilustrátor
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
děl	dělo	k1gNnPc2	dělo
===	===	k?	===
</s>
</p>
<p>
<s>
Alexandre	Alexandr	k1gInSc5	Alexandr
de	de	k?	de
Bar	bar	k1gInSc1	bar
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jeanem-Valentinem	Jeanem-Valentin	k1gMnSc7	Jeanem-Valentin
Foulquierem	Foulquier	k1gMnSc7	Foulquier
román	román	k1gInSc4	román
Hrabě	Hrabě	k1gMnSc1	Hrabě
de	de	k?	de
Chanteleine	Chantelein	k1gMnSc5	Chantelein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gédéon	Gédéon	k1gMnSc1	Gédéon
Baril	Baril	k1gMnSc1	Baril
(	(	kIx(	(
<g/>
1832	[number]	k4	1832
<g/>
-	-	kIx~	-
<g/>
1906	[number]	k4	1906
<g/>
))	))	k?	))
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
povídku	povídka	k1gFnSc4	povídka
Deset	deset	k4xCc1	deset
hodin	hodina	k1gFnPc2	hodina
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Émile-Antoine	Émile-Antoinout	k5eAaPmIp3nS	Émile-Antoinout
Bayard	Bayard	k1gInSc1	Bayard
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
–	–	k?	–
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Alphonsem	Alphons	k1gInSc7	Alphons
de	de	k?	de
Neuvillem	Neuvill	k1gInSc7	Neuvill
román	román	k1gInSc1	román
Okolo	okolo	k7c2	okolo
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
povídku	povídka	k1gFnSc4	povídka
Drama	dramo	k1gNnSc2	dramo
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Quesnay	Quesnaa	k1gFnSc2	Quesnaa
de	de	k?	de
Beaurépaire	Beaurépair	k1gInSc5	Beaurépair
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
–	–	k?	–
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Julesem	Jules	k1gMnSc7	Jules
Fératem	Férat	k1gMnSc7	Férat
Zemi	zem	k1gFnSc3	zem
kožešin	kožešina	k1gFnPc2	kožešina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hubert	Hubert	k1gMnSc1	Hubert
Clerget	Clerget	k1gMnSc1	Clerget
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
–	–	k?	–
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
společně	společně	k6eAd1	společně
Édouardem	Édouard	k1gInSc7	Édouard
Riouem	Riouem	k1gInSc4	Riouem
Ilustrovaný	ilustrovaný	k2eAgInSc1d1	ilustrovaný
zeměpis	zeměpis	k1gInSc1	zeměpis
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S.	S.	kA	S.
Drée	Drée	k1gFnSc1	Drée
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
historický	historický	k2eAgInSc1d1	historický
příběh	příběh	k1gInSc1	příběh
Vzbouřenci	vzbouřenec	k1gMnSc3	vzbouřenec
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Bounty	Bounta	k1gFnSc2	Bounta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jean-Valentin	Jean-Valentin	k2eAgInSc1d1	Jean-Valentin
Foulquier	Foulquier	k1gInSc1	Foulquier
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
-	-	kIx~	-
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
de	de	k?	de
Barem	Barma	k1gFnPc2	Barma
román	román	k1gInSc1	román
Hrabě	Hrabě	k1gMnSc1	Hrabě
de	de	k?	de
Chanteleine	Chantelein	k1gMnSc5	Chantelein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lorenz	Lorenz	k1gMnSc1	Lorenz
Frø	Frø	k1gMnSc1	Frø
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
–	–	k?	–
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
novelu	novela	k1gFnSc4	novela
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
ze	z	k7c2	z
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídkové	povídkový	k2eAgFnSc2d1	povídková
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Adrien	Adriena	k1gFnPc2	Adriena
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
–	–	k?	–
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
novelu	novela	k1gFnSc4	novela
Zajatci	zajatec	k1gMnPc1	zajatec
polárního	polární	k2eAgNnSc2d1	polární
moře	moře	k1gNnSc2	moře
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Henri	Henri	k1gNnSc1	Henri
Meyer	Meyra	k1gFnPc2	Meyra
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
–	–	k?	–
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Patnáctiletého	patnáctiletý	k2eAgMnSc4d1	patnáctiletý
kapitána	kapitán	k1gMnSc4	kapitán
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Léonem	Léon	k1gMnSc7	Léon
Benettem	Benett	k1gMnSc7	Benett
<g/>
,	,	kIx,	,
Édouardem	Édouard	k1gMnSc7	Édouard
Riouem	Riou	k1gMnSc7	Riou
a	a	k8xC	a
Julesem	Jules	k1gMnSc7	Jules
Fératem	Férat	k1gMnSc7	Férat
knihu	kniha	k1gFnSc4	kniha
Vernových	Vernův	k2eAgFnPc2d1	Vernova
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
Cesty	cesta	k1gFnSc2	cesta
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Henri	Henri	k6eAd1	Henri
de	de	k?	de
Montaut	Montaut	k1gInSc1	Montaut
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
–	–	k?	–
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Ědouardem	Ědouard	k1gInSc7	Ědouard
Riouem	Riouem	k1gInSc1	Riouem
Pět	pět	k4xCc1	pět
neděl	neděle	k1gFnPc2	neděle
v	v	k7c6	v
balóně	balón	k1gInSc6	balón
a	a	k8xC	a
Dobrodružství	dobrodružství	k1gNnSc6	dobrodružství
kapitána	kapitán	k1gMnSc2	kapitán
Hatterase	Hatterasa	k1gFnSc6	Hatterasa
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
Ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Félicien	Félicien	k2eAgInSc1d1	Félicien
de	de	k?	de
Myrbach	Myrbach	k1gInSc1	Myrbach
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
–	–	k?	–
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Léonem	Léon	k1gMnSc7	Léon
Benettem	Benett	k1gMnSc7	Benett
a	a	k8xC	a
Georgem	Georg	k1gMnSc7	Georg
Rouxem	Roux	k1gInSc7	Roux
knihu	kniha	k1gFnSc4	kniha
povídek	povídka	k1gFnPc2	povídka
Včera	včera	k6eAd1	včera
a	a	k8xC	a
zítra	zítra	k6eAd1	zítra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alphonse	Alphonse	k1gFnSc1	Alphonse
de	de	k?	de
Neuville	Neuville	k1gFnSc1	Neuville
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
–	–	k?	–
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
společně	společně	k6eAd1	společně
Ědouardem	Ědouard	k1gInSc7	Ědouard
Riouem	Riouem	k1gInSc4	Riouem
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
s	s	k7c7	s
Émilem-Antoinem	Émilem-Antoin	k1gMnSc7	Émilem-Antoin
Bayardem	Bayard	k1gMnSc7	Bayard
Okolo	okolo	k7c2	okolo
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
s	s	k7c7	s
Léonem	Léon	k1gMnSc7	Léon
Benettem	Benett	k1gMnSc7	Benett
Cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
osmdesát	osmdesát	k4xCc4	osmdesát
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paul-Dominique	Paul-Dominique	k1gInSc1	Paul-Dominique
Philippoteaux	Philippoteaux	k1gInSc1	Philippoteaux
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
–	–	k?	–
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
román	román	k1gInSc4	román
Na	na	k7c6	na
kometě	kometa	k1gFnSc6	kometa
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Léonem	Léono	k1gNnSc7	Léono
Benettem	Benett	k1gInSc7	Benett
několikadílný	několikadílný	k2eAgInSc4d1	několikadílný
geografický	geografický	k2eAgInSc4d1	geografický
a	a	k8xC	a
historický	historický	k2eAgInSc4d1	historický
spis	spis	k1gInSc4	spis
Historie	historie	k1gFnSc2	historie
velkých	velký	k2eAgInPc2d1	velký
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
François	François	k1gFnSc1	François
Schuiten	Schuitno	k1gNnPc2	Schuitno
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
Paříž	Paříž	k1gFnSc4	Paříž
ve	v	k7c6	v
dvacátém	dvacátý	k4xOgInSc6	dvacátý
století	století	k1gNnSc6	století
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Théophile	Théophile	k6eAd1	Théophile
Schuler	Schuler	k1gInSc1	Schuler
(	(	kIx(	(
<g/>
1821	[number]	k4	1821
–	–	k?	–
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
povídku	povídka	k1gFnSc4	povídka
Mistr	mistr	k1gMnSc1	mistr
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Tardi	Tard	k1gMnPc1	Tard
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
román	román	k1gInSc1	román
Kněz	kněz	k1gMnSc1	kněz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
sbírku	sbírka	k1gFnSc4	sbírka
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
San	San	k1gMnSc1	San
Carlos	Carlos	k1gMnSc1	Carlos
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Georges	Georges	k1gMnSc1	Georges
Tiret-Bognet	Tiret-Bognet	k1gMnSc1	Tiret-Bognet
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
–	–	k?	–
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
Bezejmennou	bezejmenný	k2eAgFnSc4d1	bezejmenná
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Edmond	Edmond	k1gMnSc1	Edmond
Yon	Yon	k1gMnSc1	Yon
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
–	–	k?	–
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
povídku	povídka	k1gFnSc4	povídka
Čtyřicátý	čtyřicátý	k4xOgInSc1	čtyřicátý
francouzský	francouzský	k2eAgInSc1d1	francouzský
výstup	výstup	k1gInSc1	výstup
na	na	k7c4	na
Mont	Mont	k1gInSc4	Mont
Blanc	Blanc	k1gMnSc1	Blanc
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Doktor	doktor	k1gMnSc1	doktor
Ox	Ox	k1gMnSc1	Ox
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Verneovo	Verneův	k2eAgNnSc4d1	Verneovo
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
české	český	k2eAgMnPc4d1	český
čtenáře	čtenář	k1gMnPc4	čtenář
objevil	objevit	k5eAaPmAgMnS	objevit
knihy	kniha	k1gFnPc4	kniha
Julese	Julese	k1gFnSc2	Julese
Vernea	Verneum	k1gNnSc2	Verneum
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
koupil	koupit	k5eAaPmAgMnS	koupit
knihu	kniha	k1gFnSc4	kniha
Pět	pět	k4xCc1	pět
neděl	neděle	k1gFnPc2	neděle
v	v	k7c6	v
balóně	balón	k1gInSc6	balón
a	a	k8xC	a
přivezl	přivézt	k5eAaPmAgMnS	přivézt
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
díky	díky	k7c3	díky
Nerudovi	Neruda	k1gMnSc6	Neruda
vyšel	vyjít	k5eAaPmAgMnS	vyjít
v	v	k7c6	v
knižnici	knižnice	k1gFnSc6	knižnice
Matice	matice	k1gFnSc2	matice
lidu	lid	k1gInSc2	lid
první	první	k4xOgMnSc1	první
Verneův	Verneův	k2eAgInSc1d1	Verneův
román	román	k1gInSc1	román
přeložený	přeložený	k2eAgInSc1d1	přeložený
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
–	–	k?	–
Cesta	cesta	k1gFnSc1	cesta
kolem	kolem	k7c2	kolem
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatel	nakladatel	k1gMnSc1	nakladatel
Josef	Josef	k1gMnSc1	Josef
Richard	Richard	k1gMnSc1	Richard
Vilímek	Vilímek	k1gMnSc1	Vilímek
v	v	k7c6	v
letech	let	k1gInPc6	let
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
vydal	vydat	k5eAaPmAgInS	vydat
52	[number]	k4	52
Verneových	Verneův	k2eAgInPc2d1	Verneův
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Vilímek	Vilímek	k1gMnSc1	Vilímek
také	také	k9	také
nepoužil	použít	k5eNaPmAgMnS	použít
přesný	přesný	k2eAgInSc4d1	přesný
překlad	překlad	k1gInSc4	překlad
názvů	název	k1gInPc2	název
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
názvy	název	k1gInPc4	název
značně	značně	k6eAd1	značně
upravil	upravit	k5eAaPmAgInS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
úpravy	úprava	k1gFnPc1	úprava
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
Později	pozdě	k6eAd2	pozdě
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
Verneovy	Verneův	k2eAgInPc4d1	Verneův
romány	román	k1gInPc4	román
Státní	státní	k2eAgNnSc4d1	státní
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
dětské	dětský	k2eAgFnSc2d1	dětská
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Podivuhodné	podivuhodný	k2eAgFnSc2d1	podivuhodná
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
edici	edice	k1gFnSc6	edice
Spisy	spis	k1gInPc4	spis
Julese	Julese	k1gFnSc2	Julese
Verna	Vern	k1gInSc2	Vern
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgFnSc1d2	kratší
práce	práce	k1gFnSc1	práce
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
v	v	k7c6	v
sešitové	sešitový	k2eAgFnSc6d1	sešitová
edici	edice	k1gFnSc6	edice
Karavana	karavana	k1gFnSc1	karavana
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
začalo	začít	k5eAaPmAgNnS	začít
vydávat	vydávat	k5eAaPmF	vydávat
novou	nový	k2eAgFnSc4d1	nová
řadu	řada	k1gFnSc4	řada
třinácti	třináct	k4xCc2	třináct
Verneových	Verneův	k2eAgInPc2d1	Verneův
románů	román	k1gInPc2	román
převyprávěných	převyprávěný	k2eAgInPc2d1	převyprávěný
Ondřejem	Ondřej	k1gMnSc7	Ondřej
Neffem	Neff	k1gMnSc7	Neff
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
do	do	k7c2	do
vydávání	vydávání	k1gNnSc2	vydávání
kompletního	kompletní	k2eAgNnSc2d1	kompletní
díla	dílo	k1gNnSc2	dílo
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
pustilo	pustit	k5eAaPmAgNnS	pustit
brněnské	brněnský	k2eAgNnSc1d1	brněnské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Návrat	návrat	k1gInSc1	návrat
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
vydalo	vydat	k5eAaPmAgNnS	vydat
79	[number]	k4	79
svazků	svazek	k1gInPc2	svazek
a	a	k8xC	a
ve	v	k7c6	v
vydávání	vydávání	k1gNnSc6	vydávání
dalších	další	k2eAgInPc2d1	další
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
novodobým	novodobý	k2eAgMnSc7d1	novodobý
vydavatelem	vydavatel	k1gMnSc7	vydavatel
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Vybíral	Vybíral	k1gMnSc1	Vybíral
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
Československu	Československo	k1gNnSc6	Československo
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Verneových	Verneův	k2eAgInPc2d1	Verneův
textů	text	k1gInPc2	text
občas	občas	k6eAd1	občas
vypouštěny	vypouštěn	k2eAgFnPc1d1	vypouštěna
nebo	nebo	k8xC	nebo
upravovány	upravován	k2eAgFnPc1d1	upravována
části	část	k1gFnPc1	část
s	s	k7c7	s
náboženskými	náboženský	k2eAgInPc7d1	náboženský
odkazy	odkaz	k1gInPc7	odkaz
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Verneových	Verneův	k2eAgInPc2d1	Verneův
románů	román	k1gInPc2	román
natočil	natočit	k5eAaBmAgMnS	natočit
český	český	k2eAgMnSc1d1	český
režisér	režisér	k1gMnSc1	režisér
Karel	Karel	k1gMnSc1	Karel
Zeman	Zeman	k1gMnSc1	Zeman
filmy	film	k1gInPc7	film
Vynález	vynález	k1gInSc1	vynález
zkázy	zkáza	k1gFnSc2	zkáza
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ukradená	ukradený	k2eAgFnSc1d1	ukradená
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
a	a	k8xC	a
Na	na	k7c6	na
kometě	kometa	k1gFnSc6	kometa
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1892	[number]	k4	1892
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
na	na	k7c4	na
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
údajně	údajně	k6eAd1	údajně
navštívil	navštívit	k5eAaPmAgInS	navštívit
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gNnSc3	on
podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
byl	být	k5eAaImAgInS	být
společníkem	společník	k1gMnSc7	společník
mladý	mladý	k2eAgMnSc1d1	mladý
evangelický	evangelický	k2eAgMnSc1d1	evangelický
kněz	kněz	k1gMnSc1	kněz
Ján	Ján	k1gMnSc1	Ján
Maliarik	Maliarik	k1gMnSc1	Maliarik
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgMnS	vydat
Verne	Vern	k1gInSc5	Vern
román	román	k1gInSc1	román
Tajemný	tajemný	k2eAgInSc4d1	tajemný
hrad	hrad	k1gInSc4	hrad
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Maria	Maria	k1gFnSc1	Maria
Janatka	Janatka	k1gFnSc1	Janatka
<g/>
:	:	kIx,	:
Neznámý	známý	k2eNgMnSc1d1	neznámý
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
(	(	kIx(	(
<g/>
jeho	on	k3xPp3gInSc4	on
skutečný	skutečný	k2eAgInSc4d1	skutečný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Neff	Neff	k1gMnSc1	Neff
<g/>
:	:	kIx,	:
Podivuhodný	podivuhodný	k2eAgInSc1d1	podivuhodný
svět	svět	k1gInSc1	svět
Julese	Julese	k1gFnSc1	Julese
Verna	Verna	k1gFnSc1	Verna
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS	Jevgenít
Brandis	Brandis	k1gFnSc1	Brandis
<g/>
:	:	kIx,	:
Snílek	snílek	k1gMnSc1	snílek
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
R.	R.	kA	R.
Lottman	Lottman	k1gMnSc1	Lottman
<g/>
:	:	kIx,	:
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
(	(	kIx(	(
<g/>
život	život	k1gInSc4	život
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
klasika	klasik	k1gMnSc2	klasik
sci-fi	scii	k1gFnSc2	sci-fi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7243	[number]	k4	7243
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Vadim	Vadim	k?	Vadim
Horák	Horák	k1gMnSc1	Horák
<g/>
:	:	kIx,	:
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Jos	Jos	k1gFnSc2	Jos
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Thyrsus	thyrsus	k1gInSc1	thyrsus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Neff	Neff	k1gMnSc1	Neff
<g/>
:	:	kIx,	:
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Podivuhodné	podivuhodný	k2eAgFnPc1d1	podivuhodná
cesty	cesta	k1gFnPc1	cesta
</s>
</p>
<p>
<s>
Michel	Michelit	k5eAaPmRp2nS	Michelit
Verne	Vern	k1gMnSc5	Vern
</s>
</p>
<p>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Zeman	Zeman	k1gMnSc1	Zeman
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
francouzských	francouzský	k2eAgMnPc2d1	francouzský
ilustrátorů	ilustrátor	k1gMnPc2	ilustrátor
Julese	Julese	k1gFnSc2	Julese
Verna	Verna	k1gFnSc1	Verna
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
ilustrátorů	ilustrátor	k1gMnPc2	ilustrátor
Julese	Julese	k1gFnSc2	Julese
Verna	Verna	k1gFnSc1	Verna
</s>
</p>
<p>
<s>
André	André	k1gMnSc1	André
Laurie	Laurie	k1gFnSc2	Laurie
</s>
</p>
<p>
<s>
Pierre-Jules	Pierre-Jules	k1gMnSc1	Pierre-Jules
Hetzel	Hetzel	k1gMnSc1	Hetzel
</s>
</p>
<p>
<s>
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Plné	plný	k2eAgInPc1d1	plný
texty	text	k1gInPc1	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Gallica	Gallic	k1gInSc2	Gallic
</s>
</p>
<p>
<s>
Plné	plný	k2eAgInPc1d1	plný
texty	text	k1gInPc1	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
</s>
</p>
<p>
<s>
Digitalizované	digitalizovaný	k2eAgInPc1d1	digitalizovaný
překlady	překlad	k1gInPc1	překlad
děl	dělo	k1gNnPc2	dělo
Julese	Julese	k1gFnSc1	Julese
Verna	Verna	k1gFnSc1	Verna
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
Kramerius	Kramerius	k1gMnSc1	Kramerius
NK	NK	kA	NK
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andreas	Andreas	k1gMnSc1	Andreas
Fehrmann	Fehrmann	k1gMnSc1	Fehrmann
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Collection-	Collection-	k1gFnSc7	Collection-
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Klub	klub	k1gInSc1	klub
Julese	Julese	k1gFnSc2	Julese
Vernea	Verne	k1gInSc2	Verne
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
180	[number]	k4	180
let	let	k1gInSc4	let
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
Julesa	Julesa	k1gFnSc1	Julesa
Verna	Verna	k1gFnSc1	Verna
–	–	k?	–
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Portail	Portail	k1gInSc1	Portail
Web	web	k1gInSc1	web
Francophone	Francophon	k1gInSc5	Francophon
sur	sur	k?	sur
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Éditions	Éditions	k1gInSc1	Éditions
Hetzel	Hetzel	k1gFnSc2	Hetzel
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Garmt	Garmt	k1gMnSc1	Garmt
de	de	k?	de
Vries	Vries	k1gMnSc1	Vries
<g/>
'	'	kIx"	'
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
Collection	Collection	k1gInSc1	Collection
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gMnSc5	Vern
<g/>
:	:	kIx,	:
An	An	k1gMnSc6	An
Author	Author	k1gInSc1	Author
Before	Befor	k1gInSc5	Befor
His	his	k1gNnSc6	his
Time	Tim	k1gMnPc4	Tim
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
Collection	Collection	k1gInSc1	Collection
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
Collection	Collection	k1gInSc1	Collection
-	-	kIx~	-
české	český	k2eAgFnSc6d1	Česká
FAQ	FAQ	kA	FAQ
–	–	k?	–
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
Club	club	k1gInSc1	club
–	–	k?	–
chorvatsky	chorvatsky	k6eAd1	chorvatsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
bibliothè	bibliothè	k?	bibliothè
–	–	k?	–
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LEGIE	legie	k1gFnSc1	legie
–	–	k?	–
databáze	databáze	k1gFnSc2	databáze
knih	kniha	k1gFnPc2	kniha
Fantasy	fantas	k1gInPc1	fantas
a	a	k8xC	a
Sci-Fi	scii	k1gFnPc1	sci-fi
–	–	k?	–
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Les	les	k1gInSc1	les
Voyages	Voyages	k1gMnSc1	Voyages
Extraordinaires	Extraordinaires	k1gMnSc1	Extraordinaires
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Les	les	k1gInSc1	les
Voyages	Voyages	k1gMnSc1	Voyages
Extraordinaires	Extraordinaires	k1gMnSc1	Extraordinaires
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Literární	literární	k2eAgFnSc1d1	literární
analýza	analýza	k1gFnSc1	analýza
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Le	Le	k?	Le
musée	muséat	k5eAaPmIp3nS	muséat
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
de	de	k?	de
Nantes	Nantes	k1gInSc1	Nantes
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polskie	Polskie	k1gFnSc1	Polskie
towarzystwo	towarzystwo	k6eAd1	towarzystwo
Juliusza	Juliusz	k1gMnSc2	Juliusz
Vernea	Verneus	k1gMnSc2	Verneus
–	–	k?	–
polsky	polsky	k6eAd1	polsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Works	Works	kA	Works
of	of	k?	of
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
University	universita	k1gFnPc4	universita
of	of	k?	of
Adelaide	Adelaid	k1gInSc5	Adelaid
–	–	k?	–
–	–	k?	–
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Dutch	Dutch	k1gMnSc1	Dutch
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
Society	societa	k1gFnSc2	societa
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ilustrátoři	ilustrátor	k1gMnPc1	ilustrátor
Podivuhodných	podivuhodný	k2eAgFnPc2d1	podivuhodná
cest	cesta	k1gFnPc2	cesta
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
World	World	k1gMnSc1	World
map	mapa	k1gFnPc2	mapa
of	of	k?	of
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
editions	editions	k1gInSc1	editions
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc2	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Jules	Jules	k1gInSc1	Jules
Verne	Vern	k1gInSc5	Vern
mobile	mobile	k1gNnSc4	mobile
ebooks	ebooksit	k5eAaPmRp2nS	ebooksit
</s>
</p>
