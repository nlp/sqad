<s>
Kosmos	kosmos	k1gInSc1
21	#num#	k4
</s>
<s>
Kosmos	kosmos	k1gInSc4
21	#num#	k4
<g/>
Jiné	jiný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
3	#num#	k4
<g/>
MV-	MV-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
1	#num#	k4
COSPAR	COSPAR	kA
</s>
<s>
1963-044A	1963-044A	k4
Start	start	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1963	#num#	k4
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
UT	UT	kA
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Bajkonur	Bajkonura	k1gFnPc2
Nosná	nosný	k2eAgFnSc1d1
raketa	raketa	k1gFnSc1
</s>
<s>
Molnija	Molnija	k6eAd1
Typ	typ	k1gInSc1
oběžné	oběžný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
</s>
<s>
LEO	Leo	k1gMnSc1
Zánik	zánik	k1gInSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc4
1963	#num#	k4
Výrobce	výrobce	k1gMnSc1
</s>
<s>
OKB-1	OKB-1	k4
Druh	druh	k1gInSc1
</s>
<s>
planetární	planetární	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
6500	#num#	k4
kg	kg	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Parametry	parametr	k1gInPc1
dráhy	dráha	k1gFnSc2
</s>
<s>
Apogeum	apogeum	k1gNnSc1
<g/>
216	#num#	k4
km	km	kA
</s>
<s>
Perigeum	perigeum	k1gNnSc1
<g/>
182	#num#	k4
km	km	kA
</s>
<s>
Sklon	sklon	k1gInSc1
dráhy	dráha	k1gFnSc2
<g/>
64,83	64,83	k4
°	°	k?
</s>
<s>
Doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
<g/>
88,5	88,5	k4
minut	minuta	k1gFnPc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kosmos	kosmos	k1gInSc1
21	#num#	k4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
<g/>
21	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgNnSc4d1
označení	označení	k1gNnSc4
pro	pro	k7c4
sovětskou	sovětský	k2eAgFnSc4d1
vesmírnou	vesmírný	k2eAgFnSc4d1
sondu	sonda	k1gFnSc4
typu	typ	k1gInSc2
3	#num#	kA
<g/>
MV-	MV-	kA
<g/>
1	#num#	kA
<g/>
,	,	kIx,
výrobní	výrobní	k2eAgNnSc4d1
číslo	číslo	k1gNnSc1
1	#num#	k4
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
3М	3М	kA
№	№	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sondy	sonda	k1gFnPc4
řady	řada	k1gFnSc2
3MV-1	3MV-1	k4
až	až	k9
-4	-4	k4
byly	být	k5eAaImAgFnP
určeny	určit	k5eAaPmNgInP
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
Venuše	Venuše	k1gFnSc2
a	a	k8xC
Marsu	Mars	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmos	kosmos	k1gInSc1
21	#num#	k4
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
vypuštěná	vypuštěný	k2eAgFnSc1d1
sonda	sonda	k1gFnSc1
svého	svůj	k3xOyFgInSc2
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
letu	let	k1gInSc2
byla	být	k5eAaImAgFnS
prověrka	prověrka	k1gFnSc1
systémů	systém	k1gInPc2
sondy	sonda	k1gFnSc2
při	při	k7c6
letu	let	k1gInSc6
v	v	k7c6
meziplanetárním	meziplanetární	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
průlet	průlet	k1gInSc1
nad	nad	k7c7
odvrácenou	odvrácený	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Měsíce	měsíc	k1gInSc2
a	a	k8xC
pořízení	pořízení	k1gNnSc6
jejích	její	k3xOp3gFnPc2
vysoce	vysoce	k6eAd1
kvalitních	kvalitní	k2eAgFnPc2d1
fotografií	fotografia	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sonda	sonda	k1gFnSc1
byla	být	k5eAaImAgFnS
vynesena	vynést	k5eAaPmNgFnS
raketou	raketa	k1gFnSc7
Molnija	Molnij	k1gInSc2
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1963	#num#	k4
v	v	k7c4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
UT	UT	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
důsledku	důsledek	k1gInSc6
chybné	chybný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
rakety	raketa	k1gFnSc2
(	(	kIx(
<g/>
bloku	blok	k1gInSc2
L	L	kA
<g/>
)	)	kIx)
sonda	sonda	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
na	na	k7c6
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
Země	zem	k1gFnSc2
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1963	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
zanikla	zaniknout	k5eAaPmAgFnS
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Spaec	k1gInSc2
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
družic	družice	k1gFnPc2
a	a	k8xC
kosmických	kosmický	k2eAgFnPc2d1
sond	sonda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2007-12-20	2007-12-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
1963-044A	1963-044A	k4
(	(	kIx(
<g/>
α	α	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
Kosmos	kosmos	k1gInSc1
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Л	Л	k?
<g/>
,	,	kIx,
К	К	k?
Р	Р	k?
<g/>
.	.	kIx.
Н	Н	k?
Mapc	Mapc	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
А	А	k?
м	м	k?
с	с	k?
с	с	k?
3	#num#	k4
<g/>
М	М	k?
<g/>
.	.	kIx.
Н	Н	k?
к	к	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1996	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1561	#num#	k4
<g/>
-	-	kIx~
<g/>
1078	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Program	program	k1gInSc1
Veněra	Veněra	k1gFnSc1
</s>
<s>
1VA	1VA	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
1	#num#	k4
•	•	k?
Sputnik	sputnik	k1gInSc1
19	#num#	k4
•	•	k?
Sputnik	sputnik	k1gInSc1
20	#num#	k4
•	•	k?
Sputnik	sputnik	k1gInSc1
21	#num#	k4
•	•	k?
Kosmos	kosmos	k1gInSc1
21	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
1964A	1964A	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
1964B	1964B	k4
•	•	k?
Kosmos	kosmos	k1gInSc1
27	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
2	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
3	#num#	k4
•	•	k?
Kosmos	kosmos	k1gInSc1
96	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
1965A	1965A	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
4	#num#	k4
•	•	k?
Kosmos	kosmos	k1gInSc1
167	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
5	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
6	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
7	#num#	k4
•	•	k?
Kosmos	kosmos	k1gInSc1
359	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
8	#num#	k4
•	•	k?
Kosmos	kosmos	k1gInSc1
482	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
9	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
10	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
11	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
12	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
13	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
14	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
15	#num#	k4
•	•	k?
Veněra	Veněra	k1gFnSc1
16	#num#	k4
•	•	k?
Veněra-D	Veněra-D	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
