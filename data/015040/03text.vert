<s>
Starověká	starověký	k2eAgFnSc1d1
římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
římských	římský	k2eAgMnPc2d1
legionářů	legionář	k1gMnPc2
s	s	k7c7
plátovou	plátový	k2eAgFnSc7d1
zbrojí	zbroj	k1gFnSc7
(	(	kIx(
<g/>
lorica	loric	k2eAgFnSc1d1
segmentata	segmentata	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1
římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgFnSc1d1
bojová	bojový	k2eAgFnSc1d1
složka	složka	k1gFnSc1
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
nejmocnější	mocný	k2eAgFnSc7d3
vojenskou	vojenský	k2eAgFnSc7d1
silou	síla	k1gFnSc7
své	svůj	k3xOyFgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legie	legie	k1gFnSc1
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
porážely	porážet	k5eAaImAgFnP
nepřátele	nepřítel	k1gMnPc4
od	od	k7c2
Británie	Británie	k1gFnSc2
až	až	k9
po	po	k7c6
Mezopotámii	Mezopotámie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
se	se	k3xPyFc4
však	však	k9
stala	stát	k5eAaPmAgFnS
římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
takto	takto	k6eAd1
výkonnou	výkonný	k2eAgFnSc4d1
<g/>
,	,	kIx,
musela	muset	k5eAaImAgFnS
projít	projít	k5eAaPmF
celou	celý	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
reforem	reforma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
začátku	začátek	k1gInSc2
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
byl	být	k5eAaImAgInS
Řím	Řím	k1gInSc1
silně	silně	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgInS
keltskou	keltský	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
a	a	k8xC
podle	podle	k7c2
toho	ten	k3xDgInSc2
římští	římský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
a	a	k8xC
byli	být	k5eAaImAgMnP
také	také	k6eAd1
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
raném	raný	k2eAgNnSc6d1
období	období	k1gNnSc6
republiky	republika	k1gFnSc2
bojovali	bojovat	k5eAaImAgMnP
římští	římský	k2eAgMnPc1d1
pěšáci	pěšák	k1gMnPc1
ve	v	k7c6
falangách	falanga	k1gFnPc6
těžkooděnců	těžkooděnec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
ve	v	k7c6
čtvrtém	čtvrtý	k4xOgNnSc6
století	století	k1gNnSc6
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byly	být	k5eAaImAgFnP
zavedeny	zaveden	k2eAgFnPc1d1
legie	legie	k1gFnPc1
a	a	k8xC
manipuly	manipul	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Velkým	velký	k2eAgMnSc7d1
reformátorem	reformátor	k1gMnSc7
vojska	vojsko	k1gNnSc2
byl	být	k5eAaImAgMnS
Gaius	Gaius	k1gMnSc1
Marius	Marius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
udělal	udělat	k5eAaPmAgInS
z	z	k7c2
legií	legie	k1gFnPc2
profesionální	profesionální	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc4
vojáci	voják	k1gMnPc1
sloužili	sloužit	k5eAaImAgMnP
ve	v	k7c6
vojsku	vojsko	k1gNnSc6
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladem	klad	k1gInSc7
těchto	tento	k3xDgFnPc2
reforem	reforma	k1gFnPc2
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
římský	římský	k2eAgMnSc1d1
pěšák	pěšák	k1gMnSc1
neměl	mít	k5eNaImAgMnS
konkurenci	konkurence	k1gFnSc4
<g/>
,	,	kIx,
záporem	zápor	k1gInSc7
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
římští	římský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
stále	stále	k6eAd1
více	hodně	k6eAd2
upínali	upínat	k5eAaImAgMnP
ke	k	k7c3
svému	svůj	k1gMnSc3
veliteli	velitel	k1gMnSc3
než	než	k8xS
k	k	k7c3
zákonům	zákon	k1gInPc3
republiky	republika	k1gFnSc2
a	a	k8xC
senátu	senát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
také	také	k9
jeden	jeden	k4xCgInSc1
z	z	k7c2
důvodů	důvod	k1gInPc2
<g/>
,	,	kIx,
proč	proč	k6eAd1
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
republika	republika	k1gFnSc1
a	a	k8xC
vzniklo	vzniknout	k5eAaPmAgNnS
císařství	císařství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římští	římský	k2eAgMnPc1d1
pěšáci	pěšák	k1gMnPc1
byli	být	k5eAaImAgMnP
výborně	výborně	k6eAd1
vyzbrojeni	vyzbrojen	k2eAgMnPc1d1
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
tvrdý	tvrdý	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
byl	být	k5eAaImAgInS
pověstný	pověstný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgMnSc3
v	v	k7c6
pozdním	pozdní	k2eAgNnSc6d1
období	období	k1gNnSc6
republiky	republika	k1gFnSc2
a	a	k8xC
raném	raný	k2eAgNnSc6d1
období	období	k1gNnSc6
císařství	císařství	k1gNnSc2
disponoval	disponovat	k5eAaBmAgInS
Řím	Řím	k1gInSc1
nejlepším	dobrý	k2eAgNnSc7d3
vojskem	vojsko	k1gNnSc7
starověku	starověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
pozdního	pozdní	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
nastal	nastat	k5eAaPmAgInS
úpadek	úpadek	k1gInSc1
říše	říš	k1gFnSc2
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gInSc7
i	i	k9
úpadek	úpadek	k1gInSc1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
římského	římský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
byli	být	k5eAaImAgMnP
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
dál	daleko	k6eAd2
více	hodně	k6eAd2
verbováni	verbován	k2eAgMnPc1d1
barbaři	barbar	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
konec	konec	k1gInSc4
klasické	klasický	k2eAgFnSc2d1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pěchota	pěchota	k1gFnSc1
rané	raný	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Keltský	keltský	k2eAgInSc4d1
a	a	k8xC
etruský	etruský	k2eAgInSc4d1
vliv	vliv	k1gInSc4
</s>
<s>
Kmeny	kmen	k1gInPc1
vnitrozemské	vnitrozemský	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
byly	být	k5eAaImAgFnP
až	až	k9
do	do	k7c2
šestého	šestý	k4xOgNnSc2
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
velice	velice	k6eAd1
ovlivněny	ovlivněn	k2eAgInPc1d1
keltskou	keltský	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
vliv	vliv	k1gInSc1
se	se	k3xPyFc4
projevoval	projevovat	k5eAaImAgInS
v	v	k7c6
mnoha	mnoho	k4c6
oblastech	oblast	k1gFnPc6
římské	římský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nejvíce	hodně	k6eAd3,k6eAd1
bylo	být	k5eAaImAgNnS
ovlivněno	ovlivnit	k5eAaPmNgNnS
římské	římský	k2eAgNnSc1d1
vojenství	vojenství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
důkazy	důkaz	k1gInPc1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
římském	římský	k2eAgNnSc6d1
vojenství	vojenství	k1gNnSc6
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
existovaly	existovat	k5eAaImAgInP
elitní	elitní	k2eAgInPc1d1
řády	řád	k1gInPc1
vítězů	vítěz	k1gMnPc2
a	a	k8xC
válečných	válečný	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
zasvěceni	zasvětit	k5eAaPmNgMnP
bohu	bůh	k1gMnSc6
války	válka	k1gFnSc2
Martovi	Martův	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Výzbroj	výzbroj	k1gFnSc1
a	a	k8xC
výstroj	výstroj	k1gFnSc1
římských	římský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
také	také	k9
nesla	nést	k5eAaImAgFnS
značný	značný	k2eAgInSc4d1
keltský	keltský	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římské	římský	k2eAgInPc1d1
meče	meč	k1gInPc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
keltské	keltský	k2eAgFnPc1d1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
dlouhé	dlouhý	k2eAgFnPc1d1
přibližně	přibližně	k6eAd1
70	#num#	k4
centimetrů	centimetr	k1gInPc2
a	a	k8xC
sloužily	sloužit	k5eAaImAgFnP
spíše	spíše	k9
k	k	k7c3
sekání	sekání	k1gNnSc3
než	než	k8xS
k	k	k7c3
bodání	bodání	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
římské	římský	k2eAgFnPc4d1
přilbice	přilbice	k1gFnPc4
<g/>
,	,	kIx,
štíty	štít	k1gInPc4
a	a	k8xC
brnění	brnění	k1gNnSc4
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
napodobeniny	napodobenina	k1gFnPc4
keltských	keltský	k2eAgInPc2d1
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
byly	být	k5eAaImAgInP
zřejmě	zřejmě	k6eAd1
složeny	složit	k5eAaPmNgInP
ze	z	k7c2
skupin	skupina	k1gFnPc2
válečníků	válečník	k1gMnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
velel	velet	k5eAaImAgInS
vždy	vždy	k6eAd1
nějaký	nějaký	k3yIgMnSc1
šlechtic	šlechtic	k1gMnSc1
nebo	nebo	k8xC
výjimečně	výjimečně	k6eAd1
zkušený	zkušený	k2eAgMnSc1d1
a	a	k8xC
udatný	udatný	k2eAgMnSc1d1
válečník	válečník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
armády	armáda	k1gFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
úkol	úkol	k1gInSc4
bránit	bránit	k5eAaImF
své	svůj	k3xOyFgNnSc4
město	město	k1gNnSc4
před	před	k7c7
okolními	okolní	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
a	a	k8xC
zároveň	zároveň	k6eAd1
posouvat	posouvat	k5eAaImF
hranice	hranice	k1gFnPc4
na	na	k7c4
úkor	úkor	k1gInSc4
jiných	jiný	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečníci	válečník	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
zřejmě	zřejmě	k6eAd1
jednotlivě	jednotlivě	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
jejich	jejich	k3xOp3gInSc1
úspěch	úspěch	k1gInSc1
byl	být	k5eAaImAgInS
často	často	k6eAd1
ku	k	k7c3
prospěchu	prospěch	k1gInSc3
většině	většina	k1gFnSc6
<g/>
,	,	kIx,
osobní	osobní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
a	a	k8xC
sláva	sláva	k1gFnSc1
byly	být	k5eAaImAgInP
pro	pro	k7c4
mnohé	mnohý	k2eAgFnPc4d1
z	z	k7c2
nich	on	k3xPp3gFnPc2
motivací	motivace	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
organizace	organizace	k1gFnSc1
byla	být	k5eAaImAgFnS
zastaralá	zastaralý	k2eAgFnSc1d1
a	a	k8xC
Řím	Řím	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
netěšil	těšit	k5eNaImAgMnS
velké	velký	k2eAgFnSc3d1
úctě	úcta	k1gFnSc3
a	a	k8xC
ani	ani	k8xC
nedosahoval	dosahovat	k5eNaImAgMnS
velkých	velký	k2eAgInPc2d1
územních	územní	k2eAgInPc2d1
zisků	zisk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
600	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
si	se	k3xPyFc3
Řím	Řím	k1gInSc4
podrobili	podrobit	k5eAaPmAgMnP
Etruskové	Etrusk	k1gMnPc1
a	a	k8xC
provedli	provést	k5eAaPmAgMnP
zde	zde	k6eAd1
vojenské	vojenský	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Etruskové	Etrusk	k1gMnPc1
byli	být	k5eAaImAgMnP
značně	značně	k6eAd1
ovlivněni	ovlivnit	k5eAaPmNgMnP
řeckou	řecký	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
a	a	k8xC
používali	používat	k5eAaImAgMnP
falangu	falanga	k1gFnSc4
řeckého	řecký	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
zavedli	zavést	k5eAaPmAgMnP
i	i	k9
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etruskové	Etrusk	k1gMnPc1
také	také	k9
zavedli	zavést	k5eAaPmAgMnP
všeobecnou	všeobecný	k2eAgFnSc4d1
brannou	branný	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Branná	branný	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
měla	mít	k5eAaImAgFnS
svůj	svůj	k3xOyFgInSc4
počátek	počátek	k1gInSc4
v	v	k7c6
době	doba	k1gFnSc6
vlády	vláda	k1gFnSc2
etruského	etruský	k2eAgNnSc2d1
král	král	k1gMnSc1
Servia	Servius	k1gMnSc4
Tullia	Tullius	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
uspořádal	uspořádat	k5eAaPmAgMnS
římskou	římský	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
do	do	k7c2
tříd	třída	k1gFnPc2
odpovídajících	odpovídající	k2eAgNnPc2d1
bohatství	bohatství	k1gNnPc2
a	a	k8xC
sociálnímu	sociální	k2eAgNnSc3d1
postavení	postavení	k1gNnSc3
jejich	jejich	k3xOp3gMnPc2
příslušníků	příslušník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lid	lid	k1gInSc1
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
tříd	třída	k1gFnPc2
známých	známý	k2eAgFnPc2d1
jako	jako	k9
centurie	centurie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
ze	z	k7c2
tříd	třída	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
války	válka	k1gFnSc2
sama	sám	k3xTgMnSc4
vybavovala	vybavovat	k5eAaImAgFnS
v	v	k7c6
rozsahu	rozsah	k1gInSc6
odpovídajícímu	odpovídající	k2eAgNnSc3d1
jejich	jejich	k3xOp3gFnPc3
finančním	finanční	k2eAgFnPc3d1
možnostem	možnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušníci	příslušník	k1gMnPc1
nejmajetnější	majetný	k2eAgFnSc2d3
třídy	třída	k1gFnSc2
si	se	k3xPyFc3
pořizovali	pořizovat	k5eAaImAgMnP
kompletní	kompletní	k2eAgNnSc4d1
hoplítské	hoplítský	k2eAgNnSc4d1
brnění	brnění	k1gNnSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejchudší	chudý	k2eAgFnPc4d3
třídy	třída	k1gFnPc4
<g/>
,	,	kIx,
ozbrojené	ozbrojený	k2eAgInPc4d1
praky	prak	k1gInPc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
využívaly	využívat	k5eAaImAgFnP,k5eAaPmAgFnP
jako	jako	k8xC,k8xS
prakovníci	prakovník	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Styl	styl	k1gInSc1
boje	boj	k1gInSc2
byl	být	k5eAaImAgInS
stejný	stejný	k2eAgInSc1d1
jako	jako	k9
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falanga	falanga	k1gFnSc1
byla	být	k5eAaImAgFnS
uspořádaná	uspořádaný	k2eAgFnSc1d1
do	do	k7c2
osmi	osm	k4xCc2
řad	řada	k1gFnPc2
a	a	k8xC
falangisté	falangista	k1gMnPc1
byli	být	k5eAaImAgMnP
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP
kopím	kopí	k1gNnSc7
<g/>
,	,	kIx,
štítem	štít	k1gInSc7
<g/>
,	,	kIx,
helmicí	helmice	k1gFnSc7
a	a	k8xC
brněním	brnění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Římané	Říman	k1gMnPc1
vypudili	vypudit	k5eAaPmAgMnP
Etrusky	etrusky	k6eAd1
koncem	koncem	k7c2
šestého	šestý	k4xOgNnSc2
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
ale	ale	k8xC
falangu	falanga	k1gFnSc4
řeckého	řecký	k2eAgInSc2d1
stylu	styl	k1gInSc2
si	se	k3xPyFc3
zachovali	zachovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falanga	falanga	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
vůbec	vůbec	k9
nehodila	hodit	k5eNaPmAgFnS
k	k	k7c3
boji	boj	k1gInSc3
v	v	k7c6
hornatých	hornatý	k2eAgFnPc6d1
Apeninách	Apeniny	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římané	Říman	k1gMnPc1
proto	proto	k8xC
utrpěli	utrpět	k5eAaPmAgMnP
několik	několik	k4yIc4
těžkých	těžký	k2eAgFnPc2d1
porážek	porážka	k1gFnPc2
od	od	k7c2
Galů	Gal	k1gMnPc2
a	a	k8xC
Samnitů	Samnit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
(	(	kIx(
<g/>
koncem	koncem	k7c2
čtvrtého	čtvrtý	k4xOgNnSc2
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
přestali	přestat	k5eAaPmAgMnP
Římané	Říman	k1gMnPc1
falangu	falanga	k1gFnSc4
používat	používat	k5eAaImF
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Manipulové	Manipulový	k2eAgNnSc1d1
válečnictví	válečnictví	k1gNnSc1
</s>
<s>
Zatímco	zatímco	k8xS
válčení	válčení	k1gNnSc2
taktikou	taktika	k1gFnSc7
falangy	falanga	k1gFnSc2
dominovalo	dominovat	k5eAaImAgNnS
Latiu	Latium	k1gNnSc3
od	od	k7c2
poloviny	polovina	k1gFnSc2
šestého	šestý	k4xOgNnSc2
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
do	do	k7c2
století	století	k1gNnSc2
čtvrtého	čtvrtý	k4xOgMnSc2
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6
čtvrtého	čtvrtý	k4xOgNnSc2
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
od	od	k7c2
toho	ten	k3xDgNnSc2
upustilo	upustit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
falangy	falanga	k1gFnSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
používat	používat	k5eAaImF
takzvané	takzvaný	k2eAgInPc4d1
manipuly	manipul	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manipulová	Manipulový	k2eAgFnSc1d1
formace	formace	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
určitého	určitý	k2eAgInSc2d1
počtu	počet	k1gInSc2
linií	linie	k1gFnSc7
pěchoty	pěchota	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
každá	každý	k3xTgFnSc1
linie	linie	k1gFnSc1
byla	být	k5eAaImAgFnS
tvořena	tvořen	k2eAgFnSc1d1
bloky	blok	k1gInPc4
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
bloky	blok	k1gInPc7
zůstával	zůstávat	k5eAaImAgInS
široký	široký	k2eAgInSc1d1
prostor	prostor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
umožňoval	umožňovat	k5eAaImAgInS
postupovat	postupovat	k5eAaImF
vpřed	vpřed	k6eAd1
nebo	nebo	k8xC
se	se	k3xPyFc4
stahovat	stahovat	k5eAaImF
nezávisle	závisle	k6eNd1
na	na	k7c6
pohybu	pohyb	k1gInSc6
bojové	bojový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
jako	jako	k8xS,k8xC
celku	celek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
manipulů	manipul	k1gInPc2
tvořilo	tvořit	k5eAaImAgNnS
římskou	římský	k2eAgFnSc4d1
legii	legie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
linie	linie	k1gFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
patnácti	patnáct	k4xCc2
manipulů	manipul	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nich	on	k3xPp3gInPc6
bojovali	bojovat	k5eAaImAgMnP
hastati	hastat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
mladíci	mladík	k1gMnPc1
vyzbrojení	vyzbrojení	k1gNnPc2
kopími	kopí	k1gNnPc7
<g/>
,	,	kIx,
později	pozdě	k6eAd2
meči	meč	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Byli	být	k5eAaImAgMnP
pouze	pouze	k6eAd1
lehce	lehko	k6eAd1
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP
a	a	k8xC
lehce	lehko	k6eAd1
obrněni	obrněn	k2eAgMnPc1d1
<g/>
,	,	kIx,
proto	proto	k8xC
jejich	jejich	k3xOp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
předností	přednost	k1gFnSc7
byla	být	k5eAaImAgFnS
rychlost	rychlost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nimi	on	k3xPp3gMnPc7
následovalo	následovat	k5eAaImAgNnS
dalších	další	k2eAgNnPc2d1
patnáct	patnáct	k4xCc1
manipulů	manipul	k1gInPc2
složených	složený	k2eAgInPc2d1
z	z	k7c2
principů	princip	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
výborně	výborně	k6eAd1
vyzbrojení	vyzbrojený	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
sil	síla	k1gFnPc2
bojovali	bojovat	k5eAaImAgMnP
meči	meč	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgInPc6d1
manipulech	manipul	k1gInPc6
byli	být	k5eAaImAgMnP
triariové	triariový	k2eAgFnPc1d1
<g/>
,	,	kIx,
veteráni	veterán	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	s	k7c7
vzhledem	vzhled	k1gInSc7
k	k	k7c3
vyššímu	vysoký	k2eAgNnSc3d2
stáří	stáří	k1gNnSc3
pohybovali	pohybovat	k5eAaImAgMnP
pomaleji	pomale	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
dokázali	dokázat	k5eAaPmAgMnP
využívat	využívat	k5eAaPmF,k5eAaImF
svých	svůj	k3xOyFgFnPc2
zkušeností	zkušenost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojovali	bojovat	k5eAaImAgMnP
kopími	kopí	k1gNnPc7
a	a	k8xC
fungovali	fungovat	k5eAaImAgMnP
jako	jako	k9
pevné	pevný	k2eAgNnSc4d1
těžiště	těžiště	k1gNnSc4
legie	legie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
uspořádání	uspořádání	k1gNnSc1
se	se	k3xPyFc4
nepříteli	nepřítel	k1gMnSc3
jevilo	jevit	k5eAaImAgNnS
jako	jako	k8xC,k8xS
šachovnicový	šachovnicový	k2eAgInSc1d1
vzor	vzor	k1gInSc1
připomínající	připomínající	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
zploštělých	zploštělý	k2eAgInPc2d1
klínů	klín	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
jako	jako	k8xS,k8xC
první	první	k4xOgMnPc1
vstupovali	vstupovat	k5eAaImAgMnP
do	do	k7c2
boje	boj	k1gInSc2
hastati	hastat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
byli	být	k5eAaImAgMnP
odraženi	odrazit	k5eAaPmNgMnP
<g/>
,	,	kIx,
stáhli	stáhnout	k5eAaPmAgMnP
se	se	k3xPyFc4
a	a	k8xC
prošli	projít	k5eAaPmAgMnP
mezerami	mezera	k1gFnPc7
mezi	mezi	k7c7
principy	princip	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
pustili	pustit	k5eAaPmAgMnP
do	do	k7c2
boje	boj	k1gInSc2
principové	principový	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
i	i	k9
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
byli	být	k5eAaImAgMnP
odraženi	odrazit	k5eAaPmNgMnP
<g/>
,	,	kIx,
ustoupili	ustoupit	k5eAaPmAgMnP
z	z	k7c2
přední	přední	k2eAgFnSc2d1
linie	linie	k1gFnSc2
k	k	k7c3
triariům	triarius	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
triariové	triariový	k2eAgInPc1d1
propustili	propustit	k5eAaPmAgMnP
hastaty	hastata	k1gFnPc4
a	a	k8xC
principy	princip	k1gInPc4
za	za	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
uzavřeli	uzavřít	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
linie	linie	k1gFnPc4
a	a	k8xC
vytvořili	vytvořit	k5eAaPmAgMnP
z	z	k7c2
kopí	kopí	k1gNnSc2
nepropustnou	propustný	k2eNgFnSc4d1
zeď	zeď	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reformy	reforma	k1gFnPc1
a	a	k8xC
výzbroj	výzbroj	k1gFnSc1
římského	římský	k2eAgMnSc2d1
pěšáka	pěšák	k1gMnSc2
</s>
<s>
Gladius	Gladius	k1gMnSc1
</s>
<s>
Římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
prošla	projít	k5eAaPmAgFnS
dalšími	další	k2eAgFnPc7d1
<g/>
,	,	kIx,
leč	leč	k8xC,k8xS
nijak	nijak	k6eAd1
zásadními	zásadní	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
a	a	k8xC
reformami	reforma	k1gFnPc7
během	během	k7c2
druhé	druhý	k4xOgFnSc2
punské	punský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Způsobil	způsobit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
nedostatek	nedostatek	k1gInSc4
vhodných	vhodný	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
by	by	kYmCp3nP
bránili	bránit	k5eAaImAgMnP
Řím	Řím	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
katastrofální	katastrofální	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
u	u	k7c2
Kann	Kanny	k1gFnPc2
(	(	kIx(
<g/>
216	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
do	do	k7c2
římského	římský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
povoláváni	povolávat	k5eAaImNgMnP
k	k	k7c3
vojsku	vojsko	k1gNnSc3
rolníci	rolník	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
otroci	otrok	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
392	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
dostávali	dostávat	k5eAaImAgMnP
pěšáci	pěšák	k1gMnPc1
pravidelný	pravidelný	k2eAgInSc4d1
plat	plat	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgInS
první	první	k4xOgInSc4
krok	krok	k1gInSc4
k	k	k7c3
profesionální	profesionální	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
byla	být	k5eAaImAgFnS
stále	stále	k6eAd1
složena	složit	k5eAaPmNgFnS
z	z	k7c2
milice	milice	k1gFnSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
už	už	k6eAd1
měla	mít	k5eAaImAgFnS
řadu	řada	k1gFnSc4
prvků	prvek	k1gInPc2
profesionální	profesionální	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
každého	každý	k3xTgMnSc2
odvedence	odvedenec	k1gMnSc2
se	se	k3xPyFc4
očekávalo	očekávat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
opatří	opatřit	k5eAaPmIp3nP
vlastní	vlastní	k2eAgFnSc4d1
zbroj	zbroj	k1gFnSc4
a	a	k8xC
bude	být	k5eAaImBp3nS
sloužit	sloužit	k5eAaImF
po	po	k7c4
dobu	doba	k1gFnSc4
16	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
době	doba	k1gFnSc6
války	válka	k1gFnSc2
až	až	k9
20	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
taktickou	taktický	k2eAgFnSc7d1
formací	formace	k1gFnSc7
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byla	být	k5eAaImAgFnS
legie	legie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legie	legie	k1gFnSc1
čítala	čítat	k5eAaImAgFnS
4200	#num#	k4
až	až	k9
5000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
centurií	centurie	k1gFnPc2
po	po	k7c6
80	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
mužích	muž	k1gMnPc6
a	a	k8xC
dvě	dva	k4xCgFnPc1
centurie	centurie	k1gFnPc1
tvořily	tvořit	k5eAaImAgFnP
jeden	jeden	k4xCgInSc4
manipul	manipul	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
pěšáků	pěšák	k1gMnPc2
patřila	patřit	k5eAaImAgFnS
k	k	k7c3
takzvané	takzvaný	k2eAgFnSc3d1
těžké	těžký	k2eAgFnSc3d1
pěchotě	pěchota	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
římské	římský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
sloužili	sloužit	k5eAaImAgMnP
i	i	k9
příslušníci	příslušník	k1gMnPc1
lehkých	lehký	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
tak	tak	k6eAd1
nízkém	nízký	k2eAgInSc6d1
počtu	počet	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
byli	být	k5eAaImAgMnP
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
zbytečné	zbytečný	k2eAgMnPc4d1
(	(	kIx(
<g/>
nedostatek	nedostatek	k1gInSc1
jízdy	jízda	k1gFnSc2
a	a	k8xC
lehké	lehký	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
se	se	k3xPyFc4
plně	plně	k6eAd1
projevil	projevit	k5eAaPmAgInS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kann	Kanny	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Každý	každý	k3xTgMnSc1
římský	římský	k2eAgMnSc1d1
pěšák	pěšák	k1gMnSc1
používal	používat	k5eAaImAgMnS
k	k	k7c3
ochraně	ochrana	k1gFnSc3
štít	štít	k1gInSc1
scutum	scutum	k1gNnSc1
a	a	k8xC
byl	být	k5eAaImAgMnS
vybaven	vybavit	k5eAaPmNgMnS
párem	pár	k1gInSc7
oštěpů	oštěp	k1gInPc2
zvaných	zvaný	k2eAgInPc2d1
pilum	pilum	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Scutum	Scutum	k1gNnSc1
<g/>
,	,	kIx,
vyrobené	vyrobený	k2eAgNnSc1d1
z	z	k7c2
vrstveného	vrstvený	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
a	a	k8xC
pokryté	pokrytý	k2eAgFnSc6d1
látkou	látka	k1gFnSc7
či	či	k8xC
kůží	kůže	k1gFnSc7
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
nejspíše	nejspíše	k9
keltského	keltský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štít	štít	k1gInSc1
se	se	k3xPyFc4
držel	držet	k5eAaImAgInS
za	za	k7c4
jednoduchou	jednoduchý	k2eAgFnSc4d1
rukojeť	rukojeť	k1gFnSc4
umístěnou	umístěný	k2eAgFnSc4d1
za	za	k7c7
středovou	středový	k2eAgFnSc7d1
puklicí	puklice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
boji	boj	k1gInSc3
s	s	k7c7
mečem	meč	k1gInSc7
a	a	k8xC
k	k	k7c3
vrhání	vrhání	k1gNnSc3
oštěpem	oštěp	k1gInSc7
se	se	k3xPyFc4
hodil	hodit	k5eAaImAgInS,k5eAaPmAgInS
lépe	dobře	k6eAd2
než	než	k8xS
řecký	řecký	k2eAgInSc1d1
štít	štít	k1gInSc1
hoplon	hoplon	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Velkou	velký	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
pěšáků	pěšák	k1gMnPc2
bylo	být	k5eAaImAgNnS
využíváno	využívat	k5eAaImNgNnS,k5eAaPmNgNnS
kroužkové	kroužkový	k2eAgNnSc1d1
brnění	brnění	k1gNnSc1
a	a	k8xC
příležitostně	příležitostně	k6eAd1
pancíř	pancíř	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helmy	helma	k1gFnPc4
byly	být	k5eAaImAgFnP
stále	stále	k6eAd1
kopiemi	kopie	k1gFnPc7
keltských	keltský	k2eAgInPc2d1
a	a	k8xC
řeckých	řecký	k2eAgInPc2d1
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíc	nejvíc	k6eAd1,k6eAd3
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
helma	helma	k1gFnSc1
montefortino	montefortin	k2eAgNnSc1d1
<g/>
,	,	kIx,
vylepšená	vylepšený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
keltské	keltský	k2eAgFnSc2d1
helmice	helmice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oštěpy	oštěp	k1gInPc4
měli	mít	k5eAaImAgMnP
dvojího	dvojí	k4xRgMnSc2
druhu	druh	k1gInSc2
-	-	kIx~
silné	silný	k2eAgFnPc1d1
a	a	k8xC
slabé	slabý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
protivník	protivník	k1gMnSc1
přiblížil	přiblížit	k5eAaPmAgMnS
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
legionáři	legionář	k1gMnPc1
vrhli	vrhnout	k5eAaImAgMnP,k5eAaPmAgMnP
svá	svůj	k3xOyFgFnSc1
pila	pila	k1gFnSc1
a	a	k8xC
vyrazili	vyrazit	k5eAaPmAgMnP
do	do	k7c2
útoku	útok	k1gInSc2
(	(	kIx(
<g/>
velice	velice	k6eAd1
účinná	účinný	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
pilum	pilum	k1gNnSc1
bylo	být	k5eAaImAgNnS
vybaveno	vybavit	k5eAaPmNgNnS
zpětnými	zpětný	k2eAgInPc7d1
háčky	háček	k1gInPc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
protivník	protivník	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
oštěp	oštěp	k1gInSc4
vytáhnout	vytáhnout	k5eAaPmF
z	z	k7c2
těla	tělo	k1gNnSc2
či	či	k8xC
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Protivník	protivník	k1gMnSc1
byl	být	k5eAaImAgMnS
po	po	k7c6
zásahu	zásah	k1gInSc6
zpravidla	zpravidla	k6eAd1
vyřazen	vyřadit	k5eAaPmNgMnS
z	z	k7c2
boje	boj	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
se	se	k3xPyFc4
oštěp	oštěp	k1gInSc1
zabodl	zabodnout	k5eAaPmAgInS
do	do	k7c2
štítu	štít	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
zvýšil	zvýšit	k5eAaPmAgInS
jeho	jeho	k3xOp3gFnSc4
váhu	váha	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
dále	daleko	k6eAd2
nepoužitelný	použitelný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
legionářů	legionář	k1gMnPc2
však	však	k9
zůstával	zůstávat	k5eAaImAgInS
meč	meč	k1gInSc1
gladius	gladius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
punské	punský	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
užíval	užívat	k5eAaImAgInS
takzvaný	takzvaný	k2eAgInSc1d1
gladius	gladius	k1gInSc1
hispaniensis	hispaniensis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
vyráběli	vyrábět	k5eAaImAgMnP
zbrojíři	zbrojíř	k1gMnPc1
v	v	k7c6
Hispánii	Hispánie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
z	z	k7c2
vynikajícího	vynikající	k2eAgNnSc2d1
železa	železo	k1gNnSc2
(	(	kIx(
<g/>
z	z	k7c2
Hispánie	Hispánie	k1gFnSc2
pocházelo	pocházet	k5eAaImAgNnS
nejlepší	dobrý	k2eAgNnSc1d3
železo	železo	k1gNnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
měl	mít	k5eAaImAgMnS
nevídané	vídaný	k2eNgNnSc4d1
ostří	ostří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používal	používat	k5eAaImAgMnS
se	se	k3xPyFc4
hlavně	hlavně	k9
k	k	k7c3
bodání	bodání	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
daleko	daleko	k6eAd1
účinnější	účinný	k2eAgMnSc1d2
než	než	k8xS
sekání	sekání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc1
měřil	měřit	k5eAaImAgInS
přibližně	přibližně	k6eAd1
50	#num#	k4
centimetrů	centimetr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojenecké	spojenecký	k2eAgInPc1d1
kontingenty	kontingent	k1gInPc1
a	a	k8xC
žoldnéři	žoldnér	k1gMnPc1
</s>
<s>
Římané	Říman	k1gMnPc1
byli	být	k5eAaImAgMnP
mistři	mistr	k1gMnPc1
v	v	k7c6
získávání	získávání	k1gNnSc6
spojenců	spojenec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
národ	národ	k1gInSc1
nebo	nebo	k8xC
kmen	kmen	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
porazili	porazit	k5eAaPmAgMnP
<g/>
,	,	kIx,
donutili	donutit	k5eAaPmAgMnP
k	k	k7c3
uzavření	uzavření	k1gNnSc3
spojenectví	spojenectví	k1gNnSc2
(	(	kIx(
<g/>
některá	některý	k3yIgNnPc1
města	město	k1gNnPc1
a	a	k8xC
kmeny	kmen	k1gInPc1
vstupovaly	vstupovat	k5eAaImAgFnP
do	do	k7c2
tohoto	tento	k3xDgNnSc2
spojenectví	spojenectví	k1gNnSc2
z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovéto	takovýto	k3xDgNnSc1
spojenectví	spojenectví	k1gNnSc1
bylo	být	k5eAaImAgNnS
výhodné	výhodný	k2eAgNnSc1d1
pro	pro	k7c4
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojence	spojenec	k1gMnSc4
chránila	chránit	k5eAaImAgFnS
římská	římský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
území	území	k1gNnSc6
byly	být	k5eAaImAgFnP
zřizovány	zřizován	k2eAgFnPc1d1
římské	římský	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
posádky	posádka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
Řím	Řím	k1gInSc4
představovali	představovat	k5eAaImAgMnP
spojenci	spojenec	k1gMnPc1
nekonečnou	konečný	k2eNgFnSc4d1
studnici	studnice	k1gFnSc4
lidských	lidský	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
spojenců	spojenec	k1gMnPc2
se	se	k3xPyFc4
očekávalo	očekávat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
poskytnou	poskytnout	k5eAaPmIp3nP
vojáky	voják	k1gMnPc4
do	do	k7c2
jednotek	jednotka	k1gFnPc2
organizovaných	organizovaný	k2eAgFnPc2d1
na	na	k7c6
římských	římský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
podporovaly	podporovat	k5eAaImAgFnP
římské	římský	k2eAgFnPc4d1
legie	legie	k1gFnPc4
v	v	k7c6
boji	boj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavé	zajímavý	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
spojenci	spojenec	k1gMnPc1
nemuseli	muset	k5eNaImAgMnP
hradit	hradit	k5eAaImF
stravu	strava	k1gFnSc4
ani	ani	k8xC
zbraně	zbraň	k1gFnPc4
<g/>
,	,	kIx,
všechno	všechen	k3xTgNnSc4
toto	tento	k3xDgNnSc4
obstarával	obstarávat	k5eAaImAgInS
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
určité	určitý	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
tato	tento	k3xDgNnPc1
spojenectví	spojenectví	k1gNnPc1
stávala	stávat	k5eAaImAgNnP
opravdu	opravdu	k6eAd1
silnými	silný	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hannibal	Hannibal	k1gInSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgInS
skoro	skoro	k6eAd1
jistý	jistý	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Římem	Řím	k1gInSc7
ujařmené	ujařmený	k2eAgInPc4d1
národy	národ	k1gInPc4
se	se	k3xPyFc4
vzbouří	vzbouřit	k5eAaPmIp3nS
<g/>
,	,	kIx,
jakmile	jakmile	k8xS
Římany	Říman	k1gMnPc4
několikrát	několikrát	k6eAd1
porazí	porazit	k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
Římští	římský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
v	v	k7c6
boji	boj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byli	být	k5eAaImAgMnP
Římané	Říman	k1gMnPc1
drtivě	drtivě	k6eAd1
poraženi	porazit	k5eAaPmNgMnP
u	u	k7c2
Kann	Kanny	k1gFnPc2
<g/>
,	,	kIx,
očekával	očekávat	k5eAaImAgInS
Hannibal	Hannibal	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
proběhne	proběhnout	k5eAaPmIp3nS
obrovská	obrovský	k2eAgFnSc1d1
vzpoura	vzpoura	k1gFnSc1
proti	proti	k7c3
Římu	Řím	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
nestalo	stát	k5eNaPmAgNnS
se	se	k3xPyFc4
tak	tak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
Hannibalovi	Hannibal	k1gMnSc3
se	se	k3xPyFc4
přidalo	přidat	k5eAaPmAgNnS
jen	jen	k9
několik	několik	k4yIc1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
byla	být	k5eAaImAgNnP
ihned	ihned	k6eAd1
poražena	poražen	k2eAgNnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
Hannibal	Hannibal	k1gInSc1
byl	být	k5eAaImAgInS
stejně	stejně	k6eAd1
daleko	daleko	k6eAd1
jako	jako	k8xC,k8xS
na	na	k7c6
začátku	začátek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
dokazuje	dokazovat	k5eAaImIp3nS
pevnost	pevnost	k1gFnSc1
římského	římský	k2eAgInSc2d1
spojeneckého	spojenecký	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
různě	různě	k6eAd1
dlouhé	dlouhý	k2eAgFnSc6d1
věrné	věrný	k2eAgFnSc6d1
službě	služba	k1gFnSc6
dostávala	dostávat	k5eAaImAgFnS
spojenecká	spojenecký	k2eAgFnSc1d1
města	město	k1gNnSc2
nový	nový	k2eAgInSc4d1
typ	typ	k1gInSc4
římského	římský	k2eAgNnSc2d1
občanství	občanství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
občanství	občanství	k1gNnSc1
se	se	k3xPyFc4
nazývalo	nazývat	k5eAaImAgNnS
sine	sinus	k1gInSc5
suffragio	suffragio	k1gNnSc4
(	(	kIx(
<g/>
občanství	občanství	k1gNnSc4
bez	bez	k7c2
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nositel	nositel	k1gMnSc1
tohoto	tento	k3xDgNnSc2
občanství	občanství	k1gNnSc2
měl	mít	k5eAaImAgInS
omezená	omezený	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
platit	platit	k5eAaImF
daně	daň	k1gFnPc4
<g/>
,	,	kIx,
nastupoval	nastupovat	k5eAaImAgMnS
na	na	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nemohl	moct	k5eNaImAgMnS
se	se	k3xPyFc4
podílet	podílet	k5eAaImF
na	na	k7c6
římském	římský	k2eAgInSc6d1
politickém	politický	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Řím	Řím	k1gInSc1
také	také	k9
často	často	k6eAd1
zabíral	zabírat	k5eAaImAgInS
půdu	půda	k1gFnSc4
poražených	poražený	k2eAgMnPc2d1
protivníků	protivník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
půdě	půda	k1gFnSc6
se	se	k3xPyFc4
poté	poté	k6eAd1
usadili	usadit	k5eAaPmAgMnP
chudí	chudý	k2eAgMnPc1d1
římští	římský	k2eAgMnPc1d1
rolníci	rolník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
předtím	předtím	k6eAd1
nepodléhali	podléhat	k5eNaImAgMnP
vojenské	vojenský	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
zbohatli	zbohatnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
už	už	k6eAd1
této	tento	k3xDgFnSc2
povinnosti	povinnost	k1gFnSc2
podléhali	podléhat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
vypukla	vypuknout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
všech	všecek	k3xTgNnPc2
spojeneckých	spojenecký	k2eAgNnPc2d1
měst	město	k1gNnPc2
byli	být	k5eAaImAgMnP
posíláni	posílán	k2eAgMnPc1d1
legáti	legát	k1gMnPc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
kolik	kolik	k4yQc4,k4yRc4,k4yIc4
mužů	muž	k1gMnPc2
to	ten	k3xDgNnSc1
či	či	k8xC
ono	onen	k3xDgNnSc1,k3xPp3gNnSc1
město	město	k1gNnSc1
poskytne	poskytnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
218	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
počet	počet	k1gInSc4
spojeneckých	spojenecký	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
činil	činit	k5eAaImAgInS
250	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
a	a	k8xC
26	#num#	k4
000	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
počet	počet	k1gInSc4
spojenců	spojenec	k1gMnPc2
a	a	k8xC
římských	římský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
činil	činit	k5eAaImAgInS
tento	tento	k3xDgInSc1
poměr	poměr	k1gInSc1
skoro	skoro	k6eAd1
vždy	vždy	k6eAd1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojenečtí	spojenecký	k2eAgMnPc1d1
pěšáci	pěšák	k1gMnPc1
byli	být	k5eAaImAgMnP
skoro	skoro	k6eAd1
stejně	stejně	k6eAd1
vycvičeni	vycvičit	k5eAaPmNgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
výzbroj	výzbroj	k1gFnSc4
měli	mít	k5eAaImAgMnP
zpravidla	zpravidla	k6eAd1
o	o	k7c4
něco	něco	k3yInSc4
starší	starý	k2eAgFnPc1d2
než	než	k8xS
římští	římský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
důležitou	důležitý	k2eAgFnSc4d1
složku	složka	k1gFnSc4
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
tvořili	tvořit	k5eAaImAgMnP
žoldnéři	žoldnér	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
byli	být	k5eAaImAgMnP
verbováni	verbovat	k5eAaImNgMnP
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
specifické	specifický	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
Galové	Gal	k1gMnPc1
byli	být	k5eAaImAgMnP
výborní	výborný	k2eAgMnPc1d1
jezdci	jezdec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žoldnéři	žoldnéř	k1gMnPc7
také	také	k6eAd1
byli	být	k5eAaImAgMnP
často	často	k6eAd1
posíláni	posílán	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
dar	dar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hierón	Hierón	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syrakuský	syrakuský	k2eAgInSc4d1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
punské	punský	k2eAgFnSc2d1
války	válka	k1gFnSc2
poslal	poslat	k5eAaPmAgMnS
Římanům	Říman	k1gMnPc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
1,000	1,000	k4
lučištníků	lučištník	k1gMnPc2
a	a	k8xC
prakovníků	prakovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
druhé	druhý	k4xOgFnSc2
punské	punský	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
žoldnéřské	žoldnéřský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
nedílnou	dílný	k2eNgFnSc7d1
části	část	k1gFnSc6
římského	římský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pěchota	pěchota	k1gFnSc1
pozdní	pozdní	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Profesionalizace	profesionalizace	k1gFnSc1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
</s>
<s>
Římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
se	se	k3xPyFc4
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
stále	stále	k6eAd1
více	hodně	k6eAd2
profesionalizovala	profesionalizovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
100	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
pěchota	pěchota	k1gFnSc1
plně	plně	k6eAd1
profesionální	profesionální	k2eAgFnSc7d1
částí	část	k1gFnSc7
armády	armáda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
legie	legie	k1gFnPc1
přeměnily	přeměnit	k5eAaPmAgFnP
ve	v	k7c4
stálé	stálý	k2eAgFnPc4d1
formace	formace	k1gFnPc4
se	s	k7c7
shodným	shodný	k2eAgInSc7d1
počtem	počet	k1gInSc7
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc1
moderních	moderní	k2eAgFnPc2d1
divizí	divize	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Legie	legie	k1gFnSc1
se	se	k3xPyFc4
stále	stále	k6eAd1
dělila	dělit	k5eAaImAgFnS
na	na	k7c4
do	do	k7c2
tří	tři	k4xCgFnPc2
řad	řada	k1gFnPc2
nazývaných	nazývaný	k2eAgFnPc2d1
hastati	hastat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
principes	principes	k1gInSc4
a	a	k8xC
triarii	triarie	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
tyto	tento	k3xDgFnPc1
části	část	k1gFnPc1
vojska	vojsko	k1gNnSc2
se	se	k3xPyFc4
již	již	k6eAd1
nelišily	lišit	k5eNaImAgFnP
výcvikem	výcvik	k1gInSc7
<g/>
,	,	kIx,
výstrojí	výstroj	k1gFnSc7
<g/>
,	,	kIx,
společenským	společenský	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
ani	ani	k8xC
kvalitou	kvalita	k1gFnSc7
výzbroje	výzbroj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
pěšáci	pěšák	k1gMnPc1
v	v	k7c6
boji	boj	k1gInSc6
používali	používat	k5eAaImAgMnP
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
<g/>
:	:	kIx,
gladius	gladius	k1gInSc1
a	a	k8xC
pilum	pilum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
organizace	organizace	k1gFnSc1
pěchoty	pěchota	k1gFnSc2
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
druhého	druhý	k4xOgNnSc2
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc4
důležité	důležitý	k2eAgFnPc4d1
změny	změna	k1gFnPc4
provedl	provést	k5eAaPmAgMnS
jediný	jediný	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
Gaius	Gaius	k1gMnSc1
Marius	Marius	k1gMnSc1
<g/>
,	,	kIx,
šestinásobný	šestinásobný	k2eAgMnSc1d1
konzul	konzul	k1gMnSc1
a	a	k8xC
velice	velice	k6eAd1
zkušený	zkušený	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgFnPc7
reformami	reforma	k1gFnPc7
se	se	k3xPyFc4
zasadil	zasadit	k5eAaPmAgInS
o	o	k7c4
změnu	změna	k1gFnSc4
římské	římský	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Kresba	kresba	k1gFnSc1
římského	římský	k2eAgMnSc2d1
legionáře	legionář	k1gMnSc2
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Každý	každý	k3xTgMnSc1
římský	římský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
po	po	k7c6
Mariovi	Mario	k1gMnSc6
si	se	k3xPyFc3
udělal	udělat	k5eAaPmAgMnS
z	z	k7c2
armády	armáda	k1gFnSc2
jistou	jistý	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
moci	moct	k5eAaImF
a	a	k8xC
svou	svůj	k3xOyFgFnSc4
politickou	politický	k2eAgFnSc4d1
vážnost	vážnost	k1gFnSc4
budoval	budovat	k5eAaImAgMnS
na	na	k7c6
vojenských	vojenský	k2eAgInPc6d1
triumfech	triumf	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
vzorem	vzor	k1gInSc7
se	se	k3xPyFc4
řídili	řídit	k5eAaImAgMnP
skoro	skoro	k6eAd1
všichni	všechen	k3xTgMnPc1
římští	římský	k2eAgMnPc1d1
vojevůdci	vojevůdce	k1gMnPc1
(	(	kIx(
<g/>
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
římská	římský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
za	za	k7c4
působení	působení	k1gNnSc4
mnoha	mnoho	k4c2
dalších	další	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
změnila	změnit	k5eAaPmAgFnS
na	na	k7c4
římskou	římský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
I	i	k8xC
když	když	k8xS
tyto	tento	k3xDgFnPc4
důležité	důležitý	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
zavedl	zavést	k5eAaPmAgMnS
Gaius	Gaius	k1gMnSc1
Marius	Marius	k1gMnSc1
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
historiků	historik	k1gMnPc2
shodně	shodně	k6eAd1
soudí	soudit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
reformy	reforma	k1gFnPc1
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
byly	být	k5eAaImAgInP
záležitostí	záležitost	k1gFnSc7
evoluce	evoluce	k1gFnSc2
a	a	k8xC
nikoli	nikoli	k9
revoluce	revoluce	k1gFnPc1
a	a	k8xC
že	že	k8xS
Marius	Marius	k1gInSc1
udělal	udělat	k5eAaPmAgInS
pouze	pouze	k6eAd1
jakýsi	jakýsi	k3yIgInSc1
pomyslný	pomyslný	k2eAgInSc1d1
krok	krok	k1gInSc1
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
kdyby	kdyby	kYmCp3nS
k	k	k7c3
těmto	tento	k3xDgFnPc3
reformám	reforma	k1gFnPc3
nepřikročil	přikročit	k5eNaPmAgMnS
Marius	Marius	k1gMnSc1
<g/>
,	,	kIx,
sáhl	sáhnout	k5eAaPmAgMnS
by	by	kYmCp3nS
k	k	k7c3
nim	on	k3xPp3gMnPc3
nakonec	nakonec	k6eAd1
někdo	někdo	k3yInSc1
jiný	jiný	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěchota	pěchota	k1gFnSc1
v	v	k7c6
Mariově	Mariův	k2eAgFnSc6d1
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
stále	stále	k6eAd1
založena	založit	k5eAaPmNgFnS
na	na	k7c4
odvodu	odvod	k1gInSc3
římských	římský	k2eAgMnPc2d1
majetných	majetný	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
sloužili	sloužit	k5eAaImAgMnP
po	po	k7c4
dobu	doba	k1gFnSc4
šesti	šest	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
římské	římský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
schválil	schválit	k5eAaPmAgMnS
všeobecné	všeobecný	k2eAgInPc4d1
odvody	odvod	k1gInPc4
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
52	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Války	válka	k1gFnSc2
ve	v	k7c6
druhém	druhý	k4xOgInSc6
a	a	k8xC
třetím	třetí	k4xOgInSc6
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
římskou	římský	k2eAgFnSc4d1
brannou	branný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
výrazně	výrazně	k6eAd1
změnily	změnit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
válečných	válečný	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
se	se	k3xPyFc4
omezení	omezení	k1gNnSc1
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
dočasně	dočasně	k6eAd1
rušilo	rušit	k5eAaImAgNnS
<g/>
,	,	kIx,
když	když	k8xS
bylo	být	k5eAaImAgNnS
zapotřebí	zapotřebí	k6eAd1
více	hodně	k6eAd2
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
velkého	velký	k2eAgInSc2d1
počtu	počet	k1gInSc2
válek	válka	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
z	z	k7c2
většiny	většina	k1gFnSc2
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
stali	stát	k5eAaPmAgMnP
v	v	k7c6
podstatě	podstata	k1gFnSc6
vojáci	voják	k1gMnPc1
z	z	k7c2
povolání	povolání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
muž	muž	k1gMnSc1
ukončil	ukončit	k5eAaPmAgMnS
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
se	se	k3xPyFc4
neměl	mít	k5eNaImAgInS
kam	kam	k6eAd1
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
na	na	k7c4
šest	šest	k4xCc4
let	léto	k1gNnPc2
dobrovolně	dobrovolně	k6eAd1
zapsat	zapsat	k5eAaPmF
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenská	vojenský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
velkým	velký	k2eAgNnSc7d1
lákadlem	lákadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plat	plat	k1gInSc1
římského	římský	k2eAgMnSc2d1
vojáka	voják	k1gMnSc2
té	ten	k3xDgFnSc3
doby	doba	k1gFnSc2
se	se	k3xPyFc4
rovnal	rovnat	k5eAaImAgMnS
platu	plat	k1gInSc3
průměrného	průměrný	k2eAgMnSc2d1
občana	občan	k1gMnSc2
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
věrnou	věrný	k2eAgFnSc4d1
službu	služba	k1gFnSc4
dostávali	dostávat	k5eAaImAgMnP
vojáci	voják	k1gMnPc1
půdu	půda	k1gFnSc4
a	a	k8xC
odměny	odměna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
otevírala	otevírat	k5eAaImAgFnS
možnost	možnost	k1gFnSc1
povýšení	povýšení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
přinášelo	přinášet	k5eAaImAgNnS
finanční	finanční	k2eAgFnPc4d1
odměny	odměna	k1gFnPc4
a	a	k8xC
získání	získání	k1gNnSc4
společenské	společenský	k2eAgFnSc2d1
vážnosti	vážnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
vojevůdců	vojevůdce	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
přijímalo	přijímat	k5eAaImAgNnS
do	do	k7c2
svých	svůj	k3xOyFgFnPc2
armád	armáda	k1gFnPc2
chudé	chudý	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgNnSc3,k3yRgNnSc3,k3yQgNnSc3
by	by	k9
jinak	jinak	k6eAd1
kvůli	kvůli	k7c3
nemajetnosti	nemajetnost	k1gFnSc3
byla	být	k5eAaImAgFnS
vojenská	vojenský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
znemožněna	znemožnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mariova	Mariův	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
</s>
<s>
Tito	tento	k3xDgMnPc1
noví	nový	k2eAgMnPc1d1
odvedenci	odvedenec	k1gMnPc1
si	se	k3xPyFc3
ale	ale	k9
nemohli	moct	k5eNaImAgMnP
dovolit	dovolit	k5eAaPmF
vlastní	vlastní	k2eAgFnSc4d1
zbroj	zbroj	k1gFnSc4
ani	ani	k8xC
výzbroj	výzbroj	k1gFnSc4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
122	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
politik	politik	k1gMnSc1
Gaius	Gaius	k1gMnSc1
Gracchus	Gracchus	k1gMnSc1
<g/>
,	,	kIx,
tribun	tribun	k1gMnSc1
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
vydal	vydat	k5eAaPmAgInS
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
kterého	který	k3yQgNnSc2,k3yRgNnSc2,k3yIgNnSc2
musel	muset	k5eAaImAgInS
stát	stát	k1gInSc1
poskytovat	poskytovat	k5eAaImF
vojenské	vojenský	k2eAgNnSc4d1
oblečení	oblečení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
tomuto	tento	k3xDgInSc3
zákonu	zákon	k1gInSc3
se	se	k3xPyFc4
výzbroj	výzbroj	k1gFnSc1
<g/>
,	,	kIx,
výstroj	výstroj	k1gFnSc1
a	a	k8xC
zbraně	zbraň	k1gFnPc1
pěšáků	pěšák	k1gMnPc2
standardizovaly	standardizovat	k5eAaBmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
na	na	k7c6
konci	konec	k1gInSc6
rané	raný	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
i	i	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
používali	používat	k5eAaImAgMnP
legionáři	legionář	k1gMnPc1
kroužkové	kroužkový	k2eAgNnSc4d1
brnění	brnění	k1gNnPc4
<g/>
,	,	kIx,
helmy	helma	k1gFnPc4
Montefortino	Montefortin	k2eAgNnSc4d1
a	a	k8xC
helmy	helma	k1gFnSc2
etrusko	etrusko	k6eAd1
-	-	kIx~
korintského	korintský	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scutum	Scutum	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
nezměnilo	změnit	k5eNaPmAgNnS
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
oválný	oválný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
stejný	stejný	k2eAgInSc1d1
gladius	gladius	k1gInSc1
byl	být	k5eAaImAgInS
zavěšen	zavěsit	k5eAaPmNgInS
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
boku	bok	k1gInSc6
legionáře	legionář	k1gMnSc2
(	(	kIx(
<g/>
na	na	k7c6
levém	levý	k2eAgInSc6d1
boku	bok	k1gInSc6
by	by	kYmCp3nS
mu	on	k3xPp3gMnSc3
při	při	k7c6
vytažení	vytažení	k1gNnSc6
meče	meč	k1gInSc2
z	z	k7c2
pochvy	pochva	k1gFnSc2
vadil	vadit	k5eAaImAgInS
štít	štít	k1gInSc1
scutum	scutum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
pilu	pila	k1gFnSc4
standardního	standardní	k2eAgInSc2d1
typu	typ	k1gInSc2
byly	být	k5eAaImAgFnP
provedeny	provést	k5eAaPmNgFnP
inovace	inovace	k1gFnPc1
připisované	připisovaný	k2eAgFnPc1d1
samotnému	samotný	k2eAgMnSc3d1
Mariovi	Mario	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plútarchos	Plútarchos	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
oštěp	oštěp	k1gInSc4
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Až	až	k9
dosud	dosud	k6eAd1
byla	být	k5eAaImAgFnS
část	část	k1gFnSc1
ratiště	ratiště	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
spočívala	spočívat	k5eAaImAgFnS
v	v	k7c6
kovové	kovový	k2eAgFnSc6d1
hlavici	hlavice	k1gFnSc6
<g/>
,	,	kIx,
připevněna	připevnit	k5eAaPmNgFnS
dvěma	dva	k4xCgInPc7
železnými	železný	k2eAgInPc7d1
hřeby	hřeb	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
však	však	k9
Marius	Marius	k1gMnSc1
odstranil	odstranit	k5eAaPmAgMnS
hřeby	hřeb	k1gInPc4
a	a	k8xC
nahradil	nahradit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
dřevěným	dřevěný	k2eAgInSc7d1
kolíkem	kolík	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
snadno	snadno	k6eAd1
zlomil	zlomit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
pak	pak	k6eAd1
bylo	být	k5eAaImAgNnS
pilum	pilum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
po	po	k7c6
nárazu	náraz	k1gInSc6
do	do	k7c2
štítu	štít	k1gInSc2
nepřítele	nepřítel	k1gMnSc4
nezůstalo	zůstat	k5eNaPmAgNnS
rovné	rovný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gNnSc1
ratiště	ratiště	k1gNnSc1
se	se	k3xPyFc4
ohnulo	ohnout	k5eAaPmAgNnS
k	k	k7c3
zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
nejspíš	nejspíš	k9
vyrobit	vyrobit	k5eAaPmF
oštěp	oštěp	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
proniknutí	proniknutí	k1gNnSc6
do	do	k7c2
štítu	štít	k1gInSc2
nepoužitelný	použitelný	k2eNgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
znemožnit	znemožnit	k5eAaPmF
manipulaci	manipulace	k1gFnSc4
se	s	k7c7
samotným	samotný	k2eAgInSc7d1
štítem	štít	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
Mariova	Mariův	k2eAgNnSc2d1
období	období	k1gNnSc2
se	se	k3xPyFc4
také	také	k9
začaly	začít	k5eAaPmAgFnP
používat	používat	k5eAaImF
k	k	k7c3
boji	boj	k1gInSc3
kohorty	kohorta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
že	že	k9
v	v	k7c6
římské	římský	k2eAgFnSc6d1
pěchotě	pěchota	k1gFnSc6
sloužilo	sloužit	k5eAaImAgNnS
čím	čí	k3xOyQgNnSc7,k3xOyRgNnSc7
dál	daleko	k6eAd2
více	hodně	k6eAd2
délesloužících	délesloužící	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
vojáky	voják	k1gMnPc4
se	se	k3xPyFc4
vytvářely	vytvářet	k5eAaImAgFnP
velice	velice	k6eAd1
pevné	pevný	k2eAgFnPc4d1
společenské	společenský	k2eAgFnPc4d1
vazby	vazba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
legie	legie	k1gFnSc1
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
měla	mít	k5eAaImAgFnS
svého	svůj	k3xOyFgMnSc4
ochránce	ochránce	k1gMnSc4
(	(	kIx(
<g/>
jakýsi	jakýsi	k3yIgInSc1
talisman	talisman	k1gInSc1
<g/>
)	)	kIx)
zhmotněného	zhmotněný	k2eAgNnSc2d1
do	do	k7c2
podoby	podoba	k1gFnSc2
orla	orel	k1gMnSc4
nastandartě	nastandarta	k1gFnSc6
(	(	kIx(
<g/>
korouhvi	korouhev	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztráta	ztráta	k1gFnSc1
orla	orel	k1gMnSc2
<g/>
,	,	kIx,
posvátného	posvátný	k2eAgInSc2d1
symbolu	symbol	k1gInSc2
legie	legie	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
národní	národní	k2eAgFnSc4d1
katastrofu	katastrofa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
popisu	popis	k1gInSc2
různých	různý	k2eAgMnPc2d1
římských	římský	k2eAgMnPc2d1
historiků	historik	k1gMnPc2
se	se	k3xPyFc4
spíše	spíše	k9
než	než	k8xS
manipuly	manipul	k1gInPc1
a	a	k8xC
legie	legie	k1gFnPc1
používaly	používat	k5eAaImAgFnP
k	k	k7c3
boji	boj	k1gInSc3
kohorty	kohorta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kohorty	kohorta	k1gFnPc4
byly	být	k5eAaImAgFnP
v	v	k7c6
podstatě	podstata	k1gFnSc6
malé	malý	k2eAgFnSc2d1
<g/>
,	,	kIx,
polonezávislé	polonezávislý	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
se	se	k3xPyFc4
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
staly	stát	k5eAaPmAgFnP
o	o	k7c4
hodně	hodně	k6eAd1
pružnější	pružný	k2eAgInSc4d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Mohly	moct	k5eAaImAgFnP
se	se	k3xPyFc4
libovolně	libovolně	k6eAd1
přesouvat	přesouvat	k5eAaImF
po	po	k7c6
bitevním	bitevní	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
bylo	být	k5eAaImAgNnS
zapotřebí	zapotřebí	k6eAd1
(	(	kIx(
<g/>
Caesar	Caesar	k1gMnSc1
u	u	k7c2
Farsalu	Farsal	k1gInSc2
záměrně	záměrně	k6eAd1
natěsnal	natěsnat	k5eAaPmAgMnS
kohorty	kohorta	k1gFnSc2
blízko	blízko	k6eAd1
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zvýšil	zvýšit	k5eAaPmAgInS
celkovou	celkový	k2eAgFnSc4d1
útočnou	útočný	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
své	svůj	k3xOyFgFnSc2
pěchoty	pěchota	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gladius	Gladius	k1gInSc1
už	už	k6eAd1
přestal	přestat	k5eAaPmAgInS
být	být	k5eAaImF
jedinou	jediný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
při	při	k7c6
boji	boj	k1gInSc6
zblízka	zblízka	k6eAd1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
pro	pro	k7c4
ně	on	k3xPp3gInPc4
zůstával	zůstávat	k5eAaImAgInS
zbraní	zbraň	k1gFnSc7
nejdůležitější	důležitý	k2eAgFnSc2d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Začalo	začít	k5eAaPmAgNnS
se	se	k3xPyFc4
používat	používat	k5eAaImF
pilum	pilum	k1gInSc4
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
bodné	bodný	k2eAgNnSc1d1
kopí	kopí	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
na	na	k7c4
římskou	římský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
zaútočila	zaútočit	k5eAaPmAgFnS
nepřátelská	přátelský	k2eNgFnSc1d1
jízda	jízda	k1gFnSc1
<g/>
,	,	kIx,
sevřeli	sevřít	k5eAaPmAgMnP
se	se	k3xPyFc4
římští	římský	k2eAgMnPc1d1
pěšáci	pěšák	k1gMnPc1
do	do	k7c2
pevných	pevný	k2eAgInPc2d1
šiků	šik	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
používali	používat	k5eAaImAgMnP
pila	pila	k1gFnSc1
jako	jako	k9
kopí	kopit	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
jakmile	jakmile	k8xS
nepřátelský	přátelský	k2eNgInSc1d1
útok	útok	k1gInSc1
ochabl	ochabnout	k5eAaPmAgInS
<g/>
,	,	kIx,
rozevřeli	rozevřít	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
řady	řada	k1gFnPc4
a	a	k8xC
s	s	k7c7
meči	meč	k1gInPc7
v	v	k7c6
ruce	ruka	k1gFnSc6
vyrazili	vyrazit	k5eAaPmAgMnP
do	do	k7c2
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
punské	punský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
používat	používat	k5eAaImF
lučištníci	lučištník	k1gMnPc1
a	a	k8xC
prakovníci	prakovník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
střelecké	střelecký	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
podporovaly	podporovat	k5eAaImAgFnP
vojáky	voják	k1gMnPc4
v	v	k7c6
boji	boj	k1gInSc6
a	a	k8xC
bránily	bránit	k5eAaImAgInP
římské	římský	k2eAgInPc1d1
tábory	tábor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
legie	legie	k1gFnPc4
byla	být	k5eAaImAgFnS
také	také	k6eAd1
vybavena	vybavit	k5eAaPmNgFnS
určitým	určitý	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
katapultů	katapult	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc5
byly	být	k5eAaImAgInP
jednak	jednak	k8xC
používány	používán	k2eAgInPc1d1
při	při	k7c6
dobývání	dobývání	k1gNnSc6
měst	město	k1gNnPc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
jako	jako	k8xS,k8xC
střelecká	střelecký	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
taktika	taktika	k1gFnSc1
se	se	k3xPyFc4
skvěle	skvěle	k6eAd1
osvědčila	osvědčit	k5eAaPmAgFnS
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
řeckým	řecký	k2eAgFnPc3d1
falangám	falanga	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výcvik	výcvik	k1gInSc1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
</s>
<s>
Římští	římský	k2eAgMnPc1d1
rekruti	rekrut	k1gMnPc1
trénovali	trénovat	k5eAaImAgMnP
každý	každý	k3xTgInSc4
den	den	k1gInSc4
po	po	k7c4
dobu	doba	k1gFnSc4
čtyř	čtyři	k4xCgInPc2
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Výcvik	výcvik	k1gInSc1
započal	započnout	k5eAaPmAgInS
zkoušením	zkoušení	k1gNnSc7
vojenských	vojenský	k2eAgInPc2d1
pochodů	pochod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
rekrutů	rekrut	k1gMnPc2
se	se	k3xPyFc4
požadovalo	požadovat	k5eAaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ušli	ujít	k5eAaPmAgMnP
29	#num#	k4
kilometrů	kilometr	k1gInPc2
za	za	k7c4
pět	pět	k4xCc4
hodin	hodina	k1gFnPc2
běžným	běžný	k2eAgInSc7d1
krokem	krok	k1gInSc7
a	a	k8xC
35	#num#	k4
kilometrů	kilometr	k1gInPc2
za	za	k7c4
pět	pět	k4xCc4
hodin	hodina	k1gFnPc2
zrychleným	zrychlený	k2eAgInSc7d1
krokem	krok	k1gInSc7
se	s	k7c7
zavazadlem	zavazadlo	k1gNnSc7
o	o	k7c6
váze	váha	k1gFnSc6
20,5	20,5	k4
kilogramů	kilogram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
břemeno	břemeno	k1gNnSc1
sloužilo	sloužit	k5eAaImAgNnS
pouze	pouze	k6eAd1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
římský	římský	k2eAgMnSc1d1
pěšák	pěšák	k1gMnSc1
zvykl	zvyknout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
nést	nést	k5eAaImF
těžkou	těžký	k2eAgFnSc4d1
zbroj	zbroj	k1gFnSc4
a	a	k8xC
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
(	(	kIx(
<g/>
20,5	20,5	k4
kilogramů	kilogram	k1gInPc2
byla	být	k5eAaImAgFnS
malá	malý	k2eAgFnSc1d1
váha	váha	k1gFnSc1
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
plnou	plný	k2eAgFnSc7d1
výzbrojí	výzbroj	k1gFnSc7
a	a	k8xC
výstrojí	výstroj	k1gFnSc7
těžkého	těžký	k2eAgMnSc2d1
římského	římský	k2eAgMnSc2d1
pěšáka	pěšák	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
tomto	tento	k3xDgInSc6
výcviku	výcvik	k1gInSc6
se	se	k3xPyFc4
vždy	vždy	k6eAd1
dával	dávat	k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
pozor	pozor	k1gInSc4
na	na	k7c4
dodržování	dodržování	k1gNnSc4
kázně	kázeň	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Centurioni	Centurion	k1gMnPc1
sledovali	sledovat	k5eAaImAgMnP
celistvost	celistvost	k1gFnSc4
řad	řada	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
jakmile	jakmile	k8xS
se	se	k3xPyFc4
někdo	někdo	k3yInSc1
opozdil	opozdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
bit	bit	k1gInSc1
holí	hole	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
římským	římský	k2eAgMnSc7d1
rekrutům	rekrut	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
zvládnout	zvládnout	k5eAaPmF
tento	tento	k3xDgInSc4
dril	dril	k1gInSc4
a	a	k8xC
naučili	naučit	k5eAaPmAgMnP
se	se	k3xPyFc4
pochodovat	pochodovat	k5eAaImF
za	za	k7c2
zvuku	zvuk	k1gInSc2
trubek	trubka	k1gFnPc2
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
nacvičit	nacvičit	k5eAaBmF
různé	různý	k2eAgInPc4d1
manévry	manévr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trénovalo	trénovat	k5eAaImAgNnS
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
<g/>
:	:	kIx,
dutý	dutý	k2eAgInSc1d1
čtverec	čtverec	k1gInSc1
<g/>
,	,	kIx,
klín	klín	k1gInSc1
<g/>
,	,	kIx,
kruh	kruh	k1gInSc1
a	a	k8xC
testudo	testudo	k1gNnSc1
(	(	kIx(
<g/>
želva	želva	k1gFnSc1
-	-	kIx~
tato	tento	k3xDgFnSc1
formace	formace	k1gFnSc1
tvořila	tvořit	k5eAaImAgFnS
jakýsi	jakýsi	k3yIgInSc4
čtverec	čtverec	k1gInSc4
ze	z	k7c2
všech	všecek	k3xTgFnPc2
stran	strana	k1gFnPc2
chráněný	chráněný	k2eAgInSc4d1
štíty	štít	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Vojákům	voják	k1gMnPc3
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
výcviku	výcvik	k1gInSc3
v	v	k7c6
překonávání	překonávání	k1gNnSc6
překážek	překážka	k1gFnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ve	v	k7c6
změnách	změna	k1gFnPc6
směru	směr	k1gInSc2
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
bylo	být	k5eAaImAgNnS
zapotřebí	zapotřebí	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
osvobozování	osvobozování	k1gNnSc6
obklíčených	obklíčený	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
Římská	římský	k2eAgFnSc1d1
formace	formace	k1gFnSc1
testudo	testudo	k1gNnSc1
Rekrut	rekrut	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
naučil	naučit	k5eAaPmAgMnS
bojovat	bojovat	k5eAaImF
mimo	mimo	k7c4
svou	svůj	k3xOyFgFnSc4
jednotku	jednotka	k1gFnSc4
(	(	kIx(
<g/>
tedy	tedy	k9
skoro	skoro	k6eAd1
nezávisle	závisle	k6eNd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Výcvik	výcvik	k1gInSc1
se	s	k7c7
zbraněmi	zbraň	k1gFnPc7
zahrnoval	zahrnovat	k5eAaImAgMnS
cvičení	cvičení	k1gNnSc4
s	s	k7c7
meči	meč	k1gInPc7
<g/>
,	,	kIx,
oštěpy	oštěp	k1gInPc7
<g/>
,	,	kIx,
kopími	kopí	k1gNnPc7
<g/>
,	,	kIx,
luky	luk	k1gInPc7
a	a	k8xC
šípy	šíp	k1gInPc7
a	a	k8xC
štíty	štít	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štít	štít	k1gInSc1
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
z	z	k7c2
vrbového	vrbový	k2eAgNnSc2d1
proutí	proutí	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
cvičný	cvičný	k2eAgMnSc1d1
byl	být	k5eAaImAgMnS
dvakrát	dvakrát	k6eAd1
těžší	těžký	k2eAgMnSc1d2
než	než	k8xS
bojový	bojový	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Meče	meč	k1gInSc2
byly	být	k5eAaImAgInP
dřevěné	dřevěný	k2eAgInPc1d1
rapíry	rapír	k1gInPc1
<g/>
,	,	kIx,
rovněž	rovněž	k9
dvojnásobné	dvojnásobný	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekrut	rekrut	k1gMnSc1
se	se	k3xPyFc4
cvičil	cvičit	k5eAaImAgMnS
na	na	k7c6
dřevěném	dřevěný	k2eAgInSc6d1
kůlu	kůl	k1gInSc6
vraženém	vražený	k2eAgInSc6d1
do	do	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
tento	tento	k3xDgInSc4
kůl	kůl	k1gInSc4
si	se	k3xPyFc3
rekrut	rekrut	k1gMnSc1
zkoušel	zkoušet	k5eAaImAgMnS
různé	různý	k2eAgInPc4d1
útoky	útok	k1gInPc4
a	a	k8xC
byla	být	k5eAaImAgFnS
hodnocena	hodnotit	k5eAaImNgFnS
jeho	jeho	k3xOp3gFnSc1
schopnost	schopnost	k1gFnSc1
vládnout	vládnout	k5eAaImF
mečem	meč	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Meče	meč	k1gInPc1
měly	mít	k5eAaImAgInP
být	být	k5eAaImF
používány	používat	k5eAaImNgInP
výhradně	výhradně	k6eAd1
k	k	k7c3
bodání	bodání	k1gNnSc3
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
k	k	k7c3
sekání	sekání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
bodnutí	bodnutí	k1gNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
velice	velice	k6eAd1
závažné	závažný	k2eAgNnSc1d1
zranění	zranění	k1gNnSc1
a	a	k8xC
také	také	k9
při	při	k7c6
něm	on	k3xPp3gInSc6
byla	být	k5eAaImAgFnS
větší	veliký	k2eAgFnSc1d2
pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
úder	úder	k1gInSc1
projde	projít	k5eAaPmIp3nS
brněním	brnění	k1gNnSc7
nepřítele	nepřítel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
výcviku	výcvik	k1gInSc6
dostal	dostat	k5eAaPmAgMnS
rekrut	rekrut	k1gMnSc1
lekce	lekce	k1gFnSc2
v	v	k7c6
plavání	plavání	k1gNnSc6
<g/>
,	,	kIx,
lukostřelbě	lukostřelba	k1gFnSc6
a	a	k8xC
v	v	k7c6
jízdě	jízda	k1gFnSc6
na	na	k7c6
koni	kůň	k1gMnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
připraven	připraven	k2eAgInSc1d1
na	na	k7c4
každou	každý	k3xTgFnSc4
situaci	situace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Užívání	užívání	k1gNnSc1
meče	meč	k1gInSc2
římskými	římský	k2eAgMnPc7d1
pěšáky	pěšák	k1gMnPc7
mohlo	moct	k5eAaImAgNnS
vycházet	vycházet	k5eAaImF
z	z	k7c2
keltského	keltský	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
byl	být	k5eAaImAgInS
Řím	Řím	k1gInSc1
již	již	k6eAd1
plně	plně	k6eAd1
civilizovanou	civilizovaný	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
stále	stále	k6eAd1
tu	tu	k6eAd1
převládal	převládat	k5eAaImAgInS
kult	kult	k1gInSc1
boje	boj	k1gInSc2
muže	muž	k1gMnSc2
proti	proti	k7c3
muži	muž	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
skoro	skoro	k6eAd1
vymizel	vymizet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
už	už	k6eAd1
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgInS
žádný	žádný	k3yNgInSc1
rozdíl	rozdíl	k1gInSc1
ve	v	k7c6
výstroji	výstroj	k1gInSc6
a	a	k8xC
výcviku	výcvik	k1gInSc6
římských	římský	k2eAgInPc2d1
hastatů	hastat	k1gInPc2
<g/>
,	,	kIx,
principů	princip	k1gInPc2
a	a	k8xC
triariů	triari	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotní	samotnět	k5eAaImIp3nS
hastati	hastat	k5eAaPmF,k5eAaImF
a	a	k8xC
principové	principový	k2eAgFnPc1d1
se	se	k3xPyFc4
vlastně	vlastně	k9
spojili	spojit	k5eAaPmAgMnP
a	a	k8xC
vytvořili	vytvořit	k5eAaPmAgMnP
jednu	jeden	k4xCgFnSc4
část	část	k1gFnSc4
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
v	v	k7c6
římské	římský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
chtěl	chtít	k5eAaImAgMnS
stát	stát	k5eAaImF,k5eAaPmF
důstojníkem	důstojník	k1gMnSc7
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
projít	projít	k5eAaPmF
řadou	řada	k1gFnSc7
cvičení	cvičení	k1gNnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
znal	znát	k5eAaImAgInS
všechny	všechen	k3xTgInPc4
postupy	postup	k1gInPc4
a	a	k8xC
při	při	k7c6
každé	každý	k3xTgFnSc6
situaci	situace	k1gFnSc6
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
má	mít	k5eAaImIp3nS
dělat	dělat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotní	samotný	k2eAgMnPc1d1
Germáni	Germán	k1gMnPc1
a	a	k8xC
Galové	Gal	k1gMnPc1
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgMnPc1d1
nepřátelé	nepřítel	k1gMnPc1
Římanů	Říman	k1gMnPc2
<g/>
)	)	kIx)
také	také	k9
používali	používat	k5eAaImAgMnP
k	k	k7c3
boji	boj	k1gInSc3
meče	meč	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
římský	římský	k2eAgInSc1d1
dokonalý	dokonalý	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
a	a	k8xC
disciplína	disciplína	k1gFnSc1
rozhodly	rozhodnout	k5eAaPmAgFnP
skoro	skoro	k6eAd1
každou	každý	k3xTgFnSc4
velkou	velký	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pěchota	pěchota	k1gFnSc1
raného	raný	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
</s>
<s>
Strategie	strategie	k1gFnSc1
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
výzbroj	výzbroj	k1gFnSc4
pěchoty	pěchota	k1gFnSc2
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
římského	římský	k2eAgMnSc2d1
vojáka	voják	k1gMnSc2
z	z	k7c2
dob	doba	k1gFnPc2
raného	raný	k2eAgNnSc2d1
římského	římský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
</s>
<s>
Mnoho	mnoho	k4c1
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
poznalo	poznat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
vojenský	vojenský	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
je	být	k5eAaImIp3nS
základem	základ	k1gInSc7
politické	politický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
zajišťuje	zajišťovat	k5eAaImIp3nS
popularitu	popularita	k1gFnSc4
u	u	k7c2
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
raného	raný	k2eAgNnSc2d1
římského	římský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
byla	být	k5eAaImAgFnS
používána	používán	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
postupného	postupný	k2eAgNnSc2d1
opevňování	opevňování	k1gNnSc2
hranic	hranice	k1gFnPc2
a	a	k8xC
občasných	občasný	k2eAgInPc2d1
výbojů	výboj	k1gInPc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
tyto	tento	k3xDgInPc1
výboje	výboj	k1gInPc1
přerostly	přerůst	k5eAaPmAgInP
až	až	k9
v	v	k7c4
dobyvačné	dobyvačný	k2eAgFnPc4d1
války	válka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
období	období	k1gNnSc2
raného	raný	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
byla	být	k5eAaImAgFnS
dobyta	dobyt	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
,	,	kIx,
Dácie	Dácie	k1gFnSc1
a	a	k8xC
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
i	i	k8xC
Mezopotámie	Mezopotámie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Řím	Řím	k1gInSc1
neohrožovali	ohrožovat	k5eNaImAgMnP
skutečně	skutečně	k6eAd1
velcí	velký	k2eAgMnPc1d1
soupeři	soupeř	k1gMnPc1
<g/>
,	,	kIx,
nepotřeboval	potřebovat	k5eNaImAgMnS
ani	ani	k8xC
početnou	početný	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
31	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
měl	mít	k5eAaImAgMnS
císař	císař	k1gMnSc1
Augustus	Augustus	k1gMnSc1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
šedesát	šedesát	k4xCc4
legií	legie	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
snížil	snížit	k5eAaPmAgInS
na	na	k7c4
dvacet	dvacet	k4xCc4
osm	osm	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěchotu	pěchota	k1gFnSc4
sloužící	sloužící	k1gFnSc2
v	v	k7c6
těchto	tento	k3xDgFnPc6
legiích	legie	k1gFnPc6
tvořili	tvořit	k5eAaImAgMnP
už	už	k6eAd1
výhradně	výhradně	k6eAd1
profesionální	profesionální	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Požadavek	požadavek	k1gInSc1
vlastnictví	vlastnictví	k1gNnSc2
majetku	majetek	k1gInSc2
či	či	k8xC
půdy	půda	k1gFnSc2
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
a	a	k8xC
vojenská	vojenský	k2eAgFnSc1d1
služba	služba	k1gFnSc1
se	se	k3xPyFc4
značně	značně	k6eAd1
prodloužila	prodloužit	k5eAaPmAgFnS
(	(	kIx(
<g/>
20	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
pěchoty	pěchota	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
dob	doba	k1gFnPc2
Mariových	Mariův	k2eAgFnPc2d1
prakticky	prakticky	k6eAd1
nezměnila	změnit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
vedení	vedení	k1gNnSc4
centurií	centurie	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
jejich	jejich	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
stáli	stát	k5eAaImAgMnP
většinou	většinou	k6eAd1
velice	velice	k6eAd1
zkušení	zkušený	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
volení	volení	k1gNnSc1
vlastními	vlastní	k2eAgMnPc7d1
vojáky	voják	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Měli	mít	k5eAaImAgMnP
proto	proto	k8xC
u	u	k7c2
mužstva	mužstvo	k1gNnSc2
značný	značný	k2eAgInSc4d1
respekt	respekt	k1gInSc4
a	a	k8xC
také	také	k9
byli	být	k5eAaImAgMnP
zvyklí	zvyklý	k2eAgMnPc1d1
na	na	k7c4
vojenský	vojenský	k2eAgInSc4d1
život	život	k1gInSc4
plný	plný	k2eAgInSc4d1
útrap	útrap	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnSc1
centuriona	centurio	k1gMnSc2
byla	být	k5eAaImAgFnS
zastávána	zastávat	k5eAaImNgFnS
maximálně	maximálně	k6eAd1
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybavení	vybavení	k1gNnSc1
pěšáků	pěšák	k1gMnPc2
se	se	k3xPyFc4
podobalo	podobat	k5eAaImAgNnS
vybavení	vybavení	k1gNnSc1
pozdní	pozdní	k2eAgFnSc2d1
republikánské	republikánský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojovali	bojovat	k5eAaImAgMnP
prakticky	prakticky	k6eAd1
stejnými	stejný	k2eAgFnPc7d1
zbraněmi	zbraň	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
prošly	projít	k5eAaPmAgFnP
jen	jen	k9
menšími	malý	k2eAgFnPc7d2
úpravami	úprava	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pilum	Pilum	k1gNnSc1
bylo	být	k5eAaImAgNnS
odlehčeno	odlehčen	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
s	s	k7c7
ním	on	k3xPp3gMnSc7
pěšák	pěšák	k1gMnSc1
mohl	moct	k5eAaImAgInS
lépe	dobře	k6eAd2
manipulovat	manipulovat	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
nejspíše	nejspíše	k9
stabilizováno	stabilizovat	k5eAaBmNgNnS
olověným	olověný	k2eAgNnSc7d1
závažím	závaží	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc1
se	se	k3xPyFc4
poněkud	poněkud	k6eAd1
zkrátil	zkrátit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gInSc1
hrot	hrot	k1gInSc1
byl	být	k5eAaImAgInS
relativně	relativně	k6eAd1
delší	dlouhý	k2eAgMnSc1d2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
něj	on	k3xPp3gMnSc4
pohlíželi	pohlížet	k5eAaImAgMnP
jako	jako	k9
na	na	k7c4
čistě	čistě	k6eAd1
bodnou	bodný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Kroužkové	kroužkový	k2eAgNnSc1d1
brnění	brnění	k1gNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
používat	používat	k5eAaImF
plátové	plátový	k2eAgNnSc4d1
brnění	brnění	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
vynalezli	vynaleznout	k5eAaPmAgMnP
samotní	samotný	k2eAgMnPc1d1
Římané	Říman	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Brnění	brnění	k1gNnSc6
pěšáků	pěšák	k1gMnPc2
zvané	zvaný	k2eAgFnSc2d1
lorica	loricum	k1gNnSc2
segmentata	segmentata	k1gFnSc1
bylo	být	k5eAaImAgNnS
z	z	k7c2
železných	železný	k2eAgInPc2d1
pásků	pásek	k1gInPc2
spojených	spojený	k2eAgInPc2d1
háčky	háček	k1gInPc7
<g/>
,	,	kIx,
řemínky	řemínek	k1gInPc1
nebo	nebo	k8xC
pásky	pásek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chránilo	chránit	k5eAaImAgNnS
horní	horní	k2eAgFnSc4d1
část	část	k1gFnSc4
těla	tělo	k1gNnSc2
a	a	k8xC
ramena	rameno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
testech	test	k1gInPc6
provedených	provedený	k2eAgInPc2d1
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
silné	silný	k2eAgNnSc1d1
a	a	k8xC
pružné	pružný	k2eAgNnSc1d1
<g/>
,	,	kIx,
vyžaduje	vyžadovat	k5eAaImIp3nS
však	však	k9
častou	častý	k2eAgFnSc4d1
údržbu	údržba	k1gFnSc4
a	a	k8xC
velikou	veliký	k2eAgFnSc4d1
zručnost	zručnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Štít	štít	k1gInSc1
scutum	scutum	k1gNnSc4
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
oválný	oválný	k2eAgInSc1d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
nahrazen	nahradit	k5eAaPmNgInS
tvarem	tvar	k1gInSc7
obdélníkovým	obdélníkový	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
byl	být	k5eAaImAgInS
z	z	k7c2
vrstveného	vrstvený	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
potaženého	potažený	k2eAgNnSc2d1
kůží	kůže	k1gFnSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
hrany	hrana	k1gFnSc2
chránil	chránit	k5eAaImAgInS
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
římské	římský	k2eAgFnSc6d1
pěchotě	pěchota	k1gFnSc6
vládla	vládnout	k5eAaImAgFnS
přísná	přísný	k2eAgFnSc1d1
uniformita	uniformita	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
ale	ale	k8xC
netýkalo	týkat	k5eNaImAgNnS
přileb	přilba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpoužívanéjší	Nejpoužívanéjš	k1gFnPc2
byla	být	k5eAaImAgFnS
přilba	přilba	k1gFnSc1
galského	galský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
vlastně	vlastně	k9
zdokonalená	zdokonalený	k2eAgFnSc1d1
přilba	přilba	k1gFnSc1
Montefortino	Montefortin	k2eAgNnSc1d1
<g/>
,	,	kIx,
doplněná	doplněný	k2eAgFnSc1d1
o	o	k7c4
chrániče	chránič	k1gInPc4
krku	krk	k1gInSc2
a	a	k8xC
celkově	celkově	k6eAd1
zmenšená	zmenšený	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomocné	pomocný	k2eAgInPc1d1
sbory	sbor	k1gInPc1
a	a	k8xC
válčení	válčení	k1gNnSc1
v	v	k7c6
období	období	k1gNnSc6
raného	raný	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
raného	raný	k2eAgNnSc2d1
římského	římský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
bylo	být	k5eAaImAgNnS
ve	v	k7c6
zvětšené	zvětšený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
používáno	používat	k5eAaImNgNnS
spojeneckých	spojenecký	k2eAgInPc2d1
pomocných	pomocný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
(	(	kIx(
<g/>
auxilia	auxilia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodů	důvod	k1gInPc2
bylo	být	k5eAaImAgNnS
hned	hned	k6eAd1
několik	několik	k4yIc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
neřímský	římský	k2eNgMnSc1d1
občan	občan	k1gMnSc1
přihlásil	přihlásit	k5eAaPmAgMnS
k	k	k7c3
vojsku	vojsko	k1gNnSc3
a	a	k8xC
odsloužil	odsloužit	k5eAaPmAgMnS
si	se	k3xPyFc3
pětadvaceti	pětadvacet	k4xCc3
let	léto	k1gNnPc2
v	v	k7c6
armádě	armáda	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
římské	římský	k2eAgNnSc4d1
občanství	občanství	k1gNnSc4
(	(	kIx(
<g/>
toto	tento	k3xDgNnSc1
občanství	občanství	k1gNnSc1
platilo	platit	k5eAaImAgNnS
i	i	k9
pro	pro	k7c4
jeho	jeho	k3xOp3gMnPc4
potomky	potomek	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
I	i	k9
když	když	k8xS
Římané	Říman	k1gMnPc1
postupně	postupně	k6eAd1
udělovaly	udělovat	k5eAaImAgFnP
právo	právo	k1gNnSc4
římských	římský	k2eAgFnPc2d1
občanům	občan	k1gMnPc3
svým	svůj	k3xOyFgMnPc3
věrným	věrný	k2eAgMnPc3d1
spojencům	spojenec	k1gMnPc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
pomalý	pomalý	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římští	římský	k2eAgMnPc1d1
pěšáci	pěšák	k1gMnPc1
byli	být	k5eAaImAgMnP
sice	sice	k8xC
hlavní	hlavní	k2eAgFnSc7d1
údernou	úderný	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mnoho	mnoho	k4c4
povinností	povinnost	k1gFnPc2
museli	muset	k5eAaImAgMnP
plnit	plnit	k5eAaImF
právě	právě	k9
auxilia	auxilia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římští	římský	k2eAgMnPc5d1
auxilia	auxilius	k1gMnSc4
se	se	k3xPyFc4
ani	ani	k9
tak	tak	k6eAd1
nepoužívala	používat	k5eNaImAgFnS
v	v	k7c6
tvrdém	tvrdý	k2eAgInSc6d1
boji	boj	k1gInSc6
jako	jako	k8xS,k8xC
spíše	spíše	k9
k	k	k7c3
různým	různý	k2eAgInPc3d1
podřadným	podřadný	k2eAgInPc3d1
úkolům	úkol	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Práce	práce	k1gFnPc1
v	v	k7c6
kasárnách	kasárny	k1gFnPc6
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgInPc1d1
průzkumy	průzkum	k1gInPc1
a	a	k8xC
podporování	podporování	k1gNnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
(	(	kIx(
<g/>
římská	římský	k2eAgFnSc1d1
auxila	auxila	k1gFnSc1
byla	být	k5eAaImAgFnS
většinou	většinou	k6eAd1
ceněna	cenit	k5eAaImNgFnS
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
schopnost	schopnost	k1gFnSc4
jezdit	jezdit	k5eAaImF
na	na	k7c6
koni	kůň	k1gMnSc6
<g/>
)	)	kIx)
byly	být	k5eAaImAgInP
na	na	k7c6
bedrech	bedra	k1gNnPc6
pomocných	pomocný	k2eAgMnPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Kohorty	kohorta	k1gFnPc1
auxilií	auxilie	k1gFnSc7
byly	být	k5eAaImAgFnP
sestavovány	sestavovat	k5eAaImNgFnP
z	z	k7c2
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
žili	žít	k5eAaImAgMnP
v	v	k7c6
konkrétních	konkrétní	k2eAgFnPc6d1
částech	část	k1gFnPc6
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
určilo	určit	k5eAaPmAgNnS
i	i	k9
jejich	jejich	k3xOp3gNnSc1
působení	působení	k1gNnSc1
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
obdobích	období	k1gNnPc6
byla	být	k5eAaImAgFnS
auxilia	auxilia	k1gFnSc1
sestavována	sestavovat	k5eAaImNgFnS
jinde	jinde	k6eAd1
a	a	k8xC
tyto	tento	k3xDgFnPc1
jednotky	jednotka	k1gFnPc1
tvořily	tvořit	k5eAaImAgFnP
jakýsi	jakýsi	k3yIgInSc4
mix	mix	k1gInSc4
všech	všecek	k3xTgMnPc2
obyvatelů	obyvatel	k1gMnPc2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římští	římský	k2eAgMnPc5d1
auxiliaZaměření	auxiliaZaměření	k1gNnSc6
jednotek	jednotka	k1gFnPc2
pomocných	pomocný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
nebylo	být	k5eNaImAgNnS
všestranné	všestranný	k2eAgNnSc1d1
nýbrž	nýbrž	k8xC
specifické	specifický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovaly	existovat	k5eAaImAgFnP
jednotky	jednotka	k1gFnPc1
lučištníků	lučištník	k1gMnPc2
<g/>
,	,	kIx,
prakovníků	prakovník	k1gMnPc2
<g/>
,	,	kIx,
jezdců	jezdec	k1gMnPc2
a	a	k8xC
těžkých	těžký	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Vybavení	vybavení	k1gNnSc4
těchto	tento	k3xDgFnPc2
jednotek	jednotka	k1gFnPc2
bylo	být	k5eAaImAgNnS
většinou	většinou	k6eAd1
aspoň	aspoň	k9
o	o	k7c4
generaci	generace	k1gFnSc4
starší	starý	k2eAgFnPc1d2
než	než	k8xS
vybavení	vybavení	k1gNnSc1
římských	římský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
(	(	kIx(
<g/>
pěšáci	pěšák	k1gMnPc1
pomocných	pomocný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
většinou	většinou	k6eAd1
používali	používat	k5eAaImAgMnP
kroužkové	kroužkový	k2eAgNnSc4d1
brnění	brnění	k1gNnSc4
místo	místo	k7c2
účinnějšího	účinný	k2eAgNnSc2d2
brnění	brnění	k1gNnSc2
lorica	loricum	k1gNnSc2
segmentata	segmentat	k1gMnSc2
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
auxilií	auxilie	k1gFnPc2
byla	být	k5eAaImAgFnS
úplně	úplně	k6eAd1
stejná	stejný	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
organizace	organizace	k1gFnSc1
pravidelné	pravidelný	k2eAgFnSc2d1
římské	římský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
válčení	válčení	k1gNnSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
krátká	krátké	k1gNnPc4
tažení	tažení	k1gNnSc2
a	a	k8xC
velké	velký	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
byly	být	k5eAaImAgInP
vzácné	vzácný	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Taktika	taktika	k1gFnSc1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
zůstávala	zůstávat	k5eAaImAgFnS
stejná	stejný	k2eAgFnSc1d1
(	(	kIx(
<g/>
hlavně	hlavně	k9
proto	proto	k8xC
že	že	k8xS
byla	být	k5eAaImAgFnS
pořád	pořád	k6eAd1
účinná	účinný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapříčiněno	zapříčinit	k5eAaPmNgNnS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
hlavně	hlavně	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
římská	římský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
neměla	mít	k5eNaImAgFnS
silného	silný	k2eAgMnSc4d1
soupeře	soupeř	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
byli	být	k5eAaImAgMnP
sami	sám	k3xTgMnPc1
Římané	Říman	k1gMnPc1
agresoři	agresor	k1gMnPc1
a	a	k8xC
byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc4
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
napadali	napadat	k5eAaPmAgMnP,k5eAaImAgMnP,k5eAaBmAgMnP
cizí	cizí	k2eAgFnPc4d1
země	zem	k1gFnPc4
a	a	k8xC
dobývali	dobývat	k5eAaImAgMnP
je	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tažení	tažení	k1gNnSc1
bylo	být	k5eAaImAgNnS
většinou	většina	k1gFnSc7
zapříčiněno	zapříčiněn	k2eAgNnSc4d1
nějakou	nějaký	k3yIgFnSc4
vzpourou	vzpoura	k1gFnSc7
určitého	určitý	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
vzpoury	vzpoura	k1gFnPc1
byly	být	k5eAaImAgFnP
vždy	vždy	k6eAd1
potlačeny	potlačen	k2eAgFnPc1d1
(	(	kIx(
<g/>
i	i	k8xC
když	když	k8xS
ne	ne	k9
vždy	vždy	k6eAd1
tak	tak	k6eAd1
snadno	snadno	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
spíše	spíše	k9
zdokonalovala	zdokonalovat	k5eAaImAgFnS
v	v	k7c6
již	již	k6eAd1
zavedených	zavedený	k2eAgInPc6d1
postupech	postup	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavně	hlavně	k6eAd1
v	v	k7c6
obléhacích	obléhací	k2eAgInPc6d1
bojích	boj	k1gInPc6
se	se	k3xPyFc4
Římané	Říman	k1gMnPc1
stali	stát	k5eAaPmAgMnP
opravdovými	opravdový	k2eAgMnPc7d1
mistry	mistr	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
toto	tento	k3xDgNnSc4
období	období	k1gNnSc4
je	být	k5eAaImIp3nS
také	také	k9
typické	typický	k2eAgNnSc1d1
narůstající	narůstající	k2eAgNnSc1d1
používání	používání	k1gNnSc1
střelných	střelný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
zvětšovány	zvětšován	k2eAgInPc4d1
počty	počet	k1gInPc4
lučištníků	lučištník	k1gMnPc2
<g/>
,	,	kIx,
prakovníků	prakovník	k1gMnPc2
a	a	k8xC
různých	různý	k2eAgFnPc2d1
obléhacích	obléhací	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
(	(	kIx(
<g/>
balista	balista	k1gFnSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
byly	být	k5eAaImAgFnP
hojně	hojně	k6eAd1
používány	používat	k5eAaImNgFnP
různé	různý	k2eAgFnPc1d1
formace	formace	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
byly	být	k5eAaImAgInP
účinné	účinný	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
u	u	k7c2
nich	on	k3xPp3gMnPc2
zapotřebí	zapotřebí	k6eAd1
velkého	velký	k2eAgInSc2d1
výcviku	výcvik	k1gInSc2
a	a	k8xC
disciplíny	disciplína	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
samo	sám	k3xTgNnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
dokazuje	dokazovat	k5eAaImIp3nS
vycvičenost	vycvičenost	k1gFnSc4
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pretoriánská	pretoriánský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
</s>
<s>
Pretoriánská	pretoriánský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
byla	být	k5eAaImAgFnS
elitní	elitní	k2eAgFnSc4d1
stráž	stráž	k1gFnSc4
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pretoriáni	pretorián	k1gMnPc1
byli	být	k5eAaImAgMnP
vojenská	vojenský	k2eAgFnSc1d1
síla	síla	k1gFnSc1
Říma	Řím	k1gInSc2
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
udržovala	udržovat	k5eAaImAgFnS
pořádek	pořádek	k1gInSc4
a	a	k8xC
moc	moc	k6eAd1
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
(	(	kIx(
<g/>
mnohokrát	mnohokrát	k6eAd1
se	se	k3xPyFc4
však	však	k9
pretoriáni	pretorián	k1gMnPc1
obrátili	obrátit	k5eAaPmAgMnP
proti	proti	k7c3
vlastním	vlastní	k2eAgMnPc3d1
císařům	císař	k1gMnPc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
jednotky	jednotka	k1gFnPc1
pěchoty	pěchota	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
těšily	těšit	k5eAaImAgFnP
relativnímu	relativní	k2eAgInSc3d1
klidu	klid	k1gInSc3
<g/>
,	,	kIx,
luxusu	luxus	k1gInSc6
<g/>
,	,	kIx,
kratší	krátký	k2eAgFnPc1d2
služby	služba	k1gFnPc1
<g/>
,	,	kIx,
lepšímu	dobrý	k2eAgInSc3d2
žoldu	žold	k1gInSc3
a	a	k8xC
různým	různý	k2eAgFnPc3d1
odměnám	odměna	k1gFnPc3
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
času	čas	k1gInSc2
nemuseli	muset	k5eNaImAgMnP
pretoriáni	pretorián	k1gMnPc1
dělat	dělat	k5eAaImF
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
než	než	k8xS
hlídat	hlídat	k5eAaImF
císařův	císařův	k2eAgInSc4d1
palác	palác	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
prostředí	prostředí	k1gNnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
zdát	zdát	k5eAaPmF,k5eAaImF
jako	jako	k9
nesprávné	správný	k2eNgInPc1d1
pro	pro	k7c4
elitní	elitní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jakmile	jakmile	k8xS
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
bitvy	bitva	k1gFnSc2
<g/>
,	,	kIx,
ukázal	ukázat	k5eAaPmAgInS
se	se	k3xPyFc4
výborný	výborný	k2eAgInSc1d1
výcvik	výcvik	k1gInSc1
<g/>
,	,	kIx,
velení	velení	k1gNnSc1
a	a	k8xC
motivace	motivace	k1gFnSc1
těchto	tento	k3xDgFnPc2
jednotek	jednotka	k1gFnPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
prvního	první	k4xOgMnSc2
a	a	k8xC
začátkem	začátkem	k7c2
druhého	druhý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
běžnou	běžný	k2eAgFnSc7d1
praxí	praxe	k1gFnSc7
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
římští	římský	k2eAgMnPc1d1
císaři	císař	k1gMnPc1
brali	brát	k5eAaImAgMnP
svoji	svůj	k3xOyFgFnSc4
gardu	garda	k1gFnSc4
sebou	se	k3xPyFc7
na	na	k7c4
válečná	válečný	k2eAgNnPc4d1
tažení	tažení	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skoro	skoro	k6eAd1
všichni	všechen	k3xTgMnPc1
pretoriáni	pretorián	k1gMnPc1
byli	být	k5eAaImAgMnP
pěšáci	pěšák	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
pěchoty	pěchota	k1gFnSc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
i	i	k9
určité	určitý	k2eAgNnSc1d1
procento	procento	k1gNnSc1
jízdy	jízda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jízdní	jízdní	k2eAgInSc4d1
vojsko	vojsko	k1gNnSc1
pretoriánů	pretorián	k1gMnPc2
bylo	být	k5eAaImAgNnS
určeno	určit	k5eAaPmNgNnS
pro	pro	k7c4
dlouho	dlouho	k6eAd1
sloužící	sloužící	k2eAgMnPc4d1
a	a	k8xC
zkušené	zkušený	k2eAgMnPc4d1
pretoriány	pretorián	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
měl	mít	k5eAaImAgMnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
osobní	osobní	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
z	z	k7c2
germánský	germánský	k2eAgInSc4d1
vojáků	voják	k1gMnPc2
působících	působící	k2eAgMnPc2d1
společně	společně	k6eAd1
s	s	k7c7
pretoriány	pretorián	k1gMnPc7
(	(	kIx(
<g/>
v	v	k7c6
pozdních	pozdní	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
impéria	impérium	k1gNnSc2
byla	být	k5eAaImAgFnS
pretoriánská	pretoriánský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
složena	složit	k5eAaPmNgFnS
výhradně	výhradně	k6eAd1
z	z	k7c2
žoldnéřů	žoldnéř	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvod	důvod	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
loajalita	loajalita	k1gFnSc1
těchto	tento	k3xDgFnPc2
jednotek	jednotka	k1gFnPc2
byla	být	k5eAaImAgFnS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
běžných	běžný	k2eAgMnPc2d1
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
těchto	tento	k3xDgMnPc2
žoldnéřů	žoldnéř	k1gMnPc2
měla	mít	k5eAaImAgFnS
zastrašit	zastrašit	k5eAaPmF
případně	případně	k6eAd1
císařovy	císařův	k2eAgMnPc4d1
vrahy	vrah	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhled	vzhled	k1gInSc4
pretoriánů	pretorián	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
vybavení	vybavení	k1gNnSc2
se	se	k3xPyFc4
postupem	postup	k1gInSc7
času	čas	k1gInSc2
měnily	měnit	k5eAaImAgFnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
některé	některý	k3yIgFnPc1
součásti	součást	k1gFnPc1
vybavení	vybavení	k1gNnSc2
se	se	k3xPyFc4
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
gardu	garda	k1gFnSc4
staly	stát	k5eAaPmAgInP
charakteristické	charakteristický	k2eAgInPc1d1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přilba	přilba	k1gFnSc1
attického	attický	k2eAgInSc2d1
stylu	styl	k1gInSc2
a	a	k8xC
oválný	oválný	k2eAgInSc1d1
štít	štít	k1gInSc1
scutum	scutum	k1gNnSc1
byl	být	k5eAaImAgInS
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1
částí	část	k1gFnSc7
pretoriánské	pretoriánský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c6
jiné	jiný	k2eAgFnSc6d1
části	část	k1gFnSc6
vybavení	vybavení	k1gNnSc2
<g/>
,	,	kIx,
neexistují	existovat	k5eNaImIp3nP
důkazy	důkaz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
by	by	kYmCp3nS
toto	tento	k3xDgNnSc1
vybavení	vybavení	k1gNnSc1
odlišovalo	odlišovat	k5eAaImAgNnS
od	od	k7c2
vybavení	vybavení	k1gNnSc2
běžného	běžný	k2eAgMnSc2d1
pěšáka	pěšák	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pretoriáni	pretorián	k1gMnPc1
nejspíše	nejspíše	k9
používali	používat	k5eAaImAgMnP
pilum	pilum	k1gInSc4
i	i	k8xC
proslulý	proslulý	k2eAgInSc1d1
meč	meč	k1gInSc1
gladius	gladius	k1gInSc1
(	(	kIx(
<g/>
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
míře	míra	k1gFnSc6
používali	používat	k5eAaImAgMnP
i	i	k9
kopí	kopí	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
povinností	povinnost	k1gFnSc7
pretoriánské	pretoriánský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
bylo	být	k5eAaImAgNnS
hlídat	hlídat	k5eAaImF
císařský	císařský	k2eAgInSc4d1
palác	palác	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
den	den	k1gInSc4
dostával	dostávat	k5eAaImAgMnS
velitel	velitel	k1gMnSc1
pretoriánů	pretorián	k1gMnPc2
od	od	k7c2
samotného	samotný	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
zvláštní	zvláštní	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
nestalo	stát	k5eNaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
císař	císař	k1gMnSc1
zabit	zabít	k5eAaPmNgMnS
vrahy	vrah	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
byli	být	k5eAaImAgMnP
převlečeni	převlečen	k2eAgMnPc1d1
za	za	k7c4
pretoriány	pretorián	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
císař	císař	k1gMnSc1
nebo	nebo	k8xC
člen	člen	k1gMnSc1
císařské	císařský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
vydal	vydat	k5eAaPmAgMnS
na	na	k7c4
procházku	procházka	k1gFnSc4
městem	město	k1gNnSc7
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
samozřejmě	samozřejmě	k6eAd1
doprovázeny	doprovázen	k2eAgMnPc4d1
pretoriány	pretorián	k1gMnPc4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Římě	Řím	k1gInSc6
nenosili	nosit	k5eNaImAgMnP
pretoriáni	pretorián	k1gMnPc1
brnění	brnění	k1gNnSc2
<g/>
,	,	kIx,
jenom	jenom	k9
tuniku	tunika	k1gFnSc4
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	on	k3xPp3gFnPc4
odlišovala	odlišovat	k5eAaImAgFnS
od	od	k7c2
běžných	běžný	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zvyk	zvyk	k1gInSc1
měl	mít	k5eAaImAgInS
kořeny	kořen	k1gInPc4
v	v	k7c6
počátcích	počátek	k1gInPc6
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pretoriánská	pretoriánský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
Římě	Řím	k1gInSc6
samostatný	samostatný	k2eAgInSc1d1
tábor	tábor	k1gInSc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
táboře	tábor	k1gInSc6
byli	být	k5eAaImAgMnP
ubytováni	ubytován	k2eAgMnPc1d1
všichni	všechen	k3xTgMnPc1
příslušníci	příslušník	k1gMnPc1
pretoriánské	pretoriánský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
(	(	kIx(
<g/>
zdi	zeď	k1gFnPc1
tohoto	tento	k3xDgInSc2
tábora	tábor	k1gInSc2
jsou	být	k5eAaImIp3nP
patrné	patrný	k2eAgNnSc1d1
dodnes	dodnes	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
raném	raný	k2eAgNnSc6d1
císařství	císařství	k1gNnSc6
byli	být	k5eAaImAgMnP
k	k	k7c3
pretoriánské	pretoriánský	k2eAgFnSc3d1
gardě	garda	k1gFnSc3
verbováni	verbován	k2eAgMnPc1d1
muži	muž	k1gMnPc1
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věková	věkový	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
byla	být	k5eAaImAgFnS
od	od	k7c2
17	#num#	k4
do	do	k7c2
23	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
muži	muž	k1gMnPc1
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
několika	několik	k4yIc6
případech	případ	k1gInPc6
ukázali	ukázat	k5eAaPmAgMnP
jako	jako	k9
nespolehliví	spolehlivý	k2eNgMnPc1d1
a	a	k8xC
neloajální	loajální	k2eNgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
k	k	k7c3
moci	moc	k1gFnSc3
dostal	dostat	k5eAaPmAgMnS
Septimius	Septimius	k1gMnSc1
Severus	Severus	k1gMnSc1
propustil	propustit	k5eAaPmAgMnS
stávající	stávající	k2eAgMnPc4d1
pretoriány	pretorián	k1gMnPc4
a	a	k8xC
nahradil	nahradit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
muži	muž	k1gMnPc1
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
dunajských	dunajský	k2eAgFnPc2d1
legií	legie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byli	být	k5eAaImAgMnP
do	do	k7c2
pretoriánské	pretoriánský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
přijímáni	přijímán	k2eAgMnPc1d1
pouze	pouze	k6eAd1
muži	muž	k1gMnPc1
z	z	k7c2
méně	málo	k6eAd2
civilizovaných	civilizovaný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
nepodléhaly	podléhat	k5eNaImAgFnP
romanizaci	romanizace	k1gFnSc4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidání	přidání	k1gNnPc1
k	k	k7c3
pretoriánské	pretoriánský	k2eAgFnSc2d1
gardy	garda	k1gFnSc2
znamenalo	znamenat	k5eAaImAgNnS
celkem	celkem	k6eAd1
časté	častý	k2eAgFnPc4d1
odměny	odměna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legionáři	legionář	k1gMnPc1
sloužili	sloužit	k5eAaImAgMnP
16	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
pobírali	pobírat	k5eAaImAgMnP
dvojnásobný	dvojnásobný	k2eAgInSc4d1
plat	plat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římští	římský	k2eAgMnPc1d1
císaři	císař	k1gMnPc1
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
dávali	dávat	k5eAaImAgMnP
pretoriánům	pretorián	k1gMnPc3
časté	častý	k2eAgFnPc4d1
odměny	odměna	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
získali	získat	k5eAaPmAgMnP
jejich	jejich	k3xOp3gFnSc4
loajalitu	loajalita	k1gFnSc4
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
také	také	k9
často	často	k6eAd1
vyznamenáváni	vyznamenáván	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
pretoriány	pretorián	k1gMnPc7
a	a	k8xC
obyčejnými	obyčejný	k2eAgMnPc7d1
pěšáky	pěšák	k1gMnPc7
byl	být	k5eAaImAgInS
zapříčiněn	zapříčiněn	k2eAgInSc1d1
trvalou	trvalý	k2eAgFnSc7d1
přítomností	přítomnost	k1gFnSc7
pretoriánů	pretorián	k1gMnPc2
v	v	k7c6
Římě	Řím	k1gInSc6
a	a	k8xC
jejich	jejich	k3xOp3gFnSc7
mocí	moc	k1gFnSc7
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
a	a	k8xC
sesazovat	sesazovat	k5eAaImF
císaře	císař	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pretoriánská	pretoriánský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
také	také	k9
představovala	představovat	k5eAaImAgFnS
můstek	můstek	k1gInSc4
z	z	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
mezi	mezi	k7c4
římskou	římský	k2eAgFnSc4d1
elitu	elita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
pretorián	pretorián	k1gMnSc1
sloužil	sloužit	k5eAaImAgMnS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgMnS
se	se	k3xPyFc4
po	po	k7c6
několika	několik	k4yIc6
letech	léto	k1gNnPc6
stát	stát	k5eAaImF,k5eAaPmF
úředníkem	úředník	k1gMnSc7
na	na	k7c6
velitelství	velitelství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgMnS
pretorián	pretorián	k1gMnSc1
velice	velice	k6eAd1
schopný	schopný	k2eAgMnSc1d1
a	a	k8xC
měl	mít	k5eAaImAgInS
mnoho	mnoho	k4c4
zásluh	zásluha	k1gFnPc2
mohl	moct	k5eAaImAgMnS
být	být	k5eAaImF
císařem	císař	k1gMnSc7
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
jako	jako	k9
evocati	evocat	k5eAaImF,k5eAaPmF
augusti	august	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
postu	post	k1gInSc2
mohl	moct	k5eAaImAgInS
stoupat	stoupat	k5eAaImF
po	po	k7c6
profesním	profesní	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
až	až	k9
na	na	k7c4
post	post	k1gInSc4
centuriona	centurio	k1gMnSc2
gardy	garda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
post	post	k1gInSc4
byl	být	k5eAaImAgMnS
nejspíše	nejspíše	k9
konečný	konečný	k2eAgMnSc1d1
a	a	k8xC
pretorián	pretorián	k1gMnSc1
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
podařilo	podařit	k5eAaPmAgNnS
mohl	moct	k5eAaImAgInS
odejít	odejít	k5eAaPmF
na	na	k7c4
odpočinek	odpočinek	k1gInSc4
z	z	k7c2
velkou	velký	k2eAgFnSc4d1
rentou	renta	k1gFnSc7
a	a	k8xC
velkými	velký	k2eAgFnPc7d1
zásluhami	zásluha	k1gFnPc7
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pěchota	pěchota	k1gFnSc1
pozdního	pozdní	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
pěchoty	pěchota	k1gFnSc2
</s>
<s>
Římští	římský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
znázornění	znázornění	k1gNnSc2
na	na	k7c6
Konstantinově	Konstantinův	k2eAgMnSc6d1
obloukuOd	obloukuOd	k6eAd1
konce	konec	k1gInPc4
druhého	druhý	k4xOgNnSc2
století	století	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c4
římskou	římský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
začala	začít	k5eAaPmAgFnS
valit	valit	k5eAaImF
až	až	k9
příliš	příliš	k6eAd1
velká	velký	k2eAgFnSc1d1
řada	řada	k1gFnSc1
nepřátel	nepřítel	k1gMnPc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgInP
kmeny	kmen	k1gInPc1
Germánů	Germán	k1gMnPc2
a	a	k8xC
poté	poté	k6eAd1
vojsko	vojsko	k1gNnSc1
Hunů	Hun	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
východu	východ	k1gInSc2
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
zase	zase	k9
silná	silný	k2eAgFnSc1d1
Sasánovská	Sasánovský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
jako	jako	k9
rovnocenný	rovnocenný	k2eAgMnSc1d1
nepřítel	nepřítel	k1gMnSc1
říši	říš	k1gFnSc6
Římské	římský	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
vnější	vnější	k2eAgInPc4d1
útoky	útok	k1gInPc4
<g/>
,	,	kIx,
kombinované	kombinovaný	k2eAgFnSc2d1
z	z	k7c2
vnitřními	vnitřní	k2eAgInPc7d1
nepokoji	nepokoj	k1gInPc7
nakonec	nakonec	k6eAd1
vedly	vést	k5eAaImAgInP
k	k	k7c3
pádů	pád	k1gInPc2
Římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úpadek	úpadek	k1gInSc1
Římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
byl	být	k5eAaImAgInS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
úpadkem	úpadek	k1gInSc7
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nebyla	být	k5eNaImAgFnS
schopna	schopen	k2eAgFnSc1d1
odrazit	odrazit	k5eAaPmF
nepřátele	nepřítel	k1gMnPc4
a	a	k8xC
byla	být	k5eAaImAgNnP
několikrát	několikrát	k6eAd1
drtivě	drtivě	k6eAd1
poražena	poražen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
římských	římský	k2eAgMnPc2d1
vojevůdců	vojevůdce	k1gMnPc2
a	a	k8xC
císařů	císař	k1gMnPc2
volalo	volat	k5eAaImAgNnS
k	k	k7c3
návratů	návrat	k1gInPc2
ke	k	k7c3
starým	starý	k2eAgFnPc3d1
hodnotám	hodnota	k1gFnPc3
jako	jako	k9
bylo	být	k5eAaImAgNnS
římské	římský	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
a	a	k8xC
římské	římský	k2eAgNnSc1d1
vojenství	vojenství	k1gNnSc1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejznámějším	známý	k2eAgMnSc7d3
spisovatelem	spisovatel	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
volal	volat	k5eAaImAgMnS
k	k	k7c3
návratu	návrat	k1gInSc3
tradičního	tradiční	k2eAgNnSc2d1
římského	římský	k2eAgNnSc2d1
vojenství	vojenství	k1gNnSc2
byl	být	k5eAaImAgMnS
Publius	Publius	k1gMnSc1
Flavius	Flavius	k1gMnSc1
Renatus	Renatus	k1gMnSc1
Vegetius	Vegetius	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
sepsal	sepsat	k5eAaPmAgMnS
dílo	dílo	k1gNnSc4
De	De	k?
Re	re	k9
Militari	Militari	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnost	významnost	k1gFnSc1
tohoto	tento	k3xDgNnSc2
díla	dílo	k1gNnSc2
byla	být	k5eAaImAgFnS
pro	pro	k7c4
římskou	římský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
mizivá	mizivý	k2eAgFnSc1d1
(	(	kIx(
<g/>
římské	římský	k2eAgNnSc1d1
vojenství	vojenství	k1gNnSc1
nadále	nadále	k6eAd1
upadalo	upadat	k5eAaImAgNnS,k5eAaPmAgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
Vegetiovo	Vegetiův	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
bylo	být	k5eAaImAgNnS
nejpoužívanějším	používaný	k2eAgInSc7d3
vojenským	vojenský	k2eAgInSc7d1
dílem	díl	k1gInSc7
celého	celý	k2eAgInSc2d1
středověku	středověk	k1gInSc2
a	a	k8xC
raného	raný	k2eAgInSc2d1
novověku	novověk	k1gInSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vpády	vpád	k1gInPc1
barbarů	barbar	k1gMnPc2
byli	být	k5eAaImAgMnP
ze	z	k7c2
začátku	začátek	k1gInSc2
malé	malý	k2eAgInPc4d1
a	a	k8xC
vždy	vždy	k6eAd1
se	se	k3xPyFc4
je	on	k3xPp3gInPc4
podařilo	podařit	k5eAaPmAgNnS
rychle	rychle	k6eAd1
odrazit	odrazit	k5eAaPmF
a	a	k8xC
zničit	zničit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
těchto	tento	k3xDgInPc6
vpádech	vpád	k1gInPc6
většinou	většinou	k6eAd1
následoval	následovat	k5eAaImAgInS
římský	římský	k2eAgInSc4d1
nájezdy	nájezd	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgInP
dotyčné	dotyčný	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
či	či	k8xC
národy	národ	k1gInPc1
potrestány	potrestán	k2eAgInPc1d1
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
troufalost	troufalost	k1gFnSc4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
tyto	tento	k3xDgInPc4
nájezdy	nájezd	k1gInPc4
zesílily	zesílit	k5eAaPmAgInP
a	a	k8xC
barbaři	barbar	k1gMnPc1
se	se	k3xPyFc4
při	při	k7c6
nich	on	k3xPp3gInPc6
více	hodně	k6eAd2
než	než	k8xS
na	na	k7c4
ničení	ničení	k1gNnSc4
a	a	k8xC
rabování	rabování	k1gNnSc4
zaměřili	zaměřit	k5eAaPmAgMnP
na	na	k7c4
osidlování	osidlování	k1gNnSc4
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
se	se	k3xPyFc4
pokoušelo	pokoušet	k5eAaImAgNnS
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
zlepšit	zlepšit	k5eAaPmF
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
mnohokrát	mnohokrát	k6eAd1
napadl	napadnout	k5eAaPmAgMnS
Germánii	Germánie	k1gFnSc4
a	a	k8xC
snažil	snažit	k5eAaImAgInS
se	se	k3xPyFc4
tamní	tamní	k2eAgInSc1d1
Germány	Germán	k1gMnPc7
přimět	přimět	k5eAaPmF
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
vzdali	vzdát	k5eAaPmAgMnP
nájezdů	nájezd	k1gInPc2
na	na	k7c4
římské	římský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Septimius	Septimius	k1gMnSc1
Severus	Severus	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgInS
síť	síť	k1gFnSc4
mobilních	mobilní	k2eAgFnPc2d1
záloh	záloha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
měly	mít	k5eAaImAgFnP
zastavit	zastavit	k5eAaPmF
barbarské	barbarský	k2eAgInPc4d1
vpády	vpád	k1gInPc4
již	již	k6eAd1
v	v	k7c6
zárodku	zárodek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dioklecián	Dioklecián	k1gMnSc1
provedl	provést	k5eAaPmAgMnS
velice	velice	k6eAd1
zásadní	zásadní	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
v	v	k7c6
oboru	obor	k1gInSc6
vojenství	vojenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšířil	rozšířit	k5eAaPmAgMnS
odvody	odvod	k1gInPc4
(	(	kIx(
<g/>
služba	služba	k1gFnSc1
v	v	k7c6
armádě	armáda	k1gFnSc6
byla	být	k5eAaImAgFnS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
povinná	povinný	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
tím	ten	k3xDgNnSc7
zvětšil	zvětšit	k5eAaPmAgMnS
armádu	armáda	k1gFnSc4
na	na	k7c4
500	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstantin	Konstantin	k1gMnSc1
Veliký	veliký	k2eAgInSc1d1
vytvořil	vytvořit	k5eAaPmAgInS
síť	síť	k1gFnSc4
pohraničních	pohraniční	k2eAgFnPc2d1
posádek	posádka	k1gFnPc2
(	(	kIx(
<g/>
limitanei	limitanei	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
měly	mít	k5eAaImAgInP
řešit	řešit	k5eAaImF
malé	malý	k2eAgInPc1d1
vpády	vpád	k1gInPc1
barbarů	barbar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
větší	veliký	k2eAgInSc4d2
vpád	vpád	k1gInSc4
měly	mít	k5eAaImAgInP
tyto	tento	k3xDgFnPc4
jednotky	jednotka	k1gFnPc4
obsazovat	obsazovat	k5eAaImF
opevněná	opevněný	k2eAgNnPc4d1
města	město	k1gNnPc4
či	či	k8xC
pevnosti	pevnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comitatenses	Comitatensesa	k1gFnPc2
byly	být	k5eAaImAgFnP
mobilní	mobilní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
co	co	k9
nejdřív	dříve	k6eAd3
shromáždit	shromáždit	k5eAaPmF
a	a	k8xC
porazit	porazit	k5eAaPmF
barbarské	barbarský	k2eAgFnPc4d1
armády	armáda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejradikálnější	radikální	k2eAgFnSc7d3
změnou	změna	k1gFnSc7
byla	být	k5eAaImAgFnS
zcela	zcela	k6eAd1
jistě	jistě	k6eAd1
Diokleciánova	Diokleciánův	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služba	služba	k1gFnSc1
v	v	k7c6
armádě	armáda	k1gFnSc6
byla	být	k5eAaImAgFnS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
značně	značně	k6eAd1
nepopulární	populární	k2eNgFnPc1d1
a	a	k8xC
existují	existovat	k5eAaImIp3nP
záznamy	záznam	k1gInPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
si	se	k3xPyFc3
římští	římský	k2eAgMnPc1d1
občané	občan	k1gMnPc1
radši	rád	k6eAd2
způsobovali	způsobovat	k5eAaImAgMnP
zranění	zraněný	k2eAgMnPc1d1
(	(	kIx(
<g/>
usekávali	usekávat	k5eAaImAgMnP
si	se	k3xPyFc3
prsty	prst	k1gInPc1
i	i	k9
celá	celý	k2eAgNnPc1d1
zápěstí	zápěstí	k1gNnPc1
<g/>
)	)	kIx)
než	než	k8xS
aby	aby	kYmCp3nP
nastoupili	nastoupit	k5eAaPmAgMnP
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
takových	takový	k3xDgFnPc2
změn	změna	k1gFnPc2
byl	být	k5eAaImAgInS
úpadek	úpadek	k1gInSc1
kvality	kvalita	k1gFnSc2
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pěšáci	pěšák	k1gMnPc1
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
již	již	k6eAd1
nebyli	být	k5eNaImAgMnP
takový	takový	k3xDgInSc4
jací	jaký	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
jejich	jejich	k3xOp3gNnSc4
předchůdci	předchůdce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytratila	vytratit	k5eAaPmAgFnS
ze	z	k7c2
veškerá	veškerý	k3xTgFnSc1
disciplína	disciplína	k1gFnSc1
a	a	k8xC
vlastenectví	vlastenectví	k1gNnSc1
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
bylo	být	k5eAaImAgNnS
dávnou	dávný	k2eAgFnSc7d1
minulostí	minulost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
jako	jako	k9
osudové	osudový	k2eAgNnSc1d1
v	v	k7c6
mnoha	mnoho	k4c2
bitvách	bitva	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časté	častý	k2eAgInPc1d1
byly	být	k5eAaImAgInP
útěky	útěk	k1gInPc1
z	z	k7c2
bitevního	bitevní	k2eAgNnSc2d1
pole	pole	k1gNnSc2
a	a	k8xC
mnoho	mnoho	k4c1
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
zběhlo	zběhnout	k5eAaPmAgNnS
k	k	k7c3
nepříteli	nepřítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
pěšáků	pěšák	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byla	být	k5eAaImAgFnS
tvořena	tvořen	k2eAgFnSc1d1
z	z	k7c2
obyvatel	obyvatel	k1gMnPc2
Galie	Galie	k1gFnSc2
a	a	k8xC
Germánie	Germánie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
z	z	k7c2
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
vytratila	vytratit	k5eAaPmAgFnS
uniformita	uniformita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legie	legie	k1gFnSc1
jako	jako	k8xC,k8xS
pravidelná	pravidelný	k2eAgFnSc1d1
formace	formace	k1gFnSc1
začala	začít	k5eAaPmAgFnS
ztrácet	ztrácet	k5eAaImF
svůj	svůj	k3xOyFgInSc4
význam	význam	k1gInSc4
a	a	k8xC
počet	počet	k1gInSc1
římských	římský	k2eAgMnPc2d1
pěšáků	pěšák	k1gMnPc2
v	v	k7c6
legii	legie	k1gFnSc6
byl	být	k5eAaImAgMnS
proměnlivý	proměnlivý	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
a	a	k8xC
bojová	bojový	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
legionáře	legionář	k1gMnSc2
z	z	k7c2
konce	konec	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
stoletíVýzbroj	stoletíVýzbroj	k1gInSc1
a	a	k8xC
bojová	bojový	k2eAgFnSc1d1
technika	technika	k1gFnSc1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
se	se	k3xPyFc4
od	od	k7c2
dob	doba	k1gFnPc2
raného	raný	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
velice	velice	k6eAd1
změnila	změnit	k5eAaPmAgFnS
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velkému	velký	k2eAgInSc3d1
úpadku	úpadek	k1gInSc3
a	a	k8xC
zjednodušení	zjednodušení	k1gNnSc3
bojové	bojový	k2eAgFnSc2d1
taktiky	taktika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvod	důvod	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
římské	římský	k2eAgFnSc3d1
pěchotě	pěchota	k1gFnSc3
byli	být	k5eAaImAgMnP
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
dál	daleko	k6eAd2
více	hodně	k6eAd2
přijímáni	přijímán	k2eAgMnPc1d1
barbaři	barbar	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
vojáci	voják	k1gMnPc1
nebyli	být	k5eNaImAgMnP
vystrojeni	vystrojit	k5eAaPmNgMnP
ani	ani	k8xC
vycvičeni	vycvičit	k5eAaPmNgMnP
jako	jako	k8xS,k8xC
dřívější	dřívější	k2eAgMnPc1d1
římský	římský	k2eAgMnSc1d1
pěšák	pěšák	k1gMnSc1
(	(	kIx(
<g/>
nebylo	být	k5eNaImAgNnS
dost	dost	k6eAd1
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1
učitelů	učitel	k1gMnPc2
a	a	k8xC
peněz	peníze	k1gInPc2
na	na	k7c4
drahou	drahý	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
se	se	k3xPyFc4
nedostávalo	dostávat	k5eNaImAgNnS
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
tzv.	tzv.	kA
barbarizaci	barbarizace	k1gFnSc4
římského	římský	k2eAgNnSc2d1
vojenství	vojenství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgFnPc1d1
římské	římský	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
jako	jako	k8xC,k8xS
gladius	gladius	k1gInSc1
<g/>
,	,	kIx,
pilum	pilum	k1gNnSc1
a	a	k8xC
scutum	scutum	k1gNnSc1
vymizely	vymizet	k5eAaPmAgFnP
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barbaři	barbar	k1gMnPc1
si	se	k3xPyFc3
při	při	k7c6
službě	služba	k1gFnSc6
v	v	k7c6
římské	římský	k2eAgFnSc6d1
pěchotě	pěchota	k1gFnSc6
ponechávali	ponechávat	k5eAaImAgMnP
své	svůj	k3xOyFgFnPc4
národní	národní	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
pěšáka	pěšák	k1gMnSc2
byl	být	k5eAaImAgInS
dlouhý	dlouhý	k2eAgInSc1d1
meč	meč	k1gInSc1
spatha	spatha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
byl	být	k5eAaImAgInS
meč	meč	k1gInSc1
spatha	spatha	k1gMnSc1
používán	používán	k2eAgMnSc1d1
převážně	převážně	k6eAd1
jako	jako	k8xC,k8xS
zbraň	zbraň	k1gFnSc1
jezdců	jezdec	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
postupem	postup	k1gInSc7
času	čas	k1gInSc2
ho	on	k3xPp3gInSc4
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
i	i	k9
pěchota	pěchota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spatha	Spatha	k1gMnSc1
byl	být	k5eAaImAgInS
dlouhý	dlouhý	k2eAgInSc4d1
70,5	70,5	k4
centimetrů	centimetr	k1gInPc2
(	(	kIx(
<g/>
gladius	gladius	k1gInSc1
pouze	pouze	k6eAd1
50	#num#	k4
centimetrů	centimetr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc1
měl	mít	k5eAaImAgInS
pouze	pouze	k6eAd1
krátký	krátký	k2eAgInSc1d1
hrot	hrot	k1gInSc1
což	což	k3yQnSc4,k3yRnSc4
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
používán	používat	k5eAaImNgInS
k	k	k7c3
sekání	sekání	k1gNnSc3
(	(	kIx(
<g/>
tedy	tedy	k8xC
tradičnímu	tradiční	k2eAgInSc3d1
stylu	styl	k1gInSc3
boje	boj	k1gInSc2
barbarů	barbar	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bodný	bodný	k2eAgInSc1d1
útok	útok	k1gInSc1
byl	být	k5eAaImAgInS
zavržen	zavrhnout	k5eAaPmNgInS
(	(	kIx(
<g/>
bodný	bodný	k2eAgInSc1d1
útok	útok	k1gInSc1
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
pěšáci	pěšák	k1gMnPc1
naučit	naučit	k5eAaPmF
<g/>
,	,	kIx,
sekání	sekání	k1gNnPc4
nikoliv	nikoliv	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
považuje	považovat	k5eAaImIp3nS
Vegetius	Vegetius	k1gInSc4
za	za	k7c4
politování	politování	k1gNnSc4
hodné	hodný	k2eAgFnSc2d1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
oštěpu	oštěp	k1gInSc2
pilum	pilum	k1gInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
jiný	jiný	k2eAgInSc1d1
typ	typ	k1gInSc1
oštěpu	oštěp	k1gInSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
spiculum	spiculum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiculum	Spiculum	k1gInSc1
byl	být	k5eAaImAgInS
oštěp	oštěp	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
používali	používat	k5eAaImAgMnP
barbaři	barbar	k1gMnPc1
a	a	k8xC
byl	být	k5eAaImAgMnS
méně	málo	k6eAd2
účinnější	účinný	k2eAgMnSc1d2
než	než	k8xS
římské	římský	k2eAgNnSc1d1
pilum	pilum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
pěšáků	pěšák	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zanechalo	zanechat	k5eAaPmAgNnS
nošení	nošení	k1gNnSc4
brnění	brnění	k1gNnSc2
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
přileb	přilba	k1gFnPc2
(	(	kIx(
<g/>
opět	opět	k6eAd1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
vidět	vidět	k5eAaImF
vliv	vliv	k1gInSc4
barbarů	barbar	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
osudné	osudný	k2eAgNnSc1d1
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
jízdními	jízdní	k2eAgMnPc7d1
lučištníky	lučištník	k1gMnPc7
Hunů	Hun	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přilba	přilba	k1gFnSc1
byl	být	k5eAaImAgInS
vyrobena	vyrobit	k5eAaPmNgFnS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
polovin	polovina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
k	k	k7c3
sobě	se	k3xPyFc3
připojily	připojit	k5eAaPmAgFnP
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
tedy	tedy	k9
uzpůsobena	uzpůsobit	k5eAaPmNgFnS
k	k	k7c3
masové	masový	k2eAgFnSc3d1
výrobě	výroba	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
postrádala	postrádat	k5eAaImAgFnS
kvalitu	kvalita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
brnění	brnění	k1gNnSc2
se	se	k3xPyFc4
římský	římský	k2eAgMnSc1d1
pěšák	pěšák	k1gMnSc1
plně	plně	k6eAd1
spoléhal	spoléhat	k5eAaImAgMnS
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
štít	štít	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
většinou	většinou	k6eAd1
oválný	oválný	k2eAgInSc1d1
<g/>
,	,	kIx,
vyroben	vyroben	k2eAgInSc1d1
z	z	k7c2
vrstveného	vrstvený	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
a	a	k8xC
pokryt	pokryt	k2eAgInSc1d1
surovou	surový	k2eAgFnSc7d1
kůží	kůže	k1gFnSc7
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manipulového	Manipulový	k2eAgNnSc2d1
(	(	kIx(
<g/>
též	též	k6eAd1
šachovnicového	šachovnicový	k2eAgNnSc2d1
<g/>
)	)	kIx)
válečnictví	válečnictví	k1gNnSc2
se	se	k3xPyFc4
již	již	k6eAd1
nepoužívalo	používat	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
nebyli	být	k5eNaImAgMnP
dostatečně	dostatečně	k6eAd1
vycvičeni	vycvičit	k5eAaPmNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
dokázali	dokázat	k5eAaPmAgMnP
vytvořit	vytvořit	k5eAaPmF
dokonalý	dokonalý	k2eAgInSc4d1
manipul	manipul	k1gInSc4
a	a	k8xC
ještě	ještě	k9
další	další	k2eAgFnPc4d1
římské	římský	k2eAgFnPc4d1
formace	formace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
manipulu	manipul	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
vytvořena	vytvořit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
linie	linie	k1gFnSc1
pěchoty	pěchota	k1gFnSc2
(	(	kIx(
<g/>
nejspíše	nejspíše	k9
perská	perský	k2eAgFnSc1d1
linie	linie	k1gFnSc1
sparabara	sparabara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střelecká	střelecký	k2eAgFnSc1d1
síla	síla	k1gFnSc1
pěchoty	pěchota	k1gFnSc2
byla	být	k5eAaImAgFnS
velice	velice	k6eAd1
zvětšena	zvětšen	k2eAgFnSc1d1
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zabráněno	zabráněn	k2eAgNnSc1d1
přímému	přímý	k2eAgInSc3d1
útoku	útok	k1gInSc3
nepřítele	nepřítel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střelecká	střelecký	k2eAgFnSc1d1
síla	síla	k1gFnSc1
pěchoty	pěchota	k1gFnSc2
rozhodla	rozhodnout	k5eAaPmAgFnS
mnoho	mnoho	k4c4
bitev	bitva	k1gFnPc2
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
nebyl	být	k5eNaImAgMnS
skoro	skoro	k6eAd1
žádný	žádný	k3yNgInSc4
rozdíl	rozdíl	k1gInSc4
mezi	mezi	k7c7
římským	římský	k2eAgMnSc7d1
a	a	k8xC
germánským	germánský	k2eAgMnSc7d1
pěšákem	pěšák	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojová	bojový	k2eAgFnSc1d1
formace	formace	k1gFnSc1
byla	být	k5eAaImAgFnS
též	též	k9
změněna	změněn	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgFnPc6
řadách	řada	k1gFnPc6
byli	být	k5eAaImAgMnP
umístěni	umístěn	k2eAgMnPc1d1
muži	muž	k1gMnPc1
těžké	těžký	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušníci	příslušník	k1gMnPc1
těžké	těžký	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
byli	být	k5eAaImAgMnP
většinou	většina	k1gFnSc7
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP
mečem	meč	k1gInSc7
spatha	spath	k1gMnSc2
a	a	k8xC
oštěpem	oštěp	k1gInSc7
spiculum	spiculum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
ve	v	k7c6
většině	většina	k1gFnSc6
případech	případ	k1gInPc6
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
starší	starší	k1gMnPc4
a	a	k8xC
zkušené	zkušený	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
řadě	řada	k1gFnSc6
byli	být	k5eAaImAgMnP
lučištníci	lučištník	k1gMnPc1
příslušníci	příslušník	k1gMnPc1
lehké	lehký	k2eAgFnSc2d1
a	a	k8xC
střední	střední	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
lehčí	lehký	k2eAgFnSc4d2
zbroj	zbroj	k1gFnSc4
a	a	k8xC
také	také	k9
lehčí	lehký	k2eAgInSc4d2
oštěp	oštěp	k1gInSc4
zvaný	zvaný	k2eAgInSc1d1
vericulum	vericulum	k1gInSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
třetí	třetí	k4xOgNnSc4
a	a	k8xC
čtvrté	čtvrtý	k4xOgNnSc4
řadě	řada	k1gFnSc6
byli	být	k5eAaImAgMnP
umístěni	umístěn	k2eAgMnPc1d1
kopiníci	kopiník	k1gMnPc1
<g/>
,	,	kIx,
lučištníci	lučištník	k1gMnPc1
<g/>
,	,	kIx,
prakovníci	prakovník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
započala	započnout	k5eAaPmAgFnS
bitvy	bitva	k1gFnPc4
<g/>
,	,	kIx,
vystoupili	vystoupit	k5eAaPmAgMnP
tito	tento	k3xDgMnPc1
vojáci	voják	k1gMnPc1
kupředu	kupředu	k6eAd1
<g/>
,	,	kIx,
vypálili	vypálit	k5eAaPmAgMnP
své	svůj	k3xOyFgInPc4
střely	střel	k1gInPc4
na	na	k7c4
útočícího	útočící	k2eAgMnSc4d1
nepřítele	nepřítel	k1gMnSc4
a	a	k8xC
stáhli	stáhnout	k5eAaPmAgMnP
se	se	k3xPyFc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgFnPc6d1
řadách	řada	k1gFnPc6
stáli	stát	k5eAaImAgMnP
další	další	k2eAgMnPc1d1
příslušníci	příslušník	k1gMnPc1
střeleckých	střelecký	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
a	a	k8xC
římské	římský	k2eAgFnSc2d1
artilerie	artilerie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
nebyla	být	k5eNaImAgFnS
používána	používán	k2eAgFnSc1d1
balista	balista	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jednodušší	jednoduchý	k2eAgMnSc1d2
(	(	kIx(
<g/>
méně	málo	k6eAd2
přesný	přesný	k2eAgInSc1d1
<g/>
)	)	kIx)
onager	onager	k1gInSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
divoký	divoký	k2eAgMnSc1d1
osel	osel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
jednoramenný	jednoramenný	k2eAgInSc4d1
katapult	katapult	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
zpětný	zpětný	k2eAgInSc1d1
ráz	ráz	k1gInSc1
byl	být	k5eAaImAgInS
přirovnáván	přirovnávat	k5eAaImNgInS
ke	k	k7c3
kopnutí	kopnutí	k1gNnSc3
osla	osel	k1gMnSc2
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
název	název	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
útočila	útočit	k5eAaImAgFnS
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
vzácný	vzácný	k2eAgInSc4d1
pohled	pohled	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
používána	používán	k2eAgFnSc1d1
formace	formace	k1gFnSc1
klínu	klín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
této	tento	k3xDgFnSc6
formaci	formace	k1gFnSc6
vytvořili	vytvořit	k5eAaPmAgMnP
pěšáci	pěšák	k1gMnPc1
jakýsi	jakýsi	k3yIgInSc4
trojúhelník	trojúhelník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tyto	tento	k3xDgFnPc1
změny	změna	k1gFnPc1
měly	mít	k5eAaImAgFnP
za	za	k7c4
následek	následek	k1gInSc4
pád	pád	k1gInSc4
Římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
konec	konec	k1gInSc1
starověké	starověký	k2eAgFnSc2d1
římské	římský	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Starověká	starověký	k2eAgFnSc1d1
římská	římský	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Starověký	starověký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
</s>
<s>
Římská	římský	k2eAgFnSc1d1
legie	legie	k1gFnSc1
</s>
<s>
Gladius	Gladius	k1gMnSc1
</s>
<s>
Centurie	centurie	k1gFnSc1
</s>
<s>
Kohorta	kohorta	k1gFnSc1
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
</s>
<s>
Jízdní	jízdní	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
starověku	starověk	k1gInSc2
</s>
<s>
Obléhací	obléhací	k2eAgFnPc1d1
války	válka	k1gFnPc1
starověku	starověk	k1gInSc2
</s>
<s>
Námořní	námořní	k2eAgFnPc1d1
války	válka	k1gFnPc1
starověku	starověk	k1gInSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ANGLIM	ANGLIM	kA
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojové	bojový	k2eAgFnPc4d1
techniky	technika	k1gFnPc4
starověkého	starověký	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Deus	Deus	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
41	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Anglim	Anglima	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
28.1	28.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Anglim	Anglima	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
42.1	42.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Anglim	Anglima	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
43	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PENROSE	PENROSE	kA
<g/>
,	,	kIx,
Jane	Jan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řím	Řím	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
nepřátelé	nepřítel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Miroslav	Miroslav	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
26	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Penrose	Penrosa	k1gFnSc3
<g/>
,	,	kIx,
str	str	kA
26.1	26.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Penrose	Penrosa	k1gFnSc3
<g/>
,	,	kIx,
str	str	kA
27.1	27.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
HUF	huf	k0
<g/>
,	,	kIx,
Hans	Hans	k1gMnSc1
-	-	kIx~
Christian	Christian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmocnější	mocný	k2eAgFnSc1d3
říše	říše	k1gFnSc1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frýdek	Frýdek	k1gInSc1
Místek	Místek	k1gInSc4
<g/>
:	:	kIx,
Alpress	Alpress	k1gInSc1
s.	s.	k?
r.	r.	kA
o.	o.	k?
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
125	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
Huff	Huff	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
123.1	123.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
Huff	Huff	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
128.1	128.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Anglim	Anglima	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
63.1	63.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
7	#num#	k4
8	#num#	k4
Anglim	Anglima	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
65.1	65.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Penrose	Penrosa	k1gFnSc3
<g/>
,	,	kIx,
str	str	kA
176.1	176.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Penrose	Penrosa	k1gFnSc3
<g/>
,	,	kIx,
str	str	kA
177.1	177.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
Anglim	Anglima	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
68.1	68.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
Anglim	Anglima	k1gFnPc2
<g/>
,	,	kIx,
str	str	kA
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PENROSE	PENROSE	kA
<g/>
,	,	kIx,
Jane	Jan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řím	Řím	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
nepřátelé	nepřítel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Miroslav	Miroslav	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86977	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GOLDSWORTH	GOLDSWORTH	kA
<g/>
,	,	kIx,
Adrian	Adrian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římské	římský	k2eAgFnPc4d1
válečnictví	válečnictví	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Cassell	Cassell	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
304	#num#	k4
<g/>
-	-	kIx~
<g/>
35265	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CONNOLLY	CONNOLLY	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecké	řecký	k2eAgInPc1d1
a	a	k8xC
římské	římský	k2eAgNnSc4d1
umění	umění	k1gNnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Greeenhillské	Greeenhillský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TACITUS	TACITUS	kA
<g/>
,	,	kIx,
Publius	Publius	k1gMnSc1
Cornelius	Cornelius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dějin	dějiny	k1gFnPc2
císařského	císařský	k2eAgInSc2d1
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BURIAN	Burian	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římské	římský	k2eAgInPc1d1
impérium	impérium	k1gNnSc1
<g/>
:	:	kIx,
vrchol	vrchol	k1gInSc1
a	a	k8xC
proměny	proměna	k1gFnPc1
antické	antický	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LE	LE	kA
BEHOC	BEHOC	kA
<g/>
,	,	kIx,
Yann	Yann	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imperiální	imperiální	k2eAgFnSc1d1
římská	římský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
Batsford	Batsford	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KUBELKA	Kubelka	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římské	římský	k2eAgFnPc4d1
reálie	reálie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
:	:	kIx,
vl	vl	k?
<g/>
.	.	kIx.
<g/>
n.	n.	k?
<g/>
,	,	kIx,
1898	#num#	k4
<g/>
.	.	kIx.
196	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
-	-	kIx~
kapitola	kapitola	k1gFnSc1
Vojenství	vojenství	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
56	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Antika	antika	k1gFnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
