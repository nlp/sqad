<s>
Italské	italský	k2eAgInPc1d1
espresso	espressa	k1gFnSc5
je	být	k5eAaImIp3nS
ze	z	k7c2
7	[number]	k4
g	g	kA
pomleté	pomletý	k2eAgFnSc2d1
kávy	káva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1
je	být	k5eAaImIp3nS
protlačena	protlačit	k5eAaPmNgFnS
vodou	voda	k1gFnSc7
o	o	k7c6
tlaku	tlak	k1gInSc6
8-10	[number]	k4
barů	bar	k1gInPc2
a	a	k8xC
teplotě	teplota	k1gFnSc6
88-90	[number]	k4
°	°	k?
<g/>
C.	C.	kA
Vytéká	vytékat	k5eAaImIp3nS
cca	cca	kA
25	[number]	k4
sekund	sekunda	k1gFnPc2
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
30	[number]	k4
ml	ml	kA
tekutiny	tekutina	k1gFnSc2
<g/>
.	.	kIx.
</s>