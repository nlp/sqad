<s>
Caffè	Caffè	k?	Caffè
espresso	espressa	k1gFnSc5	espressa
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
dosl.	dosl.	k?	dosl.
"	"	kIx"	"
<g/>
káva	káva	k1gFnSc1	káva
na	na	k7c4	na
vyžádání	vyžádání	k1gNnPc4	vyžádání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kávový	kávový	k2eAgInSc4d1	kávový
nápoj	nápoj	k1gInSc4	nápoj
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
možné	možný	k2eAgNnSc1d1	možné
setkávat	setkávat	k5eAaImF	setkávat
se	s	k7c7	s
zkomoleninami	zkomolenina	k1gFnPc7	zkomolenina
preso	presa	k1gFnSc5	presa
<g/>
,	,	kIx,	,
či	či	k9wB	či
presso	pressa	k1gFnSc5	pressa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
expreso	expresa	k1gFnSc5	expresa
<g/>
,	,	kIx,	,
taková	takový	k3xDgNnPc4	takový
označení	označení	k1gNnPc4	označení
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
nesprávná	správný	k2eNgFnSc1d1	nesprávná
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
prostředí	prostředí	k1gNnSc6	prostředí
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
či	či	k8xC	či
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
nápoj	nápoj	k1gInSc4	nápoj
termín	termín	k1gInSc1	termín
piccolo	piccola	k1gFnSc5	piccola
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc4d1	malý
<g/>
"	"	kIx"	"
–	–	k?	–
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
považován	považován	k2eAgInSc1d1	považován
některými	některý	k3yIgFnPc7	některý
experty	expert	k1gMnPc4	expert
za	za	k7c4	za
nesmyslný	smyslný	k2eNgInSc4d1	nesmyslný
<g/>
,	,	kIx,	,
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
řetězce	řetězec	k1gInSc2	řetězec
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nabídce	nabídka	k1gFnSc6	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
původ	původ	k1gInSc4	původ
výrazu	výraz	k1gInSc2	výraz
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
přesném	přesný	k2eAgNnSc6d1	přesné
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
espresso	espressa	k1gFnSc5	espressa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
debaty	debata	k1gFnPc1	debata
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
prostě	prostě	k9	prostě
"	"	kIx"	"
<g/>
vytlačený	vytlačený	k2eAgMnSc1d1	vytlačený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
espresso	espressa	k1gFnSc5	espressa
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
původem	původ	k1gInSc7	původ
příbuzné	příbuzná	k1gFnSc2	příbuzná
např.	např.	kA	např.
s	s	k7c7	s
výrazem	výraz	k1gInSc7	výraz
"	"	kIx"	"
<g/>
express	express	k1gInSc1	express
<g/>
"	"	kIx"	"
-	-	kIx~	-
expresní	expresní	k2eAgInSc1d1	expresní
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
tady	tady	k6eAd1	tady
a	a	k8xC	a
teď	teď	k6eAd1	teď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
rychle	rychle	k6eAd1	rychle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
rovněž	rovněž	k9	rovněž
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
rychlým	rychlý	k2eAgInSc7d1	rychlý
způsobem	způsob	k1gInSc7	způsob
přípravy	příprava	k1gFnSc2	příprava
kávy	káva	k1gFnSc2	káva
espresso	espressa	k1gFnSc5	espressa
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
výraz	výraz	k1gInSc1	výraz
espresso	espressa	k1gFnSc5	espressa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
příčestím	příčestí	k1gNnSc7	příčestí
(	(	kIx(	(
<g/>
z	z	k7c2	z
inf.	inf.	k?	inf.
esprimere	esprimrat	k5eAaPmIp3nS	esprimrat
-	-	kIx~	-
"	"	kIx"	"
<g/>
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
"	"	kIx"	"
<g/>
vytlačit	vytlačit	k5eAaPmF	vytlačit
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
snad	snad	k9	snad
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
přání	přání	k1gNnSc1	přání
<g/>
,	,	kIx,	,
pocit	pocit	k1gInSc1	pocit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
ovšem	ovšem	k9	ovšem
o	o	k7c6	o
kávě	káva	k1gFnSc6	káva
obvykle	obvykle	k6eAd1	obvykle
hovoří	hovořit	k5eAaImIp3nS	hovořit
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
caffè	caffè	k?	caffè
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
káva	káva	k1gFnSc1	káva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
právě	právě	k9	právě
způsob	způsob	k1gInSc1	způsob
"	"	kIx"	"
<g/>
espresso	espressa	k1gFnSc5	espressa
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
nejběžněji	běžně	k6eAd3	běžně
konzumovanou	konzumovaný	k2eAgFnSc7d1	konzumovaná
formou	forma	k1gFnSc7	forma
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
se	se	k3xPyFc4	se
však	však	k9	však
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
dobám	doba	k1gFnPc3	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
espresso	espressa	k1gFnSc5	espressa
připraveno	připravit	k5eAaPmNgNnS	připravit
a	a	k8xC	a
podáváno	podávat	k5eAaImNgNnS	podávat
na	na	k7c6	na
barech	bar	k1gInPc6	bar
'	'	kIx"	'
<g/>
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
all	all	k?	all
<g/>
'	'	kIx"	'
<g/>
espresso	espressa	k1gFnSc5	espressa
<g/>
)	)	kIx)	)
zákazníka	zákazník	k1gMnSc4	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
tedy	tedy	k9	tedy
nemá	mít	k5eNaImIp3nS	mít
název	název	k1gInSc4	název
souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
kávy	káva	k1gFnSc2	káva
připravuje	připravovat	k5eAaImIp3nS	připravovat
(	(	kIx(	(
<g/>
ex-	ex-	k?	ex-
=	=	kIx~	=
vy-	vy-	k?	vy-
<g/>
,	,	kIx,	,
presso	pressa	k1gFnSc5	pressa
=	=	kIx~	=
tlačit	tlačit	k5eAaImF	tlačit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Italské	italský	k2eAgInPc1d1	italský
espresso	espressa	k1gFnSc5	espressa
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
7	[number]	k4	7
g	g	kA	g
pomleté	pomletý	k2eAgFnSc2d1	pomletá
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
protlačena	protlačit	k5eAaPmNgFnS	protlačit
vodou	voda	k1gFnSc7	voda
o	o	k7c6	o
tlaku	tlak	k1gInSc6	tlak
8-10	[number]	k4	8-10
barů	bar	k1gInPc2	bar
a	a	k8xC	a
teplotě	teplota	k1gFnSc6	teplota
88-90	[number]	k4	88-90
°	°	k?	°
<g/>
C.	C.	kA	C.
Vytéká	vytékat	k5eAaImIp3nS	vytékat
cca	cca	kA	cca
25	[number]	k4	25
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
30	[number]	k4	30
ml	ml	kA	ml
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
je	být	k5eAaImIp3nS	být
kréma	kréma	k1gFnSc1	kréma
(	(	kIx(	(
<g/>
créma	créma	k1gFnSc1	créma
<g/>
)	)	kIx)	)
barvy	barva	k1gFnSc2	barva
lískového	lískový	k2eAgInSc2d1	lískový
oříšku	oříšek	k1gInSc2	oříšek
silná	silný	k2eAgFnSc1d1	silná
cca	cca	kA	cca
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Tmavší	tmavý	k2eAgFnSc1d2	tmavší
barva	barva	k1gFnSc1	barva
pěny	pěna	k1gFnSc2	pěna
znamená	znamenat	k5eAaImIp3nS	znamenat
příliš	příliš	k6eAd1	příliš
vysoký	vysoký	k2eAgInSc1d1	vysoký
tlak	tlak	k1gInSc1	tlak
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
při	při	k7c6	při
filtraci	filtrace	k1gFnSc6	filtrace
<g/>
,	,	kIx,	,
daný	daný	k2eAgInSc4d1	daný
např.	např.	kA	např.
velkým	velký	k2eAgNnSc7d1	velké
manuálním	manuální	k2eAgNnSc7d1	manuální
stlačením	stlačení	k1gNnSc7	stlačení
kávy	káva	k1gFnSc2	káva
ve	v	k7c6	v
filtru	filtr	k1gInSc6	filtr
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
jemným	jemný	k2eAgNnSc7d1	jemné
namletím	namletí	k1gNnSc7	namletí
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hořká	hořký	k2eAgFnSc1d1	hořká
nebo	nebo	k8xC	nebo
také	také	k9	také
"	"	kIx"	"
<g/>
přepálená	přepálený	k2eAgFnSc1d1	přepálená
<g/>
"	"	kIx"	"
chuť	chuť	k1gFnSc4	chuť
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Dodržení	dodržení	k1gNnSc1	dodržení
všech	všecek	k3xTgFnPc2	všecek
podmínek	podmínka	k1gFnPc2	podmínka
tlaků	tlak	k1gInPc2	tlak
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
extrakci	extrakce	k1gFnSc4	extrakce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
chybuje	chybovat	k5eAaImIp3nS	chybovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
přípravy	příprava	k1gFnSc2	příprava
a	a	k8xC	a
objemu	objem	k1gInSc2	objem
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Správné	správný	k2eAgNnSc1d1	správné
espresso	espressa	k1gFnSc5	espressa
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
25-30	[number]	k4	25-30
ml	ml	kA	ml
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
hrníčku	hrníček	k1gInSc6	hrníček
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
snahou	snaha	k1gFnSc7	snaha
nechat	nechat	k5eAaPmF	nechat
kávu	káva	k1gFnSc4	káva
protékat	protékat	k5eAaImF	protékat
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
nesprávnou	správný	k2eNgFnSc4d1	nesprávná
chuť	chuť	k1gFnSc4	chuť
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
větší	veliký	k2eAgInSc4d2	veliký
objem	objem	k1gInSc4	objem
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
kávu	káva	k1gFnSc4	káva
doplnit	doplnit	k5eAaPmF	doplnit
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
americano	americana	k1gFnSc5	americana
-	-	kIx~	-
káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
zředěná	zředěný	k2eAgFnSc1d1	zředěná
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kávu	káva	k1gFnSc4	káva
připravit	připravit	k5eAaPmF	připravit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
až	až	k6eAd1	až
tří	tři	k4xCgFnPc2	tři
dávek	dávka	k1gFnPc2	dávka
(	(	kIx(	(
<g/>
14	[number]	k4	14
g	g	kA	g
resp.	resp.	kA	resp.
21	[number]	k4	21
g	g	kA	g
<g/>
)	)	kIx)	)
do	do	k7c2	do
většího	veliký	k2eAgNnSc2d2	veliký
sítka	sítko	k1gNnSc2	sítko
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
stejně	stejně	k6eAd1	stejně
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
se	se	k3xPyFc4	se
namele	namlít	k5eAaBmIp3nS	namlít
v	v	k7c6	v
mlýnku	mlýnek	k1gInSc6	mlýnek
na	na	k7c4	na
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
dávka	dávka	k1gFnSc1	dávka
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
7	[number]	k4	7
gramů	gram	k1gInPc2	gram
na	na	k7c4	na
porci	porce	k1gFnSc4	porce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
do	do	k7c2	do
páky	páka	k1gFnSc2	páka
a	a	k8xC	a
stlačí	stlačit	k5eAaPmIp3nS	stlačit
pěchovadlem	pěchovadlo	k1gNnSc7	pěchovadlo
<g/>
.	.	kIx.	.
</s>
<s>
Páku	páka	k1gFnSc4	páka
vložíme	vložit	k5eAaPmIp1nP	vložit
do	do	k7c2	do
kávovaru	kávovar	k1gInSc2	kávovar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kávu	káva	k1gFnSc4	káva
pod	pod	k7c7	pod
stálým	stálý	k2eAgInSc7d1	stálý
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
devíti	devět	k4xCc2	devět
barů	bar	k1gInPc2	bar
<g/>
,	,	kIx,	,
extrahuje	extrahovat	k5eAaBmIp3nS	extrahovat
do	do	k7c2	do
nahřátého	nahřátý	k2eAgInSc2d1	nahřátý
hrníčku	hrníček	k1gInSc2	hrníček
<g/>
.	.	kIx.	.
</s>
<s>
Hrníček	hrníček	k1gInSc1	hrníček
se	se	k3xPyFc4	se
nahřívá	nahřívat	k5eAaImIp3nS	nahřívat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
kávovaru	kávovar	k1gInSc2	kávovar
předem	předem	k6eAd1	předem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
espresso	espressa	k1gFnSc5	espressa
udrželo	udržet	k5eAaPmAgNnS	udržet
svoji	svůj	k3xOyFgFnSc4	svůj
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
mít	mít	k5eAaImF	mít
nahřáté	nahřátý	k2eAgNnSc4d1	nahřáté
dno	dno	k1gNnSc4	dno
hrníčku	hrníček	k1gInSc2	hrníček
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hrníčky	hrníček	k1gInPc1	hrníček
staví	stavit	k5eAaPmIp3nP	stavit
na	na	k7c4	na
kávovar	kávovar	k1gInSc4	kávovar
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
lze	lze	k6eAd1	lze
hrníček	hrníček	k1gInSc1	hrníček
předem	předem	k6eAd1	předem
nahřát	nahřát	k5eAaPmNgInS	nahřát
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Espresso	Espressa	k1gFnSc5	Espressa
ristretto	ristretta	k1gFnSc5	ristretta
–	–	k?	–
espresso	espressa	k1gFnSc5	espressa
"	"	kIx"	"
<g/>
zhuštěné	zhuštěný	k2eAgFnSc2d1	zhuštěná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
extra	extra	k6eAd1	extra
silné	silný	k2eAgFnPc1d1	silná
espresso	espressa	k1gFnSc5	espressa
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
podílem	podíl	k1gInSc7	podíl
vody	voda	k1gFnSc2	voda
cappuccino	cappuccino	k1gNnSc1	cappuccino
–	–	k?	–
espresso	espressa	k1gFnSc5	espressa
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
mléčnou	mléčný	k2eAgFnSc7d1	mléčná
pěnou	pěna	k1gFnSc7	pěna
latte	latte	k5eAaPmIp2nP	latte
macchiato	macchiato	k6eAd1	macchiato
–	–	k?	–
espresso	espressa	k1gFnSc5	espressa
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
mléčnou	mléčný	k2eAgFnSc7d1	mléčná
pěnou	pěna	k1gFnSc7	pěna
espresso	espressa	k1gFnSc5	espressa
macchiato	macchiat	k2eAgNnSc1d1	macchiato
–	–	k?	–
espresso	espressa	k1gFnSc5	espressa
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
espresso	espressa	k1gFnSc5	espressa
con	con	k?	con
panna	panna	k1gFnSc1	panna
–	–	k?	–
espresso	espressa	k1gFnSc5	espressa
se	se	k3xPyFc4	se
smetanou	smetana	k1gFnSc7	smetana
espresso	espressa	k1gFnSc5	espressa
americano	americana	k1gFnSc5	americana
–	–	k?	–
espresso	espressa	k1gFnSc5	espressa
doplněné	doplněný	k2eAgInPc1d1	doplněný
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
pro	pro	k7c4	pro
větší	veliký	k2eAgInSc4d2	veliký
objem	objem	k1gInSc4	objem
caffè	caffè	k?	caffè
crema	cremum	k1gNnSc2	cremum
café	café	k1gNnPc2	café
crè	crè	k?	crè
–	–	k?	–
francouzská	francouzský	k2eAgFnSc1d1	francouzská
podoba	podoba	k1gFnSc1	podoba
kávy	káva	k1gFnSc2	káva
podávané	podávaný	k2eAgFnPc4d1	podávaná
s	s	k7c7	s
horkou	horký	k2eAgFnSc7d1	horká
smetanou	smetana	k1gFnSc7	smetana
</s>
