<s>
Montérky	montérky	k1gFnPc1	montérky
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
také	také	k9	také
modráky	modrák	k1gInPc1	modrák
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
druhem	druh	k1gInSc7	druh
pracovního	pracovní	k2eAgInSc2d1	pracovní
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
především	především	k6eAd1	především
jako	jako	k8xS	jako
ochranný	ochranný	k2eAgInSc1d1	ochranný
oděv	oděv	k1gInSc1	oděv
při	při	k7c6	při
montážních	montážní	k2eAgFnPc6d1	montážní
a	a	k8xC	a
technických	technický	k2eAgFnPc6d1	technická
či	či	k8xC	či
zahradnických	zahradnický	k2eAgFnPc6d1	zahradnická
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
