<p>
<s>
Montérky	montérky	k1gFnPc1	montérky
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
také	také	k9	také
modráky	modrák	k1gInPc1	modrák
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
druhem	druh	k1gInSc7	druh
pracovního	pracovní	k2eAgInSc2d1	pracovní
oděvu	oděv	k1gInSc2	oděv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
především	především	k6eAd1	především
jako	jako	k8xC	jako
ochranný	ochranný	k2eAgInSc1d1	ochranný
oděv	oděv	k1gInSc1	oděv
při	při	k7c6	při
montážních	montážní	k2eAgFnPc6d1	montážní
a	a	k8xC	a
technických	technický	k2eAgFnPc6d1	technická
či	či	k8xC	či
zahradnických	zahradnický	k2eAgFnPc6d1	zahradnická
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
označují	označovat	k5eAaImIp3nP	označovat
pouze	pouze	k6eAd1	pouze
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
speciálních	speciální	k2eAgFnPc2d1	speciální
kalhot	kalhoty	k1gFnPc2	kalhoty
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
kapsami	kapsa	k1gFnPc7	kapsa
po	po	k7c6	po
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
vzadu	vzadu	k6eAd1	vzadu
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnSc7d1	přední
kapsou	kapsa	k1gFnSc7	kapsa
na	na	k7c6	na
laclu	laclu	k?	laclu
a	a	k8xC	a
kšandami	kšanda	k1gFnPc7	kšanda
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
kombinéze	kombinéza	k1gFnSc3	kombinéza
čili	čili	k8xC	čili
overalu	overal	k1gInSc3	overal
nemají	mít	k5eNaImIp3nP	mít
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
pro	pro	k7c4	pro
zakrytí	zakrytí	k1gNnSc4	zakrytí
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
rukávy	rukáv	k1gInPc7	rukáv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Montérky	montérky	k1gFnPc1	montérky
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
z	z	k7c2	z
denimu	denim	k1gInSc2	denim
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vydrží	vydržet	k5eAaPmIp3nS	vydržet
i	i	k9	i
nešetrné	šetrný	k2eNgNnSc1d1	nešetrné
zacházení	zacházení	k1gNnSc1	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jeansy	jeansa	k1gFnPc1	jeansa
byly	být	k5eAaImAgFnP	být
nebo	nebo	k8xC	nebo
vlastně	vlastně	k9	vlastně
stále	stále	k6eAd1	stále
jsou	být	k5eAaImIp3nP	být
pracovními	pracovní	k2eAgFnPc7d1	pracovní
kalhotami	kalhoty	k1gFnPc7	kalhoty
(	(	kIx(	(
<g/>
montérkami	montérky	k1gFnPc7	montérky
<g/>
)	)	kIx)	)
pocházejícími	pocházející	k2eAgFnPc7d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
zlaté	zlatý	k2eAgFnSc2d1	zlatá
horečky	horečka	k1gFnSc2	horečka
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
montérky	montérky	k1gFnPc1	montérky
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
