<s>
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
</s>
<s>
Symfonicky	symfonicky	k6eAd1
orchestr	orchestr	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
Zkratka	zkratka	k1gFnSc1
</s>
<s>
FOK	FOK	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1934	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Účel	účel	k1gInSc1
</s>
<s>
kultura	kultura	k1gFnSc1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Místo	místo	k7c2
</s>
<s>
Obecní	obecní	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Šéfdirigent	šéfdirigent	k1gMnSc1
od	od	k7c2
2020	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Brauner	Brauner	k1gMnSc1
Klíčové	klíčový	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Pekárek	Pekárek	k1gMnSc1
(	(	kIx(
<g/>
zakladatel	zakladatel	k1gMnSc1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.fok.cz	www.fok.cz	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Prague	Prague	k1gFnSc1
Symphony	Symphona	k1gFnSc2
Orchestra	orchestra	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
symfonické	symfonický	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
klasický	klasický	k2eAgInSc4d1
symfonický	symfonický	k2eAgInSc4d1
orchestr	orchestr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7
dnešního	dnešní	k2eAgInSc2d1
Symfonického	symfonický	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
byl	být	k5eAaImAgMnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1934	#num#	k4
dirigent	dirigent	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
organizátor	organizátor	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Pekárek	Pekárek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
činnosti	činnost	k1gFnSc2
nového	nový	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
vymezil	vymezit	k5eAaPmAgInS
slovy	slovo	k1gNnPc7
Film	film	k1gInSc1
<g/>
–	–	k?
<g/>
Opera	opera	k1gFnSc1
<g/>
–	–	k?
<g/>
Koncert	koncert	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
F.	F.	kA
O.	O.	kA
K.	K.	kA
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
názvu	název	k1gInSc2
orchestru	orchestr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
nahrávání	nahrávání	k1gNnSc3
hudby	hudba	k1gFnSc2
k	k	k7c3
většině	většina	k1gFnSc3
českých	český	k2eAgInPc2d1
filmů	film	k1gInPc2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
(	(	kIx(
<g/>
Hrdinný	hrdinný	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
Korkorán	Korkorán	k2eAgMnSc1d1
<g/>
,	,	kIx,
Dívka	dívka	k1gFnSc1
v	v	k7c6
modrém	modré	k1gNnSc6
<g/>
,	,	kIx,
Hej	hej	k6eAd1
rup	rupět	k5eAaImRp2nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Svět	svět	k1gInSc1
patří	patřit	k5eAaImIp3nS
nám	my	k3xPp1nPc3
<g/>
,	,	kIx,
Škola	škola	k1gFnSc1
základ	základ	k1gInSc1
života	život	k1gInSc2
<g/>
,	,	kIx,
Tři	tři	k4xCgNnPc4
vejce	vejce	k1gNnPc4
do	do	k7c2
skla	sklo	k1gNnSc2
<g/>
,	,	kIx,
Kristián	Kristián	k1gMnSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
a	a	k8xC
pravidelným	pravidelný	k2eAgNnSc7d1
vystupováním	vystupování	k1gNnSc7
v	v	k7c6
živém	živý	k2eAgNnSc6d1
vysílání	vysílání	k1gNnSc6
Československého	československý	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
vešel	vejít	k5eAaPmAgInS
Orchestr	orchestr	k1gInSc1
F.	F.	kA
O.	O.	kA
K.	K.	kA
ve	v	k7c4
známost	známost	k1gFnSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
zajištěna	zajistit	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
existence	existence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
postupný	postupný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
koncertní	koncertní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
hlavním	hlavní	k2eAgMnSc7d1
protagonistou	protagonista	k1gMnSc7
byl	být	k5eAaImAgMnS
od	od	k7c2
samého	samý	k3xTgInSc2
počátku	počátek	k1gInSc2
dr	dr	kA
<g/>
.	.	kIx.
Václav	Václav	k1gMnSc1
Smetáček	smetáček	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
dokázal	dokázat	k5eAaPmAgInS
ve	v	k7c6
velmi	velmi	k6eAd1
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
vybudovat	vybudovat	k5eAaPmF
z	z	k7c2
orchestru	orchestr	k1gInSc2
velké	velký	k2eAgNnSc1d1
symfonické	symfonický	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
schopné	schopný	k2eAgNnSc1d1
umělecky	umělecky	k6eAd1
plně	plně	k6eAd1
obstát	obstát	k5eAaPmF
v	v	k7c6
silné	silný	k2eAgFnSc6d1
domácí	domácí	k2eAgFnSc6d1
konkurenci	konkurence	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
šéfdirigentem	šéfdirigent	k1gMnSc7
orchestru	orchestr	k1gInSc2
a	a	k8xC
stál	stát	k5eAaImAgInS
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
celých	celý	k2eAgNnPc2d1
30	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
působení	působení	k1gNnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
orchestr	orchestr	k1gInSc1
vysoké	vysoký	k2eAgFnSc2d1
interpretační	interpretační	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
a	a	k8xC
mezinárodního	mezinárodní	k2eAgNnSc2d1
renomé	renomé	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
mnohaletém	mnohaletý	k2eAgNnSc6d1
úsilí	úsilí	k1gNnSc6
vedení	vedení	k1gNnSc2
orchestru	orchestr	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
počátek	počátek	k1gInSc4
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
zřídilo	zřídit	k5eAaPmAgNnS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
vzoru	vzor	k1gInSc6
jiných	jiný	k2eAgNnPc2d1
evropských	evropský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
vlastní	vlastnit	k5eAaImIp3nS
reprezentační	reprezentační	k2eAgNnSc1d1
koncertní	koncertní	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
byla	být	k5eAaImAgFnS
zachována	zachován	k2eAgFnSc1d1
<g/>
,	,	kIx,
nový	nový	k2eAgInSc1d1
název	název	k1gInSc1
orchestru	orchestr	k1gInSc2
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
vyjel	vyjet	k5eAaPmAgInS
orchestr	orchestr	k1gInSc1
na	na	k7c4
svá	svůj	k3xOyFgNnPc4
první	první	k4xOgNnSc4
zahraniční	zahraniční	k2eAgNnSc4d1
turné	turné	k1gNnSc4
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
a	a	k8xC
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
tak	tak	k9
své	svůj	k3xOyFgNnSc4
pravidelné	pravidelný	k2eAgNnSc4d1
a	a	k8xC
intenzivní	intenzivní	k2eAgNnSc4d1
působení	působení	k1gNnSc4
na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Sídlem	sídlo	k1gNnSc7
Symfonického	symfonický	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
je	být	k5eAaImIp3nS
Obecní	obecní	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Orchestr	orchestr	k1gInSc1
na	na	k7c4
dotek	dotek	k1gInSc4
</s>
<s>
Každou	každý	k3xTgFnSc4
sezónu	sezóna	k1gFnSc4
orchestr	orchestr	k1gInSc1
odehraje	odehrát	k5eAaPmIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
padesátku	padesátka	k1gFnSc4
koncertů	koncert	k1gInPc2
a	a	k8xC
odjíždí	odjíždět	k5eAaImIp3nS
hostovat	hostovat	k5eAaImF
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
orchestr	orchestr	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
k	k	k7c3
tradici	tradice	k1gFnSc3
zahajování	zahajování	k1gNnSc2
sezóny	sezóna	k1gFnSc2
koncertem	koncert	k1gInSc7
s	s	k7c7
filmovou	filmový	k2eAgFnSc7d1
hudbou	hudba	k1gFnSc7
ve	v	k7c6
Valdštejnské	valdštejnský	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
je	být	k5eAaImIp3nS
šéfdirigentem	šéfdirigent	k1gMnSc7
Symfonického	symfonický	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
Tomáš	Tomáš	k1gMnSc1
Brauner	Brauner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hostujícím	hostující	k2eAgMnSc7d1
dirigentem	dirigent	k1gMnSc7
je	být	k5eAaImIp3nS
Jac	Jac	k1gMnSc1
van	vana	k1gFnPc2
Steen	Stena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Komorní	komorní	k2eAgInPc1d1
koncerty	koncert	k1gInPc1
</s>
<s>
V	v	k7c6
nabídce	nabídka	k1gFnSc6
Symfonického	symfonický	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
jsou	být	k5eAaImIp3nP
i	i	k9
komorní	komorní	k2eAgInPc1d1
koncerty	koncert	k1gInPc1
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Šimona	Šimona	k1gFnSc1
a	a	k8xC
Judy	judo	k1gNnPc7
(	(	kIx(
<g/>
řada	řada	k1gFnSc1
koncertů	koncert	k1gInPc2
komorní	komorní	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
a	a	k8xC
staré	starý	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
klavírní	klavírní	k2eAgInPc1d1
recitály	recitál	k1gInPc1
ve	v	k7c6
Dvořákově	Dvořákův	k2eAgFnSc6d1
síni	síň	k1gFnSc6
Rudolfina	Rudolfinum	k1gNnSc2
<g/>
,	,	kIx,
cyklus	cyklus	k1gInSc1
komorních	komorní	k2eAgInPc2d1
koncertů	koncert	k1gInPc2
Obrazy	obraz	k1gInPc1
a	a	k8xC
hudba	hudba	k1gFnSc1
v	v	k7c6
Anežském	anežský	k2eAgInSc6d1
klášteře	klášter	k1gInSc6
nebo	nebo	k8xC
cyklus	cyklus	k1gInSc1
Slovo	slovo	k1gNnSc1
a	a	k8xC
hudba	hudba	k1gFnSc1
v	v	k7c6
Divadle	divadlo	k1gNnSc6
Viola	Viola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Edukativní	edukativní	k2eAgInPc1d1
pořady	pořad	k1gInPc1
</s>
<s>
Orchestr	orchestr	k1gInSc1
pořádá	pořádat	k5eAaImIp3nS
populární	populární	k2eAgInPc4d1
pořady	pořad	k1gInPc4
pro	pro	k7c4
rodiny	rodina	k1gFnPc4
s	s	k7c7
dětmi	dítě	k1gFnPc7
s	s	k7c7
názvem	název	k1gInSc7
Orchestr	orchestr	k1gInSc1
na	na	k7c4
dotek	dotek	k1gInSc4
<g/>
,	,	kIx,
organizuje	organizovat	k5eAaBmIp3nS
množství	množství	k1gNnSc1
koncertů	koncert	k1gInPc2
a	a	k8xC
edukativních	edukativní	k2eAgInPc2d1
pořadů	pořad	k1gInPc2
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
stupně	stupeň	k1gInPc4
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbená	oblíbený	k2eAgNnPc1d1
jsou	být	k5eAaImIp3nP
rovněž	rovněž	k9
interaktivní	interaktivní	k2eAgNnSc4d1
setkání	setkání	k1gNnSc4
hudebního	hudební	k2eAgInSc2d1
klubu	klub	k1gInSc2
Fík	fík	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgNnSc1d1
hostování	hostování	k1gNnSc1
</s>
<s>
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
hostoval	hostovat	k5eAaImAgInS
ve	v	k7c6
většině	většina	k1gFnSc6
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
opakovaně	opakovaně	k6eAd1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
,	,	kIx,
tradičně	tradičně	k6eAd1
zajíždí	zajíždět	k5eAaImIp3nS
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštívil	navštívit	k5eAaPmAgMnS
rovněž	rovněž	k9
Jižní	jižní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc4
<g/>
,	,	kIx,
Tchaj-wan	Tchaj-wan	k1gInSc4
<g/>
,	,	kIx,
Koreu	Korea	k1gFnSc4
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
Omán	Omán	k1gInSc4
<g/>
,	,	kIx,
Čínu	Čína	k1gFnSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
orchestr	orchestr	k1gInSc1
zavítal	zavítat	k5eAaPmAgInS
do	do	k7c2
vídeňského	vídeňský	k2eAgInSc2d1
Konzerthausu	Konzerthaus	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
turné	turné	k1gNnSc4
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
absolvoval	absolvovat	k5eAaPmAgMnS
zájezdy	zájezd	k1gInPc4
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
Polska	Polsko	k1gNnSc2
nebo	nebo	k8xC
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
uskutečnil	uskutečnit	k5eAaPmAgInS
opět	opět	k6eAd1
zájezd	zájezd	k1gInSc4
do	do	k7c2
Japonska	Japonsko	k1gNnSc2
a	a	k8xC
absolvoval	absolvovat	k5eAaPmAgInS
mimořádně	mimořádně	k6eAd1
úspěšné	úspěšný	k2eAgNnSc4d1
turné	turné	k1gNnSc4
po	po	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
s	s	k7c7
Mahlerovou	Mahlerův	k2eAgFnSc7d1
Symfonií	symfonie	k1gFnSc7
č.	č.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tradice	tradice	k1gFnSc1
orchestru	orchestr	k1gInSc2
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
orchestru	orchestr	k1gInSc2
dokládá	dokládat	k5eAaImIp3nS
také	také	k9
rozsáhlý	rozsáhlý	k2eAgInSc4d1
katalog	katalog	k1gInSc4
gramofonových	gramofonový	k2eAgFnPc2d1
<g/>
,	,	kIx,
rozhlasových	rozhlasový	k2eAgFnPc2d1
a	a	k8xC
televizních	televizní	k2eAgInPc2d1
snímků	snímek	k1gInPc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgNnPc4d1
česká	český	k2eAgNnPc4d1
i	i	k8xC
světová	světový	k2eAgNnPc4d1
hudební	hudební	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
nahrávek	nahrávka	k1gFnPc2
byla	být	k5eAaImAgFnS
pořízena	pořídit	k5eAaPmNgFnS
firmou	firma	k1gFnSc7
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
jméno	jméno	k1gNnSc1
orchestru	orchestr	k1gInSc2
se	se	k3xPyFc4
však	však	k9
objevuje	objevovat	k5eAaImIp3nS
i	i	k9
na	na	k7c6
labelech-etiketách	labelech-etiketa	k1gFnPc6
firem	firma	k1gFnPc2
BMG	BMG	kA
Conifer	Conifer	k1gMnSc1
<g/>
,	,	kIx,
Philips	Philips	kA
<g/>
,	,	kIx,
Erato	Erato	k1gNnSc1
<g/>
,	,	kIx,
Universal	Universal	k1gFnPc1
<g/>
,	,	kIx,
Harmonia	harmonium	k1gNnPc1
Mundi	Mund	k1gMnPc5
(	(	kIx(
<g/>
Praga	Praga	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
<g/>
,	,	kIx,
Koch	Koch	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
Panton	Panton	k1gInSc1
<g/>
,	,	kIx,
Music	Music	k1gMnSc1
Vars	Varsa	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
vzniká	vznikat	k5eAaImIp3nS
nahrávka	nahrávka	k1gFnSc1
skladeb	skladba	k1gFnPc2
Karla	Karel	k1gMnSc2
Husy	Husa	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Významní	významný	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
</s>
<s>
Šéfdirigenti	šéfdirigent	k1gMnPc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Pekárek	Pekárek	k1gMnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Smetáček	smetáček	k1gInSc1
(	(	kIx(
<g/>
1942	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Slovák	Slovák	k1gMnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Rohan	Rohan	k1gMnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Bělohlávek	Bělohlávek	k1gMnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
Altrichter	Altrichter	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Turnovský	turnovský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gaetano	Gaetana	k1gFnSc5
Delogu	Delog	k1gMnSc6
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Serge	Serge	k6eAd1
Baudo	Baudo	k1gNnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kout	kout	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pietari	Pietari	k1gNnSc1
Inkinen	Inkinna	k1gFnPc2
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Brauner	Brauner	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1
dirigenti	dirigent	k1gMnPc1
</s>
<s>
Umělecké	umělecký	k2eAgNnSc1d1
renomé	renomé	k1gNnSc1
a	a	k8xC
respekt	respekt	k1gInSc4
si	se	k3xPyFc3
orchestr	orchestr	k1gInSc4
získává	získávat	k5eAaImIp3nS
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
mezinárodně	mezinárodně	k6eAd1
uznávaných	uznávaný	k2eAgMnPc2d1
dirigentů	dirigent	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
Václav	Václav	k1gMnSc1
Talich	Talich	k1gMnSc1
</s>
<s>
Rafael	Rafael	k1gMnSc1
Kubelík	Kubelík	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Ančerl	Ančerl	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Neumann	Neumann	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Košler	Košler	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Válek	Válek	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Altrichter	Altrichter	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Koutník	Koutník	k1gMnSc1
</s>
<s>
Sir	sir	k1gMnSc1
Georg	Georg	k1gMnSc1
Solti	Solť	k1gFnSc2
</s>
<s>
Seiji	Sei	k6eAd2
Ozawa	Ozawa	k1gMnSc1
</s>
<s>
Zubin	Zubin	k1gMnSc1
Mehta	Mehta	k1gMnSc1
</s>
<s>
Sir	sir	k1gMnSc1
Charles	Charles	k1gMnSc1
Mackerras	Mackerras	k1gMnSc1
</s>
<s>
Gennadij	Gennadít	k5eAaPmRp2nS
Rožděstvenskij	Rožděstvenskij	k1gMnSc5
</s>
<s>
Elijahu	Elijah	k1gInSc3
Inbal	Inbal	k1gInSc1
</s>
<s>
Helmuth	Helmuth	k1gInSc1
Rilling	Rilling	k1gInSc1
</s>
<s>
Andrej	Andrej	k1gMnSc1
Borejko	Borejka	k1gFnSc5
</s>
<s>
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Spolupracující	spolupracující	k2eAgMnPc1d1
špičkoví	špičkový	k2eAgMnPc1d1
sólisté	sólista	k1gMnPc1
</s>
<s>
David	David	k1gMnSc1
Oistrach	Oistrach	k1gMnSc1
</s>
<s>
Isaac	Isaac	k1gFnSc1
Stern	sternum	k1gNnPc2
</s>
<s>
Josef	Josef	k1gMnSc1
Suk	Suk	k1gMnSc1
mladší	mladý	k2eAgMnSc1d2
</s>
<s>
Arthur	Arthur	k1gMnSc1
Rubinstein	Rubinstein	k1gMnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Firkušný	Firkušný	k2eAgMnSc1d1
</s>
<s>
Svjatoslav	Svjatoslav	k1gMnSc1
Richter	Richter	k1gMnSc1
</s>
<s>
Claudio	Claudio	k1gNnSc1
Arrau	Arraus	k1gInSc2
</s>
<s>
Murray	Murraa	k1gFnPc1
Perahia	Perahium	k1gNnSc2
</s>
<s>
Ivan	Ivan	k1gMnSc1
Moravec	Moravec	k1gMnSc1
</s>
<s>
Garrick	Garrick	k1gMnSc1
Ohlsson	Ohlsson	k1gMnSc1
</s>
<s>
Maurice	Maurika	k1gFnSc3
André	André	k1gMnSc1
</s>
<s>
Christian	Christian	k1gMnSc1
Lindberg	Lindberg	k1gMnSc1
</s>
<s>
Mstislav	Mstislava	k1gFnPc2
Rostropovič	Rostropovič	k1gMnSc1
</s>
<s>
Mischa	Mischa	k1gFnSc1
Maisky	Maiska	k1gFnSc2
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Schiff	Schiff	k1gMnSc1
</s>
<s>
Katia	Katia	k1gFnSc1
Ricciarelliová	Ricciarelliový	k2eAgFnSc1d1
</s>
<s>
Gabriela	Gabriela	k1gFnSc1
Beňačková	Beňačková	k1gFnSc1
</s>
<s>
Eva	Eva	k1gFnSc1
Urbanová	Urbanová	k1gFnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Dvorský	Dvorský	k1gMnSc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Hampson	Hampson	k1gMnSc1
</s>
<s>
Ruggero	Ruggero	k1gNnSc1
Raimondi	Raimond	k1gMnPc1
</s>
<s>
Renée	René	k1gMnPc4
Flemingová	Flemingový	k2eAgNnPc5d1
</s>
<s>
Sergei	Sergei	k6eAd1
Nakarjakov	Nakarjakov	k1gInSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Hudeček	Hudeček	k1gMnSc1
</s>
<s>
Edita	Edita	k1gFnSc1
Gruberová	Gruberová	k1gFnSc1
</s>
<s>
Vadim	Vadim	k?
Repin	Repin	k1gInSc1
</s>
<s>
Pinchas	Pinchas	k1gMnSc1
Zukerman	Zukerman	k1gMnSc1
</s>
<s>
José	José	k1gNnSc1
Cura	Cur	k1gInSc2
(	(	kIx(
<g/>
rezidenční	rezidenční	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Symfonického	symfonický	k2eAgInSc2d1
orchestru	orchestr	k1gInSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
</s>
<s>
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
v	v	k7c6
Českém	český	k2eAgInSc6d1
hudebním	hudební	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
v	v	k7c6
Českém	český	k2eAgInSc6d1
hudebním	hudební	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
(	(	kIx(
<g/>
musicologica	musicologica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
Daniel	Daniel	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
hostem	host	k1gMnSc7
pořadu	pořad	k1gInSc2
Hovory	hovora	k1gMnPc4
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
22	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010710320	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1215150-6	1215150-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2286	#num#	k4
2451	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82162662	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
132559294	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82162662	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
