<s>
Symfonický	symfonický	k2eAgInSc1d1
orchestr	orchestr	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
FOK	FOK	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Prague	Prague	k1gFnSc1
Symphony	Symphona	k1gFnSc2
Orchestra	orchestra	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
symfonické	symfonický	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
klasický	klasický	k2eAgInSc4d1
symfonický	symfonický	k2eAgInSc4d1
orchestr	orchestr	k1gInSc4
<g/>
.	.	kIx.
</s>