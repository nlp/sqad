<s>
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
</s>
<s>
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
</s>
<s>
Triviální	triviální	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
křemenný	křemenný	k2eAgInSc1d1
písek	písek	k1gInSc1
<g/>
,	,	kIx,
silika	silika	k1gFnSc1
</s>
<s>
Latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Oxidum	Oxidum	k1gInSc1
silicii	silicium	k1gNnPc7
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Silicon	Silicon	kA
dioxide	dioxid	k1gInSc5
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Siliciumdioxid	Siliciumdioxid	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
SiO	SiO	k?
<g/>
2	#num#	k4
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
bílá	bílý	k2eAgFnSc1d1
práškovitá	práškovitý	k2eAgFnSc1d1
nebo	nebo	k8xC
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7631-86-9	7631-86-9	k4
</s>
<s>
EC-no	EC-no	k1gNnSc1
(	(	kIx(
<g/>
EINECS	EINECS	kA
<g/>
/	/	kIx~
<g/>
ELINCS	ELINCS	kA
<g/>
/	/	kIx~
<g/>
NLP	NLP	kA
<g/>
)	)	kIx)
</s>
<s>
231-545-4	231-545-4	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
24261	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
O	o	k7c4
<g/>
=	=	kIx~
<g/>
[	[	kIx(
<g/>
Si	se	k3xPyFc3
<g/>
]	]	kIx)
<g/>
=	=	kIx~
<g/>
O	o	k7c6
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
O	o	k7c6
<g/>
2	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
</s>
<s>
Číslo	číslo	k1gNnSc1
RTECS	RTECS	kA
</s>
<s>
VV7565000	VV7565000	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
60,085	60,085	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
změny	změna	k1gFnSc2
krystalové	krystalový	k2eAgFnSc2d1
modifikace	modifikace	k1gFnSc2
</s>
<s>
10,5	10,5	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
α	α	k?
→	→	k?
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
skelného	skelný	k2eAgInSc2d1
přechodu	přechod	k1gInSc2
</s>
<s>
1	#num#	k4
150	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
<g/>
,	,	kIx,
amorfní	amorfní	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
2,33	2,33	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
kristobalit	kristobalita	k1gFnPc2
<g/>
)	)	kIx)
2,651	2,651	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
0	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
2,648	2,648	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
2,264	2,264	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
tridymit	tridymit	k1gInSc1
<g/>
)	)	kIx)
2,2	2,2	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgInPc1d1
<g/>
,	,	kIx,
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
kristobalit	kristobalit	k1gInSc1
nPř	nPř	k?
<g/>
=	=	kIx~
1,487	1,487	k4
nPm	nPm	k?
<g/>
=	=	kIx~
1,484	1,484	k4
křemen	křemen	k1gInSc1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
nPř	nPř	k?
<g/>
=	=	kIx~
1,544	1,544	k4
220	#num#	k4
2	#num#	k4
nPm	nPm	k?
<g/>
=	=	kIx~
1,553	1,553	k4
32	#num#	k4
tridymitnPa	tridymitnPa	k1gFnSc1
<g/>
=	=	kIx~
1,475	1,475	k4
<g/>
nPb	nPb	k?
<g/>
=	=	kIx~
1,476	1,476	k4
<g/>
nPc	nPc	k?
<g/>
=	=	kIx~
1,478	1,478	k4
amorfní	amorfní	k2eAgFnSc1d1
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
nP	nP	k?
<g/>
=	=	kIx~
1,458	1,458	k4
86	#num#	k4
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
6,5	6,5	k4
(	(	kIx(
<g/>
kristobalit	kristobalita	k1gFnPc2
<g/>
)	)	kIx)
7	#num#	k4
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
7	#num#	k4
(	(	kIx(
<g/>
tridymit	tridymit	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
</s>
<s>
0,016	0,016	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
ml	ml	kA
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
permitivita	permitivita	k1gFnSc1
ε	ε	k1gFnPc2
</s>
<s>
3,75	3,75	k4
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgInPc1d1
<g/>
,	,	kIx,
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Součinitel	součinitel	k1gInSc1
tepelné	tepelný	k2eAgFnSc2d1
vodivosti	vodivost	k1gFnSc2
</s>
<s>
křemen	křemen	k1gInSc1
5,88	5,88	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
38	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
hrana	hrana	k1gFnSc1
a	a	k8xC
<g/>
)	)	kIx)
11,06	11,06	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
38	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
hrana	hrana	k1gFnSc1
c	c	k0
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
5,19	5,19	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
93	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
hrana	hrana	k1gFnSc1
a	a	k8xC
<g/>
)	)	kIx)
9,34	9,34	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
93	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
hrana	hrana	k1gFnSc1
c	c	k0
<g/>
)	)	kIx)
4,50	4,50	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
149	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
hrana	hrana	k1gFnSc1
a	a	k8xC
<g/>
)	)	kIx)
8,99	8,99	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
149	#num#	k4
°	°	k?
<g/>
C	C	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
hrana	hrana	k1gFnSc1
c	c	k0
<g/>
)	)	kIx)
amorfní	amorfní	k2eAgMnPc1d1
0,84	0,84	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
-	-	kIx~
<g/>
150	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
1,05	1,05	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
-	-	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
100	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
1,21	1,21	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
-	-	kIx~
<g/>
50	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
1,32	1,32	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
0	#num#	k4
°	°	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
1,34	1,34	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
18	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
1,41	1,41	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
50	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
1,48	1,48	k4
W	W	kA
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
*	*	kIx~
<g/>
K	k	k7c3
<g/>
)	)	kIx)
(	(	kIx(
<g/>
100	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Součinitel	součinitel	k1gInSc1
délkové	délkový	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
</s>
<s>
5,5	5,5	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
1015	#num#	k4
<g/>
–	–	k?
<g/>
1018	#num#	k4
Ώ	Ώ	k1gInSc1
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Krystalová	krystalový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
čtverečná	čtverečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
α	α	k1gInSc1
<g/>
)	)	kIx)
krychlová	krychlový	k2eAgFnSc1d1
(	(	kIx(
<g/>
β	β	k1gInSc1
<g/>
)	)	kIx)
šesterečná	šesterečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
α	α	k1gInSc1
<g/>
)	)	kIx)
šesterečná	šesterečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
β	β	k1gInSc1
<g/>
)	)	kIx)
jednoklonná	jednoklonný	k2eAgFnSc1d1
(	(	kIx(
<g/>
α	α	k1gInSc1
<g/>
)	)	kIx)
kosočtverečná	kosočtverečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
α	α	k?
<g/>
'	'	kIx"
<g/>
-tridymit	-tridymit	k1gInSc1
<g/>
)	)	kIx)
šesterečná	šesterečný	k2eAgFnSc1d1
(	(	kIx(
<g/>
β	β	k1gInSc1
<g/>
)	)	kIx)
amorfní	amorfní	k2eAgFnSc1d1
</s>
<s>
Hrana	hrana	k1gFnSc1
krystalové	krystalový	k2eAgFnSc2d1
mřížky	mřížka	k1gFnSc2
</s>
<s>
α	α	k1gInSc1
(	(	kIx(
<g/>
při	při	k7c6
30	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
491,36	491,36	k4
pm	pm	k?
<g/>
;	;	kIx,
c	c	k0
<g/>
=	=	kIx~
692,62	692,62	k4
pm	pm	k?
β	β	k1gFnPc2
(	(	kIx(
<g/>
při	při	k7c6
405	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
713,82	713,82	k4
pm	pm	k?
α	α	k1gInSc1
(	(	kIx(
<g/>
při	při	k7c6
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
491,36	491,36	k4
<g />
.	.	kIx.
</s>
<s hack="1">
pm	pm	k?
<g/>
;	;	kIx,
c	c	k0
<g/>
=	=	kIx~
540,51	540,51	k4
pm	pm	k?
β	β	k1gInSc1
(	(	kIx(
<g/>
při	při	k7c6
575	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
499,9	499,9	k4
pm	pm	k?
<g/>
;	;	kIx,
c	c	k0
<g/>
=	=	kIx~
545,92	545,92	k4
pm	pm	k?
α	α	k1gFnPc2
(	(	kIx(
<g/>
při	při	k7c6
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
1	#num#	k4
854	#num#	k4
pm	pm	k?
<g/>
;	;	kIx,
b	b	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
501	#num#	k4
pm	pm	k?
<g/>
;	;	kIx,
c	c	k0
<g/>
=	=	kIx~
2	#num#	k4
579	#num#	k4
pm	pm	k?
<g/>
;	;	kIx,
β	β	k?
<g/>
=	=	kIx~
117	#num#	k4
<g/>
°	°	k?
40	#num#	k4
<g/>
´	´	k?
α	α	k?
<g/>
'	'	kIx"
<g/>
-tridymit	-tridymit	k1gInSc1
(	(	kIx(
<g/>
při	při	k7c6
220	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
874	#num#	k4
pm	pm	k?
<g/>
;	;	kIx,
b	b	k?
<g/>
=	=	kIx~
504	#num#	k4
pm	pm	k?
<g/>
;	;	kIx,
c	c	k0
<g/>
=	=	kIx~
824	#num#	k4
pm	pm	k?
β	β	k1gFnPc2
(	(	kIx(
<g/>
při	při	k7c6
405	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
<g/>
=	=	kIx~
504,63	504,63	k4
pm	pm	k?
<g/>
;	;	kIx,
c	c	k0
<g/>
=	=	kIx~
825,63	825,63	k4
pm	pm	k?
</s>
<s>
Tvar	tvar	k1gInSc1
molekuly	molekula	k1gFnSc2
</s>
<s>
tetraedr	tetraedr	k1gInSc1
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
kristobalit	kristobalit	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
tridymit	tridymit	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Entalpie	entalpie	k1gFnSc1
tání	tání	k1gNnSc3
Δ	Δ	k5eAaPmF
</s>
<s>
128	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
(	(	kIx(
<g/>
kristobalit	kristobalit	k1gInSc1
<g/>
)	)	kIx)
142	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
42,7	42,7	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
kristobalit	kristobalita	k1gFnPc2
<g/>
)	)	kIx)
41,85	41,85	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
43,5	43,5	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
tridymit	tridymit	k1gInSc1
<g/>
)	)	kIx)
46,9	46,9	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
Gibbsova	Gibbsův	k2eAgFnSc1d1
energie	energie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
kristobalit	kristobalit	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
tridymit	tridymit	k1gInSc1
<g/>
)	)	kIx)
−	−	k?
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Izobarické	izobarický	k2eAgNnSc1d1
měrné	měrný	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
cp	cp	k?
</s>
<s>
0,735	0,735	k4
7	#num#	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
kristobalit	kristobalita	k1gFnPc2
<g/>
)	)	kIx)
0,739	0,739	k4
9	#num#	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
křemen	křemen	k1gInSc1
<g/>
)	)	kIx)
0,742	0,742	k4
7	#num#	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
tridymit	tridymit	k1gInSc1
<g/>
)	)	kIx)
0,738	0,738	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
amorfní	amorfní	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
žádné	žádný	k3yNgInPc1
nejsou	být	k5eNaImIp3nP
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S22	S22	k4
</s>
<s>
NFPA	NFPA	kA
704	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Teplota	teplota	k1gFnSc1
vznícení	vznícení	k1gNnSc2
</s>
<s>
nehořlavý	hořlavý	k2eNgInSc1d1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SiO	SiO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
tvoří	tvořit	k5eAaImIp3nS
nejméně	málo	k6eAd3
22	#num#	k4
fází	fáze	k1gFnPc2
a	a	k8xC
dvanáct	dvanáct	k4xCc4
polymorfních	polymorfní	k2eAgFnPc2d1
forem	forma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
této	tento	k3xDgFnSc3
rozmanitosti	rozmanitost	k1gFnSc3
a	a	k8xC
velkému	velký	k2eAgInSc3d1
praktickému	praktický	k2eAgInSc3d1
významu	význam	k1gInSc3
patří	patřit	k5eAaImIp3nS
tento	tento	k3xDgInSc1
oxid	oxid	k1gInSc1
mezi	mezi	k7c4
nejstudovanější	studovaný	k2eAgFnPc4d3
látky	látka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
V	v	k7c6
přírodě	příroda	k1gFnSc6
jej	on	k3xPp3gInSc4
nacházíme	nacházet	k5eAaImIp1nP
nejčastěji	často	k6eAd3
ve	v	k7c6
formě	forma	k1gFnSc6
α	α	k5eAaPmIp3nS
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnPc2
např.	např.	kA
žuly	žula	k1gFnSc2
a	a	k8xC
pískovce	pískovec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modifikace	modifikace	k1gFnSc1
oxidu	oxid	k1gInSc2
křemičitého	křemičitý	k2eAgInSc2d1
se	se	k3xPyFc4
převážně	převážně	k6eAd1
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
tetraedrů	tetraedr	k1gInPc2
SiO	SiO	k1gFnSc2
<g/>
4	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
propojeny	propojit	k5eAaPmNgInP
přes	přes	k7c4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
termodynamicky	termodynamicky	k6eAd1
nejstabilnější	stabilní	k2eAgFnSc6d3
formě	forma	k1gFnSc6
(	(	kIx(
<g/>
za	za	k7c2
laboratorní	laboratorní	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
α	α	k1gInSc2
–	–	k?
tvoří	tvořit	k5eAaImIp3nS
tyto	tento	k3xDgInPc4
tetraedry	tetraedr	k1gInPc4
vzájemně	vzájemně	k6eAd1
spojené	spojený	k2eAgFnPc4d1
šroubovice	šroubovice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
přechody	přechod	k1gInPc1
mezi	mezi	k7c7
nejběžnějšími	běžný	k2eAgFnPc7d3
krystalickými	krystalický	k2eAgFnPc7d1
modifikacemi	modifikace	k1gFnPc7
SiO	SiO	k1gFnSc2
<g/>
2	#num#	k4
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
odolný	odolný	k2eAgMnSc1d1
vůči	vůči	k7c3
kyselinám	kyselina	k1gFnPc3
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
fluorovodíkové	fluorovodíkový	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
reaguje	reagovat	k5eAaBmIp3nS
takto	takto	k6eAd1
<g/>
:	:	kIx,
SiO	SiO	kA
<g/>
2	#num#	k4
+	+	kIx~
4	#num#	k4
HF	HF	kA
→	→	k?
SiF	SiF	kA
<g/>
4	#num#	k4
+	+	kIx~
2	#num#	k4
H2O	H2O	kA
Horké	Horké	k2eAgInPc2d1
koncentrované	koncentrovaný	k2eAgInPc4d1
alkalické	alkalický	k2eAgInPc4d1
hydroxidy	hydroxid	k1gInPc4
jej	on	k3xPp3gMnSc4
pomalu	pomalu	k6eAd1
rozpouštějí	rozpouštět	k5eAaImIp3nP
za	za	k7c2
vzniku	vznik	k1gInSc2
alkalických	alkalický	k2eAgInPc2d1
křemičitanů	křemičitan	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
taveninách	tavenina	k1gFnPc6
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
podstatně	podstatně	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
zvýšené	zvýšený	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
(	(	kIx(
<g/>
nad	nad	k7c7
1	#num#	k4
000	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
reaguje	reagovat	k5eAaBmIp3nS
i	i	k9
s	s	k7c7
vodíkem	vodík	k1gInSc7
a	a	k8xC
uhlíkem	uhlík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
fluorem	fluor	k1gInSc7
reaguje	reagovat	k5eAaBmIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
fluoridu	fluorid	k1gInSc2
křemičitého	křemičitý	k2eAgInSc2d1
a	a	k8xC
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reakce	reakce	k1gFnSc1
s	s	k7c7
oxidy	oxid	k1gInPc7
kovů	kov	k1gInPc2
a	a	k8xC
polokovů	polokov	k1gInPc2
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
významné	významný	k2eAgFnPc1d1
ve	v	k7c6
sklářském	sklářský	k2eAgInSc6d1
a	a	k8xC
keramickém	keramický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
křišťál	křišťál	k1gInSc1
-	-	kIx~
krystalický	krystalický	k2eAgInSc1d1
oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
</s>
<s>
minerál	minerál	k1gInSc1
ametyst	ametyst	k1gInSc1
</s>
<s>
V	v	k7c6
průmyslu	průmysl	k1gInSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
převážně	převážně	k6eAd1
α	α	k2eAgMnSc1d1
<g/>
,	,	kIx,
křemenné	křemenný	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
<g/>
,	,	kIx,
silikagel	silikagel	k1gInSc1
<g/>
,	,	kIx,
kouřový	kouřový	k2eAgInSc1d1
křemen	křemen	k1gInSc1
a	a	k8xC
diatomit	diatomit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Piezoelektrických	piezoelektrický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
křemene	křemen	k1gInSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
krystalových	krystalový	k2eAgInPc6d1
oscilátorech	oscilátor	k1gInPc6
a	a	k8xC
filtrech	filtr	k1gInPc6
v	v	k7c6
převodnících	převodník	k1gInPc6
a	a	k8xC
snímačích	snímač	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
nenachází	nacházet	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
čistý	čistý	k2eAgInSc1d1
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
připravovat	připravovat	k5eAaImF
hydrotermálními	hydrotermální	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc4
křemičitý	křemičitý	k2eAgInSc4d1
také	také	k6eAd1
dále	daleko	k6eAd2
najdeme	najít	k5eAaPmIp1nP
v	v	k7c6
čisté	čistý	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
v	v	k7c6
jádru	jádro	k1gNnSc6
optických	optický	k2eAgInPc2d1
kabelů	kabel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Křemenné	křemenný	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
je	být	k5eAaImIp3nS
výjimečně	výjimečně	k6eAd1
odolné	odolný	k2eAgFnSc2d1
vůči	vůči	k7c3
teplotním	teplotní	k2eAgInPc3d1
šokům	šok	k1gInPc3
a	a	k8xC
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
malou	malý	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
koeficientu	koeficient	k1gInSc2
tepelné	tepelný	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
běžného	běžný	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
má	mít	k5eAaImIp3nS
vysokou	vysoký	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
měknutí	měknutí	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ztěžuje	ztěžovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
zpracování	zpracování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
kvalitní	kvalitní	k2eAgNnSc1d1
laboratorní	laboratorní	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
(	(	kIx(
<g/>
např.	např.	kA
pro	pro	k7c4
kyvety	kyveta	k1gFnPc4
pro	pro	k7c4
UV	UV	kA
a	a	k8xC
VIS	vis	k1gInSc1
spektrofotometrii	spektrofotometrie	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Silikagel	silikagel	k1gInSc1
se	se	k3xPyFc4
díky	díky	k7c3
vysokému	vysoký	k2eAgInSc3d1
povrchu	povrch	k1gInSc3
používá	používat	k5eAaImIp3nS
jako	jako	k9
sušidlo	sušidlo	k1gNnSc1
<g/>
,	,	kIx,
sorbent	sorbent	k1gInSc1
<g/>
,	,	kIx,
nosič	nosič	k1gInSc1
katalyzátorů	katalyzátor	k1gInPc2
atd.	atd.	kA
</s>
<s>
V	v	k7c6
potravinářství	potravinářství	k1gNnSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pod	pod	k7c7
označením	označení	k1gNnSc7
E	E	kA
551	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oxidy	oxid	k1gInPc1
s	s	k7c7
prvkem	prvek	k1gInSc7
v	v	k7c6
oxidačním	oxidační	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
americičitý	americičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
AmO	AmO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
uhličitý	uhličitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CO	co	k6eAd1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
ceričitý	ceričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CeO	CeO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
chloričitý	chloričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ClO	clo	k1gNnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
chromičitý	chromičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
CrO	CrO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
dusičitý	dusičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
NO	no	k9
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
germaničitý	germaničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
GeO	GeO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
hafničitý	hafničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
HfO	HfO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
neptuničitý	ptuničitý	k2eNgInSc1d1
(	(	kIx(
<g/>
NpO	NpO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
olovičitý	olovičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PbO	PbO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
manganičitý	manganičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
MnO	MnO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
osmičitý	osmičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
OsO	osa	k1gFnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
plutoničitý	plutoničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PuO	PuO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
protaktiničitý	protaktiničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
PaO	PaO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
rheničitý	rheničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ReO	Rea	k1gFnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
rutheničitý	rutheničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
RuO	RuO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
seleničitý	seleničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SeO	SeO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SiO	SiO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
siřičitý	siřičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SO	So	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
telluričitý	telluričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
TeO	Tea	k1gFnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
thoričitý	thoričitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ThO	ThO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
cíničitý	cíničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
SnO	SnO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
titaničitý	titaničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
TiO	TiO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
wolframičitý	wolframičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
WO	WO	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
uraničitý	uraničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
UO	UO	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
vanadičitý	vanadičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
VO	VO	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
zirkoničitý	zirkoničitý	k2eAgInSc1d1
(	(	kIx(
<g/>
ZrO	ZrO	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
viz	vidět	k5eAaImRp2nS
též	též	k9
Trioxid	Trioxid	k1gInSc4
uhlíku	uhlík	k1gInSc2
(	(	kIx(
<g/>
CO	co	k9
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
praseodymito-praseodymičitý	praseodymito-praseodymičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Pr	pr	k0
<g/>
6	#num#	k4
<g/>
O	o	k7c4
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4077447-8	4077447-8	k4
</s>
