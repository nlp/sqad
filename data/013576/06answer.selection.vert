<s>
Oxid	oxid	k1gInSc1
křemičitý	křemičitý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
odolný	odolný	k2eAgMnSc1d1
vůči	vůči	k7c3
kyselinám	kyselina	k1gFnPc3
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
fluorovodíkové	fluorovodíkový	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
reaguje	reagovat	k5eAaBmIp3nS
takto	takto	k6eAd1
<g/>
:	:	kIx,
SiO	SiO	kA
<g/>
2	#num#	k4
+	+	kIx~
4	#num#	k4
HF	HF	kA
→	→	k?
SiF	SiF	kA
<g/>
4	#num#	k4
+	+	kIx~
2	#num#	k4
H2O	H2O	kA
Horké	Horké	k2eAgInPc2d1
koncentrované	koncentrovaný	k2eAgInPc4d1
alkalické	alkalický	k2eAgInPc4d1
hydroxidy	hydroxid	k1gInPc4
jej	on	k3xPp3gMnSc4
pomalu	pomalu	k6eAd1
rozpouštějí	rozpouštět	k5eAaImIp3nP
za	za	k7c2
vzniku	vznik	k1gInSc2
alkalických	alkalický	k2eAgInPc2d1
křemičitanů	křemičitan	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
taveninách	tavenina	k1gFnPc6
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
podstatně	podstatně	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>