<s>
Káhira	Káhira	k1gFnSc1	Káhira
(	(	kIx(	(
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
<g/>
,	,	kIx,	,
Al-Qáhira	Al-Qáhira	k1gFnSc1	Al-Qáhira
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vítězná	vítězný	k2eAgFnSc1d1	vítězná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
hovorové	hovorový	k2eAgFnSc6d1	hovorová
egyptské	egyptský	k2eAgFnSc6d1	egyptská
arabštině	arabština	k1gFnSc6	arabština
obvykle	obvykle	k6eAd1	obvykle
Masr	Masra	k1gFnPc2	Masra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
město	město	k1gNnSc4	město
afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Káhira	Káhira	k1gFnSc1	Káhira
sama	sám	k3xTgFnSc1	sám
čítá	čítat	k5eAaImIp3nS	čítat
přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
000	[number]	k4	000
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dvojměstí	dvojměstí	k1gNnSc1	dvojměstí
Káhira-Gíza	Káhira-Gíz	k1gMnSc2	Káhira-Gíz
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
přilehlými	přilehlý	k2eAgMnPc7d1	přilehlý
městy	město	k1gNnPc7	město
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
aglomeraci	aglomerace	k1gFnSc3	aglomerace
Velká	velký	k2eAgFnSc1d1	velká
Káhira	Káhira	k1gFnSc1	Káhira
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
15	[number]	k4	15
907	[number]	k4	907
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
pozici	pozice	k1gFnSc3	pozice
na	na	k7c6	na
kulturním	kulturní	k2eAgNnSc6d1	kulturní
rozhraní	rozhraní	k1gNnSc6	rozhraní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
důležitosti	důležitost	k1gFnSc2	důležitost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
Káhira	Káhira	k1gFnSc1	Káhira
přezdívku	přezdívka	k1gFnSc4	přezdívka
Matka	matka	k1gFnSc1	matka
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
أ	أ	k?	أ
<g/>
ّ	ّ	k?	ّ
ا	ا	k?	ا
<g/>
,	,	kIx,	,
Umm	Umm	k1gMnSc1	Umm
ad-Dunja	ad-Dunja	k1gMnSc1	ad-Dunja
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
kosmopolitnímu	kosmopolitní	k2eAgInSc3d1	kosmopolitní
rázu	ráz	k1gInSc3	ráz
a	a	k8xC	a
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
obyvatel	obyvatel	k1gMnPc2	obyvatel
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Big	Big	k1gFnSc1	Big
Mango	mango	k1gNnSc4	mango
(	(	kIx(	(
<g/>
protiváha	protiváha	k1gFnSc1	protiváha
newyorského	newyorský	k2eAgMnSc2d1	newyorský
Big	Big	k1gMnSc2	Big
Apple	Apple	kA	Apple
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Egypta	Egypt	k1gInSc2	Egypt
při	při	k7c6	při
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Nilu	Nil	k1gInSc2	Nil
<g/>
,	,	kIx,	,
bezprostředně	bezprostředně	k6eAd1	bezprostředně
před	před	k7c7	před
jižním	jižní	k2eAgInSc7d1	jižní
okrajem	okraj	k1gInSc7	okraj
Delty	delta	k1gFnSc2	delta
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nP	ležet
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
při	při	k7c6	při
Nilu	Nil	k1gInSc6	Nil
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nP	tyčit
moderní	moderní	k2eAgInPc1d1	moderní
mrakodrapy	mrakodrap	k1gInPc1	mrakodrap
a	a	k8xC	a
luxusní	luxusní	k2eAgInPc1d1	luxusní
hotely	hotel	k1gInPc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Káhiře	Káhira	k1gFnSc3	Káhira
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
nilské	nilský	k2eAgInPc1d1	nilský
ostrovy	ostrov	k1gInPc1	ostrov
Gezíra	Gezír	k1gMnSc2	Gezír
a	a	k8xC	a
Róda	Ródus	k1gMnSc2	Ródus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
protějším	protější	k2eAgInSc6d1	protější
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
s	s	k7c7	s
Káhirou	Káhira	k1gFnSc7	Káhira
stavebně	stavebně	k6eAd1	stavebně
splývá	splývat	k5eAaImIp3nS	splývat
Gíza	Gíza	k1gFnSc1	Gíza
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
okraji	okraj	k1gInSc6	okraj
stojí	stát	k5eAaImIp3nP	stát
světoznámé	světoznámý	k2eAgFnPc1d1	světoznámá
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kontinentální	kontinentální	k2eAgFnSc3d1	kontinentální
poloze	poloha	k1gFnSc3	poloha
města	město	k1gNnSc2	město
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
tropického	tropický	k2eAgInSc2d1	tropický
a	a	k8xC	a
subtropického	subtropický	k2eAgInSc2d1	subtropický
pásu	pás	k1gInSc2	pás
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
blízkostí	blízkost	k1gFnSc7	blízkost
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
letní	letní	k2eAgFnPc1d1	letní
denní	denní	k2eAgFnPc1d1	denní
teploty	teplota	k1gFnPc1	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
minimální	minimální	k2eAgFnSc2d1	minimální
noční	noční	k2eAgFnSc2d1	noční
teploty	teplota	k1gFnSc2	teplota
klesají	klesat	k5eAaImIp3nP	klesat
pod	pod	k7c7	pod
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Srážkové	srážkový	k2eAgInPc1d1	srážkový
úhrny	úhrn	k1gInPc1	úhrn
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc1d1	nízká
a	a	k8xC	a
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
déšť	déšť	k1gInSc1	déšť
přichází	přicházet	k5eAaImIp3nS	přicházet
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
káhirského	káhirský	k2eAgNnSc2d1	Káhirské
počasí	počasí	k1gNnSc2	počasí
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
chamsíny	chamsín	k1gInPc1	chamsín
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
větry	vítr	k1gInPc4	vítr
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změny	změna	k1gFnSc2	změna
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
proudění	proudění	k1gNnSc2	proudění
nad	nad	k7c7	nad
pouští	poušť	k1gFnSc7	poušť
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
dostává	dostávat	k5eAaImIp3nS	dostávat
velmi	velmi	k6eAd1	velmi
horký	horký	k2eAgInSc1d1	horký
pouštní	pouštní	k2eAgInSc1d1	pouštní
vzduch	vzduch	k1gInSc1	vzduch
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málokterý	málokterý	k3yIgInSc1	málokterý
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
natolik	natolik	k6eAd1	natolik
svázané	svázaný	k2eAgFnPc4d1	svázaná
dějiny	dějiny	k1gFnPc4	dějiny
své	svůj	k3xOyFgInPc4	svůj
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Káhira	Káhira	k1gFnSc1	Káhira
sama	sám	k3xTgFnSc1	sám
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
až	až	k9	až
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
několik	několik	k4yIc4	několik
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
egyptské	egyptský	k2eAgFnSc2d1	egyptská
mytologie	mytologie	k1gFnSc2	mytologie
svedli	svést	k5eAaPmAgMnP	svést
bohové	bůh	k1gMnPc1	bůh
Hor	hora	k1gFnPc2	hora
a	a	k8xC	a
Sutech	Sutech	k1gInSc1	Sutech
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
čtvrti	čtvrt	k1gFnSc2	čtvrt
Heliopolis	Heliopolis	k1gFnSc2	Heliopolis
boj	boj	k1gInSc1	boj
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
Káhiry	Káhira	k1gFnSc2	Káhira
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
egyptské	egyptský	k2eAgFnSc2d1	egyptská
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
Mennofer	Mennofra	k1gFnPc2	Mennofra
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Nové	Nové	k2eAgFnSc2d1	Nové
říše	říš	k1gFnSc2	říš
Iunu	Iunus	k1gInSc2	Iunus
<g/>
.	.	kIx.	.
</s>
<s>
Kameny	kámen	k1gInPc1	kámen
na	na	k7c4	na
pyramidy	pyramida	k1gFnPc4	pyramida
v	v	k7c6	v
Gíze	Gíze	k1gFnSc6	Gíze
byly	být	k5eAaImAgFnP	být
lámány	lámat	k5eAaImNgFnP	lámat
v	v	k7c6	v
lomech	lom	k1gInPc6	lom
káhirského	káhirský	k2eAgInSc2d1	káhirský
kopce	kopec	k1gInSc2	kopec
Muqqatám	Muqqata	k1gFnPc3	Muqqata
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Římanů	Říman	k1gMnPc2	Říman
a	a	k8xC	a
Byzance	Byzanc	k1gFnSc2	Byzanc
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
velká	velký	k2eAgFnSc1d1	velká
pevnost	pevnost	k1gFnSc1	pevnost
Babylon	Babylon	k1gInSc1	Babylon
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
které	který	k3yQgFnSc2	který
postupně	postupně	k6eAd1	postupně
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
koptská	koptský	k2eAgFnSc1d1	koptská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přináší	přinášet	k5eAaImIp3nS	přinášet
Amr	Amr	k1gMnSc1	Amr
Ibn	Ibn	k1gMnSc1	Ibn
al-As	al-As	k6eAd1	al-As
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
chalífy	chalífa	k1gMnSc2	chalífa
Umara	Umar	k1gMnSc2	Umar
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Amrova	Amrův	k2eAgInSc2d1	Amrův
tábora	tábor	k1gInSc2	tábor
založeného	založený	k2eAgInSc2d1	založený
v	v	k7c6	v
r.	r.	kA	r.
641	[number]	k4	641
vzniká	vznikat	k5eAaImIp3nS	vznikat
Fustát	Fustát	k1gInSc4	Fustát
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
arabské	arabský	k2eAgNnSc1d1	arabské
město	město	k1gNnSc1	město
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
Amrova	Amrův	k2eAgFnSc1d1	Amrův
mešita	mešita	k1gFnSc1	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Abbásovských	Abbásovský	k2eAgMnPc2d1	Abbásovský
chalífů	chalífa	k1gMnPc2	chalífa
(	(	kIx(	(
<g/>
750	[number]	k4	750
<g/>
-	-	kIx~	-
<g/>
905	[number]	k4	905
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
chopil	chopit	k5eAaPmAgMnS	chopit
místodržící	místodržící	k1gMnSc1	místodržící
Ahmad	Ahmad	k1gInSc4	Ahmad
Íbn	Íbn	k1gFnSc2	Íbn
Tulún	Tulún	k1gInSc1	Tulún
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nedaleko	nedaleko	k7c2	nedaleko
Fustátu	Fustát	k1gInSc2	Fustát
založil	založit	k5eAaPmAgInS	založit
jako	jako	k9	jako
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
sídlo	sídlo	k1gNnSc4	sídlo
město	město	k1gNnSc1	město
al-Káta	al-Káta	k1gFnSc1	al-Káta
<g/>
'	'	kIx"	'
<g/>
i.	i.	k?	i.
Z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
dochovala	dochovat	k5eAaPmAgFnS	dochovat
především	především	k9	především
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
ukázkou	ukázka	k1gFnSc7	ukázka
baghdádského	baghdádský	k2eAgInSc2d1	baghdádský
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
Tulúnovců	Tulúnovec	k1gMnPc2	Tulúnovec
a	a	k8xC	a
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
šíítská	šíítský	k2eAgFnSc1d1	šíítská
dynastie	dynastie	k1gFnSc1	dynastie
Fátimovců	Fátimovec	k1gInPc2	Fátimovec
(	(	kIx(	(
<g/>
969	[number]	k4	969
<g/>
-	-	kIx~	-
<g/>
1171	[number]	k4	1171
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
také	také	k9	také
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
969	[number]	k4	969
založili	založit	k5eAaPmAgMnP	založit
nedaleko	nedaleko	k7c2	nedaleko
Fustátu	Fustát	k1gInSc2	Fustát
novou	nový	k2eAgFnSc4d1	nová
rezidenční	rezidenční	k2eAgFnSc4d1	rezidenční
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Nazvali	nazvat	k5eAaBmAgMnP	nazvat
ji	on	k3xPp3gFnSc4	on
Misr	Misr	k1gMnSc1	Misr
al-Qáhira	al-Qáhira	k1gMnSc1	al-Qáhira
(	(	kIx(	(
<g/>
Vítězné	vítězný	k2eAgNnSc1d1	vítězné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
po	po	k7c6	po
planetě	planeta	k1gFnSc6	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
přízvisko	přízvisko	k1gNnSc1	přízvisko
Vítězný	vítězný	k2eAgMnSc1d1	vítězný
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalé	zachovalý	k2eAgFnPc1d1	zachovalá
hradby	hradba	k1gFnPc1	hradba
a	a	k8xC	a
městské	městský	k2eAgFnPc1d1	městská
brány	brána	k1gFnPc1	brána
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Fátimovců	Fátimovec	k1gInPc2	Fátimovec
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
další	další	k2eAgFnPc1d1	další
jejich	jejich	k3xOp3gFnPc1	jejich
stavby	stavba	k1gFnPc1	stavba
tvoří	tvořit	k5eAaImIp3nP	tvořit
neodmyslitelnou	odmyslitelný	k2eNgFnSc4d1	neodmyslitelná
součást	součást	k1gFnSc4	součást
staré	starý	k2eAgFnSc2d1	stará
islámské	islámský	k2eAgFnSc2d1	islámská
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Fátimovci	Fátimovec	k1gMnPc1	Fátimovec
také	také	k9	také
založili	založit	k5eAaPmAgMnP	založit
mešitu	mešita	k1gFnSc4	mešita
a	a	k8xC	a
univerzitu	univerzita	k1gFnSc4	univerzita
al-Azhar	al-Azhara	k1gFnPc2	al-Azhara
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
Fustátu	Fustát	k1gInSc2	Fustát
v	v	k7c6	v
r.	r.	kA	r.
1136	[number]	k4	1136
během	během	k7c2	během
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
se	se	k3xPyFc4	se
centrum	centrum	k1gNnSc1	centrum
Egypta	Egypt	k1gInSc2	Egypt
definitivně	definitivně	k6eAd1	definitivně
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
káhirských	káhirský	k2eAgFnPc2d1	Káhirská
mešit	mešita	k1gFnPc2	mešita
a	a	k8xC	a
mauzoleí	mauzoleum	k1gNnPc2	mauzoleum
ale	ale	k9	ale
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Mamlúků	Mamlúk	k1gInPc2	Mamlúk
(	(	kIx(	(
<g/>
1250	[number]	k4	1250
<g/>
-	-	kIx~	-
<g/>
1517	[number]	k4	1517
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Káhira	Káhira	k1gFnSc1	Káhira
sídlem	sídlo	k1gNnSc7	sídlo
chalífy	chalífa	k1gMnSc2	chalífa
a	a	k8xC	a
skutečným	skutečný	k2eAgNnSc7d1	skutečné
centrem	centrum	k1gNnSc7	centrum
celého	celý	k2eAgInSc2d1	celý
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc2	dobytí
osmanskými	osmanský	k2eAgInPc7d1	osmanský
Turky	turek	k1gInPc7	turek
a	a	k8xC	a
popravě	poprava	k1gFnSc6	poprava
posledního	poslední	k2eAgMnSc2d1	poslední
mamlúckého	mamlúcký	k2eAgMnSc2d1	mamlúcký
chalífy	chalífa	k1gMnSc2	chalífa
(	(	kIx(	(
<g/>
1517	[number]	k4	1517
<g/>
)	)	kIx)	)
město	město	k1gNnSc1	město
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
mnoho	mnoho	k6eAd1	mnoho
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
předchozí	předchozí	k2eAgFnSc2d1	předchozí
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
i	i	k9	i
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Napoleonovou	Napoleonův	k2eAgFnSc7d1	Napoleonova
invazí	invaze	k1gFnSc7	invaze
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Muhammadu	Muhammada	k1gFnSc4	Muhammada
Alímu	Alím	k1gInSc2	Alím
vymanit	vymanit	k5eAaPmF	vymanit
Egypt	Egypt	k1gInSc1	Egypt
z	z	k7c2	z
přímého	přímý	k2eAgInSc2d1	přímý
osmanského	osmanský	k2eAgInSc2d1	osmanský
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnPc3	jeho
schopnostem	schopnost	k1gFnPc3	schopnost
začala	začít	k5eAaPmAgFnS	začít
Káhira	Káhira	k1gFnSc1	Káhira
znovu	znovu	k6eAd1	znovu
získávat	získávat	k5eAaImF	získávat
svůj	svůj	k3xOyFgInSc4	svůj
ztracený	ztracený	k2eAgInSc4d1	ztracený
lesk	lesk	k1gInSc4	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
země	zem	k1gFnSc2	zem
umožnil	umožnit	k5eAaPmAgInS	umožnit
novou	nový	k2eAgFnSc4d1	nová
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
výstavbu	výstavba	k1gFnSc4	výstavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
jeho	jeho	k3xOp3gMnPc2	jeho
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
následovníků	následovník	k1gMnPc2	následovník
<g/>
.	.	kIx.	.
</s>
<s>
Káhira	Káhira	k1gFnSc1	Káhira
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
o	o	k7c6	o
moderní	moderní	k2eAgFnSc6d1	moderní
čtvrti	čtvrt	k1gFnSc6	čtvrt
stavěné	stavěný	k2eAgFnSc6d1	stavěná
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
evropských	evropský	k2eAgFnPc2d1	Evropská
metropolí	metropol	k1gFnPc2	metropol
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Heliopolis	Heliopolis	k1gFnSc1	Heliopolis
<g/>
,	,	kIx,	,
Helwán	Helwán	k1gMnSc1	Helwán
nebo	nebo	k8xC	nebo
Zamálik	Zamálik	k1gMnSc1	Zamálik
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Gezíra	Gezír	k1gMnSc2	Gezír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
značně	značně	k6eAd1	značně
proměnily	proměnit	k5eAaPmAgFnP	proměnit
tvář	tvář	k1gFnSc4	tvář
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
prakticky	prakticky	k6eAd1	prakticky
nezměněna	změněn	k2eNgFnSc1d1	nezměněna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
budovány	budovat	k5eAaImNgFnP	budovat
především	především	k9	především
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
lidové	lidový	k2eAgFnPc1d1	lidová
čtvrti	čtvrt	k1gFnPc1	čtvrt
(	(	kIx(	(
<g/>
Imbába	Imbába	k1gMnSc1	Imbába
<g/>
,	,	kIx,	,
Bulaq	Bulaq	k1gMnSc1	Bulaq
<g/>
,	,	kIx,	,
Madínat	Madínat	k1gMnSc1	Madínat
Nasr	Nasr	k1gMnSc1	Nasr
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
absorbovat	absorbovat	k5eAaBmF	absorbovat
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
stěhujících	stěhující	k2eAgMnPc2d1	stěhující
se	se	k3xPyFc4	se
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
či	či	k8xC	či
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
Přetlak	přetlak	k1gInSc1	přetlak
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
egyptská	egyptský	k2eAgFnSc1d1	egyptská
vláda	vláda	k1gFnSc1	vláda
řešit	řešit	k5eAaImF	řešit
i	i	k8xC	i
budováním	budování	k1gNnSc7	budování
satelitních	satelitní	k2eAgNnPc2d1	satelitní
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Město	město	k1gNnSc1	město
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
10	[number]	k4	10
<g/>
.	.	kIx.	.
rammadánu	rammadán	k2eAgFnSc4d1	rammadán
<g/>
,	,	kIx,	,
Madínat	Madínat	k1gFnSc4	Madínat
Sadat	Sadat	k1gInSc4	Sadat
<g/>
,	,	kIx,	,
New	New	k1gFnSc4	New
Cairo	Cairo	k1gNnSc1	Cairo
<g/>
)	)	kIx)	)
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
či	či	k8xC	či
vzdálenějším	vzdálený	k2eAgNnSc6d2	vzdálenější
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
všechnu	všechen	k3xTgFnSc4	všechen
snahu	snaha	k1gFnSc4	snaha
místních	místní	k2eAgMnPc2d1	místní
úřadů	úřad	k1gInPc2	úřad
o	o	k7c4	o
nějakou	nějaký	k3yIgFnSc4	nějaký
formu	forma	k1gFnSc4	forma
regulace	regulace	k1gFnSc2	regulace
se	se	k3xPyFc4	se
Káhira	Káhira	k1gFnSc1	Káhira
i	i	k9	i
nadále	nadále	k6eAd1	nadále
nekontrolovatelně	kontrolovatelně	k6eNd1	kontrolovatelně
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jejích	její	k3xOp3gMnPc2	její
obyvatel	obyvatel	k1gMnPc2	obyvatel
prudce	prudko	k6eAd1	prudko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
al-Azhar	al-Azhara	k1gFnPc2	al-Azhara
Nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
káhirská	káhirský	k2eAgFnSc1d1	Káhirská
mešita	mešita	k1gFnSc1	mešita
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
970	[number]	k4	970
z	z	k7c2	z
popudu	popud	k1gInSc2	popud
fátimovského	fátimovský	k2eAgMnSc2d1	fátimovský
dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
Egypta	Egypt	k1gInSc2	Egypt
Džauhára	Džauhár	k1gMnSc4	Džauhár
as-Sikiliho	as-Sikili	k1gMnSc4	as-Sikili
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
975	[number]	k4	975
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
mešitě	mešita	k1gFnSc6	mešita
založena	založit	k5eAaPmNgFnS	založit
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
stále	stále	k6eAd1	stále
fungující	fungující	k2eAgFnSc7d1	fungující
univerzitou	univerzita	k1gFnSc7	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
si	se	k3xPyFc3	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
pověst	pověst	k1gFnSc4	pověst
nejdůležitějšího	důležitý	k2eAgNnSc2d3	nejdůležitější
střediska	středisko	k1gNnSc2	středisko
zabývajícího	zabývající	k2eAgInSc2d1	zabývající
se	se	k3xPyFc4	se
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
výkladem	výklad	k1gInSc7	výklad
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
výukou	výuka	k1gFnSc7	výuka
klasické	klasický	k2eAgFnSc2d1	klasická
arabštiny	arabština	k1gFnSc2	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Bazar	bazar	k1gInSc1	bazar
Chán	chán	k1gMnSc1	chán
al-Chalílí	al-Chalílet	k5eAaPmIp3nS	al-Chalílet
je	on	k3xPp3gNnSc4	on
největší	veliký	k2eAgInSc1d3	veliký
bazar	bazar	k1gInSc1	bazar
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Muski	Musk	k1gFnSc2	Musk
tvoří	tvořit	k5eAaImIp3nS	tvořit
jádro	jádro	k1gNnSc1	jádro
islámské	islámský	k2eAgFnSc2d1	islámská
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
stojí	stát	k5eAaImIp3nP	stát
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
původní	původní	k2eAgFnSc2d1	původní
Wikály	Wikála	k1gFnSc2	Wikála
al-Bazar	al-Bazara	k1gFnPc2	al-Bazara
založené	založený	k2eAgFnPc4d1	založená
v	v	k7c6	v
r.	r.	kA	r.
1382	[number]	k4	1382
pro	pro	k7c4	pro
ubytování	ubytování	k1gNnSc4	ubytování
cizích	cizí	k2eAgMnPc2d1	cizí
kupců	kupec	k1gMnPc2	kupec
a	a	k8xC	a
skladování	skladování	k1gNnSc2	skladování
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
bazaru	bazar	k1gInSc2	bazar
plná	plný	k2eAgFnSc1d1	plná
především	především	k6eAd1	především
kýčovitého	kýčovitý	k2eAgNnSc2d1	kýčovité
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
zlatých	zlatý	k2eAgInPc2d1	zlatý
šperků	šperk	k1gInPc2	šperk
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgFnPc4d1	hlavní
turistické	turistický	k2eAgFnPc4d1	turistická
trasy	trasa	k1gFnPc4	trasa
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
sehnat	sehnat	k5eAaPmF	sehnat
i	i	k9	i
běžné	běžný	k2eAgNnSc4d1	běžné
zboží	zboží	k1gNnSc4	zboží
jako	jako	k8xC	jako
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
nádobí	nádobí	k1gNnSc4	nádobí
nebo	nebo	k8xC	nebo
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Citadela	citadela	k1gFnSc1	citadela
je	být	k5eAaImIp3nS	být
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
pevnost	pevnost	k1gFnSc1	pevnost
poblíž	poblíž	k7c2	poblíž
úpatí	úpatí	k1gNnSc2	úpatí
kopce	kopec	k1gInSc2	kopec
Mukkatám	Mukkata	k1gFnPc3	Mukkata
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
zakladatelem	zakladatel	k1gMnSc7	zakladatel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1176	[number]	k4	1176
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Saláh	Saláh	k1gMnSc1	Saláh
ad-Dín	ad-Dín	k1gMnSc1	ad-Dín
ibn	ibn	k?	ibn
Ajjúb	Ajjúb	k1gMnSc1	Ajjúb
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Citadela	citadela	k1gFnSc1	citadela
sídlem	sídlo	k1gNnSc7	sídlo
egyptských	egyptský	k2eAgMnPc2d1	egyptský
vládců	vládce	k1gMnPc2	vládce
či	či	k8xC	či
místodržících	místodržící	k1gMnPc2	místodržící
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
byla	být	k5eAaImAgFnS	být
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
původních	původní	k2eAgFnPc2d1	původní
budov	budova	k1gFnPc2	budova
srovnána	srovnán	k2eAgFnSc1d1	srovnána
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
uvolnily	uvolnit	k5eAaPmAgFnP	uvolnit
místo	místo	k6eAd1	místo
nové	nový	k2eAgFnSc3d1	nová
výstavbě	výstavba	k1gFnSc3	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavější	zajímavý	k2eAgFnSc7d3	nejzajímavější
stavbou	stavba	k1gFnSc7	stavba
Citadely	citadela	k1gFnSc2	citadela
je	být	k5eAaImIp3nS	být
Alabastrová	alabastrový	k2eAgFnSc1d1	alabastrová
mešita	mešita	k1gFnSc1	mešita
postavená	postavený	k2eAgFnSc1d1	postavená
Muhammadem	Muhammad	k1gInSc7	Muhammad
Alím	Alím	k1gMnSc1	Alím
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k6eAd1	také
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
mešity	mešita	k1gFnSc2	mešita
je	být	k5eAaImIp3nS	být
i	i	k9	i
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
hodinami	hodina	k1gFnPc7	hodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
Muhammadu	Muhammada	k1gFnSc4	Muhammada
Alimu	Alimo	k1gNnSc3	Alimo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
věnoval	věnovat	k5eAaPmAgMnS	věnovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
Filip	Filip	k1gMnSc1	Filip
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
důležité	důležitý	k2eAgFnPc4d1	důležitá
památky	památka	k1gFnPc4	památka
Citadely	citadela	k1gFnSc2	citadela
patří	patřit	k5eAaImIp3nP	patřit
mešity	mešita	k1gFnPc1	mešita
sultána	sultána	k1gFnSc1	sultána
an-Násira	an-Násira	k1gFnSc1	an-Násira
Muhammada	Muhammada	k1gFnSc1	Muhammada
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
mamlúcká	mamlúcký	k2eAgFnSc1d1	mamlúcká
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
Citadele	citadela	k1gFnSc6	citadela
<g/>
,	,	kIx,	,
a	a	k8xC	a
mešita	mešita	k1gFnSc1	mešita
Sulejmána	Sulejmán	k1gMnSc2	Sulejmán
Nádherného	nádherný	k2eAgMnSc2d1	nádherný
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
stavba	stavba	k1gFnSc1	stavba
v	v	k7c6	v
osmanském	osmanský	k2eAgInSc6d1	osmanský
stylu	styl	k1gInSc6	styl
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
harému	harém	k1gInSc6	harém
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nachází	nacházet	k5eAaImIp3nS	nacházet
egyptské	egyptský	k2eAgNnSc4d1	egyptské
národní	národní	k2eAgNnSc4d1	národní
vojenské	vojenský	k2eAgNnSc4d1	vojenské
muzeum	muzeum	k1gNnSc4	muzeum
zachycující	zachycující	k2eAgNnSc4d1	zachycující
vojenské	vojenský	k2eAgFnPc4d1	vojenská
dějiny	dějiny	k1gFnPc4	dějiny
Egypta	Egypt	k1gInSc2	Egypt
od	od	k7c2	od
faraónů	faraón	k1gMnPc2	faraón
až	až	k9	až
po	po	k7c4	po
nástup	nástup	k1gInSc4	nástup
prezidenta	prezident	k1gMnSc2	prezident
Mubaraka	Mubarak	k1gMnSc2	Mubarak
s	s	k7c7	s
venkovní	venkovní	k2eAgFnSc7d1	venkovní
expozicí	expozice	k1gFnSc7	expozice
tanků	tank	k1gInPc2	tank
a	a	k8xC	a
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Egyptské	egyptský	k2eAgNnSc1d1	egyptské
muzeum	muzeum	k1gNnSc1	muzeum
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
sbírku	sbírka	k1gFnSc4	sbírka
staroegyptských	staroegyptský	k2eAgFnPc2d1	staroegyptská
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
Francouzem	Francouz	k1gMnSc7	Francouz
Augustem	August	k1gMnSc7	August
Mariettem	Mariett	k1gMnSc7	Mariett
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
r.	r.	kA	r.
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
padesáti	padesát	k4xCc6	padesát
místnostech	místnost	k1gFnPc6	místnost
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
téměř	téměř	k6eAd1	téměř
130	[number]	k4	130
tisíc	tisíc	k4xCgInPc2	tisíc
různých	různý	k2eAgInPc2d1	různý
exponátů	exponát	k1gInPc2	exponát
od	od	k7c2	od
kolosálních	kolosální	k2eAgFnPc2d1	kolosální
soch	socha	k1gFnPc2	socha
po	po	k7c4	po
drobné	drobný	k2eAgInPc4d1	drobný
šperky	šperk	k1gInPc4	šperk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
historii	historie	k1gFnSc4	historie
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
sbírek	sbírka	k1gFnPc2	sbírka
jsou	být	k5eAaImIp3nP	být
poklady	poklad	k1gInPc4	poklad
z	z	k7c2	z
Tutanchamonova	Tutanchamonův	k2eAgInSc2d1	Tutanchamonův
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnPc2	jeho
dvou	dva	k4xCgFnPc2	dva
zlatých	zlatý	k2eAgFnPc2d1	zlatá
rakví	rakev	k1gFnPc2	rakev
a	a	k8xC	a
slavné	slavný	k2eAgFnSc2d1	slavná
posmrtné	posmrtný	k2eAgFnSc2d1	posmrtná
masky	maska	k1gFnSc2	maska
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
i	i	k9	i
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
papyrus	papyrus	k1gInSc4	papyrus
a	a	k8xC	a
lotos	lotos	k1gInSc4	lotos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
Mariettův	Mariettův	k2eAgInSc1d1	Mariettův
hrob	hrob	k1gInSc1	hrob
a	a	k8xC	a
památník	památník	k1gInSc1	památník
významných	významný	k2eAgMnPc2d1	významný
egyptologů	egyptolog	k1gMnPc2	egyptolog
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
současná	současný	k2eAgFnSc1d1	současná
prostorová	prostorový	k2eAgFnSc1d1	prostorová
kapacita	kapacita	k1gFnSc1	kapacita
muzea	muzeum	k1gNnSc2	muzeum
přestává	přestávat	k5eAaImIp3nS	přestávat
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
<g/>
,	,	kIx,	,
připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
stavba	stavba	k1gFnSc1	stavba
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
poblíž	poblíž	k6eAd1	poblíž
pyramid	pyramid	k1gInSc4	pyramid
s	s	k7c7	s
plánovaným	plánovaný	k2eAgNnSc7d1	plánované
otevřením	otevření	k1gNnSc7	otevření
po	po	k7c6	po
r.	r.	kA	r.
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Heliopolis	Heliopolis	k1gFnSc1	Heliopolis
je	být	k5eAaImIp3nS	být
archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
severovýchodním	severovýchodní	k2eAgNnSc6d1	severovýchodní
káhirském	káhirský	k2eAgNnSc6d1	Káhirské
předměstí	předměstí	k1gNnSc6	předměstí
Heliopolis	Heliopolis	k1gFnSc2	Heliopolis
<g/>
.	.	kIx.	.
</s>
<s>
Stávalo	stávat	k5eAaImAgNnS	stávat
zde	zde	k6eAd1	zde
město	město	k1gNnSc1	město
Iunu	Iunus	k1gInSc2	Iunus
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
jediný	jediný	k2eAgInSc1d1	jediný
stojící	stojící	k2eAgInSc1d1	stojící
obelisk	obelisk	k1gInSc1	obelisk
<g/>
.	.	kIx.	.
</s>
<s>
Novodobé	novodobý	k2eAgFnPc1d1	novodobá
Heliopolis	Heliopolis	k1gFnPc1	Heliopolis
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
secesním	secesní	k2eAgInSc6d1	secesní
stylu	styl	k1gInSc6	styl
vystavět	vystavět	k5eAaPmF	vystavět
belgický	belgický	k2eAgMnSc1d1	belgický
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
průmyslník	průmyslník	k1gMnSc1	průmyslník
Édouard	Édouard	k1gMnSc1	Édouard
Louis	Louis	k1gMnSc1	Louis
Joseph	Joseph	k1gMnSc1	Joseph
baron	baron	k1gMnSc1	baron
Empain	Empain	k1gMnSc1	Empain
a	a	k8xC	a
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
luxusní	luxusní	k2eAgFnPc4d1	luxusní
káhirské	káhirský	k2eAgFnPc4d1	Káhirská
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
tu	tu	k6eAd1	tu
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
egyptský	egyptský	k2eAgMnSc1d1	egyptský
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Islámská	islámský	k2eAgFnSc1d1	islámská
Káhira	Káhira	k1gFnSc1	Káhira
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
část	část	k1gFnSc1	část
Káhiry	Káhira	k1gFnSc2	Káhira
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
fátimovské	fátimovský	k2eAgFnSc2d1	fátimovská
rezidenční	rezidenční	k2eAgFnSc2d1	rezidenční
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
původních	původní	k2eAgFnPc2d1	původní
hradeb	hradba	k1gFnPc2	hradba
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
bránami	brána	k1gFnPc7	brána
<g/>
:	:	kIx,	:
Báb	Báb	k1gFnSc7	Báb
Zuwajlou	Zuwajlá	k1gFnSc4	Zuwajlá
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
Báb	Báb	k1gMnSc1	Báb
al-Futúh	al-Futúh	k1gMnSc1	al-Futúh
a	a	k8xC	a
Báb	Báb	k1gMnSc1	Báb
an-Nasr	an-Nasr	k1gMnSc1	an-Nasr
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
tepnu	tepna	k1gFnSc4	tepna
islámské	islámský	k2eAgFnSc2d1	islámská
Káhiry	Káhira	k1gFnSc2	Káhira
představuje	představovat	k5eAaImIp3nS	představovat
ulice	ulice	k1gFnSc1	ulice
al-Mu	al-Mus	k1gInSc2	al-Mus
<g/>
'	'	kIx"	'
<g/>
izz	izz	k?	izz
li-Dín	li-Dín	k1gMnSc1	li-Dín
Alláh	Alláh	k1gMnSc1	Alláh
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nepřeberného	přeberný	k2eNgNnSc2d1	nepřeberné
množství	množství	k1gNnSc2	množství
historických	historický	k2eAgFnPc2d1	historická
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
Hakimova	Hakimův	k2eAgFnSc1d1	Hakimova
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
a	a	k8xC	a
madrasa	madrasa	k1gFnSc1	madrasa
sultána	sultán	k1gMnSc2	sultán
Ghaurího	Ghaurí	k1gMnSc2	Ghaurí
<g/>
,	,	kIx,	,
mešita	mešita	k1gFnSc1	mešita
al-Azhar	al-Azhar	k1gInSc1	al-Azhar
<g/>
,	,	kIx,	,
historický	historický	k2eAgInSc1d1	historický
mamlúcký	mamlúcký	k2eAgInSc1d1	mamlúcký
dům	dům	k1gInSc1	dům
Bajt	bajt	k1gInSc1	bajt
Suhajmí	Suhajmí	k1gNnSc4	Suhajmí
atp.	atp.	kA	atp.
Islámská	islámský	k2eAgFnSc1d1	islámská
Káhira	Káhira	k1gFnSc1	Káhira
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Káhirská	káhirský	k2eAgFnSc1d1	Káhirská
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
186	[number]	k4	186
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
televizní	televizní	k2eAgFnSc1d1	televizní
a	a	k8xC	a
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
r.	r.	kA	r.
1961	[number]	k4	1961
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
lotosu	lotos	k1gInSc2	lotos
stojící	stojící	k2eAgFnPc4d1	stojící
v	v	k7c6	v
parku	park	k1gInSc6	park
na	na	k7c6	na
nilském	nilský	k2eAgInSc6d1	nilský
ostrově	ostrov	k1gInSc6	ostrov
Gezíra	Gezír	k1gMnSc2	Gezír
<g/>
.	.	kIx.	.
</s>
<s>
Monumentální	monumentální	k2eAgInSc1d1	monumentální
portál	portál	k1gInSc1	portál
se	s	k7c7	s
širokým	široký	k2eAgNnSc7d1	široké
vstupním	vstupní	k2eAgNnSc7d1	vstupní
schodištěm	schodiště	k1gNnSc7	schodiště
z	z	k7c2	z
asuánské	asuánský	k2eAgFnSc2d1	Asuánská
růžové	růžový	k2eAgFnSc2d1	růžová
žuly	žula	k1gFnSc2	žula
uvádí	uvádět	k5eAaImIp3nS	uvádět
vstupní	vstupní	k2eAgFnSc4d1	vstupní
halu	hala	k1gFnSc4	hala
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vyzdobena	vyzdobit	k5eAaPmNgFnS	vyzdobit
barevnými	barevný	k2eAgInPc7d1	barevný
okny	okno	k1gNnPc7	okno
a	a	k8xC	a
nástěnnou	nástěnný	k2eAgFnSc7d1	nástěnná
mozaikou	mozaika	k1gFnSc7	mozaika
znázorňující	znázorňující	k2eAgFnSc4d1	znázorňující
tradiční	tradiční	k2eAgFnSc4d1	tradiční
i	i	k8xC	i
současné	současný	k2eAgInPc4d1	současný
výjevy	výjev	k1gInPc4	výjev
ze	z	k7c2	z
života	život	k1gInSc2	život
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Věž	věž	k1gFnSc1	věž
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
otáčecí	otáčecí	k2eAgFnSc4d1	otáčecí
restauraci	restaurace	k1gFnSc4	restaurace
a	a	k8xC	a
bar	bar	k1gInSc4	bar
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
viditelnosti	viditelnost	k1gFnSc6	viditelnost
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
nabízí	nabízet	k5eAaImIp3nS	nabízet
výhled	výhled	k1gInSc1	výhled
na	na	k7c4	na
pyramidy	pyramida	k1gFnPc4	pyramida
v	v	k7c6	v
Gíze	Gíze	k1gFnSc6	Gíze
<g/>
,	,	kIx,	,
Sakkáře	Sakkář	k1gMnSc2	Sakkář
a	a	k8xC	a
Abúsíru	Abúsír	k1gInSc2	Abúsír
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
i	i	k8xC	i
Nilskou	nilský	k2eAgFnSc4d1	nilská
přehradu	přehrada	k1gFnSc4	přehrada
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
budova	budova	k1gFnSc1	budova
nové	nový	k2eAgFnSc2d1	nová
káhirské	káhirský	k2eAgFnSc2d1	Káhirská
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
obrovské	obrovský	k2eAgNnSc1d1	obrovské
sportovní	sportovní	k2eAgNnSc1d1	sportovní
centrum	centrum	k1gNnSc1	centrum
patřící	patřící	k2eAgFnSc2d1	patřící
klubu	klub	k1gInSc2	klub
Al-Ahly	Al-Ahly	k1gMnPc1	Al-Ahly
SC	SC	kA	SC
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgNnSc1d1	jižní
město	město	k1gNnSc1	město
mrtvých	mrtvý	k1gMnPc2	mrtvý
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Citadely	citadela	k1gFnSc2	citadela
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
chudá	chudý	k2eAgFnSc1d1	chudá
čtvrť	čtvrť	k1gFnSc1	čtvrť
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
hřbitov	hřbitov	k1gInSc4	hřbitov
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
funkční	funkční	k2eAgFnPc1d1	funkční
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
útočištěm	útočiště	k1gNnSc7	útočiště
a	a	k8xC	a
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
žijících	žijící	k2eAgFnPc2d1	žijící
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
budovách	budova	k1gFnPc6	budova
postavených	postavený	k2eAgFnPc2d1	postavená
mezi	mezi	k7c7	mezi
hroby	hrob	k1gInPc7	hrob
<g/>
,	,	kIx,	,
mauzolei	mauzoleum	k1gNnPc7	mauzoleum
a	a	k8xC	a
mešitami	mešita	k1gFnPc7	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
staveb	stavba	k1gFnPc2	stavba
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
káhirské	káhirský	k2eAgFnPc4d1	Káhirská
památky	památka	k1gFnPc4	památka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vznikaly	vznikat	k5eAaImAgFnP	vznikat
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejzajímavější	zajímavý	k2eAgFnPc4d3	nejzajímavější
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
počítat	počítat	k5eAaImF	počítat
mešitu	mešita	k1gFnSc4	mešita
Sajjídy	Sajjída	k1gFnSc2	Sajjída
Náfisy	Náfis	k1gInPc1	Náfis
s	s	k7c7	s
hrobkou	hrobka	k1gFnSc7	hrobka
abbásovských	abbásovský	k2eAgMnPc2d1	abbásovský
chalífů	chalífa	k1gMnPc2	chalífa
(	(	kIx(	(
<g/>
1242	[number]	k4	1242
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
egyptské	egyptský	k2eAgFnSc2d1	egyptská
vládkyně	vládkyně	k1gFnSc2	vládkyně
Šagarrat	Šagarrat	k1gMnSc1	Šagarrat
ad-Dur	ad-Dur	k1gMnSc1	ad-Dur
(	(	kIx(	(
<g/>
1250	[number]	k4	1250
<g/>
)	)	kIx)	)
či	či	k8xC	či
komplex	komplex	k1gInSc4	komplex
Huš	huš	k0	huš
al-Báša	al-Báša	k6eAd1	al-Báša
<g/>
,	,	kIx,	,
hrobku	hrobek	k1gInSc6	hrobek
příbuzných	příbuzný	k1gMnPc2	příbuzný
Muhammada	Muhammada	k1gFnSc1	Muhammada
Alího	Alí	k1gMnSc2	Alí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
hřbitova	hřbitov	k1gInSc2	hřbitov
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
mešita	mešita	k1gFnSc1	mešita
s	s	k7c7	s
hrobkou	hrobka	k1gFnSc7	hrobka
imáma	imám	k1gMnSc2	imám
aš-Šáfi	aš-Šáfi	k1gNnSc2	aš-Šáfi
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc4	zakladatel
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
čtyřech	čtyři	k4xCgFnPc2	čtyři
islámských	islámský	k2eAgFnPc2d1	islámská
právních	právní	k2eAgFnPc2d1	právní
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgNnSc1d1	severní
město	město	k1gNnSc1	město
mrtvých	mrtvý	k1gMnPc2	mrtvý
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgNnSc4d2	menší
než	než	k8xS	než
jižní	jižní	k2eAgNnSc4d1	jižní
město	město	k1gNnSc4	město
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
islámské	islámský	k2eAgFnSc2d1	islámská
Káhiry	Káhira	k1gFnSc2	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
severního	severní	k2eAgNnSc2d1	severní
města	město	k1gNnSc2	město
představují	představovat	k5eAaImIp3nP	představovat
tří	tři	k4xCgFnPc2	tři
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
komplexy	komplex	k1gInPc7	komplex
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pohřbení	pohřbený	k2eAgMnPc1d1	pohřbený
mamlúčtí	mamlúcký	k2eAgMnPc1d1	mamlúcký
sultáni	sultán	k1gMnPc1	sultán
Kajtbáj	Kajtbáj	k1gMnSc1	Kajtbáj
<g/>
,	,	kIx,	,
Barkúk	Barkúk	k1gMnSc1	Barkúk
a	a	k8xC	a
Inal	Inal	k1gMnSc1	Inal
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
také	také	k9	také
hřbitov	hřbitov	k1gInSc1	hřbitov
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc1d1	uložen
ostatky	ostatek	k1gInPc1	ostatek
egyptských	egyptský	k2eAgMnPc2d1	egyptský
vojáků	voják	k1gMnPc2	voják
padlých	padlý	k1gMnPc2	padlý
v	v	k7c6	v
arabsko-izraelských	arabskozraelský	k2eAgFnPc6d1	arabsko-izraelská
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kajtbájově	Kajtbájův	k2eAgFnSc6d1	Kajtbájův
mešitě	mešita	k1gFnSc6	mešita
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
její	její	k3xOp3gMnPc1	její
správci	správce	k1gMnPc1	správce
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgMnSc6	který
jsou	být	k5eAaImIp3nP	být
stopy	stopa	k1gFnPc4	stopa
chodidel	chodidlo	k1gNnPc2	chodidlo
patřící	patřící	k2eAgInSc1d1	patřící
údajně	údajně	k6eAd1	údajně
proroku	prorok	k1gMnSc3	prorok
Muhammadovi	Muhammada	k1gMnSc3	Muhammada
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
Káhira	Káhira	k1gFnSc1	Káhira
-	-	kIx~	-
Koptské	koptský	k2eAgNnSc4d1	Koptské
město	město	k1gNnSc4	město
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
čtvrť	čtvrť	k1gFnSc1	čtvrť
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
římské	římský	k2eAgFnSc2d1	římská
pevnosti	pevnost	k1gFnSc2	pevnost
Babylon	Babylon	k1gInSc1	Babylon
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vykopávky	vykopávka	k1gFnPc4	vykopávka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Koptském	koptský	k2eAgNnSc6d1	Koptské
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řecký	řecký	k2eAgInSc1d1	řecký
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
Mar	Mar	k1gFnSc2	Mar
Girgis	Girgis	k1gFnSc2	Girgis
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kostely	kostel	k1gInPc1	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnPc1	Barbora
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Sergia	Sergia	k1gFnSc1	Sergia
a	a	k8xC	a
nově	nově	k6eAd1	nově
zrekonstruované	zrekonstruovaný	k2eAgNnSc1d1	zrekonstruované
koptské	koptský	k2eAgNnSc1d1	Koptské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
těchto	tento	k3xDgFnPc2	tento
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
také	také	k9	také
množství	množství	k1gNnSc4	množství
méně	málo	k6eAd2	málo
významných	významný	k2eAgInPc2d1	významný
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
hřbitovů	hřbitov	k1gInPc2	hřbitov
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
různých	různý	k2eAgFnPc2d1	různá
církví	církev	k1gFnPc2	církev
(	(	kIx(	(
<g/>
koptské	koptský	k2eAgFnSc2d1	koptská
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgFnSc2d1	řecká
<g/>
,	,	kIx,	,
arménské	arménský	k2eAgFnSc2d1	arménská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koptské	koptský	k2eAgFnSc6d1	koptská
částí	část	k1gFnSc7	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
Ben	Ben	k1gInSc1	Ben
Ezrova	Ezrův	k2eAgFnSc1d1	Ezrův
synagoga	synagoga	k1gFnSc1	synagoga
(	(	kIx(	(
<g/>
zal	zal	k?	zal
<g/>
.	.	kIx.	.
r.	r.	kA	r.
827	[number]	k4	827
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
koptského	koptský	k2eAgNnSc2d1	Koptské
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nP	ležet
vykopávky	vykopávka	k1gFnPc1	vykopávka
původního	původní	k2eAgNnSc2d1	původní
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Fustátu	Fustát	k1gInSc2	Fustát
a	a	k8xC	a
Amrova	Amrův	k2eAgFnSc1d1	Amrův
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
první	první	k4xOgFnSc2	první
egyptské	egyptský	k2eAgFnSc2d1	egyptská
mešity	mešita	k1gFnSc2	mešita
z	z	k7c2	z
r.	r.	kA	r.
641	[number]	k4	641
<g/>
.	.	kIx.	.
</s>
<s>
Visutý	visutý	k2eAgInSc4d1	visutý
kostel	kostel	k1gInSc4	kostel
Al-Muallaka	Al-Muallak	k1gMnSc2	Al-Muallak
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
několikrán	několikrán	k1gInSc1	několikrán
upravován	upravován	k2eAgInSc1d1	upravován
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
složité	složitý	k2eAgNnSc1d1	složité
určit	určit	k5eAaPmF	určit
jeho	jeho	k3xOp3gNnSc4	jeho
přesné	přesný	k2eAgNnSc4d1	přesné
stáří	stáří	k1gNnSc4	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
Kostel	kostel	k1gInSc1	kostel
má	mít	k5eAaImIp3nS	mít
interiér	interiér	k1gInSc1	interiér
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
bohatou	bohatý	k2eAgFnSc7d1	bohatá
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vzácných	vzácný	k2eAgFnPc2d1	vzácná
ikon	ikona	k1gFnPc2	ikona
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
Íbn	Íbn	k?	Íbn
Tulúnova	Tulúnův	k2eAgFnSc1d1	Tulúnův
mešita	mešita	k1gFnSc1	mešita
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
funkční	funkční	k2eAgFnSc1d1	funkční
káhirská	káhirský	k2eAgFnSc1d1	Káhirská
mešita	mešita	k1gFnSc1	mešita
vystavěná	vystavěný	k2eAgFnSc1d1	vystavěná
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
876	[number]	k4	876
a	a	k8xC	a
879	[number]	k4	879
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
abbásovským	abbásovský	k2eAgMnSc7d1	abbásovský
místodržícím	místodržící	k1gMnSc7	místodržící
Íbn	Íbn	k1gMnSc7	Íbn
Tulúnem	Tulún	k1gMnSc7	Tulún
v	v	k7c6	v
architektonickém	architektonický	k2eAgInSc6d1	architektonický
stylu	styl	k1gInSc6	styl
iráckých	irácký	k2eAgFnPc2d1	irácká
mešit	mešita	k1gFnPc2	mešita
včetně	včetně	k7c2	včetně
spirálového	spirálový	k2eAgInSc2d1	spirálový
minaretu	minaret	k1gInSc2	minaret
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
mešity	mešita	k1gFnSc2	mešita
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Gayer-Andersonovo	Gayer-Andersonův	k2eAgNnSc1d1	Gayer-Andersonův
muzeum	muzeum	k1gNnSc1	muzeum
uchovávající	uchovávající	k2eAgNnSc1d1	uchovávající
množství	množství	k1gNnSc1	množství
orientálních	orientální	k2eAgFnPc2d1	orientální
sbírek	sbírka	k1gFnPc2	sbírka
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
islámského	islámský	k2eAgInSc2d1	islámský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Dálného	dálný	k2eAgInSc2d1	dálný
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
britský	britský	k2eAgMnSc1d1	britský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
lékař	lékař	k1gMnSc1	lékař
John	John	k1gMnSc1	John
Gayer-Anderson	Gayer-Anderson	k1gMnSc1	Gayer-Anderson
<g/>
.	.	kIx.	.
</s>
<s>
Madrasa	Madrasa	k1gFnSc1	Madrasa
sultána	sultán	k1gMnSc2	sultán
Hasana	Hasan	k1gMnSc2	Hasan
Káhirská	káhirský	k2eAgFnSc1d1	Káhirská
univerzita	univerzita	k1gFnSc1	univerzita
Americká	americký	k2eAgFnSc1d1	americká
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
Univerzita	univerzita	k1gFnSc1	univerzita
Ain	Ain	k1gFnPc2	Ain
Šams	Šamsa	k1gFnPc2	Šamsa
Káhirská	káhirský	k2eAgFnSc1d1	Káhirská
opera	opera	k1gFnSc1	opera
Muzeum	muzeum	k1gNnSc1	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc3	umění
Muzeum	muzeum	k1gNnSc1	muzeum
islámského	islámský	k2eAgNnSc2d1	islámské
umění	umění	k1gNnSc2	umění
Zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
muzeum	muzeum	k1gNnSc4	muzeum
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
centrum	centrum	k1gNnSc4	centrum
al-Ahly	al-Ahnout	k5eAaPmAgFnP	al-Ahnout
Gezira	Gezira	k1gFnSc1	Gezira
Sporting	Sporting	k1gInSc4	Sporting
Club	club	k1gInSc1	club
Káhirský	káhirský	k2eAgInSc1d1	káhirský
stadion	stadion	k1gInSc1	stadion
Káhirská	káhirský	k2eAgFnSc1d1	Káhirská
ZOO	zoo	k1gFnSc1	zoo
Ammán	Ammán	k1gInSc1	Ammán
<g/>
,	,	kIx,	,
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Athény	Athéna	k1gFnSc2	Athéna
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Alžír	Alžír	k1gInSc1	Alžír
<g/>
,	,	kIx,	,
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
Bagdád	Bagdád	k1gInSc1	Bagdád
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
Bejrút	Bejrút	k1gInSc1	Bejrút
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc1	Libanon
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
Damašek	Damašek	k1gInSc1	Damašek
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
Džidda	Džidda	k1gFnSc1	Džidda
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Chartúm	Chartúm	k1gInSc1	Chartúm
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
Lisabon	Lisabon	k1gInSc1	Lisabon
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc4	Španělsko
Moskva	Moskva	k1gFnSc1	Moskva
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
Rabat	rabat	k1gInSc1	rabat
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Sã	Sã	k1gFnSc1	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Tunis	Tunis	k1gInSc1	Tunis
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnPc1	D.C.
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
TAUER	TAUER	kA	TAUER
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
WILLIAMS	WILLIAMS	kA	WILLIAMS
<g/>
,	,	kIx,	,
Caroline	Carolin	k1gInSc5	Carolin
<g/>
,	,	kIx,	,
Islamic	Islamic	k1gMnSc1	Islamic
Monuments	Monumentsa	k1gFnPc2	Monumentsa
in	in	k?	in
Cairo	Cairo	k1gNnSc1	Cairo
<g/>
,	,	kIx,	,
New	New	k1gFnPc1	New
York-Cairo	York-Cairo	k1gNnSc1	York-Cairo
<g/>
,	,	kIx,	,
American	American	k1gInSc1	American
University	universita	k1gFnSc2	universita
in	in	k?	in
Cairo	Cairo	k1gNnSc1	Cairo
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
RODENBECK	RODENBECK	kA	RODENBECK
<g/>
,	,	kIx,	,
Max	max	kA	max
<g/>
,	,	kIx,	,
Cairo	Cairo	k1gNnSc1	Cairo
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
City	City	k1gFnSc2	City
Victorious	Victorious	k1gInSc1	Victorious
<g/>
,	,	kIx,	,
Cairo	Cairo	k1gNnSc1	Cairo
<g/>
,	,	kIx,	,
American	American	k1gInSc1	American
University	universita	k1gFnSc2	universita
in	in	k?	in
Cairo	Cairo	k1gNnSc1	Cairo
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
BAREŠ	Bareš	k1gMnSc1	Bareš
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
GOMBÁR	GOMBÁR	kA	GOMBÁR
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
;	;	kIx,	;
VESELÝ	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
LUKAVEC	LUKAVEC	kA	LUKAVEC
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
ji	on	k3xPp3gFnSc4	on
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
,	,	kIx,	,
neviděl	vidět	k5eNaImAgMnS	vidět
svět	svět	k1gInSc4	svět
<g/>
:	:	kIx,	:
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Od	od	k7c2	od
českého	český	k2eAgNnSc2d1	české
Tokia	Tokio	k1gNnSc2	Tokio
k	k	k7c3	k
exotické	exotický	k2eAgFnSc3d1	exotická
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Malvern	Malvern	k1gNnSc1	Malvern
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
317	[number]	k4	317
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87580	[number]	k4	87580
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
a	a	k8xC	a
v	v	k7c6	v
Heliopoli	Heliopole	k1gFnSc6	Heliopole
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Káhira	Káhira	k1gFnSc1	Káhira
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
