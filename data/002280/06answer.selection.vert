<s>
Počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kontinentální	kontinentální	k2eAgFnSc3d1	kontinentální
poloze	poloha	k1gFnSc3	poloha
města	město	k1gNnSc2	město
na	na	k7c4	na
rozhraní	rozhraní	k1gNnSc4	rozhraní
tropického	tropický	k2eAgInSc2d1	tropický
a	a	k8xC	a
subtropického	subtropický	k2eAgInSc2d1	subtropický
pásu	pás	k1gInSc2	pás
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
blízkostí	blízkost	k1gFnSc7	blízkost
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
