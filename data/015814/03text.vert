<s>
William	William	k6eAd1
S.	S.	kA
Burroughs	Burroughsa	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Man	Man	k1gMnSc1
Within	Within	k1gMnSc1
</s>
<s>
William	William	k6eAd1
S.	S.	kA
Burroughs	Burroughsa	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Man	Man	k1gMnSc1
Within	Within	k1gMnSc1
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
William	William	k6eAd1
S.	S.	kA
Burroughs	Burroughsa	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Man	mana	k1gFnPc2
Within	Within	k1gMnSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Yony	Yona	k1gFnSc2
Leyser	Leysra	k1gFnPc2
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Laurie	Laurie	k1gFnSc1
Andersonová	Andersonová	k1gFnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
2010	#num#	k4
Distribuce	distribuce	k1gFnSc1
</s>
<s>
Oscilloscope	Oscilloscop	k1gMnSc5
William	William	k1gInSc1
S.	S.	kA
Burroughs	Burroughs	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Man	Man	k1gMnSc1
Within	Within	k1gMnSc1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
William	William	k6eAd1
S.	S.	kA
Burroughs	Burroughsa	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Man	Man	k1gMnSc1
Within	Within	k1gMnSc1
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc4d1
dokumentární	dokumentární	k2eAgInSc4d1
film	film	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
natočil	natočit	k5eAaBmAgMnS
režisér	režisér	k1gMnSc1
Yony	Yona	k1gFnSc2
Leyser	Leyser	k1gInSc1
o	o	k7c6
spisovateli	spisovatel	k1gMnSc6
Williamu	William	k1gInSc2
S.	S.	kA
Burroughsovi	Burroughs	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystupují	vystupovat	k5eAaImIp3nP
v	v	k7c6
něm	on	k3xPp3gInSc6
například	například	k6eAd1
Patti	Patť	k1gFnSc2
Smith	Smith	k1gInSc1
<g/>
,	,	kIx,
Iggy	Igga	k1gFnPc1
Pop	pop	k1gMnSc1
<g/>
,	,	kIx,
Gus	Gus	k1gMnSc1
Van	vana	k1gFnPc2
Sant	Sant	k1gMnSc1
a	a	k8xC
Genesis	Genesis	k1gFnSc1
P-Orridge	P-Orridge	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hudbě	hudba	k1gFnSc6
k	k	k7c3
filmu	film	k1gInSc3
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
členové	člen	k1gMnPc1
skupiny	skupina	k1gFnSc2
Sonic	Sonic	k1gMnSc1
Youth	Youth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypravěčem	vypravěč	k1gMnSc7
je	být	k5eAaImIp3nS
Peter	Peter	k1gMnSc1
Weller	Weller	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
hrál	hrát	k5eAaImAgMnS
ve	v	k7c6
filmu	film	k1gInSc6
Nahý	nahý	k2eAgInSc4d1
oběd	oběd	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
adaptací	adaptace	k1gFnSc7
Burroughsova	Burroughsův	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snímek	snímek	k1gInSc1
získal	získat	k5eAaPmAgInS
několik	několik	k4yIc4
ocenění	ocenění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritik	kritik	k1gMnSc1
Stephen	Stephna	k1gFnPc2
Holden	Holdna	k1gFnPc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
recenzi	recenze	k1gFnSc6
pro	pro	k7c4
The	The	k1gFnSc4
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
tomto	tento	k3xDgInSc6
dokumentu	dokument	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
by	by	kYmCp3nS
si	se	k3xPyFc3
zasloužil	zasloužit	k5eAaPmAgMnS
trvat	trvat	k5eAaImF
o	o	k7c4
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
déle	dlouho	k6eAd2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
ani	ani	k8xC
jediné	jediný	k2eAgNnSc1d1
zbytečné	zbytečný	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
či	či	k8xC
záběr	záběr	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HOLDEN	HOLDEN	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naked	Naked	k1gInSc1
Lusts	Lusts	k1gInSc4
and	and	k?
Natural	Natural	k?
Painkillers	Painkillers	k1gInSc1
<g/>
:	:	kIx,
Portrait	Portrait	k1gInSc1
of	of	k?
a	a	k8xC
Literary	Literara	k1gFnSc2
Outlaw	Outlaw	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
2010-11-16	2010-11-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
William	William	k6eAd1
S.	S.	kA
Burroughs	Burroughsa	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Man	Man	k1gMnSc1
Within	Within	k1gMnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
William	William	k6eAd1
S.	S.	kA
Burroughs	Burroughsa	k1gFnPc2
<g/>
:	:	kIx,
A	a	k9
Man	Man	k1gMnSc1
Within	Within	k1gMnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dílo	dílo	k1gNnSc1
Williama	William	k1gMnSc2
Sewarda	Sewarda	k1gFnSc1
Burroughse	Burroughse	k1gFnSc1
Romány	Román	k1gMnPc4
a	a	k8xC
novely	novela	k1gFnPc4
</s>
<s>
A	a	k8xC
hroši	hroch	k1gMnPc1
se	se	k3xPyFc4
uvařili	uvařit	k5eAaPmAgMnP
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
nádržích	nádrž	k1gFnPc6
•	•	k?
Feťák	Feťák	k?
•	•	k?
Teplouš	teplouš	k1gMnSc1
•	•	k?
Nahý	nahý	k2eAgInSc4d1
oběd	oběd	k1gInSc4
•	•	k?
Hebká	hebký	k2eAgFnSc1d1
mašinka	mašinka	k1gFnSc1
•	•	k?
Lístek	lístek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
explodoval	explodovat	k5eAaBmAgInS
•	•	k?
Dead	Dead	k1gInSc1
Fingers	Fingers	k1gInSc1
Talk	Talk	k1gInSc1
•	•	k?
Nova	nova	k1gFnSc1
Express	express	k1gInSc1
•	•	k?
The	The	k1gMnSc2
Last	Last	k2eAgInSc4d1
Words	Words	k1gInSc4
of	of	k?
Dutch	Dutch	k1gInSc1
Schultz	Schultz	k1gInSc1
•	•	k?
Divocí	divoký	k2eAgMnPc1d1
hoši	hoch	k1gMnPc1
•	•	k?
Port	port	k1gInSc1
of	of	k?
Saints	Saints	k1gInSc1
•	•	k?
Blade	Blad	k1gInSc5
Runner	Runnero	k1gNnPc2
(	(	kIx(
<g/>
a	a	k8xC
movie	movie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Města	město	k1gNnSc2
rudých	rudý	k2eAgFnPc2d1
nocí	noc	k1gFnPc2
•	•	k?
Ohyzdný	ohyzdný	k2eAgMnSc1d1
duch	duch	k1gMnSc1
•	•	k?
Místo	místo	k7c2
slepých	slepý	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
•	•	k?
Západní	západní	k2eAgFnSc2d1
země	zem	k1gFnSc2
•	•	k?
Škola	škola	k1gFnSc1
mého	můj	k3xOp1gInSc2
života	život	k1gInSc2
<g/>
:	:	kIx,
Kniha	kniha	k1gFnSc1
snů	sen	k1gInPc2
•	•	k?
Kočka	kočka	k1gFnSc1
v	v	k7c4
nás	my	k3xPp1nPc4
Další	další	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Interzóna	Interzóna	k1gFnSc1
•	•	k?
Hubitel	hubitel	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Ah	ah	k0
Pook	Pook	k1gInSc4
Is	Is	k1gFnSc3
Here	Here	k1gInSc1
•	•	k?
Tornado	Tornada	k1gFnSc5
Alley	Alle	k2eAgFnPc1d1
•	•	k?
The	The	k1gFnPc1
Burroughs	Burroughsa	k1gFnPc2
File	Fil	k1gFnSc2
•	•	k?
The	The	k1gMnSc1
Third	Third	k1gMnSc1
Mind	Mind	k1gMnSc1
•	•	k?
Dopisy	dopis	k1gInPc1
o	o	k7c4
Yage	Yag	k1gMnSc4
•	•	k?
Poslední	poslední	k2eAgNnSc1d1
slova	slovo	k1gNnPc1
Nahrávky	nahrávka	k1gFnSc2
</s>
<s>
Dead	Dead	k6eAd1
City	cit	k1gInPc1
Radio	radio	k1gNnSc1
•	•	k?
Spare	Spar	k1gMnSc5
Ass	Ass	k1gMnSc5
Annie	Annius	k1gMnSc5
and	and	k?
Other	Other	k1gMnSc1
Tales	Tales	k1gMnSc1
Filmy	film	k1gInPc7
</s>
<s>
Burroughs	Burroughs	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Movie	Movie	k1gFnSc2
•	•	k?
Words	Words	k1gInSc1
of	of	k?
Advice	Advice	k1gFnSc1
<g/>
:	:	kIx,
William	William	k1gInSc1
S.	S.	kA
Burroughs	Burroughs	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Road	Road	k1gInSc1
•	•	k?
William	William	k1gInSc1
S.	S.	kA
Burroughs	Burroughs	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
Man	Man	k1gMnSc1
Within	Within	k1gMnSc1
Rodina	rodina	k1gFnSc1
</s>
<s>
Joan	Joan	k1gMnSc1
Vollmer	Vollmer	k1gMnSc1
(	(	kIx(
<g/>
manželka	manželka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
William	William	k1gInSc1
S.	S.	kA
Burroughs	Burroughs	k1gInSc1
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
syn	syn	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
