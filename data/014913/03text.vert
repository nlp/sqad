<s>
Capoeira	Capoeira	k6eAd1
</s>
<s>
Bojovník	bojovník	k1gMnSc1
capoeiry	capoeira	k1gFnSc2
</s>
<s>
Capoeira	Capoeira	k6eAd1
je	být	k5eAaImIp3nS
brazilský	brazilský	k2eAgInSc1d1
taneční	taneční	k2eAgInSc1d1
styl	styl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
komplexní	komplexní	k2eAgNnSc4d1
kulturní	kulturní	k2eAgNnSc4d1
fenomén	fenomén	k1gNnSc4
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgNnSc4d1
svébytné	svébytný	k2eAgNnSc4d1
bojové	bojový	k2eAgNnSc4d1
umění	umění	k1gNnSc4
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgInSc4d1
pohybovým	pohybový	k2eAgInSc7d1
stylem	styl	k1gInSc7
tanec	tanec	k1gInSc1
s	s	k7c7
akrobatickými	akrobatický	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
,	,	kIx,
doprovázené	doprovázený	k2eAgInPc1d1
původní	původní	k2eAgFnSc7d1
hudbou	hudba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Kolébkou	kolébka	k1gFnSc7
capoeiry	capoeira	k1gFnSc2
je	být	k5eAaImIp3nS
brazilský	brazilský	k2eAgInSc1d1
stát	stát	k1gInSc1
Bahia	Bahium	k1gNnSc2
<g/>
,	,	kIx,
tradičně	tradičně	k6eAd1
zemědělská	zemědělský	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
historicky	historicky	k6eAd1
negativně	negativně	k6eAd1
poznamenána	poznamenat	k5eAaPmNgFnS
masovým	masový	k2eAgInSc7d1
dovozem	dovoz	k1gInSc7
afrických	africký	k2eAgMnPc2d1
otroků	otrok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Capoeira	Capoeir	k1gInSc2
je	být	k5eAaImIp3nS
tradičné	tradičný	k2eAgNnSc1d1
považována	považován	k2eAgNnPc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejvýznamnějších	významný	k2eAgInPc2d3
fenoménů	fenomén	k1gInPc2
původně	původně	k6eAd1
africké	africký	k2eAgFnPc1d1
-	-	kIx~
především	především	k6eAd1
pak	pak	k6eAd1
angolské	angolský	k2eAgFnPc1d1
-	-	kIx~
kulturní	kulturní	k2eAgFnPc1d1
tradice	tradice	k1gFnPc1
a	a	k8xC
výsledek	výsledek	k1gInSc1
následné	následný	k2eAgFnSc2d1
syntézy	syntéza	k1gFnSc2
africké	africký	k2eAgFnSc2d1
<g/>
,	,	kIx,
původní	původní	k2eAgFnSc2d1
brazilské	brazilský	k2eAgFnSc2d1
a	a	k8xC
expandované	expandovaný	k2eAgFnSc2d1
evropské	evropský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
Brazílie	Brazílie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
procesům	proces	k1gInPc3
globalizace	globalizace	k1gFnSc2
zažívá	zažívat	k5eAaImIp3nS
capoeira	capoeira	k6eAd1
mohutnou	mohutný	k2eAgFnSc4d1
expanzi	expanze	k1gFnSc4
takřka	takřka	k6eAd1
do	do	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nabývá	nabývat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
od	od	k7c2
sedmdesátých	sedmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
neobvyklé	obvyklý	k2eNgFnSc2d1
popularity	popularita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgInPc1d1
kontexty	kontext	k1gInPc1
</s>
<s>
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
obranu	obrana	k1gFnSc4
beze	beze	k7c2
zbraní	zbraň	k1gFnPc2
proti	proti	k7c3
zlodějům	zloděj	k1gMnPc3
slepic	slepice	k1gFnPc2
<g/>
,	,	kIx,
násilníkům	násilník	k1gMnPc3
a	a	k8xC
lupičům	lupič	k1gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
proti	proti	k7c3
zvůli	zvůle	k1gFnSc3
bílých	bílý	k2eAgMnPc2d1
otrokářů	otrokář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1707	#num#	k4
byla	být	k5eAaImAgFnS
capoeira	capoeira	k6eAd1
zakázaná	zakázaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otroci	otrok	k1gMnPc1
ji	on	k3xPp3gFnSc4
dále	daleko	k6eAd2
praktikovali	praktikovat	k5eAaImAgMnP
jako	jako	k9
„	„	k?
<g/>
bojový	bojový	k2eAgInSc1d1
tanec	tanec	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
směs	směs	k1gFnSc1
elegantních	elegantní	k2eAgInPc2d1
pohybů	pohyb	k1gInPc2
a	a	k8xC
akrobacie	akrobacie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Traduje	tradovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
moderní	moderní	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
capoeiry	capoeira	k1gFnSc2
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
v	v	k7c6
breakdance	breakdanka	k1gFnSc6
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Bronxu	Bronx	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Capoeiristé	Capoeirista	k1gMnPc1
však	však	k9
nepovažují	považovat	k5eNaImIp3nP
tuto	tento	k3xDgFnSc4
domněnku	domněnka	k1gFnSc4
za	za	k7c4
pravdivou	pravdivý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1808	#num#	k4
brazilská	brazilský	k2eAgFnSc1d1
vládnoucí	vládnoucí	k2eAgFnSc1d1
třída	třída	k1gFnSc1
tvrdě	tvrdě	k6eAd1
potlačovala	potlačovat	k5eAaImAgFnS
toto	tento	k3xDgNnSc4
bojové	bojový	k2eAgNnSc1d1
umění	umění	k1gNnSc1
a	a	k8xC
proti	proti	k7c3
capoeiristům	capoeirista	k1gMnPc3
postupovala	postupovat	k5eAaImAgFnS
neobyčejně	obyčejně	k6eNd1
brutálně	brutálně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1825	#num#	k4
byli	být	k5eAaImAgMnP
barevní	barevný	k2eAgMnPc1d1
otroci	otrok	k1gMnPc1
v	v	k7c6
takovém	takový	k3xDgInSc6
postavení	postavení	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
pískání	pískání	k1gNnSc1
či	či	k8xC
postávání	postávání	k1gNnSc1
na	na	k7c6
rohu	roh	k1gInSc6
ulice	ulice	k1gFnSc2
<g/>
,	,	kIx,
anebo	anebo	k8xC
vyluzování	vyluzování	k1gNnSc1
jakéhokoli	jakýkoli	k3yIgInSc2
zvuku	zvuk	k1gInSc2
bylo	být	k5eAaImAgNnS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
projev	projev	k1gInSc4
zakázané	zakázaný	k2eAgFnSc2d1
capoeiry	capoeira	k1gFnSc2
a	a	k8xC
velmi	velmi	k6eAd1
tvrdě	tvrdě	k6eAd1
trestáno	trestán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Navzdory	navzdory	k7c3
těmto	tento	k3xDgFnPc3
pohnutým	pohnutý	k2eAgFnPc3d1
historickým	historický	k2eAgFnPc3d1
událostem	událost	k1gFnPc3
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
capoeira	capoeir	k1gInSc2
opět	opět	k6eAd1
stala	stát	k5eAaPmAgFnS
<g/>
,	,	kIx,
díky	díky	k7c3
státu	stát	k1gInSc3
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
uznávaným	uznávaný	k2eAgNnSc7d1
bojovým	bojový	k2eAgNnSc7d1
uměním	umění	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgNnP
se	se	k3xPyFc4
populární	populární	k2eAgNnPc1d1
<g/>
,	,	kIx,
dostala	dostat	k5eAaPmAgNnP
pevná	pevný	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
po	po	k7c6
fotbalu	fotbal	k1gInSc6
druhým	druhý	k4xOgInSc7
nejoblíbenějším	oblíbený	k2eAgInSc7d3
brazilským	brazilský	k2eAgInSc7d1
sportem	sport	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
novodobé	novodobý	k2eAgMnPc4d1
průkopníky	průkopník	k1gMnPc4
Capoery	Capoera	k1gFnSc2
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Lateef	Lateef	k1gMnSc1
Crowder	Crowder	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
výrazy	výraz	k1gInPc1
ze	z	k7c2
slovníku	slovník	k1gInSc2
</s>
<s>
aluno	aluno	k6eAd1
–	–	k?
žák	žák	k1gMnSc1
capoeiry	capoeira	k1gFnSc2
</s>
<s>
ataque	ataque	k1gInSc1
–	–	k?
útočný	útočný	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
</s>
<s>
batizado	batizada	k1gFnSc5
–	–	k?
ceremonie	ceremonie	k1gFnSc1
vstupu	vstup	k1gInSc2
do	do	k7c2
capoeiry	capoeira	k1gFnSc2
</s>
<s>
troca	troca	k1gFnSc1
de	de	k?
cordas	cordas	k1gInSc4
--	--	k?
slavnostní	slavnostní	k2eAgNnSc4d1
udělování	udělování	k1gNnSc4
technických	technický	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
(	(	kIx(
<g/>
pásků	pásek	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
cocorinha	cocorinha	k1gFnSc1
–	–	k?
podřep	podřep	k1gInSc1
</s>
<s>
ginga	ginga	k1gFnSc1
–	–	k?
základní	základní	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
capoeiry	capoeira	k1gFnSc2
<g/>
,	,	kIx,
práce	práce	k1gFnSc2
nohou	noha	k1gFnPc2
</s>
<s>
golpe	golpat	k5eAaPmIp3nS
–	–	k?
úder	úder	k1gInSc1
<g/>
,	,	kIx,
útok	útok	k1gInSc1
</s>
<s>
jogar	jogar	k1gMnSc1
–	–	k?
bojovník	bojovník	k1gMnSc1
</s>
<s>
jogo	jogo	k6eAd1
–	–	k?
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
boj	boj	k1gInSc4
dvou	dva	k4xCgMnPc2
capoeiristů	capoeirista	k1gMnPc2
v	v	k7c6
rodě	rod	k1gInSc6
</s>
<s>
roda	roda	k1gFnSc1
–	–	k?
místo	místo	k1gNnSc1
pro	pro	k7c4
capoeiru	capoeira	k1gFnSc4
<g/>
,	,	kIx,
kruh	kruh	k1gInSc4
z	z	k7c2
capoeiristů	capoeirista	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
středu	střed	k1gInSc6
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
jogo	jogo	k6eAd1
</s>
<s>
berimbau	berimbau	k6eAd1
–	–	k?
základní	základní	k2eAgInPc4d1
hudební	hudební	k2eAgInPc4d1
nástroj	nástroj	k1gInSc4
v	v	k7c6
Capoeiře	Capoeira	k1gFnSc6
</s>
<s>
atabaque	atabaque	k1gInSc1
–	–	k?
velký	velký	k2eAgInSc1d1
buben	buben	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
rodě	rod	k1gInSc6
pouze	pouze	k6eAd1
jedenau	jedenau	k5eAaPmIp1nS
grande	grand	k1gMnSc5
-	-	kIx~
velká	velký	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
</s>
<s>
agogo	agogo	k1gMnSc1
–	–	k?
další	další	k2eAgInSc4d1
nástroj	nástroj	k1gInSc4
složený	složený	k2eAgInSc4d1
ze	z	k7c2
dvou	dva	k4xCgInPc2
kokosových	kokosový	k2eAgInPc2d1
ořechů	ořech	k1gInPc2
</s>
<s>
pandeiro	pandeiro	k1gNnSc1
-	-	kIx~
tamburína	tamburína	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
Asociace	asociace	k1gFnSc1
Capoeiry	Capoeira	k1gFnSc2
-	-	kIx~
asociace	asociace	k1gFnSc1
s	s	k7c7
největším	veliký	k2eAgNnSc7d3
zastoupením	zastoupení	k1gNnSc7
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Abadá	Abadý	k2eAgFnSc1d1
Capoeira	Capoeira	k1gFnSc1
–	–	k?
Brazilská	brazilský	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
na	na	k7c4
podporu	podpora	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
umění	umění	k1gNnSc2
capoeira	capoeir	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Capoeira	Capoeir	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Capoeira	Capoeira	k6eAd1
Appresado	Appresada	k1gFnSc5
-	-	kIx~
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
-	-	kIx~
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Axé	Axé	k?
Capoeira	Capoeira	k1gFnSc1
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Vem	Vem	k?
Camará	Camarý	k2eAgFnSc1d1
Capoeira	Capoeira	k1gFnSc1
ČR	ČR	kA
</s>
<s>
Capoeira	Capoeira	k1gMnSc1
Candeias	Candeias	k1gMnSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Capoeira	Capoeira	k1gMnSc1
Candeias	Candeias	k1gMnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Bojové	bojový	k2eAgNnSc1d1
umění	umění	k1gNnSc1
Capoeira	Capoeir	k1gInSc2
</s>
<s>
http://www.capoeiramandela.cz	http://www.capoeiramandela.cz	k1gInSc1
<g/>
%	%	kIx~
<g/>
5	#num#	k4
<g/>
B	B	kA
<g/>
%	%	kIx~
<g/>
5	#num#	k4
<g/>
D	D	kA
</s>
<s>
http://www.gingamundo.cz/	http://www.gingamundo.cz/	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4196664-8	4196664-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85019991	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85019991	#num#	k4
</s>
