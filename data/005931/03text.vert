<s>
Fantasy	fantas	k1gInPc1	fantas
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
podžánrů	podžánr	k1gInPc2	podžánr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
nemají	mít	k5eNaImIp3nP	mít
přímé	přímý	k2eAgInPc4d1	přímý
protějšky	protějšek	k1gInPc4	protějšek
v	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
nebo	nebo	k8xC	nebo
folklóru	folklór	k1gInSc6	folklór
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgMnPc6	který
je	on	k3xPp3gInPc4	on
fantasy	fantas	k1gInPc4	fantas
založeno	založen	k2eAgNnSc1d1	založeno
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
literárními	literární	k2eAgInPc7d1	literární
žánry	žánr	k1gInPc7	žánr
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
science	science	k1gFnSc1	science
fiction	fiction	k1gInSc4	fiction
a	a	k8xC	a
horor	horor	k1gInSc4	horor
<g/>
.	.	kIx.	.
</s>
<s>
Málo	málo	k4c1	málo
děl	dělo	k1gNnPc2	dělo
lze	lze	k6eAd1	lze
řadit	řadit	k5eAaImF	řadit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
podžánru	podžánr	k1gInSc2	podžánr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
historické	historický	k2eAgFnSc2d1	historická
fantasy	fantas	k1gInPc4	fantas
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc4	žánr
jasnější	jasný	k2eAgFnSc1d2	jasnější
reference	reference	k1gFnSc1	reference
na	na	k7c4	na
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Philip	Philip	k1gMnSc1	Philip
Pullman	Pullman	k1gMnSc1	Pullman
-	-	kIx~	-
Zlatý	zlatý	k2eAgInSc1d1	zlatý
kompas	kompas	k1gInSc1	kompas
Susanna	Susanen	k2eAgFnSc1d1	Susanna
Clarková	Clarková	k1gFnSc1	Clarková
-	-	kIx~	-
Jonathan	Jonathan	k1gMnSc1	Jonathan
Strange	Strang	k1gFnSc2	Strang
&	&	k?	&
pan	pan	k1gMnSc1	pan
Norrell	Norrell	k1gMnSc1	Norrell
Tento	tento	k3xDgInSc4	tento
žánr	žánr	k1gInSc4	žánr
tvoří	tvořit	k5eAaImIp3nP	tvořit
příběhy	příběh	k1gInPc4	příběh
v	v	k7c6	v
světě	svět	k1gInSc6	svět
velmi	velmi	k6eAd1	velmi
blízkém	blízký	k2eAgNnSc6d1	blízké
naší	náš	k3xOp1gFnSc6	náš
realitě	realita	k1gFnSc6	realita
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
příběh	příběh	k1gInSc1	příběh
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
existenci	existence	k1gFnSc4	existence
magie	magie	k1gFnSc2	magie
nebo	nebo	k8xC	nebo
magických	magický	k2eAgFnPc2d1	magická
bytostí	bytost	k1gFnPc2	bytost
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
upíři	upír	k1gMnPc1	upír
či	či	k8xC	či
nesmrtelní	nesmrtelný	k1gMnPc1	nesmrtelný
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skutečnosti	skutečnost	k1gFnPc1	skutečnost
bývají	bývat	k5eAaImIp3nP	bývat
běžným	běžný	k2eAgMnPc3d1	běžný
obyvatelům	obyvatel	k1gMnPc3	obyvatel
daného	daný	k2eAgInSc2d1	daný
světa	svět	k1gInSc2	svět
skryté	skrytý	k2eAgInPc1d1	skrytý
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
alternativní	alternativní	k2eAgFnSc6d1	alternativní
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
žánr	žánr	k1gInSc1	žánr
se	se	k3xPyFc4	se
často	často	k6eAd1	často
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c4	na
městské	městský	k2eAgNnSc4d1	Městské
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nazýván	nazýván	k2eAgInSc1d1	nazýván
urban	urban	k1gInSc1	urban
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
-	-	kIx~	-
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
Peter	Peter	k1gMnSc1	Peter
V.	V.	kA	V.
Brett	Brett	k1gMnSc1	Brett
-	-	kIx~	-
Tetovaný	tetovaný	k2eAgMnSc1d1	tetovaný
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
–	–	k?	–
Nikdykde	Nikdykd	k1gInSc5	Nikdykd
China	China	k1gFnSc1	China
Miéville	Miéville	k1gNnSc1	Miéville
–	–	k?	–
Král	Král	k1gMnSc1	Král
Krysa	krysa	k1gFnSc1	krysa
Tony	Tony	k1gFnSc2	Tony
DiTerlizzi	DiTerlizze	k1gFnSc4	DiTerlizze
<g/>
,	,	kIx,	,
Holly	Holl	k1gInPc4	Holl
Blacková	Blacková	k1gFnSc1	Blacková
–	–	k?	–
Kronika	kronika	k1gFnSc1	kronika
rodu	rod	k1gInSc2	rod
Spiderwicků	Spiderwicek	k1gMnPc2	Spiderwicek
Jim	on	k3xPp3gMnPc3	on
Butcher	Butchra	k1gFnPc2	Butchra
–	–	k?	–
Dresden	Dresdna	k1gFnPc2	Dresdna
Files	Files	k1gMnSc1	Files
Sergej	Sergej	k1gMnSc1	Sergej
Lukjaněnko	Lukjaněnka	k1gFnSc5	Lukjaněnka
–	–	k?	–
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
Denní	denní	k2eAgFnSc1d1	denní
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
,	,	kIx,	,
...	...	k?	...
Rick	Rick	k1gMnSc1	Rick
Riordan	Riordan	k1gMnSc1	Riordan
–	–	k?	–
Percy	Perca	k1gFnSc2	Perca
Jackson	Jackson	k1gMnSc1	Jackson
a	a	k8xC	a
Olympané	Olympan	k1gMnPc1	Olympan
Tímto	tento	k3xDgInSc7	tento
termínem	termín	k1gInSc7	termín
bývá	bývat	k5eAaImIp3nS	bývat
označováno	označovat	k5eAaImNgNnS	označovat
fantasy	fantas	k1gInPc4	fantas
současného	současný	k2eAgInSc2d1	současný
světa	svět	k1gInSc2	svět
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
tématech	téma	k1gNnPc6	téma
<g/>
,	,	kIx,	,
symbolech	symbol	k1gInPc6	symbol
a	a	k8xC	a
archetypech	archetyp	k1gInPc6	archetyp
z	z	k7c2	z
mýtů	mýtus	k1gInPc2	mýtus
a	a	k8xC	a
folklóru	folklór	k1gInSc2	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Lint	Lint	k1gInSc1	Lint
-	-	kIx~	-
Zelený	zelený	k2eAgInSc1d1	zelený
plášť	plášť	k1gInSc1	plášť
Robert	Robert	k1gMnSc1	Robert
Holdstock	Holdstock	k1gMnSc1	Holdstock
-	-	kIx~	-
Les	les	k1gInSc1	les
mytág	mytág	k1gMnSc1	mytág
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
-	-	kIx~	-
Američtí	americký	k2eAgMnPc1d1	americký
bohové	bůh	k1gMnPc1	bůh
Steampunk	Steampunka	k1gFnPc2	Steampunka
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
historické	historický	k2eAgInPc4d1	historický
fantasy	fantas	k1gInPc1	fantas
nebo	nebo	k8xC	nebo
alternativní	alternativní	k2eAgFnSc1d1	alternativní
historie	historie	k1gFnSc1	historie
zasazená	zasazený	k2eAgFnSc1d1	zasazená
často	často	k6eAd1	často
do	do	k7c2	do
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
doby	doba	k1gFnSc2	doba
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
silných	silný	k2eAgInPc6d1	silný
prvcích	prvek	k1gInPc6	prvek
parní	parní	k2eAgFnSc2d1	parní
technologie	technologie	k1gFnSc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoká	k1gFnSc1	vysoká
fantasy	fantas	k1gInPc1	fantas
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
též	též	k9	též
hrdinská	hrdinský	k2eAgFnSc1d1	hrdinská
či	či	k8xC	či
epická	epický	k2eAgFnSc1d1	epická
fantasy	fantas	k1gInPc1	fantas
tvoří	tvořit	k5eAaImIp3nP	tvořit
epické	epický	k2eAgInPc1d1	epický
příběhy	příběh	k1gInPc1	příběh
většinou	většina	k1gFnSc7	většina
o	o	k7c6	o
boji	boj	k1gInSc6	boj
dobra	dobro	k1gNnSc2	dobro
a	a	k8xC	a
zla	zlo	k1gNnSc2	zlo
ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
či	či	k8xC	či
paralelní	paralelní	k2eAgNnSc1d1	paralelní
tomu	ten	k3xDgNnSc3	ten
našemu	náš	k3xOp1gNnSc3	náš
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
morální	morální	k2eAgNnSc4d1	morální
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgInSc2	který
většinou	většinou	k6eAd1	většinou
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
žádné	žádný	k3yNgNnSc4	žádný
relativizování	relativizování	k1gNnSc4	relativizování
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vytvářen	vytvářen	k2eAgInSc1d1	vytvářen
propracovaný	propracovaný	k2eAgInSc1d1	propracovaný
"	"	kIx"	"
<g/>
druhotný	druhotný	k2eAgInSc1d1	druhotný
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
včetně	včetně	k7c2	včetně
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
rodokmenů	rodokmen	k1gInPc2	rodokmen
a	a	k8xC	a
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkino	k1gNnPc2	Tolkino
–	–	k?	–
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
David	David	k1gMnSc1	David
Eddings	Eddingsa	k1gFnPc2	Eddingsa
–	–	k?	–
Pěšec	pěšec	k1gMnSc1	pěšec
proroctví	proroctví	k1gNnSc2	proroctví
Robert	Robert	k1gMnSc1	Robert
Jordan	Jordan	k1gMnSc1	Jordan
–	–	k?	–
Kolo	kolo	k1gNnSc1	kolo
času	čas	k1gInSc2	čas
E.	E.	kA	E.
R.	R.	kA	R.
Eddison	Eddison	k1gMnSc1	Eddison
–	–	k?	–
Červ	červ	k1gMnSc1	červ
Ouroboros	Ouroborosa	k1gFnPc2	Ouroborosa
Steven	Stevna	k1gFnPc2	Stevna
Erikson	Erikson	k1gInSc1	Erikson
-	-	kIx~	-
Malazská	Malazský	k2eAgFnSc1d1	Malazská
kniha	kniha	k1gFnSc1	kniha
padlých	padlý	k1gMnPc2	padlý
Historická	historický	k2eAgFnSc1d1	historická
fantasy	fantas	k1gInPc4	fantas
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
menšími	malý	k2eAgInPc7d2	menší
prvky	prvek	k1gInPc7	prvek
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
fantastických	fantastický	k2eAgInPc2d1	fantastický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
příběh	příběh	k1gInSc1	příběh
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
povětšinou	povětšinou	k6eAd1	povětšinou
blízko	blízko	k6eAd1	blízko
skutečnému	skutečný	k2eAgInSc3d1	skutečný
středověku	středověk	k1gInSc3	středověk
a	a	k8xC	a
starověku	starověk	k1gInSc3	starověk
a	a	k8xC	a
lze	lze	k6eAd1	lze
zde	zde	k6eAd1	zde
nalézt	nalézt	k5eAaPmF	nalézt
reference	reference	k1gFnPc4	reference
na	na	k7c4	na
skutečné	skutečný	k2eAgFnPc4d1	skutečná
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
a	a	k8xC	a
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
motivy	motiv	k1gInPc1	motiv
převzaté	převzatý	k2eAgInPc1d1	převzatý
z	z	k7c2	z
artušovské	artušovský	k2eAgFnSc2d1	artušovská
romance	romance	k1gFnSc2	romance
(	(	kIx(	(
<g/>
T.	T.	kA	T.
H.	H.	kA	H.
White	Whit	k1gInSc5	Whit
–	–	k?	–
Někdejší	někdejší	k2eAgMnSc1d1	někdejší
a	a	k8xC	a
budoucí	budoucí	k2eAgMnSc1d1	budoucí
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
George	Georg	k1gMnSc2	Georg
R.	R.	kA	R.
R.	R.	kA	R.
Martin	Martin	k1gMnSc1	Martin
-	-	kIx~	-
Píseň	píseň	k1gFnSc1	píseň
ledu	led	k1gInSc2	led
a	a	k8xC	a
ohně	oheň	k1gInSc2	oheň
Guy	Guy	k1gMnSc1	Guy
Gavriel	Gavriel	k1gMnSc1	Gavriel
Kay	Kay	k1gMnSc1	Kay
-	-	kIx~	-
Tigana	Tigana	k1gFnSc1	Tigana
John	John	k1gMnSc1	John
Flanagan	Flanagan	k1gMnSc1	Flanagan
-	-	kIx~	-
Hraničářův	hraničářův	k2eAgMnSc1d1	hraničářův
učeň	učeň	k1gMnSc1	učeň
Fantasy	fantas	k1gInPc4	fantas
meče	meč	k1gInSc2	meč
a	a	k8xC	a
magie	magie	k1gFnSc1	magie
založená	založený	k2eAgFnSc1d1	založená
Robertem	Robert	k1gMnSc7	Robert
E.	E.	kA	E.
Howardem	Howard	k1gMnSc7	Howard
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
Barbarem	barbar	k1gMnSc7	barbar
Conanem	Conan	k1gMnSc7	Conan
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vysoké	vysoká	k1gFnSc2	vysoká
fantasy	fantas	k1gInPc1	fantas
soustředí	soustředit	k5eAaPmIp3nP	soustředit
především	především	k9	především
na	na	k7c4	na
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
akci	akce	k1gFnSc4	akce
a	a	k8xC	a
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
méně	málo	k6eAd2	málo
na	na	k7c4	na
epiku	epika	k1gFnSc4	epika
a	a	k8xC	a
morální	morální	k2eAgNnSc4d1	morální
pozadí	pozadí	k1gNnSc4	pozadí
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
E.	E.	kA	E.
Howard	Howard	k1gMnSc1	Howard
–	–	k?	–
Barbar	barbar	k1gMnSc1	barbar
Conan	Conan	k1gMnSc1	Conan
Michael	Michael	k1gMnSc1	Michael
Moorcock	Moorcock	k1gMnSc1	Moorcock
–	–	k?	–
Elrik	Elrik	k1gMnSc1	Elrik
Fritz	Fritz	k1gMnSc1	Fritz
Leiber	Leiber	k1gMnSc1	Leiber
-	-	kIx~	-
Fahfrd	Fahfrd	k1gMnSc1	Fahfrd
a	a	k8xC	a
Šedý	Šedý	k1gMnSc1	Šedý
Myšilov	myšilov	k1gMnSc1	myšilov
</s>
<s>
Komická	komický	k2eAgFnSc1d1	komická
fantasy	fantas	k1gInPc4	fantas
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
komikou	komika	k1gFnSc7	komika
a	a	k8xC	a
satirou	satira	k1gFnSc7	satira
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
zde	zde	k6eAd1	zde
nutně	nutně	k6eAd1	nutně
dodržována	dodržován	k2eAgNnPc1d1	dodržováno
žánrová	žánrový	k2eAgNnPc1d1	žánrové
omezení	omezení	k1gNnPc1	omezení
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
blízké	blízký	k2eAgNnSc1d1	blízké
vysokému	vysoký	k2eAgInSc3d1	vysoký
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Terry	Terra	k1gFnPc1	Terra
Pratchett	Pratchett	k1gInSc1	Pratchett
–	–	k?	–
série	série	k1gFnSc2	série
Zeměplocha	Zeměploch	k1gMnSc4	Zeměploch
John	John	k1gMnSc1	John
Morressy	Morressa	k1gFnSc2	Morressa
–	–	k?	–
série	série	k1gFnSc2	série
Kedrigern	Kedrigerna	k1gFnPc2	Kedrigerna
Piers	Piersa	k1gFnPc2	Piersa
Anthony	Anthona	k1gFnSc2	Anthona
–	–	k?	–
Chameleon	chameleon	k1gMnSc1	chameleon
a	a	k8xC	a
zaklínadlo	zaklínadlo	k1gNnSc1	zaklínadlo
Robert	Robert	k1gMnSc1	Robert
Asprin	Asprin	k1gInSc1	Asprin
–	–	k?	–
Další	další	k2eAgInSc1d1	další
prima	prima	k2eAgInSc1d1	prima
mýtus	mýtus	k1gInSc1	mýtus
Dark	Dark	k1gInSc4	Dark
fantasy	fantas	k1gInPc7	fantas
tvoří	tvořit	k5eAaImIp3nS	tvořit
většina	většina	k1gFnSc1	většina
hororů	horor	k1gInPc2	horor
s	s	k7c7	s
nadpřirozenými	nadpřirozený	k2eAgInPc7d1	nadpřirozený
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
bytostmi	bytost	k1gFnPc7	bytost
jako	jako	k8xS	jako
upíři	upír	k1gMnPc1	upír
<g/>
,	,	kIx,	,
vlkodlaci	vlkodlak	k1gMnPc1	vlkodlak
a	a	k8xC	a
mumie	mumie	k1gFnPc1	mumie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
fantasy	fantas	k1gInPc1	fantas
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
krutých	krutý	k2eAgInPc2d1	krutý
a	a	k8xC	a
démonických	démonický	k2eAgInPc2d1	démonický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
P.	P.	kA	P.
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
–	–	k?	–
V	v	k7c6	v
horách	hora	k1gFnPc6	hora
šílenství	šílenství	k1gNnSc2	šílenství
Anne	Ann	k1gMnSc2	Ann
Rice	Ric	k1gMnSc2	Ric
-	-	kIx~	-
Upíří	upíří	k2eAgFnSc2d1	upíří
kroniky	kronika	k1gFnSc2	kronika
William	William	k1gInSc1	William
King	King	k1gMnSc1	King
-	-	kIx~	-
série	série	k1gFnSc1	série
Terrarchové	Terrarchová	k1gFnSc2	Terrarchová
Petra	Petra	k1gFnSc1	Petra
Neomillnerová	Neomillnerová	k1gFnSc1	Neomillnerová
-	-	kIx~	-
Nakažení	nakažení	k1gNnPc4	nakažení
Pohádková	pohádkový	k2eAgNnPc4d1	pohádkové
fantasy	fantas	k1gInPc4	fantas
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
především	především	k6eAd1	především
folklórem	folklór	k1gInSc7	folklór
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
jeho	jeho	k3xOp3gInPc4	jeho
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
žánrem	žánr	k1gInSc7	žánr
autorské	autorský	k2eAgFnSc2d1	autorská
pohádky	pohádka	k1gFnSc2	pohádka
a	a	k8xC	a
převyprávěním	převyprávění	k1gNnSc7	převyprávění
klasických	klasický	k2eAgFnPc2d1	klasická
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
ignorovány	ignorován	k2eAgInPc1d1	ignorován
standardy	standard	k1gInPc1	standard
"	"	kIx"	"
<g/>
budování	budování	k1gNnSc1	budování
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
přestože	přestože	k8xS	přestože
samotné	samotný	k2eAgNnSc1d1	samotné
prostředí	prostředí	k1gNnSc1	prostředí
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
blízké	blízký	k2eAgInPc4d1	blízký
vysoké	vysoký	k2eAgInPc4d1	vysoký
či	či	k8xC	či
historické	historický	k2eAgInPc4d1	historický
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
dětské	dětský	k2eAgMnPc4d1	dětský
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
hrdinové	hrdina	k1gMnPc1	hrdina
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
dětského	dětský	k2eAgInSc2d1	dětský
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Věku	věk	k1gInSc2	věk
čtenáře	čtenář	k1gMnPc4	čtenář
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
přizpůsbena	přizpůsben	k2eAgFnSc1d1	přizpůsben
míra	míra	k1gFnSc1	míra
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nP	chybět
sexuální	sexuální	k2eAgFnPc1d1	sexuální
scény	scéna	k1gFnPc1	scéna
aj.	aj.	kA	aj.
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnSc1	Lewis
-	-	kIx~	-
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
Gerald	Gerald	k1gMnSc1	Gerald
Durrell	Durrell	k1gMnSc1	Durrell
-	-	kIx~	-
Mluvící	mluvící	k2eAgInSc1d1	mluvící
balík	balík	k1gInSc1	balík
Lewis	Lewis	k1gFnSc1	Lewis
Carroll	Carroll	k1gMnSc1	Carroll
-	-	kIx~	-
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
L.	L.	kA	L.
<g/>
F.	F.	kA	F.
Baum	Baum	k1gMnSc1	Baum
-	-	kIx~	-
Čaroděj	čaroděj	k1gMnSc1	čaroděj
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
Oz	Oz	k1gMnPc2	Oz
Michael	Michael	k1gMnSc1	Michael
Ende	End	k1gInSc2	End
–	–	k?	–
Nekonečný	konečný	k2eNgInSc1d1	nekonečný
příběh	příběh	k1gInSc1	příběh
Karel	Karel	k1gMnSc1	Karel
Antonín	Antonín	k1gMnSc1	Antonín
–	–	k?	–
Menšíčkova	Menšíčkův	k2eAgFnSc1d1	Menšíčkův
evančická	evančická	k1gFnSc1	evančická
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Science	Science	k1gFnSc2	Science
fantasy	fantas	k1gInPc4	fantas
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
sci-fi	scii	k1gNnPc2	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obsaženy	obsažen	k2eAgInPc4d1	obsažen
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xC	jako
kosmické	kosmický	k2eAgInPc4d1	kosmický
lety	let	k1gInPc4	let
a	a	k8xC	a
laserové	laserový	k2eAgFnPc4d1	laserová
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
magie	magie	k1gFnSc1	magie
či	či	k8xC	či
jiná	jiný	k2eAgFnSc1d1	jiná
forma	forma	k1gFnSc1	forma
nadpřirozena	nadpřirozeno	k1gNnSc2	nadpřirozeno
a	a	k8xC	a
postavy	postava	k1gFnPc1	postava
spadají	spadat	k5eAaImIp3nP	spadat
pod	pod	k7c4	pod
žánrové	žánrový	k2eAgInPc4d1	žánrový
archetypy	archetyp	k1gInPc4	archetyp
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc4	příběh
podobné	podobný	k2eAgInPc4d1	podobný
žánru	žánr	k1gInSc3	žánr
"	"	kIx"	"
<g/>
meč	meč	k1gInSc1	meč
a	a	k8xC	a
magie	magie	k1gFnSc1	magie
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgFnPc1d1	nazývána
"	"	kIx"	"
<g/>
meč	meč	k1gInSc1	meč
a	a	k8xC	a
planeta	planeta	k1gFnSc1	planeta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
širším	široký	k2eAgInSc7d2	širší
žánrem	žánr	k1gInSc7	žánr
planetární	planetární	k2eAgFnSc2d1	planetární
romance	romance	k1gFnSc2	romance
typickým	typický	k2eAgInSc7d1	typický
sériemi	série	k1gFnPc7	série
Barsoom	Barsoom	k1gInSc1	Barsoom
a	a	k8xC	a
Amtor	Amtor	k1gInSc1	Amtor
od	od	k7c2	od
Edgara	Edgar	k1gMnSc2	Edgar
Rice	Ric	k1gMnSc2	Ric
Burroughse	Burroughs	k1gMnSc2	Burroughs
<g/>
.	.	kIx.	.
</s>
<s>
Marion	Marion	k1gInSc1	Marion
Zimmer	Zimmra	k1gFnPc2	Zimmra
Bradley	Bradlea	k1gFnSc2	Bradlea
–	–	k?	–
série	série	k1gFnSc2	série
Darkover	Darkover	k1gMnSc1	Darkover
Jack	Jack	k1gMnSc1	Jack
Vance	Vance	k1gMnSc1	Vance
–	–	k?	–
Umírající	umírající	k1gMnSc1	umírající
země	zem	k1gFnSc2	zem
Anne	Ann	k1gFnSc2	Ann
McCaffrey	McCaffrea	k1gFnSc2	McCaffrea
–	–	k?	–
série	série	k1gFnSc1	série
Perm	perm	k1gInSc1	perm
série	série	k1gFnSc2	série
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
války	válka	k1gFnSc2	válka
série	série	k1gFnSc2	série
Warhammer	Warhammer	k1gInSc1	Warhammer
40.000	[number]	k4	40.000
</s>
