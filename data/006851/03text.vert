<s>
Mono	mono	k2eAgFnSc1d1	mono
Lake	Lake	k1gFnSc1	Lake
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
také	také	k9	také
zkrátka	zkrátka	k6eAd1	zkrátka
Mono	mono	k2eAgFnSc1d1	mono
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slané	slaný	k2eAgNnSc1d1	slané
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Mono	mono	k6eAd1	mono
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
2	[number]	k4	2
030	[number]	k4	030
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hloubku	hloubka	k1gFnSc4	hloubka
17	[number]	k4	17
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
48	[number]	k4	48
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
3,66	[number]	k4	3,66
krychlových	krychlový	k2eAgInPc2d1	krychlový
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1944	[number]	k4	1944
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
GFAJ-1	GFAJ-1	k4	GFAJ-1
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mono	mono	k6eAd1	mono
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
