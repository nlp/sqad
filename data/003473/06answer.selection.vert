<s>
Jádrem	jádro	k1gNnSc7	jádro
této	tento	k3xDgFnSc2	tento
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
dvanáct	dvanáct	k4xCc1	dvanáct
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
13.	[number]	k4	13.
balada	balada	k1gFnSc1	balada
–	–	k?	–
Lilie	lilie	k1gFnSc2	lilie
<g/>
)	)	kIx)	)
básní	báseň	k1gFnPc2	báseň
oddílu	oddíl	k1gInSc2	oddíl
Pověsti	pověst	k1gFnSc2	pověst
národní	národní	k2eAgFnSc2d1	národní
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
předchází	předcházet	k5eAaImIp3nS	předcházet
úvodní	úvodní	k2eAgFnSc4d1	úvodní
báseň	báseň	k1gFnSc4	báseň
Kytice	kytice	k1gFnSc2	kytice
<g/>
.	.	kIx.	.
</s>
