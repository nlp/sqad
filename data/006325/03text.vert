<s>
Matthew	Matthew	k?	Matthew
Abram	Abram	k1gInSc1	Abram
Groening	Groening	k1gInSc1	Groening
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1954	[number]	k4	1954
v	v	k7c6	v
Portlandu	Portland	k1gInSc6	Portland
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
Oregon	Oregon	k1gNnSc4	Oregon
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
karikaturista	karikaturista	k1gMnSc1	karikaturista
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
animovaných	animovaný	k2eAgInPc2d1	animovaný
seriálů	seriál	k1gInPc2	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
a	a	k8xC	a
Futurama	Futurama	k?	Futurama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc7	jejich
producentem	producent	k1gMnSc7	producent
a	a	k8xC	a
scenáristou	scenárista	k1gMnSc7	scenárista
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
animátora	animátor	k1gMnSc2	animátor
a	a	k8xC	a
producenta	producent	k1gMnSc2	producent
Homera	Homer	k1gMnSc2	Homer
a	a	k8xC	a
učitelky	učitelka	k1gFnSc2	učitelka
Margaret	Margareta	k1gFnPc2	Margareta
Groeningových	Groeningový	k2eAgFnPc2d1	Groeningový
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Mark	Mark	k1gMnSc1	Mark
a	a	k8xC	a
sestry	sestra	k1gFnPc1	sestra
jsou	být	k5eAaImIp3nP	být
Patty	Patt	k1gInPc4	Patt
<g/>
,	,	kIx,	,
Lisa	Lisa	k1gFnSc1	Lisa
a	a	k8xC	a
Maggie	Maggie	k1gFnSc1	Maggie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Craig	Craig	k1gMnSc1	Craig
Bartlett	Bartlett	k1gMnSc1	Bartlett
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
Lincolnově	Lincolnův	k2eAgFnSc6d1	Lincolnova
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Portlandu	Portland	k1gInSc6	Portland
byl	být	k5eAaImAgInS	být
studentským	studentský	k2eAgMnSc7d1	studentský
presidentem	president	k1gMnSc7	president
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
kreslil	kreslit	k5eAaImAgMnS	kreslit
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
Evergreen	evergreen	k1gInSc1	evergreen
State	status	k1gInSc5	status
College	College	k1gNnPc7	College
v	v	k7c6	v
Olympii	Olympia	k1gFnSc6	Olympia
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
studia	studio	k1gNnSc2	studio
redaktorem	redaktor	k1gMnSc7	redaktor
školních	školní	k2eAgFnPc2d1	školní
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Deborah	Deborah	k1gMnSc1	Deborah
Caplanovou	Caplanová	k1gFnSc4	Caplanová
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Homer	Homer	k1gMnSc1	Homer
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
syn	syn	k1gMnSc1	syn
Abe	Abe	k1gMnSc1	Abe
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
skončilo	skončit	k5eAaPmAgNnS	skončit
rozvodem	rozvod	k1gInSc7	rozvod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
kreslení	kreslení	k1gNnSc6	kreslení
komiksu	komiks	k1gInSc2	komiks
Life	Lif	k1gFnSc2	Lif
in	in	k?	in
Hell	Hell	k1gInSc1	Hell
(	(	kIx(	(
<g/>
Život	život	k1gInSc1	život
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosud	dosud	k6eAd1	dosud
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
novinách	novina	k1gFnPc6	novina
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
sérii	série	k1gFnSc6	série
knih	kniha	k1gFnPc2	kniha
School	School	k1gInSc1	School
is	is	k?	is
Hell	Hell	k1gInSc1	Hell
(	(	kIx(	(
<g/>
Škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
peklo	peklo	k1gNnSc4	peklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Love	lov	k1gInSc5	lov
is	is	k?	is
Hell	Hell	k1gInSc1	Hell
(	(	kIx(	(
<g/>
Láska	láska	k1gFnSc1	láska
je	být	k5eAaImIp3nS	být
peklo	peklo	k1gNnSc4	peklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Work	Work	k1gMnSc1	Work
is	is	k?	is
Hell	Hell	k1gMnSc1	Hell
(	(	kIx(	(
<g/>
Práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
peklo	peklo	k1gNnSc4	peklo
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gMnSc1	The
Big	Big	k1gMnSc1	Big
Book	Book	k1gMnSc1	Book
of	of	k?	of
Hell	Hell	k1gMnSc1	Hell
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
pekla	peklo	k1gNnSc2	peklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Life	Life	k1gFnSc1	Life
in	in	k?	in
Hell	Hell	k1gInSc1	Hell
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgInS	získat
pozornost	pozornost	k1gFnSc4	pozornost
hollywoodského	hollywoodský	k2eAgMnSc2d1	hollywoodský
producenta	producent	k1gMnSc2	producent
a	a	k8xC	a
zakladatele	zakladatel	k1gMnSc2	zakladatel
Gracie	Gracie	k1gFnSc1	Gracie
Films	Films	k1gInSc1	Films
Jamese	Jamese	k1gFnSc1	Jamese
Brookse	Brooks	k1gMnSc2	Brooks
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sháněl	shánět	k5eAaImAgMnS	shánět
vatu	vata	k1gFnSc4	vata
do	do	k7c2	do
pořadu	pořad	k1gInSc2	pořad
The	The	k1gMnSc2	The
Tracey	Tracea	k1gMnSc2	Tracea
Ullman	Ullman	k1gMnSc1	Ullman
Show	show	k1gNnSc7	show
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
stanicí	stanice	k1gFnSc7	stanice
FOX	fox	k1gInSc1	fox
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
sérii	série	k1gFnSc4	série
krátkých	krátký	k2eAgInPc2d1	krátký
skečů	skeč	k1gInPc2	skeč
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
formát	formát	k1gInSc1	formát
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
odvysílán	odvysílán	k2eAgInSc1d1	odvysílán
dvacetiminutový	dvacetiminutový	k2eAgInSc1d1	dvacetiminutový
díl	díl	k1gInSc1	díl
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Vánoce	Vánoce	k1gFnPc1	Vánoce
u	u	k7c2	u
Simpsonových	Simpsonová	k1gFnPc2	Simpsonová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nový	nový	k2eAgInSc1d1	nový
seriál	seriál	k1gInSc1	seriál
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
vynesla	vynést	k5eAaPmAgFnS	vynést
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
autorem	autor	k1gMnSc7	autor
surrealistického	surrealistický	k2eAgInSc2d1	surrealistický
komiksu	komiks	k1gInSc2	komiks
Akbar	Akbar	k1gMnSc1	Akbar
and	and	k?	and
Jeff	Jeff	k1gMnSc1	Jeff
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
postavičky	postavička	k1gFnPc1	postavička
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
v	v	k7c6	v
Life	Life	k1gFnSc6	Life
in	in	k?	in
Hell	Hella	k1gFnPc2	Hella
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
další	další	k2eAgInSc1d1	další
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
animovaný	animovaný	k2eAgInSc1d1	animovaný
seriál	seriál	k1gInSc1	seriál
Futurama	Futurama	k?	Futurama
se	se	k3xPyFc4	se
na	na	k7c6	na
obrazovkách	obrazovka	k1gFnPc6	obrazovka
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Groening	Groening	k1gInSc1	Groening
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
města	město	k1gNnSc2	město
Groningen	Groningen	k1gInSc1	Groningen
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
černým	černý	k2eAgInSc7d1	černý
humorem	humor	k1gInSc7	humor
anglického	anglický	k2eAgMnSc2d1	anglický
karikaturisty	karikaturista	k1gMnSc2	karikaturista
Ronalda	Ronald	k1gMnSc2	Ronald
Searla	Searl	k1gMnSc2	Searl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
rokenrolové	rokenrolový	k2eAgFnSc2d1	rokenrolová
skupiny	skupina	k1gFnSc2	skupina
Rock	rock	k1gInSc1	rock
Bottom	Bottom	k1gInSc1	Bottom
Remainders	Remaindersa	k1gFnPc2	Remaindersa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
spisovateli	spisovatel	k1gMnSc3	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
levák	levák	k1gMnSc1	levák
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeho	jeho	k3xOp3gFnPc4	jeho
postavičky	postavička	k1gFnPc4	postavička
Bart	Bart	k2eAgInSc1d1	Bart
Simpson	Simpson	k1gInSc1	Simpson
<g/>
,	,	kIx,	,
Marge	Marge	k1gFnSc1	Marge
Simpsonová	Simpsonová	k1gFnSc1	Simpsonová
<g/>
,	,	kIx,	,
Ned	Ned	k1gFnSc1	Ned
Flanders	Flanders	k1gInSc1	Flanders
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Burns	Burns	k1gInSc1	Burns
a	a	k8xC	a
Vočko	Vočko	k1gNnSc1	Vočko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
obdržel	obdržet	k5eAaPmAgMnS	obdržet
svoji	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Matt	Matt	k2eAgInSc4d1	Matt
Groening	Groening	k1gInSc4	Groening
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Matt	Matt	k1gInSc1	Matt
Groening	Groening	k1gInSc1	Groening
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Matt	Matt	k2eAgInSc1d1	Matt
Groening	Groening	k1gInSc1	Groening
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Matt	Matt	k2eAgInSc1d1	Matt
Groening	Groening	k1gInSc1	Groening
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Rock	rock	k1gInSc1	rock
Bottom	Bottom	k1gInSc1	Bottom
Remainders	Remainders	k1gInSc1	Remainders
Matt	Matt	k2eAgInSc1d1	Matt
Groening	Groening	k1gInSc4	Groening
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
