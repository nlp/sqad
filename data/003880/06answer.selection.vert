<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Čung-chua	Čunghua	k1gFnSc1	Čung-chua
žen-min	ženin	k1gInSc1	žen-min
kung-che-kuo	kungheuo	k6eAd1	kung-che-kuo
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Zhō	Zhō	k1gFnSc2	Zhō
rénmín	rénmín	k1gInSc1	rénmín
gò	gò	k?	gò
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
中	中	k?	中
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
ČLR	ČLR	kA	ČLR
či	či	k8xC	či
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
země	země	k1gFnSc1	země
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
ekonomikou	ekonomika	k1gFnSc7	ekonomika
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
dává	dávat	k5eAaImIp3nS	dávat
vzrůst	vzrůst	k1gInSc1	vzrůst
nové	nový	k2eAgFnSc2d1	nová
supervelmoci	supervelmoc	k1gFnSc2	supervelmoc
<g/>
.	.	kIx.	.
</s>
