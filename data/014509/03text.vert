<s>
Přátelé	přítel	k1gMnPc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
pojem	pojem	k1gInSc1
přítel	přítel	k1gMnSc1
nebo	nebo	k8xC
přátelství	přátelství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Přátelé	přítel	k1gMnPc1
Logo	logo	k1gNnSc4
seriáluZákladní	seriáluZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Friends	Friends	k6eAd1
Žánr	žánr	k1gInSc1
</s>
<s>
sitcom	sitcom	k1gInSc1
Námět	námět	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
CraneMarta	CraneMart	k1gMnSc2
Kauffman	Kauffman	k1gMnSc1
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Jennifer	Jennifer	k1gInSc1
AnistonCourteney	AnistonCourtenea	k1gMnSc2
CoxLisa	CoxLis	k1gMnSc2
KudrowMatt	KudrowMatt	k1gMnSc1
LeBlancMatthew	LeBlancMatthew	k1gMnSc1
PerryDavid	PerryDavida	k1gFnPc2
Schwimmer	Schwimmer	k1gMnSc1
Skladatel	skladatel	k1gMnSc1
znělky	znělka	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Crane	Cran	k1gInSc5
Marta	Marta	k1gFnSc1
Kauffman	Kauffman	k1gMnSc1
Michael	Michael	k1gMnSc1
Skloff	Skloff	k1gMnSc1
Allee	Allee	k1gFnSc4
Willis	Willis	k1gFnSc2
Phil	Phil	k1gInSc1
Sō	Sō	k1gInSc7
Danny	Danna	k1gFnSc2
Wilde	Wild	k1gInSc5
Úvodní	úvodní	k2eAgFnSc4d1
znělka	znělka	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Be	Be	k1gMnSc5
There	Ther	k1gMnSc5
for	forum	k1gNnPc2
You	You	k1gMnSc7
<g/>
“	“	k?
od	od	k7c2
The	The	k1gFnSc2
Rembrandts	Rembrandts	k1gInSc4
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
USA	USA	kA
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Počet	počet	k1gInSc1
řad	řad	k1gInSc1
</s>
<s>
10	#num#	k4
Počet	počet	k1gInSc4
dílů	díl	k1gInPc2
</s>
<s>
236	#num#	k4
(	(	kIx(
<g/>
seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
<g/>
)	)	kIx)
Obvyklá	obvyklý	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
;	;	kIx,
výjimečně	výjimečně	k6eAd1
23-29	23-29	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
nerozdělená	rozdělený	k2eNgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
45	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
Produkce	produkce	k1gFnSc1
a	a	k8xC
štáb	štáb	k1gInSc1
Producent	producent	k1gMnSc1
</s>
<s>
David	David	k1gMnSc1
Crane	Cran	k1gInSc5
a	a	k8xC
Marta	Mars	k1gMnSc2
Kauffman	Kauffman	k1gMnSc1
Produkčníspolečnost	Produkčníspolečnost	k1gFnSc1
</s>
<s>
Warner	Warner	k1gMnSc1
Bros	Brosa	k1gFnPc2
Distributor	distributor	k1gMnSc1
</s>
<s>
Warner	Warner	k1gMnSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domestic	Domestice	k1gFnPc2
Television	Television	k1gInSc4
DistributionNetflixFandangoNow	DistributionNetflixFandangoNow	k1gFnSc4
Premiérové	premiérový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
Stanice	stanice	k1gFnSc1
</s>
<s>
NBC	NBC	kA
Formát	formát	k1gInSc1
obrazu	obraz	k1gInSc2
</s>
<s>
4	#num#	k4
:	:	kIx,
3	#num#	k4
/	/	kIx~
16	#num#	k4
:	:	kIx,
<g/>
9	#num#	k4
Vysíláno	vysílán	k2eAgNnSc1d1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1994	#num#	k4
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
Posloupnost	posloupnost	k1gFnSc1
Související	související	k2eAgFnSc2d1
</s>
<s>
Joey	Joea	k1gFnPc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgNnSc1d1
webové	webový	k2eAgNnSc1d1
stránkyPřátelé	stránkyPřátelý	k2eAgNnSc1d1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
,	,	kIx,
IMDb	IMDb	k1gMnSc1
<g/>
,	,	kIx,
TV	TV	kA
<g/>
.	.	kIx.
<g/>
comNěkterá	comNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přátelé	přítel	k1gMnPc1
(	(	kIx(
<g/>
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
originále	originál	k1gInSc6
Friends	Friends	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
sitcom	sitcom	k1gInSc1
o	o	k7c6
skupině	skupina	k1gFnSc6
přátel	přítel	k1gMnPc2
žijících	žijící	k2eAgFnPc2d1
v	v	k7c4
Greenwich	Greenwich	k1gInSc4
Village	Villag	k1gFnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
epizoda	epizoda	k1gFnSc1
byla	být	k5eAaImAgFnS
odvysílána	odvysílat	k5eAaPmNgFnS
na	na	k7c6
stanici	stanice	k1gFnSc6
NBC	NBC	kA
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1994	#num#	k4
<g/>
,	,	kIx,
poslední	poslední	k2eAgMnSc1d1
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seriál	seriál	k1gInSc1
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
Davidem	David	k1gMnSc7
Cranem	Cran	k1gMnSc7
a	a	k8xC
Martou	Marta	k1gFnSc7
Kauffmanovou	Kauffmanův	k2eAgFnSc7d1
v	v	k7c6
produkci	produkce	k1gFnSc6
Kevina	Kevin	k1gMnSc2
S.	S.	kA
Brighta	Bright	k1gMnSc2
<g/>
,	,	kIx,
Marty	Marta	k1gFnSc2
Kauffmanové	Kauffmanová	k1gFnSc2
a	a	k8xC
Davida	David	k1gMnSc2
Cranea	Craneus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
vysílán	vysílat	k5eAaImNgMnS
ve	v	k7c4
více	hodně	k6eAd2
než	než	k8xS
sto	sto	k4xCgNnSc4
zemích	zem	k1gFnPc6
a	a	k8xC
stále	stále	k6eAd1
se	se	k3xPyFc4
umisťuje	umisťovat	k5eAaImIp3nS
na	na	k7c6
velmi	velmi	k6eAd1
dobrých	dobrý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
na	na	k7c6
žebříčcích	žebříček	k1gInPc6
sledovanosti	sledovanost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc4d1
epizodu	epizoda	k1gFnSc4
seriálu	seriál	k1gInSc2
sledovalo	sledovat	k5eAaImAgNnS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
na	na	k7c4
51,1	51,1	k4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
vysílání	vysílání	k1gNnSc2
obdržel	obdržet	k5eAaPmAgInS
seriál	seriál	k1gInSc1
7	#num#	k4
cen	cena	k1gFnPc2
Emmy	Emma	k1gFnSc2
<g/>
,	,	kIx,
Zlatý	zlatý	k2eAgInSc1d1
glóbus	glóbus	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
ceny	cena	k1gFnSc2
SAG	SAG	kA
<g/>
,	,	kIx,
dalších	další	k2eAgNnPc2d1
56	#num#	k4
různých	různý	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
a	a	k8xC
152	#num#	k4
nominací	nominace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Úvodní	úvodní	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
„	„	k?
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Be	Be	k1gMnSc5
There	Ther	k1gMnSc5
for	forum	k1gNnPc2
You	You	k1gMnSc2
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
skupiny	skupina	k1gFnSc2
The	The	k1gFnSc2
Rembrandts	Rembrandtsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
sérii	série	k1gFnSc6
se	se	k3xPyFc4
představuje	představovat	k5eAaImIp3nS
šest	šest	k4xCc1
hlavních	hlavní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
<g/>
:	:	kIx,
Rachel	Rachel	k1gInSc1
Greenová	Greenová	k1gFnSc1
<g/>
,	,	kIx,
servírka	servírka	k1gFnSc1
se	s	k7c7
zálibou	záliba	k1gFnSc7
v	v	k7c6
módě	móda	k1gFnSc6
<g/>
;	;	kIx,
Monica	Monic	k2eAgFnSc1d1
Gellerová	Gellerová	k1gFnSc1
<g/>
,	,	kIx,
profesionální	profesionální	k2eAgFnSc1d1
kuchařka	kuchařka	k1gFnSc1
<g/>
;	;	kIx,
její	její	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
Ross	Ross	k1gInSc1
Geller	Geller	k1gInSc1
<g/>
,	,	kIx,
doktor	doktor	k1gMnSc1
paleontologie	paleontologie	k1gFnSc2
<g/>
;	;	kIx,
Phoebe	Phoeb	k1gInSc5
Bufetová	bufetový	k2eAgFnSc1d1
<g/>
,	,	kIx,
masérka	masérka	k1gFnSc1
a	a	k8xC
písničkářka	písničkářka	k1gFnSc1
<g/>
;	;	kIx,
Joey	Joea	k1gFnPc1
Tribbiani	Tribbiaň	k1gFnSc3
<g/>
,	,	kIx,
nepříliš	příliš	k6eNd1
úspěšný	úspěšný	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
Chandler	Chandler	k1gInSc1
Bing	bingo	k1gNnPc2
<g/>
,	,	kIx,
Rossův	Rossův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
z	z	k7c2
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
pracuje	pracovat	k5eAaImIp3nS
pro	pro	k7c4
neznámou	známý	k2eNgFnSc4d1
firmu	firma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
utekla	utéct	k5eAaPmAgFnS
svému	svůj	k3xOyFgMnSc3
nastávajícímu	nastávající	k1gMnSc3
od	od	k7c2
oltáře	oltář	k1gInSc2
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
Rachel	Rachel	k1gMnSc1
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastěhuje	nastěhovat	k5eAaPmIp3nS
se	se	k3xPyFc4
k	k	k7c3
Monice	Monika	k1gFnSc3
<g/>
,	,	kIx,
své	svůj	k3xOyFgFnSc3
kamarádce	kamarádka	k1gFnSc3
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
<g/>
,	,	kIx,
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
začít	začít	k5eAaPmF
nový	nový	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
na	na	k7c6
svých	svůj	k3xOyFgInPc6
bohatých	bohatý	k2eAgInPc6d1
rodičích	rodič	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
začátek	začátek	k1gInSc4
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
podaří	podařit	k5eAaPmIp3nS
získat	získat	k5eAaPmF
místo	místo	k1gNnSc4
servírky	servírka	k1gFnSc2
v	v	k7c6
kavárně	kavárna	k1gFnSc6
Central	Central	k1gFnSc2
Perk	Perk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ross	Ross	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
je	být	k5eAaImIp3nS
do	do	k7c2
Rachel	Rachlo	k1gNnPc2
zamilovaný	zamilovaný	k2eAgMnSc1d1
od	od	k7c2
svých	svůj	k3xOyFgNnPc2
středoškolských	středoškolský	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
snaží	snažit	k5eAaImIp3nP
po	po	k7c4
celou	celý	k2eAgFnSc4d1
sérii	série	k1gFnSc4
sdělit	sdělit	k5eAaPmF
své	svůj	k3xOyFgInPc4
pocity	pocit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
vůbec	vůbec	k9
jednoduché	jednoduchý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
brzdí	brzdit	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
nejistota	nejistota	k1gFnSc1
<g/>
,	,	kIx,
překážkou	překážka	k1gFnSc7
mu	on	k3xPp3gMnSc3
je	být	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
přítel	přítel	k1gMnSc1
Rachel	Rachel	k1gMnSc1
<g/>
,	,	kIx,
vášnivý	vášnivý	k2eAgMnSc1d1
Ital	Ital	k1gMnSc1
Paolo	Paolo	k1gNnSc4
a	a	k8xC
těhotenství	těhotenství	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
lesbické	lesbický	k2eAgFnSc2d1
ex-manželky	ex-manželka	k1gFnSc2
Carol	Carola	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ho	on	k3xPp3gMnSc4
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
do	do	k7c2
obtížné	obtížný	k2eAgFnSc2d1
situace	situace	k1gFnSc2
s	s	k7c7
Carolinou	Carolin	k2eAgFnSc7d1
partnerkou	partnerka	k1gFnSc7
Susan	Susana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
první	první	k4xOgFnSc2
série	série	k1gFnSc2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodí	narodit	k5eAaPmIp3nS
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
pojmenují	pojmenovat	k5eAaPmIp3nP
Ben	Ben	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joey	k1gInPc4
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
trvalý	trvalý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
plnými	plný	k2eAgInPc7d1
doušky	doušek	k1gInPc7
si	se	k3xPyFc3
užívá	užívat	k5eAaImIp3nS
svého	svůj	k3xOyFgInSc2
svobodného	svobodný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoebe	Phoeb	k1gInSc5
má	mít	k5eAaImIp3nS
dosti	dosti	k6eAd1
podivnou	podivný	k2eAgFnSc4d1
a	a	k8xC
výstřední	výstřední	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
důsledkem	důsledek	k1gInSc7
jejího	její	k3xOp3gNnSc2
krutého	krutý	k2eAgNnSc2d1
dětství	dětství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
provázela	provázet	k5eAaImAgFnS
např.	např.	kA
sebevražda	sebevražda	k1gFnSc1
její	její	k3xOp3gFnSc2
nevlastní	vlastní	k2eNgFnSc2d1
matky	matka	k1gFnSc2
a	a	k8xC
následné	následný	k2eAgInPc4d1
roky	rok	k1gInPc4
strávené	strávený	k2eAgInPc4d1
na	na	k7c6
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chandler	Chandler	k1gMnSc1
ukončuje	ukončovat	k5eAaImIp3nS
dlouhodobý	dlouhodobý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
přítelkyní	přítelkyně	k1gFnSc7
Janice	Janice	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
jejich	jejich	k3xOp3gFnPc1
cesty	cesta	k1gFnPc1
se	se	k3xPyFc4
během	během	k7c2
trvání	trvání	k1gNnSc2
seriálu	seriál	k1gInSc2
ještě	ještě	k6eAd1
několikrát	několikrát	k6eAd1
střetnou	střetnout	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
série	série	k1gFnSc2
je	být	k5eAaImIp3nS
Ross	Ross	k1gInSc1
pozván	pozvat	k5eAaPmNgInS
do	do	k7c2
Číny	Čína	k1gFnSc2
kvůli	kvůli	k7c3
paleontologickým	paleontologický	k2eAgFnPc3d1
vykopávkám	vykopávka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chandler	Chandler	k1gMnSc1
se	se	k3xPyFc4
před	před	k7c7
přáteli	přítel	k1gMnPc7
prořekne	prořeknout	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Ross	Ross	k1gInSc1
miluje	milovat	k5eAaImIp3nS
Rachel	Rachel	k1gInSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
si	se	k3xPyFc3
následně	následně	k6eAd1
uvědomí	uvědomit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
city	city	k1gFnSc1
opětuje	opětovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
série	série	k1gFnSc1
končí	končit	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Rachel	Rachel	k1gInSc1
čeká	čekat	k5eAaImIp3nS
na	na	k7c6
letišti	letiště	k1gNnSc6
na	na	k7c4
Rossův	Rossův	k2eAgInSc4d1
návrat	návrat	k1gInSc4
z	z	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
Druhá	druhý	k4xOgFnSc1
série	série	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
více	hodně	k6eAd2
dějových	dějový	k2eAgFnPc2d1
linií	linie	k1gFnPc2
a	a	k8xC
postavy	postava	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
průběhu	průběh	k1gInSc6
dále	daleko	k6eAd2
vyvíjí	vyvíjet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ross	Ross	k1gInSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
z	z	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rachel	Rachel	k1gInSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
čeká	čekat	k5eAaImIp3nS
na	na	k7c6
letišti	letiště	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
k	k	k7c3
němu	on	k3xPp3gNnSc3
cítí	cítit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
Ross	Ross	k1gInSc1
nepřistává	přistávat	k5eNaImIp3nS
sám	sám	k3xTgInSc1
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gInSc7
přichází	přicházet	k5eAaImIp3nS
na	na	k7c4
scénu	scéna	k1gFnSc4
Julie	Julie	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
poznal	poznat	k5eAaPmAgMnS
během	během	k7c2
studia	studio	k1gNnSc2
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Racheliny	Rachelin	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
sdělit	sdělit	k5eAaPmF
Rossovi	Rossův	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
miluje	milovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
přesným	přesný	k2eAgInSc7d1
odrazem	odraz	k1gInSc7
jeho	jeho	k3xOp3gInPc2
pokusů	pokus	k1gInPc2
v	v	k7c6
první	první	k4xOgFnSc6
sérii	série	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ross	Ross	k1gInSc1
se	se	k3xPyFc4
během	během	k7c2
prvních	první	k4xOgFnPc2
epizod	epizoda	k1gFnPc2
této	tento	k3xDgFnSc2
série	série	k1gFnSc2
rozhodne	rozhodnout	k5eAaPmIp3nS
s	s	k7c7
Julie	Julie	k1gFnSc1
rozejít	rozejít	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
cítí	cítit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Rachel	Rachel	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
ta	ten	k3xDgFnSc1
pravá	pravý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jen	jen	k9
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
několik	několik	k4yIc4
dalších	další	k2eAgFnPc2d1
epizod	epizoda	k1gFnPc2
musel	muset	k5eAaImAgMnS
překonávat	překonávat	k5eAaImF
napětí	napětí	k1gNnSc4
a	a	k8xC
zášť	zášť	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
vznikne	vzniknout	k5eAaPmIp3nS
po	po	k7c6
nedorozumění	nedorozumění	k1gNnSc6
se	s	k7c7
seznamem	seznam	k1gInSc7
negativních	negativní	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
Rachel	Rachel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
spolu	spolu	k6eAd1
začnou	začít	k5eAaPmIp3nP
chodit	chodit	k5eAaImF
<g/>
,	,	kIx,
Rachel	Rachel	k1gMnSc1
totiž	totiž	k9
uvidí	uvidět	k5eAaPmIp3nS
dlouho	dlouho	k6eAd1
skrývané	skrývaný	k2eAgNnSc4d1
domácí	domácí	k2eAgNnSc4d1
video	video	k1gNnSc4
z	z	k7c2
noci	noc	k1gFnSc2
jejího	její	k3xOp3gMnSc2
a	a	k8xC
Monicina	Monicin	k2eAgInSc2d1
maturitního	maturitní	k2eAgInSc2d1
plesu	ples	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Ross	Ross	k1gInSc1
chystal	chystat	k5eAaImAgInS
zastoupit	zastoupit	k5eAaPmF
její	její	k3xOp3gFnPc4
plesové	plesový	k2eAgFnPc4d1
garde	garde	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
ji	on	k3xPp3gFnSc4
téměř	téměř	k6eAd1
“	“	k?
<g/>
nechalo	nechat	k5eAaPmAgNnS
na	na	k7c6
holičkách	holička	k1gFnPc6
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monica	Monica	k1gFnSc1
je	být	k5eAaImIp3nS
povýšena	povýšit	k5eAaPmNgFnS
na	na	k7c4
pozici	pozice	k1gFnSc4
šéfkuchařky	šéfkuchařka	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
vzápětí	vzápětí	k6eAd1
je	být	k5eAaImIp3nS
propuštěna	propustit	k5eAaPmNgFnS
pro	pro	k7c4
přijímání	přijímání	k1gNnSc4
“	“	k?
<g/>
dárků	dárek	k1gInPc2
<g/>
”	”	k?
od	od	k7c2
dodavatele	dodavatel	k1gMnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
pravidly	pravidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
peněz	peníze	k1gInPc2
přijme	přijmout	k5eAaPmIp3nS
ponižující	ponižující	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
servírky	servírka	k1gFnSc2
v	v	k7c6
restauraci	restaurace	k1gFnSc6
ve	v	k7c6
stylu	styl	k1gInSc6
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objeví	objevit	k5eAaPmIp3nS
Dr	dr	kA
<g/>
.	.	kIx.
Richard	Richard	k1gMnSc1
Burke	Burk	k1gFnSc2
<g/>
,	,	kIx,
rodinný	rodinný	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
nedávno	nedávno	k6eAd1
rozvedl	rozvést	k5eAaPmAgMnS
a	a	k8xC
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
o	o	k7c4
21	#num#	k4
let	léto	k1gNnPc2
starší	starý	k2eAgMnSc1d2
než	než	k8xS
Monica	Monica	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
spolu	spolu	k6eAd1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
série	série	k1gFnSc2
chodí	chodit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejím	její	k3xOp3gInSc6
konci	konec	k1gInSc6
se	se	k3xPyFc4
rozejdou	rozejít	k5eAaPmIp3nP
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
si	se	k3xPyFc3
Monica	Monica	k1gFnSc1
uvědomí	uvědomit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
nedokázala	dokázat	k5eNaPmAgFnS
sdílet	sdílet	k5eAaImF
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
s	s	k7c7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
s	s	k7c7
ní	on	k3xPp3gFnSc7
nechce	chtít	k5eNaImIp3nS
v	v	k7c6
budoucnosti	budoucnost	k1gFnSc6
založit	založit	k5eAaPmF
rodinu	rodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepříliš	příliš	k6eNd1
úspěšnému	úspěšný	k2eAgInSc3d1
Joeymu	Joeym	k1gInSc3
se	se	k3xPyFc4
začne	začít	k5eAaPmIp3nS
dařit	dařit	k5eAaImF
<g/>
,	,	kIx,
získá	získat	k5eAaPmIp3nS
totiž	totiž	k9
roli	role	k1gFnSc4
neurochirurga	neurochirurg	k1gMnSc2
Drake	Drak	k1gMnSc2
Ramoraye	Ramoray	k1gMnSc2
ve	v	k7c6
fiktivní	fiktivní	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
televizního	televizní	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Dny	dna	k1gFnSc2
našeho	náš	k3xOp1gInSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
Tak	tak	k9
jde	jít	k5eAaImIp3nS
čas	čas	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
brzy	brzy	k6eAd1
o	o	k7c4
ni	on	k3xPp3gFnSc4
přijde	přijít	k5eAaPmIp3nS
<g/>
,	,	kIx,
vlivem	vlivem	k7c2
svého	svůj	k3xOyFgNnSc2
vyjádření	vyjádření	k1gNnSc2
v	v	k7c6
interview	interview	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
repliky	replika	k1gFnPc4
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
píše	psát	k5eAaImIp3nS
sám	sám	k3xTgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scenáristi	Scenáristi	k?
jeho	jeho	k3xOp3gFnSc4
postavu	postava	k1gFnSc4
nechají	nechat	k5eAaPmIp3nP
tragicky	tragicky	k6eAd1
zahynout	zahynout	k5eAaPmF
ve	v	k7c6
výtahu	výtah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následkem	následkem	k7c2
svého	svůj	k3xOyFgInSc2
kariérního	kariérní	k2eAgInSc2d1
propadu	propad	k1gInSc2
již	již	k6eAd1
není	být	k5eNaImIp3nS
schopen	schopen	k2eAgMnSc1d1
platit	platit	k5eAaImF
svůj	svůj	k3xOyFgInSc4
drahý	drahý	k2eAgInSc4d1
byt	byt	k1gInSc4
a	a	k8xC
stěhuje	stěhovat	k5eAaImIp3nS
se	se	k3xPyFc4
zpět	zpět	k6eAd1
k	k	k7c3
Chandlerovi	Chandler	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
zbaví	zbavit	k5eAaPmIp3nS
poněkud	poněkud	k6eAd1
vyšinutého	vyšinutý	k2eAgMnSc2d1
spolubydlícího	spolubydlící	k1gMnSc2
Eddieho	Eddie	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tahle	tenhle	k3xDgFnSc1
zkušenost	zkušenost	k1gFnSc4
prohloubí	prohloubit	k5eAaPmIp3nS
Chandlerovo	Chandlerův	k2eAgNnSc1d1
a	a	k8xC
Joeyho	Joey	k1gMnSc2
přátelství	přátelství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
druhé	druhý	k4xOgFnSc2
série	série	k1gFnSc2
se	se	k3xPyFc4
Chandler	Chandler	k1gMnSc1
skrze	skrze	k?
internet	internet	k1gInSc1
seznámí	seznámit	k5eAaPmIp3nS
s	s	k7c7
velice	velice	k6eAd1
milou	milý	k2eAgFnSc7d1
ženou	žena	k1gFnSc7
<g/>
,	,	kIx,
jen	jen	k9
aby	aby	kYmCp3nS
při	při	k7c6
jejich	jejich	k3xOp3gNnSc6
prvním	první	k4xOgNnSc6
osobním	osobní	k2eAgNnSc6d1
setkání	setkání	k1gNnSc6
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Janice	Janice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Třetí	třetí	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
Ve	v	k7c6
třetí	třetí	k4xOgFnSc6
sérii	série	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
posilnění	posilnění	k1gNnSc3
formátu	formát	k1gInSc2
seriálu	seriál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rachel	Rachel	k1gMnSc1
začne	začít	k5eAaPmIp3nS
pracovat	pracovat	k5eAaImF
u	u	k7c2
Bloomingdale	Bloomingdala	k1gFnSc6
<g/>
'	'	kIx"
<g/>
s	s	k7c7
a	a	k8xC
Ross	Ross	k1gInSc1
žárlí	žárlit	k5eAaImIp3nS
na	na	k7c4
jejího	její	k3xOp3gMnSc4
kolegu	kolega	k1gMnSc4
Marka	Marek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztah	vztah	k1gInSc1
Rosse	Rosse	k1gFnSc2
a	a	k8xC
Rachel	Rachel	k1gInSc1
se	se	k3xPyFc4
rozpadne	rozpadnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Ross	Ross	k1gInSc1
vyspí	vyspat	k5eAaPmIp3nS
s	s	k7c7
holkou	holka	k1gFnSc7
od	od	k7c2
kopírky	kopírka	k1gFnSc2
–	–	k?
Cloe	Clo	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
trvání	trvání	k1gNnSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
rozcházeli	rozcházet	k5eAaImAgMnP
<g/>
“	“	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
běžícím	běžící	k2eAgInSc7d1
gagem	gag	k1gInSc7
během	během	k7c2
následujících	následující	k2eAgFnPc2d1
sérií	série	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
série	série	k1gFnSc2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
spory	spor	k1gInPc1
mezi	mezi	k7c7
hlavními	hlavní	k2eAgMnPc7d1
protagonisty	protagonista	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
usmíření	usmíření	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozchodu	rozchod	k1gInSc6
Rosse	Rosse	k1gFnSc2
a	a	k8xC
Rachel	Rachel	k1gInSc4
se	se	k3xPyFc4
následující	následující	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
nevěnuje	věnovat	k5eNaImIp3nS
jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
Chandlerovi	Chandler	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
těžkou	těžký	k2eAgFnSc4d1
ránu	rána	k1gFnSc4
v	v	k7c6
dětství	dětství	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
dozvěděl	dozvědět	k5eAaPmAgMnS
o	o	k7c6
rozvodu	rozvod	k1gInSc6
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoebe	Phoeb	k1gMnSc5
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
se	se	k3xPyFc4
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
rodinu	rodina	k1gFnSc4
kromě	kromě	k7c2
svého	svůj	k3xOyFgNnSc2
dvojčete	dvojče	k1gNnSc2
Uršuly	Uršula	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
polovičním	poloviční	k2eAgMnSc7d1
bratrem	bratr	k1gMnSc7
Frankem	Frank	k1gMnSc7
(	(	kIx(
<g/>
Giovani	Giovaň	k1gFnSc6
Ribisi	Ribise	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
závěru	závěr	k1gInSc6
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnSc1
pravá	pravý	k2eAgFnSc1d1
matka	matka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ani	ani	k9
nevěděla	vědět	k5eNaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Phoebe	Phoeb	k1gInSc5
existuje	existovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
žije	žít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joeum	k1gNnPc7
se	se	k3xPyFc4
zakouká	zakoukat	k5eAaPmIp3nS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
kolegyně	kolegyně	k1gFnSc2
herečky	herečka	k1gFnSc2
z	z	k7c2
divadla	divadlo	k1gNnSc2
–	–	k?
(	(	kIx(
<g/>
Dina	Dina	k1gMnSc1
Meyer	Meyer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
však	však	k9
k	k	k7c3
němu	on	k3xPp3gInSc3
stejné	stejný	k2eAgFnPc4d1
city	city	k1gFnPc4
nechová	chovat	k5eNaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ani	ani	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
vyspí	vyspat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
změní	změnit	k5eAaPmIp3nS
poté	poté	k6eAd1
co	co	k9
ji	on	k3xPp3gFnSc4
opustí	opustit	k5eAaPmIp3nP
její	její	k3xOp3gMnSc1
přítel	přítel	k1gMnSc1
<g/>
/	/	kIx~
<g/>
režisér	režisér	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
dozví	dozvědět	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
recenze	recenze	k1gFnPc1
jeho	jeho	k3xOp3gFnSc2
hry	hra	k1gFnSc2
ji	on	k3xPp3gFnSc4
označily	označit	k5eAaPmAgInP
za	za	k7c4
propadák	propadák	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnPc1
vztah	vztah	k1gInSc1
však	však	k9
netrvá	trvat	k5eNaImIp3nS
dlouho	dlouho	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
Kate	kat	k1gInSc5
dostane	dostat	k5eAaPmIp3nS
nabídku	nabídka	k1gFnSc4
na	na	k7c4
účinkování	účinkování	k1gNnSc4
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monica	Monic	k1gInSc2
si	se	k3xPyFc3
začne	začít	k5eAaPmIp3nS
s	s	k7c7
milionářem	milionář	k1gMnSc7
Pete	Pete	k1gNnSc2
Beckerem	Becker	k1gMnSc7
(	(	kIx(
<g/>
Jon	Jon	k1gFnSc1
Favreau	Favreaus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
ho	on	k3xPp3gMnSc4
bere	brát	k5eAaImIp3nS
jako	jako	k9
kamaráda	kamarád	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
spolu	spolu	k6eAd1
chodí	chodit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozejdou	rozejít	k5eAaPmIp3nP
se	se	k3xPyFc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
Pete	Pete	k1gInSc1
<g/>
,	,	kIx,
coby	coby	k?
zápasník	zápasník	k1gMnSc1
v	v	k7c6
ringu	ring	k1gInSc6
ve	v	k7c6
volném	volný	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
,	,	kIx,
skončí	skončit	k5eAaPmIp3nS
dvakrát	dvakrát	k6eAd1
hodně	hodně	k6eAd1
zmlácený	zmlácený	k2eAgInSc1d1
a	a	k8xC
nechce	chtít	k5eNaImIp3nS
přestat	přestat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
Během	během	k7c2
natáčení	natáčení	k1gNnSc2
čtvrté	čtvrtá	k1gFnSc2
série	série	k1gFnSc2
otěhotněla	otěhotnět	k5eAaPmAgFnS
herečka	herečka	k1gFnSc1
Lisa	Lisa	k1gFnSc1
Kudrow	Kudrow	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
tvůrci	tvůrce	k1gMnPc1
seriálu	seriál	k1gInSc2
vysvětlili	vysvětlit	k5eAaPmAgMnP
její	její	k3xOp3gNnPc4
těhotenství	těhotenství	k1gNnPc4
<g/>
,	,	kIx,
vymysleli	vymyslet	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Phoebe	Phoeb	k1gInSc5
bude	být	k5eAaImBp3nS
náhradní	náhradní	k2eAgInPc1d1
matkou	matka	k1gFnSc7
pro	pro	k7c4
svého	svůj	k3xOyFgMnSc4
bratra	bratr	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc4
manželku	manželka	k1gFnSc4
(	(	kIx(
<g/>
Debra	Debr	k1gInSc2
Jo	jo	k9
Rupp	Rupp	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ross	Rossa	k1gFnPc2
a	a	k8xC
Rachel	Rachela	k1gFnPc2
se	se	k3xPyFc4
na	na	k7c6
začátku	začátek	k1gInSc6
série	série	k1gFnSc2
usmíří	usmířit	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
se	se	k3xPyFc4
znovu	znovu	k6eAd1
rozejdou	rozejít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
série	série	k1gFnSc2
si	se	k3xPyFc3
Monika	Monika	k1gFnSc1
a	a	k8xC
Rachel	Rachel	k1gInSc1
musí	muset	k5eAaImIp3nS
vyměnit	vyměnit	k5eAaPmF
byt	byt	k1gInSc4
s	s	k7c7
Joeym	Joeym	k1gInSc4
a	a	k8xC
Chandlerem	Chandler	k1gMnSc7
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
prohrají	prohrát	k5eAaPmIp3nP
sázku	sázka	k1gFnSc4
o	o	k7c4
to	ten	k3xDgNnSc4
jak	jak	k8xS,k8xC
moc	moc	k6eAd1
se	se	k3xPyFc4
mezi	mezi	k7c7
sebou	se	k3xPyFc7
znají	znát	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
však	však	k9
chtějí	chtít	k5eAaImIp3nP
byt	byt	k1gInSc4
zpátky	zpátky	k6eAd1
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
Joeyho	Joey	k1gMnSc4
a	a	k8xC
Chandlera	Chandler	k1gMnSc4
uplatí	uplatit	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
jim	on	k3xPp3gMnPc3
dají	dát	k5eAaPmIp3nP
sezónní	sezónní	k2eAgFnSc4d1
permanentku	permanentka	k1gFnSc4
na	na	k7c4
Knicks	Knicks	k1gInSc4
a	a	k8xC
minutu	minuta	k1gFnSc4
se	se	k3xPyFc4
líbají	líbat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
uprostřed	uprostřed	k7c2
série	série	k1gFnSc2
se	se	k3xPyFc4
Ross	Ross	k1gInSc1
seznámí	seznámit	k5eAaPmIp3nS
s	s	k7c7
Angličankou	Angličanka	k1gFnSc7
Emily	Emil	k1gMnPc4
(	(	kIx(
<g/>
Helen	Helena	k1gFnPc2
Baxendale	Baxendala	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c4
závěr	závěr	k1gInSc4
série	série	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
natáčel	natáčet	k5eAaImAgMnS
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
Ross	Ross	k1gInSc4
a	a	k8xC
Emily	Emil	k1gMnPc4
svatbu	svatba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chandler	Chandler	k1gInSc4
a	a	k8xC
Monica	Monica	k1gFnSc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
vyspí	vyspat	k5eAaPmIp3nS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
si	se	k3xPyFc3
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
svatebčanů	svatebčan	k1gMnPc2
splete	splést	k5eAaPmIp3nS
Monicu	Monica	k1gFnSc4
s	s	k7c7
Rossovou	Rossův	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
a	a	k8xC
Monica	Monica	k1gFnSc1
najde	najít	k5eAaPmIp3nS
útěchu	útěcha	k1gFnSc4
v	v	k7c6
náručí	náručí	k1gNnSc6
přítele	přítel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
blížícím	blížící	k2eAgMnSc7d1
se	se	k3xPyFc4
termínem	termín	k1gInSc7
svatby	svatba	k1gFnSc2
propadá	propadat	k5eAaPmIp3nS,k5eAaImIp3nS
Rachel	Rachel	k1gInSc1
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
dál	daleko	k6eAd2
větší	veliký	k2eAgFnSc4d2
depresi	deprese	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
pokusí	pokusit	k5eAaPmIp3nS
dostat	dostat	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
navrhne	navrhnout	k5eAaPmIp3nS
svému	svůj	k3xOyFgMnSc3
příteli	přítel	k1gMnSc3
Joshuovi	Joshu	k1gMnSc3
svatbu	svatba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
vypraví	vypravit	k5eAaPmIp3nS
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Rossovi	Ross	k1gMnSc3
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
miluje	milovat	k5eAaImIp3nS
a	a	k8xC
svatbu	svatba	k1gFnSc4
tak	tak	k6eAd1
překonala	překonat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
mu	on	k3xPp3gMnSc3
však	však	k9
nakonec	nakonec	k6eAd1
neřekne	říct	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechno	všechen	k3xTgNnSc1
se	se	k3xPyFc4
zkomplikuje	zkomplikovat	k5eAaPmIp3nS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
při	při	k7c6
svatebním	svatební	k2eAgInSc6d1
slibu	slib	k1gInSc6
Ross	Rossa	k1gFnPc2
místo	místo	k7c2
Emilina	Emilina	k1gFnSc1
jméno	jméno	k1gNnSc4
řekne	říct	k5eAaPmIp3nS
jméno	jméno	k1gNnSc4
Rachel	Rachela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pátá	pátý	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
páté	pátý	k4xOgFnSc2
série	série	k1gFnSc2
se	se	k3xPyFc4
Monica	Monica	k1gFnSc1
s	s	k7c7
Chandlerem	Chandler	k1gMnSc7
snaží	snažit	k5eAaImIp3nS
udržet	udržet	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
nový	nový	k2eAgInSc4d1
vztah	vztah	k1gInSc4
v	v	k7c6
tajnosti	tajnost	k1gFnSc6
před	před	k7c7
svými	svůj	k3xOyFgMnPc7
přáteli	přítel	k1gMnPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Rossovo	Rossův	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
s	s	k7c7
Emily	Emil	k1gMnPc7
končí	končit	k5eAaImIp3nS
krátce	krátce	k6eAd1
po	po	k7c6
jejich	jejich	k3xOp3gFnSc6
svatbě	svatba	k1gFnSc6
ještě	ještě	k6eAd1
dříve	dříve	k6eAd2
než	než	k8xS
vůbec	vůbec	k9
začne	začít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoebe	Phoeb	k1gInSc5
začne	začít	k5eAaPmIp3nS
chodit	chodit	k5eAaImF
s	s	k7c7
policistou	policista	k1gMnSc7
Garym	Garym	k1gInSc4
(	(	kIx(
<g/>
Michael	Michael	k1gMnSc1
Rapaport	Rapaport	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
se	se	k3xPyFc4
seznámí	seznámit	k5eAaPmIp3nP
díky	díky	k7c3
nálezu	nález	k1gInSc3
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
policejního	policejní	k2eAgInSc2d1
odznaku	odznak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnPc1
vztah	vztah	k1gInSc1
však	však	k9
skončí	skončit	k5eAaPmIp3nS
krátce	krátce	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
Garym	Garym	k1gInSc1
nastěhují	nastěhovat	k5eAaPmIp3nP
do	do	k7c2
nového	nový	k2eAgInSc2d1
bytu	byt	k1gInSc2
a	a	k8xC
ráno	ráno	k6eAd1
po	po	k7c6
probuzení	probuzení	k1gNnSc6
zastřelí	zastřelit	k5eAaPmIp3nP
Gary	Gary	k1gInPc1
zpívajícího	zpívající	k2eAgMnSc2d1
ptáčka	ptáček	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vztah	vztah	k1gInSc1
Monicy	Monica	k1gFnSc2
a	a	k8xC
Chandlera	Chandler	k1gMnSc4
vyjde	vyjít	k5eAaPmIp3nS
najevo	najevo	k6eAd1
a	a	k8xC
na	na	k7c6
výletě	výlet	k1gInSc6
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegas	k1gInSc4
se	se	k3xPyFc4
rozhodnou	rozhodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vezmou	vzít	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Série	série	k1gFnSc1
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
opilý	opilý	k2eAgInSc1d1
Ross	Ross	k1gInSc1
a	a	k8xC
Rachel	Rachel	k1gInSc1
vypotácí	vypotácet	k5eAaPmIp3nS
ze	z	k7c2
svatební	svatební	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
série	série	k1gFnSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
nominaci	nominace	k1gFnSc4
Emmy	Emma	k1gFnSc2
na	na	k7c4
Nejlepší	dobrý	k2eAgInSc4d3
komediální	komediální	k2eAgInSc4d1
seriál	seriál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Šestá	šestý	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
V	v	k7c6
úvodní	úvodní	k2eAgFnSc6d1
epizodě	epizoda	k1gFnSc6
šesté	šestý	k4xOgFnSc2
série	série	k1gFnSc2
vyjde	vyjít	k5eAaPmIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
manželství	manželství	k1gNnSc1
Rosse	Rosse	k1gFnSc2
a	a	k8xC
Rachel	Rachela	k1gFnPc2
je	být	k5eAaImIp3nS
opilecká	opilecký	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
přestože	přestože	k8xS
se	se	k3xPyFc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
Ross	Ross	k1gInSc1
nemá	mít	k5eNaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
po	po	k7c6
neúspěšné	úspěšný	k2eNgFnSc6d1
anulaci	anulace	k1gFnSc6
manželství	manželství	k1gNnSc2
rozvedou	rozvést	k5eAaPmIp3nP
(	(	kIx(
<g/>
Rossův	Rossův	k2eAgInSc4d1
třetí	třetí	k4xOgInSc4
rozvod	rozvod	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monica	Monic	k1gInSc2
a	a	k8xC
Chandler	Chandler	k1gMnSc1
se	se	k3xPyFc4
dohodnou	dohodnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
bydlet	bydlet	k5eAaImF
spolu	spolu	k6eAd1
a	a	k8xC
Rachel	Rachel	k1gInSc1
se	se	k3xPyFc4
odstěhuje	odstěhovat	k5eAaPmIp3nS
k	k	k7c3
Phoebe	Phoeb	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joe	k2eAgInPc1d1
<g/>
,	,	kIx,
stále	stále	k6eAd1
neúspěšný	úspěšný	k2eNgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
spolubydlící	spolubydlící	k1gFnSc4
a	a	k8xC
dostane	dostat	k5eAaPmIp3nS
roli	role	k1gFnSc4
v	v	k7c6
kabelovém	kabelový	k2eAgInSc6d1
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
„	„	k?
<g/>
Mac	Mac	kA
a	a	k8xC
C.	C.	kA
H.	H.	kA
E.	E.	kA
E.	E.	kA
S.	S.	kA
E.	E.	kA
<g/>
,	,	kIx,
<g/>
“	“	k?
kde	kde	k6eAd1
hraje	hrát	k5eAaImIp3nS
po	po	k7c6
boku	bok	k1gInSc6
robota	robot	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ross	Ross	k1gInSc1
dostane	dostat	k5eAaPmIp3nS
práci	práce	k1gFnSc4
jako	jako	k8xC,k8xS
profesor	profesor	k1gMnSc1
na	na	k7c6
Newyorské	newyorský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
začne	začít	k5eAaPmIp3nS
chodit	chodit	k5eAaImF
se	s	k7c7
studentkou	studentka	k1gFnSc7
Elizabeth	Elizabeth	k1gFnSc1
(	(	kIx(
<g/>
Alexandra	Alexandra	k1gFnSc1
Holden	Holdna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejího	její	k3xOp3gMnSc2
otce	otec	k1gMnSc2
hraje	hrát	k5eAaImIp3nS
ve	v	k7c6
třech	tři	k4xCgFnPc6
epizodách	epizoda	k1gFnPc6
Bruce	Bruce	k1gMnSc1
Willis	Willis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoebiin	Phoebiin	k2eAgInSc4d1
a	a	k8xC
Rachelin	Rachelin	k2eAgInSc4d1
byt	byt	k1gInSc4
zachvátí	zachvátit	k5eAaPmIp3nS
požár	požár	k1gInSc1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
němuž	jenž	k3xRgNnSc3
se	se	k3xPyFc4
Rachel	Rachel	k1gMnSc1
přestěhuje	přestěhovat	k5eAaPmIp3nS
k	k	k7c3
Joeymu	Joeymum	k1gNnSc3
a	a	k8xC
Phoebe	Phoeb	k1gInSc5
k	k	k7c3
Chandlerovi	Chandler	k1gMnSc3
a	a	k8xC
Monice	Monika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
epizodě	epizoda	k1gFnSc6
se	se	k3xPyFc4
Chandler	Chandler	k1gMnSc1
rozhodne	rozhodnout	k5eAaPmIp3nS
požádat	požádat	k5eAaPmF
Monicu	Monica	k1gFnSc4
o	o	k7c4
ruku	ruka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
ale	ale	k8xC
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
původní	původní	k2eAgInSc4d1
plán	plán	k1gInSc4
na	na	k7c4
požádání	požádání	k1gNnSc4
o	o	k7c4
ruku	ruka	k1gFnSc4
selže	selhat	k5eAaPmIp3nS
<g/>
,	,	kIx,
chce	chtít	k5eAaImIp3nS
Monicu	Monica	k1gFnSc4
zmást	zmást	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
neustále	neustále	k6eAd1
vyjadřovat	vyjadřovat	k5eAaImF
proti	proti	k7c3
svatbě	svatba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
však	však	k9
Monicu	Monicum	k1gNnSc3
v	v	k7c6
restauraci	restaurace	k1gFnSc6
navštíví	navštívit	k5eAaPmIp3nS
bývalý	bývalý	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ji	on	k3xPp3gFnSc4
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
stále	stále	k6eAd1
miluje	milovat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
s	s	k7c7
ní	on	k3xPp3gFnSc7
chce	chtít	k5eAaImIp3nS
mít	mít	k5eAaImF
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monica	Monic	k1gInSc2
nakonec	nakonec	k6eAd1
prokoukne	prokouknout	k5eAaPmIp3nS
Chandlerův	Chandlerův	k2eAgInSc1d1
plán	plán	k1gInSc1
a	a	k8xC
sama	sám	k3xTgFnSc1
ho	on	k3xPp3gMnSc4
klečíc	klečet	k5eAaImSgFnS
požádá	požádat	k5eAaPmIp3nS
o	o	k7c4
ruku	ruka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
protože	protože	k8xS
začne	začít	k5eAaPmIp3nS
brečet	brečet	k5eAaImF
<g/>
,	,	kIx,
žádost	žádost	k1gFnSc4
nedokončí	dokončit	k5eNaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
ji	on	k3xPp3gFnSc4
Chandler	Chandler	k1gMnSc1
požádá	požádat	k5eAaPmIp3nS
o	o	k7c4
ruku	ruka	k1gFnSc4
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epizoda	epizoda	k1gFnSc1
končí	končit	k5eAaImIp3nS
velkou	velký	k2eAgFnSc7d1
oslavou	oslava	k1gFnSc7
s	s	k7c7
jejich	jejich	k3xOp3gMnPc7
přáteli	přítel	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
stáli	stát	k5eAaImAgMnP
za	za	k7c7
dveřmi	dveře	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Série	série	k1gFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2000	#num#	k4
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
Nejlepší	dobrý	k2eAgInSc4d3
komediální	komediální	k2eAgInSc4d1
seriál	seriál	k1gInSc4
cen	cena	k1gFnPc2
Emmy	Emma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
Sedmá	sedmý	k4xOgFnSc1
série	série	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
svatbě	svatba	k1gFnSc3
Monicy	Monica	k1gFnSc2
a	a	k8xC
Chandlera	Chandler	k1gMnSc2
a	a	k8xC
jejím	její	k3xOp3gFnPc3
přípravám	příprava	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joeyho	Joey	k1gMnSc4
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
zruší	zrušit	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
znovu	znovu	k6eAd1
mu	on	k3xPp3gMnSc3
nabídnou	nabídnout	k5eAaPmIp3nP
roli	role	k1gFnSc4
v	v	k7c6
Dnech	den	k1gInPc6
našeho	náš	k3xOp1gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoebin	Phoebin	k2eAgInSc4d1
byt	byt	k1gInSc4
je	být	k5eAaImIp3nS
opraven	opravit	k5eAaPmNgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
zboření	zboření	k1gNnSc3
nelegálně	legálně	k6eNd1
postavení	postavení	k1gNnSc4
příčky	příčka	k1gFnSc2
Rachel	Rachela	k1gFnPc2
nakonec	nakonec	k6eAd1
zůstane	zůstat	k5eAaPmIp3nS
u	u	k7c2
Joeyho	Joey	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
dvě	dva	k4xCgFnPc1
epizody	epizoda	k1gFnPc1
série	série	k1gFnSc2
se	se	k3xPyFc4
týkají	týkat	k5eAaImIp3nP
Moničiny	Moničin	k2eAgFnPc1d1
a	a	k8xC
Chandlerovy	Chandlerův	k2eAgFnPc1d1
svatby	svatba	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
roli	role	k1gFnSc6
Chandlerova	Chandlerův	k2eAgMnSc2d1
otce	otec	k1gMnSc2
objevila	objevit	k5eAaPmAgFnS
Kathleen	Kathleen	k1gInSc4
Turner	turner	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěrečné	závěrečný	k2eAgInPc1d1
momenty	moment	k1gInPc1
série	série	k1gFnSc2
prozrazují	prozrazovat	k5eAaImIp3nP
Rachelino	Rachelin	k2eAgNnSc1d1
těhotenství	těhotenství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Osmá	osmý	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
epizodě	epizoda	k1gFnSc6
osmé	osmý	k4xOgFnSc2
série	série	k1gFnSc2
se	se	k3xPyFc4
hledá	hledat	k5eAaImIp3nS
otec	otec	k1gMnSc1
Rachelina	Rachelin	k2eAgNnSc2d1
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
epizodě	epizoda	k1gFnSc6
se	se	k3xPyFc4
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jím	on	k3xPp3gNnSc7
je	být	k5eAaImIp3nS
Ross	Ross	k1gInSc4
a	a	k8xC
v	v	k7c6
následující	následující	k2eAgFnSc6d1
epizodě	epizoda	k1gFnSc6
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
Rachel	Rachel	k1gInSc4
řekne	říct	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joeum	k1gNnPc7
se	se	k3xPyFc4
zamiloval	zamilovat	k5eAaPmAgMnS
do	do	k7c2
své	svůj	k3xOyFgFnSc2
spolubydlící	spolubydlící	k1gFnSc2
Rachel	Rachel	k1gInSc1
a	a	k8xC
když	když	k8xS
jí	on	k3xPp3gFnSc3
o	o	k7c6
tom	ten	k3xDgNnSc6
pověděl	povědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
oba	dva	k4xCgInPc1
se	se	k3xPyFc4
cítili	cítit	k5eAaImAgMnP
trapně	trapně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
vztah	vztah	k1gInSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
vrátil	vrátit	k5eAaPmAgInS
do	do	k7c2
starých	starý	k2eAgFnPc2d1
kolejí	kolej	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
závěru	závěr	k1gInSc6
série	série	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Rachel	Rachela	k1gFnPc2
porodila	porodit	k5eAaPmAgFnS
dceru	dcera	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
s	s	k7c7
Rossem	Ross	k1gInSc7
pojmenovali	pojmenovat	k5eAaPmAgMnP
Emma	Emma	k1gFnSc1
<g/>
,	,	kIx,
přijala	přijmout	k5eAaPmAgFnS
neúmyslnou	úmyslný	k2eNgFnSc4d1
nabídku	nabídka	k1gFnSc4
k	k	k7c3
sňatku	sňatek	k1gInSc3
od	od	k7c2
Joeyho	Joey	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
si	se	k3xPyFc3
mylně	mylně	k6eAd1
vyložila	vyložit	k5eAaPmAgFnS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
Joey	Joea	k1gFnSc2
sehnul	sehnout	k5eAaPmAgMnS
pro	pro	k7c4
prstýnek	prstýnek	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vypadl	vypadnout	k5eAaPmAgInS
z	z	k7c2
Rossova	Rossův	k2eAgInSc2d1
kabátu	kabát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
série	série	k1gFnSc1
vyhrála	vyhrát	k5eAaPmAgFnS
cenu	cena	k1gFnSc4
Emmy	Emma	k1gFnSc2
za	za	k7c4
Nejlepší	dobrý	k2eAgInSc4d3
komediální	komediální	k2eAgInSc4d1
seriál	seriál	k1gInSc4
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Devátá	devátý	k4xOgFnSc1
série	série	k1gFnSc1
</s>
<s>
V	v	k7c6
deváté	devátý	k4xOgFnSc6
sérii	série	k1gFnSc6
začnou	začít	k5eAaPmIp3nP
Ross	Ross	k1gInSc4
a	a	k8xC
Rachel	Rachel	k1gInSc4
bydlet	bydlet	k5eAaImF
s	s	k7c7
jejich	jejich	k3xOp3gFnSc7
dcerkou	dcerka	k1gFnSc7
Emmou	Emma	k1gFnSc7
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
vyjasní	vyjasnit	k5eAaPmIp3nS
omyl	omyl	k1gInSc1
ohledně	ohledně	k7c2
Joeyho	Joey	k1gMnSc2
nabídky	nabídka	k1gFnSc2
k	k	k7c3
sňatku	sňatek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rachel	Rachel	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
brzy	brzy	k6eAd1
po	po	k7c6
sporech	spor	k1gInPc6
s	s	k7c7
Rossem	Ross	k1gMnSc7
vrátí	vrátit	k5eAaPmIp3nS
k	k	k7c3
Joeymu	Joeym	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monica	Monic	k1gInSc2
a	a	k8xC
Chandler	Chandler	k1gMnSc1
<g/>
,	,	kIx,
inspirovaní	inspirovaný	k2eAgMnPc1d1
Rossem	Rosso	k1gNnSc7
a	a	k8xC
Rachel	Rachela	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozhodnou	rozhodnout	k5eAaPmIp3nP
mít	mít	k5eAaImF
vlastní	vlastní	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
několik	několik	k4yIc1
epizod	epizoda	k1gFnPc2
marně	marně	k6eAd1
pokoušejí	pokoušet	k5eAaImIp3nP
o	o	k7c6
otěhotnění	otěhotnění	k1gNnSc6
nakonec	nakonec	k6eAd1
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
oba	dva	k4xCgMnPc1
fyzicky	fyzicky	k6eAd1
neschopní	schopný	k2eNgMnPc1d1
spolu	spolu	k6eAd1
počít	počít	k5eAaPmF
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
Mike	Mike	k1gFnSc1
Hannigan	Hannigan	k1gMnSc1
(	(	kIx(
<g/>
Paul	Paul	k1gMnSc1
Rudd	Rudd	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
nový	nový	k2eAgMnSc1d1
Phoebiin	Phoebiin	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
se	se	k3xPyFc4
však	však	k9
z	z	k7c2
Minska	Minsk	k1gInSc2
vrátí	vrátit	k5eAaPmIp3nS
vědec	vědec	k1gMnSc1
David	David	k1gMnSc1
(	(	kIx(
<g/>
Hank	Hank	k1gInSc1
Azaria	Azarium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
Phoebe	Phoeb	k1gInSc5
chodila	chodit	k5eAaImAgFnS
v	v	k7c6
první	první	k4xOgFnSc6
sérii	série	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sama	sám	k3xTgFnSc1
se	se	k3xPyFc4
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
musí	muset	k5eAaImIp3nP
rozhodnout	rozhodnout	k5eAaPmF
a	a	k8xC
nakonec	nakonec	k6eAd1
zvolí	zvolit	k5eAaPmIp3nS
Mikea	Mikea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
deváté	devátý	k4xOgFnSc2
série	série	k1gFnSc2
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
Barbadosu	Barbados	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
všichni	všechen	k3xTgMnPc1
letí	letět	k5eAaImIp3nP
na	na	k7c4
paleontologickou	paleontologický	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
Ross	Ross	k1gInSc4
hlavní	hlavní	k2eAgInSc4d1
projev	projev	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
se	se	k3xPyFc4
také	také	k9
objeví	objevit	k5eAaPmIp3nS
paleontoložka	paleontoložka	k1gFnSc1
Charlie	Charlie	k1gMnSc1
(	(	kIx(
<g/>
Aisha	Aisha	k1gMnSc1
Tyler	Tyler	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgFnSc1
černošská	černošský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
ve	v	k7c6
vedlejší	vedlejší	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charlie	Charlie	k1gMnSc1
je	být	k5eAaImIp3nS
Joeyho	Joey	k1gMnSc4
inteligentní	inteligentní	k2eAgFnSc2d1
přítelkyně	přítelkyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joe	k1gMnPc4
Charlie	Charlie	k1gMnSc1
přitahuje	přitahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přes	přes	k7c4
to	ten	k3xDgNnSc4
ji	on	k3xPp3gFnSc4
nakonec	nakonec	k6eAd1
zaujme	zaujmout	k5eAaPmIp3nS
Ross	Ross	k1gInSc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
je	být	k5eAaImIp3nS
inteligencí	inteligence	k1gFnSc7
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
Charlie	Charlie	k1gMnSc1
opustí	opustit	k5eAaPmIp3nS
Joeyho	Joey	k1gMnSc2
<g/>
,	,	kIx,
Joey	Joea	k1gMnSc2
a	a	k8xC
Rachel	Rachel	k1gInSc1
opět	opět	k6eAd1
projeví	projevit	k5eAaPmIp3nS
své	svůj	k3xOyFgInPc4
city	cit	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jeden	jeden	k4xCgMnSc1
ke	k	k7c3
druhému	druhý	k4xOgMnSc3
chovají	chovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhlasí	souhlasit	k5eAaImIp3nS
však	však	k9
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
nejprve	nejprve	k6eAd1
nebudou	být	k5eNaImBp3nP
brát	brát	k5eAaImF
tolik	tolik	k6eAd1
v	v	k7c4
potaz	potaz	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
změní	změnit	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
Joey	Joe	k2eAgFnPc1d1
uvidí	uvidět	k5eAaPmIp3nP
Rosse	Rosse	k1gFnPc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
líbá	líbat	k5eAaImIp3nS
Charlie	Charlie	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Série	série	k1gFnSc1
končí	končit	k5eAaImIp3nS
záběrem	záběr	k1gInSc7
na	na	k7c4
líbajícího	líbající	k2eAgMnSc4d1
se	se	k3xPyFc4
Joeyho	Joey	k1gMnSc4
a	a	k8xC
Rachel	Rachel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Desátá	desátá	k1gFnSc1
série	série	k1gFnSc2
</s>
<s>
V	v	k7c6
desáté	desátý	k4xOgFnSc6
sérii	série	k1gFnSc6
se	se	k3xPyFc4
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
několik	několik	k4yIc1
motivů	motiv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joeum	k1gNnPc7
a	a	k8xC
Rachel	Rachel	k1gInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
zvládnout	zvládnout	k5eAaPmF
Rossovu	Rossův	k2eAgFnSc4d1
vnitřní	vnitřní	k2eAgFnSc4d1
nevyrovnanost	nevyrovnanost	k1gFnSc4
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
vztahem	vztah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
však	však	k9
stane	stanout	k5eAaPmIp3nS
pohromou	pohroma	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
rozhodnout	rozhodnout	k5eAaPmF
zůstat	zůstat	k5eAaPmF
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charlie	Charlie	k1gMnSc1
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
vrátit	vrátit	k5eAaPmF
k	k	k7c3
bývalému	bývalý	k2eAgMnSc3d1
příteli	přítel	k1gMnSc3
Benjaminu	Benjamin	k1gMnSc3
Hobartovi	Hobart	k1gMnSc3
(	(	kIx(
<g/>
Greg	Greg	k1gMnSc1
Kinnear	Kinnear	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
kterého	který	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
se	se	k3xPyFc4
Ross	Ross	k1gInSc1
snaží	snažit	k5eAaImIp3nS
získat	získat	k5eAaPmF
výzkumný	výzkumný	k2eAgInSc1d1
grant	grant	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monica	Monicum	k1gNnPc1
a	a	k8xC
Chandler	Chandler	k1gInSc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
adoptovat	adoptovat	k5eAaPmF
dítě	dítě	k1gNnSc4
a	a	k8xC
seznámili	seznámit	k5eAaPmAgMnP
se	se	k3xPyFc4
s	s	k7c7
Erikou	Erika	k1gFnSc7
(	(	kIx(
<g/>
Anna	Anna	k1gFnSc1
Faris	Faris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
biologickou	biologický	k2eAgFnSc7d1
matkou	matka	k1gFnSc7
z	z	k7c2
Ohia	Ohio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erika	Erika	k1gFnSc1
jim	on	k3xPp3gMnPc3
na	na	k7c6
konci	konec	k1gInSc6
série	série	k1gFnSc2
porodí	porodit	k5eAaPmIp3nP
dvojčata	dvojče	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phoebe	Phoeb	k1gInSc5
a	a	k8xC
Mike	Mike	k1gNnPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
závěru	závěr	k1gInSc6
série	série	k1gFnSc2
svatbu	svatba	k1gFnSc4
a	a	k8xC
Rachel	Rachel	k1gInSc4
vezme	vzít	k5eAaPmIp3nS
práci	práce	k1gFnSc4
u	u	k7c2
Louise	Louis	k1gMnSc2
Vuittona	Vuitton	k1gMnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ross	Rossa	k1gFnPc2
se	se	k3xPyFc4
Rachel	Rachel	k1gInSc1
vyzná	vyznat	k5eAaPmIp3nS,k5eAaBmIp3nS
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
lásky	láska	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
i	i	k9
přesto	přesto	k8xC
nasedá	nasedat	k5eAaImIp3nS
do	do	k7c2
letadla	letadlo	k1gNnSc2
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chandler	Chandler	k1gInSc4
a	a	k8xC
Monica	Monica	k1gFnSc1
se	se	k3xPyFc4
chystají	chystat	k5eAaImIp3nP
odstěhovat	odstěhovat	k5eAaPmF
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
bytu	byt	k1gInSc2
na	na	k7c6
předměstí	předměstí	k1gNnSc6
a	a	k8xC
Joey	Joea	k1gFnSc2
je	být	k5eAaImIp3nS
naštvaný	naštvaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
všechno	všechen	k3xTgNnSc1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
zhrzený	zhrzený	k2eAgInSc1d1
Ross	Ross	k1gInSc1
dorazí	dorazit	k5eAaPmIp3nS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
bytu	byt	k1gInSc2
<g/>
,	,	kIx,
poslechne	poslechnout	k5eAaPmIp3nS
si	se	k3xPyFc3
vzkazy	vzkaz	k1gInPc4
na	na	k7c6
záznamníku	záznamník	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
i	i	k9
jeden	jeden	k4xCgInSc1
od	od	k7c2
Rachel	Rachela	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
něm	on	k3xPp3gNnSc6
vyzná	vyznat	k5eAaPmIp3nS,k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
taky	taky	k6eAd1
miluje	milovat	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
vzkaz	vzkaz	k1gInSc1
skončí	skončit	k5eAaPmIp3nS
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
dostane	dostat	k5eAaPmIp3nS
z	z	k7c2
letadla	letadlo	k1gNnSc2
a	a	k8xC
Ross	Ross	k1gInSc1
tak	tak	k6eAd1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
do	do	k7c2
Paříže	Paříž	k1gFnSc2
odletěla	odletět	k5eAaPmAgFnS
nebo	nebo	k8xC
ne	ne	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzápětí	vzápětí	k6eAd1
se	se	k3xPyFc4
objeví	objevit	k5eAaPmIp3nS
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
dveřích	dveře	k1gFnPc6
a	a	k8xC
řekne	říct	k5eAaPmIp3nS
mu	on	k3xPp3gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
také	také	k9
miluje	milovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
poslední	poslední	k2eAgFnSc2d1
epizody	epizoda	k1gFnSc2
seriálu	seriál	k1gInSc2
se	se	k3xPyFc4
plačící	plačící	k2eAgMnPc1d1
Rachel	Rachel	k1gMnSc1
zeptá	zeptat	k5eAaPmIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Nedáme	dát	k5eNaPmIp1nP
si	se	k3xPyFc3
kafe	kafe	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
<g/>
“	“	k?
na	na	k7c4
což	což	k3yRnSc4,k3yQnSc4
Chandler	Chandler	k1gMnSc1
odpoví	odpovědět	k5eAaPmIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Jasně	jasně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kde	kde	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
což	což	k3yQnSc4,k3yRnSc4
jsou	být	k5eAaImIp3nP
poslední	poslední	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
pronesená	pronesený	k2eAgNnPc1d1
v	v	k7c6
tomto	tento	k3xDgInSc6
seriálu	seriál	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
postav	postava	k1gFnPc2
seriálu	seriál	k1gInSc2
Přátelé	přítel	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s>
Jennifer	Jennifer	k1gInSc1
Aniston	Aniston	k1gInSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Miriam	Miriam	k1gFnSc1
Chytilová	Chytilová	k1gFnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Rachel	Rachel	k1gMnSc1
Greenová	Greenová	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Rachel	Rachel	k1gInSc1
Greenová-Gellerová	Greenová-Gellerová	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
módní	módní	k2eAgFnSc1d1
nadšenkyně	nadšenkyně	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
potom	potom	k6eAd1
co	co	k9
utekla	utéct	k5eAaPmAgFnS
od	od	k7c2
svatby	svatba	k1gFnSc2
s	s	k7c7
Barrym	Barrym	k1gInSc1
<g/>
,	,	kIx,
pracovala	pracovat	k5eAaImAgFnS
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
v	v	k7c6
kavárně	kavárna	k1gFnSc6
Central	Central	k1gMnSc2
Perk	Perk	k?
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
neprorazila	prorazit	k5eNaPmAgFnS
v	v	k7c6
módním	módní	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
nejprve	nejprve	k6eAd1
u	u	k7c2
Bloomingdale	Bloomingdala	k1gFnSc6
<g/>
'	'	kIx"
<g/>
s	s	k7c7
a	a	k8xC
později	pozdě	k6eAd2
u	u	k7c2
Ralpha	Ralph	k1gMnSc2
Laurena	Lauren	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Nejdelší	dlouhý	k2eAgInSc4d3
vztah	vztah	k1gInSc4
měla	mít	k5eAaImAgFnS
s	s	k7c7
Rossem	Ross	k1gInSc7
<g/>
,	,	kIx,
za	za	k7c4
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
se	se	k3xPyFc4
v	v	k7c6
podnapilém	podnapilý	k2eAgInSc6d1
stavu	stav	k1gInSc6
provdala	provdat	k5eAaPmAgFnS
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
rozvedli	rozvést	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
mají	mít	k5eAaImIp3nP
dceru	dcera	k1gFnSc4
Emmu	Emma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
dílu	díl	k1gInSc6
seriálu	seriál	k1gInSc2
se	se	k3xPyFc4
Ross	Ross	k1gInSc1
a	a	k8xC
Rachel	Rachel	k1gInSc1
znovu	znovu	k6eAd1
dají	dát	k5eAaPmIp3nP
dohromady	dohromady	k6eAd1
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
vztah	vztah	k1gInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
vyjádření	vyjádření	k1gNnSc2
Joeyho	Joey	k1gMnSc2
v	v	k7c6
seriálu	seriál	k1gInSc6
Joey	Joea	k1gFnSc2
<g/>
,	,	kIx,
volném	volný	k2eAgNnSc6d1
pokračování	pokračování	k1gNnSc6
seriálu	seriál	k1gInSc2
Přátelé	přítel	k1gMnPc1
<g/>
,	,	kIx,
zpečetěn	zpečetěn	k2eAgInSc1d1
i	i	k9
manželstvím	manželství	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Courteney	Courtene	k2eAgFnPc1d1
Cox	Cox	k1gFnPc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Pavla	Pavla	k1gFnSc1
Rychlá	rychlý	k2eAgFnSc1d1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
Monica	Monic	k2eAgFnSc1d1
Gellerová	Gellerová	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Monica	Monica	k1gFnSc1
Gellerová-Bingová	Gellerová-Bingový	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Rossova	Rossův	k2eAgFnSc1d1
sestra	sestra	k1gFnSc1
a	a	k8xC
šéfkuchařka	šéfkuchařka	k1gFnSc1
v	v	k7c6
luxusní	luxusní	k2eAgFnSc6d1
restauraci	restaurace	k1gFnSc6
Javu	Javus	k1gInSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
restauracích	restaurace	k1gFnPc6
<g/>
,	,	kIx,
např.	např.	kA
U	u	k7c2
Alessandra	Alessandra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
dospívání	dospívání	k1gNnSc2
měla	mít	k5eAaImAgFnS
problémy	problém	k1gInPc4
s	s	k7c7
obezitou	obezita	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
posedlá	posedlý	k2eAgFnSc1d1
pořádkem	pořádek	k1gInSc7
<g/>
,	,	kIx,
uklízením	uklízení	k1gNnSc7
a	a	k8xC
čistotou	čistota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
bytě	byt	k1gInSc6
po	po	k7c6
babičce	babička	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
soutěživá	soutěživý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
minulosti	minulost	k1gFnSc6
měla	mít	k5eAaImAgFnS
vztah	vztah	k1gInSc4
s	s	k7c7
rodinným	rodinný	k2eAgMnSc7d1
známým	známý	k2eAgMnSc7d1
Richardem	Richard	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
si	se	k3xPyFc3
začala	začít	k5eAaPmAgFnS
s	s	k7c7
Chandlerem	Chandler	k1gMnSc7
<g/>
,	,	kIx,
za	za	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
provdala	provdat	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lisa	Lisa	k1gFnSc1
Kudrow	Kudrow	k1gFnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Stanislava	Stanislava	k1gFnSc1
Jachnická	Jachnický	k2eAgFnSc1d1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
Phoebe	Phoeb	k1gInSc5
Bufetová	bufetový	k2eAgNnPc1d1
(	(	kIx(
<g/>
v	v	k7c6
originále	originál	k1gInSc6
Phoebe	Phoeb	k1gInSc5
Buffay	Buffa	k2eAgInPc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Phoebe	Phoeb	k1gInSc5
Bufetová-Hanniganová	Bufetová-Hanniganová	k1gFnSc1
/	/	kIx~
Buffay-Hannigan	Buffay-Hannigan	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
potrhlá	potrhlý	k2eAgFnSc1d1
masérka	masérka	k1gFnSc1
a	a	k8xC
folková	folkový	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Měla	mít	k5eAaImAgFnS
těžké	těžký	k2eAgNnSc4d1
dětství	dětství	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
podepsalo	podepsat	k5eAaPmAgNnS
na	na	k7c6
její	její	k3xOp3gFnSc6
výstřednosti	výstřednost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
identické	identický	k2eAgNnSc4d1
dvojče	dvojče	k1gNnSc4
Uršulu	Uršula	k1gFnSc4
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
však	však	k9
nestýká	stýkat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vegetariánka	vegetariánka	k1gFnSc1
a	a	k8xC
svému	svůj	k3xOyFgMnSc3
nevlastnímu	vlastní	k2eNgMnSc3d1
bratrovi	bratr	k1gMnSc3
porodila	porodit	k5eAaPmAgFnS
trojčata	trojče	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
seriálu	seriál	k1gInSc2
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
Mika	Mik	k1gMnSc2
Hannigana	Hannigan	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Matt	Matt	k2eAgInSc1d1
LeBlanc	LeBlanc	k1gInSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Rychlý	Rychlý	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
Joey	Joea	k1gFnPc4
Tribbiani	Tribbiaň	k1gFnSc3
je	být	k5eAaImIp3nS
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
známý	známý	k1gMnSc1
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
Dr	dr	kA
<g/>
.	.	kIx.
Drakea	Drake	k2eAgFnSc1d1
Remoraye	Remoraye	k1gFnSc1
v	v	k7c6
televizním	televizní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
Dny	dna	k1gFnSc2
našeho	náš	k3xOp1gInSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
Tak	tak	k9
jde	jít	k5eAaImIp3nS
čas	čas	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Původem	původ	k1gInSc7
je	být	k5eAaImIp3nS
Ital	Ital	k1gMnSc1
a	a	k8xC
má	mít	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
trochu	trochu	k6eAd1
méně	málo	k6eAd2
bystrý	bystrý	k2eAgInSc1d1
<g/>
,	,	kIx,
miluje	milovat	k5eAaImIp3nS
jídlo	jídlo	k1gNnSc1
a	a	k8xC
je	být	k5eAaImIp3nS
velký	velký	k2eAgMnSc1d1
sukničkář	sukničkář	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc7
největším	veliký	k2eAgMnSc7d3
přítelem	přítel	k1gMnSc7
je	být	k5eAaImIp3nS
Chandler	Chandler	k1gMnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
bydlel	bydlet	k5eAaImAgMnS
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
Chandler	Chandler	k1gMnSc1
odstěhoval	odstěhovat	k5eAaPmAgMnS
k	k	k7c3
Monice	Monika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joey	Joe	k1gMnPc7
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
volně	volně	k6eAd1
navazujícího	navazující	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Joey	Joea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Matthew	Matthew	k?
Perry	Perra	k1gFnPc1
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
<g/>
:	:	kIx,
Rostislav	Rostislav	k1gMnSc1
Čtvrtlík	Čtvrtlík	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
několika	několik	k4yIc6
dílech	díl	k1gInPc6
Aleš	Aleš	k1gMnSc1
Procházka	Procházka	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xS,k8xC
Chandler	Chandler	k1gMnSc1
Bing	bingo	k1gNnPc2
měl	mít	k5eAaImAgMnS
dříve	dříve	k6eAd2
v	v	k7c6
popisu	popis	k1gInSc6
práce	práce	k1gFnSc2
statistickou	statistický	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
a	a	k8xC
rekonfiguraci	rekonfigurace	k1gFnSc4
dat	datum	k1gNnPc2
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
mezinárodní	mezinárodní	k2eAgFnSc6d1
firmě	firma	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
však	však	k9
začal	začít	k5eAaPmAgInS
pracovat	pracovat	k5eAaImF
v	v	k7c6
reklamní	reklamní	k2eAgFnSc6d1
agentuře	agentura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dětství	dětství	k1gNnSc6
ho	on	k3xPp3gMnSc2
poznamenal	poznamenat	k5eAaPmAgInS
rozvod	rozvod	k1gInSc4
rodičů	rodič	k1gMnPc2
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
měl	mít	k5eAaImAgInS
problémy	problém	k1gInPc4
se	s	k7c7
ženami	žena	k1gFnPc7
a	a	k8xC
jakýkoliv	jakýkoliv	k3yIgInSc4
vztah	vztah	k1gInSc4
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
byl	být	k5eAaImAgInS
frustrující	frustrující	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Monikou	Monika	k1gFnSc7
a	a	k8xC
adoptovali	adoptovat	k5eAaPmAgMnP
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
charakteristický	charakteristický	k2eAgInSc1d1
svým	svůj	k3xOyFgInSc7
sarkastickým	sarkastický	k2eAgInSc7d1
smyslem	smysl	k1gInSc7
pro	pro	k7c4
humor	humor	k1gInSc4
a	a	k8xC
tanečními	taneční	k2eAgFnPc7d1
kreacemi	kreace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
gay	gay	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
David	David	k1gMnSc1
Schwimmer	Schwimmer	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
dabing	dabing	k1gInSc1
<g/>
:	:	kIx,
Daniel	Daniel	k1gMnSc1
Rous	Rous	k1gMnSc1
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
Ross	Ross	k1gInSc1
Geller	Geller	k1gMnSc1
je	být	k5eAaImIp3nS
Moničin	Moničin	k2eAgMnSc1d1
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
paleontolog	paleontolog	k1gMnSc1
pracující	pracující	k1gMnSc1
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
prehistorických	prehistorický	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
profesor	profesor	k1gMnSc1
paleontologie	paleontologie	k1gFnSc2
na	na	k7c6
Newyorské	newyorský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
rád	rád	k6eAd1
slušnou	slušný	k2eAgFnSc4d1
mluvu	mluva	k1gFnSc4
a	a	k8xC
od	od	k7c2
střední	střední	k2eAgFnSc2d1
školy	škola	k1gFnSc2
miluje	milovat	k5eAaImIp3nS
Rachel	Rachel	k1gMnSc1
Greenovou	Greenová	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nemá	mít	k5eNaImIp3nS
štěstí	štěstí	k1gNnSc4
na	na	k7c4
vztahy	vztah	k1gInPc4
s	s	k7c7
ženami	žena	k1gFnPc7
a	a	k8xC
je	být	k5eAaImIp3nS
třikrát	třikrát	k6eAd1
rozvedený	rozvedený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
–	–	k?
Bena	Bena	k?
a	a	k8xC
Emmu	Emma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
byla	být	k5eAaImAgFnS
mediálně	mediálně	k6eAd1
propírána	propírat	k5eAaImNgFnS
jeho	jeho	k3xOp3gFnSc1
podoba	podoba	k1gFnSc1
s	s	k7c7
ex-ministrem	ex-ministr	k1gInSc7
Vladimírem	Vladimír	k1gMnSc7
Mlynářem	Mlynář	k1gMnSc7
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
byla	být	k5eAaImAgNnP
skutečně	skutečně	k6eAd1
velmi	velmi	k6eAd1
výrazná	výrazný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Produkce	produkce	k1gFnSc1
</s>
<s>
Na	na	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
podílelo	podílet	k5eAaImAgNnS
množství	množství	k1gNnSc1
scenáristů	scenárista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
dílů	díl	k1gInPc2
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gMnPc6
podíleli	podílet	k5eAaImAgMnP
Ted	Ted	k1gMnSc1
Cohen	Cohen	k1gInSc1
(	(	kIx(
<g/>
25	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
Reich	Reich	k?
(	(	kIx(
<g/>
25	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
Silveri	Silver	k1gFnSc2
(	(	kIx(
<g/>
23	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Shana	Shana	k1gFnSc1
Goldberg-Meehan	Goldberg-Meehana	k1gFnPc2
(	(	kIx(
<g/>
22	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Necelou	celý	k2eNgFnSc4d1
polovinu	polovina	k1gFnSc4
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc6
režírovali	režírovat	k5eAaImAgMnP
Gary	Gara	k1gFnPc4
Halvorson	Halvorson	k1gInSc1
(	(	kIx(
<g/>
55	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Kevin	Kevin	k1gMnSc1
Bright	Bright	k1gMnSc1
(	(	kIx(
<g/>
54	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgMnPc3d1
pravidelným	pravidelný	k2eAgMnPc3d1
režisérům	režisér	k1gMnPc3
patřili	patřit	k5eAaImAgMnP
Michael	Michael	k1gMnSc1
Lembeck	Lembeck	k1gMnSc1
(	(	kIx(
<g/>
24	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
James	James	k1gMnSc1
Burrows	Burrows	k1gInSc1
(	(	kIx(
<g/>
15	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gail	Gail	k1gMnSc1
Mancuso	Mancusa	k1gFnSc5
(	(	kIx(
<g/>
14	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Bonerz	Bonerz	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
David	David	k1gMnSc1
Schwimmer	Schwimmer	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Ben	Ben	k1gInSc1
Weiss	Weiss	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
epizod	epizoda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vysílání	vysílání	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
Přátelé	přítel	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s>
ŘadaDílyPremiéra	ŘadaDílyPremiéra	k1gFnSc1
v	v	k7c4
USAPremiéra	USAPremiér	k1gMnSc4
v	v	k7c6
ČR	ČR	kA
</s>
<s>
První	první	k4xOgMnPc1
dílPoslední	dílPosledný	k2eAgMnPc1d1
dílPrvní	dílPrvnit	k5eAaPmIp3nP
dílPoslední	dílPosledný	k2eAgMnPc1d1
díl	díl	k1gInSc4
</s>
<s>
1	#num#	k4
</s>
<s>
2419940922	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
199419950518	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
199519970104	#num#	k4
<g/>
a	a	k8xC
<g/>
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
199719970614	#num#	k4
<g/>
a	a	k8xC
<g/>
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1997	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
2419950921	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
199519960516	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
199619980112	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
199819980622	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1998	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2519960919	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
199619970515	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
199719980901	#num#	k4
<g/>
a	a	k8xC
<g/>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
199819990216	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1999	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2419970925	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
199719980507	#num#	k4
<g/>
a	a	k8xC
<g/>
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
199819990223	#num#	k4
<g/>
a	a	k8xC
<g/>
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
199919990803	#num#	k4
<g/>
a	a	k8xC
<g/>
3	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1999	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
2419980924	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
199819990520	#num#	k4
<g/>
a	a	k8xC
<g/>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
199919990810	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
199920000118	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2000	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2519990923	#num#	k4
<g/>
a	a	k8xC
<g/>
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
199920000518	#num#	k4
<g/>
a	a	k8xC
<g/>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200020010109	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
200120010710	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
2420001012	#num#	k4
<g/>
a	a	k8xC
<g/>
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
200020010517	#num#	k4
<g/>
a	a	k8xC
<g/>
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200120010717	#num#	k4
<g/>
a	a	k8xC
<g/>
17	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
200120020115	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2002	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
2420010927	#num#	k4
<g/>
a	a	k8xC
<g/>
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200120020516	#num#	k4
<g/>
a	a	k8xC
<g/>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200220030107	#num#	k4
<g/>
a	a	k8xC
<g/>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
200320030617	#num#	k4
<g/>
a	a	k8xC
<g/>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
2420020926	#num#	k4
<g/>
a	a	k8xC
<g/>
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200220030515	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200320030909	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200320040224	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
1820030925	#num#	k4
<g/>
a	a	k8xC
<g/>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200320040506	#num#	k4
<g/>
a	a	k8xC
<g/>
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
200420040908	#num#	k4
<g/>
a	a	k8xC
<g/>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
200420041229	#num#	k4
<g/>
a	a	k8xC
<g/>
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2004	#num#	k4
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1
vliv	vliv	k1gInSc1
</s>
<s>
Kavárna	kavárna	k1gFnSc1
Central	Central	k1gFnSc2
Perk	Perk	k?
–	–	k?
jedno	jeden	k4xCgNnSc4
z	z	k7c2
hlavních	hlavní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
setkávání	setkávání	k1gNnSc2
šestice	šestice	k1gFnSc1
přátel	přítel	k1gMnPc2
</s>
<s>
Seriál	seriál	k1gInSc4
Přátelé	přítel	k1gMnPc1
významně	významně	k6eAd1
přispívá	přispívat	k5eAaImIp3nS
do	do	k7c2
některých	některý	k3yIgFnPc2
oblastí	oblast	k1gFnPc2
pop-kultury	pop-kultura	k1gFnSc2
–	–	k?
zejména	zejména	k9
módy	móda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
vliv	vliv	k1gInSc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
jak	jak	k8xC,k8xS
v	v	k7c6
oblasti	oblast	k1gFnSc6
každodenní	každodenní	k2eAgFnSc2d1
módy	móda	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
vlasových	vlasový	k2eAgInPc2d1
účesů	účes	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
účes	účes	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
měla	mít	k5eAaImAgFnS
Jennifer	Jennifer	k1gInSc1
Aniston	Aniston	k1gInSc1
v	v	k7c6
první	první	k4xOgFnSc6
sérii	série	k1gFnSc6
seriálu	seriál	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
kadeřnictví	kadeřnictví	k1gNnSc6
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
„	„	k?
<g/>
Rachel	Rachel	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
byl	být	k5eAaImAgMnS
v	v	k7c6
době	doba	k1gFnSc6
vysílání	vysílání	k1gNnSc2
dané	daný	k2eAgFnSc2d1
série	série	k1gFnSc2
napodobován	napodobovat	k5eAaImNgInS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Joeyho	Joeyze	k6eAd1
„	„	k?
<g/>
balicí	balicí	k2eAgFnSc1d1
<g/>
“	“	k?
fráze	fráze	k1gFnSc1
„	„	k?
<g/>
Tak	tak	k9
jak	jak	k8xS,k8xC
to	ten	k3xDgNnSc1
de	de	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
populární	populární	k2eAgFnSc1d1
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
slangu	slang	k1gInSc6
při	při	k7c6
navazování	navazování	k1gNnSc6
náhodné	náhodný	k2eAgFnSc2d1
známosti	známost	k1gFnSc2
<g/>
,	,	kIx,
či	či	k8xC
při	při	k7c6
vítání	vítání	k1gNnSc6
přátel	přítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seriál	seriál	k1gInSc4
rovněž	rovněž	k9
popularizoval	popularizovat	k5eAaImAgMnS
myšlenku	myšlenka	k1gFnSc4
seznamu	seznam	k1gInSc2
celebrit	celebrita	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
by	by	kYmCp3nS
se	se	k3xPyFc4
dotyčný	dotyčný	k2eAgMnSc1d1
mohl	moct	k5eAaImAgMnS
se	s	k7c7
souhlasem	souhlas	k1gInSc7
svého	svůj	k3xOyFgMnSc2
partnera	partner	k1gMnSc2
či	či	k8xC
partnerky	partnerka	k1gFnPc4
vyspat	vyspat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
epizodě	epizoda	k1gFnSc6
Bratrská	bratrský	k2eAgFnSc1d1
láska	láska	k1gFnSc1
si	se	k3xPyFc3
všichni	všechen	k3xTgMnPc1
své	svůj	k3xOyFgInPc4
seznamy	seznam	k1gInPc4
vymění	vyměnit	k5eAaPmIp3nS
ústně	ústně	k6eAd1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Ross	Ross	k1gInSc1
si	se	k3xPyFc3
vytvoří	vytvořit	k5eAaPmIp3nS
skutečný	skutečný	k2eAgInSc4d1
seznam	seznam	k1gInSc4
a	a	k8xC
nechá	nechat	k5eAaPmIp3nS
jej	on	k3xPp3gMnSc4
zalaminovat	zalaminovat	k5eAaPmF,k5eAaImF,k5eAaBmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
své	svůj	k3xOyFgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
již	již	k6eAd1
později	pozdě	k6eAd2
nemohl	moct	k5eNaImAgMnS
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Velký	velký	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
kulturu	kultura	k1gFnSc4
měla	mít	k5eAaImAgFnS
také	také	k9
podoba	podoba	k1gFnSc1
kavárny	kavárna	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
seriálová	seriálový	k2eAgFnSc1d1
parta	parta	k1gFnSc1
přátel	přítel	k1gMnPc2
scházela	scházet	k5eAaImAgFnS
–	–	k?
Central	Central	k1gFnSc2
Perk	Perk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Íránský	íránský	k2eAgMnSc1d1
miliardář	miliardář	k1gMnSc1
Mojtaba	Mojtaba	k1gMnSc1
Asadian	Asadian	k1gMnSc1
si	se	k3xPyFc3
roku	rok	k1gInSc3
2006	#num#	k4
dokonce	dokonce	k9
nechal	nechat	k5eAaPmAgMnS
název	název	k1gInSc4
a	a	k8xC
podobu	podoba	k1gFnSc4
Central	Central	k1gFnSc1
Perku	perko	k1gNnSc3
registrovat	registrovat	k5eAaBmF
ve	v	k7c6
32	#num#	k4
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
a	a	k8xC
nabízí	nabízet	k5eAaImIp3nS
je	on	k3xPp3gFnPc4
k	k	k7c3
franšízingovému	franšízingový	k2eAgInSc3d1
obchodu	obchod	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Styl	styl	k1gInSc1
kavárny	kavárna	k1gFnSc2
však	však	k9
i	i	k9
bez	bez	k7c2
toho	ten	k3xDgNnSc2
spontánně	spontánně	k6eAd1
zkopírovala	zkopírovat	k5eAaPmAgFnS
řada	řada	k1gFnSc1
podniků	podnik	k1gInPc2
po	po	k7c6
světě	svět	k1gInSc6
–	–	k?
jeden	jeden	k4xCgMnSc1
Central	Central	k1gFnSc3
Perk	Perk	k?
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
na	na	k7c4
Broadwick	Broadwick	k1gInSc4
Street	Streeta	k1gFnPc2
v	v	k7c6
londýnském	londýnský	k2eAgNnSc6d1
Soho	Soho	k1gNnSc1
<g/>
,	,	kIx,
další	další	k2eAgFnSc4d1
otevřel	otevřít	k5eAaPmAgMnS
čínský	čínský	k2eAgMnSc1d1
obchodník	obchodník	k1gMnSc1
Du	Du	k?
Xin	Xin	k1gMnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
roku	rok	k1gInSc2
2010	#num#	k4
atp.	atp.	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přátelé	přítel	k1gMnPc1
jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejúspěšnějších	úspěšný	k2eAgInPc2d3
televizních	televizní	k2eAgInPc2d1
sitcomů	sitcom	k1gInPc2
v	v	k7c6
historii	historie	k1gFnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
si	se	k3xPyFc3
získal	získat	k5eAaPmAgInS
takovou	takový	k3xDgFnSc4
popularitu	popularita	k1gFnSc4
a	a	k8xC
prestiž	prestiž	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
episodách	episoda	k1gFnPc6
objevila	objevit	k5eAaPmAgFnS
řada	řada	k1gFnSc1
celebrit	celebrita	k1gFnPc2
ve	v	k7c6
vedlejších	vedlejší	k2eAgFnPc6d1
rolích	role	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
Brad	brada	k1gFnPc2
Pitt	Pitta	k1gFnPc2
<g/>
,	,	kIx,
Julia	Julius	k1gMnSc2
Roberts	Roberts	k1gInSc4
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
Clooney	Cloonea	k1gMnSc2
<g/>
,	,	kIx,
Jean-Claude	Jean-Claud	k1gInSc5
Van	van	k1gInSc1
Damme	Damm	k1gInSc5
<g/>
,	,	kIx,
Danny	Danna	k1gMnSc2
DeVito	DeVit	k2eAgNnSc1d1
<g/>
,	,	kIx,
Bruce	Bruce	k1gFnSc1
Willis	Willis	k1gFnSc1
<g/>
,	,	kIx,
Charlie	Charlie	k1gMnSc1
Sheen	Sheen	k1gInSc1
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
další	další	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závěrečná	závěrečný	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
seriálu	seriál	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
čtvrtým	čtvrtý	k4xOgInSc7
nejsledovanějším	sledovaný	k2eAgInSc7d3
seriálovým	seriálový	k2eAgInSc7d1
finále	finále	k1gNnSc4
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
po	po	k7c6
MASH	MASH	kA
<g/>
,	,	kIx,
Cheers	Cheers	k1gInSc1
a	a	k8xC
Seinfeldovi	Seinfeldův	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vidělo	vidět	k5eAaImAgNnS
ji	on	k3xPp3gFnSc4
52,5	52,5	k4
milionu	milion	k4xCgInSc2
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sledovanosti	sledovanost	k1gFnSc6
seriálu	seriál	k1gInSc2
nastaly	nastat	k5eAaPmAgInP
dva	dva	k4xCgInPc1
významné	významný	k2eAgInPc1d1
zlomy	zlom	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skok	skok	k1gInSc1
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
zejména	zejména	k9
mezi	mezi	k7c7
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
sérií	série	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přibylo	přibýt	k5eAaPmAgNnS
v	v	k7c6
průměru	průměr	k1gInSc6
5	#num#	k4
milionu	milion	k4xCgInSc2
diváků	divák	k1gMnPc2
(	(	kIx(
<g/>
patrně	patrně	k6eAd1
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
konci	konec	k1gInSc6
5	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
se	se	k3xPyFc4
Rachel	Rachela	k1gFnPc2
a	a	k8xC
Ross	Rossa	k1gFnPc2
znovu	znovu	k6eAd1
vzali	vzít	k5eAaPmAgMnP
<g/>
,	,	kIx,
ovšem	ovšem	k9
v	v	k7c6
opilosti	opilost	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
skok	skok	k1gInSc4
pak	pak	k6eAd1
přišel	přijít	k5eAaPmAgMnS
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
nástupem	nástup	k1gInSc7
osmé	osmý	k4xOgFnSc2
série	série	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přibylo	přibýt	k5eAaPmAgNnS
dalších	další	k2eAgInPc2d1
5	#num#	k4
milionů	milion	k4xCgInPc2
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozorovatelé	pozorovatel	k1gMnPc1
tento	tento	k3xDgInSc4
nárůst	nárůst	k1gInSc4
dávaly	dávat	k5eAaImAgFnP
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
s	s	k7c7
útoky	útok	k1gInPc7
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
série	série	k1gFnSc1
začala	začít	k5eAaPmAgFnS
těsně	těsně	k6eAd1
po	po	k7c6
útocích	útok	k1gInPc6
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2001	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
seriál	seriál	k1gInSc1
se	se	k3xPyFc4
odehrával	odehrávat	k5eAaImAgInS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
diváky	divák	k1gMnPc4
zajímal	zajímat	k5eAaImAgInS
vztah	vztah	k1gInSc1
Rachel	Rachel	k1gInSc1
a	a	k8xC
Rosse	Rosse	k1gFnSc1
jako	jako	k8xC,k8xS
centrální	centrální	k2eAgFnSc1d1
zápletka	zápletka	k1gFnSc1
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
i	i	k9
závěr	závěr	k1gInSc4
seriálu	seriál	k1gInSc2
<g/>
,	,	kIx,
byť	byť	k8xS
kritici	kritik	k1gMnPc1
upozorňovali	upozorňovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gNnSc4
finální	finální	k2eAgNnSc4d1
obnovení	obnovení	k1gNnSc4
partnerského	partnerský	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
ze	z	k7c2
závěrečné	závěrečný	k2eAgFnSc2d1
epizody	epizoda	k1gFnSc2
ne	ne	k9
zcela	zcela	k6eAd1
respektovalo	respektovat	k5eAaImAgNnS
vývoj	vývoj	k1gInSc4
charakterů	charakter	k1gInPc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
spíše	spíše	k9
„	„	k?
<g/>
na	na	k7c6
přání	přání	k1gNnSc6
fanoušků	fanoušek	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kulturolog	Kulturolog	k1gMnSc1
David	David	k1gMnSc1
Katzman	Katzman	k1gMnSc1
upozornil	upozornit	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
seriál	seriál	k1gInSc1
zobrazil	zobrazit	k5eAaPmAgInS
zlom	zlom	k1gInSc4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
americké	americký	k2eAgFnSc2d1
a	a	k8xC
celé	celý	k2eAgFnSc2d1
západní	západní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrdinové	Hrdinové	k2eAgInPc1d1
totiž	totiž	k8xC
žijí	žít	k5eAaImIp3nP
životním	životní	k2eAgInSc7d1
stylem	styl	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
pevným	pevný	k2eAgInSc7d1
bodem	bod	k1gInSc7
není	být	k5eNaImIp3nS
partnerský	partnerský	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
skupina	skupina	k1gFnSc1
přátel	přítel	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
nahrazuje	nahrazovat	k5eAaImIp3nS
tradiční	tradiční	k2eAgFnSc4d1
rodinu	rodina	k1gFnSc4
a	a	k8xC
člověk	člověk	k1gMnSc1
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
sám	sám	k3xTgMnSc1
konstruuje	konstruovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Autorka	autorka	k1gFnSc1
seriálu	seriál	k1gInSc2
Marta	Marta	k1gFnSc1
Kauffmanová	Kauffmanová	k1gFnSc1
potvrdila	potvrdit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
zmapovat	zmapovat	k5eAaPmF
<g />
.	.	kIx.
</s>
<s hack="1">
onu	onen	k3xDgFnSc4
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pro	pro	k7c4
mladého	mladý	k2eAgMnSc4d1
člověka	člověk	k1gMnSc4
„	„	k?
<g/>
jsou	být	k5eAaImIp3nP
přátelé	přítel	k1gMnPc1
jeho	jeho	k3xOp3gFnSc7
rodinou	rodina	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
v	v	k7c6
postmoderní	postmoderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
rozšiřuje	rozšiřovat	k5eAaImIp3nS
za	za	k7c4
třicátý	třicátý	k4xOgInSc4
rok	rok	k1gInSc4
věku	věk	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
nebývalo	bývat	k5eNaImAgNnS
zvykem	zvyk	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
také	také	k6eAd1
vysvětlila	vysvětlit	k5eAaPmAgFnS
<g/>
,	,	kIx,
proč	proč	k6eAd1
nelze	lze	k6eNd1
s	s	k7c7
odstupem	odstup	k1gInSc7
času	čas	k1gInSc2
natočit	natočit	k5eAaBmF
film	film	k1gInSc4
na	na	k7c4
seriál	seriál	k1gInSc4
navazující	navazující	k2eAgInSc4d1
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6
Sexu	sex	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
o	o	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
v	v	k7c6
pravidelných	pravidelný	k2eAgInPc6d1
cyklech	cyklus	k1gInPc6
spekuluje	spekulovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
Přátel	přítel	k1gMnPc2
ovlivnil	ovlivnit	k5eAaPmAgInS
vývoj	vývoj	k1gInSc4
žánru	žánr	k1gInSc2
sitcom	sitcom	k1gInSc1
přesunem	přesun	k1gInSc7
důrazu	důraz	k1gInSc2
z	z	k7c2
rodinného	rodinný	k2eAgInSc2d1
života	život	k1gInSc2
na	na	k7c4
život	život	k1gInSc4
neformálních	formální	k2eNgFnPc2d1
skupin	skupina	k1gFnPc2
přátel	přítel	k1gMnPc2
(	(	kIx(
<g/>
započátý	započátý	k2eAgMnSc1d1
již	již	k6eAd1
Seinfeldem	Seinfeld	k1gInSc7
či	či	k8xC
Cheers	Cheers	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c7
jím	on	k3xPp3gNnSc7
ovlivněný	ovlivněný	k2eAgMnSc1d1
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
označován	označovat	k5eAaImNgInS
především	především	k9
seriál	seriál	k1gInSc1
Jak	jak	k6eAd1
jsem	být	k5eAaImIp1nS
potkal	potkat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Seriál	seriál	k1gInSc1
měl	mít	k5eAaImAgInS
i	i	k9
jeden	jeden	k4xCgInSc1
tzv.	tzv.	kA
spin-off	spin-off	k1gInSc4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
postava	postava	k1gFnSc1
Joeyiho	Joeyi	k1gMnSc2
osamostatnila	osamostatnit	k5eAaPmAgFnS
a	a	k8xC
získala	získat	k5eAaPmAgFnS
vlastní	vlastní	k2eAgInSc4d1
sitcom	sitcom	k1gInSc4
Joey	Joea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NBC	NBC	kA
ho	on	k3xPp3gNnSc4
nasadila	nasadit	k5eAaPmAgFnS
na	na	k7c4
stejný	stejný	k2eAgInSc4d1
čas	čas	k1gInSc4
jako	jako	k8xS,k8xC
Přátele	přítel	k1gMnPc4
a	a	k8xC
úvodní	úvodní	k2eAgFnSc4d1
epizodu	epizoda	k1gFnSc4
skutečně	skutečně	k6eAd1
sledovalo	sledovat	k5eAaImAgNnS
jen	jen	k9
o	o	k7c4
málo	málo	k6eAd1
méně	málo	k6eAd2
diváků	divák	k1gMnPc2
než	než	k8xS
v	v	k7c6
průměru	průměr	k1gInSc6
sledovalo	sledovat	k5eAaImAgNnS
Přátele	přítel	k1gMnPc4
(	(	kIx(
<g/>
cca	cca	kA
18	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
sledovanost	sledovanost	k1gFnSc1
rychle	rychle	k6eAd1
klesala	klesat	k5eAaImAgFnS
až	až	k9
k	k	k7c3
sedmi	sedm	k4xCc3
milionům	milion	k4xCgInPc3
<g/>
,	,	kIx,
načež	načež	k6eAd1
byl	být	k5eAaImAgInS
seriál	seriál	k1gInSc1
zastaven	zastavit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
autorů	autor	k1gMnPc2
Přátel	přítel	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
na	na	k7c6
scénáři	scénář	k1gInSc6
k	k	k7c3
Joeymu	Joeym	k1gInSc3
odmítli	odmítnout	k5eAaPmAgMnP
podílet	podílet	k5eAaImF
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
nepochopením	nepochopení	k1gNnSc7
postavy	postava	k1gFnSc2
Joeyho	Joey	k1gMnSc2
v	v	k7c6
Přátelích	přítel	k1gMnPc6
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
nepříjemným	příjemný	k2eNgInSc7d1
charakterovým	charakterový	k2eAgInSc7d1
posunem	posun	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
ocenění	ocenění	k1gNnSc2
a	a	k8xC
nominací	nominace	k1gFnSc7
seriálu	seriál	k1gInSc2
Přátelé	přítel	k1gMnPc5
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
vysílání	vysílání	k1gNnSc2
získal	získat	k5eAaPmAgInS
seriál	seriál	k1gInSc1
několik	několik	k4yIc1
cen	cena	k1gFnPc2
Emmy	Emma	k1gFnSc2
a	a	k8xC
některá	některý	k3yIgNnPc4
další	další	k2eAgNnPc4d1
ocenění	ocenění	k1gNnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Zlatý	zlatý	k2eAgInSc4d1
globus	globus	k1gInSc4
nebo	nebo	k8xC
ceny	cena	k1gFnPc4
SAG	SAG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
63	#num#	k4
nominací	nominace	k1gFnPc2
na	na	k7c4
ceny	cena	k1gFnPc4
Emmy	Emma	k1gFnSc2
jich	on	k3xPp3gFnPc2
seriál	seriál	k1gInSc1
proměnil	proměnit	k5eAaPmAgInS
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jennifer	Jennifer	k1gInSc1
Aniston	Aniston	k1gInSc1
a	a	k8xC
Lisa	Lisa	k1gFnSc1
Kudrow	Kudrow	k1gMnPc2
jsou	být	k5eAaImIp3nP
jediné	jediný	k2eAgFnPc1d1
herečky	herečka	k1gFnPc1
v	v	k7c6
seriálu	seriál	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vyhrály	vyhrát	k5eAaPmAgFnP
Emmy	Emma	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Ceny	cena	k1gFnPc1
Emmy	Emma	k1gFnSc2
</s>
<s>
2008	#num#	k4
–	–	k?
Nejvzpomínanější	vzpomínaný	k2eAgInSc1d3
televizní	televizní	k2eAgInSc1d1
moment	moment	k1gInSc1
–	–	k?
komedie	komedie	k1gFnSc2
(	(	kIx(
<g/>
za	za	k7c4
díl	díl	k1gInSc4
„	„	k?
<g/>
Rossovo	Rossův	k2eAgNnSc4d1
odhalení	odhalení	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgFnSc1d3
hostující	hostující	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
–	–	k?
Christina	Christin	k2eAgNnSc2d1
Applegate	Applegat	k1gMnSc5
</s>
<s>
2002	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgInSc4d3
televizní	televizní	k2eAgInSc4d1
miniseriál	miniseriál	k1gInSc4
nebo	nebo	k8xC
televizní	televizní	k2eAgInSc4d1
film	film	k1gInSc4
</s>
<s>
2002	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgInSc4d3
televizní	televizní	k2eAgInSc4d1
seriál	seriál	k1gInSc4
–	–	k?
kategorie	kategorie	k1gFnSc2
muzikál	muzikál	k1gInSc1
a	a	k8xC
veselohra	veselohra	k1gFnSc1
–	–	k?
nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
–	–	k?
Jennifer	Jennifer	k1gInSc1
Aniston	Aniston	k1gInSc1
</s>
<s>
2000	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgMnSc1d3
hostující	hostující	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
–	–	k?
Bruce	Bruce	k1gFnSc2
Willis	Willis	k1gFnSc2
</s>
<s>
1998	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgInSc4d3
televizní	televizní	k2eAgInSc4d1
seriál	seriál	k1gInSc4
–	–	k?
kategorie	kategorie	k1gFnSc2
muzikál	muzikál	k1gInSc1
a	a	k8xC
veselohra	veselohra	k1gFnSc1
–	–	k?
nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
–	–	k?
Lisa	Lisa	k1gFnSc1
Kudrow	Kudrow	k1gFnSc1
</s>
<s>
1996	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgFnSc1d3
režie	režie	k1gFnSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
–	–	k?
Michael	Michael	k1gMnSc1
Lembeck	Lembeck	k1gMnSc1
(	(	kIx(
<g/>
za	za	k7c4
díl	díl	k1gInSc4
„	„	k?
<g/>
Joeyho	Joey	k1gMnSc2
ctitelka	ctitelka	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1
globus	globus	k1gInSc1
</s>
<s>
2003	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgInSc4d3
televizní	televizní	k2eAgInSc4d1
seriál	seriál	k1gInSc4
–	–	k?
kategorie	kategorie	k1gFnSc2
muzikál	muzikál	k1gInSc1
a	a	k8xC
veselohra	veselohra	k1gFnSc1
–	–	k?
nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
–	–	k?
Jennifer	Jennifer	k1gInSc1
Aniston	Aniston	k1gInSc1
</s>
<s>
Screen	Screen	k2eAgInSc1d1
Actors	Actors	k1gInSc1
Guild	Guilda	k1gFnPc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
2000	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgFnSc1d3
herečka	herečka	k1gFnSc1
v	v	k7c6
komediálním	komediální	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
–	–	k?
Lisa	Lisa	k1gFnSc1
Kudrow	Kudrow	k1gFnSc1
</s>
<s>
1996	#num#	k4
–	–	k?
Nejlepší	dobrý	k2eAgFnSc1d3
herecká	herecký	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
–	–	k?
J.	J.	kA
Aniston	Aniston	k1gInSc1
<g/>
,	,	kIx,
C.	C.	kA
Cox-Arquette	Cox-Arquett	k1gInSc5
<g/>
,	,	kIx,
L.	L.	kA
Kudrow	Kudrow	k1gFnSc4
<g/>
,	,	kIx,
M.	M.	kA
LeBlanc	LeBlanc	k1gInSc1
<g/>
,	,	kIx,
M.	M.	kA
Perry	Perra	k1gFnPc1
<g/>
,	,	kIx,
D.	D.	kA
Schwimmer	Schwimmer	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Friends	Friendsa	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Estimated	Estimated	k1gMnSc1
51.1	51.1	k4
<g/>
M	M	kA
Tune	Tun	k1gInSc2
in	in	k?
for	forum	k1gNnPc2
'	'	kIx"
<g/>
Friends	Friends	k1gInSc1
<g/>
'	'	kIx"
Finale	Finala	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fox	fox	k1gInSc1
News	News	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
Herci	herc	k1gInPc7
/	/	kIx~
Postavy	postav	k1gInPc7
(	(	kIx(
<g/>
ženy	žena	k1gFnPc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friends	Friends	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Phoebe	Phoeb	k1gInSc5
Buffay	Buffa	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friends	Friends	k1gInSc1
Finale	Finala	k1gFnSc3
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Herci	herc	k1gInPc7
/	/	kIx~
Postavy	postava	k1gFnPc4
(	(	kIx(
<g/>
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Friends	Friends	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Přátelé	přítel	k1gMnPc1
–	–	k?
postava	postava	k1gFnSc1
Chandler	Chandler	k1gInSc1
Bing	bingo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serialzone	Serialzon	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Jennifer	Jennifer	k1gInSc1
Aniston	Aniston	k1gInSc1
in	in	k?
Famous	Famous	k1gInSc1
Friends	Friends	k1gInSc1
Hairstyle	Hairstyl	k1gInSc5
Called	Called	k1gMnSc1
"	"	kIx"
<g/>
The	The	k1gMnSc1
Rachel	Rachel	k1gMnSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beautiful	Beautiful	k1gInSc1
Hairstyles	Hairstyles	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://edition.cnn.com/2010/WORLD/asiapcf/07/02/friends.china.central.perk/index.html	http://edition.cnn.com/2010/WORLD/asiapcf/07/02/friends.china.central.perk/index.html	k1gMnSc1
<g/>
↑	↑	k?
Rodman	Rodman	k1gMnSc1
<g/>
,	,	kIx,
Sarah	Sarah	k1gFnSc1
(	(	kIx(
<g/>
May	May	k1gMnSc1
7	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Six	Six	k1gFnSc1
pals	palsa	k1gFnPc2
depart	departa	k1gFnPc2
on	on	k3xPp3gMnSc1
a	a	k8xC
classy	class	k1gInPc4
note	note	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boston	Boston	k1gInSc1
Herald.	Herald.	k1gFnSc2
p.	p.	k?
3	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Katzman	Katzman	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
M.	M.	kA
(	(	kIx(
<g/>
Summer	Summer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
TV	TV	kA
and	and	k?
American	American	k1gInSc1
Culture	Cultur	k1gMnSc5
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Studies	Studies	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
39	#num#	k4
<g/>
:	:	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.3news.co.nz	www.3news.co.nz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.theage.com.au/news/tv--radio/friendly-art-of-funny/2006/12/05/1165080950967.html?page=fullpage#contentSwap2	http://www.theage.com.au/news/tv--radio/friendly-art-of-funny/2006/12/05/1165080950967.html?page=fullpage#contentSwap2	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Přátelé	přítel	k1gMnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Přátelé	přítel	k1gMnPc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Přátelé	přítel	k1gMnPc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Přátelé	přítel	k1gMnPc1
Postavy	postava	k1gFnSc2
</s>
<s>
Rachel	Rachel	k1gInSc1
Greenová	Greenová	k1gFnSc1
•	•	k?
Monica	Monica	k1gFnSc1
Gellerová	Gellerová	k1gFnSc1
•	•	k?
Phoebe	Phoeb	k1gInSc5
Bufetová	bufetový	k2eAgNnPc1d1
•	•	k?
Joey	Joey	k1gInPc7
Tribbiani	Tribbiaň	k1gFnSc3
•	•	k?
Chandler	Chandler	k1gMnSc1
Bing	bingo	k1gNnPc2
•	•	k?
Ross	Ross	k1gInSc4
Geller	Geller	k1gInSc1
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Jennifer	Jennifer	k1gInSc1
Aniston	Aniston	k1gInSc1
•	•	k?
Courteney	Courtenea	k1gFnSc2
Cox	Cox	k1gMnSc7
•	•	k?
Lisa	Lisa	k1gFnSc1
Kudrow	Kudrow	k1gFnSc2
•	•	k?
Matt	Matt	k2eAgInSc1d1
LeBlanc	LeBlanc	k1gInSc1
•	•	k?
Matthew	Matthew	k1gMnSc2
Perry	Perra	k1gMnSc2
•	•	k?
David	David	k1gMnSc1
Schwimmer	Schwimmer	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
seznam	seznam	k1gInSc1
postav	postav	k1gInSc1
•	•	k?
seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
•	•	k?
seznam	seznam	k1gInSc1
ocenění	ocenění	k1gNnSc2
a	a	k8xC
nominací	nominace	k1gFnPc2
•	•	k?
„	„	k?
<g/>
I	I	kA
<g/>
'	'	kIx"
<g/>
ll	ll	k?
Be	Be	k1gMnSc5
There	Ther	k1gMnSc5
for	forum	k1gNnPc2
You	You	k1gMnSc7
<g/>
“	“	k?
•	•	k?
Joey	Joea	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4719613-0	4719613-0	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
205795873	#num#	k4
</s>
