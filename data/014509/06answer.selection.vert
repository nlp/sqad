<s>
V	v	k7c6
první	první	k4xOgFnSc6
sérii	série	k1gFnSc6
se	se	k3xPyFc4
představuje	představovat	k5eAaImIp3nS
šest	šest	k4xCc1
hlavních	hlavní	k2eAgFnPc2d1
postav	postava	k1gFnPc2
<g/>
:	:	kIx,
Rachel	Rachel	k1gInSc1
Greenová	Greenová	k1gFnSc1
<g/>
,	,	kIx,
servírka	servírka	k1gFnSc1
se	s	k7c7
zálibou	záliba	k1gFnSc7
v	v	k7c6
módě	móda	k1gFnSc6
<g/>
;	;	kIx,
Monica	Monic	k2eAgFnSc1d1
Gellerová	Gellerová	k1gFnSc1
<g/>
,	,	kIx,
profesionální	profesionální	k2eAgFnSc1d1
kuchařka	kuchařka	k1gFnSc1
<g/>
;	;	kIx,
její	její	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
Ross	Ross	k1gInSc1
Geller	Geller	k1gInSc1
<g/>
,	,	kIx,
doktor	doktor	k1gMnSc1
paleontologie	paleontologie	k1gFnSc2
<g/>
;	;	kIx,
Phoebe	Phoeb	k1gInSc5
Bufetová	bufetový	k2eAgFnSc1d1
<g/>
,	,	kIx,
masérka	masérka	k1gFnSc1
a	a	k8xC
písničkářka	písničkářka	k1gFnSc1
<g/>
;	;	kIx,
Joey	Joea	k1gFnPc1
Tribbiani	Tribbiaň	k1gFnSc3
<g/>
,	,	kIx,
nepříliš	příliš	k6eNd1
úspěšný	úspěšný	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
Chandler	Chandler	k1gInSc1
Bing	bingo	k1gNnPc2
<g/>
,	,	kIx,
Rossův	Rossův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
z	z	k7c2
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
pracuje	pracovat	k5eAaImIp3nS
pro	pro	k7c4
neznámou	známý	k2eNgFnSc4d1
firmu	firma	k1gFnSc4
<g/>
.	.	kIx.
</s>