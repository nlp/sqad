<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
podoba	podoba	k1gFnSc1	podoba
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
;	;	kIx,	;
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
čeština	čeština	k1gFnSc1	čeština
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
čistě	čistě	k6eAd1	čistě
spisovná	spisovný	k2eAgFnSc1d1	spisovná
nebo	nebo	k8xC	nebo
prostě	prostě	k6eAd1	prostě
spisovná	spisovný	k2eAgFnSc1d1	spisovná
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
veškerý	veškerý	k3xTgInSc4	veškerý
veřejný	veřejný	k2eAgInSc4d1	veřejný
styk	styk	k1gInSc4	styk
(	(	kIx(	(
<g/>
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
tvary	tvar	k1gInPc4	tvar
píši	psát	k5eAaImIp1nS	psát
<g/>
,	,	kIx,	,
děkují	děkovat	k5eAaImIp3nP	děkovat
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
a	a	k8xC	a
úřední	úřední	k2eAgFnSc4d1	úřední
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
při	při	k7c6	při
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
a	a	k8xC	a
úředních	úřední	k2eAgFnPc6d1	úřední
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
v	v	k7c6	v
úředních	úřední	k2eAgInPc6d1	úřední
i	i	k8xC	i
jiných	jiný	k2eAgInPc6d1	jiný
dokumentech	dokument	k1gInPc6	dokument
<g/>
,	,	kIx,	,
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
veřejnoprávních	veřejnoprávní	k2eAgNnPc6d1	veřejnoprávní
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
i	i	k9	i
v	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
tištěných	tištěný	k2eAgFnPc6d1	tištěná
<g/>
,	,	kIx,	,
mluvených	mluvený	k2eAgFnPc6d1	mluvená
nebo	nebo	k8xC	nebo
internetových	internetový	k2eAgFnPc6d1	internetová
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
moravské	moravský	k2eAgFnSc2d1	Moravská
obecné	obecný	k2eAgFnSc2d1	obecná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
zachytila	zachytit	k5eAaPmAgFnS	zachytit
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Blahoslavova	Blahoslavův	k2eAgFnSc1d1	Blahoslavova
bible	bible	k1gFnSc1	bible
a	a	k8xC	a
zejména	zejména	k9	zejména
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
<g/>
.	.	kIx.	.
hovorovou	hovorový	k2eAgFnSc4d1	hovorová
(	(	kIx(	(
<g/>
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
<g/>
)	)	kIx)	)
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
volnější	volný	k2eAgFnSc1d2	volnější
forma	forma	k1gFnSc1	forma
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
s	s	k7c7	s
regionálními	regionální	k2eAgFnPc7d1	regionální
variantami	varianta	k1gFnPc7	varianta
<g/>
,	,	kIx,	,
připouštějící	připouštějící	k2eAgInPc1d1	připouštějící
některé	některý	k3yIgInPc1	některý
jevy	jev	k1gInPc1	jev
obecné	obecný	k2eAgFnSc2d1	obecná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
nejvýrazněji	výrazně	k6eAd3	výrazně
středočeskou	středočeský	k2eAgFnSc4d1	Středočeská
a	a	k8xC	a
méně	málo	k6eAd2	málo
již	již	k6eAd1	již
moravskou	moravský	k2eAgFnSc7d1	Moravská
a	a	k8xC	a
slezskou	slezský	k2eAgFnSc7d1	Slezská
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
určená	určený	k2eAgNnPc1d1	určené
pro	pro	k7c4	pro
běžný	běžný	k2eAgInSc4d1	běžný
neoficiální	oficiální	k2eNgInSc4d1	neoficiální
mluvený	mluvený	k2eAgInSc4d1	mluvený
styk	styk	k1gInSc4	styk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
připouští	připouštět	k5eAaImIp3nS	připouštět
tvary	tvar	k1gInPc4	tvar
píšu	psát	k5eAaImIp1nS	psát
<g/>
,	,	kIx,	,
děkujou	děkovat	k5eAaImIp3nP	děkovat
<g/>
,	,	kIx,	,
můžou	můžou	k?	můžou
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
soustavou	soustava	k1gFnSc7	soustava
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
psanou	psaný	k2eAgFnSc4d1	psaná
i	i	k8xC	i
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
podobu	podoba	k1gFnSc4	podoba
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
především	především	k6eAd1	především
určují	určovat	k5eAaImIp3nP	určovat
<g/>
:	:	kIx,	:
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
<g/>
,	,	kIx,	,
správnou	správný	k2eAgFnSc4d1	správná
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
správné	správný	k2eAgNnSc4d1	správné
tvoření	tvoření	k1gNnSc4	tvoření
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
ohýbání	ohýbání	k1gNnSc1	ohýbání
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
koncovkami	koncovka	k1gFnPc7	koncovka
<g/>
,	,	kIx,	,
přiměřený	přiměřený	k2eAgInSc1d1	přiměřený
způsob	způsob	k1gInSc1	způsob
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Dodržování	dodržování	k1gNnSc2	dodržování
těchto	tento	k3xDgNnPc2	tento
pravidel	pravidlo	k1gNnPc2	pravidlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
komunikaci	komunikace	k1gFnSc6	komunikace
závazné	závazný	k2eAgFnPc1d1	závazná
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
vyžadováno	vyžadovat	k5eAaImNgNnS	vyžadovat
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
pravidly	pravidlo	k1gNnPc7	pravidlo
firem	firma	k1gFnPc2	firma
apod.	apod.	kA	apod.
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
kodifikována	kodifikovat	k5eAaBmNgFnS	kodifikovat
v	v	k7c6	v
publikacích	publikace	k1gFnPc6	publikace
vydávaných	vydávaný	k2eAgFnPc6d1	vydávaná
Ústavem	ústav	k1gInSc7	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Pravidlech	pravidlo	k1gNnPc6	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
Slovníku	slovník	k1gInSc2	slovník
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
v	v	k7c4	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
asi	asi	k9	asi
192	[number]	k4	192
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovníku	slovník	k1gInSc2	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
(	(	kIx(	(
<g/>
asi	asi	k9	asi
50	[number]	k4	50
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Akademickém	akademický	k2eAgInSc6d1	akademický
slovníku	slovník	k1gInSc6	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
významů	význam	k1gInPc2	význam
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Největší	veliký	k2eAgInSc1d3	veliký
existující	existující	k2eAgInSc1d1	existující
český	český	k2eAgInSc1d1	český
výkladový	výkladový	k2eAgInSc1d1	výkladový
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
Příruční	příruční	k2eAgInSc1d1	příruční
slovník	slovník	k1gInSc1	slovník
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1935-1957	[number]	k4	1935-1957
(	(	kIx(	(
<g/>
asi	asi	k9	asi
250	[number]	k4	250
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
v	v	k7c6	v
9	[number]	k4	9
svazcích	svazek	k1gInPc6	svazek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xC	jako
poněkud	poněkud	k6eAd1	poněkud
zastarávající	zastarávající	k2eAgFnSc1d1	zastarávající
<g/>
.	.	kIx.	.
</s>
<s>
Periodikem	periodikum	k1gNnSc7	periodikum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhají	probíhat	k5eAaImIp3nP	probíhat
seriózní	seriózní	k2eAgFnPc1d1	seriózní
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
spisovné	spisovný	k2eAgFnSc3d1	spisovná
podobě	podoba	k1gFnSc3	podoba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Naše	náš	k3xOp1gFnSc1	náš
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
časopisů	časopis	k1gInPc2	časopis
vydávaných	vydávaný	k2eAgNnPc2d1	vydávané
ÚJČ	ÚJČ	kA	ÚJČ
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
slovníky	slovník	k1gInPc1	slovník
Slovník	slovník	k1gInSc1	slovník
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
Mimo	mimo	k7c4	mimo
spisovnou	spisovný	k2eAgFnSc4d1	spisovná
češtinu	čeština	k1gFnSc4	čeština
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
útvary	útvar	k1gInPc4	útvar
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
nářečí	nářečí	k1gNnSc2	nářečí
profesní	profesní	k2eAgFnSc1d1	profesní
mluva	mluva	k1gFnSc1	mluva
slang	slang	k1gInSc4	slang
argot	argot	k1gInSc1	argot
</s>
