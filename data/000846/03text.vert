<s>
Kapavka	kapavka	k1gFnSc1	kapavka
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
gonorrhoea	gonorrhoea	k1gFnSc1	gonorrhoea
či	či	k8xC	či
hovorově	hovorově	k6eAd1	hovorově
tripl	tripl	k1gInSc1	tripl
nebo	nebo	k8xC	nebo
slangově	slangově	k6eAd1	slangově
kapela	kapela	k1gFnSc1	kapela
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
sexuálně	sexuálně	k6eAd1	sexuálně
přenosné	přenosný	k2eAgFnSc3d1	přenosná
nemoci	nemoc	k1gFnSc3	nemoc
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ji	on	k3xPp3gFnSc4	on
gramnegativní	gramnegativní	k2eAgFnSc1d1	gramnegativní
bakterie	bakterie	k1gFnSc1	bakterie
Neisseria	Neisserium	k1gNnSc2	Neisserium
gonorrhoeae	gonorrhoea	k1gInSc2	gonorrhoea
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
charakter	charakter	k1gInSc4	charakter
hnisavého	hnisavý	k2eAgInSc2d1	hnisavý
zánětu	zánět	k1gInSc2	zánět
sliznic	sliznice	k1gFnPc2	sliznice
vylučovacích	vylučovací	k2eAgInPc2d1	vylučovací
a	a	k8xC	a
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
konečník	konečník	k1gInSc1	konečník
<g/>
,	,	kIx,	,
hltan	hltan	k1gInSc1	hltan
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spojivky	spojivka	k1gFnPc1	spojivka
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
pojmenování	pojmenování	k1gNnSc1	pojmenování
gonorrhoea	gonorrhoea	k1gFnSc1	gonorrhoea
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
slov	slovo	k1gNnPc2	slovo
gonos	gonos	k1gInSc1	gonos
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
semeno	semeno	k1gNnSc4	semeno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
rhoe	rhoe	k6eAd1	rhoe
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
téci	téct	k5eAaImF	téct
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
odráží	odrážet	k5eAaImIp3nS	odrážet
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
výměšek	výměšek	k1gInSc1	výměšek
hlenu	hlen	k1gInSc2	hlen
byl	být	k5eAaImAgInS	být
chybně	chybně	k6eAd1	chybně
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
semeno	semeno	k1gNnSc1	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starších	starý	k2eAgInPc6d2	starší
záznamech	záznam	k1gInPc6	záznam
odkazovala	odkazovat	k5eAaImAgFnS	odkazovat
etymologie	etymologie	k1gFnSc1	etymologie
slova	slovo	k1gNnSc2	slovo
k	k	k7c3	k
biblickému	biblický	k2eAgNnSc3d1	biblické
městu	město	k1gNnSc3	město
Gomoria	Gomorium	k1gNnSc2	Gomorium
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
Gomora	Gomora	k1gFnSc1	Gomora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
tak	tak	k9	tak
z	z	k7c2	z
lidové	lidový	k2eAgFnSc2d1	lidová
asociace	asociace	k1gFnSc2	asociace
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
výraz	výraz	k1gInSc1	výraz
kapavka	kapavka	k1gFnSc1	kapavka
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
kapat	kapat	k5eAaImF	kapat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
onomatopoického	onomatopoický	k2eAgInSc2d1	onomatopoický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Hovorové	hovorový	k2eAgNnSc1d1	hovorové
označení	označení	k1gNnSc1	označení
tripl	tripl	k1gInSc1	tripl
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
Tripper	Tripper	k1gMnSc1	Tripper
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
nemoc	nemoc	k1gFnSc4	nemoc
rovněž	rovněž	k9	rovněž
odvozeného	odvozený	k2eAgNnSc2d1	odvozené
ze	z	k7c2	z
slovesa	sloveso	k1gNnSc2	sloveso
trippen	trippen	k2eAgMnSc1d1	trippen
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
kapat	kapat	k5eAaImF	kapat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
CDC	CDC	kA	CDC
(	(	kIx(	(
<g/>
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Disease	Diseasa	k1gFnSc3	Diseasa
Control	Control	k1gInSc1	Control
and	and	k?	and
Prevention	Prevention	k1gInSc1	Prevention
-	-	kIx~	-
Středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
nemocemi	nemoc	k1gFnPc7	nemoc
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
prevencí	prevence	k1gFnSc7	prevence
<g/>
)	)	kIx)	)
v	v	k7c6	v
USA	USA	kA	USA
358995	[number]	k4	358995
případů	případ	k1gInPc2	případ
kapavky	kapavka	k1gFnSc2	kapavka
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
75	[number]	k4	75
procent	procento	k1gNnPc2	procento
všech	všecek	k3xTgMnPc2	všecek
zaznamenaných	zaznamenaný	k2eAgMnPc2d1	zaznamenaný
případů	případ	k1gInPc2	případ
kapavky	kapavka	k1gFnSc2	kapavka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
-	-	kIx~	-
29	[number]	k4	29
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejpostiženější	postižený	k2eAgFnSc1d3	nejpostiženější
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
-	-	kIx~	-
19	[number]	k4	19
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
pak	pak	k6eAd1	pak
věková	věkový	k2eAgFnSc1d1	věková
kategorie	kategorie	k1gFnSc1	kategorie
20	[number]	k4	20
-	-	kIx~	-
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomové	ekonom	k1gMnPc1	ekonom
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
roční	roční	k2eAgInPc1d1	roční
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
kapavky	kapavka	k1gFnSc2	kapavka
a	a	k8xC	a
souvisejících	související	k2eAgFnPc2d1	související
komplikací	komplikace	k1gFnPc2	komplikace
činí	činit	k5eAaImIp3nS	činit
téměř	téměř	k6eAd1	téměř
1,1	[number]	k4	1,1
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Kapavka	kapavka	k1gFnSc1	kapavka
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
šíří	šířit	k5eAaImIp3nS	šířit
při	při	k7c6	při
pohlavním	pohlavní	k2eAgInSc6d1	pohlavní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Infikované	infikovaný	k2eAgFnPc1d1	infikovaná
ženy	žena	k1gFnPc1	žena
mohou	moct	k5eAaImIp3nP	moct
též	též	k9	též
přenést	přenést	k5eAaPmF	přenést
kapavku	kapavka	k1gFnSc4	kapavka
na	na	k7c4	na
novorozence	novorozenec	k1gMnSc4	novorozenec
během	během	k7c2	během
porodu	porod	k1gInSc2	porod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
projeví	projevit	k5eAaPmIp3nS	projevit
jako	jako	k9	jako
oční	oční	k2eAgFnSc2d1	oční
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
komplikace	komplikace	k1gFnSc1	komplikace
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
prakticky	prakticky	k6eAd1	prakticky
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
novorozenci	novorozenec	k1gMnPc1	novorozenec
narozeni	narozen	k2eAgMnPc1d1	narozen
v	v	k7c6	v
porodnici	porodnice	k1gFnSc6	porodnice
mají	mít	k5eAaImIp3nP	mít
preventivně	preventivně	k6eAd1	preventivně
ošetřeny	ošetřen	k2eAgFnPc1d1	ošetřena
oči	oko	k1gNnPc4	oko
vkápnutím	vkápnutí	k1gNnPc3	vkápnutí
Opthalmo	Opthalma	k1gFnSc5	Opthalma
Septonexu	septonex	k1gInSc3	septonex
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
činí	činit	k5eAaImIp3nS	činit
obvykle	obvykle	k6eAd1	obvykle
2-14	[number]	k4	2-14
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
obvykle	obvykle	k6eAd1	obvykle
objevují	objevovat	k5eAaImIp3nP	objevovat
mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
dnem	den	k1gInSc7	den
po	po	k7c4	po
infikaci	infikace	k1gFnSc4	infikace
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
asymptomatičtí	asymptomatický	k2eAgMnPc1d1	asymptomatický
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
<g/>
)	)	kIx)	)
i	i	k9	i
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
stykem	styk	k1gInSc7	styk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejúčinnější	účinný	k2eAgFnSc7d3	nejúčinnější
prevencí	prevence	k1gFnSc7	prevence
věrnost	věrnost	k1gFnSc4	věrnost
jedinému	jediný	k2eAgInSc3d1	jediný
partnerovi	partner	k1gMnSc3	partner
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
pečlivý	pečlivý	k2eAgInSc1d1	pečlivý
výběr	výběr	k1gInSc1	výběr
sexuálních	sexuální	k2eAgMnPc2d1	sexuální
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
účinně	účinně	k6eAd1	účinně
snižuje	snižovat	k5eAaImIp3nS	snižovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
přenosu	přenos	k1gInSc2	přenos
správné	správný	k2eAgNnSc1d1	správné
užití	užití	k1gNnSc1	užití
bariérové	bariérový	k2eAgFnSc2d1	bariérová
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kondom	kondom	k1gInSc1	kondom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
infikované	infikovaný	k2eAgNnSc1d1	infikované
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tubulární	tubulární	k2eAgInSc1d1	tubulární
epitel	epitel	k1gInSc1	epitel
močové	močový	k2eAgFnSc2d1	močová
trubice	trubice	k1gFnSc2	trubice
a	a	k8xC	a
endocervix	endocervix	k1gInSc1	endocervix
(	(	kIx(	(
<g/>
výstelka	výstelka	k1gFnSc1	výstelka
děložního	děložní	k2eAgNnSc2d1	děložní
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
a	a	k8xC	a
vylučovacích	vylučovací	k2eAgInPc2d1	vylučovací
orgánů	orgán	k1gInPc2	orgán
se	se	k3xPyFc4	se
bakteriím	bakterie	k1gFnPc3	bakterie
daří	dařit	k5eAaImIp3nP	dařit
v	v	k7c6	v
konečníku	konečník	k1gInSc6	konečník
<g/>
,	,	kIx,	,
orofarynxu	orofarynx	k1gInSc2	orofarynx
(	(	kIx(	(
<g/>
ústní	ústní	k2eAgFnSc4d1	ústní
část	část	k1gFnSc4	část
hltanu	hltan	k1gInSc2	hltan
<g/>
)	)	kIx)	)
a	a	k8xC	a
spojivkách	spojivka	k1gFnPc6	spojivka
(	(	kIx(	(
<g/>
očí	oko	k1gNnPc2	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vulva	vulva	k1gFnSc1	vulva
a	a	k8xC	a
vagina	vagina	k1gFnSc1	vagina
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
ušetřeny	ušetřen	k2eAgFnPc1d1	ušetřena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
pokryté	pokrytý	k2eAgMnPc4d1	pokrytý
vrstevními	vrstevní	k2eAgFnPc7d1	vrstevní
epitelními	epitelní	k2eAgFnPc7d1	epitelní
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
prvním	první	k4xOgNnSc7	první
místem	místo	k1gNnSc7	místo
infekce	infekce	k1gFnSc2	infekce
obvykle	obvykle	k6eAd1	obvykle
děložní	děložní	k2eAgNnSc4d1	děložní
hrdlo	hrdlo	k1gNnSc4	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
nemoc	nemoc	k1gFnSc1	nemoc
může	moct	k5eAaImIp3nS	moct
rozšířit	rozšířit	k5eAaPmF	rozšířit
do	do	k7c2	do
dělohy	děloha	k1gFnSc2	děloha
a	a	k8xC	a
vejcovodů	vejcovod	k1gInPc2	vejcovod
a	a	k8xC	a
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
tzv.	tzv.	kA	tzv.
pánevní	pánevní	k2eAgFnSc4d1	pánevní
zánětovou	zánětový	k2eAgFnSc4d1	zánětová
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
příčin	příčina	k1gFnPc2	příčina
postihuje	postihovat	k5eAaImIp3nS	postihovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
milión	milión	k4xCgInSc4	milión
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
USA	USA	kA	USA
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
neplodnost	neplodnost	k1gFnSc1	neplodnost
(	(	kIx(	(
<g/>
postiženo	postihnout	k5eAaPmNgNnS	postihnout
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
či	či	k8xC	či
mimoděložní	mimoděložní	k2eAgNnSc1d1	mimoděložní
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnPc4	infekce
kapavkou	kapavka	k1gFnSc7	kapavka
signifikantně	signifikantně	k6eAd1	signifikantně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
infikování	infikování	k1gNnSc2	infikování
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
patrně	patrně	k6eAd1	patrně
o	o	k7c4	o
důsledek	důsledek	k1gInSc4	důsledek
oslabení	oslabení	k1gNnSc2	oslabení
a	a	k8xC	a
narušení	narušení	k1gNnSc2	narušení
povrchu	povrch	k1gInSc2	povrch
sliznic	sliznice	k1gFnPc2	sliznice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
kapavka	kapavka	k1gFnSc1	kapavka
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
50	[number]	k4	50
<g/>
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
s	s	k7c7	s
kapavkou	kapavka	k1gFnSc7	kapavka
je	být	k5eAaImIp3nS	být
asymptomatických	asymptomatický	k2eAgInPc6d1	asymptomatický
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jsou	být	k5eAaImIp3nP	být
symptomy	symptom	k1gInPc1	symptom
mírné	mírný	k2eAgInPc1d1	mírný
či	či	k8xC	či
atypické	atypický	k2eAgInPc1d1	atypický
<g/>
.	.	kIx.	.
</s>
<s>
Pacientka	pacientka	k1gFnSc1	pacientka
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
stěžovat	stěžovat	k5eAaImF	stěžovat
na	na	k7c4	na
vaginální	vaginální	k2eAgInSc4d1	vaginální
výtok	výtok	k1gInSc4	výtok
<g/>
,	,	kIx,	,
dysurii	dysurie	k1gFnSc4	dysurie
(	(	kIx(	(
<g/>
obtíže	obtíž	k1gFnPc4	obtíž
při	při	k7c6	při
močení	močení	k1gNnSc6	močení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezimenstruační	mezimenstruační	k2eAgNnSc4d1	mezimenstruační
krvácení	krvácení	k1gNnSc4	krvácení
nebo	nebo	k8xC	nebo
krvácení	krvácení	k1gNnSc4	krvácení
po	po	k7c6	po
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
<s>
Cervix	Cervix	k1gInSc1	Cervix
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jevit	jevit	k5eAaImF	jevit
normálně	normálně	k6eAd1	normálně
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
cervicitida	cervicitida	k1gFnSc1	cervicitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
děložního	děložní	k2eAgNnSc2d1	děložní
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
)	)	kIx)	)
s	s	k7c7	s
hlenohnisavým	hlenohnisavý	k2eAgInSc7d1	hlenohnisavý
výpotkem	výpotek	k1gInSc7	výpotek
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižení	k1gNnSc1	postižení
močové	močový	k2eAgFnSc2d1	močová
trubice	trubice	k1gFnSc2	trubice
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
slabou	slabý	k2eAgFnSc4d1	slabá
dysurii	dysurie	k1gFnSc4	dysurie
<g/>
,	,	kIx,	,
hnisavou	hnisavý	k2eAgFnSc4d1	hnisavá
sekreci	sekrece	k1gFnSc4	sekrece
a	a	k8xC	a
časté	častý	k2eAgNnSc4d1	časté
nutkání	nutkání	k1gNnSc4	nutkání
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
uretritidy	uretritida	k1gFnSc2	uretritida
(	(	kIx(	(
<g/>
zánětu	zánět	k1gInSc2	zánět
močové	močový	k2eAgFnSc2d1	močová
trubice	trubice	k1gFnSc2	trubice
<g/>
)	)	kIx)	)
a	a	k8xC	a
cervicitidy	cervicitis	k1gFnPc1	cervicitis
při	při	k7c6	při
vyšetření	vyšetření	k1gNnSc6	vyšetření
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jasná	jasný	k2eAgFnSc1d1	jasná
vodítko	vodítko	k1gNnSc4	vodítko
pro	pro	k7c4	pro
diagnózu	diagnóza	k1gFnSc4	diagnóza
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obě	dva	k4xCgNnPc1	dva
místa	místo	k1gNnPc1	místo
bývají	bývat	k5eAaImIp3nP	bývat
infikována	infikovat	k5eAaBmNgNnP	infikovat
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
pacientek	pacientka	k1gFnPc2	pacientka
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilejší	pokročilý	k2eAgInPc1d2	pokročilejší
symptomy	symptom	k1gInPc1	symptom
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
indikovat	indikovat	k5eAaBmF	indikovat
vývoj	vývoj	k1gInSc4	vývoj
pánevní	pánevní	k2eAgFnSc2d1	pánevní
zánětové	zánětový	k2eAgFnSc2d1	zánětová
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
křeče	křeč	k1gFnPc4	křeč
a	a	k8xC	a
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
krvácení	krvácení	k1gNnSc4	krvácení
mezi	mezi	k7c7	mezi
menstruačními	menstruační	k2eAgFnPc7d1	menstruační
periodami	perioda	k1gFnPc7	perioda
<g/>
,	,	kIx,	,
dávení	dávení	k1gNnSc4	dávení
nebo	nebo	k8xC	nebo
horečku	horečka	k1gFnSc4	horečka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgInSc7d3	nejčastější
projevem	projev	k1gInSc7	projev
dysurie	dysurie	k1gFnSc2	dysurie
(	(	kIx(	(
<g/>
obtíže	obtíž	k1gFnPc1	obtíž
při	při	k7c6	při
močení	močení	k1gNnSc6	močení
<g/>
)	)	kIx)	)
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
řezáním	řezání	k1gNnSc7	řezání
<g/>
,	,	kIx,	,
pálením	pálení	k1gNnSc7	pálení
a	a	k8xC	a
hustým	hustý	k2eAgInSc7d1	hustý
hojným	hojný	k2eAgInSc7d1	hojný
hnisavým	hnisavý	k2eAgInSc7d1	hnisavý
(	(	kIx(	(
<g/>
podobným	podobný	k2eAgInSc7d1	podobný
kondenzovanému	kondenzovaný	k2eAgNnSc3d1	kondenzované
mléku	mléko	k1gNnSc3	mléko
<g/>
)	)	kIx)	)
výtokem	výtok	k1gInSc7	výtok
z	z	k7c2	z
močové	močový	k2eAgFnSc2d1	močová
trubice	trubice	k1gFnSc2	trubice
(	(	kIx(	(
<g/>
uretry	uretra	k1gFnSc2	uretra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1	vyšetření
ukáže	ukázat	k5eAaPmIp3nS	ukázat
zčervenalý	zčervenalý	k2eAgInSc4d1	zčervenalý
externí	externí	k2eAgInSc4d1	externí
uretrální	uretrální	k2eAgInSc4d1	uretrální
močovod	močovod	k1gInSc4	močovod
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
efektivní	efektivní	k2eAgFnSc2d1	efektivní
léčby	léčba	k1gFnSc2	léčba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
infekce	infekce	k1gFnSc2	infekce
rozšířit	rozšířit	k5eAaPmF	rozšířit
na	na	k7c4	na
nadvarle	nadvarle	k1gNnSc4	nadvarle
(	(	kIx(	(
<g/>
epididymis	epididymis	k1gInSc1	epididymis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varlata	varle	k1gNnPc1	varle
(	(	kIx(	(
<g/>
testes	testes	k1gInSc1	testes
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
prostatu	prostata	k1gFnSc4	prostata
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
navenek	navenek	k6eAd1	navenek
bolestí	bolest	k1gFnSc7	bolest
nebo	nebo	k8xC	nebo
svrběním	svrbění	k1gNnSc7	svrbění
šourku	šourek	k1gInSc2	šourek
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
takto	takto	k6eAd1	takto
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
infekce	infekce	k1gFnSc2	infekce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
neplodnost	neplodnost	k1gFnSc4	neplodnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nakažení	nakažení	k1gNnPc2	nakažení
kapavkou	kapavka	k1gFnSc7	kapavka
v	v	k7c6	v
hltanu	hltan	k1gInSc6	hltan
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
i	i	k8xC	i
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
angína	angína	k1gFnSc1	angína
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
i	i	k9	i
muži	muž	k1gMnPc1	muž
s	s	k7c7	s
rektální	rektální	k2eAgFnSc7d1	rektální
(	(	kIx(	(
<g/>
konečníkovou	konečníkový	k2eAgFnSc7d1	Konečníková
<g/>
)	)	kIx)	)
kapavkou	kapavka	k1gFnSc7	kapavka
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
anální	anální	k2eAgInSc4d1	anální
výtok	výtok	k1gInSc4	výtok
<g/>
,	,	kIx,	,
perianální	perianálnit	k5eAaPmIp3nS	perianálnit
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
análu	anál	k1gInSc2	anál
<g/>
)	)	kIx)	)
svrběnÍ	svrbění	k1gNnPc4	svrbění
a	a	k8xC	a
rektální	rektální	k2eAgNnSc4d1	rektální
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
Proktoskopie	Proktoskopie	k1gFnSc1	Proktoskopie
(	(	kIx(	(
<g/>
instrumentální	instrumentální	k2eAgNnSc1d1	instrumentální
vyšetření	vyšetření	k1gNnSc1	vyšetření
rekta	rektum	k1gNnSc2	rektum
<g/>
)	)	kIx)	)
ukáže	ukázat	k5eAaPmIp3nS	ukázat
zapálenou	zapálený	k2eAgFnSc4d1	zapálená
slizniční	slizniční	k2eAgFnSc4d1	slizniční
membránu	membrána	k1gFnSc4	membrána
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
slizu	sliz	k1gInSc2	sliz
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
nebo	nebo	k8xC	nebo
jiní	jiný	k2eAgMnPc1d1	jiný
zdravotníci	zdravotník	k1gMnPc1	zdravotník
obvykle	obvykle	k6eAd1	obvykle
používají	používat	k5eAaImIp3nP	používat
3	[number]	k4	3
laboratorní	laboratorní	k2eAgFnPc1d1	laboratorní
techniky	technika	k1gFnPc1	technika
na	na	k7c4	na
diagnostiku	diagnostika	k1gFnSc4	diagnostika
kapavky	kapavka	k1gFnSc2	kapavka
<g/>
:	:	kIx,	:
barvicí	barvicí	k2eAgInPc1d1	barvicí
vzorky	vzorek	k1gInPc1	vzorek
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
bakterii	bakterie	k1gFnSc6	bakterie
<g/>
,	,	kIx,	,
detekce	detekce	k1gFnSc1	detekce
bakteriálních	bakteriální	k2eAgInPc2d1	bakteriální
genů	gen	k1gInPc2	gen
nebo	nebo	k8xC	nebo
DNA	dno	k1gNnSc2	dno
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
a	a	k8xC	a
růst	růst	k1gInSc4	růst
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
laboratórních	laboratórní	k2eAgFnPc6d1	laboratórní
kulturách	kultura	k1gFnPc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
doktorů	doktor	k1gMnPc2	doktor
preferuje	preferovat	k5eAaImIp3nS	preferovat
použití	použití	k1gNnSc4	použití
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jednoho	jeden	k4xCgInSc2	jeden
testu	test	k1gInSc2	test
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
přesnosti	přesnost	k1gFnSc2	přesnost
diagnostiky	diagnostika	k1gFnSc2	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Barvicí	barvicí	k2eAgInSc1d1	barvicí
test	test	k1gInSc1	test
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
výtěr	výtěr	k1gInSc4	výtěr
výtoku	výtok	k1gInSc2	výtok
z	z	k7c2	z
penisu	penis	k1gInSc2	penis
nebo	nebo	k8xC	nebo
cervixu	cervix	k1gInSc2	cervix
<g/>
.	.	kIx.	.
</s>
<s>
Vzorek	vzorek	k1gInSc1	vzorek
se	se	k3xPyFc4	se
umístí	umístit	k5eAaPmIp3nS	umístit
na	na	k7c4	na
sklíčko	sklíčko	k1gNnSc4	sklíčko
a	a	k8xC	a
obarví	obarvit	k5eAaPmIp3nS	obarvit
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
pak	pak	k6eAd1	pak
výsledek	výsledek	k1gInSc4	výsledek
prohlédne	prohlédnout	k5eAaPmIp3nS	prohlédnout
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
test	test	k1gInSc1	test
je	být	k5eAaImIp3nS	být
spolehlivý	spolehlivý	k2eAgMnSc1d1	spolehlivý
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
nelze	lze	k6eNd1	lze
negativní	negativní	k2eAgInSc1d1	negativní
výsledek	výsledek	k1gInSc1	výsledek
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
průkazný	průkazný	k2eAgInSc4d1	průkazný
(	(	kIx(	(
<g/>
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
pacientek	pacientka	k1gFnPc2	pacientka
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
močové	močový	k2eAgInPc1d1	močový
nebo	nebo	k8xC	nebo
cervikální	cervikální	k2eAgInPc1d1	cervikální
výtěry	výtěr	k1gInPc1	výtěr
pro	pro	k7c4	pro
test	test	k1gInSc4	test
detekující	detekující	k2eAgInPc4d1	detekující
geny	gen	k1gInPc4	gen
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
testy	test	k1gInPc1	test
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
přesné	přesný	k2eAgFnPc1d1	přesná
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
přesnější	přesný	k2eAgMnSc1d2	přesnější
než	než	k8xS	než
kultivace	kultivace	k1gFnSc1	kultivace
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
lékařů	lékař	k1gMnPc2	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Kultivační	kultivační	k2eAgInSc1d1	kultivační
test	test	k1gInSc1	test
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dání	dání	k1gNnSc4	dání
vzorku	vzorek	k1gInSc2	vzorek
výtoku	výtok	k1gInSc2	výtok
do	do	k7c2	do
kultivační	kultivační	k2eAgFnSc2d1	kultivační
nádoby	nádoba	k1gFnSc2	nádoba
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
inkubaci	inkubace	k1gFnSc3	inkubace
na	na	k7c4	na
2	[number]	k4	2
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
bakterie	bakterie	k1gFnPc4	bakterie
růst	růst	k5eAaImF	růst
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
tohoto	tento	k3xDgInSc2	tento
testu	test	k1gInSc2	test
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
vzorek	vzorek	k1gInSc1	vzorek
odebere	odebrat	k5eAaPmIp3nS	odebrat
<g/>
.	.	kIx.	.
</s>
<s>
Kultury	kultura	k1gFnPc1	kultura
cervikálních	cervikální	k2eAgInPc2d1	cervikální
vzorků	vzorek	k1gInPc2	vzorek
detekují	detekovat	k5eAaImIp3nP	detekovat
infekci	infekce	k1gFnSc4	infekce
asi	asi	k9	asi
na	na	k7c4	na
90	[number]	k4	90
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
mohou	moct	k5eAaImIp3nP	moct
též	též	k9	též
vzít	vzít	k5eAaPmF	vzít
kulturu	kultura	k1gFnSc4	kultura
na	na	k7c4	na
detekci	detekce	k1gFnSc4	detekce
kapavky	kapavka	k1gFnSc2	kapavka
z	z	k7c2	z
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
.	.	kIx.	.
</s>
<s>
Kultury	kultura	k1gFnPc1	kultura
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
i	i	k9	i
testování	testování	k1gNnSc4	testování
na	na	k7c4	na
bakterie	bakterie	k1gFnPc4	bakterie
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
lékům	lék	k1gInPc3	lék
<g/>
.	.	kIx.	.
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
2-14	[number]	k4	2-14
dnů	den	k1gInPc2	den
Základ	základ	k1gInSc4	základ
léčby	léčba	k1gFnSc2	léčba
tvoří	tvořit	k5eAaImIp3nS	tvořit
správné	správný	k2eAgNnSc4d1	správné
použití	použití	k1gNnSc4	použití
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
antibiotické	antibiotický	k2eAgFnSc2d1	antibiotická
rezistence	rezistence	k1gFnSc2	rezistence
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
použití	použití	k1gNnSc2	použití
penicilinu	penicilin	k1gInSc2	penicilin
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
kapavky	kapavka	k1gFnSc2	kapavka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
preferuje	preferovat	k5eAaImIp3nS	preferovat
léčba	léčba	k1gFnSc1	léčba
např.	např.	kA	např.
ceftriaxonem	ceftriaxon	k1gMnSc7	ceftriaxon
<g/>
,	,	kIx,	,
co	co	k9	co
je	být	k5eAaImIp3nS	být
cefalosporin	cefalosporin	k1gInSc4	cefalosporin
třetí	třetí	k4xOgFnSc2	třetí
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Doktoři	doktor	k1gMnPc1	doktor
obvykle	obvykle	k6eAd1	obvykle
předepisují	předepisovat	k5eAaImIp3nP	předepisovat
1	[number]	k4	1
dávku	dávka	k1gFnSc4	dávka
jednoho	jeden	k4xCgNnSc2	jeden
z	z	k7c2	z
následujících	následující	k2eAgNnPc2d1	následující
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
:	:	kIx,	:
cefixim	cefixim	k1gMnSc1	cefixim
ceftriaxon	ceftriaxon	k1gMnSc1	ceftriaxon
ciprofloxacin	ciprofloxacin	k1gMnSc1	ciprofloxacin
ofloxacin	ofloxacin	k1gMnSc1	ofloxacin
levofloxacin	levofloxacin	k1gMnSc1	levofloxacin
azitromycin	azitromycin	k1gMnSc1	azitromycin
Protože	protože	k8xS	protože
koinfekce	koinfekce	k1gFnSc1	koinfekce
s	s	k7c7	s
chlamydiemi	chlamydie	k1gFnPc7	chlamydie
je	být	k5eAaImIp3nS	být
častá	častý	k2eAgFnSc1d1	častá
<g/>
,	,	kIx,	,
lékaři	lékař	k1gMnPc1	lékař
často	často	k6eAd1	často
předepisují	předepisovat	k5eAaImIp3nP	předepisovat
kombinaci	kombinace	k1gFnSc4	kombinace
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ceftriaxon	ceftriaxon	k1gInSc1	ceftriaxon
a	a	k8xC	a
doxycyklin	doxycyklin	k1gInSc1	doxycyklin
nebo	nebo	k8xC	nebo
azitromycin	azitromycin	k1gInSc1	azitromycin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
léčí	léčit	k5eAaImIp3nS	léčit
obě	dva	k4xCgFnPc4	dva
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemoc	nemoc	k1gFnSc1	nemoc
byla	být	k5eAaImAgFnS	být
skutečně	skutečně	k6eAd1	skutečně
vyléčena	vyléčen	k2eAgFnSc1d1	vyléčena
<g/>
,	,	kIx,	,
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
se	se	k3xPyFc4	se
i	i	k9	i
následné	následný	k2eAgInPc1d1	následný
kontrolní	kontrolní	k2eAgInPc1d1	kontrolní
testy	test	k1gInPc1	test
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyloučí	vyloučit	k5eAaPmIp3nP	vyloučit
přenašečství	přenašečství	k1gNnSc4	přenašečství
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgMnPc1d1	sexuální
partneři	partner	k1gMnPc1	partner
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
připadá	připadat	k5eAaImIp3nS	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
prohlédnuti	prohlédnut	k2eAgMnPc1d1	prohlédnut
<g/>
.	.	kIx.	.
</s>
<s>
Těhotné	těhotný	k2eAgFnPc4d1	těhotná
matky	matka	k1gFnPc4	matka
infikované	infikovaný	k2eAgFnPc4d1	infikovaná
kapavkou	kapavka	k1gFnSc7	kapavka
mohou	moct	k5eAaImIp3nP	moct
přenést	přenést	k5eAaPmF	přenést
nemoc	nemoc	k1gFnSc4	nemoc
na	na	k7c4	na
děti	dítě	k1gFnPc4	dítě
během	během	k7c2	během
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
Gonokoková	gonokokový	k2eAgFnSc1d1	gonokoková
konjunktivitida	konjunktivitida	k1gFnSc1	konjunktivitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
oční	oční	k2eAgFnSc2d1	oční
spojivky	spojivka	k1gFnSc2	spojivka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
příčin	příčina	k1gFnPc2	příčina
slepoty	slepota	k1gFnSc2	slepota
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jí	on	k3xPp3gFnSc3	on
předejít	předejít	k5eAaPmF	předejít
aplikací	aplikace	k1gFnSc7	aplikace
vhodných	vhodný	k2eAgInPc2d1	vhodný
očních	oční	k2eAgInPc2d1	oční
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nasazují	nasazovat	k5eAaImIp3nP	nasazovat
při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
nechala	nechat	k5eAaPmAgFnS	nechat
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
nemoci	nemoc	k1gFnPc4	nemoc
včetně	včetně	k7c2	včetně
kapavky	kapavka	k1gFnSc2	kapavka
<g/>
.	.	kIx.	.
</s>
