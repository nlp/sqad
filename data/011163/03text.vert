<p>
<s>
Šógo	Šógo	k6eAd1	Šógo
Taniguči	Taniguč	k1gFnSc3	Taniguč
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonský	japonský	k2eAgMnSc1d1	japonský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Hrával	hrávat	k5eAaImAgMnS	hrávat
za	za	k7c4	za
Kawasaki	Kawasake	k1gFnSc4	Kawasake
Frontale	Frontal	k1gMnSc5	Frontal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Šógo	Šógo	k6eAd1	Šógo
Taniguči	Taniguč	k1gFnSc3	Taniguč
odehrál	odehrát	k5eAaPmAgInS	odehrát
za	za	k7c4	za
japonský	japonský	k2eAgInSc4d1	japonský
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
celkem	celkem	k6eAd1	celkem
2	[number]	k4	2
reprezentační	reprezentační	k2eAgInSc1d1	reprezentační
utkání	utkání	k1gNnSc3	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
National	Nationat	k5eAaPmAgInS	Nationat
Football	Football	k1gMnSc1	Football
Teams	Teamsa	k1gFnPc2	Teamsa
</s>
</p>
