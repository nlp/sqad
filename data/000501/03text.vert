<s>
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
osmá	osmý	k4xOgFnSc1	osmý
a	a	k8xC	a
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
nejvzdálenější	vzdálený	k2eAgFnSc1d3	nejvzdálenější
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
;	;	kIx,	;
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rovníkovým	rovníkový	k2eAgInSc7d1	rovníkový
průměrem	průměr	k1gInSc7	průměr
okolo	okolo	k7c2	okolo
50	[number]	k4	50
000	[number]	k4	000
km	km	kA	km
spadá	spadat	k5eAaImIp3nS	spadat
mezi	mezi	k7c4	mezi
menší	malý	k2eAgFnPc4d2	menší
plynné	plynný	k2eAgFnPc4d1	plynná
obry	obr	k1gMnPc7	obr
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
přímo	přímo	k6eAd1	přímo
pozorovat	pozorovat	k5eAaImF	pozorovat
pouze	pouze	k6eAd1	pouze
svrchní	svrchní	k2eAgFnPc4d1	svrchní
vrstvy	vrstva	k1gFnPc4	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
několik	několik	k4yIc4	několik
velkých	velký	k2eAgFnPc2d1	velká
temných	temný	k2eAgFnPc2d1	temná
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
připomínajících	připomínající	k2eAgFnPc2d1	připomínající
skvrny	skvrna	k1gFnPc4	skvrna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
má	mít	k5eAaImIp3nS	mít
charakteristicky	charakteristicky	k6eAd1	charakteristicky
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
mj.	mj.	kA	mj.
přítomností	přítomnost	k1gFnSc7	přítomnost
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
metanu	metan	k1gInSc2	metan
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
podobná	podobný	k2eAgFnSc1d1	podobná
Uranu	Uran	k1gMnSc3	Uran
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
rozdílné	rozdílný	k2eAgNnSc4d1	rozdílné
složení	složení	k1gNnSc4	složení
než	než	k8xS	než
další	další	k2eAgMnPc1d1	další
plynní	plynný	k2eAgMnPc1d1	plynný
obři	obr	k1gMnPc1	obr
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
vyčleňováni	vyčleňovat	k5eAaImNgMnP	vyčleňovat
do	do	k7c2	do
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
kategorie	kategorie	k1gFnSc2	kategorie
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ledoví	ledový	k2eAgMnPc1d1	ledový
obři	obr	k1gMnPc1	obr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
podílem	podíl	k1gInSc7	podíl
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
čpavku	čpavek	k1gInSc2	čpavek
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
stavba	stavba	k1gFnSc1	stavba
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
obohacená	obohacený	k2eAgNnPc4d1	obohacené
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
Johannem	Johann	k1gMnSc7	Johann
Gallem	Gall	k1gMnSc7	Gall
a	a	k8xC	a
studentem	student	k1gMnSc7	student
astronomie	astronomie	k1gFnSc2	astronomie
Louisem	Louis	k1gMnSc7	Louis
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arrestem	Arrest	k1gMnSc7	Arrest
jako	jako	k8xS	jako
vůbec	vůbec	k9	vůbec
jediná	jediný	k2eAgFnSc1d1	jediná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
matematických	matematický	k2eAgInPc2d1	matematický
výpočtů	výpočet	k1gInPc2	výpočet
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
odchylek	odchylka	k1gFnPc2	odchylka
okolních	okolní	k2eAgFnPc2d1	okolní
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
planeta	planeta	k1gFnSc1	planeta
dostala	dostat	k5eAaPmAgFnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
římského	římský	k2eAgMnSc2d1	římský
boha	bůh	k1gMnSc2	bůh
moří	mořit	k5eAaImIp3nS	mořit
Neptuna	Neptun	k1gMnSc4	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Neptun	Neptun	k1gInSc1	Neptun
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stejným	stejný	k2eAgInSc7d1	stejný
procesem	proces	k1gInSc7	proces
jako	jako	k8xC	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
před	před	k7c7	před
4,6	[number]	k4	4,6
až	až	k9	až
4,7	[number]	k4	4,7
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohly	moct	k5eAaImAgFnP	moct
velké	velký	k2eAgFnPc4d1	velká
plynné	plynný	k2eAgFnPc4d1	plynná
planety	planeta	k1gFnPc4	planeta
vzniknout	vzniknout	k5eAaPmF	vzniknout
a	a	k8xC	a
zformovat	zformovat	k5eAaPmF	zformovat
se	se	k3xPyFc4	se
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
teorii	teorie	k1gFnSc4	teorie
akrece	akrece	k1gFnSc2	akrece
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
akrece	akrece	k1gFnSc2	akrece
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
postupně	postupně	k6eAd1	postupně
slepovaly	slepovat	k5eAaImAgFnP	slepovat
drobné	drobný	k2eAgFnPc1d1	drobná
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
větší	veliký	k2eAgFnPc1d2	veliký
částice	částice	k1gFnPc1	částice
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
balvany	balvan	k1gInPc4	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgFnPc4d1	neustálá
srážky	srážka	k1gFnPc4	srážka
těles	těleso	k1gNnPc2	těleso
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
narůstání	narůstání	k1gNnSc1	narůstání
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
tělesa	těleso	k1gNnPc1	těleso
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
velká	velký	k2eAgNnPc1d1	velké
železokamenitá	železokamenitý	k2eAgNnPc1d1	železokamenitý
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zárodky	zárodek	k1gInPc7	zárodek
terestrických	terestrický	k2eAgFnPc2d1	terestrická
(	(	kIx(	(
<g/>
kamenných	kamenný	k2eAgFnPc2d1	kamenná
<g/>
)	)	kIx)	)
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobná	podobný	k2eAgNnPc1d1	podobné
tělesa	těleso	k1gNnPc1	těleso
mohla	moct	k5eAaImAgNnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
ve	v	k7c6	v
vzdálenějších	vzdálený	k2eAgFnPc6d2	vzdálenější
oblastech	oblast	k1gFnPc6	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlivem	vliv	k1gInSc7	vliv
velké	velký	k2eAgFnSc2d1	velká
gravitace	gravitace	k1gFnSc2	gravitace
začala	začít	k5eAaPmAgFnS	začít
strhávat	strhávat	k5eAaImF	strhávat
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgInS	začít
nabalovat	nabalovat	k5eAaImF	nabalovat
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
planeta	planeta	k1gFnSc1	planeta
dorostla	dorůst	k5eAaPmAgFnS	dorůst
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
naopak	naopak	k6eAd1	naopak
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgFnPc1d1	velká
planety	planeta	k1gFnPc1	planeta
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
postupným	postupný	k2eAgNnSc7d1	postupné
slepováním	slepování	k1gNnSc7	slepování
drobných	drobný	k2eAgFnPc2d1	drobná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
rychlým	rychlý	k2eAgNnSc7d1	rychlé
smrštěním	smrštění	k1gNnSc7	smrštění
z	z	k7c2	z
nahuštěného	nahuštěný	k2eAgInSc2d1	nahuštěný
shluku	shluk	k1gInSc2	shluk
v	v	k7c6	v
zárodečném	zárodečný	k2eAgNnSc6d1	zárodečné
disku	disco	k1gNnSc6	disco
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
několika	několik	k4yIc2	několik
gravitačních	gravitační	k2eAgInPc2d1	gravitační
kolapsů	kolaps	k1gInPc2	kolaps
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Alan	Alan	k1gMnSc1	Alan
Boss	boss	k1gMnSc1	boss
z	z	k7c2	z
Carnegie	Carnegie	k1gFnSc2	Carnegie
Institution	Institution	k1gInSc1	Institution
of	of	k?	of
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
planety	planeta	k1gFnSc2	planeta
Neptun	Neptun	k1gInSc1	Neptun
trval	trvat	k5eAaImAgInS	trvat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
existují	existovat	k5eAaImIp3nP	existovat
názory	názor	k1gInPc1	názor
popírající	popírající	k2eAgNnSc1d1	popírající
tyto	tento	k3xDgFnPc4	tento
teorie	teorie	k1gFnPc4	teorie
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Argumentuje	argumentovat	k5eAaImIp3nS	argumentovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Neptun	Neptun	k1gInSc1	Neptun
a	a	k8xC	a
Uran	Uran	k1gInSc1	Uran
nemohly	moct	k5eNaImAgInP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
takovéto	takovýto	k3xDgFnSc6	takovýto
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
protoplanetární	protoplanetární	k2eAgInSc1d1	protoplanetární
disk	disk	k1gInSc1	disk
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
hustý	hustý	k2eAgInSc4d1	hustý
na	na	k7c6	na
akreci	akrece	k1gFnSc6	akrece
takto	takto	k6eAd1	takto
velkých	velký	k2eAgNnPc2d1	velké
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
současné	současný	k2eAgInPc4d1	současný
modely	model	k1gInPc4	model
<g/>
.	.	kIx.	.
</s>
<s>
Případným	případný	k2eAgNnSc7d1	případné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
lokální	lokální	k2eAgFnSc1d1	lokální
nestabilita	nestabilita	k1gFnSc1	nestabilita
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1	alternativní
hypotéza	hypotéza	k1gFnSc1	hypotéza
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
blíže	blízce	k6eAd2	blízce
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
hustota	hustota	k1gFnSc1	hustota
meziplanetární	meziplanetární	k2eAgFnSc2d1	meziplanetární
látky	látka	k1gFnSc2	látka
větší	veliký	k2eAgFnSc2d2	veliký
a	a	k8xC	a
až	až	k9	až
časem	časem	k6eAd1	časem
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
planetární	planetární	k2eAgFnSc3d1	planetární
migraci	migrace	k1gFnSc3	migrace
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
migrace	migrace	k1gFnSc2	migrace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mezi	mezi	k7c4	mezi
planetology	planetolog	k1gMnPc4	planetolog
favorizována	favorizován	k2eAgFnSc1d1	favorizována
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lépe	dobře	k6eAd2	dobře
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
malé	malý	k2eAgInPc4d1	malý
objekty	objekt	k1gInPc4	objekt
za	za	k7c4	za
drahou	drahá	k1gFnSc4	drahá
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
velkých	velký	k2eAgInPc2d1	velký
Neptunových	Neptunův	k2eAgInPc2d1	Neptunův
měsíců	měsíc	k1gInPc2	měsíc
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
vznikaly	vznikat	k5eAaImAgFnP	vznikat
kamenné	kamenný	k2eAgFnPc4d1	kamenná
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
však	však	k9	však
Neptun	Neptun	k1gInSc1	Neptun
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
velmi	velmi	k6eAd1	velmi
vzdálen	vzdálit	k5eAaPmNgMnS	vzdálit
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
z	z	k7c2	z
fází	fáze	k1gFnPc2	fáze
vzniku	vznik	k1gInSc2	vznik
měsíců	měsíc	k1gInPc2	měsíc
nevystoupila	vystoupit	k5eNaPmAgFnS	vystoupit
teplota	teplota	k1gFnSc1	teplota
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hodnoty	hodnota	k1gFnPc4	hodnota
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
okolí	okolí	k1gNnSc2	okolí
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
tak	tak	k6eAd1	tak
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
lehce	lehko	k6eAd1	lehko
tavitelných	tavitelný	k2eAgFnPc2d1	tavitelná
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
disku	disk	k1gInSc2	disk
okolo	okolo	k7c2	okolo
vznikající	vznikající	k2eAgFnSc2d1	vznikající
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
i	i	k8xC	i
hmotností	hmotnost	k1gFnSc7	hmotnost
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc4d1	podobný
Uranu	Uran	k1gInSc3	Uran
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
1,024	[number]	k4	1,024
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
kg	kg	kA	kg
je	být	k5eAaImIp3nS	být
Neptun	Neptun	k1gMnSc1	Neptun
těleso	těleso	k1gNnSc4	těleso
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
hmotností	hmotnost	k1gFnSc7	hmotnost
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
většími	veliký	k2eAgMnPc7d2	veliký
plynnými	plynný	k2eAgMnPc7d1	plynný
obry	obr	k1gMnPc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	se	k3xPyFc4	se
Zemí	zem	k1gFnSc7	zem
je	být	k5eAaImIp3nS	být
Neptun	Neptun	k1gInSc1	Neptun
sedmnáctkrát	sedmnáctkrát	k6eAd1	sedmnáctkrát
hmotnější	hmotný	k2eAgInSc1d2	hmotnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
rovníku	rovník	k1gInSc2	rovník
Neptunu	Neptun	k1gInSc2	Neptun
je	být	k5eAaImIp3nS	být
24	[number]	k4	24
764	[number]	k4	764
km	km	kA	km
-	-	kIx~	-
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Uran	Uran	k1gInSc1	Uran
a	a	k8xC	a
Neptun	Neptun	k1gInSc1	Neptun
podobného	podobný	k2eAgNnSc2d1	podobné
složení	složení	k1gNnSc2	složení
tvořeného	tvořený	k2eAgNnSc2d1	tvořené
částečně	částečně	k6eAd1	částečně
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
vyčleňují	vyčleňovat	k5eAaImIp3nP	vyčleňovat
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
ledových	ledový	k2eAgMnPc2d1	ledový
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
než	než	k8xS	než
Uran	Uran	k1gInSc1	Uran
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
povrchu	povrch	k1gInSc2	povrch
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Složení	složení	k1gNnSc1	složení
Neptunu	Neptun	k1gInSc2	Neptun
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
velice	velice	k6eAd1	velice
podobné	podobný	k2eAgNnSc4d1	podobné
složení	složení	k1gNnSc4	složení
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
a	a	k8xC	a
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
složena	složit	k5eAaPmNgFnS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
kamení	kamení	k1gNnSc1	kamení
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
okolo	okolo	k7c2	okolo
15	[number]	k4	15
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
menšího	malý	k2eAgNnSc2d2	menší
množství	množství	k1gNnSc2	množství
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
jádra	jádro	k1gNnSc2	jádro
zabírá	zabírat	k5eAaImIp3nS	zabírat
přibližně	přibližně	k6eAd1	přibližně
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
poloměru	poloměr	k1gInSc2	poloměr
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
kamenného	kamenný	k2eAgNnSc2d1	kamenné
jádra	jádro	k1gNnSc2	jádro
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
,	,	kIx,	,
ledu	led	k1gInSc2	led
a	a	k8xC	a
tekutého	tekutý	k2eAgInSc2d1	tekutý
čpavku	čpavek	k1gInSc2	čpavek
s	s	k7c7	s
metanem	metan	k1gInSc7	metan
<g/>
.	.	kIx.	.
</s>
<s>
Kamenné	kamenný	k2eAgNnSc1d1	kamenné
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
složeninou	složenina	k1gFnSc7	složenina
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
a	a	k8xC	a
silikátů	silikát	k1gInPc2	silikát
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1,2	[number]	k4	1,2
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
5130	[number]	k4	5130
K	K	kA	K
respektive	respektive	k9	respektive
7	[number]	k4	7
Mbar	Mbara	k1gFnPc2	Mbara
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tímto	tento	k3xDgNnSc7	tento
velkým	velký	k2eAgNnSc7d1	velké
jádrem	jádro	k1gNnSc7	jádro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
třetina	třetina	k1gFnSc1	třetina
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pláště	plášť	k1gInSc2	plášť
tvořená	tvořený	k2eAgFnSc1d1	tvořená
nejspíše	nejspíše	k9	nejspíše
směsicí	směsice	k1gFnSc7	směsice
horkých	horký	k2eAgInPc2d1	horký
plynů	plyn	k1gInPc2	plyn
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
i	i	k9	i
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odrazu	odraz	k1gInSc6	odraz
světla	světlo	k1gNnSc2	světlo
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
metan	metan	k1gInSc1	metan
nejvíce	hodně	k6eAd3	hodně
rozptyluje	rozptylovat	k5eAaImIp3nS	rozptylovat
modré	modrý	k2eAgInPc4d1	modrý
paprsky	paprsek	k1gInPc4	paprsek
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
červenou	červený	k2eAgFnSc4d1	červená
část	část	k1gFnSc4	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
plášť	plášť	k1gInSc1	plášť
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
desetinásobku	desetinásobek	k1gInSc2	desetinásobek
až	až	k8xS	až
patnáctinásobku	patnáctinásobek	k1gInSc2	patnáctinásobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
mikrovlnného	mikrovlnný	k2eAgNnSc2d1	mikrovlnné
záření	záření	k1gNnSc2	záření
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
Neptunu	Neptun	k1gInSc6	Neptun
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
měřením	měření	k1gNnSc7	měření
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
Neptunu	Neptun	k1gInSc2	Neptun
bude	být	k5eAaImBp3nS	být
přibližně	přibližně	k6eAd1	přibližně
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sonda	sonda	k1gFnSc1	sonda
naměřila	naměřit	k5eAaBmAgFnS	naměřit
-	-	kIx~	-
°	°	k?	°
<g/>
C.	C.	kA	C.
Tento	tento	k3xDgInSc4	tento
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
naměřených	naměřený	k2eAgFnPc6d1	naměřená
hodnotách	hodnota	k1gFnPc6	hodnota
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Neptun	Neptun	k1gInSc1	Neptun
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přehřátý	přehřátý	k2eAgInSc1d1	přehřátý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
1730	[number]	k4	1730
až	až	k9	až
4730	[number]	k4	4730
°	°	k?	°
<g/>
C.	C.	kA	C.
Modely	model	k1gInPc1	model
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
okolo	okolo	k7c2	okolo
7000	[number]	k4	7000
km	km	kA	km
mohly	moct	k5eAaImAgInP	moct
nacházet	nacházet	k5eAaImF	nacházet
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
vznik	vznik	k1gInSc4	vznik
diamantů	diamant	k1gInPc2	diamant
z	z	k7c2	z
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
diamanty	diamant	k1gInPc1	diamant
by	by	kYmCp3nP	by
pak	pak	k6eAd1	pak
padaly	padat	k5eAaImAgFnP	padat
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
má	mít	k5eAaImIp3nS	mít
zelenomodrou	zelenomodrý	k2eAgFnSc4d1	zelenomodrá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
zabírá	zabírat	k5eAaImIp3nS	zabírat
nejspíše	nejspíše	k9	nejspíše
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
planetárního	planetární	k2eAgInSc2d1	planetární
poloměru	poloměr	k1gInSc2	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
bouřlivější	bouřlivý	k2eAgMnPc4d2	bouřlivější
<g/>
,	,	kIx,	,
proměnlivější	proměnlivý	k2eAgMnPc4d2	proměnlivější
než	než	k8xS	než
atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
(	(	kIx(	(
<g/>
19	[number]	k4	19
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mraky	mrak	k1gInPc1	mrak
různé	různý	k2eAgFnSc2d1	různá
výšky	výška	k1gFnSc2	výška
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
unášeny	unášen	k2eAgFnPc1d1	unášena
rychlostí	rychlost	k1gFnSc7	rychlost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Velké	velký	k2eAgFnSc2d1	velká
tmavé	tmavý	k2eAgFnSc2d1	tmavá
skvrny	skvrna	k1gFnSc2	skvrna
až	až	k9	až
2000	[number]	k4	2000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
zjištěnou	zjištěný	k2eAgFnSc4d1	zjištěná
rychlost	rychlost	k1gFnSc4	rychlost
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
větrů	vítr	k1gInPc2	vítr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
vanou	vanout	k5eAaImIp3nP	vanout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
rovníkem	rovník	k1gInSc7	rovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
proti	proti	k7c3	proti
rotaci	rotace	k1gFnSc3	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
do	do	k7c2	do
pásů	pás	k1gInPc2	pás
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
dobu	doba	k1gFnSc4	doba
oběhu	oběh	k1gInSc2	oběh
19	[number]	k4	19
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc1	rotace
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
atmosféra	atmosféra	k1gFnSc1	atmosféra
planety	planeta	k1gFnSc2	planeta
rotuje	rotovat	k5eAaImIp3nS	rotovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
jevem	jev	k1gInSc7	jev
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
2	[number]	k4	2
Velká	velký	k2eAgFnSc1d1	velká
tmavá	tmavý	k2eAgFnSc1d1	tmavá
skvrna	skvrna	k1gFnSc1	skvrna
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
široká	široký	k2eAgFnSc1d1	široká
jako	jako	k8xC	jako
Země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
či	či	k8xC	či
jako	jako	k9	jako
polovina	polovina	k1gFnSc1	polovina
známé	známý	k2eAgFnSc2d1	známá
Velké	velký	k2eAgFnSc2d1	velká
rudé	rudý	k2eAgFnSc2d1	rudá
skvrny	skvrna	k1gFnSc2	skvrna
na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíš	nejspíš	k9	nejspíš
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
vír	vír	k1gInSc1	vír
<g/>
,	,	kIx,	,
otáčející	otáčející	k2eAgFnPc1d1	otáčející
se	se	k3xPyFc4	se
rychlostí	rychlost	k1gFnSc7	rychlost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
bublinu	bublina	k1gFnSc4	bublina
vystupující	vystupující	k2eAgFnSc4d1	vystupující
z	z	k7c2	z
hlubších	hluboký	k2eAgFnPc2d2	hlubší
částí	část	k1gFnPc2	část
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Větry	vítr	k1gInPc1	vítr
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
skvrnou	skvrna	k1gFnSc7	skvrna
na	na	k7c4	na
západ	západ	k1gInSc4	západ
rychlostí	rychlost	k1gFnPc2	rychlost
okolo	okolo	k7c2	okolo
1080	[number]	k4	1080
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Opětovné	opětovný	k2eAgNnSc1d1	opětovné
pozorování	pozorování	k1gNnSc1	pozorování
Neptunu	Neptun	k1gInSc2	Neptun
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velká	velký	k2eAgFnSc1d1	velká
tmavá	tmavý	k2eAgFnSc1d1	tmavá
skvrna	skvrna	k1gFnSc1	skvrna
zmizela	zmizet	k5eAaPmAgFnS	zmizet
či	či	k8xC	či
byla	být	k5eAaImAgFnS	být
překryta	překrýt	k5eAaPmNgFnS	překrýt
dalšími	další	k2eAgInPc7d1	další
útvary	útvar	k1gInPc7	útvar
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
Velké	velký	k2eAgFnSc2d1	velká
temné	temný	k2eAgFnSc2d1	temná
skvrny	skvrna	k1gFnSc2	skvrna
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
pozorována	pozorován	k2eAgNnPc1d1	pozorováno
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
Malá	malý	k2eAgFnSc1d1	malá
temná	temný	k2eAgFnSc1d1	temná
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
výšce	výška	k1gFnSc6	výška
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
rychlostí	rychlost	k1gFnSc7	rychlost
prolétají	prolétat	k5eAaPmIp3nP	prolétat
malé	malý	k2eAgInPc1d1	malý
jasné	jasný	k2eAgInPc1d1	jasný
obláčky	obláček	k1gInPc1	obláček
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
ledovými	ledový	k2eAgInPc7d1	ledový
krystaly	krystal	k1gInPc7	krystal
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
skvrn	skvrna	k1gFnPc2	skvrna
byly	být	k5eAaImAgInP	být
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
objeveny	objeven	k2eAgInPc1d1	objeven
i	i	k8xC	i
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
světlé	světlý	k2eAgInPc1d1	světlý
mraky	mrak	k1gInPc1	mrak
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
každých	každý	k3xTgFnPc2	každý
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vžilo	vžít	k5eAaPmAgNnS	vžít
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
skútr	skútr	k1gInSc1	skútr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
byly	být	k5eAaImAgInP	být
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
i	i	k8xC	i
mraky	mrak	k1gInPc1	mrak
nápadně	nápadně	k6eAd1	nápadně
připomínající	připomínající	k2eAgInPc1d1	připomínající
pozemské	pozemský	k2eAgInPc1d1	pozemský
cirry	cirr	k1gInPc1	cirr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
mračna	mračna	k1gFnSc1	mračna
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
spíše	spíše	k9	spíše
než	než	k8xS	než
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
tvořena	tvořit	k5eAaImNgFnS	tvořit
krystalky	krystalek	k1gInPc7	krystalek
metanu	metan	k1gInSc2	metan
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
tvoří	tvořit	k5eAaImIp3nS	tvořit
2,5	[number]	k4	2,5
až	až	k9	až
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
pozorování	pozorování	k1gNnSc1	pozorování
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
střídání	střídání	k1gNnSc3	střídání
ročních	roční	k2eAgFnPc2d1	roční
období	období	k1gNnSc4	období
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
snímků	snímek	k1gInPc2	snímek
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
nárůstu	nárůst	k1gInSc3	nárůst
odraženého	odražený	k2eAgNnSc2d1	odražené
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vysvětlováno	vysvětlovat	k5eAaImNgNnS	vysvětlovat
právě	právě	k9	právě
změnou	změna	k1gFnSc7	změna
roční	roční	k2eAgFnSc2d1	roční
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2002	[number]	k4	2002
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
světlosti	světlost	k1gFnSc2	světlost
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
způsobováno	způsobovat	k5eAaImNgNnS	způsobovat
nárůstem	nárůst	k1gInSc7	nárůst
množství	množství	k1gNnSc4	množství
světlejších	světlý	k2eAgInPc2d2	světlejší
mraků	mrak	k1gInPc2	mrak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podpořilo	podpořit	k5eAaPmAgNnS	podpořit
předchozí	předchozí	k2eAgNnSc1d1	předchozí
pozorování	pozorování	k1gNnSc1	pozorování
prováděné	prováděný	k2eAgNnSc1d1	prováděné
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Lowellovy	Lowellův	k2eAgFnSc2d1	Lowellova
observatoře	observatoř	k1gFnSc2	observatoř
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c6	na
Neptunu	Neptun	k1gInSc6	Neptun
panují	panovat	k5eAaImIp3nP	panovat
čtyři	čtyři	k4xCgNnPc1	čtyři
roční	roční	k2eAgNnPc1d1	roční
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
projevovat	projevovat	k5eAaImF	projevovat
teplejším	teplý	k2eAgNnSc7d2	teplejší
létem	léto	k1gNnSc7	léto
a	a	k8xC	a
studenou	studený	k2eAgFnSc7d1	studená
zimou	zima	k1gFnSc7	zima
s	s	k7c7	s
postupným	postupný	k2eAgInSc7d1	postupný
přechodem	přechod	k1gInSc7	přechod
přes	přes	k7c4	přes
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
doby	doba	k1gFnSc2	doba
oběhu	oběh	k1gInSc2	oběh
planety	planeta	k1gFnSc2	planeta
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
165	[number]	k4	165
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
délka	délka	k1gFnSc1	délka
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
na	na	k7c6	na
Neptunu	Neptun	k1gInSc6	Neptun
bude	být	k5eAaImBp3nS	být
dosahovat	dosahovat	k5eAaImF	dosahovat
okolo	okolo	k7c2	okolo
40	[number]	k4	40
let	léto	k1gNnPc2	léto
pro	pro	k7c4	pro
jednotlivou	jednotlivý	k2eAgFnSc4d1	jednotlivá
periodu	perioda	k1gFnSc4	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
definitivní	definitivní	k2eAgNnSc4d1	definitivní
potvrzení	potvrzení	k1gNnSc4	potvrzení
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pokračovat	pokračovat	k5eAaImF	pokračovat
s	s	k7c7	s
pozorováními	pozorování	k1gNnPc7	pozorování
přibližně	přibližně	k6eAd1	přibližně
dalších	další	k2eAgNnPc2d1	další
20	[number]	k4	20
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
údaj	údaj	k1gInSc1	údaj
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yIgFnPc4	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
docházet	docházet	k5eAaImF	docházet
neustále	neustále	k6eAd1	neustále
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
jasu	jas	k1gInSc2	jas
jižních	jižní	k2eAgFnPc2d1	jižní
oblastí	oblast	k1gFnPc2	oblast
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
střídání	střídání	k1gNnSc6	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
podporuje	podporovat	k5eAaImIp3nS	podporovat
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
skloněná	skloněný	k2eAgFnSc1d1	skloněná
o	o	k7c4	o
29	[number]	k4	29
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
23,5	[number]	k4	23,5
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
detekovala	detekovat	k5eAaImAgFnS	detekovat
i	i	k9	i
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Uranovo	Uranův	k2eAgNnSc1d1	Uranovo
dipólové	dipólový	k2eAgNnSc1d1	dipólový
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
podivně	podivně	k6eAd1	podivně
orientované	orientovaný	k2eAgInPc1d1	orientovaný
<g/>
.	.	kIx.	.
</s>
<s>
Sklon	sklon	k1gInSc1	sklon
osy	osa	k1gFnSc2	osa
je	být	k5eAaImIp3nS	být
47	[number]	k4	47
<g/>
°	°	k?	°
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rotační	rotační	k2eAgFnSc3d1	rotační
ose	osa	k1gFnSc3	osa
a	a	k8xC	a
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
posunutá	posunutý	k2eAgFnSc1d1	posunutá
od	od	k7c2	od
středu	střed	k1gInSc2	střed
o	o	k7c6	o
0,55	[number]	k4	0,55
poloměru	poloměr	k1gInSc6	poloměr
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
13	[number]	k4	13
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznik	vznik	k1gInSc1	vznik
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
vodivého	vodivý	k2eAgInSc2d1	vodivý
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
nejspíše	nejspíše	k9	nejspíše
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
vrstvách	vrstva	k1gFnPc6	vrstva
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
stejně	stejně	k6eAd1	stejně
podivně	podivně	k6eAd1	podivně
orientované	orientovaný	k2eAgNnSc4d1	orientované
i	i	k9	i
u	u	k7c2	u
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
obecnou	obecný	k2eAgFnSc4d1	obecná
vlastnost	vlastnost	k1gFnSc4	vlastnost
ledových	ledový	k2eAgMnPc2d1	ledový
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
planety	planeta	k1gFnSc2	planeta
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
14	[number]	k4	14
μ	μ	k?	μ
a	a	k8xC	a
magnetický	magnetický	k2eAgInSc1d1	magnetický
dipólový	dipólový	k2eAgInSc1d1	dipólový
moment	moment	k1gInSc1	moment
0,2	[number]	k4	0,2
<g/>
×	×	k?	×
<g/>
1018	[number]	k4	1018
Tm	Tm	k1gMnPc2	Tm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
27	[number]	k4	27
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
magnetický	magnetický	k2eAgInSc4d1	magnetický
dipólový	dipólový	k2eAgInSc4d1	dipólový
moment	moment	k1gInSc4	moment
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
i	i	k9	i
polární	polární	k2eAgFnSc1d1	polární
zář	zář	k1gFnSc1	zář
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
podobné	podobný	k2eAgFnSc2d1	podobná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
jako	jako	k8xC	jako
u	u	k7c2	u
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
plochy	plocha	k1gFnSc2	plocha
dostává	dostávat	k5eAaImIp3nS	dostávat
900	[number]	k4	900
<g/>
krát	krát	k6eAd1	krát
méně	málo	k6eAd2	málo
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
2,7	[number]	k4	2,7
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
než	než	k8xS	než
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zdroj	zdroj	k1gInSc1	zdroj
této	tento	k3xDgFnSc2	tento
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
vyzařované	vyzařovaný	k2eAgFnSc2d1	vyzařovaná
energie	energie	k1gFnSc2	energie
není	být	k5eNaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Vyzařovaná	vyzařovaný	k2eAgFnSc1d1	vyzařovaná
energie	energie	k1gFnSc1	energie
však	však	k9	však
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
existenci	existence	k1gFnSc4	existence
bouřlivých	bouřlivý	k2eAgInPc2d1	bouřlivý
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gMnSc1	Neptun
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc4	slunce
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4	[number]	k4	4
498	[number]	k4	498
252	[number]	k4	252
900	[number]	k4	900
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
nejvíce	hodně	k6eAd3	hodně
na	na	k7c4	na
4	[number]	k4	4
459	[number]	k4	459
631	[number]	k4	631
496	[number]	k4	496
km	km	kA	km
a	a	k8xC	a
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
na	na	k7c4	na
4	[number]	k4	4
536	[number]	k4	536
874	[number]	k4	874
325	[number]	k4	325
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
velké	velký	k2eAgFnSc2d1	velká
excentricity	excentricita	k1gFnSc2	excentricita
dráhy	dráha	k1gFnSc2	dráha
Pluta	Pluto	k1gNnSc2	Pluto
se	se	k3xPyFc4	se
Neptun	Neptun	k1gInSc1	Neptun
může	moct	k5eAaImIp3nS	moct
dočasně	dočasně	k6eAd1	dočasně
ocitnout	ocitnout	k5eAaPmF	ocitnout
dál	daleko	k6eAd2	daleko
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
než	než	k8xS	než
Pluto	Pluto	k1gNnSc4	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Pluto	Pluto	k1gNnSc1	Pluto
počítalo	počítat	k5eAaImAgNnS	počítat
za	za	k7c4	za
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
tak	tak	k9	tak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Neptun	Neptun	k1gInSc1	Neptun
v	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
dobách	doba	k1gFnPc6	doba
dostával	dostávat	k5eAaImAgMnS	dostávat
na	na	k7c4	na
devátou	devátý	k4xOgFnSc4	devátý
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
planet	planeta	k1gFnPc2	planeta
podle	podle	k7c2	podle
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
Neptun	Neptun	k1gMnSc1	Neptun
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
165	[number]	k4	165
let	léto	k1gNnPc2	léto
a	a	k8xC	a
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
za	za	k7c4	za
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Neptunovi	Neptunův	k2eAgMnPc1d1	Neptunův
trojáni	troján	k1gMnPc1	troján
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Jupiteru	Jupiter	k1gInSc2	Jupiter
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
libračních	librační	k2eAgInPc6d1	librační
bodech	bod	k1gInPc6	bod
Neptunu	Neptun	k1gInSc2	Neptun
objevena	objeven	k2eAgNnPc1d1	objeveno
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
sdílejí	sdílet	k5eAaImIp3nP	sdílet
stejnou	stejný	k2eAgFnSc4d1	stejná
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
jako	jako	k8xC	jako
planeta	planeta	k1gFnSc1	planeta
Neptun	Neptun	k1gInSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
poznatku	poznatek	k1gInSc3	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
trojáni	troján	k1gMnPc1	troján
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
ostatně	ostatně	k6eAd1	ostatně
již	již	k6eAd1	již
déle	dlouho	k6eAd2	dlouho
před	před	k7c7	před
samotným	samotný	k2eAgNnSc7d1	samotné
pozorováním	pozorování	k1gNnSc7	pozorování
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
u	u	k7c2	u
Neptunu	Neptun	k1gInSc2	Neptun
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
trojánů	troján	k1gMnPc2	troján
kopírujících	kopírující	k2eAgMnPc2d1	kopírující
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
kolem	kolem	k7c2	kolem
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
libračního	librační	k2eAgInSc2d1	librační
bodu	bod	k1gInSc2	bod
L4	L4	k1gMnSc2	L4
ležícího	ležící	k2eAgMnSc2d1	ležící
před	před	k7c7	před
samotnou	samotný	k2eAgFnSc7d1	samotná
planetou	planeta	k1gFnSc7	planeta
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
60	[number]	k4	60
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prstence	prstenec	k1gInSc2	prstenec
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
prstenců	prstenec	k1gInPc2	prstenec
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
až	až	k9	až
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pomohla	pomoct	k5eAaPmAgFnS	pomoct
objevit	objevit	k5eAaPmF	objevit
tři	tři	k4xCgInPc4	tři
prstence	prstenec	k1gInPc4	prstenec
okolo	okolo	k7c2	okolo
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
prstence	prstenec	k1gInPc1	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc4	pět
prstenců	prstenec	k1gInPc2	prstenec
<g/>
:	:	kIx,	:
Galle	Galle	k1gInSc1	Galle
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Verrier	Verrier	k1gMnSc1	Verrier
<g/>
,	,	kIx,	,
Lassell	Lassell	k1gMnSc1	Lassell
<g/>
,	,	kIx,	,
Arago	Arago	k1gMnSc1	Arago
a	a	k8xC	a
prstenec	prstenec	k1gInSc1	prstenec
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nevýrazné	výrazný	k2eNgInPc1d1	nevýrazný
a	a	k8xC	a
tenké	tenký	k2eAgInPc1d1	tenký
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
složení	složení	k1gNnSc1	složení
je	být	k5eAaImIp3nS	být
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
.	.	kIx.	.
</s>
<s>
Nejvzdálenější	vzdálený	k2eAgFnPc1d3	nejvzdálenější
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
prstenec	prstenec	k1gInSc1	prstenec
Adams	Adams	k1gInSc4	Adams
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
tři	tři	k4xCgInPc1	tři
výraznější	výrazný	k2eAgInPc1d2	výraznější
oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
nejvíc	nejvíc	k6eAd1	nejvíc
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zhuštění	zhuštění	k1gNnPc1	zhuštění
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
vlastní	vlastní	k2eAgNnSc4d1	vlastní
pojmenování	pojmenování	k1gNnSc4	pojmenování
<g/>
:	:	kIx,	:
Volnost	volnost	k1gFnSc1	volnost
<g/>
,	,	kIx,	,
Rovnost	rovnost	k1gFnSc1	rovnost
a	a	k8xC	a
Bratrství	bratrství	k1gNnSc1	bratrství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prstenci	prstenec	k1gInSc6	prstenec
Adams	Adamsa	k1gFnPc2	Adamsa
následuje	následovat	k5eAaImIp3nS	následovat
bezejmenný	bezejmenný	k2eAgInSc1d1	bezejmenný
prstenec	prstenec	k1gInSc1	prstenec
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
drahou	draha	k1gFnSc7	draha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
měsíc	měsíc	k1gInSc4	měsíc
Galatea	Galate	k1gInSc2	Galate
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
prstenec	prstenec	k1gInSc1	prstenec
Leverrier	Leverrier	k1gInSc1	Leverrier
s	s	k7c7	s
vnějším	vnější	k2eAgNnSc7d1	vnější
protažením	protažení	k1gNnSc7	protažení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Lassella	Lassell	k1gMnSc4	Lassell
a	a	k8xC	a
Arga	Argos	k1gMnSc4	Argos
a	a	k8xC	a
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tenký	tenký	k2eAgMnSc1d1	tenký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
široký	široký	k2eAgInSc1d1	široký
prstenec	prstenec	k1gInSc1	prstenec
Galle	Galle	k1gFnSc2	Galle
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měsíce	měsíc	k1gInSc2	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
známe	znát	k5eAaImIp1nP	znát
14	[number]	k4	14
měsíců	měsíc	k1gInPc2	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Triton	triton	k1gInSc1	triton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
jen	jen	k9	jen
17	[number]	k4	17
dní	den	k1gInPc2	den
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
vlastní	vlastní	k2eAgFnSc2d1	vlastní
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejchladnější	chladný	k2eAgNnSc1d3	nejchladnější
těleso	těleso	k1gNnSc1	těleso
pozorované	pozorovaný	k2eAgNnSc1d1	pozorované
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
jeho	jeho	k3xOp3gInSc2	jeho
povrchu	povrch	k1gInSc2	povrch
je	být	k5eAaImIp3nS	být
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
45	[number]	k4	45
K	K	kA	K
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Tritonu	triton	k1gInSc2	triton
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
původně	původně	k6eAd1	původně
známého	známý	k2eAgInSc2d1	známý
měsíce	měsíc	k1gInSc2	měsíc
Nereida	nereida	k1gFnSc1	nereida
objevila	objevit	k5eAaPmAgFnS	objevit
dalších	další	k2eAgInPc2d1	další
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
kolem	kolem	k7c2	kolem
Neptunu	Neptun	k1gInSc2	Neptun
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
5	[number]	k4	5
měsíců	měsíc	k1gInPc2	měsíc
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
14	[number]	k4	14
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc4	měsíc
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
N	N	kA	N
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
pomocí	pomocí	k7c2	pomocí
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc4	některý
měsíce	měsíc	k1gInPc4	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Triton	triton	k1gInSc1	triton
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
původně	původně	k6eAd1	původně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
části	část	k1gFnSc6	část
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nejpravděpodobnější	pravděpodobný	k2eAgMnSc1d3	nejpravděpodobnější
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
oblast	oblast	k1gFnSc1	oblast
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
tělesa	těleso	k1gNnPc1	těleso
byla	být	k5eAaImAgNnP	být
později	pozdě	k6eAd2	pozdě
zachycena	zachytit	k5eAaPmNgNnP	zachytit
Neptunem	Neptun	k1gMnSc7	Neptun
do	do	k7c2	do
gravitační	gravitační	k2eAgFnSc2d1	gravitační
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
například	například	k6eAd1	například
retrográdní	retrográdní	k2eAgFnSc1d1	retrográdní
rotace	rotace	k1gFnSc1	rotace
měsíce	měsíc	k1gInSc2	měsíc
Tritonu	triton	k1gInSc2	triton
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
zachycením	zachycení	k1gNnSc7	zachycení
Tritonu	triton	k1gInSc2	triton
došlo	dojít	k5eAaPmAgNnS	dojít
nejspíše	nejspíše	k9	nejspíše
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Přílet	přílet	k1gInSc1	přílet
Tritonu	triton	k1gInSc2	triton
do	do	k7c2	do
soustavy	soustava	k1gFnSc2	soustava
mohl	moct	k5eAaImAgMnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
narušení	narušení	k1gNnSc4	narušení
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
ostatních	ostatní	k2eAgFnPc2d1	ostatní
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Triton	triton	k1gInSc1	triton
stal	stát	k5eAaPmAgInS	stát
největším	veliký	k2eAgInSc7d3	veliký
měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
pozdější	pozdní	k2eAgFnPc4d2	pozdější
vzájemné	vzájemný	k2eAgFnPc4d1	vzájemná
srážky	srážka	k1gFnPc4	srážka
menších	malý	k2eAgNnPc2d2	menší
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
rozpady	rozpad	k1gInPc1	rozpad
a	a	k8xC	a
spojování	spojování	k1gNnSc1	spojování
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
přeměně	přeměna	k1gFnSc3	přeměna
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc4	první
opakovaně	opakovaně	k6eAd1	opakovaně
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
Neptun	Neptun	k1gMnSc1	Neptun
svým	svůj	k3xOyFgMnPc3	svůj
nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
zkonstruovaným	zkonstruovaný	k2eAgInSc7d1	zkonstruovaný
dalekohledem	dalekohled	k1gInSc7	dalekohled
italský	italský	k2eAgMnSc1d1	italský
fyzik	fyzik	k1gMnSc1	fyzik
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnSc6	Galile
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1612	[number]	k4	1612
a	a	k8xC	a
1613	[number]	k4	1613
<g/>
.	.	kIx.	.
</s>
<s>
Planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
,	,	kIx,	,
však	však	k9	však
mylně	mylně	k6eAd1	mylně
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
hvězdu	hvězda	k1gFnSc4	hvězda
a	a	k8xC	a
náznakům	náznak	k1gInPc3	náznak
jejího	její	k3xOp3gMnSc2	její
(	(	kIx(	(
<g/>
ve	v	k7c6	v
dnech	den	k1gInPc6	den
pozorování	pozorování	k1gNnPc2	pozorování
obzvlášť	obzvlášť	k6eAd1	obzvlášť
slabého	slabý	k2eAgInSc2d1	slabý
<g/>
)	)	kIx)	)
pohybu	pohyb	k1gInSc2	pohyb
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
další	další	k2eAgFnSc4d1	další
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Alexis	Alexis	k1gFnPc2	Alexis
Bouvard	Bouvard	k1gMnSc1	Bouvard
publikoval	publikovat	k5eAaBmAgMnS	publikovat
podrobné	podrobný	k2eAgFnPc4d1	podrobná
tabulky	tabulka	k1gFnPc4	tabulka
poloh	poloha	k1gFnPc2	poloha
tří	tři	k4xCgFnPc2	tři
tehdy	tehdy	k6eAd1	tehdy
známých	známý	k2eAgFnPc2d1	známá
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
planety	planeta	k1gFnSc2	planeta
Uranu	Uran	k1gInSc2	Uran
se	se	k3xPyFc4	se
nová	nový	k2eAgNnPc1d1	nové
pozorování	pozorování	k1gNnPc1	pozorování
s	s	k7c7	s
tabulkovými	tabulkový	k2eAgInPc7d1	tabulkový
propočty	propočet	k1gInPc7	propočet
znatelně	znatelně	k6eAd1	znatelně
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Bouvard	Bouvard	k1gInSc1	Bouvard
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
pečlivém	pečlivý	k2eAgNnSc6d1	pečlivé
zkoumání	zkoumání	k1gNnSc6	zkoumání
těchto	tento	k3xDgFnPc2	tento
nepravidelností	nepravidelnost	k1gFnPc2	nepravidelnost
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
Uranu	Uran	k1gInSc2	Uran
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorované	pozorovaný	k2eAgFnPc1d1	pozorovaná
odchylky	odchylka	k1gFnPc1	odchylka
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
působení	působení	k1gNnSc6	působení
další	další	k2eAgInSc4d1	další
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neznámé	známý	k2eNgFnSc2d1	neznámá
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1843	[number]	k4	1843
až	až	k9	až
1846	[number]	k4	1846
přibližnou	přibližný	k2eAgFnSc4d1	přibližná
polohu	poloha	k1gFnSc4	poloha
předpokládaného	předpokládaný	k2eAgNnSc2d1	předpokládané
tělesa	těleso	k1gNnSc2	těleso
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
vypočítali	vypočítat	k5eAaPmAgMnP	vypočítat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Urbain	Urbain	k1gMnSc1	Urbain
Le	Le	k1gMnSc1	Le
Verrier	Verrier	k1gMnSc1	Verrier
a	a	k8xC	a
anglický	anglický	k2eAgMnSc1d1	anglický
astronom	astronom	k1gMnSc1	astronom
John	John	k1gMnSc1	John
Couch	Couch	k1gMnSc1	Couch
Adams	Adams	k1gInSc4	Adams
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Adamsovy	Adamsův	k2eAgInPc1d1	Adamsův
výpočty	výpočet	k1gInPc1	výpočet
byly	být	k5eAaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
jen	jen	k8xS	jen
úzkému	úzký	k2eAgInSc3d1	úzký
kruhu	kruh	k1gInSc3	kruh
britských	britský	k2eAgMnPc2d1	britský
astronomů	astronom	k1gMnPc2	astronom
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
potají	potají	k6eAd1	potají
vyvíjeli	vyvíjet	k5eAaImAgMnP	vyvíjet
horečné	horečný	k2eAgNnSc4d1	horečné
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
nalezení	nalezení	k1gNnSc4	nalezení
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Verrier	Verrier	k1gMnSc1	Verrier
své	svůj	k3xOyFgInPc4	svůj
postupně	postupně	k6eAd1	postupně
zpřesňované	zpřesňovaný	k2eAgInPc4d1	zpřesňovaný
výpočty	výpočet	k1gInPc4	výpočet
zveřejňoval	zveřejňovat	k5eAaImAgInS	zveřejňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
coby	coby	k?	coby
astronom-matematik	astronomatematik	k1gMnSc1	astronom-matematik
nenacházel	nacházet	k5eNaImAgMnS	nacházet
nikoho	nikdo	k3yNnSc4	nikdo
z	z	k7c2	z
francouzských	francouzský	k2eAgMnPc2d1	francouzský
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgInSc1d1	ochoten
prověření	prověření	k1gNnSc2	prověření
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
věnovat	věnovat	k5eAaPmF	věnovat
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Le	Le	k1gMnPc3	Le
Verrier	Verrier	k1gInSc1	Verrier
obrátil	obrátit	k5eAaPmAgInS	obrátit
dopisem	dopis	k1gInSc7	dopis
na	na	k7c4	na
astronoma	astronom	k1gMnSc4	astronom
Johanna	Johann	k1gMnSc4	Johann
Gottfrieda	Gottfried	k1gMnSc4	Gottfried
Galleho	Galle	k1gMnSc4	Galle
z	z	k7c2	z
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
Galle	Galle	k6eAd1	Galle
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
asistent	asistent	k1gMnSc1	asistent
Heinrich	Heinrich	k1gMnSc1	Heinrich
Louis	Louis	k1gMnSc1	Louis
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arrest	Arrest	k1gMnSc1	Arrest
nemarnili	marnit	k5eNaImAgMnP	marnit
čas	čas	k1gInSc4	čas
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
večera	večer	k1gInSc2	večer
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Le	Le	k1gFnSc2	Le
Verrierových	Verrierových	k2eAgNnSc2d1	Verrierových
doporučení	doporučení	k1gNnSc2	doporučení
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
ne	ne	k9	ne
po	po	k7c6	po
hodině	hodina	k1gFnSc6	hodina
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
necelý	celý	k2eNgInSc4d1	necelý
stupeň	stupeň	k1gInSc4	stupeň
od	od	k7c2	od
předpovězené	předpovězený	k2eAgFnSc2d1	předpovězená
polohy	poloha	k1gFnSc2	poloha
podařilo	podařit	k5eAaPmAgNnS	podařit
nalézt	nalézt	k5eAaPmF	nalézt
"	"	kIx"	"
<g/>
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
čerstvé	čerstvý	k2eAgFnSc6d1	čerstvá
mapě	mapa	k1gFnSc6	mapa
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
nebyla	být	k5eNaImAgNnP	být
zakreslena	zakreslit	k5eAaPmNgNnP	zakreslit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
následující	následující	k2eAgFnPc4d1	následující
noci	noc	k1gFnPc4	noc
opakované	opakovaný	k2eAgNnSc1d1	opakované
pozorování	pozorování	k1gNnSc1	pozorování
podezřelého	podezřelý	k2eAgInSc2d1	podezřelý
objektu	objekt	k1gInSc2	objekt
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
zřetelnou	zřetelný	k2eAgFnSc4d1	zřetelná
změnu	změna	k1gFnSc4	změna
jeho	jeho	k3xOp3gFnSc2	jeho
polohy	poloha	k1gFnSc2	poloha
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
již	již	k6eAd1	již
pochyb	pochyba	k1gFnPc2	pochyba
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
osmá	osmý	k4xOgFnSc1	osmý
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Souběžná	souběžný	k2eAgFnSc1d1	souběžná
snaha	snaha	k1gFnSc1	snaha
britských	britský	k2eAgMnPc2d1	britský
astronomů	astronom	k1gMnPc2	astronom
vyšla	vyjít	k5eAaPmAgFnS	vyjít
naprázdno	naprázdno	k6eAd1	naprázdno
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
rozptylu	rozptyl	k1gInSc3	rozptyl
Adamsových	Adamsův	k2eAgInPc2d1	Adamsův
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
spíše	spíše	k9	spíše
sváděly	svádět	k5eAaImAgFnP	svádět
ze	z	k7c2	z
stopy	stopa	k1gFnSc2	stopa
(	(	kIx(	(
<g/>
Adamsovy	Adamsův	k2eAgInPc1d1	Adamsův
výsledky	výsledek	k1gInPc1	výsledek
v	v	k7c6	v
době	doba	k1gFnSc6	doba
objevu	objev	k1gInSc2	objev
planety	planeta	k1gFnSc2	planeta
udávaly	udávat	k5eAaImAgFnP	udávat
polohu	poloha	k1gFnSc4	poloha
o	o	k7c4	o
12	[number]	k4	12
stupňů	stupeň	k1gInPc2	stupeň
mimo	mimo	k7c4	mimo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
také	také	k9	také
sehrály	sehrát	k5eAaPmAgFnP	sehrát
neuspokojivé	uspokojivý	k2eNgFnPc1d1	neuspokojivá
britské	britský	k2eAgFnPc1d1	britská
hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
mapy	mapa	k1gFnPc1	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
astronomové	astronom	k1gMnPc1	astronom
začali	začít	k5eAaPmAgMnP	začít
získávat	získávat	k5eAaImF	získávat
o	o	k7c6	o
Neptunu	Neptun	k1gInSc6	Neptun
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
informací	informace	k1gFnPc2	informace
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgInPc2d1	speciální
teleskopů	teleskop	k1gInPc2	teleskop
umístěných	umístěný	k2eAgInPc2d1	umístěný
na	na	k7c6	na
orbitální	orbitální	k2eAgFnSc6d1	orbitální
dráze	dráha	k1gFnSc6	dráha
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
z	z	k7c2	z
obrovských	obrovský	k2eAgInPc2d1	obrovský
teleskopů	teleskop	k1gInPc2	teleskop
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
nepřinášely	přinášet	k5eNaImAgFnP	přinášet
možnost	možnost	k1gFnSc4	možnost
detailnějších	detailní	k2eAgNnPc2d2	detailnější
pozorování	pozorování	k1gNnPc2	pozorování
kvůli	kvůli	k7c3	kvůli
rušivému	rušivý	k2eAgInSc3d1	rušivý
efektu	efekt	k1gInSc3	efekt
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
využívat	využívat	k5eAaPmF	využívat
systém	systém	k1gInSc4	systém
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
optiky	optika	k1gFnSc2	optika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
pozorování	pozorování	k1gNnSc6	pozorování
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgInPc1d1	dnešní
snímky	snímek	k1gInPc1	snímek
jsou	být	k5eAaImIp3nP	být
kvalitnější	kvalitní	k2eAgInPc1d2	kvalitnější
než	než	k8xS	než
snímky	snímek	k1gInPc1	snímek
zasílané	zasílaný	k2eAgInPc1d1	zasílaný
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
teleskopů	teleskop	k1gInPc2	teleskop
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
kvalitě	kvalita	k1gFnSc3	kvalita
snímků	snímek	k1gInPc2	snímek
pořízených	pořízený	k2eAgInPc2d1	pořízený
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
byl	být	k5eAaImAgInS	být
detailně	detailně	k6eAd1	detailně
sledován	sledovat	k5eAaImNgInS	sledovat
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnPc1	pozorování
mimo	mimo	k7c4	mimo
rušivé	rušivý	k2eAgInPc4d1	rušivý
vlivy	vliv	k1gInPc4	vliv
pozemské	pozemský	k2eAgFnSc2d1	pozemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
probíhala	probíhat	k5eAaImAgNnP	probíhat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
pořídit	pořídit	k5eAaPmF	pořídit
sérii	série	k1gFnSc4	série
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
dynamické	dynamický	k2eAgFnPc4d1	dynamická
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
sledování	sledování	k1gNnSc2	sledování
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
snímků	snímek	k1gInPc2	snímek
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
animace	animace	k1gFnSc1	animace
jevů	jev	k1gInPc2	jev
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
pomáhající	pomáhající	k2eAgFnSc2d1	pomáhající
vědcům	vědec	k1gMnPc3	vědec
sledovat	sledovat	k5eAaImF	sledovat
pohyby	pohyb	k1gInPc4	pohyb
mračen	mračna	k1gFnPc2	mračna
v	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
částech	část	k1gFnPc6	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
či	či	k8xC	či
sledovat	sledovat	k5eAaImF	sledovat
silný	silný	k2eAgInSc4d1	silný
jet	jet	k2eAgInSc4d1	jet
stream	stream	k1gInSc4	stream
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
pomohl	pomoct	k5eAaPmAgMnS	pomoct
po	po	k7c6	po
šestiletém	šestiletý	k2eAgNnSc6d1	šestileté
sledování	sledování	k1gNnSc6	sledování
objevit	objevit	k5eAaPmF	objevit
sezónní	sezónní	k2eAgFnPc4d1	sezónní
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
vznik	vznik	k1gInSc1	vznik
hypotézy	hypotéza	k1gFnSc2	hypotéza
o	o	k7c6	o
střídání	střídání	k1gNnSc6	střídání
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
(	(	kIx(	(
<g/>
planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
snímkována	snímkovat	k5eAaImNgFnS	snímkovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
podpůrná	podpůrný	k2eAgNnPc4d1	podpůrné
měření	měření	k1gNnPc4	měření
i	i	k8xC	i
Spitzerův	Spitzerův	k2eAgInSc4d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
sledující	sledující	k2eAgInSc1d1	sledující
vesmír	vesmír	k1gInSc1	vesmír
v	v	k7c6	v
infračerveném	infračervený	k2eAgNnSc6d1	infračervené
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Voyager	Voyagero	k1gNnPc2	Voyagero
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
kosmických	kosmický	k2eAgInPc2d1	kosmický
letů	let	k1gInPc2	let
byl	být	k5eAaImAgInS	být
Neptun	Neptun	k1gInSc1	Neptun
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
zkoumán	zkoumán	k2eAgInSc1d1	zkoumán
pouze	pouze	k6eAd1	pouze
jedinou	jediný	k2eAgFnSc7d1	jediná
planetární	planetární	k2eAgFnSc7d1	planetární
sondou	sonda	k1gFnSc7	sonda
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
prolétl	prolétnout	k5eAaPmAgMnS	prolétnout
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
přiblížení	přiblížení	k1gNnSc1	přiblížení
k	k	k7c3	k
Neptunu	Neptun	k1gInSc3	Neptun
nastalo	nastat	k5eAaPmAgNnS	nastat
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sonda	sonda	k1gFnSc1	sonda
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
planetu	planeta	k1gFnSc4	planeta
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
poslední	poslední	k2eAgFnSc1d1	poslední
velká	velký	k2eAgFnSc1d1	velká
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mohl	moct	k5eAaImAgInS	moct
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
prolétnout	prolétnout	k5eAaPmF	prolétnout
blízko	blízko	k7c2	blízko
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
5000	[number]	k4	5000
km	km	kA	km
nad	nad	k7c7	nad
pólem	pólo	k1gNnSc7	pólo
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
i	i	k9	i
kolem	kolem	k7c2	kolem
měsíce	měsíc	k1gInSc2	měsíc
Triton	triton	k1gInSc1	triton
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
40	[number]	k4	40
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
kolem	kolem	k7c2	kolem
Neptunu	Neptun	k1gInSc2	Neptun
sonda	sonda	k1gFnSc1	sonda
objevila	objevit	k5eAaPmAgFnS	objevit
Velkou	velký	k2eAgFnSc4d1	velká
tmavou	tmavý	k2eAgFnSc4d1	tmavá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
však	však	k9	však
Hubbleův	Hubbleův	k2eAgInSc4d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
dalekohled	dalekohled	k1gInSc4	dalekohled
později	pozdě	k6eAd2	pozdě
nenalezl	nalézt	k5eNaBmAgMnS	nalézt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
skvrna	skvrna	k1gFnSc1	skvrna
již	již	k6eAd1	již
zmizela	zmizet	k5eAaPmAgFnS	zmizet
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
atmosférickou	atmosférický	k2eAgFnSc4d1	atmosférická
poruchu	porucha	k1gFnSc4	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obrovské	obrovský	k2eAgNnSc4d1	obrovské
mračno	mračno	k1gNnSc4	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
usoudilo	usoudit	k5eAaPmAgNnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
mezeru	mezera	k1gFnSc4	mezera
v	v	k7c6	v
oblačnosti	oblačnost	k1gFnSc6	oblačnost
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
spatřit	spatřit	k5eAaPmF	spatřit
nižší	nízký	k2eAgFnSc1d2	nižší
vrstvy	vrstva	k1gFnPc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólu	pól	k1gInSc2	pól
sonda	sonda	k1gFnSc1	sonda
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
polární	polární	k2eAgFnSc4d1	polární
záři	záře	k1gFnSc4	záře
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
sonda	sonda	k1gFnSc1	sonda
odeslala	odeslat	k5eAaPmAgFnS	odeslat
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
okolo	okolo	k7c2	okolo
10	[number]	k4	10
000	[number]	k4	000
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
pomohla	pomoct	k5eAaPmAgFnS	pomoct
změřit	změřit	k5eAaPmF	změřit
velikost	velikost	k1gFnSc4	velikost
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc1	rotace
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
existenci	existence	k1gFnSc4	existence
Neptunových	Neptunův	k2eAgInPc2d1	Neptunův
prstenců	prstenec	k1gInPc2	prstenec
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
šest	šest	k4xCc4	šest
nových	nový	k2eAgInPc2d1	nový
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
nebyla	být	k5eNaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
schválena	schválit	k5eAaPmNgFnS	schválit
žádná	žádný	k3yNgFnSc1	žádný
další	další	k2eAgFnSc1d1	další
mise	mise	k1gFnSc1	mise
k	k	k7c3	k
Neptunu	Neptun	k1gInSc3	Neptun
či	či	k8xC	či
některému	některý	k3yIgInSc3	některý
z	z	k7c2	z
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
inženýrských	inženýrský	k2eAgInPc2d1	inženýrský
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
sonda	sonda	k1gFnSc1	sonda
měla	mít	k5eAaImAgFnS	mít
vypadat	vypadat	k5eAaImF	vypadat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
ještě	ještě	k9	ještě
nebyl	být	k5eNaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
či	či	k8xC	či
definitivně	definitivně	k6eAd1	definitivně
odsouhlasen	odsouhlasen	k2eAgInSc1d1	odsouhlasen
<g/>
.	.	kIx.	.
</s>
<s>
Koncepty	koncept	k1gInPc1	koncept
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
například	například	k6eAd1	například
atmosférickou	atmosférický	k2eAgFnSc4d1	atmosférická
sondu	sonda	k1gFnSc4	sonda
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
oproti	oproti	k7c3	oproti
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
bude	být	k5eAaImBp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
bližší	blízký	k2eAgFnSc6d2	bližší
původní	původní	k2eAgFnSc6d1	původní
mlhovině	mlhovina	k1gFnSc6	mlhovina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
startu	start	k1gInSc6	start
sondy	sonda	k1gFnSc2	sonda
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
2016	[number]	k4	2016
až	až	k9	až
2018	[number]	k4	2018
s	s	k7c7	s
příletem	přílet	k1gInSc7	přílet
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2035	[number]	k4	2035
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
vyslání	vyslání	k1gNnSc4	vyslání
sondy	sonda	k1gFnSc2	sonda
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
taktéž	taktéž	k?	taktéž
vyvinout	vyvinout	k5eAaPmF	vyvinout
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
napájení	napájení	k1gNnSc2	napájení
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přílišnou	přílišný	k2eAgFnSc4d1	přílišná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
nebude	být	k5eNaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
u	u	k7c2	u
Neptunu	Neptun	k1gInSc2	Neptun
používat	používat	k5eAaImF	používat
fotovoltaické	fotovoltaický	k2eAgInPc4d1	fotovoltaický
panely	panel	k1gInPc4	panel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
energii	energie	k1gFnSc4	energie
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
získávat	získávat	k5eAaImF	získávat
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
rozpadem	rozpad	k1gInSc7	rozpad
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
radioizotopovém	radioizotopový	k2eAgInSc6d1	radioizotopový
generátoru	generátor	k1gInSc6	generátor
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgInPc1d2	novější
plány	plán	k1gInPc1	plán
počítají	počítat	k5eAaImIp3nP	počítat
s	s	k7c7	s
vysláním	vyslání	k1gNnSc7	vyslání
sondy	sonda	k1gFnSc2	sonda
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2015	[number]	k4	2015
až	až	k9	až
2020	[number]	k4	2020
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
gravitační	gravitační	k2eAgInSc4d1	gravitační
prak	prak	k1gInSc4	prak
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
letu	let	k1gInSc2	let
a	a	k8xC	a
příletu	přílet	k1gInSc2	přílet
sondy	sonda	k1gFnSc2	sonda
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgFnPc4d1	známá
podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
takové	takový	k3xDgFnSc6	takový
sondě	sonda	k1gFnSc6	sonda
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
vybavení	vybavení	k1gNnSc1	vybavení
a	a	k8xC	a
ani	ani	k8xC	ani
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
vědecká	vědecký	k2eAgFnSc1d1	vědecká
komunita	komunita	k1gFnSc1	komunita
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
sestavení	sestavení	k1gNnSc4	sestavení
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
sonda	sonda	k1gFnSc1	sonda
měla	mít	k5eAaImAgFnS	mít
vykonat	vykonat	k5eAaPmF	vykonat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přinesla	přinést	k5eAaPmAgFnS	přinést
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
planetě	planeta	k1gFnSc6	planeta
či	či	k8xC	či
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
měsíci	měsíc	k1gInSc6	měsíc
Tritonovi	triton	k1gMnSc3	triton
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc4	Neptun
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozorování	pozorování	k1gNnPc4	pozorování
stačí	stačit	k5eAaBmIp3nS	stačit
obyčejný	obyčejný	k2eAgInSc1d1	obyčejný
triedr	triedr	k1gInSc1	triedr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
chce	chtít	k5eAaImIp3nS	chtít
vidět	vidět	k5eAaImF	vidět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
malou	malý	k2eAgFnSc4d1	malá
tečku	tečka	k1gFnSc4	tečka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
použít	použít	k5eAaPmF	použít
větší	veliký	k2eAgInSc4d2	veliký
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
modrozeleného	modrozelený	k2eAgInSc2d1	modrozelený
disku	disk	k1gInSc2	disk
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
použít	použít	k5eAaPmF	použít
dalekohled	dalekohled	k1gInSc4	dalekohled
s	s	k7c7	s
minimálně	minimálně	k6eAd1	minimálně
25	[number]	k4	25
až	až	k9	až
30	[number]	k4	30
centimetrů	centimetr	k1gInPc2	centimetr
velkým	velký	k2eAgNnSc7d1	velké
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
oposice	oposice	k1gFnSc2	oposice
je	být	k5eAaImIp3nS	být
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
Neptunu	Neptun	k1gInSc2	Neptun
7,8	[number]	k4	7,8
<g/>
m	m	kA	m
a	a	k8xC	a
úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
2,4	[number]	k4	2,4
<g/>
"	"	kIx"	"
<g/>
..	..	k?	..
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
Neptun	Neptun	k1gInSc1	Neptun
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Kozoroha	Kozoroh	k1gMnSc4	Kozoroh
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
Neptun	Neptun	k1gMnSc1	Neptun
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
měsíce	měsíc	k1gInPc4	měsíc
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnPc4	pozorování
mezi	mezi	k7c7	mezi
červencem	červenec	k1gInSc7	červenec
a	a	k8xC	a
listopadem	listopad	k1gInSc7	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjištění	zjištění	k1gNnSc4	zjištění
polohy	poloha	k1gFnSc2	poloha
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgMnSc1d3	nejvhodnější
použít	použít	k5eAaPmF	použít
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
softwarů	software	k1gInPc2	software
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Stellarium	Stellarium	k1gNnSc4	Stellarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ke	k	k7c3	k
konkrétnímu	konkrétní	k2eAgNnSc3d1	konkrétní
datu	datum	k1gNnSc3	datum
přesně	přesně	k6eAd1	přesně
určí	určit	k5eAaPmIp3nS	určit
pozici	pozice	k1gFnSc4	pozice
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
starořímského	starořímský	k2eAgMnSc2d1	starořímský
boha	bůh	k1gMnSc2	bůh
Neptuna	Neptun	k1gMnSc2	Neptun
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
Saturna	Saturn	k1gMnSc4	Saturn
a	a	k8xC	a
Opina	Opin	k1gMnSc4	Opin
<g/>
,	,	kIx,	,
představujícího	představující	k2eAgMnSc4d1	představující
původně	původně	k6eAd1	původně
boha	bůh	k1gMnSc2	bůh
toků	tok	k1gInPc2	tok
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
boha	bůh	k1gMnSc4	bůh
moří	mořit	k5eAaImIp3nS	mořit
<g/>
,	,	kIx,	,
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
jezdeckých	jezdecký	k2eAgInPc2d1	jezdecký
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
později	pozdě	k6eAd2	pozdě
ztotožněn	ztotožněn	k2eAgInSc1d1	ztotožněn
s	s	k7c7	s
řeckým	řecký	k2eAgInSc7d1	řecký
Poseidónem	Poseidón	k1gInSc7	Poseidón
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
manželek	manželka	k1gFnPc2	manželka
z	z	k7c2	z
bohyně	bohyně	k1gFnSc2	bohyně
Salacie	Salacie	k1gFnSc2	Salacie
na	na	k7c4	na
Amfitrité	Amfitritý	k2eAgNnSc4d1	Amfitritý
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
pro	pro	k7c4	pro
planetu	planeta	k1gFnSc4	planeta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
trojzubec	trojzubec	k1gInSc1	trojzubec
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Neptunu	Neptun	k1gInSc2	Neptun
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
nazývala	nazývat	k5eAaImAgFnS	nazývat
více	hodně	k6eAd2	hodně
názvy	název	k1gInPc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mluvilo	mluvit	k5eAaImAgNnS	mluvit
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
planetě	planeta	k1gFnSc6	planeta
za	za	k7c7	za
Uranem	Uran	k1gInSc7	Uran
<g/>
"	"	kIx"	"
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
Le	Le	k1gFnSc6	Le
Verrierově	Verrierův	k2eAgFnSc6d1	Verrierův
planetě	planeta	k1gFnSc6	planeta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
definitivní	definitivní	k2eAgNnSc4d1	definitivní
pojmenování	pojmenování	k1gNnSc4	pojmenování
planety	planeta	k1gFnSc2	planeta
vzešel	vzejít	k5eAaPmAgMnS	vzejít
od	od	k7c2	od
Galleho	Galle	k1gMnSc2	Galle
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
propagoval	propagovat	k5eAaImAgMnS	propagovat
jméno	jméno	k1gNnSc4	jméno
Janus	Janus	k1gMnSc1	Janus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
začal	začít	k5eAaPmAgInS	začít
Challis	Challis	k1gInSc1	Challis
používat	používat	k5eAaImF	používat
jméno	jméno	k1gNnSc4	jméno
Okeanos	Okeanosa	k1gFnPc2	Okeanosa
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
měl	mít	k5eAaImAgMnS	mít
objevitel	objevitel	k1gMnSc1	objevitel
právo	právo	k1gNnSc4	právo
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
nově	nově	k6eAd1	nově
objevenou	objevený	k2eAgFnSc4d1	objevená
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Verrier	Verrier	k1gMnSc1	Verrier
rychle	rychle	k6eAd1	rychle
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nově	nově	k6eAd1	nově
objevená	objevený	k2eAgFnSc1d1	objevená
planeta	planeta	k1gFnSc1	planeta
ponese	ponést	k5eAaPmIp3nS	ponést
jméno	jméno	k1gNnSc4	jméno
Neptun	Neptun	k1gMnSc1	Neptun
a	a	k8xC	a
nepravdivě	pravdivě	k6eNd1	pravdivě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
schválen	schválit	k5eAaPmNgInS	schválit
francouzským	francouzský	k2eAgInSc7d1	francouzský
úřadem	úřad	k1gInSc7	úřad
Bureau	Bureaus	k1gInSc2	Bureaus
des	des	k1gNnSc2	des
Longitudes	Longitudesa	k1gFnPc2	Longitudesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
snaha	snaha	k1gFnSc1	snaha
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
planetu	planeta	k1gFnSc4	planeta
zpět	zpět	k6eAd1	zpět
po	po	k7c6	po
Le	Le	k1gMnSc6	Le
Verrierovi	Verrier	k1gMnSc6	Verrier
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
mimo	mimo	k7c4	mimo
Francii	Francie	k1gFnSc4	Francie
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
byl	být	k5eAaImAgInS	být
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
Uran	Uran	k1gInSc4	Uran
na	na	k7c4	na
"	"	kIx"	"
<g/>
Herschel	Herschel	k1gInSc4	Herschel
<g/>
"	"	kIx"	"
dle	dle	k7c2	dle
jejího	její	k3xOp3gMnSc4	její
objevitele	objevitel	k1gMnSc4	objevitel
sira	sir	k1gMnSc4	sir
Williama	William	k1gMnSc4	William
Herschela	Herschel	k1gMnSc4	Herschel
a	a	k8xC	a
pro	pro	k7c4	pro
Neptun	Neptun	k1gInSc4	Neptun
používat	používat	k5eAaImF	používat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Leverrier	Leverrier	k1gInSc4	Leverrier
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Struve	Struev	k1gFnSc2	Struev
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1846	[number]	k4	1846
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
název	název	k1gInSc4	název
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
rychle	rychle	k6eAd1	rychle
přijat	přijmout	k5eAaPmNgInS	přijmout
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
zachována	zachován	k2eAgFnSc1d1	zachována
tradice	tradice	k1gFnSc1	tradice
<g/>
,	,	kIx,	,
že	že	k8xS	že
planety	planeta	k1gFnPc1	planeta
jsou	být	k5eAaImIp3nP	být
pojmenovávány	pojmenovávat	k5eAaImNgFnP	pojmenovávat
po	po	k7c6	po
mytologických	mytologický	k2eAgFnPc6d1	mytologická
postavách	postava	k1gFnPc6	postava
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
Neptun	Neptun	k1gInSc1	Neptun
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
astrologii	astrologie	k1gFnSc6	astrologie
žádný	žádný	k3yNgInSc1	žádný
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byla	být	k5eAaImAgNnP	být
objevena	objevit	k5eAaPmNgNnP	objevit
až	až	k6eAd1	až
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
<g/>
,	,	kIx,	,
starým	starý	k2eAgInPc3d1	starý
národům	národ	k1gInPc3	národ
nebyla	být	k5eNaImAgFnS	být
její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
moderních	moderní	k2eAgMnPc2d1	moderní
astrologů	astrolog	k1gMnPc2	astrolog
však	však	k9	však
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
Neptun	Neptun	k1gInSc4	Neptun
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
transpersonální	transpersonální	k2eAgFnSc6d1	transpersonální
<g/>
"	"	kIx"	"
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgInS	spojovat
i	i	k8xC	i
s	s	k7c7	s
celospolečenskými	celospolečenský	k2eAgInPc7d1	celospolečenský
a	a	k8xC	a
dlouhodobými	dlouhodobý	k2eAgInPc7d1	dlouhodobý
procesy	proces	k1gInPc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Překročení	překročení	k1gNnSc1	překročení
hranic	hranice	k1gFnPc2	hranice
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc4	jejich
znejistění	znejistění	k1gNnSc4	znejistění
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
astrologii	astrologie	k1gFnSc6	astrologie
základním	základní	k2eAgInSc7d1	základní
významem	význam	k1gInSc7	význam
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
kladným	kladný	k2eAgNnSc7d1	kladné
i	i	k8xC	i
záporným	záporný	k2eAgNnSc7d1	záporné
znaménkem	znaménko	k1gNnSc7	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
témat	téma	k1gNnPc2	téma
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
planetou	planeta	k1gFnSc7	planeta
tak	tak	k6eAd1	tak
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
nevědomí	nevědomí	k1gNnSc1	nevědomí
a	a	k8xC	a
hypnóza	hypnóza	k1gFnSc1	hypnóza
<g/>
,	,	kIx,	,
iluze	iluze	k1gFnSc1	iluze
<g/>
,	,	kIx,	,
podvody	podvod	k1gInPc1	podvod
<g/>
,	,	kIx,	,
drogy	droga	k1gFnPc1	droga
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
romantismus	romantismus	k1gInSc4	romantismus
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgNnSc4d1	sociální
cítění	cítění	k1gNnSc4	cítění
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgNnSc4d1	náboženské
vytržení	vytržení	k1gNnSc4	vytržení
<g/>
,	,	kIx,	,
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
vlastního	vlastní	k2eAgInSc2d1	vlastní
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
transcendenci	transcendence	k1gFnSc6	transcendence
a	a	k8xC	a
spáse	spása	k1gFnSc6	spása
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
věcí	věc	k1gFnPc2	věc
prozaičtějších	prozaický	k2eAgFnPc2d2	prozaičtější
spadají	spadat	k5eAaPmIp3nP	spadat
pod	pod	k7c4	pod
Neptunův	Neptunův	k2eAgInSc4d1	Neptunův
patronát	patronát	k1gInSc4	patronát
třeba	třeba	k6eAd1	třeba
i	i	k9	i
módní	módní	k2eAgInPc4d1	módní
trendy	trend	k1gInPc4	trend
nebo	nebo	k8xC	nebo
Internet	Internet	k1gInSc4	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
moderní	moderní	k2eAgMnPc1d1	moderní
astrologové	astrolog	k1gMnPc1	astrolog
učinili	učinit	k5eAaImAgMnP	učinit
pokus	pokus	k1gInSc4	pokus
včlenit	včlenit	k5eAaPmF	včlenit
Neptun	Neptun	k1gInSc4	Neptun
i	i	k9	i
do	do	k7c2	do
tradičního	tradiční	k2eAgInSc2d1	tradiční
systému	systém	k1gInSc2	systém
"	"	kIx"	"
<g/>
vládců	vládce	k1gMnPc2	vládce
znamení	znamení	k1gNnSc6	znamení
<g/>
"	"	kIx"	"
a	a	k8xC	a
přiřadili	přiřadit	k5eAaPmAgMnP	přiřadit
mu	on	k3xPp3gMnSc3	on
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
znamením	znamení	k1gNnSc7	znamení
Ryb	Ryby	k1gFnPc2	Ryby
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
však	však	k9	však
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
astrologii	astrologie	k1gFnSc6	astrologie
patří	patřit	k5eAaImIp3nS	patřit
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gMnSc1	Neptun
se	se	k3xPyFc4	se
vyskytl	vyskytnout	k5eAaPmAgMnS	vyskytnout
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
sci-fi	scii	k1gNnSc2	sci-fi
filmů	film	k1gInPc2	film
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
vyskytl	vyskytnout	k5eAaPmAgMnS	vyskytnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
jako	jako	k8xC	jako
neobyvatelná	obyvatelný	k2eNgFnSc1d1	neobyvatelná
ledová	ledový	k2eAgFnSc1d1	ledová
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Spirito	Spirito	k?	Spirito
gentil	gentit	k5eAaPmAgMnS	gentit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
napsal	napsat	k5eAaBmAgMnS	napsat
Olaf	Olaf	k1gMnSc1	Olaf
Stepledon	Stepledon	k1gMnSc1	Stepledon
epický	epický	k2eAgInSc4d1	epický
román	román	k1gInSc4	román
Last	Last	k2eAgInSc4d1	Last
and	and	k?	and
First	First	k1gInSc4	First
Men	Men	k1gMnSc4	Men
<g/>
:	:	kIx,	:
A	a	k9	a
Story	story	k1gFnSc1	story
of	of	k?	of
the	the	k?	the
Near	Near	k1gInSc1	Near
and	and	k?	and
Far	fara	k1gFnPc2	fara
Future	Futur	k1gMnSc5	Futur
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
Neptun	Neptun	k1gInSc1	Neptun
jako	jako	k8xC	jako
domov	domov	k1gInSc1	domov
vyspělé	vyspělý	k2eAgFnSc2d1	vyspělá
lidské	lidský	k2eAgFnSc2d1	lidská
rasy	rasa	k1gFnSc2	rasa
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgMnS	objevovat
jako	jako	k8xS	jako
planeta	planeta	k1gFnSc1	planeta
s	s	k7c7	s
globálním	globální	k2eAgInSc7d1	globální
oceánem	oceán	k1gInSc7	oceán
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
Kapitána	kapitán	k1gMnSc2	kapitán
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vyšel	vyjít	k5eAaPmAgInS	vyjít
román	román	k1gInSc1	román
Nearly	Nearla	k1gFnSc2	Nearla
Neptune	Neptun	k1gInSc5	Neptun
od	od	k7c2	od
Hugha	Hugh	k1gMnSc2	Hugh
Walterse	Walterse	k1gFnSc2	Walterse
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
prvním	první	k4xOgInSc6	první
pilotovaném	pilotovaný	k2eAgInSc6d1	pilotovaný
letu	let	k1gInSc6	let
k	k	k7c3	k
Neptunu	Neptun	k1gMnSc3	Neptun
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
však	však	k9	však
skončí	skončit	k5eAaPmIp3nS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
oheň	oheň	k1gInSc1	oheň
zničí	zničit	k5eAaPmIp3nS	zničit
systémy	systém	k1gInPc4	systém
podpory	podpora	k1gFnSc2	podpora
života	život	k1gInSc2	život
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
Neptun	Neptun	k1gInSc1	Neptun
například	například	k6eAd1	například
ve	v	k7c6	v
sci-fi	scii	k1gNnSc6	sci-fi
hororu	horor	k1gInSc2	horor
Horizont	horizont	k1gInSc1	horizont
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
líčící	líčící	k2eAgInSc1d1	líčící
příběh	příběh	k1gInSc1	příběh
ztracené	ztracený	k2eAgFnSc2d1	ztracená
lodi	loď	k1gFnSc2	loď
za	za	k7c7	za
drahou	draha	k1gFnSc7	draha
Neptunu	Neptun	k1gInSc2	Neptun
a	a	k8xC	a
záchranné	záchranný	k2eAgFnSc2d1	záchranná
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
planety	planeta	k1gFnSc2	planeta
samotné	samotný	k2eAgInPc1d1	samotný
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c4	v
umění	umění	k1gNnSc4	umění
i	i	k8xC	i
její	její	k3xOp3gInPc4	její
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
americký	americký	k2eAgMnSc1d1	americký
autor	autor	k1gMnSc1	autor
Larry	Larra	k1gFnSc2	Larra
Niven	Niven	k1gInSc1	Niven
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Prstenec	prstenec	k1gInSc1	prstenec
situoval	situovat	k5eAaBmAgInS	situovat
na	na	k7c4	na
měsíc	měsíc	k1gInSc4	měsíc
Nereida	nereida	k1gFnSc1	nereida
základnu	základna	k1gFnSc4	základna
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
rasy	rasa	k1gFnSc2	rasa
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
Outsider	outsider	k1gMnSc1	outsider
<g/>
.	.	kIx.	.
</s>
