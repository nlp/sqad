<p>
<s>
Chodník	chodník	k1gInSc1	chodník
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
nebo	nebo	k8xC	nebo
samostatná	samostatný	k2eAgFnSc1d1	samostatná
pozemní	pozemní	k2eAgFnSc1d1	pozemní
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
chodcům	chodec	k1gMnPc3	chodec
k	k	k7c3	k
přesunu	přesun	k1gInSc3	přesun
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Chodníkem	chodník	k1gInSc7	chodník
zpravidla	zpravidla	k6eAd1	zpravidla
bývají	bývat	k5eAaImIp3nP	bývat
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
místní	místní	k2eAgFnPc1d1	místní
komunikace	komunikace	k1gFnPc1	komunikace
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
průjezdní	průjezdní	k2eAgInPc1d1	průjezdní
úseky	úsek	k1gInPc1	úsek
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
zastavěných	zastavěný	k2eAgFnPc6d1	zastavěná
částech	část	k1gFnPc6	část
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
nebo	nebo	k8xC	nebo
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
chodníky	chodník	k1gInPc1	chodník
nazývají	nazývat	k5eAaImIp3nP	nazývat
i	i	k9	i
stezky	stezka	k1gFnPc1	stezka
v	v	k7c6	v
nezastavěné	zastavěný	k2eNgFnSc6d1	nezastavěná
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
ovlivněných	ovlivněný	k2eAgFnPc2d1	ovlivněná
slovenštinou	slovenština	k1gFnSc7	slovenština
<g/>
.	.	kIx.	.
</s>
<s>
Chodník	chodník	k1gInSc1	chodník
je	být	k5eAaImIp3nS	být
také	také	k9	také
častý	častý	k2eAgInSc1d1	častý
a	a	k8xC	a
důležitý	důležitý	k2eAgInSc1d1	důležitý
prvek	prvek	k1gInSc1	prvek
zahradní	zahradní	k2eAgFnSc2d1	zahradní
architektury	architektura	k1gFnSc2	architektura
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
sadovnické	sadovnický	k2eAgFnSc6d1	Sadovnická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavební	stavební	k2eAgNnSc1d1	stavební
provedení	provedení	k1gNnSc1	provedení
==	==	k?	==
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
chodníku	chodník	k1gInSc2	chodník
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
tvořen	tvořit	k5eAaImNgInS	tvořit
asfaltem	asfalt	k1gInSc7	asfalt
<g/>
,	,	kIx,	,
dlažebními	dlažební	k2eAgFnPc7d1	dlažební
kostkami	kostka	k1gFnPc7	kostka
<g/>
,	,	kIx,	,
klasickou	klasický	k2eAgFnSc7d1	klasická
<g/>
,	,	kIx,	,
mozaikovou	mozaikový	k2eAgFnSc7d1	mozaiková
betonovou	betonový	k2eAgFnSc7d1	betonová
či	či	k8xC	či
zahradní	zahradní	k2eAgFnSc7d1	zahradní
dlažbou	dlažba	k1gFnSc7	dlažba
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgInPc7d1	jiný
materiály	materiál	k1gInPc7	materiál
zpevňující	zpevňující	k2eAgFnSc2d1	zpevňující
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chodník	chodník	k1gInSc1	chodník
bývá	bývat	k5eAaImIp3nS	bývat
oproti	oproti	k7c3	oproti
vozovce	vozovka	k1gFnSc3	vozovka
zvýšen	zvýšit	k5eAaPmNgInS	zvýšit
a	a	k8xC	a
od	od	k7c2	od
vozovky	vozovka	k1gFnSc2	vozovka
je	být	k5eAaImIp3nS	být
chodník	chodník	k1gInSc1	chodník
oddělen	oddělit	k5eAaPmNgInS	oddělit
obrubníkem	obrubník	k1gInSc7	obrubník
<g/>
,	,	kIx,	,
pásem	pás	k1gInSc7	pás
zpravidla	zpravidla	k6eAd1	zpravidla
kamenných	kamenný	k2eAgInPc2d1	kamenný
dílů	díl	k1gInPc2	díl
ve	v	k7c6	v
výškové	výškový	k2eAgFnSc6d1	výšková
úrovni	úroveň	k1gFnSc6	úroveň
chodníku	chodník	k1gInSc2	chodník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
silným	silný	k2eAgInSc7d1	silný
provozem	provoz	k1gInSc7	provoz
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
úzkých	úzký	k2eAgInPc2d1	úzký
chodníků	chodník	k1gInPc2	chodník
a	a	k8xC	a
u	u	k7c2	u
východů	východ	k1gInPc2	východ
z	z	k7c2	z
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
chodník	chodník	k1gInSc1	chodník
od	od	k7c2	od
vozovky	vozovka	k1gFnSc2	vozovka
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
i	i	k9	i
zábradlím	zábradlí	k1gNnSc7	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Kryté	krytý	k2eAgNnSc1d1	kryté
zábradlí	zábradlí	k1gNnSc1	zábradlí
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
znečištěním	znečištění	k1gNnSc7	znečištění
chodců	chodec	k1gMnPc2	chodec
nečistotami	nečistota	k1gFnPc7	nečistota
rozstřikovanými	rozstřikovaný	k2eAgFnPc7d1	rozstřikovaná
koly	kola	k1gFnPc4	kola
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
komunikace	komunikace	k1gFnSc2	komunikace
chodník	chodník	k1gInSc1	chodník
obvykle	obvykle	k6eAd1	obvykle
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
k	k	k7c3	k
budovám	budova	k1gFnPc3	budova
<g/>
,	,	kIx,	,
plotům	plot	k1gInPc3	plot
<g/>
,	,	kIx,	,
zdem	zeď	k1gFnPc3	zeď
nebo	nebo	k8xC	nebo
zeleni	zeleň	k1gFnSc3	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
zeleně	zeleň	k1gFnSc2	zeleň
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
trávníku	trávník	k1gInSc3	trávník
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
rovněž	rovněž	k9	rovněž
oddělen	oddělit	k5eAaPmNgInS	oddělit
obrubníkem	obrubník	k1gInSc7	obrubník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
provedení	provedení	k1gNnSc1	provedení
bývá	bývat	k5eAaImIp3nS	bývat
různé	různý	k2eAgNnSc1d1	různé
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mostech	most	k1gInPc6	most
<g/>
,	,	kIx,	,
náspech	násep	k1gInPc6	násep
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
svahů	svah	k1gInPc2	svah
a	a	k8xC	a
v	v	k7c6	v
podobných	podobný	k2eAgNnPc6d1	podobné
místech	místo	k1gNnPc6	místo
bývá	bývat	k5eAaImIp3nS	bývat
chodník	chodník	k1gInSc1	chodník
chráněn	chránit	k5eAaImNgInS	chránit
zábradlím	zábradlí	k1gNnSc7	zábradlí
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
umístěno	umístit	k5eAaPmNgNnS	umístit
stromořadí	stromořadí	k1gNnSc1	stromořadí
<g/>
,	,	kIx,	,
prodejní	prodejní	k2eAgFnSc1d1	prodejní
stánky	stánek	k1gInPc4	stánek
<g/>
,	,	kIx,	,
lavičky	lavička	k1gFnPc4	lavička
<g/>
,	,	kIx,	,
odpadkové	odpadkový	k2eAgInPc4d1	odpadkový
koše	koš	k1gInPc4	koš
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
sloupy	sloup	k1gInPc1	sloup
veřejného	veřejný	k2eAgNnSc2d1	veřejné
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
zastávkové	zastávkový	k2eAgInPc4d1	zastávkový
sloupky	sloupek	k1gInPc4	sloupek
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
platné	platný	k2eAgFnSc2d1	platná
pro	pro	k7c4	pro
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
vozovku	vozovka	k1gFnSc4	vozovka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgInPc1d1	technický
požadavky	požadavek	k1gInPc1	požadavek
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc7d1	základní
technickou	technický	k2eAgFnSc7d1	technická
normou	norma	k1gFnSc7	norma
obsahující	obsahující	k2eAgInPc1d1	obsahující
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
chodníky	chodník	k1gInPc4	chodník
je	být	k5eAaImIp3nS	být
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6110	[number]	k4	6110
Projektování	projektování	k1gNnPc2	projektování
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
nejnovější	nový	k2eAgFnSc1d3	nejnovější
verze	verze	k1gFnSc1	verze
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
technické	technický	k2eAgInPc4d1	technický
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
chodníky	chodník	k1gInPc4	chodník
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
398	[number]	k4	398
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
obecných	obecný	k2eAgInPc6d1	obecný
technických	technický	k2eAgInPc6d1	technický
požadavcích	požadavek	k1gInPc6	požadavek
zabezpečujících	zabezpečující	k2eAgInPc2d1	zabezpečující
bezbariérové	bezbariérový	k2eAgNnSc4d1	bezbariérové
užívání	užívání	k1gNnSc4	užívání
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chodníky	chodník	k1gInPc4	chodník
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
široké	široký	k2eAgNnSc1d1	široké
nejméně	málo	k6eAd3	málo
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Překážky	překážka	k1gFnPc1	překážka
na	na	k7c6	na
komunikacích	komunikace	k1gFnPc6	komunikace
pro	pro	k7c4	pro
pěší	pěší	k2eAgFnSc4d1	pěší
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
stožáry	stožár	k1gInPc7	stožár
veřejného	veřejný	k2eAgNnSc2d1	veřejné
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgInPc1d1	telefonní
automaty	automat	k1gInPc1	automat
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
osazeny	osadit	k5eAaPmNgFnP	osadit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
průchozí	průchozí	k2eAgInSc1d1	průchozí
profil	profil	k1gInSc1	profil
šířky	šířka	k1gFnSc2	šířka
nejméně	málo	k6eAd3	málo
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
hodnotu	hodnota	k1gFnSc4	hodnota
lze	lze	k6eAd1	lze
snížit	snížit	k5eAaPmF	snížit
až	až	k9	až
na	na	k7c4	na
900	[number]	k4	900
mm	mm	kA	mm
u	u	k7c2	u
technického	technický	k2eAgNnSc2d1	technické
vybavení	vybavení	k1gNnSc2	vybavení
komunikací	komunikace	k1gFnPc2	komunikace
s	s	k7c7	s
svislého	svislý	k2eAgNnSc2d1	svislé
dopravního	dopravní	k2eAgNnSc2d1	dopravní
značení	značení	k1gNnSc2	značení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
chodníků	chodník	k1gInPc2	chodník
<g/>
,	,	kIx,	,
schodišť	schodiště	k1gNnPc2	schodiště
<g/>
,	,	kIx,	,
šikmých	šikmý	k2eAgFnPc2d1	šikmá
ramp	rampa	k1gFnPc2	rampa
a	a	k8xC	a
podlah	podlaha	k1gFnPc2	podlaha
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
komunikací	komunikace	k1gFnPc2	komunikace
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
rovný	rovný	k2eAgInSc4d1	rovný
<g/>
,	,	kIx,	,
pevný	pevný	k2eAgInSc4d1	pevný
a	a	k8xC	a
upravený	upravený	k2eAgInSc4d1	upravený
proti	proti	k7c3	proti
skluzu	skluz	k1gInSc3	skluz
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
součinitele	součinitel	k1gInSc2	součinitel
smykového	smykový	k2eAgNnSc2d1	smykové
tření	tření	k1gNnSc2	tření
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejméně	málo	k6eAd3	málo
0,6	[number]	k4	0,6
<g/>
,	,	kIx,	,
u	u	k7c2	u
šikmých	šikmý	k2eAgFnPc2d1	šikmá
ramp	rampa	k1gFnPc2	rampa
pak	pak	k6eAd1	pak
0,6	[number]	k4	0,6
+	+	kIx~	+
tg	tg	kA	tg
α	α	k?	α
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
α	α	k?	α
je	být	k5eAaImIp3nS	být
úhel	úhel	k1gInSc4	úhel
sklonu	sklon	k1gInSc2	sklon
rampy	rampa	k1gFnSc2	rampa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chodníky	chodník	k1gInPc4	chodník
smí	smět	k5eAaImIp3nS	smět
mít	mít	k5eAaImF	mít
podélný	podélný	k2eAgInSc1d1	podélný
sklon	sklon	k1gInSc1	sklon
nejvýše	nejvýše	k6eAd1	nejvýše
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
(	(	kIx(	(
<g/>
8,33	[number]	k4	8,33
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
příčný	příčný	k2eAgInSc1d1	příčný
sklon	sklon	k1gInSc1	sklon
nejvýše	nejvýše	k6eAd1	nejvýše
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
(	(	kIx(	(
<g/>
2,0	[number]	k4	2,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úsecích	úsek	k1gInPc6	úsek
s	s	k7c7	s
podélným	podélný	k2eAgInSc7d1	podélný
sklonem	sklon	k1gInSc7	sklon
větším	veliký	k2eAgInSc7d2	veliký
než	než	k8xS	než
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
(	(	kIx(	(
<g/>
5,0	[number]	k4	5,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
delších	dlouhý	k2eAgInPc2d2	delší
než	než	k8xS	než
200	[number]	k4	200
m	m	kA	m
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zřízena	zřízen	k2eAgNnPc4d1	zřízeno
odpočívadla	odpočívadlo	k1gNnPc4	odpočívadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chodníky	chodník	k1gInPc4	chodník
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
přechodů	přechod	k1gInPc2	přechod
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
snížený	snížený	k2eAgInSc4d1	snížený
obrubník	obrubník	k1gInSc4	obrubník
na	na	k7c4	na
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
20	[number]	k4	20
mm	mm	kA	mm
oproti	oproti	k7c3	oproti
vozovce	vozovka	k1gFnSc3	vozovka
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
opatřeny	opatřen	k2eAgInPc1d1	opatřen
signálními	signální	k2eAgInPc7d1	signální
pásy	pás	k1gInPc7	pás
spojujícími	spojující	k2eAgInPc7d1	spojující
varovné	varovný	k2eAgInPc1d1	varovný
pásy	pás	k1gInPc7	pás
(	(	kIx(	(
<g/>
umístěnými	umístěný	k2eAgInPc7d1	umístěný
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
sníženého	snížený	k2eAgInSc2d1	snížený
obrubníku	obrubník	k1gInSc2	obrubník
<g/>
)	)	kIx)	)
s	s	k7c7	s
vodicími	vodicí	k2eAgFnPc7d1	vodicí
liniemi	linie	k1gFnPc7	linie
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
pro	pro	k7c4	pro
pěší	pěší	k1gMnPc4	pěší
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
řešeny	řešit	k5eAaImNgFnP	řešit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
důsledně	důsledně	k6eAd1	důsledně
dodržena	dodržet	k5eAaPmNgFnS	dodržet
vodicí	vodicí	k2eAgFnSc1d1	vodicí
linie	linie	k1gFnSc1	linie
pro	pro	k7c4	pro
zrakově	zrakově	k6eAd1	zrakově
postižené	postižený	k2eAgFnPc4d1	postižená
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
umělou	umělý	k2eAgFnSc4d1	umělá
vodicí	vodicí	k2eAgFnSc4d1	vodicí
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
-li	i	k?	-li
přirozená	přirozený	k2eAgFnSc1d1	přirozená
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
s	s	k7c7	s
pásem	pás	k1gInSc7	pás
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
se	se	k3xPyFc4	se
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
hmatný	hmatný	k2eAgInSc1d1	hmatný
pás	pás	k1gInSc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Přerušení	přerušení	k1gNnSc1	přerušení
přirozené	přirozený	k2eAgFnSc2d1	přirozená
vodicí	vodicí	k2eAgFnSc2d1	vodicí
linie	linie	k1gFnSc2	linie
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
8000	[number]	k4	8000
mm	mm	kA	mm
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
doplněno	doplnit	k5eAaPmNgNnS	doplnit
vodicí	vodicí	k2eAgFnSc7d1	vodicí
linií	linie	k1gFnSc7	linie
umělou	umělý	k2eAgFnSc7d1	umělá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Právní	právní	k2eAgNnSc1d1	právní
postavení	postavení	k1gNnSc1	postavení
chodníků	chodník	k1gInPc2	chodník
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
§	§	k?	§
12	[number]	k4	12
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
4	[number]	k4	4
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
chodníky	chodník	k1gInPc1	chodník
<g/>
,	,	kIx,	,
chodníky	chodník	k1gInPc1	chodník
pod	pod	k7c7	pod
podloubími	podloubí	k1gNnPc7	podloubí
<g/>
,	,	kIx,	,
podchody	podchod	k1gInPc7	podchod
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
a	a	k8xC	a
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
přechodů	přechod	k1gInPc2	přechod
pro	pro	k7c4	pro
chodce	chodec	k1gMnSc4	chodec
součástí	součást	k1gFnSc7	součást
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
samostatnými	samostatný	k2eAgFnPc7d1	samostatná
místními	místní	k2eAgFnPc7d1	místní
komunikacemi	komunikace	k1gFnPc7	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gMnSc7	jejich
vlastníkem	vlastník	k1gMnSc7	vlastník
vždycky	vždycky	k6eAd1	vždycky
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Formulace	formulace	k1gFnSc1	formulace
zákona	zákon	k1gInSc2	zákon
ale	ale	k8xC	ale
vnáší	vnášet	k5eAaImIp3nS	vnášet
nejasno	jasno	k6eNd1	jasno
ohledně	ohledně	k7c2	ohledně
chodníků	chodník	k1gInPc2	chodník
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
k	k	k7c3	k
silnicím	silnice	k1gFnPc3	silnice
nebo	nebo	k8xC	nebo
účelovým	účelový	k2eAgFnPc3d1	účelová
komunikacím	komunikace	k1gFnPc3	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
§	§	k?	§
34	[number]	k4	34
Zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
128	[number]	k4	128
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
obcích	obec	k1gFnPc6	obec
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
veřejně	veřejně	k6eAd1	veřejně
přístupné	přístupný	k2eAgInPc1d1	přístupný
chodníky	chodník	k1gInPc1	chodník
veřejným	veřejný	k2eAgNnSc7d1	veřejné
prostranstvím	prostranství	k1gNnSc7	prostranství
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
§	§	k?	§
4	[number]	k4	4
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
565	[number]	k4	565
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
místních	místní	k2eAgInPc6d1	místní
poplatcích	poplatek	k1gInPc6	poplatek
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
obec	obec	k1gFnSc1	obec
požadovat	požadovat	k5eAaImF	požadovat
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
užívání	užívání	k1gNnSc1	užívání
veřejného	veřejný	k2eAgNnSc2d1	veřejné
prostranství	prostranství	k1gNnSc2	prostranství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
chodník	chodník	k1gInSc1	chodník
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
publikovaný	publikovaný	k2eAgMnSc1d1	publikovaný
pod	pod	k7c7	pod
č.	č.	k?	č.
211	[number]	k4	211
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
však	však	k9	však
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
poplatek	poplatek	k1gInSc4	poplatek
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
obec	obec	k1gFnSc1	obec
požadovat	požadovat	k5eAaImF	požadovat
od	od	k7c2	od
vlastníka	vlastník	k1gMnSc2	vlastník
chodníku	chodník	k1gInSc2	chodník
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
za	za	k7c4	za
umístění	umístění	k1gNnSc4	umístění
reklamy	reklama	k1gFnSc2	reklama
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
==	==	k?	==
</s>
</p>
<p>
<s>
Chodník	chodník	k1gInSc1	chodník
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Pravidlech	pravidlo	k1gNnPc6	pravidlo
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
výslovně	výslovně	k6eAd1	výslovně
definován	definovat	k5eAaBmNgInS	definovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
chůze	chůze	k1gFnSc2	chůze
po	po	k7c6	po
pozemní	pozemní	k2eAgFnSc6d1	pozemní
komunikaci	komunikace	k1gFnSc6	komunikace
stanoví	stanovit	k5eAaPmIp3nS	stanovit
§	§	k?	§
53	[number]	k4	53
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
silničním	silniční	k2eAgInSc6d1	silniční
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Chodec	chodec	k1gMnSc1	chodec
musí	muset	k5eAaImIp3nS	muset
užívat	užívat	k5eAaImF	užívat
přednostně	přednostně	k6eAd1	přednostně
chodníku	chodník	k1gInSc2	chodník
nebo	nebo	k8xC	nebo
stezky	stezka	k1gFnSc2	stezka
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
pohybující	pohybující	k2eAgFnSc1d1	pohybující
se	se	k3xPyFc4	se
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
,	,	kIx,	,
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
nebo	nebo	k8xC	nebo
obdobném	obdobný	k2eAgNnSc6d1	obdobné
sportovním	sportovní	k2eAgNnSc6d1	sportovní
vybavení	vybavení	k1gNnSc6	vybavení
nesmí	smět	k5eNaImIp3nS	smět
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
nebo	nebo	k8xC	nebo
na	na	k7c6	na
stezce	stezka	k1gFnSc6	stezka
pro	pro	k7c4	pro
chodce	chodec	k1gMnSc4	chodec
ohrozit	ohrozit	k5eAaPmF	ohrozit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
chodce	chodec	k1gMnPc4	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
komunikace	komunikace	k1gFnSc1	komunikace
více	hodně	k6eAd2	hodně
chodníků	chodník	k1gInPc2	chodník
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
chodníku	chodník	k1gInSc2	chodník
má	mít	k5eAaImIp3nS	mít
chodec	chodec	k1gMnSc1	chodec
užít	užít	k5eAaPmF	užít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
straně	strana	k1gFnSc6	strana
chodníku	chodník	k1gInSc2	chodník
je	být	k5eAaImIp3nS	být
chodec	chodec	k1gMnSc1	chodec
povinen	povinen	k2eAgMnSc1d1	povinen
jít	jít	k5eAaImF	jít
–	–	k?	–
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
chodníku	chodník	k1gInSc6	chodník
se	se	k3xPyFc4	se
chodí	chodit	k5eAaImIp3nS	chodit
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
chodníku	chodník	k1gInSc2	chodník
užije	užít	k5eAaPmIp3nS	užít
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
útvar	útvar	k1gInSc1	útvar
chodců	chodec	k1gMnPc2	chodec
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
jít	jít	k5eAaImF	jít
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
nejvýše	nejvýše	k6eAd1	nejvýše
ve	v	k7c6	v
dvojstupu	dvojstup	k1gInSc6	dvojstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Levé	levý	k2eAgFnPc1d1	levá
krajnice	krajnice	k1gFnPc1	krajnice
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
levého	levý	k2eAgInSc2d1	levý
okraje	okraj	k1gInSc2	okraj
vozovky	vozovka	k1gFnSc2	vozovka
může	moct	k5eAaImIp3nS	moct
chodec	chodec	k1gMnSc1	chodec
použít	použít	k5eAaPmF	použít
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
chodník	chodník	k1gInSc1	chodník
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
neschůdný	schůdný	k2eNgMnSc1d1	neschůdný
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
nese	nést	k5eAaImIp3nS	nést
<g/>
-li	i	k?	-li
chodec	chodec	k1gMnSc1	chodec
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
chodce	chodec	k1gMnPc4	chodec
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
ruční	ruční	k2eAgInSc1d1	ruční
nebo	nebo	k8xC	nebo
invalidní	invalidní	k2eAgInSc1d1	invalidní
vozík	vozík	k1gInSc1	vozík
<g/>
,	,	kIx,	,
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
útvar	útvar	k1gInSc1	útvar
chodců	chodec	k1gMnPc2	chodec
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krajnici	krajnice	k1gFnSc6	krajnice
nebo	nebo	k8xC	nebo
při	při	k7c6	při
okraji	okraj	k1gInSc6	okraj
vozovky	vozovka	k1gFnSc2	vozovka
mohou	moct	k5eAaImIp3nP	moct
jít	jít	k5eAaImF	jít
nejvýše	nejvýše	k6eAd1	nejvýše
dva	dva	k4xCgMnPc1	dva
chodci	chodec	k1gMnPc1	chodec
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
v	v	k7c6	v
nebezpečných	bezpečný	k2eNgInPc6d1	nebezpečný
úsecích	úsek	k1gInPc6	úsek
nebo	nebo	k8xC	nebo
obdobích	období	k1gNnPc6	období
pouze	pouze	k6eAd1	pouze
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
účastníci	účastník	k1gMnPc1	účastník
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
než	než	k8xS	než
chodci	chodec	k1gMnPc1	chodec
nesmějí	smát	k5eNaImIp3nP	smát
chodníku	chodník	k1gInSc3	chodník
užívat	užívat	k5eAaImF	užívat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Zákoně	zákon	k1gInSc6	zákon
o	o	k7c6	o
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
§	§	k?	§
53	[number]	k4	53
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
"	"	kIx"	"
<g/>
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
dopravní	dopravní	k2eAgFnSc7d1	dopravní
značkou	značka	k1gFnSc7	značka
zčásti	zčásti	k6eAd1	zčásti
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
povoleno	povolit	k5eAaPmNgNnS	povolit
parkování	parkování	k1gNnSc1	parkování
–	–	k?	–
ani	ani	k8xC	ani
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
však	však	k9	však
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
nesmí	smět	k5eNaImIp3nS	smět
stát	stát	k5eAaImF	stát
vozidlo	vozidlo	k1gNnSc4	vozidlo
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
hmotnosti	hmotnost	k1gFnSc6	hmotnost
převyšující	převyšující	k2eAgFnSc6d1	převyšující
3500	[number]	k4	3500
kg	kg	kA	kg
<g/>
,	,	kIx,	,
jednonápravový	jednonápravový	k2eAgInSc1d1	jednonápravový
traktor	traktor	k1gInSc1	traktor
<g/>
,	,	kIx,	,
motorový	motorový	k2eAgInSc1d1	motorový
ruční	ruční	k2eAgInSc1d1	ruční
vozík	vozík	k1gInSc1	vozík
a	a	k8xC	a
pracovní	pracovní	k2eAgInSc1d1	pracovní
stroj	stroj	k1gInSc1	stroj
samojízdný	samojízdný	k2eAgInSc1d1	samojízdný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chodník	chodník	k1gInSc1	chodník
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k8xC	jako
stezka	stezka	k1gFnSc1	stezka
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
použitá	použitý	k2eAgFnSc1d1	použitá
varianta	varianta	k1gFnSc1	varianta
svislé	svislý	k2eAgFnSc2d1	svislá
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
a	a	k8xC	a
vodorovné	vodorovný	k2eAgNnSc4d1	vodorovné
dopravní	dopravní	k2eAgNnSc4d1	dopravní
značení	značení	k1gNnSc4	značení
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
chodce	chodec	k1gMnPc4	chodec
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
vyhrazeny	vyhrazen	k2eAgInPc4d1	vyhrazen
oddělené	oddělený	k2eAgInPc4d1	oddělený
pruhy	pruh	k1gInPc4	pruh
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gInPc3	on
určena	určit	k5eAaPmNgFnS	určit
celá	celý	k2eAgFnSc1d1	celá
šířka	šířka	k1gFnSc1	šířka
chodníku	chodník	k1gInSc2	chodník
<g/>
.	.	kIx.	.
§	§	k?	§
58	[number]	k4	58
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
nepřímo	přímo	k6eNd1	přímo
povoluje	povolovat	k5eAaImIp3nS	povolovat
po	po	k7c6	po
chodníku	chodník	k1gInSc6	chodník
jízdu	jízda	k1gFnSc4	jízda
dětem	dítě	k1gFnPc3	dítě
mladším	mladý	k2eAgFnPc3d2	mladší
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
bez	bez	k7c2	bez
dohledu	dohled	k1gInSc2	dohled
osoby	osoba	k1gFnSc2	osoba
starší	starý	k2eAgInPc1d2	starší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povinnost	povinnost	k1gFnSc1	povinnost
zajistit	zajistit	k5eAaPmF	zajistit
schůdnost	schůdnost	k1gFnSc4	schůdnost
chodníku	chodník	k1gInSc2	chodník
přísluší	příslušet	k5eAaImIp3nS	příslušet
jeho	jeho	k3xOp3gMnSc3	jeho
vlastníkovi	vlastník	k1gMnSc3	vlastník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Povinnost	povinnost	k1gFnSc1	povinnost
zajistit	zajistit	k5eAaPmF	zajistit
schůdnost	schůdnost	k1gFnSc4	schůdnost
chodníku	chodník	k1gInSc2	chodník
</s>
</p>
<p>
<s>
Chodec	chodec	k1gMnSc1	chodec
</s>
</p>
<p>
<s>
Krajnice	krajnice	k1gFnSc1	krajnice
</s>
</p>
<p>
<s>
Stezka	stezka	k1gFnSc1	stezka
pro	pro	k7c4	pro
chodce	chodec	k1gMnSc4	chodec
</s>
</p>
<p>
<s>
Stezka	stezka	k1gFnSc1	stezka
pro	pro	k7c4	pro
cyklisty	cyklista	k1gMnPc4	cyklista
</s>
</p>
<p>
<s>
Pěší	pěší	k2eAgFnSc1d1	pěší
zóna	zóna	k1gFnSc1	zóna
</s>
</p>
<p>
<s>
Přechod	přechod	k1gInSc1	přechod
pro	pro	k7c4	pro
chodce	chodec	k1gMnSc4	chodec
</s>
</p>
<p>
<s>
Pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
chodník	chodník	k1gInSc1	chodník
(	(	kIx(	(
<g/>
travelátor	travelátor	k1gInSc1	travelátor
<g/>
,	,	kIx,	,
nestupňový	stupňový	k2eNgInSc1d1	stupňový
eskalátor	eskalátor	k1gInSc1	eskalátor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
</s>
</p>
<p>
<s>
Polní	polní	k2eAgFnSc1d1	polní
cesta	cesta	k1gFnSc1	cesta
</s>
</p>
<p>
<s>
Lesní	lesní	k2eAgFnSc1d1	lesní
cesta	cesta	k1gFnSc1	cesta
</s>
</p>
<p>
<s>
Hollywoodský	hollywoodský	k2eAgInSc1d1	hollywoodský
chodník	chodník	k1gInSc1	chodník
slávy	sláva	k1gFnSc2	sláva
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chodník	chodník	k1gInSc1	chodník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chodník	chodník	k1gInSc1	chodník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
6110	[number]	k4	6110
Projektování	projektování	k1gNnPc2	projektování
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
anotace	anotace	k1gFnSc1	anotace
a	a	k8xC	a
náhled	náhled	k1gInSc1	náhled
obsahu	obsah	k1gInSc2	obsah
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
</s>
</p>
