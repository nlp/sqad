<s>
Z	z	k7c2	z
botanického	botanický	k2eAgNnSc2d1	botanické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
čajovník	čajovník	k1gInSc1	čajovník
stálezelený	stálezelený	k2eAgInSc1d1	stálezelený
keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
dorůstající	dorůstající	k2eAgInSc1d1	dorůstající
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
výšky	výška	k1gFnSc2	výška
5	[number]	k4	5
až	až	k9	až
15	[number]	k4	15
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
kolem	kolem	k7c2	kolem
třiceti	třicet	k4xCc2	třicet
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
