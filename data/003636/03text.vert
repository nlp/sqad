<s>
Plagiát	plagiát	k1gInSc1	plagiát
je	být	k5eAaImIp3nS	být
umělecké	umělecký	k2eAgNnSc4d1	umělecké
nebo	nebo	k8xC	nebo
vědecké	vědecký	k2eAgNnSc4d1	vědecké
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
než	než	k8xS	než
skutečný	skutečný	k2eAgMnSc1d1	skutečný
autor	autor	k1gMnSc1	autor
neprávem	neprávo	k1gNnSc7	neprávo
vydává	vydávat	k5eAaImIp3nS	vydávat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
činnost	činnost	k1gFnSc1	činnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plagiace	plagiace	k1gFnSc1	plagiace
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
plagiarismus	plagiarismus	k1gInSc1	plagiarismus
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
autorský	autorský	k2eAgInSc1d1	autorský
zákon	zákon	k1gInSc1	zákon
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
například	například	k6eAd1	například
od	od	k7c2	od
německého	německý	k2eAgMnSc2d1	německý
-	-	kIx~	-
pojem	pojem	k1gInSc1	pojem
plagiát	plagiát	k1gInSc1	plagiát
nepoužívá	používat	k5eNaImIp3nS	používat
a	a	k8xC	a
nedefinuje	definovat	k5eNaBmIp3nS	definovat
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
norma	norma	k1gFnSc1	norma
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
5127-2003	[number]	k4	5127-2003
jej	on	k3xPp3gMnSc4	on
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
představení	představení	k1gNnSc1	představení
duševního	duševní	k2eAgNnSc2d1	duševní
díla	dílo	k1gNnSc2	dílo
jiného	jiný	k2eAgMnSc2d1	jiný
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
půjčeného	půjčený	k2eAgMnSc2d1	půjčený
nebo	nebo	k8xC	nebo
napodobeného	napodobený	k2eAgInSc2d1	napodobený
vcelku	vcelku	k6eAd1	vcelku
nebo	nebo	k8xC	nebo
zčásti	zčásti	k6eAd1	zčásti
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
svého	svůj	k3xOyFgNnSc2	svůj
vlastního	vlastní	k2eAgNnSc2d1	vlastní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
a	a	k8xC	a
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
jedinečným	jedinečný	k2eAgInSc7d1	jedinečný
výsledkem	výsledek	k1gInSc7	výsledek
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
činnosti	činnost	k1gFnSc2	činnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějak	nějak	k6eAd1	nějak
objektivně	objektivně	k6eAd1	objektivně
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
překladu	překlad	k1gInSc2	překlad
<g/>
,	,	kIx,	,
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
databáze	databáze	k1gFnSc2	databáze
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Autorský	autorský	k2eAgInSc1d1	autorský
zákon	zákon	k1gInSc1	zákon
však	však	k9	však
výslovně	výslovně	k6eAd1	výslovně
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
předmětem	předmět	k1gInSc7	předmět
právní	právní	k2eAgFnSc2d1	právní
ochrany	ochrana	k1gFnSc2	ochrana
–	–	k?	–
a	a	k8xC	a
tedy	tedy	k9	tedy
ani	ani	k8xC	ani
plagiace	plagiace	k1gFnSc1	plagiace
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
"	"	kIx"	"
<g/>
zejména	zejména	k9	zejména
námět	námět	k1gInSc1	námět
díla	dílo	k1gNnSc2	dílo
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
denní	denní	k2eAgFnSc1d1	denní
zpráva	zpráva	k1gFnSc1	zpráva
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc1d1	jiný
údaj	údaj	k1gInSc1	údaj
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
objev	objev	k1gInSc1	objev
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgFnSc1d1	vědecká
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
matematický	matematický	k2eAgInSc1d1	matematický
a	a	k8xC	a
obdobný	obdobný	k2eAgInSc1d1	obdobný
vzorec	vzorec	k1gInSc1	vzorec
<g/>
,	,	kIx,	,
statistický	statistický	k2eAgInSc1d1	statistický
graf	graf	k1gInSc1	graf
a	a	k8xC	a
podobný	podobný	k2eAgInSc1d1	podobný
předmět	předmět	k1gInSc1	předmět
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
Slovo	slovo	k1gNnSc1	slovo
plagiát	plagiát	k1gInSc1	plagiát
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
latinského	latinský	k2eAgInSc2d1	latinský
plagiarius	plagiarius	k1gInSc4	plagiarius
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
unesl	unést	k5eAaPmAgInS	unést
svobodnou	svobodný	k2eAgFnSc4d1	svobodná
osobu	osoba	k1gFnSc4	osoba
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
trestalo	trestat	k5eAaImAgNnS	trestat
bičováním	bičování	k1gNnSc7	bičování
(	(	kIx(	(
<g/>
plaga	plaga	k1gFnSc1	plaga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
velmi	velmi	k6eAd1	velmi
silnou	silný	k2eAgFnSc4d1	silná
metaforu	metafora	k1gFnSc4	metafora
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
římský	římský	k2eAgMnSc1d1	římský
básník	básník	k1gMnSc1	básník
Martialis	Martialis	k1gFnSc2	Martialis
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
když	když	k8xS	když
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
Fidentinus	Fidentinus	k1gMnSc1	Fidentinus
recitoval	recitovat	k5eAaImAgMnS	recitovat
jeho	jeho	k3xOp3gFnPc4	jeho
básně	báseň	k1gFnPc4	báseň
jako	jako	k9	jako
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
šířit	šířit	k5eAaImF	šířit
psané	psaný	k2eAgInPc4d1	psaný
texty	text	k1gInPc4	text
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
začali	začít	k5eAaPmAgMnP	začít
autoři	autor	k1gMnPc1	autor
zdůrazňovat	zdůrazňovat	k5eAaImF	zdůrazňovat
dílo	dílo	k1gNnSc4	dílo
jako	jako	k8xS	jako
své	své	k1gNnSc4	své
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
individualistických	individualistický	k2eAgFnPc6d1	individualistická
dobách	doba	k1gFnPc6	doba
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
díla	dílo	k1gNnPc4	dílo
často	často	k6eAd1	často
anonymní	anonymní	k2eAgFnPc1d1	anonymní
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
se	se	k3xPyFc4	se
přetvářela	přetvářet	k5eAaImAgFnS	přetvářet
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
dokonce	dokonce	k9	dokonce
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
šířilo	šířit	k5eAaImAgNnS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
pozdně	pozdně	k6eAd1	pozdně
středověká	středověký	k2eAgFnSc1d1	středověká
učebnice	učebnice	k1gFnSc1	učebnice
malířství	malířství	k1gNnSc2	malířství
Cennina	Cennin	k2eAgMnSc2d1	Cennin
Cenniniho	Cennini	k1gMnSc2	Cennini
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samozřejmé	samozřejmý	k2eAgNnSc4d1	samozřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
začínající	začínající	k2eAgMnSc1d1	začínající
malíř	malíř	k1gMnSc1	malíř
bude	být	k5eAaImBp3nS	být
kopírovat	kopírovat	k5eAaImF	kopírovat
díla	dílo	k1gNnPc4	dílo
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
autorství	autorství	k1gNnSc2	autorství
se	se	k3xPyFc4	se
vyostřila	vyostřit	k5eAaPmAgFnS	vyostřit
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
a	a	k8xC	a
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
duševní	duševní	k2eAgNnSc1d1	duševní
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
samozřejmost	samozřejmost	k1gFnSc4	samozřejmost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
plagiátu	plagiát	k1gInSc2	plagiát
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odlišit	odlišit	k5eAaPmF	odlišit
citát	citát	k1gInSc4	citát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovšem	ovšem	k9	ovšem
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
podle	podle	k7c2	podle
autorského	autorský	k2eAgInSc2d1	autorský
zákona	zákon	k1gInSc2	zákon
"	"	kIx"	"
<g/>
přiměřený	přiměřený	k2eAgInSc1d1	přiměřený
rozsah	rozsah	k1gInSc1	rozsah
<g/>
"	"	kIx"	"
a	a	k8xC	a
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
uvedením	uvedení	k1gNnSc7	uvedení
autora	autor	k1gMnSc2	autor
a	a	k8xC	a
pramene	pramen	k1gInSc2	pramen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
například	například	k6eAd1	například
pro	pro	k7c4	pro
recenzi	recenze	k1gFnSc4	recenze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
a	a	k8xC	a
literárním	literární	k2eAgInSc6d1	literární
provozu	provoz	k1gInSc6	provoz
se	se	k3xPyFc4	se
cituje	citovat	k5eAaBmIp3nS	citovat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
nedbání	nedbání	k1gNnSc2	nedbání
těchto	tento	k3xDgFnPc2	tento
zásad	zásada	k1gFnPc2	zásada
se	se	k3xPyFc4	se
však	však	k9	však
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
přísně	přísně	k6eAd1	přísně
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
přesná	přesný	k2eAgFnSc1d1	přesná
hranice	hranice	k1gFnSc1	hranice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Usvědčený	usvědčený	k2eAgMnSc1d1	usvědčený
plagiátor	plagiátor	k1gMnSc1	plagiátor
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
odbornou	odborný	k2eAgFnSc4d1	odborná
autoritu	autorita	k1gFnSc4	autorita
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
vědeckého	vědecký	k2eAgInSc2d1	vědecký
provozu	provoz	k1gInSc2	provoz
neformálně	formálně	k6eNd1	formálně
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
je	být	k5eAaImIp3nS	být
plagiát	plagiát	k1gInSc1	plagiát
recenzenta	recenzent	k1gMnSc4	recenzent
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
posuzovanou	posuzovaný	k2eAgFnSc4d1	posuzovaná
práci	práce	k1gFnSc4	práce
pozdrží	pozdržet	k5eAaPmIp3nP	pozdržet
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
obsah	obsah	k1gInSc4	obsah
či	či	k8xC	či
myšlenky	myšlenka	k1gFnPc4	myšlenka
sám	sám	k3xTgMnSc1	sám
uveřejní	uveřejnit	k5eAaPmIp3nS	uveřejnit
jako	jako	k9	jako
vlastní	vlastní	k2eAgNnSc1d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
vysokém	vysoký	k2eAgInSc6d1	vysoký
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
plagiáty	plagiát	k1gInPc1	plagiát
dvojího	dvojí	k4xRgMnSc2	dvojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
Studentské	studentský	k2eAgFnPc1d1	studentská
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
při	při	k7c6	při
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zkouškách	zkouška	k1gFnPc6	zkouška
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zejména	zejména	k9	zejména
u	u	k7c2	u
závěrečných	závěrečný	k2eAgFnPc2d1	závěrečná
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
webu	web	k1gInSc2	web
<g/>
.	.	kIx.	.
</s>
<s>
Učitelské	učitelský	k2eAgNnSc4d1	učitelské
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pokud	pokud	k8xS	pokud
učitel	učitel	k1gMnSc1	učitel
zneužije	zneužít	k5eAaPmIp3nS	zneužít
práci	práce	k1gFnSc4	práce
svého	svůj	k3xOyFgMnSc2	svůj
studenta	student	k1gMnSc2	student
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
vlastní	vlastní	k2eAgMnPc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
že	že	k8xS	že
plagiát	plagiát	k1gInSc1	plagiát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
vyloučení	vyloučení	k1gNnSc3	vyloučení
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
k	k	k7c3	k
následnému	následný	k2eAgNnSc3d1	následné
odebrání	odebrání	k1gNnSc3	odebrání
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
český	český	k2eAgInSc1d1	český
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
předepisuje	předepisovat	k5eAaImIp3nS	předepisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
práce	práce	k1gFnSc2	práce
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
písemných	písemný	k2eAgFnPc2d1	písemná
prací	práce	k1gFnPc2	práce
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
kontrola	kontrola	k1gFnSc1	kontrola
velice	velice	k6eAd1	velice
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
proto	proto	k8xC	proto
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
odevzdávání	odevzdávání	k1gNnSc1	odevzdávání
prací	práce	k1gFnPc2	práce
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
počítačové	počítačový	k2eAgFnPc4d1	počítačová
metody	metoda	k1gFnPc4	metoda
odhalování	odhalování	k1gNnSc2	odhalování
plagiátů	plagiát	k1gInPc2	plagiát
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
odebrání	odebrání	k1gNnSc2	odebrání
titulu	titul	k1gInSc2	titul
však	však	k9	však
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
jednoznačně	jednoznačně	k6eAd1	jednoznačně
stanovena	stanovit	k5eAaPmNgFnS	stanovit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
plagiátů	plagiát	k1gInPc2	plagiát
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
systém	systém	k1gInSc1	systém
Theses	Theses	k1gInSc1	Theses
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
provozován	provozovat	k5eAaImNgInS	provozovat
Masarykovou	Masarykův	k2eAgFnSc7d1	Masarykova
univerzitou	univerzita	k1gFnSc7	univerzita
a	a	k8xC	a
který	který	k3yRgInSc4	který
používá	používat	k5eAaImIp3nS	používat
třicítka	třicítka	k1gFnSc1	třicítka
českých	český	k2eAgFnPc2d1	Česká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Obdobou	obdoba	k1gFnSc7	obdoba
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
Odevzdej	odevzdat	k5eAaPmRp2nS	odevzdat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
opět	opět	k6eAd1	opět
provozuje	provozovat	k5eAaImIp3nS	provozovat
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
který	který	k3yQgMnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
plagiátů	plagiát	k1gInPc2	plagiát
v	v	k7c6	v
seminárních	seminární	k2eAgFnPc6d1	seminární
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
hojně	hojně	k6eAd1	hojně
rozšířeno	rozšířen	k2eAgNnSc4d1	rozšířeno
rovněž	rovněž	k9	rovněž
tzv.	tzv.	kA	tzv.
komerční	komerční	k2eAgNnSc1d1	komerční
plagiátorství	plagiátorství	k1gNnSc1	plagiátorství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
určitý	určitý	k2eAgInSc1d1	určitý
komerční	komerční	k2eAgInSc1d1	komerční
subjekt	subjekt	k1gInSc1	subjekt
<g/>
(	(	kIx(	(
<g/>
firma	firma	k1gFnSc1	firma
<g/>
)	)	kIx)	)
vypracuje	vypracovat	k5eAaPmIp3nS	vypracovat
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
studentskou	studentský	k2eAgFnSc4d1	studentská
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
seminární	seminární	k2eAgFnSc7d1	seminární
<g/>
,	,	kIx,	,
bakalářskou	bakalářský	k2eAgFnSc7d1	Bakalářská
<g/>
,	,	kIx,	,
diplomovou	diplomový	k2eAgFnSc7d1	Diplomová
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
student	student	k1gMnSc1	student
následně	následně	k6eAd1	následně
vydává	vydávat	k5eAaPmIp3nS	vydávat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
formy	forma	k1gFnPc1	forma
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
obvykle	obvykle	k6eAd1	obvykle
systém	systém	k1gInSc1	systém
Odevzdej	odevzdat	k5eAaPmRp2nS	odevzdat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
ani	ani	k8xC	ani
Theses	Theses	k1gInSc1	Theses
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
neodhalí	odhalit	k5eNaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
firmy	firma	k1gFnPc1	firma
nabízejí	nabízet	k5eAaImIp3nP	nabízet
zpracování	zpracování	k1gNnSc4	zpracování
originálního	originální	k2eAgNnSc2d1	originální
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Odhalení	odhalení	k1gNnSc1	odhalení
takového	takový	k3xDgInSc2	takový
případu	případ	k1gInSc2	případ
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vyučující	vyučující	k2eAgMnSc1d1	vyučující
nemá	mít	k5eNaImIp3nS	mít
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
plagiátorství	plagiátorství	k1gNnSc4	plagiátorství
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Zamezit	zamezit	k5eAaPmF	zamezit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
způsobu	způsob	k1gInSc3	způsob
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
dalo	dát	k5eAaPmAgNnS	dát
např.	např.	kA	např.
častějšími	častý	k2eAgFnPc7d2	častější
konzultacemi	konzultace	k1gFnPc7	konzultace
studentů	student	k1gMnPc2	student
s	s	k7c7	s
vedoucími	vedoucí	k2eAgMnPc7d1	vedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Sovák	Sovák	k1gMnSc1	Sovák
<g/>
,	,	kIx,	,
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
a	a	k8xC	a
Libor	Libor	k1gMnSc1	Libor
Nedorost	dorůst	k5eNaPmDgInS	dorůst
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
<g/>
,	,	kIx,	,
opisovali	opisovat	k5eAaImAgMnP	opisovat
a	a	k8xC	a
publikovali	publikovat	k5eAaBmAgMnP	publikovat
pod	pod	k7c7	pod
svými	svůj	k3xOyFgNnPc7	svůj
jmény	jméno	k1gNnPc7	jméno
cizí	cizit	k5eAaImIp3nS	cizit
odborné	odborný	k2eAgInPc4d1	odborný
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
"	"	kIx"	"
<g/>
Yarda	Yarda	k1gMnSc1	Yarda
<g/>
"	"	kIx"	"
Helešic	Helešic	k1gMnSc1	Helešic
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
skupiny	skupina	k1gFnSc2	skupina
Support	support	k1gInSc1	support
Lesbiens	Lesbiens	k1gInSc1	Lesbiens
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
písničce	písnička	k1gFnSc6	písnička
In	In	k1gFnPc2	In
Da	Da	k1gFnSc1	Da
Yard	yard	k1gInSc1	yard
motiv	motiv	k1gInSc1	motiv
starší	starý	k2eAgFnSc2d2	starší
písničky	písnička	k1gFnSc2	písnička
Jana	Jan	k1gMnSc2	Jan
Kalouska	Kalousek	k1gMnSc2	Kalousek
Chodím	chodit	k5eAaImIp1nS	chodit
ulicí	ulice	k1gFnSc7	ulice
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Harrison	Harrison	k1gMnSc1	Harrison
byl	být	k5eAaImAgMnS	být
usvědčen	usvědčit	k5eAaPmNgMnS	usvědčit
z	z	k7c2	z
plagiátorství	plagiátorství	k1gNnSc2	plagiátorství
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nechtěného	chtěný	k2eNgInSc2d1	nechtěný
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
Chiffons	Chiffons	k1gInSc1	Chiffons
"	"	kIx"	"
<g/>
He	he	k0	he
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
So	So	kA	So
Fine	Fin	k1gMnSc5	Fin
<g/>
"	"	kIx"	"
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
svého	svůj	k3xOyFgInSc2	svůj
hitu	hit	k1gInSc2	hit
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Sweet	Sweet	k1gMnSc1	Sweet
Lord	lord	k1gMnSc1	lord
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Belgický	belgický	k2eAgMnSc1d1	belgický
skladatel	skladatel	k1gMnSc1	skladatel
Salvatore	Salvator	k1gMnSc5	Salvator
Acquavivo	Acquavivo	k1gNnSc1	Acquavivo
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
soudní	soudní	k2eAgFnSc6d1	soudní
při	pře	k1gFnSc6	pře
proti	proti	k7c3	proti
zpěvačce	zpěvačka	k1gFnSc3	zpěvačka
Madonně	Madonně	k1gFnSc2	Madonně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
albu	album	k1gNnSc6	album
Ray	Ray	k1gFnSc2	Ray
of	of	k?	of
Light	Lighta	k1gFnPc2	Lighta
vydala	vydat	k5eAaPmAgFnS	vydat
písničku	písnička	k1gFnSc4	písnička
"	"	kIx"	"
<g/>
Frozen	Frozen	k2eAgMnSc1d1	Frozen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
jeho	jeho	k3xOp3gFnSc3	jeho
písničce	písnička	k1gFnSc3	písnička
"	"	kIx"	"
<g/>
Ma	Ma	k1gMnSc1	Ma
vie	vie	k?	vie
fout	fout	k1gMnSc1	fout
le	le	k?	le
camp	camp	k1gInSc1	camp
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
raper	raper	k1gMnSc1	raper
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Timbaland	Timbaland	k1gInSc4	Timbaland
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
kritizován	kritizován	k2eAgInSc1d1	kritizován
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
použil	použít	k5eAaPmAgInS	použít
hudební	hudební	k2eAgInSc1d1	hudební
modul	modul	k1gInSc1	modul
finského	finský	k2eAgMnSc2d1	finský
amatérského	amatérský	k2eAgMnSc2d1	amatérský
hudebníka	hudebník	k1gMnSc2	hudebník
Janne	Jann	k1gInSc5	Jann
Sunniho	Sunni	k1gMnSc2	Sunni
v	v	k7c6	v
hitu	hit	k1gInSc6	hit
Nelly	Nella	k1gFnSc2	Nella
Furtado	Furtada	k1gFnSc5	Furtada
"	"	kIx"	"
<g/>
Do	do	k7c2	do
It	It	k1gFnSc2	It
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
MGA	MGA	kA	MGA
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
panenky	panenka	k1gFnSc2	panenka
Bratz	Bratza	k1gFnPc2	Bratza
musí	muset	k5eAaImIp3nS	muset
zastavit	zastavit	k5eAaPmF	zastavit
výrobu	výroba	k1gFnSc4	výroba
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
panenek	panenka	k1gFnPc2	panenka
kvůli	kvůli	k7c3	kvůli
prohře	prohra	k1gFnSc3	prohra
s	s	k7c7	s
konkurenční	konkurenční	k2eAgFnSc7d1	konkurenční
panenkou	panenka	k1gFnSc7	panenka
Barbie	Barbie	k1gFnSc2	Barbie
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
drží	držet	k5eAaImIp3nS	držet
autorská	autorský	k2eAgFnSc1d1	autorská
práva	práv	k2eAgFnSc1d1	práva
společnost	společnost	k1gFnSc1	společnost
Mattel	Mattela	k1gFnPc2	Mattela
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
měly	mít	k5eAaImAgFnP	mít
panenky	panenka	k1gFnPc1	panenka
Bratz	Bratz	k1gInSc4	Bratz
zmizet	zmizet	k5eAaPmF	zmizet
z	z	k7c2	z
pultů	pult	k1gInPc2	pult
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
obchodech	obchod	k1gInPc6	obchod
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Proděkan	proděkan	k1gMnSc1	proděkan
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Západočeské	západočeský	k2eAgFnSc2d1	Západočeská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
Ivan	Ivan	k1gMnSc1	Ivan
Tomažič	Tomažič	k1gMnSc1	Tomažič
opsal	opsat	k5eAaPmAgMnS	opsat
desítky	desítka	k1gFnPc4	desítka
stran	strana	k1gFnPc2	strana
své	svůj	k3xOyFgFnSc2	svůj
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
oponentský	oponentský	k2eAgInSc4d1	oponentský
posudek	posudek	k1gInSc4	posudek
disertace	disertace	k1gFnSc2	disertace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
děkan	děkan	k1gMnSc1	děkan
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
plagiátem	plagiát	k1gInSc7	plagiát
<g/>
.	.	kIx.	.
</s>
<s>
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
Jitky	Jitka	k1gFnSc2	Jitka
Wiszczorové	Wiszczorová	k1gFnSc2	Wiszczorová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
komise	komise	k1gFnSc1	komise
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
plagiát	plagiát	k1gInSc4	plagiát
upozorněna	upozornit	k5eAaPmNgFnS	upozornit
<g/>
,	,	kIx,	,
obhájena	obhájit	k5eAaPmNgFnS	obhájit
na	na	k7c4	na
výbornou	výborná	k1gFnSc4	výborná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
podnikání	podnikání	k1gNnSc2	podnikání
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
ignorovala	ignorovat	k5eAaImAgFnS	ignorovat
dodané	dodaný	k2eAgInPc4d1	dodaný
odkazy	odkaz	k1gInPc4	odkaz
a	a	k8xC	a
argumentovala	argumentovat	k5eAaImAgFnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
Theses	Theses	k1gInSc1	Theses
neodhalil	odhalit	k5eNaPmAgInS	odhalit
shodu	shoda	k1gFnSc4	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Převzatých	převzatý	k2eAgInPc2d1	převzatý
textů	text	k1gInPc2	text
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
zkušební	zkušební	k2eAgFnSc2d1	zkušební
komise	komise	k1gFnSc2	komise
s	s	k7c7	s
reportéry	reportér	k1gMnPc7	reportér
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
mluvit	mluvit	k5eAaImF	mluvit
<g/>
;	;	kIx,	;
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
byl	být	k5eAaImAgMnS	být
rektor	rektor	k1gMnSc1	rektor
VŠP	VŠP	kA	VŠP
Vladimír	Vladimír	k1gMnSc1	Vladimír
Krajčík	Krajčík	k1gMnSc1	Krajčík
<g/>
,	,	kIx,	,
redaktora	redaktor	k1gMnSc2	redaktor
ČT	ČT	kA	ČT
osobně	osobně	k6eAd1	osobně
vykázal	vykázat	k5eAaPmAgMnS	vykázat
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
svůj	svůj	k3xOyFgInSc4	svůj
webový	webový	k2eAgInSc4d1	webový
portál	portál	k1gInSc4	portál
téměř	téměř	k6eAd1	téměř
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
okopírovala	okopírovat	k5eAaPmAgFnS	okopírovat
podle	podle	k7c2	podle
americké	americký	k2eAgFnSc2d1	americká
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
CBS	CBS	kA	CBS
<g/>
.	.	kIx.	.
</s>
