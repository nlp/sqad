<p>
<s>
Koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
tramvajovým	tramvajový	k2eAgInSc7d1	tramvajový
systémem	systém	k1gInSc7	systém
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
pátým	pátý	k4xOgNnSc7	pátý
městem	město	k1gNnSc7	město
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6	Rakousku-Uhersko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zavedlo	zavést	k5eAaPmAgNnS	zavést
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Dopravu	doprava	k1gFnSc4	doprava
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Brünner	Brünnra	k1gFnPc2	Brünnra
Tramway	Tramwaa	k1gFnSc2	Tramwaa
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
für	für	k?	für
Personen-	Personen-	k1gMnSc1	Personen-
und	und	k?	und
Frachten	Frachten	k2eAgMnSc1d1	Frachten
Verkehr	Verkehr	k1gMnSc1	Verkehr
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nerentabilitě	nerentabilita	k1gFnSc3	nerentabilita
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
etapa	etapa	k1gFnSc1	etapa
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koňky	koňka	k1gFnSc2	koňka
je	být	k5eAaImIp3nS	být
datována	datovat	k5eAaImNgFnS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1876	[number]	k4	1876
až	až	k6eAd1	až
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jediná	jediný	k2eAgFnSc1d1	jediná
trať	trať	k1gFnSc1	trať
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
společnosti	společnost	k1gFnSc2	společnost
Brünner	Brünnra	k1gFnPc2	Brünnra
Tramway-Unternehmung	Tramway-Unternehmung	k1gMnSc1	Tramway-Unternehmung
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
několikaleté	několikaletý	k2eAgFnSc6d1	několikaletá
přestávce	přestávka	k1gFnSc6	přestávka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
také	také	k9	také
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
animálním	animální	k2eAgInSc7d1	animální
provozem	provoz	k1gInSc7	provoz
<g/>
,	,	kIx,	,
provoz	provoz	k1gInSc1	provoz
této	tento	k3xDgFnSc2	tento
koňky	koňka	k1gFnSc2	koňka
ale	ale	k8xC	ale
už	už	k6eAd1	už
nebyl	být	k5eNaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
letech	let	k1gInPc6	let
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1874	[number]	k4	1874
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
tratí	trať	k1gFnPc2	trať
===	===	k?	===
</s>
</p>
<p>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tramvaje	tramvaj	k1gFnSc2	tramvaj
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
staly	stát	k5eAaPmAgInP	stát
omnibusy	omnibus	k1gInPc1	omnibus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
totiž	totiž	k8xC	totiž
jezdily	jezdit	k5eAaImAgFnP	jezdit
dvě	dva	k4xCgFnPc1	dva
omnibusové	omnibusový	k2eAgFnPc1d1	omnibusová
linky	linka	k1gFnPc1	linka
<g/>
:	:	kIx,	:
Olomoucká	olomoucký	k2eAgFnSc1d1	olomoucká
–	–	k?	–
Lužánky	Lužánek	k1gInPc4	Lužánek
a	a	k8xC	a
Velké	velký	k2eAgNnSc4d1	velké
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
náměstí	náměstí	k1gNnSc1	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
–	–	k?	–
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spojovaly	spojovat	k5eAaImAgFnP	spojovat
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
s	s	k7c7	s
dělnickými	dělnický	k2eAgNnPc7d1	dělnické
předměstími	předměstí	k1gNnPc7	předměstí
vystavěnými	vystavěný	k2eAgNnPc7d1	vystavěné
podél	podél	k7c2	podél
radiálních	radiální	k2eAgFnPc2d1	radiální
cest	cesta	k1gFnPc2	cesta
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
začala	začít	k5eAaPmAgFnS	začít
koňská	koňský	k2eAgFnSc1d1	koňská
tramvaj	tramvaj	k1gFnSc1	tramvaj
jezdit	jezdit	k5eAaImF	jezdit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
tramvajové	tramvajový	k2eAgInPc1d1	tramvajový
systémy	systém	k1gInPc1	systém
následovaly	následovat	k5eAaImAgInP	následovat
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
původnímu	původní	k2eAgNnSc3d1	původní
městu	město	k1gNnSc3	město
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
středověkých	středověký	k2eAgFnPc2d1	středověká
hradeb	hradba	k1gFnPc2	hradba
připojeny	připojit	k5eAaPmNgFnP	připojit
předměstské	předměstský	k2eAgFnPc1d1	předměstská
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
zde	zde	k6eAd1	zde
během	během	k7c2	během
několika	několik	k4yIc2	několik
staletí	staletí	k1gNnPc2	staletí
vyrostly	vyrůst	k5eAaPmAgInP	vyrůst
<g/>
)	)	kIx)	)
proto	proto	k8xC	proto
mělo	mít	k5eAaImAgNnS	mít
předpoklady	předpoklad	k1gInPc4	předpoklad
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
páté	pátý	k4xOgFnSc2	pátý
městské	městský	k2eAgFnSc2d1	městská
kolejové	kolejový	k2eAgFnSc2d1	kolejová
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
habsburském	habsburský	k2eAgNnSc6d1	habsburské
mocnářství	mocnářství	k1gNnSc6	mocnářství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
bylo	být	k5eAaImAgNnS	být
podáno	podat	k5eAaPmNgNnS	podat
několik	několik	k4yIc1	několik
návrhů	návrh	k1gInPc2	návrh
pro	pro	k7c4	pro
zřízení	zřízení	k1gNnSc4	zřízení
tratí	tratit	k5eAaImIp3nS	tratit
tramvaje	tramvaj	k1gFnPc4	tramvaj
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
projednala	projednat	k5eAaPmAgFnS	projednat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1869	[number]	k4	1869
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
Brnu	Brno	k1gNnSc3	Brno
udělena	udělit	k5eAaPmNgFnS	udělit
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
obchodu	obchod	k1gInSc2	obchod
koncese	koncese	k1gFnSc2	koncese
k	k	k7c3	k
provozování	provozování	k1gNnSc3	provozování
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
železnice	železnice	k1gFnSc2	železnice
tzv.	tzv.	kA	tzv.
amerického	americký	k2eAgInSc2d1	americký
systému	systém	k1gInSc2	systém
pro	pro	k7c4	pro
sedm	sedm	k4xCc4	sedm
vzájemně	vzájemně	k6eAd1	vzájemně
navazujících	navazující	k2eAgInPc2d1	navazující
traťových	traťový	k2eAgInPc2d1	traťový
úseků	úsek	k1gInPc2	úsek
<g/>
,	,	kIx,	,
počítalo	počítat	k5eAaImAgNnS	počítat
se	se	k3xPyFc4	se
také	také	k9	také
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
vozovnami	vozovna	k1gFnPc7	vozovna
<g/>
.	.	kIx.	.
<g/>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Brünner	Brünnra	k1gFnPc2	Brünnra
Tramway	Tramwaa	k1gFnSc2	Tramwaa
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
für	für	k?	für
Personen-	Personen-	k1gFnSc2	Personen-
und	und	k?	und
Frachten	Frachten	k2eAgInSc4d1	Frachten
Verkehr	Verkehr	k1gInSc4	Verkehr
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
a	a	k8xC	a
nákladní	nákladní	k2eAgFnSc4d1	nákladní
dopravu	doprava	k1gFnSc4	doprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
akcionáři	akcionář	k1gMnPc7	akcionář
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
brněnští	brněnský	k2eAgMnPc1d1	brněnský
<g/>
,	,	kIx,	,
vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
i	i	k8xC	i
pešťští	pešťský	k2eAgMnPc1d1	pešťský
podnikatelé	podnikatel	k1gMnPc1	podnikatel
<g/>
,	,	kIx,	,
zmocněncem	zmocněnec	k1gMnSc7	zmocněnec
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
úřady	úřad	k1gInPc7	úřad
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
brněnský	brněnský	k2eAgMnSc1d1	brněnský
advokát	advokát	k1gMnSc1	advokát
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heschl	Heschl	k1gMnSc1	Heschl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
podal	podat	k5eAaPmAgMnS	podat
městu	město	k1gNnSc3	město
první	první	k4xOgInSc4	první
návrh	návrh	k1gInSc4	návrh
koňky	koňka	k1gFnSc2	koňka
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
měla	mít	k5eAaImAgFnS	mít
kapitál	kapitál	k1gInSc4	kapitál
jeden	jeden	k4xCgInSc4	jeden
milion	milion	k4xCgInSc4	milion
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
začala	začít	k5eAaPmAgFnS	začít
společnost	společnost	k1gFnSc1	společnost
stavět	stavět	k5eAaImF	stavět
první	první	k4xOgFnSc1	první
trať	trať	k1gFnSc1	trať
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
dráhy	dráha	k1gFnSc2	dráha
od	od	k7c2	od
Kiosku	kiosek	k1gInSc2	kiosek
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
po	po	k7c6	po
černohorské	černohorský	k2eAgFnSc6d1	černohorská
státní	státní	k2eAgFnSc6d1	státní
silnici	silnice	k1gFnSc6	silnice
do	do	k7c2	do
tehdy	tehdy	k6eAd1	tehdy
samostatného	samostatný	k2eAgInSc2d1	samostatný
městyse	městys	k1gInSc2	městys
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
často	často	k6eAd1	často
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
Kartouzy	Kartouzy	k1gInPc1	Kartouzy
<g/>
)	)	kIx)	)
k	k	k7c3	k
hostinci	hostinec	k1gInSc3	hostinec
Semilasso	Semilassa	k1gFnSc5	Semilassa
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
dvoře	dvůr	k1gInSc6	dvůr
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
nacházet	nacházet	k5eAaImF	nacházet
vozovna	vozovna	k1gFnSc1	vozovna
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
tramvajovou	tramvajový	k2eAgFnSc7d1	tramvajová
společností	společnost	k1gFnSc7	společnost
a	a	k8xC	a
městem	město	k1gNnSc7	město
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
až	až	k9	až
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
dnů	den	k1gInPc2	den
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
totiž	totiž	k9	totiž
požádalo	požádat	k5eAaPmAgNnS	požádat
Vídeň	Vídeň	k1gFnSc4	Vídeň
o	o	k7c4	o
opis	opis	k1gInSc4	opis
tamní	tamní	k2eAgFnSc2d1	tamní
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
provozní	provozní	k2eAgInPc1d1	provozní
předpisy	předpis	k1gInPc1	předpis
a	a	k8xC	a
poznatky	poznatek	k1gInPc1	poznatek
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
předvedení	předvedení	k1gNnSc1	předvedení
šesti	šest	k4xCc2	šest
letních	letní	k2eAgInPc2d1	letní
vozů	vůz	k1gInPc2	vůz
městskému	městský	k2eAgInSc3d1	městský
úřadu	úřad	k1gInSc3	úřad
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejný	stejný	k2eAgInSc1d1	stejný
typ	typ	k1gInSc1	typ
vozidel	vozidlo	k1gNnPc2	vozidlo
jezdí	jezdit	k5eAaImIp3nS	jezdit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
s	s	k7c7	s
koňmi	kůň	k1gMnPc7	kůň
byly	být	k5eAaImAgFnP	být
deponovány	deponován	k2eAgFnPc4d1	deponována
ve	v	k7c6	v
vozovně	vozovna	k1gFnSc6	vozovna
vytvořené	vytvořený	k2eAgFnSc6d1	vytvořená
u	u	k7c2	u
hostince	hostinec	k1gInSc2	hostinec
Semilasso	Semilassa	k1gFnSc5	Semilassa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Provoz	provoz	k1gInSc1	provoz
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1869	[number]	k4	1869
na	na	k7c4	na
převážně	převážně	k6eAd1	převážně
jednokolejné	jednokolejný	k2eAgInPc4d1	jednokolejný
normálněrozchodné	normálněrozchodný	k2eAgInPc4d1	normálněrozchodný
(	(	kIx(	(
<g/>
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
)	)	kIx)	)
trati	trať	k1gFnSc2	trať
Kiosk	kiosk	k1gInSc1	kiosk
–	–	k?	–
Kartouzy	kartouza	k1gFnSc2	kartouza
s	s	k7c7	s
výhybnami	výhybna	k1gFnPc7	výhybna
<g/>
;	;	kIx,	;
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Kiosk	kiosk	k1gInSc1	kiosk
–	–	k?	–
Mýto	mýto	k1gNnSc1	mýto
(	(	kIx(	(
<g/>
křižovatka	křižovatka	k1gFnSc1	křižovatka
dnešních	dnešní	k2eAgFnPc2d1	dnešní
ulic	ulice	k1gFnPc2	ulice
Lidická	lidický	k2eAgFnSc1d1	Lidická
–	–	k?	–
Štefánikova	Štefánikův	k2eAgFnSc1d1	Štefánikova
–	–	k?	–
Pionýrská	pionýrský	k2eAgFnSc1d1	Pionýrská
–	–	k?	–
Kotlářská	kotlářský	k2eAgFnSc1d1	Kotlářská
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
<g/>
.	.	kIx.	.
</s>
<s>
Tramvaje	tramvaj	k1gFnPc1	tramvaj
jezdily	jezdit	k5eAaImAgFnP	jezdit
od	od	k7c2	od
šesti	šest	k4xCc2	šest
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
deseti	deset	k4xCc2	deset
večer	večer	k6eAd1	večer
<g/>
,	,	kIx,	,
interval	interval	k1gInSc4	interval
mezi	mezi	k7c7	mezi
spoji	spoj	k1gInPc7	spoj
byl	být	k5eAaImAgInS	být
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
do	do	k7c2	do
devíti	devět	k4xCc2	devět
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
hodin	hodina	k1gFnPc2	hodina
odpoledne	odpoledne	k6eAd1	odpoledne
a	a	k8xC	a
od	od	k7c2	od
pěti	pět	k4xCc2	pět
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
byl	být	k5eAaImAgInS	být
interval	interval	k1gInSc4	interval
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdné	k1gNnSc4	jízdné
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
jízdu	jízda	k1gFnSc4	jízda
činilo	činit	k5eAaImAgNnS	činit
15	[number]	k4	15
krejcarů	krejcar	k1gInPc2	krejcar
<g/>
,	,	kIx,	,
za	za	k7c4	za
dítě	dítě	k1gNnSc4	dítě
do	do	k7c2	do
10	[number]	k4	10
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
platilo	platit	k5eAaImAgNnS	platit
7,5	[number]	k4	7,5
krejcaru	krejcar	k1gInSc2	krejcar
<g/>
.	.	kIx.	.
</s>
<s>
Výpravna	výpravna	k1gFnSc1	výpravna
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc1d1	dopravní
kancelář	kancelář	k1gFnSc1	kancelář
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
Kiosku	kiosek	k1gInSc6	kiosek
<g/>
.	.	kIx.	.
</s>
<s>
Kolejnice	kolejnice	k1gFnPc1	kolejnice
byly	být	k5eAaImAgFnP	být
tvořeny	tvořen	k2eAgFnPc4d1	tvořena
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
podélnými	podélný	k2eAgInPc7d1	podélný
trámci	trámec	k1gInPc7	trámec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
okuty	okout	k5eAaPmNgInP	okout
železnými	železný	k2eAgFnPc7d1	železná
pásy	pás	k1gInPc1	pás
se	s	k7c7	s
žlábky	žlábek	k1gInPc7	žlábek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kolejnice	kolejnice	k1gFnPc1	kolejnice
potom	potom	k6eAd1	potom
ležely	ležet	k5eAaImAgFnP	ležet
na	na	k7c6	na
příčných	příčný	k2eAgInPc6d1	příčný
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
pražcích	pražec	k1gInPc6	pražec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
výstavba	výstavba	k1gFnSc1	výstavba
dalších	další	k2eAgFnPc2d1	další
tratí	trať	k1gFnPc2	trať
podle	podle	k7c2	podle
schváleného	schválený	k2eAgInSc2d1	schválený
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Kiosku	kiosek	k1gInSc2	kiosek
přes	přes	k7c4	přes
Radwitovo	Radwitův	k2eAgNnSc4d1	Radwitův
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Žerotínovo	Žerotínův	k2eAgNnSc1d1	Žerotínovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
nově	nově	k6eAd1	nově
postaveného	postavený	k2eAgInSc2d1	postavený
Červeného	Červeného	k2eAgInSc2d1	Červeného
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
po	po	k7c6	po
Eliščině	Eliščin	k2eAgFnSc6d1	Eliščina
třídě	třída	k1gFnSc6	třída
(	(	kIx(	(
<g/>
Husově	Husův	k2eAgFnSc6d1	Husova
<g/>
)	)	kIx)	)
k	k	k7c3	k
Městskému	městský	k2eAgInSc3d1	městský
dvoru	dvůr	k1gInSc3	dvůr
(	(	kIx(	(
<g/>
Šilingrovo	Šilingrův	k2eAgNnSc1d1	Šilingrovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
tratě	trať	k1gFnSc2	trať
i	i	k9	i
krátká	krátký	k2eAgFnSc1d1	krátká
odbočka	odbočka	k1gFnSc1	odbočka
po	po	k7c6	po
Raduitově	Raduitův	k2eAgFnSc6d1	Raduitův
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Marešově	Marešův	k2eAgFnSc6d1	Marešova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
druhá	druhý	k4xOgFnSc1	druhý
vozovna	vozovna	k1gFnSc1	vozovna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1869	[number]	k4	1869
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
trať	trať	k1gFnSc1	trať
Kiosk	kiosk	k1gInSc1	kiosk
–	–	k?	–
Koliště	koliště	k1gNnSc2	koliště
–	–	k?	–
Celnice	celnice	k1gFnSc2	celnice
(	(	kIx(	(
<g/>
Malinovského	Malinovský	k2eAgNnSc2d1	Malinovské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
–	–	k?	–
Nádraží	nádraží	k1gNnSc2	nádraží
–	–	k?	–
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
Mendlovo	Mendlův	k2eAgNnSc1d1	Mendlovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
koncový	koncový	k2eAgInSc1d1	koncový
úsek	úsek	k1gInSc1	úsek
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
jednokolejný	jednokolejný	k2eAgMnSc1d1	jednokolejný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInPc1d1	další
nové	nový	k2eAgInPc1d1	nový
úseky	úsek	k1gInPc1	úsek
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
začaly	začít	k5eAaPmAgFnP	začít
tramvaje	tramvaj	k1gFnPc1	tramvaj
jezdit	jezdit	k5eAaImF	jezdit
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Malinovského	Malinovský	k2eAgNnSc2d1	Malinovské
náměstí	náměstí	k1gNnSc2	náměstí
po	po	k7c6	po
Cejlu	Cejl	k1gInSc6	Cejl
do	do	k7c2	do
Zábrdovic	Zábrdovice	k1gFnPc2	Zábrdovice
(	(	kIx(	(
<g/>
po	po	k7c4	po
křižovatku	křižovatka	k1gFnSc4	křižovatka
s	s	k7c7	s
ulicí	ulice	k1gFnSc7	ulice
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
<g/>
;	;	kIx,	;
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
trať	trať	k1gFnSc1	trať
s	s	k7c7	s
výhybnami	výhybna	k1gFnPc7	výhybna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
trať	trať	k1gFnSc1	trať
Staré	Stará	k1gFnSc2	Stará
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Hlinky	hlinka	k1gFnSc2	hlinka
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
(	(	kIx(	(
<g/>
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
byl	být	k5eAaImAgInS	být
jednokolejný	jednokolejný	k2eAgMnSc1d1	jednokolejný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
konečné	konečná	k1gFnSc6	konečná
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
třetí	třetí	k4xOgFnSc1	třetí
vozovna	vozovna	k1gFnSc1	vozovna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
nákladní	nákladní	k2eAgFnSc1d1	nákladní
trať	trať	k1gFnSc1	trať
od	od	k7c2	od
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
k	k	k7c3	k
Rosickému	rosický	k2eAgNnSc3d1	rosické
nádraží	nádraží	k1gNnSc3	nádraží
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
výstavba	výstavba	k1gFnSc1	výstavba
sítě	síť	k1gFnSc2	síť
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koňky	koňka	k1gFnSc2	koňka
dokončena	dokončen	k2eAgFnSc1d1	dokončena
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
činila	činit	k5eAaImAgFnS	činit
14,15	[number]	k4	14,15
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
návrh	návrh	k1gInSc1	návrh
počítal	počítat	k5eAaImAgInS	počítat
i	i	k9	i
s	s	k7c7	s
tratí	trať	k1gFnSc7	trať
od	od	k7c2	od
nádraží	nádraží	k1gNnSc2	nádraží
po	po	k7c6	po
Křenové	křenový	k2eAgFnSc6d1	Křenová
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
ale	ale	k9	ale
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
jezdily	jezdit	k5eAaImAgFnP	jezdit
tramvaje	tramvaj	k1gFnPc1	tramvaj
na	na	k7c6	na
čtyřech	čtyři	k4xCgFnPc6	čtyři
linkách	linka	k1gFnPc6	linka
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInPc1d1	dnešní
názvy	název	k1gInPc1	název
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
–	–	k?	–
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
–	–	k?	–
Šilingrovo	Šilingrův	k2eAgNnSc4d1	Šilingrovo
náměstí	náměstí	k1gNnSc4	náměstí
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
–	–	k?	–
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
–	–	k?	–
Cejl	Cejl	k1gInSc1	Cejl
–	–	k?	–
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
</s>
</p>
<p>
<s>
===	===	k?	===
Úpadek	úpadek	k1gInSc4	úpadek
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
zastavení	zastavení	k1gNnSc4	zastavení
dopravy	doprava	k1gFnSc2	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
provozu	provoz	k1gInSc2	provoz
byl	být	k5eAaImAgInS	být
zejména	zejména	k9	zejména
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
a	a	k8xC	a
o	o	k7c6	o
nedělích	neděle	k1gFnPc6	neděle
provoz	provoz	k1gInSc1	provoz
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
totiž	totiž	k9	totiž
často	často	k6eAd1	často
cestovali	cestovat	k5eAaImAgMnP	cestovat
novým	nový	k2eAgInSc7d1	nový
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
za	za	k7c7	za
rekreací	rekreace	k1gFnSc7	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
nastaly	nastat	k5eAaPmAgInP	nastat
ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
letní	letní	k2eAgInPc4d1	letní
výlety	výlet	k1gInPc4	výlet
(	(	kIx(	(
<g/>
především	především	k9	především
do	do	k7c2	do
Pisárek	Pisárka	k1gFnPc2	Pisárka
ke	k	k7c3	k
Svratce	Svratka	k1gFnSc3	Svratka
<g/>
)	)	kIx)	)
Brňanů	Brňan	k1gMnPc2	Brňan
ubývaly	ubývat	k5eAaImAgFnP	ubývat
a	a	k8xC	a
pro	pro	k7c4	pro
běžné	běžný	k2eAgMnPc4d1	běžný
dělníky	dělník	k1gMnPc4	dělník
bylo	být	k5eAaImAgNnS	být
každodenní	každodenní	k2eAgNnSc4d1	každodenní
dojíždění	dojíždění	k1gNnSc4	dojíždění
tramvají	tramvaj	k1gFnPc2	tramvaj
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgNnSc1d1	drahé
(	(	kIx(	(
<g/>
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
čtvrtí	čtvrt	k1gFnPc2	čtvrt
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
továren	továrna	k1gFnPc2	továrna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úbytek	úbytek	k1gInSc1	úbytek
cestujících	cestující	k1gMnPc2	cestující
byl	být	k5eAaImAgInS	být
také	také	k9	také
částečně	částečně	k6eAd1	částečně
zapříčiněn	zapříčinit	k5eAaPmNgMnS	zapříčinit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
trať	trať	k1gFnSc1	trať
původní	původní	k2eAgFnSc1d1	původní
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
pouze	pouze	k6eAd1	pouze
obepínala	obepínat	k5eAaImAgFnS	obepínat
<g/>
,	,	kIx,	,
koleje	kolej	k1gFnPc1	kolej
do	do	k7c2	do
úplného	úplný	k2eAgInSc2d1	úplný
středu	střed	k1gInSc2	střed
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zavedeny	zaveden	k2eAgInPc4d1	zaveden
nebyly	být	k5eNaImAgInP	být
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
úzkým	úzký	k2eAgFnPc3d1	úzká
a	a	k8xC	a
křivolakým	křivolaký	k2eAgFnPc3d1	křivolaká
ulicím	ulice	k1gFnPc3	ulice
<g/>
,	,	kIx,	,
asanace	asanace	k1gFnSc1	asanace
jádra	jádro	k1gNnSc2	jádro
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k6eAd1	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zřejmě	zřejmě	k6eAd1	zřejmě
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1872	[number]	k4	1872
byl	být	k5eAaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
od	od	k7c2	od
Kiosku	kiosek	k1gInSc2	kiosek
k	k	k7c3	k
Městskému	městský	k2eAgInSc3d1	městský
dvoru	dvůr	k1gInSc3	dvůr
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
cestujícími	cestující	k1gMnPc7	cestující
využívána	využívat	k5eAaPmNgNnP	využívat
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
května	květen	k1gInSc2	květen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
vytrhány	vytrhán	k2eAgFnPc1d1	vytrhána
koleje	kolej	k1gFnPc1	kolej
a	a	k8xC	a
zrušena	zrušen	k2eAgFnSc1d1	zrušena
vozovna	vozovna	k1gFnSc1	vozovna
v	v	k7c6	v
Raduitově	Raduitův	k2eAgFnSc6d1	Raduitův
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
i	i	k9	i
likvidace	likvidace	k1gFnSc1	likvidace
nákladní	nákladní	k2eAgFnSc2d1	nákladní
trati	trať	k1gFnSc2	trať
spojující	spojující	k2eAgFnSc2d1	spojující
obě	dva	k4xCgNnPc4	dva
brněnská	brněnský	k2eAgNnPc4d1	brněnské
nádraží	nádraží	k1gNnPc4	nádraží
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1872	[number]	k4	1872
již	již	k9	již
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1872	[number]	k4	1872
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
zimní	zimní	k2eAgFnSc2d1	zimní
sezóny	sezóna	k1gFnSc2	sezóna
zastaven	zastavit	k5eAaPmNgInS	zastavit
provoz	provoz	k1gInSc1	provoz
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
Staré	Staré	k2eAgNnSc1d1	Staré
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
zrušena	zrušen	k2eAgFnSc1d1	zrušena
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
od	od	k7c2	od
celnice	celnice	k1gFnSc2	celnice
do	do	k7c2	do
Zábrdovic	Zábrdovice	k1gFnPc2	Zábrdovice
<g/>
.	.	kIx.	.
</s>
<s>
Dopravce	dopravce	k1gMnSc1	dopravce
tehdy	tehdy	k6eAd1	tehdy
požádal	požádat	k5eAaPmAgMnS	požádat
město	město	k1gNnSc4	město
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
dále	daleko	k6eAd2	daleko
provozovat	provozovat	k5eAaImF	provozovat
pouze	pouze	k6eAd1	pouze
trať	trať	k1gFnSc4	trať
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
by	by	kYmCp3nS	by
na	na	k7c6	na
tuto	tento	k3xDgFnSc4	tento
relaci	relace	k1gFnSc4	relace
vypravila	vypravit	k5eAaPmAgFnS	vypravit
třikrát	třikrát	k6eAd1	třikrát
za	za	k7c4	za
den	den	k1gInSc4	den
omnibus	omnibus	k1gInSc1	omnibus
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
návrhem	návrh	k1gInSc7	návrh
ale	ale	k8xC	ale
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
a	a	k8xC	a
požadovala	požadovat	k5eAaImAgFnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
i	i	k9	i
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
jezdilo	jezdit	k5eAaImAgNnS	jezdit
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
alespoň	alespoň	k9	alespoň
pět	pět	k4xCc4	pět
tramvajových	tramvajový	k2eAgInPc2d1	tramvajový
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
společnost	společnost	k1gFnSc1	společnost
ale	ale	k9	ale
tento	tento	k3xDgInSc4	tento
požadavek	požadavek	k1gInSc4	požadavek
nesplnila	splnit	k5eNaPmAgFnS	splnit
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1873	[number]	k4	1873
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
stavební	stavební	k2eAgFnSc3d1	stavební
úpravě	úprava	k1gFnSc3	úprava
tratí	trať	k1gFnPc2	trať
první	první	k4xOgFnSc2	první
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Kiosk	kiosk	k1gInSc1	kiosk
–	–	k?	–
Celnice	celnice	k1gFnSc1	celnice
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
z	z	k7c2	z
Koliště	koliště	k1gNnSc2	koliště
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
trasy	trasa	k1gFnSc2	trasa
po	po	k7c6	po
Rooseveltově	Rooseveltův	k2eAgFnSc6d1	Rooseveltova
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Švédské	švédský	k2eAgNnSc1d1	švédské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
přimkla	přimknout	k5eAaPmAgFnS	přimknout
k	k	k7c3	k
centru	centrum	k1gNnSc3	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
na	na	k7c6	na
Kolišti	koliště	k1gNnSc6	koliště
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
koňskou	koňský	k2eAgFnSc4d1	koňská
tramvaj	tramvaj	k1gFnSc4	tramvaj
nepříznivé	příznivý	k2eNgFnSc2d1	nepříznivá
sklonové	sklonový	k2eAgFnSc2d1	sklonový
poměry	poměra	k1gFnSc2	poměra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1873	[number]	k4	1873
a	a	k8xC	a
1874	[number]	k4	1874
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
trať	trať	k1gFnSc1	trať
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Zmenšující	zmenšující	k2eAgFnSc1d1	zmenšující
se	se	k3xPyFc4	se
poptávka	poptávka	k1gFnSc1	poptávka
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc1	nedostatek
financí	finance	k1gFnPc2	finance
na	na	k7c4	na
rychle	rychle	k6eAd1	rychle
chátrající	chátrající	k2eAgInSc4d1	chátrající
traťový	traťový	k2eAgInSc4d1	traťový
svršek	svršek	k1gInSc4	svršek
a	a	k8xC	a
nákladné	nákladný	k2eAgNnSc1d1	nákladné
vydržování	vydržování	k1gNnSc1	vydržování
koní	kůň	k1gMnPc2	kůň
i	i	k8xC	i
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
krachu	krach	k1gInSc3	krach
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koňky	koňka	k1gFnSc2	koňka
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
1875	[number]	k4	1875
tak	tak	k9	tak
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
===	===	k?	===
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Brünner	Brünnra	k1gFnPc2	Brünnra
Tramway	Tramwaa	k1gFnSc2	Tramwaa
Gesellschaft	Gesellschaft	k1gMnSc1	Gesellschaft
für	für	k?	für
Personen-	Personen-	k1gFnSc2	Personen-
und	und	k?	und
Frachten	Frachten	k2eAgInSc4d1	Frachten
Verkehr	Verkehr	k1gInSc4	Verkehr
disponovala	disponovat	k5eAaBmAgFnS	disponovat
celkem	celkem	k6eAd1	celkem
53	[number]	k4	53
osobními	osobní	k2eAgNnPc7d1	osobní
a	a	k8xC	a
10	[number]	k4	10
nákladními	nákladní	k2eAgInPc7d1	nákladní
vozy	vůz	k1gInPc7	vůz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1869	[number]	k4	1869
a	a	k8xC	a
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInPc1d1	osobní
vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
dvou	dva	k4xCgFnPc2	dva
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
zimní	zimní	k2eAgInPc4d1	zimní
vozy	vůz	k1gInPc4	vůz
byly	být	k5eAaImAgFnP	být
klasické	klasický	k2eAgFnPc1d1	klasická
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
<g/>
,	,	kIx,	,
letní	letní	k2eAgFnPc1d1	letní
(	(	kIx(	(
<g/>
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
)	)	kIx)	)
neměly	mít	k5eNaImAgFnP	mít
okna	okno	k1gNnSc2	okno
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
kryté	krytý	k2eAgFnPc1d1	krytá
pouze	pouze	k6eAd1	pouze
střechou	střecha	k1gFnSc7	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Výrobcem	výrobce	k1gMnSc7	výrobce
některých	některý	k3yIgInPc2	některý
zimních	zimní	k2eAgInPc2d1	zimní
vozů	vůz	k1gInPc2	vůz
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
zachované	zachovaný	k2eAgFnSc2d1	zachovaná
dokumentace	dokumentace	k1gFnSc2	dokumentace
firma	firma	k1gFnSc1	firma
Hernalser	Hernalser	k1gMnSc1	Hernalser
Waggon-Fabriks	Waggon-Fabriksa	k1gFnPc2	Waggon-Fabriksa
Actien-Gesellschaft	Actien-Gesellschaft	k1gMnSc1	Actien-Gesellschaft
Wien	Wien	k1gMnSc1	Wien
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
letní	letní	k2eAgInPc4d1	letní
vozy	vůz	k1gInPc4	vůz
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
společnost	společnost	k1gFnSc1	společnost
Wagon	Wagon	k1gInSc1	Wagon
und	und	k?	und
Strassenbahnen	Strassenbahnen	k1gInSc1	Strassenbahnen
–	–	k?	–
Bauunternehmung	Bauunternehmung	k1gInSc1	Bauunternehmung
Dreyhansen	Dreyhansen	k1gInSc1	Dreyhansen
<g/>
,	,	kIx,	,
Seidler	Seidler	k1gMnSc1	Seidler
<g/>
,	,	kIx,	,
Schwarz	Schwarz	k1gMnSc1	Schwarz
und	und	k?	und
Co	co	k9	co
<g/>
.	.	kIx.	.
</s>
<s>
Nákladní	nákladní	k2eAgInPc1d1	nákladní
vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
evidenčními	evidenční	k2eAgFnPc7d1	evidenční
čísly	čísnout	k5eAaPmAgInP	čísnout
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
(	(	kIx(	(
<g/>
otevřené	otevřený	k2eAgFnPc4d1	otevřená
s	s	k7c7	s
bočnicemi	bočnice	k1gFnPc7	bočnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
(	(	kIx(	(
<g/>
otevřené	otevřený	k2eAgNnSc1d1	otevřené
plošinové	plošinový	k2eAgNnSc1d1	plošinové
<g/>
)	)	kIx)	)
a	a	k8xC	a
25	[number]	k4	25
a	a	k8xC	a
26	[number]	k4	26
(	(	kIx(	(
<g/>
zavřené	zavřený	k2eAgFnSc2d1	zavřená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
provozu	provoz	k1gInSc2	provoz
zůstaly	zůstat	k5eAaPmAgInP	zůstat
vozy	vůz	k1gInPc1	vůz
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
provozovatel	provozovatel	k1gMnSc1	provozovatel
druhé	druhý	k4xOgFnSc2	druhý
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koňky	koňka	k1gFnSc2	koňka
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
pro	pro	k7c4	pro
zchátralý	zchátralý	k2eAgInSc4d1	zchátralý
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
letech	let	k1gInPc6	let
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Provoz	provoz	k1gInSc1	provoz
===	===	k?	===
</s>
</p>
<p>
<s>
Vrcholní	vrcholný	k2eAgMnPc1d1	vrcholný
představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
chtěli	chtít	k5eAaImAgMnP	chtít
i	i	k9	i
přes	přes	k7c4	přes
první	první	k4xOgInSc4	první
neúspěch	neúspěch	k1gInSc4	neúspěch
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jezdila	jezdit	k5eAaImAgFnS	jezdit
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
s	s	k7c7	s
Bernhardem	Bernhard	k1gMnSc7	Bernhard
Kollmannem	Kollmann	k1gMnSc7	Kollmann
<g/>
,	,	kIx,	,
ředitelem	ředitel	k1gMnSc7	ředitel
společnosti	společnost	k1gFnSc2	společnost
provozující	provozující	k2eAgFnSc4d1	provozující
koňku	koňka	k1gFnSc4	koňka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
projevil	projevit	k5eAaPmAgInS	projevit
o	o	k7c6	o
podnikání	podnikání	k1gNnSc6	podnikání
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Brnem	Brno	k1gNnSc7	Brno
a	a	k8xC	a
Kollmannem	Kollmanno	k1gNnSc7	Kollmanno
zajistila	zajistit	k5eAaPmAgFnS	zajistit
podnikateli	podnikatel	k1gMnSc3	podnikatel
šestiletou	šestiletý	k2eAgFnSc4d1	šestiletá
lhůtu	lhůta	k1gFnSc4	lhůta
na	na	k7c4	na
provozování	provozování	k1gNnSc4	provozování
tratě	trať	k1gFnSc2	trať
z	z	k7c2	z
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
do	do	k7c2	do
Pisárek	Pisárka	k1gFnPc2	Pisárka
<g/>
,	,	kIx,	,
během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
měl	mít	k5eAaImAgMnS	mít
Kollmann	Kollmann	k1gInSc4	Kollmann
postavit	postavit	k5eAaPmF	postavit
trať	trať	k1gFnSc4	trať
skrz	skrz	k7c4	skrz
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Malinovského	Malinovský	k2eAgNnSc2d1	Malinovské
náměstí	náměstí	k1gNnSc2	náměstí
ulicí	ulice	k1gFnSc7	ulice
Kobližnou	kobližný	k2eAgFnSc7d1	Kobližná
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Velké	velký	k2eAgNnSc1d1	velké
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
ulicí	ulice	k1gFnSc7	ulice
Českou	český	k2eAgFnSc7d1	Česká
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Rudolfovou	Rudolfův	k2eAgFnSc7d1	Rudolfova
<g/>
)	)	kIx)	)
na	na	k7c4	na
Moravské	moravský	k2eAgNnSc4d1	Moravské
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
ke	k	k7c3	k
Kiosku	kiosek	k1gInSc3	kiosek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
postavena	postaven	k2eAgFnSc1d1	postavena
i	i	k8xC	i
trať	trať	k1gFnSc1	trať
ze	z	k7c2	z
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
k	k	k7c3	k
Ústřednímu	ústřední	k2eAgInSc3d1	ústřední
hřbitovu	hřbitov	k1gInSc3	hřbitov
<g/>
,	,	kIx,	,
výhledově	výhledově	k6eAd1	výhledově
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
stále	stále	k6eAd1	stále
platné	platný	k2eAgFnSc2d1	platná
koncese	koncese	k1gFnSc2	koncese
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
i	i	k8xC	i
tratě	trať	k1gFnPc1	trať
po	po	k7c6	po
Křenové	křenový	k2eAgFnSc6d1	Křenová
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
Cejlu	Cejlo	k1gNnSc6	Cejlo
<g/>
.	.	kIx.	.
</s>
<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
dostávat	dostávat	k5eAaImF	dostávat
od	od	k7c2	od
města	město	k1gNnSc2	město
subvenci	subvence	k1gFnSc4	subvence
3	[number]	k4	3
000	[number]	k4	000
zlatých	zlatá	k1gFnPc2	zlatá
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
povinnost	povinnost	k1gFnSc4	povinnost
se	se	k3xPyFc4	se
starat	starat	k5eAaImF	starat
o	o	k7c4	o
traťový	traťový	k2eAgInSc4d1	traťový
svršek	svršek	k1gInSc4	svršek
a	a	k8xC	a
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
Kollmannem	Kollmanno	k1gNnSc7	Kollmanno
a	a	k8xC	a
městem	město	k1gNnSc7	město
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
koňky	koňka	k1gFnSc2	koňka
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
společnost	společnost	k1gFnSc1	společnost
Brünner	Brünner	k1gMnSc1	Brünner
Tramway-Unternehmung	Tramway-Unternehmung	k1gMnSc1	Tramway-Unternehmung
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
oficiálně	oficiálně	k6eAd1	oficiálně
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
správa	správa	k1gFnSc1	správa
tramwayská	tramwayská	k1gFnSc1	tramwayská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
však	však	k9	však
již	již	k6eAd1	již
brzo	brzo	k6eAd1	brzo
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
provozu	provoz	k1gInSc2	provoz
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
tento	tento	k3xDgInSc1	tento
podnik	podnik	k1gInSc1	podnik
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
zajistil	zajistit	k5eAaPmAgMnS	zajistit
si	se	k3xPyFc3	se
Kollmann	Kollmann	k1gNnSc4	Kollmann
svého	svůj	k3xOyFgMnSc2	svůj
zástupce	zástupce	k1gMnSc2	zástupce
Moritze	Moritz	k1gMnSc2	Moritz
Kohna	Kohn	k1gMnSc2	Kohn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
<g />
.	.	kIx.	.
</s>
<s>
financoval	financovat	k5eAaBmAgMnS	financovat
koně	kůň	k1gMnPc4	kůň
a	a	k8xC	a
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
který	který	k3yQgInSc1	který
také	také	k9	také
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
pro	pro	k7c4	pro
jednání	jednání	k1gNnSc4	jednání
mezi	mezi	k7c7	mezi
podnikem	podnik	k1gInSc7	podnik
a	a	k8xC	a
městem	město	k1gNnSc7	město
Brnem	Brno	k1gNnSc7	Brno
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInPc4	první
vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
dodány	dodat	k5eAaPmNgInP	dodat
začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byly	být	k5eAaImAgInP	být
vozy	vůz	k1gInPc1	vůz
předvedeny	předveden	k2eAgInPc1d1	předveden
úřadům	úřad	k1gInPc3	úřad
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
na	na	k7c4	na
jednokolejné	jednokolejný	k2eAgInPc4d1	jednokolejný
normálněrozchodné	normálněrozchodný	k2eAgInPc4d1	normálněrozchodný
(	(	kIx(	(
<g/>
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
)	)	kIx)	)
trati	trať	k1gFnSc2	trať
s	s	k7c7	s
výhybnami	výhybna	k1gFnPc7	výhybna
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
první	první	k4xOgFnSc4	první
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
koňku	koňka	k1gFnSc4	koňka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
vozovna	vozovna	k1gFnSc1	vozovna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1876	[number]	k4	1876
byl	být	k5eAaImAgInS	být
zprovozněn	zprovozněn	k2eAgInSc1d1	zprovozněn
zbývající	zbývající	k2eAgInSc4d1	zbývající
úsek	úsek	k1gInSc4	úsek
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
–	–	k?	–
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
(	(	kIx(	(
<g/>
s	s	k7c7	s
vozovnou	vozovna	k1gFnSc7	vozovna
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
u	u	k7c2	u
Semilassa	Semilass	k1gMnSc2	Semilass
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
jediné	jediný	k2eAgFnSc6d1	jediná
trati	trať	k1gFnSc6	trať
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc1	dva
linky	linka	k1gFnPc1	linka
<g/>
,	,	kIx,	,
cestující	cestující	k1gMnPc1	cestující
museli	muset	k5eAaImAgMnP	muset
u	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
přestupovat	přestupovat	k5eAaImF	přestupovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
měly	mít	k5eAaImAgFnP	mít
tramvaje	tramvaj	k1gFnPc1	tramvaj
jezdit	jezdit	k5eAaImF	jezdit
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
Kollmann	Kollmanna	k1gFnPc2	Kollmanna
ale	ale	k8xC	ale
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
zavedl	zavést	k5eAaPmAgInS	zavést
interval	interval	k1gInSc4	interval
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Koňka	koňka	k1gFnSc1	koňka
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byla	být	k5eAaImAgFnS	být
jezdila	jezdit	k5eAaImAgFnS	jezdit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
bylo	být	k5eAaImAgNnS	být
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
vypravováno	vypravovat	k5eAaImNgNnS	vypravovat
10	[number]	k4	10
vozů	vůz	k1gInPc2	vůz
(	(	kIx(	(
<g/>
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
přes	přes	k7c4	přes
znění	znění	k1gNnSc4	znění
smlouvy	smlouva	k1gFnSc2	smlouva
ale	ale	k8xC	ale
traťový	traťový	k2eAgInSc1d1	traťový
svršek	svršek	k1gInSc1	svršek
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
udržován	udržovat	k5eAaImNgInS	udržovat
<g/>
,	,	kIx,	,
také	také	k9	také
koně	kůň	k1gMnPc1	kůň
nebyli	být	k5eNaImAgMnP	být
v	v	k7c6	v
příliš	příliš	k6eAd1	příliš
dobrém	dobrý	k2eAgInSc6d1	dobrý
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
běžného	běžný	k2eAgInSc2d1	běžný
provozu	provoz	k1gInSc2	provoz
projevilo	projevit	k5eAaPmAgNnS	projevit
i	i	k9	i
při	při	k7c6	při
zkoušce	zkouška	k1gFnSc6	zkouška
parní	parní	k2eAgFnSc2d1	parní
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
horké	horký	k2eAgFnPc4d1	horká
novinky	novinka	k1gFnPc4	novinka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivka	lokomotivka	k1gFnSc1	lokomotivka
Krauss	Kraussa	k1gFnPc2	Kraussa
(	(	kIx(	(
<g/>
továrny	továrna	k1gFnPc1	továrna
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
Linci	Linec	k1gInSc6	Linec
<g/>
)	)	kIx)	)
zapůjčila	zapůjčit	k5eAaPmAgFnS	zapůjčit
brněnskému	brněnský	k2eAgMnSc3d1	brněnský
tramvajovému	tramvajový	k2eAgMnSc3d1	tramvajový
podniku	podnik	k1gInSc2	podnik
jednu	jeden	k4xCgFnSc4	jeden
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
pro	pro	k7c4	pro
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
jízda	jízda	k1gFnSc1	jízda
je	on	k3xPp3gNnSc4	on
konala	konat	k5eAaImAgFnS	konat
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
ale	ale	k8xC	ale
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
úseku	úsek	k1gInSc6	úsek
vykolejila	vykolejit	k5eAaPmAgFnS	vykolejit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
zkušební	zkušební	k2eAgFnSc1d1	zkušební
jízda	jízda	k1gFnSc1	jízda
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
dobového	dobový	k2eAgInSc2d1	dobový
tisku	tisk	k1gInSc2	tisk
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
skvěle	skvěle	k6eAd1	skvěle
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
ale	ale	k8xC	ale
tentokrát	tentokrát	k6eAd1	tentokrát
nastaly	nastat	k5eAaPmAgFnP	nastat
s	s	k7c7	s
moravským	moravský	k2eAgNnSc7d1	Moravské
místodržitelstvím	místodržitelství	k1gNnSc7	místodržitelství
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zkoušky	zkouška	k1gFnPc1	zkouška
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gNnSc2	jeho
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
místodržitelství	místodržitelství	k1gNnSc2	místodržitelství
pohrozilo	pohrozit	k5eAaPmAgNnS	pohrozit
podniku	podnik	k1gInSc6	podnik
pokutou	pokuta	k1gFnSc7	pokuta
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
zkušební	zkušební	k2eAgFnSc1d1	zkušební
jízda	jízda	k1gFnSc1	jízda
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
výrobce	výrobce	k1gMnSc2	výrobce
i	i	k8xC	i
místodržitelství	místodržitelství	k1gNnSc6	místodržitelství
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
konstatování	konstatování	k1gNnSc1	konstatování
<g/>
,	,	kIx,	,
že	že	k8xS	že
stávající	stávající	k2eAgInSc1d1	stávající
traťový	traťový	k2eAgInSc1d1	traťový
svršek	svršek	k1gInSc1	svršek
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
vozidla	vozidlo	k1gNnPc4	vozidlo
a	a	k8xC	a
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
případným	případný	k2eAgNnSc7d1	případné
zavedením	zavedení	k1gNnSc7	zavedení
by	by	kYmCp3nS	by
jej	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zcela	zcela	k6eAd1	zcela
vyměnit	vyměnit	k5eAaPmF	vyměnit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
říjnu	říjen	k1gInSc3	říjen
1879	[number]	k4	1879
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
podnik	podnik	k1gInSc1	podnik
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdvoukolejní	zdvoukolejnit	k5eAaPmIp3nS	zdvoukolejnit
celou	celý	k2eAgFnSc4d1	celá
trať	trať	k1gFnSc4	trať
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
a	a	k8xC	a
zavede	zavést	k5eAaPmIp3nS	zavést
parní	parní	k2eAgFnSc1d1	parní
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
ze	z	k7c2	z
Starého	Starého	k2eAgNnSc2d1	Starého
Brna	Brno	k1gNnSc2	Brno
do	do	k7c2	do
Pisárek	Pisárka	k1gFnPc2	Pisárka
<g/>
,	,	kIx,	,
podmínkou	podmínka	k1gFnSc7	podmínka
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
povolení	povolení	k1gNnSc4	povolení
pro	pro	k7c4	pro
vybudování	vybudování	k1gNnSc4	vybudování
jednokolejné	jednokolejný	k2eAgFnSc2d1	jednokolejná
trati	trať	k1gFnSc2	trať
pro	pro	k7c4	pro
koňku	koňka	k1gFnSc4	koňka
přes	přes	k7c4	přes
centrum	centrum	k1gNnSc4	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
ale	ale	k9	ale
s	s	k7c7	s
provozovatelem	provozovatel	k1gMnSc7	provozovatel
nedohodla	dohodnout	k5eNaPmAgFnS	dohodnout
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
klesající	klesající	k2eAgFnSc3d1	klesající
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
přepravě	přeprava	k1gFnSc6	přeprava
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
souvisejícímu	související	k2eAgInSc3d1	související
poklesu	pokles	k1gInSc3	pokles
příjmů	příjem	k1gInPc2	příjem
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tramvaje	tramvaj	k1gFnSc2	tramvaj
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
z	z	k7c2	z
Králova	Králův	k2eAgNnSc2d1	Královo
Pole	pole	k1gNnSc2	pole
do	do	k7c2	do
Pisárek	Pisárka	k1gFnPc2	Pisárka
zastaven	zastavit	k5eAaPmNgInS	zastavit
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Bernhardem	Bernhard	k1gMnSc7	Bernhard
Kollmannem	Kollmann	k1gMnSc7	Kollmann
již	již	k6eAd1	již
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
nebyla	být	k5eNaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Brno	Brno	k1gNnSc1	Brno
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bez	bez	k7c2	bez
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
provoz	provoz	k1gInSc1	provoz
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spolehlivě	spolehlivě	k6eAd1	spolehlivě
fungovala	fungovat	k5eAaImAgFnS	fungovat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
tramvaj	tramvaj	k1gFnSc1	tramvaj
elektrická	elektrický	k2eAgFnSc1d1	elektrická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
===	===	k?	===
</s>
</p>
<p>
<s>
Vozový	vozový	k2eAgInSc1d1	vozový
park	park	k1gInSc1	park
společnosti	společnost	k1gFnSc2	společnost
Brünner	Brünnra	k1gFnPc2	Brünnra
Tramway-Unternehmung	Tramway-Unternehmunga	k1gFnPc2	Tramway-Unternehmunga
trvale	trvale	k6eAd1	trvale
tvořilo	tvořit	k5eAaImAgNnS	tvořit
13	[number]	k4	13
dvojspřežních	dvojspřežní	k2eAgInPc2d1	dvojspřežní
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
(	(	kIx(	(
<g/>
zimního	zimní	k2eAgInSc2d1	zimní
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
otevřeného	otevřený	k2eAgInSc2d1	otevřený
(	(	kIx(	(
<g/>
letního	letní	k2eAgInSc2d1	letní
<g/>
)	)	kIx)	)
typu	typ	k1gInSc2	typ
bez	bez	k7c2	bez
oken	okno	k1gNnPc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
devět	devět	k4xCc4	devět
vozů	vůz	k1gInPc2	vůz
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
parní	parní	k2eAgFnSc7d1	parní
dráhou	dráha	k1gFnSc7	dráha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gNnSc4	on
využila	využít	k5eAaPmAgFnS	využít
jako	jako	k9	jako
vlečné	vlečný	k2eAgInPc4d1	vlečný
vozy	vůz	k1gInPc4	vůz
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
č.	č.	k?	č.
1	[number]	k4	1
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
vyrobené	vyrobený	k2eAgNnSc4d1	vyrobené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
firmou	firma	k1gFnSc7	firma
Hernalser	Hernalsra	k1gFnPc2	Hernalsra
Waggon-Fabriks	Waggon-Fabriksa	k1gFnPc2	Waggon-Fabriksa
Actien-Gesellschaft	Actien-Gesellschaft	k2eAgInSc4d1	Actien-Gesellschaft
Wien	Wien	k1gInSc4	Wien
<g/>
,	,	kIx,	,
jezdily	jezdit	k5eAaImAgInP	jezdit
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
elektrické	elektrický	k2eAgFnSc2d1	elektrická
tramvaje	tramvaj	k1gFnSc2	tramvaj
(	(	kIx(	(
<g/>
č.	č.	k?	č.
92	[number]	k4	92
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
č.	č.	k?	č.
192	[number]	k4	192
<g/>
–	–	k?	–
<g/>
198	[number]	k4	198
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definitivně	definitivně	k6eAd1	definitivně
vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
vůz	vůz	k1gInSc1	vůz
č.	č.	k?	č.
2	[number]	k4	2
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
na	na	k7c4	na
montážní	montážní	k2eAgInSc4d1	montážní
vůz	vůz	k1gInSc4	vůz
trolejového	trolejový	k2eAgNnSc2d1	trolejové
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
<g/>
Čtyři	čtyři	k4xCgFnPc1	čtyři
otevřené	otevřený	k2eAgFnPc1d1	otevřená
vozy	vůz	k1gInPc4	vůz
druhé	druhý	k4xOgFnSc2	druhý
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koňky	koňka	k1gFnSc2	koňka
jezdily	jezdit	k5eAaImAgInP	jezdit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
s	s	k7c7	s
evidenčními	evidenční	k2eAgNnPc7d1	evidenční
čísly	číslo	k1gNnPc7	číslo
7	[number]	k4	7
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
113	[number]	k4	113
a	a	k8xC	a
114	[number]	k4	114
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vlečné	vlečný	k2eAgFnPc1d1	vlečná
byly	být	k5eAaImAgFnP	být
využity	využít	k5eAaPmNgFnP	využít
i	i	k9	i
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
elektrické	elektrický	k2eAgFnSc2d1	elektrická
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nesly	nést	k5eAaImAgInP	nést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
čísla	číslo	k1gNnSc2	číslo
233	[number]	k4	233
<g/>
,	,	kIx,	,
236	[number]	k4	236
<g/>
,	,	kIx,	,
234	[number]	k4	234
a	a	k8xC	a
235	[number]	k4	235
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
č.	č.	k?	č.
533	[number]	k4	533
<g/>
,	,	kIx,	,
536	[number]	k4	536
<g/>
,	,	kIx,	,
534	[number]	k4	534
a	a	k8xC	a
535	[number]	k4	535
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1927	[number]	k4	1927
a	a	k8xC	a
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
kompletaci	kompletace	k1gFnSc6	kompletace
sbírky	sbírka	k1gFnSc2	sbírka
brněnských	brněnský	k2eAgFnPc2d1	brněnská
tramvají	tramvaj	k1gFnPc2	tramvaj
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
objeven	objevit	k5eAaPmNgInS	objevit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
otevřených	otevřený	k2eAgInPc2d1	otevřený
vozů	vůz	k1gInPc2	vůz
druhé	druhý	k4xOgFnSc2	druhý
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koňky	koňka	k1gFnSc2	koňka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
po	po	k7c4	po
vyřazení	vyřazení	k1gNnSc4	vyřazení
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k8xS	jako
zahradní	zahradní	k2eAgFnSc1d1	zahradní
chatka	chatka	k1gFnSc1	chatka
v	v	k7c6	v
Jundrově	Jundrov	k1gInSc6	Jundrov
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
ev.	ev.	k?	ev.
č.	č.	k?	č.
6	[number]	k4	6
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
sbírky	sbírka	k1gFnSc2	sbírka
Technického	technický	k2eAgNnSc2d1	technické
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
renovován	renovovat	k5eAaBmNgMnS	renovovat
v	v	k7c6	v
Ústředních	ústřední	k2eAgFnPc6d1	ústřední
dílnách	dílna	k1gFnPc6	dílna
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
částí	část	k1gFnPc2	část
pojezdu	pojezd	k1gInSc2	pojezd
vozu	vůz	k1gInSc2	vůz
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
využívaného	využívaný	k2eAgInSc2d1	využívaný
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xS	jako
montážní	montážní	k2eAgInSc1d1	montážní
vůz	vůz	k1gInSc1	vůz
trolejového	trolejový	k2eAgNnSc2d1	trolejové
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
vůz	vůz	k1gInSc4	vůz
druhé	druhý	k4xOgFnSc2	druhý
brněnské	brněnský	k2eAgFnSc2d1	brněnská
koňky	koňka	k1gFnSc2	koňka
č.	č.	k?	č.
6	[number]	k4	6
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
výroby	výroba	k1gFnSc2	výroba
1876	[number]	k4	1876
nejstarším	starý	k2eAgNnSc7d3	nejstarší
zachovaným	zachovaný	k2eAgNnSc7d1	zachované
vozidlem	vozidlo	k1gNnSc7	vozidlo
MHD	MHD	kA	MHD
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
při	při	k7c6	při
mimořádných	mimořádný	k2eAgFnPc6d1	mimořádná
jízdách	jízda	k1gFnPc6	jízda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
každoroční	každoroční	k2eAgFnSc1d1	každoroční
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
nostalgie	nostalgie	k1gFnSc1	nostalgie
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
koňka	koňka	k1gFnSc1	koňka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
soupravě	souprava	k1gFnSc6	souprava
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
koněspřežnou	koněspřežný	k2eAgFnSc4d1	koněspřežná
dráhu	dráha	k1gFnSc4	dráha
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
hledala	hledat	k5eAaImAgFnS	hledat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
zájemce	zájemce	k1gMnSc2	zájemce
o	o	k7c4	o
provozování	provozování	k1gNnSc4	provozování
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
jedné	jeden	k4xCgFnSc2	jeden
bruselské	bruselský	k2eAgFnSc2d1	bruselská
společnosti	společnost	k1gFnSc2	společnost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
uváděl	uvádět	k5eAaImAgInS	uvádět
přeložení	přeložení	k1gNnSc4	přeložení
tratě	trať	k1gFnSc2	trať
do	do	k7c2	do
trasy	trasa	k1gFnSc2	trasa
přes	přes	k7c4	přes
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
dnešní	dnešní	k2eAgNnSc4d1	dnešní
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nadále	nadále	k6eAd1	nadále
zajišťován	zajišťovat	k5eAaImNgInS	zajišťovat
provoz	provoz	k1gInSc1	provoz
animální	animální	k2eAgInSc1d1	animální
trakcí	trakce	k1gFnSc7	trakce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
koncových	koncový	k2eAgInPc6d1	koncový
úsecích	úsek	k1gInPc6	úsek
Staré	Stará	k1gFnSc2	Stará
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
a	a	k8xC	a
Lužánky	Lužánka	k1gFnSc2	Lužánka
–	–	k?	–
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
již	již	k6eAd1	již
měly	mít	k5eAaImAgFnP	mít
jezdit	jezdit	k5eAaImF	jezdit
soupravy	souprava	k1gFnPc1	souprava
parní	parní	k2eAgFnPc1d1	parní
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
jednala	jednat	k5eAaImAgFnS	jednat
i	i	k9	i
s	s	k7c7	s
Wilhelmem	Wilhelm	k1gInSc7	Wilhelm
von	von	k1gInSc4	von
Lindheimem	Lindheim	k1gInSc7	Lindheim
<g/>
,	,	kIx,	,
propagátorem	propagátor	k1gMnSc7	propagátor
a	a	k8xC	a
stavitelem	stavitel	k1gMnSc7	stavitel
městských	městský	k2eAgFnPc2d1	městská
parních	parní	k2eAgFnPc2d1	parní
pouličních	pouliční	k2eAgFnPc2d1	pouliční
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
doporučil	doporučit	k5eAaPmAgMnS	doporučit
městu	město	k1gNnSc3	město
parní	parní	k2eAgFnSc4d1	parní
trakci	trakce	k1gFnSc4	trakce
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
však	však	k9	však
v	v	k7c6	v
září	září	k1gNnSc6	září
1882	[number]	k4	1882
získalo	získat	k5eAaPmAgNnS	získat
pouze	pouze	k6eAd1	pouze
obnovenou	obnovený	k2eAgFnSc4d1	obnovená
koncesi	koncese	k1gFnSc4	koncese
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
a	a	k8xC	a
provozu	provoz	k1gInSc3	provoz
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
mezi	mezi	k7c7	mezi
městem	město	k1gNnSc7	město
a	a	k8xC	a
von	von	k1gInSc4	von
Lindheimem	Lindheim	k1gInSc7	Lindheim
z	z	k7c2	z
února	únor	k1gInSc2	únor
1884	[number]	k4	1884
měl	mít	k5eAaImAgMnS	mít
podnikatel	podnikatel	k1gMnSc1	podnikatel
postavit	postavit	k5eAaPmF	postavit
trať	trať	k1gFnSc4	trať
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
–	–	k?	–
Pisárky	Pisárka	k1gFnSc2	Pisárka
a	a	k8xC	a
trať	trať	k1gFnSc4	trať
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
městem	město	k1gNnSc7	město
Kiosk	kiosk	k1gInSc1	kiosk
(	(	kIx(	(
<g/>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
–	–	k?	–
Rudolfská	Rudolfský	k2eAgFnSc1d1	Rudolfská
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
byl	být	k5eAaImAgInS	být
povolen	povolen	k2eAgInSc1d1	povolen
koněspřežný	koněspřežný	k2eAgInSc1d1	koněspřežný
<g/>
,	,	kIx,	,
parní	parní	k2eAgInSc1d1	parní
i	i	k8xC	i
elektrický	elektrický	k2eAgInSc1d1	elektrický
provoz	provoz	k1gInSc1	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
však	však	k9	však
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
trati	trať	k1gFnSc6	trať
a	a	k8xC	a
výhradně	výhradně	k6eAd1	výhradně
parní	parní	k2eAgFnSc7d1	parní
trakcí	trakce	k1gFnSc7	trakce
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1884	[number]	k4	1884
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
zatlačit	zatlačit	k5eAaPmF	zatlačit
na	na	k7c4	na
von	von	k1gInSc4	von
Lindheima	Lindheimum	k1gNnSc2	Lindheimum
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtělo	chtít	k5eAaImAgNnS	chtít
schválení	schválení	k1gNnSc4	schválení
tratě	trať	k1gFnSc2	trať
k	k	k7c3	k
Ústřednímu	ústřední	k2eAgInSc3d1	ústřední
hřbitovu	hřbitov	k1gInSc3	hřbitov
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
parní	parní	k2eAgInSc4d1	parní
provoz	provoz	k1gInSc4	provoz
<g/>
)	)	kIx)	)
podmínit	podmínit	k5eAaPmF	podmínit
urychlenou	urychlený	k2eAgFnSc7d1	urychlená
stavbou	stavba	k1gFnSc7	stavba
tratě	trata	k1gFnSc6	trata
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
koňku	koňka	k1gFnSc4	koňka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
měl	mít	k5eAaImAgMnS	mít
postavit	postavit	k5eAaPmF	postavit
i	i	k9	i
úsek	úsek	k1gInSc4	úsek
do	do	k7c2	do
Zábrdovic	Zábrdovice	k1gFnPc2	Zábrdovice
<g/>
.	.	kIx.	.
</s>
<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
sice	sice	k8xC	sice
zahájí	zahájit	k5eAaPmIp3nS	zahájit
stavbu	stavba	k1gFnSc4	stavba
tratě	trať	k1gFnSc2	trať
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
že	že	k8xS	že
zastaví	zastavit	k5eAaPmIp3nS	zastavit
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
veškerý	veškerý	k3xTgInSc4	veškerý
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
zastupitelstvo	zastupitelstvo	k1gNnSc4	zastupitelstvo
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
podmínky	podmínka	k1gFnSc2	podmínka
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tratě	trať	k1gFnSc2	trať
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
městem	město	k1gNnSc7	město
tak	tak	k9	tak
již	již	k6eAd1	již
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
<g/>
Von	von	k1gInSc1	von
Lindheimova	Lindheimův	k2eAgFnSc1d1	Lindheimův
společnost	společnost	k1gFnSc1	společnost
Brünner	Brünnra	k1gFnPc2	Brünnra
Dampf-Tramway	Dampf-Tramwaa	k1gMnSc2	Dampf-Tramwaa
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
trať	trať	k1gFnSc4	trať
ale	ale	k8xC	ale
zakoupila	zakoupit	k5eAaPmAgFnS	zakoupit
celkem	celkem	k6eAd1	celkem
sedm	sedm	k4xCc4	sedm
nových	nový	k2eAgInPc2d1	nový
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
s	s	k7c7	s
čísly	číslo	k1gNnPc7	číslo
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
mimo	mimo	k7c4	mimo
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
salonní	salonní	k2eAgInSc1d1	salonní
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
číslo	číslo	k1gNnSc1	číslo
není	být	k5eNaImIp3nS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
doloženo	doložit	k5eAaPmNgNnS	doložit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
parní	parní	k2eAgFnSc2d1	parní
trakce	trakce	k1gFnSc2	trakce
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
koňky	koňka	k1gFnSc2	koňka
č.	č.	k?	č.
9	[number]	k4	9
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
,	,	kIx,	,
13	[number]	k4	13
a	a	k8xC	a
14	[number]	k4	14
a	a	k8xC	a
salonní	salonní	k2eAgInSc1d1	salonní
vůz	vůz	k1gInSc1	vůz
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
odprodány	odprodat	k5eAaPmNgInP	odprodat
do	do	k7c2	do
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zahájily	zahájit	k5eAaPmAgInP	zahájit
provoz	provoz	k1gInSc4	provoz
koněspřežné	koněspřežný	k2eAgFnSc2d1	koněspřežná
tramvaje	tramvaj	k1gFnSc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
<g/>
.	.	kIx.	.
č.	č.	k?	č.
11	[number]	k4	11
a	a	k8xC	a
12	[number]	k4	12
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
navíc	navíc	k6eAd1	navíc
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ani	ani	k9	ani
schváleny	schválit	k5eAaPmNgInP	schválit
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
rovněž	rovněž	k9	rovněž
prodány	prodat	k5eAaPmNgFnP	prodat
do	do	k7c2	do
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Parní	parní	k2eAgFnSc1d1	parní
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
NESIBA	NESIBA	kA	NESIBA
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
100	[number]	k4	100
let	léto	k1gNnPc2	léto
elektrické	elektrický	k2eAgFnSc2d1	elektrická
pouliční	pouliční	k2eAgFnSc2d1	pouliční
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
:	:	kIx,	:
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Wolf	Wolf	k1gMnSc1	Wolf
–	–	k?	–
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
WOLF	Wolf	k1gMnSc1	Wolf
&	&	k?	&
Tramvajklub	Tramvajklub	k1gMnSc1	Tramvajklub
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MORÁVEK	Morávek	k1gMnSc1	Morávek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
120	[number]	k4	120
let	léto	k1gNnPc2	léto
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLAPKA	Klapka	k1gMnSc1	Klapka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
80	[number]	k4	80
let	léto	k1gNnPc2	léto
elektrické	elektrický	k2eAgFnSc2d1	elektrická
dráhy	dráha	k1gFnSc2	dráha
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
–	–	k?	–
50	[number]	k4	50
let	léto	k1gNnPc2	léto
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
Technické	technický	k2eAgNnSc4d1	technické
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Koněspřežná	koněspřežný	k2eAgFnSc1d1	koněspřežná
tramvaj	tramvaj	k1gFnSc1	tramvaj
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
