<s>
Pearl	Pearl	k1gInSc1	Pearl
Jam	jáma	k1gFnPc2	jáma
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
rockových	rockový	k2eAgFnPc2d1	rocková
kapel	kapela	k1gFnPc2	kapela
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
hnutím	hnutí	k1gNnSc7	hnutí
grunge	grung	k1gFnSc2	grung
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
a	a	k8xC	a
Alice	Alice	k1gFnSc2	Alice
in	in	k?	in
Chains	Chains	k1gInSc1	Chains
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
hlavní	hlavní	k2eAgMnPc4d1	hlavní
představitele	představitel	k1gMnPc4	představitel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
zvuk	zvuk	k1gInSc1	zvuk
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
vždy	vždy	k6eAd1	vždy
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
experimentovat	experimentovat	k5eAaImF	experimentovat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
desky	deska	k1gFnPc1	deska
již	již	k6eAd1	již
dlouho	dlouho	k6eAd1	dlouho
neprodávají	prodávat	k5eNaImIp3nP	prodávat
tak	tak	k6eAd1	tak
masově	masově	k6eAd1	masově
jako	jako	k8xC	jako
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	můj	k3xOp1gFnSc1	můj
kapela	kapela	k1gFnSc1	kapela
početnou	početný	k2eAgFnSc4d1	početná
základnu	základna	k1gFnSc4	základna
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
koncerty	koncert	k1gInPc1	koncert
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgFnPc4	ten
nejnavštěvovanější	navštěvovaný	k2eAgFnPc4d3	nejnavštěvovanější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kapelu	kapela	k1gFnSc4	kapela
založili	založit	k5eAaPmAgMnP	založit
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Mother	Mothra	k1gFnPc2	Mothra
Love	lov	k1gInSc5	lov
Bone	bon	k1gInSc5	bon
(	(	kIx(	(
<g/>
Jeff	Jeff	k1gMnSc1	Jeff
Ament	Ament	k1gMnSc1	Ament
<g/>
,	,	kIx,	,
Stone	ston	k1gInSc5	ston
Gossard	Gossarda	k1gFnPc2	Gossarda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
zpěvák	zpěvák	k1gMnSc1	zpěvák
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
předávkoval	předávkovat	k5eAaPmAgMnS	předávkovat
heroinem	heroin	k1gInSc7	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Mikem	Mik	k1gMnSc7	Mik
McCreadym	McCreadymum	k1gNnPc2	McCreadymum
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
bubeníkem	bubeník	k1gMnSc7	bubeník
Red	Red	k1gMnSc7	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnPc7	Chil
Peppers	Peppersa	k1gFnPc2	Peppersa
<g/>
,	,	kIx,	,
Jackem	Jacek	k1gMnSc7	Jacek
Ironsem	Irons	k1gMnSc7	Irons
nahráli	nahrát	k5eAaPmAgMnP	nahrát
pár	pár	k4xCyI	pár
demo	demo	k2eAgFnPc2d1	demo
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
jim	on	k3xPp3gMnPc3	on
ještě	ještě	k6eAd1	ještě
chyběl	chybět	k5eAaImAgMnS	chybět
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
surfař	surfař	k1gMnSc1	surfař
ze	z	k7c2	z
San	San	k1gFnSc2	San
Diega	Dieg	k1gMnSc2	Dieg
Eddie	Eddie	k1gFnSc2	Eddie
Vedder	Veddra	k1gFnPc2	Veddra
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
pak	pak	k6eAd1	pak
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
základy	základ	k1gInPc1	základ
prvních	první	k4xOgFnPc2	první
písní	píseň	k1gFnPc2	píseň
Pearl	Pearla	k1gFnPc2	Pearla
Jam	jam	k1gInSc1	jam
(	(	kIx(	(
<g/>
Alive	Aliev	k1gFnPc1	Aliev
<g/>
,	,	kIx,	,
Once	Onc	k1gInPc1	Onc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Debutová	debutový	k2eAgFnSc1d1	debutová
deska	deska	k1gFnSc1	deska
Ten	ten	k3xDgInSc4	ten
vyšla	vyjít	k5eAaPmAgFnS	vyjít
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
alba	alba	k1gFnSc1	alba
Vs	Vs	k1gFnSc1	Vs
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Vitalogy	Vitalog	k1gInPc1	Vitalog
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
zpočátku	zpočátku	k6eAd1	zpočátku
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
–	–	k?	–
zejména	zejména	k9	zejména
frontmanem	frontman	k1gInSc7	frontman
skupiny	skupina	k1gFnSc2	skupina
Nirvana	Nirvan	k1gMnSc2	Nirvan
<g/>
,	,	kIx,	,
Kurtem	Kurt	k1gMnSc7	Kurt
Cobainem	Cobain	k1gMnSc7	Cobain
–	–	k?	–
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
z	z	k7c2	z
obrovského	obrovský	k2eAgInSc2d1	obrovský
rozmachu	rozmach	k1gInSc2	rozmach
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
dělá	dělat	k5eAaImIp3nS	dělat
výdělečný	výdělečný	k2eAgInSc1d1	výdělečný
podnik	podnik	k1gInSc1	podnik
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
Cobain	Cobain	k1gMnSc1	Cobain
a	a	k8xC	a
Vedder	Vedder	k1gMnSc1	Vedder
byli	být	k5eAaImAgMnP	být
blízcí	blízký	k2eAgMnPc1d1	blízký
přátelé	přítel	k1gMnPc1	přítel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
Pearl	Pearl	k1gInSc1	Pearl
Jam	jáma	k1gFnPc2	jáma
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
účastnit	účastnit	k5eAaImF	účastnit
akcí	akce	k1gFnSc7	akce
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgMnPc2	který
byli	být	k5eAaImAgMnP	být
tlačeni	tlačit	k5eAaImNgMnP	tlačit
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
firmou	firma	k1gFnSc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
Nenatáčeli	natáčet	k5eNaImAgMnP	natáčet
například	například	k6eAd1	například
moc	moc	k6eAd1	moc
videoklipů	videoklip	k1gInPc2	videoklip
a	a	k8xC	a
bojkotovali	bojkotovat	k5eAaImAgMnP	bojkotovat
firmu	firma	k1gFnSc4	firma
Ticketmaster	Ticketmastra	k1gFnPc2	Ticketmastra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
využíval	využívat	k5eAaPmAgMnS	využívat
svého	svůj	k3xOyFgNnSc2	svůj
monopolního	monopolní	k2eAgNnSc2d1	monopolní
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
diktovala	diktovat	k5eAaImAgFnS	diktovat
kapele	kapela	k1gFnSc3	kapela
cenu	cena	k1gFnSc4	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
dekádě	dekáda	k1gFnSc6	dekáda
strávili	strávit	k5eAaPmAgMnP	strávit
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
kazili	kazit	k5eAaImAgMnP	kazit
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Doposud	doposud	k6eAd1	doposud
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
prodalo	prodat	k5eAaPmAgNnS	prodat
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
jejich	jejich	k3xOp3gNnPc2	jejich
alb	album	k1gNnPc2	album
a	a	k8xC	a
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
<s>
Stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
rockových	rockový	k2eAgFnPc2d1	rocková
skupin	skupina	k1gFnPc2	skupina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Ten	ten	k3xDgInSc4	ten
začíná	začínat	k5eAaImIp3nS	začínat
skupina	skupina	k1gFnSc1	skupina
nahrávat	nahrávat	k5eAaImF	nahrávat
už	už	k6eAd1	už
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1991	[number]	k4	1991
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
prodávalo	prodávat	k5eAaImAgNnS	prodávat
poměrně	poměrně	k6eAd1	poměrně
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
15	[number]	k4	15
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
skladeb	skladba	k1gFnPc2	skladba
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
koncertní	koncertní	k2eAgFnSc1d1	koncertní
verze	verze	k1gFnSc1	verze
skladby	skladba	k1gFnSc2	skladba
Alive	Aliev	k1gFnSc2	Aliev
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doslova	doslova	k6eAd1	doslova
nabité	nabitý	k2eAgInPc4d1	nabitý
hity	hit	k1gInPc4	hit
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
Alive	Aliev	k1gFnSc2	Aliev
<g/>
,	,	kIx,	,
Even	Even	k1gInSc4	Even
Flow	Flow	k1gFnSc2	Flow
<g/>
,	,	kIx,	,
Jeremy	Jerema	k1gFnSc2	Jerema
a	a	k8xC	a
Oceans	Oceans	k1gInSc4	Oceans
vyšly	vyjít	k5eAaPmAgInP	vyjít
i	i	k9	i
jako	jako	k9	jako
singly	singl	k1gInPc1	singl
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
kandidátem	kandidát	k1gMnSc7	kandidát
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc4	píseň
Black	Blacka	k1gFnPc2	Blacka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
neudělat	udělat	k5eNaPmF	udělat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
osobní	osobní	k2eAgInSc1d1	osobní
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
zničilo	zničit	k5eAaPmAgNnS	zničit
videem	video	k1gNnSc7	video
nebo	nebo	k8xC	nebo
vydáním	vydání	k1gNnSc7	vydání
singlu	singl	k1gInSc2	singl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
pocitů	pocit	k1gInPc2	pocit
úzkosti	úzkost	k1gFnSc2	úzkost
<g/>
,	,	kIx,	,
samoty	samota	k1gFnSc2	samota
a	a	k8xC	a
deprese	deprese	k1gFnSc2	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
Alive	Aliev	k1gFnSc2	Aliev
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
skutečné	skutečný	k2eAgFnPc4d1	skutečná
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Eddie	Eddie	k1gFnSc1	Eddie
Vedder	Veddra	k1gFnPc2	Veddra
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
jako	jako	k9	jako
teenager	teenager	k1gMnSc1	teenager
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
skutečný	skutečný	k2eAgMnSc1d1	skutečný
otec	otec	k1gMnSc1	otec
není	být	k5eNaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
koho	kdo	k3yInSc4	kdo
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokládal	pokládat	k5eAaImAgMnS	pokládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Vs	Vs	k1gFnSc2	Vs
<g/>
.	.	kIx.	.
a	a	k8xC	a
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
950	[number]	k4	950
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Go	Go	k1gMnPc1	Go
<g/>
,	,	kIx,	,
Dissident	Dissident	k1gMnSc1	Dissident
<g/>
,	,	kIx,	,
Daughter	Daughter	k1gMnSc1	Daughter
<g/>
,	,	kIx,	,
Rearviewmirror	Rearviewmirror	k1gMnSc1	Rearviewmirror
<g/>
,	,	kIx,	,
Animal	animal	k1gMnSc1	animal
a	a	k8xC	a
Elderly	Elderla	k1gMnSc2	Elderla
Woman	Womana	k1gFnPc2	Womana
Behind	Behind	k1gMnSc1	Behind
The	The	k1gMnSc1	The
Counter	Counter	k1gMnSc1	Counter
In	In	k1gMnSc1	In
A	a	k8xC	a
Small	Small	k1gMnSc1	Small
Town	Town	k1gMnSc1	Town
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
masovém	masový	k2eAgInSc6d1	masový
úspěchu	úspěch	k1gInSc6	úspěch
grunge	grung	k1gInSc2	grung
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
úspěch	úspěch	k1gInSc4	úspěch
alba	album	k1gNnSc2	album
Nevermind	Neverminda	k1gFnPc2	Neverminda
od	od	k7c2	od
Nirvany	Nirvan	k1gMnPc7	Nirvan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
hudební	hudební	k2eAgFnSc2d1	hudební
firmy	firma	k1gFnSc2	firma
snažili	snažit	k5eAaImAgMnP	snažit
vytěžit	vytěžit	k5eAaPmF	vytěžit
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kapel	kapela	k1gFnPc2	kapela
co	co	k9	co
nejvíc	nejvíc	k6eAd1	nejvíc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
spor	spor	k1gInSc1	spor
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Ticketmaster	Ticketmastra	k1gFnPc2	Ticketmastra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prodávala	prodávat	k5eAaImAgFnS	prodávat
předražené	předražený	k2eAgInPc4d1	předražený
lístky	lístek	k1gInPc4	lístek
na	na	k7c4	na
jejich	jejich	k3xOp3gInPc4	jejich
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
na	na	k7c4	na
protest	protest	k1gInSc4	protest
zrušila	zrušit	k5eAaPmAgFnS	zrušit
letní	letní	k2eAgNnSc4d1	letní
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Vitalogy	Vitaloga	k1gFnSc2	Vitaloga
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1994	[number]	k4	1994
(	(	kIx(	(
<g/>
na	na	k7c6	na
LP	LP	kA	LP
už	už	k6eAd1	už
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
skupinu	skupina	k1gFnSc4	skupina
opustil	opustit	k5eAaPmAgMnS	opustit
bubeník	bubeník	k1gMnSc1	bubeník
Dave	Dav	k1gInSc5	Dav
Abbruzzese	Abbruzzesa	k1gFnSc3	Abbruzzesa
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gInSc4	on
Jack	Jack	k1gInSc4	Jack
Irons	Ironsa	k1gFnPc2	Ironsa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Abbruzzesem	Abbruzzes	k1gMnSc7	Abbruzzes
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
členy	člen	k1gMnPc7	člen
skupiny	skupina	k1gFnSc2	skupina
byly	být	k5eAaImAgFnP	být
názorové	názorový	k2eAgFnPc1d1	názorová
neshody	neshoda	k1gFnPc1	neshoda
<g/>
,	,	kIx,	,
Abbruzzese	Abbruzzese	k1gFnSc1	Abbruzzese
například	například	k6eAd1	například
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
bojkotem	bojkot	k1gInSc7	bojkot
Ticketmaster	Ticketmastra	k1gFnPc2	Ticketmastra
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
skladba	skladba	k1gFnSc1	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
Vitaology	Vitaolog	k1gMnPc4	Vitaolog
<g/>
,	,	kIx,	,
Spin	spin	k1gInSc1	spin
The	The	k1gFnSc2	The
Black	Blacka	k1gFnPc2	Blacka
Circle	Circle	k1gFnSc2	Circle
<g/>
,	,	kIx,	,
<g/>
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Vitalogy	Vitalog	k1gMnPc4	Vitalog
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
skladby	skladba	k1gFnPc4	skladba
Not	nota	k1gFnPc2	nota
For	forum	k1gNnPc2	forum
You	You	k1gMnPc2	You
<g/>
,	,	kIx,	,
Corduroy	Corduroa	k1gFnPc1	Corduroa
<g/>
,	,	kIx,	,
Immortality	Immortalita	k1gFnPc1	Immortalita
a	a	k8xC	a
Better	Better	k1gMnSc1	Better
Man	Man	k1gMnSc1	Man
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
písni	píseň	k1gFnSc6	píseň
Immortality	Immortalita	k1gFnSc2	Immortalita
a	a	k8xC	a
Better	Better	k1gInSc1	Better
Man	mana	k1gFnPc2	mana
panoval	panovat	k5eAaImAgInS	panovat
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
Kurta	Kurt	k1gMnSc2	Kurt
Cobaina	Cobain	k1gMnSc2	Cobain
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
Eddie	Eddie	k1gFnSc1	Eddie
Vedder	Veddra	k1gFnPc2	Veddra
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
.	.	kIx.	.
</s>
<s>
Pearl	Pearl	k1gInSc1	Pearl
Jam	jáma	k1gFnPc2	jáma
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
bojkotu	bojkot	k1gInSc6	bojkot
firmy	firma	k1gFnSc2	firma
Ticketmaster	Ticketmastra	k1gFnPc2	Ticketmastra
i	i	k9	i
při	při	k7c6	při
turné	turné	k1gNnSc6	turné
Vitalogy	Vitaloga	k1gFnSc2	Vitaloga
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Nehráli	hrát	k5eNaImAgMnP	hrát
na	na	k7c6	na
scénách	scéna	k1gFnPc6	scéna
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
lístky	lístek	k1gInPc4	lístek
a	a	k8xC	a
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
nehráli	hrát	k5eNaImAgMnP	hrát
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
desky	deska	k1gFnPc1	deska
už	už	k6eAd1	už
nebyly	být	k5eNaImAgFnP	být
takovým	takový	k3xDgNnSc7	takový
horkým	horký	k2eAgNnSc7d1	horké
zbožím	zboží	k1gNnSc7	zboží
jako	jako	k9	jako
předcházející	předcházející	k2eAgMnSc1d1	předcházející
tři	tři	k4xCgNnPc1	tři
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
No	no	k9	no
Code	Code	k1gNnSc1	Code
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
zvuk	zvuk	k1gInSc1	zvuk
kapely	kapela	k1gFnSc2	kapela
se	se	k3xPyFc4	se
posunul	posunout	k5eAaPmAgInS	posunout
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
albem	album	k1gNnSc7	album
sice	sice	k8xC	sice
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
americké	americký	k2eAgFnSc2d1	americká
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydržela	vydržet	k5eAaPmAgFnS	vydržet
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
spadla	spadnout	k5eAaPmAgFnS	spadnout
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vystoupalo	vystoupat	k5eAaPmAgNnS	vystoupat
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
americké	americký	k2eAgFnSc2d1	americká
albové	albový	k2eAgFnSc2d1	albová
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
experimentální	experimentální	k2eAgInSc1d1	experimentální
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
cítit	cítit	k5eAaImF	cítit
i	i	k8xC	i
jiné	jiný	k2eAgInPc4d1	jiný
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
úderná	úderný	k2eAgFnSc1d1	úderná
jako	jako	k8xC	jako
předchozí	předchozí	k2eAgFnSc1d1	předchozí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
i	i	k9	i
mluvené	mluvený	k2eAgNnSc4d1	mluvené
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
No	no	k9	no
Code	Code	k1gInSc1	Code
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
singly	singl	k1gInPc7	singl
Hail	Haila	k1gFnPc2	Haila
<g/>
,	,	kIx,	,
Hail	Hail	k1gMnSc1	Hail
<g/>
,	,	kIx,	,
Who	Who	k1gMnSc1	Who
You	You	k1gMnSc1	You
Are	ar	k1gInSc5	ar
a	a	k8xC	a
Off	Off	k1gMnSc1	Off
He	he	k0	he
Goes	Goes	k1gInSc4	Goes
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
bojkot	bojkot	k1gInSc1	bojkot
Ticketmasters	Ticketmastersa	k1gFnPc2	Ticketmastersa
stále	stále	k6eAd1	stále
trval	trvat	k5eAaImAgInS	trvat
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
téměř	téměř	k6eAd1	téměř
nehrála	hrát	k5eNaImAgFnS	hrát
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
evropské	evropský	k2eAgNnSc4d1	Evropské
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Pátá	pátý	k4xOgFnSc1	pátý
deska	deska	k1gFnSc1	deska
Yield	Yielda	k1gFnPc2	Yielda
vyšla	vyjít	k5eAaPmAgFnS	vyjít
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Vrátili	vrátit	k5eAaPmAgMnP	vrátit
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
ke	k	k7c3	k
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
měla	mít	k5eAaImAgFnS	mít
alba	alba	k1gFnSc1	alba
jejich	jejich	k3xOp3gInPc2	jejich
začátků	začátek	k1gInPc2	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c4	na
první	první	k4xOgFnSc4	první
místo	místo	k7c2	místo
americké	americký	k2eAgFnSc2d1	americká
albové	albový	k2eAgFnSc2d1	albová
hitparády	hitparáda	k1gFnSc2	hitparáda
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
jsou	být	k5eAaImIp3nP	být
Given	Given	k1gInSc4	Given
to	ten	k3xDgNnSc1	ten
Fly	Fly	k1gMnSc1	Fly
<g/>
,	,	kIx,	,
Wishlist	Wishlist	k1gMnSc1	Wishlist
a	a	k8xC	a
Do	do	k7c2	do
the	the	k?	the
Evolution	Evolution	k1gInSc1	Evolution
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
odešel	odejít	k5eAaPmAgMnS	odejít
bubeník	bubeník	k1gMnSc1	bubeník
Jack	Jack	k1gMnSc1	Jack
Irons	Irons	k1gInSc4	Irons
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
ho	on	k3xPp3gNnSc4	on
Matt	Matt	k1gMnSc1	Matt
Cameron	Cameron	k1gMnSc1	Cameron
z	z	k7c2	z
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
neexistujících	existující	k2eNgFnPc2d1	neexistující
Soundgarden	Soundgardna	k1gFnPc2	Soundgardna
<g/>
.	.	kIx.	.
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
deska	deska	k1gFnSc1	deska
Binaural	Binaural	k1gFnSc2	Binaural
spatřila	spatřit	k5eAaPmAgFnS	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
singly	singl	k1gInPc4	singl
Nothing	Nothing	k1gInSc1	Nothing
As	as	k1gNnSc6	as
It	It	k1gFnSc2	It
Seems	Seems	k1gInSc4	Seems
a	a	k8xC	a
Light	Light	k2eAgInSc4d1	Light
Years	Years	k1gInSc4	Years
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
jen	jen	k9	jen
asi	asi	k9	asi
700	[number]	k4	700
tisíc	tisíc	k4xCgInPc2	tisíc
kopií	kopie	k1gFnSc7	kopie
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
této	tento	k3xDgFnSc2	tento
desky	deska	k1gFnSc2	deska
mělo	mít	k5eAaImAgNnS	mít
podpořit	podpořit	k5eAaPmF	podpořit
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ale	ale	k9	ale
skončilo	skončit	k5eAaPmAgNnS	skončit
tragicky	tragicky	k6eAd1	tragicky
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
se	se	k3xPyFc4	se
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
dánském	dánský	k2eAgNnSc6d1	dánské
Roskilde	Roskild	k1gInSc5	Roskild
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
zvukovým	zvukový	k2eAgInPc3d1	zvukový
problémům	problém	k1gInPc3	problém
začal	začít	k5eAaPmAgInS	začít
tlačit	tlačit	k5eAaImF	tlačit
dav	dav	k1gInSc1	dav
asi	asi	k9	asi
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
co	co	k9	co
nejvíc	nejvíc	k6eAd1	nejvíc
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
hodině	hodina	k1gFnSc6	hodina
kapela	kapela	k1gFnSc1	kapela
hraní	hraň	k1gFnPc2	hraň
přerušila	přerušit	k5eAaPmAgFnS	přerušit
a	a	k8xC	a
Eddie	Eddie	k1gFnSc2	Eddie
Vedder	Vedder	k1gMnSc1	Vedder
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
postoupil	postoupit	k5eAaPmAgInS	postoupit
víc	hodně	k6eAd2	hodně
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předchozím	předchozí	k2eAgInSc6d1	předchozí
dešti	dešť	k1gInSc6	dešť
byla	být	k5eAaImAgFnS	být
zem	zem	k1gFnSc1	zem
rozblácená	rozblácený	k2eAgFnSc1d1	rozblácená
a	a	k8xC	a
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
upadlo	upadnout	k5eAaPmAgNnS	upadnout
<g/>
.	.	kIx.	.
9	[number]	k4	9
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
ušlapáno	ušlapat	k5eAaPmNgNnS	ušlapat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
udusilo	udusit	k5eAaPmAgNnS	udusit
<g/>
,	,	kIx,	,
dalších	další	k2eAgMnPc2d1	další
30	[number]	k4	30
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc4d1	zbylý
dva	dva	k4xCgInPc4	dva
koncerty	koncert	k1gInPc4	koncert
evropského	evropský	k2eAgNnSc2d1	Evropské
turné	turné	k1gNnSc2	turné
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
zvažovala	zvažovat	k5eAaImAgFnS	zvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přestane	přestat	k5eAaPmIp3nS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
se	se	k3xPyFc4	se
Eddie	Eddie	k1gFnSc1	Eddie
Vedder	Veddra	k1gFnPc2	Veddra
a	a	k8xC	a
Mike	Mike	k1gFnPc2	Mike
McCready	McCreada	k1gFnSc2	McCreada
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
Neilem	Neil	k1gMnSc7	Neil
Youngem	Young	k1gInSc7	Young
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
představili	představit	k5eAaPmAgMnP	představit
skladbu	skladba	k1gFnSc4	skladba
Long	Longa	k1gFnPc2	Longa
Road	Roado	k1gNnPc2	Roado
na	na	k7c6	na
benefičním	benefiční	k2eAgInSc6d1	benefiční
koncertě	koncert	k1gInSc6	koncert
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
album	album	k1gNnSc1	album
Riot	Riot	k1gInSc4	Riot
Act	Act	k1gMnPc2	Act
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
zklamáním	zklamání	k1gNnSc7	zklamání
a	a	k8xC	a
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
jen	jen	k9	jen
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
album	album	k1gNnSc1	album
experimentuje	experimentovat	k5eAaImIp3nS	experimentovat
s	s	k7c7	s
folkovými	folkový	k2eAgInPc7d1	folkový
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
Arc	Arc	k1gFnSc2	Arc
je	být	k5eAaImIp3nS	být
poctou	pocta	k1gFnSc7	pocta
devíti	devět	k4xCc3	devět
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Roskilde	Roskild	k1gMnSc5	Roskild
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
tragédii	tragédie	k1gFnSc4	tragédie
také	také	k9	také
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
skladba	skladba	k1gFnSc1	skladba
Love	lov	k1gInSc5	lov
Boat	Boata	k1gFnPc2	Boata
Captain	Captain	k1gInSc1	Captain
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zpívá	zpívat	k5eAaImIp3nS	zpívat
"	"	kIx"	"
<g/>
Lost	Lost	k1gInSc4	Lost
9	[number]	k4	9
friends	friends	k1gInSc1	friends
we	we	k?	we
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
never	never	k1gInSc1	never
know	know	k?	know
<g/>
...	...	k?	...
2	[number]	k4	2
years	yearsa	k1gFnPc2	yearsa
ago	aga	k1gMnSc5	aga
today	todaum	k1gNnPc7	todaum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Před	před	k7c7	před
dvěma	dva	k4xCgNnPc7	dva
lety	léto	k1gNnPc7	léto
jsme	být	k5eAaImIp1nP	být
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
9	[number]	k4	9
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsme	být	k5eAaImIp1nP	být
nikdy	nikdy	k6eAd1	nikdy
nepoznali	poznat	k5eNaPmAgMnP	poznat
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
kapela	kapela	k1gFnSc1	kapela
opustila	opustit	k5eAaPmAgFnS	opustit
label	label	k1gInSc4	label
Epic	Epic	k1gFnSc1	Epic
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
vydali	vydat	k5eAaPmAgMnP	vydat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
skladba	skladba	k1gFnSc1	skladba
Man	mana	k1gFnPc2	mana
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Hour	Hour	k1gInSc4	Hour
k	k	k7c3	k
filmu	film	k1gInSc3	film
Big	Big	k1gMnSc1	Big
Fish	Fish	k1gMnSc1	Fish
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
natočil	natočit	k5eAaBmAgMnS	natočit
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc4	Burton
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
ještě	ještě	k6eAd1	ještě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
B-stran	Btrana	k1gFnPc2	B-strana
a	a	k8xC	a
rarit	rarita	k1gFnPc2	rarita
Lost	Losta	k1gFnPc2	Losta
Dogs	Dogs	k1gInSc1	Dogs
a	a	k8xC	a
DVD	DVD	kA	DVD
z	z	k7c2	z
vystoupení	vystoupení	k1gNnSc2	vystoupení
z	z	k7c2	z
Madison	Madisona	k1gFnPc2	Madisona
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Live	Live	k1gFnSc1	Live
at	at	k?	at
the	the	k?	the
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
následuvalo	následuvat	k5eAaImAgNnS	následuvat
koncertní	koncertní	k2eAgNnSc1d1	koncertní
album	album	k1gNnSc1	album
Live	Liv	k1gInSc2	Liv
at	at	k?	at
Benaroya	Benaroya	k1gMnSc1	Benaroya
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kompilace	kompilace	k1gFnSc1	kompilace
Rearviewmirror	Rearviewmirrora	k1gFnPc2	Rearviewmirrora
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgNnSc4	který
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
hity	hit	k1gInPc1	hit
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc1d1	poslední
studiová	studiový	k2eAgFnSc1d1	studiová
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Pearl	Pearl	k1gInSc1	Pearl
Jam	jáma	k1gFnPc2	jáma
a	a	k8xC	a
vyšla	vyjít	k5eAaPmAgFnS	vyjít
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
World	Worlda	k1gFnPc2	Worlda
Wide	Widus	k1gMnSc5	Widus
Suicide	Suicid	k1gMnSc5	Suicid
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hitparády	hitparáda	k1gFnSc2	hitparáda
Billboard	billboard	k1gInSc1	billboard
Modern	Modern	k1gInSc1	Modern
Rock	rock	k1gInSc1	rock
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Pearl	Pearl	k1gInSc1	Pearl
Jam	jáma	k1gFnPc2	jáma
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nehráli	hrát	k5eNaImAgMnP	hrát
už	už	k6eAd1	už
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
headlinerem	headliner	k1gMnSc7	headliner
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
festivalech	festival	k1gInPc6	festival
Reading	Reading	k1gInSc1	Reading
a	a	k8xC	a
Leeds	Leeds	k1gInSc1	Leeds
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
po	po	k7c6	po
incidentu	incident	k1gInSc6	incident
v	v	k7c6	v
dánském	dánský	k2eAgNnSc6d1	dánské
Roskilde	Roskild	k1gInSc5	Roskild
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
na	na	k7c6	na
žádných	žádný	k3yNgInPc6	žádný
festivalech	festival	k1gInPc6	festival
nehrát	hrát	k5eNaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Eddie	Eddie	k1gFnSc1	Eddie
Vedder	Veddra	k1gFnPc2	Veddra
začal	začít	k5eAaPmAgInS	začít
oba	dva	k4xCgInPc4	dva
koncerty	koncert	k1gInPc4	koncert
prosbou	prosba	k1gFnSc7	prosba
k	k	k7c3	k
publiku	publikum	k1gNnSc3	publikum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
brali	brát	k5eAaImAgMnP	brát
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
ohled	ohled	k1gInSc4	ohled
<g/>
.	.	kIx.	.
</s>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Ament	Ament	k1gMnSc1	Ament
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Eddie	Eddie	k1gFnSc1	Eddie
Vedder	Vedder	k1gMnSc1	Vedder
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Stone	ston	k1gInSc5	ston
Gossard	Gossard	k1gInSc1	Gossard
-	-	kIx~	-
levá	levý	k2eAgFnSc1d1	levá
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Matt	Matt	k2eAgInSc1d1	Matt
Cameron	Cameron	k1gInSc1	Cameron
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
od	od	k7c2	od
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Mike	Mike	k1gNnSc4	Mike
McCready	McCreada	k1gFnPc1	McCreada
-	-	kIx~	-
pravá	pravý	k2eAgFnSc1d1	pravá
kytara	kytara	k1gFnSc1	kytara
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
od	od	k7c2	od
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Boom	boom	k1gInSc1	boom
Gaspar	Gaspar	k1gInSc1	Gaspar
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
(	(	kIx(	(
<g/>
od	od	k7c2	od
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Dříve	dříve	k6eAd2	dříve
<g/>
:	:	kIx,	:
Dave	Dav	k1gInSc5	Dav
Krusen	krusna	k1gFnPc2	krusna
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
až	až	k9	až
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Matt	Matt	k2eAgInSc1d1	Matt
Chamberlain	Chamberlain	k1gInSc1	Chamberlain
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Abbruzzese	Abbruzzese	k1gFnPc1	Abbruzzese
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
až	až	k9	až
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Jack	Jack	k1gInSc1	Jack
Irons	Irons	k1gInSc1	Irons
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
až	až	k9	až
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
-	-	kIx~	-
Ten	ten	k3xDgInSc1	ten
1993	[number]	k4	1993
-	-	kIx~	-
Vs	Vs	k1gFnSc2	Vs
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
-	-	kIx~	-
Vitalogy	Vitaloga	k1gFnSc2	Vitaloga
1996	[number]	k4	1996
-	-	kIx~	-
No	no	k9	no
Code	Code	k1gInSc1	Code
1998	[number]	k4	1998
-	-	kIx~	-
Yield	Yield	k1gInSc1	Yield
1998	[number]	k4	1998
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
on	on	k3xPp3gMnSc1	on
Two	Two	k1gMnSc1	Two
Legs	Legs	k1gInSc4	Legs
2000	[number]	k4	2000
-	-	kIx~	-
Binaural	Binaural	k1gFnSc1	Binaural
2002	[number]	k4	2002
-	-	kIx~	-
Riot	Riot	k1gInSc1	Riot
Act	Act	k1gFnSc2	Act
2003	[number]	k4	2003
-	-	kIx~	-
Lost	Lost	k2eAgInSc1d1	Lost
Dogs	Dogs	k1gInSc1	Dogs
-	-	kIx~	-
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
písničky	písnička	k1gFnSc2	písnička
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
B-strany	Btran	k1gInPc4	B-stran
singlů	singl	k1gInPc2	singl
2004	[number]	k4	2004
-	-	kIx~	-
RearViewMirror	RearViewMirror	k1gInSc1	RearViewMirror
-	-	kIx~	-
best	best	k1gInSc1	best
of	of	k?	of
2006	[number]	k4	2006
-	-	kIx~	-
Pearl	Pearl	k1gInSc1	Pearl
Jam	jam	k1gInSc1	jam
2009	[number]	k4	2009
-	-	kIx~	-
Backspacer	Backspacer	k1gInSc1	Backspacer
2011	[number]	k4	2011
-	-	kIx~	-
PJ20	PJ20	k1gFnSc1	PJ20
-	-	kIx~	-
soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
dokumentu	dokument	k1gInSc3	dokument
o	o	k7c4	o
Pearl	Pearl	k1gInSc4	Pearl
Jam	jáma	k1gFnPc2	jáma
(	(	kIx(	(
<g/>
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
verze	verze	k1gFnSc2	verze
a	a	k8xC	a
dema	demum	k1gNnSc2	demum
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Lightning	Lightning	k1gInSc4	Lightning
Bolt	Bolt	k2eAgInSc4d1	Bolt
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pearl	Pearla	k1gFnPc2	Pearla
Jam	jáma	k1gFnPc2	jáma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
