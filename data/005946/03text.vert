<s>
Pojem	pojem	k1gInSc4	pojem
klínové	klínový	k2eAgNnSc1d1	klínové
písmo	písmo	k1gNnSc1	písmo
znamená	znamenat	k5eAaImIp3nS	znamenat
způsob	způsob	k1gInSc4	způsob
zápisu	zápis	k1gInSc2	zápis
<g/>
,	,	kIx,	,
styl	styl	k1gInSc1	styl
psaní	psaní	k1gNnSc2	psaní
znaků	znak	k1gInPc2	znak
starověkých	starověký	k2eAgNnPc2d1	starověké
písem	písmo	k1gNnPc2	písmo
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc4d1	určený
prostředky	prostředek	k1gInPc7	prostředek
dostupnými	dostupný	k2eAgInPc7d1	dostupný
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hlínou	hlína	k1gFnSc7	hlína
a	a	k8xC	a
rákosem	rákos	k1gInSc7	rákos
<g/>
.	.	kIx.	.
</s>
<s>
Neoznačuje	označovat	k5eNaImIp3nS	označovat
žádnou	žádný	k3yNgFnSc4	žádný
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
sadu	sada	k1gFnSc4	sada
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
latinka	latinka	k1gFnSc1	latinka
nebo	nebo	k8xC	nebo
alfabeta	alfabeta	k1gFnSc1	alfabeta
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
byly	být	k5eAaImAgFnP	být
standardně	standardně	k6eAd1	standardně
psané	psaný	k2eAgFnPc1d1	psaná
<g/>
,	,	kIx,	,
vytlačované	vytlačovaný	k2eAgNnSc1d1	vytlačované
rákosovým	rákosový	k2eAgNnSc7d1	rákosové
pisátkem	pisátko	k1gNnSc7	pisátko
do	do	k7c2	do
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hliněných	hliněný	k2eAgFnPc2d1	hliněná
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
trvanlivosti	trvanlivost	k1gFnSc3	trvanlivost
následně	následně	k6eAd1	následně
vypálena	vypálen	k2eAgFnSc1d1	vypálena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vypálení	vypálení	k1gNnSc3	vypálení
také	také	k6eAd1	také
došlo	dojít	k5eAaPmAgNnS	dojít
často	často	k6eAd1	často
nechtěně	chtěně	k6eNd1	chtěně
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgNnP	být
města	město	k1gNnPc1	město
vypálena	vypálen	k2eAgNnPc1d1	vypáleno
nepřátelskými	přátelský	k2eNgNnPc7d1	nepřátelské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgFnSc4d1	dnešní
archeologii	archeologie	k1gFnSc4	archeologie
znamená	znamenat	k5eAaImIp3nS	znamenat
uchování	uchování	k1gNnSc4	uchování
mnoha	mnoho	k4c2	mnoho
klínopisných	klínopisný	k2eAgInPc2d1	klínopisný
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jinak	jinak	k6eAd1	jinak
nezachovaly	zachovat	k5eNaPmAgFnP	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Klínopisné	klínopisný	k2eAgInPc1d1	klínopisný
znaky	znak	k1gInPc1	znak
ale	ale	k9	ale
mohly	moct	k5eAaImAgInP	moct
také	také	k9	také
být	být	k5eAaImF	být
tesány	tesán	k2eAgInPc4d1	tesán
do	do	k7c2	do
kamene	kámen	k1gInSc2	kámen
či	či	k8xC	či
psány	psán	k2eAgInPc1d1	psán
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
počátcích	počátek	k1gInPc6	počátek
sumerské	sumerský	k2eAgInPc1d1	sumerský
klínové	klínový	k2eAgInPc1d1	klínový
znaky	znak	k1gInPc1	znak
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
starších	starý	k2eAgInPc2d2	starší
piktogramů	piktogram	k1gInPc2	piktogram
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
otočeny	otočit	k5eAaPmNgInP	otočit
o	o	k7c6	o
90	[number]	k4	90
<g/>
°	°	k?	°
a	a	k8xC	a
zjednodušeny	zjednodušit	k5eAaPmNgInP	zjednodušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
znázornit	znázornit	k5eAaPmF	znázornit
ustálenými	ustálený	k2eAgInPc7d1	ustálený
tahy	tah	k1gInPc7	tah
rydla	rydlo	k1gNnSc2	rydlo
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
později	pozdě	k6eAd2	pozdě
převzali	převzít	k5eAaPmAgMnP	převzít
Akkaďané	Akkaďan	k1gMnPc1	Akkaďan
(	(	kIx(	(
<g/>
např.	např.	kA	např.
El-amarnské	Elmarnský	k2eAgInPc1d1	El-amarnský
dopisy	dopis	k1gInPc1	dopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
a	a	k8xC	a
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
toto	tento	k3xDgNnSc4	tento
písmo	písmo	k1gNnSc4	písmo
částečně	částečně	k6eAd1	částečně
upravili	upravit	k5eAaPmAgMnP	upravit
<g/>
,	,	kIx,	,
stylizovali	stylizovat	k5eAaImAgMnP	stylizovat
a	a	k8xC	a
zjednodušili	zjednodušit	k5eAaPmAgMnP	zjednodušit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
semitskou	semitský	k2eAgFnSc4d1	semitská
akkadštinu	akkadština	k1gFnSc4	akkadština
jako	jako	k8xS	jako
slabičné	slabičný	k2eAgFnPc4d1	slabičná
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
struktuře	struktura	k1gFnSc3	struktura
semitských	semitský	k2eAgInPc2d1	semitský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
toto	tento	k3xDgNnSc4	tento
písmo	písmo	k1gNnSc4	písmo
dále	daleko	k6eAd2	daleko
převzaly	převzít	k5eAaPmAgFnP	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Klínové	klínový	k2eAgNnSc1d1	klínové
písmo	písmo	k1gNnSc1	písmo
akkadských	akkadský	k2eAgInPc2d1	akkadský
dialektů	dialekt	k1gInPc2	dialekt
většinou	většina	k1gFnSc7	většina
není	být	k5eNaImIp3nS	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
-	-	kIx~	-
znaky	znak	k1gInPc1	znak
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
hodnotu	hodnota	k1gFnSc4	hodnota
slabikovou	slabikový	k2eAgFnSc4d1	slabiková
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
hodnotu	hodnota	k1gFnSc4	hodnota
logogramu	logogram	k1gInSc2	logogram
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hodnotu	hodnota	k1gFnSc4	hodnota
celého	celý	k2eAgNnSc2d1	celé
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
mohou	moct	k5eAaImIp3nP	moct
prostě	prostě	k9	prostě
jen	jen	k9	jen
označovat	označovat	k5eAaImF	označovat
druh	druh	k1gInSc4	druh
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
následuje	následovat	k5eAaImIp3nS	následovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
GIŠ	GIŠ	kA	GIŠ
"	"	kIx"	"
<g/>
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
"	"	kIx"	"
či	či	k8xC	či
DINGIR	DINGIR	kA	DINGIR
"	"	kIx"	"
<g/>
bůh	bůh	k1gMnSc1	bůh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
četly	číst	k5eAaImAgFnP	číst
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obvykle	obvykle	k6eAd1	obvykle
stojí	stát	k5eAaImIp3nS	stát
před	před	k7c7	před
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
determinativ	determinativ	k1gInSc1	determinativ
<g/>
.	.	kIx.	.
</s>
<s>
Jednoznačné	jednoznačný	k2eAgFnPc1d1	jednoznačná
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
ani	ani	k8xC	ani
samotné	samotný	k2eAgFnPc1d1	samotná
slabičné	slabičný	k2eAgFnPc1d1	slabičná
hodnoty	hodnota	k1gFnPc1	hodnota
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
znak	znak	k1gInSc1	znak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
čten	číst	k5eAaImNgInS	číst
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
jedna	jeden	k4xCgFnSc1	jeden
slabika	slabika	k1gFnSc1	slabika
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
několika	několik	k4yIc7	několik
různými	různý	k2eAgInPc7d1	různý
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Čtení	čtení	k1gNnSc4	čtení
těchto	tento	k3xDgInPc2	tento
slabičných	slabičný	k2eAgInPc2d1	slabičný
klínopisných	klínopisný	k2eAgInPc2d1	klínopisný
textů	text	k1gInPc2	text
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
klínové	klínový	k2eAgNnSc4d1	klínové
slabičné	slabičný	k2eAgNnSc4d1	slabičné
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	on	k3xPp3gMnPc4	on
používali	používat	k5eAaImAgMnP	používat
Asyřané	Asyřan	k1gMnPc1	Asyřan
a	a	k8xC	a
Babyloňané	Babyloňan	k1gMnPc1	Babyloňan
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
zjednodušením	zjednodušení	k1gNnPc3	zjednodušení
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
úprava	úprava	k1gFnSc1	úprava
babylonského	babylonský	k2eAgInSc2d1	babylonský
klínopisu	klínopis	k1gInSc2	klínopis
pro	pro	k7c4	pro
starou	starý	k2eAgFnSc4d1	stará
(	(	kIx(	(
<g/>
indoevropskou	indoevropský	k2eAgFnSc4d1	indoevropská
<g/>
)	)	kIx)	)
perštinu	perština	k1gFnSc4	perština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
počet	počet	k1gInSc4	počet
znaků	znak	k1gInPc2	znak
notně	notně	k6eAd1	notně
omezila	omezit	k5eAaPmAgFnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
toto	tento	k3xDgNnSc4	tento
písmo	písmo	k1gNnSc4	písmo
však	však	k9	však
nepřevzaly	převzít	k5eNaPmAgInP	převzít
jen	jen	k9	jen
mezopotámské	mezopotámský	k2eAgInPc1d1	mezopotámský
semitské	semitský	k2eAgInPc1d1	semitský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
např.	např.	kA	např.
vzdálenější	vzdálený	k2eAgFnSc1d2	vzdálenější
indoevropská	indoevropský	k2eAgFnSc1d1	indoevropská
chetitština	chetitština	k1gFnSc1	chetitština
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
slabičné	slabičný	k2eAgFnSc3d1	slabičná
formě	forma	k1gFnSc3	forma
znaků	znak	k1gInPc2	znak
mohl	moct	k5eAaImAgMnS	moct
pak	pak	k6eAd1	pak
Bedřich	Bedřich	k1gMnSc1	Bedřich
Hrozný	hrozný	k2eAgMnSc1d1	hrozný
chetitské	chetitský	k2eAgInPc4d1	chetitský
texty	text	k1gInPc4	text
dešifrovat	dešifrovat	k5eAaBmF	dešifrovat
a	a	k8xC	a
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgNnPc2	tento
slabikových	slabikový	k2eAgNnPc2d1	slabikové
a	a	k8xC	a
poloslabičných	poloslabičný	k2eAgNnPc2d1	poloslabičný
složitých	složitý	k2eAgNnPc2d1	složité
písem	písmo	k1gNnPc2	písmo
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
klínové	klínový	k2eAgNnSc1d1	klínové
písmo	písmo	k1gNnSc1	písmo
ryze	ryze	k6eAd1	ryze
hláskové	hláskový	k2eAgNnSc1d1	hláskové
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc1	každý
znak	znak	k1gInSc1	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jedné	jeden	k4xCgFnSc3	jeden
hlásce	hláska	k1gFnSc3	hláska
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
souhlásce	souhláska	k1gFnSc3	souhláska
<g/>
,	,	kIx,	,
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
nezapisovaly	zapisovat	k5eNaImAgFnP	zapisovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
ugaritské	ugaritský	k2eAgNnSc4d1	ugaritský
klínové	klínový	k2eAgNnSc4d1	klínové
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
i	i	k9	i
v	v	k7c6	v
semitských	semitský	k2eAgInPc6d1	semitský
jazycích	jazyk	k1gInPc6	jazyk
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
písmo	písmo	k1gNnSc4	písmo
hláskové	hláskový	k2eAgNnSc4d1	hláskové
podle	podle	k7c2	podle
fénického	fénický	k2eAgInSc2d1	fénický
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
rozmachem	rozmach	k1gInSc7	rozmach
<g/>
,	,	kIx,	,
souvisejícím	související	k2eAgMnSc7d1	související
také	také	k9	také
s	s	k7c7	s
prosazením	prosazení	k1gNnSc7	prosazení
nových	nový	k2eAgInPc2d1	nový
psacích	psací	k2eAgInPc2d1	psací
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
papyru	papyr	k1gInSc2	papyr
s	s	k7c7	s
inkoustem	inkoust	k1gInSc7	inkoust
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
skončila	skončit	k5eAaPmAgFnS	skončit
éra	éra	k1gFnSc1	éra
klínového	klínový	k2eAgNnSc2d1	klínové
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
