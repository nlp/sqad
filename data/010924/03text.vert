<p>
<s>
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Marka	Marek	k1gMnSc4	Marek
Rutteho	Rutte	k1gMnSc4	Rutte
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
menšinová	menšinový	k2eAgFnSc1d1	menšinová
koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
dvou	dva	k4xCgFnPc2	dva
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
–	–	k?	–
Lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
VVD	VVD	kA	VVD
<g/>
,	,	kIx,	,
31	[number]	k4	31
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
Křesťanskodemokratické	křesťanskodemokratický	k2eAgFnSc2d1	Křesťanskodemokratická
výzvy	výzva	k1gFnSc2	výzva
(	(	kIx(	(
<g/>
CDA	CDA	kA	CDA
<g/>
,	,	kIx,	,
21	[number]	k4	21
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Ruttem	Rutt	k1gMnSc7	Rutt
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
svobodu	svoboda	k1gFnSc4	svoboda
(	(	kIx(	(
<g/>
PVV	PVV	kA	PVV
<g/>
,	,	kIx,	,
24	[number]	k4	24
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
přímo	přímo	k6eAd1	přímo
neúčastní	účastnit	k5eNaImIp3nP	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Jmenována	jmenován	k2eAgFnSc1d1	jmenována
byla	být	k5eAaImAgFnS	být
královnou	královna	k1gFnSc7	královna
Beatrix	Beatrix	k1gInSc1	Beatrix
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
složení	složení	k1gNnSc1	složení
vzešlo	vzejít	k5eAaPmAgNnS	vzejít
z	z	k7c2	z
výsledku	výsledek	k1gInSc2	výsledek
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
kabinet	kabinet	k1gInSc4	kabinet
Jana	Jan	k1gMnSc2	Jan
Petera	Peter	k1gMnSc2	Peter
Balkenendeho	Balkenende	k1gMnSc2	Balkenende
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
PVV	PVV	kA	PVV
vláda	vláda	k1gFnSc1	vláda
disponovala	disponovat	k5eAaBmAgFnS	disponovat
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
těsnou	těsný	k2eAgFnSc4d1	těsná
většinou	většina	k1gFnSc7	většina
až	až	k8xS	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
PVV	PVV	kA	PVV
opustil	opustit	k5eAaPmAgMnS	opustit
poslanec	poslanec	k1gMnSc1	poslanec
Hero	Hero	k1gMnSc1	Hero
Brinkman	Brinkman	k1gMnSc1	Brinkman
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Brinkman	Brinkman	k1gMnSc1	Brinkman
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
vládu	vláda	k1gFnSc4	vláda
podporovat	podporovat	k5eAaImF	podporovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
jako	jako	k9	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
<g/>
,	,	kIx,	,
PVV	PVV	kA	PVV
odvolala	odvolat	k5eAaPmAgFnS	odvolat
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
vládě	vláda	k1gFnSc6	vláda
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
když	když	k8xS	když
selhala	selhat	k5eAaPmAgFnS	selhat
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
nových	nový	k2eAgNnPc6d1	nové
úsporných	úsporný	k2eAgNnPc6d1	úsporné
opatřeních	opatření	k1gNnPc6	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
předčasným	předčasný	k2eAgFnPc3d1	předčasná
volbám	volba	k1gFnPc3	volba
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgInPc2	jenž
byla	být	k5eAaImAgFnS	být
vláda	vláda	k1gFnSc1	vláda
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
druhou	druhý	k4xOgFnSc4	druhý
vládou	vláda	k1gFnSc7	vláda
Marka	Marek	k1gMnSc2	Marek
Rutteho	Rutte	k1gMnSc2	Rutte
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvanácti	dvanáct	k4xCc7	dvanáct
ministry	ministr	k1gMnPc7	ministr
a	a	k8xC	a
osmi	osm	k4xCc7	osm
státními	státní	k2eAgMnPc7d1	státní
sekretáři	sekretář	k1gMnPc7	sekretář
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
náměstci	náměstek	k1gMnPc1	náměstek
ministrů	ministr	k1gMnPc2	ministr
neboli	neboli	k8xC	neboli
mladší	mladý	k2eAgMnPc1d2	mladší
ministři	ministr	k1gMnPc1	ministr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posty	post	k1gInPc1	post
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
paritně	paritně	k6eAd1	paritně
<g/>
,	,	kIx,	,
VVD	VVD	kA	VVD
i	i	k8xC	i
CDA	CDA	kA	CDA
obsadily	obsadit	k5eAaPmAgFnP	obsadit
po	po	k7c6	po
šesti	šest	k4xCc6	šest
ministrech	ministr	k1gMnPc6	ministr
a	a	k8xC	a
čtyřech	čtyři	k4xCgInPc6	čtyři
státních	státní	k2eAgInPc6d1	státní
sekretářích	sekretář	k1gInPc6	sekretář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stranický	stranický	k2eAgMnSc1d1	stranický
lídr	lídr	k1gMnSc1	lídr
VVD	VVD	kA	VVD
Mark	Mark	k1gMnSc1	Mark
Rutte	Rutt	k1gInSc5	Rutt
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
stranický	stranický	k2eAgMnSc1d1	stranický
lídr	lídr	k1gMnSc1	lídr
CDA	CDA	kA	CDA
Maxime	Maxim	k1gMnSc5	Maxim
Verhagen	Verhagen	k1gInSc1	Verhagen
pak	pak	k6eAd1	pak
obsadil	obsadit	k5eAaPmAgInS	obsadit
křeslo	křeslo	k1gNnSc4	křeslo
místopředsedy	místopředseda	k1gMnSc2	místopředseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rutte	Rutt	k1gInSc5	Rutt
cabinet	cabineta	k1gFnPc2	cabineta
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Marka	Marek	k1gMnSc4	Marek
Rutteho	Rutte	k1gMnSc4	Rutte
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
)	)	kIx)	)
Nizozoemská	Nizozoemský	k2eAgFnSc1d1	Nizozoemský
vláda	vláda	k1gFnSc1	vláda
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
stránce	stránka	k1gFnSc6	stránka
</s>
</p>
