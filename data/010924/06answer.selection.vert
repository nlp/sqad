<s>
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
Marka	Marek	k1gMnSc4	Marek
Rutteho	Rutte	k1gMnSc4	Rutte
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
menšinová	menšinový	k2eAgFnSc1d1	menšinová
koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
dvou	dva	k4xCgFnPc2	dva
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
–	–	k?	–
Lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
VVD	VVD	kA	VVD
<g/>
,	,	kIx,	,
31	[number]	k4	31
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
Křesťanskodemokratické	křesťanskodemokratický	k2eAgFnSc2d1	Křesťanskodemokratická
výzvy	výzva	k1gFnSc2	výzva
(	(	kIx(	(
<g/>
CDA	CDA	kA	CDA
<g/>
,	,	kIx,	,
21	[number]	k4	21
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Ruttem	Rutt	k1gMnSc7	Rutt
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
svobodu	svoboda	k1gFnSc4	svoboda
(	(	kIx(	(
<g/>
PVV	PVV	kA	PVV
<g/>
,	,	kIx,	,
24	[number]	k4	24
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
přímo	přímo	k6eAd1	přímo
neúčastní	účastnit	k5eNaImIp3nP	účastnit
<g/>
.	.	kIx.	.
</s>
