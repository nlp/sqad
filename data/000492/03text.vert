<s>
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
též	též	k9	též
Mercurius	Mercurius	k1gInSc1	Mercurius
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
bůh	bůh	k1gMnSc1	bůh
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
zisku	zisk	k1gInSc2	zisk
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc2	cestování
<g/>
,	,	kIx,	,
lsti	lest	k1gFnSc2	lest
<g/>
,	,	kIx,	,
zlodějů	zloděj	k1gMnPc2	zloděj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
poslem	posel	k1gMnSc7	posel
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
protějškem	protějšek	k1gInSc7	protějšek
a	a	k8xC	a
vzorem	vzor	k1gInSc7	vzor
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
bůh	bůh	k1gMnSc1	bůh
Hermés	Hermés	k1gInSc1	Hermés
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gMnSc1	Merkur
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Máie	Mái	k1gFnSc2	Mái
a	a	k8xC	a
Iova	Iovum	k1gNnSc2	Iovum
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
posel	posel	k1gMnSc1	posel
bohů	bůh	k1gMnPc2	bůh
bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
s	s	k7c7	s
okřídlenými	okřídlený	k2eAgFnPc7d1	okřídlená
botami	bota	k1gFnPc7	bota
či	či	k8xC	či
okřídlenou	okřídlený	k2eAgFnSc7d1	okřídlená
přilbou	přilba	k1gFnSc7	přilba
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jej	on	k3xPp3gMnSc4	on
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
kohout	kohout	k1gInSc1	kohout
symbolizující	symbolizující	k2eAgInSc1d1	symbolizující
nový	nový	k2eAgInSc1d1	nový
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
beran	beran	k1gMnSc1	beran
nebo	nebo	k8xC	nebo
kozel	kozel	k1gMnSc1	kozel
symboly	symbol	k1gInPc7	symbol
plodnosti	plodnost	k1gFnSc2	plodnost
či	či	k8xC	či
želva	želva	k1gFnSc1	želva
odkazující	odkazující	k2eAgFnSc1d1	odkazující
na	na	k7c4	na
Merkurův	Merkurův	k2eAgInSc4d1	Merkurův
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
želvího	želví	k2eAgInSc2d1	želví
krunýře	krunýř	k1gInSc2	krunýř
stvořil	stvořit	k5eAaPmAgMnS	stvořit
lyru	lyra	k1gFnSc4	lyra
<g/>
.	.	kIx.	.
</s>
<s>
Alchymisté	alchymista	k1gMnPc1	alchymista
přiřazovali	přiřazovat	k5eAaImAgMnP	přiřazovat
boha	bůh	k1gMnSc4	bůh
Merkura	Merkur	k1gMnSc4	Merkur
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
Merkur	Merkur	k1gInSc1	Merkur
ke	k	k7c3	k
rtuti	rtuť	k1gFnSc3	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Rtuť	rtuť	k1gFnSc1	rtuť
se	se	k3xPyFc4	se
latinsky	latinsky	k6eAd1	latinsky
řekne	říct	k5eAaPmIp3nS	říct
mercurius	mercurius	k1gInSc1	mercurius
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Angelologie	Angelologie	k1gFnSc2	Angelologie
ztotožňován	ztotožňován	k2eAgMnSc1d1	ztotožňován
s	s	k7c7	s
andělem	anděl	k1gMnSc7	anděl
Rafaelem	Rafael	k1gMnSc7	Rafael
<g/>
.	.	kIx.	.
</s>
