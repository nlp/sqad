<s>
Indium	indium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
In	In	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Indium	indium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
snadno	snadno	k6eAd1
tavitelný	tavitelný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
a	a	k8xC
dobře	dobře	k6eAd1
tažný	tažný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
