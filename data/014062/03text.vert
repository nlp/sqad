<s>
Indium	indium	k1gNnSc1
</s>
<s>
Indium	indium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
5	#num#	k4
<g/>
p	p	k?
<g/>
1	#num#	k4
</s>
<s>
115	#num#	k4
</s>
<s>
In	In	kA
</s>
<s>
49	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Indium	indium	k1gNnSc1
<g/>
,	,	kIx,
In	In	k1gFnSc1
<g/>
,	,	kIx,
49	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Indium	indium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
p	p	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
0,25	0,25	k4
ppm	ppm	k?
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-74-6	7440-74-6	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
114,818	114,818	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
167	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
142	#num#	k4
<g/>
±	±	k?
<g/>
5	#num#	k4
pm	pm	k?
</s>
<s>
Van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Waalsův	Waalsův	k2eAgMnSc1d1
poloměr	poloměr	k1gInSc4
</s>
<s>
193	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Kr	Kr	k1gMnSc6
<g/>
]	]	kIx)
4	#num#	k4
<g/>
d	d	k?
<g/>
10	#num#	k4
5	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
5	#num#	k4
<g/>
p	p	k?
<g/>
1	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
I	I	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
III	III	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,78	1,78	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
558,3	558,3	k4
kJ	kJ	k?
<g/>
·	·	k?
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
1820,7	1820,7	k4
kJ	kJ	k?
<g/>
·	·	k?
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
2704	#num#	k4
kJ	kJ	k?
<g/>
·	·	k?
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
tetragonální	tetragonální	k2eAgFnSc1d1
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
7,31	7,31	k4
g	g	kA
<g/>
·	·	k?
<g/>
cm	cm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
1215	#num#	k4
m	m	kA
<g/>
·	·	k?
<g/>
s	s	k7c7
<g/>
−	−	k?
<g/>
1	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
81,8	81,8	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
156,60	156,60	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
429,75	429,75	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
2072	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
345,15	345,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
trojného	trojný	k2eAgInSc2d1
bodu	bod	k1gInSc2
T3	T3	k1gFnSc2
</s>
<s>
156,594	156,594	k4
<g/>
5	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
429,744	429,744	k4
<g/>
5	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Tlak	tlak	k1gInSc1
trojného	trojný	k2eAgInSc2d1
bodu	bod	k1gInSc2
p	p	k?
<g/>
3	#num#	k4
</s>
<s>
~	~	kIx~
1	#num#	k4
kPa	kPa	k?
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
tání	tání	k1gNnSc2
</s>
<s>
3,281	3,281	k4
kJ	kJ	k?
<g/>
·	·	k?
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Skupenské	skupenský	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
varu	var	k1gInSc2
</s>
<s>
231,8	231,8	k4
kJ	kJ	k?
<g/>
·	·	k?
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Molární	molární	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
26,74	26,74	k4
J	J	kA
<g/>
·	·	k?
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
·	·	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
83,7	83,7	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
diamagnetické	diamagnetický	k2eAgNnSc1d1
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
113	#num#	k4
<g/>
In	In	k1gFnSc1
</s>
<s>
4,29	4,29	k4
%	%	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
spontánní	spontánní	k2eAgNnSc1d1
štěpení	štěpení	k1gNnSc1
</s>
<s>
<	<	kIx(
<g/>
24.281	24.281	k4
</s>
<s>
115	#num#	k4
<g/>
In	In	k1gFnSc1
</s>
<s>
95,71	95,71	k4
%	%	kIx~
</s>
<s>
4.41	4.41	k4
<g/>
×	×	k?
<g/>
1014	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
β	β	k?
<g/>
−	−	k?
</s>
<s>
0,495	0,495	k4
</s>
<s>
115	#num#	k4
<g/>
Sn	Sn	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ga	Ga	k?
<g/>
⋏	⋏	k?
</s>
<s>
Kadmium	kadmium	k1gNnSc1
≺	≺	k?
<g/>
In	In	k1gMnSc1
<g/>
≻	≻	k?
Cín	cín	k1gInSc1
</s>
<s>
⋎	⋎	k?
<g/>
Tl	Tl	k1gFnSc1
</s>
<s>
Indium	indium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
In	In	k1gFnSc2
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Indium	indium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
snadno	snadno	k6eAd1
tavitelný	tavitelný	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
měkký	měkký	k2eAgInSc1d1
a	a	k8xC
dobře	dobře	k6eAd1
tažný	tažný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Poměrně	poměrně	k6eAd1
řídce	řídce	k6eAd1
se	se	k3xPyFc4
vyskytující	vyskytující	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
nalézající	nalézající	k2eAgMnSc1d1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
jako	jako	k9
příměs	příměs	k1gFnSc1
v	v	k7c6
rudách	ruda	k1gFnPc6
hliníku	hliník	k1gInSc2
a	a	k8xC
zinku	zinek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
<g/>
,	,	kIx,
běžné	běžný	k2eAgNnSc1d1
mocenství	mocenství	k1gNnSc1
je	být	k5eAaImIp3nS
InIII	InIII	k1gFnSc4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
výjimečné	výjimečný	k2eAgNnSc1d1
a	a	k8xC
nestálé	stálý	k2eNgNnSc1d1
je	být	k5eAaImIp3nS
InI	InI	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
supravodičem	supravodič	k1gInSc7
prvního	první	k4xOgInSc2
typu	typ	k1gInSc2
za	za	k7c2
teplot	teplota	k1gFnPc2
pod	pod	k7c4
3,403	3,403	k4
K.	K.	kA
</s>
<s>
Objevili	objevit	k5eAaPmAgMnP
jej	on	k3xPp3gMnSc4
roku	rok	k1gInSc2
1863	#num#	k4
Ferdinand	Ferdinand	k1gMnSc1
Reich	Reich	k?
a	a	k8xC
Hieronymus	Hieronymus	k1gMnSc1
T.	T.	kA
Richter	Richter	k1gMnSc1
ve	v	k7c6
spektru	spektrum	k1gNnSc6
zbytků	zbytek	k1gInPc2
po	po	k7c6
zpracování	zpracování	k1gNnSc6
zinkové	zinkový	k2eAgFnSc2d1
rudy	ruda	k1gFnSc2
sfaleritu	sfalerit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
pochází	pocházet	k5eAaImIp3nS
i	i	k9
jeho	jeho	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
podle	podle	k7c2
modrého	modré	k1gNnSc2
zbarvení	zbarvení	k1gNnPc2
spektrální	spektrální	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgNnSc1d1
organické	organický	k2eAgNnSc1d1
barvivo	barvivo	k1gNnSc1
indigo	indigo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
výroba	výroba	k1gFnSc1
</s>
<s>
Indium	indium	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
značně	značně	k6eAd1
vzácným	vzácný	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
činí	činit	k5eAaImIp3nS
pouze	pouze	k6eAd1
0,1	0,1	k4
ppm	ppm	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
natolik	natolik	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
nelze	lze	k6eNd1
změřit	změřit	k5eAaPmF
ani	ani	k9
nejcitlivějšími	citlivý	k2eAgFnPc7d3
analytickými	analytický	k2eAgFnPc7d1
technikami	technika	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
india	indium	k1gNnSc2
přibližně	přibližně	k6eAd1
100	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
horninách	hornina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
vždy	vždy	k6eAd1
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
příměs	příměs	k1gFnSc1
v	v	k7c6
rudách	ruda	k1gFnPc6
hliníku	hliník	k1gInSc2
a	a	k8xC
zinku	zinek	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
je	být	k5eAaImIp3nS
také	také	k9
průmyslově	průmyslově	k6eAd1
získáváno	získávat	k5eAaImNgNnS
elektrolýzou	elektrolýza	k1gFnSc7
roztoku	roztok	k1gInSc2
chloridu	chlorid	k1gInSc2
inditého	inditý	k2eAgInSc2d1
InCl	InCl	k1gInSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2009	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
vědci	vědec	k1gMnPc1
z	z	k7c2
technické	technický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
saském	saský	k2eAgInSc6d1
Freibergu	Freiberg	k1gInSc6
objevili	objevit	k5eAaPmAgMnP
v	v	k7c6
Krušných	krušný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
kolem	kolem	k7c2
tisícovky	tisícovka	k1gFnSc2
tun	tuna	k1gFnPc2
india	indium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
omezené	omezený	k2eAgFnSc3d1
dostupnosti	dostupnost	k1gFnSc3
hrozí	hrozit	k5eAaImIp3nS
v	v	k7c6
nejbližších	blízký	k2eAgInPc6d3
letech	let	k1gInPc6
kritický	kritický	k2eAgInSc1d1
nedostatek	nedostatek	k1gInSc1
zdrojů	zdroj	k1gInPc2
prvku	prvek	k1gInSc2
pro	pro	k7c4
technologické	technologický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
První	první	k4xOgNnSc1
významnější	významný	k2eAgNnSc1d2
využití	využití	k1gNnSc1
kovového	kovový	k2eAgNnSc2d1
india	indium	k1gNnSc2
spočívalo	spočívat	k5eAaImAgNnS
v	v	k7c6
ochraně	ochrana	k1gFnSc6
ložisek	ložisko	k1gNnPc2
např.	např.	kA
leteckých	letecký	k2eAgInPc2d1
motorů	motor	k1gInPc2
proti	proti	k7c3
opotřebování	opotřebování	k1gNnSc3
a	a	k8xC
korozi	koroze	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
metodou	metoda	k1gFnSc7
galvanického	galvanický	k2eAgNnSc2d1
pokovování	pokovování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
pohledu	pohled	k1gInSc2
je	být	k5eAaImIp3nS
indium	indium	k1gNnSc1
důležité	důležitý	k2eAgNnSc1d1
i	i	k9
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Krystalická	krystalický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
india	indium	k1gNnSc2
</s>
<s>
Některé	některý	k3yIgFnPc1
slitiny	slitina	k1gFnPc1
mají	mít	k5eAaImIp3nP
nízké	nízký	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
tání	tání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgMnSc4
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
ve	v	k7c6
slitinách	slitina	k1gFnPc6
s	s	k7c7
galliem	gallium	k1gNnSc7
<g/>
,	,	kIx,
kadmiem	kadmium	k1gNnSc7
<g/>
,	,	kIx,
olovem	olovo	k1gNnSc7
a	a	k8xC
cínem	cín	k1gInSc7
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
tavné	tavný	k2eAgFnSc2d1
pojistky	pojistka	k1gFnSc2
pro	pro	k7c4
teploty	teplota	k1gFnPc4
mezi	mezi	k7c7
0	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
°	°	k?
<g/>
C	C	kA
využitelné	využitelný	k2eAgFnPc1d1
např.	např.	kA
pro	pro	k7c4
spouštění	spouštění	k1gNnSc4
samočinných	samočinný	k2eAgInPc2d1
hasicích	hasicí	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slitina	slitina	k1gFnSc1
Galinstan	Galinstan	k1gInSc1
(	(	kIx(
<g/>
Ga	Ga	k1gMnSc1
<g/>
+	+	kIx~
<g/>
In	In	k1gMnSc1
<g/>
+	+	kIx~
<g/>
Sn	Sn	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kapalná	kapalný	k2eAgFnSc1d1
dokonce	dokonce	k9
i	i	k9
při	při	k7c6
teplotách	teplota	k1gFnPc6
do	do	k7c2
−	−	k?
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
Tenká	tenký	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
india	indium	k1gNnSc2
nanesená	nanesený	k2eAgFnSc1d1
na	na	k7c4
rovný	rovný	k2eAgInSc4d1
povrch	povrch	k1gInSc4
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgFnSc4d1
zrcadlovou	zrcadlový	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
odolnější	odolný	k2eAgMnSc1d2
vůči	vůči	k7c3
korozi	koroze	k1gFnSc3
než	než	k8xS
klasická	klasický	k2eAgNnPc4d1
stříbrná	stříbrný	k2eAgNnPc4d1
zrcadla	zrcadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
tato	tento	k3xDgNnPc1
speciální	speciální	k2eAgNnPc1d1
indiová	indiový	k2eAgNnPc1d1
zrcadla	zrcadlo	k1gNnPc1
odrážejí	odrážet	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
části	část	k1gFnPc4
světelného	světelný	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
nejkvalitnějšími	kvalitní	k2eAgNnPc7d3
zrcadly	zrcadlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
energetice	energetika	k1gFnSc6
slouží	sloužit	k5eAaImIp3nS
slitina	slitina	k1gFnSc1
Ag-Cd-In	Ag-Cd-In	k1gInSc1
jako	jako	k8xS,k8xC
materiál	materiál	k1gInSc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
kontrolních	kontrolní	k2eAgFnPc2d1
moderátorových	moderátorův	k2eAgFnPc2d1
tyčí	tyč	k1gFnPc2
pro	pro	k7c4
některé	některý	k3yIgInPc4
typy	typ	k1gInPc4
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
využití	využití	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
indium	indium	k1gNnSc4
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
v	v	k7c6
elektronickém	elektronický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slitiny	slitina	k1gFnSc2
Ge-In	Ge-Ina	k1gFnPc2
jsou	být	k5eAaImIp3nP
důležitým	důležitý	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
při	při	k7c6
výrobě	výroba	k1gFnSc6
mnoha	mnoho	k4c2
polovodičových	polovodičový	k2eAgInPc2d1
prvků	prvek	k1gInPc2
–	–	k?
zejména	zejména	k9
tranzistorů	tranzistor	k1gInPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
termistorů	termistor	k1gInPc2
a	a	k8xC
světlo	světlo	k1gNnSc4
emitujících	emitující	k2eAgFnPc2d1
diod	dioda	k1gFnPc2
(	(	kIx(
<g/>
LED	LED	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tenké	tenký	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
ITO	ITO	kA
(	(	kIx(
<g/>
Indium	indium	k1gNnSc1
Tin	Tina	k1gFnPc2
Oxide	oxid	k1gInSc5
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
elektricky	elektricky	k6eAd1
vodivé	vodivý	k2eAgFnPc1d1
a	a	k8xC
současně	současně	k6eAd1
průhledné	průhledný	k2eAgFnPc1d1
a	a	k8xC
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
klíčovou	klíčový	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
LCD	LCD	kA
displejů	displej	k1gInPc2
a	a	k8xC
dotykových	dotykový	k2eAgFnPc2d1
obrazovek	obrazovka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Indium	indium	k1gNnSc1
slouží	sloužit	k5eAaImIp3nP
také	také	k9
k	k	k7c3
pájení	pájení	k1gNnSc3
polovodičových	polovodičový	k2eAgInPc2d1
spojů	spoj	k1gInPc2
za	za	k7c2
nízkých	nízký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
využití	využití	k1gNnSc1
lze	lze	k6eAd1
najít	najít	k5eAaPmF
na	na	k7c6
povrchu	povrch	k1gInSc6
CD	CD	kA
disků	disk	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
indium	indium	k1gNnSc1
používá	používat	k5eAaImIp3nS
opět	opět	k6eAd1
v	v	k7c6
tenké	tenký	k2eAgFnSc6d1
vrstvě	vrstva	k1gFnSc6
na	na	k7c6
zpevnění	zpevnění	k1gNnSc6
celého	celý	k2eAgInSc2d1
disku	disk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
BURKITBAYEVA	BURKITBAYEVA	kA
<g/>
,	,	kIx,
Bibisara	Bibisara	k1gFnSc1
<g/>
;	;	kIx,
ARGIMBAYEVA	ARGIMBAYEVA	kA
<g/>
,	,	kIx,
Akmaral	Akmaral	k1gFnSc1
<g/>
;	;	kIx,
RAKHYMBAY	RAKHYMBAY	kA
<g/>
,	,	kIx,
Gulmira	Gulmiro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Refining	Refining	k1gInSc1
of	of	k?
Rough	Rough	k1gInSc1
Indium	indium	k1gNnSc1
by	by	k9
Method	Method	k1gInSc1
of	of	k?
Reactionary	Reactionara	k1gFnSc2
Electrolysis	Electrolysis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
MATEC	MATEC	kA
Web	web	k1gInSc1
of	of	k?
Conferences	Conferences	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
96	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2261	#num#	k4
<g/>
-	-	kIx~
<g/>
236	#num#	k4
<g/>
X.	X.	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.105	10.105	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
matecconf	matecconf	k1gInSc1
<g/>
/	/	kIx~
<g/>
20179600005	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Krušné	krušný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
mají	mít	k5eAaImIp3nP
největší	veliký	k2eAgFnPc1d3
zásoby	zásoba	k1gFnPc1
vzácného	vzácný	k2eAgInSc2d1
prvku	prvek	k1gInSc2
india	indium	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
-	-	kIx~
Novinky	novinka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MF	MF	kA
Dnes	dnes	k6eAd1
<g/>
:	:	kIx,
Hory	hora	k1gFnPc1
jsou	být	k5eAaImIp3nP
plné	plný	k2eAgNnSc4d1
india	indium	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
těžbu	těžba	k1gFnSc4
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekolist	Ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Energy	Energ	k1gInPc4
Department	department	k1gInSc1
Releases	Releases	k1gMnSc1
New	New	k1gMnSc1
Critical	Critical	k1gMnSc1
Materials	Materials	k1gInSc4
Strategy	Stratega	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2010	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
indium	indium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Indium	indium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4161516-5	4161516-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5794	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85065672	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85065672	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
