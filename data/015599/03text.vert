<s>
Starověké	starověký	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Parthenón	Parthenón	k1gInSc1
–	–	k?
symbol	symbol	k1gInSc1
starověkého	starověký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
</s>
<s>
Starověké	starověký	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
antické	antický	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
období	období	k1gNnSc4
řeckých	řecký	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
ve	v	k7c6
starověku	starověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
širším	široký	k2eAgNnSc6d2
pojetí	pojetí	k1gNnSc6
zahrnuje	zahrnovat	k5eAaImIp3nS
dobu	doba	k1gFnSc4
od	od	k7c2
příchodu	příchod	k1gInSc2
řeckých	řecký	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
do	do	k7c2
egejské	egejský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
až	až	k9
po	po	k7c4
dobu	doba	k1gFnSc4
nadvlády	nadvláda	k1gFnSc2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
a	a	k8xC
formování	formování	k1gNnSc2
říše	říš	k1gFnSc2
byzantské	byzantský	k2eAgFnSc2d1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
pak	pak	k6eAd1
období	období	k1gNnSc1
samostatné	samostatný	k2eAgFnSc2d1
antické	antický	k2eAgFnSc2d1
řecké	řecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
archaickou	archaický	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
utváření	utváření	k1gNnSc2
klasické	klasický	k2eAgFnSc2d1
řecké	řecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
dobu	doba	k1gFnSc4
Homéra	Homér	k1gMnSc2
a	a	k8xC
kolonizace	kolonizace	k1gFnSc2
Středomoří	středomoří	k1gNnSc2
a	a	k8xC
Černomoří	Černomoří	k1gNnSc2
<g/>
,	,	kIx,
klasické	klasický	k2eAgNnSc4d1
období	období	k1gNnSc4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
vrcholící	vrcholící	k2eAgFnSc1d1
v	v	k7c6
Periklových	Periklův	k2eAgFnPc6d1
Athénách	Athéna	k1gFnPc6
a	a	k8xC
helénské	helénský	k2eAgNnSc4d1
období	období	k1gNnSc4
po	po	k7c6
sjednocení	sjednocení	k1gNnSc6
řeckého	řecký	k2eAgInSc2d1
světa	svět	k1gInSc2
Filipem	Filip	k1gMnSc7
a	a	k8xC
Alexandrem	Alexandr	k1gMnSc7
Makedonským	makedonský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
po	po	k7c6
ztrátě	ztráta	k1gFnSc6
samostatnosti	samostatnost	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
řecká	řecký	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
a	a	k8xC
vzdělanost	vzdělanost	k1gFnSc1
dále	daleko	k6eAd2
rozvíjela	rozvíjet	k5eAaImAgFnS
pod	pod	k7c7
římskou	římský	k2eAgFnSc7d1
nadvládou	nadvláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Antické	antický	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
vytvořilo	vytvořit	k5eAaPmAgNnS
mimořádně	mimořádně	k6eAd1
vyspělou	vyspělý	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
v	v	k7c6
nejrůznějších	různý	k2eAgInPc6d3
oborech	obor	k1gInPc6
<g/>
,	,	kIx,
od	od	k7c2
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
divadla	divadlo	k1gNnSc2
<g/>
,	,	kIx,
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
lékařství	lékařství	k1gNnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
<g/>
,	,	kIx,
matematika	matematika	k1gFnSc1
<g/>
,	,	kIx,
astronomie	astronomie	k1gFnSc1
<g/>
)	)	kIx)
až	až	k9
po	po	k7c4
rétoriku	rétorika	k1gFnSc4
a	a	k8xC
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
kolébkou	kolébka	k1gFnSc7
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
zásadní	zásadní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
utváření	utváření	k1gNnSc4
římské	římský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
jejím	její	k3xOp3gNnSc6
prostřednictví	prostřednictví	k1gNnSc6
i	i	k9
Západní	západní	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecká	řecký	k2eAgFnSc1d1
vzdělanost	vzdělanost	k1gFnSc1
byla	být	k5eAaImAgFnS
uchovávána	uchovávat	k5eAaImNgFnS
a	a	k8xC
pěstována	pěstován	k2eAgFnSc1d1
i	i	k9
v	v	k7c6
Byzanci	Byzanc	k1gFnSc6
a	a	k8xC
arabsko-muslimském	arabsko-muslimský	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
vliv	vliv	k1gInSc4
na	na	k7c4
formování	formování	k1gNnSc4
křesťanství	křesťanství	k1gNnSc2
(	(	kIx(
<g/>
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
tomismus	tomismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
řecké	řecký	k2eAgFnSc3d1
kultuře	kultura	k1gFnSc3
se	se	k3xPyFc4
vracela	vracet	k5eAaImAgFnS
i	i	k9
renesance	renesance	k1gFnSc1
a	a	k8xC
klasicismus	klasicismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Starověký	starověký	k2eAgInSc1d1
řecký	řecký	k2eAgInSc1d1
svět	svět	k1gInSc1
byl	být	k5eAaImAgInS
soustředěn	soustředit	k5eAaPmNgInS
do	do	k7c2
jednotlivých	jednotlivý	k2eAgInPc2d1
městských	městský	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
polis	polis	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
neomezovaly	omezovat	k5eNaImAgFnP
jen	jen	k9
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
díky	díky	k7c3
námořní	námořní	k2eAgFnSc3d1
kolonizaci	kolonizace	k1gFnSc3
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
i	i	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
:	:	kIx,
egejské	egejský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
Kypr	Kypr	k1gInSc1
<g/>
,	,	kIx,
pobřeží	pobřeží	k1gNnSc1
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
(	(	kIx(
<g/>
především	především	k9
Iónie	Iónie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sicílie	Sicílie	k1gFnSc2
a	a	k8xC
jižní	jižní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
(	(	kIx(
<g/>
Magna	Magen	k2eAgFnSc1d1
Graecia	Graecia	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Černého	Černého	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
Ilýrie	Ilýrie	k1gFnSc2
<g/>
,	,	kIx,
Thrákie	Thrákie	k1gFnSc2
<g/>
,	,	kIx,
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
Kyrenaiky	Kyrenaika	k1gFnSc2
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc2d1
Galie	Galie	k1gFnSc2
a	a	k8xC
severovýchodní	severovýchodní	k2eAgFnSc2d1
části	část	k1gFnSc2
Iberského	iberský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Egejská	egejský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Egejská	egejský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
a	a	k8xC
Mykénská	mykénský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Lví	lví	k2eAgFnSc1d1
brána	brána	k1gFnSc1
v	v	k7c6
Mykénách	Mykény	k1gFnPc6
<g/>
,	,	kIx,
centru	centrum	k1gNnSc6
nejstarší	starý	k2eAgFnSc2d3
řecké	řecký	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3
dějiny	dějiny	k1gFnPc1
Řecka	Řecko	k1gNnSc2
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
tzv.	tzv.	kA
egejské	egejský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
egejské	egejský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
bronzové	bronzový	k2eAgFnPc1d1
vyvinuly	vyvinout	k5eAaPmAgFnP
předřecké	předřecký	k2eAgFnPc1d1
vyspělé	vyspělý	k2eAgFnPc1d1
kultury	kultura	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c6
ostrovech	ostrov	k1gInPc6
v	v	k7c6
centrální	centrální	k2eAgFnSc6d1
části	část	k1gFnSc6
kykladská	kykladský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
na	na	k7c6
Krétě	Kréta	k1gFnSc6
mínojská	mínojský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ovládla	ovládnout	k5eAaPmAgFnS
egejskou	egejský	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
mykénská	mykénský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
1650	#num#	k4
<g/>
–	–	k?
<g/>
1100	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Mykénách	Mykény	k1gFnPc6
na	na	k7c6
pevninském	pevninský	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
rozluštění	rozluštění	k1gNnSc3
lineárního	lineární	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
B	B	kA
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
Řeky	Řek	k1gMnPc4
a	a	k8xC
bývá	bývat	k5eAaImIp3nS
vznik	vznik	k1gInSc4
této	tento	k3xDgFnSc2
kultury	kultura	k1gFnSc2
spojován	spojovat	k5eAaImNgMnS
s	s	k7c7
vpádem	vpád	k1gInSc7
řeckého	řecký	k2eAgInSc2d1
kmene	kmen	k1gInSc2
Achájů	Acháj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
doba	doba	k1gFnSc1
je	být	k5eAaImIp3nS
období	období	k1gNnSc4
řeckých	řecký	k2eAgInPc2d1
hérojů	héroj	k1gInPc2
a	a	k8xC
mýtů	mýtus	k1gInPc2
jako	jako	k9
byla	být	k5eAaImAgFnS
trojská	trojský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
známé	známý	k2eAgNnSc1d1
z	z	k7c2
pozdějších	pozdní	k2eAgInPc2d2
Homérových	Homérův	k2eAgInPc2d1
eposů	epos	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kréta	Kréta	k1gFnSc1
</s>
<s>
Na	na	k7c6
Krétě	Kréta	k1gFnSc6
jako	jako	k8xC,k8xS
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
civilizace	civilizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kréťané	Kréťan	k1gMnPc1
znali	znát	k5eAaImAgMnP
tři	tři	k4xCgInPc4
druhy	druh	k1gInPc4
písma	písmo	k1gNnSc2
–	–	k?
hieroglyfické	hieroglyfický	k2eAgNnSc4d1
<g/>
,	,	kIx,
lineární	lineární	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
A	A	kA
(	(	kIx(
<g/>
dosud	dosud	k6eAd1
nerozluštěno	rozluštěn	k2eNgNnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
lineární	lineární	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
B	B	kA
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
se	se	k3xPyFc4
za	za	k7c2
velké	velký	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
mezinárodních	mezinárodní	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
podařilo	podařit	k5eAaPmAgNnS
rozluštit	rozluštit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
na	na	k7c6
Krétě	Kréta	k1gFnSc6
stavět	stavět	k5eAaImF
první	první	k4xOgInSc4
města	město	k1gNnSc2
s	s	k7c7
paláci	palác	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
začalo	začít	k5eAaPmAgNnS
první	první	k4xOgNnSc1
historické	historický	k2eAgNnSc1d1
období	období	k1gNnSc1
nazývané	nazývaný	k2eAgNnSc1d1
„	„	k?
<g/>
období	období	k1gNnSc4
prvních	první	k4xOgInPc2
paláců	palác	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
první	první	k4xOgInPc4
paláce	palác	k1gInPc4
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
Knossos	Knossos	k1gInSc1
<g/>
,	,	kIx,
Faistos	Faistos	k1gInSc1
a	a	k8xC
Mallia	Mallia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1700	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
tyto	tento	k3xDgInPc4
paláce	palác	k1gInPc4
zničilo	zničit	k5eAaPmAgNnS
pravděpodobně	pravděpodobně	k6eAd1
obrovské	obrovský	k2eAgNnSc1d1
zemětřesení	zemětřesení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgFnSc1d1
éra	éra	k1gFnSc1
krétské	krétský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
„	„	k?
<g/>
období	období	k1gNnSc4
druhých	druhý	k4xOgInPc2
paláců	palác	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
zaznamenaly	zaznamenat	k5eAaPmAgInP
mínojské	mínojský	k2eAgInPc1d1
paláce	palác	k1gInPc1
největší	veliký	k2eAgInSc1d3
rozkvět	rozkvět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Temné	temný	k2eAgNnSc1d1
období	období	k1gNnSc1
(	(	kIx(
<g/>
1200	#num#	k4
<g/>
–	–	k?
<g/>
800	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Temné	temný	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
periodu	perioda	k1gFnSc4
Řecka	Řecko	k1gNnSc2
po	po	k7c6
pádu	pád	k1gInSc6
mykénské	mykénský	k2eAgFnSc2d1
a	a	k8xC
dalších	další	k2eAgFnPc2d1
civilizací	civilizace	k1gFnPc2
pod	pod	k7c7
náporem	nápor	k1gInSc7
mořských	mořský	k2eAgInPc2d1
národů	národ	k1gInPc2
se	se	k3xPyFc4
pro	pro	k7c4
nedostatek	nedostatek	k1gInSc4
pramenů	pramen	k1gInPc2
vžil	vžít	k5eAaPmAgMnS
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
době	doba	k1gFnSc6
název	název	k1gInSc4
temné	temný	k2eAgNnSc4d1
období	období	k1gNnSc4
či	či	k8xC
temná	temný	k2eAgNnPc4d1
staletí	staletí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Archaická	archaický	k2eAgFnSc1d1
doba	doba	k1gFnSc1
(	(	kIx(
<g/>
800	#num#	k4
<g/>
–	–	k?
<g/>
500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
se	se	k3xPyFc4
rodilo	rodit	k5eAaImAgNnS
v	v	k7c6
archaické	archaický	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
Bazilika	bazilika	k1gFnSc1
v	v	k7c6
Paestu	Paest	k1gInSc6
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
</s>
<s>
Archaická	archaický	k2eAgFnSc1d1
doba	doba	k1gFnSc1
následovala	následovat	k5eAaImAgFnS
po	po	k7c6
skončení	skončení	k1gNnSc6
temného	temný	k2eAgNnSc2d1
období	období	k1gNnSc2
někdy	někdy	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
9	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
v	v	k7c6
mnoha	mnoho	k4c6
směrech	směr	k1gInPc6
položila	položit	k5eAaPmAgFnS
základy	základ	k1gInPc7
pozdějšího	pozdní	k2eAgNnSc2d2
klasického	klasický	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
události	událost	k1gFnSc2
<g/>
,	,	kIx,
symbolizující	symbolizující	k2eAgInSc4d1
počátek	počátek	k1gInSc4
této	tento	k3xDgFnSc2
epochy	epocha	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pokládáno	pokládán	k2eAgNnSc4d1
konání	konání	k1gNnSc4
prvních	první	k4xOgFnPc2
antických	antický	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
podle	podle	k7c2
tradice	tradice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
776	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
rovněž	rovněž	k9
vznik	vznik	k1gInSc4
homérských	homérský	k2eAgInPc2d1
eposů	epos	k1gInPc2
Ilias	Ilias	k1gFnSc1
a	a	k8xC
Odyssea	Odyssea	k1gFnSc1
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Rozmach	rozmach	k1gInSc1
městských	městský	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
V	v	k7c6
Řecku	Řecko	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
zformovala	zformovat	k5eAaPmAgFnS
tradiční	tradiční	k2eAgFnSc1d1
forma	forma	k1gFnSc1
organizace	organizace	k1gFnSc2
starověké	starověký	k2eAgFnSc2d1
řecké	řecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
–	–	k?
polis	polis	k1gInSc1
(	(	kIx(
<g/>
městský	městský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
byla	být	k5eAaImAgFnS
polis	polis	k1gFnSc1
neboli	neboli	k8xC
obec	obec	k1gFnSc1
pravděpodobně	pravděpodobně	k6eAd1
zemědělským	zemědělský	k2eAgNnSc7d1
sídlištěm	sídliště	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
vedl	vést	k5eAaImAgInS
rozvoj	rozvoj	k1gInSc4
obchodu	obchod	k1gInSc2
a	a	k8xC
řemesel	řemeslo	k1gNnPc2
ke	k	k7c3
vzniku	vznik	k1gInSc3
různých	různý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
polis	polis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
právního	právní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
tvořili	tvořit	k5eAaImAgMnP
členové	člen	k1gMnPc1
obce	obec	k1gFnSc2
pospolitost	pospolitost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
své	svůj	k3xOyFgFnSc3
vnitřní	vnitřní	k2eAgFnSc3d1
záležitosti	záležitost	k1gFnSc3
upravovala	upravovat	k5eAaImAgFnS
podle	podle	k7c2
vlastních	vlastní	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hlavním	hlavní	k2eAgInSc7d1
principem	princip	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
byla	být	k5eAaImAgFnS
budována	budován	k2eAgFnSc1d1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
soukromé	soukromý	k2eAgNnSc1d1
vlastnictví	vlastnictví	k1gNnSc1
pozemků	pozemek	k1gInPc2
a	a	k8xC
půdy	půda	k1gFnSc2
jejími	její	k3xOp3gMnPc7
členy	člen	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
představovali	představovat	k5eAaImAgMnP
původní	původní	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
polis	polis	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	on	k3xPp3gMnPc4
odlišovalo	odlišovat	k5eAaImAgNnS
od	od	k7c2
usedlých	usedlý	k2eAgMnPc2d1
cizinců	cizinec	k1gMnPc2
(	(	kIx(
<g/>
metoikos	metoikos	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
otroků	otrok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polis	Polis	k1gFnSc1
nezahrnovala	zahrnovat	k5eNaImAgFnS
pouze	pouze	k6eAd1
samotné	samotný	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
i	i	k9
zázemí	zázemí	k1gNnSc1
města	město	k1gNnSc2
(	(	kIx(
<g/>
chórá	chórat	k5eAaPmIp3nS
<g/>
)	)	kIx)
a	a	k8xC
obyvatelé	obyvatel	k1gMnPc1
tohoto	tento	k3xDgNnSc2
zázemí	zázemí	k1gNnSc2
byli	být	k5eAaImAgMnP
taktéž	taktéž	k?
rovnoprávnými	rovnoprávný	k2eAgMnPc7d1
členy	člen	k1gMnPc7
polis	polis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
polis	polis	k1gFnPc1
s	s	k7c7
vlastním	vlastní	k2eAgNnSc7d1
rozlehlým	rozlehlý	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Athény	Athéna	k1gFnPc1
nebo	nebo	k8xC
Sparta	Sparta	k1gFnSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
spíše	spíše	k9
výjimkami	výjimka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
každá	každý	k3xTgFnSc1
polis	polis	k1gFnSc1
měla	mít	k5eAaImAgFnS
hrad	hrad	k1gInSc4
(	(	kIx(
<g/>
akropolis	akropolis	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
agoru	agora	k1gFnSc4
–	–	k?
náměstí	náměstí	k1gNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
sloužilo	sloužit	k5eAaImAgNnS
jako	jako	k9
hospodářské	hospodářský	k2eAgNnSc1d1
a	a	k8xC
politické	politický	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
společenské	společenský	k2eAgFnSc2d1
pyramidy	pyramida	k1gFnSc2
stál	stát	k5eAaImAgMnS
nejprve	nejprve	k6eAd1
král	král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královská	královský	k2eAgFnSc1d1
moc	moc	k1gFnSc1
uvnitř	uvnitř	k7c2
obce	obec	k1gFnSc2
nespočívala	spočívat	k5eNaImAgFnS
ani	ani	k8xC
tak	tak	k6eAd1
na	na	k7c6
šlechtickém	šlechtický	k2eAgInSc6d1
původu	původ	k1gInSc6
těchto	tento	k3xDgMnPc2
králů	král	k1gMnPc2
jako	jako	k8xS,k8xC
spíše	spíše	k9
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
bohatství	bohatství	k1gNnSc6
a	a	k8xC
společenské	společenský	k2eAgFnSc3d1
autoritě	autorita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvinutější	rozvinutý	k2eAgFnPc4d2
polis	polis	k1gFnPc4
se	se	k3xPyFc4
vyznačovaly	vyznačovat	k5eAaImAgFnP
silnou	silný	k2eAgFnSc7d1
sociální	sociální	k2eAgFnSc7d1
diferenciací	diferenciace	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
společnost	společnost	k1gFnSc1
byla	být	k5eAaImAgFnS
tvořena	tvořit	k5eAaImNgFnS
následujícími	následující	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
svobodných	svobodný	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
velkostatkáři	velkostatkář	k1gMnPc1
<g/>
,	,	kIx,
rolníci	rolník	k1gMnPc1
<g/>
,	,	kIx,
řemeslníci	řemeslník	k1gMnPc1
<g/>
,	,	kIx,
obchodníci	obchodník	k1gMnPc1
a	a	k8xC
nádeníci	nádeník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
polis	polis	k1gFnSc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
nebyla	být	k5eNaImAgFnS
příliš	příliš	k6eAd1
malá	malý	k2eAgFnSc1d1
<g/>
,	,	kIx,
si	se	k3xPyFc3
střežila	střežit	k5eAaImAgFnS
svoji	svůj	k3xOyFgFnSc4
autonomii	autonomie	k1gFnSc4
a	a	k8xC
nebyla	být	k5eNaImAgFnS
ochotná	ochotný	k2eAgFnSc1d1
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
dobrovolně	dobrovolně	k6eAd1
vzdát	vzdát	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
byla	být	k5eAaImAgFnS
proto	proto	k8xC
ve	v	k7c6
starověkém	starověký	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
považována	považován	k2eAgFnSc1d1
za	za	k7c4
normální	normální	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
–	–	k?
rozšíření	rozšíření	k1gNnSc2
řecké	řecký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Řecká	řecký	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1
a	a	k8xC
fénická	fénický	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
</s>
<s>
Přelidnění	přelidnění	k1gNnSc1
a	a	k8xC
výsledný	výsledný	k2eAgInSc1d1
nedostatek	nedostatek	k1gInSc1
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
potřeba	potřeba	k6eAd1
chránit	chránit	k5eAaImF
obchodní	obchodní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
ve	v	k7c6
vzdálených	vzdálený	k2eAgInPc6d1
krajích	kraj	k1gInPc6
byly	být	k5eAaImAgFnP
hlavními	hlavní	k2eAgFnPc7d1
příčinami	příčina	k1gFnPc7
řecké	řecký	k2eAgFnSc2d1
kolonizace	kolonizace	k1gFnSc2
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
až	až	k9
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Již	již	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
2	#num#	k4
<g/>
.	.	kIx.
tisíciletí	tisíciletí	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
přesídlili	přesídlit	k5eAaPmAgMnP
první	první	k4xOgMnPc1
Řekové	Řek	k1gMnPc1
během	během	k7c2
„	„	k?
<g/>
iónské	iónský	k2eAgFnSc2d1
kolonizace	kolonizace	k1gFnSc2
<g/>
“	“	k?
do	do	k7c2
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
–	–	k?
například	například	k6eAd1
do	do	k7c2
Mílétu	Mílét	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
údajně	údajně	k6eAd1
osídlen	osídlit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1053	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
(	(	kIx(
<g/>
Řekové	Řek	k1gMnPc1
zde	zde	k6eAd1
ale	ale	k8xC
zřejmě	zřejmě	k6eAd1
žili	žít	k5eAaImAgMnP
již	již	k6eAd1
předtím	předtím	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expanze	expanze	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
750	#num#	k4
až	až	k9
550	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k9
„	„	k?
<g/>
velká	velký	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
vedla	vést	k5eAaImAgFnS
k	k	k7c3
založení	založení	k1gNnSc3
řeckých	řecký	k2eAgFnPc2d1
poleis	poleis	k1gFnPc2
v	v	k7c6
odlehlých	odlehlý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
Černého	Černého	k2eAgNnSc2d1
moře	moře	k1gNnSc2
(	(	kIx(
<g/>
Pontus	Pontus	k1gInSc1
Euxinus	Euxinus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
především	především	k9
kolonie	kolonie	k1gFnPc1
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
a	a	k8xC
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
–	–	k?
například	například	k6eAd1
Naxos	Naxos	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
735	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
Syrákúsy	Syrákúsa	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
730	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc4
byla	být	k5eAaImAgFnS
proto	proto	k8xC
později	pozdě	k6eAd2
nazývána	nazývat	k5eAaImNgFnS
Magna	Magen	k2eAgFnSc1d1
Graecia	Graecia	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Velké	velký	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmu	pojmout	k5eAaPmIp1nS
kolonizace	kolonizace	k1gFnSc1
však	však	k9
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
nelze	lze	k6eNd1
přikládat	přikládat	k5eAaImF
moderní	moderní	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
nově	nově	k6eAd1
založené	založený	k2eAgFnPc1d1
obce	obec	k1gFnPc1
byly	být	k5eAaImAgFnP
zcela	zcela	k6eAd1
nezávislé	závislý	k2eNgFnPc1d1
na	na	k7c6
svých	svůj	k3xOyFgNnPc6
mateřských	mateřský	k2eAgNnPc6d1
městech	město	k1gNnPc6
a	a	k8xC
usidlování	usidlování	k1gNnSc1
se	se	k3xPyFc4
dělo	dít	k5eAaBmAgNnS,k5eAaImAgNnS
jenom	jenom	k9
v	v	k7c6
těch	ten	k3xDgNnPc6
územích	území	k1gNnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
očekávat	očekávat	k5eAaImF
vážný	vážný	k2eAgInSc4d1
odpor	odpor	k1gInSc4
domorodých	domorodý	k2eAgMnPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolonie	kolonie	k1gFnSc2
obvykle	obvykle	k6eAd1
přebíraly	přebírat	k5eAaImAgInP
náboženské	náboženský	k2eAgInPc1d1
kulty	kult	k1gInPc1
a	a	k8xC
ústavu	ústav	k1gInSc2
mateřského	mateřský	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vliv	vliv	k1gInSc1
řecké	řecký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
se	se	k3xPyFc4
prostřednictvím	prostřednictvím	k7c2
kolonizace	kolonizace	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgInS
z	z	k7c2
egejské	egejský	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
do	do	k7c2
celého	celý	k2eAgNnSc2d1
Středozemí	středozemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
700	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zesílily	zesílit	k5eAaPmAgInP
orientální	orientální	k2eAgInPc1d1
vlivy	vliv	k1gInPc1
působící	působící	k2eAgInPc1d1
na	na	k7c4
řeckou	řecký	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
důležitou	důležitý	k2eAgFnSc4d1
zprostředkující	zprostředkující	k2eAgFnSc4d1
roli	role	k1gFnSc4
sehrála	sehrát	k5eAaPmAgFnS
nejprve	nejprve	k6eAd1
města	město	k1gNnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Euboia	Euboium	k1gNnSc2
a	a	k8xC
později	pozdě	k6eAd2
hlavně	hlavně	k9
Korint	Korint	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řekové	Řek	k1gMnPc1
již	již	k6eAd1
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
převzali	převzít	k5eAaPmAgMnP
od	od	k7c2
Féničanů	Féničan	k1gMnPc2
abecedu	abeceda	k1gFnSc4
a	a	k8xC
uzpůsobili	uzpůsobit	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
podle	podle	k7c2
potřeb	potřeba	k1gFnPc2
svého	svůj	k3xOyFgInSc2
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
dochovaný	dochovaný	k2eAgInSc1d1
řecký	řecký	k2eAgInSc1d1
nápis	nápis	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
poloviny	polovina	k1gFnSc2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
V	v	k7c6
důsledku	důsledek	k1gInSc6
prudkého	prudký	k2eAgInSc2d1
hospodářského	hospodářský	k2eAgInSc2d1
a	a	k8xC
politického	politický	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
zažila	zažít	k5eAaPmAgFnS
řecká	řecký	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
v	v	k7c6
archaické	archaický	k2eAgFnSc6d1
době	doba	k1gFnSc6
své	svůj	k3xOyFgNnSc4
první	první	k4xOgNnSc4
období	období	k1gNnSc4
rozkvětu	rozkvět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgFnPc4
znalosti	znalost	k1gFnPc4
a	a	k8xC
objevy	objev	k1gInPc4
sdělovali	sdělovat	k5eAaImAgMnP
tehdy	tehdy	k6eAd1
světu	svět	k1gInSc3
astronom	astronom	k1gMnSc1
Thalés	Thalés	k1gInSc1
z	z	k7c2
Mílétu	Mílét	k1gInSc2
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
Pýthagorás	Pýthagorás	k1gInSc1
nebo	nebo	k8xC
filosof	filosof	k1gMnSc1
Anaximandros	Anaximandrosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vědomí	vědomí	k1gNnSc1
společného	společný	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
vzájemnosti	vzájemnost	k1gFnSc2
mezi	mezi	k7c7
Helény	Helén	k1gMnPc7
bylo	být	k5eAaImAgNnS
až	až	k6eAd1
do	do	k7c2
doby	doba	k1gFnSc2
řecko	řecko	k6eAd1
<g/>
‑	‑	k?
<g/>
perských	perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
poměrně	poměrně	k6eAd1
slabé	slabý	k2eAgNnSc1d1
a	a	k8xC
projevovalo	projevovat	k5eAaImAgNnS
se	se	k3xPyFc4
hlavně	hlavně	k9
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
sousedním	sousední	k2eAgFnPc3d1
<g/>
,	,	kIx,
neřeckým	řecký	k2eNgFnPc3d1
národům	národ	k1gInPc3
(	(	kIx(
<g/>
barbaři	barbar	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
především	především	k9
ale	ale	k8xC
také	také	k9
společným	společný	k2eAgNnSc7d1
řeckým	řecký	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
(	(	kIx(
<g/>
řecká	řecký	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starověký	starověký	k2eAgInSc1d1
řecký	řecký	k2eAgInSc1d1
svět	svět	k1gInSc1
byl	být	k5eAaImAgInS
hluboce	hluboko	k6eAd1
zbožný	zbožný	k2eAgInSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeho	jeho	k3xOp3gNnSc1
náboženství	náboženství	k1gNnSc1
se	se	k3xPyFc4
zakládalo	zakládat	k5eAaImAgNnS
na	na	k7c6
mýtech	mýtus	k1gInPc6
a	a	k8xC
hrdinských	hrdinský	k2eAgInPc6d1
příbězích	příběh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakékoli	jakýkoli	k3yIgNnSc1
veřejné	veřejný	k2eAgNnSc1d1
nebo	nebo	k8xC
soukromé	soukromý	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
bylo	být	k5eAaImAgNnS
doprovázeno	doprovázet	k5eAaImNgNnS
vzýváním	vzývání	k1gNnSc7
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějším	důležitý	k2eAgNnSc7d3
všeřeckým	všeřecký	k2eAgNnSc7d1
posvátným	posvátný	k2eAgNnSc7d1
místem	místo	k1gNnSc7
bylo	být	k5eAaImAgNnS
orákulum	orákulum	k1gNnSc1
v	v	k7c6
Delfách	Delfy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
různými	různý	k2eAgInPc7d1
řeckými	řecký	k2eAgInPc7d1
náboženskými	náboženský	k2eAgInPc7d1
kulty	kult	k1gInPc7
vznikla	vzniknout	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
panhelénských	panhelénský	k2eAgFnPc2d1
slavností	slavnost	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byly	být	k5eAaImAgFnP
například	například	k6eAd1
isthmické	isthmický	k2eAgFnPc1d1
<g/>
,	,	kIx,
pýthijské	pýthijský	k2eAgFnPc1d1
<g/>
,	,	kIx,
nemejské	mejský	k2eNgFnPc1d1
nebo	nebo	k8xC
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgInPc1d3
byly	být	k5eAaImAgInP
právě	právě	k9
posledně	posledně	k6eAd1
zmiňované	zmiňovaný	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
měly	mít	k5eAaImAgFnP
poprvé	poprvé	k6eAd1
konat	konat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
776	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Proměna	proměna	k1gFnSc1
státního	státní	k2eAgNnSc2d1
založení	založení	k1gNnSc2
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
</s>
<s>
Svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
při	při	k7c6
kolonizaci	kolonizace	k1gFnSc6
sehrály	sehrát	k5eAaPmAgInP
také	také	k9
boje	boj	k1gInPc1
uvnitř	uvnitř	k7c2
poleis	poleis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aristokracie	aristokracie	k1gFnSc1
získala	získat	k5eAaPmAgFnS
postupem	postup	k1gInSc7
času	čas	k1gInSc2
na	na	k7c6
vlivu	vliv	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
odrazilo	odrazit	k5eAaPmAgNnS
v	v	k7c6
oslabování	oslabování	k1gNnSc6
dosavadní	dosavadní	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Království	království	k1gNnSc1
bylo	být	k5eAaImAgNnS
mezi	mezi	k7c7
léty	léto	k1gNnPc7
800	#num#	k4
až	až	k8xS
650	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pozvolna	pozvolna	k6eAd1
nahrazeno	nahradit	k5eAaPmNgNnS
vládou	vláda	k1gFnSc7
oligarchie	oligarchie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
moc	moc	k6eAd1
spočívala	spočívat	k5eAaImAgFnS
v	v	k7c6
rukou	ruka	k1gFnPc6
malé	malý	k2eAgFnSc2d1
skupinky	skupinka	k1gFnSc2
aristokratů	aristokrat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
městských	městský	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
na	na	k7c6
vládě	vláda	k1gFnSc6
podílelo	podílet	k5eAaImAgNnS
i	i	k9
obyčejné	obyčejný	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
demokracie	demokracie	k1gFnSc1
jako	jako	k8xC,k8xS
například	například	k6eAd1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
se	se	k3xPyFc4
vyvinula	vyvinout	k5eAaPmAgFnS
teprve	teprve	k6eAd1
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
650	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byla	být	k5eAaImAgFnS
oligarchie	oligarchie	k1gFnPc4
v	v	k7c6
některých	některý	k3yIgFnPc6
obcích	obec	k1gFnPc6
svržena	svrhnout	k5eAaPmNgFnS
tyrany	tyran	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nabyli	nabýt	k5eAaPmAgMnP
moc	moc	k6eAd1
zcela	zcela	k6eAd1
neoprávněně	oprávněně	k6eNd1
a	a	k8xC
jejichž	jejichž	k3xOyRp3gFnSc1
vláda	vláda	k1gFnSc1
tudíž	tudíž	k8xC
postrádala	postrádat	k5eAaImAgFnS
legitimitu	legitimita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyranem	tyran	k1gMnSc7
se	se	k3xPyFc4
stával	stávat	k5eAaImAgInS
zpravidla	zpravidla	k6eAd1
mocný	mocný	k2eAgMnSc1d1
aristokrat	aristokrat	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
si	se	k3xPyFc3
svoji	svůj	k3xOyFgFnSc4
moc	moc	k6eAd1
zajistil	zajistit	k5eAaPmAgMnS
vojenskou	vojenský	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyrani	tyran	k1gMnPc1
se	se	k3xPyFc4
často	často	k6eAd1
opírali	opírat	k5eAaImAgMnP
o	o	k7c4
podporu	podpora	k1gFnSc4
neprivilegovaných	privilegovaný	k2eNgFnPc2d1
vrstev	vrstva	k1gFnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byl	být	k5eAaImAgMnS
zlomen	zlomen	k2eAgInSc4d1
dřívější	dřívější	k2eAgInSc4d1
vliv	vliv	k1gInSc4
aristokracie	aristokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
tyranů	tyran	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Periandros	Periandrosa	k1gFnPc2
v	v	k7c4
Korintu	Korinta	k1gFnSc4
nebo	nebo	k8xC
Gelón	Gelón	k1gInSc4
či	či	k8xC
Agathoklés	Agathoklés	k1gInSc4
v	v	k7c6
Syrákúsách	Syrákúsa	k1gFnPc6
<g/>
,	,	kIx,
náleželo	náležet	k5eAaImAgNnS
k	k	k7c3
výjimečným	výjimečný	k2eAgFnPc3d1
vůdčím	vůdčí	k2eAgFnPc3d1
osobnostem	osobnost	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Nejvýznamnější	významný	k2eAgInPc1d3
řecké	řecký	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
vzestup	vzestup	k1gInSc1
Sparty	Sparta	k1gFnSc2
a	a	k8xC
Athén	Athéna	k1gFnPc2
</s>
<s>
Sparta	Sparta	k1gFnSc1
–	–	k?
nejmocnější	mocný	k2eAgInSc1d3
stát	stát	k1gInSc1
Řecka	Řecko	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Sparta	Sparta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Peloponésu	Peloponés	k1gInSc6
byl	být	k5eAaImAgInS
nejprve	nejprve	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
vůdčím	vůdčí	k2eAgInSc7d1
státem	stát	k1gInSc7
Argos	Argos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
soupeř	soupeř	k1gMnSc1
–	–	k?
Sparta	Sparta	k1gFnSc1
–	–	k?
se	se	k3xPyFc4
však	však	k9
po	po	k7c4
dobytí	dobytí	k1gNnSc4
Messénie	Messénie	k1gFnSc2
ve	v	k7c6
dvou	dva	k4xCgFnPc6
rozhořčených	rozhořčený	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
v	v	k7c6
průběhu	průběh	k1gInSc6
8	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
stala	stát	k5eAaPmAgFnS
vedoucí	vedoucí	k1gFnSc1
mocností	mocnost	k1gFnPc2
poloostrova	poloostrov	k1gInSc2
a	a	k8xC
celého	celý	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
Lakedaimón	Lakedaimón	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
aristokratickým	aristokratický	k2eAgInSc7d1
státem	stát	k1gInSc7
na	na	k7c6
jihu	jih	k1gInSc6
Peloponéského	peloponéský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
dvěma	dva	k4xCgInPc7
králi	král	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
spartského	spartský	k2eAgInSc2d1
státu	stát	k1gInSc2
byla	být	k5eAaImAgFnS
ústava	ústava	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
polomytickým	polomytický	k2eAgInSc7d1
Lykúrgem	Lykúrg	k1gInSc7
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
spartská	spartský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
řídila	řídit	k5eAaImAgFnS
přísnou	přísný	k2eAgFnSc7d1
vojenskou	vojenský	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ze	z	k7c2
Sparty	Sparta	k1gFnSc2
učinilo	učinit	k5eAaImAgNnS,k5eAaPmAgNnS
největší	veliký	k2eAgFnSc4d3
vojenskou	vojenský	k2eAgFnSc4d1
moc	moc	k1gFnSc4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoje	své	k1gNnSc4
postavení	postavení	k1gNnSc2
vedoucí	vedoucí	k2eAgFnSc2d1
mocnosti	mocnost	k1gFnSc2
v	v	k7c6
Řecku	Řecko	k1gNnSc6
stvrdila	stvrdit	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
někdy	někdy	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
550	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vytvořením	vytvoření	k1gNnSc7
peloponéského	peloponéský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Sparty	Sparta	k1gFnSc2
sdružoval	sdružovat	k5eAaImAgInS
většinu	většina	k1gFnSc4
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
pevninského	pevninský	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Spartské	spartský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
tvořeno	tvořit	k5eAaImNgNnS
těžkooděnými	těžkooděný	k2eAgMnPc7d1
pěšáky	pěšák	k1gMnPc7
–	–	k?
hoplíty	hoplíto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
členila	členit	k5eAaImAgFnS
do	do	k7c2
tří	tři	k4xCgFnPc2
vrstev	vrstva	k1gFnPc2
<g/>
:	:	kIx,
postavení	postavení	k1gNnSc1
plnoprávných	plnoprávný	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
zaujímali	zaujímat	k5eAaImAgMnP
spartiaté	spartiat	k1gMnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc4
počet	počet	k1gInSc1
byl	být	k5eAaImAgInS
ale	ale	k9
velmi	velmi	k6eAd1
malý	malý	k2eAgInSc1d1
(	(	kIx(
<g/>
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
době	doba	k1gFnSc6
snad	snad	k9
kolem	kolem	k7c2
devíti	devět	k4xCc2
až	až	k9
desíti	deset	k4xCc2
tisíc	tisíc	k4xCgInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vlivem	vliv	k1gInSc7
neustálých	neustálý	k2eAgFnPc2d1
válek	válka	k1gFnPc2
trvale	trvale	k6eAd1
klesal	klesat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svobodní	svobodný	k2eAgMnPc1d1
občané	občan	k1gMnPc1
bez	bez	k7c2
politických	politický	k2eAgNnPc2d1
práv	právo	k1gNnPc2
se	se	k3xPyFc4
nazývali	nazývat	k5eAaImAgMnP
perioikové	perioik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
věnovali	věnovat	k5eAaImAgMnP,k5eAaPmAgMnP
především	především	k6eAd1
obchodu	obchod	k1gInSc2
a	a	k8xC
řemeslům	řemeslo	k1gNnPc3
<g/>
,	,	kIx,
kterými	který	k3yQgInPc7,k3yRgInPc7,k3yIgInPc7
spartiaté	spartiat	k1gMnPc1
opovrhovali	opovrhovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnižší	nízký	k2eAgFnSc4d3
vrstvu	vrstva	k1gFnSc4
tvořili	tvořit	k5eAaImAgMnP
heilóti	heilót	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
potomky	potomek	k1gMnPc7
původního	původní	k2eAgNnSc2d1
podrobeného	podrobený	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
a	a	k8xC
měli	mít	k5eAaImAgMnP
postavení	postavení	k1gNnSc4
státních	státní	k2eAgMnPc2d1
otroků	otrok	k1gMnPc2
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgNnPc2
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
relativně	relativně	k6eAd1
nízkému	nízký	k2eAgInSc3d1
počtu	počet	k1gInSc3
oproti	oproti	k7c3
ostatní	ostatní	k2eAgFnSc3d1
podmaněné	podmaněný	k2eAgFnSc3d1
populaci	populace	k1gFnSc3
podnikali	podnikat	k5eAaImAgMnP
spartiaté	spartiat	k1gMnPc1
vůči	vůči	k7c3
heilótům	heilót	k1gMnPc3
tzv.	tzv.	kA
krypteie	krypteie	k1gFnPc1
–	–	k?
výpravy	výprava	k1gFnSc2
za	za	k7c7
účelem	účel	k1gInSc7
masakrování	masakrování	k1gNnSc2
otroků	otrok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparťané	Sparťan	k1gMnPc1
tak	tak	k6eAd1
omezovali	omezovat	k5eAaImAgMnP
riziko	riziko	k1gNnSc4
jejich	jejich	k3xOp3gNnSc2
povstání	povstání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Třebaže	třebaže	k8xS
králové	král	k1gMnPc1
stáli	stát	k5eAaImAgMnP
v	v	k7c6
čele	čelo	k1gNnSc6
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
válečných	válečný	k2eAgNnPc2d1
tažení	tažení	k1gNnPc2
jim	on	k3xPp3gMnPc3
nepříslušela	příslušet	k5eNaImAgFnS
nijak	nijak	k6eAd1
zvlášť	zvlášť	k6eAd1
veliká	veliký	k2eAgFnSc1d1
moc	moc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
dohlížet	dohlížet	k5eAaImF
na	na	k7c4
krále	král	k1gMnSc4
byla	být	k5eAaImAgFnS
pověřena	pověřen	k2eAgFnSc1d1
pětičlenná	pětičlenný	k2eAgFnSc1d1
rada	rada	k1gFnSc1
eforů	efor	k1gMnPc2
(	(	kIx(
<g/>
dozorců	dozorce	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
každoročně	každoročně	k6eAd1
volená	volený	k2eAgFnSc1d1
shromážděním	shromáždění	k1gNnSc7
všech	všecek	k3xTgMnPc2
plnoprávných	plnoprávný	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
(	(	kIx(
<g/>
apella	apella	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgInSc7d1
důležitým	důležitý	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
byla	být	k5eAaImAgFnS
gerúsia	gerúsia	k1gFnSc1
–	–	k?
sbor	sbor	k1gInSc1
28	#num#	k4
stařešinů	stařešina	k1gMnPc2
(	(	kIx(
<g/>
gerontů	geron	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
zasedání	zasedání	k1gNnSc2
se	se	k3xPyFc4
zúčastňovali	zúčastňovat	k5eAaImAgMnP
i	i	k9
oba	dva	k4xCgMnPc1
králové	král	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
volený	volený	k2eAgMnSc1d1
apellou	apellat	k5eAaPmIp3nP
z	z	k7c2
řad	řada	k1gFnPc2
členů	člen	k1gInPc2
aristokracie	aristokracie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
nejméně	málo	k6eAd3
60	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidový	lidový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
pak	pak	k6eAd1
rozhodnutí	rozhodnutí	k1gNnSc4
sboru	sbor	k1gInSc2
starších	starší	k1gMnPc2
schválil	schválit	k5eAaPmAgMnS
nebo	nebo	k8xC
zamítnul	zamítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
nesměl	smět	k5eNaImAgMnS
jej	on	k3xPp3gMnSc4
však	však	k9
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Athény	Athéna	k1gFnPc1
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
demokracii	demokracie	k1gFnSc3
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Starověké	starověký	k2eAgFnSc2d1
Athény	Athéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Athénská	athénský	k2eAgFnSc1d1
akropole	akropole	k1gFnSc1
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Sparty	Sparta	k1gFnSc2
v	v	k7c6
Athénách	Athéna	k1gFnPc6
získalo	získat	k5eAaPmAgNnS
veškeré	veškerý	k3xTgNnSc1
obyvatelstvo	obyvatelstvo	k1gNnSc1
okolních	okolní	k2eAgFnPc2d1
atických	atický	k2eAgFnPc2d1
osad	osada	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
dobyty	dobýt	k5eAaPmNgFnP
a	a	k8xC
připojeny	připojit	k5eAaPmNgFnP
již	již	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
temného	temný	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
plné	plný	k2eAgNnSc4d1
občanství	občanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
683	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
bylo	být	k5eAaImAgNnS
odstraněno	odstranit	k5eAaPmNgNnS
dosavadní	dosavadní	k2eAgNnSc1d1
královské	královský	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
vládou	vláda	k1gFnSc7
rodové	rodový	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
(	(	kIx(
<g/>
eupatridai	eupatridai	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
držela	držet	k5eAaImAgFnS
moc	moc	k6eAd1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
až	až	k6eAd1
do	do	k7c2
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Eupatridé	Eupatridý	k2eAgFnSc2d1
zřídili	zřídit	k5eAaPmAgMnP
oligarchickou	oligarchický	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
a	a	k8xC
ustanovili	ustanovit	k5eAaPmAgMnP
úřad	úřad	k1gInSc4
archontů	archón	k1gMnPc2
<g/>
,	,	kIx,
každoročně	každoročně	k6eAd1
volených	volený	k2eAgMnPc2d1
nejvyšších	vysoký	k2eAgMnPc2d3
úředníků	úředník	k1gMnPc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
621	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
nechal	nechat	k5eAaPmAgMnS
Drakón	Drakón	k1gInSc4
sepsat	sepsat	k5eAaPmF
zvykové	zvykový	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
bylo	být	k5eAaImAgNnS
zamezeno	zamezen	k2eAgNnSc1d1
svévolnému	svévolný	k2eAgNnSc3d1
soudnictví	soudnictví	k1gNnSc3
aristokratů	aristokrat	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
jednu	jeden	k4xCgFnSc4
generaci	generace	k1gFnSc4
později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
594	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
provedl	provést	k5eAaPmAgMnS
Solón	Solón	k1gMnSc1
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
politického	politický	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odstranil	odstranit	k5eAaPmAgInS
šlechtická	šlechtický	k2eAgNnPc4d1
privilegia	privilegium	k1gNnPc4
a	a	k8xC
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
řízení	řízení	k1gNnSc2
státu	stát	k1gInSc2
také	také	k9
nižší	nízký	k2eAgFnSc1d2
vrstvy	vrstva	k1gFnPc1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgInSc4d1
pro	pro	k7c4
podíl	podíl	k1gInSc4
na	na	k7c4
řízení	řízení	k1gNnSc4
státu	stát	k1gInSc2
nebyl	být	k5eNaImAgInS
nyní	nyní	k6eAd1
původ	původ	k1gInSc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
majetek	majetek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občané	občan	k1gMnPc1
byli	být	k5eAaImAgMnP
podle	podle	k7c2
velikosti	velikost	k1gFnSc2
svého	svůj	k3xOyFgNnSc2
jmění	jmění	k1gNnSc2
rozděleni	rozdělit	k5eAaPmNgMnP
do	do	k7c2
čtyř	čtyři	k4xCgFnPc2
tříd	třída	k1gFnPc2
<g/>
:	:	kIx,
nejbohatší	bohatý	k2eAgFnPc1d3
pentakosiomedimnoi	pentakosiomedimno	k1gFnPc1
<g/>
,	,	kIx,
hippeis	hippeis	k1gFnPc1
(	(	kIx(
<g/>
jezdci	jezdec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zeugítai	zeugíta	k1gMnSc5
(	(	kIx(
<g/>
těžkooděnci	těžkooděnec	k1gMnSc6
<g/>
)	)	kIx)
a	a	k8xC
thétes	thétes	k1gInSc1
(	(	kIx(
<g/>
nádeníci	nádeník	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
prvně	prvně	k?
jmenovaní	jmenovaný	k1gMnPc1
byli	být	k5eAaImAgMnP
nejbohatší	bohatý	k2eAgFnSc7d3
vrstvou	vrstva	k1gFnSc7
s	s	k7c7
nejvíce	nejvíce	k6eAd1,k6eAd3
politickými	politický	k2eAgNnPc7d1
právy	právo	k1gNnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
současně	současně	k6eAd1
i	i	k9
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
povinnostmi	povinnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
mohli	moct	k5eAaImAgMnP
zastávat	zastávat	k5eAaImF
úřad	úřad	k1gInSc4
archónta	archónt	k1gInSc2
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
museli	muset	k5eAaImAgMnP
ale	ale	k9
hradit	hradit	k5eAaImF
nákladné	nákladný	k2eAgFnPc1d1
náboženské	náboženský	k2eAgFnPc1d1
slavnosti	slavnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Otroci	otrok	k1gMnPc1
představovali	představovat	k5eAaImAgMnP
v	v	k7c6
athénské	athénský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
všudypřítomné	všudypřítomný	k2eAgFnSc2d1
pracovní	pracovní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
a	a	k8xC
odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
tvořili	tvořit	k5eAaImAgMnP
až	až	k9
třetinu	třetina	k1gFnSc4
athénské	athénský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
životní	životní	k2eAgInSc1d1
standard	standard	k1gInSc1
nemusel	muset	k5eNaImAgInS
být	být	k5eAaImF
nutně	nutně	k6eAd1
špatný	špatný	k2eAgInSc4d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
jejich	jejich	k3xOp3gMnSc1
pán	pán	k1gMnSc1
jim	on	k3xPp3gMnPc3
mohl	moct	k5eAaImAgMnS
přiznat	přiznat	k5eAaPmF
právo	právo	k1gNnSc4
vést	vést	k5eAaImF
vlastní	vlastní	k2eAgFnSc4d1
živnost	živnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
byli	být	k5eAaImAgMnP
ovšem	ovšem	k9
otroci	otrok	k1gMnPc1
naprosto	naprosto	k6eAd1
bezprávní	bezprávní	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
jejich	jejich	k3xOp3gNnSc2
propuštění	propuštění	k1gNnSc2
na	na	k7c4
svobodu	svoboda	k1gFnSc4
obdrželi	obdržet	k5eAaPmAgMnP
titul	titul	k1gInSc4
usedlých	usedlý	k2eAgMnPc2d1
cizinců	cizinec	k1gMnPc2
a	a	k8xC
postrádali	postrádat	k5eAaImAgMnP
proto	proto	k8xC
politická	politický	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
ve	v	k7c6
většině	většina	k1gFnSc6
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dokázala	dokázat	k5eAaPmAgFnS
prosadit	prosadit	k5eAaPmF
tyranie	tyranie	k1gFnSc1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
polovině	polovina	k1gFnSc6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
chopil	chopit	k5eAaPmAgMnS
moci	moct	k5eAaImF
Peisistratos	Peisistratos	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
ponechal	ponechat	k5eAaPmAgInS
Solónovu	Solónův	k2eAgFnSc4d1
ústavu	ústava	k1gFnSc4
formálně	formálně	k6eAd1
v	v	k7c6
platnosti	platnost	k1gFnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
všechny	všechen	k3xTgInPc1
úřady	úřad	k1gInPc1
ve	v	k7c6
státě	stát	k1gInSc6
byly	být	k5eAaImAgInP
vykonávány	vykonáván	k2eAgInPc4d1
členy	člen	k1gInPc4
jeho	jeho	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
nebo	nebo	k8xC
důvěrníky	důvěrník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gMnPc2
více	hodně	k6eAd2
než	než	k8xS
dvacetileté	dvacetiletý	k2eAgFnPc1d1
vlády	vláda	k1gFnPc1
zažily	zažít	k5eAaPmAgFnP
Athény	Athéna	k1gFnPc4
značný	značný	k2eAgInSc4d1
hospodářský	hospodářský	k2eAgInSc4d1
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgInSc4d1
a	a	k8xC
také	také	k9
zahraničněpolitický	zahraničněpolitický	k2eAgInSc1d1
mocenský	mocenský	k2eAgInSc1d1
rozmach	rozmach	k1gInSc1
(	(	kIx(
<g/>
rovněž	rovněž	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
vítězství	vítězství	k1gNnSc2
ve	v	k7c6
válce	válka	k1gFnSc6
s	s	k7c7
Megarou	Megara	k1gFnSc7
o	o	k7c4
ostrov	ostrov	k1gInSc4
Salamína	Salamín	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peisistratův	Peisistratův	k2eAgInSc1d1
syn	syn	k1gMnSc1
Hippiás	Hippiás	k1gInSc4
byl	být	k5eAaImAgMnS
však	však	k9
v	v	k7c6
roce	rok	k1gInSc6
510	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vyhnán	vyhnán	k2eAgInSc1d1
lidovým	lidový	k2eAgNnSc7d1
povstáním	povstání	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
přivedlo	přivést	k5eAaPmAgNnS
k	k	k7c3
moci	moc	k1gFnSc3
aristokrata	aristokrat	k1gMnSc2
Kleisthena	Kleisthen	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jím	jíst	k5eAaImIp1nS
uskutečněné	uskutečněný	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
v	v	k7c6
letech	léto	k1gNnPc6
509	#num#	k4
až	až	k9
507	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
znamenaly	znamenat	k5eAaImAgFnP
zrod	zrod	k1gInSc4
athénské	athénský	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
plnoprávní	plnoprávní	k2eAgMnPc1d1
občané	občan	k1gMnPc1
nad	nad	k7c4
třicet	třicet	k4xCc4
let	léto	k1gNnPc2
obdrželi	obdržet	k5eAaPmAgMnP
nezávisle	závisle	k6eNd1
na	na	k7c6
svém	svůj	k3xOyFgInSc6
majetku	majetek	k1gInSc6
nebo	nebo	k8xC
stavu	stav	k1gInSc6
rovná	rovnat	k5eAaImIp3nS
politická	politický	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
otroci	otrok	k1gMnPc1
a	a	k8xC
usedlí	usedlý	k2eAgMnPc1d1
cizinci	cizinec	k1gMnPc1
zůstali	zůstat	k5eAaPmAgMnP
z	z	k7c2
tohoto	tento	k3xDgInSc2
uspořádání	uspořádání	k1gNnSc6
vyloučeni	vyloučit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
shromáždění	shromáždění	k1gNnSc1
(	(	kIx(
<g/>
ekklésiá	ekklésiá	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
se	se	k3xPyFc4
směl	smět	k5eAaImAgMnS
každý	každý	k3xTgMnSc1
občan	občan	k1gMnSc1
svobodně	svobodně	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
<g/>
,	,	kIx,
vydávalo	vydávat	k5eAaPmAgNnS,k5eAaImAgNnS
zákony	zákon	k1gInPc4
a	a	k8xC
řídilo	řídit	k5eAaImAgNnS
státní	státní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
prostřednictvím	prostřednictvím	k7c2
usnesení	usnesení	k1gNnSc2
schválených	schválený	k2eAgFnPc2d1
většinou	většina	k1gFnSc7
svých	svůj	k3xOyFgMnPc2
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reprezentativním	reprezentativní	k2eAgInSc7d1
a	a	k8xC
výkonným	výkonný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
byla	být	k5eAaImAgFnS
„	„	k?
<g/>
rada	rada	k1gFnSc1
pěti	pět	k4xCc3
set	set	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
búlé	búlá	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
skládala	skládat	k5eAaImAgFnS
z	z	k7c2
padesáti	padesát	k4xCc2
členů	člen	k1gInPc2
z	z	k7c2
každé	každý	k3xTgFnSc2
z	z	k7c2
deseti	deset	k4xCc7
Kleisthenem	Kleistheno	k1gNnSc7
nově	nově	k6eAd1
uspořádaných	uspořádaný	k2eAgFnPc2d1
fýl	fýl	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
fýla	fýlus	k1gMnSc4
mimoto	mimoto	k6eAd1
volila	volit	k5eAaImAgFnS
jednoho	jeden	k4xCgMnSc4
stratéga	stratég	k1gMnSc4
(	(	kIx(
<g/>
vojevůdce	vojevůdce	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
úřady	úřad	k1gInPc1
a	a	k8xC
soudci	soudce	k1gMnPc1
byli	být	k5eAaImAgMnP
určováni	určovat	k5eAaImNgMnP
losem	los	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
byl	být	k5eAaImAgInS
zaveden	zaveden	k2eAgInSc1d1
institut	institut	k1gInSc1
střepinového	střepinový	k2eAgInSc2d1
soudu	soud	k1gInSc2
(	(	kIx(
<g/>
ostrakismos	ostrakismos	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
zabránit	zabránit	k5eAaPmF
znovunastolení	znovunastolení	k1gNnSc4
tyranidy	tyranida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
doba	doba	k1gFnSc1
(	(	kIx(
<g/>
480	#num#	k4
<g/>
–	–	k?
<g/>
323	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Klasické	klasický	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Řecko-perské	řecko-perský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Řecko-perské	řecko-perský	k2eAgFnPc1d1
války	válka	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
500	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
povstaly	povstat	k5eAaPmAgFnP
řecké	řecký	k2eAgFnPc1d1
obce	obec	k1gFnPc1
v	v	k7c6
Iónii	Iónie	k1gFnSc6
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
proti	proti	k7c3
perské	perský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
byly	být	k5eAaImAgInP
vystaveny	vystavit	k5eAaPmNgInP
od	od	k7c2
zániku	zánik	k1gInSc2
lýdské	lýdský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
546	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
jistou	jistý	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
i	i	k9
v	v	k7c6
obcích	obec	k1gFnPc6
pevninského	pevninský	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
především	především	k9
v	v	k7c6
Athénách	Athéna	k1gFnPc6
a	a	k8xC
v	v	k7c6
Eretrii	Eretrie	k1gFnSc6
(	(	kIx(
<g/>
město	město	k1gNnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Euboia	Euboium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
poskytlo	poskytnout	k5eAaPmAgNnS
perskému	perský	k2eAgMnSc3d1
velkokráli	velkokrál	k1gMnSc3
Dáreiovi	Dáreius	k1gMnSc3
I.	I.	kA
záminku	záminka	k1gFnSc4
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Řecko	Řecko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
iónské	iónský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
se	se	k3xPyFc4
tak	tak	k9
stalo	stát	k5eAaPmAgNnS
roznětkou	roznětka	k1gFnSc7
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
dlouhotrvajícího	dlouhotrvající	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
mezi	mezi	k7c7
Helény	Helén	k1gMnPc7
a	a	k8xC
Peršany	Peršan	k1gMnPc7
<g/>
,	,	kIx,
o	o	k7c6
němž	jenž	k3xRgNnSc6
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
podává	podávat	k5eAaImIp3nS
výklad	výklad	k1gInSc1
řecký	řecký	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Hérodotos	Hérodotos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
494	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Dáreios	Dáreios	k1gMnSc1
porazil	porazit	k5eAaPmAgMnS
a	a	k8xC
ztrestal	ztrestat	k5eAaPmAgMnS
iónské	iónský	k2eAgMnPc4d1
Řeky	Řek	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mílétos	Mílétos	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
stál	stát	k5eAaImAgMnS
v	v	k7c6
čele	čelo	k1gNnSc6
vzpoury	vzpoura	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Peršany	peršan	k1gInPc4
srovnán	srovnán	k2eAgMnSc1d1
se	s	k7c7
zemí	zem	k1gFnSc7
a	a	k8xC
ostatní	ostatní	k1gNnSc1
města	město	k1gNnSc2
se	se	k3xPyFc4
Peršanům	Peršan	k1gMnPc3
rychle	rychle	k6eAd1
poddala	poddat	k5eAaPmAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nato	nato	k6eAd1
Dáreios	Dáreios	k1gMnSc1
poslal	poslat	k5eAaPmAgMnS
do	do	k7c2
Řecka	Řecko	k1gNnSc2
emisary	emisar	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jeho	jeho	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
žádali	žádat	k5eAaImAgMnP
podrobení	podrobený	k2eAgMnPc1d1
Řeků	Řek	k1gMnPc2
perské	perský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Sparta	Sparta	k1gFnSc1
a	a	k8xC
Athény	Athéna	k1gFnPc1
odmítly	odmítnout	k5eAaPmAgFnP
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
svým	svůj	k3xOyFgNnSc7
loďstvem	loďstvo	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
490	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zničit	zničit	k5eAaPmF
nejprve	nejprve	k6eAd1
Eretrii	Eretrie	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
podnikl	podniknout	k5eAaPmAgMnS
výpravu	výprava	k1gFnSc4
proti	proti	k7c3
vzpurným	vzpurný	k2eAgMnPc3d1
Athéňanům	Athéňan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
planině	planina	k1gFnSc6
poblíž	poblíž	k7c2
místa	místo	k1gNnSc2
jménem	jméno	k1gNnSc7
Marathón	Marathón	k1gInSc4
v	v	k7c6
Attice	Attika	k1gFnSc6
však	však	k9
třikrát	třikrát	k6eAd1
silnější	silný	k2eAgNnSc4d2
perské	perský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
podlehlo	podlehnout	k5eAaPmAgNnS
athénským	athénský	k2eAgInSc7d1
hoplítům	hoplít	k1gMnPc3
pod	pod	k7c7
velením	velení	k1gNnSc7
Miltiada	Miltiada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Xerxés	Xerxés	k1gInSc1
I.	I.	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nastoupil	nastoupit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
486	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c4
perský	perský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
otci	otec	k1gMnSc6
Dáreiovi	Dáreius	k1gMnSc6
<g/>
,	,	kIx,
shromáždil	shromáždit	k5eAaPmAgInS
k	k	k7c3
dobytí	dobytí	k1gNnSc3
Řecka	Řecko	k1gNnSc2
největší	veliký	k2eAgFnSc4d3
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
jakou	jaký	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
starověk	starověk	k1gInSc1
spatřil	spatřit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
zajištění	zajištění	k1gNnSc3
pozemního	pozemní	k2eAgInSc2d1
přístupu	přístup	k1gInSc2
dal	dát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
481	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vybudovat	vybudovat	k5eAaPmF
z	z	k7c2
lodí	loď	k1gFnPc2
most	most	k1gInSc4
přes	přes	k7c4
Helléspont	Helléspont	k1gInSc4
<g/>
,	,	kIx,
po	po	k7c6
němž	jenž	k3xRgInSc6
jeho	jeho	k3xOp3gInSc6
armáda	armáda	k1gFnSc1
přešla	přejít	k5eAaPmAgFnS
z	z	k7c2
Asie	Asie	k1gFnSc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
byl	být	k5eAaImAgMnS
pod	pod	k7c7
dojmem	dojem	k1gInSc7
této	tento	k3xDgFnSc2
hrozby	hrozba	k1gFnSc2
založen	založit	k5eAaPmNgInS
helénský	helénský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yIgNnSc3,k3yRgNnSc3,k3yQgNnSc3
kromě	kromě	k7c2
Sparty	Sparta	k1gFnSc2
náležely	náležet	k5eAaImAgInP
také	také	k6eAd1
některé	některý	k3yIgFnPc1
řecké	řecký	k2eAgFnPc1d1
obce	obec	k1gFnPc1
včetně	včetně	k7c2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
obcí	obec	k1gFnPc2
především	především	k9
na	na	k7c6
severu	sever	k1gInSc6
však	však	k9
zůstávala	zůstávat	k5eAaImAgNnP
vně	vně	k7c2
spolku	spolek	k1gInSc2
a	a	k8xC
dokonce	dokonce	k9
byly	být	k5eAaImAgFnP
připravené	připravený	k2eAgFnPc1d1
podrobit	podrobit	k5eAaPmF
se	s	k7c7
králi	král	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
desetileté	desetiletý	k2eAgFnSc2d1
přestávky	přestávka	k1gFnSc2
v	v	k7c6
bojích	boj	k1gInPc6
dokázal	dokázat	k5eAaPmAgInS
Themistoklés	Themistoklés	k1gInSc1
přesvědčit	přesvědčit	k5eAaPmF
Athéňany	Athéňan	k1gMnPc4
o	o	k7c6
nutnosti	nutnost	k1gFnSc6
výstavby	výstavba	k1gFnSc2
válečného	válečný	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
ukázalo	ukázat	k5eAaPmAgNnS
jako	jako	k9
rozhodující	rozhodující	k2eAgInSc4d1
čin	čin	k1gInSc4
celé	celý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průsmyku	průsmyk	k1gInSc6
Thermopyly	Thermopyly	k1gFnPc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
spojuje	spojovat	k5eAaImIp3nS
Thesálii	Thesálie	k1gFnSc4
se	s	k7c7
středním	střední	k2eAgNnSc7d1
Řeckem	Řecko	k1gNnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
postupujícím	postupující	k2eAgMnPc3d1
Peršanům	peršan	k1gInPc3
v	v	k7c6
roce	rok	k1gInSc6
480	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
postavil	postavit	k5eAaPmAgMnS
na	na	k7c4
odpor	odpor	k1gInSc4
spartský	spartský	k2eAgMnSc1d1
král	král	k1gMnSc1
Leónidás	Leónidása	k1gFnPc2
I.	I.	kA
Přes	přes	k7c4
několikanásobnou	několikanásobný	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
barbarů	barbar	k1gMnPc2
Helénové	Helén	k1gMnPc1
neustoupili	ustoupit	k5eNaPmAgMnP
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
byli	být	k5eAaImAgMnP
v	v	k7c6
důsledku	důsledek	k1gInSc6
zrady	zrada	k1gFnSc2
obklíčeni	obklíčit	k5eAaPmNgMnP
<g/>
,	,	kIx,
padli	padnout	k5eAaPmAgMnP,k5eAaImAgMnP
hrdinskou	hrdinský	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgNnSc6
vítězství	vítězství	k1gNnSc6
vtáhli	vtáhnout	k5eAaPmAgMnP
Peršané	Peršan	k1gMnPc1
do	do	k7c2
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
už	už	k6eAd1
předtím	předtím	k6eAd1
opuštěných	opuštěný	k2eAgMnPc2d1
svými	svůj	k3xOyFgMnPc7
obyvateli	obyvatel	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
zcela	zcela	k6eAd1
toto	tento	k3xDgNnSc4
město	město	k1gNnSc4
vypálili	vypálit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
v	v	k7c6
námořní	námořní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
řecká	řecký	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
<g/>
,	,	kIx,
vedená	vedený	k2eAgFnSc1d1
Themistoklem	Themistokl	k1gInSc7
a	a	k8xC
složená	složený	k2eAgFnSc1d1
z	z	k7c2
malých	malý	k2eAgFnPc2d1
ale	ale	k8xC
snadno	snadno	k6eAd1
ovladatelných	ovladatelný	k2eAgFnPc2d1
triér	triéra	k1gFnPc2
<g/>
,	,	kIx,
zvítězila	zvítězit	k5eAaPmAgFnS
nad	nad	k7c7
početně	početně	k6eAd1
silnější	silný	k2eAgFnSc7d2
ale	ale	k8xC
těžkopádnější	těžkopádný	k2eAgFnSc7d2
perskou	perský	k2eAgFnSc7d1
flotilou	flotila	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zničením	zničení	k1gNnSc7
perského	perský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
přivodili	přivodit	k5eAaPmAgMnP,k5eAaBmAgMnP
Řekové	Řek	k1gMnPc1
obrovskému	obrovský	k2eAgNnSc3d1
perskému	perský	k2eAgNnSc3d1
vojsku	vojsko	k1gNnSc3
potíže	potíž	k1gFnSc2
se	s	k7c7
zásobováním	zásobování	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jeho	jeho	k3xOp3gFnSc4
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
přimělo	přimět	k5eAaPmAgNnS
k	k	k7c3
návratu	návrat	k1gInSc3
do	do	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
perských	perský	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
byl	být	k5eAaImAgInS
poražen	poražen	k2eAgInSc1d1
Helény	Helén	k1gMnPc7
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Pausaniem	Pausanium	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
479	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Platají	Platají	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
těchto	tento	k3xDgFnPc6
porážkách	porážka	k1gFnPc6
Peršané	Peršan	k1gMnPc1
upustili	upustit	k5eAaPmAgMnP
od	od	k7c2
svých	svůj	k3xOyFgInPc2
plánů	plán	k1gInPc2
na	na	k7c4
dobytí	dobytí	k1gNnSc4
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
478	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
bylo	být	k5eAaImAgNnS
zahájeno	zahájit	k5eAaPmNgNnS
tažení	tažení	k1gNnSc1
k	k	k7c3
osvobození	osvobození	k1gNnSc3
iónských	iónský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
zdráhala	zdráhat	k5eAaImAgFnS
poskytnout	poskytnout	k5eAaPmF
pomoc	pomoc	k1gFnSc4
těmto	tento	k3xDgMnPc3
pro	pro	k7c4
ně	on	k3xPp3gFnPc4
příliš	příliš	k6eAd1
vzdáleným	vzdálený	k2eAgMnPc3d1
soukmenovcům	soukmenovec	k1gMnPc3
<g/>
,	,	kIx,
čehož	což	k3yQnSc2,k3yRnSc2
využily	využít	k5eAaPmAgFnP
Athény	Athéna	k1gFnPc1
k	k	k7c3
založení	založení	k1gNnSc3
délského	délský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
začalo	začít	k5eAaPmAgNnS
soupeření	soupeření	k1gNnSc1
Sparty	Sparta	k1gFnSc2
a	a	k8xC
Athén	Athéna	k1gFnPc2
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
mělo	mít	k5eAaImAgNnS
nakonec	nakonec	k6eAd1
vyústit	vyústit	k5eAaPmF
v	v	k7c4
peloponéskou	peloponéský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
se	se	k3xPyFc4
dokázaly	dokázat	k5eAaPmAgFnP
s	s	k7c7
vydatným	vydatný	k2eAgNnSc7d1
přispěním	přispění	k1gNnSc7
svých	svůj	k3xOyFgMnPc2
spojenců	spojenec	k1gMnPc2
úspěšně	úspěšně	k6eAd1
prosadit	prosadit	k5eAaPmF
proti	proti	k7c3
Peršanům	Peršan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnPc1
cíl	cíl	k1gInSc1
–	–	k?
osvobození	osvobození	k1gNnSc2
Iónie	Iónie	k1gFnSc2
–	–	k?
byl	být	k5eAaImAgInS
vítězně	vítězně	k6eAd1
završen	završit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
465	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
athénským	athénský	k2eAgMnSc7d1
vojevůdcem	vojevůdce	k1gMnSc7
Kimónem	Kimón	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
od	od	k7c2
tohoto	tento	k3xDgInSc2
okamžiku	okamžik	k1gInSc2
napadali	napadat	k5eAaImAgMnP,k5eAaPmAgMnP,k5eAaBmAgMnP
perské	perský	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
v	v	k7c6
Egyptě	Egypt	k1gInSc6
a	a	k8xC
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Středomoří	středomoří	k1gNnSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
však	však	k9
vyčerpali	vyčerpat	k5eAaPmAgMnP
zdroje	zdroj	k1gInPc4
spolku	spolek	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
449	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
uzavření	uzavření	k1gNnSc3
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
s	s	k7c7
Persií	Persie	k1gFnSc7
<g/>
,	,	kIx,
nazývané	nazývaný	k2eAgFnSc2d1
Kalliův	Kalliův	k2eAgInSc4d1
mír	mír	k1gInSc4
(	(	kIx(
<g/>
jeho	jeho	k3xOp3gFnSc1
autenticita	autenticita	k1gFnSc1
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
moderními	moderní	k2eAgMnPc7d1
historiky	historik	k1gMnPc7
zpochybňována	zpochybňován	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dominance	dominance	k1gFnSc1
Athén	Athéna	k1gFnPc2
</s>
<s>
Periklova	Periklův	k2eAgFnSc1d1
busta	busta	k1gFnSc1
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
</s>
<s>
V	v	k7c6
desetiletích	desetiletí	k1gNnPc6
po	po	k7c6
skončení	skončení	k1gNnSc6
hlavní	hlavní	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
(	(	kIx(
<g/>
479	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
zažívaly	zažívat	k5eAaImAgFnP
Athény	Athéna	k1gFnPc1
vrchol	vrchol	k1gInSc1
svého	svůj	k3xOyFgInSc2
politického	politický	k2eAgInSc2d1
a	a	k8xC
kulturního	kulturní	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
vedoucí	vedoucí	k2eAgFnSc3d1
roli	role	k1gFnSc3
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Peršanům	Peršan	k1gMnPc3
a	a	k8xC
také	také	k9
díky	díky	k7c3
síle	síla	k1gFnSc3
své	svůj	k3xOyFgFnSc2
flotily	flotila	k1gFnSc2
předstihly	předstihnout	k5eAaPmAgFnP
svého	svůj	k3xOyFgMnSc2
soka	sok	k1gMnSc2
–	–	k?
Spartu	Sparta	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
dosud	dosud	k6eAd1
nejmocnějším	mocný	k2eAgInSc7d3
řeckým	řecký	k2eAgInSc7d1
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
byla	být	k5eAaImAgFnS
potvrzena	potvrdit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
478	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
200	#num#	k4
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
spojilo	spojit	k5eAaPmAgNnS
v	v	k7c6
délském	délský	k2eAgInSc6d1
spolku	spolek	k1gInSc6
vedeném	vedený	k2eAgInSc6d1
Athénami	Athéna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
tohoto	tento	k3xDgInSc2
spolku	spolek	k1gInSc2
slíbili	slíbit	k5eAaPmAgMnP
odvádět	odvádět	k5eAaImF
do	do	k7c2
společné	společný	k2eAgFnSc2d1
kasy	kasa	k1gFnSc2
pravidelné	pravidelný	k2eAgInPc4d1
příspěvky	příspěvek	k1gInPc4
určené	určený	k2eAgInPc4d1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
a	a	k8xC
údržbu	údržba	k1gFnSc4
loďstva	loďstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
462	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
provedl	provést	k5eAaPmAgInS
Efialtés	Efialtés	k1gInSc1
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
athénské	athénský	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
byla	být	k5eAaImAgFnS
omezena	omezit	k5eAaPmNgFnS
pravomoc	pravomoc	k1gFnSc1
areopagu	areopag	k1gInSc2
jakožto	jakožto	k8xS
výkonného	výkonný	k2eAgInSc2d1
orgánu	orgán	k1gInSc2
a	a	k8xC
nejvyššího	vysoký	k2eAgInSc2d3
soudu	soud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
funkce	funkce	k1gFnSc1
nadále	nadále	k6eAd1
vykonávala	vykonávat	k5eAaImAgFnS
rada	rada	k1gFnSc1
pěti	pět	k4xCc2
set	sto	k4xCgNnPc2
a	a	k8xC
lidové	lidový	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
odmítla	odmítnout	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
athénskou	athénský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
k	k	k7c3
potlačení	potlačení	k1gNnSc3
masivního	masivní	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
heilótů	heilót	k1gMnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
obávala	obávat	k5eAaImAgFnS
rozšíření	rozšíření	k1gNnSc3
athénské	athénský	k2eAgFnSc2d1
moci	moc	k1gFnSc2
na	na	k7c4
Peloponés	Peloponés	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
by	by	kYmCp3nP
byl	být	k5eAaImAgInS
ještě	ještě	k6eAd1
více	hodně	k6eAd2
podlomen	podlomen	k2eAgInSc1d1
její	její	k3xOp3gInSc1
vliv	vliv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zavraždění	zavraždění	k1gNnSc6
Efialta	Efialt	k1gInSc2
převzal	převzít	k5eAaPmAgMnS
vedení	vedení	k1gNnSc4
Athén	Athéna	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
461	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Periklés	Periklés	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Periklés	Periklés	k1gInSc1
byl	být	k5eAaImAgInS
velice	velice	k6eAd1
často	často	k6eAd1
volen	volit	k5eAaImNgMnS
do	do	k7c2
úřadu	úřad	k1gInSc2
stratéga	stratég	k1gMnSc2
(	(	kIx(
<g/>
v	v	k7c6
letech	léto	k1gNnPc6
443	#num#	k4
až	až	k9
429	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zastával	zastávat	k5eAaImAgInS
tuto	tento	k3xDgFnSc4
funkci	funkce	k1gFnSc4
nepřetržitě	přetržitě	k6eNd1
<g/>
)	)	kIx)
a	a	k8xC
vynutil	vynutit	k5eAaPmAgInS
si	se	k3xPyFc3
ještě	ještě	k6eAd1
větší	veliký	k2eAgNnSc4d2
posílení	posílení	k1gNnSc4
demokratických	demokratický	k2eAgInPc2d1
prvků	prvek	k1gInPc2
v	v	k7c6
athénské	athénský	k2eAgFnSc6d1
ústavě	ústava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
461	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Periklés	Periklés	k1gInSc1
uzákonil	uzákonit	k5eAaPmAgInS
odměňování	odměňování	k1gNnSc4
vykonávání	vykonávání	k1gNnSc2
veřejných	veřejný	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
umožnil	umožnit	k5eAaPmAgInS
neomezený	omezený	k2eNgInSc4d1
přístup	přístup	k1gInSc4
lidu	lid	k1gInSc2
(	(	kIx(
<g/>
demos	demos	k1gInSc1
<g/>
)	)	kIx)
k	k	k7c3
výkonu	výkon	k1gInSc3
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
správy	správa	k1gFnSc2
a	a	k8xC
soudnictví	soudnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
460	#num#	k4
až	až	k9
457	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byly	být	k5eAaImAgInP
vystavěny	vystavět	k5eAaPmNgInP
tzv.	tzv.	kA
„	„	k?
<g/>
dlouhé	dlouhý	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
spojovaly	spojovat	k5eAaImAgFnP
Athény	Athéna	k1gFnPc4
s	s	k7c7
přístavem	přístav	k1gInSc7
Pireus	Pireus	k1gMnSc1
a	a	k8xC
vytvářely	vytvářet	k5eAaImAgFnP
z	z	k7c2
Athén	Athéna	k1gFnPc2
nedobytnou	dobytný	k2eNgFnSc4d1
pevnost	pevnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Periklés	Periklés	k1gInSc1
prosadil	prosadit	k5eAaPmAgInS
také	také	k9
pokračování	pokračování	k1gNnSc4
války	válka	k1gFnSc2
s	s	k7c7
Peršany	Peršan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
výprava	výprava	k1gFnSc1
na	na	k7c4
podporu	podpora	k1gFnSc4
Egypťanů	Egypťan	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
povstali	povstat	k5eAaPmAgMnP
proti	proti	k7c3
Peršanům	Peršan	k1gMnPc3
<g/>
,	,	kIx,
skončila	skončit	k5eAaPmAgFnS
nezdarem	nezdar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athénská	athénský	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
dokázala	dokázat	k5eAaPmAgFnS
Peršany	Peršan	k1gMnPc4
rozhodně	rozhodně	k6eAd1
porazit	porazit	k5eAaPmF
teprve	teprve	k6eAd1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Salamíny	Salamína	k1gFnSc2
na	na	k7c6
Kypru	Kypr	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
450	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Následně	následně	k6eAd1
athénský	athénský	k2eAgMnSc1d1
vyjednavač	vyjednavač	k1gMnSc1
Kalliás	Kalliása	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
449	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
dohodl	dohodnout	k5eAaPmAgInS
s	s	k7c7
Peršany	Peršan	k1gMnPc7
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gNnSc6
zavázali	zavázat	k5eAaPmAgMnP
respektovat	respektovat	k5eAaImF
autonomii	autonomie	k1gFnSc4
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
na	na	k7c6
maloasijském	maloasijský	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
a	a	k8xC
nevysílat	vysílat	k5eNaImF
své	svůj	k3xOyFgFnPc4
lodě	loď	k1gFnPc4
do	do	k7c2
Egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Athény	Athéna	k1gFnPc1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
uplatňovaly	uplatňovat	k5eAaImAgFnP
velmi	velmi	k6eAd1
agresivní	agresivní	k2eAgFnSc4d1
zahraniční	zahraniční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athénský	athénský	k2eAgInSc1d1
námořní	námořní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
(	(	kIx(
<g/>
symmachia	symmachia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
původním	původní	k2eAgInSc7d1
účelem	účel	k1gInSc7
byla	být	k5eAaImAgFnS
porážka	porážka	k1gFnSc1
Peršanů	Peršan	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
postupem	postup	k1gInSc7
doby	doba	k1gFnSc2
vyvinul	vyvinout	k5eAaPmAgInS
ve	v	k7c4
skutečnou	skutečný	k2eAgFnSc4d1
athénskou	athénský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
(	(	kIx(
<g/>
arché	arché	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
představovala	představovat	k5eAaImAgFnS
nástroj	nástroj	k1gInSc4
k	k	k7c3
prosazení	prosazení	k1gNnSc3
ryze	ryze	k6eAd1
athénských	athénský	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
spolku	spolek	k1gInSc2
pozvolna	pozvolna	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
své	svůj	k3xOyFgNnSc4
postavení	postavení	k1gNnSc4
athénských	athénský	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
a	a	k8xC
byli	být	k5eAaImAgMnP
Athénám	Athéna	k1gFnPc3
v	v	k7c6
podstatě	podstata	k1gFnSc6
podřízeni	podřídit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
469	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ostrov	ostrov	k1gInSc1
Naxos	Naxos	k1gInSc1
pokusil	pokusit	k5eAaPmAgInS
opustit	opustit	k5eAaPmF
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
vojensky	vojensky	k6eAd1
poražen	porazit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
zacházely	zacházet	k5eAaImAgFnP
Athény	Athéna	k1gFnPc1
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
i	i	k9
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
odpadly	odpadnout	k5eAaPmAgFnP
od	od	k7c2
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšiřování	rozšiřování	k1gNnSc1
demokracie	demokracie	k1gFnSc2
začalo	začít	k5eAaPmAgNnS
časem	časem	k6eAd1
sloužit	sloužit	k5eAaImF
jako	jako	k9
prostředek	prostředek	k1gInSc4
k	k	k7c3
dosažení	dosažení	k1gNnSc3
athénských	athénský	k2eAgInPc2d1
cílů	cíl	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
Athéňané	Athéňan	k1gMnPc1
mnohdy	mnohdy	k6eAd1
dopouštěli	dopouštět	k5eAaImAgMnP
značných	značný	k2eAgFnPc2d1
krutostí	krutost	k1gFnSc7
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
například	například	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
peloponéské	peloponéský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolních	okolní	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
především	především	k6eAd1
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
<g/>
,	,	kIx,
Thébách	Théby	k1gFnPc6
a	a	k8xC
Korintu	Korint	k1gInSc6
<g/>
,	,	kIx,
narůstaly	narůstat	k5eAaImAgFnP
obavy	obava	k1gFnPc1
z	z	k7c2
athénského	athénský	k2eAgInSc2d1
vzestupu	vzestup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Megara	Megara	k1gFnSc1
vystoupila	vystoupit	k5eAaPmAgFnS
z	z	k7c2
peloponéského	peloponéský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
připojila	připojit	k5eAaPmAgFnS
k	k	k7c3
Athénám	Athéna	k1gFnPc3
<g/>
,	,	kIx,
vypukl	vypuknout	k5eAaPmAgInS
v	v	k7c6
letech	léto	k1gNnPc6
460	#num#	k4
až	až	k9
446	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
konflikt	konflikt	k1gInSc1
někdy	někdy	k6eAd1
také	také	k9
nazývaný	nazývaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
první	první	k4xOgFnSc1
peloponéská	peloponéský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gInSc6
konci	konec	k1gInSc6
byl	být	k5eAaImAgInS
sice	sice	k8xC
uzavřen	uzavřít	k5eAaPmNgInS
třicetiletý	třicetiletý	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
,	,	kIx,
přesto	přesto	k8xC
však	však	k9
ve	v	k7c6
vztazích	vztah	k1gInPc6
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
městy	město	k1gNnPc7
panovalo	panovat	k5eAaImAgNnS
nadále	nadále	k6eAd1
napětí	napětí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
Spartě	Sparta	k1gFnSc3
ponechaly	ponechat	k5eAaPmAgFnP
volnou	volný	k2eAgFnSc4d1
ruku	ruka	k1gFnSc4
na	na	k7c6
Peloponésu	Peloponés	k1gInSc6
a	a	k8xC
samy	sám	k3xTgFnPc1
se	se	k3xPyFc4
koncentrovaly	koncentrovat	k5eAaBmAgInP
výhradně	výhradně	k6eAd1
na	na	k7c4
rozšiřování	rozšiřování	k1gNnSc4
svého	svůj	k3xOyFgInSc2
vlivu	vliv	k1gInSc2
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
<g/>
,	,	kIx,
Thrákii	Thrákie	k1gFnSc6
a	a	k8xC
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Řekové	Řek	k1gMnPc1
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
a	a	k8xC
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
museli	muset	k5eAaImAgMnP
bránit	bránit	k5eAaImF
dvojímu	dvojí	k4xRgNnSc3
ohrožení	ohrožení	k1gNnSc3
<g/>
:	:	kIx,
ze	z	k7c2
strany	strana	k1gFnSc2
Etrusků	Etrusk	k1gMnPc2
a	a	k8xC
proti	proti	k7c3
mocným	mocný	k2eAgMnPc3d1
Kartágincům	Kartáginec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
neustálým	neustálý	k2eAgFnPc3d1
válkám	válka	k1gFnPc3
s	s	k7c7
Kartágem	Kartágo	k1gNnSc7
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
Punové	Pun	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
480	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
drtivě	drtivě	k6eAd1
poraženi	porazit	k5eAaPmNgMnP
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Himéry	Himéra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
(	(	kIx(
<g/>
474	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
dosáhli	dosáhnout	k5eAaPmAgMnP
podobného	podobný	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
jihoitalští	jihoitalský	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
v	v	k7c6
námořní	námořní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kýmé	Kýmá	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
znamenala	znamenat	k5eAaImAgFnS
konec	konec	k1gInSc4
etruské	etruský	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obcích	obec	k1gFnPc6
Velkého	velký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
se	se	k3xPyFc4
k	k	k7c3
moci	moc	k1gFnSc3
dostávali	dostávat	k5eAaImAgMnP
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
v	v	k7c6
mateřském	mateřský	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
různí	různit	k5eAaImIp3nP
tyrani	tyran	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
vyvoláno	vyvolat	k5eAaPmNgNnS
patrně	patrně	k6eAd1
častější	častý	k2eAgFnSc7d2
potřebou	potřeba	k1gFnSc7
silného	silný	k2eAgMnSc4d1
vůdce	vůdce	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
by	by	kYmCp3nS
dokázal	dokázat	k5eAaPmAgMnS
ochránit	ochránit	k5eAaPmF
stát	stát	k1gInSc4
před	před	k7c7
vnějšími	vnější	k2eAgFnPc7d1
hrozbami	hrozba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byli	být	k5eAaImAgMnP
pozoruhodnými	pozoruhodný	k2eAgMnPc7d1
jedinci	jedinec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Gelón	Gelón	k1gInSc4
v	v	k7c6
Syrákúsách	Syrákúsa	k1gFnPc6
byl	být	k5eAaImAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
pokládán	pokládat	k5eAaImNgMnS
za	za	k7c4
nejmocnějšího	mocný	k2eAgMnSc4d3
muže	muž	k1gMnSc4
řeckého	řecký	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Peloponéská	peloponéský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Peloponéská	peloponéský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Bitvy	bitva	k1gFnPc1
a	a	k8xC
tažení	tažení	k1gNnSc1
peloponéské	peloponéský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Trvalé	trvalý	k2eAgInPc1d1
rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
athénským	athénský	k2eAgInSc7d1
námořním	námořní	k2eAgInSc7d1
spolkem	spolek	k1gInSc7
a	a	k8xC
peloponéským	peloponéský	k2eAgInSc7d1
spolkem	spolek	k1gInSc7
vyústily	vyústit	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
431	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ve	v	k7c6
vypuknutí	vypuknutí	k1gNnSc6
peloponéské	peloponéský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezprostředních	bezprostřední	k2eAgFnPc2d1
příčin	příčina	k1gFnPc2
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
zásadní	zásadní	k2eAgInSc4d1
význam	význam	k1gInSc4
měly	mít	k5eAaImAgInP
tři	tři	k4xCgInPc1
incidenty	incident	k1gInPc1
popisované	popisovaný	k2eAgFnSc2d1
Thúkýdidem	Thúkýdid	k1gInSc7
a	a	k8xC
Plútarchem	Plútarch	k1gInSc7
<g/>
:	:	kIx,
konflikt	konflikt	k1gInSc1
mezi	mezi	k7c7
Korintem	Korint	k1gInSc7
a	a	k8xC
jeho	jeho	k3xOp3gFnSc7
kolonií	kolonie	k1gFnSc7
Kerkýrou	Kerkýra	k1gFnSc7
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
zasáhly	zasáhnout	k5eAaPmAgFnP
Athény	Athéna	k1gFnPc1
<g/>
,	,	kIx,
odpadnutí	odpadnutí	k1gNnSc1
korintské	korintský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
Poteidaie	Poteidaie	k1gFnSc2
od	od	k7c2
námořního	námořní	k2eAgInSc2d1
spolku	spolek	k1gInSc2
a	a	k8xC
athénské	athénský	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
o	o	k7c4
uvalení	uvalení	k1gNnSc4
obchodních	obchodní	k2eAgFnPc2d1
sankcí	sankce	k1gFnPc2
proti	proti	k7c3
Megaře	Megara	k1gFnSc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
členem	člen	k1gInSc7
peloponéského	peloponéský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc4
byly	být	k5eAaImAgFnP
následně	následně	k6eAd1
členy	člen	k1gMnPc7
peloponéského	peloponéský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
obviněny	obvinit	k5eAaPmNgInP
z	z	k7c2
porušení	porušení	k1gNnSc2
třicetiletého	třicetiletý	k2eAgInSc2d1
míru	mír	k1gInSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
Sparta	Sparta	k1gFnSc1
vyhlásila	vyhlásit	k5eAaPmAgFnS
Athénám	Athéna	k1gFnPc3
válku	válek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
historiků	historik	k1gMnPc2
považuje	považovat	k5eAaImIp3nS
uvedené	uvedený	k2eAgFnPc4d1
příčiny	příčina	k1gFnPc4
za	za	k7c4
pouhé	pouhý	k2eAgFnPc4d1
záminky	záminka	k1gFnPc4
k	k	k7c3
rozpoutání	rozpoutání	k1gNnSc3
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
byl	být	k5eAaImAgInS
totiž	totiž	k9
strach	strach	k1gInSc1
Sparty	Sparta	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gMnPc2
spojenců	spojenec	k1gMnPc2
z	z	k7c2
rostoucího	rostoucí	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgFnPc1d1
válečné	válečný	k2eAgFnPc1d1
akce	akce	k1gFnPc1
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
náhlým	náhlý	k2eAgNnSc7d1
přepadením	přepadení	k1gNnSc7
Platají	Platají	k1gFnSc2
spartskými	spartský	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
Thébany	Thébana	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
431	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Sparťané	Sparťan	k1gMnPc1
chtěli	chtít	k5eAaImAgMnP
Athény	Athéna	k1gFnPc4
zlomit	zlomit	k5eAaPmF
pravidelnými	pravidelný	k2eAgInPc7d1
každoročními	každoroční	k2eAgInPc7d1
vpády	vpád	k1gInPc7
do	do	k7c2
Attiky	Attika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinek	účinek	k1gInSc1
těchto	tento	k3xDgInPc2
nájezdů	nájezd	k1gInPc2
ale	ale	k9
nebyl	být	k5eNaImAgInS
takový	takový	k3xDgInSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
očekávali	očekávat	k5eAaImAgMnP
<g/>
,	,	kIx,
neboť	neboť	k8xC
Periklés	Periklés	k1gInSc1
evakuoval	evakuovat	k5eAaBmAgInS
veškeré	veškerý	k3xTgNnSc4
obyvatelstvo	obyvatelstvo	k1gNnSc4
obce	obec	k1gFnSc2
za	za	k7c4
dlouhé	dlouhý	k2eAgFnPc4d1
zdi	zeď	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
Athéňané	Athéňan	k1gMnPc1
bezpečně	bezpečně	k6eAd1
chráněni	chránit	k5eAaImNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
athénská	athénský	k2eAgFnSc1d1
flotila	flotila	k1gFnSc1
drancovala	drancovat	k5eAaImAgFnS
pobřeží	pobřeží	k1gNnSc3
Peloponésu	Peloponés	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Periklés	Periklésa	k1gFnPc2
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tím	ten	k3xDgMnSc7
Sparťany	Sparťan	k1gMnPc7
vyčerpá	vyčerpat	k5eAaPmIp3nS
a	a	k8xC
přinutí	přinutit	k5eAaPmIp3nS
ke	k	k7c3
kapitulaci	kapitulace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athénská	athénský	k2eAgFnSc1d1
strategie	strategie	k1gFnSc1
byla	být	k5eAaImAgFnS
ale	ale	k8xC
vážně	vážně	k6eAd1
narušena	narušen	k2eAgFnSc1d1
propuknutím	propuknutí	k1gNnSc7
epidemie	epidemie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
430	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
padl	padnout	k5eAaImAgMnS,k5eAaPmAgMnS
za	za	k7c4
oběť	oběť	k1gFnSc4
i	i	k9
athénský	athénský	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
místo	místo	k1gNnSc4
nastoupili	nastoupit	k5eAaPmAgMnP
dva	dva	k4xCgMnPc4
rozdílní	rozdílný	k2eAgMnPc1d1
politikové	politik	k1gMnPc1
<g/>
:	:	kIx,
agresivní	agresivní	k2eAgMnSc1d1
Kleón	Kleón	k1gMnSc1
a	a	k8xC
po	po	k7c6
smíru	smír	k1gInSc6
se	s	k7c7
Spartou	Sparta	k1gFnSc7
volající	volající	k2eAgInSc1d1
Níkiás	Níkiás	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
425	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
po	po	k7c6
zajetí	zajetí	k1gNnSc6
mnoha	mnoho	k4c2
svých	svůj	k3xOyFgMnPc2
občanů	občan	k1gMnPc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Sfaktérie	Sfaktérie	k1gFnSc2
zdála	zdát	k5eAaImAgFnS
Sparta	Sparta	k1gFnSc1
připravená	připravený	k2eAgFnSc1d1
přijmout	přijmout	k5eAaPmF
mír	mír	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
Kleón	Kleón	k1gInSc1
svou	svůj	k3xOyFgFnSc4
neústupností	neústupnost	k1gFnPc2
tuto	tento	k3xDgFnSc4
možnost	možnost	k1gFnSc4
ukončit	ukončit	k5eAaPmF
konflikt	konflikt	k1gInSc4
zhatil	zhatit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
reagovala	reagovat	k5eAaBmAgFnS
tažením	tažení	k1gNnSc7
do	do	k7c2
Thrákie	Thrákie	k1gFnSc2
pod	pod	k7c7
velením	velení	k1gNnSc7
Brásida	Brásid	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
424	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
bylo	být	k5eAaImAgNnS
ohroženo	ohrozit	k5eAaPmNgNnS
zásobování	zásobování	k1gNnSc4
Athén	Athéna	k1gFnPc2
obilím	obilí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Brásida	Brásid	k1gMnSc2
a	a	k8xC
Kleóna	Kleón	k1gMnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Amfipole	Amfipole	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
421	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
uzavřeno	uzavřít	k5eAaPmNgNnS
příměří	příměří	k1gNnSc2
(	(	kIx(
<g/>
Níkiův	Níkiův	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spory	spor	k1gInPc4
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
spolky	spolek	k1gInPc7
nicméně	nicméně	k8xC
nadále	nadále	k6eAd1
přetrvávaly	přetrvávat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Sparta	Sparta	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
prestiž	prestiž	k1gFnSc4
byla	být	k5eAaImAgFnS
oslabená	oslabený	k2eAgFnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
utkat	utkat	k5eAaPmF
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
úhlavním	úhlavní	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
na	na	k7c6
Peloponéském	peloponéský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
Argem	Argos	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
pokoušel	pokoušet	k5eAaImAgMnS
zaujmout	zaujmout	k5eAaPmF
její	její	k3xOp3gNnSc4
postavení	postavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňané	Athéňan	k1gMnPc1
podněcovaní	podněcovaný	k2eAgMnPc1d1
Alkibiadem	Alkibiad	k1gInSc7
využili	využít	k5eAaPmAgMnP
spartského	spartský	k2eAgNnSc2d1
zaneprázdnění	zaneprázdnění	k1gNnSc2
k	k	k7c3
výpravě	výprava	k1gFnSc3
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
v	v	k7c6
letech	léto	k1gNnPc6
415	#num#	k4
až	až	k9
413	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Toto	tento	k3xDgNnSc1
tažení	tažení	k1gNnSc1
ale	ale	k9
skončilo	skončit	k5eAaPmAgNnS
athénskou	athénský	k2eAgFnSc7d1
pohromou	pohroma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsazení	obsazení	k1gNnSc1
Syrákús	Syrákúsa	k1gFnPc2
se	se	k3xPyFc4
nezdařilo	zdařit	k5eNaPmAgNnS
a	a	k8xC
athénské	athénský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
následně	následně	k6eAd1
zcela	zcela	k6eAd1
zničeno	zničen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečná	válečný	k2eAgFnSc1d1
štěstěna	štěstěna	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
začala	začít	k5eAaPmAgFnS
pomalu	pomalu	k6eAd1
přiklánět	přiklánět	k5eAaImF
na	na	k7c4
stranu	strana	k1gFnSc4
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgMnSc3
napomohl	napomoct	k5eAaPmAgInS
i	i	k9
samotný	samotný	k2eAgInSc1d1
Alkibiadés	Alkibiadés	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
přeběhl	přeběhnout	k5eAaPmAgMnS
ke	k	k7c3
Sparťanům	Sparťan	k1gMnPc3
a	a	k8xC
přesvědčil	přesvědčit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
obsadili	obsadit	k5eAaPmAgMnP
osadu	osada	k1gFnSc4
Dekeleia	Dekeleium	k1gNnSc2
v	v	k7c6
Attice	Attika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
opěrného	opěrný	k2eAgInSc2d1
bodu	bod	k1gInSc2
Sparťané	Sparťan	k1gMnPc1
následně	následně	k6eAd1
kontrolovali	kontrolovat	k5eAaImAgMnP
celou	celý	k2eAgFnSc4d1
Attiku	Attika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
perského	perský	k2eAgNnSc2d1
zlata	zlato	k1gNnSc2
vystavěla	vystavět	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
vlastní	vlastní	k2eAgFnSc4d1
mocnou	mocný	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
a	a	k8xC
přenesla	přenést	k5eAaPmAgFnS
válku	válka	k1gFnSc4
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Iónie	Iónie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neustále	neustále	k6eAd1
se	se	k3xPyFc4
stupňující	stupňující	k2eAgInPc1d1
finanční	finanční	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
Athén	Athéna	k1gFnPc2
na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
spojence	spojenka	k1gFnSc6
vedly	vést	k5eAaImAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
stále	stále	k6eAd1
více	hodně	k6eAd2
členů	člen	k1gInPc2
námořního	námořní	k2eAgInSc2d1
spolku	spolek	k1gInSc2
povstávalo	povstávat	k5eAaImAgNnS
proti	proti	k7c3
Athéňanům	Athéňan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezdary	nezdar	k1gInPc4
ve	v	k7c6
válce	válka	k1gFnSc6
přiměly	přimět	k5eAaPmAgInP
aristokratickou	aristokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
v	v	k7c6
Athénách	Athéna	k1gFnPc6
k	k	k7c3
provedení	provedení	k1gNnSc3
oligarchického	oligarchický	k2eAgInSc2d1
převratu	převrat	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
411	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Ten	ten	k3xDgInSc4
byl	být	k5eAaImAgInS
však	však	k9
již	již	k6eAd1
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
potlačen	potlačen	k2eAgMnSc1d1
<g/>
,	,	kIx,
také	také	k9
s	s	k7c7
podporou	podpora	k1gFnSc7
do	do	k7c2
Athén	Athéna	k1gFnPc2
se	se	k3xPyFc4
opět	opět	k6eAd1
navrátivšího	navrátivší	k2eAgNnSc2d1
Alkibiada	Alkibiada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéňanům	Athéňan	k1gMnPc3
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
fázi	fáze	k1gFnSc6
války	válka	k1gFnSc2
začalo	začít	k5eAaPmAgNnS
opět	opět	k6eAd1
dařit	dařit	k5eAaImF
a	a	k8xC
dosáhli	dosáhnout	k5eAaPmAgMnP
několika	několik	k4yIc7
vítězstvích	vítězství	k1gNnPc6
(	(	kIx(
<g/>
Sparta	Sparta	k1gFnSc1
jim	on	k3xPp3gMnPc3
znovu	znovu	k6eAd1
nabídla	nabídnout	k5eAaPmAgFnS
mír	mír	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
novým	nový	k2eAgMnSc7d1
velitelem	velitel	k1gMnSc7
spartské	spartský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
stal	stát	k5eAaPmAgMnS
vojevůdce	vojevůdce	k1gMnSc1
Lýsandros	Lýsandrosa	k1gFnPc2
<g/>
,	,	kIx,
nastal	nastat	k5eAaPmAgInS
ve	v	k7c6
válce	válka	k1gFnSc6
definitivní	definitivní	k2eAgInSc4d1
zvrat	zvrat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
406	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Athény	Athéna	k1gFnPc1
ještě	ještě	k6eAd1
zvítězily	zvítězit	k5eAaPmAgFnP
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Arginúských	Arginúský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
již	již	k6eAd1
v	v	k7c6
dalším	další	k2eAgInSc6d1
roce	rok	k1gInSc6
ale	ale	k8xC
Lýsandros	Lýsandrosa	k1gFnPc2
zničil	zničit	k5eAaPmAgInS
s	s	k7c7
pomocí	pomoc	k1gFnSc7
lsti	lest	k1gFnSc2
athénskou	athénský	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Aigospotamoi	Aigospotamo	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
404	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vyhladověné	vyhladověný	k2eAgFnSc2d1
<g/>
,	,	kIx,
obležené	obležený	k2eAgFnSc2d1
a	a	k8xC
spojenci	spojenec	k1gMnPc7
opuštěné	opuštěný	k2eAgFnSc2d1
Athény	Athéna	k1gFnSc2
konečně	konečně	k6eAd1
kapitulovaly	kapitulovat	k5eAaBmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Mírové	mírový	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
ukončily	ukončit	k5eAaPmAgFnP
athénskou	athénský	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc4
nesměly	smět	k5eNaImAgFnP
udržovat	udržovat	k5eAaImF
flotilu	flotila	k1gFnSc4
a	a	k8xC
musely	muset	k5eAaImAgFnP
poskytovat	poskytovat	k5eAaImF
vojenskou	vojenský	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
Spartě	Sparta	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demokracie	demokracie	k1gFnSc1
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
prospartskou	prospartský	k2eAgFnSc7d1
oligarchickou	oligarchický	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
třiceti	třicet	k4xCc2
tyranů	tyran	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délský	Délský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
byl	být	k5eAaImAgInS
rozpuštěn	rozpustit	k5eAaPmNgInS
a	a	k8xC
někdejší	někdejší	k2eAgMnPc1d1
athénští	athénský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
byli	být	k5eAaImAgMnP
podřízeni	podřídit	k5eAaPmNgMnP
Spartě	Sparta	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
někteří	některý	k3yIgMnPc1
členové	člen	k1gMnPc1
peloponéského	peloponéský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
žádali	žádat	k5eAaImAgMnP
zničení	zničení	k1gNnSc3
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
Sparťané	Sparťan	k1gMnPc1
tento	tento	k3xDgInSc4
návrh	návrh	k1gInSc4
zamítli	zamítnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
neboť	neboť	k8xC
chtěli	chtít	k5eAaImAgMnP
v	v	k7c6
Řecku	Řecko	k1gNnSc6
zachovat	zachovat	k5eAaPmF
rovnováhu	rovnováha	k1gFnSc4
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korint	Korinta	k1gFnPc2
a	a	k8xC
Théby	Théby	k1gFnPc1
se	se	k3xPyFc4
však	však	k9
cítily	cítit	k5eAaImAgFnP
podvedeny	podvést	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cíle	cíl	k1gInSc2
<g/>
,	,	kIx,
jichž	jenž	k3xRgInPc2
toužily	toužit	k5eAaImAgFnP
ve	v	k7c6
válce	válka	k1gFnSc6
dosáhnout	dosáhnout	k5eAaPmF
<g/>
,	,	kIx,
zůstaly	zůstat	k5eAaPmAgFnP
nenaplněny	naplněn	k2eNgFnPc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	on	k3xPp3gMnPc4
přimělo	přimět	k5eAaPmAgNnS
od	od	k7c2
nynějška	nynějšek	k1gInSc2
sledovat	sledovat	k5eAaImF
pouze	pouze	k6eAd1
vlastní	vlastní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
často	často	k6eAd1
v	v	k7c6
rozporu	rozpor	k1gInSc6
se	s	k7c7
zájmy	zájem	k1gInPc7
Sparty	Sparta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Boj	boj	k1gInSc1
o	o	k7c6
hegemonii	hegemonie	k1gFnSc6
<g/>
:	:	kIx,
soupeření	soupeření	k1gNnSc1
Sparty	Sparta	k1gFnSc2
a	a	k8xC
Théb	Théby	k1gFnPc2
</s>
<s>
Sparta	Sparta	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
vítězství	vítězství	k1gNnSc6
nad	nad	k7c7
Athénami	Athéna	k1gFnPc7
stala	stát	k5eAaPmAgFnS
hegemonem	hegemon	k1gMnSc7
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
situace	situace	k1gFnSc2
byla	být	k5eAaImAgNnP
nicméně	nicméně	k8xC
složitá	složitý	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidské	lidský	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
byly	být	k5eAaImAgFnP
vyčerpány	vyčerpat	k5eAaPmNgFnP
dlouhou	dlouhý	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
a	a	k8xC
svornost	svornost	k1gFnSc1
peloponéského	peloponéský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
začala	začít	k5eAaPmAgFnS
ihned	ihned	k6eAd1
po	po	k7c6
porážce	porážka	k1gFnSc6
Athén	Athéna	k1gFnPc2
upadat	upadat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
došlo	dojít	k5eAaPmAgNnS
mezi	mezi	k7c7
Spartou	Sparta	k1gFnSc7
a	a	k8xC
Persií	Persie	k1gFnSc7
v	v	k7c6
letech	léto	k1gNnPc6
400	#num#	k4
až	až	k9
394	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
k	k	k7c3
válce	válka	k1gFnSc3
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
smlouvě	smlouva	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
412	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
z	z	k7c2
obou	dva	k4xCgInPc2
států	stát	k1gInPc2
spojence	spojenec	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
totiž	totiž	k9
Sparťané	Sparťan	k1gMnPc1
zavázali	zavázat	k5eAaPmAgMnP
vydat	vydat	k5eAaPmF
Persii	Persie	k1gFnSc4
maloasijské	maloasijský	k2eAgFnSc2d1
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
však	však	k9
Sparťané	Sparťan	k1gMnPc1
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
vítězství	vítězství	k1gNnSc6
v	v	k7c6
peloponéské	peloponéský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
odmítli	odmítnout	k5eAaPmAgMnP
učinit	učinit	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespokojené	spokojený	k2eNgFnPc4d1
Théby	Théby	k1gFnPc4
a	a	k8xC
Korint	Korint	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
době	doba	k1gFnSc6
spojily	spojit	k5eAaPmAgFnP
s	s	k7c7
tradičními	tradiční	k2eAgMnPc7d1
spartskými	spartský	k2eAgMnPc7d1
nepřáteli	nepřítel	k1gMnPc7
<g/>
:	:	kIx,
Argem	Argos	k1gInSc7
a	a	k8xC
Athénami	Athéna	k1gFnPc7
<g/>
,	,	kIx,
a	a	k8xC
společně	společně	k6eAd1
bojovaly	bojovat	k5eAaImAgInP
proti	proti	k7c3
Sparťanům	Sparťan	k1gMnPc3
v	v	k7c6
korintské	korintský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
letech	léto	k1gNnPc6
395	#num#	k4
až	až	k9
387	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Ta	ten	k3xDgFnSc1
skončila	skončit	k5eAaPmAgFnS
tzv.	tzv.	kA
královským	královský	k2eAgInSc7d1
mírem	mír	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
perským	perský	k2eAgInSc7d1
diktátem	diktát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gMnSc6
bylo	být	k5eAaImAgNnS
potvrzeno	potvrdit	k5eAaPmNgNnS
vedoucí	vedoucí	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
Sparty	Sparta	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ale	ale	k9
ztratila	ztratit	k5eAaPmAgFnS
hodně	hodně	k6eAd1
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
dřívějšího	dřívější	k2eAgInSc2d1
kreditu	kredit	k1gInSc2
osvoboditele	osvoboditel	k1gMnPc4
Helénů	Helén	k1gMnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
míru	mír	k1gInSc6
byla	být	k5eAaImAgFnS
sice	sice	k8xC
všem	všecek	k3xTgFnPc3
obcím	obec	k1gFnPc3
zaručena	zaručen	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
maloasijské	maloasijský	k2eAgFnPc1d1
poleis	poleis	k1gFnPc1
a	a	k8xC
Kypr	Kypr	k1gInSc1
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgInP
do	do	k7c2
rukou	ruka	k1gFnPc2
perského	perský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Théby	Théby	k1gFnPc1
navíc	navíc	k6eAd1
musely	muset	k5eAaImAgFnP
rozpustit	rozpustit	k5eAaPmF
boiótský	boiótský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgNnSc3
Athény	Athéna	k1gFnSc2
obdržely	obdržet	k5eAaPmAgInP
zpět	zpět	k6eAd1
některé	některý	k3yIgInPc1
ostrovy	ostrov	k1gInPc1
v	v	k7c6
Egejském	egejský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Řecko	Řecko	k1gNnSc1
v	v	k7c6
době	doba	k1gFnSc6
thébské	thébský	k2eAgFnSc2d1
hegemonie	hegemonie	k1gFnSc2
<g/>
,	,	kIx,
371	#num#	k4
<g/>
–	–	k?
<g/>
362	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Na	na	k7c6
principu	princip	k1gInSc6
autonomie	autonomie	k1gFnSc2
a	a	k8xC
rovnoprávnosti	rovnoprávnost	k1gFnSc2
stavěla	stavět	k5eAaImAgFnS
myšlenka	myšlenka	k1gFnSc1
koiné	koiné	k1gFnSc2
eiréné	eiréná	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
všeobecný	všeobecný	k2eAgInSc4d1
mír	mír	k1gInSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
vedle	vedle	k7c2
panhelénismu	panhelénismus	k1gInSc2
stala	stát	k5eAaPmAgFnS
nejzásadnější	zásadní	k2eAgFnSc7d3
politickou	politický	k2eAgFnSc7d1
ideou	idea	k1gFnSc7
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myšlenka	myšlenka	k1gFnSc1
věčného	věčný	k2eAgInSc2d1
míru	mír	k1gInSc2
v	v	k7c6
Řecku	Řecko	k1gNnSc6
však	však	k9
vždy	vždy	k6eAd1
selhala	selhat	k5eAaPmAgFnS
na	na	k7c6
nemožnosti	nemožnost	k1gFnSc6
zajistit	zajistit	k5eAaPmF
jeho	jeho	k3xOp3gNnPc4
garantování	garantování	k1gNnPc4
bez	bez	k7c2
existence	existence	k1gFnSc2
silné	silný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
ale	ale	k9
vylučovala	vylučovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královský	královský	k2eAgInSc1d1
mír	mír	k1gInSc1
je	být	k5eAaImIp3nS
mnohými	mnohý	k2eAgMnPc7d1
historiky	historik	k1gMnPc7
pokládán	pokládat	k5eAaImNgInS
za	za	k7c4
první	první	k4xOgInSc4
pokus	pokus	k1gInSc4
o	o	k7c6
realizaci	realizace	k1gFnSc6
koiné	koiné	k1gFnSc2
eiréné	eiréná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
strážce	strážce	k1gMnSc2
královského	královský	k2eAgInSc2d1
míru	mír	k1gInSc2
byla	být	k5eAaImAgFnS
určena	určen	k2eAgFnSc1d1
Sparta	Sparta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
si	se	k3xPyFc3
tak	tak	k6eAd1
chtěla	chtít	k5eAaImAgFnS
zabezpečit	zabezpečit	k5eAaPmF
vlastní	vlastní	k2eAgFnPc4d1
dosažené	dosažený	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlak	tlak	k1gInSc1
ostatních	ostatní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
jí	jíst	k5eAaImIp3nS
však	však	k9
stále	stále	k6eAd1
více	hodně	k6eAd2
nutil	nutit	k5eAaImAgMnS
k	k	k7c3
defenzívě	defenzíva	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
se	se	k3xPyFc4
pomalu	pomalu	k6eAd1
zotavily	zotavit	k5eAaPmAgFnP
z	z	k7c2
porážky	porážka	k1gFnSc2
v	v	k7c6
peloponéské	peloponéský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
377	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
obnovily	obnovit	k5eAaPmAgFnP
námořní	námořní	k2eAgInSc4d1
spolek	spolek	k1gInSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
ve	v	k7c6
značně	značně	k6eAd1
zmenšené	zmenšený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
a	a	k8xC
rovněž	rovněž	k9
na	na	k7c6
demokratičtějším	demokratický	k2eAgInSc6d2
základu	základ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
Thébané	Thébaný	k2eAgFnSc2d1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
Athéňanů	Athéňan	k1gMnPc2
vyhnali	vyhnat	k5eAaPmAgMnP
Spartou	Sparta	k1gFnSc7
dosazenou	dosazený	k2eAgFnSc4d1
aristokratickou	aristokratický	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
a	a	k8xC
obnovili	obnovit	k5eAaPmAgMnP
boiótský	boiótský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
selhaly	selhat	k5eAaPmAgInP
spartské	spartský	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
prosadit	prosadit	k5eAaPmF
všeobecný	všeobecný	k2eAgInSc4d1
mír	mír	k1gInSc4
na	na	k7c6
thébském	thébský	k2eAgInSc6d1
a	a	k8xC
athénském	athénský	k2eAgInSc6d1
odporu	odpor	k1gInSc6
<g/>
,	,	kIx,
vypověděla	vypovědět	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
Thébám	Théby	k1gFnPc3
válku	válek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
371	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
bitvě	bitva	k1gFnSc3
u	u	k7c2
Leukter	Leuktra	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
bylo	být	k5eAaImAgNnS
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nepřemožitelné	přemožitelný	k2eNgNnSc1d1
spartské	spartský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
drtivě	drtivě	k6eAd1
poraženo	porazit	k5eAaPmNgNnS
Thébany	Théban	k1gMnPc7
<g/>
,	,	kIx,
vedenými	vedený	k2eAgMnPc7d1
geniálním	geniální	k2eAgFnPc3d1
Epameinóndou	Epameinónda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
první	první	k4xOgFnSc7
přesvědčivou	přesvědčivý	k2eAgFnSc7d1
porážkou	porážka	k1gFnSc7
Sparťanů	Sparťan	k1gMnPc2
v	v	k7c6
otevřené	otevřený	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
definitivně	definitivně	k6eAd1
skončila	skončit	k5eAaPmAgFnS
spartská	spartský	k2eAgFnSc1d1
hegemonie	hegemonie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následkem	následkem	k7c2
toho	ten	k3xDgInSc2
ztratila	ztratit	k5eAaPmAgFnS
Sparta	Sparta	k1gFnSc1
Messénii	Messénie	k1gFnSc3
a	a	k8xC
upadla	upadnout	k5eAaPmAgFnS
na	na	k7c4
úroveň	úroveň	k1gFnSc4
druhořadého	druhořadý	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Théby	Théby	k1gFnPc1
po	po	k7c6
tomto	tento	k3xDgNnSc6
vítězství	vítězství	k1gNnSc6
získaly	získat	k5eAaPmAgInP
hegemonní	hegemonní	k2eAgInPc1d1
postavení	postavení	k1gNnSc4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
již	již	k6eAd1
po	po	k7c6
několika	několik	k4yIc6
letech	léto	k1gNnPc6
mělo	mít	k5eAaImAgNnS
vzít	vzít	k5eAaPmF
za	za	k7c4
své	svůj	k3xOyFgNnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
odvěcí	odvěký	k2eAgMnPc1d1
nepřátelé	nepřítel	k1gMnPc1
Sparta	Sparta	k1gFnSc1
a	a	k8xC
Athény	Athéna	k1gFnSc2
se	se	k3xPyFc4
cítili	cítit	k5eAaImAgMnP
thébskou	thébský	k2eAgFnSc7d1
hegemonií	hegemonie	k1gFnSc7
ohroženi	ohrozit	k5eAaPmNgMnP
a	a	k8xC
spojili	spojit	k5eAaPmAgMnP
se	se	k3xPyFc4
proto	proto	k8xC
proti	proti	k7c3
Thébám	Théby	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
samotná	samotný	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
u	u	k7c2
Mantineie	Mantineie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
362	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
skončila	skončit	k5eAaPmAgFnS
nerozhodně	rozhodně	k6eNd1
<g/>
,	,	kIx,
thébský	thébský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
Epameinóndás	Epameinóndása	k1gFnPc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
a	a	k8xC
Théby	Théby	k1gFnPc4
tak	tak	k8xC,k8xS
ztratily	ztratit	k5eAaPmAgFnP
svého	svůj	k3xOyFgMnSc4
jedinečného	jedinečný	k2eAgMnSc4d1
vůdce	vůdce	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
nástupce	nástupce	k1gMnSc1
zavlekl	zavleknout	k5eAaPmAgMnS
Thébany	Théban	k1gInPc4
do	do	k7c2
desetiletého	desetiletý	k2eAgInSc2d1
bezvýsledného	bezvýsledný	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
s	s	k7c7
Fókidou	Fókida	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mantineie	Mantineie	k1gFnSc2
už	už	k6eAd1
nebylo	být	k5eNaImAgNnS
žádné	žádný	k3yNgNnSc1
polis	polis	k1gInSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
schopná	schopný	k2eAgFnSc1d1
zaujmout	zaujmout	k5eAaPmF
pozici	pozice	k1gFnSc4
řeckého	řecký	k2eAgMnSc2d1
hegemona	hegemon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
sice	sice	k8xC
byly	být	k5eAaImAgFnP
hlavními	hlavní	k2eAgMnPc7d1
vítězi	vítěz	k1gMnPc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
jejich	jejich	k3xOp3gFnSc1
snaha	snaha	k1gFnSc1
obnovit	obnovit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
někdejší	někdejší	k2eAgFnSc4d1
dominanci	dominance	k1gFnSc4
v	v	k7c6
námořním	námořní	k2eAgInSc6d1
spolku	spolek	k1gInSc6
narazila	narazit	k5eAaPmAgFnS
na	na	k7c4
odpor	odpor	k1gInSc4
jeho	jeho	k3xOp3gMnPc2
členů	člen	k1gMnPc2
a	a	k8xC
skončila	skončit	k5eAaPmAgFnS
trpkou	trpký	k2eAgFnSc7d1
porážkou	porážka	k1gFnSc7
Athén	Athéna	k1gFnPc2
ve	v	k7c6
spojenecké	spojenecký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
letech	léto	k1gNnPc6
(	(	kIx(
<g/>
357	#num#	k4
<g/>
–	–	k?
<g/>
355	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
354	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Thébané	Thébaný	k2eAgFnSc2d1
požádali	požádat	k5eAaPmAgMnP
Filipa	Filip	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonského	makedonský	k2eAgMnSc4d1
o	o	k7c4
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
Fókům	Fók	k1gInPc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
Makedoncům	Makedonec	k1gMnPc3
umožnili	umožnit	k5eAaPmAgMnP
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
řeckých	řecký	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
zatím	zatím	k6eAd1
dosáhly	dosáhnout	k5eAaPmAgInP
Syrakúsy	Syrakús	k1gInPc1
značného	značný	k2eAgInSc2d1
kulturního	kulturní	k2eAgInSc2d1
a	a	k8xC
hospodářského	hospodářský	k2eAgInSc2d1
rozmachu	rozmach	k1gInSc2
a	a	k8xC
vůdčího	vůdčí	k2eAgNnSc2d1
politického	politický	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
za	za	k7c2
vlády	vláda	k1gFnSc2
Dionýsia	Dionýsium	k1gNnSc2
I.	I.	kA
Ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
ale	ale	k9
tato	tento	k3xDgFnSc1
obec	obec	k1gFnSc1
propadla	propadnout	k5eAaPmAgFnS
do	do	k7c2
krvavé	krvavý	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
i	i	k9
ostatní	ostatní	k1gNnSc4
města	město	k1gNnSc2
sicilských	sicilský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
zažívala	zažívat	k5eAaImAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
rozkvět	rozkvět	k1gInSc1
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
už	už	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byli	být	k5eAaImAgMnP
zdejší	zdejší	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
nuceni	nucen	k2eAgMnPc1d1
neustále	neustále	k6eAd1
válčit	válčit	k5eAaImF
s	s	k7c7
Kartáginci	Kartáginec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozmach	rozmach	k1gInSc1
zažívaly	zažívat	k5eAaImAgInP
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
i	i	k9
další	další	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
stojící	stojící	k2eAgFnPc1d1
spíše	spíše	k9
na	na	k7c6
okraji	okraj	k1gInSc6
řeckého	řecký	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
především	především	k6eAd1
Thesálie	Thesálie	k1gFnSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
Iásóna	Iásón	k1gMnSc2
z	z	k7c2
Fer	Fer	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
Korint	Korint	k1gInSc1
a	a	k8xC
Megara	Megara	k1gFnSc1
se	se	k3xPyFc4
zotavily	zotavit	k5eAaPmAgFnP
z	z	k7c2
válek	válka	k1gFnPc2
na	na	k7c6
přelomu	přelom	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
bohatly	bohatnout	k5eAaImAgFnP
z	z	k7c2
obchodu	obchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nástup	nástup	k1gInSc1
Makedonie	Makedonie	k1gFnSc1
–	–	k?
počátek	počátek	k1gInSc4
úpadku	úpadek	k1gInSc2
řeckých	řecký	k2eAgFnPc2d1
poleis	poleis	k1gFnPc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Starověká	starověký	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonský	makedonský	k2eAgInSc1d1
</s>
<s>
Permanentních	permanentní	k2eAgInPc2d1
bojů	boj	k1gInPc2
mezi	mezi	k7c7
Helény	Helén	k1gMnPc7
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
souvisejícího	související	k2eAgNnSc2d1
vnitřního	vnitřní	k2eAgNnSc2d1
oslabení	oslabení	k1gNnSc2
polis	polis	k1gFnSc2
využil	využít	k5eAaPmAgInS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
vzestupu	vzestup	k1gInSc3
severní	severní	k2eAgMnSc1d1
soused	soused	k1gMnSc1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
Makedonie	Makedonie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonci	Makedonec	k1gMnPc1
nebyli	být	k5eNaImAgMnP
původně	původně	k6eAd1
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
Helény	Helén	k1gMnPc4
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
v	v	k7c6
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgMnS
jejich	jejich	k3xOp3gMnSc1
tehdejší	tehdejší	k2eAgMnSc1d1
král	král	k1gMnSc1
uznán	uznán	k2eAgMnSc1d1
za	za	k7c4
Řeka	Řek	k1gMnSc4
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
dovoleno	dovolit	k5eAaPmNgNnS
zúčastnit	zúčastnit	k5eAaPmF
se	se	k3xPyFc4
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zaostalý	zaostalý	k2eAgInSc1d1
stát	stát	k1gInSc1
však	však	k9
stál	stát	k5eAaImAgInS
nadále	nadále	k6eAd1
víceméně	víceméně	k9
stranou	strana	k1gFnSc7
politického	politický	k2eAgInSc2d1
a	a	k8xC
kulturního	kulturní	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
se	se	k3xPyFc4
ale	ale	k8xC
kolem	kolem	k7c2
poloviny	polovina	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
rozhodl	rozhodnout	k5eAaPmAgMnS
změnit	změnit	k5eAaPmF
král	král	k1gMnSc1
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonský	makedonský	k2eAgInSc1d1
<g/>
,	,	kIx,
ambiciózní	ambiciózní	k2eAgMnSc1d1
muž	muž	k1gMnSc1
vzdělaný	vzdělaný	k2eAgMnSc1d1
v	v	k7c6
Thébách	Théby	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgMnS
stálé	stálý	k2eAgNnSc4d1
a	a	k8xC
profesionální	profesionální	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
nejprve	nejprve	k6eAd1
odrazil	odrazit	k5eAaPmAgMnS
útoky	útok	k1gInPc4
kmenů	kmen	k1gInPc2
ze	z	k7c2
severu	sever	k1gInSc2
a	a	k8xC
následně	následně	k6eAd1
zkrotil	zkrotit	k5eAaPmAgMnS
věčně	věčně	k6eAd1
odbojnou	odbojný	k2eAgFnSc4d1
makedonskou	makedonský	k2eAgFnSc4d1
šlechtu	šlechta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsazením	obsazení	k1gNnSc7
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
Amfipolis	Amfipolis	k1gFnSc2
<g/>
,	,	kIx,
Methóné	Methóná	k1gFnSc2
a	a	k8xC
Poteidaia	Poteidaium	k1gNnSc2
získal	získat	k5eAaPmAgMnS
přístup	přístup	k1gInSc4
k	k	k7c3
moři	moře	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovládnutím	ovládnutí	k1gNnSc7
zlatých	zlatá	k1gFnPc2
a	a	k8xC
stříbrných	stříbrný	k2eAgInPc2d1
dolů	dol	k1gInPc2
v	v	k7c6
Makedonii	Makedonie	k1gFnSc6
a	a	k8xC
Thrákii	Thrákie	k1gFnSc6
si	se	k3xPyFc3
pak	pak	k6eAd1
zabezpečil	zabezpečit	k5eAaPmAgMnS
zdroje	zdroj	k1gInSc2
k	k	k7c3
uskutečnění	uskutečnění	k1gNnSc3
svých	svůj	k3xOyFgInPc2
ambiciózních	ambiciózní	k2eAgInPc2d1
plánů	plán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
bojoval	bojovat	k5eAaImAgMnS
proti	proti	k7c3
Fókům	Fók	k1gMnPc3
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
352	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ovládl	ovládnout	k5eAaPmAgInS
Thesálii	Thesálie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
si	se	k3xPyFc3
podmanil	podmanit	k5eAaPmAgMnS
také	také	k9
Thrákii	Thrákie	k1gFnSc4
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
346	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ovládal	ovládat	k5eAaImAgMnS
vše	všechen	k3xTgNnSc4
na	na	k7c4
sever	sever	k1gInSc4
od	od	k7c2
průsmyku	průsmyk	k1gInSc2
Thermopyly	Thermopyly	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgNnSc4
velké	velký	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
využil	využít	k5eAaPmAgInS
k	k	k7c3
podplácení	podplácení	k1gNnSc3
řeckých	řecký	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
a	a	k8xC
dosáhl	dosáhnout	k5eAaPmAgInS
tak	tak	k6eAd1
vytvoření	vytvoření	k1gNnSc4
promakedonské	promakedonský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
v	v	k7c6
každé	každý	k3xTgFnSc6
řecké	řecký	k2eAgFnSc6d1
polis	polis	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
zásah	zásah	k1gInSc4
do	do	k7c2
války	válka	k1gFnSc2
mezi	mezi	k7c7
Thébami	Théby	k1gFnPc7
a	a	k8xC
Fókidou	Fókida	k1gFnSc7
mu	on	k3xPp3gMnSc3
zajistil	zajistit	k5eAaPmAgMnS
respekt	respekt	k1gInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
chopil	chopit	k5eAaPmAgMnS
příležitosti	příležitost	k1gFnPc4
vměšovat	vměšovat	k5eAaImF
se	se	k3xPyFc4
do	do	k7c2
řeckých	řecký	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Athénský	athénský	k2eAgMnSc1d1
řečník	řečník	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
Démosthenés	Démosthenésa	k1gFnPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
rozpoznal	rozpoznat	k5eAaPmAgMnS
nebezpečí	nebezpečí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
Filip	Filip	k1gMnSc1
představoval	představovat	k5eAaImAgMnS
pro	pro	k7c4
svobodu	svoboda	k1gFnSc4
Helénů	Helén	k1gMnPc2
<g/>
,	,	kIx,
burcoval	burcovat	k5eAaImAgMnS
Athéňany	Athéňan	k1gMnPc4
sérií	série	k1gFnSc7
svých	svůj	k3xOyFgFnPc2
slavných	slavný	k2eAgFnPc2d1
řečí	řeč	k1gFnPc2
nazývaných	nazývaný	k2eAgFnPc2d1
filipiky	filipika	k1gFnPc4
do	do	k7c2
boje	boj	k1gInSc2
k	k	k7c3
zastavení	zastavení	k1gNnSc3
makedonského	makedonský	k2eAgInSc2d1
postupu	postup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
a	a	k8xC
Théby	Théby	k1gFnPc1
se	se	k3xPyFc4
z	z	k7c2
Démosthenova	Démosthenův	k2eAgInSc2d1
podnětu	podnět	k1gInSc2
rozhodly	rozhodnout	k5eAaPmAgFnP
čelit	čelit	k5eAaImF
vzrůstajícímu	vzrůstající	k2eAgInSc3d1
Filipovu	Filipův	k2eAgInSc3d1
vlivu	vliv	k1gInSc3
a	a	k8xC
uzavřely	uzavřít	k5eAaPmAgFnP
spojenectví	spojenectví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filip	Filip	k1gMnSc1
ale	ale	k9
udeřil	udeřit	k5eAaPmAgMnS
první	první	k4xOgFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postoupil	postoupit	k5eAaPmAgMnS
do	do	k7c2
Řecka	Řecko	k1gNnSc2
a	a	k8xC
rozdrtil	rozdrtit	k5eAaPmAgInS
Helény	Helén	k1gMnPc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Chairóneie	Chairóneie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
338	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Tato	tento	k3xDgFnSc1
bitva	bitva	k1gFnSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
pokládána	pokládat	k5eAaImNgFnS
za	za	k7c4
okamžik	okamžik	k1gInSc4
počátku	počátek	k1gInSc2
rozkladu	rozklad	k1gInSc2
instituce	instituce	k1gFnSc2
řeckých	řecký	k2eAgInPc2d1
městských	městský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
většina	většina	k1gFnSc1
polis	polis	k1gFnSc1
si	se	k3xPyFc3
uchovala	uchovat	k5eAaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
až	až	k9
do	do	k7c2
dob	doba	k1gFnPc2
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filip	Filip	k1gMnSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
naklonit	naklonit	k5eAaPmF
si	se	k3xPyFc3
Athéňany	Athéňan	k1gMnPc4
pochlebováním	pochlebování	k1gNnSc7
a	a	k8xC
dary	dar	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
tato	tento	k3xDgFnSc1
snaha	snaha	k1gFnSc1
se	se	k3xPyFc4
setkala	setkat	k5eAaPmAgFnS
jen	jen	k9
s	s	k7c7
omezeným	omezený	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
přinutil	přinutit	k5eAaPmAgInS
poražená	poražený	k2eAgNnPc4d1
řecká	řecký	k2eAgNnPc4d1
města	město	k1gNnPc4
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
korintského	korintský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
vůdcem	vůdce	k1gMnSc7
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
vyhlásil	vyhlásit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
úmysl	úmysl	k1gInSc4
napadnout	napadnout	k5eAaPmF
Persii	Persie	k1gFnSc4
za	za	k7c7
účelem	účel	k1gInSc7
osvobození	osvobození	k1gNnSc2
maloasijských	maloasijský	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
kvůli	kvůli	k7c3
pomstě	pomsta	k1gFnSc3
za	za	k7c4
perskou	perský	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
a	a	k8xC
zničení	zničení	k1gNnSc4
řeckých	řecký	k2eAgInPc2d1
chrámů	chrám	k1gInPc2
během	během	k7c2
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
než	než	k8xS
tak	tak	k6eAd1
stihl	stihnout	k5eAaPmAgMnS
učinit	učinit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
ale	ale	k9
v	v	k7c6
roce	rok	k1gInSc6
336	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
úkladně	úkladně	k6eAd1
zavražděn	zavražděn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
a	a	k8xC
společnost	společnost	k1gFnSc1
klasického	klasický	k2eAgInSc2d1
věku	věk	k1gInSc2
</s>
<s>
V	v	k7c6
Periklově	Periklův	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
Athény	Athéna	k1gFnPc1
vyvinuly	vyvinout	k5eAaPmAgFnP
v	v	k7c6
kulturní	kulturní	k2eAgFnSc6d1
metropoli	metropol	k1gFnSc6
celého	celý	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
z	z	k7c2
pokladny	pokladna	k1gFnSc2
námořního	námořní	k2eAgInSc2d1
spolku	spolek	k1gInSc2
umožnily	umožnit	k5eAaPmAgInP
vybudování	vybudování	k1gNnSc4
četných	četný	k2eAgInPc2d1
stavebních	stavební	k2eAgInPc2d1
projektů	projekt	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byly	být	k5eAaImAgFnP
například	například	k6eAd1
Parthenón	Parthenón	k1gInSc4
<g/>
,	,	kIx,
Propylaje	Propylaje	k1gFnSc4
a	a	k8xC
další	další	k2eAgFnPc4d1
skvostné	skvostný	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
na	na	k7c6
Akropoli	Akropole	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnSc2
se	se	k3xPyFc4
rovněž	rovněž	k9
staly	stát	k5eAaPmAgFnP
centrem	centrum	k1gNnSc7
antické	antický	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
jejíž	jejíž	k3xOyRp3gMnPc4
nejvýznamnější	významný	k2eAgMnPc4d3
představitele	představitel	k1gMnPc4
náleželi	náležet	k5eAaImAgMnP
Sókratés	Sókratés	k1gInSc4
<g/>
,	,	kIx,
Platón	platón	k1gInSc4
a	a	k8xC
Aristotelés	Aristotelés	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasická	klasický	k2eAgFnSc1d1
doba	doba	k1gFnSc1
byla	být	k5eAaImAgFnS
obdobím	období	k1gNnSc7
rozkvětu	rozkvět	k1gInSc2
Řecka	Řecko	k1gNnSc2
a	a	k8xC
řecké	řecký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavná	slavný	k2eAgFnSc1d1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jména	jméno	k1gNnSc2
dramatiků	dramatik	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
byli	být	k5eAaImAgMnP
Sofoklés	Sofoklés	k1gInSc4
<g/>
,	,	kIx,
Aischylos	Aischylos	k1gInSc4
nebo	nebo	k8xC
Eurípidés	Eurípidés	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známými	známá	k1gFnPc7
sochaři	sochař	k1gMnPc1
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
byli	být	k5eAaImAgMnP
Feidiás	Feidiása	k1gFnPc2
a	a	k8xC
Polykleitos	Polykleitosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgFnPc2d1
pak	pak	k6eAd1
také	také	k9
lékař	lékař	k1gMnSc1
Hippokrates	Hippokrates	k1gMnSc1
nebo	nebo	k8xC
dějepisci	dějepisec	k1gMnPc1
Hérodotos	Hérodotos	k1gMnSc1
a	a	k8xC
Thúkýdidés	Thúkýdidés	k1gInSc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnPc1
historická	historický	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
byla	být	k5eAaImAgNnP
pozoruhodná	pozoruhodný	k2eAgFnSc1d1
i	i	k8xC
z	z	k7c2
literárního	literární	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démosthenés	Démosthenés	k1gInSc1
byl	být	k5eAaImAgInS
až	až	k9
do	do	k7c2
dob	doba	k1gFnPc2
Cicerona	Cicero	k1gMnSc2
považován	považován	k2eAgMnSc1d1
za	za	k7c4
nepřekonatelného	překonatelný	k2eNgMnSc4d1
řečníka	řečník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působivý	působivý	k2eAgInSc1d1
příklad	příklad	k1gInSc1
kulturního	kulturní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
klasického	klasický	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
představovala	představovat	k5eAaImAgFnS
tragédie	tragédie	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInPc1
počátky	počátek	k1gInPc1
však	však	k9
sahají	sahat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
archaické	archaický	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základním	základní	k2eAgInSc7d1
pilířem	pilíř	k1gInSc7
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
bylo	být	k5eAaImAgNnS
klasické	klasický	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
<g/>
,	,	kIx,
paideia	paideia	k1gFnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
se	se	k3xPyFc4
dostávalo	dostávat	k5eAaImAgNnS
všem	všecek	k3xTgMnPc3
svobodným	svobodný	k2eAgMnPc3d1
Helénům	Helén	k1gMnPc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
odlišovali	odlišovat	k5eAaImAgMnP
od	od	k7c2
barbarů	barbar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasická	klasický	k2eAgFnSc1d1
doba	doba	k1gFnSc1
tak	tak	k6eAd1
položila	položit	k5eAaPmAgFnS
základy	základ	k1gInPc4
západní	západní	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgFnPc6
stavěly	stavět	k5eAaImAgInP
prakticky	prakticky	k6eAd1
všechny	všechen	k3xTgFnPc1
následující	následující	k2eAgFnPc1d1
generace	generace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
politice	politika	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
objevily	objevit	k5eAaPmAgInP
nové	nový	k2eAgInPc1d1
myšlenkové	myšlenkový	k2eAgInPc1d1
proudy	proud	k1gInPc1
a	a	k8xC
ideologie	ideologie	k1gFnSc1
jako	jako	k8xS,k8xC
sofismus	sofismus	k1gInSc1
nebo	nebo	k8xC
demokracie	demokracie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitým	důležitý	k2eAgInSc7d1
předpokladem	předpoklad	k1gInSc7
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
demokracie	demokracie	k1gFnSc2
v	v	k7c6
Athénách	Athéna	k1gFnPc6
byla	být	k5eAaImAgFnS
právě	právě	k6eAd1
existence	existence	k1gFnSc1
městského	městský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
Perikla	Perikla	k1gFnSc4
a	a	k8xC
později	pozdě	k6eAd2
ještě	ještě	k9
za	za	k7c4
Kleóna	Kleón	k1gMnSc4
dosáhla	dosáhnout	k5eAaPmAgFnS
svého	svůj	k3xOyFgMnSc4
vrcholu	vrchol	k1gInSc6
také	také	k9
radikální	radikální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
zapojením	zapojení	k1gNnSc7
nejchudších	chudý	k2eAgFnPc2d3
vrstev	vrstva	k1gFnPc2
do	do	k7c2
řízení	řízení	k1gNnSc2
státních	státní	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
(	(	kIx(
<g/>
počátek	počátek	k1gInSc1
tohoto	tento	k3xDgInSc2
procesu	proces	k1gInSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
již	již	k6eAd1
do	do	k7c2
šedesátých	šedesátý	k4xOgNnPc2
a	a	k8xC
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athénská	athénský	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
s	s	k7c7
plnoprávnou	plnoprávný	k2eAgFnSc7d1
účastí	účast	k1gFnSc7
všech	všecek	k3xTgMnPc2
občanů	občan	k1gMnPc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vzorem	vzor	k1gInSc7
pro	pro	k7c4
budoucnost	budoucnost	k1gFnSc4
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
neubránila	ubránit	k5eNaPmAgNnP
pokušení	pokušení	k1gNnPc1
provádět	provádět	k5eAaImF
agresivní	agresivní	k2eAgFnSc4d1
mocenskou	mocenský	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
v	v	k7c6
mnoha	mnoho	k4c6
směrech	směr	k1gInPc6
jde	jít	k5eAaImIp3nS
jen	jen	k9
obtížně	obtížně	k6eAd1
srovnávat	srovnávat	k5eAaImF
ji	on	k3xPp3gFnSc4
s	s	k7c7
moderní	moderní	k2eAgFnSc7d1
demokracií	demokracie	k1gFnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
jí	on	k3xPp3gFnSc3
například	například	k6eAd1
zcela	zcela	k6eAd1
chyběla	chybět	k5eAaImAgFnS
pro	pro	k7c4
dnešní	dnešní	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
tak	tak	k6eAd1
typická	typický	k2eAgFnSc1d1
dělba	dělba	k1gFnSc1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Helénistická	helénistický	k2eAgFnSc1d1
doba	doba	k1gFnSc1
(	(	kIx(
<g/>
336	#num#	k4
<g/>
–	–	k?
<g/>
146	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Helénismus	helénismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Alexandrovy	Alexandrův	k2eAgInPc1d1
výboje	výboj	k1gInPc1
</s>
<s>
Po	po	k7c6
Filipově	Filipův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
336	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
makedonský	makedonský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
jeho	jeho	k3xOp3gMnSc1
dvacetiletý	dvacetiletý	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
vydal	vydat	k5eAaPmAgInS
uskutečnit	uskutečnit	k5eAaPmF
otcovy	otcův	k2eAgInPc4d1
plány	plán	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Korintu	Korint	k1gInSc6
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgInS
shromážděním	shromáždění	k1gNnSc7
řeckých	řecký	k2eAgFnPc2d1
poleis	poleis	k1gFnPc2
uznat	uznat	k5eAaPmF
za	za	k7c4
vůdce	vůdce	k1gMnPc4
Řeků	Řek	k1gMnPc2
a	a	k8xC
poté	poté	k6eAd1
vyrazil	vyrazit	k5eAaPmAgMnS
na	na	k7c4
sever	sever	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
začal	začít	k5eAaPmAgInS
shromažďovat	shromažďovat	k5eAaImF
vojsko	vojsko	k1gNnSc4
pro	pro	k7c4
tažení	tažení	k1gNnSc4
do	do	k7c2
Persie	Persie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádrem	jádro	k1gNnSc7
tohoto	tento	k3xDgNnSc2
vojska	vojsko	k1gNnSc2
byli	být	k5eAaImAgMnP
tvrdí	tvrdý	k2eAgMnPc1d1
makedonští	makedonský	k2eAgMnPc1d1
horalové	horalové	k?
doplnění	doplnění	k1gNnSc4
o	o	k7c4
dobrovolníky	dobrovolník	k1gMnPc4
z	z	k7c2
různých	různý	k2eAgInPc2d1
koutů	kout	k1gInPc2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
kampaně	kampaň	k1gFnSc2
proti	proti	k7c3
Ilyrům	Ilyr	k1gMnPc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
ale	ale	k9
Alexandr	Alexandr	k1gMnSc1
doslechl	doslechnout	k5eAaPmAgMnS
o	o	k7c6
povstání	povstání	k1gNnSc6
řeckých	řecký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
ihned	ihned	k6eAd1
přemístil	přemístit	k5eAaPmAgInS
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
dobyl	dobýt	k5eAaPmAgMnS
vzbouřené	vzbouřený	k2eAgFnPc4d1
Théby	Théby	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
dal	dát	k5eAaPmAgInS
zničit	zničit	k5eAaPmF
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
<g/>
,	,	kIx,
buď	buď	k8xC
pobil	pobít	k5eAaPmAgMnS
<g/>
,	,	kIx,
nebo	nebo	k8xC
prodal	prodat	k5eAaPmAgMnS
do	do	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
stát	stát	k5eAaImF,k5eAaPmF
pouze	pouze	k6eAd1
chrámy	chrám	k1gInPc1
a	a	k8xC
dům	dům	k1gInSc1
básníka	básník	k1gMnSc2
Pindara	Pindar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
akt	akt	k1gInSc4
v	v	k7c6
sobě	sebe	k3xPyFc6
nesl	nést	k5eAaImAgMnS
poselství	poselství	k1gNnSc3
dvojího	dvojí	k4xRgInSc2
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednak	jednak	k8xC
tím	ten	k3xDgNnSc7
dal	dát	k5eAaPmAgInS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
nebude	být	k5eNaImBp3nS
tolerovat	tolerovat	k5eAaImF
jakýkoli	jakýkoli	k3yIgInSc4
pokus	pokus	k1gInSc4
o	o	k7c4
odpor	odpor	k1gInSc4
ze	z	k7c2
strany	strana	k1gFnSc2
Řeků	Řek	k1gMnPc2
a	a	k8xC
snadno	snadno	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
vypořádá	vypořádat	k5eAaPmIp3nS
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
chtěl	chtít	k5eAaImAgMnS
prokazovat	prokazovat	k5eAaImF
úctu	úcta	k1gFnSc4
a	a	k8xC
respekt	respekt	k1gInSc4
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
kultuře	kultura	k1gFnSc3
<g/>
,	,	kIx,
pokud	pokud	k8xS
ovšem	ovšem	k9
setrvají	setrvat	k5eAaPmIp3nP
v	v	k7c6
poslušnosti	poslušnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
334	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Alexandr	Alexandr	k1gMnSc1
napadl	napadnout	k5eAaPmAgMnS
perskou	perský	k2eAgFnSc4d1
říši	říše	k1gFnSc4
a	a	k8xC
zvítězil	zvítězit	k5eAaPmAgMnS
nad	nad	k7c7
Peršany	peršan	k1gInPc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Gráníku	Gráník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vítězství	vítězství	k1gNnSc1
mu	on	k3xPp3gMnSc3
uvolnilo	uvolnit	k5eAaPmAgNnS
cestu	cesta	k1gFnSc4
k	k	k7c3
iónskému	iónský	k2eAgNnSc3d1
pobřeží	pobřeží	k1gNnSc3
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
mohl	moct	k5eAaImAgInS
osvobodit	osvobodit	k5eAaPmF
všechny	všechen	k3xTgFnPc4
zdejší	zdejší	k2eAgFnPc4d1
řecké	řecký	k2eAgFnPc4d1
obce	obec	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
uspořádání	uspořádání	k1gNnSc6
poměrů	poměr	k1gInPc2
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
postoupil	postoupit	k5eAaPmAgMnS
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
přes	přes	k7c4
Kilíkii	Kilíkie	k1gFnSc4
pronikl	proniknout	k5eAaPmAgInS
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
333	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
porazil	porazit	k5eAaPmAgMnS
perského	perský	k2eAgInSc2d1
velkokrále	velkokrála	k1gFnSc3
Dáreia	Dáreius	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Issu	Issus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
ovládl	ovládnout	k5eAaPmAgMnS
všechna	všechen	k3xTgNnPc4
fénická	fénický	k2eAgNnPc4d1
města	město	k1gNnPc4
a	a	k8xC
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
narazil	narazit	k5eAaPmAgMnS
jen	jen	k9
na	na	k7c4
slabý	slabý	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
Egypťané	Egypťan	k1gMnPc1
ho	on	k3xPp3gMnSc4
vítali	vítat	k5eAaImAgMnP
jako	jako	k8xS,k8xC
osvoboditele	osvoboditel	k1gMnSc4
z	z	k7c2
perského	perský	k2eAgInSc2d1
útlaku	útlak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Amonově	Amonův	k2eAgFnSc6d1
věštírně	věštírna	k1gFnSc6
v	v	k7c6
oaze	oaze	k1gNnSc6
Siwá	Siwá	k1gFnSc1
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
prohlášen	prohlášen	k2eAgMnSc1d1
za	za	k7c4
boha	bůh	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dáreios	Dáreios	k1gInSc1
byl	být	k5eAaImAgInS
ochoten	ochoten	k2eAgInSc1d1
uzavřít	uzavřít	k5eAaPmF
s	s	k7c7
Alexandrem	Alexandr	k1gMnSc7
mír	mír	k1gInSc4
a	a	k8xC
Alexandr	Alexandr	k1gMnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
tak	tak	k6eAd1
mohl	moct	k5eAaImAgInS
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
slávou	sláva	k1gFnSc7
vrátit	vrátit	k5eAaPmF
do	do	k7c2
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
Alexandr	Alexandr	k1gMnSc1
byl	být	k5eAaImAgMnS
odhodlaný	odhodlaný	k2eAgMnSc1d1
dobýt	dobýt	k5eAaPmF
celou	celý	k2eAgFnSc4d1
Persii	Persie	k1gFnSc4
a	a	k8xC
ustanovit	ustanovit	k5eAaPmF
se	s	k7c7
vládcem	vládce	k1gMnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
Sýrie	Sýrie	k1gFnSc2
vytáhl	vytáhnout	k5eAaPmAgMnS
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
do	do	k7c2
Mezopotámie	Mezopotámie	k1gFnSc2
a	a	k8xC
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Gaugamél	Gaugaméla	k1gFnPc2
(	(	kIx(
<g/>
331	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
Dáreia	Dáreius	k1gMnSc2
opět	opět	k6eAd1
porazil	porazit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
pak	pak	k6eAd1
uprchl	uprchnout	k5eAaPmAgMnS
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
však	však	k9
zavražděn	zavraždit	k5eAaPmNgMnS
vlastními	vlastní	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
Alexandr	Alexandr	k1gMnSc1
stal	stát	k5eAaPmAgMnS
pánem	pán	k1gMnSc7
celé	celý	k2eAgFnSc2d1
perské	perský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezitím	mezitím	k6eAd1
Řekové	Řek	k1gMnPc1
obnovili	obnovit	k5eAaPmAgMnP
svoje	svůj	k3xOyFgNnSc4
úsilí	úsilí	k1gNnSc4
o	o	k7c4
svržení	svržení	k1gNnSc4
makedonské	makedonský	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Megalopole	Megalopole	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
331	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ale	ale	k8xC
Alexandrův	Alexandrův	k2eAgMnSc1d1
regent	regent	k1gMnSc1
Antipatros	Antipatrosa	k1gFnPc2
porazil	porazit	k5eAaPmAgMnS
Sparťany	Sparťan	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
již	již	k6eAd1
předtím	předtím	k6eAd1
odmítli	odmítnout	k5eAaPmAgMnP
připojit	připojit	k5eAaPmF
ke	k	k7c3
korintskému	korintský	k2eAgInSc3d1
spolku	spolek	k1gInSc3
a	a	k8xC
uznat	uznat	k5eAaPmF
tak	tak	k6eAd1
makedonskou	makedonský	k2eAgFnSc4d1
nadřazenost	nadřazenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandr	Alexandr	k1gMnSc1
zatím	zatím	k6eAd1
pokračoval	pokračovat	k5eAaImAgMnS
dále	daleko	k6eAd2
na	na	k7c4
východ	východ	k1gInSc4
do	do	k7c2
Baktrie	Baktrie	k1gFnSc2
a	a	k8xC
Sogdiány	Sogdián	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
326	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
překročil	překročit	k5eAaPmAgInS
řeku	řeka	k1gFnSc4
Indus	Indus	k1gInSc1
a	a	k8xC
dosáhl	dosáhnout	k5eAaPmAgMnS
Paňdžábu	Paňdžáb	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
touze	touha	k1gFnSc3
postupovat	postupovat	k5eAaImF
po	po	k7c6
řece	řeka	k1gFnSc6
Ganze	Ganga	k1gFnSc6
až	až	k6eAd1
do	do	k7c2
Bengálska	Bengálsko	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
ale	ale	k8xC
postavila	postavit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
vlastní	vlastní	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyčerpaná	vyčerpaný	k2eAgFnSc1d1
náročným	náročný	k2eAgNnPc3d1
tažením	tažení	k1gNnSc7
odmítla	odmítnout	k5eAaPmAgFnS
jít	jít	k5eAaImF
dále	daleko	k6eAd2
a	a	k8xC
vynutila	vynutit	k5eAaPmAgFnS
si	se	k3xPyFc3
návrat	návrat	k1gInSc4
do	do	k7c2
Persie	Persie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alexandr	Alexandr	k1gMnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
usadil	usadit	k5eAaPmAgInS
v	v	k7c6
Babylóně	Babylón	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
plánoval	plánovat	k5eAaImAgMnS
podniknout	podniknout	k5eAaPmF
tažení	tažení	k1gNnSc4
na	na	k7c4
západ	západ	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
roce	rok	k1gInSc6
323	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Alexandrova	Alexandrův	k2eAgFnSc1d1
říše	říše	k1gFnSc1
se	se	k3xPyFc4
záhy	záhy	k6eAd1
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
ovšem	ovšem	k9
jeho	jeho	k3xOp3gNnSc1
tažení	tažení	k1gNnSc1
trvale	trvale	k6eAd1
změnilo	změnit	k5eAaPmAgNnS
řecký	řecký	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisíce	tisíc	k4xCgInPc1
Řeků	Řek	k1gMnPc2
se	se	k3xPyFc4
už	už	k9
za	za	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
života	život	k1gInSc2
vydaly	vydat	k5eAaPmAgFnP
na	na	k7c4
východ	východ	k1gInSc4
osídlit	osídlit	k5eAaPmF
nová	nový	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
Alexandr	Alexandr	k1gMnSc1
založil	založit	k5eAaPmAgMnS
v	v	k7c6
průběhu	průběh	k1gInSc6
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc1d3
z	z	k7c2
nich	on	k3xPp3gMnPc2
byla	být	k5eAaImAgFnS
Alexandrie	Alexandrie	k1gFnSc1
v	v	k7c6
Egyptě	Egypt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
Sýrii	Sýrie	k1gFnSc6
<g/>
,	,	kIx,
Egyptě	Egypt	k1gInSc6
a	a	k8xC
v	v	k7c6
Baktrii	Baktrie	k1gFnSc6
postupně	postupně	k6eAd1
vznikla	vzniknout	k5eAaPmAgNnP
řecká	řecký	k2eAgNnPc1d1
království	království	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědomosti	vědomost	k1gFnPc1
a	a	k8xC
kultura	kultura	k1gFnSc1
východu	východ	k1gInSc2
a	a	k8xC
západu	západ	k1gInSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
vzájemně	vzájemně	k6eAd1
ovlivňovat	ovlivňovat	k5eAaImF
a	a	k8xC
působení	působení	k1gNnSc4
řecké	řecký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
se	se	k3xPyFc4
rozšířilo	rozšířit	k5eAaPmAgNnS
až	až	k9
na	na	k7c4
hranice	hranice	k1gFnPc4
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecké	řecký	k2eAgFnPc1d1
obce	obec	k1gFnPc1
hrály	hrát	k5eAaImAgFnP
však	však	k9
v	v	k7c6
novém	nový	k2eAgNnSc6d1
uspořádání	uspořádání	k1gNnSc6
světa	svět	k1gInSc2
podřadnou	podřadný	k2eAgFnSc4d1
roli	role	k1gFnSc4
jak	jak	k8xC,k8xS
vůči	vůči	k7c3
helénistickým	helénistický	k2eAgFnPc3d1
říším	říš	k1gFnPc3
<g/>
,	,	kIx,
tak	tak	k6eAd1
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
nově	nova	k1gFnSc3
se	se	k3xPyFc4
formujícím	formující	k2eAgInPc3d1
řeckým	řecký	k2eAgInPc3d1
spolkovým	spolkový	k2eAgInPc3d1
státům	stát	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Helénistický	helénistický	k2eAgInSc1d1
svět	svět	k1gInSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
Alexandrovy	Alexandrův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
Alexandrovu	Alexandrův	k2eAgFnSc4d1
říši	říše	k1gFnSc4
si	se	k3xPyFc3
mezi	mezi	k7c7
sebou	se	k3xPyFc7
rozdělili	rozdělit	k5eAaPmAgMnP
makedonští	makedonský	k2eAgMnPc1d1
generálové	generál	k1gMnPc1
–	–	k?
diadochové	diadoch	k1gMnPc1
(	(	kIx(
<g/>
„	„	k?
<g/>
nástupci	nástupce	k1gMnSc3
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
následných	následný	k2eAgFnPc2d1
válek	válka	k1gFnPc2
známých	známý	k2eAgFnPc2d1
jako	jako	k9
války	válka	k1gFnPc1
diadochů	diadoch	k1gMnPc2
vzešly	vzejít	k5eAaPmAgFnP
tři	tři	k4xCgFnPc4
velké	velký	k2eAgFnPc4d1
říše	říš	k1gFnPc4
<g/>
:	:	kIx,
v	v	k7c6
Přední	přední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
seleukovská	seleukovský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
ovládla	ovládnout	k5eAaPmAgFnS
dynastie	dynastie	k1gFnSc2
Ptolemaiovců	Ptolemaiovec	k1gMnPc2
<g/>
,	,	kIx,
Makedonie	Makedonie	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
mnoha	mnoho	k4c6
zmatcích	zmatek	k1gInPc6
zmocnili	zmocnit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
276	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Antigonovci	Antigonovec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
utužili	utužit	k5eAaPmAgMnP
makedonskou	makedonský	k2eAgFnSc4d1
hegemonii	hegemonie	k1gFnSc4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Alexandrovo	Alexandrův	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
přineslo	přinést	k5eAaPmAgNnS
řeckým	řecký	k2eAgInPc3d1
městským	městský	k2eAgInPc3d1
státům	stát	k1gInPc3
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
závažných	závažný	k2eAgInPc2d1
důsledků	důsledek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horizont	horizont	k1gInSc1
řeckého	řecký	k2eAgInSc2d1
světa	svět	k1gInSc2
se	se	k3xPyFc4
dobytím	dobytí	k1gNnSc7
Persie	Persie	k1gFnSc2
neuvěřitelně	uvěřitelně	k6eNd1
rozšířil	rozšířit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nekonečné	konečný	k2eNgInPc1d1
konflikty	konflikt	k1gInPc1
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
v	v	k7c6
dimenzích	dimenze	k1gFnPc6
tohoto	tento	k3xDgInSc2
světa	svět	k1gInSc2
jevily	jevit	k5eAaImAgFnP
jako	jako	k9
malicherné	malicherný	k2eAgFnPc1d1
a	a	k8xC
nepodstatné	podstatný	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východě	východ	k1gInSc6
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
mnoho	mnoho	k6eAd1
nových	nový	k2eAgNnPc2d1
řeckých	řecký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
systém	systém	k1gInSc1
poleis	poleis	k1gFnSc2
rozšířil	rozšířit	k5eAaPmAgInS
do	do	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
Řeků	Řek	k1gMnPc2
odešlo	odejít	k5eAaPmAgNnS
do	do	k7c2
Alexandrie	Alexandrie	k1gFnSc2
<g/>
,	,	kIx,
Antiochie	Antiochie	k1gFnSc2
a	a	k8xC
mnoha	mnoho	k4c2
jiných	jiný	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
ležících	ležící	k2eAgFnPc2d1
v	v	k7c6
až	až	k9
tak	tak	k6eAd1
vzdálených	vzdálený	k2eAgNnPc6d1
místech	místo	k1gNnPc6
jako	jako	k8xS,k8xC
Baktrie	Baktrie	k1gFnSc1
či	či	k8xC
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
Pákistánu	Pákistán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
založili	založit	k5eAaPmAgMnP
řecko-bakterské	řecko-bakterský	k2eAgNnSc4d1
a	a	k8xC
později	pozdě	k6eAd2
také	také	k9
indo-řecké	indo-řecký	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
přežilo	přežít	k5eAaPmAgNnS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
V	v	k7c6
samotném	samotný	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
ztrácely	ztrácet	k5eAaImAgFnP
poleis	poleis	k1gFnPc1
stále	stále	k6eAd1
více	hodně	k6eAd2
na	na	k7c6
významu	význam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
místo	místo	k1gNnSc4
nastoupily	nastoupit	k5eAaPmAgInP
spolkové	spolkový	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
disponovaly	disponovat	k5eAaBmAgFnP
společným	společný	k2eAgNnSc7d1
občanstvím	občanství	k1gNnSc7
<g/>
,	,	kIx,
státními	státní	k2eAgInPc7d1
orgány	orgán	k1gInPc7
a	a	k8xC
vojenskou	vojenský	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
nich	on	k3xPp3gInPc2
byl	být	k5eAaImAgInS
aitólský	aitólský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
založený	založený	k2eAgInSc1d1
ve	v	k7c6
středním	střední	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Druhý	druhý	k4xOgInSc4
podobný	podobný	k2eAgInSc4d1
útvar	útvar	k1gInSc4
představoval	představovat	k5eAaImAgInS
achajský	achajský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
vzniklý	vzniklý	k2eAgInSc1d1
spojením	spojení	k1gNnSc7
měst	město	k1gNnPc2
na	na	k7c6
severu	sever	k1gInSc6
Peloponésu	Peloponés	k1gInSc2
po	po	k7c6
roce	rok	k1gInSc6
280	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Achajský	Achajský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
aitólský	aitólský	k2eAgMnSc1d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
namířen	namířit	k5eAaPmNgInS
proti	proti	k7c3
makedonské	makedonský	k2eAgFnSc3d1
hegemonii	hegemonie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Achajové	Achajový	k2eAgFnPc1d1
se	se	k3xPyFc4
ale	ale	k9
sami	sám	k3xTgMnPc1
brzy	brzy	k6eAd1
pokusili	pokusit	k5eAaPmAgMnP
ovládnout	ovládnout	k5eAaPmF
Řecko	Řecko	k1gNnSc4
a	a	k8xC
později	pozdě	k6eAd2
dokonce	dokonce	k9
vstupovali	vstupovat	k5eAaImAgMnP
do	do	k7c2
proměnlivých	proměnlivý	k2eAgNnPc2d1
spojenectví	spojenectví	k1gNnPc2
s	s	k7c7
makedonskými	makedonský	k2eAgMnPc7d1
králi	král	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
brzy	brzy	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
dost	dost	k6eAd1
silní	silný	k2eAgMnPc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
tyto	tento	k3xDgInPc4
spolky	spolek	k1gInPc4
dokázali	dokázat	k5eAaPmAgMnP
úplně	úplně	k6eAd1
podmanit	podmanit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Řeckému	řecký	k2eAgNnSc3d1
umění	umění	k1gNnSc3
<g/>
,	,	kIx,
filozofii	filozofie	k1gFnSc3
a	a	k8xC
literatuře	literatura	k1gFnSc3
se	se	k3xPyFc4
v	v	k7c6
helénistických	helénistický	k2eAgInPc6d1
státech	stát	k1gInPc6
podařilo	podařit	k5eAaPmAgNnS
dosáhnout	dosáhnout	k5eAaPmF
nového	nový	k2eAgInSc2d1
rozmachu	rozmach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládnoucí	vládnoucí	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
seleukovské	seleukovský	k2eAgFnSc2d1
a	a	k8xC
ptolemaiovské	ptolemaiovský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
napomohly	napomoct	k5eAaPmAgInP
rozšíření	rozšíření	k1gNnSc4
řecké	řecký	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
a	a	k8xC
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
do	do	k7c2
vzdálených	vzdálený	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
starověkého	starověký	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrem	centrum	k1gNnSc7
helénstva	helénstvo	k1gNnSc2
významnějším	významný	k2eAgInPc3d2
než	než	k8xS
Athény	Athéna	k1gFnPc1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
stalo	stát	k5eAaPmAgNnS
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Ptolemaiovců	Ptolemaiovec	k1gMnPc2
–	–	k?
Alexandrie	Alexandrie	k1gFnSc2
–	–	k?
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
slavnou	slavný	k2eAgFnSc7d1
antickou	antický	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
další	další	k2eAgNnSc4d1
významné	významný	k2eAgNnSc4d1
středisko	středisko	k1gNnSc4
helénistické	helénistický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
se	se	k3xPyFc4
pozvedlo	pozvednout	k5eAaPmAgNnS
maloasijské	maloasijský	k2eAgNnSc1d1
město	město	k1gNnSc1
Pergamon	pergamon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
epocha	epocha	k1gFnSc1
zplodila	zplodit	k5eAaPmAgFnS
výjimečné	výjimečný	k2eAgMnPc4d1
jedince	jedinec	k1gMnPc4
<g/>
,	,	kIx,
jakými	jaký	k3yRgInPc7,k3yIgInPc7,k3yQgInPc7
byli	být	k5eAaImAgMnP
matematikové	matematik	k1gMnPc1
Eukleidés	Eukleidésa	k1gFnPc2
a	a	k8xC
Archimédés	Archimédésa	k1gFnPc2
<g/>
,	,	kIx,
filozofové	filozof	k1gMnPc1
Epikúros	Epikúrosa	k1gFnPc2
a	a	k8xC
Zénón	Zénón	k1gInSc1
z	z	k7c2
Kitia	Kitius	k1gMnSc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
básníci	básník	k1gMnPc1
Apollonios	Apolloniosa	k1gFnPc2
z	z	k7c2
Rhodu	Rhodos	k1gInSc2
či	či	k8xC
Theokritos	Theokritosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Antigonovci	Antigonovec	k1gMnPc1
<g/>
:	:	kIx,
převaha	převaha	k1gFnSc1
Makedonie	Makedonie	k1gFnSc1
</s>
<s>
Alexandrova	Alexandrův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
se	se	k3xPyFc4
řeckým	řecký	k2eAgNnSc7d1
politikům	politik	k1gMnPc3
jevila	jevit	k5eAaImAgNnP
jako	jako	k8xS,k8xC
výtečná	výtečný	k2eAgFnSc1d1
příležitost	příležitost	k1gFnSc1
k	k	k7c3
znovuzískání	znovuzískání	k1gNnSc3
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
vzplanutí	vzplanutí	k1gNnSc3
lamijské	lamijský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
323	#num#	k4
až	až	k9
322	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Počáteční	počáteční	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
však	však	k8xC
byly	být	k5eAaImAgFnP
brzy	brzy	k6eAd1
zmařeny	zmařit	k5eAaPmNgFnP
příchodem	příchod	k1gInSc7
makedonského	makedonský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
z	z	k7c2
východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démosthenés	Démosthenésa	k1gFnPc2
po	po	k7c6
této	tento	k3xDgFnSc6
porážce	porážka	k1gFnSc6
spáchal	spáchat	k5eAaPmAgMnS
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
nad	nad	k7c7
Makedonií	Makedonie	k1gFnSc7
připadla	připadnout	k5eAaPmAgFnS
poté	poté	k6eAd1
Antipatrovu	Antipatrův	k2eAgMnSc3d1
synovi	syn	k1gMnSc3
Kassandrovi	Kassandr	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
po	po	k7c6
několikaletém	několikaletý	k2eAgNnSc6d1
válčení	válčení	k1gNnSc6
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
pánem	pán	k1gMnSc7
většiny	většina	k1gFnSc2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kassandros	Kassandrosa	k1gFnPc2
založil	založit	k5eAaPmAgInS
nové	nový	k2eAgNnSc4d1
makedonské	makedonský	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
v	v	k7c6
Soluni	Soluň	k1gFnSc6
a	a	k8xC
byl	být	k5eAaImAgMnS
celkem	celkem	k6eAd1
schopným	schopný	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
dal	dát	k5eAaPmAgMnS
zavraždit	zavraždit	k5eAaPmF
Alexandrova	Alexandrův	k2eAgMnSc4d1
syna	syn	k1gMnSc4
Alexandra	Alexandr	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
305	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
prohlásit	prohlásit	k5eAaPmF
makedonským	makedonský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kassandrova	Kassandrov	k1gInSc2
moc	moc	k6eAd1
byla	být	k5eAaImAgFnS
ale	ale	k8xC
ohrožována	ohrožovat	k5eAaImNgFnS
Antigonem	Antigon	k1gMnSc7
I.	I.	kA
Jednookým	jednooký	k2eAgFnPc3d1
<g/>
,	,	kIx,
vládcem	vládce	k1gMnSc7
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Řekům	Řek	k1gMnPc3
slíbil	slíbit	k5eAaPmAgMnS
vrátit	vrátit	k5eAaPmF
svobodu	svoboda	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
jej	on	k3xPp3gMnSc4
podpoří	podpořit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
307	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
obsadil	obsadit	k5eAaPmAgMnS
Antigonův	Antigonův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Démétrios	Démétrios	k1gMnSc1
Athény	Athéna	k1gFnSc2
a	a	k8xC
obnovil	obnovit	k5eAaPmAgInS
demokratické	demokratický	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
odstraněno	odstranit	k5eAaPmNgNnS
po	po	k7c6
skončení	skončení	k1gNnSc6
lamijské	lamijský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
v	v	k7c6
roce	rok	k1gInSc6
301	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
koalice	koalice	k1gFnSc2
Kassandra	Kassandr	k1gMnSc2
a	a	k8xC
ostatních	ostatní	k2eAgMnPc2d1
helénistických	helénistický	k2eAgMnPc2d1
králů	král	k1gMnPc2
porazila	porazit	k5eAaPmAgFnS
Antigona	Antigona	k1gFnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Ipsu	Ipsus	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byli	být	k5eAaImAgMnP
Antigonovci	Antigonovec	k1gMnPc7
dočasně	dočasně	k6eAd1
odstaveni	odstaven	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Přesto	přesto	k8xC
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
po	po	k7c6
Kassandrově	Kassandrův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
297	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
právě	právě	k9
Démétrios	Démétrios	k1gMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
chopil	chopit	k5eAaPmAgMnS
makedonského	makedonský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
a	a	k8xC
získal	získat	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Řeckem	Řecko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Makedonie	Makedonie	k1gFnSc2
byl	být	k5eAaImAgInS
ale	ale	k8xC
po	po	k7c6
několika	několik	k4yIc6
letech	léto	k1gNnPc6
vyhnán	vyhnat	k5eAaPmNgInS
spojenými	spojený	k2eAgFnPc7d1
silami	síla	k1gFnPc7
Pyrrha	Pyrrhos	k1gMnSc2
a	a	k8xC
Lýsimacha	Lýsimach	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
285	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
porazil	porazit	k5eAaPmAgMnS
Lýsimachos	Lýsimachos	k1gInSc4
Pyrrha	Pyrrhos	k1gMnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
pánem	pán	k1gMnSc7
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
celého	celý	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
však	však	k9
Lýsimachos	Lýsimachos	k1gMnSc1
poražen	porazit	k5eAaPmNgMnS
a	a	k8xC
zabit	zabít	k5eAaPmNgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kúrúpedia	Kúrúpedium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
vpadli	vpadnout	k5eAaPmAgMnP
do	do	k7c2
Makedonie	Makedonie	k1gFnSc2
ze	z	k7c2
severu	sever	k1gInSc2
Keltové	Kelt	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nastalém	nastalý	k2eAgInSc6d1
chaosu	chaos	k1gInSc6
se	se	k3xPyFc4
dokázal	dokázat	k5eAaPmAgMnS
prosadit	prosadit	k5eAaPmF
Antigonos	Antigonos	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gonatás	Gonatás	k1gInSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Démétria	Démétrium	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
Kelty	Kelt	k1gMnPc4
porazil	porazit	k5eAaPmAgMnS
a	a	k8xC
ustavil	ustavit	k5eAaPmAgMnS
se	se	k3xPyFc4
makedonským	makedonský	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojením	spojení	k1gNnSc7
se	se	k3xPyFc4
Seleukovci	Seleukovec	k1gInSc6
proti	proti	k7c3
Ptolemaiovcům	Ptolemaiovec	k1gMnPc3
dosáhl	dosáhnout	k5eAaPmAgMnS
vyvedení	vyvedení	k1gNnSc4
své	svůj	k3xOyFgFnSc2
dynastie	dynastie	k1gFnSc2
z	z	k7c2
izolace	izolace	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
si	se	k3xPyFc3
zabezpečil	zabezpečit	k5eAaPmAgMnS
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dynastie	dynastie	k1gFnSc1
Antigonovců	Antigonovec	k1gMnPc2
se	se	k3xPyFc4
na	na	k7c6
makedonském	makedonský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
udržela	udržet	k5eAaPmAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
168	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Antigonovská	Antigonovský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
v	v	k7c6
řeckých	řecký	k2eAgFnPc6d1
obcích	obec	k1gFnPc6
byla	být	k5eAaImAgFnS
narušována	narušován	k2eAgFnSc1d1
povstáními	povstání	k1gNnPc7
<g/>
,	,	kIx,
vyvolávanými	vyvolávaný	k2eAgFnPc7d1
protimakedonskými	protimakedonský	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
podporovaných	podporovaný	k2eAgInPc2d1
Ptolemaiovci	Ptolemaiovec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
vehementně	vehementně	k6eAd1
usilovali	usilovat	k5eAaImAgMnP
podkopat	podkopat	k5eAaPmF
moc	moc	k1gFnSc4
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antigonos	Antigonos	k1gMnSc1
umístil	umístit	k5eAaPmAgMnS
makedonskou	makedonský	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
do	do	k7c2
Korintu	Korint	k1gInSc2
<g/>
,	,	kIx,
strategicky	strategicky	k6eAd1
důležitého	důležitý	k2eAgNnSc2d1
místa	místo	k1gNnSc2
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Athény	Athéna	k1gFnPc1
<g/>
,	,	kIx,
Rhodos	Rhodos	k1gInSc1
<g/>
,	,	kIx,
Pergamon	pergamon	k1gInSc1
a	a	k8xC
některé	některý	k3yIgInPc4
další	další	k2eAgInPc4d1
řecké	řecký	k2eAgInPc4d1
státy	stát	k1gInPc4
(	(	kIx(
<g/>
především	především	k6eAd1
achajský	achajský	k2eAgInSc1d1
a	a	k8xC
aitólský	aitólský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
)	)	kIx)
si	se	k3xPyFc3
dokázaly	dokázat	k5eAaPmAgFnP
zachovat	zachovat	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
267	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
přesvědčil	přesvědčit	k5eAaPmAgMnS
Ptolemaios	Ptolemaios	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filadelfos	Filadelfos	k1gInSc1
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
ke	k	k7c3
vzpouře	vzpoura	k1gFnSc3
proti	proti	k7c3
Antigonovi	Antigon	k1gMnSc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
začala	začít	k5eAaPmAgFnS
Chremónidova	Chremónidův	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
podle	podle	k7c2
tehdejšího	tehdejší	k2eAgMnSc2d1
vůdce	vůdce	k1gMnSc2
Athén	Athéna	k1gFnPc2
Chremónida	Chremónid	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
egyptské	egyptský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
byli	být	k5eAaImAgMnP
Řekové	Řek	k1gMnPc1
poraženi	poražen	k2eAgMnPc1d1
a	a	k8xC
Athény	Athéna	k1gFnPc1
ztratily	ztratit	k5eAaPmAgFnP
jak	jak	k9
svoji	svůj	k3xOyFgFnSc4
nezávislost	nezávislost	k1gFnSc4
tak	tak	k6eAd1
demokratické	demokratický	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
porážka	porážka	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
konec	konec	k1gInSc4
působení	působení	k1gNnSc2
Athén	Athéna	k1gFnPc2
jako	jako	k8xS,k8xC
důležitého	důležitý	k2eAgInSc2d1
politického	politický	k2eAgInSc2d1
činitele	činitel	k1gInSc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
Athény	Athéna	k1gFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
největším	veliký	k2eAgNnSc7d3
<g/>
,	,	kIx,
nejbohatším	bohatý	k2eAgNnSc7d3
a	a	k8xC
nejkulturnějším	kulturní	k2eAgNnSc7d3
městem	město	k1gNnSc7
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
258	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Antigonos	Antigonos	k1gInSc1
završil	završit	k5eAaPmAgInS
svoje	svůj	k3xOyFgNnSc4
vítězství	vítězství	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
porazil	porazit	k5eAaPmAgInS
egyptskou	egyptský	k2eAgFnSc4d1
flotilu	flotila	k1gFnSc4
u	u	k7c2
ostrova	ostrov	k1gInSc2
Kóu	Kóu	k1gFnSc2
a	a	k8xC
podřídil	podřídit	k5eAaPmAgInS
si	se	k3xPyFc3
tak	tak	k6eAd1
všechny	všechen	k3xTgInPc1
egejské	egejský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
kromě	kromě	k7c2
Rhodu	Rhodos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aitólský	Aitólský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
makedonským	makedonský	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
a	a	k8xC
získal	získat	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Thébami	Théby	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Antigonos	Antigonos	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
239	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Již	již	k6eAd1
před	před	k7c7
jeho	jeho	k3xOp3gFnSc7
smrtí	smrt	k1gFnSc7
se	se	k3xPyFc4
vzbouřily	vzbouřit	k5eAaPmAgInP
městské	městský	k2eAgInPc1d1
státy	stát	k1gInPc1
sdružené	sdružený	k2eAgInPc1d1
v	v	k7c6
Achajském	Achajský	k2eAgInSc6d1
spolku	spolek	k1gInSc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc7
vůdčí	vůdčí	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
byl	být	k5eAaImAgInS
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
Arátos	Arátos	k1gMnSc1
ze	z	k7c2
Sikyónu	Sikyón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
243	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
získali	získat	k5eAaPmAgMnP
Achajové	Achaj	k1gMnPc1
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
Korint	Korinta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Antigonovu	Antigonův	k2eAgMnSc3d1
synovi	syn	k1gMnSc3
a	a	k8xC
nástupci	nástupce	k1gMnSc3
Démétriovi	Démétrius	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
se	se	k3xPyFc4
následně	následně	k6eAd1
postavila	postavit	k5eAaPmAgFnS
koalice	koalice	k1gFnSc1
achajského	achajský	k2eAgInSc2d1
a	a	k8xC
aitólského	aitólský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
spolky	spolek	k1gInPc4
obvykle	obvykle	k6eAd1
vedly	vést	k5eAaImAgInP
války	válek	k1gInPc1
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
nyní	nyní	k6eAd1
se	se	k3xPyFc4
sjednotily	sjednotit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démétrios	Démétrios	k1gInSc1
s	s	k7c7
nimi	on	k3xPp3gInPc7
válčil	válčit	k5eAaImAgInS
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
odpadnutí	odpadnutí	k1gNnSc1
Épeiru	Épeir	k1gInSc2
a	a	k8xC
neúspěšné	úspěšný	k2eNgInPc4d1
boje	boj	k1gInPc4
proti	proti	k7c3
barbarským	barbarský	k2eAgInPc3d1
Dardánům	Dardán	k1gInPc3
na	na	k7c6
severu	sever	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
Démétrios	Démétrios	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
229	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
padl	padnout	k5eAaPmAgInS,k5eAaImAgInS
<g/>
,	,	kIx,
ještě	ještě	k9
prohloubily	prohloubit	k5eAaPmAgFnP
stávající	stávající	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Achajové	Achaj	k1gMnPc1
byli	být	k5eAaImAgMnP
zcela	zcela	k6eAd1
nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
kontrolovali	kontrolovat	k5eAaImAgMnP
většinu	většina	k1gFnSc4
jižního	jižní	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Athény	Athéna	k1gFnPc1
zůstávaly	zůstávat	k5eAaImAgFnP
stranou	stranou	k6eAd1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Démétrios	Démétrios	k1gInSc4
po	po	k7c6
sobě	sebe	k3xPyFc6
zanechal	zanechat	k5eAaPmAgInS
desetiletého	desetiletý	k2eAgMnSc4d1
syna	syn	k1gMnSc4
Filipa	Filip	k1gMnSc4
<g/>
,	,	kIx,
za	za	k7c4
nějž	jenž	k3xRgMnSc4
měl	mít	k5eAaImAgInS
prozatím	prozatím	k6eAd1
vykonávat	vykonávat	k5eAaImF
vládu	vláda	k1gFnSc4
jako	jako	k8xC,k8xS
regent	regent	k1gMnSc1
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
strýc	strýc	k1gMnSc1
Antigonos	Antigonos	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dósón	Dósón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
konečně	konečně	k6eAd1
porazil	porazit	k5eAaPmAgMnS
Aitóly	Aitól	k1gMnPc4
a	a	k8xC
zabránil	zabránit	k5eAaPmAgMnS
jim	on	k3xPp3gMnPc3
oddělit	oddělit	k5eAaPmF
od	od	k7c2
Makedonie	Makedonie	k1gFnSc2
Thesálii	Thesálie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sparta	Sparta	k1gFnSc1
setrvávala	setrvávat	k5eAaImAgFnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
nepřátelském	přátelský	k2eNgInSc6d1
vztahu	vztah	k1gInSc6
k	k	k7c3
Achajům	Achaj	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
227	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
spartský	spartský	k2eAgMnSc1d1
král	král	k1gMnSc1
Kleomenés	Kleomenésa	k1gFnPc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Achaje	achat	k5eAaImSgInS
drtivě	drtivě	k6eAd1
porazil	porazit	k5eAaPmAgMnS
a	a	k8xC
získal	získat	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
spolkem	spolek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arátos	Arátos	k1gMnSc1
se	se	k3xPyFc4
za	za	k7c2
této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
Dósónem	Dósón	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nad	nad	k7c4
Sparťany	Sparťan	k1gMnPc4
v	v	k7c6
roce	rok	k1gInSc6
222	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zvítězil	zvítězit	k5eAaPmAgMnS
a	a	k8xC
zmocnil	zmocnit	k5eAaPmAgMnS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc2
města	město	k1gNnSc2
–	–	k?
Sparta	Sparta	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
obsazena	obsadit	k5eAaPmNgFnS
cizí	cizí	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Mrak	mrak	k1gInSc1
na	na	k7c6
západě	západ	k1gInSc6
</s>
<s>
V	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
konfliktu	konflikt	k1gInSc3
mezi	mezi	k7c4
Řeky	řeka	k1gFnPc4
a	a	k8xC
Římany	Říman	k1gMnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
Tarenťané	Tarenťan	k1gMnPc1
ohrožovaní	ohrožovaný	k2eAgMnPc1d1
Římany	Říman	k1gMnPc4
si	se	k3xPyFc3
na	na	k7c4
pomoc	pomoc	k1gFnSc4
proti	proti	k7c3
nim	on	k3xPp3gFnPc3
pozvali	pozvat	k5eAaPmAgMnP
épeirského	épeirský	k2eAgMnSc4d1
krále	král	k1gMnSc4
Pyrrha	Pyrrhos	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pyrrhos	Pyrrhos	k1gMnSc1
byl	být	k5eAaImAgMnS
talentovaným	talentovaný	k2eAgMnSc7d1
vojevůdcem	vojevůdce	k1gMnSc7
<g/>
,	,	kIx,
přesto	přesto	k8xC
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgMnSc1d1
Římany	Říman	k1gMnPc7
zdolat	zdolat	k5eAaPmF
a	a	k8xC
po	po	k7c6
několika	několik	k4yIc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
bez	bez	k7c2
úspěchu	úspěch	k1gInSc2
vrátil	vrátit	k5eAaPmAgInS
zpět	zpět	k6eAd1
do	do	k7c2
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tarent	Tarent	k1gInSc4
a	a	k8xC
ostatní	ostatní	k1gNnSc4
jihoitalské	jihoitalský	k2eAgFnSc2d1
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
byly	být	k5eAaImAgInP
poté	poté	k6eAd1
postupně	postupně	k6eAd1
ovládnuty	ovládnut	k2eAgMnPc4d1
Římany	Říman	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
se	se	k3xPyFc4
řecké	řecký	k2eAgFnPc1d1
obce	obec	k1gFnPc1
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
staly	stát	k5eAaPmAgInP
předmětem	předmět	k1gInSc7
sporu	spor	k1gInSc2
mezi	mezi	k7c7
Římany	Říman	k1gMnPc7
a	a	k8xC
Kartáginci	Kartáginec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
první	první	k4xOgFnSc2
punské	punský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
241	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
velká	velká	k1gFnSc1
část	část	k1gFnSc1
Sicílie	Sicílie	k1gFnSc1
ocitla	ocitnout	k5eAaPmAgFnS
pod	pod	k7c7
římskou	římský	k2eAgFnSc7d1
nadvládou	nadvláda	k1gFnSc7
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
Syrákúsy	Syrákúsa	k1gFnPc1
si	se	k3xPyFc3
podržely	podržet	k5eAaPmAgFnP
samostatnost	samostatnost	k1gFnSc4
jako	jako	k8xC,k8xS
římský	římský	k2eAgMnSc1d1
spojenec	spojenec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
punské	punský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c4
vítězství	vítězství	k1gNnSc4
Hannibala	Hannibal	k1gMnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kann	Kanny	k1gFnPc2
<g/>
,	,	kIx,
přešly	přejít	k5eAaPmAgInP
Syrákúsy	Syrákús	k1gInPc1
na	na	k7c4
stranu	strana	k1gFnSc4
Kartága	Kartágo	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
římskou	římský	k2eAgFnSc4d1
odvetu	odveta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
211	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Římané	Říman	k1gMnPc1
Syrákúsy	Syrákúsa	k1gFnSc2
dobyli	dobýt	k5eAaPmAgMnP
a	a	k8xC
definitivně	definitivně	k6eAd1
ukončili	ukončit	k5eAaPmAgMnP
nezávislost	nezávislost	k1gFnSc4
tohoto	tento	k3xDgNnSc2
řeckého	řecký	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
obléhání	obléhání	k1gNnSc6
města	město	k1gNnSc2
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
i	i	k8xC
slavný	slavný	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
Archimédés	Archimédésa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Řecko	Řecko	k1gNnSc1
a	a	k8xC
Makedonie	Makedonie	k1gFnSc1
před	před	k7c7
začátkem	začátek	k1gInSc7
druhé	druhý	k4xOgFnSc2
makedonské	makedonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
221	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zemřel	zemřít	k5eAaPmAgMnS
Antigonos	Antigonos	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dósón	Dósón	k1gInSc4
a	a	k8xC
na	na	k7c4
makedonský	makedonský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
Filip	Filip	k1gMnSc1
V.	V.	kA
přezdívaný	přezdívaný	k2eAgMnSc1d1
„	„	k?
<g/>
milovník	milovník	k1gMnSc1
Řecka	Řecko	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filip	Filip	k1gMnSc1
V.	V.	kA
vstoupil	vstoupit	k5eAaPmAgInS
do	do	k7c2
historie	historie	k1gFnSc2
jako	jako	k8xS,k8xC
poslední	poslední	k2eAgMnSc1d1
makedonský	makedonský	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
ovládal	ovládat	k5eAaImAgInS
Řecko	Řecko	k1gNnSc4
a	a	k8xC
byl	být	k5eAaImAgMnS
schopen	schopen	k2eAgMnSc1d1
bránit	bránit	k5eAaImF
Helény	Helén	k1gMnPc4
před	před	k7c7
rostoucím	rostoucí	k2eAgInSc7d1
mrakem	mrak	k1gInSc7
na	na	k7c6
západě	západ	k1gInSc6
<g/>
:	:	kIx,
stále	stále	k6eAd1
mocnějšími	mocný	k2eAgMnPc7d2
Římany	Říman	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzavřením	uzavření	k1gNnSc7
míru	mír	k1gInSc2
z	z	k7c2
Naupaktu	Naupakt	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
217	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Filip	Filip	k1gMnSc1
ukončil	ukončit	k5eAaPmAgMnS
vítězně	vítězně	k6eAd1
válku	válka	k1gFnSc4
s	s	k7c7
Aitóly	Aitól	k1gMnPc7
a	a	k8xC
kontroloval	kontrolovat	k5eAaImAgMnS
takřka	takřka	k6eAd1
celé	celý	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
Rhodu	Rhodos	k1gInSc2
a	a	k8xC
Pergamu	Pergamos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
Filipova	Filipův	k2eAgInSc2d1
nástupu	nástup	k1gInSc2
na	na	k7c4
trůn	trůn	k1gInSc4
se	se	k3xPyFc4
Římané	Říman	k1gMnPc1
po	po	k7c6
vítězství	vítězství	k1gNnSc6
nad	nad	k7c7
Ilyry	Ilyr	k1gMnPc7
pevně	pevně	k6eAd1
usadili	usadit	k5eAaPmAgMnP
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Jaderského	jaderský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
v	v	k7c6
sousedství	sousedství	k1gNnSc6
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filip	Filip	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
cítil	cítit	k5eAaImAgMnS
Římany	Říman	k1gMnPc4
ohrožován	ohrožován	k2eAgMnSc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
proto	proto	k8xC
spojil	spojit	k5eAaPmAgMnS
s	s	k7c7
římským	římský	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
Kartágem	Kartágo	k1gNnSc7
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
215	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
alianci	aliance	k1gFnSc6
s	s	k7c7
Hannibalem	Hannibal	k1gInSc7
Barkou	Barka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
tím	ten	k3xDgNnSc7
však	však	k9
Římanům	Říman	k1gMnPc3
poskytl	poskytnout	k5eAaPmAgMnS
záminku	záminka	k1gFnSc4
k	k	k7c3
zásahům	zásah	k1gInPc3
do	do	k7c2
řeckých	řecký	k2eAgFnPc2d1
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římu	Řím	k1gInSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
dosud	dosud	k6eAd1
Filipovi	Filip	k1gMnSc3
loajální	loajální	k2eAgFnSc2d1
aitólský	aitólský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
a	a	k8xC
uzavřít	uzavřít	k5eAaPmF
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
nepřítelem	nepřítel	k1gMnSc7
Antigonovců	Antigonovec	k1gMnPc2
<g/>
,	,	kIx,
Pergamem	Pergamon	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
makedonská	makedonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
vypukla	vypuknout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
215	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
skončila	skončit	k5eAaPmAgFnS
nerozhodně	rozhodně	k6eNd1
o	o	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgNnSc7d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
podstatným	podstatný	k2eAgInSc7d1
<g/>
,	,	kIx,
důsledkem	důsledek	k1gInSc7
této	tento	k3xDgFnSc2
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
Makedonie	Makedonie	k1gFnSc1
Římany	Říman	k1gMnPc4
osudově	osudově	k6eAd1
znepřátelila	znepřátelit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
202	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Římané	Říman	k1gMnPc1
porazili	porazit	k5eAaPmAgMnP
Kartágo	Kartágo	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jim	on	k3xPp3gMnPc3
umožnilo	umožnit	k5eAaPmAgNnS
napnout	napnout	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
východním	východní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
200	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zahájili	zahájit	k5eAaPmAgMnP
kvůli	kvůli	k7c3
zanedbatelnému	zanedbatelný	k2eAgInSc3d1
sporu	spor	k1gInSc2
druhou	druhý	k4xOgFnSc4
makedonskou	makedonský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
pravou	pravý	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
byla	být	k5eAaImAgFnS
římská	římský	k2eAgFnSc1d1
touha	touha	k1gFnSc1
po	po	k7c6
pomstě	pomsta	k1gFnSc6
a	a	k8xC
také	také	k9
obava	obava	k1gFnSc1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
Makedonie	Makedonie	k1gFnSc1
nestala	stát	k5eNaPmAgFnS
spojencem	spojenec	k1gMnSc7
Seleukovců	Seleukovec	k1gInPc2
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc2d3
říše	říš	k1gFnSc2
východu	východ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filipovi	Filipův	k2eAgMnPc1d1
řečtí	řecký	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
jej	on	k3xPp3gMnSc4
záhy	záhy	k6eAd1
opustili	opustit	k5eAaPmAgMnP
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
197	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgInS
římským	římský	k2eAgMnSc7d1
prokonzul	prokonzul	k1gMnSc1
Titem	Tit	k1gMnSc7
Quinctiem	Quinctius	k1gMnSc7
Flamininem	Flaminin	k1gInSc7
rozhodně	rozhodně	k6eAd1
poražen	porazit	k5eAaPmNgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kynoskefal	Kynoskefal	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filip	Filip	k1gMnSc1
musel	muset	k5eAaImAgMnS
sice	sice	k8xC
rozpustit	rozpustit	k5eAaPmF
svoji	svůj	k3xOyFgFnSc4
flotilu	flotila	k1gFnSc4
a	a	k8xC
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
římským	římský	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
mírové	mírový	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
byly	být	k5eAaImAgFnP
pro	pro	k7c4
něho	on	k3xPp3gMnSc4
celkově	celkově	k6eAd1
snesitelné	snesitelný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Flamininus	Flamininus	k1gMnSc1
byl	být	k5eAaImAgMnS
obdivovatelem	obdivovatel	k1gMnSc7
řecké	řecký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
umírněným	umírněný	k2eAgMnSc7d1
mužem	muž	k1gMnSc7
<g/>
,	,	kIx,
prohlásil	prohlásit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
196	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c6
isthmických	isthmický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
za	za	k7c4
svobodné	svobodný	k2eAgNnSc4d1
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
Římané	Říman	k1gMnPc1
umístili	umístit	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
posádky	posádka	k1gFnPc4
do	do	k7c2
Korintu	Korint	k1gInSc2
a	a	k8xC
Chalkidy	Chalkida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
svoboda	svoboda	k1gFnSc1
přislíbená	přislíbený	k2eAgFnSc1d1
Římany	Říman	k1gMnPc7
byla	být	k5eAaImAgFnS
iluzí	iluze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
města	město	k1gNnSc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Rhodu	Rhodos	k1gInSc2
byla	být	k5eAaImAgFnS
zapojena	zapojen	k2eAgFnSc1d1
do	do	k7c2
nového	nový	k2eAgInSc2d1
spolku	spolek	k1gInSc2
řízeného	řízený	k2eAgNnSc2d1
Římany	Říman	k1gMnPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
demokracie	demokracie	k1gFnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
obcích	obec	k1gFnPc6
postupně	postupně	k6eAd1
nahrazována	nahrazovat	k5eAaImNgFnS
prořímskými	prořímský	k2eAgInPc7d1
aristokratickými	aristokratický	k2eAgInPc7d1
režimy	režim	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
192	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vypukla	vypuknout	k5eAaPmAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Římem	Řím	k1gInSc7
a	a	k8xC
seleukovskou	seleukovský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gMnSc7
tehdejším	tehdejší	k2eAgMnSc7d1
králem	král	k1gMnSc7
byl	být	k5eAaImAgMnS
ambiciózní	ambiciózní	k2eAgMnSc1d1
Antiochos	Antiochos	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
viděly	vidět	k5eAaImAgFnP
v	v	k7c6
Antiochovi	Antioch	k1gMnSc6
osvoboditele	osvoboditel	k1gMnSc4
od	od	k7c2
Římské	římský	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jejich	jejich	k3xOp3gFnSc1
naděje	naděje	k1gFnSc1
vzaly	vzít	k5eAaPmAgInP
za	za	k7c4
své	svůj	k3xOyFgNnSc4
<g/>
,	,	kIx,
když	když	k8xS
byl	být	k5eAaImAgMnS
Antiochos	Antiochos	k1gMnSc1
poražen	porazit	k5eAaPmNgMnS
Římany	Říman	k1gMnPc7
(	(	kIx(
<g/>
s	s	k7c7
Filipovým	Filipův	k2eAgNnSc7d1
přispěním	přispění	k1gNnSc7
<g/>
)	)	kIx)
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Thermopyl	Thermopyly	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
191	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
V	v	k7c6
průběhu	průběh	k1gInSc6
této	tento	k3xDgFnSc2
války	válka	k1gFnSc2
římské	římský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
poprvé	poprvé	k6eAd1
stanulo	stanout	k5eAaPmAgNnS
na	na	k7c6
půdě	půda	k1gFnSc6
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Antiocha	Antioch	k1gMnSc4
znovu	znovu	k6eAd1
porazilo	porazit	k5eAaPmAgNnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Magnésie	Magnésie	k1gFnSc2
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antiochos	Antiochos	k1gMnSc1
byl	být	k5eAaImAgMnS
následně	následně	k6eAd1
donucen	donutit	k5eAaPmNgMnS
vzdát	vzdát	k5eAaPmF
se	se	k3xPyFc4
veškerého	veškerý	k3xTgNnSc2
území	území	k1gNnSc2
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
v	v	k7c6
mírové	mírový	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
z	z	k7c2
Apameie	Apameie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římané	Říman	k1gMnPc1
tak	tak	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
pouhých	pouhý	k2eAgNnPc2d1
deseti	deset	k4xCc2
let	léto	k1gNnPc2
odstranili	odstranit	k5eAaPmAgMnP
všechny	všechen	k3xTgMnPc4
své	svůj	k3xOyFgMnPc4
potenciální	potenciální	k2eAgMnPc4d1
nepřátele	nepřítel	k1gMnPc4
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
zajistili	zajistit	k5eAaPmAgMnP
si	se	k3xPyFc3
tak	tak	k6eAd1
dominantní	dominantní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
Římané	Říman	k1gMnPc1
ještě	ještě	k9
hlouběji	hluboko	k6eAd2
zasahovali	zasahovat	k5eAaImAgMnP
do	do	k7c2
řecké	řecký	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
,	,	kIx,
chytře	chytro	k6eAd1
využívajíce	využívat	k5eAaImSgMnP
rozporů	rozpor	k1gInPc2
mezi	mezi	k7c7
aristokraty	aristokrat	k1gMnPc7
a	a	k8xC
demokraty	demokrat	k1gMnPc7
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
obcích	obec	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonie	Makedonie	k1gFnSc1
si	se	k3xPyFc3
udržovala	udržovat	k5eAaImAgFnS
svoji	svůj	k3xOyFgFnSc4
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
formálně	formálně	k6eAd1
byla	být	k5eAaImAgFnS
římským	římský	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Filip	Filip	k1gMnSc1
V.	V.	kA
v	v	k7c6
roce	rok	k1gInSc6
179	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
nastoupil	nastoupit	k5eAaPmAgMnS
po	po	k7c6
něm	on	k3xPp3gMnSc6
na	na	k7c6
trůn	trůn	k1gInSc4
jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Perseus	Perseus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
stejně	stejně	k6eAd1
jako	jako	k9
všichni	všechen	k3xTgMnPc1
Antigonovci	Antigonovec	k1gMnPc1
snil	snít	k5eAaImAgMnS
o	o	k7c4
sjednocení	sjednocení	k1gNnSc4
Řeků	Řek	k1gMnPc2
pod	pod	k7c7
makedonskou	makedonský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makedonie	Makedonie	k1gFnSc1
byla	být	k5eAaImAgFnS
však	však	k9
nyní	nyní	k6eAd1
příliš	příliš	k6eAd1
slabá	slabý	k2eAgFnSc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
něco	něco	k3yInSc4
takového	takový	k3xDgNnSc2
uskutečnit	uskutečnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římský	římský	k2eAgMnSc1d1
spojenec	spojenec	k1gMnSc1
<g/>
,	,	kIx,
pergamský	pergamský	k2eAgMnSc1d1
král	král	k1gMnSc1
Eumenés	Eumenésa	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
přesto	přesto	k8xC
Římany	Říman	k1gMnPc4
přesvědčil	přesvědčit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Perseus	Perseus	k1gMnSc1
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
představuje	představovat	k5eAaImIp3nS
hrozbu	hrozba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
řecké	řecký	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
</s>
<s>
Eumenovy	Eumenův	k2eAgFnPc1d1
intriky	intrika	k1gFnPc1
přiměly	přimět	k5eAaPmAgFnP
římský	římský	k2eAgInSc4d1
senát	senát	k1gInSc4
k	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
třetí	třetí	k4xOgFnSc2
makedonské	makedonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
171	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Římané	Říman	k1gMnPc1
přivedli	přivést	k5eAaPmAgMnP
do	do	k7c2
Řecka	Řecko	k1gNnSc2
ohromné	ohromný	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
něž	jenž	k3xRgFnPc4
Makedonci	Makedonec	k1gMnPc1
nebyli	být	k5eNaImAgMnP
rovnocenným	rovnocenný	k2eAgMnSc7d1
soupeřem	soupeř	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perseus	Perseus	k1gMnSc1
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgMnSc1d1
spojit	spojit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
ostatními	ostatní	k2eAgInPc7d1
řeckými	řecký	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
díky	díky	k7c3
mizernému	mizerný	k2eAgNnSc3d1
římskému	římský	k2eAgNnSc3d1
velení	velení	k1gNnSc3
dokázal	dokázat	k5eAaPmAgInS
Římanům	Říman	k1gMnPc3
úspěšné	úspěšný	k2eAgNnSc4d1
vzdorovat	vzdorovat	k5eAaImF
téměř	téměř	k6eAd1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
168	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vyslal	vyslat	k5eAaPmAgMnS
senát	senát	k1gInSc4
do	do	k7c2
Řecka	Řecko	k1gNnSc2
vojevůdce	vojevůdce	k1gMnSc2
Lucia	Lucius	k1gMnSc2
Aemilia	Aemilius	k1gMnSc2
Paulla	Paull	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nakonec	nakonec	k6eAd1
Persea	Perseus	k1gMnSc4
ničivě	ničivě	k6eAd1
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Pydny	Pydna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
makedonský	makedonský	k2eAgMnSc1d1
král	král	k1gMnSc1
byl	být	k5eAaImAgMnS
pak	pak	k6eAd1
zajat	zajat	k2eAgMnSc1d1
a	a	k8xC
potupně	potupně	k6eAd1
veden	vést	k5eAaImNgInS
Římem	Řím	k1gInSc7
během	běh	k1gInSc7
Paullova	Paullův	k2eAgInSc2d1
triumfálního	triumfální	k2eAgInSc2d1
průvodu	průvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antigonovci	Antigonovec	k1gMnPc7
tak	tak	k6eAd1
byli	být	k5eAaImAgMnP
sesazeni	sesadit	k5eAaPmNgMnP
a	a	k8xC
makedonské	makedonský	k2eAgNnSc1d1
království	království	k1gNnSc1
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
čtyř	čtyři	k4xCgInPc2
malých	malý	k2eAgInPc2d1
států	stát	k1gInPc2
závislých	závislý	k2eAgInPc2d1
na	na	k7c6
Římu	Řím	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
řecké	řecký	k2eAgFnPc1d1
obce	obec	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
slíbily	slíbit	k5eAaPmAgFnP
Perseovi	Perseův	k2eAgMnPc1d1
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
tvrdě	tvrdě	k6eAd1
potrestány	potrestán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
římským	římský	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
fakticky	fakticky	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
svoji	svůj	k3xOyFgFnSc4
nezávislost	nezávislost	k1gFnSc4
dokonce	dokonce	k9
i	i	k9
římští	římský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
Pergamon	pergamon	k1gInSc4
a	a	k8xC
Rhodos	Rhodos	k1gInSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
válku	válka	k1gFnSc4
proti	proti	k7c3
Perseovi	Perseus	k1gMnSc3
iniciovali	iniciovat	k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
vedením	vedení	k1gNnSc7
dobrodruha	dobrodruh	k1gMnSc2
zvaného	zvaný	k2eAgInSc2d1
Andriskos	Andriskos	k1gInSc4
Makedonie	Makedonie	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
149	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
naposledy	naposledy	k6eAd1
povstala	povstat	k5eAaPmAgFnS
proti	proti	k7c3
Římanům	Říman	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vzpoura	vzpoura	k1gFnSc1
byla	být	k5eAaImAgFnS
však	však	k9
již	již	k6eAd1
po	po	k7c6
jednom	jeden	k4xCgInSc6
roce	rok	k1gInSc6
zlomena	zlomen	k2eAgFnSc1d1
<g/>
,	,	kIx,
důsledkem	důsledek	k1gInSc7
čehož	což	k3yRnSc2,k3yQnSc2
bylo	být	k5eAaImAgNnS
přeměnění	přeměnění	k1gNnSc3
Makedonie	Makedonie	k1gFnSc2
v	v	k7c4
římskou	římský	k2eAgFnSc4d1
provincii	provincie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osud	osud	k1gInSc4
první	první	k4xOgFnSc2
helénistické	helénistický	k2eAgFnSc2d1
říše	říš	k1gFnSc2
se	se	k3xPyFc4
naplnil	naplnit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římané	Říman	k1gMnPc1
nyní	nyní	k6eAd1
vystupňovali	vystupňovat	k5eAaPmAgMnP
své	svůj	k3xOyFgInPc4
požadavky	požadavek	k1gInPc4
vůči	vůči	k7c3
ostatním	ostatní	k2eAgMnPc3d1
Helénům	Helén	k1gMnPc3
a	a	k8xC
žádali	žádat	k5eAaImAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
achajský	achajský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc1d1
opora	opora	k1gFnSc1
řecké	řecký	k2eAgFnSc2d1
nezávislosti	nezávislost	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
rozpuštěn	rozpuštěn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Achajové	Achaj	k1gMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
a	a	k8xC
vyhlásili	vyhlásit	k5eAaPmAgMnP
Římanům	Říman	k1gMnPc3
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
se	se	k3xPyFc4
přidala	přidat	k5eAaPmAgFnS
na	na	k7c4
stranu	strana	k1gFnSc4
Achajů	Achaj	k1gInPc2
<g/>
,	,	kIx,
dokonce	dokonce	k9
i	i	k9
otroci	otrok	k1gMnPc1
byli	být	k5eAaImAgMnP
osvobozeni	osvobodit	k5eAaPmNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
bojovat	bojovat	k5eAaImF
za	za	k7c4
svobodu	svoboda	k1gFnSc4
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římský	římský	k2eAgMnSc1d1
konzul	konzul	k1gMnSc1
Lucius	Lucius	k1gMnSc1
Mummius	Mummius	k1gMnSc1
postoupil	postoupit	k5eAaPmAgMnS
se	s	k7c7
čtyřmi	čtyři	k4xCgFnPc7
legiemi	legie	k1gFnPc7
z	z	k7c2
Makedonie	Makedonie	k1gFnSc2
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
porazil	porazit	k5eAaPmAgInS
Řeky	Řek	k1gMnPc4
na	na	k7c4
Isthmu	Isthma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odvetu	odvet	k1gInSc6
za	za	k7c2
povstání	povstání	k1gNnSc2
bylo	být	k5eAaImAgNnS
nejvýznamnější	významný	k2eAgNnSc1d3
město	město	k1gNnSc1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
Korint	Korint	k1gMnSc1
<g/>
,	,	kIx,
srovnán	srovnán	k2eAgMnSc1d1
se	s	k7c7
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
definitivně	definitivně	k6eAd1
ukončilo	ukončit	k5eAaPmAgNnS
řecký	řecký	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
Sparty	Sparta	k1gFnSc2
a	a	k8xC
Athén	Athéna	k1gFnPc2
se	se	k3xPyFc4
tak	tak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
146	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
stal	stát	k5eAaPmAgMnS
celý	celý	k2eAgInSc4d1
řecký	řecký	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
římským	římský	k2eAgInSc7d1
protektorátem	protektorát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Římská	římský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
(	(	kIx(
<g/>
146	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
–	–	k?
395	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
</s>
<s>
Mithridatés	Mithridatés	k1gInSc1
a	a	k8xC
římské	římský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
133	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
zemřel	zemřít	k5eAaPmAgMnS
pergamský	pergamský	k2eAgMnSc1d1
král	král	k1gMnSc1
Attalos	Attalos	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
království	království	k1gNnSc1
připadlo	připadnout	k5eAaPmAgNnS
podle	podle	k7c2
jeho	jeho	k3xOp3gFnSc2
závěti	závěť	k1gFnSc2
Římanům	Říman	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řím	Řím	k1gInSc1
tím	ten	k3xDgNnSc7
získal	získat	k5eAaPmAgInS
přímou	přímý	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
egejskými	egejský	k2eAgInPc7d1
ostrovy	ostrov	k1gInPc7
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
si	se	k3xPyFc3
pevnou	pevný	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
na	na	k7c6
asijském	asijský	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečná	konečný	k2eAgFnSc1d1
zkáza	zkáza	k1gFnSc1
Řecka	Řecko	k1gNnSc2
nastala	nastat	k5eAaPmAgFnS
po	po	k7c6
roce	rok	k1gInSc6
88	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pontský	pontský	k2eAgMnSc1d1
král	král	k1gMnSc1
Mithridatés	Mithridatésa	k1gFnPc2
vypověděl	vypovědět	k5eAaPmAgMnS
Římanům	Říman	k1gMnPc3
válku	válka	k1gFnSc4
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
zmasakrovat	zmasakrovat	k5eAaPmF
80	#num#	k4
000	#num#	k4
Římanů	Říman	k1gMnPc2
a	a	k8xC
Italiků	Italik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
Mithridatés	Mithridatés	k1gInSc4
nebyl	být	k5eNaImAgMnS
Helén	Helén	k1gMnSc1
<g/>
,	,	kIx,
mnoho	mnoho	k6eAd1
řeckých	řecký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
svrhlo	svrhnout	k5eAaPmAgNnS
své	svůj	k3xOyFgMnPc4
prořímské	prořímský	k2eAgMnPc4d1
loutkové	loutkový	k2eAgMnPc4d1
vládce	vládce	k1gMnPc4
a	a	k8xC
přidalo	přidat	k5eAaPmAgNnS
se	se	k3xPyFc4
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgInS
vytlačen	vytlačit	k5eAaPmNgInS
z	z	k7c2
Řecka	Řecko	k1gNnSc2
římským	římský	k2eAgMnSc7d1
vojevůdcem	vojevůdce	k1gMnSc7
Luciem	Lucius	k1gMnSc7
Corneliem	Cornelium	k1gNnSc7
Sullou	Sulla	k1gMnSc7
<g/>
,	,	kIx,
stihla	stihnout	k5eAaPmAgFnS
řecké	řecký	k2eAgFnPc4d1
obce	obec	k1gFnPc4
římská	římský	k2eAgFnSc1d1
pomsta	pomsta	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
plně	plně	k6eAd1
nevzpamatovaly	vzpamatovat	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
s	s	k7c7
Římany	Říman	k1gMnPc7
Řecko	Řecko	k1gNnSc1
značně	značně	k6eAd1
vylidnily	vylidnit	k5eAaPmAgInP
a	a	k8xC
demoralizovaly	demoralizovat	k5eAaBmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Helénové	Helén	k1gMnPc1
se	se	k3xPyFc4
již	již	k6eAd1
nikdy	nikdy	k6eAd1
více	hodně	k6eAd2
neodvážili	odvážit	k5eNaPmAgMnP
postavit	postavit	k5eAaPmF
římské	římský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
a	a	k8xC
města	město	k1gNnPc1
jako	jako	k8xS,k8xC
Athény	Athéna	k1gFnPc1
<g/>
,	,	kIx,
Korint	Korint	k1gInSc1
<g/>
,	,	kIx,
Soluň	Soluň	k1gFnSc1
nebo	nebo	k8xC
Patras	Patras	k1gInSc1
už	už	k6eAd1
nikdy	nikdy	k6eAd1
nedosáhla	dosáhnout	k5eNaPmAgFnS
svého	svůj	k3xOyFgNnSc2
někdejšího	někdejší	k2eAgNnSc2d1
bohatství	bohatství	k1gNnSc2
a	a	k8xC
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
pohromy	pohroma	k1gFnPc1
přinesly	přinést	k5eAaPmAgFnP
do	do	k7c2
Řecka	Řecko	k1gNnSc2
římské	římský	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
V	v	k7c6
roce	rok	k1gInSc6
48	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Julius	Julius	k1gMnSc1
Caesar	Caesar	k1gMnSc1
porazil	porazit	k5eAaPmAgMnS
Pompeia	Pompeius	k1gMnSc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Farsálu	Farsál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Filipp	Filippy	k1gInPc2
zvítězil	zvítězit	k5eAaPmAgMnS
Marcus	Marcus	k1gMnSc1
Antonius	Antonius	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
42	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
nad	nad	k7c7
Caesarovými	Caesarův	k2eAgMnPc7d1
vrahy	vrah	k1gMnPc7
Brutem	Brut	k1gMnSc7
a	a	k8xC
Cassiem	Cassius	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
31	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
skončily	skončit	k5eAaPmAgFnP
občanské	občanský	k2eAgFnPc1d1
války	válka	k1gFnPc1
vítězstvím	vítězství	k1gNnSc7
Octaviana	Octavian	k1gMnSc2
nad	nad	k7c7
Marcem	Marce	k1gMnSc7
Antoniem	Antonio	k1gMnSc7
a	a	k8xC
Kleopatrou	Kleopatra	k1gFnSc7
u	u	k7c2
Actia	Actium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Octavianus	Octavianus	k1gInSc1
zřídil	zřídit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
27	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
provincii	provincie	k1gFnSc6
Achaia	Achaius	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
řecké	řecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
přímo	přímo	k6eAd1
připojil	připojit	k5eAaPmAgInS
k	k	k7c3
římskému	římský	k2eAgInSc3d1
státu	stát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
120	#num#	k4
<g/>
,	,	kIx,
červeně	červeně	k6eAd1
zvýrazněna	zvýrazněn	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
Achaea	Achae	k1gInSc2
</s>
<s>
Řecko	Řecko	k1gNnSc1
bylo	být	k5eAaImAgNnS
významnou	významný	k2eAgFnSc7d1
východní	východní	k2eAgFnSc7d1
provincií	provincie	k1gFnSc7
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ostatně	ostatně	k6eAd1
dosvědčuje	dosvědčovat	k5eAaImIp3nS
i	i	k9
označování	označování	k1gNnSc1
kultury	kultura	k1gFnSc2
římského	římský	k2eAgInSc2d1
státu	stát	k1gInSc2
jako	jako	k8xC,k8xS
řecko-římské	řecko-římský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtina	řečtina	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
po	po	k7c4
staletí	staletí	k1gNnPc4
jako	jako	k8xC,k8xS
lingua	lingu	k2eAgNnPc4d1
franca	francum	k1gNnPc4
východu	východ	k1gInSc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
římské	římský	k2eAgFnPc4d1
elity	elita	k1gFnPc4
bylo	být	k5eAaImAgNnS
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
pozdní	pozdní	k2eAgFnSc2d1
antiky	antika	k1gFnSc2
naprosto	naprosto	k6eAd1
přirozené	přirozený	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
kromě	kromě	k7c2
latiny	latina	k1gFnSc2
ovládaly	ovládat	k5eAaImAgInP
také	také	k9
řečtinu	řečtina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
řeckých	řecký	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
a	a	k8xC
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Galénos	Galénosa	k1gFnPc2
<g/>
,	,	kIx,
vykonalo	vykonat	k5eAaPmAgNnS
většinu	většina	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
díla	dílo	k1gNnSc2
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
politici	politik	k1gMnPc1
a	a	k8xC
obzvláště	obzvláště	k6eAd1
císaři	císař	k1gMnPc1
se	se	k3xPyFc4
podíleli	podílet	k5eAaImAgMnP
na	na	k7c6
výstavbě	výstavba	k1gFnSc6
nových	nový	k2eAgFnPc2d1
budov	budova	k1gFnPc2
v	v	k7c6
řeckých	řecký	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
,	,	kIx,
především	především	k6eAd1
na	na	k7c6
athénské	athénský	k2eAgFnSc6d1
agoře	agora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
příliš	příliš	k6eAd1
nezměnil	změnit	k5eNaPmAgInS
od	od	k7c2
dob	doba	k1gFnPc2
před	před	k7c7
Římany	Říman	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Římská	římský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
byla	být	k5eAaImAgFnS
pod	pod	k7c7
silným	silný	k2eAgInSc7d1
řeckým	řecký	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
přiznával	přiznávat	k5eAaImAgMnS
i	i	k8xC
samotný	samotný	k2eAgMnSc1d1
Horatius	Horatius	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
proslulém	proslulý	k2eAgInSc6d1
verši	verš	k1gInSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
Graecia	Graecius	k1gMnSc4
capta	capt	k1gInSc2
ferum	ferum	k1gInSc1
victorem	victor	k1gInSc7
cepit	cepit	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
poražené	poražený	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
přemohlo	přemoct	k5eAaPmAgNnS
hrubého	hrubý	k2eAgMnSc4d1
vítěze	vítěz	k1gMnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homérovy	Homérův	k2eAgInPc1d1
eposy	epos	k1gInPc1
inspirovaly	inspirovat	k5eAaBmAgInP
Vergilia	Vergilius	k1gMnSc4
k	k	k7c3
napsání	napsání	k1gNnSc3
Aeneidy	Aeneida	k1gFnSc2
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
jiní	jiný	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
jako	jako	k9
například	například	k6eAd1
Seneca	Seneca	k1gMnSc1
mladší	mladý	k2eAgFnSc2d2
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
psát	psát	k5eAaImF
řecky	řecky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římská	římský	k2eAgFnSc1d1
nobilita	nobilita	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
pohlížela	pohlížet	k5eAaImAgFnS
na	na	k7c4
Řeky	Řek	k1gMnPc4
jako	jako	k8xS,k8xC
na	na	k7c4
zaostalé	zaostalý	k2eAgMnPc4d1
římské	římský	k2eAgMnPc4d1
poddané	poddaný	k1gMnPc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
hlavním	hlavní	k2eAgMnSc7d1
politickým	politický	k2eAgMnSc7d1
oponentem	oponent	k1gMnSc7
lidí	člověk	k1gMnPc2
jako	jako	k8xC,k8xS
Scipio	Scipio	k6eAd1
Africanus	Africanus	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
inklinovali	inklinovat	k5eAaImAgMnP
ke	k	k7c3
studiu	studio	k1gNnSc3
řecké	řecký	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
a	a	k8xC
hodnotili	hodnotit	k5eAaImAgMnP
řeckou	řecký	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
a	a	k8xC
vědu	věda	k1gFnSc4
jako	jako	k8xS,k8xC
příklad	příklad	k1gInSc4
hodný	hodný	k2eAgInSc4d1
následování	následování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
řeckou	řecký	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
mělo	mít	k5eAaImAgNnS
slabost	slabost	k1gFnSc1
rovněž	rovněž	k9
mnoho	mnoho	k4c4
římských	římský	k2eAgMnPc2d1
císařů	císař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nero	Nero	k1gMnSc1
navštívil	navštívit	k5eAaPmAgMnS
Řecko	Řecko	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
66	#num#	k4
a	a	k8xC
závodil	závodit	k5eAaImAgMnS
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
navzdory	navzdory	k7c3
pravidlům	pravidlo	k1gNnPc3
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
zakazovala	zakazovat	k5eAaImAgNnP
účast	účast	k1gFnSc4
Neřekům	Neřek	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmě	samozřejmě	k6eAd1
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
každé	každý	k3xTgFnSc6
disciplíně	disciplína	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
67	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgMnS
svobodu	svoboda	k1gFnSc4
Řeků	Řek	k1gMnPc2
na	na	k7c6
isthmických	isthmický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Korintu	Korint	k1gInSc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
Flamininus	Flamininus	k1gInSc1
před	před	k7c7
více	hodně	k6eAd2
než	než	k8xS
250	#num#	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nero	Nero	k1gMnSc1
poskytl	poskytnout	k5eAaPmAgMnS
Řekům	Řek	k1gMnPc3
četná	četný	k2eAgNnPc4d1
privilegia	privilegium	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jim	on	k3xPp3gMnPc3
ovšem	ovšem	k9
jeho	jeho	k3xOp3gMnPc1
nástupci	nástupce	k1gMnPc1
zase	zase	k9
odebrali	odebrat	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktéž	Taktéž	k?
Hadrianus	Hadrianus	k1gMnSc1
byl	být	k5eAaImAgMnS
nakloněn	nakloněn	k2eAgMnSc1d1
Řekům	Řek	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předtím	předtím	k6eAd1
než	než	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
císařem	císař	k1gMnSc7
vykonával	vykonávat	k5eAaImAgInS
úřad	úřad	k1gInSc1
archonta	archón	k1gMnSc2
v	v	k7c6
Athénách	Athéna	k1gFnPc6
a	a	k8xC
obohatil	obohatit	k5eAaPmAgMnS
toto	tento	k3xDgNnSc4
město	město	k1gNnSc4
o	o	k7c4
některé	některý	k3yIgInPc4
z	z	k7c2
jeho	jeho	k3xOp3gInPc2
architektonických	architektonický	k2eAgInPc2d1
skvostů	skvost	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecko	Řecko	k1gNnSc1
se	s	k7c7
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
římského	římský	k2eAgInSc2d1
Orientu	Orient	k1gInSc2
dostávalo	dostávat	k5eAaImAgNnS
již	již	k6eAd1
od	od	k7c2
prvních	první	k4xOgNnPc2
staletí	staletí	k1gNnPc2
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
pod	pod	k7c4
stále	stále	k6eAd1
větší	veliký	k2eAgInSc4d2
vliv	vliv	k1gInSc4
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apoštol	apoštol	k1gMnSc1
Pavel	Pavel	k1gMnSc1
kázal	kázat	k5eAaImAgMnS
v	v	k7c6
Korintu	Korint	k1gInSc6
a	a	k8xC
Athénách	Athéna	k1gFnPc6
a	a	k8xC
Řecko	Řecko	k1gNnSc1
se	se	k3xPyFc4
velice	velice	k6eAd1
brzy	brzy	k6eAd1
stalo	stát	k5eAaPmAgNnS
jednou	jednou	k6eAd1
z	z	k7c2
nejvíce	hodně	k6eAd3,k6eAd1
christianizovaných	christianizovaný	k2eAgFnPc2d1
částí	část	k1gFnPc2
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
principátu	principát	k1gInSc2
bylo	být	k5eAaImAgNnS
Řecko	Řecko	k1gNnSc1
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
provincií	provincie	k1gFnPc2
Achaea	Achaeum	k1gNnSc2
<g/>
,	,	kIx,
Macedonia	Macedonium	k1gNnSc2
a	a	k8xC
Epirus	Epirus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
vlády	vláda	k1gFnSc2
Diocletiana	Diocletiana	k1gFnSc1
na	na	k7c6
konci	konec	k1gInSc6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
náleželo	náležet	k5eAaImAgNnS
území	území	k1gNnSc4
Řecka	Řecko	k1gNnSc2
do	do	k7c2
diecéze	diecéze	k1gFnSc2
Moesie	Moesie	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
vládcem	vládce	k1gMnSc7
byl	být	k5eAaImAgMnS
císař	císař	k1gMnSc1
Galerius	Galerius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
Konstantina	Konstantin	k1gMnSc4
Velikého	veliký	k2eAgInSc2d1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
diecézí	diecéze	k1gFnPc2
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
Achaea	Achaeum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Theodosius	Theodosius	k1gInSc1
I.	I.	kA
rozdělil	rozdělit	k5eAaPmAgInS
diecézi	diecéze	k1gFnSc4
Makedonie	Makedonie	k1gFnSc2
do	do	k7c2
provincií	provincie	k1gFnPc2
Creta	Creto	k1gNnSc2
<g/>
,	,	kIx,
Achaea	Achaeum	k1gNnSc2
<g/>
,	,	kIx,
Thessalia	Thessalium	k1gNnSc2
<g/>
,	,	kIx,
Epirus	Epirus	k1gMnSc1
Vetus	Vetus	k1gMnSc1
<g/>
,	,	kIx,
Epirus	Epirus	k1gMnSc1
Nova	novum	k1gNnSc2
a	a	k8xC
Macedonia	Macedonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egejské	egejský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
byly	být	k5eAaImAgInP
jako	jako	k9
provincie	provincie	k1gFnSc2
Insulae	Insula	k1gInSc2
součástí	součást	k1gFnSc7
diecéze	diecéze	k1gFnSc2
Asiana	Asian	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
obzvláště	obzvláště	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
za	za	k7c2
vlády	vláda	k1gFnSc2
Theodosia	Theodosius	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Řecko	Řecko	k1gNnSc1
vystaveno	vystaven	k2eAgNnSc1d1
ničivým	ničivý	k2eAgNnSc7d1
vpádům	vpád	k1gInPc3
germánských	germánský	k2eAgMnPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
Arcadia	Arcadium	k1gNnSc2
pronikli	proniknout	k5eAaPmAgMnP
Vizigóti	Vizigót	k1gMnPc1
vedení	vedení	k1gNnSc2
náčelníkem	náčelník	k1gMnSc7
Alarichem	Alarich	k1gMnSc7
do	do	k7c2
Řecka	Řecko	k1gNnSc2
a	a	k8xC
následně	následně	k6eAd1
vydrancovali	vydrancovat	k5eAaPmAgMnP
Athény	Athéna	k1gFnPc4
<g/>
,	,	kIx,
Korint	Korint	k1gInSc4
a	a	k8xC
Peloponés	Peloponés	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alarich	Alaricha	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
Gótové	Gót	k1gMnPc1
později	pozdě	k6eAd2
opustili	opustit	k5eAaPmAgMnP
Balkán	Balkán	k1gInSc4
a	a	k8xC
vydali	vydat	k5eAaPmAgMnP
se	se	k3xPyFc4
do	do	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
410	#num#	k4
vyplenili	vyplenit	k5eAaPmAgMnP
Řím	Řím	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
Řecko	Řecko	k1gNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
relativně	relativně	k6eAd1
stabilní	stabilní	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
poloviny	polovina	k1gFnSc2
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
země	zem	k1gFnPc1
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
plně	plně	k6eAd1
nezotavila	zotavit	k5eNaPmAgFnS
z	z	k7c2
římského	římský	k2eAgNnSc2d1
dobytí	dobytí	k1gNnSc2
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecko	Řecko	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
chudou	chudý	k2eAgFnSc4d1
a	a	k8xC
málo	málo	k6eAd1
zalidněnou	zalidněný	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
těžiště	těžiště	k1gNnSc1
řecké	řecký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
se	se	k3xPyFc4
postupem	postup	k1gInSc7
doby	doba	k1gFnSc2
přesunulo	přesunout	k5eAaPmAgNnS
do	do	k7c2
Anatolie	Anatolie	k1gFnSc2
a	a	k8xC
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
<g/>
,	,	kIx,
Sparta	Sparta	k1gFnSc1
a	a	k8xC
ostatní	ostatní	k2eAgFnPc1d1
obce	obec	k1gFnPc1
se	se	k3xPyFc4
ocitly	ocitnout	k5eAaPmAgFnP
na	na	k7c4
periferii	periferie	k1gFnSc4
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
jejich	jejich	k3xOp3gFnPc2
soch	socha	k1gFnPc2
a	a	k8xC
umělecké	umělecký	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
bylo	být	k5eAaImAgNnS
odstraněno	odstranit	k5eAaPmNgNnS
a	a	k8xC
převezeno	převézt	k5eAaPmNgNnS
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
zůstávala	zůstávat	k5eAaImAgFnS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejmocnějších	mocný	k2eAgNnPc2d3
center	centrum	k1gNnPc2
křesťanství	křesťanství	k1gNnSc2
v	v	k7c6
pozdně	pozdně	k6eAd1
římském	římský	k2eAgInSc6d1
a	a	k8xC
raně	raně	k6eAd1
byzantském	byzantský	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitý	důležitý	k2eAgInSc1d1
mezník	mezník	k1gInSc1
řeckých	řecký	k2eAgFnPc2d1
i	i	k8xC
římských	římský	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
představuje	představovat	k5eAaImIp3nS
rok	rok	k1gInSc4
330	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Konstantin	Konstantin	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
přesunul	přesunout	k5eAaPmAgMnS
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
říše	říš	k1gFnSc2
do	do	k7c2
řecké	řecký	k2eAgFnSc2d1
osady	osada	k1gFnSc2
Byzantion	Byzantion	k1gNnSc1
při	při	k7c6
Bosporu	Bospor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
symbolizuje	symbolizovat	k5eAaImIp3nS
počátek	počátek	k1gInSc4
nové	nový	k2eAgFnSc2d1
etapy	etapa	k1gFnSc2
římských	římský	k2eAgFnPc2d1
a	a	k8xC
řeckých	řecký	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
–	–	k?
zrod	zrod	k1gInSc4
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Antikes	Antikesa	k1gFnPc2
Griechenland	Griechenlando	k1gNnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Hellenistic	Hellenistice	k1gFnPc2
Greece	Greeec	k1gInSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Roman	Roman	k1gMnSc1
Greece	Greece	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Nejstarší	starý	k2eAgInSc1d3
dochovaný	dochovaný	k2eAgInSc1d1
ucelený	ucelený	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
Kréty	Kréta	k1gFnSc2
z	z	k7c2
obce	obec	k1gFnSc2
Gortyn	Gortyn	k1gInSc1
(	(	kIx(
<g/>
Gortynský	Gortynský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
</s>
<s>
ARRIÁNOS	ARRIÁNOS	kA
<g/>
,	,	kIx,
Tažení	tažení	k1gNnSc4
Alexandra	Alexandr	k1gMnSc2
Makedonského	makedonský	k2eAgMnSc2d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-206-0045-0	80-206-0045-0	k4
</s>
<s>
HÉRODOTOS	Hérodotos	k1gMnSc1
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-200-1192-7	80-200-1192-7	k4
</s>
<s>
PLÚTARCHOS	PLÚTARCHOS	kA
<g/>
,	,	kIx,
Životopisy	životopis	k1gInPc4
slavných	slavný	k2eAgMnPc2d1
Řeků	Řek	k1gMnPc2
a	a	k8xC
Římanů	Říman	k1gMnPc2
I	I	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Arista	Arista	k1gMnSc1
<g/>
,	,	kIx,
Baset	Baset	k1gMnSc1
<g/>
,	,	kIx,
Maitrea	Maitrea	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86410	#num#	k4
<g/>
-	-	kIx~
<g/>
46	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-86410-47-0	978-80-86410-47-0	k4
</s>
<s>
THÚKÝDIDÉS	THÚKÝDIDÉS	kA
<g/>
,	,	kIx,
Dějiny	dějiny	k1gFnPc1
peloponéské	peloponéský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1977	#num#	k4
</s>
<s>
XENOFÓN	XENOFÓN	kA
<g/>
,	,	kIx,
Řecké	řecký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1982	#num#	k4
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
</s>
<s>
BUTTINOVÁ	BUTTINOVÁ	kA
<g/>
,	,	kIx,
Anne-Marie	Anne-Marie	k1gFnSc1
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
776	#num#	k4
až	až	k8xS
338	#num#	k4
př.n.l.	př.n.l.	k?
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7106-566-8	80-7106-566-8	k4
</s>
<s>
BOUZEK	BOUZEK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
ONDŘEJOVÁ	ONDŘEJOVÁ	kA
<g/>
,	,	kIx,
Iva	Iva	k1gFnSc1
<g/>
,	,	kIx,
Periklovo	Periklův	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-204-0083-4	80-204-0083-4	k4
</s>
<s>
FERNAU	FERNAU	kA
<g/>
,	,	kIx,
Joachim	Joachim	k1gInSc1
<g/>
,	,	kIx,
Od	od	k7c2
Olympu	Olymp	k1gInSc2
k	k	k7c3
Akropoli	Akropole	k1gFnSc3
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Brána	brána	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7243-088-2	80-7243-088-2	k4
</s>
<s>
FROLÍKOVÁ	FROLÍKOVÁ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
,	,	kIx,
Politická	politický	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
klasického	klasický	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-200-0285-5	80-200-0285-5	k4
</s>
<s>
GASPAROV	GASPAROV	kA
<g/>
,	,	kIx,
Michail	Michail	k1gMnSc1
<g/>
,	,	kIx,
Na	na	k7c6
počátku	počátek	k1gInSc6
bylo	být	k5eAaImAgNnS
Řecko	Řecko	k1gNnSc1
<g/>
:	:	kIx,
vyprávění	vyprávění	k1gNnSc1
o	o	k7c6
starověké	starověký	k2eAgFnSc6d1
civilizaci	civilizace	k1gFnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Art	Art	k1gFnSc1
slovo	slovo	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-903278-0-X	80-903278-0-X	k4
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
,	,	kIx,
Zrození	zrození	k1gNnSc1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7257-929-0	80-7257-929-0	k4
</s>
<s>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
,	,	kIx,
Klasické	klasický	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7341-404-X	80-7341-404-X	k4
</s>
<s>
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Zrození	zrození	k1gNnSc1
evropské	evropský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Arista	Arista	k1gFnSc1
<g/>
,	,	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86410-37-4	80-86410-37-4	k4
</s>
<s>
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Kolébka	kolébka	k1gFnSc1
demokracie	demokracie	k1gFnSc1
<g/>
:	:	kIx,
dějiny	dějiny	k1gFnPc1
a	a	k8xC
kultura	kultura	k1gFnSc1
klasického	klasický	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
5	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př.n.l.	př.n.l.	k?
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Arista	Arista	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86410-04-8	80-86410-04-8	k4
</s>
<s>
OLIVA	Oliva	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Řecko	Řecko	k1gNnSc1
mezi	mezi	k7c7
Makedonií	Makedonie	k1gFnSc7
a	a	k8xC
Římem	Řím	k1gInSc7
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-200-0435-1	80-200-0435-1	k4
</s>
<s>
Řekové	Řek	k1gMnPc1
a	a	k8xC
rozkvět	rozkvět	k1gInSc1
antiky	antika	k1gFnSc2
<g/>
:	:	kIx,
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
objevy	objev	k1gInPc4
<g/>
,	,	kIx,
rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
Balios	Balios	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-242-0509-2	80-242-0509-2	k4
</s>
<s>
Starověké	starověký	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
<g/>
:	:	kIx,
čítanka	čítanka	k1gFnSc1
k	k	k7c3
dějinám	dějiny	k1gFnPc3
starověku	starověk	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1976	#num#	k4
</s>
<s>
ŚWIDERKOVÁ	ŚWIDERKOVÁ	kA
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
,	,	kIx,
Tvář	tvář	k1gFnSc1
helénistického	helénistický	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1983	#num#	k4
</s>
<s>
VAVŘÍNEK	Vavřínek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
,	,	kIx,
Alexandr	Alexandr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
1967	#num#	k4
</s>
<s>
ZAMAROVSKÝ	ZAMAROVSKÝ	kA
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
<g/>
,	,	kIx,
Řecký	řecký	k2eAgInSc1d1
zázrak	zázrak	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gInSc1
–	–	k?
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
Erika	Erika	k1gFnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-242-0403-7	80-242-0403-7	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Řecka	Řecko	k1gNnSc2
</s>
<s>
Helénismus	helénismus	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
starověkého	starověký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
</s>
<s>
Řecká	řecký	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
</s>
<s>
Starověk	starověk	k1gInSc1
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
</s>
<s>
Starověké	starověký	k2eAgNnSc1d1
řecké	řecký	k2eAgNnSc1d1
právo	právo	k1gNnSc1
</s>
<s>
Starověký	starověký	k2eAgInSc1d1
Řím	Řím	k1gInSc1
</s>
<s>
Umění	umění	k1gNnSc1
starověkého	starověký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Starověké	starověký	k2eAgFnSc2d1
Řecko	Řecko	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Řecké	řecký	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
na	na	k7c6
Antice	antika	k1gFnSc6
</s>
<s>
Ancient	Ancient	k1gMnSc1
Greece	Greece	k1gMnSc1
(	(	kIx(
<g/>
website	websit	k1gInSc5
from	from	k6eAd1
the	the	k?
British	British	k1gInSc1
Museum	museum	k1gNnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ancient	Ancient	k1gMnSc1
Greece	Greece	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ancient	Ancient	k1gMnSc1
Greece	Greece	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Greece	Greece	k1gFnSc1
(	(	kIx(
<g/>
Livius	Livius	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dějiny	dějiny	k1gFnPc1
Řecka	Řecko	k1gNnSc2
Starověké	starověký	k2eAgFnSc6d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Egejská	egejský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Heladská	heladský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Kykladská	kykladský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Mínojská	Mínojský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
•	•	k?
Mykénská	mykénský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
Formování	formování	k1gNnSc2
řecké	řecký	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
</s>
<s>
Temné	temný	k2eAgNnSc1d1
období	období	k1gNnSc1
•	•	k?
Archaické	archaický	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
•	•	k?
Achájové	Achájový	k2eAgFnSc2d1
•	•	k?
Aiólové	Aiólová	k1gFnSc2
•	•	k?
Dórové	Dór	k1gMnPc1
•	•	k?
Iónové	Ión	k1gMnPc1
Klasické	klasický	k2eAgFnSc2d1
Řecko	Řecko	k1gNnSc1
</s>
<s>
Polis	Polis	k1gInSc1
(	(	kIx(
<g/>
Athény	Athéna	k1gFnPc1
•	•	k?
Sparta	Sparta	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tyrannis	Tyrannis	k1gFnSc2
•	•	k?
Řecko-perské	řecko-perský	k2eAgFnSc2d1
války	válka	k1gFnSc2
•	•	k?
Peloponéská	peloponéský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
Helénistická	helénistický	k2eAgFnSc1d1
epocha	epocha	k1gFnSc1
</s>
<s>
Makedonské	makedonský	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
•	•	k?
Diadochové	diadoch	k1gMnPc1
(	(	kIx(
<g/>
Antigonovci	Antigonovec	k1gMnPc1
•	•	k?
Ptolemaiovci	Ptolemaiovec	k1gMnSc3
•	•	k?
Seleukovci	Seleukovec	k1gInSc6
<g/>
)	)	kIx)
•	•	k?
Války	válka	k1gFnSc2
diadochů	diadoch	k1gMnPc2
•	•	k?
Makedonské	makedonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
Římské	římský	k2eAgNnSc4d1
období	období	k1gNnSc4
</s>
<s>
Achaia	Achaia	k1gFnSc1
•	•	k?
Macedonia	Macedonium	k1gNnSc2
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Pozdní	pozdní	k2eAgFnSc1d1
antika	antika	k1gFnSc1
•	•	k?
Herakleiovci	Herakleiovec	k1gInSc6
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Amorejská	Amorejský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Makedonská	makedonský	k2eAgFnSc1d1
dynastie	dynastie	k1gFnSc1
•	•	k?
Komnenovci	Komnenovec	k1gMnSc3
•	•	k?
Angelovci	Angelovec	k1gMnSc3
•	•	k?
Čtvrtá	čtvrtý	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
(	(	kIx(
<g/>
nástupnické	nástupnický	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Nikájské	Nikájský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
•	•	k?
Trapezuntské	trapezuntský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
•	•	k?
Epirský	Epirský	k2eAgInSc4d1
despotát	despotát	k1gInSc4
<g/>
;	;	kIx,
křižácké	křižácký	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Latinské	latinský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
•	•	k?
Soluňské	soluňský	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Athénské	athénský	k2eAgNnSc4d1
vévodství	vévodství	k1gNnSc4
•	•	k?
Achajské	Achajský	k2eAgNnSc4d1
knížectví	knížectví	k1gNnSc4
<g/>
)	)	kIx)
•	•	k?
Palaiologové	Palaiolog	k1gMnPc1
•	•	k?
Pád	Pád	k1gInSc1
Konstantinopole	Konstantinopol	k1gInSc2
•	•	k?
Morejský	Morejský	k2eAgInSc1d1
despotát	despotát	k1gInSc1
Osmanská	osmanský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
</s>
<s>
Armatolové	Armatol	k1gMnPc1
•	•	k?
Kleftové	kleft	k1gMnPc1
•	•	k?
Kodžabašijové	Kodžabašijový	k2eAgNnSc1d1
•	•	k?
Fanarioté	Fanariotý	k2eAgFnSc2d1
•	•	k?
Válka	Válka	k1gMnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
Moderní	moderní	k2eAgInSc4d1
řecký	řecký	k2eAgInSc4d1
stát	stát	k1gInSc4
</s>
<s>
První	první	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Řecké	řecký	k2eAgNnSc4d1
království	království	k1gNnSc4
•	•	k?
Druhá	druhý	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Režim	režim	k1gInSc1
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
•	•	k?
Okupace	okupace	k1gFnSc2
(	(	kIx(
<g/>
ELAS	ELAS	kA
•	•	k?
EDES	EDES	kA
<g/>
)	)	kIx)
•	•	k?
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
junta	junta	k1gFnSc1
•	•	k?
Třetí	třetí	k4xOgFnSc1
helénská	helénský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
Nová	nový	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
•	•	k?
PASOK	PASOK	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Antika	antika	k1gFnSc1
|	|	kIx~
Řecko	Řecko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
131513	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
5937152139995311100002	#num#	k4
</s>
