<s>
Starověké	starověký	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
antické	antický	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
období	období	k1gNnSc4
řeckých	řecký	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
ve	v	k7c6
starověku	starověk	k1gInSc6
<g/>
.	.	kIx.
</s>