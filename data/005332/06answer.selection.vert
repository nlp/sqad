<s>
Jablka	jablko	k1gNnPc1	jablko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vitamín	vitamín	k1gInSc4	vitamín
C	C	kA	C
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
antioxidantů	antioxidant	k1gInPc2	antioxidant
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
DNA	dno	k1gNnPc4	dno
v	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
buňkách	buňka	k1gFnPc6	buňka
a	a	k8xC	a
snižují	snižovat	k5eAaImIp3nP	snižovat
tak	tak	k6eAd1	tak
riziko	riziko	k1gNnSc4	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
množství	množství	k1gNnSc4	množství
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
