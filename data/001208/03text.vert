<s>
Antonín	Antonín	k1gMnSc1	Antonín
Josef	Josef	k1gMnSc1	Josef
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1904	[number]	k4	1904
Letňany	Letňan	k1gMnPc7	Letňan
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1975	[number]	k4	1975
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
třetí	třetí	k4xOgMnSc1	třetí
československý	československý	k2eAgMnSc1d1	československý
komunistický	komunistický	k2eAgMnSc1d1	komunistický
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
šestý	šestý	k4xOgMnSc1	šestý
prezident	prezident	k1gMnSc1	prezident
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Úřadoval	úřadovat	k5eAaImAgMnS	úřadovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1957	[number]	k4	1957
až	až	k9	až
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Novotný	Novotný	k1gMnSc1	Novotný
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
chudé	chudý	k2eAgFnSc2d1	chudá
rodiny	rodina	k1gFnSc2	rodina
zedníka	zedník	k1gMnSc4	zedník
Antonína	Antonín	k1gMnSc4	Antonín
Novotného	Novotný	k1gMnSc4	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
z	z	k7c2	z
Letňan	Letňana	k1gFnPc2	Letňana
a	a	k8xC	a
Barbory	Barbora	k1gFnSc2	Barbora
<g/>
,	,	kIx,	,
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Hlaváčové	Hlaváčová	k1gFnPc1	Hlaváčová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
z	z	k7c2	z
Velkých	velký	k2eAgFnPc2d1	velká
Čakovic	Čakovice	k1gFnPc2	Čakovice
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Letňanech	Letňan	k1gMnPc6	Letňan
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
č.	č.	k?	č.
p.	p.	k?	p.
28	[number]	k4	28
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
bydliště	bydliště	k1gNnSc4	bydliště
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
děd	děd	k1gMnSc1	děd
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
)	)	kIx)	)
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
dostal	dostat	k5eAaPmAgMnS	dostat
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Remigia	Remigius	k1gMnSc2	Remigius
ve	v	k7c6	v
farnosti	farnost	k1gFnSc6	farnost
Čakovice	Čakovice	k1gFnSc2	Čakovice
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Josef	Josef	k1gMnSc1	Josef
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgInS	vyučit
se	se	k3xPyFc4	se
strojním	strojní	k2eAgMnSc7d1	strojní
zámečníkem	zámečník	k1gMnSc7	zámečník
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Boženou	Božena	k1gFnSc7	Božena
Fridrichovou	Fridrichová	k1gFnSc7	Fridrichová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zápisu	zápis	k1gInSc2	zápis
v	v	k7c6	v
matrice	matrika	k1gFnSc6	matrika
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1921	[number]	k4	1921
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prošel	projít	k5eAaPmAgMnS	projít
řadou	řada	k1gFnSc7	řada
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
organizaci	organizace	k1gFnSc6	organizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
tajemník	tajemník	k1gMnSc1	tajemník
KV	KV	kA	KV
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
Hodoníně	Hodonín	k1gInSc6	Hodonín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
dělník	dělník	k1gMnSc1	dělník
ve	v	k7c6	v
vysočanské	vysočanský	k2eAgFnSc6d1	Vysočanská
Včele	včela	k1gFnSc6	včela
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
ilegální	ilegální	k2eAgFnSc6d1	ilegální
činnosti	činnost	k1gFnSc6	činnost
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
vězněn	věznit	k5eAaImNgMnS	věznit
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
Mauthausen-Gusen	Mauthausen-Gusen	k1gInSc1	Mauthausen-Gusen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
tajemníkem	tajemník	k1gMnSc7	tajemník
KV	KV	kA	KV
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
tajemníkem	tajemník	k1gMnSc7	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
zálibou	záliba	k1gFnSc7	záliba
byla	být	k5eAaImAgFnS	být
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
mariáš	mariáš	k1gInSc1	mariáš
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
provozoval	provozovat	k5eAaImAgMnS	provozovat
i	i	k9	i
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rezidenci	rezidence	k1gFnSc6	rezidence
na	na	k7c6	na
Orlíku	orlík	k1gMnSc6	orlík
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
věnoval	věnovat	k5eAaImAgMnS	věnovat
však	však	k9	však
i	i	k9	i
tělovýchovným	tělovýchovný	k2eAgNnSc7d1	tělovýchovné
vystoupením	vystoupení	k1gNnSc7	vystoupení
(	(	kIx(	(
<g/>
československá	československý	k2eAgFnSc1d1	Československá
spartakiáda	spartakiáda	k1gFnSc1	spartakiáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Antonín	Antonín	k1gMnSc1	Antonín
Zápotocký	Zápotocký	k1gMnSc1	Zápotocký
čekalo	čekat	k5eAaImAgNnS	čekat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
bude	být	k5eAaImBp3nS	být
Viliam	Viliam	k1gMnSc1	Viliam
Široký	Široký	k1gMnSc1	Široký
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
už	už	k6eAd1	už
probíhaly	probíhat	k5eAaImAgFnP	probíhat
přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
volbu	volba	k1gFnSc4	volba
Širokého	Širokého	k2eAgFnPc3d1	Širokého
<g/>
,	,	kIx,	,
tiskly	tisknout	k5eAaImAgInP	tisknout
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
jeho	jeho	k3xOp3gInPc1	jeho
prezidentské	prezidentský	k2eAgInPc1d1	prezidentský
obrazy	obraz	k1gInPc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
komunistická	komunistický	k2eAgFnSc1d1	komunistická
reprezentace	reprezentace	k1gFnSc1	reprezentace
byla	být	k5eAaImAgFnS	být
zrovna	zrovna	k6eAd1	zrovna
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
u	u	k7c2	u
Chruščova	Chruščův	k2eAgInSc2d1	Chruščův
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
ale	ale	k9	ale
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dostal	dostat	k5eAaPmAgMnS	dostat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
Zápotockého	Zápotockého	k2eAgFnSc6d1	Zápotockého
<g/>
,	,	kIx,	,
osobně	osobně	k6eAd1	osobně
doporučil	doporučit	k5eAaPmAgMnS	doporučit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
Antonína	Antonín	k1gMnSc2	Antonín
Novotného	Novotný	k1gMnSc2	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
KSČ	KSČ	kA	KSČ
doporučení	doporučení	k1gNnSc4	doporučení
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vedení	vedení	k1gNnSc2	vedení
akceptoval	akceptovat	k5eAaBmAgMnS	akceptovat
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Antonína	Antonín	k1gMnSc4	Antonín
Novotného	Novotný	k1gMnSc4	Novotný
jako	jako	k8xS	jako
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
když	když	k8xS	když
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
všech	všecek	k3xTgMnPc2	všecek
353	[number]	k4	353
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
křesla	křeslo	k1gNnSc2	křeslo
mu	on	k3xPp3gMnSc3	on
dopomohly	dopomoct	k5eAaPmAgInP	dopomoct
pouze	pouze	k6eAd1	pouze
Chruščovovy	Chruščovův	k2eAgFnPc1d1	Chruščovova
osobní	osobní	k2eAgFnPc1d1	osobní
sympatie	sympatie	k1gFnPc1	sympatie
<g/>
.	.	kIx.	.
</s>
<s>
Intelektuálně	intelektuálně	k6eAd1	intelektuálně
však	však	k9	však
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
nestačil	stačit	k5eNaBmAgMnS	stačit
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
vystoupení	vystoupení	k1gNnSc2	vystoupení
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
posměch	posměch	k1gInSc4	posměch
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
své	svůj	k3xOyFgInPc4	svůj
projevy	projev	k1gInPc4	projev
četl	číst	k5eAaImAgMnS	číst
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
mu	on	k3xPp3gMnSc3	on
všechna	všechen	k3xTgNnPc4	všechen
cizí	cizí	k2eAgNnPc4d1	cizí
slova	slovo	k1gNnPc4	slovo
přepisovat	přepisovat	k5eAaImF	přepisovat
ve	v	k7c6	v
fonetické	fonetický	k2eAgFnSc6d1	fonetická
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
a	a	k8xC	a
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
schopnostech	schopnost	k1gFnPc6	schopnost
pochyboval	pochybovat	k5eAaImAgMnS	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Svědectví	svědectví	k1gNnSc1	svědectví
jeho	jeho	k3xOp3gFnSc2	jeho
neteře	neteř	k1gFnSc2	neteř
z	z	k7c2	z
konce	konec	k1gInSc2	konec
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Novotný	Novotný	k1gMnSc1	Novotný
drží	držet	k5eAaImIp3nS	držet
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
dlaních	dlaň	k1gFnPc6	dlaň
a	a	k8xC	a
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
nestačí	stačit	k5eNaBmIp3nS	stačit
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
obstát	obstát	k5eAaPmF	obstát
<g/>
.	.	kIx.	.
</s>
<s>
Styděl	stydět	k5eAaImAgMnS	stydět
se	se	k3xPyFc4	se
před	před	k7c7	před
vzdělanými	vzdělaný	k2eAgMnPc7d1	vzdělaný
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
inteligenci	inteligence	k1gFnSc3	inteligence
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
choval	chovat	k5eAaImAgMnS	chovat
tvrdě	tvrdě	k6eAd1	tvrdě
a	a	k8xC	a
přezíravě	přezíravě	k6eAd1	přezíravě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
došlo	dojít	k5eAaPmAgNnS	dojít
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
uvolnění	uvolnění	k1gNnSc3	uvolnění
a	a	k8xC	a
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
rehabilitaci	rehabilitace	k1gFnSc3	rehabilitace
většiny	většina	k1gFnSc2	většina
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
odsouzených	odsouzený	k1gMnPc2	odsouzený
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Jiřího	Jiří	k1gMnSc2	Jiří
Pernese	Pernese	k1gFnSc2	Pernese
Novotný	Novotný	k1gMnSc1	Novotný
trávil	trávit	k5eAaImAgMnS	trávit
v	v	k7c6	v
prezidentském	prezidentský	k2eAgInSc6d1	prezidentský
úřadě	úřad	k1gInSc6	úřad
svá	svůj	k3xOyFgNnPc4	svůj
dopoledne	dopoledne	k6eAd1	dopoledne
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
totiž	totiž	k9	totiž
nadále	nadále	k6eAd1	nadále
věnoval	věnovat	k5eAaPmAgMnS	věnovat
své	svůj	k3xOyFgFnPc4	svůj
důležitější	důležitý	k2eAgFnPc4d2	důležitější
funkci	funkce	k1gFnSc4	funkce
prvního	první	k4xOgMnSc2	první
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Novotný	Novotný	k1gMnSc1	Novotný
byl	být	k5eAaImAgMnS	být
první	první	k4xOgFnSc7	první
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nechávala	nechávat	k5eAaImAgFnS	nechávat
oslovovat	oslovovat	k5eAaImF	oslovovat
"	"	kIx"	"
<g/>
soudruhu	soudruh	k1gMnSc5	soudruh
prezidente	prezident	k1gMnSc5	prezident
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
snížit	snížit	k5eAaPmF	snížit
plat	plat	k1gInSc4	plat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
na	na	k7c4	na
zasedání	zasedání	k1gNnSc4	zasedání
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
aktivně	aktivně	k6eAd1	aktivně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
schválení	schválení	k1gNnSc3	schválení
návrhu	návrh	k1gInSc2	návrh
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
socialistické	socialistický	k2eAgFnPc1d1	socialistická
<g/>
,	,	kIx,	,
nahrazující	nahrazující	k2eAgFnSc1d1	nahrazující
Ústavu	ústav	k1gInSc2	ústav
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc4	který
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
sešlo	sejít	k5eAaPmAgNnS	sejít
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1960	[number]	k4	1960
schválilo	schválit	k5eAaPmAgNnS	schválit
novou	nový	k2eAgFnSc4d1	nová
územní	územní	k2eAgFnSc4d1	územní
organizaci	organizace	k1gFnSc4	organizace
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc1d1	spočívající
ve	v	k7c6	v
slučování	slučování	k1gNnSc6	slučování
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
snížení	snížení	k1gNnSc4	snížení
počtu	počet	k1gInSc2	počet
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
konferenci	konference	k1gFnSc6	konference
KSČ	KSČ	kA	KSČ
Novotný	Novotný	k1gMnSc1	Novotný
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
projevu	projev	k1gInSc6	projev
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
"	"	kIx"	"
<g/>
vítězství	vítězství	k1gNnSc3	vítězství
socialismu	socialismus	k1gInSc2	socialismus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čímž	což	k3yQnSc7	což
myslel	myslet	k5eAaImAgMnS	myslet
téměř	téměř	k6eAd1	téměř
úplnou	úplný	k2eAgFnSc4d1	úplná
likvidaci	likvidace	k1gFnSc4	likvidace
soukromého	soukromý	k2eAgNnSc2d1	soukromé
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
výrobních	výrobní	k2eAgInPc2d1	výrobní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
)	)	kIx)	)
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
společnosti	společnost	k1gFnSc2	společnost
bez	bez	k7c2	bez
vykořisťovatelských	vykořisťovatelský	k2eAgFnPc2d1	vykořisťovatelská
tříd	třída	k1gFnPc2	třída
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
schválilo	schválit	k5eAaPmAgNnS	schválit
tuto	tento	k3xDgFnSc4	tento
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
tradiční	tradiční	k2eAgInSc4d1	tradiční
název	název	k1gInSc4	název
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k8xC	i
nový	nový	k2eAgInSc1d1	nový
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Antonína	Antonín	k1gMnSc4	Antonín
Novotného	Novotný	k1gMnSc4	Novotný
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
společnosti	společnost	k1gFnSc2	společnost
problém	problém	k1gInSc1	problém
slovenský	slovenský	k2eAgInSc1d1	slovenský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
ústava	ústava	k1gFnSc1	ústava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
centralistické	centralistický	k2eAgNnSc4d1	centralistické
řízení	řízení	k1gNnSc4	řízení
<g/>
,	,	kIx,	,
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
demokratický	demokratický	k2eAgInSc4d1	demokratický
centralismus	centralismus	k1gInSc4	centralismus
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
pragocentrický	pragocentrický	k2eAgInSc1d1	pragocentrický
aparát	aparát	k1gInSc1	aparát
velice	velice	k6eAd1	velice
silně	silně	k6eAd1	silně
zastával	zastávat	k5eAaImAgMnS	zastávat
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
pro	pro	k7c4	pro
slovenské	slovenský	k2eAgInPc4d1	slovenský
národní	národní	k2eAgInPc4d1	národní
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
sebeuplatnění	sebeuplatnění	k1gNnSc1	sebeuplatnění
slovenského	slovenský	k2eAgInSc2d1	slovenský
národa	národ	k1gInSc2	národ
jak	jak	k8xS	jak
politicky	politicky	k6eAd1	politicky
tak	tak	k9	tak
kulturně	kulturně	k6eAd1	kulturně
nebylo	být	k5eNaImAgNnS	být
žádné	žádný	k3yNgNnSc1	žádný
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
představitelů	představitel	k1gMnPc2	představitel
Matice	matice	k1gFnSc2	matice
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
převzít	převzít	k5eAaPmF	převzít
dary	dar	k1gInPc4	dar
a	a	k8xC	a
obvinil	obvinit	k5eAaPmAgMnS	obvinit
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
separatismu	separatismus	k1gInSc2	separatismus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Slovenska	Slovensko	k1gNnSc2	Slovensko
zakázal	zakázat	k5eAaPmAgMnS	zakázat
vzít	vzít	k5eAaPmF	vzít
dary	dar	k1gInPc4	dar
od	od	k7c2	od
Matice	matice	k1gFnSc2	matice
slovenské	slovenský	k2eAgFnSc2d1	slovenská
i	i	k8xC	i
své	svůj	k3xOyFgFnSc3	svůj
manželce	manželka	k1gFnSc3	manželka
se	se	k3xPyFc4	se
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Božka	Božka	k1gFnSc1	Božka
nic	nic	k3yNnSc1	nic
neber	brát	k5eNaImRp2nS	brát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pak	pak	k6eAd1	pak
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
však	však	k9	však
československá	československý	k2eAgFnSc1d1	Československá
společnost	společnost	k1gFnSc1	společnost
postupně	postupně	k6eAd1	postupně
značně	značně	k6eAd1	značně
liberalizovala	liberalizovat	k5eAaImAgFnS	liberalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
tzv.	tzv.	kA	tzv.
Nová	Nová	k1gFnSc1	Nová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
vznikala	vznikat	k5eAaImAgNnP	vznikat
malá	malý	k2eAgNnPc1d1	malé
divadla	divadlo	k1gNnPc1	divadlo
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
výrazně	výrazně	k6eAd1	výrazně
svobodomyslné	svobodomyslný	k2eAgFnPc1d1	svobodomyslná
Literární	literární	k2eAgFnPc1d1	literární
noviny	novina	k1gFnPc1	novina
atd.	atd.	kA	atd.
Novotného	Novotný	k1gMnSc4	Novotný
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
dotkly	dotknout	k5eAaPmAgFnP	dotknout
tzv.	tzv.	kA	tzv.
strahovské	strahovský	k2eAgFnPc4d1	Strahovská
události	událost	k1gFnPc4	událost
na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
v	v	k7c6	v
menze	menza	k1gFnSc6	menza
kolejí	kolej	k1gFnPc2	kolej
neúspěšně	úspěšně	k6eNd1	úspěšně
hledat	hledat	k5eAaImF	hledat
se	s	k7c7	s
studenty	student	k1gMnPc7	student
řešení	řešení	k1gNnSc2	řešení
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
<g/>
.	.	kIx.	.
</s>
<s>
Masarykovské	masarykovský	k2eAgInPc1d1	masarykovský
názory	názor	k1gInPc1	názor
studentů	student	k1gMnPc2	student
se	se	k3xPyFc4	se
dostávaly	dostávat	k5eAaImAgFnP	dostávat
do	do	k7c2	do
přímého	přímý	k2eAgInSc2d1	přímý
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
názory	názor	k1gInPc7	názor
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c6	o
svobodné	svobodný	k2eAgFnSc6d1	svobodná
vůli	vůle	k1gFnSc6	vůle
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
nepotřebnosti	nepotřebnost	k1gFnSc2	nepotřebnost
Lidových	lidový	k2eAgFnPc2d1	lidová
milicí	milice	k1gFnPc2	milice
<g/>
,	,	kIx,	,
porušování	porušování	k1gNnSc1	porušování
akademické	akademický	k2eAgFnSc2d1	akademická
svobody	svoboda	k1gFnSc2	svoboda
Státní	státní	k2eAgFnSc1d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
potlačování	potlačování	k1gNnSc1	potlačování
studentských	studentský	k2eAgInPc2d1	studentský
nepokojů	nepokoj	k1gInPc2	nepokoj
a	a	k8xC	a
také	také	k9	také
o	o	k7c6	o
možnostech	možnost	k1gFnPc6	možnost
drobného	drobný	k2eAgNnSc2d1	drobné
soukromého	soukromý	k2eAgNnSc2d1	soukromé
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
po	po	k7c6	po
pokusu	pokus	k1gInSc6	pokus
Novotného	Novotného	k2eAgMnSc1d1	Novotného
omezit	omezit	k5eAaPmF	omezit
liberalizaci	liberalizace	k1gFnSc4	liberalizace
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
vydávání	vydávání	k1gNnSc4	vydávání
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
liberálních	liberální	k2eAgFnPc2d1	liberální
Literárních	literární	k2eAgFnPc2d1	literární
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
zakázal	zakázat	k5eAaPmAgMnS	zakázat
např.	např.	kA	např.
promítání	promítání	k1gNnSc4	promítání
filmu	film	k1gInSc2	film
Bílá	bílý	k2eAgFnSc1d1	bílá
paní	paní	k1gFnSc1	paní
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tehdy	tehdy	k6eAd1	tehdy
zaplňoval	zaplňovat	k5eAaImAgInS	zaplňovat
československé	československý	k2eAgMnPc4d1	československý
biografy	biograf	k1gMnPc4	biograf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
politická	politický	k2eAgFnSc1d1	politická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
využili	využít	k5eAaPmAgMnP	využít
jeho	jeho	k3xOp3gMnPc1	jeho
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Stalinisté	stalinista	k1gMnPc1	stalinista
Biľak	Biľak	k1gMnSc1	Biľak
a	a	k8xC	a
Kolder	Kolder	k1gMnSc1	Kolder
a	a	k8xC	a
také	také	k9	také
Hendrych	Hendrych	k1gMnSc1	Hendrych
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
reformními	reformní	k2eAgMnPc7d1	reformní
komunisty	komunista	k1gMnPc7	komunista
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
odvolat	odvolat	k5eAaPmF	odvolat
Novotného	Novotný	k1gMnSc4	Novotný
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
prvního	první	k4xOgNnSc2	první
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c2	za
něj	on	k3xPp3gInSc2	on
postavily	postavit	k5eAaPmAgFnP	postavit
Lidové	lidový	k2eAgFnPc1d1	lidová
milice	milice	k1gFnPc1	milice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poté	poté	k6eAd1	poté
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
únorovém	únorový	k2eAgInSc6d1	únorový
projevu	projev	k1gInSc6	projev
zcela	zcela	k6eAd1	zcela
zaplnily	zaplnit	k5eAaPmAgFnP	zaplnit
pražské	pražský	k2eAgNnSc4d1	Pražské
Staroměstské	staroměstský	k2eAgNnSc4d1	Staroměstské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
řešení	řešení	k1gNnSc1	řešení
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
patové	patový	k2eAgFnSc2d1	patová
situace	situace	k1gFnSc2	situace
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
svého	svůj	k3xOyFgMnSc4	svůj
oblíbence	oblíbenec	k1gMnSc4	oblíbenec
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
slovenského	slovenský	k2eAgMnSc4d1	slovenský
komunistu	komunista	k1gMnSc4	komunista
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
jako	jako	k8xS	jako
kompromisního	kompromisní	k2eAgMnSc2d1	kompromisní
kandidáta	kandidát	k1gMnSc2	kandidát
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
1	[number]	k4	1
<g/>
.	.	kIx.	.
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Dubček	Dubček	k1gInSc1	Dubček
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1968	[number]	k4	1968
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
významné	významný	k2eAgFnSc2d1	významná
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
byl	být	k5eAaImAgInS	být
prezidentem	prezident	k1gMnSc7	prezident
až	až	k8xS	až
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
přinucen	přinutit	k5eAaPmNgMnS	přinutit
odstoupit	odstoupit	k5eAaPmF	odstoupit
i	i	k9	i
z	z	k7c2	z
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
odejít	odejít	k5eAaPmF	odejít
z	z	k7c2	z
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Novotný	Novotný	k1gMnSc1	Novotný
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
umístěním	umístění	k1gNnSc7	umístění
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
nepodporoval	podporovat	k5eNaImAgMnS	podporovat
nástup	nástup	k1gInSc4	nástup
Brežněva	Brežněv	k1gMnSc2	Brežněv
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
(	(	kIx(	(
<g/>
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
jeho	on	k3xPp3gMnSc2	on
rivala	rival	k1gMnSc2	rival
Kosygina	Kosygin	k1gMnSc2	Kosygin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
umožnil	umožnit	k5eAaPmAgInS	umožnit
postupnou	postupný	k2eAgFnSc4d1	postupná
liberalizaci	liberalizace	k1gFnSc4	liberalizace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
postoj	postoj	k1gInSc1	postoj
ke	k	k7c3	k
Slovensku	Slovensko	k1gNnSc3	Slovensko
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
problematický	problematický	k2eAgInSc4d1	problematický
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Matici	matice	k1gFnSc4	matice
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
pomluvil	pomluvit	k5eAaPmAgMnS	pomluvit
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
vyplývalo	vyplývat	k5eAaImAgNnS	vyplývat
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
nejen	nejen	k6eAd1	nejen
historického	historický	k2eAgNnSc2d1	historické
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
omezil	omezit	k5eAaPmAgMnS	omezit
pravomoci	pravomoc	k1gFnPc4	pravomoc
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
dalších	další	k2eAgInPc2d1	další
orgánů	orgán	k1gInPc2	orgán
zrušil	zrušit	k5eAaPmAgInS	zrušit
Sbor	sbor	k1gInSc1	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
-	-	kIx~	-
národní	národní	k2eAgInSc1d1	národní
orgán	orgán	k1gInSc1	orgán
moci	moct	k5eAaImF	moct
vládní	vládní	k2eAgFnPc4d1	vládní
a	a	k8xC	a
výkonné	výkonný	k2eAgFnPc4d1	výkonná
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc1	Československo
stalo	stát	k5eAaPmAgNnS	stát
de	de	k?	de
facto	facto	k1gNnSc1	facto
unitárním	unitární	k2eAgInSc7d1	unitární
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krachu	krach	k1gInSc6	krach
III	III	kA	III
<g/>
.	.	kIx.	.
pětiletého	pětiletý	k2eAgInSc2d1	pětiletý
plánu	plán	k1gInSc2	plán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
získal	získat	k5eAaPmAgInS	získat
pochybnosti	pochybnost	k1gFnSc2	pochybnost
o	o	k7c6	o
centrálním	centrální	k2eAgNnSc6d1	centrální
řízení	řízení	k1gNnSc6	řízení
československého	československý	k2eAgNnSc2d1	Československé
hospodářství	hospodářství	k1gNnSc2	hospodářství
(	(	kIx(	(
<g/>
vystoupení	vystoupení	k1gNnSc1	vystoupení
na	na	k7c6	na
Letné	Letná	k1gFnSc6	Letná
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
připustil	připustit	k5eAaPmAgInS	připustit
význam	význam	k1gInSc1	význam
prezidentů	prezident	k1gMnPc2	prezident
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
Beneše	Beneš	k1gMnSc2	Beneš
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
obnově	obnova	k1gFnSc6	obnova
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rehabilitacích	rehabilitace	k1gFnPc6	rehabilitace
nepřímo	přímo	k6eNd1	přímo
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
neblahou	blahý	k2eNgFnSc4d1	neblahá
roli	role	k1gFnSc4	role
obou	dva	k4xCgFnPc2	dva
svých	svůj	k3xOyFgMnPc2	svůj
bezprostředních	bezprostřední	k2eAgMnPc2d1	bezprostřední
předchůdců	předchůdce	k1gMnPc2	předchůdce
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
práva	právo	k1gNnPc1	právo
byla	být	k5eAaImAgNnP	být
daná	daný	k2eAgFnSc1d1	daná
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
ústavou	ústava	k1gFnSc7	ústava
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gFnSc7	jeho
vlastní	vlastní	k2eAgFnSc7d1	vlastní
ústavou	ústava	k1gFnSc7	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Novotný	Novotný	k1gMnSc1	Novotný
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnPc7	jeho
předchůdci	předchůdce	k1gMnPc7	předchůdce
<g/>
,	,	kIx,	,
terčem	terč	k1gInSc7	terč
četných	četný	k2eAgNnPc2d1	četné
anekdot	anekdoton	k1gNnPc2	anekdoton
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vysmívaly	vysmívat	k5eAaImAgFnP	vysmívat
jeho	jeho	k3xOp3gFnPc1	jeho
nevzdělanosti	nevzdělanost	k1gFnPc1	nevzdělanost
a	a	k8xC	a
ješitnosti	ješitnost	k1gFnPc1	ješitnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidem	lid	k1gInSc7	lid
byl	být	k5eAaImAgInS	být
posměšně	posměšně	k6eAd1	posměšně
přezdíván	přezdíván	k2eAgMnSc1d1	přezdíván
Urugvajec	Urugvajec	k1gMnSc1	Urugvajec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
málokdo	málokdo	k3yInSc1	málokdo
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
toto	tento	k3xDgNnSc4	tento
pojmenování	pojmenování	k1gNnSc4	pojmenování
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
jihoamerickou	jihoamerický	k2eAgFnSc7d1	jihoamerická
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zkomolením	zkomolení	k1gNnSc7	zkomolení
z	z	k7c2	z
"	"	kIx"	"
<g/>
ruk	ruka	k1gFnPc2	ruka
u	u	k7c2	u
vajec	vejce	k1gNnPc2	vejce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
narážka	narážka	k1gFnSc1	narážka
na	na	k7c4	na
prezidentův	prezidentův	k2eAgInSc4d1	prezidentův
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
postoj	postoj	k1gInSc4	postoj
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
v	v	k7c6	v
rozkroku	rozkrok	k1gInSc6	rozkrok
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
u	u	k7c2	u
vajec	vejce	k1gNnPc2	vejce
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
měl	mít	k5eAaImAgMnS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
schöne	schönout	k5eAaPmIp3nS	schönout
Toni	Toni	k1gFnSc1	Toni
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jako	jako	k9	jako
host	host	k1gMnSc1	host
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
zasedání	zasedání	k1gNnSc4	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
údajně	údajně	k6eAd1	údajně
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejkrásnějším	krásný	k2eAgMnSc7d3	nejkrásnější
státníkem	státník	k1gMnSc7	státník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
byl	být	k5eAaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
