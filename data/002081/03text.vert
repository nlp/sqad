<s>
Tokio	Tokio	k1gNnSc1	Tokio
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
東	東	k?	東
<g/>
,	,	kIx,	,
Tókjó	Tókjó	k1gNnSc1	Tókjó
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
sídlo	sídlo	k1gNnSc1	sídlo
japonského	japonský	k2eAgMnSc2d1	japonský
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
Východní	východní	k2eAgNnSc4d1	východní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
oficiálně	oficiálně	k6eAd1	oficiálně
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
několika	několik	k4yIc2	několik
prefektur	prefektura	k1gFnPc2	prefektura
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
Saitama	Saitama	k1gFnSc1	Saitama
<g/>
,	,	kIx,	,
Čiba	Čiba	k1gFnSc1	Čiba
<g/>
,	,	kIx,	,
Kanagawa	Kanagawa	k1gFnSc1	Kanagawa
<g/>
,	,	kIx,	,
okrajově	okrajově	k6eAd1	okrajově
i	i	k9	i
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
článek	článek	k1gInSc4	článek
prefektura	prefektura	k1gFnSc1	prefektura
Tokio	Tokio	k1gNnSc4	Tokio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
souvislé	souvislý	k2eAgNnSc4d1	souvislé
osídlení	osídlení	k1gNnSc4	osídlení
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xC	jako
Tokio	Tokio	k1gNnSc1	Tokio
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
jako	jako	k8xC	jako
Velké	velký	k2eAgNnSc4d1	velké
Tokio	Tokio	k1gNnSc4	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
prefektury	prefektura	k1gFnSc2	prefektura
Tokio	Tokio	k1gNnSc4	Tokio
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
nachází	nacházet	k5eAaImIp3nS	nacházet
vládní	vládní	k2eAgFnPc4d1	vládní
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
císařský	císařský	k2eAgInSc4d1	císařský
palác	palác	k1gInSc4	palác
a	a	k8xC	a
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
dopravní	dopravní	k2eAgNnSc1d1	dopravní
centrum	centrum	k1gNnSc1	centrum
oblasti	oblast	k1gFnSc2	oblast
megalopole	megalopole	k1gFnSc2	megalopole
označované	označovaný	k2eAgFnSc2d1	označovaná
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jako	jako	k8xS	jako
Kantó	Kantó	k1gFnSc6	Kantó
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
jako	jako	k8xC	jako
Tokio	Tokio	k1gNnSc4	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
prefektury	prefektura	k1gFnPc4	prefektura
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
horami	hora	k1gFnPc7	hora
a	a	k8xC	a
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
Tokio	Tokio	k1gNnSc1	Tokio
nemá	mít	k5eNaImIp3nS	mít
jedno	jeden	k4xCgNnSc1	jeden
městské	městský	k2eAgNnSc1d1	Městské
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řadu	řada	k1gFnSc4	řada
menších	malý	k2eAgInPc2d2	menší
či	či	k8xC	či
větších	veliký	k2eAgInPc2d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
jsou	být	k5eAaImIp3nP	být
rozmístěna	rozmístěn	k2eAgNnPc1d1	rozmístěno
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
kruhové	kruhový	k2eAgFnSc2d1	kruhová
městské	městský	k2eAgFnSc2d1	městská
železnice	železnice	k1gFnSc2	železnice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
dále	daleko	k6eAd2	daleko
převážně	převážně	k6eAd1	převážně
o	o	k7c6	o
Tokiu	Tokio	k1gNnSc6	Tokio
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Honšú	Honšú	k1gFnSc2	Honšú
v	v	k7c6	v
regionu	region	k1gInSc6	region
Kantó	Kantó	k1gFnSc2	Kantó
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
s	s	k7c7	s
předměstími	předměstí	k1gNnPc7	předměstí
okolo	okolo	k7c2	okolo
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
25	[number]	k4	25
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
japonské	japonský	k2eAgFnPc1d1	japonská
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
Edo	Eda	k1gMnSc5	Eda
<g/>
.	.	kIx.	.
</s>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
dnes	dnes	k6eAd1	dnes
problémy	problém	k1gInPc4	problém
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
seismicky	seismicky	k6eAd1	seismicky
aktivní	aktivní	k2eAgFnSc6d1	aktivní
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
skoro	skoro	k6eAd1	skoro
40	[number]	k4	40
miliony	milion	k4xCgInPc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
momentálně	momentálně	k6eAd1	momentálně
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
největší	veliký	k2eAgNnSc4d3	veliký
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
Edo	Eda	k1gMnSc5	Eda
<g/>
.	.	kIx.	.
</s>
<s>
Postihlo	postihnout	k5eAaPmAgNnS	postihnout
ho	on	k3xPp3gNnSc4	on
mnoho	mnoho	k6eAd1	mnoho
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
:	:	kIx,	:
1923	[number]	k4	1923
-	-	kIx~	-
Velké	velký	k2eAgNnSc1d1	velké
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
Kantó	Kantó	k1gFnSc6	Kantó
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
142	[number]	k4	142
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
-	-	kIx~	-
vybombardováno	vybombardovat	k5eAaPmNgNnS	vybombardovat
USA	USA	kA	USA
pomocí	pomocí	k7c2	pomocí
333	[number]	k4	333
B-29	B-29	k1gMnPc2	B-29
a	a	k8xC	a
1	[number]	k4	1
665	[number]	k4	665
tun	tuna	k1gFnPc2	tuna
napalmu	napalm	k1gInSc2	napalm
<g/>
,	,	kIx,	,
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
nejsmrtelnější	smrtelní	k2eAgInSc1d3	smrtelní
letecký	letecký	k2eAgInSc1d1	letecký
útok	útok	k1gInSc1	útok
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zničena	zničen	k2eAgFnSc1d1	zničena
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
přes	přes	k7c4	přes
40	[number]	k4	40
km2	km2	k4	km2
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zhruba	zhruba	k6eAd1	zhruba
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
území	území	k1gNnSc2	území
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
dopadla	dopadnout	k5eAaPmAgNnP	dopadnout
další	další	k2eAgNnPc1d1	další
japonská	japonský	k2eAgNnPc1d1	Japonské
města	město	k1gNnPc1	město
Kóbe	Kóbe	k1gNnSc2	Kóbe
<g/>
,	,	kIx,	,
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
,	,	kIx,	,
Jokohama	Jokohama	k1gFnSc1	Jokohama
<g/>
...	...	k?	...
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1995	[number]	k4	1995
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c4	na
tokijské	tokijský	k2eAgNnSc4d1	Tokijské
metro	metro	k1gNnSc4	metro
teroristická	teroristický	k2eAgFnSc1d1	teroristická
sekta	sekta	k1gFnSc1	sekta
Óm	óm	k1gInSc1	óm
šinrikjó	šinrikjó	k?	šinrikjó
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
vůdce	vůdce	k1gMnSc1	vůdce
Asahara	Asahar	k1gMnSc2	Asahar
Šókó	Šókó	k1gFnSc2	Šókó
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
(	(	kIx(	(
<g/>
poprava	poprava	k1gFnSc1	poprava
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
vykonána	vykonán	k2eAgFnSc1d1	vykonána
<g/>
)	)	kIx)	)
A	a	k9	a
další	další	k2eAgInPc4d1	další
menší	malý	k2eAgInPc4d2	menší
či	či	k8xC	či
větší	veliký	k2eAgInPc4d2	veliký
otřesy	otřes	k1gInPc4	otřes
a	a	k8xC	a
vlny	vlna	k1gFnSc2	vlna
tsunami	tsunami	k1gNnSc7	tsunami
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
bez	bez	k7c2	bez
aglomerace	aglomerace	k1gFnSc2	aglomerace
kolem	kolem	k7c2	kolem
12	[number]	k4	12
527	[number]	k4	527
115	[number]	k4	115
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
aglomerací	aglomerace	k1gFnSc7	aglomerace
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
38	[number]	k4	38
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tokijské	tokijský	k2eAgFnSc3d1	Tokijská
aglomeraci	aglomerace	k1gFnSc3	aglomerace
patří	patřit	k5eAaImIp3nS	patřit
Kawasaki	Kawasaki	k1gNnSc1	Kawasaki
<g/>
,	,	kIx,	,
Jokohama	Jokohama	k1gFnSc1	Jokohama
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
okolních	okolní	k2eAgNnPc2d1	okolní
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prognóz	prognóza	k1gFnPc2	prognóza
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Tokia	Tokio	k1gNnSc2	Tokio
nadále	nadále	k6eAd1	nadále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
a	a	k8xC	a
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Extrémní	extrémní	k2eAgNnSc4d1	extrémní
množství	množství	k1gNnSc4	množství
aut	auto	k1gNnPc2	auto
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
působí	působit	k5eAaImIp3nS	působit
značné	značný	k2eAgInPc4d1	značný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
bývalo	bývat	k5eAaImAgNnS	bývat
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejznečištěnějších	znečištěný	k2eAgNnPc2d3	nejznečištěnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
rozmístěny	rozmístěn	k2eAgFnPc1d1	rozmístěna
tabule	tabule	k1gFnPc1	tabule
s	s	k7c7	s
údaji	údaj	k1gInPc7	údaj
o	o	k7c4	o
znečištění	znečištění	k1gNnSc4	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
svým	svůj	k3xOyFgMnSc7	svůj
celoročním	celoroční	k2eAgInSc7d1	celoroční
smogovým	smogový	k2eAgInSc7d1	smogový
příkrovem	příkrov	k1gInSc7	příkrov
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
podstatně	podstatně	k6eAd1	podstatně
mění	měnit	k5eAaImIp3nS	měnit
k	k	k7c3	k
lepšímu	dobrý	k2eAgMnSc3d2	lepší
<g/>
,	,	kIx,	,
za	za	k7c2	za
jasných	jasný	k2eAgInPc2d1	jasný
dnů	den	k1gInPc2	den
lze	lze	k6eAd1	lze
dokonce	dokonce	k9	dokonce
z	z	k7c2	z
některých	některý	k3yIgNnPc2	některý
míst	místo	k1gNnPc2	místo
spatřit	spatřit	k5eAaPmF	spatřit
horu	hora	k1gFnSc4	hora
Fudži	Fudž	k1gFnSc3	Fudž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedá	dát	k5eNaPmIp3nS	dát
koupat	koupat	k5eAaImF	koupat
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
kilometry	kilometr	k1gInPc4	kilometr
od	od	k7c2	od
Tokia	Tokio	k1gNnSc2	Tokio
je	být	k5eAaImIp3nS	být
znečištění	znečištění	k1gNnSc1	znečištění
pláží	pláž	k1gFnPc2	pláž
patrné	patrný	k2eAgFnPc1d1	patrná
<g/>
.	.	kIx.	.
</s>
<s>
Tokio	Tokio	k1gNnSc1	Tokio
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
japonským	japonský	k2eAgInSc7d1	japonský
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
pro	pro	k7c4	pro
vnitrostátní	vnitrostátní	k2eAgFnSc4d1	vnitrostátní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
leteckou	letecký	k2eAgFnSc4d1	letecká
<g/>
,	,	kIx,	,
silniční	silniční	k2eAgFnSc4d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Páteř	páteř	k1gFnSc1	páteř
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
tvoří	tvořit	k5eAaImIp3nS	tvořit
velmi	velmi	k6eAd1	velmi
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vhodně	vhodně	k6eAd1	vhodně
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednokolejkami	jednokolejka	k1gFnPc7	jednokolejka
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
vlaků	vlak	k1gInPc2	vlak
<g/>
(	(	kIx(	(
<g/>
povrchového	povrchový	k2eAgNnSc2d1	povrchové
metra	metro	k1gNnSc2	metro
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
modernizací	modernizace	k1gFnPc2	modernizace
před	před	k7c7	před
Olympiádou	olympiáda	k1gFnSc7	olympiáda
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
linka	linka	k1gFnSc1	linka
byla	být	k5eAaImAgFnS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
jako	jako	k8xS	jako
zajímavost	zajímavost	k1gFnSc4	zajímavost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionu	region	k1gInSc6	region
Kantó	Kantó	k1gFnSc2	Kantó
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgNnPc4	dva
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
Haneda	Haned	k1gMnSc2	Haned
(	(	kIx(	(
<g/>
羽	羽	k?	羽
Haneda	Haneda	k1gMnSc1	Haneda
kúkó	kúkó	k?	kúkó
<g/>
,	,	kIx,	,
羽	羽	k?	羽
Haneda	Haneda	k1gMnSc1	Haneda
kokusai	kokusa	k1gFnSc2	kokusa
kúkó	kúkó	k?	kúkó
<g/>
,	,	kIx,	,
東	東	k?	東
Tókjó	Tókjó	k1gNnSc4	Tókjó
kokusai	kokusa	k1gFnSc2	kokusa
kúkó	kúkó	k?	kúkó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
vnitrostátní	vnitrostátní	k2eAgInPc4d1	vnitrostátní
lety	let	k1gInPc4	let
a	a	k8xC	a
na	na	k7c4	na
chartery	charter	k1gMnPc4	charter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívají	využívat	k5eAaPmIp3nP	využívat
ho	on	k3xPp3gNnSc4	on
také	také	k9	také
čínské	čínský	k2eAgFnPc1d1	čínská
aerolinie	aerolinie	k1gFnPc1	aerolinie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
dopravu	doprava	k1gFnSc4	doprava
slouží	sloužit	k5eAaImIp3nS	sloužit
letiště	letiště	k1gNnSc1	letiště
Narita	Naritum	k1gNnSc2	Naritum
(	(	kIx(	(
<g/>
成	成	k?	成
-	-	kIx~	-
Narita	Narita	k1gMnSc1	Narita
kokusai	kokusa	k1gFnSc2	kokusa
kúkó	kúkó	k?	kúkó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Tokia	Tokio	k1gNnSc2	Tokio
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Čiba	Čib	k1gInSc2	Čib
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
Tokia	Tokio	k1gNnSc2	Tokio
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
významné	významný	k2eAgInPc1d1	významný
přístavy	přístav	k1gInPc1	přístav
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Jokohamě	Jokohama	k1gFnSc6	Jokohama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
plují	plout	k5eAaImIp3nP	plout
lodě	loď	k1gFnPc1	loď
do	do	k7c2	do
USA	USA	kA	USA
přes	přes	k7c4	přes
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
a	a	k8xC	a
také	také	k9	také
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
i	i	k8xC	i
japonských	japonský	k2eAgInPc2d1	japonský
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Tokijské	tokijský	k2eAgNnSc1d1	Tokijské
metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
provozováno	provozován	k2eAgNnSc1d1	provozováno
dvěma	dva	k4xCgFnPc7	dva
nezávislými	závislý	k2eNgFnPc7d1	nezávislá
institucemi	instituce	k1gFnPc7	instituce
<g/>
:	:	kIx,	:
Odborem	odbor	k1gInSc7	odbor
dopravy	doprava	k1gFnSc2	doprava
tokijské	tokijský	k2eAgFnSc2d1	Tokijská
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
Tokyo	Tokyo	k6eAd1	Tokyo
Metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
dvanácti	dvanáct	k4xCc7	dvanáct
linkami	linka	k1gFnPc7	linka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
řada	řada	k1gFnSc1	řada
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c6	na
povrchové	povrchový	k2eAgFnSc6d1	povrchová
tratě	trata	k1gFnSc6	trata
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgMnPc2d1	různý
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
návaznost	návaznost	k1gFnSc1	návaznost
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
through-service	throughervice	k1gFnSc1	through-service
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
příměstské	příměstský	k2eAgInPc1d1	příměstský
povrchové	povrchový	k2eAgInPc1d1	povrchový
vlaky	vlak	k1gInPc1	vlak
v	v	k7c6	v
určené	určený	k2eAgFnSc6d1	určená
stanici	stanice	k1gFnSc6	stanice
sjedou	sjet	k5eAaPmIp3nP	sjet
pod	pod	k7c4	pod
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
jako	jako	k8xS	jako
vlak	vlak	k1gInSc4	vlak
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
vlaky	vlak	k1gInPc1	vlak
a	a	k8xC	a
metro	metro	k1gNnSc1	metro
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
Autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
odbor	odbor	k1gInSc1	odbor
dopravy	doprava	k1gFnSc2	doprava
tokijské	tokijský	k2eAgFnSc2d1	Tokijská
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
soukromí	soukromý	k2eAgMnPc1d1	soukromý
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc4	množství
městských	městský	k2eAgInPc2d1	městský
<g/>
,	,	kIx,	,
regionálních	regionální	k2eAgInPc2d1	regionální
i	i	k8xC	i
dálkových	dálkový	k2eAgInPc2d1	dálkový
spojů	spoj	k1gInPc2	spoj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
velké	velký	k2eAgInPc1d1	velký
železniční	železniční	k2eAgInPc1d1	železniční
terminály	terminál	k1gInPc1	terminál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
nádraží	nádraží	k1gNnSc1	nádraží
Šindžuku	Šindžuk	k1gInSc2	Šindžuk
nebo	nebo	k8xC	nebo
tokijské	tokijský	k2eAgNnSc4d1	Tokijské
centrální	centrální	k2eAgNnSc4d1	centrální
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
také	také	k9	také
auta	auto	k1gNnPc1	auto
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
rovnými	rovný	k2eAgFnPc7d1	rovná
a	a	k8xC	a
kilometry	kilometr	k1gInPc7	kilometr
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
ulicemi	ulice	k1gFnPc7	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Aut	aut	k1gInSc1	aut
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
neúnosně	únosně	k6eNd1	únosně
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
kapacita	kapacita	k1gFnSc1	kapacita
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Zácpy	zácpa	k1gFnPc1	zácpa
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
situaci	situace	k1gFnSc3	situace
regulovat	regulovat	k5eAaImF	regulovat
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepovolí	povolit	k5eNaPmIp3nS	povolit
registraci	registrace	k1gFnSc4	registrace
nového	nový	k2eAgInSc2d1	nový
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
majitel	majitel	k1gMnSc1	majitel
neprokáže	prokázat	k5eNaPmIp3nS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
kde	kde	k6eAd1	kde
zaparkovat	zaparkovat	k5eAaPmF	zaparkovat
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Tokijským	tokijský	k2eAgInSc7d1	tokijský
zálivem	záliv	k1gInSc7	záliv
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
dopravu	doprava	k1gFnSc4	doprava
mírně	mírně	k6eAd1	mírně
zlepšit	zlepšit	k5eAaPmF	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
taxislužba	taxislužba	k1gFnSc1	taxislužba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
hromadné	hromadný	k2eAgFnSc3d1	hromadná
dopravě	doprava	k1gFnSc3	doprava
podstatně	podstatně	k6eAd1	podstatně
dražší	drahý	k2eAgInSc1d2	dražší
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgInPc1d1	stejný
neduhy	neduh	k1gInPc1	neduh
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
auta	auto	k1gNnPc1	auto
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
-	-	kIx~	-
zpravidla	zpravidla	k6eAd1	zpravidla
neunikne	uniknout	k5eNaPmIp3nS	uniknout
zácpám	zácpa	k1gFnPc3	zácpa
<g/>
.	.	kIx.	.
</s>
<s>
Specialitou	specialita	k1gFnSc7	specialita
místních	místní	k2eAgInPc2d1	místní
taxíků	taxík	k1gInPc2	taxík
je	být	k5eAaImIp3nS	být
automatické	automatický	k2eAgNnSc4d1	automatické
otvírání	otvírání	k1gNnSc4	otvírání
a	a	k8xC	a
zavírání	zavírání	k1gNnSc4	zavírání
dveří	dveře	k1gFnPc2	dveře
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
provádí	provádět	k5eAaImIp3nS	provádět
řidič	řidič	k1gMnSc1	řidič
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tokijská	tokijský	k2eAgFnSc1d1	Tokijská
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
protkána	protkat	k5eAaPmNgFnS	protkat
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejhustších	hustý	k2eAgFnPc2d3	nejhustší
a	a	k8xC	a
také	také	k9	také
nejsložitějších	složitý	k2eAgFnPc2d3	nejsložitější
železničních	železniční	k2eAgFnPc2d1	železniční
sítí	síť	k1gFnPc2	síť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tokijské	tokijský	k2eAgNnSc1d1	Tokijské
centrální	centrální	k2eAgNnSc1d1	centrální
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgInSc7d1	přirozený
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnPc4d1	hlavní
japonské	japonský	k2eAgFnPc4d1	japonská
dálkové	dálkový	k2eAgFnPc4d1	dálková
tratě	trať	k1gFnPc4	trať
<g/>
,	,	kIx,	,
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centrum	k1gNnSc7	centrum
sítě	síť	k1gFnSc2	síť
Šinkansen	Šinkansna	k1gFnPc2	Šinkansna
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečnou	výjimečný	k2eAgFnSc7d1	výjimečná
vlastností	vlastnost	k1gFnSc7	vlastnost
tohoto	tento	k3xDgInSc2	tento
železničního	železniční	k2eAgInSc2d1	železniční
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
silná	silný	k2eAgFnSc1d1	silná
integrace	integrace	k1gFnSc1	integrace
s	s	k7c7	s
podzemní	podzemní	k2eAgFnSc7d1	podzemní
dráhou	dráha	k1gFnSc7	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
příměstské	příměstský	k2eAgFnPc1d1	příměstská
vlakové	vlakový	k2eAgFnPc1d1	vlaková
soupravy	souprava	k1gFnPc1	souprava
na	na	k7c6	na
vybraných	vybraný	k2eAgNnPc6d1	vybrané
nádražích	nádraží	k1gNnPc6	nádraží
sjíždějí	sjíždět	k5eAaImIp3nP	sjíždět
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
jako	jako	k8xC	jako
vlaky	vlak	k1gInPc1	vlak
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
a	a	k8xC	a
příměstská	příměstský	k2eAgFnSc1d1	příměstská
železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
metropoli	metropol	k1gFnSc6	metropol
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
našeho	náš	k3xOp1gInSc2	náš
pohledu	pohled	k1gInSc2	pohled
charakter	charakter	k1gInSc1	charakter
povrchového	povrchový	k2eAgNnSc2d1	povrchové
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
výhradně	výhradně	k6eAd1	výhradně
elektrické	elektrický	k2eAgFnPc1d1	elektrická
článkové	článkový	k2eAgFnPc1d1	článková
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
rozmanitosti	rozmanitost	k1gFnSc6	rozmanitost
typů	typ	k1gInPc2	typ
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Soupravy	souprava	k1gFnPc1	souprava
jsou	být	k5eAaImIp3nP	být
udržovány	udržovat	k5eAaImNgFnP	udržovat
v	v	k7c6	v
dokonalé	dokonalý	k2eAgFnSc6d1	dokonalá
čistotě	čistota	k1gFnSc6	čistota
i	i	k8xC	i
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nP	jezdit
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
a	a	k8xC	a
na	na	k7c4	na
čas	čas	k1gInSc4	čas
-	-	kIx~	-
jízdní	jízdní	k2eAgInSc4d1	jízdní
řád	řád	k1gInSc4	řád
je	být	k5eAaImIp3nS	být
rozepsán	rozepsán	k2eAgMnSc1d1	rozepsán
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
na	na	k7c4	na
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
tratě	trať	k1gFnPc1	trať
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
linie	linie	k1gFnSc2	linie
Jamanote	Jamanot	k1gMnSc5	Jamanot
<g/>
,	,	kIx,	,
spravuje	spravovat	k5eAaImIp3nS	spravovat
společnost	společnost	k1gFnSc1	společnost
JR	JR	kA	JR
East	Easta	k1gFnPc2	Easta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
železniční	železniční	k2eAgFnSc7d1	železniční
správou	správa	k1gFnSc7	správa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
drah	draha	k1gFnPc2	draha
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
provozována	provozovat	k5eAaImNgFnS	provozovat
menšími	malý	k2eAgFnPc7d2	menší
železničními	železniční	k2eAgFnPc7d1	železniční
společnostmi	společnost	k1gFnPc7	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
JR	JR	kA	JR
East	East	k1gInSc1	East
konkurovat	konkurovat	k5eAaImF	konkurovat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
kratší	krátký	k2eAgFnSc7d2	kratší
jízdní	jízdní	k2eAgFnSc7d1	jízdní
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
nižšími	nízký	k2eAgFnPc7d2	nižší
nebo	nebo	k8xC	nebo
žádnými	žádný	k3yNgInPc7	žádný
příplatky	příplatek	k1gInPc7	příplatek
či	či	k8xC	či
pouhou	pouhý	k2eAgFnSc7d1	pouhá
nižší	nízký	k2eAgFnSc7d2	nižší
cenou	cena	k1gFnSc7	cena
jízdenky	jízdenka	k1gFnSc2	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
menších	malý	k2eAgInPc2d2	menší
je	být	k5eAaImIp3nS	být
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
železnice	železnice	k1gFnSc1	železnice
KEIO	KEIO	kA	KEIO
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
společnosti	společnost	k1gFnPc1	společnost
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
svých	svůj	k3xOyFgFnPc2	svůj
železnic	železnice	k1gFnPc2	železnice
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
volily	volit	k5eAaImAgFnP	volit
jiný	jiný	k2eAgInSc4d1	jiný
rozchod	rozchod	k1gInSc4	rozchod
kolejí	kolej	k1gFnPc2	kolej
než	než	k8xS	než
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
státní	státní	k2eAgFnSc2d1	státní
japonské	japonský	k2eAgFnSc2d1	japonská
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
několika	několik	k4yIc7	několik
rozchody	rozchod	k1gInPc7	rozchod
kolejí	kolej	k1gFnPc2	kolej
<g/>
:	:	kIx,	:
1067	[number]	k4	1067
mm	mm	kA	mm
<g/>
,	,	kIx,	,
1372	[number]	k4	1372
mm	mm	kA	mm
a	a	k8xC	a
1435	[number]	k4	1435
mm	mm	kA	mm
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
s	s	k7c7	s
sebou	se	k3xPyFc7	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
přináší	přinášet	k5eAaImIp3nP	přinášet
nutnost	nutnost	k1gFnSc4	nutnost
<g/>
,	,	kIx,	,
mít	mít	k5eAaImF	mít
na	na	k7c6	na
nádražích	nádraží	k1gNnPc6	nádraží
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
rozchod	rozchod	k1gInSc4	rozchod
kolejí	kolej	k1gFnPc2	kolej
separátní	separátní	k2eAgFnSc4d1	separátní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
tokijských	tokijský	k2eAgFnPc2d1	Tokijská
železnic	železnice	k1gFnPc2	železnice
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
častá	častý	k2eAgFnSc1d1	častá
přeplněnost	přeplněnost	k1gFnSc1	přeplněnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
nejdůležitějších	důležitý	k2eAgFnPc6d3	nejdůležitější
linkách	linka	k1gFnPc6	linka
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
stanicích	stanice	k1gFnPc6	stanice
dokonce	dokonce	k9	dokonce
existují	existovat	k5eAaImIp3nP	existovat
placení	placený	k2eAgMnPc1d1	placený
"	"	kIx"	"
<g/>
nacpávači	nacpávač	k1gMnPc1	nacpávač
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přeplněné	přeplněný	k2eAgFnPc1d1	přeplněná
soupravy	souprava	k1gFnPc1	souprava
pak	pak	k6eAd1	pak
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
a	a	k8xC	a
nástup	nástup	k1gInSc4	nástup
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
zpoždění	zpoždění	k1gNnSc1	zpoždění
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Jakarta	Jakarta	k1gFnSc1	Jakarta
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
Káhira	Káhira	k1gFnSc1	Káhira
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Peking	Peking	k1gInSc1	Peking
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Sã	Sã	k1gFnSc1	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
</s>
