<s>
NULL	NULL	kA	NULL
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
žádnou	žádný	k3yNgFnSc4	žádný
hodnotu	hodnota	k1gFnSc4	hodnota
nebo	nebo	k8xC	nebo
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
nullus	nullus	k1gInSc4	nullus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
reprezentaci	reprezentace	k1gFnSc4	reprezentace
ASCII	ascii	kA	ascii
řetězce	řetězec	k1gInSc2	řetězec
je	být	k5eAaImIp3nS	být
bajt	bajt	k1gInSc1	bajt
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
nula	nula	k1gFnSc1	nula
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
null	nulnout	k5eAaPmAgInS	nulnout
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
null	null	k1gMnSc1	null
terminator	terminator	k1gMnSc1	terminator
nebo	nebo	k8xC	nebo
zkráceně	zkráceně	k6eAd1	zkráceně
NUL	nula	k1gFnPc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
a	a	k8xC	a
odvozených	odvozený	k2eAgFnPc6d1	odvozená
aplikacích	aplikace	k1gFnPc6	aplikace
slouží	sloužit	k5eAaImIp3nS	sloužit
tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc1	znak
jako	jako	k8xC	jako
ukončovací	ukončovací	k2eAgInSc1d1	ukončovací
znak	znak	k1gInSc1	znak
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Escape	Escapat	k5eAaPmIp3nS	Escapat
sekvence	sekvence	k1gFnSc1	sekvence
pro	pro	k7c4	pro
znak	znak	k1gInSc4	znak
NUL	nula	k1gFnPc2	nula
je	být	k5eAaImIp3nS	být
\	\	kIx~	\
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programování	programování	k1gNnSc6	programování
je	být	k5eAaImIp3nS	být
NULL	NULL	kA	NULL
speciální	speciální	k2eAgFnSc1d1	speciální
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
adresní	adresní	k2eAgInSc1d1	adresní
ukazatel	ukazatel	k1gInSc1	ukazatel
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
referenčního	referenční	k2eAgInSc2d1	referenční
odkazu	odkaz	k1gInSc2	odkaz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
(	(	kIx(	(
<g/>
abstraktně	abstraktně	k6eAd1	abstraktně
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
)	)	kIx)	)
neukazuje	ukazovat	k5eNaImIp3nS	ukazovat
nikam	nikam	k6eAd1	nikam
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
0	[number]	k4	0
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
jako	jako	k9	jako
konvence	konvence	k1gFnSc1	konvence
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
ukazatele	ukazatel	k1gInSc2	ukazatel
za	za	k7c4	za
NULL	NULL	kA	NULL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programovacím	programovací	k2eAgInSc6d1	programovací
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
NULL	NULL	kA	NULL
reprezentována	reprezentovat	k5eAaImNgFnS	reprezentovat
binární	binární	k2eAgFnSc7d1	binární
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
v	v	k7c6	v
programovacím	programovací	k2eAgInSc6d1	programovací
jazyce	jazyk	k1gInSc6	jazyk
Pascal	pascal	k1gInSc1	pascal
nebo	nebo	k8xC	nebo
Ruby	rub	k1gInPc1	rub
se	se	k3xPyFc4	se
taková	takový	k3xDgFnSc1	takový
hodnota	hodnota	k1gFnSc1	hodnota
označuje	označovat	k5eAaImIp3nS	označovat
klíčovým	klíčový	k2eAgNnSc7d1	klíčové
slovem	slovo	k1gNnSc7	slovo
nil	nil	k?	nil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
assembleru	assembler	k1gInSc6	assembler
o	o	k7c4	o
NULL	NULL	kA	NULL
nebo	nebo	k8xC	nebo
nil	nil	k?	nil
nemluvíme	mluvit	k5eNaImIp1nP	mluvit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
tomu	ten	k3xDgMnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
některý	některý	k3yIgInSc1	některý
z	z	k7c2	z
adresních	adresní	k2eAgInPc2d1	adresní
registrů	registr	k1gInPc2	registr
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgInSc1d1	nulový
–	–	k?	–
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
adresovat	adresovat	k5eAaBmF	adresovat
"	"	kIx"	"
<g/>
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
NULL	NULL	kA	NULL
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
u	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
programovacích	programovací	k2eAgMnPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
nejspíš	nejspíš	k9	nejspíš
skončilo	skončit	k5eAaPmAgNnS	skončit
chybovou	chybový	k2eAgFnSc7d1	chybová
hláškou	hláška	k1gFnSc7	hláška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
architektury	architektura	k1gFnSc2	architektura
PC	PC	kA	PC
AT	AT	kA	AT
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
adresa	adresa	k1gFnSc1	adresa
přerušení	přerušení	k1gNnSc2	přerušení
INT	INT	kA	INT
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
NULL	NULL	kA	NULL
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
ve	v	k7c6	v
skriptovacích	skriptovací	k2eAgInPc6d1	skriptovací
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
striktně	striktně	k6eAd1	striktně
nehlídá	hlídat	k5eNaImIp3nS	hlídat
typy	typ	k1gInPc4	typ
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
použít	použít	k5eAaPmF	použít
NULL	NULL	kA	NULL
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
kontextu	kontext	k1gInSc6	kontext
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
zamění	zaměnit	k5eAaPmIp3nS	zaměnit
na	na	k7c6	na
<g/>
:	:	kIx,	:
0	[number]	k4	0
(	(	kIx(	(
<g/>
nulu	nula	k1gFnSc4	nula
<g/>
)	)	kIx)	)
v	v	k7c6	v
případě	případ	k1gInSc6	případ
aritmetických	aritmetický	k2eAgFnPc2d1	aritmetická
operací	operace	k1gFnPc2	operace
"	"	kIx"	"
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prázdný	prázdný	k2eAgInSc1d1	prázdný
řetězec	řetězec	k1gInSc1	řetězec
<g/>
)	)	kIx)	)
v	v	k7c6	v
případě	případ	k1gInSc6	případ
textových	textový	k2eAgFnPc2d1	textová
operací	operace	k1gFnPc2	operace
false	false	k6eAd1	false
v	v	k7c6	v
případě	případ	k1gInSc6	případ
booleovských	booleovský	k2eAgFnPc2d1	booleovská
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
relačních	relační	k2eAgFnPc6d1	relační
databázích	databáze	k1gFnPc6	databáze
označuje	označovat	k5eAaImIp3nS	označovat
NULL	NULL	kA	NULL
speciální	speciální	k2eAgFnSc4d1	speciální
hodnotu	hodnota	k1gFnSc4	hodnota
uloženou	uložený	k2eAgFnSc4d1	uložená
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
neznámou	známý	k2eNgFnSc4d1	neznámá
<g/>
,	,	kIx,	,
nedefinovanou	definovaný	k2eNgFnSc4d1	nedefinovaná
apod.	apod.	kA	apod.
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnost	vlastnost	k1gFnSc1	vlastnost
sloupce	sloupec	k1gInSc2	sloupec
tabulky	tabulka	k1gFnSc2	tabulka
být	být	k5eAaImF	být
NULL	NULL	kA	NULL
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
podstatná	podstatný	k2eAgFnSc1d1	podstatná
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
nezávisle	závisle	k6eNd1	závisle
(	(	kIx(	(
<g/>
orthogonálně	orthogonálně	k6eAd1	orthogonálně
<g/>
)	)	kIx)	)
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
typu	typ	k1gInSc6	typ
<g/>
,	,	kIx,	,
rozsahu	rozsah	k1gInSc6	rozsah
či	či	k8xC	či
dalších	další	k2eAgFnPc6d1	další
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
CHAR	CHAR	kA	CHAR
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
NULL	NULL	kA	NULL
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
–	–	k?	–
(	(	kIx(	(
<g/>
prázdný	prázdný	k2eAgInSc1d1	prázdný
řetězec	řetězec	k1gInSc1	řetězec
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
NULL	NULL	kA	NULL
–	–	k?	–
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
chápána	chápán	k2eAgFnSc1d1	chápána
jako	jako	k8xC	jako
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
NULL	NULL	kA	NULL
zpravidla	zpravidla	k6eAd1	zpravidla
platí	platit	k5eAaImIp3nS	platit
zvláštní	zvláštní	k2eAgNnPc4d1	zvláštní
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
výpočty	výpočet	k1gInPc4	výpočet
a	a	k8xC	a
porovnávání	porovnávání	k1gNnSc4	porovnávání
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
algebraické	algebraický	k2eAgFnSc2d1	algebraická
operace	operace	k1gFnSc2	operace
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
NULL	NULL	kA	NULL
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
NULL	NULL	kA	NULL
<g/>
.	.	kIx.	.
</s>
<s>
NULL	NULL	kA	NULL
tedy	tedy	k9	tedy
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
a	a	k8xC	a
i	i	k9	i
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
jako	jako	k9	jako
parametr	parametr	k1gInSc1	parametr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
pravý	pravý	k2eAgInSc1d1	pravý
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
SQL	SQL	kA	SQL
vrátí	vrátit	k5eAaPmIp3nS	vrátit
NULL	NULL	kA	NULL
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ryze	ryze	k6eAd1	ryze
matematicky	matematicky	k6eAd1	matematicky
buď	buď	k8xC	buď
nemají	mít	k5eNaImIp3nP	mít
řešení	řešení	k1gNnSc4	řešení
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
chyba	chyba	k1gFnSc1	chyba
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
mimo	mimo	k7c4	mimo
definiční	definiční	k2eAgInSc4d1	definiční
obor	obor	k1gInSc4	obor
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
dotaz	dotaz	k1gInSc1	dotaz
<g/>
:	:	kIx,	:
SELECT	SELECT	kA	SELECT
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
MOD	MOD	kA	MOD
<g/>
(	(	kIx(	(
<g/>
5,0	[number]	k4	5,0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
SQRT	SQRT	kA	SQRT
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
LOG	log	kA	log
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ASIN	ASIN	kA	ASIN
<g/>
(	(	kIx(	(
<g/>
1.1	[number]	k4	1.1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ACOS	ACOS	kA	ACOS
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1.1	[number]	k4	1.1
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
--	--	k?	--
COT	COT	kA	COT
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
...	...	k?	...
<g/>
vrátí	vrátit	k5eAaPmIp3nS	vrátit
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
políčkách	políčko	k1gNnPc6	políčko
hodnotu	hodnota	k1gFnSc4	hodnota
NULL	NULL	kA	NULL
<g/>
.	.	kIx.	.
</s>
<s>
Porovnávání	porovnávání	k1gNnSc1	porovnávání
NULL	NULL	kA	NULL
s	s	k7c7	s
čímkoli	cokoli	k3yInSc7	cokoli
se	se	k3xPyFc4	se
vyhodnotí	vyhodnotit	k5eAaPmIp3nS	vyhodnotit
opět	opět	k6eAd1	opět
hodnotou	hodnota	k1gFnSc7	hodnota
NULL	NULL	kA	NULL
(	(	kIx(	(
<g/>
v	v	k7c6	v
Booleovském	booleovský	k2eAgInSc6d1	booleovský
prostoru	prostor	k1gInSc6	prostor
interpretováno	interpretován	k2eAgNnSc1d1	interpretováno
jako	jako	k8xS	jako
NEPRAVDA	nepravda	k1gFnSc1	nepravda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
speciální	speciální	k2eAgFnSc7d1	speciální
hodnotou	hodnota	k1gFnSc7	hodnota
UNKNOWN	UNKNOWN	kA	UNKNOWN
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
porovnávání	porovnávání	k1gNnSc4	porovnávání
s	s	k7c7	s
NULL	NULL	kA	NULL
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
porovnání	porovnání	k1gNnSc1	porovnání
NULL	NULL	kA	NULL
=	=	kIx~	=
NULL	NULL	kA	NULL
je	být	k5eAaImIp3nS	být
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
neznámá	známý	k2eNgFnSc1d1	neznámá
hodnota	hodnota	k1gFnSc1	hodnota
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nemusí	muset	k5eNaImIp3nS	muset
rovnat	rovnat	k5eAaImF	rovnat
jiné	jiný	k2eAgFnSc3d1	jiná
neznámé	známý	k2eNgFnSc3d1	neznámá
hodnotě	hodnota	k1gFnSc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
se	se	k3xPyFc4	se
SQL	SQL	kA	SQL
lehce	lehko	k6eAd1	lehko
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
oboru	obor	k1gInSc3	obor
fuzzy	fuzza	k1gFnSc2	fuzza
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hodnota	hodnota	k1gFnSc1	hodnota
NULL	NULL	kA	NULL
v	v	k7c6	v
SQL	SQL	kA	SQL
testuje	testovat	k5eAaImIp3nS	testovat
speciálním	speciální	k2eAgInSc7d1	speciální
výrazem	výraz	k1gInSc7	výraz
sloupec	sloupec	k1gInSc1	sloupec
IS	IS	kA	IS
NULL	NULL	kA	NULL
(	(	kIx(	(
<g/>
sloupec	sloupec	k1gInSc1	sloupec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hodnotu	hodnota	k1gFnSc4	hodnota
NULL	NULL	kA	NULL
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
sloupec	sloupec	k1gInSc1	sloupec
IS	IS	kA	IS
NOT	nota	k1gFnPc2	nota
NULL	NULL	kA	NULL
(	(	kIx(	(
<g/>
sloupec	sloupec	k1gInSc1	sloupec
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
hodnotu	hodnota	k1gFnSc4	hodnota
NULL	NULL	kA	NULL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
skriptovacích	skriptovací	k2eAgInPc6d1	skriptovací
jazycích	jazyk	k1gInPc6	jazyk
pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
existuje	existovat	k5eAaImIp3nS	existovat
speciální	speciální	k2eAgFnSc1d1	speciální
funkce	funkce	k1gFnSc1	funkce
(	(	kIx(	(
<g/>
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
is_null	is_null	k1gInSc1	is_null
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
sloupec	sloupec	k1gInSc1	sloupec
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
hodnotu	hodnota	k1gFnSc4	hodnota
NULL	NULL	kA	NULL
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
při	při	k7c6	při
spojování	spojování	k1gNnSc6	spojování
tabulek	tabulka	k1gFnPc2	tabulka
(	(	kIx(	(
<g/>
JOIN	JOIN	kA	JOIN
<g/>
)	)	kIx)	)
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
vzít	vzít	k5eAaPmF	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
a	a	k8xC	a
nadefinovat	nadefinovat	k5eAaPmF	nadefinovat
chování	chování	k1gNnSc4	chování
systému	systém	k1gInSc2	systém
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloupec	sloupec	k1gInSc1	sloupec
má	mít	k5eAaImIp3nS	mít
opravdu	opravdu	k6eAd1	opravdu
hodnotu	hodnota	k1gFnSc4	hodnota
NULL	NULL	kA	NULL
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sloupce	sloupec	k1gInSc2	sloupec
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
definován	definován	k2eAgInSc1d1	definován
unikátní	unikátní	k2eAgInSc1d1	unikátní
klíč	klíč	k1gInSc1	klíč
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
vložit	vložit	k5eAaPmF	vložit
dvě	dva	k4xCgFnPc4	dva
stejné	stejný	k2eAgFnPc4d1	stejná
(	(	kIx(	(
<g/>
duplicitní	duplicitní	k2eAgFnPc4d1	duplicitní
<g/>
)	)	kIx)	)
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
porušena	porušit	k5eAaPmNgFnS	porušit
datová	datový	k2eAgFnSc1d1	datová
integrita	integrita	k1gFnSc1	integrita
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
nějakém	nějaký	k3yIgInSc6	nějaký
úkonu	úkon	k1gInSc6	úkon
manipulujícím	manipulující	k2eAgInSc6d1	manipulující
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
<g/>
)	)	kIx)	)
přesto	přesto	k8xC	přesto
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
potřeba	potřeba	k6eAd1	potřeba
dočasně	dočasně	k6eAd1	dočasně
duplicitní	duplicitní	k2eAgFnSc4d1	duplicitní
hodnotu	hodnota	k1gFnSc4	hodnota
zapsat	zapsat	k5eAaPmF	zapsat
<g/>
.	.	kIx.	.
</s>
<s>
SŘBD	SŘBD	kA	SŘBD
pak	pak	k6eAd1	pak
zareaguje	zareagovat	k5eAaPmIp3nS	zareagovat
chybovou	chybový	k2eAgFnSc7d1	chybová
hláškou	hláška	k1gFnSc7	hláška
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
z	z	k7c2	z
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
unikátním	unikátní	k2eAgInSc6d1	unikátní
klíči	klíč	k1gInSc6	klíč
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
"	"	kIx"	"
<g/>
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
"	"	kIx"	"
NULL	NULL	kA	NULL
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnPc1	konstrukce
WITH	WITH	kA	WITH
ROLLUP	ROLLUP	kA	ROLLUP
u	u	k7c2	u
agregačních	agregační	k2eAgFnPc2d1	agregační
funkcí	funkce	k1gFnPc2	funkce
používá	používat	k5eAaImIp3nS	používat
NULL	NULL	kA	NULL
speciálním	speciální	k2eAgInSc7d1	speciální
způsobem	způsob	k1gInSc7	způsob
–	–	k?	–
jako	jako	k9	jako
hodnotu	hodnota	k1gFnSc4	hodnota
sjednocující	sjednocující	k2eAgInPc4d1	sjednocující
agregované	agregovaný	k2eAgInPc4d1	agregovaný
sloupečky	sloupeček	k1gInPc4	sloupeček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
unixových	unixový	k2eAgInPc6d1	unixový
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
soubor	soubor	k1gInSc1	soubor
/	/	kIx~	/
<g/>
dev	dev	k?	dev
<g/>
/	/	kIx~	/
<g/>
null	null	k1gMnSc1	null
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
tzv.	tzv.	kA	tzv.
bit	bit	k2eAgInSc1d1	bit
bucket	bucket	k1gInSc1	bucket
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgMnSc2	který
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
cokoli	cokoli	k3yInSc4	cokoli
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
veškerá	veškerý	k3xTgNnPc1	veškerý
zapsaná	zapsaný	k2eAgNnPc1d1	zapsané
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
nenávratně	návratně	k6eNd1	návratně
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
operačních	operační	k2eAgInPc6d1	operační
systémem	systém	k1gInSc7	systém
firmy	firma	k1gFnSc2	firma
Microsoft	Microsoft	kA	Microsoft
(	(	kIx(	(
<g/>
MS-DOS	MS-DOS	k1gMnSc1	MS-DOS
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
takové	takový	k3xDgNnSc1	takový
zařízení	zařízení	k1gNnSc1	zařízení
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
NUL	nula	k1gFnPc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
soubory	soubor	k1gInPc1	soubor
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
zahazování	zahazování	k1gNnSc3	zahazování
nežádoucích	žádoucí	k2eNgNnPc2d1	nežádoucí
hlášení	hlášení	k1gNnPc2	hlášení
programu	program	k1gInSc2	program
pomocí	pomocí	k7c2	pomocí
přesměrování	přesměrování	k1gNnSc2	přesměrování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
program	program	k1gInSc1	program
>	>	kIx)	>
/	/	kIx~	/
<g/>
dev	dev	k?	dev
<g/>
/	/	kIx~	/
<g/>
null	null	k1gMnSc1	null
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
0	[number]	k4	0
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
NaN	NaN	k1gFnSc1	NaN
</s>
