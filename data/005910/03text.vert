<s>
Guido	Guido	k1gNnSc1	Guido
van	vana	k1gFnPc2	vana
Rossum	Rossum	k1gInSc1	Rossum
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
počítačový	počítačový	k2eAgMnSc1d1	počítačový
programátor	programátor	k1gMnSc1	programátor
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
především	především	k6eAd1	především
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Rossum	Rossum	k1gInSc1	Rossum
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
a	a	k8xC	a
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
získal	získat	k5eAaPmAgInS	získat
magisterský	magisterský	k2eAgInSc1d1	magisterský
titul	titul	k1gInSc1	titul
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
výzkumných	výzkumný	k2eAgFnPc6d1	výzkumná
institucích	instituce	k1gFnPc6	instituce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
National	National	k1gFnSc2	National
Research	Research	k1gMnSc1	Research
Institute	institut	k1gInSc5	institut
for	forum	k1gNnPc2	forum
Mathematics	Mathematics	k1gInSc1	Mathematics
and	and	k?	and
Computer	computer	k1gInSc1	computer
Science	Science	k1gFnSc1	Science
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc1d1	národní
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
informatiku	informatika	k1gFnSc4	informatika
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
CWI	CWI	kA	CWI
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
Institute	institut	k1gInSc5	institut
of	of	k?	of
Standards	Standards	k1gInSc1	Standards
and	and	k?	and
Technology	technolog	k1gMnPc7	technolog
(	(	kIx(	(
<g/>
Národní	národní	k2eAgInSc1d1	národní
institut	institut	k1gInSc1	institut
standardů	standard	k1gInPc2	standard
a	a	k8xC	a
technologie	technologie	k1gFnSc2	technologie
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
NIST	NIST	kA	NIST
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Gaithersburg	Gaithersburg	k1gInSc1	Gaithersburg
<g/>
,	,	kIx,	,
Maryland	Maryland	k1gInSc1	Maryland
<g/>
)	)	kIx)	)
a	a	k8xC	a
Corporation	Corporation	k1gInSc4	Corporation
for	forum	k1gNnPc2	forum
National	National	k1gMnSc2	National
Research	Researcha	k1gFnPc2	Researcha
Initiatives	Initiatives	k1gMnSc1	Initiatives
(	(	kIx(	(
<g/>
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
iniciativu	iniciativa	k1gFnSc4	iniciativa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
CNRI	CNRI	kA	CNRI
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Reston	Reston	k1gInSc1	Reston
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc1	Virginium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
vycházejícího	vycházející	k2eAgInSc2d1	vycházející
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
Simula	Simulum	k1gNnSc2	Simulum
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
původu	původ	k1gInSc6	původ
Pythonu	Python	k1gMnSc6	Python
napsal	napsat	k5eAaBmAgInS	napsat
Van	van	k1gInSc1	van
Rossum	Rossum	k1gInSc1	Rossum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
toto	tento	k3xDgNnSc1	tento
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Před	před	k7c7	před
šesti	šest	k4xCc7	šest
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
hledal	hledat	k5eAaImAgMnS	hledat
zábavný	zábavný	k2eAgInSc4d1	zábavný
programátorský	programátorský	k2eAgInSc4d1	programátorský
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
by	by	kYmCp3nS	by
mě	já	k3xPp1nSc4	já
zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
přes	přes	k7c4	přes
týden	týden	k1gInSc4	týden
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Má	můj	k3xOp1gFnSc1	můj
kancelář	kancelář	k1gFnSc1	kancelář
...	...	k?	...
byla	být	k5eAaImAgFnS	být
zavřená	zavřený	k2eAgFnSc1d1	zavřená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
jsem	být	k5eAaImIp1nS	být
domácí	domácí	k2eAgInSc4d1	domácí
počítač	počítač	k1gInSc4	počítač
a	a	k8xC	a
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
na	na	k7c6	na
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
napíšu	napsat	k5eAaPmIp1nS	napsat
interpret	interpret	k1gMnSc1	interpret
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
skriptovací	skriptovací	k2eAgInSc4d1	skriptovací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
jsem	být	k5eAaImIp1nS	být
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
<g/>
:	:	kIx,	:
následníka	následník	k1gMnSc2	následník
jazyka	jazyk	k1gInSc2	jazyk
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
programátory	programátor	k1gMnPc4	programátor
v	v	k7c6	v
Unixu	Unix	k1gInSc6	Unix
<g/>
/	/	kIx~	/
<g/>
C.	C.	kA	C.
Jako	jako	k8xC	jako
pracovní	pracovní	k2eAgInSc4d1	pracovní
název	název	k1gInSc4	název
jsem	být	k5eAaImIp1nS	být
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Python	Python	k1gMnSc1	Python
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
nevážné	vážný	k2eNgFnSc6d1	nevážná
<g />
.	.	kIx.	.
</s>
<s>
náladě	nálada	k1gFnSc3	nálada
(	(	kIx(	(
<g/>
a	a	k8xC	a
taky	taky	k6eAd1	taky
jsem	být	k5eAaImIp1nS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
Monty	Monta	k1gFnSc2	Monta
Pythonova	Pythonův	k2eAgInSc2d1	Pythonův
Létajícího	létající	k2eAgInSc2d1	létající
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Úvod	úvod	k1gInSc4	úvod
knihy	kniha	k1gFnSc2	kniha
Programming	Programming	k1gInSc4	Programming
Python	Python	k1gMnSc1	Python
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Lutz	Lutz	k1gMnSc1	Lutz
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Reilly	Reilla	k1gFnPc4	Reilla
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
poslal	poslat	k5eAaPmAgInS	poslat
Van	van	k1gInSc1	van
Rossum	Rossum	k1gInSc1	Rossum
agentuře	agentura	k1gFnSc6	agentura
DARPA	DARPA	kA	DARPA
finanční	finanční	k2eAgFnSc2d1	finanční
návrh	návrh	k1gInSc4	návrh
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Computer	computer	k1gInSc1	computer
Programming	Programming	k1gInSc1	Programming
for	forum	k1gNnPc2	forum
Everybody	Everyboda	k1gFnSc2	Everyboda
(	(	kIx(	(
<g/>
Počítačové	počítačový	k2eAgNnSc1d1	počítačové
programování	programování	k1gNnSc1	programování
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
hlouběji	hluboko	k6eAd2	hluboko
definoval	definovat	k5eAaBmAgMnS	definovat
vytyčené	vytyčený	k2eAgInPc4d1	vytyčený
cíle	cíl	k1gInPc4	cíl
Pythonu	Python	k1gMnSc3	Python
<g/>
:	:	kIx,	:
snadný	snadný	k2eAgInSc1d1	snadný
a	a	k8xC	a
intuitivní	intuitivní	k2eAgInSc1d1	intuitivní
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
dostatečně	dostatečně	k6eAd1	dostatečně
mocný	mocný	k2eAgMnSc1d1	mocný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obstál	obstát	k5eAaPmAgInS	obstát
mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgMnPc7d1	hlavní
konkurenty	konkurent	k1gMnPc7	konkurent
otevřený	otevřený	k2eAgInSc4d1	otevřený
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
každý	každý	k3xTgMnSc1	každý
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
vývoje	vývoj	k1gInSc2	vývoj
kód	kód	k1gInSc1	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
srozumitelný	srozumitelný	k2eAgInSc1d1	srozumitelný
jako	jako	k8xS	jako
běžná	běžný	k2eAgFnSc1d1	běžná
angličtina	angličtina	k1gFnSc1	angličtina
vhodný	vhodný	k2eAgInSc4d1	vhodný
pro	pro	k7c4	pro
běžné	běžný	k2eAgInPc4d1	běžný
každodenní	každodenní	k2eAgInPc4d1	každodenní
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc4d1	umožňující
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
Tyto	tento	k3xDgFnPc4	tento
ambice	ambice	k1gFnPc1	ambice
jsou	být	k5eAaImIp3nP	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
naplněny	naplnit	k5eAaPmNgFnP	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
populárním	populární	k2eAgMnSc7d1	populární
programovacím	programovací	k2eAgInSc7d1	programovací
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
Internetu	Internet	k1gInSc2	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
komunitou	komunita	k1gFnSc7	komunita
Pythonu	Python	k1gMnSc6	Python
je	být	k5eAaImIp3nS	být
Van	van	k1gInSc1	van
Rossum	Rossum	k1gInSc1	Rossum
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
Benevolent	Benevolent	k1gInSc4	Benevolent
Dictator	Dictator	k1gInSc4	Dictator
for	forum	k1gNnPc2	forum
Life	Lif	k1gMnSc2	Lif
(	(	kIx(	(
<g/>
BDFL	BDFL	kA	BDFL
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
benevolentní	benevolentní	k2eAgMnPc1d1	benevolentní
doživotní	doživotní	k2eAgMnSc1d1	doživotní
diktátor	diktátor	k1gMnSc1	diktátor
<g/>
)	)	kIx)	)
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
dohledu	dohled	k1gInSc6	dohled
nad	nad	k7c7	nad
procesem	proces	k1gInSc7	proces
vývoje	vývoj	k1gInSc2	vývoj
Pythonu	Python	k1gMnSc3	Python
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
významná	významný	k2eAgNnPc4d1	významné
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
obdržel	obdržet	k5eAaPmAgMnS	obdržet
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
FOSDEM	FOSDEM	kA	FOSDEM
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
od	od	k7c2	od
FSF	FSF	kA	FSF
Free	Free	k1gInSc1	Free
Software	software	k1gInSc1	software
Award	Award	k1gInSc4	Award
(	(	kIx(	(
<g/>
Ocenění	ocenění	k1gNnSc2	ocenění
svobodného	svobodný	k2eAgNnSc2d1	svobodné
software	software	k1gInSc4	software
<g/>
)	)	kIx)	)
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Guido	Guido	k1gNnSc1	Guido
van	vana	k1gFnPc2	vana
Rossum	Rossum	k1gInSc4	Rossum
je	být	k5eAaImIp3nS	být
bratrem	bratr	k1gMnSc7	bratr
Justa	Just	k1gMnSc2	Just
Van	vana	k1gFnPc2	vana
Rossuma	Rossum	k1gMnSc2	Rossum
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
návrháře	návrhář	k1gMnSc2	návrhář
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
design	design	k1gInSc1	design
fontu	font	k1gInSc2	font
<g/>
,	,	kIx,	,
použitého	použitý	k2eAgInSc2d1	použitý
v	v	k7c6	v
logu	log	k1gInSc6	log
"	"	kIx"	"
<g/>
Python	Python	k1gMnSc1	Python
powered	powered	k1gMnSc1	powered
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Guido	Guido	k1gNnSc1	Guido
van	vana	k1gFnPc2	vana
Rossum	Rossum	k1gNnSc4	Rossum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Guido	Guido	k1gNnSc1	Guido
van	van	k1gInSc1	van
Rossuma	Rossuma	k1gFnSc1	Rossuma
All	All	k1gMnPc2	All
Things	Things	k1gInSc4	Things
Pythonic	Pythonice	k1gFnPc2	Pythonice
(	(	kIx(	(
<g/>
Weblog	Weblog	k1gInSc1	Weblog
<g/>
)	)	kIx)	)
Computer	computer	k1gInSc1	computer
Programming	Programming	k1gInSc1	Programming
for	forum	k1gNnPc2	forum
Everybody	Everyboda	k1gFnSc2	Everyboda
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Pythonu	Python	k1gMnSc3	Python
</s>
