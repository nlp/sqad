<p>
<s>
Štír	štír	k1gMnSc1	štír
kýlnatý	kýlnatý	k2eAgMnSc1d1	kýlnatý
(	(	kIx(	(
<g/>
Euscorpius	Euscorpius	k1gMnSc1	Euscorpius
tergestinus	tergestinus	k1gMnSc1	tergestinus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
štíra	štír	k1gMnSc2	štír
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Euscorpius	Euscorpius	k1gInSc1	Euscorpius
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
nezvěstný	zvěstný	k2eNgInSc1d1	nezvěstný
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
opětovném	opětovný	k2eAgInSc6d1	opětovný
nálezu	nález	k1gInSc6	nález
štírů	štír	k1gMnPc2	štír
dosud	dosud	k6eAd1	dosud
nebyly	být	k5eNaImAgInP	být
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
oficiálně	oficiálně	k6eAd1	oficiálně
vyvráceny	vyvrácen	k2eAgInPc1d1	vyvrácen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejmenší	malý	k2eAgMnSc1d3	nejmenší
štír	štír	k1gMnSc1	štír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
současný	současný	k2eAgInSc1d1	současný
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
limitován	limitovat	k5eAaBmNgInS	limitovat
jen	jen	k9	jen
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
populace	populace	k1gFnSc2	populace
na	na	k7c6	na
slapské	slapský	k2eAgFnSc6d1	Slapská
lokalitě	lokalita	k1gFnSc6	lokalita
podobné	podobný	k2eAgFnSc6d1	podobná
stráni	stráň	k1gFnSc6	stráň
blízko	blízko	k7c2	blízko
rakouského	rakouský	k2eAgNnSc2d1	rakouské
města	město	k1gNnSc2	město
Kremže	Kremže	k1gFnSc2	Kremže
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
prakticky	prakticky	k6eAd1	prakticky
totožný	totožný	k2eAgInSc1d1	totožný
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
biogeografický	biogeografický	k2eAgInSc1d1	biogeografický
rámec	rámec	k1gInSc1	rámec
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
pěti	pět	k4xCc6	pět
zemích	zem	k1gFnPc6	zem
svého	svůj	k3xOyFgInSc2	svůj
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
rozmanitá	rozmanitý	k2eAgNnPc4d1	rozmanité
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
:	:	kIx,	:
okraje	okraj	k1gInPc1	okraj
a	a	k8xC	a
nitra	nitro	k1gNnPc1	nitro
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
sklepy	sklep	k1gInPc4	sklep
<g/>
,	,	kIx,	,
půdy	půda	k1gFnPc4	půda
<g/>
,	,	kIx,	,
zídky	zídka	k1gFnPc4	zídka
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
apod.	apod.	kA	apod.
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
délky	délka	k1gFnPc4	délka
okolo	okolo	k7c2	okolo
4	[number]	k4	4
cm	cm	kA	cm
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
jed	jed	k1gInSc1	jed
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
je	on	k3xPp3gInPc4	on
nosí	nosit	k5eAaImIp3nS	nosit
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
svlékání	svlékání	k1gNnSc2	svlékání
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
nepřijímají	přijímat	k5eNaImIp3nP	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
kanibalismus	kanibalismus	k1gInSc1	kanibalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Krči	Krč	k1gFnSc6	Krč
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
kolonie	kolonie	k1gFnSc1	kolonie
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tohoto	tento	k3xDgMnSc4	tento
štíra	štír	k1gMnSc4	štír
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
nedaleko	nedaleko	k7c2	nedaleko
potoka	potok	k1gInSc2	potok
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yQnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
12	[number]	k4	12
dospělých	dospělý	k2eAgMnPc2d1	dospělý
štírů	štír	k1gMnPc2	štír
a	a	k8xC	a
3	[number]	k4	3
starší	starý	k2eAgFnPc4d2	starší
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Štíři	štír	k1gMnPc1	štír
byli	být	k5eAaImAgMnP	být
vysazeni	vysazen	k2eAgMnPc1d1	vysazen
nebo	nebo	k8xC	nebo
zavlečeni	zavlečen	k2eAgMnPc1d1	zavlečen
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
nebyl	být	k5eNaImAgMnS	být
nalezen	nalezen	k2eAgMnSc1d1	nalezen
žádný	žádný	k1gMnSc1	žádný
štír	štír	k1gMnSc1	štír
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
13	[number]	k4	13
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
1	[number]	k4	1
dospělec	dospělec	k1gMnSc1	dospělec
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nepůvodní	původní	k2eNgInSc1d1	nepůvodní
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
štír	štír	k1gMnSc1	štír
není	být	k5eNaImIp3nS	být
zas	zas	k6eAd1	zas
tak	tak	k6eAd1	tak
běžně	běžně	k6eAd1	běžně
chovaný	chovaný	k2eAgMnSc1d1	chovaný
zástupce	zástupce	k1gMnSc1	zástupce
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
nenáročností	nenáročnost	k1gFnSc7	nenáročnost
chovu	chov	k1gInSc2	chov
velmi	velmi	k6eAd1	velmi
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
teráriu	terárium	k1gNnSc6	terárium
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgNnP	mít
pohybovat	pohybovat	k5eAaImF	pohybovat
od	od	k7c2	od
22-27	[number]	k4	22-27
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
docílíme	docílit	k5eAaPmIp1nP	docílit
přitápěním	přitápění	k1gNnPc3	přitápění
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bodové	bodový	k2eAgFnSc2d1	bodová
žárovky	žárovka	k1gFnSc2	žárovka
či	či	k8xC	či
topného	topný	k2eAgInSc2d1	topný
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
chovatelů	chovatel	k1gMnPc2	chovatel
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgMnPc4	tento
štíry	štír	k1gMnPc4	štír
chová	chovat	k5eAaImIp3nS	chovat
při	při	k7c6	při
běžné	běžný	k2eAgFnSc6d1	běžná
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
štíry	štír	k1gMnPc4	štír
se	se	k3xPyFc4	se
jim	on	k3xPp3gFnPc3	on
daří	dařit	k5eAaImIp3nS	dařit
i	i	k9	i
odchovávat	odchovávat	k5eAaImF	odchovávat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
substrát	substrát	k1gInSc1	substrát
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgMnSc1d1	vhodný
lignocel	lignocet	k5eAaPmAgMnS	lignocet
<g/>
,	,	kIx,	,
rašelina	rašelina	k1gFnSc1	rašelina
či	či	k8xC	či
kousky	kousek	k1gInPc1	kousek
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
hrabanka	hrabanka	k1gFnSc1	hrabanka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
úkrytů	úkryt	k1gInPc2	úkryt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dobře	dobře	k6eAd1	dobře
poslouží	posloužit	k5eAaPmIp3nS	posloužit
kůra	kůra	k1gFnSc1	kůra
nebo	nebo	k8xC	nebo
rozbitý	rozbitý	k2eAgInSc1d1	rozbitý
květináč	květináč	k1gInSc1	květináč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
potravu	potrava	k1gFnSc4	potrava
předkládáme	předkládat	k5eAaImIp1nP	předkládat
hmyz	hmyz	k1gInSc4	hmyz
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgMnSc1d3	nejvhodnější
jsou	být	k5eAaImIp3nP	být
cvrčci	cvrček	k1gMnPc1	cvrček
<g/>
,	,	kIx,	,
švábi	šváb	k1gMnPc1	šváb
<g/>
,	,	kIx,	,
kobylky	kobylka	k1gFnPc1	kobylka
a	a	k8xC	a
červi	červ	k1gMnPc1	červ
<g/>
.	.	kIx.	.
</s>
<s>
Odchov	odchov	k1gInSc1	odchov
těchto	tento	k3xDgMnPc2	tento
štírů	štír	k1gMnPc2	štír
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
podobný	podobný	k2eAgInSc1d1	podobný
ostatním	ostatní	k2eAgMnPc3d1	ostatní
zástupcům	zástupce	k1gMnPc3	zástupce
téhož	týž	k3xTgInSc2	týž
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
Slapské	slapský	k2eAgFnSc2d1	Slapská
přehrady	přehrada	k1gFnSc2	přehrada
existovala	existovat	k5eAaImAgFnS	existovat
kolonie	kolonie	k1gFnSc1	kolonie
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
dvě	dva	k4xCgFnPc4	dva
kolonie	kolonie	k1gFnPc4	kolonie
)	)	kIx)	)
tohoto	tento	k3xDgMnSc4	tento
štíra	štír	k1gMnSc4	štír
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
hotelu	hotel	k1gInSc2	hotel
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
lokalita	lokalita	k1gFnSc1	lokalita
byla	být	k5eAaImAgFnS	být
místem	místo	k1gNnSc7	místo
výskytu	výskyt	k1gInSc2	výskyt
dalších	další	k2eAgInPc2d1	další
vzácných	vzácný	k2eAgInPc2d1	vzácný
pavoukovců	pavoukovec	k1gInPc2	pavoukovec
a	a	k8xC	a
jižních	jižní	k2eAgFnPc2d1	jižní
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
<g/>
,	,	kIx,	,
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
stepníci	stepník	k1gMnPc1	stepník
a	a	k8xC	a
kudlanky	kudlanka	k1gFnPc1	kudlanka
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
podobnou	podobný	k2eAgFnSc7d1	podobná
lokalitou	lokalita	k1gFnSc7	lokalita
v	v	k7c6	v
Kremži	Kremže	k1gFnSc6	Kremže
a	a	k8xC	a
genetickým	genetický	k2eAgFnPc3d1	genetická
analýzám	analýza	k1gFnPc3	analýza
existuje	existovat	k5eAaImIp3nS	existovat
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
drasticky	drasticky	k6eAd1	drasticky
zredukováno	zredukovat	k5eAaPmNgNnS	zredukovat
dobou	doba	k1gFnSc7	doba
ledovou	ledový	k2eAgFnSc7d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
panovala	panovat	k5eAaImAgFnS	panovat
domněnka	domněnka	k1gFnSc1	domněnka
o	o	k7c6	o
zavlečení	zavlečení	k1gNnSc6	zavlečení
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
i	i	k9	i
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
možné	možný	k2eAgInPc4d1	možný
zdroje	zdroj	k1gInPc4	zdroj
zavlečení	zavlečení	k1gNnSc2	zavlečení
se	se	k3xPyFc4	se
uvádělo	uvádět	k5eAaImAgNnS	uvádět
dovážení	dovážení	k1gNnSc1	dovážení
kamene	kámen	k1gInSc2	kámen
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
hradu	hrad	k1gInSc2	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zavlečení	zavlečený	k2eAgMnPc1d1	zavlečený
chataři	chatař	k1gMnPc1	chatař
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
zavlečení	zavlečení	k1gNnSc1	zavlečení
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
štírů	štír	k1gMnPc2	štír
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
již	již	k6eAd1	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
o	o	k7c6	o
něco	něco	k6eAd1	něco
dávnějších	dávný	k2eAgFnPc6d2	dávnější
a	a	k8xC	a
kolonie	kolonie	k1gFnSc2	kolonie
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
choulostivost	choulostivost	k1gFnSc4	choulostivost
mláďat	mládě	k1gNnPc2	mládě
sdílenou	sdílený	k2eAgFnSc4d1	sdílená
všemi	všecek	k3xTgMnPc7	všecek
štíry	štír	k1gMnPc7	štír
a	a	k8xC	a
množství	množství	k1gNnSc3	množství
potenciálních	potenciální	k2eAgFnPc2d1	potenciální
nebezpečí	nebezpeč	k1gFnPc2wB	nebezpeč
a	a	k8xC	a
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
svědectví	svědectví	k1gNnSc2	svědectví
početná	početný	k2eAgFnSc1d1	početná
a	a	k8xC	a
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
.	.	kIx.	.
</s>
<s>
Štíři	štír	k1gMnPc1	štír
zavlečení	zavlečení	k1gNnPc2	zavlečení
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
či	či	k8xC	či
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
nevytvořili	vytvořit	k5eNaPmAgMnP	vytvořit
trvale	trvale	k6eAd1	trvale
existující	existující	k2eAgFnSc4d1	existující
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
rozmnožující	rozmnožující	k2eAgFnSc1d1	rozmnožující
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
původních	původní	k2eAgFnPc6d1	původní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Synantropní	Synantropní	k2eAgInSc1d1	Synantropní
Euscorpius	Euscorpius	k1gInSc1	Euscorpius
flavicaudis	flavicaudis	k1gFnSc2	flavicaudis
se	se	k3xPyFc4	se
však	však	k9	však
stal	stát	k5eAaPmAgInS	stát
prosperujícím	prosperující	k2eAgMnSc7d1	prosperující
druhem	druh	k1gMnSc7	druh
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
není	být	k5eNaImIp3nS	být
původní	původní	k2eAgInSc4d1	původní
druh	druh	k1gInSc4	druh
anglické	anglický	k2eAgFnSc2d1	anglická
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
a	a	k8xC	a
během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
zde	zde	k6eAd1	zde
zdomácněl	zdomácnět	k5eAaPmAgInS	zdomácnět
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
i	i	k8xC	i
jemu	on	k3xPp3gMnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
vymření	vymření	k1gNnPc4	vymření
vlivem	vliv	k1gInSc7	vliv
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
