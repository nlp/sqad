<p>
<s>
Llŷ	Llŷ	k?	Llŷ
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
poangličtěně	poangličtěně	k6eAd1	poangličtěně
Lleyn	Lleyn	k1gNnSc1	Lleyn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poloostrov	poloostrov	k1gInSc4	poloostrov
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
Walesu	Wales	k1gInSc6	Wales
u	u	k7c2	u
Irského	irský	k2eAgNnSc2d1	irské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Anglesey	Anglesea	k1gFnSc2	Anglesea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
poloostrova	poloostrov	k1gInSc2	poloostrov
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
<g/>
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
poloostrov	poloostrov	k1gInSc4	poloostrov
cestovali	cestovat	k5eAaImAgMnP	cestovat
poutníci	poutník	k1gMnPc1	poutník
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Enlli	Enlle	k1gFnSc4	Enlle
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zde	zde	k6eAd1	zde
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
velšsky	velšsky	k6eAd1	velšsky
mluvících	mluvící	k2eAgMnPc2d1	mluvící
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
jazyk	jazyk	k1gInSc1	jazyk
používán	používat	k5eAaImNgInS	používat
nejvíce	hodně	k6eAd3	hodně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
leží	ležet	k5eAaImIp3nS	ležet
například	například	k6eAd1	například
obce	obec	k1gFnSc2	obec
či	či	k8xC	či
vesnice	vesnice	k1gFnSc2	vesnice
Aberdaron	Aberdaron	k1gMnSc1	Aberdaron
<g/>
,	,	kIx,	,
Llanaelhaearn	Llanaelhaearn	k1gMnSc1	Llanaelhaearn
<g/>
,	,	kIx,	,
Llannor	Llannor	k1gMnSc1	Llannor
<g/>
,	,	kIx,	,
Llanystumdwy	Llanystumdwa	k1gFnPc1	Llanystumdwa
<g/>
,	,	kIx,	,
Nefyn	Nefyn	k1gInSc1	Nefyn
a	a	k8xC	a
Tudweiliog	Tudweiliog	k1gInSc1	Tudweiliog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Llŷ	Llŷ	k1gFnSc2	Llŷ
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
