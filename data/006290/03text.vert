<s>
Román	román	k1gInSc1	román
Laskavé	laskavý	k2eAgFnSc2d1	laskavá
bohyně	bohyně	k1gFnSc2	bohyně
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Les	les	k1gInSc1	les
Bienveillantes	Bienveillantes	k1gInSc1	Bienveillantes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
amerického	americký	k2eAgMnSc2d1	americký
autora	autor	k1gMnSc2	autor
Jonathana	Jonathan	k1gMnSc2	Jonathan
Littella	Littell	k1gMnSc2	Littell
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
originále	originál	k1gInSc6	originál
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
pak	pak	k6eAd1	pak
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
svého	svůj	k3xOyFgNnSc2	svůj
vydání	vydání	k1gNnSc2	vydání
získala	získat	k5eAaPmAgFnS	získat
kniha	kniha	k1gFnSc1	kniha
prestižní	prestižní	k2eAgFnSc4d1	prestižní
Cenu	cena	k1gFnSc4	cena
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Akademie	akademie	k1gFnSc2	akademie
a	a	k8xC	a
Goncourtovu	Goncourtův	k2eAgFnSc4d1	Goncourtova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Devítisetstránková	devítisetstránkový	k2eAgFnSc1d1	devítisetstránková
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
historickou	historický	k2eAgFnSc7d1	historická
fikcí	fikce	k1gFnSc7	fikce
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
skutečných	skutečný	k2eAgFnPc6d1	skutečná
událostech	událost	k1gFnPc6	událost
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pamětí	paměť	k1gFnPc2	paměť
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
životě	život	k1gInSc6	život
nacistického	nacistický	k2eAgMnSc2d1	nacistický
úředníka	úředník	k1gMnSc2	úředník
Maximiliena	Maximilien	k1gMnSc2	Maximilien
Aueho	Aue	k1gMnSc2	Aue
<g/>
,	,	kIx,	,
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
roli	role	k1gFnSc4	role
v	v	k7c6	v
systematickém	systematický	k2eAgNnSc6d1	systematické
vyhlazování	vyhlazování	k1gNnSc6	vyhlazování
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
vnímání	vnímání	k1gNnSc6	vnímání
této	tento	k3xDgFnSc2	tento
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
knihy	kniha	k1gFnSc2	kniha
staví	stavit	k5eAaImIp3nS	stavit
čtenáře	čtenář	k1gMnPc4	čtenář
před	před	k7c4	před
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
po	po	k7c6	po
dočtení	dočtení	k1gNnSc6	dočtení
knihy	kniha	k1gFnSc2	kniha
často	často	k6eAd1	často
neexistuje	existovat	k5eNaImIp3nS	existovat
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
by	by	kYmCp3nP	by
reagovali	reagovat	k5eAaBmAgMnP	reagovat
a	a	k8xC	a
jak	jak	k8xC	jak
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
chovali	chovat	k5eAaImAgMnP	chovat
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
Aue	Aue	k1gMnSc1	Aue
<g/>
.	.	kIx.	.
</s>
<s>
Netradiční	tradiční	k2eNgFnSc1d1	netradiční
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
námět	námět	k1gInSc4	námět
knihy	kniha	k1gFnSc2	kniha
(	(	kIx(	(
<g/>
a	a	k8xC	a
absence	absence	k1gFnSc1	absence
morálního	morální	k2eAgNnSc2d1	morální
hodnocení	hodnocení	k1gNnSc2	hodnocení
Aueho	Aue	k1gMnSc2	Aue
skutků	skutek	k1gInPc2	skutek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
struktura	struktura	k1gFnSc1	struktura
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
podkapitoly	podkapitola	k1gFnPc4	podkapitola
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
(	(	kIx(	(
<g/>
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
malého	malý	k2eAgInSc2d1	malý
počtu	počet	k1gInSc2	počet
odstavců	odstavec	k1gInPc2	odstavec
<g/>
,	,	kIx,	,
časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
složitých	složitý	k2eAgFnPc2d1	složitá
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zabírají	zabírat	k5eAaImIp3nP	zabírat
dvě	dva	k4xCgNnPc1	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
premiéře	premiéra	k1gFnSc6	premiéra
Divadlem	divadlo	k1gNnSc7	divadlo
pod	pod	k7c7	pod
Palmovkou	Palmovka	k1gFnSc7	Palmovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
je	být	k5eAaImIp3nS	být
představena	představen	k2eAgFnSc1d1	představena
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stručnosti	stručnost	k1gFnSc6	stručnost
je	být	k5eAaImIp3nS	být
nastíněn	nastíněn	k2eAgInSc4d1	nastíněn
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
po	po	k7c4	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgInPc4d1	světový
válce	válec	k1gInPc4	válec
a	a	k8xC	a
důvody	důvod	k1gInPc4	důvod
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
sepsat	sepsat	k5eAaPmF	sepsat
paměti	paměť	k1gFnPc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
popis	popis	k1gInSc1	popis
historických	historický	k2eAgFnPc2d1	historická
i	i	k8xC	i
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
událostí	událost	k1gFnPc2	událost
-	-	kIx~	-
jeho	jeho	k3xOp3gFnPc4	jeho
práce	práce	k1gFnPc4	práce
v	v	k7c4	v
Einsatzgruppen	Einsatzgruppen	k2eAgInSc4d1	Einsatzgruppen
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Babi	Babi	k?	Babi
Jaru	jar	k1gInSc6	jar
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
vydání	vydání	k1gNnSc6	vydání
knihy	kniha	k1gFnSc2	kniha
figuruje	figurovat	k5eAaImIp3nS	figurovat
jinak	jinak	k6eAd1	jinak
méně	málo	k6eAd2	málo
používaný	používaný	k2eAgInSc1d1	používaný
český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
Babí	babí	k2eAgFnSc2d1	babí
rokle	rokle	k1gFnSc2	rokle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
dovolená	dovolená	k1gFnSc1	dovolená
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
<g/>
,	,	kIx,	,
následné	následný	k2eAgNnSc4d1	následné
nasazení	nasazení	k1gNnSc4	nasazení
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
záchrana	záchrana	k1gFnSc1	záchrana
po	po	k7c6	po
vážném	vážný	k2eAgNnSc6d1	vážné
zranění	zranění	k1gNnSc6	zranění
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
ve	v	k7c6	v
Stalingradu	Stalingrad	k1gInSc3	Stalingrad
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
rekonvalescence	rekonvalescence	k1gFnSc1	rekonvalescence
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
návštěva	návštěva	k1gFnSc1	návštěva
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
zahrnující	zahrnující	k2eAgFnPc1d1	zahrnující
návštěvy	návštěva	k1gFnPc1	návštěva
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
události	událost	k1gFnPc1	událost
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
včetně	včetně	k7c2	včetně
plánování	plánování	k1gNnSc2	plánování
transportů	transport	k1gInPc2	transport
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
bombardování	bombardování	k1gNnSc1	bombardování
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
návštěvu	návštěva	k1gFnSc4	návštěva
u	u	k7c2	u
sestry	sestra	k1gFnSc2	sestra
v	v	k7c6	v
Pomořanech	Pomořany	k1gInPc6	Pomořany
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
obleženého	obležený	k2eAgInSc2d1	obležený
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
útěk	útěk	k1gInSc4	útěk
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rovněž	rovněž	k9	rovněž
popis	popis	k1gInSc1	popis
Aueho	Aue	k1gMnSc2	Aue
incestního	incestní	k2eAgInSc2d1	incestní
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
bisexuální	bisexuální	k2eAgInPc1d1	bisexuální
sklony	sklon	k1gInPc1	sklon
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
fantazií	fantazie	k1gFnPc2	fantazie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
řadu	řada	k1gFnSc4	řada
známých	známý	k2eAgFnPc2d1	známá
událostí	událost	k1gFnPc2	událost
-	-	kIx~	-
Aue	Aue	k1gFnSc1	Aue
často	často	k6eAd1	často
nedokončí	dokončit	k5eNaPmIp3nS	dokončit
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c4	o
nějaké	nějaký	k3yIgFnPc4	nějaký
takové	takový	k3xDgFnPc4	takový
události	událost	k1gFnPc4	událost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
čtenář	čtenář	k1gMnSc1	čtenář
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zájmu	zájem	k1gInSc2	zájem
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
může	moct	k5eAaImIp3nS	moct
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
knize	kniha	k1gFnSc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
nacistický	nacistický	k2eAgMnSc1d1	nacistický
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
a	a	k8xC	a
vypravěč	vypravěč	k1gMnSc1	vypravěč
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
německo-francouzské	německorancouzský	k2eAgFnSc2d1	německo-francouzská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
strávil	strávit	k5eAaPmAgMnS	strávit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
postupně	postupně	k6eAd1	postupně
stoupá	stoupat	k5eAaImIp3nS	stoupat
po	po	k7c6	po
kariérním	kariérní	k2eAgInSc6d1	kariérní
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
"	"	kIx"	"
<g/>
konečném	konečný	k2eAgNnSc6d1	konečné
řešení	řešení	k1gNnSc6	řešení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Aueho	Aueze	k6eAd1	Aueze
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
nacistický	nacistický	k2eAgMnSc1d1	nacistický
úředník	úředník	k1gMnSc1	úředník
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
být	být	k5eAaImF	být
ve	v	k7c4	v
správný	správný	k2eAgInSc4d1	správný
čas	čas	k1gInSc4	čas
na	na	k7c6	na
správném	správný	k2eAgNnSc6d1	správné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Aue	Aue	k1gMnSc1	Aue
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
zmíní	zmínit	k5eAaPmIp3nS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
na	na	k7c6	na
správném	správný	k2eAgNnSc6d1	správné
místě	místo	k1gNnSc6	místo
chvilku	chvilka	k1gFnSc4	chvilka
před	před	k7c7	před
správným	správný	k2eAgInSc7d1	správný
momentem	moment	k1gInSc7	moment
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jen	jen	k9	jen
vést	vést	k5eAaImF	vést
na	na	k7c6	na
vlně	vlna	k1gFnSc6	vlna
změn	změna	k1gFnPc2	změna
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Zachrání	zachránit	k5eAaPmIp3nS	zachránit
Auemu	Auem	k1gMnSc3	Auem
život	život	k1gInSc1	život
při	při	k7c6	při
evakuaci	evakuace	k1gFnSc6	evakuace
ze	z	k7c2	z
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řada	řada	k1gFnSc1	řada
skutečných	skutečný	k2eAgFnPc2d1	skutečná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
nacistické	nacistický	k2eAgMnPc4d1	nacistický
pohlaváry	pohlavár	k1gMnPc4	pohlavár
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
členy	člen	k1gMnPc4	člen
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uvést	uvést	k5eAaPmF	uvést
Reichsführera	Reichsführer	k1gMnSc2	Reichsführer
Heinricha	Heinrich	k1gMnSc2	Heinrich
Himmlera	Himmler	k1gMnSc2	Himmler
<g/>
,	,	kIx,	,
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Hösse	Höss	k1gMnSc2	Höss
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc2	ministr
Alberta	Albert	k1gMnSc2	Albert
Speera	Speer	k1gMnSc2	Speer
nebo	nebo	k8xC	nebo
důstojníka	důstojník	k1gMnSc2	důstojník
SS	SS	kA	SS
Otto	Otto	k1gMnSc1	Otto
Ohlendorfa	Ohlendorf	k1gMnSc2	Ohlendorf
<g/>
.	.	kIx.	.
</s>
<s>
Originální	originální	k2eAgInSc1d1	originální
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Les	les	k1gInSc1	les
Bienveillantes	Bienveillantes	k1gInSc1	Bienveillantes
Rok	rok	k1gInSc1	rok
vydání	vydání	k1gNnSc1	vydání
<g/>
:	:	kIx,	:
2006	[number]	k4	2006
Nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
:	:	kIx,	:
Gallimard	Gallimard	k1gMnSc1	Gallimard
České	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
Překladatel	překladatel	k1gMnSc1	překladatel
<g/>
:	:	kIx,	:
Michala	Michal	k1gMnSc2	Michal
Marková	Marková	k1gFnSc1	Marková
Počet	počet	k1gInSc1	počet
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
868	[number]	k4	868
ISBN	ISBN	kA	ISBN
(	(	kIx(	(
<g/>
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
978-80-207-1278-3	[number]	k4	978-80-207-1278-3
iDnes	iDnesa	k1gFnPc2	iDnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Laskavé	laskavý	k2eAgFnSc2d1	laskavá
bohyně	bohyně	k1gFnSc2	bohyně
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dočtete	dočíst	k5eAaPmIp2nP	dočíst
s	s	k7c7	s
pauzami	pauza	k1gFnPc7	pauza
na	na	k7c4	na
zvracení	zvracení	k1gNnSc4	zvracení
iDnes	iDnesa	k1gFnPc2	iDnesa
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
knižní	knižní	k2eAgFnSc2d1	knižní
senzace	senzace	k1gFnSc2	senzace
Laskavé	laskavý	k2eAgFnSc2d1	laskavá
bohyně	bohyně	k1gFnSc2	bohyně
</s>
