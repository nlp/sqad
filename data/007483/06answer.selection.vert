<s>
Trvalé	trvalý	k2eAgNnSc1d1	trvalé
lidské	lidský	k2eAgNnSc1d1	lidské
osídlení	osídlení	k1gNnSc1	osídlení
zde	zde	k6eAd1	zde
však	však	k9	však
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
<g/>
,	,	kIx,	,
přímým	přímý	k2eAgMnSc7d1	přímý
předchůdcem	předchůdce	k1gMnSc7	předchůdce
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
slovanské	slovanský	k2eAgNnSc1d1	slovanské
hradiště	hradiště	k1gNnSc1	hradiště
Staré	Staré	k2eAgInPc1d1	Staré
Zámky	zámek	k1gInPc1	zámek
<g/>
.	.	kIx.	.
</s>
