<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
biologicky	biologicky	k6eAd1	biologicky
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
nebuněčných	buněčný	k2eNgInPc2d1	nebuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
?	?	kIx.	?
</s>
