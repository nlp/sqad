<s>
Virologie	virologie	k1gFnSc1	virologie
je	být	k5eAaImIp3nS	být
biologický	biologický	k2eAgInSc4d1	biologický
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
nebuněčných	buněčný	k2eNgInPc2d1	nebuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
viroidů	viroid	k1gInPc2	viroid
a	a	k8xC	a
virusoidů	virusoid	k1gInPc2	virusoid
<g/>
.	.	kIx.	.
</s>
