<s>
Virologie	virologie	k1gFnSc1	virologie
je	být	k5eAaImIp3nS	být
biologický	biologický	k2eAgInSc4d1	biologický
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
nebuněčných	buněčný	k2eNgInPc2d1	nebuněčný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
viroidů	viroid	k1gInPc2	viroid
a	a	k8xC	a
virusoidů	virusoid	k1gInPc2	virusoid
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
působnosti	působnost	k1gFnSc2	působnost
zařazovány	zařazován	k2eAgInPc1d1	zařazován
i	i	k8xC	i
priony	prion	k1gInPc1	prion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
hraničí	hraničit	k5eAaImIp3nP	hraničit
zejména	zejména	k9	zejména
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
dalšími	další	k2eAgInPc7d1	další
obory	obor	k1gInPc7	obor
<g/>
:	:	kIx,	:
biochemie	biochemie	k1gFnSc1	biochemie
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
genetika	genetika	k1gFnSc1	genetika
<g/>
,	,	kIx,	,
epidemiologie	epidemiologie	k1gFnSc1	epidemiologie
<g/>
,	,	kIx,	,
parazitologie	parazitologie	k1gFnSc1	parazitologie
a	a	k8xC	a
imunologie	imunologie	k1gFnSc1	imunologie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
virologie	virologie	k1gFnSc2	virologie
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
považuje	považovat	k5eAaImIp3nS	považovat
ruský	ruský	k2eAgMnSc1d1	ruský
vědec	vědec	k1gMnSc1	vědec
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Ivanovskij	Ivanovskij	k1gMnSc1	Ivanovskij
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
filtroval	filtrovat	k5eAaImAgInS	filtrovat
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
šťávu	šťáva	k1gFnSc4	šťáva
tabáku	tabák	k1gInSc2	tabák
a	a	k8xC	a
objekty	objekt	k1gInPc4	objekt
způsobující	způsobující	k2eAgFnSc4d1	způsobující
mozaiku	mozaika	k1gFnSc4	mozaika
tabáku	tabák	k1gInSc2	tabák
tímto	tento	k3xDgNnSc7	tento
filtrem	filtr	k1gInSc7	filtr
prošly	projít	k5eAaPmAgFnP	projít
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
nazvány	nazván	k2eAgInPc1d1	nazván
viry	vir	k1gInPc1	vir
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k9	tak
virologie	virologie	k1gFnSc1	virologie
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
virus	virus	k1gInSc1	virus
tabákové	tabákový	k2eAgFnSc2d1	tabáková
mozaiky	mozaika	k1gFnSc2	mozaika
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgInSc4	první
objevený	objevený	k2eAgInSc4d1	objevený
virus	virus	k1gInSc4	virus
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
virus	virus	k1gInSc1	virus
<g/>
"	"	kIx"	"
původně	původně	k6eAd1	původně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
"	"	kIx"	"
<g/>
jed	jed	k1gInSc1	jed
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
mikrob	mikrob	k1gInSc4	mikrob
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
jsou	být	k5eAaImIp3nP	být
poněkud	poněkud	k6eAd1	poněkud
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nezachytí	zachytit	k5eNaPmIp3nS	zachytit
na	na	k7c6	na
mikrobiologickém	mikrobiologický	k2eAgNnSc6d1	mikrobiologické
sítu	síto	k1gNnSc6	síto
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
to	ten	k3xDgNnSc1	ten
dělají	dělat	k5eAaImIp3nP	dělat
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
mikroby	mikrob	k1gInPc1	mikrob
procházely	procházet	k5eAaImAgInP	procházet
sítem	síto	k1gNnSc7	síto
a	a	k8xC	a
filtrát	filtrát	k1gInSc1	filtrát
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
infekční	infekční	k2eAgInSc1d1	infekční
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
další	další	k2eAgInPc1d1	další
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
příklady	příklad	k1gInPc1	příklad
takových	takový	k3xDgInPc2	takový
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
kompendium	kompendium	k1gNnSc1	kompendium
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
Filterable	Filterable	k1gFnSc4	Filterable
Viruses	Virusesa	k1gFnPc2	Virusesa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
filtrovatelné	filtrovatelný	k2eAgInPc1d1	filtrovatelný
viry	vir	k1gInPc1	vir
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
postupně	postupně	k6eAd1	postupně
byl	být	k5eAaImAgInS	být
přívlastek	přívlastek	k1gInSc1	přívlastek
"	"	kIx"	"
<g/>
filtrovatelné	filtrovatelný	k2eAgNnSc1d1	filtrovatelný
<g/>
"	"	kIx"	"
vypuštěn	vypuštěn	k2eAgMnSc1d1	vypuštěn
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
virus	virus	k1gInSc1	virus
získalo	získat	k5eAaPmAgNnS	získat
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
význam	význam	k1gInSc1	význam
–	–	k?	–
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
chápeme	chápat	k5eAaImIp1nP	chápat
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
řeč	řeč	k1gFnSc4	řeč
o	o	k7c6	o
virologii	virologie	k1gFnSc6	virologie
jakožto	jakožto	k8xS	jakožto
vědní	vědní	k2eAgFnSc3d1	vědní
disciplíně	disciplína	k1gFnSc3	disciplína
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
lékaři	lékař	k1gMnPc1	lékař
a	a	k8xC	a
zahradníci	zahradník	k1gMnPc1	zahradník
setkávali	setkávat	k5eAaImAgMnP	setkávat
se	s	k7c7	s
životními	životní	k2eAgInPc7d1	životní
projevy	projev	k1gInPc7	projev
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
vědci	vědec	k1gMnPc1	vědec
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
roubování	roubování	k1gNnSc6	roubování
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
choroba	choroba	k1gFnSc1	choroba
přenést	přenést	k5eAaPmF	přenést
z	z	k7c2	z
rostliny	rostlina	k1gFnSc2	rostlina
na	na	k7c4	na
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
však	však	k9	však
byly	být	k5eAaImAgFnP	být
infekce	infekce	k1gFnPc1	infekce
přisuzovány	přisuzovat	k5eAaImNgFnP	přisuzovat
bakteriím	bakterie	k1gFnPc3	bakterie
a	a	k8xC	a
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
něčeho	něco	k3yInSc2	něco
menšího	malý	k2eAgMnSc2d2	menší
se	se	k3xPyFc4	se
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
existenci	existence	k1gFnSc6	existence
takových	takový	k3xDgFnPc2	takový
malých	malý	k2eAgFnPc2d1	malá
infekčních	infekční	k2eAgFnPc2d1	infekční
částic	částice	k1gFnPc2	částice
spekuloval	spekulovat	k5eAaImAgInS	spekulovat
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Jacob	Jacoba	k1gFnPc2	Jacoba
Henle	Henle	k1gNnSc2	Henle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
své	svůj	k3xOyFgMnPc4	svůj
kolegy	kolega	k1gMnPc4	kolega
nepřesvědčil	přesvědčit	k5eNaPmAgInS	přesvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
průlom	průlom	k1gInSc1	průlom
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ruský	ruský	k2eAgMnSc1d1	ruský
botanik	botanik	k1gMnSc1	botanik
Dmitrij	Dmitrij	k1gMnSc1	Dmitrij
Ivanovskij	Ivanovskij	k1gMnSc1	Ivanovskij
provedl	provést	k5eAaPmAgMnS	provést
slavný	slavný	k2eAgInSc4d1	slavný
pokus	pokus	k1gInSc4	pokus
s	s	k7c7	s
extrakty	extrakt	k1gInPc7	extrakt
z	z	k7c2	z
tabáku	tabák	k1gInSc2	tabák
napadeného	napadený	k2eAgInSc2d1	napadený
tzv.	tzv.	kA	tzv.
tabákovou	tabákový	k2eAgFnSc7d1	tabáková
mozaikou	mozaika	k1gFnSc7	mozaika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
extrakt	extrakt	k1gInSc1	extrakt
přecedil	přecedit	k5eAaPmAgInS	přecedit
přes	přes	k7c4	přes
síto	síto	k1gNnSc4	síto
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
žádné	žádný	k3yNgFnPc4	žádný
bakterie	bakterie	k1gFnPc1	bakterie
neprojdou	projít	k5eNaPmIp3nP	projít
<g/>
,	,	kIx,	,
filtrát	filtrát	k1gInSc1	filtrát
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
infekční	infekční	k2eAgInSc1d1	infekční
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Ivanovskij	Ivanovskij	k1gMnSc1	Ivanovskij
však	však	k9	však
příčinu	příčina	k1gFnSc4	příčina
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
neodhalil	odhalit	k5eNaPmAgInS	odhalit
a	a	k8xC	a
stále	stále	k6eAd1	stále
hledal	hledat	k5eAaImAgInS	hledat
původce	původce	k1gMnSc4	původce
tabákové	tabákový	k2eAgFnSc2d1	tabáková
mozaiky	mozaika	k1gFnSc2	mozaika
mezi	mezi	k7c7	mezi
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
výsledky	výsledek	k1gInPc1	výsledek
navíc	navíc	k6eAd1	navíc
poněkud	poněkud	k6eAd1	poněkud
zapadly	zapadnout	k5eAaPmAgInP	zapadnout
a	a	k8xC	a
skutečný	skutečný	k2eAgInSc4d1	skutečný
význam	význam	k1gInSc4	význam
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
vyplul	vyplout	k5eAaPmAgMnS	vyplout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
když	když	k8xS	když
experiment	experiment	k1gInSc1	experiment
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
Martinus	Martinus	k1gMnSc1	Martinus
Beijerinck	Beijerinck	k1gMnSc1	Beijerinck
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
popsal	popsat	k5eAaPmAgMnS	popsat
infekční	infekční	k2eAgFnSc2d1	infekční
částice	částice	k1gFnSc2	částice
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
contagium	contagium	k1gNnSc1	contagium
vivum	vivum	k1gNnSc1	vivum
fluidum	fluidum	k1gNnSc1	fluidum
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
<g/>
nakažlivá	nakažlivý	k2eAgFnSc1d1	nakažlivá
živoucí	živoucí	k2eAgFnSc1d1	živoucí
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
virů	vir	k1gInPc2	vir
zodpovědných	zodpovědný	k2eAgInPc2d1	zodpovědný
za	za	k7c4	za
různá	různý	k2eAgNnPc4d1	různé
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
objeveným	objevený	k2eAgInSc7d1	objevený
virem	vir	k1gInSc7	vir
napadajícím	napadající	k2eAgInSc7d1	napadající
živočichy	živočich	k1gMnPc4	živočich
byl	být	k5eAaImAgInS	být
virus	virus	k1gInSc1	virus
slintavky	slintavka	k1gFnSc2	slintavka
a	a	k8xC	a
kulhavky	kulhavka	k1gFnSc2	kulhavka
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prvním	první	k4xOgInSc7	první
objeveným	objevený	k2eAgInSc7d1	objevený
lidským	lidský	k2eAgInSc7d1	lidský
virem	vir	k1gInSc7	vir
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
virus	virus	k1gInSc1	virus
žluté	žlutý	k2eAgFnSc2d1	žlutá
zimnice	zimnice	k1gFnSc2	zimnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
objevil	objevit	k5eAaPmAgInS	objevit
Peyton	Peyton	k1gInSc1	Peyton
Rous	rous	k1gInSc1	rous
první	první	k4xOgInPc4	první
virus	virus	k1gInSc4	virus
způsobující	způsobující	k2eAgNnSc1d1	způsobující
nádorové	nádorový	k2eAgNnSc1d1	nádorové
bujení	bujení	k1gNnSc1	bujení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Rousův	Rousův	k2eAgInSc1d1	Rousův
sarkom	sarkom	k1gInSc1	sarkom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
etapách	etapa	k1gFnPc6	etapa
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
však	však	k9	však
stále	stále	k6eAd1	stále
nebylo	být	k5eNaImAgNnS	být
jasno	jasno	k6eAd1	jasno
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
vlastně	vlastně	k9	vlastně
jsou	být	k5eAaImIp3nP	být
viry	vir	k1gInPc1	vir
zač	zač	k6eAd1	zač
–	–	k?	–
tyto	tento	k3xDgInPc4	tento
dohady	dohad	k1gInPc4	dohad
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
až	až	k9	až
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Herelleho	Herelle	k1gMnSc2	Herelle
plakový	plakový	k2eAgInSc1d1	plakový
test	test	k1gInSc1	test
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
první	první	k4xOgInSc4	první
elektronmikroskopický	elektronmikroskopický	k2eAgInSc4d1	elektronmikroskopický
snímek	snímek	k1gInSc4	snímek
virů	vir	k1gInPc2	vir
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
sice	sice	k8xC	sice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
byly	být	k5eAaImAgInP	být
poměrně	poměrně	k6eAd1	poměrně
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
mikroskopické	mikroskopický	k2eAgInPc1d1	mikroskopický
snímky	snímek	k1gInPc1	snímek
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
chemická	chemický	k2eAgFnSc1d1	chemická
struktura	struktura	k1gFnSc1	struktura
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
poněkud	poněkud	k6eAd1	poněkud
zahalena	zahalit	k5eAaPmNgFnS	zahalit
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
Vinson	Vinson	k1gInSc4	Vinson
a	a	k8xC	a
Petre	Petr	k1gMnSc5	Petr
vysráželi	vysrážet	k5eAaPmAgMnP	vysrážet
virus	virus	k1gInSc4	virus
tabákové	tabákový	k2eAgFnSc2d1	tabáková
mozaiky	mozaika	k1gFnSc2	mozaika
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
protein	protein	k1gInSc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
prokázali	prokázat	k5eAaPmAgMnP	prokázat
jeho	jeho	k3xOp3gInSc4	jeho
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
elektrickém	elektrický	k2eAgNnSc6d1	elektrické
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
další	další	k2eAgInSc4d1	další
důkaz	důkaz	k1gInSc4	důkaz
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
viry	vir	k1gInPc1	vir
jsou	být	k5eAaImIp3nP	být
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
A.	A.	kA	A.
Purdy-Beale	Purdy-Beala	k1gFnSc6	Purdy-Beala
zase	zase	k9	zase
připravil	připravit	k5eAaPmAgMnS	připravit
proti	proti	k7c3	proti
virům	vir	k1gInPc3	vir
protilátky	protilátka	k1gFnSc2	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jím	jíst	k5eAaImIp1nS	jíst
zkoumané	zkoumaný	k2eAgInPc4d1	zkoumaný
bakteriofágy	bakteriofág	k1gInPc4	bakteriofág
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mimo	mimo	k6eAd1	mimo
proteinů	protein	k1gInPc2	protein
také	také	k9	také
fosfor	fosfor	k1gInSc4	fosfor
a	a	k8xC	a
deoxyribonukleovou	deoxyribonukleový	k2eAgFnSc4d1	deoxyribonukleová
kyselinu	kyselina	k1gFnSc4	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
virech	vir	k1gInPc6	vir
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
ribonukleové	ribonukleový	k2eAgFnSc2d1	ribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přelomem	přelom	k1gInSc7	přelom
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
krystalizace	krystalizace	k1gFnSc1	krystalizace
virových	virový	k2eAgFnPc2d1	virová
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
Wendell	Wendell	k1gInSc1	Wendell
Stanley	Stanlea	k1gFnSc2	Stanlea
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Padesátá	padesátý	k4xOgNnPc1	padesátý
léta	léto	k1gNnPc1	léto
byla	být	k5eAaImAgNnP	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
studia	studio	k1gNnSc2	studio
centrálního	centrální	k2eAgNnSc2d1	centrální
dogmatu	dogma	k1gNnSc2	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
náhodou	náhodou	k6eAd1	náhodou
<g/>
,	,	kIx,	,
že	že	k8xS	že
viry	vir	k1gInPc1	vir
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
i	i	k9	i
Crick	Crick	k1gMnSc1	Crick
a	a	k8xC	a
Watson	Watson	k1gMnSc1	Watson
<g/>
.	.	kIx.	.
</s>
<s>
Koneckonců	koneckonců	k9	koneckonců
<g/>
,	,	kIx,	,
i	i	k8xC	i
slavný	slavný	k2eAgInSc4d1	slavný
Hersheyho-Chaseové	Hersheyho-Chaseové	k2eAgInSc4d1	Hersheyho-Chaseové
experiment	experiment	k1gInSc4	experiment
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
viry	vir	k1gInPc7	vir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
pokroku	pokrok	k1gInSc3	pokrok
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
virů	vir	k1gInPc2	vir
jak	jak	k8xS	jak
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
živočišných	živočišný	k2eAgInPc2d1	živočišný
i	i	k8xC	i
bakteriálních	bakteriální	k2eAgInPc2d1	bakteriální
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
vědci	vědec	k1gMnPc1	vědec
začali	začít	k5eAaPmAgMnP	začít
používat	používat	k5eAaImF	používat
viry	vir	k1gInPc4	vir
jako	jako	k8xS	jako
modelové	modelový	k2eAgInPc4d1	modelový
organismy	organismus	k1gInPc4	organismus
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
obecných	obecný	k2eAgInPc2d1	obecný
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
následně	následně	k6eAd1	následně
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zobecnit	zobecnit	k5eAaPmF	zobecnit
na	na	k7c4	na
všechen	všechen	k3xTgInSc4	všechen
pozemský	pozemský	k2eAgInSc4d1	pozemský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
začíná	začínat	k5eAaImIp3nS	začínat
převrat	převrat	k1gInSc4	převrat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
experimentální	experimentální	k2eAgFnSc6d1	experimentální
biologii	biologie	k1gFnSc6	biologie
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
genového	genový	k2eAgNnSc2d1	genové
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
viry	vira	k1gFnPc1	vira
byly	být	k5eAaImAgFnP	být
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Koneckonců	koneckonců	k9	koneckonců
<g/>
,	,	kIx,	,
právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
reverzní	reverzní	k2eAgFnSc1d1	reverzní
transkriptáza	transkriptáza	k1gFnSc1	transkriptáza
retrovirů	retrovir	k1gInPc2	retrovir
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
plní	plnit	k5eAaImIp3nS	plnit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
funkci	funkce	k1gFnSc4	funkce
v	v	k7c6	v
biochemické	biochemický	k2eAgFnSc6d1	biochemická
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumy	výzkum	k1gInPc1	výzkum
došly	dojít	k5eAaPmAgInP	dojít
až	až	k9	až
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
začali	začít	k5eAaPmAgMnP	začít
obávat	obávat	k5eAaImF	obávat
možných	možný	k2eAgNnPc2d1	možné
rizik	riziko	k1gNnPc2	riziko
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
a	a	k8xC	a
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
určité	určitý	k2eAgInPc4d1	určitý
experimenty	experiment	k1gInPc4	experiment
uvaleno	uvalen	k2eAgNnSc4d1	uvaleno
moratorium	moratorium	k1gNnSc4	moratorium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyřešení	vyřešení	k1gNnSc6	vyřešení
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
otázek	otázka	k1gFnPc2	otázka
však	však	k9	však
experimenty	experiment	k1gInPc1	experiment
nyní	nyní	k6eAd1	nyní
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c4	o
roli	role	k1gFnSc4	role
virů	vir	k1gInPc2	vir
ve	v	k7c6	v
vzniku	vznik	k1gInSc6	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
či	či	k8xC	či
třeba	třeba	k6eAd1	třeba
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
nových	nový	k2eAgFnPc2d1	nová
vakcín	vakcína	k1gFnPc2	vakcína
proti	proti	k7c3	proti
virovým	virový	k2eAgNnPc3d1	virové
onemocněním	onemocnění	k1gNnPc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výzvou	výzva	k1gFnSc7	výzva
pro	pro	k7c4	pro
virology	virolog	k1gMnPc4	virolog
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
