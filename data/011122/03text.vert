<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
požární	požární	k2eAgFnSc1d1	požární
signalizace	signalizace	k1gFnSc1	signalizace
(	(	kIx(	(
<g/>
EPS	EPS	kA	EPS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyhrazené	vyhrazený	k2eAgNnSc1d1	vyhrazené
požárně	požárně	k6eAd1	požárně
bezpečnostní	bezpečnostní	k2eAgNnSc1d1	bezpečnostní
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
požární	požární	k2eAgFnSc1d1	požární
signalizace	signalizace	k1gFnSc1	signalizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pomocí	pomocí	k7c2	pomocí
hlásičů	hlásič	k1gMnPc2	hlásič
včasnou	včasný	k2eAgFnSc4d1	včasná
signalizaci	signalizace	k1gFnSc4	signalizace
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Signály	signál	k1gInPc1	signál
z	z	k7c2	z
hlásičů	hlásič	k1gMnPc2	hlásič
požáru	požár	k1gInSc2	požár
jsou	být	k5eAaImIp3nP	být
přijímány	přijímat	k5eAaImNgInP	přijímat
ústřednou	ústředna	k1gFnSc7	ústředna
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ústředny	ústředna	k1gFnSc2	ústředna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
recepci	recepce	k1gFnSc6	recepce
budovy	budova	k1gFnSc2	budova
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
den	den	k1gInSc4	den
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
stálá	stálý	k2eAgFnSc1d1	stálá
obsluha	obsluha	k1gFnSc1	obsluha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
signálu	signál	k1gInSc2	signál
požáru	požár	k1gInSc2	požár
má	mít	k5eAaImIp3nS	mít
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
prověření	prověření	k1gNnSc4	prověření
skutečného	skutečný	k2eAgInSc2d1	skutečný
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
odvolání	odvolání	k1gNnSc2	odvolání
planého	planý	k2eAgInSc2d1	planý
poplachu	poplach	k1gInSc2	poplach
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
EPS	EPS	kA	EPS
přivolá	přivolat	k5eAaPmIp3nS	přivolat
pomocí	pomocí	k7c2	pomocí
zařízení	zařízení	k1gNnSc2	zařízení
zařízení	zařízení	k1gNnPc2	zařízení
dálkového	dálkový	k2eAgInSc2d1	dálkový
přenosu	přenos	k1gInSc2	přenos
(	(	kIx(	(
<g/>
ZDP	ZDP	kA	ZDP
<g/>
)	)	kIx)	)
jednotku	jednotka	k1gFnSc4	jednotka
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
stálá	stálý	k2eAgFnSc1d1	stálá
obsluha	obsluha	k1gFnSc1	obsluha
(	(	kIx(	(
<g/>
režim	režim	k1gInSc1	režim
noc	noc	k1gFnSc4	noc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
PO	Po	kA	Po
přivolána	přivolat	k5eAaPmNgFnS	přivolat
neprodleně	prodleně	k6eNd1	prodleně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgNnSc2	tento
nejběžnějšího	běžný	k2eAgNnSc2d3	nejběžnější
označení	označení	k1gNnSc2	označení
EPS	EPS	kA	EPS
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
označení	označení	k1gNnPc4	označení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
LDP	LDP	kA	LDP
(	(	kIx(	(
<g/>
Lokální	lokální	k2eAgFnSc1d1	lokální
detekce	detekce	k1gFnSc1	detekce
požáru	požár	k1gInSc2	požár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc7	druh
EPS	EPS	kA	EPS
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jednostupňová	jednostupňový	k2eAgNnPc4d1	jednostupňové
===	===	k?	===
</s>
</p>
<p>
<s>
Jednostupňová	jednostupňový	k2eAgNnPc4d1	jednostupňové
EPS	EPS	kA	EPS
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ústředen	ústředna	k1gFnPc2	ústředna
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgInP	připojit
samočinné	samočinný	k2eAgInPc1d1	samočinný
a	a	k8xC	a
tlačítkové	tlačítkový	k2eAgInPc1d1	tlačítkový
hlásiče	hlásič	k1gInPc1	hlásič
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ústřednu	ústředna	k1gFnSc4	ústředna
jsou	být	k5eAaImIp3nP	být
zapojeny	zapojen	k2eAgFnPc4d1	zapojena
doplňující	doplňující	k2eAgNnPc4d1	doplňující
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ovládací	ovládací	k2eAgNnPc1d1	ovládací
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Jednostupňová	jednostupňový	k2eAgNnPc4d1	jednostupňové
EPS	EPS	kA	EPS
nemá	mít	k5eNaImIp3nS	mít
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
ústřednu	ústředna	k1gFnSc4	ústředna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vícestupňová	vícestupňový	k2eAgFnSc1d1	vícestupňová
EPS	EPS	kA	EPS
===	===	k?	===
</s>
</p>
<p>
<s>
Vícestupňová	vícestupňový	k2eAgNnPc4d1	vícestupňové
EPS	EPS	kA	EPS
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgFnPc4d1	hlavní
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
ústředny	ústředna	k1gFnPc4	ústředna
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
jsou	být	k5eAaImIp3nP	být
připojeny	připojit	k5eAaPmNgInP	připojit
samočinné	samočinný	k2eAgInPc1d1	samočinný
a	a	k8xC	a
tlačítkové	tlačítkový	k2eAgInPc1d1	tlačítkový
hlásiče	hlásič	k1gInPc1	hlásič
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
ústředny	ústředna	k1gFnSc2	ústředna
nižšího	nízký	k2eAgInSc2d2	nižší
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Systémy	systém	k1gInPc1	systém
EPS	EPS	kA	EPS
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
dva	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
systému	systém	k1gInSc2	systém
EPS	EPS	kA	EPS
s	s	k7c7	s
kolektivní	kolektivní	k2eAgFnSc7d1	kolektivní
adresací	adresace	k1gFnSc7	adresace
je	být	k5eAaImIp3nS	být
ústředna	ústředna	k1gFnSc1	ústředna
schopna	schopen	k2eAgFnSc1d1	schopna
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yQgFnSc2	který
hlásící	hlásící	k2eAgFnSc2d1	hlásící
linky	linka	k1gFnSc2	linka
přišel	přijít	k5eAaPmAgInS	přijít
signál	signál	k1gInSc1	signál
POŽÁR	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezjistí	zjistit	k5eNaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgInSc2	který
hlásiče	hlásič	k1gInSc2	hlásič
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nevyhovující	vyhovující	k2eNgMnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
určené	určený	k2eAgNnSc4d1	určené
místo	místo	k1gNnSc4	místo
požáru	požár	k1gInSc2	požár
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
doby	doba	k1gFnSc2	doba
pro	pro	k7c4	pro
včasný	včasný	k2eAgInSc4d1	včasný
zásah	zásah	k1gInSc4	zásah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
EPS	EPS	kA	EPS
s	s	k7c7	s
kolektivní	kolektivní	k2eAgFnSc7d1	kolektivní
adresací	adresace	k1gFnSc7	adresace
(	(	kIx(	(
<g/>
konvenční	konvenční	k2eAgInSc1d1	konvenční
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Konvenční	konvenční	k2eAgInPc1d1	konvenční
systémy	systém	k1gInPc1	systém
EPS	EPS	kA	EPS
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgMnPc4d1	vhodný
pro	pro	k7c4	pro
objekty	objekt	k1gInPc4	objekt
menšího	malý	k2eAgInSc2d2	menší
rozsahu	rozsah	k1gInSc2	rozsah
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c4	na
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
změn	změna	k1gFnPc2	změna
na	na	k7c6	na
smyčce	smyčka	k1gFnSc6	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Smyčka	smyčka	k1gFnSc1	smyčka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
požární	požární	k2eAgInSc4d1	požární
hlásič	hlásič	k1gInSc4	hlásič
a	a	k8xC	a
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
požáru	požár	k1gInSc2	požár
změní	změnit	k5eAaPmIp3nS	změnit
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
upozorní	upozornit	k5eAaPmIp3nS	upozornit
ústřednu	ústředna	k1gFnSc4	ústředna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
signál	signál	k1gInSc4	signál
vyhodnotí	vyhodnotit	k5eAaPmIp3nS	vyhodnotit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
nákladný	nákladný	k2eAgInSc1d1	nákladný
než	než	k8xS	než
adresovatelný	adresovatelný	k2eAgInSc1d1	adresovatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
EPS	EPS	kA	EPS
s	s	k7c7	s
individuální	individuální	k2eAgFnSc7d1	individuální
adresací	adresace	k1gFnSc7	adresace
(	(	kIx(	(
<g/>
adresovatelný	adresovatelný	k2eAgInSc1d1	adresovatelný
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
identifikaci	identifikace	k1gFnSc3	identifikace
stavu	stav	k1gInSc2	stav
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
hlásičů	hlásič	k1gInPc2	hlásič
na	na	k7c6	na
hlásící	hlásící	k2eAgFnSc6d1	hlásící
lince	linka	k1gFnSc6	linka
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvky	prvek	k1gInPc1	prvek
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
vzájemně	vzájemně	k6eAd1	vzájemně
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
identifikovat	identifikovat	k5eAaBmF	identifikovat
prvek	prvek	k1gInSc4	prvek
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
přesné	přesný	k2eAgNnSc1d1	přesné
umístění	umístění	k1gNnSc1	umístění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
signál	signál	k1gInSc4	signál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
hlásiče	hlásič	k1gInPc1	hlásič
<g/>
)	)	kIx)	)
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
s	s	k7c7	s
ústřednou	ústředna	k1gFnSc7	ústředna
individuálně	individuálně	k6eAd1	individuálně
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomoc	k1gFnPc2	pomoc
software	software	k1gInSc1	software
lze	lze	k6eAd1	lze
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
řadit	řadit	k5eAaImF	řadit
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
oddělených	oddělený	k2eAgFnPc2d1	oddělená
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
rozlohu	rozloha	k1gFnSc4	rozloha
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
až	až	k8xS	až
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
části	část	k1gFnPc1	část
EPS	EPS	kA	EPS
==	==	k?	==
</s>
</p>
<p>
<s>
Hlásiče	hlásič	k1gInPc1	hlásič
požáru	požár	k1gInSc2	požár
</s>
</p>
<p>
<s>
Ústředny	ústředna	k1gFnPc1	ústředna
EPS	EPS	kA	EPS
</s>
</p>
<p>
<s>
Doplňující	doplňující	k2eAgNnSc1d1	doplňující
zařízení	zařízení	k1gNnSc1	zařízení
</s>
</p>
<p>
<s>
===	===	k?	===
Hlásiče	hlásič	k1gInPc1	hlásič
požáru	požár	k1gInSc2	požár
===	===	k?	===
</s>
</p>
<p>
<s>
Hlásiče	hlásič	k1gInPc1	hlásič
požáru	požár	k1gInSc2	požár
sledují	sledovat	k5eAaImIp3nP	sledovat
čidly	čidlo	k1gNnPc7	čidlo
a	a	k8xC	a
elektronicky	elektronicky	k6eAd1	elektronicky
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
určité	určitý	k2eAgInPc4d1	určitý
fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
parametry	parametr	k1gInPc4	parametr
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
změny	změna	k1gFnPc4	změna
provázející	provázející	k2eAgInSc1d1	provázející
vznik	vznik	k1gInSc1	vznik
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
vysílají	vysílat	k5eAaImIp3nP	vysílat
příslušný	příslušný	k2eAgInSc4d1	příslušný
signál	signál	k1gInSc4	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc4	rozdělení
hlásičů	hlásič	k1gInPc2	hlásič
požáru	požár	k1gInSc2	požár
podle	podle	k7c2	podle
spouštění	spouštění	k1gNnSc2	spouštění
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tlačítkové	tlačítkový	k2eAgInPc1d1	tlačítkový
hlásiče	hlásič	k1gInPc1	hlásič
====	====	k?	====
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
hlásiče	hlásič	k1gInPc1	hlásič
nevyhodnocují	vyhodnocovat	k5eNaImIp3nP	vyhodnocovat
žádné	žádný	k3yNgInPc1	žádný
vnější	vnější	k2eAgInPc1d1	vnější
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
parametry	parametr	k1gInPc1	parametr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
stisk	stisk	k1gInSc1	stisk
tlačítka	tlačítko	k1gNnSc2	tlačítko
<g/>
,	,	kIx,	,
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
lidského	lidský	k2eAgInSc2d1	lidský
činitele	činitel	k1gInSc2	činitel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musí	muset	k5eAaImIp3nS	muset
tuto	tento	k3xDgFnSc4	tento
změnu	změna	k1gFnSc4	změna
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
a	a	k8xC	a
stisknout	stisknout	k5eAaPmF	stisknout
tlačítko	tlačítko	k1gNnSc4	tlačítko
hlásiče	hlásič	k1gInSc2	hlásič
<g/>
,	,	kIx,	,
předají	předat	k5eAaPmIp3nP	předat
údaj	údaj	k1gInSc4	údaj
do	do	k7c2	do
ústředny	ústředna	k1gFnSc2	ústředna
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Samočinné	samočinný	k2eAgInPc1d1	samočinný
hlásiče	hlásič	k1gInPc1	hlásič
====	====	k?	====
</s>
</p>
<p>
<s>
Samočinné	samočinný	k2eAgInPc1d1	samočinný
hlásiče	hlásič	k1gInPc1	hlásič
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
nebo	nebo	k8xC	nebo
změnu	změna	k1gFnSc4	změna
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
parametrů	parametr	k1gInPc2	parametr
požáru	požár	k1gInSc2	požár
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
zásahu	zásah	k1gInSc2	zásah
lidského	lidský	k2eAgInSc2d1	lidský
činitele	činitel	k1gInSc2	činitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samočinné	samočinný	k2eAgInPc1d1	samočinný
hlásiče	hlásič	k1gInPc1	hlásič
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
hlásiče	hlásič	k1gInPc4	hlásič
bodové	bodový	k2eAgInPc4d1	bodový
a	a	k8xC	a
lineární	lineární	k2eAgInPc4d1	lineární
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bodové	bodový	k2eAgInPc1d1	bodový
hlásiče	hlásič	k1gInPc1	hlásič
sledují	sledovat	k5eAaImIp3nP	sledovat
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
parametry	parametr	k1gInPc1	parametr
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lineární	lineární	k2eAgInPc1d1	lineární
hlásiče	hlásič	k1gInPc1	hlásič
sledují	sledovat	k5eAaImIp3nP	sledovat
změnu	změna	k1gFnSc4	změna
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
parametrů	parametr	k1gInPc2	parametr
na	na	k7c6	na
určitém	určitý	k2eAgInSc6d1	určitý
úseku	úsek	k1gInSc6	úsek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
hlásičů	hlásič	k1gInPc2	hlásič
podle	podle	k7c2	podle
sledovaných	sledovaný	k2eAgInPc2d1	sledovaný
parametrů	parametr	k1gInPc2	parametr
===	===	k?	===
</s>
</p>
<p>
<s>
Teplotní	teplotní	k2eAgInPc1d1	teplotní
hlásiče	hlásič	k1gInPc1	hlásič
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
jednak	jednak	k8xC	jednak
překročení	překročení	k1gNnSc1	překročení
určité	určitý	k2eAgFnSc2d1	určitá
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
jednak	jednak	k8xC	jednak
rychlost	rychlost	k1gFnSc1	rychlost
jejího	její	k3xOp3gNnSc2	její
zvyšování	zvyšování	k1gNnSc2	zvyšování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kouřové	kouřový	k2eAgInPc1d1	kouřový
hlásiče	hlásič	k1gInPc1	hlásič
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
vznik	vznik	k1gInSc4	vznik
požáru	požár	k1gInSc2	požár
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zjištění	zjištění	k1gNnSc4	zjištění
přítomnosti	přítomnost	k1gFnSc2	přítomnost
aerosolů	aerosol	k1gInPc2	aerosol
</s>
</p>
<p>
<s>
Multifunkční	multifunkční	k2eAgInPc1d1	multifunkční
hlásiče	hlásič	k1gInPc1	hlásič
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
teplotní	teplotní	k2eAgNnPc4d1	teplotní
a	a	k8xC	a
kouřová	kouřový	k2eAgNnPc4d1	kouřové
čidla	čidlo	k1gNnPc4	čidlo
do	do	k7c2	do
jediné	jediný	k2eAgFnSc2d1	jediná
jednotky	jednotka	k1gFnSc2	jednotka
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
volby	volba	k1gFnSc2	volba
typu	typ	k1gInSc2	typ
čidel	čidlo	k1gNnPc2	čidlo
pro	pro	k7c4	pro
aktivaci	aktivace	k1gFnSc4	aktivace
</s>
</p>
<p>
<s>
Hlásiče	hlásič	k1gInSc2	hlásič
vyzařování	vyzařování	k1gNnSc2	vyzařování
plamene	plamen	k1gInSc2	plamen
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c6	na
vyzařování	vyzařování	k1gNnSc6	vyzařování
plamene	plamen	k1gInSc2	plamen
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
části	část	k1gFnSc6	část
spektra	spektrum	k1gNnSc2	spektrum
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
hlásiče	hlásič	k1gInPc1	hlásič
se	se	k3xPyFc4	se
v	v	k7c6	v
ČR	ČR	kA	ČR
používají	používat	k5eAaImIp3nP	používat
výjimečně	výjimečně	k6eAd1	výjimečně
mj.	mj.	kA	mj.
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
nákladnost	nákladnost	k1gFnSc4	nákladnost
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
změn	změna	k1gFnPc2	změna
sledované	sledovaný	k2eAgFnSc2d1	sledovaná
veličiny	veličina	k1gFnSc2	veličina
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnPc1d1	maximální
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c6	na
překročení	překročení	k1gNnSc6	překročení
mezní	mezní	k2eAgFnSc2d1	mezní
hodnoty	hodnota	k1gFnSc2	hodnota
</s>
</p>
<p>
<s>
Diferenciální	diferenciální	k2eAgFnPc1d1	diferenciální
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c6	na
překročení	překročení	k1gNnSc6	překročení
rychlostní	rychlostní	k2eAgFnSc2d1	rychlostní
změny	změna	k1gFnSc2	změna
parametru	parametr	k1gInSc2	parametr
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kombinované	kombinovaný	k2eAgFnPc1d1	kombinovaná
mají	mít	k5eAaImIp3nP	mít
maximální	maximální	k2eAgFnPc1d1	maximální
i	i	k8xC	i
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
část	část	k1gFnSc1	část
<g/>
;	;	kIx,	;
reagují	reagovat	k5eAaBmIp3nP	reagovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
reakce	reakce	k1gFnSc2	reakce
alespoň	alespoň	k9	alespoň
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
</s>
</p>
<p>
<s>
Inteligentní	inteligentní	k2eAgFnPc1d1	inteligentní
mají	mít	k5eAaImIp3nP	mít
vestavěnou	vestavěný	k2eAgFnSc4d1	vestavěná
"	"	kIx"	"
<g/>
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
"	"	kIx"	"
vyhodnocení	vyhodnocení	k1gNnSc4	vyhodnocení
změn	změna	k1gFnPc2	změna
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
parametru	parametr	k1gInSc2	parametr
</s>
</p>
<p>
<s>
S	s	k7c7	s
analýzou	analýza	k1gFnSc7	analýza
tvaru	tvar	k1gInSc2	tvar
signálu	signál	k1gInSc2	signál
(	(	kIx(	(
<g/>
sledují	sledovat	k5eAaImIp3nP	sledovat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
střežení	střežení	k1gNnSc2	střežení
časové	časový	k2eAgFnPc1d1	časová
křivky	křivka	k1gFnPc1	křivka
průběhu	průběh	k1gInSc2	průběh
teploty	teplota	k1gFnPc1	teplota
<g/>
,	,	kIx,	,
hustoty	hustota	k1gFnPc1	hustota
kouře	kouř	k1gInSc2	kouř
<g/>
,	,	kIx,	,
koncentrace	koncentrace	k1gFnSc1	koncentrace
CO	co	k9	co
<g/>
,	,	kIx,	,
IR	Ir	k1gMnSc1	Ir
záření	záření	k1gNnSc2	záření
apod.	apod.	kA	apod.
a	a	k8xC	a
porovnávají	porovnávat	k5eAaImIp3nP	porovnávat
je	on	k3xPp3gNnSc4	on
se	s	k7c7	s
vzorky	vzorek	k1gInPc7	vzorek
křivek	křivka	k1gFnPc2	křivka
<g/>
,	,	kIx,	,
získanými	získaný	k2eAgFnPc7d1	získaná
při	při	k7c6	při
laboratorních	laboratorní	k2eAgInPc6d1	laboratorní
testech	test	k1gInPc6	test
a	a	k8xC	a
uloženými	uložený	k2eAgInPc7d1	uložený
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
hlásiče	hlásič	k1gInSc2	hlásič
<g/>
.	.	kIx.	.
</s>
<s>
Dokáží	dokázat	k5eAaPmIp3nP	dokázat
tak	tak	k6eAd1	tak
odlišit	odlišit	k5eAaPmF	odlišit
skutečný	skutečný	k2eAgInSc4d1	skutečný
požár	požár	k1gInSc4	požár
od	od	k7c2	od
nepožárního	požární	k2eNgInSc2d1	požární
podnětu	podnět	k1gInSc2	podnět
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
sváření	sváření	k1gNnSc1	sváření
<g/>
,	,	kIx,	,
efektová	efektový	k2eAgFnSc1d1	efektová
mlha	mlha	k1gFnSc1	mlha
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgInPc4d1	výrobní
procesy	proces	k1gInPc4	proces
produkující	produkující	k2eAgInSc4d1	produkující
kouř	kouř	k1gInSc4	kouř
<g/>
,	,	kIx,	,
hustý	hustý	k2eAgInSc4d1	hustý
prach	prach	k1gInSc4	prach
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
časového	časový	k2eAgNnSc2d1	časové
zpoždění	zpoždění	k1gNnSc2	zpoždění
===	===	k?	===
</s>
</p>
<p>
<s>
Hlásiče	hlásič	k1gInPc1	hlásič
se	s	k7c7	s
zpožděním	zpoždění	k1gNnSc7	zpoždění
-	-	kIx~	-
sledovaný	sledovaný	k2eAgInSc4d1	sledovaný
parametr	parametr	k1gInSc4	parametr
nebo	nebo	k8xC	nebo
rychlost	rychlost	k1gFnSc4	rychlost
jeho	jeho	k3xOp3gFnSc2	jeho
změny	změna	k1gFnSc2	změna
musí	muset	k5eAaImIp3nS	muset
překračovat	překračovat	k5eAaImF	překračovat
mezní	mezní	k2eAgFnSc4d1	mezní
hodnotu	hodnota	k1gFnSc4	hodnota
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
potom	potom	k6eAd1	potom
hlásič	hlásič	k1gMnSc1	hlásič
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlásiče	hlásič	k1gInPc1	hlásič
bez	bez	k7c2	bez
zpoždění	zpoždění	k1gNnSc2	zpoždění
reagují	reagovat	k5eAaBmIp3nP	reagovat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
překročení	překročení	k1gNnSc6	překročení
maximální	maximální	k2eAgFnSc2d1	maximální
mezní	mezní	k2eAgFnSc2d1	mezní
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
diferenciální	diferenciální	k2eAgFnPc4d1	diferenciální
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Speciální	speciální	k2eAgInPc1d1	speciální
typy	typ	k1gInPc1	typ
hlásičů	hlásič	k1gMnPc2	hlásič
===	===	k?	===
</s>
</p>
<p>
<s>
Nasávací	nasávací	k2eAgInPc1d1	nasávací
hlásiče	hlásič	k1gInPc1	hlásič
se	s	k7c7	s
vzorkováním	vzorkování	k1gNnSc7	vzorkování
vzduchu	vzduch	k1gInSc2	vzduch
jsou	být	k5eAaImIp3nP	být
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
hlásiče	hlásič	k1gInPc1	hlásič
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
kouřové	kouřový	k2eAgFnPc1d1	kouřová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
čidla	čidlo	k1gNnSc2	čidlo
nasáván	nasáván	k2eAgInSc4d1	nasáván
vzduch	vzduch	k1gInSc4	vzduch
ze	z	k7c2	z
střeženého	střežený	k2eAgInSc2d1	střežený
prostoru	prostor	k1gInSc2	prostor
</s>
</p>
<p>
<s>
Hlásiče	hlásič	k1gInPc1	hlásič
plamene	plamen	k1gInSc2	plamen
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c4	na
sledování	sledování	k1gNnSc4	sledování
modulovaného	modulovaný	k2eAgNnSc2d1	modulované
vyzařování	vyzařování	k1gNnSc2	vyzařování
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
4,3	[number]	k4	4,3
μ	μ	k?	μ
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ústředny	ústředna	k1gFnSc2	ústředna
EPS	EPS	kA	EPS
==	==	k?	==
</s>
</p>
<p>
<s>
Ústředny	ústředna	k1gFnPc1	ústředna
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgFnPc4	tento
základní	základní	k2eAgFnPc4d1	základní
funkce	funkce	k1gFnPc4	funkce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
vyhodnocování	vyhodnocování	k1gNnSc1	vyhodnocování
signalizace	signalizace	k1gFnSc2	signalizace
hlásičů	hlásič	k1gInPc2	hlásič
-	-	kIx~	-
stavy	stav	k1gInPc1	stav
<g/>
:	:	kIx,	:
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
porucha	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
požár	požár	k1gInSc1	požár
</s>
</p>
<p>
<s>
ovládání	ovládání	k1gNnSc1	ovládání
připojených	připojený	k2eAgNnPc2d1	připojené
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
spouští	spouštět	k5eAaImIp3nS	spouštět
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
evakuaci	evakuace	k1gFnSc4	evakuace
<g/>
,	,	kIx,	,
větrání	větrání	k1gNnSc1	větrání
<g/>
,	,	kIx,	,
sirény	siréna	k1gFnPc1	siréna
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
evakuační	evakuační	k2eAgInSc1d1	evakuační
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
přivolává	přivolávat	k5eAaImIp3nS	přivolávat
hasičskou	hasičský	k2eAgFnSc4d1	hasičská
pomoc	pomoc	k1gFnSc4	pomoc
</s>
</p>
<p>
<s>
kontrola	kontrola	k1gFnSc1	kontrola
provozuschopnosti	provozuschopnost	k1gFnSc2	provozuschopnost
celého	celý	k2eAgInSc2d1	celý
systému	systém	k1gInSc2	systém
EPS	EPS	kA	EPS
</s>
</p>
<p>
<s>
nepřetržité	přetržitý	k2eNgNnSc4d1	nepřetržité
napájení	napájení	k1gNnSc4	napájení
hlásičů	hlásič	k1gInPc2	hlásič
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
<g/>
Ústředna	ústředna	k1gFnSc1	ústředna
musí	muset	k5eAaImIp3nS	muset
obsluze	obsluha	k1gFnSc3	obsluha
signalizovat	signalizovat	k5eAaImF	signalizovat
minimálně	minimálně	k6eAd1	minimálně
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
stavy	stav	k1gInPc4	stav
<g/>
:	:	kIx,	:
PROVOZ	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
PORUCHA	porucha	k1gFnSc1	porucha
<g/>
,	,	kIx,	,
POŽÁR	požár	k1gInSc1	požár
<g/>
.	.	kIx.	.
</s>
<s>
Ústředna	ústředna	k1gFnSc1	ústředna
EPS	EPS	kA	EPS
může	moct	k5eAaImIp3nS	moct
požár	požár	k1gInSc4	požár
signalizovat	signalizovat	k5eAaImF	signalizovat
jednak	jednak	k8xC	jednak
obsluze	obsluha	k1gFnSc3	obsluha
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
signalizaci	signalizace	k1gFnSc4	signalizace
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnSc2	jeho
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
zařízení	zařízení	k1gNnSc2	zařízení
dálkového	dálkový	k2eAgInSc2d1	dálkový
přenosu	přenos	k1gInSc2	přenos
(	(	kIx(	(
<g/>
ZDP	ZDP	kA	ZDP
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
poplachový	poplachový	k2eAgInSc4d1	poplachový
signál	signál	k1gInSc4	signál
přenést	přenést	k5eAaPmF	přenést
např.	např.	kA	např.
na	na	k7c4	na
ohlašovnu	ohlašovna	k1gFnSc4	ohlašovna
požáru	požár	k1gInSc2	požár
HZS	HZS	kA	HZS
<g/>
.	.	kIx.	.
</s>
<s>
Signalizace	signalizace	k1gFnSc1	signalizace
požáru	požár	k1gInSc2	požár
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jednostupňová	jednostupňový	k2eAgFnSc1d1	jednostupňová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dvoustupňová	dvoustupňový	k2eAgFnSc1d1	dvoustupňová
</s>
</p>
<p>
<s>
Při	při	k7c6	při
jednostupňové	jednostupňový	k2eAgFnSc6d1	jednostupňová
signalizaci	signalizace	k1gFnSc6	signalizace
ústředna	ústředna	k1gFnSc1	ústředna
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
poplach	poplach	k1gInSc4	poplach
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
poplach	poplach	k1gInSc1	poplach
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
požáru	požár	k1gInSc2	požár
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
signál	signál	k1gInSc1	signál
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
pokynů	pokyn	k1gInPc2	pokyn
pro	pro	k7c4	pro
evakuaci	evakuace	k1gFnSc4	evakuace
<g/>
,	,	kIx,	,
provedení	provedení	k1gNnSc3	provedení
opatření	opatření	k1gNnPc2	opatření
na	na	k7c6	na
technologiích	technologie	k1gFnPc6	technologie
dle	dle	k7c2	dle
havarijního	havarijní	k2eAgInSc2d1	havarijní
plánu	plán	k1gInSc2	plán
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
dvoustupňové	dvoustupňový	k2eAgFnSc6d1	dvoustupňová
signalizaci	signalizace	k1gFnSc6	signalizace
může	moct	k5eAaImIp3nS	moct
ústředna	ústředna	k1gFnSc1	ústředna
signalizovat	signalizovat	k5eAaImF	signalizovat
úsekový	úsekový	k2eAgInSc4d1	úsekový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
poplach	poplach	k1gInSc4	poplach
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
používá	používat	k5eAaImIp3nS	používat
dva	dva	k4xCgInPc4	dva
provozní	provozní	k2eAgInPc4d1	provozní
režimy	režim	k1gInPc4	režim
<g/>
:	:	kIx,	:
DEN	den	k1gInSc4	den
a	a	k8xC	a
NOC	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
DEN	den	k1gInSc1	den
je	být	k5eAaImIp3nS	být
zapnut	zapnout	k5eAaPmNgInS	zapnout
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
personálu	personál	k1gInSc2	personál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
případný	případný	k2eAgInSc1d1	případný
poplach	poplach	k1gInSc1	poplach
ověří	ověřit	k5eAaPmIp3nS	ověřit
<g/>
.	.	kIx.	.
</s>
<s>
Režim	režim	k1gInSc1	režim
NOC	noc	k1gFnSc1	noc
je	být	k5eAaImIp3nS	být
zapnut	zapnout	k5eAaPmNgInS	zapnout
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
personálu	personál	k1gInSc2	personál
<g/>
.	.	kIx.	.
tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
signál	signál	k1gInSc4	signál
od	od	k7c2	od
tlačítkových	tlačítkový	k2eAgMnPc2d1	tlačítkový
a	a	k8xC	a
samočinných	samočinný	k2eAgMnPc2d1	samočinný
hlásičů	hlásič	k1gMnPc2	hlásič
<g/>
.	.	kIx.	.
</s>
<s>
Signál	signál	k1gInSc1	signál
z	z	k7c2	z
tlačítkového	tlačítkový	k2eAgInSc2d1	tlačítkový
hlásiče	hlásič	k1gInSc2	hlásič
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
věrohodný	věrohodný	k2eAgInSc4d1	věrohodný
a	a	k8xC	a
proto	proto	k8xC	proto
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
spuštění	spuštění	k1gNnSc3	spuštění
všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
poplachu	poplach	k1gInSc2	poplach
<g/>
.	.	kIx.	.
</s>
<s>
Signalizaci	signalizace	k1gFnSc4	signalizace
od	od	k7c2	od
samočinných	samočinný	k2eAgMnPc2d1	samočinný
hlásičů	hlásič	k1gMnPc2	hlásič
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
nejprve	nejprve	k6eAd1	nejprve
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
.	.	kIx.	.
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
DEN	den	k1gInSc1	den
signalizován	signalizován	k2eAgInSc1d1	signalizován
nejprve	nejprve	k6eAd1	nejprve
úsekový	úsekový	k2eAgInSc1d1	úsekový
poplach	poplach	k1gInSc1	poplach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
režimu	režim	k1gInSc6	režim
NOC	noc	k1gFnSc4	noc
je	být	k5eAaImIp3nS	být
ihned	ihned	k6eAd1	ihned
signalizován	signalizovat	k5eAaImNgInS	signalizovat
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
externí	externí	k2eAgInSc4d1	externí
poplach	poplach	k1gInSc4	poplach
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zapojením	zapojení	k1gNnSc7	zapojení
lidského	lidský	k2eAgInSc2d1	lidský
faktoru	faktor	k1gInSc2	faktor
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
vyhodnocování	vyhodnocování	k1gNnSc2	vyhodnocování
poplachů	poplach	k1gInPc2	poplach
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
systému	systém	k1gInSc2	systém
vnesen	vnést	k5eAaPmNgInS	vnést
eventuální	eventuální	k2eAgInSc1d1	eventuální
zdroj	zdroj	k1gInSc1	zdroj
nespolehlivosti	nespolehlivost	k1gFnSc2	nespolehlivost
(	(	kIx(	(
<g/>
omyl	omyl	k1gInSc1	omyl
<g/>
,	,	kIx,	,
úraz	úraz	k1gInSc1	úraz
<g/>
,	,	kIx,	,
selhání	selhání	k1gNnSc1	selhání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc1	systém
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
činnost	činnost	k1gFnSc4	činnost
člověka	člověk	k1gMnSc4	člověk
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
zařízení	zařízení	k1gNnPc1	zařízení
EPS	EPS	kA	EPS
==	==	k?	==
</s>
</p>
<p>
<s>
ZDP	ZDP	kA	ZDP
<g/>
:	:	kIx,	:
Zařízení	zařízení	k1gNnPc1	zařízení
dálkového	dálkový	k2eAgInSc2d1	dálkový
přenosu	přenos	k1gInSc2	přenos
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přenos	přenos	k1gInSc1	přenos
alespoň	alespoň	k9	alespoň
základních	základní	k2eAgInPc2d1	základní
provozních	provozní	k2eAgInPc2d1	provozní
stavů	stav	k1gInPc2	stav
POŽÁR	požár	k1gInSc1	požár
a	a	k8xC	a
PORUCHA	porucha	k1gFnSc1	porucha
na	na	k7c4	na
určené	určený	k2eAgNnSc4d1	určené
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
ohlašovna	ohlašovna	k1gFnSc1	ohlašovna
požárů	požár	k1gInPc2	požár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
je	být	k5eAaImIp3nS	být
zajištěn	zajistit	k5eAaPmNgInS	zajistit
i	i	k9	i
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
<g/>
,	,	kIx,	,
či	či	k8xC	či
selhání	selhání	k1gNnSc1	selhání
obsluhy	obsluha	k1gFnSc2	obsluha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OPPO	OPPO	kA	OPPO
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
obsluhy	obsluha	k1gFnSc2	obsluha
ústředny	ústředna	k1gFnSc2	ústředna
EPS	EPS	kA	EPS
jednotkou	jednotka	k1gFnSc7	jednotka
PO	Po	kA	Po
v	v	k7c6	v
případě	případ	k1gInSc6	případ
požáru	požár	k1gInSc2	požár
signalizovaného	signalizovaný	k2eAgInSc2d1	signalizovaný
EPS	EPS	kA	EPS
se	se	k3xPyFc4	se
připojují	připojovat	k5eAaImIp3nP	připojovat
tzv.	tzv.	kA	tzv.
Obslužná	obslužný	k2eAgFnSc1d1	obslužná
pole	pole	k1gFnSc1	pole
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
OPPO	OPPO	kA	OPPO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provádět	provádět	k5eAaImF	provádět
základní	základní	k2eAgFnSc4d1	základní
obsluhu	obsluha	k1gFnSc4	obsluha
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KTPO	KTPO	kA	KTPO
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
vstupu	vstup	k1gInSc2	vstup
jednotky	jednotka	k1gFnSc2	jednotka
do	do	k7c2	do
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
Klíčový	klíčový	k2eAgInSc4d1	klíčový
trezor	trezor	k1gInSc4	trezor
požární	požární	k2eAgFnSc2d1	požární
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
KTPO	KTPO	kA	KTPO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
klíč	klíč	k1gInSc1	klíč
od	od	k7c2	od
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZOKT	ZOKT	kA	ZOKT
:	:	kIx,	:
Zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
odvod	odvod	k1gInSc4	odvod
kouře	kouř	k1gInSc2	kouř
a	a	k8xC	a
tepla	teplo	k1gNnSc2	teplo
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
umožňující	umožňující	k2eAgNnSc4d1	umožňující
automaticky	automaticky	k6eAd1	automaticky
nebo	nebo	k8xC	nebo
ručně	ručně	k6eAd1	ručně
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
tlačítka	tlačítko	k1gNnSc2	tlačítko
<g/>
)	)	kIx)	)
otevřít	otevřít	k5eAaPmF	otevřít
střešní	střešní	k2eAgMnSc1d1	střešní
okno	okno	k1gNnSc4	okno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
kouřové	kouřový	k2eAgFnSc2d1	kouřová
klapky	klapka	k1gFnSc2	klapka
a	a	k8xC	a
odvést	odvést	k5eAaPmF	odvést
tak	tak	k6eAd1	tak
mimo	mimo	k7c4	mimo
prostory	prostora	k1gFnPc4	prostora
kouř	kouř	k1gInSc4	kouř
<g/>
,	,	kIx,	,
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
teplo	teplo	k1gNnSc4	teplo
vznikající	vznikající	k2eAgNnSc4d1	vznikající
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
požárního	požární	k2eAgNnSc2d1	požární
větrání	větrání	k1gNnSc2	větrání
jsou	být	k5eAaImIp3nP	být
napojeny	napojit	k5eAaPmNgFnP	napojit
na	na	k7c4	na
ústřednu	ústředna	k1gFnSc4	ústředna
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
nesmí	smět	k5eNaImIp3nS	smět
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
funkci	funkce	k1gFnSc4	funkce
sprinklerových	sprinklerův	k2eAgFnPc2d1	sprinklerův
hlavic	hlavice	k1gFnPc2	hlavice
a	a	k8xC	a
detektorů	detektor	k1gInPc2	detektor
kouře	kouř	k1gInSc2	kouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protipožární	protipožární	k2eAgFnPc1d1	protipožární
únikové	únikový	k2eAgFnPc1d1	úniková
dveře	dveře	k1gFnPc1	dveře
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgFnP	vyrábět
ze	z	k7c2	z
speciálních	speciální	k2eAgInPc2d1	speciální
dveřních	dveřní	k2eAgInPc2d1	dveřní
profilů	profil	k1gInPc2	profil
s	s	k7c7	s
utěsněním	utěsnění	k1gNnSc7	utěsnění
proti	proti	k7c3	proti
prachu	prach	k1gInSc3	prach
<g/>
,	,	kIx,	,
hluku	hluk	k1gInSc3	hluk
a	a	k8xC	a
úniku	únik	k1gInSc3	únik
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
jsou	být	k5eAaImIp3nP	být
napojeny	napojen	k2eAgInPc1d1	napojen
na	na	k7c6	na
EPS	EPS	kA	EPS
<g/>
,	,	kIx,	,
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
se	se	k3xPyFc4	se
samy	sám	k3xTgFnPc1	sám
otevřou	otevřít	k5eAaPmIp3nP	otevřít
nebo	nebo	k8xC	nebo
případně	případně	k6eAd1	případně
po	po	k7c6	po
evakuaci	evakuace	k1gFnSc6	evakuace
objektu	objekt	k1gInSc2	objekt
naopak	naopak	k6eAd1	naopak
zavřou	zavřít	k5eAaPmIp3nP	zavřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroly	kontrola	k1gFnSc2	kontrola
==	==	k?	==
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
požární	požární	k2eAgFnSc1d1	požární
signalizace	signalizace	k1gFnSc1	signalizace
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
EPS	EPS	kA	EPS
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
vyhrazené	vyhrazený	k2eAgNnSc4d1	vyhrazené
požárně	požárně	k6eAd1	požárně
bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
a	a	k8xC	a
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
246	[number]	k4	246
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
požární	požární	k2eAgFnSc6d1	požární
prevenci	prevence	k1gFnSc6	prevence
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
provádět	provádět	k5eAaImF	provádět
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
zařízení	zařízení	k1gNnSc6	zařízení
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
kontroly	kontrola	k1gFnSc2	kontrola
provozuschopnosti	provozuschopnost	k1gFnSc2	provozuschopnost
a	a	k8xC	a
zkoušky	zkouška	k1gFnSc2	zkouška
činnosti	činnost	k1gFnSc2	činnost
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zajištění	zajištění	k1gNnSc2	zajištění
požární	požární	k2eAgFnSc2d1	požární
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
objektu	objekt	k1gInSc2	objekt
nebo	nebo	k8xC	nebo
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
kontroly	kontrola	k1gFnPc4	kontrola
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
ročních	roční	k2eAgFnPc2d1	roční
kontrol	kontrola	k1gFnPc2	kontrola
provozuschopnosti	provozuschopnost	k1gFnSc2	provozuschopnost
požárně	požárně	k6eAd1	požárně
bezpečnostního	bezpečnostní	k2eAgNnSc2d1	bezpečnostní
zařízení	zařízení	k1gNnSc2	zařízení
se	se	k3xPyFc4	se
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
při	při	k7c6	při
provozu	provoz	k1gInSc6	provoz
provádějí	provádět	k5eAaImIp3nP	provádět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
×	×	k?	×
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
-	-	kIx~	-
zkoušky	zkouška	k1gFnSc2	zkouška
činnosti	činnost	k1gFnSc2	činnost
pro	pro	k7c4	pro
ústředny	ústředna	k1gFnPc4	ústředna
a	a	k8xC	a
navazující	navazující	k2eAgNnSc4d1	navazující
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
×	×	k?	×
za	za	k7c4	za
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
-	-	kIx~	-
zkoušky	zkouška	k1gFnSc2	zkouška
činnosti	činnost	k1gFnSc2	činnost
samočinných	samočinný	k2eAgMnPc2d1	samočinný
hlásičů	hlásič	k1gMnPc2	hlásič
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
EPS	EPS	kA	EPS
ovládá	ovládat	k5eAaImIp3nS	ovládat
<g/>
.	.	kIx.	.
<g/>
Zkoušky	zkouška	k1gFnPc1	zkouška
činnosti	činnost	k1gFnSc2	činnost
provádí	provádět	k5eAaImIp3nP	provádět
osoby	osoba	k1gFnPc1	osoba
pověřené	pověřený	k2eAgFnPc1d1	pověřená
údržbou	údržba	k1gFnSc7	údržba
tohoto	tento	k3xDgNnSc2	tento
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
provádění	provádění	k1gNnSc6	provádění
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
kontroly	kontrola	k1gFnSc2	kontrola
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
zařízení	zařízení	k1gNnSc6	zařízení
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
předem	předem	k6eAd1	předem
uvědoměna	uvědomit	k5eAaPmNgFnS	uvědomit
osoba	osoba	k1gFnSc1	osoba
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
provoz	provoz	k1gInSc4	provoz
a	a	k8xC	a
obsluhu	obsluha	k1gFnSc4	obsluha
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
provozuschopnosti	provozuschopnost	k1gFnSc2	provozuschopnost
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
zkoušku	zkouška	k1gFnSc4	zkouška
činnosti	činnost	k1gFnSc2	činnost
při	při	k7c6	při
shodném	shodný	k2eAgInSc6d1	shodný
termínu	termín	k1gInSc6	termín
provedení	provedení	k1gNnSc2	provedení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
půlroční	půlroční	k2eAgFnSc2d1	půlroční
zkoušky	zkouška	k1gFnSc2	zkouška
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyhotovit	vyhotovit	k5eAaPmF	vyhotovit
doklad	doklad	k1gInSc4	doklad
<g/>
,	,	kIx,	,
a	a	k8xC	a
provést	provést	k5eAaPmF	provést
zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
provozní	provozní	k2eAgFnSc2d1	provozní
knihy	kniha	k1gFnSc2	kniha
EPS	EPS	kA	EPS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součásti	součást	k1gFnPc4	součást
každého	každý	k3xTgInSc2	každý
systému	systém	k1gInSc2	systém
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
měsíčním	měsíční	k2eAgFnPc3d1	měsíční
zkouškám	zkouška	k1gFnPc3	zkouška
činnosti	činnost	k1gFnSc2	činnost
postačí	postačit	k5eAaPmIp3nS	postačit
zápis	zápis	k1gInSc1	zápis
do	do	k7c2	do
provozní	provozní	k2eAgFnSc2d1	provozní
knihy	kniha	k1gFnSc2	kniha
EPS	EPS	kA	EPS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
požární	požární	k2eAgFnPc4d1	požární
signalizace	signalizace	k1gFnPc4	signalizace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
požární	požární	k2eAgFnPc4d1	požární
signalizace	signalizace	k1gFnPc4	signalizace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
č.	č.	k?	č.
<g/>
246	[number]	k4	246
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
–	–	k?	–
vyhláška	vyhláška	k1gFnSc1	vyhláška
o	o	k7c6	o
požární	požární	k2eAgFnSc6d1	požární
prevenci	prevence	k1gFnSc6	prevence
</s>
</p>
