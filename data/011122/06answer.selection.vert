<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
požární	požární	k2eAgFnSc1d1	požární
signalizace	signalizace	k1gFnSc1	signalizace
(	(	kIx(	(
<g/>
EPS	EPS	kA	EPS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyhrazené	vyhrazený	k2eAgNnSc1d1	vyhrazené
požárně	požárně	k6eAd1	požárně
bezpečnostní	bezpečnostní	k2eAgNnSc1d1	bezpečnostní
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
požární	požární	k2eAgFnSc1d1	požární
signalizace	signalizace	k1gFnSc1	signalizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pomocí	pomocí	k7c2	pomocí
hlásičů	hlásič	k1gMnPc2	hlásič
včasnou	včasný	k2eAgFnSc4d1	včasná
signalizaci	signalizace	k1gFnSc4	signalizace
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
