<p>
<s>
Kreolština	kreolština	k1gFnSc1	kreolština
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
criollo	criollo	k1gNnSc1	criollo
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
jazykovědě	jazykověda	k1gFnSc6	jazykověda
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
změněný	změněný	k2eAgInSc1d1	změněný
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vlivu	vliv	k1gInSc2	vliv
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tímto	tento	k3xDgInSc7	tento
vlivem	vliv	k1gInSc7	vliv
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
vliv	vliv	k1gInSc1	vliv
mateřského	mateřský	k2eAgInSc2d1	mateřský
jazyka	jazyk	k1gInSc2	jazyk
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Kreolizované	Kreolizovaný	k2eAgInPc1d1	Kreolizovaný
jazyky	jazyk	k1gInPc1	jazyk
vznikaly	vznikat	k5eAaImAgInP	vznikat
především	především	k9	především
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
evropští	evropský	k2eAgMnPc1d1	evropský
osadníci	osadník	k1gMnPc1	osadník
a	a	k8xC	a
stýkali	stýkat	k5eAaImAgMnP	stýkat
se	se	k3xPyFc4	se
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nejen	nejen	k6eAd1	nejen
tam	tam	k6eAd1	tam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k6eAd1	tak
kreolizované	kreolizovaný	k2eAgFnPc1d1	kreolizovaná
formy	forma	k1gFnPc1	forma
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
původní	původní	k2eAgFnSc3d1	původní
podobě	podoba	k1gFnSc3	podoba
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
pozměněny	pozměnit	k5eAaPmNgInP	pozměnit
a	a	k8xC	a
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jim	on	k3xPp3gMnPc3	on
dobře	dobře	k6eAd1	dobře
srozumitelné	srozumitelný	k2eAgNnSc4d1	srozumitelné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
mylné	mylný	k2eAgFnSc3d1	mylná
představě	představa	k1gFnSc3	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
formy	forma	k1gFnPc1	forma
kreolštiny	kreolština	k1gFnSc2	kreolština
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
podobné	podobný	k2eAgNnSc4d1	podobné
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
například	například	k6eAd1	například
kreolizovaná	kreolizovaný	k2eAgFnSc1d1	kreolizovaná
francouzština	francouzština	k1gFnSc1	francouzština
a	a	k8xC	a
kreolizovaná	kreolizovaný	k2eAgFnSc1d1	kreolizovaná
malajština	malajština	k1gFnSc1	malajština
zcela	zcela	k6eAd1	zcela
nesrozumitelné	srozumitelný	k2eNgFnSc2d1	nesrozumitelná
a	a	k8xC	a
např.	např.	kA	např.
verze	verze	k1gFnPc4	verze
kreolizované	kreolizovaný	k2eAgFnSc2d1	kreolizovaná
francouzštiny	francouzština	k1gFnSc2	francouzština
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
často	často	k6eAd1	často
méně	málo	k6eAd2	málo
podobné	podobný	k2eAgFnPc1d1	podobná
než	než	k8xS	než
se	s	k7c7	s
samotnou	samotný	k2eAgFnSc7d1	samotná
spisovnou	spisovný	k2eAgFnSc7d1	spisovná
francouzštinou	francouzština	k1gFnSc7	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
věta	věta	k1gFnSc1	věta
všichni	všechen	k3xTgMnPc1	všechen
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
společně	společně	k6eAd1	společně
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
naší	náš	k3xOp1gFnSc2	náš
budoucnosti	budoucnost	k1gFnSc2	budoucnost
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
francouzsky	francouzsky	k6eAd1	francouzsky
nous	nous	k6eAd1	nous
tous	tous	k6eAd1	tous
avons	avons	k6eAd1	avons
besoin	besoin	k2eAgInSc1d1	besoin
de	de	k?	de
travailler	travailler	k1gInSc1	travailler
ensemble	ensemble	k1gInSc1	ensemble
à	à	k?	à
créer	créer	k1gInSc1	créer
notre	notr	k1gInSc5	notr
avenir	avenir	k1gInSc4	avenir
a	a	k8xC	a
seychelsky	seychelsky	k6eAd1	seychelsky
nou	nou	k?	nou
tou	ten	k3xDgFnSc7	ten
bezwen	bezwno	k1gNnPc2	bezwno
travay	travaa	k1gMnSc2	travaa
ansamn	ansamna	k1gFnPc2	ansamna
pou	pou	k?	pou
kree	kree	k1gNnSc4	kree
nou	nou	k?	nou
lavenir	lavenira	k1gFnPc2	lavenira
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
podobná	podobný	k2eAgFnSc1d1	podobná
(	(	kIx(	(
<g/>
kreolizované	kreolizovaný	k2eAgInPc1d1	kreolizovaný
jazyky	jazyk	k1gInPc1	jazyk
obvykle	obvykle	k6eAd1	obvykle
používají	používat	k5eAaImIp3nP	používat
více	hodně	k6eAd2	hodně
fonetický	fonetický	k2eAgInSc4d1	fonetický
přepis	přepis	k1gInSc4	přepis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
kreolštiny	kreolština	k1gFnSc2	kreolština
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
mulati	mulat	k1gMnPc1	mulat
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
mestici	mestic	k1gMnPc1	mestic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
španělsky	španělsky	k6eAd1	španělsky
hovořících	hovořící	k2eAgFnPc6d1	hovořící
zemích	zem	k1gFnPc6	zem
nebo	nebo	k8xC	nebo
mezi	mezi	k7c7	mezi
Afrikánci	Afrikánec	k1gMnPc7	Afrikánec
převládají	převládat	k5eAaImIp3nP	převládat
i	i	k9	i
uživatelé	uživatel	k1gMnPc1	uživatel
světlé	světlý	k2eAgFnSc2d1	světlá
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
pojem	pojem	k1gInSc4	pojem
kreol	kreol	k1gMnSc1	kreol
často	často	k6eAd1	často
neoznačuje	označovat	k5eNaImIp3nS	označovat
osobu	osoba	k1gFnSc4	osoba
používající	používající	k2eAgFnSc4d1	používající
kreolštinu	kreolština	k1gFnSc4	kreolština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potomka	potomek	k1gMnSc2	potomek
evropských	evropský	k2eAgMnPc2d1	evropský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
bez	bez	k7c2	bez
bližšího	bližší	k1gNnSc2	bližší
jazykového	jazykový	k2eAgNnSc2d1	jazykové
určení	určení	k1gNnSc2	určení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
zaměňování	zaměňování	k1gNnSc3	zaměňování
<g/>
.	.	kIx.	.
</s>
<s>
Kreolizací	Kreolizace	k1gFnSc7	Kreolizace
a	a	k8xC	a
kreolštinou	kreolština	k1gFnSc7	kreolština
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
obor	obor	k1gInSc1	obor
lingvistiky	lingvistika	k1gFnSc2	lingvistika
zvaný	zvaný	k2eAgInSc1d1	zvaný
kreolistika	kreolistika	k1gFnSc1	kreolistika
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
zažila	zažít	k5eAaPmAgFnS	zažít
naposledy	naposledy	k6eAd1	naposledy
významný	významný	k2eAgInSc4d1	významný
případ	případ	k1gInSc4	případ
kreolizace	kreolizace	k1gFnSc2	kreolizace
v	v	k7c6	v
období	období	k1gNnSc6	období
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kreolizací	kreolizace	k1gFnSc7	kreolizace
latiny	latina	k1gFnSc2	latina
postupně	postupně	k6eAd1	postupně
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
románské	románský	k2eAgInPc1d1	románský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Předstupněm	předstupeň	k1gInSc7	předstupeň
kreolizace	kreolizace	k1gFnSc2	kreolizace
je	být	k5eAaImIp3nS	být
pidžinizace	pidžinizace	k1gFnSc1	pidžinizace
<g/>
.	.	kIx.	.
</s>
<s>
Pidžinizací	Pidžinizace	k1gFnSc7	Pidžinizace
původního	původní	k2eAgInSc2d1	původní
jazyka	jazyk	k1gInSc2	jazyk
vzniká	vznikat	k5eAaImIp3nS	vznikat
dorozumívací	dorozumívací	k2eAgInSc1d1	dorozumívací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
úplnou	úplný	k2eAgFnSc4d1	úplná
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Pidžin	Pidžin	k1gMnSc1	Pidžin
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
mateřštinou	mateřština	k1gFnSc7	mateřština
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vyvinout	vyvinout	k5eAaPmF	vyvinout
v	v	k7c4	v
mateřštinu	mateřština	k1gFnSc4	mateřština
následující	následující	k2eAgFnSc2d1	následující
generace	generace	k1gFnSc2	generace
mluvčích	mluvčí	k1gMnPc2	mluvčí
a	a	k8xC	a
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
úplné	úplný	k2eAgFnSc3d1	úplná
gramatizaci	gramatizace	k1gFnSc3	gramatizace
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
kreolizován	kreolizován	k2eAgMnSc1d1	kreolizován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
kreolizovaných	kreolizovaný	k2eAgInPc2d1	kreolizovaný
jazyků	jazyk	k1gInPc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
francouzštiny	francouzština	k1gFnSc2	francouzština
===	===	k?	===
</s>
</p>
<p>
<s>
karibská	karibský	k2eAgFnSc1d1	karibská
francouzština	francouzština	k1gFnSc1	francouzština
</s>
</p>
<p>
<s>
guayanština	guayanština	k1gFnSc1	guayanština
ve	v	k7c6	v
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
Guyaně	Guyana	k1gFnSc6	Guyana
</s>
</p>
<p>
<s>
louisianská	louisianský	k2eAgFnSc1d1	louisianská
kreolština	kreolština	k1gFnSc1	kreolština
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
</s>
</p>
<p>
<s>
haitská	haitský	k2eAgFnSc1d1	Haitská
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
</s>
</p>
<p>
<s>
antilská	antilský	k2eAgFnSc1d1	antilská
kreolština	kreolština	k1gFnSc1	kreolština
</s>
</p>
<p>
<s>
guadeloupská	guadeloupský	k2eAgFnSc1d1	guadeloupský
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c6	na
Guadeloupe	Guadeloupe	k1gFnSc6	Guadeloupe
</s>
</p>
<p>
<s>
dominická	dominický	k2eAgFnSc1d1	dominický
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c6	na
Dominice	Dominika	k1gFnSc6	Dominika
</s>
</p>
<p>
<s>
martinická	martinický	k2eAgFnSc1d1	martinická
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c6	na
Martiniku	Martinik	k1gInSc6	Martinik
</s>
</p>
<p>
<s>
svato-lucijská	svatoucijský	k2eAgFnSc1d1	svato-lucijský
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Lucii	Lucie	k1gFnSc6	Lucie
</s>
</p>
<p>
<s>
bourbonská	bourbonský	k2eAgFnSc1d1	Bourbonská
francouzština	francouzština	k1gFnSc1	francouzština
</s>
</p>
<p>
<s>
réunionská	réunionský	k2eAgFnSc1d1	réunionský
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c6	na
Réunionu	Réunion	k1gInSc6	Réunion
</s>
</p>
<p>
<s>
mauricijská	mauricijský	k2eAgFnSc1d1	mauricijská
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c4	na
Mauritius	Mauritius	k1gInSc4	Mauritius
</s>
</p>
<p>
<s>
seychelština	seychelština	k1gFnSc1	seychelština
na	na	k7c6	na
Seychelách	Seychely	k1gFnPc6	Seychely
</s>
</p>
<p>
<s>
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
kreolizované	kreolizovaný	k2eAgFnSc2d1	kreolizovaná
francouzštiny	francouzština	k1gFnSc2	francouzština
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
na	na	k7c6	na
Nové	Nové	k2eAgFnSc6d1	Nové
Kaledonii	Kaledonie	k1gFnSc6	Kaledonie
(	(	kIx(	(
<g/>
Tayo	Tayo	k6eAd1	Tayo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
angličtiny	angličtina	k1gFnSc2	angličtina
===	===	k?	===
</s>
</p>
<p>
<s>
bislamština	bislamština	k1gFnSc1	bislamština
na	na	k7c4	na
Vanuatu	Vanuata	k1gFnSc4	Vanuata
</s>
</p>
<p>
<s>
jamajský	jamajský	k2eAgInSc1d1	jamajský
patois	patois	k1gInSc1	patois
na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
</s>
</p>
<p>
<s>
havajská	havajský	k2eAgFnSc1d1	Havajská
angličtina	angličtina	k1gFnSc1	angličtina
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
</s>
</p>
<p>
<s>
gullah	gullah	k1gInSc1	gullah
v	v	k7c6	v
Georgii	Georgie	k1gFnSc6	Georgie
</s>
</p>
<p>
<s>
pitcairnština	pitcairnština	k1gFnSc1	pitcairnština
na	na	k7c6	na
Pitcairnově	Pitcairnův	k2eAgInSc6d1	Pitcairnův
ostrově	ostrov	k1gInSc6	ostrov
</s>
</p>
<p>
<s>
belizská	belizský	k2eAgFnSc1d1	Belizská
kreolština	kreolština	k1gFnSc1	kreolština
v	v	k7c6	v
Belize	Beliza	k1gFnSc6	Beliza
</s>
</p>
<p>
<s>
Tok	tok	k1gInSc1	tok
Pisin	Pisin	k1gInSc1	Pisin
na	na	k7c6	na
Papui-Nové	Papui-Nové	k2eAgFnSc6d1	Papui-Nové
Guineji	Guinea	k1gFnSc6	Guinea
</s>
</p>
<p>
<s>
Sranan	Sranan	k1gMnSc1	Sranan
Tongo	Tongo	k1gMnSc1	Tongo
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
</s>
</p>
<p>
<s>
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
španělštiny	španělština	k1gFnSc2	španělština
===	===	k?	===
</s>
</p>
<p>
<s>
papiamento	papiamento	k1gNnSc1	papiamento
na	na	k7c6	na
Arubě	Aruba	k1gFnSc6	Aruba
<g/>
,	,	kIx,	,
Bonaire	Bonair	k1gInSc5	Bonair
a	a	k8xC	a
Curaçau	Curaçaus	k1gInSc3	Curaçaus
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
ovšem	ovšem	k9	ovšem
řazené	řazený	k2eAgFnPc1d1	řazená
i	i	k9	i
mezi	mezi	k7c4	mezi
kreolizovanou	kreolizovaný	k2eAgFnSc4d1	kreolizovaná
portugalštinu	portugalština	k1gFnSc4	portugalština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
chamorro	chamorro	k6eAd1	chamorro
na	na	k7c6	na
Guamu	Guamo	k1gNnSc6	Guamo
a	a	k8xC	a
Severních	severní	k2eAgInPc2d1	severní
Mariánách	Mariánách	k?	Mariánách
</s>
</p>
<p>
<s>
některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
smíšením	smíšení	k1gNnSc7	smíšení
s	s	k7c7	s
indiánskými	indiánský	k2eAgMnPc7d1	indiánský
(	(	kIx(	(
<g/>
palenquero	palenquero	k1gNnSc1	palenquero
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
chavacano	chavacana	k1gFnSc5	chavacana
používané	používaný	k2eAgInPc4d1	používaný
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
portugalštiny	portugalština	k1gFnSc2	portugalština
===	===	k?	===
</s>
</p>
<p>
<s>
kapverdská	kapverdský	k2eAgFnSc1d1	Kapverdská
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c6	na
Kapverdách	Kapverda	k1gFnPc6	Kapverda
</s>
</p>
<p>
<s>
papiamento	papiamento	k1gNnSc1	papiamento
na	na	k7c6	na
Arubě	Aruba	k1gFnSc6	Aruba
<g/>
,	,	kIx,	,
Bonaire	Bonair	k1gInSc5	Bonair
a	a	k8xC	a
Curaçau	Curaçaa	k1gMnSc4	Curaçaa
</s>
</p>
<p>
<s>
angolská	angolský	k2eAgFnSc1d1	angolská
kreolština	kreolština	k1gFnSc1	kreolština
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc1d1	princův
ostrov	ostrov	k1gInSc1	ostrov
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
hindštiny	hindština	k1gFnSc2	hindština
===	===	k?	===
</s>
</p>
<p>
<s>
Andamanská	Andamanský	k2eAgFnSc1d1	Andamanský
kreolizovaná	kreolizovaný	k2eAgFnSc1d1	kreolizovaná
hindština	hindština	k1gFnSc1	hindština
na	na	k7c6	na
Andamanských	Andamanský	k2eAgInPc6d1	Andamanský
ostrovech	ostrov	k1gInPc6	ostrov
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
malajštiny	malajština	k1gFnSc2	malajština
===	===	k?	===
</s>
</p>
<p>
<s>
peranakanština	peranakanština	k1gFnSc1	peranakanština
<g/>
,	,	kIx,	,
ambonština	ambonština	k1gFnSc1	ambonština
<g/>
,	,	kIx,	,
bandština	bandština	k1gFnSc1	bandština
<g/>
,	,	kIx,	,
betawi	betawi	k1gNnSc1	betawi
<g/>
,	,	kIx,	,
babang	babang	k1gInSc1	babang
<g/>
,	,	kIx,	,
kupangština	kupangština	k1gFnSc1	kupangština
<g/>
,	,	kIx,	,
manado	manada	k1gFnSc5	manada
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc6	Malajsie
a	a	k8xC	a
na	na	k7c6	na
Cejlonu	Cejlon	k1gInSc6	Cejlon
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
němčiny	němčina	k1gFnSc2	němčina
===	===	k?	===
</s>
</p>
<p>
<s>
unserdeutsch	unserdeutsch	k1gInSc1	unserdeutsch
(	(	kIx(	(
<g/>
rabaulština	rabaulština	k1gFnSc1	rabaulština
<g/>
)	)	kIx)	)
na	na	k7c6	na
Papui-Nové	Papui-Nové	k2eAgFnSc6d1	Papui-Nové
Guineji	Guinea	k1gFnSc6	Guinea
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
===	===	k?	===
</s>
</p>
<p>
<s>
Sranan	Sranan	k1gMnSc1	Sranan
Tongo	Tongo	k1gMnSc1	Tongo
neboli	neboli	k8xC	neboli
surinamština	surinamština	k1gFnSc1	surinamština
v	v	k7c6	v
Surinamu	Surinam	k1gInSc6	Surinam
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Nizozemské	nizozemský	k2eAgFnSc6d1	nizozemská
Guyaně	Guyana	k1gFnSc6	Guyana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
afrikánština	afrikánština	k1gFnSc1	afrikánština
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
i	i	k8xC	i
francouzštinou	francouzština	k1gFnSc7	francouzština
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kreolizované	kreolizovaný	k2eAgFnPc1d1	kreolizovaná
formy	forma	k1gFnPc1	forma
afrikánštiny	afrikánština	k1gFnSc2	afrikánština
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
oorlamština	oorlamština	k1gFnSc1	oorlamština
v	v	k7c6	v
Namibii	Namibie	k1gFnSc6	Namibie
</s>
</p>
<p>
<s>
A	a	k9	a
další	další	k2eAgMnPc1d1	další
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
arabštiny	arabština	k1gFnSc2	arabština
===	===	k?	===
</s>
</p>
<p>
<s>
Maltština	maltština	k1gFnSc1	maltština
<g/>
,	,	kIx,	,
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
jazyk	jazyk	k1gInSc1	jazyk
Malty	Malta	k1gFnSc2	Malta
</s>
</p>
<p>
<s>
Několik	několik	k4yIc1	několik
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižní	jižní	k2eAgFnSc2d1	jižní
Sahary	Sahara	k1gFnSc2	Sahara
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Nubi	Nubi	k1gNnSc1	Nubi
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
čínštiny	čínština	k1gFnSc2	čínština
===	===	k?	===
</s>
</p>
<p>
<s>
tangwang	tangwang	k1gMnSc1	tangwang
<g/>
,	,	kIx,	,
hokaglish	hokaglish	k1gMnSc1	hokaglish
<g/>
,	,	kIx,	,
wutun	wutun	k1gMnSc1	wutun
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
ásámštiny	ásámština	k1gFnSc2	ásámština
===	===	k?	===
</s>
</p>
<p>
<s>
Nágálandská	Nágálandský	k2eAgFnSc1d1	Nágálandský
kreolština	kreolština	k1gFnSc1	kreolština
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
japonštiny	japonština	k1gFnSc2	japonština
===	===	k?	===
</s>
</p>
<p>
<s>
Ilanská	Ilanský	k2eAgFnSc1d1	Ilanský
kreolizovaná	kreolizovaný	k2eAgFnSc1d1	kreolizovaná
japonština	japonština	k1gFnSc1	japonština
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KOVÁŘ	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Vybrané	vybraný	k2eAgInPc1d1	vybraný
problémy	problém	k1gInPc4	problém
z	z	k7c2	z
regionální	regionální	k2eAgFnSc2d1	regionální
geografie	geografie	k1gFnSc2	geografie
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Frankofonie	Frankofonie	k1gFnSc2	Frankofonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
:	:	kIx,	:
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
60	[number]	k4	60
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978-80-7368-354-2	[number]	k4	978-80-7368-354-2
</s>
</p>
<p>
<s>
KLÉGR	KLÉGR	kA	KLÉGR
<g/>
,	,	kIx,	,
A.	A.	kA	A.
et	et	k?	et
ZIMA	Zima	k1gMnSc1	Zima
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Světem	svět	k1gInSc7	svět
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kreoloid	Kreoloid	k1gInSc1	Kreoloid
</s>
</p>
