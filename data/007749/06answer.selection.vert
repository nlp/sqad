<s>
Jelikož	jelikož	k8xS	jelikož
Venuše	Venuše	k1gFnSc1	Venuše
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gInSc1	její
povrch	povrch	k1gInSc1	povrch
zcela	zcela	k6eAd1	zcela
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
a	a	k8xC	a
částicemi	částice	k1gFnPc7	částice
dopadajícími	dopadající	k2eAgFnPc7d1	dopadající
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
