<p>
<s>
Mořice	Mořic	k1gMnPc4	Mořic
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Prostějov	Prostějov	k1gInSc1	Prostějov
v	v	k7c6	v
Olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
532	[number]	k4	532
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1238	[number]	k4	1238
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
z	z	k7c2	z
listiny	listina	k1gFnSc2	listina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
bratr	bratr	k1gMnSc1	bratr
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
a	a	k8xC	a
markrabě	markrabě	k1gMnSc1	markrabě
moravský	moravský	k2eAgMnSc1d1	moravský
Přemysl	Přemysl	k1gMnSc1	Přemysl
Doubravnickému	Doubravnický	k2eAgInSc3d1	Doubravnický
klášteru	klášter	k1gInSc3	klášter
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
ves	ves	k1gFnSc4	ves
Semislav	Semislava	k1gFnPc2	Semislava
na	na	k7c6	na
Opavsku	Opavsko	k1gNnSc6	Opavsko
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
svědky	svědek	k1gMnPc4	svědek
vyhotovení	vyhotovení	k1gNnPc2	vyhotovení
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
Předibor	Předibor	k1gInSc1	Předibor
z	z	k7c2	z
Mořic	Mořic	k1gMnSc1	Mořic
–	–	k?	–
nižší	nízký	k2eAgMnSc1d2	nižší
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
v	v	k7c4	v
Mořicích	mořicí	k2eAgInPc2d1	mořicí
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
se	se	k3xPyFc4	se
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
se	se	k3xPyFc4	se
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Mořic	Mořic	k1gMnSc1	Mořic
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
páni	pan	k1gMnPc1	pan
z	z	k7c2	z
Kravař	Kravaře	k1gInPc2	Kravaře
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
šlechtické	šlechtický	k2eAgInPc1d1	šlechtický
rody	rod	k1gInPc1	rod
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
Žerotínové	Žerotín	k1gMnPc1	Žerotín
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
panství	panství	k1gNnSc1	panství
patřilo	patřit	k5eAaImAgNnS	patřit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1620	[number]	k4	1620
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
Bedřichu	Bedřich	k1gMnSc6	Bedřich
Vilémovi	Vilém	k1gMnSc6	Vilém
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
<g/>
,	,	kIx,	,
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
stavovském	stavovský	k2eAgNnSc6d1	Stavovské
povstání	povstání	k1gNnSc6	povstání
zbaveném	zbavený	k2eAgInSc6d1	zbavený
majetku	majetek	k1gInSc6	majetek
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
Mořice	Mořic	k1gMnPc4	Mořic
jako	jako	k8xS	jako
konfiskát	konfiskát	k1gInSc4	konfiskát
Maxmilián	Maxmiliána	k1gFnPc2	Maxmiliána
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Lichtenštejn	Lichtenštejn	k1gMnSc1	Lichtenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
záhy	záhy	k6eAd1	záhy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1633	[number]	k4	1633
<g/>
,	,	kIx,	,
daroval	darovat	k5eAaPmAgMnS	darovat
Mořice	Mořic	k1gMnPc4	Mořic
klášteru	klášter	k1gInSc3	klášter
paulánů	paulán	k1gMnPc2	paulán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Kateřinou	Kateřina	k1gFnSc7	Kateřina
rozenou	rozený	k2eAgFnSc7d1	rozená
z	z	k7c2	z
Boskovic	Boskovice	k1gInPc2	Boskovice
ve	v	k7c6	v
Vranově	Vranov	k1gInSc6	Vranov
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
řádu	řád	k1gInSc2	řád
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
Mořice	Mořic	k1gMnPc4	Mořic
až	až	k6eAd1	až
do	do	k7c2	do
jeho	jeho	k3xOp3gNnSc2	jeho
zrušení	zrušení	k1gNnSc2	zrušení
císařem	císař	k1gMnSc7	císař
Josefem	Josef	k1gMnSc7	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1784	[number]	k4	1784
<g/>
,	,	kIx,	,
panství	panství	k1gNnSc1	panství
pak	pak	k6eAd1	pak
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Moravského	moravský	k2eAgInSc2d1	moravský
náboženského	náboženský	k2eAgInSc2d1	náboženský
fondu	fond	k1gInSc2	fond
<g/>
;	;	kIx,	;
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
je	být	k5eAaImIp3nS	být
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
za	za	k7c4	za
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
rakouský	rakouský	k2eAgMnSc1d1	rakouský
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Karel	Karel	k1gMnSc1	Karel
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Parmy	Parma	k1gFnSc2	Parma
a	a	k8xC	a
Modeny	Modena	k1gFnSc2	Modena
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
jeho	jeho	k3xOp3gMnSc1	jeho
synovec	synovec	k1gMnSc1	synovec
František	František	k1gMnSc1	František
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
na	na	k7c6	na
Modeně	Modena	k1gFnSc6	Modena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
panství	panství	k1gNnPc2	panství
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Fürstenberkem	Fürstenberek	k1gMnSc7	Fürstenberek
pro	pro	k7c4	pro
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
držení	držení	k1gNnSc6	držení
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zbytkový	zbytkový	k2eAgInSc1d1	zbytkový
statek	statek	k1gInSc1	statek
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
polnosti	polnost	k1gFnPc1	polnost
byly	být	k5eAaImAgFnP	být
přiděleny	přidělit	k5eAaPmNgFnP	přidělit
občanům	občan	k1gMnPc3	občan
z	z	k7c2	z
Mořic	Mořic	k1gMnSc1	Mořic
<g/>
,	,	kIx,	,
Němčic	Němčice	k1gFnPc2	Němčice
<g/>
,	,	kIx,	,
Nezamyslic	Nezamyslice	k1gFnPc2	Nezamyslice
a	a	k8xC	a
Vrchoslavic	Vrchoslavice	k1gFnPc2	Vrchoslavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgFnSc3d1	významná
události	událost	k1gFnSc3	událost
===	===	k?	===
</s>
</p>
<p>
<s>
1807	[number]	k4	1807
–	–	k?	–
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
byla	být	k5eAaImAgFnS	být
zničena	zničen	k2eAgFnSc1d1	zničena
celá	celý	k2eAgFnSc1d1	celá
obec	obec	k1gFnSc1	obec
i	i	k9	i
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
dvou	dva	k4xCgNnPc2	dva
stavení	stavení	k1gNnPc2	stavení
za	za	k7c7	za
mlýnskou	mlýnský	k2eAgFnSc7d1	Mlýnská
strouhou	strouha	k1gFnSc7	strouha
</s>
</p>
<p>
<s>
1866	[number]	k4	1866
–	–	k?	–
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
prusko-rakouské	pruskoakouský	k2eAgFnSc2d1	prusko-rakouská
války	válka	k1gFnSc2	válka
epidemie	epidemie	k1gFnSc2	epidemie
cholery	cholera	k1gFnSc2	cholera
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sem	sem	k6eAd1	sem
zavlekli	zavleknout	k5eAaPmAgMnP	zavleknout
pruští	pruský	k2eAgMnPc1d1	pruský
vojáci	voják	k1gMnPc1	voják
–	–	k?	–
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
29	[number]	k4	29
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
1871	[number]	k4	1871
–	–	k?	–
otevřena	otevřen	k2eAgFnSc1d1	otevřena
obecní	obecní	k2eAgFnSc1d1	obecní
knihovna	knihovna	k1gFnSc1	knihovna
</s>
</p>
<p>
<s>
1872	[number]	k4	1872
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
otevřena	otevřít	k5eAaPmNgFnS	otevřít
nová	nový	k2eAgFnSc1d1	nová
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
1890	[number]	k4	1890
–	–	k?	–
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Rolnicko-občanská	rolnickobčanský	k2eAgFnSc1d1	rolnicko-občanský
záložna	záložna	k1gFnSc1	záložna
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
kampeličku	kampelička	k1gFnSc4	kampelička
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1891	[number]	k4	1891
–	–	k?	–
vznik	vznik	k1gInSc1	vznik
družstva	družstvo	k1gNnSc2	družstvo
Parní	parní	k2eAgFnSc2d1	parní
mlátičky	mlátička	k1gFnSc2	mlátička
</s>
</p>
<p>
<s>
1896	[number]	k4	1896
–	–	k?	–
založena	založen	k2eAgFnSc1d1	založena
Dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
hasičská	hasičský	k2eAgFnSc1d1	hasičská
jednota	jednota	k1gFnSc1	jednota
</s>
</p>
<p>
<s>
1897	[number]	k4	1897
–	–	k?	–
zřízen	zřízen	k2eAgInSc1d1	zřízen
samostatný	samostatný	k2eAgInSc1d1	samostatný
poštovní	poštovní	k2eAgInSc1d1	poštovní
úřad	úřad	k1gInSc1	úřad
</s>
</p>
<p>
<s>
1900	[number]	k4	1900
–	–	k?	–
vznik	vznik	k1gInSc1	vznik
Družstva	družstvo	k1gNnSc2	družstvo
pro	pro	k7c4	pro
pojištění	pojištění	k1gNnSc4	pojištění
hovězího	hovězí	k2eAgInSc2d1	hovězí
dobytka	dobytek	k1gInSc2	dobytek
</s>
</p>
<p>
<s>
1910	[number]	k4	1910
–	–	k?	–
postaveno	postavit	k5eAaPmNgNnS	postavit
hasičské	hasičský	k2eAgNnSc1d1	hasičské
skladiště	skladiště	k1gNnSc1	skladiště
</s>
</p>
<p>
<s>
1919	[number]	k4	1919
–	–	k?	–
založena	založen	k2eAgFnSc1d1	založena
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Sokol	Sokol	k1gMnSc1	Sokol
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
odbočka	odbočka	k1gFnSc1	odbočka
Sokola	Sokol	k1gMnSc2	Sokol
v	v	k7c6	v
Němčicích	Němčice	k1gFnPc6	Němčice
nad	nad	k7c7	nad
Hanou	Hana	k1gFnSc7	Hana
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
–	–	k?	–
zřízena	zřízen	k2eAgFnSc1d1	zřízena
telefonní	telefonní	k2eAgFnSc1d1	telefonní
hovorna	hovorna	k1gFnSc1	hovorna
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
otevřena	otevřít	k5eAaPmNgFnS	otevřít
Sokolovna	sokolovna	k1gFnSc1	sokolovna
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
ustaven	ustavit	k5eAaPmNgInS	ustavit
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
JZD	JZD	kA	JZD
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
obec	obec	k1gFnSc4	obec
i	i	k9	i
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
uvádí	uvádět	k5eAaImIp3nS	uvádět
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
i	i	k9	i
příslušnost	příslušnost	k1gFnSc4	příslušnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
či	či	k8xC	či
následné	následný	k2eAgNnSc4d1	následné
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Mořice	Mořic	k1gMnSc2	Mořic
–	–	k?	–
barokní	barokní	k2eAgInSc4d1	barokní
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
tvrze	tvrz	k1gFnSc2	tvrz
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
připomínaná	připomínaný	k2eAgNnPc1d1	připomínané
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1384	[number]	k4	1384
<g/>
)	)	kIx)	)
koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nebo	nebo	k8xC	nebo
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
<s>
Archivně	archivně	k6eAd1	archivně
je	být	k5eAaImIp3nS	být
doložena	doložen	k2eAgFnSc1d1	doložena
dostavba	dostavba	k1gFnSc1	dostavba
věže	věž	k1gFnSc2	věž
roku	rok	k1gInSc2	rok
1709	[number]	k4	1709
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
zámek	zámek	k1gInSc1	zámek
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
zřejmě	zřejmě	k6eAd1	zřejmě
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Martina	Martin	k1gMnSc2	Martin
–	–	k?	–
V	v	k7c6	v
letech	let	k1gInPc6	let
1703	[number]	k4	1703
<g/>
–	–	k?	–
<g/>
1728	[number]	k4	1728
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
etapách	etapa	k1gFnPc6	etapa
1703	[number]	k4	1703
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
1708	[number]	k4	1708
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
1728	[number]	k4	1728
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zámek	zámek	k1gInSc1	zámek
patřil	patřit	k5eAaImAgInS	patřit
řádu	řád	k1gInSc3	řád
paulánů	paulán	k1gMnPc2	paulán
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kaple	kaple	k1gFnSc2	kaple
neznámého	známý	k2eNgNnSc2d1	neznámé
stáří	stáří	k1gNnSc2	stáří
(	(	kIx(	(
<g/>
Řehoř	Řehoř	k1gMnSc1	Řehoř
Wolný	Wolný	k1gMnSc1	Wolný
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Mořicích	mořicí	k2eAgInPc6d1	mořicí
se	se	k3xPyFc4	se
kostel	kostel	k1gInSc1	kostel
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1384	[number]	k4	1384
a	a	k8xC	a
1437	[number]	k4	1437
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
správná	správný	k2eAgFnSc1d1	správná
<g/>
,	,	kIx,	,
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
bez	bez	k7c2	bez
stopy	stopa	k1gFnSc2	stopa
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
kaple	kaple	k1gFnSc1	kaple
vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
v	v	k7c6	v
r.	r.	kA	r.
1709	[number]	k4	1709
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgInSc1d1	pamětní
nápis	nápis	k1gInSc1	nápis
v	v	k7c6	v
kartuši	kartuš	k1gFnSc6	kartuš
nad	nad	k7c7	nad
portálem	portál	k1gInSc7	portál
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kardinál	kardinál	k1gMnSc1	kardinál
V.	V.	kA	V.
H.	H.	kA	H.
Schrattenbach	Schrattenbach	k1gMnSc1	Schrattenbach
kapli	kaple	k1gFnSc4	kaple
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1728	[number]	k4	1728
ke	k	k7c3	k
cti	čest	k1gFnSc3	čest
sv.	sv.	kA	sv.
Františka	František	k1gMnSc4	František
z	z	k7c2	z
Pauly	Paula	k1gFnSc2	Paula
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
stavby	stavba	k1gFnSc2	stavba
s	s	k7c7	s
centrální	centrální	k2eAgFnSc7d1	centrální
dispozicí	dispozice	k1gFnSc7	dispozice
je	být	k5eAaImIp3nS	být
vyzdoben	vyzdobit	k5eAaPmNgMnS	vyzdobit
freskami	freska	k1gFnPc7	freska
<g/>
,	,	kIx,	,
venkovní	venkovní	k2eAgInSc1d1	venkovní
ochoz	ochoz	k1gInSc1	ochoz
zdobí	zdobit	k5eAaImIp3nS	zdobit
sochy	socha	k1gFnPc4	socha
světců	světec	k1gMnPc2	světec
a	a	k8xC	a
andělů	anděl	k1gMnPc2	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
vranovského	vranovský	k2eAgInSc2d1	vranovský
paulánského	paulánský	k2eAgInSc2d1	paulánský
kláštera	klášter	k1gInSc2	klášter
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
mořická	mořický	k2eAgFnSc1d1	mořický
kaple	kaple	k1gFnSc1	kaple
zbořena	zbořen	k2eAgFnSc1d1	zbořena
<g/>
;	;	kIx,	;
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
obce	obec	k1gFnSc2	obec
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
a	a	k8xC	a
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
jako	jako	k9	jako
filiální	filiální	k2eAgInSc4d1	filiální
kostel	kostel	k1gInSc4	kostel
do	do	k7c2	do
sousedních	sousední	k2eAgFnPc2d1	sousední
Nezamyslic	Nezamyslice	k1gFnPc2	Nezamyslice
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
tehdy	tehdy	k6eAd1	tehdy
také	také	k6eAd1	také
získala	získat	k5eAaPmAgFnS	získat
nynější	nynější	k2eAgNnSc4d1	nynější
patrocinium	patrocinium	k1gNnSc4	patrocinium
–	–	k?	–
zasvěcení	zasvěcený	k2eAgMnPc1d1	zasvěcený
sv.	sv.	kA	sv.
Martinovi	Martinovi	k1gRnPc1	Martinovi
</s>
</p>
<p>
<s>
Smírčí	smírčí	k2eAgInSc1d1	smírčí
kříž	kříž	k1gInSc1	kříž
u	u	k7c2	u
hřbitovní	hřbitovní	k2eAgFnSc2d1	hřbitovní
zdi	zeď	k1gFnSc2	zeď
–	–	k?	–
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
baba	baba	k1gFnSc1	baba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vytesán	vytesán	k2eAgInSc4d1	vytesán
z	z	k7c2	z
hrubozrnného	hrubozrnný	k2eAgInSc2d1	hrubozrnný
pískovce	pískovec	k1gInSc2	pískovec
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
původu	původ	k1gInSc6	původ
–	–	k?	–
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
příchodu	příchod	k1gInSc2	příchod
slovanských	slovanský	k2eAgMnPc2d1	slovanský
věrozvěstů	věrozvěst	k1gMnPc2	věrozvěst
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
druhé	druhý	k4xOgNnSc1	druhý
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
smírčí	smírčí	k2eAgInSc4d1	smírčí
kříž	kříž	k1gInSc4	kříž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1430	[number]	k4	1430
<g/>
.	.	kIx.	.
<g/>
Celý	celý	k2eAgInSc1d1	celý
zámecký	zámecký	k2eAgInSc1d1	zámecký
areál	areál	k1gInSc1	areál
se	s	k7c7	s
zámeckou	zámecký	k2eAgFnSc7d1	zámecká
kaplí	kaple	k1gFnSc7	kaple
a	a	k8xC	a
sladovnou	sladovna	k1gFnSc7	sladovna
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
nejvzácnějším	vzácný	k2eAgFnPc3d3	nejvzácnější
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Územím	území	k1gNnSc7	území
obce	obec	k1gFnSc2	obec
prochází	procházet	k5eAaImIp3nS	procházet
dálnice	dálnice	k1gFnSc1	dálnice
D1	D1	k1gFnSc1	D1
s	s	k7c7	s
exitem	exit	k1gInSc7	exit
244	[number]	k4	244
<g/>
;	;	kIx,	;
silnice	silnice	k1gFnSc2	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
47	[number]	k4	47
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Vyškov	Vyškov	k1gInSc1	Vyškov
–	–	k?	–
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
a	a	k8xC	a
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
433	[number]	k4	433
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
Němčice	Němčice	k1gFnSc2	Němčice
nad	nad	k7c7	nad
Hanou	Hana	k1gFnSc7	Hana
–	–	k?	–
Mořice	Mořic	k1gMnSc2	Mořic
–	–	k?	–
Morkovice-Slížany	Morkovice-Slížana	k1gFnSc2	Morkovice-Slížana
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Nezamyslice	Nezamyslice	k1gFnPc1	Nezamyslice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mořice	Mořic	k1gMnSc2	Mořic
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Mořice	Mořic	k1gMnSc2	Mořic
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
