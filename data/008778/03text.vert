<p>
<s>
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Tulln	Tulln	k1gInSc1	Tulln
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
;	;	kIx,	;
žák	žák	k1gMnSc1	žák
Gustava	Gustav	k1gMnSc2	Gustav
Klimta	Klimt	k1gMnSc2	Klimt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
opustil	opustit	k5eAaPmAgMnS	opustit
vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
secesi	secese	k1gFnSc4	secese
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
působivý	působivý	k2eAgInSc4d1	působivý
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
svou	svůj	k3xOyFgFnSc7	svůj
náladou	nálada	k1gFnSc7	nálada
zapadá	zapadat	k5eAaImIp3nS	zapadat
do	do	k7c2	do
evropského	evropský	k2eAgInSc2d1	evropský
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
provokativních	provokativní	k2eAgInPc2d1	provokativní
aktů	akt	k1gInPc2	akt
v	v	k7c6	v
pohnutých	pohnutý	k2eAgFnPc6d1	pohnutá
pozicích	pozice	k1gFnPc6	pozice
<g/>
,	,	kIx,	,
portrétů	portrét	k1gInPc2	portrét
měst	město	k1gNnPc2	město
a	a	k8xC	a
vlastních	vlastní	k2eAgFnPc2d1	vlastní
podobizen	podobizna	k1gFnPc2	podobizna
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
pobýval	pobývat	k5eAaImAgMnS	pobývat
a	a	k8xC	a
tvořil	tvořit	k5eAaImAgInS	tvořit
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
pocházela	pocházet	k5eAaImAgFnS	pocházet
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1890	[number]	k4	1890
v	v	k7c6	v
Tullnu	Tullno	k1gNnSc6	Tullno
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
Karl	Karl	k1gMnSc1	Karl
Ludwig	Ludwig	k1gMnSc1	Ludwig
Schiele	Schiel	k1gInPc4	Schiel
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
přednosta	přednosta	k1gMnSc1	přednosta
stanice	stanice	k1gFnSc2	stanice
na	na	k7c6	na
rakouských	rakouský	k2eAgFnPc6d1	rakouská
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
Ballenstedtu	Ballenstedt	k1gInSc2	Ballenstedt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Soukupová	Soukupová	k1gFnSc1	Soukupová
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otci	otec	k1gMnSc6	otec
z	z	k7c2	z
Mírovic	Mírovice	k1gFnPc2	Mírovice
byla	být	k5eAaImAgFnS	být
napůl	napůl	k6eAd1	napůl
Češka	Češka	k1gFnSc1	Češka
a	a	k8xC	a
po	po	k7c6	po
mamince	maminka	k1gFnSc6	maminka
z	z	k7c2	z
převážně	převážně	k6eAd1	převážně
německého	německý	k2eAgInSc2d1	německý
Krumlova	Krumlov	k1gInSc2	Krumlov
napůl	napůl	k6eAd1	napůl
Němka	Němka	k1gFnSc1	Němka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
porodila	porodit	k5eAaPmAgFnS	porodit
děvče	děvče	k1gNnSc4	děvče
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
však	však	k9	však
narodili	narodit	k5eAaPmAgMnP	narodit
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Elvira	Elvira	k1gFnSc1	Elvira
<g/>
,	,	kIx,	,
narozená	narozený	k2eAgFnSc1d1	narozená
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dožila	dožít	k5eAaPmAgFnS	dožít
jen	jen	k9	jen
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Melanie	Melanie	k1gFnSc1	Melanie
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pak	pak	k6eAd1	pak
přišla	přijít	k5eAaPmAgFnS	přijít
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
na	na	k7c4	na
svět	svět	k1gInSc4	svět
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
sestra	sestra	k1gFnSc1	sestra
Gertrude	Gertrud	k1gInSc5	Gertrud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgInS	mít
zvlášť	zvlášť	k6eAd1	zvlášť
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
Adolfovi	Adolf	k1gMnSc3	Adolf
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
přednostou	přednosta	k1gMnSc7	přednosta
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
v	v	k7c6	v
Tullnu	Tullno	k1gNnSc6	Tullno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
zdědil	zdědit	k5eAaPmAgMnS	zdědit
celoživotní	celoživotní	k2eAgFnSc4d1	celoživotní
zálibu	záliba	k1gFnSc4	záliba
k	k	k7c3	k
železnici	železnice	k1gFnSc3	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
byl	být	k5eAaImAgMnS	být
železničním	železniční	k2eAgMnSc7d1	železniční
inženýrem	inženýr	k1gMnSc7	inženýr
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
Egonův	Egonův	k2eAgMnSc1d1	Egonův
pozdější	pozdní	k2eAgMnSc1d2	pozdější
poručník	poručník	k1gMnSc1	poručník
Leopold	Leopolda	k1gFnPc2	Leopolda
Czihaczek	Czihaczka	k1gFnPc2	Czihaczka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dráhy	dráha	k1gFnSc2	dráha
pracovala	pracovat	k5eAaImAgFnS	pracovat
posléze	posléze	k6eAd1	posléze
i	i	k9	i
sestra	sestra	k1gFnSc1	sestra
Melanie	Melanie	k1gFnSc1	Melanie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
plachý	plachý	k2eAgInSc1d1	plachý
a	a	k8xC	a
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
a	a	k8xC	a
raději	rád	k6eAd2	rád
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
sám	sám	k3xTgMnSc1	sám
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
vyhledával	vyhledávat	k5eAaImAgInS	vyhledávat
společnost	společnost	k1gFnSc4	společnost
druhých	druhý	k4xOgFnPc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
tiché	tichý	k2eAgNnSc4d1	tiché
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
nevyznačovalo	vyznačovat	k5eNaImAgNnS	vyznačovat
nápadnou	nápadný	k2eAgFnSc7d1	nápadná
ctižádostí	ctižádost	k1gFnSc7	ctižádost
nebo	nebo	k8xC	nebo
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
nadáním	nadání	k1gNnSc7	nadání
ani	ani	k8xC	ani
na	na	k7c6	na
reálném	reálný	k2eAgNnSc6d1	reálné
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Kremži	Kremže	k1gFnSc6	Kremže
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
třídu	třída	k1gFnSc4	třída
musel	muset	k5eAaImAgMnS	muset
dokonce	dokonce	k9	dokonce
opakovat	opakovat	k5eAaImF	opakovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
později	pozdě	k6eAd2	pozdě
kvitoval	kvitovat	k5eAaBmAgMnS	kvitovat
nepřívětivou	přívětivý	k2eNgFnSc7d1	nepřívětivá
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
"	"	kIx"	"
<g/>
neomalení	omalený	k2eNgMnPc1d1	neomalený
učitelé	učitel	k1gMnPc1	učitel
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
jeho	jeho	k3xOp3gNnSc7	jeho
"	"	kIx"	"
<g/>
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
prokázal	prokázat	k5eAaPmAgMnS	prokázat
píli	píle	k1gFnSc3	píle
a	a	k8xC	a
nadšení	nadšení	k1gNnSc3	nadšení
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
kreslení	kreslení	k1gNnSc1	kreslení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
z	z	k7c2	z
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gMnSc1	Ludwig
Karl	Karl	k1gMnSc1	Karl
Strauch	Strauch	k1gMnSc1	Strauch
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Max	Max	k1gMnSc1	Max
Kahrer	Kahrer	k1gMnSc1	Kahrer
a	a	k8xC	a
augustiniánský	augustiniánský	k2eAgMnSc1d1	augustiniánský
kanovník	kanovník	k1gMnSc1	kanovník
Wolgang	Wolgang	k1gMnSc1	Wolgang
Pauker	Pauker	k1gMnSc1	Pauker
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
s	s	k7c7	s
porozuměním	porozumění	k1gNnSc7	porozumění
podporovali	podporovat	k5eAaImAgMnP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
přes	přes	k7c4	přes
námitky	námitka	k1gFnPc4	námitka
jeho	on	k3xPp3gMnSc2	on
poručníka	poručník	k1gMnSc2	poručník
Leopolda	Leopold	k1gMnSc2	Leopold
Czihaczeka	Czihaczeek	k1gMnSc2	Czihaczeek
schválili	schválit	k5eAaPmAgMnP	schválit
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předcházející	předcházející	k2eAgInSc1d1	předcházející
rok	rok	k1gInSc1	rok
patřil	patřit	k5eAaImAgInS	patřit
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
k	k	k7c3	k
jedněm	jeden	k4xCgFnPc3	jeden
k	k	k7c3	k
nejbolestnějším	bolestný	k2eAgInSc7d3	nejbolestnější
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1905	[number]	k4	1905
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
progresivní	progresivní	k2eAgFnSc4d1	progresivní
paralýzu	paralýza	k1gFnSc4	paralýza
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
finančních	finanční	k2eAgFnPc2d1	finanční
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
měla	mít	k5eAaImAgFnS	mít
otcova	otcův	k2eAgFnSc1d1	otcova
smrt	smrt	k1gFnSc1	smrt
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
nyní	nyní	k6eAd1	nyní
bez	bez	k7c2	bez
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
osoby	osoba	k1gFnSc2	osoba
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgInS	mít
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
důvěrný	důvěrný	k2eAgInSc4d1	důvěrný
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
vřelý	vřelý	k2eAgInSc4d1	vřelý
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
ztráta	ztráta	k1gFnSc1	ztráta
otce	otec	k1gMnSc2	otec
jej	on	k3xPp3gInSc4	on
natrvalo	natrvalo	k6eAd1	natrvalo
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenost	zkušenost	k1gFnSc1	zkušenost
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rodině	rodina	k1gFnSc3	rodina
Schielových	Schielových	k2eAgFnSc4d1	Schielových
věru	věra	k1gFnSc4	věra
nebyla	být	k5eNaImAgFnS	být
cizí	cizí	k2eAgFnSc1d1	cizí
<g/>
,	,	kIx,	,
mladého	mladý	k2eAgMnSc4d1	mladý
Egona	Egon	k1gMnSc4	Egon
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
i	i	k9	i
jako	jako	k9	jako
umělce	umělec	k1gMnPc4	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
stala	stát	k5eAaPmAgFnS	stát
významným	významný	k2eAgInSc7d1	významný
námětem	námět	k1gInSc7	námět
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
otcova	otcův	k2eAgFnSc1d1	otcova
smrt	smrt	k1gFnSc1	smrt
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
<g/>
,	,	kIx,	,
coby	coby	k?	coby
jinoch	jinoch	k1gMnSc1	jinoch
<g/>
,	,	kIx,	,
cítil	cítit	k5eAaImAgMnS	cítit
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
vyvážit	vyvážit	k5eAaPmF	vyvážit
ztrátu	ztráta	k1gFnSc4	ztráta
uctívaného	uctívaný	k2eAgMnSc2d1	uctívaný
otce	otec	k1gMnSc2	otec
stupňováním	stupňování	k1gNnSc7	stupňování
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
a	a	k8xC	a
poručníka	poručník	k1gMnSc2	poručník
Leopolda	Leopold	k1gMnSc2	Leopold
Czihaczeka	Czihaczeek	k1gMnSc2	Czihaczeek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
milovníkem	milovník	k1gMnSc7	milovník
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výtvarnému	výtvarný	k2eAgNnSc3d1	výtvarné
umění	umění	k1gNnSc3	umění
vůbec	vůbec	k9	vůbec
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
<g/>
,	,	kIx,	,
prosadit	prosadit	k5eAaPmF	prosadit
přání	přání	k1gNnSc4	přání
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
Christiana	Christian	k1gMnSc4	Christian
Griepenkerla	Griepenkerl	k1gMnSc4	Griepenkerl
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíře	malíř	k1gMnSc2	malíř
historických	historický	k2eAgInPc2d1	historický
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
portrétů	portrét	k1gInPc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
u	u	k7c2	u
"	"	kIx"	"
<g/>
staromódního	staromódní	k2eAgMnSc2d1	staromódní
<g/>
"	"	kIx"	"
Griepenkerla	Griepenkerla	k1gFnSc1	Griepenkerla
jej	on	k3xPp3gMnSc4	on
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
lineárně	lineárně	k6eAd1	lineárně
plošný	plošný	k2eAgInSc1d1	plošný
styl	styl	k1gInSc1	styl
Gustava	Gustav	k1gMnSc2	Gustav
Klimta	Klimt	k1gMnSc2	Klimt
a	a	k8xC	a
secesionistů	secesionista	k1gMnPc2	secesionista
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
též	též	k9	též
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
menších	malý	k2eAgFnPc6d2	menší
pracích	práce	k1gFnPc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Schiele	Schiel	k1gInSc2	Schiel
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
Klimt	Klimt	k1gInSc4	Klimt
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uctíval	uctívat	k5eAaImAgInS	uctívat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
samostatné	samostatný	k2eAgFnSc2d1	samostatná
tvorby	tvorba	k1gFnSc2	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
opustil	opustit	k5eAaPmAgMnS	opustit
akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimochodem	mimochodem	k9	mimochodem
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
neprospíval	prospívat	k5eNaImAgMnS	prospívat
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
bývalými	bývalý	k2eAgMnPc7d1	bývalý
Griepenkerlovými	Griepenkerlův	k2eAgMnPc7d1	Griepenkerlův
žáky	žák	k1gMnPc7	žák
Antonem	Anton	k1gMnSc7	Anton
Faistauerem	Faistauer	k1gMnSc7	Faistauer
<g/>
,	,	kIx,	,
Karlem	Karel	k1gMnSc7	Karel
Zakovsekem	Zakovsek	k1gInSc7	Zakovsek
a	a	k8xC	a
jinými	jiná	k1gFnPc7	jiná
založil	založit	k5eAaPmAgMnS	založit
Skupinu	skupina	k1gFnSc4	skupina
nového	nový	k2eAgNnSc2d1	nové
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Neukunstgruppe	Neukunstgrupp	k1gMnSc5	Neukunstgrupp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
manifestu	manifest	k1gInSc6	manifest
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
opravené	opravený	k2eAgFnSc6d1	opravená
podobě	podoba	k1gFnSc6	podoba
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Die	Die	k1gFnSc1	Die
Aktion	Aktion	k1gInSc1	Aktion
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
umělce	umělec	k1gMnPc4	umělec
jako	jako	k8xS	jako
člověka	člověk	k1gMnSc4	člověk
povolaného	povolaný	k1gMnSc4	povolaný
a	a	k8xC	a
načrtl	načrtnout	k5eAaPmAgMnS	načrtnout
překvapující	překvapující	k2eAgNnSc4d1	překvapující
krédo	krédo	k1gNnSc4	krédo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nový	nový	k2eAgMnSc1d1	nový
umělec	umělec	k1gMnSc1	umělec
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
tvůrcem	tvůrce	k1gMnSc7	tvůrce
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
stavět	stavět	k5eAaImF	stavět
základ	základ	k1gInSc4	základ
bezprostředně	bezprostředně	k6eAd1	bezprostředně
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
všeho	všecek	k3xTgNnSc2	všecek
minulého	minulý	k2eAgNnSc2d1	Minulé
a	a	k8xC	a
tradičního	tradiční	k2eAgNnSc2d1	tradiční
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
novým	nový	k2eAgMnSc7d1	nový
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
přehlídka	přehlídka	k1gFnSc1	přehlídka
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Internationale	Internationale	k1gMnSc4	Internationale
Kunstschau	Kunstschaa	k1gMnSc4	Kunstschaa
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Schielemu	Schielem	k1gInSc2	Schielem
první	první	k4xOgFnSc4	první
příležitost	příležitost	k1gFnSc4	příležitost
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
významné	významný	k2eAgFnPc1d1	významná
výstavy	výstava	k1gFnPc1	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
portrétů	portrét	k1gInPc2	portrét
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kresbami	kresba	k1gFnPc7	kresba
Oskara	Oskar	k1gMnSc2	Oskar
Kokoschky	Kokoschka	k1gMnSc2	Kokoschka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
rozruch	rozruch	k1gInSc4	rozruch
už	už	k6eAd1	už
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
na	na	k7c6	na
první	první	k4xOgFnSc6	první
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
přehlídce	přehlídka	k1gFnSc6	přehlídka
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
však	však	k9	však
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
sotva	sotva	k6eAd1	sotva
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
než	než	k8xS	než
Schiele	Schiel	k1gInPc4	Schiel
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kokoschka	Kokoschka	k1gMnSc1	Kokoschka
cítil	cítit	k5eAaImAgMnS	cítit
v	v	k7c6	v
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
mladším	mladý	k2eAgInSc7d2	mladší
Schielem	Schiel	k1gInSc7	Schiel
vážného	vážný	k1gMnSc2	vážný
soka	sok	k1gMnSc2	sok
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
adresu	adresa	k1gFnSc4	adresa
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
několik	několik	k4yIc4	několik
tvrdých	tvrdý	k2eAgFnPc2d1	tvrdá
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
výstavě	výstava	k1gFnSc6	výstava
nebylo	být	k5eNaImAgNnS	být
tedy	tedy	k9	tedy
divu	diva	k1gFnSc4	diva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
sám	sám	k3xTgMnSc1	sám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopisu	dopis	k1gInSc6	dopis
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josefu	Josef	k1gMnSc3	Josef
Czermakovi	Czermak	k1gMnSc3	Czermak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzy	brzy	k6eAd1	brzy
bude	být	k5eAaImBp3nS	být
vystavovat	vystavovat	k5eAaImF	vystavovat
"	"	kIx"	"
<g/>
sám	sám	k3xTgInSc1	sám
<g/>
"	"	kIx"	"
v	v	k7c4	v
Mietkeho	Mietke	k1gMnSc4	Mietke
galerii	galerie	k1gFnSc6	galerie
<g/>
,	,	kIx,	,
a	a	k8xC	a
dopis	dopis	k1gInSc1	dopis
ukončil	ukončit	k5eAaPmAgInS	ukončit
obratem	obrat	k1gInSc7	obrat
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sám	sám	k3xTgMnSc1	sám
sebou	se	k3xPyFc7	se
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Do	do	k7c2	do
března	březen	k1gInSc2	březen
jsem	být	k5eAaImIp1nS	být
prošel	projít	k5eAaPmAgMnS	projít
Klimtem	Klimt	k1gMnSc7	Klimt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
zcela	zcela	k6eAd1	zcela
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
K	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Gustavem	Gustav	k1gMnSc7	Gustav
Klimtem	Klimt	k1gMnSc7	Klimt
došlo	dojít	k5eAaPmAgNnS	dojít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
něj	on	k3xPp3gMnSc2	on
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
prosbu	prosba	k1gFnSc4	prosba
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
nemohli	moct	k5eNaImAgMnP	moct
vyměnit	vyměnit	k5eAaPmF	vyměnit
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
naivně	naivně	k6eAd1	naivně
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
s	s	k7c7	s
radostí	radost	k1gFnSc7	radost
dal	dát	k5eAaPmAgMnS	dát
několik	několik	k4yIc4	několik
svých	svůj	k3xOyFgFnPc2	svůj
vlastních	vlastní	k2eAgFnPc2d1	vlastní
kreseb	kresba	k1gFnPc2	kresba
za	za	k7c7	za
jedinou	jediný	k2eAgFnSc7d1	jediná
Klimtovou	Klimtová	k1gFnSc7	Klimtová
<g/>
.	.	kIx.	.
</s>
<s>
Klimt	Klimt	k1gMnSc1	Klimt
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pročpak	pročpak	k6eAd1	pročpak
si	se	k3xPyFc3	se
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
chcete	chtít	k5eAaImIp2nP	chtít
vyměnit	vyměnit	k5eAaPmF	vyměnit
listy	list	k1gInPc1	list
?	?	kIx.	?
</s>
<s>
Stejně	stejně	k6eAd1	stejně
kreslíte	kreslit	k5eAaImIp2nP	kreslit
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
já	já	k3xPp1nSc1	já
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Rád	rád	k2eAgMnSc1d1	rád
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
výměnou	výměna	k1gFnSc7	výměna
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
koupil	koupit	k5eAaPmAgMnS	koupit
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jej	on	k3xPp3gNnSc2	on
značně	značně	k6eAd1	značně
potěšilo	potěšit	k5eAaPmAgNnS	potěšit
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
výměna	výměna	k1gFnSc1	výměna
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
si	se	k3xPyFc3	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Erwinem	Erwin	k1gMnSc7	Erwin
Osenem	Osen	k1gMnSc7	Osen
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
pronajali	pronajmout	k5eAaPmAgMnP	pronajmout
ateliér	ateliér	k1gInSc4	ateliér
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
napsal	napsat	k5eAaBmAgInS	napsat
svému	svůj	k3xOyFgMnSc3	svůj
pozdějšímu	pozdní	k2eAgMnSc3d2	pozdější
švagrovi	švagr	k1gMnSc3	švagr
Antonu	Anton	k1gMnSc3	Anton
Peschkovi	Peschek	k1gMnSc3	Peschek
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
útěk	útěk	k1gInSc4	útěk
<g/>
"	"	kIx"	"
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
odůvodnil	odůvodnit	k5eAaPmAgMnS	odůvodnit
jej	on	k3xPp3gMnSc4	on
svým	svůj	k3xOyFgInSc7	svůj
odporem	odpor	k1gInSc7	odpor
k	k	k7c3	k
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Peskcho	Peskcha	k1gMnSc5	Peskcha
<g/>
,	,	kIx,	,
chci	chtít	k5eAaImIp1nS	chtít
opustit	opustit	k5eAaPmF	opustit
Vídeň	Vídeň	k1gFnSc1	Vídeň
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
hnusně	hnusně	k6eAd1	hnusně
<g/>
!	!	kIx.	!
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
mi	já	k3xPp1nSc3	já
tu	tu	k6eAd1	tu
závidí	závidět	k5eAaImIp3nP	závidět
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
úskoční	úskočný	k2eAgMnPc1d1	úskočný
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgMnPc1d1	dřívější
kolegové	kolega	k1gMnPc1	kolega
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
pohlížejí	pohlížet	k5eAaImIp3nP	pohlížet
nenávistnýma	nenávistný	k2eAgNnPc7d1	nenávistné
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jsou	být	k5eAaImIp3nP	být
stíny	stín	k1gInPc1	stín
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
černé	černý	k2eAgNnSc1d1	černé
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
být	být	k5eAaImF	být
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
odjet	odjet	k5eAaPmF	odjet
na	na	k7c4	na
Šumavu	Šumava	k1gFnSc4	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
Květen	květen	k1gInSc1	květen
<g/>
,	,	kIx,	,
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
<g/>
,	,	kIx,	,
srpen	srpen	k1gInSc1	srpen
<g/>
,	,	kIx,	,
září	září	k1gNnSc4	září
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc4	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Musím	muset	k5eAaImIp1nS	muset
vidět	vidět	k5eAaImF	vidět
nové	nový	k2eAgFnPc4d1	nová
věci	věc	k1gFnPc4	věc
a	a	k8xC	a
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
ochutnat	ochutnat	k5eAaPmF	ochutnat
tmavou	tmavý	k2eAgFnSc4d1	tmavá
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
praskající	praskající	k2eAgInPc4d1	praskající
stromy	strom	k1gInPc4	strom
a	a	k8xC	a
divoké	divoký	k2eAgInPc4d1	divoký
větry	vítr	k1gInPc4	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
udiveně	udiveně	k6eAd1	udiveně
zírat	zírat	k5eAaImF	zírat
na	na	k7c4	na
rozpadající	rozpadající	k2eAgInSc4d1	rozpadající
se	se	k3xPyFc4	se
zahradní	zahradní	k2eAgInPc1d1	zahradní
ploty	plot	k1gInPc1	plot
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
slyšet	slyšet	k5eAaImF	slyšet
mladé	mladý	k2eAgInPc4d1	mladý
březové	březový	k2eAgInPc4d1	březový
porosty	porost	k1gInPc4	porost
a	a	k8xC	a
chvějící	chvějící	k2eAgInPc1d1	chvějící
se	se	k3xPyFc4	se
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
radovat	radovat	k5eAaImF	radovat
se	se	k3xPyFc4	se
z	z	k7c2	z
vlhkých	vlhký	k2eAgNnPc2d1	vlhké
zeleno-modrých	zelenoodrý	k2eAgNnPc2d1	zeleno-modré
údolí	údolí	k1gNnPc2	údolí
zvečera	zvečera	k6eAd1	zvečera
<g/>
,	,	kIx,	,
uvědomit	uvědomit	k5eAaPmF	uvědomit
si	se	k3xPyFc3	se
třpytící	třpytící	k2eAgFnPc4d1	třpytící
se	se	k3xPyFc4	se
zlaté	zlatý	k2eAgFnSc2d1	zlatá
rybky	rybka	k1gFnSc2	rybka
<g/>
,	,	kIx,	,
vidět	vidět	k5eAaImF	vidět
bílé	bílý	k2eAgInPc4d1	bílý
mraky	mrak	k1gInPc4	mrak
kupící	kupící	k2eAgInPc4d1	kupící
se	se	k3xPyFc4	se
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
chci	chtít	k5eAaImIp1nS	chtít
mluvit	mluvit	k5eAaImF	mluvit
ke	k	k7c3	k
květinám	květina	k1gFnPc3	květina
<g/>
,	,	kIx,	,
k	k	k7c3	k
trávám	tráva	k1gFnPc3	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
růžové	růžový	k2eAgMnPc4d1	růžový
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
staré	starý	k2eAgInPc4d1	starý
úctyhodné	úctyhodný	k2eAgInPc4d1	úctyhodný
kostely	kostel	k1gInPc4	kostel
<g/>
,	,	kIx,	,
vědět	vědět	k5eAaImF	vědět
co	co	k9	co
říkají	říkat	k5eAaImIp3nP	říkat
malé	malý	k2eAgInPc4d1	malý
dómy	dóm	k1gInPc4	dóm
<g/>
,	,	kIx,	,
běhat	běhat	k5eAaImF	běhat
bez	bez	k7c2	bez
zastavení	zastavení	k1gNnPc2	zastavení
po	po	k7c6	po
oblých	oblý	k2eAgInPc6d1	oblý
travnatých	travnatý	k2eAgInPc6d1	travnatý
kopcích	kopec	k1gInPc6	kopec
a	a	k8xC	a
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
širými	širý	k2eAgFnPc7d1	širá
planinami	planina	k1gFnPc7	planina
<g/>
,	,	kIx,	,
chci	chtít	k5eAaImIp1nS	chtít
políbit	políbit	k5eAaPmF	políbit
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
čichat	čichat	k5eAaImF	čichat
hebké	hebký	k2eAgFnPc4d1	hebká
teplé	teplý	k2eAgFnPc4d1	teplá
květiny	květina	k1gFnPc4	květina
v	v	k7c6	v
mechu	mech	k1gInSc6	mech
a	a	k8xC	a
pak	pak	k6eAd1	pak
vytvořím	vytvořit	k5eAaPmIp1nS	vytvořit
věci	věc	k1gFnPc4	věc
tak	tak	k6eAd1	tak
krásné	krásný	k2eAgFnPc1d1	krásná
<g/>
,	,	kIx,	,
pole	pole	k1gNnSc1	pole
barev	barva	k1gFnPc2	barva
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
Krumlově	Krumlov	k1gInSc6	Krumlov
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
především	především	k9	především
na	na	k7c4	na
akty	akt	k1gInPc4	akt
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
se	se	k3xPyFc4	se
do	do	k7c2	do
Krumlova	Krumlov	k1gInSc2	Krumlov
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
Wally	Wall	k1gMnPc7	Wall
Neuzilovou	Neuzilová	k1gFnSc7	Neuzilová
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc7d1	bývalá
Klimtovou	Klimtový	k2eAgFnSc7d1	Klimtový
modelkou	modelka	k1gFnSc7	modelka
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
pak	pak	k6eAd1	pak
strávil	strávit	k5eAaPmAgMnS	strávit
několik	několik	k4yIc4	několik
následujících	následující	k2eAgNnPc2d1	následující
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zanedlouho	zanedlouho	k6eAd1	zanedlouho
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
Krumlova	Krumlov	k1gInSc2	Krumlov
vyhnán	vyhnat	k5eAaPmNgInS	vyhnat
pro	pro	k7c4	pro
nevázaný	vázaný	k2eNgInSc4d1	nevázaný
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
s	s	k7c7	s
Wally	Wall	k1gMnPc7	Wall
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
kreslil	kreslit	k5eAaImAgMnS	kreslit
velmi	velmi	k6eAd1	velmi
mladé	mladý	k2eAgFnPc4d1	mladá
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
nadále	nadále	k6eAd1	nadále
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
alternativu	alternativa	k1gFnSc4	alternativa
k	k	k7c3	k
"	"	kIx"	"
<g/>
mrtvé	mrtvý	k2eAgFnSc3d1	mrtvá
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
se	se	k3xPyFc4	se
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
do	do	k7c2	do
Neulengbachu	Neulengbach	k1gInSc2	Neulengbach
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našel	najít	k5eAaPmAgMnS	najít
za	za	k7c4	za
rozumnou	rozumný	k2eAgFnSc4d1	rozumná
cenu	cena	k1gFnSc4	cena
dům	dům	k1gInSc4	dům
stojící	stojící	k2eAgInPc4d1	stojící
uprostřed	uprostřed	k7c2	uprostřed
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Svatost	svatost	k1gFnSc1	svatost
jeho	on	k3xPp3gNnSc2	on
erotického	erotický	k2eAgNnSc2d1	erotické
umění	umění	k1gNnSc2	umění
nemohla	moct	k5eNaImAgFnS	moct
zabránit	zabránit	k5eAaPmF	zabránit
Schieleho	Schiele	k1gMnSc4	Schiele
zatčení	zatčení	k1gNnSc2	zatčení
v	v	k7c6	v
Neulengbachu	Neulengbach	k1gInSc6	Neulengbach
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
kresby	kresba	k1gFnPc1	kresba
byly	být	k5eAaImAgFnP	být
zkonfiskovány	zkonfiskovat	k5eAaPmNgFnP	zkonfiskovat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
obžalován	obžalovat	k5eAaPmNgMnS	obžalovat
z	z	k7c2	z
únosu	únos	k1gInSc2	únos
a	a	k8xC	a
znásilnění	znásilnění	k1gNnSc2	znásilnění
nezletilé	zletilý	k2eNgFnSc2d1	nezletilá
dívky	dívka	k1gFnSc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšetřovací	vyšetřovací	k2eAgFnSc6d1	vyšetřovací
vazbě	vazba	k1gFnSc6	vazba
strávil	strávit	k5eAaPmAgMnS	strávit
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Obvinění	obvinění	k1gNnSc4	obvinění
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
při	při	k7c6	při
přelíčení	přelíčení	k1gNnSc6	přelíčení
v	v	k7c6	v
St.	st.	kA	st.
Pöltenu	Pölten	k1gInSc6	Pölten
odvolána	odvolat	k5eAaPmNgFnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
Schiele	Schiela	k1gFnSc6	Schiela
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
jen	jen	k9	jen
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
vězení	vězení	k1gNnSc2	vězení
za	za	k7c2	za
šíření	šíření	k1gNnSc2	šíření
nemorálních	morální	k2eNgFnPc2d1	nemorální
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
soudce	soudce	k1gMnPc4	soudce
pro	pro	k7c4	pro
výstrahu	výstraha	k1gFnSc4	výstraha
spálil	spálit	k5eAaPmAgMnS	spálit
nad	nad	k7c7	nad
plamenem	plamen	k1gInSc7	plamen
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
vazbě	vazba	k1gFnSc6	vazba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
i	i	k9	i
několik	několik	k4yIc4	několik
maleb	malba	k1gFnPc2	malba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
se	se	k3xPyFc4	se
Schiele	Schiel	k1gInPc4	Schiel
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
sestrami	sestra	k1gFnPc7	sestra
Harmsovými	Harmsův	k2eAgFnPc7d1	Harmsův
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
brát	brát	k5eAaImF	brát
hodiny	hodina	k1gFnPc4	hodina
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
leptu	lept	k1gInSc2	lept
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
většího	veliký	k2eAgInSc2d2	veliký
úspěchu	úspěch	k1gInSc2	úspěch
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Reininghausovy	Reininghausův	k2eAgFnPc4d1	Reininghausův
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
úspěch	úspěch	k1gInSc1	úspěch
však	však	k9	však
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
Bruselu	Brusel	k1gInSc6	Brusel
a	a	k8xC	a
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Schiela	Schiela	k1gFnSc1	Schiela
také	také	k9	také
začaly	začít	k5eAaPmAgInP	začít
zajímat	zajímat	k5eAaImF	zajímat
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
pořizuje	pořizovat	k5eAaImIp3nS	pořizovat
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgInPc4	první
originální	originální	k2eAgInPc4d1	originální
snímky	snímek	k1gInPc4	snímek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Akty	akt	k1gInPc1	akt
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nutkavosti	nutkavost	k1gFnSc3	nutkavost
odvážných	odvážný	k2eAgMnPc2d1	odvážný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
obscénních	obscénní	k2eAgFnPc2d1	obscénní
pozic	pozice	k1gFnPc2	pozice
není	být	k5eNaImIp3nS	být
divu	diva	k1gFnSc4	diva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schieleho	Schiele	k1gMnSc2	Schiele
manželka	manželka	k1gFnSc1	manželka
Edith	Edith	k1gInSc1	Edith
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
svatbě	svatba	k1gFnSc6	svatba
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gInSc7	jeho
jediným	jediný	k2eAgInSc7d1	jediný
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
a	a	k8xC	a
nechtěla	chtít	k5eNaImAgFnS	chtít
trvale	trvale	k6eAd1	trvale
uspokojovat	uspokojovat	k5eAaImF	uspokojovat
požadavky	požadavek	k1gInPc4	požadavek
svého	svůj	k1gMnSc2	svůj
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
neznal	neznat	k5eAaImAgMnS	neznat
žádná	žádný	k3yNgNnPc4	žádný
tabu	tabu	k1gNnPc4	tabu
<g/>
.	.	kIx.	.
</s>
<s>
Bezesporu	bezesporu	k9	bezesporu
s	s	k7c7	s
úlevou	úleva	k1gFnSc7	úleva
a	a	k8xC	a
rezignovaně	rezignovaně	k6eAd1	rezignovaně
přenechala	přenechat	k5eAaPmAgFnS	přenechat
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
zase	zase	k9	zase
modelům	model	k1gInPc3	model
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
století	století	k1gNnSc2	století
měly	mít	k5eAaImAgFnP	mít
modelky	modelka	k1gFnPc1	modelka
postavení	postavení	k1gNnSc2	postavení
podobné	podobný	k2eAgFnPc1d1	podobná
prostitutkám	prostitutka	k1gFnPc3	prostitutka
a	a	k8xC	a
nemohly	moct	k5eNaImAgFnP	moct
si	se	k3xPyFc3	se
dovolit	dovolit	k5eAaPmF	dovolit
přepych	přepych	k1gInSc4	přepych
sebeúcty	sebeúcta	k1gFnSc2	sebeúcta
nebo	nebo	k8xC	nebo
odmítání	odmítání	k1gNnSc1	odmítání
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
příklad	příklad	k1gInSc4	příklad
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
modelek	modelka	k1gFnPc2	modelka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Klimt	Klimt	k1gMnSc1	Klimt
nutil	nutit	k5eAaImAgMnS	nutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
stála	stát	k5eAaImAgFnS	stát
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
hlediska	hledisko	k1gNnSc2	hledisko
právě	právě	k9	právě
proto	proto	k6eAd1	proto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
pokročilém	pokročilý	k2eAgInSc6d1	pokročilý
stupni	stupeň	k1gInSc6	stupeň
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
umělecké	umělecký	k2eAgFnSc6d1	umělecká
stránce	stránka	k1gFnSc6	stránka
zacházel	zacházet	k5eAaImAgMnS	zacházet
Schiele	Schiel	k1gInPc4	Schiel
s	s	k7c7	s
ženskými	ženská	k1gFnPc7	ženská
i	i	k8xC	i
mužskými	mužský	k2eAgInPc7d1	mužský
akty	akt	k1gInPc7	akt
stejně	stejně	k6eAd1	stejně
násilně	násilně	k6eAd1	násilně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
poměr	poměr	k1gInSc1	poměr
k	k	k7c3	k
modelům	model	k1gInPc3	model
nebyl	být	k5eNaImAgMnS	být
typický	typický	k2eAgMnSc1d1	typický
pro	pro	k7c4	pro
běžné	běžný	k2eAgInPc4d1	běžný
postoje	postoj	k1gInPc4	postoj
nebo	nebo	k8xC	nebo
praxi	praxe	k1gFnSc4	praxe
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
v	v	k7c6	v
ateliérech	ateliér	k1gInPc6	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
muselo	muset	k5eAaImAgNnS	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
oficiální	oficiální	k2eAgFnSc1d1	oficiální
morálka	morálka	k1gFnSc1	morálka
byla	být	k5eAaImAgFnS	být
prudérní	prudérní	k2eAgFnSc1d1	prudérní
a	a	k8xC	a
bigotní	bigotní	k2eAgFnSc1d1	bigotní
<g/>
,	,	kIx,	,
cítila	cítit	k5eAaImAgFnS	cítit
pohoršena	pohoršit	k5eAaPmNgFnS	pohoršit
jeho	jeho	k3xOp3gFnSc1	jeho
akty	akt	k1gInPc4	akt
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
naprostou	naprostý	k2eAgFnSc4d1	naprostá
a	a	k8xC	a
agresivní	agresivní	k2eAgFnSc4d1	agresivní
nahotu	nahota	k1gFnSc4	nahota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
ničím	ničí	k3xOyNgNnSc7	ničí
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
posedlým	posedlý	k2eAgNnSc7d1	posedlé
pozorováním	pozorování	k1gNnSc7	pozorování
nahého	nahý	k2eAgNnSc2d1	nahé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Schieleho	Schieleze	k6eAd1	Schieleze
lehkomyslné	lehkomyslný	k2eAgNnSc1d1	lehkomyslné
používání	používání	k1gNnSc1	používání
mladých	mladý	k2eAgFnPc2d1	mladá
modelek	modelka	k1gFnPc2	modelka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
kreslil	kreslit	k5eAaImAgInS	kreslit
nahé	nahý	k2eAgFnPc4d1	nahá
a	a	k8xC	a
-	-	kIx~	-
z	z	k7c2	z
morálního	morální	k2eAgNnSc2d1	morální
hlediska	hledisko	k1gNnSc2	hledisko
-	-	kIx~	-
v	v	k7c6	v
pochybných	pochybný	k2eAgFnPc6d1	pochybná
pózách	póza	k1gFnPc6	póza
vedlo	vést	k5eAaImAgNnS	vést
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
k	k	k7c3	k
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgFnSc3d1	zmíněná
neulengbašské	ulengbašský	k2eNgFnSc3d1	ulengbašský
aféře	aféra	k1gFnSc3	aféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pozdější	pozdní	k2eAgNnPc4d2	pozdější
léta	léto	k1gNnPc4	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Schiele	Schiele	k6eAd1	Schiele
se	se	k3xPyFc4	se
slavnostně	slavnostně	k6eAd1	slavnostně
oženil	oženit	k5eAaPmAgMnS	oženit
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
s	s	k7c7	s
Edith	Edith	k1gInSc1	Edith
Harmsovou	Harmsová	k1gFnSc4	Harmsová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
žil	žít	k5eAaImAgMnS	žít
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
probíhající	probíhající	k2eAgFnSc3d1	probíhající
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
dostal	dostat	k5eAaPmAgMnS	dostat
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
povolávací	povolávací	k2eAgInSc1d1	povolávací
rozkaz	rozkaz	k1gInSc1	rozkaz
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vlivným	vlivný	k2eAgMnPc3d1	vlivný
přímluvcům	přímluvce	k1gMnPc3	přímluvce
však	však	k9	však
působil	působit	k5eAaImAgMnS	působit
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
administrativních	administrativní	k2eAgFnPc6d1	administrativní
pozicích	pozice	k1gFnPc6	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
zpočátku	zpočátku	k6eAd1	zpočátku
pojímal	pojímat	k5eAaImAgInS	pojímat
umění	umění	k1gNnSc4	umění
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k8xC	jako
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
projev	projev	k1gInSc4	projev
osamělého	osamělý	k2eAgNnSc2d1	osamělé
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
i	i	k9	i
za	za	k7c4	za
instituci	instituce	k1gFnSc4	instituce
a	a	k8xC	a
společensky	společensky	k6eAd1	společensky
významnou	významný	k2eAgFnSc4d1	významná
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pomoci	pomoc	k1gFnSc3	pomoc
Leopolda	Leopold	k1gMnSc2	Leopold
Lieglera	Liegler	k1gMnSc2	Liegler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
roku	rok	k1gInSc6	rok
1916	[number]	k4	1916
napsal	napsat	k5eAaPmAgMnS	napsat
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
"	"	kIx"	"
<g/>
Graphische	Graphischus	k1gMnSc5	Graphischus
Kunste	Kunst	k1gMnSc5	Kunst
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Grafická	grafický	k2eAgNnPc1d1	grafické
umění	umění	k1gNnPc1	umění
<g/>
)	)	kIx)	)
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Schiele	Schiel	k1gInPc4	Schiel
jako	jako	k8xC	jako
voják	voják	k1gMnSc1	voják
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1917	[number]	k4	1917
konečně	konečně	k6eAd1	konečně
přeložen	přeložit	k5eAaPmNgInS	přeložit
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tedy	tedy	k9	tedy
mít	mít	k5eAaImF	mít
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
události	událost	k1gFnSc6	událost
v	v	k7c6	v
uměleckém	umělecký	k2eAgInSc6d1	umělecký
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
na	na	k7c6	na
několika	několik	k4yIc6	několik
výstavách	výstava	k1gFnPc6	výstava
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
organizace	organizace	k1gFnPc4	organizace
"	"	kIx"	"
<g/>
Válečné	válečný	k2eAgFnPc4d1	válečná
výstavy	výstava	k1gFnPc4	výstava
<g/>
"	"	kIx"	"
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
první	první	k4xOgInPc4	první
veřejné	veřejný	k2eAgInPc4d1	veřejný
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Franz	Franz	k1gMnSc1	Franz
Martin	Martin	k1gMnSc1	Martin
Haberditzl	Haberditzl	k1gMnSc1	Haberditzl
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
Moderní	moderní	k2eAgFnSc2d1	moderní
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
napřed	napřed	k6eAd1	napřed
několik	několik	k4yIc4	několik
Schieleho	Schiele	k1gMnSc2	Schiele
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
dokonce	dokonce	k9	dokonce
velký	velký	k2eAgInSc1d1	velký
portrét	portrét	k1gInSc1	portrét
Edith	Editha	k1gFnPc2	Editha
Schieleové	Schieleová	k1gFnSc2	Schieleová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
koupě	koupě	k1gFnSc1	koupě
měla	mít	k5eAaImAgFnS	mít
pro	pro	k7c4	pro
Schiela	Schiela	k1gFnSc1	Schiela
takový	takový	k3xDgInSc4	takový
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
že	že	k8xS	že
neváhal	váhat	k5eNaImAgMnS	váhat
a	a	k8xC	a
na	na	k7c4	na
Haberditzlovo	Haberditzlův	k2eAgNnSc4d1	Haberditzlův
přání	přání	k1gNnSc4	přání
přemaloval	přemalovat	k5eAaPmAgMnS	přemalovat
Edithinu	Edithin	k2eAgFnSc4d1	Edithin
sukni	sukně	k1gFnSc4	sukně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
předtím	předtím	k6eAd1	předtím
pestrá	pestrý	k2eAgFnSc1d1	pestrá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
svému	svůj	k3xOyFgMnSc3	svůj
švagrovi	švagr	k1gMnSc3	švagr
Antonu	Anton	k1gMnSc3	Anton
Peschkovi	Peschek	k1gMnSc3	Peschek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
mu	on	k3xPp3gMnSc3	on
sdělil	sdělit	k5eAaPmAgInS	sdělit
svůj	svůj	k3xOyFgInSc4	svůj
plán	plán	k1gInSc4	plán
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
svazu	svaz	k1gInSc2	svaz
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
jeho	on	k3xPp3gInSc4	on
ideový	ideový	k2eAgInSc4d1	ideový
koncept	koncept	k1gInSc4	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Ambiciózní	ambiciózní	k2eAgInSc1d1	ambiciózní
projekt	projekt	k1gInSc1	projekt
zvaný	zvaný	k2eAgInSc1d1	zvaný
Kunsthalle	Kunsthalle	k1gInSc1	Kunsthalle
neměl	mít	k5eNaImAgInS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
války	válka	k1gFnSc2	válka
naději	nadát	k5eAaBmIp1nS	nadát
na	na	k7c4	na
realizaci	realizace	k1gFnSc4	realizace
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přece	přece	k9	přece
nelze	lze	k6eNd1	lze
v	v	k7c6	v
Schieleho	Schiele	k1gMnSc2	Schiele
konceptu	koncept	k1gInSc6	koncept
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
"	"	kIx"	"
<g/>
provolání	provolání	k1gNnSc4	provolání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
jakoby	jakoby	k8xS	jakoby
náboženským	náboženský	k2eAgFnPc3d1	náboženská
představám	představa	k1gFnPc3	představa
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
přidružily	přidružit	k5eAaPmAgFnP	přidružit
i	i	k9	i
zcela	zcela	k6eAd1	zcela
pragmatické	pragmatický	k2eAgInPc4d1	pragmatický
prozaické	prozaický	k2eAgInPc4d1	prozaický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Gustav	Gustav	k1gMnSc1	Gustav
Klimt	Klimt	k1gMnSc1	Klimt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
jednu	jeden	k4xCgFnSc4	jeden
generaci	generace	k1gFnSc4	generace
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
doposledka	doposledka	k6eAd1	doposledka
i	i	k9	i
Schieleho	Schiele	k1gMnSc4	Schiele
uctívaným	uctívaný	k2eAgInSc7d1	uctívaný
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Schiele	Schiele	k6eAd1	Schiele
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
stal	stát	k5eAaPmAgInS	stát
již	již	k6eAd1	již
poměrně	poměrně	k6eAd1	poměrně
velmi	velmi	k6eAd1	velmi
známou	známý	k2eAgFnSc7d1	známá
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
Klimtova	Klimtův	k2eAgMnSc4d1	Klimtův
legitimního	legitimní	k2eAgMnSc4d1	legitimní
nástupce	nástupce	k1gMnSc4	nástupce
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
49	[number]	k4	49
<g/>
.	.	kIx.	.
výstavy	výstava	k1gFnPc4	výstava
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnSc6d1	konaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
uznán	uznán	k2eAgInSc1d1	uznán
za	za	k7c4	za
hlavu	hlava	k1gFnSc4	hlava
vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Převzal	převzít	k5eAaPmAgInS	převzít
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
plakát	plakát	k1gInSc4	plakát
výstavy	výstava	k1gFnSc2	výstava
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
"	"	kIx"	"
<g/>
Společnost	společnost	k1gFnSc1	společnost
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
obrazy	obraz	k1gInPc7	obraz
a	a	k8xC	a
kresbami	kresba	k1gFnPc7	kresba
udělal	udělat	k5eAaPmAgMnS	udělat
tak	tak	k9	tak
vynikající	vynikající	k2eAgInSc4d1	vynikající
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
i	i	k9	i
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
curyšský	curyšský	k2eAgInSc1d1	curyšský
Neue	Neue	k1gInSc1	Neue
Zurcher	Zurchra	k1gFnPc2	Zurchra
Zeitung	Zeitunga	k1gFnPc2	Zeitunga
<g/>
.	.	kIx.	.
</s>
<s>
Ocitl	ocitnout	k5eAaPmAgInS	ocitnout
se	se	k3xPyFc4	se
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
očekávaného	očekávaný	k2eAgInSc2d1	očekávaný
úspěchu	úspěch	k1gInSc2	úspěch
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgMnSc1d1	vědom
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc2	postavení
jako	jako	k8xC	jako
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
na	na	k7c4	na
španělskou	španělský	k2eAgFnSc4d1	španělská
chřipku	chřipka	k1gFnSc4	chřipka
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
nakazil	nakazit	k5eAaPmAgMnS	nakazit
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
Edith	Editha	k1gFnPc2	Editha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rané	raný	k2eAgNnSc4d1	rané
dílo	dílo	k1gNnSc4	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
Schielovy	Schielův	k2eAgFnPc4d1	Schielův
práce	práce	k1gFnPc4	práce
patřily	patřit	k5eAaImAgFnP	patřit
nejvíce	nejvíce	k6eAd1	nejvíce
autoportréty	autoportrét	k1gInPc4	autoportrét
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
<g/>
,	,	kIx,	,
a	a	k8xC	a
právě	právě	k9	právě
ty	ten	k3xDgFnPc1	ten
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinu	většina	k1gFnSc4	většina
Schielovy	Schielův	k2eAgFnSc2d1	Schielův
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
autoportrétech	autoportrét	k1gInPc6	autoportrét
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1905	[number]	k4	1905
až	až	k8xS	až
1907	[number]	k4	1907
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
Schielova	Schielův	k2eAgFnSc1d1	Schielův
snaha	snaha	k1gFnSc1	snaha
používat	používat	k5eAaImF	používat
velkolepé	velkolepý	k2eAgNnSc4d1	velkolepé
exhibicionistické	exhibicionistický	k2eAgNnSc4d1	exhibicionistické
vyvyšování	vyvyšování	k1gNnSc4	vyvyšování
své	svůj	k3xOyFgFnSc2	svůj
osoby	osoba	k1gFnSc2	osoba
ke	k	k7c3	k
kompenzaci	kompenzace	k1gFnSc3	kompenzace
chybějícího	chybějící	k2eAgInSc2d1	chybějící
obdivu	obdiv	k1gInSc2	obdiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
předtím	předtím	k6eAd1	předtím
jeho	jeho	k3xOp3gInPc3	jeho
kreslířským	kreslířský	k2eAgInPc3d1	kreslířský
pokusům	pokus	k1gInPc3	pokus
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
nedávno	nedávno	k6eAd1	nedávno
zesnulý	zesnulý	k1gMnSc1	zesnulý
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
velmi	velmi	k6eAd1	velmi
miloval	milovat	k5eAaImAgMnS	milovat
a	a	k8xC	a
ctil	ctít	k5eAaImAgMnS	ctít
<g/>
.	.	kIx.	.
</s>
<s>
Schieleho	Schieleze	k6eAd1	Schieleze
uměleckému	umělecký	k2eAgNnSc3d1	umělecké
sebevědomí	sebevědomí	k1gNnSc3	sebevědomí
jistě	jistě	k6eAd1	jistě
prospělo	prospět	k5eAaPmAgNnS	prospět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
akademii	akademie	k1gFnSc4	akademie
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
teprve	teprve	k6eAd1	teprve
šestnáct	šestnáct	k4xCc1	šestnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
"	"	kIx"	"
<g/>
klimtovském	klimtovský	k2eAgNnSc6d1	klimtovský
období	období	k1gNnSc6	období
<g/>
"	"	kIx"	"
přibývá	přibývat	k5eAaImIp3nS	přibývat
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
autoportrétech	autoportrét	k1gInPc6	autoportrét
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stále	stále	k6eAd1	stále
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
autoportrétech	autoportrét	k1gInPc6	autoportrét
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konkrétní	konkrétní	k2eAgInSc1d1	konkrétní
výraz	výraz	k1gInSc1	výraz
nachází	nacházet	k5eAaImIp3nS	nacházet
Schielovo	Schielův	k2eAgNnSc4d1	Schielův
sbližování	sbližování	k1gNnSc4	sbližování
s	s	k7c7	s
Klimtem	Klimto	k1gNnSc7	Klimto
v	v	k7c6	v
obraze	obraz	k1gInSc6	obraz
Vodní	vodní	k2eAgMnPc1d1	vodní
duchové	duch	k1gMnPc1	duch
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1907	[number]	k4	1907
a	a	k8xC	a
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Převzal	převzít	k5eAaPmAgInS	převzít
z	z	k7c2	z
Klimtových	Klimtův	k2eAgMnPc2d1	Klimtův
vodních	vodní	k2eAgMnPc2d1	vodní
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
namalovaných	namalovaný	k2eAgFnPc2d1	namalovaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
motiv	motiv	k1gInSc4	motiv
žen	žena	k1gFnPc2	žena
vznášejících	vznášející	k2eAgFnPc2d1	vznášející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
poloze	poloha	k1gFnSc6	poloha
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
včlenil	včlenit	k5eAaPmAgMnS	včlenit
do	do	k7c2	do
zdobeného	zdobený	k2eAgInSc2d1	zdobený
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
Klimtově	Klimtův	k2eAgFnSc6d1	Klimtova
malbě	malba	k1gFnSc6	malba
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
portrétech	portrét	k1gInPc6	portrét
jsou	být	k5eAaImIp3nP	být
smyslné	smyslný	k2eAgFnPc1d1	smyslná
linie	linie	k1gFnPc1	linie
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
malované	malovaný	k2eAgFnPc4d1	malovaná
dokonale	dokonale	k6eAd1	dokonale
naturalistickým	naturalistický	k2eAgInSc7d1	naturalistický
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
zahaleny	zahalen	k2eAgInPc4d1	zahalen
abstraktními	abstraktní	k2eAgFnPc7d1	abstraktní
dekorativními	dekorativní	k2eAgFnPc7d1	dekorativní
barevnými	barevný	k2eAgFnPc7d1	barevná
plochami	plocha	k1gFnPc7	plocha
a	a	k8xC	a
tvary	tvar	k1gInPc7	tvar
<g/>
,	,	kIx,	,
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
Schieleho	Schieleha	k1gFnSc5	Schieleha
postavy	postava	k1gFnPc4	postava
samy	sám	k3xTgFnPc1	sám
součástí	součást	k1gFnSc7	součást
ornamentální	ornamentální	k2eAgFnSc1d1	ornamentální
abstrakce	abstrakce	k1gFnSc1	abstrakce
kompoziční	kompoziční	k2eAgFnSc2d1	kompoziční
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Klimtově	Klimtův	k2eAgFnSc6d1	Klimtova
kresbě	kresba	k1gFnSc6	kresba
těl	tělo	k1gNnPc2	tělo
linie	linie	k1gFnSc2	linie
ještě	ještě	k6eAd1	ještě
definuje	definovat	k5eAaBmIp3nS	definovat
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
esteticky	esteticky	k6eAd1	esteticky
stylizovány	stylizován	k2eAgInPc1d1	stylizován
do	do	k7c2	do
melodických	melodický	k2eAgInPc2d1	melodický
<g/>
,	,	kIx,	,
harmonických	harmonický	k2eAgInPc2d1	harmonický
autonomních	autonomní	k2eAgInPc2d1	autonomní
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Schieleho	Schiele	k1gMnSc2	Schiele
linie	linie	k1gFnSc2	linie
působí	působit	k5eAaImIp3nP	působit
nezávisle	závisle	k6eNd1	závisle
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc4	prostředek
interpretace	interpretace	k1gFnSc2	interpretace
,	,	kIx,	,
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
není	být	k5eNaImIp3nS	být
tělesná	tělesný	k2eAgFnSc1d1	tělesná
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
hranatost	hranatost	k1gFnSc1	hranatost
má	mít	k5eAaImIp3nS	mít
emoční	emoční	k2eAgFnPc4d1	emoční
<g/>
,	,	kIx,	,
expresivní	expresivní	k2eAgFnPc4d1	expresivní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schiele	Schiele	k6eAd1	Schiele
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
asi	asi	k9	asi
přes	přes	k7c4	přes
30	[number]	k4	30
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
se	se	k3xPyFc4	se
Schiele	Schiela	k1gFnSc3	Schiela
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
odpoutával	odpoutávat	k5eAaImAgMnS	odpoutávat
od	od	k7c2	od
Klimtovy	Klimtův	k2eAgFnSc2d1	Klimtova
elegantní	elegantní	k2eAgFnSc2d1	elegantní
linie	linie	k1gFnSc2	linie
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kreslířský	kreslířský	k2eAgInSc4d1	kreslířský
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
dokonce	dokonce	k9	dokonce
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
velký	velký	k2eAgInSc1d1	velký
vzor	vzor	k1gInSc1	vzor
Klimt	Klimt	k2eAgInSc1d1	Klimt
ochotně	ochotně	k6eAd1	ochotně
uznal	uznat	k5eAaPmAgInS	uznat
za	za	k7c4	za
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klimtovy	Klimtův	k2eAgFnPc1d1	Klimtova
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
určitou	určitý	k2eAgFnSc4d1	určitá
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
pózu	póza	k1gFnSc4	póza
nebo	nebo	k8xC	nebo
polohu	poloha	k1gFnSc4	poloha
<g/>
,	,	kIx,	,
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
víceméně	víceméně	k9	víceméně
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
obrys	obrys	k1gInSc1	obrys
<g/>
,	,	kIx,	,
měkká	měkký	k2eAgFnSc1d1	měkká
<g/>
,	,	kIx,	,
plynoucí	plynoucí	k2eAgFnSc1d1	plynoucí
kontura	kontura	k1gFnSc1	kontura
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
tělo	tělo	k1gNnSc4	tělo
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
a	a	k8xC	a
budí	budit	k5eAaImIp3nS	budit
jen	jen	k9	jen
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
dojem	dojem	k1gInSc4	dojem
prchavosti	prchavost	k1gFnSc2	prchavost
<g/>
.	.	kIx.	.
</s>
<s>
Schieleho	Schieleze	k6eAd1	Schieleze
kreslířská	kreslířský	k2eAgFnSc1d1	kreslířská
linie	linie	k1gFnSc1	linie
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
zdá	zdát	k5eAaPmIp3nS	zdát
křehká	křehký	k2eAgFnSc1d1	křehká
a	a	k8xC	a
stažená	stažený	k2eAgFnSc1d1	stažená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
útržkovitá	útržkovitý	k2eAgFnSc1d1	útržkovitá
<g/>
,	,	kIx,	,
málokdy	málokdy	k6eAd1	málokdy
rovná	rovnat	k5eAaImIp3nS	rovnat
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přerušovaná	přerušovaný	k2eAgFnSc1d1	přerušovaná
<g/>
,	,	kIx,	,
slábne	slábnout	k5eAaImIp3nS	slábnout
či	či	k8xC	či
sílí	sílet	k5eAaImIp3nS	sílet
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
důrazu	důraz	k1gInSc6	důraz
kladeném	kladený	k2eAgInSc6d1	kladený
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
detaily	detail	k1gInPc4	detail
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
však	však	k9	však
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
jistá	jistý	k2eAgFnSc1d1	jistá
a	a	k8xC	a
zvládnutá	zvládnutý	k2eAgFnSc1d1	zvládnutá
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
velmi	velmi	k6eAd1	velmi
skeptičtí	skeptický	k2eAgMnPc1d1	skeptický
kritici	kritik	k1gMnPc1	kritik
museli	muset	k5eAaImAgMnP	muset
uznat	uznat	k5eAaPmF	uznat
Schieleho	Schiele	k1gMnSc2	Schiele
genialitu	genialita	k1gFnSc4	genialita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
je	být	k5eAaImIp3nS	být
nápadné	nápadný	k2eAgNnSc1d1	nápadné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schiele	Schiel	k1gInPc1	Schiel
opouští	opouštět	k5eAaImIp3nP	opouštět
normální	normální	k2eAgNnSc4d1	normální
použití	použití	k1gNnSc4	použití
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
prostorově	prostorově	k6eAd1	prostorově
odůvodnila	odůvodnit	k5eAaPmAgFnS	odůvodnit
pozice	pozice	k1gFnPc4	pozice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jeho	jeho	k3xOp3gFnPc1	jeho
postavy	postava	k1gFnPc1	postava
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obraz	obraz	k1gInSc4	obraz
Autoportrét	autoportrét	k1gInSc4	autoportrét
se	s	k7c7	s
zdviženým	zdvižený	k2eAgInSc7d1	zdvižený
pravým	pravý	k2eAgInSc7d1	pravý
loktem	loket	k1gInSc7	loket
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naprosto	naprosto	k6eAd1	naprosto
antiakademicky	antiakademicky	k6eAd1	antiakademicky
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
subjektivně	subjektivně	k6eAd1	subjektivně
"	"	kIx"	"
<g/>
vynalézá	vynalézat	k5eAaImIp3nS	vynalézat
<g/>
"	"	kIx"	"
úhly	úhel	k1gInPc1	úhel
pohledy	pohled	k1gInPc1	pohled
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
se	se	k3xPyFc4	se
postavy	postav	k1gInPc7	postav
zdají	zdát	k5eAaPmIp3nP	zdát
zkřivené	zkřivený	k2eAgInPc1d1	zkřivený
<g/>
,	,	kIx,	,
zkroucené	zkroucený	k2eAgInPc1d1	zkroucený
a	a	k8xC	a
deformované	deformovaný	k2eAgInPc1d1	deformovaný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nově	nově	k6eAd1	nově
objevená	objevený	k2eAgFnSc1d1	objevená
technika	technika	k1gFnSc1	technika
vynáší	vynášet	k5eAaImIp3nS	vynášet
Schiela	Schiela	k1gFnSc1	Schiela
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
malířského	malířský	k2eAgNnSc2d1	malířské
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
obrazy	obraz	k1gInPc4	obraz
Sedící	sedící	k2eAgFnSc1d1	sedící
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
Černovlasá	černovlasý	k2eAgFnSc1d1	černovlasá
dívka	dívka	k1gFnSc1	dívka
nebo	nebo	k8xC	nebo
Opovržení	opovržení	k1gNnSc1	opovržení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pozdní	pozdní	k2eAgNnSc4d1	pozdní
dílo	dílo	k1gNnSc4	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
Schiele	Schiel	k1gInSc2	Schiel
poměrně	poměrně	k6eAd1	poměrně
opustil	opustit	k5eAaPmAgMnS	opustit
akty	akt	k1gInPc4	akt
a	a	k8xC	a
začínal	začínat	k5eAaImAgMnS	začínat
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
spíše	spíše	k9	spíše
"	"	kIx"	"
<g/>
klidnějšími	klidný	k2eAgInPc7d2	klidnější
<g/>
"	"	kIx"	"
náměty	námět	k1gInPc7	námět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
přírody	příroda	k1gFnSc2	příroda
nebo	nebo	k8xC	nebo
krajiny	krajina	k1gFnSc2	krajina
se	se	k3xPyFc4	se
Schiele	Schiela	k1gFnSc3	Schiela
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
celkovým	celkový	k2eAgInSc7d1	celkový
uměleckým	umělecký	k2eAgInSc7d1	umělecký
vývojem	vývoj	k1gInSc7	vývoj
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
po	po	k7c6	po
prvních	první	k4xOgInPc6	první
pokusech	pokus	k1gInPc6	pokus
od	od	k7c2	od
popisného	popisný	k2eAgNnSc2d1	popisné
zobrazování	zobrazování	k1gNnSc2	zobrazování
a	a	k8xC	a
dospěl	dochvít	k5eAaPmAgMnS	dochvít
oklikou	oklika	k1gFnSc7	oklika
přes	přes	k7c4	přes
klimtovskou	klimtovský	k2eAgFnSc4d1	klimtovský
ornamentalizaci	ornamentalizace	k1gFnSc4	ornamentalizace
předmětů	předmět	k1gInPc2	předmět
ke	k	k7c3	k
způsobu	způsob	k1gInSc3	způsob
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
věci	věc	k1gFnPc1	věc
získávají	získávat	k5eAaImIp3nP	získávat
ve	v	k7c6	v
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc3d2	veliký
míře	míra	k1gFnSc3	míra
antropomorfní	antropomorfní	k2eAgFnSc2d1	antropomorfní
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
sběrateli	sběratel	k1gMnPc7	sběratel
Franzu	Franza	k1gFnSc4	Franza
Hauerovi	Hauerův	k2eAgMnPc1d1	Hauerův
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
popsal	popsat	k5eAaPmAgInS	popsat
Schiele	Schiel	k1gInSc2	Schiel
svoji	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
ukončený	ukončený	k2eAgInSc4d1	ukončený
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
na	na	k7c4	na
kreslené	kreslený	k2eAgInPc4d1	kreslený
návrhy	návrh	k1gInPc4	návrh
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dělám	dělat	k5eAaImIp1nS	dělat
také	také	k9	také
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
a	a	k8xC	a
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
obkreslování	obkreslování	k1gNnSc1	obkreslování
podle	podle	k7c2	podle
přírody	příroda	k1gFnSc2	příroda
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
nemá	mít	k5eNaImIp3nS	mít
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lépe	dobře	k6eAd2	dobře
maluji	malovat	k5eAaImIp1nS	malovat
obrazy	obraz	k1gInPc4	obraz
podle	podle	k7c2	podle
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vizi	vize	k1gFnSc4	vize
krajiny	krajina	k1gFnSc2	krajina
-	-	kIx~	-
hlavně	hlavně	k9	hlavně
teď	teď	k6eAd1	teď
pozoruji	pozorovat	k5eAaImIp1nS	pozorovat
hmotný	hmotný	k2eAgInSc4d1	hmotný
pohyb	pohyb	k1gInSc4	pohyb
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
to	ten	k3xDgNnSc1	ten
připomíná	připomínat	k5eAaImIp3nS	připomínat
podobné	podobný	k2eAgInPc1d1	podobný
pohyby	pohyb	k1gInPc1	pohyb
lidských	lidský	k2eAgNnPc2d1	lidské
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgInPc1d1	podobný
pocity	pocit	k1gInPc1	pocit
radosti	radost	k1gFnSc2	radost
a	a	k8xC	a
bolu	bol	k1gInSc2	bol
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Malování	malování	k1gNnSc1	malování
samotné	samotný	k2eAgFnSc2d1	samotná
mi	já	k3xPp1nSc3	já
nestačí	stačit	k5eNaBmIp3nP	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
barvami	barva	k1gFnPc7	barva
dají	dát	k5eAaPmIp3nP	dát
vytvořit	vytvořit	k5eAaPmF	vytvořit
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
-	-	kIx~	-
Intenzivně	intenzivně	k6eAd1	intenzivně
<g/>
,	,	kIx,	,
celou	celý	k2eAgFnSc7d1	celá
bytostí	bytost	k1gFnSc7	bytost
a	a	k8xC	a
srdcem	srdce	k1gNnSc7	srdce
vnímáme	vnímat	k5eAaImIp1nP	vnímat
podzimní	podzimní	k2eAgInSc4d1	podzimní
strom	strom	k1gInSc4	strom
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
bych	by	kYmCp1nS	by
namalovat	namalovat	k5eAaPmF	namalovat
tuto	tento	k3xDgFnSc4	tento
zádumčivost	zádumčivost	k1gFnSc4	zádumčivost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
již	již	k6eAd1	již
u	u	k7c2	u
Slunečnice	slunečnice	k1gFnSc2	slunečnice
II	II	kA	II
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
postavy	postava	k1gFnPc4	postava
s	s	k7c7	s
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc1	období
není	být	k5eNaImIp3nS	být
již	již	k6eAd1	již
ani	ani	k8xC	ani
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
křehká	křehký	k2eAgFnSc1d1	křehká
rostlina	rostlina	k1gFnSc1	rostlina
obalena	obalit	k5eAaPmNgFnS	obalit
dekorativním	dekorativní	k2eAgInSc7d1	dekorativní
zámotkem	zámotek	k1gInSc7	zámotek
ornamentních	ornamentní	k2eAgFnPc2d1	ornamentní
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
působí	působit	k5eAaImIp3nS	působit
izolovaně	izolovaně	k6eAd1	izolovaně
<g/>
,	,	kIx,	,
nechráněně	chráněně	k6eNd1	chráněně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
desetiletí	desetiletí	k1gNnSc2	desetiletí
vznikají	vznikat	k5eAaImIp3nP	vznikat
obrazy	obraz	k1gInPc4	obraz
měst	město	k1gNnPc2	město
a	a	k8xC	a
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
jsou	být	k5eAaImIp3nP	být
symbolická	symbolický	k2eAgFnSc1d1	symbolická
tíha	tíha	k1gFnSc1	tíha
a	a	k8xC	a
vizionářská	vizionářský	k2eAgFnSc1d1	vizionářská
hloubka	hloubka	k1gFnSc1	hloubka
potlačeny	potlačen	k2eAgInPc1d1	potlačen
jinými	jiný	k2eAgFnPc7d1	jiná
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Rozpadající	rozpadající	k2eAgInSc4d1	rozpadající
se	se	k3xPyFc4	se
mlýn	mlýn	k1gInSc4	mlýn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Schiele	Schiel	k1gInSc2	Schiel
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
v	v	k7c6	v
Muhlingu	Muhling	k1gInSc6	Muhling
<g/>
,	,	kIx,	,
projevil	projevit	k5eAaPmAgInS	projevit
zesílený	zesílený	k2eAgInSc1d1	zesílený
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
věčnou	věčný	k2eAgFnSc4d1	věčná
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neponechává	ponechávat	k5eNaImIp3nS	ponechávat
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
čistou	čistý	k2eAgFnSc4d1	čistá
iluzi	iluze	k1gFnSc4	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
obrazy	obraz	k1gInPc4	obraz
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Předměstský	předměstský	k2eAgInSc1d1	předměstský
dům	dům	k1gInSc1	dům
s	s	k7c7	s
prádlem	prádlo	k1gNnSc7	prádlo
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Průčelí	průčelí	k1gNnSc1	průčelí
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krumlovská	krumlovský	k2eAgFnSc1d1	Krumlovská
krajina	krajina	k1gFnSc1	krajina
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
město	město	k1gNnSc1	město
VI	VI	kA	VI
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Stein	Stein	k1gMnSc1	Stein
nad	nad	k7c7	nad
Dunajem	Dunaj	k1gInSc7	Dunaj
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc4d1	velký
formát	formát	k1gInSc4	formát
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc1	přehled
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
aktu	akt	k1gInSc2	akt
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
lepence	lepenka	k1gFnSc6	lepenka
<g/>
,	,	kIx,	,
24,4	[number]	k4	24,4
×	×	k?	×
18	[number]	k4	18
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
s	s	k7c7	s
rukou	ruka	k1gFnSc7	ruka
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
uhel	uhel	k1gInSc1	uhel
<g/>
,	,	kIx,	,
44,3	[number]	k4	44,3
×	×	k?	×
30,5	[number]	k4	30,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
uhel	uhel	k1gInSc1	uhel
44,3	[number]	k4	44,3
×	×	k?	×
31	[number]	k4	31
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
<g/>
,	,	kIx,	,
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
zvýrazněním	zvýraznění	k1gNnSc7	zvýraznění
<g/>
,	,	kIx,	,
55,8	[number]	k4	55,8
×	×	k?	×
36,9	[number]	k4	36,9
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
s	s	k7c7	s
obnaženým	obnažený	k2eAgNnSc7d1	obnažené
břichem	břicho	k1gNnSc7	břicho
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
55,2	[number]	k4	55,2
×	×	k?	×
36,4	[number]	k4	36,4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
se	s	k7c7	s
zdviženým	zdvižený	k2eAgInSc7d1	zdvižený
pravým	pravý	k2eAgInSc7d1	pravý
loktem	loket	k1gInSc7	loket
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
křída	křída	k1gFnSc1	křída
<g/>
,	,	kIx,	,
47,6	[number]	k4	47,6
×	×	k?	×
31,1	[number]	k4	31,1
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedící	sedící	k2eAgInSc1d1	sedící
mužský	mužský	k2eAgInSc1d1	mužský
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
a	a	k8xC	a
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
152,5	[number]	k4	152,5
×	×	k?	×
150	[number]	k4	150
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Masturbace	masturbace	k1gFnSc1	masturbace
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48	[number]	k4	48
×	×	k?	×
32,1	[number]	k4	32,1
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eros	Eros	k1gMnSc1	Eros
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
křída	křída	k1gFnSc1	křída
<g/>
,	,	kIx,	,
55,9	[number]	k4	55,9
×	×	k?	×
45,7	[number]	k4	45,7
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
ve	v	k7c6	v
fialové	fialový	k2eAgFnSc6d1	fialová
košili	košile	k1gFnSc6	košile
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48,4	[number]	k4	48,4
×	×	k?	×
32,2	[number]	k4	32,2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autoportrét	autoportrét	k1gInSc1	autoportrét
ze	z	k7c2	z
zdviženými	zdvižený	k2eAgFnPc7d1	zdvižená
pažemi	paže	k1gFnPc7	paže
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gFnSc1	kvaš
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48,4	[number]	k4	48,4
×	×	k?	×
32,1	[number]	k4	32,1
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prorok	prorok	k1gMnSc1	prorok
(	(	kIx(	(
<g/>
dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
autoportrét	autoportrét	k1gInSc1	autoportrét
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
110,3	[number]	k4	110,3
×	×	k?	×
50,3	[number]	k4	50,3
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
,	,	kIx,	,
Staatsgalerie	Staatsgalerie	k1gFnSc1	Staatsgalerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Klosterneuburgu	Klosterneuburg	k1gInSc6	Klosterneuburg
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
96,6	[number]	k4	96,6
×	×	k?	×
53,5	[number]	k4	53,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Oberosterreichisches	Oberosterreichisches	k1gInSc1	Oberosterreichisches
Landesmuseum	Landesmuseum	k1gInSc1	Landesmuseum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plachetnice	plachetnice	k1gFnSc1	plachetnice
na	na	k7c6	na
vlnící	vlnící	k2eAgFnSc6d1	vlnící
se	se	k3xPyFc4	se
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
na	na	k7c6	na
lepence	lepenka	k1gFnSc6	lepenka
<g/>
,	,	kIx,	,
25	[number]	k4	25
×	×	k?	×
18	[number]	k4	18
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Neue	Neue	k1gFnSc1	Neue
Galerie	galerie	k1gFnSc2	galerie
am	am	k?	am
Landesmuseum	Landesmuseum	k1gInSc1	Landesmuseum
Joanneum	Joanneum	k1gInSc1	Joanneum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vodní	vodní	k2eAgMnPc1d1	vodní
duchové	duch	k1gMnPc1	duch
I	I	kA	I
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gFnSc1	kvaš
<g/>
,	,	kIx,	,
barevná	barevný	k2eAgFnSc1d1	barevná
křída	křída	k1gFnSc1	křída
<g/>
,	,	kIx,	,
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
27,5	[number]	k4	27,5
×	×	k?	×
53,5	[number]	k4	53,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukřižování	ukřižování	k1gNnSc1	ukřižování
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
42	[number]	k4	42
×	×	k?	×
42	[number]	k4	42
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Portrét	portrét	k1gInSc1	portrét
Gerti	Gerť	k1gFnSc2	Gerť
Schielové	Schielová	k1gFnSc2	Schielová
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
140	[number]	k4	140
×	×	k?	×
140	[number]	k4	140
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
The	The	k1gFnPc1	The
museum	museum	k1gNnSc1	museum
of	of	k?	of
Modern	Modern	k1gMnSc1	Modern
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
odkaz	odkaz	k1gInSc1	odkaz
a	a	k8xC	a
částečný	částečný	k2eAgInSc1d1	částečný
dar	dar	k1gInSc1	dar
Ronalda	Ronald	k1gMnSc2	Ronald
S.	S.	kA	S.
Laudera	Lauder	k1gMnSc2	Lauder
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
svatozáří	svatozář	k1gFnSc7	svatozář
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
;	;	kIx,	;
vymývaná	vymývaný	k2eAgFnSc1d1	vymývaná
tušová	tušový	k2eAgFnSc1d1	tušová
kresba	kresba	k1gFnSc1	kresba
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
15,4	[number]	k4	15,4
×	×	k?	×
9,9	[number]	k4	9,9
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opovržení	opovržení	k1gNnSc1	opovržení
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
uhel	uhel	k1gInSc1	uhel
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
zvýrazněním	zvýraznění	k1gNnSc7	zvýraznění
<g/>
,	,	kIx,	,
45	[number]	k4	45
×	×	k?	×
31,4	[number]	k4	31,4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc1	tři
uličníci	uličník	k1gMnPc1	uličník
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
44,6	[number]	k4	44,6
×	×	k?	×
30,8	[number]	k4	30,8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
Kosmack	Kosmack	k1gMnSc1	Kosmack
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
100	[number]	k4	100
×	×	k?	×
100	[number]	k4	100
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Osterreichische	Osterreichische	k1gFnSc1	Osterreichische
Galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
kloboukem	klobouk	k1gInSc7	klobouk
(	(	kIx(	(
<g/>
Erwin	Erwin	k1gMnSc1	Erwin
Dominik	Dominik	k1gMnSc1	Dominik
Osen	osen	k1gInSc1	osen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
;	;	kIx,	;
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
uhel	uhel	k1gInSc1	uhel
<g/>
,	,	kIx,	,
45	[number]	k4	45
×	×	k?	×
31	[number]	k4	31
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedící	sedící	k2eAgFnSc1d1	sedící
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48	[number]	k4	48
×	×	k?	×
31,5	[number]	k4	31,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Haag	Haag	k1gInSc1	Haag
<g/>
,	,	kIx,	,
Haags	Haags	k1gInSc1	Haags
Gemeentenmuseum	Gemeentenmuseum	k1gInSc1	Gemeentenmuseum
voor	voor	k1gInSc1	voor
Moderne	Modern	k1gInSc5	Modern
Kunst	Kunst	k1gFnSc4	Kunst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černovlasá	černovlasý	k2eAgFnSc1d1	černovlasá
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
45	[number]	k4	45
×	×	k?	×
31,6	[number]	k4	31,6
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Oberlin	Oberlin	k1gInSc1	Oberlin
(	(	kIx(	(
<g/>
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Allen	Allen	k1gMnSc1	Allen
Memorial	Memorial	k1gMnSc1	Memorial
Art	Art	k1gMnSc1	Art
Museum	museum	k1gNnSc4	museum
<g/>
,	,	kIx,	,
Oberlin	Oberlin	k2eAgInSc1d1	Oberlin
College	College	k1gInSc1	College
<g/>
,	,	kIx,	,
Friends	Friends	k1gInSc1	Friends
of	of	k?	of
Art	Art	k1gFnSc1	Art
Fund	fund	k1gInSc1	fund
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgNnPc4	dva
malá	malý	k2eAgNnPc4d1	malé
děvčátka	děvčátko	k1gNnPc4	děvčátko
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
40	[number]	k4	40
×	×	k?	×
30,6	[number]	k4	30,6
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Umělcova	umělcův	k2eAgFnSc1d1	umělcova
místnost	místnost	k1gFnSc1	místnost
v	v	k7c6	v
Neulenbachu	Neulenbach	k1gInSc6	Neulenbach
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Můj	můj	k3xOp1gInSc4	můj
obývací	obývací	k2eAgInSc4d1	obývací
pokoj	pokoj	k1gInSc4	pokoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
40	[number]	k4	40
×	×	k?	×
31,7	[number]	k4	31,7
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Historisches	Historisches	k1gInSc1	Historisches
Museum	museum	k1gNnSc1	museum
der	drát	k5eAaImRp2nS	drát
Stadt	Stadt	k1gMnSc1	Stadt
Wien	Wien	k1gMnSc1	Wien
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podzimní	podzimní	k2eAgInPc1d1	podzimní
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
79,5	[number]	k4	79,5
×	×	k?	×
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wally	Wall	k1gInPc1	Wall
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gFnSc1	kvaš
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
29,7	[number]	k4	29,7
×	×	k?	×
26	[number]	k4	26
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Lengenfeld	Lengenfeld	k1gInSc1	Lengenfeld
<g/>
,	,	kIx,	,
Christa	Christa	k1gMnSc1	Christa
Hauer-Frumann	Hauer-Frumann	k1gMnSc1	Hauer-Frumann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
a	a	k8xC	a
Otto	Otto	k1gMnSc1	Otto
Beneschovi	Beneschův	k2eAgMnPc1d1	Beneschův
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
portrét	portrét	k1gInSc1	portrét
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
121	[number]	k4	121
×	×	k?	×
131	[number]	k4	131
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Neue	Neue	k1gFnSc1	Neue
Galerie	galerie	k1gFnSc1	galerie
der	drát	k5eAaImRp2nS	drát
Stadt	Stadt	k1gMnSc1	Stadt
Linz	Linz	k1gMnSc1	Linz
<g/>
,	,	kIx,	,
Wolgang-Gurlitt-Museum	Wolgang-Gurlitt-Museum	k1gInSc1	Wolgang-Gurlitt-Museum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zápasník	zápasník	k1gMnSc1	zápasník
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gFnSc1	kvaš
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48,8	[number]	k4	48,8
×	×	k?	×
32,2	[number]	k4	32,2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedící	sedící	k2eAgInSc1d1	sedící
dívčí	dívčí	k2eAgInSc1d1	dívčí
akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gFnSc1	kvaš
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48,3	[number]	k4	48,3
×	×	k?	×
32	[number]	k4	32
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
a	a	k8xC	a
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gFnSc1	kvaš
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48,1	[number]	k4	48,1
×	×	k?	×
31,9	[number]	k4	31,9
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stojící	stojící	k2eAgInSc1d1	stojící
mužský	mužský	k2eAgInSc1d1	mužský
akt	akt	k1gInSc1	akt
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
bederní	bederní	k2eAgFnSc7d1	bederní
zástěrou	zástěra	k1gFnSc7	zástěra
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48	[number]	k4	48
×	×	k?	×
32	[number]	k4	32
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Graphische	Graphische	k1gFnSc1	Graphische
Sammlung	Sammlunga	k1gFnPc2	Sammlunga
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvojitý	dvojitý	k2eAgInSc1d1	dvojitý
autoportrét	autoportrét	k1gInSc1	autoportrét
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
32,5	[number]	k4	32,5
×	×	k?	×
49,4	[number]	k4	49,4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc4	dva
objímající	objímající	k2eAgFnPc4d1	objímající
se	se	k3xPyFc4	se
dívky	dívka	k1gFnSc2	dívka
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
dívčí	dívčí	k2eAgInSc1d1	dívčí
pár	pár	k4xCyI	pár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gInSc1	kvaš
<g/>
,	,	kIx,	,
akvarel	akvarel	k1gInSc1	akvarel
a	a	k8xC	a
tužka	tužka	k1gFnSc1	tužka
<g/>
,	,	kIx,	,
48	[number]	k4	48
×	×	k?	×
32,7	[number]	k4	32,7
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Szépmuvészeti	Szépmuvészeti	k1gFnSc1	Szépmuvészeti
Múzeum	Múzeum	k1gInSc1	Múzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Edith	Edith	k1gMnSc1	Edith
Schielová	Schielová	k1gFnSc1	Schielová
v	v	k7c6	v
pruhovaných	pruhovaný	k2eAgInPc6d1	pruhovaný
šatech	šat	k1gInPc6	šat
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
180	[number]	k4	180
×	×	k?	×
110	[number]	k4	110
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Haag	Haag	k1gInSc1	Haag
<g/>
,	,	kIx,	,
Haags	Haags	k1gInSc1	Haags
Gemeentemuseum	Gemeentemuseum	k1gInSc1	Gemeentemuseum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
1915	[number]	k4	1915
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
150	[number]	k4	150
×	×	k?	×
180	[number]	k4	180
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Osterreichische	Osterreichische	k1gFnSc1	Osterreichische
Galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krumlovská	krumlovský	k2eAgFnSc1d1	Krumlovská
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
110	[number]	k4	110
×	×	k?	×
140,5	[number]	k4	140,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Neue	Neue	k1gFnSc1	Neue
Galerie	galerie	k1gFnSc1	galerie
der	drát	k5eAaImRp2nS	drát
Stadt	Stadt	k1gMnSc1	Stadt
Linz	Linz	k1gMnSc1	Linz
<g/>
,	,	kIx,	,
Wolgang-Gurlitt-Museum	Wolgang-Gurlitt-Museum	k1gInSc1	Wolgang-Gurlitt-Museum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedící	sedící	k2eAgFnSc1d1	sedící
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
pokrčenou	pokrčený	k2eAgFnSc7d1	pokrčená
levou	levý	k2eAgFnSc7d1	levá
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
;	;	kIx,	;
kvaš	kvaš	k1gFnSc1	kvaš
a	a	k8xC	a
černá	černý	k2eAgFnSc1d1	černá
křída	křída	k1gFnSc1	křída
<g/>
,	,	kIx,	,
46	[number]	k4	46
×	×	k?	×
30,5	[number]	k4	30,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objetí	objetí	k1gNnSc1	objetí
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
100	[number]	k4	100
×	×	k?	×
170,2	[number]	k4	170,2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Osterreichiste	Osterreichist	k1gMnSc5	Osterreichist
Galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
110,5	[number]	k4	110,5
×	×	k?	×
141	[number]	k4	141
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Osterreichische	Osterreichische	k1gFnSc1	Osterreichische
Galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předměstský	předměstský	k2eAgInSc1d1	předměstský
dům	dům	k1gInSc1	dům
s	s	k7c7	s
prádlem	prádlo	k1gNnSc7	prádlo
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
110	[number]	k4	110
×	×	k?	×
140,4	[number]	k4	140,4
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc4d1	soukromý
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
<g/>
;	;	kIx,	;
olej	olej	k1gInSc1	olej
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
152,5	[number]	k4	152,5
×	×	k?	×
162,5	[number]	k4	162,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Osterreichische	Osterreichische	k1gFnSc1	Osterreichische
Galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Steiner	Steiner	k1gMnSc1	Steiner
<g/>
,	,	kIx,	,
Reinhard	Reinhard	k1gMnSc1	Reinhard
<g/>
:	:	kIx,	:
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
ISBN	ISBN	kA	ISBN
3-8228-2573-5	[number]	k4	3-8228-2573-5
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
ve	v	k7c4	v
WikicitátechInformační	WikicitátechInformační	k2eAgInSc4d1	WikicitátechInformační
systém	systém	k1gInSc4	systém
abART	abART	k?	abART
<g/>
:	:	kIx,	:
Schiele	Schiel	k1gInSc2	Schiel
Egon	Egon	k1gMnSc1	Egon
</s>
</p>
<p>
<s>
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc4	dokument
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
<g/>
:	:	kIx,	:
Těhotná	těhotný	k2eAgFnSc1d1	těhotná
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
</s>
</p>
<p>
<s>
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
Art	Art	k1gMnSc1	Art
Centrum	centrum	k1gNnSc4	centrum
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
</s>
</p>
