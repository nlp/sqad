<s>
František	František	k1gMnSc1	František
Čížek	Čížek	k1gMnSc1	Čížek
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1850	[number]	k4	1850
Křelina	Křelina	k1gFnSc1	Křelina
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1889	[number]	k4	1889
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
veřejný	veřejný	k2eAgMnSc1d1	veřejný
činitel	činitel	k1gMnSc1	činitel
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
sokolském	sokolský	k2eAgNnSc6d1	sokolské
hnutí	hnutí	k1gNnSc6	hnutí
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
cvičitelského	cvičitelský	k2eAgInSc2d1	cvičitelský
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
1878	[number]	k4	1878
náměstek	náměstka	k1gFnPc2	náměstka
náčelníka	náčelník	k1gMnSc2	náčelník
Sokola	Sokol	k1gMnSc2	Sokol
pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
tamtéž	tamtéž	k6eAd1	tamtéž
místostarosta	místostarosta	k1gMnSc1	místostarosta
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
náčelník	náčelník	k1gInSc1	náčelník
České	český	k2eAgFnSc2d1	Česká
obce	obec	k1gFnSc2	obec
sokolské	sokolská	k1gFnSc2	sokolská
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
Sokola	Sokol	k1gMnSc2	Sokol
pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
výboru	výbor	k1gInSc2	výbor
Národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
severočeské	severočeský	k2eAgFnSc2d1	Severočeská
a	a	k8xC	a
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
Klubu	klub	k1gInSc2	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Současníci	současník	k1gMnPc1	současník
jej	on	k3xPp3gMnSc4	on
oceňovali	oceňovat	k5eAaImAgMnP	oceňovat
za	za	k7c4	za
obětavou	obětavý	k2eAgFnSc4d1	obětavá
organizátorskou	organizátorský	k2eAgFnSc4d1	organizátorská
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
Sokole	Sokol	k1gInSc6	Sokol
a	a	k8xC	a
nezištnou	zištný	k2eNgFnSc4d1	nezištná
pomoc	pomoc	k1gFnSc4	pomoc
příslušníkům	příslušník	k1gMnPc3	příslušník
české	český	k2eAgFnSc2d1	Česká
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1850	[number]	k4	1850
v	v	k7c6	v
Křelině	Křelina	k1gFnSc6	Křelina
u	u	k7c2	u
Jičína	Jičín	k1gInSc2	Jičín
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
věnovali	věnovat	k5eAaPmAgMnP	věnovat
všechny	všechen	k3xTgInPc4	všechen
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
jičínské	jičínský	k2eAgNnSc4d1	Jičínské
gymnázium	gymnázium	k1gNnSc4	gymnázium
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xS	jako
student	student	k1gMnSc1	student
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Sokola	Sokol	k1gMnSc2	Sokol
a	a	k8xC	a
do	do	k7c2	do
Akademického	akademický	k2eAgInSc2d1	akademický
čtenářského	čtenářský	k2eAgInSc2d1	čtenářský
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studie	k1gFnPc2	studie
rovněž	rovněž	k9	rovněž
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
jako	jako	k8xS	jako
jednoroční	jednoroční	k2eAgMnSc1d1	jednoroční
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
tam	tam	k6eAd1	tam
řadu	řada	k1gFnSc4	řada
cenných	cenný	k2eAgFnPc2d1	cenná
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
poručíka	poručík	k1gMnSc2	poručík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Sokole	Sokol	k1gInSc6	Sokol
přijat	přijmout	k5eAaPmNgMnS	přijmout
do	do	k7c2	do
cvičitelského	cvičitelský	k2eAgInSc2d1	cvičitelský
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
důležitým	důležitý	k2eAgInSc7d1	důležitý
úkolem	úkol	k1gInSc7	úkol
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
a	a	k8xC	a
zdokonalit	zdokonalit	k5eAaPmF	zdokonalit
soustavu	soustava	k1gFnSc4	soustava
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
šermu	šerm	k1gInSc2	šerm
u	u	k7c2	u
náčelníka	náčelník	k1gMnSc2	náčelník
Tyrše	Tyrš	k1gMnSc2	Tyrš
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
náměstka	náměstek	k1gMnSc2	náměstek
Josefa	Josef	k1gMnSc2	Josef
Müllera	Müller	k1gMnSc2	Müller
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Sokol	Sokol	k1gInSc1	Sokol
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
kvůli	kvůli	k7c3	kvůli
Tyršově	Tyršův	k2eAgFnSc3d1	Tyršova
chronické	chronický	k2eAgFnSc3d1	chronická
nemoci	nemoc	k1gFnSc3	nemoc
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
časté	častý	k2eAgFnSc2d1	častá
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
pobytům	pobyt	k1gInPc3	pobyt
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
byl	být	k5eAaImAgMnS	být
Čížek	Čížek	k1gMnSc1	Čížek
zvolen	zvolit	k5eAaPmNgMnS	zvolit
náměstkem	náměstek	k1gMnSc7	náměstek
náčelníka	náčelník	k1gMnSc2	náčelník
a	a	k8xC	a
po	po	k7c6	po
Tyršově	Tyršův	k2eAgFnSc6d1	Tyršova
smrti	smrt	k1gFnSc6	smrt
převzal	převzít	k5eAaPmAgMnS	převzít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Projevoval	projevovat	k5eAaImAgMnS	projevovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
rázný	rázný	k2eAgMnSc1d1	rázný
a	a	k8xC	a
zkušený	zkušený	k2eAgMnSc1d1	zkušený
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
železnou	železný	k2eAgFnSc4d1	železná
kázeň	kázeň	k1gFnSc4	kázeň
a	a	k8xC	a
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
,	,	kIx,	,
přísně	přísně	k6eAd1	přísně
vytýkal	vytýkat	k5eAaImAgMnS	vytýkat
nedostatky	nedostatek	k1gInPc4	nedostatek
a	a	k8xC	a
nedbalost	nedbalost	k1gFnSc4	nedbalost
<g/>
,	,	kIx,	,
na	na	k7c4	na
stížnosti	stížnost	k1gFnPc4	stížnost
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
zadáním	zadání	k1gNnSc7	zadání
úkolu	úkol	k1gInSc2	úkol
stěžovateli	stěžovatel	k1gMnPc7	stěžovatel
<g/>
.	.	kIx.	.
</s>
<s>
Napomenutí	napomenutí	k1gNnSc4	napomenutí
občas	občas	k6eAd1	občas
doplnil	doplnit	k5eAaPmAgMnS	doplnit
"	"	kIx"	"
<g/>
úderem	úder	k1gInSc7	úder
na	na	k7c4	na
záloktí	záloktí	k1gNnSc4	záloktí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
Sokole	Sokol	k1gInSc6	Sokol
tak	tak	k8xC	tak
známým	známý	k2eAgNnSc7d1	známé
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
složena	složen	k2eAgFnSc1d1	složena
humorná	humorný	k2eAgFnSc1d1	humorná
báseň	báseň	k1gFnSc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
většiny	většina	k1gFnSc2	většina
výletů	výlet	k1gInPc2	výlet
a	a	k8xC	a
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
získalo	získat	k5eAaPmAgNnS	získat
velkou	velký	k2eAgFnSc4d1	velká
oblibu	obliba	k1gFnSc4	obliba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
získal	získat	k5eAaPmAgInS	získat
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
právního	právní	k2eAgInSc2d1	právní
výboru	výbor	k1gInSc2	výbor
Sokola	Sokol	k1gMnSc4	Sokol
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
otevřel	otevřít	k5eAaPmAgMnS	otevřít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
advokátní	advokátní	k2eAgFnSc4d1	advokátní
kancelář	kancelář	k1gFnSc4	kancelář
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
místostarostou	místostarosta	k1gMnSc7	místostarosta
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
výboru	výbor	k1gInSc2	výbor
druhých	druhý	k4xOgFnPc2	druhý
sokolských	sokolský	k2eAgFnPc2d1	Sokolská
slavností	slavnost	k1gFnPc2	slavnost
(	(	kIx(	(
<g/>
nakonec	nakonec	k6eAd1	nakonec
nepovolených	povolený	k2eNgMnPc2d1	nepovolený
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
dalších	další	k2eAgFnPc6d1	další
veřejných	veřejný	k2eAgFnPc6d1	veřejná
aktivitách	aktivita	k1gFnPc6	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
starostou	starosta	k1gMnSc7	starosta
Akademického	akademický	k2eAgInSc2d1	akademický
čtenářského	čtenářský	k2eAgInSc2d1	čtenářský
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
členem	člen	k1gInSc7	člen
výboru	výbor	k1gInSc2	výbor
nově	nově	k6eAd1	nově
zřízené	zřízený	k2eAgFnPc1d1	zřízená
Národní	národní	k2eAgFnPc1d1	národní
jednoty	jednota	k1gFnPc1	jednota
severočeské	severočeský	k2eAgFnPc1d1	Severočeská
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
náměstkem	náměstek	k1gMnSc7	náměstek
starosty	starosta	k1gMnSc2	starosta
(	(	kIx(	(
<g/>
A.	A.	kA	A.
P.	P.	kA	P.
Trojana	Trojan	k1gMnSc2	Trojan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
během	během	k7c2	během
jehož	jehož	k3xOyRp3gFnSc2	jehož
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
řídil	řídit	k5eAaImAgMnS	řídit
schůze	schůze	k1gFnSc1	schůze
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
při	při	k7c6	při
zakládání	zakládání	k1gNnSc6	zakládání
spolkového	spolkový	k2eAgInSc2d1	spolkový
časopisu	časopis	k1gInSc2	časopis
Česká	český	k2eAgFnSc1d1	Česká
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
vděčnost	vděčnost	k1gFnSc4	vděčnost
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
příslušníků	příslušník	k1gMnPc2	příslušník
české	český	k2eAgFnSc2d1	Česká
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
často	často	k6eAd1	často
právně	právně	k6eAd1	právně
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
bezplatně	bezplatně	k6eAd1	bezplatně
<g/>
.	.	kIx.	.
</s>
<s>
Spoluzakládal	spoluzakládat	k5eAaImAgInS	spoluzakládat
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
i	i	k8xC	i
dalších	další	k2eAgNnPc2d1	další
sdružení	sdružení	k1gNnPc2	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
sportovně	sportovně	k6eAd1	sportovně
založený	založený	k2eAgInSc1d1	založený
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
statnou	statný	k2eAgFnSc4d1	statná
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
projevoval	projevovat	k5eAaImAgMnS	projevovat
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
životní	životní	k2eAgFnSc4d1	životní
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
svěžest	svěžest	k1gFnSc4	svěžest
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
aktivitám	aktivita	k1gFnPc3	aktivita
patřil	patřit	k5eAaImAgInS	patřit
šerm	šerm	k1gInSc1	šerm
a	a	k8xC	a
jezdectví	jezdectví	k1gNnSc1	jezdectví
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
projížděl	projíždět	k5eAaImAgMnS	projíždět
na	na	k7c4	na
koni	kůň	k1gMnPc7	kůň
okolí	okolí	k1gNnSc2	okolí
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Slibnou	slibný	k2eAgFnSc4d1	slibná
budoucnost	budoucnost	k1gFnSc4	budoucnost
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
předčasně	předčasně	k6eAd1	předčasně
ukončil	ukončit	k5eAaPmAgInS	ukončit
nádor	nádor	k1gInSc1	nádor
na	na	k7c6	na
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1888	[number]	k4	1888
mu	on	k3xPp3gMnSc3	on
už	už	k6eAd1	už
nemoc	nemoc	k1gFnSc1	nemoc
působila	působit	k5eAaImAgFnS	působit
závratě	závrať	k1gFnPc4	závrať
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
rychlejším	rychlý	k2eAgInSc6d2	rychlejší
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
radu	rada	k1gFnSc4	rada
lékaře	lékař	k1gMnSc2	lékař
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
do	do	k7c2	do
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
a	a	k8xC	a
Kysiblu	Kysibl	k1gInSc2	Kysibl
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
odtud	odtud	k6eAd1	odtud
ještě	ještě	k9	ještě
v	v	k7c6	v
horším	zlý	k2eAgInSc6d2	horší
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
upoutaný	upoutaný	k2eAgMnSc1d1	upoutaný
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Nepolevoval	polevovat	k5eNaImAgMnS	polevovat
ale	ale	k9	ale
v	v	k7c6	v
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Gabriela	Gabriel	k1gMnSc2	Gabriel
Žižky	Žižka	k1gMnSc2	Žižka
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
němuž	jenž	k3xRgInSc3	jenž
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
zvolen	zvolit	k5eAaPmNgMnS	zvolit
starostou	starosta	k1gMnSc7	starosta
Sokola	Sokol	k1gMnSc2	Sokol
pražského	pražský	k2eAgMnSc2d1	pražský
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
prvním	první	k4xOgInSc7	první
náčelníkem	náčelník	k1gInSc7	náčelník
České	český	k2eAgFnSc2d1	Česká
obce	obec	k1gFnSc2	obec
sokolské	sokolská	k1gFnSc2	sokolská
<g/>
.	.	kIx.	.
</s>
<s>
Nechával	nechávat	k5eAaImAgMnS	nechávat
se	se	k3xPyFc4	se
dopravovat	dopravovat	k5eAaImF	dopravovat
na	na	k7c4	na
schůze	schůze	k1gFnPc4	schůze
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
organizoval	organizovat	k5eAaBmAgMnS	organizovat
setkání	setkání	k1gNnSc4	setkání
cvičitelů	cvičitel	k1gMnPc2	cvičitel
<g/>
,	,	kIx,	,
z	z	k7c2	z
lůžka	lůžko	k1gNnSc2	lůžko
organizoval	organizovat	k5eAaBmAgInS	organizovat
spolkovou	spolkový	k2eAgFnSc4d1	spolková
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mrtvici	mrtvice	k1gFnSc4	mrtvice
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
Olšanech	Olšany	k1gInPc6	Olšany
<g/>
.	.	kIx.	.
</s>
<s>
Čížek	Čížek	k1gMnSc1	Čížek
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
oceňovaný	oceňovaný	k2eAgMnSc1d1	oceňovaný
jako	jako	k8xC	jako
nezištný	zištný	k2eNgMnSc1d1	nezištný
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
rázný	rázný	k2eAgMnSc1d1	rázný
organizátor	organizátor	k1gMnSc1	organizátor
a	a	k8xC	a
pevný	pevný	k2eAgInSc1d1	pevný
charakter	charakter	k1gInSc1	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
advokátní	advokátní	k2eAgFnSc1d1	advokátní
kancelář	kancelář	k1gFnSc1	kancelář
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
dobré	dobrý	k2eAgFnPc4d1	dobrá
pověsti	pověst	k1gFnPc4	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
čestným	čestný	k2eAgMnSc7d1	čestný
členem	člen	k1gMnSc7	člen
sokolské	sokolský	k2eAgFnSc2d1	Sokolská
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
Jičíně	Jičín	k1gInSc6	Jičín
a	a	k8xC	a
Duchcově	Duchcův	k2eAgMnSc6d1	Duchcův
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
jako	jako	k9	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
právní	právní	k2eAgNnSc4d1	právní
zastupování	zastupování	k1gNnSc4	zastupování
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc2	on
pohřbu	pohřeb	k1gInSc2	pohřeb
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
zástupci	zástupce	k1gMnPc1	zástupce
šedesáti	šedesát	k4xCc2	šedesát
sokolských	sokolský	k2eAgFnPc2d1	Sokolská
jednot	jednota	k1gFnPc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
smutečním	smuteční	k2eAgNnSc6d1	smuteční
shromáždění	shromáždění	k1gNnSc6	shromáždění
připomněl	připomnět	k5eAaPmAgMnS	připomnět
jeho	jeho	k3xOp3gFnPc4	jeho
zásluhy	zásluha	k1gFnPc4	zásluha
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
první	první	k4xOgMnSc1	první
místopředseda	místopředseda	k1gMnSc1	místopředseda
pražského	pražský	k2eAgMnSc2d1	pražský
Sokola	Sokol	k1gMnSc2	Sokol
Josef	Josef	k1gMnSc1	Josef
Víšek	Víšek	k1gMnSc1	Víšek
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
České	český	k2eAgFnSc2d1	Česká
obce	obec	k1gFnSc2	obec
sokolské	sokolská	k1gFnSc2	sokolská
Jan	Jan	k1gMnSc1	Jan
Podlipný	Podlipný	k1gMnSc1	Podlipný
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
Národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
severočeské	severočeský	k2eAgFnSc2d1	Severočeská
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
.	.	kIx.	.
</s>
