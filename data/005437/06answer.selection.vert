<s>
Tudor	tudor	k1gInSc1	tudor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1485	[number]	k4	1485
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ukončil	ukončit	k5eAaPmAgInS	ukončit
válku	válka	k1gFnSc4	válka
růží	růž	k1gFnPc2	růž
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rozhodný	rozhodný	k2eAgMnSc1d1	rozhodný
a	a	k8xC	a
schopný	schopný	k2eAgMnSc1d1	schopný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
centralizoval	centralizovat	k5eAaBmAgMnS	centralizovat
moc	moc	k6eAd1	moc
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
