<s>
Diffieho	Diffieze	k6eAd1
<g/>
–	–	k?
<g/>
Hellmanův	Hellmanův	k2eAgInSc4d1
protokol	protokol	k1gInSc4
s	s	k7c7
využitím	využití	k1gNnSc7
eliptických	eliptický	k2eAgFnPc2d1
křivek	křivka	k1gFnPc2
</s>
<s>
Animovaný	animovaný	k2eAgInSc1d1
princip	princip	k1gInSc1
ECDH	ECDH	kA
</s>
<s>
Diffieho-Hellmanův	Diffie-Hellmanův	k2eAgInSc1d1
protokol	protokol	k1gInSc1
s	s	k7c7
využitím	využití	k1gNnSc7
eliptických	eliptický	k2eAgFnPc2d1
křivek	křivka	k1gFnPc2
(	(	kIx(
<g/>
ECDH	ECDH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
varianta	varianta	k1gFnSc1
Diffieho-Hellmanova	Diffie-Hellmanův	k2eAgInSc2d1
protokolu	protokol	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
využívá	využívat	k5eAaImIp3nS
eliptických	eliptický	k2eAgFnPc2d1
křivek	křivka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
šifrovací	šifrovací	k2eAgInSc4d1
protokol	protokol	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
umožňuje	umožňovat	k5eAaImIp3nS
dvěma	dva	k4xCgFnPc3
stranám	strana	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
v	v	k7c6
životě	život	k1gInSc6
nesetkaly	setkat	k5eNaPmAgFnP
ani	ani	k8xC
spolu	spolu	k6eAd1
nekomunikovaly	komunikovat	k5eNaImAgFnP
<g/>
,	,	kIx,
sdílet	sdílet	k5eAaImF
„	„	k?
<g/>
tajné	tajný	k2eAgFnSc2d1
<g/>
“	“	k?
informace	informace	k1gFnSc2
na	na	k7c6
nechráněném	chráněný	k2eNgInSc6d1
komunikačním	komunikační	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
na	na	k7c6
sdělovacím	sdělovací	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
<g/>
,	,	kIx,
přes	přes	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
se	se	k3xPyFc4
posílají	posílat	k5eAaImIp3nP
informace	informace	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
telefon	telefon	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
informace	informace	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přímo	přímo	k6eAd1
použity	použít	k5eAaPmNgFnP
jako	jako	k8xS,k8xC
klíč	klíč	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
ještě	ještě	k9
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
jiného	jiný	k2eAgInSc2d1
klíče	klíč	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
použit	použít	k5eAaPmNgInS
k	k	k7c3
zašifrování	zašifrování	k1gNnSc3
další	další	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
s	s	k7c7
použitím	použití	k1gNnSc7
symetrické	symetrický	k2eAgFnSc2d1
šifry	šifra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vytváření	vytváření	k1gNnSc1
klíče	klíč	k1gInSc2
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgMnSc1
účastník	účastník	k1gMnSc1
-	-	kIx~
Alice	Alice	k1gFnSc1
-	-	kIx~
chce	chtít	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
sdílený	sdílený	k2eAgInSc4d1
klíč	klíč	k1gInSc4
s	s	k7c7
druhým	druhý	k4xOgMnSc7
účastníkem	účastník	k1gMnSc7
-	-	kIx~
Bobem	bob	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
jediném	jediný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
spolu	spolu	k6eAd1
mohou	moct	k5eAaImIp3nP
komunikovat	komunikovat	k5eAaImF
<g/>
,	,	kIx,
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nP
špehové	špeh	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předně	předně	k6eAd1
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
dohodnout	dohodnout	k5eAaPmF
na	na	k7c6
parametrech	parametr	k1gInPc6
(	(	kIx(
<g/>
p	p	k?
<g/>
,	,	kIx,
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
<g/>
,	,	kIx,
G	G	kA
<g/>
,	,	kIx,
n	n	k0
<g/>
,	,	kIx,
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
p	p	k?
je	být	k5eAaImIp3nS
prvočíslo	prvočíslo	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgInPc3,k3yQgInPc3,k3yRgInPc3
definujeme	definovat	k5eAaBmIp1nP
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
konstanty	konstanta	k1gFnPc4
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
z	z	k7c2
rovnice	rovnice	k1gFnSc2
eliptické	eliptický	k2eAgFnSc2d1
křivky	křivka	k1gFnSc2
<g/>
,	,	kIx,
bod	bod	k1gInSc4
G	G	kA
na	na	k7c6
eliptické	eliptický	k2eAgFnSc6d1
křivce	křivka	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
řád	řád	k1gInSc1
n	n	k0
a	a	k8xC
kofaktor	kofaktor	k1gInSc1
h	h	k?
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
udává	udávat	k5eAaImIp3nS
podíl	podíl	k1gInSc4
počtu	počet	k1gInSc2
prvků	prvek	k1gInPc2
grupy	grupa	k1gFnSc2
bodů	bod	k1gInPc2
na	na	k7c6
eliptické	eliptický	k2eAgFnSc6d1
křivce	křivka	k1gFnSc6
a	a	k8xC
řádu	řád	k1gInSc6
bodu	bod	k1gInSc2
G.	G.	kA
</s>
<s>
Nyní	nyní	k6eAd1
si	se	k3xPyFc3
každá	každý	k3xTgFnSc1
strana	strana	k1gFnSc1
musí	muset	k5eAaImIp3nS
zvolit	zvolit	k5eAaPmF
své	svůj	k3xOyFgInPc4
klíče	klíč	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
ze	z	k7c2
soukromého	soukromý	k2eAgInSc2d1
klíče	klíč	k1gInSc2
d	d	k?
(	(	kIx(
<g/>
náhodně	náhodně	k6eAd1
vybrané	vybraný	k2eAgNnSc1d1
celé	celý	k2eAgNnSc1d1
kladné	kladný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
z	z	k7c2
intervalu	interval	k1gInSc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
<g/>
n-	n-	k?
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
veřejného	veřejný	k2eAgInSc2d1
klíče	klíč	k1gInSc2
Q	Q	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
Q	Q	kA
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
G	G	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q	Q	kA
<g/>
=	=	kIx~
<g/>
dG	dg	kA
<g/>
}	}	kIx)
</s>
<s>
Alice	Alice	k1gFnSc1
tedy	tedy	k9
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
dvojici	dvojice	k1gFnSc4
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
,	,	kIx,
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}}	}}	k?
</s>
<s>
Bob	Bob	k1gMnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
,	,	kIx,
</s>
<s>
Q	Q	kA
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}}	}}	k?
</s>
<s>
Aby	aby	kYmCp3nP
tento	tento	k3xDgInSc4
protokol	protokol	k1gInSc4
fungoval	fungovat	k5eAaImAgInS
<g/>
,	,	kIx,
strany	strana	k1gFnPc1
si	se	k3xPyFc3
musí	muset	k5eAaImIp3nP
vyměnit	vyměnit	k5eAaPmF
veřejné	veřejný	k2eAgInPc1d1
klíče	klíč	k1gInPc1
(	(	kIx(
<g/>
Alice	Alice	k1gFnSc1
pošle	poslat	k5eAaPmIp3nS
Bobovi	Bob	k1gMnSc3
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
Bob	Bob	k1gMnSc1
pošle	poslat	k5eAaPmIp3nS
Alici	Alice	k1gFnSc4
</s>
<s>
Q	Q	kA
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}}	}}	k?
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nyní	nyní	k6eAd1
může	moct	k5eAaImIp3nS
Alice	Alice	k1gFnSc1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
bod	bod	k1gInSc4
Z	z	k7c2
<g/>
,	,	kIx,
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
Q	Q	kA
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
Bob	Bob	k1gMnSc1
může	moct	k5eAaImIp3nS
nalézt	nalézt	k5eAaPmF,k5eAaBmF
bod	bod	k1gInSc4
Z	z	k7c2
<g/>
'	'	kIx"
<g/>
,	,	kIx,
</s>
<s>
Z	z	k7c2
</s>
<s>
′	′	k?
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
'	'	kIx"
<g/>
=	=	kIx~
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}}	}}	k?
</s>
<s>
Tyto	tento	k3xDgInPc4
body	bod	k1gInPc4
Z	Z	kA
<g/>
,	,	kIx,
Z	Z	kA
<g/>
'	'	kIx"
jsou	být	k5eAaImIp3nP
totožné	totožný	k2eAgFnPc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
</s>
<s>
Z	z	k7c2
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
Q	Q	kA
</s>
<s>
B	B	kA
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
(	(	kIx(
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
G	G	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
G	G	kA
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
(	(	kIx(
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
G	G	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
</s>
<s>
=	=	kIx~
</s>
<s>
Z	z	k7c2
</s>
<s>
′	′	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
G	G	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
G	G	kA
<g/>
=	=	kIx~
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
G	G	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
Z	z	k7c2
<g/>
'	'	kIx"
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Příklad	příklad	k1gInSc1
</s>
<s>
Alice	Alice	k1gFnSc1
a	a	k8xC
Bob	Bob	k1gMnSc1
zvolí	zvolit	k5eAaPmIp3nS
prvočíslo	prvočíslo	k1gNnSc4
</s>
<s>
p	p	k?
</s>
<s>
=	=	kIx~
</s>
<s>
23	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
p	p	k?
<g/>
=	=	kIx~
<g/>
23	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
bod	bod	k1gInSc1
G	G	kA
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
;	;	kIx,
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
koeficienty	koeficient	k1gInPc1
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
4	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
3	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
27	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
2	#num#	k4
</s>
<s>
≠	≠	k?
</s>
<s>
0	#num#	k4
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
4	#num#	k4
<g/>
a	a	k8xC
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
27	#num#	k4
<g/>
b	b	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
neq	neq	k?
0	#num#	k4
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
eliptickou	eliptický	k2eAgFnSc4d1
křivku	křivka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Alice	Alice	k1gFnSc1
zvolí	zvolit	k5eAaPmIp3nS
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
spočítá	spočítat	k5eAaPmIp3nS
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
G	G	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
G	G	kA
<g/>
}	}	kIx)
</s>
<s>
:	:	kIx,
</s>
<s>
s	s	k7c7
</s>
<s>
≡	≡	k?
</s>
<s>
3	#num#	k4
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
y	y	k?
</s>
<s>
mod	mod	k?
</s>
<s>
p	p	k?
</s>
<s>
≡	≡	k?
</s>
<s>
3	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
13	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
16	#num#	k4
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
≡	≡	k?
</s>
<s>
13	#num#	k4
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
s	s	k7c7
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
y	y	k?
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
mod	mod	k?
p	p	k?
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
3	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
13	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
16	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
13	#num#	k4
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
x	x	k?
</s>
<s>
A	a	k9
</s>
<s>
≡	≡	k?
</s>
<s>
s	s	k7c7
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
2	#num#	k4
</s>
<s>
x	x	k?
</s>
<s>
mod	mod	k?
</s>
<s>
p	p	k?
</s>
<s>
≡	≡	k?
</s>
<s>
13	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
13	#num#	k4
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
≡	≡	k?
</s>
<s>
5	#num#	k4
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x_	x_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
s	s	k7c7
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
x	x	k?
<g/>
\	\	kIx~
<g/>
mod	mod	k?
p	p	k?
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
13	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
13	#num#	k4
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
5	#num#	k4
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
y	y	k?
</s>
<s>
A	a	k9
</s>
<s>
≡	≡	k?
</s>
<s>
s	s	k7c7
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
A	a	k9
</s>
<s>
)	)	kIx)
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
mod	mod	k?
</s>
<s>
p	p	k?
</s>
<s>
≡	≡	k?
</s>
<s>
13	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
13	#num#	k4
</s>
<s>
−	−	k?
</s>
<s>
5	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
−	−	k?
</s>
<s>
16	#num#	k4
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
≡	≡	k?
</s>
<s>
19	#num#	k4
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y_	y_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
s	s	k7c7
<g/>
(	(	kIx(
<g/>
x-x_	x-x_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
-y	-y	k?
<g/>
\	\	kIx~
<g/>
mod	mod	k?
p	p	k?
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
13	#num#	k4
<g/>
(	(	kIx(
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
19	#num#	k4
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Alice	Alice	k1gFnSc1
získává	získávat	k5eAaImIp3nS
souřadnice	souřadnice	k1gFnPc4
bodu	bod	k1gInSc2
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
</s>
<s>
[	[	kIx(
</s>
<s>
5	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
19	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
;	;	kIx,
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
tento	tento	k3xDgInSc1
bod	bod	k1gInSc1
posílá	posílat	k5eAaImIp3nS
Bobovi	Bob	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Bob	Bob	k1gMnSc1
zvolí	zvolit	k5eAaPmIp3nS
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
4	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
spočítá	spočítat	k5eAaPmIp3nS
</s>
<s>
Q	Q	kA
</s>
<s>
B	B	kA
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
2	#num#	k4
</s>
<s>
G	G	kA
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
2	#num#	k4
<g/>
G	G	kA
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
R	R	kA
<g/>
}	}	kIx)
</s>
<s>
:	:	kIx,
</s>
<s>
Bob	Bob	k1gMnSc1
analogicky	analogicky	k6eAd1
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS
bod	bod	k1gInSc1
G	G	kA
<g/>
,	,	kIx,
získá	získat	k5eAaPmIp3nS
pomocný	pomocný	k2eAgInSc1d1
bod	bod	k1gInSc1
R	R	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
opět	opět	k6eAd1
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Bob	Bob	k1gMnSc1
získává	získávat	k5eAaImIp3nS
souřadnice	souřadnice	k1gFnPc4
bodu	bod	k1gInSc2
</s>
<s>
Q	Q	kA
</s>
<s>
B	B	kA
</s>
<s>
[	[	kIx(
</s>
<s>
17	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
3	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
;	;	kIx,
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
tento	tento	k3xDgInSc1
bod	bod	k1gInSc1
posílá	posílat	k5eAaImIp3nS
Alici	Alice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Alice	Alice	k1gFnSc1
nyní	nyní	k6eAd1
může	moct	k5eAaImIp3nS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
bod	bod	k1gInSc4
Z	z	k7c2
<g/>
:	:	kIx,
</s>
<s>
Z	z	k7c2
</s>
<s>
≡	≡	k?
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
Q	Q	kA
</s>
<s>
B	B	kA
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
≡	≡	k?
</s>
<s>
2	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
[	[	kIx(
</s>
<s>
17	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
3	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
[	[	kIx(
</s>
<s>
13	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
16	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
\	\	kIx~
<g/>
equiv	equiva	k1gFnPc2
d_	d_	k?
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
2	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdota	k1gFnPc2
[	[	kIx(
<g/>
17	#num#	k4
<g/>
;	;	kIx,
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
=	=	kIx~
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
;	;	kIx,
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
}	}	kIx)
</s>
<s>
Bob	Bob	k1gMnSc1
může	moct	k5eAaImIp3nS
nalézt	nalézt	k5eAaPmF,k5eAaBmF
bod	bod	k1gInSc4
Z	z	k7c2
<g/>
'	'	kIx"
<g/>
:	:	kIx,
</s>
<s>
Z	z	k7c2
</s>
<s>
′	′	k?
</s>
<s>
≡	≡	k?
</s>
<s>
d	d	k?
</s>
<s>
B	B	kA
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
≡	≡	k?
</s>
<s>
4	#num#	k4
</s>
<s>
⋅	⋅	k?
</s>
<s>
[	[	kIx(
</s>
<s>
5	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
19	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
mod	mod	k?
</s>
<s>
23	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
[	[	kIx(
</s>
<s>
13	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
16	#num#	k4
</s>
<s>
]	]	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
Z	z	k7c2
<g/>
'	'	kIx"
<g/>
\	\	kIx~
<g/>
equiv	equivit	k5eAaPmRp2nS
d_	d_	k?
<g/>
{	{	kIx(
<g/>
B	B	kA
<g/>
}	}	kIx)
<g/>
Q_	Q_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
A	A	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
\	\	kIx~
<g/>
equiv	equiv	k6eAd1
4	#num#	k4
<g/>
\	\	kIx~
<g/>
cdot	cdota	k1gFnPc2
[	[	kIx(
<g/>
5	#num#	k4
<g/>
;	;	kIx,
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
\	\	kIx~
<g/>
mod	mod	k?
23	#num#	k4
<g/>
=	=	kIx~
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
;	;	kIx,
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
}	}	kIx)
</s>
<s>
Z	z	k7c2
<g/>
=	=	kIx~
<g/>
Z	z	k7c2
<g/>
'	'	kIx"
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Byly	být	k5eAaImAgFnP
zvoleny	zvolen	k2eAgInPc4d1
nevhodné	vhodný	k2eNgInPc4d1
koeficienty	koeficient	k1gInPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
ostatní	ostatní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
příklad	příklad	k1gInSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
ilustrační	ilustrační	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
protokol	protokol	k1gInSc1
je	být	k5eAaImIp3nS
bezpečný	bezpečný	k2eAgInSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
nezveřejnilo	zveřejnit	k5eNaPmAgNnS
nic	nic	k6eAd1
kromě	kromě	k7c2
veřejných	veřejný	k2eAgInPc2d1
klíčů	klíč	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nejsou	být	k5eNaImIp3nP
tajné	tajný	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
ani	ani	k8xC
jedna	jeden	k4xCgFnSc1
strana	strana	k1gFnSc1
nedokáže	dokázat	k5eNaPmIp3nS
zjistit	zjistit	k5eAaPmF
soukromý	soukromý	k2eAgInSc4d1
klíč	klíč	k1gInSc4
té	ten	k3xDgFnSc2
druhé	druhý	k4xOgFnSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
nevyřešila	vyřešit	k5eNaPmAgFnS
problém	problém	k1gInSc4
diskrétního	diskrétní	k2eAgInSc2d1
logaritmu	logaritmus	k1gInSc2
u	u	k7c2
eliptických	eliptický	k2eAgFnPc2d1
křivek	křivka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Veřejné	veřejný	k2eAgInPc1d1
klíče	klíč	k1gInPc1
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
neměnné	měnný	k2eNgNnSc4d1,k2eAgNnSc4d1
(	(	kIx(
<g/>
a	a	k8xC
shledané	shledaný	k2eAgNnSc1d1
důvěryhodnými	důvěryhodný	k2eAgFnPc7d1
třeba	třeba	k6eAd1
pomocí	pomocí	k7c2
certifikátů	certifikát	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
dočasné	dočasný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dočasné	dočasný	k2eAgInPc1d1
klíče	klíč	k1gInPc1
nejsou	být	k5eNaImIp3nP
nutně	nutně	k6eAd1
ověřené	ověřený	k2eAgFnPc4d1
<g/>
,	,	kIx,
takže	takže	k8xS
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
ověření	ověření	k1gNnSc1
vyžadováno	vyžadován	k2eAgNnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
jej	on	k3xPp3gInSc4
zajistit	zajistit	k5eAaPmF
jinými	jiný	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Diffieho-Hellmanova	Diffieho-Hellmanův	k2eAgFnSc1d1
výměna	výměna	k1gFnSc1
klíčů	klíč	k1gInPc2
je	být	k5eAaImIp3nS
ale	ale	k9
napadnutelná	napadnutelný	k2eAgFnSc1d1
(	(	kIx(
<g/>
může	moct	k5eAaImIp3nS
obsahovat	obsahovat	k5eAaImF
zadní	zadní	k2eAgNnPc4d1
vrátka	vrátka	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://jiggerwit.wordpress.com/2013/09/25/the-nsa-back-door-to-nist/	https://jiggerwit.wordpress.com/2013/09/25/the-nsa-back-door-to-nist/	k4
-	-	kIx~
The	The	k1gFnSc2
NSA	NSA	kA
back	back	k1gMnSc1
door	door	k1gMnSc1
to	ten	k3xDgNnSc4
NIST	NIST	kA
</s>
