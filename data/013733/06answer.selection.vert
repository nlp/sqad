<s>
Diffieho-Hellmanův	Diffie-Hellmanův	k2eAgInSc1d1
protokol	protokol	k1gInSc1
s	s	k7c7
využitím	využití	k1gNnSc7
eliptických	eliptický	k2eAgFnPc2d1
křivek	křivka	k1gFnPc2
(	(	kIx(
<g/>
ECDH	ECDH	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
varianta	varianta	k1gFnSc1
Diffieho-Hellmanova	Diffie-Hellmanův	k2eAgInSc2d1
protokolu	protokol	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
využívá	využívat	k5eAaImIp3nS
eliptických	eliptický	k2eAgFnPc2d1
křivek	křivka	k1gFnPc2
<g/>
.	.	kIx.
</s>