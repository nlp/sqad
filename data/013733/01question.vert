<s>
Jaká	jaký	k3yQgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
varianty	varianta	k1gFnSc2
protokolu	protokol	k1gInSc2
Diffieho-Hellmana	Diffie-Hellman	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
využívá	využívat	k5eAaImIp3nS
eliptické	eliptický	k2eAgFnPc4d1
křivky	křivka	k1gFnPc4
<g/>
?	?	kIx.
</s>