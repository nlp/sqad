<s>
Ideologický	ideologický	k2eAgInSc1d1	ideologický
základ	základ	k1gInSc1	základ
Hlinkova	Hlinkův	k2eAgNnSc2d1	Hlinkovo
politického	politický	k2eAgNnSc2d1	politické
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
tvořilo	tvořit	k5eAaImAgNnS	tvořit
hluboké	hluboký	k2eAgNnSc1d1	hluboké
vlastenecké	vlastenecký	k2eAgNnSc1d1	vlastenecké
cítění	cítění	k1gNnSc1	cítění
(	(	kIx(	(
<g/>
zvýrazněné	zvýrazněný	k2eAgNnSc1d1	zvýrazněné
nejprve	nejprve	k6eAd1	nejprve
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
nadvládou	nadvláda	k1gFnSc7	nadvláda
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
zřetelně	zřetelně	k6eAd1	zřetelně
slabším	slabý	k2eAgNnSc7d2	slabší
postavením	postavení	k1gNnSc7	postavení
Slováků	Slováky	k1gInPc2	Slováky
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klerikalismus	klerikalismus	k1gInSc1	klerikalismus
a	a	k8xC	a
antikomunismus	antikomunismus	k1gInSc1	antikomunismus
<g/>
.	.	kIx.	.
</s>
