<s>
Ilorin	Ilorin	k1gInSc1
</s>
<s>
Ilorin	Ilorin	k1gInSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
Nigérie	Nigérie	k1gFnSc1
</s>
<s>
Ilorin	Ilorin	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
765	#num#	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
847	#num#	k4
582	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
108	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
PSČ	PSČ	kA
</s>
<s>
240001	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ilorin	Ilorin	k1gInSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
na	na	k7c6
západě	západ	k1gInSc6
Nigérie	Nigérie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
přibližně	přibližně	k6eAd1
848	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
federálního	federální	k2eAgInSc2d1
státu	stát	k1gInSc2
Kwara	Kwaro	k1gNnSc2
i	i	k9
domorodého	domorodý	k2eAgInSc2d1
Ilorinského	Ilorinský	k2eAgInSc2d1
emirátu	emirát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpočetnějšími	početní	k2eAgFnPc7d3
etnickými	etnický	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
jsou	být	k5eAaImIp3nP
Jorubové	Joruba	k1gMnPc1
<g/>
,	,	kIx,
Hausové	Haus	k1gMnPc1
a	a	k8xC
Fulbové	Fulb	k1gMnPc1
<g/>
,	,	kIx,
z	z	k7c2
náboženství	náboženství	k1gNnSc2
převládá	převládat	k5eAaImIp3nS
islám	islám	k1gInSc1
a	a	k8xC
křesťanství	křesťanství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
okolo	okolo	k7c2
roku	rok	k1gInSc2
1450	#num#	k4
a	a	k8xC
strážilo	strážit	k5eAaImAgNnS
severní	severní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
říše	říš	k1gFnSc2
Oyo	Oyo	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1817	#num#	k4
se	se	k3xPyFc4
velitel	velitel	k1gMnSc1
města	město	k1gNnSc2
Afonja	Afonja	k1gMnSc1
vzpbouřil	vzpbouřit	k5eAaPmAgMnS,k5eAaImAgMnS
proti	proti	k7c3
centrální	centrální	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
a	a	k8xC
vyhlásil	vyhlásit	k5eAaPmAgMnS
nezávislost	nezávislost	k1gFnSc4
Ilorinu	Ilorin	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgMnS
vazalem	vazal	k1gMnSc7
Sokotského	Sokotský	k2eAgInSc2d1
sultanátu	sultanát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
se	se	k3xPyFc4
město	město	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
britskou	britský	k2eAgFnSc7d1
državou	država	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
vládne	vládnout	k5eAaImIp3nS
tropické	tropický	k2eAgNnSc4d1
savanové	savanový	k2eAgNnSc4d1
klima	klima	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilorinem	Ilorin	k1gInSc7
protéká	protékat	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Awun	Awun	k1gInSc1
(	(	kIx(
<g/>
přítok	přítok	k1gInSc1
Nigeru	Niger	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severozápadně	severozápadně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
vrch	vrch	k1gInSc1
Sobi	sob	k1gMnPc1
Hill	Hill	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
vyhlášeným	vyhlášený	k2eAgNnSc7d1
vyhlídkovým	vyhlídkový	k2eAgNnSc7d1
místem	místo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dominuje	dominovat	k5eAaImIp3nS
zde	zde	k6eAd1
potravinářský	potravinářský	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
–	–	k?
v	v	k7c6
okolí	okolí	k1gNnSc6
Ilorinu	Ilorin	k1gInSc2
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
maniok	maniok	k1gInSc1
jedlý	jedlý	k2eAgInSc1d1
<g/>
,	,	kIx,
čirok	čirok	k1gInSc1
dvoubarevný	dvoubarevný	k2eAgInSc1d1
<g/>
,	,	kIx,
ledvinovník	ledvinovník	k1gInSc1
západní	západní	k2eAgInSc1d1
<g/>
,	,	kIx,
kolovník	kolovník	k1gInSc1
zašpičatělý	zašpičatělý	k2eAgInSc1d1
<g/>
,	,	kIx,
máslovník	máslovník	k1gInSc1
africký	africký	k2eAgInSc1d1
a	a	k8xC
podzemnice	podzemnice	k1gFnSc1
olejná	olejný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Dalšími	další	k2eAgMnPc7d1
průmyslovými	průmyslový	k2eAgFnPc7d1
odvětvími	odvětví	k1gNnPc7
jsou	být	k5eAaImIp3nP
výroba	výroba	k1gFnSc1
mýdla	mýdlo	k1gNnSc2
a	a	k8xC
zpracování	zpracování	k1gNnSc2
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvinutá	rozvinutý	k2eAgFnSc1d1
jsou	být	k5eAaImIp3nP
umělecká	umělecký	k2eAgNnPc4d1
řemesla	řemeslo	k1gNnPc4
jako	jako	k8xS,k8xC
hrnčířství	hrnčířství	k1gNnPc4
a	a	k8xC
tkalcovství	tkalcovství	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
zde	zde	k6eAd1
University	universita	k1gFnSc2
of	of	k?
Ilorin	Ilorin	k1gInSc1
a	a	k8xC
Al-Hikmah	Al-Hikmah	k1gInSc1
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
má	mít	k5eAaImIp3nS
silniční	silniční	k2eAgNnSc1d1
a	a	k8xC
železniční	železniční	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
s	s	k7c7
Lagosem	Lagos	k1gInSc7
a	a	k8xC
mezinárodní	mezinárodní	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ilorin	Ilorin	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
sídlem	sídlo	k1gNnSc7
prvoligového	prvoligový	k2eAgInSc2d1
fotbalového	fotbalový	k2eAgInSc2d1
klubu	klub	k1gInSc2
Kwara	Kwara	k1gMnSc1
United	United	k1gMnSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Nigerian	Nigerian	k1gMnSc1
Traditional	Traditional	k1gMnSc1
States	States	k1gMnSc1
<g/>
.	.	kIx.
www.worldstatesmen.org	www.worldstatesmen.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tribune	tribun	k1gMnSc5
Online	Onlin	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-09-06	2017-09-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ilorin	Ilorin	k1gInSc1
|	|	kIx~
Location	Location	k1gInSc1
<g/>
,	,	kIx,
History	Histor	k1gInPc1
<g/>
,	,	kIx,
Facts	Facts	k1gInSc1
<g/>
,	,	kIx,
&	&	k?
Population	Population	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RANTER	RANTER	kA
<g/>
,	,	kIx,
Harro	Harro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilorin	Ilorin	k1gInSc1
Airport	Airport	k1gInSc4
profile	profil	k1gInSc5
-	-	kIx~
Aviation	Aviation	k1gInSc1
Safety	Safet	k2eAgInPc1d1
Network	network	k1gInPc7
<g/>
.	.	kIx.
aviation-safety	aviation-safet	k1gInPc7
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ilorin	Ilorin	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Ilorin	Ilorin	k1gInSc1
Info	Info	k6eAd1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4253394-6	4253394-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79035894	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
138360391	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79035894	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
