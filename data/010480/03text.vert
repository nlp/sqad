<p>
<s>
Dratvuo	Dratvuo	k6eAd1	Dratvuo
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
také	také	k9	také
Dratvinys	Dratvinys	k1gInSc1	Dratvinys
<g/>
,	,	kIx,	,
Dratvinis	Dratvinis	k1gInSc1	Dratvinis
<g/>
,	,	kIx,	,
Dretvinis	Dretvinis	k1gInSc1	Dretvinis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
říčka	říčka	k1gFnSc1	říčka
2	[number]	k4	2
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Litvy	Litva	k1gFnSc2	Litva
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kelmė	Kelmė	k1gFnSc2	Kelmė
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgInSc4d1	pravý
přítok	přítok	k1gInSc4	přítok
řeky	řeka	k1gFnSc2	řeka
Dubysa	Dubysa	k1gFnSc1	Dubysa
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Lyduvė	Lyduvė	k1gFnSc2	Lyduvė
1	[number]	k4	1
km	km	kA	km
před	před	k7c7	před
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
lyduvė	lyduvė	k?	lyduvė
železničním	železniční	k2eAgInSc7d1	železniční
mostem	most	k1gInSc7	most
<g/>
,	,	kIx,	,
75,5	[number]	k4	75,5
km	km	kA	km
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Dubysy	Dubysa	k1gFnSc2	Dubysa
do	do	k7c2	do
Němenu	Němen	k1gInSc2	Němen
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
1	[number]	k4	1
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
vsi	ves	k1gFnSc2	ves
Vaišviliškiai	Vaišviliškia	k1gFnSc2	Vaišviliškia
<g/>
,	,	kIx,	,
1,7	[number]	k4	1,7
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Lioliai	Liolia	k1gFnSc2	Liolia
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
klikatí	klikatit	k5eAaImIp3nS	klikatit
západoseverozápadním	západoseverozápadní	k2eAgInSc7d1	západoseverozápadní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
rybníky	rybník	k1gInPc4	rybník
Liolių	Liolių	k1gFnSc2	Liolių
II	II	kA	II
tvenkinys	tvenkinys	k6eAd1	tvenkinys
(	(	kIx(	(
<g/>
plocha	plocha	k1gFnSc1	plocha
3	[number]	k4	3
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liolių	Liolių	k1gFnSc1	Liolių
I	i	k8xC	i
tvenkinys	tvenkinys	k1gInSc1	tvenkinys
(	(	kIx(	(
<g/>
plocha	plocha	k1gFnSc1	plocha
0,9	[number]	k4	0,9
ha	ha	kA	ha
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
třemi	tři	k4xCgInPc7	tři
menšími	malý	k2eAgInPc7d2	menší
rybníčky	rybníček	k1gInPc7	rybníček
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledním	poslední	k2eAgMnSc6d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
<g/>
,	,	kIx,	,
po	po	k7c6	po
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Juodupisem	Juodupis	k1gInSc7	Juodupis
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
dalším	další	k2eAgInSc7d1	další
rybníkem	rybník	k1gInSc7	rybník
<g/>
,	,	kIx,	,
po	po	k7c6	po
soutoku	soutok	k1gInSc6	soutok
s	s	k7c7	s
Palyksnė	Palyksnė	k1gFnSc7	Palyksnė
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
až	až	k9	až
k	k	k7c3	k
soutoku	soutok	k1gInSc3	soutok
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Dubysa	Dubys	k1gMnSc2	Dubys
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
40	[number]	k4	40
m	m	kA	m
hlubokým	hluboký	k2eAgNnPc3d1	hluboké
a	a	k8xC	a
250	[number]	k4	250
m	m	kA	m
širokým	široký	k2eAgNnSc7d1	široké
říčním	říční	k2eAgNnSc7d1	říční
údolím	údolí	k1gNnSc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
spád	spád	k1gInSc1	spád
je	být	k5eAaImIp3nS	být
60	[number]	k4	60
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Přítoky	přítok	k1gInPc4	přítok
==	==	k?	==
</s>
</p>
<p>
<s>
Levé	levá	k1gFnPc1	levá
<g/>
:	:	kIx,	:
<g/>
Pravé	pravá	k1gFnPc1	pravá
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgFnSc6d1	jazyková
souvislosti	souvislost	k1gFnSc6	souvislost
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
původem	původ	k1gInSc7	původ
německého	německý	k2eAgMnSc2d1	německý
dratas	dratas	k1gMnSc1	dratas
–	–	k?	–
draht	draht	k1gMnSc1	draht
–	–	k?	–
drát	drát	k1gInSc1	drát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skloňování	skloňování	k1gNnSc2	skloňování
===	===	k?	===
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Dratvuo	Dratvuo	k6eAd1	Dratvuo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
litevštině	litevština	k1gFnSc6	litevština
rodu	rod	k1gInSc2	rod
ženského	ženský	k2eAgInSc2d1	ženský
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
jednotné	jednotný	k2eAgInPc1d1	jednotný
<g/>
,	,	kIx,	,
skloňování	skloňování	k1gNnSc1	skloňování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
V.	V.	kA	V.
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
(	(	kIx(	(
<g/>
než	než	k8xS	než
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
p.	p.	k?	p.
č.	č.	k?	č.
<g/>
j.	j.	k?	j.
<g/>
)	)	kIx)	)
pádech	pád	k1gInPc6	pád
mezi	mezi	k7c4	mezi
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
příponu	přípona	k1gFnSc4	přípona
vkládá	vkládat	k5eAaImIp3nS	vkládat
vsuvka	vsuvka	k1gFnSc1	vsuvka
-en-	n-	k?	-en-
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
respektovat	respektovat	k5eAaImF	respektovat
tato	tento	k3xDgFnSc1	tento
zvláštnost	zvláštnost	k1gFnSc1	zvláštnost
<g/>
,	,	kIx,	,
skloňovalo	skloňovat	k5eAaImAgNnS	skloňovat
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
p.	p.	k?	p.
Dratvuo	Dratvuo	k6eAd1	Dratvuo
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
p.	p.	k?	p.
Dratveně	Dratveně	k1gFnSc1	Dratveně
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
p.	p.	k?	p.
Dratveni	Dratven	k2eAgMnPc1d1	Dratven
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
p.	p.	k?	p.
Dratvuo	Dratvuo	k6eAd1	Dratvuo
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
p.	p.	k?	p.
Dratveni	Dratven	k2eAgMnPc1d1	Dratven
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
p.	p.	k?	p.
(	(	kIx(	(
<g/>
o	o	k7c4	o
<g/>
)	)	kIx)	)
Dratveni	Dratven	k2eAgMnPc1d1	Dratven
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
p.	p.	k?	p.
Dratvení	Dratvení	k1gNnSc1	Dratvení
</s>
</p>
<p>
<s>
Přivlastńovací	Přivlastńovací	k2eAgNnSc1d1	Přivlastńovací
přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
<g/>
:	:	kIx,	:
Dratvenský	Dratvenský	k2eAgInSc1d1	Dratvenský
<g/>
,	,	kIx,	,
-á	-á	k?	-á
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
é.	é.	k?	é.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
skloňování	skloňování	k1gNnSc2	skloňování
není	být	k5eNaImIp3nS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
ÚJČ	ÚJČ	kA	ÚJČ
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Lietuvos	Lietuvos	k1gInSc1	Lietuvos
autokelių	autokelių	k?	autokelių
atlasas	atlasas	k1gInSc1	atlasas
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
200	[number]	k4	200
000	[number]	k4	000
:	:	kIx,	:
Jā	Jā	k1gFnSc1	Jā
Sē	Sē	k1gFnSc2	Sē
Map	mapa	k1gFnPc2	mapa
publishers	publishersa	k1gFnPc2	publishersa
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
:	:	kIx,	:
Rī	Rī	k1gFnSc1	Rī
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
–	–	k?	–
222	[number]	k4	222
p.	p.	k?	p.
ISBN	ISBN	kA	ISBN
978-9984-07-475-7	[number]	k4	978-9984-07-475-7
</s>
</p>
