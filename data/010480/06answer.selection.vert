<s>
Název	název	k1gInSc1	název
Dratvuo	Dratvuo	k6eAd1	Dratvuo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
litevštině	litevština	k1gFnSc6	litevština
rodu	rod	k1gInSc2	rod
ženského	ženský	k2eAgInSc2d1	ženský
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
jednotné	jednotný	k2eAgInPc1d1	jednotný
<g/>
,	,	kIx,	,
skloňování	skloňování	k1gNnSc1	skloňování
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
zřídka	zřídka	k6eAd1	zřídka
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
V.	V.	kA	V.
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
(	(	kIx(	(
<g/>
než	než	k8xS	než
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
p.	p.	k?	p.
č.	č.	k?	č.
<g/>
j.	j.	k?	j.
<g/>
)	)	kIx)	)
pádech	pád	k1gInPc6	pád
mezi	mezi	k7c4	mezi
kmen	kmen	k1gInSc4	kmen
a	a	k8xC	a
příponu	přípona	k1gFnSc4	přípona
vkládá	vkládat	k5eAaImIp3nS	vkládat
vsuvka	vsuvka	k1gFnSc1	vsuvka
-en-	n-	k?	-en-
<g/>
.	.	kIx.	.
</s>
