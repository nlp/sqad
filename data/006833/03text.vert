<s>
Chris	Chris	k1gFnSc1	Chris
Thomas	Thomas	k1gMnSc1	Thomas
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Perivale	Perivala	k1gFnSc6	Perivala
<g/>
,	,	kIx,	,
Middlesex	Middlesex	k1gInSc1	Middlesex
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
například	například	k6eAd1	například
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
The	The	k1gMnPc2	The
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
Climax	Climax	k1gInSc1	Climax
Blues	blues	k1gNnSc2	blues
Band	banda	k1gFnPc2	banda
<g/>
;	;	kIx,	;
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
například	například	k6eAd1	například
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
Badfinger	Badfingra	k1gFnPc2	Badfingra
<g/>
,	,	kIx,	,
Procol	Procol	k1gInSc1	Procol
Harum	Harum	k1gInSc1	Harum
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
INXS	INXS	kA	INXS
nebo	nebo	k8xC	nebo
The	The	k1gFnSc7	The
Pretenders	Pretendersa	k1gFnPc2	Pretendersa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
produkoval	produkovat	k5eAaImAgMnS	produkovat
album	album	k1gNnSc4	album
Paris	Paris	k1gMnSc1	Paris
1919	[number]	k4	1919
velšského	velšský	k2eAgMnSc2d1	velšský
hudebníka	hudebník	k1gMnSc2	hudebník
Johna	John	k1gMnSc2	John
Calea	Caleus	k1gMnSc2	Caleus
<g/>
;	;	kIx,	;
s	s	k7c7	s
Calem	Cal	k1gInSc7	Cal
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
i	i	k9	i
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
−	−	k?	−
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
albu	album	k1gNnSc6	album
Slow	Slow	k1gMnPc4	Slow
Dazzle	Dazzle	k1gFnPc2	Dazzle
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
doprovodné	doprovodný	k2eAgFnSc6d1	doprovodná
skupině	skupina	k1gFnSc6	skupina
jako	jako	k9	jako
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
producentem	producent	k1gMnSc7	producent
nahrávek	nahrávka	k1gFnPc2	nahrávka
Eltona	Elton	k1gMnSc4	Elton
Johna	John	k1gMnSc4	John
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Beatles	beatles	k1gMnSc1	beatles
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Climax	Climax	k1gInSc1	Climax
Chicago	Chicago	k1gNnSc1	Chicago
Blues	blues	k1gFnPc2	blues
Band	banda	k1gFnPc2	banda
(	(	kIx(	(
<g/>
Climax	Climax	k1gInSc1	Climax
Blues	blues	k1gNnSc1	blues
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Climax	Climax	k1gInSc1	Climax
Blues	blues	k1gNnSc1	blues
Band	banda	k1gFnPc2	banda
Plays	Playsa	k1gFnPc2	Playsa
On	on	k3xPp3gInSc1	on
(	(	kIx(	(
<g/>
Climax	Climax	k1gInSc1	Climax
Blues	blues	k1gNnSc1	blues
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Home	Hom	k1gInSc2	Hom
(	(	kIx(	(
<g/>
Procol	Procol	k1gInSc1	Procol
Harum	Harum	k1gInSc1	Harum
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
A	a	k9	a
Lot	Lot	k1gMnSc1	Lot
of	of	k?	of
Bottle	Bottle	k1gFnSc1	Bottle
(	(	kIx(	(
<g/>
Climax	Climax	k1gInSc1	Climax
Blues	blues	k1gNnSc1	blues
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Broken	Broken	k2eAgInSc1d1	Broken
Barricades	Barricades	k1gInSc1	Barricades
(	(	kIx(	(
<g/>
Procol	Procol	k1gInSc1	Procol
Harum	Harum	k1gInSc1	Harum
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Tightly	Tightly	k1gMnSc1	Tightly
Knit	Knit	k1gMnSc1	Knit
(	(	kIx(	(
<g/>
Climax	Climax	k1gInSc1	Climax
Blues	blues	k1gNnSc1	blues
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Mick	Mick	k1gInSc1	Mick
Abrahams	Abrahams	k1gInSc1	Abrahams
(	(	kIx(	(
<g/>
Mick	Mick	k1gInSc1	Mick
Abrahams	Abrahamsa	k1gFnPc2	Abrahamsa
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Procol	Procol	k1gInSc1	Procol
<g />
.	.	kIx.	.
</s>
<s>
Harum	Harum	k1gInSc1	Harum
Live	Liv	k1gFnSc2	Liv
in	in	k?	in
Concert	Concert	k1gMnSc1	Concert
with	with	k1gMnSc1	with
the	the	k?	the
Edmonton	Edmonton	k1gInSc1	Edmonton
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
(	(	kIx(	(
<g/>
Procol	Procol	k1gInSc1	Procol
Harum	Harum	k1gInSc1	Harum
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Paris	Paris	k1gMnSc1	Paris
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Grand	grand	k1gMnSc1	grand
Hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
Procol	Procol	k1gInSc1	Procol
Harum	Harum	k1gInSc1	Harum
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
For	forum	k1gNnPc2	forum
Your	Youra	k1gFnPc2	Youra
Pleasure	Pleasur	k1gMnSc5	Pleasur
(	(	kIx(	(
<g/>
Roxy	Roxa	k1gFnPc4	Roxa
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gMnPc2	The
Dark	Dark	k1gMnSc1	Dark
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
(	(	kIx(	(
<g/>
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Ass	Ass	k1gMnSc1	Ass
(	(	kIx(	(
<g/>
Badfinger	Badfinger	k1gMnSc1	Badfinger
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Stranded	Stranded	k1gInSc1	Stranded
(	(	kIx(	(
<g/>
Roxy	Roxy	k1gInPc1	Roxy
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Exotic	Exotice	k1gFnPc2	Exotice
Birds	Birds	k1gInSc1	Birds
and	and	k?	and
Fruit	Fruit	k1gInSc1	Fruit
(	(	kIx(	(
<g/>
Procol	Procol	k1gInSc1	Procol
Harum	Harum	k1gInSc1	Harum
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Here	Her	k1gInSc2	Her
<g />
.	.	kIx.	.
</s>
<s>
Come	Come	k1gFnSc1	Come
the	the	k?	the
Warm	Warm	k1gInSc1	Warm
Jets	Jets	k1gInSc1	Jets
(	(	kIx(	(
<g/>
Brian	Brian	k1gMnSc1	Brian
Eno	Eno	k1gMnSc1	Eno
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Wish	Wish	k1gInSc1	Wish
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
(	(	kIx(	(
<g/>
Badfinger	Badfinger	k1gMnSc1	Badfinger
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Country	country	k2eAgFnPc1d1	country
Life	Lif	k1gFnPc1	Lif
(	(	kIx(	(
<g/>
Roxy	Roxy	k1gInPc1	Roxy
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Badfinger	Badfingra	k1gFnPc2	Badfingra
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Slow	Slow	k1gMnSc1	Slow
Dazzle	Dazzle	k1gMnSc1	Dazzle
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Siren	Siren	k1gInSc1	Siren
(	(	kIx(	(
<g/>
Roxy	Roxy	k1gInPc1	Roxy
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stick	Stick	k1gInSc1	Stick
Together	Togethra	k1gFnPc2	Togethra
(	(	kIx(	(
<g/>
Bryan	Bryan	k1gInSc1	Bryan
Ferry	Ferro	k1gNnPc7	Ferro
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Viva	Viv	k1gInSc2	Viv
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Roxy	Roxa	k1gFnPc4	Roxa
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Full	Full	k1gInSc1	Full
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
Frankie	Frankie	k1gFnSc1	Frankie
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Hurt	Hurt	k1gMnSc1	Hurt
(	(	kIx(	(
<g/>
Chris	Chris	k1gFnSc1	Chris
Spedding	Spedding	k1gInSc1	Spedding
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Never	Never	k1gMnSc1	Never
Mind	Mind	k1gMnSc1	Mind
the	the	k?	the
Bollocks	Bollocks	k1gInSc1	Bollocks
<g/>
,	,	kIx,	,
Here	Here	k1gInSc1	Here
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
the	the	k?	the
Sex	sex	k1gInSc1	sex
Pistols	Pistols	k1gInSc1	Pistols
(	(	kIx(	(
<g/>
Sex	sex	k1gInSc1	sex
Pistols	Pistols	k1gInSc1	Pistols
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Back	Back	k1gMnSc1	Back
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Egg	Egg	k1gFnSc1	Egg
(	(	kIx(	(
<g/>
Wings	Wings	k1gInSc1	Wings
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Pretenders	Pretenders	k1gInSc1	Pretenders
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Pretenders	Pretendersa	k1gFnPc2	Pretendersa
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Empty	Empta	k1gFnSc2	Empta
Glass	Glass	k1gInSc1	Glass
(	(	kIx(	(
<g/>
Pete	Pete	k1gInSc1	Pete
Townshend	Townshenda	k1gFnPc2	Townshenda
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Fox	fox	k1gInSc1	fox
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Pretenders	Pretenders	k1gInSc1	Pretenders
II	II	kA	II
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Pretenders	Pretendersa	k1gFnPc2	Pretendersa
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Jump	Jump	k1gMnSc1	Jump
Up	Up	k1gMnSc1	Up
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
All	All	k1gMnPc2	All
the	the	k?	the
Best	Best	k2eAgInSc4d1	Best
Cowboys	Cowboys	k1gInSc4	Cowboys
Have	Have	k1gNnSc2	Have
Chinese	Chinese	k1gFnSc2	Chinese
Eyes	Eyes	k1gInSc1	Eyes
(	(	kIx(	(
<g/>
Pete	Pete	k1gInSc1	Pete
Townshend	Townshenda	k1gFnPc2	Townshenda
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Too	Too	k1gMnSc1	Too
Low	Low	k1gMnSc1	Low
for	forum	k1gNnPc2	forum
Zero	Zero	k6eAd1	Zero
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Hysteria	Hysterium	k1gNnSc2	Hysterium
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Human	Humany	k1gInPc2	Humany
League	Leagu	k1gMnSc2	Leagu
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Learning	Learning	k1gInSc1	Learning
<g />
.	.	kIx.	.
</s>
<s>
to	ten	k3xDgNnSc4	ten
Crawl	Crawl	k1gMnSc1	Crawl
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Pretenders	Pretendersa	k1gFnPc2	Pretendersa
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Breaking	Breaking	k1gInSc1	Breaking
Hearts	Hearts	k1gInSc1	Hearts
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Listen	listen	k1gInSc1	listen
Like	Like	k1gNnSc1	Like
Thieves	Thieves	k1gInSc1	Thieves
(	(	kIx(	(
<g/>
INXS	INXS	kA	INXS
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
White	Whit	k1gInSc5	Whit
City	city	k1gNnSc1	city
<g/>
:	:	kIx,	:
A	a	k9	a
Novel	novela	k1gFnPc2	novela
(	(	kIx(	(
<g/>
Pete	Pete	k1gInSc1	Pete
Townshend	Townshenda	k1gFnPc2	Townshenda
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Kick	Kicka	k1gFnPc2	Kicka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
INXS	INXS	kA	INXS
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Reg	Reg	k1gMnSc1	Reg
Strikes	Strikes	k1gMnSc1	Strikes
Back	Back	k1gMnSc1	Back
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
Nude	Nude	k1gInSc1	Nude
Guitars	Guitars	k1gInSc1	Guitars
(	(	kIx(	(
<g/>
Brian	Brian	k1gMnSc1	Brian
Setzer	Setzer	k1gMnSc1	Setzer
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Sleeping	Sleeping	k1gInSc1	Sleeping
with	with	k1gInSc1	with
the	the	k?	the
Past	past	k1gFnSc1	past
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
X	X	kA	X
(	(	kIx(	(
<g/>
INXS	INXS	kA	INXS
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
One	One	k1gMnSc1	One
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Last	Last	k1gInSc1	Last
of	of	k?	of
the	the	k?	the
Independents	Independents	k1gInSc1	Independents
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Pretenders	Pretendersa	k1gFnPc2	Pretendersa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
(	(	kIx(	(
<g/>
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Different	Different	k1gInSc1	Different
Class	Classa	k1gFnPc2	Classa
(	(	kIx(	(
<g/>
Pulp	pulpa	k1gFnPc2	pulpa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Big	Big	k?	Big
Picture	Pictur	k1gMnSc5	Pictur
(	(	kIx(	(
<g/>
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
This	This	k1gInSc1	This
Is	Is	k1gMnSc5	Is
Hardcore	Hardcor	k1gMnSc5	Hardcor
(	(	kIx(	(
<g/>
Pulp	pulpa	k1gFnPc2	pulpa
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Run	run	k1gInSc1	run
Devil	Devil	k1gMnSc1	Devil
Run	run	k1gInSc1	run
(	(	kIx(	(
<g/>
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
How	How	k1gMnSc1	How
to	ten	k3xDgNnSc4	ten
Dismantle	Dismantle	k1gFnSc1	Dismantle
an	an	k?	an
Atomic	Atomice	k1gFnPc2	Atomice
Bomb	bomba	k1gFnPc2	bomba
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
On	on	k3xPp3gMnSc1	on
an	an	k?	an
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
Davig	Davig	k1gMnSc1	Davig
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Razorlight	Razorlight	k1gMnSc1	Razorlight
(	(	kIx(	(
<g/>
Razorlight	Razorlight	k1gMnSc1	Razorlight
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Serotonin	serotonin	k1gInSc1	serotonin
(	(	kIx(	(
<g/>
Mystery	Myster	k1gInPc1	Myster
Jets	Jetsa	k1gFnPc2	Jetsa
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Snapshot	Snapshot	k1gMnSc1	Snapshot
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Strypes	Strypes	k1gMnSc1	Strypes
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
