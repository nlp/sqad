<s>
Švýcarští	švýcarský	k2eAgMnPc1d1	švýcarský
salašničtí	salašnický	k2eAgMnPc1d1	salašnický
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Sennenhund	Sennenhund	k1gInSc1	Sennenhund
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
psích	psí	k2eAgNnPc2d1	psí
plemen	plemeno	k1gNnPc2	plemeno
pocházející	pocházející	k2eAgInSc1d1	pocházející
ze	z	k7c2	z
Švýcarských	švýcarský	k2eAgFnPc2d1	švýcarská
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
farmářské	farmářský	k2eAgMnPc4d1	farmářský
psy	pes	k1gMnPc4	pes
molossoidního	molossoidní	k2eAgInSc2d1	molossoidní
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgNnPc4	čtyři
plemena	plemeno	k1gNnPc4	plemeno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
švýcarské	švýcarský	k2eAgMnPc4d1	švýcarský
salašnické	salašnický	k2eAgMnPc4d1	salašnický
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
sdílí	sdílet	k5eAaImIp3nP	sdílet
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
trikolorní	trikolorní	k2eAgFnSc4d1	trikolorní
barvu	barva	k1gFnSc4	barva
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
silnou	silný	k2eAgFnSc4d1	silná
stavbu	stavba	k1gFnSc4	stavba
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
klidný	klidný	k2eAgInSc4d1	klidný
temperament	temperament	k1gInSc4	temperament
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
plemen	plemeno	k1gNnPc2	plemeno
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
středně	středně	k6eAd1	středně
velkých	velký	k2eAgFnPc2d1	velká
po	po	k7c4	po
velké	velký	k2eAgFnPc4d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zmiňovaná	zmiňovaný	k2eAgNnPc4d1	zmiňované
čtyři	čtyři	k4xCgNnPc4	čtyři
plemena	plemeno	k1gNnPc4	plemeno
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Bernský	bernský	k2eAgMnSc1d1	bernský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
Velký	velký	k2eAgMnSc1d1	velký
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
Appenzellský	Appenzellský	k2eAgMnSc1d1	Appenzellský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
živý	živý	k2eAgMnSc1d1	živý
<g/>
,	,	kIx,	,
temperamentní	temperamentní	k2eAgMnSc1d1	temperamentní
<g/>
,	,	kIx,	,
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
téměř	téměř	k6eAd1	téměř
kvadratický	kvadratický	k2eAgMnSc1d1	kvadratický
pes	pes	k1gMnSc1	pes
s	s	k7c7	s
typicky	typicky	k6eAd1	typicky
zatočeným	zatočený	k2eAgInSc7d1	zatočený
ocasem	ocas	k1gInSc7	ocas
nad	nad	k7c7	nad
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Věrný	věrný	k2eAgInSc1d1	věrný
jedné	jeden	k4xCgFnSc6	jeden
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
nedůvěřivý	důvěřivý	k2eNgInSc4d1	nedůvěřivý
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Výborný	výborný	k2eAgMnSc1d1	výborný
společník	společník	k1gMnSc1	společník
a	a	k8xC	a
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Entlebušský	Entlebušský	k2eAgMnSc1d1	Entlebušský
salašnický	salašnický	k2eAgMnSc1d1	salašnický
pes	pes	k1gMnSc1	pes
</s>
