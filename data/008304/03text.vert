<p>
<s>
Siesartis	Siesartis	k1gFnSc1	Siesartis
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
,	,	kIx,	,
v	v	k7c6	v
Suvalkiji	Suvalkiji	k1gFnSc6	Suvalkiji
<g/>
,	,	kIx,	,
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Šakiai	Šakia	k1gFnSc2	Šakia
<g/>
.	.	kIx.	.
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Naidynė	Naidynė	k1gFnSc2	Naidynė
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Kaunasu	Kaunas	k1gInSc2	Kaunas
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
převážně	převážně	k6eAd1	převážně
směrem	směr	k1gInSc7	směr
západním	západní	k2eAgInSc7d1	západní
na	na	k7c6	na
území	území	k1gNnSc6	území
okresu	okres	k1gInSc2	okres
Šakiai	Šakia	k1gFnSc2	Šakia
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Slavikai	Slavika	k1gFnSc2	Slavika
se	se	k3xPyFc4	se
na	na	k7c6	na
říční	říční	k2eAgFnSc6d1	říční
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Kaliningradskou	kaliningradský	k2eAgFnSc7d1	Kaliningradská
oblastí	oblast	k1gFnSc7	oblast
Ruska	Rusko	k1gNnSc2	Rusko
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Šešupė	Šešupė	k1gFnSc2	Šešupė
jako	jako	k8xS	jako
její	její	k3xOp3gInSc1	její
pravý	pravý	k2eAgInSc1d1	pravý
přítok	přítok	k1gInSc1	přítok
<g/>
,	,	kIx,	,
77,7	[number]	k4	77,7
km	km	kA	km
od	od	k7c2	od
jejího	její	k3xOp3gMnSc2	její
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
Němenu	Němen	k1gInSc2	Němen
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
ročního	roční	k2eAgInSc2d1	roční
průtoku	průtok	k1gInSc2	průtok
<g/>
:	:	kIx,	:
jaro	jaro	k1gNnSc4	jaro
-	-	kIx~	-
50	[number]	k4	50
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
léto	léto	k1gNnSc1	léto
-	-	kIx~	-
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc4	podzim
-	-	kIx~	-
16	[number]	k4	16
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
zima	zima	k1gFnSc1	zima
-	-	kIx~	-
28	[number]	k4	28
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
suchém	suchý	k2eAgNnSc6d1	suché
létě	léto	k1gNnSc6	léto
místy	místy	k6eAd1	místy
vysychá	vysychat	k5eAaImIp3nS	vysychat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
==	==	k?	==
</s>
</p>
<p>
<s>
Naidynė	Naidynė	k?	Naidynė
<g/>
,	,	kIx,	,
Gerdžiai	Gerdžia	k1gMnPc5	Gerdžia
<g/>
,	,	kIx,	,
Akmenynė	Akmenynė	k1gMnPc5	Akmenynė
<g/>
,	,	kIx,	,
Spruktiškė	Spruktiškė	k1gMnPc5	Spruktiškė
<g/>
,	,	kIx,	,
Kerai	Kera	k1gMnPc5	Kera
<g/>
,	,	kIx,	,
Liepalotai	Liepalota	k1gMnPc5	Liepalota
<g/>
,	,	kIx,	,
Lukšiai	Lukšia	k1gMnPc5	Lukšia
<g/>
,	,	kIx,	,
Girė	Girė	k1gMnPc5	Girė
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
Šakiai	Šakia	k1gFnSc2	Šakia
<g/>
,	,	kIx,	,
Valiai	Valia	k1gMnPc5	Valia
<g/>
,	,	kIx,	,
Pyragiai	Pyragia	k1gMnPc5	Pyragia
<g/>
,	,	kIx,	,
Duobiškė	Duobiškė	k1gMnPc5	Duobiškė
<g/>
,	,	kIx,	,
Duobiškiai	Duobiškia	k1gMnPc5	Duobiškia
<g/>
,	,	kIx,	,
Baltkojai	Baltkoja	k1gMnPc5	Baltkoja
<g/>
,	,	kIx,	,
Vabališkiai	Vabališkia	k1gMnPc5	Vabališkia
<g/>
,	,	kIx,	,
Gotlybiškiai	Gotlybiškia	k1gMnPc5	Gotlybiškia
<g/>
,	,	kIx,	,
Endrikiai	Endrikia	k1gMnPc5	Endrikia
<g/>
,	,	kIx,	,
Bū	Bū	k1gMnPc5	Bū
<g/>
,	,	kIx,	,
Juškakaimiai	Juškakaimia	k1gMnPc5	Juškakaimia
<g/>
,	,	kIx,	,
Slavikai	Slavika	k1gFnSc5	Slavika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přítoky	přítok	k1gInPc4	přítok
==	==	k?	==
</s>
</p>
<p>
<s>
Levé	levá	k1gFnPc1	levá
<g/>
:	:	kIx,	:
<g/>
Pravé	pravá	k1gFnPc1	pravá
</s>
</p>
<p>
<s>
==	==	k?	==
Jazykové	jazykový	k2eAgFnSc6d1	jazyková
souvislosti	souvislost	k1gFnSc6	souvislost
==	==	k?	==
</s>
</p>
<p>
<s>
Siesartis	Siesartis	k1gFnSc1	Siesartis
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
litevštině	litevština	k1gFnSc6	litevština
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
rodu	rod	k1gInSc2	rod
ženského	ženský	k2eAgInSc2d1	ženský
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
jednotné	jednotný	k2eAgNnSc1d1	jednotné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Siesartis	Siesartis	k1gFnSc2	Siesartis
(	(	kIx(	(
<g/>
Šešupė	Šešupė	k1gMnSc1	Šešupė
<g/>
)	)	kIx)	)
na	na	k7c6	na
litevské	litevský	k2eAgFnSc6d1	Litevská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Siesartis	Siesartis	k1gFnSc2	Siesartis
(	(	kIx(	(
<g/>
Šešupė	Šešupė	k1gFnSc1	Šešupė
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
