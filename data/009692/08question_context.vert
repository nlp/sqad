<s>
Netopýrek	netopýrek	k1gMnSc1	netopýrek
thajský	thajský	k2eAgMnSc1d1	thajský
(	(	kIx(	(
<g/>
Craseonycteris	Craseonycteris	k1gInSc1	Craseonycteris
thonglongyai	thonglongya	k1gFnSc2	thonglongya
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
netopýr	netopýr	k1gMnSc1	netopýr
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
netopýrkovitých	topýrkovitý	k2eNgMnPc2d1	topýrkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejlehčího	lehký	k2eAgMnSc4d3	nejlehčí
netopýra	netopýr	k1gMnSc4	netopýr
a	a	k8xC	a
o	o	k7c4	o
nejmenšího	malý	k2eAgMnSc4d3	nejmenší
savce	savec	k1gMnSc4	savec
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
