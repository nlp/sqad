<p>
<s>
Netopýrek	netopýrek	k1gMnSc1	netopýrek
thajský	thajský	k2eAgMnSc1d1	thajský
(	(	kIx(	(
<g/>
Craseonycteris	Craseonycteris	k1gInSc1	Craseonycteris
thonglongyai	thonglongya	k1gFnSc2	thonglongya
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
netopýr	netopýr	k1gMnSc1	netopýr
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
netopýrkovitých	topýrkovitý	k2eNgMnPc2d1	topýrkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejlehčího	lehký	k2eAgMnSc4d3	nejlehčí
netopýra	netopýr	k1gMnSc4	netopýr
a	a	k8xC	a
o	o	k7c4	o
nejmenšího	malý	k2eAgMnSc4d3	nejmenší
savce	savec	k1gMnSc4	savec
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgMnSc1	tento
netopýr	netopýr	k1gMnSc1	netopýr
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
asi	asi	k9	asi
tři	tři	k4xCgInPc1	tři
centimetry	centimetr	k1gInPc1	centimetr
a	a	k8xC	a
rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
nejmenší	malý	k2eAgMnSc1d3	nejmenší
známý	známý	k2eAgMnSc1d1	známý
netopýr	netopýr	k1gMnSc1	netopýr
a	a	k8xC	a
současně	současně	k6eAd1	současně
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgMnPc2d3	nejmenší
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
čmeláčí	čmeláčí	k2eAgMnSc1d1	čmeláčí
netopýr	netopýr	k1gMnSc1	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
jeho	jeho	k3xOp3gInSc7	jeho
význačným	význačný	k2eAgInSc7d1	význačný
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
nos	nos	k1gInSc1	nos
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
prasečímu	prasečí	k2eAgInSc3d1	prasečí
rypáku	rypák	k1gInSc3	rypák
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
nemá	mít	k5eNaImIp3nS	mít
ocásek	ocásek	k1gInSc1	ocásek
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc1d1	velké
uši	ucho	k1gNnPc4	ucho
s	s	k7c7	s
nápadnými	nápadný	k2eAgFnPc7d1	nápadná
záklopkami	záklopka	k1gFnPc7	záklopka
zvukovodu	zvukovod	k1gInSc2	zvukovod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Netopýrek	netopýrek	k1gMnSc1	netopýrek
thajský	thajský	k2eAgMnSc1d1	thajský
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
thajském	thajský	k2eAgInSc6d1	thajský
Národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Sai	Sai	k1gMnSc3	Sai
Yok	Yok	k1gMnSc3	Yok
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
na	na	k7c6	na
území	území	k1gNnSc6	území
sousední	sousední	k2eAgFnSc2d1	sousední
Barmy	Barma	k1gFnSc2	Barma
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
mnohé	mnohý	k2eAgInPc4d1	mnohý
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
netopýrů	netopýr	k1gMnPc2	netopýr
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
hmyzu	hmyz	k1gInSc2	hmyz
echolokátor	echolokátor	k1gMnSc1	echolokátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
velikosti	velikost	k1gFnSc3	velikost
má	mít	k5eAaImIp3nS	mít
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vznášet	vznášet	k5eAaImF	vznášet
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
lovit	lovit	k5eAaImF	lovit
kořist	kořist	k1gFnSc4	kořist
na	na	k7c6	na
listech	list	k1gInPc6	list
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Netopýrci	netopýrek	k1gMnPc1	netopýrek
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
horních	horní	k2eAgFnPc6d1	horní
částech	část	k1gFnPc6	část
vápencových	vápencový	k2eAgFnPc2d1	vápencová
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
strop	strop	k1gInSc4	strop
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
místa	místo	k1gNnPc1	místo
jim	on	k3xPp3gMnPc3	on
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
dobrou	dobrý	k2eAgFnSc4d1	dobrá
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
netopýrci	netopýrek	k1gMnPc1	netopýrek
neztrácejí	ztrácet	k5eNaImIp3nP	ztrácet
tělesné	tělesný	k2eAgNnSc4d1	tělesné
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tak	tak	k6eAd1	tak
drobná	drobný	k2eAgNnPc4d1	drobné
teplokrevná	teplokrevný	k2eAgNnPc4d1	teplokrevné
zvířata	zvíře	k1gNnPc4	zvíře
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Thajský	thajský	k2eAgMnSc1d1	thajský
biolog	biolog	k1gMnSc1	biolog
Kitti	Kitti	k1gNnSc2	Kitti
Thonglongya	Thonglongyum	k1gNnSc2	Thonglongyum
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
chytil	chytit	k5eAaPmAgMnS	chytit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
poblíž	poblíž	k7c2	poblíž
vodopádu	vodopád	k1gInSc2	vodopád
Sai	Sai	k1gMnSc2	Sai
Yok	Yok	k1gMnSc2	Yok
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
asi	asi	k9	asi
50	[number]	k4	50
netopýrů	netopýr	k1gMnPc2	netopýr
neznámého	známý	k2eNgInSc2d1	neznámý
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
exemplářů	exemplář	k1gInPc2	exemplář
poslal	poslat	k5eAaPmAgMnS	poslat
doktoru	doktor	k1gMnSc3	doktor
J.	J.	kA	J.
E.	E.	kA	E.
Hillovi	Hill	k1gMnSc3	Hill
z	z	k7c2	z
Britského	britský	k2eAgNnSc2d1	Britské
přírodovědného	přírodovědný	k2eAgNnSc2d1	Přírodovědné
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Kitti	Kitt	k1gMnPc1	Kitt
Thonglongya	Thonglongy	k1gInSc2	Thonglongy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastně	vlastně	k9	vlastně
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
netopýra	netopýr	k1gMnSc2	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úcty	úcta	k1gFnSc2	úcta
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
doktor	doktor	k1gMnSc1	doktor
Hill	Hill	k1gMnSc1	Hill
netopýra	netopýr	k1gMnSc4	netopýr
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Craseonycteris	Craseonycteris	k1gFnSc4	Craseonycteris
thonglongyai	thonglongya	k1gFnSc2	thonglongya
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
netopýrek	netopýrek	k1gMnSc1	netopýrek
thajský	thajský	k2eAgMnSc1d1	thajský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kitti	Kitť	k1gFnSc2	Kitť
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hog-nosed	Hogosed	k1gMnSc1	Hog-nosed
Bat	Bat	k1gMnSc7	Bat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
netopýrek	netopýrek	k1gMnSc1	netopýrek
thajský	thajský	k2eAgMnSc1d1	thajský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Craseonycteris	Craseonycteris	k1gFnSc2	Craseonycteris
thonglongyai	thonglongya	k1gFnSc2	thonglongya
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Netopýrek	netopýrek	k1gMnSc1	netopýrek
thajský	thajský	k2eAgMnSc1d1	thajský
na	na	k7c4	na
BioLib	BioLib	k1gInSc4	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
