<p>
<s>
Motocykl	motocykl	k1gInSc1	motocykl
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
motorka	motorka	k1gFnSc1	motorka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dvoukolový	dvoukolový	k2eAgInSc4d1	dvoukolový
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
poháněný	poháněný	k2eAgInSc4d1	poháněný
spalovacím	spalovací	k2eAgInSc7d1	spalovací
nebo	nebo	k8xC	nebo
elektrickým	elektrický	k2eAgInSc7d1	elektrický
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Kola	kolo	k1gNnPc1	kolo
jsou	být	k5eAaImIp3nP	být
umístěna	umístit	k5eAaPmNgNnP	umístit
za	za	k7c4	za
sebou	se	k3xPyFc7	se
a	a	k8xC	a
pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
motor	motor	k1gInSc1	motor
a	a	k8xC	a
převodovka	převodovka	k1gFnSc1	převodovka
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
motocyklu	motocykl	k1gInSc6	motocykl
obkročmo	obkročmo	k6eAd1	obkročmo
a	a	k8xC	a
směr	směr	k1gInSc1	směr
jízdy	jízda	k1gFnSc2	jízda
řídí	řídit	k5eAaImIp3nS	řídit
přenášením	přenášení	k1gNnSc7	přenášení
váhy	váha	k1gFnSc2	váha
(	(	kIx(	(
<g/>
naklánění	naklánění	k1gNnSc1	naklánění
motocyklu	motocykl	k1gInSc2	motocykl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
motocykl	motocykl	k1gInSc4	motocykl
poháněný	poháněný	k2eAgInSc4d1	poháněný
petrolejem	petrolej	k1gInSc7	petrolej
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gMnSc1	Daimler
a	a	k8xC	a
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Maybach	Maybach	k1gMnSc1	Maybach
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
motocyklu	motocykl	k1gInSc2	motocykl
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
motocykl	motocykl	k1gInSc1	motocykl
byl	být	k5eAaImAgInS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
poháněn	poháněn	k2eAgInSc4d1	poháněn
párou	pára	k1gFnSc7	pára
<g/>
.	.	kIx.	.
</s>
<s>
Motocykl	motocykl	k1gInSc1	motocykl
poháněný	poháněný	k2eAgInSc1d1	poháněný
benzínem	benzín	k1gInSc7	benzín
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Daimler	Daimler	k1gMnSc1	Daimler
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
vynálezce	vynálezce	k1gMnSc1	vynálezce
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
motocykly	motocykl	k1gInPc4	motocykl
nezajímal	zajímat	k5eNaImAgMnS	zajímat
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
pouze	pouze	k6eAd1	pouze
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
svůj	svůj	k3xOyFgInSc4	svůj
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
rozvoj	rozvoj	k1gInSc4	rozvoj
"	"	kIx"	"
<g/>
motorových	motorový	k2eAgFnPc2d1	motorová
dvoukolek	dvoukolka	k1gFnPc2	dvoukolka
<g/>
"	"	kIx"	"
velmi	velmi	k6eAd1	velmi
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
<g/>
.	.	kIx.	.
</s>
<s>
Sériovou	sériový	k2eAgFnSc4d1	sériová
výrobu	výroba	k1gFnSc4	výroba
zahájila	zahájit	k5eAaPmAgFnS	zahájit
německá	německý	k2eAgFnSc1d1	německá
firma	firma	k1gFnSc1	firma
Hildebrand	Hildebranda	k1gFnPc2	Hildebranda
&	&	k?	&
Wolfmüller	Wolfmüller	k1gMnSc1	Wolfmüller
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
český	český	k2eAgInSc4d1	český
motocykl	motocykl	k1gInSc4	motocykl
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
Václav	Václav	k1gMnSc1	Václav
Klement	Klement	k1gMnSc1	Klement
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
knihkupec	knihkupec	k1gMnSc1	knihkupec
<g/>
)	)	kIx)	)
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Laurin	Laurin	k1gInSc1	Laurin
(	(	kIx(	(
<g/>
mechanik	mechanik	k1gMnSc1	mechanik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnSc3	veřejnost
jej	on	k3xPp3gMnSc4	on
předvedli	předvést	k5eAaPmAgMnP	předvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
civilní	civilní	k2eAgFnSc1d1	civilní
výroba	výroba	k1gFnSc1	výroba
motocyklů	motocykl	k1gInPc2	motocykl
zastavila	zastavit	k5eAaPmAgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
hrál	hrát	k5eAaImAgInS	hrát
motocykl	motocykl	k1gInSc1	motocykl
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
armádě	armáda	k1gFnSc6	armáda
a	a	k8xC	a
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
strategii	strategie	k1gFnSc6	strategie
"	"	kIx"	"
<g/>
bleskové	bleskový	k2eAgFnSc2d1	blesková
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Blitzkrieg	Blitzkrieg	k1gInSc1	Blitzkrieg
<g/>
)	)	kIx)	)
s	s	k7c7	s
rychle	rychle	k6eAd1	rychle
pohyblivými	pohyblivý	k2eAgFnPc7d1	pohyblivá
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
civilní	civilní	k2eAgFnSc1d1	civilní
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
obnovena	obnovit	k5eAaPmNgFnS	obnovit
až	až	k9	až
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motocykly	motocykl	k1gInPc4	motocykl
může	moct	k5eAaImIp3nS	moct
pohánět	pohánět	k5eAaImF	pohánět
dvoutaktní	dvoutaktní	k2eAgInSc4d1	dvoutaktní
nebo	nebo	k8xC	nebo
čtyřtaktní	čtyřtaktní	k2eAgInSc4d1	čtyřtaktní
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
terminologii	terminologie	k1gFnSc6	terminologie
řadíme	řadit	k5eAaImIp1nP	řadit
motocykly	motocykl	k1gInPc4	motocykl
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
jednostopá	jednostopý	k2eAgNnPc4d1	jednostopé
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
raných	raný	k2eAgInPc2d1	raný
vynálezů	vynález	k1gInPc2	vynález
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Jednostopé	jednostopý	k2eAgNnSc4d1	jednostopé
motorové	motorový	k2eAgNnSc4d1	motorové
vozidlo	vozidlo	k1gNnSc4	vozidlo
==	==	k?	==
</s>
</p>
<p>
<s>
Jednostopá	jednostopý	k2eAgNnPc1d1	jednostopé
vozidla	vozidlo	k1gNnPc1	vozidlo
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
vozidla	vozidlo	k1gNnPc1	vozidlo
se	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
koly	kolo	k1gNnPc7	kolo
uspořádanými	uspořádaný	k2eAgInPc7d1	uspořádaný
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vozidla	vozidlo	k1gNnPc1	vozidlo
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
postranní	postranní	k2eAgInSc4d1	postranní
vozík	vozík	k1gInSc4	vozík
a	a	k8xC	a
lze	lze	k6eAd1	lze
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
táhnout	táhnout	k5eAaImF	táhnout
přívěs	přívěs	k1gInSc4	přívěs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moped	moped	k1gInSc4	moped
nebo	nebo	k8xC	nebo
jízdní	jízdní	k2eAgNnSc4d1	jízdní
kolo	kolo	k1gNnSc4	kolo
s	s	k7c7	s
pomocným	pomocný	k2eAgInSc7d1	pomocný
motorkem	motorek	k1gInSc7	motorek
===	===	k?	===
</s>
</p>
<p>
<s>
Moped	moped	k1gInSc4	moped
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
kombinaci	kombinace	k1gFnSc4	kombinace
motocyklu	motocykl	k1gInSc2	motocykl
a	a	k8xC	a
jízdního	jízdní	k2eAgNnSc2d1	jízdní
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
vybaven	vybavit	k5eAaPmNgInS	vybavit
pedály	pedál	k1gInPc7	pedál
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
objem	objem	k1gInSc4	objem
motoru	motor	k1gInSc2	motor
do	do	k7c2	do
50	[number]	k4	50
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
City-bike	Cityike	k6eAd1	City-bike
</s>
</p>
<p>
<s>
Fun-bike	Funike	k6eAd1	Fun-bike
</s>
</p>
<p>
<s>
Naked-bike	Nakedike	k6eAd1	Naked-bike
</s>
</p>
<p>
<s>
Enduro-bike	Enduroike	k6eAd1	Enduro-bike
</s>
</p>
<p>
<s>
===	===	k?	===
Malý	malý	k2eAgInSc1d1	malý
motocykl	motocykl	k1gInSc1	motocykl
===	===	k?	===
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgInPc1d1	Malé
motocykly	motocykl	k1gInPc1	motocykl
tvoří	tvořit	k5eAaImIp3nP	tvořit
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
mopedem	moped	k1gInSc7	moped
a	a	k8xC	a
ostatními	ostatní	k2eAgInPc7d1	ostatní
silničními	silniční	k2eAgInPc7d1	silniční
typy	typ	k1gInPc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
motor	motor	k1gInSc4	motor
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
50	[number]	k4	50
až	až	k8xS	až
125	[number]	k4	125
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
malých	malý	k2eAgInPc2d1	malý
motocyklů	motocykl	k1gInPc2	motocykl
a	a	k8xC	a
mopedů	moped	k1gInPc2	moped
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
národními	národní	k2eAgInPc7d1	národní
technickými	technický	k2eAgInPc7d1	technický
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skútr	skútr	k1gInSc4	skútr
===	===	k?	===
</s>
</p>
<p>
<s>
Skútr	skútr	k1gInSc1	skútr
je	být	k5eAaImIp3nS	být
vozidlo	vozidlo	k1gNnSc1	vozidlo
podobné	podobný	k2eAgNnSc1d1	podobné
motocyklu	motocykl	k1gInSc2	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
obvykle	obvykle	k6eAd1	obvykle
malými	malá	k1gFnPc7	malá
koly	kola	k1gFnSc2	kola
<g/>
,	,	kIx,	,
nízkoobjemovým	nízkoobjemův	k2eAgInSc7d1	nízkoobjemův
motorem	motor	k1gInSc7	motor
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
50	[number]	k4	50
-	-	kIx~	-
125	[number]	k4	125
cm3	cm3	k4	cm3
<g/>
)	)	kIx)	)
a	a	k8xC	a
částečnou	částečný	k2eAgFnSc7d1	částečná
kapotáží	kapotáž	k1gFnSc7	kapotáž
s	s	k7c7	s
podlážkou	podlážka	k1gFnSc7	podlážka
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
rámu	rám	k1gInSc2	rám
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
nesedí	sedit	k5eNaImIp3nS	sedit
obkročmo	obkročmo	k6eAd1	obkročmo
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
snožmo	snožmo	k6eAd1	snožmo
jako	jako	k9	jako
na	na	k7c4	na
židli	židle	k1gFnSc4	židle
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
bývá	bývat	k5eAaImIp3nS	bývat
vybaven	vybavit	k5eAaPmNgInS	vybavit
automatickou	automatický	k2eAgFnSc7d1	automatická
převodovkou	převodovka	k1gFnSc7	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
-	-	kIx~	-
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc4d1	horní
třídu	třída	k1gFnSc4	třída
skútrů	skútr	k1gInPc2	skútr
tvoří	tvořit	k5eAaImIp3nS	tvořit
velké	velký	k2eAgInPc4d1	velký
cestovní	cestovní	k2eAgInPc4d1	cestovní
skútry	skútr	k1gInPc4	skútr
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
motoru	motor	k1gInSc2	motor
250	[number]	k4	250
až	až	k9	až
850	[number]	k4	850
cm3	cm3	k4	cm3
a	a	k8xC	a
s	s	k7c7	s
výkony	výkon	k1gInPc7	výkon
20	[number]	k4	20
až	až	k9	až
50	[number]	k4	50
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Motocykl	motocykl	k1gInSc1	motocykl
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
jízdu	jízda	k1gFnSc4	jízda
po	po	k7c6	po
silnicích	silnice	k1gFnPc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
kolen	koleno	k1gNnPc2	koleno
má	mít	k5eAaImIp3nS	mít
palivovou	palivový	k2eAgFnSc4d1	palivová
nádrž	nádrž	k1gFnSc4	nádrž
a	a	k8xC	a
motor	motor	k1gInSc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podepření	podepření	k1gNnSc4	podepření
nohou	noha	k1gFnSc7	noha
slouží	sloužit	k5eAaImIp3nP	sloužit
stupačky	stupačka	k1gFnPc1	stupačka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uvedené	uvedený	k2eAgInPc4d1	uvedený
motocykly	motocykl	k1gInPc4	motocykl
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
nástupce	nástupce	k1gMnSc4	nástupce
či	či	k8xC	či
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Cestovní	cestovní	k2eAgInSc4d1	cestovní
motocykl	motocykl	k1gInSc4	motocykl
====	====	k?	====
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
pohodlný	pohodlný	k2eAgInSc1d1	pohodlný
motocykl	motocykl	k1gInSc1	motocykl
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
nádrží	nádrž	k1gFnSc7	nádrž
a	a	k8xC	a
často	často	k6eAd1	často
s	s	k7c7	s
kapotáží	kapotáž	k1gFnSc7	kapotáž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
posádku	posádka	k1gFnSc4	posádka
proti	proti	k7c3	proti
větru	vítr	k1gInSc3	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
jízdy	jízda	k1gFnPc4	jízda
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
typy	typ	k1gInPc1	typ
bývají	bývat	k5eAaImIp3nP	bývat
luxusně	luxusně	k6eAd1	luxusně
vybaveny	vybavit	k5eAaPmNgInP	vybavit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
připomínají	připomínat	k5eAaImIp3nP	připomínat
spíše	spíše	k9	spíše
automobil	automobil	k1gInSc4	automobil
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInPc1d1	cestovní
motocykly	motocykl	k1gInPc1	motocykl
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
postranním	postranní	k2eAgInSc7d1	postranní
vozíkem	vozík	k1gInSc7	vozík
(	(	kIx(	(
<g/>
sajdkára	sajdkár	k1gMnSc2	sajdkár
-	-	kIx~	-
sidecar	sidecar	k1gInSc1	sidecar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Naháč	naháč	k1gMnSc1	naháč
(	(	kIx(	(
<g/>
Naked	Naked	k1gMnSc1	Naked
bike	bikat	k5eAaPmIp3nS	bikat
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
motocykl	motocykl	k1gInSc4	motocykl
bez	bez	k7c2	bez
kapotáže	kapotáž	k1gFnSc2	kapotáž
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
aerodynamickou	aerodynamický	k2eAgFnSc7d1	aerodynamická
kapotáží	kapotáž	k1gFnSc7	kapotáž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
supersportovních	supersportovní	k2eAgInPc2d1	supersportovní
motocyklů	motocykl	k1gInPc2	motocykl
liší	lišit	k5eAaImIp3nS	lišit
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
ergonomií	ergonomie	k1gFnSc7	ergonomie
<g/>
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gInSc1	jezdec
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
vzpřímenější	vzpřímený	k2eAgFnSc4d2	vzpřímenější
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pohodlnější	pohodlný	k2eAgFnSc1d2	pohodlnější
i	i	k9	i
na	na	k7c4	na
delší	dlouhý	k2eAgFnPc4d2	delší
trasy	trasa	k1gFnPc4	trasa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
především	především	k6eAd1	především
vyšším	vysoký	k2eAgNnSc7d2	vyšší
umístěním	umístění	k1gNnSc7	umístění
řidítek	řidítko	k1gNnPc2	řidítko
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
bývá	bývat	k5eAaImIp3nS	bývat
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xS	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
typů	typ	k1gInPc2	typ
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
řadové	řadový	k2eAgInPc1d1	řadový
čtyřválcové	čtyřválcový	k2eAgInPc1d1	čtyřválcový
motory	motor	k1gInPc1	motor
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dvouválcové	dvouválcový	k2eAgInPc4d1	dvouválcový
řadové	řadový	k2eAgInPc4d1	řadový
a	a	k8xC	a
vidlicové	vidlicový	k2eAgInPc4d1	vidlicový
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgNnSc1d1	časté
je	být	k5eAaImIp3nS	být
přejímání	přejímání	k1gNnSc1	přejímání
motorů	motor	k1gInPc2	motor
ze	z	k7c2	z
supersportovních	supersportovní	k2eAgInPc2d1	supersportovní
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
sníží	snížit	k5eAaPmIp3nS	snížit
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
vyššího	vysoký	k2eAgInSc2d2	vyšší
kroutícího	kroutící	k2eAgInSc2d1	kroutící
momentu	moment	k1gInSc2	moment
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
i	i	k9	i
zkrátí	zkrátit	k5eAaPmIp3nP	zkrátit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
převodové	převodový	k2eAgInPc4d1	převodový
stupně	stupeň	k1gInPc4	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Enduro	Endura	k1gFnSc5	Endura
motocykl	motocykl	k1gInSc4	motocykl
====	====	k?	====
</s>
</p>
<p>
<s>
Motocykl	motocykl	k1gInSc1	motocykl
univerzální	univerzální	k2eAgFnSc2d1	univerzální
konstrukce	konstrukce	k1gFnSc2	konstrukce
kompromisně	kompromisně	k6eAd1	kompromisně
sestaven	sestavit	k5eAaPmNgInS	sestavit
na	na	k7c4	na
jízdu	jízda	k1gFnSc4	jízda
po	po	k7c6	po
všech	všecek	k3xTgInPc6	všecek
typech	typ	k1gInPc6	typ
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Enduro	Endura	k1gFnSc5	Endura
závody	závod	k1gInPc1	závod
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
automobilové	automobilový	k2eAgFnSc2d1	automobilová
rallye	rallye	k1gFnSc2	rallye
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
odněkud-někam	odněkuděkam	k6eAd1	odněkud-někam
na	na	k7c4	na
etapy	etapa	k1gFnPc4	etapa
putuje	putovat	k5eAaImIp3nS	putovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trať	trať	k1gFnSc1	trať
vede	vést	k5eAaImIp3nS	vést
rozmanitými	rozmanitý	k2eAgInPc7d1	rozmanitý
terény	terén	k1gInPc7	terén
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jak	jak	k6eAd1	jak
technické	technický	k2eAgFnPc4d1	technická
tak	tak	k8xC	tak
rychlostní	rychlostní	k2eAgFnPc4d1	rychlostní
pasáže	pasáž	k1gFnPc4	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Motocykly	motocykl	k1gInPc1	motocykl
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
vybaveny	vybavit	k5eAaPmNgInP	vybavit
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
stavěny	stavit	k5eAaImNgInP	stavit
na	na	k7c4	na
výdrž	výdrž	k1gFnSc4	výdrž
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Terénní	terénní	k2eAgInSc4d1	terénní
motocykl	motocykl	k1gInSc4	motocykl
====	====	k?	====
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
též	též	k9	též
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
kros	kros	k1gInSc1	kros
(	(	kIx(	(
<g/>
cross	cross	k1gInSc1	cross
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukcí	konstrukce	k1gFnSc7	konstrukce
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
zdolávání	zdolávání	k1gNnSc4	zdolávání
náročného	náročný	k2eAgInSc2d1	náročný
terénu	terén	k1gInSc2	terén
mimo	mimo	k7c4	mimo
zpevněné	zpevněný	k2eAgFnPc4d1	zpevněná
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
konstrukce	konstrukce	k1gFnPc4	konstrukce
pro	pro	k7c4	pro
amatérské	amatérský	k2eAgNnSc4d1	amatérské
použití	použití	k1gNnSc4	použití
a	a	k8xC	a
pro	pro	k7c4	pro
závody	závod	k1gInPc4	závod
/	/	kIx~	/
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kros	kros	k1gInSc1	kros
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
uzavřených	uzavřený	k2eAgInPc6d1	uzavřený
okruzích	okruh	k1gInPc6	okruh
v	v	k7c6	v
určeném	určený	k2eAgInSc6d1	určený
počtu	počet	k1gInSc6	počet
kol	kolo	k1gNnPc2	kolo
či	či	k8xC	či
rozjezdů	rozjezd	k1gInPc2	rozjezd
<g/>
.	.	kIx.	.
</s>
<s>
Motocykly	motocykl	k1gInPc1	motocykl
nemají	mít	k5eNaImIp3nP	mít
osvětlení	osvětlení	k1gNnSc4	osvětlení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stavěny	stavit	k5eAaImNgFnP	stavit
na	na	k7c4	na
vysoký	vysoký	k2eAgInSc4d1	vysoký
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
krátkodobé	krátkodobý	k2eAgNnSc4d1	krátkodobé
zatížení	zatížení	k1gNnSc4	zatížení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Cruiser	Cruisero	k1gNnPc2	Cruisero
====	====	k?	====
</s>
</p>
<p>
<s>
Mohutný	mohutný	k2eAgInSc1d1	mohutný
motocykl	motocykl	k1gInSc1	motocykl
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
amerických	americký	k2eAgInPc2d1	americký
strojů	stroj	k1gInPc2	stroj
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
vybaven	vybavit	k5eAaPmNgInS	vybavit
příčně	příčně	k6eAd1	příčně
uloženým	uložený	k2eAgInSc7d1	uložený
vidlicovým	vidlicový	k2eAgInSc7d1	vidlicový
dvouválcem	dvouválec	k1gInSc7	dvouválec
se	s	k7c7	s
vzduchovým	vzduchový	k2eAgNnSc7d1	vzduchové
chlazením	chlazení	k1gNnSc7	chlazení
(	(	kIx(	(
<g/>
výrazné	výrazný	k2eAgNnSc1d1	výrazné
žebrování	žebrování	k1gNnSc1	žebrování
<g/>
)	)	kIx)	)
o	o	k7c6	o
zdvihovém	zdvihový	k2eAgInSc6d1	zdvihový
objemu	objem	k1gInSc6	objem
od	od	k7c2	od
1	[number]	k4	1
000	[number]	k4	000
cm3	cm3	k4	cm3
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Ctí	ctít	k5eAaImIp3nS	ctít
tradici	tradice	k1gFnSc4	tradice
koncepce	koncepce	k1gFnSc2	koncepce
motocyklu	motocykl	k1gInSc2	motocykl
se	s	k7c7	s
shodnými	shodný	k2eAgInPc7d1	shodný
rozměry	rozměr	k1gInPc7	rozměr
předního	přední	k2eAgNnSc2d1	přední
a	a	k8xC	a
zadního	zadní	k2eAgNnSc2d1	zadní
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
konstrukčně	konstrukčně	k6eAd1	konstrukčně
se	se	k3xPyFc4	se
od	od	k7c2	od
vzhledu	vzhled	k1gInSc2	vzhled
původních	původní	k2eAgInPc2d1	původní
motocyklů	motocykl	k1gInPc2	motocykl
příliš	příliš	k6eAd1	příliš
neodlišuje	odlišovat	k5eNaImIp3nS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgInPc1	tento
motocykly	motocykl	k1gInPc1	motocykl
spoustu	spoustu	k6eAd1	spoustu
chromovaných	chromovaný	k2eAgFnPc2d1	chromovaná
součástí	součást	k1gFnPc2	součást
<g/>
,	,	kIx,	,
motory	motor	k1gInPc1	motor
laděné	laděný	k2eAgInPc1d1	laděný
do	do	k7c2	do
nízkých	nízký	k2eAgFnPc2d1	nízká
otáček	otáčka	k1gFnPc2	otáčka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
uzpůsobeny	uzpůsoben	k2eAgFnPc1d1	uzpůsobena
pro	pro	k7c4	pro
pohodlné	pohodlný	k2eAgNnSc4d1	pohodlné
a	a	k8xC	a
stylové	stylový	k2eAgNnSc4d1	stylové
cestování	cestování	k1gNnSc4	cestování
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
trasy	trasa	k1gFnPc4	trasa
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Chopper	Choppero	k1gNnPc2	Choppero
====	====	k?	====
</s>
</p>
<p>
<s>
Motocykl	motocykl	k1gInSc1	motocykl
vycházející	vycházející	k2eAgInSc1d1	vycházející
z	z	k7c2	z
cruiseru	cruiser	k1gInSc2	cruiser
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
výraznými	výrazný	k2eAgFnPc7d1	výrazná
vzhledovými	vzhledový	k2eAgFnPc7d1	vzhledová
úpravami	úprava	k1gFnPc7	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnější	podstatný	k2eAgInPc1d2	podstatnější
než	než	k8xS	než
jízdní	jízdní	k2eAgInPc4d1	jízdní
výkony	výkon	k1gInPc4	výkon
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
dvouválcovým	dvouválcový	k2eAgInSc7d1	dvouválcový
vidlicovým	vidlicový	k2eAgInSc7d1	vidlicový
motorem	motor	k1gInSc7	motor
o	o	k7c6	o
zdvihovém	zdvihový	k2eAgInSc6d1	zdvihový
objemu	objem	k1gInSc6	objem
kolem	kolem	k7c2	kolem
1	[number]	k4	1
000	[number]	k4	000
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
byl	být	k5eAaImAgInS	být
chopper	chopper	k1gInSc1	chopper
osekaný	osekaný	k2eAgInSc1d1	osekaný
(	(	kIx(	(
<g/>
chopped	chopped	k1gInSc1	chopped
<g/>
)	)	kIx)	)
motocykl	motocykl	k1gInSc1	motocykl
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nepodstatných	podstatný	k2eNgFnPc2d1	nepodstatná
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
<s>
Choppery	Choppera	k1gFnPc1	Choppera
se	se	k3xPyFc4	se
proslavily	proslavit	k5eAaPmAgFnP	proslavit
především	především	k6eAd1	především
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Bezstarostná	bezstarostný	k2eAgFnSc1d1	bezstarostná
jízda	jízda	k1gFnSc1	jízda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
choppery	choppera	k1gFnPc1	choppera
staly	stát	k5eAaPmAgFnP	stát
exkluzivním	exkluzivní	k2eAgNnSc7d1	exkluzivní
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
řada	řada	k1gFnSc1	řada
doplňků	doplněk	k1gInPc2	doplněk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
splývavé	splývavý	k2eAgFnSc2d1	splývavá
zádi	záď	k1gFnSc2	záď
cruiseru	cruiser	k1gInSc2	cruiser
má	mít	k5eAaImIp3nS	mít
správný	správný	k2eAgInSc1d1	správný
chopper	chopper	k1gInSc1	chopper
zadní	zadní	k2eAgInSc1d1	zadní
blatník	blatník	k1gInSc4	blatník
jakoby	jakoby	k8xS	jakoby
ustřižený	ustřižený	k2eAgInSc4d1	ustřižený
a	a	k8xC	a
zvednutý	zvednutý	k2eAgInSc4d1	zvednutý
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
<g/>
,	,	kIx,	,
řídítka	řídítka	k1gNnPc1	řídítka
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
"	"	kIx"	"
<g/>
vlaštovky	vlaštovka	k1gFnPc1	vlaštovka
<g/>
"	"	kIx"	"
vytažené	vytažený	k2eAgFnPc1d1	vytažená
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c4	nad
jezdcova	jezdcův	k2eAgNnPc4d1	jezdcovo
ramena	rameno	k1gNnPc4	rameno
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gMnSc4	on
nutí	nutit	k5eAaImIp3nS	nutit
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
rovnou	rovný	k2eAgFnSc7d1	rovná
tyčí	tyč	k1gFnSc7	tyč
být	být	k5eAaImF	být
v	v	k7c6	v
předklonu	předklon	k1gInSc6	předklon
a	a	k8xC	a
s	s	k7c7	s
roztaženýma	roztažený	k2eAgFnPc7d1	roztažená
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chopperu	chopper	k1gInSc6	chopper
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jezdí	jezdit	k5eAaImIp3nP	jezdit
s	s	k7c7	s
takzvanými	takzvaný	k2eAgInPc7d1	takzvaný
highway	highway	k1gInPc7	highway
předkopy	předkopa	k1gFnSc2	předkopa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
systém	systém	k1gInSc4	systém
stupaček	stupačka	k1gFnPc2	stupačka
posunutých	posunutý	k2eAgFnPc2d1	posunutá
daleko	daleko	k6eAd1	daleko
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
úpravu	úprava	k1gFnSc4	úprava
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přepákování	přepákování	k1gNnSc1	přepákování
pedálů	pedál	k1gInPc2	pedál
řazení	řazení	k1gNnSc2	řazení
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
brzdy	brzda	k1gFnSc2	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gInSc1	jezdec
pak	pak	k6eAd1	pak
jede	jet	k5eAaImIp3nS	jet
na	na	k7c6	na
motocyklu	motocykl	k1gInSc6	motocykl
s	s	k7c7	s
pohodlně	pohodlně	k6eAd1	pohodlně
roztaženýma	roztažený	k2eAgFnPc7d1	roztažená
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Scrambler	Scrambler	k1gInSc4	Scrambler
====	====	k?	====
</s>
</p>
<p>
<s>
Scrambler	Scrambler	k1gInSc1	Scrambler
je	být	k5eAaImIp3nS	být
stroj	stroj	k1gInSc1	stroj
vycházející	vycházející	k2eAgInSc1d1	vycházející
ze	z	k7c2	z
silničního	silniční	k2eAgInSc2d1	silniční
motocyklu	motocykl	k1gInSc2	motocykl
klasického	klasický	k2eAgNnSc2d1	klasické
<g/>
/	/	kIx~	/
<g/>
retro	retro	k1gNnSc2	retro
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
s	s	k7c7	s
potenciálem	potenciál	k1gInSc7	potenciál
jízdy	jízda	k1gFnSc2	jízda
i	i	k8xC	i
v	v	k7c6	v
lehčím	lehký	k2eAgInSc6d2	lehčí
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Posed	posed	k1gInSc1	posed
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgFnPc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
naháčů	naháč	k1gInPc2	naháč
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
zdvih	zdvih	k1gInSc4	zdvih
odpružení	odpružení	k1gNnSc2	odpružení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
scramblery	scrambler	k1gMnPc4	scrambler
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
typická	typický	k2eAgNnPc4d1	typické
širší	široký	k2eAgNnPc4d2	širší
a	a	k8xC	a
vyšší	vysoký	k2eAgNnPc4d2	vyšší
řídítka	řídítka	k1gNnPc4	řídítka
a	a	k8xC	a
špalkové	špalkový	k2eAgFnPc4d1	Špalková
pneumatiky	pneumatika	k1gFnPc4	pneumatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Závodní	závodní	k2eAgInSc1d1	závodní
silniční	silniční	k2eAgInSc1d1	silniční
motocykl	motocykl	k1gInSc1	motocykl
====	====	k?	====
</s>
</p>
<p>
<s>
Silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
aerodynamicky	aerodynamicky	k6eAd1	aerodynamicky
tvarovaný	tvarovaný	k2eAgInSc1d1	tvarovaný
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc1d1	určený
především	především	k9	především
k	k	k7c3	k
dosahování	dosahování	k1gNnSc3	dosahování
vysokých	vysoký	k2eAgFnPc2d1	vysoká
rychlostí	rychlost	k1gFnPc2	rychlost
na	na	k7c6	na
závodních	závodní	k2eAgInPc6d1	závodní
okruzích	okruh	k1gInPc6	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgInPc1d1	silniční
závodní	závodní	k2eAgInPc1d1	závodní
motocykly	motocykl	k1gInPc1	motocykl
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
podle	podle	k7c2	podle
objemu	objem	k1gInSc2	objem
motoru	motor	k1gInSc2	motor
mezi	mezi	k7c7	mezi
50	[number]	k4	50
a	a	k8xC	a
1	[number]	k4	1
300	[number]	k4	300
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Silnější	silný	k2eAgMnPc1d2	silnější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
rychlostí	rychlost	k1gFnSc7	rychlost
přes	přes	k7c4	přes
400	[number]	k4	400
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
====	====	k?	====
Plochodrážní	plochodrážní	k2eAgInSc4d1	plochodrážní
motocykl	motocykl	k1gInSc4	motocykl
====	====	k?	====
</s>
</p>
<p>
<s>
Specializovaný	specializovaný	k2eAgInSc1d1	specializovaný
stroj	stroj	k1gInSc1	stroj
pro	pro	k7c4	pro
závody	závod	k1gInPc4	závod
na	na	k7c4	na
400	[number]	k4	400
m	m	kA	m
škvárovém	škvárový	k2eAgInSc6d1	škvárový
oválu	ovál	k1gInSc6	ovál
<g/>
,	,	kIx,	,
1	[number]	k4	1
000	[number]	k4	000
m	m	kA	m
pískových	pískový	k2eAgFnPc6d1	písková
dráhách	dráha	k1gFnPc6	dráha
<g/>
,	,	kIx,	,
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
nebo	nebo	k8xC	nebo
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
přímý	přímý	k2eAgInSc4d1	přímý
záběr	záběr	k1gInSc4	záběr
(	(	kIx(	(
<g/>
dvourychlostní	dvourychlostní	k2eAgFnSc1d1	dvourychlostní
převodovka	převodovka	k1gFnSc1	převodovka
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
závody	závod	k1gInPc4	závod
na	na	k7c6	na
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
a	a	k8xC	a
na	na	k7c6	na
ledových	ledový	k2eAgFnPc6d1	ledová
dráhách	dráha	k1gFnPc6	dráha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
je	být	k5eAaImIp3nS	být
uzpůsobená	uzpůsobený	k2eAgFnSc1d1	uzpůsobená
pro	pro	k7c4	pro
jízdu	jízda	k1gFnSc4	jízda
smykem	smyk	k1gInSc7	smyk
v	v	k7c6	v
levotočivých	levotočivý	k2eAgFnPc6d1	levotočivá
zatáčkách	zatáčka	k1gFnPc6	zatáčka
<g/>
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
alkoholové	alkoholový	k2eAgNnSc4d1	alkoholové
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Technickými	technický	k2eAgInPc7d1	technický
předpisy	předpis	k1gInPc7	předpis
je	být	k5eAaImIp3nS	být
omezeno	omezen	k2eAgNnSc1d1	omezeno
používání	používání	k1gNnSc1	používání
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Technické	technický	k2eAgInPc1d1	technický
parametry	parametr	k1gInPc1	parametr
plochodrážních	plochodrážní	k2eAgInPc2d1	plochodrážní
strojů	stroj	k1gInPc2	stroj
</s>
</p>
<p>
<s>
motocykl	motocykl	k1gInSc1	motocykl
s	s	k7c7	s
jednoválcovým	jednoválcový	k2eAgInSc7d1	jednoválcový
čtyřventilovým	čtyřventilový	k2eAgInSc7d1	čtyřventilový
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ledovou	ledový	k2eAgFnSc4d1	ledová
plochou	plochý	k2eAgFnSc4d1	plochá
dráhu	dráha	k1gFnSc4	dráha
pouze	pouze	k6eAd1	pouze
dvouventilový	dvouventilový	k2eAgInSc1d1	dvouventilový
motor	motor	k1gInSc1	motor
</s>
</p>
<p>
<s>
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
dovolenou	dovolený	k2eAgFnSc7d1	dovolená
hmotností	hmotnost	k1gFnSc7	hmotnost
80	[number]	k4	80
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
motocykl	motocykl	k1gInSc1	motocykl
není	být	k5eNaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
brzdou	brzda	k1gFnSc7	brzda
<g/>
.	.	kIx.	.
<g/>
Motor	motor	k1gInSc1	motor
</s>
</p>
<p>
<s>
čtyřventilový	čtyřventilový	k2eAgInSc4d1	čtyřventilový
jednoválec	jednoválec	k1gInSc4	jednoválec
SOHC	SOHC	kA	SOHC
chlazený	chlazený	k2eAgInSc4d1	chlazený
vzduchem	vzduch	k1gInSc7	vzduch
</s>
</p>
<p>
<s>
obsah	obsah	k1gInSc1	obsah
493	[number]	k4	493
cm3	cm3	k4	cm3
</s>
</p>
<p>
<s>
maximální	maximální	k2eAgInSc4d1	maximální
výkon	výkon	k1gInSc4	výkon
48	[number]	k4	48
-	-	kIx~	-
51	[number]	k4	51
kW	kW	kA	kW
</s>
</p>
<p>
<s>
vrtání	vrtání	k1gNnSc4	vrtání
x	x	k?	x
zdvih	zdvih	k1gInSc1	zdvih
90	[number]	k4	90
x	x	k?	x
77,6	[number]	k4	77,6
mm	mm	kA	mm
</s>
</p>
<p>
<s>
kompresní	kompresní	k2eAgInSc1d1	kompresní
poměr	poměr	k1gInSc1	poměr
13,5	[number]	k4	13,5
-	-	kIx~	-
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
</s>
</p>
<p>
<s>
maximální	maximální	k2eAgFnPc4d1	maximální
otáčky	otáčka	k1gFnPc4	otáčka
11	[number]	k4	11
000	[number]	k4	000
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
</s>
</p>
<p>
<s>
palivo	palivo	k1gNnSc1	palivo
methanol	methanol	k1gInSc1	methanol
</s>
</p>
<p>
<s>
hmotnost	hmotnost	k1gFnSc1	hmotnost
28-30	[number]	k4	28-30
kg	kg	kA	kg
</s>
</p>
<p>
<s>
====	====	k?	====
Kaskadérský	kaskadérský	k2eAgInSc4d1	kaskadérský
motocykl	motocykl	k1gInSc4	motocykl
====	====	k?	====
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
street	street	k1gInSc1	street
<g/>
.	.	kIx.	.
</s>
<s>
Motocykl	motocykl	k1gInSc1	motocykl
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
extrémní	extrémní	k2eAgInPc4d1	extrémní
až	až	k8xS	až
kaskadérské	kaskadérský	k2eAgInPc4d1	kaskadérský
jízdní	jízdní	k2eAgInPc4d1	jízdní
úkony	úkon	k1gInPc4	úkon
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
jízda	jízda	k1gFnSc1	jízda
po	po	k7c6	po
zadním	zadní	k2eAgMnSc6d1	zadní
<g/>
/	/	kIx~	/
<g/>
předním	přední	k2eAgNnSc6d1	přední
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
drifting	drifting	k1gInSc4	drifting
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
prvky	prvek	k1gInPc4	prvek
náročné	náročný	k2eAgInPc4d1	náročný
na	na	k7c4	na
sehranost	sehranost	k1gFnSc4	sehranost
jezdce	jezdec	k1gMnSc2	jezdec
se	s	k7c7	s
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
motocykl	motocykl	k1gInSc1	motocykl
co	co	k9	co
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
(	(	kIx(	(
<g/>
odstrojeny	odstrojen	k2eAgInPc4d1	odstrojen
kapoty	kapot	k1gInPc4	kapot
<g/>
,	,	kIx,	,
tuningové	tuningový	k2eAgInPc4d1	tuningový
odlehčené	odlehčený	k2eAgInPc4d1	odlehčený
díly	díl	k1gInPc4	díl
<g/>
)	)	kIx)	)
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgMnS	mít
co	co	k9	co
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
kroutící	kroutící	k2eAgInSc4d1	kroutící
moment	moment	k1gInSc4	moment
(	(	kIx(	(
<g/>
-tuningové	uningový	k2eAgFnPc4d1	-tuningový
úpravy	úprava	k1gFnPc4	úprava
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
výkonnostní	výkonnostní	k2eAgInPc4d1	výkonnostní
kity	kit	k1gInPc4	kit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konstrukční	konstrukční	k2eAgFnPc1d1	konstrukční
stavby	stavba	k1gFnPc1	stavba
nejvíce	hodně	k6eAd3	hodně
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
kategorie	kategorie	k1gFnSc2	kategorie
závodních	závodní	k2eAgInPc2d1	závodní
motocyklů	motocykl	k1gInPc2	motocykl
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravidlem	pravidlem	k6eAd1	pravidlem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Dragster	Dragstrum	k1gNnPc2	Dragstrum
====	====	k?	====
</s>
</p>
<p>
<s>
Specializovaný	specializovaný	k2eAgInSc1d1	specializovaný
stroj	stroj	k1gInSc1	stroj
na	na	k7c4	na
závody	závod	k1gInPc4	závod
ve	v	k7c6	v
zrychlení	zrychlení	k1gNnSc6	zrychlení
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
402	[number]	k4	402
m	m	kA	m
(	(	kIx(	(
<g/>
čtvrt	čtvrt	k1gFnSc4	čtvrt
míle	míle	k1gFnSc2	míle
<g/>
)	)	kIx)	)
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Motocykl	motocykl	k1gInSc1	motocykl
se	s	k7c7	s
sajdkárou	sajdkára	k1gFnSc7	sajdkára
(	(	kIx(	(
<g/>
postranním	postranní	k2eAgInSc7d1	postranní
vozíkem	vozík	k1gInSc7	vozík
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Méně	málo	k6eAd2	málo
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
motocyklu	motocykl	k1gInSc3	motocykl
připojen	připojen	k2eAgInSc1d1	připojen
postranní	postranní	k2eAgInSc1d1	postranní
vozík	vozík	k1gInSc1	vozík
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
nebo	nebo	k8xC	nebo
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
poháněna	poháněn	k2eAgFnSc1d1	poháněna
obě	dva	k4xCgNnPc4	dva
zadní	zadní	k2eAgNnPc4d1	zadní
kola	kolo	k1gNnPc4	kolo
(	(	kIx(	(
<g/>
motocyklu	motocykl	k1gInSc2	motocykl
i	i	k8xC	i
vozíku	vozík	k1gInSc2	vozík
<g/>
)	)	kIx)	)
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
výhoda	výhoda	k1gFnSc1	výhoda
v	v	k7c6	v
obtížném	obtížný	k2eAgInSc6d1	obtížný
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Motocykl	motocykl	k1gInSc1	motocykl
s	s	k7c7	s
postranním	postranní	k2eAgInSc7d1	postranní
vozíkem	vozík	k1gInSc7	vozík
tvoří	tvořit	k5eAaImIp3nS	tvořit
jedno	jeden	k4xCgNnSc1	jeden
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tříkolku	tříkolka	k1gFnSc4	tříkolka
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
jej	on	k3xPp3gInSc4	on
tedy	tedy	k9	tedy
v	v	k7c6	v
silničním	silniční	k2eAgInSc6d1	silniční
provozu	provoz	k1gInSc6	provoz
používat	používat	k5eAaImF	používat
střídavě	střídavě	k6eAd1	střídavě
se	s	k7c7	s
sajdkárou	sajdkára	k1gFnSc7	sajdkára
anebo	anebo	k8xC	anebo
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kapotovaný	Kapotovaný	k2eAgInSc4d1	Kapotovaný
motocykl	motocykl	k1gInSc4	motocykl
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
kapotovaný	kapotovaný	k2eAgInSc1d1	kapotovaný
motocykl	motocykl	k1gInSc1	motocykl
dálník	dálník	k1gInSc1	dálník
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
ekomobil	ekomobit	k5eAaPmAgMnS	ekomobit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dopravním	dopravní	k2eAgInSc7d1	dopravní
prostředkem	prostředek	k1gInSc7	prostředek
stojícím	stojící	k2eAgInSc7d1	stojící
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
mezi	mezi	k7c7	mezi
motocyklem	motocykl	k1gInSc7	motocykl
a	a	k8xC	a
automobilem	automobil	k1gInSc7	automobil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jednostopé	jednostopý	k2eAgNnSc1d1	jednostopé
přípojné	přípojný	k2eAgNnSc1d1	přípojné
vozidlo	vozidlo	k1gNnSc1	vozidlo
===	===	k?	===
</s>
</p>
<p>
<s>
Typickým	typický	k2eAgMnSc7d1	typický
představitelem	představitel	k1gMnSc7	představitel
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc1d1	historický
přívěsný	přívěsný	k2eAgInSc1d1	přívěsný
vozík	vozík	k1gInSc1	vozík
PAv	pávit	k5eAaImRp2nS	pávit
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
drobného	drobný	k2eAgInSc2d1	drobný
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
automechanika	automechanik	k1gMnSc4	automechanik
<g/>
;	;	kIx,	;
Rolf	Rolf	k1gMnSc1	Rolf
Gscheidle	Gscheidlo	k1gNnSc6	Gscheidlo
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
;	;	kIx,	;
Sobotáles	sobotáles	k1gInSc1	sobotáles
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dálník	Dálník	k1gMnSc1	Dálník
</s>
</p>
<p>
<s>
Moped	moped	k1gInSc1	moped
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
motocykl	motocykl	k1gInSc1	motocykl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
motocykl	motocykl	k1gInSc1	motocykl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
http://www.motomagazin.cz/index.php?action=pav&	[url]	k?	http://www.motomagazin.cz/index.php?action=pav&
</s>
</p>
<p>
<s>
http://www.justride.cz	[url]	k1gInSc1	http://www.justride.cz
-	-	kIx~	-
motorkářský	motorkářský	k2eAgInSc1d1	motorkářský
portál	portál	k1gInSc1	portál
plný	plný	k2eAgInSc1d1	plný
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
reportáží	reportáž	k1gFnPc2	reportáž
<g/>
,	,	kIx,	,
kalendáře	kalendář	k1gInPc1	kalendář
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
http://www.motozpravodaj.cz	[url]	k1gInSc1	http://www.motozpravodaj.cz
-	-	kIx~	-
rozcestník	rozcestník	k1gInSc1	rozcestník
motorkářských	motorkářský	k2eAgInPc2d1	motorkářský
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naleznete	naleznout	k5eAaPmIp2nP	naleznout
všechny	všechen	k3xTgInPc4	všechen
články	článek	k1gInPc4	článek
přehledně	přehledně	k6eAd1	přehledně
a	a	k8xC	a
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
</s>
</p>
