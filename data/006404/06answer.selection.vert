<s>
Novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
mluvčí	mluvčí	k1gMnSc1	mluvčí
Petr	Petr	k1gMnSc1	Petr
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
zlatý	zlatý	k2eAgInSc4d1	zlatý
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
za	za	k7c4	za
popírání	popírání	k1gNnSc4	popírání
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
celou	celý	k2eAgFnSc4d1	celá
soutěž	soutěž	k1gFnSc4	soutěž
za	za	k7c4	za
něco	něco	k3yInSc4	něco
"	"	kIx"	"
<g/>
vysoce	vysoce	k6eAd1	vysoce
hloupého	hloupý	k2eAgInSc2d1	hloupý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
