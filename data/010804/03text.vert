<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Andrš	Andrš	k1gMnSc1	Andrš
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1932	[number]	k4	1932
Tis	Tisa	k1gFnPc2	Tisa
u	u	k7c2	u
Rychnova	Rychnov	k1gInSc2	Rychnov
n.	n.	k?	n.
Kněžnou	kněžna	k1gFnSc7	kněžna
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
Československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
lidové	lidový	k2eAgFnSc2d1	lidová
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
ústřední	ústřední	k2eAgMnSc1d1	ústřední
tajemník	tajemník	k1gMnSc1	tajemník
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc7d1	původní
profesí	profes	k1gFnSc7	profes
byl	být	k5eAaImAgInS	být
textilním	textilní	k2eAgMnSc7d1	textilní
dělníkem	dělník	k1gMnSc7	dělník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
školu	škola	k1gFnSc4	škola
tkalcovskou	tkalcovský	k2eAgFnSc4d1	tkalcovská
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
dálkově	dálkově	k6eAd1	dálkově
studoval	studovat	k5eAaImAgMnS	studovat
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
ČSL	ČSL	kA	ČSL
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
letech	let	k1gInPc6	let
1951	[number]	k4	1951
<g/>
–	–	k?	–
<g/>
1965	[number]	k4	1965
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
tajemníka	tajemník	k1gMnSc2	tajemník
okresní	okresní	k2eAgFnSc2d1	okresní
organizace	organizace	k1gFnSc2	organizace
ČSL	ČSL	kA	ČSL
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
byl	být	k5eAaImAgMnS	být
zástupcem	zástupce	k1gMnSc7	zástupce
krajského	krajský	k2eAgMnSc2d1	krajský
tajemníka	tajemník	k1gMnSc2	tajemník
ČSL	ČSL	kA	ČSL
pro	pro	k7c4	pro
Východočeský	východočeský	k2eAgInSc4d1	východočeský
kraj	kraj	k1gInSc4	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
stranické	stranický	k2eAgFnSc6d1	stranická
centrále	centrála	k1gFnSc6	centrála
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1977	[number]	k4	1977
coby	coby	k?	coby
tajemník	tajemník	k1gMnSc1	tajemník
a	a	k8xC	a
vedoucí	vedoucí	k1gMnSc1	vedoucí
sekretariátu	sekretariát	k1gInSc2	sekretariát
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
ČSL	ČSL	kA	ČSL
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
jako	jako	k8xS	jako
ústřední	ústřední	k2eAgMnSc1d1	ústřední
tajemník	tajemník	k1gMnSc1	tajemník
ČSL	ČSL	kA	ČSL
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gInSc1	jeho
nástup	nástup	k1gInSc1	nástup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
ústředního	ústřední	k2eAgMnSc2d1	ústřední
tajemníka	tajemník	k1gMnSc2	tajemník
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnPc2	součást
výměny	výměna	k1gFnSc2	výměna
kádrů	kádr	k1gInPc2	kádr
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
volbou	volba	k1gFnSc7	volba
Zbyňka	Zbyněk	k1gMnSc2	Zbyněk
Žalmana	Žalman	k1gMnSc2	Žalman
předsedou	předseda	k1gMnSc7	předseda
ČSL	ČSL	kA	ČSL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
strany	strana	k1gFnSc2	strana
patřil	patřit	k5eAaImAgInS	patřit
ke	k	k7c3	k
konzervativnímu	konzervativní	k2eAgNnSc3d1	konzervativní
<g/>
,	,	kIx,	,
prokomunistickému	prokomunistický	k2eAgNnSc3d1	prokomunistické
křídlu	křídlo	k1gNnSc3	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
reformní	reformní	k2eAgFnSc1d1	reformní
skupina	skupina	k1gFnSc1	skupina
funkcionářů	funkcionář	k1gMnPc2	funkcionář
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Richard	Richard	k1gMnSc1	Richard
Sacher	Sachra	k1gFnPc2	Sachra
<g/>
,	,	kIx,	,
a	a	k8xC	a
požadovala	požadovat	k5eAaImAgFnS	požadovat
jeho	on	k3xPp3gInSc4	on
odchod	odchod	k1gInSc4	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Podpisová	podpisový	k2eAgFnSc1d1	podpisová
akce	akce	k1gFnSc1	akce
proti	proti	k7c3	proti
Adršovi	Adrš	k1gMnSc3	Adrš
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
vyzrazena	vyzrazen	k2eAgFnSc1d1	vyzrazena
a	a	k8xC	a
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
pak	pak	k6eAd1	pak
varovala	varovat	k5eAaImAgFnS	varovat
opoziční	opoziční	k2eAgFnSc4d1	opoziční
frakci	frakce	k1gFnSc4	frakce
před	před	k7c4	před
následky	následek	k1gInPc4	následek
takového	takový	k3xDgInSc2	takový
kroku	krok	k1gInSc2	krok
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
ho	on	k3xPp3gInSc4	on
evidovala	evidovat	k5eAaImAgFnS	evidovat
jako	jako	k8xC	jako
tajného	tajný	k2eAgMnSc2d1	tajný
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
(	(	kIx(	(
<g/>
krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
Petr	Petr	k1gMnSc1	Petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
zasedl	zasednout	k5eAaPmAgMnS	zasednout
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
části	část	k1gFnSc2	část
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
44	[number]	k4	44
–	–	k?	–
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
Východočeský	východočeský	k2eAgInSc1d1	východočeský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
znovu	znovu	k6eAd1	znovu
získal	získat	k5eAaPmAgInS	získat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc1	obvod
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
)	)	kIx)	)
a	a	k8xC	a
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc1	obvod
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Federálním	federální	k2eAgNnSc6d1	federální
shromáždění	shromáždění	k1gNnSc6	shromáždění
setrval	setrvat	k5eAaPmAgMnS	setrvat
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
procesu	proces	k1gInSc2	proces
kooptací	kooptace	k1gFnPc2	kooptace
do	do	k7c2	do
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
převzal	převzít	k5eAaPmAgMnS	převzít
Josef	Josef	k1gMnSc1	Josef
Lux	Lux	k1gMnSc1	Lux
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Andrš	Andrš	k1gMnSc1	Andrš
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
</s>
</p>
