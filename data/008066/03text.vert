<s>
Milka	Milka	k1gFnSc1	Milka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
značka	značka	k1gFnSc1	značka
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
Mondelē	Mondelē	k1gMnSc1	Mondelē
International	International	k1gMnSc1	International
vyrábějící	vyrábějící	k2eAgFnSc4d1	vyrábějící
čokoládu	čokoláda	k1gFnSc4	čokoláda
a	a	k8xC	a
čokoládové	čokoládový	k2eAgInPc4d1	čokoládový
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
Švýcarem	Švýcar	k1gMnSc7	Švýcar
Philippem	Philipp	k1gMnSc7	Philipp
Suchardem	Suchardem	k?	Suchardem
a	a	k8xC	a
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
byly	být	k5eAaImAgInP	být
její	její	k3xOp3gInPc1	její
produkty	produkt	k1gInPc1	produkt
baleny	balen	k2eAgInPc1d1	balen
do	do	k7c2	do
fialového	fialový	k2eAgInSc2d1	fialový
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
fialová	fialový	k2eAgFnSc1d1	fialová
kráva	kráva	k1gFnSc1	kráva
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Milka	Milek	k1gMnSc2	Milek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
čokoláda	čokoláda	k1gFnSc1	čokoláda
vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
míst	místo	k1gNnPc2	místo
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
závody	závod	k1gInPc1	závod
v	v	k7c6	v
Lörrachu	Lörrach	k1gInSc6	Lörrach
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bludenz	Bludenz	k1gInSc1	Bludenz
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Svoge	Svoge	k1gFnSc1	Svoge
(	(	kIx(	(
<g/>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Braşov	Braşov	k1gInSc1	Braşov
(	(	kIx(	(
<g/>
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jankowice	Jankowice	k1gFnSc1	Jankowice
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Trosťanec	Trosťanec	k1gInSc4	Trosťanec
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
čokoláda	čokoláda	k1gFnSc1	čokoláda
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
i	i	k9	i
v	v	k7c6	v
brazilském	brazilský	k2eAgNnSc6d1	brazilské
městě	město	k1gNnSc6	město
Paraná	Paraná	k1gFnSc1	Paraná
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c6	na
patentovém	patentový	k2eAgInSc6d1	patentový
úřadě	úřad	k1gInSc6	úřad
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Berlíně	Berlín	k1gInSc6	Berlín
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
německého	německý	k2eAgMnSc2d1	německý
výrobce	výrobce	k1gMnSc2	výrobce
čokolády	čokoláda	k1gFnSc2	čokoláda
Philippa	Philipp	k1gMnSc4	Philipp
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
název	název	k1gInSc1	název
Milka	Milek	k1gMnSc2	Milek
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spojením	spojení	k1gNnSc7	spojení
německých	německý	k2eAgNnPc2d1	německé
slov	slovo	k1gNnPc2	slovo
Milch	Milch	k1gInSc1	Milch
(	(	kIx(	(
<g/>
mléko	mléko	k1gNnSc1	mléko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kakao	kakao	k1gNnSc1	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
společnosti	společnost	k1gFnSc2	společnost
Milka	Milka	k1gFnSc1	Milka
alpská	alpský	k2eAgFnSc1d1	alpská
kráva	kráva	k1gFnSc1	kráva
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
představila	představit	k5eAaPmAgFnS	představit
veřejnosti	veřejnost	k1gFnPc4	veřejnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
reklamě	reklama	k1gFnSc6	reklama
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
agentury	agentura	k1gFnSc2	agentura
Young	Young	k1gMnSc1	Young
<g/>
&	&	k?	&
<g/>
Rubicam	Rubicam	k1gInSc1	Rubicam
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
již	již	k6eAd1	již
ve	v	k7c6	v
110	[number]	k4	110
reklamních	reklamní	k2eAgInPc6d1	reklamní
spotech	spot	k1gInPc6	spot
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krávu	kráva	k1gFnSc4	kráva
plemene	plemeno	k1gNnSc2	plemeno
Fleckvieh	Fleckvieha	k1gFnPc2	Fleckvieha
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
hledící	hledící	k2eAgMnSc1d1	hledící
na	na	k7c4	na
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
fialově	fialově	k6eAd1	fialově
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Fialově	fialově	k6eAd1	fialově
jsou	být	k5eAaImIp3nP	být
vybarveny	vybarven	k2eAgFnPc1d1	vybarvena
tmavší	tmavý	k2eAgFnPc1d2	tmavší
oblasti	oblast	k1gFnPc1	oblast
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krku	krk	k1gInSc6	krk
má	mít	k5eAaImIp3nS	mít
uvázán	uvázán	k2eAgInSc1d1	uvázán
zvon	zvon	k1gInSc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
ale	ale	k9	ale
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
čokolády	čokoláda	k1gFnSc2	čokoláda
již	již	k6eAd1	již
objevila	objevit	k5eAaPmAgFnS	objevit
kráva	kráva	k1gFnSc1	kráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
