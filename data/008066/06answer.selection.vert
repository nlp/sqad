<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
Švýcarem	Švýcar	k1gMnSc7	Švýcar
Philippem	Philipp	k1gMnSc7	Philipp
Suchardem	Suchardem	k?	Suchardem
a	a	k8xC	a
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
byly	být	k5eAaImAgInP	být
její	její	k3xOp3gInPc1	její
produkty	produkt	k1gInPc1	produkt
baleny	balen	k2eAgInPc1d1	balen
do	do	k7c2	do
fialového	fialový	k2eAgInSc2d1	fialový
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
fialová	fialový	k2eAgFnSc1d1	fialová
kráva	kráva	k1gFnSc1	kráva
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Milka	Milek	k1gMnSc2	Milek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
