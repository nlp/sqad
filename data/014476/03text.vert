<s>
Kroměřížský	kroměřížský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
</s>
<s>
Kroměřížský	kroměřížský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
/	/	kIx~
Kremsierer	Kremsierer	k1gMnSc1
Reichstag	Reichstag	k1gMnSc1
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
vlevo	vlevo	k6eAd1
arcibiskupský	arcibiskupský	k2eAgInSc1d1
zámek	zámek	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
ústavodárný	ústavodárný	k2eAgInSc1d1
sněm	sněm	k1gInSc1
zasedal	zasedat	k5eAaImAgInS
Předseda	předseda	k1gMnSc1
</s>
<s>
Anton	Anton	k1gMnSc1
Strohbach	Strohbach	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
</s>
<s>
Franciszek	Franciszek	k1gInSc1
Jan	Jan	k1gMnSc1
Smolka	Smolka	k1gMnSc1
Místopředsedové	místopředseda	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Lasser	Lassra	k1gFnPc2
von	von	k1gInSc1
Zollheim	Zollheim	k1gMnSc1
</s>
<s>
Kroměřížský	kroměřížský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Kremsierer	Kremsierer	k1gMnSc1
Reichstag	Reichstag	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
zasedání	zasedání	k1gNnSc2
Ústavodárného	ústavodárný	k2eAgInSc2d1
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
začalo	začít	k5eAaPmAgNnS
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1848	#num#	k4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
však	však	k9
vypukla	vypuknout	k5eAaPmAgFnS
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1848	#num#	k4
přerušeno	přerušit	k5eAaPmNgNnS
a	a	k8xC
přesunuto	přesunout	k5eAaPmNgNnS
do	do	k7c2
Kroměříže	Kroměříž	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pokračovalo	pokračovat	k5eAaImAgNnS
od	od	k7c2
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1848	#num#	k4
v	v	k7c6
Arcibiskupském	arcibiskupský	k2eAgInSc6d1
zámku	zámek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
tohoto	tento	k3xDgNnSc2
data	datum	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
Kroměřížský	kroměřížský	k2eAgInSc4d1
sněm	sněm	k1gInSc4
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
předchozí	předchozí	k2eAgNnSc1d1
jednání	jednání	k1gNnSc1
neslo	nést	k5eAaImAgNnS
název	název	k1gInSc4
Vídeňský	vídeňský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1
zámek	zámek	k1gInSc1
dnes	dnes	k6eAd1
</s>
<s>
Poslanci	poslanec	k1gMnPc1
tohoto	tento	k3xDgInSc2
sněmu	sněm	k1gInSc2
byli	být	k5eAaImAgMnP
voleni	volit	k5eAaImNgMnP
zemskými	zemský	k2eAgInPc7d1
sněmy	sněm	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sněmu	sněm	k1gInSc2
předsedal	předsedat	k5eAaImAgMnS
haličský	haličský	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
Franciszek	Franciszka	k1gFnPc2
Jan	Jan	k1gMnSc1
Smolka	Smolka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
zde	zde	k6eAd1
zastupoval	zastupovat	k5eAaImAgMnS
například	například	k6eAd1
František	František	k1gMnSc1
Palacký	Palacký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Kajetán	Kajetán	k1gMnSc1
Tyl	Tyl	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Bolemír	Bolemír	k1gMnSc1
Nebeský	nebeský	k2eAgMnSc1d1
nebo	nebo	k8xC
František	František	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Rieger	Rieger	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroměřížský	kroměřížský	k2eAgInSc1d1
volební	volební	k2eAgInSc1d1
obvod	obvod	k1gInSc1
zastupoval	zastupovat	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
Ohéral	Ohéral	k1gMnSc1
<g/>
,	,	kIx,
uváděný	uváděný	k2eAgMnSc1d1
jako	jako	k9
redaktor	redaktor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
sněm	sněm	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1848	#num#	k4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
zrušil	zrušit	k5eAaPmAgInS
poddanství	poddanství	k1gNnSc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
nyní	nyní	k6eAd1
jeho	jeho	k3xOp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
programem	program	k1gInSc7
vytvoření	vytvoření	k1gNnSc2
nové	nový	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
specifikovala	specifikovat	k5eAaBmAgFnS
zejména	zejména	k9
vztah	vztah	k1gInSc4
Říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
a	a	k8xC
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
potlačení	potlačení	k1gNnSc6
povstání	povstání	k1gNnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
Alfredem	Alfred	k1gMnSc7
Windischgrätzem	Windischgrätz	k1gMnSc7
už	už	k6eAd1
však	však	k9
neměl	mít	k5eNaImAgInS
sněm	sněm	k1gInSc1
téměř	téměř	k6eAd1
žádnou	žádný	k3yNgFnSc4
skutečnou	skutečný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Felixe	Felix	k1gMnSc2
Schwarzenberga	Schwarzenberg	k1gMnSc2
sice	sice	k8xC
předstírala	předstírat	k5eAaImAgFnS
zájem	zájem	k1gInSc4
o	o	k7c4
dění	dění	k1gNnSc4
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
nástupem	nástup	k1gInSc7
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
na	na	k7c4
trůn	trůn	k1gInSc4
připravila	připravit	k5eAaPmAgFnS
vlastní	vlastní	k2eAgFnSc4d1
ústavu	ústava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgNnPc1d1
práva	právo	k1gNnPc1
občanů	občan	k1gMnPc2
byla	být	k5eAaImAgNnP
vypracována	vypracovat	k5eAaPmNgNnP
již	již	k6eAd1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
(	(	kIx(
<g/>
s	s	k7c7
podílem	podíl	k1gInSc7
Riegera	Riegero	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
byla	být	k5eAaImAgFnS
předložena	předložen	k2eAgFnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
k	k	k7c3
prvnímu	první	k4xOgNnSc3
čtení	čtení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problematický	problematický	k2eAgInSc1d1
byl	být	k5eAaImAgInS
už	už	k6eAd1
první	první	k4xOgInSc1
paragraf	paragraf	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgInSc2
všechna	všechen	k3xTgNnPc1
politická	politický	k2eAgNnPc1d1
moc	moc	k6eAd1
ve	v	k7c6
státě	stát	k1gInSc6
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
proti	proti	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
se	se	k3xPyFc4
ihned	ihned	k6eAd1
ohradil	ohradit	k5eAaPmAgMnS
František	František	k1gMnSc1
Stadion	stadion	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
argumentoval	argumentovat	k5eAaImAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
moc	moc	k6eAd1
patří	patřit	k5eAaImIp3nS
panovníkovi	panovník	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
otázka	otázka	k1gFnSc1
nebyla	být	k5eNaImAgFnS
vyřešena	vyřešit	k5eAaPmNgFnS
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
bylo	být	k5eAaImAgNnS
odhlasováno	odhlasován	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
vyřešena	vyřešit	k5eAaPmNgFnS
až	až	k9
při	při	k7c6
konečném	konečný	k2eAgNnSc6d1
hlasování	hlasování	k1gNnSc6
o	o	k7c6
ústavě	ústava	k1gFnSc6
jako	jako	k8xC,k8xS
celku	celek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protože	protože	k8xS
rakouská	rakouský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
potlačila	potlačit	k5eAaPmAgFnS
všechny	všechen	k3xTgInPc4
nepokoje	nepokoj	k1gInPc4
a	a	k8xC
zvítězila	zvítězit	k5eAaPmAgFnS
i	i	k9
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
pozice	pozice	k1gFnSc1
panovníka	panovník	k1gMnSc2
natolik	natolik	k6eAd1
silná	silný	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nakonec	nakonec	k6eAd1
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1849	#num#	k4
mohl	moct	k5eAaImAgMnS
vydat	vydat	k5eAaPmF
manifest	manifest	k1gInSc4
o	o	k7c4
rozpuštění	rozpuštění	k1gNnSc4
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroměříž	Kroměříž	k1gFnSc1
byla	být	k5eAaImAgFnS
obsazena	obsadit	k5eAaPmNgFnS
vojskem	vojsko	k1gNnSc7
a	a	k8xC
císař	císař	k1gMnSc1
vyhlásil	vyhlásit	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
Březnovou	březnový	k2eAgFnSc4d1
ústavu	ústava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
jsou	být	k5eAaImIp3nP
na	na	k7c6
řadě	řada	k1gFnSc6
domů	dům	k1gInPc2
umístěny	umístit	k5eAaPmNgFnP
pamětní	pamětní	k2eAgFnPc1d1
desky	deska	k1gFnPc1
věnované	věnovaný	k2eAgFnPc1d1
významným	významný	k2eAgMnPc3d1
osobnostem	osobnost	k1gFnPc3
sněmu	sněm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Arcibiskupský	arcibiskupský	k2eAgInSc4d1
zámek	zámek	k1gInSc4
a	a	k8xC
zahrady	zahrada	k1gFnSc2
Kroměříž	Kroměříž	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠTAJF	ŠTAJF	k?
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revoluční	revoluční	k2eAgFnSc1d1
léta	léto	k1gNnPc4
1848-1849	1848-1849	k4
a	a	k8xC
české	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
194	#num#	k4
s.	s.	k?
S.	S.	kA
142	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroměřížský	kroměřížský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1849	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
109	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
190	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SPÁČIL	Spáčil	k1gMnSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroměřížský	kroměřížský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
1849	#num#	k4
<g/>
:	:	kIx,
kronika	kronika	k1gFnSc1
prvního	první	k4xOgNnSc2
ústavodárného	ústavodárný	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
rakouských	rakouský	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroměříž	Kroměříž	k1gFnSc1
<g/>
:	:	kIx,
Městský	městský	k2eAgInSc1d1
osvětový	osvětový	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
1933	#num#	k4
<g/>
.	.	kIx.
119	#num#	k4
s.	s.	k?
</s>
<s>
SPÁČIL	Spáčil	k1gMnSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ať	ať	k8xC,k8xS
žije	žít	k5eAaImIp3nS
sněm	sněm	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Družstvo	družstvo	k1gNnSc1
Moravského	moravský	k2eAgNnSc2d1
kola	kolo	k1gNnSc2
spisovatelů	spisovatel	k1gMnPc2
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
.	.	kIx.
375	#num#	k4
s.	s.	k?
</s>
<s>
Kroměřížský	kroměřížský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
1848	#num#	k4
–	–	k?
1849	#num#	k4
a	a	k8xC
tradice	tradice	k1gFnSc1
parlamentarismu	parlamentarismus	k1gInSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc4
příspěvků	příspěvek	k1gInPc2
ze	z	k7c2
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
mezinárodní	mezinárodní	k2eAgFnSc2d1
konference	konference	k1gFnSc2
Kroměříž	Kroměříž	k1gFnSc1
1998	#num#	k4
<g/>
:	:	kIx,
Katos	Katosa	k1gFnPc2
Kojetín	Kojetín	k1gInSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
490	#num#	k4
s.	s.	k?
</s>
<s>
PINKAVA	PINKAVA	k?
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroměříž	Kroměříž	k1gFnSc1
ve	v	k7c6
dnech	den	k1gInPc6
ústavodárného	ústavodárný	k2eAgInSc2d1
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
:	:	kIx,
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1848	#num#	k4
až	až	k9
7	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1849	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroměříž	Kroměříž	k1gFnSc1
<g/>
:	:	kIx,
Pálková	Pálková	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
114	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kroměřížská	kroměřížský	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
poslanců	poslanec	k1gMnPc2
Říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
poslanců	poslanec	k1gMnPc2
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ústavní	ústavní	k2eAgInSc1d1
a	a	k8xC
politický	politický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
a	a	k8xC
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
Ústavní	ústavní	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
</s>
<s>
dubnová	dubnový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
•	•	k?
kroměřížská	kroměřížský	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
březnová	březnový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
silvestrovské	silvestrovský	k2eAgInPc1d1
patenty	patent	k1gInPc1
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
)	)	kIx)
•	•	k?
říjnový	říjnový	k2eAgInSc4d1
diplom	diplom	k1gInSc4
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
)	)	kIx)
•	•	k?
únorová	únorový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
)	)	kIx)
•	•	k?
zářijový	zářijový	k2eAgInSc4d1
manifest	manifest	k1gInSc4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1865	#num#	k4
<g/>
)	)	kIx)
•	•	k?
rakousko-uherské	rakousko-uherský	k2eAgNnSc4d1
vyrovnání	vyrovnání	k1gNnSc4
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
•	•	k?
prosincová	prosincový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
•	•	k?
chorvatsko-uherské	chorvatsko-uherský	k2eAgNnSc4d1
vyrovnání	vyrovnání	k1gNnSc4
(	(	kIx(
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
•	•	k?
fundamentální	fundamentální	k2eAgInPc4d1
články	článek	k1gInPc4
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
dubnová	dubnový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Taaffeho	Taaffe	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
(	(	kIx(
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Badeniho	Badeni	k1gMnSc2
volební	volební	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
•	•	k?
všeobecné	všeobecný	k2eAgNnSc4d1
a	a	k8xC
rovné	rovný	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
Legislativní	legislativní	k2eAgNnPc1d1
a	a	k8xC
exekutivní	exekutivní	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
</s>
<s>
Říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Frankfurtský	frankfurtský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Uherský	uherský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
zemské	zemský	k2eAgInPc1d1
sněmy	sněm	k1gInPc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
rakousko-uherské	rakousko-uherský	k2eAgFnSc2d1
delegace	delegace	k1gFnSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
vláda	vláda	k1gFnSc1
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
–	–	k?
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
•	•	k?
vláda	vláda	k1gFnSc1
Předlitavska	Předlitavsko	k1gNnSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
vláda	vláda	k1gFnSc1
Uherska	Uhersko	k1gNnSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
společná	společný	k2eAgFnSc1d1
ministerstva	ministerstvo	k1gNnSc2
Rakouska-Uherska	Rakouska-Uherska	k1gFnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
•	•	k?
zemské	zemský	k2eAgInPc1d1
výbory	výbor	k1gInPc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
</s>
