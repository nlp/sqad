<s>
Kroměřížský	kroměřížský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Kremsierer	Kremsierer	k1gMnSc1
Reichstag	Reichstag	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
zasedání	zasedání	k1gNnSc2
Ústavodárného	ústavodárný	k2eAgInSc2d1
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
začalo	začít	k5eAaPmAgNnS
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1848	#num#	k4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
však	však	k9
vypukla	vypuknout	k5eAaPmAgFnS
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1848	#num#	k4
přerušeno	přerušit	k5eAaPmNgNnS
a	a	k8xC
přesunuto	přesunout	k5eAaPmNgNnS
do	do	k7c2
Kroměříže	Kroměříž	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pokračovalo	pokračovat	k5eAaImAgNnS
od	od	k7c2
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1848	#num#	k4
v	v	k7c6
Arcibiskupském	arcibiskupský	k2eAgInSc6d1
zámku	zámek	k1gInSc6
<g/>
.	.	kIx.
</s>
