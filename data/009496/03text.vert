<p>
<s>
Rozhlas	rozhlas	k1gInSc1	rozhlas
je	být	k5eAaImIp3nS	být
telekomunikační	telekomunikační	k2eAgNnSc4d1	telekomunikační
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
jednosměrný	jednosměrný	k2eAgInSc4d1	jednosměrný
přenos	přenos	k1gInSc4	přenos
zvuku	zvuk	k1gInSc2	zvuk
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
<g/>
.	.	kIx.	.
</s>
<s>
Hovorově	hovorově	k6eAd1	hovorově
se	se	k3xPyFc4	se
rozhlas	rozhlas	k1gInSc1	rozhlas
označuje	označovat	k5eAaImIp3nS	označovat
slovem	slovem	k6eAd1	slovem
rádio	rádio	k1gNnSc1	rádio
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
radius	radius	k1gInSc1	radius
<g/>
,	,	kIx,	,
paprsek	paprsek	k1gInSc1	paprsek
a	a	k8xC	a
radiatio	radiatio	k1gNnSc1	radiatio
<g/>
,	,	kIx,	,
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rádio	rádio	k1gNnSc1	rádio
bylo	být	k5eAaImAgNnS	být
vynalezeno	vynaleznout	k5eAaPmNgNnS	vynaleznout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jako	jako	k9	jako
přenosové	přenosový	k2eAgNnSc4d1	přenosové
médium	médium	k1gNnSc4	médium
slouží	sloužit	k5eAaImIp3nP	sloužit
rádiové	rádiový	k2eAgFnPc1d1	rádiová
vlny	vlna	k1gFnPc1	vlna
(	(	kIx(	(
<g/>
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
z	z	k7c2	z
pozemních	pozemní	k2eAgMnPc2d1	pozemní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
satelitních	satelitní	k2eAgInPc2d1	satelitní
vysílačů	vysílač	k1gInPc2	vysílač
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
i	i	k9	i
kovové	kovový	k2eAgNnSc1d1	kovové
vedení	vedení	k1gNnSc1	vedení
(	(	kIx(	(
<g/>
rozhlas	rozhlas	k1gInSc1	rozhlas
po	po	k7c6	po
drátě	drát	k1gInSc6	drát
<g/>
,	,	kIx,	,
místní	místní	k2eAgInSc1d1	místní
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozhlas	rozhlas	k1gInSc1	rozhlas
šíří	šířit	k5eAaImIp3nS	šířit
i	i	k9	i
pomocí	pomocí	k7c2	pomocí
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojením	spojení	k1gNnSc7	spojení
rádiový	rádiový	k2eAgInSc1d1	rádiový
či	či	k8xC	či
rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
přijímač	přijímač	k1gInSc1	přijímač
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
technické	technický	k2eAgNnSc4d1	technické
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
přijímání	přijímání	k1gNnSc4	přijímání
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
vysílání	vysílání	k1gNnSc1	vysílání
samo	sám	k3xTgNnSc1	sám
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
slovem	slovem	k6eAd1	slovem
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
==	==	k?	==
</s>
</p>
<p>
<s>
Přenášený	přenášený	k2eAgInSc1d1	přenášený
akustický	akustický	k2eAgInSc1d1	akustický
(	(	kIx(	(
<g/>
zvukový	zvukový	k2eAgInSc1d1	zvukový
<g/>
)	)	kIx)	)
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
poměrně	poměrně	k6eAd1	poměrně
nízkých	nízký	k2eAgInPc2d1	nízký
kmitočtů	kmitočet	k1gInPc2	kmitočet
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
Hz	Hz	kA	Hz
–	–	k?	–
15	[number]	k4	15
kHz	khz	kA	khz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
jako	jako	k9	jako
elektromagnetické	elektromagnetický	k2eAgFnPc1d1	elektromagnetická
vlny	vlna	k1gFnPc1	vlna
nedaly	dát	k5eNaPmAgFnP	dát
vysílat	vysílat	k5eAaImF	vysílat
a	a	k8xC	a
šířit	šířit	k5eAaImF	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zvukový	zvukový	k2eAgInSc1d1	zvukový
signál	signál	k1gInSc1	signál
moduluje	modulovat	k5eAaImIp3nS	modulovat
na	na	k7c4	na
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgInPc4d2	vyšší
kmitočty	kmitočet	k1gInPc4	kmitočet
tzv.	tzv.	kA	tzv.
nosné	nosný	k2eAgFnPc4d1	nosná
vlny	vlna	k1gFnPc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
užívaly	užívat	k5eAaImAgInP	užívat
kmitočty	kmitočet	k1gInPc1	kmitočet
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
30	[number]	k4	30
–	–	k?	–
300	[number]	k4	300
kHz	khz	kA	khz
<g/>
,	,	kIx,	,
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
300	[number]	k4	300
kHz	khz	kA	khz
–	–	k?	–
3	[number]	k4	3
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
a	a	k8xC	a
krátkých	krátký	k2eAgFnPc2d1	krátká
vln	vlna	k1gFnPc2	vlna
(	(	kIx(	(
<g/>
3	[number]	k4	3
MHz	Mhz	kA	Mhz
–	–	k?	–
30	[number]	k4	30
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
vysílací	vysílací	k2eAgFnSc2d1	vysílací
antény	anténa	k1gFnSc2	anténa
šíří	šířit	k5eAaImIp3nP	šířit
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
pásmech	pásmo	k1gNnPc6	pásmo
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
amplitudová	amplitudový	k2eAgFnSc1d1	amplitudová
modulace	modulace	k1gFnSc1	modulace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zvukového	zvukový	k2eAgInSc2d1	zvukový
signálu	signál	k1gInSc2	signál
mění	měnit	k5eAaImIp3nS	měnit
amplituda	amplituda	k1gFnSc1	amplituda
(	(	kIx(	(
<g/>
rozkmit	rozkmit	k1gInSc1	rozkmit
<g/>
)	)	kIx)	)
nosné	nosný	k2eAgFnPc1d1	nosná
vlny	vlna	k1gFnPc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
však	však	k9	však
velmi	velmi	k6eAd1	velmi
rozměrné	rozměrný	k2eAgFnSc2d1	rozměrná
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
vysílací	vysílací	k2eAgFnSc2d1	vysílací
antény	anténa	k1gFnSc2	anténa
a	a	k8xC	a
velké	velký	k2eAgInPc4d1	velký
vysílací	vysílací	k2eAgInPc4d1	vysílací
výkony	výkon	k1gInPc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
vln	vlna	k1gFnPc2	vlna
se	se	k3xPyFc4	se
také	také	k9	také
vejde	vejít	k5eAaPmIp3nS	vejít
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
vysílačů	vysílač	k1gInPc2	vysílač
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
velkou	velký	k2eAgFnSc4d1	velká
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mohou	moct	k5eAaImIp3nP	moct
rušit	rušit	k5eAaImF	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnPc1d1	střední
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
tisíců	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
vysílačů	vysílač	k1gInPc2	vysílač
<g/>
,	,	kIx,	,
vysílací	vysílací	k2eAgFnSc1d1	vysílací
anténa	anténa	k1gFnSc1	anténa
i	i	k8xC	i
výkon	výkon	k1gInSc1	výkon
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnPc1d1	střední
vlny	vlna	k1gFnPc1	vlna
byly	být	k5eAaImAgFnP	být
nejběžnějším	běžný	k2eAgNnSc7d3	nejběžnější
pásmem	pásmo	k1gNnSc7	pásmo
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
rozhlasu	rozhlas	k1gInSc2	rozhlas
až	až	k6eAd1	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Šíření	šíření	k1gNnSc1	šíření
krátkých	krátký	k2eAgFnPc2d1	krátká
vln	vlna	k1gFnPc2	vlna
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
jak	jak	k6eAd1	jak
tzv.	tzv.	kA	tzv.
přímou	přímý	k2eAgFnSc7d1	přímá
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
odraženou	odražený	k2eAgFnSc7d1	odražená
vlnou	vlna	k1gFnSc7	vlna
od	od	k7c2	od
vrstev	vrstva	k1gFnPc2	vrstva
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
.	.	kIx.	.
</s>
<s>
Antény	anténa	k1gFnPc1	anténa
i	i	k8xC	i
výkony	výkon	k1gInPc1	výkon
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgNnSc4d1	malé
<g/>
,	,	kIx,	,
šíření	šíření	k1gNnSc4	šíření
však	však	k9	však
více	hodně	k6eAd2	hodně
závisí	záviset	k5eAaImIp3nS	záviset
také	také	k9	také
na	na	k7c6	na
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
vlnách	vlna	k1gFnPc6	vlna
vyhrazena	vyhrazen	k2eAgNnPc1d1	vyhrazeno
oddělená	oddělený	k2eAgNnPc1d1	oddělené
pásma	pásmo	k1gNnPc1	pásmo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
označují	označovat	k5eAaImIp3nP	označovat
délkou	délka	k1gFnSc7	délka
vlny	vlna	k1gFnSc2	vlna
<g/>
:	:	kIx,	:
10	[number]	k4	10
m	m	kA	m
(	(	kIx(	(
<g/>
30	[number]	k4	30
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
15	[number]	k4	15
m	m	kA	m
(	(	kIx(	(
<g/>
20	[number]	k4	20
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
20	[number]	k4	20
m	m	kA	m
(	(	kIx(	(
<g/>
15	[number]	k4	15
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
m	m	kA	m
(	(	kIx(	(
<g/>
10	[number]	k4	10
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
40	[number]	k4	40
m	m	kA	m
(	(	kIx(	(
<g/>
7	[number]	k4	7
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k8xC	i
80	[number]	k4	80
a	a	k8xC	a
160	[number]	k4	160
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnPc4d1	krátká
vlny	vlna	k1gFnPc4	vlna
(	(	kIx(	(
<g/>
VKV	VKV	kA	VKV
<g/>
,	,	kIx,	,
30	[number]	k4	30
–	–	k?	–
300	[number]	k4	300
MHz	Mhz	kA	Mhz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ultra	ultra	k2eAgFnSc2d1	ultra
krátké	krátký	k2eAgFnSc2d1	krátká
vlny	vlna	k1gFnSc2	vlna
(	(	kIx(	(
<g/>
UKV	UKV	kA	UKV
<g/>
,	,	kIx,	,
300	[number]	k4	300
MHz	Mhz	kA	Mhz
–	–	k?	–
3	[number]	k4	3
GHz	GHz	k1gFnPc2	GHz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vystačí	vystačit	k5eAaBmIp3nP	vystačit
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
anténami	anténa	k1gFnPc7	anténa
a	a	k8xC	a
vysílacími	vysílací	k2eAgInPc7d1	vysílací
výkony	výkon	k1gInPc7	výkon
<g/>
,	,	kIx,	,
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k9	také
jen	jen	k9	jen
do	do	k7c2	do
malých	malý	k2eAgFnPc2d1	malá
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
(	(	kIx(	(
<g/>
až	až	k9	až
stovky	stovka	k1gFnPc4	stovka
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
se	se	k3xPyFc4	se
však	však	k9	však
vejde	vejít	k5eAaPmIp3nS	vejít
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
širší	široký	k2eAgNnSc4d2	širší
vysílací	vysílací	k2eAgNnSc4d1	vysílací
pásmo	pásmo	k1gNnSc4	pásmo
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
frekvenční	frekvenční	k2eAgFnSc4d1	frekvenční
modulaci	modulace	k1gFnSc4	modulace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zvukového	zvukový	k2eAgInSc2d1	zvukový
signálu	signál	k1gInSc2	signál
mění	měnit	k5eAaImIp3nS	měnit
frekvence	frekvence	k1gFnSc1	frekvence
vysílače	vysílač	k1gInSc2	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
frekvenční	frekvenční	k2eAgFnSc2d1	frekvenční
modulace	modulace	k1gFnSc2	modulace
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
účinného	účinný	k2eAgNnSc2d1	účinné
potlačení	potlačení	k1gNnSc2	potlačení
poruch	porucha	k1gFnPc2	porucha
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
lepší	dobrý	k2eAgFnSc1d2	lepší
kvalita	kvalita	k1gFnSc1	kvalita
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
také	také	k9	také
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
družicové	družicový	k2eAgNnSc1d1	družicové
vysílání	vysílání	k1gNnSc1	vysílání
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
vysokých	vysoký	k2eAgFnPc6d1	vysoká
frekvencích	frekvence	k1gFnPc6	frekvence
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysílací	vysílací	k2eAgFnSc7d1	vysílací
anténou	anténa	k1gFnSc7	anténa
na	na	k7c6	na
umělé	umělý	k2eAgFnSc6d1	umělá
družici	družice	k1gFnSc6	družice
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
klasickou	klasický	k2eAgFnSc4d1	klasická
frekvenční	frekvenční	k2eAgFnSc4d1	frekvenční
modulaci	modulace	k1gFnSc4	modulace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
modulaci	modulace	k1gFnSc4	modulace
digitální	digitální	k2eAgFnSc4d1	digitální
(	(	kIx(	(
<g/>
ADR	ADR	kA	ADR
<g/>
,	,	kIx,	,
DVB-S	DVB-S	k1gFnSc1	DVB-S
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnSc4d1	digitální
modulaci	modulace	k1gFnSc4	modulace
mohou	moct	k5eAaImIp3nP	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
pozemní	pozemní	k2eAgInPc1d1	pozemní
digitální	digitální	k2eAgInPc1d1	digitální
vysílače	vysílač	k1gInPc1	vysílač
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
DAB	DAB	kA	DAB
nebo	nebo	k8xC	nebo
DVB-	DVB-	k1gFnSc1	DVB-
<g/>
T.	T.	kA	T.
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
možnost	možnost	k1gFnSc4	možnost
vysílání	vysílání	k1gNnSc4	vysílání
přijímat	přijímat	k5eAaImF	přijímat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
signálem	signál	k1gInSc7	signál
<g/>
,	,	kIx,	,
vysílaným	vysílaný	k2eAgInSc7d1	vysílaný
rozhlasovými	rozhlasový	k2eAgInPc7d1	rozhlasový
vysílači	vysílač	k1gInPc7	vysílač
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
shodném	shodný	k2eAgNnSc6d1	shodné
území	území	k1gNnSc6	území
obvykle	obvykle	k6eAd1	obvykle
současně	současně	k6eAd1	současně
vysílá	vysílat	k5eAaImIp3nS	vysílat
více	hodně	k6eAd2	hodně
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutné	nutný	k2eAgNnSc1d1	nutné
jednotlivým	jednotlivý	k2eAgNnSc7d1	jednotlivé
vysíláním	vysílání	k1gNnSc7	vysílání
přidělit	přidělit	k5eAaPmF	přidělit
různé	různý	k2eAgInPc4d1	různý
kmitočty	kmitočet	k1gInPc4	kmitočet
(	(	kIx(	(
<g/>
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
)	)	kIx)	)
nosných	nosný	k2eAgFnPc2d1	nosná
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Přidělená	přidělený	k2eAgFnSc1d1	přidělená
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kmitočet	kmitočet	k1gInSc1	kmitočet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
kanál	kanál	k1gInSc4	kanál
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nutnosti	nutnost	k1gFnSc3	nutnost
koordinace	koordinace	k1gFnSc2	koordinace
a	a	k8xC	a
zamezení	zamezení	k1gNnSc4	zamezení
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
rušení	rušení	k1gNnSc2	rušení
jsou	být	k5eAaImIp3nP	být
kanály	kanál	k1gInPc1	kanál
přidělovány	přidělován	k2eAgInPc1d1	přidělován
centrální	centrální	k2eAgFnSc7d1	centrální
autoritou	autorita	k1gFnSc7	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Český	český	k2eAgInSc1d1	český
telekomunikační	telekomunikační	k2eAgInSc1d1	telekomunikační
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
také	také	k9	také
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
anténní	anténní	k2eAgInSc4d1	anténní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
parametry	parametr	k1gInPc4	parametr
vysílače	vysílač	k1gInSc2	vysílač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
šířeno	šířen	k2eAgNnSc4d1	šířeno
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
především	především	k6eAd1	především
formou	forma	k1gFnSc7	forma
veřejnoprávní	veřejnoprávní	k2eAgFnSc2d1	veřejnoprávní
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
funkci	funkce	k1gFnSc4	funkce
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
Vinohradská	vinohradský	k2eAgFnSc1d1	Vinohradská
12	[number]	k4	12
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2	[number]	k4	2
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgNnPc4d1	tradiční
a	a	k8xC	a
legendární	legendární	k2eAgNnPc4d1	legendární
sídlo	sídlo	k1gNnSc4	sídlo
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
kousek	kousek	k1gInSc1	kousek
nad	nad	k7c7	nad
budovou	budova	k1gFnSc7	budova
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
šířeno	šířen	k2eAgNnSc1d1	šířeno
jako	jako	k8xS	jako
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
služba	služba	k1gFnSc1	služba
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
občany	občan	k1gMnPc4	občan
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Kavčích	kavčí	k2eAgFnPc6d1	kavčí
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
a	a	k8xC	a
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
totality	totalita	k1gFnSc2	totalita
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
monopol	monopol	k1gInSc4	monopol
na	na	k7c4	na
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
monopol	monopol	k1gInSc1	monopol
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
a	a	k8xC	a
při	při	k7c6	při
splnění	splnění	k1gNnSc6	splnění
stanovených	stanovený	k2eAgFnPc2d1	stanovená
podmínek	podmínka	k1gFnPc2	podmínka
může	moct	k5eAaImIp3nS	moct
kdokoli	kdokoli	k3yInSc1	kdokoli
dostat	dostat	k5eAaPmF	dostat
licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
licencí	licence	k1gFnSc7	licence
je	být	k5eAaImIp3nS	být
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc6d1	vzniklá
stanici	stanice	k1gFnSc6	stanice
přidělen	přidělit	k5eAaPmNgInS	přidělit
vysílací	vysílací	k2eAgInSc1d1	vysílací
kmitočet	kmitočet	k1gInSc1	kmitočet
a	a	k8xC	a
povolený	povolený	k2eAgInSc1d1	povolený
výkon	výkon	k1gInSc1	výkon
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vysílá	vysílat	k5eAaImIp3nS	vysílat
velká	velký	k2eAgFnSc1d1	velká
řada	řada	k1gFnSc1	řada
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
soukromých	soukromý	k2eAgFnPc2d1	soukromá
<g/>
)	)	kIx)	)
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
resp.	resp.	kA	resp.
malých	malý	k2eAgFnPc2d1	malá
rádií	rádio	k1gNnPc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
působí	působit	k5eAaImIp3nS	působit
pouze	pouze	k6eAd1	pouze
regionálně	regionálně	k6eAd1	regionálně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hitrádio	Hitrádio	k6eAd1	Hitrádio
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
Rádio	rádio	k1gNnSc1	rádio
Orlicko	Orlicko	k1gNnSc1	Orlicko
nebo	nebo	k8xC	nebo
Radio	radio	k1gNnSc1	radio
Relax	Relax	k1gInSc1	Relax
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
soukromých	soukromý	k2eAgFnPc2d1	soukromá
stanic	stanice	k1gFnPc2	stanice
má	mít	k5eAaImIp3nS	mít
přidělenu	přidělen	k2eAgFnSc4d1	přidělena
licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
celoplošné	celoplošný	k2eAgNnSc4d1	celoplošné
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
řady	řada	k1gFnSc2	řada
soukromých	soukromý	k2eAgFnPc2d1	soukromá
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rádio	rádio	k1gNnSc1	rádio
Impuls	impuls	k1gInSc1	impuls
<g/>
,	,	kIx,	,
Frekvence	frekvence	k1gFnSc1	frekvence
1	[number]	k4	1
či	či	k8xC	či
Evropa	Evropa	k1gFnSc1	Evropa
2	[number]	k4	2
<g/>
)	)	kIx)	)
vysílají	vysílat	k5eAaImIp3nP	vysílat
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
i	i	k8xC	i
veřejnoprávní	veřejnoprávní	k2eAgInPc1d1	veřejnoprávní
subjekty	subjekt	k1gInPc1	subjekt
–	–	k?	–
domácí	domácí	k2eAgInSc1d1	domácí
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
a	a	k8xC	a
francouzské	francouzský	k2eAgNnSc1d1	francouzské
RFI	RFI	kA	RFI
(	(	kIx(	(
<g/>
Radio	radio	k1gNnSc1	radio
France	Franc	k1gMnSc2	Franc
Internationale	Internationale	k1gMnSc2	Internationale
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
udělila	udělit	k5eAaPmAgFnS	udělit
licenci	licence	k1gFnSc4	licence
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2025	[number]	k4	2025
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
provozovala	provozovat	k5eAaImAgFnS	provozovat
vysílání	vysílání	k1gNnSc4	vysílání
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
také	také	k9	také
britská	britský	k2eAgFnSc1d1	britská
BBC	BBC	kA	BBC
World	World	k1gInSc1	World
Service	Service	k1gFnSc1	Service
<g/>
.	.	kIx.	.
<g/>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
je	být	k5eAaImIp3nS	být
Radio	radio	k1gNnSc1	radio
Proglas	Proglasa	k1gFnPc2	Proglasa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nezisková	ziskový	k2eNgFnSc1d1	nezisková
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
technologie	technologie	k1gFnSc2	technologie
===	===	k?	===
</s>
</p>
<p>
<s>
1873	[number]	k4	1873
–	–	k?	–
James	James	k1gInSc1	James
Clerk	Clerk	k1gInSc1	Clerk
Maxwell	maxwell	k1gInSc1	maxwell
matematicky	matematicky	k6eAd1	matematicky
popsal	popsat	k5eAaPmAgInS	popsat
princip	princip	k1gInSc1	princip
šíření	šíření	k1gNnSc2	šíření
elektromagnetických	elektromagnetický	k2eAgFnPc2d1	elektromagnetická
vln	vlna	k1gFnPc2	vlna
</s>
</p>
<p>
<s>
1876	[number]	k4	1876
–	–	k?	–
Heinrich	Heinrich	k1gMnSc1	Heinrich
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hertz	hertz	k1gInSc1	hertz
poprvé	poprvé	k6eAd1	poprvé
dokázal	dokázat	k5eAaPmAgInS	dokázat
existenci	existence	k1gFnSc4	existence
těchto	tento	k3xDgFnPc2	tento
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
předchůdce	předchůdce	k1gMnSc4	předchůdce
dnešní	dnešní	k2eAgFnSc2d1	dnešní
dipólové	dipólový	k2eAgFnSc2d1	dipólová
antény	anténa	k1gFnSc2	anténa
</s>
</p>
<p>
<s>
1893	[number]	k4	1893
–	–	k?	–
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
předvádí	předvádět	k5eAaImIp3nS	předvádět
první	první	k4xOgFnSc4	první
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
demonstraci	demonstrace	k1gFnSc4	demonstrace
rádia	rádio	k1gNnSc2	rádio
a	a	k8xC	a
radiového	radiový	k2eAgNnSc2d1	radiové
spojení	spojení	k1gNnSc2	spojení
</s>
</p>
<p>
<s>
1895	[number]	k4	1895
–	–	k?	–
Alexandr	Alexandr	k1gMnSc1	Alexandr
Stěpanovič	Stěpanovič	k1gMnSc1	Stěpanovič
Popov	Popov	k1gInSc4	Popov
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
první	první	k4xOgFnSc4	první
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
telegrafickou	telegrafický	k2eAgFnSc4d1	telegrafická
stanici	stanice	k1gFnSc4	stanice
za	za	k7c4	za
využití	využití	k1gNnSc4	využití
hromosvodu	hromosvod	k1gInSc2	hromosvod
jako	jako	k8xS	jako
antény	anténa	k1gFnSc2	anténa
</s>
</p>
<p>
<s>
1896	[number]	k4	1896
–	–	k?	–
Guglielmo	Guglielma	k1gFnSc5	Guglielma
Marchese	Marchesa	k1gFnSc6	Marchesa
Marconi	Marcoň	k1gFnSc6	Marcoň
telegrafuje	telegrafovat	k5eAaBmIp3nS	telegrafovat
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
kilometry	kilometr	k1gInPc4	kilometr
za	za	k7c4	za
použití	použití	k1gNnPc4	použití
dodnes	dodnes	k6eAd1	dodnes
používaného	používaný	k2eAgInSc2d1	používaný
čtvrtvlnného	čtvrtvlnný	k2eAgInSc2d1	čtvrtvlnný
unipólu	unipól	k1gInSc2	unipól
(	(	kIx(	(
<g/>
využil	využít	k5eAaPmAgMnS	využít
uzemnění	uzemnění	k1gNnSc4	uzemnění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1906	[number]	k4	1906
–	–	k?	–
Valdemar	Valdemara	k1gFnPc2	Valdemara
Poulsen	Poulsna	k1gFnPc2	Poulsna
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
telegraf	telegraf	k1gInSc1	telegraf
pomocí	pomocí	k7c2	pomocí
netlumených	tlumený	k2eNgInPc2d1	netlumený
kmitů	kmit	k1gInPc2	kmit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vysílal	vysílat	k5eAaImAgInS	vysílat
morseovku	morseovka	k1gFnSc4	morseovka
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
frekvenci	frekvence	k1gFnSc6	frekvence
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
teoreticky	teoreticky	k6eAd1	teoreticky
schopen	schopen	k2eAgMnSc1d1	schopen
přenášet	přenášet	k5eAaImF	přenášet
řeč	řeč	k1gFnSc4	řeč
</s>
</p>
<p>
<s>
1907	[number]	k4	1907
–	–	k?	–
Quirino	Quirino	k1gNnSc1	Quirino
Majorana	Majorana	k1gFnSc1	Majorana
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
první	první	k4xOgInSc4	první
"	"	kIx"	"
<g/>
hovor	hovor	k1gInSc4	hovor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
75	[number]	k4	75
<g/>
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1910	[number]	k4	1910
–	–	k?	–
Lee	Lea	k1gFnSc6	Lea
de	de	k?	de
Forest	Forest	k1gFnSc1	Forest
snímá	snímat	k5eAaImIp3nS	snímat
zvuk	zvuk	k1gInSc4	zvuk
opery	opera	k1gFnSc2	opera
Carmen	Carmen	k2eAgInSc4d1	Carmen
a	a	k8xC	a
přenáší	přenášet	k5eAaImIp3nS	přenášet
jej	on	k3xPp3gMnSc4	on
rozhlasově	rozhlasově	k6eAd1	rozhlasově
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
–	–	k?	–
snadné	snadný	k2eAgNnSc4d1	snadné
zhotovení	zhotovení	k1gNnSc4	zhotovení
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
přijímače	přijímač	k1gInSc2	přijímač
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
krystalky	krystalka	k1gFnPc1	krystalka
<g/>
,	,	kIx,	,
učinilo	učinit	k5eAaImAgNnS	učinit
tuto	tento	k3xDgFnSc4	tento
technologii	technologie	k1gFnSc4	technologie
široce	široko	k6eAd1	široko
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
a	a	k8xC	a
elektronky	elektronka	k1gFnPc1	elektronka
umožnily	umožnit	k5eAaPmAgFnP	umožnit
prakticky	prakticky	k6eAd1	prakticky
libovolné	libovolný	k2eAgNnSc4d1	libovolné
zesílení	zesílení	k1gNnSc4	zesílení
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
===	===	k?	===
</s>
</p>
<p>
<s>
1910	[number]	k4	1910
–	–	k?	–
první	první	k4xOgInSc4	první
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
přenos	přenos	k1gInSc4	přenos
z	z	k7c2	z
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
–	–	k?	–
rozhlas	rozhlas	k1gInSc1	rozhlas
vysílá	vysílat	k5eAaImIp3nS	vysílat
výsledky	výsledek	k1gInPc4	výsledek
amerických	americký	k2eAgFnPc2d1	americká
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
</s>
</p>
<p>
<s>
1922	[number]	k4	1922
–	–	k?	–
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
zahájeno	zahájen	k2eAgNnSc1d1	zahájeno
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
rozhlasové	rozhlasový	k2eAgNnSc1d1	rozhlasové
vysílání	vysílání	k1gNnSc1	vysílání
BBC	BBC	kA	BBC
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
–	–	k?	–
ve	v	k7c6	v
Kbelích	Kbely	k1gInPc6	Kbely
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
začíná	začínat	k5eAaImIp3nS	začínat
vysílat	vysílat	k5eAaImF	vysílat
český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
"	"	kIx"	"
<g/>
Radiojournal	radiojournal	k1gInSc1	radiojournal
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
–	–	k?	–
První	první	k4xOgInSc1	první
přenos	přenos	k1gInSc1	přenos
při	při	k7c6	při
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
pořádaných	pořádaný	k2eAgInPc2d1	pořádaný
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
</s>
</p>
<p>
<s>
===	===	k?	===
Rozhlas	rozhlas	k1gInSc1	rozhlas
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
složitých	složitý	k2eAgInPc6d1	složitý
počátcích	počátek	k1gInPc6	počátek
vysílání	vysílání	k1gNnSc2	vysílání
českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
z	z	k7c2	z
vysílače	vysílač	k1gInSc2	vysílač
ve	v	k7c6	v
Kbelích	Kbely	k1gInPc6	Kbely
se	se	k3xPyFc4	se
studio	studio	k1gNnSc1	studio
několikrát	několikrát	k6eAd1	několikrát
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
a	a	k8xC	a
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
obtížnou	obtížný	k2eAgFnSc7d1	obtížná
finanční	finanční	k2eAgFnSc7d1	finanční
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
Radiojournal	radiojournal	k1gInSc1	radiojournal
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
navýšil	navýšit	k5eAaPmAgInS	navýšit
základní	základní	k2eAgNnSc4d1	základní
jmění	jmění	k1gNnSc4	jmění
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
další	další	k2eAgNnSc4d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
programové	programový	k2eAgFnSc2d1	programová
nabídky	nabídka	k1gFnSc2	nabídka
(	(	kIx(	(
<g/>
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
umělecký	umělecký	k2eAgInSc1d1	umělecký
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
<g/>
,	,	kIx,	,
vysílání	vysílání	k1gNnSc1	vysílání
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reportáž	reportáž	k1gFnSc1	reportáž
z	z	k7c2	z
VII	VII	kA	VII
<g/>
.	.	kIx.	.
všesokolského	všesokolský	k2eAgInSc2d1	všesokolský
sletu	slet	k1gInSc2	slet
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zprostředkovala	zprostředkovat	k5eAaPmAgFnS	zprostředkovat
posluchačům	posluchač	k1gMnPc3	posluchač
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
významné	významný	k2eAgFnPc4d1	významná
události	událost	k1gFnPc4	událost
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
rozhlasu	rozhlas	k1gInSc2	rozhlas
velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1926	[number]	k4	1926
český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
vysílal	vysílat	k5eAaImAgInS	vysílat
první	první	k4xOgFnSc4	první
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
reportáž	reportáž	k1gFnSc4	reportáž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
fotbalové	fotbalový	k2eAgNnSc1d1	fotbalové
utkání	utkání	k1gNnSc1	utkání
Slavie	slavie	k1gFnSc2	slavie
a	a	k8xC	a
Hungarie	Hungarie	k1gFnSc2	Hungarie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zahájil	zahájit	k5eAaPmAgMnS	zahájit
český	český	k2eAgInSc4d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
simultánní	simultánní	k2eAgNnSc1d1	simultánní
vysílání	vysílání	k1gNnSc1	vysílání
stejného	stejný	k2eAgInSc2d1	stejný
programu	program	k1gInSc2	program
z	z	k7c2	z
více	hodně	k6eAd2	hodně
vysílačů	vysílač	k1gInPc2	vysílač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
už	už	k6eAd1	už
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vlivnou	vlivný	k2eAgFnSc4d1	vlivná
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
zavedenou	zavedený	k2eAgFnSc4d1	zavedená
instituci	instituce	k1gFnSc4	instituce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rozhlasu	rozhlas	k1gInSc2	rozhlas
přicházeli	přicházet	k5eAaImAgMnP	přicházet
noví	nový	k2eAgMnPc1d1	nový
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
odbornou	odborný	k2eAgFnSc7d1	odborná
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
i	i	k8xC	i
inovativními	inovativní	k2eAgInPc7d1	inovativní
nápady	nápad	k1gInPc7	nápad
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
brněnská	brněnský	k2eAgFnSc1d1	brněnská
škola	škola	k1gFnSc1	škola
experimentovala	experimentovat	k5eAaImAgFnS	experimentovat
s	s	k7c7	s
vysíláním	vysílání	k1gNnSc7	vysílání
ze	z	k7c2	z
záznamu	záznam	k1gInSc2	záznam
a	a	k8xC	a
montážemi	montáž	k1gFnPc7	montáž
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlas	rozhlas	k1gInSc1	rozhlas
zařadil	zařadit	k5eAaPmAgInS	zařadit
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
pásma	pásmo	k1gNnSc2	pásmo
a	a	k8xC	a
školské	školský	k2eAgNnSc4d1	školské
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
nacisty	nacista	k1gMnSc2	nacista
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
rozhlas	rozhlas	k1gInSc1	rozhlas
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
sloužil	sloužit	k5eAaImAgMnS	sloužit
místo	místo	k7c2	místo
pošty	pošta	k1gFnSc2	pošta
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
<g/>
,	,	kIx,	,
zavedena	zaveden	k2eAgFnSc1d1	zavedena
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
redakce	redakce	k1gFnSc1	redakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
vysílalo	vysílat	k5eAaImAgNnS	vysílat
do	do	k7c2	do
komunistického	komunistický	k2eAgNnSc2d1	komunistické
Československa	Československo	k1gNnSc2	Československo
několik	několik	k4yIc4	několik
západních	západní	k2eAgFnPc2d1	západní
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
byly	být	k5eAaImAgFnP	být
Rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Hlas	hlas	k1gInSc1	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
nebo	nebo	k8xC	nebo
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
občanů	občan	k1gMnPc2	občan
zdrojem	zdroj	k1gInSc7	zdroj
necenzurovaných	cenzurovaný	k2eNgFnPc2d1	necenzurovaná
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
byl	být	k5eAaImAgInS	být
také	také	k9	také
signál	signál	k1gInSc1	signál
Rádia	rádio	k1gNnSc2	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
rušen	rušit	k5eAaImNgInS	rušit
rušičkami	rušička	k1gFnPc7	rušička
<g/>
,	,	kIx,	,
vysílání	vysílání	k1gNnSc1	vysílání
BBC	BBC	kA	BBC
nebylo	být	k5eNaImAgNnS	být
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
rušeno	rušit	k5eAaImNgNnS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
bylo	být	k5eAaImAgNnS	být
spuštěno	spuštěn	k2eAgNnSc1d1	spuštěno
FM	FM	kA	FM
vysílání	vysílání	k1gNnSc1	vysílání
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nastalo	nastat	k5eAaPmAgNnS	nastat
jisté	jistý	k2eAgNnSc4d1	jisté
uvolnění	uvolnění	k1gNnSc4	uvolnění
cenzury	cenzura	k1gFnSc2	cenzura
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
stereofonní	stereofonní	k2eAgNnSc1d1	stereofonní
vysílání	vysílání	k1gNnSc1	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
během	během	k7c2	během
invaze	invaze	k1gFnSc2	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
rozhlasem	rozhlas	k1gInSc7	rozhlas
zabito	zabít	k5eAaPmNgNnS	zabít
15	[number]	k4	15
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Rozhlas	rozhlas	k1gInSc1	rozhlas
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
snažil	snažit	k5eAaImAgMnS	snažit
informovat	informovat	k5eAaBmF	informovat
a	a	k8xC	a
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
národ	národ	k1gInSc4	národ
k	k	k7c3	k
nenásilnému	násilný	k2eNgInSc3d1	nenásilný
odporu	odpor	k1gInSc3	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Sovětským	sovětský	k2eAgMnPc3d1	sovětský
okupantům	okupant	k1gMnPc3	okupant
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zamezit	zamezit	k5eAaPmF	zamezit
svobodnému	svobodný	k2eAgNnSc3d1	svobodné
vysílání	vysílání	k1gNnSc3	vysílání
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
<g/>
rozhlas	rozhlas	k1gInSc1	rozhlas
sehrál	sehrát	k5eAaPmAgMnS	sehrát
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
okupace	okupace	k1gFnSc2	okupace
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
odporu	odpor	k1gInSc6	odpor
k	k	k7c3	k
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgInPc1d1	následný
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
roky	rok	k1gInPc1	rok
normalizace	normalizace	k1gFnSc2	normalizace
znamenaly	znamenat	k5eAaImAgInP	znamenat
odchod	odchod	k1gInSc4	odchod
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ČsRo	ČsRo	k6eAd1	ČsRo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhlas	rozhlas	k1gInSc1	rozhlas
sehrál	sehrát	k5eAaPmAgInS	sehrát
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
i	i	k9	i
při	při	k7c6	při
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
poměrně	poměrně	k6eAd1	poměrně
uvolňování	uvolňování	k1gNnSc4	uvolňování
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vstup	vstup	k1gInSc1	vstup
soukromých	soukromý	k2eAgNnPc2d1	soukromé
rádií	rádio	k1gNnPc2	rádio
do	do	k7c2	do
éteru	éter	k1gInSc2	éter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
rozhlas	rozhlas	k1gInSc1	rozhlas
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mladé	mladý	k2eAgNnSc1d1	mladé
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Radiojournal	radiojournal	k1gInSc1	radiojournal
vypsal	vypsat	k5eAaPmAgInS	vypsat
soutěž	soutěž	k1gFnSc4	soutěž
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
pojmenování	pojmenování	k1gNnSc4	pojmenování
rádia	rádio	k1gNnSc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
rozhlas	rozhlas	k1gInSc4	rozhlas
použil	použít	k5eAaPmAgMnS	použít
poprvé	poprvé	k6eAd1	poprvé
redaktor	redaktor	k1gMnSc1	redaktor
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
termín	termín	k1gInSc1	termín
radiožurnál	radiožurnál	k1gInSc1	radiožurnál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Média	médium	k1gNnPc1	médium
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgFnPc2d1	Česká
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
</s>
</p>
<p>
<s>
Zahraniční	zahraniční	k2eAgNnSc1d1	zahraniční
rozhlasové	rozhlasový	k2eAgNnSc1d1	rozhlasové
vysílání	vysílání	k1gNnSc1	vysílání
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
</p>
<p>
<s>
Rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
program	program	k1gInSc1	program
</s>
</p>
<p>
<s>
Pořad	pořad	k1gInSc4	pořad
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
rozhlas	rozhlas	k1gInSc1	rozhlas
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
rozhlas	rozhlas	k1gInSc1	rozhlas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
rozhlasových	rozhlasový	k2eAgMnPc2d1	rozhlasový
VKV	VKV	kA	VKV
vysílačů	vysílač	k1gMnPc2	vysílač
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Asociace	asociace	k1gFnSc1	asociace
provozovatelů	provozovatel	k1gMnPc2	provozovatel
soukromého	soukromý	k2eAgNnSc2d1	soukromé
vysílání	vysílání	k1gNnSc2	vysílání
(	(	kIx(	(
<g/>
APSV	APSV	kA	APSV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
(	(	kIx(	(
<g/>
ČRo	ČRo	k1gFnSc1	ČRo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rádio	rádio	k1gNnSc1	rádio
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
rádia	rádio	k1gNnSc2	rádio
na	na	k7c6	na
Rádiu	rádius	k1gInSc6	rádius
Junior	junior	k1gMnSc1	junior
</s>
</p>
