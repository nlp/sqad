<p>
<s>
Bank	bank	k1gInSc1	bank
of	of	k?	of
America	Americus	k1gMnSc2	Americus
Plaza	plaz	k1gMnSc2	plaz
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
72	[number]	k4	72
patry	patro	k1gNnPc7	patro
měří	měřit	k5eAaImIp3nP	měřit
281	[number]	k4	281
m.	m.	k?	m.
Budova	budova	k1gFnSc1	budova
nabízí	nabízet	k5eAaImIp3nS	nabízet
177	[number]	k4	177
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
prostor	prostora	k1gFnPc2	prostora
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
firmami	firma	k1gFnPc7	firma
JPJ	JPJ	kA	JPJ
Architects	Architectsa	k1gFnPc2	Architectsa
Inc	Inc	k1gFnPc2	Inc
<g/>
.	.	kIx.	.
a	a	k8xC	a
HLM	HLM	kA	HLM
Design	design	k1gInSc1	design
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
podle	podle	k7c2	podle
států	stát	k1gInPc2	stát
USA	USA	kA	USA
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bank	banka	k1gFnPc2	banka
of	of	k?	of
America	Americus	k1gMnSc2	Americus
Plaza	plaz	k1gMnSc2	plaz
(	(	kIx(	(
<g/>
Dallas	Dallas	k1gInSc1	Dallas
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
budovy	budova	k1gFnSc2	budova
</s>
</p>
<p>
<s>
záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
skyscraperpage	skyscraperpag	k1gFnSc2	skyscraperpag
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
Emporis	Emporis	k1gFnSc2	Emporis
</s>
</p>
