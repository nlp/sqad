<p>
<s>
Oxetan	Oxetan	k1gInSc1	Oxetan
(	(	kIx(	(
<g/>
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
1,3	[number]	k4	1,3
<g/>
-propylenoxid	ropylenoxida	k1gFnPc2	-propylenoxida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cyklická	cyklický	k2eAgFnSc1d1	cyklická
organická	organický	k2eAgFnSc1d1	organická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
izomer	izomer	k1gInSc1	izomer
propylenoxidu	propylenoxid	k1gInSc2	propylenoxid
<g/>
.	.	kIx.	.
</s>
<s>
Rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
etherech	ether	k1gInPc6	ether
a	a	k8xC	a
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
v	v	k7c6	v
acetonu	aceton	k1gInSc6	aceton
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
zahřáta	zahřát	k5eAaPmNgFnS	zahřát
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
rozkladu	rozklad	k1gInSc2	rozklad
vydává	vydávat	k5eAaPmIp3nS	vydávat
štiplavý	štiplavý	k2eAgInSc1d1	štiplavý
kouř	kouř	k1gInSc1	kouř
a	a	k8xC	a
dráždivé	dráždivý	k2eAgInPc1d1	dráždivý
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úniku	únik	k1gInSc6	únik
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
(	(	kIx(	(
<g/>
fotochemické	fotochemický	k2eAgFnPc1d1	fotochemická
reakce	reakce	k1gFnPc1	reakce
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc1d1	rychlý
výpar	výpar	k1gInSc1	výpar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Testování	testování	k1gNnPc1	testování
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
tuto	tento	k3xDgFnSc4	tento
sloučeninu	sloučenina	k1gFnSc4	sloučenina
jako	jako	k8xS	jako
vysoce	vysoce	k6eAd1	vysoce
karcinogenní	karcinogenní	k2eAgInPc4d1	karcinogenní
<g/>
,	,	kIx,	,
genotoxicita	genotoxicita	k1gFnSc1	genotoxicita
nebyla	být	k5eNaImAgFnS	být
zatím	zatím	k6eAd1	zatím
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Oxetan	Oxetan	k1gInSc1	Oxetan
je	být	k5eAaImIp3nS	být
využit	využít	k5eAaPmNgInS	využít
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
světlo-emitujících	světlomitující	k2eAgFnPc2d1	světlo-emitující
diod	dioda	k1gFnPc2	dioda
(	(	kIx(	(
<g/>
OLED	OLED	kA	OLED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
také	také	k9	také
při	při	k7c6	při
polymeraci	polymerace	k1gFnSc6	polymerace
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
potenciál	potenciál	k1gInSc4	potenciál
využití	využití	k1gNnSc2	využití
ve	v	k7c6	v
farmacii	farmacie	k1gFnSc6	farmacie
právě	právě	k9	právě
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
fotochemické	fotochemický	k2eAgFnSc3d1	fotochemická
reaktivnosti	reaktivnost	k1gFnSc3	reaktivnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
tvořit	tvořit	k5eAaImF	tvořit
jakousi	jakýsi	k3yIgFnSc4	jakýsi
kostru	kostra	k1gFnSc4	kostra
pro	pro	k7c4	pro
vznik	vznik	k1gInSc1	vznik
různorodým	různorodý	k2eAgInSc7d1	různorodý
komplexních	komplexní	k2eAgFnPc2d1	komplexní
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
léčiva	léčivo	k1gNnPc4	léčivo
<g/>
,	,	kIx,	,
ochranné	ochranný	k2eAgInPc4d1	ochranný
prostředky	prostředek	k1gInPc4	prostředek
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
apod.	apod.	kA	apod.
Látky	látka	k1gFnSc2	látka
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
oxetanem	oxetan	k1gInSc7	oxetan
se	se	k3xPyFc4	se
rychleji	rychle	k6eAd2	rychle
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
například	například	k6eAd1	například
tlumit	tlumit	k5eAaImF	tlumit
genovou	genový	k2eAgFnSc4d1	genová
expresi	exprese	k1gFnSc4	exprese
(	(	kIx(	(
<g/>
přepis	přepis	k1gInSc4	přepis
a	a	k8xC	a
translaci	translace	k1gFnSc4	translace
genů	gen	k1gInPc2	gen
do	do	k7c2	do
proteinů	protein	k1gInPc2	protein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
regulovat	regulovat	k5eAaImF	regulovat
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
protoonkogeny	protoonkogen	k1gInPc4	protoonkogen
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc4d1	způsobující
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
ale	ale	k9	ale
spíše	spíše	k9	spíše
v	v	k7c6	v
začátcích	začátek	k1gInPc6	začátek
a	a	k8xC	a
ani	ani	k8xC	ani
toxicita	toxicita	k1gFnSc1	toxicita
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
plně	plně	k6eAd1	plně
prozkoumána	prozkoumat	k5eAaPmNgNnP	prozkoumat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Oxetan	Oxetan	k1gInSc1	Oxetan
na	na	k7c4	na
PubChem	PubCh	k1gInSc7	PubCh
</s>
</p>
<p>
<s>
Oxetan	Oxetan	k1gInSc1	Oxetan
na	na	k7c4	na
Hazardous	Hazardous	k1gInSc4	Hazardous
Substances	Substancesa	k1gFnPc2	Substancesa
Data	datum	k1gNnSc2	datum
Bank	banka	k1gFnPc2	banka
</s>
</p>
<p>
<s>
Gómez-Bombarelli	Gómez-Bombarell	k1gMnPc1	Gómez-Bombarell
R	R	kA	R
<g/>
,	,	kIx,	,
et	et	k?	et
all	all	k?	all
<g/>
.	.	kIx.	.
</s>
<s>
Alkylating	Alkylating	k1gInSc1	Alkylating
potential	potential	k1gInSc1	potential
of	of	k?	of
oxetanes	oxetanes	k1gInSc1	oxetanes
<g/>
.	.	kIx.	.
</s>
<s>
Chem	Chem	k1gInSc1	Chem
Res	Res	k1gFnSc2	Res
Toxicol	Toxicola	k1gFnPc2	Toxicola
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
;	;	kIx,	;
<g/>
23	[number]	k4	23
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
1275	[number]	k4	1275
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
10.102	[number]	k4	10.102
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
tx	tx	k?	tx
<g/>
100153	[number]	k4	100153
<g/>
w.	w.	k?	w.
</s>
</p>
<p>
<s>
Wuitschik	Wuitschik	k1gInSc1	Wuitschik
G	G	kA	G
<g/>
,	,	kIx,	,
et	et	k?	et
all	all	k?	all
<g/>
.	.	kIx.	.
</s>
<s>
Oxetanes	Oxetanes	k1gMnSc1	Oxetanes
in	in	k?	in
drug	drug	k1gMnSc1	drug
discovery	discovera	k1gFnSc2	discovera
<g/>
:	:	kIx,	:
structural	structurat	k5eAaPmAgMnS	structurat
and	and	k?	and
synthetic	synthetice	k1gFnPc2	synthetice
insights	insights	k6eAd1	insights
<g/>
.	.	kIx.	.
</s>
<s>
J	J	kA	J
Med	med	k1gInSc1	med
Chem.	Chem.	k1gFnSc1	Chem.
2010	[number]	k4	2010
Apr	Apr	k1gFnSc1	Apr
22	[number]	k4	22
<g/>
;	;	kIx,	;
<g/>
53	[number]	k4	53
<g/>
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
3227	[number]	k4	3227
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
.	.	kIx.	.
doi	doi	k?	doi
<g/>
:	:	kIx,	:
10.102	[number]	k4	10.102
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
jm	jm	k?	jm
<g/>
9018788	[number]	k4	9018788
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wang	Wang	k1gInSc1	Wang
M	M	kA	M
<g/>
,	,	kIx,	,
et	et	k?	et
all	all	k?	all
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
oxetane	oxetat	k5eAaPmIp3nS	oxetat
ring	ring	k1gInSc4	ring
in	in	k?	in
taxol	taxol	k1gInSc1	taxol
<g/>
.	.	kIx.	.
</s>
<s>
J	J	kA	J
Org	Org	k1gFnSc1	Org
Chem.	Chem.	k1gFnSc1	Chem.
2000	[number]	k4	2000
Feb	Feb	k1gFnSc1	Feb
25	[number]	k4	25
<g/>
;	;	kIx,	;
<g/>
65	[number]	k4	65
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
1059	[number]	k4	1059
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pradeepkumar	Pradeepkumar	k1gMnSc1	Pradeepkumar
PI	pi	k0	pi
<g/>
,	,	kIx,	,
et	et	k?	et
all	all	k?	all
<g/>
.	.	kIx.	.
</s>
<s>
Synthesis	Synthesis	k1gFnSc1	Synthesis
<g/>
,	,	kIx,	,
physicochemical	physicochemicat	k5eAaPmAgMnS	physicochemicat
and	and	k?	and
biochemical	biochemicat	k5eAaPmAgInS	biochemicat
studies	studies	k1gInSc1	studies
of	of	k?	of
1	[number]	k4	1
<g/>
'	'	kIx"	'
<g/>
,2	,2	k4	,2
<g/>
'	'	kIx"	'
<g/>
-oxetane	xetat	k5eAaPmIp3nS	-oxetat
constrained	constrained	k1gInSc1	constrained
adenosine	adenosinout	k5eAaPmIp3nS	adenosinout
and	and	k?	and
guanosine	guanosinout	k5eAaPmIp3nS	guanosinout
modified	modified	k1gMnSc1	modified
oligonucleotides	oligonucleotides	k1gMnSc1	oligonucleotides
<g/>
,	,	kIx,	,
and	and	k?	and
their	their	k1gMnSc1	their
comparison	comparison	k1gMnSc1	comparison
with	with	k1gMnSc1	with
those	those	k1gFnSc2	those
of	of	k?	of
the	the	k?	the
corresponding	corresponding	k1gInSc1	corresponding
cytidine	cytidinout	k5eAaPmIp3nS	cytidinout
and	and	k?	and
thymidine	thymidinout	k5eAaPmIp3nS	thymidinout
analogues	analogues	k1gInSc1	analogues
<g/>
.	.	kIx.	.
</s>
<s>
J	J	kA	J
Am	Am	k1gMnSc1	Am
Chem	Chem	k1gMnSc1	Chem
Soc	soc	kA	soc
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
Sep	Sep	k1gFnSc1	Sep
22	[number]	k4	22
<g/>
;	;	kIx,	;
<g/>
126	[number]	k4	126
<g/>
(	(	kIx(	(
<g/>
37	[number]	k4	37
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
11484	[number]	k4	11484
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
</p>
