<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
telekomunikační	telekomunikační	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
telekomunikační	telekomunikační	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1865	#num#	k4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
Oficiální	oficiální	k2eAgNnSc1d1
web	web	k1gInSc4
</s>
<s>
www.itu.int	www.itu.int	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
telekomunikační	telekomunikační	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
ITU	ITU	kA
–	–	k?
International	International	k1gFnSc1
Telecommunication	Telecommunication	k1gInSc1
Union	union	k1gInSc1
<g/>
,	,	kIx,
UIT	UIT	kA
–	–	k?
Union	union	k1gInSc1
internationale	internationale	k6eAd1
des	des	k1gNnSc4
télécommunications	télécommunicationsa	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
specializovaná	specializovaný	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
OSN	OSN	kA
zabývající	zabývající	k2eAgInPc4d1
se	s	k7c7
problematikou	problematika	k1gFnSc7
informačních	informační	k2eAgFnPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
ITU	ITU	kA
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1865	#num#	k4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
jako	jako	k9
Mezinárodní	mezinárodní	k2eAgFnSc2d1
telegrafní	telegrafní	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
popud	popud	k1gInSc4
francouzské	francouzský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
z	z	k7c2
potřeby	potřeba	k1gFnSc2
koordinace	koordinace	k1gFnSc2
telegrafního	telegrafní	k2eAgInSc2d1
provozu	provoz	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgInSc2
dne	den	k1gInSc2
totiž	totiž	k9
byla	být	k5eAaImAgFnS
na	na	k7c6
Mezinárodní	mezinárodní	k2eAgFnSc6d1
telegrafní	telegrafní	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
podepsána	podepsán	k2eAgFnSc1d1
Mezinárodní	mezinárodní	k2eAgFnSc1d1
telegrafní	telegrafní	k2eAgFnSc1d1
konvence	konvence	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zakládající	zakládající	k2eAgInSc4d1
dokument	dokument	k1gInSc4
podepsaly	podepsat	k5eAaPmAgInP
evropské	evropský	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
i	i	k8xC
francouzský	francouzský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
a	a	k8xC
ruský	ruský	k2eAgMnSc1d1
car	car	k1gMnSc1
<g/>
)	)	kIx)
vyjma	vyjma	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
tím	ten	k3xDgNnSc7
přijata	přijat	k2eAgFnSc1d1
mezinárodní	mezinárodní	k2eAgFnSc1d1
Morseova	Morseův	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
(	(	kIx(
<g/>
odlišná	odlišný	k2eAgFnSc1d1
od	od	k7c2
původní	původní	k2eAgFnSc2d1
americké	americký	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
působnost	působnost	k1gFnSc1
ITU	ITU	kA
rozšířila	rozšířit	k5eAaPmAgFnS
na	na	k7c4
celý	celý	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vynálezu	vynález	k1gInSc6
telefonu	telefon	k1gInSc2
a	a	k8xC
rádia	rádio	k1gNnSc2
se	se	k3xPyFc4
její	její	k3xOp3gFnSc1
působnost	působnost	k1gFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
o	o	k7c4
koordinaci	koordinace	k1gFnSc4
telefonního	telefonní	k2eAgNnSc2d1
a	a	k8xC
později	pozdě	k6eAd2
radiokomunikačního	radiokomunikační	k2eAgInSc2d1
provozu	provoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
Mezinárodní	mezinárodní	k2eAgInSc1d1
poradní	poradní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
pro	pro	k7c4
telegraf	telegraf	k1gInSc4
–	–	k?
CCIT	CCIT	kA
a	a	k8xC
Mezinárodní	mezinárodní	k2eAgInSc1d1
poradní	poradní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
pro	pro	k7c4
telefon	telefon	k1gInSc4
–	–	k?
CCIF	CCIF	kA
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
spojeny	spojit	k5eAaPmNgInP
pod	pod	k7c7
názvem	název	k1gInSc7
CCITT	CCITT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1927	#num#	k4
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
Mezinárodní	mezinárodní	k2eAgInSc1d1
poradní	poradní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
pro	pro	k7c4
radiokomunikace	radiokomunikace	k1gFnPc4
–	–	k?
CCIR	CCIR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Unie	unie	k1gFnSc1
svůj	svůj	k3xOyFgInSc4
dnešní	dnešní	k2eAgInSc4d1
název	název	k1gInSc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
odbornou	odborný	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
OSN	OSN	kA
(	(	kIx(
<g/>
Specialized	Specialized	k1gMnSc1
Agency	Agenca	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
Mezinárodní	mezinárodní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
zápis	zápis	k1gInSc4
kmitočtů	kmitočet	k1gInPc2
–	–	k?
IFRB	IFRB	kA
(	(	kIx(
<g/>
International	International	k1gMnSc2
Frequency	Frequenca	k1gMnSc2
Radio	radio	k1gNnSc1
Board	Board	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ITU	ITU	kA
je	být	k5eAaImIp3nS
ze	z	k7c2
všech	všecek	k3xTgFnPc2
organizací	organizace	k1gFnPc2
OSN	OSN	kA
nejstarší	starý	k2eAgFnPc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlem	sídlo	k1gNnSc7
ITU	ITU	kA
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Ženeva	Ženeva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
orgánem	orgán	k1gInSc7
je	být	k5eAaImIp3nS
Konference	konference	k1gFnSc1
vládních	vládní	k2eAgMnPc2d1
zmocněnců	zmocněnec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
a	a	k8xC
činnosti	činnost	k1gFnSc2
</s>
<s>
Současná	současný	k2eAgFnSc1d1
organizační	organizační	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
tři	tři	k4xCgInPc4
sektory	sektor	k1gInPc4
Unie	unie	k1gFnSc1
–	–	k?
Radiokomunikace	radiokomunikace	k1gFnSc1
(	(	kIx(
<g/>
ITU-R	ITU-R	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
CCIR	CCIR	kA
<g/>
,	,	kIx,
Normalizace	normalizace	k1gFnSc1
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
(	(	kIx(
<g/>
ITU-T	ITU-T	k1gFnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
CCITT	CCITT	kA
<g/>
)	)	kIx)
a	a	k8xC
Rozvoj	rozvoj	k1gInSc1
telekomunikací	telekomunikace	k1gFnPc2
(	(	kIx(
<g/>
ITU-D	ITU-D	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
ze	z	k7c2
tří	tři	k4xCgInPc2
sektorů	sektor	k1gInPc2
ITU	ITU	kA
pracuje	pracovat	k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
konferencí	konference	k1gFnPc2
a	a	k8xC
zasedání	zasedání	k1gNnSc6
studijních	studijní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
delegáti	delegát	k1gMnPc1
z	z	k7c2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
vypracovávají	vypracovávat	k5eAaImIp3nP
doporučení	doporučený	k2eAgMnPc1d1
sloužící	sloužící	k1gMnPc1
jako	jako	k8xC,k8xS
základ	základ	k1gInSc1
pro	pro	k7c4
koordinovaný	koordinovaný	k2eAgInSc4d1
provoz	provoz	k1gInSc4
globálních	globální	k2eAgFnPc2d1
telekomunikačních	telekomunikační	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Studijní	studijní	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
<g/>
,	,	kIx,
vytvořené	vytvořený	k2eAgFnPc1d1
z	z	k7c2
expertů	expert	k1gMnPc2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
důležitých	důležitý	k2eAgFnPc2d1
telekomunikačních	telekomunikační	k2eAgFnPc2d1
provozních	provozní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
a	a	k8xC
výzkumných	výzkumný	k2eAgInPc2d1
a	a	k8xC
vědeckých	vědecký	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
zajišťují	zajišťovat	k5eAaImIp3nP
technickou	technický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
Unie	unie	k1gFnSc2
a	a	k8xC
připravují	připravovat	k5eAaImIp3nP
podrobné	podrobný	k2eAgFnPc1d1
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
jsou	být	k5eAaImIp3nP
základem	základ	k1gInSc7
pro	pro	k7c4
vytvoření	vytvoření	k1gNnSc4
uznávaných	uznávaný	k2eAgNnPc2d1
doporučení	doporučení	k1gNnPc2
ITU	ITU	kA
<g/>
.	.	kIx.
</s>
<s>
ITU-R	ITU-R	k?
</s>
<s>
ITU-R	ITU-R	k?
vypracovává	vypracovávat	k5eAaImIp3nS
technické	technický	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
pozemních	pozemní	k2eAgFnPc2d1
a	a	k8xC
kosmických	kosmický	k2eAgFnPc2d1
bezdrátových	bezdrátový	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
a	a	k8xC
systémů	systém	k1gInPc2
a	a	k8xC
vypracovává	vypracovávat	k5eAaImIp3nS
provozní	provozní	k2eAgInPc4d1
postupy	postup	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
vypracovává	vypracovávat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
technické	technický	k2eAgFnPc4d1
studie	studie	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
slouží	sloužit	k5eAaImIp3nP
jako	jako	k9
základ	základ	k1gInSc4
pro	pro	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
v	v	k7c6
právních	právní	k2eAgInPc6d1
předpisech	předpis	k1gInPc6
projednávaných	projednávaný	k2eAgInPc2d1
na	na	k7c6
radiokomunikačních	radiokomunikační	k2eAgFnPc6d1
konferencích	konference	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
ITU-T	ITU-T	k?
</s>
<s>
V	v	k7c6
ITU-T	ITU-T	k1gFnSc6
experti	expert	k1gMnPc1
připravují	připravovat	k5eAaImIp3nP
technické	technický	k2eAgFnPc4d1
specifikace	specifikace	k1gFnPc4
pro	pro	k7c4
telekomunikační	telekomunikační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
sítě	síť	k1gFnPc4
a	a	k8xC
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
jejich	jejich	k3xOp3gInSc2
provozu	provoz	k1gInSc2
<g/>
,	,	kIx,
fungování	fungování	k1gNnSc2
a	a	k8xC
údržby	údržba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
práce	práce	k1gFnSc2
rovněž	rovněž	k9
pokrývá	pokrývat	k5eAaImIp3nS
vytvoření	vytvoření	k1gNnSc4
tarifních	tarifní	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
včetně	včetně	k7c2
metod	metoda	k1gFnPc2
účtování	účtování	k1gNnPc2
používaných	používaný	k2eAgMnPc2d1
při	při	k7c6
poskytování	poskytování	k1gNnSc6
mezinárodních	mezinárodní	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
ITU-D	ITU-D	k?
</s>
<s>
Experti	expert	k1gMnPc1
z	z	k7c2
ITU-D	ITU-D	k1gMnSc2
zaměřují	zaměřovat	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
na	na	k7c4
přípravu	příprava	k1gFnSc4
doporučení	doporučení	k1gNnPc2
<g/>
,	,	kIx,
názorů	názor	k1gInPc2
<g/>
,	,	kIx,
směrnic	směrnice	k1gFnPc2
<g/>
,	,	kIx,
příruček	příručka	k1gFnPc2
a	a	k8xC
zpráv	zpráva	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
poskytují	poskytovat	k5eAaImIp3nP
pracovníkům	pracovník	k1gMnPc3
provádějícím	provádějící	k2eAgInSc7d1
rozhodování	rozhodování	k1gNnSc4
v	v	k7c6
rozvojových	rozvojový	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
týkající	týkající	k2eAgFnSc4d1
se	se	k3xPyFc4
vývoje	vývoj	k1gInPc1
strategií	strategie	k1gFnPc2
a	a	k8xC
politiky	politika	k1gFnSc2
pro	pro	k7c4
management	management	k1gInSc4
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Každý	každý	k3xTgInSc1
sektor	sektor	k1gInSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
aparát	aparát	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
realizaci	realizace	k1gFnSc4
pracovního	pracovní	k2eAgInSc2d1
plánu	plán	k1gInSc2
sektoru	sektor	k1gInSc2
a	a	k8xC
koordinuje	koordinovat	k5eAaBmIp3nS
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
telekomunikací	telekomunikace	k1gFnPc2
</s>
<s>
Úřad	úřad	k1gInSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
telekomunikací	telekomunikace	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Telecommunication	Telecommunication	k1gInSc1
Standardization	Standardization	k1gInSc1
Bureau	Bureaa	k1gMnSc4
<g/>
,	,	kIx,
TSB	TSB	kA
<g/>
)	)	kIx)
poskytuje	poskytovat	k5eAaImIp3nS
sekretariát	sekretariát	k1gInSc1
pro	pro	k7c4
práci	práce	k1gFnSc4
v	v	k7c6
sektoru	sektor	k1gInSc6
ITU-T	ITU-T	k1gFnSc2
a	a	k8xC
služby	služba	k1gFnSc2
pro	pro	k7c4
účastníky	účastník	k1gMnPc4
prací	prací	k2eAgFnSc2d1
ITU-T	ITU-T	k1gFnSc2
<g/>
,	,	kIx,
rozšiřuje	rozšiřovat	k5eAaImIp3nS
informace	informace	k1gFnPc4
o	o	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
telekomunikacích	telekomunikace	k1gFnPc6
a	a	k8xC
vypracovává	vypracovávat	k5eAaImIp3nS
dohody	dohoda	k1gFnSc2
s	s	k7c7
mnoha	mnoho	k4c2
mezinárodními	mezinárodní	k2eAgFnPc7d1
normalizačními	normalizační	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
ITU-T	ITU-T	k?
(	(	kIx(
<g/>
normalizace	normalizace	k1gFnSc2
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
<g/>
)	)	kIx)
zajišťuje	zajišťovat	k5eAaImIp3nS
tyto	tento	k3xDgFnPc4
činnosti	činnost	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
udržuje	udržovat	k5eAaImIp3nS
ITU-T	ITU-T	k1gMnSc7
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
<g/>
,	,	kIx,
rozšiřuje	rozšiřovat	k5eAaImIp3nS
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
informace	informace	k1gFnPc4
o	o	k7c6
činnostech	činnost	k1gFnPc6
sektoru	sektor	k1gInSc2
včetně	včetně	k7c2
plánu	plán	k1gInSc2
zasedání	zasedání	k1gNnSc2
<g/>
,	,	kIx,
oběžníků	oběžník	k1gInPc2
TSB	TSB	kA
a	a	k8xC
všech	všecek	k3xTgInPc2
pracovních	pracovní	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
aktualizuje	aktualizovat	k5eAaBmIp3nS
seznam	seznam	k1gInSc4
doporučení	doporučení	k1gNnSc4
ITU-T	ITU-T	k1gFnSc2
<g/>
,	,	kIx,
databázi	databáze	k1gFnSc4
s	s	k7c7
programem	program	k1gInSc7
prací	práce	k1gFnPc2
ITU-T	ITU-T	k1gMnPc3
<g/>
,	,	kIx,
databázi	databáze	k1gFnSc3
vyhlášení	vyhlášení	k1gNnSc2
patentů	patent	k1gInPc2
ITU-T	ITU-T	k1gMnPc3
a	a	k8xC
databázi	databáze	k1gFnSc3
termínů	termín	k1gInPc2
a	a	k8xC
definic	definice	k1gFnPc2
ITU-T	ITU-T	k1gFnSc2
(	(	kIx(
<g/>
SANCHO	SANCHO	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
poskytuje	poskytovat	k5eAaImIp3nS
a	a	k8xC
aktualizuje	aktualizovat	k5eAaBmIp3nS
jakékoliv	jakýkoliv	k3yIgFnPc4
jiné	jiný	k2eAgFnPc4d1
databáze	databáze	k1gFnPc4
požadované	požadovaný	k2eAgFnPc4d1
různými	různý	k2eAgFnPc7d1
studijními	studijní	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
;	;	kIx,
</s>
<s>
soustřeďuje	soustřeďovat	k5eAaImIp3nS
a	a	k8xC
poskytuje	poskytovat	k5eAaImIp3nS
informace	informace	k1gFnPc4
o	o	k7c6
alternativních	alternativní	k2eAgInPc6d1
postupech	postup	k1gInPc6
volání	volání	k1gNnPc2
včetně	včetně	k7c2
volání	volání	k1gNnPc2
zpět	zpět	k6eAd1
<g/>
;	;	kIx,
</s>
<s>
poskytuje	poskytovat	k5eAaImIp3nS
přidělování	přidělování	k1gNnSc1
kódu	kód	k1gInSc2
zemí	zem	k1gFnPc2
pro	pro	k7c4
telefon	telefon	k1gInSc4
<g/>
,	,	kIx,
data	datum	k1gNnPc4
a	a	k8xC
další	další	k2eAgFnPc4d1
služby	služba	k1gFnPc4
<g/>
;	;	kIx,
</s>
<s>
působí	působit	k5eAaImIp3nS
jako	jako	k9
registrační	registrační	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
univerzální	univerzální	k2eAgNnPc4d1
bezplatná	bezplatný	k2eAgNnPc4d1
telefonní	telefonní	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
;	;	kIx,
</s>
<s>
poskytuje	poskytovat	k5eAaImIp3nS
technické	technický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
telekomunikacích	telekomunikace	k1gFnPc6
a	a	k8xC
spolupracuje	spolupracovat	k5eAaImIp3nS
úzce	úzko	k6eAd1
se	s	k7c7
sektorem	sektor	k1gInSc7
ITU	ITU	kA
pro	pro	k7c4
radiokomunikace	radiokomunikace	k1gFnPc4
a	a	k8xC
sektorem	sektor	k1gInSc7
ITU-D	ITU-D	k1gFnPc2
při	při	k7c6
záležitostech	záležitost	k1gFnPc6
zajímajících	zajímající	k2eAgMnPc2d1
rozvojové	rozvojový	k2eAgFnPc4d1
země	zem	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
poskytuje	poskytovat	k5eAaImIp3nS
administrativní	administrativní	k2eAgFnSc1d1
a	a	k8xC
provozní	provozní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
prostřednictvím	prostřednictvím	k7c2
věstníku	věstník	k1gInSc2
ITU	ITU	kA
Operational	Operational	k1gFnSc1
Bulletin	bulletin	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
zajišťuje	zajišťovat	k5eAaImIp3nS
redakční	redakční	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
doporučení	doporučení	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
textů	text	k1gInPc2
schválených	schválený	k2eAgFnPc2d1
ITU-T	ITU-T	k1gFnPc2
včetně	včetně	k7c2
jejich	jejich	k3xOp3gFnSc2
distribuce	distribuce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1
poštovní	poštovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
sekretariátu	sekretariát	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
vydala	vydat	k5eAaPmAgFnS
první	první	k4xOgFnSc1
edice	edice	k1gFnSc1
známek	známka	k1gFnPc2
ve	v	k7c6
švýcarské	švýcarský	k2eAgFnSc6d1
měně	měna	k1gFnSc6
a	a	k8xC
typickém	typický	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
opatřeny	opatřen	k2eAgInPc1d1
nápisem	nápis	k1gInSc7
UNION	union	k1gInSc1
INTERNATIONALE	INTERNATIONALE	kA
DES	des	k1gNnPc2
TELECOMMUNICATIONS	TELECOMMUNICATIONS	kA
a	a	k8xC
dalším	další	k2eAgNnSc7d1
označením	označení	k1gNnSc7
HELVETIA	Helvetia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motivy	motiv	k1gInPc7
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
z	z	k7c2
oblasti	oblast	k1gFnSc2
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
se	s	k7c7
zobrazením	zobrazení	k1gNnSc7
budovy	budova	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
úřad	úřad	k1gInSc1
sídlí	sídlet	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.itu.int/en/history/Pages/ITUBorn1865.aspx	https://www.itu.int/en/history/Pages/ITUBorn1865.aspx	k1gInSc1
-	-	kIx~
The	The	k1gFnSc1
1865	#num#	k4
International	International	k1gMnSc1
Telegraph	Telegraph	k1gMnSc1
Conference	Conference	k1gFnSc2
<g/>
↑	↑	k?
HLINKA	Hlinka	k1gMnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
<g/>
;	;	kIx,
MUCHA	Mucha	k1gMnSc1
<g/>
,	,	kIx,
Ludvík	Ludvík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filatelistický	filatelistický	k2eAgInSc1d1
atlas	atlas	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
<g/>
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
GKP	GKP	kA
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
217	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mezinárodní	mezinárodní	k2eAgFnSc2d1
telekomunikační	telekomunikační	k2eAgFnSc2d1
unie	unie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Organizace	organizace	k1gFnSc1
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
Systém	systém	k1gInSc1
OSN	OSN	kA
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
•	•	k?
Pozorovatelé	pozorovatel	k1gMnPc1
•	•	k?
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
•	•	k?
Rada	rada	k1gFnSc1
bezpečnosti	bezpečnost	k1gFnSc2
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
a	a	k8xC
sociální	sociální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
•	•	k?
Sekretariát	sekretariát	k1gInSc1
(	(	kIx(
<g/>
Generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Poručenská	poručenský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgInSc1d1
soudní	soudní	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
Hlavní	hlavní	k2eAgInSc1d1
sídla	sídlo	k1gNnSc2
</s>
<s>
New	New	k?
York	York	k1gInSc1
•	•	k?
Ženeva	Ženeva	k1gFnSc1
•	•	k?
Nairobi	Nairobi	k1gNnSc2
•	•	k?
Vídeň	Vídeň	k1gFnSc1
Programy	program	k1gInPc4
<g/>
,	,	kIx,
fondya	fondya	k1gMnSc1
agentury	agentura	k1gFnSc2
</s>
<s>
FAO	FAO	kA
•	•	k?
ICAO	ICAO	kA
•	•	k?
ILO	ILO	kA
•	•	k?
IPBES	IPBES	kA
•	•	k?
IPCC	IPCC	kA
•	•	k?
MAAE	MAAE	kA
•	•	k?
UNIDO	UNIDO	kA
•	•	k?
ITU	ITU	kA
•	•	k?
UNAIDS	UNAIDS	kA
•	•	k?
SCSL	SCSL	kA
•	•	k?
UNCTAD	UNCTAD	kA
•	•	k?
UNCITRAL	UNCITRAL	kA
•	•	k?
UNDCP	UNDCP	kA
•	•	k?
UNDP	UNDP	kA
•	•	k?
UNEP	UNEP	kA
•	•	k?
UNESCO	UNESCO	kA
•	•	k?
UNODC	UNODC	kA
•	•	k?
UNFIP	UNFIP	kA
•	•	k?
UNIFEM	UNIFEM	kA
•	•	k?
UNFPA	UNFPA	kA
•	•	k?
OHCHR	OHCHR	kA
•	•	k?
UNHCR	UNHCR	kA
•	•	k?
UNHRC	UNHRC	kA
•	•	k?
HABITAT	HABITAT	kA
•	•	k?
UNICEF	UNICEF	kA
•	•	k?
UNRWA	UNRWA	kA
•	•	k?
UPU	UPU	kA
•	•	k?
WFP	WFP	kA
•	•	k?
WHO	WHO	kA
•	•	k?
WMO	WMO	kA
Rezoluce	rezoluce	k1gFnSc2
</s>
<s>
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
(	(	kIx(
<g/>
Všeobecná	všeobecný	k2eAgFnSc1d1
deklarace	deklarace	k1gFnSc1
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
)	)	kIx)
•	•	k?
Rada	rada	k1gFnSc1
bezpečnosti	bezpečnost	k1gFnSc2
Související	související	k2eAgNnSc4d1
témata	téma	k1gNnPc4
</s>
<s>
Společnost	společnost	k1gFnSc1
národů	národ	k1gInPc2
•	•	k?
Dějiny	dějiny	k1gFnPc4
OSN	OSN	kA
•	•	k?
Vlajka	vlajka	k1gFnSc1
OSN	OSN	kA
•	•	k?
Charta	charta	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
•	•	k?
Mezinárodní	mezinárodní	k2eAgInSc1d1
trestní	trestní	k2eAgInSc1d1
soud	soud	k1gInSc1
•	•	k?
OPCW	OPCW	kA
•	•	k?
Mírové	mírový	k2eAgFnSc2d1
mise	mise	k1gFnSc2
OSN	OSN	kA
•	•	k?
Den	den	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
•	•	k?
Den	den	k1gInSc1
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
Kategorie	kategorie	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Organizace	organizace	k1gFnSc1
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
171661	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2014969-4	2014969-4	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2187	#num#	k4
1858	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80089972	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
152495097	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80089972	#num#	k4
</s>
