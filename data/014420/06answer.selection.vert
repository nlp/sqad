<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
telekomunikační	telekomunikační	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
ITU	ITU	kA
–	–	k?
International	International	k1gFnSc1
Telecommunication	Telecommunication	k1gInSc1
Union	union	k1gInSc1
<g/>
,	,	kIx,
UIT	UIT	kA
–	–	k?
Union	union	k1gInSc1
internationale	internationale	k6eAd1
des	des	k1gNnSc4
télécommunications	télécommunicationsa	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
specializovaná	specializovaný	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
OSN	OSN	kA
zabývající	zabývající	k2eAgInPc4d1
se	s	k7c7
problematikou	problematika	k1gFnSc7
informačních	informační	k2eAgFnPc2d1
a	a	k8xC
komunikačních	komunikační	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>