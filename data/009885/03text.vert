<p>
<s>
Nikl-metal	Nikletat	k5eAaPmAgInS	Nikl-metat
hydridový	hydridový	k2eAgInSc1d1	hydridový
akumulátor	akumulátor	k1gInSc1	akumulátor
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
NiMH	NiMH	k1gFnSc1	NiMH
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
galvanického	galvanický	k2eAgInSc2d1	galvanický
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejčastěji	často	k6eAd3	často
používaných	používaný	k2eAgInPc2d1	používaný
druhů	druh	k1gInPc2	druh
akumulátorů	akumulátor	k1gInPc2	akumulátor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jemu	on	k3xPp3gMnSc3	on
podobným	podobný	k2eAgInSc7d1	podobný
nikl-kadmiovým	nikladmiový	k2eAgInSc7d1	nikl-kadmiový
akumulátorem	akumulátor	k1gInSc7	akumulátor
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
důvody	důvod	k1gInPc7	důvod
jeho	jeho	k3xOp3gNnSc2	jeho
velkého	velký	k2eAgNnSc2d1	velké
rozšíření	rozšíření	k1gNnSc2	rozšíření
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
značně	značně	k6eAd1	značně
velká	velký	k2eAgFnSc1d1	velká
kapacita	kapacita	k1gFnSc1	kapacita
a	a	k8xC	a
schopnost	schopnost	k1gFnSc1	schopnost
dodávat	dodávat	k5eAaImF	dodávat
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc4d1	velký
proud	proud	k1gInSc4	proud
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přijatelnou	přijatelný	k2eAgFnSc7d1	přijatelná
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Určité	určitý	k2eAgNnSc1d1	určité
omezení	omezení	k1gNnSc1	omezení
představuje	představovat	k5eAaImIp3nS	představovat
jeho	jeho	k3xOp3gNnSc4	jeho
napětí	napětí	k1gNnSc4	napětí
1,2	[number]	k4	1,2
V	V	kA	V
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgNnSc4d2	nižší
než	než	k8xS	než
napětí	napětí	k1gNnSc4	napětí
běžných	běžný	k2eAgFnPc2d1	běžná
baterií	baterie	k1gFnPc2	baterie
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
použití	použití	k1gNnSc4	použití
s	s	k7c7	s
1,5	[number]	k4	1,5
V	V	kA	V
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektrochemická	elektrochemický	k2eAgFnSc1d1	elektrochemická
reakce	reakce	k1gFnSc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
Záporná	záporný	k2eAgFnSc1d1	záporná
elektroda	elektroda	k1gFnSc1	elektroda
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
speciální	speciální	k2eAgFnSc7d1	speciální
kovovou	kovový	k2eAgFnSc7d1	kovová
slitinou	slitina	k1gFnSc7	slitina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
směs	směs	k1gFnSc1	směs
hydridů	hydrid	k1gInPc2	hydrid
neurčitého	určitý	k2eNgNnSc2d1	neurčité
složení	složení	k1gNnSc2	složení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
slitina	slitina	k1gFnSc1	slitina
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
vzácných	vzácný	k2eAgInPc2d1	vzácný
kovů	kov	k1gInPc2	kov
–	–	k?	–
lanthanu	lanthan	k1gInSc6	lanthan
<g/>
,	,	kIx,	,
ceru	cer	k1gInSc6	cer
<g/>
,	,	kIx,	,
neodymu	neodym	k1gInSc6	neodym
<g/>
,	,	kIx,	,
praseodymu	praseodym	k1gInSc6	praseodym
<g/>
.	.	kIx.	.
</s>
<s>
Kladná	kladný	k2eAgFnSc1d1	kladná
elektroda	elektroda	k1gFnSc1	elektroda
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
oxid-hydroxidu	oxidydroxid	k1gInSc2	oxid-hydroxid
niklitého	niklitý	k2eAgInSc2d1	niklitý
–	–	k?	–
NiO	NiO	k1gFnSc1	NiO
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektrolytem	elektrolyt	k1gInSc7	elektrolyt
je	být	k5eAaImIp3nS	být
vodný	vodný	k2eAgInSc1d1	vodný
roztok	roztok	k1gInSc1	roztok
hydroxidu	hydroxid	k1gInSc2	hydroxid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
reakce	reakce	k1gFnSc1	reakce
vybíjení	vybíjení	k1gNnSc2	vybíjení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
MH	MH	kA	MH
+	+	kIx~	+
NiO	NiO	k1gMnSc1	NiO
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
→	→	k?	→
M	M	kA	M
+	+	kIx~	+
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
Na	na	k7c6	na
záporné	záporný	k2eAgFnSc6d1	záporná
elektroděMH	elektroděMH	k?	elektroděMH
+	+	kIx~	+
OH	OH	kA	OH
<g/>
−	−	k?	−
→	→	k?	→
M	M	kA	M
+	+	kIx~	+
H2O	H2O	k1gMnSc1	H2O
+	+	kIx~	+
e	e	k0	e
<g/>
−	−	k?	−
<g/>
Na	na	k7c4	na
kladné	kladný	k2eAgFnPc4d1	kladná
elektroděNiO	elektroděNiO	k?	elektroděNiO
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
+	+	kIx~	+
H2O	H2O	k1gFnSc1	H2O
+	+	kIx~	+
e	e	k0	e
<g/>
−	−	k?	−
→	→	k?	→
Ni	on	k3xPp3gFnSc4	on
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
OH	OH	kA	OH
<g/>
−	−	k?	−
<g/>
Kde	kde	k9	kde
M	M	kA	M
a	a	k8xC	a
MH	MH	kA	MH
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
případně	případně	k6eAd1	případně
navázaným	navázaný	k2eAgInSc7d1	navázaný
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nabíjení	nabíjení	k1gNnSc6	nabíjení
probíhají	probíhat	k5eAaImIp3nP	probíhat
uvedené	uvedený	k2eAgFnSc2d1	uvedená
reakce	reakce	k1gFnSc2	reakce
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Jmenovité	jmenovitý	k2eAgNnSc1d1	jmenovité
napětí	napětí	k1gNnSc1	napětí
je	být	k5eAaImIp3nS	být
1,2	[number]	k4	1,2
V.	V.	kA	V.
Napětí	napětí	k1gNnSc1	napětí
(	(	kIx(	(
<g/>
naprázdno	naprázdno	k6eAd1	naprázdno
<g/>
)	)	kIx)	)
plně	plně	k6eAd1	plně
nabitého	nabitý	k2eAgInSc2d1	nabitý
článku	článek	k1gInSc2	článek
je	být	k5eAaImIp3nS	být
1,4	[number]	k4	1,4
V	V	kA	V
<g/>
;	;	kIx,	;
napětí	napětí	k1gNnSc4	napětí
vybitého	vybitý	k2eAgInSc2d1	vybitý
článku	článek	k1gInSc2	článek
je	být	k5eAaImIp3nS	být
1,0	[number]	k4	1,0
V.	V.	kA	V.
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
akumulátoru	akumulátor	k1gInSc2	akumulátor
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
velká	velký	k2eAgFnSc1d1	velká
úroveň	úroveň	k1gFnSc1	úroveň
samovybíjení	samovybíjení	k1gNnSc2	samovybíjení
–	–	k?	–
asi	asi	k9	asi
15-30	[number]	k4	15-30
%	%	kIx~	%
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
se	se	k3xPyFc4	se
samovybíjení	samovybíjení	k1gNnSc1	samovybíjení
podstatně	podstatně	k6eAd1	podstatně
sníží	snížit	k5eAaPmIp3nS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgFnSc1d1	uvedená
úroveň	úroveň	k1gFnSc1	úroveň
samovybíjení	samovybíjení	k1gNnSc2	samovybíjení
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
klasických	klasický	k2eAgFnPc2d1	klasická
NiMH	NiMH	k1gFnPc2	NiMH
akumulátorů	akumulátor	k1gInPc2	akumulátor
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kapacitou	kapacita	k1gFnSc7	kapacita
(	(	kIx(	(
<g/>
2500	[number]	k4	2500
-	-	kIx~	-
2700	[number]	k4	2700
mAh	mAh	k?	mAh
u	u	k7c2	u
AA	AA	kA	AA
a	a	k8xC	a
1000	[number]	k4	1000
-	-	kIx~	-
1200	[number]	k4	1200
mAh	mAh	k?	mAh
u	u	k7c2	u
AAA	AAA	kA	AAA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
hojně	hojně	k6eAd1	hojně
využíváno	využívat	k5eAaImNgNnS	využívat
akumulátorů	akumulátor	k1gInPc2	akumulátor
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
kapacitou	kapacita	k1gFnSc7	kapacita
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
2000	[number]	k4	2000
mAh	mAh	k?	mAh
AA	AA	kA	AA
a	a	k8xC	a
800	[number]	k4	800
mAh	mAh	k?	mAh
u	u	k7c2	u
AAA	AAA	kA	AAA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
daleko	daleko	k6eAd1	daleko
nižším	nízký	k2eAgNnSc7d2	nižší
samovybíjením	samovybíjení	k1gNnSc7	samovybíjení
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc7d2	delší
životností	životnost	k1gFnSc7	životnost
(	(	kIx(	(
<g/>
reálně	reálně	k6eAd1	reálně
až	až	k9	až
1000	[number]	k4	1000
nabíjecích	nabíjecí	k2eAgInPc2d1	nabíjecí
cyklů	cyklus	k1gInPc2	cyklus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgInPc1	takovýto
akumulátory	akumulátor	k1gInPc1	akumulátor
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
ekologické	ekologický	k2eAgFnPc1d1	ekologická
<g/>
"	"	kIx"	"
a	a	k8xC	a
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
přednabity	přednabita	k1gFnPc1	přednabita
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Ready	ready	k0	ready
to	ten	k3xDgNnSc1	ten
use	usus	k1gInSc5	usus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
tedy	tedy	k9	tedy
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yInSc7	co
nižší	nízký	k2eAgFnSc1d2	nižší
celková	celkový	k2eAgFnSc1d1	celková
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
delší	dlouhý	k2eAgFnSc1d2	delší
životnost	životnost	k1gFnSc1	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
akumulátory	akumulátor	k1gInPc1	akumulátor
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
do	do	k7c2	do
stále	stále	k6eAd1	stále
používaných	používaný	k2eAgInPc2d1	používaný
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vysílačky	vysílačka	k1gFnPc4	vysílačka
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgFnPc4d1	dětská
chůvičky	chůvička	k1gFnPc4	chůvička
a	a	k8xC	a
přenosné	přenosný	k2eAgInPc4d1	přenosný
telefony	telefon	k1gInPc4	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Nízkokapacitní	Nízkokapacitní	k2eAgFnSc1d1	Nízkokapacitní
NiMH	NiMH	k1gFnSc1	NiMH
akumulátory	akumulátor	k1gInPc1	akumulátor
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
-	-	kIx~	-
1400	[number]	k4	1400
mAh	mAh	k?	mAh
u	u	k7c2	u
AA	AA	kA	AA
a	a	k8xC	a
500	[number]	k4	500
-	-	kIx~	-
600	[number]	k4	600
mAh	mAh	k?	mAh
u	u	k7c2	u
AAA	AAA	kA	AAA
<g/>
)	)	kIx)	)
totiž	totiž	k9	totiž
můžeme	moct	k5eAaImIp1nP	moct
nabíjet	nabíjet	k5eAaImF	nabíjet
při	při	k7c6	při
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
stavu	stav	k1gInSc6	stav
nabití	nabití	k1gNnSc2	nabití
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
větších	veliký	k2eAgNnPc2d2	veliký
poškození	poškození	k1gNnPc2	poškození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výhody	výhoda	k1gFnPc1	výhoda
NiMH	NiMH	k1gMnPc2	NiMH
===	===	k?	===
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
NiMH	NiMH	k1gMnSc1	NiMH
akumulátor	akumulátor	k1gInSc1	akumulátor
oproti	oproti	k7c3	oproti
starším	starý	k2eAgFnPc3d2	starší
NiCd	NiCd	k1gInSc1	NiCd
akumulátorům	akumulátor	k1gInPc3	akumulátor
až	až	k6eAd1	až
2,5	[number]	k4	2,5
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc4d2	veliký
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Dokáže	dokázat	k5eAaPmIp3nS	dokázat
dodávat	dodávat	k5eAaImF	dodávat
relativně	relativně	k6eAd1	relativně
vysoké	vysoký	k2eAgInPc4d1	vysoký
proudy	proud	k1gInPc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc4d1	malý
pořizovací	pořizovací	k2eAgInPc4d1	pořizovací
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
rychlonabíjení	rychlonabíjení	k1gNnSc2	rychlonabíjení
bez	bez	k7c2	bez
většího	veliký	k2eAgNnSc2d2	veliký
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
1,2	[number]	k4	1,2
V	V	kA	V
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
během	během	k7c2	během
vybíjení	vybíjení	k1gNnSc2	vybíjení
neklesá	klesat	k5eNaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
NiMH	NiMH	k1gFnSc2	NiMH
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
(	(	kIx(	(
<g/>
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
méně	málo	k6eAd2	málo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
baterie	baterie	k1gFnPc1	baterie
začínají	začínat	k5eAaImIp3nP	začínat
"	"	kIx"	"
<g/>
blokovat	blokovat	k5eAaImF	blokovat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
se	se	k3xPyFc4	se
zase	zase	k9	zase
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
1,2	[number]	k4	1,2
V	V	kA	V
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
elektronické	elektronický	k2eAgInPc4d1	elektronický
přístroje	přístroj	k1gInPc4	přístroj
nedostatečné	nedostatečná	k1gFnSc2	nedostatečná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
CETL	CETL	kA	CETL
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
:	:	kIx,	:
Aplikace	aplikace	k1gFnSc1	aplikace
elektrochemických	elektrochemický	k2eAgInPc2d1	elektrochemický
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
ČVUT	ČVUT	kA	ČVUT
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-01-02859-3	[number]	k4	80-01-02859-3
</s>
</p>
<p>
<s>
MAREK	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
STEHLÍK	Stehlík	k1gMnSc1	Stehlík
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
:	:	kIx,	:
Hermetické	hermetický	k2eAgInPc1d1	hermetický
akumulátory	akumulátor	k1gInPc1	akumulátor
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
,	,	kIx,	,
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
IN-EL	IN-EL	k1gFnSc2	IN-EL
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86230-34-1	[number]	k4	80-86230-34-1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nikl-metal	Nikletal	k1gMnSc1	Nikl-metal
hydridový	hydridový	k2eAgMnSc1d1	hydridový
akumulátor	akumulátor	k1gInSc4	akumulátor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Nikl-metalhydridová	Nikletalhydridový	k2eAgFnSc1d1	Nikl-metalhydridový
akumulátorová	akumulátorový	k2eAgFnSc1d1	akumulátorová
baterie	baterie	k1gFnSc1	baterie
</s>
</p>
<p>
<s>
NiMH	NiMH	k?	NiMH
akumulátory	akumulátor	k1gInPc1	akumulátor
v	v	k7c6	v
Abecedě	abeceda	k1gFnSc6	abeceda
baterií	baterie	k1gFnPc2	baterie
a	a	k8xC	a
akumulátorů	akumulátor	k1gInPc2	akumulátor
</s>
</p>
