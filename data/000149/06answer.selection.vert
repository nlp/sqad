<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
ptactva	ptactvo	k1gNnSc2	ptactvo
je	být	k5eAaImIp3nS	být
slaven	slaven	k2eAgMnSc1d1	slaven
každoročně	každoročně	k6eAd1	každoročně
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
ve	v	k7c4	v
výroční	výroční	k2eAgInSc4d1	výroční
den	den	k1gInSc4	den
podepsání	podepsání	k1gNnSc2	podepsání
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
Konvence	konvence	k1gFnSc2	konvence
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
užitečného	užitečný	k2eAgNnSc2d1	užitečné
ptactva	ptactvo	k1gNnSc2	ptactvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
slaví	slavit	k5eAaImIp3nP	slavit
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
