<s>
Jaké	jaký	k3yIgNnSc1	jaký
psychické	psychický	k2eAgNnSc1d1	psychické
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
postižený	postižený	k2eAgMnSc1d1	postižený
vnímá	vnímat	k5eAaImIp3nS	vnímat
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
jako	jako	k9	jako
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
?	?	kIx.	?
</s>
