<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
lykantropie	lykantropie	k1gFnSc1	lykantropie
je	být	k5eAaImIp3nS	být
psychické	psychický	k2eAgNnSc4d1	psychické
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podstatné	podstatný	k2eAgFnSc3d1	podstatná
změně	změna	k1gFnSc3	změna
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
postižený	postižený	k2eAgMnSc1d1	postižený
touto	tento	k3xDgFnSc7	tento
nemocí	nemoc	k1gFnSc7	nemoc
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
identifikovat	identifikovat	k5eAaBmF	identifikovat
i	i	k9	i
s	s	k7c7	s
krávou	kráva	k1gFnSc7	kráva
<g/>
,	,	kIx,	,
medvědem	medvěd	k1gMnSc7	medvěd
<g/>
,	,	kIx,	,
kočkou	kočka	k1gFnSc7	kočka
<g/>
,	,	kIx,	,
červem	červ	k1gMnSc7	červ
<g/>
,	,	kIx,	,
roztočem	roztoč	k1gMnSc7	roztoč
<g/>
,	,	kIx,	,
slepicí	slepice	k1gFnSc7	slepice
či	či	k8xC	či
jinými	jiný	k2eAgNnPc7d1	jiné
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
příznaky	příznak	k1gInPc4	příznak
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
lze	lze	k6eAd1	lze
řadit	řadit	k5eAaImF	řadit
bezděčně	bezděčně	k6eAd1	bezděčně
vydávané	vydávaný	k2eAgInPc4d1	vydávaný
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
zmatenost	zmatenost	k1gFnSc4	zmatenost
<g/>
,	,	kIx,	,
špatnou	špatný	k2eAgFnSc4d1	špatná
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
plazení	plazení	k1gNnSc6	plazení
aj.	aj.	kA	aj.
Název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
duševní	duševní	k2eAgFnSc2d1	duševní
poruchy	porucha	k1gFnSc2	porucha
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starořeckého	starořecký	k2eAgMnSc2d1	starořecký
λ	λ	k?	λ
(	(	kIx(	(
<g/>
lukánthropos	lukánthropos	k1gMnSc1	lukánthropos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
λ	λ	k?	λ
(	(	kIx(	(
<g/>
lúkos	lúkos	k1gMnSc1	lúkos
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vlk	vlk	k1gMnSc1	vlk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ἄ	ἄ	k?	ἄ
(	(	kIx(	(
<g/>
ánthrō	ánthrō	k?	ánthrō
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
lykantropie	lykantropie	k1gFnSc2	lykantropie
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
ve	v	k7c6	v
fantastice	fantastika	k1gFnSc6	fantastika
a	a	k8xC	a
mytologii	mytologie	k1gFnSc6	mytologie
pro	pro	k7c4	pro
magickou	magický	k2eAgFnSc4d1	magická
proměnu	proměna	k1gFnSc4	proměna
člověka	člověk	k1gMnSc2	člověk
ve	v	k7c4	v
vlka	vlk	k1gMnSc4	vlk
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
