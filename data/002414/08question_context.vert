<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
lykantropie	lykantropie	k1gFnSc1	lykantropie
je	být	k5eAaImIp3nS	být
psychické	psychický	k2eAgNnSc4d1	psychické
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podstatné	podstatný	k2eAgFnSc3d1	podstatná
změně	změna	k1gFnSc3	změna
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
postižený	postižený	k2eAgMnSc1d1	postižený
touto	tento	k3xDgFnSc7	tento
nemocí	nemoc	k1gFnSc7	nemoc
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
identifikovat	identifikovat	k5eAaBmF	identifikovat
i	i	k9	i
s	s	k7c7	s
krávou	kráva	k1gFnSc7	kráva
<g/>
,	,	kIx,	,
medvědem	medvěd	k1gMnSc7	medvěd
<g/>
,	,	kIx,	,
kočkou	kočka	k1gFnSc7	kočka
<g/>
,	,	kIx,	,
červem	červ	k1gMnSc7	červ
<g/>
,	,	kIx,	,
roztočem	roztoč	k1gMnSc7	roztoč
<g/>
,	,	kIx,	,
slepicí	slepice	k1gFnSc7	slepice
či	či	k8xC	či
jinými	jiný	k2eAgNnPc7d1	jiné
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
.	.	kIx.	.
</s>

