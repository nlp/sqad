<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
lykantropie	lykantropie	k1gFnSc1	lykantropie
je	být	k5eAaImIp3nS	být
psychické	psychický	k2eAgNnSc4d1	psychické
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgInSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
podstatné	podstatný	k2eAgFnSc3d1	podstatná
změně	změna	k1gFnSc3	změna
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
