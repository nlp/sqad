<s>
Studia	studio	k1gNnPc4	studio
Cinecittá	Cinecittý	k2eAgFnSc1d1	Cinecittá
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
Benitem	Benit	k1gInSc7	Benit
Mussolinim	Mussolinima	k1gFnPc2	Mussolinima
<g/>
,	,	kIx,	,
a	a	k8xC	a
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
bombardována	bombardovat	k5eAaImNgFnS	bombardovat
Spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
