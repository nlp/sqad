<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
I.	I.	kA	I.
(	(	kIx(	(
<g/>
v	v	k7c6	v
rodové	rodový	k2eAgFnSc6d1	rodová
posloupnosti	posloupnost	k1gFnSc6	posloupnost
jako	jako	k8xS	jako
habsburský	habsburský	k2eAgMnSc1d1	habsburský
hrabě	hrabě	k1gMnSc1	hrabě
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1218	[number]	k4	1218
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Limburg	Limburg	k1gInSc1	Limburg
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1291	[number]	k4	1291
<g/>
,	,	kIx,	,
Špýr	Špýr	k1gInSc1	Špýr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1273	[number]	k4	1273
<g/>
-	-	kIx~	-
<g/>
1291	[number]	k4	1291
<g/>
.	.	kIx.	.
</s>
