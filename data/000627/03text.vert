<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
I.	I.	kA	I.
(	(	kIx(	(
<g/>
v	v	k7c6	v
rodové	rodový	k2eAgFnSc6d1	rodová
posloupnosti	posloupnost	k1gFnSc6	posloupnost
jako	jako	k8xC	jako
habsburský	habsburský	k2eAgMnSc1d1	habsburský
hrabě	hrabě	k1gMnSc1	hrabě
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1218	[number]	k4	1218
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Limburg	Limburg	k1gInSc1	Limburg
-	-	kIx~	-
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1291	[number]	k4	1291
<g/>
,	,	kIx,	,
Špýr	Špýr	k1gInSc1	Špýr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1273	[number]	k4	1273
<g/>
-	-	kIx~	-
<g/>
1291	[number]	k4	1291
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
pocházel	pocházet	k5eAaImAgInS	pocházet
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
hraběcího	hraběcí	k2eAgInSc2d1	hraběcí
rodu	rod	k1gInSc2	rod
z	z	k7c2	z
jihozápadního	jihozápadní	k2eAgNnSc2d1	jihozápadní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Albrecht	Albrecht	k1gMnSc1	Albrecht
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgInSc1d1	habsburský
zvaný	zvaný	k2eAgInSc1d1	zvaný
Bílý	bílý	k2eAgInSc1d1	bílý
zahynul	zahynout	k5eAaPmAgMnS	zahynout
roku	rok	k1gInSc2	rok
1239	[number]	k4	1239
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
mladý	mladý	k2eAgMnSc1d1	mladý
hrabě	hrabě	k1gMnSc1	hrabě
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
svého	svůj	k3xOyFgNnSc2	svůj
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tvořilo	tvořit	k5eAaImAgNnS	tvořit
hrabství	hrabství	k1gNnSc1	hrabství
Aargau	Aargaus	k1gInSc2	Aargaus
(	(	kIx(	(
<g/>
Arga	Argos	k1gMnSc2	Argos
<g/>
)	)	kIx)	)
s	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
hradem	hrad	k1gInSc7	hrad
Habsburg	Habsburg	k1gInSc1	Habsburg
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Habichtsburg	Habichtsburg	k1gInSc1	Habichtsburg
-	-	kIx~	-
Jestřábí	jestřábí	k2eAgInSc1d1	jestřábí
hrad	hrad	k1gInSc1	hrad
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Horní	horní	k2eAgNnSc1d1	horní
Alsasko	Alsasko	k1gNnSc1	Alsasko
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zkušeným	zkušený	k2eAgMnSc7d1	zkušený
<g/>
,	,	kIx,	,
protřelým	protřelý	k2eAgMnSc7d1	protřelý
a	a	k8xC	a
bezohledným	bezohledný	k2eAgMnSc7d1	bezohledný
politikem	politik	k1gMnSc7	politik
i	i	k8xC	i
drsným	drsný	k2eAgMnSc7d1	drsný
válečníkem	válečník	k1gMnSc7	válečník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
pozvolné	pozvolný	k2eAgNnSc1d1	pozvolné
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
rodových	rodový	k2eAgFnPc2d1	rodová
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
příležitost	příležitost	k1gFnSc1	příležitost
získat	získat	k5eAaPmF	získat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
roku	rok	k1gInSc2	rok
1273	[number]	k4	1273
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
říšští	říšský	k2eAgMnPc1d1	říšský
kurfiřti	kurfiřt	k1gMnPc1	kurfiřt
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
královskou	královský	k2eAgFnSc4d1	královská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
dobových	dobový	k2eAgNnPc2d1	dobové
hledisek	hledisko	k1gNnPc2	hledisko
již	již	k6eAd1	již
dost	dost	k6eAd1	dost
starý	starý	k2eAgInSc1d1	starý
<g/>
,	,	kIx,	,
korunu	koruna	k1gFnSc4	koruna
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
panovníkem	panovník	k1gMnSc7	panovník
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejmocnějším	mocný	k2eAgInPc3d3	nejmocnější
z	z	k7c2	z
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
<g/>
,	,	kIx,	,
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
volbu	volba	k1gFnSc4	volba
neuznal	uznat	k5eNaPmAgMnS	uznat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
o	o	k7c4	o
říšskou	říšský	k2eAgFnSc4d1	říšská
korunu	koruna	k1gFnSc4	koruna
sám	sám	k3xTgMnSc1	sám
usiloval	usilovat	k5eAaImAgInS	usilovat
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stvrzením	stvrzení	k1gNnSc7	stvrzení
Rudolfa	Rudolf	k1gMnSc2	Rudolf
I.	I.	kA	I.
za	za	k7c4	za
římského	římský	k2eAgMnSc4d1	římský
krále	král	k1gMnSc4	král
váhal	váhat	k5eAaImAgMnS	váhat
i	i	k9	i
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
X.	X.	kA	X.
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc2	jenž
zájmy	zájem	k1gInPc7	zájem
Přemysl	Přemysl	k1gMnSc1	Přemysl
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Záleželo	záležet	k5eAaImAgNnS	záležet
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
smíření	smíření	k1gNnSc6	smíření
obou	dva	k4xCgMnPc2	dva
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
následovat	následovat	k5eAaImF	následovat
nová	nový	k2eAgFnSc1d1	nová
kruciáta	kruciáta	k1gFnSc1	kruciáta
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
proto	proto	k8xC	proto
dával	dávat	k5eAaImAgMnS	dávat
papežské	papežský	k2eAgFnSc3d1	Papežská
kurii	kurie	k1gFnSc3	kurie
najevo	najevo	k6eAd1	najevo
svoji	svůj	k3xOyFgFnSc4	svůj
ochotu	ochota	k1gFnSc4	ochota
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
,	,	kIx,	,
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
papeže	papež	k1gMnSc4	papež
projevy	projev	k1gInPc7	projev
oddanosti	oddanost	k1gFnSc2	oddanost
a	a	k8xC	a
úcty	úcta	k1gFnSc2	úcta
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
stará	starý	k2eAgNnPc4d1	staré
říšská	říšský	k2eAgNnPc4d1	říšské
práva	právo	k1gNnPc4	právo
na	na	k7c6	na
území	území	k1gNnSc6	území
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1274	[number]	k4	1274
Řehoř	Řehoř	k1gMnSc1	Řehoř
X.	X.	kA	X.
jeho	jeho	k3xOp3gFnSc4	jeho
volbu	volba	k1gFnSc4	volba
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Souhlas	souhlas	k1gInSc1	souhlas
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
však	však	k9	však
Rudolf	Rudolf	k1gMnSc1	Rudolf
získat	získat	k5eAaPmF	získat
nesnažil	snažit	k5eNaImAgMnS	snažit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
rovnou	rovnou	k6eAd1	rovnou
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Toužil	toužit	k5eAaImAgMnS	toužit
totiž	totiž	k9	totiž
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Habsburkové	Habsburk	k1gMnPc1	Habsburk
získali	získat	k5eAaPmAgMnP	získat
postavení	postavení	k1gNnSc3	postavení
říšských	říšský	k2eAgMnPc2d1	říšský
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
pak	pak	k8xC	pak
jako	jako	k8xS	jako
vládce	vládce	k1gMnSc1	vládce
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
solidní	solidní	k2eAgFnSc4d1	solidní
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
bohatým	bohatý	k2eAgMnPc3d1	bohatý
kurfiřtům	kurfiřt	k1gMnPc3	kurfiřt
a	a	k8xC	a
knížatům	kníže	k1gMnPc3wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
(	(	kIx(	(
<g/>
tak	tak	k9	tak
jako	jako	k9	jako
v	v	k7c6	v
mnohých	mnohý	k2eAgNnPc6d1	mnohé
jiných	jiný	k2eAgNnPc6d1	jiné
<g/>
)	)	kIx)	)
mocné	mocný	k2eAgNnSc1d1	mocné
přemyslovské	přemyslovský	k2eAgNnSc1d1	přemyslovské
soustátí	soustátí	k1gNnSc1	soustátí
budilo	budit	k5eAaImAgNnS	budit
závist	závist	k1gFnSc4	závist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Přemyslově	Přemyslův	k2eAgInSc6d1	Přemyslův
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nabízely	nabízet	k5eAaImAgFnP	nabízet
bývalé	bývalý	k2eAgFnPc4d1	bývalá
babenberské	babenberský	k2eAgFnPc4d1	Babenberská
a	a	k8xC	a
sponheimské	sponheimský	k2eAgFnPc4d1	sponheimský
državy	država	k1gFnPc4	država
<g/>
,	,	kIx,	,
říšská	říšský	k2eAgNnPc4d1	říšské
léna	léno	k1gNnPc4	léno
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgNnPc2	jenž
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
Rudolf	Rudolf	k1gMnSc1	Rudolf
obvinil	obvinit	k5eAaPmAgMnS	obvinit
roku	rok	k1gInSc2	rok
1274	[number]	k4	1274
na	na	k7c6	na
říšském	říšský	k2eAgInSc6d1	říšský
sněmu	sněm	k1gInSc6	sněm
<g/>
,	,	kIx,	,
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
zcela	zcela	k6eAd1	zcela
svévolně	svévolně	k6eAd1	svévolně
<g/>
.	.	kIx.	.
</s>
<s>
Požadoval	požadovat	k5eAaImAgMnS	požadovat
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
byl	být	k5eAaImAgInS	být
však	však	k9	však
dobře	dobře	k6eAd1	dobře
vědom	vědom	k2eAgMnSc1d1	vědom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
I.	I.	kA	I.
již	již	k6eAd1	již
nemusel	muset	k5eNaImAgInS	muset
následně	následně	k6eAd1	následně
udělit	udělit	k5eAaPmF	udělit
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
poslušnost	poslušnost	k1gFnSc4	poslušnost
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rudolfovo	Rudolfův	k2eAgNnSc1d1	Rudolfovo
zvolení	zvolení	k1gNnSc1	zvolení
je	být	k5eAaImIp3nS	být
neplatné	platný	k2eNgNnSc1d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
rozezlený	rozezlený	k2eAgMnSc1d1	rozezlený
král	král	k1gMnSc1	král
zabavil	zabavit	k5eAaPmAgMnS	zabavit
nejen	nejen	k6eAd1	nejen
Rakousy	Rakousy	k1gInPc4	Rakousy
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc1	Korutany
<g/>
,	,	kIx,	,
Kraňsko	Kraňsko	k1gNnSc1	Kraňsko
i	i	k8xC	i
Chebsko	Chebsko	k1gNnSc1	Chebsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
Přemysl	Přemysl	k1gMnSc1	Přemysl
nesložil	složit	k5eNaPmAgMnS	složit
lenní	lenní	k2eAgInSc4d1	lenní
hold	hold	k1gInSc4	hold
jako	jako	k8xS	jako
vládci	vládce	k1gMnPc1	vládce
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
nepřiznal	přiznat	k5eNaPmAgInS	přiznat
mu	on	k3xPp3gMnSc3	on
ani	ani	k8xC	ani
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
rodové	rodový	k2eAgFnPc4d1	rodová
državy	država	k1gFnPc4	država
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
vyřkl	vyřknout	k5eAaPmAgMnS	vyřknout
říšskou	říšský	k2eAgFnSc4d1	říšská
klatbu	klatba	k1gFnSc4	klatba
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
tak	tak	k6eAd1	tak
všem	všecek	k3xTgMnPc3	všecek
nespokojeným	spokojený	k2eNgMnPc3d1	nespokojený
šlechticům	šlechtic	k1gMnPc3	šlechtic
beztrestně	beztrestně	k6eAd1	beztrestně
proti	proti	k7c3	proti
panovníkovi	panovník	k1gMnSc3	panovník
vystoupit	vystoupit	k5eAaPmF	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
také	také	k9	také
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokořit	pokořit	k5eAaPmF	pokořit
Přemysla	Přemysl	k1gMnSc4	Přemysl
na	na	k7c6	na
válečném	válečný	k2eAgNnSc6d1	válečné
poli	pole	k1gNnSc6	pole
nebylo	být	k5eNaImAgNnS	být
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
když	když	k8xS	když
zradil	zradit	k5eAaPmAgMnS	zradit
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
jeho	jeho	k3xOp3gFnSc2	jeho
spojenec	spojenec	k1gMnSc1	spojenec
bavorský	bavorský	k2eAgMnSc1d1	bavorský
vévoda	vévoda	k1gMnSc1	vévoda
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
z	z	k7c2	z
Wittelsbachu	Wittelsbach	k1gInSc2	Wittelsbach
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
Rudolfovu	Rudolfův	k2eAgNnSc3d1	Rudolfovo
říšskému	říšský	k2eAgNnSc3d1	říšské
vojsku	vojsko	k1gNnSc3	vojsko
průchod	průchod	k1gInSc4	průchod
svým	svůj	k3xOyFgNnSc7	svůj
územím	území	k1gNnSc7	území
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
vpád	vpád	k1gInSc1	vpád
do	do	k7c2	do
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
rakouští	rakouský	k2eAgMnPc1d1	rakouský
<g/>
,	,	kIx,	,
štýrští	štýrský	k2eAgMnPc1d1	štýrský
a	a	k8xC	a
korutanští	korutanský	k2eAgMnPc1d1	korutanský
páni	pan	k1gMnPc1	pan
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
protivila	protivit	k5eAaImAgFnS	protivit
Přemyslova	Přemyslův	k2eAgFnSc1d1	Přemyslova
vláda	vláda	k1gFnSc1	vláda
pevné	pevný	k2eAgFnSc2d1	pevná
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
od	od	k7c2	od
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
odpadli	odpadnout	k5eAaPmAgMnP	odpadnout
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
usilovně	usilovně	k6eAd1	usilovně
budované	budovaný	k2eAgNnSc4d1	budované
panství	panství	k1gNnSc4	panství
v	v	k7c6	v
alpských	alpský	k2eAgFnPc6d1	alpská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
nečekaně	nečekaně	k6eAd1	nečekaně
zhroutilo	zhroutit	k5eAaPmAgNnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
stanul	stanout	k5eAaPmAgMnS	stanout
Rudolf	Rudolf	k1gMnSc1	Rudolf
před	před	k7c7	před
Vídní	Vídeň	k1gFnSc7	Vídeň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jediná	jediný	k2eAgFnSc1d1	jediná
kladla	klást	k5eAaImAgFnS	klást
úspěšně	úspěšně	k6eAd1	úspěšně
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
povstala	povstat	k5eAaPmAgFnS	povstat
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
panovníkovi	panovník	k1gMnSc3	panovník
i	i	k9	i
česká	český	k2eAgFnSc1d1	Česká
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
o	o	k7c4	o
domácí	domácí	k2eAgInSc4d1	domácí
trůn	trůn	k1gInSc4	trůn
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nevýhodný	výhodný	k2eNgInSc1d1	nevýhodný
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
osvobodit	osvobodit	k5eAaPmF	osvobodit
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Otakar	Otakar	k1gMnSc1	Otakar
raději	rád	k6eAd2	rád
svému	svůj	k3xOyFgMnSc3	svůj
protivníkovi	protivník	k1gMnSc3	protivník
před	před	k7c7	před
obleženým	obležený	k2eAgNnSc7d1	obležené
městem	město	k1gNnSc7	město
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mírového	mírový	k2eAgNnSc2d1	Mírové
ujednání	ujednání	k1gNnSc2	ujednání
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
vzdát	vzdát	k5eAaPmF	vzdát
všech	všecek	k3xTgNnPc2	všecek
říšských	říšský	k2eAgNnPc2d1	říšské
lén	léno	k1gNnPc2	léno
<g/>
.	.	kIx.	.
</s>
<s>
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
propaganda	propaganda	k1gFnSc1	propaganda
si	se	k3xPyFc3	se
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
románovou	románový	k2eAgFnSc4d1	románová
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Přemysl	Přemysl	k1gMnSc1	Přemysl
dostavil	dostavit	k5eAaPmAgMnS	dostavit
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
do	do	k7c2	do
Rudolfova	Rudolfův	k2eAgInSc2d1	Rudolfův
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
"	"	kIx"	"
<g/>
se	s	k7c7	s
zlomeným	zlomený	k2eAgNnSc7d1	zlomené
srdcem	srdce	k1gNnSc7	srdce
a	a	k8xC	a
na	na	k7c6	na
kolenou	koleno	k1gNnPc6	koleno
<g/>
"	"	kIx"	"
složil	složit	k5eAaPmAgMnS	složit
římskému	římský	k2eAgMnSc3d1	římský
králi	král	k1gMnSc3	král
hold	hold	k1gInSc4	hold
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
vazal	vazal	k1gMnSc1	vazal
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nucen	nucen	k2eAgInSc1d1	nucen
ctít	ctít	k5eAaImF	ctít
rytířské	rytířský	k2eAgFnPc4d1	rytířská
zásady	zásada	k1gFnPc4	zásada
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
nepoklekal	poklekat	k5eNaImAgMnS	poklekat
před	před	k7c4	před
zraky	zrak	k1gInPc4	zrak
jeho	jeho	k3xOp3gMnPc2	jeho
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
tradovalo	tradovat	k5eAaImAgNnS	tradovat
<g/>
.	.	kIx.	.
</s>
<s>
Odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
mu	on	k3xPp3gMnSc3	on
devět	devět	k4xCc1	devět
zástav	zástava	k1gFnPc2	zástava
svých	svůj	k3xOyFgFnPc2	svůj
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgFnPc2	který
mu	on	k3xPp3gMnSc3	on
Rudolf	Rudolf	k1gMnSc1	Rudolf
vrátil	vrátit	k5eAaPmAgInS	vrátit
jen	jen	k9	jen
dvě	dva	k4xCgFnPc4	dva
-	-	kIx~	-
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
I.	I.	kA	I.
mohl	moct	k5eAaImAgMnS	moct
slavnostně	slavnostně	k6eAd1	slavnostně
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
v	v	k7c6	v
Rakousích	Rakousy	k1gInPc6	Rakousy
a	a	k8xC	a
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
nejprve	nejprve	k6eAd1	nejprve
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
udělil	udělit	k5eAaPmAgMnS	udělit
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
svým	svůj	k3xOyFgMnPc3	svůj
synům	syn	k1gMnPc3	syn
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
I.	I.	kA	I.
a	a	k8xC	a
Rudolfovi	Rudolfův	k2eAgMnPc1d1	Rudolfův
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Korutany	Korutany	k1gInPc4	Korutany
a	a	k8xC	a
Kraňsko	Kraňsko	k1gNnSc4	Kraňsko
spravoval	spravovat	k5eAaImAgInS	spravovat
z	z	k7c2	z
Rudolfova	Rudolfův	k2eAgNnSc2d1	Rudolfovo
pověření	pověření	k1gNnSc2	pověření
jeho	jeho	k3xOp3gFnSc4	jeho
spojenec	spojenec	k1gMnSc1	spojenec
Menhard	Menhard	k1gMnSc1	Menhard
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Korutanského	korutanský	k2eAgMnSc2d1	korutanský
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
tyto	tento	k3xDgInPc1	tento
země	země	k1gFnSc1	země
rovněž	rovněž	k9	rovněž
svěřil	svěřit	k5eAaPmAgInS	svěřit
jako	jako	k9	jako
říšské	říšský	k2eAgNnSc4d1	říšské
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1277	[number]	k4	1277
napsal	napsat	k5eAaBmAgMnS	napsat
do	do	k7c2	do
Brescie	Brescie	k1gFnSc2	Brescie
vzkaz	vzkaz	k1gInSc1	vzkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nemohl	moct	k5eNaImAgMnS	moct
postavit	postavit	k5eAaPmF	postavit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc3	strana
Přemysl	Přemysl	k1gMnSc1	Přemysl
se	se	k3xPyFc4	se
zjevně	zjevně	k6eAd1	zjevně
nesmířil	smířit	k5eNaPmAgMnS	smířit
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
rakouských	rakouský	k2eAgFnPc2d1	rakouská
držav	država	k1gFnPc2	država
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
proto	proto	k8xC	proto
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
neustále	neustále	k6eAd1	neustále
podněcoval	podněcovat	k5eAaImAgMnS	podněcovat
českou	český	k2eAgFnSc4d1	Česká
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
,	,	kIx,	,
především	především	k9	především
mocný	mocný	k2eAgInSc4d1	mocný
rod	rod	k1gInSc4	rod
Vítkovců	Vítkovec	k1gMnPc2	Vítkovec
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
vytrácely	vytrácet	k5eAaImAgFnP	vytrácet
sympatie	sympatie	k1gFnPc1	sympatie
k	k	k7c3	k
Rudolfově	Rudolfův	k2eAgFnSc3d1	Rudolfova
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
posílený	posílený	k2eAgMnSc1d1	posílený
vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c7	nad
Přemyslem	Přemysl	k1gMnSc7	Přemysl
i	i	k8xC	i
ziskem	zisk	k1gInSc7	zisk
nových	nový	k2eAgFnPc2d1	nová
držav	država	k1gFnPc2	država
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgMnS	počínat
vůči	vůči	k7c3	vůči
knížatům	kníže	k1gMnPc3wR	kníže
mnohem	mnohem	k6eAd1	mnohem
bezohledněji	bezohledně	k6eAd2	bezohledně
a	a	k8xC	a
nenaplnil	naplnit	k5eNaPmAgMnS	naplnit
tak	tak	k6eAd1	tak
představy	představa	k1gFnPc4	představa
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
o	o	k7c6	o
panovníkovi	panovník	k1gMnSc6	panovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
vměšovat	vměšovat	k5eAaImF	vměšovat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnSc7	záležitost
jejich	jejich	k3xOp3gNnSc2	jejich
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Rudolfovy	Rudolfův	k2eAgFnPc1d1	Rudolfova
rodové	rodový	k2eAgFnPc1d1	rodová
državy	država	k1gFnPc1	država
se	se	k3xPyFc4	se
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
pod	pod	k7c7	pod
tíživým	tíživý	k2eAgInSc7d1	tíživý
tlakem	tlak	k1gInSc7	tlak
permanentně	permanentně	k6eAd1	permanentně
přitahovaného	přitahovaný	k2eAgInSc2d1	přitahovaný
daňového	daňový	k2eAgInSc2d1	daňový
šroubu	šroub	k1gInSc2	šroub
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
pánů	pan	k1gMnPc2	pan
pak	pak	k6eAd1	pak
začali	začít	k5eAaPmAgMnP	začít
pomýšlet	pomýšlet	k5eAaImF	pomýšlet
na	na	k7c4	na
Přemyslův	Přemyslův	k2eAgInSc4d1	Přemyslův
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Rudolfem	Rudolf	k1gMnSc7	Rudolf
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
českým	český	k2eAgMnSc7d1	český
soupeřem	soupeř	k1gMnSc7	soupeř
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
nanovo	nanovo	k6eAd1	nanovo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konečnému	konečný	k2eAgNnSc3d1	konečné
střetnutí	střetnutí	k1gNnSc3	střetnutí
došlo	dojít	k5eAaPmAgNnS	dojít
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
u	u	k7c2	u
vesničky	vesnička	k1gFnSc2	vesnička
Dürnkrut	Dürnkrut	k1gInSc1	Dürnkrut
(	(	kIx(	(
<g/>
Suché	Suché	k2eAgFnSc2d1	Suché
Kruty	kruta	k1gFnSc2	kruta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
poset	poset	k2eAgMnSc1d1	poset
mnoha	mnoho	k4c7	mnoho
ranami	rána	k1gFnPc7	rána
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
Rudolf	Rudolf	k1gMnSc1	Rudolf
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
českého	český	k2eAgMnSc4d1	český
panovníka	panovník	k1gMnSc4	panovník
ušetřili	ušetřit	k5eAaPmAgMnP	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
obsadil	obsadit	k5eAaPmAgMnS	obsadit
vítězný	vítězný	k2eAgMnSc1d1	vítězný
Rudolf	Rudolf	k1gMnSc1	Rudolf
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc1d1	habsburský
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
bezprávně	bezprávně	k6eAd1	bezprávně
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
zmítaného	zmítaný	k2eAgInSc2d1	zmítaný
po	po	k7c6	po
Přemyslově	Přemyslův	k2eAgFnSc6d1	Přemyslova
smrti	smrt	k1gFnSc6	smrt
bezvládím	bezvládí	k1gNnSc7	bezvládí
a	a	k8xC	a
svévolí	svévole	k1gFnSc7	svévole
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Slíbil	slíbit	k5eAaPmAgMnS	slíbit
sice	sice	k8xC	sice
svoji	svůj	k3xOyFgFnSc4	svůj
ochranu	ochrana	k1gFnSc4	ochrana
ovdovělé	ovdovělý	k2eAgFnSc3d1	ovdovělá
královně	královna	k1gFnSc3	královna
Kunhutě	Kunhuta	k1gFnSc3	Kunhuta
a	a	k8xC	a
jejím	její	k3xOp3gFnPc3	její
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Kolína	Kolín	k1gInSc2	Kolín
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
V.	V.	kA	V.
Braniborským	braniborský	k2eAgFnPc3d1	Braniborská
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
neodvážil	odvážit	k5eNaPmAgMnS	odvážit
pustit	pustit	k5eAaPmF	pustit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ponechá	ponechat	k5eAaPmIp3nS	ponechat
správu	správa	k1gFnSc4	správa
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
poručnictví	poručnictví	k1gNnPc2	poručnictví
nad	nad	k7c7	nad
mladičkým	mladičký	k2eAgMnSc7d1	mladičký
Václavem	Václav	k1gMnSc7	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
jako	jako	k9	jako
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
válečné	válečný	k2eAgInPc4d1	válečný
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
Moravu	Morava	k1gFnSc4	Morava
(	(	kIx(	(
<g/>
usiloval	usilovat	k5eAaImAgMnS	usilovat
však	však	k9	však
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
trvalého	trvalý	k2eAgNnSc2d1	trvalé
držení	držení	k1gNnSc2	držení
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kladsko	Kladsko	k1gNnSc1	Kladsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dosud	dosud	k6eAd1	dosud
součástí	součást	k1gFnSc7	součást
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
vratislavského	vratislavský	k2eAgMnSc2d1	vratislavský
knížete	kníže	k1gMnSc2	kníže
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Proba	Proba	k1gMnSc1	Proba
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
pospíšil	pospíšit	k5eAaPmAgMnS	pospíšit
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
obsazením	obsazení	k1gNnSc7	obsazení
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
dal	dát	k5eAaPmAgMnS	dát
Rudolf	Rudolf	k1gMnSc1	Rudolf
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
již	již	k6eAd1	již
po	po	k7c4	po
dvě	dva	k4xCgNnPc4	dva
století	století	k1gNnPc4	století
nedílně	nedílně	k6eAd1	nedílně
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
přemyslovskému	přemyslovský	k2eAgInSc3d1	přemyslovský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1283	[number]	k4	1283
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
braniborského	braniborský	k2eAgNnSc2d1	braniborské
zajetí	zajetí	k1gNnSc2	zajetí
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
Václav	Václava	k1gFnPc2	Václava
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
jeho	jeho	k3xOp3gFnSc4	jeho
přízeň	přízeň	k1gFnSc4	přízeň
i	i	k8xC	i
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
české	český	k2eAgFnPc4d1	Česká
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Garantovat	garantovat	k5eAaBmF	garantovat
mu	on	k3xPp3gInSc3	on
ho	on	k3xPp3gMnSc4	on
nyní	nyní	k6eAd1	nyní
mělo	mít	k5eAaImAgNnS	mít
především	především	k9	především
sňatkové	sňatkový	k2eAgNnSc1d1	sňatkové
spojení	spojení	k1gNnSc1	spojení
Habsburků	Habsburk	k1gMnPc2	Habsburk
s	s	k7c7	s
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
<g/>
,	,	kIx,	,
dohodnuté	dohodnutý	k2eAgFnSc3d1	dohodnutá
již	již	k9	již
roku	rok	k1gInSc2	rok
1276	[number]	k4	1276
před	před	k7c7	před
Vídní	Vídeň	k1gFnSc7	Vídeň
a	a	k8xC	a
potvrzené	potvrzená	k1gFnSc2	potvrzená
královnou	královna	k1gFnSc7	královna
vdovou	vdova	k1gFnSc7	vdova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1285	[number]	k4	1285
se	se	k3xPyFc4	se
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
konala	konat	k5eAaImAgFnS	konat
svatba	svatba	k1gFnSc1	svatba
Rudolfovy	Rudolfův	k2eAgFnSc2d1	Rudolfova
dcery	dcera	k1gFnSc2	dcera
Guty	Guta	k1gMnSc2	Guta
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Rudolf	Rudolf	k1gMnSc1	Rudolf
I.	I.	kA	I.
udělil	udělit	k5eAaPmAgMnS	udělit
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Moravu	Morava	k1gFnSc4	Morava
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
správy	správa	k1gFnSc2	správa
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
již	již	k6eAd1	již
1283	[number]	k4	1283
<g/>
)	)	kIx)	)
svému	svůj	k1gMnSc3	svůj
zeti	zet	k5eAaImF	zet
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vycházel	vycházet	k5eAaImAgMnS	vycházet
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
potřebným	potřebný	k2eAgInSc7d1	potřebný
rádcem	rádce	k1gMnSc7	rádce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obavami	obava	k1gFnPc7	obava
však	však	k9	však
hleděl	hledět	k5eAaImAgInS	hledět
na	na	k7c4	na
vzestup	vzestup	k1gInSc4	vzestup
svého	svůj	k3xOyFgMnSc2	svůj
bývalého	bývalý	k2eAgMnSc2d1	bývalý
spojence	spojenec	k1gMnSc2	spojenec
Záviše	Záviš	k1gMnSc2	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
Rudolfovi	Rudolf	k1gMnSc6	Rudolf
komplikoval	komplikovat	k5eAaBmAgMnS	komplikovat
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Albrecht	Albrecht	k1gMnSc1	Albrecht
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
někteří	některý	k3yIgMnPc1	některý
čeští	český	k2eAgMnPc1d1	český
páni	pan	k1gMnPc1	pan
hledali	hledat	k5eAaImAgMnP	hledat
oporu	opora	k1gFnSc4	opora
proti	proti	k7c3	proti
Závišovi	Záviš	k1gMnSc3	Záviš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rostoucím	rostoucí	k2eAgNnSc6d1	rostoucí
napětí	napětí	k1gNnSc6	napětí
se	se	k3xPyFc4	se
král	král	k1gMnSc1	král
snažil	snažit	k5eAaImAgMnS	snažit
svého	svůj	k3xOyFgMnSc4	svůj
zetě	zeť	k1gMnSc4	zeť
všemožně	všemožně	k6eAd1	všemožně
podpořit	podpořit	k5eAaPmF	podpořit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
dá	dát	k5eAaPmIp3nS	dát
svůj	svůj	k3xOyFgInSc4	svůj
kurfiřtský	kurfiřtský	k2eAgInSc4d1	kurfiřtský
hlas	hlas	k1gInSc4	hlas
při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
dalšímu	další	k2eAgNnSc3d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
Rudolfovi	Rudolfův	k2eAgMnPc1d1	Rudolfův
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnPc1	naděje
Rudolfa	Rudolf	k1gMnSc2	Rudolf
I.	I.	kA	I.
<g/>
,	,	kIx,	,
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
"	"	kIx"	"
<g/>
zájmy	zájem	k1gInPc4	zájem
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
kladl	klást	k5eAaImAgMnS	klást
nad	nad	k7c4	nad
prospěch	prospěch	k1gInSc4	prospěch
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
Habsburkové	Habsburk	k1gMnPc1	Habsburk
udrží	udržet	k5eAaPmIp3nP	udržet
římskou	římský	k2eAgFnSc4d1	římská
korunu	koruna	k1gFnSc4	koruna
dědičně	dědičně	k6eAd1	dědičně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nesplnily	splnit	k5eNaPmAgFnP	splnit
a	a	k8xC	a
na	na	k7c4	na
říšský	říšský	k2eAgInSc4d1	říšský
trůn	trůn	k1gInSc4	trůn
usedl	usednout	k5eAaPmAgMnS	usednout
z	z	k7c2	z
Václavovy	Václavův	k2eAgFnSc2d1	Václavova
vůle	vůle	k1gFnSc2	vůle
Adolf	Adolf	k1gMnSc1	Adolf
Nasavský	Nasavský	k2eAgMnSc1d1	Nasavský
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
moci	moc	k1gFnSc2	moc
jeho	jeho	k3xOp3gInSc2	jeho
rodu	rod	k1gInSc2	rod
však	však	k9	však
již	již	k9	již
byly	být	k5eAaImAgFnP	být
položeny	položit	k5eAaPmNgFnP	položit
a	a	k8xC	a
Albrecht	Albrecht	k1gMnSc1	Albrecht
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
o	o	k7c4	o
6	[number]	k4	6
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
v	v	k7c6	v
předtuše	předtucha	k1gFnSc6	předtucha
blížící	blížící	k2eAgFnPc1d1	blížící
se	se	k3xPyFc4	se
smrti	smrt	k1gFnSc2	smrt
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Špýru	Špýr	k1gInSc2	Špýr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
špýrského	špýrský	k2eAgInSc2d1	špýrský
dómu	dóm	k1gInSc2	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Dante	Dante	k1gMnSc1	Dante
Alighieri	Alighier	k1gFnSc2	Alighier
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Božské	božský	k2eAgFnSc6d1	božská
komediii	komediie	k1gFnSc6	komediie
umístil	umístit	k5eAaPmAgMnS	umístit
do	do	k7c2	do
Očistce	očistec	k1gInSc2	očistec
<g/>
.	.	kIx.	.
</s>
