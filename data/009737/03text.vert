<p>
<s>
Ninazu	Ninaz	k1gInSc2	Ninaz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
sumerské	sumerský	k2eAgFnSc6d1	sumerská
mytologii	mytologie	k1gFnSc6	mytologie
bohem	bůh	k1gMnSc7	bůh
podsvětí	podsvětí	k1gNnSc2	podsvětí
a	a	k8xC	a
bohem	bůh	k1gMnSc7	bůh
'	'	kIx"	'
<g/>
hojení	hojení	k1gNnSc2	hojení
a	a	k8xC	a
léčení	léčení	k1gNnSc2	léčení
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
Enlila	Enlil	k1gMnSc2	Enlil
a	a	k8xC	a
Ninlil	Ninlil	k1gMnSc2	Ninlil
anebo	anebo	k8xC	anebo
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
alternativních	alternativní	k2eAgFnPc2d1	alternativní
tradic	tradice	k1gFnPc2	tradice
Ereškigaly	Ereškigala	k1gFnSc2	Ereškigala
a	a	k8xC	a
Gugalanna	Gugalanno	k1gNnSc2	Gugalanno
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
otcem	otec	k1gMnSc7	otec
Ningišzida	Ningišzid	k1gMnSc2	Ningišzid
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
Enki	Enk	k1gFnSc2	Enk
a	a	k8xC	a
Ninhursag	Ninhursag	k1gInSc1	Ninhursag
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
druha	druh	k1gMnSc2	druh
Ninsuty	Ninsut	k1gInPc4	Ninsut
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
bohyň	bohyně	k1gFnPc2	bohyně
zrozených	zrozený	k2eAgFnPc2d1	zrozená
pro	pro	k7c4	pro
ulehčování	ulehčování	k1gNnSc4	ulehčování
Enkimu	Enkim	k1gInSc2	Enkim
od	od	k7c2	od
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ninazu	Ninaz	k1gMnSc3	Ninaz
byl	být	k5eAaImAgMnS	být
patronem	patron	k1gMnSc7	patron
města	město	k1gNnSc2	město
Ešnunna	Ešnunno	k1gNnSc2	Ešnunno
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Tispakem	Tispak	k1gInSc7	Tispak
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
svatyněmi	svatyně	k1gFnPc7	svatyně
byly	být	k5eAaImAgInP	být
E-sikul	Eikul	k1gInSc4	E-sikul
a	a	k8xC	a
E-kurma	Eurma	k1gNnSc4	E-kurma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	on	k3xPp3gMnSc2	on
blízkého	blízký	k2eAgMnSc2d1	blízký
příbuzného	příbuzný	k1gMnSc2	příbuzný
Nergala	Nergal	k1gMnSc2	Nergal
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
benevolentního	benevolentní	k2eAgMnSc4d1	benevolentní
boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mýty	mýtus	k1gInPc1	mýtus
==	==	k?	==
</s>
</p>
<p>
<s>
Ereškigal	Ereškigal	k1gInSc1	Ereškigal
a	a	k8xC	a
Nergal	Nergal	k1gInSc1	Nergal
(	(	kIx(	(
<g/>
též	též	k9	též
Nergal	Nergal	k1gMnSc1	Nergal
a	a	k8xC	a
Ereškigal	Ereškigal	k1gMnSc1	Ereškigal
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Epos	epos	k1gInSc1	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
</s>
</p>
<p>
<s>
Inin	Inin	k1gInSc1	Inin
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ninazu	Ninaz	k1gInSc2	Ninaz
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sumer	Sumer	k1gMnSc1	Sumer
</s>
</p>
<p>
<s>
Sumersko-akkadská	Sumerskokkadský	k2eAgFnSc1d1	Sumersko-akkadský
mytologie	mytologie	k1gFnSc1	mytologie
</s>
</p>
<p>
<s>
Sumersko-akkadská	Sumerskokkadský	k2eAgFnSc1d1	Sumersko-akkadský
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
