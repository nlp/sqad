<s>
Seriál	seriál	k1gInSc1	seriál
sleduje	sledovat	k5eAaImIp3nS	sledovat
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
brutální	brutální	k2eAgFnSc2d1	brutální
vraždy	vražda	k1gFnSc2	vražda
populární	populární	k2eAgFnSc2d1	populární
dívky	dívka	k1gFnSc2	dívka
a	a	k8xC	a
středoškolské	středoškolský	k2eAgFnSc2d1	středoškolská
královny	královna	k1gFnSc2	královna
<g/>
,	,	kIx,	,
Laury	Laura	k1gFnSc2	Laura
Palmerové	Palmerová	k1gFnSc2	Palmerová
(	(	kIx(	(
<g/>
Sheryl	Sheryl	k1gInSc1	Sheryl
Lee	Lea	k1gFnSc3	Lea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vede	vést	k5eAaImIp3nS	vést
speciální	speciální	k2eAgMnSc1d1	speciální
agent	agent	k1gMnSc1	agent
FBI	FBI	kA	FBI
Dale	Dale	k1gFnSc1	Dale
Cooper	Cooper	k1gMnSc1	Cooper
(	(	kIx(	(
<g/>
Kyle	Kyle	k1gFnSc1	Kyle
MacLachlan	MacLachlan	k1gMnSc1	MacLachlan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
