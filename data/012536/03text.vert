<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
umisťované	umisťovaný	k2eAgNnSc4d1	umisťované
zpravidla	zpravidla	k6eAd1	zpravidla
do	do	k7c2	do
letadel	letadlo	k1gNnPc2	letadlo
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
také	také	k9	také
termín	termín	k1gInSc1	termín
letový	letový	k2eAgInSc4d1	letový
zapisovač	zapisovač	k1gInSc4	zapisovač
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zaznamenávání	zaznamenávání	k1gNnSc2	zaznamenávání
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
parametrů	parametr	k1gInPc2	parametr
letu	let	k1gInSc2	let
pro	pro	k7c4	pro
objasnění	objasnění	k1gNnSc4	objasnění
příčin	příčina	k1gFnPc2	příčina
případné	případný	k2eAgFnSc2d1	případná
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
dohod	dohoda	k1gFnPc2	dohoda
je	on	k3xPp3gNnSc4	on
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
všechny	všechen	k3xTgFnPc4	všechen
letecké	letecký	k2eAgFnPc4d1	letecká
společnosti	společnost	k1gFnPc4	společnost
umístěny	umístěn	k2eAgFnPc4d1	umístěna
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černou	černý	k2eAgFnSc4d1	černá
skříňku	skříňka	k1gFnSc4	skříňka
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
australský	australský	k2eAgMnSc1d1	australský
technik	technik	k1gMnSc1	technik
David	David	k1gMnSc1	David
Warren	Warrna	k1gFnPc2	Warrna
a	a	k8xC	a
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
ji	on	k3xPp3gFnSc4	on
ARL	ARL	kA	ARL
Flight	Flight	k1gMnSc1	Flight
Memory	Memora	k1gFnSc2	Memora
Unit	Unita	k1gFnPc2	Unita
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
USA	USA	kA	USA
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
do	do	k7c2	do
letadel	letadlo	k1gNnPc2	letadlo
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
černé	černý	k2eAgFnPc4d1	černá
skříňky	skříňka	k1gFnPc4	skříňka
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
do	do	k7c2	do
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
při	při	k7c6	při
leteckých	letecký	k2eAgFnPc6d1	letecká
nehodách	nehoda	k1gFnPc6	nehoda
poškozena	poškodit	k5eAaPmNgFnS	poškodit
nejméně	málo	k6eAd3	málo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
skříněk	skříňka	k1gFnPc2	skříňka
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Cockpit	Cockpit	k5eAaPmF	Cockpit
voice	voika	k1gFnSc3	voika
recorder	recorder	k1gMnSc1	recorder
(	(	kIx(	(
<g/>
CVR	CVR	kA	CVR
<g/>
)	)	kIx)	)
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
rozhovory	rozhovor	k1gInPc1	rozhovor
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
kabině	kabina	k1gFnSc6	kabina
</s>
</p>
<p>
<s>
Flight	Flight	k5eAaPmF	Flight
data	datum	k1gNnPc4	datum
recorder	recordra	k1gFnPc2	recordra
(	(	kIx(	(
<g/>
FDR	FDR	kA	FDR
<g/>
)	)	kIx)	)
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
letové	letový	k2eAgInPc4d1	letový
údaje	údaj	k1gInPc4	údaj
(	(	kIx(	(
<g/>
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc4	tlak
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
letadle	letadlo	k1gNnSc6	letadlo
<g/>
,	,	kIx,	,
stav	stav	k1gInSc4	stav
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalšího	další	k2eAgMnSc2d1	další
<g/>
)	)	kIx)	)
</s>
<s>
Černá	Černá	k1gFnSc1	Černá
skříňka	skříňka	k1gFnSc1	skříňka
je	být	k5eAaImIp3nS	být
konstruována	konstruovat	k5eAaImNgFnS	konstruovat
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
neporušeného	porušený	k2eNgInSc2d1	neporušený
obsahu	obsah	k1gInSc2	obsah
i	i	k9	i
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
odolávat	odolávat	k5eAaImF	odolávat
teplotě	teplota	k1gFnSc6	teplota
1000	[number]	k4	1000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
vydrží	vydržet	k5eAaPmIp3nS	vydržet
silné	silný	k2eAgNnSc1d1	silné
přetížení	přetížení	k1gNnSc1	přetížení
(	(	kIx(	(
<g/>
3600	[number]	k4	3600
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vodotěsná	vodotěsný	k2eAgFnSc1d1	vodotěsná
a	a	k8xC	a
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
nekorodujících	korodující	k2eNgInPc2d1	nekorodující
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
i	i	k8xC	i
miniaturní	miniaturní	k2eAgInSc1d1	miniaturní
vysílač	vysílač	k1gInSc1	vysílač
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
vysílá	vysílat	k5eAaImIp3nS	vysílat
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
skříňka	skříňka	k1gFnSc1	skříňka
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snazší	snadný	k2eAgNnSc4d2	snazší
nalezení	nalezení	k1gNnSc4	nalezení
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
křiklavě	křiklavě	k6eAd1	křiklavě
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgNnPc1d1	další
využití	využití	k1gNnPc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
probíhá	probíhat	k5eAaImIp3nS	probíhat
vývoj	vývoj	k1gInSc1	vývoj
černé	černý	k2eAgFnSc2d1	černá
skříňky	skříňka	k1gFnSc2	skříňka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
umisťována	umisťovat	k5eAaImNgFnS	umisťovat
do	do	k7c2	do
motorových	motorový	k2eAgNnPc2d1	motorové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucnost	budoucnost	k1gFnSc1	budoucnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
letecké	letecký	k2eAgFnSc3d1	letecká
katastrofě	katastrofa	k1gFnSc3	katastrofa
na	na	k7c6	na
letu	let	k1gInSc6	let
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
Rio	Rio	k1gFnPc2	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
–	–	k?	–
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
;	;	kIx,	;
stroj	stroj	k1gInSc1	stroj
se	se	k3xPyFc4	se
zřítil	zřítit	k5eAaPmAgInS	zřítit
za	za	k7c2	za
neobjasněných	objasněný	k2eNgFnPc2d1	neobjasněná
okolností	okolnost	k1gFnPc2	okolnost
do	do	k7c2	do
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
skříňky	skříňka	k1gFnPc1	skříňka
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
až	až	k9	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
diskuse	diskuse	k1gFnPc4	diskuse
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
změně	změna	k1gFnSc6	změna
zaznamenávání	zaznamenávání	k1gNnSc6	zaznamenávání
letových	letový	k2eAgNnPc2d1	letové
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
mj.	mj.	kA	mj.
o	o	k7c4	o
nahrazení	nahrazení	k1gNnSc4	nahrazení
černých	černý	k2eAgFnPc2d1	černá
skříněk	skříňka	k1gFnPc2	skříňka
datovými	datový	k2eAgFnPc7d1	datová
přenosy	přenos	k1gInPc4	přenos
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
pomocí	pomoc	k1gFnPc2	pomoc
satelitů	satelit	k1gInPc2	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
však	však	k9	však
byla	být	k5eAaImAgFnS	být
přenášena	přenášen	k2eAgFnSc1d1	přenášena
i	i	k9	i
veškerá	veškerý	k3xTgFnSc1	veškerý
hlasová	hlasový	k2eAgFnSc1d1	hlasová
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
piloty	pilot	k1gMnPc7	pilot
uvnitř	uvnitř	k7c2	uvnitř
pilotní	pilotní	k2eAgFnSc2d1	pilotní
kabiny	kabina	k1gFnSc2	kabina
<g/>
,	,	kIx,	,
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
to	ten	k3xDgNnSc1	ten
protesty	protest	k1gInPc7	protest
ohledně	ohledně	k7c2	ohledně
narušení	narušení	k1gNnSc2	narušení
jejich	jejich	k3xOp3gNnSc2	jejich
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Tachograf	tachograf	k1gInSc1	tachograf
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
(	(	kIx(	(
<g/>
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Černé	Černé	k2eAgFnPc1d1	Černé
skříňky	skříňka	k1gFnPc1	skříňka
-	-	kIx~	-
přednáška	přednáška	k1gFnSc1	přednáška
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
na	na	k7c4	na
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
na	na	k7c4	na
Novinky	novinka	k1gFnPc4	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Databáze	databáze	k1gFnSc1	databáze
záznamníku	záznamník	k1gInSc2	záznamník
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
</s>
</p>
