<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skrinka	skrinka	k1gFnSc1	skrinka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
konstruována	konstruovat	k5eAaImNgFnS	konstruovat
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
neporušeného	porušený	k2eNgInSc2d1	neporušený
obsahu	obsah	k1gInSc2	obsah
i	i	k9	i
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
?	?	kIx.	?
</s>
