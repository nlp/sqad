<s>
Andrea	Andrea	k1gFnSc1	Andrea
Kerestešová	Kerestešová	k1gFnSc1	Kerestešová
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1984	[number]	k4	1984
Hlinné	hlinný	k2eAgInPc1d1	hlinný
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Růžičková	Růžičková	k1gFnSc1	Růžičková
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
modelka	modelka	k1gFnSc1	modelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
například	například	k6eAd1	například
z	z	k7c2	z
filmu	film	k1gInSc2	film
Rafťáci	Rafťák	k1gMnPc1	Rafťák
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
Kláru	Klára	k1gFnSc4	Klára
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
výraznému	výrazný	k2eAgInSc3d1	výrazný
slovenskému	slovenský	k2eAgInSc3d1	slovenský
akcentu	akcent	k1gInSc3	akcent
ji	on	k3xPp3gFnSc4	on
nadabovala	nadabovat	k5eAaPmAgFnS	nadabovat
Andrea	Andrea	k1gFnSc1	Andrea
Elsnerová	Elsnerová	k1gFnSc1	Elsnerová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
reklamy	reklama	k1gFnSc2	reklama
na	na	k7c4	na
Vodafone	Vodafon	k1gInSc5	Vodafon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
začala	začít	k5eAaPmAgFnS	začít
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
vysílat	vysílat	k5eAaImF	vysílat
retrospektivní	retrospektivní	k2eAgInSc4d1	retrospektivní
seriál	seriál	k1gInSc4	seriál
Vyprávěj	vyprávět	k5eAaImRp2nS	vyprávět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
hlavní	hlavní	k2eAgFnPc4d1	hlavní
ženské	ženský	k2eAgFnPc4d1	ženská
role	role	k1gFnPc4	role
–	–	k?	–
Evy	Eva	k1gFnSc2	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
obor	obor	k1gInSc4	obor
Učiteľstvo	Učiteľstvo	k1gNnSc1	Učiteľstvo
1	[number]	k4	1
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
a	a	k8xC	a
obor	obor	k1gInSc1	obor
Tvorivá	Tvorivý	k2eAgFnSc1d1	Tvorivý
dramatika	dramatika	k1gFnSc1	dramatika
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
Trnavské	trnavský	k2eAgFnSc2d1	Trnavská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
magistr	magistr	k1gMnSc1	magistr
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
–	–	k?	–
3	[number]	k4	3
plus	plus	k1gInSc1	plus
1	[number]	k4	1
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Donutilem	Donutil	k1gMnSc7	Donutil
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2006	[number]	k4	2006
–	–	k?	–
Rafťáci	Rafťák	k1gMnPc1	Rafťák
[	[	kIx(	[
<g/>
mluví	mluvit	k5eAaImIp3nS	mluvit
Andrea	Andrea	k1gFnSc1	Andrea
Elsnerová	Elsnerová	k1gFnSc1	Elsnerová
<g/>
]	]	kIx)	]
2007	[number]	k4	2007
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Světla	světlo	k1gNnPc1	světlo
pasáže	pasáž	k1gFnSc2	pasáž
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
–	–	k?	–
Vyprávěj	vyprávět	k5eAaImRp2nS	vyprávět
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
–	–	k?	–
Fabrika	fabrika	k1gFnSc1	fabrika
smrti	smrt	k1gFnSc2	smrt
<g/>
:	:	kIx,	:
Mladá	mladá	k1gFnSc1	mladá
krv	krvit	k5eAaImRp2nS	krvit
2014	[number]	k4	2014
–	–	k?	–
Všiváci	všivák	k1gMnPc1	všivák
2016	[number]	k4	2016
–	–	k?	–
Ordinace	ordinace	k1gFnSc1	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
2	[number]	k4	2
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andrea	Andrea	k1gFnSc1	Andrea
Kerestešová	Kerestešová	k1gFnSc1	Kerestešová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
Andrea	Andrea	k1gFnSc1	Andrea
Kerestešová	Kerestešová	k1gFnSc1	Kerestešová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Andrea	Andrea	k1gFnSc1	Andrea
Kerestešová	Kerestešová	k1gFnSc1	Kerestešová
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Andrea	Andrea	k1gFnSc1	Andrea
Kerestešová	Kerestešová	k1gFnSc1	Kerestešová
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chat	chata	k1gFnPc2	chata
s	s	k7c7	s
osobností	osobnost	k1gFnSc7	osobnost
ČT	ČT	kA	ČT
</s>
