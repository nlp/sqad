<p>
<s>
Balalajka	balalajka	k1gFnSc1	balalajka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
б	б	k?	б
<g/>
́	́	k?	́
<g/>
й	й	k?	й
<g/>
,	,	kIx,	,
balalajka	balalajka	k1gFnSc1	balalajka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strunný	strunný	k2eAgInSc4d1	strunný
nástroj	nástroj	k1gInSc4	nástroj
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
s	s	k7c7	s
trojdílnou	trojdílný	k2eAgFnSc7d1	trojdílná
rezonanční	rezonanční	k2eAgFnSc7d1	rezonanční
skříní	skříň	k1gFnSc7	skříň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
struny	struna	k1gFnSc2	struna
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
6	[number]	k4	6
v	v	k7c6	v
páru	pár	k1gInSc6	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmatník	hmatník	k1gInSc1	hmatník
je	být	k5eAaImIp3nS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
pražci	pražec	k1gInPc7	pražec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
6	[number]	k4	6
velikostech	velikost	k1gFnPc6	velikost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
piccolo	piccola	k1gFnSc5	piccola
(	(	kIx(	(
<g/>
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
prima	prima	k6eAd1	prima
</s>
</p>
<p>
<s>
sekunda	sekunda	k1gFnSc1	sekunda
</s>
</p>
<p>
<s>
altová	altový	k2eAgFnSc1d1	Altová
</s>
</p>
<p>
<s>
basová	basový	k2eAgFnSc1d1	basová
</s>
</p>
<p>
<s>
kontrabasová	kontrabasový	k2eAgFnSc1d1	kontrabasová
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
větší	veliký	k2eAgFnSc1d2	veliký
kopie	kopie	k1gFnSc1	kopie
-	-	kIx~	-
sub-kontrabasová	subontrabasová	k1gFnSc1	sub-kontrabasová
<g/>
)	)	kIx)	)
<g/>
Nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
sólový	sólový	k2eAgInSc1d1	sólový
nástroj	nástroj	k1gInSc1	nástroj
je	být	k5eAaImIp3nS	být
prima	prima	k2eAgInSc1d1	prima
<g/>
,	,	kIx,	,
laděná	laděný	k2eAgFnSc1d1	laděná
E-E-	E-E-	k1gFnSc1	E-E-
<g/>
A.	A.	kA	A.
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
balalajka	balalajka	k1gFnSc1	balalajka
laděna	laděn	k2eAgFnSc1d1	laděna
"	"	kIx"	"
<g/>
kytarovým	kytarový	k2eAgInSc7d1	kytarový
stylem	styl	k1gInSc7	styl
<g/>
"	"	kIx"	"
G-H-D	G-H-D	k1gFnSc1	G-H-D
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
tři	tři	k4xCgFnPc4	tři
nejtenčí	tenký	k2eAgFnPc4d3	nejtenčí
struny	struna	k1gFnPc4	struna
ruské	ruský	k2eAgFnSc2d1	ruská
kytary	kytara	k1gFnSc2	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
ruské	ruský	k2eAgMnPc4d1	ruský
kytaristy	kytarista	k1gMnPc4	kytarista
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
tenhle	tenhle	k3xDgInSc4	tenhle
způsob	způsob	k1gInSc4	způsob
ladění	ladění	k1gNnSc4	ladění
nerad	nerad	k2eAgMnSc1d1	nerad
viděn	viděn	k2eAgMnSc1d1	viděn
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Piccolo	Piccola	k1gFnSc5	Piccola
<g/>
,	,	kIx,	,
prima	prima	k6eAd1	prima
a	a	k8xC	a
sekunda	sekunda	k1gFnSc1	sekunda
mají	mít	k5eAaImIp3nP	mít
struny	struna	k1gFnSc2	struna
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
ze	z	k7c2	z
střev	střevo	k1gNnPc2	střevo
<g/>
;	;	kIx,	;
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ale	ale	k9	ale
začíná	začínat	k5eAaImIp3nS	začínat
používat	používat	k5eAaImF	používat
i	i	k9	i
nylon	nylon	k1gInSc4	nylon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Brač	Brač	k1gInSc1	Brač
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Citera	citera	k1gFnSc1	citera
</s>
</p>
<p>
<s>
Kithara	kithara	k1gFnSc1	kithara
</s>
</p>
<p>
<s>
Kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Mandolína	mandolína	k1gFnSc1	mandolína
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
balalajka	balalajka	k1gFnSc1	balalajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
balalajka	balalajka	k1gFnSc1	balalajka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Balalaika	Balalaika	k1gFnSc1	Balalaika
and	and	k?	and
Domra	domra	k1gFnSc1	domra
Association	Association	k1gInSc1	Association
of	of	k?	of
America	America	k1gFnSc1	America
</s>
</p>
<p>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
<g/>
:	:	kIx,	:
Noty	nota	k1gFnSc2	nota
</s>
</p>
<p>
<s>
Balalaika	Balalaika	k1gFnSc1	Balalaika
<g/>
.	.	kIx.	.
<g/>
fr	fr	k0	fr
<g/>
:	:	kIx,	:
Historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
mp	mp	k?	mp
<g/>
3	[number]	k4	3
atd	atd	kA	atd
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
</s>
</p>
