<s>
AMV	AMV	kA
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
AMV	AMV	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Anime	Anime	k1gNnSc1
music	music	k1gMnSc1
video	video	k1gNnSc4
(	(	kIx(
<g/>
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
AMV	AMV	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hudební	hudební	k2eAgInSc4d1
videoklip	videoklip	k1gInSc4
obsahující	obsahující	k2eAgInSc4d1
jednu	jeden	k4xCgFnSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
scén	scéna	k1gFnPc2
z	z	k7c2
anime	anime	k1gNnSc1
<g/>
,	,	kIx,
doplněnou	doplněný	k2eAgFnSc7d1
vhodnou	vhodný	k2eAgFnSc7d1
a	a	k8xC
odpovídající	odpovídající	k2eAgFnSc7d1
hudbou	hudba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
drtivé	drtivý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
amatérskou	amatérský	k2eAgFnSc4d1
a	a	k8xC
neoficiální	oficiální	k2eNgFnSc4d1,k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
(	(	kIx(
<g/>
stylem	styl	k1gInSc7
by	by	kYmCp3nP
fans	fans	k1gInSc4
for	forum	k1gNnPc2
fans	fans	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kvalitní	kvalitní	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
se	se	k3xPyFc4
promítají	promítat	k5eAaImIp3nP
na	na	k7c6
anime-srazech	anime-sraz	k1gInPc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
po	po	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
už	už	k6eAd1
samotný	samotný	k2eAgInSc1d1
název	název	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
japonskou	japonský	k2eAgFnSc4d1
animaci	animace	k1gFnSc4
<g/>
,	,	kIx,
dobré	dobrá	k1gFnPc4
AMV	AMV	kA
lze	lze	k6eAd1
sestříhat	sestříhat	k5eAaPmF
i	i	k9
z	z	k7c2
nejaponské	japonský	k2eNgFnSc2d1
animace	animace	k1gFnSc2
a	a	k8xC
jako	jako	k9
hudební	hudební	k2eAgInSc4d1
doprovod	doprovod	k1gInSc4
lze	lze	k6eAd1
použít	použít	k5eAaPmF
téměř	téměř	k6eAd1
libovolnou	libovolný	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
k	k	k7c3
videu	video	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
AMV	AMV	kA
wiki	wiki	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
odkazy	odkaz	k1gInPc4
na	na	k7c4
význačná	význačný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
v	v	k7c6
tomto	tento	k3xDgInSc6
oboru	obor	k1gInSc6
Archivováno	archivován	k2eAgNnSc1d1
15	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Portál	portál	k1gInSc1
o	o	k7c4
AMV	AMV	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
