<s>
Herbert	Herbert	k1gMnSc1	Herbert
Alexander	Alexandra	k1gFnPc2	Alexandra
Rosenfeld	Rosenfeld	k1gMnSc1	Rosenfeld
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1910	[number]	k4	1910
Norimberk	Norimberk	k1gInSc1	Norimberk
−	−	k?	−
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1986	[number]	k4	1986
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
kleinánské	kleinánský	k2eAgFnSc2d1	kleinánský
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
dvě	dva	k4xCgFnPc4	dva
formy	forma	k1gFnPc4	forma
subjektivity	subjektivita	k1gFnSc2	subjektivita
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
grandiózní	grandiózní	k2eAgInSc1d1	grandiózní
Self	Self	k1gInSc1	Self
a	a	k8xC	a
libidinosní	libidinosní	k2eAgInSc1d1	libidinosní
Self	Self	k1gInSc1	Self
<g/>
.	.	kIx.	.
</s>
<s>
Libidinosní	Libidinosní	k2eAgInSc1d1	Libidinosní
Self	Self	k1gInSc1	Self
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
stává	stávat	k5eAaImIp3nS	stávat
závislým	závislý	k2eAgMnPc3d1	závislý
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
grandiózní	grandiózní	k2eAgInSc1d1	grandiózní
Self	Self	k1gInSc1	Self
děsí	děsit	k5eAaImIp3nS	děsit
a	a	k8xC	a
proto	proto	k8xC	proto
agresivně	agresivně	k6eAd1	agresivně
napadá	napadat	k5eAaBmIp3nS	napadat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
grandiózní	grandiózní	k2eAgInSc4d1	grandiózní
Self	Self	k1gInSc4	Self
v	v	k7c6	v
osobnosti	osobnost	k1gFnSc6	osobnost
převáží	převážet	k5eAaImIp3nS	převážet
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
dle	dle	k7c2	dle
Rosenfelda	Rosenfeld	k1gMnSc2	Rosenfeld
narcistická	narcistický	k2eAgFnSc1d1	narcistická
povahová	povahový	k2eAgFnSc1d1	povahová
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
základna	základna	k1gFnSc1	základna
psychózy	psychóza	k1gFnSc2	psychóza
i	i	k8xC	i
hraniční	hraniční	k2eAgFnSc2d1	hraniční
poruchy	porucha	k1gFnSc2	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Self	Self	k1gInSc1	Self
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
bez	bez	k7c2	bez
libidinosního	libidinosní	k2eAgNnSc2d1	libidinosní
ukojení	ukojení	k1gNnSc2	ukojení
prázdné	prázdný	k2eAgNnSc1d1	prázdné
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
zoufaleji	zoufale	k6eAd2	zoufale
začne	začít	k5eAaPmIp3nS	začít
dychtit	dychtit	k5eAaImF	dychtit
po	po	k7c6	po
druhých	druhý	k4xOgMnPc6	druhý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
grandiózní	grandiózní	k2eAgInSc1d1	grandiózní
Self	Self	k1gInSc1	Self
k	k	k7c3	k
ještě	ještě	k6eAd1	ještě
paničtějším	panický	k2eAgFnPc3d2	panický
obranným	obranný	k2eAgFnPc3d1	obranná
reakcím	reakce	k1gFnPc3	reakce
a	a	k8xC	a
větší	veliký	k2eAgFnSc3d2	veliký
agresi	agrese	k1gFnSc3	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Takoví	takový	k3xDgMnPc1	takový
jedinec	jedinec	k1gMnSc1	jedinec
pak	pak	k8xC	pak
druhé	druhý	k4xOgNnSc1	druhý
využívá	využívat	k5eAaImIp3nS	využívat
a	a	k8xC	a
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
popírá	popírat	k5eAaImIp3nS	popírat
tím	ten	k3xDgNnSc7	ten
svou	svůj	k3xOyFgFnSc4	svůj
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
a	a	k8xC	a
chorobnou	chorobný	k2eAgFnSc4d1	chorobná
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
vztahu	vztah	k1gInSc6	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jedinec	jedinec	k1gMnSc1	jedinec
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jen	jen	k9	jen
narcistickým	narcistický	k2eAgMnSc7d1	narcistický
<g/>
,	,	kIx,	,
zafixují	zafixovat	k5eAaPmIp3nP	zafixovat
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc4	jeho
strategie	strategie	k1gFnPc4	strategie
na	na	k7c4	na
zdůrazňování	zdůrazňování	k1gNnSc4	zdůrazňování
své	svůj	k3xOyFgFnSc2	svůj
separace	separace	k1gFnSc2	separace
od	od	k7c2	od
druhých	druhý	k4xOgFnPc2	druhý
(	(	kIx(	(
<g/>
jíž	jenž	k3xRgFnSc3	jenž
si	se	k3xPyFc3	se
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
druhým	druhý	k4xOgNnSc7	druhý
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
ještě	ještě	k9	ještě
k	k	k7c3	k
patologičtějšímu	patologický	k2eAgInSc3d2	patologický
vývoji	vývoj	k1gInSc3	vývoj
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
stavům	stav	k1gInPc3	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedinec	jedinec	k1gMnSc1	jedinec
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
rozlišit	rozlišit	k5eAaPmF	rozlišit
mezi	mezi	k7c4	mezi
Self	Self	k1gInSc4	Self
a	a	k8xC	a
objektem	objekt	k1gInSc7	objekt
(	(	kIx(	(
<g/>
sebou	se	k3xPyFc7	se
a	a	k8xC	a
druhým	druhý	k4xOgMnSc7	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
si	se	k3xPyFc3	se
jedinec	jedinec	k1gMnSc1	jedinec
"	"	kIx"	"
<g/>
přivlastňuje	přivlastňovat	k5eAaImIp3nS	přivlastňovat
<g/>
"	"	kIx"	"
dobré	dobrý	k2eAgFnPc1d1	dobrá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
dobré	dobrý	k2eAgNnSc1d1	dobré
na	na	k7c6	na
druhých	druhý	k4xOgMnPc6	druhý
považuje	považovat	k5eAaImIp3nS	považovat
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nejasné	jasný	k2eNgFnSc2d1	nejasná
hranice	hranice	k1gFnSc2	hranice
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
druhými	druhý	k4xOgFnPc7	druhý
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
to	ten	k3xDgNnSc1	ten
zlé	zlý	k2eAgNnSc1d1	zlé
ze	z	k7c2	z
sebe	sebe	k3xPyFc4	sebe
"	"	kIx"	"
<g/>
odkládá	odkládat	k5eAaImIp3nS	odkládat
<g/>
"	"	kIx"	"
do	do	k7c2	do
druhých	druhý	k4xOgFnPc2	druhý
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
zlé	zlý	k2eAgFnPc4d1	zlá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
posiluje	posilovat	k5eAaImIp3nS	posilovat
jeho	jeho	k3xOp3gFnSc4	jeho
grandiozitu	grandiozita	k1gFnSc4	grandiozita
a	a	k8xC	a
nenávist	nenávist	k1gFnSc4	nenávist
vůči	vůči	k7c3	vůči
druhým	druhý	k4xOgMnPc3	druhý
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
i	i	k9	i
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bez	bez	k7c2	bez
nich	on	k3xPp3gInPc2	on
nemůže	moct	k5eNaImIp3nS	moct
tuto	tento	k3xDgFnSc4	tento
"	"	kIx"	"
<g/>
sebe-pročišťovací	seberočišťovací	k2eAgFnSc4d1	sebe-pročišťovací
<g/>
"	"	kIx"	"
hru	hra	k1gFnSc4	hra
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Superego	Superego	k6eAd1	Superego
takových	takový	k3xDgMnPc2	takový
narcistů	narcista	k1gMnPc2	narcista
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
slabé	slabý	k2eAgNnSc1d1	slabé
<g/>
,	,	kIx,	,
primitivní	primitivní	k2eAgFnSc1d1	primitivní
<g/>
,	,	kIx,	,
předoidipovské	předoidipovské	k2eAgFnSc1d1	předoidipovské
<g/>
,	,	kIx,	,
a	a	k8xC	a
Self	Self	k1gInSc1	Self
má	mít	k5eAaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
ho	on	k3xPp3gMnSc4	on
projikovat	projikovat	k5eAaImF	projikovat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
budí	budit	k5eAaImIp3nP	budit
paranoidní	paranoidní	k2eAgInPc4d1	paranoidní
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
bát	bát	k5eAaImF	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
druzí	druhý	k4xOgMnPc1	druhý
ho	on	k3xPp3gMnSc4	on
ztrestají	ztrestat	k5eAaPmIp3nP	ztrestat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
rozlišil	rozlišit	k5eAaPmAgInS	rozlišit
libidinosní	libidinosní	k2eAgInSc1d1	libidinosní
narcismus	narcismus	k1gInSc1	narcismus
a	a	k8xC	a
destruktivní	destruktivní	k2eAgInSc1d1	destruktivní
narcismus	narcismus	k1gInSc1	narcismus
<g/>
.	.	kIx.	.
</s>
<s>
Libidinosní	Libidinosní	k2eAgMnSc1d1	Libidinosní
narcista	narcista	k1gMnSc1	narcista
se	se	k3xPyFc4	se
především	především	k9	především
posiluje	posilovat	k5eAaImIp3nS	posilovat
identifikací	identifikace	k1gFnSc7	identifikace
s	s	k7c7	s
dobrým	dobrý	k2eAgInSc7d1	dobrý
objektem	objekt	k1gInSc7	objekt
<g/>
,	,	kIx,	,
destruktivní	destruktivní	k2eAgMnSc1d1	destruktivní
narcista	narcista	k1gMnSc1	narcista
idealizuje	idealizovat	k5eAaBmIp3nS	idealizovat
útočné	útočný	k2eAgInPc4d1	útočný
grandiózní	grandiózní	k2eAgInPc4d1	grandiózní
Self	Self	k1gInSc4	Self
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
destruktivní	destruktivní	k2eAgNnSc1d1	destruktivní
a	a	k8xC	a
izolacionistické	izolacionistický	k2eAgNnSc1d1	izolacionistické
<g/>
.	.	kIx.	.
</s>
<s>
FONAGY	FONAGY	kA	FONAGY
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
,	,	kIx,	,
TARGETOVÁ	TARGETOVÁ	kA	TARGETOVÁ
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
<g/>
:	:	kIx,	:
Psychoanalytické	psychoanalytický	k2eAgFnPc1d1	psychoanalytická
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Portál	portál	k1gInSc1	portál
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7178-993-3	[number]	k4	80-7178-993-3
</s>
