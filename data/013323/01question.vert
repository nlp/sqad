<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
husitský	husitský	k2eAgMnSc1d1	husitský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1424	[number]	k4	1424
padl	padnout	k5eAaImAgInS	padnout
během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
hradu	hrad	k1gInSc2	hrad
u	u	k7c2	u
Přibyslavi	Přibyslav	k1gFnSc2	Přibyslav
<g/>
?	?	kIx.	?
</s>
