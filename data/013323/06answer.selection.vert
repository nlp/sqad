<s>
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
a	a	k8xC	a
Kalicha	kalich	k1gInSc2	kalich
(	(	kIx(	(
<g/>
kolem	kolem	k6eAd1	kolem
1360	[number]	k4	1360
Trocnov	Trocnov	k1gInSc4	Trocnov
–	–	k?	–
11.	[number]	k4	11.
října	říjen	k1gInSc2	říjen
1424	[number]	k4	1424
Přibyslav	Přibyslava	k1gFnPc2	Přibyslava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
husitský	husitský	k2eAgMnSc1d1	husitský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgMnSc1d1	pokládaný
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
husitské	husitský	k2eAgFnSc2d1	husitská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
doktríny	doktrína	k1gFnSc2	doktrína
a	a	k8xC	a
autora	autor	k1gMnSc2	autor
či	či	k8xC	či
prvního	první	k4xOgMnSc2	první
uživatele	uživatel	k1gMnSc2	uživatel
defenzivní	defenzivní	k2eAgFnSc2d1	defenzivní
bojové	bojový	k2eAgFnSc2d1	bojová
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
<g/>
.	.	kIx.	.
</s>
