<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1811	[number]	k4	1811
Miletín	Miletín	k1gInSc1	Miletín
–	–	k?	–
21.	[number]	k4	21.
listopadu	listopad	k1gInSc2	listopad
1870	[number]	k4	1870
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
českých	český	k2eAgFnPc2d1	Česká
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
;	;	kIx,	;
představitel	představitel	k1gMnSc1	představitel
literárního	literární	k2eAgInSc2d1	literární
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
českého	český	k2eAgNnSc2d1	české
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
svých	svůj	k3xOyFgFnPc6	svůj
studiích	studie	k1gFnPc6	studie
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgMnS	být
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
a	a	k8xC	a
sekretářem	sekretář	k1gInSc7	sekretář
Českého	český	k2eAgNnSc2d1	české
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
archivář	archivář	k1gMnSc1	archivář
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
pomocných	pomocný	k2eAgInPc2d1	pomocný
úřadů	úřad	k1gInPc2	úřad
pražských	pražský	k2eAgInPc2d1	pražský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
studijních	studijní	k2eAgNnPc6d1	studijní
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Erben	Erben	k1gMnSc1	Erben
stýkal	stýkat	k5eAaImAgMnS	stýkat
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Hynkem	Hynek	k1gMnSc7	Hynek
Máchou	Mácha	k1gMnSc7	Mácha
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
také	také	k9	také
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Palackým	Palacký	k1gMnSc7	Palacký
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
českého	český	k2eAgInSc2d1	český
diplomatáře	diplomatář	k1gInSc2	diplomatář
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
politickými	politický	k2eAgInPc7d1	politický
názory	názor	k1gInPc7	názor
byl	být	k5eAaImAgInS	být
trvale	trvale	k6eAd1	trvale
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
s	s	k7c7	s
Martinem	Martin	k1gMnSc7	Martin
Hattalou	Hattalý	k2eAgFnSc4d1	Hattalý
Erbena	Erben	k1gMnSc2	Erben
vyčerpávaly	vyčerpávat	k5eAaImAgFnP	vyčerpávat
a	a	k8xC	a
přispívaly	přispívat	k5eAaImAgFnP	přispívat
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
jeho	jeho	k3xOp3gInSc2	jeho
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Erben	Erben	k1gMnSc1	Erben
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
především	především	k6eAd1	především
jako	jako	k8xC	jako
sběratel	sběratel	k1gMnSc1	sběratel
lidové	lidový	k2eAgFnSc2d1	lidová
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
názorů	názor	k1gInPc2	názor
bratří	bratřit	k5eAaImIp3nS	bratřit
Grimmů	Grimm	k1gInPc2	Grimm
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
hledal	hledat	k5eAaImAgMnS	hledat
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
lidové	lidový	k2eAgFnSc6d1	lidová
slovesnosti	slovesnost	k1gFnSc6	slovesnost
odraz	odraz	k1gInSc1	odraz
starých	starý	k2eAgInPc2d1	starý
mýtů	mýtus	k1gInPc2	mýtus
(	(	kIx(	(
<g/>
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Grimmů	Grimm	k1gMnPc2	Grimm
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hledali	hledat	k5eAaImAgMnP	hledat
germánské	germánský	k2eAgInPc4d1	germánský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
lidové	lidový	k2eAgNnSc1d1	lidové
podání	podání	k1gNnSc1	podání
a	a	k8xC	a
tradice	tradice	k1gFnPc1	tradice
během	během	k7c2	během
věků	věk	k1gInPc2	věk
přetvořily	přetvořit	k5eAaPmAgFnP	přetvořit
a	a	k8xC	a
často	často	k6eAd1	často
zakryly	zakrýt	k5eAaPmAgInP	zakrýt
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
Erbenovy	Erbenův	k2eAgFnSc2d1	Erbenova
sběratelské	sběratelský	k2eAgFnSc2d1	sběratelská
činnosti	činnost	k1gFnSc2	činnost
byly	být	k5eAaImAgInP	být
tři	tři	k4xCgInPc1	tři
svazky	svazek	k1gInPc1	svazek
Písní	píseň	k1gFnPc2	píseň
národních	národní	k2eAgFnPc2d1	národní
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
–	–	k?	–
<g/>
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
a	a	k8xC	a
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Prostonárodní	prostonárodní	k2eAgFnSc2d1	prostonárodní
české	český	k2eAgFnSc2d1	Česká
písně	píseň	k1gFnSc2	píseň
a	a	k8xC	a
říkadla	říkadlo	k1gNnSc2	říkadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
však	však	k9	však
Erben	Erben	k1gMnSc1	Erben
proslul	proslout	k5eAaPmAgMnS	proslout
sbírkou	sbírka	k1gFnSc7	sbírka
Kytice	kytice	k1gFnSc2	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgInPc2d1	národní
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
Kytice	kytice	k1gFnSc1	kytice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
podruhé	podruhé	k6eAd1	podruhé
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
verzi	verze	k1gFnSc6	verze
(	(	kIx(	(
<g/>
s	s	k7c7	s
oddílem	oddíl	k1gInSc7	oddíl
příležitostných	příležitostný	k2eAgFnPc2d1	příležitostná
písní	píseň	k1gFnPc2	píseň
<g/>
)	)	kIx)	)
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kytice	kytice	k1gFnSc1	kytice
z	z	k7c2	z
básní	báseň	k1gFnPc2	báseň
K.	K.	kA	K.
J.	J.	kA	J.
Erbena	Erben	k1gMnSc2	Erben
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
této	tento	k3xDgFnSc2	tento
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
dvanáct	dvanáct	k4xCc1	dvanáct
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
13.	[number]	k4	13.
balada	balada	k1gFnSc1	balada
–	–	k?	–
Lilie	lilie	k1gFnSc2	lilie
<g/>
)	)	kIx)	)
básní	báseň	k1gFnPc2	báseň
oddílu	oddíl	k1gInSc2	oddíl
Pověsti	pověst	k1gFnSc2	pověst
národní	národní	k2eAgFnSc2d1	národní
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
předchází	předcházet	k5eAaImIp3nS	předcházet
úvodní	úvodní	k2eAgFnSc4d1	úvodní
báseň	báseň	k1gFnSc4	báseň
Kytice	kytice	k1gFnSc2	kytice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
===	===	k?	===
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Miletíně	Miletín	k1gInSc6	Miletín
7.	[number]	k4	7.
listopadu	listopad	k1gInSc2	listopad
1811	[number]	k4	1811
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
příjmení	příjmení	k1gNnSc1	příjmení
jeho	jeho	k3xOp3gInPc2	jeho
předků	předek	k1gInPc2	předek
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
psáno	psát	k5eAaImNgNnS	psát
Erban	Erban	k1gMnSc1	Erban
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc3	jeho
rodičům	rodič	k1gMnPc3	rodič
–	–	k?	–
ševci	švec	k1gMnSc6	švec
a	a	k8xC	a
sadaři	sadař	k1gMnSc6	sadař
Janu	Jan	k1gMnSc6	Jan
Erbenovi	Erben	k1gMnSc6	Erben
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
Anně	Anna	k1gFnSc3	Anna
–	–	k?	–
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgNnP	narodit
dvojčata	dvojče	k1gNnPc1	dvojče
Karel	Karla	k1gFnPc2	Karla
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
však	však	k9	však
již	již	k6eAd1	již
29.	[number]	k4	29.
12.	[number]	k4	12.
1811	[number]	k4	1811
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Karel	Karel	k1gMnSc1	Karel
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
nebyl	být	k5eNaImAgMnS	být
zdravotně	zdravotně	k6eAd1	zdravotně
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
churavěl	churavět	k5eAaImAgMnS	churavět
a	a	k8xC	a
trpěl	trpět	k5eAaImAgMnS	trpět
poruchou	porucha	k1gFnSc7	porucha
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Základního	základní	k2eAgNnSc2d1	základní
vzdělání	vzdělání	k1gNnSc2	vzdělání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c6	v
miletínské	miletínský	k2eAgFnSc6d1	miletínská
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučovali	vyučovat	k5eAaImAgMnP	vyučovat
také	také	k9	také
jeho	jeho	k3xOp3gInSc4	jeho
strýc	strýc	k1gMnSc1	strýc
a	a	k8xC	a
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
postupně	postupně	k6eAd1	postupně
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
všechna	všechen	k3xTgNnPc4	všechen
jeho	jeho	k3xOp3gNnPc4	jeho
nadání	nadání	k1gNnPc4	nadání
včetně	včetně	k7c2	včetně
hudebního	hudební	k2eAgNnSc2d1	hudební
a	a	k8xC	a
připravovali	připravovat	k5eAaImAgMnP	připravovat
ho	on	k3xPp3gNnSc4	on
na	na	k7c4	na
pozdější	pozdní	k2eAgNnPc4d2	pozdější
studia	studio	k1gNnPc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
chtěli	chtít	k5eAaImAgMnP	chtít
mít	mít	k5eAaImF	mít
také	také	k9	také
učitele	učitel	k1gMnSc4	učitel
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
vadě	vada	k1gFnSc3	vada
řeči	řeč	k1gFnSc2	řeč
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Později	pozdě	k6eAd2	pozdě
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
ustavení	ustavení	k1gNnSc3	ustavení
české	český	k2eAgFnSc6d1	Česká
právnické	právnický	k2eAgFnSc6d1	právnická
terminologii	terminologie	k1gFnSc6	terminologie
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
hradeckém	hradecký	k2eAgNnSc6d1	hradecké
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
si	se	k3xPyFc3	se
sám	sám	k3xTgInSc4	sám
obstarat	obstarat	k5eAaPmF	obstarat
výdělek	výdělek	k1gInSc4	výdělek
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vyučováním	vyučování	k1gNnSc7	vyučování
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
gymnázium	gymnázium	k1gNnSc4	gymnázium
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
roku	rok	k1gInSc2	rok
1825	[number]	k4	1825
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
rád	rád	k6eAd1	rád
podnikal	podnikat	k5eAaImAgMnS	podnikat
výlety	výlet	k1gInPc4	výlet
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
rodného	rodný	k2eAgInSc2d1	rodný
Miletína	Miletín	k1gInSc2	Miletín
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Erben	Erben	k1gMnSc1	Erben
na	na	k7c4	na
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
započal	započnout	k5eAaPmAgInS	započnout
studium	studium	k1gNnSc4	studium
práv	právo	k1gNnPc2	právo
(	(	kIx(	(
<g/>
1833	[number]	k4	1833
<g/>
–	–	k?	–
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
studiu	studio	k1gNnSc6	studio
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
projevily	projevit	k5eAaPmAgFnP	projevit
jeho	jeho	k3xOp3gFnPc1	jeho
historické	historický	k2eAgFnPc1d1	historická
a	a	k8xC	a
přírodovědecké	přírodovědecký	k2eAgInPc1d1	přírodovědecký
zájmy	zájem	k1gInPc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
Erben	Erben	k1gMnSc1	Erben
začal	začít	k5eAaPmAgMnS	začít
svými	svůj	k3xOyFgFnPc7	svůj
básněmi	báseň	k1gFnPc7	báseň
přispívat	přispívat	k5eAaImF	přispívat
do	do	k7c2	do
českých	český	k2eAgInPc2d1	český
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rodinný	rodinný	k2eAgInSc1d1	rodinný
život	život	k1gInSc1	život
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
byla	být	k5eAaImAgFnS	být
Barbora	Barbora	k1gFnSc1	Barbora
(	(	kIx(	(
<g/>
Betyna	Betyna	k?	Betyna
<g/>
)	)	kIx)	)
Mečířová	Mečířová	k1gFnSc1	Mečířová
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
6.	[number]	k4	6.
1818	[number]	k4	1818
Žebrák	Žebrák	k1gInSc1	Žebrák
–	–	k?	–
20.	[number]	k4	20.
8.	[number]	k4	8.
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
na	na	k7c6	na
prázdninových	prázdninový	k2eAgInPc6d1	prázdninový
pobytech	pobyt	k1gInPc6	pobyt
v	v	k7c6	v
Žebráku	žebrák	k1gMnSc6	žebrák
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
desetileté	desetiletý	k2eAgFnSc6d1	desetiletá
známosti	známost	k1gFnSc6	známost
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
a	a	k8xC	a
manželé	manžel	k1gMnPc1	manžel
Erbenovi	Erbenův	k2eAgMnPc1d1	Erbenův
měli	mít	k5eAaImAgMnP	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Blaženu	Blažena	k1gFnSc4	Blažena
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ladislavu	Ladislava	k1gFnSc4	Ladislava
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaromíra	Jaromíra	k1gFnSc1	Jaromíra
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bohuslavu	Bohuslava	k1gFnSc4	Bohuslava
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manželky	manželka	k1gFnSc2	manželka
se	se	k3xPyFc4	se
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
Žofií	Žofie	k1gFnSc7	Žofie
Mastnou	mastný	k2eAgFnSc7d1	mastná
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
5.	[number]	k4	5.
1836	[number]	k4	1836
Jičín	Jičín	k1gInSc1	Jičín
–	–	k?	–
23.	[number]	k4	23.
9.	[number]	k4	9.
1905	[number]	k4	1905
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
ještě	ještě	k6eAd1	ještě
syna	syn	k1gMnSc4	syn
Vladimíra	Vladimír	k1gMnSc4	Vladimír
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc4	Maria
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
dokončil	dokončit	k5eAaPmAgMnS	dokončit
svá	svůj	k3xOyFgNnPc4	svůj
právnická	právnický	k2eAgNnPc4d1	právnické
studia	studio	k1gNnPc4	studio
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Erben	Erben	k1gMnSc1	Erben
jako	jako	k8xS	jako
praktikant	praktikant	k1gMnSc1	praktikant
k	k	k7c3	k
hrdelnímu	hrdelní	k2eAgInSc3d1	hrdelní
soudu	soud	k1gInSc3	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
také	také	k9	také
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
Palackému	Palacký	k1gMnSc3	Palacký
spravovat	spravovat	k5eAaImF	spravovat
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
zanedbaný	zanedbaný	k2eAgInSc1d1	zanedbaný
archiv	archiv	k1gInSc1	archiv
stavovský	stavovský	k2eAgInSc1d1	stavovský
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
způsobilý	způsobilý	k2eAgInSc1d1	způsobilý
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
zvolen	zvolit	k5eAaPmNgMnS	zvolit
českou	český	k2eAgFnSc7d1	Česká
učenou	učený	k2eAgFnSc7d1	učená
společností	společnost	k1gFnSc7	společnost
nauk	nauka	k1gFnPc2	nauka
za	za	k7c4	za
aktuára	aktuár	k1gMnSc4	aktuár
(	(	kIx(	(
<g/>
kdysi	kdysi	k6eAd1	kdysi
nižší	nízký	k2eAgMnSc1d2	nižší
správní	správní	k2eAgMnSc1d1	správní
úředník	úředník	k1gMnSc1	úředník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
historických	historický	k2eAgNnPc6d1	historické
studiích	studio	k1gNnPc6	studio
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
řádným	řádný	k2eAgMnSc7d1	řádný
členem	člen	k1gMnSc7	člen
Královské	královský	k2eAgFnSc2d1	královská
české	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1852	[number]	k4	1852
<g/>
–	–	k?	–
<g/>
1853	[number]	k4	1853
a	a	k8xC	a
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1864	[number]	k4	1864
pak	pak	k6eAd1	pak
jejím	její	k3xOp3gMnSc7	její
direktorem	direktor	k1gMnSc7	direktor
(	(	kIx(	(
<g/>
předsedou	předseda	k1gMnSc7	předseda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
členem	člen	k1gMnSc7	člen
jazykové	jazykový	k2eAgFnSc2d1	jazyková
komise	komise	k1gFnSc2	komise
při	při	k7c6	při
Matici	matice	k1gFnSc6	matice
české	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
tak	tak	k9	tak
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
formování	formování	k1gNnSc6	formování
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1848	[number]	k4	1848
nebyl	být	k5eNaImAgInS	být
moc	moc	k6eAd1	moc
příznivý	příznivý	k2eAgInSc1d1	příznivý
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
hodlal	hodlat	k5eAaImAgMnS	hodlat
zabývat	zabývat	k5eAaImF	zabývat
historií	historie	k1gFnSc7	historie
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
Erben	Erben	k1gMnSc1	Erben
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
Erben	Erben	k1gMnSc1	Erben
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obeznámil	obeznámit	k5eAaPmAgInS	obeznámit
tamní	tamní	k2eAgMnPc4d1	tamní
občany	občan	k1gMnPc4	občan
s	s	k7c7	s
děním	dění	k1gNnSc7	dění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
o	o	k7c6	o
důležitých	důležitý	k2eAgFnPc6d1	důležitá
věcech	věc	k1gFnPc6	věc
mezi	mezi	k7c7	mezi
Jihoslovany	Jihoslovan	k1gMnPc7	Jihoslovan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
překladatelem	překladatel	k1gMnSc7	překladatel
u	u	k7c2	u
zemské	zemský	k2eAgFnSc2d1	zemská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
byl	být	k5eAaImAgMnS	být
Erben	Erben	k1gMnSc1	Erben
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
sekretářem	sekretář	k1gMnSc7	sekretář
Českého	český	k2eAgNnSc2d1	české
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
archivářem	archivář	k1gMnSc7	archivář
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
–	–	k?	–
konečně	konečně	k6eAd1	konečně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tak	tak	k6eAd1	tak
dostalo	dostat	k5eAaPmAgNnS	dostat
uspokojivého	uspokojivý	k2eAgNnSc2d1	uspokojivé
finančního	finanční	k2eAgNnSc2d1	finanční
ohodnocení	ohodnocení	k1gNnSc2	ohodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Erben	Erben	k1gMnSc1	Erben
se	se	k3xPyFc4	se
také	také	k9	také
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
vědeckého	vědecký	k2eAgInSc2d1	vědecký
a	a	k8xC	a
kritického	kritický	k2eAgInSc2d1	kritický
časopisu	časopis	k1gInSc2	časopis
Obzor	obzor	k1gInSc1	obzor
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
dodnes	dodnes	k6eAd1	dodnes
vycházejícího	vycházející	k2eAgInSc2d1	vycházející
časopisu	časopis	k1gInSc2	časopis
Právník	právník	k1gMnSc1	právník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
byl	být	k5eAaImAgMnS	být
Erben	Erben	k1gMnSc1	Erben
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ředitelem	ředitel	k1gMnSc7	ředitel
pomocných	pomocný	k2eAgFnPc2d1	pomocná
kanceláří	kancelář	k1gFnPc2	kancelář
úřadů	úřad	k1gInPc2	úřad
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
také	také	k9	také
vydává	vydávat	k5eAaPmIp3nS	vydávat
Prostonárodní	prostonárodní	k2eAgFnPc4d1	prostonárodní
české	český	k2eAgFnPc4d1	Česká
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
říkadla	říkadlo	k1gNnPc4	říkadlo
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Sto	sto	k4xCgNnSc4	sto
prostonárodních	prostonárodní	k2eAgFnPc2d1	prostonárodní
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
v	v	k7c6	v
nářečích	nářečí	k1gNnPc6	nářečí
původních	původní	k2eAgNnPc6d1	původní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
21.	[number]	k4	21.
listopadu	listopad	k1gInSc2	listopad
1870	[number]	k4	1870
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Tří	tři	k4xCgFnPc2	tři
tykví	tykev	k1gFnPc2	tykev
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
Michalské	Michalský	k2eAgFnSc6d1	Michalská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
Malostranském	malostranský	k2eAgInSc6d1	malostranský
hřbitově	hřbitov	k1gInSc6	hřbitov
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
však	však	k9	však
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
přemístěny	přemístěn	k2eAgInPc4d1	přemístěn
na	na	k7c4	na
pražské	pražský	k2eAgInPc4d1	pražský
Olšanské	olšanský	k2eAgInPc4d1	olšanský
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Adresy	adresa	k1gFnSc2	adresa
pobytu	pobyt	k1gInSc2	pobyt
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnPc4d1	pamětní
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
pomníky	pomník	k1gInPc4	pomník
==	==	k?	==
</s>
</p>
<p>
<s>
1857	[number]	k4	1857
<g/>
–	–	k?	–
<g/>
1867	[number]	k4	1867
<g/>
:	:	kIx,	:
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Velkého	velký	k2eAgInSc2d1	velký
střevíce	střevíc	k1gInSc2	střevíc
(	(	kIx(	(
<g/>
také	také	k9	také
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Velké	velký	k2eAgFnSc2d1	velká
boty	bota	k1gFnSc2	bota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vlašská	vlašský	k2eAgFnSc1d1	vlašská
333/30	[number]	k4	333/30
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1867	[number]	k4	1867
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
<g/>
)	)	kIx)	)
–	–	k?	–
1870	[number]	k4	1870
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
11.	[number]	k4	11.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Jelena	Jelen	k1gMnSc2	Jelen
(	(	kIx(	(
<g/>
také	také	k9	také
U	u	k7c2	u
Pěti	pět	k4xCc2	pět
tykví	tykev	k1gFnPc2	tykev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michalská	Michalský	k2eAgFnSc1d1	Michalská
440/11	[number]	k4	440/11
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
textem	text	k1gInSc7	text
<g/>
:	:	kIx,	:
Zde	zde	k6eAd1	zde
žil	žíla	k1gFnPc2	žíla
a	a	k8xC	a
dne	den	k1gInSc2	den
21.	[number]	k4	21.
listopadu	listopad	k1gInSc2	listopad
1870	[number]	k4	1870
zemřel	zemřít	k5eAaPmAgMnS	zemřít
KAREL	Karel	k1gMnSc1	Karel
JAR	jaro	k1gNnPc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
ERBEN	Erben	k1gMnSc1	Erben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Těžiště	těžiště	k1gNnSc1	těžiště
svého	svůj	k3xOyFgInSc2	svůj
odborného	odborný	k2eAgInSc2d1	odborný
zájmu	zájem	k1gInSc2	zájem
Erben	Erben	k1gMnSc1	Erben
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
v	v	k7c6	v
edicích	edice	k1gFnPc6	edice
folklórních	folklórní	k2eAgInPc2d1	folklórní
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
především	především	k9	především
českých	český	k2eAgFnPc2d1	Česká
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Srovnával	srovnávat	k5eAaImAgMnS	srovnávat
jejich	jejich	k3xOp3gFnPc4	jejich
varianty	varianta	k1gFnPc4	varianta
a	a	k8xC	a
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nejlépe	dobře	k6eAd3	dobře
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
předpokládanému	předpokládaný	k2eAgInSc3d1	předpokládaný
původnímu	původní	k2eAgInSc3d1	původní
tvaru	tvar	k1gInSc3	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
písně	píseň	k1gFnPc4	píseň
se	se	k3xPyFc4	se
díval	dívat	k5eAaImAgInS	dívat
jako	jako	k9	jako
na	na	k7c4	na
zpívané	zpívaný	k2eAgInPc4d1	zpívaný
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
přihlížel	přihlížet	k5eAaImAgMnS	přihlížet
proto	proto	k8xC	proto
i	i	k9	i
k	k	k7c3	k
nápěvům	nápěv	k1gInPc3	nápěv
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
rovněž	rovněž	k9	rovněž
vydával	vydávat	k5eAaPmAgInS	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Erben	Erben	k1gMnSc1	Erben
neuznával	uznávat	k5eNaImAgMnS	uznávat
vzpoury	vzpoura	k1gFnSc2	vzpoura
proti	proti	k7c3	proti
osudu	osud	k1gInSc3	osud
<g/>
,	,	kIx,	,
uctíval	uctívat	k5eAaImAgInS	uctívat
daný	daný	k2eAgInSc4d1	daný
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
básních	báseň	k1gFnPc6	báseň
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
témata	téma	k1gNnPc1	téma
viny	vina	k1gFnSc2	vina
a	a	k8xC	a
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Sto	sto	k4xCgNnSc4	sto
prostonárodních	prostonárodní	k2eAgFnPc2d1	prostonárodní
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
v	v	k7c6	v
nářečích	nářečí	k1gNnPc6	nářečí
původních	původní	k2eAgMnPc2d1	původní
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
–	–	k?	–
jakási	jakýsi	k3yIgFnSc1	jakýsi
"	"	kIx"	"
<g/>
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
čítanka	čítanka	k1gFnSc1	čítanka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
národní	národní	k2eAgFnPc1d1	národní
jiných	jiný	k2eAgFnPc2d1	jiná
větví	větev	k1gFnPc2	větev
slovanských	slovanský	k2eAgInPc2d1	slovanský
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
–	–	k?	–
"	"	kIx"	"
<g/>
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
čítanka	čítanka	k1gFnSc1	čítanka
<g/>
"	"	kIx"	"
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
úpravě	úprava	k1gFnSc6	úprava
a	a	k8xC	a
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
</s>
</p>
<p>
<s>
Kytice	kytice	k1gFnSc1	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgInPc2d1	národní
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vydání	vydání	k1gNnSc1	vydání
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
–	–	k?	–
jediná	jediný	k2eAgFnSc1d1	jediná
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
vydal	vydat	k5eAaPmAgMnS	vydat
<g/>
;	;	kIx,	;
podkladem	podklad	k1gInSc7	podklad
Kytice	kytice	k1gFnSc2	kytice
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc1d1	stará
české	český	k2eAgFnPc1d1	Česká
lidové	lidový	k2eAgFnPc1d1	lidová
báje	báj	k1gFnPc1	báj
</s>
</p>
<p>
<s>
Písně	píseň	k1gFnPc1	píseň
národní	národní	k2eAgFnPc1d1	národní
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
500	[number]	k4	500
písní	píseň	k1gFnPc2	píseň
</s>
</p>
<p>
<s>
Prostonárodní	prostonárodní	k2eAgFnPc1d1	prostonárodní
české	český	k2eAgFnPc1d1	Česká
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
říkadla	říkadlo	k1gNnPc1	říkadlo
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
–	–	k?	–
pětidílná	pětidílný	k2eAgFnSc1d1	pětidílná
sbírka	sbírka	k1gFnSc1	sbírka
folklóru	folklór	k1gInSc2	folklór
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
romantismem	romantismus	k1gInSc7	romantismus
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidová	lidový	k2eAgFnSc1d1	lidová
slovesnost	slovesnost	k1gFnSc1	slovesnost
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
nedochovaného	dochovaný	k2eNgInSc2d1	nedochovaný
prastarého	prastarý	k2eAgInSc2d1	prastarý
mýtu	mýtus	k1gInSc2	mýtus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
jakousi	jakýsi	k3yIgFnSc4	jakýsi
národní	národní	k2eAgFnSc4d1	národní
povahu	povaha	k1gFnSc4	povaha
(	(	kIx(	(
<g/>
charakter	charakter	k1gInSc4	charakter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mýtus	mýtus	k1gInSc1	mýtus
měl	mít	k5eAaImAgInS	mít
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
ucelený	ucelený	k2eAgInSc4d1	ucelený
soubor	soubor	k1gInSc4	soubor
představ	představa	k1gFnPc2	představa
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
navzájem	navzájem	k6eAd1	navzájem
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
mýtus	mýtus	k1gInSc1	mýtus
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
znovu	znovu	k6eAd1	znovu
sestavit	sestavit	k5eAaPmF	sestavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgInPc1d1	český
pohádkyErben	pohádkyErben	k2eAgMnSc1d1	pohádkyErben
se	se	k3xPyFc4	se
nespokojil	spokojit	k5eNaPmAgMnS	spokojit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
lidovou	lidový	k2eAgFnSc4d1	lidová
tvorbu	tvorba	k1gFnSc4	tvorba
pouze	pouze	k6eAd1	pouze
sbíral	sbírat	k5eAaImAgMnS	sbírat
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
kriticky	kriticky	k6eAd1	kriticky
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
–	–	k?	–
vlastenecké	vlastenecký	k2eAgNnSc4d1	vlastenecké
dílo	dílo	k1gNnSc4	dílo
</s>
</p>
<p>
<s>
Večer	večer	k1gInSc1	večer
–	–	k?	–
vlastenecké	vlastenecký	k2eAgNnSc4d1	vlastenecké
dílo	dílo	k1gNnSc4	dílo
</s>
</p>
<p>
<s>
Tulák	tulák	k1gMnSc1	tulák
</s>
</p>
<p>
<s>
Na	na	k7c6	na
hřbitověK	hřbitověK	k?	hřbitověK
jeho	jeho	k3xOp3gFnPc4	jeho
sběratelské	sběratelský	k2eAgFnPc4d1	sběratelská
činnosti	činnost	k1gFnPc4	činnost
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sbíral	sbírat	k5eAaImAgMnS	sbírat
lidové	lidový	k2eAgFnPc4d1	lidová
písničky	písnička	k1gFnPc4	písnička
<g/>
,	,	kIx,	,
říkadla	říkadlo	k1gNnPc4	říkadlo
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
atd.	atd.	kA	atd.
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
působili	působit	k5eAaImAgMnP	působit
například	například	k6eAd1	například
František	František	k1gMnSc1	František
Sušil	sušit	k5eAaImAgMnS	sušit
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Bartoš	Bartoš	k1gMnSc1	Bartoš
nebo	nebo	k8xC	nebo
Františka	Františka	k1gFnSc1	Františka
Stránecká	Stránecký	k2eAgFnSc1d1	Stránecká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Historická	historický	k2eAgNnPc1d1	historické
díla	dílo	k1gNnPc1	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
musejní	musejní	k2eAgFnSc2d1	musejní
letopisů	letopis	k1gInPc2	letopis
Kosmových	Kosmův	k2eAgFnPc2d1	Kosmova
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Puklice	puklice	k1gFnSc2	puklice
ze	z	k7c2	z
Vstruh	Vstruha	k1gFnPc2	Vstruha
</s>
</p>
<p>
<s>
Příspěvky	příspěvek	k1gInPc1	příspěvek
k	k	k7c3	k
dějepisu	dějepis	k1gInSc3	dějepis
českému	český	k2eAgInSc3d1	český
<g/>
,	,	kIx,	,
sebrané	sebraný	k2eAgInPc1d1	sebraný
ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
letopisů	letopis	k1gInPc2	letopis
ruských	ruský	k2eAgInPc2d1	ruský
</s>
</p>
<p>
<s>
Měsíčník	měsíčník	k1gInSc1	měsíčník
hodin	hodina	k1gFnPc2	hodina
staročeských	staročeský	k2eAgFnPc2d1	staročeská
na	na	k7c6	na
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
</s>
</p>
<p>
<s>
Regesta	Regesta	k1gMnSc1	Regesta
diplomatica	diplomatica	k1gMnSc1	diplomatica
nec	nec	k?	nec
non	non	k?	non
epistolaria	epistolarium	k1gNnSc2	epistolarium
Bohemiae	Bohemiae	k1gFnPc2	Bohemiae
et	et	k?	et
Moraviae	Moraviae	k1gNnSc6	Moraviae
–	–	k?	–
Výtahy	výtah	k1gInPc4	výtah
listin	listina	k1gFnPc2	listina
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
nejstaršími	starý	k2eAgFnPc7d3	nejstarší
českými	český	k2eAgFnPc7d1	Česká
a	a	k8xC	a
moravskými	moravský	k2eAgFnPc7d1	Moravská
listinami	listina	k1gFnPc7	listina
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1253	[number]	k4	1253
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
smrti	smrt	k1gFnSc2	smrt
Václava	Václava	k1gFnSc1	Václava
I.	I.	kA	I.
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jeho	jeho	k3xOp3gFnPc4	jeho
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
práce	práce	k1gFnPc4	práce
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
díly	díl	k1gInPc7	díl
navázal	navázat	k5eAaPmAgMnS	navázat
Josef	Josef	k1gMnSc1	Josef
Emler	Emler	k1gMnSc1	Emler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sebrané	sebraný	k2eAgInPc1d1	sebraný
spisy	spis	k1gInPc1	spis
Jana	Jan	k1gMnSc2	Jan
HusaNapsal	HusaNapsal	k1gFnSc2	HusaNapsal
vědecká	vědecký	k2eAgNnPc4d1	vědecké
pojednání	pojednání	k1gNnPc4	pojednání
k	k	k7c3	k
legendě	legenda	k1gFnSc3	legenda
o	o	k7c6	o
svaté	svatý	k2eAgFnSc6d1	svatá
Kateřině	Kateřina	k1gFnSc6	Kateřina
<g/>
,	,	kIx,	,
Tomáši	Tomáš	k1gMnSc5	Tomáš
Štítném	štítný	k2eAgInSc6d1	štítný
<g/>
,	,	kIx,	,
J.	J.	kA	J.
A.	A.	kA	A.
Komenském	Komenský	k1gMnSc6	Komenský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
starších	starý	k2eAgNnPc2d2	starší
českých	český	k2eAgNnPc2d1	české
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Fričem	Frič	k1gMnSc7	Frič
a	a	k8xC	a
Antonínem	Antonín	k1gMnSc7	Antonín
Strobachem	Strobach	k1gMnSc7	Strobach
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
Soudní	soudní	k2eAgInSc4d1	soudní
a	a	k8xC	a
konkursní	konkursní	k2eAgInSc4d1	konkursní
řád	řád	k1gInSc4	řád
a	a	k8xC	a
Občanský	občanský	k2eAgInSc1d1	občanský
zákoník	zákoník	k1gInSc1	zákoník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Autentický	autentický	k2eAgInSc1d1	autentický
ukazatel	ukazatel	k1gInSc1	ukazatel
===	===	k?	===
</s>
</p>
<p>
<s>
Genealogové	genealog	k1gMnPc1	genealog
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
badatelé	badatel	k1gMnPc1	badatel
používají	používat	k5eAaImIp3nP	používat
Erbenův	Erbenův	k2eAgInSc4d1	Erbenův
Autentický	autentický	k2eAgInSc4d1	autentický
ukazatel	ukazatel	k1gInSc4	ukazatel
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc2	náměstí
i	i	k8xC	i
čísel	číslo	k1gNnPc2	číslo
domovních	domovní	k2eAgMnPc2d1	domovní
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
hl	hl	k?	hl
<g/>
.	.	kIx.	.
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
dle	dle	k7c2	dle
nového	nový	k2eAgMnSc2d1	nový
<g/>
,	,	kIx,	,
starého	starý	k2eAgNnSc2d1	staré
i	i	k8xC	i
nejstaršího	starý	k2eAgNnSc2d3	nejstarší
číslování	číslování	k1gNnSc2	číslování
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
převodník	převodník	k1gInSc4	převodník
pražských	pražský	k2eAgNnPc2d1	Pražské
čísel	číslo	k1gNnPc2	číslo
popisných	popisný	k2eAgNnPc2d1	popisné
z	z	k7c2	z
r.	r.	kA	r.
1805	[number]	k4	1805
na	na	k7c4	na
tehdy	tehdy	k6eAd1	tehdy
nová	nový	k2eAgNnPc4d1	nové
čísla	číslo	k1gNnPc4	číslo
orientační	orientační	k2eAgFnSc1d1	orientační
včetně	včetně	k7c2	včetně
názvů	název	k1gInPc2	název
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Autentický	autentický	k2eAgInSc1d1	autentický
ukazatel	ukazatel	k1gInSc1	ukazatel
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
též	též	k9	též
srovnání	srovnání	k1gNnSc1	srovnání
popisných	popisný	k2eAgNnPc2d1	popisné
čísel	číslo	k1gNnPc2	číslo
z	z	k7c2	z
r.	r.	kA	r.
1770	[number]	k4	1770
a	a	k8xC	a
z	z	k7c2	z
r.	r.	kA	r.
1805	[number]	k4	1805
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
jména	jméno	k1gNnSc2	jméno
majitelů	majitel	k1gMnPc2	majitel
domů	dům	k1gInPc2	dům
a	a	k8xC	a
domovní	domovní	k2eAgNnSc1d1	domovní
znamení	znamení	k1gNnSc1	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čísla	číslo	k1gNnPc1	číslo
domů	domů	k6eAd1	domů
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
pražských	pražský	k2eAgMnPc2d1	pražský
čtvrtí	čtvrtit	k5eAaImIp3nS	čtvrtit
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
,	,	kIx,	,
Josefov	Josefov	k1gInSc1	Josefov
<g/>
)	)	kIx)	)
a	a	k8xC	a
předměstí	předměstí	k1gNnSc4	předměstí
Karlín	Karlín	k1gInSc1	Karlín
a	a	k8xC	a
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k1gNnPc7	další
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
AUGUSTA	Augusta	k1gMnSc1	Augusta
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918.	[number]	k4	1918.
4.	[number]	k4	4.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999.	[number]	k4	1999.
571	[number]	k4	571
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85983-94	[number]	k4	80-85983-94
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
93	[number]	k4	93
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
archivářů	archivář	k1gMnPc2	archivář
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
/	/	kIx~	/
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Hoffmanová	Hoffmanová	k1gFnSc1	Hoffmanová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Pražáková	Pražáková	k1gFnSc1	Pražáková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2000.	[number]	k4	2000.
830	[number]	k4	830
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7277-023-3	[number]	k4	80-7277-023-3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
164	[number]	k4	164
<g/>
–	–	k?	–
<g/>
165	[number]	k4	165
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
16.	[number]	k4	16.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Ep	Ep	k1gMnSc1	Ep
<g/>
–	–	k?	–
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
;	;	kIx,	;
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2013.	[number]	k4	2013.
136	[number]	k4	136
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-200-2292-9	[number]	k4	978-80-200-2292-9
(	(	kIx(	(
<g/>
Academia	academia	k1gFnSc1	academia
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ISBN	ISBN	kA	ISBN
978-80-7286-215-3	[number]	k4	978-80-7286-215-3
(	(	kIx(	(
<g/>
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bohemia	bohemia	k1gFnSc1	bohemia
docta	docto	k1gNnSc2	docto
:	:	kIx,	:
k	k	k7c3	k
historickým	historický	k2eAgInPc3d1	historický
kořenům	kořen	k1gInPc3	kořen
vědy	věda	k1gFnSc2	věda
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
/	/	kIx~	/
Alena	Alena	k1gFnSc1	Alena
Míšková	Míšková	k1gFnSc1	Míšková
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Franc	Franc	k1gMnSc1	Franc
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Kostlán	Kostlán	k2eAgMnSc1d1	Kostlán
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
529	[number]	k4	529
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-200-1809-0	[number]	k4	978-80-200-1809-0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
91	[number]	k4	91
<g/>
–	–	k?	–
<g/>
92	[number]	k4	92
<g/>
,	,	kIx,	,
143.	[number]	k4	143.
</s>
</p>
<p>
<s>
BRANDL	BRANDL	kA	BRANDL
<g/>
,	,	kIx,	,
Vincenc	Vincenc	k1gMnSc1	Vincenc
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
Moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
19.	[number]	k4	19.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
/	/	kIx~	/
redakce	redakce	k1gFnSc2	redakce
Otakar	Otakar	k1gMnSc1	Otakar
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
84	[number]	k4	84
<g/>
–	–	k?	–
<g/>
88	[number]	k4	88
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
2.	[number]	k4	2.
<g/>
,	,	kIx,	,
Literatura	literatura	k1gFnSc1	literatura
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
/	/	kIx~	/
Redaktor	redaktor	k1gMnSc1	redaktor
svazku	svazek	k1gInSc2	svazek
Felix	Felix	k1gMnSc1	Felix
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1960.	[number]	k4	1960.
684	[number]	k4	684
s	s	k7c7	s
<g/>
.	.	kIx.	.
S.	S.	kA	S.
542	[number]	k4	542
<g/>
–	–	k?	–
<g/>
566	[number]	k4	566
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Otomar	Otomar	k1gMnSc1	Otomar
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Vřeteno	vřeteno	k1gNnSc1	vřeteno
osudu	osud	k1gInSc2	osud
(	(	kIx(	(
<g/>
životopisný	životopisný	k2eAgInSc1d1	životopisný
román	román	k1gInSc1	román
o	o	k7c4	o
K.	K.	kA	K.
J.	J.	kA	J.
Erbenovi	Erben	k1gMnSc3	Erben
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
Řitka	Řitka	k1gFnSc1	Řitka
2015	[number]	k4	2015
</s>
</p>
<p>
<s>
FORST	FORST	kA	FORST
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
<g/>
–	–	k?	–
<g/>
G.	G.	kA	G.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1985.	[number]	k4	1985.
900	[number]	k4	900
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-200-0797-0	[number]	k4	80-200-0797-0
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
667	[number]	k4	667
<g/>
–	–	k?	–
<g/>
674	[number]	k4	674
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUDĚLKA	KUDĚLKA	kA	KUDĚLKA
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
;	;	kIx,	;
ŠIMEČEK	Šimeček	k1gMnSc1	Šimeček
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
VEČERKA	Večerka	k1gMnSc1	Večerka
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
slavistika	slavistika	k1gFnSc1	slavistika
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
období	období	k1gNnSc6	období
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
19.	[number]	k4	19.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1995.	[number]	k4	1995.
393	[number]	k4	393
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85268-41-8	[number]	k4	80-85268-41-8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
342	[number]	k4	342
<g/>
–	–	k?	–
<g/>
343	[number]	k4	343
<g/>
,	,	kIx,	,
351.	[number]	k4	351.
</s>
</p>
<p>
<s>
KUDĚLKA	KUDĚLKA	kA	KUDĚLKA
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
;	;	kIx,	;
ŠIMEČEK	Šimeček	k1gMnSc1	Šimeček
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
VEČERKA	Večerka	k1gMnSc1	Večerka
<g/>
,	,	kIx,	,
Radoslav	Radoslav	k1gMnSc1	Radoslav
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
slavistika	slavistika	k1gFnSc1	slavistika
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918.	[number]	k4	1918.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
1997.	[number]	k4	1997.
477	[number]	k4	477
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85268-69-8	[number]	k4	80-85268-69-8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
295	[number]	k4	295
<g/>
–	–	k?	–
<g/>
296	[number]	k4	296
<g/>
,	,	kIx,	,
339.	[number]	k4	339.
</s>
</p>
<p>
<s>
KUDĚLKA	KUDĚLKA	kA	KUDĚLKA
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
;	;	kIx,	;
ŠIMEČEK	Šimeček	k1gMnSc1	Šimeček
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Československé	československý	k2eAgFnPc1d1	Československá
práce	práce	k1gFnPc1	práce
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
dějinách	dějiny	k1gFnPc6	dějiny
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
od	od	k7c2	od
r.	r.	kA	r.
1760	[number]	k4	1760
:	:	kIx,	:
biograficko-bibliografický	biografickoibliografický	k2eAgInSc1d1	biograficko-bibliografický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1972.	[number]	k4	1972.
560	[number]	k4	560
s	s	k7c7	s
<g/>
.	.	kIx.	.
S.	S.	kA	S.
109	[number]	k4	109
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTNAR	KUTNAR	kA	KUTNAR
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
;	;	kIx,	;
MAREK	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Přehledné	přehledný	k2eAgFnPc1d1	přehledná
dějiny	dějiny	k1gFnPc1	dějiny
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
slovenského	slovenský	k2eAgNnSc2d1	slovenské
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
:	:	kIx,	:
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
národní	národní	k2eAgFnSc2d1	národní
kultury	kultura	k1gFnSc2	kultura
až	až	k9	až
do	do	k7c2	do
sklonku	sklonek	k1gInSc2	sklonek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
2.	[number]	k4	2.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1997.	[number]	k4	1997.
1065	[number]	k4	1065
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7106-252-9	[number]	k4	80-7106-252-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
:	:	kIx,	:
národopisná	národopisný	k2eAgFnSc1d1	národopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
sv.	sv.	kA	sv.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2007.	[number]	k4	2007.
284	[number]	k4	284
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-204-1711-4	[number]	k4	978-80-204-1711-4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
49	[number]	k4	49
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠTECH	ŠTECH	kA	ŠTECH
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zamlženém	zamlžený	k2eAgNnSc6d1	zamlžené
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
80.	[number]	k4	80.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Borovice	borovice	k1gFnSc1	borovice
Erbenka	Erbenka	k1gFnSc1	Erbenka
</s>
</p>
<p>
<s>
Erbenův	Erbenův	k2eAgInSc1d1	Erbenův
dub	dub	k1gInSc1	dub
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
v	v	k7c6	v
souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
</s>
</p>
<p>
<s>
Výstava	výstava	k1gFnSc1	výstava
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
o	o	k7c6	o
Erbenově	Erbenův	k2eAgInSc6d1	Erbenův
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	dílo	k1gNnSc6	dílo
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
<g/>
:	:	kIx,	:
Protichůdci	protichůdce	k1gMnPc1	protichůdce
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
poznámek	poznámka	k1gFnPc2	poznámka
o	o	k7c6	o
vztahu	vztah	k1gInSc6	vztah
Erbenova	Erbenův	k2eAgNnSc2d1	Erbenovo
básnického	básnický	k2eAgNnSc2d1	básnické
díla	dílo	k1gNnSc2	dílo
k	k	k7c3	k
Máchovu	Máchův	k2eAgInSc3d1	Máchův
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Databazeknih.cz	Databazeknih.cza	k1gFnPc2	Databazeknih.cza
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
na	na	k7c4	na
Kinoboxu.cz	Kinoboxu.cz	k1gInSc4	Kinoboxu.cz
</s>
</p>
<p>
<s>
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Jaromír	Jaromír	k1gMnSc1	Jaromír
Erben	Erben	k1gMnSc1	Erben
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
</s>
</p>
