<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
stříbrobílé	stříbrobílý	k2eAgFnSc2d1	stříbrobílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
díky	díky	k7c3	díky
oxidaci	oxidace	k1gFnSc3	oxidace
po	po	k7c6	po
čase	čas	k1gInSc6	čas
přechází	přecházet	k5eAaImIp3nS	přecházet
k	k	k7c3	k
šedé	šedý	k2eAgFnSc3d1	šedá
barvě	barva	k1gFnSc3	barva
<g/>
.	.	kIx.	.
</s>
