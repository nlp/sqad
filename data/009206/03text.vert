<p>
<s>
Jak	jak	k6eAd1	jak
divoký	divoký	k2eAgInSc1d1	divoký
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
mutus	mutus	k1gInSc1	mutus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
pravých	pravý	k2eAgMnPc2d1	pravý
turů	tur	k1gMnPc2	tur
adaptovaný	adaptovaný	k2eAgInSc4d1	adaptovaný
pro	pro	k7c4	pro
život	život	k1gInSc4	život
ve	v	k7c6	v
vysokohorských	vysokohorský	k2eAgFnPc6d1	vysokohorská
oblastech	oblast	k1gFnPc6	oblast
centrální	centrální	k2eAgFnSc2d1	centrální
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
vyhubením	vyhubení	k1gNnSc7	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
Domestikovanou	domestikovaný	k2eAgFnSc7d1	domestikovaná
formou	forma	k1gFnSc7	forma
divokého	divoký	k2eAgMnSc4d1	divoký
jaka	jak	k1gMnSc4	jak
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
domácí	domácí	k2eAgMnSc1d1	domácí
(	(	kIx(	(
<g/>
Bos	bos	k2eAgInSc1d1	bos
grunniens	grunniens	k1gInSc1	grunniens
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pojmenování	pojmenování	k1gNnPc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
jak	jak	k6eAd1	jak
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tibetského	tibetský	k2eAgInSc2d1	tibetský
výrazu	výraz	k1gInSc2	výraz
gyag	gyaga	k1gFnPc2	gyaga
(	(	kIx(	(
<g/>
ག	ག	k?	ག
<g/>
་	་	k?	་
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
jačí	jačí	k1gMnSc1	jačí
býk	býk	k1gMnSc1	býk
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
z	z	k7c2	z
tibetšiny	tibetšina	k1gFnSc2	tibetšina
bylo	být	k5eAaImAgNnS	být
přejato	přejmout	k5eAaPmNgNnS	přejmout
do	do	k7c2	do
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Domorodá	domorodý	k2eAgNnPc1d1	domorodé
jména	jméno	k1gNnPc1	jméno
jaka	jak	k1gMnSc2	jak
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
centrální	centrální	k2eAgFnSc2d1	centrální
Asie	Asie	k1gFnSc2	Asie
jsou	být	k5eAaImIp3nP	být
rozmanitá	rozmanitý	k2eAgNnPc4d1	rozmanité
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
tibetsky	tibetsky	k6eAd1	tibetsky
<g/>
:	:	kIx,	:
ག	ག	k?	ག
<g/>
་	་	k?	་
gyag	gyag	k1gMnSc1	gyag
(	(	kIx(	(
<g/>
býk	býk	k1gMnSc1	býk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
འ	འ	k?	འ
<g/>
ྲ	ྲ	k?	ྲ
<g/>
ི	ི	k?	ི
<g/>
་	་	k?	་
dri	dri	k?	dri
</s>
</p>
<p>
<s>
nepálsky	nepálsky	k6eAd1	nepálsky
<g/>
:	:	kIx,	:
ják	ják	k?	ják
</s>
</p>
<p>
<s>
tádžicky	tádžicky	k6eAd1	tádžicky
<g/>
:	:	kIx,	:
kutos	kutos	k1gInSc1	kutos
</s>
</p>
<p>
<s>
mongolsky	mongolsky	k6eAd1	mongolsky
<g/>
:	:	kIx,	:
sarlyk	sarlyk	k6eAd1	sarlyk
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
divoký	divoký	k2eAgMnSc1d1	divoký
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejmohutnějším	mohutný	k2eAgMnPc3d3	nejmohutnější
turům	tur	k1gMnPc3	tur
<g/>
,	,	kIx,	,
samec	samec	k1gInSc1	samec
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
kohoutkové	kohoutkový	k2eAgFnSc2d1	kohoutková
výšky	výška	k1gFnSc2	výška
až	až	k9	až
210	[number]	k4	210
cm	cm	kA	cm
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hmotnosti	hmotnost	k1gFnSc3	hmotnost
kolem	kolem	k7c2	kolem
jedné	jeden	k4xCgFnSc2	jeden
tuny	tuna	k1gFnSc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Kráva	kráva	k1gFnSc1	kráva
je	být	k5eAaImIp3nS	být
daleko	daleko	k6eAd1	daleko
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
býk	býk	k1gMnSc1	býk
a	a	k8xC	a
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
hmotnosti	hmotnost	k1gFnSc2	hmotnost
nejvýš	vysoce	k6eAd3	vysoce
500	[number]	k4	500
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
mimořádně	mimořádně	k6eAd1	mimořádně
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
trupem	trup	k1gInSc7	trup
<g/>
.	.	kIx.	.
</s>
<s>
Hrudník	hrudník	k1gInSc1	hrudník
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
čtrnácti	čtrnáct	k4xCc7	čtrnáct
páry	pár	k1gInPc1	pár
žeber	žebro	k1gNnPc2	žebro
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
třinácti	třináct	k4xCc2	třináct
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
turů	tur	k1gMnPc2	tur
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
ozdobena	ozdobit	k5eAaPmNgFnS	ozdobit
mohutnými	mohutný	k2eAgInPc7d1	mohutný
rohy	roh	k1gInPc7	roh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
rohy	roh	k1gInPc4	roh
býka	býk	k1gMnSc2	býk
obvod	obvod	k1gInSc1	obvod
až	až	k6eAd1	až
50	[number]	k4	50
cm	cm	kA	cm
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc1	kráva
divokých	divoký	k2eAgMnPc2d1	divoký
jaků	jak	k1gMnPc2	jak
a	a	k8xC	a
jaci	jak	k1gMnPc1	jak
domácí	domácí	k1gFnSc2	domácí
mají	mít	k5eAaImIp3nP	mít
rohy	roh	k1gInPc4	roh
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
kratší	krátký	k2eAgNnSc4d2	kratší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
daleko	daleko	k6eAd1	daleko
slabší	slabý	k2eAgMnPc1d2	slabší
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
hnědočerná	hnědočerný	k2eAgFnSc1d1	hnědočerná
až	až	k6eAd1	až
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
hustá	hustý	k2eAgFnSc1d1	hustá
<g/>
,	,	kIx,	,
vlnitá	vlnitý	k2eAgFnSc1d1	vlnitá
a	a	k8xC	a
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
a	a	k8xC	a
končetinách	končetina	k1gFnPc6	končetina
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
hrudi	hruď	k1gFnSc6	hruď
splývá	splývat	k5eAaImIp3nS	splývat
srst	srst	k1gFnSc4	srst
do	do	k7c2	do
výrazné	výrazný	k2eAgFnSc2d1	výrazná
hřívy	hříva	k1gFnSc2	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Neobyčejně	obyčejně	k6eNd1	obyčejně
bohatě	bohatě	k6eAd1	bohatě
osrstěný	osrstěný	k2eAgInSc1d1	osrstěný
je	být	k5eAaImIp3nS	být
také	také	k9	také
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
oháňky	oháňka	k1gFnSc2	oháňka
proti	proti	k7c3	proti
hmyzu	hmyz	k1gInSc3	hmyz
(	(	kIx(	(
<g/>
čámara	čámara	k1gFnSc1	čámara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
Číně	Čína	k1gFnSc6	Čína
ozdoby	ozdoba	k1gFnSc2	ozdoba
úřednických	úřednický	k2eAgInPc2d1	úřednický
klobouků	klobouk	k1gInPc2	klobouk
a	a	k8xC	a
v	v	k7c6	v
Mongolské	mongolský	k2eAgFnSc6d1	mongolská
či	či	k8xC	či
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
vojenské	vojenský	k2eAgInPc4d1	vojenský
odznaky	odznak	k1gInPc4	odznak
zvané	zvaný	k2eAgFnSc2d1	zvaná
tugy	tuga	k1gFnSc2	tuga
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
zvané	zvaný	k2eAgInPc1d1	zvaný
koňské	koňský	k2eAgInPc1d1	koňský
ohony	ohon	k1gInPc1	ohon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Divocí	divoký	k2eAgMnPc1d1	divoký
jakové	jak	k1gMnPc1	jak
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Bhútán	Bhútán	k1gInSc1	Bhútán
a	a	k8xC	a
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
svým	svůj	k3xOyFgInSc7	svůj
výskytem	výskyt	k1gInSc7	výskyt
až	až	k9	až
do	do	k7c2	do
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
bezlesé	bezlesý	k2eAgFnPc1d1	bezlesá
vrchoviny	vrchovina	k1gFnPc1	vrchovina
<g/>
,	,	kIx,	,
kopce	kopec	k1gInPc1	kopec
<g/>
,	,	kIx,	,
náhorní	náhorní	k2eAgFnPc1d1	náhorní
plošiny	plošina	k1gFnPc1	plošina
a	a	k8xC	a
hory	hora	k1gFnPc1	hora
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
3200	[number]	k4	3200
m	m	kA	m
až	až	k9	až
5400	[number]	k4	5400
m.	m.	k?	m.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Divocí	divoký	k2eAgMnPc1d1	divoký
jaci	jak	k1gMnPc1	jak
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
plachá	plachý	k2eAgNnPc1d1	plaché
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
stádech	stádo	k1gNnPc6	stádo
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
10	[number]	k4	10
až	až	k9	až
30	[number]	k4	30
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
býci	býk	k1gMnPc1	býk
jsou	být	k5eAaImIp3nP	být
samotáři	samotář	k1gMnPc1	samotář
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
především	především	k9	především
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
čich	čich	k1gInSc4	čich
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
zrak	zrak	k1gInSc1	zrak
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
slabou	slabý	k2eAgFnSc7d1	slabá
stránkou	stránka	k1gFnSc7	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
býci	býk	k1gMnPc1	býk
většinou	většinou	k6eAd1	většinou
až	až	k6eAd1	až
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc1	kráva
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
257	[number]	k4	257
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
u	u	k7c2	u
domácích	domácí	k2eAgMnPc2d1	domácí
jaků	jak	k1gMnPc2	jak
je	být	k5eAaImIp3nS	být
březost	březost	k1gFnSc1	březost
poněkud	poněkud	k6eAd1	poněkud
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
Tele	tele	k1gNnSc1	tele
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
od	od	k7c2	od
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
věku	věk	k1gInSc2	věk
začíná	začínat	k5eAaImIp3nS	začínat
přijímat	přijímat	k5eAaImF	přijímat
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Jaci	jak	k1gMnPc1	jak
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
hlavně	hlavně	k9	hlavně
trávou	tráva	k1gFnSc7	tráva
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
lukách	luka	k1gNnPc6	luka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žerou	žrát	k5eAaImIp3nP	žrát
také	také	k9	také
kosodřevinu	kosodřevina	k1gFnSc4	kosodřevina
a	a	k8xC	a
lišejníky	lišejník	k1gInPc4	lišejník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Yak	Yak	k1gFnSc2	Yak
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jak	jak	k6eAd1	jak
divoký	divoký	k2eAgMnSc1d1	divoký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jak	jak	k6eAd1	jak
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
Yak	Yak	k1gMnPc1	Yak
Research	Researcha	k1gFnPc2	Researcha
Center	centrum	k1gNnPc2	centrum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
Yak	Yak	k1gFnSc1	Yak
Information	Information	k1gInSc1	Information
Center	centrum	k1gNnPc2	centrum
</s>
</p>
