<s>
Plynofikace	plynofikace	k1gFnSc1
</s>
<s>
Plynofikace	plynofikace	k1gFnSc1
je	být	k5eAaImIp3nS
plošné	plošný	k2eAgNnSc1d1
zavádění	zavádění	k1gNnSc1
infrastruktury	infrastruktura	k1gFnSc2
pro	pro	k7c4
distribuci	distribuce	k1gFnSc4
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
v	v	k7c6
aglomeracích	aglomerace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
ČR	ČR	kA
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
pojem	pojem	k1gInSc1
ujal	ujmout	k5eAaPmAgInS
především	především	k6eAd1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
zlepšování	zlepšování	k1gNnSc4
kvality	kvalita	k1gFnSc2
ovzduší	ovzduší	k1gNnSc2
se	se	k3xPyFc4
podporoval	podporovat	k5eAaImAgInS
přechod	přechod	k1gInSc1
na	na	k7c4
ekologičtější	ekologický	k2eAgInSc4d2
způsob	způsob	k1gInSc4
topení	topení	k1gNnSc2
od	od	k7c2
hnědého	hnědý	k2eAgNnSc2d1
uhlí	uhlí	k1gNnSc2
k	k	k7c3
zemnímu	zemní	k2eAgInSc3d1
plynu	plyn	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městech	město	k1gNnPc6
se	se	k3xPyFc4
plošně	plošně	k6eAd1
budovaly	budovat	k5eAaImAgInP
rozvody	rozvod	k1gInPc4
a	a	k8xC
přípojky	přípojka	k1gFnPc4
plynového	plynový	k2eAgNnSc2d1
potrubí	potrubí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
