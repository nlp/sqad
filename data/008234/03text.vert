<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
korporace	korporace	k1gFnSc2	korporace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
základní	základní	k2eAgInSc1d1	základní
kapitál	kapitál	k1gInSc1	kapitál
se	se	k3xPyFc4	se
neskládá	skládat	k5eNaImIp3nS	skládat
z	z	k7c2	z
(	(	kIx(	(
<g/>
nehmotných	hmotný	k2eNgFnPc2d1	nehmotná
<g/>
,	,	kIx,	,
abstraktních	abstraktní	k2eAgFnPc2d1	abstraktní
<g/>
)	)	kIx)	)
podílů	podíl	k1gInPc2	podíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
cenné	cenný	k2eAgInPc1d1	cenný
papíry	papír	k1gInPc1	papír
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
zaknihované	zaknihovaný	k2eAgInPc1d1	zaknihovaný
cenné	cenný	k2eAgInPc1d1	cenný
papíry	papír	k1gInPc1	papír
<g/>
)	)	kIx)	)
tento	tento	k3xDgInSc1	tento
podíl	podíl	k1gInSc1	podíl
představující	představující	k2eAgInSc1d1	představující
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníci	vlastník	k1gMnPc1	vlastník
akcií	akcie	k1gFnPc2	akcie
(	(	kIx(	(
<g/>
akcionáři	akcionář	k1gMnPc1	akcionář
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
(	(	kIx(	(
<g/>
dividendu	dividenda	k1gFnSc4	dividenda
<g/>
)	)	kIx)	)
a	a	k8xC	a
související	související	k2eAgNnPc4d1	související
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
jsou	být	k5eAaImIp3nP	být
soukromoprávní	soukromoprávní	k2eAgFnPc1d1	soukromoprávní
obchodní	obchodní	k2eAgFnPc1d1	obchodní
korporace	korporace	k1gFnPc1	korporace
s	s	k7c7	s
volnou	volný	k2eAgFnSc7d1	volná
převoditelností	převoditelnost	k1gFnSc7	převoditelnost
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějinný	dějinný	k2eAgInSc4d1	dějinný
vývoj	vývoj	k1gInSc4	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Forma	forma	k1gFnSc1	forma
sdružování	sdružování	k1gNnSc2	sdružování
majetků	majetek	k1gInPc2	majetek
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
podílníci	podílník	k1gMnPc1	podílník
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
výnosy	výnos	k1gInPc4	výnos
úměrně	úměrně	k6eAd1	úměrně
k	k	k7c3	k
výši	výše	k1gFnSc3	výše
svých	svůj	k3xOyFgInPc2	svůj
podílů	podíl	k1gInPc2	podíl
a	a	k8xC	a
ručí	ručit	k5eAaImIp3nS	ručit
za	za	k7c4	za
výsledky	výsledek	k1gInPc4	výsledek
jen	jen	k9	jen
těmito	tento	k3xDgInPc7	tento
podíly	podíl	k1gInPc7	podíl
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
už	už	k6eAd1	už
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
společností	společnost	k1gFnPc2	společnost
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
středověkých	středověký	k2eAgNnPc6d1	středověké
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1602	[number]	k4	1602
založená	založený	k2eAgFnSc1d1	založená
Nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
Vereenigte	Vereenigt	k1gMnSc5	Vereenigt
Oost-Indische	Oost-Indischus	k1gMnSc5	Oost-Indischus
Compagnie	Compagnie	k1gFnPc1	Compagnie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
monopol	monopol	k1gInSc1	monopol
na	na	k7c4	na
veškerý	veškerý	k3xTgInSc4	veškerý
nizozemský	nizozemský	k2eAgInSc4d1	nizozemský
obchod	obchod	k1gInSc4	obchod
na	na	k7c4	na
východ	východ	k1gInSc4	východ
i	i	k8xC	i
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1612	[number]	k4	1612
chtěli	chtít	k5eAaImAgMnP	chtít
investoři	investor	k1gMnPc1	investor
zpět	zpět	k6eAd1	zpět
svou	svůj	k3xOyFgFnSc4	svůj
hotovost	hotovost	k1gFnSc4	hotovost
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
prodat	prodat	k5eAaPmF	prodat
své	svůj	k3xOyFgFnPc4	svůj
akcie	akcie	k1gFnPc4	akcie
jinému	jiný	k1gMnSc3	jiný
investorovi	investor	k1gMnSc3	investor
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
akciový	akciový	k2eAgInSc4d1	akciový
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc7	první
akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
K.K.	K.K.	k1gFnSc1	K.K.
Privilegirte	Privilegirt	k1gInSc5	Privilegirt
Zucker-Raffinerie	Zucker-Raffinerie	k1gFnPc4	Zucker-Raffinerie
zu	zu	k?	zu
Königsaal	Königsaal	k1gInSc1	Königsaal
bey	bey	k?	bey
Prag	Praga	k1gFnPc2	Praga
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
zní	znět	k5eAaImIp3nS	znět
C.	C.	kA	C.
k.	k.	k?	k.
privilegovaná	privilegovaný	k2eAgFnSc1d1	privilegovaná
rafinerie	rafinerie	k1gFnSc1	rafinerie
cukru	cukr	k1gInSc2	cukr
ve	v	k7c6	v
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pokusy	pokus	k1gInPc1	pokus
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Schwimmanstalt	Schwimmanstalt	k1gInSc1	Schwimmanstalt
in	in	k?	in
Prag	Prag	k1gInSc1	Prag
auf	auf	k?	auf
der	drát	k5eAaImRp2nS	drát
Moldau	Moldaa	k1gMnSc4	Moldaa
(	(	kIx(	(
<g/>
Plavecký	plavecký	k2eAgInSc4d1	plavecký
ústav	ústav	k1gInSc4	ústav
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Vltavě	Vltava	k1gFnSc6	Vltava
<g/>
)	)	kIx)	)
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
nebo	nebo	k8xC	nebo
Pražská	pražský	k2eAgFnSc1d1	Pražská
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
paroplavební	paroplavební	k2eAgFnSc1d1	paroplavební
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
upravil	upravit	k5eAaPmAgMnS	upravit
právní	právní	k2eAgNnSc4d1	právní
postavení	postavení	k1gNnSc4	postavení
akciových	akciový	k2eAgFnPc2d1	akciová
společností	společnost	k1gFnPc2	společnost
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
spolcích	spolek	k1gInPc6	spolek
(	(	kIx(	(
<g/>
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgInSc4	jenž
navázalo	navázat	k5eAaPmAgNnS	navázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
ministerské	ministerský	k2eAgNnSc4d1	ministerské
nařízení	nařízení	k1gNnSc4	nařízení
nazvané	nazvaný	k2eAgNnSc4d1	nazvané
Akciový	akciový	k2eAgInSc4d1	akciový
regulativ	regulativ	k1gInSc4	regulativ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
upravoval	upravovat	k5eAaImAgMnS	upravovat
akciové	akciový	k2eAgFnSc3d1	akciová
společnosti	společnost	k1gFnSc3	společnost
velmi	velmi	k6eAd1	velmi
kusý	kusý	k2eAgInSc1d1	kusý
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
243	[number]	k4	243
<g/>
/	/	kIx~	/
<g/>
1949	[number]	k4	1949
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
politického	politický	k2eAgInSc2d1	politický
režimu	režim	k1gInSc2	režim
nahrazen	nahradit	k5eAaPmNgInS	nahradit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
104	[number]	k4	104
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Poté	poté	k6eAd1	poté
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
upravoval	upravovat	k5eAaImAgInS	upravovat
obchodní	obchodní	k2eAgInSc1d1	obchodní
zákoník	zákoník	k1gInSc1	zákoník
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zákonu	zákon	k1gInSc6	zákon
o	o	k7c6	o
obchodních	obchodní	k2eAgFnPc6d1	obchodní
korporacích	korporace	k1gFnPc6	korporace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgNnSc1d1	české
právo	právo	k1gNnSc1	právo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Charakter	charakter	k1gInSc1	charakter
společnosti	společnost	k1gFnSc2	společnost
===	===	k?	===
</s>
</p>
<p>
<s>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
je	být	k5eAaImIp3nS	být
právnická	právnický	k2eAgFnSc1d1	právnická
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
statutárním	statutární	k2eAgMnSc7d1	statutární
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
volené	volený	k2eAgNnSc1d1	volené
představenstvo	představenstvo	k1gNnSc1	představenstvo
(	(	kIx(	(
<g/>
či	či	k8xC	či
statutární	statutární	k2eAgMnSc1d1	statutární
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Majetek	majetek	k1gInSc1	majetek
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vydaných	vydaný	k2eAgFnPc2d1	vydaná
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
nominální	nominální	k2eAgFnSc7d1	nominální
či	či	k8xC	či
emisní	emisní	k2eAgFnSc7d1	emisní
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
;	;	kIx,	;
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
s	s	k7c7	s
akciemi	akcie	k1gFnPc7	akcie
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
tržní	tržní	k2eAgFnSc1d1	tržní
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
poptávky	poptávka	k1gFnSc2	poptávka
a	a	k8xC	a
nabídky	nabídka	k1gFnSc2	nabídka
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
mění	měnit	k5eAaImIp3nS	měnit
cena	cena	k1gFnSc1	cena
těchto	tento	k3xDgFnPc2	tento
akcií	akcie	k1gFnPc2	akcie
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
i	i	k9	i
hodnota	hodnota	k1gFnSc1	hodnota
pro	pro	k7c4	pro
vlastníky	vlastník	k1gMnPc4	vlastník
celého	celý	k2eAgInSc2d1	celý
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
i	i	k9	i
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
značně	značně	k6eAd1	značně
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
za	za	k7c4	za
svoje	svůj	k3xOyFgInPc4	svůj
závazky	závazek	k1gInPc4	závazek
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
celým	celý	k2eAgInSc7d1	celý
svým	svůj	k3xOyFgInSc7	svůj
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
akcionář	akcionář	k1gMnSc1	akcionář
za	za	k7c7	za
závazky	závazek	k1gInPc7	závazek
společnosti	společnost	k1gFnSc2	společnost
neručí	ručit	k5eNaImIp3nP	ručit
a	a	k8xC	a
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
uhradit	uhradit	k5eAaPmF	uhradit
ztrátu	ztráta	k1gFnSc4	ztráta
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
už	už	k6eAd1	už
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
nemusí	muset	k5eNaImIp3nS	muset
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
ztráty	ztráta	k1gFnSc2	ztráta
vytvářet	vytvářet	k5eAaImF	vytvářet
povinný	povinný	k2eAgInSc4d1	povinný
rezervní	rezervní	k2eAgInSc4d1	rezervní
fond	fond	k1gInSc4	fond
<g/>
.	.	kIx.	.
</s>
<s>
Rezervní	rezervní	k2eAgInSc1d1	rezervní
fond	fond	k1gInSc1	fond
však	však	k9	však
zřídit	zřídit	k5eAaPmF	zřídit
může	moct	k5eAaImIp3nS	moct
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
uvedenou	uvedený	k2eAgFnSc7d1	uvedená
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
firmě	firma	k1gFnSc6	firma
zkratku	zkratka	k1gFnSc4	zkratka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
společnosti	společnost	k1gFnPc1	společnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
akc	akc	k?	akc
<g/>
.	.	kIx.	.
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
společnosti	společnost	k1gFnSc2	společnost
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
utváření	utváření	k1gNnSc6	utváření
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
zásadní	zásadní	k2eAgInPc1d1	zásadní
dva	dva	k4xCgInPc1	dva
momenty	moment	k1gInPc1	moment
–	–	k?	–
založení	založení	k1gNnSc4	založení
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
založena	založit	k5eAaPmNgFnS	založit
jedním	jeden	k4xCgInSc7	jeden
zakladatelem	zakladatel	k1gMnSc7	zakladatel
či	či	k8xC	či
více	hodně	k6eAd2	hodně
zakladateli	zakladatel	k1gMnPc7	zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
založení	založení	k1gNnSc3	založení
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
postačuje	postačovat	k5eAaImIp3nS	postačovat
přijetí	přijetí	k1gNnSc1	přijetí
stanov	stanova	k1gFnPc2	stanova
zakladateli	zakladatel	k1gMnSc6	zakladatel
<g/>
.	.	kIx.	.
</s>
<s>
Stanovy	stanova	k1gFnPc1	stanova
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
povahu	povaha	k1gFnSc4	povaha
veřejné	veřejný	k2eAgFnSc2d1	veřejná
listiny	listina	k1gFnSc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
účinné	účinný	k2eAgNnSc1d1	účinné
<g/>
,	,	kIx,	,
splatil	splatit	k5eAaPmAgMnS	splatit
<g/>
-li	i	k?	-li
každý	každý	k3xTgMnSc1	každý
zakladatel	zakladatel	k1gMnSc1	zakladatel
případné	případný	k2eAgNnSc4d1	případné
emisní	emisní	k2eAgNnSc4d1	emisní
ážio	ážio	k1gNnSc4	ážio
a	a	k8xC	a
alespoň	alespoň	k9	alespoň
30	[number]	k4	30
%	%	kIx~	%
hodnoty	hodnota	k1gFnPc1	hodnota
upsaných	upsaný	k2eAgFnPc2d1	upsaná
akcií	akcie	k1gFnPc2	akcie
v	v	k7c6	v
době	doba	k1gFnSc6	doba
určené	určený	k2eAgFnSc2d1	určená
ve	v	k7c6	v
stanovách	stanova	k1gFnPc6	stanova
<g/>
,	,	kIx,	,
nejpozději	pozdě	k6eAd3	pozdě
však	však	k9	však
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
podání	podání	k1gNnSc2	podání
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
zápis	zápis	k1gInSc4	zápis
společnosti	společnost	k1gFnSc2	společnost
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
výše	výše	k1gFnSc1	výše
základního	základní	k2eAgInSc2d1	základní
kapitálu	kapitál	k1gInSc2	kapitál
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
000	[number]	k4	000
000	[number]	k4	000
Kč	Kč	kA	Kč
nebo	nebo	k8xC	nebo
80	[number]	k4	80
000	[number]	k4	000
EUR	euro	k1gNnPc2	euro
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
<g/>
-	-	kIx~	-
<g/>
li	li	k8xS	li
společnost	společnost	k1gFnSc1	společnost
účetnictví	účetnictví	k1gNnSc2	účetnictví
v	v	k7c6	v
EUR	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
<g/>
Akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
vzniká	vznikat	k5eAaImIp3nS	vznikat
dnem	den	k1gInSc7	den
zápisu	zápis	k1gInSc2	zápis
do	do	k7c2	do
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Orgány	orgán	k1gInPc1	orgán
společnosti	společnost	k1gFnSc2	společnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
právu	právo	k1gNnSc6	právo
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgMnSc6d1	tradiční
dualistický	dualistický	k2eAgInSc1d1	dualistický
systém	systém	k1gInSc4	systém
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
struktury	struktura	k1gFnSc2	struktura
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
evropský	evropský	k2eAgMnSc1d1	evropský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
<g/>
,	,	kIx,	,
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
představenstvo	představenstvo	k1gNnSc1	představenstvo
jako	jako	k8xC	jako
statutární	statutární	k2eAgInSc1d1	statutární
orgán	orgán	k1gInSc1	orgán
a	a	k8xC	a
dozorčí	dozorčí	k2eAgFnSc1d1	dozorčí
rada	rada	k1gFnSc1	rada
jako	jako	k9	jako
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
obchodních	obchodní	k2eAgFnPc6d1	obchodní
korporacích	korporace	k1gFnPc6	korporace
ale	ale	k8xC	ale
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zvolit	zvolit	k5eAaPmF	zvolit
také	také	k9	také
systém	systém	k1gInSc1	systém
monistický	monistický	k2eAgInSc1d1	monistický
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c4	za
společnost	společnost	k1gFnSc4	společnost
jedná	jednat	k5eAaImIp3nS	jednat
statutární	statutární	k2eAgMnSc1d1	statutární
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
doplňován	doplňovat	k5eAaImNgInS	doplňovat
správní	správní	k2eAgFnSc7d1	správní
radou	rada	k1gFnSc7	rada
jako	jako	k8xC	jako
orgánem	orgán	k1gInSc7	orgán
kontrolně-řídícím	kontrolně-řídící	k2eAgInSc7d1	kontrolně-řídící
(	(	kIx(	(
<g/>
anglosaský	anglosaský	k2eAgMnSc1d1	anglosaský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
angloamerický	angloamerický	k2eAgMnSc1d1	angloamerický
<g/>
,	,	kIx,	,
model	model	k1gInSc1	model
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Dualistický	dualistický	k2eAgInSc1d1	dualistický
systém	systém	k1gInSc1	systém
====	====	k?	====
</s>
</p>
<p>
<s>
Valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
–	–	k?	–
je	být	k5eAaImIp3nS	být
shromáždění	shromáždění	k1gNnSc1	shromáždění
všech	všecek	k3xTgMnPc2	všecek
akcionářů	akcionář	k1gMnPc2	akcionář
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
vlastníků	vlastník	k1gMnPc2	vlastník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
orgán	orgán	k1gInSc1	orgán
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ustavující	ustavující	k2eAgFnSc1d1	ustavující
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
pak	pak	k6eAd1	pak
například	například	k6eAd1	například
o	o	k7c6	o
změnách	změna	k1gFnPc6	změna
stanov	stanova	k1gFnPc2	stanova
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
orgány	orgán	k1gInPc4	orgán
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
schvalují	schvalovat	k5eAaImIp3nP	schvalovat
rozdělení	rozdělení	k1gNnSc4	rozdělení
zisku	zisk	k1gInSc2	zisk
a	a	k8xC	a
účetní	účetní	k2eAgFnSc4d1	účetní
závěrku	závěrka	k1gFnSc4	závěrka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
akcionářů	akcionář	k1gMnPc2	akcionář
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
počet	počet	k1gInSc1	počet
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
počtu	počet	k1gInSc2	počet
vlastněných	vlastněný	k2eAgFnPc2d1	vlastněná
akcií	akcie	k1gFnPc2	akcie
<g/>
.	.	kIx.	.
<g/>
Představenstvo	představenstvo	k1gNnSc1	představenstvo
–	–	k?	–
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgInSc1d1	statutární
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
řídit	řídit	k5eAaImF	řídit
společnost	společnost	k1gFnSc4	společnost
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
valnými	valný	k2eAgFnPc7d1	valná
hromadami	hromada	k1gFnPc7	hromada
<g/>
,	,	kIx,	,
operativně	operativně	k6eAd1	operativně
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
a	a	k8xC	a
vést	vést	k5eAaImF	vést
účetnictví	účetnictví	k1gNnSc4	účetnictví
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	on	k3xPp3gInPc4	on
volí	volit	k5eAaImIp3nS	volit
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
<g/>
,	,	kIx,	,
ledaže	ledaže	k8xS	ledaže
stanovy	stanova	k1gFnPc1	stanova
určí	určit	k5eAaPmIp3nP	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
působnost	působnost	k1gFnSc1	působnost
náleží	náležet	k5eAaImIp3nS	náležet
dozorčí	dozorčí	k2eAgFnSc3d1	dozorčí
radě	rada	k1gFnSc3	rada
<g/>
.	.	kIx.	.
</s>
<s>
Neurčí	určit	k5eNaPmIp3nP	určit
<g/>
-li	i	k?	-li
stanovy	stanova	k1gFnPc1	stanova
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
představenstvo	představenstvo	k1gNnSc1	představenstvo
3	[number]	k4	3
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Délku	délka	k1gFnSc4	délka
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
členů	člen	k1gMnPc2	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
sjednat	sjednat	k5eAaPmF	sjednat
libovolně	libovolně	k6eAd1	libovolně
<g/>
.	.	kIx.	.
<g/>
Dozorčí	dozorčí	k2eAgFnSc1d1	dozorčí
rada	rada	k1gFnSc1	rada
–	–	k?	–
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
působnost	působnost	k1gFnSc4	působnost
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
povoleno	povolen	k2eAgNnSc1d1	povoleno
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
účetnictví	účetnictví	k1gNnSc4	účetnictví
i	i	k8xC	i
všechny	všechen	k3xTgInPc4	všechen
další	další	k2eAgInPc4d1	další
doklady	doklad	k1gInPc4	doklad
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
členy	člen	k1gMnPc4	člen
vždy	vždy	k6eAd1	vždy
volí	volit	k5eAaImIp3nS	volit
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
<g/>
.	.	kIx.	.
</s>
<s>
Neurčí	určit	k5eNaPmIp3nP	určit
<g/>
-li	i	k?	-li
stanovy	stanova	k1gFnPc1	stanova
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dozorčí	dozorčí	k2eAgFnSc1d1	dozorčí
rada	rada	k1gFnSc1	rada
3	[number]	k4	3
členy	člen	k1gInPc4	člen
a	a	k8xC	a
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
člena	člen	k1gMnSc2	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společnostech	společnost	k1gFnPc6	společnost
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
volí	volit	k5eAaImIp3nP	volit
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
a	a	k8xC	a
počet	počet	k1gInSc1	počet
členů	člen	k1gMnPc2	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
dělitelný	dělitelný	k2eAgInSc4d1	dělitelný
třemi	tři	k4xCgNnPc7	tři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Monistický	monistický	k2eAgInSc1d1	monistický
systém	systém	k1gInSc1	systém
====	====	k?	====
</s>
</p>
<p>
<s>
Valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
–	–	k?	–
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
dualistického	dualistický	k2eAgInSc2d1	dualistický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
–	–	k?	–
kontrolně-řídící	kontrolně-řídící	k2eAgInSc4d1	kontrolně-řídící
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Neurčí	určit	k5eNaPmIp3nP	určit
<g/>
-li	i	k?	-li
stanovy	stanova	k1gFnPc1	stanova
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
správní	správní	k2eAgFnSc1d1	správní
rada	rada	k1gFnSc1	rada
3	[number]	k4	3
členy	člen	k1gMnPc7	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Statutární	statutární	k2eAgMnSc1d1	statutární
ředitel	ředitel	k1gMnSc1	ředitel
–	–	k?	–
statutární	statutární	k2eAgInSc1d1	statutární
orgán	orgán	k1gInSc1	orgán
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
správní	správní	k2eAgFnSc7d1	správní
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Přísluší	příslušet	k5eAaImIp3nS	příslušet
mu	on	k3xPp3gMnSc3	on
obchodní	obchodní	k2eAgNnSc1d1	obchodní
vedení	vedení	k1gNnSc1	vedení
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Označení	označení	k1gNnSc1	označení
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
==	==	k?	==
</s>
</p>
<p>
<s>
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
(	(	kIx(	(
<g/>
А	А	k?	А
д	д	k?	д
<g/>
,	,	kIx,	,
А	А	k?	А
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
(	(	kIx(	(
<g/>
Aktieselskab	Aktieselskab	k1gInSc1	Aktieselskab
<g/>
,	,	kIx,	,
A	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
(	(	kIx(	(
<g/>
Osakeyhtiö	Osakeyhtiö	k1gFnSc1	Osakeyhtiö
<g/>
,	,	kIx,	,
OY	OY	kA	OY
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
Société	Sociétý	k2eAgNnSc1d1	Société
anonyme	anonym	k1gInSc5	anonym
<g/>
,	,	kIx,	,
S.	S.	kA	S.
A.	A.	kA	A.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
(	(	kIx(	(
<g/>
dioničko	dioničko	k6eAd1	dioničko
društvo	društvo	k6eAd1	društvo
<g/>
,	,	kIx,	,
d.d.	d.d.	k?	d.d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
Società	Società	k1gFnPc1	Società
per	pero	k1gNnPc2	pero
Azioni	Azion	k1gMnPc1	Azion
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
p.	p.	k?	p.
<g/>
A.	A.	kA	A.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
(	(	kIx(	(
<g/>
Részvénytársaság	Részvénytársaság	k1gMnSc1	Részvénytársaság
<g/>
,	,	kIx,	,
Rt	Rt	k1gMnSc1	Rt
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
Zártkörűen	Zártkörűen	k2eAgMnSc1d1	Zártkörűen
Működő	Működő	k1gMnSc1	Működő
Részvénytársaság	Részvénytársaság	k1gMnSc1	Részvénytársaság
<g/>
,	,	kIx,	,
Zrt	Zrt	k1gMnSc1	Zrt
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
Aktiengesellschaft	Aktiengesellschaft	k1gInSc1	Aktiengesellschaft
<g/>
,	,	kIx,	,
AG	AG	kA	AG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
(	(	kIx(	(
<g/>
Aksjeselskap	Aksjeselskap	k1gInSc1	Aksjeselskap
<g/>
,	,	kIx,	,
AS	as	k1gInSc1	as
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
Spółka	Spółka	k1gFnSc1	Spółka
Akcyjna	Akcyjna	k1gFnSc1	Akcyjna
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
Societate	Societat	k1gMnSc5	Societat
pe	pe	k?	pe
Actiuni	Actiuň	k1gMnSc5	Actiuň
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
a.	a.	k?	a.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
А	А	k?	А
о	о	k?	о
<g/>
,	,	kIx,	,
Akcioněrnoje	Akcioněrnoj	k1gInPc1	Akcioněrnoj
Obščestvo	Obščestvo	k1gNnSc1	Obščestvo
<g/>
,	,	kIx,	,
AO	AO	kA	AO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
(	(	kIx(	(
<g/>
akciová	akciový	k2eAgFnSc1d1	akciová
spoločnosť	spoločnostit	k5eAaPmRp2nS	spoločnostit
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
nebo	nebo	k8xC	nebo
též	též	k9	též
účastinná	účastinný	k2eAgFnSc1d1	účastinný
spoločnosť	spoločnostit	k5eAaPmRp2nS	spoločnostit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
(	(	kIx(	(
<g/>
delniška	delniška	k1gFnSc1	delniška
družba	družba	k1gFnSc1	družba
<g/>
,	,	kIx,	,
d.d.	d.d.	k?	d.d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
(	(	kIx(	(
<g/>
akcionarsko	akcionarsko	k6eAd1	akcionarsko
društvo	društvo	k6eAd1	društvo
<g/>
,	,	kIx,	,
a.d.	a.d.	k?	a.d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
Sociedad	Sociedad	k1gInSc1	Sociedad
anónima	anónim	k1gMnSc2	anónim
<g/>
,	,	kIx,	,
S.	S.	kA	S.
A.	A.	kA	A.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
(	(	kIx(	(
<g/>
Aktiebolag	Aktiebolag	k1gInSc1	Aktiebolag
<g/>
,	,	kIx,	,
AB	AB	kA	AB
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Lasák	Lasák	k1gMnSc1	Lasák
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Pokorná	Pokorná	k1gFnSc1	Pokorná
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Čáp	Čáp	k1gMnSc1	Čáp
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Doležil	Doležil	k1gFnSc1	Doležil
<g/>
,	,	kIx,	,
T.	T.	kA	T.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
obchodních	obchodní	k2eAgFnPc6d1	obchodní
korporacích	korporace	k1gFnPc6	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Komentář	komentář	k1gInSc1	komentář
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Wolkers	Wolkers	k1gInSc1	Wolkers
Kluwer	Kluwra	k1gFnPc2	Kluwra
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Štenglová	Štenglová	k1gFnSc1	Štenglová
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g/>
,	,	kIx,	,
Cileček	Cileček	k1gMnSc1	Cileček
<g/>
,	,	kIx,	,
F.	F.	kA	F.
<g/>
,	,	kIx,	,
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Šuk	šuk	k0	šuk
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
obchodních	obchodní	k2eAgFnPc6d1	obchodní
korporacích	korporace	k1gFnPc6	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Komentář	komentář	k1gInSc1	komentář
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
C.	C.	kA	C.
H.	H.	kA	H.
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
1008	[number]	k4	1008
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Hejda	Hejda	k1gMnSc1	Hejda
<g/>
,	,	kIx,	,
J.	J.	kA	J.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
obchodních	obchodní	k2eAgFnPc6d1	obchodní
korporacích	korporace	k1gFnPc6	korporace
<g/>
.	.	kIx.	.
</s>
<s>
Výklad	výklad	k1gInSc1	výklad
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
ustanovení	ustanovení	k1gNnSc2	ustanovení
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnSc2	jejich
návaznosti	návaznost	k1gFnSc2	návaznost
na	na	k7c4	na
české	český	k2eAgInPc4d1	český
a	a	k8xC	a
evropské	evropský	k2eAgInPc4d1	evropský
předpisy	předpis	k1gInPc4	předpis
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Linde	Lind	k1gMnSc5	Lind
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
784	[number]	k4	784
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1	encyklopedické
heslo	heslo	k1gNnSc4	heslo
Akciová	akciový	k2eAgFnSc1d1	akciová
neb	neb	k8xC	neb
akcijní	akcijní	k2eAgFnSc1d1	akcijní
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c4	v
WikizdrojíchČást	WikizdrojíchČást	k1gInSc4	WikizdrojíchČást
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
obchodních	obchodní	k2eAgFnPc6d1	obchodní
korporacích	korporace	k1gFnPc6	korporace
<g/>
,	,	kIx,	,
věnující	věnující	k2eAgFnSc7d1	věnující
se	se	k3xPyFc4	se
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
zakonyprolidi	zakonyprolidit	k5eAaPmRp2nS	zakonyprolidit
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
,	,	kIx,	,
nadační	nadační	k2eAgInSc4d1	nadační
fond	fond	k1gInSc4	fond
</s>
</p>
