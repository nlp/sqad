<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
erotika	erotika	k1gFnSc1	erotika
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnPc4	třetí
pokračování	pokračování	k1gNnPc4	pokračování
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
komedie-trilogie	komedierilogie	k1gFnSc2	komedie-trilogie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Trošky	troška	k1gFnSc2	troška
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnPc1d1	předchozí
dvě	dva	k4xCgFnPc1	dva
komedie	komedie	k1gFnPc1	komedie
byly	být	k5eAaImAgFnP	být
situovány	situovat	k5eAaBmNgFnP	situovat
do	do	k7c2	do
malé	malý	k2eAgFnSc2d1	malá
vesnice	vesnice	k1gFnSc2	vesnice
Hoštice	Hoštice	k1gFnSc2	Hoštice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
i	i	k9	i
natáčeny	natáčen	k2eAgFnPc1d1	natáčena
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
je	být	k5eAaImIp3nS	být
děj	děj	k1gInSc1	děj
situován	situovat	k5eAaBmNgInS	situovat
i	i	k8xC	i
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Hoštic	Hoštice	k1gFnPc2	Hoštice
jedou	jet	k5eAaImIp3nP	jet
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c4	na
výměnný	výměnný	k2eAgInSc4d1	výměnný
pobyt	pobyt	k1gInSc4	pobyt
<g/>
,	,	kIx,	,
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zvelebit	zvelebit	k5eAaPmF	zvelebit
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
založení	založení	k1gNnSc2	založení
nudistické	nudistický	k2eAgFnSc2d1	nudistická
pláže	pláž	k1gFnSc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
ale	ale	k9	ale
nečekaně	nečekaně	k6eAd1	nečekaně
zvrtne	zvrtnout	k5eAaPmIp3nS	zvrtnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
italská	italský	k2eAgFnSc1d1	italská
delegace	delegace	k1gFnSc1	delegace
dorazí	dorazit	k5eAaPmIp3nS	dorazit
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
erotika	erotika	k1gFnSc1	erotika
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
erotika	erotika	k1gFnSc1	erotika
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
