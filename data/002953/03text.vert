<s>
Kontradikce	kontradikce	k1gFnSc1	kontradikce
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
contra-dicere	contraicrat	k5eAaPmIp3nS	contra-dicrat
<g/>
,	,	kIx,	,
protiřečit	protiřečit	k5eAaImF	protiřečit
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
spor	spor	k1gInSc4	spor
nebo	nebo	k8xC	nebo
protimluv	protimluv	k1gInSc4	protimluv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
logice	logika	k1gFnSc6	logika
znamená	znamenat	k5eAaImIp3nS	znamenat
dvojici	dvojice	k1gFnSc4	dvojice
takových	takový	k3xDgInPc2	takový
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
pravdivosti	pravdivost	k1gFnSc2	pravdivost
jednoho	jeden	k4xCgMnSc2	jeden
plyne	plynout	k5eAaImIp3nS	plynout
nepravdivost	nepravdivost	k1gFnSc1	nepravdivost
druhého	druhý	k4xOgMnSc2	druhý
a	a	k8xC	a
z	z	k7c2	z
nepravdivosti	nepravdivost	k1gFnSc2	nepravdivost
jednoho	jeden	k4xCgMnSc2	jeden
plyne	plynout	k5eAaImIp3nS	plynout
pravdivost	pravdivost	k1gFnSc1	pravdivost
druhého	druhý	k4xOgMnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Kontradiktorní	kontradiktorní	k2eAgMnPc1d1	kontradiktorní
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
dva	dva	k4xCgInPc4	dva
atributy	atribut	k1gInPc4	atribut
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
společný	společný	k2eAgInSc4d1	společný
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
dohromady	dohromady	k6eAd1	dohromady
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
všechny	všechen	k3xTgInPc4	všechen
možné	možný	k2eAgInPc4d1	možný
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
výroky	výrok	k1gInPc1	výrok
"	"	kIx"	"
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
A	a	k9	a
není	být	k5eNaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
kontradiktorické	kontradiktorický	k2eAgFnPc1d1	kontradiktorická
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
platí	platit	k5eAaImIp3nP	platit
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
platit	platit	k5eAaImF	platit
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Konjunkce	konjunkce	k1gFnSc1	konjunkce
takových	takový	k3xDgInPc2	takový
výroků	výrok	k1gInPc2	výrok
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
disjunkce	disjunkce	k1gFnSc1	disjunkce
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
(	(	kIx(	(
<g/>
tautologická	tautologický	k2eAgFnSc1d1	tautologická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Contradictio	Contradictio	k6eAd1	Contradictio
in	in	k?	in
adiecto	adiecto	k1gNnSc4	adiecto
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
protimluv	protimluv	k1gInSc4	protimluv
či	či	k8xC	či
spor	spor	k1gInSc4	spor
ve	v	k7c4	v
spojení	spojení	k1gNnSc4	spojení
podstatného	podstatný	k2eAgNnSc2d1	podstatné
a	a	k8xC	a
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
kulatý	kulatý	k2eAgInSc1d1	kulatý
čtverec	čtverec	k1gInSc1	čtverec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výroky	výrok	k1gInPc1	výrok
"	"	kIx"	"
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
A	a	k9	a
je	být	k5eAaImIp3nS	být
černé	černé	k1gNnSc1	černé
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
A	a	k9	a
není	být	k5eNaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
neplyne	plynout	k5eNaImIp3nS	plynout
ještě	ještě	k9	ještě
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
černé	černý	k2eAgNnSc1d1	černé
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
výroky	výrok	k1gInPc1	výrok
nejsou	být	k5eNaImIp3nP	být
kontradiktorické	kontradiktorický	k2eAgInPc1d1	kontradiktorický
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
kontrární	kontrární	k2eAgNnSc4d1	kontrární
(	(	kIx(	(
<g/>
opačné	opačný	k2eAgNnSc4d1	opačné
<g/>
,	,	kIx,	,
protivy	protiv	k1gInPc4	protiv
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
části	část	k1gFnPc1	část
textu	text	k1gInSc2	text
navzájem	navzájem	k6eAd1	navzájem
protiřečí	protiřečit	k5eAaImIp3nS	protiřečit
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
takový	takový	k3xDgInSc1	takový
text	text	k1gInSc1	text
za	za	k7c4	za
nekoherentní	koherentní	k2eNgNnSc4d1	nekoherentní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
mluvě	mluva	k1gFnSc6	mluva
někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
kontradikce	kontradikce	k1gFnPc4	kontradikce
významovou	významový	k2eAgFnSc4d1	významová
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
širšího	široký	k2eAgInSc2d2	širší
kontextu	kontext	k1gInSc2	kontext
<g/>
:	:	kIx,	:
Ten	ten	k3xDgMnSc1	ten
muž	muž	k1gMnSc1	muž
není	být	k5eNaImIp3nS	být
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
naše	náš	k3xOp1gMnPc4	náš
je	on	k3xPp3gMnPc4	on
i	i	k8xC	i
vaše	váš	k3xOp2gMnPc4	váš
<g/>
.	.	kIx.	.
</s>
