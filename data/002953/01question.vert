<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývájí	nazývájet	k5eAaPmIp3nP	nazývájet
výroky	výrok	k1gInPc1	výrok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
?	?	kIx.	?
</s>
