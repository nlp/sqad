<s>
Takové	takový	k3xDgInPc1	takový
výroky	výrok	k1gInPc1	výrok
nejsou	být	k5eNaImIp3nP	být
kontradiktorické	kontradiktorický	k2eAgInPc1d1	kontradiktorický
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
kontrární	kontrární	k2eAgNnSc4d1	kontrární
(	(	kIx(	(
<g/>
opačné	opačný	k2eAgNnSc4d1	opačné
<g/>
,	,	kIx,	,
protivy	protiv	k1gInPc4	protiv
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
současně	současně	k6eAd1	současně
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
