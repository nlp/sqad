<s>
Mars	Mars	k1gInSc1	Mars
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejmenší	malý	k2eAgFnSc1d3	nejmenší
planeta	planeta	k1gFnSc1	planeta
soustavy	soustava	k1gFnSc2	soustava
po	po	k7c6	po
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
římském	římský	k2eAgMnSc6d1	římský
bohu	bůh	k1gMnSc6	bůh
války	válka	k1gFnSc2	válka
Martovi	Mars	k1gMnSc3	Mars
<g/>
.	.	kIx.	.
</s>

