<p>
<s>
Richňava	Richňava	k1gFnSc1	Richňava
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
umělých	umělý	k2eAgNnPc2d1	umělé
jezer	jezero	k1gNnPc2	jezero
při	při	k7c6	při
Banské	banský	k2eAgFnSc6d1	Banská
Štiavnici	Štiavnica	k1gFnSc6	Štiavnica
<g/>
.	.	kIx.	.
</s>
<s>
Jezera	jezero	k1gNnPc1	jezero
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
tajchy	tajch	k1gInPc1	tajch
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
jako	jako	k9	jako
zásobníky	zásobník	k1gInPc1	zásobník
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
Richňavské	Richňavský	k2eAgNnSc4d1	Richňavský
jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
velké	velký	k2eAgInPc1d1	velký
tajchy	tajch	k1gInPc1	tajch
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvýše	vysoce	k6eAd3	vysoce
položeným	položený	k2eAgInPc3d1	položený
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Banské	banský	k2eAgFnSc2d1	Banská
Štiavnice	Štiavnica	k1gFnSc2	Štiavnica
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
a	a	k8xC	a
Malé	Malé	k2eAgNnSc1d1	Malé
Richňavské	Richňavský	k2eAgNnSc1d1	Richňavský
jezero	jezero	k1gNnSc1	jezero
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
propojeny	propojit	k5eAaPmNgFnP	propojit
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
je	on	k3xPp3gInPc4	on
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
hráz	hráz	k1gFnSc1	hráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přístup	přístup	k1gInSc4	přístup
-	-	kIx~	-
k	k	k7c3	k
jezerům	jezero	k1gNnPc3	jezero
se	se	k3xPyFc4	se
dostaneme	dostat	k5eAaPmIp1nP	dostat
pohodlně	pohodlně	k6eAd1	pohodlně
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
autem	auto	k1gNnSc7	auto
nebo	nebo	k8xC	nebo
autobusem	autobus	k1gInSc7	autobus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgNnPc1	dva
jezera	jezero	k1gNnPc1	jezero
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
osmnáctém	osmnáctý	k4xOgNnSc6	osmnáctý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
vlastně	vlastně	k9	vlastně
nádrž	nádrž	k1gFnSc1	nádrž
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc7d2	veliký
objemem	objem	k1gInSc7	objem
jímané	jímaný	k2eAgFnSc2d1	jímaná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
vybudování	vybudování	k1gNnSc2	vybudování
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
prosakování	prosakování	k1gNnSc1	prosakování
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnSc2d1	velká
nádrže	nádrž	k1gFnSc2	nádrž
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
oprávněné	oprávněný	k2eAgFnPc4d1	oprávněná
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
poškození	poškození	k1gNnSc2	poškození
<g/>
,	,	kIx,	,
či	či	k8xC	či
snad	snad	k9	snad
protržení	protržení	k1gNnSc2	protržení
hráze	hráz	k1gFnSc2	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
shromážděné	shromážděný	k2eAgFnSc2d1	shromážděná
vody	voda	k1gFnSc2	voda
k	k	k7c3	k
důlním	důlní	k2eAgInPc3d1	důlní
dílům	díl	k1gInPc3	díl
byla	být	k5eAaImAgFnS	být
řešena	řešit	k5eAaImNgFnS	řešit
systémem	systém	k1gInSc7	systém
vodních	vodní	k2eAgFnPc2d1	vodní
štol	štola	k1gFnPc2	štola
a	a	k8xC	a
struh	strouha	k1gFnPc2	strouha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
nabízí	nabízet	k5eAaImIp3nS	nabízet
jezero	jezero	k1gNnSc1	jezero
možnost	možnost	k1gFnSc1	možnost
koupání	koupání	k1gNnSc1	koupání
a	a	k8xC	a
vodních	vodní	k2eAgInPc2d1	vodní
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Richňavské	Richňavský	k2eAgNnSc1d1	Richňavský
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
známé	známý	k2eAgInPc4d1	známý
mezi	mezi	k7c7	mezi
potápěči	potápěč	k1gMnPc7	potápěč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zde	zde	k6eAd1	zde
pravidelně	pravidelně	k6eAd1	pravidelně
pořádají	pořádat	k5eAaImIp3nP	pořádat
školení	školení	k1gNnPc1	školení
a	a	k8xC	a
kurzy	kurz	k1gInPc1	kurz
potápění	potápění	k1gNnSc2	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
jako	jako	k9	jako
rybářský	rybářský	k2eAgInSc1d1	rybářský
revír	revír	k1gInSc1	revír
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
červeně	červeně	k6eAd1	červeně
zbarvené	zbarvený	k2eAgNnSc1d1	zbarvené
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kvůli	kvůli	k7c3	kvůli
řase	řasa	k1gFnSc3	řasa
Tovellia	Tovellius	k1gMnSc2	Tovellius
sanguinea	sanguineus	k1gMnSc2	sanguineus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Richňava	Richňava	k1gFnSc1	Richňava
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
