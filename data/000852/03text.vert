<s>
Velká	velká	k1gFnSc1	velká
kunratická	kunratický	k2eAgFnSc1d1	Kunratická
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
běžecký	běžecký	k2eAgInSc1d1	běžecký
přespolní	přespolní	k2eAgInSc1d1	přespolní
závod	závod	k1gInSc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
Běhá	běhat	k5eAaImIp3nS	běhat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
druhou	druhý	k4xOgFnSc4	druhý
listopadovou	listopadový	k2eAgFnSc4d1	listopadová
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
Kunratickém	kunratický	k2eAgInSc6d1	kunratický
lese	les	k1gInSc6	les
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnSc4d1	hlavní
trať	trať	k1gFnSc4	trať
závodu	závod	k1gInSc2	závod
(	(	kIx(	(
<g/>
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
kategorii	kategorie	k1gFnSc6	kategorie
a	a	k8xC	a
kategorii	kategorie	k1gFnSc6	kategorie
ženy-Hrádek	ženy-Hrádek	k1gInSc1	ženy-Hrádek
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
charakteristická	charakteristický	k2eAgNnPc1d1	charakteristické
tři	tři	k4xCgNnPc4	tři
prudká	prudký	k2eAgNnPc4d1	prudké
stoupání	stoupání	k1gNnPc4	stoupání
a	a	k8xC	a
tři	tři	k4xCgInPc1	tři
přeběhy	přeběh	k1gInPc1	přeběh
Kunratického	kunratický	k2eAgInSc2d1	kunratický
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
"	"	kIx"	"
<g/>
výběh	výběh	k1gInSc1	výběh
k	k	k7c3	k
Hrádku	Hrádok	k1gInSc3	Hrádok
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
60	[number]	k4	60
m	m	kA	m
převýšení	převýšení	k1gNnSc2	převýšení
45	[number]	k4	45
m	m	kA	m
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
běžící	běžící	k2eAgFnPc4d1	běžící
přes	přes	k7c4	přes
Hrádek	hrádek	k1gInSc4	hrádek
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
3	[number]	k4	3
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
ženská	ženský	k2eAgFnSc1d1	ženská
trať	trať	k1gFnSc1	trať
měří	měřit	k5eAaImIp3nS	měřit
1	[number]	k4	1
330	[number]	k4	330
m.	m.	k?	m.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
trasa	trasa	k1gFnSc1	trasa
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
na	na	k7c4	na
1900	[number]	k4	1900
<g/>
m	m	kA	m
<g/>
(	(	kIx(	(
<g/>
trať	trať	k1gFnSc1	trať
dorostenců	dorostenec	k1gMnPc2	dorostenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
běhá	běhat	k5eAaImIp3nS	běhat
21	[number]	k4	21
kategorií	kategorie	k1gFnPc2	kategorie
na	na	k7c6	na
pěti	pět	k4xCc6	pět
různých	různý	k2eAgFnPc6d1	různá
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
Velké	velký	k2eAgNnSc4d1	velké
kunratické	kunratický	k2eAgNnSc4d1	Kunratické
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
mnoho	mnoho	k4c1	mnoho
rekordů	rekord	k1gInPc2	rekord
především	především	k9	především
podle	podle	k7c2	podle
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Nejcennější	cenný	k2eAgInPc1d3	nejcennější
jsou	být	k5eAaImIp3nP	být
rekordy	rekord	k1gInPc1	rekord
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
mužů	muž	k1gMnPc2	muž
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Traťový	traťový	k2eAgInSc1d1	traťový
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
mužů	muž	k1gMnPc2	muž
drží	držet	k5eAaImIp3nP	držet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
český	český	k2eAgMnSc1d1	český
atlet	atlet	k1gMnSc1	atlet
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Zwiefelhofer	Zwiefelhofer	k1gMnSc1	Zwiefelhofer
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
časem	čas	k1gInSc7	čas
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
58,9	[number]	k4	58,9
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
V	v	k7c6	v
ženských	ženský	k2eAgFnPc6d1	ženská
kategoriích	kategorie	k1gFnPc6	kategorie
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
traťové	traťový	k2eAgInPc1d1	traťový
rekordy	rekord	k1gInPc1	rekord
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
trasy	trasa	k1gFnSc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
kategorii	kategorie	k1gFnSc6	kategorie
žen	žena	k1gFnPc2	žena
jej	on	k3xPp3gInSc4	on
drží	držet	k5eAaImIp3nS	držet
Pavla	Pavla	k1gFnSc1	Pavla
Havlová	Havlová	k1gFnSc1	Havlová
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
časem	čas	k1gInSc7	čas
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
33,9	[number]	k4	33,9
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
ženy	žena	k1gFnSc2	žena
poprvé	poprvé	k6eAd1	poprvé
běžely	běžet	k5eAaImAgInP	běžet
přes	přes	k7c4	přes
Hrádek	hrádek	k1gInSc4	hrádek
<g/>
.	.	kIx.	.
</s>
<s>
Rekordně	rekordně	k6eAd1	rekordně
nejrychlejší	rychlý	k2eAgFnSc1d3	nejrychlejší
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
Kamila	Kamila	k1gFnSc1	Kamila
Gregorová	Gregorová	k1gFnSc1	Gregorová
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
časem	čas	k1gInSc7	čas
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
58,1	[number]	k4	58,1
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
jako	jako	k8xC	jako
klubový	klubový	k2eAgInSc4d1	klubový
závod	závod	k1gInSc4	závod
Vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
sportu	sport	k1gInSc2	sport
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jej	on	k3xPp3gMnSc4	on
pořádá	pořádat	k5eAaImIp3nS	pořádat
Atletický	atletický	k2eAgInSc1d1	atletický
klub	klub	k1gInSc1	klub
Spartak	Spartak	k1gInSc1	Spartak
Praha	Praha	k1gFnSc1	Praha
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Padesátý	padesátý	k4xOgInSc1	padesátý
ročník	ročník	k1gInSc1	ročník
Velké	velká	k1gFnSc2	velká
kunratické	kunratický	k2eAgFnSc2d1	Kunratická
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
předsedy	předseda	k1gMnSc2	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
Juana	Juan	k1gMnSc4	Juan
Antonia	Antonio	k1gMnSc4	Antonio
Samaranche	Samaranch	k1gMnSc4	Samaranch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tehdy	tehdy	k6eAd1	tehdy
osobně	osobně	k6eAd1	osobně
předával	předávat	k5eAaImAgMnS	předávat
pamětní	pamětní	k2eAgFnPc4d1	pamětní
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
rekordních	rekordní	k2eAgNnPc2d1	rekordní
9	[number]	k4	9
128	[number]	k4	128
běžců	běžec	k1gMnPc2	běžec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc1	závod
méně	málo	k6eAd2	málo
masový	masový	k2eAgInSc1d1	masový
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
mezi	mezi	k7c4	mezi
atlety	atlet	k1gMnPc4	atlet
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
velice	velice	k6eAd1	velice
vyhledávaný	vyhledávaný	k2eAgInSc1d1	vyhledávaný
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
především	především	k9	především
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
trasy	trasa	k1gFnSc2	trasa
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
po	po	k7c6	po
území	území	k1gNnSc6	území
přírodní	přírodní	k2eAgFnSc2d1	přírodní
památky	památka	k1gFnSc2	památka
Údolí	údolí	k1gNnSc2	údolí
Kunratického	kunratický	k2eAgInSc2d1	kunratický
potoka	potok	k1gInSc2	potok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
celkový	celkový	k2eAgInSc1d1	celkový
limit	limit	k1gInSc1	limit
3	[number]	k4	3
400	[number]	k4	400
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
