<s>
U	u	k7c2
všech	všecek	k3xTgInPc2
Slovanů	Slovan	k1gInPc2
je	být	k5eAaImIp3nS
předpokládána	předpokládán	k2eAgFnSc1d1
víra	víra	k1gFnSc1
v	v	k7c4
Peruna	Perun	k1gMnSc4
<g/>
,	,	kIx,
boha	bůh	k1gMnSc4
hromu	hrom	k1gInSc2
a	a	k8xC
blesku	blesk	k1gInSc2
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
známý	známý	k2eAgInSc1d1
také	také	k9
nejspíš	nejspíš	k9
byl	být	k5eAaImAgMnS
chtonický	chtonický	k2eAgMnSc1d1
bůh	bůh	k1gMnSc1
Veles	Veles	k1gMnSc1
spojovaný	spojovaný	k2eAgInSc4d1
se	s	k7c7
stády	stádo	k1gNnPc7
<g/>
,	,	kIx,
bohatstvím	bohatství	k1gNnSc7
a	a	k8xC
magií	magie	k1gFnSc7
a	a	k8xC
Mokoš	Mokoš	k1gFnSc1
<g/>
,	,	kIx,
bohyně	bohyně	k1gFnSc1
vody	voda	k1gFnSc2
a	a	k8xC
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>