<s>
Slovanská	slovanský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
</s>
<s>
Morana	Morana	k1gFnSc1
a	a	k8xC
Vesna	Vesna	k1gFnSc1
v	v	k7c6
moderním	moderní	k2eAgNnSc6d1
podání	podání	k1gNnSc6
</s>
<s>
Slovanská	slovanský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc4
předkřesťanských	předkřesťanský	k2eAgFnPc2d1
slovanských	slovanský	k2eAgFnPc2d1
představ	představa	k1gFnPc2
a	a	k8xC
příběhů	příběh	k1gInPc2
o	o	k7c6
stvoření	stvoření	k1gNnSc6
a	a	k8xC
přirozenosti	přirozenost	k1gFnSc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
bozích	bůh	k1gMnPc6
<g/>
,	,	kIx,
démonech	démon	k1gMnPc6
a	a	k8xC
hrdinech	hrdina	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
slovanského	slovanský	k2eAgNnSc2d1
předkřesťanského	předkřesťanský	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
a	a	k8xC
sdílí	sdílet	k5eAaImIp3nS
mnohé	mnohý	k2eAgInPc4d1
společné	společný	k2eAgInPc4d1
rysy	rys	k1gInPc4
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
indoevropskými	indoevropský	k2eAgFnPc7d1
mytologiemi	mytologie	k1gFnPc7
<g/>
,	,	kIx,
například	například	k6eAd1
významné	významný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
hromovládného	hromovládný	k2eAgMnSc2d1
boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
jejích	její	k3xOp3gInPc2
prvků	prvek	k1gInPc2
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgInP
ve	v	k7c6
slovanském	slovanský	k2eAgInSc6d1
lidové	lidový	k2eAgFnPc4d1
tradici	tradice	k1gFnSc4
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
prvky	prvek	k1gInPc7
křesťanskými	křesťanský	k2eAgInPc7d1
a	a	k8xC
neslovanskými	slovanský	k2eNgInPc7d1
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
významnou	významný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
rodnověří	rodnověří	k1gNnSc2
–	–	k?
slovanské	slovanský	k2eAgFnSc2d1
novopohanské	novopohanský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stav	stav	k1gInSc1
poznání	poznání	k1gNnSc2
slovanské	slovanský	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
je	být	k5eAaImIp3nS
velké	velký	k2eAgFnSc3d1
míře	míra	k1gFnSc3
limitován	limitovat	k5eAaBmNgInS
vzácností	vzácnost	k1gFnPc2
a	a	k8xC
torzovitostí	torzovitost	k1gFnPc2
dobových	dobový	k2eAgInPc2d1
písemných	písemný	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
o	o	k7c6
ní	on	k3xPp3gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autentické	autentický	k2eAgInPc4d1
slovanské	slovanský	k2eAgInPc4d1
mýty	mýtus	k1gInPc4
prakticky	prakticky	k6eAd1
nejsou	být	k5eNaImIp3nP
zachovány	zachován	k2eAgInPc1d1
a	a	k8xC
na	na	k7c4
její	její	k3xOp3gFnSc4
podobu	podoba	k1gFnSc4
se	se	k3xPyFc4
usuzuje	usuzovat	k5eAaImIp3nS
kromě	kromě	k7c2
historických	historický	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
především	především	k9
z	z	k7c2
komparativní	komparativní	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
<g/>
,	,	kIx,
lingvistiky	lingvistika	k1gFnSc2
<g/>
,	,	kIx,
archeologie	archeologie	k1gFnSc2
a	a	k8xC
pozdního	pozdní	k2eAgInSc2d1
folklóru	folklór	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Studium	studium	k1gNnSc1
</s>
<s>
Socha	socha	k1gFnSc1
Radegasta	Radegast	k1gMnSc2
z	z	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
na	na	k7c6
beskydském	beskydský	k2eAgInSc6d1
vrchu	vrch	k1gInSc6
Radhošť	Radhošť	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Studium	studium	k1gNnSc1
slovanské	slovanský	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
a	a	k8xC
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Písemné	písemný	k2eAgInPc1d1
prameny	pramen	k1gInPc1
o	o	k7c6
slovanské	slovanský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
chudé	chudý	k2eAgMnPc4d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
do	do	k7c2
množství	množství	k1gNnSc2
tak	tak	k9
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
do	do	k7c2
obsahu	obsah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškeré	veškerý	k3xTgInPc4
prameny	pramen	k1gInPc4
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
několika	několik	k4yIc2
z	z	k7c2
arabského	arabský	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
také	také	k9
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
křesťanských	křesťanský	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
zatíženy	zatížit	k5eAaPmNgInP
jejich	jejich	k3xOp3gNnSc1
vírou	víra	k1gFnSc7
a	a	k8xC
znalostí	znalost	k1gFnSc7
antického	antický	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
pramenů	pramen	k1gInPc2
je	být	k5eAaImIp3nS
také	také	k9
dílem	díl	k1gInSc7
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
neuměli	umět	k5eNaImAgMnP
slovanský	slovanský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dalším	další	k2eAgInSc7d1
problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
že	že	k8xS
až	až	k9
na	na	k7c4
výjimky	výjimka	k1gFnPc4
prameny	pramen	k1gInPc1
pochází	pocházet	k5eAaImIp3nP
až	až	k9
z	z	k7c2
doby	doba	k1gFnSc2
christianizace	christianizace	k1gFnSc2
či	či	k8xC
doby	doba	k1gFnSc2
těsně	těsně	k6eAd1
předcházející	předcházející	k2eAgFnPc1d1
nebo	nebo	k8xC
následující	následující	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
už	už	k6eAd1
byly	být	k5eAaImAgFnP
náboženské	náboženský	k2eAgFnPc1d1
a	a	k8xC
mytologické	mytologický	k2eAgFnPc1d1
představy	představa	k1gFnPc1
Slovanů	Slovan	k1gInPc2
diferencovány	diferencován	k2eAgFnPc1d1
a	a	k8xC
tak	tak	k6eAd1
nelze	lze	k6eNd1
tyto	tento	k3xDgFnPc4
zprávy	zpráva	k1gFnPc4
vztáhnout	vztáhnout	k5eAaPmF
na	na	k7c4
celé	celý	k2eAgNnSc4d1
slovanské	slovanský	k2eAgNnSc4d1
náboženství	náboženství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
východoslovanském	východoslovanský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgInSc7d1
pramenem	pramen	k1gInSc7
Pověst	pověst	k1gFnSc1
dávných	dávný	k2eAgNnPc2d1
let	léto	k1gNnPc2
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
různá	různý	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
církevní	církevní	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
především	především	k9
kázání	kázání	k1gNnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
mytologii	mytologie	k1gFnSc4
Polabských	polabský	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
zase	zase	k9
různé	různý	k2eAgFnPc1d1
kroniky	kronika	k1gFnPc1
křesťanských	křesťanský	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
Dětmara	Dětmara	k1gFnSc1
z	z	k7c2
Merseburku	Merseburk	k1gInSc2
nebo	nebo	k8xC
Helmolda	Helmoldo	k1gNnSc2
z	z	k7c2
Bosau	Bosaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1
zájem	zájem	k1gInSc1
o	o	k7c4
slovanskou	slovanský	k2eAgFnSc4d1
mytologii	mytologie	k1gFnSc4
se	se	k3xPyFc4
počal	počnout	k5eAaPmAgMnS
rozvíjet	rozvíjet	k5eAaImF
především	především	k9
s	s	k7c7
nástupem	nástup	k1gInSc7
romantismu	romantismus	k1gInSc2
na	na	k7c6
počátku	počátek	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
takzvané	takzvaný	k2eAgFnSc2d1
mytologické	mytologický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
díla	dílo	k1gNnSc2
bratří	bratr	k1gMnPc2
Grimmů	Grimm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
počal	počnout	k5eAaPmAgMnS
nastupovat	nastupovat	k5eAaImF
do	do	k7c2
této	tento	k3xDgFnSc2
oblasti	oblast	k1gFnSc2
pozitivistický	pozitivistický	k2eAgInSc4d1
přístup	přístup	k1gInSc4
vymezující	vymezující	k2eAgInSc4d1
se	se	k3xPyFc4
proti	proti	k7c3
spekulativním	spekulativní	k2eAgFnPc3d1
hypotézám	hypotéza	k1gFnPc3
a	a	k8xC
nekritické	kritický	k2eNgNnSc1d1
čerpání	čerpání	k1gNnSc1
z	z	k7c2
pozdní	pozdní	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
zájmu	zájem	k1gInSc2
silně	silně	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
archeologickými	archeologický	k2eAgInPc7d1
poznatky	poznatek	k1gInPc7
a	a	k8xC
srovnávací	srovnávací	k2eAgFnSc7d1
mytologií	mytologie	k1gFnSc7
založenou	založený	k2eAgFnSc7d1
na	na	k7c6
trojfunkční	trojfunkční	k2eAgFnSc6d1
hypotéze	hypotéza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mytologické	mytologický	k2eAgFnPc1d1
představy	představa	k1gFnPc1
</s>
<s>
Slovanské	slovanský	k2eAgInPc1d1
mýty	mýtus	k1gInPc1
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgInP
pouze	pouze	k6eAd1
v	v	k7c6
zlomkovité	zlomkovitý	k2eAgFnSc6d1
a	a	k8xC
nepůvodní	původní	k2eNgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
však	však	k9
bohatý	bohatý	k2eAgInSc1d1
materiál	materiál	k1gInSc1
svědčící	svědčící	k2eAgInSc1d1
o	o	k7c6
animistickém	animistický	k2eAgInSc6d1
pohledu	pohled	k1gInSc6
na	na	k7c4
svět	svět	k1gInSc4
a	a	k8xC
bytostech	bytost	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jej	on	k3xPp3gMnSc4
obývaly	obývat	k5eAaImAgInP
a	a	k8xC
také	také	k9
o	o	k7c6
sezónních	sezónní	k2eAgFnPc6d1
slavnostech	slavnost	k1gFnPc6
<g/>
,	,	kIx,
svědčících	svědčící	k2eAgInPc2d1
o	o	k7c6
cyklickém	cyklický	k2eAgNnSc6d1
chápání	chápání	k1gNnSc6
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kosmogonie	kosmogonie	k1gFnSc1
</s>
<s>
Mýtus	mýtus	k1gInSc1
o	o	k7c6
stvoření	stvoření	k1gNnSc6
světa	svět	k1gInSc2
se	se	k3xPyFc4
u	u	k7c2
Slovanů	Slovan	k1gMnPc2
zachoval	zachovat	k5eAaPmAgInS
pouze	pouze	k6eAd1
ve	v	k7c6
folklórním	folklórní	k2eAgNnSc6d1
podání	podání	k1gNnSc6
a	a	k8xC
v	v	k7c6
Povesti	Povest	k1gFnSc6
vremenných	vremenný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
sice	sice	k8xC
má	mít	k5eAaImIp3nS
spíše	spíše	k9
podobu	podoba	k1gFnSc4
křesťanského	křesťanský	k2eAgInSc2d1
apokryfu	apokryf	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gNnSc1
podání	podání	k1gNnSc1
stvoření	stvoření	k1gNnSc2
světa	svět	k1gInSc2
folklóru	folklór	k1gInSc2
odpovídá	odpovídat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c6
stvoření	stvoření	k1gNnSc6
dvěma	dva	k4xCgInPc7
demiurgy	demiurg	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
má	mít	k5eAaImIp3nS
tvůrčí	tvůrčí	k2eAgInSc4d1
potenciál	potenciál	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
pasivní	pasivní	k2eAgInSc4d1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
tvůrčího	tvůrčí	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
představováni	představován	k2eAgMnPc1d1
většinou	většinou	k6eAd1
v	v	k7c6
ptačí	ptačí	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
později	pozdě	k6eAd2
christianizováni	christianizovat	k5eAaImNgMnP
na	na	k7c4
Boha	bůh	k1gMnSc4
a	a	k8xC
Ďábla	ďábel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
je	být	k5eAaImIp3nS
pasivním	pasivní	k2eAgMnSc7d1
stvořitelem	stvořitel	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
někdy	někdy	k6eAd1
ani	ani	k9
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
svět	svět	k1gInSc4
stvořit	stvořit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ďábel	ďábel	k1gMnSc1
se	se	k3xPyFc4
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
příkaz	příkaz	k1gInSc4
potápí	potápět	k5eAaImIp3nS
na	na	k7c4
dno	dno	k1gNnSc4
světového	světový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
a	a	k8xC
přináší	přinášet	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
odtud	odtud	k6eAd1
hrst	hrst	k1gFnSc1
písku	písek	k1gInSc2
a	a	k8xC
zeminy	zemina	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
Bůh	bůh	k1gMnSc1
stvoří	stvořit	k5eAaPmIp3nS
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
názorů	názor	k1gInPc2
je	být	k5eAaImIp3nS
pasivním	pasivní	k2eAgMnSc7d1
stvořitelem	stvořitel	k1gMnSc7
Svarog	Svaroga	k1gFnPc2
a	a	k8xC
aktivním	aktivní	k2eAgNnSc7d1
Veles	Veles	k1gInSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
nejasná	jasný	k2eNgNnPc1d1
božstva	božstvo	k1gNnPc1
Bělboh	Bělboha	k1gFnPc2
a	a	k8xC
Černoboh	Černoboh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Povesti	Povest	k1gFnSc6
vremenných	vremenný	k2eAgNnPc2d1
let	léto	k1gNnPc2
zní	znět	k5eAaImIp3nS
mýtus	mýtus	k1gInSc1
následovně	následovně	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
podstatou	podstata	k1gFnSc7
stvoření	stvoření	k1gNnSc2
světa	svět	k1gInSc2
je	být	k5eAaImIp3nS
součinnost	součinnost	k1gFnSc4
Boha	bůh	k1gMnSc2
a	a	k8xC
Satana	Satan	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
na	na	k7c6
počátku	počátek	k1gInSc6
oba	dva	k4xCgMnPc1
vznášeli	vznášet	k5eAaImAgMnP
nad	nad	k7c7
nekonečným	konečný	k2eNgInSc7d1
praoceánem	praoceán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
verzí	verze	k1gFnPc2
vytvoří	vytvořit	k5eAaPmIp3nS
Satan	satan	k1gInSc1
zemi	zem	k1gFnSc4
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
vlastní	vlastní	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
jiné	jiný	k2eAgFnSc2d1
na	na	k7c4
příkaz	příkaz	k1gInSc4
Boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
toho	ten	k3xDgNnSc2
schopen	schopen	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
andělé	anděl	k1gMnPc1
jsou	být	k5eAaImIp3nP
příliš	příliš	k6eAd1
lehcí	lehký	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyzvednutí	vyzvednutí	k1gNnSc3
země	zem	k1gFnSc2
z	z	k7c2
vody	voda	k1gFnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podaří	podařit	k5eAaPmIp3nS
až	až	k9
napotřetí	napotřetí	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
ji	on	k3xPp3gFnSc4
vyzvedává	vyzvedávat	k5eAaImIp3nS
ve	v	k7c6
jménu	jméno	k1gNnSc6
Božím	božit	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
hrsti	hrst	k1gFnSc2
písku	písek	k1gInSc2
v	v	k7c6
rukou	ruka	k1gFnPc6
pro	pro	k7c4
Boha	bůh	k1gMnSc4
skryje	skrýt	k5eAaPmIp3nS
další	další	k2eAgFnSc1d1
v	v	k7c6
ústech	ústa	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
však	však	k9
začne	začít	k5eAaPmIp3nS
písek	písek	k1gInSc4
růst	růst	k5eAaImF
<g/>
,	,	kIx,
vykašlává	vykašlávat	k5eAaImIp3nS
ho	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
pustiny	pustina	k1gFnPc1
<g/>
,	,	kIx,
hory	hora	k1gFnPc1
<g/>
,	,	kIx,
skály	skála	k1gFnPc1
a	a	k8xC
bažiny	bažina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
tvoří	tvořit	k5eAaImIp3nS
roviny	rovina	k1gFnPc4
a	a	k8xC
úrodná	úrodný	k2eAgNnPc4d1
pole	pole	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Mýtus	mýtus	k1gInSc1
o	o	k7c4
stvoření	stvoření	k1gNnSc4
světa	svět	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
dvou	dva	k4xCgMnPc2
demiurgů	demiurg	k1gMnPc2
se	se	k3xPyFc4
ke	k	k7c3
Slovanům	Slovan	k1gMnPc3
dostal	dostat	k5eAaPmAgInS
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
Eurasie	Eurasie	k1gFnSc2
<g/>
,	,	kIx,
nejspíš	nejspíš	k9
prostřednictvím	prostřednictvím	k7c2
ugrofinských	ugrofinský	k2eAgInPc2d1
či	či	k8xC
turkotatarských	turkotatarský	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
jiných	jiný	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
mýtus	mýtus	k1gInSc4
dualistický	dualistický	k2eAgInSc4d1
<g/>
,	,	kIx,
ovlivněný	ovlivněný	k2eAgInSc4d1
íránským	íránský	k2eAgMnSc7d1
prostředním	prostřední	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgInPc4d1
mýty	mýtus	k1gInPc4
o	o	k7c4
stvoření	stvoření	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
slovinský	slovinský	k2eAgInSc1d1
o	o	k7c6
kosmickém	kosmický	k2eAgNnSc6d1
vejci	vejce	k1gNnSc6
sneseném	snesený	k2eAgNnSc6d1
božským	božský	k2eAgMnSc7d1
kohoutem	kohout	k1gMnSc7
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgNnSc2,k3yIgNnSc2,k3yQgNnSc2
se	se	k3xPyFc4
vylilo	vylít	k5eAaPmAgNnS
sedm	sedm	k4xCc1
řek	řeka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zúrodnily	zúrodnit	k5eAaPmAgFnP
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
nejspíše	nejspíše	k9
pod	pod	k7c7
vlivem	vliv	k1gInSc7
orfismu	orfismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
slovinském	slovinský	k2eAgNnSc6d1
<g/>
,	,	kIx,
slovenském	slovenský	k2eAgNnSc6d1
a	a	k8xC
podkarpatském	podkarpatský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
existují	existovat	k5eAaImIp3nP
také	také	k9
mýty	mýtus	k1gInPc1
o	o	k7c6
vzniku	vznik	k1gInSc6
světa	svět	k1gInSc2
z	z	k7c2
ohně	oheň	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Antropogonie	Antropogonie	k1gFnSc1
</s>
<s>
Také	také	k9
o	o	k7c6
stvoření	stvoření	k1gNnSc6
člověka	člověk	k1gMnSc2
hovoří	hovořit	k5eAaImIp3nS
Povest	Povest	k1gFnSc4
vremenných	vremenný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
byl	být	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
stvořen	stvořit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
se	se	k3xPyFc4
myl	mýt	k5eAaImAgMnS
v	v	k7c6
lázni	lázeň	k1gFnSc6
a	a	k8xC
zpotil	zpotit	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
i	i	k8xC
otřel	otřít	k5eAaPmAgMnS
se	se	k3xPyFc4
věchtem	věchet	k1gInSc7
a	a	k8xC
hodil	hodit	k5eAaImAgMnS,k5eAaPmAgMnS
jej	on	k3xPp3gInSc4
z	z	k7c2
nebe	nebe	k1gNnSc2
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
spor	spor	k1gInSc1
mezi	mezi	k7c7
Satanem	Satan	k1gMnSc7
a	a	k8xC
Bohem	bůh	k1gMnSc7
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
má	mít	k5eAaImIp3nS
z	z	k7c2
věchtu	věchet	k1gInSc2
stvořit	stvořit	k5eAaPmF
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
stvořil	stvořit	k5eAaPmAgMnS
Satan	satan	k1gInSc4
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
Bůh	bůh	k1gMnSc1
vložil	vložit	k5eAaPmAgMnS
do	do	k7c2
něj	on	k3xPp3gInSc2
duši	duše	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
až	až	k9
člověk	člověk	k1gMnSc1
umře	umřít	k5eAaPmIp3nS
<g/>
,	,	kIx,
tělo	tělo	k1gNnSc1
šlo	jít	k5eAaImAgNnS
do	do	k7c2
země	zem	k1gFnSc2
a	a	k8xC
duše	duše	k1gFnSc2
k	k	k7c3
Bohu	bůh	k1gMnSc3
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
některých	některý	k3yIgInPc2
mýtů	mýtus	k1gInPc2
stvořil	stvořit	k5eAaPmAgMnS
člověka	člověk	k1gMnSc2
Bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
Satan	Satan	k1gMnSc1
ho	on	k3xPp3gInSc4
poplival	poplivat	k5eAaPmAgMnS
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vzniklo	vzniknout	k5eAaPmAgNnS
všechno	všechen	k3xTgNnSc4
lidské	lidský	k2eAgNnSc4d1
trápení	trápení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
se	se	k3xPyFc4
tradovalo	tradovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
člověk	člověk	k1gMnSc1
vznikl	vzniknout	k5eAaPmAgMnS
z	z	k7c2
kapky	kapka	k1gFnSc2
potu	pot	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
spadla	spadnout	k5eAaPmAgFnS
z	z	k7c2
Božího	boží	k2eAgNnSc2d1
čela	čelo	k1gNnSc2
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
byl	být	k5eAaImAgMnS
člověk	člověk	k1gMnSc1
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
pracovat	pracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Rusku	Rusko	k1gNnSc6
byl	být	k5eAaImAgInS
znám	znám	k2eAgInSc1d1
příběh	příběh	k1gInSc1
o	o	k7c6
Rodovi	Roda	k1gMnSc6
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
stále	stále	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
nové	nový	k2eAgMnPc4d1
lidi	člověk	k1gMnPc4
házením	házení	k1gNnSc7
hrud	hrouda	k1gFnPc2
hlíny	hlína	k1gFnSc2
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
motiv	motiv	k1gInSc1
je	být	k5eAaImIp3nS
analogický	analogický	k2eAgInSc1d1
mýtu	mýtus	k1gInSc6
o	o	k7c6
Deukaliónovi	Deukalión	k1gMnSc6
a	a	k8xC
Pyrze	Pyrza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
západních	západní	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
byl	být	k5eAaImAgInS
zas	zas	k6eAd1
znám	znám	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
o	o	k7c6
prvním	první	k4xOgInSc6
člověku	člověk	k1gMnSc3
králi	král	k1gMnSc3
Muži	muž	k1gMnSc3
(	(	kIx(
<g/>
Mandžakovi	Mandžak	k1gMnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
analogický	analogický	k2eAgMnSc1d1
germánskému	germánský	k2eAgMnSc3d1
Mannovi	Mann	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Kosmologie	kosmologie	k1gFnSc1
</s>
<s>
Baba	baba	k1gFnSc1
Jaga	Jaga	k1gFnSc1
</s>
<s>
Ve	v	k7c6
slovanském	slovanský	k2eAgInSc6d1
folklóru	folklór	k1gInSc6
je	být	k5eAaImIp3nS
často	často	k6eAd1
země	země	k1gFnSc1
líčena	líčen	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
spočívající	spočívající	k2eAgFnSc1d1
na	na	k7c6
rybě	ryba	k1gFnSc6
<g/>
,	,	kIx,
velrybě	velryba	k1gFnSc6
či	či	k8xC
několika	několik	k4yIc2
rybách	ryba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
býval	bývat	k5eAaImAgInS
rozdělován	rozdělovat	k5eAaImNgInS
na	na	k7c4
horní	horní	k2eAgFnSc4d1
–	–	k?
nebe	nebe	k1gNnSc2
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc4d1
–	–	k?
zemi	zem	k1gFnSc4
a	a	k8xC
dolní	dolní	k2eAgFnSc4d1
–	–	k?
podsvětí	podsvětí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgInP
světovým	světový	k2eAgInSc7d1
stromem	strom	k1gInSc7
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
je	být	k5eAaImIp3nS
dub	dub	k1gInSc1
<g/>
,	,	kIx,
méně	málo	k6eAd2
často	často	k6eAd1
borovice	borovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vodorovné	vodorovný	k2eAgFnSc6d1
ose	osa	k1gFnSc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
svět	svět	k1gInSc1
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
světové	světový	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemi	zem	k1gFnSc6
ztotožňované	ztotožňovaný	k2eAgFnSc6d1
s	s	k7c7
Mokoš	Mokoš	k1gMnSc1
se	se	k3xPyFc4
dostávalo	dostávat	k5eAaImAgNnS
veliké	veliký	k2eAgFnPc4d1
úcty	úcta	k1gFnPc4
a	a	k8xC
například	například	k6eAd1
tráva	tráva	k1gFnSc1
byla	být	k5eAaImAgFnS
chápána	chápat	k5eAaImNgFnS
jako	jako	k9
její	její	k3xOp3gInPc1
vlasy	vlas	k1gInPc1
<g/>
,	,	kIx,
skály	skála	k1gFnPc1
jako	jako	k8xS,k8xC
její	její	k3xOp3gFnPc1
kosti	kost	k1gFnPc1
a	a	k8xC
řeky	řeka	k1gFnPc1
jako	jako	k8xS,k8xC
její	její	k3xOp3gFnPc4
žíly	žíla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
byla	být	k5eAaImAgFnS
chápána	chápat	k5eAaImNgFnS
jako	jako	k9
matka	matka	k1gFnSc1
a	a	k8xC
nebesa	nebesa	k1gNnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
bohem	bůh	k1gMnSc7
je	být	k5eAaImIp3nS
Svarog	Svarog	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
otec	otec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
nebeská	nebeský	k2eAgNnPc1d1
tělesa	těleso	k1gNnPc1
mají	mít	k5eAaImIp3nP
božskou	božský	k2eAgFnSc4d1
podstatu	podstata	k1gFnSc4
<g/>
,	,	kIx,
Slunce	slunce	k1gNnSc1
bylo	být	k5eAaImAgNnS
chápáno	chápat	k5eAaImNgNnS
jako	jako	k8xC,k8xS
vševidoucí	vševidoucí	k2eAgNnSc1d1
oko	oko	k1gNnSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
okřídlený	okřídlený	k2eAgMnSc1d1
mladík	mladík	k1gMnSc1
s	s	k7c7
křídly	křídlo	k1gNnPc7
nebo	nebo	k8xC
v	v	k7c6
indoevropském	indoevropský	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
jedoucí	jedoucí	k2eAgFnSc2d1
po	po	k7c6
obloze	obloha	k1gFnSc6
v	v	k7c6
zlatém	zlatý	k2eAgInSc6d1
voze	vůz	k1gInSc6
taženém	tažený	k2eAgInSc6d1
bělouši	bělouš	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráno	ráno	k6eAd1
se	se	k3xPyFc4
koupe	koupat	k5eAaImIp3nS
v	v	k7c6
moři	moře	k1gNnSc6
a	a	k8xC
při	při	k7c6
své	svůj	k3xOyFgFnSc6
denní	denní	k2eAgFnSc6d1
pouti	pouť	k1gFnSc6
bojovalo	bojovat	k5eAaImAgNnS
s	s	k7c7
démony	démon	k1gMnPc7
mraků	mrak	k1gInPc2
a	a	k8xC
při	při	k7c6
zatmění	zatmění	k1gNnSc6
bylo	být	k5eAaImAgNnS
ohrožováno	ohrožovat	k5eAaImNgNnS
drakem	drak	k1gInSc7
či	či	k8xC
vlkodlakem	vlkodlak	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
je	být	k5eAaImIp3nS
ztotožňované	ztotožňovaný	k2eAgNnSc1d1
s	s	k7c7
bohem	bůh	k1gMnSc7
Dažbogem	Dažbog	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měsíc	měsíc	k1gInSc1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gMnSc7
mladším	mladý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
<g/>
,	,	kIx,
v	v	k7c6
Rusku	Rusko	k1gNnSc6
někdy	někdy	k6eAd1
sestrou	sestra	k1gFnSc7
<g/>
,	,	kIx,
nejspíš	nejspíš	k9
pod	pod	k7c7
řeckým	řecký	k2eAgInSc7d1
vlivem	vliv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrál	hrát	k5eAaImAgMnS
důležitou	důležitý	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
v	v	k7c6
magii	magie	k1gFnSc6
a	a	k8xC
léčitelství	léčitelství	k1gNnSc6
a	a	k8xC
věřilo	věřit	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
němu	on	k3xPp3gNnSc3
putují	putovat	k5eAaImIp3nP
duše	duše	k1gFnPc4
zemřelých	zemřelý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
tato	tento	k3xDgNnPc1
tělesa	těleso	k1gNnPc1
tradičně	tradičně	k6eAd1
sídlí	sídlet	k5eAaImIp3nP
za	za	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
sestrou	sestra	k1gFnSc7
byla	být	k5eAaImAgFnS
Zora	Zora	k1gFnSc1
<g/>
,	,	kIx,
často	často	k6eAd1
milenka	milenka	k1gFnSc1
jednoho	jeden	k4xCgInSc2
z	z	k7c2
nich	on	k3xPp3gMnPc2
či	či	k8xC
obou	dva	k4xCgNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
byly	být	k5eAaImAgFnP
chápány	chápat	k5eAaImNgFnP
nejčastěji	často	k6eAd3
jako	jako	k8xS,k8xC
děti	dítě	k1gFnPc1
Slunce	slunce	k1gNnSc2
a	a	k8xC
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřilo	věřit	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
hvězdu	hvězda	k1gFnSc4
a	a	k8xC
ta	ten	k3xDgFnSc1
v	v	k7c6
okamžiku	okamžik	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
padá	padat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Podsvětí	podsvětí	k1gNnSc1
zvané	zvaný	k2eAgFnSc2d1
nav	navit	k5eAaImRp2nS
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
ležící	ležící	k2eAgFnSc1d1
za	za	k7c7
mořem	moře	k1gNnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chápáno	chápat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
klidné	klidný	k2eAgNnSc1d1
a	a	k8xC
vlhké	vlhký	k2eAgNnSc1d1
místo	místo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládl	vládnout	k5eAaImAgInS
mu	on	k3xPp3gNnSc3
nejspíš	nejspíš	k9
Veles	Veles	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakožto	jakožto	k8xS
ráj	ráj	k1gInSc1
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
Irij	Irij	k1gInSc1
nebo	nebo	k8xC
ztotožňován	ztotožňovat	k5eAaImNgInS
s	s	k7c7
ostrovem	ostrov	k1gInSc7
Bujan	bujan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pohádkách	pohádka	k1gFnPc6
se	se	k3xPyFc4
často	často	k6eAd1
Onen	onen	k3xDgInSc1
svět	svět	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
spíš	spíš	k9
jako	jako	k9
říše	říše	k1gFnSc1
přízračných	přízračný	k2eAgFnPc2d1
bytostí	bytost	k1gFnPc2
než	než	k8xS
mrtvých	mrtvý	k2eAgFnPc2d1
duší	duše	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
také	také	k9
nazýváno	nazýván	k2eAgNnSc4d1
„	„	k?
<g/>
Třikrát	třikrát	k6eAd1
desáté	desátý	k4xOgNnSc1
království	království	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
leží	ležet	k5eAaImIp3nP
za	za	k7c7
hranicemi	hranice	k1gFnPc7
lidského	lidský	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
hrdiny	hrdina	k1gMnSc2
do	do	k7c2
něj	on	k3xPp3gInSc2
se	se	k3xPyFc4
většinou	většinou	k6eAd1
neobjede	objet	k5eNaPmIp3nS
bez	bez	k7c2
průvodce	průvodce	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
třeba	třeba	k6eAd1
čarodějnice	čarodějnice	k1gFnSc1
<g/>
,	,	kIx,
kůň	kůň	k1gMnSc1
či	či	k8xC
pták	pták	k1gMnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
kouzelného	kouzelný	k2eAgInSc2d1
předmětu	předmět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
motiv	motiv	k1gInSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ovlivněn	ovlivnit	k5eAaPmNgInS
sibiřským	sibiřský	k2eAgInSc7d1
šamanismem	šamanismus	k1gInSc7
a	a	k8xC
průvodce	průvodce	k1gMnSc1
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
funkci	funkce	k1gFnSc4
psychopompa	psychopomp	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zakladatelské	zakladatelský	k2eAgInPc1d1
mýty	mýtus	k1gInPc1
</s>
<s>
Ve	v	k7c6
slovanském	slovanský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
pověstí	pověst	k1gFnPc2
o	o	k7c6
počátcích	počátek	k1gInPc6
kmenů	kmen	k1gInPc2
či	či	k8xC
dynastií	dynastie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vykazují	vykazovat	k5eAaImIp3nP
rysy	rys	k1gInPc4
mýtu	mýtus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
zaznamenány	zaznamenán	k2eAgInPc1d1
středověkými	středověký	k2eAgMnPc7d1
křesťanskými	křesťanský	k2eAgMnPc7d1
kronikáři	kronikář	k1gMnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
dílo	dílo	k1gNnSc1
bylo	být	k5eAaImAgNnS
ovlivněno	ovlivnit	k5eAaPmNgNnS
biblickými	biblický	k2eAgInPc7d1
a	a	k8xC
antickými	antický	k2eAgInPc7d1
motivy	motiv	k1gInPc7
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
objevuje	objevovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
prvků	prvek	k1gInPc2
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
předkřesťanských	předkřesťanský	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
původní	původní	k2eAgFnSc6d1
formě	forma	k1gFnSc6
mohly	moct	k5eAaImAgFnP
mít	mít	k5eAaImF
podobu	podoba	k1gFnSc4
eposu	epos	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Slovo	slovo	k1gNnSc1
o	o	k7c6
pluku	pluk	k1gInSc6
Igorově	Igorův	k2eAgInSc6d1
<g/>
,	,	kIx,
ruské	ruský	k2eAgFnPc4d1
byliny	bylina	k1gFnPc4
a	a	k8xC
jihoslovanské	jihoslovanský	k2eAgFnPc4d1
junácké	junácký	k2eAgFnPc4d1
písně	píseň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
představuje	představovat	k5eAaImIp3nS
zakladatelský	zakladatelský	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
příběh	příběh	k1gInSc1
zaznamenaný	zaznamenaný	k2eAgInSc1d1
Kosmou	Kosma	k1gMnSc7
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
o	o	k7c6
příchodu	příchod	k1gInSc6
bezejmenného	bezejmenný	k2eAgInSc2d1
lidu	lid	k1gInSc2
k	k	k7c3
hoře	hora	k1gFnSc3
Říp	Říp	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
si	se	k3xPyFc3
zvolí	zvolit	k5eAaPmIp3nS
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
vůdci	vůdce	k1gMnSc6
Čechovi	Čech	k1gMnSc6
jméno	jméno	k1gNnSc4
Čechové	Čechové	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
vyprávění	vyprávění	k1gNnSc2
o	o	k7c6
soudci	soudce	k1gMnSc6
Krokovi	Kroek	k1gMnSc6
a	a	k8xC
jeho	jeho	k3xOp3gFnPc6
dcerách	dcera	k1gFnPc6
Kazi	Kazi	k1gFnSc2
<g/>
,	,	kIx,
Tetce	tetka	k1gFnSc6
a	a	k8xC
Libuši	Libuše	k1gFnSc6
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
poslední	poslední	k2eAgFnSc1d1
jmenovaná	jmenovaná	k1gFnSc1
zdědí	zdědit	k5eAaPmIp3nS
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
otci	otec	k1gMnSc6
soudcovskou	soudcovský	k2eAgFnSc7d1
funkci	funkce	k1gFnSc6
a	a	k8xC
nakonec	nakonec	k6eAd1
si	se	k3xPyFc3
bere	brát	k5eAaImIp3nS
za	za	k7c4
manželka	manželka	k1gFnSc1
oráče	oráč	k1gMnSc2
Přemysla	Přemysl	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yQgNnSc7,k3yRgNnSc7,k3yIgNnSc7
zakládá	zakládat	k5eAaImIp3nS
přemyslovskou	přemyslovský	k2eAgFnSc4d1
dynastii	dynastie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
o	o	k7c6
bezejmenné	bezejmenný	k2eAgFnSc6d1
panně	panna	k1gFnSc6
hadačce	hadačka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
si	se	k3xPyFc3
bere	brát	k5eAaImIp3nS
Přemysla	Přemysl	k1gMnSc2
<g/>
,	,	kIx,
už	už	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
zaznamenán	zaznamenat	k5eAaPmNgInS
z	z	k7c2
Kristiánově	Kristiánův	k2eAgFnSc6d1
legendě	legenda	k1gFnSc6
z	z	k7c2
konce	konec	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kosmově	Kosmův	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
ještě	ještě	k9
epizoda	epizoda	k1gFnSc1
o	o	k7c6
Dívčí	dívčí	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
Lucké	lucký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
seznam	seznam	k1gInSc4
legendárních	legendární	k2eAgNnPc2d1
přemyslovských	přemyslovský	k2eAgNnPc2d1
knížat	kníže	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
pověsti	pověst	k1gFnPc1
byly	být	k5eAaImAgFnP
dále	daleko	k6eAd2
přepracovávány	přepracovávat	k5eAaImNgInP
a	a	k8xC
doplňovány	doplňovat	k5eAaImNgInP
v	v	k7c6
dalších	další	k2eAgInPc6d1
dílech	díl	k1gInPc6
<g/>
,	,	kIx,
především	především	k9
Dalimilově	Dalimilův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
<g/>
,	,	kIx,
Hájkově	Hájkův	k2eAgFnSc6d1
kronice	kronika	k1gFnSc6
a	a	k8xC
Starých	Starých	k2eAgFnPc6d1
pověstech	pověst	k1gFnPc6
českých	český	k2eAgMnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Polsku	Polsko	k1gNnSc6
existuje	existovat	k5eAaImIp3nS
o	o	k7c6
počátcích	počátek	k1gInPc6
polského	polský	k2eAgInSc2d1
národa	národ	k1gInSc2
a	a	k8xC
dynastie	dynastie	k1gFnSc2
více	hodně	k6eAd2
verzí	verze	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendární	legendární	k2eAgMnSc1d1
praotec	praotec	k1gMnSc1
Lech	Lech	k1gMnSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
až	až	k9
v	v	k7c6
Kronice	kronika	k1gFnSc6
velkopolské	velkopolský	k2eAgFnSc6d1
z	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
Vincenta	Vincent	k1gMnSc2
Kadlubka	Kadlubek	k1gMnSc2
z	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vypráví	vyprávět	k5eAaImIp3nS
o	o	k7c4
Krakovi	krakův	k2eAgMnPc1d1
<g/>
,	,	kIx,
drakobijci	drakobijce	k1gMnSc3
zakladateli	zakladatel	k1gMnSc3
Krakova	Krakov	k1gInSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
stejnojmenném	stejnojmenný	k2eAgInSc6d1
synovi	syn	k1gMnSc3
a	a	k8xC
dceři	dcera	k1gFnSc3
Wandě	Wanda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
poté	poté	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
knížete	kníže	k1gNnSc4wR
Popela	popel	k1gInSc2
<g/>
,	,	kIx,
uváděného	uváděný	k2eAgInSc2d1
již	již	k9
Kronikou	kronika	k1gFnSc7
Galla	Gall	k1gMnSc2
Anonyma	anonym	k1gMnSc2
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
po	po	k7c6
jehož	jehož	k3xOyRp3gFnSc6
smrti	smrt	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
trůn	trůn	k1gInSc4
Zemomysl	Zemomysl	k1gInSc1
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
oráče	oráč	k1gMnSc2
Piasta	Piasta	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
dynastie	dynastie	k1gFnSc2
Piastovců	Piastovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
tyto	tento	k3xDgInPc1
příběhy	příběh	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
některé	některý	k3yIgInPc4
mytické	mytický	k2eAgInPc4d1
motivy	motiv	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
oráče	oráč	k1gMnSc2
–	–	k?
zakladatel	zakladatel	k1gMnSc1
dynastie	dynastie	k1gFnSc2
jako	jako	k8xC,k8xS
v	v	k7c6
českých	český	k2eAgFnPc6d1
pověstech	pověst	k1gFnPc6
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
spíše	spíše	k9
podobu	podoba	k1gFnSc4
dvorské	dvorský	k2eAgFnSc2d1
pověsti	pověst	k1gFnSc2
ovlivněné	ovlivněný	k2eAgInPc1d1
cizími	cizí	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
ze	z	k7c2
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
doložena	doložit	k5eAaPmNgFnS
ruská	ruský	k2eAgFnSc1d1
pověst	pověst	k1gFnSc1
o	o	k7c6
založení	založení	k1gNnSc6
Kyjeva	Kyjev	k1gInSc2
třemi	tři	k4xCgNnPc7
bratry	bratr	k1gMnPc7
jménem	jméno	k1gNnSc7
Kyj	kyj	k1gInSc1
<g/>
,	,	kIx,
Šček	Ščeka	k1gFnPc2
a	a	k8xC
Choriv	Choriva	k1gFnPc2
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
jejich	jejich	k3xOp3gFnSc7
sestrou	sestra	k1gFnSc7
Lybeď	Lybeď	k1gFnSc7
<g/>
´	´	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Jaana	Jaan	k1gMnSc2
Puhvela	Puhvel	k1gMnSc2
jsou	být	k5eAaImIp3nP
mytickým	mytický	k2eAgInSc7d1
eposem	epos	k1gInSc7
také	také	k9
dějiny	dějiny	k1gFnPc1
Kyjevské	kyjevský	k2eAgFnSc2d1
Rusi	Rus	k1gFnSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
988	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
postavy	postava	k1gFnPc1
jako	jako	k8xC,k8xS
Rurik	Rurik	k1gMnSc1
<g/>
,	,	kIx,
Oleg	Oleg	k1gMnSc1
<g/>
,	,	kIx,
Igor	Igor	k1gMnSc1
a	a	k8xC
Svjatoslav	Svjatoslava	k1gFnPc2
nejsou	být	k5eNaImIp3nP
historické	historický	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mýtické	mýtický	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Slovanští	slovanštit	k5eAaImIp3nP
démoni	démon	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
bohů	bůh	k1gMnPc2
znali	znát	k5eAaImAgMnP
pohanští	pohanský	k2eAgMnPc1d1
Slované	Slovan	k1gMnPc1
i	i	k9
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
nižších	nízký	k2eAgFnPc2d2
bytostí	bytost	k1gFnPc2
<g/>
,	,	kIx,
nazývány	nazýván	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
většinou	většinou	k6eAd1
slovem	slovem	k6eAd1
běs	běs	k1gInSc1
či	či	k8xC
div	div	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
indickým	indický	k2eAgMnSc7d1
déva	dév	k2eAgMnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgNnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
též	též	k9
ctili	ctít	k5eAaImAgMnP
a	a	k8xC
obětovali	obětovat	k5eAaBmAgMnP
jim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ty	ten	k3xDgMnPc4
patřili	patřit	k5eAaImAgMnP
například	například	k6eAd1
beregině	beregina	k1gFnSc3
<g/>
,	,	kIx,
víly	víla	k1gFnPc1
či	či	k8xC
upíři	upír	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
patří	patřit	k5eAaImIp3nS
zmiňované	zmiňovaný	k2eAgFnPc4d1
víly	víla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
různých	různý	k2eAgNnPc6d1
podáních	podání	k1gNnPc6
existují	existovat	k5eAaImIp3nP
víly	víla	k1gFnPc1
lesní	lesní	k2eAgFnPc1d1
<g/>
,	,	kIx,
vzdušné	vzdušný	k2eAgFnPc1d1
<g/>
,	,	kIx,
horské	horský	k2eAgFnPc1d1
a	a	k8xC
také	také	k9
víly	víla	k1gFnPc4
zlé	zlá	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
další	další	k2eAgFnPc1d1
ženské	ženský	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
jim	on	k3xPp3gMnPc3
podobné	podobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
především	především	k9
rusalky	rusalka	k1gFnPc4
<g/>
,	,	kIx,
divé	divý	k2eAgFnPc4d1
ženy	žena	k1gFnPc4
nebo	nebo	k8xC
divoženky	divoženka	k1gFnPc4
doprovázené	doprovázený	k2eAgFnPc4d1
divými	divý	k2eAgMnPc7d1
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženské	ženský	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
byly	být	k5eAaImAgFnP
spojovány	spojovat	k5eAaImNgFnP
také	také	k9
s	s	k7c7
osudem	osud	k1gInSc7
a	a	k8xC
časem	čas	k1gInSc7
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
například	například	k6eAd1
Rožanice	Rožanice	k1gFnSc1
čili	čili	k8xC
sudičky	sudička	k1gFnPc1
nebo	nebo	k8xC
polednice	polednice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známi	znám	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
také	také	k9
duchové	duch	k1gMnPc1
lesní	lesní	k2eAgMnPc1d1
<g/>
,	,	kIx,
zvaní	zvaný	k2eAgMnPc1d1
lešijové	lešij	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
polní	polní	k2eAgMnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
žithola	žithola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voda	voda	k1gFnSc1
byla	být	k5eAaImAgFnS
obývána	obývat	k5eAaImNgFnS
vodníky	vodník	k1gMnPc7
<g/>
,	,	kIx,
oheň	oheň	k1gInSc1
duchem	duch	k1gMnSc7
jménem	jméno	k1gNnSc7
ovinnik	ovinnik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
také	také	k9
skupina	skupina	k1gFnSc1
domácích	domácí	k2eAgMnPc2d1
duchů	duch	k1gMnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
domovoj	domovoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
západních	západní	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
byly	být	k5eAaImAgFnP
známy	znám	k2eAgFnPc1d1
malé	malý	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
jako	jako	k9
jsou	být	k5eAaImIp3nP
ludkové	ludkový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
duchů	duch	k1gMnPc2
bylo	být	k5eAaImAgNnS
také	také	k9
spojováno	spojovat	k5eAaImNgNnS
s	s	k7c7
mrtvými	mrtvý	k2eAgInPc7d1
nebo	nebo	k8xC
jim	on	k3xPp3gFnPc3
byly	být	k5eAaImAgFnP
přisuzovány	přisuzovat	k5eAaImNgFnP
démonické	démonický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gMnPc4
například	například	k6eAd1
Baba	baba	k1gFnSc1
Jaga	Jaga	k1gFnSc1
<g/>
,	,	kIx,
Kostěj	Kostěj	k1gMnSc1
<g/>
,	,	kIx,
vlkodlak	vlkodlak	k1gMnSc1
<g/>
,	,	kIx,
čert	čert	k1gMnSc1
nebo	nebo	k8xC
upír	upír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Bohové	bůh	k1gMnPc1
</s>
<s>
Moderní	moderní	k2eAgInSc1d1
idol	idol	k1gInSc1
Velese	Velese	k1gFnSc2
na	na	k7c6
Velízu	Velíz	k1gInSc6
<g/>
,	,	kIx,
bůh	bůh	k1gMnSc1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
země	zem	k1gFnSc2
a	a	k8xC
podsvětí	podsvětí	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc4
slovanských	slovanský	k2eAgInPc2d1
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Písemné	písemný	k2eAgInPc1d1
záznamy	záznam	k1gInPc1
o	o	k7c6
slovanské	slovanský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
neuvádí	uvádět	k5eNaImIp3nS
jednotný	jednotný	k2eAgInSc4d1
panteon	panteon	k1gInSc4
<g/>
,	,	kIx,
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
mluvit	mluvit	k5eAaImF
minimálně	minimálně	k6eAd1
o	o	k7c6
dvou	dva	k4xCgInPc6
okruzích	okruh	k1gInPc6
<g/>
,	,	kIx,
jednom	jeden	k4xCgNnSc6
ruském	ruský	k2eAgInSc6d1
a	a	k8xC
druhém	druhý	k4xOgInSc6
polabském	polabský	k2eAgMnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
badatelé	badatel	k1gMnPc1
zcela	zcela	k6eAd1
odmítají	odmítat	k5eAaImIp3nP
existenci	existence	k1gFnSc4
jednotného	jednotný	k2eAgInSc2d1
slovanského	slovanský	k2eAgInSc2d1
panteonu	panteon	k1gInSc2
a	a	k8xC
mytologie	mytologie	k1gFnSc2
<g/>
,	,	kIx,
spíše	spíše	k9
se	se	k3xPyFc4
však	však	k9
předpokládá	předpokládat	k5eAaImIp3nS
že	že	k8xS
jednotná	jednotný	k2eAgFnSc1d1
archaická	archaický	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
u	u	k7c2
Slovanů	Slovan	k1gInPc2
existovala	existovat	k5eAaImAgFnS
až	až	k9
do	do	k7c2
počátku	počátek	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Slovanské	slovanský	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
„	„	k?
<g/>
bůh	bůh	k1gMnSc1
<g/>
“	“	k?
souvisí	souviset	k5eAaImIp3nS
se	s	k7c7
sanskrtským	sanskrtský	k2eAgInSc7d1
bhaga	bhag	k1gMnSc2
<g/>
,	,	kIx,
„	„	k?
<g/>
rozdělující	rozdělující	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
dárce	dárce	k1gMnSc1
dobra	dobro	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
dokládají	dokládat	k5eAaImIp3nP
že	že	k8xS
Slované	Slovan	k1gMnPc1
už	už	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
své	svůj	k3xOyFgFnSc2
expanze	expanze	k1gFnSc2
ctili	ctít	k5eAaImAgMnP
bohy	bůh	k1gMnPc7
v	v	k7c6
lidské	lidský	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
všech	všecek	k3xTgInPc2
Slovanů	Slovan	k1gInPc2
je	být	k5eAaImIp3nS
předpokládána	předpokládán	k2eAgFnSc1d1
víra	víra	k1gFnSc1
v	v	k7c4
Peruna	Perun	k1gMnSc4
<g/>
,	,	kIx,
boha	bůh	k1gMnSc4
hromu	hrom	k1gInSc2
a	a	k8xC
blesku	blesk	k1gInSc2
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
známý	známý	k2eAgInSc1d1
také	také	k9
nejspíš	nejspíš	k9
byl	být	k5eAaImAgMnS
chtonický	chtonický	k2eAgMnSc1d1
bůh	bůh	k1gMnSc1
Veles	Veles	k1gMnSc1
spojovaný	spojovaný	k2eAgInSc4d1
se	s	k7c7
stády	stádo	k1gNnPc7
<g/>
,	,	kIx,
bohatstvím	bohatství	k1gNnSc7
a	a	k8xC
magií	magie	k1gFnSc7
a	a	k8xC
Mokoš	Mokoš	k1gFnSc1
<g/>
,	,	kIx,
bohyně	bohyně	k1gFnSc1
vody	voda	k1gFnSc2
a	a	k8xC
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perun	perun	k1gInSc1
společně	společně	k6eAd1
s	s	k7c7
Velesem	Veles	k1gInSc7
však	však	k9
představovali	představovat	k5eAaImAgMnP
hlavní	hlavní	k2eAgNnSc4d1
božstvo	božstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgInPc1d1
prameny	pramen	k1gInPc1
pak	pak	k6eAd1
uvádí	uvádět	k5eAaImIp3nP
nejvyššího	vysoký	k2eAgMnSc4d3
boha-stvořitele	boha-stvořitel	k1gMnSc4
Svaroga	Svarog	k1gMnSc4
<g/>
,	,	kIx,
boha	bůh	k1gMnSc2
slunce	slunce	k1gNnSc2
Dažboga	Dažbog	k1gMnSc2
a	a	k8xC
několik	několik	k4yIc1
dalších	další	k2eAgNnPc2d1
božstev	božstvo	k1gNnPc2
s	s	k7c7
více	hodně	k6eAd2
či	či	k8xC
méně	málo	k6eAd2
nejasnou	jasný	k2eNgFnSc7d1
funkcí	funkce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozici	pozice	k1gFnSc6
stvořitele	stvořitel	k1gMnSc2
podle	podle	k7c2
některých	některý	k3yIgFnPc2
teorií	teorie	k1gFnPc2
nahrazuje	nahrazovat	k5eAaImIp3nS
Svaroga	Svaroga	k1gFnSc1
Rod	rod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
polabských	polabský	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
jsou	být	k5eAaImIp3nP
známa	znám	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
především	především	k9
hlavních	hlavní	k2eAgNnPc2d1
kmenových	kmenový	k2eAgNnPc2d1
božstev	božstvo	k1gNnPc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
prameny	pramen	k1gInPc1
zmiňují	zmiňovat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgInPc1d1
idoly	idol	k1gInPc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
bez	bez	k7c2
udání	udání	k1gNnSc2
jména	jméno	k1gNnSc2
i	i	k8xC
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
jména	jméno	k1gNnPc4
spíše	spíše	k9
než	než	k8xS
teonyma	teonyma	k1gFnSc1
připomínají	připomínat	k5eAaImIp3nP
přízviska	přízvisko	k1gNnPc1
a	a	k8xC
tituly	titul	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pod	pod	k7c7
více	hodně	k6eAd2
jmény	jméno	k1gNnPc7
lze	lze	k6eAd1
najít	najít	k5eAaPmF
jedno	jeden	k4xCgNnSc4
božstvo	božstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
patří	patřit	k5eAaImIp3nS
Svantovít	Svantovít	k1gMnSc1
ctěný	ctěný	k2eAgMnSc1d1
Rány	Rána	k1gFnPc4
<g/>
,	,	kIx,
Svarožic	Svarožic	k1gMnSc1
ctěný	ctěný	k2eAgInSc4d1
Ratary	Ratar	k1gInPc4
<g/>
,	,	kIx,
Triglav	Triglav	k1gInSc1
ctěný	ctěný	k2eAgInSc1d1
Pomořany	Pomořan	k1gMnPc7
a	a	k8xC
Jarovít	Jarovít	k1gMnSc1
ctěný	ctěný	k2eAgMnSc1d1
ve	v	k7c6
Wolgastu	Wolgast	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Helmondovy	Helmondův	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
znali	znát	k5eAaImAgMnP
také	také	k9
pasivní	pasivní	k2eAgNnSc4d1
nebeské	nebeský	k2eAgNnSc4d1
božstvo	božstvo	k1gNnSc4
<g/>
,	,	kIx,
nejspíš	nejspíš	k9
totožné	totožný	k2eAgFnPc1d1
se	se	k3xPyFc4
Svarogem	Svarog	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítnuta	odmítnut	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
teorie	teorie	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
bohové	bůh	k1gMnPc1
polabských	polabský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
jsou	být	k5eAaImIp3nP
zbožštěná	zbožštěný	k2eAgNnPc1d1
knížata	kníže	k1gNnPc1
a	a	k8xC
vojevůdci	vojevůdce	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nP
se	se	k3xPyFc4
další	další	k2eAgNnPc1d1
slovanská	slovanský	k2eAgNnPc1d1
božstva	božstvo	k1gNnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
existence	existence	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
většinou	většinou	k6eAd1
zpochybňována	zpochybňovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
o	o	k7c4
postavy	postava	k1gFnPc4
uváděné	uváděný	k2eAgFnPc4d1
v	v	k7c6
polských	polský	k2eAgFnPc6d1
raně	raně	k6eAd1
novověkých	novověký	k2eAgFnPc6d1
kronikách	kronika	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
však	však	k9
podle	podle	k7c2
některých	některý	k3yIgInPc2
názorů	názor	k1gInPc2
skutečně	skutečně	k6eAd1
jsou	být	k5eAaImIp3nP
reliktem	relikt	k1gInSc7
archaické	archaický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
vymyšlené	vymyšlený	k2eAgMnPc4d1
bohy	bůh	k1gMnPc4
uváděné	uváděný	k2eAgMnPc4d1
v	v	k7c6
lužickosrbských	lužickosrbský	k2eAgFnPc6d1
kronikách	kronika	k1gFnPc6
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Flins	Flins	k1gInSc4
a	a	k8xC
v	v	k7c6
kronikách	kronika	k1gFnPc6
českých	český	k2eAgFnPc6d1
jako	jako	k8xS,k8xC
Vesna	Vesna	k1gFnSc1
nebo	nebo	k8xC
Devana	Devana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některými	některý	k3yIgFnPc7
badateli	badatel	k1gMnPc7
byly	být	k5eAaImAgFnP
za	za	k7c2
božstva	božstvo	k1gNnSc2
považovány	považován	k2eAgInPc1d1
svátky	svátek	k1gInPc1
či	či	k8xC
při	při	k7c6
nich	on	k3xPp3gFnPc6
používané	používaný	k2eAgFnPc1d1
figury	figura	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Jarilo	Jarila	k1gFnSc5
nebo	nebo	k8xC
Kračun	Kračuno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Figury	figura	k1gFnSc2
jarních	jarní	k2eAgFnPc2d1
slavností	slavnost	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Jarilo	Jarila	k1gFnSc5
<g/>
,	,	kIx,
však	však	k9
skutečně	skutečně	k6eAd1
původně	původně	k6eAd1
byly	být	k5eAaImAgFnP
zpodobněním	zpodobnění	k1gNnSc7
vegetačního	vegetační	k2eAgNnSc2d1
božstva	božstvo	k1gNnSc2
a	a	k8xC
bývá	bývat	k5eAaImIp3nS
připouštěna	připouštěn	k2eAgFnSc1d1
existence	existence	k1gFnSc1
bohyň	bohyně	k1gFnPc2
Lady	Lada	k1gFnSc2
a	a	k8xC
Morany	Morana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
badatelé	badatel	k1gMnPc1
také	také	k9
předpokládají	předpokládat	k5eAaImIp3nP
u	u	k7c2
Slovanů	Slovan	k1gInPc2
henoteismus	henoteismus	k1gInSc1
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
Peruna	Perun	k1gMnSc4
chápaného	chápaný	k2eAgMnSc4d1
jako	jako	k8xS,k8xC
nejvyšší	vysoký	k2eAgNnSc4d3
božstvo	božstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
by	by	kYmCp3nS
pak	pak	k6eAd1
znamenalo	znamenat	k5eAaImAgNnS
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
kultu	kult	k1gInSc6
přiblížili	přiblížit	k5eAaPmAgMnP
sousedním	sousední	k2eAgInPc3d1
ugrofinským	ugrofinský	k2eAgInPc3d1
a	a	k8xC
turkotatarským	turkotatarský	k2eAgInPc3d1
národům	národ	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
Perunovi	Perunův	k2eAgMnPc1d1
zcela	zcela	k6eAd1
určitě	určitě	k6eAd1
konkuroval	konkurovat	k5eAaImAgMnS
Veles	Veles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Trojfunkční	trojfunkční	k2eAgFnSc1d1
hypotéza	hypotéza	k1gFnSc1
</s>
<s>
Slovanská	slovanský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
byla	být	k5eAaImAgFnS
zkoumána	zkoumat	k5eAaImNgFnS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
trojfunkční	trojfunkční	k2eAgFnSc2d1
hypotézy	hypotéza	k1gFnSc2
George	George	k1gFnSc1
Dumézila	Dumézila	k1gFnSc1
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
formulována	formulovat	k5eAaImNgFnS
v	v	k7c6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
předpokládá	předpokládat	k5eAaImIp3nS
u	u	k7c2
Indoevropanů	Indoevropan	k1gMnPc2
„	„	k?
<g/>
univerzální	univerzální	k2eAgInSc4d1
klasifikační	klasifikační	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
“	“	k?
sestávající	sestávající	k2eAgMnSc1d1
ze	z	k7c2
tří	tři	k4xCgFnPc2
funkcí	funkce	k1gFnPc2
<g/>
:	:	kIx,
svrchovanosti	svrchovanost	k1gFnSc2
<g/>
,	,	kIx,
války	válka	k1gFnSc2
a	a	k8xC
plodnosti	plodnost	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
jak	jak	k6eAd1
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
tak	tak	k8xS,k8xC
náboženské	náboženský	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnSc1
svrchovanosti	svrchovanost	k1gFnSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
konceptu	koncept	k1gInSc6
zahrnuje	zahrnovat	k5eAaImIp3nS
náboženství	náboženství	k1gNnSc4
<g/>
,	,	kIx,
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
magii	magie	k1gFnSc4
a	a	k8xC
právo	právo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnSc1
této	tento	k3xDgFnSc2
hypotézy	hypotéza	k1gFnSc2
na	na	k7c4
slovanskou	slovanský	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
je	být	k5eAaImIp3nS
však	však	k9
problematizována	problematizován	k2eAgFnSc1d1
její	její	k3xOp3gFnSc7
odlišnou	odlišný	k2eAgFnSc7d1
sociální	sociální	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výše	výše	k1gFnSc1
zmíněné	zmíněný	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
v	v	k7c6
Dumézilově	Dumézilův	k2eAgInSc6d1
konceptu	koncept	k1gInSc6
odpovídají	odpovídat	k5eAaImIp3nP
složkám	složka	k1gFnPc3
kněží	kněz	k1gMnPc2
<g/>
,	,	kIx,
válečníků	válečník	k1gMnPc2
a	a	k8xC
zemědělců	zemědělec	k1gMnPc2
vyjádřenou	vyjádřený	k2eAgFnSc4d1
například	například	k6eAd1
indickými	indický	k2eAgFnPc7d1
varnami	varna	k1gFnPc7
bráhmanů	bráhman	k1gMnPc2
<g/>
,	,	kIx,
kšatrijů	kšatrij	k1gMnPc2
a	a	k8xC
vaišjů	vaišj	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
Slovanů	Slovan	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
u	u	k7c2
Germánů	Germán	k1gMnPc2
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
figurovala	figurovat	k5eAaImAgFnS
trojice	trojice	k1gFnSc1
šlechta-svobodní	šlechta-svobodní	k2eAgFnSc4d1
zemědělci-otroci	zemědělci-otroce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
Polabských	polabský	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
<g/>
,	,	kIx,
u	u	k7c2
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
je	být	k5eAaImIp3nS
doložena	doložen	k2eAgFnSc1d1
kněžská	kněžský	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
první	první	k4xOgFnPc1
dvě	dva	k4xCgFnPc1
funkce	funkce	k1gFnPc1
ve	v	k7c6
slovanské	slovanský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
splývaly	splývat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ojedinělým	ojedinělý	k2eAgInSc7d1
dokladem	doklad	k1gInSc7
klasické	klasický	k2eAgFnSc2d1
trojné	trojný	k2eAgFnSc2d1
koncepce	koncepce	k1gFnSc2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
ruská	ruský	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
Golubinaja	Golubinaja	k1gFnSc1
kniga	kniga	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
přisuzován	přisuzován	k2eAgMnSc1d1
různým	různý	k2eAgFnPc3d1
společenským	společenský	k2eAgFnPc3d1
vrstvám	vrstva	k1gFnPc3
původ	původ	k1gInSc4
z	z	k7c2
různých	různý	k2eAgFnPc2d1
částí	část	k1gFnPc2
prvního	první	k4xOgMnSc4
člověka	člověk	k1gMnSc4
Adama	Adam	k1gMnSc4
<g/>
,	,	kIx,
carům	car	k1gMnPc3
z	z	k7c2
hlavy	hlava	k1gFnSc2
<g/>
,	,	kIx,
knížatům-bojarům	knížatům-bojar	k1gMnPc3
z	z	k7c2
ostatků	ostatek	k1gInPc2
a	a	k8xC
rolníkům	rolník	k1gMnPc3
z	z	k7c2
kolene	kolen	k1gMnSc5
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
navíc	navíc	k6eAd1
vykazuje	vykazovat	k5eAaImIp3nS
silnou	silný	k2eAgFnSc4d1
podobnost	podobnost	k1gFnSc4
s	s	k7c7
rgvédským	rgvédský	k2eAgNnSc7d1
podáním	podání	k1gNnSc7
o	o	k7c6
vzniku	vznik	k1gInSc6
lidstva	lidstvo	k1gNnSc2
z	z	k7c2
Puruši	Puruch	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
folklórní	folklórní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
vzniklé	vzniklý	k2eAgNnSc4d1
v	v	k7c4
křesťanské	křesťanský	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
doložené	doložená	k1gFnPc4
až	až	k9
v	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
o	o	k7c4
dědictví	dědictví	k1gNnSc4
společných	společný	k2eAgInPc2d1
indoevropských	indoevropský	k2eAgInPc2d1
kořenů	kořen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
odlišností	odlišnost	k1gFnPc2
v	v	k7c6
sociální	sociální	k2eAgFnSc6d1
struktuře	struktura	k1gFnSc6
je	být	k5eAaImIp3nS
však	však	k9
aplikace	aplikace	k1gFnSc1
trojfunkční	trojfunkční	k2eAgFnSc2d1
hypotézy	hypotéza	k1gFnSc2
ztížena	ztížit	k5eAaPmNgFnS
nedostatkem	nedostatek	k1gInSc7
pramenů	pramen	k1gInPc2
o	o	k7c6
slovanské	slovanský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkce	funkce	k1gFnSc1
božstev	božstvo	k1gNnPc2
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
předmětem	předmět	k1gInSc7
spekulací	spekulace	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
bohů	bůh	k1gMnPc2
Polabských	polabský	k2eAgMnPc2d1
Slovanů	Slovan	k1gMnPc2
zase	zase	k9
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
kumulaci	kumulace	k1gFnSc3
mnoha	mnoho	k4c2
různých	různý	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Svantovít	Svantovít	k1gMnSc1
nese	nést	k5eAaImIp3nS
rysy	rys	k1gInPc4
božstva	božstvo	k1gNnSc2
svrchovanosti	svrchovanost	k1gFnSc2
<g/>
,	,	kIx,
války	válka	k1gFnSc2
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k9
plodnosti	plodnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
nedostatek	nedostatek	k1gInSc4
pramenů	pramen	k1gInPc2
se	s	k7c7
slovanskou	slovanský	k2eAgFnSc7d1
mytologií	mytologie	k1gFnSc7
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
pracích	prak	k1gInPc6
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
výjimky	výjimka	k1gFnPc4
<g/>
,	,	kIx,
nezabýval	zabývat	k5eNaImAgMnS
ani	ani	k8xC
Georges	Georges	k1gMnSc1
Dumézil	Dumézil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upozornil	upozornit	k5eAaPmAgMnS
však	však	k9
na	na	k7c4
nápadnou	nápadný	k2eAgFnSc4d1
shodu	shoda	k1gFnSc4
„	„	k?
<g/>
starších	starý	k2eAgMnPc2d2
bohatýrů	bohatýr	k1gMnPc2
<g/>
"	"	kIx"
ruských	ruský	k2eAgFnPc2d1
bylin	bylina	k1gFnPc2
<g/>
:	:	kIx,
Volcha	Volch	k1gMnSc2
Vseslajeviče	Vseslajevič	k1gMnSc2
<g/>
,	,	kIx,
Svjatogora	Svjatogor	k1gMnSc2
Kolyvaniče	Kolyvaniče	k1gMnSc2
a	a	k8xC
Mikuly	Mikula	k1gMnSc2
Seljanoviče	Seljanovič	k1gMnSc2
s	s	k7c7
třemi	tři	k4xCgFnPc7
indoevropskými	indoevropský	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
výše	výše	k1gFnSc2,k1gFnSc2wB
zmíněné	zmíněný	k2eAgInPc1d1
problémy	problém	k1gInPc1
zpochybnili	zpochybnit	k5eAaPmAgMnP
vhodnost	vhodnost	k1gFnSc4
aplikace	aplikace	k1gFnSc2
trojfunkční	trojfunkční	k2eAgFnSc2d1
hypotézy	hypotéza	k1gFnSc2
na	na	k7c4
slovanskou	slovanský	k2eAgFnSc4d1
mytologii	mytologie	k1gFnSc4
badatelé	badatel	k1gMnPc1
jako	jako	k9
Zdeněk	Zdeněk	k1gMnSc1
Váňa	Váňa	k1gMnSc1
nebo	nebo	k8xC
Naďa	Naďa	k1gFnSc1
a	a	k8xC
Martin	Martin	k1gMnSc1
Profantovi	Profant	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Váňa	Váňa	k1gMnSc1
také	také	k9
spíše	spíše	k9
než	než	k8xS
na	na	k7c4
trojnou	trojný	k2eAgFnSc4d1
koncepci	koncepce	k1gFnSc4
usuzuje	usuzovat	k5eAaImIp3nS
na	na	k7c4
dvojnou	dvojný	k2eAgFnSc4d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jedna	jeden	k4xCgFnSc1
funkce	funkce	k1gFnSc1
první	první	k4xOgFnSc2
by	by	kYmCp3nP
slučovala	slučovat	k5eAaImAgFnS
válku	válka	k1gFnSc4
se	s	k7c7
suverenitou	suverenita	k1gFnSc7
a	a	k8xC
byla	být	k5eAaImAgFnS
by	by	kYmCp3nP
reprezentována	reprezentovat	k5eAaImNgFnS
Perunem	perun	k1gInSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
druhá	druhý	k4xOgFnSc1
funkce	funkce	k1gFnSc1
by	by	kYmCp3nS
slučovala	slučovat	k5eAaImAgFnS
magií	magie	k1gFnSc7
s	s	k7c7
plodností	plodnost	k1gFnSc7
a	a	k8xC
byla	být	k5eAaImAgFnS
by	by	kYmCp3nP
reprezentována	reprezentovat	k5eAaImNgFnS
Velesem	Veles	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
Slovanů	Slovan	k1gMnPc2
nelze	lze	k6eNd1
také	také	k9
nalézt	nalézt	k5eAaBmF,k5eAaPmF
trojici	trojice	k1gFnSc4
božstev	božstvo	k1gNnPc2
zastupující	zastupující	k2eAgNnSc1d1
všechny	všechen	k3xTgFnPc4
funkce	funkce	k1gFnPc4
za	za	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
jsou	být	k5eAaImIp3nP
považována	považován	k2eAgFnSc1d1
římská	římský	k2eAgFnSc1d1
archaická	archaický	k2eAgFnSc1d1
triáda	triáda	k1gFnSc1
<g/>
:	:	kIx,
Jupiter	Jupiter	k1gMnSc1
<g/>
,	,	kIx,
Mars	Mars	k1gMnSc1
<g/>
,	,	kIx,
Quirinus	Quirinus	k1gMnSc1
nebo	nebo	k8xC
božstva	božstvo	k1gNnPc4
ctěná	ctěný	k2eAgNnPc4d1
v	v	k7c6
pruské	pruský	k2eAgFnSc6d1
Romuvě	Romuva	k1gFnSc6
<g/>
:	:	kIx,
Patollo	Patollo	k1gNnSc1
<g/>
,	,	kIx,
Perkun	Perkun	k1gNnSc1
<g/>
,	,	kIx,
Potrimpo	Potrimpa	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
tak	tak	k6eAd1
interpretovat	interpretovat	k5eAaBmF
trojici	trojice	k1gFnSc3
Rujevít	Rujevít	k1gMnSc1
<g/>
,	,	kIx,
Porevít	Porevít	k1gMnSc1
a	a	k8xC
Porenut	Porenut	k2eAgMnSc1d1
ctěnou	ctěný	k2eAgFnSc7d1
v	v	k7c6
rujánské	rujánský	k2eAgFnSc6d1
Korenici	Korenice	k1gFnSc6
<g/>
,	,	kIx,
nejasný	jasný	k2eNgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
význam	význam	k1gInSc1
zmínky	zmínka	k1gFnSc2
o	o	k7c6
ctění	ctění	k1gNnSc6
severské	severský	k2eAgFnSc2d1
trojice	trojice	k1gFnSc2
Ódin	Ódin	k1gMnSc1
<g/>
,	,	kIx,
Thór	Thór	k1gMnSc1
a	a	k8xC
Freya	Freya	k1gMnSc1
Lutici	Lutik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
také	také	k9
pokusy	pokus	k1gInPc1
v	v	k7c6
rámci	rámec	k1gInSc6
trojfunkční	trojfunkční	k2eAgFnSc2d1
hypotézy	hypotéza	k1gFnSc2
interpretovat	interpretovat	k5eAaBmF
zbručský	zbručský	k2eAgInSc4d1
idol	idol	k1gInSc4
a	a	k8xC
boha	bůh	k1gMnSc2
Trihlava	Trihlava	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
funkcí	funkce	k1gFnPc2
</s>
<s>
Mezi	mezi	k7c7
prvními	první	k4xOgMnPc7
badateli	badatel	k1gMnPc7
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
aplikovat	aplikovat	k5eAaBmF
trojfunkční	trojfunkční	k2eAgFnSc4d1
hypotézu	hypotéza	k1gFnSc4
na	na	k7c4
slovanskou	slovanský	k2eAgFnSc4d1
mytologii	mytologie	k1gFnSc4
figuroval	figurovat	k5eAaImAgMnS
především	především	k9
Vladimír	Vladimír	k1gMnSc1
Toporov	Toporov	k1gInSc1
a	a	k8xC
Vjačeslav	Vjačeslav	k1gMnSc1
Ivanov	Ivanov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
jako	jako	k8xC,k8xS
božstvo	božstvo	k1gNnSc4
první	první	k4xOgFnSc2
funkce	funkce	k1gFnSc2
chápou	chápat	k5eAaImIp3nP
Striboga	Stribog	k1gMnSc4
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yQgMnSc4,k3yIgMnSc4
na	na	k7c6
základě	základ	k1gInSc6
jedné	jeden	k4xCgFnSc2
z	z	k7c2
etymologií	etymologie	k1gFnPc2
vykládají	vykládat	k5eAaImIp3nP
jako	jako	k9
„	„	k?
<g/>
boha	bůh	k1gMnSc4
otců	otec	k1gMnPc2
<g/>
,	,	kIx,
boha	bůh	k1gMnSc4
předků	předek	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
božstvo	božstvo	k1gNnSc4
druhé	druhý	k4xOgFnSc2
funkce	funkce	k1gFnSc2
pak	pak	k6eAd1
identifikovali	identifikovat	k5eAaBmAgMnP
Peruna	Perun	k1gMnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
božstvo	božstvo	k1gNnSc4
třetí	třetí	k4xOgFnSc2
funkce	funkce	k1gFnSc2
Velese	Velese	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
postavy	postava	k1gFnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
vzájemně	vzájemně	k6eAd1
nepřátelské	přátelský	k2eNgInPc4d1
na	na	k7c6
základě	základ	k1gInSc6
své	svůj	k3xOyFgFnSc2
hypotézy	hypotéza	k1gFnSc2
základního	základní	k2eAgInSc2d1
mýtu	mýtus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aleksander	Aleksander	k1gMnSc1
Gieysztor	Gieysztor	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
aplikaci	aplikace	k1gFnSc6
hypotézy	hypotéza	k1gFnSc2
počítal	počítat	k5eAaImAgInS
i	i	k9
s	s	k7c7
vnitřní	vnitřní	k2eAgFnSc7d1
dualismem	dualismus	k1gInSc7
první	první	k4xOgFnSc2
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c4
božstvo	božstvo	k1gNnSc4
varunovského	varunovský	k2eAgInSc2d1
pólu	pól	k1gInSc2
označil	označit	k5eAaPmAgMnS
Velese	Velese	k1gFnPc4
a	a	k8xC
za	za	k7c4
mitrovské	mitrovský	k2eAgNnSc4d1
Peruna	Perun	k1gMnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
jeho	jeho	k3xOp3gFnPc2
hypostazí	hypostaze	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
podle	podle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
názoru	názor	k1gInSc2
jsou	být	k5eAaImIp3nP
Svantovít	Svantovít	k1gMnSc1
a	a	k8xC
Jarovít	Jarovít	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečnickou	válečnický	k2eAgFnSc4d1
druhou	druhý	k4xOgFnSc4
funkci	funkce	k1gFnSc4
pod	pod	k7c4
něj	on	k3xPp3gMnSc4
reprezentovali	reprezentovat	k5eAaImAgMnP
sluneční	sluneční	k2eAgNnPc4d1
božstva	božstvo	k1gNnPc4
Svarog	Svaroga	k1gFnPc2
<g/>
,	,	kIx,
Dažbog	Dažboga	k1gFnPc2
a	a	k8xC
Svarožič	Svarožič	k1gInSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
funkci	funkce	k1gFnSc6
třetí	třetí	k4xOgMnSc1
Mokoš	Mokoš	k1gMnSc1
<g/>
,	,	kIx,
Rod	rod	k1gInSc1
a	a	k8xC
případní	případný	k2eAgMnPc1d1
slovanští	slovanský	k2eAgMnPc1d1
blíženci	blíženec	k1gMnPc1
–	–	k?
Lel	Lel	k1gFnSc1
a	a	k8xC
Polel	Polel	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gieysztorovým	Gieysztorův	k2eAgInPc3d1
závěrům	závěr	k1gInPc3
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
názor	názor	k1gInSc1
Michala	Michal	k1gMnSc2
Téry	tér	k1gInPc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
však	však	k9
Peruna	Perun	k1gMnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
božstvo	božstvo	k1gNnSc4
druhé	druhý	k4xOgFnSc2
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
otázku	otázka	k1gFnSc4
duality	dualita	k1gFnSc2
druhé	druhý	k4xOgFnSc2
funkce	funkce	k1gFnSc2
nechává	nechávat	k5eAaImIp3nS
nerozřešenou	rozřešený	k2eNgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Perunovým	Perunův	k2eAgNnSc7d1
zařazením	zařazení	k1gNnSc7
k	k	k7c3
druhé	druhý	k4xOgFnSc3
funkci	funkce	k1gFnSc3
souhlasí	souhlasit	k5eAaImIp3nS
i	i	k9
Martin	Martin	k1gMnSc1
Golema	Golem	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vidí	vidět	k5eAaImIp3nS
dualitu	dualita	k1gFnSc4
první	první	k4xOgFnPc4
funkce	funkce	k1gFnPc4
reprezentovanou	reprezentovaný	k2eAgFnSc4d1
Svarogem	Svarog	k1gInSc7
a	a	k8xC
Velesem	Veles	k1gInSc7
ve	v	k7c6
východoslovanském	východoslovanský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
Černobohem	Černoboh	k1gMnSc7
a	a	k8xC
Provem	Prov	k1gMnSc7
v	v	k7c6
polabském	polabský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prototeismus	Prototeismus	k1gInSc1
</s>
<s>
Polský	polský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Henryk	Henryk	k1gMnSc1
Łowmiański	Łowmiańske	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
formuloval	formulovat	k5eAaImAgMnS
hypotézu	hypotéza	k1gFnSc4
že	že	k8xS
Slované	Slovan	k1gMnPc1
znali	znát	k5eAaImAgMnP
pouze	pouze	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
výraznou	výrazný	k2eAgFnSc7d1
božskou	božský	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
–	–	k?
boha	bůh	k1gMnSc2
nebes	nebesa	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
existoval	existovat	k5eAaImAgInS
u	u	k7c2
nich	on	k3xPp3gFnPc2
tedy	tedy	k9
prototeismus	prototeismus	k1gInSc4
či	či	k8xC
pramonoteismus	pramonoteismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgInS
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
u	u	k7c2
západních	západní	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
nazýván	nazývat	k5eAaImNgInS
Svarog	Svarog	k1gInSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Perun	perun	k1gInSc4
–	–	k?
v	v	k7c6
důsledku	důsledek	k1gInSc6
akcentu	akcent	k1gInSc2
na	na	k7c4
jeho	jeho	k3xOp3gInPc4
hromovládné	hromovládný	k2eAgInPc4d1
atributy	atribut	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
v	v	k7c6
důsledků	důsledek	k1gInPc2
kontaktů	kontakt	k1gInPc2
s	s	k7c7
křesťanstvím	křesťanství	k1gNnSc7
mělo	mít	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
individualizaci	individualizace	k1gFnSc3
hypostazí	hypostaze	k1gFnPc2
nebeského	nebeský	k2eAgMnSc2d1
boha	bůh	k1gMnSc2
a	a	k8xC
také	také	k9
deifikaci	deifikace	k1gFnSc4
původně	původně	k6eAd1
démonických	démonický	k2eAgFnPc2d1
bytostí	bytost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
hypotézu	hypotéza	k1gFnSc4
zakládá	zakládat	k5eAaImIp3nS
na	na	k7c6
pasáži	pasáž	k1gFnSc6
z	z	k7c2
díla	dílo	k1gNnSc2
Válka	Válek	k1gMnSc2
s	s	k7c7
Góty	Gót	k1gMnPc7
Prokopia	Prokopium	k1gNnSc2
z	z	k7c2
Kaisareie	Kaisareie	k1gFnSc2
pocházejícího	pocházející	k2eAgMnSc2d1
přibližně	přibližně	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
560	#num#	k4
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
o	o	k7c6
Slovanech	Slovan	k1gMnPc6
a	a	k8xC
Antech	Ant	k1gMnPc6
uvádí	uvádět	k5eAaImIp3nS
následující	následující	k2eAgFnSc1d1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Domnívají	domnívat	k5eAaImIp3nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jeden	jeden	k4xCgInSc1
z	z	k7c2
[	[	kIx(
<g/>
jejich	jejich	k3xOp3gMnPc2
<g/>
]	]	kIx)
bohů	bůh	k1gMnPc2
(	(	kIx(
<g/>
theós	theós	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
tvůrce	tvůrce	k1gMnSc1
blesku	blesk	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
jediným	jediný	k2eAgMnSc7d1
pánem	pán	k1gMnSc7
všech	všecek	k3xTgFnPc2
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
obětují	obětovat	k5eAaBmIp3nP
mu	on	k3xPp3gMnSc3
býky	býk	k1gMnPc4
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
zvířecí	zvířecí	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neznají	neznat	k5eAaImIp3nP,k5eNaImIp3nP
osud	osud	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
ani	ani	k8xC
nepřiznávají	přiznávat	k5eNaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
nějaký	nějaký	k3yIgInSc4
vliv	vliv	k1gInSc4
na	na	k7c4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
pak	pak	k6eAd1
blíží	blížit	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
smrt	smrt	k1gFnSc4
–	–	k?
buď	buď	k8xC
že	že	k8xS
onemocní	onemocnět	k5eAaPmIp3nP
<g/>
,	,	kIx,
anebo	anebo	k8xC
jdou	jít	k5eAaImIp3nP
do	do	k7c2
války	válka	k1gFnSc2
-	-	kIx~
<g/>
,	,	kIx,
slibují	slibovat	k5eAaImIp3nP
bohu	bůh	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
ihned	ihned	k6eAd1
přinesou	přinést	k5eAaPmIp3nP
děkovnou	děkovný	k2eAgFnSc4d1
oběť	oběť	k1gFnSc4
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
přežijí	přežít	k5eAaPmIp3nP
<g/>
;	;	kIx,
a	a	k8xC
když	když	k8xS
vyváznou	vyváznout	k5eAaPmIp3nP
<g/>
,	,	kIx,
obětují	obětovat	k5eAaBmIp3nP
mu	on	k3xPp3gNnSc3
co	co	k9
slíbili	slíbit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
předpokládají	předpokládat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
touto	tento	k3xDgFnSc7
obětí	oběť	k1gFnSc7
vykoupili	vykoupit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
záchranu	záchrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uctívají	uctívat	k5eAaImIp3nP
řeky	řeka	k1gFnPc1
<g/>
,	,	kIx,
vodní	vodní	k2eAgFnPc1d1
víly	víla	k1gFnPc1
(	(	kIx(
<g/>
nymfai	nymfai	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
různá	různý	k2eAgNnPc1d1
další	další	k2eAgNnPc1d1
božstva	božstvo	k1gNnPc1
(	(	kIx(
<g/>
daimonia	daimonium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obětují	obětovat	k5eAaBmIp3nP
jim	on	k3xPp3gMnPc3
všem	všecek	k3xTgNnPc3
a	a	k8xC
z	z	k7c2
těch	ten	k3xDgFnPc2
obětí	oběť	k1gFnPc2
vyvozují	vyvozovat	k5eAaImIp3nP
věštby	věštba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Łowmiańského	Łowmiańského	k2eAgFnSc1d1
hypotéza	hypotéza	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
založena	založit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
rukopisných	rukopisný	k2eAgFnPc2d1
variant	varianta	k1gFnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
druhá	druhý	k4xOgFnSc1
by	by	kYmCp3nP
spíše	spíše	k9
poukazovala	poukazovat	k5eAaImAgFnS
na	na	k7c4
kultický	kultický	k2eAgInSc4d1
henoteismus	henoteismus	k1gInSc4
známý	známý	k1gMnSc1
z	z	k7c2
polabského	polabský	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
v	v	k7c6
rozporu	rozpor	k1gInSc6
s	s	k7c7
rozvinutým	rozvinutý	k2eAgInSc7d1
polyteismem	polyteismus	k1gInSc7
jiných	jiný	k2eAgNnPc6d1
indoevropských	indoevropský	k2eAgNnPc6d1
náboženstvích	náboženství	k1gNnPc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc4
podoba	podoba	k1gFnSc1
je	být	k5eAaImIp3nS
lépe	dobře	k6eAd2
doložena	doložit	k5eAaPmNgFnS
dobovými	dobový	k2eAgInPc7d1
prameny	pramen	k1gInPc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
předpokladu	předpoklad	k1gInSc2
že	že	k8xS
již	již	k6eAd1
Praindoevropané	Praindoevropaný	k2eAgNnSc1d1
měli	mít	k5eAaImAgMnP
bohatou	bohatý	k2eAgFnSc4d1
mytologii	mytologie	k1gFnSc4
a	a	k8xC
věřili	věřit	k5eAaImAgMnP
ve	v	k7c6
více	hodně	k6eAd2
božstev	božstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prokopios	Prokopiosa	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
znalost	znalost	k1gFnSc4
slovanského	slovanský	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
byla	být	k5eAaImAgFnS
nejspíše	nejspíše	k9
jenom	jenom	k6eAd1
povrchní	povrchní	k2eAgInSc1d1
a	a	k8xC
setkal	setkat	k5eAaPmAgInS
se	se	k3xPyFc4
jen	jen	k9
s	s	k7c7
úzkou	úzký	k2eAgFnSc7d1
částí	část	k1gFnSc7
slovanské	slovanský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
–	–	k?
účastníky	účastník	k1gMnPc7
válečných	válečný	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgInS
také	také	k9
použít	použít	k5eAaPmF
interpretatio	interpretatio	k6eAd1
christiana	christiana	k1gFnSc1
či	či	k8xC
interpretatio	interpretatio	k1gNnSc1
graeca	graec	k1gInSc2
–	–	k?
nabízí	nabízet	k5eAaImIp3nS
se	se	k3xPyFc4
ovlivnění	ovlivnění	k1gNnSc2
postavou	postava	k1gFnSc7
Dia	Dia	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
taktéž	taktéž	k?
možné	možný	k2eAgNnSc4d1
že	že	k8xS
Slované	Slovan	k1gMnPc1
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
potkal	potkat	k5eAaPmAgInS
akcentovali	akcentovat	k5eAaImAgMnP
kult	kult	k1gInSc4
nebeského	nebeský	k2eAgMnSc2d1
boha	bůh	k1gMnSc2
s	s	k7c7
válečnickou	válečnický	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Henoteismus	Henoteismus	k1gInSc1
</s>
<s>
Mírnější	mírný	k2eAgFnSc4d2
verzi	verze	k1gFnSc4
hypotézy	hypotéza	k1gFnSc2
o	o	k7c6
prototeismu	prototeismus	k1gInSc6
představuje	představovat	k5eAaImIp3nS
názor	názor	k1gInSc1
že	že	k8xS
Slované	Slovan	k1gMnPc1
byly	být	k5eAaImAgInP
henoteisty	henoteist	k1gInPc1
<g/>
,	,	kIx,
tedy	tedy	k9
uctívali	uctívat	k5eAaImAgMnP
jedno	jeden	k4xCgNnSc4
božstvo	božstvo	k1gNnSc4
jako	jako	k8xC,k8xS
nejvyšší	vysoký	k2eAgFnSc4d3
<g/>
,	,	kIx,
ale	ale	k8xC
zároveň	zároveň	k6eAd1
znali	znát	k5eAaImAgMnP
a	a	k8xC
uctívali	uctívat	k5eAaImAgMnP
i	i	k9
jiné	jiný	k2eAgMnPc4d1
bohy	bůh	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napovídá	napovídat	k5eAaBmIp3nS
tomu	ten	k3xDgNnSc3
například	například	k6eAd1
tvrzením	tvrzení	k1gNnSc7
Helmolda	Helmolda	k1gMnSc1
z	z	k7c2
Bosau	Bosaus	k1gInSc2
o	o	k7c6
Polabských	polabský	k2eAgInPc6d1
Slovanech	Slovan	k1gInPc6
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
díle	díl	k1gInSc6
Kronika	kronika	k1gFnSc1
Slovanů	Slovan	k1gMnPc2
z	z	k7c2
konce	konec	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
prezentuje	prezentovat	k5eAaBmIp3nS
božstvo	božstvo	k1gNnSc4
typu	typ	k1gInSc2
deus	deus	k1gInSc4
otiosus	otiosus	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Mají	mít	k5eAaImIp3nP
tedy	tedy	k9
mnohotvárné	mnohotvárný	k2eAgFnPc1d1
božské	božský	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
<g/>
,	,	kIx,
jimž	jenž	k3xRgInPc3
přisuzují	přisuzovat	k5eAaImIp3nP
pole	pole	k1gNnSc4
a	a	k8xC
lesy	les	k1gInPc4
<g/>
,	,	kIx,
smutky	smutek	k1gInPc4
a	a	k8xC
rozkoše	rozkoš	k1gFnPc4
<g/>
,	,	kIx,
nepopírají	popírat	k5eNaImIp3nP
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
bůh	bůh	k1gMnSc1
na	na	k7c6
nebesích	nebesa	k1gNnPc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
poroučí	poroučet	k5eAaImIp3nS
ostatním	ostatní	k2eAgMnSc7d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nad	nad	k7c4
jiné	jiný	k2eAgNnSc4d1
mocný	mocný	k2eAgInSc4d1
a	a	k8xC
stará	starat	k5eAaImIp3nS
se	se	k3xPyFc4
jen	jen	k9
o	o	k7c4
věci	věc	k1gFnPc4
nadpozemské	nadpozemský	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1
bohové	bůh	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
vzešli	vzejít	k5eAaPmAgMnP
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
krve	krev	k1gFnSc2
vykonávají	vykonávat	k5eAaImIp3nP
úkoly	úkol	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jim	on	k3xPp3gMnPc3
jsou	být	k5eAaImIp3nP
přiděleny	přidělen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
každý	každý	k3xTgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
vznešenější	vznešený	k2eAgFnSc1d2
<g/>
,	,	kIx,
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
blíže	blízce	k6eAd2
stojí	stát	k5eAaImIp3nS
bohu	bůh	k1gMnSc3
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
V	v	k7c6
podobném	podobný	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
byl	být	k5eAaImAgInS
jako	jako	k9
deus	deus	k1gInSc1
deorum	deorum	k1gInSc1
„	„	k?
<g/>
bůh	bůh	k1gMnSc1
bohů	bůh	k1gMnPc2
<g/>
“	“	k?
označován	označován	k2eAgMnSc1d1
Svantovít	Svantovít	k1gMnSc1
a	a	k8xC
jako	jako	k8xS,k8xC
summus	summus	k1gMnSc1
deus	deusa	k1gFnPc2
„	„	k?
<g/>
nejvyšší	vysoký	k2eAgMnSc1d3
bůh	bůh	k1gMnSc1
<g/>
“	“	k?
zase	zase	k9
Trihlav	Trihlav	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
takto	takto	k6eAd1
zní	znět	k5eAaImIp3nS
pasáž	pasáž	k1gFnSc4
podle	podle	k7c2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
rukopisů	rukopis	k1gInPc2
<g/>
,	,	kIx,
podle	podle	k7c2
druhého	druhý	k4xOgNnSc2
lze	lze	k6eAd1
číst	číst	k5eAaImF
„	„	k?
<g/>
Domnívají	domnívat	k5eAaImIp3nP
se	se	k3xPyFc4
že	že	k8xS
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgMnSc1d1
bůh	bůh	k1gMnSc1
<g/>
...	...	k?
<g/>
“	“	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
MÁCHAL	Máchal	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bájesloví	bájesloví	k1gNnSc1
slovanské	slovanský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Votobia	Votobia	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85619	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TÉRA	TÉRA	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perun	Perun	k1gMnSc1
<g/>
:	:	kIx,
Bůh	bůh	k1gMnSc1
hromovládce	hromovládce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červený	červený	k2eAgInSc1d1
Kostelec	Kostelec	k1gInSc1
<g/>
:	:	kIx,
Pabel	Pabel	k1gInSc1
Mervart	Mervarta	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86818	#num#	k4
<g/>
-	-	kIx~
<g/>
82	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
234	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Téra	Téra	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
VÁŇA	Váňa	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc4
slovanských	slovanský	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
a	a	k8xC
démonů	démon	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7038	#num#	k4
<g/>
-	-	kIx~
<g/>
187	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
55	#num#	k4
<g/>
-	-	kIx~
<g/>
56	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Váňa	Váňa	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Atweri	Atweri	k1gNnSc1
<g/>
,	,	kIx,
Slovanské	slovanský	k2eAgNnSc1d1
pohanství	pohanství	k1gNnSc1
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
Vlivy	vliv	k1gInPc1
novopohanství	novopohanství	k1gNnSc2
na	na	k7c4
společnost	společnost	k1gFnSc4
i	i	k8xC
jednotlivce	jednotlivec	k1gMnPc4
<g/>
,	,	kIx,
Magisterská	magisterský	k2eAgFnSc1d1
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
48	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WARNEROVÁ	WARNEROVÁ	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
,	,	kIx,
Ruské	ruský	k2eAgInPc1d1
mýty	mýtus	k1gInPc1
<g/>
,	,	kIx,
Levné	levný	k2eAgFnPc1d1
knihy	kniha	k1gFnPc1
KMa	KMa	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7309	#num#	k4
<g/>
-	-	kIx~
<g/>
359	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
↑	↑	k?
Váňa	Váňa	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
250	#num#	k4
<g/>
-	-	kIx~
<g/>
255	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Váňa	Váňa	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
257	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Váňa	Váňa	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
257	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PUHVEL	PUHVEL	kA
<g/>
,	,	kIx,
Jaan	Jaan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srovnávací	srovnávací	k2eAgFnPc4d1
mytologie	mytologie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
177	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
274	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Atweri	Atweri	k1gNnSc1
<g/>
,	,	kIx,
Slovanské	slovanský	k2eAgNnSc1d1
pohanství	pohanství	k1gNnSc1
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
Vlivy	vliv	k1gInPc1
novopohanství	novopohanství	k1gNnSc2
na	na	k7c4
společnost	společnost	k1gFnSc4
i	i	k8xC
jednotlivce	jednotlivec	k1gMnPc4
<g/>
,	,	kIx,
Magisterská	magisterský	k2eAgFnSc1d1
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Téra	Téra	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
311	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Atweri	Atweri	k1gNnSc1
<g/>
,	,	kIx,
Slovanské	slovanský	k2eAgNnSc1d1
pohanství	pohanství	k1gNnSc1
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
Vlivy	vliv	k1gInPc1
novopohanství	novopohanství	k1gNnSc2
na	na	k7c4
společnost	společnost	k1gFnSc4
i	i	k8xC
jednotlivce	jednotlivec	k1gMnPc4
<g/>
,	,	kIx,
Magisterská	magisterský	k2eAgFnSc1d1
diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
51	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Téra	Téra	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
318	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DYNDA	Dynda	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archaické	archaický	k2eAgNnSc1d1
slovanské	slovanský	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
komparativní	komparativní	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
Georgese	Georgese	k1gFnSc2
Dumézila	Dumézila	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Ústav	ústav	k1gInSc1
filosofie	filosofie	k1gFnSc2
a	a	k8xC
religionistiky	religionistika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Dalibor	Dalibor	k1gMnSc1
Antalík	Antalík	k1gMnSc1
<g/>
.	.	kIx.
s.	s.	k?
19	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Dynda	Dynda	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dynda	Dynda	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Váňa	Váňa	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
262	#num#	k4
<g/>
-	-	kIx~
<g/>
263	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PROFANTOVÁ	PROFANTOVÁ	kA
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
<g/>
;	;	kIx,
PROFANT	profant	k1gInSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
slovanských	slovanský	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
a	a	k8xC
mýtů	mýtus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
219	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
32	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dynda	Dynda	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
28	#num#	k4
<g/>
-	-	kIx~
<g/>
33	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dynda	Dynda	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
48,52	48,52	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dynda	Dynda	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dynda	Dynda	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Téra	Téra	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
318	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Téra	Téra	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
66	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DYNDA	Dynda	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovanské	slovanský	k2eAgInPc1d1
pohanství	pohanství	k1gNnSc4
ve	v	k7c6
středověkých	středověký	k2eAgInPc6d1
latinských	latinský	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dolní	dolní	k2eAgInPc1d1
Břežany	Břežany	k1gInPc1
<g/>
:	:	kIx,
Scriptorium	Scriptorium	k1gNnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88013	#num#	k4
<g/>
-	-	kIx~
<g/>
52	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Téra	Téra	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
66	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HELMOLD	HELMOLD	kA
Z	z	k7c2
BOSAU	BOSAU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
Slovanů	Slovan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
786	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
150	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Helmold	Helmold	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
NIEDERLE	NIEDERLE	kA
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovanské	slovanský	k2eAgFnPc4d1
starožitnosti	starožitnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Bursík	Bursík	k1gMnSc1
&	&	k?
Kohout	Kohout	k1gMnSc1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
150	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
I.	I.	kA
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MÁCHAL	Máchal	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
Bájesloví	bájesloví	k1gNnSc1
slovanské	slovanský	k2eAgNnSc1d1
<g/>
,	,	kIx,
Votobia	Votobia	k1gFnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85866-91-9	80-85866-91-9	k4
</s>
<s>
NIEDERLE	NIEDERLE	kA
<g/>
,	,	kIx,
Lubor	Lubor	k1gMnSc1
<g/>
,	,	kIx,
Rukověť	rukověť	k1gFnSc1
slovanských	slovanský	k2eAgFnPc2d1
starožitností	starožitnost	k1gFnPc2
<g/>
,	,	kIx,
ČSAV	ČSAV	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1953	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
PITRO	PITRO	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
VOKÁČ	Vokáč	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
,	,	kIx,
Bohové	bůh	k1gMnPc1
dávných	dávný	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
<g/>
,	,	kIx,
ISV	ISV	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
</s>
<s>
TÉRA	TÉRA	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
,	,	kIx,
Perun	Perun	k1gMnSc1
-	-	kIx~
bůh	bůh	k1gMnSc1
hromovládce	hromovládce	k1gMnSc1
:	:	kIx,
sonda	sonda	k1gFnSc1
do	do	k7c2
slovanského	slovanský	k2eAgNnSc2d1
archaického	archaický	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
<g/>
,	,	kIx,
Červený	červený	k2eAgInSc1d1
Kostelec	Kostelec	k1gInSc1
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Mervart	Mervarta	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-86818-82-5	978-80-86818-82-5	k4
</s>
<s>
VÁŇA	Váňa	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
,	,	kIx,
Svět	svět	k1gInSc1
slovanských	slovanský	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
a	a	k8xC
démonů	démon	k1gMnPc2
<g/>
,	,	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1990	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7038-187-6	80-7038-187-6	k4
</s>
<s>
GIEYSZTOR	GIEYSZTOR	kA
<g/>
,	,	kIx,
Aleksander	Aleksander	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mytologie	mytologie	k1gFnSc1
Slovanů	Slovan	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Historické	historický	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
<g/>
;	;	kIx,
sv.	sv.	kA
85	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
257	#num#	k4
<g/>
-	-	kIx~
<g/>
3218	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Slovanské	slovanský	k2eAgNnSc1d1
pohanství	pohanství	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
kniha	kniha	k1gFnSc1
o	o	k7c4
mytologii	mytologie	k1gFnSc4
starých	starý	k2eAgMnPc2d1
Slovanů	Slovan	k1gMnPc2
-	-	kIx~
HOSTINSKÝ	hostinský	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Záboj	Záboj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stará	starý	k2eAgFnSc1d1
vieronauka	vieronauk	k1gMnSc2
slovenská	slovenský	k2eAgNnPc1d1
:	:	kIx,
Vek	veka	k1gFnPc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
kniha	kniha	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
]	]	kIx)
Pešť	Pešť	k1gFnSc1
<g/>
:	:	kIx,
Minerva	Minerva	k1gFnSc1
<g/>
,	,	kIx,
1871	#num#	k4
<g/>
.	.	kIx.
122	#num#	k4
s.	s.	k?
-	-	kIx~
dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
v	v	k7c6
Digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
UKB	UKB	kA
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Slovanská	slovanský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Slovanská	slovanský	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
a	a	k8xC
folklór	folklór	k1gInSc1
božstva	božstvo	k1gNnSc2
a	a	k8xC
démonivýchodních	démonivýchodní	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
<g/>
(	(	kIx(
<g/>
zejména	zejména	k9
Rusů	Rus	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Vladimírův	Vladimírův	k2eAgInSc1d1
panteon	panteon	k1gInSc1
</s>
<s>
Perun	Perun	k1gMnSc1
•	•	k?
Chors-Dažbog	Chors-Dažbog	k1gMnSc1
•	•	k?
Stribog	Stribog	k1gMnSc1
•	•	k?
Simargl	Simargl	k1gMnSc1
•	•	k?
Mokoš	Mokoš	k1gMnSc1
ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
Veles	Veles	k1gMnSc1
•	•	k?
Svarog	Svarog	k1gMnSc1
•	•	k?
Svarožic	Svarožic	k1gMnSc1
•	•	k?
Trojan	Trojan	k1gMnSc1
•	•	k?
Zbručský	Zbručský	k2eAgInSc4d1
idol	idol	k1gInSc4
pozdní	pozdní	k2eAgInPc1d1
prameny	pramen	k1gInPc1
afolklór	afolklóra	k1gFnPc2
</s>
<s>
Jarilo	Jarít	k5eAaPmAgNnS,k5eAaImAgNnS,k5eAaBmAgNnS
•	•	k?
Rod	rod	k1gInSc1
•	•	k?
Rožanice	Rožanice	k1gFnSc2
•	•	k?
Kupalo	Kupala	k1gFnSc5
•	•	k?
Kostroma	Kostrom	k1gMnSc2
•	•	k?
Marena	Maren	k1gMnSc2
•	•	k?
Ognyena	Ognyen	k1gMnSc2
(	(	kIx(
<g/>
Ohnivá	ohnivý	k2eAgFnSc1d1
Marie	Marie	k1gFnSc1
<g/>
)	)	kIx)
démoni	démon	k1gMnPc1
a	a	k8xC
duchové	duch	k1gMnPc1
</s>
<s>
Baba	baba	k1gFnSc1
Jaga	Jaga	k1gFnSc1
•	•	k?
běs	běs	k1gInSc1
•	•	k?
domovoj	domovoj	k1gInSc1
•	•	k?
Kostěj	Kostěj	k1gFnSc2
•	•	k?
lešij	lešít	k5eAaPmRp2nS
•	•	k?
Polevik	Polevik	k1gInSc1
•	•	k?
rusalka	rusalka	k1gFnSc1
•	•	k?
víla	víla	k1gFnSc1
•	•	k?
vlkodlak	vlkodlak	k1gMnSc1
•	•	k?
zmej	zmej	k1gMnSc1
</s>
<s>
Zbručský	Zbručský	k2eAgInSc1d1
idol	idol	k1gInSc1
božstva	božstvo	k1gNnSc2
a	a	k8xC
démonizápadních	démonizápadní	k2eAgInPc2d1
Slovanů	Slovan	k1gInPc2
</s>
<s>
Obodrité	Obodrita	k1gMnPc1
</s>
<s>
Prove	Proev	k1gFnPc1
•	•	k?
Podaga	Podaga	k1gFnSc1
•	•	k?
Živa	Živa	k1gFnSc1
Lutici	Lutice	k1gFnSc4
</s>
<s>
Černoboh	Černoboh	k1gMnSc1
•	•	k?
Černohlav	Černohlav	k1gMnSc1
•	•	k?
bohové-blíženci	bohové-blíženec	k1gMnSc3
•	•	k?
Jarovít	Jarovít	k1gMnSc1
•	•	k?
Radgost	Radgost	k1gInSc1
•	•	k?
Rujevít	Rujevít	k1gFnSc2
•	•	k?
Porenut	Porenut	k2eAgInSc1d1
(	(	kIx(
<g/>
Turupit	Turupit	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Porevít	Porevít	k1gMnSc1
•	•	k?
Pizamar	Pizamar	k1gMnSc1
•	•	k?
Pripegala	Pripegala	k1gFnSc1
•	•	k?
Svarožic	Svarožic	k1gMnSc1
•	•	k?
Svantovít	Svantovít	k1gMnSc1
•	•	k?
Triglav	Triglav	k1gMnSc1
Pomořané	Pomořan	k1gMnPc1
</s>
<s>
Jarovít	Jarovít	k5eAaPmF
•	•	k?
Triglav	Triglav	k1gMnSc1
•	•	k?
Wolinský	Wolinský	k2eAgInSc1d1
idol	idol	k1gInSc1
•	•	k?
Wolinské	Wolinský	k2eAgInPc1d1
kopí	kopit	k5eAaImIp3nP
pozdní	pozdní	k2eAgInPc1d1
prameny	pramen	k1gInPc1
afolklór	afolklóra	k1gFnPc2
</s>
<s>
Jessa	Jessa	k1gFnSc1
•	•	k?
Kresnik	Kresnik	k1gMnSc1
/	/	kIx~
Zduhač	Zduhač	k1gMnSc1
•	•	k?
Lada	Lada	k1gFnSc1
/	/	kIx~
Lado	lado	k1gNnSc1
•	•	k?
Lel	Lel	k1gMnSc1
a	a	k8xC
Polel	Polel	k1gMnSc1
•	•	k?
Morana	Morana	k1gFnSc1
•	•	k?
Dodola	Dodola	k1gFnSc1
/	/	kIx~
Perperuna	Perperuna	k1gFnSc1
démoni	démon	k1gMnPc1
a	a	k8xC
duchové	duch	k1gMnPc1
</s>
<s>
čert	čert	k1gMnSc1
•	•	k?
ludkové	ludková	k1gFnSc2
•	•	k?
rusalka	rusalka	k1gFnSc1
•	•	k?
sudičky	sudička	k1gFnSc2
•	•	k?
upír	upír	k1gMnSc1
•	•	k?
víla	víla	k1gFnSc1
•	•	k?
vodník	vodník	k1gMnSc1
•	•	k?
vlkodlak	vlkodlak	k1gMnSc1
•	•	k?
zmej	zmej	k1gMnSc1
</s>
<s>
hypotetická	hypotetický	k2eAgNnPc1d1
božstva	božstvo	k1gNnPc1
</s>
<s>
Bělbog	Bělbog	k1gMnSc1
•	•	k?
German	German	k1gMnSc1
•	•	k?
Pogoda	Pogoda	k1gMnSc1
•	•	k?
Pohvizd	pohvizd	k1gInSc1
•	•	k?
Zora	Zora	k1gFnSc1
epika	epika	k1gFnSc1
</s>
<s>
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
Lech	Lech	k1gMnSc1
a	a	k8xC
Rus	Rus	k1gMnSc1
(	(	kIx(
<g/>
Praotec	praotec	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
Lech	Lech	k1gMnSc1
<g/>
,	,	kIx,
kníže	kníže	k1gMnSc1
Rus	Rus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc1d1
dynastický	dynastický	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
•	•	k?
Ilja	Ilja	k1gMnSc1
Muromec	Muromec	k1gMnSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
dynastický	dynastický	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
•	•	k?
Slovo	slovo	k1gNnSc1
o	o	k7c6
pluku	pluk	k1gInSc6
Igorově	Igorův	k2eAgInSc6d1
kosmologie	kosmologie	k1gFnSc2
</s>
<s>
Bujan	bujan	k1gMnSc1
•	•	k?
Nav	navit	k5eAaImRp2nS
slovanské	slovanský	k2eAgFnPc4d1
svátky	svátek	k1gInPc7
</s>
<s>
Dožínky	dožínky	k1gFnPc1
•	•	k?
Dziady	Dziada	k1gFnSc2
•	•	k?
Hromnice	hromnice	k1gFnSc2
•	•	k?
Kupadelné	Kupadelný	k2eAgInPc4d1
svátky	svátek	k1gInPc4
•	•	k?
Kračun	Kračun	k1gInSc1
•	•	k?
vynášení	vynášení	k1gNnSc1
smrti	smrt	k1gFnSc2
/	/	kIx~
vítání	vítání	k1gNnSc4
jara	jaro	k1gNnSc2
související	související	k2eAgNnSc1d1
</s>
<s>
Prillwitzské	Prillwitzský	k2eAgInPc1d1
idoly	idol	k1gInPc1
•	•	k?
slovanské	slovanský	k2eAgNnSc1d1
pohanství	pohanství	k1gNnSc1
•	•	k?
rodnověří	rodnověřit	k5eAaPmIp3nS,k5eAaImIp3nS
•	•	k?
indoevropské	indoevropský	k2eAgNnSc4d1
náboženství	náboženství	k1gNnSc4
•	•	k?
Velesova	Velesův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
</s>
