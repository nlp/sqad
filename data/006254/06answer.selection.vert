<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Dobromila	Dobromila	k1gFnSc1	Dobromila
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
<g/>
,	,	kIx,	,
dívčím	dívčí	k2eAgNnSc7d1	dívčí
jménem	jméno	k1gNnSc7	jméno
Artmannová	Artmannová	k1gFnSc1	Artmannová
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1785	[number]	k4	1785
Všeradice	Všeradice	k1gFnSc2	Všeradice
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1845	[number]	k4	1845
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
buditelka	buditelka	k1gFnSc1	buditelka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
kuchařek	kuchařka	k1gFnPc2	kuchařka
<g/>
,	,	kIx,	,
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
a	a	k8xC	a
krátkých	krátký	k2eAgFnPc2d1	krátká
próz	próza	k1gFnPc2	próza
<g/>
.	.	kIx.	.
</s>
