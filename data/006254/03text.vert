<s>
Magdalena	Magdalena	k1gFnSc1	Magdalena
Dobromila	Dobromila	k1gFnSc1	Dobromila
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
<g/>
,	,	kIx,	,
dívčím	dívčí	k2eAgNnSc7d1	dívčí
jménem	jméno	k1gNnSc7	jméno
Artmannová	Artmannová	k1gFnSc1	Artmannová
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1785	[number]	k4	1785
Všeradice	Všeradice	k1gFnSc2	Všeradice
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1845	[number]	k4	1845
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
buditelka	buditelka	k1gFnSc1	buditelka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
kuchařek	kuchařka	k1gFnPc2	kuchařka
<g/>
,	,	kIx,	,
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
a	a	k8xC	a
krátkých	krátký	k2eAgFnPc2d1	krátká
próz	próza	k1gFnPc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
především	především	k9	především
jako	jako	k8xC	jako
autorka	autorka	k1gFnSc1	autorka
knihy	kniha	k1gFnSc2	kniha
Domácí	domácí	k2eAgFnSc1d1	domácí
kuchařka	kuchařka	k1gFnSc1	kuchařka
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
dětství	dětství	k1gNnSc1	dětství
nebylo	být	k5eNaImAgNnS	být
příliš	příliš	k6eAd1	příliš
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
,	,	kIx,	,
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
je	on	k3xPp3gMnPc4	on
především	především	k6eAd1	především
úmrtí	úmrť	k1gFnSc7	úmrť
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Františka	František	k1gMnSc2	František
Artmanna	Artmann	k1gMnSc2	Artmann
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
purkrabího	purkrabí	k1gMnSc2	purkrabí
na	na	k7c4	na
panství	panství	k1gNnSc4	panství
Všeradice	Všeradice	k1gFnSc2	Všeradice
u	u	k7c2	u
Hořovic	Hořovice	k1gFnPc2	Hořovice
<g/>
,	,	kIx,	,
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
její	její	k3xOp3gFnSc2	její
poslední	poslední	k2eAgFnSc2d1	poslední
sestry	sestra	k1gFnSc2	sestra
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
Magdalénou	Magdaléna	k1gFnSc7	Magdaléna
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
odmala	odmala	k6eAd1	odmala
projevovala	projevovat	k5eAaImAgFnS	projevovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
inteligenci	inteligence	k1gFnSc4	inteligence
a	a	k8xC	a
zájem	zájem	k1gInSc4	zájem
o	o	k7c6	o
učení	učení	k1gNnSc6	učení
<g/>
.	.	kIx.	.
</s>
<s>
Rodinný	rodinný	k2eAgMnSc1d1	rodinný
přítel	přítel	k1gMnSc1	přítel
Eugenikus	Eugenikus	k1gMnSc1	Eugenikus
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
vychovatel	vychovatel	k1gMnSc1	vychovatel
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
hraběte	hrabě	k1gMnSc2	hrabě
Kaunice	Kaunice	k1gFnSc2	Kaunice
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
Magdalénu	Magdaléna	k1gFnSc4	Magdaléna
k	k	k7c3	k
četbě	četba	k1gFnSc3	četba
a	a	k8xC	a
katolické	katolický	k2eAgFnSc3d1	katolická
víře	víra	k1gFnSc3	víra
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
modlitební	modlitební	k2eAgFnPc1d1	modlitební
knížky	knížka	k1gFnPc1	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
ji	on	k3xPp3gFnSc4	on
sama	sám	k3xTgFnSc1	sám
také	také	k9	také
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
ji	on	k3xPp3gFnSc4	on
poslala	poslat	k5eAaPmAgFnS	poslat
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
stěhování	stěhování	k1gNnSc1	stěhování
čekalo	čekat	k5eAaImAgNnS	čekat
Magdalénu	Magdaléna	k1gFnSc4	Magdaléna
k	k	k7c3	k
tetě	teta	k1gFnSc3	teta
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
již	již	k6eAd1	již
starat	starat	k5eAaImF	starat
o	o	k7c4	o
domácnost	domácnost	k1gFnSc4	domácnost
a	a	k8xC	a
pomáhat	pomáhat	k5eAaImF	pomáhat
matce	matka	k1gFnSc3	matka
vydělávat	vydělávat	k5eAaImF	vydělávat
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
18	[number]	k4	18
let	léto	k1gNnPc2	léto
Magdaléna	Magdaléna	k1gFnSc1	Magdaléna
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
německojazyčném	německojazyčný	k2eAgNnSc6d1	německojazyčné
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
neuměla	umět	k5eNaImAgFnS	umět
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
seznámení	seznámení	k1gNnSc3	seznámení
se	se	k3xPyFc4	se
a	a	k8xC	a
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
buditelem	buditel	k1gMnSc7	buditel
a	a	k8xC	a
spisovatelem	spisovatel	k1gMnSc7	spisovatel
Janem	Jan	k1gMnSc7	Jan
Aloisem	Alois	k1gMnSc7	Alois
Sudipravem	Sudiprav	k1gInSc7	Sudiprav
Rettigem	Rettig	k1gMnSc7	Rettig
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
po	po	k7c6	po
sňatku	sňatek	k1gInSc2	sňatek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
stala	stát	k5eAaPmAgFnS	stát
vlastenecky	vlastenecky	k6eAd1	vlastenecky
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
buditelka	buditelka	k1gFnSc1	buditelka
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
A.	A.	kA	A.
S.	S.	kA	S.
Rettig	Rettig	k1gMnSc1	Rettig
zastával	zastávat	k5eAaImAgMnS	zastávat
jako	jako	k8xC	jako
právník	právník	k1gMnSc1	právník
funkci	funkce	k1gFnSc4	funkce
radního	radní	k2eAgMnSc2d1	radní
v	v	k7c6	v
několika	několik	k4yIc6	několik
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
tak	tak	k6eAd1	tak
pobývala	pobývat	k5eAaImAgFnS	pobývat
v	v	k7c6	v
Přelouči	Přelouč	k1gFnSc6	Přelouč
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
a	a	k8xC	a
posledních	poslední	k2eAgNnPc2d1	poslední
11	[number]	k4	11
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Rettig	Rettig	k1gMnSc1	Rettig
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
poznal	poznat	k5eAaPmAgMnS	poznat
schopnosti	schopnost	k1gFnPc4	schopnost
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
neomezoval	omezovat	k5eNaImAgMnS	omezovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
její	její	k3xOp3gInPc4	její
první	první	k4xOgInPc4	první
české	český	k2eAgInPc4d1	český
verše	verš	k1gInPc4	verš
cizeloval	cizelovat	k5eAaImAgMnS	cizelovat
<g/>
,	,	kIx,	,
přivedl	přivést	k5eAaPmAgMnS	přivést
jí	on	k3xPp3gFnSc3	on
na	na	k7c4	na
pomoc	pomoc	k1gFnSc1	pomoc
zkušené	zkušený	k2eAgMnPc4d1	zkušený
rádce	rádce	k1gMnPc4	rádce
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
je	být	k5eAaImIp3nS	být
uveřejňoval	uveřejňovat	k5eAaImAgMnS	uveřejňovat
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
básněmi	báseň	k1gFnPc7	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
moderním	moderní	k2eAgMnSc7d1	moderní
mužem	muž	k1gMnSc7	muž
uznalým	uznalý	k2eAgMnSc7d1	uznalý
k	k	k7c3	k
ženské	ženský	k2eAgFnSc3d1	ženská
osobnosti	osobnost	k1gFnSc3	osobnost
a	a	k8xC	a
jejím	její	k3xOp3gFnPc3	její
vlohám	vloha	k1gFnPc3	vloha
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
porozumění	porozumění	k1gNnSc4	porozumění
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
pracovitou	pracovitý	k2eAgFnSc4d1	pracovitá
a	a	k8xC	a
aktivní	aktivní	k2eAgFnSc4d1	aktivní
manželku	manželka	k1gFnSc4	manželka
jako	jako	k8xS	jako
málokterý	málokterý	k3yIgMnSc1	málokterý
muž	muž	k1gMnSc1	muž
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
se	se	k3xPyFc4	se
po	po	k7c6	po
osobní	osobní	k2eAgFnSc6d1	osobní
smutné	smutný	k2eAgFnSc6d1	smutná
zkušenosti	zkušenost	k1gFnSc6	zkušenost
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
věnovat	věnovat	k5eAaPmF	věnovat
zejména	zejména	k6eAd1	zejména
výchově	výchova	k1gFnSc3	výchova
a	a	k8xC	a
výuce	výuka	k1gFnSc3	výuka
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
kurzech	kurz	k1gInPc6	kurz
je	být	k5eAaImIp3nS	být
učila	učit	k5eAaImAgFnS	učit
hospodaření	hospodaření	k1gNnSc4	hospodaření
<g/>
,	,	kIx,	,
vaření	vaření	k1gNnSc1	vaření
<g/>
,	,	kIx,	,
domácím	domácí	k2eAgFnPc3d1	domácí
pracím	práce	k1gFnPc3	práce
a	a	k8xC	a
české	český	k2eAgFnSc3d1	Česká
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Půjčovala	půjčovat	k5eAaImAgFnS	půjčovat
dívkám	dívka	k1gFnPc3	dívka
české	český	k2eAgFnSc2d1	Česká
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
předčítala	předčítat	k5eAaImAgFnS	předčítat
jim	on	k3xPp3gMnPc3	on
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Angažovala	angažovat	k5eAaBmAgFnS	angažovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
svůj	svůj	k3xOyFgInSc4	svůj
společenský	společenský	k2eAgInSc4d1	společenský
salón	salón	k1gInSc4	salón
(	(	kIx(	(
<g/>
kafíčkovou	kafíčkový	k2eAgFnSc4d1	Kafíčková
společnost	společnost	k1gFnSc4	společnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
českou	český	k2eAgFnSc4d1	Česká
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
prosadila	prosadit	k5eAaPmAgFnS	prosadit
vyčištění	vyčištění	k1gNnSc4	vyčištění
studánky	studánka	k1gFnSc2	studánka
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
altánu	altán	k1gInSc2	altán
<g/>
.	.	kIx.	.
</s>
<s>
Pořádala	pořádat	k5eAaImAgFnS	pořádat
literární	literární	k2eAgNnSc4d1	literární
posezení	posezení	k1gNnSc4	posezení
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
recitovaly	recitovat	k5eAaImAgFnP	recitovat
české	český	k2eAgFnPc1d1	Česká
básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
významných	významný	k2eAgFnPc2d1	významná
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
svého	své	k1gNnSc2	své
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
referovala	referovat	k5eAaBmAgFnS	referovat
o	o	k7c4	o
nich	on	k3xPp3gFnPc2	on
jako	jako	k9	jako
dopisovatelka	dopisovatelka	k1gFnSc1	dopisovatelka
časopisu	časopis	k1gInSc2	časopis
Květy	Květa	k1gFnSc2	Květa
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
stýkala	stýkat	k5eAaImAgFnS	stýkat
s	s	k7c7	s
vlastenci	vlastenec	k1gMnPc7	vlastenec
<g/>
,	,	kIx,	,
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Palackým	Palacký	k1gMnSc7	Palacký
<g/>
,	,	kIx,	,
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Jungmannem	Jungmann	k1gMnSc7	Jungmann
<g/>
,	,	kIx,	,
Pavlem	Pavel	k1gMnSc7	Pavel
Josefem	Josef	k1gMnSc7	Josef
Šafaříkem	Šafařík	k1gMnSc7	Šafařík
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
vlastenci	vlastenec	k1gMnPc7	vlastenec
vedla	vést	k5eAaImAgFnS	vést
korespondenci	korespondence	k1gFnSc4	korespondence
<g/>
.	.	kIx.	.
</s>
<s>
Výhradním	výhradní	k2eAgMnSc7d1	výhradní
vydavatelem	vydavatel	k1gMnSc7	vydavatel
jejích	její	k3xOp3gNnPc2	její
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
přítelem	přítel	k1gMnSc7	přítel
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Hostivít	Hostivít	k1gMnSc1	Hostivít
František	František	k1gMnSc1	František
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
královéhradecký	královéhradecký	k2eAgMnSc1d1	královéhradecký
knihtiskař	knihtiskař	k1gMnSc1	knihtiskař
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
literární	literární	k2eAgInPc1d1	literární
pokusy	pokus	k1gInPc1	pokus
M.	M.	kA	M.
D.	D.	kA	D.
Rettigové	Rettigový	k2eAgFnPc1d1	Rettigová
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
jejích	její	k3xOp3gMnPc2	její
vrstevníků	vrstevník	k1gMnPc2	vrstevník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
plodná	plodný	k2eAgFnSc1d1	plodná
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaBmAgFnS	napsat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
drobnějších	drobný	k2eAgNnPc2d2	drobnější
i	i	k8xC	i
rozsáhlejších	rozsáhlý	k2eAgNnPc2d2	rozsáhlejší
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
se	se	k3xPyFc4	se
odhodlala	odhodlat	k5eAaPmAgFnS	odhodlat
psát	psát	k5eAaImF	psát
a	a	k8xC	a
publikovat	publikovat	k5eAaBmF	publikovat
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
uveřejňovala	uveřejňovat	k5eAaImAgFnS	uveřejňovat
drobné	drobný	k2eAgFnPc4d1	drobná
prozaické	prozaický	k2eAgFnPc4d1	prozaická
i	i	k8xC	i
veršované	veršovaný	k2eAgFnPc4d1	veršovaná
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
Dobroslav	Dobroslava	k1gFnPc2	Dobroslava
<g/>
,	,	kIx,	,
Čechoslav	Čechoslav	k1gMnSc1	Čechoslav
<g/>
,	,	kIx,	,
Poutník	poutník	k1gMnSc1	poutník
slovanský	slovanský	k2eAgMnSc1d1	slovanský
<g/>
,	,	kIx,	,
v	v	k7c6	v
almanachu	almanach	k1gInSc6	almanach
Milozor	Milozora	k1gFnPc2	Milozora
a	a	k8xC	a
alamanachu	alamanach	k1gInSc2	alamanach
Milina	Milino	k1gNnSc2	Milino
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vydala	vydat	k5eAaPmAgFnS	vydat
několik	několik	k4yIc4	několik
knih	kniha	k1gFnPc2	kniha
povídek	povídka	k1gFnPc2	povídka
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
především	především	k9	především
dospívajícím	dospívající	k2eAgFnPc3d1	dospívající
dívkám	dívka	k1gFnPc3	dívka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
výbornou	výborný	k2eAgFnSc7d1	výborná
vypravěčkou	vypravěčka	k1gFnSc7	vypravěčka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
zakladatelku	zakladatelka	k1gFnSc4	zakladatelka
české	český	k2eAgFnSc2d1	Česká
červené	červený	k2eAgFnSc2d1	červená
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaPmAgFnS	věnovat
výhradně	výhradně	k6eAd1	výhradně
literatuře	literatura	k1gFnSc3	literatura
prakticky	prakticky	k6eAd1	prakticky
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
tvorba	tvorba	k1gFnSc1	tvorba
nikdy	nikdy	k6eAd1	nikdy
nedosahovala	dosahovat	k5eNaImAgFnS	dosahovat
vysokých	vysoký	k2eAgFnPc2d1	vysoká
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
kvalit	kvalita	k1gFnPc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
má	mít	k5eAaImIp3nS	mít
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
šířením	šíření	k1gNnSc7	šíření
národního	národní	k2eAgNnSc2d1	národní
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
osvěty	osvěta	k1gFnSc2	osvěta
v	v	k7c6	v
ženském	ženský	k2eAgNnSc6d1	ženské
měšťanském	měšťanský	k2eAgNnSc6d1	měšťanské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
Domácí	domácí	k2eAgFnSc3d1	domácí
kuchařce	kuchařka	k1gFnSc3	kuchařka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nejenže	nejenže	k6eAd1	nejenže
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
znalectví	znalectví	k1gNnSc3	znalectví
české	český	k2eAgFnSc2d1	Česká
kuchyně	kuchyně	k1gFnSc2	kuchyně
a	a	k8xC	a
sbírku	sbírka	k1gFnSc4	sbírka
kuchařských	kuchařský	k2eAgInPc2d1	kuchařský
receptů	recept	k1gInPc2	recept
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
měšťanském	měšťanský	k2eAgNnSc6d1	měšťanské
a	a	k8xC	a
lidovém	lidový	k2eAgNnSc6d1	lidové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Rettigové	Rettigový	k2eAgFnSc2d1	Rettigová
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Domácí	domácí	k2eAgFnSc1d1	domácí
kuchařka	kuchařka	k1gFnSc1	kuchařka
vydaná	vydaný	k2eAgFnSc1d1	vydaná
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
vydáních	vydání	k1gNnPc6	vydání
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
rozličných	rozličný	k2eAgFnPc2d1	rozličná
myšlenek	myšlenka	k1gFnPc2	myšlenka
–	–	k?	–
upistografie	upistografie	k1gFnSc2	upistografie
–	–	k?	–
veršovaná	veršovaný	k2eAgNnPc4d1	veršované
blahopřání	blahopřání	k1gNnPc4	blahopřání
<g/>
,	,	kIx,	,
naivní	naivní	k2eAgFnPc4d1	naivní
poezie	poezie	k1gFnPc4	poezie
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
lidovou	lidový	k2eAgFnSc4d1	lidová
tvořivost	tvořivost	k1gFnSc4	tvořivost
Chudobičky	chudobička	k1gFnSc2	chudobička
–	–	k?	–
milostné	milostný	k2eAgFnPc1d1	milostná
povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
dramatické	dramatický	k2eAgFnPc1d1	dramatická
hříčky	hříčka	k1gFnPc1	hříčka
pro	pro	k7c4	pro
učenou	učený	k2eAgFnSc4d1	učená
společnost	společnost	k1gFnSc4	společnost
Kafíčko	kafíčko	k1gNnSc1	kafíčko
–	–	k?	–
jak	jak	k6eAd1	jak
připravit	připravit	k5eAaPmF	připravit
kávu	káva	k1gFnSc4	káva
a	a	k8xC	a
zákusky	zákuska	k1gFnPc4	zákuska
včetně	včetně	k7c2	včetně
rad	rada	k1gFnPc2	rada
společenské	společenský	k2eAgFnSc2d1	společenská
konverzace	konverzace	k1gFnSc2	konverzace
Bílá	bílý	k2eAgFnSc1d1	bílá
růže	růže	k1gFnSc1	růže
–	–	k?	–
žertovné	žertovný	k2eAgNnSc1d1	žertovné
dvouaktové	dvouaktový	k2eAgNnSc1d1	dvouaktový
drama	drama	k1gNnSc1	drama
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
Mařenčin	Mařenčin	k2eAgInSc4d1	Mařenčin
košíček	košíček	k1gInSc4	košíček
-	-	kIx~	-
příběhy	příběh	k1gInPc4	příběh
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
ponaučením	ponaučení	k1gNnSc7	ponaučení
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
Domácí	domácí	k2eAgFnSc1d1	domácí
kuchařka	kuchařka	k1gFnSc1	kuchařka
–	–	k?	–
beletristicky	beletristicky	k6eAd1	beletristicky
psaná	psaný	k2eAgFnSc1d1	psaná
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
činorodý	činorodý	k2eAgInSc4d1	činorodý
přístup	přístup	k1gInSc4	přístup
ženy	žena	k1gFnSc2	žena
k	k	k7c3	k
domácím	domácí	k2eAgFnPc3d1	domácí
pracím	práce	k1gFnPc3	práce
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
:	:	kIx,	:
Magdalena	Magdalena	k1gFnSc1	Magdalena
Dobromila	Dobromila	k1gFnSc1	Dobromila
Rettigová	Rettigový	k2eAgFnSc1d1	Rettigová
<g/>
,	,	kIx,	,
veselohra	veselohra	k1gFnSc1	veselohra
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
jednáních	jednání	k1gNnPc6	jednání
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
málo	málo	k6eAd1	málo
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
životopisná	životopisný	k2eAgFnSc1d1	životopisná
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
uvedená	uvedený	k2eAgFnSc1d1	uvedená
kuchařská	kuchařský	k2eAgFnSc1d1	kuchařská
kniha	kniha	k1gFnSc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Pamětní	pamětní	k2eAgFnSc4d1	pamětní
síň	síň	k1gFnSc4	síň
M.	M.	kA	M.
D.	D.	kA	D.
Rettigové	Rettigový	k2eAgNnSc1d1	Rettigové
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
Portrétní	portrétní	k2eAgFnSc1d1	portrétní
busta	busta	k1gFnSc1	busta
M.	M.	kA	M.
D.	D.	kA	D.
Rettigové	Rettig	k1gMnPc1	Rettig
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
památky	památka	k1gFnPc1	památka
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
Portrétní	portrétní	k2eAgFnSc1d1	portrétní
kresba	kresba	k1gFnSc1	kresba
(	(	kIx(	(
<g/>
akvarel	akvarel	k1gInSc1	akvarel
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyšívaný	vyšívaný	k2eAgInSc4d1	vyšívaný
gratulační	gratulační	k2eAgInSc4d1	gratulační
lístek	lístek	k1gInSc4	lístek
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Literární	literární	k2eAgFnSc4d1	literární
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
spravuje	spravovat	k5eAaImIp3nS	spravovat
Památník	památník	k1gInSc1	památník
národního	národní	k2eAgNnSc2d1	národní
písemnictví	písemnictví	k1gNnSc2	písemnictví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
autorčiných	autorčin	k2eAgFnPc6d1	autorčina
rodných	rodný	k2eAgFnPc6d1	rodná
Všeradicích	Všeradice	k1gFnPc6	Všeradice
Galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
muzeum	muzeum	k1gNnSc1	muzeum
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Dobromily	Dobromila	k1gFnSc2	Dobromila
Rettigové	Rettigový	k2eAgFnSc2d1	Rettigová
</s>
