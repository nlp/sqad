<p>
<s>
Okr	okr	k1gInSc1	okr
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
ὠ	ὠ	k?	ὠ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zemitý	zemitý	k2eAgInSc1d1	zemitý
pigment	pigment	k1gInSc1	pigment
žluté	žlutý	k2eAgFnSc2d1	žlutá
až	až	k8xS	až
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
jílu	jíl	k1gInSc2	jíl
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
obsahem	obsah	k1gInSc7	obsah
oxidu	oxid	k1gInSc2	oxid
železa	železo	k1gNnSc2	železo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
odvozen	odvodit	k5eAaPmNgInS	odvodit
název	název	k1gInSc1	název
pro	pro	k7c4	pro
okrovou	okrový	k2eAgFnSc4d1	okrová
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Člověkem	člověk	k1gMnSc7	člověk
je	být	k5eAaImIp3nS	být
barvivo	barvivo	k1gNnSc1	barvivo
používáno	používat	k5eAaImNgNnS	používat
již	již	k6eAd1	již
od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Okr	okr	k1gInSc1	okr
světlý	světlý	k2eAgInSc1d1	světlý
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
barev	barva	k1gFnPc2	barva
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
okr	okr	k1gInSc1	okr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
okr	okr	k1gInSc1	okr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
