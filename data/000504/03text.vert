<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
i	i	k9	i
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
provincie	provincie	k1gFnSc2	provincie
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Madridu	Madrid	k1gInSc6	Madrid
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
španělským	španělský	k2eAgNnSc7d1	španělské
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
díky	díky	k7c3	díky
originálním	originální	k2eAgFnPc3d1	originální
stavbám	stavba	k1gFnPc3	stavba
architekta	architekt	k1gMnSc2	architekt
Antoni	Antoň	k1gFnSc3	Antoň
Gaudího	Gaudí	k1gMnSc2	Gaudí
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
<g/>
.	.	kIx.	.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
technických	technický	k2eAgFnPc2d1	technická
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc2d1	obchodní
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
také	také	k9	také
řadu	řada	k1gFnSc4	řada
významných	významný	k2eAgFnPc2d1	významná
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
severovýchodního	severovýchodní	k2eAgNnSc2d1	severovýchodní
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
okolní	okolní	k2eAgInSc1d1	okolní
kraj	kraj	k1gInSc1	kraj
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
bohatý	bohatý	k2eAgInSc1d1	bohatý
na	na	k7c4	na
suroviny	surovina	k1gFnPc4	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
dějištěm	dějiště	k1gNnSc7	dějiště
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
navštěvovaným	navštěvovaný	k2eAgNnPc3d1	navštěvované
evropským	evropský	k2eAgNnPc3d1	Evropské
městům	město	k1gNnPc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
polohou	poloha	k1gFnSc7	poloha
a	a	k8xC	a
relativní	relativní	k2eAgFnSc7d1	relativní
blízkostí	blízkost	k1gFnSc7	blízkost
u	u	k7c2	u
francouzských	francouzský	k2eAgFnPc2d1	francouzská
hranic	hranice	k1gFnPc2	hranice
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
Evropanů	Evropan	k1gMnPc2	Evropan
snadněji	snadno	k6eAd2	snadno
dostupná	dostupný	k2eAgFnSc1d1	dostupná
než	než	k8xS	než
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Španělského	španělský	k2eAgNnSc2d1	španělské
království	království	k1gNnSc2	království
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
založena	založit	k5eAaPmNgFnS	založit
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
st.	st.	kA	st.
před	před	k7c7	před
K.	K.	kA	K.
kartaginským	kartaginský	k2eAgMnSc7d1	kartaginský
generálem	generál	k1gMnSc7	generál
Hamilkarem	Hamilkar	k1gMnSc7	Hamilkar
Barkasem	Barkas	k1gMnSc7	Barkas
a	a	k8xC	a
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Barcina	Barcina	k1gFnSc1	Barcina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
218	[number]	k4	218
před	před	k7c4	před
K.	K.	kA	K.
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Barcelonu	Barcelona	k1gFnSc4	Barcelona
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
takovou	takový	k3xDgFnSc4	takový
důležitost	důležitost	k1gFnSc4	důležitost
jako	jako	k8xC	jako
jihozápadně	jihozápadně	k6eAd1	jihozápadně
ležící	ležící	k2eAgFnSc1d1	ležící
Tarragona	Tarragona	k1gFnSc1	Tarragona
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úpadku	úpadek	k1gInSc6	úpadek
Římského	římský	k2eAgNnSc2d1	římské
císařství	císařství	k1gNnSc2	císařství
převzali	převzít	k5eAaPmAgMnP	převzít
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Vizigóti	Vizigót	k1gMnPc1	Vizigót
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
8	[number]	k4	8
<g/>
.	.	kIx.	.
st.	st.	kA	st.
město	město	k1gNnSc1	město
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Maurové	Maurové	k?	Maurové
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgNnSc6	týž
století	století	k1gNnSc6	století
Barcelonu	Barcelona	k1gFnSc4	Barcelona
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
st.	st.	kA	st.
se	se	k3xPyFc4	se
Barcelona	Barcelona	k1gFnSc1	Barcelona
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
ekonomických	ekonomický	k2eAgNnPc2d1	ekonomické
a	a	k8xC	a
obchodních	obchodní	k2eAgNnPc2d1	obchodní
center	centrum	k1gNnPc2	centrum
Aragonské	aragonský	k2eAgFnSc2d1	Aragonská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1469	[number]	k4	1469
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonského	aragonský	k2eAgMnSc4d1	aragonský
s	s	k7c7	s
Isabelou	Isabela	k1gFnSc7	Isabela
I.	I.	kA	I.
Kastilskou	kastilský	k2eAgFnSc7d1	Kastilská
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
přenesení	přenesení	k1gNnSc2	přenesení
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
do	do	k7c2	do
Madridu	Madrid	k1gInSc2	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
rozvoj	rozvoj	k1gInSc1	rozvoj
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
Amerikou	Amerika	k1gFnSc7	Amerika
řízený	řízený	k2eAgInSc1d1	řízený
z	z	k7c2	z
Madridu	Madrid	k1gInSc2	Madrid
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
centralizaci	centralizace	k1gFnSc6	centralizace
moci	moc	k1gFnSc2	moc
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
Barcelony	Barcelona	k1gFnSc2	Barcelona
a	a	k8xC	a
nespokojenosti	nespokojenost	k1gFnSc2	nespokojenost
jejich	jejich	k3xOp3gMnPc2	jejich
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1640	[number]	k4	1640
-	-	kIx~	-
59	[number]	k4	59
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byli	být	k5eAaImAgMnP	být
Katalánci	Katalánec	k1gMnPc1	Katalánec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
francouzští	francouzský	k2eAgMnPc1d1	francouzský
spojenci	spojenec	k1gMnPc1	spojenec
poraženi	porazit	k5eAaPmNgMnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
město	město	k1gNnSc1	město
zažívá	zažívat	k5eAaImIp3nS	zažívat
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
do	do	k7c2	do
Barcelony	Barcelona	k1gFnSc2	Barcelona
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
noví	nový	k2eAgMnPc1d1	nový
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
čtvrť	čtvrť	k1gFnSc1	čtvrť
Eixample	Eixample	k1gFnSc2	Eixample
s	s	k7c7	s
rovnými	rovný	k2eAgFnPc7d1	rovná
městskými	městský	k2eAgFnPc7d1	městská
třídami	třída	k1gFnPc7	třída
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
barcelonská	barcelonský	k2eAgFnSc1d1	barcelonská
střední	střední	k2eAgFnSc1d1	střední
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
st.	st.	kA	st.
jsou	být	k5eAaImIp3nP	být
vystavěny	vystavět	k5eAaPmNgInP	vystavět
nové	nový	k2eAgInPc1d1	nový
secesní	secesní	k2eAgInPc1d1	secesní
paláce	palác	k1gInPc1	palác
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
katedrále	katedrála	k1gFnSc6	katedrála
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
obroda	obroda	k1gFnSc1	obroda
katalánské	katalánský	k2eAgFnSc2d1	katalánská
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
obsazeno	obsazen	k2eAgNnSc4d1	obsazeno
fašisty	fašista	k1gMnPc7	fašista
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zákazu	zákaz	k1gInSc2	zákaz
výuky	výuka	k1gFnSc2	výuka
katalánštiny	katalánština	k1gFnSc2	katalánština
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
nastává	nastávat	k5eAaImIp3nS	nastávat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
F.	F.	kA	F.
Franca	Franca	k?	Franca
<g/>
.	.	kIx.	.
</s>
<s>
Katalánsko	Katalánsko	k1gNnSc1	Katalánsko
získává	získávat	k5eAaImIp3nS	získávat
status	status	k1gInSc4	status
autonomního	autonomní	k2eAgInSc2d1	autonomní
regionu	region	k1gInSc2	region
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
je	být	k5eAaImIp3nS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
první	první	k4xOgFnSc1	první
katalánská	katalánský	k2eAgFnSc1d1	katalánská
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
konaly	konat	k5eAaImAgFnP	konat
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgNnPc3d3	nejznámější
dílům	dílo	k1gNnPc3	dílo
Gaudího	Gaudí	k1gMnSc2	Gaudí
patří	patřit	k5eAaImIp3nS	patřit
chrám	chrám	k1gInSc4	chrám
Sagrada	Sagrada	k1gFnSc1	Sagrada
Familia	Familia	k1gFnSc1	Familia
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
města	město	k1gNnSc2	město
a	a	k8xC	a
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
výstavba	výstavba	k1gFnSc1	výstavba
stále	stále	k6eAd1	stále
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
financována	financovat	k5eAaBmNgFnS	financovat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
darů	dar	k1gInPc2	dar
<g/>
,	,	kIx,	,
práce	práce	k1gFnPc1	práce
postupují	postupovat	k5eAaImIp3nP	postupovat
pomalu	pomalu	k6eAd1	pomalu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gaudí	Gaudí	k1gNnSc4	Gaudí
dal	dát	k5eAaPmAgMnS	dát
tomuto	tento	k3xDgInSc3	tento
objektu	objekt	k1gInSc3	objekt
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Mohutná	mohutný	k2eAgFnSc1d1	mohutná
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
osmi	osm	k4xCc7	osm
vysoko	vysoko	k6eAd1	vysoko
čnějícími	čnějící	k2eAgFnPc7d1	čnějící
věžemi	věž	k1gFnPc7	věž
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
stala	stát	k5eAaPmAgFnS	stát
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
bodem	bod	k1gInSc7	bod
v	v	k7c6	v
siluetě	silueta	k1gFnSc6	silueta
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
věží	věž	k1gFnPc2	věž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
stoupá	stoupat	k5eAaImIp3nS	stoupat
po	po	k7c6	po
točitých	točitý	k2eAgInPc6d1	točitý
schodech	schod	k1gInPc6	schod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nádherný	nádherný	k2eAgInSc4d1	nádherný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgMnPc1d1	další
Gaudího	Gaudí	k1gMnSc2	Gaudí
známé	známý	k2eAgFnPc1d1	známá
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
Milà	Milà	k1gFnSc1	Milà
(	(	kIx(	(
<g/>
Casa	Cas	k2eAgFnSc1d1	Casa
Milà	Milà	k1gFnSc1	Milà
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
Batlló	Batlló	k1gFnSc1	Batlló
(	(	kIx(	(
<g/>
casa	cas	k2eAgFnSc1d1	casa
Batlló	Batlló	k1gFnSc1	Batlló
<g/>
)	)	kIx)	)
a	a	k8xC	a
Park	park	k1gInSc1	park
Güell	Güella	k1gFnPc2	Güella
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parku	park	k1gInSc6	park
Güell	Güella	k1gFnPc2	Güella
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
pěkný	pěkný	k2eAgInSc4d1	pěkný
výhled	výhled	k1gInSc4	výhled
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
muzeum	muzeum	k1gNnSc1	muzeum
Gaudího	Gaudí	k1gMnSc2	Gaudí
a	a	k8xC	a
spousta	spousta	k1gFnSc1	spousta
zajímavostí	zajímavost	k1gFnPc2	zajímavost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kamenné	kamenný	k2eAgFnPc4d1	kamenná
lavičky	lavička	k1gFnPc4	lavička
zdobené	zdobený	k2eAgFnPc4d1	zdobená
barevnými	barevný	k2eAgInPc7d1	barevný
keramickými	keramický	k2eAgInPc7d1	keramický
střepy	střep	k1gInPc7	střep
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
slavným	slavný	k2eAgInPc3d1	slavný
kulturně	kulturně	k6eAd1	kulturně
historickým	historický	k2eAgFnPc3d1	historická
památkám	památka	k1gFnPc3	památka
Barcelony	Barcelona	k1gFnSc2	Barcelona
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
"	"	kIx"	"
<g/>
Gotická	gotický	k2eAgFnSc1d1	gotická
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
soubor	soubor	k1gInSc1	soubor
staveb	stavba	k1gFnPc2	stavba
ohraničený	ohraničený	k2eAgMnSc1d1	ohraničený
římskými	římský	k2eAgFnPc7d1	římská
hradbami	hradba	k1gFnPc7	hradba
s	s	k7c7	s
dominující	dominující	k2eAgFnSc7d1	dominující
gotickou	gotický	k2eAgFnSc7d1	gotická
katedrálou	katedrála	k1gFnSc7	katedrála
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
sv.	sv.	kA	sv.
Eulálie	Eulálie	k1gFnSc1	Eulálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Plaça	Plaç	k1gInSc2	Plaç
del	del	k?	del
Rei	Rea	k1gFnSc3	Rea
stojí	stát	k5eAaImIp3nS	stát
muzeum	muzeum	k1gNnSc1	muzeum
historie	historie	k1gFnSc2	historie
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
podzemí	podzemí	k1gNnSc6	podzemí
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
vykopávky	vykopávka	k1gFnPc4	vykopávka
z	z	k7c2	z
římského	římský	k2eAgNnSc2d1	římské
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
tu	tu	k6eAd1	tu
byly	být	k5eAaImAgFnP	být
lázně	lázeň	k1gFnPc1	lázeň
s	s	k7c7	s
bazénem	bazén	k1gInSc7	bazén
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
tu	tu	k6eAd1	tu
nalezeny	nalézt	k5eAaBmNgInP	nalézt
i	i	k9	i
vodní	vodní	k2eAgInPc1d1	vodní
kanály	kanál	k1gInPc1	kanál
a	a	k8xC	a
amfory	amfora	k1gFnPc1	amfora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Plaça	Plaç	k1gInSc2	Plaç
del	del	k?	del
Rei	Rea	k1gFnSc3	Rea
stojí	stát	k5eAaImIp3nS	stát
rovněž	rovněž	k9	rovněž
Palau	Palaus	k1gInSc2	Palaus
del	del	k?	del
Lloctinent	Lloctinent	k1gInSc1	Lloctinent
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
místokrále	místokrál	k1gMnSc2	místokrál
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
umístěn	umístit	k5eAaPmNgInS	umístit
Archív	archív	k1gInSc1	archív
Koruny	koruna	k1gFnSc2	koruna
Aragonské	aragonský	k2eAgFnSc2d1	Aragonská
<g/>
.	.	kIx.	.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
má	mít	k5eAaImIp3nS	mít
spoustu	spousta	k1gFnSc4	spousta
gotických	gotický	k2eAgFnPc2d1	gotická
památek	památka	k1gFnPc2	památka
<g/>
;	;	kIx,	;
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
jejího	její	k3xOp3gInSc2	její
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
jich	on	k3xPp3gFnPc2	on
najdeme	najít	k5eAaPmIp1nP	najít
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Barrio	Barrio	k6eAd1	Barrio
Gótico	Gótico	k6eAd1	Gótico
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
určitě	určitě	k6eAd1	určitě
patří	patřit	k5eAaImIp3nS	patřit
budova	budova	k1gFnSc1	budova
radnice	radnice	k1gFnSc2	radnice
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Casa	Casa	k1gFnSc1	Casa
de	de	k?	de
la	la	k1gNnSc1	la
Ciutat	Ciutat	k1gFnSc1	Ciutat
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Plaça	Plaça	k1gMnSc1	Plaça
Sant	Sant	k1gMnSc1	Sant
Jaume	Jaum	k1gMnSc5	Jaum
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgFnSc2d1	středověká
loděnice	loděnice	k1gFnSc2	loděnice
Les	les	k1gInSc1	les
Drassanes	Drassanes	k1gMnSc1	Drassanes
leží	ležet	k5eAaImIp3nS	ležet
nedaleko	nedaleko	k7c2	nedaleko
přístavu	přístav	k1gInSc2	přístav
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
největším	veliký	k2eAgNnSc7d3	veliký
a	a	k8xC	a
nejzachovalejším	zachovalý	k2eAgNnSc7d3	nejzachovalejší
zařízením	zařízení	k1gNnSc7	zařízení
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
Námořní	námořní	k2eAgInSc4d1	námořní
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc7d1	městská
čtvrtí	čtvrt	k1gFnSc7	čtvrt
Ribera	Ribero	k1gNnSc2	Ribero
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
ulice	ulice	k1gFnPc1	ulice
se	se	k3xPyFc4	se
křižují	křižovat	k5eAaImIp3nP	křižovat
v	v	k7c6	v
pravých	pravý	k2eAgInPc6d1	pravý
úhlech	úhel	k1gInPc6	úhel
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
ulice	ulice	k1gFnSc1	ulice
Montcada	Montcada	k1gFnSc1	Montcada
<g/>
.	.	kIx.	.
</s>
<s>
Existovala	existovat	k5eAaImAgFnS	existovat
již	již	k6eAd1	již
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
bydlely	bydlet	k5eAaImAgInP	bydlet
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nejmocnější	mocný	k2eAgFnSc1d3	nejmocnější
šlechtické	šlechtický	k2eAgFnPc4d1	šlechtická
rodiny	rodina	k1gFnPc4	rodina
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
palácích	palác	k1gInPc6	palác
Palau	Palaus	k1gInSc2	Palaus
Aguilar	Aguilara	k1gFnPc2	Aguilara
a	a	k8xC	a
Palau	Palaus	k1gInSc2	Palaus
del	del	k?	del
Baró	Baró	k1gMnSc1	Baró
de	de	k?	de
Castellet	Castellet	k1gInSc1	Castellet
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Picassovo	Picassův	k2eAgNnSc4d1	Picassovo
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
paláci	palác	k1gInSc6	palác
Dalmases	Dalmasesa	k1gFnPc2	Dalmasesa
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
gotická	gotický	k2eAgFnSc1d1	gotická
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
El	Ela	k1gFnPc2	Ela
Pi	pi	k0	pi
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
kostelů	kostel	k1gInPc2	kostel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
věž	věž	k1gFnSc4	věž
tvoří	tvořit	k5eAaImIp3nS	tvořit
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejcharakterističtějších	charakteristický	k2eAgInPc2d3	nejcharakterističtější
rysů	rys	k1gInPc2	rys
gotické	gotický	k2eAgFnSc2d1	gotická
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fasádě	fasáda	k1gFnSc6	fasáda
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
i	i	k9	i
rozměrná	rozměrný	k2eAgFnSc1d1	rozměrná
rozeta	rozeta	k1gFnSc1	rozeta
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Santa	Sant	k1gMnSc2	Sant
Ana	Ana	k1gMnSc2	Ana
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
jakoby	jakoby	k8xS	jakoby
v	v	k7c6	v
ústraní	ústraní	k1gNnSc6	ústraní
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
nedaleká	daleký	k2eNgFnSc1d1	nedaleká
Plaça	Plaça	k1gFnSc1	Plaça
de	de	k?	de
Catalunya	Catalunya	k1gFnSc1	Catalunya
(	(	kIx(	(
<g/>
náměstí	náměstí	k1gNnSc1	náměstí
s	s	k7c7	s
fontánami	fontána	k1gFnPc7	fontána
<g/>
)	)	kIx)	)
kypí	kypět	k5eAaImIp3nS	kypět
velkoměstským	velkoměstský	k2eAgInSc7d1	velkoměstský
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
trvala	trvat	k5eAaImAgFnS	trvat
dvě	dva	k4xCgNnPc4	dva
staletí	staletí	k1gNnPc4	staletí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
přechod	přechod	k1gInSc1	přechod
od	od	k7c2	od
románského	románský	k2eAgInSc2d1	románský
stylu	styl	k1gInSc2	styl
ke	k	k7c3	k
gotické	gotický	k2eAgFnSc3d1	gotická
architektuře	architektura	k1gFnSc3	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výtvor	výtvor	k1gInSc4	výtvor
katalánské	katalánský	k2eAgFnSc2d1	katalánská
gotiky	gotika	k1gFnSc2	gotika
pokládají	pokládat	k5eAaImIp3nP	pokládat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
obdivovatelé	obdivovatel	k1gMnPc1	obdivovatel
kostel	kostel	k1gInSc4	kostel
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
del	del	k?	del
Mar	Mar	k1gMnSc2	Mar
<g/>
.	.	kIx.	.
</s>
<s>
Stavěl	stavět	k5eAaImAgInS	stavět
se	se	k3xPyFc4	se
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
kostele	kostel	k1gInSc6	kostel
La	la	k1gNnSc2	la
Mercè	Mercè	k1gFnSc2	Mercè
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
původně	původně	k6eAd1	původně
patřil	patřit	k5eAaImAgInS	patřit
ke	k	k7c3	k
konventu	konvent	k1gInSc3	konvent
milosrdných	milosrdný	k2eAgMnPc2d1	milosrdný
bratří	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uctívá	uctívat	k5eAaImIp3nS	uctívat
gotická	gotický	k2eAgFnSc1d1	gotická
socha	socha	k1gFnSc1	socha
ochránkyně	ochránkyně	k1gFnSc2	ochránkyně
a	a	k8xC	a
patronky	patronka	k1gFnSc2	patronka
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
Boží	boží	k2eAgFnSc2d1	boží
Matky	matka	k1gFnSc2	matka
milosrdné	milosrdný	k2eAgFnSc2d1	milosrdná
<g/>
.	.	kIx.	.
</s>
<s>
Katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
a	a	k8xC	a
sv.	sv.	kA	sv.
Eulálie	Eulálie	k1gFnSc1	Eulálie
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
stavba	stavba	k1gFnSc1	stavba
budovaná	budovaný	k2eAgFnSc1d1	budovaná
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dvou	dva	k4xCgInPc2	dva
starověkých	starověký	k2eAgInPc2d1	starověký
kostelů	kostel	k1gInPc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kryptě	krypta	k1gFnSc6	krypta
svaté	svatý	k2eAgFnSc2d1	svatá
Eulálie	Eulálie	k1gFnSc2	Eulálie
pod	pod	k7c7	pod
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
uvidíme	uvidět	k5eAaPmIp1nP	uvidět
jemně	jemně	k6eAd1	jemně
opracovaný	opracovaný	k2eAgInSc4d1	opracovaný
alabastrový	alabastrový	k2eAgInSc4d1	alabastrový
sarkofág	sarkofág	k1gInSc4	sarkofág
<g/>
.	.	kIx.	.
</s>
<s>
Oltářní	oltářní	k2eAgInPc4d1	oltářní
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
sv.	sv.	kA	sv.
Gabriela	Gabriela	k1gFnSc1	Gabriela
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Ambrože	Ambrož	k1gMnSc2	Ambrož
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Martina	Martina	k1gFnSc1	Martina
a	a	k8xC	a
Proměnění	proměnění	k1gNnSc1	proměnění
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejcennějšími	cenný	k2eAgInPc7d3	nejcennější
uměleckými	umělecký	k2eAgInPc7d1	umělecký
poklady	poklad	k1gInPc7	poklad
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc4	klášter
Pedralbes	Pedralbesa	k1gFnPc2	Pedralbesa
založila	založit	k5eAaPmAgFnS	založit
roku	rok	k1gInSc2	rok
1326	[number]	k4	1326
královna	královna	k1gFnSc1	královna
Elisenda	Elisenda	k1gFnSc1	Elisenda
z	z	k7c2	z
Montcada	Montcada	k1gFnSc1	Montcada
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
postup	postup	k1gInSc1	postup
stavebních	stavební	k2eAgFnPc2d1	stavební
prací	práce	k1gFnPc2	práce
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
křížová	křížový	k2eAgFnSc1d1	křížová
chodba	chodba	k1gFnSc1	chodba
s	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
galériemi	galérie	k1gFnPc7	galérie
a	a	k8xC	a
klášterní	klášterní	k2eAgFnPc4d1	klášterní
budovy	budova	k1gFnPc4	budova
tvoří	tvořit	k5eAaImIp3nS	tvořit
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
harmonickou	harmonický	k2eAgFnSc4d1	harmonická
jednotu	jednota	k1gFnSc4	jednota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
presbyteriu	presbyterium	k1gNnSc6	presbyterium
vedle	vedle	k7c2	vedle
oltáře	oltář	k1gInSc2	oltář
je	být	k5eAaImIp3nS	být
náhrobek	náhrobek	k1gInSc1	náhrobek
zakladatelky	zakladatelka	k1gFnSc2	zakladatelka
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgNnSc1d1	mořské
akvárium	akvárium	k1gNnSc1	akvárium
-	-	kIx~	-
prvotřídní	prvotřídní	k2eAgNnSc1d1	prvotřídní
akvárium	akvárium	k1gNnSc1	akvárium
se	s	k7c7	s
22	[number]	k4	22
obřími	obří	k2eAgNnPc7d1	obří
akvárii	akvárium	k1gNnPc7	akvárium
a	a	k8xC	a
80	[number]	k4	80
metrů	metr	k1gInPc2	metr
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
skleněným	skleněný	k2eAgInSc7d1	skleněný
tunelem	tunel	k1gInSc7	tunel
protínajícím	protínající	k2eAgInSc7d1	protínající
obří	obří	k2eAgFnPc4d1	obří
nádrž	nádrž	k1gFnSc4	nádrž
se	s	k7c7	s
6	[number]	k4	6
miliony	milion	k4xCgInPc7	milion
litry	litr	k1gInPc4	litr
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
žraloky	žralok	k1gMnPc4	žralok
<g/>
,	,	kIx,	,
rejnoky	rejnok	k1gMnPc4	rejnok
<g/>
,	,	kIx,	,
měsíční	měsíční	k2eAgFnSc4d1	měsíční
rybu	ryba	k1gFnSc4	ryba
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
ryby	ryba	k1gFnPc4	ryba
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
je	být	k5eAaImIp3nS	být
živým	živý	k2eAgNnSc7d1	živé
velkoměstem	velkoměsto	k1gNnSc7	velkoměsto
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
středomořského	středomořský	k2eAgNnSc2d1	středomořské
přímořského	přímořský	k2eAgNnSc2d1	přímořské
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
poměrně	poměrně	k6eAd1	poměrně
suchým	suchý	k2eAgNnSc7d1	suché
létem	léto	k1gNnSc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Srdcem	srdce	k1gNnSc7	srdce
Barcelony	Barcelona	k1gFnSc2	Barcelona
je	být	k5eAaImIp3nS	být
bulvár	bulvár	k1gInSc1	bulvár
Ramblas	Ramblas	k1gInSc1	Ramblas
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Rambles	Rambles	k1gInSc1	Rambles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
nejživějším	živý	k2eAgNnSc7d3	nejživější
místem	místo	k1gNnSc7	místo
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pestrá	pestrý	k2eAgFnSc1d1	pestrá
a	a	k8xC	a
živá	živý	k2eAgFnSc1d1	živá
ulice	ulice	k1gFnSc1	ulice
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
Plaça	Plaç	k1gInSc2	Plaç
Catalunya	Cataluny	k1gInSc2	Cataluny
(	(	kIx(	(
<g/>
náměstí	náměstí	k1gNnSc4	náměstí
s	s	k7c7	s
fontánami	fontána	k1gFnPc7	fontána
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
k	k	k7c3	k
přístavu	přístav	k1gInSc3	přístav
(	(	kIx(	(
<g/>
nábřeží	nábřeží	k1gNnSc1	nábřeží
s	s	k7c7	s
molem	mol	k1gInSc7	mol
a	a	k8xC	a
dominantním	dominantní	k2eAgInSc7d1	dominantní
68	[number]	k4	68
metrů	metr	k1gInPc2	metr
vysokým	vysoký	k2eAgInSc7d1	vysoký
podstavcem	podstavec	k1gInSc7	podstavec
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rambla	Rambnout	k5eAaPmAgFnS	Rambnout
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
korytem	koryto	k1gNnSc7	koryto
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Barcelona	Barcelona	k1gFnSc1	Barcelona
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
10	[number]	k4	10
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
respektují	respektovat	k5eAaImIp3nP	respektovat
původní	původní	k2eAgFnPc1d1	původní
historické	historický	k2eAgFnPc1d1	historická
čtvrti	čtvrt	k1gFnPc1	čtvrt
a	a	k8xC	a
obce	obec	k1gFnPc1	obec
připojené	připojený	k2eAgFnPc1d1	připojená
k	k	k7c3	k
městu	město	k1gNnSc3	město
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Jednotlivé	jednotlivý	k2eAgFnSc6d1	jednotlivá
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
district	district	k1gInSc1	district
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
barri	barri	k6eAd1	barri
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barcelonské	barcelonský	k2eAgNnSc1d1	Barcelonské
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Ciutat	Ciutat	k1gFnSc1	Ciutat
Vella	Vella	k1gFnSc1	Vella
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
:	:	kIx,	:
La	la	k1gNnSc1	la
Barceloneta	Barceloneto	k1gNnSc2	Barceloneto
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Gotic	Gotice	k1gFnPc2	Gotice
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Raval	Raval	k1gInSc1	Raval
a	a	k8xC	a
Sant	Sant	k1gInSc1	Sant
Pere	prát	k5eAaImIp3nS	prát
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Caterina	Caterina	k1gFnSc1	Caterina
a	a	k8xC	a
la	la	k1gNnSc1	la
Ribera	Ribero	k1gNnSc2	Ribero
<g/>
.	.	kIx.	.
</s>
<s>
Ciutat	Ciutat	k5eAaImF	Ciutat
Vella	Vell	k1gMnSc4	Vell
(	(	kIx(	(
<g/>
4,5	[number]	k4	4,5
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
111	[number]	k4	111
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
La	la	k1gNnSc1	la
Barceloneta	Barceloneto	k1gNnSc2	Barceloneto
El	Ela	k1gFnPc2	Ela
Gò	Gò	k1gFnSc2	Gò
El	Ela	k1gFnPc2	Ela
Raval	Raval	k1gMnSc1	Raval
Sant	Sant	k1gMnSc1	Sant
Pere	prát	k5eAaImIp3nS	prát
<g/>
,	,	kIx,	,
Santa	Santa	k1gFnSc1	Santa
Caterina	Caterina	k1gFnSc1	Caterina
a	a	k8xC	a
la	la	k1gNnSc1	la
Ribera	Riber	k1gMnSc2	Riber
Eixample	Eixample	k1gMnSc2	Eixample
(	(	kIx(	(
<g/>
7,5	[number]	k4	7,5
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
262	[number]	k4	262
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Sants-Montjuï	Sants-Montjuï	k1gMnSc1	Sants-Montjuï
(	(	kIx(	(
<g/>
21,4	[number]	k4	21,4
km	km	kA	km
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
,	,	kIx,	,
177	[number]	k4	177
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Les	les	k1gInSc1	les
Corts	Corts	k1gInSc1	Corts
(	(	kIx(	(
<g/>
6,1	[number]	k4	6,1
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
82	[number]	k4	82
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Sarrià	Sarrià	k1gFnPc2	Sarrià
Gervasi	Gervas	k1gMnPc1	Gervas
(	(	kIx(	(
<g/>
20,1	[number]	k4	20,1
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
140	[number]	k4	140
500	[number]	k4	500
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Grà	Grà	k1gMnSc1	Grà
(	(	kIx(	(
<g/>
4,2	[number]	k4	4,2
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
120	[number]	k4	120
100	[number]	k4	100
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Horta-Guinardó	Horta-Guinardó	k1gMnSc6	Horta-Guinardó
(	(	kIx(	(
<g/>
12	[number]	k4	12
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
169	[number]	k4	169
900	[number]	k4	900
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Nou	Nou	k1gFnSc1	Nou
Barris	Barris	k1gFnSc1	Barris
(	(	kIx(	(
<g/>
8	[number]	k4	8
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
165	[number]	k4	165
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Sant	Sant	k1gMnSc1	Sant
Andreu	Andrea	k1gFnSc4	Andrea
(	(	kIx(	(
<g/>
6,6	[number]	k4	6,6
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
142	[number]	k4	142
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Sant	Sant	k1gInSc1	Sant
Martí	Martí	k1gNnSc2	Martí
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
10,8	[number]	k4	10,8
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
221	[number]	k4	221
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
c	c	k0	c
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
častější	častý	k2eAgFnSc1d2	častější
i	i	k8xC	i
počeštěná	počeštěný	k2eAgFnSc1d1	počeštěná
španělská	španělský	k2eAgFnSc1d1	španělská
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
katalánská	katalánský	k2eAgFnSc1d1	katalánská
kodifikovaná	kodifikovaný	k2eAgFnSc1d1	kodifikovaná
<g/>
)	)	kIx)	)
výslovnost	výslovnost	k1gFnSc1	výslovnost
se	s	k7c7	s
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
kodifikovaná	kodifikovaný	k2eAgFnSc1d1	kodifikovaná
španělská	španělský	k2eAgFnSc1d1	španělská
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
<g/>
)	)	kIx)	)
výslovnost	výslovnost	k1gFnSc1	výslovnost
by	by	kYmCp3nS	by
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
mezizubným	mezizubný	k2eAgInSc7d1	mezizubný
třeným	třený	k2eAgInSc7d1	třený
θ	θ	k?	θ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
dnes	dnes	k6eAd1	dnes
ale	ale	k8xC	ale
Pravidla	pravidlo	k1gNnPc1	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
výslovnostní	výslovnostní	k2eAgFnSc4d1	výslovnostní
odchylku	odchylka	k1gFnSc4	odchylka
od	od	k7c2	od
psané	psaný	k2eAgFnSc2d1	psaná
podoby	podoba	k1gFnSc2	podoba
nepředepisují	předepisovat	k5eNaImIp3nP	předepisovat
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tak	tak	k6eAd1	tak
i	i	k9	i
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
barcelona	barcelona	k1gFnSc1	barcelona
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc4	týž
platí	platit	k5eAaImIp3nS	platit
např.	např.	kA	např.
i	i	k8xC	i
pro	pro	k7c4	pro
němčinu	němčina	k1gFnSc4	němčina
<g/>
;	;	kIx,	;
německá	německý	k2eAgFnSc1d1	německá
kodifikovaná	kodifikovaný	k2eAgFnSc1d1	kodifikovaná
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
[	[	kIx(	[
<g/>
barceˈ	barceˈ	k?	barceˈ
<g/>
:	:	kIx,	:
<g/>
na	na	k7c4	na
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Toulouse	Toulouse	k1gInSc1	Toulouse
Business	business	k1gInSc1	business
School	School	k1gInSc4	School
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Barcelona	Barcelona	k1gFnSc1	Barcelona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Barcelona	Barcelona	k1gFnSc1	Barcelona
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
příručka	příručka	k1gFnSc1	příručka
na	na	k7c4	na
Wikivoyage	Wikivoyage	k1gInSc4	Wikivoyage
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
VisitTown	VisitTown	k1gInSc1	VisitTown
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Barcelona	Barcelona	k1gFnSc1	Barcelona
Daily	Daila	k1gFnSc2	Daila
Photos	Photosa	k1gFnPc2	Photosa
Barcelona	Barcelona	k1gFnSc1	Barcelona
Travel	Travel	k1gInSc1	Travel
Guide	Guid	k1gMnSc5	Guid
Online	Onlin	k1gMnSc5	Onlin
průvodce	průvodce	k1gMnSc5	průvodce
Barcelonou	Barcelona	k1gFnSc7	Barcelona
</s>
