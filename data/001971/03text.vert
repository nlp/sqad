<s>
Monrovia	Monrovia	k1gFnSc1	Monrovia
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
západoafrického	západoafrický	k2eAgInSc2d1	západoafrický
státu	stát	k1gInSc2	stát
Libérie	Libérie	k1gFnSc2	Libérie
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
mysu	mys	k1gInSc6	mys
Mezurado	Mezurada	k1gFnSc5	Mezurada
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
ústí	ústit	k5eAaImIp3nP	ústit
řeky	řeka	k1gFnPc1	řeka
Mezurado	Mezurada	k1gFnSc5	Mezurada
a	a	k8xC	a
Saint	Saint	k1gMnSc1	Saint
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
pátého	pátý	k4xOgMnSc2	pátý
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Jamese	Jamese	k1gFnSc1	Jamese
Monroea	Monroea	k1gFnSc1	Monroea
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jenž	k3xRgNnSc2	jenž
vládního	vládní	k2eAgNnSc2d1	vládní
období	období	k1gNnSc2	období
začala	začít	k5eAaPmAgFnS	začít
repatriace	repatriace	k1gFnSc1	repatriace
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
City	City	k1gFnPc4	City
of	of	k?	of
Christ	Christ	k1gInSc1	Christ
(	(	kIx(	(
<g/>
Kristovo	Kristův	k2eAgNnSc1d1	Kristovo
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
osvobození	osvobození	k1gNnSc4	osvobození
otroci	otrok	k1gMnPc1	otrok
z	z	k7c2	z
USA	USA	kA	USA
vyjadřovali	vyjadřovat	k5eAaImAgMnP	vyjadřovat
svůj	svůj	k3xOyFgInSc4	svůj
vděk	vděk	k1gInSc4	vděk
Spasiteli	spasitel	k1gMnSc3	spasitel
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
osvobození	osvobozený	k2eAgMnPc1d1	osvobozený
černí	černý	k2eAgMnPc1d1	černý
otroci	otrok	k1gMnPc1	otrok
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
usidlování	usidlování	k1gNnSc1	usidlování
na	na	k7c6	na
území	území	k1gNnSc6	území
budoucí	budoucí	k2eAgFnSc2d1	budoucí
Libérie	Libérie	k1gFnSc2	Libérie
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Americká	americký	k2eAgFnSc1d1	americká
kolonizační	kolonizační	k2eAgFnSc1d1	kolonizační
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
dělilo	dělit	k5eAaImAgNnS	dělit
na	na	k7c4	na
parcely	parcela	k1gFnPc4	parcela
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
čtvrt	čtvrt	k1gFnSc4	čtvrt
akru	akr	k1gInSc2	akr
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
losem	los	k1gInSc7	los
přidělovaly	přidělovat	k5eAaImAgFnP	přidělovat
novým	nový	k2eAgMnSc7d1	nový
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
svého	svůj	k3xOyFgInSc2	svůj
typu	typ	k1gInSc2	typ
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
západě	západ	k1gInSc6	západ
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Libérie	Libérie	k1gFnSc1	Libérie
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
Monrovia	Monrovia	k1gFnSc1	Monrovia
jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
starosty	starosta	k1gMnSc2	starosta
byly	být	k5eAaImAgFnP	být
vyhlášeny	vyhlásit	k5eAaPmNgFnP	vyhlásit
roku	rok	k1gInSc3	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
Monrovie	Monrovia	k1gFnSc2	Monrovia
dopláceli	doplácet	k5eAaImAgMnP	doplácet
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
Kru	kra	k1gFnSc4	kra
byl	být	k5eAaImAgInS	být
vytlačen	vytlačit	k5eAaPmNgMnS	vytlačit
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
mysu	mys	k1gInSc2	mys
Mezurado	Mezurada	k1gFnSc5	Mezurada
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
založili	založit	k5eAaPmAgMnP	založit
osadu	osada	k1gFnSc4	osada
Krutown	Krutowna	k1gFnPc2	Krutowna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
spojena	spojit	k5eAaPmNgFnS	spojit
z	z	k7c2	z
Monrovií	Monrovia	k1gFnPc2	Monrovia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
poničeno	poničen	k2eAgNnSc1d1	poničeno
během	během	k7c2	během
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
petrochemie	petrochemie	k1gFnPc4	petrochemie
a	a	k8xC	a
potravinářství	potravinářství	k1gNnSc4	potravinářství
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
zpracování	zpracování	k1gNnSc4	zpracování
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
obchodní	obchodní	k2eAgFnSc7d1	obchodní
třídou	třída	k1gFnSc7	třída
je	on	k3xPp3gFnPc4	on
Gurley	Gurlea	k1gFnPc4	Gurlea
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Providence	providence	k1gFnSc2	providence
stojí	stát	k5eAaImIp3nS	stát
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
významné	významný	k2eAgFnPc4d1	významná
události	událost	k1gFnPc4	událost
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Libérie	Libérie	k1gFnSc2	Libérie
<g/>
.	.	kIx.	.
</s>
<s>
Monrovia	Monrovia	k1gFnSc1	Monrovia
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
pěti	pět	k4xCc2	pět
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
vlastní	vlastní	k2eAgFnSc1d1	vlastní
Monrovia	Monrovia	k1gFnSc1	Monrovia
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Mambapoint	Mambapoint	k1gInSc1	Mambapoint
<g/>
)	)	kIx)	)
Krutown	Krutown	k1gInSc1	Krutown
Camp	camp	k1gInSc1	camp
Johnson	Johnsona	k1gFnPc2	Johnsona
ostrov	ostrov	k1gInSc1	ostrov
Bushrot	Bushrot	k1gMnSc1	Bushrot
Sincop	Sincop	k1gInSc1	Sincop
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
panují	panovat	k5eAaImIp3nP	panovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
<g/>
°	°	k?	°
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
24	[number]	k4	24
<g/>
°	°	k?	°
C.	C.	kA	C.
Nejdeštivější	deštivý	k2eAgInSc4d3	nejdeštivější
měsíc	měsíc	k1gInSc4	měsíc
je	být	k5eAaImIp3nS	být
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
srážky	srážka	k1gFnPc1	srážka
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
958	[number]	k4	958
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
spadne	spadnout	k5eAaPmIp3nS	spadnout
průměrně	průměrně	k6eAd1	průměrně
50	[number]	k4	50
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
797	[number]	k4	797
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
a	a	k8xC	a
udává	udávat	k5eAaImIp3nS	udávat
pouhých	pouhý	k2eAgMnPc2d1	pouhý
700	[number]	k4	700
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
značně	značně	k6eAd1	značně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Monrovia	Monrovia	k1gFnSc1	Monrovia
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
