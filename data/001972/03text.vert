<s>
Vilnius	Vilnius	k1gInSc1	Vilnius
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
česky	česky	k6eAd1	česky
Vilno	Vilno	k6eAd1	Vilno
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
Vilnius	Vilnius	k1gInSc1	Vilnius
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Wilna	Wilen	k2eAgFnSc1d1	Wilen
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Wilno	Wilno	k6eAd1	Wilno
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
В	В	k?	В
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
В	В	k?	В
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
В	В	k?	В
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
měl	mít	k5eAaImAgInS	mít
Vilnius	Vilnius	k1gInSc4	Vilnius
542	[number]	k4	542
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
57,8	[number]	k4	57,8
%	%	kIx~	%
tvořili	tvořit	k5eAaImAgMnP	tvořit
Litevci	Litevec	k1gMnPc1	Litevec
<g/>
,	,	kIx,	,
18,7	[number]	k4	18,7
%	%	kIx~	%
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
14	[number]	k4	14
%	%	kIx~	%
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
4	[number]	k4	4
%	%	kIx~	%
Bělorusové	Bělorus	k1gMnPc1	Bělorus
<g/>
,	,	kIx,	,
0,5	[number]	k4	0,5
%	%	kIx~	%
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
ostatní	ostatní	k2eAgFnSc2d1	ostatní
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
400	[number]	k4	400
km2	km2	k4	km2
<g/>
.	.	kIx.	.
3,6	[number]	k4	3,6
km2	km2	k4	km2
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
plochy	plocha	k1gFnSc2	plocha
zabírá	zabírat	k5eAaImIp3nS	zabírat
Staré	Staré	k2eAgNnSc1d1	Staré
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Vilniusu	Vilnius	k1gInSc2	Vilnius
<g/>
,	,	kIx,	,
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Litvě	Litva	k1gFnSc6	Litva
(	(	kIx(	(
<g/>
54	[number]	k4	54
<g/>
°	°	k?	°
<g/>
41	[number]	k4	41
<g/>
"	"	kIx"	"
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
25	[number]	k4	25
<g/>
°	°	k?	°
<g/>
17	[number]	k4	17
<g/>
"	"	kIx"	"
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
)	)	kIx)	)
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řek	k1gMnSc1	řek
Vilnia	Vilnium	k1gNnSc2	Vilnium
a	a	k8xC	a
Neris	Neris	k1gFnSc2	Neris
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k7c2	poblíž
litevských	litevský	k2eAgFnPc2d1	Litevská
státních	státní	k2eAgFnPc2d1	státní
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
šlo	jít	k5eAaImAgNnS	jít
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
kulturní	kulturní	k2eAgNnSc4d1	kulturní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
geografické	geografický	k2eAgNnSc4d1	geografické
centrum	centrum	k1gNnSc4	centrum
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
Litevského	litevský	k2eAgNnSc2d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
a	a	k8xC	a
také	také	k9	také
o	o	k7c4	o
významné	významný	k2eAgNnSc4d1	významné
město	město	k1gNnSc4	město
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
polsko-litevské	polskoitevský	k2eAgFnSc2d1	polsko-litevská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
přechodové	přechodový	k2eAgInPc4d1	přechodový
mezi	mezi	k7c7	mezi
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
a	a	k8xC	a
přímořským	přímořský	k2eAgInSc7d1	přímořský
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
+6,1	+6,1	k4	+6,1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
lednová	lednový	k2eAgFnSc1d1	lednová
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
-	-	kIx~	-
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
červencová	červencový	k2eAgFnSc1d1	červencová
+17,0	+17,0	k4	+17,0
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
činí	činit	k5eAaImIp3nP	činit
661	[number]	k4	661
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vilniusu	Vilnius	k1gInSc6	Vilnius
bývají	bývat	k5eAaImIp3nP	bývat
extrémně	extrémně	k6eAd1	extrémně
horká	horký	k2eAgNnPc4d1	horké
léta	léto	k1gNnPc4	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teplota	teplota	k1gFnSc1	teplota
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Oblast	oblast	k1gFnSc1	oblast
Vilniusu	Vilnius	k1gMnSc3	Vilnius
byla	být	k5eAaImAgFnS	být
obydlena	obydlet	k5eAaPmNgFnS	obydlet
odedávna	odedávna	k6eAd1	odedávna
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
četné	četný	k2eAgInPc1d1	četný
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Litevská	litevský	k2eAgFnSc1d1	Litevská
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Vilniusu	Vilnius	k1gInSc2	Vilnius
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
<g/>
,	,	kIx,	,
Gediminas	Gediminas	k1gMnSc1	Gediminas
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
věštecký	věštecký	k2eAgInSc1d1	věštecký
sen	sen	k1gInSc1	sen
o	o	k7c6	o
železném	železný	k2eAgMnSc6d1	železný
vlku	vlk	k1gMnSc6	vlk
stojícím	stojící	k2eAgNnPc3d1	stojící
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
požádal	požádat	k5eAaPmAgInS	požádat
kněze	kněz	k1gMnPc4	kněz
o	o	k7c4	o
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
postavit	postavit	k5eAaPmF	postavit
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
velké	velký	k2eAgNnSc4d1	velké
město	město	k1gNnSc4	město
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zkomolením	zkomolení	k1gNnSc7	zkomolení
jména	jméno	k1gNnSc2	jméno
řeky	řeka	k1gFnSc2	řeka
Viliji	Viliji	k1gFnSc2	Viliji
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Vilniusem	Vilnius	k1gInSc7	Vilnius
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psaných	psaný	k2eAgInPc6d1	psaný
pramenech	pramen	k1gInPc6	pramen
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
poprvé	poprvé	k6eAd1	poprvé
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1323	[number]	k4	1323
<g/>
.	.	kIx.	.
</s>
<s>
Vilnius	Vilnius	k1gMnSc1	Vilnius
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známým	známý	k2eAgMnSc7d1	známý
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
Gediminas	Gediminas	k1gMnSc1	Gediminas
pozval	pozvat	k5eAaPmAgMnS	pozvat
německé	německý	k2eAgMnPc4d1	německý
obchodníky	obchodník	k1gMnPc4	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
Gediminasem	Gediminas	k1gMnSc7	Gediminas
na	na	k7c6	na
Hradním	hradní	k2eAgInSc6d1	hradní
kopci	kopec	k1gInSc6	kopec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1387	[number]	k4	1387
byla	být	k5eAaImAgFnS	být
městu	město	k1gNnSc3	město
udělena	udělit	k5eAaPmNgFnS	udělit
magdeburská	magdeburský	k2eAgFnSc1d1	Magdeburská
městská	městský	k2eAgFnSc1d1	městská
práva	právo	k1gNnPc4	právo
Vladislavem	Vladislav	k1gMnSc7	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellem	Jagell	k1gInSc7	Jagell
<g/>
,	,	kIx,	,
králem	král	k1gMnSc7	král
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
velkoknížetem	velkokníže	k1gNnSc7wR	velkokníže
litevským	litevský	k2eAgNnSc7d1	litevské
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1503	[number]	k4	1503
až	až	k9	až
1522	[number]	k4	1522
byly	být	k5eAaImAgFnP	být
vybudovány	vybudován	k2eAgFnPc1d1	vybudována
městské	městský	k2eAgFnPc1d1	městská
hradby	hradba	k1gFnPc1	hradba
s	s	k7c7	s
devíti	devět	k4xCc7	devět
branami	brána	k1gFnPc7	brána
a	a	k8xC	a
třemi	tři	k4xCgFnPc7	tři
věžemi	věž	k1gFnPc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Vilnius	Vilnius	k1gInSc4	Vilnius
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vrchol	vrchol	k1gInSc1	vrchol
svého	svůj	k3xOyFgInSc2	svůj
rozvoje	rozvoj	k1gInSc2	rozvoj
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
posledního	poslední	k2eAgMnSc2d1	poslední
Jagellonce	Jagellonec	k1gMnSc2	Jagellonec
Zikmunda	Zikmund	k1gMnSc2	Zikmund
II	II	kA	II
<g/>
.	.	kIx.	.
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sem	sem	k6eAd1	sem
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
svůj	svůj	k3xOyFgInSc4	svůj
dvůr	dvůr	k1gInSc4	dvůr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1544	[number]	k4	1544
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
stoletích	století	k1gNnPc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
dále	daleko	k6eAd2	daleko
rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
díky	díky	k7c3	díky
založení	založení	k1gNnSc3	založení
Vilniuské	vilniuský	k2eAgFnSc2d1	Vilniuská
univerzity	univerzita	k1gFnSc2	univerzita
králem	král	k1gMnSc7	král
a	a	k8xC	a
velkoknížetem	velkokníže	k1gMnSc7	velkokníže
Štěpánem	Štěpán	k1gMnSc7	Štěpán
Báthorym	Báthorym	k1gInSc4	Báthorym
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1579	[number]	k4	1579
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
a	a	k8xC	a
kulturních	kulturní	k2eAgNnPc2d1	kulturní
středisek	středisko	k1gNnPc2	středisko
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
centrem	centrum	k1gNnSc7	centrum
litevského	litevský	k2eAgNnSc2d1	litevské
velkoknížectví	velkoknížectví	k1gNnSc2	velkoknížectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1655	[number]	k4	1655
byl	být	k5eAaImAgInS	být
Vilnius	Vilnius	k1gInSc1	Vilnius
obsazen	obsadit	k5eAaPmNgInS	obsadit
ruskou	ruský	k2eAgFnSc7d1	ruská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vypleněn	vyplenit	k5eAaPmNgInS	vyplenit
a	a	k8xC	a
vypálen	vypálen	k2eAgInSc1d1	vypálen
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
zmasakrováni	zmasakrovat	k5eAaPmNgMnP	zmasakrovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
však	však	k9	však
již	již	k6eAd1	již
třetím	třetí	k4xOgMnSc7	třetí
největším	veliký	k2eAgMnSc7d3	veliký
městem	město	k1gNnSc7	město
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
zboření	zboření	k1gNnSc4	zboření
hradeb	hradba	k1gFnPc2	hradba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1799	[number]	k4	1799
-	-	kIx~	-
1805	[number]	k4	1805
<g/>
,	,	kIx,	,
zachovala	zachovat	k5eAaPmAgFnS	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
Brána	brána	k1gFnSc1	brána
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
(	(	kIx(	(
<g/>
litevsky	litevsky	k6eAd1	litevsky
Aušros	Aušrosa	k1gFnPc2	Aušrosa
Vartai	Varta	k1gFnSc2	Varta
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Ostra	Ostra	k1gFnSc1	Ostra
brama	brama	k1gFnSc1	brama
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Ostrá	ostrý	k2eAgFnSc1d1	ostrá
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třetím	třetí	k4xOgNnSc6	třetí
dělení	dělení	k1gNnSc6	dělení
Polska	Polska	k1gFnSc1	Polska
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1795	[number]	k4	1795
byl	být	k5eAaImAgMnS	být
Vilnius	Vilnius	k1gMnSc1	Vilnius
anektován	anektovat	k5eAaBmNgMnS	anektovat
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
gubernie	gubernie	k1gFnSc2	gubernie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
město	město	k1gNnSc4	město
dobyl	dobýt	k5eAaPmAgInS	dobýt
Napoleon	napoleon	k1gInSc1	napoleon
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
tažení	tažení	k1gNnSc2	tažení
na	na	k7c4	na
Moskvu	Moskva	k1gFnSc4	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
Vilniusu	Vilnius	k1gInSc2	Vilnius
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hrobem	hrob	k1gInSc7	hrob
tisíců	tisíc	k4xCgInPc2	tisíc
zraněných	zraněný	k2eAgMnPc2d1	zraněný
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
město	město	k1gNnSc4	město
postihly	postihnout	k5eAaPmAgFnP	postihnout
represe	represe	k1gFnPc1	represe
po	po	k7c6	po
nezdařených	zdařený	k2eNgNnPc6d1	nezdařené
polských	polský	k2eAgNnPc6d1	polské
povstáních	povstání	k1gNnPc6	povstání
proti	proti	k7c3	proti
ruské	ruský	k2eAgFnSc3d1	ruská
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadovém	listopadový	k2eAgNnSc6d1	listopadové
povstání	povstání	k1gNnSc6	povstání
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1831-32	[number]	k4	1831-32
byla	být	k5eAaImAgNnP	být
mj.	mj.	kA	mj.
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
vilniuská	vilniuský	k2eAgFnSc1d1	Vilniuská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Vilnius	Vilnius	k1gInSc1	Vilnius
okupován	okupovat	k5eAaBmNgInS	okupovat
Německem	Německo	k1gNnSc7	Německo
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
prohlášen	prohlášen	k2eAgInSc1d1	prohlášen
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
krátce	krátce	k6eAd1	krátce
existující	existující	k2eAgFnSc2d1	existující
Sovětské	sovětský	k2eAgFnSc2d1	sovětská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
Litvy	Litva	k1gFnSc2	Litva
a	a	k8xC	a
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
Vilnius	Vilnius	k1gInSc1	Vilnius
obsazen	obsadit	k5eAaPmNgInS	obsadit
polskými	polský	k2eAgFnPc7d1	polská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Bolševická	bolševický	k2eAgNnPc1d1	bolševické
vojska	vojsko	k1gNnPc1	vojsko
postupující	postupující	k2eAgNnPc1d1	postupující
od	od	k7c2	od
východu	východ	k1gInSc2	východ
ho	on	k3xPp3gInSc4	on
později	pozdě	k6eAd2	pozdě
obsadila	obsadit	k5eAaPmAgFnS	obsadit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
ho	on	k3xPp3gInSc4	on
Poláci	Polák	k1gMnPc1	Polák
dobyli	dobýt	k5eAaPmAgMnP	dobýt
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1920	[number]	k4	1920
město	město	k1gNnSc4	město
opět	opět	k6eAd1	opět
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
Rusové	Rus	k1gMnPc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
ruských	ruský	k2eAgNnPc2d1	ruské
vojsk	vojsko	k1gNnPc2	vojsko
u	u	k7c2	u
Varšavy	Varšava	k1gFnSc2	Varšava
Sověti	Sovět	k1gMnPc1	Sovět
předávají	předávat	k5eAaImIp3nP	předávat
Vilnius	Vilnius	k1gInSc4	Vilnius
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc3d1	vzniklá
Litvě	Litva	k1gFnSc3	Litva
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1920	[number]	k4	1920
však	však	k9	však
Vilnius	Vilnius	k1gInSc1	Vilnius
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
litevsko-běloruská	litevskoěloruský	k2eAgFnSc1d1	litevsko-běloruský
divize	divize	k1gFnSc1	divize
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
je	být	k5eAaImIp3nS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
"	"	kIx"	"
<g/>
Střední	střední	k2eAgFnSc4d1	střední
Litvu	Litva	k1gFnSc4	Litva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
parlament	parlament	k1gInSc1	parlament
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
odhlasoval	odhlasovat	k5eAaPmAgInS	odhlasovat
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
Polsku	Polsko	k1gNnSc3	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vilnius	Vilnius	k1gInSc1	Vilnius
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
vilenského	vilenský	k2eAgNnSc2d1	vilenský
vojvodství	vojvodství	k1gNnSc2	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
Litva	Litva	k1gFnSc1	Litva
nicméně	nicméně	k8xC	nicméně
polskou	polský	k2eAgFnSc4d1	polská
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
nad	nad	k7c7	nad
bývalou	bývalý	k2eAgFnSc7d1	bývalá
Střední	střední	k2eAgFnSc7d1	střední
Litvou	Litva	k1gFnSc7	Litva
nikdy	nikdy	k6eAd1	nikdy
neuznala	uznat	k5eNaPmAgFnS	uznat
a	a	k8xC	a
Vilnius	Vilnius	k1gInSc4	Vilnius
oficiálně	oficiálně	k6eAd1	oficiálně
považovala	považovat	k5eAaImAgFnS	považovat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
ostatně	ostatně	k6eAd1	ostatně
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
navázaly	navázat	k5eAaPmAgInP	navázat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
polské	polský	k2eAgFnSc2d1	polská
vlády	vláda	k1gFnSc2	vláda
prodělal	prodělat	k5eAaPmAgInS	prodělat
Vilnius	Vilnius	k1gInSc1	Vilnius
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
znovuotevřena	znovuotevřen	k2eAgFnSc1d1	znovuotevřen
univerzita	univerzita	k1gFnSc1	univerzita
Štěpána	Štěpán	k1gMnSc2	Štěpán
Báthoryho	Báthory	k1gMnSc2	Báthory
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
měl	mít	k5eAaImAgInS	mít
Vilnius	Vilnius	k1gInSc1	Vilnius
195	[number]	k4	195
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Vilnius	Vilnius	k1gInSc1	Vilnius
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1939	[number]	k4	1939
na	na	k7c6	na
základě	základ	k1gInSc6	základ
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
obsazen	obsadit	k5eAaPmNgInS	obsadit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
armáda	armáda	k1gFnSc1	armáda
nekladla	klást	k5eNaImAgFnS	klást
větší	veliký	k2eAgInSc4d2	veliký
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
její	její	k3xOp3gFnPc1	její
hlavní	hlavní	k2eAgFnPc1d1	hlavní
síly	síla	k1gFnPc1	síla
bojovaly	bojovat	k5eAaImAgFnP	bojovat
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ruských	ruský	k2eAgInPc2d1	ruský
plánů	plán	k1gInPc2	plán
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
Vilnius	Vilnius	k1gInSc1	Vilnius
stát	stát	k5eAaImF	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
litevsko-ruských	litevskouský	k2eAgInPc6d1	litevsko-ruský
rozhovorech	rozhovor	k1gInPc6	rozhovor
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
ale	ale	k8xC	ale
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
předání	předání	k1gNnSc1	předání
města	město	k1gNnSc2	město
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
Litvě	Litva	k1gFnSc3	Litva
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
její	její	k3xOp3gInSc4	její
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
zřízením	zřízení	k1gNnSc7	zřízení
ruských	ruský	k2eAgFnPc2d1	ruská
vojenských	vojenský	k2eAgFnPc2d1	vojenská
základen	základna	k1gFnPc2	základna
na	na	k7c6	na
litevském	litevský	k2eAgNnSc6d1	litevské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
Litva	Litva	k1gFnSc1	Litva
okupována	okupovat	k5eAaBmNgFnS	okupovat
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Vilnius	Vilnius	k1gInSc1	Vilnius
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Litevské	litevský	k2eAgFnSc2d1	Litevská
sovětské	sovětský	k2eAgFnSc2d1	sovětská
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
tajná	tajný	k2eAgFnSc1d1	tajná
policie	policie	k1gFnSc1	policie
NKVD	NKVD	kA	NKVD
tehdy	tehdy	k6eAd1	tehdy
zatkla	zatknout	k5eAaPmAgFnS	zatknout
cca	cca	kA	cca
35	[number]	k4	35
-	-	kIx~	-
40	[number]	k4	40
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
a	a	k8xC	a
poslala	poslat	k5eAaPmAgFnS	poslat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
gulagů	gulag	k1gInPc2	gulag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1941	[number]	k4	1941
padl	padnout	k5eAaImAgInS	padnout
Vilnius	Vilnius	k1gInSc1	Vilnius
do	do	k7c2	do
německých	německý	k2eAgFnPc2d1	německá
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
okupanti	okupant	k1gMnPc1	okupant
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
asi	asi	k9	asi
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
95	[number]	k4	95
%	%	kIx~	%
místních	místní	k2eAgMnPc2d1	místní
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšné	úspěšný	k2eNgNnSc4d1	neúspěšné
židovské	židovský	k2eAgNnSc4d1	Židovské
povstání	povstání	k1gNnSc4	povstání
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1943	[number]	k4	1943
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
konečného	konečný	k2eAgMnSc4d1	konečný
destrukci	destrukce	k1gFnSc4	destrukce
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vedla	vést	k5eAaImAgFnS	vést
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
postava	postava	k1gFnSc1	postava
Jacoba	Jacoba	k1gFnSc1	Jacoba
Gense	Gense	k1gFnSc1	Gense
<g/>
.	.	kIx.	.
</s>
<s>
Vilnius	Vilnius	k1gInSc1	Vilnius
byl	být	k5eAaImAgInS	být
osvobozen	osvobodit	k5eAaPmNgInS	osvobodit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1944	[number]	k4	1944
vojáky	voják	k1gMnPc7	voják
polské	polský	k2eAgFnSc2d1	polská
odbojové	odbojový	k2eAgFnSc2d1	odbojová
Armii	Armie	k1gFnSc3	Armie
Krajowej	Krajowej	k1gInSc1	Krajowej
(	(	kIx(	(
<g/>
Zemské	zemský	k2eAgFnSc2d1	zemská
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
během	během	k7c2	během
Operace	operace	k1gFnSc2	operace
Ostrá	ostrý	k2eAgFnSc1d1	ostrá
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
polskými	polský	k2eAgMnPc7d1	polský
povstalci	povstalec	k1gMnPc7	povstalec
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
sovětské	sovětský	k2eAgFnPc1d1	sovětská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
města	město	k1gNnSc2	město
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
polských	polský	k2eAgMnPc2d1	polský
vojáků	voják	k1gMnPc2	voják
zatčena	zatknout	k5eAaPmNgFnS	zatknout
NKVD	NKVD	kA	NKVD
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
Sověti	Sovět	k1gMnPc1	Sovět
z	z	k7c2	z
Vilniusu	Vilnius	k1gInSc2	Vilnius
odsunuli	odsunout	k5eAaPmAgMnP	odsunout
většinu	většina	k1gFnSc4	většina
polského	polský	k2eAgNnSc2d1	polské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
nahradili	nahradit	k5eAaPmAgMnP	nahradit
je	on	k3xPp3gMnPc4	on
Rusy	Rus	k1gMnPc4	Rus
a	a	k8xC	a
Litevci	Litevec	k1gMnPc5	Litevec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
ve	v	k7c6	v
městě	město	k1gNnSc6	město
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
demonstracím	demonstrace	k1gFnPc3	demonstrace
proti	proti	k7c3	proti
sovětskému	sovětský	k2eAgInSc3d1	sovětský
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
střetu	střet	k1gInSc3	střet
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
obsadit	obsadit	k5eAaPmF	obsadit
televizní	televizní	k2eAgFnSc1d1	televizní
vysílací	vysílací	k2eAgFnSc1d1	vysílací
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
14	[number]	k4	14
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
a	a	k8xC	a
přes	přes	k7c4	přes
700	[number]	k4	700
raněno	ranit	k5eAaPmNgNnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Vilnius	Vilnius	k1gMnSc1	Vilnius
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
společně	společně	k6eAd1	společně
s	s	k7c7	s
rakouským	rakouský	k2eAgInSc7d1	rakouský
Lincem	Linec	k1gInSc7	Linec
za	za	k7c4	za
Evropské	evropský	k2eAgNnSc4d1	Evropské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
kultury	kultura	k1gFnSc2	kultura
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Vilnius	Vilnius	k1gInSc1	Vilnius
je	být	k5eAaImIp3nS	být
výchozím	výchozí	k2eAgInSc7d1	výchozí
bodem	bod	k1gInSc7	bod
dálnic	dálnice	k1gFnPc2	dálnice
Vilnius-Kaunas-Klaipė	Vilnius-Kaunas-Klaipė	k1gFnPc2	Vilnius-Kaunas-Klaipė
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilnius-Panevė	Vilnius-Panevė	k1gFnSc1	Vilnius-Panevė
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilnius-Minsk	Vilnius-Minsk	k1gInSc1	Vilnius-Minsk
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilnius-Grodno	Vilnius-Grodno	k1gNnSc1	Vilnius-Grodno
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilnius-Utena	Vilnius-Uten	k2eAgFnSc1d1	Vilnius-Uten
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilnius-Lida	Vilnius-Lida	k1gFnSc1	Vilnius-Lida
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilnius-Marijampolė	Vilnius-Marijampolė	k1gFnSc1	Vilnius-Marijampolė
(	(	kIx(	(
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
létají	létat	k5eAaImIp3nP	létat
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
všech	všecek	k3xTgNnPc2	všecek
významných	významný	k2eAgNnPc2d1	významné
evropských	evropský	k2eAgNnPc2d1	Evropské
měst	město	k1gNnPc2	město
a	a	k8xC	a
vilniuské	vilniuský	k2eAgNnSc1d1	Vilniuské
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
významným	významný	k2eAgInSc7d1	významný
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
Neris	Neris	k1gFnSc2	Neris
je	být	k5eAaImIp3nS	být
splavná	splavný	k2eAgFnSc1d1	splavná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zajišťována	zajišťován	k2eAgFnSc1d1	zajišťována
trolejbusy	trolejbus	k1gInPc7	trolejbus
a	a	k8xC	a
autobusy	autobus	k1gInPc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
je	být	k5eAaImIp3nS	být
plánován	plánovat	k5eAaImNgInS	plánovat
systém	systém	k1gInSc1	systém
městské	městský	k2eAgFnSc2d1	městská
železnice	železnice	k1gFnSc2	železnice
<g/>
;	;	kIx,	;
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
základ	základ	k1gInSc4	základ
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
nynější	nynější	k2eAgNnSc4d1	nynější
vlakové	vlakový	k2eAgNnSc4d1	vlakové
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
satelitní	satelitní	k2eAgFnSc2d1	satelitní
čtvrti	čtvrt	k1gFnSc2	čtvrt
Naujoji	Naujoj	k1gInSc3	Naujoj
Vilnia	Vilnium	k1gNnSc2	Vilnium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
depo	depo	k1gNnSc1	depo
elektrických	elektrický	k2eAgFnPc2d1	elektrická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
motorové	motorový	k2eAgInPc1d1	motorový
vlaky	vlak	k1gInPc1	vlak
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
Europa	Europa	k1gFnSc1	Europa
Tower	Tower	k1gMnSc1	Tower
</s>
