<s>
Vilnius	Vilnius	k1gInSc4	Vilnius
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vrchol	vrchol	k1gInSc1	vrchol
svého	svůj	k3xOyFgInSc2	svůj
rozvoje	rozvoj	k1gInSc2	rozvoj
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
posledního	poslední	k2eAgMnSc2d1	poslední
Jagellonce	Jagellonec	k1gMnSc2	Jagellonec
Zikmunda	Zikmund	k1gMnSc2	Zikmund
II	II	kA	II
<g/>
.	.	kIx.	.
Augusta	Augusta	k1gMnSc1	Augusta
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sem	sem	k6eAd1	sem
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
svůj	svůj	k3xOyFgInSc4	svůj
dvůr	dvůr	k1gInSc4	dvůr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1544	[number]	k4	1544
<g/>
.	.	kIx.	.
</s>
