<s>
Zoopraxiskop	Zoopraxiskop	k1gInSc1	Zoopraxiskop
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
zoopraxiscope	zoopraxiscop	k1gMnSc5	zoopraxiscop
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
zobrazování	zobrazování	k1gNnSc4	zobrazování
pohyblivého	pohyblivý	k2eAgInSc2d1	pohyblivý
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
promítačka	promítačka	k1gFnSc1	promítačka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
kotouček	kotouček	k1gInSc1	kotouček
s	s	k7c7	s
nalepenými	nalepený	k2eAgFnPc7d1	nalepená
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
točení	točení	k1gNnSc6	točení
klikou	klika	k1gFnSc7	klika
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
kotouč	kotouč	k1gInSc1	kotouč
otáčel	otáčet	k5eAaImAgInS	otáčet
a	a	k8xC	a
přes	přes	k7c4	přes
objektivy	objektiv	k1gInPc4	objektiv
se	se	k3xPyFc4	se
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
promítal	promítat	k5eAaImAgMnS	promítat
pohybující	pohybující	k2eAgMnSc1d1	pohybující
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
jej	on	k3xPp3gNnSc4	on
pionýr	pionýr	k1gMnSc1	pionýr
fotografie	fotografia	k1gFnSc2	fotografia
Eadweard	Eadweard	k1gMnSc1	Eadweard
Muybridge	Muybridge	k1gInSc1	Muybridge
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
první	první	k4xOgNnSc4	první
video	video	k1gNnSc4	video
projektor	projektor	k1gInSc1	projektor
<g/>
.	.	kIx.	.
</s>
<s>
Zoopraxiskop	Zoopraxiskop	k1gInSc1	Zoopraxiskop
promítal	promítat	k5eAaImAgInS	promítat
obraz	obraz	k1gInSc4	obraz
z	z	k7c2	z
rotujícího	rotující	k2eAgInSc2d1	rotující
kotoučku	kotouček	k1gInSc2	kotouček
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
tak	tak	k9	tak
dojem	dojem	k1gInSc4	dojem
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgInP	být
obrazy	obraz	k1gInPc1	obraz
na	na	k7c6	na
kotoučku	kotouček	k1gInSc6	kotouček
namalované	namalovaný	k2eAgNnSc1d1	namalované
na	na	k7c4	na
sklo	sklo	k1gNnSc4	sklo
jako	jako	k8xS	jako
siluety	silueta	k1gFnPc4	silueta
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
disků	disk	k1gInPc2	disk
<g/>
,	,	kIx,	,
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
v	v	k7c6	v
období	období	k1gNnSc6	období
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
obrysy	obrys	k1gInPc4	obrys
kresby	kresba	k1gFnPc1	kresba
vytištěné	vytištěný	k2eAgFnPc1d1	vytištěná
na	na	k7c4	na
disk	disk	k1gInSc4	disk
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
ručně	ručně	k6eAd1	ručně
kolorované	kolorovaný	k2eAgFnPc1d1	kolorovaná
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
animace	animace	k1gFnPc1	animace
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgInPc1d1	složitý
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
řadu	řada	k1gFnSc4	řada
kombinací	kombinace	k1gFnPc2	kombinace
sekvencí	sekvence	k1gFnPc2	sekvence
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Disk	disk	k1gInSc1	disk
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
unikátní	unikátní	k2eAgFnSc7d1	unikátní
metodou	metoda	k1gFnSc7	metoda
–	–	k?	–
obrazy	obraz	k1gInPc1	obraz
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
malých	malý	k2eAgNnPc6d1	malé
trojúhelníkových	trojúhelníkový	k2eAgNnPc6d1	trojúhelníkové
sklíčkách	sklíčko	k1gNnPc6	sklíčko
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
lepily	lepit	k5eAaImAgInP	lepit
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
skleněný	skleněný	k2eAgInSc4d1	skleněný
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
kinetoskop	kinetoskop	k1gInSc4	kinetoskop
Thomase	Thomas	k1gMnSc2	Thomas
Edisona	Edison	k1gMnSc2	Edison
a	a	k8xC	a
Williama	William	k1gMnSc2	William
Kennedyho	Kennedy	k1gMnSc2	Kennedy
Dicksona	Dickson	k1gMnSc2	Dickson
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
komerční	komerční	k2eAgInSc1d1	komerční
systém	systém	k1gInSc1	systém
promítání	promítání	k1gNnSc2	promítání
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
sedmdesáti	sedmdesát	k4xCc3	sedmdesát
jedna	jeden	k4xCgFnSc1	jeden
dochovaných	dochovaný	k2eAgMnPc2d1	dochovaný
disků	disk	k1gInPc2	disk
zoopraxiskopu	zoopraxiskop	k1gInSc2	zoopraxiskop
byly	být	k5eAaImAgFnP	být
nedávno	nedávno	k6eAd1	nedávno
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Eadweard	Eadwearda	k1gFnPc2	Eadwearda
Muybridge	Muybridge	k1gFnSc1	Muybridge
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Kingston	Kingston	k1gInSc1	Kingston
Museum	museum	k1gNnSc1	museum
Bequest	Bequest	k1gInSc1	Bequest
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Projection	Projection	k1gInSc1	Projection
Box	box	k1gInSc1	box
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
