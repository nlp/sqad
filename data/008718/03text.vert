<p>
<s>
Dorian	Dorian	k1gInSc1	Dorian
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
variantou	varianta	k1gFnSc7	varianta
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Dorián	Dorián	k1gMnSc1	Dorián
<g/>
.	.	kIx.	.
</s>
<s>
Vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Dorie	Dorie	k1gFnSc2	Dorie
<g/>
,	,	kIx,	,
Dořan	Dořan	k1gMnSc1	Dořan
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dórové	Dór	k1gMnPc1	Dór
byli	být	k5eAaImAgMnP	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
řeckých	řecký	k2eAgInPc2d1	řecký
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
Dorus	Dorus	k1gMnSc1	Dorus
jejich	jejich	k3xOp3gInSc4	jejich
pravěký	pravěký	k2eAgInSc4d1	pravěký
předek	předek	k1gInSc4	předek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgMnSc4	který
byli	být	k5eAaImAgMnP	být
pojmenováni	pojmenován	k2eAgMnPc1d1	pojmenován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
maďarského	maďarský	k2eAgInSc2d1	maďarský
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dorian	Doriana	k1gFnPc2	Doriana
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Dorian	Dorian	k1gInSc1	Dorian
</s>
</p>
<p>
<s>
Maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Dorián	Dorián	k1gMnSc1	Dorián
</s>
</p>
<p>
<s>
Latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Dorius	Dorius	k1gInSc1	Dorius
</s>
</p>
<p>
<s>
==	==	k?	==
Známí	známý	k2eAgMnPc1d1	známý
nositelé	nositel	k1gMnPc1	nositel
jména	jméno	k1gNnSc2	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
Gray	Graa	k1gFnSc2	Graa
–	–	k?	–
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Obraz	obraz	k1gInSc1	obraz
Doriana	Dorian	k1gMnSc2	Dorian
Graye	Gray	k1gMnSc2	Gray
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gMnSc5	Wild
</s>
</p>
<p>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
Yates	Yates	k1gMnSc1	Yates
–	–	k?	–
anglický	anglický	k2eAgMnSc1d1	anglický
profesionální	profesionální	k2eAgMnSc1d1	profesionální
kulturista	kulturista	k1gMnSc1	kulturista
<g/>
,	,	kIx,	,
šestinásobný	šestinásobný	k2eAgMnSc1d1	šestinásobný
vítěz	vítěz	k1gMnSc1	vítěz
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Olympia	Olympia	k1gFnSc1	Olympia
</s>
</p>
<p>
<s>
Dorian	Dorian	k1gMnSc1	Dorian
Ursuul	Ursuula	k1gFnPc2	Ursuula
–	–	k?	–
princ	princ	k1gMnSc1	princ
s	s	k7c7	s
věšteckým	věštecký	k2eAgInSc7d1	věštecký
darem	dar	k1gInSc7	dar
z	z	k7c2	z
trilogie	trilogie	k1gFnSc2	trilogie
Noční	noční	k2eAgMnSc1d1	noční
anděl	anděl	k1gMnSc1	anděl
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
Brent	Brent	k?	Brent
Weeks	Weeks	k1gInSc1	Weeks
</s>
</p>
