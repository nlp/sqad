<s>
V	v	k7c6	v
pekingském	pekingský	k2eAgInSc6d1	pekingský
dialektu	dialekt	k1gInSc6	dialekt
existují	existovat	k5eAaImIp3nP	existovat
4	[number]	k4	4
intonace	intonace	k1gFnPc4	intonace
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgNnSc4d1	označované
číslem	číslo	k1gNnSc7	číslo
nebo	nebo	k8xC	nebo
diakritikou	diakritika	k1gFnSc7	diakritika
nad	nad	k7c7	nad
samohláskou	samohláska	k1gFnSc7	samohláska
<g/>
:	:	kIx,	:
rovná	rovný	k2eAgFnSc1d1	rovná
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
rovnou	rovnou	k6eAd1	rovnou
čárou	čára	k1gFnSc7	čára
stoupavá	stoupavý	k2eAgFnSc1d1	stoupavá
ze	z	k7c2	z
středních	střední	k2eAgFnPc2d1	střední
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
intonace	intonace	k1gFnSc1	intonace
otázky	otázka	k1gFnSc2	otázka
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
čárkou	čárka	k1gFnSc7	čárka
zprava	zprava	k6eAd1	zprava
stoupavá	stoupavý	k2eAgFnSc1d1	stoupavá
z	z	k7c2	z
hlubokých	hluboký	k2eAgInPc2d1	hluboký
do	do	k7c2	do
středních	střední	k2eAgFnPc2d1	střední
poloh	poloha	k1gFnPc2	poloha
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
hluboké	hluboký	k2eAgNnSc4d1	hluboké
(	(	kIx(	(
<g/>
připomíná	připomínat	k5eAaImIp3nS	připomínat
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
přeptávání	přeptávání	k1gNnSc1	přeptávání
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
Cože	cože	k6eAd1	cože
<g/>
?!!!	?!!!	k?	?!!!
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
háčkem	háček	k1gInSc7	háček
klesavý	klesavý	k2eAgInSc1d1	klesavý
z	z	k7c2	z
vysokých	vysoký	k2eAgFnPc2d1	vysoká
do	do	k7c2	do
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
připomíná	připomínat	k5eAaImIp3nS	připomínat
ostrý	ostrý	k2eAgInSc1d1	ostrý
rozkaz	rozkaz	k1gInSc1	rozkaz
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
