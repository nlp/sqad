<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
přinesla	přinést	k5eAaPmAgFnS	přinést
první	první	k4xOgNnPc4	první
masivní	masivní	k2eAgNnPc4d1	masivní
využití	využití	k1gNnPc4	využití
telegrafu	telegraf	k1gInSc2	telegraf
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnSc2	železnice
a	a	k8xC	a
kulometů	kulomet	k1gInPc2	kulomet
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
porážkou	porážka	k1gFnSc7	porážka
populačně	populačně	k6eAd1	populačně
i	i	k9	i
průmyslově	průmyslově	k6eAd1	průmyslově
slabšího	slabý	k2eAgInSc2d2	slabší
Jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
