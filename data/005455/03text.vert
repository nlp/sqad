<s>
Hlediště	hlediště	k1gNnSc1	hlediště
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
ohraničený	ohraničený	k2eAgInSc1d1	ohraničený
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
z	z	k7c2	z
hlediště	hlediště	k1gNnSc2	hlediště
pozorují	pozorovat	k5eAaImIp3nP	pozorovat
představení	představení	k1gNnSc4	představení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
kině	kino	k1gNnSc6	kino
<g/>
,	,	kIx,	,
koncertním	koncertní	k2eAgInSc6d1	koncertní
sále	sál	k1gInSc6	sál
nebo	nebo	k8xC	nebo
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlediště	hlediště	k1gNnSc1	hlediště
je	být	k5eAaImIp3nS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
diváci	divák	k1gMnPc1	divák
mohli	moct	k5eAaImAgMnP	moct
při	při	k7c6	při
sledování	sledování	k1gNnSc6	sledování
představení	představení	k1gNnSc2	představení
sedět	sedět	k5eAaImF	sedět
nebo	nebo	k8xC	nebo
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podlaha	podlaha	k1gFnSc1	podlaha
hlediště	hlediště	k1gNnSc2	hlediště
v	v	k7c6	v
nově	nově	k6eAd1	nově
budovaných	budovaný	k2eAgInPc6d1	budovaný
objektech	objekt	k1gInPc6	objekt
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
zajištění	zajištění	k1gNnSc2	zajištění
lepší	dobrý	k2eAgFnSc2d2	lepší
viditelnosti	viditelnost	k1gFnSc2	viditelnost
jeviště	jeviště	k1gNnSc2	jeviště
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
jako	jako	k8xC	jako
šikmá	šikmý	k2eAgFnSc1d1	šikmá
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgNnSc1d1	divadelní
hlediště	hlediště	k1gNnSc1	hlediště
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
vpředu	vpředu	k6eAd1	vpředu
odděleno	oddělit	k5eAaPmNgNnS	oddělit
od	od	k7c2	od
jeviště	jeviště	k1gNnSc2	jeviště
portálem	portál	k1gInSc7	portál
s	s	k7c7	s
oponou	opona	k1gFnSc7	opona
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
portálu	portál	k1gInSc2	portál
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
hledištěm	hlediště	k1gNnSc7	hlediště
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
instalována	instalován	k2eAgFnSc1d1	instalována
rampa	rampa	k1gFnSc1	rampa
<g/>
.	.	kIx.	.
</s>
<s>
Hlediště	hlediště	k1gNnSc1	hlediště
v	v	k7c6	v
moderních	moderní	k2eAgNnPc6d1	moderní
kinech	kino	k1gNnPc6	kino
bývají	bývat	k5eAaImIp3nP	bývat
nyní	nyní	k6eAd1	nyní
oddělena	oddělit	k5eAaPmNgNnP	oddělit
pouze	pouze	k6eAd1	pouze
volným	volný	k2eAgInSc7d1	volný
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
u	u	k7c2	u
dříve	dříve	k6eAd2	dříve
budovaných	budovaný	k2eAgInPc2d1	budovaný
objektů	objekt	k1gInPc2	objekt
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
instalována	instalovat	k5eAaBmNgFnS	instalovat
i	i	k9	i
rampa	rampa	k1gFnSc1	rampa
a	a	k8xC	a
použita	použit	k2eAgFnSc1d1	použita
rovněž	rovněž	k9	rovněž
opona	opona	k1gFnSc1	opona
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
hlediště	hlediště	k1gNnSc2	hlediště
na	na	k7c6	na
stadionech	stadion	k1gInPc6	stadion
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k9	především
k	k	k7c3	k
bezpečnostním	bezpečnostní	k2eAgInPc3d1	bezpečnostní
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
často	často	k6eAd1	často
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
několik	několik	k4yIc4	několik
zón	zóna	k1gFnPc2	zóna
(	(	kIx(	(
<g/>
zábradlí	zábradlí	k1gNnSc1	zábradlí
<g/>
,	,	kIx,	,
skleněná	skleněný	k2eAgFnSc1d1	skleněná
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
volný	volný	k2eAgInSc1d1	volný
prostor	prostor	k1gInSc1	prostor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
hlediště	hlediště	k1gNnSc2	hlediště
volný	volný	k2eAgInSc4d1	volný
<g/>
.	.	kIx.	.
</s>
<s>
Divák	divák	k1gMnSc1	divák
musí	muset	k5eAaImIp3nS	muset
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
vstupné	vstupné	k1gNnSc4	vstupné
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
tzv.	tzv.	kA	tzv.
vstupenka	vstupenka	k1gFnSc1	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kartička	kartička	k1gFnSc1	kartička
nebo	nebo	k8xC	nebo
lístek	lístek	k1gInSc1	lístek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
nejen	nejen	k6eAd1	nejen
zaplacení	zaplacení	k1gNnPc4	zaplacení
vstupného	vstupné	k1gNnSc2	vstupné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
údaje	údaj	k1gInPc4	údaj
(	(	kIx(	(
<g/>
název	název	k1gInSc4	název
představení	představení	k1gNnSc2	představení
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
a	a	k8xC	a
čas	čas	k1gInSc1	čas
začátku	začátek	k1gInSc2	začátek
představení	představení	k1gNnSc2	představení
<g/>
,	,	kIx,	,
označení	označení	k1gNnSc2	označení
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
bude	být	k5eAaImBp3nS	být
divák	divák	k1gMnSc1	divák
představení	představení	k1gNnSc4	představení
sledovat	sledovat	k5eAaImF	sledovat
aj	aj	kA	aj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnPc1	vstupenka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc4	vstup
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každé	každý	k3xTgNnSc4	každý
představení	představení	k1gNnSc4	představení
pořadatel	pořadatel	k1gMnSc1	pořadatel
vydává	vydávat	k5eAaPmIp3nS	vydávat
tolik	tolik	k4yIc4	tolik
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kapacitě	kapacita	k1gFnSc3	kapacita
hlediště	hlediště	k1gNnSc2	hlediště
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
do	do	k7c2	do
hlediště	hlediště	k1gNnSc2	hlediště
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
pouze	pouze	k6eAd1	pouze
diváci	divák	k1gMnPc1	divák
se	s	k7c7	s
vstupenkami	vstupenka	k1gFnPc7	vstupenka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
větší	veliký	k2eAgFnSc1d2	veliký
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
nehrozí	hrozit	k5eNaImIp3nS	hrozit
přeplnění	přeplnění	k1gNnSc1	přeplnění
hlediště	hlediště	k1gNnSc2	hlediště
<g/>
.	.	kIx.	.
</s>
