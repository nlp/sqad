<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
ohraničený	ohraničený	k2eAgInSc4d1	ohraničený
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
pozorující	pozorující	k2eAgNnSc1d1	pozorující
představení	představení	k1gNnSc1	představení
<g/>
?	?	kIx.	?
</s>
